import java.io.UnsupportedEncodingException;
import java.util.ArrayList;



/**
 * 
 * ReadStringDataクラス
 * 
 * 2005.12.05	Masami SHIMIZU	2006年度帳票レイアウト変更
 * 
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ReadStringData {

	// 大学数
	private static final int UNIVCOUNT = 8;

	/**
	 * 大学・学部情報を取得
	 * @param lineData
	 * @return
	 */
	public ArrayList getUnivFacultyData(String lineData) throws UnsupportedEncodingException {
		
		String[] univData = null;
		ArrayList lreturn = new ArrayList();
		
		// 読み込み開始位置
		final int START = 14;
		// 大学名の長さ
		final int LENGTH_UNIV = 14;
		// 大学名と学部名の間隔
		final int BLANK = 2;
		// 学部文字列の長さ
		final int LENGTH_FACULTY = 10;
		// 項目間隔
		final int SEPARATE = 38;
		// 入力文字列をbyte変換
		byte[] bt = lineData.getBytes("MS932");
		// 開始位置用
		int start = START;
		for (int i = 0; i < UNIVCOUNT; i++) {
			// 大学名・学部名格納変数
			univData = new String[2];
			// 大学名７文字（14byte）
			univData[0] = RightTrim(getByteString(bt, start, LENGTH_UNIV));
			// 学部名５文字（10文字）
			univData[1] = RightTrim(getByteString(bt, start+LENGTH_UNIV+BLANK, LENGTH_FACULTY));
			lreturn.add(univData);
			start += SEPARATE;
		}
		
		return lreturn;
	}

	/**
	 * 学科情報を取得
	 * @param lineData
	 * @return
	 */
	public ArrayList getDepartmentData(String lineData) throws UnsupportedEncodingException {
		// TODO 自動生成されたメソッド・スタブ
		String[] DepData = null;
		ArrayList lreturn = new ArrayList();
		
		// 読み込み開始位置
		final int START = 18;
		// 学科名の長さ
		final int LENGTH_DEP = 18;
		// 項目間隔
		final int SEPARATE = 35;
		// 入力文字列をbyte変換
		byte[] bt = lineData.getBytes("MS932");
		// 開始位置用
		int start = START;
		
		for (int i = 0; i < UNIVCOUNT; i++)  {
			// 学科名格納変数初期化
			DepData = new String[1];
			DepData[0] = Trim(getByteString(bt, start, LENGTH_DEP));
			lreturn.add(DepData);
			start += SEPARATE;
		}
		
		return lreturn;
	}

	/**
	 * 定員／昨年倍率を取得
	 * @param lineData
	 * @return
	 */
	public ArrayList getRegNumberData(String lineData) throws UnsupportedEncodingException {
		// TODO 自動生成されたメソッド・スタブ
		
		String[] RegNumber = null;
		ArrayList lreturn = new ArrayList();
		
		// 読み込み開始位置
		final int START = 9;
		// 読み込み文字列のバイト長
		final int LENGTH = 10;
		// 定員と昨年倍率の間隔
		final int SEPARATE_STRING = 16;
		// ターゲット文字列間隔
		final int SEPARATE = 32;
		// 入力文字列をbyteに変換
		byte[] bt = lineData.getBytes("MS932");
		// 開始位置用
		int start = START;
		
		for (int i = 0; i < UNIVCOUNT; i++) {
			// 定員と昨年倍率一時格納用変数
			RegNumber = new String[2];
			// 全角数字を半角数字に変換する
			String tmp1 = LeftTrim(getByteString(bt, start, LENGTH));
			String tmp2 = LeftTrim(getByteString(bt, start+SEPARATE_STRING, LENGTH));
			RegNumber[0] = changeHanData(tmp1);
			RegNumber[1] = changeHanData(tmp2);
			lreturn.add(RegNumber);
			start += SEPARATE;
		}
		
		return lreturn;
	}


	/**
	 * 分布データを取得
	 * @param lineData
	 * @return
	 */
	public ArrayList getDistributionData(String lineData) throws UnsupportedEncodingException {
		// TODO 自動生成されたメソッド・スタブ
		String[] DistData = null;
		ArrayList lreturn = new ArrayList();
		
		// 読み込み開始位置
		final int START = 4;
		// 得点文字列のバイト長
		final int LENGTH_SCORE = 8;
		// 出願予定（今年）の判定データのバイト長
		final int LENGTH_NOWJUDGE = 2;
		// 出願予定（今年）の人員データのバイト長
		final int LENGTH_NOWPERSON = 8;
		// 出願予定（昨年）の人員データのバイト長
		final int LENGTH_LASTPERSON = 8;
		// 合否実態（合格）の判定データのバイト長
		final int LENGTH_PASSJUDGE = 2;
		// 合否実態（合格）の人員データのバイト長
		final int LENGTH_PASSPERSON = 6;
		// 合否実態（不合格）の人員データのバイト長
		final int LENGTH_UNPASSPERSON = 8;
		// 得点と出願予定（今年）の判定との間隔
		final int SEPARATE_1 = 10;
		// 出願予定（今年）の判定と人員との間隔
		final int SEPARATE_2 = 2;
		// 出願予定（今年）の人員と出願予定（昨年）の人員との間隔
		final int SEPARATE_3 = 9;
		// 出願予定（昨年）の人員と合否実態（合格）の判定との間隔
		final int SEPARATE_4 = 10;
		// 合否実態（合格）の判定と人員との間隔
		final int SEPARATE_5 = 2;
		// 合否実態（合格）の人員と合否実態（不合格）の人員との間隔
		final int SEPARATE_6 = 7;
		// データ間隔
		final int SEPARATE = 10;
		//入力文字列をbyteに変換
		byte[] bt = lineData.getBytes("MS932");
		
		// 開始位置
		int start = START;
		
		for (int i = 0; i < UNIVCOUNT; i++) {
			DistData = new String[9];
			// 得点
			String sScore = LeftTrim(getByteString(bt, start, LENGTH_SCORE));
			// 出願予定・今年・判定
			String sApplyNowJudge = LeftTrim(getByteString(bt, start+SEPARATE_1, LENGTH_NOWJUDGE));
			// 出願予定・今年・人員
			String sApplyNowPerson = LeftTrim(getByteString(bt, start+SEPARATE_1+SEPARATE_2, LENGTH_NOWPERSON));
			// 出願予定・昨年・人員
			String sApplyLastPerson = LeftTrim(getByteString(bt, start+SEPARATE_1+SEPARATE_2+SEPARATE_3,LENGTH_LASTPERSON));
			// 合否実態・合格・判定
			String sRealPassJudge = LeftTrim(getByteString(bt, start+SEPARATE_1+SEPARATE_2+SEPARATE_3+SEPARATE_4,LENGTH_PASSJUDGE));
			// 合否実態・合格・人員
			String sRealPassPerson = LeftTrim(getByteString(bt, start+SEPARATE_1+SEPARATE_2+SEPARATE_3+SEPARATE_4+SEPARATE_5, LENGTH_PASSPERSON));
			// 合否実態・不合格・人員
			String sRealUnpassPerson = LeftTrim(getByteString(bt, start+SEPARATE_1+SEPARATE_2+SEPARATE_3+SEPARATE_4+SEPARATE_5+SEPARATE_6, LENGTH_UNPASSPERSON));
			
			// 判定を変換
			sApplyNowJudge = changeHanData(sApplyNowJudge);
			if(sApplyNowJudge.equals("A")) {
				sApplyNowJudge = "濃";
			} else if (sApplyNowJudge.equals("B")) {
				sApplyNowJudge = "ボ";
			} else if (sApplyNowJudge.equals("C")) {
				sApplyNowJudge = "注";
			}
			sRealPassJudge = changeHanData(sRealPassJudge);
			if(sRealPassJudge.equals("A")) {
				sRealPassJudge = "濃";
			} else if (sRealPassJudge.equals("B")) {
				sRealPassJudge = "ボ";
			} else if (sRealPassJudge.equals("C")) {
				sRealPassJudge = "注";
			}
			
			DistData[0] = changeHanData(sScore);
			DistData[1] = sApplyNowJudge;
			DistData[2] = changeHanData(sApplyNowPerson);
			// 昨年の出願予定・判定を追加（帳票書き出し用）
			DistData[3] = "";
			DistData[4] = changeHanData(sApplyLastPerson);
			DistData[5] = sRealPassJudge;
			DistData[6] = changeHanData(sRealPassPerson);
			// 合否実態・不合格・判定を追加（帳票書き出し用）
			DistData[7] = "";
			DistData[8] = changeHanData(sRealUnpassPerson);
			lreturn.add(DistData);
			start += SEPARATE_1+SEPARATE_2+SEPARATE_3+SEPARATE_4+SEPARATE_5+SEPARATE_6+SEPARATE;
		}
		
		return lreturn;
	}

	/**
	 * 合計を取得
	 * @param lineData
	 * @return
	 */
	public ArrayList getSumData(String lineData) throws UnsupportedEncodingException {
		// TODO 自動生成されたメソッド・スタブ
		
		String[] SumData = null;
		ArrayList lreturn = new ArrayList();
		
		// 読み込み開始位置
		final int START = 16;
		// 読み込み文字列長
		final int LENGTH = 8;
		// 合否実態（合格）の人員データのバイト長
		final int LENGTH_PASSPERSON = 8;
		// 出願予定（今年）と出願予定（昨年）の間隔
		final int SEPARATE_NOWLAST = 9;
		// 出願予定と合否実態の間隔
		final int SEPARETE_APPPAS = 10;
		// 合否実態（合格）と合否実態（不合格）の間隔
		final int SEPARATE_PASS = 9;
		// データ間隔
		final int SEPARATE = 22;
		// 入力文字列をbyteに変換
		byte[] bt = lineData.getBytes("MS932");
		// 開始位置用
		int start = START;
		
		for (int i = 0; i < UNIVCOUNT; i++) {
			SumData = new String[4];
			String tmpApp1 = LeftTrim(getByteString(bt, start, LENGTH));
			String tmpApp2 = LeftTrim(getByteString(bt, start+SEPARATE_NOWLAST, LENGTH));
			String tmpPass = LeftTrim(getByteString(bt, start+SEPARATE_NOWLAST+SEPARETE_APPPAS, LENGTH_PASSPERSON));
			String tmpUnPass = LeftTrim(getByteString(bt, start+SEPARATE_NOWLAST+SEPARETE_APPPAS+SEPARATE_PASS, LENGTH));
			
			SumData[0] = changeHanData(tmpApp1);
			SumData[1] = changeHanData(tmpApp2);
			SumData[2] = changeHanData(tmpPass);
			SumData[3] = changeHanData(tmpUnPass);
			lreturn.add(SumData);
			start += SEPARATE_NOWLAST+SEPARETE_APPPAS+SEPARATE_PASS+SEPARATE;
		}
		
		return lreturn;
	}

	/**
	 * 平均点を取得
	 * @param lineData
	 * @return
	 */
	public ArrayList getAverageData(String lineData) throws UnsupportedEncodingException {
		// TODO 自動生成されたメソッド・スタブ
		String[] AverageData = null;
		ArrayList lreturn = new ArrayList();
		
		// 読み込み開始位置
		final int START = 12;
		// 読み込み文字列長
		final int LENGTH = 8;
		// 合否実態（合格）の人員データのバイト長
		final int LENGTH_PASSPERSON = 8;
		// 出願予定（今年）と出願予定（昨年）の間隔
		final int SEPARATE_NOWLAST = 9;
		// 出願予定と合否実態の間隔
		final int SEPARETE_APPPAS = 10;
		// 合否実態（合格）と合否実態（不合格）の間隔
		final int SEPARATE_PASS = 9;
		// データ間隔
		final int SEPARATE = 18;
		// 入力文字列をbyteに変換
		byte[] bt = lineData.getBytes("MS932");
		// 開始位置用
		int start = START;
		
		for (int i = 0; i < UNIVCOUNT; i++) {
			AverageData = new String[4];
			String tmpApp1 = Trim(getByteString(bt, start, LENGTH));
			String tmpApp2 = Trim(getByteString(bt, start+SEPARATE_NOWLAST, LENGTH));
			String tmpPass = Trim(getByteString(bt, start+SEPARATE_NOWLAST+SEPARETE_APPPAS, LENGTH_PASSPERSON));
			String tmpUnPass = Trim(getByteString(bt, start+SEPARATE_NOWLAST+SEPARETE_APPPAS+SEPARATE_PASS, LENGTH));
			
			AverageData[0] = changeHanData(tmpApp1);
			AverageData[1] = changeHanData(tmpApp2);
			AverageData[2] = changeHanData(tmpPass);
			AverageData[3] = changeHanData(tmpUnPass);
			lreturn.add(AverageData);
			start += SEPARATE_NOWLAST+SEPARETE_APPPAS+SEPARATE_PASS+SEPARATE;
		}
		
		return lreturn;
	}

	/**
	 * 二次偏差値を取得
	 * @param lineData
	 * @return
	 */
	public ArrayList getRankData(String lineData) throws UnsupportedEncodingException {
		// TODO 自動生成されたメソッド・スタブ
		String[] RankData = null;
		ArrayList lreturn = new ArrayList();
		
		// 読み込み開始位置
		final int START = 20;
		// 二次偏差値（下限）データのバイト長
		final int LENGTH_LOWER = 8;
		// 「〜」のバイト長
		final int LENGTH_WAVE = 2;
		// 二次偏差値（上限）データのバイト長
		final int LENGTH_UPPER = 8;
		// 下限と「〜」の間隔
		final int SEPARATE_LOWERWAVE = 8;
		// 「〜」と上限の間隔
		final int SEPARATE_WAVEUPPER = 2;
		// データ間隔
		final int SEPARATE = 35;
		// 入力文字をbyteに変換
		byte[] bt = lineData.getBytes("MS932");
		// 開始位置用
		int start = START;
		
		for (int i = 0; i < UNIVCOUNT; i++) {
			RankData = new String[3];
			String sLower = Trim(getByteString(bt, start, LENGTH_LOWER));
			String sWave = getByteString(bt, start + SEPARATE_LOWERWAVE, LENGTH_WAVE);
			String sUpper = Trim(getByteString(bt, start + SEPARATE_LOWERWAVE + SEPARATE_WAVEUPPER, LENGTH_UPPER));
			
			RankData[0] = changeHanData(sLower);
			RankData[1] = sWave;
			RankData[2] = changeHanData(sUpper);
			lreturn.add(RankData);
			
			start += SEPARATE_LOWERWAVE + SEPARATE_WAVEUPPER + SEPARATE;
		}
		
		return lreturn;
	}

	
	/**
	 * 第一段階選抜を取得
	 * @param lineData
	 * @return
	 */
	public ArrayList getChoiceData(String lineData) throws UnsupportedEncodingException {
		// TODO 自動生成されたメソッド・スタブ
		String[] choiceData = null;
		ArrayList lreturn = new ArrayList();
		
		// 読み込み開始位置
		final int START = 4;
		// 「第一段階」のバイト長
		final int LENGTH_TITLE = 8;
		// 倍率のバイト長
		final int LENGTH_RATE = 10;
		// 「倍」のバイト長
		final int LENGTH_RATE2 = 2;
		// 「（」のバイト長
		final int LENGTH_LEFT = 2;
		// 得点のバイト長
		final int LENGTH_SCORE = 8;
		// 「点」のバイト長
		final int LENGTH_SCORE2 = 2;
		// 「）」のバイト長
		final int LENGTH_RIGHT = 2;
		// 「第一段階」と倍率の間隔
		final int SEPARETE_TITLERATE = 10;
		// 倍率と「倍」の間隔
		final int SEPARETE_RATERATE2 = 10;
		// 「倍」と「（」の間隔
		final int SEPARATE_RATE2LEFT = 2;
		// 「（」と得点の間隔
		final int SEPARATE_LEFTSCORE = 2;
		// 得点と「点」の間隔
		final int SEPARATE_SCORESCORE2 = 8;
		// 「点」と「）」の間隔
		final int SEPARATE_SCORE2RIGHT =2;
		// データ間隔
		final int SEPARATE = 11;
		// 入力文字をbyteに変換
		byte[] bt = lineData.getBytes("MS932");
		// 開始位置用
		int start = START;
		
		for (int i = 0; i < UNIVCOUNT; i++) {
			choiceData = new String[3];
			// 「第一段階」
			String sTitle = Trim(getByteString(bt, start, LENGTH_TITLE));
			// 倍率
			String sRate = LeftTrim(getByteString(bt, start + SEPARETE_TITLERATE, LENGTH_RATE));
			// 「倍」
			String sRate2 = Trim(getByteString(bt, start + SEPARETE_TITLERATE + SEPARETE_RATERATE2, LENGTH_RATE2));
			// 「（」
			String sLeft = Trim(getByteString(bt, start + SEPARETE_TITLERATE + SEPARETE_RATERATE2 + SEPARATE_RATE2LEFT, LENGTH_LEFT));
			// 得点
			String sScore = LeftTrim(getByteString(bt, start + SEPARETE_TITLERATE + SEPARETE_RATERATE2 + SEPARATE_RATE2LEFT + SEPARATE_LEFTSCORE, LENGTH_SCORE));
			// 「点」
			String sScore2 = Trim(getByteString(bt, start + SEPARETE_TITLERATE + SEPARETE_RATERATE2 + SEPARATE_RATE2LEFT + SEPARATE_LEFTSCORE + SEPARATE_SCORESCORE2, LENGTH_SCORE2));
			// 「）」
			String sRight = Trim(getByteString(bt, start + SEPARETE_TITLERATE + SEPARETE_RATERATE2 + SEPARATE_RATE2LEFT + SEPARATE_LEFTSCORE + SEPARATE_SCORESCORE2 + SEPARATE_SCORE2RIGHT, LENGTH_RIGHT));

			// 得点をそのまま出力する
			if (sScore.equals("−−−")){
			} else if (sScore.length() == 0){
				sScore = "　　　　";
			} else {
				sScore = changeHanData(sScore);
				sScore = setFrontSpace(sScore,LENGTH_SCORE/2);
			}
			
			choiceData[0] = sTitle;
			choiceData[1] = changeHanData(sRate) + sRate2;
			choiceData[2] = changeHanData(sLeft) + sScore + sScore2 + changeHanData(sRight);
			lreturn.add(choiceData);
			
			start += SEPARETE_TITLERATE + SEPARETE_RATERATE2 + SEPARATE_RATE2LEFT + SEPARATE_LEFTSCORE + SEPARATE_SCORESCORE2 + SEPARATE_SCORE2RIGHT + SEPARATE;
		}
		
		return lreturn;
	}

	/**
	 * 出願締切を取得
	 * @param lineData
	 * @return
	 */
	public ArrayList getApplicationData(String lineData) throws UnsupportedEncodingException {
		// TODO 自動生成されたメソッド・スタブ
		String[] choiceData = null;
		ArrayList lreturn = new ArrayList();
		
		// 読み込み開始位置
		final int START = 4;
		// 「出願締切」のバイト長
		final int LENGTH_TITLE = 8;
		// 出願締切１のバイト長
		final int LENGTH_APPLICATION1 = 12;
		// 出願締切２のバイト長
		final int LENGTH_APPLICATION2 = 12;
		// 「第一段階」と出願締切１の間隔
		final int SEPARETE_TITLEAPPLICATION1 = 10;
		// 出願締切１と出願締切２の間隔
		final int SEPARETE_APPLICATION1APPLICATION2 = 14;
		// データ間隔
		final int SEPARATE = 21;
		// 入力文字をbyteに変換
		byte[] bt = lineData.getBytes("MS932");
		// 開始位置用
		int start = START;
		
		for (int i = 0; i < UNIVCOUNT; i++) {
			choiceData = new String[3];
			String sTitle = Trim(getByteString(bt, start, LENGTH_TITLE));
			String sApplication1 = Trim(getByteString(bt, start + SEPARETE_TITLEAPPLICATION1, LENGTH_APPLICATION1));
			String sApplication2 = Trim(getByteString(bt, start + SEPARETE_TITLEAPPLICATION1 + SEPARETE_APPLICATION1APPLICATION2, LENGTH_APPLICATION2));
			
			choiceData[0] = sTitle;
			choiceData[1] = changeHanData(sApplication1);
			choiceData[2] = changeHanData(sApplication2);
			lreturn.add(choiceData);
			
			start += SEPARETE_TITLEAPPLICATION1 + SEPARETE_APPLICATION1APPLICATION2 + SEPARATE;
		}
		
		return lreturn;
	}

	/**
	 * センタ：二次を取得
	 * @param lineData
	 * @return
	 */
	public ArrayList getRatioData(String lineData) throws UnsupportedEncodingException {
		// TODO 自動生成されたメソッド・スタブ
		
		String[] ratioData = null;
		ArrayList lreturn = new ArrayList();
		
		// 読み込み開始位置
		final int START = 20;
		// センター比率のバイト長
		final int LENGTH_CENTERRATIO = 8;
		// 「：」のバイト長
		final int LENGTH_SEMICOLON = 2;
		// 二次比率のバイト長
		final int LENGTH_SECONDRATIO = 8;
		// センター比率と「：」の間隔
		final int SEPARATE_CENTERSEMI = 8;
		// 「：」と二次比率の間隔
		final int SEPARATE_SEMISECOND = 2;
		// データ間隔
		final int SEPARATE = 35;
		// 入力文字をbyteに変換
		byte[] bt = lineData.getBytes("MS932");
		// 開始位置用
		int start = START;
		
		for (int i = 0; i < UNIVCOUNT; i++) {
			ratioData = new String[3];
			// センター比率
			String sCenter = RightTrim(getByteString(bt, start, LENGTH_CENTERRATIO));
			// セミコロン
			String sSemicolon = Trim(getByteString(bt, start + SEPARATE_CENTERSEMI, LENGTH_SEMICOLON));
			// 二次比率
			String sSecond = RightTrim(getByteString(bt, start + SEPARATE_CENTERSEMI + SEPARATE_SEMISECOND , LENGTH_SECONDRATIO));
			
			ratioData[0] = changeHanData(sCenter);
			ratioData[1] = sSemicolon;
			ratioData[2] = changeHanData(sSecond);
			lreturn.add(ratioData);
			
			start += SEPARATE_CENTERSEMI + SEPARATE_SEMISECOND + SEPARATE;
		}
		
		return lreturn;
	}
	
	/**
	 * ボーダ得点を取得
	 * @param lineData
	 * @return
	 */
	public ArrayList getBorderData(String lineData) throws UnsupportedEncodingException {
		// TODO 自動生成されたメソッド・スタブ
		
		String[] ratioData = null;
		ArrayList lreturn = new ArrayList();
		
		// 読み込み開始位置
		final int START = 20;
		// ボーダー得点のバイト長
		final int LENGTH_BORDER = 8;
		// 「（」のバイト長
		final int LENGTH_LEFT = 2;
		// ボーダ得点率のバイト長
		final int LENGTH_BORDERRATE = 6;
		// 「％」のバイト長
		final int LENGTH_PERCENT = 2;
		// 「）」のバイト長
		final int LENGTH_RIGHT = 2;
		// ボーダー得点と「（」の間隔
		final int SEPARATE_BORDERLEFT = 8;
		// 「（」とボーダ得点率の間隔
		final int SEPARATE_LEFTBORDERRATE = 2;
		// ボーダ得点率と「％」の間隔
		final int SEPARATE_BORDERRATEPERCENT = 6;
		// 「％」と「）」の間隔
		final int SEPARATE_PERCENTRIGHT = 2;
		// データ間隔
		final int SEPARATE = 27;
		// 入力文字をbyteに変換
		byte[] bt = lineData.getBytes("MS932");
		// 開始位置用
		int start = START;
		
		for (int i = 0; i < UNIVCOUNT; i++) {
			ratioData = new String[5];
			// ボーダー得点
			String sBorder = LeftTrim(getByteString(bt, start, LENGTH_BORDER));
			// （」
			String sLeft = Trim(getByteString(bt, start + SEPARATE_BORDERLEFT, LENGTH_LEFT));
			// ボーダ得点率
			String sBorderrate = LeftTrim(getByteString(bt, start + SEPARATE_BORDERLEFT + SEPARATE_LEFTBORDERRATE, LENGTH_BORDERRATE));
			// 「％」
			String sPercent = Trim(getByteString(bt, start + SEPARATE_BORDERLEFT + SEPARATE_LEFTBORDERRATE + SEPARATE_BORDERRATEPERCENT, LENGTH_PERCENT));
			// 「）」
			String sRight = Trim(getByteString(bt, start + SEPARATE_BORDERLEFT + SEPARATE_LEFTBORDERRATE + SEPARATE_BORDERRATEPERCENT + SEPARATE_PERCENTRIGHT, LENGTH_RIGHT));
			
			ratioData[0] = changeHanData(sBorder);
			ratioData[1] = sLeft;
			ratioData[2] = changeHanData(sBorderrate);
			ratioData[3] = sPercent;
			ratioData[4] = sRight;
			lreturn.add(ratioData);
			
			start += SEPARATE_BORDERLEFT + SEPARATE_LEFTBORDERRATE + SEPARATE_BORDERRATEPERCENT + SEPARATE_PERCENTRIGHT + SEPARATE;
		}
		
		return lreturn;
	}
	
	/**
	 * 文字列長をあわせるために文字列の最後に半角空白を足す
	 * @param str
	 * @return
	 */
	private String setSpace(String str) {
		// TODO 自動生成されたメソッド・スタブ
		final int STRING_SIZE = 15;
		
		StringBuffer buf = new StringBuffer(str);
		for (int i = 0; i < STRING_SIZE - str.length(); i++) {
			buf.append(" ");
		}
		
		return buf.toString();
	}

	/**
	 * 文字列長をあわせるために文字列の前に半角空白を足す
	 * @param str
	 * @param len
	 * @return
	 */
	private String setFrontSpace(String str, int len) {
		// TODO 自動生成されたメソッド・スタブ

		StringBuffer buf = new StringBuffer("");
		for (int i = 0; i < len - str.length(); i++) {
			buf.append(" ");
		}
		buf.append(str);
		
		return buf.toString();
	}

	/**
	 * バイトから文字列を抜き出す
	 * @param bt		元配列
	 * @param start	開始位置
	 * @param len		長さ
	 * @return
	 */
	private String getByteString(byte[] bt, int start, int len) {
		// TODO 自動生成されたメソッド・スタブ
		
		byte[] buf = new byte[len];
		for (int i = 0; i < len; i++) {
			buf[i] = bt[i+start-1];
		}
		
		String str = null;
		try {
			str = new String(buf, "MS932");
		} catch (UnsupportedEncodingException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return str;
	}
	
	/** 文字列の右側の全角空白を取り除く
	* 
	*  @param    inBuff            String          入力文字列
	*  @return   inBuff(整形後)    String          変換後入力文字列
	*/
	public String RightTrim(String inBuff){
    
		String strData = "";
		int intcnt = 0;
		// 半角空白を省く
        inBuff = inBuff.trim();
		for (int i=inBuff.length(); i > 0 ; i--){
			//後ろから探して最初に出現する全角空白以外の文字の出現位置を特定する
			strData = inBuff.substring(i - 1,i);
			if (!strData.equals("　")){
				intcnt = i;
				break;
			}
			// すべて全角空白の場合には「""」でreturn
			if (i == 1) {
				return "";
			}
		}

		return inBuff.substring(0,intcnt);

	}
	
	/** 文字列の左側の全角空白を取り除く
	* 
	*  @param    inBuff            String          入力文字列
	*  @return   inBuff(整形後)    String          変換後入力文字列
	*/
	public String LeftTrim(String inBuff){

		String strData = "";
		int intcnt = 0;
		//半角空白を省く
		inBuff = inBuff.trim();
		for (int i=0; i < inBuff.length() ; i++){
			//後ろから探して最初に出現する全角空白以外の文字の出現位置を特定する
			strData = inBuff.substring(i,i+1);
			if (!strData.equals("　")){
				intcnt = i;
				break;
			}
			// すべて全角空白の場合には「""」をreturn
			if (i == inBuff.length() -1) {
				return "";
			}
		}

		return inBuff.substring(intcnt,inBuff.length());

	}
	
	/** 文字列の両端の全角空白を取り除く
	* 
	*  @param    inBuff            String          入力文字列
	*  @return   inBuff(整形後)    String          変換後入力文字列
	*/
	public String Trim(String inBuff){

		return RightTrim(LeftTrim(inBuff));

	}
	

	/**
	 * 全角数字を半角数字にする
	 * @param tmp1
	 * @return String
	 */
	private String changeHanData(String tmp) {
		// TODO 自動生成されたメソッド・スタブ
		String str = null;
		if ("".equals(tmp)) {
			str = "";
		}
		else {
			str = JpnStringConv.latinZen2Han(tmp);
		}
		return str;
	}
}

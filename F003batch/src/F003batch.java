import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import com.fujitsu.systemwalker.outputassist.connector.ConnectorException;


/**
 *
 * F003batchクラス
 *
 * 2005.12.05	Masami SHIMIZU	2006年度帳票レイアウト変更
 *
 */
public class F003batch {

    //プロパティファイル名
    private static final String properties = "output.properties";

    // ファイル内に書かれている総ページ数
    private static final int PAGE_NUMBER = 35;
    private static final int DIST_NUMBER = 76;
    // 大学区分（国公立大学）
    private static final String UNIVKB_NATIONAL = "01";
    // 模試名
    private static final String EXAM_NAME = "年度共通テストリサーチ";

    private static String FLHOME = null;
	private static String FLHOSTDATA = null;

    public static void main(String[] args) throws FileNotFoundException {

		try {
			getPropertyData();
			System.out.println("プロパティファイル読み込み完了");
		} catch (IOException e) {
			e.printStackTrace();
		}

        ////////////////////////////////////////////////////////////
        // 2004.11.09 F.Urakawa ホストデータ取得変更
        // ホストデータ保存先をpropertiesファイルから取得
        // FLHOSTDATAに格納
		String[] fileNames = new File(FLHOSTDATA).list();

		HashMap researchMap = new HashMap();

		for (int i = 0; i < fileNames.length; i++) {

			String name = fileNames[i];
			String sUnivcd = name.substring(name.length()-2, name.length());

			ArrayList lUnivList = new ArrayList();
			if (researchMap.containsKey(sUnivcd)) {
				lUnivList = (ArrayList)researchMap.get(sUnivcd);
			// なければ作る
			} else {
				lUnivList = new ArrayList();
				researchMap.put(sUnivcd, lUnivList);
			}
			lUnivList.add(name);
		}
		//ホストデータ格納先取得ここまで
		////////////////////////////////////////////////////////////

        String lineData = null;

        // ファイルデータ読み込み用
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader=null;

        // ファイルデータ書き出し用
        FileOutputStream pdffos = null;
        OutputStreamWriter pdfosw = null;
        BufferedWriter pdfbw = null;

        // 大学・学部用変数
        ArrayList lUnivFaculty = null;
        // 学科用変数
        ArrayList lDepartment = null;
        // 定員・昨年倍率用変数
        ArrayList lRegNumber = null;
        // 分布データ用変数
        ArrayList ldist = null;
        // 合計用変数
        ArrayList lSum = null;
        // 平均点用変数
        ArrayList lAverage = null;
        // 二次偏差値用変数
        ArrayList lRank = null;
        // 第１段階／出願締切用変数
        ArrayList lchoice = null;
        // センタ：二次用変数
        ArrayList lratio = null;
        // ボーダ得点用変数
        ArrayList lborder = null;

        ReadFile rf = new ReadFile();
        ReadStringData rsd = new ReadStringData();

        // ファイル書き出しオブジェクト初期化
        /*----------------------------------------------------------------------------------------------------*/
        /* [2004/11/02 nino] フォルダ構成変更                                                                 */
        /* FLHOME/university/year(4桁)/大学区分コード(2桁)"01"固定/                                           */
        /*----------------------------------------------------------------------------------------------------*/
        /* String path = "C:\\Program Files\\eclipse\\workspace\\keinavi\\freemenu\\2004\\university\\01\\";  */
        /*----------------------------------------------------------------------------------------------------*/

        // ------------------------------------------------------------
		// 2005.01.13
		// Yoshimoto KAWAI - Totec
        // 年度の取得方法を修正
        //Date date =new Date();
        //SimpleDateFormat  formatter = new SimpleDateFormat("yyyy");
        //String year = formatter.format(date);
        String year;
        // センターリサーチ年度
        String cr_year;
        Calendar calendar = Calendar.getInstance();
        // 3月31日までなら前年度・センターリサーチ年度はそのまま
		if (calendar.get(Calendar.MONTH) < 3) {
			year = String.valueOf(calendar.get(Calendar.YEAR) - 1);
			cr_year = String.valueOf(calendar.get(Calendar.YEAR));
		// 4月1日以降ならそのまま・センターリサーチ年度は＋１年
		} else {
			year = String.valueOf(calendar.get(Calendar.YEAR));
			cr_year = String.valueOf(calendar.get(Calendar.YEAR) + 1);
		}

		// ------------------------------------------------------------

		// 模試名をセット
		cr_year = JpnStringConv.latinHan2Zen(cr_year) + EXAM_NAME;

        Set key = researchMap.keySet();
        Iterator it = key.iterator();

		// pdf作成単位をListに追加
		ArrayList allpdfDataList = null;

        // 大学区分ごと
        while (it.hasNext()) {

        	//pdfファイルリストを初期化
        	allpdfDataList = new ArrayList();

        	// 大学区分取得
        	String sUnivkbn = (String)it.next();
        	// ファイル名リスト取得
			ArrayList fileList = (ArrayList)researchMap.get(sUnivkbn);


            String path = FLHOME + "university" + File.separator + year  + File.separator + sUnivkbn + File.separator;
            /*----------------------------------------------------------------------------------------------------*/
            // ディレクトリ作成
            File f = new File(path);
            f.mkdirs();
            //--------------------------

			// ホストデータを取得
			String FilePath = null;
			Iterator fileit = fileList.iterator();


			// 大学区分に属するファイルごと
	        while (fileit.hasNext()) {
	        	String fileName = (String)fileit.next();
	        	FilePath = FLHOSTDATA + File.separator + fileName;

			try{
				fileInputStream= new FileInputStream(FilePath);
				inputStreamReader=new InputStreamReader(fileInputStream,"MS932");
				bufferedReader = new BufferedReader(inputStreamReader);

	            //吐き出し先ファイル名作成
	            String pdfname = makeFileName(fileName);
	            // pdf作成リストに追加
	            allpdfDataList.add(path + pdfname);
	            pdffos = new FileOutputStream(path + pdfname);

	            pdfosw = new OutputStreamWriter(pdffos,"UTF-8");
	            pdfbw = new BufferedWriter(pdfosw);

	            OutputDataFile odf = new OutputDataFile();

	            int i = 0;
	            // ファイル名から大学区分コードを取得
	            String sDivcd = getDivcdFromFileName(fileName);
	            // ファイル名から地区コードを取得
	            String sAreacd = getAreacdFromFileName(fileName);
	            for (; ;) {
	                // 一時格納変数初期化
	                lUnivFaculty = new ArrayList();
	                lDepartment = new ArrayList();
	                lRegNumber = new ArrayList();
	                ldist = new ArrayList();
	                lSum = new ArrayList();
	                lAverage = new ArrayList();
	                lRank = new ArrayList();
	                lchoice = new ArrayList();
	                lratio = new ArrayList();
	                lratio = new ArrayList();
	                lborder = new ArrayList();

	                //１〜２行目は無視
	                rf.throughLine(bufferedReader, 2);
	                // ３行目（大学・学部）
	                lineData = rf.getData(bufferedReader);
	                lUnivFaculty = rsd.getUnivFacultyData(lineData);
	                // ４行目（学科）
	                lineData = rf.getData(bufferedReader);
	                lDepartment = rsd.getDepartmentData(lineData);
	                // ５行目（定員／昨年倍率）
	                lineData = rf.getData(bufferedReader);
	                lRegNumber = rsd.getRegNumberData(lineData);
	                // ６行目（センタ：二次）
	                lineData = rf.getData(bufferedReader);
	                lratio = rsd.getRatioData(lineData);
	                // ７行目（ボーダ得点）
	                lineData = rf.getData(bufferedReader);
	                lborder = rsd.getBorderData(lineData);
	                // ８行目（二次偏差値）
	                lineData = rf.getData(bufferedReader);
	                lRank = rsd.getRankData(lineData);
	                // ９行目（第１段階／出願締切）
	                lineData = rf.getData(bufferedReader);
	                if (sDivcd.equals(UNIVKB_NATIONAL)) {
	                	// 国公立大学
	                	lchoice = rsd.getChoiceData(lineData);
	                } else {
	                	// センター私立・センター短大
	                	lchoice = rsd.getApplicationData(lineData);
	                }
	                // １０行目（無視）
	                rf.throughLine(bufferedReader, 1);
	                // １１〜７６行目（分布データ）
	                for (int cnt = 11; cnt <= DIST_NUMBER; cnt++) {
	                    lineData = rf.getData(bufferedReader);
	                    ldist.add(rsd.getDistributionData(lineData));
	                }
	                // ７７行目（合計）
	                lineData = rf.getData(bufferedReader);
	                lSum = rsd.getSumData(lineData);
	                // ７８行目（平均点）
	                lineData = rf.getData(bufferedReader);
	                lAverage = rsd.getAverageData(lineData);
	                // ７９行目（ページ数）PageデータはListCreatorで作成
	                rf.throughLine(bufferedReader, 1);

	                //ファイル書き出し処理
	                // 引数の語尾二つは地区コードと大学区分コード
	                // 実際はファイル名から取得する
	                odf.outputDataFile(pdfbw,
	                            lUnivFaculty,
	                            lDepartment,
	                            lRegNumber,
	                            ldist,
	                            lSum,
	                            lAverage,
	                            lRank,
	                            lchoice,
	                            lratio,
	                            lborder,
	                            sAreacd,
	                            sDivcd,
	                            new Integer((i+1)%10).toString(),
	                            cr_year);

	                i++;

	                // メモリ確保のため一旦ストリームを閉じる
	                if (pdfbw != null) {
	                    pdfbw.close();
	                }
	                if (pdfosw != null) {
	                    pdfosw.close();
	                }
	                if (pdffos != null) {
	                    pdffos.close();
	                }
	                // ファイルの最後尾に書き込み
	                pdffos = new FileOutputStream(path + pdfname,true);
	                pdfosw = new OutputStreamWriter(pdffos,"UTF-8");
	                pdfbw = new BufferedWriter(pdfosw);
	            }

		        }catch(IOException ioe){
					//ioe.printStackTrace();
		            //throw ioe;

		        }catch (Exception e) {
		            e.printStackTrace();
		        }
		        finally{
		            try{
		                if(bufferedReader!=null){
		                    bufferedReader.close();
		                }
		                if(inputStreamReader!=null){
		                    inputStreamReader.close();
		                }
		                if(fileInputStream!=null){
		                    fileInputStream.close();
		                }
		                if (pdffos != null) {
		                    pdffos.close();
		                }
		                if (pdfosw != null) {
		                    pdfosw.close();
		                }
		                if (pdfbw != null) {
		                    pdfbw.close();
		                }
		            }
		            catch(IOException e){
		                //throw e;
		            }
		        }
	        }

			//帳票出力処理
			Makepdf mkpdf = new Makepdf();
			Iterator f003it = allpdfDataList.iterator();
			try {
				while (f003it.hasNext()) {
					// 帳票作成用のデータファイル（絶対パス）
					String f003path = (String)f003it.next();
					String ScriptName = null;
					String filename = null;

					filename = new File(f003path).getName();
					ScriptName = "f003";
					mkpdf.execute(f003path, filename, ScriptName);
					//使用したデータファイルを削除
					new File(f003path).delete();

				}

				//圧縮処理(短大は圧縮処理をしない)
				if (!"03".equals(sUnivkbn)) {
					Utils.makeZipPDFFile(allpdfDataList);
				}
			} catch (ConnectorException e1) {
				// TODO 自動生成された catch ブロック
				System.out.println(e1.getLocalizedMessage());
				e1.printStackTrace();
			}

			/**** 前年度のリサーチディレクトリを削除 ************************/
			boolean judge = false;
			judge = Utils.delExamDirectory(FLHOME, "university", year, sUnivkbn);
			if (!judge) {
				System.out.println("年度：「" + year + "」 "
								 + "前年度のリサーチディレクトリの削除に失敗しました。");
			}

        }
    }

    /**
     * 入力ファイルから、出力ファイル名を作成する
	 * @param fileName
	 */
	private static String makeFileName(String fileName) {

		String sDivcd = getDivcdFromFileName(fileName);
		String sAreacd = getAreacdFromFileName(fileName);
		return "univ" + sDivcd + sAreacd + ".dat";
	}

	/**
	 * ファイル名から地区コードを取得
	 * @param fileName
	 * @return
	 */
	private static String getAreacdFromFileName(String fileName) {
		// TODO 自動生成されたメソッド・スタブ
		return fileName.substring(fileName.length()-4, fileName.length()-2);
	}

	/**
	 * ファイル名から大学区分コードを取得
	 * @param fileName
	 * @return
	 */
	private static String getDivcdFromFileName(String fileName) {
		// TODO 自動生成されたメソッド・スタブ
		return fileName.substring(fileName.length()-2, fileName.length());
	}

	/**
     * プロパティから環境変数を取得
     */
    private static void getPropertyData() throws IOException{

        String path = System.getProperty("user.dir");
        //path = new File(path).getParent();
        Properties prop = new Properties();
        // 場所は"user.dir"/conf/output.properties
        prop.load(new FileInputStream(new File(path + File.separator + "conf" + File.separator + properties)));
        // propertyから取得
        FLHOME = (String)prop.getProperty("FLHOME");
		FLHOSTDATA = (String)prop.getProperty("FLHOSTDATA");
    }

}
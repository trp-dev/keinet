import java.io.*;

/**
 * バッチ処理時ファイル読み込み用クラス
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ReadFile {
	
	/**
	 * ファイルから行単位でデータを取得
	 * @param bufferedReader
	 * @return
	 */
	public String getData(BufferedReader bufferedReader) throws IOException {
		// TODO 自動生成されたメソッド・スタブ
		String lineData = null;
		if ((lineData = bufferedReader.readLine()) == null) {
			throw new IOException();
		}
		
//		return correctToMS932(lineData);
		return lineData;
	}


	/**
	 * 指定行数分だけ読み飛ばす
	 * @param bufferedReader
	 * @param cnt
	 */
	public void throughLine(BufferedReader bufferedReader, int cnt) throws IOException {
		// TODO 自動生成されたメソッド・スタブ
		for (int i = 0; i < cnt; i++) {
			if (bufferedReader.readLine() == null) {
				throw new IOException();
			}
		}
	}
	
	/**
	 * MS932 文字列の補正を行います。
	 * "Unicode" コンバータでエンコードしようとした際に
	 * 正常に変換できない部分を補正します。
	 */
	public static String correctToMS932(String s) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			buf.append(correctToMS932(s.charAt(i)));
		}
		return new String(buf);
	}

	public static char correctToMS932(char c) {
		switch (c) {
			case 0xff5e:        // WAVE DASH ->
				return 0x301c;  // FULLWIDTH TILDE
			case 0x2225:        // DOUBLE VERTICAL LINE ->
				return 0x2016;  // PARALLEL TO
			case 0xff0d:        // MINUS SIGN ->
				return 0x2212;  // FULLWIDTH HYPHEN-MINUS
		}
		return c;
	}
	
}
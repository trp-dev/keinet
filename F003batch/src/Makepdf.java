import java.io.*;
import com.fujitsu.systemwalker.outputassist.connector.*;


public class Makepdf {
	
	public void execute(String fullpath, String fileName, String ScriptName) throws ConnectorException {
		
		String sAssets;            /* 帳票格納ディレクトリ名           */
		String sPdfDir;            /* PDFファイル格納ディレクトリ名    */
		//String sPdfLocation;       /* PDF格納先URL                     */
		FormsFile form = null;
		
		try{

			/* 初期化パラメータを取得します */
			//sAssets      = "C:\\Program Files\\eclipse\\workspace\\makePDFFile\\";
			//sPdfDir      = "/root/lctest/";
			sPdfDir      = "/root/lctest/";
			//sPdfLocation = "";
			

			/*---受信パラメータを取得します------------------------------*/
			//String temp;

			/* データ区切り文字を取得します */
			String sDelimit    = "|";


			/*---FormsFileオブジェクトを設定します----------------------------*/
			/* 定義帯データを指定します */
			String record = System.getProperty("user.dir");
			/* 定義体が存在するDirを指定して、FormsFile クラスを作成します */
			form = new FormsFile(new File(record).getParent() + File.separator + "lcdefine" + File.separator);
			System.out.println("定義体の場所："+new File(record).getParent() + File.separator + "lcdefine" + File.separator);
			// 入力ファイルをUTF-8で指定します(データファイルパス必要)
			form.setDataFile(fullpath, FormBase.CODE_UTF8);
			System.out.println("datafile:"+fullpath);
			// 定義体ファイルのエンコードを指定します
			form.setFileType(FormBase.CODE_UTF8);
			// 帳票出力時のファイル名を指定します
			form.setScriptFile(ScriptName);
			System.out.println("定義体："+ScriptName);
			//区切り文字を指定します
			form.setGrpDelimit(sDelimit);
			


			
			/*---PrintProperties の設定をします---------------------------*/
			File filepdf = null;        /* 作成するPDFファイルのFileオブジェクト         */

			/* PrintProperites オブジェクトを構築します */
			PrintProperties prop = new PrintProperties();

			/* プロパティ指定(出力方法)をします */
			/* 出力方法 PDFファイルを選択したとき */
			prop.setDirectMethod( PrintProperties.OUTPUTMODE_PDF );

			/* PDFファイル名を生成します */
			String parentPath = new File(fullpath).getParent();
			String tmpName = fileName.substring(0, fileName.indexOf("."));
			filepdf = new File( new File(parentPath) ,tmpName + ".pdf");
			System.out.println("pdf："+filepdf);
			/* プロパティ指定(PDFファイル名)をします */
			prop.setKeepPdf( filepdf.getPath() );
			System.out.println("PDFSavePoint："+filepdf.getPath());
			/* ファイル作者を指定します */
			prop.setPdfAuthor("河合塾");
			/* ファイルタイトルを指定します */
			prop.setTitle("大学・学部・学科別学力分布");
//			/* ファイルのコメントを指定します */
//			prop.setComment("");

			/*---帳票を出力します-----------------------------------------*/

			/* PrintForm オブジェクトを作成します */
			PrintForm pform = new PrintForm();

			/* 帳票を出力します */
			pform.PrintOut( form, prop );

		}finally {
			try{
				/* Javaインタフェースが使用している資源を解放します */
				if ( form != null ){
					form.cleanup();
				}
			}catch ( Exception e ){
				e.printStackTrace();
			}
		}

		
	}
	
}

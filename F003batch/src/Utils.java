import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Utils {

    /**
    * 文字列をカンマで分割して文字列配列にする
    * ※空文字列も含む
    * @param string
    * @return
    */
   public static String[] splitComma(String string) {
       List container = new LinkedList(); // 入れ物

       int index = 0;
       int start = 0;
       while ((index = string.indexOf(',', start)) != -1) {
           container.add(string.substring(start, index));
           start = index + 1;
       }
       container.add(string.substring(start, string.length()));

       return (String[]) container.toArray(new String[0]);
   }


    /**
     * zipファイル作成
     * @param path
     * @param zipFilelist
     * @param examCD
     */
     public static void makeZipCSVFile(String path, ArrayList zipFilelist, String sExamkey, String sAreakey) {
         //入力ストリーム
         InputStream is = null;
         // zipファイル出力ストリーム
         ZipOutputStream os = null;
         //zipファイル出力ストリームインスタンスを作成
         try {
             os = new ZipOutputStream(new FileOutputStream(path + "pat"+sExamkey+sAreakey+"00csv.zip"));
             byte[] buf = new byte[1024];
             // ファイルリスト分処理
             for (int cnt = 0; cnt < zipFilelist.size(); cnt++) {
                 String fp = (String)zipFilelist.get(cnt);
                 String fn = fp.substring(fp.lastIndexOf(File.separator) + 1, fp.length());
                 is = new FileInputStream(fp);
                 ZipEntry ze = new ZipEntry(fn);
                 os.putNextEntry( ze );
                 int len = 0;
                 // 書き込み開始
                 while ((len = is.read(buf)) != -1) {
                     os.write(buf, 0, len);
                 }
                 os.closeEntry();
             }
         } catch (FileNotFoundException e3) {
             // TODO 自動生成された catch ブロック
             e3.printStackTrace();
         } catch (IOException e) {
             // TODO 自動生成された catch ブロック
             e.printStackTrace();
         }
         finally {
             try {
                 if (is != null) is.close();
                 if (os != null) os.close();
             } catch (IOException e4) {
                 // TODO 自動生成された catch ブロック
                 e4.printStackTrace();
             }
         }
    
     }
     
    /**
      * PDFファイル圧縮版を作成
      * @param pdflist1
      * @param flag
      */
     public static void makeZipPDFFile(ArrayList pdflist) {

        // flagの内容を元に作成するファイル名を決定
        String filename = null;
        // 最初のひとつから圧縮ファイルの名前を決定
        String onename = (String)pdflist.get(0);
        // ファイル名を取得
        String tmpname = new File(onename).getName();
        // 拡張子を除く
        String subname = tmpname.substring(0, tmpname.indexOf("."));
        // 名前保管変数
        String tmp = null;
        // 下4桁を取り除く
        tmp = subname.substring(0, subname.length()-2);
        filename = tmp + "00pdf.zip";
        // 保存先ディレクトリを取得する
        String path = new File(onename).getParent();

        //入力ストリーム
        InputStream is = null;
        // zipファイル出力ストリーム
        ZipOutputStream os = null;
        //zipファイル出力ストリームインスタンスを作成
        try {
            os = new ZipOutputStream(new FileOutputStream(path + File.separator + filename));
            byte[] buf = new byte[1024];
            // ファイルリスト分処理
            for (int cnt = 0; cnt < pdflist.size(); cnt++) {
                String fp = (String)pdflist.get(cnt);
                // .datファイル
                String pdfName = new File(fp).getName();
                // 拡張子をpdfに変換
                String fn = pdfName.substring(0, pdfName.indexOf(".")) + ".pdf";
                is = new FileInputStream(path + File.separator + fn);
                ZipEntry ze = new ZipEntry(fn);
                os.putNextEntry( ze );
                int len = 0;
                // 書き込み開始
                while ((len = is.read(buf)) != -1) {
                       os.write(buf, 0, len);
                }
                os.closeEntry();
            }
        } catch (FileNotFoundException e3) {
            // TODO 自動生成された catch ブロック
            e3.printStackTrace();
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
        finally {
            try {
                   if (is != null) is.close();
                   if (os != null) os.close();
            } catch (IOException e4) {
                   // TODO 自動生成された catch ブロック
                   e4.printStackTrace();
            }
        }

     }


	   /**
	* ファイルを作成した模試コードの前年度のディレクトリを削除
	* 模試コードディレクトリが全て削除された場合は前年度ディレクトリも削除
	* 
	* @param  homePath   ホームパス
	*                       "/keinavi/download/freemenu/"
	* @param  divDir     各区分ディレクトリ
	*                       "university"   :大学・学部・学科別学力分布
	* @param  targetYear 作成した年度
	* @param  sUnivkbn   大学区分コード 01:国公立　02:私立　短大:03
	* @return true:成功  false:失敗
	* 
	* [2004/11/16 F.Urakawa]
	*/
   public static boolean delExamDirectory(String homePath, 
											 String divDir, 
											 String targetYear,
											 String sUnivkbn) {
	   boolean judge = true;

	   // 前年度
	   String delYear = String.valueOf(Integer.valueOf(targetYear).intValue() - 1);
	   // 模試コードディレクトリ
	   String path = homePath+divDir+File.separator+delYear+File.separator+sUnivkbn+File.separator;

	   // 大学区分ディレクトリ削除
	   File file = new File(path);
	   if (file.exists()) {
		   // 模試コードディレクトリ配下のディレクトリ及びファイルを全て削除
		   judge = delDir(path);
		   // 指定された模試コードディレクトリを削除
		   if ( judge ) {
			   judge = file.delete();
		   }
	   }
	   // ディレクトリの削除に成功した場合は年度ディレクトリ削除チェック
	   if (judge) {
		   path = homePath + divDir + File.separator + delYear + File.separator;
		   file = new File(path);
		   if (file.exists()) {
			   String[] fileList = file.list();
			   if (fileList.length == 0 ) {
				   judge = file.delete();
			   }
		   }
	   }
	   return judge;
  }
   
   /**
	* ディレクトリの再帰削除処理
	* @param  path       パス
	* @return true:成功  false:失敗
	* 
	* [2004/11/16 F.Urakawa]
	*/
   public static boolean delDir(String path) {
	   boolean judge = true;
	   String[] list = new File( path ).list();

	   for ( int ii = 0 ; ii < list.length ; ii++ ) {
		   File file = new File( path + list[ii] );
		   if ( file.isFile() ) {
			   // ファイル削除
			   judge = file.delete();
		   } else {
			   // ディレクトリの場合、再帰する
			   judge = delDir( path + list[ii] + File.separator );
			   if ( judge ) {
				   judge = file.delete();
			   }
		   }
		   if ( !judge ) {
			   break;
		   }
	   }
	   return judge;
   }

}
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

public class OutputDataFile {

	private static final int UNIVCOUNT = 8;
	private static final String SEPARATOR = "|";

	/**
	 * 帳票作成用データファイル書き出し処理
	 * @param pdfbw
	 * @param lUnivFaculty
	 * @param lDepartment
	 * @param lRegNumber
	 * @param ldist
	 * @param lSum
	 * @param lAverage
	 * @param lRank
	 * @param lchoice
	 * @param lratio
	 * @param 地区コード
	 * @param 大学区分コード
	 * @param ページ番号
	 */
	public void outputDataFile(BufferedWriter pdfbw, ArrayList lUnivFaculty, ArrayList lDepartment, ArrayList lRegNumber, ArrayList ldist, ArrayList lSum, ArrayList lAverage, ArrayList lRank, ArrayList lchoice, ArrayList lratio, ArrayList lborder, String sArea, String sDivision, String page, String cr_year) throws IOException {
		// TODO 自動生成されたメソッド・スタブ
		for (int i = 0; i < ldist.size(); i++) {
			ArrayList ltmpdist = (ArrayList)ldist.get(i);
			// ページ番号
			pdfbw.write(page);
			pdfbw.write(SEPARATOR);
			// センターリサーチ年度
			pdfbw.write(cr_year);
			pdfbw.write(SEPARATOR);
			// 大学区分
			pdfbw.write(makeHeadTitle(sArea, sDivision));
			pdfbw.write(SEPARATOR);
			// 大学名・学部名
			writeData(pdfbw, lUnivFaculty);
			// 学科名
			writeData(pdfbw, lDepartment);
			// 定員・倍率
			writeData(pdfbw, lRegNumber);
			// センタ：二次
			writeData(pdfbw, lratio);
			// ボーダ得点
			writeData(pdfbw, lborder);
			// 二次偏差値
			writeData(pdfbw, lRank);
			// 第１段階／出願締切
			writeData(pdfbw, lchoice);
			// 合計
			writeData(pdfbw, lSum);
			// 平均点
			writeData(pdfbw, lAverage);
			// 分布データ
			for (int cnt = 0; cnt < ltmpdist.size(); cnt++) {
				String[] dist = (String[])ltmpdist.get(cnt);
				writeDestData(pdfbw, dist);
			}
			
			//改行
			pdfbw.newLine();
		}
	}
	
	
	/**
	 * 帳票作成時ファイルヘッダ用「大学区分名（地区名）」を用意する
	 * @param sArea
	 * @param sDivision
	 */
	private  String makeHeadTitle(String sArea, String sDivision) {
		// TODO 自動生成されたメソッド・スタブ
		// 大学区分名
		String sDivName = null;
		// 地区名
		String sAreaName = null;
		
		
		//////////////地区名決定////////////////
		// 北海道
		if ("01".equals(sArea)) {
			sAreaName = "北海道";
		}
		// 東北
		else if ("02".equals(sArea)) {
			sAreaName = "東北";
		}
		// 関東
		else if ("03".equals(sArea)) {
			sAreaName = "関東";
		}
		// 甲信越
		else if ("04".equals(sArea)) {
			sAreaName = "甲信越";
		}
		// 北陸
		else if ("05".equals(sArea)) {
			sAreaName = "北陸";
		}
		// 東海
		else if ("06".equals(sArea)) {
			sAreaName = "東海";
		}
		// 近畿
		else if ("07".equals(sArea)) {
			sAreaName = "近畿";
		}
		// 中国
		else if ("08".equals(sArea)) {
			sAreaName = "中国";
		}
		// 四国
		else if ("09".equals(sArea)) {
			sAreaName = "四国";
		}
		// 九州沖縄
		else if ("10".equals(sArea)) {
			sAreaName = "九州沖縄";
		}
		// 東京
		else if ("11".equals(sArea)){
			sAreaName = "東京";
		}
		// 短大（全国）の場合は"00"
		else {
			sAreaName = "全国";
		}
		
		///////////////大学区分名///////////////////
		// 国公立大学
		if ("01".equals(sDivision)) {
			sDivName = "国公立大学";
		}
		// センター私立
		else if ("02".equals(sDivision)) {
			sDivName = "共テ私立";
		}
		// センター短大
		else {
			sDivName = "共テ短大";
		}
		
		return sAreaName+"( "+sDivName+" )";
	}
	/**
	 * @param pdfbw
	 * @param dep
	 */
	private void writeDestData(BufferedWriter pdfbw, String[] dep) throws IOException {
		// TODO 自動生成されたメソッド・スタブ
		for (int i = 0; i < dep.length; i++) {
			pdfbw.write(dep[i]);
			pdfbw.write(SEPARATOR);
		}
	}
	/**
	 * 書き出し
	 * @param pdfbw
	 * @param list
	 */
	private void writeData(BufferedWriter pdfbw, ArrayList list) throws IOException {
		// TODO 自動生成されたメソッド・スタブ
		
		for (int i = 0; i < UNIVCOUNT; i++) {
			String[] str = (String[])list.get(i);
			for (int j = 0; j < str.length; j++) {
				
				pdfbw.write(str[j]);
				pdfbw.write(SEPARATOR);
			}
		}
		
	}
	
	
	
}
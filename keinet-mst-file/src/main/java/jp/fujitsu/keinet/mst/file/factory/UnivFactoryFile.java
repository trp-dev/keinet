package jp.fujitsu.keinet.mst.file.factory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.fj.kawaijuku.judgement.Constants.COURSE_ALLOT;
import jp.co.fj.kawaijuku.judgement.beans.CefrBean;
import jp.co.fj.kawaijuku.judgement.beans.CefrConvScoreBean;
import jp.co.fj.kawaijuku.judgement.beans.EngPtBean;
import jp.co.fj.kawaijuku.judgement.beans.EngPtDetailBean;
import jp.co.fj.kawaijuku.judgement.beans.ExamSubjectBean;
import jp.co.fj.kawaijuku.judgement.beans.JpnDescTotalEvaluationBean;
import jp.co.fj.kawaijuku.judgement.data.EngSikakuAppInfo;
import jp.co.fj.kawaijuku.judgement.data.EngSikakuScoreInfo;
import jp.co.fj.kawaijuku.judgement.data.JpnKijutuScoreInfo;
import jp.co.fj.kawaijuku.judgement.data.Rank;
import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.kawaijuku.judgement.data.UnivStemma;
import jp.co.fj.kawaijuku.judgement.factory.ParentFactory;
import jp.co.fj.kawaijuku.judgement.factory.UnivComment;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoBasicBean;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoCenterBean;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoContainer;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoKamokuBean;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoSecondBean;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterBasicPlus;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterChoiceSchoolPlus;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterContainer;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterCourse;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSelectGpCourse;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSelectGroup;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSubject;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.fujitsu.keinet.mst.file.model.UnivCircumstance;
import jp.fujitsu.keinet.mst.file.model.UnivDataFile;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;

/**
 *
 * ファイル用マスタ展開処理
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class UnivFactoryFile extends ParentFactory {

    /** 大学マスタ基本情報（UNIVMASTER_BASIC）ファイル */
    private final File fileuUivmasterBasic;

    /** 大学マスタ模試用志望校（UNIVMASTER_CHOICESCHOOL）ファイル */
    private final File fileUnivmasterChoiceschool;

    /** 大学マスタ模試用教科（UNIVMASTER_COURSE）ファイル */
    private final File fileUnivmasterCourse;

    /** 大学マスタ模試用科目（UNIVMASTER_SUBJECT）ファイル */
    private final File fileUnivmasterSubject;

    /** 大学マスタ模試用選択グループ（UNIVMASTER_SELECTGROUP）ファイル */
    private final File fileUnivmasterSelectgroup;

    /** 大学マスタ模試用選択グループ教科（UNIVMASTER_SELECTGPCOURSE）ファイル */
    private final File fileUnivmasterSelectgpcourse;

    /** ランクマスタファイル */
    private final File fileRank;

    /** 大学系統マスタ（UNIVSTEMMA）ファイル */
    private final File fileUnivstemma;

    /** 入試状況ファイル */
    private final File fileCircumstance;

    /** 大学公表合否人数ファイル */
    private final File fileUnivPassFailCount;

    /** 大学情報マスタ（UNIVMASTER_INFO）ファイル */
    private final File fileUnivmasterInfo;

    /** 大学その他名称マスタ（UNIVMASTER_OTHERNAME）ファイル */
    private final File fileUnivmasterOtherName;

    /** 県マスタ（PREFECTURE）ファイル */
    private final File filePrefecture;

    /** 情報誌用科目1ファイル */
    private final File fileKamoku1;

    /** 情報誌用科目2ファイル */
    private final File fileKamoku2;

    /** 情報誌用科目3ファイル */
    private final File fileKamoku3;

    /** 情報誌用科目4ファイル */
    private final File fileKamoku4;

    /** 記述式総合評価マスタファイル */
    /*private  File fileDescCompRating;*/

    /** 模試科目マスタファイル */
    private final File fileExamSubject;

    /** 注釈文マスタファイル */
    private final File fileUnivComment;

    /** 読み込み文字コード */
    private final String charset;

    private String eventYear;

    /**
     * インターネットバンザイ用のコンストラクタです。
     */
    public UnivFactoryFile(String charset, String dir, String fileuUivmasterBasic, String fileUnivmasterChoiceschool, String fileUnivmasterCourse, String fileUnivmasterSubject, String fileUnivmasterSelectgroup,
            String fileUnivmasterSelectgpcourse, String fileRank, String fileUnivstemma, String fileCircumstance, String fileUnivPassFailCount, String fileUnivmasterInfo, String fileUnivmasterOtherName, String filePrefecture,
            String fileKamoku1, String fileKamoku2, String fileKamoku3, String fileKamoku4, String fileUnivComment, String fileDescCompRating, String fileExamSubject) throws FileNotFoundException {

        this.charset = charset;

        this.fileuUivmasterBasic = createFile(dir, fileuUivmasterBasic);
        this.fileUnivmasterChoiceschool = createFile(dir, fileUnivmasterChoiceschool);
        this.fileUnivmasterCourse = createFile(dir, fileUnivmasterCourse);
        this.fileUnivmasterSubject = createFile(dir, fileUnivmasterSubject);
        this.fileUnivmasterSelectgroup = createFile(dir, fileUnivmasterSelectgroup);
        this.fileUnivmasterSelectgpcourse = createFile(dir, fileUnivmasterSelectgpcourse);
        this.fileRank = createFile(dir, fileRank);
        this.fileUnivstemma = createFile(dir, fileUnivstemma);
        this.fileCircumstance = createFile(dir, fileCircumstance);
        this.fileUnivPassFailCount = createFile(dir, fileUnivPassFailCount);
        this.fileUnivmasterInfo = createFile(dir, fileUnivmasterInfo);
        this.fileUnivmasterOtherName = createFile(dir, fileUnivmasterOtherName);
        this.filePrefecture = createFile(dir, filePrefecture);
        this.fileKamoku1 = createFile(dir, fileKamoku1);
        this.fileKamoku2 = createFile(dir, fileKamoku2);
        this.fileKamoku3 = createFile(dir, fileKamoku3);
        this.fileKamoku4 = createFile(dir, fileKamoku4);
        this.fileUnivComment = createFile(dir, fileUnivComment);
        /*this.fileDescCompRating = createFile(dir, fileDescCompRating);*/
        this.fileExamSubject = createFile(dir, fileExamSubject);
    }

    /**
     * モバイルバンザイ用のコンストラクタです。
     */
    public UnivFactoryFile(String charset, String dir, String fileuUivmasterBasic, String fileUnivmasterChoiceschool, String fileUnivmasterCourse, String fileUnivmasterSubject, String fileUnivmasterSelectgroup,
            String fileUnivmasterSelectgpcourse, String fileRank, String fileUnivstemma, String fileCircumstance, String fileUnivPassFailCount) throws FileNotFoundException {
        this(charset, dir, fileuUivmasterBasic, fileUnivmasterChoiceschool, fileUnivmasterCourse, fileUnivmasterSubject, fileUnivmasterSelectgroup, fileUnivmasterSelectgpcourse, fileRank, fileUnivstemma, fileCircumstance,
                fileUnivPassFailCount, null, null, null, null, null, null, null, null, null, null);
    }

    /**
     * 判定コンペアツール用のコンストラクタです。
     */
    public UnivFactoryFile(String charset, String dir, String fileuUivmasterBasic, String fileUnivmasterChoiceschool, String fileUnivmasterCourse, String fileUnivmasterSubject, String fileUnivmasterSelectgroup,
            String fileUnivmasterSelectgpcourse, String fileRank) throws FileNotFoundException {
        this(charset, dir, fileuUivmasterBasic, fileUnivmasterChoiceschool, fileUnivmasterCourse, fileUnivmasterSubject, fileUnivmasterSelectgroup, fileUnivmasterSelectgpcourse, fileRank, null, null, null);
    }

    /**
     * データファイルオブジェクトを生成します。
     *
     * @param dir データファイルディレクトリ
     * @param file データファイル名
     * @return データファイルオブジェクト
     */
    private static File createFile(String dir, String filename) throws FileNotFoundException {
        if (filename == null || filename.length() == 0) {
            return null;
        } else {
            File file = new File(dir, filename);
            if (!file.isFile()) {
                throw new FileNotFoundException(file.getAbsolutePath());
            }
            return file;
        }
    }

    @Override
    protected UnivMasterContainer createUnivMasterContainer() {
        return new UnivMasterFileContainer();
    }

    @Override
    protected void prepareUnivExam(UnivMasterContainer container) throws Exception {
        /* 前年度倍率 */
        preMag2Memory((UnivMasterFileContainer) container);
        /* 入試状況 */
        circumstance2Memory((UnivMasterFileContainer) container);
        /* 大学情報マスタ */
        univInfo2Memory((UnivMasterFileContainer) container);
        /* 大学その他名称マスタ */
        univOtherName2Memory((UnivMasterFileContainer) container);
        /* 県マスタ */
        prefecture2Memory((UnivMasterFileContainer) container);
    }

    /**
     * 前年度倍率をメモリに展開します。
     *
     * @param container {@link UnivMasterFileContainer}
     */
    private void preMag2Memory(UnivMasterFileContainer container) throws IOException {

        if (fileUnivPassFailCount == null) {
            return;
        }

        Reader in = null;
        try {
            in = new InputStreamReader(new FileInputStream(fileUnivPassFailCount), charset);

            for (CSVRecord rec : toCSVParser(in)) {

                if (rec.size() < 8) {
                    System.out.println(fileUnivPassFailCount.getName() + " line :" + rec.getRecordNumber() + " : レコードサイズサイズが不正です。" + rec.size());
                    continue;
                }

                /* 年度 */
                String eventYear = rec.get(0);
                if (eventYear.length() != 4) {
                    /* 年度が4桁でない（ヘッダの「年度」）場合は飛ばす */
                    continue;
                }

                /* 大学コード10桁 */
                String univCd10 = rec.get(2);
                if (!contains(container, univCd10)) {
                    continue;
                }

                /* 大学公表志願者数 */
                BigDecimal snum = toBigDecimal(rec.get(3));

                /* 大学公表受験者数 */
                BigDecimal jnum = toBigDecimal(rec.get(4));

                /* 大学公表合格者数 */
                BigDecimal gnum = toBigDecimal(rec.get(5));

                /* 非公表フラグ */
                if (rec.size() > 8 && "1".equals(rec.get(8))) {
                    continue;
                }

                /* 前年度倍率 */
                BigDecimal preMagnification = null;
                if (gnum.intValue() > 0) {
                    if (jnum.intValue() >= 0) {
                        preMagnification = jnum.divide(gnum, 1, RoundingMode.HALF_UP);
                    } else if (snum.intValue() >= 0) {
                        preMagnification = snum.divide(gnum, 1, RoundingMode.HALF_UP);
                    }
                }
                if (preMagnification != null) {
                    container.getPreMagMap().put(univCd10, preMagnification.toString());
                }
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    /**
     * ReaderをCSVParserに変換します。
     *
     * @param in {@link Reader}
     * @return {@link CSVParser}
     */
    private CSVParser toCSVParser(Reader in) throws IOException {
        return CSVFormat.RFC4180.withRecordSeparator('\n').parse(in);
    }

    /**
     * 文字列をBigDecimalに変換します。空文字列は-1を返します。
     *
     * @param str 文字列
     * @return {@link BigDecimal}
     */
    private BigDecimal toBigDecimal(String str) {
        return StringUtils.isEmpty(str) ? BigDecimal.valueOf(-1) : new BigDecimal(str);
    }

    @Override
    protected void deployStemma(UnivMasterContainer container) throws Exception {

        if (fileUnivstemma == null) {
            return;
        }

        int[] indice = { 4, 4, 2, 4, 1, 4 };

        int length = 0;

        for (int i = 0; i < indice.length; i++) {
            length += indice[i];
        }

        String[] string = new String[indice.length];

        BufferedReader ds = null;
        try {
            ds = new BufferedReader(new InputStreamReader(new FileInputStream(fileUnivstemma), charset));

            int lineNum = 0;

            String line;
            while ((line = ds.readLine()) != null) {

                lineNum++;

                byte[] data = line.getBytes(charset);
                if (data.length != length) {
                    System.out.println(fileUnivstemma.getName() + " line :" + lineNum + " : データの長さが不正です。" + data.length + "/" + length);
                    continue;
                }

                int index = 0;
                for (int i = 0; i < string.length; i++) {
                    string[i] = getString(data, index, index += indice[i], charset);
                }

                final String univKey = string[1] + string[2] + string[3];

                if (!contains(container, univKey)) {
                    continue;
                }

                List stemmas = (List) container.getStemmaMap().get(univKey);
                if (stemmas == null) {
                    stemmas = new ArrayList();
                    container.getStemmaMap().put(univKey, stemmas);
                }

                stemmas.add(new UnivStemma(string[4], string[5]));
            }
        } finally {
            if (ds != null) {
                ds.close();
            }
        }
    }

    /**
     * 入試状況をメモリに展開します。
     *
     * @param container {@link UnivMasterFileContainer}
     */
    private void circumstance2Memory(UnivMasterFileContainer container) throws IOException {

        if (fileCircumstance == null) {
            return;
        }

        int[] indice = { 10, 4, 6, 6, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };

        int length = 0;

        for (int i = 0; i < indice.length; i++) {
            length += indice[i];
        }

        String[] string = new String[indice.length];

        BufferedReader ds = null;
        try {
            ds = new BufferedReader(new InputStreamReader(new FileInputStream(fileCircumstance), charset));

            int lineNum = 0;

            String line;
            while ((line = ds.readLine()) != null) {

                lineNum++;

                byte[] data = line.getBytes(charset);
                if (data.length != length) {
                    System.out.println(fileCircumstance.getName() + " line :" + lineNum + " : データの長さが不正です。" + data.length + "/" + length);
                    continue;
                }

                int index = 0;
                for (int i = 0; i < string.length; i++) {
                    string[i] = getString(data, index, index += indice[i], charset);
                }

                /* 大学コード10桁 */
                String univCd10 = string[0];
                if (!contains(container, univCd10)) {
                    continue;
                }

                /* グラフの起点 */
                int startingPoint = Integer.parseInt(string[1]);

                /* 合計人数 */
                int total = Integer.parseInt(string[2]);

                /* 平均点 */
                double average = Double.parseDouble(string[3]);

                /* 得点に対する人数 */
                int[] details = new int[27];
                for (int i = 0; i < details.length; i++) {
                    details[i] = Integer.parseInt(string[i + 4]);
                }

                container.getCircumstanceMap().put(univCd10, new UnivCircumstance(univCd10, startingPoint, total, average, details));
            }
        } finally {
            if (ds != null) {
                ds.close();
            }
        }
    }

    /**
     * 大学情報マスタをメモリに展開します。
     *
     * @param container {@link UnivMasterFileContainer}
     */
    private void univInfo2Memory(UnivMasterFileContainer container) throws IOException {

        if (fileUnivmasterInfo == null) {
            return;
        }

        Reader in = null;
        try {
            in = new InputStreamReader(new FileInputStream(fileUnivmasterInfo), charset);

            for (CSVRecord rec : toCSVParser(in)) {

                if (rec.size() != 22) {
                    System.out.println(fileUnivmasterInfo.getName() + " line :" + rec.getRecordNumber() + " : レコードサイズサイズが不正です。" + rec.size());
                    continue;
                }

                /* 年度 */
                String eventYear = rec.get(0);
                if (eventYear.length() != 4) {
                    /* 年度が4桁でない場合(ヘッダの「年度」)は飛ばす */
                    continue;
                }

                /* 大学区分 */
                String univDiv = rec.get(1);

                /* 大学コード */
                String univCd = rec.get(2);
                if (!contains(container, univCd)) {
                    continue;
                }

                /* 大学コード表名 */
                String univNameCode = rec.get(4);

                /* 大学属性 */
                String univAttr = rec.get(5);

                /* 大学カナ名 */
                String univNameKana = rec.get(8);

                if ("大".equals(univAttr)) {
                    /* 「大学」「ﾀﾞｲｶﾞｸ」を付加する */
                    univNameCode += "大学";
                    univNameKana += "ﾀﾞｲｶﾞｸ";
                } else if ("6".equals(univDiv)) {
                    /* 「ﾀﾞｲｶﾞｸｺｳ」を付加する */
                    univNameKana += "ﾀﾞｲｶﾞｸｺｳ";
                }

                /* 大学カナ名は全角カタカナに変換して保持する */
                univNameKana = jp.co.fj.kawaijuku.judgement.util.JpnStringUtil.hanKana2ZenKana(univNameKana);

                container.getUnivNameCodeMap().put(univCd, univNameCode);
                container.getUnivNameKanaMap().put(univCd, univNameKana);
                container.getKanaNumMap().put(univCd, rec.get(21));
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    /**
     * 大学その他名称マスタをメモリに展開します。
     *
     * @param container {@link UnivMasterFileContainer}
     */
    private void univOtherName2Memory(UnivMasterFileContainer container) throws IOException {

        if (fileUnivmasterOtherName == null) {
            return;
        }

        Reader in = null;
        try {
            in = new InputStreamReader(new FileInputStream(fileUnivmasterOtherName), "UTF-8");

            for (CSVRecord rec : toCSVParser(in)) {

                if (rec.size() != 3) {
                    System.out.println(fileUnivmasterOtherName.getName() + " line :" + rec.getRecordNumber() + " : レコードサイズサイズが不正です。" + rec.size());
                    continue;
                }

                /* 大学コード */
                String univCd = rec.get(0);
                if (univCd.length() != 4 || !contains(container, univCd)) {
                    /* 大学コードが4桁でない場合(ヘッダの「大学コード」)は飛ばす */
                    continue;
                }

                container.getUnivNameOtherMap().put(univCd, rec.get(2));
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    /**
     * 県マスタをメモリに展開します。
     *
     * @param container {@link UnivMasterFileContainer}
     */
    private void prefecture2Memory(UnivMasterFileContainer container) throws IOException {

        if (filePrefecture == null) {
            return;
        }

        int[] indice = { 2, 6, 2 };

        int length = 0;

        for (int i = 0; i < indice.length; i++) {
            length += indice[i];
        }

        String[] string = new String[indice.length];

        BufferedReader ds = null;
        try {
            ds = new BufferedReader(new InputStreamReader(new FileInputStream(filePrefecture), charset));

            int lineNum = 0;

            String line;
            while ((line = ds.readLine()) != null) {

                lineNum++;

                byte[] data = line.getBytes(charset);
                if (data.length != length) {
                    System.out.println(filePrefecture.getName() + " line :" + lineNum + " : データの長さが不正です。" + data.length + "/" + length);
                    continue;
                }

                int index = 0;
                for (int i = 0; i < string.length; i++) {
                    string[i] = getString(data, index, index += indice[i], charset);
                }

                container.getPrefNameMap().put(string[0], string[1]);
            }
        } finally {
            if (ds != null) {
                ds.close();
            }
        }
    }

    @Override
    protected UnivData createUniv(UnivMasterContainer container, UnivMasterBasicPlus basic) {

        UnivMasterFileContainer fileContainer = (UnivMasterFileContainer) container;

        UnivDataFile univ = new UnivDataFile(basic);

        /* 足切り配点を設定する */
        String pickedPointSub = (String) fileContainer.getPickedPointFullMap().get(univ.getUniqueKey());
        if (pickedPointSub != null) {
            univ.setPickedPointSub(Integer.parseInt(pickedPointSub));
        }

        /* 前年度倍率を設定する */
        setPreMag(univ, (String) fileContainer.getPreMagMap().get(univ.getUniqueKey()));

        /* 入試状況データ設定する */
        univ.setCircumstance((UnivCircumstance) fileContainer.getCircumstanceMap().get(univ.getUniqueKey()));

        return univ;
    }

    @Override
    protected void deployRank(UnivMasterContainer container) throws Exception {

        int[] indice = { 2, 2, 4, 4 };

        int length = 0;

        for (int i = 0; i < indice.length; i++) {
            length += indice[i];
        }

        String[] string = new String[indice.length];

        BufferedReader ds = null;
        try {
            ds = new BufferedReader(new InputStreamReader(new FileInputStream(fileRank), charset));

            int lineNum = 0;

            String line;
            while ((line = ds.readLine()) != null) {

                lineNum++;

                byte[] data = line.getBytes(charset);
                if (data.length != length) {
                    System.out.println(fileRank.getName() + " line :" + lineNum + " : データの長さが不正です。" + data.length + "/" + length);
                    continue;
                }

                int index = 0;
                for (int i = 0; i < string.length; i++) {
                    string[i] = getString(data, index, index += indice[i], charset);
                }

                /* ランクコード */
                String rankCd = string[0];

                /* ランク名 */
                String rankName = string[1];

                /* ランク上限値 */
                double rankHLimit = Double.parseDouble(string[2].equals("99.9") ? "99" : string[2]);

                /* ランク下限値 */
                double rankLLimit = Double.parseDouble(string[3].equals("99.9") ? "99" : string[3]);

                container.getRankMaster().put(rankCd, new Rank(rankName, rankHLimit, rankLLimit));
            }
        } finally {
            if (ds != null) {
                ds.close();
            }
        }
    }

    @Override
    protected void deployUnivMasterBasic(UnivMasterContainer container) throws Exception {

        UnivMasterFileContainer fileContainer = (UnivMasterFileContainer) container;

        int[] indice = { 4, //00:行事年度
                2, //01:模試区分
                1, //02:学校区分
                2, //03:大学区分
                4, //04:大学コード
                2, //05:学部コード
                4, //06:学科コード
                5, //07:志望校コード５桁
                2, //08:コード発生模試区分
                2, //09:コード有効模試区分
                1, //10:入試形態
                1, //11:大学グループ区分
                1, //12:出願制限区分
                3, //13:学部内容コード
                4, //14:学科通番
                1, //15:大地区コード
                3, //16:営業コード
                2, //17:受験校校舎県コード
                2, //18:受験校本部県コード
                2, //19:入試系統コード−中分類
                20, //20:入試系統コード−小分類
                1, //21:文理コード
                18, //22:大学名
                14, //23:学部名
                16, //24:学科名
                14, //25:大学短縮名
                10, //26:学部短縮名
                12, //27:学科短縮名
                12, //28:大学カナ名
                8, //29:学部カナ名
                10, //30:学科カナ名
                7, //31:一般入試定員
                1, //32:入試定員信頼性
                1, //33:大学男女共学区分
                1, //34:大学夜間部区分
                2, //35:大学グループ名
                3, //36:入試試験有無区分
                4, //37:公表用足切倍率
                4, //38:公表用足切予想得点
                //:ランク繰り返し
                2, //39:入試ランク_今年
                5, //40:入試ランク偏差値_今年
                1, //41:ボーダ入力区分
                5, //42:ボーダ得点1_今年
                5, //43:ボーダ得点2_今年
                5, //44:ボーダ得点3_今年
                3, //45:ボーダ得点率1_今年
                3, //46:ボーダ得点率2_今年
                3, //47:ボーダ得点率3_今年
                5, //48:ボーダ得点1(認定試験無し)_今年
                5, //49:ボーダ得点2(認定試験無し)_今年
                5, //50:ボーダ得点3(認定試験無し)_今年
                3, //51:ボーダ得点率1(認定試験無し)_今年
                3, //52:ボーダ得点率2(認定試験無し)_今年
                3, //53:ボーダ得点率3(認定試験無し)_今年
                5, //54:足切予想得点 _今年
                3, //55:足切得点率_今年
                1, //56:ドッキング判定不要区分
                //代表分野コード
                2, //57:代表−中系統
                2, //58:代表−小系統
                //代表−教育内容コード
                2, //59:代表−分野
                2, //60:代表−枝番
                1, //61:戸籍改組区分
                1, //62:日程方式
                1, //63:日程方式枝番
                48, //64:日程方式名
                1, //65:主要大区分
                1, //66:芸大系区分
                //教科配点-センタ
                5, //67:英語教科配点
                5, //68:数学教科配点
                5, //69:国語教科配点
                5, //70:理科教科配点
                5, //71:社会教科配点
                5, //72:その他教科配点
                //教科配点-二次
                5, //73:英語教科配点
                5, //74:数学教科配点
                5, //75:国語教科配点
                5, //76:理科教科配点
                5, //77:社会教科配点
                5, //78:その他教科配点
                //教科配点-第一段階
                5, //79:英語教科配点
                5, //80:数学教科配点
                5, //81:国語教科配点
                5, //82:理科教科配点
                5, //83:社会教科配点
                5, //84:その他教科配点
                1, //85:国公立私大区分
                1, //86:ボーダ入力不要区分
                1, //87:ランク入力不要区分
                1, //88:模試使用区分
                1, //89:リサーチ使用区分
                1, //90:追跡使用区分
                5, //91:入試満点値−共通テスト
                5, //92:総合配点−共通テスト
                5, //93:入試満点値−共通テスト（参加試験なし）
                5, //94:総合配点−共通テスト（参加試験なし）
                1, //95:設定パターン
                3, //96:出願要件パターンコード
                1, //97:英語参加試験ボーダー利用フラグ
                1, //98:英語参加試験代表枝番
                39 //99:学科ソートキー
        };

        int length = 0;

        for (int i = 0; i < indice.length; i++) {
            length += indice[i];
        }

        String[] string = new String[indice.length];

        try (BufferedReader ds = new BufferedReader(new InputStreamReader(new FileInputStream(fileuUivmasterBasic), charset))) {

            int lineNum = 0;

            String line;
            while ((line = ds.readLine()) != null) {

                lineNum++;

                byte[] text = line.getBytes(charset);
                if (text.length != length) {
                    System.out.println(fileuUivmasterBasic.getName() + " line :" + lineNum + " : データの長さが不正です。" + text.length + "/" + length);
                    continue;
                }

                int index = 0;
                for (int i = 0; i < string.length; i++) {
                    string[i] = getString(text, index, index += indice[i], charset);
                }

                this.eventYear = string[0];

                /* 読み込み対象チェック */
                if (!contains(container, string[4] + string[5] + string[6])) {
                    continue;
                }

                /* 大学コード表名 */
                String univNameCode = (String) fileContainer.getUnivNameCodeMap().get(string[4]);
                if (univNameCode == null) {
                    /* 大学情報マスタ.大学コード表名が存在しない場合のデフォルト値を設定 */
                    univNameCode = string[22];
                }

                /* 大学カナ名 */
                String univNameKana = (String) fileContainer.getUnivNameKanaMap().get(string[4]);
                if (univNameKana == null) {
                    /* 大学情報マスタ.大学カナ名が存在しない場合のデフォルト値を設定 */
                    univNameKana = toSearchStr(string[28]);
                }

                UnivMasterBasicPlus data = new UnivMasterBasicPlus( //
                        string[4], //univCd
                        string[5], //facultyCd
                        string[6], //deptCd
                        string[3], //uniDiv
                        string[25], //uniNameAbbr
                        string[26], //facultyNameAbbr
                        string[27], //deptNameAbbr
                        toSearchStr(string[28]), //大学名ｶﾅ
                        toSearchStr(string[30]), //学科名ｶﾅ
                        string[11].length() == 0 ? JudgementConstants.Univ.SCHEDULE_NML : string[11], //uniGDiv
                        string[18], //prefCdExamHo
                        string[17], //prefCdExamSh
                        Integer.parseInt(string[42].equals("99999") ? "9999" : string[42]), //borderScore1
                        Integer.parseInt(string[43].equals("99999") ? "9999" : string[43]), //borderScore2
                        Integer.parseInt(string[44].equals("99999") ? "9999" : string[44]), //borderScore3
                        string[46].equals("999") ? 99.0 : Double.parseDouble(string[46]), //borderScoreRate2
                        string[21], //bunriCd
                        "9999999".equals(string[31]) ? 0 : Integer.parseInt(string[31]), //pubEntExamCapa
                        Integer.parseInt(string[33]), //uniCoedDiv
                        Integer.parseInt(string[34]), //uniNightDiv
                        string[39], //entExamRank
                        string[99], //deptSortKey
                        "9999".equals(string[38]) ? 0 : Integer.parseInt(string[38]), //pubRejectExScore
                        Integer.parseInt(string[54]), //rejectExScore
                        Integer.parseInt(string[56]), //dockingNeedLDiv
                        (String) fileContainer.getPrefNameMap().get(string[18]), //本部県名
                        (String) fileContainer.getPrefNameMap().get(string[17]), //校舎県名
                        null, //※バンザイでは不要 rs.getString("DISTRICTCD"),
                        null, //※バンザイでは不要 rs.getString("REMARKS"));
                        string[7], //大学コード5桁
                        univNameCode, //大学コード表名
                        univNameKana, //大学カナ名
                        (String) fileContainer.getUnivNameOtherMap().get(string[4]), //大学その他名称
                        (String) fileContainer.getKanaNumMap().get(string[4]), //カナ付50音順番号
                        string[13], //学部内容コード
                        string[14], //学科通番
                        string[62], //日程方式
                        string[63], //日程方式枝番
                        string[32], //入試定員信頼性
                        string[41], //ボーダ入力区分:BORDERINDIV
                        Integer.parseInt(string[48].equals("99999") ? "9999" : string[48]), //ボーダ得点1(認定試験無し)_今年:BORDERSCORENOEXAM1
                        Integer.parseInt(string[49].equals("99999") ? "9999" : string[49]), //ボーダ得点2(認定試験無し)_今年:BORDERSCORENOEXAM2
                        Integer.parseInt(string[50].equals("99999") ? "9999" : string[50]), //ボーダ得点3(認定試験無し)_今年:BORDERSCORENOEXAM3
                        string[51].equals("999") ? 99.0 : Double.parseDouble(string[51]), //ボーダ得点率1(認定試験無し)_今年:BORDERSCORERATENOEXAM1
                        string[52].equals("999") ? 99.0 : Double.parseDouble(string[52]), //ボーダ得点率2(認定試験無し)_今年:BORDERSCORERATENOEXAM2
                        string[53].equals("999") ? 99.0 : Double.parseDouble(string[53]), //ボーダ得点率3(認定試験無し)_今年:BORDERSCORERATENOEXAM3
                        Integer.parseInt(string[91].equals("99999") ? "9999" : string[91]), //入試満点値−共通テスト:ENTEXAMFULLPNT_CENTER
                        Integer.parseInt(string[92].equals("99999") ? "9999" : string[92]), //総合配点−共通テスト:TOTALALLOTPNT_CENTER
                        Integer.parseInt(string[93].equals("99999") ? "9999" : string[93]), //入試満点値−共通テスト（参加試験なし）:ENTEXAMFULLPNT_CENTER_NOEXAM
                        Integer.parseInt(string[94].equals("99999") ? "9999" : string[94]), //総合配点−共通テスト（参加試験なし）:TOTALALLOTPNT_CENTER_NOEXAM
                        string[95], //設定パターン:SETTINGPATTERN
                        string[96], //出願要件パターンコードAPP_REQ_PATTERN_CD
                        string[97], //英語参加試験ボーダー利用フラグ:ENGPT_BORDERUSEFLG
                        string[98] //英語参加試験代表枝番:ENGPT_REPBRANCH
                );

                container.getUnivMasterBasic().add(data);
            }
        }
    }

    /**
     * 半角カタカナ文字列を検索用に変換します。
     *
     * @param string 変換する半角カタカナ文字列
     * @return 変換した半角カタカナ文字列
     */
    private String toSearchStr(String string) {
        return StringUtils.replaceChars(string, "-ｧｨｩｪｫｬｭｮｯ ﾞﾟ", "ｰｱｲｳｴｵﾔﾕﾖﾂ");
    }

    @Override
    protected void deployUnivMasterChoiceSchool(UnivMasterContainer container) throws Exception {

        UnivMasterFileContainer fileContainer = (UnivMasterFileContainer) container;

        int[] indice = { 4, //00:行事年度
                1, //01:学校区分
                4, //02:大学コード
                2, //03:学部コード
                4, //04:学科コード
                2, //05:模試区分
                1, //06:推薦用区分
                1, //07:来年用区分
                1, //08:入試試験区分
                3, //09:大学コード枝番
                3, //10:大学コード枝番−元
                1, //11:自動枝番フラグ
                4, //12:総合配点
                4, //13:総合配点＿小論文なし
                4, //14:入試満点値
                4, //15:入試満点値＿小論文なし
                5, //16:イレギュラ配点1
                1, //17:最大選択科目数−英語1
                1, //18:最大選択科目数−数学1
                1, //19:最大選択科目数−国語1
                1, //20:最大選択科目数−理科1
                1, //21:最大選択科目数−社会1
                1, //22:イレギュラ科目数1
                5, //23:イレギュラ配点2
                1, //24:最大選択科目数−英語2
                1, //25:最大選択科目数−数学2
                1, //26:最大選択科目数−国語2
                1, //27:最大選択科目数−理科2
                1, //28:最大選択科目数−社会2
                1, //29:イレギュラ科目数2
                5, //30:イレギュラ配点3
                1, //31:最大選択科目数−英語3
                1, //32:最大選択科目数−数学3
                1, //33:最大選択科目数−国語3
                1, //34:最大選択科目数−理科3
                1, //35:最大選択科目数−社会3
                1, //36:イレギュラ科目数3
                5, //37:イレギュラ配点4
                1, //38:最大選択科目数−英語4
                1, //39:最大選択科目数−数学4
                1, //40:最大選択科目数−国語4
                1, //41:最大選択科目数−理科4
                1, //42:最大選択科目数−社会4
                1, //43:イレギュラ科目数4
                5, //44:イレギュラ配点5
                1, //45:最大選択科目数−英語5
                1, //46:最大選択科目数−数学5
                1, //47:最大選択科目数−国語5
                1, //48:最大選択科目数−理科5
                1, //49:最大選択科目数−社会5
                1, //50:イレギュラ科目数5
                5, //51:イレギュラ配点6
                1, //52:最大選択科目数−英語6
                1, //53:最大選択科目数−数学6
                1, //54:最大選択科目数−国語6
                1, //55:最大選択科目数−理科6
                1, //56:最大選択科目数−社会6
                1, //57:イレギュラ科目数6
                200, //58:判定科目名
                1, //59:第一解答科目フラグー理科
                1, //60:第一解答科目フラグー社会
                1, //61:優先枝番
                1, //62:理科同一科目選択不可
                3, //63:得点換算パターンコード−英語参加試験
                1, //64:加点区分−英語参加試験
                4, //65:加点配点−英語参加試験
                3, //66:得点換算パターンコード−国語記述式
                1, //67:加点区分−国語記述式
                4, //68:加点配点−国語記述式
                1, //69:最善枝番ソート用得点換算利用区分
                8 //70:大学マスタ最終更新日
        };

        int length = 0;

        for (int i = 0; i < indice.length; i++) {
            length += indice[i];
        }

        String[] string = new String[indice.length];

        try (BufferedReader ds = new BufferedReader(new InputStreamReader(new FileInputStream(fileUnivmasterChoiceschool), charset))) {

            int lineNum = 0;

            String line;
            while ((line = ds.readLine()) != null) {

                lineNum++;

                byte[] text = line.getBytes(charset);
                if (text.length != length) {
                    System.out.println(fileUnivmasterChoiceschool.getName() + " line :" + lineNum + " : データの長さが不正です。" + text.length + "/" + length);
                    continue;
                }

                int index = 0;
                for (int i = 0; i < string.length; i++) {
                    string[i] = getString(text, index, index += indice[i], charset);
                }

                /* 読み込み対象チェック */
                String key = string[2] + string[3] + string[4];
                if (!contains(container, key)) {
                    continue;
                }

                /* 学校区分が「5」のみ対象 */
                if (!"5".equals(string[1])) {
                    continue;
                }

                /* 推薦用区分が「0」のみ対象 */
                if (!"0".equals(string[6])) {
                    continue;
                }

                /* 来年用区分が「0」または「9」のみ対象 */
                if (!("0".equals(string[7]) || "9".equals(string[7]))) {
                    continue;
                }

                /* 入試試験区分が「3」かつ枝番が「001」 */
                String entExamDiv = string[8];
                String branchCd = string[9];
                if ("3".equals(entExamDiv) && "001".equals(branchCd)) {
                    fileContainer.getPickedPointFullMap().put(string[2] + string[3] + string[4], string[15]);
                }

                /* 入試試験区分が「1」または「2」または「3」のみ対象 */
                if (!("1".equals(entExamDiv) || "2".equals(entExamDiv) || "3".equals(entExamDiv))) {
                    continue;
                }

                UnivMasterChoiceSchoolPlus data = new UnivMasterChoiceSchoolPlus(//
                        Integer.parseInt(string[12]), //rs.getInt("TOTALALLOTPNT"),
                        Integer.parseInt(string[13]), //rs.getInt("TOTALALLOTPNT_NREP"),
                        Integer.parseInt(string[14]), //rs.getInt("ENTEXAMFULLPNT"),
                        string[15], //rs.getString("ENTEXAMFULLPNT_NREP"),
                        Double.parseDouble(string[16]) / 10, //rs.getString("IRRALALLOTPNT1"),
                        Integer.parseInt(string[17]), //rs.getString("MAXSELECT_EN1"),
                        Integer.parseInt(string[18]), //rs.getString("MAXSELECT_MA1"),
                        Integer.parseInt(string[19]), //rs.getString("MAXSELECT_JA1"),
                        Integer.parseInt(string[20]), //rs.getString("MAXSELECT_SC1"),
                        Integer.parseInt(string[21]), //rs.getString("MAXSELECT_SO1"),
                        Integer.parseInt(string[22]), //rs.getString("IRR_SUB1"),
                        Double.parseDouble(string[23]) / 10, //rs.getString("IRRALALLOTPNT2"),
                        Integer.parseInt(string[24]), //rs.getString("MAXSELECT_EN2"),
                        Integer.parseInt(string[25]), //rs.getString("MAXSELECT_MA2"),
                        Integer.parseInt(string[26]), //rs.getString("MAXSELECT_JA2"),
                        Integer.parseInt(string[27]), //rs.getString("MAXSELECT_SC2"),
                        Integer.parseInt(string[28]), //rs.getString("MAXSELECT_SO2"),
                        Integer.parseInt(string[29]), //rs.getString("IRR_SUB2"),
                        Double.parseDouble(string[30]) / 10, //rs.getString("IRRALALLOTPNT3"),
                        Integer.parseInt(string[31]), //rs.getString("MAXSELECT_EN3"),
                        Integer.parseInt(string[32]), //rs.getString("MAXSELECT_MA3"),
                        Integer.parseInt(string[33]), //rs.getString("MAXSELECT_JA3"),
                        Integer.parseInt(string[34]), //rs.getString("MAXSELECT_SC3"),
                        Integer.parseInt(string[35]), //rs.getString("MAXSELECT_SO3"),
                        Integer.parseInt(string[36]), //rs.getString("IRR_SUB3"),
                        Double.parseDouble(string[37]) / 10, //rs.getString("IRRALALLOTPNT4"),
                        Integer.parseInt(string[38]), //rs.getString("MAXSELECT_EN4"),
                        Integer.parseInt(string[39]), //rs.getString("MAXSELECT_MA4"),
                        Integer.parseInt(string[40]), //rs.getString("MAXSELECT_JA4"),
                        Integer.parseInt(string[41]), //rs.getString("MAXSELECT_SC4"),
                        Integer.parseInt(string[42]), //rs.getString("MAXSELECT_SO4"),
                        Integer.parseInt(string[43]), //rs.getString("IRR_SUB4"),
                        Double.parseDouble(string[44]) / 10, //rs.getString("IRRALALLOTPNT5"),
                        Integer.parseInt(string[45]), //rs.getString("MAXSELECT_EN5"),
                        Integer.parseInt(string[46]), //rs.getString("MAXSELECT_MA5"),
                        Integer.parseInt(string[47]), //rs.getString("MAXSELECT_JA5"),
                        Integer.parseInt(string[48]), //rs.getString("MAXSELECT_SC5"),
                        Integer.parseInt(string[49]), //rs.getString("MAXSELECT_SO5"),
                        Integer.parseInt(string[50]), //rs.getString("IRR_SUB5"),
                        Double.parseDouble(string[51]) / 10, //rs.getString("IRRALALLOTPNT6"),
                        Integer.parseInt(string[52]), //rs.getString("MAXSELECT_EN6"),
                        Integer.parseInt(string[53]), //rs.getString("MAXSELECT_MA6"),
                        Integer.parseInt(string[54]), //rs.getString("MAXSELECT_JA6"),
                        Integer.parseInt(string[55]), //rs.getString("MAXSELECT_SC6"),
                        Integer.parseInt(string[56]), //rs.getString("MAXSELECT_SO6"),
                        Integer.parseInt(string[57]), //rs.getString("IRR_SUB6"),
                        string[58], //rs.getString("SUBCOUNT"),
                        null, //rs.getString("STRING")
                        Integer.parseInt(string[59]), //rs.getInt("FIRSTANS_SC_FLG")
                        Integer.parseInt(string[60]), //rs.getInt("FIRSTANS_SO_FLG")
                        Integer.parseInt(string[61]), //rs.getInt("PRIORITYBRANCHCD")
                        string[62], //SAMECHOICE_NG_SC
                        string[63], //CENTERCVSSCORE_ENGPT
                        string[64], //ADDPTDIV_ENGPT
                        Integer.parseInt(string[65]), //ADDPT_ENGPT
                        string[66], //CENTERCVSSCORE_KOKUGO_DESC
                        string[67], //ADDPTDIV_KOKUGO_DESC
                        Integer.parseInt(string[68]), //ADDPT_KOKUGO_DESC
                        string[69] //BESTBRAN_SORT_CVSSCORE_USEDIV
                );

                container.addUnivMasterChoiceSchool(data, key, branchCd, entExamDiv);
            }
        }
    }

    @Override
    protected void deployUnivMasterChoiceSchoolPub(UnivMasterContainer container) throws Exception {
    }

    @Override
    protected void deployUnivMasterCourse(UnivMasterContainer container) throws Exception {

        int[] indice = { 4, //行事年度
                1, //学校区分
                4, //大学コード
                2, //学部コード
                4, //学科コード
                2, //模試区分
                1, //推薦用区分
                1, //来年用区分
                1, //入試試験区分
                3, //大学コード枝番
                1, //教科コード
                5, //必須配点
                1, //必須科目数
                1, //センター二次同一科目選択区分
        };

        int length = 0;

        for (int i = 0; i < indice.length; i++) {
            length += indice[i];
        }

        String[] string = new String[indice.length];

        BufferedReader ds = null;
        try {
            ds = new BufferedReader(new InputStreamReader(new FileInputStream(fileUnivmasterCourse), charset));

            int lineNum = 0;

            String line;
            while ((line = ds.readLine()) != null) {

                lineNum++;

                byte[] text = line.getBytes(charset);
                if (text.length != length) {
                    System.out.println(fileUnivmasterCourse.getName() + " line :" + lineNum + " : データの長さが不正です。" + text.length + "/" + length);
                    continue;
                }

                int index = 0;
                for (int i = 0; i < string.length; i++) {
                    string[i] = getString(text, index, index += indice[i], charset);
                }

                /* 読み込み対象チェック */
                String key = string[2] + string[3] + string[4];
                if (!contains(container, key)) {
                    continue;
                }

                /* 学校区分が「5」のみ対象 */
                if (!"5".equals(string[1])) {
                    continue;
                }

                /* 推薦用区分が「0」のみ対象 */
                if (!"0".equals(string[6])) {
                    continue;
                }

                /* 来年用区分が「0」または「9」のみ対象 */
                if (!("0".equals(string[7]) || "9".equals(string[7]))) {
                    continue;
                }

                /* 入試試験区分が「1」または「2」または「3」のみ対象 */
                String entExamDiv = string[8];
                if (!("1".equals(entExamDiv) || "2".equals(entExamDiv) || "3".equals(entExamDiv))) {
                    continue;
                }

                UnivMasterCourse data = new UnivMasterCourse( //
                        Double.parseDouble(string[11]) / 10, //rs.getDouble("NECESALLOTPNT"),
                        Integer.parseInt(string[12]), //rs.getInt("NECESSUBCOUNT"),
                        Integer.parseInt(string[13])); //rs.getInt("CEN_SECSUBCHOICEDIV"));

                container.addUnivMasterCource(data, key, string[9], entExamDiv, string[10]);
            }
        } finally {
            if (ds != null) {
                ds.close();
            }
        }
    }

    @Override
    protected void deployUnivMasterCoursePub(UnivMasterContainer container) throws Exception {
    }

    @Override
    protected void deployUnivMasterSelectGpCourse(UnivMasterContainer container) throws Exception {

        int[] indice = { 4, //行事年度
                1, //学校区分
                4, //大学コード
                2, //学部コード
                4, //学科コード
                2, //模試区分
                1, //推薦用区分
                1, //来年用区分
                1, //入試試験区分
                3, //大学コード枝番
                1, //選択グループ番号
                1, //教科コード
                1, //選択グループ教科別選択科目数
        };

        int length = 0;

        for (int i = 0; i < indice.length; i++) {
            length += indice[i];
        }

        String[] string = new String[indice.length];

        BufferedReader ds = null;
        try {
            ds = new BufferedReader(new InputStreamReader(new FileInputStream(fileUnivmasterSelectgpcourse), charset));

            int lineNum = 0;

            String line;
            while ((line = ds.readLine()) != null) {

                lineNum++;

                byte[] text = line.getBytes(charset);
                if (text.length != length) {
                    System.out.println(fileUnivmasterSelectgpcourse.getName() + " line :" + lineNum + " : データの長さが不正です。" + text.length + "/" + length);
                    continue;
                }

                int index = 0;
                for (int i = 0; i < string.length; i++) {
                    string[i] = getString(text, index, index += indice[i], charset);
                }

                /* 読み込み対象チェック */
                String key = string[2] + string[3] + string[4];
                if (!contains(container, key)) {
                    continue;
                }

                /* 学校区分が「5」のみ対象 */
                if (!"5".equals(string[1])) {
                    continue;
                }

                /* 推薦用区分が「0」のみ対象 */
                if (!"0".equals(string[6])) {
                    continue;
                }

                /* 来年用区分が「0」または「9」のみ対象 */
                if (!("0".equals(string[7]) || "9".equals(string[7]))) {
                    continue;
                }

                /* 入試試験区分が「1」または「2」または「3」のみ対象 */
                String entExamDiv = string[8];
                if (!("1".equals(entExamDiv) || "2".equals(entExamDiv) || "3".equals(entExamDiv))) {
                    continue;
                }

                UnivMasterSelectGpCourse data = new UnivMasterSelectGpCourse(Integer.parseInt(string[12]));

                container.addUnivMasterSelectGpCourse(data, key, string[9], entExamDiv, string[10], string[11]);
            }
        } finally {
            if (ds != null) {
                ds.close();
            }
        }
    }

    @Override
    protected void deployUnivMasterSelectGpCoursePub(UnivMasterContainer container) throws Exception {
    }

    @Override
    protected void deployUnivMasterSelectGroup(UnivMasterContainer container) throws Exception {

        int[] indice = { 4, //行事年度
                1, //学校区分
                4, //大学コード
                2, //学部コード
                4, //学科コード
                2, //模試区分
                1, //推薦用区分
                1, //来年用区分
                1, //入試試験区分
                3, //大学コード枝番
                1, //選択グループ番号
                1, //選択グループ科目数
                5, //選択グループ配点
        };

        int length = 0;

        for (int i = 0; i < indice.length; i++) {
            length += indice[i];
        }

        String[] string = new String[indice.length];

        BufferedReader ds = null;
        try {
            ds = new BufferedReader(new InputStreamReader(new FileInputStream(fileUnivmasterSelectgroup), charset));

            int lineNum = 0;

            String line;
            while ((line = ds.readLine()) != null) {

                lineNum++;

                byte[] text = line.getBytes(charset);
                if (text.length != length) {
                    System.out.println(fileUnivmasterSelectgroup.getName() + " line :" + lineNum + " : データの長さが不正です。" + text.length + "/" + length);
                    continue;
                }

                int index = 0;
                for (int i = 0; i < string.length; i++) {
                    string[i] = getString(text, index, index += indice[i], charset);
                }

                /* 読み込み対象チェック */
                String key = string[2] + string[3] + string[4];
                if (!contains(container, key)) {
                    continue;
                }

                /* 学校区分が「5」のみ対象 */
                if (!"5".equals(string[1])) {
                    continue;
                }

                /* 推薦用区分が「0」のみ対象 */
                if (!"0".equals(string[6])) {
                    continue;
                }

                /* 来年用区分が「0」または「9」のみ対象 */
                if (!("0".equals(string[7]) || "9".equals(string[7]))) {
                    continue;
                }

                /* 入試試験区分が「1」または「2」または「3」のみ対象 */
                String entExamDiv = string[8];
                if (!("1".equals(entExamDiv) || "2".equals(entExamDiv) || "3".equals(entExamDiv))) {
                    continue;
                }

                UnivMasterSelectGroup data = new UnivMasterSelectGroup( //
                        Integer.parseInt(string[11]), //rs.getInt("SELECTGSUBCOUNT"),
                        Double.parseDouble(string[12]) / 10); //rs.getDouble("SELECTGALLOTPNT"));

                container.addUnivMasterSelectGroup(data, key, string[9], entExamDiv, string[10]);
            }
        } finally {
            if (ds != null) {
                ds.close();
            }
        }
    }

    @Override
    protected void deployUnivMasterSelectGroupPub(UnivMasterContainer container) throws Exception {
    }

    @Override
    protected void deployUnivMasterSubject(UnivMasterContainer container) throws Exception {

        int[] indice = { 4, //行事年度
                1, //学校区分
                4, //大学コード
                2, //学部コード
                4, //学科コード
                2, //模試区分
                1, //推薦用区分
                1, //来年用区分
                1, //入試試験区分
                3, //大学コード枝番
                4, //受験科目コード
                1, //教科コード
                2, //科目ビット
                5, //科目配点
                2, //科目範囲
                1, //同時選択不可区分
        };

        int length = 0;

        for (int i = 0; i < indice.length; i++) {
            length += indice[i];
        }

        String[] string = new String[indice.length];

        BufferedReader ds = null;
        try {
            ds = new BufferedReader(new InputStreamReader(new FileInputStream(fileUnivmasterSubject), charset));

            int lineNum = 0;

            String line;
            while ((line = ds.readLine()) != null) {

                lineNum++;

                byte[] text = line.getBytes(charset);
                if (text.length != length) {
                    System.out.println(fileUnivmasterSubject.getName() + " line :" + lineNum + " : データの長さが不正です。" + text.length + "/" + length);
                    continue;
                }

                int index = 0;
                for (int i = 0; i < string.length; i++) {
                    string[i] = getString(text, index, index += indice[i], charset);
                }

                /* 読み込み対象チェック */
                String key = string[2] + string[3] + string[4];
                if (!contains(container, key)) {
                    continue;
                }

                /* 学校区分が「5」のみ対象 */
                if (!"5".equals(string[1])) {
                    continue;
                }

                /* 推薦用区分が「0」のみ対象 */
                if (!"0".equals(string[6])) {
                    continue;
                }

                /* 来年用区分が「0」または「9」のみ対象 */
                if (!("0".equals(string[7]) || "9".equals(string[7]))) {
                    continue;
                }

                /* 入試試験区分が「1」または「2」または「3」のみ対象 */
                String entExamDiv = string[8];
                if (!("1".equals(entExamDiv) || "2".equals(entExamDiv) || "3".equals(entExamDiv))) {
                    continue;
                }

                UnivMasterSubject data = new UnivMasterSubject( //
                        string[12], //rs.getString("SUBBIT"),
                        Double.parseDouble(string[13]) / 10, //rs.getDouble("SUBPNT"),
                        string[14], //rs.getString("SUBAREA"));
                        string[15]); //rs.getString("CONCURRENT_NG")

                container.addUnivMasterSubject(data, key, string[9], entExamDiv, string[10]);
            }
        } finally {
            if (ds != null) {
                ds.close();
            }
        }
    }

    @Override
    protected void deployUnivMasterSubjectPub(UnivMasterContainer container) throws Exception {
    }

    /**
     * 倍率データ変換
     */
    private void setPreMag(UnivDataFile u, String preMag) {

        if (preMag == null) {
            preMag = "";
        }

        u.setPreMagnification(preMag);
    }

    /**
     * インデックス値から文字列を算出
     */
    private String getString(byte[] data, int start, int length, String charset) throws UnsupportedEncodingException {

        String str = new String(data, start, length - start, charset).trim();

        char[] c = str.toCharArray();

        for (int i = c.length; i > 0; i--) {
            if (c[i - 1] != '　') {
                return str.substring(0, i);
            }
        }

        return "";
    }

    /**
     * 制限対象に含まれているかどうかを調べます。
     *
     * @param container {@link UnivMasterContainer}
     * @param uniqueKey 大学コードまたは10桁コード
     * @return 制限対象に含まれているならtrue
     */
    private boolean contains(UnivMasterContainer container, String uniqueKey) {
        if (container.getRestriction().isEmpty()) {
            return true;
        } else {
            return container.getRestriction().contains(uniqueKey) || container.getRestriction().contains(uniqueKey.substring(0, 4));
        }
    }

    /**
     * {@inheritDoc}
     */
    public String getExamDiv() {
        /* センター・リサーチの模試区分で固定する */
        return "07";
    }

    @Override
    protected void loadUnivInfoBasic(UnivInfoContainer container) throws Exception {

        if (fileKamoku1 == null) {
            return;
        }

        try (Reader in = new InputStreamReader(new FileInputStream(fileKamoku1), charset)) {

            for (CSVRecord rec : toCSVParser(in)) {

                if (rec.size() != 65) {
                    System.out.println(fileKamoku1.getName() + " line :" + rec.getRecordNumber() + " : レコードサイズサイズが不正です。" + rec.size());
                    continue;
                }

                /* 結合キー */
                String combiKey = rec.get(0);
                if (combiKey.length() != 9) {
                    /* 結合キーが9桁でない場合(ヘッダ文字列）は飛ばす */
                    continue;
                }

                container.getUnivInfoBasicList().add(new UnivInfoBasicBean(combiKey, rec.get(30), rec.get(45)));
            }
        }
    }

    @Override
    protected void loadUnivInfoCenter(UnivInfoContainer container) throws Exception {

        if (fileKamoku2 == null) {
            return;
        }

        Map<String, UnivInfoKamokuBean> kamoku = loadUnivInfoKamoku();
        try (Reader in = new InputStreamReader(new FileInputStream(fileKamoku2), charset)) {

            for (CSVRecord rec : toCSVParser(in)) {

                if (rec.size() != 157) {
                    System.out.println(fileKamoku2.getName() + " line :" + rec.getRecordNumber() + " : レコードサイズサイズが不正です。" + rec.size());
                    continue;
                }

                /* 結合キー */
                String combiKey = rec.get(0);
                if (combiKey.length() != 9) {
                    /* 結合キーが9桁でない場合(ヘッダ文字列）は飛ばす */
                    continue;
                }

                UnivInfoCenterBean bean = new UnivInfoCenterBean();

                /* 共通情報を設定する */
                bean.setScTrust(rec.get(5));
                bean.setTotalSc(rec.get(6));

                /* 外国語の「その他科目選択区分」による調整処理(JNRZ0574_02.sqlを元に実装)
                String gaiShoPt = rec.get(12);
                String gaiShoSel = rec.get(13);
                String gaiDaiPt = rec.get(14);
                String gaiDaiSel = rec.get(15);
                if ("1".equals(rec.get(10))) {
                    if ("2".equals(gaiShoSel) && !"2".equals(gaiDaiSel)) {
                        gaiShoPt = "";
                        gaiShoSel = "";
                    } else if ("2".equals(gaiDaiSel)) {
                        gaiDaiPt = "";
                        gaiDaiSel = "";
                    }
                }*/
                bean.addGai(rec.get(25), rec.get(26), rec.get(27), rec.get(28), rec.get(29), COURSE_ALLOT.ENG);

                /* 数学の「その他科目選択区分」による調整処理(JNRZ0574_02.sqlを元に実装)
                String mathShoPt = rec.get(32);
                String mathShoSel = rec.get(33);
                String mathDaiPt = rec.get(34);
                String mathDaiSel = rec.get(35);
                if ("1".equals(rec.get(30))) {
                    if ("2".equals(mathShoSel) && !"2".equals(mathDaiSel)) {
                        mathShoPt = "";
                        mathShoSel = "";
                    } else if ("2".equals(mathDaiSel)) {
                        mathDaiPt = "";
                        mathDaiSel = "";
                    }
                }*/
                bean.addMath(rec.get(50), rec.get(55), rec.get(56), rec.get(57), rec.get(58), COURSE_ALLOT.MAT);

                bean.addKokugo(rec.get(72), rec.get(73), rec.get(74), rec.get(75), rec.get(76), COURSE_ALLOT.JAP);
                bean.addRika(rec.get(99), rec.get(100), rec.get(101), rec.get(102), rec.get(103), COURSE_ALLOT.SCI);
                bean.addSociety(rec.get(123), rec.get(124), rec.get(125), rec.get(126), rec.get(127), COURSE_ALLOT.SOC);
                if (kamoku.containsKey(combiKey)) {
                    bean.setSubName(kamoku.get(combiKey).getCenSubName());
                }
                /*bean.setSubName(rec.get(120));*/

                /* センタ固有情報を設定する */
                bean.setItem7(rec.get(144));
                bean.setRika1ansFlg(rec.get(105));
                bean.setSociety1ansFlg(rec.get(129));

                /* 英語認定試験配点を設定する */
                bean.addEngCert("認", rec.get(9), rec.get(30), rec.get(31), rec.get(32), rec.get(33), rec.get(34));
                /* 国語記述配点を設定する */
                bean.addJpnDesc(rec.get(70), rec.get(77), rec.get(78), rec.get(79), rec.get(80), rec.get(81));

                container.getUnivInfoCenterMap().put(combiKey, bean);
            }
        }
    }

    @Override
    protected void loadUnivInfoSecond(UnivInfoContainer container) throws Exception {

        if (fileKamoku3 == null) {
            return;
        }

        Map<String, UnivInfoKamokuBean> kamoku = loadUnivInfoKamoku();
        try (Reader in = new InputStreamReader(new FileInputStream(fileKamoku3), charset);) {

            for (CSVRecord rec : toCSVParser(in)) {

                if (rec.size() != 162) {
                    System.out.println(fileKamoku3.getName() + " line :" + rec.getRecordNumber() + " : レコードサイズサイズが不正です。" + rec.size());
                    continue;
                }

                /* 結合キー */
                String combiKey = rec.get(0);
                if (combiKey.length() != 9) {
                    /* 結合キーが9桁でない場合(ヘッダ文字列）は飛ばす */
                    continue;
                }

                UnivInfoSecondBean bean = new UnivInfoSecondBean();

                /* 共通情報を設定する */
                bean.setScTrust(rec.get(5));
                bean.setTotalSc(rec.get(6));
                bean.addGai(rec.get(20), rec.get(21), rec.get(22), rec.get(23), rec.get(24), COURSE_ALLOT.ENG);
                bean.addMath(rec.get(40), rec.get(41), rec.get(42), rec.get(43), rec.get(44), COURSE_ALLOT.MAT);
                bean.addKokugo(rec.get(54), rec.get(55), rec.get(56), rec.get(57), rec.get(58), COURSE_ALLOT.JAP);
                bean.addRika(rec.get(69), rec.get(70), rec.get(71), rec.get(72), rec.get(73), COURSE_ALLOT.SCI);
                bean.addSociety(rec.get(85), rec.get(86), rec.get(87), rec.get(88), rec.get(89), COURSE_ALLOT.SOC);
                /*bean.setSubName(rec.get(142));*/
                if (kamoku.containsKey(combiKey)) {
                    bean.setSubName(kamoku.get(combiKey).getSecSubName());
                }
                /* 英語認定試験配点を設定する */
                bean.addEngCert("英資", rec.get(91), rec.get(98), rec.get(99), rec.get(100), rec.get(101), rec.get(102));

                /* 二次固有情報を設定する */
                bean.addEssay(rec.get(105), rec.get(106), rec.get(107), rec.get(108));
                bean.addTotal(rec.get(109), rec.get(110), rec.get(111), rec.get(112));
                bean.addInterview(rec.get(113), rec.get(114), rec.get(115), rec.get(116));
                bean.addPlactical(rec.get(117), rec.get(118), rec.get(119), rec.get(120));
                bean.addBoki(rec.get(121), rec.get(122), rec.get(123), rec.get(124));
                bean.addInfo(rec.get(125), rec.get(126), rec.get(127), rec.get(128));
                bean.addResearch(rec.get(129), rec.get(130), rec.get(131), rec.get(132));
                bean.addHoka(rec.get(133), rec.get(134), rec.get(135), rec.get(136));
                bean.setHoka2TrustPt(rec.get(137));
                bean.setHoka2Pt(rec.get(138));

                container.getUnivInfoSecondMap().put(combiKey, bean);
            }
        }
    }

    private Map<String, UnivInfoKamokuBean> loadUnivInfoKamoku() throws Exception {

        Map<String, UnivInfoKamokuBean> result = new HashMap();
        if (fileKamoku4 == null) {
            return result;
        }

        try (Reader in = new InputStreamReader(new FileInputStream(fileKamoku4), charset);) {

            for (CSVRecord rec : toCSVParser(in)) {

                if (rec.size() != 40) {
                    System.out.println(fileKamoku3.getName() + " line :" + rec.getRecordNumber() + " : レコードサイズサイズが不正です。" + rec.size());
                    continue;
                }

                /* 結合キー */
                String combiKey = rec.get(1) + rec.get(2) + rec.get(3);
                if (combiKey.length() != 9) {
                    /* 結合キーが9桁でない場合(ヘッダ文字列）は飛ばす */
                    continue;
                }

                UnivInfoKamokuBean bean = new UnivInfoKamokuBean();

                /* 情報誌科目を設定する */
                bean.setJoinKey(combiKey);
                bean.setUnivCd(rec.get(0));
                bean.setCenSubName(rec.get(9));
                bean.setSecSubName(rec.get(12));

                result.put(combiKey, bean);
            }
        }
        return result;
    }

    @Override
    protected void deployUnivComment(UnivMasterContainer container) throws Exception {

        if (fileUnivComment == null) {
            return;
        }

        int[] indice = { 9, //模試コード
                4, //行事年度
                2, //模試区分
                10, //大学コード10桁
                1, //種別フラグ
                18, //大学名
                14, //学部
                16, //学科（募集個表名）
                10, //短縮コメント
                136, //コメント
        };

        int length = 0;

        for (int i = 0; i < indice.length; i++) {
            length += indice[i];
        }

        String[] string = new String[indice.length];

        BufferedReader ds = null;
        try {
            ds = new BufferedReader(new InputStreamReader(new FileInputStream(fileUnivComment), charset));

            int lineNum = 0;

            String line;
            while ((line = ds.readLine()) != null) {

                lineNum++;

                byte[] text = line.getBytes(charset);
                if (text.length != length) {
                    System.out.println(fileUnivComment.getName() + " line :" + lineNum + " : データの長さが不正です。" + text.length + "/" + length);
                    continue;
                }

                int index = 0;
                for (int i = 0; i < string.length; i++) {
                    string[i] = getString(text, index, index += indice[i], charset);
                }

                /* 読み込み対象チェック */
                String key = string[3];
                if (!contains(container, key)) {
                    continue;
                }

                /* 模試区分が「07」のみ対象 */
                if (!"07".equals(string[2])) {
                    continue;
                }

                container.getUnivCommentMap().put(string[3], new UnivComment(string[4], string[9]));
            }
        } finally {
            if (ds != null) {
                ds.close();
            }
        }
    }

    @Override
    protected List<CefrBean> deployCefrInfo() throws Exception {
        return null;
    }

    @Override
    protected List<EngPtBean> deployEngCert() throws Exception {
        return null;
    }

    @Override
    protected List<EngPtDetailBean> deployEngCertDetail() throws Exception {
        return null;
    }

    @Override
    protected List<CefrConvScoreBean> deployCefrConvScore() throws Exception {
        return null;
    }

    @Override
    protected List<JpnDescTotalEvaluationBean> deployJpnDescTotalEval() throws Exception {
        List<JpnDescTotalEvaluationBean> result = new ArrayList();

        /*if (fileDescCompRating == null) {
            return result;
        }
        
        int[] indice = { 4, //行事年度
                2, //模試区分
                4, //科目コード
                2, //問１評価
                2, //問２評価
                2, //問３評価
                1 //総合評価
        };
        
        int length = 0;
        
        for (int i = 0; i < indice.length; i++) {
            length += indice[i];
        }
        
        String[] string = new String[indice.length];
        
        try (BufferedReader ds = new BufferedReader(new InputStreamReader(new FileInputStream(fileDescCompRating), charset));) {
        
            int lineNum = 0;
        
            String line;
            while ((line = ds.readLine()) != null) {
        
                lineNum++;
        
                byte[] text = line.getBytes(charset);
                if (text.length != length) {
                    System.out.println(fileDescCompRating.getName() + " line :" + lineNum + " : データの長さが不正です。" + text.length + "/" + length);
                    continue;
                }
        
                int index = 0;
                for (int i = 0; i < string.length; i++) {
                    string[i] = getString(text, index, index += indice[i], charset);
                }
                 模試コードが「38」のみ対象
                if (!"38".equals(string[1])) {
                    continue;
                }
        
                JpnDescTotalEvaluationBean data = new JpnDescTotalEvaluationBean();
                data.setSubCd(string[2]);
                data.setQ1Evaluation(string[3]);
                data.setQ2Evaluation(string[4]);
                data.setQ3Evaluation(string[5]);
                data.setTotalEvaluation(string[6]);
        
                result.add(data);
            }
        }*/
        return result;
    }

    @Override
    protected List<EngSikakuAppInfo> deployEngPtAppReqList() throws Exception {
        return null;
    }

    @Override
    protected List<EngSikakuScoreInfo> deployEngPtScoreConvList() throws Exception {
        return null;
    }

    @Override
    protected List<JpnKijutuScoreInfo> deployKokugoDescScoreConvList() throws Exception {
        return null;
    }

    @Override
    protected Map<String, ExamSubjectBean> deployExamSubject() throws Exception {
        Map<String, ExamSubjectBean> result = new HashMap();

        if (fileExamSubject == null) {
            return result;
        }

        try (Reader in = new InputStreamReader(new FileInputStream(fileExamSubject), charset)) {

            for (CSVRecord rec : toCSVParser(in)) {

                if (rec.size() != 9) {
                    continue;
                }

                // 対象年度が異なる場合、読み飛ばす
                if (!this.eventYear.equals(rec.get(0))) {
                    continue;
                }

                // リサーチ以外の模試コードの場合、読み飛ばす
                String examCd = StringUtils.leftPad(rec.get(1), 2, "0");
                String examTypeCd = "38".equals(examCd) ? "01" : "02";
                if (!"38".equals(examCd) && !"07".equals(examCd)) {
                    continue;
                }

                if (rec.get(2).compareTo("7000") > 0) {
                    continue;
                }
                ExamSubjectBean data = new ExamSubjectBean();
                data.setSubCd(rec.get(2));
                data.setSubName(rec.get(3));
                data.setSubAllotPnt(Integer.valueOf(rec.get(5)));
                data.setDispSequence(rec.get(6));

                if (!result.containsKey(examTypeCd + data.getSubCd())) {
                    result.put(examTypeCd + data.getSubCd(), data);
                }
            }
        }
        return result;
    }

}

package jp.fujitsu.keinet.mst.file.factory;

import java.util.HashMap;
import java.util.Map;

import jp.co.fj.kawaijuku.judgement.factory.UnivMasterContainer;

/**
 *
 * ファイル用の {@link UnivMasterContainer} です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("rawtypes")
public class UnivMasterFileContainer extends UnivMasterContainer {

    /** 前年度倍率マップ */
    private HashMap preMagMap;

    /** 入試状況マップ */
    private HashMap circumstanceMap;

    /** 足切り配点マップ */
    private Map pickedPointFullMap;

    /** 大学コード表名マップ */
    private Map univNameCodeMap;

    /** 大学カナ名マップ */
    private Map univNameKanaMap;

    /** 大学その他名称マップ */
    private Map univNameOtherMap;

    /** 県名マップ */
    private Map prefNameMap;

    /** カナ付50音順番号マップ */
    private Map kanaNumMap;

    /**
     * {@inheritDoc}
     */
    protected void initialize() {

        super.initialize();

        if (getRestriction().isEmpty()) {
            preMagMap = new HashMap(RECNUM_BASIC);
            circumstanceMap = new HashMap(RECNUM_BASIC);
            pickedPointFullMap = new HashMap();
            univNameCodeMap = new HashMap(RECNUM_BASIC / 10);
            univNameKanaMap = new HashMap(RECNUM_BASIC / 10);
            univNameOtherMap = new HashMap();
            kanaNumMap = new HashMap(RECNUM_BASIC / 10);
            prefNameMap = new HashMap();
        } else {
            preMagMap = new HashMap();
            circumstanceMap = new HashMap();
            pickedPointFullMap = new HashMap();
            univNameCodeMap = new HashMap();
            univNameKanaMap = new HashMap();
            univNameOtherMap = new HashMap();
            kanaNumMap = new HashMap();
            prefNameMap = new HashMap();
        }
    }

    /**
     * 前年度倍率マップを返します。
     *
     * @return 前年度倍率マップ
     */
    public HashMap getPreMagMap() {
        return preMagMap;
    }

    /**
     * 入試状況マップを返します。
     *
     * @return 入試状況マップ
     */
    public HashMap getCircumstanceMap() {
        return circumstanceMap;
    }

    /**
     * 足切り配点マップを返します。
     *
     * @return 足切り配点マップ
     */
    public Map getPickedPointFullMap() {
        return pickedPointFullMap;
    }

    /**
     * 大学コード表名マップを返します。
     *
     * @return 大学コード表名マップ
     */
    public Map getUnivNameCodeMap() {
        return univNameCodeMap;
    }

    /**
     * 大学カナ名マップを返します。
     *
     * @return 大学カナ名マップ
     */
    public Map getUnivNameKanaMap() {
        return univNameKanaMap;
    }

    /**
     * 大学その他名称マップを返します。
     *
     * @return 大学その他名称マップ
     */
    public Map getUnivNameOtherMap() {
        return univNameOtherMap;
    }

    /**
     * 県名マップを返します。
     *
     * @return 県名マップ
     */
    public Map getPrefNameMap() {
        return prefNameMap;
    }

    /**
     * カナ付50音順番号マップを返します。
     *
     * @return カナ付50音順番号マップ
     */
    public Map getKanaNumMap() {
        return kanaNumMap;
    }

}

package jp.ac.kawaijuku.keinet.dto;

import lombok.Data;

@Data
public class CKAI0050BRqtDto {

    private CKAI0050BRqt CKAI0050BRqt;
}

@Data
final class CKAI0050BRqt {
    private CmnItmRqt CmnItmRqt;
    private String AgrMngKey;
    private RgstUpdItm RgstUpdItm;
    private UpdObj UpdObj;
}
@Data
final class CmnItmRqt {
    private String ConnOriSys;
    private String ReqId;
    private String ReqFnctnId;
    private String PrcRqstDtm;
    private String APIPrcTyp;
}
@Data
final class RgstUpdItm {
    private String CmnAgrId;
    private String PSW;
    private String NewPSW;
    private String KnaFir;
    private String KnaNm;
    private String ChncrFir;
    private String ChncrNm;
    private String Sex;
    private String Brdy;
    private String Pstcd;
    private String Adr1;
    private String Adr2;
    private String HomTelNo;
    private String CryNo;
    private String Blng;
    private String SclCd;
    private String Grd;
    private String Cls;
    private String ClsNo;
    private String GrdtYr;
    private String MlAdrs;
    private String GrdnChncrFir;
    private String GrdnChncrNm;
    private String DMSndTyp;
}
@Data
final class UpdObj {
    private String CmnAgrIdUpdFlg;
    private String KnaFirUpdFlg;
    private String KnaNmUpdFlg;
    private String ChncrFirUpdFlg;
    private String ChncrNmUpdFlg;
    private String SexUpdFlg;
    private String BrdyUpdFlg;
    private String PstcdUpdFlg;
    private String Adr1UpdFlg;
    private String Adr2UpdFlg;
    private String HomTelNoUpdFlg;
    private String CryNoUpdFlg;
    private String BlngUpdFlg;
    private String SclCdUpdFlg;
    private String GrdUpdFlg;
    private String ClsUpdFlg;
    private String ClsNoUpdFlg;
    private String GrdtYrUpdFlg;
    private String MlAdrsUpdFlg;
    private String GrdnChncrFirUpdFlg;
    private String GrdnChncrNmUpdFlg;
    private String DMSndTypUpdFlg;
}
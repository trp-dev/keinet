package jp.ac.kawaijuku.keinet;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import jp.ac.kawaijuku.keinet.dto.AccountSectionDto;
import jp.ac.kawaijuku.keinet.dto.CKAI0050BRqtDto;
import jp.ac.kawaijuku.keinet.dto.CKAI0050BRspDto;
import jp.ac.kawaijuku.keinet.dto.StaffAuthDto;
import jp.ac.kawaijuku.keinet.dto.ValidateScreenDto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class Controller {

    /** ロガー */
    private final static Log logger = LogFactory.getLog(Controller.class);

    @RequestMapping(path = "/StaffAuthentication", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public StaffAuthDto method1() {
        StaffAuthDto res = new StaffAuthDto();
        res.setAuthResult("OK");
        res.setSessionID("123");
        res.setExpiration("000000000000");
        return res;
    }

    @RequestMapping(path = "/validateCanUsingScreen", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public ValidateScreenDto method2() {
        ValidateScreenDto res = new ValidateScreenDto();
        res.setAuthResult("OK");
        return res;
    }

    @RequestMapping(path = "/getAccountingSectionList", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public AccountSectionDto method3() {
        AccountSectionDto res = new AccountSectionDto();
        res.getAccountingSectionCode().add("027");
        res.getAccountingSectionCode().add("029");
        res.getAccountingSectionCode().add("031");
        res.getAccountingSectionCode().add("032");
        res.getAccountingSectionCode().add("033");
        return res;
    }

    /**
     * 共通認証IDスタブAPI
     * @param request
     * @return
     */
    @RequestMapping(path = "/ck/rest/CKAI0050B", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CKAI0050BRspDto method4(CKAI0050BRqtDto request) {
        logger.info("method=[/ck/rest/CKAI0050B] start");
        ObjectMapper mapper = new ObjectMapper();
        CKAI0050BRspDto res = new CKAI0050BRspDto();
        File file = new File(getClass().getClassLoader().getResource("response.json").getFile());
        try {
            Thread.sleep(20000);
            String json = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())), Charset.forName("UTF-8"));
            res = mapper.readValue(json, CKAI0050BRspDto.class);
        } catch (Exception e) {
            logger.error("method=[/ck/rest/CKAI0050B] error", e);
        } finally {
            logger.info("method=[/ck/rest/CKAI0050B] end");
        }
        return res;
    }

    /**
     * 共通認証IDスタブAPIの呼び出し
     * @return
     */
    @RequestMapping(path = "/callapi", method = RequestMethod.GET)
    public String method5() {
        String res = "OK";
        try {
            CKAI0050BRqtDto req = new CKAI0050BRqtDto();
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(req);
            sendRequest(json);
        } catch (Exception e) {
            res = "NG";
            logger.error(e);
        }
        return res;
    }

    /*
    * 認証IDAPIにリクエストをPOSTする
    *
    * @param json リクエスト用JSON文字列
    */
    public void sendRequest(String json) throws IOException {
        HttpURLConnection uc;
        boolean flg = false;
        try {
            // サーバとの接続が出来ない(connect timed out)
            //URL url = new URL("http://192.168.111.111:8081/stub/ck/rest/CKAI0050B");
            // ホスト名が解決できない(UnknownHost)
            //URL url = new URL("http://localhost1:8081/stub/ck/rest/CKAI0050B");
            // ポートでサービスが提供されていない(Connection refused)
            //URL url = new URL("http://localhost:18081/stub/ck/rest/CKAI0050B");
            // 正常
            URL url = new URL("http://localhost:8081/stub/ck/rest/CKAI0050B");
            uc = (HttpURLConnection) url.openConnection();
            uc.setRequestMethod("POST");
            uc.setUseCaches(false);
            uc.setDoOutput(true);
            uc.setConnectTimeout(5000);
            // 応答なし(Read timed out)
            uc.setReadTimeout(3000);
            uc.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            uc.connect();
            flg = true;
            try (OutputStreamWriter out = new OutputStreamWriter(new BufferedOutputStream(uc.getOutputStream()))) {
                out.write(json);
            }

            try (BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()))) {
                String line = in.readLine();
                StringBuilder body = new StringBuilder();
                while (line != null) {
                    body.append(line);
                    line = in.readLine();
                }
                uc.disconnect();
                resutPost(body.toString());
            }

            return;
        } catch (SocketTimeoutException e) {
            if (flg) {
                logger.error("共通認証ID API ERR API=CKAI0050BRqt 処理タイムアウト");
            } else {
                logger.error("共通認証ID API ERR API=CKAI0050BRqt 接続タイムアウト");
            }
            throw e;
        } catch (IOException e) {
            throw e;
        }
    }

    /**
    * 認証IDAPIにPOSTした結果を処理する
    *
    * @param json POST結果JSON文字列
    */
    private void resutPost(String json) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            CKAI0050BRspDto res = new CKAI0050BRspDto();

            res = mapper.readValue(json, CKAI0050BRspDto.class);
            if ("0".equals(res.getCkai0050BRsp().getCmnItmRsp().getApiPrcRslt())) {
                logger.info("共通認証ID API OK API=CKAI0050BRqt");
            } else {
                logger.error("共通認証ID API ERR API=CKAI0050BRqt");
            }
        } catch (Exception e) {
            logger.error(e);
            return;
        }
    }

}

package jp.ac.kawaijuku.keinet.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement(name="GetAccountingSectionList")
public class AccountSectionDto {

    /**
     * コンストラクタです。
     */
    public AccountSectionDto() {
        super();
        this.accountingSectionCode = new ArrayList<>();
    }

    private List<String> accountingSectionCode;
}

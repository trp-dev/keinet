package jp.ac.kawaijuku.keinet.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public final class MmbInfRgstUpdRqtDto {
    @JsonProperty("AgrMngKey")
    private String agrMngKey;
}
package jp.ac.kawaijuku.keinet.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CKAI0050BRspBodyDto {
        @JsonProperty("AgrMngKey")
        private String agrMngKey;
        @JsonProperty("CmnAgrIdDupList")
        private List<String> cmnAgrIdDupList;
        @JsonProperty("CmnItmRsp")
        private CmnItmRspDto cmnItmRsp;
        @JsonProperty("MmbInfRgstUpdRqt")
        private MmbInfRgstUpdRqtDto mmbInfRgstUpdRqt;
}

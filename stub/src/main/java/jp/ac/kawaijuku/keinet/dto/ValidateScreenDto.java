package jp.ac.kawaijuku.keinet.dto;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement(name="ValidateCanUsingScreen")
public class ValidateScreenDto {

    private String authResult;
}


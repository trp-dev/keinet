package jp.ac.kawaijuku.keinet.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CKAI0050BRspDto {

    @JsonProperty("CKAI0050BRsp")
    private CKAI0050BRspBodyDto ckai0050BRsp;
}


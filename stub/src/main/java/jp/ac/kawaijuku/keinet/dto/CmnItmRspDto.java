package jp.ac.kawaijuku.keinet.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CmnItmRspDto {
        @JsonProperty("APIPrcRslt")
        private String apiPrcRslt;
        @JsonProperty("APIPrcTyp")
        private String apiPrcTyp;
        @JsonProperty("AplMsg")
        private List<String> aplMsg;
        @JsonProperty("ConnOriSys")
        private String connOriSys;
        @JsonProperty("PrcEndDtm")
        private String prcEndDtm;
        @JsonProperty("PrcRqstDtm")
        private String prcRqstDtm;
        @JsonProperty("PrcStrDtm")
        private String prcStrDtm;
        @JsonProperty("ReqFnctnId")
        private String reqFnctnId;
        @JsonProperty("ReqId")
        private String reqId;
        @JsonProperty("ResFnctnId")
        private String resFnctnId;
        @JsonProperty("ResId")
        private String resId;
        @JsonProperty("SysErr")
        private List<String> sysErr;

}

package jp.ac.kawaijuku.keinet.dto;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement(name="staffAuthentication")
public class StaffAuthDto {

    private String authResult;
    private String sessionID;
    private String expiration;
}


#! /bin/csh 

###### 模試別成績データダウンロードファイル本番機アップスクリプト
###### 定義模試分のファイルをファイルサーバからコピー

set nowdate = `date +%Y%m%d`
set setexamfile = "/keinavi/download/freemenu/examup/setexamfile.txt"
set resullog = "/home/kei-navi/resul.log"
set outlogs = "/home/kei-navi/copy.log"

if ($#argv <= 0) then
	#公開日
	set opendate = (20061113)
	#年度
	set year = (2006)
	#模試コード
	set exam = (06)
else
	set year=`awk -F, '{print $1}' $setexamfile`
	set exam=`awk -F, '{print $2}' $setexamfile`
	if ($#year != $#exam) then
		echo "--- ERROR：定義ファイルのフォーマットに誤りがあります ---"
		echo "1" > $resullog
		exit 30
	endif
endif

set errsflag = 0
set count = 1
while ($count <= $#exam)

	if ($#argv <= 0) then
		echo "現在は $nowdate"
		echo "現在は $nowdate" >> $outlogs
		if ($nowdate != $opendate[$count]) then
			@ count ++
			continue
		endif
	endif

####### 模試別成績データダウンロード
	if (! -f /keinavi/download/freemenu/examup/$year[$count]/$exam[$count].tar.Z) then
		echo "--- ERROR：年度：$year[$count], 模試：$exam[$count]データが取得先に存在しません。---"
		set errsflag = 1
		@ count ++
		continue
	endif
	if (! -d /keinavi/freemenu/exam/$year[$count]) then
		mkdir /keinavi/freemenu/exam/$year[$count]
	endif
	cd /keinavi/freemenu/exam/$year[$count]/
	if (-d $exam[$count]) then
		echo " すでに 年度：$year[$count], 模試：$exam[$count]データが存在するので旧フォルダは削除します。"
		echo " すでに 年度：$year[$count], 模試：$exam[$count]データが存在するので旧フォルダは削除します。" >> $outlogs
		\rm -rf $exam[$count]
	endif 
	echo " コピー 年度：$year[$count], 模試：$exam[$count]"
	echo " $nowdate  コピー 年度：$year[$count], 模試：$exam[$count]" >> $outlogs
#	cp -rp /keinavi/download/freemenu/examup/$year[$count]/$exam[$count] ./
	/home/kei-navi/ftp.sh /keinavi/freemenu/examup/$year[$count] /keinavi/freemenu/exam/$year[$count] $exam[$count].tar.Z 10.1.4.195
	#-- 転送ファイル解凍
	zcat $exam[$count].tar.Z | tar xf -
	\rm -rf $exam[$count].tar.Z

	#-- 2年前データの削除
	set old2year = `expr $year[$count] - 2`
	if (-d /keinavi/freemenu/exam/$old2year/$exam[$count]) then
		\rm -rf /keinavi/freemenu/exam/$old2year/$exam[$count]

		echo " 過年度データ削除 年度：$old2year, 模試：$exam[$count]"
		echo " 過年度データ削除 年度：$old2year, 模試：$exam[$count]" >> $outlogs
	endif


####### 2020.01.29 共通テスト対応 ADD STA
####### 学力要素別成績データ
        if (! -f /keinavi/download/freemenu/abilityup/$year[$count]/$exam[$count].tar.Z) then
                echo "--- ERROR：年度：$year[$count], 模試：$exam[$count]データが取得先に存在しません。---"
                set errsflag = 1
                @ count ++
                continue
        endif
        if (! -d /keinavi/freemenu/ability/$year[$count]) then
                mkdir /keinavi/freemenu/ability/$year[$count]
        endif
        cd /keinavi/freemenu/ability/$year[$count]/
        if (-d $exam[$count]) then
                echo " すでに 年度：$year[$count], 模試：$exam[$count]データが存在するので旧フォルダは削除します。"
                echo " すでに 年度：$year[$count], 模試：$exam[$count]データが存在するので旧フォルダは削除します。" >> $outlogs
                \rm -rf $exam[$count]
        endif
        echo " コピー 年度：$year[$count], 模試：$exam[$count]"
        echo " $nowdate  コピー 年度：$year[$count], 模試：$exam[$count]" >> $outlogs
        /home/kei-navi/ftp.sh /keinavi/freemenu/abilityup/$year[$count] /keinavi/freemenu/ability/$year[$count] $exam[$count].tar.Z 10.1.4.195
        #-- 転送ファイル解凍
        zcat $exam[$count].tar.Z | tar xf -
        \rm -rf $exam[$count].tar.Z

        #-- 2年前データの削除
        set old2year = `expr $year[$count] - 2`
        if (-d /keinavi/freemenu/ability/$old2year/$exam[$count]) then
                \rm -rf /keinavi/freemenu/ability/$old2year/$exam[$count]

                echo " 過年度データ削除 年度：$old2year, 模試：$exam[$count]"
                echo " 過年度データ削除 年度：$old2year, 模試：$exam[$count]" >> $outlogs
        endif
####### 2020.01.29 共通テスト対応 ADD END


	##-- 受験学力測定用データ展開
	if (($exam[$count] == 37)||($exam[$count] == 69)||($exam[$count] == 79)||($exam[$count] == 87)||($exam[$count] == 88)) then
		set num = 1
		set nummax = 2
		while ($num <= $nummax) 
			if (-f /keinavi/download/freemenu/examup/$year[$count]/$exam[$count]_$num.tar.Z) then
				cd /keinavi/freemenu/exam/$year[$count]
				if (-d $exam[$count]_$num) then
					echo " すでに 年度：$year[$count], 模試：$exam[$count]_${num}データが存在するので旧フォルダは削除します。"
					echo " すでに 年度：$year[$count], 模試：$exam[$count]_${num}データが存在するので旧フォルダは削除します。" >> $outlogs
					\rm -rf $exam[$count]_$num
				endif 
				echo " コピー 年度：$year[$count], 模試：$exam[$count]_$num"
				echo " コピー 年度：$year[$count], 模試：$exam[$count]_$num" >> $outlogs
				/home/kei-navi/ftp.sh /keinavi/freemenu/examup/$year[$count] /keinavi/freemenu/exam/$year[$count] $exam[$count]_$num.tar.Z 10.1.4.195

				zcat $exam[$count]_$num.tar.Z | tar xf -
				\rm -rf $exam[$count]_$num.tar.Z
	
				#-- 2年前データの削除
				if (-d /keinavi/freemenu/exam/$old2year/$exam[$count]_$num) then
					\rm -rf /keinavi/freemenu/exam/$old2year/$exam[$count]_$num
			
					echo " 過年度データ削除 年度：$old2year, 模試：$exam[$count]_$num"
					echo " 過年度データ削除 年度：$old2year, 模試：$exam[$count]_$num" >> $outlogs
				endif
			endif
			@ num ++
		end
	endif


	@ count ++
end
if ($errsflag == 1) then
	echo "30" > $resullog
	chmod 666 $resullog
	exit 30
endif
echo "0" > $resullog
chmod 666 $resullog
exit 0

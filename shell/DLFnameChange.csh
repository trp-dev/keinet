#! /bin/csh 

## 高校コード置換によりDLファイル名（高校コード）の変更

set basic = "/keinavi/freemenu/exam"
set nen = "2007"
set pa = "${basic}/${nen}"
echo "対象年度：$pa"
set examlist = `ls $pa`
set count = 1
set schoollist = `cat old.txt`  #旧高校コードリスト
set changelist = `cat new.txt`  #新高校コードリスト

while ($count <= $#examlist)
	set exam = $examlist[$count]

	echo "---- 確認模試： $exam ----"

	set bkdir = "bkDLfiles/${nen}/${exam}"
	mkdir -p $bkdir

	set count2 = 1
	while ($count2 <= $#schoollist)
		set resetcount = 0
		set schoolcd = `echo $schoollist[$count2] | tr -d '\r\n'`

		if (-f "${pa}/${exam}/$schoolcd.csv") then
			echo "**** ファイル確認：$schoolcd *****"

			set schoolcd2 = `echo $changelist[$count2] | tr -d '\r\n'`
			if (-f "${pa}/$exam/$schoolcd2.csv") then
				echo " ***** 変更後ファイル確認：$schoolcd2 *****"
				set resetcount = 1
			endif

			if ($resetcount == 0) then
				#-- ファイルバックアップ
				cp -p  ${pa}/${exam}/$schoolcd.csv $bkdir/

				echo " -- ファイル内の高校コード変更 : ${nen}/${exam}/$schoolcd.csv , 高校コード：$schoolcd → $schoolcd2"
				./DLchangeBundleCd.pl ${pa}/${exam}/$schoolcd.csv $bkdir $schoolcd $schoolcd2
				
				echo " -- ファイル名変更 ${bkdir}/$schoolcd-change.csv ${pa}/$exam/$schoolcd2.csv"
				cp -p ${bkdir}/$schoolcd-change.csv ${pa}/$exam/$schoolcd2.csv
	
				\rm -f  ${pa}/${exam}/$schoolcd.csv 
			endif
		endif

		@ count2 ++
	end
	echo " "

	rmdir $bkdir
	echo " "
	@ count ++
end


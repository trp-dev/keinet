@echo off

rem ジョブID（パラメータ）
set jobid=%1

rem ↓変更して下さい↓
set based=D:\keinet_project\
rem ↑変更して下さい↑

rem ########################
rem      ORACLE接続情報     
rem      要注意！！！！     
rem ########################
set orastr=KEINAVI/KEINAVI@XE

rem ディレクトリ
set jobdir=%based%workspace\shell\JOBNET\%jobid%
set indir=%based%data\keinavi\IN
set outdir=%based%data\keinavi\OUT

rem 引数チェック
if "%1"=="" ( 
    @echo エラー：引数が指定されていません > kekka.log
    exit
)

rem echo jobid=%jobid%
rem echo jobdir=%jobdir%
rem echo %orastr%
rem echo CONTROL=%jobdir%\%jobid%.ctl
rem echo DATA=%indir%\%jobid%.dat
rem echo BAD=%outdir%\%jobid%.bad
rem echo LOG=%outdir%\%jobid%.log
rem pause

if exist %jobdir%\%jobid%_01.sql (
	sqlplus %orastr% @%jobdir%\%jobid%_01.sql
)

sqlldr  %orastr%  CONTROL=%jobdir%\%jobid%.ctl  DATA=%indir%\%jobid%.dat  BAD=%outdir%\%jobid%.bad  LOG=%outdir%\%jobid%.log errors=0 READSIZE=50000000 BINDSIZE=50000000 ROWS=5000

if exist %jobdir%\%jobid%_02.sql (
	sqlplus %orastr% @%jobdir%\%jobid%_02.sql
)

exit /b 0

#!/usr/bin/perl

##高校コード置換作業におけるDLデータ内の高校コード変更
#第一パラメータ：対象ファイル名
#第二パラメータ：出力先
#第三パラメータ：旧高校コード
#第四パラメータ：新高校コード

$TAR_FILE = "@ARGV[0]";
$BUNDLECD_OLD = "@ARGV[2]";
$BUNDLECD_NEW = "@ARGV[3]";
$OUT_FILE = $ARGV[1] . "/" . $BUNDLECD_OLD . "-change.csv";

if(! open(IN, "$TAR_FILE")){
        print "FILE OPEN ERROR!\n";
        exit;
}
if(! open(OUT, ">$OUT_FILE") ){
	close(IN);
	exit;
}
$count = 0;
while(<IN>){

	$flg = 0;
        chomp($_);

	if ($count == 0) {
		print OUT $_ . "\n";
#		print "line one out\n";
		$count ++;
		next;
	}

	$outSTR = $_;
        $cd  = substr($_,  0, 15);
	$str1   = substr($_,  15, 5);
        $substr = substr($_,  20, );

	if ($str1 == $BUNDLECD_OLD) {
		$outSTR = $cd .
			$BUNDLECD_NEW .
			$substr . "\n";
	} else {
		print "-- no match code --\n"
	}
	
	print OUT $outSTR;	


	$count ++;
}
close(OUT);
close(IN);

exit;

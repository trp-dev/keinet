------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 設問別成績（クラス）
--  テーブル名      : QRECORD_C
--  データファイル名: QRECORD_C
--  機能            : 設問別成績（クラス）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
INSERT PRESERVE BLANKS
INTO TABLE QRECORD_C_TMP
(  
   EXAMYEAR        POSITION(01:04)   CHAR,
   EXAMCD          POSITION(05:06)   CHAR,
   BUNDLECD        POSITION(07:11)   CHAR,
   GRADE           POSITION(12:13)   INTEGER EXTERNAL,
   CLASS           POSITION(14:15)   CHAR,
   SUBCD           POSITION(16:19)   CHAR,
   QUESTION_NO     POSITION(20:21)   INTEGER EXTERNAL,
   AVGPNT          POSITION(22:26)   INTEGER EXTERNAL,
   NUMBERS         POSITION(27:34)   INTEGER EXTERNAL,
   AVGSCORERATE    POSITION(35:38)   INTEGER EXTERNAL
)


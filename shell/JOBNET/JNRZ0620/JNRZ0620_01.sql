-- ************************************************
-- *機 能 名 : 学校マスタ
-- *機 能 ID : JNRZ0620_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE SCHOOL_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE SCHOOL_TMP  (
       BUNDLECD               CHAR(5)              NOT NULL,    /*      一括コード          */ 
       BUNDLENAME             VARCHAR2(28)                 ,    /*      一括名              */ 
       BUNDLENAME_KANA        VARCHAR2(20)                 ,    /*      一括名カナ          */ 
       NPPDIV                 CHAR(2)                      ,    /*      立区分              */ 
       RANK                   CHAR(1)                      ,    /*      ランク              */ 
       FACULTYCD              CHAR(2)                      ,    /*      県コード            */ 
       TEL                    CHAR(13)                     ,    /*      代表ＴＥＬ          */ 
       PARENTSCHOOLCD         CHAR(5)                      ,    /*      親学校コード        */ 
       STOPDATE               CHAR(8)                      ,    /*      停止日              */ 
       NEWGETDATE             CHAR(8)                      ,    /*      新規取得日          */ 
       RENEWALDATE1           CHAR(8)                      ,    /*      更新日1             */ 
       RENEWALDATE2           CHAR(8)                      ,    /*      更新日2             */ 
       RENEWALDATE3           CHAR(8)                      ,    /*      更新日3             */ 
       CONSTRAINT PK_SCHOOL_TMP PRIMARY KEY (
                 BUNDLECD                                 /*  一括コード    */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

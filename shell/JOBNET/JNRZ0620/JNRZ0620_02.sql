-- *********************************************************
-- *機 能 名 : 学校マスタ
-- *機 能 ID : JNRZ0620_02.sql
-- *作 成 日 : 2004/08/31
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 学校マスタの更新処理
DECLARE
       wk_stopdate      CHAR(8);        -- 既存レコードの停止日退避用
       wk_newgetdate    CHAR(8);        -- 既存レコードの新規取得日退避用
       wk_renewaldate1  CHAR(8);        -- 既存レコードの更新日1退避用
       wk_renewaldate2  CHAR(8);        -- 既存レコードの更新日2退避用
       wk_renewaldate3  CHAR(8);        -- 既存レコードの更新日3退避用
       wk_oldstop       VARCHAR2(8);    -- 既存レコードの停止日確認用
       wk_oldnewget     VARCHAR2(8);    -- 既存レコードの新規取得日確認用
       wk_newstop       VARCHAR2(8);    -- 既存レコードの停止日確認用
       wk_newnewget     VARCHAR2(8);    -- 既存レコードの新規取得日確認用
       wk_rowid         ROWID;          -- 既存レコードのROWID退避用
BEGIN
    FOR r_newcur IN (SELECT BUNDLECD,
                            BUNDLENAME,
                            BUNDLENAME_KANA,
                            NPPDIV,
                            RANK,
                            FACULTYCD,
                            TEL,
                            PARENTSCHOOLCD,
                            STOPDATE,
                            NEWGETDATE
                     FROM   SCHOOL_TMP
                     WHERE  (BUNDLECD NOT LIKE '9____') AND 
                            (BUNDLECD NOT LIKE '8____') AND 
                            (BUNDLECD NOT LIKE '__999'))
    LOOP
        BEGIN
            -- 既存レコードを検索
            SELECT ROWID,   STOPDATE,   NEWGETDATE,   RENEWALDATE1,   RENEWALDATE2,   RENEWALDATE3
            INTO   wk_rowid,wk_stopdate,wk_newgetdate,wk_renewaldate1,wk_renewaldate2,wk_renewaldate3
            FROM   SCHOOL
            WHERE  BUNDLECD = r_newcur.BUNDLECD;

            -- 停止日、新規取得日チェック
            -- (どちらかが異なれば更新日をクリア)
            wk_oldstop   := wk_stopdate;
            wk_oldnewget := wk_newgetdate;
            wk_newstop   := r_newcur.STOPDATE;
            wk_newnewget := r_newcur.NEWGETDATE;
            IF wk_oldstop IS NULL THEN
                wk_oldstop := '';
            END IF;
            IF wk_oldnewget IS NULL THEN
                wk_oldnewget := '';
            END IF;
            IF wk_newstop IS NULL THEN
                wk_newstop := '';
            END IF;
            IF wk_newnewget IS NULL THEN
                wk_newnewget := '';
            END IF;

            IF (wk_oldstop!=wk_newstop) OR (wk_oldnewget!=wk_newnewget) THEN
                wk_renewaldate1 := NULL;
                wk_renewaldate2 := NULL;
                wk_renewaldate3 := NULL;
            END IF;

            -- あれば更新
            UPDATE SCHOOL SET
                BUNDLENAME      = r_newcur.BUNDLENAME,
                BUNDLENAME_KANA = r_newcur.BUNDLENAME_KANA,
                NPPDIV          = r_newcur.NPPDIV,
                RANK            = r_newcur.RANK,
                FACULTYCD       = r_newcur.FACULTYCD,
                TEL             = r_newcur.TEL,
                PARENTSCHOOLCD  = r_newcur.PARENTSCHOOLCD,
                STOPDATE        = r_newcur.STOPDATE,
                NEWGETDATE      = r_newcur.NEWGETDATE,
                RENEWALDATE1    = wk_renewaldate1,
                RENEWALDATE2    = wk_renewaldate2,
                RENEWALDATE3    = wk_renewaldate3
            WHERE ROWID = wk_rowid;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                -- なければ新規作成
                INSERT INTO SCHOOL
                VALUES (
                    r_newcur.BUNDLECD,
                    r_newcur.BUNDLENAME,
                    r_newcur.BUNDLENAME_KANA,
                    r_newcur.NPPDIV,
                    r_newcur.RANK,
                    r_newcur.FACULTYCD,
                    r_newcur.TEL,
                    r_newcur.PARENTSCHOOLCD,
                    r_newcur.STOPDATE,
                    r_newcur.NEWGETDATE,
                    NULL,
                    NULL,
                    NULL
                );
            WHEN OTHERS THEN
                RAISE;
        END;

    END LOOP;

EXCEPTION
    WHEN OTHERS THEN
        RAISE;
END;
/

-- マスタから削除されたデータの削除
-- （県データBUNDLECD=**aaaは対象外とする）
DELETE FROM SCHOOL
WHERE  SCHOOL.BUNDLECD IN (SELECT SCHOOL.BUNDLECD
                           FROM   SCHOOL
                           WHERE  (NOT EXISTS (SELECT SCHOOL_TMP.BUNDLECD
                                               FROM   SCHOOL_TMP
                                               WHERE  SCHOOL.BUNDLECD=SCHOOL_TMP.BUNDLECD))
                           AND    (BUNDLECD NOT LIKE '__aaa'));


-- テンポラリテーブル削除
DROP TABLE SCHOOL_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

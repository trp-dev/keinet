------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 学校マスタ
--  テーブル名      : SCHOOL
--  データファイル名: SCHOOL
--  機能            : 学校マスタをデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE SCHOOL_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   BUNDLECD             POSITION(01:05)      CHAR,
   BUNDLENAME           POSITION(06:33)      CHAR,
   BUNDLENAME_KANA      POSITION(34:53)      CHAR,
   NPPDIV               POSITION(54:55)      CHAR,
   RANK                 POSITION(56:56)      CHAR,
   FACULTYCD            POSITION(57:58)      CHAR,
   TEL                  POSITION(59:71)      CHAR,
   PARENTSCHOOLCD       POSITION(72:76)      CHAR,
   STOPDATE             POSITION(77:84)      CHAR,
   NEWGETDATE           POSITION(85:92)      CHAR
)

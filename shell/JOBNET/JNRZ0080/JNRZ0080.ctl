------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 科目別成績（クラス）
--  テーブル名      : SUBRECORD_C
--  データファイル名: SUBRECORD_C
--  機能            : 科目別成績（クラス）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
INSERT PRESERVE BLANKS
INTO TABLE SUBRECORD_C_TMP
(  EXAMYEAR         POSITION(01:04)  CHAR,
   EXAMCD           POSITION(05:06)  CHAR,
   BUNDLECD         POSITION(07:11)  CHAR,
   GRADE            POSITION(12:13)  INTEGER EXTERNAL,
   CLASS            POSITION(14:15)  CHAR,
   SUBCD            POSITION(16:19)  CHAR,
   AVGPNT           POSITION(20:24)  INTEGER EXTERNAL,
   AVGDEVIATION     POSITION(25:28)  INTEGER EXTERNAL,
   AVGSCORERATE     POSITION(29:32)  INTEGER EXTERNAL,
   NUMBERS          POSITION(33:40)  INTEGER EXTERNAL
)

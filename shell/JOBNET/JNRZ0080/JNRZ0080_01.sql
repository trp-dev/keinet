-- ************************************************
-- *機 能 名 : 科目別成績(クラス)
-- *機 能 ID : JNRZ0080_01.sql
-- *作 成 日 : 2004/08/30
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE SUBRECORD_C_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成(ダイレクトパスロードを行うため、実テーブルと一部定義が異なる)
CREATE TABLE SUBRECORD_C_TMP  (
       EXAMYEAR              CHAR(4)              NOT NULL,   /*   模試年度        */ 
       EXAMCD                CHAR(2)              NOT NULL,   /*   模試コード      */ 
       BUNDLECD              CHAR(5)              NOT NULL,   /*   一括コード      */ 
       GRADE                 NUMBER(2,0)          NOT NULL,   /*   学年            */ 
       CLASS                 CHAR(2)              NOT NULL,   /*   クラス          */ 
       SUBCD                 CHAR(4)              NOT NULL,   /*   科目コード      */ 
       AVGPNT                NUMBER(6,0)                  ,   /*   平均点          */ 
       AVGDEVIATION          NUMBER(5,0)                  ,   /*   平均偏差値      */ 
       AVGSCORERATE          NUMBER(5,0)                  ,   /*   平均得点率      */ 
       NUMBERS               NUMBER(8,0)                  ,   /*   人数            */ 
       CONSTRAINT PK_SUBRECORD_C_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  模試年度    */
                 EXAMCD                    ,               /*  模試コード  */
                 BUNDLECD                  ,               /*  一括コード  */
                 GRADE                     ,               /*  学年        */
                 CLASS                     ,               /*  クラス      */
                 SUBCD                                     /*  科目コード  */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

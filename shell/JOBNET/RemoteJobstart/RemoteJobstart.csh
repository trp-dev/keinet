#! /bin/csh 

set JOBNAME = $1
set fileoutname = "/keinavi/JOBNET/remotejobout"
set nowstore = "/keinavi/JOBNET/NowStoreExam"


#echo " $FILENAME"
	
#----リモートシェルを実行
/usr/bin/ssh satu04test /keinavi/JOBNET/JobnetStart_ini.sh ${JOBNAME}

### 2016.09.15 FWEST)田中　エラー判定処理修正 STA ###
set rtncords = $status

if ($rtncords != 0) then
	echo $rtncords > $fileoutname

#if ($status != 0) then
#	echo $status > $fileoutname
### 2016.09.15 FWEST)田中　エラー判定処理修正 END ###

else
	/usr/bin/rsh satu04test cat /keinavi/JOBNET/result_${JOBNAME} > $fileoutname 
endif
/usr/bin/rsh satu04test \rm /keinavi/JOBNET/result_${JOBNAME}

#----コマンド実行結果の識別
set cords = `tail -1 $fileoutname`
if ($cords > 1) then
	echo " "
	echo "処理に失敗しました。下記の内容を元にファイルフォーマットを一度確認してください。"
	echo "=============================================================================="
	#---- エラーファイルを出力（ファイルの25行目〜90行目を表示)
	/usr/bin/rsh satu04test sed -n '25,/120/p' /keinavi/JOBNET/${JOBNAME}/${JOBNAME}.log
	echo "=============================================================================="
	\rm -f /home/navikdc/datastore/inside_release/store.lck
	/usr/bin/rsh satu04test \rm ${nowstore}
	
else if ($cords < 0) then
	echo " "
	echo "処理異常により、処理に失敗しました。管理者に確認してください。"
	\rm -f /home/navikdc/datastore/inside_release/store.lck
	/usr/bin/rsh satu04test \rm ${nowstore}
#else if (($cords == 0)&&((${JOBNAME} == "JNRZ0550")|(${JOBNAME} == "JNRZ0560")||(${JOBNAME} == "JNRZ0580")||(${JOBNAME} == "JNRZ0590"))) then
#	echo "0" >> "/keinavi/JOBNET/restarttomflg"
endif

if (-f $fileoutname) then
	\rm -f $fileoutname
endif
exit $cords



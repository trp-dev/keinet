#! /bin/sh 

nowym=`date +%Y%m`
logym=`expr $nowym - 4`
export logy=`echo $logym | awk '{printf "%-.4s\n",$0}'`
export Log=/keinavi/HULFT/LogDir/store${logy}.log
export nowstoref=/keinavi/JOBNET/StoringExam

## JOBNET起動ログ出力
## 第1パラメータ：JOBNETの起動/停止/エラー識別、第2パラメータ：対象データファイル
if [ $# = 2 ]&&[ -f $2 ]
then

	if [ -f $nowstoref ]
	then
		failname=`echo $2 | sed -e 's/.*\///g'`

		#登録模試定義ファイルより登録中模試コードを取得
		examstr=`cat $nowstoref`
		
		case $1 in 
			"start")
				# ファイルのデータサイズとラインを取得
				size=`wc -cl $2`
				size1=`set -- $size; shift 0; echo $1`
				size2=`set -- $size; shift 1; echo $1`

				# ファイル受信日時を取得	
				datetime=`ls -l $2`
				target1=`set -- $datetime; shift 5; echo $1`
				target2=`set -- $datetime; shift 6; echo $1`
				target3=`set -- $datetime; shift 7; echo $1`
		
				echo "`date +%Y%m%d`-`date +%H%M%S`  [$examstr] $failname [${target1}${target2} ${target3}] [${size1}]件 [${size2}]byte start" >> $Log
				;;
			"err")
				echo "  -----  $failname STORE ERROR  -----  " >> $Log
				;;
			"end")
				echo "`date +%Y%m%d`-`date +%H%M%S` [$examstr] $failname end" >> $Log
				;;
		esac
	else
		failname=`echo $2 | sed -e 's/.*\///g'`

		#ファイルの先頭行と最終行から各先頭6バイト(年度+模試コード)を取得
		export string1=`head -1 $2 | awk '{printf "%-.6s\n",$0}'`
		export string2=`tail -1 $2 | awk '{printf "%-.6s\n",$0}'`

		#取得文字列が年度+模試コード(数字のみ）で構成されているか判別
		resul1=`expr $string1 : "[0-9]\{6\}"`
		resul2=`expr $string2 : "[0-9]\{6\}"`
		if [ $resul1 != 0 ]&&[ $resul2 != 0 ]
		then
			#ファイルがすべて同一の模試データか判別
			if [ " $string1" = " $string2" ]
			then

				case $1 in 
					"start")
						# ファイルのデータサイズとラインを取得
						size=`wc -cl $2`
						size1=`set -- $size; shift 0; echo $1`
						size2=`set -- $size; shift 1; echo $1`

						# ファイル受信日時を取得	
						datetime=`ls -l $2`
						target1=`set -- $datetime; shift 5; echo $1`
						target2=`set -- $datetime; shift 6; echo $1`
						target3=`set -- $datetime; shift 7; echo $1`
		
						echo "`date +%Y%m%d`-`date +%H%M%S`  [$string1] $failname [${target1}${target2} ${target3}] [${size1}]件 [${size2}]byte start" >> $Log
						;;
					"err")
						echo "  -----  $failname STORE ERROR  -----  " >> $Log
						;;
					"end")
						echo "`date +%Y%m%d`-`date +%H%M%S` [$string1] $failname end" >> $Log
						;;
				esac
			else

				export year1=`echo $string1 | awk '{printf "%-.4s\n",$0}'`
				export year2=`echo $string2 | awk '{printf "%-.4s\n",$0}'`
				export exam2=`expr $string2 : ".*\(..\)"`
				if [ $year1 = $year2 ]
				then

					if [ $string1 = ${year1}61 -a $string2 = ${year2}71 ]||[ $string1 = ${year1}62 -a $string2 = ${year2}72 ]||[ $string1 = ${year1}63 -a $string2 = ${year2}73 ]
					then
						export dirname=${string1}_${exam2}
						if [ $1 = "start" ];
						then
							size=`wc -cl $2`
							size1=`set -- $size; shift 0; echo $1`
							size2=`set -- $size; shift 1; echo $1`
	
							datetime=`ls -l $2`
							target1=`set -- $datetime; shift 5; echo $1`
							target2=`set -- $datetime; shift 6; echo $1`
							target3=`set -- $datetime; shift 7; echo $1`
			
							echo "`date +%Y%m%d`-`date +%H%M%S`  $dirname $failname [${target1}${target2} ${target3}] [${size1}]件 [${size2}]byte start" >> $Log
						elif [ $1 = "err" ];
						then
							echo "  -----  $failname STORE ERROR  -----  " >> $Log
						else
							echo "`date +%Y%m%d`-`date +%H%M%S` $dirname $failname end" >> $Log
						fi
					fi
				fi
			fi
		else
			echo "OUT OF FILE"
		fi
	fi
else
#	echo $"Usage: $0 {start|err|end} DataFilename" >> $Log
#	echo $"Usage: $0 {start|err|end} DataFilename"
	echo " " >> $Log
fi


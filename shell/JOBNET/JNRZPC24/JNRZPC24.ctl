------------------------------------------------------------------------------
--  システム名      : PC版模試判定バンザイ統合
--  概念ファイル名  : 
--  テーブル名      : SMF0201F -> UNIVCOMMENT
--  機能            : 
--  作成日          : 2016/09/05
--  修正日          : 
--  備考            : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE UNIVCOMMENT
(  
    EVENTYEAR                POSITION(10:13)     CHAR,
    EXAMDIV                  POSITION(14:15)     CHAR,
    UNIVCD                   POSITION(16:19)     CHAR,
    FACULTYCD                POSITION(20:21)     CHAR,
    DEPTCD                   POSITION(22:25)     CHAR,
    TYPEFLG                  POSITION(26:26)     CHAR PRESERVE BLANKS,
    COMMENTSTATEMENT_ABBR    POSITION(75:84)     CHAR,
    COMMENTSTATEMENT         POSITION(85:220)    CHAR
)

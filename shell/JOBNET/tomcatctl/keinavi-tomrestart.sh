#!/bin/sh
TGT='satu02test satu01 satu02 satu03';
for svr in $TGT; do
    ping $svr -c 1 > /dev/null 2>&1
    if [ $? -eq 0 ];then
      echo "$svr is alive. Shutdown tomcat..."
      /usr/bin/rsh $svr "/home/kei-navi/restart_tomcat.sh"
    else
      echo "$svr doesn't exists."
    fi
done
echo "..Wait 10 secs.."
sleep 10

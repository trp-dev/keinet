#!/bin/sh

svr=S$1
ping $svr -c 1 > /dev/null 2>&1
if [ $? -eq 0 ];then
  echo "$svr is alive. restart tomcat..."
  /usr/bin/rsh $svr "/home/kei-navi/restart_tomcat.sh"
else
  echo "$svr doesn't exists."
fi

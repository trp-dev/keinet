#!/bin/sh
TGT='satu02test satu01 satu02 satu03';

rc=0
date > /keinavi/JOBNET/tomcatctl/keinavi-tomstart.log
echo "Tomcat process start." >> /keinavi/JOBNET/tomcatctl/keinavi-tomstart.log

for svr in $TGT; do
    ping $svr -c 1 > /dev/null 2>&1
    if [ $? -eq 0 ];then
      echo "-- $svr is alive. --"
      count=0
      while :
      do
        ret=`/usr/bin/rsh $svr 'ps aux | grep java | grep ^tomcat ; echo $?'`
        echo $ret
        if [ "$ret" != "1" ];then
          count=$(( $count + 1 ))
          if [ $count -ge 10 ];then
            echo "..Retry error. Couldn't stop tomcat.."
            echo "$svr ..Retry error. Couldn't stop tomcat.." >> /keinavi/JOBNET/tomcatctl/keinavi-tomstart.log
            rc=$(( $rc + 1 ))
            break
          else
            echo "..Tomcat process is already exists. Wait 10 secs.."
            sleep 10
          fi
        else
          echo "..Starting tomcat.."
          /usr/bin/rsh $svr "/usr/local/tomcat/bin/startup.sh"
          break
        fi
      done
    else
      echo "$svr doesn't exists."
      rc=30
      slp=0
    fi
done

exit $rc

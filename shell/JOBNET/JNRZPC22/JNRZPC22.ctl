------------------------------------------------------------------------------
--  システム名      : PC版模試判定バンザイ統合
--  概念ファイル名  : 
--  テーブル名      : DB22_BUNPU -> UNIVDIST_NORMAL_TMP
--  機能            : 
--  作成日          : 2014/05/02
--  修正日          : 
--  備考            : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE UNIVDIST_NORMAL_TMP
FIELDS TERMINATED BY ','
-- OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
	EXAMYEAR      ,
	EXAMCD        ,
	UNIVCD        ,
	FACULTYCD     ,
	DEPTCD1       ,
	DEPTCD2       ,
	PATTERNCD     ,
	RANK          ,
	PUBENTEXAMCAPA,
	NUMBERS_A     ,
	AVGPNT_A      ,
	BORDER1       ,
	NUMBERS1      ,
	BORDER2       ,
	NUMBERS2      ,
	BORDER3       ,
	NUMBERS3      ,
	BORDER4       ,
	NUMBERS4      ,
	BORDER5       ,
	NUMBERS5      ,
	BORDER6       ,
	NUMBERS6      ,
	BORDER7       ,
	NUMBERS7      ,
	BORDER8       ,
	NUMBERS8      ,
	BORDER9       ,
	NUMBERS9      ,
	BORDER10      ,
	NUMBERS10     ,
	BORDER11      ,
	NUMBERS11     ,
	BORDER12      ,
	NUMBERS12     ,
	BORDER13      ,
	NUMBERS13     ,
	BORDER14      ,
	NUMBERS14     ,
	BORDER15      ,
	NUMBERS15     ,
	BORDER16      ,
	NUMBERS16     ,
	BORDER17      ,
	NUMBERS17     ,
	BORDER18      ,
	NUMBERS18     ,
	BORDER19      ,
	NUMBERS19     ,
	NUMBERS_S     ,
	AVGPNT_S      ,
	NUMBERS_G     ,
	AVGPNT_G      
)

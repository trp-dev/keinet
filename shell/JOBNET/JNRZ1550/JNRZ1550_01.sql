-- ************************************************
-- *機 能 名 : 記述系模試小問別成績（高校）
-- *機 能 ID : JNRZ1550_01.sql
-- *作 成 日 : 2019/09/24
-- *作 成 者 : QQ)瀬尾
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE SHOQUESTIONRECORD_S_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE SHOQUESTIONRECORD_S_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM SHOQUESTIONRECORD_S WHERE 0=1;

-- 主キーの設定
ALTER TABLE SHOQUESTIONRECORD_S_TMP ADD CONSTRAINT PK_SHOQUESTIONRECORD_S_TMP PRIMARY KEY 
(
   EXAMYEAR
 , EXAMCD
 , BUNDLECD
 , SUBCD
 , KWEB_SHOQUESTION_DEPT_NO
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

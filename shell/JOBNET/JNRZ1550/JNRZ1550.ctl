------------------------------------------------------------------------------
-- *機 能 名 : 記述系模試小問別成績（高校）のCTLファイル
-- *機 能 ID : JNRZ1550.ctl
-- *作 成 日 : 2019/09/24
-- *作 成 者 : QQ)瀬尾
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE SHOQUESTIONRECORD_S_TMP
(  
   EXAMYEAR                 POSITION(01:04)  CHAR 
 , EXAMCD                   POSITION(05:06)  CHAR 
 , BUNDLECD                 POSITION(07:11)  CHAR 
 , SUBCD                    POSITION(12:15)  CHAR 
 , KWEB_SHOQUESTION_DEPT_NO POSITION(16:18)  INTEGER EXTERNAL 
 , AVGPNT                   POSITION(19:22)  INTEGER EXTERNAL "DECODE(:AVGPNT,                   '9999', null, :AVGPNT/10)"
 , NUMBERS                  POSITION(23:30)  INTEGER EXTERNAL 
 , AVGSCORERATE             POSITION(31:34)  INTEGER EXTERNAL "DECODE(:AVGSCORERATE,             '9999', null, :AVGSCORERATE/10)"
)

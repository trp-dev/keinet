-- *********************************************************
-- *機 能 名 : 記述系模試小問別成績（高校）
-- *機 能 ID : JNRZ1550_02.sql
-- *作 成 日 : 2019/09/24
-- *作 成 者 : QQ)瀬尾
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM SHOQUESTIONRECORD_S MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM SHOQUESTIONRECORD_S_TMP TMP
         WHERE 1=1
           AND MAIN.EXAMYEAR                 = TMP.EXAMYEAR
           AND MAIN.EXAMCD                   = TMP.EXAMCD
           AND MAIN.BUNDLECD                 = TMP.BUNDLECD
           AND MAIN.SUBCD                    = TMP.SUBCD
           AND MAIN.KWEB_SHOQUESTION_DEPT_NO = TMP.KWEB_SHOQUESTION_DEPT_NO
       )
;

-- テンポラリから本物へ登録
INSERT INTO SHOQUESTIONRECORD_S SELECT * FROM SHOQUESTIONRECORD_S_TMP;

-- テンポラリテーブル削除
DROP TABLE SHOQUESTIONRECORD_S_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

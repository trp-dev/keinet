#!/bin/sh

#時刻起動ジョブ監視用シェル
#2015/04/27 FWEST)KUZUYA

export fpath=/keinavi/JOBNET/JOBENDCHECK
export jobnetname=$1
export outfile=${fpath}/result_${jobnetname}.log

echo ${jobnetname} > ${outfile}
date >> ${outfile}

exit 0

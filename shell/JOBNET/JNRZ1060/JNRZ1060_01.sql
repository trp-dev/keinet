-- ************************************************
-- *機 能 名 : ガイドライン科目文字列マスタ
-- *機 能 ID : JNRZ1060_01.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE GUIDESUBSTRING_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE GUIDESUBSTRING_TMP( 
       EXAMDIV             CHAR(2)              NOT NULL,   /*     模試区分            */
       UNIVCD              CHAR(4)              NOT NULL,   /*     大学コード          */
       FACULTYCD           CHAR(2)              NOT NULL,   /*     学部コード          */
       DEPTCD              CHAR(2)              NOT NULL,   /*     学科コード          */
       ENTEXAMDIV          CHAR(1)              NOT NULL,   /*     入試試験区分        */
       BRANCHCD            CHAR(2)              NOT NULL,   /*     枝番                */
       STRING              VARCHAR2(200)                ,   /*     文字列              */
    CONSTRAINT PK_GUIDESUBSTRING_TMP PRIMARY KEY(
        EXAMDIV,                                            /*     模試区分            */
        UNIVCD,                                             /*     大学コード          */
        FACULTYCD,                                          /*     学部コード          */
        DEPTCD,                                             /*     学科コード          */
        ENTEXAMDIV,                                         /*     入試試験区分        */
        BRANCHCD                                            /*     枝番                */
    )
        USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

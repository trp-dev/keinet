------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : ガイドライン科目文字列マスタ
--  テーブル名      : GUIDESUBSTRING_TMP
--  データファイル名: GUIDESUBSTRINGyyyymmdd.csv
--  機能            : ガイドライン科目文字列マスタをデータファイルの内容に置き換える
--  作成日          : 2004/08/25
--  修正日          : 2004/10/05
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE GUIDESUBSTRING_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    EXAMDIV    "DECODE(LENGTH(:EXAMDIV),   1, CONCAT('0', :EXAMDIV),   2, :EXAMDIV)",
    UNIVCD,
    FACULTYCD  "DECODE(LENGTH(:FACULTYCD), 1, CONCAT('0', :FACULTYCD), 2, :FACULTYCD)",
    DEPTCD     "DECODE(LENGTH(:DEPTCD),    1, CONCAT('0', :DEPTCD),    2, :DEPTCD)",
    ENTEXAMDIV,
    BRANCHCD   "DECODE(LENGTH(:BRANCHCD),  1, CONCAT('0', :BRANCHCD),  2, :BRANCHCD)",
    STRING
)

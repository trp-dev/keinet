-- ************************************************
-- *機 能 名 : ガイドライン科目文字列マスタ
-- *機 能 ID : JNRZ1060_02.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM GUIDESUBSTRING;

-- テンポラリテーブルからデータを挿入
INSERT INTO GUIDESUBSTRING
SELECT * FROM GUIDESUBSTRING_TMP;

-- テンポラリテーブル削除
DROP TABLE GUIDESUBSTRING_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

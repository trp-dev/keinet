#! /bin/sh 

#export conffile="/keinavi/JOBNET/JN_JudgeCheck/checkexam.txt"
export conffile="/home/navikdc/JudgeCheck/checkexam.txt"
export checkfiles="INDIVIDUALRECORD"
export workdirpath="/keinavi/JOBNET/JN_JudgeCheck"

#--- 判定チェック模試コード取得
export judgeexamtmp=`cat $conffile`
export judgeexam1=`echo $judgeexamtmp | tr -d '\r\n'`
export judgeexam=`echo  $judgeexam1 | awk '{printf "%-.6s\n",$0}'`
examcd=`expr ${judgeexam} : ".*\(..\)"`

echo "--- 判定チェック処理開始 (対象模試：$examcd ）"

if [ $examcd = "01" ]||[ $examcd = "02" ]||[ $examcd = "03" ]||[ $examcd = "04" ]||[ $examcd = "66" ]||[ $examcd = "38" ]
then
	#--- 判定チェックリモート実行（対象データがマーク系模試の場合）
	rsh satu01test -l kei-navi /keinavi/JudgeCheck/RemoteJudgeEXE.sh 01

#elif [ $examcd = "05" ]||[ $examcd = "06" ]||[ $examcd = "07" ]
#-- 高12分割により追加
elif [ $examcd = "05" ]||[ $examcd = "06" ]||[ $examcd = "07" ]||[ $examcd = "65" ]
then
	#--- 判定チェックリモート実行（対象データが記述系模試の場合）
	rsh satu01test -l kei-navi /keinavi/JudgeCheck/RemoteJudgeEXE.sh 02
fi

echo " "
echo "判定チェック処理完了 ---"
echo " "


rsh satu01test -l kei-navi mv /keinavi/JudgeCheck/file/INDIVIDUALRECORD_cut /keinavi/JudgeCheck/file/INDIVIDUALRECORD_cut-$judgeexam

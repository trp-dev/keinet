#! /bin/sh

#export BasicWorkDir="/keinavi/JOBNET/JN_JudgeCheck"

export BasicWorkDir="/home/navikdc/JudgeCheck/result"
export conffile="/home/navikdc/JudgeCheck/checkexam.txt"
export workfile="/keinavi/JOBNET/JN_JudgeCheck/INDIVIDUALRECORD"

#--- 判定チェック模試コード取得
export judgeexamtmp=`cat $conffile`
export judgeexam1=`echo $judgeexamtmp | tr -d '\r\n'`
export judgeexam=`echo  $judgeexam1 | awk '{printf "%-.6s\n",$0}'`


#--- ホストとの差分結果出力
rsh satu01test -l kei-navi /keinavi/JudgeCheck/eraseOut.pl /keinavi/JudgeCheck/result_log.csv

#--- 判定チェック結果出力をダウンロード可能エリアへコピー
echo "--- $judgeexam 判定チェック結果ファイル取得 ---"
nowdate=`date +%Y%m%d%H%M%S`
rsh satu01test -l kei-navi cat /keinavi/JudgeCheck/result_log.csv_eraseOut > ${BasicWorkDir}/${nowdate}-${judgeexam}_log.csv

lineout=`wc -l ${BasicWorkDir}/${nowdate}-${judgeexam}_log.csv`
fline=`set -- $lineout; shift 0; echo $1`

echo "======================================="
echo " 差分件数: $fline "
echo " ファイル:${BasicWorkDir}/${nowdate}-${judgeexam}_log.csv"
echo "======================================="

rsh satu01test -l kei-navi \rm /keinavi/JudgeCheck/result_log.csv*
\rm -f ${workfile}*

\rm -f $conffile

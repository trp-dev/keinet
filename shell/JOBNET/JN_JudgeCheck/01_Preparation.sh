#! /bin/sh 

#export conffile="/keinavi/JOBNET/JN_JudgeCheck/checkexam.txt"

export conffile="/home/navikdc/JudgeCheck/checkexam.txt"
export datapath="/keinavi/HULFT/NewEXAM"
export checkfiles="INDIVIDUALRECORD"

export workdirpath="/keinavi/JOBNET/JN_JudgeCheck"

#--- 定義ファイルチェック
if [[ ! -f $conffile ]]; then
	echo "--- 定義ファイルが存在しません ---"
	exit 30
fi

export judgeexamtmp=`cat $conffile`
#--- 変数内に改行があった場合は削除(空行にも対応)
export judgeexam1=`echo $judgeexamtmp | tr -d '\r\n'`
export judgeexam=`echo  $judgeexam1 | awk '{printf "%-.6s\n",$0}'`

export whilecount=1
while [ $whilecount -le 2 ];
do
	if [ $whilecount = 2 ]; then
		datapath="/keinavi/HULFT/NewEXAM/$judgeexam"
	fi

	#--- 判定対象データの確認
	(ls ${datapath}/${checkfiles}* > listout.log) >& /dev/null
	filelists=`cat listout.log`
	\rm -f listout.log

	fcount=1
	for files in $filelists; do
		echo "--- ファイルチェック... "
		echo " "
	
		head_string=`head -1 $files | awk '{printf "%-.6s\n",$0}'`
		tail_string=`tail -1 $files | awk '{printf "%-.6s\n",$0}'`

		if [ $head_string = $tail_string ]&&[ $head_string = $judgeexam ]; then
			cp -p $files $workdirpath/INDIVIDUALRECORD
			fcount=`expr $fcount + 1`
		fi
	done

	#--- 判定チェック対象の模試データ有無確認
	if [ $fcount = 1 ]; then
		if [ $whilecount = 2 ]; then
			echo "--- 判定チェック対象データが存在しません ---"
			exit 30
		fi
		whilecount=`expr $whilecount + 1`
		continue
	fi

	break
done

#--- 判定チェック用入力ファイル作成
cd $workdirpath
echo "--- 判定チェック用入力ファイル作成 ---"
$workdirpath/INDIVIDUAL_cut.pl $workdirpath/INDIVIDUALRECORD

echo " "

#--- ファイルレコード数取得
fsize=`wc -l $workdirpath/INDIVIDUALRECORD_cut`
fsize1=`set -- $fsize; shift 0; echo $1`

#--- 判定チェック実行サーバへファイル転送
echo "--- 判定チェック実行サーバへファイル(処理件数：$fsize1)転送..."
/keinavi/HULFT/ftp.sh /keinavi/JudgeCheck/file $workdirpath INDIVIDUALRECORD_cut 10.1.4.198

echo " "
echo "--- 判定チェック事前準備完了 ---"

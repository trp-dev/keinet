#!/bin/sh

#環境変数設定
export JOBNAME=JNRZ3020
export WORKDIR=/keinavi/JOBNET/$JOBNAME

#名寄せ処理実行(Java)
cd /keinavi/bat/FreeMenu/F002_2
/keinavi/bat/FreeMenu/F002_2/F002_2.sh

#Javaプログラム戻り値チェック(0以外は異常終了)
#if [ $? -ne 0 ]
#then
#    echo バッチ処理でエラーが発生しました
#    exit 30
#fi

#正常終了
echo バッチ処理が正常に終了しました
exit  0    #正常終了

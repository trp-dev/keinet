#! /bin/csh

set examtest1 = "/keinavi/HULFT/JOBNET/200605release-tmp"
echo $examtest1

set examtest = `echo $examtest1 | sed -e s'/.*\///'g`
echo "ファイルのフルパスからパス削除 $examtest"

#set endexam = `echo $examtest | sed -e s'/....$//'g`
set endexam = `echo $examtest | sed -e s'/[a-z-]*$//'g`

echo "文字列削除　$endexam"

set dada = `echo $endexam | sed -e s'/^[0-9]\{4\}//'g`
echo "前から4文字削除 $dada"

set exam1 = `echo $dada | awk '{printf "%-.2s\n",$0}'`
set exam2 = `expr $dada : ".*\(..\)"`
echo "前2文字取得 $exam1 , 後ろ2文字取得 $exam2"
if ($exam1 == $exam2) then
	set exam = $exam1
else
	set exam = ($exam1 $exam2)
endif
echo "変数個数 $#exam"

if (-f /keinavi/JOBNET/test/aaa) then
	echo "ルート１"
#	mv /keinavi/JOBNET/test/adada /keinavi/JOBNET/test/gggg
#else if (-f /keinavi/JOBNET/test/kkkk) then
#	echo "ルート２"
#	mv /keinavi/JOBNET/test/kkkk /keinavi/JOBNET/test/ddddd
endif

------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 科目分布別成績（全国）
--  テーブル名      : SUBDISTRECORD_A
--  データファイル名: SUBDISTRECORD_A
--  機能            : 科目分布別成績（全国）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE SUBDISTRECORD_A_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   EXAMYEAR     POSITION(01:04)  CHAR,
   EXAMCD       POSITION(05:06)  CHAR,
   SUBCD        POSITION(07:10)  CHAR,
   DEVZONECD    POSITION(11:12)  CHAR,
   NUMBERS      POSITION(13:20)  INTEGER EXTERNAL "NULLIF(:NUMBERS,   '99999999')",
   COMPRATIO    POSITION(21:24)  INTEGER EXTERNAL "DECODE(:COMPRATIO,     '9999', null, :COMPRATIO/10)"
)

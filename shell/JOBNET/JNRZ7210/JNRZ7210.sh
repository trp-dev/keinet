#!/bin/sh

#センターリサーチ契約校選択リスト配信
cd /keinavi/HULFT
/usr/local/HULFT/bin/utlsend -f RZRM7210

if [ $? -ne 0 ]
then
    echo 配信エラーが発生しました
    exit 30
fi

mv PACTSCHOOL_SELECTLIST WorkDir/PACTSCHOOL_SELECTLIST

exit 0

-- ************************************************
-- *機 能 名 : 
-- *機 能 ID : CORRES_01.sql
-- *作 成 日 : 2012/02/03
-- *作 成 者 : KCN udono
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE CORRESPONDINGSCHOOL_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE CORRESPONDINGSCHOOL_TMP  (
       SECTORSORTINGCD            CHAR(3)              NOT NULL,
       SCHOOLCD                   CHAR(5)              NOT NULL,
       CONSTRAINT PK_CORRESPONDINGSCHOOL_TMP PRIMARY KEY (
                 SECTORSORTINGCD           ,
                 SCHOOLCD
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

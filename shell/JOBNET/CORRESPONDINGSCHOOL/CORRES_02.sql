-- *********************************************************
-- *機 能 名 : 
-- *機 能 ID : CORRES_02.sql
-- *作 成 日 : 2012/02/03
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM CORRESPONDINGSCHOOL;

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO CORRESPONDINGSCHOOL
SELECT * FROM CORRESPONDINGSCHOOL_TMP;

-- テンポラリテーブル削除
DROP TABLE CORRESPONDINGSCHOOL_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

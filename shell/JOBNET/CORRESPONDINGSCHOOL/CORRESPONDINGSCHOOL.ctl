------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 
--  テーブル名      : CORRESPONDINGSCHOOL
--  データファイル名: CORRESPONDINGSCHOOL
--  機能            : 
--  作成日          : 2012/02/03
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE CORRESPONDINGSCHOOL_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   SECTORSORTINGCD,
   SCHOOLCD
)

------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 大学マスタ公表用選択グループ
--  テーブル名      : UNIVMASTER_SELECTGROUP_PUB
--  データファイル名: UNIVMASTER_SELECTGROUP_PUB
--  機能            : 大学マスタ公表用選択グループをデータファイルの内容に置き換える
--  作成日          : 2010/02/10
--  修正日          : 
--  備考            : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE UNIVMASTER_SELECTGROUP_PUB_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    EVENTYEAR,
    SCHOOLDIV,
    UNIVCD,
    EXAMDIV,
    ENTEXAMDIV,
    UNICDBRANCHCD          CHAR "LPAD(:UNICDBRANCHCD, 3, '0')",
    SELECTG_NO,
    SELECTGSUBCOUNT        INTEGER EXTERNAL "NULLIF(:SELECTGSUBCOUNT,          '9')",
    SELECTGALLOTPNT        INTEGER EXTERNAL "DECODE(:SELECTGALLOTPNT,'99999', null, :SELECTGALLOTPNT)"
)

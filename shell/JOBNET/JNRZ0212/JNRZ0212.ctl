------------------------------------------------------------------------------
-- *機 能 名 : 志望大学評価別人数（英語認定試験含む）(クラス)のCTLファイル
-- *機 能 ID : JNRZ0212.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE RATINGNUMBER_C_ENGPT_TMP
(  
   EXAMYEAR             POSITION(01:04)  CHAR
 , EXAMCD               POSITION(05:06)  CHAR
 , BUNDLECD             POSITION(07:11)  CHAR
 , GRADE                POSITION(12:13)  INTEGER EXTERNAL
 , CLASS                POSITION(14:15)  CHAR
 , RATINGDIV            POSITION(16:16)  CHAR
 , UNIVCOUNTINGDIV      POSITION(17:17)  CHAR
 , UNIVCD               POSITION(18:21)  CHAR
 , FACULTYCD            POSITION(22:23)  CHAR
 , DEPTCD               POSITION(24:27)  CHAR
 , AGENDACD             POSITION(28:28)  CHAR
 , STUDENTDIV           POSITION(29:29)  CHAR
 , TOTALCANDIDATENUM    POSITION(30:33)  INTEGER EXTERNAL
 , FIRSTCANDIDATENUM    POSITION(34:37)  INTEGER EXTERNAL
 , RATINGNUM_A          POSITION(38:41)  INTEGER EXTERNAL
 , RATINGNUM_B          POSITION(42:45)  INTEGER EXTERNAL
 , RATINGNUM_C          POSITION(46:49)  INTEGER EXTERNAL
 , RATINGNUM_D          POSITION(50:53)  INTEGER EXTERNAL
 , RATINGNUM_E          POSITION(54:57)  INTEGER EXTERNAL
)

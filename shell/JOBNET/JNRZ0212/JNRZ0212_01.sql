-- ************************************************
-- *機 能 名 : 志望大学評価別人数（英語認定試験含む）(クラス)
-- *機 能 ID : JNRZ0212_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE RATINGNUMBER_C_ENGPT_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE RATINGNUMBER_C_ENGPT_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM RATINGNUMBER_C_ENGPT WHERE 0=1;

-- 主キーの設定
ALTER TABLE RATINGNUMBER_C_ENGPT_TMP ADD CONSTRAINT PK_RATINGNUMBER_C_ENGPT_TMP PRIMARY KEY 
(
   EXAMYEAR
 , EXAMCD
 , BUNDLECD
 , GRADE
 , CLASS
 , RATINGDIV
 , UNIVCOUNTINGDIV
 , UNIVCD
 , FACULTYCD
 , DEPTCD
 , AGENDACD
 , STUDENTDIV
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

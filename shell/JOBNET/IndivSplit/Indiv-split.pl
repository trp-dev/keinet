#!/usr/bin/perl

#### 個人成績表データを契約校リストから契約校と非契約校をわける
#### 主にレギュラーデータが対象

$ORG_FILE = "@ARGV[0]";
$OUT_FILE = "/keinavi/JOBNET/IndivSplit/INDIVIDUALRECORD-pact";
$OUT_FILE2 = "/keinavi/JOBNET/IndivSplit/INDIVIDUALRECORD-nopact";
$TEMP_FILE = "/keinavi/HULFT/WorkDir/PACTSCHOOL_SELECTLIST";

if(! open(IN, "$ORG_FILE")){
        print "FILE OPEN ERROR!\n";
        exit;
}
if(! open(OUT, ">$OUT_FILE") ){
        print "FILE OPEN ERROR!!\n";
	close(IN);
	exit;
}
if(! open(OUT2, ">$OUT_FILE2") ){
        print "FILE OPEN ERROR\n";
	close(IN);
	close(OUT);
	exit;
}
$count = 0;
$count1 = 0;
while(<IN>){
	$count1 ++;
	$flg = 0;
        chomp($_);
        $schoolcd  = substr($_,  17, 5);
        $allstr = substr($_, 0, );

	if(! open(IN2, "$TEMP_FILE")){
       		 print "FILE OPEN ERROR!\n";
       		 exit;
	}

	while(<IN2>){
		
        	chomp($_);
        	$univcd = substr($_,  0, );
		if($schoolcd == $univcd){
			print OUT "$allstr\n";
#			print "$count1 行目, $univcd pactschool\n";
			$flg = 1;
			$count ++;
			last;
		}
	}
	if ($flg == 0){
#		print "$count1\n";
		print OUT2 "$allstr\n";
	}
	close(IN2);
}
close(OUT);
close(OUT2);
close(IN);

print "**** 契約校データ $count/$count1 record 出力 ****\n";
exit;

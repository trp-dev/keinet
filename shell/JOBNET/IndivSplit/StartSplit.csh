#! /bin/csh

set dirs = "/keinavi/JOBNET/IndivSplit"
set examconffile = "$dirs/tgexamcd"

set pactlist = "/keinavi/HULFT/WorkDir/PACTSCHOOL_SELECTLIST"

if ((-f $examconffile)&&(-f $pactlist)) then
	echo "--- 個表データの契約校データ、非契約校データの分割処理開始..."
	set examcdstr = `cat $examconffile`
	echo "分割ターゲット模試：$examcdstr"
	
	set tgindivfile = "/keinavi/HULFT/NewEXAM/${examcdstr}/INDIVIDUALRECORD"
	cd $dirs
	
#	echo "$dirs/Indiv-split.pl $tgindivfile"
	if (! -f $tgindivfile) then
		echo " ファイルが存在しません。 $tgindivfile"
		if (-f $examconffile) then
			\rm -f $examconffile
		endif
		exit 1
	endif
		
	$dirs/Indiv-split.pl $tgindivfile

	set sizes = `wc -cl $tgindivfile`
	set sizes1 = `wc -cl $dirs/INDIVIDUALRECORD-pact`
	set sizes2 = `wc -cl $dirs/INDIVIDUALRECORD-nopact`

	echo " "
	echo "各ファイル内データ件数確認"
	echo "   個表データ全件数： $sizes[1]"
	echo "   契約校データ件数： $sizes1[1]"
	echo "   非契約データ件数： $sizes2[1]"

	mv  $dirs/INDIVIDUALRECORD-pact /keinavi/HULFT/NewEXAM/${examcdstr}
	chmod 664 /keinavi/HULFT/NewEXAM/${examcdstr}/INDIVIDUALRECORD-pact
	mv  $dirs/INDIVIDUALRECORD-nopact /keinavi/HULFT/NewEXAM/${examcdstr}
	chmod 664 /keinavi/HULFT/NewEXAM/${examcdstr}/INDIVIDUALRECORD-nopact
	echo " "
	echo "契約校データ、非契約校データの分割処理正常終了 ---"

	set examcdstr2 = `cat $examconffile`
	# 対象模試ファイルは、処理開始時から内容に変化がなければ削除する
	if (${examcdstr} == ${examcdstr2}) then
		\rm -f $examconffile
	endif
endif



-- ************************************************
-- *機 能 名 : 国語記述式総合評価（県）
-- *機 能 ID : JNRZ1330_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE KOKUGODESC_COMPEVAL_P_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE KOKUGODESC_COMPEVAL_P_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM KOKUGODESC_COMPEVAL_P WHERE 0=1;

-- 主キーの設定
ALTER TABLE KOKUGODESC_COMPEVAL_P_TMP ADD CONSTRAINT PK_KOKUGODESC_COMPEVAL_P_TMP PRIMARY KEY 
(
   EXAMYEAR
 , EXAMCD
 , PREFCD
 , SUBCD
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

-- *********************************************************
-- *機 能 名 : 国語記述式総合評価（県）
-- *機 能 ID : JNRZ1330_02.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM KOKUGODESC_COMPEVAL_P MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM KOKUGODESC_COMPEVAL_P_TMP TMP
         WHERE 1=1
           AND MAIN.EXAMYEAR  = TMP.EXAMYEAR
           AND MAIN.EXAMCD    = TMP.EXAMCD
           AND MAIN.PREFCD    = TMP.PREFCD
           AND MAIN.SUBCD     = TMP.SUBCD
       )
;

-- テンポラリから本物へ登録
INSERT INTO KOKUGODESC_COMPEVAL_P SELECT * FROM KOKUGODESC_COMPEVAL_P_TMP;

-- テンポラリテーブル削除
DROP TABLE KOKUGODESC_COMPEVAL_P_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

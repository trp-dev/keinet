#! /bin/csh 

### お役立ちリンクの模試成績速報　リンク表示名変更スクリプト 
###


set MainDir = "/keinavi/JOBNET/JN_UsefulLinkMainte"
set fileDir = ${MainDir}/file
set targetfile = $fileDir/usefulLink-tmp.jsp

set Upfilename = "usefulLink.jsp"

#-- リンク名定義ファイル
#set configfile = "/keinavi/JOBNET/JN_UsefulLinkMainte/LinkExamName.txt"
set configfile = "/keinavi/help_user/OtherMainte/Set/LinkExamName.txt"

set UpFileDir = (/usr/local/tomcat/webapps2/keinavi/jsp/shared_lib /usr/local/tomcat/webapps2/keispro/jsp/shared_lib)

#set changeflg = 0

set upflag = $1
if ($upflag == 1) then

        #--- 定義ファイルの有無確認
	if (! -f $configfile) then
		echo "***** Config file ERROR *****"
		echo "---- 定義ファイルがありません。定義ファイルを配置後、再度実行してください。----"
#               set changeflg = 1
#               set configfile = "/keinavi/help_user/OtherMainte/Get/LinkExamName.txt"
		exit 30
	endif

	#--- 定義ファイル読み込み
	set examstring_tmp = `cat $configfile`

	#--- 定義ファイル内文字列チェック
	#-- 改行削除
	echo "--- 定義ファイル内文字列チェック ---"
	set examstringtmp = `echo ${examstring_tmp} | tr -d '\r\n'`
	set examstring  = `echo $examstringtmp | sed -e s'/\//\\\//'g`
	
        #-- ファイル行数が１行で無い場合はフォーマットエラー
	if ($#examstring != 1) then
		echo "****** Config file Format ERROR *****"
		echo "---- 定義ファイルのフォーマットに誤りがあります。ファイルを確認してください。----"
		exit 30
	endif

	#--- 文字列書き込み（置換対象文字列の設定）
	set changestring = "s/#newexamname#/$examstring/g"

	#--- ファイル出力
	sed -e $changestring $targetfile > ${fileDir}/${Upfilename}

	if ($status != 0) then
		exit 30
	endif

	echo "--- 模試名変更処理完了 ---"

	#--- 検証機への各フォルダへ展開
	set counts = 1
	echo " "
	echo "--- ファイルを検証機へ転送 ---"
	while ($counts <= $#UpFileDir)

		#--- 検証機へFTP転送
		/keinavi/HULFT/ftp.sh $UpFileDir[$counts] ${fileDir} ${Upfilename} 10.1.4.198
		@ counts ++
	end

	#--- 各ファイルを検証機内で同期する
	${MainDir}/sync_content_usefullink.sh 1

	#--- 本番機展開有無フラグ
	touch ${MainDir}/ServUp.txt

	echo " "
	echo "--- 検証機のリンク模試名変更完了 ---"

else if($upflag == 2) then

	if (! -f ${MainDir}/ServUp.txt) then
		echo "***** UpDate ERROR ******"
		echo "---- 検証機への反映がされていません。検証機への反映後、再度実施してください ----"
		exit 30
	endif

	#--- 各ファイルを本番機へ展開（検証機と同期）する
	echo "--- ファイルを本番機へ転送 ---"
	echo " "
	${MainDir}/sync_content_usefullink.sh 2

	echo "--- 本番機のリンク模試名変更完了 ---"
	\rm ${MainDir}/ServUp.txt
	\rm ${fileDir}/${Upfilename}

	\mv -f /keinavi/help_user/OtherMainte/Set/LinkExamName* /keinavi/help_user/OtherMainte/Get
endif


#! /bin/csh

#### 検証機登録データ用のJOBNET起動
#### 対象模試をファイルサーバへ転送＆JOBNET起動のためのメッセージ事象発生
### (過年度志望大学評価人数登録の優先度を、一番最後から通常にしたため関連処理はコメントアウト)

set lockf = "/home/navikdc/datastore/inside_release/store.lck"

if (-f $lockf) then
	set NewexamDir = `cat $lockf`
endif

#--データ種別により模試定義ファイルが異なる。
if ($NewexamDir == "/keinavi/HULFT/NewEXAM") then
	#-- レギュラー、事後・得点修正、都立、テスト
	set examstoreconf = "${NewexamDir}/selectioncd/grepexam"
	set dtypeflg = 0

	###--過年度志望大学評価別人数用
#	if ((-f "${NewexamDir}/selectioncd/ratingstore)&&(! -f $examstoreconf)) then
#		set examstoreconf = "${NewexamDir}/selectioncd/ratingstore"
#		set dtypeflg = 2
#	endif
else
	#-- 本番機登録完了後のデータ抽出データ
	set examstoreconf = "${NewexamDir}/grepexamstr"
	set dtypeflg = 1
endif
	
if (-f $examstoreconf) then
	set storeexamcount = `cat $examstoreconf`
	\rm -f $examstoreconf

	echo "--- 登録対象模試 ($storeexamcount) すべてに対しJOBNET起動を開始します。"

	set counter = 1
	while ($counter <= $#storeexamcount) 
		if ($counter == 1) then
			set firstexam = $storeexamcount[$counter]
			echo "$firstexam" > /keinavi/JOBNET/NowStoreExam

			#--- レギュラーデータ時のみの処理のためデータ種別文字列を取得
			set datatystr = `echo $firstexam | sed -e s'/[0-9]_*//'g`
			if (($datatystr == "release")&&($dtypeflg == 0)) then
				#--- 模試コード限定のため、模試コード取得
				set datatystr = `echo $firstexam | sed -e s'/[a-z]*//'g`
				set ecd = `echo $datatystr | sed -e s'/^[0-9]\{4\}//'g`

#				if (($ecd == 01)||($ecd == 02)||($ecd == 03)||($ecd == 04)||($ecd == 05)||($ecd == 06)||($ecd == 07)||($ecd == 09)||($ecd == 61_71)||($ecd == 62_72)||($ecd == 63_73)||($ecd == 66)||($ecd == 65_75)) then
#				if (($ecd == 01)||($ecd == 02)||($ecd == 03)||($ecd == 04)||($ecd == 05)||($ecd == 06)||($ecd == 07)||($ecd == 09)||($ecd == 61_71)||($ecd == 62_72)||($ecd == 63_73)||($ecd == 66)||($ecd == 65)||($ecd == 75)) then
				if (($ecd == 01)||($ecd == 02)||($ecd == 03)||($ecd == 04)||($ecd == 05)||($ecd == 06)||($ecd == 07)||($ecd == 09)||($ecd == 61)||($ecd == 62)||($ecd == 63)||($ecd == 71)||($ecd == 72)||($ecd == 73)||($ecd == 66)||($ecd == 65)||($ecd == 74)) then
					#--- 個表データ分割用に模試コードをファイルに出力
					echo $firstexam > /keinavi/JOBNET/IndivSplit/tgexamcd
				endif

				#--- 過年度の志望大学評価別人数データ登録用
#				if (($ecd != 61_71)&&($ecd != 62_72)&&($ecd != 63_73)&&($ecd != 75)&&($ecd != 28_68_78)&&($ecd != 37_69_79)) then
#					set oldyearstr = `expr $datatystr - 100`
#					echo "$oldyearstr" > "${NewexamDir}/selectioncd/ratingstore"
#				endif
			endif

			#---ログ出力用登録対象模試定義ファイル転送
			/keinavi/HULFT/ftp.sh /keinavi/JOBNET /keinavi/JOBNET NowStoreExam
			rm -f /keinavi/JOBNET/NowStoreExam
		else
			echo "$storeexamcount[$counter]" >> $examstoreconf
		endif
		@ counter ++
	end

	set ftpfile = "${firstexam}selection.tar.Z"

	#--- ファイルの圧縮＆転送
	if (${dtypeflg} == 1) then
		#-- 本番機登録完了データの抽出データ
		set mvdir = ${NewexamDir}/temp
		cd $mvdir
		set firstexamse = ${firstexam}
	else
		#-- レギュラー・事後・得点修正・都立・テスト 
		set mvdir = ${NewexamDir}/$firstexam/
		cd $mvdir
		set firstexamse = ${firstexam}selection
	endif

	tar -Zcf ${ftpfile} ${firstexamse}/*
	echo " --- ファイル転送開始..."
	/keinavi/HULFT/ftp.sh /HULFT ${mvdir} ${ftpfile}

	#--- ファイルの展開
	echo " --- 転送完了ファイルの展開開始..."
	/usr/bin/ssh satu04test /keinavi/JOBNET/decompression.sh /HULFT ${ftpfile}
        if ($status != 0) then
                echo "--- ERROR：リモートシェル実行（転送ファイルの展開）に失敗しました ---"
                \rm $ftpfile
                exit
        endif

	#--- 転送したファイルを登録データ対象フォルダへ配置&削除
	set tardir = "/HULFT/${firstexamse}"
	/usr/bin/ssh satu04test "cp -p ${tardir}/* /keinavi/HULFT/WorkDir/"
	/usr/bin/rsh satu04test "\rm -rf ${tardir}"

	#--- 圧縮ファイル削除
	\rm -rf ${firstexamse}
	\rm -f ${mvdir}/${ftpfile}

	/usr/bin/rsh satu04test /keinavi/JOBNET/storelog.sh
	
	#--- JOBNET起動(メッセージ事象発生)
	if (-f /keinavi/JOBNET/IndivSplit/tgexamcd) then
		echo " --- 個人成績表データ分割 JOBNET起動(メッセージ事象発生)　模試：${firstexam}"
		echo " "
		jobschmsgevent SPLIT-START -h satu04
	endif

	echo " --- 各種データ展開処理(検) JOBNET起動(メッセージ事象発生)　模試：${firstexam}"
	jobschmsgevent JOBNET-START -h satu04

	exit 10
endif

/usr/bin/rsh satu04test "rm -f /keinavi/JOBNET/NowStoreExam"
\rm -f $lockf
echo "--- データ登録完了 ---"
exit 0

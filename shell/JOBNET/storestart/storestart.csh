#! /bin/csh 

#### 本番機登録データ用のJOBNET起動
#### 対象模試を登録用フォルダへ移動＆JOBNET起動のためのメッセージ事象発生

#-- 残模試データ登録リストファイル
set storeconff = "/keinavi/JOBNET/remainderexam"
#-- ログ出力用データ登録中模試確認ファイル
set storinge = "/keinavi/JOBNET/StoringExam"
#-- 登録ロックファイル
set storelock = "/home/navikdc/datastore/release/store.lck"
#-- DL公開JOBNET用公開模試リストファイル
set DLreleaseexam = "/keinavi/JOBNET/DLstoreexam"
set DLstatusfile = "/keinavi/JOBNET/setexamfile.txt"

#-- 統計集作成定義ファイル
set checkstatistics = "/home/navikdc/Statistics/storedata.csv"

#-- 過年度の志望大学評価別人数データ登録定義ファイル
set RatingStore = "/keinavi/JOBNET/PreviousRating"          ## マーク・記述用
set OPRatingStore = "/keinavi/JOBNET/PreviousOPRating"      ## 私大・OP用

set enddatadir = "/keinavi/HULFT/DATA"
set newexamdir = "/keinavi/HULFT/NewEXAM"
set StoreWorkdir = "/keinavi/HULFT/WorkDir"

#--登録完了データファイルをデータ管理フォルダへ移動
if (-f $storinge) then
	set storeexamstr = `cat $storinge`
	#-- 模試種別文字列削除（年度＋模試コード取得）
	set endexam = `echo $storeexamstr | sed -e s'/[a-z]*$//'g`
	#-- 年度取得（先頭4バイト取得）
	set yearstring = `echo $endexam | awk '{printf "%-.4s\n",$0}'`
	#-- 模試コード取得（先頭4バイト(年度)削除）
	set examdelyear = `echo $endexam | sed -e s'/^[0-9]\{4\}//'g`
	#-- 模試種別文字列取得
	set endstoretype = `echo $storeexamstr | sed -e s'/[0-9]_*//'g`
	
	set endyearexam = ${endexam}
	
#	#-- 対象模試（高1･2模試）が単体登録の場合
#	if (($examdelyear == 61)||($examdelyear == 71)) then
#		#-- 移動先管理フォルダ名を固定にする
#		set endyearexam = ${yearstring}61_71
#	else if (($examdelyear == 62)||($examdelyear == 72)) then
#		#-- 移動先管理フォルダ名を固定にする
#		set endyearexam = ${yearstring}62_72
#	else if (($examdelyear == 63)||($examdelyear == 73)) then
#		#-- 移動先管理フォルダ名を固定にする
#		set endyearexam = ${yearstring}63_73
#--高12分割
#	else if (($examdelyear == 65)||($examdelyear == 75)) then
#		#-- 移動先管理フォルダ名を固定にする
#		set endyearexam = ${yearstring}65_75
#	endif


	#--移動先管理フォルダ定義	
	set movedatadir = "$enddatadir/$endyearexam"

	if (! -d $movedatadir) then
		mkdir $movedatadir
		chmod 777 $movedatadir
	endif

	if (($endstoretype == "zigo")||($endstoretype == "score")||($endstoretype == "toritu")) then
		#--レギュラデータ以外の時の管理フォルダ作成
		if (! -d $movedatadir/after) then
			mkdir $movedatadir/after
			chmod 777 $movedatadir/after
		endif

		#--レギュラデータ以外の時の模試種類別管理フォルダ作成
		if (! -d $movedatadir/after/${storeexamstr}) then
			mkdir $movedatadir/after/${storeexamstr}
			chmod 777 $movedatadir/after/${storeexamstr}
		else
			if (-d $movedatadir/after/${storeexamstr}-old) then
				if (-d $movedatadir/after/${storeexamstr}-old2) then
					if (-d $movedatadir/after/${storeexamstr}-old3) then
						if (-d $movedatadir/after/${storeexamstr}-old4) then
							\rm -rf $movedatadir/after/${storeexamstr}-old4
						endif
						mv $movedatadir/after/${storeexamstr}-old3 $movedatadir/after/${storeexamstr}-old4
					endif
					mv $movedatadir/after/${storeexamstr}-old2 $movedatadir/after/${storeexamstr}-old3
				endif
				mv $movedatadir/after/${storeexamstr}-old $movedatadir/after/${storeexamstr}-old2
			endif
			mv $movedatadir/after/${storeexamstr} $movedatadir/after/${storeexamstr}-old
			mkdir $movedatadir/after/${storeexamstr}
			chmod 777 $movedatadir/after/${storeexamstr}
		endif

		#--移動先管理フォルダ再定義
		set movedatadir = "$enddatadir/$endyearexam/after/${storeexamstr}"
	endif

	#-- 模試コード取得(年度を表す先頭4文字を削除)
#	set endsexamcd = `echo $endyearexam | sed -e s'/^[0-9]\{4\}//'g`

	#-- データリリースJOBNET起動時のみ登録完了データファイルを移動する
	if (((-f $DLreleaseexam)&&(-d $newexamdir/${storeexamstr}))||(($examdelyear == 12)||($examdelyear == 13)||($examdelyear == 15)||($examdelyear == 16)||($examdelyear == 18)||($examdelyear == 19)||($examdelyear == 21)||($examdelyear == 22)||($examdelyear == 24)||($examdelyear == 25)||($examdelyear == 26)||($examdelyear == 27)||($examdelyear == 31)||($examdelyear == 32)||($examdelyear == 41)||($examdelyear == 42)||($examdelyear == 43))) then
		echo "登録完了データファイル移動"
		echo "mvコマンド　$newexamdir/${storeexamstr}/　${movedatadir}"
		
		set filemvlist = `ls $newexamdir/${storeexamstr}`
		set filemvcount = 1
		while ($filemvcount <= $#filemvlist)
			set filename = $filemvlist[$filemvcount]
			if ((-f ${movedatadir}/$filename)||(-d ${movedatadir}/$filename)) then
				if ((-f ${movedatadir}/${filename}-old)||(-d ${movedatadir}/${filename}-old)) then
					if ((-f ${movedatadir}/${filename}-old2)||(-d ${movedatadir}/${filename}-old2)) then
						if ((-f ${movedatadir}/${filename}-old3)||(-d ${movedatadir}/${filename}-old3)) then
							\rm -rf ${movedatadir}/${filename}-old3
						endif
						mv ${movedatadir}/${filename}-old2 ${movedatadir}/${filename}-old3
					endif
					mv ${movedatadir}/${filename}-old ${movedatadir}/${filename}-old2
				endif
				mv ${movedatadir}/$filename ${movedatadir}/${filename}-old
			endif
			mv $newexamdir/${storeexamstr}/$filename ${movedatadir}/$filename
			@ filemvcount ++
		end
		rmdir $newexamdir/${storeexamstr}


		#-- レギュラーデータ公開のみの処理
		if (($endstoretype != "zigo")&&($endstoretype != "score")&&($endstoretype != "toritu")) then
			#-- テストデータ(テストディレクトリ)の削除
			if (-d $newexamdir/${storeexamstr}test) then
				echo " テストディレクトリの削除 $newexamdir/${storeexamstr}test"
#				\rm -rf $newexamdir/${storeexamstr}test
			endif
		
			#-- 統合過年度と過年度志望大学評価別人数データ登録定義
			if (($examdelyear == 01)||($examdelyear == 02)||($examdelyear == 03)||($examdelyear == 04)||($examdelyear == 05)||($examdelyear == 06)||($examdelyear == 07)||($examdelyear == 09)||($examdelyear == 38)||($examdelyear == 66)) then
				set ratingyear = `expr $endexam - 100`
				set ratingyear2 = `expr $endexam - 200`
				echo ${ratingyear} >> $RatingStore
				echo ${ratingyear2} >> $RatingStore
#			else if (($examdelyear == 61_71)||($examdelyear == 62_72)||($examdelyear == 63_73)||($examdelyear == 65_75)) then
#			else if (($examdelyear == 61_71)||($examdelyear == 62_72)||($examdelyear == 63_73)||($examdelyear == 65)||($examdelyear == 75)) then
			else if (($examdelyear == 61)||($examdelyear == 62)||($examdelyear == 63)||($examdelyear == 71)||($examdelyear == 72)||($examdelyear == 73)||($examdelyear == 65)||($examdelyear == 74)||($examdelyear == 76)||($examdelyear == 77)) then
				set tempsy = `expr $yearstring - 1`
				set tempsy2 = `expr $yearstring - 2`
				set ratingyear = ${tempsy}${examdelyear}
				set ratingyear2 = ${tempsy2}${examdelyear}
				echo ${ratingyear} >> $RatingStore
				echo ${ratingyear2} >> $RatingStore
			else if (($examdelyear == 12)||($examdelyear == 13)||($examdelyear == 15)||($examdelyear == 16)||($examdelyear == 18)||($examdelyear == 19)||($examdelyear == 21)||($examdelyear == 22)||($examdelyear == 24)||($examdelyear == 25)||($examdelyear == 26)||($examdelyear == 27)||($examdelyear == 31)||($examdelyear == 32)||($examdelyear == 41)||($examdelyear == 42)||($examdelyear == 43)) then
				set ratingyear = `expr $endexam - 100`
				set ratingyear2 = `expr $endexam - 200`
				echo ${ratingyear} >> $OPRatingStore
				echo ${ratingyear2} >> $OPRatingStore
			
			endif
		endif
	endif

	echo "--- ${storeexamstr} 模試データ登録完了 ---"
	echo "　"
	
endif

#--登録処理対象模試の確認とJOBNET起動
if (-f $storeconff) then
	set allstoreexam = `cat $storeconff`
	\rm -f $storeconff

	echo "--- 登録対象模試 ($allstoreexam) すべてに対しJOBNET起動を開始します。"
	set counters = 1
	while ($counters <= $#allstoreexam) 

		if ($counters == 1) then
			set firstexam = $allstoreexam[$counters]
		        set yearstr = `echo $firstexam | awk -F, '{printf $1}'`
		        set examstr = `echo $firstexam | awk -F, '{printf $2}'`
		        set typestr = `echo $firstexam | awk -F, '{printf $3}'`
	
			if (($typestr == "regular")||($typestr == "release")) then
				set firstexamstr = ${yearstr}${examstr}release
			else
				set firstexamstr = ${yearstr}${examstr}${typestr}
			endif
	
			#---ログ出力用登録中模試定義ファイル作成
			echo $firstexamstr > $storinge
			echo " 登録対象データ： $firstexamstr" 
		else
			set queueexam = $allstoreexam[$counters]
	
			#---模試登録状況確認ファイル
			echo "$queueexam" >> $storeconff
			echo " 登録待機データ： $queueexam" 
		endif
		@ counters ++
	end


	#--- 登録対象ファイルを登録データ対象フォルダへ配置
	if ($typestr == "regular") then
		if (-f ${newexamdir}/${firstexamstr}/EXAMQUESTION) then
			cp -p ${newexamdir}/${firstexamstr}/EXAMQUESTION ${StoreWorkdir}
		endif
		cp -p ${newexamdir}/${firstexamstr}/EXAMTAKENUM_* ${StoreWorkdir}
		if (-f ${newexamdir}/${firstexamstr}/QRECORD_A) then
			cp -p ${newexamdir}/${firstexamstr}/QRECORD_A ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/QRECORD_P) then
			cp -p ${newexamdir}/${firstexamstr}/QRECORD_P ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/QRECORD_S) then
			cp -p ${newexamdir}/${firstexamstr}/QRECORD_S ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/QRECORD_C) then
			cp -p ${newexamdir}/${firstexamstr}/QRECORD_C ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/QRECORDZONE_A) then
			cp -p ${newexamdir}/${firstexamstr}/QRECORDZONE_A ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/QRECORDZONE) then
			cp -p ${newexamdir}/${firstexamstr}/QRECORDZONE ${StoreWorkdir}
		endif
		cp -p ${newexamdir}/${firstexamstr}/SUB* ${StoreWorkdir}
		if (-f ${newexamdir}/${firstexamstr}/RATINGNUMBER_A) then
			cp -p ${newexamdir}/${firstexamstr}/RATINGNUMBER_A ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/RATINGNUMBER_P) then
			cp -p ${newexamdir}/${firstexamstr}/RATINGNUMBER_P ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/RATINGNUMBER_S) then
			cp -p ${newexamdir}/${firstexamstr}/RATINGNUMBER_S ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/RATINGNUMBER_C) then
			cp -p ${newexamdir}/${firstexamstr}/RATINGNUMBER_C ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/UNIVMASTER_BASIC) then
			cp -p ${newexamdir}/${firstexamstr}/UNIVMASTER_BASIC ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/UNIVMASTER_CHOICESCHOOL ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/UNIVMASTER_COURSE ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/UNIVMASTER_SELECTGPCOURSE ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/UNIVMASTER_SELECTGROUP ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/UNIVMASTER_SUBJECT ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/PUB_UNIVMASTER_BASIC ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/PUB_UNIVMASTER_CHOICESCHOOL ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/PUB_UNIVMASTER_COURSE ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/PUB_UNIVMASTER_SELECTGPCOURSE ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/PUB_UNIVMASTER_SELECTGROUP ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/PUB_UNIVMASTER_SUBJECT ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/EXAMSCHEDULEDETAIL ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/UNIVCDTRANS_ORD ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/UNIVSTEMMA ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/UNIVMASTER_INFO ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/KO_UNIVMASTER_BASIC) then
			cp -p ${newexamdir}/${firstexamstr}/KO_UNIVMASTER_BASIC ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/KO_UNIVMASTER_BASIC-lastdiv) then
			cp -p ${newexamdir}/${firstexamstr}/KO_UNIVMASTER_BASIC-lastdiv ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/KO_UNIVMASTER_BASIC-lastdiv2) then
			cp -p ${newexamdir}/${firstexamstr}/KO_UNIVMASTER_BASIC-lastdiv2 ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/KO_UNIVMASTER_BASIC-lastdiv3 ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/KO_UNIVMASTER_BASIC-lastdiv4 ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/UNIVMASTER_BASIC-lastdiv) then
			cp -p ${newexamdir}/${firstexamstr}/UNIVMASTER_BASIC-lastdiv ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/UNIVMASTER_BASIC-lastyear00) then
			cp -p ${newexamdir}/${firstexamstr}/UNIVMASTER_BASIC-lastyear00 ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/KO_UNIVCDTRANS_ORD) then
			cp -p ${newexamdir}/${firstexamstr}/KO_UNIVCDTRANS_ORD ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/MARKANSWER_NO) then
			cp -p ${newexamdir}/${firstexamstr}/MARKANSWER_NO ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/MARKQSCORERATE) then
			cp -p ${newexamdir}/${firstexamstr}/MARKQSCORERATE ${StoreWorkdir}
		endif
#		if (-f ${newexamdir}/${firstexamstr}/MARKQDEVZONEANSRATE_A) then
#			cp -p ${newexamdir}/${firstexamstr}/MARKQDEVZONEANSRATE_A ${StoreWorkdir}
#		endif
		if (-f ${newexamdir}/${firstexamstr}/MARKQDEVZONEANSRATE_S) then
			cp -p ${newexamdir}/${firstexamstr}/MARKQDEVZONEANSRATE_S ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/CENTERCVSSCORE) then
			cp -p ${newexamdir}/${firstexamstr}/CENTERCVSSCORE ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/H12_UNIVMASTER) then
			cp -p ${newexamdir}/${firstexamstr}/H12_UNIVMASTER ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/JH01_KAMOKU1) then
			cp -p ${newexamdir}/${firstexamstr}/JH01_KAMOKU1 ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/JH01_KAMOKU2) then
			cp -p ${newexamdir}/${firstexamstr}/JH01_KAMOKU2 ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/JH01_KAMOKU3) then
			cp -p ${newexamdir}/${firstexamstr}/JH01_KAMOKU3 ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/JH01_KAMOKU4) then
			cp -p ${newexamdir}/${firstexamstr}/JH01_KAMOKU4 ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/KWEBSHOQUESTION) then
			cp -p ${newexamdir}/${firstexamstr}/KWEBSHOQUESTION ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/KWEBSHOQUE_ACDMC) then
			cp -p ${newexamdir}/${firstexamstr}/KWEBSHOQUE_ACDMC ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/SHOQUESTIONRECORD_A) then
			cp -p ${newexamdir}/${firstexamstr}/SHOQUESTIONRECORD_A ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/SHOQUESTIONRECORD_S) then
			cp -p ${newexamdir}/${firstexamstr}/SHOQUESTIONRECORD_S ${StoreWorkdir}
		endif
		if (-f ${newexamdir}/${firstexamstr}/ACADEMICRECORD_I_FD) then
			cp -p ${newexamdir}/${firstexamstr}/ACADEMICRECORD_I_FD ${StoreWorkdir}
		endif

		#-- 模試公開時にマスタを入れ直すため
		if (($examstr == 01)||($examstr == 02)||($examstr == 03)||($examstr == 04)||($examstr == 05)||($examstr == 06)) then
			set cpafteryear = `expr $yearstr - 1`
			cp -p ${newexamdir}/${firstexamstr}/UNIVMASTER_BASIC ${newexamdir}/${cpafteryear}${examstr}
#			cp -p ${newexamdir}/${firstexamstr}/UNIVCDTRANS_ORD ${newexamdir}/${cpafteryear}
		endif
		
	else if ($typestr == "release") then
#		if (($examstr != 01)&&($examstr != 02)&&($examstr != 03)&&($examstr != 04)&&($examstr != 05)&&($examstr != 06)&&($examstr != 07)&&($examstr != 09)&&($examstr != 61_71)&&($examstr != 62_72)&&($examstr != 63_73)&&($examstr != 66)&&($examstr != 65_75)) then
#		if (($examstr != 01)&&($examstr != 02)&&($examstr != 03)&&($examstr != 04)&&($examstr != 05)&&($examstr != 06)&&($examstr != 07)&&($examstr != 09)&&($examstr != 61_71)&&($examstr != 62_72)&&($examstr != 63_73)&&($examstr != 66)&&($examstr != 65)&&($examstr != 75)) then
		if (($examstr != 01)&&($examstr != 02)&&($examstr != 03)&&($examstr != 04)&&($examstr != 05)&&($examstr != 06)&&($examstr != 07)&&($examstr != 09)&&($examstr != 61)&&($examstr != 62)&&($examstr != 63)&&($examstr != 71)&&($examstr != 72)&&($examstr != 73)&&($examstr != 66)&&($examstr != 65)&&($examstr != 74)) then
			#-- 数字からはじまるフォルダ以外をコピー
#			cp -p ${newexamdir}/${firstexamstr}/* ${StoreWorkdir}
			cp -p ${newexamdir}/${firstexamstr}/[^0-9]* ${StoreWorkdir}
			if (-f ${StoreWorkdir}/EXAMSUBJECT) then
				\rm -f ${StoreWorkdir}/EXAMSUBJECT*
			endif
		else
#			cp -p ${newexamdir}/${firstexamstr}/INDIVIDUALRECORD ${StoreWorkdir}
			mv ${newexamdir}/${firstexamstr}/INDIVIDUALRECORD-pact ${StoreWorkdir}/INDIVIDUALRECORD
			mv ${newexamdir}/${firstexamstr}/INDIVIDUALRECORD-nopact ${StoreWorkdir}
			if (-f ${newexamdir}/${firstexamstr}/PREVSUCCESS) then
				cp -p ${newexamdir}/${firstexamstr}/PREVSUCCESS ${StoreWorkdir}
			endif
		endif
	else
		echo "ファイルコピー ${newexamdir}/${firstexamstr}/ ${StoreWorkdir}"
		cp -p ${newexamdir}/${firstexamstr}/* ${StoreWorkdir}
	endif

	#--- ログ出力
	/keinavi/JOBNET/storelog.sh
	
	#--- JOBNET起動(メッセージ事象発生)
	if ((-f $DLreleaseexam)&&(! -f $DLstatusfile)) then
		echo " --- 模試別・統計集データ展開 JOBNET起動(メッセージ事象発生)"
		jobschmsgevent DLUp-START -h satu04
	endif
	
	echo " --- データ登録 JOBNET起動(メッセージ事象発生)　模試：${firstexam}"
	jobschmsgevent DataUp-START -h satu04

	exit 10
endif


#--- JOBNET起動(メッセージ事象発生)
if (-f $checkstatistics) then
	echo " --- 統計集データ作成 JOBNET起動(メッセージ事象発生)"
	jobschmsgevent Statis-START -h satu04
endif

if (-f $OPRatingStore) then
	echo " --- 過年度の志望大学評価別人数データ登録 JOBNET起動(メッセージ事象発生)"
	jobschmsgevent BeRat-START -h satu04
endif



if (-f $DLreleaseexam) then
	\rm -f $DLreleaseexam
endif
if (-f $DLstatusfile) then
	\rm -f $DLstatusfile
endif

if (-f $storinge) then
	\rm -f $storinge
endif
if (-f $storelock) then
	\rm -f $storelock
endif
exit 0

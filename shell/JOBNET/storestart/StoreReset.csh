#! /bin/csh


### JOBNET処理の初期化（処理異常で生成ファイルなどが残ってしまったとき）

#echo "--- JOBNET処理が異常終了しているため生成ファイルなどを削除します。---"
#echo " "
echo "JOBNET処理の初期化開始..."
echo " "

set lists = "/keinavi/JOBNET/storestart/conf.txt"
set path2 = "/home/navikdc/datastore/inside_release"
set path3 = "/home/navikdc/datastore/release"


#--- データ登録対象模試定義ファイル
set rmtargetfile = `cat $lists`

set rmcount = 1
while ($rmcount <= $#rmtargetfile) 
	set filepath = `echo $rmtargetfile[$rmcount] | awk -F, '{print $1}'`
	set filename = `echo $rmtargetfile[$rmcount] | awk -F, '{print $2}'`

	if (-f ${filepath}) then
		echo " ${filepath}  [${filename}] を削除"
		\rm -f ${filepath}
	endif

	@ rmcount ++
end
echo " "
echo "JOBNET処理の初期化終了"
echo " "
#echo "--- JOBNET処理の初期化によりJOBNET起動が可能になりました。---"



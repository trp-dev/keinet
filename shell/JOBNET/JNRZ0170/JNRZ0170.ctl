------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 設問成績層別成績（高校）
--  テーブル名      : QRECORDZONE
--  データファイル名: QRECORDZONE
--  機能            : 設問成績層別成績（高校）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE QRECORDZONE_TMP
(  
   EXAMYEAR        POSITION(01:04)  CHAR,
   EXAMCD          POSITION(05:06)  CHAR,
   BUNDLECD        POSITION(07:11)  CHAR,
   SUBCD           POSITION(12:15)  CHAR,
   QUESTION_NO     POSITION(16:17)  INTEGER EXTERNAL,
   DEVZONECD       POSITION(18:18)  CHAR,
   AVGPNT          POSITION(19:23)  INTEGER EXTERNAL "DECODE(:AVGPNT,         '99999', null, :AVGPNT/10)",
   NUMBERS         POSITION(24:31)  INTEGER EXTERNAL "NULLIF(:NUMBERS,     '99999999')",
   AVGSCORERATE    POSITION(32:35)  INTEGER EXTERNAL "DECODE(:AVGSCORERATE,    '9999', null, :AVGSCORERATE/10)"
)


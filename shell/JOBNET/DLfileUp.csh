#! /bin/csh

set setexamfile = "/keinavi/freemenu/examup/setexamfile.txt"
set upyear = `awk -F, '{print $1}' $setexamfile`
set upexam = `awk -F, '{print $2}' $setexamfile`
set freedir = "/keinavi/freemenu"
set freemenuF = "/keinavi/bat/FreeMenu"

set counters = 1
while ($counters <= $#upexam)

	#--- 高校別総合成績一覧表
	if (-d ${freemenuF}/F002_1/allresult/$upyear[$counters]/$upexam[$counters]) then
		if (-d ${freedir}/allresult_stop/$upyear[$counters]/$upexam[$counters]) then
			\rm -rf ${freedir}/allresult_stop/$upyear[$counters]/$upexam[$counters]
		endif
		echo "--- 統計集（高校別総合成績一覧表）データ $upyear[$counters]$upexam[$counters] の本番機展開実行 "
		if (! -d ${freedir}/allresult_stop/$upyear[$counters]) then
			mkdir ${freedir}/allresult_stop/$upyear[$counters]
			chmod 775 ${freedir}/allresult_stop/$upyear[$counters]
		endif
		mv ${freemenuF}/F002_1/allresult/$upyear[$counters]/$upexam[$counters] ${freedir}/allresult_stop/$upyear[$counters]
		echo "統計集（高校別総合成績一覧表）データ の本番機展開正常終了 ---"

		#--- 過年度(２年前)データの削除
		set oldtwoyear = `expr $upyear[$counters] - 2`
		if (-d ${freedir}/allresult_stop/${oldtwoyear}/$upexam[$counters]) then
			\rm -rf ${freedir}/allresult_stop/${oldtwoyear}/$upexam[$counters]
			echo "--- 過年度 統計集削除 allresult/${oldtwoyear}/$upexam[$counters] ---"
		endif
	endif

	#--- 型/科目別・高校別成績一覧表
	if (-d ${freemenuF}/F002_2/allpattern/$upyear[$counters]/$upexam[$counters]) then
		if (-d ${freedir}/allpattern_stop/$upyear[$counters]/$upexam[$counters]) then
			\rm -rf ${freedir}/allpattern_stop/$upyear[$counters]/$upexam[$counters]
		endif
		echo "--- 統計集（型/科目別・高校別成績一覧表）データ $upyear[$counters]$upexam[$counters] の本番機展開実行 "
		if (! -d ${freedir}/allpattern_stop/$upyear[$counters]) then
			mkdir ${freedir}/allpattern_stop/$upyear[$counters]
			chmod 775 ${freedir}/allpattern_stop/$upyear[$counters]
		endif
		mv ${freemenuF}/F002_2/allpattern/$upyear[$counters]/$upexam[$counters] ${freedir}/allpattern_stop/$upyear[$counters]
		echo "統計集（型/科目別・高校別成績一覧表）データ の本番機展開正常終了 ---"

		#--- 過年度(２年前)データの削除
		set oldtwoyear = `expr $upyear[$counters] - 2`
		if (-d ${freedir}/allpattern_stop/${oldtwoyear}/$upexam[$counters]) then
			\rm -rf ${freedir}/allpattern_stop/${oldtwoyear}/$upexam[$counters]
			echo "--- 過年度 統計集削除 allpattern/${oldtwoyear}/$upexam[$counters] ---"
		endif
	endif

	#--- 高校別設問別成績一覧表
	if (-d ${freemenuF}/F002_3/question/$upyear[$counters]/$upexam[$counters]) then
		if (-d ${freedir}/question_stop/$upyear[$counters]/$upexam[$counters]) then
			\rm -rf ${freedir}/question_stop/$upyear[$counters]/$upexam[$counters]
		endif
		echo "--- 統計集（高校別設問別成績一覧表）データ $upyear[$counters]$upexam[$counters] の本番機展開実行 "
		if (! -d ${freedir}/question_stop/$upyear[$counters]) then
			mkdir ${freedir}/question_stop/$upyear[$counters]
			chmod 775 ${freedir}/question_stop/$upyear[$counters]
		endif
		mv ${freemenuF}/F002_3/question/$upyear[$counters]/$upexam[$counters] ${freedir}/question_stop/$upyear[$counters]
		echo "統計集（高校別設問別成績一覧表）データ の本番機展開正常終了 ---"

		#--- 過年度(２年前)データの削除
		set oldtwoyear = `expr $upyear[$counters] - 2`
		if (-d ${freedir}/question_stop/${oldtwoyear}/$upexam[$counters]) then
			\rm -rf ${freedir}/question_stop/${oldtwoyear}/$upexam[$counters]
			echo "--- 過年度 統計集削除 question/${oldtwoyear}/$upexam[$counters] ---"
		endif
	endif
	
	#--- 国公立大学学部別・高校別志望状況
	if (-d ${freemenuF}/F002_4/allcandidate/$upyear[$counters]/$upexam[$counters]) then
		if (-d ${freedir}/allcandidate_stop/$upyear[$counters]/$upexam[$counters]) then
			\rm -rf ${freedir}/allcandidate_stop/$upyear[$counters]/$upexam[$counters]
		endif
		echo "--- 統計集（国公立大学学部別・高校別志望状況）データ $upyear[$counters]$upexam[$counters] の本番機展開実行 "
		if (! -d ${freedir}/allcandidate_stop/$upyear[$counters]) then
			mkdir ${freedir}/allcandidate_stop/$upyear[$counters]
			chmod 775 ${freedir}/allcandidate_stop/$upyear[$counters]
		endif
		mv ${freemenuF}/F002_4/allcandidate/$upyear[$counters]/$upexam[$counters] ${freedir}/allcandidate_stop/$upyear[$counters]
		echo "統計集（国公立大学学部別・高校別志望状況）データ の本番機展開正常終了 ---"

		#--- 過年度(２年前)データの削除
		set oldtwoyear = `expr $upyear[$counters] - 2`
		if (-d ${freedir}/allcandidate_stop/${oldtwoyear}/$upexam[$counters]) then
			\rm -rf ${freedir}/allcandidate_stop/${oldtwoyear}/$upexam[$counters]
			echo "--- 過年度 統計集削除 allcandidate/${oldtwoyear}/$upexam[$counters] ---"
		endif
	endif
	
	#--- 志望大学学部学科別成績分布
	if (-d ${freemenuF}/F002_5/univresult/$upyear[$counters]/$upexam[$counters]) then
		if (-d ${freedir}/univresult/$upyear[$counters]/$upexam[$counters]) then
			\rm -rf ${freedir}/univresult/$upyear[$counters]/$upexam[$counters]
		endif
		echo "--- 統計集（志望大学学部学科別成績分布）データ $upyear[$counters]$upexam[$counters] の本番機展開実行 "
		if (! -d ${freedir}/univresult/$upyear[$counters]) then
			mkdir ${freedir}/univresult/$upyear[$counters]
			chmod 775 ${freedir}/univresult/$upyear[$counters]
		endif
		mv ${freemenuF}/F002_5/univresult/$upyear[$counters]/$upexam[$counters] ${freedir}/univresult/$upyear[$counters]
		echo "統計集（志望大学学部学科別成績分布）データ の本番機展開正常終了 ---"

		#--- 過年度(２年前)データの削除
		set oldtwoyear = `expr $upyear[$counters] - 2`
		if (-d ${freedir}/univresult/${oldtwoyear}/$upexam[$counters]) then
			\rm -rf ${freedir}/univresult/${oldtwoyear}/$upexam[$counters]
			echo "--- 過年度 統計集削除 univresult/${oldtwoyear}/$upexam[$counters] ---"
		endif
	endif

	#--- ここで模試別成績データダウンロードのデータファイルフォルダを削除する
	if (-f ${freedir}/examup/$upyear[$counters]/$upexam[$counters].tar.Z) then
		echo "--- 模試別成績データダウンロード展開用データフォルダ削除 $upyear[$counters]$upexam[$counters].tar.Z "
		rm -rf ${freedir}/examup/$upyear[$counters]/$upexam[$counters].tar.Z

		##--- 受験学力測定用調査回答データ
		if (-f ${freedir}/examup/$upyear[$counters]/$upexam[$counters]_1.tar.Z) then
			rm -rf ${freedir}/examup/$upyear[$counters]/$upexam[$counters]_1.tar.Z
		endif
		if (-f ${freedir}/examup/$upyear[$counters]/$upexam[$counters]_2.tar.Z) then
			rm -rf ${freedir}/examup/$upyear[$counters]/$upexam[$counters]_2.tar.Z
		endif
	endif

	@ counters ++
end

rm -f $setexamfile

-- ************************************************
-- *機 能 名 : 高１２大学マスタ
-- *機 能 ID : JNRZ0541_01.sql
-- *作 成 日 : 2009/12/08
-- *作 成 者 : KCN 
-- *更 新 日 : 
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE H12_UNIVMASTER_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE H12_UNIVMASTER_TMP  (
       EVENTYEAR                  CHAR(4)              NOT NULL,    /*   行事年度             */ 
       EXAMDIV                    CHAR(2)              NOT NULL,    /*   模試区分             */ 
       SCHOOLDIV                  CHAR(1)                      ,    /*    */ 
       UNIDIV                     CHAR(2)                      ,    /*    */ 
       UNIVCD                     CHAR(4)              NOT NULL,    /*    */ 
       FACULTYCD                  CHAR(2)              NOT NULL,    /*    */ 
       DEPTCD                     CHAR(4)              NOT NULL,    /*    */ 
       CHOICECD5                  CHAR(5)                      ,    /*    */ 
       CDEXAMDIV                  CHAR(2)                      ,    /*    */ 
       CDVALIDEXAMDIV             CHAR(2)                      ,    /*    */ 
       ENTEXAMMODE                CHAR(1)                      ,    /*    */ 
       UNIGDIV                    CHAR(1)                      ,    /*    */ 
       APPLILIMITDIV              CHAR(1)                      ,    /*    */ 
       FACULTYCONCD               CHAR(3)                      ,    /*    */ 
       DEPTSERIAL_NO              CHAR(4)                      ,    /*    */ 
       LDISTRICTCD                CHAR(1)                      ,    /*    */ 
       BUSINESSCD                 CHAR(3)                      ,    /*    */ 
       PREFCD_EXAMSH              CHAR(2)                      ,    /*    */ 
       PREFCD_EXAMHO              CHAR(2)                      ,    /*    */ 
       ENTEXAMSTEMMACD_M          CHAR(2)                      ,    /*    */ 
       ENTEXAMSTEMMACD_S          CHAR(20)                     ,    /*    */ 
       BUNRICD                    CHAR(1)                      ,    /*    */ 
       UNIVNAME                   VARCHAR2(18)                 ,    /*    */ 
       FACULTYNAME                VARCHAR2(14)                 ,    /*    */ 
       DEPTNAME                   VARCHAR2(16)                 ,    /*    */ 
       UNINAME_ABBR               VARCHAR2(14)                 ,    /*    */ 
       FACULTYNAME_ABBR           VARCHAR2(10)                 ,    /*    */ 
       DEPTNAME_ABBR              VARCHAR2(12)                 ,    /*    */ 
       UNIVNAME_KANA              VARCHAR2(12)                 ,    /*    */ 
       FACULTYNAME_KANA           VARCHAR2(8)                  ,    /*    */ 
       DEPTNAME_KANA              VARCHAR2(10)                 ,    /*    */ 
       PUBENTEXAMCAPA             NUMBER(7,0)                  ,    /*    */ 
       EXAMCAPAREL                CHAR(1)                      ,    /*    */ 
       UNICOEDDIV                 CHAR(1)                      ,    /*    */ 
       UNINIGHTDIV                CHAR(1)                      ,    /*    */ 
       UNIGNAME                   VARCHAR2(2)                  ,    /*    */ 
       ENTEXAMORNDIV              CHAR(3)                      ,    /*    */ 
       PUBREJECTCOMP              NUMBER(4,2)                  ,    /*    */ 
       PUBREJECTEXSCORE           NUMBER(4,0)                  ,    /*    */ 
       ENTEXAMRANK                CHAR(2)                      ,    /*    */ 
       ENTEXAMRANK_DEVI           NUMBER(5,1)                  ,    /*    */ 
       BORDERSCORE1               NUMBER(5,0)                  ,    /*    */ 
       BORDERSCORE2               NUMBER(5,0)                  ,    /*    */ 
       BORDERSCORE3               NUMBER(5,0)                  ,    /*    */ 
       BORDERSCORERATE1           NUMBER(3,0)                  ,    /*    */ 
       BORDERSCORERATE2           NUMBER(3,0)                  ,    /*    */ 
       BORDERSCORERATE3           NUMBER(3,0)                  ,    /*    */ 
       REJECTEXSCORE              NUMBER(5,0)                  ,    /*    */ 
       REJECTSCORERATE            NUMBER(3,0)                  ,    /*    */ 
       DOCKINGNEEDLDIV            CHAR(1)                      ,    /*    */ 
       REP_MSTEMMA                CHAR(2)                      ,    /*    */ 
       REP_SSTEMMA                CHAR(20)                     ,    /*    */ 
       REP_REGION                 CHAR(2)                      ,    /*    */ 
       REP_BRANCHCD               CHAR(2)                      ,    /*    */ 
       REGISTREORDIV              CHAR(1)                      ,    /*    */ 
       SCHEDULESYS                CHAR(1)                      ,    /*    */ 
       SCHEDULESYSBRANCHCD        CHAR(1)                      ,    /*    */ 
       SCHEDULESYSNAME            VARCHAR2(48)                 ,    /*    */ 
       LMAINDIV                   CHAR(1)                      ,    /*    */ 
       ARTUNIDIV                  CHAR(1)                      ,    /*    */ 
       COURSEALLOTPNT_EN_CENTER   NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_MA_CENTER   NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_JA_CENTER   NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_SC_CENTER   NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_SO_CENTER   NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_ET_CENTER   NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_EN_SECOND   NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_MA_SECOND   NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_JA_SECOND   NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_SC_SECOND   NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_SO_SECOND   NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_ET_SECOND   NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_EN_FIRST    NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_MA_FIRST    NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_JA_FIRST    NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_SC_FIRST    NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_SO_FIRST    NUMBER(5,1)                  ,    /*    */ 
       COURSEALLOTPNT_ET_FIRST    NUMBER(5,1)                  ,    /*    */ 
       PUBPRIUNIDIV               CHAR(1)                      ,    /*    */ 
       BORDERINPNEEDLDIV          CHAR(1)                      ,    /*    */ 
       RANKINPNEEDLDIV            CHAR(1)                      ,    /*    */ 
       EXAMUSEDIV                 CHAR(1)                      ,    /*    */ 
       RESEACHUSEDIV              CHAR(1)                      ,    /*    */ 
       PURSUEUSEDIV               CHAR(1)                      ,    /*    */ 
       DEPTSORTKEY                CHAR(39)                     ,    /*    */ 
       CONSTRAINT PK_H12_UNIVMASTER_TMP PRIMARY KEY (
                 EVENTYEAR                 ,               /*  年度        */
                 EXAMDIV                   ,               /*  模試区分    */
                 UNIVCD                    ,               /*  大学コード  */
                 FACULTYCD                 ,               /*  学部コード  */
                 DEPTCD                                    /*  学科コード  */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

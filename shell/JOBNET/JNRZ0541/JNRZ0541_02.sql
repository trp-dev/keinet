-- *********************************************************
-- *機 能 名 : 高１２大学マスタ
-- *機 能 ID : JNRZ0541_02.sql
-- *作 成 日 : 2009/12/08
-- *更 新 日 : 
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM H12_UNIVMASTER
WHERE  (EVENTYEAR,EXAMDIV)
       IN(SELECT DISTINCT EVENTYEAR,EXAMDIV
          FROM   H12_UNIVMASTER_TMP);

-- 不要レコードをまず削除
DELETE FROM H12_UNIVMASTER_TMP
WHERE EXAMUSEDIV='0';

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO H12_UNIVMASTER
SELECT * FROM H12_UNIVMASTER_TMP;

-- 8年以上前のデータを削除
DELETE FROM H12_UNIVMASTER
WHERE  TO_NUMBER(EVENTYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 8);

-- 更新対象データの7年前のデータを削除
DELETE FROM H12_UNIVMASTER
WHERE  (EVENTYEAR,EXAMDIV)
       IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 7)),EXAMDIV
          FROM   H12_UNIVMASTER_TMP);

-- テンポラリテーブル削除
DROP TABLE H12_UNIVMASTER_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

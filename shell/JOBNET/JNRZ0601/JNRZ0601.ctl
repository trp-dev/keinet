------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 大学コード変換順引きマスタ
--  テーブル名      : UNIVCDTRANS_ORD
--  データファイル名: UNIVCDTRANS_ORD
--  機能            : 大学コード変換順引きマスタをデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 2009/12/08
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE UNIVCDTRANS_ORD_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   YEAR           POSITION(01:04)      CHAR "DECODE(:EXAMCD, '00', :YEAR-1, :YEAR)",
   OLDUNIV8CD     POSITION(05:14)      CHAR,
   EXAMCD         POSITION(15:16)      CHAR "DECODE(:EXAMCD, '00', '09', :EXAMCD)",
   NEWUNIV8CD     POSITION(17:26)      CHAR
)

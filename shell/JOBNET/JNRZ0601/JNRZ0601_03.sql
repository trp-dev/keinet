-- *********************************************************
-- *機 能 名 : 大学紐付けデータ作成
-- *機 能 ID : JNRZ0601_03.sql
-- *作 成 日 : 2004/10/04
-- *更 新 日 : 2020/09/17
-- *更新内容 :
-- *********************************************************

-- PL/SQLブロックからの表示をONにする
SET ECHO ON
SET LINESIZE 200
SET SERVEROUTPUT ON

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 移行データの名寄せ処理
DECLARE
    c_examyear1         CHAR(4);                -- １つ前の模試年度
    c_examdiv1          CHAR(2);                -- １つ前の模試区分
    new_univ8cd         CHAR(10);               -- 紐付けマスタ最新大学コード検索用

-- メイン処理
BEGIN
    -- 大学コード順引きマスタ読み込み
    DBMS_OUTPUT.PUT_LINE('大学コード紐付け処理開始');
    FOR r_cur IN (SELECT YEAR,OLDUNIV8CD,EXAMCD,NEWUNIV8CD
                  FROM   UNIVCDTRANS_ORD_TMP
                  WHERE  (OLDUNIV8CD != '9999999999'))
    LOOP

    -- ログ出力
    DBMS_OUTPUT.PUT_LINE('>> 対象データ' ||
                               ' [年度='             || TO_CHAR(r_cur.YEAR)       ||
                               '][模試区分='         || TO_CHAR(r_cur.EXAMCD)     ||
                               '][変更前大学コード=' || TO_CHAR(r_cur.OLDUNIV8CD) ||
                               '][変更後大学コード=' || TO_CHAR(r_cur.NEWUNIV8CD) || ']');

    -- 大学コード紐付けマスタ検索処理
    BEGIN
        -- 大学コード紐付けマスタ検索
        SELECT DISTINCT NEWUNIV8CD
        INTO   new_univ8cd
        FROM   UNIVCDHOOKUP
        WHERE  NEWUNIV8CD = r_cur.OLDUNIV8CD;

        -- 大学コードの更新
        DBMS_OUTPUT.PUT_LINE('更新対象データあり');
        DBMS_OUTPUT.PUT_LINE('紐付けデータ更新 [' || TO_CHAR(r_cur.OLDUNIV8CD) ||
                                   '] -> [' || TO_CHAR(r_cur.NEWUNIV8CD) || ']');

        -- 該当レコードがあれば全ての最新大学コードを変更後大学コードに更新する
        FOR r_cur_hookup IN (
            SELECT
                YEAR,
                OLDEXAMDIV,
                OLDUNIV8CD
            FROM
                UNIVCDHOOKUP
            WHERE 
                NEWUNIV8CD = r_cur.OLDUNIV8CD
        )
        LOOP
            BEGIN
                -- 変更後大学コードに更新する
                UPDATE UNIVCDHOOKUP SET
                    NEWUNIV8CD  = r_cur.NEWUNIV8CD
                WHERE
                    YEAR = r_cur_hookup.YEAR
                AND
                    OLDEXAMDIV = r_cur_hookup.OLDEXAMDIV
                AND
                    OLDUNIV8CD = r_cur_hookup.OLDUNIV8CD
                AND
                    NEWUNIV8CD = r_cur.OLDUNIV8CD;
            EXCEPTION
                -- UPDATE時のキー重複
                WHEN DUP_VAL_ON_INDEX THEN

                    DBMS_OUTPUT.PUT_LINE('重複データあり');
                    DBMS_OUTPUT.PUT_LINE(
                        '>> 対象データ削除' || ' [年度=' || TO_CHAR(r_cur_hookup.YEAR) || ']'
                                        || ' [模試区分=' || TO_CHAR(r_cur_hookup.OLDEXAMDIV) || ']'
                                        || ' [変更前大学コード=' || TO_CHAR(r_cur_hookup.OLDUNIV8CD) || ']'
                                        || ' [変更後大学コード=' || TO_CHAR(r_cur.OLDUNIV8CD) || ']'
                    );
                    -- 重複レコードを削除
                    DELETE FROM
                        UNIVCDHOOKUP
                    WHERE
                        YEAR = r_cur_hookup.YEAR
                    AND 
                        OLDEXAMDIV = r_cur_hookup.OLDEXAMDIV
                    AND 
                        OLDUNIV8CD = r_cur_hookup.OLDUNIV8CD
                    AND 
                        NEWUNIV8CD = r_cur.OLDUNIV8CD;
            END;
        END LOOP;

    EXCEPTION
        -- 紐付けマスタを作成
        WHEN NO_DATA_FOUND THEN
            -- １つ前の年度と模試区分を取得
            c_examyear1 := r_cur.YEAR;
            c_examdiv1  := r_cur.EXAMCD;
            IF c_examdiv1='00' THEN
                c_examyear1 := LTRIM(TO_CHAR(TO_NUMBER(c_examyear1) - 1, '0999'));
                c_examdiv1  := '09';
            ELSE
                c_examdiv1  := LTRIM(TO_CHAR(TO_NUMBER(c_examdiv1) - 1, '09'));
            END IF;

            -- 紐付けマスタデータを作成
            DBMS_OUTPUT.PUT_LINE('更新対象データなし(１つ前の紐付けマスタ作成)');
            DBMS_OUTPUT.PUT_LINE('紐付けデータ作成' ||
                               ' [年度='             || TO_CHAR(c_examyear1)      ||
                               '][模試区分='         || TO_CHAR(c_examdiv1)       ||
                               '][変更前大学コード=' || TO_CHAR(r_cur.OLDUNIV8CD) ||
                               '][変更後大学コード=' || TO_CHAR(r_cur.NEWUNIV8CD) || ']');

            INSERT INTO UNIVCDHOOKUP
            VALUES (
                c_examyear1,
                c_examdiv1,
                r_cur.OLDUNIV8CD,
                r_cur.NEWUNIV8CD
            );

        -- エラーEXCEPTIONを親に投げる
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('２つ前の紐付けマスタ検索処理エラー発生');
            RAISE;
    END;

    -- 移行データループ
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('大学コード紐付け処理が正常に終了しました');

-- メイン処理：例外処理
EXCEPTION
    -- エラーEXCEPTIONを親に投げる
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('!!! 大学コード紐付け処理にてエラーが発生しました !!!');
        RAISE;
END;
/

-- 更新対象データの1年前のデータを削除
DECLARE
    c_examyear1         CHAR(4);                -- １つ前の模試年度
    c_examdiv1          CHAR(2);                -- １つ前の模試区分

-- メイン処理
BEGIN

    -- 大学コード順引きマスタ読み込み
    FOR r_cur IN (SELECT DISTINCT YEAR,EXAMCD
                  FROM   UNIVCDTRANS_ORD_TMP
                  WHERE  (OLDUNIV8CD != '9999999999'))
    LOOP

    -- 大学コード紐付けマスタ検索処理
    BEGIN

        -- １つ前の年度と模試区分を取得
        c_examyear1 := r_cur.YEAR;
        c_examdiv1  := r_cur.EXAMCD;
        IF c_examdiv1='00' THEN
            c_examyear1 := LTRIM(TO_CHAR(TO_NUMBER(c_examyear1) - 1, '0999'));
            c_examdiv1  := '09';
        ELSE
            c_examdiv1  := LTRIM(TO_CHAR(TO_NUMBER(c_examdiv1) - 1, '09'));
        END IF;

        -- 更に1年前を求める
        c_examyear1 := LTRIM(TO_CHAR(TO_NUMBER(c_examyear1) - 1, '0999'));

        -- 更新対象データの1年前のデータを削除
        DBMS_OUTPUT.PUT_LINE('削除対象 [年度=' || TO_CHAR(c_examyear1) || '] [模試区分=' || TO_CHAR(c_examdiv1) || ']' );
        DELETE FROM UNIVCDHOOKUP
        WHERE  (YEAR=c_examyear1 AND OLDEXAMDIV=c_examdiv1);

    EXCEPTION
        -- エラーEXCEPTIONを無視
        WHEN OTHERS THEN
            NULL;
    END;

    -- 移行データループ
    END LOOP;
END;
/

-- 2年以上前のデータを削除
DELETE FROM UNIVCDHOOKUP
WHERE  TO_NUMBER(YEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 2);

-- テンポラリテーブル削除
DROP TABLE UNIVCDTRANS_ORD_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

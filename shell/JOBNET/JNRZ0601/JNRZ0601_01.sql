-- ************************************************
-- *機 能 名 : 大学コード変換順引きマスタ
-- *機 能 ID : JNRZ0601_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 : 2009/12/08
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE UNIVCDTRANS_ORD_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE UNIVCDTRANS_ORD_TMP  (
       YEAR               CHAR(4)        NOT NULL,    /*     年度                      */ 
       OLDUNIV8CD         CHAR(10)       NOT NULL,    /*     変更前10桁大学コード      */ 
       EXAMCD             CHAR(2)        NOT NULL,    /*     変更時模試区分            */ 
       NEWUNIV8CD         CHAR(10)       NOT NULL,    /*     変更後10桁大学コード      */ 
       CONSTRAINT PK_UNIVCDTRANS_ORD_TMP PRIMARY KEY (
                 YEAR                  ,               /*  年度                 */
                 OLDUNIV8CD            ,               /*  変更前10桁大学コード */
                 EXAMCD                ,               /*  変更時模試区分       */
                 NEWUNIV8CD                            /*  変更後10桁大学コード */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

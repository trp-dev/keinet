#! /bin/csh 

#### 大学マスタ関連ファイルを検証機へ送信するバッチ
#### （対象は下記変数定義パス 配下の各マスタの最新ファイル）

####　大学マスタのファイル先のパスを指定する (設定項目) #######
set targetplace = "/keinavi/HULFT/NewEXAM"


set place = "/keinavi/JOBNET/JN_UnivDataUpdate2"
cd $place

set TargetFiles = (SUBRECORD_A UNIVMASTER_BASIC UNIVMASTER_CHOICESCHOOL UNIVMASTER_COURSE UNIVMASTER_SUBJECT UNIVMASTER_SELECTGROUP UNIVMASTER_SELECTGPCOURSE EXAMSCHEDULEDETAIL UNIVSTEMMA)
set TargetFiD = (RZRG1110 RZRG5110 RZRG5120 RZRG5130 RZRG5140 RZRG5160 RZRG5190 RZRG5180 RZRG5170)

set count = 1
set fcheck = 0

while ($count <= $#TargetFiles)

	(ls -t $targetplace/$TargetFiles[$count]* > out.log) >& /dev/null 
	set FileList=`cat out.log` 
	if ($#FileList == 0) then

		(ls -t $targetplace/Univmaster > out.log) >& /dev/null
		set dirList=`cat out.log`
		if ($#dirList == 0) then

	        	echo "***** $TargetFiles[$count] $TargetFiD[$count] データファイルが見つかりません *****"
      			@ count ++
			@ fcheck ++
			continue
		endif

		(ls -t $targetplace/Univmaster/$dirList[1]/$TargetFiles[$count]* > out.log) >& /dev/null
		set FileList=`cat out.log`
		if ($#FileList == 0) then
	        	echo "***** $TargetFiles[$count] $TargetFiD[$count] データファイルが見つかりません *****"
      			@ count ++
			@ fcheck ++
			continue
		endif
			
	endif
	set StoreFullFile = $FileList[1]
	cp -p $StoreFullFile $TargetFiles[$count]
	set StoreFile = $TargetFiles[$count]
	echo "---- 対象ファイル：$StoreFullFile を $TargetFiles[$count] として送信"
	if (($TargetFiles[$count] == UNIVMASTER_CHOICESCHOOL)||($TargetFiles[$count] == UNIVMASTER_COURSE)||($TargetFiles[$count] == UNIVMASTER_SUBJECT)||($TargetFiles[$count] == UNIVMASTER_SELECTGROUP)||($TargetFiles[$count] == UNIVMASTER_SELECTGPCOURSE)||($TargetFiles[$count] == PUB_UNIVMASTER_CHOICESCHOOL)||($TargetFiles[$count] == PUB_UNIVMASTER_COURSE)||($TargetFiles[$count] == PUB_UNIVMASTER_SUBJECT)||($TargetFiles[$count] == PUB_UNIVMASTER_SELECTGROUP)||($TargetFiles[$count] == PUB_UNIVMASTER_SELECTGPCOURSE)) then
		set string1=`head -1 $StoreFile | awk '{printf "%-.17s\n",$0}'`
		set year=`echo $string1 | awk '{printf "%-.4s\n",$0}'`
		set exam=`expr $string1 : ".*\(..\)"`
		echo "対象年度・模試区分：$year$exam"
	else if (($TargetFiles[$count] == UNIVMASTER_BASIC)||($TargetFiles[$count] == PUB_UNIVMASTER_BASIC)) then
		set string1=`head -1 $StoreFile | awk '{printf "%-.6s\n",$0}'`
		set year=`echo $string1 | awk '{printf "%-.4s\n",$0}'`
		set exam=`expr $string1 : ".*\(..\)"`
		echo "対象年度・模試区分：$string1"

		#--- データ登録時に紐付き処理をしないようにファイル名変更
		mv $TargetFiles[$count] $TargetFiles[$count]-lastyear00
		set StoreFile = $TargetFiles[$count]-lastyear00
	endif

	#-- orauser にてファイルを転送する
	/keinavi/HULFT/ftp.sh /keinavi/HULFT/WorkDir/ $place $StoreFile 
	\rm $StoreFile

	@ count ++
end

\rm out.log

if ($fcheck != 0) then
	exit 30
endif

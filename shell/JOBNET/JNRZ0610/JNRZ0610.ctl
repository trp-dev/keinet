------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 判定イレギュラファイル
--  テーブル名      : IRREGULARFILE
--  データファイル名: IRREGULARFILE
--  機能            : 判定イレギュラファイルをデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE IRREGULARFILE_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   EXAMYEAR                POSITION(01:04)        CHAR,
   EXAMDIV                 POSITION(05:05)        CHAR "CONCAT('0', :EXAMDIV)",
   SCHOOLDIV               POSITION(06:06)        CHAR,
   UNIVCD8                 POSITION(07:14)        CHAR,
   ENTEXAMDIV              POSITION(15:15)        CHAR,
   BRANCHCD                POSITION(16:17)        CHAR,
   IRREGULARDIV            POSITION(18:18)        CHAR,
   IRREGULARCOURSEDIV      POSITION(19:19)        CHAR,
   IRREGULAR1              POSITION(20:24)        INTEGER EXTERNAL "DECODE(:IRREGULAR1,     '99999', null, :IRREGULAR1/10)",
   IRREGULAR2              POSITION(25:29)        INTEGER EXTERNAL "DECODE(:IRREGULAR2,     '99999', null, :IRREGULAR2/10)",
   IRREGULAR3              POSITION(30:34)        INTEGER EXTERNAL "DECODE(:IRREGULAR3,     '99999', null, :IRREGULAR3/10)",
   IRREGULAR4              POSITION(35:39)        INTEGER EXTERNAL "DECODE(:IRREGULAR4,     '99999', null, :IRREGULAR4/10)",
   IRREGULAR5              POSITION(40:44)        INTEGER EXTERNAL "DECODE(:IRREGULAR5,     '99999', null, :IRREGULAR5/10)",
   IRREGULAR6              POSITION(45:49)        INTEGER EXTERNAL "DECODE(:IRREGULAR6,     '99999', null, :IRREGULAR6/10)",
   IRREGULAR7              POSITION(50:54)        INTEGER EXTERNAL "DECODE(:IRREGULAR7,     '99999', null, :IRREGULAR7/10)",
   IRREGULAR8              POSITION(55:59)        INTEGER EXTERNAL "DECODE(:IRREGULAR8,     '99999', null, :IRREGULAR8/10)",
   IRREGULAR9              POSITION(60:64)        INTEGER EXTERNAL "DECODE(:IRREGULAR9,     '99999', null, :IRREGULAR9/10)",
   IRREGULAR10             POSITION(65:69)        INTEGER EXTERNAL "DECODE(:IRREGULAR10,    '99999', null, :IRREGULAR10/10)",
   MAXSELECT_EN1           POSITION(70:70)        CHAR,
   MAXSELECT_EN2           POSITION(71:71)        CHAR,
   MAXSELECT_EN3           POSITION(72:72)        CHAR,
   MAXSELECT_EN4           POSITION(73:73)        CHAR,
   MAXSELECT_EN5           POSITION(74:74)        CHAR,
   MAXSELECT_EN6           POSITION(75:75)        CHAR,
   MAXSELECT_MA1           POSITION(76:76)        CHAR,
   MAXSELECT_MA2           POSITION(77:77)        CHAR,
   MAXSELECT_MA3           POSITION(78:78)        CHAR,
   MAXSELECT_MA4           POSITION(79:79)        CHAR,
   MAXSELECT_MA5           POSITION(80:80)        CHAR,
   MAXSELECT_MA6           POSITION(81:81)        CHAR,
   MAXSELECT_JA1           POSITION(82:82)        CHAR,
   MAXSELECT_JA2           POSITION(83:83)        CHAR,
   MAXSELECT_JA3           POSITION(84:84)        CHAR,
   MAXSELECT_JA4           POSITION(85:85)        CHAR,
   MAXSELECT_JA5           POSITION(86:86)        CHAR,
   MAXSELECT_JA6           POSITION(87:87)        CHAR,
   MAXSELECT_SC1           POSITION(88:88)        CHAR,
   MAXSELECT_SC2           POSITION(89:89)        CHAR,
   MAXSELECT_SC3           POSITION(90:90)        CHAR,
   MAXSELECT_SC4           POSITION(91:91)        CHAR,
   MAXSELECT_SC5           POSITION(92:92)        CHAR,
   MAXSELECT_SC6           POSITION(93:93)        CHAR,
   MAXSELECT_GH1           POSITION(94:94)        CHAR,
   MAXSELECT_GH2           POSITION(95:95)        CHAR,
   MAXSELECT_GH3           POSITION(96:96)        CHAR,
   MAXSELECT_GH4           POSITION(97:97)        CHAR,
   MAXSELECT_GH5           POSITION(98:98)        CHAR,
   MAXSELECT_GH6           POSITION(99:99)        CHAR,
   MAXSELECT_CI1           POSITION(100:100)      CHAR,
   MAXSELECT_CI2           POSITION(101:101)      CHAR,
   MAXSELECT_CI3           POSITION(102:102)      CHAR,
   MAXSELECT_CI4           POSITION(103:103)      CHAR,
   MAXSELECT_CI5           POSITION(104:104)      CHAR,
   MAXSELECT_CI6           POSITION(105:105)      CHAR,
   IRR_SUB1                POSITION(106:106)      CHAR,
   IRR_SUB2                POSITION(107:107)      CHAR,
   IRR_SUB3                POSITION(108:108)      CHAR,
   IRR_SUB4                POSITION(109:109)      CHAR,
   IRR_SUB5                POSITION(110:110)      CHAR,
   IRR_SUB6                POSITION(111:111)      CHAR
)

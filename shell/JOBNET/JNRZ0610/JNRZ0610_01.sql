-- ************************************************
-- *機 能 名 : 判定イレギュラファイル
-- *機 能 ID : JNRZ0610_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE IRREGULARFILE_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE IRREGULARFILE_TMP  (
       EXAMYEAR                   CHAR(4)               NOT NULL,   /*  年度                        */
       EXAMDIV                    CHAR(2)               NOT NULL,   /*  模試区分                    */
       SCHOOLDIV                  CHAR(1)                       ,   /*  学校区分                    */
       UNIVCD8                    CHAR(8)                       ,   /*  大学コード(8桁)             */
       ENTEXAMDIV                 CHAR(1)                       ,   /*  入試試験区分                */
       BRANCHCD                   CHAR(2)                       ,   /*  枝番                        */
       IRREGULARDIV               CHAR(1)                       ,   /*  判定イレギュラ区分          */
       IRREGULARCOURSEDIV         CHAR(1)                       ,   /*  判定イレギュラ教科区分      */
       IRREGULAR1                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点1         */
       IRREGULAR2                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点2         */
       IRREGULAR3                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点3         */
       IRREGULAR4                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点4         */
       IRREGULAR5                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点5         */
       IRREGULAR6                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点6         */
       IRREGULAR7                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点7         */
       IRREGULAR8                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点8         */
       IRREGULAR9                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点9         */
       IRREGULAR10                NUMBER(5,1)                   ,   /*  判定イレギュラ配点10        */
       MAXSELECT_EN1              CHAR(1)                       ,   /*  最大選択科目数－英語１      */
       MAXSELECT_EN2              CHAR(1)                       ,   /*  最大選択科目数－英語２      */
       MAXSELECT_EN3              CHAR(1)                       ,   /*  最大選択科目数－英語３      */
       MAXSELECT_EN4              CHAR(1)                       ,   /*  最大選択科目数－英語４      */
       MAXSELECT_EN5              CHAR(1)                       ,   /*  最大選択科目数－英語５      */
       MAXSELECT_EN6              CHAR(1)                       ,   /*  最大選択科目数－英語６      */
       MAXSELECT_MA1              CHAR(1)                       ,   /*  最大選択科目数－数学１      */
       MAXSELECT_MA2              CHAR(1)                       ,   /*  最大選択科目数－数学２      */
       MAXSELECT_MA3              CHAR(1)                       ,   /*  最大選択科目数－数学３      */
       MAXSELECT_MA4              CHAR(1)                       ,   /*  最大選択科目数－数学４      */
       MAXSELECT_MA5              CHAR(1)                       ,   /*  最大選択科目数－数学５      */
       MAXSELECT_MA6              CHAR(1)                       ,   /*  最大選択科目数－数学６      */
       MAXSELECT_JA1              CHAR(1)                       ,   /*  最大選択科目数－国語１      */
       MAXSELECT_JA2              CHAR(1)                       ,   /*  最大選択科目数－国語２      */
       MAXSELECT_JA3              CHAR(1)                       ,   /*  最大選択科目数－国語３      */
       MAXSELECT_JA4              CHAR(1)                       ,   /*  最大選択科目数－国語４      */
       MAXSELECT_JA5              CHAR(1)                       ,   /*  最大選択科目数－国語５      */
       MAXSELECT_JA6              CHAR(1)                       ,   /*  最大選択科目数－国語６      */
       MAXSELECT_SC1              CHAR(1)                       ,   /*  最大選択科目数－理科１      */
       MAXSELECT_SC2              CHAR(1)                       ,   /*  最大選択科目数－理科２      */
       MAXSELECT_SC3              CHAR(1)                       ,   /*  最大選択科目数－理科３      */
       MAXSELECT_SC4              CHAR(1)                       ,   /*  最大選択科目数－理科４      */
       MAXSELECT_SC5              CHAR(1)                       ,   /*  最大選択科目数－理科５      */
       MAXSELECT_SC6              CHAR(1)                       ,   /*  最大選択科目数－理科６      */
       MAXSELECT_GH1              CHAR(1)                       ,   /*  最大選択科目数－地歴１      */
       MAXSELECT_GH2              CHAR(1)                       ,   /*  最大選択科目数－地歴２      */
       MAXSELECT_GH3              CHAR(1)                       ,   /*  最大選択科目数－地歴３      */
       MAXSELECT_GH4              CHAR(1)                       ,   /*  最大選択科目数－地歴４      */
       MAXSELECT_GH5              CHAR(1)                       ,   /*  最大選択科目数－地歴５      */
       MAXSELECT_GH6              CHAR(1)                       ,   /*  最大選択科目数－地歴６      */
       MAXSELECT_CI1              CHAR(1)                       ,   /*  最大選択科目数－公民１      */
       MAXSELECT_CI2              CHAR(1)                       ,   /*  最大選択科目数－公民２      */
       MAXSELECT_CI3              CHAR(1)                       ,   /*  最大選択科目数－公民３      */
       MAXSELECT_CI4              CHAR(1)                       ,   /*  最大選択科目数－公民４      */
       MAXSELECT_CI5              CHAR(1)                       ,   /*  最大選択科目数－公民５      */
       MAXSELECT_CI6              CHAR(1)                       ,   /*  最大選択科目数－公民６      */
       IRR_SUB1                   CHAR(1)                       ,   /*  イレギュラ科目数１          */
       IRR_SUB2                   CHAR(1)                       ,   /*  イレギュラ科目数２          */
       IRR_SUB3                   CHAR(1)                       ,   /*  イレギュラ科目数３          */
       IRR_SUB4                   CHAR(1)                       ,   /*  イレギュラ科目数４          */
       IRR_SUB5                   CHAR(1)                       ,   /*  イレギュラ科目数５          */
       IRR_SUB6                   CHAR(1)                       ,   /*  イレギュラ科目数６          */
       CONSTRAINT PK_IRREGULARFILE_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  年度                   */
                 EXAMDIV                   ,               /*  模試区分               */
                 SCHOOLDIV                 ,               /*  学校区分               */
                 UNIVCD8                   ,               /*  大学コード(8桁)        */
                 ENTEXAMDIV                ,               /*  入試試験区分           */
                 BRANCHCD                  ,               /*  枝番                   */
                 IRREGULARDIV              ,               /*  判定イレギュラ区分     */
                 IRREGULARCOURSEDIV                        /*  判定イレギュラ教科区分 */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

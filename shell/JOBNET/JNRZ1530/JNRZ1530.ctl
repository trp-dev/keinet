------------------------------------------------------------------------------
-- *機 能 名 : Ｋ－Ｗｅｂ小問学力要素マスタのCTLファイル
-- *機 能 ID : JNRZ1530.ctl
-- *作 成 日 : 2019/09/24
-- *作 成 者 : QQ)瀬尾
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE KWEBSHOQUE_ACDMC_TMP
(  
   EXAMYEAR                 POSITION(01:04)  CHAR 
 , EXAMCD                   POSITION(05:06)  CHAR 
 , SUBCD                    POSITION(07:10)  CHAR 
 , KWEB_SHOQUESTION_DEPT_NO POSITION(11:13)  INTEGER EXTERNAL 
 , ANSWERSHEETCD            POSITION(14:15)  CHAR 
 , ANSWERSHEET_DEPT_NO      POSITION(16:18)  INTEGER EXTERNAL 
 , ACADEMICDETAILCD         POSITION(19:21)  CHAR 
 , ACADEMICCD               POSITION(22:22)  CHAR 
)

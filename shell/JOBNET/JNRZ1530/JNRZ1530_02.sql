-- *********************************************************
-- *機 能 名 : Ｋ－Ｗｅｂ小問学力要素マスタ
-- *機 能 ID : JNRZ1530_02.sql
-- *作 成 日 : 2019/09/24
-- *作 成 者 : QQ)瀬尾
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM KWEBSHOQUE_ACDMC MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM KWEBSHOQUE_ACDMC_TMP TMP
         WHERE 1=1
           AND MAIN.EXAMYEAR                 = TMP.EXAMYEAR
           AND MAIN.EXAMCD                   = TMP.EXAMCD
           AND MAIN.SUBCD                    = TMP.SUBCD
           AND MAIN.KWEB_SHOQUESTION_DEPT_NO = TMP.KWEB_SHOQUESTION_DEPT_NO
           AND MAIN.ANSWERSHEETCD            = TMP.ANSWERSHEETCD
           AND MAIN.ANSWERSHEET_DEPT_NO      = TMP.ANSWERSHEET_DEPT_NO
           AND MAIN.ACADEMICDETAILCD         = TMP.ACADEMICDETAILCD
       )
;

-- テンポラリから本物へ登録
INSERT INTO KWEBSHOQUE_ACDMC SELECT * FROM KWEBSHOQUE_ACDMC_TMP;

-- テンポラリテーブル削除
DROP TABLE KWEBSHOQUE_ACDMC_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

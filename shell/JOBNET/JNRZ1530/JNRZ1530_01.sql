-- ************************************************
-- *機 能 名 : Ｋ－Ｗｅｂ小問学力要素マスタ
-- *機 能 ID : JNRZ1530_01.sql
-- *作 成 日 : 2019/09/24
-- *作 成 者 : QQ)瀬尾
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE KWEBSHOQUE_ACDMC_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE KWEBSHOQUE_ACDMC_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM KWEBSHOQUE_ACDMC WHERE 0=1;

-- 主キーの設定
ALTER TABLE KWEBSHOQUE_ACDMC_TMP ADD CONSTRAINT PK_KWEBSHOQUE_ACDMC_TMP PRIMARY KEY 
(
   EXAMYEAR
 , EXAMCD
 , SUBCD
 , KWEB_SHOQUESTION_DEPT_NO
 , ANSWERSHEETCD
 , ANSWERSHEET_DEPT_NO
 , ACADEMICDETAILCD
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

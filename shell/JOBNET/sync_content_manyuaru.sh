#!/bin/sh

#--- 同期基点Server Set
PARENT_SRV='satu01test'

#--- Target Server Set
if [ $1 = "1" ]
then
 	#-- 検証機同期サーバ
	TARGETSRV='satu03test'
elif [ $1 = "2" ]
then	
	#-- 本番機同期サーバ
	TARGETSRV='satu02test satu01 satu02 satu03'
fi

#--- Start rsyncing
echo " "
echo "コンテンツ同期を開始します..."
for srv in $TARGETSRV; do
	echo " "
	echo "*******$PARENT_SRV to $srv*******"
	rsh -l kei-navi $PARENT_SRV "rsync -avz --delete /usr/local/tomcat/webapps2/keinavi/underconstruction.html $srv:/usr/local/tomcat/webapps2/keinavi/underconstruction.html"
	rsh -l kei-navi $PARENT_SRV "rsync -avz --delete /usr/local/tomcat/webapps2/keispro/underconstruction.html $srv:/usr/local/tomcat/webapps2/keispro/underconstruction.html"
done
echo " "
echo "...コンテンツ同期を終了しました"



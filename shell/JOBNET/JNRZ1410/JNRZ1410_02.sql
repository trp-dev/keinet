-- *********************************************************
-- *機 能 名 : 記述式設問別条件マスタ
-- *機 能 ID : JNRZ1410_02.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM DESCQUEPROVISO MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM DESCQUEPROVISO_TMP TMP
         WHERE 1=1
           AND MAIN.EVENTYEAR        = TMP.EVENTYEAR
           AND MAIN.EXAMCD           = TMP.EXAMCD
           AND MAIN.SUBCD            = TMP.SUBCD
           AND MAIN.QUESTION_NO      = TMP.QUESTION_NO
           AND MAIN.POINTPROVISONUM  = TMP.POINTPROVISONUM
       )
;

-- テンポラリから本物へ登録
INSERT INTO DESCQUEPROVISO SELECT * FROM DESCQUEPROVISO_TMP;

-- テンポラリテーブル削除
DROP TABLE DESCQUEPROVISO_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

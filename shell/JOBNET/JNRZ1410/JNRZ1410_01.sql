-- ************************************************
-- *機 能 名 : 記述式設問別条件マスタ
-- *機 能 ID : JNRZ1410_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE DESCQUEPROVISO_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE DESCQUEPROVISO_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM DESCQUEPROVISO WHERE 0=1;

-- 主キーの設定
ALTER TABLE DESCQUEPROVISO_TMP ADD CONSTRAINT PK_DESCQUEPROVISO_TMP PRIMARY KEY 
(
   EVENTYEAR
 , EXAMCD
 , SUBCD
 , QUESTION_NO
 , POINTPROVISONUM
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

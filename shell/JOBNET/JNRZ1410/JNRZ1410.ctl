------------------------------------------------------------------------------
-- *機 能 名 : 記述式設問別条件マスタのCTLファイル
-- *機 能 ID : JNRZ1410.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE DESCQUEPROVISO_TMP
(  
   EVENTYEAR        POSITION(01:04)  CHAR 
 , EXAMCD           POSITION(05:06)  CHAR 
 , SUBCD            POSITION(07:10)  CHAR 
 , QUESTION_NO      POSITION(11:12)  CHAR 
 , POINTPROVISONUM  POSITION(13:13)  INTEGER EXTERNAL 
 , POINTPROVISONAME POSITION(14:113)  INTEGER EXTERNAL 
)

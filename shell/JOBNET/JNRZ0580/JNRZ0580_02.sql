-- *********************************************************
-- *機 能 名 : 高3模試用判定大学マスタ
-- *機 能 ID : JNRZ0580_02.sql
-- *作 成 日 : 2004/08/31
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM HS3_UNIV_EXAM
WHERE  (YEAR,EXAMDIV)
       IN(SELECT DISTINCT YEAR,EXAMDIV
          FROM   HS3_UNIV_EXAM_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO HS3_UNIV_EXAM
SELECT * FROM HS3_UNIV_EXAM_TMP;

-- 2年以上前のデータを削除
DELETE FROM HS3_UNIV_EXAM
WHERE  TO_NUMBER(YEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 2);

-- 更新対象データの1年前のデータを削除
DELETE FROM HS3_UNIV_EXAM
WHERE  (YEAR,EXAMDIV)
--       IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 1)),EXAMDIV
       IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(YEAR) - 1)),EXAMDIV
          FROM   HS3_UNIV_EXAM_TMP);

-- テンポラリテーブル削除
DROP TABLE HS3_UNIV_EXAM_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

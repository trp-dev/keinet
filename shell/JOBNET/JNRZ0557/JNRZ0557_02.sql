-- *********************************************************
-- *機 能 名 : 大学系統マスタ
-- *機 能 ID : JNRZ0557_02.sql
-- *作 成 日 : 2009/12/17
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM UNIVSTEMMA
WHERE  (YEAR)
       IN(SELECT DISTINCT YEAR
          FROM   UNIVSTEMMA_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO UNIVSTEMMA
SELECT * FROM UNIVSTEMMA_TMP;

-- 3年以上前のデータを削除
DELETE FROM UNIVSTEMMA
WHERE  TO_NUMBER(YEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 3);

-- テンポラリテーブル削除
DROP TABLE UNIVSTEMMA_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

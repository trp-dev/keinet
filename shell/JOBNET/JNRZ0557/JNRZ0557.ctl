------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 大学系統マスタ
--  テーブル名      : UNIVSTEMMA
--  データファイル名: UNIVSTEMMA
--  機能            : 大学系統マスタをデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 2009/12/08
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE UNIVSTEMMA_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   YEAR             POSITION(01:04)      CHAR,
   UNIVCD           POSITION(05:08)      CHAR,
   FACULTYCD        POSITION(09:10)      CHAR,
   DEPTCD           POSITION(11:14)      CHAR,
   STEMMADIV        POSITION(15:15)      CHAR,
   STEMMACD         POSITION(16:19)      CHAR
)

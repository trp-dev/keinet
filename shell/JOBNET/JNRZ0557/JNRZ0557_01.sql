-- ************************************************
-- *機 能 名 : 大学系統マスタ
-- *機 能 ID : JNRZ0557_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : 
-- *更 新 日 : 2009/12/17
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE UNIVSTEMMA_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE UNIVSTEMMA_TMP  (
       YEAR          CHAR(4)        NOT NULL,    /*    年度            */ 
       UNIVCD        CHAR(4)        NOT NULL,    /*    大学コード      */ 
       FACULTYCD     CHAR(2)        NOT NULL,    /*    学部コード      */ 
       DEPTCD        CHAR(4)        NOT NULL,    /*    学科コード      */ 
       STEMMADIV     CHAR(1)        NOT NULL,    /*    系統区分        */ 
       STEMMACD      CHAR(4)        NOT NULL,    /*    系統コード      */ 
       CONSTRAINT PK_UNIVSTEMMA_TMP PRIMARY KEY (
                 YEAR                  ,               /*  年度        */
                 UNIVCD                ,               /*  大学コード  */
                 FACULTYCD             ,               /*  学部コード  */
                 DEPTCD                ,               /*  学科コード  */
                 STEMMADIV             ,               /*  系統区分    */
                 STEMMACD                              /*  系統コード  */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

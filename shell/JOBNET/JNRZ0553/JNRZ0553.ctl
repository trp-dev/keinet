------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 大学マスタ模試用教科
--  テーブル名      : UNIVMASTER_COURSE
--  データファイル名: UNIVMASTER_COURSE
--  機能            : 大学マスタ模試用教科をデータファイルの内容に置き換える
--  作成日          : 2009/12/17
--  修正日          : 
--  備考            : ２次(ENTEXAMDIV=2)のみ、英語、数学、国語の判定必須科目が
--                  : １以上であれば１にする
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE UNIVMASTER_COURSE_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    EVENTYEAR                 POSITION(01:04)        CHAR,
    SCHOOLDIV                 POSITION(05:05)        CHAR,
    UNIVCD                    POSITION(06:09)        CHAR,
    FACULTYCD                 POSITION(10:11)        CHAR,
    DEPTCD                    POSITION(12:15)        CHAR,
    EXAMDIV                   POSITION(16:17)        CHAR,
    ADMISSIONDIV              POSITION(18:18)        CHAR,
    NEXTYEARDIV               POSITION(19:19)        CHAR,
    ENTEXAMDIV                POSITION(20:20)        CHAR,
    UNICDBRANCHCD             POSITION(21:23)        CHAR,
    COURSECD                  POSITION(24:24)        CHAR,
    NECESALLOTPNT             POSITION(25:29)        INTEGER EXTERNAL "DECODE(:NECESALLOTPNT,   '99999', null, :NECESALLOTPNT/10)",
    NECESSUBCOUNT             POSITION(30:30)        CHAR,
    CEN_SECSUBCHOICEDIV       POSITION(31:31)        CHAR
)

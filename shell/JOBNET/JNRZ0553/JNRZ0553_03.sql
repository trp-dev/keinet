-- ************************************************
-- *機 能 名 : 大学マスタ模試用教科
-- *機 能 ID : JNRZ0553_01.sql
-- *作 成 日 : 2009/12/17
-- *作 成 者 : 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE UNIVMASTER_COURSE_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

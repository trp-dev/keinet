-- ************************************************
-- *機 能 名 : 入試日程詳細
-- *機 能 ID : JNRZ0560_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE SCHEDULEDETAIL_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE SCHEDULEDETAIL_TMP  (
       YEAR              CHAR(4)              NOT NULL,    /*   年度              */ 
       UNIVCD            CHAR(4)              NOT NULL,    /*   大学コード        */ 
       FACULTYCD         CHAR(2)              NOT NULL,    /*   学部コード        */ 
       DEPTCD            CHAR(2)              NOT NULL,    /*   学科コード        */ 
       ENTEXAMDIV1       CHAR(1)              NOT NULL,    /*   入試区分１        */ 
       ENTEXAMDIV2       CHAR(1)              NOT NULL,    /*   入試区分２        */ 
       ENTEXAMDIV3       CHAR(1)              NOT NULL,    /*   入試区分３        */ 
       ENTEXAMINPLEDATE1 CHAR(4)                      ,    /*   入試実施日１      */ 
       ENTEXAMINPLEDATE2 CHAR(4)                      ,    /*   入試実施日２      */ 
       ENTEXAMINPLEDATE3 CHAR(4)                      ,    /*   入試実施日３      */ 
       ENTEXAMINPLEDATE4 CHAR(4)                      ,    /*   入試実施日４      */ 
       INPLEDATE1        CHAR(1)                      ,    /*   実施日相関１      */ 
       INPLEDATE2        CHAR(1)                      ,    /*   実施日相関２      */ 
       INPLEDATE3        CHAR(1)                      ,    /*   実施日相関３      */ 
       APPLIDEADLINE     CHAR(4)                      ,    /*   出願締切日        */ 
       SUCANNDATE        CHAR(4)                      ,    /*   合格発表日        */ 
       PROCEDEADLINE     CHAR(4)                      ,    /*   入学手続締切日    */ 
       CONSTRAINT PK_SCHEDULEDETAIL_TMP PRIMARY KEY (
                 YEAR                      ,               /*  年度        */
                 UNIVCD                    ,               /*  大学コード  */
                 FACULTYCD                 ,               /*  学部コード  */
                 DEPTCD                    ,               /*  学科コード  */
                 ENTEXAMDIV1               ,               /*  入試区分１  */
                 ENTEXAMDIV2               ,               /*  入試区分２  */
                 ENTEXAMDIV3                               /*  入試区分３  */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

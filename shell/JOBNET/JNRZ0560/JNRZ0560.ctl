------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 入試日程詳細
--  テーブル名      : SCHEDULEDETAIL
--  データファイル名: SCHEDULEDETAIL
--  機能            : 入試日程詳細をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE SCHEDULEDETAIL_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    YEAR                     POSITION(01:04)    CHAR,
    UNIVCD                   POSITION(05:08)    CHAR,
    FACULTYCD                POSITION(09:10)    CHAR,
    DEPTCD                   POSITION(11:12)    CHAR,
    ENTEXAMDIV1              POSITION(13:13)    CHAR,
    ENTEXAMDIV2              POSITION(14:14)    CHAR,
    ENTEXAMDIV3              POSITION(15:15)    CHAR,
    ENTEXAMINPLEDATE1        POSITION(16:19)    CHAR,
    ENTEXAMINPLEDATE2        POSITION(20:23)    CHAR,
    ENTEXAMINPLEDATE3        POSITION(24:27)    CHAR,
    ENTEXAMINPLEDATE4        POSITION(28:31)    CHAR,
    INPLEDATE1               POSITION(32:32)    CHAR,
    INPLEDATE2               POSITION(33:33)    CHAR,
    INPLEDATE3               POSITION(34:34)    CHAR,
    APPLIDEADLINE            POSITION(35:38)    CHAR,
    SUCANNDATE               POSITION(39:42)    CHAR,
    PROCEDEADLINE            POSITION(43:46)    CHAR
)

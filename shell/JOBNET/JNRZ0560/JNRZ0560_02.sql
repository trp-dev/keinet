-- *********************************************************
-- *機 能 名 : 入試日程詳細
-- *機 能 ID : JNRZ0560_02.sql
-- *作 成 日 : 2004/08/31
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM SCHEDULEDETAIL
WHERE  (YEAR)
       IN(SELECT DISTINCT YEAR
          FROM   SCHEDULEDETAIL_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO SCHEDULEDETAIL
SELECT * FROM SCHEDULEDETAIL_TMP;

-- 1年以上前のデータを削除
DELETE FROM SCHEDULEDETAIL
WHERE  TO_NUMBER(YEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 1);

-- テンポラリテーブル削除
DROP TABLE SCHEDULEDETAIL_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

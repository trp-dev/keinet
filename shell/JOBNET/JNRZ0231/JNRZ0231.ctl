------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : マーク模試解答番号マスタ
--  テーブル名      : MARKANSWER_NO
--  データファイル名: MARKANSWER_NO
--  機能            : マーク模試解答番号マスタをデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 2009/11/30
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE MARKANSWER_NO_TMP
WHEN ANSWER_NO != ''
(  
   EXAMYEAR          POSITION(01:04)   CHAR,
   EXAMCD            POSITION(05:06)   CHAR,
   SUBCD             POSITION(07:10)   CHAR,
   ANSWER_NO         POSITION(11:16)   CHAR,
   BIGQUESTION_NO    POSITION(17:19)   CHAR,
   BIGQUESTION       POSITION(20:31)   CHAR,
   ALLOTPNT          POSITION(32:34)   INTEGER EXTERNAL "NULLIF(:ALLOTPNT,               '999')",
   CORRECTANSWER     POSITION(35:35)   CHAR
)


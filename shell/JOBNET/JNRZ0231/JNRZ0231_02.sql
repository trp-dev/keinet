-- *********************************************************
-- *機 能 名 : マーク模試解答番号マスタ
-- *機 能 ID : JNRZ0231_02.sql
-- *作 成 日 : 2004/08/30
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM MARKANSWER_NO
WHERE  (EXAMYEAR,EXAMCD)
       IN(SELECT DISTINCT EXAMYEAR,EXAMCD
          FROM   MARKANSWER_NO_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO MARKANSWER_NO
SELECT * FROM MARKANSWER_NO_TMP;

-- 8年以上前のデータを削除
-- DELETE FROM MARKANSWER_NO
-- WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 8);

-- 更新対象データの7年前のデータを削除
-- DELETE FROM MARKANSWER_NO
-- WHERE  (EXAMYEAR,EXAMCD)
--        IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 7)),EXAMCD
--          FROM   MARKANSWER_NO_TMP);

-- テンポラリテーブル削除
DROP TABLE MARKANSWER_NO_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

-- ************************************************
-- *機 能 名 : マーク模試解答番号マスタ
-- *機 能 ID : JNRZ0231_01.sql
-- *作 成 日 : 2004/08/30
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 : 2009/11/30
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE MARKANSWER_NO_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE MARKANSWER_NO_TMP  (
       EXAMYEAR             CHAR(4)                NOT NULL,   /*   模試年度       */ 
       EXAMCD               CHAR(2)                NOT NULL,   /*   模試コード     */ 
       SUBCD                CHAR(4)                NOT NULL,   /*   科目コード     */ 
       ANSWER_NO            VARCHAR2(6)            NOT NULL,   /*   解答番号       */ 
       ALLOTPNT             NUMBER(3)                      ,   /*   配点           */ 
       CORRECTANSWER        CHAR(1)                        ,   /*   正答           */ 
       BIGQUESTION_NO       CHAR(3)                        ,   /*   問題番号       */ 
       BIGQUESTION          VARCHAR2(12)                   ,   /*   大問・内容     */ 
       CONSTRAINT PK_MARKANSWER_NO_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  模試年度    */
                 EXAMCD                    ,               /*  模試コード  */
                 SUBCD                     ,               /*  科目コード  */
                 ANSWER_NO                                 /*  解答番号    */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

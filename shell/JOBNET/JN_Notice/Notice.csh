#! /bin/csh 

### トップページ/お知らせ更新スクリプト 
###


set MainDir = "/keinavi/JOBNET/JN_Notice"
set targetcomDir = "/keinavi/help_user/OtherMainte"
set targetDir = "${targetcomDir}/Set"
set Upfilename = "Notice.html"

#-- メンテナンス対象ファイル
set targetfile = "${targetDir}/${Upfilename}"

#-- syncスクリプト
set syncscrip = "${MainDir}/sync_content_Notice.sh"

#-- ファイル配置場所
set UpFileDir = (/usr/local/tomcat/webapps2/keinavi/mainte_html /usr/local/tomcat/webapps2/keispro/mainte_html/)


set upflag = $1
if ($upflag == 1) then

	#--- メンテナンス対象ファイルの有無確認
	if (! -f $targetfile) then
		echo "***** File Check ERROR *****"
		echo "---- メンテナンス対象ファイルがありません。対象ファイルを配置後、再度実行してください。----"
		exit 30
	endif

	#--- 現在のファイルをバックアップ
	if (-f ${targetcomDir}/Get/${Upfilename}) then
		echo " "
		echo "--- ファイルをバックアップ ---"
		cp -p ${targetcomDir}/Get/${Upfilename} $MainDir/bk
	endif

	#--- 検証機への各フォルダへ展開
	set counts = 1
	echo " "
	echo "--- メンテナンス対象ファイルを検証機へ転送 ---"
	while ($counts <= $#UpFileDir)

		#--- 検証機へFTP転送
		/keinavi/HULFT/ftp.sh $UpFileDir[$counts] ${targetDir} ${Upfilename} 10.1.4.198
		@ counts ++
	end

	#--- 各ファイルを検証機内で同期する
	$syncscrip 1

	#--- 本番機展開有無フラグ
	touch ${MainDir}/ServUp.txt

	echo " "
	echo "--- 検証機のトップページ/お知らせの更新完了 ---"

else if($upflag == 2) then

	if (! -f ${MainDir}/ServUp.txt) then
		echo "***** UpDate ERROR ******"
		echo "---- 検証機への反映がされていません。検証機への反映後、再度実施してください ----"
		exit 30
	endif

	#--- 各ファイルを本番機へ展開（検証機と同期）する
	$syncscrip 2
	
	\rm ${MainDir}/ServUp.txt
	if (-f ${MainDir}/bk/${Upfilename}) then
		\rm ${MainDir}/bk/${Upfilename}
	endif

	\mv ${targetcomDir}/Set/${Upfilename} ${targetcomDir}/Get

	echo " "
	echo "--- 本番機のトップページ/お知らせの更新完了 ---"
endif


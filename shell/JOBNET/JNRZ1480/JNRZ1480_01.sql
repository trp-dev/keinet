-- ************************************************
-- *機 能 名 : 英語認定試験_得点換算マスタ
-- *機 能 ID : JNRZ1480_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE ENGPT_SCORECONV_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE ENGPT_SCORECONV_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM ENGPT_SCORECONV WHERE 0=1;

-- 主キーの設定
ALTER TABLE ENGPT_SCORECONV_TMP ADD CONSTRAINT PK_ENGPT_SCORECONV_TMP PRIMARY KEY 
(
   EVENTYEAR
 , EXAMDIV
 , UNIVSINGLECD
 , CENTERCVSSCORE_ENGPT
 , NEXTYEARDIV
 , ELIGIBILITYCD
 , ELIGIBILITYNUM
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

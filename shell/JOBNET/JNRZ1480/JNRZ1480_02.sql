-- *********************************************************
-- *機 能 名 : 英語認定試験_得点換算マスタ
-- *機 能 ID : JNRZ1480_02.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM ENGPT_SCORECONV MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM ENGPT_SCORECONV_TMP TMP
         WHERE 1=1
           AND MAIN.EVENTYEAR             = TMP.EVENTYEAR
           AND MAIN.EXAMDIV               = TMP.EXAMDIV
           AND MAIN.UNIVSINGLECD          = TMP.UNIVSINGLECD
           AND MAIN.CENTERCVSSCORE_ENGPT  = TMP.CENTERCVSSCORE_ENGPT
           AND MAIN.NEXTYEARDIV           = TMP.NEXTYEARDIV
           AND MAIN.ELIGIBILITYCD         = TMP.ELIGIBILITYCD
           AND MAIN.ELIGIBILITYNUM        = TMP.ELIGIBILITYNUM
       )
;

-- テンポラリから本物へ登録
INSERT INTO ENGPT_SCORECONV SELECT * FROM ENGPT_SCORECONV_TMP;

-- テンポラリテーブル削除
DROP TABLE ENGPT_SCORECONV_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

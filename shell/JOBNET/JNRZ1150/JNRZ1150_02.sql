-- ************************************************
-- *機 能 名 : 模試コード変換マスタ
-- *機 能 ID : JNRZ1150_02.sql
-- *作 成 日 : 2006/10/30
-- *作 成 者 : KCN)goto
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM EXAMCODETRANS;

-- テンポラリテーブルからデータを挿入
INSERT INTO EXAMCODETRANS
SELECT * FROM EXAMCODETRANS_TMP;

-- テンポラリテーブル削除
DROP TABLE EXAMCODETRANS_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

------------------------------------------------------------------------------
--  システム名      : Kei-Navi
--  概念ファイル名  : 模試コード変換マスタ
--  テーブル名      : EXAMCODETRANS_TMP
--  データファイル名: EXAMCODETRANSyyyymmdd.csv
--  機能            : 模試コード変換マスタをデータファイルの内容に置き換える
--  作成日          : 2006/10/30
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE EXAMCODETRANS_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    EXAMYEAR,
    OLDEXAMCD	"DECODE(LENGTH(:OLDEXAMCD), 1, CONCAT('0', :OLDEXAMCD), 2, :OLDEXAMCD)",
    NEWEXAMCD	"DECODE(LENGTH(:NEWEXAMCD), 1, CONCAT('0', :NEWEXAMCD), 2, :NEWEXAMCD)"
)

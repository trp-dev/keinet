-- ************************************************
-- *機 能 名 : 模試コード変換マスタ
-- *機 能 ID : JNRZ1150_01.sql
-- *作 成 日 : 2006/10/30
-- *作 成 者 : KCN)goto
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE EXAMCODETRANS_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成

CREATE TABLE EXAMCODETRANS_TMP(   
        EXAMYEAR               CHAR(4)          NOT NULL,    /*    年度                 */ 
        OLDEXAMCD              CHAR(2)          NOT NULL,    /*    変更前模試コード     */ 
        NEWEXAMCD              CHAR(2)          NOT NULL,    /*    変更後模試コード     */ 
    CONSTRAINT PK_EXAMCODETRANS_TMP PRIMARY KEY(
        EXAMYEAR,                                       /*    年度                 */
        OLDEXAMCD,           				/*    変更前模試コード     */ 
        NEWEXAMCD                                       /*    変更後模試コード     */
    )
    USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

#!/bin/sh

BACKUP_DIR="/var/backup"
DAY=`date +%y%m%d%H%M`

# OperationMGR 
echo "OperatonMGR環境をバックアップします"
/opt/systemwalker/bin/mpbko -b /keinavi/mpbackup/mpbackup_$DAY -SN -d
tar --directory=/keinavi -cpf $BACKUP_DIR/backup.tar mpbackup

# JOBNET
echo "JOBNETディレクトリをバックアップします"
tar --directory=/keinavi -rpf $BACKUP_DIR/backup.tar JOBNET

# HULFT
echo "HULFT環境をバックアップします"
tar --directory=/usr/local -rpf $BACKUP_DIR/backup.tar HULFT

echo "書庫を圧縮します.."
gzip /var/backup/backup.tar
mv /var/backup/backup.tar.gz $BACKUP_DIR/backup_${DAY}.tar.gz
echo "完了 -> $BACKUP_DIR/backup_${DAY}.tar.gz"

rm -rf /keinavi/mpbackup/*

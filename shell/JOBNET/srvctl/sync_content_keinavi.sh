#!/bin/sh

LOCKFILE="/keinavi/JOBNET/lockfile/srv_stop.lock"

#--- Check lockfile
if [ ! -f $LOCKFILE ];then
    echo "サービス中です。処理を中止します。"
    exit 20
fi

if [ $# -lt 1 ]
then
        echo "parameter error"
        exit
fi


#--- Target Server Set
if [ $1 = "7" ]
then
	PARENT_SVR="satu01" 
	TARGETSRV='satu02test satu02 satu03'
elif [ $1 = "9" ]
then	
	PARENT_SVR="satu03" 
	TARGETSRV='satu02test satu01 satu02'
fi

#--- Start syncing
echo "コンテンツ同期を開始します..."
for srv in $TARGETSRV; do
	echo " "
	echo "*******$PARENT_SVR to $srv*******"
	rsh $PARENT_SVR "rsync -avz --delete /usr/local/tomcat/webapps2/keinavi/ $srv:/usr/local/tomcat/webapps2/keinavi/"
done
echo " "
echo "...コンテンツ同期を終了しました"



#!/bin/sh

LOCKFILE="/keinavi/JOBNET/lockfile/srv_stop.lock"

# Start Web Servers
echo "Apacheを運用モードで再起動します.."

echo "*******satu01 stop*******"
/usr/bin/rsh satu01 /usr/local/apache2/bin/apachectl.sh normal
echo "*******satu02 stop*******"
/usr/bin/rsh satu02 /usr/local/apache2/bin/apachectl.sh normal
echo "*******satu03 stop*******"
/usr/bin/rsh satu03 /usr/local/apache2/bin/apachectl.sh normal

# Delete lockfile
if [ -f $LOCKFILE ]; then
    echo "ロックファイルを削除します.."
    echo "--$LOCKFILE"
    rm -f $LOCKFILE
fi


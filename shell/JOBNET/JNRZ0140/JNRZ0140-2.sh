#!/bin/sh

#環境変数設定
export JOBNAME=JNRZ0140
export CSVFILE=QRECORD_P
export SRCDIR=/keinavi/HULFT/temp/temp2
export WORKDIR=/keinavi/JOBNET/$JOBNAME

#SQL*PLUSテンポラリテーブル作成
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ0140_01.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo テンポラリテーブル作成処理でエラーが発生しました
    exit 30
fi


#SQL*LOADERシェル実行
/keinavi/JOBNET/common/JNRZ0IMP.sh

#SQL*LOADER戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo SQL*LOADERでエラーが発生しました
    exit 30
fi


#SQL*PLUSデータインポート->テンポラリテーブル削除
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ0140_02.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo データのインポート処理でエラーが発生しました
    exit 30
fi

#正常終了
echo データのインポート処理が正常に終了しました
exit  0    #正常終了

-- ************************************************
-- *機 能 名 : 設問別成績(県)
-- *機 能 ID : JNRZ0140_01.sql
-- *作 成 日 : 2004/08/30
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE QRECORD_P_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE QRECORD_P_TMP  (
       EXAMYEAR            CHAR(4)              NOT NULL,   /*   模試年度        */ 
       EXAMCD              CHAR(2)              NOT NULL,   /*   模試コード      */ 
       PREFCD              CHAR(2)              NOT NULL,   /*   県コード        */ 
       SUBCD               CHAR(4)              NOT NULL,   /*   科目コード      */ 
       QUESTION_NO         NUMBER(2,0)          NOT NULL,   /*   設問番号        */ 
       AVGPNT              NUMBER(5,1)                  ,   /*   平均点          */ 
       NUMBERS             NUMBER(8,0)                  ,   /*   人数            */ 
       AVGSCORERATE        NUMBER(4,1)                  ,   /*   平均得点率      */ 
       CONSTRAINT PK_QRECORD_P_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  模試年度    */
                 EXAMCD                    ,               /*  模試コード  */
                 PREFCD                    ,               /*  県コード    */
                 SUBCD                     ,               /*  科目コード  */
                 QUESTION_NO                               /*  設問番号    */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 設問別成績（県）
--  テーブル名      : QRECORD_P
--  データファイル名: QRECORD_P
--  機能            : 設問別成績（県）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE QRECORD_P_TMP
(  
   EXAMYEAR        POSITION(01:04)  CHAR,
   EXAMCD          POSITION(05:06)  CHAR,
   PREFCD          POSITION(07:08)  CHAR,
   SUBCD           POSITION(09:12)  CHAR,
   QUESTION_NO     POSITION(13:14)  INTEGER EXTERNAL,
   AVGPNT          POSITION(15:19)  INTEGER EXTERNAL "DECODE(:AVGPNT,         '99999', null, :AVGPNT/10)",
   NUMBERS         POSITION(20:27)  INTEGER EXTERNAL "NULLIF(:NUMBERS,     '99999999')",
   AVGSCORERATE    POSITION(28:31)  INTEGER EXTERNAL "DECODE(:AVGSCORERATE,    '9999', null, :AVGSCORERATE/10)"
)


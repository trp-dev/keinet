-- *********************************************************
-- *機 能 名 : 設問別成績(県)
-- *機 能 ID : JNRZ0140_02.sql
-- *作 成 日 : 2004/08/30
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM QRECORD_P
WHERE  (EXAMYEAR,EXAMCD)
       IN(SELECT DISTINCT EXAMYEAR,EXAMCD
          FROM   QRECORD_P_TMP);

-- テンポラリテーブルから更新対象データを挿入
-- （県コード99は対象外）
INSERT INTO QRECORD_P
SELECT * FROM QRECORD_P_TMP
WHERE  PREFCD!='99';

-- 設問別成績（高校）データ作成済みの場合は削除する
DELETE FROM QRECORD_S
WHERE  (EXAMYEAR,EXAMCD)
       IN(SELECT DISTINCT EXAMYEAR,EXAMCD
          FROM   QRECORD_P_TMP)
AND    BUNDLECD LIKE '__aaa';

-- 設問別成績（高校）データの作成
-- （県コード＋’aaa'を一括コードとする）
-- （県コード99は対象外）
INSERT INTO QRECORD_S
SELECT EXAMYEAR,EXAMCD,(PREFCD || 'aaa'),SUBCD,QUESTION_NO,AVGPNT,NUMBERS,AVGSCORERATE
FROM   QRECORD_P_TMP
WHERE  PREFCD!='99';

-- 8年以上前のデータを削除
-- DELETE FROM QRECORD_P
-- WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 8);

-- 更新対象データの7年前のデータを削除
-- DELETE FROM QRECORD_P
-- WHERE  (EXAMYEAR,EXAMCD)
--        IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 7)),EXAMCD
--           FROM   QRECORD_P_TMP);

-- テンポラリテーブル削除
DROP TABLE QRECORD_P_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

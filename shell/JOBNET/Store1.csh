#! /bin/csh


set DIR = /keinavi/JOBNET
set Log = $DIR/Store.log
set temp = /keinavi/HULFT/WorkDir

###-------データ削除
#/home/ora_ins_account/DBdelete/SqlStart-while.csh 
#/home/ora_ins_account/DBdelete/SqlStart.csh 4 2005 78 74214

set EXAM = (200905)

set count = 1
while($count <= $#EXAM)

### ------データコピー
#	cp -p /HULFT/$EXAM[$count]/* $temp/
	cp -p $temp/200905selection/* $temp/

	echo "  " >> $Log

	#EXAMTAKENUM_A
	if (-f $temp/EXAMTAKENUM_A) then
		set size = `wc -cl $temp/EXAMTAKENUM_A`
		cd /keinavi/JOBNET/JNRZ0010
		if (-f JNRZ0010.bad) then
			\rm JNRZ0010.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] EXAMTAKENUM_A [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0010.sh
		if ($status == 30) then
			if (-f JNRZ0010.bad) then
				cp -p JNRZ0010.log JNRZ0010-$EXAM[$count].log
			endif
			echo "-----  EXAMTAKENUM_A ERROR   Log:JNRZ0010-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` EXAMTAKENUM_A end >> $Log
	endif

	#EXAMTAKENUM_P
	if (-f $temp/EXAMTAKENUM_P) then
		set size = `wc -cl $temp/EXAMTAKENUM_P`
		cd /keinavi/JOBNET/JNRZ0020
		if (-f JNRZ0020.bad) then
			\rm JNRZ0020.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] EXAMTAKENUM_P  [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0020.sh
		if ($status == 30) then
			if (-f JNRZ0020.bad) then
				cp -p JNRZ0020.log JNRZ0020-$EXAM[$count].log
			endif
			echo "-----  EXAMTAKENUM_P ERROR   Log:JNRZ0020-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` EXAMTAKENUM_P end >> $Log
	endif


	#EXAMTAKENUM_S
        if (-f $temp/EXAMTAKENUM_S) then
                set size = `wc -cl $temp/EXAMTAKENUM_S`
		cd /keinavi/JOBNET/JNRZ0030
		if (-f JNRZ0030.bad) then
			\rm JNRZ0030.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] EXAMTAKENUM_S [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0030.sh
		if ($status == 30) then
			if (-f JNRZ0030.bad) then
				cp -p JNRZ0030.log JNRZ0030-$EXAM[$count].log
			endif
			echo "-----  EXAMTAKENUM_S ERROR   Log:JNRZ0030-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` EXAMTAKENUM_S end >> $Log
	endif	

	#EXAMTAKENUM_C
        if (-f $temp/EXAMTAKENUM_C) then
                set size = `wc -cl $temp/EXAMTAKENUM_C`
		cd /keinavi/JOBNET/JNRZ0040
		if (-f JNRZ0040.bad) then
			\rm JNRZ0040.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] EXAMTAKENUM_C [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0040.sh
		if ($status == 30) then
			if (-f JNRZ0040.bad) then
				cp -p JNRZ0040.log JNRZ0040-$EXAM[$count].log
			endif
			echo "-----  EXAMTAKENUM_C ERROR   Log:JNRZ0040-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` EXAMTAKENUM_C end >> $Log
	endif

	#RATINGNUMBER_C
        if (-f $temp/RATINGNUMBER_C) then
		set size = `wc -cl $temp/RATINGNUMBER_C`
		cd /keinavi/JOBNET/JNRZ0210
		if (-f JNRZ0210.bad) then
			\rm JNRZ0210.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] RATINGNUMBER_C [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0210.sh
		if ($status == 30) then
			if (-f JNRZ0210.bad) then
				cp -p JNRZ0210.log JNRZ0210-$EXAM[$count].log
			endif
			echo "-----  RATINGNUMBER_C ERROR   Log:JNRZ0210-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` RATINGNUMBER_C end >> $Log
	endif


	#SUBRECORD_C
        if (-f $temp/SUBRECORD_C) then
                set size = `wc -cl $temp/SUBRECORD_C`
		cd /keinavi/JOBNET/JNRZ0080
		if (-f JNRZ0080.bad) then
			\rm JNRZ0080.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] SUBRECORD_C [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0080.sh
		if ($status == 30) then
			if (-f JNRZ0080.bad) then
				cp -p JNRZ0080.log JNRZ0080-$EXAM[$count].log
			endif
			echo "-----  SUBRECORD_C ERROR   Log:JNRZ0080-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` SUBRECORD_C end >> $Log
	endif


	#SUBDISTRECORD_C
	if (-f $temp/SUBDISTRECORD_C) then
		set size = `wc -cl $temp/SUBDISTRECORD_C`
		cd /keinavi/JOBNET/JNRZ0120
		if (-f JNRZ0120.bad ) then
			\rm JNRZ0120.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] SUBDISTRECORD_C [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0120.sh
		if ($status == 30) then
			if (-f JNRZ0120.bad ) then
				cp -p JNRZ0120.log JNRZ0120-$EXAM[$count].log
			endif
			echo "-----  SUBDISTRECORD_C ERROR   Log:JNRZ0120-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` SUBDISTRECORD_C end >> $Log
	endif


	#QRECORD_C
        if (-f $temp/QRECORD_C) then
                set size = `wc -cl $temp/QRECORD_C`
		cd /keinavi/JOBNET/JNRZ0160
		if (-f JNRZ0160.bad ) then
			\rm JNRZ0160.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] QRECORD_C [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0160.sh
		if ($status == 30) then
			if (-f JNRZ0160.bad ) then
				cp -p JNRZ0160.log JNRZ0160-$EXAM[$count].log
			endif
			echo "-----  QRECORD_A ERROR   Log:JNRZ0130-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` QRECORD_C end >> $Log
	endif


	#SUBRECORD_A
        if (-f $temp/SUBRECORD_A) then
                set size = `wc -cl $temp/SUBRECORD_A`
		cd /keinavi/JOBNET/JNRZ0050
		if (-f JNRZ0050.bad ) then
			\rm JNRZ0050.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] SUBRECORD_A [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0050.sh
		if ($status == 30) then
		if (-f JNRZ0050.bad ) then
			cp -p JNRZ0050.log JNRZ0050-$EXAM[$count].log
		endif
			echo "-----  SUBRECORD_A ERROR   Log:JNRZ0050-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` SUBRECORD_A end >> $Log
	endif

	#SUBRECORD_P
        if (-f $temp/SUBRECORD_P) then
                set size = `wc -cl $temp/SUBRECORD_P`
		cd /keinavi/JOBNET/JNRZ0060
		if (-f JNRZ0060.bad ) then
			\rm JNRZ0060.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] SUBRECORD_P [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0060.sh
		if ($status == 30) then
		if (-f JNRZ0060.bad ) then
			cp -p JNRZ0060.log JNRZ0060-$EXAM[$count].log
		endif
			echo "-----  SUBRECORD_P ERROR   Log:JNRZ0060-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` SUBRECORD_P end >> $Log
	endif

	#SUBRECORD_S
        if (-f $temp/SUBRECORD_S) then
                set size = `wc -cl $temp/SUBRECORD_S`
		cd /keinavi/JOBNET/JNRZ0070
		if (-f JNRZ0070.bad ) then
			\rm JNRZ0070.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] SUBRECORD_S [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0070.sh
		if ($status == 30) then
		if (-f JNRZ0070.bad ) then
			cp -p JNRZ0070.log JNRZ0070-$EXAM[$count].log
		endif
			echo "-----  SUBRECORD_S ERROR   Log:JNRZ0070-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S`   SUBRECORD_S end >> $Log
	endif

	#SUBDISTRECORD_A
        if (-f $temp/SUBDISTRECORD_A) then
                set size = `wc -cl $temp/SUBDISTRECORD_A`
		cd /keinavi/JOBNET/JNRZ0090
		if (-f JNRZ0090.bad ) then
			\rm JNRZ0090.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count]  SUBDISTRECORD_A [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0090.sh
		if ($status == 30) then
		if (-f JNRZ0090.bad ) then
			cp -p JNRZ0090.log JNRZ0090-$EXAM[$count].log
		endif
			echo "-----  SUBDISTRECORD_A ERROR   Log:JNRZ0090-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S`   SUBDISTRECORD_A end >> $Log
	endif

	#SUBDISTRECORD_P
        if (-f $temp/SUBDISTRECORD_P) then
                set size = `wc -cl $temp/SUBDISTRECORD_P`
		cd /keinavi/JOBNET/JNRZ0100
		if (-f JNRZ0100.bad ) then
			\rm JNRZ0100.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count]  SUBDISTRECORD_P [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0100.sh
		if ($status == 30) then
			if (-f JNRZ0100.bad ) then
				cp -p JNRZ0100.log JNRZ0100-$EXAM[$count].log
			endif
			echo "-----  SUBDISTRECORD_P ERROR   Log:JNRZ0100-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S`   SUBDISTRECORD_P end >> $Log
	endif

	#SUBDISTRECORD_S
        if (-f $temp/SUBDISTRECORD_S) then
                set size = `wc -cl $temp/SUBDISTRECORD_S`
		cd /keinavi/JOBNET/JNRZ0110
		if (-f JNRZ0110.bad ) then
			\rm JNRZ0110.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count]  SUBDISTRECORD_S [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0110.sh
		if ($status == 30) then
			if (-f JNRZ0110.bad ) then
				cp -p JNRZ0110.log JNRZ0110-$EXAM[$count].log
			endif
			echo "-----  SUBDISTRECORD_S ERROR   Log:JNRZ0110-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` SUBDISTRECORD_S end >> $Log
	endif

	#QRECORD_A
        if (-f $temp/QRECORD_A) then
                set size = `wc -cl $temp/QRECORD_A`
		cd /keinavi/JOBNET/JNRZ0130
		if (-f JNRZ0130.bad ) then
			\rm JNRZ0130.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] QRECORD_A [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0130.sh
		if ($status == 30) then
			if (-f JNRZ0130.bad ) then
				cp -p JNRZ0130.log JNRZ0130-$EXAM[$count].log
			endif
			echo "-----  QRECORD_A ERROR   Log:JNRZ0130-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` QRECORD_A end >> $Log
	endif

	#QRECORD_P
        if (-f $temp/QRECORD_P) then
                set size = `wc -cl $temp/QRECORD_P`
		cd /keinavi/JOBNET/JNRZ0140
		if (-f JNRZ0140.bad ) then
			\rm JNRZ0140.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] QRECORD_P [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0140.sh
		if ($status == 30) then
			if (-f JNRZ0140.bad ) then
				cp -p JNRZ0140.log JNRZ0140-$EXAM[$count].log
			endif
			echo "-----  QRECORD_P ERROR   Log:JNRZ0140-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` QRECORD_P end >> $Log
	endif

	#QRECORD_S
        if (-f $temp/QRECORD_S) then
                set size = `wc -cl $temp/QRECORD_S`
		cd /keinavi/JOBNET/JNRZ0150
		if (-f JNRZ0150.bad ) then
			\rm JNRZ0150.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] QRECORD_S [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0150.sh
		if ($status == 30) then
			if (-f JNRZ0150.bad ) then
				cp -p JNRZ0150.log JNRZ0150-$EXAM[$count].log
			endif
			echo "-----  QRECORD_S ERROR   Log:JNRZ0150-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` QRECORD_S end >> $Log
	endif


	#QRECORDZONE
        if (-f $temp/QRECORDZONE) then
                set size = `wc -cl $temp/QRECORDZONE`
		cd /keinavi/JOBNET/JNRZ0170
		if (-f JNRZ0170.bad ) then
			\rm JNRZ0170.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] QRECORDZONE [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0170.sh
		if ($status == 30) then
			if (-f JNRZ0170.bad ) then
				cp -p JNRZ0170.log JNRZ0170-$EXAM[$count].log
			endif
			echo "-----  QRECORDZONE ERROR   Log:JNRZ0170-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` QRECORDZONE end >> $Log
	endif

	#RATINGNUMBER_A
        if (-f $temp/RATINGNUMBER_A) then
                set size = `wc -cl $temp/RATINGNUMBER_A`
		cd /keinavi/JOBNET/JNRZ0180
		if (-f JNRZ0180.bad ) then
			\rm JNRZ0180.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] RATINGNUMBER_A [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0180.sh
		if ($status == 30) then
			if (-f JNRZ0180.bad ) then
				cp -p JNRZ0180.log JNRZ0180-$EXAM[$count].log
			endif
			echo "-----  RATINGNUMBER_A ERROR   Log:JNRZ0180-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` RATINGNUMBER_A end >> $Log
	endif

	#RATINGNUMBER_P
        if (-f $temp/RATINGNUMBER_P) then
                set size = `wc -cl $temp/RATINGNUMBER_P`
		cd /keinavi/JOBNET/JNRZ0190
		if (-f JNRZ0190.bad ) then
			\rm JNRZ0190.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] RATINGNUMBER_P [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0190.sh
		if ($status == 30) then
			if (-f JNRZ0190.bad ) then
				cp -p JNRZ0190.log JNRZ0190-$EXAM[$count].log
			endif
			echo "-----  RATINGNUMBER_P ERROR   Log:JNRZ0190-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` RATINGNUMBER_P end >> $Log
	endif

	#RATINGNUMBER_S
        if (-f $temp/RATINGNUMBER_S) then
                set size = `wc -cl $temp/RATINGNUMBER_S`
		cd /keinavi/JOBNET/JNRZ0200
		if (-f JNRZ0200.bad ) then
			\rm JNRZ0200.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] RATINGNUMBER_S [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0200.sh
		if ($status == 30) then
			if (-f JNRZ0200.bad ) then
				cp -p JNRZ0200.log JNRZ0200-$EXAM[$count].log
			endif
			echo "-----  RATINGNUMBER_S ERROR   Log:JNRZ0200-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` RATINGNUMBER_S end >> $Log
	endif

	#MARKQSCORERATE
        if (-f $temp/MARKQSCORERATE) then
                set size = `wc -cl $temp/MARKQSCORERATE`
		cd /keinavi/JOBNET/JNRZ0220
		if (-f JNRZ0220.bad ) then
			\rm JNRZ0220.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] MARKQSCORERATE [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0220.sh
		if (-f JNRZ0220.bad ) then
			cp -p JNRZ0220.log JNRZ0220-$EXAM[$count].log
			echo "----- MARKQSCORERATE  ERROR   Log:JNRZ0220-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` MARKQSCORERATE end >> $Log
	endif

	#MARKANSWER_NO
        if (-f $temp/MARKANSWER_NO) then
                set size = `wc -cl $temp/MARKANSWER_NO`
		cd /keinavi/JOBNET/JNRZ0230
		if (-f JNRZ0230.bad ) then
			\rm JNRZ0230.bad
		endif
		echo `date +%Y%m%d%k%M%S` " $EXAM[$count] MARKANSWER_NO [$size[1]]件 [$size[2]]byte start" >> $Log
		./JNRZ0230.sh
		if (-f JNRZ0230.bad ) then
			cp -p JNRZ0230.log JNRZ0230-$EXAM[$count].log
			echo "----- MARKANSWER_NO ERROR   Log:JNRZ0230-$EXAM[$count].log ---" >> $Log
		endif
		echo `date +%Y%m%d%k%M%S` MARKANSWER_NO end >> $Log
	endif

        #INDIVIDUALRECORD
        if (-f $temp/INDIVIDUALRECORD) then
                set size = `wc -cl $temp/INDIVIDUALRECORD`
                cd /keinavi/JOBNET/JNRZ0630
                if (-f JNRZ0630.bad) then
                        \rm JNRZ0630.bad
                endif
                echo `date +%Y%m%d%k%M%S` " $EXAM[$count] INDIVIDUALRECORD [$size[1]]件 [$size[2]]byte start" >> $Log
                ./JNRZ0630.sh
                if ($status == 30) then
                        cp -p JNRZ0630.log JNRZ0630-$EXAM[$count].log
                        echo "-----  INDIVIDUALRECORD ERROR   Log:JNRZ0630-$EXAM[$count].log ---" >> $Log
                endif
                echo `date +%Y%m%d%k%M%S` INDIVIDUALRECORD end >> $Log
#       \rm $temp/INDIVIDUALRECORD
        endif

	cd ../

#/keinavi/JOBNET/univ.csh $EXAM[$count]
#/keinavi/JOBNET/indiv.csh $EXAM[$count]
	@ count ++
end


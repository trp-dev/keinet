-- ************************************************
-- *機 能 名 : 模試受験者総数（クラス）
-- *機 能 ID : JNRZ0040_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE EXAMTAKENUM_C_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE EXAMTAKENUM_C_TMP  (
       EXAMYEAR                CHAR(4)           NOT NULL,   /*  模試年度           */
       EXAMCD                  CHAR(2)           NOT NULL,   /*  模試コード         */
       BUNDLECD                CHAR(5)           NOT NULL,   /*  一括コード         */
       GRADE                   NUMBER(2)         NOT NULL,   /*  学年               */
       CLASS                   CHAR(2)           NOT NULL,   /*  クラス             */
       NUMBERS                 NUMBER(4,0),                  /*  人数               */
       CONSTRAINT PK_EXAMTAKENUM_C_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  模試年度    */
                 EXAMCD                    ,               /*  模試コード  */
                 BUNDLECD                  ,               /*  一括コード  */
                 GRADE                     ,               /*  学年        */
                 CLASS                                     /*  クラス      */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

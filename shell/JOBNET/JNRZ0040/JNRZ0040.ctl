------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 模試受験者総数(クラス）
--  テーブル名      : EXAMTAKENUM_C
--  データファイル名: EXAMTAKENUM_C
--  機能            : 模試受験者総数(クラス）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
INSERT PRESERVE BLANKS
INTO TABLE EXAMTAKENUM_C_TMP
(  
   EXAMYEAR                POSITION(01:04)      CHAR,
   EXAMCD                  POSITION(05:06)      CHAR,
   BUNDLECD                POSITION(07:11)      CHAR,
   GRADE                   POSITION(12:13)      INTEGER EXTERNAL,
   CLASS                   POSITION(14:15)      CHAR,
   NUMBERS                 POSITION(16:19)      INTEGER EXTERNAL
)

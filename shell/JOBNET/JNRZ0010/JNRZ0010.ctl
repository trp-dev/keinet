------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 模試受験者総数(全国）
--  テーブル名      : EXAMTAKENUM_A
--  データファイル名: EXAMTAKENUM_A
--  機能            : 模試受験者総数(全国）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE EXAMTAKENUM_A_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   EXAMYEAR                POSITION(01:04)      CHAR,
   EXAMCD                  POSITION(05:06)      CHAR,
   STUDENTDIV              POSITION(07:07)      CHAR,
   NUMBERS                 POSITION(08:15)      INTEGER EXTERNAL "NULLIF(:NUMBERS,  '99999999')"
)

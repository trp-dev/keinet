-- ************************************************
-- *機 能 名 : 模試受験者総数（全国）
-- *機 能 ID : JNRZ0010_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE EXAMTAKENUM_A_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE EXAMTAKENUM_A_TMP  (
       EXAMYEAR                CHAR(4)           NOT NULL,   /*  模試年度           */
       EXAMCD                  CHAR(2)           NOT NULL,   /*  模試コード         */
       STUDENTDIV              CHAR(1)           NOT NULL,   /*  県コード           */
       NUMBERS                 NUMBER(8,0),                  /*  現役高卒区分       */
       CONSTRAINT PK_EXAMTAKENUM_A_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  模試年度    */
                 EXAMCD                    ,               /*  模試コード  */
                 STUDENTDIV                                /*  県コード    */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

-- ************************************************
-- *機 能 名 : 模試科目変換マスタ
-- *機 能 ID : JNRZ1070_01.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE INFORMATION_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE INFORMATION_TMP(  
       INFOID            CHAR(6)               NOT NULL,   /*   お知らせＩＤ       */
       DISPLAYDIV        CHAR(1)                       ,   /*   表示区分           */
       TITLE             VARCHAR2(60)                  ,   /*   タイトル           */
       DISPSTARTDTIM     CHAR(10)                      ,   /*   掲載開始日時       */
       DISPENDDTIM       CHAR(10)                      ,   /*   掲載終了日時       */
       TEXT              VARCHAR2(1000)                ,   /*   本文               */
    CONSTRAINT PK_INFORMATION_TMP PRIMARY KEY(
            INFOID                                          /*   お知らせＩＤ       */
    )
    USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;
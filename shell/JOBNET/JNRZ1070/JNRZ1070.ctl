------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : お知らせマスタ
--  テーブル名      : INFORMATION_TMP
--  データファイル名: INFORMATIONyyyymmdd.csv
--  機能            : お知らせマスタをデータファイルの内容に置き換える
--  作成日          : 2004/08/25
--  修正日          : 2004/10/11
--  備考            : 物理レコード数行を１論理レコードするため、論理レコードの
--                  : 行末を'"'にて判定する。
--                  : 項目の追加などがある場合は、TEXT項目を必ず最後にするなど
--                  : 注意が必要。
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
CONTINUEIF LAST!='"'
PRESERVE BLANKS
INTO TABLE INFORMATION_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    INFOID "LTRIM(TO_CHAR(TO_NUMBER(:INFOID),'099999'))",
    DISPLAYDIV,
    TITLE,
    DISPSTARTDTIM,
    DISPENDDTIM,
    TEXT CHAR(1000)
)

------------------------------------------------------------------------------
-- *機 能 名 : 英語認定試験別・合計CEFR取得状況（高校）のCTLファイル
-- *機 能 ID : JNRZ1260.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE ENGPT_CEFRACQSTATUS_S_TMP
(  
   EXAMYEAR     POSITION(01:04)  CHAR 
 , EXAMCD       POSITION(05:06)  CHAR 
 , BUNDLECD     POSITION(07:11)  CHAR 
 , ENGPTCD      POSITION(12:13)  CHAR 
 , ENGPTLEVELCD POSITION(14:15)  CHAR 
 , CEFRLEVELCD  POSITION(16:17)  CHAR 
 , NUMBERS      POSITION(18:24)  INTEGER EXTERNAL 
 , COMPRATIO    POSITION(25:28)  INTEGER EXTERNAL "DECODE(:COMPRATIO,    '9999', null, :COMPRATIO/10)"
)

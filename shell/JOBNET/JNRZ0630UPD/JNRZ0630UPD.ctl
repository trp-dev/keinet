------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : Kei-Netサービス移行データ
--  テーブル名      : 
--  データファイル名: [学校コード].csv
--  機能            : 既存Kei-Netサービスにて行われた名寄せデータを
--                    一時テーブルにインポートする
--  作成日          : 2004/10/01
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE JNRZ0630UPD_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   EXAMYEAR         POSITION(01:04)  CHAR,
   EXAMCD           POSITION(05:06)  CHAR,
   SCHOOLCD         POSITION(07:11)  CHAR,
   GRADE            POSITION(12:13)  CHAR,
   CLASS            POSITION(14:18)  CHAR "NVL(:CLASS,' ')",
   CLASS_NO         POSITION(19:23)  CHAR,
   SEX              POSITION(24:24)  CHAR,
   NAME_KANA        POSITION(25:54)  CHAR,
   CUSTOMER_NO      POSITION(55:65)  CHAR
)

-- *********************************************************
-- *機 能 名 : Kei-Netサービス移行データ
-- *機 能 ID : JNRZ0600_02.sql
-- *作 成 日 : 2004/08/31
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- PL/SQLブロックからの表示をONにする
SET ECHO ON
SET LINESIZE 200
SET SERVEROUTPUT ON

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 移行データの名寄せ処理
DECLARE
    i_s_individualid    CHAR(10);               -- 個表：高校用個人ID
    i_attendgrade       CHAR(2);                -- 個表：受講学年
    i_student_no        CHAR(6);                -- 個表：学籍番号
    i_region            CHAR(1);                -- 個表：所属地区
    i_schoolhouse       CHAR(3);                -- 個表：所属校舎
    b_individualid      CHAR(10);               -- 学籍基本情報：個人ID
    b_rowid             ROWID;                  -- 学籍基本情報：既存レコードのROWID退避用
    h_individualid      CHAR(10);               -- 学籍履歴情報：個人ID
    h_rowid             ROWID;                  -- 学籍履歴情報：既存レコードのROWID退避用
    n_updflg            NUMBER(1);              -- 更新対象フラグ：0=非対象/1=対象
    wk_i_rowid          ROWID;                  -- 個表：既存レコードのROWID退避用
    wk_i_s_individualid CHAR(10);               -- 個表：高校用個人ID(一時保持用)
    wk_h_individualid   CHAR(10);               -- 学籍履歴情報：個人ID(一時保持用)
    wk_rowid            ROWID;                  -- 個表：既存レコードのROWID退避用
    log_file            UTL_FILE.FILE_TYPE;     -- ログファイル

-- メイン処理
BEGIN

    -- ログファイルオープン(ディレクトリの指定は大文字にすること)
    log_file := UTL_FILE.FOPEN ('JOBLOGDIR', 'JNRZ0630UPD.log', 'W', 1000);

    -- 移行データ読み込み
    DBMS_OUTPUT.PUT_LINE('データ移行処理開始');
    UTL_FILE.PUT_LINE(log_file,'データ移行処理開始');
    FOR r_cur IN (SELECT EXAMYEAR,EXAMCD,SCHOOLCD,GRADE,CLASS,CLASS_NO,CUSTOMER_NO,NAME_KANA
                  FROM   JNRZ0630UPD_TMP)
    LOOP

        UTL_FILE.PUT_LINE(log_file,'移行データ '   ||
                             ' [模試年度:'   || TO_CHAR(r_cur.EXAMYEAR)    ||
                             '][模試コード:' || TO_CHAR(r_cur.EXAMCD)      ||
                             '][学校コード:' || TO_CHAR(r_cur.SCHOOLCD)    ||
                             '][学年:'       || TO_CHAR(r_cur.GRADE)       ||
                             '][クラス:'     || TO_CHAR(r_cur.CLASS)       ||
                             '][クラス番号:' || TO_CHAR(r_cur.CLASS_NO)    ||
                             '][顧客番号:'   || TO_CHAR(r_cur.CUSTOMER_NO) ||
                             '][カナ氏名:'   || TO_CHAR(r_cur.NAME_KANA)   || ']');

        -- 更新対象フラグクリア
        n_updflg := 0;

        -- 個表データ検索処理
        BEGIN
            -- 個表データ検索
            SELECT ROWID,   NVL(S_INDIVIDUALID,''),ATTENDGRADE,  STUDENT_NO,  REGION,  SCHOOLHOUSE
            INTO   wk_rowid,i_s_individualid,      i_attendgrade,i_student_no,i_region,i_schoolhouse
            FROM   INDIVIDUALRECORD
            WHERE  (EXAMYEAR    = r_cur.EXAMYEAR)
            AND    (EXAMCD      = r_cur.EXAMCD)
            AND    (CUSTOMER_NO = r_cur.CUSTOMER_NO);

            UTL_FILE.PUT_LINE(log_file,'個表データあり '           ||
                                 ' [高校用個人ID:'           || TO_CHAR(i_s_individualid)  ||
                                 '][受講学年:'               || TO_CHAR(i_attendgrade)     ||
                                 '][学籍番号:'               || TO_CHAR(i_student_no)      ||
                                 '][所属地区:'               || TO_CHAR(i_region)          ||
                                 '][所属校舎:'               || TO_CHAR(i_schoolhouse)     ||
                                 '][ROWID:'                  || TO_CHAR(wk_rowid)          ||
                                 '] <= 検索条件 [模試年度='  || TO_CHAR(r_cur.EXAMYEAR)    ||
                                 '][模試コード='             || TO_CHAR(r_cur.EXAMCD)      ||
                                 '][顧客番号='               || TO_CHAR(r_cur.CUSTOMER_NO) || ']');

            -- 学籍基本情報検索処理
            BEGIN
                -- 学籍基本情報検索
                SELECT NVL(INDIVIDUALID,''), ROWID
                INTO   b_individualid,       b_rowid
                FROM   BASICINFO
                WHERE  (SCHOOLCD    = r_cur.SCHOOLCD)
                AND    (CUSTOMER_NO = r_cur.CUSTOMER_NO);

                UTL_FILE.PUT_LINE(log_file,'学籍基本情報あり '           ||
                                     ' [個人ID:'                   || TO_CHAR(b_individualid) ||
                                     '] <= 検索条件 [学校コード='  || TO_CHAR(r_cur.SCHOOLCD)            ||
                                     '][顧客番号='                 || TO_CHAR(r_cur.CUSTOMER_NO)         || ']');


                -- 学籍基本情報更新
                BEGIN
                    UPDATE BASICINFO SET
                           NAME_KANA  = r_cur.NAME_KANA
                    WHERE  ROWID = b_rowid;

                    UTL_FILE.PUT_LINE(log_file,'学籍基本情報更新' ||
                                         ' [カナ氏名='            || TO_CHAR(r_cur.NAME_KANA)  ||
                                         '] <= 検索条件 [ROWID='  || TO_CHAR(b_rowid)          || ']');

                EXCEPTION
                    -- エラーEXCEPTIONを親に投げる
                    WHEN OTHERS THEN
                        UTL_FILE.PUT_LINE(log_file,'学籍基本情報更新処理にてエラー発生');
                        RAISE;
                END;

                -- 学籍履歴情報検索処理
                BEGIN
                    -- 学籍履歴情報検索
                    SELECT NVL(INDIVIDUALID,''), ROWID
                    INTO   h_individualid,       h_rowid
                    FROM   HISTORYINFO
                    WHERE  (YEAR         = r_cur.EXAMYEAR)
                    AND    (INDIVIDUALID = b_individualid);

                    UTL_FILE.PUT_LINE(log_file,'学籍履歴情報あり '    ||
                                         ' [個人ID:'            || TO_CHAR(h_individualid) ||
                                         '] <= 検索条件 [年度=' || TO_CHAR(r_cur.EXAMYEAR) ||
                                         '][個人ID='            || TO_CHAR(b_individualid) || ']');


                    -- 学籍履歴情報更新
                    BEGIN
                        UPDATE HISTORYINFO SET
                               GRADE    = r_cur.GRADE,
                               CLASS    = SUBSTR(r_cur.CLASS,4,2),
                               CLASS_NO = r_cur.CLASS_NO
                        WHERE  ROWID = h_rowid;

                        UTL_FILE.PUT_LINE(log_file,'学籍履歴情報更新' ||
                                                   ' [学年='       || TO_CHAR(r_cur.GRADE)                ||
                                                   '][クラス='     || TO_CHAR(SUBSTR(r_cur.CLASS,4,2))    ||
                                                   '][クラス番号=' || TO_CHAR(r_cur.CLASS_NO)             ||
                                                   '] <= 検索条件 [ROWID='  || TO_CHAR(h_rowid)           || ']');

                    EXCEPTION
                        -- エラーEXCEPTIONを親に投げる
                        WHEN OTHERS THEN
                            UTL_FILE.PUT_LINE(log_file,'学籍履歴情報更新処理にてエラー発生');
                            RAISE;
                    END;

                    -- 学籍基本情報と学籍履歴情報の個人IDが一致
                    -- 且つ 個表：高校用個人IDと異なる場合
                    IF  (b_individualid = h_individualid)
                    AND (b_individualid != i_s_individualid) THEN

                        -- 更新対象フラグON
                        n_updflg := 1;

                    END IF;

                -- 学籍履歴情報検索処理：例外処理
                EXCEPTION
                    -- なければ別条件で学籍履歴情報検索
                    WHEN NO_DATA_FOUND THEN
                        UTL_FILE.PUT_LINE(log_file,'学籍履歴情報なし => 別条件にて再検索');
                        -- 学籍履歴情報再検索処理
                        BEGIN
                            -- 学籍履歴情報再検索
                            SELECT NVL(INDIVIDUALID,'')
                            INTO   wk_h_individualid
                            FROM   HISTORYINFO
                            WHERE  (INDIVIDUALID = b_individualid)
                            AND    (YEAR         = r_cur.EXAMYEAR);

                            UTL_FILE.PUT_LINE(log_file,'学籍履歴情報あり（再検索 => 処理なし） ' ||
                                                 ' [個人ID:'              || TO_CHAR(wk_h_individualid) ||
                                                 '] <= 検索条件 [個人ID=' || TO_CHAR(b_individualid)    ||
                                                 '][年度='                || TO_CHAR(r_cur.EXAMYEAR)    || ']');

                        -- 学籍履歴情報再検索処理：例外処理
                        EXCEPTION
                            -- ない場合にレコード作成
                            WHEN NO_DATA_FOUND THEN

                                -- 学籍履歴情報作成
                                INSERT INTO HISTORYINFO
                                VALUES (b_individualid,
                                        r_cur.EXAMYEAR,
                                        r_cur.GRADE,
                                        SUBSTR(r_cur.CLASS,4,2),
                                        r_cur.CLASS_NO,
                                        i_student_no,
                                        i_attendgrade,
                                        i_region,
                                        i_schoolhouse
                                );

                                UTL_FILE.PUT_LINE(log_file,'学籍履歴情報作成 ' ||
                                                     ' [個人ID='     || TO_CHAR(b_individualid) ||
                                                     '][年度='       || TO_CHAR(r_cur.EXAMYEAR) ||
                                                     '][学年='       || TO_CHAR(r_cur.GRADE)    ||
                                                     '][クラス='     || TO_CHAR(SUBSTR(r_cur.CLASS,4,2))    ||
                                                     '][クラス番号=' || TO_CHAR(r_cur.CLASS_NO) ||
                                                     '][学籍番号='   || TO_CHAR(i_student_no)   ||
                                                     '][受講学年='   || TO_CHAR(i_attendgrade)  ||
                                                     '][所属地区='   || TO_CHAR(i_region)       ||
                                                     '][所属校舎='   || TO_CHAR(i_schoolhouse)  || ']');

                                -- 更新対象フラグON
                                n_updflg := 1;

                            -- エラーEXCEPTIONを親に投げる
                            WHEN OTHERS THEN
                                UTL_FILE.PUT_LINE(log_file,'学籍履歴情報検索（再検索）にてエラー発生');
                                RAISE;
                        END;

                    -- 複数件のレコードが返った場合も何もしない
                    WHEN TOO_MANY_ROWS THEN
                        NULL;

                    -- エラーEXCEPTIONを親に投げる
                    WHEN OTHERS THEN
                        UTL_FILE.PUT_LINE(log_file,'学籍履歴情報検索にてエラー発生');
                        RAISE;
                END;

            -- 学籍基本情報検索処理：例外処理
            EXCEPTION
                -- なければ何もしない
                WHEN NO_DATA_FOUND THEN
                    UTL_FILE.PUT_LINE(log_file,'学籍基本情報なし');
                    NULL;

                -- エラーEXCEPTIONを親に投げる
                WHEN OTHERS THEN
                    UTL_FILE.PUT_LINE(log_file,'学籍基本情報検索にてエラー発生');
                    RAISE;
            END;

        -- 個表データ検索処理：例外処理
        EXCEPTION
            -- なければ何もしない
            WHEN NO_DATA_FOUND THEN
                UTL_FILE.PUT_LINE(log_file,'個表データなし');
                NULL;

            -- エラーEXCEPTIONを親に投げる
            WHEN OTHERS THEN
                UTL_FILE.PUT_LINE(log_file,'個表データ検索にてエラー発生');
                RAISE;
        END;

        -- 更新対象フラグがONであれば
        -- 個表データを別条件で再検索
        IF (n_updflg = 0) THEN
            UTL_FILE.PUT_LINE(log_file,'個表データ再検索処理なし');

            -- 個表データ：個人IDの更新処理
            BEGIN
                UPDATE INDIVIDUALRECORD SET
                       GRADE          = r_cur.GRADE,
                       CLASS          = SUBSTR(r_cur.CLASS,4,2),
                       CLASS_NO       = r_cur.CLASS_NO,
                       NAME_KANA      = r_cur.NAME_KANA
                WHERE  ROWID = wk_rowid;

                UTL_FILE.PUT_LINE(log_file,'個表データ更新' ||
                                           ' [学年='        || TO_CHAR(r_cur.GRADE)                ||
                                           '][クラス='      || TO_CHAR(SUBSTR(r_cur.CLASS,4,2))    ||
                                           '][クラス番号='  || TO_CHAR(r_cur.CLASS_NO)             ||
                                           '][カナ氏名='    || TO_CHAR(r_cur.NAME_KANA)            ||
                                           '] <= 検索条件 [ROWID='  || TO_CHAR(wk_rowid)           || ']');

            EXCEPTION
                -- エラーEXCEPTIONを親に投げる
                WHEN OTHERS THEN
                    UTL_FILE.PUT_LINE(log_file,'個表データ更新処理にてエラー発生');
                    RAISE;
            END;

        ELSE
            UTL_FILE.PUT_LINE(log_file,'個表データ再検索処理あり');

            -- 個表データ再検索処理
            BEGIN
                SELECT NVL(S_INDIVIDUALID,''), ROWID
                INTO   wk_i_s_individualid,    wk_i_rowid
                FROM   INDIVIDUALRECORD
                WHERE  (EXAMYEAR       = r_cur.EXAMYEAR)
                AND    (EXAMCD         = r_cur.EXAMCD)
                AND    (S_INDIVIDUALID = b_individualid);

                UTL_FILE.PUT_LINE(log_file,'個表データあり（再検索 => 処理なし） ' ||
                                     ' [高校用個人ID:'           || TO_CHAR(wk_i_s_individualid) ||
                                     '] <= 検索条件 [模試年度='  || TO_CHAR(r_cur.EXAMYEAR)      ||
                                     '][模試コード='             || TO_CHAR(r_cur.EXAMCD)        ||
                                     '][高校用個人ID='           || TO_CHAR(b_individualid)      || ']');

                -- 個表データ更新
                BEGIN
                    UPDATE INDIVIDUALRECORD SET
                           GRADE          = r_cur.GRADE,
                           CLASS          = SUBSTR(r_cur.CLASS,4,2),
                           CLASS_NO       = r_cur.CLASS_NO,
                           NAME_KANA      = r_cur.NAME_KANA
                    WHERE  ROWID = wk_i_rowid;

                    UTL_FILE.PUT_LINE(log_file,'個表データ更新' ||
                                               ' [学年='       || TO_CHAR(r_cur.GRADE)                ||
                                               '][クラス='     || TO_CHAR(SUBSTR(r_cur.CLASS,4,2))    ||
                                               '][クラス番号=' || TO_CHAR(r_cur.CLASS_NO)             ||
                                               '][カナ氏名='   || TO_CHAR(r_cur.NAME_KANA)            ||
                                               '] <= 検索条件 [ROWID='  || TO_CHAR(wk_i_rowid)        || ']');

                EXCEPTION
                    -- エラーEXCEPTIONを親に投げる
                    WHEN OTHERS THEN
                        UTL_FILE.PUT_LINE(log_file,'個表データ更新処理にてエラー発生');
                        RAISE;
                END;

            -- 個表データ再検索処理：例外処理
            EXCEPTION
                -- 個表データがない場合
                WHEN NO_DATA_FOUND THEN
                    UTL_FILE.PUT_LINE(log_file,'個表データなし（再検索 => 科目別成績、設問別成績、志望校評価、個表データ更新）');

                    -- 科目別成績：個人IDの更新処理
                    BEGIN
                        UPDATE SUBRECORD_I SET
                               INDIVIDUALID  = b_individualid
                        WHERE  (INDIVIDUALID = i_s_individualid)
                        AND    (EXAMYEAR     = r_cur.EXAMYEAR)
                        AND    (EXAMCD       = r_cur.EXAMCD);

                        UTL_FILE.PUT_LINE(log_file,'科目別成績更新 ' ||
                                             ' [個人ID='              || TO_CHAR(b_individualid)   ||
                                             '] <= 検索条件 [個人ID=' || TO_CHAR(i_s_individualid) ||
                                             '][模試年度='            || TO_CHAR(r_cur.EXAMYEAR)   ||
                                             '][模試コード='          || TO_CHAR(r_cur.EXAMCD)     || ']');

                    EXCEPTION
                        -- エラーEXCEPTIONを親に投げる
                        WHEN OTHERS THEN
                            UTL_FILE.PUT_LINE(log_file,'科目別成績更新処理にてエラー発生');
                            RAISE;
                    END;

                    -- 設問別成績：個人IDの更新処理
                    BEGIN
                        UPDATE QRECORD_I SET
                               INDIVIDUALID  = b_individualid
                        WHERE  (INDIVIDUALID = i_s_individualid)
                        AND    (EXAMYEAR     = r_cur.EXAMYEAR)
                        AND    (EXAMCD       = r_cur.EXAMCD);

                        UTL_FILE.PUT_LINE(log_file,'設問別成績更新 '  ||
                                             ' [個人ID='              || TO_CHAR(b_individualid)   ||
                                             '] <= 検索条件 [個人ID=' || TO_CHAR(i_s_individualid) ||
                                             '][模試年度='            || TO_CHAR(r_cur.EXAMYEAR)   ||
                                             '][模試コード='          || TO_CHAR(r_cur.EXAMCD)     || ']');

                    EXCEPTION
                        -- エラーEXCEPTIONを親に投げる
                        WHEN OTHERS THEN
                            UTL_FILE.PUT_LINE(log_file,'設問別成績更新処理にてエラー発生');
                            RAISE;
                    END;

                    -- 志望校評価：個人IDの更新処理
                    BEGIN
                        UPDATE CANDIDATERATING SET
                               INDIVIDUALID  = b_individualid
                        WHERE  (INDIVIDUALID = i_s_individualid)
                        AND    (EXAMYEAR     = r_cur.EXAMYEAR)
                        AND    (EXAMCD       = r_cur.EXAMCD);

                        UTL_FILE.PUT_LINE(log_file,'志望校評価更新 ' ||
                                             ' [個人ID='              || TO_CHAR(b_individualid)   ||
                                             '] <= 検索条件 [個人ID=' || TO_CHAR(i_s_individualid) ||
                                             '][模試年度='            || TO_CHAR(r_cur.EXAMYEAR)   ||
                                             '][模試コード='          || TO_CHAR(r_cur.EXAMCD)     || ']');

                    EXCEPTION
                        -- エラーEXCEPTIONを親に投げる
                        WHEN OTHERS THEN
                            UTL_FILE.PUT_LINE(log_file,'志望校評価更新処理にてエラー発生');
                            RAISE;
                    END;

                    -- 個表データ：個人IDの更新処理
                    BEGIN
                        UPDATE INDIVIDUALRECORD SET
                               S_INDIVIDUALID = b_individualid,
                               GRADE          = r_cur.GRADE,
                               CLASS          = SUBSTR(r_cur.CLASS,4,2),
                               CLASS_NO       = r_cur.CLASS_NO,
                               NAME_KANA      = r_cur.NAME_KANA
                        WHERE  ROWID = wk_rowid;

                        UTL_FILE.PUT_LINE(log_file,'個表データ更新' ||
                                                   ' [個人ID='     || TO_CHAR(b_individualid)             ||
                                                   '][学年='       || TO_CHAR(r_cur.GRADE)                ||
                                                   '][クラス='     || TO_CHAR(SUBSTR(r_cur.CLASS,4,2))    ||
                                                   '][クラス番号=' || TO_CHAR(r_cur.CLASS_NO)             ||
                                                   '][カナ氏名='   || TO_CHAR(r_cur.NAME_KANA)            ||
                                                   '] <= 検索条件 [ROWID='  || TO_CHAR(wk_rowid)          || ']');


                    EXCEPTION
                        -- エラーEXCEPTIONを親に投げる
                        WHEN OTHERS THEN
                            UTL_FILE.PUT_LINE(log_file,'個表データ更新処理にてエラー発生');
                            RAISE;
                    END;

                -- エラーEXCEPTIONを親に投げる
                WHEN OTHERS THEN
                    UTL_FILE.PUT_LINE(log_file,'個表データ検索処理（再検索）にてエラー発生');
                    RAISE;
            END;

        END IF;

    -- 移行データループ
    END LOOP;

    -- 更新の確定
    COMMIT;
    UTL_FILE.PUT_LINE(log_file,'データ移行処理が正常に終了しました');

    -- ログファイルクローズ
    UTL_FILE.FFLUSH(log_file);
    UTL_FILE.FCLOSE(log_file);

-- メイン処理：例外処理
EXCEPTION
    -- ログファイルのパス指定エラー
    WHEN UTL_FILE.INVALID_PATH THEN
        DBMS_OUTPUT.PUT_LINE('ログファイルのパス指定に誤りがあります');

    -- ログファイルの操作エラー
    WHEN UTL_FILE.INVALID_OPERATION THEN
        DBMS_OUTPUT.PUT_LINE('ログファイルのパス指定に誤りがあります');

    -- エラーEXCEPTIONを親に投げる
    WHEN OTHERS THEN
        IF UTL_FILE.IS_OPEN(log_file) THEN
            UTL_FILE.FFLUSH(log_file);
            UTL_FILE.FCLOSE(log_file);
        END IF;

        DBMS_OUTPUT.PUT_LINE('データ移行処理にてエラーが発生しました');
        UTL_FILE.PUT_LINE(log_file,'データ移行処理にてエラーが発生しました');
        RAISE;
END;
/

-- SQL*PLUS終了
EXIT;

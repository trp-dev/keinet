-- ************************************************
-- *機 能 名 : Kei-Netサービス移行データ
-- *機 能 ID : JNRZ0630UPD_01.sql
-- *作 成 日 : 2004/10/01
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE JNRZ0630UPD_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE JNRZ0630UPD_TMP  (
       EXAMYEAR                CHAR(4)           NOT NULL,   /*  模試年度           */
       EXAMCD                  CHAR(2)           NOT NULL,   /*  模試コード         */
       SCHOOLCD                CHAR(5)                   ,   /*  学校コード         */
       GRADE                   NUMBER(2)                 ,   /*  学年               */
       CLASS                   CHAR(5)                   ,   /*  クラス             */
       CLASS_NO                CHAR(5)                   ,   /*  クラス番号         */
       SEX                     CHAR(1)                   ,   /*  性別               */
       NAME_KANA               VARCHAR2(30)              ,   /*  カナ氏名           */
       CUSTOMER_NO             CHAR(11)          NOT NULL,   /*  顧客番号           */
       CONSTRAINT PK_JNRZ0630UPD_TMP PRIMARY KEY (
           EXAMYEAR,                                         /*  模試年度           */
           EXAMCD,                                           /*  模試コード         */
           CUSTOMER_NO                                       /*  顧客番号           */
       )
       USING INDEX TABLESPACE     KEINAVI_STG_INDX
)
       TABLESPACE       KEINAVI_STG_DATA;

-- SQL*PLUS終了
EXIT;

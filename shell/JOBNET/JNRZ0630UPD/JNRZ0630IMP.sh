#!/bin/sh

#一時ファイル名
TMPTILE=$JOBNAME.tmp

#SQL*LOADER用ファイル
CTLFILE=$JOBNAME.ctl
LOGFILE=$JOBNAME.log
BADFILE=$JOBNAME.bad
DSCFILE=$JOBNAME.dsc
CSVLIST=${SRCDIR}/CSVLIST.txt
RESULT=0

#以前の不要ファイルの削除
rm -f $WORKDIR/$TMPTILE
rm -f $WORKDIR/$LOGFILE
rm -f $WORKDIR/$BADFILE
rm -f $WORKDIR/$DSCFILE

#該当ファイルの有無を検索
COUNT=1
ls ${SRCDIR}/${CSVFILE} > ${CSVLIST}
FCOUNT=`cat ${CSVLIST} | wc -l`

#該当ファイルがあれば
if [ "${FCOUNT}" -gt 0 ]
then

    #一時ファイルの作成
    touch $WORKDIR/$TMPTILE

    #ファイルリストがある間
    while test ${COUNT} -le ${FCOUNT}
    do

        #マスタファイル名を取得
        DATFILE=`sed -n ${COUNT}p < ${CSVLIST}`

        #SQL*LOADER起動
        echo ${DATFILE}
        sqlldr PARFILE=/keinavi/JOBNET/config/keinavi.par CONTROL=$WORKDIR/$CTLFILE DATA=$DATFILE LOG=$WORKDIR/$LOGFILE${COUNT} ERRORS=1000000 ROWS=1000

        #SQL*LOADER戻り値チェック(0以外は異常終了)
        if [ $? -ne 0 ]
        then
#            echo SQL*LOADERでエラーが発生しました
            exit 30
        fi

        #SQL*PLUSデータインポート->テンポラリテーブル削除
        /keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ0630UPD_02.sql
        RESULT=$?

        mv /keinavi/JOBNET/JOBLOG/JNRZ0630UPD.log  /keinavi/JOBNET/JOBLOG/JNRZ0630UPD.log${COUNT}

        #SQL*PLUS戻り値チェック(0以外は異常終了)
        if [ ${RESULT} -ne 0 ]
        then
            echo データのインポート処理でエラーが発生しました
            exit 30
        fi

        #カウンタアップ
        COUNT=`expr $COUNT + 1`
    done

    #一時ファイルの削除
    rm -f $WORKDIR/$TMPTILE

    #終了コード発行
    if [ -f $WORKDIR/$TMPTILE -o -f $WORKDIR/$BADFILE ]
    then
#        echo データのインポート処理でエラーが発生しました
        exit 30    #異常終了
#    else
#        echo データのインポート処理が正常に終了しました
        exit  0    #正常終了
    fi
else
    #該当ファイルがない場合はエラー
    echo データファイルが見つかりません
    exit 30    #異常終了
fi

------------------------------------------------------------------------------
--  システム名      : PC版模試判定バンザイ統合
--  概念ファイル名  : 
--  テーブル名      : DB10_BIKOU -> UNIVINFO_TMP_DB10_BIKOU
--  機能            : 
--  作成日          : 2013/07/29
--  修正日          : 
--  備考            : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE UNIVINFO_TMP_DB10_BIKOU_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    NENDO,
    BIKOUCODE,
    BIKOUMEI1,
    BIKOUMEI2,
    N_KUBUN,
    VOL3_KUBUN,
    MEMO_KUBUN,
    CHECK_PATTERN1,
    CHECK_PATTERN2,
    CHECK_PATTERN3,
    CHECK_PATTERN4,
    CHECK_PATTERN5,
    CREATE_DATE,
    CREATE_SYOKUINCODE,
    UPDATE_DATE,
    UPDATE_SYOKUINCODE
)

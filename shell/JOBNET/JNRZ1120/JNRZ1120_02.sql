-- *********************************************************
-- *機 能 名 : 大学系統マスタ
-- *機 能 ID : UPD_UNIVSTEMMA.sql
-- *作 成 日 : 2004/08/31
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 大学系統マスタの更新処理
DECLARE
    wk_rowid    ROWID;          -- 既存レコードのROWID
BEGIN
    FOR r_cap IN (SELECT USERID,
                         SCHOOLCD,
                         PACTDIV,
                         MAXSESSION,
                         REALTIMESESSION,
                         CERT_CN,
                         CERT_OU,
                         CERT_O,
                         CERT_L,
                         CERT_S,
                         CERT_C,
                         CERT_BEFOREDATA,
                         CERT_USERDIV,
                         CERT_FLG,
                         ISSUED_BEFOREDATE,
                         PACTTERM,
                         REGISTRYDATE,
                         RENEWALDATE,
                         LOGINPWD,
                         DEFAULTPWD,
                         FEATPROVFLG
                FROM     PACTSCHOOL_TMP
                WHERE    CERT_USERDIV!='6')
    LOOP
        BEGIN
            /* 既存レコードを検索 */
            SELECT ROWID INTO wk_rowid
            FROM   PACTSCHOOL
            WHERE  USERID    = r_cap.USERID;

            /* あれば更新 */
            UPDATE PACTSCHOOL SET
                   SCHOOLCD          = r_cap.SCHOOLCD,
                   PACTDIV           = r_cap.PACTDIV,
                   MAXSESSION        = r_cap.MAXSESSION,
                   REALTIMESESSION   = r_cap.REALTIMESESSION,
                   CERT_CN           = r_cap.CERT_CN,
                   CERT_OU           = r_cap.CERT_OU,
                   CERT_O            = r_cap.CERT_O,
                   CERT_L            = r_cap.CERT_L,
                   CERT_S            = r_cap.CERT_S,
                   CERT_C            = r_cap.CERT_C,
                   CERT_BEFOREDATA   = r_cap.CERT_BEFOREDATA,
                   CERT_USERDIV      = r_cap.CERT_USERDIV,
                   CERT_FLG          = r_cap.CERT_FLG,
                   ISSUED_BEFOREDATE = r_cap.ISSUED_BEFOREDATE,
                   PACTTERM          = r_cap.PACTTERM,
                   REGISTRYDATE      = r_cap.REGISTRYDATE,
                   RENEWALDATE       = r_cap.RENEWALDATE,
                   LOGINPWD          = r_cap.LOGINPWD,
                   DEFAULTPWD        = r_cap.DEFAULTPWD,
                   FEATPROVFLG       = r_cap.FEATPROVFLG
            WHERE  ROWID     = wk_rowid;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                /* なければ新規作成 */
                INSERT INTO PACTSCHOOL
                VALUES (
                   r_cap.USERID,
                   r_cap.SCHOOLCD,
                   r_cap.PACTDIV,
                   r_cap.MAXSESSION,
                   r_cap.REALTIMESESSION,
                   r_cap.CERT_CN,
                   r_cap.CERT_OU,
                   r_cap.CERT_O,
                   r_cap.CERT_L,
                   r_cap.CERT_S,
                   r_cap.CERT_C,
                   r_cap.CERT_BEFOREDATA,
                   r_cap.CERT_USERDIV,
                   r_cap.CERT_FLG,
                   r_cap.ISSUED_BEFOREDATE,
                   r_cap.PACTTERM,
                   r_cap.REGISTRYDATE,
                   r_cap.RENEWALDATE,
                   r_cap.LOGINPWD,
                   r_cap.DEFAULTPWD,
                   r_cap.FEATPROVFLG
                );
            WHEN OTHERS THEN
                RAISE;
        END;

    END LOOP;

EXCEPTION
    WHEN OTHERS THEN
        RAISE;
END;
/

-- マスタから削除されたデータの削除
-- （証明書ユーザー区分=6:ヘルプデスクは対象外とする）
DELETE FROM PACTSCHOOL
WHERE  PACTSCHOOL.USERID IN (SELECT PACTSCHOOL.USERID
                             FROM   PACTSCHOOL
                             WHERE  (NOT EXISTS (SELECT PACTSCHOOL_TMP.USERID
                                                 FROM   PACTSCHOOL_TMP
                                                 WHERE  PACTSCHOOL.USERID=PACTSCHOOL_TMP.USERID)
                                                 AND    (CERT_USERDIV!='6'))
                             AND    (CERT_USERDIV!='6'));

-- テンポラリテーブル削除
DROP TABLE PACTSCHOOL_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

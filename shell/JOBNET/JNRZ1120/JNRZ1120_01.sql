-- ************************************************
-- *機 能 名 : 契約校マスタ
-- *機 能 ID : JNRZ1120_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE PACTSCHOOL_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE PACTSCHOOL_TMP (
    USERID                  CHAR(8)         NOT NULL,   /*  ユーザID                    */
    SCHOOLCD                CHAR(5)                 ,   /*  学校コード                  */
    PACTDIV                 CHAR(1)                 ,   /*  契約区分                    */
    MAXSESSION              NUMBER(3,0)             ,   /*  最大セッション数            */
    REALTIMESESSION         NUMBER(3,0)             ,   /*  リアルタイムセッション数    */
    CERT_CN                 VARCHAR2(16)            ,   /*  証明書情報CN                */
    CERT_OU                 VARCHAR2(16)            ,   /*  証明書情報OU                */
    CERT_O                  VARCHAR2(32)            ,   /*  証明書情報O                 */
    CERT_L                  VARCHAR2(32)            ,   /*  証明書情報L                 */
    CERT_S                  VARCHAR2(32)            ,   /*  証明書情報S                 */
    CERT_C                  VARCHAR2(2)             ,   /*  証明書情報C                 */
    CERT_BEFOREDATA         VARCHAR2(8)             ,   /*  証明書有効期限              */
    CERT_USERDIV            VARCHAR2(1)             ,   /*  証明書ユーザ区分            */
    CERT_FLG                CHAR(1)                 ,   /*  証明書有効フラグ            */
    ISSUED_BEFOREDATE       VARCHAR2(8)             ,   /*  発行済証明書有効期限        */
    PACTTERM                CHAR(8)                 ,   /*  契約期限                    */
    REGISTRYDATE            CHAR(8)                 ,   /*  登録日                      */
    RENEWALDATE             CHAR(8)                 ,   /*  更新日                      */
    LOGINPWD                VARCHAR2(8)             ,   /*  ログインパスワード          */
    DEFAULTPWD              VARCHAR2(8)             ,   /*  初期パスワード              */
    FEATPROVFLG             CHAR(1)                 ,   /*  私書箱機能                  */
    CONSTRAINT PK_PACTSCHOOL_TMP PRIMARY KEY(USERID)
    USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

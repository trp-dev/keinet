------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 契約校マスタ
--  テーブル名      : PACTSCHOOL
--  データファイル名: PACTSCHOOLyyyymmdd.csv
--  機能            : 契約校マスタをデータファイルの内容に置き換える
--  作成日          : 2004/08/25
--  修正日          : 2004/09/10
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE PACTSCHOOL_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    USERID,
    SCHOOLCD,
    PACTDIV,
    MAXSESSION,
    REALTIMESESSION,
    CERT_CN,
    CERT_OU,
    CERT_O,
    CERT_L,
    CERT_S,
    CERT_C,
    CERT_BEFOREDATA,
    CERT_USERDIV,
    CERT_FLG,
    ISSUED_BEFOREDATE,
    PACTTERM,
    REGISTRYDATE,
    RENEWALDATE,
    LOGINPWD,
    DEFAULTPWD,
    FEATPROVFLG
)

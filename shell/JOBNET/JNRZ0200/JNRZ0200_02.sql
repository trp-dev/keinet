-- *********************************************************
-- *機 能 名 : 志望大学評価別人数(高校)
-- *機 能 ID : JNRZ0200_02.sql
-- *作 成 日 : 2004/08/30
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM RATINGNUMBER_S
WHERE  (EXAMYEAR,EXAMCD,BUNDLECD)
       IN(SELECT DISTINCT EXAMYEAR,EXAMCD,BUNDLECD
          FROM   RATINGNUMBER_S_TMP);

-- テンポラリテーブルから更新対象データを挿入
-- （一括コード'98998','84***','**999'は対象外）
INSERT INTO RATINGNUMBER_S
SELECT * FROM RATINGNUMBER_S_TMP
WHERE  ((BUNDLECD!='98998')
AND     (BUNDLECD NOT LIKE '84___')
AND     (BUNDLECD NOT LIKE '__999'));

-- 8年以上前のデータを削除
-- DELETE FROM RATINGNUMBER_S
-- WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 8);

-- 更新対象データの7年前のデータを削除
-- DELETE FROM RATINGNUMBER_S
-- WHERE  (EXAMYEAR,EXAMCD)
--        IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 7)),EXAMCD
--          FROM   RATINGNUMBER_S_TMP);

-- テンポラリテーブル削除
DROP TABLE RATINGNUMBER_S_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

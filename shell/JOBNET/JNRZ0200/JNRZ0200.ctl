------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 志望大学評価別人数（高校）
--  テーブル名      : RATINGNUMBER_S
--  データファイル名: RATINGNUMBER_S
--  機能            : 志望大学評価別人数（高校）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE RATINGNUMBER_S_TMP
(  
   EXAMYEAR             POSITION(01:04)  CHAR,
   EXAMCD               POSITION(05:06)  CHAR,
   BUNDLECD             POSITION(07:11)  CHAR,
   RATINGDIV            POSITION(12:12)  CHAR,
   UNIVCOUNTINGDIV      POSITION(13:13)  CHAR,
   UNIVCD               POSITION(14:17)  CHAR,
   FACULTYCD            POSITION(18:19)  CHAR,
   DEPTCD               POSITION(20:21)  CHAR,
   AGENDACD             POSITION(22:22)  CHAR "NVL(:AGENDACD,' ')",
   STUDENTDIV           POSITION(23:23)  CHAR,
   AVGDEVIATION         POSITION(24:27)  INTEGER EXTERNAL "DECODE(:AVGDEVIATION,          '9999', null, :AVGDEVIATION/10)",
   AVGSCORERATE         POSITION(28:31)  INTEGER EXTERNAL "DECODE(:AVGSCORERATE,          '9999', null, :AVGSCORERATE/10)",
   TOTALCANDIDATENUM    POSITION(32:39)  INTEGER EXTERNAL "NULLIF(:TOTALCANDIDATENUM, '99999999')",
   FIRSTCANDIDATENUM    POSITION(40:47)  INTEGER EXTERNAL "NULLIF(:FIRSTCANDIDATENUM, '99999999')",
   RATINGNUM_A          POSITION(48:55)  INTEGER EXTERNAL "NULLIF(:RATINGNUM_A,       '99999999')",
   RATINGNUM_B          POSITION(56:63)  INTEGER EXTERNAL "NULLIF(:RATINGNUM_B,       '99999999')",
   RATINGNUM_C          POSITION(64:71)  INTEGER EXTERNAL "NULLIF(:RATINGNUM_C,       '99999999')",
   RATINGNUM_D          POSITION(72:79)  INTEGER EXTERNAL "NULLIF(:RATINGNUM_D,       '99999999')",
   RATINGNUM_E          POSITION(80:87)  INTEGER EXTERNAL "NULLIF(:RATINGNUM_E,       '99999999')"
)


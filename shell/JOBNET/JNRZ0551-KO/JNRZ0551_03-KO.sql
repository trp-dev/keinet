-- *********************************************************
-- *機 能 名 : 大学紐付けデータ作成
-- *機 能 ID : JNRZ0551_03.sql
-- *作 成 日 : 2004/10/04
-- *更 新 日 : 2009/12/15
-- *更新内容 :
-- *********************************************************

-- PL/SQLブロックからの表示をONにする
SET ECHO ON
SET LINESIZE 200
SET SERVEROUTPUT ON

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 移行データの名寄せ処理
DECLARE
    c_examyear1         VARCHAR2(4);            -- １つ前の模試年度
    c_examdiv1          VARCHAR2(2);            -- １つ前の模試区分
    c_examyear2         VARCHAR2(4);            -- ２つ前の模試年度
    c_examdiv2          VARCHAR2(2);            -- ２つ前の模試区分
    uh_examyear         VARCHAR2(4);            -- １つ前の模試年度検索用
    uh_examdiv          VARCHAR2(2);            -- １つ前の模試区分検索用
    uh_olduniv8cd       VARCHAR2(10);           -- １つ前の変更前大学コード検索用

-- メイン処理
BEGIN
    -- 高3大学マスタ（志望用名称）更新対象データ読み込み
    -- １模試分のみが前提
    DBMS_OUTPUT.PUT_LINE('大学コード紐付け処理開始');
    FOR r_newcur IN (SELECT   DISTINCT EVENTYEAR,     EXAMDIV
                     FROM     KO_UNIVMASTER_BASIC_TMP
                     ORDER BY EVENTYEAR,EXAMDIV)
    LOOP

        -- １つ前の年度と模試区分を取得
        c_examyear1 := LTRIM(TO_CHAR(TO_NUMBER(r_newcur.EVENTYEAR), '0999'));
        c_examdiv1  := LTRIM(TO_CHAR(TO_NUMBER(r_newcur.EXAMDIV), '09'));
        IF c_examdiv1='00' THEN
            c_examyear1 := LTRIM(TO_CHAR(TO_NUMBER(c_examyear1) - 1, '0999'));
            c_examdiv1  := '09';
        ELSE
            c_examdiv1  := LTRIM(TO_CHAR(TO_NUMBER(c_examdiv1) - 1, '09'));
        END IF;
delete from KO_UNIVCDHOOKUP where (YEAR = c_examyear1) and (OLDEXAMDIV = c_examdiv1);

        -- ２つ前の年度と模試区分を取得
        c_examyear2 := c_examyear1;
        c_examdiv2  := c_examdiv1;
        IF c_examdiv2='00' THEN
            c_examyear2 := LTRIM(TO_CHAR(TO_NUMBER(c_examyear2) - 1, '0999'));
            c_examdiv2  := '09';
        ELSE
            c_examdiv2  := LTRIM(TO_CHAR(TO_NUMBER(c_examdiv2) - 1, '09'));
        END IF;

        -- ログ出力
        DBMS_OUTPUT.PUT_LINE('対象模試(１回前)' ||
                                   ' [年度='                  || TO_CHAR(c_examyear1)     ||
                                   '][模試区分='              || TO_CHAR(c_examdiv1)      ||
                                   '] 対象模試(２回前)' ||
                                   ' [年度='                  || TO_CHAR(c_examyear2)     ||
                                   '][模試区分='              || TO_CHAR(c_examdiv2)      || ']');

        -- ２つ前の年度と模試区分で紐付けマスタを検索
        FOR r_cur IN (SELECT NEWUNIV8CD
                      FROM   KO_UNIVCDHOOKUP
                      WHERE  (YEAR       = c_examyear2)
                      AND    (OLDEXAMDIV = c_examdiv2))
        LOOP

            -- ２つ前の紐付けマスタが存在する場合
            BEGIN
                -- 該当データがある場合は再度１つ前の紐付けマスタを検索
                DBMS_OUTPUT.PUT_LINE('２回前の模試あり [大学コード=' || TO_CHAR(r_cur.NEWUNIV8CD) || ']');
                SELECT DISTINCT YEAR,        OLDEXAMDIV, OLDUNIV8CD
                INTO            uh_examyear, uh_examdiv, uh_olduniv8cd
                FROM   KO_UNIVCDHOOKUP
                WHERE  (YEAR       = c_examyear1)
                AND    (OLDEXAMDIV = c_examdiv1)
                AND    (OLDUNIV8CD = r_cur.NEWUNIV8CD);

                -- １つ前の紐付けマスタが存在する場合は何もしない
                DBMS_OUTPUT.PUT_LINE('１回前の模試あり(処理なし)');
                NULL;

            EXCEPTION
                -- なければデータ作成
                WHEN NO_DATA_FOUND THEN
                    DBMS_OUTPUT.PUT_LINE('１回前の模試なし');
                    DBMS_OUTPUT.PUT_LINE('１つ前の紐付けマスタ作成' ||
                                      ' [年度='                  || TO_CHAR(c_examyear1)      ||
                                      '][模試区分='              || TO_CHAR(c_examdiv1)       ||
                                      '][変更前/最新大学コード=' || TO_CHAR(r_cur.NEWUNIV8CD) || ']');
                    INSERT INTO KO_UNIVCDHOOKUP
                    VALUES (
                        c_examyear1,
                        c_examdiv1,
                        r_cur.NEWUNIV8CD,
                        r_cur.NEWUNIV8CD
                    );

                -- エラーEXCEPTIONを親に投げる
                WHEN OTHERS THEN
                    DBMS_OUTPUT.PUT_LINE('!!! ２つ前の紐付けマスタ検索処理エラー発生 !!!');
                    RAISE;
            END;

        -- ２つ前の紐付けマスタループ
        END LOOP;

    -- 最新高３大学マスタループ
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('大学コード紐付け処理が正常に終了しました');

-- メイン処理：例外処理
EXCEPTION
    -- エラーEXCEPTIONを親に投げる
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('!!! 大学コード紐付け処理にてエラーが発生しました !!!');
        RAISE;
END;
/

-- テンポラリテーブル削除
DROP TABLE KO_UNIVMASTER_BASIC_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

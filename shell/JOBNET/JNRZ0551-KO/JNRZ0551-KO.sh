#!/bin/sh

#環境変数設定
export JOBNAME=JNRZ0551-KO
export CSVFILE=KO_UNIVMASTER_BASIC
export SRCDIR=/keinavi/HULFT/WorkDir
export WORKDIR=/keinavi/JOBNET/$JOBNAME

export flist=`ls -r $SRCDIR/KO_UNIVMASTER_BASIC*`

mv ${SRCDIR}/$CSVFILE ${SRCDIR}/${CSVFILE}-tmp

for test in $flist; do
	if [ $test = ${SRCDIR}/$CSVFILE ]
	then
		mv ${SRCDIR}/${CSVFILE}-tmp ${SRCDIR}/$CSVFILE
	else
		mv $test ${SRCDIR}/$CSVFILE
	fi

	/keinavi/JOBNET/JNRZ0551-KO/JNRZ0551-KO1.sh
done


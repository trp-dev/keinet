set feedback off
set heading off
set pagesize 0
set verify off
SET SERVEROUTPUT ON
VARIABLE vEXIT NUMBER;
DECLARE
BEGIN
	:vEXIT := 0;


	-- 既存データの削除
	EXECUTE IMMEDIATE 'TRUNCATE TABLE HANTEI&2..UNIVMASTER_BASIC';
	EXECUTE IMMEDIATE 'TRUNCATE TABLE HANTEI&2..UNIVMASTER_CHOICESCHOOL';
	EXECUTE IMMEDIATE 'TRUNCATE TABLE HANTEI&2..UNIVMASTER_COURSE';
	EXECUTE IMMEDIATE 'TRUNCATE TABLE HANTEI&2..UNIVMASTER_SUBJECT';
	EXECUTE IMMEDIATE 'TRUNCATE TABLE HANTEI&2..UNIVMASTER_SELECTGROUP';
	EXECUTE IMMEDIATE 'TRUNCATE TABLE HANTEI&2..UNIVMASTER_SELECTGPCOURSE';
	EXECUTE IMMEDIATE 'TRUNCATE TABLE HANTEI&2..EXAMSCHEDULEDETAIL';
	EXECUTE IMMEDIATE 'TRUNCATE TABLE HANTEI&2..SUBRECORD_A';
	EXECUTE IMMEDIATE 'TRUNCATE TABLE HANTEI&2..EXAMSUBJECT';
	EXECUTE IMMEDIATE 'TRUNCATE TABLE HANTEI&2..EXAMINATION';
	EXECUTE IMMEDIATE 'TRUNCATE TABLE HANTEI&2..CENTERCVSSCORE';

	-- データ登録
	insert into HANTEI&2..UNIVMASTER_BASIC          select * from KEINAVI.UNIVMASTER_BASIC          where eventyear='&1' and examdiv='&2';
	dbms_output.put_line('UNIVMASTER_BASIC:          ' || LPAD(SQL%ROWCOUNT, 6) || '件登録しました。');
	insert into HANTEI&2..UNIVMASTER_CHOICESCHOOL   select * from KEINAVI.UNIVMASTER_CHOICESCHOOL   where eventyear='&1' and examdiv='&2';
	dbms_output.put_line('UNIVMASTER_CHOICESCHOOL:   ' || LPAD(SQL%ROWCOUNT, 6) || '件登録しました。');
	insert into HANTEI&2..UNIVMASTER_COURSE         select * from KEINAVI.UNIVMASTER_COURSE         where eventyear='&1' and examdiv='&2';
	dbms_output.put_line('UNIVMASTER_COURSE:         ' || LPAD(SQL%ROWCOUNT, 6) || '件登録しました。');
	insert into HANTEI&2..UNIVMASTER_SUBJECT        select * from KEINAVI.UNIVMASTER_SUBJECT        where eventyear='&1' and examdiv='&2';
	dbms_output.put_line('UNIVMASTER_SUBJECT:        ' || LPAD(SQL%ROWCOUNT, 6) || '件登録しました。');
	insert into HANTEI&2..UNIVMASTER_SELECTGROUP    select * from KEINAVI.UNIVMASTER_SELECTGROUP    where eventyear='&1' and examdiv='&2';
	dbms_output.put_line('UNIVMASTER_SELECTGROUP:    ' || LPAD(SQL%ROWCOUNT, 6) || '件登録しました。');
	insert into HANTEI&2..UNIVMASTER_SELECTGPCOURSE select * from KEINAVI.UNIVMASTER_SELECTGPCOURSE where eventyear='&1' and examdiv='&2';
	dbms_output.put_line('UNIVMASTER_SELECTGPCOURSE: ' || LPAD(SQL%ROWCOUNT, 6) || '件登録しました。');
	insert into HANTEI&2..EXAMSCHEDULEDETAIL        select * from KEINAVI.EXAMSCHEDULEDETAIL        where year='&1' and examdiv='&2';
	dbms_output.put_line('EXAMSCHEDULEDETAIL:        ' || LPAD(SQL%ROWCOUNT, 6) || '件登録しました。');
	insert into HANTEI&2..SUBRECORD_A               select * from KEINAVI.SUBRECORD_A               where examyear='&1';
	dbms_output.put_line('SUBRECORD_A:               ' || LPAD(SQL%ROWCOUNT, 6) || '件登録しました。');
	insert into HANTEI&2..EXAMSUBJECT               select * from KEINAVI.EXAMSUBJECT               where examyear='&1';
	dbms_output.put_line('EXAMSUBJECT:               ' || LPAD(SQL%ROWCOUNT, 6) || '件登録しました。');
	insert into HANTEI&2..EXAMINATION               select * from KEINAVI.EXAMINATION               where examyear='&1';
	dbms_output.put_line('EXAMINATION:               ' || LPAD(SQL%ROWCOUNT, 6) || '件登録しました。');
	insert into HANTEI&2..CENTERCVSSCORE            select * from KEINAVI.CENTERCVSSCORE            where examyear=SUBSTR('&1', 3, 2);
	dbms_output.put_line('CENTERCVSSCORE:            ' || LPAD(SQL%ROWCOUNT, 6) || '件登録しました。');

    COMMIT;
EXCEPTION
	WHEN OTHERS THEN
		dbms_output.put_line(SQLERRM);
		ROLLBACK;
		:vEXIT := 99;
END;
/ 
EXIT :vEXIT;

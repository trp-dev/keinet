#!/bin/bash
cur=$(cd $(dirname $0); pwd)


#start
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "判定コンペア start."
echo ""

cd /keinavi/JOBNET/hanteiCheck

export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.3.7-0.el7_6.x86_64
examcds='01 02 03 04 05 06 07'

#判定コンペア実行
for examcd in $examcds; do
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "[模試コード:${examcd}]：判定コンペア実行 start."
    $JAVA_HOME/bin/java -Xmx1536m -Xms1024m -Dlogging.config=config/log4j2-${examcd}.properties -jar comparator-1.0.0.jar --spring.profiles.active=${examcd}
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "[模試コード:${examcd}]：判定コンペア実行 end."
    echo ""
done


#end
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "判定コンペア end."

#!/bin/bash
cur=$(cd $(dirname $0); pwd)


#パラメータチェック
#パラメータチェック
if [ "$1" = "" ]; then
    echo "パラメータ１：年度が未設定です。"
    exit 1
fi
expr "$1" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ１：年度が不正です。【$1】"
    exit 1
fi
year=$1
today=`date "+%Y%m%d"`
examdivs='01 02 03 04 05 06'
#start
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "[KEINAVI] -> [HANTEI]：大学マスタ作成 start."
echo ""


#for debug
echo "********** for debug **********"
echo "対象年度：     "$year
echo "処理日：       "$today
echo "********** for debug **********"
echo ""


#大学マスタの作成
for div in $examdivs; do
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "[HANTEI${div}]：大学マスタ作成 start."
    sqlplus -s oracle/oracle@S11DBA_BATCH2 @$cur/createUnivmaster.sql $year ${div}
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "[HANTEI${div}]：大学マスタ作成 end."
    echo ""
done


#end
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "[KEINAVI] -> [HANTEI]：大学マスタ作成 end."
exit $ret

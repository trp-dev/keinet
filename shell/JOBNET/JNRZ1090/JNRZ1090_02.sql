-- ************************************************
-- *機 能 名 : ヘルプマスタ
-- *機 能 ID : JNRZ1090_02.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM HELP;

-- テンポラリテーブルからデータを挿入
-- LONG型データを含むため通常のINSERTではなくSQL*PLUSのCOPYを使用する
-- INSERT INTO HELP
-- SELECT * FROM HELP_TMP;
COMMIT;
set long 10000000
COPY FROM keinavi/39ra8ma@S11DBA_BATCH2 TO keinavi/39ra8ma@S11DBA_BATCH2 REPLACE HELP USING SELECT * FROM HELP_TMP;

-- テンポラリテーブル削除
DROP TABLE HELP_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

-- ************************************************
-- *機 能 名 : ヘルプマスタ
-- *機 能 ID : JNRZ1090_01.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE HELP_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE HELP_TMP(   
       HELPID               CHAR(6)             NOT NULL,   /*    ヘルプID           */
       TITLEMEI             VARCHAR2(60)                ,   /*    タイトル文字列     */
       CATEGORY             VARCHAR2(30)                ,   /*    カテゴリ           */
       DISPSEQUENCE         NUMBER(4)                   ,   /*    表示順             */
       RENEWALDATE          CHAR(8)                     ,   /*    更新日             */
       TEXT                 LONG                        ,   /*    説明文             */
    CONSTRAINT PK_HELP_TMP PRIMARY KEY(
       HELPID                                               /*    ヘルプID           */
    )
    USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;
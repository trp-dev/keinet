------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 大学マスタ模試用選択グループ教科
--  テーブル名      : UNIVMASTER_SELECTGPCOURSE
--  データファイル名: UNIVMASTER_SELECTGPCOURSE
--  機能            : 大学マスタ模試用選択グループ教科をデータファイルの内容に置き換える
--  作成日          : 2009/12/17
--  修正日          : 
--  備考            : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE UNIVMASTER_SELECTGPCOURSE_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    EVENTYEAR                 POSITION(01:04)        CHAR,
    SCHOOLDIV                 POSITION(05:05)        CHAR,
    UNIVCD                    POSITION(06:09)        CHAR,
    FACULTYCD                 POSITION(10:11)        CHAR,
    DEPTCD                    POSITION(12:15)        CHAR,
    EXAMDIV                   POSITION(16:17)        CHAR,
    ADMISSIONDIV              POSITION(18:18)        CHAR,
    NEXTYEARDIV               POSITION(19:19)        CHAR,
    ENTEXAMDIV                POSITION(20:20)        CHAR,
    UNICDBRANCHCD             POSITION(21:23)        CHAR,
    SELECTG_NO                POSITION(24:24)        CHAR,
    COURSECD                  POSITION(25:25)        CHAR,
    SGPERSUBSUBCOUNT          POSITION(26:26)        CHAR
)

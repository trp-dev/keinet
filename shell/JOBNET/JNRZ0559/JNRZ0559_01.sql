-- ************************************************
-- *機 能 名 : 大学マスタ模試用選択グループ教科
-- *機 能 ID : JNRZ0559_01.sql
-- *作 成 日 : 2009/12/17
-- *作 成 者 : 
-- *更 新 日 : 
-- *更新内容 : 
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE UNIVMASTER_SELECTGPCOURSE_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE UNIVMASTER_SELECTGPCOURSE_TMP  (
       EVENTYEAR                    CHAR(4)                NOT NULL,   /*   行事年度                    */
       SCHOOLDIV                    CHAR(1)                NOT NULL,   /*   学校区分                    */
       UNIVCD                       CHAR(4)                NOT NULL,   /*   大学コード                  */
       FACULTYCD                    CHAR(2)                NOT NULL,   /*   学部コード                  */
       DEPTCD                       CHAR(4)                NOT NULL,   /*   学科コード                  */
       EXAMDIV                      CHAR(2)                NOT NULL,   /*   模試区分                    */
       ADMISSIONDIV                 CHAR(1)                NOT NULL,   /*   推薦用区分                  */
       NEXTYEARDIV                  CHAR(1)                NOT NULL,   /*   来年用区分                  */
       ENTEXAMDIV                   CHAR(1)                NOT NULL,   /*   入試試験区分                */
       UNICDBRANCHCD                CHAR(3)                NOT NULL,   /*   大学コード枝番              */
       SELECTG_NO                   CHAR(1)                NOT NULL,   /*   選択グループ番号            */
       COURSECD                     CHAR(1)                NOT NULL,   /*   教科コード                  */
       SGPERSUBSUBCOUNT             CHAR(1)                NOT NULL,   /*   選択グループ教科別選択科目数*/
       CONSTRAINT PK_UNIVMASTER_SELECTGPCOURSE_T PRIMARY KEY (
                 EVENTYEAR             ,               /*  行事年度          */
                 SCHOOLDIV             ,               /*  学校区分          */
                 UNIVCD                ,               /*  大学コード        */
                 FACULTYCD             ,               /*  学部コード        */
                 DEPTCD                ,               /*  学科コード        */
                 EXAMDIV               ,               /*  模試区分          */
                 ADMISSIONDIV          ,               /*  推薦用区分        */
                 NEXTYEARDIV           ,               /*  来年用区分        */
                 ENTEXAMDIV            ,               /*  入試試験区分      */
                 UNICDBRANCHCD         ,               /*  大学コード枝番    */
                 SELECTG_NO            ,               /*  選択グループ番号  */
                 COURSECD                              /*  教科コード        */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

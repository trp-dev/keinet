------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 科目分布別成績（高校）
--  テーブル名      : SUBDISTRECORD_C
--  データファイル名: SUBDISTRECORD_C
--  機能            : 科目分布別成績（高校）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
INSERT PRESERVE BLANKS
INTO TABLE SUBDISTRECORD_C_TMP
(  
   EXAMYEAR    POSITION(01:04)  CHAR,
   EXAMCD      POSITION(05:06)  CHAR,
   BUNDLECD    POSITION(07:11)  CHAR,
   GRADE       POSITION(12:13)  INTEGER EXTERNAL,
   CLASS       POSITION(14:15)  CHAR,
   SUBCD       POSITION(16:19)  CHAR,
   DEVZONECD   POSITION(20:21)  CHAR,
   NUMBERS     POSITION(22:29)  INTEGER EXTERNAL,
   COMPRATIO   POSITION(30:33)  INTEGER EXTERNAL
)


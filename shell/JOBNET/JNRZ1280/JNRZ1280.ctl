------------------------------------------------------------------------------
-- *機 能 名 : 数学記述式設問（全国）のCTLファイル
-- *機 能 ID : JNRZ1280.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE MATHDESCQUE_A_TMP
(  
   EXAMYEAR    POSITION(01:04)  CHAR 
 , EXAMCD      POSITION(05:06)  CHAR 
 , SUBCD       POSITION(07:10)  CHAR 
 , QUESTION_NO POSITION(11:12)  CHAR 
 , AVGPNT      POSITION(13:16)  INTEGER EXTERNAL "DECODE(:AVGPNT,      '9999', null, :AVGPNT/10)"
)

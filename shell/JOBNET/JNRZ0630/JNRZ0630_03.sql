-- ************************************************
-- *機 能 名 : Kei-Netサービス移行データ
-- *機 能 ID : JNRZ0630_02.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;

-- 3年以上前のデータを削除
-- INDIVIDUALRECORD（個票データ）
DELETE FROM INDIVIDUALRECORD
WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 3);

-- HISTORYINFO（学籍履歴情報）
DELETE FROM HISTORYINFO
WHERE  TO_NUMBER(YEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 3);

-- SUBRECORD_I（科目別成績(個人)）
DELETE FROM SUBRECORD_I
WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 3);

-- QRECORD_I（設問別成績(個人)）
DELETE FROM QRECORD_I
WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 3);

-- CANDIDATERATING（志望校評価(個人)）
DELETE FROM CANDIDATERATING
WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 3);

-- SUBRECORD_PP（科目別成績(P保護対象)）
DELETE FROM SUBRECORD_PP
WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 3);

-- QRECORD_PP（設問別成績(P保護対象)）
DELETE FROM QRECORD_PP
WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 3);

-- CANDIDATERATING_PP（志望校評価(P保護対象)）
DELETE FROM CANDIDATERATING_PP
WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 3);


-- BASICINFO(HISTORYINFO削除処理後、該当個人IDの学籍履歴情報が存在していなければ削除)
DELETE FROM BASICINFO
WHERE BASICINFO.INDIVIDUALID IN (SELECT BASICINFO.INDIVIDUALID
                        FROM BASICINFO
                        WHERE (NOT EXISTS (SELECT HISTORYINFO.INDIVIDUALID
                                            FROM HISTORYINFO
                                            WHERE BASICINFO.INDIVIDUALID=HISTORYINFO.INDIVIDUALID)
                                )
                        );


-- AUTODETERMINATION(HISTORYINFO削除処理後、該当個人IDの学籍履歴情報が存在していなければ削除)
DELETE FROM AUTODETERMINATION
WHERE AUTODETERMINATION.INDIVIDUALID IN (SELECT AUTODETERMINATION.INDIVIDUALID
                        FROM AUTODETERMINATION
                        WHERE (NOT EXISTS (SELECT HISTORYINFO.INDIVIDUALID
                                            FROM HISTORYINFO
                                            WHERE AUTODETERMINATION.INDIVIDUALID=HISTORYINFO.INDIVIDUALID)
                                )
                        );

-- DETERMHISTORY(HISTORYINFO削除処理後、該当個人IDの学籍履歴情報が存在していなければ削除)
DELETE FROM DETERMHISTORY
WHERE DETERMHISTORY.INDIVIDUALID IN (SELECT DETERMHISTORY.INDIVIDUALID
                        FROM DETERMHISTORY
                        WHERE (NOT EXISTS (SELECT HISTORYINFO.INDIVIDUALID
                                            FROM HISTORYINFO
                                            WHERE DETERMHISTORY.INDIVIDUALID=HISTORYINFO.INDIVIDUALID)
                                )
                        );

-- HANGUPPROVISO(HISTORYINFO削除処理後、該当個人IDの学籍履歴情報が存在していなければ削除)
DELETE FROM HANGUPPROVISO
WHERE HANGUPPROVISO.INDIVIDUALID IN (SELECT HANGUPPROVISO.INDIVIDUALID
                        FROM HANGUPPROVISO
                        WHERE (NOT EXISTS (SELECT HISTORYINFO.INDIVIDUALID
                                            FROM HISTORYINFO
                                            WHERE HANGUPPROVISO.INDIVIDUALID=HISTORYINFO.INDIVIDUALID)
                                )
                        );

-- INTERVIEW(HISTORYINFO削除処理後、該当個人IDの学籍履歴情報が存在していなければ削除)
DELETE FROM INTERVIEW
WHERE INTERVIEW.INDIVIDUALID IN (SELECT INTERVIEW.INDIVIDUALID
                        FROM INTERVIEW
                        WHERE (NOT EXISTS (SELECT HISTORYINFO.INDIVIDUALID
                                            FROM HISTORYINFO
                                            WHERE INTERVIEW.INDIVIDUALID=HISTORYINFO.INDIVIDUALID)
                                )
                        );

-- EXAMPLANUNIV(HISTORYINFO削除処理後、該当個人IDの学籍履歴情報が存在していなければ削除)
DELETE FROM EXAMPLANUNIV
WHERE EXAMPLANUNIV.INDIVIDUALID IN (SELECT EXAMPLANUNIV.INDIVIDUALID
                        FROM EXAMPLANUNIV
                        WHERE (NOT EXISTS (SELECT HISTORYINFO.INDIVIDUALID
                                            FROM HISTORYINFO
                                            WHERE EXAMPLANUNIV.INDIVIDUALID=HISTORYINFO.INDIVIDUALID)
                                )
                        );

-- SQL*PLUS終了
EXIT;

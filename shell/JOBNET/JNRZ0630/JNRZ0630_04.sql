-- ************************************************
-- *機 能 名 : Kei-Netサービス移行データ
-- *機 能 ID : JNRZ0630_01.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE INDIVIDUALRECORD_TMP2 CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

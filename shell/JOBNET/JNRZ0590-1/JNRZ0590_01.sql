-- ************************************************
-- *機 能 名 : 高3公表用判定大学マスタ
-- *機 能 ID : JNRZ0590_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE HS3_UNIV_PUB_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE HS3_UNIV_PUB_TMP  (
       YEAR                         CHAR(4)                NOT NULL,   /*   年度	                    */
       UNIVCD                       CHAR(4)                NOT NULL,   /*   大学コード	                */
       FACULTYCD                    CHAR(2)                NOT NULL,   /*   学部コード	                */
       DEPTCD                       CHAR(2)                NOT NULL,   /*   学科コード	                */
       EXAMDIV                      CHAR(2)                NOT NULL,   /*   模試区分	                */
       ENTEXAMDIV                   CHAR(1)                NOT NULL,   /*   入試試験区分	            */
       BRANCHCD                     CHAR(2)                NOT NULL,   /*   枝番	                    */
       ALLOTPNTRATE                 NUMBER(5,0)                    ,   /*   配点比	                    */
       FULLPNT                      NUMBER(5,0)                    ,   /*   満点値	                    */
       SUBSTRING                    VARCHAR2(120)                  ,   /*   科目文字列	                */
       NECESSUB_EN                  CHAR(1)                        ,   /*   判定必須科目-英語	        */
       CHOICESUB_EN                 CHAR(1)                        ,   /*   判定選択科目-英語	        */
       COURSEPNT_EN                 NUMBER(5,1)                    ,   /*   判定教科配点-英語	        */
       NECESSUB_MA                  CHAR(2)                        ,   /*   判定必須科目-数学	        */
       CHOICESUB_MA                 CHAR(2)                        ,   /*   判定選択科目-数学	        */
       COURSEPNT_MA                 NUMBER(5,1)                    ,   /*   判定教科配点-数学	        */
       NECESSUB_JA                  CHAR(1)                        ,   /*   判定必須科目-国語	        */
       CHOICESUB_JA                 CHAR(1)                        ,   /*   判定選択科目-国語	        */
       COURSEPNT_JA                 NUMBER(5,1)                    ,   /*   判定教科配点-国語	        */
       NECESSUB_SC                  CHAR(2)                        ,   /*   判定必須科目-理科	        */
       CHOICESUB_SC                 CHAR(2)                        ,   /*   判定選択科目-理科	        */
       COURSEPNT_SC                 NUMBER(5,1)                    ,   /*   判定教科配点-理科	        */
       NECESSUB_GH                  CHAR(1)                        ,   /*   判定必須科目-地歴	        */
       CHOICESUB_GH                 CHAR(1)                        ,   /*   判定選択科目-地歴	        */
       COURSEPNT_GH                 NUMBER(5,1)                    ,   /*   判定教科配点-地歴	        */
       NECESSUB_CI                  CHAR(1)                        ,   /*   判定必須科目-公民	        */
       CHOICESUB_CI                 CHAR(1)                        ,   /*   判定選択科目-公民	        */
       COURSEPNT_CI                 NUMBER(5,1)                    ,   /*   判定教科配点-公民	        */
       COURSEPNT_ET                 NUMBER(5,1)                    ,   /*   判定教科配点-その他	        */
       SUBPNT_ET                    CHAR(18)                       ,   /*   判定科目配点-その他	        */
       CHOICESUBCOUNT1              CHAR(2)                        ,   /*   判定選択科目数１	        */
       CHOICESUBCOUNT2              CHAR(2)                        ,   /*   判定選択科目数２	        */
       CHOICESUBCOUNT3              CHAR(2)                        ,   /*   判定選択科目数３	        */
       CHOICESUBCOUNT4              CHAR(2)                        ,   /*   判定選択科目数４	        */
       CHOICESUBCOUNT5              CHAR(2)                        ,   /*   判定選択科目数５	        */
       CHOICESUBCOUNT6              CHAR(2)                        ,   /*   判定選択科目数６	        */
       CHOICEALLOTPNT1              NUMBER(5,1)                    ,   /*   判定選択配点１	            */
       CHOICEALLOTPNT2              NUMBER(5,1)                    ,   /*   判定選択配点２	            */
       CHOICEALLOTPNT3              NUMBER(5,1)                    ,   /*   判定選択配点３	            */
       CHOICEALLOTPNT4              NUMBER(5,1)                    ,   /*   判定選択配点４	            */
       CHOICEALLOTPNT5              NUMBER(5,1)                    ,   /*   判定選択配点５	            */
       CHOICEALLOTPNT6              NUMBER(5,1)                    ,   /*   判定選択配点６	            */
       SUBBIT_EN1                   VARCHAR2(2)                    ,   /*   科目ビット-英語1	        */
       SUBBIT_EN2                   VARCHAR2(2)                    ,   /*   科目ビット-英語2	        */
       SUBBIT_EN3                   VARCHAR2(2)                    ,   /*   科目ビット-英語3	        */
       SUBBIT_EN4                   VARCHAR2(2)                    ,   /*   科目ビット-英語4	        */
       SUBBIT_MA1                   VARCHAR2(2)                    ,   /*   科目ビット-数学1	        */
       SUBBIT_MA2                   VARCHAR2(2)                    ,   /*   科目ビット-数学2	        */
       SUBBIT_MA3                   VARCHAR2(2)                    ,   /*   科目ビット-数学3	        */
       SUBBIT_MA4                   VARCHAR2(2)                    ,   /*   科目ビット-数学4	        */
       SUBBIT_MA5                   VARCHAR2(2)                    ,   /*   科目ビット-数学5	        */
       SUBBIT_MA6                   VARCHAR2(2)                    ,   /*   科目ビット-数学6	        */
       SUBBIT_MA7                   VARCHAR2(2)                    ,   /*   科目ビット-数学7	        */
       SUBBIT_MA8                   VARCHAR2(2)                    ,   /*   科目ビット-数学8	        */
       SUBBIT_JA1                   VARCHAR2(2)                    ,   /*   科目ビット-国語1	        */
       SUBBIT_JA2                   VARCHAR2(2)                    ,   /*   科目ビット-国語2	        */
       SUBBIT_JA3                   VARCHAR2(2)                    ,   /*   科目ビット-国語3	        */
       SUBBIT_JA4                   VARCHAR2(2)                    ,   /*   科目ビット-国語4	        */
       SUBBIT_JA5                   VARCHAR2(2)                    ,   /*   科目ビット-国語5	        */
       SUBBIT_JA6                   VARCHAR2(2)                    ,   /*   科目ビット-国語6	        */
       SUBBIT_JA7                   VARCHAR2(2)                    ,   /*   科目ビット-国語7	        */
       SUBBIT_JA8                   VARCHAR2(2)                    ,   /*   科目ビット-国語8	        */
       SUBBIT_SC1                   VARCHAR2(2)                    ,   /*   科目ビット-理科1	        */
       SUBBIT_SC2                   VARCHAR2(2)                    ,   /*   科目ビット-理科2	        */
       SUBBIT_SC3                   VARCHAR2(2)                    ,   /*   科目ビット-理科3	        */
       SUBBIT_SC4                   VARCHAR2(2)                    ,   /*   科目ビット-理科4	        */
       SUBBIT_SC5                   VARCHAR2(2)                    ,   /*   科目ビット-理科5	        */
       SUBBIT_SC6                   VARCHAR2(2)                    ,   /*   科目ビット-理科6	        */
       SUBBIT_SC7                   VARCHAR2(2)                    ,   /*   科目ビット-理科7	        */
       SUBBIT_SC8                   VARCHAR2(2)                    ,   /*   科目ビット-理科8	        */
       SUBBIT_SC9                   VARCHAR2(2)                    ,   /*   科目ビット-理科9	        */
       SUBBIT_SC10                  VARCHAR2(2)                    ,   /*   科目ビット-理科10	        */
       SUBBIT_SC11                  VARCHAR2(2)                    ,   /*   科目ビット-理科11	        */
       SUBBIT_SC12                  VARCHAR2(2)                    ,   /*   科目ビット-理科12	        */
       SUBBIT_GH1                   VARCHAR2(2)                    ,   /*   科目ビット-地歴1	        */
       SUBBIT_GH2                   VARCHAR2(2)                    ,   /*   科目ビット-地歴2	        */
       SUBBIT_GH3                   VARCHAR2(2)                    ,   /*   科目ビット-地歴3	        */
       SUBBIT_GH4                   VARCHAR2(2)                    ,   /*   科目ビット-地歴4	        */
       SUBBIT_GH5                   VARCHAR2(2)                    ,   /*   科目ビット-地歴5	        */
       SUBBIT_GH6                   VARCHAR2(2)                    ,   /*   科目ビット-地歴6	        */
       SUBBIT_GH7                   VARCHAR2(2)                    ,   /*   科目ビット-地歴7	        */
       SUBBIT_GH8                   VARCHAR2(2)                    ,   /*   科目ビット-地歴8	        */
       SUBBIT_CI1                   VARCHAR2(2)                    ,   /*   科目ビット-公民1	        */
       SUBBIT_CI2                   VARCHAR2(2)                    ,   /*   科目ビット-公民2	        */
       SUBBIT_CI3                   VARCHAR2(2)                    ,   /*   科目ビット-公民3	        */
       SUBBIT_CI4                   VARCHAR2(2)                    ,   /*   科目ビット-公民4	        */
       SUBBIT_ET1                   VARCHAR2(2)                    ,   /*   科目ビット-その他1	        */
       SUBBIT_ET2                   VARCHAR2(2)                    ,   /*   科目ビット-その他2	        */
       SUBBIT_ET3                   VARCHAR2(2)                    ,   /*   科目ビット-その他3	        */
       SUBBIT_ET4                   VARCHAR2(2)                    ,   /*   科目ビット-その他4	        */
       SUBBIT_ET5                   VARCHAR2(2)                    ,   /*   科目ビット-その他5	        */
       SUBBIT_ET6                   VARCHAR2(2)                    ,   /*   科目ビット-その他6	        */
       SUBBIT_ET7                   VARCHAR2(2)                    ,   /*   科目ビット-その他7	        */
       SUBBIT_ET8                   VARCHAR2(2)                    ,   /*   科目ビット-その他8	        */
       BRANCHNAME                   VARCHAR2(20)                   ,   /*   入試枝番名称	        */
       CONSTRAINT PK_HS3_UNIV_PUB_TMP PRIMARY KEY (
                 YEAR                  ,               /*  年度         */
                 UNIVCD                ,               /*  大学コード   */
                 FACULTYCD             ,               /*  学部コード   */
                 DEPTCD                ,               /*  学科コード   */
                 EXAMDIV               ,               /*  模試区分     */
                 ENTEXAMDIV            ,               /*  入試試験区分 */
                 BRANCHCD                              /*  枝番         */
       )
       USING INDEX TABLESPACE     KEINAVI_STG_INDX
)
       TABLESPACE       KEINAVI_STG_DATA;

-- SQL*PLUS終了
EXIT;

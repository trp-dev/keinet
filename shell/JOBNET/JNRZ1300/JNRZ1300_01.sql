-- ************************************************
-- *機 能 名 : 数学記述式設問（高校）
-- *機 能 ID : JNRZ1300_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE MATHDESCQUE_S_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE MATHDESCQUE_S_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM MATHDESCQUE_S WHERE 0=1;

-- 主キーの設定
ALTER TABLE MATHDESCQUE_S_TMP ADD CONSTRAINT PK_MATHDESCQUE_S_TMP PRIMARY KEY 
(
   EXAMYEAR
 , EXAMCD
 , BUNDLECD
 , SUBCD
 , QUESTION_NO
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

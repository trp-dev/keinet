------------------------------------------------------------------------------
-- *機 能 名 : 数学記述式設問（高校）のCTLファイル
-- *機 能 ID : JNRZ1300.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE MATHDESCQUE_S_TMP
(  
   EXAMYEAR    POSITION(01:04)  CHAR 
 , EXAMCD      POSITION(05:06)  CHAR 
 , BUNDLECD    POSITION(07:11)  CHAR 
 , SUBCD       POSITION(12:15)  CHAR 
 , QUESTION_NO POSITION(16:17)  CHAR 
 , AVGPNT      POSITION(18:21)  INTEGER EXTERNAL "DECODE(:AVGPNT,      '9999', null, :AVGPNT/10)"
)

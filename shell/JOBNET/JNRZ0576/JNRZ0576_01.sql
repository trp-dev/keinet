-- ************************************************
-- *機 能 名 : 情報誌科目（科目名）
-- *機 能 ID : JNRZ0576_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE JH01_KAMOKU4_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE JH01_KAMOKU4_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM JH01_KAMOKU4 WHERE 0=1;

-- 主キーの設定
ALTER TABLE JH01_KAMOKU4_TMP ADD CONSTRAINT PK_JH01_KAMOKU4_TMP PRIMARY KEY 
(
   COMBINATION_KEY1
 , CEN_BRANCH_CD
 , SEC_BRANCH_CD
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

-- *********************************************************
-- *機 能 名 : 情報誌科目（科目名）
-- *機 能 ID : JNRZ0576_02.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
TRUNCATE TABLE JH01_KAMOKU4;

-- テンポラリから本物へ登録
INSERT INTO JH01_KAMOKU4 SELECT * FROM JH01_KAMOKU4_TMP;

-- テンポラリテーブル削除
DROP TABLE JH01_KAMOKU4_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

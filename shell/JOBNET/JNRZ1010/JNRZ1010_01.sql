-- ************************************************
-- *機 能 名 : 模試名
-- *機 能 ID : JNRZ1010_01.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE EXAMINATION_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE EXAMINATION_TMP(  
     EXAMYEAR         CHAR(4)          NOT NULL,    /*   模試年度               */ 
     EXAMCD           CHAR(2)          NOT NULL,    /*   模試コード             */ 
     EXAMNAME         VARCHAR2(40)             ,    /*   模試名                 */ 
     EXAMNAME_ABBR    VARCHAR2(20)             ,    /*   模試短縮名             */ 
     EXAMTYPECD       CHAR(2)                  ,    /*   模試種類コード         */ 
     EXAMST           CHAR(1)                  ,    /*   模試回                 */ 
     EXAMDIV          CHAR(2)                  ,    /*   模試区分               */ 
     TERGETGRADE      CHAR(2)                  ,    /*   対象学年               */ 
     INPLEDATE        CHAR(8)                  ,    /*   模試実施基準日         */ 
     IN_DATAOPENDATE  CHAR(12)                 ,    /*   内部データ開放日       */ 
     OUT_DATAOPENDATE CHAR(12)                 ,    /*   外部データ開放日       */ 
     DOCKINGEXAMCD    CHAR(2)                  ,    /*   ﾄﾞｯｷﾝｸﾞ先模試コード    */ 
     DOCKINGTYPE      CHAR(1)                  ,    /*   ﾄﾞｯｷﾝｸﾞｾﾝﾀｰ2次種別     */ 
     DISPSEQUENCE     NUMBER(2,0)              ,    /*   表示順序               */ 
     MASTERDIV        CHAR(1)                  ,    /*   大学マスタ区分         */ 
    CONSTRAINT PK_EXAMINATION_TMP PRIMARY KEY(
            EXAMYEAR, 
            EXAMCD
    )
    USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

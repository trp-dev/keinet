------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 模試マスタ
--  テーブル名      : EXAMINATION_TMP
--  データファイル名: EXAMINATIONyyyymmdd.csv
--  機能            : 模試マスタをデータファイルの内容に置き換える
--  作成日          : 2004/08/25
--  修正日          : 2004/09/10
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE EXAMINATION_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   EXAMYEAR,
   EXAMCD          "DECODE(LENGTH(:EXAMCD), 1, CONCAT('0', :EXAMCD), 2, :EXAMCD)",
   EXAMNAME,
   EXAMNAME_ABBR,
   EXAMTYPECD      "DECODE(LENGTH(:EXAMTYPECD), 1, CONCAT('0', :EXAMTYPECD), 2, :EXAMTYPECD)",
   EXAMST,
   EXAMDIV         "DECODE(LENGTH(:EXAMDIV), 1, CONCAT('0', :EXAMDIV), 2, :EXAMDIV)",
   TERGETGRADE     "DECODE(LENGTH(:TERGETGRADE), 1, CONCAT('0', :TERGETGRADE), 2, :TERGETGRADE)",
   INPLEDATE,
   IN_DATAOPENDATE  "LTRIM(RTRIM(:IN_DATAOPENDATE))",
   OUT_DATAOPENDATE "LTRIM(RTRIM(:OUT_DATAOPENDATE))",
   DOCKINGEXAMCD    "DECODE(LENGTH(:DOCKINGEXAMCD), 1, CONCAT('0', :DOCKINGEXAMCD), 2, :DOCKINGEXAMCD)",
   DOCKINGTYPE,
   DISPSEQUENCE,
   MASTERDIV
)

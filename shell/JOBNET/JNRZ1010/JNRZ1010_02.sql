-- ************************************************
-- *機 能 名 : 模試名
-- *機 能 ID : JNRZ1010_02.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;

-- 更新対象データを削除
DELETE FROM EXAMINATION;

-- テンポラリテーブルからデータを挿入
INSERT INTO EXAMINATION
SELECT * FROM EXAMINATION_TMP;

-- 8年以上前のデータを削除(2006.08.08 kcn)goto add 対応確定でないためまだコメントアウト)
-- DELETE FROM EXAMINATION
-- WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 8);

-- テンポラリテーブル削除
DROP TABLE EXAMINATION_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

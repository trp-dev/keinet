#!/bin/bash

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "ログファイル圧縮 start."
echo ""

back=/keinavi/joblog
prev=`date --date '1 month ago' "+%Y%m"`

#for debug
echo "********** for debug **********"
echo "対象年月： "$prev
echo "********** for debug **********"
echo ""

#圧縮
tar --remove-files -zcvf $back/$prev.tar.gz $back/$prev*

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "ログファイル圧縮 end."
exit 0

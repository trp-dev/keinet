#!/bin/sh

function move_files () {
    src=$1
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` move files start. path:[${src}]
    #ジョブネットのログファイルを取得
    files=$(find $src -type f -maxdepth 1 -name "*.o*" -mtime +0 -or -name "*.e*" -mtime +0)

    #ファイル数分ループ
    for fp in $files; do
        #ファイル名を取得
        file=$(basename $fp)

        #ファイル情報を取得
        dt=$(date +%Y%m%d -r $fp)
        tm=$(date +%H%M%S -r $fp)
        size=`wc -c $fp | awk '{print $1}'`

        #0バイトの場合
        if [ $size -eq 0 ]; then
            rm -rfv $fp
            continue
        fi

        #ディレクトリが存在しない場合
        if [ ! -e $back/$dt ]; then
            #ディレクトリを作成
            echo make dir:[$back/$dt]
            mkdir -p $back/$dt
        fi

        #ファイルを移動
        mv -fv $fp $back/$dt/$file"_"$tm.log
        chmod 644 $back/$dt/$file"_"$tm.log
    done
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` move files end.
}


back=/keinavi/joblog

#/rootディレクトリ配下
move_files /root
#/home/kei-naviディレクトリ配下
move_files /home/kei-navi
#/home/orauserディレクトリ配下
move_files /home/orauser
#/home/ora_ins_accountディレクトリ配下
move_files /home/ora_ins_account
#/keinavi/JOBNETディレクトリ配下
move_files /keinavi/JOBNET
#/keinavi/batディレクトリ配下
move_files /keinavi/bat

exit 0

#!/bin/csh
# SQL起動スクリプト

set HOME = /keinavi/JOBNET/AutoMasterStore
set SetDir = /keinavi/help_user/MasterMainte/Set
set GetDir = /keinavi/help_user/MasterMainte/Get
set OutFile = $HOME/Filelist.txt
set ConfFile = $HOME/TableName.conf

#------Setフォルダ内のアップロードファイルリストを取得
ls $SetDir > $OutFile
echo `date` >> $HOME/Update.log

#------ファイルリスト数を取得
set line = `cat $OutFile | wc -l`
set count = 1

while ($count <= $line)
	#------ファイルリストから各行のファイル名を取得
	set filename = `sed -n {$count}p < $OutFile`

	#------テーブル名定義ファイルから検索IDを取得
	set confline = `wc -l < $ConfFile`
	set count2 = 1
	while ($count2 <= $confline)
		set TableID = `sed -n {$count2}p < $ConfFile`

		#-----アップロードファイル名と検索IDのマッチング
		echo "$filename" | grep $TableID
		if ($status == 0) then
			#------マッチングしたTableIDファイルからJOBNETIDを取得
			set JOBID = `cat $HOME/$TableID`
  			echo $JOBID

			#------マッチングしたTableIDのsql起動
			echo `date` "     $TableID Update"  >> $HOME/Update.log
			cd /keinavi/JOBNET/$JOBID
			./${JOBID}.sh
			if ($status == 0) then
				echo `date` "    $TableID Update end"  >> $HOME/Update.log
				break;
			endif
			echo `date` "  --- $TableID Update ERROR "  >> $HOME/Update.log
			break;
		endif
		@ count2 ++
	end
	@ count ++
end

\rm $OutFile

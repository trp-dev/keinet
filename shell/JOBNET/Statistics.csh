#! /bin/csh 

####### 統計集データ作成バッチ
####### パラメータ無し時はファイルない定義にて直接実行。
####### パラメータあり時はJOBNETにて起動。

#set flg1 = (2005,74 2005,66)
#set flg2 = (2005,74 2005,66)
#set flg3 = (2005,74 2005,66)
#set flg4 = ()

if ($#argv > 0) then
	set confexam = "/keinavi/JOBNET/storedata.csv"
	if (! -f $confexam) then
		echo "---- ERROR: not find $confexam file ----"
		exit 30
	endif
endif

#-- ファイル内の改行コード削除
#set fileout = `cat $csvfile`
#set confexam = `echo ${fileout} | tr -d '\r\n'`


set resultcd = 0

#-- 高校別総合成績一覧表
if (($1 == F002_1)||($#argv < 1)) then
	if ($#argv > 0) then
		set flg1 = `grep 1$ $confexam`
	endif
	if ($#flg1 != 0) then
		cd /keinavi/bat/FreeMenu/F002_1
		set count = 1
		while($count <= $#flg1) 
			set year = `echo $flg1[$count] | awk -F, '{print $1}'`
			set exam = `echo $flg1[$count] | awk -F, '{print $2}'`
			if (-d /keinavi/bat/FreeMenu/F002_1/allresult/${year}/${exam}) then
				\rm -rf /keinavi/bat/FreeMenu/F002_1/allresult/${year}/${exam}
			endif
			@ count ++
		end

		echo "--- 統計集（高校別総合成績一覧表）作成開始"
		./F002_1.sh

		set count = 1
		while($count <= $#flg1) 
			set year = `echo $flg1[$count] | awk -F, '{print $1}'`
			set exam = `echo $flg1[$count] | awk -F, '{print $2}'`
	
			#--作成フォルダから検証用フォルダへコピー
			if (-d /keinavi/freemenu/validation/allresult_stop/${year}/${exam}) then
				\rm -rf /keinavi/freemenu/validation/allresult_stop/${year}/${exam}
			endif
			if (-d /keinavi/bat/FreeMenu/F002_1/allresult/${year}/${exam}) then
				if (! -d /keinavi/freemenu/validation/allresult_stop/${year}) then
					mkdir /keinavi/freemenu/validation/allresult_stop/${year}
					chown orauser /keinavi/freemenu/validation/allresult_stop/${year}
					chgrp ora_ins_group /keinavi/freemenu/validation/allresult_stop/${year}
					chmod 775 /keinavi/freemenu/validation/allresult_stop/${year}
				endif
				chown -R orauser /keinavi/bat/FreeMenu/F002_1/allresult/${year}
				chgrp -R ora_ins_group /keinavi/bat/FreeMenu/F002_1/allresult/${year}
				chmod 775 /keinavi/bat/FreeMenu/F002_1/allresult/${year}/${exam}
				cp -rp /keinavi/bat/FreeMenu/F002_1/allresult/${year}/${exam} /keinavi/freemenu/validation/allresult_stop/${year}/
			endif
			@ count ++
		end
		echo "統計集（高校別総合成績一覧表）作成完了 ---"
		set resultcd = 1
	endif
endif

#-- 型/科目別・高校別成績一覧表
if (($#argv < 1)||($1 == F002_2)) then
	if ($#argv > 0) then
		set flg2 = `grep 2$ $confexam`
	endif
	if ($#flg2 != 0) then
		cd /keinavi/bat/FreeMenu/F002_2
		set count = 1
		while($count <= $#flg2) 
			set year = `echo $flg2[$count] | awk -F, '{print $1}'`
			set exam = `echo $flg2[$count] | awk -F, '{print $2}'`
			if (-d /keinavi/bat/FreeMenu/F002_2/allpattern/${year}/${exam}) then
				\rm -rf /keinavi/bat/FreeMenu/F002_2/allpattern/${year}/${exam}
			endif
			@ count ++
		end

		echo "--- 統計集（型/科目別・高校別成績一覧表）作成開始"
		./F002_2.sh

		set count = 1
		while($count <= $#flg2) 
			set year = `echo $flg2[$count] | awk -F, '{print $1}'`
			set exam = `echo $flg2[$count] | awk -F, '{print $2}'`

			#--作成フォルダから検証用フォルダへコピー
			if (-d /keinavi/freemenu/validation/allpattern_stop/${year}/${exam}) then
				\rm -rf /keinavi/freemenu/validation/allpattern_stop/${year}/${exam}
			endif
			if (-d /keinavi/bat/FreeMenu/F002_2/allpattern_stop/${year}/${exam}) then
				if (! -d /keinavi/freemenu/validation/allpattern_stop/${year}) then
					mkdir /keinavi/freemenu/validation/allpattern_stop/${year}
					chown orauser /keinavi/freemenu/validation/allpattern_stop/${year}
					chgrp ora_ins_group /keinavi/freemenu/validation/allpattern_stop/${year}
					chmod 775 /keinavi/freemenu/validation/allpattern_stop/${year}
				endif
				chown -R orauser /keinavi/bat/FreeMenu/F002_2/allpattern/${year}
				chgrp -R ora_ins_group /keinavi/bat/FreeMenu/F002_2/allpattern/${year}
				chmod 775 /keinavi/bat/FreeMenu/F002_2/allpattern/${year}/${exam}
				cp -rp /keinavi/bat/FreeMenu/F002_2/allpattern/${year}/${exam} /keinavi/freemenu/validation/allpattern_stop/${year}/
			endif
			@ count ++
		end
		echo "統計集（型/科目別・高校別成績一覧表）作成完了 ---"
		set resultcd = 1
	endif
endif

#-- 高校別設問別成績一覧表
if (($#argv < 1)||($1 == F002_3)) then
	if ($#argv > 0) then
		set flg3 = `grep 3$ $confexam`
	endif
	if ($#flg3 != 0) then
		cd /keinavi/bat/FreeMenu/F002_3
		set count = 1
		while($count <= $#flg3) 
			set year = `echo $flg3[$count] | awk -F, '{print $1}'`
			set exam = `echo $flg3[$count] | awk -F, '{print $2}'`
			if (-d /keinavi/bat/FreeMenu/F002_3/question/${year}/${exam}) then
				\rm -rf /keinavi/bat/FreeMenu/F002_3/question/${year}/${exam}
			endif
			@ count ++
		end
		echo "--- 統計集（高校別設問別成績一覧表）作成開始"
		./F002_3.sh

		set count = 1
		while($count <= $#flg3) 
			set year = `echo $flg3[$count] | awk -F, '{print $1}'`
			set exam = `echo $flg3[$count] | awk -F, '{print $2}'`

			#-- 作成フォルダから検証用フォルダへコピー
			if (-d /keinavi/freemenu/validation/question_stop/${year}/${exam}) then
				\rm -rf /keinavi/freemenu/validation/question_stop/${year}/${exam}
			endif
			if (-d /keinavi/bat/FreeMenu/F002_3/question/${year}/${exam}) then
				if (! -d /keinavi/freemenu/validation/question_stop/${year}) then
					mkdir /keinavi/freemenu/validation/question_stop/${year}
					chown orauser /keinavi/freemenu/validation/question_stop/${year}
					chgrp ora_ins_group /keinavi/freemenu/validation/question_stop/${year}
					chmod 775 /keinavi/freemenu/validation/question_stop/${year}
				endif
				chown -R orauser /keinavi/bat/FreeMenu/F002_3/question/${year}
				chgrp -R ora_ins_group /keinavi/bat/FreeMenu/F002_3/question/${year}
				chmod 775 /keinavi/bat/FreeMenu/F002_3/question/${year}/${exam}
				cp -rp /keinavi/bat/FreeMenu/F002_3/question/${year}/${exam} /keinavi/freemenu/validation/question_stop/${year}/
			endif
			@ count ++
		end
		echo "統計集（高校別設問別成績一覧表）作成完了 ---"
		set resultcd = 1
	endif
endif

#-- 国公立大学学部別・高校別志望状況
if (($#argv < 1)||($1 == F002_4)) then
	if ($#argv > 0) then
		set flg4 = `grep 4$ $confexam`
	endif
	if ($#flg4 != 0) then
		cd /keinavi/bat/FreeMenu/F002_4
		set count = 1
		while($count <= $#flg4) 
			set year = `echo $flg4[$count] | awk -F, '{print $1}'`
			set exam = `echo $flg4[$count] | awk -F, '{print $2}'`

			if (-d /keinavi/bat/FreeMenu/F002_4/allcandidate/${year}/${exam}) then
				\rm -rf /keinavi/bat/FreeMenu/F002_4/allcandidate/${year}/${exam}
			endif
			@ count ++
		end
		echo "--- 統計集（国公立大学学部別・高校別志望状況）作成開始"
		./F002_4.sh
	
		set count = 1
		while($count <= $#flg4) 
			set year = `echo $flg4[$count] | awk -F, '{print $1}'`
			set exam = `echo $flg4[$count] | awk -F, '{print $2}'`

			#-- 作成フォルダから検証用フォルダへコピー
			if (-d /keinavi/freemenu/validation/allcandidate_stop/${year}/${exam}) then
				\rm -rf /keinavi/freemenu/validation/allcandidate_stop/${year}/${exam}
			endif
			if (-d /keinavi/bat/FreeMenu/F002_4/allcandidate/${year}/${exam}) then
				if (! -d /keinavi/freemenu/validation/allcandidate_stop/${year}) then
					mkdir /keinavi/freemenu/validation/allcandidate_stop/${year}
					chown orauser /keinavi/freemenu/validation/allcandidate_stop/${year}
					chgrp ora_ins_group /keinavi/freemenu/validation/allcandidate_stop/${year}
					chmod 775 /keinavi/freemenu/validation/allcandidate_stop/${year}
				endif
				chown -R orauser /keinavi/bat/FreeMenu/F002_4/allcandidate/${year}
				chgrp -R ora_ins_group /keinavi/bat/FreeMenu/F002_4/allcandidate/${year}
				chmod 775 /keinavi/bat/FreeMenu/F002_4/allcandidate/${year}/${exam}
				cp -rp /keinavi/bat/FreeMenu/F002_4/allcandidate/${year}/${exam} /keinavi/freemenu/validation/allcandidate_stop/${year}/
			endif
			@ count ++
		end
		echo "統計集（国公立大学学部別・高校別志望状況）作成完了 ---"
		set resultcd = 1
	endif
endif

if ($#argv > 0) then
	#--- 定義ファイルの削除
	rm -f $confexam
endif

if ($resultcd == 0) then
	echo "登録対象データは存在しません"
	exit 11
endif
exit 0


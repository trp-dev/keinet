#!/usr/bin/perl
########################################
# 判定チェックツール input用ファイル作成
#

$ORG_FILE = "/keinavi/HULFT/WorkDir/INDIVIDUALRECORD";
$OUT_FILE = "/keinavi/HULFT/WorkDir/INDIVIDUALRECORD_add9";

if(! open(IN, "$ORG_FILE")){
        print "FILE OPEN ERROR!\n";
        exit;
}
if(! open(OUT, ">$OUT_FILE") ){
        close(IN);
        exit;
}

while(<IN>){
        chomp($_);
        $header  = substr($_,    0, 108);
        $subrec01 = substr($_,  108, 30);
        $subrec02 = substr($_,  138, 30);
        $subrec03 = substr($_,  168, 30);
        $subrec04 = substr($_,  198, 30);
        $subrec05 = substr($_,  228, 30);
        $subrec06 = substr($_,  258, 30);
        $subrec07 = substr($_,  288, 30);
        $subrec08 = substr($_,  318, 30);
        $subrec09 = substr($_,  348, 30);
        $subrec10 = substr($_,  378, 30);
        $subrec11 = substr($_,  408, 30);
        $subrec12 = substr($_,  438, 30);
        $subrec13 = substr($_,  468, 30);
        $subrec14 = substr($_,  498, 30);
        $subrec15 = substr($_,  528, 30);
        $subrec16 = substr($_,  558, 30);
        $subrec17 = substr($_,  588, 30);
        $subrec18 = substr($_,  618, 30);
        $subrec19 = substr($_,  648, 30);
        $subrec20 = substr($_,  678, 30);
        $subrec21 = substr($_,  708, 30);
        $subrec22 = substr($_,  738, 30);
        $footer   = substr($_,  768, );

        $outSTR = "$header" .
                $subrec01 . '9'.
                $subrec02 . '9'.
                $subrec03 . '9'.
                $subrec04 . '9'.
                $subrec05 . '9'.
                $subrec06 . '9'.
                $subrec07 . '9'.
                $subrec08 . '9'.
                $subrec09 . '9'.
                $subrec10 . '9'.
                $subrec11 . '9'.
                $subrec12 . '9'.
                $subrec13 . '9'.
                $subrec14 . '9'.
                $subrec15 . '9'.
                $subrec16 . '9'.
                $subrec17 . '9'.
                $subrec18 . '9'.
                $subrec19 . '9'.
                $subrec20 . '9'.
                $subrec21 . '9'.
                $subrec22 . '9'.
                $footer;
        print OUT "$outSTR\n";
}
close(OUT);
close(IN);

print "FINISH!\n";
exit;



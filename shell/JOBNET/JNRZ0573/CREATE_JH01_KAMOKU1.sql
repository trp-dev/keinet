-- ************************************************
-- *機 能 名 : 情報誌科目（共通）
-- *機 能 ID : CREATE_JH01_KAMOKU1.sql
-- *作 成 日 : 2015/09/28
-- *作 成 者 : 富士通システムズ・ウエスト
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- 情報誌科目（共通）テーブル削除
DROP TABLE JH01_KAMOKU1 CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 情報誌科目（共通）テーブル作成
CREATE TABLE JH01_KAMOKU1  (
       COMBINATION_KEY1   CHAR(5)        NOT NULL,    /*   記入用５桁コード    */
       COMBINATION_KEY2   CHAR(2)        NOT NULL,    /*   センタ枝番          */
       COMBINATION_KEY3   CHAR(2)        NOT NULL,    /*   二次枝番            */
       THE_YEAR           NUMBER(4,0)            ,    /*   年度                */
       MOCK_TYP           CHAR(2)                ,    /*   模試区分            */
       UNIV_TYP           CHAR(1)                ,    /*   大学区分            */
       SET_PTN            CHAR(1)                ,    /*   設定パターン        */
       SET_PTN_NAME       VARCHAR2(12)           ,    /*   設定パターン名      */
       HONBU_PREF_CD      CHAR(2)                ,    /*   本部県コード        */
       UNIV_KANA          VARCHAR2(40)           ,    /*   大学カナ名          */
       DAY_NIGHT_TYP      CHAR(1)                ,    /*   昼夜部区分          */
       CON_CD             CHAR(3)                ,    /*   学部内容コード      */
       DEP_SERIAL_NO      NUMBER(2,0)            ,    /*   学部通番            */
       UNIV_GROUP         CHAR(1)                ,    /*   大学グループ        */
       SCH_METHOD_CD      CHAR(2)                ,    /*   日程方式コード      */
       SUB_SERIAL_NO      NUMBER(4,0)            ,    /*   学科通番            */
       SORT_NO            NUMBER(2,0)            ,    /*   ソート              */
       MIDDLE_SYS_CD      CHAR(2)                ,    /*   中系統コード        */
       SHO_SYS_MAIN       CHAR(2)                ,    /*   小系統メイン        */
       SHO_SYS2           CHAR(2)                ,    /*   小系統２            */
       SHO_SYS3           CHAR(2)                ,    /*   小系統３            */
       SHO_SYS4           CHAR(2)                ,    /*   小系統４            */
       SHO_SYS5           CHAR(2)                ,    /*   小系統５            */
       SHO_SYS6           CHAR(2)                ,    /*   小系統６            */
       SHO_SYS7           CHAR(2)                ,    /*   小系統７            */
       SHO_SYS8           CHAR(2)                ,    /*   小系統８            */
       SHO_SYS9           CHAR(2)                ,    /*   小系統９            */
       SHO_SYS10          CHAR(2)                ,    /*   小系統１０          */
       DETAIL_SYS_CD      CHAR(4)                ,    /*   詳細系統コード      */
       UNIV_CHG_TYP       CHAR(1)                ,    /*   大学改組区分        */
       DEP_CHG_TYP        CHAR(1)                ,    /*   学部改組区分        */
       SUB_CHG_TYP        CHAR(1)                ,    /*   学科改組区分        */
       REG_CD10           CHAR(10)               ,    /*   戸籍１０桁コード    */
       ENTRY5             CHAR(5)                ,    /*   記入用５桁          */
       CEN_BRANCH_CD      CHAR(2)                ,    /*   センタ枝番          */
       SEC_BRANCH_CD      CHAR(2)                ,    /*   二次枝番            */
       UNIV_C_NAME        VARCHAR2(28)           ,    /*   大学名（短）        */
       DEP_INF_NAME       VARCHAR2(40)           ,    /*   情報誌用学部名      */
       BOSHU_O_NAME       VARCHAR2(32)           ,    /*   募集区分（個）      */
       UNIV_NAME          VARCHAR2(80)           ,    /*   大学名              */
       UNIV               VARCHAR2(16)           ,    /*   大学                */
       DEP_CD_NAME        VARCHAR2(80)           ,    /*   学部名              */
       DEP                VARCHAR2(16)           ,    /*   学部                */
       SUB_CD_NAME        VARCHAR2(80)           ,    /*   学科名              */
       SCH_METHOD_NAME    VARCHAR2(28)           ,    /*   日程方式名          */
       BRANCH_NAME        VARCHAR2(40)           ,    /*   枝番名称            */
       CAPA_TRUST         CHAR(1)                ,    /*   定員信頼            */
       BOSHU_CAPA         NUMBER(5,0)            ,    /*   募集定員            */
       CEN_PERFECT_SC     NUMBER(5,1)            ,    /*   センタ満点          */
       BORDER_SC          NUMBER(5,1)            ,    /*   ボーダ得点          */
       BORDER_SC_RATE     NUMBER(2,0)            ,    /*   ボーダ得点率        */
       SEC_PERFECT_SC     NUMBER(5,1)            ,    /*   二次満点            */
       RANK               CHAR(2)                ,    /*   ランク              */
       RANK_SS            NUMBER(4,1)            ,    /*   ランク偏差値        */
       CONSTRAINT PK_JH01_KAMOKU1 PRIMARY KEY (
                 COMBINATION_KEY1         ,               /*   記入用５桁コード    */
                 COMBINATION_KEY2         ,               /*   センタ枝番          */
                 COMBINATION_KEY3                         /*   二次枝番            */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

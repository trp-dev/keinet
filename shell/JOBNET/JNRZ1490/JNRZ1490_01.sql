-- ************************************************
-- *機 能 名 : 認定試験情報マスタ
-- *機 能 ID : JNRZ1490_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE ENGPTINFO_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE ENGPTINFO_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM ENGPTINFO WHERE 0=1;

-- 主キーの設定
ALTER TABLE ENGPTINFO_TMP ADD CONSTRAINT PK_ENGPTINFO_TMP PRIMARY KEY 
(
   EVENTYEAR
 , EXAMDIV
 , ENGPTCD
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

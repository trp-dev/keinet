-- *********************************************************
-- *機 能 名 : 認定試験情報マスタ
-- *機 能 ID : JNRZ1490_02.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM ENGPTINFO MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM ENGPTINFO_TMP TMP
         WHERE 1=1
           AND MAIN.EVENTYEAR  = TMP.EVENTYEAR
           AND MAIN.EXAMDIV    = TMP.EXAMDIV
           AND MAIN.ENGPTCD    = TMP.ENGPTCD
       )
;

-- テンポラリから本物へ登録
INSERT INTO ENGPTINFO SELECT * FROM ENGPTINFO_TMP;

-- テンポラリテーブル削除
DROP TABLE ENGPTINFO_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

#!/bin/sh

# backup_oracle_data_3.sh
#
# ★★★注意★★★ rootユーザで実行する事
#
# <処理概要>
#   DB1サーバのOracleデータ用HDDパーティションの内容をOPC機能を利用して、
#   バックアップ領域(HDD)にフルデータコピーによるバックアップを実施するための
#   後処理を実施する。(下記、★★★部分)
#
# <バックアップ全体の流れ>
#   1) Oracleをバックアップモード開始 
#   2) Oracleの制御ファイルの一部のバックアップファイルを作成
#   3) OracleのデータファイルのHDDパーティション全体をコピー (OPC機能を利用)
#   4) Oracleをバックアップモード終了
#   5) Oracleのアーカイブログファイルの出力(REDOログのスイッチ)
#   6) OracleのアーカイブログファイルをDB2サーバにバックアップ(転送) ★★★
#   7) Oracleの不要なアーカイブログファイルを削除 ★★★
#
# <作成者>
#   2010/4/28  KCN
#

## DBインスタンスが起動中である事の確認 (非起動中なら強制終了)
ps -elf | grep ora_...._keinavi | grep -v grep > /tmp/db1_status.txt
if [ ! -s /tmp/db1_status.txt ];then
    echo "`date` <ERR> DB1:keinavi Stoping to rejected."
    rm /tmp/db1_status.txt
    exit 20
fi
rm /tmp/db1_status.txt

## アーカイブログファイルをDB2サーバにバックアップ(転送) ※リカバリに必要
#echo "`date` <INF> Archive log DB2 send."
#rsync -av --delete -e /usr/bin/rsh /keinavi/oracle/oradata/keinavi/archive rpve04test:/keinavi/oracle/DB1_KEINAVI_ARCHIVE_BACKUP

## 不要なアーカイブログファイルを削除 (古いファイルを削除)
echo "`date` <INF> Archive log deleted."
/usr/bin/find /keinavi/oracle/oradata/keinavi/archive -name *.dbf -mtime +5
/usr/bin/find /keinavi/oracle/oradata/keinavi/archive -name *.dbf -mtime +5 | xargs rm -f

## 終了処理
echo "`date` <INF> End"

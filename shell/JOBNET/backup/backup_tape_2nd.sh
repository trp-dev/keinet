#!/bin/sh

#
# テープへのバックアップスクリプト
#   < 月第2週目用 >
#

# netvaultジョブをトリガーで呼び出す
#   第二週トリガー名：KEINAVI-2nd

rsh rodn2 "/keinavi/netvault/util/nvtrigger -server rodn2 -wait Schedule-KEINAVI-2nd"

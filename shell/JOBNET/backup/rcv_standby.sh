#!/bin/sh

# !!This script must be executed by oracle user.
#    and needs recover.sql on the same dir.

ARC_DIR="/keinavi/oracle/oradata/keinavistby/archive"
BAC_DIR="/keinavi/oracle/oradata/keinavistby/archive/old"

echo "### Start Recovery" 

# Check necessary sql files.
if [ ! -f ./recover.sql ]; then
  echo "SQL file is not found. Exit."
  exit 30
fi

echo "# Recover standby database.."
#sqlplus "sys/manager@C12DB_batch1stby as sysdba" @./recover.sql
if [ $? -ne 0 ]; then
  exit 30
fi

echo "# Move archive logs to backup dir.."
#mv $ARC_DIR/* $BAC_DIR/
echo "mv $ARC_DIR/* $BAC_DIR/"
if [ $? -ne 0 ]; then
  exit 30
fi

echo "# Remove old archive logs.."
#rm -f $ARC_DIR/*
echo "rm -f $FROM_ARC_DIR/*"

echo "All processes are finished correctly."
exit 0

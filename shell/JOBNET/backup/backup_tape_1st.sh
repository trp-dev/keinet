#!/bin/sh

#
# テープへのバックアップスクリプト
#   < 月第一週目用 >
#

# netvaultジョブをトリガーで呼び出す
#   第一週トリガー名：KEINAVI-1st

rsh rodn2 "/keinavi/netvault/util/nvtrigger -server rodn2 -wait KEINAVI-1st"

#!/bin/sh

ADDRESS="sniper@kawai-juku.ac.jp"
#ADDRESS="isomura@keinet.ne.jp"
SERVERNAME="tca.keinet.ne.jp"
INTPER=70
INTDISCSIZE=100000

FLGWARN=`df -k | grep -iv Filesystem | awk '{print $4"\t"$5"\t"$6}' | sed s/%// | while read AVAIL USE MOUNT
do
  if [ ${MOUNT} = "/etc/mnttab" ] || [ ${MOUNT} = "/proc" ] || [ ${MOUNT} = "/dev/fd" ]; then
    continue;
  fi
  if [ ${AVAIL} -le ${INTDISCSIZE} ]; then
     echo 9;
     break;
  fi
  if [ ${USE} -ge ${INTPER} ]; then
    echo 9;
    break;
  fi
done`

if [ ${FLGWARN} ]; then
  if [ ${FLGWARN} = "9" ]; then
#    df -k | /bin/mail -s "${SERVERNAME} server disc space warning" ${ADDRESS}
  echo "WARNING!"
  fi
fi
exit 1


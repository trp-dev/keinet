#!/bin/sh

#
# テープへのバックアップスクリプト
#   < 月第4週目用 >
#

# netvaultジョブをトリガーで呼び出す
#   第4週トリガー名：KEINAVI-4th

rsh rodn2 "/keinavi/netvault/util/nvtrigger -server rodn2 -wait KEINAVI-4th"

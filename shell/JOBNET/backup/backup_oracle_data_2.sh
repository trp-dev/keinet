#!/bin/sh

# backup_oracle_data_2.sh
#
# ★★★注意★★★ ora_ins_accountユーザで実行する事
#
# <処理概要>
#   DB1サーバのOracleデータ用HDDパーティションの内容をOPC機能を利用して、
#   バックアップ領域(HDD)にフルデータコピーによるバックアップを実施するための
#   後処理を実施する。(下記、★★★部分)
#
# <バックアップ全体の流れ>
#   1) Oracleをバックアップモード開始 
#   2) Oracleの制御ファイルの一部のバックアップファイルを作成
#   3) OracleのデータファイルのHDDパーティション全体をコピー (OPC機能を利用)
#   4) Oracleをバックアップモード終了 ★★★
#   5) Oracleのアーカイブログファイルの出力(REDOログのスイッチ) ★★★
#   6) OracleのアーカイブログファイルをDB2サーバにバックアップ(転送)
#   7) Oracleの不要なアーカイブログファイルを削除
#
# <作成者>
#   2010/4/28  KCN
#

## 環境変数
export ORACLE_BASE=/opt/oracle
export ORACLE_HOME=$ORACLE_BASE/product/9.2.0
export ORACLE_SID=C12DB_batch1
export NLS_LANG=Japanese_Japan.JA16EUC
export ORACLE_DOC=$ORACLE_HOME/doc
export ORA_NLS33=$ORACLE_HOME/ocommon/nls/admin/data
export PATH=$PATH:$ORACLE_HOME/bin
export ORACLE_NLS=JA16EUC
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ORACLE_HOME/lib
export LANG=ja_JP.eucJP

alias RUN_SQL='sqlplus -s "sys/manager@C12DB_batch1 as sysdba"'

## DBインスタンスが起動中である事の確認 (非起動中なら強制終了)
ps -elf | grep ora_...._keinavi | grep -v grep > /tmp/db1_status.txt
if [ ! -s /tmp/db1_status.txt ];then
    echo "`date` <ERR> DB1:keinavi Stoping to rejected."
    rm /tmp/db1_status.txt
    exit 20
fi
rm /tmp/db1_status.txt

## バックアップモードの出力
echo "`date` <INF> Backup mode status display. NO1"
RUN_SQL <<EOF
  set linesize 200
  set pagesize 999
  select tablespace_name,file_name,file_id,v\$backup.status from dba_data_files,v\$backup where dba_data_files.FILE_ID= v\$backup.FILE#;
  exit
EOF

## バックアップモード終了
echo "`date` <INF> Backup mode end."
RUN_SQL <<EOF
  alter tablespace DRSYS          end backup;
  alter tablespace INDX           end backup;
  alter tablespace SYSTEM         end backup;
  alter tablespace TOOLS          end backup;
  alter tablespace UNDOTBS1       end backup;
  alter tablespace XDB            end backup;
  alter tablespace KEINAVI_INDX    end backup;
  alter tablespace KEINAVI_DATA   end backup;
  alter tablespace KEINAVI_DATA    end backup;
  alter tablespace KEINAVI_DATA    end backup;
  alter tablespace KEINAVI_DATA    end backup;
  alter tablespace KEISPRO_INDIV  end backup;
  alter tablespace KEISPRO_MAIN   end backup;
  alter tablespace KEISPRO_MST    end backup;
  exit
EOF

## バックアップモードの出力
echo "`date` <INF> Backup mode status display.NO2"
RUN_SQL <<EOF
  set linesize 200
  set pagesize 999
  select tablespace_name,file_name,file_id,v\$backup.status from dba_data_files,v\$backup where dba_data_files.FILE_ID= v\$backup.FILE#;
  exit
EOF

## アーカイブログファイルの出力(REDOログのスイッチ)
echo "`date` <INF> Archive log swhich."
RUN_SQL <<EOF
  alter system archive log current;
  exit
EOF

## 終了処理
unalias RUN_SQL
echo "`date` <INF> End"

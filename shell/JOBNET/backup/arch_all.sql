-- archivelogバックアップ前処理
-- アーカイブログをファイルに書き出す

--- Stop DB
shutdown immediate;

--- Startup DB
startup;

--- Write out redo logs to archive. 
alter system archive log current;
archive log list;

--- Stop DB
--shutdown immediate;

exit;

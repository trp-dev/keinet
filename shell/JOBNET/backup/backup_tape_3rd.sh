#!/bin/sh

#
# テープへのバックアップスクリプト
#   < 月第3週目用 >
#

# netvaultジョブをトリガーで呼び出す
#   第3週トリガー名：KEINAVI-3rd

rsh rodn2 "/keinavi/netvault/util/nvtrigger -server rodn2 -wait KEINAVI-3rd"

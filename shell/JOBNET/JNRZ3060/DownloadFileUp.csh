#! /bin/csh 

##### 学力要素データダウンロード 自動作成(検証機転送&作成)バッチ
##### ユーザ orauser にて実行可能（パラメータ0は単独、1は他JOBNETからの起動。なしは定義ファイルなしで起動）

set DLupconf = "/keinavi/JOBNET/AbilityDLup"

set place = "/keinavi/HULFT"
cd $place
if ($1 == 0) then
    /keinavi/JOBNET/Fcheck/fcheck.csh 1
    if ($status != 0) then
        exit 30
    endif

    #-- 受信ファイルチェックの問題がなければ、定義ファイルを削除
    \rm -f /home/navikdc/datastore/inside_release/*
    echo " "
endif

#---　データ抽出処理などでは定義ファイルはなしのため
if ((($1 == 0)||($1 == 1))&&(! -f $DLupconf)) then
        echo "---- DLファイル展開対象ではありません ----"
        exit 1
endif


#--- 対象データファイルリスト取得
(ls -rt ACADEMICRECORD_I_FD* > counttemp.log) >& /dev/null
set FILELIST = `cat counttemp.log`
\rm -f counttemp.log
if ($#FILELIST == 0) then
    echo "---- データファイルが見つかりません ----"
endif

set storecount = 0     #DLFileは存在するが定義ファイルに対する模試のDLFileがない場合を識別するフラグ
set count = 1
while ($count <= $#FILELIST)
    
    #----対象ファイル名を取得
    set target =  $FILELIST[$count]

    #----ファイルから年度と模試コードを取得
    set orgyear=`awk -F, NR=="2"'{print $2}' $target`
    set exam=`awk -F, NR=="2"'{print $3}' $target`

    #----センターリサーチ用に年度を1年戻す
    if ($exam == "38") then
        set year = `expr $orgyear - 1`
    else
        set year = $orgyear
    endif

    #---- 定義ファイル以外のデータは登録処理を行わない
    if ((($1 == 0)||($1 == 1))&&(-f $DLupconf)) then
        set resultcd = `grep "$year$exam" "$DLupconf"`
        if ($resultcd == "") then
            @ count ++
            continue
        endif
    endif    
    echo "--- 登録対象ファイル：$target (年度:$year 模試:$exam)"

    set storecount = 1 

    #----対象ファイルを検証機へFTP転送
    echo "--- ファイル転送開始..."
    /keinavi/HULFT/ftp_rode.sh /HULFT/ACADEMICRECORD_I_FD $place $target satu04 orauser
    #----リモートシェルを実行&結果取得
    /usr/bin/rsh satu04 /keinavi/JOBNET/JNRZ3060/JNRZ3060.sh $target $orgyear $exam
    if ($status != 0) then
        echo "-1" > resulout.log
    else
        /usr/bin/rsh satu04 cat /keinavi/JOBNET/ABILITY.log > resulout.log
    endif
    /usr/bin/rsh satu04 \rm /keinavi/JOBNET/ABILITY.log

    #----コマンド実行結果の識別
    set outcord = `head -1 resulout.log`
    \rm -f resulout.log
    if ($outcord > 0) then
        echo "---- 学力要素データダウンロードファイル $target に対して処理を中断します ----" 
        echo " "
         @ count ++
        continue
    endif
        
    echo "--- 学力要素データダウンロード ${year}${exam} 作成完了 ---" 
    echo " "

    #----作成ファイルを検証機へアップ
    /usr/bin/rsh satu01test -l kei-navi /home/kei-navi/dataupscript/dataup.sh /keinavi/freemenu/abilityup/${year} ${exam}.tar.Z /keinavi/freemenu/ability/${year} 10.1.4.177
    if ($status != 0) then
#        /usr/bin/rsh satu04 \rm /keinavi/freemenu/examup/${year}/${exam}.tar.Z
        echo "  "
        @ count ++
        continue
        else
                echo "--- 学力要素データダウンロードデータの同期開始..."
                /usr/bin/rsh satu01test -l kei-navi /usr/local/etc/rsync_keinavi_download.sh
                echo "学力要素データダウンロードデータの同期処理完了 ---"
    endif
    echo "--- 作成ファイル 検証機展開処理完了 ---"

    #----対象ファイルをDATAフォルダへ移動
#    echo "--- 対象ファイルの移動..."
#    if ((${exam} == 61)||(${exam} == 71)) then
#        #--- #1高１２模試
#        set yeare = "${year}61_71"
#        set yexam = "$yeare/${exam}"
#    else if ((${exam} == 62)||(${exam} == 72)) then
#        #--- #2高１２模試
#        set yeare = "${year}62_72"
#        set yexam = "$yeare/${exam}"
#    else if ((${exam} == 63)||(${exam} == 73)) then
#        #--- #3高１２模試
#        set yeare = "${year}63_73"
#        set yexam = "$yeare/${exam}"
#    else if ((${exam} == 65)||(${exam} == 75)) then
#        #--- 高１２記述模試
#        set yeare = "${year}65_75"
#        set yexam = "$yeare/${exam}"
#    else if ((${exam} == 28)||(${exam} == 68)||(${exam} == 78)) then
#        #--- 新テスト
#        set yeare = "${year}28_68_78"
#        set yexam = "$yeare/${exam}"
#    else if ((${exam} == 37)||(${exam} == 69)||(${exam} == 79)) then
#        #--- 受験学力測定テスト
#        set yeare = "${year}37_69_79"
#        set yexam = "$yeare/${exam}"
#    else
        set yeare = 0
        set yexam = "${year}${exam}"
#    endif
            
    if (! -d /keinavi/HULFT/DATA/${yexam}) then
        mkdir -p /keinavi/HULFT/DATA/${yexam}
        chmod 777 /keinavi/HULFT/DATA/${yexam}
        if ($yeare != 0) then
            chmod 777 /keinavi/HULFT/DATA/$yeare
        endif
    endif
    mv $target /keinavi/HULFT/DATA/${yexam}
    echo "  "

    @ count ++
end

#-- OP模試などDLファイルが存在しないとき 
if ((($1 == 0)||($1 == 1))&&(-f $DLupconf)) then
    if (($#FILELIST == 0)||($storecount == 0)) then
        echo "---- 登録対象のデータファイルが存在しません ----"
    endif
    \rm -f $DLupconf
endif

-- *********************************************************
-- *機 能 名 : 国語設問別条件別正答率・設問別評価人数（県）
-- *機 能 ID : JNRZ1370_02.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM KKG_ANSRT_EVALNUMS_P MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM KKG_ANSRT_EVALNUMS_P_TMP TMP
         WHERE 1=1
           AND MAIN.EXAMYEAR     = TMP.EXAMYEAR
           AND MAIN.EXAMCD       = TMP.EXAMCD
           AND MAIN.PREFCD       = TMP.PREFCD
           AND MAIN.SUBCD        = TMP.SUBCD
           AND MAIN.QUESTION_NO  = TMP.QUESTION_NO
       )
;

-- テンポラリから本物へ登録
INSERT INTO KKG_ANSRT_EVALNUMS_P SELECT * FROM KKG_ANSRT_EVALNUMS_P_TMP;

-- テンポラリテーブル削除
DROP TABLE KKG_ANSRT_EVALNUMS_P_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

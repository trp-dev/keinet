------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 模試科目マスタ
--  テーブル名      : EXAMSUBJECT
--  データファイル名: EXAMSUBJECTyyyymmdd.csv
--  機能            : 模試科目マスタをデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 2004/09/23
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE EXAMSUBJECT_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   EXAMYEAR,
   EXAMCD       "DECODE(LENGTH(:EXAMCD), 1, CONCAT('0', :EXAMCD), 2, :EXAMCD)",
   SUBCD,
   SUBNAME,
   SUBADDRNAME,
   SUBALLOTPNT  "NULLIF(:SUBALLOTPNT,     '9999')",
   DISPSEQUENCE,
   FUSION_SUBCD,
   NOTDISPLAYFLG  "lpad(:NOTDISPLAYFLG, 8, '0')" 
)

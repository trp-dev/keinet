-- ************************************************
-- *機 能 名 : 模試科目マスタ
-- *機 能 ID : JNRZ1020_01.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE EXAMSUBJECT_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

CREATE TABLE EXAMSUBJECT_TMP(
    EXAMYEAR       CHAR(4)       NOT NULL,    /*   模試年度    */ 
    EXAMCD         CHAR(2)       NOT NULL,    /*   模試コード  */ 
    SUBCD          CHAR(4)       NOT NULL,    /*   科目コード  */ 
    SUBNAME        VARCHAR2(20)          ,    /*   科目名      */ 
    SUBADDRNAME    VARCHAR2(6)           ,    /*   科目短縮名  */ 
    SUBALLOTPNT    NUMBER(4,0)           ,    /*   配点        */ 
    DISPSEQUENCE   NUMBER(4)             ,    /*   表示順序    */ 
    FUSION_SUBCD   CHAR(4)               ,    /*   集計先科目コード */
    NOTDISPLAYFLG  CHAR(8)               ,    /*   非表示制御フラグ */
CONSTRAINT PK_EXAMSUBJECT_TMP PRIMARY KEY(
        EXAMYEAR,
        EXAMCD,
        SUBCD
    )
    USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

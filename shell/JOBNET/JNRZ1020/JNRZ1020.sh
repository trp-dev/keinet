#!/bin/sh

#環境変数設定
export JOBNAME=JNRZ1020
export CSVFILE=EXAMSUBJECT*.csv
export SRCDIR=/keinavi/help_user/MasterMainte/Set
export DSTDIR=/keinavi/help_user/MasterMainte/Get
export WORKDIR=/keinavi/JOBNET/$JOBNAME

#SQL*PLUSテンポラリテーブル作成
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ1020_01.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo テンポラリテーブル作成処理でエラーが発生しました
    exit 30
fi


#SQL*LOADERシェル実行
/keinavi/JOBNET/common/JNRZ1IMP.sh

#SQL*LOADER戻り値チェック(0以外は異常終了)
RET=$?
if [ $RET -ne 0 ]
then
    if [ $RET -le 10 ]
    then
        #SQL*PLUSテンポラリテーブル削除
        /keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ1020_03.sql
        echo 処理対象ファイルなし（正常終了）
        exit 0
    else
        echo SQL*LOADERでエラーが発生しました
        exit 30
    fi
fi

#SQL*PLUSデータインポート->テンポラリテーブル削除
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ1020_02.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo データのインポート処理でエラーが発生しました
    exit 30
fi

#正常終了
echo データのインポート処理が正常に終了しました
exit  0    #正常終了

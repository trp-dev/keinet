-- ************************************************
-- *機 能 名 : 志望大学評価別人数(全国)
-- *機 能 ID : JNRZ0181_01.sql
-- *作 成 日 : 2004/08/30
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 : 2009/11/30
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE RATINGNUMBER_A_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE RATINGNUMBER_A_TMP  (
       EXAMYEAR                  CHAR(4)             NOT NULL,   /*     模試年度            */ 
       EXAMCD                    CHAR(2)             NOT NULL,   /*     模試コード          */ 
       RATINGDIV                 CHAR(1)             NOT NULL,   /*     評価区分            */ 
       UNIVCOUNTINGDIV           CHAR(1)             NOT NULL,   /*     大学集計区分        */ 
       UNIVCD                    CHAR(4)             NOT NULL,   /*     大学コード          */ 
       FACULTYCD                 CHAR(2)             NOT NULL,   /*     学部コード          */ 
       DEPTCD                    CHAR(4)             NOT NULL,   /*     学科コード          */ 
       AGENDACD                  CHAR(1)             NOT NULL,   /*     日程コード          */ 
       STUDENTDIV                CHAR(1)             NOT NULL,   /*     現役高卒区分        */ 
       AVGDEVIATION              NUMBER(4,1)                 ,   /*     平均偏差値          */ 
       AVGSCORERATE              NUMBER(4,1)                 ,   /*     平均得点率          */ 
       TOTALCANDIDATENUM         NUMBER(8,0)                 ,   /*     総志望者数          */ 
       FIRSTCANDIDATENUM         NUMBER(8,0)                 ,   /*     第一志望者数        */ 
       RATINGNUM_A               NUMBER(8,0)                 ,   /*     評価別人数Ａ        */ 
       RATINGNUM_B               NUMBER(8,0)                 ,   /*     評価別人数Ｂ        */ 
       RATINGNUM_C               NUMBER(8,0)                 ,   /*     評価別人数Ｃ        */ 
       RATINGNUM_D               NUMBER(8,0)                 ,   /*     評価別人数Ｄ        */ 
       RATINGNUM_E               NUMBER(8,0)                 ,   /*     評価別人数Ｅ        */ 
       CONSTRAINT PK_RATINGNUMBER_A_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  模試年度      */
                 EXAMCD                    ,               /*  模試コード    */
                 RATINGDIV                 ,               /*  評価区分      */
                 UNIVCOUNTINGDIV           ,               /*  大学集計区分  */
                 UNIVCD                    ,               /*  大学コード    */
                 FACULTYCD                 ,               /*  学部コード    */
                 DEPTCD                    ,               /*  学科コード    */
                 AGENDACD                  ,               /*  日程コード    */
                 STUDENTDIV                                /*  現役高卒区分  */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

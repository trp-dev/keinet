-- *********************************************************
-- *機 能 名 : 大学系統マスタ
-- *機 能 ID : JNRZ1130_02.sql
-- *作 成 日 : 2004/08/31
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 大学系統マスタの更新処理
DECLARE
    wk_rowid    ROWID;          -- 既存レコードのROWID
BEGIN
    FOR r_cap IN (SELECT USERID,
                         CERT_BEFOREDATA,
                         REGISTRYDATE,
                         RENEWALDATE,
                         LOGINPWD
                FROM     HELPDESKID_TMP)
    LOOP
        BEGIN
            /* 既存レコードを検索 */
            SELECT ROWID INTO wk_rowid
            FROM   PACTSCHOOL
            WHERE  USERID    = r_cap.USERID;

            /* あれば更新 */
            UPDATE PACTSCHOOL SET
                   CERT_BEFOREDATA   = r_cap.CERT_BEFOREDATA,
                   REGISTRYDATE      = r_cap.REGISTRYDATE,
                   RENEWALDATE       = r_cap.RENEWALDATE,
                   LOGINPWD          = r_cap.LOGINPWD
            WHERE  ROWID             = wk_rowid;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                /* なければ新規作成 */
                INSERT INTO PACTSCHOOL
                VALUES (
                   r_cap.USERID,
                   NULL,
                   '5',
                   999,
                   0,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   r_cap.CERT_BEFOREDATA,
                   '6',
                   '1',
                   NULL,
                   NULL,
                   r_cap.REGISTRYDATE,
                   r_cap.RENEWALDATE,
                   r_cap.LOGINPWD,
                   r_cap.LOGINPWD,
                   '0'
                );
            WHEN OTHERS THEN
                RAISE;
        END;

    END LOOP;

EXCEPTION
    WHEN OTHERS THEN
        RAISE;
END;
/

-- マスタから削除されたデータの削除
-- （証明書ユーザー区分=6:ヘルプデスクは対象外とする）
DELETE FROM PACTSCHOOL
WHERE  PACTSCHOOL.USERID IN (SELECT PACTSCHOOL.USERID
                             FROM   PACTSCHOOL
                             WHERE  (NOT EXISTS (SELECT HELPDESKID_TMP.USERID
                                                 FROM   HELPDESKID_TMP
                                                 WHERE  PACTSCHOOL.USERID=HELPDESKID_TMP.USERID))
                             AND    (CERT_USERDIV='6'));

-- テンポラリテーブル削除
DROP TABLE HELPDESKID_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

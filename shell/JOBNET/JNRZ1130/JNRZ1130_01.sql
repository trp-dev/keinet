-- ************************************************
-- *機 能 名 : 契約校マスタ(ヘルプデスク登録用)
-- *機 能 ID : JNRZ1130_01.sql
-- *作 成 日 : 2004/12/14
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE HELPDESKID_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE HELPDESKID_TMP (
    USERID                  CHAR(8)         NOT NULL,   /*  ユーザID                    */
    CERT_BEFOREDATA         VARCHAR2(8)             ,   /*  証明書有効期限              */
    REGISTRYDATE            CHAR(8)                 ,   /*  登録日                      */
    RENEWALDATE             CHAR(8)                 ,   /*  更新日                      */
    LOGINPWD                VARCHAR2(8)             ,   /*  ログインパスワード          */
    CONSTRAINT PK_HELPDESKID_TMP PRIMARY KEY(USERID)
    USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

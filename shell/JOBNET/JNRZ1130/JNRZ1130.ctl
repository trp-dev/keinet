------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : ヘルプデスクIDマスタ
--  テーブル名      : PACTSCHOOL(HELPDESKID_TMPからインポート)
--  データファイル名: HELPDESKIDyyyymmdd.csv
--  機能            : ヘルプデスクIDマスタをデータファイルの内容に置き換える
--  作成日          : 2004/12/14
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE HELPDESKID_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    USERID,
    CERT_BEFOREDATA,
    REGISTRYDATE,
    RENEWALDATE,
    LOGINPWD
)

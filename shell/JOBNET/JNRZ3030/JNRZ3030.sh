#!/bin/sh


#環境変数設定
export JOBNAME=JNRZ3030
export WORKDIR=/keinavi/JOBNET/$JOBNAME

#大学入試センター試験分析データ集PDF作成処理実行(Java)
cd /keinavi/bat/FreeMenu/F003
/keinavi/bat/FreeMenu/F003/F003.sh

\rm -f data/*
chown -R ora_ins_account /keinavi/bat/FreeMenu/F003/university/
chgrp -R ora_ins_group /keinavi/bat/FreeMenu/F003/university/

#Javaプログラム戻り値チェック(0以外は異常終了)
#if [ $? -ne 0 ]
#then
#    echo バッチ処理でエラーが発生しました
#    exit 30
#fi

#正常終了
echo バッチ処理が正常に終了しました
exit  0    #正常終了

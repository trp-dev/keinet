#! /bin/csh

##### センター・リサーチ大学別学力分布PDF 自動作成(ファイルサーバ転送&作成)バッチ
##### ユーザ orauser にて実行可能

set fpath = "/keinavi/HULFT/dscr_files"
set flgfile = "${fpath}/storeflg"
set JOBNETNAME = "JNRZ3030"
set outfilename = "/keinavi/JOBNET/returnresult"

if (-f $flgfile) then
	set storeyear = `cat $flgfile`
	set targetdir = "${fpath}/${storeyear}-tmp"
	if (! -d $targetdir) then
		echo " --- 登録対象年度フォルダが見当たりません。"
		echo " --- 登録処理を中断します。"
		exit
	endif

	echo "--- ${storeyear} 年度センター・リサーチ大学別学力分布PDF作成開始..." 
	echo " "

	cd $targetdir
	set FILELIST = `ls -rt DSCR*`
		
	echo "--- 各ファイル転送開始&登録開始"
	set count = 1
	while ($count <= $#FILELIST)
		#----対象ファイル名を取得
		set target =  $FILELIST[$count]
			
		echo "--- 登録対象ファイル：$target"
			
		#----対象ファイルをファイルサーバへFTP転送
		/keinavi/HULFT/ftp.sh /keinavi/bat/FreeMenu/F003/data $targetdir $target
		@ count ++
	end

	#----リモートシェルを実行
#	rsh satu04 /keinavi/bat/FreeMenu/F003/F003.sh
#	rsh satu04 /keinavi/JOBNET/JNRZ3030/JNRZ3030.sh 
	rsh satu04 -l root /keinavi/JOBNET/JobnetStart_ini.sh ${JOBNETNAME}

	if ($status != 0) then
		echo "-1" > $outfilename
	else
		rsh satu04 cat /keinavi/JOBNET/result_${JOBNETNAME} > $outfilename
	endif
	rsh satu04 \rm /keinavi/JOBNET/result_${JOBNETNAME}

	#----コマンド実行結果の識別
	set cord = `tail -1 $outfilename`
	rm -f $outfilename

	exit	
endif

echo " --- 登録フラグファイルが存在しません。"
echo " --- 登録処理は行われませんでした。"

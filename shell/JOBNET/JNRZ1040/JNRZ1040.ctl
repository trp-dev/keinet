------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 模試科目変換マスタ
--  テーブル名      : SUBJECTTRANS_TMP
--  データファイル名: SUBJECTTRANSyyyymmdd.csv
--  機能            : 模試科目変換マスタをデータファイルの内容に置き換える
--  作成日          : 2004/08/25
--  修正日          : 2004/10/05
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE SUBJECTTRANS_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    EXAMYEAR,
    EXAMCD	"DECODE(LENGTH(:EXAMCD), 1, CONCAT('0', :EXAMCD), 2, :EXAMCD)",
    OLDSUBCD,
    NEWSUBCD
)

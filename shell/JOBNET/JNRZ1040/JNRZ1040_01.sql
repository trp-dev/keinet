-- ************************************************
-- *機 能 名 : 模試科目変換マスタ
-- *機 能 ID : JNRZ1040_01.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE SUBJECTTRANS_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成

CREATE TABLE SUBJECTTRANS_TMP(   
        EXAMYEAR               CHAR(4)          NOT NULL,    /*    年度                 */ 
        EXAMCD             CHAR(2)          NOT NULL,    /*    模試コード     */ 
        OLDSUBCD           CHAR(4)          NOT NULL,    /*    変更前科目コード     */ 
        NEWSUBCD           CHAR(4)                  ,    /*    変更後科目コード     */ 
    CONSTRAINT PK_SUBJECTTRANS_TMP PRIMARY KEY(
        EXAMYEAR,                                            /*    年度                 */
        EXAMCD,           			     	/*    模試コード     */ 
        OLDSUBCD                                         /*    変更前科目コード     */
    )
    USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

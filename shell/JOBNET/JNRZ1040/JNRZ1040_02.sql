-- ************************************************
-- *機 能 名 : 模試科目変換マスタ
-- *機 能 ID : JNRZ1040_02.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM SUBJECTTRANS;

-- テンポラリテーブルからデータを挿入
INSERT INTO SUBJECTTRANS
SELECT * FROM SUBJECTTRANS_TMP;

-- テンポラリテーブル削除
DROP TABLE SUBJECTTRANS_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

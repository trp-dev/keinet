-- *********************************************************
-- *機 能 名 : 学校住所マスタ
-- *機 能 ID : JNRZ0x01_02.sql
-- *作 成 日 : 2011/04/18
-- *更 新 日 : 2011/04/18
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM MST_SCHOOLADDRESS;

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO MST_SCHOOLADDRESS
SELECT * FROM MST_SCHOOLADDRESS_TMP;

-- テンポラリテーブル削除
DROP TABLE MST_SCHOOLADDRESS_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

-- ************************************************
-- *機 能 名 : 学校住所マスタ
-- *機 能 ID : JNRZ0x01_01.sql
-- *作 成 日 : 2011/04/18
-- *作 成 者 : KCN
-- *更 新 日 : 2011/04/18
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE MST_SCHOOLADDRESS_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE MST_SCHOOLADDRESS_TMP  (
       SCHOOLCD                   CHAR(5)              NOT NULL,
       POSTCODE                   CHAR(8)              NOT NULL,
       ADDRESS                    VARCHAR2(1024)               ,
       CONSTRAINT PK_MST_SCHOOLADDRESS_TMP PRIMARY KEY (
                 SCHOOLCD
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

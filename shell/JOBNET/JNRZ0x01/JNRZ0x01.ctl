------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 学校住所マスタ
--  テーブル名      : MST_SCHOOLADDRESS
--  データファイル名: MST_SCHOOLADDRESS
--  機能            : 学校住所マスタをデータファイルの内容に置き換える
--  作成日          : 2011/04/18
--  修正日          : 2011/04/18
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE MST_SCHOOLADDRESS_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    SCHOOLCD    POSITION( 02: 06) CHAR,
    POSTCODE    POSITION(180:186) CHAR "SUBSTR(:POSTCODE, 1, 3) || '-' || SUBSTR(:POSTCODE, 4, 4)",
    ADDRESS     POSITION( 98:161) CHAR "REPLACE(NVL(SUBSTR(:ADDRESS, 1, 16), ''), '　','') || ' ' || REPLACE(NVL(SUBSTR(:ADDRESS, 17, 16), ''), '　','')"
)

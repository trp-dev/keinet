------------------------------------------------------------------------------
-- *機 能 名 : CEFR取得状況（全国）のCTLファイル
-- *機 能 ID : JNRZ1200.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE CEFRACQUISITIONSTATUS_A_TMP
(  
   EXAMYEAR    POSITION(01:04)  CHAR 
 , EXAMCD      POSITION(05:06)  CHAR 
 , CEFRLEVELCD POSITION(07:08)  CHAR 
 , NUMBERS     POSITION(09:15)  INTEGER EXTERNAL 
 , COMPRATIO   POSITION(16:19)  INTEGER EXTERNAL "DECODE(:COMPRATIO,   '9999', null, :COMPRATIO/10)"
)

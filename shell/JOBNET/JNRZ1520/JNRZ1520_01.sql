-- ************************************************
-- *機 能 名 : Ｋ－Ｗｅｂ小問マスタ
-- *機 能 ID : JNRZ1520_01.sql
-- *作 成 日 : 2019/09/24
-- *作 成 者 : QQ)瀬尾
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE KWEBSHOQUESTION_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE KWEBSHOQUESTION_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM KWEBSHOQUESTION WHERE 0=1;

-- 主キーの設定
ALTER TABLE KWEBSHOQUESTION_TMP ADD CONSTRAINT PK_KWEBSHOQUESTION_TMP PRIMARY KEY 
(
   EXAMYEAR
 , EXAMCD
 , SUBCD
 , KWEB_SHOQUESTION_DEPT_NO
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

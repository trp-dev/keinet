#! /bin/csh 

#### 大学マスタ関連ファイルを検証機へ送信するバッチ
#### （対象は下記変数定義パス 配下の各マスタの最新ファイル）

####　大学マスタのファイル先のパスを指定する (設定項目) #######
set targetplace = "/keinavi/HULFT/NewEXAM"


set place = "/keinavi/JOBNET/JN_UnivDataUpdate"
cd $place

#set TargetFiles = (HS3_UNIV_EXAM HS3_UNIV_NAME HS3_UNIV_PUB IRREGULARFILE SCHEDULEDETAIL UNIVCDTRANS_ORD UNIVSTEMMA)
set TargetFiles = (HS3_UNIV_NAME HS3_UNIV_EXAM HS3_UNIV_PUB IRREGULARFILE SCHEDULEDETAIL UNIVSTEMMA)
set TargetFiD = (RZRM5110 RZRM5120 RZRM5130 RZRM5140 RZRM5180 RZRM5170)

set count = 1
set fcheck = 0
 
while ($count <= $#TargetFiles)

	(ls -t $targetplace/$TargetFiles[$count]* > out.log) >& /dev/null 
	set FileList=`cat out.log` 
	if ($#FileList == 0) then

		(ls -t $targetplace/Univmaster > out.log) >& /dev/null
		set dirList=`cat out.log`
		if ($#dirList == 0) then

	        	echo "***** $TargetFiles[$count] $TargetFiD[$count] データファイルが見つかりません *****"
      			@ count ++
			@ fcheck ++
			continue
		endif

		(ls -t $targetplace/Univmaster/$dirList[1]/$TargetFiles[$count]* > out.log) >& /dev/null
		set FileList=`cat out.log`
		if ($#FileList == 0) then
	        	echo "***** $TargetFiles[$count] $TargetFiD[$count] データファイルが見つかりません *****"
      			@ count ++
			@ fcheck ++
			continue
		endif
			
	endif
	set StoreFullFile = $FileList[1]
	cp -p $StoreFullFile $TargetFiles[$count]
	set StoreFile = $TargetFiles[$count]
	echo "---- 対象ファイル：$StoreFullFile を $TargetFiles[$count] として送信"
	if (($TargetFiles[$count] == HS3_UNIV_EXAM)||($TargetFiles[$count] == HS3_UNIV_PUB)) then
		set string1=`head -1 $StoreFile | awk '{printf "%-.14s\n",$0}'`
		set year=`echo $string1 | awk '{printf "%-.4s\n",$0}'`
		set exam=`expr $string1 : ".*\(..\)"`

		echo "対象年度・模試区分：$year$exam"
	else if ($TargetFiles[$count] == HS3_UNIV_NAME) then
		set string1=`head -1 $StoreFile | awk '{printf "%-.6s\n",$0}'`
		set year=`echo $string1 | awk '{printf "%-.4s\n",$0}'`
		set exam=`expr $string1 : ".*\(..\)"`
		
		echo "対象年度・模試区分：$string1"

		#--- データ登録時に紐付き処理をしないようにファイル名変更
		mv $TargetFiles[$count] $TargetFiles[$count]-lastyear00
		set StoreFile = $TargetFiles[$count]-lastyear00

	else if ($TargetFiles[$count] == IRREGULARFILE) then
		set string1=`head -1 $StoreFile | awk '{printf "%-.5s\n",$0}'`
		set year=`echo $string1 | awk '{printf "%-.4s\n",$0}'`
		set examtmp=`expr $string1 : ".*\(.\)"`
		set exam="0${examtmp}"
		echo "対象年度・模試区分：$year$exam"
	endif

	#-- orauser にてファイルを転送する
	/keinavi/HULFT/ftp.sh /keinavi/HULFT/WorkDir/ $place $StoreFile 
	\rm $StoreFile

	@ count ++
end

\rm out.log

if ($fcheck != 0) then
	exit 30
endif

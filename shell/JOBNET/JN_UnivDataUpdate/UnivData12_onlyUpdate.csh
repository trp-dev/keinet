#! /bin/csh 

#### 大学マスタ関連ファイルを検証機へ送信するバッチ
#### （対象は下記変数定義パス 配下の各マスタの最新ファイル）
#### 高12マスタのみを転送

####　大学マスタのファイル先のパスを指定する (設定項目) #######
set targetplace = "/keinavi/HULFT/NewEXAM"
#set targetplace = "/keinavi/HULFT/NewEXAM/Univmaster"


set place = "/keinavi/JOBNET/JN_UnivDataUpdate"
cd $place

set TargetFiles = (HS12_UNIV)
set TargetFiD = (RZRM5210)

set count = 1
set fcheck = 0
if ($1 == release) then
	if (-f /keinavi/HULFT/WorkDir/HS12_UNIV-20070914) then
		echo "ファイルを　/keinavi/HULFT/WorkDirに配置します "
		mv /keinavi/HULFT/WorkDir/HS12_UNIV-20070914 /keinavi/HULFT/WorkDir/HS12_UNIV
	endif
	exit
endif



while ($count <= $#TargetFiles)

	(ls -t $targetplace/$TargetFiles[$count]* > out.log) >& /dev/null 
	set FileList=`cat out.log` 
	echo "---- ファイルの有無をチェックします。 ---- "
	if ($#FileList == 0) then

		(ls -t $targetplace > out.log) >& /dev/null
		set dirList=`cat out.log`
		if ($#dirList == 0) then

	        	echo "***** $TargetFiles[$count] 【$TargetFiD[$count]】 データファイルが見つかりません *****"
      			@ count ++
			@ fcheck ++
			continue
		endif

		(ls -t $targetplace/Univmaster/$dirList[1]/$TargetFiles[$count]* > out.log) >& /dev/null
		set FileList=`cat out.log`
		if ($#FileList == 0) then
			echo "***** $TargetFiles[$count] $TargetFiD[$count] データファイルが見つかりません *****"
			@ count ++
			@ fcheck ++
			continue
		endif

	endif
	set StoreFullFile = $FileList[1]
	cp -p $StoreFullFile $TargetFiles[$count]
	set StoreFile = $TargetFiles[$count]
	echo "---- 対象ファイル：$StoreFullFile を $TargetFiles[$count] として送信"
	if ($TargetFiles[$count] == HS12_UNIV) then
		set string1=`head -1 $StoreFile | awk '{printf "%-.6s\n",$0}'`
		set year=`echo $string1 | awk '{printf "%-.4s\n",$0}'`
		set exam=`expr $string1 : ".*\(..\)"`	

		echo "対象年度・模試区分：$string1"
	endif

	#-- orauser にてファイルを転送する
	/keinavi/HULFT/ftp.sh /keinavi/HULFT/WorkDir/ $place $StoreFile 
	\rm $StoreFile

	cp -p $StoreFullFile /keinavi/HULFT/WorkDir/$TargetFiles[$count]-20070914
	mv $StoreFullFile /keinavi/HULFT/DATA/200961_71/$TargetFiles[$count]-20070914

	@ count ++
end

\rm out.log

if ($fcheck != 0) then
	exit 30
endif


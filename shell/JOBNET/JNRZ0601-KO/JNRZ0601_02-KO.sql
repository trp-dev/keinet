-- *********************************************************
-- *機 能 名 : 大学コード変換順引きマスタ
-- *機 能 ID : JNRZ0601_02-KO.sql
-- *作 成 日 : 2007/02/22
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM KO_UNIVCDTRANS_ORD
WHERE  (YEAR,EXAMCD)
       IN(SELECT DISTINCT YEAR,EXAMCD
          FROM   KO_UNIVCDTRANS_ORD_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO KO_UNIVCDTRANS_ORD
SELECT * FROM KO_UNIVCDTRANS_ORD_TMP;

-- 8年以上前のデータを削除
DELETE FROM KO_UNIVCDTRANS_ORD
WHERE  TO_NUMBER(YEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 8);

-- 更新対象データの7年前のデータを削除
DELETE FROM KO_UNIVCDTRANS_ORD
WHERE  (YEAR,EXAMCD)
       IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 7)),EXAMCD
          FROM   KO_UNIVCDTRANS_ORD_TMP);

-- SQL*PLUS終了
EXIT;

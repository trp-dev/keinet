-- ************************************************
-- *機 能 名 : 大学マスタ基本情報
-- *機 能 ID : JNRZ0551_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 : 2009/12/15
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE UNIVMASTER_BASIC_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE UNIVMASTER_BASIC_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM UNIVMASTER_BASIC WHERE 0=1;

-- 主キーの設定
ALTER TABLE UNIVMASTER_BASIC_TMP ADD CONSTRAINT PK_UNIVMASTER_BASIC_TMP PRIMARY KEY 
(
   EVENTYEAR
 , EXAMDIV
 , UNIVCD
 , FACULTYCD
 , DEPTCD
) USING INDEX TABLESPACE KEINAVI_INDX;

-- SQL*PLUS終了
EXIT;

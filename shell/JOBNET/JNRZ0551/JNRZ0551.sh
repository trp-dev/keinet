#!/bin/sh

#環境変数設定
export FILEDIR=/keinavi/HULFT/WorkDir
export SRCDIR=/keinavi/HULFT/WorkDir
export CSVFILE=UNIVMASTER_BASIC

#昨年の00区分大学マスタ登録
if [ -f ${FILEDIR}/UNIVMASTER_BASIC-lastyear00 ]
then
	if [ -f ${FILEDIR}/UNIVMASTER_BASIC ]
	then
		mv ${FILEDIR}/UNIVMASTER_BASIC ${FILEDIR}/UNIVMASTER_BASIC-tmprename
	fi
	mv ${FILEDIR}/UNIVMASTER_BASIC-lastyear00 ${FILEDIR}/UNIVMASTER_BASIC


	/keinavi/JOBNET/JNRZ0551/JNRZ0551-lastyear00.sh
	if [ $? -ne 0 ]
	then
		exit 30
	fi
	if [ -f ${FILEDIR}/UNIVMASTER_BASIC-tmprename ]
	then
		mv ${FILEDIR}/UNIVMASTER_BASIC-tmprename ${FILEDIR}/UNIVMASTER_BASIC
	fi
fi

#大学マスタファイルの有無確認
/keinavi/JOBNET/common/JNRZFCHECK.sh
if [ $? -eq 0 ]
then
        fflg=0
	#03,09区分の大学マスタ登録
	if [ -f ${FILEDIR}/UNIVMASTER_BASIC-lastdiv ]
	then
		mv ${FILEDIR}/UNIVMASTER_BASIC ${FILEDIR}/UNIVMASTER_BASIC-tmprename
		mv ${FILEDIR}/UNIVMASTER_BASIC-lastdiv ${FILEDIR}/UNIVMASTER_BASIC

		/keinavi/JOBNET/JNRZ0551/JNRZ0551-1.sh
		if [ $? -ne 0 ]
		then
			exit 30
		fi

		mv ${FILEDIR}/UNIVMASTER_BASIC-tmprename ${FILEDIR}/UNIVMASTER_BASIC
	fi

	/keinavi/JOBNET/JNRZ0551/JNRZ0551-1.sh
	if [ $? -ne 0 ]
	then
		exit 30
	fi
else
	echo 処理対象ファイルなし（正常終了）
	fflg=1
fi
	

#校内成績処理システムの就職大学マスタ登録
if [ -f ${FILEDIR}/KO_UNIVMASTER_BASIC ]
then
	/keinavi/JOBNET/JNRZ0551-KO/JNRZ0551-KO.sh
fi

if [ ${fflg} -eq 1 ]
then
	exit 1
fi
exit  0    #正常終了

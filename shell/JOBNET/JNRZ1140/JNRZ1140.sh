#!/bin/sh
/keinavi/JOBNET/JNRZ1140/JNRZ1140.cgi

#export DISPLAY=127.0.0.1:0.0
export ORACLE_BASE=/opt/oracle
export ORACLE_HOME=$ORACLE_BASE/product/9.2.0
export NLS_LANG=Japanese_Japan.JA16EUC
export ORA_NLS33=$ORACLE_HOME/ocommon/nls/admin/data
export ORACLE_SID=S11DBA_BATCH2
export ORACLE_DOC=$ORACLE_HOME/doc
export PATH=$PATH:$ORACLE_HOME/bin
export LD_LIBRARY_PATH=$ORACLE_HOME/lib

##SQL*PLUSによるパスワード更新処理(Kei-Netサービス)
#sqlplus system/manager@knet @/keinavi/JOBNET/JNRZ1140/.haiten2
#rm /keinavi/JOBNET/JNRZ1140/.haiten2

##DBのエクスポート(Kei-Netサービス)
#exp system/manager@knet FULL=Y FILE=/keinavi/pwchg/test.txt
##chmod 600 /root/expdat.dmp
##cp -p /root/expdat.dmp /root/test.txt
##rm -f /root/expdat.dmp

#SQL*PLUSによるパスワード更新処理(Kei-Naviサービス)
/keinavi/JOBNET/common/JNRZSQLEXE.sh /keinavi/JOBNET/JNRZ1140/.haiten4
rm /keinavi/JOBNET/JNRZ1140/.haiten4

#正常終了
echo データの更新処理が正常に終了しました
exit  0    #正常終了

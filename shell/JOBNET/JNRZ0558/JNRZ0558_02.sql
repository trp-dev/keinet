-- *********************************************************
-- *機 能 名 : 入試日程詳細
-- *機 能 ID : JNRZ0558_02.sql
-- *作 成 日 : 2004/08/31
-- *更 新 日 : 2009/12/17
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM EXAMSCHEDULEDETAIL
WHERE  (YEAR,EXAMDIV)
       IN(SELECT DISTINCT YEAR,EXAMDIV
          FROM   EXAMSCHEDULEDETAIL_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO EXAMSCHEDULEDETAIL
SELECT * FROM EXAMSCHEDULEDETAIL_TMP;

-- 2年以上前のデータを削除
DELETE FROM EXAMSCHEDULEDETAIL
WHERE  TO_NUMBER(YEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 2);

-- テンポラリテーブル削除
DROP TABLE EXAMSCHEDULEDETAIL_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

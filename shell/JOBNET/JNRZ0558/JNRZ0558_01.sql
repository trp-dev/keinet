-- ************************************************
-- *機 能 名 : 入試日程詳細
-- *機 能 ID : JNRZ0558_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 : 2009/12/17
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE EXAMSCHEDULEDETAIL_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE EXAMSCHEDULEDETAIL_TMP  (
       YEAR              CHAR(4)              NOT NULL,    /*   年度              */ 
       UNIVCD            CHAR(4)              NOT NULL,    /*   大学コード        */ 
       FACULTYCD         CHAR(2)              NOT NULL,    /*   学部コード        */ 
       DEPTCD            CHAR(4)              NOT NULL,    /*   学科コード        */ 
       EXAMDIV           CHAR(2)              NOT NULL,    /*   模試区分          */ 
       ENTEXAMDIV_1_2TERM   CHAR(2)           NOT NULL,    /*   入試１期２期区分  */ 
       ENTEXAMDIV_1_2ORDER  CHAR(1)           NOT NULL,    /*   入試１次２次区分  */ 
       SCHOOLPROVENTDIV     CHAR(1)           NOT NULL,    /*   入試本学地方区分  */ 
       SCHEDULEBRANCHNAME   VARCHAR2(6)               ,    /*   日程枝番名        */ 
       ENTEXAMAPPLIDEADLINE    CHAR(8)                ,    /*   入試出願締切日    */ 
       ENTEXAMAPPLIDEADLINEDIV CHAR(1)                ,    /*   入試出願締切区分  */ 
       ENTEXAMINPLEDATE1_1 CHAR(8)                    ,    /*   入試実施日１－１  */ 
       ENTEXAMINPLEDATE1_2 CHAR(8)                    ,    /*   入試実施日１－２  */ 
       ENTEXAMINPLEDATE1_3 CHAR(8)                    ,    /*   入試実施日１－３  */ 
       ENTEXAMINPLEDATE1_4 CHAR(8)                    ,    /*   入試実施日１－４  */ 
       ENTEXAMINPLEDATE2_1 CHAR(8)                    ,    /*   入試実施日２－１  */ 
       ENTEXAMINPLEDATE2_2 CHAR(8)                    ,    /*   入試実施日２－２  */ 
       ENTEXAMINPLEDATE2_3 CHAR(8)                    ,    /*   入試実施日２－３  */ 
       ENTEXAMINPLEDATE2_4 CHAR(8)                    ,    /*   入試実施日２－４  */ 
       PLEDATEDIV1_1       CHAR(1)                    ,    /*   入試実施日区分１－１ */ 
       PLEDATEDIV1_2       CHAR(1)                    ,    /*   入試実施日区分１－２ */ 
       PLEDATEDIV1_3       CHAR(1)                    ,    /*   入試実施日区分１－３ */ 
       PLEDATEUNITEDIV     CHAR(1)                    ,    /*   入試日結合区分       */ 
       PLEDATEDIV2_1       CHAR(1)                    ,    /*   入試実施日区分２－１ */ 
       PLEDATEDIV2_2       CHAR(1)                    ,    /*   入試実施日区分２－２ */ 
       PLEDATEDIV2_3       CHAR(1)                    ,    /*   入試実施日区分２－３ */ 
       SUCANNDATE          CHAR(8)                    ,    /*   入試合格発表日       */ 
       PROCEDEADLINE       CHAR(8)                    ,    /*   入試手続締切日       */ 
       CONSTRAINT PK_EXAMSCHEDULEDETAIL_TMP PRIMARY KEY (
                 YEAR                      ,               /*  年度        */
                 UNIVCD                    ,               /*  大学コード  */
                 FACULTYCD                 ,               /*  学部コード  */
                 DEPTCD                    ,               /*  学科コード  */
                 EXAMDIV                   ,               /*  模試区分    */
                 ENTEXAMDIV_1_2TERM        ,               /*  入試１期２期区分  */
                 ENTEXAMDIV_1_2ORDER       ,               /*  入試１次２次区分  */
                 SCHOOLPROVENTDIV                          /*  入試本学地方区分  */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

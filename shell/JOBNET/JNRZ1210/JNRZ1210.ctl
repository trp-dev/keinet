------------------------------------------------------------------------------
-- *機 能 名 : CEFR取得状況（県）のCTLファイル
-- *機 能 ID : JNRZ1210.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE CEFRACQUISITIONSTATUS_P_TMP
(  
   EXAMYEAR    POSITION(01:04)  CHAR 
 , EXAMCD      POSITION(05:06)  CHAR 
 , PREFCD      POSITION(07:08)  CHAR 
 , CEFRLEVELCD POSITION(09:10)  CHAR 
 , NUMBERS     POSITION(11:17)  INTEGER EXTERNAL 
 , COMPRATIO   POSITION(18:21)  INTEGER EXTERNAL "DECODE(:COMPRATIO,   '9999', null, :COMPRATIO/10)"
)

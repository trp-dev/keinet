#! /bin/csh 

if (($#argv < 1 ) || (($1 != -1)&&($1 != 0)&&($1 != 1)) || (($1 == 1)&&($#argv < 3)))then
	echo " ---- param error ----"
	echo " 第1パラメータ:grep校取得先識別フラグ -1,0:ファイル、1:スクリプト内定義(必須)"
	echo " 第2パラメータ:grepフォルダ  (第1パラが0の時は不要)" 
	echo " 第3パラメータ:grep年度+模試コード (第1パラが0の時は不要)"
	echo " 第4パラメータ:grepファイル名 (すべて対象時は不要)"
	exit
endif

set getflg = $1
set targetdir = /u02/HULFT-backup
set filepath = /u02/HULFT-backup
#set targetdir = /keinavi/HULFT/DATA
#set filepath = /keinavi/HULFT/DATA
set examconffpath = "/home/navikdc/datastore/inside_release"

set list = ()
set count = 1
set allflg = 0         #全国、県データの対象有無フラグ

#grep対象模試定義ファイルの読み込み（検証校定義がファイル時）
if (($getflg == 0)||($getflg == -1)) then
	(ls ${examconffpath}/grep* > tmp.log) >& /dev/null
	set list = `cat tmp.log`
	\rm tmp.log
	if ($#list == 0) then
		echo "---- Config File ERROR ----"
		echo "  検証校定義ファイルが見つかりません "
		exit 30
	endif
endif

cd $filepath
	
#grep対象校取得（検証校定義がスクリプト内時）
if ($getflg == 1) then
	set indir = $2
	set year = $3
	set list = (1)
        set year2 = `echo $year | awk '{printf "%-.4s\n",$0}'`
        set exam2 = `expr $year : ".*\(..\)"`

	##########---- grepの対象校 (必要に応じて高校コードのみ変更) ###########
	##########---- ※第1パラメータが[1]の値のみ有効              ###########
if (($year == "200701")||($year == "200601")||($year == "200501")||($year == "200401")||($year == "200301")||($year == "200201")||($year == "200101")||($year == "200001")) then
	set schoolcdlist = (10127 11543 13693 12547 26539 23156 14124 27565 13613 12130 14138 14562 14143 12545 01219 03160 08503 20147 13610 11526)
else if (($year == "200702")||($year == "200602")||($year == "200502")||($year == "200402")||($year == "200302")||($year == "200202")||($year == "200102")||($year == "200002")) then
	set schoolcdlist = (09118 03148 23103 31102 23107 12513 23104 13552 23142 19130 03152 18101 19120 20159 04806 07114 24801 27544 11524 08510)
else if (($year == "200703")||($year == "200603")||($year == "200503")||($year == "200403")||($year == "200303")||($year == "200203")||($year == "200103")||($year == "200003")) then
	set schoolcdlist = (23231 07122 27537 21515 13746 24118 13674 24812 23552 14552 11515 14542 15209 11545 13612 05102 23166 27592 26131 20155)
else if (($year == "200704")||($year == "200604")||($year == "200504")||($year == "200404")||($year == "200304")||($year == "200204")||($year == "200104")||($year == "200004")) then
	set schoolcdlist = (19126 24803 23819 20109 14572 11109 07151 23502 11537 20127 41104 27003 07503 26123 30112 23832 34161 22530 24510 04503)
else if (($year == "200705")||($year == "200605")||($year == "200505")||($year == "200405")||($year == "200305")||($year == "200205")||($year == "200105")||($year == "200005")) then
	set schoolcdlist = (14573 40212 13711 23838 14530 23133 01507 05116 03136 34508 23226 12521 11535 10509 23230 23517 27536 14546 23128 13698)
else if (($year == "200706")||($year == "200606")||($year == "200506")||($year == "200406")||($year == "200306")||($year == "200206")||($year == "200106")||($year == "200006")) then
	set schoolcdlist = (27132 06101 23108 23110 27560 22536 24144 11808 12174 11107 41101 20146 23127 38124 23274 13601 04516 21142 23801 19106)
else if (($year == "200707")||($year == "200607")||($year == "200507")||($year == "200407")||($year == "200307")||($year == "200207")||($year == "200107")||($year == "200007")) then
	set schoolcdlist = (23232 23264 24120 23250 37129 13213 25508 19107 22535 13696 09506 11126 27806 05128 24126 13122 27105 11102 28544 23233)
else if (($year == "200709")||($year == "200609")||($year == "200509")||($year == "200409")||($year == "200309")||($year == "200209")||($year == "200109")||($year == "200009")) then
	set schoolcdlist = (13563 11542 13320 10130 23531 11109 12539 11521 11506 27584 14554 07172 11203 11520 07152 13713 12556 13622 40146 14576)
else if (($year == "200710")||($year == "200610")||($year == "200510")||($year == "200410")||($year == "200310")||($year == "200210")||($year == "200110")||($year == "200010")) then
	set schoolcdlist = (13563 11542 13320 10130 23531 11109 12539 11521 11506 27584 14554 07172 11203 11520 07152 13713 12556 13622 40146 14576)
else if (($year == "200761")||($year == "200661")||($year == "200561")||($year == "200461")||($year == "200361")||($year == "200261")||($year == "200161")||($year == "200061")) then
	set schoolcdlist = (38125 26001 09156 11207 13562 37510 34517 13539 08517 13626 24513 24507 12522 11162 24502 13731 14578 13712 12111 13686)
else if (($year == "200762")||($year == "200662")||($year == "200562")||($year == "200462")||($year == "200362")||($year == "200262")||($year == "200162")||($year == "200062")) then
	set schoolcdlist = (09128 12193 09122 12502 11104 28519 08196 13618 23528 10512 01555 14553 15103 13682 40217 13607 36140 03120 08155 13168)
else if (($year == "200763")||($year == "200663")||($year == "200563")||($year == "200463")||($year == "200363")||($year == "200263")||($year == "200163")||($year == "200063")) then
	set schoolcdlist = (12230 26106 13112 24805 28224 10132 13645 27223 11506 14551 19125 13622 27174 27551 26103 12190 26152 28182 19801 27158)
else if (($year == "200771")||($year == "200671")||($year == "200571")||($year == "200471")||($year == "200371")||($year == "200271")||($year == "200171")||($year == "200071")) then
	set schoolcdlist = (13332 30134 14535 13181 13248 26109 26153 27593 47501 28537 34804 27580 33514 34102 34167 04110 10508 26138 26149 30507)
else if (($year == "200772")||($year == "200672")||($year == "200572")||($year == "200472")||($year == "200372")||($year == "200272")||($year == "200172")||($year == "200072")) then
	set schoolcdlist = (34122 34154 34159 45503 02104 05101 05123 05132 08201 10123 10136 10504 15514 21801 22188 29136 29801 34131 34173 34174)
else if (($year == "200773")||($year == "200673")||($year == "200573")||($year == "200473")||($year == "200373")||($year == "200273")||($year == "200173")||($year == "200073")) then
	set schoolcdlist = (26156 27810 29150 14566 13524 14103 13719 13817 29502 13581 13723 23239 11117 27501 13717 13665 13501 11176 13239 11255)
else if (($year == "200765")||($year == "200665")||($year == "200565")||($year == "200465")||($year == "200365")||($year == "200265")||($year == "200165")||($year == "200065")) then
	set schoolcdlist = (13738 15101 29103 12548 13547 13207 13157 10101 08119 13149 27509 08191 20113 24133 20175 20516 23271 13266 45117 14157)
else if (($year == "200667")||($year == "200567")||($year == "200467")||($year == "200367")||($year == "200267")||($year == "200167")||($year == "200067")) then
	set schoolcdlist = (13738 15101 29103 12548 13547 13207 13157 10101 08119 13149 27509 08191 20113 24133 20175 20516 23271 13266 45117 14157)
else if (($year == "200766")||($year == "200666")||($year == "200566")||($year == "200466")||($year == "200366")||($year == "200266")||($year == "200166")||($year == "200066")) then
	set schoolcdlist = (08141 15135 27166 02516 23161 28520 13165 08211 17149 40801 21135 11111 13737 21180 23225 11216 11128 23263 23209 26814)
else if (($year == "200775")||($year == "200675")||($year == "200575")||($year == "200475")||($year == "200375")||($year == "200275")||($year == "200175")||($year == "200075")) then
	set schoolcdlist = (14524 11537 27560 14531 11530 13693 11543 14553 13664 12506 08515 15103 12515 12524 13671 13697 11104 13814 11207 13682)
else if (($year == "200774")||($year == "200674")||($year == "200574")||($year == "200474")||($year == "200374")||($year == "200274")||($year == "200174")||($year == "200074")) then
	set schoolcdlist = (14524 11537 27560 14531 11530 13693 11543 14553 13664 12506 08515 15103 12515 12524 13671 13697 11104 13814 11207 13682)
endif
	set allflg = 1
endif

#-- 出力ファイルの初期化
#if (-f $filepath/grepexamstr) then
#	\rm $filepath/grepexamstr
#	\rm -rf $filepath/temp/2*
#endif

while($count <= $#list)

	if (($getflg == 0)||($getflg == -1)) then
		#-- 抽出対象模試ファイルをリストから取得
		set targetfile = $list[$count]
	        #-- フルファイルパスからファイル名文字列を取得
		set failname = `echo $targetfile | sed -e 's/.*\///g'`
		#-- ファイルパスから対象模試＋模試コード文字列を取得
		set year = `expr $targetfile : ".*\(......\)"`
		set orgyear = `expr $targetfile : ".*\(......\)"`
                set year2 = `echo $year | awk '{printf "%-.4s\n",$0}'`
                set exam2 = `expr $year : ".*\(..\)"`
		#-- 抽出対象フォルダを模試ごとに識別
		if (($exam2 == 61)||($exam2 == 71)) then
			set indir = ${year2}61_71
		else if (($exam2 == 62)||($exam2 == 72)) then
			set indir = ${year2}62_72
		else if (($exam2 == 63)||($exam2 == 73)) then
			set indir = ${year2}63_73
		else if (($exam2 == 28)||($exam2 == 68)||($exam2 == 78)) then
			set indir = ${year2}28_68_78
		else
			set indir = $year
		endif
		#-- 抽出用対象データフォルダの確認
		if (! -d $indir) then
			echo "--- ERROR： Grep対象フォルダ $indir がみつかりません ---"
			@ count ++
			continue
		endif
		#-- ファイルよりgrep対象校を取得
		set schoolcdlist = `cat $targetfile`

		if ($getflg == -1) then
			set allflg = 1
		endif
	endif

	if ($#schoolcdlist == 0) then
		echo " 模試 ${year} のgrep対象校 全件"
	else
		echo " 模試 ${year} のgrep対象校 $schoolcdlist"
	endif

	#-- 抽出用の文字列作成
	set count2 = 1
	while($count2 <= $#schoolcdlist)
		set strtem = $schoolcdlist[$count2]
		#--- 変数内に改行があった場合は削除
		set str = `echo ${strtem} | tr -d '\r\n'`

		if ($str != "") then
			if($count2 == 1)then
				set gstring = "-e ^${year}$str"
				set gstring2 = "-e ^${year}...........$str"
			else
				set gstring = "${gstring} -e ^${year}$str"
				set gstring2 = "${gstring2} -e ^${year}...........$str"
			endif
		endif
		@ count2 ++
	end
	set orgyear = $year
	set orginyear = $indir

	#-- ベース出力用フォルダ
	set dir = "temp/${indir}selection"

	#-- 事後換算・得点修正・都立換算データの有無確認
# 1019変更	(ls -r ${indir}/after > tmp.log) >& /dev/null
	(ls -tr ${indir}/after > tmp.log) >& /dev/null
        set subdir = `cat tmp.log`
        \rm tmp.log
        if ($#subdir != 0) then
		set masterindir = "${indir}/after"
        endif

	set examtystr = ""
	set count5 = 0
	while ($count5 <= $#subdir) 
	
		#-- 事後換算・得点修正・都立換算データ用に出力先変更	
		if ($count5 > 0) then
			set targety = $subdir[$count5]
			set dir = temp/${subdir[$count5]}selection
			set indir = ${masterindir}/$subdir[$count5]
			set allflg = 0

			#-- データ種別文字列取得
			set examtystr = `echo $targety | sed -e s'/[0-9]_*//'g`
#			set examtystr = `echo $targety | sed -e s'/^[0-9_]\{,12\}//'g`

			set year = ${orgyear}${examtystr}
		endif
	
		#-- 抽出結果出力用フォルダの確認
		if (! -d $dir) then
			mkdir -p $dir
			chmod 777 $dir
			if ($status != 0) then
				echo "---- ERROR： フォルダ作成処理にエラーが発生したため処理を中断します。----"
				exit
			endif
		endif
#set indir = ${targetdir}/$indir
		echo "抽出対象 $indir  出力先 $dir　データ種別 $examtystr"

		#-- 抽出ファイルの個別指定
#		if ($4 != "") then
#			if (($4 == "INDIVIDUALRECORD")||($4 == "INDIVIDUALRECORD-old")) then
#				echo "grep $gstring2 start"
#				grep $gstring2 $indir/$4 >> $dir/INDIVIDUALRECORD
#			else
#				echo "grep $gstring start"
#				grep $gstring $indir/$4 > $dir/$4
#			endif
#
#			echo "grep END"
#			exit
#		endif

		if ($allflg == 1) then
			if ((-f $indir/EXAMTAKENUM_A)&&(! -f $dir/EXAMTAKENUM_A)) then
				cp -p $indir/EXAMTAKENUM_A $dir/EXAMTAKENUM_A
			else if (! -f $indir/EXAMTAKENUM_A) then
				echo "-- 模試受験者総数(全国)[EXAMTAKENUM_A]【RZRM1180】が存在しません --" 
			endif
			if ((-f $indir/EXAMTAKENUM_P)&&(! -f $dir/EXAMTAKENUM_P)) then
				cp -p $indir/EXAMTAKENUM_P $dir/EXAMTAKENUM_P
			else if (! -f $indir/EXAMTAKENUM_P) then
				echo "-- 模試受験者総数(県)[EXAMTAKENUM_P]【RZRM1280】が存在しません --" 
			endif
			if ((-f $indir/SUBRECORD_A)&&(! -f $dir/SUBRECORD_A)) then
				cp -p $indir/SUBRECORD_A $dir/SUBRECORD_A
			else if (! -f $indir/SUBRECORD_A) then
				echo "-- 科目別成績(全国)[SUBRECORD_A]【RZRM1110】が存在しません --" 
			endif
			if ((-f $indir/SUBRECORD_P)&&(! -f $dir/SUBRECORD_P)) then
				cp -p $indir/SUBRECORD_P $dir/SUBRECORD_P
			else if (! -f $indir/SUBRECORD_P) then
				echo "-- 科目別成績(県)[SUBRECORD_P]【RZRM1210】が存在しません --" 
			endif
			if ((-f $indir/SUBDISTRECORD_A)&&(! -f $dir/SUBDISTRECORD_A)) then
				cp -p $indir/SUBDISTRECORD_A $dir/SUBDISTRECORD_A
			else if (! -f $indir/SUBDISTRECORD_A) then
				echo "-- 科目分布別成績(全国)[SUBDISTRECORD_A]【RZRM1120】が存在しません --" 
			endif
			if ((-f $indir/SUBDISTRECORD_P)&&(! -f $dir/SUBDISTRECORD_P)) then
				cp -p $indir/SUBDISTRECORD_P $dir/SUBDISTRECORD_P
			else if (! -f $indir/SUBDISTRECORD_P) then
				echo "-- 科目分布別成績(県)[SUBDISTRECORD_P]【RZRM1220】が存在しません --" 
			endif
			if ((-f $indir/QRECORD_A)&&(! -f $dir/QRECORD_A)) then
				cp -p $indir/QRECORD_A $dir/QRECORD_A
			else if (! -f $indir/QRECORD_A) then
				echo "-- 設問別成績(全国)[QRECORD_A]【RZRM1130】が存在しません --" 
			endif
			if ((-f $indir/QRECORD_P)&&(! -f $dir/QRECORD_P)) then
				cp -p $indir/QRECORD_P $dir/QRECORD_P
			else if (! -f $indir/QRECORD_P) then
				echo "-- 設問別成績(県)[QRECORD_P]【RZRM1230】が存在しません --" 
			endif
			if ((-f $indir/RATINGNUMBER_A)&&(! -f $dir/RATINGNUMBER_A)) then
				cp -p $indir/RATINGNUMBER_A $dir/RATINGNUMBER_A
			else if (! -f $indir/RATINGNUMBER_A) then
				if (($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 74)) then
					echo "-- 志望大学評価別人数(全国)[RATINGNUMBER_A]【RZRM1150】が存在しません --" 
				endif
			endif
			if ((-f $indir/RATINGNUMBER_P)&&(! -f $dir/RATINGNUMBER_P)) then
				cp -p $indir/RATINGNUMBER_P $dir/RATINGNUMBER_P
			else if (! -f $indir/RATINGNUMBER_P) then
				if (($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 74)) then
					echo "-- 志望大学評価別人数(県)[RATINGNUMBER_P]【RZRM1250】が存在しません --" 
				endif
			endif
			if ((-f $indir/EXAMQUESTION)&&(! -f $dir/EXAMQUESTION)) then
				cp -p $indir/EXAMQUESTION $dir/EXAMQUESTION
			else if (! -f $indir/EXAMQUESTION) then
				echo "-- 模試設問マスタ[EXAMQUESTION]【RZRM3310】が存在しません --" 
			endif
			if ((-f $indir/MARKANSWER_NO)&&(! -f $dir/MARKANSWER_NO)) then
				cp -p $indir/MARKANSWER_NO $dir/MARKANSWER_NO
			else if (! -f $indir/MARKANSWER_NO) then
				if (($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66)) then
				echo "-- マーク模試解答番号マスタ[MARKANSWER_NO]【RZRM3410】が存在しません --" 
				endif
			endif
		endif

		#-- ログ出力
		if ($#schoolcdlist != 0) then
			echo " grep $gstring start..."
		else 
			echo " grep 全件コピー start..."
		endif


		if (-f $indir/EXAMTAKENUM_S) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/EXAMTAKENUM_S $dir/EXAMTAKENUM_S
			else
				grep $gstring $indir/EXAMTAKENUM_S >> $dir/EXAMTAKENUM_S

				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/EXAMTAKENUM_S) then
					\rm -f $dir/EXAMTAKENUM_S
#					echo "-- 模試受験者総数(高校)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- 模試受験者総数(高校)[EXAMTAKENUM_S]【RZRM1380】が存在しません --"
			endif 
		endif
		if (-f $indir/EXAMTAKENUM_C) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/EXAMTAKENUM_C $dir/EXAMTAKENUM_C
			else
				grep $gstring $indir/EXAMTAKENUM_C >> $dir/EXAMTAKENUM_C
			
				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/EXAMTAKENUM_C) then
					\rm -f $dir/EXAMTAKENUM_C
#					echo "-- 模試受験者総数(クラス)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- 模試受験者総数(クラス)[EXAMTAKENUM_C]【RZRM1480】が存在しません --" 
			endif 
		endif
		if (-f $indir/SUBRECORD_S) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/SUBRECORD_S $dir/SUBRECORD_S
			else
				grep $gstring $indir/SUBRECORD_S >> $dir/SUBRECORD_S
			
				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/SUBRECORD_S) then
					\rm -f $dir/SUBRECORD_S
#					echo "-- 科目別成績(高校)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- 科目別成績(高校)[SUBRECORD_S]【RZRM1310】が存在しません --" 
			endif
		endif
		if (-f $indir/SUBRECORD_C) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/SUBRECORD_C $dir/SUBRECORD_C
			else
				grep $gstring $indir/SUBRECORD_C >> $dir/SUBRECORD_C
			
				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/SUBRECORD_C) then
					\rm -f $dir/SUBRECORD_C
#					echo "-- 科目別成績(クラス)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- 科目別成績(クラス)[SUBRECORD_C]【RZRM1410】が存在しません --" 
			endif
		endif
		if (-f $indir/QRECORD_S) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/QRECORD_S $dir/QRECORD_S
			else
				grep $gstring $indir/QRECORD_S >> $dir/QRECORD_S
			
				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/QRECORD_S) then
					\rm -f $dir/QRECORD_S
#					echo "-- 設問別成績(高校)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($exam2 != 38)&&($examtystr != "rerun")) then
				echo "-- 設問別成績(高校)[QRECORD_S]【RZRM1330】が存在しません --" 
			endif
		endif
		if (-f $indir/QRECORD_C) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/QRECORD_C $dir/QRECORD_C
			else
				grep $gstring $indir/QRECORD_C >> $dir/QRECORD_C
			
				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/QRECORD_C) then
					\rm -f $dir/QRECORD_C
#					echo "-- 設問別成績(クラス)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($exam2 != 38)&&($examtystr != "rerun")) then
				echo "-- 設問別成績(クラス)[QRECORD_C]【RZRM1430】が存在しません --" 
			endif
		endif
		if (-f $indir/QRECORDZONE) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/QRECORDZONE $dir/QRECORDZONE
			else
				grep $gstring $indir/QRECORDZONE >> $dir/QRECORDZONE
			
				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/QRECORDZONE) then
                                        \rm -f $dir/QRECORDZONE
#					echo "-- 設問別成績層別成績データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($exam2 != 38)&&($examtystr != "rerun")) then
				echo "-- 設問別成績層別成績[QRECORDZONE]【RZRM1340】が存在しません --" 
			endif
		endif
		if (-f $indir/SUBDISTRECORD_S) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/SUBDISTRECORD_S $dir/SUBDISTRECORD_S
			else
				grep $gstring $indir/SUBDISTRECORD_S >> $dir/SUBDISTRECORD_S
				
				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/SUBDISTRECORD_S) then
                                        \rm -f $dir/SUBDISTRECORD_S
#					echo "-- 科目分布別成績(高校)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- 科目分布別成績(高校)[SUBDISTRECORD_S]【RZRM1320】が存在しません --" 
			endif
		endif
		if (-f $indir/SUBDISTRECORD_C) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/SUBDISTRECORD_C $dir/SUBDISTRECORD_C
			else
				grep $gstring $indir/SUBDISTRECORD_C >> $dir/SUBDISTRECORD_C

				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/SUBDISTRECORD_C) then
                                        \rm -f $dir/SUBDISTRECORD_C
#					echo "-- 科目分布別成績(クラス)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- 科目分布別成績(クラス)[SUBDISTRECORD_C]【RZRM1420】が存在しません --" 
			endif
		endif
		if (-f $indir/RATINGNUMBER_S) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/RATINGNUMBER_S $dir/RATINGNUMBER_S
			else
				grep $gstring $indir/RATINGNUMBER_S >> $dir/RATINGNUMBER_S

				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/RATINGNUMBER_S) then
                                        \rm -f $dir/RATINGNUMBER_S
#					echo "-- 志望大学評価別人数(高校)データには、検証校は存在しませんでした。"
				endif
				
			endif
		else
			if (($examtystr != "score")&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 74)&&($exam2 != 65)&&($examtystr != "rerun")) then
				echo "-- 志望大学評価別人数(高校)[RATINGNUMBER_S]【RZRM1350】が存在しません --" 
			endif
		endif
		if (-f $indir/RATINGNUMBER_C) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/RATINGNUMBER_C $dir/RATINGNUMBER_C
			else
				grep $gstring $indir/RATINGNUMBER_C >> $dir/RATINGNUMBER_C

				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/RATINGNUMBER_C) then
                                        \rm -f $dir/RATINGNUMBER_C
#					echo "-- 志望大学評価別人数(クラス)データには、検証校は存在しませんでした。"
				endif
				
			endif
		else
			if (($examtystr != "score")&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 74)&&($exam2 != 65)&&($examtystr != "rerun")) then
				echo "-- 志望大学評価別人数(クラス)[RATINGNUMBER_C]【RZRM1450】が存在しません --" 
			endif
		endif
		if (-f $indir/MARKQSCORERATE) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/MARKQSCORERATE $dir/MARKQSCORERATE
			else
				grep $gstring $indir/MARKQSCORERATE >> $dir/MARKQSCORERATE

				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/MARKQSCORERATE) then
                                        \rm -f $dir/MARKQSCORERATE
#					echo "-- マーク模試高校設問別正答率データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if ((($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66))&&($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- マーク模試高校設問別正答率[MARKQSCORERATE]【RZRM1360】が存在しません --" 
			endif
		endif
		if (-f $indir/INDIVIDUALRECORD) then
			if (($getflg == -1)&&(-f $indir/INDIVIDUALRECORD-old)) then
				mv $indir/INDIVIDUALRECORD $indir/INDIVIDUALRECORD-zitaku
				mv $indir/INDIVIDUALRECORD-old $indir/INDIVIDUALRECORD
			endif
			if ($#schoolcdlist == 0) then
				cp -p $indir/INDIVIDUALRECORD $dir/INDIVIDUALRECORD
				if (($getflg == -1)&&(-f $indir/INDIVIDUALRECORD-zitaku)) then
					cp -p $indir/INDIVIDUALRECORD-zitaku $dir/INDIVIDUALRECORD-zitaku
				endif
			else
				grep $gstring2 $indir/INDIVIDUALRECORD >> $dir/INDIVIDUALRECORD

				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/INDIVIDUALRECORD) then
                                        \rm -f $dir/INDIVIDUALRECORD
#					echo "-- 個表データには、検証校は存在しませんでした。"
				endif
				
			endif
		else
			if ($examtystr != "rerun") then
				echo "-- 個表データ[INDIVIDUALRECORD]【RZRM2110】が存在しません --" 
			endif
		endif

		rmdir $dir >& /dev/null
		if ($status != 0) then
			echo "${year}selection" >> $filepath/grepexamstr
		endif

		@ count5 ++
	end

	echo " grep END"

	#--- 抽出データにより上書きされるため、最新の自宅解答データも登録対象にする
	if (-d /keinavi/HULFT/NewEXAM/${orginyear}zigo) then
		if (-f /keinavi/HULFT/NewEXAM/${orginyear}zigo/INDIVIDUALRECORD-zitaku) then
			if (! -d $filepath/temp/${orginyear}zigoselection) then
				mkdir $filepath/temp/${orginyear}zigoselection
				chmod 777 $filepath/temp/${orginyear}zigoselection
			endif
			grep $gstring2 /keinavi/HULFT/NewEXAM/${orginyear}zigo/INDIVIDUALRECORD-zitaku >> $filepath/temp/${orginyear}zigoselection/INDIVIDUALRECORD 
			
			#-- grep結果が0件時はファイル削除。そうでなければ登録対象にする。
			if (! -s $filepath/temp/${orginyear}zigoselection/INDIVIDUALRECORD) then
				\rm -f $filepath/temp/${orginyear}zigoselection/INDIVIDUALRECORD
				rmdir $filepath/temp/${orginyear}zigoselection
			else
				echo "${orgyear}zigoselection" >> $filepath/grepexamstr
			endif
		endif
	endif

	#--- 抽出データにより上書きされるため、最新の得点修正データも登録対象にする
	if (-d /keinavi/HULFT/NewEXAM/${orginyear}score) then
		if (! -d $filepath/temp/${orginyear}scoreselection) then
			mkdir $filepath/temp/${orginyear}scoreselection
			chmod 777 $filepath/temp/${orginyear}scoreselection
		endif
		grep $gstring2 /keinavi/HULFT/NewEXAM/${orginyear}score/INDIVIDUALRECORD >> $filepath/temp/${orginyear}scoreselection/INDIVIDUALRECORD 
	
		#-- grep結果が0件時はファイル削除。そうでなければ登録対象にする。
		if (! -s $filepath/temp/${orginyear}scoreselection/INDIVIDUALRECORD) then
			\rm -f $filepath/temp/${orginyear}scoreselection/INDIVIDUALRECORD
			rmdir $filepath/temp/${orginyear}scoreselection
		else
			echo "${orgyear}scoreselection" >> $filepath/grepexamstr
		endif
	endif


	if ($getflg != 1) then
		rm -rf $targetfile
	endif

	@ count ++
end
if (-f $filepath/grepexamstr) then
	chmod 664 $filepath/grepexamstr
	\rm $filepath/grepexamstr
endif

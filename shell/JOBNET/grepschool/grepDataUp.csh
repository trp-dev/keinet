#! /bin/csh

###### 抽出集計・個表データを検証機へ登録するバッチ
###### 抽出対象データは既本番機へ登録済みのデータ

set Ecdoutdir = "/home/navikdc/datastore/inside_release"
set lockfile = "${Ecdoutdir}/store.lck"
set DData = "/keinavi/HULFT/DATA"
set examsetfile = "${DData}/grepexamstr"

#--- データ登録中（並行JOBNET起動）かの確認
if (-f ${lockfile}) then
	echo "----  Parallel Processing ERROR  ----"
	echo " 現在、データ登録中のため実行はできません。現在の処理が終了後、再度実行してください"
	exit 30
endif

#--- 検証校抽出
/keinavi/JOBNET/grepschool/grepschool.csh 0


if (-f ${examsetfile}) then
	set examlist = `cat ${examsetfile}`
	set count = 1
	rm -f ${examsetfile}

	echo "--- 模試：$examlist データを検証機登録データとしてセットします。"
		
	#--- 登録模試コード定義の簡易化
	set exam121 = 0
	set exam122 = 0
	set exam123 = 0
	while ($count <= $#examlist)
		set targetexam = $examlist[$count]
		set examyearcd = `echo $targetexam | awk '{printf "%-.6s\n",$0}'`
		#-- データ種別文字列取得
		set examtyp = `echo $targetexam | sed -e s'/[0-9]_*//'g`  	
		set examyear = `echo $examyearcd | awk '{printf "%-.4s\n",$0}'`	
                set examcd = `expr $examyearcd : ".*\(..\)"`

		#--- 高12模試をフォルダ形式にする
		if (($examcd == 61)||($examcd == 71)) then
			@ exam121 ++
			echo "$targetexam" >> ${DData}/temp/${examyear}61_71${examtyp}-temp
		else if (($examcd == 62)||($examcd == 72)) then
			@ exam122 ++
			echo "$targetexam" >> ${DData}/temp/${examyear}62_72${examtyp}-temp
		else if (($examcd == 63)||($examcd == 73)) then
			@ exam123 ++
			echo "$targetexam" >> ${DData}/temp/${examyear}63_73${examtyp}-temp
		else
			echo "$targetexam" >> ${examsetfile}
		endif
		
		sleep 1
		@ count ++
	end

	
	#--- 高12模試をフォルダ形式にする
	(ls -tr ${DData}/temp/*-temp > tmpout.log) >& /dev/null
	set exam12list = `cat tmpout.log`
	\rm tmpout.log
	if ($#exam12list != 0) then
		set loopcount = 1
		while ($loopcount <= $#exam12list)
			set exams = `cat $exam12list[$loopcount]`
			echo " "
			set yexam12 = `echo $exams[1] | awk '{printf "%-.6s\n",$0}'`
			set exam12typ = `echo $exams[1] | sed -e s'/[0-9]_*//'g`
			set year12cd = `echo $yexam12 | awk '{printf "%-.4s\n",$0}'`
			set exam12cd = `expr $yexam12 : ".*\(..\)"`
			if (($exam12cd == 61)||($exam12cd == 71)) then
				if ($#exams == 2) then
					echo "${year12cd}61_71${exam12typ}" >> ${examsetfile}
				else
					echo $exams >> ${examsetfile}
					#--フォルダ名変更
					mv ${DData}/temp/${year12cd}61_71${exam12typ} ${DData}/temp/$exams
				endif
			else if	(($exam12cd == 62)||($exam12cd == 72)) then
				if ($#exams == 2) then
					echo "${year12cd}62_72${exam12typ}" >> ${examsetfile}
				else
					echo $exams >> ${examsetfile}
					#--フォルダ名変更
					mv ${DData}/temp/${year12cd}62_72${exam12typ} ${DData}/temp/$exams
				endif	
			else if (($exam12cd == 63)||($exam12cd == 73)) then
				if ($#exams == 2) then
					echo "${year12cd}63_73${exam12typ}" >> ${examsetfile}
				else
					echo $exams >> ${examsetfile}
					#--フォルダ名変更
					mv ${DData}/temp/${year12cd}63_73${exam12typ} ${DData}/temp/$exams
				endif
			endif
			\rm -f $exam12list[$loopcount]

			@ loopcount ++
		end
	endif
				 
	echo "${DData}" >> ${lockfile}
	chmod 664 ${lockfile}

else
	echo "--- 検証機へ登録する模試データが存在しません"
	echo "--- データ送信や登録対象模試定義ファイル配置の確認を行ってなってください"
	exit 30
endif


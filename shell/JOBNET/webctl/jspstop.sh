#!/bin/sh

# Stop Web Servers
#  -- Change httpd to Maintenance mode.
#rsh RPVE01test /etc/rc.d/init.d/httpd.mainte restart
#rsh RPVE02test /etc/rc.d/init.d/httpd.mainte restart
rsh RPVE03test /etc/rc.d/init.d/httpd.mainte restart
rsh royn4 /etc/rc.d/init.d/httpd.mainte restart
rsh royn5 /etc/rc.d/init.d/httpd.mainte restart
#rsh royn6 /etc/rc.d/init.d/httpd.mainte restart
#rsh royn7 /etc/rc.d/init.d/httpd.mainte restart
#rsh royn8 /etc/rc.d/init.d/httpd.mainte restart
#rsh royn9 /etc/rc.d/init.d/httpd.mainte restart
#  -- Stop tomcat.
#rsh RPVE01test /etc/rc.d/init.d/tomcat stop
#rsh RPVE02test /etc/rc.d/init.d/tomcat stop
rsh RPVE03test /etc/rc.d/init.d/tomcat stop
rsh royn4 /etc/rc.d/init.d/tomcat stop
rsh royn5 /etc/rc.d/init.d/tomcat stop
#rsh royn6 /etc/rc.d/init.d/tomcat stop
#rsh royn7 /etc/rc.d/init.d/tomcat stop
#rsh royn8 /etc/rc.d/init.d/tomcat stop
#rsh royn9 /etc/rc.d/init.d/tomcat stop

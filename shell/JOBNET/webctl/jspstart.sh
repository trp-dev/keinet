#!/bin/sh

# Start Web Servers
#  -- Start tomcat.
echo "Start tomcat.."
#rsh RPVE01test /etc/rc.d/init.d/tomcat start
#rsh RPVE02test /etc/rc.d/init.d/tomcat start
echo "-RPVE03test" 
rsh RPVE03test /etc/rc.d/init.d/tomcat start
echo "-royn4" 
rsh royn4 /etc/rc.d/init.d/tomcat start
echo "-royn5" 
rsh royn5 /etc/rc.d/init.d/tomcat start
#rsh royn6 /etc/rc.d/init.d/tomcat start
#rsh royn7 /etc/rc.d/init.d/tomcat start
#rsh royn8 /etc/rc.d/init.d/tomcat start
#rsh royn9 /etc/rc.d/init.d/tomcat start

# Waiting..
sleep 8

#  -- Change httpd to Operation mode.
echo "Change httpd to Operation mode."
#rsh RPVE01test /etc/rc.d/init.d/httpd restart
#rsh RPVE02test /etc/rc.d/init.d/httpd restart
echo "-RPVE03test"
rsh RPVE03test /etc/rc.d/init.d/httpd restart
echo "-royn4"
rsh royn4 /etc/rc.d/init.d/httpd restart
echo "-royn5"
rsh royn5 /etc/rc.d/init.d/httpd restart
#rsh royn6 /etc/rc.d/init.d/httpd restart
#rsh royn7 /etc/rc.d/init.d/httpd restart
#rsh royn8 /etc/rc.d/init.d/httpd restart
#rsh royn9 /etc/rc.d/init.d/httpd restart

#! /bin/csh 

##### 受験学力測定用調査回答データダウンロード 自動作成(検証機転送&作成)バッチ
##### ユーザ orauser にて実行可能

set TYOUSAconf = "/keinavi/JOBNET/TYOUSAup"

set place = "/keinavi/HULFT"

set tyousafile1 = "TYOUSA_ONE"
set tyousafile2 = "TYOUSA_TWO"


#---　データ抽出処理などでは定義ファイルはなしのため
if (! -f $TYOUSAconf) then
        echo "---- 定義ファイルが無いか登録対象模試ではありません ----"
        exit 1
endif

cd /keinavi/HULFT
#--- 対象データファイルリスト取得
(ls -rt TYOUSA_ONE* > fcount1.log) >& /dev/null
set FILELIST1 = `cat fcount1.log`
\rm -f fcount1.log

(ls -rt TYOUSA_TWO* > fcount2.log) >& /dev/null
set FILELIST2 = `cat fcount2.log`
\rm -f fcount2.log

if ($#FILELIST1 == 0) then
	echo "---- $tyousafile1 データファイルは見つかりません ----"
endif
if ($#FILELIST2 == 0) then
	echo "---- $tyousafile2 データファイルは見つかりません ----"
endif


set TYOUSAUPCD = `cat $TYOUSAconf`
set upcount = 1
set storecount = 0 

while ($upcount <= $#TYOUSAUPCD)
	set targetTYOUSA = $TYOUSAUPCD[$upcount]
	
	set taryear=`echo $targetTYOUSA | awk -F, '{print $1}'`
	set tarflgcd=`echo $targetTYOUSA | awk -F, '{print $2}'`


	if (($tarflgcd == 1)||($tarflgcd == 3)) then

		set DLcount = 1
		while ($DLcount <= $#FILELIST1)
	
			#----対象ファイル名を取得
			set DL1target =  $FILELIST1[$DLcount]

			#----すでに処理済のファイルはスキップ
			if (! -f $DL1target) then
				@ DLcount ++
				continue
			endif

			#----ファイルから年度と模試コードを取得
			set orgyear1=`awk -F, NR=="2"'{print $2}' $DL1target`
			set exam1=`awk -F, NR=="2"'{print $3}' $DL1target`

			#---- 定義ファイル以外のデータは登録処理を行わない
			if ($taryear != $orgyear1$exam1) then 
				@ DLcount ++
				continue
			endif	
			echo "--- 登録対象ファイル：$DL1target (年度:$orgyear1 模試:$exam1)"

			set storecount = 1 

			set tarexamdir = ${exam1}_1

			#----対象ファイルを検証機へFTP転送
			echo "--- ファイル転送開始..."
			/keinavi/HULFT/ftp_rode.sh /HULFT/INDIVIDUALRECORD_FD $place $DL1target satu04 orauser
			#----リモートシェルを実行&結果取得
			/usr/bin/rsh satu04 /keinavi/JOBNET/JNRZ3040/JNRZ3040.sh $DL1target $orgyear1 $tarexamdir
			if ($status != 0) then
				echo "-1" > resulout.log
			else
				/usr/bin/rsh satu04 cat /keinavi/JOBNET/EXAMDL.log > resulout.log
			endif
			/usr/bin/rsh satu04 \rm /keinavi/JOBNET/EXAMDL.log

			#----コマンド実行結果の識別
			set outcord = `head -1 resulout.log`
			\rm -f resulout.log
			if ($outcord > 0) then
				echo "---- 調査回答１データファイル $DL1target に対して処理を中断します ----" 
				echo " "
				@ DLcount ++
				continue
			endif
		
			echo "--- 調査回答１データ ${orgyear1}${exam1} 作成完了 ---" 
			echo " "
			break
		end
	endif
	if (($tarflgcd == 2)||($tarflgcd == 3)) then

		set DLcount = 1
		while ($DLcount <= $#FILELIST2)
	
			#----対象ファイル名を取得
			set DL2target =  $FILELIST2[$DLcount]

			#----すでに処理済のファイルはスキップ
			if (! -f $DL2target) then
				@ DLcount ++
				continue
			endif

			#----ファイルから年度と模試コードを取得
			set orgyear2=`awk -F, NR=="2"'{print $2}' $DL2target`
			set exam2=`awk -F, NR=="2"'{print $3}' $DL2target`

			#---- 定義ファイル以外のデータは登録処理を行わない
			if ($taryear != $orgyear2$exam2) then 
				@ DLcount ++
				continue
			endif	
			echo "--- 登録対象ファイル：$DL2target (年度:$orgyear2 模試:$exam2)"

			set storecount = 1 

			set tarexamdir2 = ${exam2}_2

			#----対象ファイルを検証機へFTP転送
			echo "--- ファイル転送開始..."
			/keinavi/HULFT/ftp_rode.sh /HULFT/INDIVIDUALRECORD_FD $place $DL2target satu04 orauser
			#----リモートシェルを実行&結果取得
			/usr/bin/rsh satu04 /keinavi/JOBNET/JNRZ3040/JNRZ3040.sh $DL2target $orgyear2 $tarexamdir2
			if ($status != 0) then
				echo "-1" > resulout.log
			else
				/usr/bin/rsh satu04 cat /keinavi/JOBNET/EXAMDL.log > resulout.log
			endif
			/usr/bin/rsh satu04 \rm /keinavi/JOBNET/EXAMDL.log

			#----コマンド実行結果の識別
			set outcord = `head -1 resulout.log`
			\rm -f resulout.log
			if ($outcord > 0) then
				echo "---- 調査回答２データファイル $DL2target に対して処理を中断します ----" 
				echo " "
				@ DLcount ++
				continue
			endif
		
			echo "--- 調査回答２データ ${orgyear2}${exam2} 作成完了 ---" 
			echo " "
			break
		end
	endif

	if ($tarflgcd == 1) then
		set targ = ($DL1target)
		set dirs = ($tarexamdir)
		set examcd = $exam1
		set year = $orgyear1
	else if ($tarflgcd == 2) then
		set targ = ($DL2target)
		set dirs = (tarexamdir2)
		set examcd = $exam2
		set year = $orgyear2
	else if ($tarflgcd == 3) then
		set targ = ($DL1target $DL2target)
		set dirs = ($tarexamdir $tarexamdir2)
		set examcd = $exam1
		set year = $orgyear1
	endif

	set filenums = 1
	while ($filenums <= $#targ)

		#----作成ファイルを検証機へアップ
		/usr/bin/rsh satu01test -l kei-navi /home/kei-navi/dataupscript/dataup.sh /keinavi/freemenu/examup/${year} $dirs[$filenums].tar.Z /keinavi/freemenu/exam/${year} 10.1.4.177
		if ($status != 0) then
			echo "  "
			@ count ++
			continue
		else
			echo "--- 模試別成績データダウンロードデータの同期開始..."
			/usr/bin/rsh satu01test -l kei-navi /usr/local/etc/rsync_keinavi_download.sh
			echo "模試別成績データダウンロードデータの同期処理完了 ---"
		endif
		echo "--- 作成ファイル 検証機展開処理完了 ---"

		#----対象ファイルをDATAフォルダへ移動
		#--- 受験学力測定テスト
		set yeare = "${year}37_69_79"
		set yexam = "$yeare/${examcd}"
			
		if (! -d /keinavi/HULFT/DATA/${yexam}) then
			mkdir -p /keinavi/HULFT/DATA/${yexam}
			chmod 777 /keinavi/HULFT/DATA/${yexam}
			if ($yeare != 0) then
				chmod 777 /keinavi/HULFT/DATA/$yeare
			endif
		endif
		mv $targ[$filenums] /keinavi/HULFT/DATA/${yexam}
		echo "  "

		@ filenums ++
	end
	echo " "
	echo " "
	@ upcount ++
end

#-- OP模試などDLファイルが存在しないとき 
if (-f $TYOUSAconf) then
	if ($storecount == 0) then
		echo "---- 登録対象のデータファイルが存在しません ----"
	endif
	\rm -f $TYOUSAconf
endif

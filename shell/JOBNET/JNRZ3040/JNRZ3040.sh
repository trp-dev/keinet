#! /bin/sh

source ~/.bash_profile

#環境変数設定
export JOBNAME=JNRZ3040
export WORKDIR=/keinavi/JOBNET/$JOBNAME
export logoutf=/keinavi/JOBNET/EXAMDL.log

#対象ファイルをセット
export DLfilename=`echo $1 | sed -e s'/[0-9]\{14\}$//'g`
mv /HULFT/INDIVIDUALRECORD_FD/$1 /keinavi/bat/FreeMenu/F005/data/$DLfilename

#模試コード(フォルダ名)をセット
examdir=${3}


#旧ファイルをバックアップ
export newyear=$2
if [ $examdir = "38" ]
then
	newyear=`expr $2 - 1`
fi
if [ -f /keinavi/freemenu/examup/$newyear/${examdir}.tar.Z ]
then
#	if [ $examdir = "28" ]||[ $examdir = "68" ]||[ $examdir = "78" ]||[ $examdir = "37" ]||[ $examdir = "69" ]||[ $examdir = "79" ]
	if [ $examdir = "28" ]||[ $examdir = "68" ]||[ $examdir = "78" ]
	then
		echo "---- ERROR : 模試データ${newyear}$examdir に未公開データが存在します ----"
		echo 10 > $logoutf
		exit 10
	fi
	\rm -rf /keinavi/freemenu/examup/$newyear/${examdir}.tar.Z
fi

if [ -d /keinavi/freemenu/exam/$newyear/$examdir ]
then
	export bknum=
	if [ -d /keinavi/freemenu/exam/$newyear/${examdir}-old ]
	then
		export bknum=2
		test -d /keinavi/freemenu/exam/$newyear/${examdir}-old2 && \rm -rf /keinavi/freemenu/exam/$newyear/${examdir}-old
		test -d /keinavi/freemenu/exam/$newyear/${examdir}-old2 && mv /keinavi/freemenu/exam/$newyear/${examdir}-old2 /keinavi/freemenu/exam/$newyear/${examdir}-old
	fi
	test -d /keinavi/freemenu/exam/$newyear/$examdir && mv /keinavi/freemenu/exam/$newyear/$examdir /keinavi/freemenu/exam/$newyear/${examdir}-old${bknum}
fi


#高校別成績データ作成処理実行(Java)
cd /keinavi/bat/FreeMenu/F005
/keinavi/bat/FreeMenu/F005/F005.sh

if [[ ! -d /keinavi/freemenu/exam/$newyear ]]
then
	mkdir /keinavi/freemenu/exam/$newyear
	chmod 775 /keinavi/freemenu/exam/$newyear
fi
if [[ ! -d /keinavi/freemenu/examup/$newyear ]]
then
	mkdir /keinavi/freemenu/examup/$newyear
	chmod 775 /keinavi/freemenu/examup/$newyear
fi
mv /keinavi/freemenu/examup/$2/$examdir /keinavi/freemenu/exam/$newyear
cp -rp /keinavi/freemenu/exam/$newyear/$examdir /keinavi/freemenu/examup/$newyear
cd /keinavi/freemenu/examup/$newyear
tar -Zcf ${examdir}.tar.Z $examdir
#--- 本番機展開用ファイルは圧縮形式で保持するためフォルダは削除する
\rm -rf /keinavi/freemenu/examup/$newyear/$examdir 
if [ -f /keinavi/bat/FreeMenu/F005/data/INDIVIDUALRECORD_FD ]
then
	\rm -f /keinavi/bat/FreeMenu/F005/data/INDIVIDUALRECORD_FD
elif [ -f /keinavi/bat/FreeMenu/F005/data/TYOUSA_ONE ]
then
	\rm -f /keinavi/bat/FreeMenu/F005/data/TYOUSA_ONE
elif [ -f /keinavi/bat/FreeMenu/F005/data/TYOUSA_TWO ]
then
	\rm -f /keinavi/bat/FreeMenu/F005/data/TYOUSA_TWO
fi

#バッチ処理戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo バッチ処理でエラーが発生しました
    echo 30 > $logoutf
    exit 30
fi

#正常終了
echo バッチ処理が正常に終了しました
echo 0 > $logoutf
exit 0    #正常終了

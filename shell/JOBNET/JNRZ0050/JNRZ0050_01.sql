-- ************************************************
-- *機 能 名 : 科目別成績(全国)
-- *機 能 ID : JNRZ0050_01.sql
-- *作 成 日 : 2004/08/27
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE SUBRECORD_A_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE SUBRECORD_A_TMP  (
       EXAMYEAR              CHAR(4)               NOT NULL,   /*    模試年度             */ 
       EXAMCD                CHAR(2)               NOT NULL,   /*    模試コード           */ 
       SUBCD                 CHAR(4)               NOT NULL,   /*    科目コード           */ 
       STUDENTDIV            CHAR(1)               NOT NULL,   /*    現役高卒区分         */ 
       AVGPNT                NUMBER(5,1)                   ,   /*    平均点               */ 
       AVGDEVIATION          NUMBER(4,1)                   ,   /*    平均偏差値           */ 
       AVGSCORERATE          NUMBER(4,1)                   ,   /*    平均得点率           */ 
       STDDEVIATION          NUMBER(4,1)                   ,   /*    標準偏差             */ 
       HIGHESTPNT            NUMBER(4,0)                   ,   /*    最高点               */ 
       LOWESTPNT             NUMBER(4,0)                   ,   /*    最低点               */ 
       NUMBERS               NUMBER(8,0)                   ,   /*    人数                 */ 
       CONSTRAINT PK_SUBRECORD_A_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  年度  */
                 EXAMCD                    ,               /*  模試コード    */
                 SUBCD                     ,               /*  科目コード    */
                 STUDENTDIV                                /*  現役高卒区分  */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

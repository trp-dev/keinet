------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 科目別成績（全国）
--  テーブル名      : SUBRECORD_A
--  データファイル名: SUBRECORD_A
--  機能            : 科目別成績（全国）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE SUBRECORD_A_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( 
   EXAMYEAR         POSITION(01:04)  CHAR,
   EXAMCD           POSITION(05:06)  CHAR,
   SUBCD            POSITION(07:10)  CHAR,
   STUDENTDIV       POSITION(11:11)  CHAR,
   AVGPNT           POSITION(12:16)  INTEGER EXTERNAL "DECODE(:AVGPNT,         '99999', null, :AVGPNT/10)",
   AVGDEVIATION     POSITION(17:20)  INTEGER EXTERNAL "DECODE(:AVGDEVIATION,    '9999', null, :AVGDEVIATION/10)",
   AVGSCORERATE     POSITION(21:24)  INTEGER EXTERNAL "DECODE(:AVGSCORERATE,    '9999', null, :AVGSCORERATE/10)",
   STDDEVIATION     POSITION(25:28)  INTEGER EXTERNAL "DECODE(:STDDEVIATION,    '9999', null, :STDDEVIATION/10)",
   HIGHESTPNT       POSITION(29:32)  INTEGER EXTERNAL "NULLIF(:HIGHESTPNT,      '9999')",
   LOWESTPNT        POSITION(33:36)  INTEGER EXTERNAL "NULLIF(:LOWESTPNT,       '9999')",
   NUMBERS          POSITION(37:44)  INTEGER EXTERNAL "NULLIF(:NUMBERS,     '99999999')"
)

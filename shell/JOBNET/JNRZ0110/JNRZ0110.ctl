------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 科目分布別成績（高校）
--  テーブル名      : SUBDISTRECORD_S
--  データファイル名: SUBDISTRECORD_S
--  機能            : 科目分布別成績（高校）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE SUBDISTRECORD_S_TMP
(  
   EXAMYEAR     POSITION(01:04)  CHAR,
   EXAMCD       POSITION(05:06)  CHAR,
   BUNDLECD     POSITION(07:11)  CHAR,
   SUBCD        POSITION(12:15)  CHAR,
   DEVZONECD    POSITION(16:17)  CHAR,
   NUMBERS      POSITION(18:25)  INTEGER EXTERNAL "NULLIF(:NUMBERS,   '99999999')",
   COMPRATIO    POSITION(26:29)  INTEGER EXTERNAL "DECODE(:COMPRATIO,     '9999', null, :COMPRATIO/10)"
)


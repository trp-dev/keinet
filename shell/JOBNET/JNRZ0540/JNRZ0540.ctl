------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 高１・２大学マスタ（判定＆志望校名称）
--  テーブル名      : HS12_UNIV
--  データファイル名: HS12_UNIV
--  機能            : 高１・２大学マスタ（判定＆志望校名称）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE HS12_UNIV_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    EXAMYEAR             POSITION(01:04)   CHAR,
    EXAMDIV              POSITION(05:06)   CHAR,
    COLLEGECD            POSITION(07:10)   CHAR,
    FACULTYCD            POSITION(11:12)   CHAR,
    DEPTCD               POSITION(13:14)   CHAR,
    COLLEGEDIVCD         POSITION(15:15)   CHAR,
    COLLEGENAME_ABBR     POSITION(16:29)   CHAR,
    FACULTYNAME_ABBR     POSITION(30:39)   CHAR,
    DEPTNAME_ABBR        POSITION(40:51)   CHAR,
    SUPERABBRNAME        POSITION(52:69)   CHAR,
    COLLEGENAME_KANA     POSITION(70:81)   CHAR,
    FACULTYNAME_KANA     POSITION(82:89)   CHAR,
    DEPTNAME_KANA        POSITION(90:99)   CHAR,
    PREFCD_HQ            POSITION(100:101)   CHAR,
    PREFCD_SH            POSITION(102:103)   CHAR,
    RANKCD               POSITION(104:105)   CHAR,
    DEPTSORTKYE          POSITION(106:155)   CHAR
)

-- *********************************************************
-- *機 能 名 : 高１・２大学マスタ（判定＆志望校名称）
-- *機 能 ID : JNRZ0540_02.sql
-- *作 成 日 : 2004/08/31
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM HS12_UNIV
WHERE  (EXAMYEAR,EXAMDIV)
       IN(SELECT DISTINCT EXAMYEAR,EXAMDIV
          FROM   HS12_UNIV_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO HS12_UNIV
SELECT * FROM HS12_UNIV_TMP;

-- 8年以上前のデータを削除
DELETE FROM HS12_UNIV
WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 8);

-- 更新対象データの7年前のデータを削除
DELETE FROM HS12_UNIV
WHERE  (EXAMYEAR,EXAMDIV)
       IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 7)),EXAMDIV
          FROM   HS12_UNIV_TMP);

-- テンポラリテーブル削除
DROP TABLE HS12_UNIV_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

-- ************************************************
-- *機 能 名 : 高１・２大学マスタ（判定＆志望校名称）
-- *機 能 ID : JNRZ0540_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE HS12_UNIV_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE HS12_UNIV_TMP  (
       EXAMYEAR             CHAR(4)              NOT NULL,    /*   年度                 */ 
       EXAMDIV              CHAR(2)              NOT NULL,    /*   模試区分             */ 
       COLLEGECD            CHAR(4)              NOT NULL,    /*   大学コード           */ 
       FACULTYCD            CHAR(2)              NOT NULL,    /*   学部コード           */ 
       DEPTCD               CHAR(2)              NOT NULL,    /*   学科コード           */ 
       COLLEGEDIVCD         CHAR(1)                      ,    /*   大学区分コード       */ 
       COLLEGENAME_ABBR     VARCHAR2(14)                 ,    /*   大学名短縮           */ 
       FACULTYNAME_ABBR     VARCHAR2(10)                 ,    /*   学部名短縮           */ 
       DEPTNAME_ABBR        VARCHAR2(12)                 ,    /*   学科名短縮           */ 
       SUPERABBRNAME        VARCHAR2(18)                 ,    /*   超短縮名             */ 
       COLLEGENAME_KANA     VARCHAR2(12)                 ,    /*   大学名カナ           */ 
       FACULTYNAME_KANA     VARCHAR2(8)                  ,    /*   学部名カナ           */ 
       DEPTNAME_KANA        VARCHAR2(10)                 ,    /*   学科名カナ           */ 
       PREFCD_HQ            CHAR(2)                      ,    /*   本部県コード         */ 
       PREFCD_SH            CHAR(2)                      ,    /*   校舎県コード         */ 
       RANKCD               CHAR(2)                      ,    /*   難易ランクコード     */ 
       DEPTSORTKYE          CHAR(50)                     ,    /*   学科ソートキー       */ 
       CONSTRAINT PK_HS12_UNIV_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  年度        */
                 EXAMDIV                   ,               /*  模試区分    */
                 COLLEGECD                 ,               /*  大学コード  */
                 FACULTYCD                 ,               /*  学部コード  */
                 DEPTCD                                    /*  学科コード  */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

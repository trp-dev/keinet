------------------------------------------------------------------------------
-- *機 能 名 : 記述系模試小問別成績（全国）のCTLファイル
-- *機 能 ID : JNRZ1540.ctl
-- *作 成 日 : 2019/09/24
-- *作 成 者 : QQ)瀬尾
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE SHOQUESTIONRECORD_A_TMP
(  
   EXAMYEAR                 POSITION(01:04)  CHAR 
 , EXAMCD                   POSITION(05:06)  CHAR 
 , SUBCD                    POSITION(07:10)  CHAR 
 , KWEB_SHOQUESTION_DEPT_NO POSITION(11:13)  INTEGER EXTERNAL 
 , AVGPNT                   POSITION(14:17)  INTEGER EXTERNAL "DECODE(:AVGPNT,                   '9999', null, :AVGPNT/10)"
 , NUMBERS                  POSITION(18:25)  INTEGER EXTERNAL 
 , AVGSCORERATE             POSITION(26:29)  INTEGER EXTERNAL "DECODE(:AVGSCORERATE,             '9999', null, :AVGSCORERATE/10)"
)

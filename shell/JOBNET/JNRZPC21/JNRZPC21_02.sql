-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM UNIVDIST_CENTER
WHERE  (EXAMYEAR, EXAMCD)
 IN(SELECT DISTINCT EXAMYEAR, EXAMCD FROM UNIVDIST_CENTER_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO UNIVDIST_CENTER
SELECT
	EXAMYEAR           ,
	EXAMCD             ,
	UNIVCD             ,
	FACULTYCD          ,
	DEPTCD1 || DEPTCD2 ,
	PATTERNCD          ,
	ENTEXAMFULLPNT_NREP,
	BORDERSCORERATE1   ,
	BORDERSCORERATE2   ,
	BORDERSCORERATE3   ,
	BORDERSCORE2       ,
	PUBENTEXAMCAPA     ,
	NUMBERS_A          ,
	AVGPNT_A           ,
	DIST1              ,
	BORDER1            ,
	NUMBERS1           ,
	DIST2              ,
	BORDER2            ,
	NUMBERS2           ,
	DIST3              ,
	BORDER3            ,
	NUMBERS3           ,
	DIST4              ,
	BORDER4            ,
	NUMBERS4           ,
	DIST5              ,
	BORDER5            ,
	NUMBERS5           ,
	DIST6              ,
	BORDER6            ,
	NUMBERS6           ,
	DIST7              ,
	BORDER7            ,
	NUMBERS7           ,
	DIST8              ,
	BORDER8            ,
	NUMBERS8           ,
	DIST9              ,
	BORDER9            ,
	NUMBERS9           ,
	DIST10             ,
	BORDER10           ,
	NUMBERS10          ,
	DIST11             ,
	BORDER11           ,
	NUMBERS11          ,
	DIST12             ,
	BORDER12           ,
	NUMBERS12          ,
	DIST13             ,
	BORDER13           ,
	NUMBERS13          ,
	DIST14             ,
	BORDER14           ,
	NUMBERS14          ,
	DIST15             ,
	BORDER15           ,
	NUMBERS15          ,
	DIST16             ,
	BORDER16           ,
	NUMBERS16          ,
	DIST17             ,
	BORDER17           ,
	NUMBERS17          ,
	DIST18             ,
	BORDER18           ,
	NUMBERS18          ,
	DIST19             ,
	BORDER19           ,
	NUMBERS19          ,
	NUMBERS_S          ,
	AVGPNT_S           ,
	NUMBERS_G          ,
	AVGPNT_G           
FROM UNIVDIST_CENTER_TMP;

-- 2年以上前のデータを削除
DELETE FROM UNIVDIST_CENTER
WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 2);

-- 更新対象データの1年前のデータを削除
DELETE FROM UNIVDIST_CENTER
WHERE  (EXAMYEAR, EXAMCD)
 IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(EXAMYEAR) - 1)), EXAMCD FROM UNIVDIST_CENTER_TMP);

-- テンポラリテーブル削除
DROP TABLE UNIVDIST_CENTER_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;




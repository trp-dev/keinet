------------------------------------------------------------------------------
--  システム名      : PC版模試判定バンザイ統合
--  概念ファイル名  : 
--  テーブル名      : DB21_BUNPU -> UNIVDIST_CENTER_TMP
--  機能            : 
--  作成日          : 2014/05/02
--  修正日          : 
--  備考            : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE UNIVDIST_CENTER_TMP
FIELDS TERMINATED BY ','
-- OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
	EXAMYEAR           ,
	EXAMCD             ,
	UNIVCD             ,
	FACULTYCD          ,
	DEPTCD1            ,
	DEPTCD2            ,
	PATTERNCD          ,
	ENTEXAMFULLPNT_NREP,
	BORDERSCORERATE1   ,
	BORDERSCORERATE2   ,
	BORDERSCORERATE3   ,
	BORDERSCORE2       ,
	PUBENTEXAMCAPA     ,
	NUMBERS_A          ,
	AVGPNT_A           ,
	DIST1              ,
	BORDER1            ,
	NUMBERS1           ,
	DIST2              ,
	BORDER2            ,
	NUMBERS2           ,
	DIST3              ,
	BORDER3            ,
	NUMBERS3           ,
	DIST4              ,
	BORDER4            ,
	NUMBERS4           ,
	DIST5              ,
	BORDER5            ,
	NUMBERS5           ,
	DIST6              ,
	BORDER6            ,
	NUMBERS6           ,
	DIST7              ,
	BORDER7            ,
	NUMBERS7           ,
	DIST8              ,
	BORDER8            ,
	NUMBERS8           ,
	DIST9              ,
	BORDER9            ,
	NUMBERS9           ,
	DIST10             ,
	BORDER10           ,
	NUMBERS10          ,
	DIST11             ,
	BORDER11           ,
	NUMBERS11          ,
	DIST12             ,
	BORDER12           ,
	NUMBERS12          ,
	DIST13             ,
	BORDER13           ,
	NUMBERS13          ,
	DIST14             ,
	BORDER14           ,
	NUMBERS14          ,
	DIST15             ,
	BORDER15           ,
	NUMBERS15          ,
	DIST16             ,
	BORDER16           ,
	NUMBERS16          ,
	DIST17             ,
	BORDER17           ,
	NUMBERS17          ,
	DIST18             ,
	BORDER18           ,
	NUMBERS18          ,
	DIST19             ,
	BORDER19           ,
	NUMBERS19          ,
	NUMBERS_S          ,
	AVGPNT_S           ,
	NUMBERS_G          ,
	AVGPNT_G           
)

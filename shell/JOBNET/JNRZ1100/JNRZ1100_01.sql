-- ************************************************
-- *機 能 名 : ワンポイントマスタ
-- *機 能 ID : JNRZ1100_01.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE ONEPOINT_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE ONEPOINT_TMP(   
       SCREENID           CHAR(6)           NOT NULL,   /*   画面ID     */
       HELPID             CHAR(6)           NOT NULL,   /*   ヘルプID   */
       DISPSEQUENCE       NUMBER(2,0)               ,   /*   表示順     */
    CONSTRAINT PK_ONEPOINT_TMP PRIMARY KEY(
        SCREENID,                                       /*   画面ID     */
        HELPID                                          /*   ヘルプID   */
    )
    USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;
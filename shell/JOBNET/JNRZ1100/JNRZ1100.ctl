------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : ワンポイントアドバイスマスタ
--  テーブル名      : ONEPOINT_TMP
--  データファイル名: ONEPOINTyyyymmdd.csv
--  機能            : ワンポイントアドバイスマスタをデータファイルの内容に置き換える
--  作成日          : 2004/08/25
--  修正日          : 2004/09/10
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE ONEPOINT_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    SCREENID,
    HELPID "LTRIM(TO_CHAR(TO_NUMBER(:HELPID),'099999'))",
    DISPSEQUENCE
)

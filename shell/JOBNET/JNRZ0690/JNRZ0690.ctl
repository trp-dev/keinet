------------------------------------------------------------------------------
--  システム名      : KEI-NAVI
--  概念ファイル名  : 科目別成績層別成績（全国）
--  テーブル名      : SUBDEVZONERECORD_A
--  データファイル名: SUBDEVZONERECORD_A
--  機能            : 科目別成績層別成績（全国）をデータファイルの内容に置き換える
--  作成日          : 2009/12/08
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE SUBDEVZONERECORD_A_TMP
(  
   EXAMYEAR        POSITION(01:04)  CHAR,
   EXAMCD          POSITION(05:06)  CHAR,
   SUBCD           POSITION(07:10)  CHAR,
   DEVZONECD       POSITION(11:11)  CHAR,
   AVGPNT          POSITION(12:16)  INTEGER EXTERNAL "DECODE(:AVGPNT,         '99999', null, :AVGPNT/10)",
   NUMBERS         POSITION(17:24)  INTEGER EXTERNAL "NULLIF(:NUMBERS,     '99999999')",
   AVGSCORERATE    POSITION(25:28)  INTEGER EXTERNAL "DECODE(:AVGSCORERATE,    '9999', null, :AVGSCORERATE/10)"
)


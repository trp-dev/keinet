-- *********************************************************
-- *機 能 名 : 科目別成績層別成績（全国）
-- *機 能 ID : JNRZ0690_02.sql
-- *作 成 日 : 2009/12/08
-- *更 新 日 : KCN
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM SUBDEVZONERECORD_A
WHERE  (EXAMYEAR,EXAMCD)
       IN(SELECT DISTINCT EXAMYEAR,EXAMCD
          FROM   SUBDEVZONERECORD_A_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO SUBDEVZONERECORD_A
SELECT * FROM SUBDEVZONERECORD_A_TMP;

-- 8年以上前のデータを削除
-- DELETE FROM SUBDEVZONERECORD_A
-- WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 8);

-- 更新対象データの7年前のデータを削除
-- DELETE FROM SUBDEVZONERECORD_A
-- WHERE  (EXAMYEAR,EXAMCD)
--        IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 7)),EXAMCD
--           FROM   SUBDEVZONERECORD_A_TMP);

-- テンポラリテーブル削除
DROP TABLE SUBDEVZONERECORD_A_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

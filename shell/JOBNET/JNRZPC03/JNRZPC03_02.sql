-- *********************************************************
-- *機 能 名 : 情報誌用科目（二次）
-- *機 能 ID : JNRZPC03_02.sql
-- *作 成 日 : 2015/09/28
-- *作 成 者 : 富士通システムズ・ウエスト
-- *更 新 日 : 2019/09/10
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
TRUNCATE TABLE JH01_KAMOKU3;

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO JH01_KAMOKU3 SELECT * FROM JH01_KAMOKU3_TMP;

-- テンポラリテーブル削除
DROP TABLE JH01_KAMOKU3_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

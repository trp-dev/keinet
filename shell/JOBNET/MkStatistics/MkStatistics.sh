#!/bin/sh

#環境変数設定
export JOBNAME=MkStatistics
export CSVFILE=storedata.csv
export SRCDIR=/home/navikdc/Statistics
export DSTDIR=/home/navikdc
export WORKDIR=/keinavi/JOBNET/$JOBNAME

#登録対象ファイルの有無確認
/keinavi/JOBNET/common/JNRZFCHECK.sh
if [ $? -eq 10 ]
then
        echo 処理対象ファイルなし（正常終了）
        exit 1
fi

#SQL*PLUSテンポラリテーブル作成
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/MkStatistics_01.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo テンポラリテーブル作成処理でエラーが発生しました
    exit 30
fi


#SQL*LOADERシェル実行
/keinavi/JOBNET/common/JNRZ1IMP-2.sh

#SQL*LOADER戻り値チェック(0以外は異常終了)
RET=$?
if [ $RET -ne 0 ]
then
#    if [ $RET -le 10 ]
#    then
        #SQL*PLUSテンポラリテーブル削除
#        /keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/MkStatistics_03.sql
#        echo 処理対象ファイルなし（正常終了）
#        exit 0
#    else
        echo SQL*LOADERシェルの実行でエラーが発生しました
        exit 30
#    fi
fi

#SQL*PLUSデータインポート->テンポラリテーブル削除
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/MkStatistics_02.sql

#rm -f ${DSTDIR}/${CSVFILE}

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo データのインポート処理でエラーが発生しました
    exit 30
fi

#正常終了
echo データのインポート処理が正常に終了しました
exit  0    #正常終了

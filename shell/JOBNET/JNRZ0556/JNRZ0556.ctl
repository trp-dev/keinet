------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 大学マスタ模試用選択グループ
--  テーブル名      : UNIVMASTER_SELECTGROUP
--  データファイル名: UNIVMASTER_SELECTGROUP
--  機能            : 大学マスタ模試用選択グループをデータファイルの内容に置き換える
--  作成日          : 2009/12/17
--  修正日          : 
--  備考            : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE UNIVMASTER_SELECTGROUP_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    EVENTYEAR                 POSITION(01:04)        CHAR,
    SCHOOLDIV                 POSITION(05:05)        CHAR,
    UNIVCD                    POSITION(06:09)        CHAR,
    FACULTYCD                 POSITION(10:11)        CHAR,
    DEPTCD                    POSITION(12:15)        CHAR,
    EXAMDIV                   POSITION(16:17)        CHAR,
    ADMISSIONDIV              POSITION(18:18)        CHAR,
    NEXTYEARDIV               POSITION(19:19)        CHAR,
    ENTEXAMDIV                POSITION(20:20)        CHAR,
    UNICDBRANCHCD             POSITION(21:23)        CHAR,
    SELECTG_NO                POSITION(24:24)        CHAR,
    SELECTGSUBCOUNT           POSITION(25:25)        INTEGER EXTERNAL "NULLIF(:SELECTGSUBCOUNT,          '9')",
    SELECTGALLOTPNT           POSITION(26:30)        INTEGER EXTERNAL "DECODE(:SELECTGALLOTPNT,'99999', null, :SELECTGALLOTPNT/10)"
)

#!/bin/sh
# AICAによる証明書発行処理のラップシェル
# 起動パラメータ  第1パラメータ:証明書有効期限（発効日から期限までの日数）
#                 第2パラメータ:証明書用CSVファイル名（フルパス）
#                 第3パラメータ:証明書ID(YYYY999999の10桁)証明書(p12)ファイル名となる
#                 第4パラメータ:ユーザID(ke99999aの8桁)FTPダウンロード用ファイル名となる
# expectによる証明書発行処理の変更に対応しやすいようにラップする

#ディレクトリを移動
cd /usr/local/aica/keinaviCA

#expectによる証明書の発行処理
/usr/local/aica/expcert.sh $1 $2

#証明書発行処理の戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo 証明書の発行処理でエラーが発生しました
    exit 30
fi

#証明書ファイルをリネームしてFTP用ディレクトリにコピーする
cp -p /usr/local/aica/keinaviCA/p12/$3.p12 /home/kawai/work/$4.p12

#終了ステータスをそのまま返す
exit $?

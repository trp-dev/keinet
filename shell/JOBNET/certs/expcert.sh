#! /usr/bin/expect --
# AICAによる証明書発行処理
# 起動パラメータ  第1パラメータ:証明書有効期限（発効日から期限までの日数）
#                 第1パラメータ:証明書用CSVファイル名（フルパス）

#パラメータ設定
set prompt "(%|#|\\$) $"          ;# プロンプト待ち用定義
set pswd "a810ka\r"               ;# AICA用パスワード
set days [lindex $argv 0]         ;# 第1引数(有効期限)を設定
set csv [lindex $argv 1]          ;# 第2引数(csvファイル名)を設定

#証明書の有効期限を設定
spawn /usr/local/aica/bin/aica -certdays $days -crldays $days

#パスワード送信
expect "Password:"
send "$pswd"
expect -re $prompt

#コマンド実行結果の検査(成功時には"Update CA information."が返る)
#puts $expect_out(buffer) ;# 確認用
if {[string first "Update CA information." $expect_out(buffer)]<0} then {
    puts "有効期限の設定に失敗しました"
    exit 30
}

#証明書の発行
spawn /usr/local/aica/bin/aica -csv $csv

#パスワード送信
expect "Password:"
send "$pswd"
expect -re $prompt

#コマンド実行結果の検査(失敗時にも"Update CA information."が返るので"error"が返るかを見る)
#puts $expect_out(buffer) ;# 確認用
if {[string first "error" $expect_out(buffer)]>=0} then {
    puts "証明書の発行に失敗しました"
    exit 30
}

#終了
puts "証明書の発行処理が正常に終了しました"
exit 0
expect eof

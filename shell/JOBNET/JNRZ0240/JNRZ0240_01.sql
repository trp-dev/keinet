-- ************************************************
-- *機 能 名 : 過年度合格者成績
-- *機 能 ID : JNRZ0240_01.sql
-- *作 成 日 : 2004/08/30
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE PREVSUCCESS_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE PREVSUCCESS_TMP  (
       EXAMYEAR          CHAR(4)               NOT NULL,   /*   模試年度            */ 
       UNIVCD            CHAR(4)               NOT NULL,   /*   大学コード          */ 
       FACULTYCD         CHAR(2)               NOT NULL,   /*   学部コード          */ 
       DEPTCD            CHAR(2)               NOT NULL,   /*   学科コード          */ 
       EXAMTYPECD        CHAR(2)               NOT NULL,   /*   模試種類コード      */ 
       SUBCD             CHAR(4)               NOT NULL,   /*   科目コード          */ 
       SUCCESSNUM        NUMBER(8,0)                   ,   /*   合格者人数          */ 
       SUCCESSAVGDEV     NUMBER(4,1)                   ,   /*   合格者平均偏差値    */ 
       CONSTRAINT PK_PREVSUCCESS_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  模試年度        */
                 UNIVCD                    ,               /*  大学コード      */
                 FACULTYCD                 ,               /*  学部コード      */
                 DEPTCD                    ,               /*  学科コード      */
                 EXAMTYPECD                ,               /*  模試種類コード  */
                 SUBCD                                     /*  科目コード      */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

-- *********************************************************
-- *機 能 名 : 過年度合格者成績
-- *機 能 ID : JNRZ0240_02.sql
-- *作 成 日 : 2004/08/30
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM PREVSUCCESS
WHERE  (EXAMYEAR,EXAMTYPECD)
       IN(SELECT DISTINCT EXAMYEAR,EXAMTYPECD
          FROM   PREVSUCCESS_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO PREVSUCCESS
SELECT * FROM PREVSUCCESS_TMP;

-- 8年以上前のデータを削除
DELETE FROM PREVSUCCESS
WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 5);

-- 更新対象データの7年前のデータを削除
DELETE FROM PREVSUCCESS
WHERE  (EXAMYEAR,EXAMTYPECD)
       IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 4)),EXAMTYPECD
          FROM   PREVSUCCESS_TMP);

-- テンポラリテーブル削除
DROP TABLE PREVSUCCESS_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

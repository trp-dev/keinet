------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 過年度合格者成績
--  テーブル名      : PREVSUCCESS
--  データファイル名: PREVSUCCESS
--  機能            : 過年度合格者成績をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE PREVSUCCESS_TMP
(  
    EXAMYEAR           POSITION(01:04)  CHAR,
    UNIVCD             POSITION(05:08)  CHAR,
    FACULTYCD          POSITION(09:10)  CHAR,
    DEPTCD             POSITION(11:12)  CHAR,
    EXAMTYPECD         POSITION(13:14)  CHAR,
    SUBCD              POSITION(15:18)  CHAR,
    SUCCESSNUM         POSITION(19:26)  INTEGER EXTERNAL "NULLIF(:SUCCESSNUM,      '99999999')",
    SUCCESSAVGDEV      POSITION(27:30)  INTEGER EXTERNAL "DECODE(:SUCCESSAVGDEV,       '9999', null, :SUCCESSAVGDEV/10)"
)


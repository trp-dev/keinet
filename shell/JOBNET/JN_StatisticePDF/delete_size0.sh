#!/bin/sh

cd /home/navikdc/PDF

for filename in `ls PDF_*`; do
	FILESIZE=`ls -l $filename | awk '{print $5}'`
	if [ $FILESIZE = 0 ]
	then
		echo "del $filename"
		rm $filename
	fi
done


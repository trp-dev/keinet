#! /bin/csh 

#--- ファイル配置先
set targetfplace = "/home/navikdc/PDF"

#--- PDF展開ターゲット模試定義ファイル
set file = "/keinavi/JOBNET/JN_StatisticePDF/targetExamPDF"

if (! -f $file) then
	echo "---- PDFファイル展開の対象ではありません ----"
	exit 1
endif

#--- 0size ファイル削除
#/keinavi/JOBNET/JN_StatisticePDF/delete_size0.sh

#--- zipファイル解凍
#/keinavi/JOBNET/JN_StatisticePDF/unzip_PDF.sh


set tgfile = `cat $file`
set count = 1
set exam = ""
while ($count <= $#tgfile)
	set targets = $tgfile[$count]

	
	#-- 該当模試の対象ファイル名を取得(1行のCSV形式文字列より、各文字列を取得)
	set fcount = `echo ${targets} | awk -F, '{ for(i=1;i<=NF;i++) print $i;}'`
	set nums = 1
	set storeyesr = ""
	set storeexam = ""
	while ($nums <= $#fcount)
		if ($nums == 1) then
			#-- 年度取得
			set storeyesr = $fcount[$nums]
		else if ($nums == 2) then
			#-- 模試コード取得
			set storeexam = $fcount[$nums]
	
			echo "--- PDFファイル展開処理開始 模試:${storeyesr}${storeexam} "
			echo " "
		else
			#-- ファイル名（キー文字列）取得
			set tgfname = $fcount[$nums]

			#-- 大文字から小文字へ変換
			foreach smallstr ($tgfname) 
				set dirname = `echo $smallstr | tr '[A-Z]' '[a-z]'` 
			end

			cd ${targetfplace}

			#-- ファイル配置ディレクトリ構成作成
			set tgdir = "statistics/${storeyesr}${storeexam}/${dirname}/${storeyesr}/${storeexam}"
			if (! -d ${tgdir}) then
				mkdir -p ${tgdir}
				chmod -R 775 ${tgdir}
			endif

			#-- ファイルの移動
			(ls ${tgfname}${storeexam}* > temmp.log) >& /dev/null
			set temmpsount = `cat temmp.log`
			\rm -f temmp.log
			if ($#temmpsount != 0) then
				echo " mvコマンド実行 ${tgfname}${storeexam}* ${tgdir}"
				\mv -f ${tgfname}${storeexam}* ${tgdir}
			endif

			#-- zipファイルの作成
			cd ${tgdir}
			if(${tgfname} == "GOKAKUHYOKA") then
				zip ${tgfname}${storeexam}0100pdf.zip ${tgfname}${storeexam}01*.pdf
				zip ${tgfname}${storeexam}0200pdf.zip ${tgfname}${storeexam}02*.pdf
				zip ${tgfname}${storeexam}0300pdf.zip ${tgfname}${storeexam}03*.pdf
				zip ${tgfname}${storeexam}0400pdf.zip ${tgfname}${storeexam}04*.pdf
				zip ${tgfname}${storeexam}0500pdf.zip ${tgfname}${storeexam}05*.pdf
			else
				zip ${tgfname}${storeexam}0000pdf.zip ${tgfname}${storeexam}*.pdf
			endif
		endif
		@ nums ++
	end

	cd ${targetfplace}/statistics/${storeyesr}${storeexam}
	echo " "
	#-- 転送用ファイルの作成
	echo " 展開用ファイルの作成：statistics${storeyesr}${storeexam}.tar "
	if (-f statistics${storeyesr}${storeexam}.tar) then
		\rm -f statistics${storeyesr}${storeexam}.tar
	endif
	tar -cf statistics${storeyesr}${storeexam}.tar *

	#--- FTP転送　作成ファイルを検証機へアップ
	echo " "
	echo "--- ファイル転送開始..."
	/usr/bin/rsh satu01test -l kei-navi /home/kei-navi/dataupscript/dataup.sh ${targetfplace}/statistics/${storeyesr}${storeexam} statistics${storeyesr}${storeexam}.tar /keinavi/freemenu/statistics 10.1.4.177
	if ($status != 0) then
		\rm -f ${targetfplace}/statistics/${storeyesr}${storeexam}/statistics${storeyesr}${storeexam}.tar
		echo "  "
		@ count ++
		continue
	else
		echo "--- 統計集追加PDFデータファイルの同期開始..."
		/usr/bin/rsh satu01test -l kei-navi /usr/local/etc/rsync_keinavi_DLPDF.sh
		echo "...統計集追加PDFデータファイルの同期終了 ---"
	endif
	echo "--- PDFファイル 検証機展開処理完了 ---"
	echo " "


#	if ($targets =~ "#"*) then
#		set exam = `expr $targets : ".*\(..\)"`
#		echo " 模試 $exam "
#	else
#		(ls ${targets}${exam}* > out.log) >& /dev/null
#		set flists = `cat out.log`
#		\rm -f out.log
#		if ($#flists != 0) then
#			echo "ファイルあり"
#		endif
#	endif
	@ count ++
end

\rm -f ${file}

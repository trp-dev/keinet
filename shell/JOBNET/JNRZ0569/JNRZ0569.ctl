------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 大学マスタ公表用選択グループ教科
--  テーブル名      : UNIVMASTER_SELECTGPCOURSE_PUB
--  データファイル名: UNIVMASTER_SELECTGPCOURSE_PUB
--  機能            : 大学マスタ公表用選択グループ教科をデータファイルの内容に置き換える
--  作成日          : 2010/02/10
--  修正日          : 
--  備考            : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE UNIVMASTER_SELECTGPCOURSE_P_T
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    EVENTYEAR,
    SCHOOLDIV,
    UNIVCD,
    EXAMDIV,
    ENTEXAMDIV,
    UNICDBRANCHCD     CHAR "LPAD(:UNICDBRANCHCD, 3, '0')",
    SELECTG_NO,
    COURSECD,
    SGPERSUBSUBCOUNT
)

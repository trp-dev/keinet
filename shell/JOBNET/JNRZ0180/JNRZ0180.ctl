------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 志望大学評価別人数（全国）
--  テーブル名      : RATINGNUMBER_A
--  データファイル名: RATINGNUMBER_A
--  機能            : 志望大学評価別人数（全国）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE RATINGNUMBER_A_TMP
(  
   EXAMYEAR             POSITION(01:04)   CHAR,
   EXAMCD               POSITION(05:06)   CHAR,
   RATINGDIV            POSITION(07:07)   CHAR,
   UNIVCOUNTINGDIV      POSITION(08:08)   CHAR,
   UNIVCD               POSITION(09:12)   CHAR,
   FACULTYCD            POSITION(13:14)   CHAR,
   DEPTCD               POSITION(15:16)   CHAR,
   AGENDACD             POSITION(17:17)   CHAR "NVL(:AGENDACD,' ')",
   STUDENTDIV           POSITION(18:18)   CHAR,
   AVGDEVIATION         POSITION(19:22)   INTEGER EXTERNAL "DECODE(:AVGDEVIATION,          '9999', null, :AVGDEVIATION/10)",
   AVGSCORERATE         POSITION(23:26)   INTEGER EXTERNAL "DECODE(:AVGSCORERATE,          '9999', null, :AVGSCORERATE/10)",
   TOTALCANDIDATENUM    POSITION(27:34)   INTEGER EXTERNAL "NULLIF(:TOTALCANDIDATENUM, '99999999')",
   FIRSTCANDIDATENUM    POSITION(35:42)   INTEGER EXTERNAL "NULLIF(:FIRSTCANDIDATENUM, '99999999')",
   RATINGNUM_A          POSITION(43:50)   INTEGER EXTERNAL "NULLIF(:RATINGNUM_A,       '99999999')",
   RATINGNUM_B          POSITION(51:58)   INTEGER EXTERNAL "NULLIF(:RATINGNUM_B,       '99999999')",
   RATINGNUM_C          POSITION(59:66)   INTEGER EXTERNAL "NULLIF(:RATINGNUM_C,       '99999999')",
   RATINGNUM_D          POSITION(67:74)   INTEGER EXTERNAL "NULLIF(:RATINGNUM_D,       '99999999')",
   RATINGNUM_E          POSITION(75:82)   INTEGER EXTERNAL "NULLIF(:RATINGNUM_E,       '99999999')"
)


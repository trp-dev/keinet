UPDATE PACTSCHOOL
   SET PACTDIV        = ?
     , MAXSESSION     = ?
     , PACTTERM       = ?
     , FEATPROVFLG    = ?
     , KO_MAXSESSION  = ?
 WHERE 1=1
   AND USERID = ?

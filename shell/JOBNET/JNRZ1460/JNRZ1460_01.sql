-- ************************************************
-- *機 能 名 : CEFR情報
-- *機 能 ID : JNRZ1460_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE CEFR_INFO_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE CEFR_INFO_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM CEFR_INFO WHERE 0=1;

-- 主キーの設定
ALTER TABLE CEFR_INFO_TMP ADD CONSTRAINT PK_CEFR_INFO_TMP PRIMARY KEY 
(
   EVENTYEAR
 , EXAMDIV
 , CEFRLEVELCD
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

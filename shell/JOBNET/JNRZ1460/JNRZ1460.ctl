------------------------------------------------------------------------------
-- *機 能 名 : CEFR情報のCTLファイル
-- *機 能 ID : JNRZ1460.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE CEFR_INFO_TMP
(  
   EVENTYEAR          POSITION(01:04)  CHAR 
,  EXAMDIV            POSITION(05:06)  CHAR 
,  CEFRLEVELCD        POSITION(07:08)  CHAR 
,  CERFLEVELNAME      POSITION(09:12)  INTEGER EXTERNAL 
,  CERFLEVELNAME_ABBR POSITION(13:14)  INTEGER EXTERNAL 
,  SORT               POSITION(15:16)  INTEGER EXTERNAL 
)

------------------------------------------------------------------------------
-- *機 能 名 : 英語認定試験_出願要件マスタのCTLファイル
-- *機 能 ID : JNRZ1470.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE ENGPT_APPREQ_TMP
(  
   EVENTYEAR          POSITION(01:04)  CHAR 
 , EXAMDIV            POSITION(05:06)  CHAR 
 , UNIVSINGLECD       POSITION(07:10)  CHAR 
 , APP_REQ_PATTERN_CD POSITION(11:13)  CHAR 
 , NEXTYEARDIV        POSITION(14:14)  CHAR 
 , APP_REQ_USE_DIV    POSITION(15:15)  CHAR 
 , CEFRLEVELCD        POSITION(16:17)  CHAR 
 , ELIGIBILITYCD      POSITION(18:19)  CHAR 
 , APP_REQ_IN_DIV     POSITION(20:20)  CHAR 
 , SCORE              POSITION(21:25)  INTEGER EXTERNAL "DECODE(:SCORE,              '99999', null, :SCORE/10)"
)

-- *********************************************************
-- *機 能 名 : 英語認定試験_出願要件マスタ
-- *機 能 ID : JNRZ1470_02.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM ENGPT_APPREQ MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM ENGPT_APPREQ_TMP TMP
         WHERE 1=1
           AND MAIN.EVENTYEAR           = TMP.EVENTYEAR
           AND MAIN.EXAMDIV             = TMP.EXAMDIV
           AND MAIN.UNIVSINGLECD        = TMP.UNIVSINGLECD
           AND MAIN.APP_REQ_PATTERN_CD  = TMP.APP_REQ_PATTERN_CD
           AND MAIN.NEXTYEARDIV         = TMP.NEXTYEARDIV
           AND MAIN.ELIGIBILITYCD       = TMP.ELIGIBILITYCD
       )
;

-- テンポラリから本物へ登録
INSERT INTO ENGPT_APPREQ SELECT * FROM ENGPT_APPREQ_TMP;

-- テンポラリテーブル削除
DROP TABLE ENGPT_APPREQ_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

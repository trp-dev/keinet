-- ************************************************
-- *機 能 名 : 英語認定試験_出願要件マスタ
-- *機 能 ID : JNRZ1470_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE ENGPT_APPREQ_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE ENGPT_APPREQ_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM ENGPT_APPREQ WHERE 0=1;

-- 主キーの設定
ALTER TABLE ENGPT_APPREQ_TMP ADD CONSTRAINT PK_ENGPT_APPREQ_TMP PRIMARY KEY 
(
   EVENTYEAR
 , EXAMDIV
 , UNIVSINGLECD
 , APP_REQ_PATTERN_CD
 , NEXTYEARDIV
 , ELIGIBILITYCD
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

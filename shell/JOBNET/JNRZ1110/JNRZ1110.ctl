------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : ガイドライン備考マスタ
--  テーブル名      : GUIDEREMARKS_TMP
--  データファイル名: GUIDEREMARKSyyyymmdd.csv
--  機能            : ガイドライン備考マスタをデータファイルの内容に置き換える
--  作成日          : 2004/08/25
--  修正日          : 2004/09/10
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE GUIDEREMARKS_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    UNIVCD,
    FACULTYCD "DECODE(LENGTH(:FACULTYCD), 1, CONCAT('0', :FACULTYCD), 2, :FACULTYCD)",
    DEPTCD    "DECODE(LENGTH(:DEPTCD),    1, CONCAT('0', :DEPTCD),    2, :DEPTCD)",
    REMARKS
)

-- ************************************************
-- *機 能 名 : ガイドライン備考マスタ
-- *機 能 ID : JNRZ1110_01.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE GUIDEREMARKS_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE GUIDEREMARKS_TMP(
         UNIVCD         CHAR(4)             NOT NULL,   /*  大学コード      */
         FACULTYCD      CHAR(2)             NOT NULL,   /*  学部コード      */
         DEPTCD         CHAR(2)             NOT NULL,   /*  学科コード      */
         REMARKS        VARCHAR2(100)               ,   /*  備考            */
    CONSTRAINT PK_GUIDEREMARKS_TMP PRIMARY KEY(
        UNIVCD,                                         /*  大学コード      */
        FACULTYCD,                                      /*  学部コード      */
        DEPTCD                                          /*  学科コード      */
    )
    USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

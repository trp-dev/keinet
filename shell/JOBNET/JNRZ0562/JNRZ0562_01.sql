-- ************************************************
-- *機 能 名 : 大学マスタ公表用志望校
-- *機 能 ID : JNRZ0562_01.sql
-- *作 成 日 : 2010/02/10
-- *作 成 者 : 
-- *更 新 日 : 
-- *更新内容 : 
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE UNIVMASTER_CHOICESCHOOL_PUB_TM CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE UNIVMASTER_CHOICESCHOOL_PUB_TM  (
       EVENTYEAR                      CHAR(4)                NOT NULL,   /*   行事年度                    */
       SCHOOLDIV                      CHAR(1)                NOT NULL,   /*   学校区分                    */
       UNIVCD                         CHAR(10)               NOT NULL,   /*   大学コード                  */
       EXAMDIV                        CHAR(2)                NOT NULL,   /*   模試区分                    */
       ENTEXAMDIV                     CHAR(1)                NOT NULL,   /*   入試試験区分                */
       UNICDBRANCHCD                  CHAR(3)                NOT NULL,   /*   大学コード枝番              */
       TOTALALLOTPNT                  NUMBER(4,0)            NOT NULL,   /*   総合配点                    */
       TOTALALLOTPNT_NREP             NUMBER(4,0)            NOT NULL,   /*   総合配点＿小論文なし        */
       ENTEXAMFULLPNT                 NUMBER(4,0)            NOT NULL,   /*   入試満点値                  */
       ENTEXAMFULLPNT_NREP            NUMBER(4,0)            NOT NULL,   /*   入試満点値＿小論文なし      */
       ENTCOURSETYPE                  NUMBER(1,0)                    ,   /*   入試科目教科数型            */
       ENTSUBJECTTYPE                 NUMBER(1,0)                    ,   /*   入試科目科目数型            */
       SUBCOUNT                       VARCHAR2(200)          NOT NULL,   /*   判定科目名                  */
       ENTSUBTYPENAME                 VARCHAR2(120)                  ,   /*   入試科目型名                */
       ENTSUBTYPECODE                 CHAR(4)                        ,   /*   入試科目型コード            */
       SCORE_CONV_PATTERN_ENGPT       VARCHAR2(3)                    ,   /*   得点換算パターンコード－英語認定試験 */
       ADDPT_DIV_ENGPT                VARCHAR2(1)                    ,   /*   加点区分－英語認定試験      */
       ADDPT_PT_ENGPT                 NUMBER(4,1)                    ,   /*   加点配点－英語認定試験      */
       SCORE_CONV_PATTERN_KOKUGO_DESC VARCHAR2(3)                    ,   /*   得点換算パターンコード－国語記述式 */
       ADDPT_DIV_KOKUGO_DESC          VARCHAR2(1)                    ,   /*   加点区分－国語記述式        */
       ADDPT_PT_KOKUGO_DESC           NUMBER(4,1)                    ,   /*   加点配点－国語記述式        */
       CONSTRAINT PK_UNIVMASTER_CHOICESCHOOL_P_T PRIMARY KEY (
                 EVENTYEAR       ,               /*  行事年度       */
                 SCHOOLDIV       ,               /*  学校区分       */
                 UNIVCD          ,               /*  大学コード     */
                 EXAMDIV         ,               /*  模試区分       */
                 ENTEXAMDIV      ,               /*  入試試験区分   */
                 UNICDBRANCHCD                   /*  大学コード枝番 */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

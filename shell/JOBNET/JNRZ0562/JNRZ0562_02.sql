-- *********************************************************
-- *機 能 名 : 大学マスタ公表用志望校
-- *機 能 ID : JNRZ0562_02.sql
-- *作 成 日 : 2010/02/10
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM UNIVMASTER_CHOICESCHOOL_PUB
WHERE  (EVENTYEAR,EXAMDIV)
       IN(SELECT DISTINCT EVENTYEAR,EXAMDIV
          FROM   UNIVMASTER_CHOICESCHOOL_PUB_TM);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO UNIVMASTER_CHOICESCHOOL_PUB
SELECT
  EVENTYEAR,
  SCHOOLDIV,
  SUBSTR(UNIVCD, 1, 4) AS UNIVCD,
  SUBSTR(UNIVCD, 5, 2) AS FACULTYCD,
  SUBSTR(UNIVCD, 7, 4) AS DEPTCD,
  EXAMDIV,
  ENTEXAMDIV,
  UNICDBRANCHCD,
  TOTALALLOTPNT,
  TOTALALLOTPNT_NREP,
  ENTEXAMFULLPNT,
  ENTEXAMFULLPNT_NREP,
  ENTCOURSETYPE,
  ENTSUBJECTTYPE,
  SUBCOUNT,
  ENTSUBTYPENAME,
  ENTSUBTYPECODE,
  SCORE_CONV_PATTERN_ENGPT,
  ADDPT_DIV_ENGPT,
  ADDPT_PT_ENGPT,
  SCORE_CONV_PATTERN_KOKUGO_DESC,
  ADDPT_DIV_KOKUGO_DESC,
  ADDPT_PT_KOKUGO_DESC
FROM UNIVMASTER_CHOICESCHOOL_PUB_TM;

-- 2年以上前のデータを削除
DELETE FROM UNIVMASTER_CHOICESCHOOL_PUB
WHERE  TO_NUMBER(EVENTYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 2);

-- 更新対象データの1年前のデータを削除
DELETE FROM UNIVMASTER_CHOICESCHOOL_PUB
WHERE  (EVENTYEAR,EXAMDIV)
--       IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 1)),EXAMDIV
       IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(EVENTYEAR) - 1)),EXAMDIV
          FROM   UNIVMASTER_CHOICESCHOOL_PUB_TM);

-- テンポラリテーブル削除
DROP TABLE UNIVMASTER_CHOICESCHOOL_PUB_TM CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

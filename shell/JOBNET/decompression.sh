#!/bin/sh 

###### ファイルの展開＆削除スクリプト
###### 第1パラメータ:ファイル配置パス、第2パラメータ：対象ファイル名

source ~/.bash_profile

#環境変数設定
export WORKDIR=$1
export files=$2

cd $WORKDIR

(ls *.tar.Z > tmp-out.log) >& /dev/null
export FileList=`cat tmp-out.log`
rm -f tmp-out.log

export targf=`echo $FileList | grep $files`
if [[ ! -z $targf ]]
then
	zcat $files | tar xf -
fi
rm -rf $files

#バッチ処理戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo バッチ処理でエラーが発生しました
    exit 30
fi


#正常終了
exit  0    #正常終了

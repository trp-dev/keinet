------------------------------------------------------------------------------
-- *機 能 名 : 数学記述式設問（県）のCTLファイル
-- *機 能 ID : JNRZ1290.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE MATHDESCQUE_P_TMP
(  
   EXAMYEAR    POSITION(01:04)  CHAR 
 , EXAMCD      POSITION(05:06)  CHAR 
 , PREFCD      POSITION(07:08)  CHAR 
 , SUBCD       POSITION(09:12)  CHAR 
 , QUESTION_NO POSITION(13:14)  CHAR 
 , AVGPNT      POSITION(15:18)  INTEGER EXTERNAL "DECODE(:AVGPNT,      '9999', null, :AVGPNT/10)"
)

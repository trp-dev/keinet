#! /bin/csh

### ファイルサーバデータの削除シェル
### パラメータ値：なし→スクリプト内定義、第1→削除対象定義ファイルパス、第2→削除処理定義
###               削除処理定義（1→集計・個人、2→集計のみ、3→個人のみ、4→紐付き、5→過年度）


if ($#argv < 1) then
	set para = 3
	set YEAR = (2007 2007)
	set EXAM = (37 69)
else
	set filespath = $1
	if (! -f $filespath) then
		echo "削除定義ファイル($filespath) が存在しません"
		exit
	endif

	set para = $2
	set EXAM = `cat $filespath`
#	set divfile = "/keinavi/JOBNET/examcd-div.csv"
endif	


set count = 1
if ($para == 1) then
	##---- 集計・個表データのみ削除する場合
	while ($count <= $#EXAM)
		if ($#argv < 1) then
			echo "  集計・個人データの削除開始 年度:$YEAR[$count]  模試コード: $EXAM[$count]"
			sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-all.sql $YEAR[$count] $EXAM[$count]
			sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-indiv.sql $YEAR[$count] $EXAM[$count]
		else
	                set targetyexam = $EXAM[$count]
			set delyear = `echo $targetyexam | awk '{printf "%-.4s\n",$0}'`
			set delexam = `expr $targetyexam : ".*\(..\)"`
			echo "  集計・個人データの削除開始 年度:$delyear  模試コード: $delexam"
			sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-all.sql $delyear $delexam
			sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-indiv.sql $delyear $delexam
			
		endif
		@ count ++
	end

else if ($para == 2) then
	##---- 集計データのみ削除する場合
	while ($count <= $#EXAM)
		if ($#argv < 1) then
			echo "  集計データの削除開始 年度:$YEAR[$count]  模試コード: $EXAM[$count]"
			sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-all.sql $YEAR[$count] $EXAM[$count]
		else
	                set targetyexam = $EXAM[$count]
			set delyear = `echo $targetyexam | awk '{printf "%-.4s\n",$0}'`
			set delexam = `expr $targetyexam : ".*\(..\)"`

			echo "  集計データの削除開始 年度:$delyear  模試コード: $delexam"
			sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-all.sql $delyear $delexam
		endif
		@ count ++
	end

else if ($para == 3) then
	##---- 個表データのみ削除する場合
	while ($count <= $#EXAM)
		if ($#argv < 1) then
			echo "  個人データの削除開始 年度:$YEAR[$count]  模試コード: $EXAM[$count]"
			sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-indiv.sql $YEAR[$count] $EXAM[$count]
		else
	                set targetyexam = $EXAM[$count]
			set delyear = `echo $targetyexam | awk '{printf "%-.4s\n",$0}'`
			set delexam = `expr $targetyexam : ".*\(..\)"`
			echo "  個人データの削除開始 年度:$delyear  模試コード: $delexam"
			sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-indiv.sql $delyear $delexam
		endif
		@ count ++
	end

else if ($para == 4) then
	##---- 紐付きテーブルを削除する場合(現在ファイルより模試を取得する処理[パラメータあり]のみ)
	while ($count <= $#EXAM)
		set targetyexam = $EXAM[$count]
		set delyear = `echo $targetyexam | awk '{printf "%-.4s\n",$0}'`
		set delexam = `expr $targetyexam : ".*\(..\)"`

		#--- 模試コードより対応模試区分コードを取得
		set ecdanddiv = `grep "^$delexam" $divfile`
		if ($#ecdanddiv != 0) then
			set examdiv = `echo $ecdanddiv | awk -F, '{print $2}'`
			sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/del-univcdhookup.spl $delyear $examdiv	
		endif
		@ count ++
	end

else if ($para == 5) then
	##---- 過年度データを削除する場合

endif

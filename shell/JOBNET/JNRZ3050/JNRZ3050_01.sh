#! /bin/sh

export year=$1
export exam=$2

#作成フォルダから検証用フォルダへコピー
if [ -d /keinavi/freemenu/validation/univresult/${year}/${exam} ]
then
        rm -rf /keinavi/freemenu/validation/univresult/${year}/${exam}
fi
if [ -d /keinavi/bat/FreeMenu/F002_5/univresult/${year}/${exam} ]
then
	if [[ ! -d /keinavi/freemenu/validation/univresult/${year} ]]
	then
		mkdir /keinavi/freemenu/validation/univresult/${year}
		chown orauser /keinavi/freemenu/validation/univresult/${year}/
		chgrp ora_ins_group /keinavi/freemenu/validation/univresult/${year}/

		chmod 777 /keinavi/bat/FreeMenu/F002_5/univresult/${year}
	fi

	chown -R orauser /keinavi/bat/FreeMenu/F002_5/univresult/${year}/${exam}
	chgrp -R ora_ins_group /keinavi/bat/FreeMenu/F002_5/univresult/${year}/${exam}
	chmod 775 /keinavi/bat/FreeMenu/F002_5/univresult/${year}/${exam}
        cp -rp /keinavi/bat/FreeMenu/F002_5/univresult/${year}/${exam} /keinavi/freemenu/validation/univresult/${year}/

	
	#--- 過年度データ削除
	export old2year=`expr $year - 2`
	if [ -d /keinavi/freemenu/validation/univresult/${old2year}/${exam} ]
	then
		\rm -rf /keinavi/freemenu/validation/univresult/${old2year}/${exam}
		echo " "
		echo "--- 検証機　過年度データ削除　${old2year}/${exam} ---"
	fi
fi


#! /bin/csh 

###### 統計集データ（志望大学学部学科別成績分布のみ）作成バッチ
###### マーク/記述・私大模試のみ対象

set basedir = "/keinavi/HULFT/dsud_files"
set markdir = "${basedir}/marktmpdir"
set kizyutudir = "${basedir}/descriptmpdir"

set kdcdir = "/home/navikdc/Statistics"
set storeconf = "storedata.csv"
set file = "${kdcdir}/${storeconf}"
set tarfile = "DL-UNIV.tar.Z"

#----定義ファイルの有無確認
if (! -f ${file}) then
	echo "--- 登録対象模試コードの記入ファイルが存在しません ---"
	echo "--- 登録が必要な場合は定義ファイル(storedata.csv)を配置して再度行ってください ---"
	exit 1
endif

set lines = `wc -w ${file}`
set flags = 1
set count = 1
while ($count <= $lines[1]) 
	set year = `awk -F, NR==${count}'{print $1}' $file`
	set exam = `awk -F, NR==${count}'{print $2}' $file`
	set mkflagtmp = `awk -F, NR==${count}'{print $3}' $file`
	set mkflag = `echo ${mkflagtmp} | tr -d '\r\n'`

	if ($mkflag != 5) then
		@ count ++
		continue
	endif

	#----登録対象模試の判別
	if (($exam == 01)||($exam == 02)||($exam == 03)||($exam == 04)||($exam == 66)) then
		if (-d $markdir) then
			set servdir = $markdir
			@ flags ++
		else 
			echo "--- 登録対象のファイルが存在しません"
			@ count ++
			continue
		endif
	else if (($exam == 05 )||($exam == 06)||($exam == 07)||($exam == 09)||($exam == 10)||($exam == 67)) then
		if (-d $kizyutudir) then
			set servdir = $kizyutudir
			@ flags ++
		else
			echo "--- 登録対象のファイルが存在しません"
			@ count ++
			continue
		endif
	endif

	cd ${basedir}
	echo "${basedir}"
	#---- ファイル保存フォルダの確認（既に存在する場合はバックアップ）
	if (-d ${basedir}/${year}/${exam}) then
		mv ${basedir}/$year/$exam ${basedir}/${year}/${exam}-old
	endif
	mkdir -p ${basedir}/${year}/${exam}
	cp -p $servdir/* ${basedir}/${year}/${exam} #--- 処理失敗した時のためにコピーを作成

	echo "---- 対象ファイルの圧縮"
	tar -Zcvf $tarfile ${year}/${exam}/DSUD*

	#----対象ファイルをファイルサーバへFTP転送
	echo "---- 対象ファイルのファイルサーバ転送開始"
	/keinavi/HULFT/ftp.sh /keinavi/bat/FreeMenu/F002_5/input $basedir $tarfile 10.1.4.177
	#----対象ファイルの展開
	/usr/bin/rsh satu04 /keinavi/JOBNET/decompression.sh /keinavi/bat/FreeMenu/F002_5/input $tarfile
	if ($status != 0) then
		echo "--- ERROR：リモートシェル実行（転送ファイルの展開）に失敗しました ---"
		rm $tarfile
		\rm -rf ${basedir}/${year}/${exam}
		exit 30
	endif
	rm $tarfile
	@ count ++
end

if ($flags != 1) then
	#----定義データ登録
	cd /keinavi/JOBNET/MkStatistics
	/keinavi/JOBNET/MkStatistics/MkStatistics.sh
	if ($status != 0) then
		echo " "
		echo "--- ERROR：定義ファイルの読み込みに失敗しました。定義ファイルを再度確認してください ---"
		\rm -rf ${basedir}/${year}/${exam}
		exit 30
	endif

	#----リモートシェルを実行
	/usr/bin/rsh satu04 -l root /keinavi/bat/FreeMenu/F002_5/f002_5batch.sh

	/usr/bin/rsh satu04 -l root /keinavi/JOBNET/JNRZ3050/JNRZ3050_01.sh ${year} ${exam}

	rm -f /home/navikdc/${storeconf}
	\rm -rf $servdir
endif

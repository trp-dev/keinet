#! /bin/csh

##### 過年度の志望大学評価別人数のデータ登録バッチ

set DIR = /keinavi/JOBNET
set LogoutstrFile = ${DIR}/StoringExam

set temp = /keinavi/HULFT/WorkDir
set nowdate = `date +%Y%m%d`

##-- OP系のファイルが存在するときは、一連の流れでの起動のため、
##-- その定義ファイルを優先にする。
if (-f ${DIR}/PreviousOPRating) then
	#-- 私大・OP系を起動
	set TargetFName = ${DIR}/PreviousOPRating
else
	#-- マーク・記述系を起動
	set TargetFName = ${DIR}/PreviousRating
endif

#-- バッチ起動前提ファイル
set lockf1 = "/home/navikdc/datastore/release/store.lck"
#set lockf2 = "/home/navikdc/datastore/inside_release/store.lck"
set nowfile = "/home/navikdc/datastore/release/$nowdate"


#-- 登録定義ファイルの確認 
if (! -f $TargetFName) then
	echo "---- 登録定義ファイルが存在しません ----"
	exit	
endif


#-- データ登録中または登録設定がないことを確認する
if ((-f $lockf1)||(-f $nowfile)) then

	echo "データ登録中または登録予約があるため、15分処理を待つ..."
	sleep 900

	if ((-f $lockf1)||(-f $nowfile)) then
		#-- データ登録または登録設定時は処理の見送り
		echo "---- Parallel Processing ERROR  ----"
		echo " データ登録中または登録予約があるため、過年度の志望大学評価別人数データ登録は見送ります "
		chmod 777 $TargetFName

		exit
	endif

	echo " "
	echo " 処理を開始します。"
	echo " "
endif

#-- ロックファイル作成
echo "/keinavi/HULFT/NewEXAM" > ${lockf1}
chmod 664 ${lockf1}

set alltargetexam = `cat $TargetFName`
\rm -f $TargetFName

set examcount = 1
while ($examcount <= $#alltargetexam)
	#-- 実行時間制御のための現在時間取得
	set nhtimes = `date +%H`

	set targetexamcd = $alltargetexam[$examcount]

	#-- 前から4文字取得し、模試年度を取得する	
	set yearoldstr = `echo $targetexamcd | awk '{printf "%-.4s\n",$0}'`
	#-- 前から4文字を削除し、模試コードを取得する
	set examstr = `echo $targetexamcd | sed -e s'/^[0-9]\{4\}//'g`
	#-- 取得した模試コードより、前2文字、後ろ2文字を取得し、高12系模試かの確認をする。
	set checkexam1 = `echo $examstr | awk '{printf "%-.2s\n",$0}'`
	set checkexam2 = `expr $examstr : ".*\(..\)"`
	if ($checkexam1 == $checkexam2) then
		set targetexams = $checkexam1
	else 
		set targetexams = ($checkexam1 $checkexam2)
	endif

	#-- 模試による処理区別
#	if (($examstr == 01)||($examstr == 02)||($examstr == 03)||($examstr == 04)||($examstr == 05)||($examstr == 06)||($examstr == 07)||($examstr == 38)||($examstr == 61_71)||($examstr == 62_72)||($examstr == 63_73)||($examstr == 65_75)) then
#	if (($examstr == 01)||($examstr == 02)||($examstr == 03)||($examstr == 04)||($examstr == 05)||($examstr == 06)||($examstr == 07)||($examstr == 38)||($examstr == 61_71)||($examstr == 62_72)||($examstr == 63_73)||($examstr == 65)||($examstr == 75)) then
	if (($examstr == 01)||($examstr == 02)||($examstr == 03)||($examstr == 04)||($examstr == 05)||($examstr == 06)||($examstr == 07)||($examstr == 38)||($examstr == 61)||($examstr == 62)||($examstr == 63)||($examstr == 65)||($examstr == 71)||($examstr == 72)||($examstr == 73)||($examstr == 74)||($examstr == 75)) then
		echo "${yearoldstr}${examstr} データの登録"
		
		#-- マーク・記述の時は登録開始が4時以降の場合は見送り
		#if ((04 < $nhtimes)&&($nhtimes < 18)) then
		#	echo "${yearoldstr}${examstr} データの登録を次回に見送ります。"
		#	echo $targetexamcd >> $TargetFName
		#	chmod 777 $TargetFName
		#
		#	@ examcount ++
		#	continue
		#endif

	else
		echo "${yearoldstr}${examstr} データの登録"
		#-- マーク・記述以外の時は登録開始が6時以降の場合は見送り
		#if ((06 < $nhtimes)&&($nhtimes < 15)) then
		#	echo "${yearoldstr}${examstr} データの登録を次回に見送ります。"
		#	echo $targetexamcd >> $TargetFName
		#	chmod 777 $TargetFName
		#
		#	@ examcount ++
		#	continue
		#endif
	endif


	echo "--- 統合過年度＆過年度志望大学評価別人数のデータ登録開始...　対象模試：${yearoldstr}${examstr}"

	##対象模試(必須設定)
	set EXAM = ${yearoldstr}${examstr}

	echo $EXAM > $LogoutstrFile


	############ ファイルコピーの有無に注意 ######################
	if (-f /keinavi/HULFT/NewEXAM/${EXAM}/HS3_UNIV_NAME) then
		mv /keinavi/HULFT/NewEXAM/${EXAM}/HS3_UNIV_NAME $temp/
	endif
	cp -p /keinavi/HULFT/NewEXAM/$EXAM/*[^-old] $temp/


	## 志望大学評価別（高校・クラス）の削除 過年度データ投入用
	if (((-f $temp/RATINGNUMBER_P)&&(-f $temp/RATINGNUMBER_C))&&((-f /keinavi/HULFT/NewEXAM/$EXAM/RATINGNUMBER_P)&&(-f /keinavi/HULFT/NewEXAM/$EXAM/RATINGNUMBER_C))) then
		set loopcount = 1
		while ($loopcount <= $#targetexams)
echo "$yearoldstr $targetexams[$loopcount]"
			sqlplus keinavi/39ra8ma@S11DBA_BATCH2 @/home/ora_ins_account/DBscript/DBdelete/delete-RATING.sql $yearoldstr $targetexams[$loopcount]
			@ loopcount ++
		end
	endif


	#HS3_UNIV_NAME
	if (-f $temp/HS3_UNIV_NAME-lastyear00) then
		mv $temp/HS3_UNIV_NAME $temp/HS3_UNIV_NAME-temp
		mv $temp/HS3_UNIV_NAME-lastyear00 $temp/HS3_UNIV_NAME
		echo " "
		echo "過年度高３大学マスタ名称 データの登録を開始します"
		cd /keinavi/JOBNET/JNRZ0550
		./JNRZ0550-lastyear00.sh
		if (($status == 30)&&(-f JNRZ0550.bad)) then
                        cp -p JNRZ0550.log JNRZ0550-${EXAM}.log
			echo "---- 過年度高３大学マスタ名称 データ登録エラー ----"
                endif
		mv $temp/HS3_UNIV_NAME-temp $temp/HS3_UNIV_NAME
	endif
	if (-f $temp/HS3_UNIV_NAME-lastdiv) then
		mv $temp/HS3_UNIV_NAME $temp/HS3_UNIV_NAME-temp
		mv $temp/HS3_UNIV_NAME-lastdiv $temp/HS3_UNIV_NAME
		echo " "
		echo "前区分の高３大学マスタ名称 データの登録を開始します"
		cd /keinavi/JOBNET/JNRZ0550
		./JNRZ0550-lastyear00.sh
		if (($status == 30)&&(-f JNRZ0550.bad)) then
                        cp -p JNRZ0550.log JNRZ0550-${EXAM}.log
			echo "---- 前区分高３大学マスタ名称 データ登録エラー ----"
                endif
		mv $temp/HS3_UNIV_NAME-temp $temp/HS3_UNIV_NAME
	endif
	if (-f $temp/HS3_UNIV_NAME) then
		echo " "
		echo "高３大学マスタ名称 データの登録を開始します"
		cd /keinavi/JOBNET/JNRZ0550
		./JNRZ0550-lastyear00.sh
		if (($status == 30)&&(-f JNRZ0550.bad)) then
                        cp -p JNRZ0550.log JNRZ0550-${EXAM}.log
			echo "---- 高３大学マスタ名称 データ登録エラー ----"
                endif
	endif
	if (-f $temp/KO_HS3_UNIV_NAME) then
		echo " "
		echo "校内成績処理用 高３大学マスタ名称 データの登録を開始します"
		cd /keinavi/JOBNET/JNRZ0550-KO
		./JNRZ0550-KO.sh
		if (($status == 30)&&(-f JNRZ0550.bad)) then
                        cp -p JNRZ0550.log JNRZ0550-${EXAM}.log
			echo "---- 校内成績処理用 高３大学マスタ名称 データ登録エラー ----"
                endif
	endif
	if (-f $temp/KO_UNIVCDTRANS_ORD) then
		echo " "
		echo "校内成績処理用 大学コード順引き データの登録を開始します"
		cd /keinavi/JOBNET/JNRZ0600-KO
		./JNRZ0600-KO.sh
		if (($status == 30)&&(-f JNRZ0600.bad)) then
                        cp -p JNRZ0600.log JNRZ0600-${EXAM}.log
			echo "---- 校内成績処理用 大学コード順引き データ登録エラー ----"
                endif
	endif

	#RATINGNUMBER_S
	if (-f $temp/RATINGNUMBER_S) then
		echo " "
		echo "志望大学評価別人数（高校）データの登録を開始します"
		cd /keinavi/JOBNET/JNRZ0201
		if (-f JNRZ0201.bad) then
			\rm JNRZ0201.bad
		endif
		./JNRZ0201.sh
		if (($status == 30)&&(-f JNRZ0201.bad)) then
			cp -p JNRZ0201.log JNRZ0201-${EXAM}.log
		endif
	endif

	#RATINGNUMBER_C
	if (-f $temp/RATINGNUMBER_C) then
		echo " "
		echo "志望大学評価別人数（クラス）データの登録を開始します"
		cd /keinavi/JOBNET/JNRZ0211
		if (-f JNRZ0211.bad) then
			\rm JNRZ0211.bad
		endif
		./JNRZ0211.sh
		if (($status == 30)&&(-f JNRZ0211.bad)) then
			cp -p JNRZ0211.log JNRZ0211-${EXAM}.log
		endif
	endif

	#RATINGNUMBER_A
	if (-f $temp/RATINGNUMBER_A) then
		echo " "
		echo "志望大学評価別人数（全国）データの登録を開始します"
		cd /keinavi/JOBNET/JNRZ0181
		if (-f JNRZ0181.bad) then
			\rm JNRZ0181.bad
		endif
		./JNRZ0181.sh
		if (($status == 30)&&(-f JNRZ0181.bad)) then
			cp -p JNRZ0181.log JNRZ0181-${EXAM}.log
		endif
	endif
        
	#RATINGNUMBER_P
	if (-f $temp/RATINGNUMBER_P) then
		echo " "
		echo "志望大学評価別人数（県）データの登録を開始します"
		cd /keinavi/JOBNET/JNRZ0191
		if (-f JNRZ0191.bad) then
			\rm JNRZ0191.bad
		endif
		./JNRZ0191.sh
		if (($status == 30)&&(-f JNRZ0191.bad)) then
			cp -p JNRZ0191.log JNRZ0191-${EXAM}.log
		endif
	endif

	#EXAMTAKENUM_S
	if (-f $temp/EXAMTAKENUM_S) then
		echo " "
		echo "模試受験者総数（高校）データの登録を開始します"
		cd /keinavi/JOBNET/JNRZ0030
		if (-f JNRZ0030.bad) then
			\rm JNRZ0030.bad
		endif
		./JNRZ0030.sh
		if (($status == 30)&&(-f JNRZ0030.bad)) then
			cp -p JNRZ0030.log JNRZ0030-${EXAM}.log
		endif
	endif
	
	#SUBRECORD_S
	if (-f $temp/SUBRECORD_S) then
		echo " "
		echo "科目別成績（高校）データの登録を開始します"
		cd /keinavi/JOBNET/JNRZ0070
		if (-f JNRZ0070.bad) then
			\rm JNRZ0070.bad
		endif
		./JNRZ0070.sh
		if (($status == 30)&&(-f JNRZ0070.bad)) then
			cp -p JNRZ0070.log JNRZ0070-${EXAM}.log
		endif
	endif
	
	#SUBDISTRECORD_S
	if (-f $temp/SUBDISTRECORD_S) then
		echo " "
		echo "科目別分布別成績（高校）データの登録を開始します"
		cd /keinavi/JOBNET/JNRZ0110
		if (-f JNRZ0110.bad) then
			\rm JNRZ0110.bad
		endif
		./JNRZ0110.sh
		if (($status == 30)&&(-f JNRZ0110.bad)) then
			cp -p JNRZ0110.log JNRZ0110-${EXAM}.log
		endif
	endif

	echo "--- ${yearoldstr}${examstr} 統合過年度＆過年度志望大学評価別人数のデータ登録完了 ---"
	echo " "

	#--- データフォルダ移動
	if (-d /keinavi/HULFT/NewEXAM/$EXAM) then
		if (! -d /keinavi/HULFT/DATA/${EXAM}/after) then
			mkdir /keinavi/HULFT/DATA/${EXAM}/after
		endif
		mv /keinavi/HULFT/NewEXAM/$EXAM /keinavi/HULFT/DATA/${EXAM}/after/${EXAM}tougo
	endif

	@ examcount ++

end

if (-f ${lockf1}) then
	\rm -f ${lockf1}
endif

if (-f ${LogoutstrFile}) then
	\rm -f ${LogoutstrFile}
endif

------------------------------------------------------------------------------
--  システム名      : KEI-NAVI
--  概念ファイル名  : 設問成績層別成績（全国）
--  テーブル名      : QRECORDZONE_A
--  データファイル名: QRECORDZONE_A
--  機能            : 設問成績層別成績（全国）をデータファイルの内容に置き換える
--  作成日          : 2007/08/10
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE QRECORDZONE_A_TMP
(  
   EXAMYEAR        POSITION(01:04)  CHAR,
   EXAMCD          POSITION(05:06)  CHAR,
   SUBCD           POSITION(07:10)  CHAR,
   QUESTION_NO     POSITION(11:12)  INTEGER EXTERNAL,
   DEVZONECD       POSITION(13:13)  CHAR,
   AVGPNT          POSITION(14:18)  INTEGER EXTERNAL "DECODE(:AVGPNT,         '99999', null, :AVGPNT/10)",
   NUMBERS         POSITION(19:26)  INTEGER EXTERNAL "NULLIF(:NUMBERS,     '99999999')",
   AVGSCORERATE    POSITION(27:30)  INTEGER EXTERNAL "DECODE(:AVGSCORERATE,    '9999', null, :AVGSCORERATE/10)"
)


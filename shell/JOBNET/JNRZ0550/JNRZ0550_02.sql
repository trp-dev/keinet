-- *********************************************************
-- *機 能 名 : 高３大学マスタ（志望用名称）
-- *機 能 ID : JNRZ0550_02.sql
-- *作 成 日 : 2004/08/31
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM HS3_UNIV_NAME
WHERE  (EXAMYEAR,EXAMDIV)
       IN(SELECT DISTINCT EXAMYEAR,EXAMDIV
          FROM   HS3_UNIV_NAME_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO HS3_UNIV_NAME
SELECT * FROM HS3_UNIV_NAME_TMP;

-- 8年以上前のデータを削除
DELETE FROM HS3_UNIV_NAME
WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 8);

-- 更新対象データの7年前のデータを削除
DELETE FROM HS3_UNIV_NAME
WHERE  (EXAMYEAR,EXAMDIV)
--       IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 7)),EXAMDIV
       IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(EXAMYEAR) - 7)),EXAMDIV
          FROM   HS3_UNIV_NAME_TMP);

-- SQL*PLUS終了
EXIT;

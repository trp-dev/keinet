#!/bin/sh

#環境変数設定
export FILEDIR=/keinavi/HULFT/WorkDir
export SRCDIR=/keinavi/HULFT/WorkDir
export CSVFILE=HS3_UNIV_NAME

#昨年の00区分大学マスタ登録
if [ -f ${FILEDIR}/HS3_UNIV_NAME-lastyear00 ]
then
	if [ -f ${FILEDIR}/HS3_UNIV_NAME ]
	then
		mv ${FILEDIR}/HS3_UNIV_NAME ${FILEDIR}/HS3_UNIV_NAME-tmprename
	fi
	mv ${FILEDIR}/HS3_UNIV_NAME-lastyear00 ${FILEDIR}/HS3_UNIV_NAME


	/keinavi/JOBNET/JNRZ0550/JNRZ0550-lastyear00.sh
	if [ $? -ne 0 ]
	then
		exit 30
	fi
	if [ -f ${FILEDIR}/HS3_UNIV_NAME-tmprename ]
	then
		mv ${FILEDIR}/HS3_UNIV_NAME-tmprename ${FILEDIR}/HS3_UNIV_NAME
	fi
fi

#大学マスタファイルの有無確認
/keinavi/JOBNET/common/JNRZFCHECK.sh
if [ $? -eq 0 ]
then
        fflg=0
	#03,09区分の大学マスタ登録
	if [ -f ${FILEDIR}/HS3_UNIV_NAME-lastdiv ]
	then
		mv ${FILEDIR}/HS3_UNIV_NAME ${FILEDIR}/HS3_UNIV_NAME-tmprename
		mv ${FILEDIR}/HS3_UNIV_NAME-lastdiv ${FILEDIR}/HS3_UNIV_NAME

		/keinavi/JOBNET/JNRZ0550/JNRZ0550-1.sh
		if [ $? -ne 0 ]
		then
			exit 30
		fi

		mv ${FILEDIR}/HS3_UNIV_NAME-tmprename ${FILEDIR}/HS3_UNIV_NAME
	fi

	/keinavi/JOBNET/JNRZ0550/JNRZ0550-1.sh
	if [ $? -ne 0 ]
	then
		exit 30
	fi
else
	echo 処理対象ファイルなし（正常終了）
	fflg=1
fi
	

#校内成績処理システムの就職大学マスタ登録
if [ -f ${FILEDIR}/KO_HS3_UNIV_NAME ]
then
	/keinavi/JOBNET/JNRZ0550-KO/JNRZ0550-KO.sh
fi

if [ ${fflg} -eq 1 ]
then
	exit 1
fi
exit  0    #正常終了

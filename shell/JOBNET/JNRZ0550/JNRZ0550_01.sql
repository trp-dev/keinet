-- ************************************************
-- *機 能 名 : 高３大学マスタ（志望用名称）
-- *機 能 ID : JNRZ0550_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE HS3_UNIV_NAME_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE HS3_UNIV_NAME_TMP  (
       EXAMYEAR                CHAR(4)                   NOT NULL,    /*    年度                         */ 
       EXAMDIV                 CHAR(2)                   NOT NULL,    /*    模試区分                     */ 
       UNIVCD                  CHAR(4)                   NOT NULL,    /*    大学コード                   */ 
       FACULTYCD               CHAR(2)                   NOT NULL,    /*    学部コード                   */ 
       DEPTCD                  CHAR(2)                   NOT NULL,    /*    学科コード                   */ 
       UNIVDIVCD               CHAR(1)                           ,    /*    大学区分コード               */ 
       UNIVNAME_ABBR           VARCHAR2(14)                      ,    /*    大学名短縮                   */ 
       FACULTYNAME_ABBR        VARCHAR2(10)                      ,    /*    学部名短縮                   */ 
       DEPTNAME_ABBR           VARCHAR2(12)                      ,    /*    学科名短縮                   */ 
       SUPERABBRNAME           VARCHAR2(18)                      ,    /*    超短縮名                     */ 
       UNIVNAME_KANA           VARCHAR2(12)                      ,    /*    大学名カナ                   */ 
       FACULTYNAME_KANA        VARCHAR2(8)                       ,    /*    学部名カナ                   */ 
       DEPTNAME_KANA           VARCHAR2(10)                      ,    /*    学科名カナ                   */ 
       SCHEDULECD              CHAR(1)                           ,    /*    日程コード                   */ 
       PREFCD_HQ               CHAR(2)                           ,    /*    本部県コード                 */ 
       PREFCD_SH               CHAR(2)                           ,    /*    校舎県コード                 */ 
       CEN_CONCENTRATE         NUMBER(4,0)                       ,    /*    センター濃厚ライン           */ 
       CEN_BORDER              NUMBER(4,0)                       ,    /*    センターボーダーライン       */ 
       CEN_ATTENTION           NUMBER(4,0)                       ,    /*    センター注意ライン           */ 
       CEN_SCORERATE           NUMBER(4,1)                       ,    /*    センターボーダー得点率       */ 
       BUNRICD                 CHAR(1)                           ,    /*    文理コード                   */ 
       CAPACITY                NUMBER(4,0)                       ,    /*    定員                         */ 
       COEDDIV                 CHAR(1)                           ,    /*    男女共学区分                 */ 
       NIGHTFLG                CHAR(1)                           ,    /*    夜学区分                     */ 
       RANKCD                  CHAR(2)                           ,    /*    難易ランクコード             */ 
       DEPTSORTKYE             CHAR(50)                          ,    /*    学科ソートキー               */ 
       CONSTRAINT PK_HS3_UNIV_NAME_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  年度        */
                 EXAMDIV                   ,               /*  模試区分    */
                 UNIVCD                    ,               /*  大学コード  */
                 FACULTYCD                 ,               /*  学部コード  */
                 DEPTCD                                    /*  学科コード  */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

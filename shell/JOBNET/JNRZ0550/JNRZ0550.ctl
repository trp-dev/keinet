------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 高３大学マスタ（志望用名称）
--  テーブル名      : HS3_UNIV_NAME
--  データファイル名: HS3_UNIV_NAME
--  機能            : 高３大学マスタ（志望用名称）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE HS3_UNIV_NAME_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    EXAMYEAR             POSITION(01:04)   CHAR,
    EXAMDIV              POSITION(05:06)   CHAR,
    UNIVCD               POSITION(07:10)   CHAR,
    FACULTYCD            POSITION(11:12)   CHAR,
    DEPTCD               POSITION(13:14)   CHAR,
    UNIVDIVCD            POSITION(15:15)   CHAR,
    UNIVNAME_ABBR        POSITION(16:29)   CHAR,
    FACULTYNAME_ABBR     POSITION(30:39)   CHAR,
    DEPTNAME_ABBR        POSITION(40:51)   CHAR,
    SUPERABBRNAME        POSITION(52:69)   CHAR,
    UNIVNAME_KANA        POSITION(70:81)   CHAR,
    FACULTYNAME_KANA     POSITION(82:89)   CHAR,
    DEPTNAME_KANA        POSITION(90:99)   CHAR,
    SCHEDULECD           POSITION(100:100) CHAR "NVL(:SCHEDULECD,' ')",
    PREFCD_HQ            POSITION(101:102) CHAR,
    PREFCD_SH            POSITION(103:104) CHAR,
    CEN_CONCENTRATE      POSITION(105:108) INTEGER EXTERNAL "NULLIF(:CEN_CONCENTRATE,   '9999')",
    CEN_BORDER           POSITION(109:112) INTEGER EXTERNAL "NULLIF(:CEN_BORDER,        '9999')",
    CEN_ATTENTION        POSITION(113:116) INTEGER EXTERNAL "NULLIF(:CEN_ATTENTION,     '9999')",
    CEN_SCORERATE        POSITION(117:120) INTEGER EXTERNAL "DECODE(:CEN_SCORERATE,     '9999', null, :CEN_SCORERATE/10)",
    BUNRICD              POSITION(121:121) CHAR,
    CAPACITY             POSITION(122:125) INTEGER EXTERNAL "NULLIF(:CAPACITY,          '9999')",
    COEDDIV              POSITION(126:126) CHAR,
    NIGHTFLG             POSITION(127:127) CHAR,
    RANKCD               POSITION(128:129) CHAR,
    DEPTSORTKYE          POSITION(130:179) CHAR
)

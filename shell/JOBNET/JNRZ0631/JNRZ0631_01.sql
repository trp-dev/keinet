-- ************************************************
-- *機 能 名 : Kei-Netサービス移行データ
-- *機 能 ID : JNRZ0631_01.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 : 2009/12/08
-- *更新内容 : 2019/09/17
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE INDIVIDUALRECORD_TMP2 CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE INDIVIDUALRECORD_TMP2( 
       EXAMYEAR                     CHAR(4)             NOT NULL,    /*     年度                          */
       EXAMCD                       CHAR(2)             NOT NULL,    /*     模試コード                    */
       ANSWERSHEET_NO               CHAR(6)             NOT NULL,    /*     解答用紙番号                  */
       HOMESCHOOLCD                 CHAR(5)                     ,    /*     在卒校コード                  */
       BUNDLECD                     CHAR(5)                     ,    /*     一括コード                    */
       GRADE                        NUMBER(2,0)                 ,    /*     学年                          */
       CLASS                        CHAR(2)                     ,    /*     クラス                        */
       CLASS_NO                     CHAR(5)                     ,    /*     クラス番号                    */
       ATTENDGRADE                  CHAR(2)                     ,    /*     受講学年                      */
       NAME_KANA                    VARCHAR2(14)                ,    /*     カナ氏名                      */
       NAME_KANJI                   VARCHAR2(20)                ,    /*     漢字氏名                      */
       SEX                          CHAR(1)                     ,    /*     性別                          */
       BIRTHDAY                     CHAR(6)                     ,    /*     生年月日                      */
       TEL_NO                       VARCHAR2(11)                ,    /*     電話番号                      */
       CUSTOMER_NO                  CHAR(11)                    ,    /*     顧客番号                      */
       STUDENT_NO                   CHAR(6)                     ,    /*     学籍番号                      */
       REGION                       CHAR(1)                     ,    /*     所属地区                      */
       SCHOOLHOUSE                  CHAR(3)                     ,    /*     所属校舎                      */
       APPLIDATE                    CHAR(1)                     ,    /*     申し込み区分                  */
       PRIVERCYFLG                  CHAR(1)                     ,    /*     プライバシー保護フラグ         */
       EXAMTYPE                     CHAR(1)                     ,    /*     受験型                        */
       BUNRICODE                    CHAR(1)                     ,    /*     文理コード                    */
       SUBRECORD1                   CHAR(32)                    ,    /*     科目成績1                     */
       SUBRECORD2                   CHAR(32)                    ,    /*     科目成績2                     */
       SUBRECORD3                   CHAR(32)                    ,    /*     科目成績3                     */
       SUBRECORD4                   CHAR(32)                    ,    /*     科目成績4                     */
       SUBRECORD5                   CHAR(32)                    ,    /*     科目成績5                     */
       SUBRECORD6                   CHAR(32)                    ,    /*     科目成績6                     */
       SUBRECORD7                   CHAR(32)                    ,    /*     科目成績7                     */
       SUBRECORD8                   CHAR(32)                    ,    /*     科目成績8                     */
       SUBRECORD9                   CHAR(32)                    ,    /*     科目成績9                     */
       SUBRECORD10                  CHAR(32)                    ,    /*     科目成績10                    */
       SUBRECORD11                  CHAR(32)                    ,    /*     科目成績11                    */
       SUBRECORD12                  CHAR(32)                    ,    /*     科目成績12                    */
       SUBRECORD13                  CHAR(32)                    ,    /*     科目成績13                    */
       SUBRECORD14                  CHAR(32)                    ,    /*     科目成績14                    */
       SUBRECORD15                  CHAR(32)                    ,    /*     科目成績15                    */
       SUBRECORD16                  CHAR(32)                    ,    /*     科目成績16                    */
       SUBRECORD17                  CHAR(32)                    ,    /*     科目成績17                    */
       SUBRECORD18                  CHAR(32)                    ,    /*     科目成績18                    */
       SUBRECORD19                  CHAR(32)                    ,    /*     科目成績19                    */
       SUBRECORD20                  CHAR(32)                    ,    /*     科目成績20                    */
       SUBRECORD21                  CHAR(32)                    ,    /*     科目成績21                    */
       SUBRECORD22                  CHAR(32)                    ,    /*     科目成績22                    */
       SUBRECORD23                  CHAR(32)                    ,    /*     科目成績23                    */
       SUBRECORD24                  CHAR(32)                    ,    /*     科目成績24                    */
       SUBRECORD25                  CHAR(32)                    ,    /*     科目成績25                    */
       SUBRECORD26                  CHAR(32)                    ,    /*     科目成績26                    */
       QUESTIONRECORD1              CHAR(284)                   ,    /*     設問成績1                     */
       QUESTIONRECORD2              CHAR(284)                   ,    /*     設問成績2                     */
       QUESTIONRECORD3              CHAR(284)                   ,    /*     設問成績3                     */
       QUESTIONRECORD4              CHAR(284)                   ,    /*     設問成績4                     */
       QUESTIONRECORD5              CHAR(284)                   ,    /*     設問成績5                     */
       QUESTIONRECORD6              CHAR(284)                   ,    /*     設問成績6                     */
       QUESTIONRECORD7              CHAR(284)                   ,    /*     設問成績7                     */
       QUESTIONRECORD8              CHAR(284)                   ,    /*     設問成績8                     */
       QUESTIONRECORD9              CHAR(284)                   ,    /*     設問成績9                     */
       QUESTIONRECORD10             CHAR(284)                   ,    /*     設問成績10                    */
       JAPDESC                      CHAR(7)                     ,    /*     国語記述評価                  */
       ENGPT                        CHAR(38)                    ,    /*     英語認定試験                  */
       CANDIDATERATING1             CHAR(83)                    ,    /*     志望校評価1                   */
       CANDIDATERATING2             CHAR(83)                    ,    /*     志望校評価2                   */
       CANDIDATERATING3             CHAR(83)                    ,    /*     志望校評価3                   */
       CANDIDATERATING4             CHAR(83)                    ,    /*     志望校評価4                   */
       CANDIDATERATING5             CHAR(83)                    ,    /*     志望校評価5                   */
       CANDIDATERATING6             CHAR(83)                    ,    /*     志望校評価6                   */
       CANDIDATERATING7             CHAR(83)                    ,    /*     志望校評価7                   */
       CANDIDATERATING8             CHAR(83)                    ,    /*     志望校評価8                   */
       CANDIDATERATING9             CHAR(83)                    ,    /*     志望校評価9                   */
       ACADEMIC1                    CHAR(16)                    ,    /*     教科科目別学力要素1            */
       ACADEMIC2                    CHAR(16)                    ,    /*     教科科目別学力要素2            */
       ACADEMIC3                    CHAR(16)                    ,    /*     教科科目別学力要素3            */
       ACADEMIC4                    CHAR(16)                    ,    /*     教科科目別学力要素4            */
       ACADEMIC5                    CHAR(16)                    ,    /*     教科科目別学力要素5            */
       ACADEMIC6                    CHAR(16)                    ,    /*     教科科目別学力要素6            */
    CONSTRAINT PK_INDIVIDUALRECORD_TMP2 PRIMARY KEY(
        EXAMYEAR,                                                   /*     年度                           */
        EXAMCD,                                                     /*     模試コード                     */
        ANSWERSHEET_NO                                              /*     解答用紙番号                   */
    )
    USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

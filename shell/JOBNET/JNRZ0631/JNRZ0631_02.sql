-- ************************************************
-- *機 能 名 : Kei-Netサービス移行データ
-- *機 能 ID : JNRZ0631_02.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;

-- 更新対象データを削除 (エラーデータ保持のため削除中止)
-- DELETE FROM INDIVIDUALRECORD_TMP;

-- テンポラリテーブルからデータを挿入
INSERT INTO INDIVIDUALRECORD_TMP
SELECT * FROM INDIVIDUALRECORD_TMP2;

-- テンポラリテーブル削除
DROP TABLE INDIVIDUALRECORD_TMP2 CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

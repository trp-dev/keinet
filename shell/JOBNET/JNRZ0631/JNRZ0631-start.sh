#!/bin/sh

#環境変数設定
export JOBNAME=JNRZ0631
export CSVFILE=INDIVIDUALRECORD
export SRCDIR=/keinavi/HULFT/WorkDir
export WORKDIR=/keinavi/JOBNET/$JOBNAME

#登録対象ファイルの有無確認
/keinavi/JOBNET/common/JNRZFCHECK.sh
if [ $? -eq 10 ]
then
        echo 処理対象ファイルなし（正常終了）
        exit 1
fi
#共通ログへの出力
/keinavi/JOBNET/storelog.sh start ${SRCDIR}/$CSVFILE

#SQL*PLUSテンポラリテーブル作成
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ0631_01.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    /keinavi/JOBNET/storelog.sh err ${SRCDIR}/$CSVFILE
    /keinavi/JOBNET/common/JNRZ0ERR.sh
    echo テンポラリテーブル作成処理でエラーが発生しました
    exit 30
fi

#SQL*LOADERシェル実行
/keinavi/JOBNET/common/JNRZ0IMP.sh

#SQL*LOADER戻り値チェック(0以外は異常終了)
RET=$?
if [ $RET -ne 0 ]
then
#    if [ $RET -le 10 ]
#    then
        #SQL*PLUSテンポラリテーブル削除
#        /keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ0631_04.sql
#        echo 処理対象ファイルなし（正常終了）
#        exit 0
#    else
	/keinavi/JOBNET/storelog.sh err ${SRCDIR}/$CSVFILE
        /keinavi/JOBNET/common/JNRZ0ERR.sh
        echo SQL*LOADERでエラーが発生しました
        exit 30
#    fi
fi

#ログファイル先取得&ファイルライン数取得
nowy=`date +%Y%m`
logy=`expr $nowy - 4`
logys=`echo $logy | awk '{printf "%-.4s\n",$0}'`
Lpath=/keinavi/HULFT/LogDir/store${logys}.log
export targetline=`tail -1 $Lpath`
Examst=`set -- $targetline; shift 1; echo $1`        #模試コード取得
ExamCd=`echo "$Examst" | sed -e s'/[^0-9_]*//'g`
flinest=`set -- $targetline; shift 5; echo $1`       #ファイルライン数取得
filne=`echo "$flinest" | sed -e s'/[^0-9]*//'g`

#SQL*PLUSデータインポート->テンポラリテーブル削除
if [ $filne -le 100000 ]||[ $ExamCd = ${logys}37_69_79 ]
then
	/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ0631_02-del.sql
else
	/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ0631_02.sql
fi

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    /keinavi/JOBNET/storelog.sh err ${SRCDIR}/$CSVFILE
    /keinavi/JOBNET/common/JNRZ0ERR.sh
    echo データのインポート処理でエラーが発生しました
    exit 30
fi

##2015/08追加 start
#SQL*PLUS解答用紙No変換処理
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ0631_05.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    /keinavi/JOBNET/storelog.sh err ${SRCDIR}/$CSVFILE
    /keinavi/JOBNET/common/JNRZ0ERR.sh
    echo 解答用紙No変換処理でエラーが発生しました
    exit 30
fi
##2015/08追加 end

#名寄せ処理実行(Java)
/keinavi/bat/NameBrings2010/NameBrings.sh
RST=$?
##Javaプログラム戻り値チェック(0以外は異常終了)
if [ $RST -ne 0 ]
then
    /keinavi/JOBNET/storelog.sh err ${SRCDIR}/$CSVFILE
    /keinavi/JOBNET/common/JNRZ0ERR.sh
    echo データのインポート処理でエラーが発生しました
    exit 30
fi

##SQL*PLUSデータインポート->テンポラリテーブル削除
#/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ0631_03.sql

##SQL*PLUS戻り値チェック(0以外は異常終了)
#if [ $? -ne 0 ]
#then
#    /keinavi/JOBNET/common/JNRZ0ERR.sh
#    echo データのインポート処理でエラーが発生しました
#    exit 30
#fi

#正常終了
/keinavi/JOBNET/storelog.sh end ${SRCDIR}/$CSVFILE
/keinavi/JOBNET/common/JNRZ0END.sh
echo データのインポート処理が正常に終了しました
exit  0    #正常終了

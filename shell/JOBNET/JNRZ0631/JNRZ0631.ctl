------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 学校マスタ
--  テーブル名      : INDIVIDUALRECORD_TMP2
--  データファイル名: INDIVIDUALRECORD
--  機能            : 学校マスタをデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 2019/09/17
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE INDIVIDUALRECORD_TMP2
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( 
   EXAMYEAR         POSITION(01:04)      CHAR 
,  EXAMCD           POSITION(05:06)      CHAR 
,  ANSWERSHEET_NO   POSITION(07:12)      CHAR 
,  HOMESCHOOLCD     POSITION(13:17)      CHAR 
,  BUNDLECD         POSITION(18:22)      CHAR 
,  GRADE            POSITION(23:24)      INTEGER EXTERNAL 
,  CLASS            POSITION(25:26)      CHAR "NVL(:CLASS,' ')"
,  CLASS_NO         POSITION(27:31)      CHAR "NULLIF(:CLASS_NO, '99999')"
,  ATTENDGRADE      POSITION(32:33)      CHAR 
,  NAME_KANA        POSITION(34:47)      CHAR 
,  NAME_KANJI       POSITION(48:67)      CHAR 
,  SEX              POSITION(68:68)      CHAR 
,  BIRTHDAY         POSITION(69:74)      CHAR "NULLIF(:BIRTHDAY, '999999')"
,  TEL_NO           POSITION(75:85)      CHAR 
,  CUSTOMER_NO      POSITION(86:96)      CHAR 
,  STUDENT_NO       POSITION(97:102)     CHAR 
,  REGION           POSITION(103:103)    CHAR 
,  SCHOOLHOUSE      POSITION(104:106)    CHAR 
,  APPLIDATE        POSITION(107:107)    CHAR 
,  PRIVERCYFLG      POSITION(108:108)    CHAR 
,  EXAMTYPE         POSITION(109:109)    CHAR 
,  BUNRICODE        POSITION(110:110)    CHAR 
,  SUBRECORD1       POSITION(111:142)    CHAR 
,  SUBRECORD2       POSITION(143:174)    CHAR 
,  SUBRECORD3       POSITION(175:206)    CHAR 
,  SUBRECORD4       POSITION(207:238)    CHAR 
,  SUBRECORD5       POSITION(239:270)    CHAR 
,  SUBRECORD6       POSITION(271:302)    CHAR 
,  SUBRECORD7       POSITION(303:334)    CHAR 
,  SUBRECORD8       POSITION(335:366)    CHAR 
,  SUBRECORD9       POSITION(367:398)    CHAR 
,  SUBRECORD10      POSITION(399:430)    CHAR 
,  SUBRECORD11      POSITION(431:462)    CHAR 
,  SUBRECORD12      POSITION(463:494)    CHAR 
,  SUBRECORD13      POSITION(495:526)    CHAR 
,  SUBRECORD14      POSITION(527:558)    CHAR 
,  SUBRECORD15      POSITION(559:590)    CHAR 
,  SUBRECORD16      POSITION(591:622)    CHAR 
,  SUBRECORD17      POSITION(623:654)    CHAR 
,  SUBRECORD18      POSITION(655:686)    CHAR 
,  SUBRECORD19      POSITION(687:718)    CHAR 
,  SUBRECORD20      POSITION(719:750)    CHAR 
,  SUBRECORD21      POSITION(751:782)    CHAR 
,  SUBRECORD22      POSITION(783:814)    CHAR 
,  SUBRECORD23      POSITION(815:846)    CHAR 
,  SUBRECORD24      POSITION(847:878)    CHAR 
,  SUBRECORD25      POSITION(879:910)    CHAR 
,  SUBRECORD26      POSITION(911:942)    CHAR 
,  QUESTIONRECORD1  POSITION(943:1226)   CHAR 
,  QUESTIONRECORD2  POSITION(1227:1510)  CHAR 
,  QUESTIONRECORD3  POSITION(1511:1794)  CHAR 
,  QUESTIONRECORD4  POSITION(1795:2078)  CHAR 
,  QUESTIONRECORD5  POSITION(2079:2362)  CHAR 
,  QUESTIONRECORD6  POSITION(2363:2646)  CHAR 
,  QUESTIONRECORD7  POSITION(2647:2930)  CHAR 
,  QUESTIONRECORD8  POSITION(2931:3214)  CHAR 
,  QUESTIONRECORD9  POSITION(3215:3498)  CHAR 
,  QUESTIONRECORD10 POSITION(3499:3782)  CHAR 
,  JAPDESC          POSITION(3783:3789)  CHAR 
,  ENGPT            POSITION(3790:3827)  CHAR 
,  CANDIDATERATING1 POSITION(3828:3910)  CHAR 
,  CANDIDATERATING2 POSITION(3911:3993)  CHAR 
,  CANDIDATERATING3 POSITION(3994:4076)  CHAR 
,  CANDIDATERATING4 POSITION(4077:4159)  CHAR 
,  CANDIDATERATING5 POSITION(4160:4242)  CHAR 
,  CANDIDATERATING6 POSITION(4243:4325)  CHAR 
,  CANDIDATERATING7 POSITION(4326:4408)  CHAR 
,  CANDIDATERATING8 POSITION(4409:4491)  CHAR 
,  CANDIDATERATING9 POSITION(4492:4574)  CHAR 
,  ACADEMIC1        POSITION(4575:4590)  CHAR 
,  ACADEMIC2        POSITION(4591:4606)  CHAR 
,  ACADEMIC3        POSITION(4607:4622)  CHAR 
,  ACADEMIC4        POSITION(4623:4638)  CHAR 
,  ACADEMIC5        POSITION(4639:4654)  CHAR 
,  ACADEMIC6        POSITION(4655:4670)  CHAR 
)

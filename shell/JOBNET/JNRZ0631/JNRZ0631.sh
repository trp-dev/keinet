#!/bin/sh

#環境変数設定
export JOBNAME=JNRZ0631
export SRCDIR=/keinavi/HULFT/WorkDir
export WORKDIR=/keinavi/JOBNET/$JOBNAME


/keinavi/JOBNET/${JOBNAME}/${JOBNAME}-start.sh
RST=$?

export CSVFILE=INDIVIDUALRECORD-zitaku

#登録対象ファイルの有無確認
if [ -f $SRCDIR/$CSVFILE ]
then
        mv ${SRCDIR}/${CSVFILE} ${SRCDIR}/INDIVIDUALRECORD
        /keinavi/JOBNET/${JOBNAME}/${JOBNAME}-start.sh
        RST=$?
        if [ $RST -ne 0 ]
        then
            exit 30
        fi
fi

#非契約校用
export CSVFILE=INDIVIDUALRECORD-nopact
if [ -f $SRCDIR/$CSVFILE ]
then
	mv ${SRCDIR}/${CSVFILE} ${SRCDIR}/INDIVIDUALRECORD
fi

exit $RST

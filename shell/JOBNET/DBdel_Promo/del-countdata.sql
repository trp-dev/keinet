-- set feedback off
set trimspool on
set echo off
spool /keinavi/JOBNET/DBdel_Promo/tempout.log

delete from EXAMTAKENUM_S where EXAMYEAR='&1' and EXAMCD='&2';
delete from EXAMTAKENUM_C where EXAMYEAR='&1' and EXAMCD='&2';
delete from EXAMTAKENUM_A where EXAMYEAR='&1' and EXAMCD='&2';
delete from EXAMTAKENUM_P where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from SUBRECORD_A where EXAMYEAR='&1' and EXAMCD='&2';
delete from SUBRECORD_P where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from SUBRECORD_S where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from SUBRECORD_C where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from SUBDISTRECORD_S where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from SUBDISTRECORD_C where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from SUBDISTRECORD_A where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from SUBDISTRECORD_P where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from QRECORD_S where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from QRECORD_C where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from QRECORD_A where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from QRECORD_P where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from QRECORDZONE where EXAMYEAR='&1' and EXAMCD='&2';
commit;

delete from RATINGNUMBER_S where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from RATINGNUMBER_C where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from RATINGNUMBER_P where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from RATINGNUMBER_A where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from MARKQSCORERATE where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from SHOQUESTIONRECORD_A where EXAMYEAR='&1' and EXAMCD='&2';
commit;
delete from SHOQUESTIONRECORD_S where EXAMYEAR='&1' and EXAMCD='&2';
commit;
spool off
EXIT

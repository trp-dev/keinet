-- set feedback off
set trimspool on
set echo off
spool /keinavi/JOBNET/DBdel_Promo/tempout.log

delete from &1 where EXAMYEAR='&2' and EXAMCD='&3';
commit;

spool off

EXIT

-- set feedback off
set trimspool on
set echo off
spool /keinavi/JOBNET/DBdel_Promo/tempout.log

/*
 * 面談メモの削除 */
delete from INTERVIEW where INDIVIDUALID in (select distinct INDIVIDUALID from HISTORYINFO where YEAR='&1' MINUS
select distinct INDIVIDUALID from HISTORYINFO where INDIVIDUALID in (select INDIVIDUALID from HISTORYINFO where NOT YEAR='&1') and YEAR='&1');

/*
 * こだわり条件の削除 */
delete from HANGUPPROVISO where INDIVIDUALID in (select distinct INDIVIDUALID from HISTORYINFO where YEAR='&1' MINUS
select distinct INDIVIDUALID from HISTORYINFO where INDIVIDUALID in (select INDIVIDUALID from HISTORYINFO where NOT YEAR='&1') and YEAR='&1');

/*
 * 判定履歴トランの削除 */
delete from DETERMHISTORY where INDIVIDUALID in (select distinct INDIVIDUALID from HISTORYINFO where YEAR='&1' MINUS
select distinct INDIVIDUALID from HISTORYINFO where INDIVIDUALID in (select INDIVIDUALID from HISTORYINFO where NOT YEAR='&1') and YEAR='&1');

/*
 * 受験予定大学の削除 */
delete from EXAMPLANUNIV where INDIVIDUALID in (select distinct INDIVIDUALID from HISTORYINFO where YEAR='&1' MINUS
select distinct INDIVIDUALID from HISTORYINFO where INDIVIDUALID in (select INDIVIDUALID from HISTORYINFO where NOT YEAR='&1') and YEAR='&1');
commit;

/*
 * 学籍基本情報の削除 */
delete from BASICINFO where INDIVIDUALID in (select distinct INDIVIDUALID from HISTORYINFO where YEAR='&1' MINUS
select distinct INDIVIDUALID from HISTORYINFO where INDIVIDUALID in (select INDIVIDUALID from HISTORYINFO where NOT YEAR='&1') and YEAR='&1');
commit;

/*
 * 学籍履歴情報の削除 */
delete from HISTORYINFO where YEAR='&1';
commit;

/*
 * 複合クラスの削除 */
delete from CLASSGROUP where CLASSGCD in (select CLASSGCD from CLASS_GROUP where YEAR='&1');

/*
 * クラス－複合クラス対応の削除 */
delete from CLASS_GROUP where YEAR='&1';

commit;

spool off
EXIT

#! /bin/sh 

### 進級処理による過年度データの削除バッチ
### 4年前の個人データ＆8年前の集計データを削除する
### 処理制限時間内でできなければ、次回へ持ち越し

if [ $# != 1 ]; then
	echo "---- PARAMETER ERROR ----"
	exit 30
fi

#-- 現在の年度、月日を取得
nowyear=`date +%Y`
nowdate=`date +%m%d`

#-- ログ出力定義
publicfpath="/keinavi/JOBNET/DBdel_Promo"

outflistpath=`ls -t $publicfpath/log/`
fcount=1
for fpath in $outflistpath; do
	if [ $fcount -le 1 ]; then
		outfpath=$publicfpath/log/$fpath
	fi
	fcount=`expr $fcount + 1`
done

export indivlistfile="/keinavi/JOBNET/del_datatable.txt"
export listfile="/keinavi/JOBNET/del_examlist.txt"

if [ $1 = "0" ]
then
	##--- 削除定義ファイル取得 4月1日のみ起動
	
	outfpath="$publicfpath/log/delete$nowyear$nowdate.log"
	
	if [ $nowdate = 0401 ]; then
		echo "**** 各種定義ファイル作成処理 ****" >> $outfpath

		#-- 基本情報削除定義ファイル作成
		touch /keinavi/JOBNET/del_info.txt
		echo "--- 基本情報削除用定義ファイル作成完了 ファイル：/keinavi/JOBNET/del_info.txt ---"
		echo "--- 基本情報削除用定義ファイル作成完了 ファイル：/keinavi/JOBNET/del_info.txt ---" >> $outfpath
		echo " "
		echo " " >> $outfpath
		
		#-- 個人成績データ削除定義ファイル作成
		targettable='INDIVIDUALRECORD_TMP SUBRECORD_I QRECORD_I CANDIDATERATING SUBRECORD_PP QRECORD_PP CANDIDATERATING_PP INDIVIDUALRECORD SUBACADEMIC_I SUBACADEMIC_I_PP'
		for tablelist in $targettable; do
			echo $tablelist >> $indivlistfile
		done
		echo "--- 個人成績データ削除用定義ファイル作成完了 ファイル：$indivlistfile ---"
		echo "--- 個人成績データ削除用定義ファイル作成完了 ファイル：$indivlistfile ---" >> $outfpath
		echo " "
		echo " " >> $outfpath
		
		#-- 集計データ削除用対象模試一覧取得
		delyear=`expr $nowyear - 8`
		sqlplus keinavi/39ra8ma@S11DBA_BATCH2 @$publicfpath/getexamlist.sql SUBRECORD_A $delyear
		sed -n '3,/ /p' /keinavi/JOBNET/del_targetexam.txt > $listfile
		\rm -f /keinavi/JOBNET/del_targetexam.txt
		echo "--- 集計データ削除用対象模試一覧ファイル作成完了 ファイル：$listfile ---"
		echo "--- 集計データ削除用対象模試一覧ファイル作成完了 ファイル：$listfile ---" >> $outfpath
		echo " " >> $outfpath
	else
		echo "--- すでに$nowyear 年度用のデータ削除定義ファイルは作成済みかJOBNET起動日に達していません。 ---"
		echo "--- すでに$nowyear 年度用のデータ削除定義ファイルは作成済みかJOBNET起動日に達していません。 ---"  >> $outfpath
		echo " " >> $outfpath
		exit 1
	fi

elif [ $1 = "1" ]
then
	##--- 個人基本情報データ削除処理
	echo " " >> $outfpath
	echo "**** 個人基本情報データ削除処理 ****" >> $outfpath

	delyear=`expr $nowyear - 3`

	if [[ ! -f /keinavi/JOBNET/del_info.txt ]]; then
		echo "--- すでに $delyear 年度の個人基本情報データは削除されています。 ---"
		echo "--- すでに $delyear 年度の個人基本情報データは削除されています。 ---" >> $outfpath
		echo " " >> $outfpath
		exit 1
	fi
	
	#-- 基本情報データの削除開始
	echo "--- 個人基本情報データ削除処理を開始します..."
	echo "--- 個人基本情報データ削除処理を開始します..." >> $outfpath
	sqlplus keinavi/39ra8ma@S11DBA_BATCH2 @$publicfpath/del-indivinfo.sql $delyear 

	cat $publicfpath/tempout.log >> $outfpath
	\rm -f $publicfpath/tempout.log

	echo "--- $delyear 年度 個人基本情報データの削除完了しました ---" 
	echo "--- $delyear 年度 個人基本情報データの削除完了しました ---" >> $outfpath
	echo " " 
	echo " " >> $outfpath
	\rm -f /keinavi/JOBNET/del_info.txt

elif [ $1 = "2" ]
then
	##-- 個人成績データ削除処理
	echo " " >> $outfpath
	echo "**** 個人成績データ削除処理 ****" >> $outfpath
	
	delyear=`expr $nowyear - 3`
	
	if [[ ! -f $indivlistfile ]]; then
		echo "--- すでに $delyear 年度の個人成績データは削除されています。 ---"
		echo "--- すでに $delyear 年度の個人成績データは削除されています。 ---" >> $outfpath
		echo " " >> $outfpath
		exit 1
	fi

	export tablelist=`cat $indivlistfile`
	\rm -f $indivlistfile
	export nowtimes=`date +%H%M`

        export examlist=`cat $listfile`

        #--- 取得した対象模試に対して処理
        for examcd in $examlist; do
	
		for tablename in $tablelist; do
			echo "--- 処理対象年度：$delyear、処理対象模試：$examcd、テーブル：$tablename"
			echo "--- 処理対象年度：$delyear、処理対象模試：$examcd、テーブル：$tablename" >> $outfpath
		
			#--- 処理制限時間のチェック
			if [ $nowtimes -le "2200" ]&&[ $nowtimes -ge "0200" ]; then
				echo "--- 処理時間外のため、次回へ処理は持ち越します ---"
				echo "--- 処理時間外のため、次回へ処理は持ち越します ---" >> $outfpath
				echo " "
				echo " " >> $outfpath
				echo "$tablename" >> $indivlistfile    #--次回持ち越し
				continue
			fi

			#-- 個人成績データの削除開始
			echo "--- 個人成績データ削除処理を開始します..."
			echo "--- 個人成績データ削除処理を開始します..." >> $outfpath
			sqlplus keinavi/39ra8ma@S11DBA_BATCH2 @$publicfpath/del-data_tableyear.sql $tablename $delyear $examcd
		
			cat $publicfpath/tempout.log >> $outfpath
			\rm -f $publicfpath/tempout.log

			export nowtimes=`date +%H%M`
			echo "--- $nowdate-$nowtimes  $delyear 年度 $examcd 模試 $tablename 成績データの削除完了しました ---" 
			echo "--- $nowdate-$nowtimes  $delyear 年度 $examcd 模試 $tablename 成績データの削除完了しました ---" >> $outfpath
			echo " " 
			echo " " >> $outfpath
		done
	done

elif [ $1 = "3" ]
then
	##-- 集計データ削除処理
	echo " " >> $outfpath
	echo "**** 集計データ削除処理 ****" >> $outfpath

	delyear=`expr $nowyear - 8`

	if [[ ! -f $listfile ]]; then
		echo "--- すでに $delyear 年度の集計データは削除されています。 ---"
		echo "--- すでに $delyear 年度の集計データは削除されています。 ---" >> $outfpath
		echo " " >> $outfpath
		exit 1
	fi

	export examlist=`cat $listfile`
	\rm -f $listfile
	export nowtimes=`date +%H%M`

	#--- 取得した対象模試に対して処理
	for examcd in $examlist; do
		echo "--- 処理対象模試：$examcd"
		echo "--- 処理対象模試：$examcd" >> $outfpath

		#--- 対象模試別に制限時間を設定
		if [ $examcd = 01 ]||[ $examcd = 02 ]||[ $examcd = 03 ]||[ $examcd = 04 ]||[ $examcd = 05 ]||[ $examcd = 06 ]||[ $examcd = 07 ]||[ $examcd = 09 ]||[ $examcd = 38 ]; then
			export limittime="0330"
		else
			export limittime="0600"
		fi

		#--- 現時間が制限時間より超えていた場合は次回持ち越し
		if [ $nowtimes -le "2200" ]&&[ $nowtimes -ge $limittime ]; then
			echo "--- 処理時間外のため、次回へ処理は持ち越します ---"
			echo "--- 処理時間外のため、次回へ処理は持ち越します ---" >> $outfpath
			echo " "
			echo " " >> $outfpath
			echo "$examcd" >> $listfile    #--次回持ち越し
			continue
		fi

		#--- 集計データの削除開始
		echo "--- 集計データ削除処理を開始します..."
		echo "--- 集計データ削除処理を開始します..." >> $outfpath
		sqlplus keinavi/39ra8ma@S11DBA_BATCH2 @$publicfpath/del-countdata.sql $delyear $examcd
		cat $publicfpath/tempout.log >> $outfpath
		\rm -f $publicfpath/tempout.log

		export nowtimes=`date +%H%M`
		echo "--- $nowdate-$nowtimes  $delyear 年度 $examcd 集計データの削除完了しました ---"
		echo "--- $nowdate-$nowtimes  $delyear 年度 $examcd 集計データの削除完了しました ---" >> $outfpath
		echo " "
		echo " "  >> $outfpath
	done

fi

------------------------------------------------------------------------------
--  システム名      : KEI-NAVI
--  概念ファイル名  : マーク模試高校別設問・成績層別正答率(全国)
--  テーブル名      : MARKQDEVZONEANSRATE_A
--  データファイル名: MARKQDEVZONEANSRATE_A
--  機能            : マーク模試高校別設問・成績層別正答率(全国)をデータファイルの内容に置き換える
--  作成日          : 2009/12/08
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE MARKQDEVZONEANSRATE_A_TMP
(  
   EXAMYEAR        POSITION(01:04)  CHAR,
   EXAMCD          POSITION(05:06)  CHAR,
   SUBJECTCD       POSITION(07:10)  CHAR,
   ANSWER_NO       POSITION(11:16)  CHAR,
   PERFRECORDFLG   POSITION(17:17)  CHAR,
   PERFQFLG        POSITION(18:18)  CHAR,
   DEVZONECD       POSITION(19:19)  CHAR,
   NUMBERS         POSITION(20:27)  INTEGER EXTERNAL "NULLIF(:NUMBERS,     '99999999')",
   AVGSCORERATE    POSITION(28:31)  INTEGER EXTERNAL "DECODE(:AVGSCORERATE,    '9999', null, :AVGSCORERATE/10)"
)


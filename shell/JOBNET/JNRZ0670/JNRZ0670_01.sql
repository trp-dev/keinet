-- ************************************************
-- *機 能 名 : マーク模試高校別設問・成績層別正答率(全国)
-- *機 能 ID : JNRZ0670_01.sql
-- *作 成 日 : 2009/12/08
-- *作 成 者 : KCN
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE MARKQDEVZONEANSRATE_A_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE MARKQDEVZONEANSRATE_A_TMP  (
       EXAMYEAR               CHAR(4)              NOT NULL,   /*   模試年度           */ 
       EXAMCD                 CHAR(2)              NOT NULL,   /*   模試コード         */ 
       SUBJECTCD              CHAR(4)              NOT NULL,   /*   科目コード         */ 
       ANSWER_NO              VARCHAR2(6)          NOT NULL,   /*   解答番号           */ 
       PERFRECORDFLG          CHAR(1)              NOT NULL,   /*   完答レコードフラグ */ 
       PERFQFLG               CHAR(1)              NOT NULL,   /*   完答問題フラグ     */ 
       DEVZONECD              CHAR(1)              NOT NULL,   /*   成績層コード       */ 
       NUMBERS                NUMBER(8,0)                  ,   /*   人数               */ 
       AVGSCORERATE           NUMBER(4,1)                  ,   /*   平均得点率         */ 
       CONSTRAINT PK_MARKQDEVZONEANSRATE_A_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  模試年度           */
                 EXAMCD                    ,               /*  模試コード         */
                 SUBJECTCD                 ,               /*  科目コード         */
                 ANSWER_NO                 ,               /*  解答番号           */
                 PERFRECORDFLG             ,               /*  完答レコードフラグ */
                 PERFQFLG                  ,               /*   完答問題フラグ    */
                 DEVZONECD                                 /*  成績層コード       */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 科目分布別成績（県）
--  テーブル名      : SUBDISTRECORD_P
--  データファイル名: SUBDISTRECORD_P
--  機能            : 科目分布別成績（県）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE SUBDISTRECORD_P_TMP
(  
   EXAMYEAR     POSITION(01:04)   CHAR,
   EXAMCD       POSITION(05:06)   CHAR,
   PREFCD       POSITION(07:08)   CHAR,
   SUBCD        POSITION(09:12)   CHAR,
   DEVZONECD    POSITION(13:14)   CHAR,
   NUMBERS      POSITION(15:22)   INTEGER EXTERNAL "NULLIF(:NUMBERS,   '99999999')",
   COMPRATIO    POSITION(23:27)   INTEGER EXTERNAL "DECODE(:COMPRATIO,     '9999', null, :COMPRATIO/10)"
)


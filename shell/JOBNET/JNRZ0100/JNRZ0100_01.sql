-- ************************************************
-- *機 能 名 : 科目分布別成績(県)
-- *機 能 ID : JNRZ0100_01.sql
-- *作 成 日 : 2004/08/30
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE SUBDISTRECORD_P_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE SUBDISTRECORD_P_TMP  (
       EXAMYEAR          CHAR(4)                NOT NULL,   /*   模試年度            */ 
       EXAMCD            CHAR(2)                NOT NULL,   /*   模試コード          */ 
       PREFCD            CHAR(2)                NOT NULL,   /*   県コード            */ 
       SUBCD             CHAR(4)                NOT NULL,   /*   科目コード          */ 
       DEVZONECD         CHAR(2)                NOT NULL,   /*   偏差値帯コード      */ 
       NUMBERS           NUMBER(8,0)                    ,   /*   人数                */ 
       COMPRATIO         NUMBER(4,1)                    ,   /*   構成比              */ 
       CONSTRAINT PK_SUBDISTRECORD_P_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  模試年度        */
                 EXAMCD                    ,               /*  模試コード      */
                 PREFCD                    ,               /*  県コード        */
                 SUBCD                     ,               /*  科目コード      */
                 DEVZONECD                                 /*  偏差値帯コード  */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

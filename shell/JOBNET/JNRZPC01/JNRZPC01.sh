#!/bin/sh

#環境変数設定
export JOBNAME=JNRZPC01
export CSVFILE=JH01_KAMOKU1
export SRCDIR=/keinavi/HULFT/WorkDir
export WORKDIR=/keinavi/JOBNET/$JOBNAME

#登録対象ファイルの有無確認
/keinavi/JOBNET/common/JNRZFCHECK.sh
if [ $? -eq 10 ]
then
        echo 処理対象ファイルなし（正常終了）
        exit 1
fi

#共通ログへの出力
/keinavi/JOBNET/storelog.sh start ${SRCDIR}/$CSVFILE

#SQL*PLUSテンポラリテーブル作成
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZPC01_01.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    /keinavi/JOBNET/storelog.sh err ${SRCDIR}/$CSVFILE
    /keinavi/JOBNET/common/JNRZ0ERR.sh
    echo テンポラリテーブル作成処理でエラーが発生しました
    exit 30
fi


#SQL*LOADERシェル実行
/keinavi/JOBNET/common/JNRZ0IMP.sh

#SQL*LOADER戻り値チェック(0以外は異常終了)
RET=$?
if [ $RET -ne 0 ]
then
#    if [ $RET -le 10 ]
#    then
        #SQL*PLUSテンポラリテーブル削除
#        /keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZPC01_03.sql
#        echo 処理対象ファイルなし（正常終了）
#        exit 0
#    else
	/keinavi/JOBNET/storelog.sh err ${SRCDIR}/$CSVFILE
        /keinavi/JOBNET/common/JNRZ0ERR.sh
        echo SQL*LOADERでエラーが発生しました
        exit 30
#    fi
fi


#SQL*PLUSデータインポート->テンポラリテーブル削除
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZPC01_02.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    /keinavi/JOBNET/storelog.sh err ${SRCDIR}/$CSVFILE
    /keinavi/JOBNET/common/JNRZ0ERR.sh
    echo データのインポート処理でエラーが発生しました
    exit 30
fi

#正常終了
/keinavi/JOBNET/storelog.sh end ${SRCDIR}/$CSVFILE
/keinavi/JOBNET/common/JNRZ0END.sh
echo データのインポート処理が正常に終了しました
exit  0    #正常終了


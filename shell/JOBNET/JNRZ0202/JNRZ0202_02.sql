-- *********************************************************
-- *機 能 名 : 志望大学評価別人数（英語認定試験含む）(高校)
-- *機 能 ID : JNRZ0202_02.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM RATINGNUMBER_S_ENGPT MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM RATINGNUMBER_S_ENGPT_TMP TMP
         WHERE 1=1
           AND MAIN.EXAMYEAR         = TMP.EXAMYEAR
           AND MAIN.EXAMCD           = TMP.EXAMCD
           AND MAIN.BUNDLECD         = TMP.BUNDLECD
           AND MAIN.RATINGDIV        = TMP.RATINGDIV
           AND MAIN.UNIVCOUNTINGDIV  = TMP.UNIVCOUNTINGDIV
           AND MAIN.UNIVCD           = TMP.UNIVCD
           AND MAIN.FACULTYCD        = TMP.FACULTYCD
           AND MAIN.DEPTCD           = TMP.DEPTCD
           AND MAIN.AGENDACD         = TMP.AGENDACD
           AND MAIN.STUDENTDIV       = TMP.STUDENTDIV
       )
;

-- テンポラリから本物へ登録
INSERT INTO RATINGNUMBER_S_ENGPT SELECT * FROM RATINGNUMBER_S_ENGPT_TMP;

-- テンポラリテーブル削除
DROP TABLE RATINGNUMBER_S_ENGPT_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

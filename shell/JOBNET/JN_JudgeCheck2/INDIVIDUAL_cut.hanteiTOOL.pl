#!/usr/bin/perl
########################################
# 判定チェックツール input用ファイル作成
#

$ORG_FILE = shift;
$OUT_FILE = $ORG_FILE . "_cut";

if(! open(IN, "$ORG_FILE")){
	print "FILE OPEN ERROR!\n";
	exit;
}
if(! open(OUT, ">$OUT_FILE") ){
        close(IN);
        exit;
}

while(<IN>){
        chomp($_);
        $header = substr($_,    0, 812);
        $footer = substr($_, 3652, );

	print OUT "$header$footer\n";
}
close(OUT);
close(IN);

print "FINISH!\n";
exit;


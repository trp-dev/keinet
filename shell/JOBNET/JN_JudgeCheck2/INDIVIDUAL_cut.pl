#!/usr/bin/perl
########################################
# 判定チェックツール input用ファイル作成
#

$ORG_FILE = shift;
$OUT_FILE = $ORG_FILE . "_cut";

if(! open(IN, "$ORG_FILE")){
	print "FILE OPEN ERROR!\n";
	exit;
}
if(! open(OUT, ">$OUT_FILE") ){
        close(IN);
        exit;
}

while(<IN>){
        chomp($_);
        $header = substr($_,    0, 108);
        $header2= substr($_,  110, 704);
        $footer = substr($_, 3654, );

	print OUT "$header$header2$footer\n";
}
close(OUT);
close(IN);

print "FINISH!\n";
exit;


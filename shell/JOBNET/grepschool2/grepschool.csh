#! /bin/csh 

if (($#argv < 1 ) || (($1 != -1)&&($1 != 0)&&($1 != 1)) || (($1 == 1)&&($#argv < 3)))then
	echo " ---- param error ----"
	echo " 第1パラメータ:grep校取得先識別フラグ -1,0:ファイル、1:スクリプト内定義(必須)"
	echo " 第2パラメータ:grepフォルダ  (第1パラが0の時は不要)" 
	echo " 第3パラメータ:grep年度+模試コード (第1パラが0の時は不要)"
	echo " 第4パラメータ:grepファイル名 (すべて対象時は不要)"
	exit
endif

set getflg = $1
set filepath = /keinavi/HULFT/DATA
set examconffpath = "/home/navikdc/datastore/inside_release"

set list = ()
set count = 1
set allflg = 0         #全国、県データの対象有無フラグ

#grep対象模試定義ファイルの読み込み（検証校定義がファイル時）
if (($getflg == 0)||($getflg == -1)) then
	(ls ${examconffpath}/grep* > tmp.log) >& /dev/null
	set list = `cat tmp.log`
	\rm tmp.log
	if ($#list == 0) then
		echo "---- Config File ERROR ----"
		echo "  検証校定義ファイルが見つかりません "
		exit 30
	endif
endif

cd $filepath
	
#grep対象校取得（検証校定義がスクリプト内時）
if ($getflg == 1) then
	set indir = $2
	set year = $3
	set list = (1)
        set year2 = `echo $year | awk '{printf "%-.4s\n",$0}'`
        set exam2 = `expr $year : ".*\(..\)"`

	##########---- grepの対象校 (必要に応じて高校コードのみ変更) ###########
	##########---- ※第1パラメータが[1]の値のみ有効              ###########
	set schoolcdlist = (23832)
	set allflg = 0
endif

#-- 出力ファイルの初期化
if (-f $filepath/grepexamstr) then
	\rm $filepath/grepexamstr
	\rm -rf $filepath/temp/2*
endif

while($count <= $#list)

	if (($getflg == 0)||($getflg == -1)) then
		#-- 抽出対象模試ファイルをリストから取得
		set targetfile = $list[$count]
	        #-- フルファイルパスからファイル名文字列を取得
		set failname = `echo $targetfile | sed -e 's/.*\///g'`
		#-- ファイルパスから対象模試＋模試コード文字列を取得
		set year = `expr $targetfile : ".*\(......\)"`
		set orgyear = `expr $targetfile : ".*\(......\)"`
                set year2 = `echo $year | awk '{printf "%-.4s\n",$0}'`
                set exam2 = `expr $year : ".*\(..\)"`
		#-- 抽出対象フォルダを模試ごとに識別
#		if (($exam2 == 61)||($exam2 == 71)) then
#			set indir = ${year2}61_71
#		else if (($exam2 == 62)||($exam2 == 72)) then
#			set indir = ${year2}62_72
#		else if (($exam2 == 63)||($exam2 == 73)) then
#			set indir = ${year2}63_73
#		else if (($exam2 == 28)||($exam2 == 68)||($exam2 == 78)) then
#			set indir = ${year2}28_68_78
#		else
			set indir = $year
#		endif
		#-- 抽出用対象データフォルダの確認
		if (! -d $indir) then
			echo "--- ERROR： Grep対象フォルダ $indir がみつかりません ---"
			@ count ++
			continue
		endif
		#-- ファイルよりgrep対象校を取得
		set schoolcdlist = `cat $targetfile`

		if ($getflg == -1) then
			set allflg = 1
		endif
	endif

	if ($#schoolcdlist == 0) then
		echo " 模試 ${year} のgrep対象校 全件"
	else
		echo " 模試 ${year} のgrep対象校 $schoolcdlist"
	endif

	#-- 抽出用の文字列作成
	set count2 = 1
	while($count2 <= $#schoolcdlist)
		set strtem = $schoolcdlist[$count2]
		#--- 変数内に改行があった場合は削除
		set str = `echo ${strtem} | tr -d '\r\n'`

		if ($str != "") then
			if($count2 == 1)then
				set gstring = "-e ^${year}$str"
				set gstring2 = "-e ^${year}...........$str"
			else
				set gstring = "${gstring} -e ^${year}$str"
				set gstring2 = "${gstring2} -e ^${year}...........$str"
			endif
		endif
		@ count2 ++
	end
	set orgyear = $year
	set orginyear = $indir

	#-- ベース出力用フォルダ
	set dir = "temp/${indir}selection"

	#-- 事後換算・得点修正・都立換算データの有無確認
# 1019変更	(ls -r ${indir}/after > tmp.log) >& /dev/null
	(ls -tr ${indir}/after > tmp.log) >& /dev/null
        set subdir = `cat tmp.log`
        \rm tmp.log
        if ($#subdir != 0) then
		set masterindir = "${indir}/after"
        endif

	set examtystr = ""
	set count5 = 0
	while ($count5 <= $#subdir) 
	
		#-- 事後換算・得点修正・都立換算データ用に出力先変更	
		if ($count5 > 0) then
			set targety = $subdir[$count5]
			set dir = temp/${subdir[$count5]}selection
			set indir = ${masterindir}/$subdir[$count5]
			set allflg = 0

			#-- データ種別文字列取得
			set examtystr = `echo $targety | sed -e s'/[0-9]_*//'g`
#			set examtystr = `echo $targety | sed -e s'/^[0-9_]\{,12\}//'g`

			set year = ${orgyear}${examtystr}
		endif
	
		#-- 抽出結果出力用フォルダの確認
		if (! -d $dir) then
			mkdir -p $dir
			chmod 777 $dir
			if ($status != 0) then
				echo "---- ERROR： フォルダ作成処理にエラーが発生したため処理を中断します。----"
				exit
			endif
		endif

		echo "抽出対象 $indir  出力先 $dir　データ種別 $examtystr"

		#-- 抽出ファイルの個別指定
#		if ($4 != "") then
#			if (($4 == "INDIVIDUALRECORD")||($4 == "INDIVIDUALRECORD-old")) then
#				echo "grep $gstring2 start"
#				grep $gstring2 $indir/$4 >> $dir/INDIVIDUALRECORD
#			else
#				echo "grep $gstring start"
#				grep $gstring $indir/$4 > $dir/$4
#			endif
#
#			echo "grep END"
#			exit
#		endif

		if ($allflg == 1) then
			if ((-f $indir/EXAMTAKENUM_A)&&(! -f $dir/EXAMTAKENUM_A)) then
				cp -p $indir/EXAMTAKENUM_A $dir/EXAMTAKENUM_A
			else if (! -f $indir/EXAMTAKENUM_A) then
				echo "-- 模試受験者総数(全国)[EXAMTAKENUM_A]【RZRG1180】が存在しません --" 
			endif
			if ((-f $indir/EXAMTAKENUM_P)&&(! -f $dir/EXAMTAKENUM_P)) then
				cp -p $indir/EXAMTAKENUM_P $dir/EXAMTAKENUM_P
			else if (! -f $indir/EXAMTAKENUM_P) then
				echo "-- 模試受験者総数(県)[EXAMTAKENUM_P]【RZRG1280】が存在しません --" 
			endif
			if ((-f $indir/SUBRECORD_A)&&(! -f $dir/SUBRECORD_A)) then
				cp -p $indir/SUBRECORD_A $dir/SUBRECORD_A
			else if (! -f $indir/SUBRECORD_A) then
				echo "-- 科目別成績(全国)[SUBRECORD_A]【RZRG1110】が存在しません --" 
			endif
			if ((-f $indir/SUBRECORD_P)&&(! -f $dir/SUBRECORD_P)) then
				cp -p $indir/SUBRECORD_P $dir/SUBRECORD_P
			else if (! -f $indir/SUBRECORD_P) then
				echo "-- 科目別成績(県)[SUBRECORD_P]【RZRG1210】が存在しません --" 
			endif
			if ((-f $indir/SUBDISTRECORD_A)&&(! -f $dir/SUBDISTRECORD_A)) then
				cp -p $indir/SUBDISTRECORD_A $dir/SUBDISTRECORD_A
			else if (! -f $indir/SUBDISTRECORD_A) then
				echo "-- 科目分布別成績(全国)[SUBDISTRECORD_A]【RZRG1120】が存在しません --" 
			endif
			if ((-f $indir/SUBDISTRECORD_P)&&(! -f $dir/SUBDISTRECORD_P)) then
				cp -p $indir/SUBDISTRECORD_P $dir/SUBDISTRECORD_P
			else if (! -f $indir/SUBDISTRECORD_P) then
				echo "-- 科目分布別成績(県)[SUBDISTRECORD_P]【RZRG1220】が存在しません --" 
			endif
			if ((-f $indir/QRECORD_A)&&(! -f $dir/QRECORD_A)) then
				cp -p $indir/QRECORD_A $dir/QRECORD_A
			else if (! -f $indir/QRECORD_A) then
				echo "-- 設問別成績(全国)[QRECORD_A]【RZRG1130】が存在しません --" 
			endif
			if ((-f $indir/QRECORD_P)&&(! -f $dir/QRECORD_P)) then
				cp -p $indir/QRECORD_P $dir/QRECORD_P
			else if (! -f $indir/QRECORD_P) then
				echo "-- 設問別成績(県)[QRECORD_P]【RZRG1230】が存在しません --" 
			endif
			if ((-f $indir/QRECORDZONE_A)&&(! -f $dir/QRECORDZONE_A)) then
				cp -p $indir/QRECORDZONE_A $dir/QRECORDZONE_A
			else if (! -f $indir/QRECORDZONE_A) then
				echo "-- 設問別成績層別成績(全国)[QRECORDZONE_A]【RZRG1140】が存在しません --" 
			endif
			if ((-f $indir/RATINGNUMBER_A)&&(! -f $dir/RATINGNUMBER_A)) then
				cp -p $indir/RATINGNUMBER_A $dir/RATINGNUMBER_A
			else if (! -f $indir/RATINGNUMBER_A) then
				if (($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 74)) then
					echo "-- 志望大学評価別人数(全国)[RATINGNUMBER_A]【RZRG1150】が存在しません --" 
				endif
			endif
			if ((-f $indir/RATINGNUMBER_P)&&(! -f $dir/RATINGNUMBER_P)) then
				cp -p $indir/RATINGNUMBER_P $dir/RATINGNUMBER_P
			else if (! -f $indir/RATINGNUMBER_P) then
				if (($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 74)) then
					echo "-- 志望大学評価別人数(県)[RATINGNUMBER_P]【RZRG1250】が存在しません --" 
				endif
			endif
			if ((-f $indir/EXAMQUESTION)&&(! -f $dir/EXAMQUESTION)) then
				cp -p $indir/EXAMQUESTION $dir/EXAMQUESTION
			else if (! -f $indir/EXAMQUESTION) then
				echo "-- 模試設問マスタ[EXAMQUESTION]【RZRG3310】が存在しません --" 
			endif
			if ((-f $indir/MARKANSWER_NO)&&(! -f $dir/MARKANSWER_NO)) then
				cp -p $indir/MARKANSWER_NO $dir/MARKANSWER_NO
			else if (! -f $indir/MARKANSWER_NO) then
				if (($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66)) then
				echo "-- マーク模試解答番号マスタ[MARKANSWER_NO]【RZRG3410】が存在しません --" 
				endif
			endif
			if ((-f $indir/MARKQDEVZONEANSRATE_A)&&(! -f $dir/MARKQDEVZONEANSRATE_A)) then
				cp -p $indir/MARKQDEVZONEANSRATE_A $dir/MARKQDEVZONEANSRATE_A
			else if (! -f $indir/MARKQDEVZONEANSRATE_A) then
				if (($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66)) then
				echo "-- マーク模試高校別設問別成績層別正答率(全国)[MARKQDEVZONEANSRATE_A]【RZRG1190】が存在しません --" 
				endif
			endif
			if ((-f $indir/SUBDEVZONERECORD_A)&&(! -f $dir/SUBDEVZONERECORD_A)) then
				cp -p $indir/SUBDEVZONERECORD_A $dir/SUBDEVZONERECORD_A
			else if (! -f $indir/SUBDEVZONERECORD_A) then
				echo "-- 科目別成績層別成績(全国)[SUBDEVZONERECORD_A]【RZRG1100】が存在しません --" 
			endif
		endif

		#-- ログ出力
		if ($#schoolcdlist != 0) then
			echo " grep $gstring start..."
		else 
			echo " grep 全件コピー start..."
		endif


		if (-f $indir/EXAMTAKENUM_S) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/EXAMTAKENUM_S $dir/EXAMTAKENUM_S
			else
				grep $gstring $indir/EXAMTAKENUM_S >> $dir/EXAMTAKENUM_S

				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/EXAMTAKENUM_S) then
					\rm -f $dir/EXAMTAKENUM_S
#					echo "-- 模試受験者総数(高校)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- 模試受験者総数(高校)[EXAMTAKENUM_S]【RZRG1380】が存在しません --"
			endif 
		endif
		if (-f $indir/EXAMTAKENUM_C) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/EXAMTAKENUM_C $dir/EXAMTAKENUM_C
			else
				grep $gstring $indir/EXAMTAKENUM_C >> $dir/EXAMTAKENUM_C
			
				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/EXAMTAKENUM_C) then
					\rm -f $dir/EXAMTAKENUM_C
#					echo "-- 模試受験者総数(クラス)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- 模試受験者総数(クラス)[EXAMTAKENUM_C]【RZRG1480】が存在しません --" 
			endif 
		endif
		if (-f $indir/SUBRECORD_S) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/SUBRECORD_S $dir/SUBRECORD_S
			else
				grep $gstring $indir/SUBRECORD_S >> $dir/SUBRECORD_S
			
				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/SUBRECORD_S) then
					\rm -f $dir/SUBRECORD_S
#					echo "-- 科目別成績(高校)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- 科目別成績(高校)[SUBRECORD_S]【RZRG1310】が存在しません --" 
			endif
		endif
		if (-f $indir/SUBRECORD_C) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/SUBRECORD_C $dir/SUBRECORD_C
			else
				grep $gstring $indir/SUBRECORD_C >> $dir/SUBRECORD_C
			
				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/SUBRECORD_C) then
					\rm -f $dir/SUBRECORD_C
#					echo "-- 科目別成績(クラス)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- 科目別成績(クラス)[SUBRECORD_C]【RZRG1410】が存在しません --" 
			endif
		endif
		if (-f $indir/QRECORD_S) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/QRECORD_S $dir/QRECORD_S
			else
				grep $gstring $indir/QRECORD_S >> $dir/QRECORD_S
			
				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/QRECORD_S) then
					\rm -f $dir/QRECORD_S
#					echo "-- 設問別成績(高校)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($exam2 != 38)&&($examtystr != "rerun")) then
				echo "-- 設問別成績(高校)[QRECORD_S]【RZRG1330】が存在しません --" 
			endif
		endif
		if (-f $indir/QRECORD_C) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/QRECORD_C $dir/QRECORD_C
			else
				grep $gstring $indir/QRECORD_C >> $dir/QRECORD_C
			
				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/QRECORD_C) then
					\rm -f $dir/QRECORD_C
#					echo "-- 設問別成績(クラス)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($exam2 != 38)&&($examtystr != "rerun")) then
				echo "-- 設問別成績(クラス)[QRECORD_C]【RZRG1430】が存在しません --" 
			endif
		endif
		if (-f $indir/QRECORDZONE) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/QRECORDZONE $dir/QRECORDZONE
			else
				grep $gstring $indir/QRECORDZONE >> $dir/QRECORDZONE
			
				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/QRECORDZONE) then
                                        \rm -f $dir/QRECORDZONE
#					echo "-- 設問別成績層別成績データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($exam2 != 38)&&($examtystr != "rerun")) then
				echo "-- 設問別成績層別成績[QRECORDZONE]【RZRG1340】が存在しません --" 
			endif
		endif
		if (-f $indir/SUBDISTRECORD_S) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/SUBDISTRECORD_S $dir/SUBDISTRECORD_S
			else
				grep $gstring $indir/SUBDISTRECORD_S >> $dir/SUBDISTRECORD_S
				
				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/SUBDISTRECORD_S) then
                                        \rm -f $dir/SUBDISTRECORD_S
#					echo "-- 科目分布別成績(高校)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- 科目分布別成績(高校)[SUBDISTRECORD_S]【RZRG1320】が存在しません --" 
			endif
		endif
		if (-f $indir/SUBDISTRECORD_C) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/SUBDISTRECORD_C $dir/SUBDISTRECORD_C
			else
				grep $gstring $indir/SUBDISTRECORD_C >> $dir/SUBDISTRECORD_C

				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/SUBDISTRECORD_C) then
                                        \rm -f $dir/SUBDISTRECORD_C
#					echo "-- 科目分布別成績(クラス)データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- 科目分布別成績(クラス)[SUBDISTRECORD_C]【RZRG1420】が存在しません --" 
			endif
		endif
		if (-f $indir/RATINGNUMBER_S) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/RATINGNUMBER_S $dir/RATINGNUMBER_S
			else
				grep $gstring $indir/RATINGNUMBER_S >> $dir/RATINGNUMBER_S

				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/RATINGNUMBER_S) then
                                        \rm -f $dir/RATINGNUMBER_S
#					echo "-- 志望大学評価別人数(高校)データには、検証校は存在しませんでした。"
				endif
				
			endif
		else
			if (($examtystr != "score")&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 74)&&($exam2 != 65)&&($examtystr != "rerun")) then
				echo "-- 志望大学評価別人数(高校)[RATINGNUMBER_S]【RZRG1350】が存在しません --" 
			endif
		endif
		if (-f $indir/RATINGNUMBER_C) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/RATINGNUMBER_C $dir/RATINGNUMBER_C
			else
				grep $gstring $indir/RATINGNUMBER_C >> $dir/RATINGNUMBER_C

				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/RATINGNUMBER_C) then
					\rm -f $dir/RATINGNUMBER_C
#					echo "-- 志望大学評価別人数(クラス)データには、検証校は存在しませんでした。"
				endif
				
			endif
		else
			if (($examtystr != "score")&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 74)&&($exam2 != 65)&&($examtystr != "rerun")) then
				echo "-- 志望大学評価別人数(クラス)[RATINGNUMBER_C]【RZRG1450】が存在しません --" 
			endif
		endif
		if (-f $indir/MARKQSCORERATE) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/MARKQSCORERATE $dir/MARKQSCORERATE
			else
				grep $gstring $indir/MARKQSCORERATE >> $dir/MARKQSCORERATE

				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/MARKQSCORERATE) then
					\rm -f $dir/MARKQSCORERATE
#					echo "-- マーク模試高校設問別正答率データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if ((($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66))&&($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- マーク模試高校設問別正答率[MARKQSCORERATE]【RZRG1360】が存在しません --" 
			endif
		endif
		if (-f $indir/MARKQDEVZONEANSRATE_S) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/MARKQDEVZONEANSRATE_S $dir/MARKQDEVZONEANSRATE_S
			else
				grep $gstring $indir/MARKQDEVZONEANSRATE_S >> $dir/MARKQDEVZONEANSRATE_S

				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/MARKQDEVZONEANSRATE_S) then
					\rm -f $dir/MARKQDEVZONEANSRATE_S
#					echo "-- マーク模試高校別設問成績層別正答率データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if ((($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66))&&($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- マーク模試高校別設問成績層別正答率[MARKQDEVZONEANSRATE_S]【RZRG1390】が存在しません --" 
			endif
		endif
		if (-f $indir/SUBDEVZONERECORD_S) then
			if ($#schoolcdlist == 0) then
				cp -p $indir/SUBDEVZONERECORD_S $dir/SUBDEVZONERECORD_S
			else
				grep $gstring $indir/SUBDEVZONERECORD_S >> $dir/SUBDEVZONERECORD_S

				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/SUBDEVZONERECORD_S) then
					\rm -f $dir/SUBDEVZONERECORD_S
#					echo "-- 科目別成績層別成績データには、検証校は存在しませんでした。"
				endif
			endif
		else
			if (($examtystr != "score")&&($examtystr != "rerun")) then
				echo "-- 科目別成績層別成績[SUBDEVZONERECORD_S]【RZRG1300】が存在しません --" 
			endif
		endif
		if (-f $indir/INDIVIDUALRECORD) then
			if (($getflg == -1)&&(-f $indir/INDIVIDUALRECORD-old)) then
				mv $indir/INDIVIDUALRECORD $indir/INDIVIDUALRECORD-zitaku
				mv $indir/INDIVIDUALRECORD-old $indir/INDIVIDUALRECORD
			endif
			if ($#schoolcdlist == 0) then
				cp -p $indir/INDIVIDUALRECORD $dir/INDIVIDUALRECORD
				if (($getflg == -1)&&(-f $indir/INDIVIDUALRECORD-zitaku)) then
					cp -p $indir/INDIVIDUALRECORD-zitaku $dir/INDIVIDUALRECORD-zitaku
				endif
			else
				grep $gstring2 $indir/INDIVIDUALRECORD >> $dir/INDIVIDUALRECORD

				#-- grep結果が0件(0バイトファイル)の場合はファイル削除
				if (! -s $dir/INDIVIDUALRECORD) then
					\rm -f $dir/INDIVIDUALRECORD
#					echo "-- 個表データには、検証校は存在しませんでした。"
				endif
				
			endif
		else
			if ($examtystr != "rerun") then
				echo "-- 個表データ[INDIVIDUALRECORD]【RZRG2110】が存在しません --" 
			endif
		endif

		rmdir $dir >& /dev/null
		if ($status != 0) then
			echo "${year}selection" >> $filepath/grepexamstr
		endif

		@ count5 ++
	end

	echo " grep END"

	#--- 抽出データにより上書きされるため、最新の自宅解答データも登録対象にする
	if (-d /keinavi/HULFT/NewEXAM/${orginyear}zigo) then
		if (-f /keinavi/HULFT/NewEXAM/${orginyear}zigo/INDIVIDUALRECORD-zitaku) then
			if (! -d $filepath/temp/${orginyear}zigoselection) then
				mkdir $filepath/temp/${orginyear}zigoselection
				chmod 777 $filepath/temp/${orginyear}zigoselection
			endif
			grep $gstring2 /keinavi/HULFT/NewEXAM/${orginyear}zigo/INDIVIDUALRECORD-zitaku >> $filepath/temp/${orginyear}zigoselection/INDIVIDUALRECORD 
			
			#-- grep結果が0件時はファイル削除。そうでなければ登録対象にする。
			if (! -s $filepath/temp/${orginyear}zigoselection/INDIVIDUALRECORD) then
				\rm -f $filepath/temp/${orginyear}zigoselection/INDIVIDUALRECORD
				rmdir $filepath/temp/${orginyear}zigoselection
			else
				echo "${orgyear}zigoselection" >> $filepath/grepexamstr
			endif
		endif
	endif

	#--- 抽出データにより上書きされるため、最新の得点修正データも登録対象にする
	if (-d /keinavi/HULFT/NewEXAM/${orginyear}score) then
		if (! -d $filepath/temp/${orginyear}scoreselection) then
			mkdir $filepath/temp/${orginyear}scoreselection
			chmod 777 $filepath/temp/${orginyear}scoreselection
		endif
		grep $gstring2 /keinavi/HULFT/NewEXAM/${orginyear}score/INDIVIDUALRECORD >> $filepath/temp/${orginyear}scoreselection/INDIVIDUALRECORD 
	
		#-- grep結果が0件時はファイル削除。そうでなければ登録対象にする。
		if (! -s $filepath/temp/${orginyear}scoreselection/INDIVIDUALRECORD) then
			\rm -f $filepath/temp/${orginyear}scoreselection/INDIVIDUALRECORD
			rmdir $filepath/temp/${orginyear}scoreselection
		else
			echo "${orgyear}scoreselection" >> $filepath/grepexamstr
		endif
	endif


	if ($getflg != 1) then
		rm -rf $targetfile
	endif

	@ count ++
end
if (-f $filepath/grepexamstr) then
	chmod 664 $filepath/grepexamstr
endif

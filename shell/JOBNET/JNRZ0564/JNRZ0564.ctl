------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 大学マスタ公表用科目
--  テーブル名      : UNIVMASTER_SUBJECT_PUB
--  データファイル名: UNIVMASTER_SUBJECT_PUB
--  機能            : 大学マスタ公表用科目をデータファイルの内容に置き換える
--  作成日          : 2010/02/10
--  修正日          : 
--  備考            : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE UNIVMASTER_SUBJECT_PUB_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    EVENTYEAR,
    SCHOOLDIV,
    UNIVCD,
    EXAMDIV,
    ENTEXAMDIV,
    UNICDBRANCHCD  CHAR "LPAD(:UNICDBRANCHCD, 3, '0')",
    EXAMSUBCD,
    COURSECD,
    SUBBIT,
    SUBPNT         INTEGER EXTERNAL "DECODE(:SUBPNT,   '99999', null, :SUBPNT)",
    SUBAREA
)

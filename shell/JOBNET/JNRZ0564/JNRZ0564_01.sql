-- ************************************************
-- *機 能 名 : 大学マスタ公表用科目
-- *機 能 ID : JNRZ0564_01.sql
-- *作 成 日 : 2010/02/10
-- *作 成 者 : 
-- *更 新 日 : 
-- *更新内容 : 
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE UNIVMASTER_SUBJECT_PUB_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE UNIVMASTER_SUBJECT_PUB_TMP  (
       EVENTYEAR                    CHAR(4)                NOT NULL,   /*   行事年度                    */
       SCHOOLDIV                    CHAR(1)                NOT NULL,   /*   学校区分                    */
       UNIVCD                       CHAR(10)               NOT NULL,   /*   大学コード                  */
       EXAMDIV                      CHAR(2)                NOT NULL,   /*   模試区分                    */
       ENTEXAMDIV                   CHAR(1)                NOT NULL,   /*   入試試験区分                */
       UNICDBRANCHCD                CHAR(3)                NOT NULL,   /*   大学コード枝番              */
       EXAMSUBCD                    CHAR(4)                NOT NULL,   /*   受験科目コード              */
       COURSECD                     CHAR(1)                        ,   /*   教科コード                  */
       SUBBIT                       CHAR(2)                        ,   /*   科目ビット                  */
       SUBPNT                       NUMBER(5,1)                    ,   /*   科目配点                    */
       SUBAREA                      CHAR(2)                        ,   /*   科目範囲                    */
       CONSTRAINT PK_UNIVMASTER_SUBJECT_PUB_TMP PRIMARY KEY (
                 EVENTYEAR       ,               /*  行事年度       */
                 SCHOOLDIV       ,               /*  学校区分       */
                 UNIVCD          ,               /*  大学コード     */
                 EXAMDIV         ,               /*  模試区分       */
                 ENTEXAMDIV      ,               /*  入試試験区分   */
                 UNICDBRANCHCD   ,               /*  大学コード枝番 */
                 EXAMSUBCD                       /*  受験科目コード */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

-- ************************************************
-- *機 能 名 : 大学マスタ公表用科目
-- *機 能 ID : JNRZ0564_01.sql
-- *作 成 日 : 2010/02/10
-- *作 成 者 : 
-- *更 新 日 : 
-- *更新内容 : 
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE UNIVMASTER_SUBJECT_PUB_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

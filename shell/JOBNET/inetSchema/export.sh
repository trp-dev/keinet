#!/bin/bash


#パラメータチェック
if [ "$1" = "" ]; then
    echo "パラメータ１：年度が未設定です。"
    exit 1
fi
expr "$1" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ１：年度が不正です。【$1】"
    exit 1
fi

if [ "$2" = "" ]; then
    echo "パラメータ２：模試区分が未設定です。"
    exit 1
fi
expr "$2" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ２：模試区分が不正です。【$2】"
    exit 1
fi

#変数定義
year=$1
div=$2
con=keinavi/39ra8ma@S11DBA_BATCH2
base=/keinavi/bat/inetSchema
log=$base/logs/exp
dat=$base/data/$year$div

mkdir $dat

#エクスポート
exp USERID=$con TABLES=RANK                       LOG=$log/RANK.log                       FILE=$dat/RANK.dmp
exp USERID=$con TABLES=UNIVMASTER_BASIC           LOG=$log/UNIVMASTER_BASIC.log           FILE=$dat/UNIVMASTER_BASIC.dmp           query=\"WHERE eventyear=$year and examdiv=$div\"
exp USERID=$con TABLES=PREFECTURE                 LOG=$log/PREFECTURE.log                 FILE=$dat/PREFECTURE.dmp
exp USERID=$con TABLES=GUIDEREMARKS               LOG=$log/GUIDEREMARKS.log               FILE=$dat/GUIDEREMARKS.dmp
exp USERID=$con TABLES=UNIVMASTER_INFO            LOG=$log/UNIVMASTER_INFO.log            FILE=$dat/UNIVMASTER_INFO.dmp
exp USERID=$con TABLES=UNIVMASTER_OTHERNAME       LOG=$log/UNIVMASTER_OTHERNAME.log       FILE=$dat/UNIVMASTER_OTHERNAME.dmp
exp USERID=$con TABLES=UNIVMASTER_CHOICESCHOOL    LOG=$log/UNIVMASTER_CHOICESCHOOL.log    FILE=$dat/UNIVMASTER_CHOICESCHOOL.dmp    query=\"WHERE eventyear=$year and examdiv=$div\"
exp USERID=$con TABLES=GUIDESUBSTRING             LOG=$log/GUIDESUBSTRING.log             FILE=$dat/GUIDESUBSTRING.dmp
exp USERID=$con TABLES=UNIVMASTER_COURSE          LOG=$log/UNIVMASTER_COURSE.log          FILE=$dat/UNIVMASTER_COURSE.dmp          query=\"WHERE eventyear=$year and examdiv=$div\"
exp USERID=$con TABLES=UNIVMASTER_SUBJECT         LOG=$log/UNIVMASTER_SUBJECT.log         FILE=$dat/UNIVMASTER_SUBJECT.dmp         query=\"WHERE eventyear=$year and examdiv=$div\"
exp USERID=$con TABLES=UNIVMASTER_SELECTGROUP     LOG=$log/UNIVMASTER_SELECTGROUP.log     FILE=$dat/UNIVMASTER_SELECTGROUP.dmp     query=\"WHERE eventyear=$year and examdiv=$div\"
exp USERID=$con TABLES=UNIVMASTER_SELECTGPCOURSE  LOG=$log/UNIVMASTER_SELECTGPCOURSE.log  FILE=$dat/UNIVMASTER_SELECTGPCOURSE.dmp  query=\"WHERE eventyear=$year and examdiv=$div\"
exp USERID=$con TABLES=UNIVSTEMMA                 LOG=$log/UNIVSTEMMA.log                 FILE=$dat/UNIVSTEMMA.dmp
exp USERID=$con TABLES=JH01_KAMOKU1               LOG=$log/JH01_KAMOKU1.log               FILE=$dat/JH01_KAMOKU1.dmp
exp USERID=$con TABLES=JH01_KAMOKU2               LOG=$log/JH01_KAMOKU2.log               FILE=$dat/JH01_KAMOKU2.dmp
exp USERID=$con TABLES=JH01_KAMOKU3               LOG=$log/JH01_KAMOKU3.log               FILE=$dat/JH01_KAMOKU3.dmp
exp USERID=$con TABLES=JH01_KAMOKU4               LOG=$log/JH01_KAMOKU4.log               FILE=$dat/JH01_KAMOKU4.dmp
exp USERID=$con TABLES=CEFR_INFO                  LOG=$log/CEFR_INFO.log                  FILE=$dat/CEFR_INFO.dmp
exp USERID=$con TABLES=ENGPTINFO                  LOG=$log/ENGPTINFO.log                  FILE=$dat/ENGPTINFO.dmp
exp USERID=$con TABLES=ENGPTDETAILINFO            LOG=$log/ENGPTDETAILINFO.log            FILE=$dat/ENGPTDETAILINFO.dmp
exp USERID=$con TABLES=CEFRCVSSCOREINFO           LOG=$log/CEFRCVSSCOREINFO.log           FILE=$dat/CEFRCVSSCOREINFO.dmp
exp USERID=$con TABLES=DESCCOMPRATING             LOG=$log/DESCCOMPRATING.log             FILE=$dat/DESCCOMPRATING.dmp
exp USERID=$con TABLES=EXAMINATION                LOG=$log/EXAMINATION.log                FILE=$dat/EXAMINATION.dmp
exp USERID=$con TABLES=EXAMSUBJECT                LOG=$log/EXAMSUBJECT.log                FILE=$dat/EXAMSUBJECT.dmp

#!/bin/bash


#パラメータチェック
if [ "$1" = "" ]; then
    echo "パラメータ１：年度が未設定です。"
    exit 1
fi
expr "$1" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ１：年度が不正です。【$1】"
    exit 1
fi

if [ "$2" = "" ]; then
    echo "パラメータ２：模試区分が未設定です。"
    exit 1
fi
expr "$2" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ２：模試区分が不正です。【$2】"
    exit 1
fi

#変数定義
year=$1
div=$2
con=moshihantei/39ra8ma@S11DBA_BATCH2
base=/keinavi/bat/inetSchema
log=$base/logs/imp
dat=$base/data/$year$div

#データ切り捨て
sqlplus $con @/keinavi/JOBNET/inetSchema/truncate.sql

#インポート
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/RANK.log                       FILE=$dat/RANK.dmp ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/UNIVMASTER_BASIC.log           FILE=$dat/UNIVMASTER_BASIC.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/PREFECTURE.log                 FILE=$dat/PREFECTURE.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/GUIDEREMARKS.log               FILE=$dat/GUIDEREMARKS.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/UNIVMASTER_INFO.log            FILE=$dat/UNIVMASTER_INFO.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/UNIVMASTER_OTHERNAME.log       FILE=$dat/UNIVMASTER_OTHERNAME.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/UNIVMASTER_CHOICESCHOOL.log    FILE=$dat/UNIVMASTER_CHOICESCHOOL.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/GUIDESUBSTRING.log             FILE=$dat/GUIDESUBSTRING.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/UNIVMASTER_COURSE.log          FILE=$dat/UNIVMASTER_COURSE.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/UNIVMASTER_SUBJECT.log         FILE=$dat/UNIVMASTER_SUBJECT.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/UNIVMASTER_SELECTGROUP.log     FILE=$dat/UNIVMASTER_SELECTGROUP.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/UNIVMASTER_SELECTGPCOURSE.log  FILE=$dat/UNIVMASTER_SELECTGPCOURSE.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/UNIVSTEMMA.log                 FILE=$dat/UNIVSTEMMA.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/JH01_KAMOKU1.log               FILE=$dat/JH01_KAMOKU1.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/JH01_KAMOKU2.log               FILE=$dat/JH01_KAMOKU2.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/JH01_KAMOKU3.log               FILE=$dat/JH01_KAMOKU3.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/JH01_KAMOKU4.log               FILE=$dat/JH01_KAMOKU4.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/CEFR_INFO.log                  FILE=$dat/CEFR_INFO.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/ENGPTINFO.log                  FILE=$dat/ENGPTINFO.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/ENGPTDETAILINFO.log            FILE=$dat/ENGPTDETAILINFO.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/CEFRCVSSCOREINFO.log           FILE=$dat/CEFRCVSSCOREINFO.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/DESCCOMPRATING.log             FILE=$dat/DESCCOMPRATING.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/EXAMINATION.log                FILE=$dat/EXAMINATION.dmp  ignore=y
imp USERID=$con  fromuser=keinavi touser=moshihantei LOG=$log/EXAMSUBJECT.log                FILE=$dat/EXAMSUBJECT.dmp ignore=y

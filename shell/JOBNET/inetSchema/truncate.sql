SPOOL truncate.log
SET ECHO ON

truncate table RANK;
truncate table UNIVMASTER_BASIC;
truncate table PREFECTURE;
truncate table GUIDEREMARKS;
truncate table UNIVMASTER_INFO;
truncate table UNIVMASTER_OTHERNAME;
truncate table UNIVMASTER_CHOICESCHOOL;
truncate table GUIDESUBSTRING;
truncate table UNIVMASTER_COURSE;
truncate table UNIVMASTER_SUBJECT;
truncate table UNIVMASTER_SELECTGROUP;
truncate table UNIVMASTER_SELECTGPCOURSE;
truncate table UNIVSTEMMA;
truncate table JH01_KAMOKU1;
truncate table JH01_KAMOKU2;
truncate table JH01_KAMOKU3;
truncate table JH01_KAMOKU4;
truncate table CEFR_INFO;
truncate table ENGPTINFO;
truncate table ENGPTDETAILINFO;
truncate table CEFRCVSSCOREINFO;
truncate table DESCCOMPRATING;
truncate table EXAMINATION;
truncate table EXAMSUBJECT;

EXIT

#!/bin/sh

src=/keinavi/JOBNET/inetSchema/
dst=/usr/local/tomcat/webapps2/inet/WEB-INF/classes/jp/co/fj/keinavi/resources/

echo ***** satu01にハイブリッド版のcommon.propertiesをコピー *****
rsync -avz ${src}common.properties.hybrid kei-navi@satu01:${dst}common.properties 

echo ***** satu02にハイブリッド版のcommon.propertiesをコピー *****
rsync -avz ${src}common.properties.hybrid kei-navi@satu02:${dst}common.properties

echo ***** satu03にハイブリッド版のcommon.propertiesをコピー *****
rsync -avz ${src}common.properties.hybrid kei-navi@satu03:${dst}common.properties

#!/bin/sh

src=/keinavi/JOBNET/inetSchema/
dst=/usr/local/tomcat/webapps2/inet/WEB-INF/classes/jp/co/fj/keinavi/resources/

echo ***** satu01testにハイブリッド版のcommon.propertiesをコピー *****
rsync -avz ${src}common.properties.hybrid.test kei-navi@satu01test:${dst}common.properties 

echo ***** satu02testにハイブリッド版のcommon.propertiesをコピー *****
rsync -avz ${src}common.properties.hybrid.test kei-navi@satu02test:${dst}common.properties

echo ***** satu03testにハイブリッド版のcommon.propertiesをコピー *****
rsync -avz ${src}common.properties.hybrid.test kei-navi@satu03test:${dst}common.properties

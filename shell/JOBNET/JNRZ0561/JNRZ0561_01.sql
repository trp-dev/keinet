-- ************************************************
-- *機 能 名 : 大学基本情報（公表用教科配点）
-- *機 能 ID : JNRZ0561_01.sql
-- *作 成 日 : 2010/02/10
-- *作 成 者 : 
-- *更 新 日 : 2019/9/10
-- *更新内容 : 
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE UNIVMASTER_BASIC_PUB_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE UNIVMASTER_BASIC_PUB_TMP  (
       EVENTYEAR                  CHAR(4)              NOT NULL,    /*   行事年度                 */ 
       EXAMDIV                    CHAR(2)              NOT NULL,    /*   模試区分                 */ 
       SCHOOLDIV                  CHAR(1)                      ,    /*   学校区分                 */ 
       UNIDIV                     CHAR(2)                      ,    /*   大学区分                 */ 
       UNIVCD                     CHAR(10)             NOT NULL,    /*   募集区分コード           */ 
       CHOICECD5                  CHAR(5)                      ,    /*   志望校コード５桁         */ 
       CDEXAMDIV                  CHAR(2)                      ,    /*   コード発生模試区分       */ 
       CDVALIDEXAMDIV             CHAR(2)                      ,    /*   コード有効模試区分       */ 
       ENTEXAMMODE                CHAR(1)                      ,    /*   入試形態                 */ 
       UNIGDIV                    CHAR(1)                      ,    /*   大学グループ区分         */ 
       APPLILIMITDIV              CHAR(1)                      ,    /*   出願制限区分             */ 
       FACULTYCONCD               CHAR(3)                      ,    /*   学部内容コード           */ 
       DEPTSERIAL_NO              CHAR(4)                      ,    /*   学科通番                 */ 
       LDISTRICTCD                CHAR(1)                      ,    /*   大地区コード             */ 
       BUSINESSCD                 CHAR(3)                      ,    /*   営業コード               */ 
       PREFCD_EXAMSH              CHAR(2)                      ,    /*   受験校校舎県コード       */ 
       PREFCD_EXAMHO              CHAR(2)                      ,    /*   受験校本部県コード       */ 
       ENTEXAMSTEMMACD_M          CHAR(2)                      ,    /*   入試系統コード－中分類   */ 
       ENTEXAMSTEMMACD_S1         CHAR(2)                      ,    /*   入試系統コード－小分類1  */ 
       ENTEXAMSTEMMACD_S2         CHAR(2)                      ,    /*   入試系統コード－小分類2  */ 
       ENTEXAMSTEMMACD_S3         CHAR(2)                      ,    /*   入試系統コード－小分類3  */ 
       ENTEXAMSTEMMACD_S4         CHAR(2)                      ,    /*   入試系統コード－小分類4  */ 
       ENTEXAMSTEMMACD_S5         CHAR(2)                      ,    /*   入試系統コード－小分類5  */ 
       ENTEXAMSTEMMACD_S6         CHAR(2)                      ,    /*   入試系統コード－小分類6  */ 
       ENTEXAMSTEMMACD_S7         CHAR(2)                      ,    /*   入試系統コード－小分類7  */ 
       ENTEXAMSTEMMACD_S8         CHAR(2)                      ,    /*   入試系統コード－小分類8  */ 
       ENTEXAMSTEMMACD_S9         CHAR(2)                      ,    /*   入試系統コード－小分類9  */ 
       ENTEXAMSTEMMACD_S10        CHAR(2)                      ,    /*   入試系統コード－小分類10 */ 
       BUNRICD                    CHAR(1)                      ,    /*   文理コード               */ 
       UNIVNAME                   VARCHAR2(48)                 ,    /*   大学名                   */ 
       UNINAME_ABBR               VARCHAR2(36)                 ,    /*   大学短縮名               */ 
       UNIVNAME_KANA              VARCHAR2(30)                 ,    /*   大学カナ名               */ 
       PUBENTEXAMCAPA             NUMBER(7,0)                  ,    /*   一般入試定員             */ 
       EXAMCAPAREL                CHAR(1)                      ,    /*   入試定員信頼性           */ 
       UNICOEDDIV                 CHAR(1)                      ,    /*   大学男女共学区分         */ 
       UNINIGHTDIV                CHAR(1)                      ,    /*   学部昼夜区分             */ 
       UNIGNAME                   VARCHAR2(2)                  ,    /*   大学グループ名           */ 
       ENTEXAMORNDIV1             CHAR(1)                      ,    /*   入試試験有無区分1        */ 
       ENTEXAMORNDIV2             CHAR(1)                      ,    /*   入試試験有無区分2        */ 
       ENTEXAMORNDIV3             CHAR(1)                      ,    /*   入試試験有無区分3        */ 
       PUBREJECTCOMP              NUMBER(4,2)                  ,    /*   公表用足切倍率           */ 
       PUBREJECTEXSCORE           NUMBER(4,0)                  ,    /*   公表用足切予想得点       */ 
       ENTEXAMRANK                CHAR(2)                      ,    /*   入試ランク_今年          */ 
       ENTEXAMRANK_DEVI           NUMBER(5,1)                  ,    /*   入試ランク偏差値_今年    */ 
       BORDER_IN_DIV              CHAR(1)                      ,    /*   ボーダ入力区分_今年      */ 
       BORDERSCORE1               NUMBER(5,0)                  ,    /*   ボーダ得点1_今年         */ 
       BORDERSCORE2               NUMBER(5,0)                  ,    /*   ボーダ得点2_今年         */ 
       BORDERSCORE3               NUMBER(5,0)                  ,    /*   ボーダ得点3_今年         */ 
       BORDERSCORERATE1           NUMBER(3,0)                  ,    /*   ボーダ得点率1_今年       */ 
       BORDERSCORERATE2           NUMBER(3,0)                  ,    /*   ボーダ得点率2_今年       */ 
       BORDERSCORERATE3           NUMBER(3,0)                  ,    /*   ボーダ得点率3_今年       */ 
       BORDERSCORE1_ENGPT_NON     NUMBER(5,0)                  ,    /*   ボーダ得点1（認定試験無し）_今年   */ 
       BORDERSCORE2_ENGPT_NON     NUMBER(5,0)                  ,    /*   ボーダ得点2（認定試験無し）_今年   */ 
       BORDERSCORE3_ENGPT_NON     NUMBER(5,0)                  ,    /*   ボーダ得点3（認定試験無し）_今年   */ 
       BORDERSCORERATE1_ENGPT_NON NUMBER(3,0)                  ,    /*   ボーダ得点率1（認定試験無し）_今年 */ 
       BORDERSCORERATE2_ENGPT_NON NUMBER(3,0)                  ,    /*   ボーダ得点率2（認定試験無し）_今年 */ 
       BORDERSCORERATE3_ENGPT_NON NUMBER(3,0)                  ,    /*   ボーダ得点率3（認定試験無し）_今年 */ 
       REJECTEXSCORE              NUMBER(5,0)                  ,    /*   足切予想得点 _今年       */ 
       REJECTSCORERATE            NUMBER(3,0)                  ,    /*   足切得点率_今年          */ 
       TMP1_1                     CHAR(2)                      ,    /*   入試ランク_前回          */ 
       TMP1_2                     NUMBER(5,1)                  ,    /*   入試ランク偏差値_前回    */ 
       TMP1_3                     CHAR(1)                      ,    /*   ボーダ入力区分_前回      */ 
       TMP1_4                     NUMBER(5,0)                  ,    /*   ボーダ得点1_前回         */ 
       TMP1_5                     NUMBER(5,0)                  ,    /*   ボーダ得点2_前回         */ 
       TMP1_6                     NUMBER(5,0)                  ,    /*   ボーダ得点3_前回         */ 
       TMP1_7                     NUMBER(3,0)                  ,    /*   ボーダ得点率1_前回       */ 
       TMP1_8                     NUMBER(3,0)                  ,    /*   ボーダ得点率2_前回       */ 
       TMP1_9                     NUMBER(3,0)                  ,    /*   ボーダ得点率3_前回       */ 
       TMP1_10                    NUMBER(5,0)                  ,    /*   ボーダ得点1（認定試験無し）_前回   */ 
       TMP1_11                    NUMBER(5,0)                  ,    /*   ボーダ得点2（認定試験無し）_前回   */ 
       TMP1_12                    NUMBER(5,0)                  ,    /*   ボーダ得点3（認定試験無し）_前回   */ 
       TMP1_13                    NUMBER(3,0)                  ,    /*   ボーダ得点率1（認定試験無し）_前回 */ 
       TMP1_14                    NUMBER(3,0)                  ,    /*   ボーダ得点率2（認定試験無し）_前回 */ 
       TMP1_15                    NUMBER(3,0)                  ,    /*   ボーダ得点率3（認定試験無し）_前回 */ 
       TMP1_16                    NUMBER(5,0)                  ,    /*   足切予想得点 _前回       */ 
       TMP1_17                    NUMBER(3,0)                  ,    /*   足切得点率_前回          */ 
       TMP2_1                     CHAR(2)                      ,    /*   入試ランク_前年          */ 
       TMP2_2                     NUMBER(5,1)                  ,    /*   入試ランク偏差値_前年    */ 
       TMP2_3                     CHAR(1)                      ,    /*   ボーダ入力区分_前年      */ 
       TMP2_4                     NUMBER(5,0)                  ,    /*   ボーダ得点1_前年         */ 
       TMP2_5                     NUMBER(5,0)                  ,    /*   ボーダ得点2_前年         */ 
       TMP2_6                     NUMBER(5,0)                  ,    /*   ボーダ得点3_前年         */ 
       TMP2_7                     NUMBER(3,0)                  ,    /*   ボーダ得点率1_前年       */ 
       TMP2_8                     NUMBER(3,0)                  ,    /*   ボーダ得点率2_前年       */ 
       TMP2_9                     NUMBER(3,0)                  ,    /*   ボーダ得点率3_前年       */ 
       TMP2_10                    NUMBER(5,0)                  ,    /*   ボーダ得点1（認定試験無し）_前年   */ 
       TMP2_11                    NUMBER(5,0)                  ,    /*   ボーダ得点2（認定試験無し）_前年   */ 
       TMP2_12                    NUMBER(5,0)                  ,    /*   ボーダ得点3（認定試験無し）_前年   */ 
       TMP2_13                    NUMBER(3,0)                  ,    /*   ボーダ得点率1（認定試験無し）_前年 */ 
       TMP2_14                    NUMBER(3,0)                  ,    /*   ボーダ得点率2（認定試験無し）_前年 */ 
       TMP2_15                    NUMBER(3,0)                  ,    /*   ボーダ得点率3（認定試験無し）_前年 */ 
       TMP2_16                    NUMBER(5,0)                  ,    /*   足切予想得点 _前年       */ 
       TMP2_17                    NUMBER(3,0)                  ,    /*   足切得点率_前年          */ 
       TMP3_1                     CHAR(2)                      ,    /*   入試ランク_実態          */ 
       TMP3_2                     NUMBER(5,1)                  ,    /*   入試ランク偏差値_実態    */ 
       TMP3_3                     CHAR(1)                      ,    /*   ボーダ入力区分_実態      */ 
       TMP3_4                     NUMBER(5,0)                  ,    /*   ボーダ得点1_実態         */ 
       TMP3_5                     NUMBER(5,0)                  ,    /*   ボーダ得点2_実態         */ 
       TMP3_6                     NUMBER(5,0)                  ,    /*   ボーダ得点3_実態         */ 
       TMP3_7                     NUMBER(3,0)                  ,    /*   ボーダ得点率1_実態       */ 
       TMP3_8                     NUMBER(3,0)                  ,    /*   ボーダ得点率2_実態       */ 
       TMP3_9                     NUMBER(3,0)                  ,    /*   ボーダ得点率3_実態       */ 
       TMP3_10                    NUMBER(5,0)                  ,    /*   ボーダ得点1（認定試験無し）_実態   */ 
       TMP3_11                    NUMBER(5,0)                  ,    /*   ボーダ得点2（認定試験無し）_実態   */ 
       TMP3_12                    NUMBER(5,0)                  ,    /*   ボーダ得点3（認定試験無し）_実態   */ 
       TMP3_13                    NUMBER(3,0)                  ,    /*   ボーダ得点率1（認定試験無し）_実態 */ 
       TMP3_14                    NUMBER(3,0)                  ,    /*   ボーダ得点率2（認定試験無し）_実態 */ 
       TMP3_15                    NUMBER(3,0)                  ,    /*   ボーダ得点率3（認定試験無し）_実態 */ 
       TMP3_16                    NUMBER(5,0)                  ,    /*   足切予想得点 _実態       */ 
       TMP3_17                    NUMBER(3,0)                  ,    /*   足切得点率_実態          */ 
       DOCKINGNEEDLDIV            CHAR(1)                      ,    /*   ドッキング判定不要区分   */ 
       REP_MSTEMMA                CHAR(2)                      ,    /*   代表－中系統             */ 
       REP_SSTEMMA                CHAR(2)                      ,    /*   代表－小系統             */ 
       REP_REGION                 CHAR(2)                      ,    /*   代表－分野               */ 
       REP_BRANCHCD               CHAR(2)                      ,    /*   代表－枝番               */ 
       REGISTREORDIV              CHAR(1)                      ,    /*   戸籍改組区分             */ 
       SCHEDULESYS                CHAR(1)                      ,    /*   日程方式                 */ 
       SCHEDULESYSBRANCHCD        CHAR(1)                      ,    /*   日程方式枝番             */ 
       SCHEDULESYSNAME            VARCHAR2(48)                 ,    /*   日程方式名               */ 
       LMAINDIV                   CHAR(1)                      ,    /*   主要大区分               */ 
       ARTUNIDIV                  CHAR(1)                      ,    /*   芸大系区分               */ 
       COURSEALLOTPNT_EN_CENTER   NUMBER(5,1)                  ,    /*   英語教科配点             */ 
       COURSEALLOTPNT_MA_CENTER   NUMBER(5,1)                  ,    /*   数学教科配点             */ 
       COURSEALLOTPNT_JA_CENTER   NUMBER(5,1)                  ,    /*   国語教科配点             */ 
       COURSEALLOTPNT_SC_CENTER   NUMBER(5,1)                  ,    /*   理科教科配点             */ 
       COURSEALLOTPNT_SO_CENTER   NUMBER(5,1)                  ,    /*   社会教科配点             */ 
       COURSEALLOTPNT_ET_CENTER   NUMBER(5,1)                  ,    /*   その他教科配点           */ 
       COURSEALLOTPNT_EN_SECOND   NUMBER(5,1)                  ,    /*   英語教科配点             */ 
       COURSEALLOTPNT_MA_SECOND   NUMBER(5,1)                  ,    /*   数学教科配点             */ 
       COURSEALLOTPNT_JA_SECOND   NUMBER(5,1)                  ,    /*   国語教科配点             */ 
       COURSEALLOTPNT_SC_SECOND   NUMBER(5,1)                  ,    /*   理科教科配点             */ 
       COURSEALLOTPNT_SO_SECOND   NUMBER(5,1)                  ,    /*   社会教科配点             */ 
       COURSEALLOTPNT_ET_SECOND   NUMBER(5,1)                  ,    /*   その他教科配点           */ 
       COURSEALLOTPNT_EN_FIRST    NUMBER(5,1)                  ,    /*   英語教科配点             */ 
       COURSEALLOTPNT_MA_FIRST    NUMBER(5,1)                  ,    /*   数学教科配点             */ 
       COURSEALLOTPNT_JA_FIRST    NUMBER(5,1)                  ,    /*   国語教科配点             */ 
       COURSEALLOTPNT_SC_FIRST    NUMBER(5,1)                  ,    /*   理科教科配点             */ 
       COURSEALLOTPNT_SO_FIRST    NUMBER(5,1)                  ,    /*   社会教科配点             */ 
       COURSEALLOTPNT_ET_FIRST    NUMBER(5,1)                  ,    /*   その他教科配点           */ 
       PUBPRIUNIDIV               CHAR(1)                      ,    /*   国公立私大区分           */ 
       BORDERINPNEEDLDIV          CHAR(1)                      ,    /*   ボーダ入力不要区分       */ 
       RANKINPNEEDLDIV            CHAR(1)                      ,    /*   ランク入力不要区分       */ 
       EXAMUSEDIV                 CHAR(1)                      ,    /*   模試使用区分             */ 
       RESEACHUSEDIV              CHAR(1)                      ,    /*   リサーチ使用区分         */ 
       PURSUEUSEDIV               CHAR(1)                      ,    /*   追跡使用区分             */ 
       APP_REQ_PATTERN_CD         VARCHAR2(3)                  ,    /*   出願要件パターンコード   */ 
       CONSTRAINT PK_UNIVMASTER_BASIC_PUB_TMP PRIMARY KEY (
                 EVENTYEAR                 ,               /*  年度        */
                 EXAMDIV                   ,               /*  模試区分    */
                 UNIVCD                                    /*  大学コード  */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

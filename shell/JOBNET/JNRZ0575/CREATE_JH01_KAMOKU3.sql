-- ************************************************
-- *機 能 名 : 情報誌用科目（二次）
-- *機 能 ID : CREATE_JH01_KAMOKU3.sql
-- *作 成 日 : 2015/09/28
-- *作 成 者 : 富士通システムズ・ウエスト
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- 情報誌用科目（二次）テーブル削除
DROP TABLE JH01_KAMOKU3 CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 情報誌用科目（二次）テーブル作成
CREATE TABLE JH01_KAMOKU3  (
       COMBINATION_KEY1                 CHAR(5)        NOT NULL,    /*   記入用５桁コード                    */
       COMBINATION_KEY2                 CHAR(2)        NOT NULL,    /*   センタ枝番                          */
       COMBINATION_KEY3                 CHAR(2)        NOT NULL,    /*   二次枝番                            */
       SEC_SC_TRUST                     CHAR(1)                ,    /*   二次配点信頼                        */
       SEC_TOTAL_SC                     NUMBER(5,1)            ,    /*   二次総点                            */
       SEC_ENGLISH                      VARCHAR2(6)            ,    /*   二次英語                            */
       ENGLISH1                         CHAR(1)                ,    /*   英語１                              */
       ENGLISH2                         CHAR(1)                ,    /*   英語２                              */
       ORAL1                            CHAR(1)                ,    /*   オーラル１                          */
       ORAL2                            CHAR(1)                ,    /*   オーラル２                          */
       READING                          CHAR(1)                ,    /*   リーディング                        */
       WRITING                          CHAR(1)                ,    /*   ライティング                        */
       SEC_GERMAN                       VARCHAR2(6)            ,    /*   二次独語                            */
       SEC_FRENCH                       VARCHAR2(6)            ,    /*   二次仏語                            */
       SEC_CHINESE                      VARCHAR2(6)            ,    /*   二次中語                            */
       SEC_GAI                          VARCHAR2(6)            ,    /*   二次他外国語                        */
       SEC_L_D_RANGE                    CHAR(1)                ,    /*   二次ＬＤ範囲                        */
       SEC_GAI_TRUST_PT                 CHAR(1)                ,    /*   二次外国語－配点信頼                */
       SEC_GAI_SHO_PT                   NUMBER(5,1)            ,    /*   二次外国語－配点小                  */
       SEC_GAI_SHO_SEL                  CHAR(1)                ,    /*   二次外国語－必選小                  */
       SEC_GAI_DAI_PT                   NUMBER(5,1)            ,    /*   二次外国語－配点大                  */
       SEC_GAI_DAI_SEL                  CHAR(1)                ,    /*   二次外国語－必選大                  */
       SEC_GAI_PT_STATE                 CHAR(1)                ,    /*   二次外国語－配点テーブル有無        */
       SEC_MATH                         VARCHAR2(6)            ,    /*   二次数学                            */
       MATH1                            CHAR(1)                ,    /*   数学１                              */
       MATH2                            CHAR(1)                ,    /*   数学２                              */
       MATH3                            CHAR(1)                ,    /*   数学３                              */
       MATH_A                           CHAR(1)                ,    /*   数学Ａ                              */
       MATH_B                           CHAR(1)                ,    /*   数学Ｂ                              */
       MATH_A_PROBABILITY               CHAR(1)                ,    /*   数Ａ－場確                          */
       MATH_A_NO                        CHAR(1)                ,    /*   数Ａ－整数                          */
       MATH_A_DIAGRAM                   CHAR(1)                ,    /*   数Ａ－図形                          */
       MATH_B_PROBABILITY_STATISTICS    CHAR(1)                ,    /*   数Ｂ－確統                          */
       MATH_B_PROGRESSION               CHAR(1)                ,    /*   数Ｂ－数列                          */
       MATH_B_VECTOR                    CHAR(1)                ,    /*   数Ｂ－ベク                          */
       MATH_EXAM_RANGE                  CHAR(1)                ,    /*   数学模試範囲                        */
       SEC_MATH_TRUST_PT                CHAR(1)                ,    /*   二次数学－配点信頼                  */
       SEC_MATH_SHO_PT                  NUMBER(5,1)            ,    /*   二次数学－配点小                    */
       SEC_MATH_SHO_SEL                 CHAR(1)                ,    /*   二次数学－必選小                    */
       SEC_MATH_DAI_PT                  NUMBER(5,1)            ,    /*   二次数学－配点大                    */
       SEC_MATH_DAI_SEL                 CHAR(1)                ,    /*   二次数学－必選大                    */
       SEC_MATH_PT_STATE                CHAR(1)                ,    /*   二次数学－配点テーブル有無          */
       SEC_KOKUGO                       VARCHAR2(6)            ,    /*   二次国語                            */
       KOKUGO_TABLE1                    CHAR(1)                ,    /*   国表１                              */
       KOKUGO_TABLE2                    CHAR(1)                ,    /*   国表２                              */
       KOKUGO_TOTAL                     CHAR(1)                ,    /*   国語総合                            */
       MODERN_KOKUGO                    CHAR(1)                ,    /*   現代文                              */
       CLASSIC_KOKUGO                   CHAR(1)                ,    /*   古典                                */
       CLASSIC_KOKUGO_READ              CHAR(1)                ,    /*   古典講読                            */
       SEC_KOKUGO_RANGE                 CHAR(1)                ,    /*   二次現古漢範囲                      */
       SEC_KOKUGO_TRUST_PT              CHAR(1)                ,    /*   二次国語－配点信頼                  */
       SEC_KOKUGO_SHO_PT                NUMBER(5,1)            ,    /*   二次国語－配点小                    */
       SEC_KOKUGO_SHO_SEL               CHAR(1)                ,    /*   二次国語－必選小                    */
       SEC_KOKUGO_DAI_PT                NUMBER(5,1)            ,    /*   二次国語－配点大                    */
       SEC_KOKUGO_DAI_SEL               CHAR(1)                ,    /*   二次国語－必選大                    */
       SEC_KOKUGO_PT_STATE              CHAR(1)                ,    /*   二次国語－配点テーブル有無          */
       SEC_BUTU                         VARCHAR2(6)            ,    /*   二次物理                            */
       SEC_BUTU_RANGE                   CHAR(1)                ,    /*   二次物理範囲                        */
       SEC_KAGA                         VARCHAR2(6)            ,    /*   二次化学                            */
       SEC_KAGA_RANGE                   CHAR(1)                ,    /*   二次化学範囲                        */
       SEC_SEIBU                        VARCHAR2(6)            ,    /*   二次生物                            */
       SEC_SEIBU_RANGE                  CHAR(1)                ,    /*   二次生物範囲                        */
       SEC_EARTH_RIKA                   VARCHAR2(6)            ,    /*   二次地学                            */
       SEC_EARTH_RIKA_RANGE             CHAR(1)                ,    /*   二次地学範囲                        */
       SEC_RIKA_ITEM_CNT                NUMBER(1,0)            ,    /*   二次理科－科目数                    */
       SEC_RIKA_TRUST_PT                CHAR(1)                ,    /*   二次理科－配点信頼                  */
       SEC_RIKA_SHO_PT                  NUMBER(5,1)            ,    /*   二次理科－配点小                    */
       SEC_RIKA_SHO_SEL                 CHAR(1)                ,    /*   二次理科－必選小                    */
       SEC_RIKA_DAI_PT                  NUMBER(5,1)            ,    /*   二次理科－配点大                    */
       SEC_RIKA_DAI_SEL                 CHAR(1)                ,    /*   二次理科－必選大                    */
       SEC_RIKA_PT_STATE                CHAR(1)                ,    /*   二次理科－配点テーブル有無          */
       SEC_SEKAISHI                     VARCHAR2(6)            ,    /*   二次世界史                          */
       SEC_SEKAISHI_RANGE               CHAR(1)                ,    /*   二次世界史範囲                      */
       SEC_NIHONSHI                     VARCHAR2(6)            ,    /*   二次日本史                          */
       SEC_NIHONSHI_RANGE               CHAR(1)                ,    /*   二次日本史範囲                      */
       SEC_GEOGRAPHY                    VARCHAR2(6)            ,    /*   二次地理                            */
       SEC_GEOGRAPHY_RANGE              CHAR(1)                ,    /*   二次地理範囲                        */
       SEC_KINDAISHI                    VARCHAR2(6)            ,    /*   二次現社                            */
       SEC_ETHICS                       VARCHAR2(6)            ,    /*   二次倫理                            */
       SEC_POLITICS                     VARCHAR2(6)            ,    /*   二次政経                            */
       SEC_SOCIETY_ITEM_CNT             NUMBER(1,0)            ,    /*   二次地公－科目数                    */
       SEC_SOCIETY_TRUST_PT             CHAR(1)                ,    /*   二次地公－配点信頼                  */
       SEC_SOCIETY_SHO_PT               NUMBER(5,1)            ,    /*   二次地公－配点小                    */
       SEC_SOCIETY_SHO_SEL              CHAR(1)                ,    /*   二次地公－必選小                    */
       SEC_SOCIETY_DAI_PT               NUMBER(5,1)            ,    /*   二次地公－配点大                    */
       SEC_SOCIETY_DAI_SEL              CHAR(1)                ,    /*   二次地公－必選大                    */
       SEC_SOCIETY_PT_STATE             CHAR(1)                ,    /*   二次地公－配点テーブル有無          */
       SEC_ESSAY                        VARCHAR2(6)            ,    /*   二次小論文                          */
       SEC_ESSAY_TRUST_PT               CHAR(1)                ,    /*   二次小論文－配点信頼                */
       SEC_ESSAY_PT                     NUMBER(5,1)            ,    /*   二次小論文－配点                    */
       SEC_ESSAY_HISSU                  CHAR(1)                ,    /*   二次小論文－必選                    */
       SEC_TOTAL                        VARCHAR2(6)            ,    /*   二次総合                            */
       SEC_TOTAL_TRUST_PT               CHAR(1)                ,    /*   二次総合－配点信頼                  */
       SEC_TOTAL_PT                     NUMBER(5,1)            ,    /*   二次総合－配点                      */
       SEC_TOTAL_HISSU                  CHAR(1)                ,    /*   二次総合－必選                      */
       SEC_INTERVIEW                    VARCHAR2(6)            ,    /*   二次面接                            */
       SEC_INTERVIEW_TRUST_PT           CHAR(1)                ,    /*   二次面接－配点信頼                  */
       SEC_INTERVIEW_PT                 NUMBER(5,1)            ,    /*   二次面接－配点                      */
       SEC_INTERVIEW_HISSU              CHAR(1)                ,    /*   二次面接－必選                      */
       SEC_PLACTICAL                    VARCHAR2(6)            ,    /*   二次実技                            */
       SEC_PLACTICAL_TRUST_PT           CHAR(1)                ,    /*   二次実技－配点信頼                  */
       SEC_PLACTICAL_PT                 NUMBER(5,1)            ,    /*   二次実技－配点                      */
       SEC_PLACTICAL_HISSU              CHAR(1)                ,    /*   二次実技－必選                      */
       SEC_BOKI                         VARCHAR2(6)            ,    /*   二次簿記                            */
       SEC_BOKI_TRUST_PT                CHAR(1)                ,    /*   二次簿記－配点信頼                  */
       SEC_BOKI_PT                      NUMBER(5,1)            ,    /*   二次簿記－配点                      */
       SEC_BOKI_HISSU                   CHAR(1)                ,    /*   二次簿記－必選                      */
       SEC_INFO                         VARCHAR2(6)            ,    /*   二次情報                            */
       SEC_INFO_TRUST_PT                CHAR(1)                ,    /*   二次情報－配点信頼                  */
       SEC_INFO_PT                      NUMBER(5,1)            ,    /*   二次情報－配点                      */
       SEC_INFO_HISSU                   CHAR(1)                ,    /*   二次情報－必選                      */
       SEC_RESEARCH                     VARCHAR2(6)            ,    /*   二次調査書                          */
       SEC_RESEARCH_TRUST_PT            CHAR(1)                ,    /*   二次調査書－配点信頼                */
       SEC_RESEARCH_PT                  NUMBER(5,1)            ,    /*   二次調査書－配点                    */
       SEC_RESEARCH_HISSU               CHAR(1)                ,    /*   二次調査書－必選                    */
       SEC_HOKA                         VARCHAR2(6)            ,    /*   二次他                              */
       SEC_HOKA_TRUST_PT                CHAR(1)                ,    /*   二次他－配点信頼                    */
       SEC_HOKA_PT                      NUMBER(5,1)            ,    /*   二次他－配点                        */
       SEC_HOKA_HISSU                   CHAR(1)                ,    /*   二次他－必選                        */
       SEC_SONOTA_TRUST_PT              CHAR(1)                ,    /*   二次その他－配点信頼                */
       SEC_SONOTA_PT                    NUMBER(5,1)            ,    /*   二次その他－配点                    */
       SEC_SEL_PATERN                   VARCHAR2(200)          ,    /*   二次教科選択パターン                */
       SEC_SEL_PATERN_STATE             CHAR(1)                ,    /*   二次教科選択パターンテーブル有無    */
       SEC_ITEM_CD                      VARCHAR2(8)            ,    /*   二次科目型コード                    */
       SEC_SUB_CNT                      CHAR(3)                ,    /*   二次教科数                          */
       SEC_ITEM_CNT                     CHAR(3)                ,    /*   二次科目数                          */
       SEC_REMARK1                      CHAR(4)                ,    /*   二次備考１                          */
       SEC_REMARK2                      CHAR(4)                ,    /*   二次備考２                          */
       SEC_REMARK3                      CHAR(4)                ,    /*   二次備考３                          */
       SEC_REMARK4                      CHAR(4)                ,    /*   二次備考４                          */
       SEC_REMARK5                      CHAR(4)                ,    /*   二次備考５                          */
       SEC_REMARK6                      CHAR(4)                ,    /*   二次備考６                          */
       SEC_REMARK7                      CHAR(4)                ,    /*   二次備考７                          */
       SEC_REMARK8                      CHAR(4)                ,    /*   二次備考８                          */
       ITEM_CARE_COND                   CHAR(1)                ,    /*   科目メンテ状況                      */
       SEC_NOT_LINKAGE_FLG              CHAR(1)                ,    /*   二次模試非連携フラグ                */
       D_SEC_SUB_CNT_MIN                CHAR(1)                ,    /*   代表二次教科数－最小                */
       D_SEC_SUB_CNT_MAX                CHAR(1)                ,    /*   代表二次教科数－最大                */
       D_SEC_ITEM_CNT_MIN               CHAR(1)                ,    /*   代表二次科目数－最小                */
       D_SEC_ITEM_CNT_MAX               CHAR(1)                ,    /*   代表二次科目数－最大                */
       D_SEC_7ITEM_TYP_NAME             VARCHAR2(20)           ,    /*   代表二次７科目型名                  */
       SAME_PTN_ITEM                    CHAR(3)                ,    /*   同一パターン－科目                  */
       SAME_PTN_PT                      CHAR(3)                ,    /*   同一パターン－配点                  */
       SAME_PTN_REMARK                  CHAR(4)                ,    /*   同一パターン－備考                  */
       SEC_ITEM_NAME                    VARCHAR2(400)          ,    /*   二次科目名                          */
       CONSTRAINT PK_JH01_KAMOKU3 PRIMARY KEY (
                 COMBINATION_KEY1         ,               /*   記入用５桁コード    */
                 COMBINATION_KEY2         ,               /*   センタ枝番          */
                 COMBINATION_KEY3                         /*   二次枝番            */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

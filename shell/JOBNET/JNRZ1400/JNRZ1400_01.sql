-- ************************************************
-- *機 能 名 : 設問名称マスタ
-- *機 能 ID : JNRZ1400_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE QUESTIONNAME_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE QUESTIONNAME_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM QUESTIONNAME WHERE 0=1;

-- 主キーの設定
ALTER TABLE QUESTIONNAME_TMP ADD CONSTRAINT PK_QUESTIONNAME_TMP PRIMARY KEY 
(
   EVENTYEAR
 , EXAMCD
 , SUBCD
 , SHOQUESTION_NO
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

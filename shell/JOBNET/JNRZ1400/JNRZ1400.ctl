------------------------------------------------------------------------------
-- *機 能 名 : 設問名称マスタのCTLファイル
-- *機 能 ID : JNRZ1400.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE QUESTIONNAME_TMP
(  
   EVENTYEAR       POSITION(01:04)  CHAR 
 , EXAMCD          POSITION(05:06)  CHAR 
 , SUBCD           POSITION(07:10)  CHAR 
,  SHOQUESTION_NO  POSITION(11:12)  CHAR 
 , SHOQUESTIONNAME POSITION(13:32)  CHAR 
 , SHOQUESTION_PT  POSITION(33:35)  INTEGER EXTERNAL 
 , QUESTION_NO     POSITION(36:37)  CHAR 
)

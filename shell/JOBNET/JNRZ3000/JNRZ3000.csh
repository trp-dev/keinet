#! /bin/csh

###### 統計集データ（志望大学学部学科別成績分布除く）作成バッチ
###### 志望大学学部学科別成績分布を除くすべて統計集データ作成

set kdcdir = "/home/navikdc"
set filename = "storedata.csv"
set file = "${kdcdir}/${filename}"
set outfilename = "/keinavi/JOBNET/returnresult"
set JOBNETNAME = "Statistics"

echo "統計集データ作成処理開始..."

#----定義ファイルの有無確認
if (! -f ${file}) then
	echo "--- 登録対象模試コードの記入ファイルが存在しません ---"
	echo "--- 登録が必要な場合は定義ファイルを配置して再度行ってください ---"
	exit 1
endif
if ($1 == F002_1) then
	set dataout = `cat $file`
	set changout = `echo ${dataout} | tr -d '\r\n'`
	set loops = 1
	\rm -f $file
	while ($loops <= $#changout)
		echo $changout[$loops] >> $file
		@ loops ++
	end
	chmod 664 $file
endif

#---登録対象模試コード記入定義ファイルを検証機へ転送
echo "---- 対象ファイルのファイルサーバ転送開始..."
cd /keinavi/JOBNET/
/keinavi/HULFT/ftp.sh /keinavi/JOBNET $kdcdir ${filename} 10.1.4.177 orauser

#----リモートシェルを実行
/usr/bin/rsh satu04 -l root /keinavi/JOBNET/JobnetStart_ini.sh ${JOBNETNAME} $1

if ($status != 0) then
	echo "-1" > $outfilename
else
	/usr/bin/rsh satu04 cat /keinavi/JOBNET/result_${JOBNETNAME} > $outfilename
endif
/usr/bin/rsh satu04 \rm /keinavi/JOBNET/result_${JOBNETNAME}

if ($1 == F002_4) then
	rm -f ${file}
endif

#----コマンド実行結果の識別
set cord = `tail -1 $outfilename`
rm -f $outfilename
if (($cord > 0)&&($cord != 11)) then
	echo "----ERROR：統計集データ作成処理に失敗しました。 ----"
	echo "----ERROR：出力ログを確認し、原因を修正後再度実行してください。----"
	exit 30
else if ($cord < 0) then
        echo " "
        echo "----ERROR：処理異常により、処理に失敗しました。管理者に確認してください。----"
	exit $cord
else if ($cord == 11) then
	exit 1
endif
exit 0

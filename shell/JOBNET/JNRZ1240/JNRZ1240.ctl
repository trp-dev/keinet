------------------------------------------------------------------------------
-- *機 能 名 : 英語認定試験別・合計CEFR取得状況（全国）のCTLファイル
-- *機 能 ID : JNRZ1240.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE ENGPT_CEFRACQSTATUS_A_TMP
(  
   EXAMYEAR     POSITION(01:04)  CHAR 
 , EXAMCD       POSITION(05:06)  CHAR 
 , ENGPTCD      POSITION(07:08)  CHAR 
 , ENGPTLEVELCD POSITION(09:10)  CHAR 
 , CEFRLEVELCD  POSITION(11:12)  CHAR 
 , NUMBERS      POSITION(13:19)  INTEGER EXTERNAL 
 , COMPRATIO    POSITION(20:23)  INTEGER EXTERNAL "DECODE(:COMPRATIO,    '9999', null, :COMPRATIO/10)"
)

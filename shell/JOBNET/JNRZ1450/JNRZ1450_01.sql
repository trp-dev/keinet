-- ************************************************
-- *機 能 名 : CEFR換算用スコア情報
-- *機 能 ID : JNRZ1450_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE CEFRCVSSCOREINFO_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE CEFRCVSSCOREINFO_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM CEFRCVSSCOREINFO WHERE 0=1;

-- 主キーの設定
ALTER TABLE CEFRCVSSCOREINFO_TMP ADD CONSTRAINT PK_CEFRCVSSCOREINFO_TMP PRIMARY KEY 
(
   EVENTYEAR
 , EXAMDIV
 , EXAMCD
 , CEFRLEVELCD
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

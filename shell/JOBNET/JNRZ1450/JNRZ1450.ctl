------------------------------------------------------------------------------
-- *機 能 名 : CEFR換算用スコア情報のCTLファイル
-- *機 能 ID : JNRZ1450.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE CEFRCVSSCOREINFO_TMP
(  
   EVENTYEAR    POSITION(01:04)  CHAR 
 , EXAMDIV      POSITION(05:06)  CHAR 
 , EXAMCD       POSITION(07:08)  CHAR 
 , CEFRLEVELCD  POSITION(09:10)  CHAR 
 , CEFRCVSSCORE POSITION(11:15)  INTEGER EXTERNAL "DECODE(:CEFRCVSSCORE, '99999', null, :CEFRCVSSCORE/10)"
)

#!/bin/sh

#学校マスタ受信
cd /keinavi/HULFT


# 志望別成績分布データ（マーク模試）
/usr/local/HULFT/bin/utlrecv -f RZRG8A01 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A02 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A03 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A04 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A05 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A06 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A07 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A08 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A09 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A10 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A11 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B01 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B02 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B03 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B04 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B05 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B06 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B07 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B08 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B09 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B10 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B11 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8C00 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D01 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D02 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D03 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D04 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D05 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D06 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D07 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D08 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D09 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D10 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D11 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8E00 -h k03308f6gp -sync -w 300


#if [ $? -ne 0 ]
#then
#    echo 受信エラーが発生しました
#    exit 30
#fi

exit 0

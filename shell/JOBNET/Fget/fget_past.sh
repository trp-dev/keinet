#!/bin/sh

#学校マスタ受信
cd /keinavi/HULFT

# 集計データ・個表データ関連
/usr/local/HULFT/bin/utlrecv -f RZRG1100 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1110 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1120 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1130 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1140 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1150 -h k03308f6gp -sync -w 1800
/usr/local/HULFT/bin/utlrecv -f RZRG1170 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1180 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1190 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1210 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1220 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1230 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1250 -h k03308f6gp -sync -w 1800
/usr/local/HULFT/bin/utlrecv -f RZRG1280 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1300 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1310 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1320 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1330 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1340 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1350 -h k03308f6gp -sync -w 1800
/usr/local/HULFT/bin/utlrecv -f RZRG1360 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1380 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1390 -h k03308f6gp -sync -w 600
/usr/local/HULFT/bin/utlrecv -f RZRG1410 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1420 -h k03308f6gp -sync -w 600
/usr/local/HULFT/bin/utlrecv -f RZRG1430 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1450 -h k03308f6gp -sync -w 1800
/usr/local/HULFT/bin/utlrecv -f RZRG1480 -h k03308f6gp -sync -w 300

#if [ $? -ne 0 ]
#then
#    echo 受信エラーが発生しました
#    exit 30
#fi

exit 0

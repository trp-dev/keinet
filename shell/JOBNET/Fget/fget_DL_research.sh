#!/bin/sh

#学校マスタ受信
cd /keinavi/HULFT

# 志望別成績分布データ（センター・リサーチ用）
/usr/local/HULFT/bin/utlrecv -f RZRG8101 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8102 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8103 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8104 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8105 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8106 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8107 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8108 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8109 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8110 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8111 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8201 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8202 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8203 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8204 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8205 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8206 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8207 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8208 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8209 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8210 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8211 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8300 -h k03308f6gp -sync -w 300

#if [ $? -ne 0 ]
#then
#    echo 受信エラーが発生しました
#    exit 30
#fi

exit 0

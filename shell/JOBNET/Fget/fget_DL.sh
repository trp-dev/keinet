#!/bin/sh

#学校マスタ受信
cd /keinavi/HULFT


# 志望別成績分布データ（マーク模試）
/usr/local/HULFT/bin/utlrecv -f RZRG8A01 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A02 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A03 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A04 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A05 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A06 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A07 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A08 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A09 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A10 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8A11 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B01 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B02 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B03 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B04 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B05 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B06 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B07 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B08 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B09 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B10 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8B11 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8C00 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D01 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D02 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D03 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D04 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D05 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D06 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D07 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D08 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D09 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D10 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8D11 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8E00 -h k03308f6gp -sync -w 300


# 志望別成績分布データ（記述模試）
/usr/local/HULFT/bin/utlrecv -f RZRG8F01 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8F02 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8F03 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8F04 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8F05 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8F06 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8F07 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8F08 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8F09 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8F10 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8F11 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8G01 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8G02 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8G03 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8G04 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8G05 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8G06 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8G07 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8G08 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8G09 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8G10 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8G11 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG8H00 -h k03308f6gp -sync -w 300


# 統計集WEB関連（マーク系）
/usr/local/HULFT/bin/utlrecv -f RZRG9110 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9120 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9130 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9140 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9150 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9160 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9170 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG9180 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9190 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9210 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9220 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9230 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9240 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9250 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9310 -h k03308f6gp -sync -w 300

# 統計集WEB関連（記述系）
/usr/local/HULFT/bin/utlrecv -f RZRG9111 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9121 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9131 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9141 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9151 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9161 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG9171 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9181 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9191 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9211 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG9221 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG9231 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9241 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9251 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9311 -h k03308f6gp -sync -w 300

# 統計集WEB関連（高１・高１記述）
/usr/local/HULFT/bin/utlrecv -f RZRG9112 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9122 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9132 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9142 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG9152 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG9162 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG9172 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9182 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9192 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9212 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG9222 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG9232 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9242 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9252 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9312 -h k03308f6gp -sync -w 300

# 統計集WEB関連（高２・高２記述・マーク高２）
/usr/local/HULFT/bin/utlrecv -f RZRG9113 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9123 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9133 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9143 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG9153 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG9163 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG9173 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9183 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9193 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9213 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG9223 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG9233 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9243 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9253 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9313 -h k03308f6gp -sync -w 300

# 統計集WEB関連（プレステージ）
/usr/local/HULFT/bin/utlrecv -f RZRG9114 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9124 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9134 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9144 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9154 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9164 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9174 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9184 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9194 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9214 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9224 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9234 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9244 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9254 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG9314 -h k03308f6gp -sync -w 300


#if [ $? -ne 0 ]
#then
#    echo 受信エラーが発生しました
#    exit 30
#fi

exit 0

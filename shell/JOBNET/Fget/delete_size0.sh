#!/bin/sh

sleep 10

cd /keinavi/HULFT/NewEXAM
for filename in `ls [A-Z]*[0-9]`; do
	FILESIZE=`ls -l $filename | awk '{print $5}'`
	if [ $FILESIZE = 0 ]
	then
		echo "del $filename"
#		rm $filename
		mv $filename /tmp/HULFT_DEL
	fi
done


cd /keinavi/HULFT
for filename in `ls TYOUSA_*`; do
	FILESIZE=`ls -l $filename | awk '{print $5}'`
	if [ $FILESIZE = 0 ]
	then
		echo "del $filename"
#		rm $filename
		mv $filename /tmp/HULFT_DEL
	fi
done
for filename in `ls INDIVIDUALRECORD_FD*`; do
	FILESIZE=`ls -l $filename | awk '{print $5}'`
	if [ $FILESIZE = 0 ]
	then
		echo "del $filename"
#		rm $filename
		mv $filename /tmp/HULFT_DEL
	fi
done
for filename in `ls ACADEMICRECORD_I_FD*`; do
        FILESIZE=`ls -l $filename | awk '{print $5}'`
        if [ $FILESIZE = 0 ]
        then
                echo "del $filename"
#               rm $filename
                mv $filename /tmp/HULFT_DEL
        fi
done

cd /home/navikdc/PDF
for filename in `ls PDF_*`; do
        FILESIZE=`ls -l $filename | awk '{print $5}'`
        if [ $FILESIZE = 0 ]
        then
                echo "del $filename"
                rm $filename
        fi
done

for filename in `ls PDF_*`; do
        unzip -o $filename

        echo "del $filename"
        rm $filename
done

chmod 666 *.pdf

#!/bin/sh

#学校マスタ受信
cd /keinavi/HULFT

# 集計データ・個表データ関連
/usr/local/HULFT/bin/utlrecv -f RZRG1100 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1110 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1120 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1130 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1140 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG1150 -h k03308f6gp -sync -w 1800
/usr/local/HULFT/bin/utlrecv -f RZRG1170 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1180 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1190 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1210 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1220 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1230 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG1250 -h k03308f6gp -sync -w 1800
/usr/local/HULFT/bin/utlrecv -f RZRG1280 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1300 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1310 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1320 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1330 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1340 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG1350 -h k03308f6gp -sync -w 1800
/usr/local/HULFT/bin/utlrecv -f RZRG1360 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1380 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1390 -h k03308f6gp -sync -w 600
/usr/local/HULFT/bin/utlrecv -f RZRG1410 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG1420 -h k03308f6gp -sync -w 600
/usr/local/HULFT/bin/utlrecv -f RZRG1430 -h k03308f6gp -sync -w 300
#/usr/local/HULFT/bin/utlrecv -f RZRG1450 -h k03308f6gp -sync -w 1800
/usr/local/HULFT/bin/utlrecv -f RZRG1480 -h k03308f6gp -sync -w 300

# 高校別データダウンロード関連
/usr/local/HULFT/bin/utlrecv -f RZRG2110 -h k03308f6gp -sync -w 1800
/usr/local/HULFT/bin/utlrecv -f RZRG2210 -h k03308f6gp -sync -w 1800
/usr/local/HULFT/bin/utlrecv -f RZRG2220 -h k03308f6gp -sync -w 1800
/usr/local/HULFT/bin/utlrecv -f RZRG2310 -h k03308f6gp -sync -w 1800
/usr/local/HULFT/bin/utlrecv -f RZRG2410 -h k03308f6gp -sync -w 1800

# マスタ関連１（大学マスタ除く）
/usr/local/HULFT/bin/utlrecv -f RZRG3210 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG3310 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG3410 -h k03308f6gp -sync -w 300

# マスタ関連２
/usr/local/HULFT/bin/utlrecv -f RZRG4110 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG4210 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG4310 -h k03308f6gp -sync -w 300

# 大学マスタ関連
/usr/local/HULFT/bin/utlrecv -f RZRG5100 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5110 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5120 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5130 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5140 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5150 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5160 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5170 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5180 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5190 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5210 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5220 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5230 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5240 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5250 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5260 -h k03308f6gp -sync -w 300
/usr/local/HULFT/bin/utlrecv -f RZRG5270 -h k03308f6gp -sync -w 300



#if [ $? -ne 0 ]
#then
#    echo 受信エラーが発生しました
#    exit 30
#fi

exit 0

SET SERVEROUTPUT ON

BEGIN

    dbms_output.enable(10000);
    dbms_output.put_line('換算得点変換処理開始');

    FOR r_cur IN (SELECT DISTINCT SUBCD
                  FROM   CENTERCVSSCORE
                  WHERE  EXAMYEAR='04' AND EXAMCD='03'
                  ORDER BY SUBCD)
    LOOP

        dbms_output.put_line('科目[' || r_cur.SUBCD || ']開始');

        UPDATE
            subrecord_i ri
        SET
            ri.cvsscore = (
                SELECT
                    c.cvsscore
                FROM
                    centercvsscore c
                WHERE
                    c.examyear = '04'
                AND
                    c.examcd = '03'
                AND
                    c.subcd = ri.subcd
                AND
                    c.rawscore = ri.score
            )
                
        WHERE
            ri.examyear = '2004'
        AND
            ri.examcd = '03'
        AND
            ri.subcd = r_cur.SUBCD;

        COMMIT;
        dbms_output.put_line('科目[' || r_cur.SUBCD || ']終了');

    END LOOP;

    dbms_output.put_line('換算得点変換処理終了');

END;
/

-- *********************************************************
-- *機 能 名 : センター到達指標
-- *機 能 ID : JNRZ0650_02.sql
-- *作 成 日 : 2006/09/05
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM CENTER_REACHMARK
WHERE  (EXAMYEAR,EXAMCD)
       IN(SELECT DISTINCT EXAMYEAR,EXAMCD
          FROM  CENTER_REACHMARK_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO CENTER_REACHMARK
SELECT * FROM CENTER_REACHMARK_TMP;

-- 8年以上前のデータを削除
-- DELETE FROM CENTER_REACHMARK
-- WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 8);

-- 更新対象データの7年前のデータを削除
-- DELETE FROM CENTER_REACHMARK
-- WHERE  (EXAMYEAR,EXAMCD)
--        IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 7)),EXAMCD
--          FROM   CENTER_REACHMARK_TMP);

-- テンポラリテーブル削除
DROP TABLE CENTER_REACHMARK_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

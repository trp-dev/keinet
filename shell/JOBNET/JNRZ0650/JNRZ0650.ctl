------------------------------------------------------------------------------
--  システム名      : KEI-NAVI（受験学力測定テスト対応）
--  概念ファイル名  : センター到達指標
--  テーブル名      : CENTER_REACHMARK
--  データファイル名: CENTER_REACHMARK
--  機能            : センター到達指標をデータファイルの内容に置き換える
--  作成日          : 2006/08/25
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE CENTER_REACHMARK_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   EXAMYEAR                POSITION(01:04)      CHAR,
   EXAMCD                  POSITION(05:06)      CHAR,
   SUBCD                   POSITION(07:10)      CHAR,
   DEVZONECD               POSITION(11:12)      CHAR,
   RANK                    POSITION(13:16)      INTEGER EXTERNAL "DECODE(:RANK, '9999', null, :RANK/10)", 
   AVGPNT                  POSITION(17:21)      INTEGER EXTERNAL "DECODE(:AVGPNT,         '99999', null, :AVGPNT/10)"
)

#! /bin/sh

#### JOBNET起動のための汎用起動バッチ
#### 

source /home/orauser/.bash_profile

export fpath=/keinavi/JOBNET
export jobname=$1
export outfile=/keinavi/JOBNET/result_${jobname}

if [ $# -ge 2 ]
then
	if [ $2 = 0 ]
	then
		${fpath}/${jobname}.sh
		OUTCD=$?
	else
		if [ $# -eq 2 ]
		then
			${fpath}/${jobname}.csh $2
			OUTCD=$?
		else
			${fpath}/${jobname}.csh $2 $3
			OUTCD=$?
		fi
	fi
		
	if [ $OUTCD -ne 0 ]
	then
		echo $OUTCD > $outfile
		exit $OUTCD
	fi
	echo 0 > $outfile
	exit 0
fi

#-- マスタメンテナンスJOBNETなどの起動
${fpath}/${jobname}/${jobname}.sh
RESU=$?
if [ $RESU -ne 0 ]
then
	echo $RESU > $outfile
	exit $RESU
fi
echo 0 > $outfile
exit 0

------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 大学情報マスタ
--  テーブル名      : UNIVMASTER_INFO
--  データファイル名: UNIVMASTER_INFO
--  機能            : 大学情報マスタをデータファイルの内容に置き換える
--  作成日          : 2014/08/07
--  修正日          :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE UNIVMASTER_INFO
WHEN EVENTYEAR <> X'944E9378' -- 年度の値が「年度」ならタイトル行なのでスキップする
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   EVENTYEAR,
   UNIDIV,
   UNIVCD,
   PREFCD_EXAMHO,
   UNINAME_CODE,
   UNIVATTR,
   UNIVNAME_REP,
   UNIVNAME_ABBR,
   UNIVNAME_KANA,
   UNIVNAME_ADDR,
   UNIV_REORG_DIV,
   ESTAB_YEAR,
   EDUINFO_CHARGE_CD,
   SECTORCD,
   SECTOR_CHARGE_CD,
   EXAMDIV_FIRST,
   EXAMDIV_LAST,
   CREATEDATE,
   CREATE_CODE,
   LASTUPDATE,
   UPDATE_CODE,
   KANA_NUM
)

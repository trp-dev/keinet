-- ************************************************
-- *機 能 名 : 科目別成績（高校)
-- *機 能 ID : JNRZ0070_01.sql
-- *作 成 日 : 2004/08/30
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE SUBRECORD_S_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE SUBRECORD_S_TMP  (
       EXAMYEAR            CHAR(4)              NOT NULL,   /*    模試年度         */ 
       EXAMCD              CHAR(2)              NOT NULL,   /*    模試コード       */ 
       BUNDLECD            CHAR(5)              NOT NULL,   /*    一括コード       */ 
       SUBCD               CHAR(4)              NOT NULL,   /*    科目コード       */ 
       AVGPNT              NUMBER(5,1)                  ,   /*    平均点           */ 
       AVGDEVIATION        NUMBER(4,1)                  ,   /*    平均偏差値       */ 
       AVGSCORERATE        NUMBER(4,1)                  ,   /*    平均得点率       */ 
       NUMBERS             NUMBER(8,0)                  ,   /*    人数             */ 
       CONSTRAINT PK_SUBRECORD_S_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  模試年度    */
                 EXAMCD                    ,               /*  模試コード  */
                 BUNDLECD                  ,               /*  一括コード  */
                 SUBCD                                     /*  科目コード  */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

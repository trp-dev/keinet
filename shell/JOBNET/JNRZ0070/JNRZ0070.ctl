------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 科目別成績（高校）
--  テーブル名      : SUBRECORD_S
--  データファイル名: SUBRECORD_S
--  機能            : 科目別成績（高校）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE SUBRECORD_S_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  EXAMYEAR          POSITION(01:04)   CHAR,
   EXAMCD            POSITION(05:06)   CHAR,
   BUNDLECD          POSITION(07:11)   CHAR,
   SUBCD             POSITION(12:15)   CHAR,
   AVGPNT            POSITION(16:20)   INTEGER EXTERNAL "DECODE(:AVGPNT,         '99999', null, :AVGPNT/10)",
   AVGDEVIATION      POSITION(21:24)   INTEGER EXTERNAL "DECODE(:AVGDEVIATION,    '9999', null, :AVGDEVIATION/10)",
   AVGSCORERATE      POSITION(25:28)   INTEGER EXTERNAL "DECODE(:AVGSCORERATE,    '9999', null, :AVGSCORERATE/10)",
   NUMBERS           POSITION(29:36)   INTEGER EXTERNAL "NULLIF(:NUMBERS,     '99999999')"
)

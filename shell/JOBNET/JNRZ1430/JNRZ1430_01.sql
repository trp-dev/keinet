-- ************************************************
-- *機 能 名 : 記述式総合評価マスタ
-- *機 能 ID : JNRZ1430_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE DESCCOMPRATING_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE DESCCOMPRATING_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM DESCCOMPRATING WHERE 0=1;

-- 主キーの設定
ALTER TABLE DESCCOMPRATING_TMP ADD CONSTRAINT PK_DESCCOMPRATING_TMP PRIMARY KEY 
(
   EVENTYEAR
 , EXAMCD
 , SUBCD
 , SHOQUESTION1
 , SHOQUESTION2
 , SHOQUESTION3
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

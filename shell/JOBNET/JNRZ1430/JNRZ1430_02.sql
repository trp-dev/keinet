-- *********************************************************
-- *機 能 名 : 記述式総合評価マスタ
-- *機 能 ID : JNRZ1430_02.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM DESCCOMPRATING MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM DESCCOMPRATING_TMP TMP
         WHERE 1=1
           AND MAIN.EVENTYEAR     = TMP.EVENTYEAR
           AND MAIN.EXAMCD        = TMP.EXAMCD
           AND MAIN.SUBCD         = TMP.SUBCD
           AND MAIN.SHOQUESTION1  = TMP.SHOQUESTION1
           AND MAIN.SHOQUESTION2  = TMP.SHOQUESTION2
           AND MAIN.SHOQUESTION3  = TMP.SHOQUESTION3
       )
;

-- テンポラリから本物へ登録
INSERT INTO DESCCOMPRATING SELECT * FROM DESCCOMPRATING_TMP;

-- テンポラリテーブル削除
DROP TABLE DESCCOMPRATING_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

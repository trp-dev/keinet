------------------------------------------------------------------------------
-- *機 能 名 : 記述式総合評価マスタのCTLファイル
-- *機 能 ID : JNRZ1430.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE DESCCOMPRATING_TMP
(  
   EVENTYEAR      POSITION(01:04)  CHAR 
 , EXAMCD         POSITION(05:06)  CHAR 
 , SUBCD          POSITION(07:10)  CHAR 
 , SHOQUESTION1   POSITION(11:12)  CHAR 
 , SHOQUESTION2   POSITION(13:14)  CHAR 
 , SHOQUESTION3   POSITION(15:16)  CHAR 
 , DESCCOMPRATING POSITION(17:17)  CHAR 
)

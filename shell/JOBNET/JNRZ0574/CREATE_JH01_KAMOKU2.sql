-- ************************************************
-- *機 能 名 : 情報誌用科目（センター）
-- *機 能 ID : CREATE_JH01_KAMOKU2.sql
-- *作 成 日 : 2015/09/28
-- *作 成 者 : 富士通システムズ・ウエスト
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- 情報誌用科目（センター）テーブル削除
DROP TABLE JH01_KAMOKU2 CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 情報誌用科目（センター）テーブル作成
CREATE TABLE JH01_KAMOKU2  (
       COMBINATION_KEY1              CHAR(5)        NOT NULL,    /*   記入用５桁コード                      */
       COMBINATION_KEY2              CHAR(2)        NOT NULL,    /*   センタ枝番                            */
       COMBINATION_KEY3              CHAR(2)        NOT NULL,    /*   二次枝番                              */
       CEN_SC_TRUST                  CHAR(1)                ,    /*   センタ配点信頼                        */
       CEN_TOTAL_SC                  NUMBER(5,1)            ,    /*   センタ総点                            */
       CEN_ENGLISH                   VARCHAR2(6)            ,    /*   センタ英語                            */
       CEN_LISTEN                    VARCHAR2(6)            ,    /*   センタリスニング                      */
       CEN_GERMAN                    VARCHAR2(6)            ,    /*   センタ独語                            */
       CEN_FRENCH                    VARCHAR2(6)            ,    /*   センタ仏語                            */
       CEN_CHINESE                   VARCHAR2(6)            ,    /*   センタ中語                            */
       CEN_KOREAN                    VARCHAR2(6)            ,    /*   センタ韓語                            */
       CEN_GAI_CNT                   NUMBER(1,0)            ,    /*   センタ外国語－科目数                  */
       CEN_GAI_HOKA_SEL              CHAR(1)                ,    /*   センタ外国語－その他科目選択区分      */
       CEN_GAI_TRUST_PT              CHAR(1)                ,    /*   センタ外国語－配点信頼                */
       CEN_GAI_SHO_PT                NUMBER(5,1)            ,    /*   センタ外国語－配点小                  */
       CEN_GAI_SHO_SEL               CHAR(1)                ,    /*   センタ外国語－必選小                  */
       CEN_GAI_DAI_PT                NUMBER(5,1)            ,    /*   センタ外国語－配点大                  */
       CEN_GAI_DAI_SEL               CHAR(1)                ,    /*   センタ外国語－必選大                  */
       CEN_GAI_PT_STATE              CHAR(1)                ,    /*   センタ外国語－配点テーブル有無        */
       CEN_MATH1                     VARCHAR2(6)            ,    /*   センタ数学１                          */
       CEN_MATH_A                    VARCHAR2(6)            ,    /*   センタ数学Ａ                          */
       CEN_MATH2                     VARCHAR2(6)            ,    /*   センタ数学２                          */
       CEN_MATH_B                    VARCHAR2(6)            ,    /*   センタ数学Ｂ                          */
       CEN_KOGYO                     VARCHAR2(6)            ,    /*   センタ工業                            */
       CEN_BOKI                      VARCHAR2(6)            ,    /*   センタ簿記                            */
       CEN_INFO                      VARCHAR2(6)            ,    /*   センタ情報                            */
       CEN_LIM_MATH1                 CHAR(1)                ,    /*   センタ受験制限－数学１                */
       CEN_LIM_MATH2                 CHAR(1)                ,    /*   センタ受験制限－数学２                */
       CEN_LIM_KOGYO                 CHAR(1)                ,    /*   センタ受験制限－工業                  */
       CEN_LIM_BOKI                  CHAR(1)                ,    /*   センタ受験制限－簿記                  */
       CEN_LIM_INFO                  CHAR(1)                ,    /*   センタ受験制限－情報                  */
       CEN_MATH_CNT                  NUMBER(1,0)            ,    /*   センタ数学－科目数                    */
       CEN_MATH_HOKA_SEL             CHAR(1)                ,    /*   センタ数学－その他科目選択区分        */
       CEN_MATH_TRUST_PT             CHAR(1)                ,    /*   センタ数学－配点信頼                  */
       CEN_MATH_SHO_PT               NUMBER(5,1)            ,    /*   センタ数学－配点小                    */
       CEN_MATH_SHO_SEL              CHAR(1)                ,    /*   センタ数学－必選小                    */
       CEN_MATH_DAI_PT               NUMBER(5,1)            ,    /*   センタ数学－配点大                    */
       CEN_MATH_DAI_SEL              CHAR(1)                ,    /*   センタ数学－必選大                    */
       CEN_MATH_PT_STATE             CHAR(1)                ,    /*   センタ数学－配点テーブル有無          */
       CEN_KOKUGO                    VARCHAR2(6)            ,    /*   センタ国語                            */
       CEN_KOKUGO_RANGE              CHAR(1)                ,    /*   センタ現古漢範囲                      */
       CEN_KOKUGO_CNT                NUMBER(1,0)            ,    /*   センタ国語－科目数                    */
       CEN_KOKUGO_TRUST_PT           CHAR(1)                ,    /*   センタ国語－配点信頼                  */
       CEN_KOKUGO_SHO_PT             NUMBER(5,1)            ,    /*   センタ国語－配点小                    */
       CEN_KOKUGO_SHO_SEL            CHAR(1)                ,    /*   センタ国語－必選小                    */
       CEN_KOKUGO_DAI_PT             NUMBER(5,1)            ,    /*   センタ国語－配点大                    */
       CEN_KOKUGO_DAI_SEL            CHAR(1)                ,    /*   センタ国語－必選大                    */
       CEN_KOKUGO_PT_STATE           CHAR(1)                ,    /*   センタ国語－配点テーブル有無          */
       CEN_BUTU_BASE                 VARCHAR2(6)            ,    /*   センタ物理基礎                        */
       CEN_CHEMICAL_BASE             VARCHAR2(6)            ,    /*   センタ化学基礎                        */
       CEN_BIOLOGY_BASE              VARCHAR2(6)            ,    /*   センタ生物基礎                        */
       CEN_EARTH_SCIENCE_BASE        VARCHAR2(6)            ,    /*   センタ地学基礎                        */
       CEN_BUTU                      VARCHAR2(6)            ,    /*   センタ物理                            */
       CEN_KAGA                      VARCHAR2(6)            ,    /*   センタ化学                            */
       CEN_SEIBU                     VARCHAR2(6)            ,    /*   センタ生物                            */
       CEN_EARTH_RIKA                VARCHAR2(6)            ,    /*   センタ地学                            */
       CEN_LIM_BUTU_BASE             CHAR(1)                ,    /*   センタ受験制限－物理基礎              */
       CEN_LIM_CHEMICAL_BASE         CHAR(1)                ,    /*   センタ受験制限－化学基礎              */
       CEN_LIM_BIOLOGY_BASE          CHAR(1)                ,    /*   センタ受験制限－生物基礎              */
       CEN_LIM_EARTH_SCIENCE_BASE    CHAR(1)                ,    /*   センタ受験制限－地学基礎              */
       CEN_LIM_SEIBU                 CHAR(1)                ,    /*   センタ受験制限－生物                  */
       CEN_LIM_CHIGAKU               CHAR(1)                ,    /*   センタ受験制限－地学                  */
       CEN_RIKA_SHO_ITEM_CNT         NUMBER(1,0)            ,    /*   センタ理科－科目数小                  */
       CEN_RIKA_DAI_ITEM_CNT         NUMBER(1,0)            ,    /*   センタ理科－科目数大                  */
       CEN_RIKA_TRUST_PT             CHAR(1)                ,    /*   センタ理科－配点信頼                  */
       CEN_RIKA_SHO_PT               NUMBER(5,1)            ,    /*   センタ理科－配点小                    */
       CEN_RIKA_SHO_SEL              CHAR(1)                ,    /*   センタ理科－必選小                    */
       CEN_RIKA_DAI_PT               NUMBER(5,1)            ,    /*   センタ理科－配点大                    */
       CEN_RIKA_DAI_SEL              CHAR(1)                ,    /*   センタ理科－必選大                    */
       CEN_RIKA_PT_STATE             CHAR(1)                ,    /*   センタ理科－配点テーブル有無          */
       RIKA_1ANS_FLG                 CHAR(1)                ,    /*   理科第１解答科目フラグ                */
       RIKA_1ANS_METHOD              CHAR(1)                ,    /*   理科第１解答方式                      */
       RIKA_SAME_ITEM_SEL_FUKA       CHAR(1)                ,    /*   理科同一科目選択不可                  */
       CEN_SEKAISHI_A                VARCHAR2(6)            ,    /*   センタ世界Ａ                          */
       CEN_NIHONSHI_A                VARCHAR2(6)            ,    /*   センタ日本Ａ                          */
       CEN_GEOGRAPHY_A               VARCHAR2(6)            ,    /*   センタ地理Ａ                          */
       CEN_SEKAISHI_B                VARCHAR2(6)            ,    /*   センタ世界Ｂ                          */
       CEN_NIHONSHI_B                VARCHAR2(6)            ,    /*   センタ日本Ｂ                          */
       CEN_GEOGRAPHY_B               VARCHAR2(6)            ,    /*   センタ地理Ｂ                          */
       CEN_KINDAISHI                 VARCHAR2(6)            ,    /*   センタ現社                            */
       CEN_ETHICS                    VARCHAR2(6)            ,    /*   センタ倫理                            */
       CEN_POLITICS                  VARCHAR2(6)            ,    /*   センタ政経                            */
       CEN_POLITICS_AND_ETHICS       VARCHAR2(6)            ,    /*   センタ倫政                            */
       CEN_LIM_SEKAISHI_A            CHAR(1)                ,    /*   センタ受験制限－世界Ａ                */
       CEN_LIM_NIHONSHI_A            CHAR(1)                ,    /*   センタ受験制限－日本Ａ                */
       CEN_LIM_GEOGRAPHY_A           CHAR(1)                ,    /*   センタ受験制限－地理Ａ                */
       CEN_LIM_KINDAISHI             CHAR(1)                ,    /*   センタ受験制限－現社                  */
       CEN_SOCIETY_CNT               NUMBER(1,0)            ,    /*   センタ地公－科目数                    */
       CEN_SOCIETY_TRUST_PT          CHAR(1)                ,    /*   センタ地公－配点信頼                  */
       CEN_SOCIETY_SHO_PT            NUMBER(5,1)            ,    /*   センタ地公－配点小                    */
       CEN_SOCIETY_SHO_SEL           CHAR(1)                ,    /*   センタ地公－必選小                    */
       CEN_SOCIETY_DAI_PT            NUMBER(5,1)            ,    /*   センタ地公－配点大                    */
       CEN_SOCIETY_DAI_SEL           CHAR(1)                ,    /*   センタ地公－必選大                    */
       CEN_SOCIETY_PT_STATE          CHAR(1)                ,    /*   センタ地公－配点テーブル有無          */
       SOCIETY_1ANS_FLG              CHAR(1)                ,    /*   地公第１解答科目フラグ                */
       SOCIETY_1ANS_METHOD           CHAR(1)                ,    /*   地公第１解答方式                      */
       CEN_SEL_PATERN                VARCHAR2(200)          ,    /*   センタ教科選択パターン                */
       CEN_SEL_PATERN_STATE          CHAR(1)                ,    /*   センタ教科選択パターンテーブル有無    */
       CEN_REMARK1                   CHAR(4)                ,    /*   センタ備考１                          */
       CEN_REMARK2                   CHAR(4)                ,    /*   センタ備考２                          */
       CEN_REMARK3                   CHAR(4)                ,    /*   センタ備考３                          */
       CEN_REMARK4                   CHAR(4)                ,    /*   センタ備考４                          */
       CEN_REMARK5                   CHAR(4)                ,    /*   センタ備考５                          */
       CEN_REMARK6                   CHAR(4)                ,    /*   センタ備考６                          */
       CEN_REMARK7                   CHAR(4)                ,    /*   センタ備考７                          */
       CEN_REMARK8                   CHAR(4)                ,    /*   センタ備考８                          */
       CEN_ITEM_CD                   CHAR(4)                ,    /*   センタ科目型コード                    */
       CEN_SUB_CNT                   VARCHAR2(6)            ,    /*   センタ教科数                          */
       CEN_ITEM_CNT                  VARCHAR2(6)            ,    /*   センタ科目数                          */
       CEN_ITEM7                     VARCHAR2(20)           ,    /*   センタ７科目型名                      */
       CUT_RATE                      NUMBER(4,2)            ,    /*   足切倍率                              */
       CUT_PT                        NUMBER(5,1)            ,    /*   足切得点                              */
       CUT_PERFECT_PT                NUMBER(5,1)            ,    /*   足切満点                              */
       CEN_NOT_LINKAGE_FLG           CHAR(1)                ,    /*   センタ模試非連携フラグ                */
       D_CEN_SUB_CNT_MIN             CHAR(1)                ,    /*   代表センタ教科数－最小                */
       D_CEN_SUB_CNT_MAX             CHAR(1)                ,    /*   代表センタ教科数－最大                */
       D_CEN_ITEM_CNT_MIN            CHAR(1)                ,    /*   代表センタ科目数－最小                */
       D_CEN_ITEM_CNT_MAX            CHAR(1)                ,    /*   代表センタ科目数－最大                */
       D_CEN_7ITEM_TYP_NAME          VARCHAR2(20)           ,    /*   代表センタ７科目型名                  */
       SAME_PTN_ITEM                 CHAR(3)                ,    /*   同一パターン－科目                    */
       SAME_PTN_PT                   CHAR(3)                ,    /*   同一パターン－配点                    */
       SAME_PTN_REMARK               VARCHAR2(8)            ,    /*   同一パターン－備考                    */
       CEN_ITEM_NAME                 VARCHAR2(400)          ,    /*   センタ科目名                          */
       CONSTRAINT PK_JH01_KAMOKU2 PRIMARY KEY (
                 COMBINATION_KEY1         ,               /*   記入用５桁コード    */
                 COMBINATION_KEY2         ,               /*   センタ枝番          */
                 COMBINATION_KEY3                         /*   二次枝番            */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

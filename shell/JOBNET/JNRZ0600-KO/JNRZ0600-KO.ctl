------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 大学コード変換順引きマスタ
--  テーブル名      : KO_UNIVCDTRANS_ORD
--  データファイル名: KO_UNIVCDTRANS_ORD
--  機能            : 大学コード変換順引きマスタをデータファイルの内容に置き換える
--  作成日          : 2007/02/22
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE KO_UNIVCDTRANS_ORD_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   YEAR           POSITION(01:04)      CHAR,
   OLDUNIV8CD     POSITION(05:12)      CHAR,
   EXAMCD         POSITION(13:14)      CHAR,
   NEWUNIV8CD     POSITION(15:22)      CHAR
)

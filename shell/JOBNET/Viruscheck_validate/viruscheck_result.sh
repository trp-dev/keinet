#!/bin/sh

#環境変数設定
export DB_CONNECT_TARGET=S11DBA_batch2
export JOBNAME=Viruscheck_validate
export WORKDIR=/keinavi/JOBNET/$JOBNAME
export SQLPLUS_CMD=/opt/oracle/product/11.2/bin/sqlplus
export DOWNLOAD_DIR=/HULFT/download_validate/download
export VIRUS_OK_DIR=/HULFT/download_validate/viruscheck_ok
export VIRUS_NG_DIR=/HULFT/download_validate/viruscheck_ng
export ORACLE_HOME=/opt/oracle/product/11.2/
export NLS_LANG=Japanese_Japan.AL32UTF8

# 更新用SQL作成
FCOUNT=`${WORKDIR}/viruscheck_result.pl`
echo ${FCOUNT}

# 更新用SQLファイルの有無確認
FCOUNT=`find ${WORKDIR}/ -maxdepth 1 -name "update.sql"  -print | wc -l`
if [ "${FCOUNT}" -le 0 ]
then
    #echo nofile
    exit 0
fi

# 更新用SQLの実行
${SQLPLUS_CMD} -S keinavi_stg/39ra8ma@${DB_CONNECT_TARGET} @${WORKDIR}/update.sql

# 戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo データ処理でエラーが発生しました
    exit 30
fi

# ウィルス検知ファイルに対するメール送信情報取得
#${SQLPLUS_CMD} -S keinavi/39ra8ma@${DB_CONNECT_TARGET} @${WORKDIR}/select.sql

# ウィルス検知ファイルに対するメール送信
#FCOUNT=`${WORKDIR}/mail/bin/MailSend.pl`
#echo ${FCOUNT}

# ダウンロード可能ディレクトリへのファイルの移動
if [ `ls ${VIRUS_OK_DIR} | wc -l` -gt 0 ]
then
        cp -rpf ${VIRUS_OK_DIR}/* ${DOWNLOAD_DIR}/
        rm -rf ${VIRUS_OK_DIR}/*
fi

# ウィルス検知ファイルの削除
rm -rf ${VIRUS_NG_DIR}/*

# 更新用SQL削除
rm -f ${WORKDIR}/update.sql
rm -f ${WORKDIR}/select.sql
rm -f ${WORKDIR}/select_ret.txt

echo データ処理が正常に終了しました
exit  0    #正常終了


#!/usr/bin/perl
use strict;
use warnings;

# 一次変数
my $counter;
my $counter2;
my $i;

# 更新対象ファイルの読み込み
my $READ_OKDIR  = "/HULFT/download_validate/viruscheck_ok/";
my $READ_OKFILE = "/keinavi/JOBNET/Viruscheck_validate/OKfilelist.txt";
my $READ_NGDIR  = "/HULFT/download_validate/viruscheck_ng/";
my $READ_NGFILE = "/keinavi/JOBNET/Viruscheck_validate/NGfilelist.txt";
my @OK_FILELIST;
my @NG_FILELIST;

my $SQL_FILE = "/keinavi/JOBNET/Viruscheck_validate/update.sql"; # 更新用SQL
my $SQL_FILE2= "/keinavi/JOBNET/Viruscheck_validate/select.sql"; # メール送信情報取得用SQL
my $SQL_FILE2_RESULT= "/keinavi/JOBNET/Viruscheck_validate/select_ret.txt";

`/usr/bin/find $READ_OKDIR > $READ_OKFILE`;
`/usr/bin/find $READ_NGDIR > $READ_NGFILE`;

if(! open(IN, "$READ_OKFILE")){
        print "FILE OPEN ERROR!\n";
        exit;
}
if(! open(IN2, "$READ_NGFILE")){
        print "FILE OPEN ERROR!\n";
        close(IN);
        exit;
}
if(! open(OUT, ">>$SQL_FILE")){
        print "FILE OPEN ERROR!\n";
        close(IN);
        close(IN2);
        exit;
}
if(! open(OUT2, ">>$SQL_FILE2")){
        print "FILE OPEN ERROR!\n";
        close(IN);
        close(IN2);
        close(OUT);
        exit;
}

$counter = 0;
while(<IN>){
        chomp($_);
        if($_ =~ /\/$/){ next; }
        elsif($_ =~ /$READ_OKDIR[0-9]*$/){ next; }

#print "$_\n";
        $OK_FILELIST[$counter] = $_;
        $counter++;
}
close(IN);

for($i = 0; $i < @OK_FILELIST; $i++){
        if($OK_FILELIST[$i] ne ''){
                my @filename = split(/\//, $OK_FILELIST[$i]);
                print OUT "update UPLOADFILE set STATUS='1' where SERVERFILENAME='$filename[$#filename]';\n";
        }
}
print OUT "\ncommit;\n";
close(IN);


$counter2 = 0;
while(<IN2>){
        chomp($_);
        if($_ =~ /\/$/){ next; }
        elsif($_ =~ /$READ_NGDIR[0-9]*$/){ next; }

#print "$_\n";
        $NG_FILELIST[$counter2] = $_;
        $counter2++;
}


print OUT2 "set linesize 10000\n";
print OUT2 "set pagesize 0\n";
print OUT2 "set trimspool on\n";
print OUT2 "set colsep ','\n\n";
print OUT2 "spool $SQL_FILE2_RESULT\n";

for($i = 0; $i < @NG_FILELIST; $i++){
        if($NG_FILELIST[$i] ne ''){
                my @filename = split(/\//, $NG_FILELIST[$i]);
                print OUT "update UPLOADFILE set STATUS='9' where SERVERFILENAME='$filename[$#filename]';\n";
                print OUT2 "SELECT u.SCHOOLCD col1, s.BUNDLENAME col2, DECODE(p.PACTDIV, '1', '契約校', '非契約校') col3, uk.FILETYPE_NAME col5, u.UPLOADFILENAME col4 FROM UPLOADFILE u, SCHOOL s, PACTSCHOOL p, UPLOADFILEKIND uk WHERE u.SERVERFILENAME='$filename[$#filename]' AND u.SCHOOLCD=s.BUNDLECD AND u.SCHOOLCD=p.SCHOOLCD AND p.USERID like 'ke%' AND u.FILETYPE_ID=uk.FILETYPE_ID;\n";
        }
}
print OUT "\ncommit;\n";
print OUT "\nEXIT\n";

print OUT2 "spool off\n";
print OUT2 "\nEXIT\n";

close(IN2);
close(OUT);
close(OUT2);

# 更新対象ファイルの削除
unlink($READ_OKFILE);
unlink($READ_NGFILE);

`/bin/chmod 777 $SQL_FILE`;
`/bin/chmod 777 $SQL_FILE2`;

exit;


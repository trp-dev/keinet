------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 大学マスタ模試用科目
--  テーブル名      : UNIVMASTER_SUBJECT
--  データファイル名: UNIVMASTER_SUBJECT
--  機能            : 大学マスタ模試用科目をデータファイルの内容に置き換える
--  作成日          : 2009/12/17
--  修正日          : 
--  備考            : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE UNIVMASTER_SUBJECT_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    EVENTYEAR                 POSITION(01:04)        CHAR,
    SCHOOLDIV                 POSITION(05:05)        CHAR,
    UNIVCD                    POSITION(06:09)        CHAR,
    FACULTYCD                 POSITION(10:11)        CHAR,
    DEPTCD                    POSITION(12:15)        CHAR,
    EXAMDIV                   POSITION(16:17)        CHAR,
    ADMISSIONDIV              POSITION(18:18)        CHAR,
    NEXTYEARDIV               POSITION(19:19)        CHAR,
    ENTEXAMDIV                POSITION(20:20)        CHAR,
    UNICDBRANCHCD             POSITION(21:23)        CHAR,
    EXAMSUBCD                 POSITION(24:27)        CHAR,
    COURSECD                  POSITION(28:28)        CHAR,
    SUBBIT                    POSITION(29:30)        CHAR,
    SUBPNT                    POSITION(31:35)        INTEGER EXTERNAL "DECODE(:SUBPNT,   '99999', null, :SUBPNT/10)",
    SUBAREA                   POSITION(36:37)        CHAR,
    CONCURRENT_NG             POSITION(38:38)        CHAR
)

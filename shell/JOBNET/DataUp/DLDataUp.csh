#! /bin/csh 

#####　ダウンロードサービスデータの本番機展開スクリプト 
#####　模試別成績データダウンロードと統計集データの本番機展開を行う

set errorflg = 0
set DLresul = /keinavi/JOBNET/DLresul.log

if ($1 == 1) then
	#--現時刻をセット
	set startfile = `date +%Y%m%d`
	#--登録模試定義ファイルパス
	set examfilepath = "/home/navikdc/datastore/release"
else
	set startfile = "DLstoreexam"
	set examfilepath = "/keinavi/JOBNET"
endif
set checkexamfile = ${examfilepath}/${startfile}

#--登録対象のファイル（現時刻ファイル）から登録対象模試取得
(ls ${checkexamfile} > outtmp.log) >& /dev/null
set examList = `cat outtmp.log`
if ($#examList == 0) then
	echo "--- DLファイル更新のための模試が存在しません（$startfile ファイルが見つかりません） ---"
	echo "--- 処理は終了します。---"
	rm -f outtmp.log
	exit 1
endif

rm -f outtmp.log

#---定義ファイルの転送
echo "--- 模試登録定義ファイルの転送"
if ($1 == 1) then
	#--- 変数内に改行があった場合は削除(空行にも対応)
	set changestr = `cat ${checkexamfile}`
	set changeNewstr = `echo ${changestr} | tr -d '\r\n'`
	set Newcount = 1
	while ($Newcount <= $#changeNewstr)
		echo "$changeNewstr[$Newcount]" >> /keinavi/JOBNET/setexamfile.txt
		@ Newcount ++
	end

	\rm -f ${checkexamfile}
else
	cp -p ${checkexamfile} /keinavi/JOBNET/setexamfile.txt
endif
/keinavi/HULFT/ftp.sh /keinavi/freemenu/examup /keinavi/JOBNET setexamfile.txt 10.1.4.177 orauser

###---模試別成績データダウンロード公開
echo "--- 模試別成績データダウンロードファイルの本番機展開開始"
/usr/bin/rsh satu01 -l kei-navi /home/kei-navi/copy.csh flag
if ($status != 0) then
	echo "---- ERROR： 模試別成績データダウンロードの本番機展開に失敗しました。----"
	echo "-1" > $DLresul
else
	/usr/bin/rsh satu01 -l kei-navi cat /home/kei-navi/resul.log > $DLresul
endif
/usr/bin/rsh satu01 -l kei-navi \rm /home/kei-navi/resul.log

#---データの同期
echo "--- 模試別成績データダウンロードデータの同期開始..."
/usr/bin/rsh satu01 -l kei-navi /usr/local/etc/rsync_keinavi_download.sh
echo "模試別成績データダウンロードデータの同期処理完了 ---"
echo " "



###---統計集データの公開
/usr/bin/rsh satu04 /keinavi/JOBNET/DLfileUp.csh
if ($status != 0) then
	echo "---- ERROR： 統計集データの本番機展開に失敗しました。----"
	set errorflg = 1
endif



###---統計集PDFデータの公開
echo " "
echo "--- 統計集追加PDFデータファイルの本番機展開開始"
set dluplist = `cat /keinavi/JOBNET/setexamfile.txt`
set dlcounts = 1
set pdfupflg = 0
while ($dlcounts <= $#dluplist)
	set targete = $dluplist[$dlcounts]
	set DLyear = `echo $targete | awk -F, '{print $1}'`
	set DLexam = `echo $targete | awk -F, '{print $2}'`
	if (($DLyear != "")&&($DLexam != "")) then
		if (-f "/home/navikdc/PDF/statistics/${DLyear}${DLexam}/statistics${DLyear}${DLexam}.tar") then
			/usr/bin/rsh satu01 -l kei-navi /home/kei-navi/copyPDF.csh $DLyear $DLexam
#			if ($status != 0) then
#				echo "---- ERROR： 統計集追加PDFデータファイルの本番機展開に失敗しました。対象：$targete ----"
#				echo "-1" > $DLresul
#			else
#				/usr/bin/rsh satu01 -l kei-navi cat /home/kei-navi/resul.log > $DLresul
#			endif
			/usr/bin/rsh satu01 -l kei-navi \rm /home/kei-navi/resul.log
			set pdfupflg = 1
		else 
			echo "----  該当模試（${DLyear}${DLexam}）　の統計集PDFファイル(statistics${DLyear}${DLexam}.tar)が存在しません。 ---- "
			echo " "
#			set errorflg = 1
		endif
	endif
	@ dlcounts ++
end
if ($pdfupflg != 0) then
echo "--- 統計集追加PDFデータファイルの同期開始..."
	/usr/bin/rsh satu01 -l kei-navi /usr/local/etc/rsync_keinavi_DLPDF.sh
echo "...統計集追加PDFデータファイルの同期終了 ---"
echo " "
endif
echo "統計集追加PDFデータファイルの本番機展開完了 ---"
echo " "


#---転送定義ファイル&展開データファイルの削除
if ($1 == 1) then
	rm -f /keinavi/JOBNET/setexamfile.txt
endif

set resulcd = `cat $DLresul`
\rm $DLresul
if ($resulcd != 0) then
	exit $resulcd
endif
if ($errorflg != 0) then
	exit 30
endif

echo " "
echo "--- ダウンロードサービスデータの本番機展開完了 ---"
exit 0

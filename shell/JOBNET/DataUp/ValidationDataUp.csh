#! /bin/csh 

###### 集計・個表・マスタデータを検証機へ登録するバッチ
###### 

if ($#argv < 1) then
	echo " ---- param error ---- "
	echo "パラメータ：レギュラーデータ[regular]or事後換算データ[zigo]or得点修正データ[score]orテストデータ[test]orデータ抽出[grep]or再処理データ[rerun]"
	exit 30
endif

set datadiv = $1
set Ecdoutdir = "/home/navikdc/datastore/inside_release" 
set NewExamDir = "/keinavi/HULFT/NewEXAM"
set divfile = "/keinavi/JOBNET/examcd-div.csv"
set univmasterdir = "${NewExamDir}/Univmaster"
set lockfile = "${Ecdoutdir}/store.lck"

set HULFTcheck = "/keinavi/JOBNET/Fcheck/checkEnd.txt"

#--- データ登録中（並行JOBNET起動）かの確認
if (-f ${lockfile}) then
	echo "----  Parallel Processing ERROR  ----"
	echo " 現在、データ登録中のため実行はできません。現在の処理が終了後、再度実行してください "
	echo " "
	echo " 「データ実行中で無い場合は、定義ファイル配置先にあるstore.lokファイルを削除し再度実行してください」"
	exit 30
endif


#--- HULFT転送ファイルの模試チェック
#echo "--- HULFT転送ファイルの模試チェック実行 " 
if (! -f $HULFTcheck) then
	echo "----  JOBNET Validation ERROR  ----"
	echo " 事前に「HULFT受信チェック」JOBNETにて受信チェックをしてください "
	exit 30
else
	\rm -f $HULFTcheck
endif

cd /keinavi/HULFT/NewEXAM
#./check.csh

#--- 検証機校抽出
echo "--- 検証校抽出実行 " 
if ($datadiv == "regular") then
	./grep.csh 0
	if ($status != 0) then
		echo "---- 処理を中断します。----"
		exit 30
	endif

	#--テストデータ削除判別用ファイル作成
	if (-f ${NewExamDir}/selectioncd/delexam) then
		mv ${NewExamDir}/selectioncd/delexam /keinavi/JOBNET/DBdelete/grepexam
	endif

else if ($datadiv == "zigo") then
	./grep.csh -1
	if ($status != 0) then
		echo "---- 処理を中断します。----"
		exit 30
	endif
else if ($datadiv == "score") then
	./grep.csh -2
	if ($status != 0) then
		echo "---- 処理を中断します。----"
		exit 30
	endif
else if ($datadiv == "toritu") then
	./grep.csh -1
	if ($status != 0) then
		echo "---- 処理を中断します。----"
		exit 30
	endif
else if ($datadiv == "test") then
	./grep.csh -3
	if ($status != 0) then
		echo "---- 処理を中断します。----"
		exit 30
	endif
else if ($datadiv == "rerun") then
	./grep.csh -4
	if ($status != 0) then
		echo "--- 処理を中断します。----"
		exit 30
	endif
endif
set selectionname = "selection"

if (-f ${NewExamDir}/selectioncd/grepexam) then
	# --模試成績データ作成定義ファイル作成
	if (! -f /keinavi/JOBNET/ExamDLup) then
		cp -p ${NewExamDir}/selectioncd/grepexam /keinavi/JOBNET/ExamDLup
	endif

	# --学力要素別成績データ作成定義ファイル作成
	if (! -f /keinavi/JOBNET/AbilityDLup) then
		cp -p ${NewExamDir}/selectioncd/grepexam /keinavi/JOBNET/AbilityDLup
	endif

	set examlist = `cat ${NewExamDir}/selectioncd/grepexam`
	set count = 1
	rm -f ${NewExamDir}/selectioncd/grepexam

	echo "--- 模試：$examlist データを検証機登録データとしてセットします。"
		
	#--- 大学マスタ識別&登録模試コード定義の簡易化
	while ($count <= $#examlist)
		set targetexam = $examlist[$count]
		
		#-- 年度＋模試コード取得のため、先頭から6文字取得
		set top6str = `echo $targetexam | awk '{printf "%-.6s\n",$0}'`	
		set examyear = `echo ${top6str} | awk '{printf "%-.4s\n",$0}'`	
                set examcd = `expr ${top6str} : ".*\(..\)"`

#		#--- 高12模試,新テスト,受験学力テストをフォルダ形式にする
#		if (($examcd == 61)||($examcd == 71)) then
#			echo "$targetexam" >> ${NewExamDir}/selectioncd/${examyear}61_71-tmp
#		else if (($examcd == 62)||($examcd == 72)) then
#			echo "$targetexam" >> ${NewExamDir}/selectioncd/${examyear}62_72-tmp
#		else if (($examcd == 63)||($examcd == 73)) then
#			echo "$targetexam" >> ${NewExamDir}/selectioncd/${examyear}63_73-tmp
#-- 高12分割
#		else if (($examcd == 65)||($examcd == 75)) then
#			echo "$targetexam" >> ${NewExamDir}/selectioncd/${examyear}65_75-tmp
#		else if (($examcd == 28)||($examcd == 68)||($examcd == 78)) then
#			echo "$targetexam" >> ${NewExamDir}/selectioncd/${examyear}28_68_78-tmp
#		else if (($examcd == 37)||($examcd == 69)||($examcd == 79)) then
#			echo "$targetexam" >> ${NewExamDir}/selectioncd/${examyear}37_69_79-tmp
#		else
			echo "$targetexam" >> ${NewExamDir}/selectioncd/grepexam
#		endif

		#--- 模試コードより対応模試区分コードを取得
		set ecdanddiv = `grep "^$examcd" $divfile`
		if ($#ecdanddiv == 0) then
			@ count ++
			continue
		endif
		set examdiv = `echo $ecdanddiv | awk -F, '{print $2}'`

		#--- 模試区分00のときは年度を＋１
		if ($examdiv == 00 ) then
			@ examyear ++
		endif
		set targetdiv = "${examyear}${examdiv}"

		#--- 模試コードに対応する模試区分のマスタを模試コードのフォルダに移動
		if (-d ${univmasterdir}/${targetdiv}) then
			cp -p ${univmasterdir}/${targetdiv}/*[^-old] ${NewExamDir}/$targetexam/${targetexam}${selectionname}
			mv ${univmasterdir}/${targetdiv}/* ${NewExamDir}/$targetexam
			rmdir ${univmasterdir}/${targetdiv}
		endif
		sleep 1
		@ count ++
	end

	
	#--- 高12模試/新テストをフォルダ形式にする
	(ls -rt ${NewExamDir}/selectioncd/*-tmp > lists.log) >& /dev/null
	set examdirlist = `cat lists.log`
	\rm -f lists.log 
	if ($#examdirlist != 0) then
		set fcounts = 1
		while ($fcounts <= $#examdirlist)
			set targetTMPfile = $examdirlist[$fcounts]
			#-- ファイルのフルパスから最後/以降（ファイル名）を取得
			set fileonlyname = `echo $targetTMPfile | sed -e 's/.*\///g'`
			#-- ファイル名から後ろ4文字(-tmp)を削除
			set examdirname = `echo $fileonlyname | sed -e 's/....$//'g`
			#-- ファイルのラインを取得し、高12のペアか高1または高2の単体か調べる
			set tmpfline = `cat $targetTMPfile`
			#--- データ種類文字列取得（先頭6文字までの数字を削除）
			set datatystr = `echo $tmpfline[1] | sed -e s'/^[0-9]\{,6\}//'g`
			if (($#tmpfline == 2)||($#tmpfline == 3)) then
				echo "${examdirname}${datatystr}" >> ${NewExamDir}/selectioncd/grepexam
			else
				echo $tmpfline >> ${NewExamDir}/selectioncd/grepexam
			endif
			\rm -f $targetTMPfile

			@ fcounts ++
		end
	endif


	echo "/keinavi/HULFT/NewEXAM" >> ${lockfile}
	chmod 666 ${lockfile}

else
	echo "---- ERROR： 検証へ登録する模試が存在しません ----"
	echo "---- データ送信や登録対象模試定義ファイル配置の確認を行ってなってください ----"
	exit 30
endif


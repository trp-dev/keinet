#! /bin/csh 

###### 集計・個表・マスタデータを本番機登録するための準備起動バッチ
###### 登録対象模試を識別し、ジョブネットを起動
###### 起動にはファイル名が起動日の登録模試定義ファイルの配置が必要

if ($#argv < 1) then
	echo " ---- param error ---- "
	echo "パラメータ：レギュラデータ[regular]orデータ公開[release]"
	exit 30
endif

#--現時刻をセット
set storedate = `date +%Y%m%d`
#--登録模試定義ファイル配置パス
set examconfpath = "/home/navikdc/datastore/release"
#--DL用公開用模試定義ファイル
set targetexamf = "/keinavi/JOBNET/DLstoreexam"
#--絞込み公開用模試定義ファイル
set remainderf = "/keinavi/JOBNET/remainderexam"

cd $examconfpath

set NewExamDir = "/keinavi/HULFT/NewEXAM"

set storelockfile = "${examconfpath}/store.lck"

#--- データ登録中（並行JOBNET起動）かの確認
if (-f ${storelockfile}) then
	echo "----  Parallel Processing ERROR  ----"
	echo " 現在、データ登録中のため実行はできません。現在の処理が終了後、再度実行してください"
	echo " "
	echo "「データ実行中で無い場合は、定義ファイル配置先にあるstore.lokファイルを削除し再度実行してください」"
        exit 30
endif

#--登録対象のファイル（現日付ファイル）から登録対象模試取得
(ls ${examconfpath}/$storedate > logouttmp.log) >& /dev/null
set UpexamLists = `cat logouttmp.log`
\rm -f logouttmp.log
if ($#UpexamLists == 0) then
	echo "---- 模試データ登録のための定義 $storedate ファイルが見つかりません ----"
	echo "---- 処理は終了します。----"
        exit 30
endif

#--- 登録ロックファイル作成
touch $storelockfile
chmod 666 $storelockfile

set datadiv = $1

echo "$UpexamLists に定義されている模試のデータを登録します。"
set UpexamListtemp = `cat $UpexamLists`

#--- 変数内に改行があった場合は削除(空行にも対応)
set UpexamList = `echo ${UpexamListtemp} | tr -d '\r\n'`	

set examcount = 1
while ($examcount <= $#UpexamList)

	set nowstoreexam = $UpexamList[$examcount]

	#--- 変数内に改行があった場合は削除
#	set nowstoreexam = `echo ${nowstoreexamtemp} | tr -d '\r\n'`	

	echo "対象：$nowstoreexam"

	#--- 模試定義レコードから、年度、模試コード、データ種類を取得
	set examyearstr = `echo $nowstoreexam | awk -F, '{printf $1}'`	
	set examcdstr = `echo $nowstoreexam | awk -F, '{printf $2}'`	
	set examtype = `echo $nowstoreexam | awk -F, '{printf $3}'`

	#--- データ種別文字列の誤字確認
	if (($examtype != regular)&&($examtype != release)&&($examtype != zigo)&&($examtype != score)&&($examtype != toritu)) then
		echo "---- 定義ファイル内のデータ種別文字列に誤りがあります ----"
		\rm -f $storelockfile              #--ロックファイル削除
		#-- 各種ファイルの削除
		(ls ${NewExamDir}/selectioncd/*-tmp > del.log) >& /dev/null
		set deletelist = `cat del.log`
		\rm -f del.log
		if ($#deletelist != 0) then
			\rm -f ${NewExamDir}/selectioncd/*-tmp
		endif
		if (-f ${remainderf}) then
			\rm -f ${remainderf}
		else if (-f ${targetexamf}) then
			\rm -f ${targetexamf}
		endif
		exit 30
	endif
		

	#--- 統計集作成のためのレギュラーデータ投入模試を判別(a_レギュラ登録1JOBNET起動時)
	#--- 模試データ公開のための投入模試の判別(b_データリリースJOBNET起動時)
	if ((($datadiv == regular)&&($examtype != regular))||(($datadiv == release)&&($examtype == regular))) then
		echo "---- 起動JOBNETに対して、$nowstoreexam はデータ種別文字列が異なります。----"
		\rm -f $storelockfile              #--ロックファイル削除
		#-- 各種ファイルの削除
		(ls ${NewExamDir}/selectioncd/*-tmp > del.log) >& /dev/null
		set deletelist = `cat del.log`
		\rm -f del.log
		if ($#deletelist != 0) then
			\rm -f ${NewExamDir}/selectioncd/*-tmp
		endif
		if (-f ${remainderf}) then
			\rm -f ${remainderf}
		else if (-f ${targetexamf}) then
			\rm -f ${targetexamf}
		endif
		exit 30
	endif

	
	#--- 登録模試コード定義の簡易化 (高12模試をフォルダ形式にするための識別)
#	if (($examcdstr == 61)||($examcdstr == 71)) then
#		echo "$nowstoreexam" >> ${NewExamDir}/selectioncd/${examyearstr}61_71${examtype}-tmp
#	else if (($examcdstr == 62)||($examcdstr == 72)) then
#		echo "$nowstoreexam" >> ${NewExamDir}/selectioncd/${examyearstr}62_72${examtype}-tmp
#	else if (($examcdstr == 63)||($examcdstr == 73)) then
#		echo "$nowstoreexam" >> ${NewExamDir}/selectioncd/${examyearstr}63_73${examtype}-tmp
#-高12分割
#	else if (($examcdstr == 65)||($examcdstr == 75)) then
#		echo "$nowstoreexam" >> ${NewExamDir}/selectioncd/${examyearstr}65_75${examtype}-tmp
#	else if (($examcdstr == 28)||($examcdstr == 68)||($examcdstr == 78)) then
#		echo "$nowstoreexam" >> ${NewExamDir}/selectioncd/${examyearstr}28_68_78${examtype}-tmp
#	else if (($examcdstr == 37)||($examcdstr == 69)||($examcdstr == 79)) then
#		echo "$nowstoreexam" >> ${NewExamDir}/selectioncd/${examyearstr}37_69_79${examtype}-tmp
#	else
		echo "$nowstoreexam" >> ${remainderf}
		
#	endif

	#--- DLファイル展開用定義ファイル作成
	if ($datadiv != regular) then
		#--- OP模試は対象外
		if (($examcdstr != 12)&&($examcdstr != 13)&&($examcdstr != 15)&&($examcdstr != 16)&&($examcdstr != 18)&&($examcdstr != 19)&&($examcdstr != 21)&&($examcdstr != 22)&&($examcdstr != 24)&&($examcdstr != 25)&&($examcdstr != 26)&&($examcdstr != 27)&&($examcdstr != 31)&&($examcdstr != 32)&&($examcdstr != 41)&&($examcdstr != 42)&&($examcdstr != 43)) then
			echo "$nowstoreexam" >> ${targetexamf}
		endif
	endif

	sleep 1
	@ examcount ++
end

	
#--- 高12模試/新テストをディレクトリ形式にする
(ls -rt ${NewExamDir}/selectioncd/*-tmp > dlists.log) >& /dev/null
set examflist = `cat dlists.log`
\rm -f dlists.log
if ($#examflist != 0) then
	set fcounts = 1
	while ($fcounts <= $#examflist)
		set TMPfile = $examflist[$fcounts]
		#-- ファイルのフルパスから最後/以降（ファイル名）を取得
		set filename = `echo $TMPfile | sed -e s'/.*\///'g`
		#-- ファイル名の後ろから英文字と-を削除
		set examdirname = `echo $filename | sed -e s'/[a-z-]*$//'g`
		#-- 前から4文字削除(ディレクトリ形式の模試コード取得)
		set examcddir = `echo $examdirname | sed -e s'/^[0-9]\{4\}//'g`
		#-- ファイルのラインを取得し、高12のペアか高1または高2の単体か調べる
		set tmpfline = `cat $TMPfile`
		#--- データ種類文字列と年度取得
		set eyearstr = `echo $tmpfline[1] | awk -F, '{printf $1}'`
		set datatystr = `echo $tmpfline[1] | awk -F, '{printf $3}'`
		if (($#tmpfline == 2)||($#tmpfline == 3)) then
			echo "${eyearstr},${examcddir},${datatystr}" >> ${remainderf}
		else
			echo $tmpfline[1] >> ${remainderf}
		endif
		\rm -f $TMPfile

		@ fcounts ++
	end
endif


\rm -f ${examconfpath}/$storedate

#else
#	echo "---- 検証へ登録する模試が存在しません ----"
#	echo "---- データ送信や登録対象模試定義ファイル配置の確認を行ってなってください ----"
#	exit
#endif


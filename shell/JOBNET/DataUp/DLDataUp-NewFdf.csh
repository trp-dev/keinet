#! /bin/csh 

#####　ダウンロードサービスデータの本番機展開スクリプト (隠しコマンド)
#####　統計集データの本番機展開を行う

set errorflg = 0
set DLresul = /keinavi/JOBNET/DLresul.log

###---統計集PDFデータの公開

if (! -f /keinavi/JOBNET/setexamfile.txt) then
	echo " ---- Error : 定義ファイル（/keinavi/JOBNET/setexamfile.txt）を配置してください ----"
	exit 30
endif

echo "--- 統計集追加PDFデータファイルの本番機展開開始"
set dluplist = `cat /keinavi/JOBNET/setexamfile.txt`
set dlcounts = 1
set pdfupflg = 0
while ($dlcounts <= $#dluplist)
	set targete = $dluplist[$dlcounts]
	set DLyear = `echo $targete | awk -F, '{print $1}'`
	set DLexam = `echo $targete | awk -F, '{print $2}'`
	if (($DLyear != "")&&($DLexam != "")) then
		if (-f "/home/navikdc/PDF/statistics/${DLyear}${DLexam}/statistics${DLyear}${DLexam}.tar") then
			rsh royn9 -l kei-navi /home/kei-navi/copyPDF.csh $DLyear $DLexam
#			if ($status != 0) then
#				echo "---- ERROR： 統計集追加PDFデータファイルの本番機展開に失敗しました。対象：$targete ----"
#				echo "-1" > $DLresul
#			else
#				rsh royn9 -l kei-navi cat /home/kei-navi/resul.log > $DLresul
#			endif
			rsh royn9 -l kei-navi \rm /home/kei-navi/resul.log
			set pdfupflg = 1
		else 
			echo "----  該当模試（${DLyear}${DLexam}）　の統計集PDFファイル(statistics${DLyear}${DLexam}.tar)が存在しません。 ---- "
			echo " "
#			set errorflg = 1
		endif
	endif
	@ dlcounts ++
end
if ($pdfupflg != 0) then
echo "--- 統計集追加PDFデータファイルの同期開始..."
	rsh royn9 -l root /usr/local/etc/rsync_keinavi_DLPDF.sh
echo "...統計集追加PDFデータファイルの同期終了 ---"
echo " "
endif
echo "統計集追加PDFデータファイルの本番機展開完了 ---"
echo " "


#---転送定義ファイル&展開データファイルの削除
if ($1 == 1) then
	rm -f /keinavi/JOBNET/setexamfile.txt
endif

if ($errorflg != 0) then
	exit 30
endif

exit 0

#!/bin/csh 

###### ファイルの展開＆削除スクリプト

cd /home/navikdc/PDF


(ls ./PDF_* > tmp.log) >& /dev/null
set list = `cat tmp.log`
\rm tmp.log
if ($#list == 0) then
	exit 0
else
	echo " $#list ファイルを確認"
endif


set count = 1
while($count <= $#list)
	set targetfile = $list[$count]
	
	echo "${targetfile} を解凍"
	/usr/bin/unzip -o ${targetfile}
	#\rm -f ${targetfile}

	@ count ++
end

chmod 664 *.pdf

exit 0

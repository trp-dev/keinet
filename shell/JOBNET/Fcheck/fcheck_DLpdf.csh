#! /bin/csh

### DL用PDFファイル受信チェックスクリプト
### ※スクリプト起動前にチェック対象定義ファイル(DLtargetexams)が必要.
###   DLtargetexamsのファイル内容 : 年度,模試コード 　のCSV形式
### ※個別にPDFファイルの更新時はパラメータにupdateを付与

#-- チェック対象ファイル配置先
set checkfiles = "/home/navikdc/PDF"

#-- チェック対象定義ファイル
set storeexam = "/keinavi/JOBNET/Fcheck"
set Dtargetexams = ${storeexam}/DLtargetexams

#-- チェック結果出力ファイル
set basicDir = "/keinavi/JOBNET/JN_StatisticePDF"
set outfile = ${basicDir}/targetExamPDF

#-- PDF提供対象模試＆PDFファイル名定義ファイル
set filename = ${basicDir}/DLpdffilename
set examlist = ${basicDir}/DLpdftargetList

#-- チェック用定義ファイルの有無確認
if (! -f ${Dtargetexams}) then
	echo "  ---- チェック対象の模試が存在しません ----"
	exit 1
endif

#-- 旧定義ファイルの削除
if (-f $outfile) then
	\rm -f $outfile
endif


#-- zipファイルの解凍
${storeexam}/zip_decompress.csh


#-- チェック模試を取得
set list = `cat $Dtargetexams`

set errflg = 0
set tgcount = 1
while ($tgcount <= $#list)
	set exams = $list[$tgcount]

	echo "--- ターゲット $exams "
	echo " "

	set DLyear = `echo $exams | awk -F, '{print $1}'`
	set DLexam = `echo $exams | awk -F, '{print $2}'`

	#-- 対象模試より対象PDFファイルのキー番号を取得
	set target = `grep ^${DLexam} ${examlist}`
	set count = `echo ${target} | awk -F, '{ for(i=1;i<=NF;i++) print $i;}'`

	#-- 各PDFファイル名を取得
	set targetcount = 1
	set errexam = 0
	set str = ""
	while($targetcount <= $#count)
		if ($targetcount == 1) then
			echo "--- 模試コード $count[$targetcount] の各ファイルの有無確認を開始します・・・"
			set str = $DLyear,$count[$targetcount]

			@ targetcount ++
			continue
		endif
		#-- キー番号より定義PDFファイル名取得
		set tgfname = `grep ^$count[$targetcount] ${filename}`
		set name = `echo $tgfname | awk -F, '{print $2}'`

		set str = "${str},${name}"

		cd ${checkfiles}

		(ls -rt ${name}${DLexam}* > ${storeexam}/PDFout.log) >& /dev/null
		set lists = `cat ${storeexam}/PDFout.log`
		\rm -rf ${storeexam}/PDFout.log
		if ($#lists == 0) then
			echo "---- ファイル名に（${name}${DLexam}）が含まれるファイルが1つも存在しません。 ----"
			set errflg = 1
			set errexam = 1
		else
			echo "　${name} ファイル 　[${name}${DLexam}] が含まれるファイルを [$#lists] つ確認しました"
		endif

		@ targetcount ++
	end

	if ($targetcount > 1) then
		echo "$str" >> $outfile
	endif

	if ($errexam != 0) then
		echo "---- 模試コード ${DLexam} の各ファイルの有無確認に問題が発生しました ----"

	else
		echo "模試コード ${DLexam} の各ファイルの有無確認が正常に終了しました ---"
	endif
	@ tgcount ++

	echo " "
end

\rm -f $Dtargetexams

#-- ファイルチェックでエラー時は定義ファイルの削除(ただしupdateパラメタ-時は無視)
if ($errflg != 0) then
	if ($1 != "update") then
		\rm -f $outfile
	endif
	exit 30
endif

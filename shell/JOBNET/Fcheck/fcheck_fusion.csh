#! /bin/csh

set checkfusionexam = "/keinavi/JOBNET/Fcheck/fusionexam"

#-- 現時刻をセット
set nowdate = `date +%Y%m%d%H%M`

#-- ログ出力先
set fchkoutpath = "/keinavi/JOBNET/Fcheck/log"
set fchklog = "${fchkoutpath}/fchk_fusion${nowdate}.log"


#-- チェック用定義ファイルの有無確認
if (! -f ${checkfusionexam}) then
	echo "  ---- チェック対象の模試が存在しません ----"
	exit 1
endif

#-- チェック模試を取得
set list = `cat $checkfusionexam`

#set divfile = "/keinavi/JOBNET/examcd-div.csv"

#-- 定義模試より登録対象模試データファイルのチェックをする	
set count = 1
set errorflg = 0         #-- バッチ処理内の全てに対してのエラーフラグ
set yearflg = 0             #--ダウンロードファイル確認フラグ
while($count <= $#list)
	cd /keinavi/HULFT/NewEXAM

	set examerrorflg = 0    #-- 処理する1模試に対してのエラーフラグ

	#-- リストから(年度+模試コード)を取得
	set year = $list[$count]
	set year2 = `echo $year | awk '{printf "%-.4s\n",$0}'`
	set exam2 = `expr $year : ".*\(..\)"`

	echo "　"
	echo "--- 模試　$year の各データファイル有無確認の開始します"
	echo "--- 模試　$year の各データファイル有無確認の開始します" >> $fchklog

	#-- 高123プレステージは対象外
	if (($exam2 == 10)||($exam2 == 76)||($exam2 == 77)) then
		break
	endif

#	#-- 抽出対象フォルダを模試ごとに識別
#	if (($exam2 == 61)||($exam2 == 71)) then
#		set indir = ${year2}61_71                     #第１回高12模試
#	else if (($exam2 == 62)||($exam2 == 72)) then
#		set indir = ${year2}62_72                     #第２回高12模試
#	else if (($exam2 == 63)||($exam2 == 73)) then
#		set indir = ${year2}63_73                     #第３回高12模試
#	else if (($exam2 == 28)||($exam2 == 68)||($exam2 == 78)) then
#		set indir = ${year2}28_68_78                  #新テスト
#	else if (($exam2 == 37)||($exam2 == 69)||($exam2 == 79)) then
#		set indir = ${year2}37_69_79                  #受験学力測定テスト
#	else
		set indir = $year
#	endif

	#-- 抽出用対象データフォルダの確認
	if (($#argv == 0)&&(! -d $indir)) then
#		#-- 模試データが単体できたときのデータフォルダ
#		if (($exam2 == 61)||($exam2 == 71)||($exam2 == 62)||($exam2 == 72)||($exam2 == 63)||($exam2 == 73)) then
#			set indir = $year
#			if (! -d $indir) then
#				echo "  ---- ERROR： 確認対象となるデータファイル（フォルダ $indir ）がみつかりません ----"
#				@ errorflg ++
#				@ examerrorflg ++
#				@ count ++
#				continue
#			endif
#		else
			echo "  ---- ERROR： 確認対象となるデータファイル（フォルダ $indir ）がみつかりません ----"
			@ errorflg ++
			@ examerrorflg ++
			@ count ++
			continue
#		endif
	endif


	###-- チェック対象が過去1年前のとき
	if ($yearflg == 0) then
		###-- 志望大学評価別人数(全国)チェック(ファイルが無く、チェック対象が過去1年前のとき)
		if (! -f $indir/RATINGNUMBER_A) then
			if (($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "  ---- 志望大学評価別人数(全国)データ　[RATINGNUMBER_A]　【RZRM1150】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/RATINGNUMBER_A`
			set datetime = `ls -l $indir/RATINGNUMBER_A`
			echo "　志望大学評価別人数(全国)　データ[RATINGNUMBER_A] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　志望大学評価別人数(全国)　データ[RATINGNUMBER_A] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 志望大学評価別人数(県)チェック(ファイルが無く、チェック対象が過去1年前のとき)
		if (! -f $indir/RATINGNUMBER_P) then
			if (($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "  ---- 志望大学評価別人数(県)データ　[RATINGNUMBER_P]　【RZRM1250】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/RATINGNUMBER_P`
			set datetime = `ls -l $indir/RATINGNUMBER_P`
			echo "　志望大学評価別人数(県)　データ　[RATINGNUMBER_P]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　志望大学評価別人数(県)　データ　[RATINGNUMBER_P]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 志望大学評価別人数(高校)チェック(ファイルが無く、チェック対象が過去1年前のとき)
		if (! -f $indir/RATINGNUMBER_S) then
			if (($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "  ---- 志望大学評価別人数(高校)データ　[RATINGNUMBER_S]　【RZRM1350】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/RATINGNUMBER_S`
			set datetime = `ls -l $indir/RATINGNUMBER_S`
			echo "　志望大学評価別人数(高校)　データ[RATINGNUMBER_S] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　志望大学評価別人数(高校)　データ[RATINGNUMBER_S] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 志望大学評価別人数(クラス)チェック(ファイルが無く、チェック対象が過去1年前のとき)
		if (! -f $indir/RATINGNUMBER_C) then
			if (($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "  ---- 志望大学評価別人数(クラス)データ [RATINGNUMBER_C] 【RZRM1450】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/RATINGNUMBER_C`
			set datetime = `ls -l $indir/RATINGNUMBER_C`
			echo "　志望大学評価別人数(クラス)　データ[RATINGNUMBER_C] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　志望大学評価別人数(クラス)　データ[RATINGNUMBER_C] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

	endif


	###-- 模試受験者総数(高校)チェック
	if (! -f $indir/EXAMTAKENUM_S) then
			echo "  ---- 模試受験者総数(高校)データ　[EXAMTAKENUM_S]　【RZRM1380】が存在しません ----" 
			@ errorflg ++
			@ examerrorflg ++
	else
		set size = `wc -cl $indir/EXAMTAKENUM_S`
		set datetime = `ls -l $indir/EXAMTAKENUM_S`
		echo "　模試受験者総数(高校)　データ　　　[EXAMTAKENUM_S] のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
		echo "　模試受験者総数(高校)　データ　　　[EXAMTAKENUM_S] のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
	endif


	###-- 科目別成績(高校)チェック
	if (! -f $indir/SUBRECORD_S) then
			echo "  ---- 科目別成績(高校)データ　[SUBRECORD_S]　【RZRM1310】が存在しません ----" 
			@ errorflg ++
			@ examerrorflg ++
	else
		set size = `wc -cl $indir/SUBRECORD_S`
		set datetime = `ls -l $indir/SUBRECORD_S`
		echo "　科目別成績(高校)　データ　　　　　　[SUBRECORD_S]　　のファイルを　[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
		echo "　科目別成績(高校)　データ　　　　　　[SUBRECORD_S]　　のファイルを　[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
	endif


	###-- 科目分布別成績(高校)チェック
	if (! -f $indir/SUBDISTRECORD_S) then
			echo "  ---- 科目分布別成績(高校)データ　[SUBDISTRECORD_S]　【RZRM1320】が存在しません ----" 
			@ errorflg ++
			@ examerrorflg ++
	else
		set size = `wc -cl $indir/SUBDISTRECORD_S`
		set datetime = `ls -l $indir/SUBDISTRECORD_S`
		echo "　科目分布別成績(高校)　データ　　　[SUBDISTRECORD_S] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
		echo "　科目分布別成績(高校)　データ　　　[SUBDISTRECORD_S] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
	endif



	if ($examerrorflg != 0) then
		echo "---- 模試 $year の各データファイル有無確認に問題が発生しました ----"
		echo "---- 模試 $year の各データファイル有無確認に問題が発生しました ----" >> $fchklog
	else
		echo " 模試 $year の各データファイル有無確認が正常に終了しました ---"
		echo " 模試 $year の各データファイル有無確認が正常に終了しました ---" >> $fchklog
	endif

	
	#--- 1年前か2年前かの対象過年度フラグのセット
	if ($yearflg == 1) then
		set yearflg = 0
	else
		set yearflg = 1
	endif

	@ count ++
end


#--- エラーが一つでもあったら異常終了
if ($errorflg != 0) then
	#-- 異常終了でも次のバッチへ進めるようにする特殊例用
	if ($errorflg >= 100) then
		touch /keinavi/JOBNET/Fcheck/checkEnd.txt
		\rm -f $checkfusionexam
	
		echo " "
		echo " ******** 特例処理によりデータファイルが存在しない場合は、そのままスキップして次に進んでください ******** "
	endif
		
	exit 30
endif

#--- 正常終了時はデータ登録バッチの起動が可能なためのファイル作成
touch /keinavi/JOBNET/Fcheck/checkEnd.txt
\rm -f $checkfusionexam

#! /bin/csh

set examconffpath = "/home/navikdc/datastore/inside_release"
set examconfreleasefpath = "/home/navikdc/datastore/release"

#-- 現時刻をセット
set nowdate = `date +%Y%m%d%H%M`

#-- ログ出力先
set fchkoutpath = "/keinavi/JOBNET/Fcheck/log"
set fchklog = "${fchkoutpath}/fchk${nowdate}.log"

#-- ログファイル数の制限
set fchkcount = `ls -t $fchkoutpath`
set fcounter = 1
while ($fcounter <= $#fchkcount) 
	if ($fcounter > 9) then
		\rm -f $fchkoutpath/$fchkcount[$fcounter]
	endif
	@ fcounter ++
end

#-- store.lckが存在時は異常終了時と見なし、各tempファイルなどを削除（本番用、検証用共に）
#-- よってデータ登録時にファイルチェックバッチ起動はNGとする。
#if ((-f ${examconffpath}/store.lck)||(-f ${examconfreleasefpath}/store.lck)) then
#	/keinavi/JOBNET/storestart/StoreReset.csh
#endif


#-- 出力用ファイルの削除
if (-f /keinavi/JOBNET/ExamDLup) then
	\rm -f /keinavi/JOBNET/ExamDLup
endif
if (-f /keinavi/JOBNET/TYOUSAup) then
	\rm -f /keinavi/JOBNET/TYOUSAup
endif
if (-f /keinavi/JOBNET/Fcheck/fusionexam) then
	\rm -f /keinavi/JOBNET/Fcheck/fusionexam
endif
if (-f /keinavi/JOBNET/Fcheck/DLtargetexams) then
	\rm -f /keinavi/JOBNET/Fcheck/DLtargetexams
endif

#-- DLPDF対象模試定義ファイル
set DLexamlist = /keinavi/JOBNET/JN_StatisticePDF/DLpdftargetList


#-- 登録模試定義ファイルの有無確認
echo "--- データ登録定義ファイルの有無確認"
(ls ${examconffpath}/* > tmp.log) >& /dev/null
set list = `cat tmp.log`
\rm tmp.log
if ($#list == 0) then
	echo "  ---- ERROR： データ登録定義ファイルが見つかりません ----"
	exit 30
else
	echo " $#list ファイルを確認"
endif

#-- 実行パラメータなし時のファイルチェックシェル実行
if ($#argv == 0) then
	#--- HULFT転送ファイルの模試チェック
	echo "　"
	echo "--- HULFT転送ファイルの模試チェック実行 "
	cd /keinavi/HULFT/NewEXAM
	./check.csh
endif

set divfile = "/keinavi/JOBNET/examcd-div.csv"

#-- 定義模試より登録対象模試データファイルのチェックをする	
set count = 1
set errorflg = 0         #-- バッチ処理内の全てに対してのエラーフラグ
set DLfg = 1             #--ダウンロードファイル確認フラグ
while($count <= $#list)
	cd /keinavi/HULFT/NewEXAM

	set examerrorflg = 0    #-- 処理する1模試に対してのエラーフラグ

	#-- 抽出対象模試ファイルをリストから取得
	set targetfile = $list[$count]
	#-- フルファイルパスからファイル名文字列を取得
	set failname = `echo $targetfile | sed -e 's/.*\///g'`
	#-- ファイル名から数字を削除(データ種類文字列取得)
	set examtypstr = `echo $failname | sed -e s'/[0-9]*$//'g`
	#-- ファイル名から後ろ6バイト分(年度+模試コード)を取得
	set year = `expr $failname : ".*\(......\)"`
	set year2 = `echo $year | awk '{printf "%-.4s\n",$0}'`
	set exam2 = `expr $year : ".*\(..\)"`

	echo "　"
	echo "--- 模試　$failname の各データファイル有無確認の開始します"
	echo "--- 模試　$failname の各データファイル有無確認の開始します" >> $fchklog

	#-- データ種類の判別
	if (($examtypstr == zigo)||($examtypstr == toritu)) then
		set getflg = -1
	else if ($examtypstr == score) then
		set getflg = -2
	else if ($examtypstr == test) then
		set getflg = -3
	else if ($examtypstr == rerun) then
		set getflg = -4
	else if ($examtypstr == "") then
		set getflg = 0
	else
		echo "---- ERROR：データ登録定義ファイル名が異なります。ファイル名の確認をお願いします($failname)----"
		echo "---- ERROR：データ登録定義ファイル名が異なります。ファイル名の確認をお願いします($failname)----" >> $fchklog
		exit 30
	endif


	#-- 抽出対象フォルダを模試ごとに識別
#	if (($exam2 == 61)||($exam2 == 71)) then
#		set indir = ${year2}61_71                     #第１回高12模試
#	else if (($exam2 == 62)||($exam2 == 72)) then
#		set indir = ${year2}62_72                     #第２回高12模試
#	else if (($exam2 == 63)||($exam2 == 73)) then
#		set indir = ${year2}63_73                     #第３回高12模試
#	else if (($exam2 == 28)||($exam2 == 68)||($exam2 == 78)) then
#		set indir = ${year2}28_68_78                  #新テスト
#	else if (($exam2 == 37)||($exam2 == 69)||($exam2 == 79)) then
#		set indir = ${year2}37_69_79                  #受験学力測定テスト
#	else
		set indir = $year
#	endif

	#-- 抽出用対象データフォルダの確認
	if (($#argv == 0)&&(! -d $indir)) then
		#-- 模試データが単体できたときのデータフォルダ
#		if (($exam2 == 61)||($exam2 == 71)||($exam2 == 62)||($exam2 == 72)||($exam2 == 63)||($exam2 == 73)) then
#			set indir = $year
#			if (! -d $indir) then
#				echo "  ---- ERROR： 確認対象となるデータファイル（フォルダ $indir ）がみつかりません ----"
#				@ errorflg ++
#				@ examerrorflg ++
#				@ count ++
#				continue
#			endif
#		else
			echo "  ---- ERROR： 確認対象となるデータファイル（フォルダ $indir ）がみつかりません ----"
			@ errorflg ++
			@ examerrorflg ++
			@ count ++
			continue
#		endif
	endif


	if ((($getflg != -1)&&($getflg != -2))&&($#argv == 0)) then
		###-- 模試受験者総数(全国)チェック
		if (-f $indir/EXAMTAKENUM_A) then
			set size = `wc -cl $indir/EXAMTAKENUM_A`
			set datetime = `ls -l $indir/EXAMTAKENUM_A`
			echo "　模試受験者総数(全国)　データ　　　[EXAMTAKENUM_A]　のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　模試受験者総数(全国)　データ　　　[EXAMTAKENUM_A]　のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		else
			if ($getflg != -4) then
				echo "  ---- 模試受験者総数(全国)データ　[EXAMTAKENUM_A]　【RZRG1180】が存在しません ----" 
				echo "  ---- 模試受験者総数(全国)データ　[EXAMTAKENUM_A]　【RZRG1180】が存在しません ----" >> $fchklog
				@ errorflg ++
				@ examerrorflg ++
			endif
		endif

		###-- 模試受験者総数(県)チェック
		if (! -f $indir/EXAMTAKENUM_P) then
			echo "  ---- 模試受験者総数(県)データ　[EXAMTAKENUM_P]　【RZRG1280】が存在しません ----" 
			@ errorflg ++
			@ examerrorflg ++
		else
			set size = `wc -cl $indir/EXAMTAKENUM_P`
			set datetime = `ls -l $indir/EXAMTAKENUM_P`
			echo "　模試受験者総数(県)　データ　　 　　[EXAMTAKENUM_P]　のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　模試受験者総数(県)　データ　　 　　[EXAMTAKENUM_P]　のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 科目別成績(全国)チェック
		if (! -f $indir/SUBRECORD_A) then
			echo "  ---- 科目別成績(全国)データ　[SUBRECORD_A]　【RZRG1110】が存在しません ----" 
			@ errorflg ++
			@ examerrorflg ++
		else
			set size = `wc -cl $indir/SUBRECORD_A`
			set datetime = `ls -l $indir/SUBRECORD_A`
			echo "　科目別成績(全国)　データ　　　　　　[SUBRECORD_A]　　のファイルを　[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　科目別成績(全国)　データ　　　　　　[SUBRECORD_A]　　のファイルを　[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 科目別成績(県)チェック
		if (! -f $indir/SUBRECORD_P) then
			echo "  ---- 科目別成績(県)　[SUBRECORD_P]　【RZRG1210】が存在しません ----" 
			@ errorflg ++
			@ examerrorflg ++
		else
			set size = `wc -cl $indir/SUBRECORD_P`
			set datetime = `ls -l $indir/SUBRECORD_P`
			echo "　科目別成績(県)　データ　　　　　　 　[SUBRECORD_P]　　のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　科目別成績(県)　データ　　　　　　 　[SUBRECORD_P]　　のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 科目分布別成績(全国)チェック
		if (! -f $indir/SUBDISTRECORD_A) then
			echo "  ---- 科目分布別成績(全国)データ　[SUBDISTRECORD_A]　【RZRG1120】が存在しません ----" 
			@ errorflg ++
			@ examerrorflg ++
		else
			set size = `wc -cl $indir/SUBDISTRECORD_A`
			set datetime = `ls -l $indir/SUBDISTRECORD_A`
			echo "　科目分布別成績(全国)　データ　　　[SUBDISTRECORD_A] のファイルを[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　科目分布別成績(全国)　データ　　　[SUBDISTRECORD_A] のファイルを[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 科目分布別成績(県)チェック
		if (! -f $indir/SUBDISTRECORD_P) then
			echo "  ---- 科目分布別成績(県)データ　[SUBDISTRECORD_P]　【RZRG1220】が存在しません ----" 
			@ errorflg ++
			@ examerrorflg ++
		else
			set size = `wc -cl $indir/SUBDISTRECORD_P`
			set datetime = `ls -l $indir/SUBDISTRECORD_P`
			echo "　科目分布別成績(県)　データ　　 　　[SUBDISTRECORD_P] のファイルを[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　科目分布別成績(県)　データ　　 　　[SUBDISTRECORD_P] のファイルを[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 科目別成績層別成績(全国)チェック
		if (! -f $indir/SUBDEVZONERECORD_A) then
			#--- 受験学力測定テスト以外はエラー
			if (($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "  ---- 科目別成績層別成績(全国)データ　[SUBDEVZONERECORD_A]　【RZRG1100】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/SUBDEVZONERECORD_A`
			set datetime = `ls -l $indir/SUBDEVZONERECORD_A`
			echo "　科目別成績層別成績(全国)　データ　　　[SUBDEVZONERECORD_A] のファイルを[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　科目別成績層別成績(全国)　データ　　　[SUBDEVZONERECORD_A] のファイルを[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 設問別成績(全国)チェック
		if (! -f $indir/QRECORD_A) then
			#--- センターリサーチと受験学力測定テスト以外はエラー
			if (($exam2 != 38)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "  ---- 設問別成績(全国)データ　[QRECORD_A]　【RZRG1130】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/QRECORD_A`
			set datetime = `ls -l $indir/QRECORD_A`
			echo "　設問別成績(全国)　データ　　　　　　[QRECORD_A]　　　　のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　設問別成績(全国)　データ　　　　　　[QRECORD_A]　　　　のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 設問別成績(県)チェック
		if (! -f $indir/QRECORD_P) then
			#--- センターリサーチと受験学力測定テスト以外はエラー
			if (($exam2 != 38)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "  ---- 設問別成績(県)データ　[QRECORD_P]　【RZRG1230】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/QRECORD_P`
			set datetime = `ls -l $indir/QRECORD_P`
			echo "　設問別成績(県)　データ　　　　　　 　[QRECORD_P]　　　　　のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　設問別成績(県)　データ　　　　　　 　[QRECORD_P]　　　　　のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 設問別成績層成績(全国)チェック
		if (! -f $indir/QRECORDZONE_A) then
			#--- センターリサーチと受験学力測定テスト以外はエラー
			if (($exam2 != 38)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "  ---- 設問別成績層成績(全国)データ　[QRECORDZONE_A]　【RZRG1140】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/QRECORDZONE_A`
			set datetime = `ls -l $indir/QRECORDZONE_A`
			echo "　設問別成績層成績(全国)　データ　　　　　　[QRECORDZONE_A]　　　　のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　設問別成績層成績(全国)　データ　　　　　　[QRECORDZONE_A]　　　　のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 志望大学評価別人数(全国)チェック
		if (! -f $indir/RATINGNUMBER_A) then
#			if (($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)) then
			if (($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "  ---- 志望大学評価別人数(全国)データ　[RATINGNUMBER_A]　【RZRG1150】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/RATINGNUMBER_A`
			set datetime = `ls -l $indir/RATINGNUMBER_A`
			echo "　志望大学評価別人数(全国)　データ[RATINGNUMBER_A] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　志望大学評価別人数(全国)　データ[RATINGNUMBER_A] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 志望大学評価別人数(県)チェック
		if (! -f $indir/RATINGNUMBER_P) then
#			if (($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)) then
			if (($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "  ---- 志望大学評価別人数(県)データ　[RATINGNUMBER_P]　【RZRG1250】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/RATINGNUMBER_P`
			set datetime = `ls -l $indir/RATINGNUMBER_P`
			echo "　志望大学評価別人数(県)　データ　[RATINGNUMBER_P]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　志望大学評価別人数(県)　データ　[RATINGNUMBER_P]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

#		###-- マーク模試設問別成績層別正答率（全国）チェック
#		if (! -f $indir/MARKQDEVZONEANSRATE_A) then
#			if (($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66)) then
#				echo "  ---- マーク模試設問別成績層別正答率（全国）データ [MARKQDEVZONEANSRATE_A] 【RZRG1190】が存在しません ----" 
#				@ errorflg ++
#				@ examerrorflg ++
#			endif
#		else
#			set size = `wc -cl $indir/MARKQDEVZONEANSRATE_A`
#			set datetime = `ls -l $indir/MARKQDEVZONEANSRATE_A`
#			echo "　マーク模試設問別成績層別正答率（全国）　データ[MARKQDEVZONEANSRATE_A] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
#			echo "　マーク模試設問別成績層別正答率（全国）　データ[MARKQDEVZONEANSRATE_A] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
#		endif

		###-- 模試設問マスタチェック
		if (! -f $indir/EXAMQUESTION) then
			#--- センターリサーチと受験学力測定テスト以外はエラー
			if (($exam2 != 38)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "  ---- 模試設問マスタ　[EXAMQUESTION]　【RZRG3310】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/EXAMQUESTION`
			set datetime = `ls -l $indir/EXAMQUESTION`
			echo "　模試設問マスタ　データ　　　　　　　[EXAMQUESTION]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　模試設問マスタ　データ　　　　　　　[EXAMQUESTION]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- マーク模試解答番号マスタチェック
		if (! -f $indir/MARKANSWER_NO) then
			if (($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66)) then
				echo "  ---- マーク模試解答番号マスタ　[MARKANSWER_NO]　【RZRG3410】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/MARKANSWER_NO`
			set datetime = `ls -l $indir/MARKANSWER_NO`
			echo "　マーク模試解答番号マスタ　データ [MARKANSWER_NO] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　マーク模試解答番号マスタ　データ [MARKANSWER_NO] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 到達レベルチェック（受験学力測定テストのみ）
		if (! -f $indir/REACHLEVEL) then
			if (($exam2 == 37)||($exam2 == 69)||($exam2 == 79)||($exam2 == 87)||($exam2 == 88)) then
				echo "  ---- 到達レベル　　　[REACHLEVEL]　【RZRG4210】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/REACHLEVEL`
			set datetime = `ls -l $indir/REACHLEVEL`
			echo "　到達レベル　　データ　　　　　　　　[REACHLEVEL] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　到達レベル　　データ　　　　　　　　[REACHLEVEL] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

#		###-- センター到達指標チェック（受験学力測定テストのみ）
#		if (! -f $indir/CENTER_REACHMARK) then
#			if (($exam2 == 37)||($exam2 == 69)||($exam2 == 79)||($exam2 == 87)||($exam2 == 88)) then
#				echo "  ---- センター到達指標　　[CENTER_REACHMARK]　【RZRG4310】が存在しません ----" 
#				@ errorflg ++
#				@ examerrorflg ++
#			endif
#		else
#			set size = `wc -cl $indir/CENTER_REACHMARK`
#			set datetime = `ls -l $indir/CENTER_REACHMARK`
#			echo "　センター到達指標　データ　　　　　[CENTER_REACHMARK] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
#			echo "　センター到達指標　データ　　　　　[CENTER_REACHMARK] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
#		endif

#		###-- 高１２大学マスタチェック
#		if (! -f $indir/HS12_UNIV) then
#			if (($exam2 == 61)||($exam2 == 62)||($exam2 == 63)||($exam2 == 71)||($exam2 == 72)||($exam2 == 73)||($exam2 == 75)) then
#				echo "  ---- 高１２大学マスタデータ　[HS12_UNIV]　【RZRM5210】が存在しません ----" 
#				@ errorflg ++
#				@ examerrorflg ++
#			endif
#		else
#			set size = `wc -cl $indir/HS12_UNIV`
#			set datetime = `ls -l $indir/HS12_UNIV`
#			echo "　高１２大学マスタ　データ　[HS12_UNIV]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
#			echo "　高１２大学マスタ　データ　[HS12_UNIV]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
#		endif

		###-- 高１２大学マスタチェック
		if (! -f $indir/H12_UNIVMASTER) then
#			if (($exam2 == 61)||($exam2 == 62)||($exam2 == 63)||($exam2 == 71)||($exam2 == 72)||($exam2 == 73)||($exam2 == 75)||($exam2 == 76)||($exam2 == 77)) then
			if (($exam2 == 61)||($exam2 == 62)||($exam2 == 63)||($exam2 == 74)||($exam2 == 75)) then
				echo "  ---- 高１２大学マスタデータ　[H12_UNIVMASTER]　【RZRG5210】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/H12_UNIVMASTER`
			set datetime = `ls -l $indir/H12_UNIVMASTER`
			echo "　高１２大学マスタ　データ　[H12_UNIVMASTER]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　高１２大学マスタ　データ　[H12_UNIVMASTER]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		#20200207 ADD ST
		###-- Ｋ－Ｗｅｂ小問マスタチェック
		if (! -f $indir/KWEBSHOQUESTION) then
			if (($exam2 == 05)||($exam2 == 06)||($exam2 == 07)||($exam2 == 61)||($exam2 == 62)||($exam2 == 63)||($exam2 == 65)||($exam2 == 71)||($exam2 == 72)||($exam2 == 73)||($exam2 == 74)||($exam2 == 10)||($exam2 == 76)||($exam2 == 77)) then
				echo "  ---- Ｋ－Ｗｅｂ小問マスタデータ　[KWEBSHOQUESTION]　【RZRG5580】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/KWEBSHOQUESTION`
			set datetime = `ls -l $indir/KWEBSHOQUESTION`
			echo "　Ｋ－Ｗｅｂ小問マスタ　データ　[KWEBSHOQUESTION]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　Ｋ－Ｗｅｂ小問マスタ　データ　[KWEBSHOQUESTION]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- Ｋ－Ｗｅｂ小問学力要素マスタ
		if (! -f $indir/KWEBSHOQUE_ACDMC) then
			if (($exam2 == 05)||($exam2 == 06)||($exam2 == 07)||($exam2 == 61)||($exam2 == 62)||($exam2 == 63)||($exam2 == 65)||($exam2 == 71)||($exam2 == 72)||($exam2 == 73)||($exam2 == 74)||($exam2 == 10)||($exam2 == 76)||($exam2 == 77)) then
				echo "  ---- Ｋ－Ｗｅｂ小問学力要素マスタ　[KWEBSHOQUE_ACDMC]　【RZRG5590】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/KWEBSHOQUE_ACDMC`
			set datetime = `ls -l $indir/KWEBSHOQUE_ACDMC`
			echo "　Ｋ－Ｗｅｂ小問学力要素マスタ　データ　[KWEBSHOQUE_ACDMC]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　Ｋ－Ｗｅｂ小問学力要素マスタ　データ　[KWEBSHOQUE_ACDMC]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif
		
		###-- 記述系模試小問別成績データ（全国）チェック
		if (! -f $indir/SHOQUESTIONRECORD_A) then
			if (($exam2 == 05)||($exam2 == 06)||($exam2 == 07)||($exam2 == 61)||($exam2 == 62)||($exam2 == 63)||($exam2 == 65)||($exam2 == 71)||($exam2 == 72)||($exam2 == 73)||($exam2 == 74)||($exam2 == 10)||($exam2 == 76)||($exam2 == 77)) then
				echo "  ---- 記述系模試小問別成績データ（全国）データ　[SHOQUESTIONRECORD_A]　【RZRG5600】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/SHOQUESTIONRECORD_A`
			set datetime = `ls -l $indir/SHOQUESTIONRECORD_A`
			echo "　記述系模試小問別成績データ（全国）　データ　[SHOQUESTIONRECORD_A]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　記述系模試小問別成績データ（全国）　データ　[SHOQUESTIONRECORD_A]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif
		
		#20200207 ADD ED

		#--- マスタ確認のため、模試コードより対応模試区分コードを取得
		#--- 取得し模試区分コードが模試に対応した場合はマスタ処理をする
		set ecdanddiv = `grep "^$exam2" $divfile`
		if ($#ecdanddiv != 0) then
			set examdiv = `echo $ecdanddiv | awk -F, '{print $2}'`

			#--- 模試区分00のときは年度を＋１
			if ($examdiv == 00 ) then
				@ year2 ++
			endif
			set mastindir = /keinavi/HULFT/NewEXAM/Univmaster/${year2}${examdiv}
#			if (! -f $mastindir/HS3_UNIV_EXAM) then
#				echo "  ---- 高3模試用判定大学マスタ　[HS3_UNIV_EXAM]　【RZRM5120】が存在しません ----"
#				@ errorflg ++
#				@ examerrorflg ++
#			else
#				set size = `wc -cl $mastindir/HS3_UNIV_EXAM`
#				set datetime = `ls -l $mastindir/HS3_UNIV_EXAM`
#				echo "　高3模試用判定大学マスタ　データ  [HS3_UNIV_EXAM]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
#				echo "　高3模試用判定大学マスタ　データ  [HS3_UNIV_EXAM]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
#			endif
#			if (! -f $mastindir/HS3_UNIV_PUB) then
#				echo "  ---- 高3公表用判定大学マスタ　[HS3_UNIV_PUB]　【RZRM5130】が存在しません ----"
#				@ errorflg ++
#				@ examerrorflg ++
#			else
#				set size = `wc -cl $mastindir/HS3_UNIV_PUB`
#				set datetime = `ls -l $mastindir/HS3_UNIV_PUB`
#				echo "　高3公表用判定大学マスタ　データ  [HS3_UNIV_PUB]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
#				echo "　高3公表用判定大学マスタ　データ  [HS3_UNIV_PUB]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
#			endif
#			if (! -f $mastindir/HS3_UNIV_NAME) then
#				echo "  ---- 高3大学マスタ(志望用名称)　[HS3_UNIV_NAME]　【RZRM5110】が存在しません ----"
#				@ errorflg ++
#				@ examerrorflg ++
#			else
#				set size = `wc -cl $mastindir/HS3_UNIV_NAME`
#				set datetime = `ls -l $mastindir/HS3_UNIV_NAME`
#				echo "　高3大学マスタ(志望用名称)　データ  [HS3_UNIV_NAME]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
#				echo "　高3大学マスタ(志望用名称)　データ  [HS3_UNIV_NAME]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
#			endif
#			if (! -f $mastindir/SCHEDULEDETAIL) then
#				echo "  ---- 入試日程詳細　[SCHEDULEDETAIL]　【RZRM5180】が存在しません ----"
#				@ errorflg ++
#				@ examerrorflg ++
#			else
#				set size = `wc -cl $mastindir/SCHEDULEDETAIL`
#				set datetime = `ls -l $mastindir/SCHEDULEDETAIL`
#				echo "　入試日程詳細　データ　　　　　　[SCHEDULEDETAIL]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
#				echo "　入試日程詳細　データ　　　　　　[SCHEDULEDETAIL]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
#			endif
#			if (! -f $mastindir/IRREGULARFILE) then
#				echo "  ---- 判定イレギュラファイル　[IRREGULARFILE]　【RZRM5140】が存在しません ----"
#				@ errorflg ++
#				@ examerrorflg ++
#			else
#				set size = `wc -cl $mastindir/IRREGULARFILE`
#				set datetime = `ls -l $mastindir/IRREGULARFILE`
#				echo "　判定イレギュラファイル　データ　　　　[IRREGULARFILE]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
#				echo "　判定イレギュラファイル　データ　　　　[IRREGULARFILE]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
#			endif
#			if (! -f $mastindir/UNIVCDTRANS_ORD) then
#				echo "  ---- 大学コード変換順引きマスタ　[UNIVCDTRANS_ORD]　【RZRM5150】が存在しません ----"
#				@ errorflg ++
#				@ examerrorflg ++
#			else
#				set size = `wc -cl $mastindir/UNIVCDTRANS_ORD`
#				set datetime = `ls -l $mastindir/UNIVCDTRANS_ORD`
#				echo "　大学コード変換順引きマスタ　データ　　[UNIVCDTRANS_ORD]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
#				echo "　大学コード変換順引きマスタ　データ　　[UNIVCDTRANS_ORD]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
#			endif
#			if (! -f $mastindir/UNIVSTEMMA) then
#				echo "  ---- 大学系統マスタ　[UNIVSTEMMA]　【RZRM5170】が存在しません ----"
#				@ errorflg ++
#				@ examerrorflg ++
#			else
#				set size = `wc -cl $mastindir/UNIVSTEMMA`
#				set datetime = `ls -l $mastindir/UNIVSTEMMA`
#				echo "　大学系統マスタ　データ　　　　　　[UNIVSTEMMA]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
#				echo "　大学系統マスタ　データ　　　　　　[UNIVSTEMMA]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
#			endif

			## 模試用の各マスタ
			if (! -f $mastindir/UNIVMASTER_BASIC) then
				echo "  ---- 大学基本情報　[UNIVMASTER_BASIC]　【RZRG5110】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/UNIVMASTER_BASIC`
				set datetime = `ls -l $mastindir/UNIVMASTER_BASIC`
				echo "　大学基本情報　データ  [UNIVMASTER_BASIC]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学基本情報　データ  [UNIVMASTER_BASIC]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/UNIVMASTER_CHOICESCHOOL) then
				echo "  ---- 大学マスタ模試用志望校　[UNIVMASTER_CHOICESCHOOL]　【RZRG5120】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/UNIVMASTER_CHOICESCHOOL`
				set datetime = `ls -l $mastindir/UNIVMASTER_CHOICESCHOOL`
				echo "　大学マスタ模試用志望校　データ  [UNIVMASTER_CHOICESCHOOL]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学マスタ模試用志望校　データ  [UNIVMASTER_CHOICESCHOOL]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/UNIVMASTER_COURSE) then
				echo "  ---- 大学マスタ模試用教科　[UNIVMASTER_COURSE]　【RZRG5130】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/UNIVMASTER_COURSE`
				set datetime = `ls -l $mastindir/UNIVMASTER_COURSE`
				echo "　大学マスタ模試用教科　データ  [UNIVMASTER_COURSE]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学マスタ模試用教科　データ  [UNIVMASTER_COURSE]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/UNIVMASTER_SUBJECT) then
				echo "  ---- 大学マスタ模試用科目　[UNIVMASTER_SUBJECT]　【RZRG5140】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/UNIVMASTER_SUBJECT`
				set datetime = `ls -l $mastindir/UNIVMASTER_SUBJECT`
				echo "　大学マスタ模試用科目　データ  [UNIVMASTER_SUBJECT]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学マスタ模試用科目　データ  [UNIVMASTER_SUBJECT]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/UNIVMASTER_SELECTGROUP) then
				echo "  ---- 大学マスタ模試用選択グループ　[UNIVMASTER_SELECTGROUP]　【RZRG5160】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/UNIVMASTER_SELECTGROUP`
				set datetime = `ls -l $mastindir/UNIVMASTER_SELECTGROUP`
				echo "　大学マスタ模試用選択グループ　データ  [UNIVMASTER_SELECTGROUP]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学マスタ模試用選択グループ　データ  [UNIVMASTER_SELECTGROUP]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/UNIVMASTER_SELECTGPCOURSE) then
				echo "  ---- 大学マスタ模試用選択グループ教科　[UNIVMASTER_SELECTGPCOURSE]　【RZRG5190】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/UNIVMASTER_SELECTGPCOURSE`
				set datetime = `ls -l $mastindir/UNIVMASTER_SELECTGPCOURSE`
				echo "　大学マスタ模試用選択グループ教科　データ  [UNIVMASTER_SELECTGPCOURSE]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学マスタ模試用選択グループ教科　データ  [UNIVMASTER_SELECTGPCOURSE]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			
			if (! -f $mastindir/EXAMSCHEDULEDETAIL) then
				echo "  ---- 入試日程詳細　[EXAMSCHEDULEDETAIL]　【RZRG5180】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/EXAMSCHEDULEDETAIL`
				set datetime = `ls -l $mastindir/EXAMSCHEDULEDETAIL`
				echo "　入試日程詳細　データ　　　　　　[EXAMSCHEDULEDETAIL]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　入試日程詳細　データ　　　　　　[EXAMSCHEDULEDETAIL]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/UNIVCDTRANS_ORD) then
#				echo "  ---- 大学コード変換順引きマスタ　[UNIVCDTRANS_ORD]　【RZRG5150】が存在しません ----"
#				@ errorflg ++
#				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/UNIVCDTRANS_ORD`
				set datetime = `ls -l $mastindir/UNIVCDTRANS_ORD`
				echo "　大学コード変換順引きマスタ　データ　　[UNIVCDTRANS_ORD]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学コード変換順引きマスタ　データ　　[UNIVCDTRANS_ORD]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/UNIVSTEMMA) then
				echo "  ---- 大学系統マスタ　[UNIVSTEMMA]　【RZRG5170】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/UNIVSTEMMA`
				set datetime = `ls -l $mastindir/UNIVSTEMMA`
				echo "　大学系統マスタ　データ　　　　　　[UNIVSTEMMA]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学系統マスタ　データ　　　　　　[UNIVSTEMMA]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/UNIVMASTER_INFO) then
				echo "  ---- 大学情報マスタ　[UNIVMASTER_INFO]　【RZRG5410】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/UNIVMASTER_INFO`
				set datetime = `ls -l $mastindir/UNIVMASTER_INFO`
				echo "　大学情報マスタ　データ　　　　　　[UNIVMASTER_INFO]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学情報マスタ　データ　　　　　　[UNIVMASTER_INFO]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			
			## 情報誌関連
			if (! -f $mastindir/JH01_KAMOKU1) then
				echo "  ---- 情報誌科目（共通）　[JH01_KAMOKU1]　【RZRG5320】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/JH01_KAMOKU1`
				set datetime = `ls -l $mastindir/JH01_KAMOKU1`
				echo "　情報誌科目（共通）　データ　　　　　　[JH01_KAMOKU1]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　情報誌科目（共通）　データ　　　　　　[JH01_KAMOKU1]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/JH01_KAMOKU2) then
				echo "  ---- 情報誌科目（センター）　[JH01_KAMOKU2]　【RZRG5330】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/JH01_KAMOKU2`
				set datetime = `ls -l $mastindir/JH01_KAMOKU2`
				echo "　情報誌科目（センター）　データ　　　　　　[JH01_KAMOKU2]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　情報誌科目（センター）　データ　　　　　　[JH01_KAMOKU2]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/JH01_KAMOKU3) then
				echo "  ---- 情報誌科目（二次）　[JH01_KAMOKU3]　【RZRG5340】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/JH01_KAMOKU3`
				set datetime = `ls -l $mastindir/JH01_KAMOKU3`
				echo "　情報誌科目（二次）　データ　　　　　　[JH01_KAMOKU3]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　情報誌科目（二次）　データ　　　　　　[JH01_KAMOKU3]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/JH01_KAMOKU4) then
				echo "  ---- 情報誌科目（科目名）　[JH01_KAMOKU4]　【RZRG5570】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/JH01_KAMOKU4`
				set datetime = `ls -l $mastindir/JH01_KAMOKU4`
				echo "　情報誌科目（科目名）　データ　　　　　　[JH01_KAMOKU4]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　情報誌科目（科目名）　データ　　　　　　[JH01_KAMOKU4]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			
			## 公表用の各マスタ
			if (! -f $mastindir/PUB_UNIVMASTER_BASIC) then
				echo "  ---- 大学基本情報　[PUB_UNIVMASTER_BASIC]　【RZRG5220】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/PUB_UNIVMASTER_BASIC`
				set datetime = `ls -l $mastindir/PUB_UNIVMASTER_BASIC`
				echo "　大学基本情報　データ  [PUB_UNIVMASTER_BASIC]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学基本情報　データ  [PUB_UNIVMASTER_BASIC]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/PUB_UNIVMASTER_CHOICESCHOOL) then
				echo "  ---- 大学マスタ模試用志望校　[PUB_UNIVMASTER_CHOICESCHOOL]　【RZRG5230】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/PUB_UNIVMASTER_CHOICESCHOOL`
				set datetime = `ls -l $mastindir/PUB_UNIVMASTER_CHOICESCHOOL`
				echo "　大学マスタ模試用志望校　データ  [PUB_UNIVMASTER_CHOICESCHOOL]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学マスタ模試用志望校　データ  [PUB_UNIVMASTER_CHOICESCHOOL]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/PUB_UNIVMASTER_COURSE) then
				echo "  ---- 大学マスタ模試用教科　[PUB_UNIVMASTER_COURSE]　【RZRG5240】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/PUB_UNIVMASTER_COURSE`
				set datetime = `ls -l $mastindir/PUB_UNIVMASTER_COURSE`
				echo "　大学マスタ模試用教科　データ  [PUB_UNIVMASTER_COURSE]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学マスタ模試用教科　データ  [PUB_UNIVMASTER_COURSE]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/PUB_UNIVMASTER_SUBJECT) then
				echo "  ---- 大学マスタ模試用科目　[PUB_UNIVMASTER_SUBJECT]　【RZRG5250】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/PUB_UNIVMASTER_SUBJECT`
				set datetime = `ls -l $mastindir/PUB_UNIVMASTER_SUBJECT`
				echo "　大学マスタ模試用科目　データ  [PUB_UNIVMASTER_SUBJECT]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学マスタ模試用科目　データ  [PUB_UNIVMASTER_SUBJECT]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/PUB_UNIVMASTER_SELECTGROUP) then
				echo "  ---- 大学マスタ模試用選択グループ　[PUB_UNIVMASTER_SELECTGROUP]　【RZRG5260】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/PUB_UNIVMASTER_SELECTGROUP`
				set datetime = `ls -l $mastindir/PUB_UNIVMASTER_SELECTGROUP`
				echo "　大学マスタ模試用選択グループ　データ  [PUB_UNIVMASTER_SELECTGROUP]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学マスタ模試用選択グループ　データ  [PUB_UNIVMASTER_SELECTGROUP]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			if (! -f $mastindir/PUB_UNIVMASTER_SELECTGPCOURSE) then
				echo "  ---- 大学マスタ模試用選択グループ教科　[PUB_UNIVMASTER_SELECTGPCOURSE]　【RZRG5270】が存在しません ----"
				@ errorflg ++
				@ examerrorflg ++
			else
				set size = `wc -cl $mastindir/PUB_UNIVMASTER_SELECTGPCOURSE`
				set datetime = `ls -l $mastindir/PUB_UNIVMASTER_SELECTGPCOURSE`
				echo "　大学マスタ模試用選択グループ教科　データ  [PUB_UNIVMASTER_SELECTGPCOURSE]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
				echo "　大学マスタ模試用選択グループ教科　データ  [PUB_UNIVMASTER_SELECTGPCOURSE]  のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
			
		endif
		
	endif


	if ($#argv == 0) then

		###-- 模試受験者総数(高校)チェック
		if (! -f $indir/EXAMTAKENUM_S) then
			if ($getflg != -2) then
				echo "  ---- 模試受験者総数(高校)データ　[EXAMTAKENUM_S]　【RZRG1380】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/EXAMTAKENUM_S`
			set datetime = `ls -l $indir/EXAMTAKENUM_S`
			echo "　模試受験者総数(高校)　データ　　　[EXAMTAKENUM_S] のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　模試受験者総数(高校)　データ　　　[EXAMTAKENUM_S] のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 模試受験者総数(クラス)チェック
		if (! -f $indir/EXAMTAKENUM_C) then
			if (($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)&&($getflg != -2)) then
				echo "  ---- 模試受験者総数(クラス)データ　[EXAMTAKENUM_C]　【RZRG1480】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/EXAMTAKENUM_C`
			set datetime = `ls -l $indir/EXAMTAKENUM_C`
			echo "　模試受験者総数(クラス)　データ　　[EXAMTAKENUM_C] のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　模試受験者総数(クラス)　データ　　[EXAMTAKENUM_C] のファイルを  [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 科目別成績(高校)チェック
		if (! -f $indir/SUBRECORD_S) then
			if ($getflg != -2) then
				echo "  ---- 科目別成績(高校)データ　[SUBRECORD_S]　【RZRG1310】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/SUBRECORD_S`
			set datetime = `ls -l $indir/SUBRECORD_S`
			echo "　科目別成績(高校)　データ　　　　　　[SUBRECORD_S]　　のファイルを　[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　科目別成績(高校)　データ　　　　　　[SUBRECORD_S]　　のファイルを　[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif
		
		###-- 科目別成績(クラス)チェック
		if (! -f $indir/SUBRECORD_C) then
			if (($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)&&($getflg != -2)) then
				echo "  ---- 科目別成績(クラス)データ　[SUBRECORD_C]　【RZRG1410】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/SUBRECORD_C`
			set datetime = `ls -l $indir/SUBRECORD_C`
			echo "　科目別成績(クラス)　データ　　　　　[SUBRECORD_C]　　のファイルを　[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　科目別成績(クラス)　データ　　　　　[SUBRECORD_C]　　のファイルを　[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 設問別成績(高校)チェック
		if (! -f $indir/QRECORD_S) then
			if (($exam2 != 38)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)&&($getflg != -2)) then
				echo "  ---- 設問別成績(高校)データ　[QRECORD_S]　【RZRG1330】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/QRECORD_S`
			set datetime = `ls -l $indir/QRECORD_S`
			echo "　設問別成績(高校)　データ　　　　　　[QRECORD_S]　　　　のファイルを　[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　設問別成績(高校)　データ　　　　　　[QRECORD_S]　　　　のファイルを　[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 設問別成績(クラス)チェック
		if (! -f $indir/QRECORD_C) then
			if (($exam2 != 38)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)&&($getflg != -2)) then
				echo "  ---- 設問別成績(クラス)データ　[QRECORD_C]　【RZRG1430】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/QRECORD_C`
			set datetime = `ls -l $indir/QRECORD_C`
			echo "　設問別成績(クラス)　データ　　　　　[QRECORD_C]　　　　のファイルを　[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　設問別成績(クラス)　データ　　　　　[QRECORD_C]　　　　のファイルを　[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 設問別成績層別成績チェック
		if (! -f $indir/QRECORDZONE) then
			if (($exam2 != 38)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)&&($getflg != -2)) then
				echo "  ---- 設問別成績層別成績データ　[QRECORDZONE]　【RZRG1340】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/QRECORDZONE`
			set datetime = `ls -l $indir/QRECORDZONE`
			echo "　設問別成績層別成績　データ　　　　[QRECORDZONE]　　のファイルを　[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　設問別成績層別成績　データ　　　　[QRECORDZONE]　　のファイルを　[$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 科目分布別成績(高校)チェック
		if (! -f $indir/SUBDISTRECORD_S) then
			if ($getflg != -2) then
				echo "  ---- 科目分布別成績(高校)データ　[SUBDISTRECORD_S]　【RZRG1320】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/SUBDISTRECORD_S`
			set datetime = `ls -l $indir/SUBDISTRECORD_S`
			echo "　科目分布別成績(高校)　データ　　　[SUBDISTRECORD_S] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　科目分布別成績(高校)　データ　　　[SUBDISTRECORD_S] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 科目分布別成績(クラス)チェック
		if (! -f $indir/SUBDISTRECORD_C) then
			if (($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)&&($getflg != -2)) then
				echo "  ---- 科目分布別成績(クラス)データ　[SUBDISTRECORD_C]　【RZRG1420】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/SUBDISTRECORD_C`
			set datetime = `ls -l $indir/SUBDISTRECORD_C`
			echo "　科目分布別成績(クラス)　データ　　[SUBDISTRECORD_C] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　科目分布別成績(クラス)　データ　　[SUBDISTRECORD_C] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 科目別成績層別成績(高校)チェック
		if (! -f $indir/SUBDEVZONERECORD_S) then
			if (($getflg != -2)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "  ---- 科目別成績層別成績(高校)データ　[SUBDEVZONERECORD_S]　【RZRG1300】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/SUBDEVZONERECORD_S`
			set datetime = `ls -l $indir/SUBDEVZONERECORD_S`
			echo "　科目別成績層別成績(高校)　データ　　　[SUBDEVZONERECORD_S] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　科目別成績層別成績(高校)　データ　　　[SUBDEVZONERECORD_S] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 志望大学評価別人数(高校)チェック
		if (! -f $indir/RATINGNUMBER_S) then
#			if (($getflg != -2)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)) then
			if (($getflg != -2)&&($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "  ---- 志望大学評価別人数(高校)データ　[RATINGNUMBER_S]　【RZRG1350】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/RATINGNUMBER_S`
			set datetime = `ls -l $indir/RATINGNUMBER_S`
			echo "　志望大学評価別人数(高校)　データ[RATINGNUMBER_S] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　志望大学評価別人数(高校)　データ[RATINGNUMBER_S] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 志望大学評価別人数(クラス)チェック
		if (! -f $indir/RATINGNUMBER_C) then
#			if (($getflg != -2)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)) then
			if (($getflg != -2)&&($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "  ---- 志望大学評価別人数(クラス)データ [RATINGNUMBER_C] 【RZRG1450】が存在しません ----" 
				@ errorflg ++
				@ examerrorflg ++
			endif
		else
			set size = `wc -cl $indir/RATINGNUMBER_C`
			set datetime = `ls -l $indir/RATINGNUMBER_C`
			echo "　志望大学評価別人数(クラス)　データ[RATINGNUMBER_C] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　志望大学評価別人数(クラス)　データ[RATINGNUMBER_C] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- マーク模試高校設問別正答率チェック
		if (! -f $indir/MARKQSCORERATE) then
			if ($getflg != -2) then
				if (($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66)) then
					echo "  ---- マーク模試高校設問別正答率データ [MARKQSCORERATE] 【RZRG1360】が存在しません ----" 
					@ errorflg ++
					@ examerrorflg ++
				endif
			endif
		else
			set size = `wc -cl $indir/MARKQSCORERATE`
			set datetime = `ls -l $indir/MARKQSCORERATE`
			echo "　マーク模試高校設問別正答率　データ[MARKQSCORERATE] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　マーク模試高校設問別正答率　データ[MARKQSCORERATE] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- マーク模試設問別成績層別正答率（高校）チェック
		if (! -f $indir/MARKQDEVZONEANSRATE_S) then
			if ($getflg != -2) then
				if (($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66)) then
					echo "  ---- マーク模試設問別成績層別正答率（高校）データ [MARKQDEVZONEANSRATE_S] 【RZRG1390】が存在しません ----" 
					@ errorflg ++
					@ examerrorflg ++
				endif
			endif
		else
			set size = `wc -cl $indir/MARKQDEVZONEANSRATE_S`
			set datetime = `ls -l $indir/MARKQDEVZONEANSRATE_S`
			echo "　マーク模試設問別成績層別正答率（高校）　データ[MARKQDEVZONEANSRATE_S] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　マーク模試設問別成績層別正答率（高校）　データ[MARKQDEVZONEANSRATE_S] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif

		###-- 個人成績表データチェック
		if (-f $indir/INDIVIDUALRECORD) then
# 事後換算で必ず自宅回答分が存在するとは限らない？ため、下記をコメントアウト (20061013)
#			if (($examtypstr == zigo)&&(! -f $indir/INDIVIDUALRECORD-old)) then
#				echo "  ---- 個人成績表(自宅回答)データ [INDIVIDUALRECORD] 【RZRG2110】が存在しません ----" 
#				@ errorflg ++
#				@ examerrorflg ++
#			else if (($examtypstr == zigo)&&(-f $indir/INDIVIDUALRECORD-old)) then
			if (($examtypstr == zigo)&&(-f $indir/INDIVIDUALRECORD-old)) then
				set size = `wc -cl $indir/INDIVIDUALRECORD-old`
				set datetime = `ls -l $indir/INDIVIDUALRECORD-old`
				echo "　個人成績表データ　　　　　[INDIVIDUALRECORD] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
				echo "　個人成績表データ　　　　　[INDIVIDUALRECORD] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
			endif
	
			set size = `wc -cl $indir/INDIVIDUALRECORD`
			set datetime = `ls -l $indir/INDIVIDUALRECORD`
			echo "　個人成績表データ　　　　　　[INDIVIDUALRECORD] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました"
			echo "　個人成績表データ　　　　　　[INDIVIDUALRECORD] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		else
			echo "  ---- 個人成績表データ [INDIVIDUALRECORD] 【RZRG2110】が存在しません ----" 
			echo "  ---- 個人成績表データ [INDIVIDUALRECORD] 【RZRG2110】が存在しません ----" >> $fchklog
			@ errorflg ++
			@ examerrorflg ++

			#--- 受験学力測定の１～９タームまでは個表のみないため。エラーでも次のバッチへ進めるようにするため (20070406)
			if (($exam2 == 37)||($exam2 == 69)||($exam2 == 79)||($exam2 == 87)||($exam2 == 88)) then
				set errorflg = `expr $errorflg + 100`
			endif
		endif
		
		###-- 記述系模試小問別成績データ（高校）チェック
		if (! -f $indir/SHOQUESTIONRECORD_S) then
			if ($getflg != -2) then
				if (($exam2 == 05)||($exam2 == 06)||($exam2 == 07)||($exam2 == 61)||($exam2 == 62)||($exam2 == 63)||($exam2 == 65)||($exam2 == 71)||($exam2 == 72)||($exam2 == 73)||($exam2 == 74)||($exam2 == 10)||($exam2 == 76)||($exam2 == 77)) then
					echo "  ---- 記述系模試小問別成績データ（高校）データ　[SHOQUESTIONRECORD_S]　【RZRG5610】が存在しません ----" 
					@ errorflg ++
					@ examerrorflg ++
				endif
			endif
		else
			set size = `wc -cl $indir/SHOQUESTIONRECORD_S`
			set datetime = `ls -l $indir/SHOQUESTIONRECORD_S`
			echo "　記述系模試小問別成績データ（高校）　データ　[SHOQUESTIONRECORD_S]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" 
			echo "　記述系模試小問別成績データ（高校）　データ　[SHOQUESTIONRECORD_S]　のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte　で確認しました" >> $fchklog
		endif
	endif



	####-- ダウンロードファイルチェック
	if ($DLfg == 1) then
		cd /keinavi/HULFT
		
		#-- 模試成績データDLファイルが1つでもあるか(模試は区別しない)確認する
		(ls -rt INDIVIDUALRECORD_FD* > counttemp.log) >& /dev/null
		set FILElist = `cat counttemp.log`
		\rm -f counttemp.log
		
		#-- 調査回答ファイル1が1つでもあるか(模試は区別しない)確認する
		(ls -rt TYOUSA_ONE* > counttemp1.log) >& /dev/null
		set FILElist1 = `cat counttemp1.log`
		\rm -f counttemp1.log

		#-- 調査回答ファイル2が1つでもあるか(模試は区別しない)確認する
		(ls -rt TYOUSA_TWO* > counttemp2.log) >& /dev/null
		set FILElist2 = `cat counttemp2.log`
		\rm -f counttemp2.log

		set DLfg = 2
	endif

	if ($#FILElist == 0) then
		#-- 登録対象模試がＯＰ模試以外で1つもＤＬファイルがなかったらエラー
		if (($exam2 != 12)&&($exam2 != 13)&&($exam2 != 15)&&($exam2 != 16)&&($exam2 != 18)&&($exam2 != 19)&&($exam2 != 21)&&($exam2 != 22)&&($exam2 != 24)&&($exam2 != 25)&&($exam2 != 26)&&($exam2 != 27)&&($exam2 != 31)&&($exam2 != 32)&&($exam2 != 41)&&($exam2 != 42)&&($exam2 != 43)) then
		        echo "  ---- 模試成績データダウンロードファイル[INDIVIDUALRECORD_FD] 【RZRG2210】が存在しません ----"
		        echo "  ---- 模試成績データダウンロードファイル[INDIVIDUALRECORD_FD] 【RZRG2210】が存在しません ----" >> $fchklog
			@ errorflg ++
			@ examerrorflg ++

			#-- 事後換算、都立換算、得点修正のみDLファイルが来なかったときのために警告とし、フラグをセット
			if (($getflg == -2)||($getflg == -1)) then
				set errorflg = `expr $errorflg + 100`
			endif
		endif
	endif

	if ($#FILElist != 0) then
		cd /keinavi/HULFT

		#--- 模試区分00(模試コードが66)のときは年度を-１戻す
		if ((($getflg != -1)&&($getflg != -2))&&($#argv == 0)) then
			if ($#ecdanddiv != 0) then
				if ($examdiv == 00) then
					@ year2 --
				endif
			endif
		endif
		#-- DLファイルの存在時、定義ファイル内の模試と一致するか確認する
		set dataflg = 1
		set loopscount = 1
		while ($loopscount <= $#FILElist)
		
		        #----対象ファイル名を取得
			set targfile =  $FILElist[$loopscount]
		        #----ファイルから年度と模試コードを取得
			set DLyear=`awk -F, NR=="2"'{print $2}' $targfile`
			set DLexam=`awk -F, NR=="2"'{print $3}' $targfile`

			#----センターリサーチ用に定義ファイルと年度あわせるため1年引くする
			if ($DLexam == "38") then
				@ DLyear --
			endif

			if (($DLyear == $year2)&&($DLexam == $exam2)) then
				set dataflg = 2
				if ($#argv != 0) then
					echo "${year2}${exam2}" >> /keinavi/JOBNET/ExamDLup
				endif

				set size = `wc -cl $targfile`
				set datetime = `ls -l $targfile`
				echo "　模試成績データダウンロードデータ　[INDIVIDUALRECORD_FD] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte で確認しました" 
				echo "　模試成績データダウンロードデータ　[INDIVIDUALRECORD_FD] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 [$size[1]]件 [$size[2]]byte で確認しました" >> $fchklog
				break
			endif
			@ loopscount ++
		end
		if ($dataflg != 2) then
			#-- 登録対象模試がＯＰ模試以外で1つもＤＬファイルがなかったらエラー
			if (($exam2 != 12)&&($exam2 != 13)&&($exam2 != 15)&&($exam2 != 16)&&($exam2 != 18)&&($exam2 != 19)&&($exam2 != 21)&&($exam2 != 22)&&($exam2 != 24)&&($exam2 != 25)&&($exam2 != 26)&&($exam2 != 27)&&($exam2 != 31)&&($exam2 != 32)&&($exam2 != 41)&&($exam2 != 42)&&($exam2 != 43)) then
				echo "  ---- 模試成績データダウンロードファイル　[INDIVIDUALRECORD_FD]　【RZRG2210】が存在しません ----"
				echo "  ---- 模試成績データダウンロードファイル　[INDIVIDUALRECORD_FD]　【RZRG2210】が存在しません ----" >> $fchklog
				@ errorflg ++
				@ examerrorflg ++
			
				#-- 事後換算、都立換算、得点修正のみDLファイルが来なかったときのために警告とし、フラグをセット
				if (($getflg == -2)||($getflg == -1)) then
					set errorflg = `expr $errorflg + 100`
				endif
			endif
		endif
	endif
	

	set tyousaflg = 0	
	if (($#FILElist1 == 0)&&(($exam2 == 37)||($exam2 == 69)||($exam2 == 79)||($exam2 == 87)||($exam2 == 88))) then
		#-- 登録対象模試が受験学力測定テストで1つも調査回答ファイルがなかったらエラー
		echo " ---- 調査回答１データファイル[TYOUSA_ONE] 【RZRG2310】が存在しません ----"
		echo " ---- 調査回答１データファイル[TYOUSA_ONE] 【RZRG2310】が存在しません ----" >> $fchklog
		@ errorflg ++
		@ examerrorflg ++
	endif

	if ($#FILElist1 != 0) then
		cd /keinavi/HULFT
		#-- DLファイルの存在時、定義ファイル内の模試と一致するか確認する
		set dataflg = 1
		set loopscount1 = 1
		while ($loopscount1 <= $#FILElist1)
		
		        #----対象ファイル名を取得
			set targfile =  $FILElist1[$loopscount1]
		        #----ファイルから年度と模試コードを取得
			set DLyear=`awk -F, NR=="2"'{print $2}' $targfile`
			set DLexam=`awk -F, NR=="2"'{print $3}' $targfile`

			if (($DLyear == $year2)&&($DLexam == $exam2)) then
				set dataflg = 2
				
				set tyousaflg = 1

				set datetime = `ls -l $targfile`
				echo "　調査回答１データ　[TYOUSA_ONE] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 で確認しました" 
				echo "　調査回答１データ　[TYOUSA_ONE] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 で確認しました" >> $fchklog
				break
			endif
			@ loopscount1 ++
		end
		if ($dataflg != 2) then
			#-- 登録対象模試が受験学力測定テストなのにでＤＬファイルがなかったらエラー
			if (($exam2 == 37)||($exam2 == 69)||($exam2 == 79)||($exam2 == 87)||($exam2 == 88)) then
				echo "  ---- 調査回答１データファイル[TYOUSA_ONE] 【RZRG2310】が存在しません ----"
				echo "  ---- 調査回答１データファイル[TYOUSA_ONE] 【RZRG2310】が存在しません ----" >> $fchklog
				@ errorflg ++
				@ examerrorflg ++
			endif
		endif
	endif

	if (($#FILElist2 == 0)&&(($exam2 == 37)||($exam2 == 69)||($exam2 == 79)||($exam2 == 87)||($exam2 == 88))) then
		#-- 登録対象模試が受験学力測定テストで1つも調査回答ファイルがなかったらエラー
		echo " ---- 調査回答２データファイル[TYOUSA_TWO] 【RZRG2410】が存在しません ----"
		echo " ---- 調査回答２データファイル[TYOUSA_TWO] 【RZRG2410】が存在しません ----" >> $fchklog
		@ errorflg ++
		@ examerrorflg ++
	endif

	if ($#FILElist2 != 0) then
		cd /keinavi/HULFT
		#-- DLファイルの存在時、定義ファイル内の模試と一致するか確認する
		set dataflg = 1
		set loopscount1 = 1
		while ($loopscount1 <= $#FILElist2)
		
		        #----対象ファイル名を取得
			set targfile =  $FILElist2[$loopscount1]
		        #----ファイルから年度と模試コードを取得
			set DLyear=`awk -F, NR=="2"'{print $2}' $targfile`
			set DLexam=`awk -F, NR=="2"'{print $3}' $targfile`

			if (($DLyear == $year2)&&($DLexam == $exam2)) then
				set dataflg = 2

				if ($tyousaflg == 0) then
					set tyousaflg = 2
				else if ($tyousaflg == 1) then
					set tyousaflg = 3
				endif

				set datetime = `ls -l $targfile`
				echo "　調査回答２データ　[TYOUSA_TWO] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 で確認しました" 
				echo "　調査回答２データ　[TYOUSA_TWO] のファイルを [$datetime[6]$datetime[7] $datetime[8]]受信 で確認しました" >> $fchklog
				break
			endif
			@ loopscount1 ++
		end
		if ($dataflg != 2) then
			#-- 登録対象模試が受験学力測定テストなのにでＤＬファイルがなかったらエラー
			if (($exam2 == 37)||($exam2 == 69)||($exam2 == 79)||($exam2 == 87)||($exam2 == 88)) then
				echo "  ---- 調査回答２データファイル[TYOUSA_TWO] 【RZRG2410】が存在しません ----"
				echo "  ---- 調査回答２データファイル[TYOUSA_TWO] 【RZRG2410】が存在しません ----" >> $fchklog
				@ errorflg ++
				@ examerrorflg ++
			endif
		endif
	endif

	##-- 調査回答用定義ファイル作成
	if ($tyousaflg == 1) then
		echo "${year2}${exam2},1" >> /keinavi/JOBNET/TYOUSAup
	else if ($tyousaflg == 2) then
		echo "${year2}${exam2},2" >> /keinavi/JOBNET/TYOUSAup
	else if ($tyousaflg == 3) then
		echo "${year2}${exam2},3" >> /keinavi/JOBNET/TYOUSAup
	endif


	if ($examerrorflg != 0) then
		echo "---- 模試 $failname の各データファイル有無確認に問題が発生しました ----"
		echo "---- 模試 $failname の各データファイル有無確認に問題が発生しました ----" >> $fchklog
	else
		echo " 模試 $failname の各データファイル有無確認が正常に終了しました ---"
		echo " 模試 $failname の各データファイル有無確認が正常に終了しました ---" >> $fchklog
	endif


	##--- 統計集PDF追加対応データのチェック用定義ファイル作成 2008.04.21追加
	if ($getflg == 0) then
		set DLtargetexam = `grep ^${exam2} ${DLexamlist}`
		if ($DLtargetexam != "") then
			echo "${year2},${exam2}" >> /keinavi/JOBNET/Fcheck/DLtargetexams
		endif
	endif


	##--- 統合高校過年度用データのチェック用定義作成(レギュラーデータ時のみ) 2007.07.30追加
	if ($getflg == 0) then
		if (($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
			#--- 1年前を定義
			@ year2 --
			echo "${year2}${exam2}" >> /keinavi/JOBNET/Fcheck/fusionexam
			#--- 2年前を定義
			@ year2 --
			echo "${year2}${exam2}" >> /keinavi/JOBNET/Fcheck/fusionexam

		endif
	endif


	@ count ++
end


#--- エラーが一つでもあったら異常終了
if ($errorflg != 0) then
	#-- 異常終了でも次のバッチへ進めるようにする特殊例用
	if ($errorflg >= 100) then
		touch /keinavi/JOBNET/Fcheck/checkEnd.txt
		chmod 777 /keinavi/JOBNET/Fcheck/checkEnd.txt
	
		echo " "
		echo " ******** 特例処理という理由によりデータファイルが存在しない場合は、そのままスキップして次に進んでください ******** "
		echo " ******** 特例処理という理由によりデータファイルが存在しない場合は、そのままスキップして次に進んでください ******** " >> $fchklog
	endif
		
	exit 30
endif

#--- 正常終了時はデータ登録バッチの起動が可能なためのファイル作成
touch /keinavi/JOBNET/Fcheck/checkEnd.txt
chmod 777 /keinavi/JOBNET/Fcheck/checkEnd.txt

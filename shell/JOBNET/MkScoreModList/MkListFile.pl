#!/usr/bin/perl

### 得点修正者リスト作成スクリプト
### 個表データファイル名をパラメータ渡しする

$ORG_FILE = "@ARGV[0]";
$OUT_FILE = "/home/navikdc/INDIVI-list.csv";

if(! open(IN, "$ORG_FILE")){
        print "FILE OPEN ERROR!\n";
        exit;
}
if(! open(OUT, ">$OUT_FILE") ){
        close(IN);
        exit;
}

while(<IN>){
        chomp($_);
        $exam  = substr($_,   4, 2);
        $subrec01 = substr($_,   6, 6);
	$subrec02 = substr($_,  12, 5);     #HOMESCHOOL
        $subrec03 = substr($_,  17, 5);     #BundleCD
        $subrec04 = substr($_,  22, 2);     #GRADE
        $subrec05 = substr($_,  24, 2);     #class
        $subrec06 = substr($_,  26, 5);     #classNumber
        $subrec07 = substr($_,  33, 14);    #kana-name
        $subrec08 = substr($_,  47, 20);    #name
        $subrec09 = substr($_,  107, 1);    #PrivercyFLG
        $outSTR = "$exam" . ','.
        	$subrec01 . ','.
                $subrec02 . ','.
                $subrec03 . ','.
                $subrec04 . ','.
                $subrec05 . ','.
                $subrec06 . ','.
                $subrec07 . ','.
		$subrec08 . ','.
                $subrec09;
        print OUT "$outSTR\n";
}
close(OUT);
close(IN);

$newname = '/home/navikdc/' . @ARGV[1] . 'TokuSyu.csv';
$ret = rename($OUT_FILE, $newname);
if ($ret == 0) {
	printf "ファイル名変更に失敗(%s => %s)\n", $OUT_FILE, $newname;
	printf "エラー(%d:%s)\n", $!, $!;
	exit 1;
}

print "作成完了!\n";
exit;

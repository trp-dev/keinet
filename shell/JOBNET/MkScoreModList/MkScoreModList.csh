#! /bin/csh

if ($#argv > 0 ) then
	set list = `ls $1`
else
	set list = `cat /keinavi/HULFT/NewEXAM/selectioncd/grepexam`
endif

set scripath = "/keinavi/JOBNET/MkScoreModList"
cd  $scripath
set count = 1
while ($count <= $#list)
	
	if ($#argv > 0 ) then 
		#--- ファイルの行頭と行末から対象模試を取得
		set string1 = `head -1  $list[$count] | awk '{printf "%-.6s\n",$0}'`
		set string2 = `tail -1 $list[$count]  | awk '{printf "%-.6s\n",$0}'`

		#--- リスト作成対象ファイルのセット
		set changefile = $1
	else
#		set string1 = `echo $list[$count] | awk '{printf "%-.6s\n",$0}'`
		set string1 = `echo $list[$count] | sed -e s'/[a-z]*//'g`
		set string2 = $string1

		#--- リスト作成対象ファイルのセット
		set changefile = "/keinavi/HULFT/NewEXAM/$list[$count]/INDIVIDUALRECORD"
		if (! -f $changefile) then
			echo "----ERROR：得点修正者リスト作成対象ファイル($changefile) が存在しません。
			@ count ++
			continue
		endif
	endif

	echo " $changefile データより得点修正者リスト作成開始 "
	if ($string1 == $string2) then

		#--- 得点修正の始めの模試(#1記述)時に昨年の得点修正者リストファイルを削除
		set targetyear = `date +%Y`
		if ($string1 == ${targetyear}05) then
			\rm -f /home/navikdc/*.csv
		endif

		$scripath/MkListFile.pl $changefile $string1
	else
		set year1 = `echo $string1 | awk '{printf "%-.4s\n",$0}'`
		set year2 = `echo $string2 | awk '{printf "%-.4s\n",$0}'`
		set exam2 = `expr $string2 : ".*\(..\)"`
#		if ($year1 == $year2) then
#			if ((($string1 == ${year1}61)&&($string2 == ${year2}71)) || (($string1 == ${year1}62)&&($string2 == ${year2}72)) || (($string1 == ${year1}63)&&($string2 == ${year2}73))) then
#				#--- 高1,高2模試データ識別 
#				set dirname = ${string1}_${exam2}
#				$scripath/MkListFile.pl $changefile $dirname
#				@ count ++
#				continue
#			endif
#		endif
		echo " --------行頭と行末が不一致です-------" 
	endif

	@ count ++
end
 

#!/bin/sh

#環境変数設定
export JOBNAME=JNRZ0550-KO
export CSVFILE=KO_HS3_UNIV_NAME
export SRCDIR=/keinavi/HULFT/WorkDir
export WORKDIR=/keinavi/JOBNET/$JOBNAME

export flist=`ls -r $SRCDIR/KO_HS3_UNIV_NAME*`

mv ${SRCDIR}/$CSVFILE ${SRCDIR}/${CSVFILE}-tmp

for test in $flist; do
	if [ $test = ${SRCDIR}/$CSVFILE ]
	then
		mv ${SRCDIR}/${CSVFILE}-tmp ${SRCDIR}/$CSVFILE
	else
		mv $test ${SRCDIR}/$CSVFILE
	fi

	/keinavi/JOBNET/JNRZ0550-KO/JNRZ0550-KO1.sh
done


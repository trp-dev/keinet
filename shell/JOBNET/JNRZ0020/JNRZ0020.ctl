------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 模試受験者総数(県）
--  テーブル名      : EXAMTAKENUM_P
--  データファイル名: EXAMTAKENUM_P
--  機能            : 模試受験者総数(県）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE EXAMTAKENUM_P_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   EXAMYEAR                POSITION(01:04)      CHAR,
   EXAMCD                  POSITION(05:06)      CHAR,
   PREFCD                  POSITION(07:08)      CHAR,
   NUMBERS                 POSITION(09:16)      INTEGER EXTERNAL "NULLIF(:NUMBERS,  '99999999')"
)

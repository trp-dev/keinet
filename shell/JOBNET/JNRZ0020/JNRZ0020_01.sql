-- ************************************************
-- *機 能 名 : 模試受験者総数（県）
-- *機 能 ID : JNRZ0020_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE EXAMTAKENUM_P_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE EXAMTAKENUM_P_TMP  (
       EXAMYEAR                CHAR(4)           NOT NULL,   /*  模試年度           */
       EXAMCD                  CHAR(2)           NOT NULL,   /*  模試コード         */
       PREFCD                  CHAR(2)           NOT NULL,   /*  県コード           */
       NUMBERS                 NUMBER(8,0),                  /*  人数               */
       CONSTRAINT PK_EXAMTAKENUM_P_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  模試年度    */
                 EXAMCD                    ,               /*  模試コード  */
                 PREFCD                                    /*  県コード    */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

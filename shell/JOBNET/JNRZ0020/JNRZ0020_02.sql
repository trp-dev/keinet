-- *********************************************************
-- *機 能 名 : 模試受験者総数（県）
-- *機 能 ID : JNRZ0020_02.sql
-- *作 成 日 : 2004/09/29
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM EXAMTAKENUM_P
WHERE  (EXAMYEAR,EXAMCD)
       IN(SELECT DISTINCT EXAMYEAR,EXAMCD
          FROM   EXAMTAKENUM_P_TMP);

-- テンポラリテーブルから更新対象データを挿入
-- （県コード99は対象外）
INSERT INTO EXAMTAKENUM_P
SELECT * FROM EXAMTAKENUM_P_TMP
WHERE  PREFCD!='99';

-- 模試受験者総数（高校）データ作成済みの場合は削除する
DELETE FROM EXAMTAKENUM_S
WHERE  (EXAMYEAR,EXAMCD)
       IN(SELECT DISTINCT EXAMYEAR,EXAMCD
          FROM   EXAMTAKENUM_P_TMP)
AND    BUNDLECD LIKE '__aaa';

-- 模試受験者総数（高校）データの作成
-- （県コード＋’aaa'を一括コードとする）
-- （県コード99は対象外）
INSERT INTO EXAMTAKENUM_S
SELECT EXAMYEAR,EXAMCD,(PREFCD || 'aaa'),NUMBERS
FROM   EXAMTAKENUM_P_TMP
WHERE  PREFCD!='99';

-- 8年以上前のデータを削除
DELETE FROM EXAMTAKENUM_P
WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 8);

-- 更新対象データの7年前のデータを削除
DELETE FROM EXAMTAKENUM_P
WHERE  (EXAMYEAR,EXAMCD)
      IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 7)),EXAMCD
          FROM   EXAMTAKENUM_P_TMP);

-- テンポラリテーブル削除
DROP TABLE EXAMTAKENUM_P_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

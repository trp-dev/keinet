-- *********************************************************
-- *機 能 名 : 判定イレギュラファイル
-- *機 能 ID : JNRZ0610_02.sql
-- *作 成 日 : 2004/08/31
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM IRREGULARFILE
WHERE  (EXAMYEAR,EXAMDIV)
       IN(SELECT DISTINCT EXAMYEAR,EXAMDIV
          FROM   IRREGULARFILE_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO IRREGULARFILE
SELECT * FROM IRREGULARFILE_TMP;

-- 8年以上前のデータを削除
DELETE FROM IRREGULARFILE
WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 2);

-- 更新対象データの7年前のデータを削除
DELETE FROM IRREGULARFILE
WHERE  (EXAMYEAR,EXAMDIV)
       IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 1)),EXAMDIV
          FROM   IRREGULARFILE_TMP);

-- テンポラリテーブル削除
DROP TABLE IRREGULARFILE_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

-- ************************************************
-- *機 能 名 : 判定イレギュラファイル
-- *機 能 ID : JNRZ0610_01.sql
-- *作 成 日 : 2004/08/31
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE IRREGULARFILE_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE IRREGULARFILE_TMP  (
       EXAMYEAR                   CHAR(4)               NOT NULL,   /*  年度                      */
       EXAMDIV                    CHAR(2)               NOT NULL,   /*  模試区分                  */
       SCHOOLDIV                  CHAR(1)                       ,   /*  学校区分                  */
       UNIVCD8                    CHAR(8)                       ,   /*  大学コード(8桁)           */
       ENTEXAMDIV                 CHAR(1)                       ,   /*  入試試験区分              */
       IRREGULARDIV               CHAR(1)                       ,   /*  判定イレギュラ区分        */
       IRREGULARCOURSEDIV         CHAR(1)                       ,   /*  判定イレギュラ教科区分    */
       IRREGULAR1                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点1       */
       IRREGULAR2                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点2       */
       IRREGULAR3                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点3       */
       IRREGULAR4                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点4       */
       IRREGULAR5                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点5       */
       IRREGULAR6                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点6       */
       IRREGULAR7                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点7       */
       IRREGULAR8                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点8       */
       IRREGULAR9                 NUMBER(5,1)                   ,   /*  判定イレギュラ配点9       */
       CONSTRAINT PK_IRREGULARFILE_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  年度                   */
                 EXAMDIV                   ,               /*  模試区分               */
                 SCHOOLDIV                 ,               /*  学校区分               */
                 UNIVCD8                   ,               /*  大学コード(8桁)        */
                 ENTEXAMDIV                ,               /*  入試試験区分           */
                 IRREGULARDIV              ,               /*  判定イレギュラ区分     */
                 IRREGULARCOURSEDIV                        /*  判定イレギュラ教科区分 */
       )
       USING INDEX TABLESPACE     KEINAVI_STG_INDX
)
       TABLESPACE       KEINAVI_STG_DATA;

-- SQL*PLUS終了
EXIT;

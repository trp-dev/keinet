------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 判定イレギュラファイル
--  テーブル名      : IRREGULARFILE
--  データファイル名: IRREGULARFILE
--  機能            : 判定イレギュラファイルをデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE IRREGULARFILE_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   EXAMYEAR                POSITION(01:04)      CHAR,
   EXAMDIV                 POSITION(05:05)      CHAR "CONCAT('0', :EXAMDIV)",
   SCHOOLDIV               POSITION(06:06)      CHAR,
   UNIVCD8                 POSITION(07:14)      CHAR,
   ENTEXAMDIV              POSITION(15:15)      CHAR,
   IRREGULARDIV            POSITION(16:16)      CHAR,
   IRREGULARCOURSEDIV      POSITION(17:17)      CHAR,
   IRREGULAR1              POSITION(18:22)      INTEGER EXTERNAL "DECODE(:IRREGULAR1,     '99999', null, :IRREGULAR1/10)",
   IRREGULAR2              POSITION(23:27)      INTEGER EXTERNAL "DECODE(:IRREGULAR2,     '99999', null, :IRREGULAR2/10)",
   IRREGULAR3              POSITION(28:32)      INTEGER EXTERNAL "DECODE(:IRREGULAR3,     '99999', null, :IRREGULAR3/10)",
   IRREGULAR4              POSITION(33:37)      INTEGER EXTERNAL "DECODE(:IRREGULAR4,     '99999', null, :IRREGULAR4/10)",
   IRREGULAR5              POSITION(38:42)      INTEGER EXTERNAL "DECODE(:IRREGULAR5,     '99999', null, :IRREGULAR5/10)",
   IRREGULAR6              POSITION(43:47)      INTEGER EXTERNAL "DECODE(:IRREGULAR6,     '99999', null, :IRREGULAR6/10)",
   IRREGULAR7              POSITION(48:52)      INTEGER EXTERNAL "DECODE(:IRREGULAR7,     '99999', null, :IRREGULAR7/10)",
   IRREGULAR8              POSITION(53:57)      INTEGER EXTERNAL "DECODE(:IRREGULAR8,     '99999', null, :IRREGULAR8/10)",
   IRREGULAR9              POSITION(58:62)      INTEGER EXTERNAL "DECODE(:IRREGULAR9,     '99999', null, :IRREGULAR9/10)"
)

-- *********************************************************
-- *機 能 名 : 認定試験詳細情報マスタ
-- *機 能 ID : JNRZ1500_02.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM ENGPTDETAILINFO MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM ENGPTDETAILINFO_TMP TMP
         WHERE 1=1
           AND MAIN.EVENTYEAR    = TMP.EVENTYEAR
           AND MAIN.EXAMDIV      = TMP.EXAMDIV
           AND MAIN.ENGPTCD      = TMP.ENGPTCD
           AND MAIN.ENGPTLEVELCD = TMP.ENGPTLEVELCD
       )
;

-- テンポラリから本物へ登録
INSERT INTO ENGPTDETAILINFO SELECT * FROM ENGPTDETAILINFO_TMP;

-- テンポラリテーブル削除
DROP TABLE ENGPTDETAILINFO_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

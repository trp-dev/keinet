------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 科目別成績（県）
--  テーブル名      : SUBRECORD_P
--  データファイル名: SUBRECORD_P
--  機能            : 科目別成績（県）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE SUBRECORD_P_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( 
   EXAMYEAR         POSITION(01:04)  CHAR,
   EXAMCD           POSITION(05:06)  CHAR,
   PREFCD           POSITION(07:08)  CHAR,
   SUBCD            POSITION(09:12)  CHAR,
   AVGPNT           POSITION(13:17)  INTEGER EXTERNAL "DECODE(:AVGPNT,         '99999', null, :AVGPNT/10)",
   AVGDEVIATION     POSITION(18:21)  INTEGER EXTERNAL "DECODE(:AVGDEVIATION,    '9999', null, :AVGDEVIATION/10)",
   AVGSCORERATE     POSITION(22:25)  INTEGER EXTERNAL "DECODE(:AVGSCORERATE,    '9999', null, :AVGSCORERATE/10)",
   NUMBERS          POSITION(26:33)  INTEGER EXTERNAL "NULLIF(:NUMBERS,     '99999999')"
)

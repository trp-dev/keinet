-- *********************************************************
-- *機 能 名 : 情報誌用科目（センター）
-- *機 能 ID : JNRZPC02_02.sql
-- *作 成 日 : 2015/09/28
-- *作 成 者 : 富士通システムズ・ウエスト
-- *更 新 日 : 2019/09/10
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

/*
-- ＧＬ１２用センタ外国語の扱い対応１
UPDATE
      JH01_KAMOKU2_TMP
SET
      CEN_GAI_SHO_PT   = NULL ,
      CEN_GAI_SHO_SEL  = NULL 
WHERE
      CEN_GAI_HOKA_SEL =  '1'
 AND (CEN_GAI_DAI_SEL  <> '2'
 OR   CEN_GAI_DAI_SEL  is NULL)
 AND  CEN_GAI_SHO_SEL  =  '2';

-- ＧＬ１２用センタ外国語の扱い対応２
UPDATE
      JH01_KAMOKU2_TMP
SET
      CEN_GAI_DAI_PT   = NULL ,
      CEN_GAI_DAI_SEL  = NULL 
WHERE
      CEN_GAI_HOKA_SEL = '1'
 AND  CEN_GAI_DAI_SEL  = '2';

-- ＧＬ１２用センタ数学の扱い対応１
UPDATE
      JH01_KAMOKU2_TMP
SET
      CEN_MATH_SHO_PT   = NULL ,
      CEN_MATH_SHO_SEL  = NULL 
WHERE
      CEN_MATH_HOKA_SEL =  '1'
 AND (CEN_MATH_DAI_SEL  <> '2'
 OR   CEN_MATH_DAI_SEL  is NULL)
 AND  CEN_MATH_SHO_SEL  =  '2';

-- ＧＬ１２用センタ数学の扱い対応２
UPDATE
      JH01_KAMOKU2_TMP
SET
      CEN_MATH_DAI_PT   = NULL ,
      CEN_MATH_DAI_SEL  = NULL 
WHERE
      CEN_MATH_HOKA_SEL = '1'
 AND  CEN_MATH_DAI_SEL  = '2';
*/

-- 更新対象データを削除
TRUNCATE TABLE JH01_KAMOKU2;

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO JH01_KAMOKU2 SELECT * FROM JH01_KAMOKU2_TMP;

-- テンポラリテーブル削除
DROP TABLE JH01_KAMOKU2_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

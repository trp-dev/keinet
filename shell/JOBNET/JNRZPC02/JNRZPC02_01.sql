-- ************************************************
-- *機 能 名 : 情報誌用科目（センター）
-- *機 能 ID : JNRZPC02_01.sql
-- *作 成 日 : 2015/09/28
-- *作 成 者 : 富士通システムズ・ウエスト
-- *更 新 日 : 2019/09/10
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE JH01_KAMOKU2_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE JH01_KAMOKU2_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM JH01_KAMOKU2 WHERE 0=1;

-- 主キーの設定
ALTER TABLE JH01_KAMOKU2_TMP ADD CONSTRAINT PK_JH01_KAMOKU2_TMP PRIMARY KEY 
(
   JOINKEY
) USING INDEX TABLESPACE KEINAVI_INDX;

-- SQL*PLUS終了
EXIT;

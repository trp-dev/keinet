------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 模試受験者総数(高校）
--  テーブル名      : EXAMTAKENUM_S
--  データファイル名: EXAMTAKENUM_S
--  機能            : 模試受験者総数(高校）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE EXAMTAKENUM_S_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   EXAMYEAR                POSITION(01:04)      CHAR,
   EXAMCD                  POSITION(05:06)      CHAR,
   BUNDLECD                POSITION(07:11)      CHAR,
   NUMBERS                 POSITION(12:19)      INTEGER EXTERNAL "NULLIF(:NUMBERS,  '99999999')"
)

#! /bin/csh

if ($#argv <= 1 ) then
        echo " param error "
exit
endif

if ($1 == 1) then
	echo "  集計データの削除開始 年度:$2  模試コード: $3"
	sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-all.sql $2 $3
else if ($1 == 2) then
	echo "  個人データの削除開始 年度:$2  模試コード: $3"
	sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-indiv.sql $2 $3
else if ($1 == 3) then
	echo "  集計・個人データの削除開始 年度:$2  模試コード: $3"
	sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-all.sql $2 $3
	sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-indiv.sql $2 $3
else if ($1 == 4) then
	echo "  集計データの削除開始 年度:$2  模試コード: $3 一括コード：$4 "
	sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-all_bundlecd.sql $2 $3 $4
else if ($1 == 5) then
	echo "  集計データの削除開始 年度:$2 "
	sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-all_year.sql $2
else
	echo "  集計データの削除開始 年度:$2 "
	sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/delete-all_year2.sql $2
endif

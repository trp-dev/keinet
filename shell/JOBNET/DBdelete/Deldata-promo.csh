#! /bin/sh 

### 進級処理による過年度データの削除バッチ
### 
### 8年前の集計データを削除する

if [ $# != 1 ]
then
        echo "---- PARAMETER ERROR ----"
	exit
fi

nowyear=`date +%Y`
nowdate=`date +%m%d`
export listfile="/keinavi/JOBNET/del_examlist.txt"
if [ $1 = "0" ]
then
	##--- 削除定義ファイル取得 4月1日に起動
	if [ $nowdate = 0401 ]
	then

		#-- 集計データ削除用対象模試一覧取得
		delyear=`expr $nowyear - 8`
		sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBdelete/getexamlist.sql $delyear
		sed -n '3,/ /p' /keinavi/JOBNET/del_targetexam.txt > $listfile
		\rm -f /keinavi/JOBNET/del_targetexam.txt

		#-- 基本情報削除定義ファイル作成
		touch /keinavi/JOBNET/del_info.txt

		#-- 個人成績データ削除定義ファイル作成
		targettable='INDIVIDUALRECORD_TMP SUBRECORD_I QRECORD_I CANDIDATERATING SUBRECORD_PP QRECORD_PP CANDIDATERATING_PP INDIVIDUALRECORD SUBACADEMIC_I SUBACADEMIC_I_PP'
		for tablelist in $targettable; do
			echo $tablelist >> /keinavi/JOBNET/del_datatable.txt
		done
	else
		echo "--- すでに$nowyear 年度用のデー削除定義ファイルは作成済みかJOBNET起動日に達していません。---"
		exit 1
	fi
elif [ $1 = "1" ]
then
	##-- 個人基本情報データ削除処理
	
	delyear=`expr $nowyear - 3`

	if [[ ! -f /keinavi/JOBNET/del-info.txt ]]
	then
		echo "--- すでに $delyear 年度の個人基本情報データは削除されています ----"
		exit 1
	fi

	#-- 基本情報データの削除開始
	echo " "
#	sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBscript/del-indivinfo.sql $delyear 


elif [ $1 = "2" ]
then
	##-- 個人成績データ削除処理

	delyear=`expr $nowyear - 3`



elif [ $1 = "3" ]
then
	##-- 集計データ削除処理

	delyear=`expr $nowyear - 8`
	export examlist=`cat $listfile`
	\rm -f $listfile


	#--- 取得した対象模試に対して処理
	export nowtimes=`date +%H%M`
	for examcd in $examlist; do

		echo " "
		echo "--- 処理対象模試：$examcd"

		#--- 対象模試別に制限時間を設定
		if [ $examcd = 01 ]||[ $examcd = 02 ]||[ $examcd = 03 ]||[ $examcd = 04 ]||[ $examcd = 05 ]||[ $examcd = 06 ]||[ $examcd = 07 ]||[ $examcd = 09 ]||[ $examcd = 38 ]
		then
			export limittime="0330"
		else
			export limittime="0600"
		fi

		#--- 現時間が制限時間より超えていた場合は次回持ち越し
		if [ $nowtimes -le "2200" ]&&[ $nowtimes -ge $limittime ]
		then
			echo "--- 制限時間越えのため、次回へ処理は持ち越します ---"
			echo "$examcd" >> $listfile
			continue
		fi

		#--- データの削除開始
		echo "--- データ削除処理開始します..."
#		sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/DBscript/del-countdata.sql $delyear $examcd

		export nowtimes=`date +%H%M`
		echo "--- $nowtimes  $delyear 年度 $examcd 模試データの削除処理完了しました ---"
	
	done

fi

-- 集計系
delete from EXAMTAKENUM_S where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
delete from EXAMTAKENUM_C where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
delete from SUBRECORD_S where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
commit;
delete from SUBRECORD_C where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
commit;
delete from SUBDISTRECORD_S where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
commit;
delete from SUBDISTRECORD_C where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
commit;
delete from QRECORD_S where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
commit;
delete from QRECORD_C where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
commit;
delete from QRECORDZONE where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
commit;

delete from RATINGNUMBER_S where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
commit;
delete from RATINGNUMBER_C where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
commit;
delete from MARKQSCORERATE where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
commit;
delete from SHOQUESTIONRECORD_S where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
commit;

--  個人系
delete from INDIVIDUALRECORD_TMP where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';

delete from SUBRECORD_I where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
commit;

delete from QRECORD_I where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
commit;

delete from CANDIDATERATING where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';

commit;
delete from CANDIDATERATING_PP where EXAMYEAR='&1' and EXAMCD='&2' and SCHOOLCD='&3';

delete from SUBRECORD_PP where EXAMYEAR='&1' and EXAMCD='&2' and SCHOOLCD='&3';

delete from QRECORD_PP where EXAMYEAR='&1' and EXAMCD='&2'and SCHOOLCD='&3';
commit;

delete from SUBACADEMIC_I where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';

delete from SUBACADEMIC_I_PP where EXAMYEAR='&1' and EXAMCD='&2'and SCHOOLCD='&3';
commit;

-- 該当年度の該当模試のみを受験している生徒
/*
 * BASICINFO */
delete from BASICINFO where INDIVIDUALID in (select INDIVIDUALID from HISTORYINFO where YEAR='&1' and INDIVIDUALID in (
                select INDIVIDUALID from HISTORYINFO where YEAR='&1' and INDIVIDUALID in (
                        select S_INDIVIDUALID from INDIVIDUALRECORD where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3'
                        MINUS
                        select S_INDIVIDUALID from INDIVIDUALRECORD where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3' and S_INDIVIDUALID in (select distinct S_INDIVIDUALID from INDIVIDUALRECORD where NOT (EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3'))))
        MINUS
        select distinct INDIVIDUALID from HISTORYINFO where NOT YEAR='&1');

/*
 * HISTORYINFO */
delete from HISTORYINFO where YEAR='&1' and INDIVIDUALID in (select S_INDIVIDUALID from INDIVIDUALRECORD where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3'
        MINUS
        select S_INDIVIDUALID from INDIVIDUALRECORD where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3' and S_INDIVIDUALID IN (
                select distinct S_INDIVIDUALID from INDIVIDUALRECORD where NOT (EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3')));

commit;
-- 該当年度では該当模試のみ受験している生徒（該当年度以外は他の模試も受験している生徒）
/*
 * HISTORYINFO */
delete from HISTORYINFO where YEAR='&1' and INDIVIDUALID in (select S_INDIVIDUALID from INDIVIDUALRECORD where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3' and S_INDIVIDUALID IN (
      		select distinct S_INDIVIDUALID from INDIVIDUALRECORD where NOT (EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3'))
        MINUS
        select S_INDIVIDUALID from INDIVIDUALRECORD where EXAMYEAR='&1' and BUNDLECD='&3' and NOT EXAMCD='&2');
commit;

/*
 * INDIVIDUALRECORD */
delete from INDIVIDUALRECORD where EXAMYEAR='&1' and EXAMCD='&2' and BUNDLECD='&3';
commit;
EXIT

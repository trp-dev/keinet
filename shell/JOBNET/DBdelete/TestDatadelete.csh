#! /bin/csh

##### リモートシェルによるデータ削除
##### パラメータ値：1 → テストデータ削除、2 → 進級処理によるデータ削除

set examlistpath = "/keinavi/JOBNET/DBdelete"
set outfname = "/keinavi/JOBNET/returnresult"
set JOBNETNAME = "SqlStart-while"
set delparam = $1
set delflg = 0

if (! -f ${examlistpath}/grepexam) then
	echo "--- 削除定義ファイル（${examlistpath}/grepexam）が存在しません ---"
	exit
endif

#-- ファイル転送
/keinavi/HULFT/ftp.sh /keinavi/JOBNET ${examlistpath} grepexam

#-- リモートシェル実行処理判別
if ($delparam == 1) then
	echo "--- テストデータ削除処理を開始します。" 
	set delflg = 1
else
	echo "--- 過年度データ削除処理を開始します。" 
	set delflg = 5
endif
	
#-- リモートシェル実行（データ削除）
/usr/bin/ssh satu04test /keinavi/JOBNET/JobnetStart_ini.sh ${JOBNETNAME} /keinavi/JOBNET/grepexam $delflg
if ($status != 0) then
        echo $status > $outfilename
else
        /usr/bin/rsh satu04test cat /keinavi/JOBNET/result_${JOBNETNAME} > $outfname
endif
/usr/bin/rsh satu04test \rm /keinavi/JOBNET/result_${JOBNETNAME}
/usr/bin/rsh satu04test \rm /keinavi/JOBNET/grepexam
\rm -f $examlistpath/grepexam

#----コマンド実行結果の識別
set cord = `tail -1 $outfname`
if ($cord > 0) then
        echo "----ERROR：データ削除処理に失敗しました。$cord ----"
else if ($cord < 0) then
        echo " "
        echo "----ERROR：処理異常により、処理に失敗しました。管理者に確認してください。----"
else
	echo "データの削除処理が正常終了しました。 ---"
endif
rm -f $outfname
exit $cord

delete from INDIVIDUALRECORD_TMP where EXAMYEAR='&1'
and EXAMCD='&2';

delete from SUBRECORD_I where EXAMYEAR='&1'
and EXAMCD='&2';
commit;

delete from QRECORD_I where EXAMYEAR='&1'
and EXAMCD='&2';
commit;

delete from CANDIDATERATING where EXAMYEAR='&1'
and EXAMCD='&2';

commit;
delete from CANDIDATERATING_PP where EXAMYEAR='&1'
and EXAMCD='&2';

delete from SUBRECORD_PP where EXAMYEAR='&1'
and EXAMCD='&2';

delete from QRECORD_PP where EXAMYEAR='&1'
and EXAMCD='&2';
commit;

-- 2020.03.03 共通テスト対応　新規テーブル追加 ADD STA
delete from SUBACADEMIC_I where EXAMYEAR='&1'
and EXAMCD='&2';
commit;

delete from SUBACADEMIC_I_PP where EXAMYEAR='&1'
and EXAMCD='&2';
commit;
-- 2020.03.03 共通テスト対応　新規テーブル追加 ADD END


-- 該当年度の該当模試のみを受験している生徒
delete from BASICINFO where INDIVIDUALID in (select INDIVIDUALID from HISTORYINFO where YEAR='&1' and INDIVIDUALID in (
		select INDIVIDUALID from HISTORYINFO where YEAR='&1' and INDIVIDUALID in (
			select S_INDIVIDUALID from INDIVIDUALRECORD where EXAMYEAR='&1' and EXAMCD='&2'
        		MINUS
			select S_INDIVIDUALID from INDIVIDUALRECORD where EXAMYEAR='&1' and EXAMCD='&2' and S_INDIVIDUALID in (select distinct S_INDIVIDUALID from INDIVIDUALRECORD where NOT (EXAMYEAR='&1' and EXAMCD='&2'))))
        MINUS
        select distinct INDIVIDUALID from HISTORYINFO where NOT YEAR='&1');


delete from HISTORYINFO where YEAR='&1' and INDIVIDUALID in (
	select S_INDIVIDUALID from INDIVIDUALRECORD where EXAMYEAR='&1' and EXAMCD='&2' 
	MINUS
	select S_INDIVIDUALID from INDIVIDUALRECORD where EXAMYEAR='&1' and EXAMCD='&2' and S_INDIVIDUALID IN (
		select distinct S_INDIVIDUALID from INDIVIDUALRECORD where NOT (EXAMYEAR='&1' and EXAMCD='&2')));
commit;

-- 該当年度では該当模試のみ受験している生徒（該当年度以外は他の模試も受験している生徒）
delete from HISTORYINFO where YEAR='&1' and INDIVIDUALID in (
	select S_INDIVIDUALID from INDIVIDUALRECORD where EXAMYEAR='&1' and EXAMCD='&2' and S_INDIVIDUALID IN (
		select distinct S_INDIVIDUALID from INDIVIDUALRECORD where NOT (EXAMYEAR='&1' and EXAMCD='&2'))
	MINUS
	select S_INDIVIDUALID from INDIVIDUALRECORD where EXAMYEAR='&1' and NOT EXAMCD='&2');
commit;

delete from INDIVIDUALRECORD where EXAMYEAR='&1'
and EXAMCD='&2';
commit;
EXIT

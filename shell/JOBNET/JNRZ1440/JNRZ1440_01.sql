-- ************************************************
-- *機 能 名 : 記述式総合評価配点マスタ
-- *機 能 ID : JNRZ1440_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE DESCCOMPRATINGPNT_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE DESCCOMPRATINGPNT_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM DESCCOMPRATINGPNT WHERE 0=1;

-- 主キーの設定
ALTER TABLE DESCCOMPRATINGPNT_TMP ADD CONSTRAINT PK_DESCCOMPRATINGPNT_TMP PRIMARY KEY 
(
   EVENTYEAR
 , EXAMCD
 , SUBCD
 , DESCCOMPRATING
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

------------------------------------------------------------------------------
-- *機 能 名 : 記述式総合評価配点マスタのCTLファイル
-- *機 能 ID : JNRZ1440.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE DESCCOMPRATINGPNT_TMP
(  
   EVENTYEAR         POSITION(01:04)  CHAR 
 , EXAMCD            POSITION(05:06)  CHAR 
 , SUBCD             POSITION(07:10)  CHAR 
 , DESCCOMPRATING    POSITION(11:11)  CHAR 
 , DESCCOMPRATINGPNT POSITION(12:14)  CHAR 
)

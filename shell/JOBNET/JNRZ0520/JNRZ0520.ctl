------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 模試科目マスタ
--  テーブル名      : EXAMQUESTION
--  データファイル名: EXAMQUESTION
--  機能            : 模試科目マスタをデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE EXAMQUESTION_TMP
WHEN QUESTIONNAME1 != ''
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   EXAMYEAR         POSITION(01:04)  CHAR,
   EXAMCD           POSITION(05:06)  CHAR,
   SUBCD            POSITION(07:10)  CHAR,
   QUESTION_NO      POSITION(11:12)  INTEGER EXTERNAL,
   DISPLAY_NO       POSITION(13:14)  INTEGER EXTERNAL,
   QUESTIONNAME1    POSITION(15:26)  CHAR,
   QUESTIONNAME2    POSITION(27:58)  CHAR,
   QALLOTPNT        POSITION(59:62)  INTEGER EXTERNAL "NULLIF(:QALLOTPNT, '9999')"
)

-- *********************************************************
-- *機 能 名 : 模試設問マスタ
-- *機 能 ID : JNRZ0520_02.sql
-- *作 成 日 : 2004/08/30
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM EXAMQUESTION
WHERE  (EXAMYEAR,EXAMCD)
       IN(SELECT DISTINCT EXAMYEAR,EXAMCD
          FROM   EXAMQUESTION_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO EXAMQUESTION
SELECT * FROM EXAMQUESTION_TMP;

-- 8年以上前のデータを削除
-- DELETE FROM EXAMQUESTION
-- WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 8);

-- 更新対象データの7年前のデータを削除
-- DELETE FROM EXAMQUESTION
-- WHERE  (EXAMYEAR,EXAMCD)
--        IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 7)),EXAMCD
--          FROM   EXAMQUESTION_TMP);

-- テンポラリテーブル削除
DROP TABLE EXAMQUESTION_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

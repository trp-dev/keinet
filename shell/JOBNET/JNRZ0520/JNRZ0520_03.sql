-- ************************************************
-- *機 能 名 : 模試設問マスタ
-- *機 能 ID : JNRZ0520_01.sql
-- *作 成 日 : 2004/08/30
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE EXAMQUESTION_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

-- ************************************************
-- *機 能 名 : 模試設問マスタ
-- *機 能 ID : JNRZ0520_01.sql
-- *作 成 日 : 2004/08/30
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE EXAMQUESTION_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE EXAMQUESTION_TMP  (
       EXAMYEAR        CHAR(4)            NOT NULL,    /*   模試年度        */ 
       EXAMCD          CHAR(2)            NOT NULL,    /*   模試コード      */ 
       SUBCD           CHAR(4)            NOT NULL,    /*   科目コード      */ 
       QUESTION_NO     NUMBER(2,0)        NOT NULL,    /*   設問番号        */ 
       DISPLAY_NO      NUMBER(2,0)                ,    /*   表示番号        */ 
       QUESTIONNAME1   VARCHAR2(12)               ,    /*   設問内容名１    */ 
       QUESTIONNAME2   VARCHAR2(32)               ,    /*   設問内容名２    */ 
       QALLOTPNT       NUMBER(4,0)                ,    /*   配点            */ 
       CONSTRAINT PK_EXAMQUESTION_TMP PRIMARY KEY (
                 EXAMYEAR                 ,               /*  模試年度    */
                 EXAMCD                   ,               /*  模試コード  */
                 SUBCD                    ,               /*  科目コード  */
                 QUESTION_NO                              /*  設問番号    */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

#! /bin/csh

##### Kei-Navi　契約校コード取得バッチ
##### センターリサーチ用契約校リストファイル作成

set place = "/keinavi/JOBNET/getPactSchoolcd"
set outtmpfile = "$place/tmpfile"
set outfile1 = "$place/pactschoolcd1"
set outfile2 = "$place/PACTSCHOOL_SELECTLIST"
set out73cd = "$place/out73cd"
set ymd = "`date +%Y%m%d`"
set pwd = "$1-banzai"

#現行のファイルをバックアップ
cp -av ${outfile2} ${outfile2}"_"${ymd}

if (-f ${outfile2}) then
	\rm ${outfile2}
endif

#--- 契約校取得
sqlplus keinavi/39ra8ma@S11DBA_BATCH2 @/keinavi/JOBNET/getPactSchoolcd/getpact.sql > ${outtmpfile}

#--- 出力ファイルから高校コードのみを取得
grep "^[0-9]\{5\}" ${outtmpfile} > ${outfile1}

#--- 出力ファイルをバックアップ
cp -p ${outfile1} ${outfile2}

set cd = `cat ${outfile1}`

echo "--- PACTSCHOOL からの契約校取得完了 $#cd校"

echo "$#cd 校の副コード取得開始..."

sqlplus keinavi/39ra8ma@S11DBA_BATCH2 @get73cd.sql > ${out73cd}

grep "^[0-9]\{5\}" ${out73cd} >> ${outfile2}

echo " "
echo "--- 副コード取得完了"

#--- 出力ファイル削除
\rm ${outtmpfile}
\rm ${outfile1}
\rm ${out73cd}

set allcd = `cat ${outfile2}`

echo " "
echo "*** 計$#allcd 契約校コード取得完了 ***"

chmod 664 ${outfile2}

#--- ファイルコピー
#if (-f /keinavi/HULFT/WorkDir/${outfile2}) then
#	\rm /keinavi/HULFT/WorkDir/${outfile2}
#endif
#cp -p ${outfile2} /keinavi/HULFT/WorkDir

#--- 検証機へファイル転送（センタープレ用DLファイル分割のため）
echo " "
echo "--- 検証機へのファイル転送開始"
#/keinavi/HULFT/ftp.sh /keinavi/ $place PACTSCHOOL_SELECTLIST
echo "検証機へのファイル転送正常終了 ---"

#オリジナルをバックアップ
cp -a ${outfile2} ${outfile2}.org

#LF→CRLF変換
echo " "
echo "--- LF→CRLF変換開始"
sed 's/$/\r/g' ${outfile2} > ${outfile2}.crlf
echo "LF→CRLF変換終了 ---"

#ファイル名をオリジナルに戻す
mv -f ${outfile2}.crlf ${outfile2}

#zip化
echo " "
echo "--- zip化開始"
zip -P ${pwd} PACTSCHOOL_SELECTLIST.zip PACTSCHOOL_SELECTLIST
echo "zip化終了 ---"

#ファイルをオリジナルに戻す
rm -f ${outfile2}
mv -f ${outfile2}.org ${outfile2}


-- ************************************************
-- *機 能 名 : 教科マスタ
-- *機 能 ID : JNRZ1030_01.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE COURSE_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE COURSE_TMP(    
     YEAR           CHAR(4)         NOT NULL,   /*    年度        */ 
     COURSECD       CHAR(4)         NOT NULL,   /*    教科コード  */ 
     COURSENAME     VARCHAR2(10)            ,   /*    教科名      */ 
    CONSTRAINT PK_COURSE_TMP PRIMARY KEY(
            YEAR,
            COURSECD
    )
    USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

-- *********************************************************
-- *機 能 名 : CEFR取得状況（高校）
-- *機 能 ID : JNRZ1220_02.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM CEFRACQUISITIONSTATUS_S MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM CEFRACQUISITIONSTATUS_S_TMP TMP
         WHERE 1=1
           AND MAIN.EXAMYEAR     = TMP.EXAMYEAR
           AND MAIN.EXAMCD       = TMP.EXAMCD
           AND MAIN.BUNDLECD     = TMP.BUNDLECD
           AND MAIN.CEFRLEVELCD  = TMP.CEFRLEVELCD
       )
;

-- テンポラリから本物へ登録
INSERT INTO CEFRACQUISITIONSTATUS_S SELECT * FROM CEFRACQUISITIONSTATUS_S_TMP;

-- テンポラリテーブル削除
DROP TABLE CEFRACQUISITIONSTATUS_S_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

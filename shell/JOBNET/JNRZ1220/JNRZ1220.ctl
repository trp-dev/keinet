------------------------------------------------------------------------------
-- *機 能 名 : CEFR取得状況（高校）のCTLファイル
-- *機 能 ID : JNRZ1220.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE CEFRACQUISITIONSTATUS_S_TMP
(  
   EXAMYEAR    POSITION(01:04)  CHAR 
 , EXAMCD      POSITION(05:06)  CHAR 
 , BUNDLECD    POSITION(07:11)  CHAR 
 , CEFRLEVELCD POSITION(12:13)  CHAR 
 , NUMBERS     POSITION(14:20)  INTEGER EXTERNAL 
 , COMPRATIO   POSITION(21:24)  INTEGER EXTERNAL "DECODE(:COMPRATIO,   '9999', null, :COMPRATIO/10)"
)

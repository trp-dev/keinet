-- ************************************************
-- *機 能 名 : センター得点換算表
-- *機 能 ID : JNRZ0530_01.sql
-- *作 成 日 : 2004/08/30
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE CENTERCVSSCORE_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE CENTERCVSSCORE_TMP  (
       EXAMYEAR       CHAR(2)              NOT NULL,    /*    年度          */ 
       EXAMCD         CHAR(2)              NOT NULL,    /*    模試コード    */ 
       SUBCD          CHAR(4)              NOT NULL,    /*    科目コード    */ 
       RAWSCORE       NUMBER(3,0)          NOT NULL,    /*    素点          */ 
       CVSSCORE       NUMBER(3,0)                  ,    /*    換算得点      */ 
       CONSTRAINT PK_CENTERCVSSCORE_TMP PRIMARY KEY (
                 EXAMYEAR                  ,               /*  年度        */
                 EXAMCD                    ,               /*  模試コード  */
                 SUBCD                     ,               /*  科目コード  */
                 RAWSCORE                                  /*  素点        */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

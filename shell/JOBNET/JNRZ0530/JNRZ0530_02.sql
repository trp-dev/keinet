-- *********************************************************
-- *機 能 名 : センター得点換算表
-- *機 能 ID : JNRZ0530_02.sql
-- *作 成 日 : 2004/08/30
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM CENTERCVSSCORE
WHERE  (EXAMYEAR,EXAMCD)
       IN(SELECT DISTINCT EXAMYEAR,EXAMCD
          FROM   CENTERCVSSCORE_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO CENTERCVSSCORE
SELECT * FROM CENTERCVSSCORE_TMP;

-- 2年以上前のデータを削除
DELETE FROM CENTERCVSSCORE
WHERE  TO_NUMBER(TO_CHAR(TO_DATE(EXAMYEAR,'RR'),'YYYY')) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 2);

-- 更新対象データの7年前のデータを削除
DELETE FROM CENTERCVSSCORE
WHERE  (TO_CHAR(TO_DATE(EXAMYEAR,'RR'),'YYYY'),EXAMCD)
       IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 1)),EXAMCD
          FROM   CENTERCVSSCORE_TMP);

-- テンポラリテーブル削除
DROP TABLE CENTERCVSSCORE_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

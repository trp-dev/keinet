------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : センター得点換算表
--  テーブル名      : CENTERCVSSCORE
--  データファイル名: CENTERCVSSCORE
--  機能            : センター得点換算表をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE CENTERCVSSCORE_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   EXAMYEAR      POSITION(01:02)   CHAR,
   EXAMCD        POSITION(03:04)   CHAR,
   SUBCD         POSITION(05:08)   CHAR,
   RAWSCORE      POSITION(09:11)   INTEGER EXTERNAL,
   CVSSCORE      POSITION(12:14)   INTEGER EXTERNAL "NULLIF(:CVSSCORE, '999')"
)

-- ************************************************
-- *機 能 名 : 大学マスタ公表用教科
-- *機 能 ID : JNRZ0563_01.sql
-- *作 成 日 : 2010/02/10
-- *作 成 者 : 
-- *更 新 日 : 
-- *更新内容 : 
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE UNIVMASTER_COURSE_PUB_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE UNIVMASTER_COURSE_PUB_TMP  (
       EVENTYEAR                    CHAR(4)                NOT NULL,   /*   行事年度                    */
       SCHOOLDIV                    CHAR(1)                NOT NULL,   /*   学校区分                    */
       UNIVCD                       CHAR(10)               NOT NULL,   /*   大学コード                  */
       EXAMDIV                      CHAR(2)                NOT NULL,   /*   模試区分                    */
       ENTEXAMDIV                   CHAR(1)                NOT NULL,   /*   入試試験区分                */
       UNICDBRANCHCD                CHAR(3)                NOT NULL,   /*   大学コード枝番              */
       COURSECD                     CHAR(1)                NOT NULL,   /*   教科コード                  */
       NECESALLOTPNT                NUMBER(5,1)                    ,   /*   必須配点                    */
       NECESSUBCOUNT                CHAR(1)                        ,   /*   必須科目数                  */
       CEN_SECSUBCHOICEDIV          CHAR(1)                        ,   /*   センター二次同一科目選択区分*/
       CONSTRAINT PK_UNIVMASTER_COURSE_PUB_TMP PRIMARY KEY (
                 EVENTYEAR             ,               /*  行事年度          */
                 SCHOOLDIV             ,               /*  学校区分          */
                 UNIVCD                ,               /*  大学コード        */
                 EXAMDIV               ,               /*  模試区分          */
                 ENTEXAMDIV            ,               /*  入試試験区分      */
                 UNICDBRANCHCD         ,               /*  大学コード枝番    */
                 COURSECD                              /*  教科コード        */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

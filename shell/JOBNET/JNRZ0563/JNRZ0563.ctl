------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 大学マスタ公表用教科
--  テーブル名      : UNIVMASTER_COURSE_PUB
--  データファイル名: UNIVMASTER_COURSE_PUB
--  機能            : 大学マスタ公表用教科をデータファイルの内容に置き換える
--  作成日          : 2010/02/10
--  修正日          : 
--  備考            : ２次(ENTEXAMDIV=2)のみ、英語、数学、国語の判定必須科目が
--                  : １以上であれば１にする
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE UNIVMASTER_COURSE_PUB_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    EVENTYEAR,
    SCHOOLDIV,
    UNIVCD,
    EXAMDIV,
    ENTEXAMDIV,
    UNICDBRANCHCD        CHAR "LPAD(:UNICDBRANCHCD, 3, '0')",
    COURSECD,
    NECESALLOTPNT        INTEGER EXTERNAL "DECODE(:NECESALLOTPNT,   '99999', null, :NECESALLOTPNT)",
    NECESSUBCOUNT,
    CEN_SECSUBCHOICEDIV
)

#!/bin/bash
cur=$(cd $(dirname $0); pwd)


#パラメータチェック
. $cur/check_parameter.sh
if [ $? -eq 1 ]; then
    exit 1
fi


# set environment
. /keinavi/JOBNET/config/env.sh


#10年前を算出
prev_year=$(($exam_year-10))
#前回模試区分を算出
prev_exam_div=$(expr $exam_div - 1)
prev_exam_div=`printf %02d $prev_exam_div`
if [ "$prev_exam_div" = "00" ]; then
    prev_exam_div=08
fi


#変数定義
now_target="年度：[$exam_year] 模試区分：[$exam_div]"
prev_target="年度：[$prev_year] 模試区分：[$exam_div]"


#start
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "大学マスタの復活 start."
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "  from【$prev_target】"
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "    to【$now_target】"
echo ""


#for debug
echo "********** for debug **********"
echo "対象年度：     "$exam_year
echo "模試区分：     "$exam_div
echo "処理日：       "$today
echo "前回年度：     "$prev_year
echo "前回模試区分： "$prev_exam_div
echo "********** for debug **********"
echo ""


#大学マスタの復活
upd=`sqlplus -s $db_uid/$db_pwd@$db_tns << EOF
@$cur/sql/restore_univmaster.sql $exam_year $exam_div $prev_year $prev_exam_div
EOF`
ret=$?
upd=`echo $upd`
if [ $ret = 0 ]; then
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "大学マスタ(UNIVMASTER_BASIC)の更新件数：$upd件"
elif [ $ret = 1 ]; then
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "対象の大学マスタが登録されていません。 【$prev_target】"
elif [ $ret = 2 ]; then
    echo `date "+%Y/%m/%d %H:%M:%S.%3N" `"復活先の大学マスタが存在します。 【$now_target】"
elif [ $ret = 3 ]; then
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "復活対象の大学マスタが正常に更新されていません。 【$now_target】"
elif [ $ret = 4 ]; then
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "復活対象の大学マスタが正常に更新されていません。 【$now_target】"
elif [ $ret = 99 ]; then
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "対象の大学マスタ復活に失敗しました。 【$now_target】"
fi


#end
echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "大学マスタの復活 end."
exit $ret

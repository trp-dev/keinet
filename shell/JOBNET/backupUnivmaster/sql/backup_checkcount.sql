set feedback off
set heading off
set pagesize 0
set verify off
SET SERVEROUTPUT ON
VARIABLE vEXIT NUMBER;
DECLARE
    cnt NUMBER;
    cnt_jh NUMBER;
    upd NUMBER;
    bak_ub NUMBER := &5;
    bak_jh NUMBER := &6;
BEGIN
    :vEXIT := 0;

    /* チェック１：大学マスタに退避する対象年度・模試区分データが存在しないか */
    SELECT
        count(*) 
    INTO
        cnt
    FROM
        UNIVMASTER_BASIC
    WHERE
        EVENTYEAR = '&1'
    AND EXAMDIV = '&2';
    -- ０件以外の場合、エラー
    IF cnt <> 0 THEN
        --DBMS_OUTPUT.PUT_LINE('ERROR! UNIVMASTER_BASIC: EVENTYEAR=[ &1 ] EXAMDIV=[ &2 ] COUNT=[ ' || cnt || ' ]');
        :vEXIT := 1;
        RETURN;
    END IF;


    /* チェック２：大学マスタに退避した対象年度・模試区分データが存在するか */
    SELECT
        count(*) 
    INTO
        cnt
    FROM
        UNIVMASTER_BASIC
    WHERE
        EVENTYEAR = '&3'
    AND EXAMDIV = '&2';
    -- 退避件数と一致しない場合、エラー
    IF cnt <> bak_ub THEN
        :vEXIT := 2;
        RETURN;
    END IF;


    /* チェック３：情報誌科目（共通）に退避する対象年度・模試区分データが存在しないか */
    SELECT
        count(*) 
    INTO
        cnt
    FROM
        JH01_KAMOKU1
    WHERE
        THE_YEAR = '&1'
    AND MOCK_TYP = '&2';
    -- ０件以外の場合、エラー
    IF cnt <> 0 THEN
        :vEXIT := 3;
        RETURN;
    END IF;


    /* チェック４：情報誌科目（共通）に退避した対象年度・模試区分データが存在するか */
    SELECT
        count(*) 
    INTO
        cnt_jh
    FROM
        JH01_KAMOKU1
    WHERE
        THE_YEAR = '&3'
    AND MOCK_TYP = '&4';
    -- 退避件数と一致しない場合、エラー
    IF cnt_jh <> bak_jh THEN
        :vEXIT := 4;
        RETURN;
    END IF;


    /* チェック５：情報誌科目（共通テスト）の件数が情報誌科目（共通）の件数と一致するか */
    SELECT
        count(*) 
    INTO
        cnt
    FROM
        JH01_KAMOKU2;
    -- 件数が一致しない場合、エラー
    IF cnt <> cnt_jh THEN
        :vEXIT := 5;
        RETURN;
    END IF;


    /* チェック６：情報誌科目（二次）の件数が情報誌科目（共通）の件数と一致するか */
    SELECT
        count(*) 
    INTO
        cnt
    FROM
        JH01_KAMOKU3;
    -- 件数が一致しない場合、エラー
    IF cnt <> cnt_jh THEN
        :vEXIT := 6;
        RETURN;
    END IF;


    /* チェック７：情報誌科目（科目名）の件数が情報誌科目（共通）の件数と一致するか */
    SELECT
        count(*) 
    INTO
        cnt
    FROM
        JH01_KAMOKU4;
    -- 件数が一致しない場合、エラー
    IF cnt <> cnt_jh THEN
        :vEXIT := 7;
        RETURN;
    END IF;

EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        :vEXIT := 99;
END;
/ 

EXIT :vEXIT;

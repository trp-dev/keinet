set feedback off
set heading off
set pagesize 0
set verify off
SET SERVEROUTPUT ON
VARIABLE vEXIT NUMBER;
VARIABLE vCOUNT NUMBER;
DECLARE
    cnt NUMBER;
    upd NUMBER;
BEGIN
    :vEXIT := 0;

    -- 退避対象大学マスタの件数を取得
    SELECT
        count(*) 
    INTO
        cnt
    FROM
        univmaster_basic
    WHERE
        eventyear = '&3'
    AND examdiv = '&2';
    -- 退避対象が存在しない場合
    IF cnt = 0 THEN
        :vEXIT := 1;
        RETURN;
    END IF;


    -- 大学マスタの更新
    UPDATE
        univmaster_basic
    SET
        eventyear = '&1'
    WHERE
        eventyear = '&3'
    AND examdiv = '&2';
    upd := SQL%ROWCOUNT;


    -- 退避対象大学マスタの件数を取得
    SELECT
        COUNT(*)
    INTO
        cnt
    FROM
        univmaster_basic
    WHERE
        eventyear = '&3'
    AND examdiv = '&2';
    -- 退避先対象が存在する場合
    IF cnt > 0 THEN
        :vEXIT := 3;
        ROLLBACK;
        RETURN;
    END IF;


    -- 退避対象大学マスタの件数を取得
    SELECT
        count(*) 
    INTO
        cnt
    FROM
        univmaster_basic
    WHERE
        eventyear = '&1'
    AND examdiv = '&2';
    -- 件数が異なる場合
    IF cnt <> upd THEN
        :vEXIT := 4;
        ROLLBACK;
        RETURN;
    END IF;
    :vCOUNT := upd;

    COMMIT;
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        :vEXIT := 99;
END;
/ 
PRINT :vCOUNT

EXIT :vEXIT;

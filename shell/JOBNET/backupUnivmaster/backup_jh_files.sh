#!/bin/bash
cur=$(cd $(dirname $0); pwd)


#パラメータチェック
. $cur/check_parameter.sh
if [ $? -eq 1 ]; then
    exit 1
fi


# set environment
. /keinavi/JOBNET/config/env.sh


#変数定義
now_target="年度：[$exam_year] 模試区分：[$exam_div] 今回模試コード：[$exam_cd] 前回模試コード：[$prev_exam_cd] "
#前回模試が記述高２の場合、年度を-1する
if [ "$prev_exam_cd" = "65" ]; then
    exam_year=$(($exam_year-1))
fi
#前回模試がリサーチの場合、取得元を変更する
if [ "$prev_exam_cd" = "38" ]; then
    #得点調整を考慮して更新日が最新の「JH01_KAMOKU1」を探して見つかったディレクトリが最新格納先とする
    src=$(dirname $(find /keinavi/HULFT/NewEXAM/${exam_year}${prev_exam_cd}* -name 'JH01_KAMOKU1' -type f | xargs ls -tr | tail -n 1))
else
    src=/keinavi/HULFT/DATA/$exam_year$prev_exam_cd
fi
dst=/keinavi/HULFT/WorkDir
back=/home/kei-navi


#start
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "情報誌データの変更(退避) start."
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "  【$now_target】"
echo ""


#for debug
echo "********** for debug **********"
echo "対象年度：" $exam_year
echo "模試区分：" $exam_div
echo "今回模試コード：" $exam_cd
echo "前回模試コード：" $prev_exam_cd
echo "対象ファイル格納先:" $src
echo "作業ディレクトリ:" $dst
echo "退避ディレクトリ:" $back
echo "********** for debug **********"
echo ""

#不要ファイルの削除
rm -rfv $dst/*-old*


#残存ファイルのバックアップ
files=$(find ${dst} -type f ! -name "SCHOOL" -and ! -name "PACTSCHOOL_SELECTLIST")
for fp in $files; do
    #ファイル名を取得
    file=$(basename $fp)
    #ディレクトリが存在しない場合
    if [ ! -d $back/$today ]; then
        #ディレクトリを作成
        mkdir -pv $back/$today
        chown -v kei-navi.staff $back/$today
    fi
    #ファイルの移動
    mv -fv $dst/$file $back/$today
done


#対象ファイルのコピー
cp -apv $src/JH01_KAMOKU* $dst


#不要ファイルの削除
rm -rfv $dst/*-old*


#所有者の変更
chown -v orauser.ora_ins_group $dst/JH01_KAMOKU*


#権限の付与
chmod -v 777 $dst/JH01_KAMOKU*


#end
echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "情報誌データの変更(退避) end."
exit 0

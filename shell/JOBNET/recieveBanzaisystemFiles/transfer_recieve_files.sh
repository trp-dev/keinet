#!/bin/bash
cur=$(cd $(dirname $0); pwd)


#パラメータチェック
. $cur/check_parameter.sh
if [ $? -eq 1 ]; then
    exit 1
fi


# set environment
. /keinavi/JOBNET/config/env.sh


#変数定義
now_target="年度：[$exam_year] 模試区分：[$exam_div]"
src=/keinavi/HULFT/NewEXAM/PC_MOSHI_TMP/$target
dst=/keinavi/HULFT/WorkDir


#start
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "検証機への転送 start."
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "  【$now_target】"
echo ""


#for debug
echo "********** for debug **********"
echo "対象年度：     "$exam_year
echo "模試区分：     "$exam_div
echo "処理日：       "$today
echo "********** for debug **********"
echo ""


#ファイル存在確認
if [ ! -e $dst/SCHOOL ]; then
    echo "SHCOOLファイルが存在しません。"
    exit 1
fi
#oldファイルを削除
rm -fv $dst/*old*


#SCP転送
echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "SCP転送 start."
scp -p satu04:/$src/* $dst
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "SCP転送 end."
echo ""


#ファイルの所有者・アクセス権の変更
chown -v orauser.ora_ins_group $dst/*
chmod -v 777 $dst/*


#登録対象外ファイルの削除
rm -fv $dst/JH06_NITTEI_T
rm -fv $dst/DB3*

ls -l $dst

#end
echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "検証機への転送 end."
exit 0

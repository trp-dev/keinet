#!/bin/bash
cur=$(cd $(dirname $0); pwd)


#パラメータチェック
. $cur/check_parameter.sh
if [ $? -eq 1 ]; then
    exit 1
fi


# set environment
. /keinavi/JOBNET/config/env.sh


#変数定義
now_target="年度：[$exam_year] 模試区分：[$exam_div]"
base=/banzaimaster
db=$base/dbDownload
xml=dbVersion2.xml
host=rput03
src=/banzaimaster/dbDownload
dst=/var/www/html/pcbanzai


#start
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "検証機登録 start."
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "  【$now_target】"
echo ""


#for debug
echo "********** for debug **********"
echo "対象年度：     "$exam_year
echo "模試区分：     "$exam_div
echo "処理日：       "$today
echo "********** for debug **********"
echo ""


#cd
cd $db


#今回作成ファイルを取得
files=`find -type f -name "$target*.zip" | sort -r | sed 's!^.*/!!'`
for f in $files; do
    zip=$f
    break
done


echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "SCP転送 start."
#SCP転送
hosts="rput03"
for host in ${hosts}; do
    echo ""
    echo "${host} >>>"
    scp -p ${src}/$xml $rput_uid@${host}:$dst/$xml
    scp -p ${src}/$zip $rput_uid@${host}:$dst/dbDownload/$zip
    echo ""
    echo "ls -l $dst/dbDownload/$zip"
    ssh $rput_uid@$host "ls -l $dst/dbDownload/$zip"
    echo ""
    echo "cat $dst/$xml"
    ssh $rput_uid@$host "cat $dst/$xml"
    echo "<<< ${host}"
    echo ""
done
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "SCP転送 end."
echo ""

#cd
cd $cur


#end
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "検証機登録 end."
exit 0

#!/bin/bash
cur=$(cd $(dirname $0); pwd)


#パラメータチェック
. $cur/check_parameter.sh
if [ $? -eq 1 ]; then
    exit 1
fi


# set environment
. /keinavi/JOBNET/config/env.sh


#変数定義
now_target="年度：[$exam_year] 模試区分：[$exam_div]"
job=/keinavi/JOBNET
base=/banzaimaster
db=$base/dbDownload
xml=dbVersion2.xml


#start
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "大学マスタ作成 start."
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "  【$now_target】"
echo ""


#for debug
echo "********** for debug **********"
echo "対象年度：     "$exam_year
echo "模試区分：     "$exam_div
echo "処理日：       "$today
echo "********** for debug **********"
echo ""


#cd
cd $db


#過去年度のzipを削除
files=`find  -type f ! -name "$exam_year*" -and -name "*.zip" | sort`
for f in $files; do
    rm -rfv $f
done


#大学マスタ作成
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "univ.db作成 start."
bash $base/bin/banzaimaster $exam_year $exam_div
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "univ.db作成 end."
echo ""


#対象のDBが存在する場合
if [ -e $db/univ$target.db ]; then
    mv -fv $db/univ$target.db $db/univ$target.db.$today
fi


#univ.dbをリネーム
mv -fv univ.db univ$target.db


#zip化するファイル名を決定
files=`find -type f -name "$target*.zip" | sort -r`
for f in $files; do
    file=$(basename $f)
    break
done


#対象のファイルが存在しない場合
if [ "$file" = "" ]; then
    index=00
#対象のファイルが存在する場合
else
    #インクリメント
    index=$(( `echo $file | awk '{print substr($0, 7, 2)}'` + 1 ))
    index=`printf %02d $index`
fi


#zip化する
echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "アップロード用zip作成 start."
zip -v $target"$index.zip" univ$target.db
rm -fv univ$target.db
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "アップロード用zip作成 end."
echo ""


#dbVersion2.xmlを作成
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "dbVersion2.xml作成 start."
echo "<?xml version=\"1.0\"?>">$xml
echo "<db>">>$xml
echo "<files>">>$xml
files=`find -type f -name "$exam_year*.zip" | sort`
for f in $files; do
    file=$(basename $f)
    size=`wc -c $f | awk '{print $1}'`
    echo "<file size=\"$size\">$file</file>">>$xml
done
echo "</files>">>$xml
echo "</db>">>$xml
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "dbVersion2.xml作成 end."
echo ""


#模試マスタ更新(戻し)
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "模試マスタの更新(戻し) start."
upd=`sqlplus -s $db_uid/$db_pwd@$db_tns << EOF
@$cur/sql/restore_examination.sql $exam_year
EOF`
ret=$?
upd=`echo $upd`
if [ $ret -ne 0 ]; then
    echo "模試マスタの更新(戻し)に失敗しました。"
    exit 1
else
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "模試マスタの更新件数：$upd件"
fi
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "模試マスタの更新(戻し) end."
echo ""


#cd
cd $cur


#end
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "大学マスタ作成 end."
exit 0

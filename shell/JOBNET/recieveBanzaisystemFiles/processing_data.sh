#!/bin/bash
cur=$(cd $(dirname $0); pwd)


#パラメータチェック
. $cur/check_parameter.sh
if [ $? -eq 1 ]; then
    exit 1
fi


# set environment
. /keinavi/JOBNET/config/env.sh


#変数定義
now_target="年度：[$exam_year] 模試区分：[$exam_div]"


#start
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "データ加工 start."
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "  【$now_target】"
echo ""


#for debug
echo "********** for debug **********"
echo "対象年度：     "$exam_year
echo "模試区分：     "$exam_div
echo "処理日：       "$today
echo "********** for debug **********"
echo ""


#情報誌用科目日程の登録
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "情報誌用科目日程の登録 start."
upd=`sqlplus -s $db_uid/$db_pwd@$db_tns << EOF
@$cur/sql/insert_univinfo.sql
EOF`
ret=$?
if [ $ret -ne 0 ]; then
    echo "情報誌用科目日程の登録に失敗しました。"
    exit 1
fi
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "情報誌用科目日程の登録 end."
echo ""


#模試マスタバックアップ
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "模試マスタのバックアップ start."
upd=`sqlplus -s $db_uid/$db_pwd@$db_tns << EOF
@$cur/sql/backup_examination.sql $exam_year
EOF`
ret=$?
if [ $ret -ne 0 ]; then
    echo "模試マスタのバックアップに失敗しました。"
    exit 1
fi
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "模試マスタのバックアップ end."
echo ""


#模試マスタ更新
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "模試マスタの更新 start."
upd=`sqlplus -s $db_uid/$db_pwd@$db_tns << EOF
@$cur/sql/update_examination.sql $exam_year
EOF`
ret=$?
upd=`echo $upd`
if [ $ret -ne 0 ]; then
    echo "模試マスタの更新に失敗しました。"
    exit 1
else
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "模試マスタの更新件数：$upd件"
fi
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "模試マスタの更新 end."
echo ""


#end
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "データ加工 end."
exit 0

#!/bin/bash
cur=$(cd $(dirname $0); pwd)


#変数定義
base=/keinavi/JOBNET


#start
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "検証機へのファイル登録 start."
echo ""


#JH01_KAMOKU1
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "JH01_KAMOKU1 start."
$base/JNRZPC01/JNRZPC01.sh
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "JH01_KAMOKU1 end."
echo ""


#JH01_KAMOKU2
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "JH01_KAMOKU2 start."
$base/JNRZPC02/JNRZPC02.sh
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "JH01_KAMOKU2 end."
echo ""


#JH01_KAMOKU3
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "JH01_KAMOKU3 start."
$base/JNRZPC03/JNRZPC03.sh
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "JH01_KAMOKU3 end."
echo ""


#JH05_NITTEI
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "JH05_NITTEI start."
$base/JNRZPC05/JNRZPC05.sh
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "JH05_NITTEI end."
echo ""


#DB09_SHIKAKU
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "DB09_SHIKAKU start."
$base/JNRZPC09/JNRZPC09.sh
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "DB09_SHIKAKU end."
echo ""


#DB10_BIKOU
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "DB10_BIKOU start."
$base/JNRZPC10/JNRZPC10.sh
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "DB10_BIKOU end."
echo ""


#DB21_BUNPU
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "DB21_BUNPU start."
$base/JNRZPC21/JNRZPC21.sh
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "DB21_BUNPU end."
echo ""


#DB22_BUNPU
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "DB22_BUNPU start."
$base/JNRZPC22/JNRZPC22.sh
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "DB22_BUNPU end."
echo ""


#DB23_BUNPU
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "DB23_BUNPU start."
$base/JNRZPC23/JNRZPC23.sh
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "DB23_BUNPU end."
echo ""


#SMF0201F
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "SMF0201F start."
$base/JNRZPC24/JNRZPC24.sh
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "SMF0201F end."
echo ""


#JH01_KAMOKU4
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "JH01_KAMOKU4 start."
$base/JNRZPC25/JNRZPC25.sh
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "JH01_KAMOKU4 end."
echo ""


#end
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "検証機へのファイル登録 end."
exit 0

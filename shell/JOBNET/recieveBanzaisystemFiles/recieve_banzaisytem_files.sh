#!/bin/bash
cur=$(cd $(dirname $0); pwd)


#最新以外のファイルを削除
function rm_old_files () {
    files=`find $1* -type f | sort -r`
    idx=0
    for f in $files; do
        if [ $idx -ne 0 ]; then
            rm -rfv $f
        fi
        idx=$(( idx + 1))
    done    
}


#パラメータチェック
. $cur/check_parameter.sh
if [ $? -eq 1 ]; then
    exit 1
fi


# set environment
. /keinavi/JOBNET/config/env.sh


#変数定義
now_target="年度：[$exam_year] 模試区分：[$exam_div]"
fget=/keinavi/JOBNET/Fget
src=/keinavi/HULFT/NewEXAM
dst=/keinavi/HULFT/NewEXAM/PC_MOSHI_TMP/$target


#start
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "PCバンザイファイル受信 start."
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "  【$now_target】"
echo ""


#for debug
echo "********** for debug **********"
echo "対象年度：     "$exam_year
echo "模試区分：     "$exam_div
echo "処理日：       "$today
echo "********** for debug **********"
echo ""


#HULFT集信
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "HULFT受信 start."
sh $fget/fget_PC_MOSHI.sh

sh $fget/delete_size0.sh
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "HULFT受信 end."
echo ""


#ディレクトリが存在する場合、リネームする
if [ -d $dst ]; then
    mv -v $dst $dst"_"$today
fi
#ディレクトリを作成
mkdir -pv $dst
echo ""


#ファイル移動
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "ファイル移動 start."
mv -fv $src/DB* $src/JH0* $dst
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "ファイル移動 end."
echo ""


#古い受信ファイルを削除
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "JH系過去ファイル削除 start."
rm_old_files $dst/JH01_KAMOKU1
rm_old_files $dst/JH01_KAMOKU2
rm_old_files $dst/JH01_KAMOKU3
rm_old_files $dst/JH01_KAMOKU4
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "JH系過去ファイル削除 end."
echo ""


#ファイル名変更（yyyyMMddHHmmssを削除）
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "ファイル名変更(yyyyMMddHHmmss削除) start."
files=`find $dst -type f ! -name "INDIVIDUALRECORD_FD"`
for f in $files; do
    file=`echo $f | rev | cut -c 15- | rev`
    mv -fv $f $file
done
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "ファイル名変更(yyyyMMddHHmmss削除) end."
echo ""


#end
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "PCバンザイファイル受信 end."
exit 0

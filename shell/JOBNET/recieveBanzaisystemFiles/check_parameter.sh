#!/bin/bash


#パラメータチェック
if [ "$1" = "" ]; then
    echo "パラメータ１：年度が未設定です。"
    exit 1
fi
expr "$1" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ１：年度が不正です。【$1】"
    exit 1
fi

if [ "$2" = "" ]; then
    echo "パラメータ２：模試区分が未設定です。"
    exit 1
fi
expr "$2" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ２：模試区分が不正です。【$2】"
    exit 1
fi


export exam_year=$1
export exam_div=$2

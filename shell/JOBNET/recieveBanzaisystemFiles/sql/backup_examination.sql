set feedback off
set heading off
set pagesize 0
set verify off
SET SERVEROUTPUT ON

-- 模試マスタのバックアップ
CREATE TABLE EXAMINATION_&1 AS SELECT * FROM EXAMINATION WHERE EXAMYEAR = '&1';

COMMIT;

EXIT 0;

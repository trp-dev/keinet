set feedback off
set heading off
set pagesize 0
set verify off
SET SERVEROUTPUT ON
VARIABLE vEXIT NUMBER;
VARIABLE vCOUNT NUMBER;
DECLARE
    cnt03 NUMBER;
    cnt65 NUMBER;
BEGIN
    :vEXIT := 0;

    -- 模試マスタの更新（＃３共通）
    UPDATE EXAMINATION
       SET OUT_DATAOPENDATE = (SELECT OUT_DATAOPENDATE FROM EXAMINATION WHERE EXAMYEAR='&1' AND EXAMCD='07') 
     WHERE EXAMYEAR='&1'
       AND EXAMCD='03';
    cnt03 := SQL%ROWCOUNT;


    -- 模試マスタの更新（共通高２）
    UPDATE EXAMINATION
       SET OUT_DATAOPENDATE = (SELECT OUT_DATAOPENDATE FROM EXAMINATION WHERE EXAMYEAR='&1' AND EXAMCD='65')
     WHERE EXAMYEAR='&1'
       AND EXAMCD='66';
    cnt65 := SQL%ROWCOUNT;
    :vCOUNT := cnt03 + cnt65;

    COMMIT;
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        :vEXIT := 99;
END;
/
PRINT :vCOUNT

EXIT :vEXIT;

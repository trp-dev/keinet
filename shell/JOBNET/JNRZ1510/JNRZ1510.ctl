------------------------------------------------------------------------------
-- *機 能 名 : 国語記述式_得点換算マスタのCTLファイル
-- *機 能 ID : JNRZ1510.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE KOKUGO_DESC_SCORECONV_TMP
(  
   EVENTYEAR                  POSITION(01:04)  CHAR 
 , EXAMDIV                    POSITION(05:06)  CHAR 
 , UNIVSINGLECD               POSITION(07:10)  CHAR 
 , CENTERCVSSCORE_KOKUGO_DESC POSITION(11:13)  CHAR 
 , NEXTYEARDIV                POSITION(14:14)  CHAR 
 , ENTEXAMFULLPNT             POSITION(15:18)  INTEGER EXTERNAL "DECODE(:ENTEXAMFULLPNT,             '9999', null, :ENTEXAMFULLPNT/10)"
 , CVSDIV                     POSITION(19:19)  CHAR 
 , QUESTIONNUM                POSITION(20:20)  CHAR 
 , RATING                     POSITION(21:21)  CHAR 
 , SCORE                      POSITION(22:25)  INTEGER EXTERNAL "DECODE(:SCORE,                      '9999', null, :SCORE/10)"
)

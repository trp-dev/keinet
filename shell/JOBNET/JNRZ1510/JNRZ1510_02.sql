-- *********************************************************
-- *機 能 名 : 国語記述式_得点換算マスタ
-- *機 能 ID : JNRZ1510_02.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM KOKUGO_DESC_SCORECONV MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM KOKUGO_DESC_SCORECONV_TMP TMP
         WHERE 1=1
           AND MAIN.EVENTYEAR                   = TMP.EVENTYEAR
           AND MAIN.EXAMDIV                     = TMP.EXAMDIV
           AND MAIN.UNIVSINGLECD                = TMP.UNIVSINGLECD
           AND MAIN.CENTERCVSSCORE_KOKUGO_DESC  = TMP.CENTERCVSSCORE_KOKUGO_DESC
           AND MAIN.NEXTYEARDIV                 = TMP.NEXTYEARDIV
           AND MAIN.CVSDIV                      = TMP.CVSDIV
           AND MAIN.QUESTIONNUM                 = TMP.QUESTIONNUM
           AND MAIN.RATING                      = TMP.RATING
       )
;

-- テンポラリから本物へ登録
INSERT INTO KOKUGO_DESC_SCORECONV SELECT * FROM KOKUGO_DESC_SCORECONV_TMP;

-- テンポラリテーブル削除
DROP TABLE KOKUGO_DESC_SCORECONV_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

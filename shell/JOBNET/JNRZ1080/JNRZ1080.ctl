------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : お知らせ表示マスタ
--  テーブル名      : INFODISP_TMP
--  データファイル名: INFODISPyyyymmdd.csv
--  機能            : お知らせ表示マスタをデータファイルの内容に置き換える
--  作成日          : 2004/08/25
--  修正日          : 2004/09/10
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE INFODISP_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    KEISAI "DECODE(LENGTH(:KEISAI), 3, CONCAT('00', :KEISAI), 4, CONCAT('0', :KEISAI), 5, :KEISAI)",
    INFOID "LTRIM(TO_CHAR(TO_NUMBER(:INFOID),'099999'))",
    DISPSEQUENCE
)

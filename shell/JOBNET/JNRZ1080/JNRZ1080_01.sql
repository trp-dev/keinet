-- ************************************************
-- *機 能 名 : お知らせ表示マスタ
-- *機 能 ID : JNRZ1080_01.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE INFODISP_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE INFODISP_TMP(   
       KEISAI             CHAR(5)           NOT NULL,   /*   掲載場所        */
       INFOID             CHAR(6)           NOT NULL,   /*   お知らせＩＤ    */
       DISPSEQUENCE       NUMBER(2,0)               ,   /*   表示順          */
    CONSTRAINT PK_INFODISP_TMP PRIMARY KEY(
        KEISAI,                                         /*   掲載場所        */
        INFOID                                          /*   お知らせＩＤ    */
    )
    USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;
#!/bin/sh

#学校マスタ受信

date >> /keinavi/JOBNET/JNRZ7110/HULFTReceiveCheck.log
echo 学校マスタ受信START >> /keinavi/JOBNET/JNRZ7110/HULFTReceiveCheck.log

cd /keinavi/HULFT
/usr/local/HULFT/bin/utlrecv -f RZRM7110 -h k03308f6gp -sync -w 300

hulftrc=$?

#if [ $? -ne 0 ]
if [ $hulftrc -ne 0 ]
then
#    echo $? >> /keinavi/JOBNET/JNRZ7110/HULFTReceiveCheck.log
    echo $hulftrc >> /keinavi/JOBNET/JNRZ7110/HULFTReceiveCheck.log
    echo HULFT受信エラー >> /keinavi/JOBNET/JNRZ7110/HULFTReceiveCheck.log
    echo 受信エラーが発生しました
    exit 30
fi

echo HULFT受信正常終了 >> /keinavi/JOBNET/JNRZ7110/HULFTReceiveCheck.log

mv SCHOOL WorkDir/SCHOOL
/keinavi/HULFT/ftp.sh /keinavi/HULFT/WorkDir /keinavi/HULFT/WorkDir SCHOOL 

echo 学校マスタ受信END >> /keinavi/JOBNET/JNRZ7110/HULFTReceiveCheck.log

exit 0

#!/bin/sh

#正常終了時の移動先フォルダ
export ENDDIR=/keinavi/HULFT/EndDir

#データファイル名の読み込み
DATNAME=`cat $WORKDIR/datafilename.dat`

#正常終了時はデータファイルを移動する
mv ${DATNAME} ${ENDDIR}

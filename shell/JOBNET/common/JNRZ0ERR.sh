#!/bin/sh

#異常終了時の移動先フォルダ
export ERRDIR=/keinavi/HULFT/ErrDir

#異常終了時はデータファイル及びログファイル名に日時を付けて移動する

#日時の取得
DAY=`date +%Y%m%d%H%M%S`

#データファイル名の読み込み
DATNAME=`cat $WORKDIR/datafilename.dat`

#データファイルのファイル名のみを取得
DATAFILENAME=`basename ${DATNAME}`

#対象ファイルをエラーフォルダに移動
mv ${DATNAME} ${ERRDIR}/${DAY}_${DATAFILENAME}
if [ -f ${WORKDIR}/${JOBNAME}.log ]
then
cp -p  ${WORKDIR}/${JOBNAME}.log ${ERRDIR}/${DAY}_${JOBNAME}.log
fi

if [ -f ${WORKDIR}/${JOBNAME}.bad ]
then
cp -p  ${WORKDIR}/${JOBNAME}.bad ${ERRDIR}/${DAY}_${JOBNAME}.bad
fi

if [ -f ${WORKDIR}/${JOBNAME}.dsc ]
then
cp -p  ${WORKDIR}/${JOBNAME}.dsc ${ERRDIR}/${DAY}_${JOBNAME}.dsc
fi

#!/bin/sh

#一時ファイル名
TMPTILE=$JOBNAME.tmp

#SQL*LOADER用ファイル
CTLFILE=$JOBNAME.ctl
LOGFILE=$JOBNAME.log
BADFILE=$JOBNAME.bad
DSCFILE=$JOBNAME.dsc
IMPFILE=$JOBNAME.csv

#以前の不要ファイルの削除
rm -f $WORKDIR/$TMPTILE
rm -f $WORKDIR/$LOGFILE
rm -f $WORKDIR/$BADFILE
rm -f $WORKDIR/$DSCFILE
rm -f $WORKDIR/$IMPFILE

#該当ファイルの有無を検索
FCOUNT=`find ${SRCDIR}/ -name "${CSVFILE}" -maxdepth 1 -print | wc -l`

#該当ファイルがあれば
if [ "${FCOUNT}" -gt 0 ]
then
    #最新のマスタファイル名を取得
    DATFILE=`ls -t ${SRCDIR}/${CSVFILE} | sed -n '1p'`

    #一時ファイルの作成
    touch $WORKDIR/$TMPTILE

    #先頭行（項目名タイトル行）の削除
    #SQL*Loaderの機能にもあるが、複数行で１論理レコードの
    #データがあるため、あえて使用しない
    sed -n '2,$p' $DATFILE > $WORKDIR/$IMPFILE

    #マスタメンテデータは改行コードの変換を行う
    dos2unix $WORKDIR/$IMPFILE

    #SQL*LOADER起動
    sqlldr PARFILE=/keinavi/JOBNET/config/keinavi.par CONTROL=$WORKDIR/$CTLFILE DATA=$WORKDIR/$IMPFILE BAD=$WORKDIR/$BADFILE DISCARD=$WORKDIR/$DSCFILE LOG=$WORKDIR/$LOGFILE ERRORS=1000000 ROWS=1000

    #SQL*LOADER戻り値チェック(0以外は異常終了)
    if [ $? -ne 0 ]
    then
        echo SQL*LOADERでエラーが発生しました
        exit 30
    fi

    #過去データファイルの削除
    rm -f $DSTDIR/$CSVFILE

    #変換後のCSVファイル削除
    rm -f $WORKDIR/$IMPFILE

    #オリジナルデータファイルの移動
    mv $DATFILE $DSTDIR/

    #一時ファイルの削除
    rm -f $WORKDIR/$TMPTILE

    #終了コード発行
    if [ -f $WORKDIR/$TMPTILE -o -f $WORKDIR/$BADFILE ]
    then
        echo データのインポート処理でエラーが発生しました
        exit 30    #異常終了
    else
        echo データのインポート処理が正常に終了しました
        exit  0    #正常終了
    fi
else
    #該当ファイルがない場合はエラー
    echo データファイルが見つかりません
    exit 10    #異常終了
fi

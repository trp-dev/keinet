#! /bin/sh

#該当データファイルの有無確認
FCOUNT=`find ${SRCDIR}/ -name "${CSVFILE}" -maxdepth 1 -print | wc -l`

#該当ファイルが無ければエラーで返す
if [ "${FCOUNT}" -le 0 ]
then
	echo データファイルが見つかりません
	exit 10    #異常終了
fi

exit 0 


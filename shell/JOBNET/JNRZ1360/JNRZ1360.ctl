------------------------------------------------------------------------------
-- *機 能 名 : 国語設問別条件別正答率・設問別評価人数（全国）のCTLファイル
-- *機 能 ID : JNRZ1360.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE KKG_ANSRT_EVALNUMS_A_TMP
(  
   EXAMYEAR            POSITION(01:04)  CHAR 
 , EXAMCD              POSITION(05:06)  CHAR 
 , SUBCD               POSITION(07:10)  CHAR 
 , QUESTION_NO         POSITION(11:12)  CHAR 
 , CORRECT_ANSRATE_1   POSITION(13:16)  INTEGER EXTERNAL "DECODE(:CORRECT_ANSRATE_1,   '9999', null, :CORRECT_ANSRATE_1/10)"
 , CORRECT_ANSRATE_2   POSITION(17:20)  INTEGER EXTERNAL "DECODE(:CORRECT_ANSRATE_2,   '9999', null, :CORRECT_ANSRATE_2/10)"
 , CORRECT_ANSRATE_3   POSITION(21:24)  INTEGER EXTERNAL "DECODE(:CORRECT_ANSRATE_3,   '9999', null, :CORRECT_ANSRATE_3/10)"
 , CORRECT_ANSRATE_4   POSITION(25:28)  INTEGER EXTERNAL "DECODE(:CORRECT_ANSRATE_4,   '9999', null, :CORRECT_ANSRATE_4/10)"
 , CORRECT_ANSRATE_5   POSITION(29:32)  INTEGER EXTERNAL "DECODE(:CORRECT_ANSRATE_5,   '9999', null, :CORRECT_ANSRATE_5/10)"
 , CORRECT_ANSRATE_6   POSITION(33:36)  INTEGER EXTERNAL "DECODE(:CORRECT_ANSRATE_6,   '9999', null, :CORRECT_ANSRATE_6/10)"
 , CORRECT_ANSRATE_7   POSITION(37:40)  INTEGER EXTERNAL "DECODE(:CORRECT_ANSRATE_7,   '9999', null, :CORRECT_ANSRATE_7/10)"
 , CORRECT_PERSONNUM_1 POSITION(41:47)  INTEGER EXTERNAL 
 , CORRECT_PERSONNUM_2 POSITION(48:54)  INTEGER EXTERNAL 
 , CORRECT_PERSONNUM_3 POSITION(55:61)  INTEGER EXTERNAL 
 , CORRECT_PERSONNUM_4 POSITION(62:68)  INTEGER EXTERNAL 
 , CORRECT_PERSONNUM_5 POSITION(69:75)  INTEGER EXTERNAL 
 , CORRECT_PERSONNUM_6 POSITION(76:82)  INTEGER EXTERNAL 
 , CORRECT_PERSONNUM_7 POSITION(83:89)  INTEGER EXTERNAL 
 , EVAL_A_COMPRATIO     POSITION(90:93)  INTEGER EXTERNAL "DECODE(:EVAL_A_COMPRATIO,     '9999', null, :EVAL_A_COMPRATIO/10)"
 , EVAL_A_AST_COMPRATIO POSITION(94:97)  INTEGER EXTERNAL "DECODE(:EVAL_A_AST_COMPRATIO, '9999', null, :EVAL_A_AST_COMPRATIO/10)"
 , EVAL_B_COMPRATIO     POSITION(98:101)  INTEGER EXTERNAL "DECODE(:EVAL_B_COMPRATIO,     '9999', null, :EVAL_B_COMPRATIO/10)"
 , EVAL_B_AST_COMPRATIO POSITION(102:105)  INTEGER EXTERNAL "DECODE(:EVAL_B_AST_COMPRATIO, '9999', null, :EVAL_B_AST_COMPRATIO/10)"
 , EVAL_C_COMPRATIO     POSITION(106:109)  INTEGER EXTERNAL "DECODE(:EVAL_C_COMPRATIO,     '9999', null, :EVAL_C_COMPRATIO/10)"
 , EVAL_A_NUMBERS       POSITION(110:116)  INTEGER EXTERNAL 
 , EVAL_A_AST_NUMBERS   POSITION(117:123)  INTEGER EXTERNAL 
 , EVAL_B_NUMBERS       POSITION(124:130)  INTEGER EXTERNAL 
 , EVAL_B_AST_NUMBERS   POSITION(131:137)  INTEGER EXTERNAL 
 , EVAL_C_NUMBERS       POSITION(138:144)  INTEGER EXTERNAL 
 , NUMBERS              POSITION(145:151)  INTEGER EXTERNAL 
)

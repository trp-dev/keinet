-- ************************************************
-- *機 能 名 : 国語設問別条件別正答率・設問別評価人数（全国）
-- *機 能 ID : JNRZ1360_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE KKG_ANSRT_EVALNUMS_A_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE KKG_ANSRT_EVALNUMS_A_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM KKG_ANSRT_EVALNUMS_A WHERE 0=1;

-- 主キーの設定
ALTER TABLE KKG_ANSRT_EVALNUMS_A_TMP ADD CONSTRAINT PK_KKG_ANSRT_EVALNUMS_A_TMP PRIMARY KEY 
(
   EXAMYEAR
 , EXAMCD
 , SUBCD
 , QUESTION_NO
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;


-- テンポラリテーブル削除
DROP TABLE UNIVDIST_SECOND_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE UNIVDIST_SECOND_TMP  (
	EXAMYEAR       CHAR(4),
	EXAMCD         CHAR(2),
	UNIVCD         CHAR(4),
	FACULTYCD      CHAR(2),
	DEPTCD1        CHAR(2),
	DEPTCD2        CHAR(2),
	PATTERNCD      CHAR(1),
	RANK           CHAR(2),
	PUBENTEXAMCAPA NUMBER(5,0),
	NUMBERS_A      NUMBER(5,0),
	AVGPNT_A       NUMBER(5,1),
	BORDER1        CHAR(1),
	NUMBERS1       NUMBER(4,0),
	BORDER2        CHAR(1),
	NUMBERS2       NUMBER(4,0),
	BORDER3        CHAR(1),
	NUMBERS3       NUMBER(4,0),
	BORDER4        CHAR(1),
	NUMBERS4       NUMBER(4,0),
	BORDER5        CHAR(1),
	NUMBERS5       NUMBER(4,0),
	BORDER6        CHAR(1),
	NUMBERS6       NUMBER(4,0),
	BORDER7        CHAR(1),
	NUMBERS7       NUMBER(4,0),
	BORDER8        CHAR(1),
	NUMBERS8       NUMBER(4,0),
	BORDER9        CHAR(1),
	NUMBERS9       NUMBER(4,0),
	BORDER10       CHAR(1),
	NUMBERS10      NUMBER(4,0),
	BORDER11       CHAR(1),
	NUMBERS11      NUMBER(4,0),
	BORDER12       CHAR(1),
	NUMBERS12      NUMBER(4,0),
	BORDER13       CHAR(1),
	NUMBERS13      NUMBER(4,0),
	BORDER14       CHAR(1),
	NUMBERS14      NUMBER(4,0),
	BORDER15       CHAR(1),
	NUMBERS15      NUMBER(4,0),
	BORDER16       CHAR(1),
	NUMBERS16      NUMBER(4,0),
	BORDER17       CHAR(1),
	NUMBERS17      NUMBER(4,0),
	BORDER18       CHAR(1),
	NUMBERS18      NUMBER(4,0),
	BORDER19       CHAR(1),
	NUMBERS19      NUMBER(4,0),
	NUMBERS_S      NUMBER(5,0),
	AVGPNT_S       NUMBER(5,1),
	NUMBERS_G      NUMBER(5,0),
	AVGPNT_G       NUMBER(5,1),
    CONSTRAINT PK_UNIVDIST_SECOND_TMP PRIMARY KEY (
        EXAMYEAR, EXAMCD, UNIVCD, FACULTYCD, DEPTCD1, DEPTCD2, PATTERNCD
    )
    USING INDEX TABLESPACE     KEINAVI_STG_INDX
)
    TABLESPACE       KEINAVI_STG_DATA;

-- SQL*PLUS終了
EXIT;

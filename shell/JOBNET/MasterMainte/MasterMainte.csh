#! /bin/csh 

set place = "/keinavi/help_user/MasterMainte/Set"
set ExecFlg = "$1" 
set JOBNETNAME = "$2"
set Targetfname = "${3}*.csv"
set outfilename = "/keinavi/JOBNET/MasterMainte/returncd"

#----タイムスタンプにより最新の対象ファイルを取得
(ls -t $place/${Targetfname} > /keinavi/JOBNET/flistout.log) >& /dev/null
set FileList = `cat /keinavi/JOBNET/flistout.log`
\rm /keinavi/JOBNET/flistout.log
if ($#FileList == 0) then
	echo "データファイルが見つかりません"
	echo " "
	echo "処理対象ファイルなし [${3}.csv] （正常終了）"
	exit
endif

set FILENAME = $FileList[1]

#---- 検証機用か本番機用か判別
if ($ExecFlg == 0) then
	echo " 検証機への転送＆登録の対象ファイル： $FILENAME"
	
	#----対象ファイルを検証機へFTP転送
	/keinavi/HULFT/ftp.sh /keinavi/help_user/MasterMainte/Set $place $FILENAME
	#----リモートシェルを実行
	rsh satu04test /keinavi/JOBNET/JobnetStart_ini.sh ${JOBNETNAME}

	if ($status != 0) then
		echo "-1" > $outfilename
	else
		rsh satu04test cat /keinavi/JOBNET/result_${JOBNETNAME} > $outfilename 
	endif
	rsh satu04test \rm /keinavi/JOBNET/result_${JOBNETNAME}
else
	echo " 登録の対象ファイル： $FILENAME"

	#----本番機のJOBNET起動
	/keinavi/JOBNET/${JOBNETNAME}/${JOBNETNAME}.sh
	echo $status > $outfilename
endif

#----コマンド実行結果の識別
set cord = `tail -1 $outfilename`
if ($cord > 0) then
	echo " "
	echo "処理に失敗しました。下記の内容を元にファイルフォーマットを一度確認してください。"
	echo "=============================================================================="
	#---- エラーファイルを出力（ファイルの25行目～80行目を表示)
	if ($ExecFlg == 0) then
		rsh satu04test sed -n '25,/85/p' /keinavi/JOBNET/${JOBNETNAME}/${JOBNETNAME}.log
	else
		sed -n '25,/85/p' /keinavi/JOBNET/${JOBNETNAME}/${JOBNETNAME}.log
	endif
	echo "=============================================================================="
	
else if ($cord < 0) then
	echo " "
	echo "処理異常により、処理に失敗しました。管理者に確認してください。"
endif
\rm $outfilename
exit $cord



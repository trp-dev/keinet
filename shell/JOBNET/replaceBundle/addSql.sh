#!/bin/bash


now=$(date "+%Y%m%d")
base=/keinavi/JOBNET/replaceBundle
rows=$(cat /tmp/addSql.txt | wc -l)
rows=$((rows+5))

cd /keinavi/JOBNET/JNRZ0620


# ファイルをバックアップ
echo "既存ファイルをバックアップ"
cp -av options.sql options.sql.$now


# 追加分をoptions.sqlに結合
echo ""
echo "SQLを追加"
cat options.sql /tmp/addSql.txt>options.sql.edit


# EXITを削除
sed -i -e 's/EXIT//g' options.sql.edit


# EXITを末尾に追加
sed -i '$a EXIT' options.sql.edit
echo ""


# ファイルを移動
echo "編集を確定"
mv -fv options.sql.edit options.sql
echo ""


# 旧ファイルの末尾
echo "↓↓↓旧ファイルの末尾↓↓↓"
tail -n5 options.sql.$now
echo "↑↑↑旧ファイルの末尾↑↑↑"
echo ""


# 新ファイルの末尾
echo "↓↓↓新ファイルの末尾↓↓↓"
tail -n$rows options.sql
echo "↑↑↑新ファイルの末尾↑↑↑"
echo ""
echo "ファイルの末尾に正しく追加されているか確認してください。"

#!/bin/bash



# 環境設定
. ./env.sh



# step3
echo ""
echo ""
echo "■ step3 > 高校コード置換（ファイル名・高校コード）"
while read years exams before after
do
	count=0
	copies=()

	# 置換前の高校コードファイルを検索
	for file in $(cat $bundle | grep "${before}.csv"); do
		# 配列に追加
		copies+=($file)
		count=$((count+1))
	done

	echo "   対象年度: [ ${years[@]} ] 対象模試: [ ${exams[@]} ] 高校コード:[ ${before} ]  >  ${count} 件置換しました。"
	for file in ${copies[@]}; do
		dir=$src$(dirname $file)
		dest=${dir/back/csv}
		echo "     >  [ ${src}${file} ] -> [ ${dest}/${after}.csv ]"
		# 高校コード置換
		sed -e "s/,${before},/,${after},/g" $src$file > $dest/${after}.csv
	done
done < $target

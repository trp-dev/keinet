SET SERVEROUTPUT ON FORMAT WRAPPED
SET LINESIZE 1024
SET HEAD OFF
SET FEED OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
VARIABLE vEXIT NUMBER;

DECLARE
	query               LONG := NULL;
	cnt                 NUMBER := 0;
-- メイン処理
BEGIN

	-- 置換対象レコードキー情報を削除
	 EXECUTE IMMEDIATE 'TRUNCATE TABLE REPLACE_TARGET';

	:vEXIT := 0;
	DBMS_OUTPUT.PUT_LINE('高校コード置換（対象抽出）処理　開始');
	-- 置換対象レコードキー情報を登録
	FOR cur_main IN (
		SELECT *
		  FROM REPLACE_BUNDLECD
		)
	LOOP
		BEGIN
			-- クエリーを生成
			query := NULL;
			query := query || 'INSERT INTO REPLACE_TARGET ';
			query := query || 'SELECT DISTINCT EXAMYEAR, EXAMCD, BUNDLECD, :TO_BUNDLECD';
			query := query || '  FROM EXAMTAKENUM_S';
			query := query || ' WHERE BUNDLECD = :BUNDLECD';
			query := query || '   AND EXAMYEAR BETWEEN :BEGIN_EXAMYEAR AND :END_EXAMYEAR';
			IF cur_main.EXAMCD IS NOT NULL THEN
				query := query || '   AND EXAMCD IN (' || cur_main.EXAMCD || ')';
			END IF;
			EXECUTE IMMEDIATE query USING cur_main.TO_BUNDLECD, cur_main.FROM_BUNDLECD, cur_main.BEGIN_EXAMYEAR, cur_main.END_EXAMYEAR;
			cnt := cnt + SQL%ROWCOUNT;
		EXCEPTION
			-- 重複エラーは無視
			WHEN DUP_VAL_ON_INDEX THEN
				NULL;
		END;
	END LOOP;

	DBMS_OUTPUT.PUT_LINE('');
	DBMS_OUTPUT.PUT_LINE('  >> [ REPLACE_TARGET ] に登録  件数 = [ ' || TO_CHAR(cnt, '9999') || ' ]');
	DBMS_OUTPUT.PUT_LINE('');
	DBMS_OUTPUT.PUT_LINE('高校コード置換（対象抽出）処理　終了');
	COMMIT;

-- 例外処理
EXCEPTION
	WHEN OTHERS THEN
		:vEXIT := -1;
		DBMS_OUTPUT.PUT_LINE('!!! 高校コード置換（対象抽出）処理にてエラーが発生しました !!!');
		DBMS_OUTPUT.PUT_LINE('  >> ' || SQLERRM);
		ROLLBACK;
END;
/
EXIT :vEXIT;

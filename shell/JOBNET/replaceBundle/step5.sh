#!/bin/bash



# 環境設定
. ./env.sh



# step4
echo ""
echo ""
echo "■ step5 > 置換前ファイルの削除"
while read years exams before after
do
	count=0
	copies=()

	# 置換前の高校コードファイルを検索
	for file in $(cat $bundle | grep "${before}.csv"); do
		# 配列に追加
		copies+=($file)
		count=$((count+1))
	done

	echo "   対象年度: [ ${years[@]} ] 対象模試: [ ${exams[@]} ] 高校コード:[ ${before} ]  >  ${count} 件削除しました。"
	for file in ${copies[@]}; do
		# 置換前のファイルを削除
		rm -f $file
		echo "     >  [ ${file} ]"
	done
done < $target

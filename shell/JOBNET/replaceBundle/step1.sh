#!/bin/bash



# 環境設定
. ./env.sh



# step1
echo ""
echo ""
echo "■ step1 > 置換後の高校コードチェック"
while read years exams before after
do
	count=0
	errors=()

	# 置換後の高校コードファイルを検索
	for file in $(cat $bundle | grep "${after}.csv"); do
		errors+=($file)
		count=$((count+1))
		ret=1
	done

	# 結果を表示
	echo "   対象年度: [ ${years[@]} ] 対象模試: [ ${exams[@]} ] 高校コード:[ ${after} ]  >  ${count} 件"
	for file in ${errors[@]}; do
		echo "     >  [ ${file} ]"
	done
done < $target

# 置換後コードのファイルが存在する場合、処理を中断
if [ $ret -ne 0 ]; then
	echo "置換後の高校コードファイルが存在するため処理を中断します。"
	exit 1
fi
echo "重複するファイルはありません。"

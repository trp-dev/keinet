#!/bin/bash



# 環境設定
. ./env.sh



# step0 前準備
echo ""
echo ""
echo "■ step0 > 高校コード置換対象ファイル抽出"
mkdir -pv $backup/back
mkdir -pv $backup/csv
rm -fv $bundle 
tcount=0
while read base_years base_exams before after
do
    # パラメーターを配列に分解
    base_years=(${base_years//,/ })
    base_exams=(${base_exams//,/ })
    count=0

    # 置換前・後の高校コードファイルを検索
    files=$(find ${path} -type f -name "${before}.csv" -or -name "${after}.csv")
    targets=()
    # 指定された年度分繰り返す
    for year in ${base_years[@]}; do
        # 指定された模試コード分繰り返す
        for exam in ${base_exams[@]}; do
            # 指定された年度・模試コードでフィルタ
            if [ "$exam" = "ALL" ]; then
                fil="/${year}/"
            else
                fil="/${year}/${exam}/"
            fi
            # ファイルが存在する場合
            for file in $(ls -1 $files | grep $fil | grep -v "old" | grep -v "-"); do
                targets+=($file)
                count=$((count+1))
            done
        done
    done
    # 結果を表示
    echo "   対象年度: [ ${base_years[@]} ] 対象模試: [ ${base_exams[@]} ] 高校コード:[ ${before} ${after} ]  >  ${count} 件"
    for file in ${targets[@]}; do
        echo ${file}>>$bundle
		tcount=$((tcount+1))
    done
done < $target
if [ $tcount -eq 0 ]; then
	echo "対象ファイルはありません。"
	exit 0
else
	echo "[ $bundle ]に対象ファイルリストを出力しました。"
fi

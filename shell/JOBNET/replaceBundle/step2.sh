#!/bin/bash



# 環境設定
. ./env.sh



# step2
echo ""
echo ""
echo "■ step2 > 置換対象ファイルのバックアップ"
while read years exams before after
do
	count=0
	copies=()

	# 置換前の高校コードファイルを検索
	for file in $(cat $bundle | grep "${before}.csv"); do
		# 配列に追加
		copies+=($file)
		count=$((count+1))
	done

	echo "   対象年度: [ ${years[@]} ] 対象模試: [ ${exams[@]} ] 高校コード:[ ${before} ]  >  ${count} 件バックアップしました。"
	for file in ${copies[@]}; do
		echo "     >  [ ${file} ] -> [ ${backup}/back${file} ]"
		# ファイルをバックアップ
		mkdir -p $backup/back/$(dirname $file)
		cp --parents -p $file $backup/back
		mkdir -p $backup/csv/$(dirname $file)
		cp -p $file $backup/csv$(dirname $file)/${after}.csv
	done
done < $target

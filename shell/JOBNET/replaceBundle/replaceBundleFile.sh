#!/bin/bash


# 環境設定
. ./env.sh


echo "高校コード置換 開始"
echo ""
echo "******************** Environment ********************"
echo "置換対象：       ${path}"
echo "バックアップ先： ${backup}"
echo "******************** Environment ********************"


# step0 前準備
. ./step0.sh 1


# step1
. ./step1.sh 1


# step2
. ./step2.sh 1


# step3
. ./step3.sh 1


# step4
. ./step4.sh 1


# step5
. ./step5.sh 1


echo ""
echo "高校コード置換 終了"

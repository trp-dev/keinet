#!/bin/bash



# 環境設定
. ./env.sh
csv=${src/back/csv}


# step4
echo ""
echo ""
echo "■ step4 > 置換後ファイルのコピー"
while read years exams before after
do
	count=0
	copies=()

	# 置換後の高校コードファイルを検索
	for file in $(find $csv -type f -name "${after}.csv"); do
		# 配列に追加
		copies+=($file)
		count=$((count+1))
	done

	echo "   対象年度: [ ${years[@]} ] 対象模試: [ ${exams[@]} ] 高校コード:[ ${after} ]  >  ${count} 件コピーしました。"
	for file in ${copies[@]}; do
		dest=${file/"${backup}/csv"/}
		# 置換後のファイルをコピー
		cp -ip $file $dest
		echo "     >  [ ${file} ] -> [ ${dest} ]"
	done
done < $target

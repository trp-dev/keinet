------------------------------------------------------------------------------
--  システム名      : KEI-NAVI
--  概念ファイル名  : 科目別成績層別成績（高校）
--  テーブル名      : SUBDEVZONERECORD_S
--  データファイル名: SUBDEVZONERECORD_S
--  機能            : 科目別成績層別成績（高校）をデータファイルの内容に置き換える
--  作成日          : 2009/12/08
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE SUBDEVZONERECORD_S_TMP
(  
   EXAMYEAR        POSITION(01:04)  CHAR,
   EXAMCD          POSITION(05:06)  CHAR,
   BUNDLECD        POSITION(07:11)  CHAR,
   SUBCD           POSITION(12:15)  CHAR,
   DEVZONECD       POSITION(16:16)  CHAR,
   AVGPNT          POSITION(17:21)  INTEGER EXTERNAL "DECODE(:AVGPNT,         '99999', null, :AVGPNT/10)",
   NUMBERS         POSITION(22:29)  INTEGER EXTERNAL "NULLIF(:NUMBERS,     '99999999')",
   AVGSCORERATE    POSITION(30:33)  INTEGER EXTERNAL "DECODE(:AVGSCORERATE,    '9999', null, :AVGSCORERATE/10)"
)


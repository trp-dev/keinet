UPDATE PACTSCHOOL
   SET CERT_CN           = ?
     , CERT_OU           = ?
     , CERT_O            = ?
     , CERT_L            = ?
     , CERT_S            = ?
     , CERT_C            = ?
     , CERT_BEFOREDATA   = ?
     , CERT_FLG          = '1'
     , ISSUED_BEFOREDATE = ?
 WHERE 1=1
   AND USERID = ?

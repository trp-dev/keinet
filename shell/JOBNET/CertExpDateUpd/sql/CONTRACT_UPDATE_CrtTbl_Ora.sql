/* ---------------------------------------------------------------------- */
-- テーブル作成DDL(ORACLE用)
-- ファイル名：CONTRACT_UPDATE_CrtTbl_Ora.sql
-- 物理名：CONTRACT_UPDATE
-- 論理名：契約更新データ
-- 作成日：2015/10/09
/* ---------------------------------------------------------------------- */
CREATE TABLE CONTRACT_UPDATE (
	SCHOOLCD                      	Char      (5    )	-- 学校コード
,	USERID                        	Char      (8    )	-- ユーザID
,	DEFAULTPWD                    	VarChar2  (8    )	-- 初期パスワード
,	USERDIV                       	VarChar2  (1    )	-- ユーザ区分
,	PACTDIV                       	Char      (1    )	-- 契約区分
,	PACTTERM                      	Char      (8    )	-- 契約期限
,	CERT_BEFOREDATA               	VarChar2  (8    )	-- 証明書有効期限
,	MAXSESSION                    	Number    (3    )	-- 最大セッション数
,	FEATPROVFLG                   	Char      (1    )	-- 私書箱機能提供フラグ
,	REGISTRYDATE                  	Char      (8    )	-- 登録日
,	RENEWALDATE                   	Char      (8    )	-- 更新日
,	CERT_OU                       	VarChar2  (16   )	-- 証明書情報OU
,	CERT_O                        	VarChar2  (32   )	-- 証明書情報O
,	CERT_L                        	VarChar2  (32   )	-- 証明書情報L
,	CERT_S                        	VarChar2  (32   )	-- 証明書情報S
,	CERT_C                        	VarChar2  (2    )	-- 証明書情報C
,	CONSTRAINT PK_CONTRACT_UPDATE PRIMARY KEY(
	USERID
	)
) tablespace KEINAVI_DATA;

COMMENT ON TABLE CONTRACT_UPDATE IS '契約更新データ';

COMMENT ON COLUMN CONTRACT_UPDATE.SCHOOLCD                       IS '学校コード';

COMMENT ON COLUMN CONTRACT_UPDATE.USERID                         IS 'ユーザID';

COMMENT ON COLUMN CONTRACT_UPDATE.DEFAULTPWD                     IS '初期パスワード';

COMMENT ON COLUMN CONTRACT_UPDATE.USERDIV                        IS 'ユーザ区分';

COMMENT ON COLUMN CONTRACT_UPDATE.PACTDIV                        IS '契約区分';

COMMENT ON COLUMN CONTRACT_UPDATE.PACTTERM                       IS '契約期限';

COMMENT ON COLUMN CONTRACT_UPDATE.CERT_BEFOREDATA                IS '証明書有効期限';

COMMENT ON COLUMN CONTRACT_UPDATE.MAXSESSION                     IS '最大セッション数';

COMMENT ON COLUMN CONTRACT_UPDATE.FEATPROVFLG                    IS '私書箱機能提供フラグ';

COMMENT ON COLUMN CONTRACT_UPDATE.REGISTRYDATE                   IS '登録日';

COMMENT ON COLUMN CONTRACT_UPDATE.RENEWALDATE                    IS '更新日';

COMMENT ON COLUMN CONTRACT_UPDATE.CERT_OU                        IS '証明書情報OU';

COMMENT ON COLUMN CONTRACT_UPDATE.CERT_O                         IS '証明書情報O';

COMMENT ON COLUMN CONTRACT_UPDATE.CERT_L                         IS '証明書情報L';

COMMENT ON COLUMN CONTRACT_UPDATE.CERT_S                         IS '証明書情報S';

COMMENT ON COLUMN CONTRACT_UPDATE.CERT_C                         IS '証明書情報C';

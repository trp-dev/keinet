/* ---------------------------------------------------------------------- */
-- テーブル作成DDL(ORACLE用)
-- ファイル名：CERT_TARGET_CrtTbl_Ora.sql
-- 物理名：CERT_TARGET
-- 論理名：証明書発行対象データ（一時表）
-- 作成日：2015/10/21
/* ---------------------------------------------------------------------- */
CREATE GLOBAL TEMPORARY TABLE CERT_TARGET (
	LINENO                        	Number    (5    )	-- 明細行番号
,	USERID                        	Char      (8    )	-- ユーザID
) ON COMMIT PRESERVE ROWS;

COMMENT ON TABLE CERT_TARGET IS '証明書発行対象データ';

COMMENT ON COLUMN CERT_TARGET.LINENO                         IS '明細行番号';

COMMENT ON COLUMN CERT_TARGET.USERID                         IS 'ユーザID';

/* ---------------------------------------------------------------------- */
-- テーブル作成DDL(ORACLE用)
-- ファイル名：PACTSCHOOLSAVE_CrtTbl_Ora.sql
-- 物理名：PACTSCHOOLSAVE
-- 論理名：契約校マスタ昨年度
-- 作成日：2015/10/09
/* ---------------------------------------------------------------------- */
CREATE TABLE PACTSCHOOLSAVE (
	USERID                        	Char      (8    )	-- ユーザID
,	SCHOOLCD                      	Char      (5    )	-- 学校コード
,	PACTDIV                       	Char      (1    )	-- 契約区分
,	MAXSESSION                    	Number    (3    )	-- Kei-Navi最大セッション数
,	REALTIMESESSION               	Number    (3    )	-- Kei-Naviリアルタイムセッション数
,	CERT_CN                       	VarChar2  (16   )	-- 証明書情報CN
,	CERT_OU                       	VarChar2  (16   )	-- 証明書情報OU
,	CERT_O                        	VarChar2  (32   )	-- 証明書情報O
,	CERT_L                        	VarChar2  (32   )	-- 証明書情報L
,	CERT_S                        	VarChar2  (32   )	-- 証明書情報S
,	CERT_C                        	VarChar2  (2    )	-- 証明書情報C
,	CERT_BEFOREDATA               	VarChar2  (8    )	-- 証明書有効期限
,	CERT_USERDIV                  	VarChar2  (1    )	-- 証明書ユーザ区分
,	CERT_FLG                      	Char      (1    )	-- 証明書有効フラグ
,	ISSUED_BEFOREDATE             	VarChar2  (8    )	-- 発行済証明書有効期限
,	PACTTERM                      	Char      (8    )	-- 契約期限
,	REGISTRYDATE                  	Char      (8    )	-- 登録日
,	RENEWALDATE                   	Char      (8    )	-- 更新日
,	LOGINPWD                      	VarChar2  (8    )	-- ログインパスワード
,	DEFAULTPWD                    	VarChar2  (8    )	-- 初期パスワード
,	FEATPROVFLG                   	Char      (1    )	-- 私書箱機能提供フラグ
,	KO_MAXSESSION                 	Number    (3    )	-- 校内成績最大セッション数
,	KO_REALTIMESESSION            	Number    (3    )	-- 校内成績リアルタイムセッション数
,	CONSTRAINT PK_PACTSCHOOLSAVE PRIMARY KEY(
	USERID
	)
) tablespace KEINAVI_DATA;

COMMENT ON TABLE PACTSCHOOLSAVE IS '契約校マスタ昨年度';

COMMENT ON COLUMN PACTSCHOOLSAVE.USERID                         IS 'ユーザID';

COMMENT ON COLUMN PACTSCHOOLSAVE.SCHOOLCD                       IS '学校コード';

COMMENT ON COLUMN PACTSCHOOLSAVE.PACTDIV                        IS '契約区分';

COMMENT ON COLUMN PACTSCHOOLSAVE.MAXSESSION                     IS 'Kei-Navi最大セッション数';

COMMENT ON COLUMN PACTSCHOOLSAVE.REALTIMESESSION                IS 'Kei-Naviリアルタイムセッション数';

COMMENT ON COLUMN PACTSCHOOLSAVE.CERT_CN                        IS '証明書情報CN';

COMMENT ON COLUMN PACTSCHOOLSAVE.CERT_OU                        IS '証明書情報OU';

COMMENT ON COLUMN PACTSCHOOLSAVE.CERT_O                         IS '証明書情報O';

COMMENT ON COLUMN PACTSCHOOLSAVE.CERT_L                         IS '証明書情報L';

COMMENT ON COLUMN PACTSCHOOLSAVE.CERT_S                         IS '証明書情報S';

COMMENT ON COLUMN PACTSCHOOLSAVE.CERT_C                         IS '証明書情報C';

COMMENT ON COLUMN PACTSCHOOLSAVE.CERT_BEFOREDATA                IS '証明書有効期限';

COMMENT ON COLUMN PACTSCHOOLSAVE.CERT_USERDIV                   IS '証明書ユーザ区分';

COMMENT ON COLUMN PACTSCHOOLSAVE.CERT_FLG                       IS '証明書有効フラグ';

COMMENT ON COLUMN PACTSCHOOLSAVE.ISSUED_BEFOREDATE              IS '発行済証明書有効期限';

COMMENT ON COLUMN PACTSCHOOLSAVE.PACTTERM                       IS '契約期限';

COMMENT ON COLUMN PACTSCHOOLSAVE.REGISTRYDATE                   IS '登録日';

COMMENT ON COLUMN PACTSCHOOLSAVE.RENEWALDATE                    IS '更新日';

COMMENT ON COLUMN PACTSCHOOLSAVE.LOGINPWD                       IS 'ログインパスワード';

COMMENT ON COLUMN PACTSCHOOLSAVE.DEFAULTPWD                     IS '初期パスワード';

COMMENT ON COLUMN PACTSCHOOLSAVE.FEATPROVFLG                    IS '私書箱機能提供フラグ';

COMMENT ON COLUMN PACTSCHOOLSAVE.KO_MAXSESSION                  IS '校内成績最大セッション数';

COMMENT ON COLUMN PACTSCHOOLSAVE.KO_REALTIMESESSION             IS '校内成績リアルタイムセッション数';

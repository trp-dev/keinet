/* ---------------------------------------------------------------------- */
-- テーブル作成DDL(ORACLE用)
-- ファイル名：CONTRACT_PARAM_CrtTbl_Ora.sql
-- 物理名：CONTRACT_PARAM
-- 論理名：証明書発行パラメータマスタ
-- 作成日：2015/10/09
/* ---------------------------------------------------------------------- */
CREATE TABLE CONTRACT_PARAM (
	PARAJUDGECD                   	Char      (5    )	-- パラメータ識別コード
,	PARAKEY                       	Char      (5    )	-- パラメータキー
,	SETTINGVALUE1                 	NVarChar2 (10   )	-- 設定値１
,	SETTINGVALUE2                 	NVarChar2 (10   )	-- 設定値２
,	SETTINGVALUE3                 	NVarChar2 (10   )	-- 設定値３
,	SETTINGVALUE4                 	NVarChar2 (10   )	-- 設定値４
,	SETTINGVALUE5                 	NVarChar2 (10   )	-- 設定値５
,	CONSTRAINT PK_CONTRACT_PARAM PRIMARY KEY(
	PARAJUDGECD
,	PARAKEY
	)
) tablespace KEINAVI_DATA;

COMMENT ON TABLE CONTRACT_PARAM IS '証明書発行パラメータマスタ';

COMMENT ON COLUMN CONTRACT_PARAM.PARAJUDGECD                    IS 'パラメータ識別コード';

COMMENT ON COLUMN CONTRACT_PARAM.PARAKEY                        IS 'パラメータキー';

COMMENT ON COLUMN CONTRACT_PARAM.SETTINGVALUE1                  IS '設定値１';

COMMENT ON COLUMN CONTRACT_PARAM.SETTINGVALUE2                  IS '設定値２';

COMMENT ON COLUMN CONTRACT_PARAM.SETTINGVALUE3                  IS '設定値３';

COMMENT ON COLUMN CONTRACT_PARAM.SETTINGVALUE4                  IS '設定値４';

COMMENT ON COLUMN CONTRACT_PARAM.SETTINGVALUE5                  IS '設定値５';

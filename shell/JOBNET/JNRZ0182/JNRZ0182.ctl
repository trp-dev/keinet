------------------------------------------------------------------------------
-- *機 能 名 : 志望大学評価別人数（英語認定試験含む）(全国)のCTLファイル
-- *機 能 ID : JNRZ0182.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE RATINGNUMBER_A_ENGPT_TMP
(  
   EXAMYEAR             POSITION(01:04)   CHAR
 , EXAMCD               POSITION(05:06)   CHAR
 , RATINGDIV            POSITION(07:07)   CHAR
 , UNIVCOUNTINGDIV      POSITION(08:08)   CHAR
 , UNIVCD               POSITION(09:12)   CHAR
 , FACULTYCD            POSITION(13:14)   CHAR
 , DEPTCD               POSITION(15:18)   CHAR
 , AGENDACD             POSITION(19:19)   CHAR "NVL(:AGENDACD,' ')"
 , STUDENTDIV           POSITION(20:20)   CHAR
 , AVGDEVIATION         POSITION(21:24)   INTEGER EXTERNAL "DECODE(:AVGDEVIATION,          '9999', null, :AVGDEVIATION/10)"
 , AVGSCORERATE         POSITION(25:28)   INTEGER EXTERNAL "DECODE(:AVGSCORERATE,          '9999', null, :AVGSCORERATE/10)"
 , TOTALCANDIDATENUM    POSITION(29:36)   INTEGER EXTERNAL "NULLIF(:TOTALCANDIDATENUM, '99999999')"
 , FIRSTCANDIDATENUM    POSITION(37:44)   INTEGER EXTERNAL "NULLIF(:FIRSTCANDIDATENUM, '99999999')"
 , RATINGNUM_A          POSITION(45:52)   INTEGER EXTERNAL "NULLIF(:RATINGNUM_A,       '99999999')"
 , RATINGNUM_B          POSITION(53:60)   INTEGER EXTERNAL "NULLIF(:RATINGNUM_B,       '99999999')"
 , RATINGNUM_C          POSITION(61:68)   INTEGER EXTERNAL "NULLIF(:RATINGNUM_C,       '99999999')"
 , RATINGNUM_D          POSITION(69:76)   INTEGER EXTERNAL "NULLIF(:RATINGNUM_D,       '99999999')"
 , RATINGNUM_E          POSITION(77:84)   INTEGER EXTERNAL "NULLIF(:RATINGNUM_E,       '99999999')"
)

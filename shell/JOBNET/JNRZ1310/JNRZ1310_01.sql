-- ************************************************
-- *機 能 名 : 数学記述式設問（クラス）
-- *機 能 ID : JNRZ1310_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE MATHDESCQUE_C_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE MATHDESCQUE_C_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM MATHDESCQUE_C WHERE 0=1;

-- 主キーの設定
ALTER TABLE MATHDESCQUE_C_TMP ADD CONSTRAINT PK_MATHDESCQUE_C_TMP PRIMARY KEY 
(
   EXAMYEAR
 , EXAMCD
 , BUNDLECD
 , GRADE
 , CLASS
 , SUBCD
 , QUESTION_NO
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

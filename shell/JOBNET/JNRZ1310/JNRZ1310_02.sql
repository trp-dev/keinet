-- *********************************************************
-- *機 能 名 : 数学記述式設問（クラス）
-- *機 能 ID : JNRZ1310_02.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM MATHDESCQUE_C MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM MATHDESCQUE_C_TMP TMP
         WHERE 1=1
           AND MAIN.EXAMYEAR     = TMP.EXAMYEAR
           AND MAIN.EXAMCD       = TMP.EXAMCD
           AND MAIN.BUNDLECD     = TMP.BUNDLECD
           AND MAIN.GRADE        = TMP.GRADE
           AND MAIN.CLASS        = TMP.CLASS
           AND MAIN.SUBCD        = TMP.SUBCD
           AND MAIN.QUESTION_NO  = TMP.QUESTION_NO
       )
;

-- テンポラリから本物へ登録
INSERT INTO MATHDESCQUE_C SELECT * FROM MATHDESCQUE_C_TMP;

-- テンポラリテーブル削除
DROP TABLE MATHDESCQUE_C_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

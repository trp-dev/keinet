------------------------------------------------------------------------------
-- *機 能 名 : 数学記述式設問（クラス）のCTLファイル
-- *機 能 ID : JNRZ1310.ctl
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE MATHDESCQUE_C_TMP
(  
   EXAMYEAR    POSITION(01:04)  CHAR 
 , EXAMCD      POSITION(05:06)  CHAR 
 , BUNDLECD    POSITION(07:11)  CHAR 
 , GRADE       POSITION(12:13)  INTEGER EXTERNAL 
 , CLASS       POSITION(14:15)  CHAR "NVL(:CLASS,'  ')"
 , SUBCD       POSITION(16:19)  CHAR 
 , QUESTION_NO POSITION(20:21)  CHAR 
 , AVGPNT      POSITION(22:25)  INTEGER EXTERNAL "DECODE(:AVGPNT,      '9999', null, :AVGPNT/10)"
)

-- *********************************************************
-- *機 能 名 : 到達レベル
-- *機 能 ID : JNRZ0640_02.sql
-- *作 成 日 : 2006/09/05
-- *更 新 日 :
-- *更新内容 :
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 更新対象データを削除
DELETE FROM REACHLEVEL
WHERE  (EXAMYEAR,EXAMCD)
       IN(SELECT DISTINCT EXAMYEAR,EXAMCD
          FROM   REACHLEVEL_TMP);

-- テンポラリテーブルから更新対象データを挿入
INSERT INTO REACHLEVEL
SELECT * FROM REACHLEVEL_TMP;

-- 8年以上前のデータを削除
-- DELETE FROM REACHLEVEL
-- WHERE  TO_NUMBER(EXAMYEAR) <= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 8);

-- 更新対象データの7年前のデータを削除
-- DELETE FROM REACHLEVEL
-- WHERE  (EXAMYEAR,EXAMCD)
--        IN(SELECT DISTINCT (TO_CHAR(TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 7)),EXAMCD
--          FROM   REACHLEVEL_TMP);

-- テンポラリテーブル削除
DROP TABLE REACHLEVEL_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

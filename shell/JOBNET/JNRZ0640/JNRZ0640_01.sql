-- ************************************************
-- *機 能 名 : 到達レベル (受験学力測定テスト用)
-- *機 能 ID : JNRZ0640_01.sql
-- *作 成 日 : 2006/09/05
-- *作 成 者 : kcn)goto
-- *更 新 日 :
-- *更新内容 : 
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE REACHLEVEL_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE REACHLEVEL_TMP  (
       EXAMYEAR        CHAR(4)            NOT NULL,    /*   模試年度        */ 
       EXAMCD          CHAR(2)            NOT NULL,    /*   模試コード      */ 
       SUBCD           CHAR(4)            NOT NULL,    /*   科目コード      */ 
       DEVZONECD       CHAR(2)            NOT NULL,    /*   スコア帯コード  */ 
       REACHLEVEL      CHAR(1)                    ,    /*   到達レベル      */ 
       CONSTRAINT PK_REACHLEVEL_TMP PRIMARY KEY (
                 EXAMYEAR                 ,               /*  模試年度    */
                 EXAMCD                   ,               /*  模試コード  */
                 SUBCD                    ,               /*  科目コード  */
                 DEVZONECD                               /*  スコア帯コード */
       )
       USING INDEX TABLESPACE     KEINAVI_INDX
)
       TABLESPACE       KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;

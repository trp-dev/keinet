------------------------------------------------------------------------------
--  システム名      : KEI-NAVI（受験学力測定テスト対応）
--  概念ファイル名  : 到達レベル
--  テーブル名      : REACHLEVEL
--  データファイル名: REACHLEVEL
--  機能            : 到達レベルをデータファイルの内容に置き換える
--  作成日          : 2006/08/25
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE REACHLEVEL_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
   EXAMYEAR                POSITION(01:04)      CHAR,
   EXAMCD                  POSITION(05:06)      CHAR,
   SUBCD                   POSITION(07:10)      CHAR,
   DEVZONECD               POSITION(11:12)      CHAR,
   REACHLEVEL              POSITION(13:13)      CHAR
)

------------------------------------------------------------------------------
--  システム名      : PC版模試判定バンザイ統合
--  概念ファイル名  : 
--  テーブル名      : DB09_SHKAKU -> CERTIFICATIONFILE
--  機能            : 
--  作成日          : 2014/05/02
--  修正日          : 
--  備考            : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE CERTIFICATIONFILE_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
	YEAR            ,
	UNIVCD          ,
	FACULTYCD       ,
	DEPTCD          ,
	UNIV10CD        ,
	REGISTSECTION   ,
	UNINAME_ABBR    ,
	FACULTYNAME_ABBR,
	DEPTNAME_ABBR   ,
	CERTCODE        ,
	CERTNAME        ,
	CERTCHECKFLG    
)

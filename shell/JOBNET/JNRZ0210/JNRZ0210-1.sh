#!/bin/sh

#環境変数設定
export JOBNAME=JNRZ0210
export CSVFILE=RATINGNUMBER_C
export SRCDIR=/keinavi/HULFT/temp
export WORKDIR=/keinavi/JOBNET/$JOBNAME

#SQL*PLUSテンポラリテーブル作成
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ0210_01.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo テンポラリテーブル作成処理でエラーが発生しました
    exit 30
fi


#SQL*LOADERシェル実行
/keinavi/JOBNET/common/JNRZ0IMP_D.sh

#SQL*LOADER戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo SQL*LOADERでエラーが発生しました
    exit 30
fi


#SQL*PLUSデータインポート->テンポラリテーブル削除
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ0210_02.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo データのインポート処理でエラーが発生しました
    exit 30
fi

#正常終了
echo データのインポート処理が正常に終了しました
exit  0    #正常終了

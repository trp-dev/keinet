------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 志望大学評価別人数（クラス）
--  テーブル名      : RATINGNUMBER_C
--  データファイル名: RATINGNUMBER_C
--  機能            : 志望大学評価別人数（クラス）をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
INSERT PRESERVE BLANKS
INTO TABLE RATINGNUMBER_C_TMP
(  
   EXAMYEAR             POSITION(01:04)  CHAR,
   EXAMCD               POSITION(05:06)  CHAR,
   BUNDLECD             POSITION(07:11)  CHAR,
   GRADE                POSITION(12:13)  INTEGER EXTERNAL,
   CLASS                POSITION(14:15)  CHAR,
   RATINGDIV            POSITION(16:16)  CHAR,
   UNIVCOUNTINGDIV      POSITION(17:17)  CHAR,
   UNIVCD               POSITION(18:21)  CHAR,
   FACULTYCD            POSITION(22:23)  CHAR,
   DEPTCD               POSITION(24:25)  CHAR,
   AGENDACD             POSITION(26:26)  CHAR,
   STUDENTDIV           POSITION(27:27)  CHAR,
   TOTALCANDIDATENUM    POSITION(28:31)  INTEGER EXTERNAL,
   FIRSTCANDIDATENUM    POSITION(32:35)  INTEGER EXTERNAL,
   RATINGNUM_A          POSITION(36:39)  INTEGER EXTERNAL,
   RATINGNUM_B          POSITION(40:43)  INTEGER EXTERNAL,
   RATINGNUM_C          POSITION(44:47)  INTEGER EXTERNAL,
   RATINGNUM_D          POSITION(48:51)  INTEGER EXTERNAL,
   RATINGNUM_E          POSITION(52:55)  INTEGER EXTERNAL
)

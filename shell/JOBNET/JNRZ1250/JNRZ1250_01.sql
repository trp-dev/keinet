-- ************************************************
-- *機 能 名 : 英語認定試験別・合計CEFR取得状況（県）
-- *機 能 ID : JNRZ1250_01.sql
-- *作 成 日 : 2019/08/30
-- *作 成 者 : QQ)西山
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE ENGPT_CEFRACQSTATUS_P_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE ENGPT_CEFRACQSTATUS_P_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM ENGPT_CEFRACQSTATUS_P WHERE 0=1;

-- 主キーの設定
ALTER TABLE ENGPT_CEFRACQSTATUS_P_TMP ADD CONSTRAINT PK_ENGPT_CEFRACQSTATUS_P_TMP PRIMARY KEY 
(
   EXAMYEAR
 , EXAMCD
 , PREFCD
 , ENGPTCD
 , ENGPTLEVELCD
 , CEFRLEVELCD
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

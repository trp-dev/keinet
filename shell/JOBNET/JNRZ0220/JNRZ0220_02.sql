-- *********************************************************
-- *機 能 名 : マーク模試高校別設問別正答率
-- *機 能 ID : JNRZ0220_02.sql
-- *作 成 日 : 2004/08/30
-- *更 新 日 : 2019/09/10
-- *更新内容 : テーブル項目追加対応
-- *********************************************************

-- エラー時はロールバックして終了
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 登録対象データを削除
DELETE
  FROM MARKQSCORERATE MAIN
 WHERE 1=1
   AND EXISTS
       (SELECT 1
          FROM MARKQSCORERATE_TMP TMP
         WHERE 1=1
           AND MAIN.EXAMYEAR    = TMP.EXAMYEAR
           AND MAIN.EXAMCD      = TMP.EXAMCD
           AND MAIN.BUNDLECD   = TMP.BUNDLECD
           AND MAIN.SUBJECTCD    = TMP.SUBJECTCD
           AND MAIN.ANSWER_NO  = TMP.ANSWER_NO
           AND MAIN.CORRECTANSWER       = TMP.CORRECTANSWER
           AND MAIN.PERFRECORDFLG       = TMP.PERFRECORDFLG
           AND MAIN.SORTORDER = TMP.SORTORDER
       )
;

-- テンポラリから本物へ登録
INSERT INTO MARKQSCORERATE SELECT * FROM MARKQSCORERATE_TMP
 WHERE  ((BUNDLECD!='98998')
   AND   (BUNDLECD NOT LIKE '84___')
   AND   (BUNDLECD NOT LIKE '__999'));

-- テンポラリテーブル削除
DROP TABLE MARKQSCORERATE_TMP CASCADE CONSTRAINTS;

-- SQL*PLUS終了
EXIT;

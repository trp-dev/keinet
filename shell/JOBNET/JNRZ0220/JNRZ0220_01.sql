-- ************************************************
-- *機 能 名 : マーク模試高校別設問別正答率
-- *機 能 ID : JNRZ0220_01.sql
-- *作 成 日 : 2004/08/30
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 : 2019/09/10
-- *更新内容 : テーブル項目追加対応
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE MARKQSCORERATE_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE MARKQSCORERATE_TMP TABLESPACE KEINAVI_DATA AS SELECT * FROM MARKQSCORERATE WHERE 0=1;

-- 主キーの設定
ALTER TABLE MARKQSCORERATE_TMP ADD CONSTRAINT PK_MARKQSCORERATE_TMP PRIMARY KEY 
(
   EXAMYEAR
 , EXAMCD
 , BUNDLECD
 , SUBJECTCD
 , ANSWER_NO
 , CORRECTANSWER
 , PERFRECORDFLG
 , SORTORDER
)
USING INDEX TABLESPACE KEINAVI_INDX
;

-- SQL*PLUS終了
EXIT;

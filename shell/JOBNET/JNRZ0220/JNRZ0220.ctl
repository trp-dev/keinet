------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : マーク模試高校設問別正答率
--  テーブル名      : MARKQSCORERATE
--  データファイル名: MARKQSCORERATE
--  機能            : マーク模試高校設問別正答率をデータファイルの内容に置き換える
--  作成日          : 2004/09/23
--  修正日          : 2019/09/10
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
INTO TABLE MARKQSCORERATE_TMP
WHEN ANSWER_NO != ''
(  
   EXAMYEAR      POSITION(01:04)  CHAR 
,  EXAMCD        POSITION(05:06)  CHAR 
,  BUNDLECD      POSITION(07:11)  CHAR 
,  SUBJECTCD     POSITION(12:15)  CHAR 
,  ANSWER_NO     POSITION(16:21)  INTEGER EXTERNAL 
,  CORRECTANSWER POSITION(22:22)  CHAR "NVL(:CORRECTANSWER,' ')"
,  PERFRECORDFLG POSITION(23:23)  CHAR 
,  PERFQFLG      POSITION(24:24)  CHAR 
,  S_SCORERATE   POSITION(25:28)  INTEGER EXTERNAL "DECODE(:S_SCORERATE,   '9999', null, :S_SCORERATE/10)"
,  A_SCORERATE   POSITION(29:32)  INTEGER EXTERNAL "DECODE(:A_SCORERATE,   '9999', null, :A_SCORERATE/10)"
,  ERR1_ANSWER   POSITION(33:33)  CHAR 
,  ERR1_MARKRATE POSITION(34:37)  INTEGER EXTERNAL "DECODE(:ERR1_MARKRATE, '9999', null, :ERR1_MARKRATE/10)"
,  ERR2_ANSWER   POSITION(38:38)  CHAR 
,  ERR2_MARKRATE POSITION(39:42)  INTEGER EXTERNAL "DECODE(:ERR2_MARKRATE, '9999', null, :ERR2_MARKRATE/10)"
,  ERR3_ANSWER   POSITION(43:43)  CHAR 
,  ERR3_MARKRATE POSITION(44:47)  INTEGER EXTERNAL "DECODE(:ERR3_MARKRATE, '9999', null, :ERR3_MARKRATE/10)"
,  SORTORDER     POSITION(48:50)  CHAR 
)

#!/bin/sh

#環境変数設定
export JOBNAME=JNRZ0571
export WORKDIR=/keinavi/JOBNET/$JOBNAME

#大学マスタのもろもろの変更処理を担当する
target_year="2009"
target_examdiv="06"


#SQL*PLUS実行（必須科目配点変更：模試用）
sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @${WORKDIR}/JNRZ0571_01.sql $target_year $target_examdiv
if [ $? -ne 0 ]
then
    echo データのインポート処理でエラーが発生しました
    exit 30
fi


#SQL*PLUS実行（必須科目配点変更：公表用）
sqlplus keinavi_stg/39ra8ma@S11DBA_BATCH2 @${WORKDIR}/JNRZ0571_02.sql $target_year $target_examdiv
if [ $? -ne 0 ]
then
    echo データのインポート処理でエラーが発生しました
    exit 30
fi


echo データのインポート処理が正常に終了しました
exit  0    #正常終了

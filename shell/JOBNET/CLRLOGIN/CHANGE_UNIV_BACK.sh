#!/bin/sh

#環境変数設定
export JOBNAME=CLRLOGIN
export WORKDIR=/keinavi/JOBNET/$JOBNAME

#SQL*PLUS
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/CHANGE_UNIV_BACK.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo 大学マスタの変更に失敗しました
    exit 30
fi

#正常終了
echo 大学マスタを変更しました
exit  0    #正常終了

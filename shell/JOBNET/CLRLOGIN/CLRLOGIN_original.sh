#!/bin/sh

#環境変数設定
export JOBNAME=CLRLOGIN
export WORKDIR=/keinavi/JOBNET/$JOBNAME

#SQL*PLUSログイン情報オールクリア（リアルタイムセッション数／プロファイル開放）
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/CLRLOGIN_01.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    echo ログイン情報のオールクリアに失敗しました
    exit 30
fi

#正常終了
echo ログイン情報をオールクリアしました
exit  0    #正常終了

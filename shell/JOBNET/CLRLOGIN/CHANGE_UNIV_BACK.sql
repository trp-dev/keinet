-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

--
update UNIVMASTER_BASIC
set EVENTYEAR='2013' where EVENTYEAR='2003' and EXAMDIV='01';

commit;

-- SQL*PLUS終了
EXIT;

-- ************************************************
-- *機 能 名 : ログイン情報クリア
-- *機 能 ID : CLRLOGIN_01.sql
-- *作 成 日 : 2004/11/02
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- 契約校マスタ／リアルタイムセッション数クリア
UPDATE PACTSCHOOL SET REALTIMESESSION=0;
UPDATE PACTSCHOOL SET KO_REALTIMESESSION=0;

-- プロファイル／上書き禁止フラグクリア
UPDATE PROFILE SET UPDNGFLG=0;

-- SQL*PLUS終了
EXIT;

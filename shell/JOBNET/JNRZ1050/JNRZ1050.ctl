------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 部門分類マスタ
--  テーブル名      : SECTORSORTING_TMP
--  データファイル名: SECTORSORTINGyyyymmdd.csv
--  機能            : 部門分類マスタをデータファイルの内容に置き換える
--  作成日          : 2004/08/25
--  修正日          : 2004/10/05
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
TRUNCATE
PRESERVE BLANKS
INTO TABLE SECTORSORTING_TMP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    SECTORCD   "DECODE(LENGTH(:SECTORCD), 1, CONCAT('00', :SECTORCD), 2, CONCAT('0', :SECTORCD), 3, :SECTORCD)",
    HSBUSIDIVCD,
    SECTORSORTINGCD  "DECODE(LENGTH(:SECTORSORTINGCD), 1, CONCAT('00', :SECTORSORTINGCD), 2, CONCAT('0', :SECTORSORTINGCD) , 3, :SECTORSORTINGCD)",  
    SECTORNAME
)

-- ************************************************
-- *機 能 名 : 部門分類マスタ
-- *機 能 ID : JNRZ1050_01.sql
-- *作 成 日 : 2004/10/05
-- *作 成 者 : トーテックアメニティ株式会社 
-- *更 新 日 :
-- *更新内容 :
-- ************************************************

-- テンポラリテーブル削除
DROP TABLE SECTORSORTING_TMP CASCADE CONSTRAINTS;

-- エラー時はロールバックして終了
-- (テーブル削除エラーは無視する)
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- テンポラリテーブル作成
CREATE TABLE SECTORSORTING_TMP(   
       SECTORCD              CHAR(3)        NOT NULL,   /*   部門コード               */
       HSBUSIDIVCD           CHAR(1)        NOT NULL,   /*   校舎営業部判別コード     */
       SECTORSORTINGCD       CHAR(3)                ,   /*   部門分類コード           */
       SECTORNAME            VARCHAR(40)            ,   /*   部門名称                 */
    CONSTRAINT PK_SECTORSORTING_TMP PRIMARY KEY(
            SECTORCD,                                   /*   部門コード               */
            HSBUSIDIVCD                                 /*   後者営業部判別コード     */
    )
        USING INDEX TABLESPACE KEINAVI_INDX
)
TABLESPACE KEINAVI_DATA;

-- SQL*PLUS終了
EXIT;
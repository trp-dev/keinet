@echo off

rem ジョブID（パラメータ）
set jobid=%1
set sqlno=%2

rem ↓変更して下さい↓
set based=D:\keinet_project\
rem ↑変更して下さい↑

rem ########################
rem      ORACLE接続情報     
rem      要注意！！！！     
rem ########################
set orastr=KEINAVI/KEINAVI@XE

rem ディレクトリ
set jobdir=%based%workspace\shell\JOBNET\%jobid%
set indir=%based%data\keinavi\IN
set outdir=%based%data\keinavi\OUT

rem 引数チェック
if "%1"=="" ( 
    @echo エラー：引数1が指定されていません > kekka.log
    exit
)
if "%2"=="" ( 
    @echo エラー：引数2が指定されていません > kekka.log
    exit
)

sqlplus %orastr% @%jobdir%\%jobid%_%sqlno%.sql

exit /b 0

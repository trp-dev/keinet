#! /bin/csh 

if (($#argv < 1 ) || (!(($1 == 0)||($1 == -1)||($1 == -2)||($1 == -3))&&($#argv < 3)))then
	echo " ---- param error ----"
	echo " 第1パラメータ:grep校取得先識別フラグ -3,-2,-1,0:ファイル、1:スクリプト内定義(必須)"
	echo " 第2パラメータ:grepフォルダ  (第1パラが0の時は不要)" 
	echo " 第3パラメータ:grep年度+模試コード (第1パラが0の時は不要)"
	echo " 第4パラメータ:grepファイル名 (すべて対象時は不要)"
	exit
endif

set getflg = $1
set filepath = /keinavi/HULFT/NewEXAM/selectioncd
set examconffpath = "/home/navikdc/datastore/inside_release"

set list = ()
set count = 1
set nowyearm = `date +%-m`

#grep対象校先がファイル時 
if ($getflg == 0) then
	#-- 本番データに対して
	(ls ${examconffpath}/2* > tmp.log) >& /dev/null
	set list = `cat tmp.log`
	\rm tmp.log
	if ($#list == 0) then
                echo "---- ERROR： 検証校定義ファイルが見つかりません ----"
		exit 30
	endif
else if ($getflg == -1) then
	#-- 事後・都立換算データに対して
	(ls ${examconffpath}/zigo* > tmp.log) >& /dev/null
        set FileList1 = `cat tmp.log`
	\rm tmp.log
	(ls ${examconffpath}/toritu* > tmp.log) >& /dev/null
        set FileList2 = `cat tmp.log`
	\rm tmp.log
	set list = ($FileList1 $FileList2)
        if ($#list == 0) then
                echo "---- ERROR： 検証校定義ファイルが見つかりません ----"
                exit 30
        endif
else if ($getflg == -2) then
	#-- 得点修正データに対して
	(ls ${examconffpath}/score* > tmp.log) >& /dev/null
        set list = `cat tmp.log`
	\rm tmp.log
        if ($#list == 0) then
                echo "---- ERROR： 検証校定義ファイルが見つかりません ----"
                exit 30
        endif
else if ($getflg == -3) then
	#-- テストデータに対して
	(ls ${examconffpath}/test* > tmp.log) >& /dev/null
	set list = `cat tmp.log`
	\rm tmp.log
	if ($#list == 0) then
		echo "---- ERROR： 検証校定義ファイルが見つかりません ----"
		exit 30
	endif
else if ($getflg == -4) then
	#-- 再処理データに対して
	(ls ${examconffpath}/rerun* > tmp.log) >& /dev/null
	set list = `cat tmp.log`
	\rm tmp.log
	if ($#list == 0) then
		echo "---- ERROR： 検証校定義ファイルが見つかりません ----"
		exit 30
	endif
endif 	
	
#grep対象校先スクリプト時 
if ($getflg == 1) then
	set indir = $2
	set year = $3
	set list = (1)
        set year2 = `echo $year | awk '{printf "%-.4s\n",$0}'`
        set exam2 = `expr $year : ".*\(..\)"`

	##########---- grepの対象校 (必要に応じて高校コードのみ変更) ###########
	##########---- ※第1パラメータが[1]の値のみ有効              ###########
#	set schoolcdlist = (23814 24144 24502 24801 24802 24803 24814)
#	set schoolcdlist = (71092 72411)
#	set schoolcdlist = (24144 24502 24801 24802 24803)
#	set schoolcdlist = (23838 24801 71092 72411)
#	set schoolcdlist = (01101 01102 01104 01107)
#	set schoolcdlist = (24144 24801 24803 24812 71092 72411)
#	set schoolcdlist = (24144 24801 24802 24803 71092 72411)
#	set schoolcdlist = (24803 24801 23838)
#	set schoolcdlist = (23536 23838 71092 72411)
#	set schoolcdlist = (23838 24801 24803 71092 72411)
#	set schoolcdlist = (24144 24801 24803 71092 72411)
#	set schoolcdlist = (24801 24803)
#	set schoolcdlist = (16125 23131 24801)
#	set schoolcdlist = (23838 24801 24803)
#	set schoolcdlist = (21114 23131 24144 24801 24803)
#	set schoolcdlist = (24804)
#       set schoolcdlist = (24801)
#       set schoolcdlist = (01101 01103 23184)
        set schoolcdlist = (24801 24803 24144)

endif

if (-f $filepath/grepexam) then
	\rm $filepath/grepexam
endif

while($count <= $#list)

	set oldindir = 0       ###-- 過年度志望大学評価別人数データの抽出実行フラグ
	set old2indir = 0       ###-- 過年度志望大学評価別人数データの抽出実行フラグ

	set testflg = 0
	if (($getflg == 0)||($getflg == -1)||($getflg == -2)||($getflg == -3)||($getflg == -4)) then
		#-- 抽出対象模試ファイルをリストから取得
		set targetfile = $list[$count]
	        #-- フルファイルパスからファイル名文字列を取得
		set failname = `echo $targetfile | sed -e 's/.*\///g'`
		#-- ファイル名から数字を削除(データ種類文字列取得)
		set examtypstr = `echo $failname | sed -e s'/[0-9]*$//'g`
		if ($getflg == 0) then
			set examtypstr = "release"
		endif
		#-- ファイルパスから対象模試＋模試コード文字列を取得
		set year = `expr $targetfile : ".*\(......\)"`
		set year2 = `echo $year | awk '{printf "%-.4s\n",$0}'`
		set exam2 = `expr $year : ".*\(..\)"`

		##--統合過年度用に1年または2年引く
		if ($getflg == 0) then
			set old1year = `expr $year2 - 1`
			set old2year = `expr $year2 - 2`
		endif

		#-- 抽出対象フォルダを模試ごとに識別
#		if (($exam2 == 61)||($exam2 == 71)) then
#			set indir = ${year2}61_71
#			###-- 統合過年度＆の過年度志望大学登録用に年度を1年または2年引く
#			if ($getflg == 0) then
#				set oldindir = ${old1year}61_71
#				set old2indir = ${old2year}61_71
#			endif
#		else if (($exam2 == 62)||($exam2 == 72)) then
#			set indir = ${year2}62_72
#			###-- 統合過年度＆の過年度志望大学登録用に年度を1年または2年引く
#			if ($getflg == 0) then
#				set oldindir = ${old1year}62_72
#				set old2indir = ${old2year}62_72
#			endif
#		else if (($exam2 == 63)||($exam2 == 73)) then
#			set indir = ${year2}63_73
#			###-- 統合過年度＆の過年度志望大学登録用に年度を1年または2年引く
#			if ($getflg == 0) then
#				set oldindir = ${old1year}63_73
#				set old2indir = ${old2year}63_73
#			endif
#--高12分割	else if (($exam2 == 65)||($exam2 == 75)) then
#			set indir = ${year2}65_75
#			###-- 統合過年度＆の過年度志望大学登録用に年度を1年または2年引く
#			if ($getflg == 0) then
#				set oldindir = ${old1year}65_75
#				set old2indir = ${old2year}65_75
#			endif
#		else if (($exam2 == 28)||($exam2 == 68)||($exam2 == 78)) then
#			set indir = ${year2}28_68_78
#		else if (($exam2 == 37)||($exam2 == 69)||($exam2 == 79)) then
#			set indir = ${year2}37_69_79
#		else
			set indir = $year

			###-- 統合過年度＆の過年度志望大学登録用に年度を1年または2年引く
			if ($getflg == 0) then
				set oldindir = `expr $year - 100`
				set old2indir = `expr $year - 200`
			endif
#		endif

		
		#-- 抽出用対象データフォルダの確認
		if (! -d $indir) then
			set indir = "${indir}${examtypstr}"
			set testflgcount = 0
			if (! -d $indir) then
				#-- (高１・高２などの)模試データが単体できたときのデータフォルダ
				set indir = $year
				if (! -d $indir) then	
					echo "--- ERROR： Grep対象フォルダ $indir がみつかりません ---"
					@ count ++
					continue
				endif
				set testflgcount = 1
			endif
			if ($testflgcount == 0) then 
				set testflg = 1
			endif
		else 
			if (-d ${indir}${examtypstr}) then
					echo "--- ERROR： 未公開の同種類データ(${indir}${examtypstr})が存在します ---"
					echo " 未公開データを公開後、データ登録をおこなってください "
					exit 30
			endif
		endif
		#-- ファイルよりgrep対象校を取得
		set schoolcdlist = `cat $targetfile`
	endif

	#-- 抽出結果出力用フォルダの確認
	set dir = "$indir/${indir}selection"
	if (! -d $dir) then
		mkdir $dir
		if ($status != 0) then
			echo "---- ERROR： フォルダ作成処理にエラーが発生したため処理を中断します。----"
			exit
		endif
	endif
		

	#-- 抽出用の文字列作成
	set count2 = 1
	if ($#schoolcdlist == 0) then
		set orgstr = `echo ""`
		echo " 模試 ${year} のgrep対象校 全件"
	else
		#--- 変数内に改行があった場合は削除
		set orgstr = `echo ${schoolcdlist} | tr -d '\r\n'`
		echo " 模試 ${year} のgrep対象校 $#orgstr校($orgstr)"
	endif
	while($count2 <= $#orgstr)
		set str = $orgstr[$count2]
		#--- 変数内に改行があった場合は削除
#		set str = `echo ${strtem} | tr -d '\r\n'`

		if ($str != "") then
			if ($count2 == 1) then
        	                set gstring = "-e ^${year}$str"
				set gstring2 = "-e ^${year}...........$str"
		
				### --過年度用
				#if ($oldindir != 0) then
				#  	set oldgstring = "-e ^${oldindir}$str"
				#  	set old2gstring = "-e ^${old2indir}$str"
				#endif	
			else
   				set gstring = "${gstring} -e ^${year}$str"
				set gstring2 = "${gstring2} -e ^${year}...........$str"

				### --過年度用
				#if ($oldindir != 0) then
				#   	set oldgstring = "${oldgstring} -e ^${oldindir}$str"
				#   	set old2gstring = "${old2gstring} -e ^${old2indir}$str"
				#endif	
			endif
		endif
                @ count2 ++
	end

	#-- 抽出ファイルの個別指定
	if ($4 != "") then
		if (($4 == "INDIVIDUALRECORD")||($4 == "INDIVIDUALRECORD-old")) then
			echo "grep $gstring2 start"
			grep $gstring2 $indir/$4 >> $dir/INDIVIDUALRECORD
		else
			echo "grep $gstring start"
			grep $gstring $indir/$4 >> $dir/$4
		endif

		echo "grep END"
		exit
	endif

	if (($getflg != -1)&&($getflg != -2)) then
		
		if ((-f $indir/EXAMTAKENUM_A)&&(! -f $dir/EXAMTAKENUM_A)) then
			cp -p $indir/EXAMTAKENUM_A $dir/EXAMTAKENUM_A
		else if ((! -f $indir/EXAMTAKENUM_A)&&($getflg != -4)) then
			echo "-- 模試受験者総数(全国)[EXAMTAKENUM_A]【RZRG1180】が存在しません --" 
		endif
		if ((-f $indir/EXAMTAKENUM_P)&&(! -f $dir/EXAMTAKENUM_P)) then
			cp -p $indir/EXAMTAKENUM_P $dir/EXAMTAKENUM_P
		else if ((! -f $indir/EXAMTAKENUM_P)&&($getflg != -4)) then
			echo "-- 模試受験者総数(県)[EXAMTAKENUM_P]【RZRG1280】が存在しません --" 
		endif
		if ((-f $indir/SUBRECORD_A)&&(! -f $dir/SUBRECORD_A)) then
			cp -p $indir/SUBRECORD_A $dir/SUBRECORD_A
		else if ((! -f $indir/SUBRECORD_A)&&($getflg != -4)) then
			echo "-- 科目別成績(全国)[SUBRECORD_A]【RZRG1110】が存在しません --" 
		endif
		if ((-f $indir/SUBRECORD_P)&&(! -f $dir/SUBRECORD_P)) then
			cp -p $indir/SUBRECORD_P $dir/SUBRECORD_P
		else if ((! -f $indir/SUBRECORD_P)&&($getflg != -4)) then
			echo "-- 科目別成績(県)[SUBRECORD_P]【RZRG1210】が存在しません --" 
		endif
		if ((-f $indir/SUBDISTRECORD_A)&&(! -f $dir/SUBDISTRECORD_A)) then
			cp -p $indir/SUBDISTRECORD_A $dir/SUBDISTRECORD_A
		else if ((! -f $indir/SUBDISTRECORD_A)&&($getflg != -4)) then
			echo "-- 科目分布別成績(全国)[SUBDISTRECORD_A]【RZRG1120】が存在しません --" 
		endif
		if ((-f $indir/SUBDISTRECORD_P)&&(! -f $dir/SUBDISTRECORD_P)) then
			cp -p $indir/SUBDISTRECORD_P $dir/SUBDISTRECORD_P
		else if ((! -f $indir/SUBDISTRECORD_P)&&($getflg != -4)) then
			echo "-- 科目分布別成績(県)[SUBDISTRECORD_P]【RZRG1220】が存在しません --" 
		endif
		if ((-f $indir/SUBDEVZONERECORD_A)&&(! -f $dir/SUBDEVZONERECORD_A)) then
			cp -p $indir/SUBDEVZONERECORD_A $dir/SUBDEVZONERECORD_A
		else if ((! -f $indir/SUBDEVZONERECORD_A)&&($getflg != -4)) then
			echo "-- 科目別成績層別成績(全国)[SUBDEVZONERECORD_A]【RZRG1100】が存在しません --" 
		endif
		if ((-f $indir/QRECORD_A)&&(! -f $dir/QRECORD_A)) then
			cp -p $indir/QRECORD_A $dir/QRECORD_A
		else if ((! -f $indir/QRECORD_A)&&($getflg != -4)) then
			if (($exam2 != 38)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "-- 設問別成績(全国)[QRECORD_A]【RZRG1130】が存在しません --" 
			endif
		endif
		if ((-f $indir/QRECORD_P)&&(! -f $dir/QRECORD_P)) then
			cp -p $indir/QRECORD_P $dir/QRECORD_P
		else if ((! -f $indir/QRECORD_P)&&($getflg != -4)) then
			if (($exam2 != 38)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "-- 設問別成績(県)[QRECORD_P]【RZRG1230】が存在しません --" 
			endif
		endif
		if ((-f $indir/QRECORDZONE_A)&&(! -f $dir/QRECORDZONE_A)) then
			cp -p $indir/QRECORDZONE_A $dir/QRECORDZONE_A
		else if ((! -f $indir/QRECORDZONE_A)&&($getflg != -4)) then
			if (($exam2 != 38)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "-- 設問別成績層別成績(全国)[QRECORDZONE_A]【RZRG1140】が存在しません --" 
			endif
		endif
		if ((-f $indir/MARKQDEVZONEANSRATE_A)&&(! -f $dir/MARKQDEVZONEANSRATE_A)) then
			cp -p $indir/MARKQDEVZONEANSRATE_A $dir/MARKQDEVZONEANSRATE_A
		else if ((! -f $indir/MARKQDEVZONEANSRATE_A)&&($getflg != -4)) then
			if (($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66)) then
				echo "-- マーク模試設問別成績層別正答率(全国)[MARKQDEVZONEANSRATE_A]【RZRG1190】が存在しません --" 
			endif
		endif
		if ((-f $indir/RATINGNUMBER_A)&&(! -f $dir/RATINGNUMBER_A)) then
			cp -p $indir/RATINGNUMBER_A $dir/RATINGNUMBER_A
		else if ((! -f $indir/RATINGNUMBER_A)&&($getflg != -4)) then
			if (($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 65)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 74)&&($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "-- 志望大学評価別人数(全国)[RATINGNUMBER_A]【RZRG1150】が存在しません --" 
			endif
		endif
		if ((-f $indir/RATINGNUMBER_P)&&(! -f $dir/RATINGNUMBER_P)) then
			cp -p $indir/RATINGNUMBER_P $dir/RATINGNUMBER_P
		else if ((! -f $indir/RATINGNUMBER_P)&&($getflg != -4)) then
			if (($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 65)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 74)&&($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "-- 志望大学評価別人数(県)[RATINGNUMBER_P]【RZRG1250】が存在しません --" 
			endif
		endif
		if ((-f $indir/EXAMQUESTION)&&(! -f $dir/EXAMQUESTION)) then
			cp -p $indir/EXAMQUESTION $dir/EXAMQUESTION
		else if ((! -f $indir/EXAMQUESTION)&&($getflg != -4)) then
			if (($exam2 != 38)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
				echo "-- 模試設問マスタ[EXAMQUESTION]【RZRG3310】が存在しません --" 
			endif
		endif
		if ((-f $indir/H12_UNIVMASTER)&&(! -f $dir/H12_UNIVMASTER)) then
			cp -p $indir/H12_UNIVMASTER $dir/H12_UNIVMASTER
		else if ((! -f $indir/H12_UNIVMASTER)&&($getflg != -4)) then
			if (($exam2 == 61)||($exam2 == 62)||($exam2 == 63)||($exam2 == 65)||($exam2 == 71)||($exam2 == 72)||($exam2 == 73)||($exam2 == 74)) then
				echo "-- 高12大学マスタ[H12_UNIVMASTER]【RZRG5210】が存在しません --" 
			endif
		endif
		if ((-f $indir/MARKANSWER_NO)&&(! -f $dir/MARKANSWER_NO)) then
			cp -p $indir/MARKANSWER_NO $dir/MARKANSWER_NO
		else if ((! -f $indir/MARKANSWER_NO)&&($getflg != -4)) then
			if (($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66)) then
			echo "-- マーク模試解答番号マスタ[MARKANSWER_NO]【RZRG3410】が存在しません --" 
			endif
		endif
		if ((-f $indir/CENTERCVSSCORE)&&(! -f $dir/CENTERCVSSCORE)) then
			cp -p $indir/CENTERCVSSCORE $dir/CENTERCVSSCORE
		else if ((! -f $indir/CENTERCVSSCORE)&&($getflg != -4)) then
			if (($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66)) then
			echo "--センター得点換算表 [CENTERCVSSCORE]【RZRG4110】が存在しません --" 
			endif
		endif
		if ((-f $indir/REACHLEVEL)&&(! -f $dir/REACHLEVEL)) then
			cp -p $indir/REACHLEVEL $dir/REACHLEVEL
		else if ((! -f $indir/REACHLEVEL)&&($getflg != -4)) then
			if (($exam2 == 37)||($exam2 == 69)||($exam2 == 79)||($exam2 == 87)||($exam2 == 88)) then
			echo "-- 到達レベル　　[REACHLEVEL]【RZRG4210】が存在しません --" 
			endif
		endif
		if ((-f $indir/CENTER_REACHMARK)&&(! -f $dir/CENTER_REACHMARK)) then
			cp -p $indir/CENTER_REACHMARK $dir/CENTER_REACHMARK
		else if ((! -f $indir/CENTER_REACHMARK)&&($getflg != -4)) then
			if (($exam2 == 37)||($exam2 == 69)||($exam2 == 79)||($exam2 == 87)||($exam2 == 88)) then
			echo "-- センター到達指標　[CENTER_REACHMARK]【RZRG4310】が存在しません --" 
			endif
		endif
		
		#20200207 ADD ST
		
		if ((-f $indir/KWEBSHOQUESTION)&&(! -f $dir/KWEBSHOQUESTION)) then
			cp -p $indir/KWEBSHOQUESTION $dir/KWEBSHOQUESTION
		else if ((! -f $indir/KWEBSHOQUESTION)&&($getflg != -4)) then
			if (($exam2 == 05)||($exam2 == 06)||($exam2 == 07)||($exam2 == 61)||($exam2 == 62)||($exam2 == 63)||($exam2 == 65)||($exam2 == 71)||($exam2 == 72)||($exam2 == 73)||($exam2 == 74)||($exam2 == 10)||($exam2 == 76)||($exam2 == 77)) then
			echo "-- Ｋ－Ｗｅｂ小問マスタデータ　[KWEBSHOQUESTION]【RZRG5580】が存在しません --" 
			endif
		endif
		
		if ((-f $indir/KWEBSHOQUE_ACDMC)&&(! -f $dir/KWEBSHOQUE_ACDMC)) then
			cp -p $indir/KWEBSHOQUE_ACDMC $dir/KWEBSHOQUE_ACDMC
		else if ((! -f $indir/KWEBSHOQUE_ACDMC)&&($getflg != -4)) then
			if (($exam2 == 05)||($exam2 == 06)||($exam2 == 07)||($exam2 == 61)||($exam2 == 62)||($exam2 == 63)||($exam2 == 65)||($exam2 == 71)||($exam2 == 72)||($exam2 == 73)||($exam2 == 74)||($exam2 == 10)||($exam2 == 76)||($exam2 == 77)) then
			echo "-- Ｋ－Ｗｅｂ小問学力要素マスタ　[KWEBSHOQUE_ACDMC]【RZRG5590】が存在しません --" 
			endif
		endif
		if ((-f $indir/SHOQUESTIONRECORD_A)&&(! -f $dir/SHOQUESTIONRECORD_A)) then
			cp -p $indir/SHOQUESTIONRECORD_A $dir/SHOQUESTIONRECORD_A
		else if ((! -f $indir/SHOQUESTIONRECORD_A)&&($getflg != -4)) then
			if (($exam2 == 05)||($exam2 == 06)||($exam2 == 07)||($exam2 == 61)||($exam2 == 62)||($exam2 == 63)||($exam2 == 65)||($exam2 == 71)||($exam2 == 72)||($exam2 == 73)||($exam2 == 74)||($exam2 == 10)||($exam2 == 76)||($exam2 == 77)) then
			echo "-- 記述系模試小問別成績データ（全国）データ　[SHOQUESTIONRECORD_A]【RZRG5600】が存在しません --" 
			endif
		endif

		#20200207 ADD ED
	endif

	if ($#orgstr != 0) then
		echo " grep $gstring Start..."
	else 
		echo " grep 全件コピー Start..."
	endif
	if (-f $indir/EXAMTAKENUM_S) then
		if ($#orgstr == 0) then
			cp -p $indir/EXAMTAKENUM_S $dir/EXAMTAKENUM_S
		else
#			if ((-f $dir/EXAMTAKENUM_S)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 65)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 75)) then
#			if ((-f $dir/EXAMTAKENUM_S)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)) then
				grep $gstring $indir/EXAMTAKENUM_S > $dir/EXAMTAKENUM_S
#			else
#				grep $gstring $indir/EXAMTAKENUM_S >> $dir/EXAMTAKENUM_S
#			endif
		endif
	else
		if (($getflg != -2)&&($getflg != -4)) then
			echo "-- 模試受験者総数(高校)[EXAMTAKENUM_S]【RZRG1380】が存在しません --" 
		endif
	endif
	if (-f $indir/EXAMTAKENUM_C) then
		if ($#orgstr == 0) then
			cp -p $indir/EXAMTAKENUM_C $dir/EXAMTAKENUM_C
		else
#			if ((-f $dir/EXAMTAKENUM_C)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 65)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 75)) then
#			if ((-f $dir/EXAMTAKENUM_C)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)) then
				grep $gstring $indir/EXAMTAKENUM_C > $dir/EXAMTAKENUM_C
#			else
#				grep $gstring $indir/EXAMTAKENUM_C >> $dir/EXAMTAKENUM_C
#			endif
		endif
	else
		if (($getflg != -2)&&($getflg != -4)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
			echo "-- 模試受験者総数(クラス)[EXAMTAKENUM_C]【RZRG1480】が存在しません --" 
		endif
	endif
	if (-f $indir/SUBRECORD_S) then
		if ($#orgstr == 0) then
			cp -p $indir/SUBRECORD_S $dir/SUBRECORD_S
		else
#			if ((-f $dir/SUBRECORD_S)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 65)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 75)) then
#			if ((-f $dir/SUBRECORD_S)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)) then
				grep $gstring $indir/SUBRECORD_S > $dir/SUBRECORD_S
#			else
#				grep $gstring $indir/SUBRECORD_S >> $dir/SUBRECORD_S
#			endif
		endif
	else
		if (($getflg != -2)&&($getflg != -4)) then
			echo "-- 科目別成績(高校)[SUBRECORD_S]【RZRG1310】が存在しません --" 
		endif
	endif
	if (-f $indir/SUBRECORD_C) then
		if ($#orgstr == 0) then
			cp -p $indir/SUBRECORD_C $dir/SUBRECORD_C
		else
#			if ((-f $dir/SUBRECORD_C)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 65)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 75)) then
#			if ((-f $dir/SUBRECORD_C)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)) then
				grep $gstring $indir/SUBRECORD_C > $dir/SUBRECORD_C
#			else
#				grep $gstring $indir/SUBRECORD_C >> $dir/SUBRECORD_C
#			endif
		endif
	else
		if (($getflg != -2)&&($getflg != -4)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
			echo "-- 科目別成績(クラス)[SUBRECORD_C]【RZRG1410】が存在しません --" 
		endif
	endif
	if (-f $indir/QRECORD_S) then
		if ($#orgstr == 0) then
			cp -p $indir/QRECORD_S $dir/QRECORD_S
		else
#			if ((-f $dir/QRECORD_S)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 65)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 75)) then
#			if ((-f $dir/QRECORD_S)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)) then
				grep $gstring $indir/QRECORD_S > $dir/QRECORD_S
#			else
#				grep $gstring $indir/QRECORD_S >> $dir/QRECORD_S
#			endif
		endif
	else
		if (($exam2 != 38)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)&&($getflg != -2)&&($getflg != -4)) then
			echo "-- 設問別成績(高校)[QRECORD_S]【RZRG1330】が存在しません --" 
		endif
	endif
	if (-f $indir/QRECORD_C) then
		if ($#orgstr == 0) then
			cp -p $indir/QRECORD_C $dir/QRECORD_C
		else
#			if ((-f $dir/QRECORD_C)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 65)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 75)) then
#			if ((-f $dir/QRECORD_C)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)) then
				grep $gstring $indir/QRECORD_C > $dir/QRECORD_C
#			else
#				grep $gstring $indir/QRECORD_C >> $dir/QRECORD_C
#			endif
		endif
	else
		if (($exam2 != 38)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)&&($getflg != -2)&&($getflg != -4)) then
			echo "-- 設問別成績(クラス)[QRECORD_C]【RZRG1430】が存在しません --" 
		endif
	endif
	if (-f $indir/QRECORDZONE) then
		if ($#orgstr == 0) then
			cp -p $indir/QRECORDZONE $dir/QRECORDZONE
		else
			grep $gstring $indir/QRECORDZONE >> $dir/QRECORDZONE
		endif
	else
		if (($exam2 != 38)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)&&($getflg != -2)&&($getflg != -4)) then
			echo "-- 設問別成績層別成績[QRECORDZONE]【RZRG1340】が存在しません --" 
		endif
	endif
	if (-f $indir/MARKQDEVZONEANSRATE_S) then
		if ($#orgstr == 0) then
			cp -p $indir/MARKQDEVZONEANSRATE_S $dir/MARKQDEVZONEANSRATE_S
		else
			grep $gstring $indir/MARKQDEVZONEANSRATE_S >> $dir/MARKQDEVZONEANSRATE_S
		endif
	else
		if (($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66)) then
			echo "-- マーク模試設問別成績層別正答率(高校)[MARKQDEVZONEANSRATE_S]【RZRG1390】が存在しません --" 
		endif
	endif
	if (-f $indir/SUBDISTRECORD_S) then
		if ($#orgstr == 0) then
			cp -p $indir/SUBDISTRECORD_S $dir/SUBDISTRECORD_S
		else
			grep $gstring $indir/SUBDISTRECORD_S >> $dir/SUBDISTRECORD_S
		endif
	else
		if (($getflg != -2)&&($getflg != -4)) then
			echo "-- 科目分布別成績(高校)[SUBDISTRECORD_S]【RZRG1320】が存在しません --" 
		endif
	endif
	if (-f $indir/SUBDISTRECORD_C) then
		if ($#orgstr == 0) then
			cp -p $indir/SUBDISTRECORD_C $dir/SUBDISTRECORD_C
		else
			grep $gstring $indir/SUBDISTRECORD_C >> $dir/SUBDISTRECORD_C
		endif
	else
		if (($getflg != -2)&&($getflg != -4)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
			echo "-- 科目分布別成績(クラス)[SUBDISTRECORD_C]【RZRG1420】が存在しません --" 
		endif
	endif
	if (-f $indir/SUBDEVZONERECORD_S) then
		if ($#orgstr == 0) then
			cp -p $indir/SUBDEVZONERECORD_S $dir/SUBDEVZONERECORD_S
		else
			grep $gstring $indir/SUBDEVZONERECORD_S >> $dir/SUBDEVZONERECORD_S
		endif
	else
		if (($getflg != -2)&&($getflg != -4)) then
			echo "-- 科目別成績層別成績(高校)[SUBDEVZONERECORD_S]【RZRG1300】が存在しません --" 
		endif
	endif
	if (-f $indir/RATINGNUMBER_S) then
		if ($#orgstr == 0) then
			cp -p $indir/RATINGNUMBER_S $dir/RATINGNUMBER_S
		else
			grep $gstring $indir/RATINGNUMBER_S >> $dir/RATINGNUMBER_S
		endif
	else
#		if (($getflg != -2)&&($getflg != -4)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 65)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 75)&&($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)) then
		if (($getflg != -2)&&($getflg != -4)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
			echo "-- 志望大学評価別人数(高校)[RATINGNUMBER_S]【RZRG1350】が存在しません --" 
		endif
	endif
	if (-f $indir/RATINGNUMBER_C) then
		if ($#orgstr == 0) then
			cp -p $indir/RATINGNUMBER_C $dir/RATINGNUMBER_C
		else
			grep $gstring $indir/RATINGNUMBER_C >> $dir/RATINGNUMBER_C
		endif
	else
#		if (($getflg != -2)&&($getflg != -4)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 65)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 75)&&($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)) then
		if (($getflg != -2)&&($getflg != -4)&&($exam2 != 61)&&($exam2 != 62)&&($exam2 != 63)&&($exam2 != 71)&&($exam2 != 72)&&($exam2 != 73)&&($exam2 != 28)&&($exam2 != 68)&&($exam2 != 78)&&($exam2 != 37)&&($exam2 != 69)&&($exam2 != 79)&&($exam2 != 87)&&($exam2 != 88)) then
			echo "-- 志望大学評価別人数(クラス)[RATINGNUMBER_C]【RZRG1450】が存在しません --" 
		endif
	endif
	if (-f $indir/MARKQSCORERATE) then
		if ($#orgstr == 0) then
			cp -p $indir/MARKQSCORERATE $dir/MARKQSCORERATE
		else
			grep $gstring $indir/MARKQSCORERATE >> $dir/MARKQSCORERATE
		endif
	else
		if (($getflg != -2)&&($getflg != -4)) then
			if (($exam2 == 01)||($exam2 == 02)||($exam2 == 03)||($exam2 == 04)||($exam2 == 66)) then
				echo "-- マーク模試高校設問別正答率[MARKQSCORERATE]【RZRG1360】が存在しません --" 
			endif
		endif
	endif
	if (-f $indir/INDIVIDUALRECORD) then
		if (($getflg == -1)&&(-f $indir/INDIVIDUALRECORD-old)) then
			if ($examtypstr == zigo) then
				mv $indir/INDIVIDUALRECORD $indir/INDIVIDUALRECORD-zitaku
				mv $indir/INDIVIDUALRECORD-old $indir/INDIVIDUALRECORD
			endif
			if ($exam2 == 10) then
				mv $indir/INDIVIDUALRECORD $indir/INDIVIDUALRECORD_nostore
			endif
		endif
		if ($#orgstr == 0) then
			if (-f $indir/INDIVIDUALRECORD) then
				cp -p $indir/INDIVIDUALRECORD $dir/INDIVIDUALRECORD
			endif
			if (($getflg == -1)&&(-f $indir/INDIVIDUALRECORD-zitaku)) then
				cp -p $indir/INDIVIDUALRECORD-zitaku $dir/INDIVIDUALRECORD-zitaku
			endif
		else
			grep $gstring2 $indir/INDIVIDUALRECORD >> $dir/INDIVIDUALRECORD
		endif
	else
		if ($getflg != -4) then
			echo "-- 個表データ[INDIVIDUALRECORD]【RZRG2110】が存在しません --" 
		endif
	endif
	
	#20200207 ADD ST
	if (-f $indir/SHOQUESTIONRECORD_S) then
		if ($#orgstr == 0) then
			cp -p $indir/SHOQUESTIONRECORD_S $dir/SHOQUESTIONRECORD_S
		else
			grep $gstring $indir/SHOQUESTIONRECORD_S >> $dir/SHOQUESTIONRECORD_S
		endif
	else
		if (($getflg != -2)&&($getflg != -4)) then
			if (($exam2 == 05)||($exam2 == 06)||($exam2 == 07)||($exam2 == 61)||($exam2 == 62)||($exam2 == 63)||($exam2 == 65)||($exam2 == 71)||($exam2 == 72)||($exam2 == 73)||($exam2 == 74)||($exam2 == 10)||($exam2 == 76)||($exam2 == 77)) then
				echo "-- 記述系模試小問別成績データ（高校）[SHOQUESTIONRECORD_S]【RZRG5610】が存在しません --" 
			endif
		endif
	endif
	#20200207 ADD ST

	echo " grep End"


	#-- データがレギュラー時は検証校をログに出力&データ削除用定義作成
	if ($getflg == 0) then
		echo "--- $year 検証校 ---"  >> $filepath/outschoolcd
		if ($#orgstr == 0) then
			echo "    全件    " >> $filepath/outschoolcd
		else
			echo $orgstr >> $filepath/outschoolcd
		endif
		echo " " >> $filepath/outschoolcd


		#-- テストデータ削除用
		if (! ((($exam2 == 37)||($exam2 == 69)||($exam2 == 79)||($exam2 == 87)||($exam2 == 88))&&($nowyearm > 5))) then
			echo "$year" >> $filepath/delexam 
		endif
	endif

	#--データフォルダ名の変更＆登録定義ファイルへの定義
#	if (($getflg == -3)&&($testflg == 0)) then
		#-- データがテスト時ファイル格納フォルダ名を変更
#		mv $dir $indir/${indir}testselection
#		mv $indir ${indir}test
#		echo "${year}test" >> $filepath/grepexam
#	else if (($getflg == -3)&&($testflg == 1)) then
#		echo "${year}test" >> $filepath/grepexam
	if (($getflg != 1)&&($testflg == 1)) then
		echo "${year}${examtypstr}" >> $filepath/grepexam
		echo "${year}${examtypstr}" >> /keinavi/JOBNET/ExamDLup
	else if (($getflg != 1)&&($testflg == 0)) then
		mv $dir $indir/${indir}${examtypstr}selection

		set fdcount = ""
		if (-d ${indir}${examtypstr}) then
			set fdcount = 2
			if (-d ${indir}${examtypstr}2) then
				@ fdcount ++
				if (-d ${indir}${examtypstr}3) then
					@ fdcount ++
				endif
			endif
		endif

		mv $indir ${indir}${examtypstr}${fdcount}
		echo "${year}${examtypstr}${fdcount}" >> $filepath/grepexam
		echo "${year}${examtypstr}${fdcount}" >> /keinavi/JOBNET/ExamDLup
	endif



	###--- 過年度の志望大学評価別人数の抽出
	if ($oldindir != 0) then

		###-- 抽出結果出力用フォルダの確認(1年前)
		set olddir = "$oldindir/${oldindir}selection"
		if (! -d $olddir) then
			mkdir $olddir
			if ($status != 0) then
				echo "---- ERROR：過年度フォルダ（1年前）作成処理にエラーが発生したため処理を中断します。----"
			endif
		endif

		set filecount = 0
	
		echo " "
		echo " 統合過年度データ(1年前) Start..."
		if (-f $oldindir/EXAMTAKENUM_S) then
			cp -p $oldindir/EXAMTAKENUM_S $olddir/EXAMTAKENUM_S
			@ filecount ++
		else if (! -f $oldindir/EXAMTAKENUM_S) then
			echo "-- 過年度(1年前)の模試受験者総数(高校)[EXAMTAKENUM_S]【RZRG1380】が存在しません --" 
		endif
		if (-f $oldindir/SUBRECORD_S) then
			cp -p $oldindir/SUBRECORD_S $olddir/SUBRECORD_S
			@ filecount ++
		else if (! -f $oldindir/SUBRECORD_S) then
			echo "-- 過年度(1年前)の科目別成績(高校)[SUBRECORD_S]【RZRG1310】が存在しません --" 
		endif
		if (-f $oldindir/SUBDISTRECORD_S) then
			cp -p $oldindir/SUBDISTRECORD_S $olddir/SUBDISTRECORD_S
			@ filecount ++
		else if (! -f $oldindir/SUBDISTRECORD_S) then
			echo "-- 過年度(1年前)の科目分布別成績(高校)[SUBDISTRECORD_S]【RZRG1320】が存在しません --" 
		endif
	#	echo " 過年度の志望大学評価別人数データのgrep Start..."
	#
	#	if ((-f $oldindir/RATINGNUMBER_A)&&(! -f $olddir/RATINGNUMBER_A)) then
	#		cp -p $oldindir/RATINGNUMBER_A $olddir/RATINGNUMBER_A
	#		@ filecount ++
	#	else if (! -f $oldindir/RATINGNUMBER_A) then
	#		echo "-- 過年度の志望大学評価別人数(全国)[RATINGNUMBER_A]【RZRG1150】が存在しません --" 
	#	endif
	#	if ((-f $oldindir/RATINGNUMBER_P)&&(! -f $olddir/RATINGNUMBER_P)) then
	#		cp -p $oldindir/RATINGNUMBER_P $olddir/RATINGNUMBER_P
	#		@ filecount ++
	#	else if (! -f $oldindir/RATINGNUMBER_P) then
	#		echo "-- 過年度の志望大学評価別人数(県)[RATINGNUMBER_P]【RZRG1250】が存在しません --" 
	#	endif
	#	if (-f $oldindir/RATINGNUMBER_S) then
	#		if ($#orgstr == 0) then
	#			cp -p $oldindir/RATINGNUMBER_S $olddir/RATINGNUMBER_S
	#		else
	#			grep $oldgstring $oldindir/RATINGNUMBER_S >> $olddir/RATINGNUMBER_S
	#		endif
	#		@ filecount ++
	#	else if (! -f $oldindir/RATINGNUMBER_S) then
	#		echo "-- 過年度の志望大学評価別人数(高校)[RATINGNUMBER_S]【RZRG1350】が存在しません --" 
	#	endif
	#	if (-f $oldindir/RATINGNUMBER_C) then
	#		if ($#orgstr == 0) then
	#			cp -p $oldindir/RATINGNUMBER_C $olddir/RATINGNUMBER_C
	#		else
	#			grep $oldgstring $oldindir/RATINGNUMBER_C >> $olddir/RATINGNUMBER_C
	#		endif
	#		@ filecount ++
	#	else if (! -f $oldindir/RATINGNUMBER_C) then
	#		echo "-- 過年度の志望大学評価別人数(クラス)[RATINGNUMBER_C]【RZRG1450】が存在しません --" 
	#	endif

		echo " 統合過年度データ(2年前) Start..."
		set old2dir = "$old2indir/${old2indir}selection"
		if (! -d $old2dir) then
			mkdir $old2dir
			if ($status != 0) then
				echo "---- ERROR：過年度フォルダ（2年前）作成処理にエラーが発生したため処理を中断します。----"
			endif
		endif
		if (-f $old2indir/EXAMTAKENUM_S) then
			cp -p $old2indir/EXAMTAKENUM_S $old2dir/EXAMTAKENUM_S
			@ filecount ++
		else if (! -f $old2indir/EXAMTAKENUM_S) then
			echo "-- 過年度(2年前)の模試受験者総数(高校)[EXAMTAKENUM_S]【RZRG1380】が存在しません --" 
		endif
		if (-f $old2indir/SUBRECORD_S) then
			cp -p $old2indir/SUBRECORD_S $old2dir/SUBRECORD_S
			@ filecount ++
		else if (! -f $old2indir/SUBRECORD_S) then
			echo "-- 過年度(2年前)の科目別成績(高校)[SUBRECORD_S]【RZRG1310】が存在しません --" 
		endif
		if (-f $old2indir/SUBDISTRECORD_S) then
			cp -p $old2indir/SUBDISTRECORD_S $old2dir/SUBDISTRECORD_S
			@ filecount ++
		else if (! -f $old2indir/SUBDISTRECORD_S) then
			echo "-- 過年度(2年前)の科目分布別成績(高校)[SUBDISTRECORD_S]【RZRG1320】が存在しません --" 
		endif

		#-- 模試データ登録定義ファイルへ模試の定義
		if ($filecount != 0) then
			echo "${old1year}${exam2}" >> $filepath/grepexam
			echo "${old2year}${exam2}" >> $filepath/grepexam
		endif
		echo " grep End"
		echo " "
	endif
	

	
	#-- データ登録定義ファイル（検証校定義ファイル）の削除
	if ($getflg != 1) then
		rm -rf $targetfile
	endif

	@ count ++
end

if (-f $filepath/grepexam) then
	chmod 664 $filepath/grepexam
endif
if (-f /keinavi/JOBNET/ExamDLup) then
	chmod 664 /keinavi/JOBNET/ExamDLup
endif
if (-f $filepath/delexam) then
	chmod 664 $filepath/delexam
endif

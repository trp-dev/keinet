#! /bin/csh 

### HULFT受信ファイル管理スクリプト
### 起動により、ファイルを該当の模試フォルダに振り分けする

set checklog = /keinavi/HULFT/NewEXAM/check.log

if (-f check.log) then
	\rm $checklog
endif

#cd /keinavi/HULFT/NewEXAM/HULFTdata

if ($#argv > 0 ) then
        set list = `ls $1`
else
        set list = `ls`
endif

@ dee = $#list - 5
#echo " ファイル個数　$dee個をチェックします"
echo "ファイル個数　$dee個をチェックします" >> $checklog
chmod 664 $checklog

set univdir = "Univmaster"
set schedulefile = "EXAMSCHEDULEDETAIL*"
set univstemmafile = "UNIVSTEMMA*"
set univmasterinfofile = "UNIVMASTER_INFO*"
set jh01kamoku1 = "JH01_KAMOKU1*"
set jh01kamoku2 = "JH01_KAMOKU2*"
set jh01kamoku3 = "JH01_KAMOKU3*"
set jh01kamoku4 = "JH01_KAMOKU4*"

set count = 1
while ($count <= $#list)

	#---- マスタ判別フラグ定義
	set mflag = 3
	
	#--- スクリプトファイルは対象外 
	if (($list[$count] == check.csh)||($list[$count] == check.csh.org)||($list[$count] == grep.csh)||($list[$count] == grep.csh.org)||($list[$count] == selectiondata-ftp.csh)||($list[$count] == grep--New.csh)) then
		@ count ++
		continue
	endif
	
	#--- ディレクトリは対象外
	if (-d $list[$count]) then
		if (($list[$count] != Univmaster)&&($list[$count] != conf_scripts)&&($list[$count] != "selectioncd")) then
			echo " " >> $checklog
			echo "【 $list[$count] 】ファイルをチェックします" >> $checklog
		endif
		@ count ++
		continue
	endif

	echo " " >> $checklog
	echo "【 $list[$count] 】ファイルをチェックします" >> $checklog

	#---- ファイルが0バイトか確認する
	if (-z $list[$count]) then
		echo " ==== ファイル $list[$count] が0バイトです ===="
		echo " ==== ファイル $list[$count] が0バイトです ====" >> $checklog
		@ count ++
		continue
	endif

	#---- ファイルがEXAMSCHEDULEDETAIL,UNIVSTEMMA,UNIVMASTER_INFOの場合は模試区分で判別できないためスキップ
#	if (($list[$count] =~ SCHEDULEDETAIL*)||($list[$count] =~ UNIVSTEMMA*)) then
	if (($list[$count] =~ $schedulefile)||($list[$count] =~ $univstemmafile)||($list[$count] =~ $univmasterinfofile)) then
		@ count ++
		continue
	endif

	#---- ファイルがJH01_KAMOKU1,JH01_KAMOKU2,JH01_KAMOKU3の場合は模試区分で判別できないためスキップ
	if (($list[$count] =~ $jh01kamoku1)||($list[$count] =~ $jh01kamoku2)||($list[$count] =~ $jh01kamoku3)||($list[$count] =~ $jh01kamoku4)) then
		@ count ++
		continue
	endif

	if ($list[$count] =~ CENTERCVSSCORE*) then
		#--- センター得点換算表
		echo "head $list[$count] コマンド実行" >> $checklog
		set string1 = `head -1  $list[$count] | awk '{printf "%-.4s\n",$0}'`
		set string1 = "20${string1}"
		echo " 行頭 $string1 のデータ " >> $checklog
		echo "tail $list[$count] コマンド実行" >> $checklog
		set string2 = `tail -1 $list[$count]  | awk '{printf "%-.4s\n",$0}'`
		set string2 = "20${string2}"
		echo " 行末 $string2 のデータ " >> $checklog

		set year1 = `echo $string1 | awk '{printf "%-.4s\n",$0}'`
		set year2 = `echo $string2 | awk '{printf "%-.4s\n",$0}'`
		set exam2 = `expr $string2 : ".*\(..\)"`
		
	else if ($list[$count] =~ PREVSUCCESS*) then
		#--- 過年度合格者成績
		echo "head $list[$count] コマンド実行" >> $checklog
		set year1 = `head -1  $list[$count] | awk '{printf "%-.4s\n",$0}'`
		echo " 行頭 $year1 のデータ " >> $checklog
		echo "tail $list[$count] コマンド実行" >> $checklog
		set year2 = `tail -1 $list[$count]  | awk '{printf "%-.4s\n",$0}'`
		echo " 行末 $year2 のデータ " >> $checklog

		set nowyear = `date +%Y`
		@ nowyear --
		if ($year1 != $nowyear) then
			echo " ---- Data ERROR ---- "
			echo " 過年度合格者成績【RZRM1170】の年度値（$year1）に不備があります。"
			@ count ++
			continue
		endif
		@ year1 ++
		@ year2 ++
		set string1 = ${year1}01
		set string2 = ${year2}01
		set exam2 = 01

#	else if ($list[$count] =~ HS12_UNIV*) then
	else if ($list[$count] =~ H12_UNIVMASTER*) then
		#--- 高１２大学マスタ
		echo "head $list[$count] コマンド実行" >> $checklog
		set string1 = `head -1  $list[$count] | awk '{printf "%-.6s\n",$0}'`
		echo " 行頭 $string1 のデータ " >> $checklog
		echo "tail $list[$count] コマンド実行" >> $checklog
		set string2 = `tail -1 $list[$count]  | awk '{printf "%-.6s\n",$0}'`
		echo " 行末 $string2 のデータ " >> $checklog

		if ($string1 != $string2) then
			echo " ---- 行頭と行末で模試が一致しません ----" 
			echo " ---- 行頭と行末で模試が一致しません ----" >> $checklog
			@ count ++
			continue
		endif

		#--- 年度と模試区分を取得
		set year1 = `echo $string1 | awk '{printf "%-.4s\n",$0}'`
		set year2 = `echo $string2 | awk '{printf "%-.4s\n",$0}'`
		set examdiv = `expr $string1 : ".*\(..\)"`
		set examdiv2 = `expr $string2 : ".*\(..\)"`

		#--- 模試区分00のときは年度を-１
		if ($examdiv == 00 ) then
			@ year1 --
			@ year2 --
		endif

		#--- 模試コードより対応模試区分コードを取得
		set ecdanddiv = `grep "$examdiv" /keinavi/JOBNET/exam12cd-div.csv`
		if ($#ecdanddiv == 0) then
			@ count ++
			continue
		endif
		set examtmp = `echo $ecdanddiv | awk -F, '{print $1}'`


		###########--- 模試コードの確定
#		if ($examtmp == "61_71") then
#			set exam = 61
#			set exam2 = 71
#		else if ($examtmp == "62_72") then
#			set exam = 62
#			set exam2 = 72
#		else if ($examtmp == "63_73") then
#			set exam = 63
#			set exam2 = 73
#--高12分割	else if ($examtmp == "65_75") then
		if ($examtmp == "61") then
			set exam = 61
			set exam2 = 61
		else if ($examtmp == "62") then
			set exam = 62
			set exam2 = 62
		else if ($examtmp == "63") then
			set exam = 63
			set exam2 = 63
		else if ($examtmp == "71") then
			set exam = 71
			set exam2 = 71
		else if ($examtmp == "72") then
			set exam = 72
			set exam2 = 72
		else if ($examtmp == "73") then
			set exam = 73
			set exam2 = 73
		else if ($examtmp == "74") then
			set exam = 74
			set exam2 = 74
		else if ($examtmp == "75") then
			set exam = 75
			set exam2 = 75
		else if ($examtmp == "76") then
			set exam = 76
			set exam2 = 76
		else if ($examtmp == "77") then
			set exam = 77
			set exam2 = 77
		endif

		set string1 = "${year1}${exam}"
		set string2 = "${year2}${exam2}"
	
	
#	else if (($list[$count] =~ HS3_UNIV_EXAM*)||($list[$count] =~ HS3_UNIV_PUB*)||($list[$count] =~ HS3_UNIV_NAME*)||($list[$count] =~ IRREGULARFILE*)||($list[$count] =~ UNIVCDTRANS_ORD*)) then
	else if (($list[$count] =~ UNIVMASTER_*)||($list[$count] =~ PUB_UNIVMASTER_*)||($list[$count] =~ UNIVCDTRANS_ORD*)) then
		#--- 各種マスタ
		echo "head $list[$count] コマンド実行" >> $checklog
#		if ($list[$count] =~ IRREGULARFILE*) then
#			set tmpstring = `head -1 $list[$count] | awk '{printf "%-.5s\n",$0}'`
#			set tmpstring2 = `tail -1 $list[$count] | awk '{printf "%-.5s\n",$0}'`
		if ($list[$count] =~ UNIVMASTER_BASIC*) then
			set tmpstring = `head -1 $list[$count] | awk '{printf "%-.6s\n",$0}'`
			set tmpstring2 = `tail -1 $list[$count] | awk '{printf "%-.6s\n",$0}'`
		else if ($list[$count] =~ UNIVMASTER_*) then
			set tmpstring = `head -1 $list[$count] | awk '{printf "%-.17s\n",$0}'`
			set tmpstring2 = `tail -1 $list[$count] | awk '{printf "%-.17s\n",$0}'`
		else if ($list[$count] =~ PUB_UNIVMASTER_BASIC*) then
			set tmpstring = `head -1 $list[$count] | awk '{printf "%-.8s\n",$0}'`
			set tmpstring2 = `tail -1 $list[$count] | awk '{printf "%-.8s\n",$0}'`
		else if ($list[$count] =~ PUB_UNIVMASTER_*) then
			set tmpstring = `head -1 $list[$count] | awk '{printf "%-.25s\n",$0}'`
			set tmpstring2 = `tail -1 $list[$count] | awk '{printf "%-.25s\n",$0}'`
		else if ($list[$count] =~ UNIVCDTRANS_ORD*) then
			set tmpstring = `head -1 $list[$count] | awk '{printf "%-.16s\n",$0}'`
			set tmpstring2 = `tail -1 $list[$count] | awk '{printf "%-.16s\n",$0}'`
		endif

		#--- 年度と模試区分を取得
		set year = `echo $tmpstring | awk '{printf "%-.4s\n",$0}'`
		set year2 = `echo $tmpstring2 | awk '{printf "%-.4s\n",$0}'`
#		if ($list[$count] =~ IRREGULARFILE*) then
#		        set texamdiv = `expr $tmpstring : ".*\(.\)"`
#		        set texamdiv2 = `expr $tmpstring2 : ".*\(.\)"`
#		        set examdiv = "0${texamdiv}"
#		        set examdiv2 = "0${texamdiv2}"
#		else
		        set examdiv = `expr $tmpstring : ".*\(..\)"`
		        set examdiv2 = `expr $tmpstring2 : ".*\(..\)"`
#		endif

		set string1 = "${year}${examdiv}"
		set string2 = "${year2}${examdiv2}"

		echo " 行頭 $string1 のデータ " >> $checklog
		echo "tail $list[$count] コマンド実行" >> $checklog
		echo " 行末 $string2 のデータ " >> $checklog

		#--- ファイルがマスタ系というフラグを定義
		set mflag = 0

		#--- 模試区分の無いEXAMSCHEDULEDETAILとUNIVSTEMMAとUNIVMASTER_INFOを処理
		set ffiles = ()
		(ls ${schedulefile} ${univstemmafile} ${univmasterinfofile} ${jh01kamoku1} ${jh01kamoku2} ${jh01kamoku3} ${jh01kamoku4} > out1.log) >& /dev/null
		set result1 = `cat out1.log`
		\rm out1.log
		if ($#result1 > 0) then
			#--- 対象ファイルのHULFT日付取得
			set tfile = `ls -l --time-style="+%m %d %Y" $list[$count]`
			set mdate = "$tfile[6]$tfile[7]"
			#--- 各ファイルHULFT日付取得
			#--- 複数存在する場合を考慮しループで取得
			set count1 = 1
			while($count1 <= $#result1)
				set times1 = `ls -l --time-style="+%m %d %Y" $result1[$count1]`
				set mtims1 = "$times1[6]$times1[7]"
				if ($mdate == $mtims1) then
					set ffiles = ($ffiles $times1[9])
					set mflag = 1
				endif
				@ count1 ++
			end
		endif
	else
		#--- 各種マスタ以外
		echo "head $list[$count] コマンド実行" >> $checklog
		set string1 = `head -1  $list[$count] | awk '{printf "%-.6s\n",$0}'`
		echo " 行頭 $string1 のデータ " >> $checklog
		echo "tail $list[$count] コマンド実行" >> $checklog
		set string2 = `tail -1 $list[$count]  | awk '{printf "%-.6s\n",$0}'`
		echo " 行末 $string2 のデータ " >> $checklog
		
		set year1 = `echo $string1 | awk '{printf "%-.4s\n",$0}'`
		set year2 = `echo $string2 | awk '{printf "%-.4s\n",$0}'`
		set exam2 = `expr $string2 : ".*\(..\)"`
	endif

	
	#---- ファイルの行頭と行末の比較により1ファイル1模試か判別
	if ($string1 == $string2) then
		echo " 行頭と行末一致により１模試分のデータと確定 " >> $checklog
		
		#----ファイル名からタイムスタンプを削除
		set filename = `echo $list[$count] | sed -e s'/[0-9]\{14\}$//'g`
		
		#---- 特殊模試データ用の模試別フォルダ文字列の作成
		if ($mflag <= 2) then
			#---- 大学マスタ
			set string1 = "${univdir}/${string2}"
#		else
#			if (($list[$count] =~ EXAMQUESTION*)||($list[$count] =~ EXAMSUBJECT*)||($list[$count] =~ REACHLEVEL*)||($list[$count] =~ CENTER_REACHMARK*)) then
#			
#				if (($exam2 == 61)||($exam2 == 71)) then
#					#---- #1高12模試
#					set string1 = "${year2}61_71/${string2}"
#					if (! -d ${year2}61_71) then
#						mkdir ${year2}61_71
#						chmod 777 ${year2}61_71
#					endif
#				else if (($exam2 == 62)||($exam2 == 72)) then
#					#---- #2高12模試
#					set string1 = "${year2}62_72/${string2}"
#					if (! -d ${year2}62_72) then
#						mkdir ${year2}62_72
#						chmod 777 ${year2}62_72
#					endif
#				else if (($exam2 == 63)||($exam2 == 73)) then
#					#---- #3高12模試
#					set string1 = "${year2}63_73/${string2}"
#					if (! -d ${year2}63_73) then
#						mkdir ${year2}63_73
#						chmod 777 ${year2}63_73
#					endif
#				else if (($exam2 == 28)||($exam2 == 68)||($exam2 == 78)) then
#					#---- 新テスト
#					set string1 = "${year2}28_68_78/${string2}"
#					if (! -d ${year2}28_68_78) then
#					mkdir ${year2}28_68_78
#					chmod 777 ${year2}28_68_78
#				endif
#				else if (($exam2 == 37)||($exam2 == 69)||($exam2 == 79)) then
#					#---- 受験学力測定テスト
#					set string1 = "${year2}37_69_79/${string2}"
#					if (! -d ${year2}37_69_79) then
#						mkdir ${year2}37_69_79
#						chmod 777 ${year2}37_69_79
#					endif
#				endif
#			else
#				#---- 受験学力測定は１模試時も固定フォルダとする
#				if (($exam2 == 37)||($exam2 == 69)||($exam2 == 79)) then
#					set string1 = "${year2}37_69_79"
#				endif
#			endif
		endif
		if (! -d $string1) then
			mkdir -p $string1
			chmod 777 $string1
		endif
		
		#---- ファイルの移動（既存ファイル存在時はバックアップ)
		echo " mv $list[$count] $string1/$filename 実行" >> $checklog
		if (-f $string1/$filename) then
			echo " すでにファイル( $string1/$filename )が存在しています。" >> $checklog
			if (-f $string1/${filename}-old) then
				echo " 旧ファイルは( $string1/${filename}-old2 )に変更します" >> $checklog
				mv $string1/${filename}-old $string1/${filename}-old2
			endif
			echo " 旧ファイルは( $string1/${filename}-old )に変更します" >> $checklog
			mv $string1/$filename $string1/${filename}-old
		endif
		mv $list[$count] $string1/$filename
		
		if ($mflag == 1) then
			set num = 1
			while ($num <= $#ffiles)
				set filename2 = `echo $ffiles[$num] | sed -e s'/[0-9]\{14\}$//'g`
				echo " " >> $checklog
				echo " mv $ffiles[$num] $string1/$filename2 実行" >> $checklog
				if (-f $string1/$filename2) then
					echo " すでにファイル( $string1/$filename2 )が存在しています。" >> $checklog
					if (-f $string1/${filename2}-old) then
						echo " 旧ファイルは( $string1/${filename2}-old2)に変更します" >> $checklog
						mv $string1/${filename2}-old $string1/${filename2}-old2
					endif
					echo " 旧ファイルは( $string1/${filename2}-old )に変更します" >> $checklog
					mv $string1/$filename2 $string1/${filename2}-old
				endif
				mv $ffiles[$num] $string1/$filename2
				@ num ++
			end
		endif
		
		#----高12/新テストの模試設問マスタの一元化
#		if ($mflag == 3) then
#			if ((($filename == "EXAMQUESTION"))&&(($exam2 == 61)||($exam2 == 71)||($exam2 == 62)||($exam2 == 72)||($exam2 == 63)||($exam2 == 73)||($exam2 == 65)||($exam2 == 75)||($exam2 == 28)||($exam2 == 68)||($exam2 == 78))) then
#			if ((($filename == "EXAMQUESTION"))&&(($exam2 == 61)||($exam2 == 71)||($exam2 == 62)||($exam2 == 72)||($exam2 == 63)||($exam2 == 73)||($exam2 == 28)||($exam2 == 68)||($exam2 == 78))) then
#				./conf_scripts/examq-out.csh $string1 $year2 $exam2
#			endif
#		endif
		
		#----受験学力測定テストのマスタの一元化
#		if (($filename == "REACHLEVEL")||($filename == "CENTER_REACHMARK")) then
#			./conf_scripts/AchievementTest-out.csh $filename $string1 $year2 $exam2
#		endif
		
		#----模試科目マスタのCSVファイル化
		if ($filename == "EXAMSUBJECT") then
			./conf_scripts/CSVstyle-examsub.pl $string1
		endif
		#----大学名称マスタの00区分時の09区分作成対応
		if (($filename == "UNIVMASTER_BASIC")&&($mflag <= 2)) then
			./conf_scripts/mk09univname.csh /keinavi/HULFT/NewEXAM/$string1 $year $examdiv $filename
		endif
		
		#----大学名称マスタの00区分時の昨年00区分作成対応
		if ($filename == "H12_UNIVMASTER") then
			@ year1 ++       ##00区分時フォルダ形式に年度を1年引いたため
			./conf_scripts/mk09univname.csh /keinavi/HULFT/NewEXAM/$string1 $year1 $examdiv $filename
		endif
		
		#----科目別成績（全国）へのデータ補足
		if ($filename == "EXAMTAKENUM_A") then
			./conf_scripts/examtake_addredord.csh $string1 $year2 $exam2
		endif
		
		#----高12大学マスタの複製処理
		if ($filename == "H12_UNIVMASTER") then
			set ecdanddiv = (`grep "$examdiv" /keinavi/JOBNET/exam12cd-div.csv`)
			set examyear = `echo $string1 | awk '{print substr($0, 0, 4)}'`
			set subcount = 1
			while ($subcount <= $#ecdanddiv)
				set examcd = `echo $ecdanddiv[$subcount] | awk -F, '{print $1}'`
				set dirname = "${examyear}${examcd}"
				if ($string1 != $dirname) then
					if (! -d $dirname) then
						mkdir -p $dirname
						chmod 777 $dirname
					endif
					echo " cp $string1/$filename $dirname/$filename 実行" >> $checklog
					if (-f $dirname/$filename) then
						echo " すでにファイル( $dirname/$filename )が存在しています。" >> $checklog
						if (-f $dirname/${filename}-old) then
							echo " 旧ファイルは( $dirname/${filename}-old2 )に変更します" >> $checklog
							mv $dirname/${filename}-old $dirname/${filename}-old2
						endif
						echo " 旧ファイルは( $dirname/${filename}-old )に変更します" >> $checklog
						mv $dirname/$filename $dirname/${filename}-old
					endif
					cp $string1/$filename $dirname/$filename
				endif
				@ subcount ++
			end
		endif

		@ count ++
		continue
	else
		if ($year1 == $year2) then
			if ((($string1 == ${year1}61)&&($string2 == ${year2}71)) || (($string1 == ${year1}62)&&($string2 == ${year2}72)) || (($string1 == ${year1}63)&&($string2 == ${year2}73))) then
				#---高1,高2模試
				echo " 行頭と行末で高1,高2模試データと確定 " >> $checklog
				set dirname = ${string1}_${exam2}
			else if (($string1 == ${year1}28)&&($string2 == ${year2}78)) then
				#---新テスト
				echo " 行頭と行末で新テスト模試データと確定 " >> $checklog
				set dirname = ${string1}_68_${exam2}
			else if ((($string1 == ${year1}37)&&($string2 == ${year2}79))||(($string1 == ${year1}37)&&($string2 == ${year2}69))) then
				#---受験学力測定テスト
				echo " 行頭と行末で受験学力測定テストデータと確定 " >> $checklog
				set dirname = ${string1}_69_79
			else
				echo " ---- $list[$count] データチェック ----"
				echo " 行頭 $string1 のデータ " 
				echo " 行末 $string2 のデータ "
				echo " ---- 行頭と行末で模試が一致しません ----" 
				
				echo " ---- 行頭と行末で模試が一致しません ----" >> $checklog
				@ count ++
				continue
			endif
			
			if (! -d $dirname) then
				mkdir $dirname
				chmod 777 $dirname
			endif
			
			set filename = `echo $list[$count] | sed -e s'/[0-9]\{14\}$//'g`
			echo " mv $list[$count] $dirname/$filename 実行" >> $checklog
			if (-f $dirname/$filename) then
				echo " すでにファイル( $dirname/$filename )が存在しています。" >> $checklog
				if (-f $dirname/${filename}-old) then
					echo " 旧ファイルは( $dirname/${filename}-old2)に変更します" >> $checklog
					mv $dirname/${filename}-old $dirname/${filename}-old2
				endif
				echo " 旧ファイルは( $dirname/${filename}-old )に変更します" >> $checklog
				mv $dirname/$filename $dirname/${filename}-old
			endif
			mv $list[$count] $dirname/$filename
			
			#----科目別成績（全国）へのデータ補足
			if ($filename == "EXAMTAKENUM_A") then
				./conf_scripts/examtake_addredord.csh $dirname $year2 $exam2
			endif
		
			#----大学名称マスタの00区分時の昨年00区分作成対応
			if ($filename == "H12_UNIVMASTER") then
				@ year1 ++       ##00区分時フォルダ形式に年度を1年引いたため
				./conf_scripts/mk09univname.csh /keinavi/HULFT/NewEXAM/$dirname $year1 $examdiv $filename
			endif
		else
			echo " ---- $list[$count] データチェック ----"
			echo " 行頭 $string1 のデータ " 
			echo " 行末 $string2 のデータ "
			echo " ---- 行頭と行末で模試が一致しません ----" 
			echo " ---- 行頭と行末で模試が一致しません ----" >> $checklog
		endif
	endif
	@ count ++
end

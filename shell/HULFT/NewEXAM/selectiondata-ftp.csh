#! /bin/csh


set NewexamDir = /keinavi/HULFT/NewEXAM
set firstexam = $1
cd $NewexamDir

        set ftpfile = "${firstexam}selection.tar.Z"

        #--- ファイルの圧縮＆転送
        set mvdir = ${NewexamDir}/$firstexam/
        cd $mvdir
        set firstexamse = ${firstexam}selection

	 tar -Zcf ${ftpfile} ${firstexamse}/*
        echo " --- ファイル転送開始..."
        /keinavi/HULFT/ftp.sh /HULFT ${mvdir} ${ftpfile}

        #--- ファイルの展開
        echo " --- 転送完了ファイルの展開開始..."
        /usr/bin/rsh rpve04test /keinavi/JOBNET/decompression.sh /HULFT ${ftpfile}
        if ($status != 0) then
                echo "--- ERROR：リモートシェル実行（転送ファイルの展開）に失敗しました ---"
                \rm $ftpfile
                exit
        endif

        #--- 転送したファイルを登録データ対象フォルダへ配置&削除
        set tardir = "/HULFT/${firstexamse}"
        /usr/bin/rsh rpve04test "cp -p ${tardir}/* /keinavi/HULFT/WorkDir/"
        /usr/bin/rsh rpve04test "\rm -rf ${tardir}"


	#--- 体験版DBへのマスタ登録のため、DB2へのファイルの転送
	#--- ただし、大学マスタが存在する模試のみ
	if (($firstexam == "01")||($firstexam == "02")||($firstexam == "03")||($firstexam == "04")||($firstexam == "05")||($firstexam == "06")||($firstexam == "07")||($firstexam == "38")) then
		/keinavi/HULFT/ftp.sh /keinavi/HULFT/DEMO ${mvdir} ${ftpfile} 10.46.4.2 DB2
	endif


        #--- 圧縮ファイル削除
        \rm -rf ${firstexamse}
        \rm -f ${mvdir}/${ftpfile}

        /usr/bin/rsh rpve04test /keinavi/JOBNET/storelog.sh


#! /bin/csh 

###### 科目別成績（全国）のデータの補足
###### 浪人生レコードの追加

set dir = "/keinavi/HULFT/NewEXAM/$1"
set years = $2
set exams = $3
set files = "EXAMTAKENUM_A"

set lines = `wc -l ${dir}/${files}`
if ($lines[1] == 2) then

	set strin1 = `head -1  ${dir}/${files}`
	set tmpfg1 = `echo $strin1 | awk '{printf "%-.7s\n",$0}'`
	#--現役高卒区分取得
	set fg1 = `expr $tmpfg1 : ".*\(.\)"`
	#--受験人数取得
	set str1 = `expr $strin1 : ".*\(........\)"`

	set strin2 = `tail -1  ${dir}/${files}`
	set tmpfg2 = `echo $strin2 | awk '{printf "%-.7s\n",$0}'`
	#--現役高卒区分取得
	set fg2 = `expr $tmpfg2 : ".*\(.\)"`
	#--受験人数取得
	set str2 = `expr $strin2 : ".*\(........\)"`

	if (($str1 == $str2)&&($fg1 != 2)&&($fg2 != 2)) then
		echo "${years}${exams}200000000" >> ${dir}/${files}
	else if (($str1 == $str2)&&($fg1 != 1)&&($fg2 != 1)) then
		echo "${years}${exams}100000000" >> ${dir}/${files}
	endif

else if ($lines[1] == 4) then
	set count = 1
	while ($count <= 2) 
		if ($count == 1) then
		        set strin1 = `head -1 ${dir}/${files}`
		else
		        set strin1 = `head -3 ${dir}/${files} | tail -1`
		endif
		set yearexam1 = `echo $strin1 | awk '{printf "%-.6s\n",$0}'`
		set tmpfg1 = `echo $strin1 | awk '{printf "%-.7s\n",$0}'`
		#--現役高卒区分取得
		set fg1 = `expr $tmpfg1 : ".*\(.\)"`
		#--受験人数取得
		set str1 = `expr $strin1 : ".*\(........\)"`

		if ($count == 1) then
		        set strin2 = `head -2 ${dir}/${files} | tail -1`
		else 
			set strin2 = `head -4 ${dir}/${files} | tail -1`
		endif
		set yearexam2 = `echo $strin1 | awk '{printf "%-.6s\n",$0}'`
		set tmpfg2 = `echo $strin2 | awk '{printf "%-.7s\n",$0}'`
		#--現役高卒区分取得
		set fg2 = `expr $tmpfg2 : ".*\(.\)"`
		#--受験人数取得
		set str2 = `expr $strin2 : ".*\(........\)"`

		if (($yearexam1 == $yearexam2)&&($str1 == $str2)&&($fg1 != 2)&&($fg2 != 2)) then
			echo "${yearexam1}200000000" >> ${dir}/${files}
		endif
		@ count ++
	end
endif	

#! /bin/csh

####### 大学マスタの03,09区分と昨年00区分作成＆校内成績処理システム用の大学マスタ作成
####### 09区分と昨年00区分の作成は00区分時のみ(Kei-Navi用のみ) 03区分は04区分時のみ
####### 09区分時は校内成績処理システム用に作成

#set dirpath = "/keinavi/HULFT/NewEXAM"
set filedir = $1               ###入出力ファイル格納フォルダ（フルパス）
set examyear = $2              ###マスタの年度
set examdiv = $3               ###マスタの模試区分

set oldyear = $examyear

set File = $4                  ###マスタファイル名(高３用か高１２用かの識別)
set KO_File = "KO_UNIVMASTER_BASIC"
set File00 = "${filedir}/${File}-lastyear00"
set Filediv = "${filedir}/${File}-lastdiv"

## 模試区分が00区分時
if ($examdiv == 00) then
	if (-f $File00) then
		mv $File00 ${File00}-old
	endif
	if (-f $Filediv) then
		mv $Filediv ${Filediv}-old
	endif


	@ oldyear --
	set common = "s/^$examyear$examdiv/$oldyear$examdiv/g"
	set common2 = "s/^$examyear$examdiv/${oldyear}09/g"

	if (-f ${filedir}/${File}) then
	    ## 昨年の00区分作成
		sed -e ${common} ${filedir}/$File  > $File00
		chmod 664 $File00
		
		## 高3大学マスタ時は09区分作成
		if (${File} == "UNIVMASTER_BASIC") then
			sed -e ${common2} ${filedir}/$File  > $Filediv
			chmod 664 $Filediv
		endif

		## 高12大学マスタ時は登録対象マスタを昨年00区分に差し替える
		if (${File} == "H12_UNIVMASTER") then
			mv ${filedir}/${File} ${filedir}/${File}-org
			mv $File00 ${filedir}/${File}
		endif
	endif
endif


## 校内成績処理システム用大学マスタ作成
if (${File} == "UNIVMASTER_BASIC") then

	## 模試区分が04区分時03区分の作成
	if ($examdiv == 04) then
		## すでに前区分マスタが存在するときはファイル名変更し待機しておく
		if (-f $Filediv) then
			mv $Filediv ${Filediv}-old
		endif

		set common3 = "s/^$examyear$examdiv/${examyear}03/g"
		set common2 = "s/^$examyear$examdiv/${examyear}02/g"
		set common1 = "s/^$examyear$examdiv/${examyear}01/g"
		set common0 = "s/^$examyear$examdiv/${examyear}00/g"

		if (-f ${filedir}/${File}) then
			## 03区分のKei-Navi用マスタ作成
			#sed -e ${common3} ${filedir}/$File  > $Filediv
			#chmod 664 $Filediv
			## 03区分の校内成績用のマスタ作成
			sed -e ${common3} ${filedir}/$File  > ${filedir}/${KO_File}-lastdiv
			chmod 664 ${filedir}/${KO_File}-lastdiv
			## その他00区分～02区分の校内成績用のマスタ作成
			sed -e ${common2} ${filedir}/$File  > ${filedir}/${KO_File}-lastdiv2
			chmod 664 ${filedir}/${KO_File}-lastdiv2
			
			sed -e ${common1} ${filedir}/$File  > ${filedir}/${KO_File}-lastdiv3
			chmod 664 ${filedir}/${KO_File}-lastdiv3
			
			sed -e ${common0} ${filedir}/$File  > ${filedir}/${KO_File}-lastdiv4
			chmod 664 ${filedir}/${KO_File}-lastdiv4
		endif
	endif

	if (($examdiv == 04)||($examdiv == 05)||($examdiv == 06)||($examdiv == 07)||($examdiv == 08)) then
		## 模試区分が04区分～08区分時
		cp -p ${filedir}/$File ${filedir}/${KO_File}
	else if ($examdiv == 09) then
		## 模試区分が09区分は校内成績用のみのためファイル名変更
		mv ${filedir}/$File ${filedir}/${KO_File}
 	endif
 	
 	## 校内成績用に"就職",""の2レコードを追加
	set ko_univ = "s/^200501/$examyear$examdiv/g"
	if (-f ${filedir}/${KO_File}) then
		sed -e ${ko_univ} /keinavi/JOBNET/${KO_File}-org  >> ${filedir}/${KO_File}
	endif
endif

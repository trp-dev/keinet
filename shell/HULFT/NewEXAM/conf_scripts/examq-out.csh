#! /bin/csh

##### 模試設問マスタの一元化
##### 高12模試と新テストが対象

set checkdir = "/keinavi/HULFT/NewEXAM"
set OUT_FILE = "EXAMQUESTION"

#--　処理を高12、新テストにあわせ共通化
if (($3 == 61)||($3 == 71)) then
	set ORG_FILE = "$1/EXAMQUESTION"
	if ($3 == 61) then
		set targetexamcd = 71
		set targetexamcd2 = 71
	else
		set targetexamcd = 61
		set targetexamcd2 = 61
	endif
	set basedir = "${checkdir}/${2}61_71"
else if (($3 == 62)||($3 == 72)) then
	set ORG_FILE = "$1/EXAMQUESTION"
	if ($3 == 62) then
		set targetexamcd = 72
		set targetexamcd2 = 72
	else
		set targetexamcd = 62
		set targetexamcd2 = 62
	endif
	set basedir = "${checkdir}/${2}62_72"
else if (($3 == 63)||($3 == 73)) then
	set ORG_FILE = "$1/EXAMQUESTION"
	if ($3 == 63) then
		set targetexamcd = 73
		set targetexamcd2 = 73
	else
		set targetexamcd = 63
		set targetexamcd2 = 63
	endif
	set basedir = "${checkdir}/${2}63_73"
#-- 高12分割
#else if (($3 == 65)||($3 == 75)) then
#	set ORG_FILE = "$1/EXAMQUESTION"
#	if ($3 == 65) then
#		set targetexamcd = 75
#		set targetexamcd2 = 75
#	else
#		set targetexamcd = 65
#		set targetexamcd2 = 65
#	endif
#	set basedir = "${checkdir}/${2}65_75"
else if (($3 == 28)||($3 == 68)||($3 == 78)) then
	set ORG_FILE = "$1/EXAMQUESTION-chg"
	if ($3 == 28) then
		set targetexamcd = 68
		set targetexamcd2 = 78
	else if ($3 == 68) then
		set targetexamcd = 78
		set targetexamcd2 = 28
	else
		set targetexamcd = 28
		set targetexamcd2 = 68
	endif
	set basedir = "${checkdir}/${2}28_68_78"
	${checkdir}/conf_scripts/Qmaster-changeqname.pl ${basedir}/${2}${3}
endif

set otherexam = "${basedir}/${2}${targetexamcd}/${OUT_FILE}"
set otherexam2 = "${basedir}/${2}${targetexamcd2}/${OUT_FILE}"
#echo $otherexam $ORG_FILE

if ((! -f ${otherexam})&&(! -f ${otherexam2})) then
	#-- 他模試の設問マスタが無いとき（模試別のフォルダに入っていないとき）
	cat $ORG_FILE > ${basedir}/${OUT_FILE}
	exit
else if ((! -f ${ORG_FILE}-old)&&(-f ${otherexam})&&(-f ${otherexam2})) then
	#-- 他模試の設問マスタがすべてすでに存在するとき(受信が1回目)
	cat $ORG_FILE >> ${basedir}/${OUT_FILE}
	exit
else if ((! -f ${ORG_FILE}-old)&&((! -f ${otherexam})||(! -f ${otherexam2}))) then
	#-- 他模試の設問マスタどれかがすでに存在するとき
	cat $ORG_FILE >> ${basedir}/${OUT_FILE}
	exit
else if ((-f ${ORG_FILE}-old)&&(! -f ${otherexam}-old)&&(! -f ${otherexam2}-old)) then
	#-- 他模試の設問マスタは受信が1回目に対し、対象模試のマスタは2回目のとき
	cat $ORG_FILE > ${basedir}/${OUT_FILE}
	exit
else if ((-f ${ORG_FILE}-old)&&((-f ${otherexam}-old)||(-f ${otherexam2}-old))) then
	#-- 他模試も対象模試もマスタの2回目受信のとき
	cat $ORG_FILE >> ${basedir}/${OUT_FILE}
endif
	
exit

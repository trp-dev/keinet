#! /bin/csh 

##### 受験学力測定テストマスタ(CENTER_REACHMARK,REACHLEVEL)の一元化
##### 受験学力測定テストが対象
##### 第１パラメータ：ファイル名、第２：年度37_69_79/年度模試コード、第３：年度、第４：模試コード

set checkdir = "/keinavi/HULFT/NewEXAM"
#set OUT_FILE = "REACHLEVEL"
#set OUT_FILE2 = "CENTER_REACHMARK"
set targetDTname = $1

set ORG_FILE = "$2/${targetDTname}"
if ($4 == 37) then
	set targetexamcd = 69
	set targetexamcd2 = 79
else if ($4 == 69) then
	set targetexamcd = 79
	set targetexamcd2 = 37
else
	set targetexamcd = 37
	set targetexamcd2 = 69
endif
set basedir = "${checkdir}/${3}37_69_79"



set otherexam = "${basedir}/${3}${targetexamcd}/${targetDTname}"
set otherexam2 = "${basedir}/${3}${targetexamcd2}/${targetDTname}"
#echo $otherexam $ORG_FILE

#-- 4分前以上に生成されたファイルは削除
find $basedir -mmin +4 -user orauser -name $targetDTname -exec rm -f {} \;

#if ((! -f ${otherexam})&&(! -f ${otherexam2})) then
	#-- 他模試のマスタが無いとき（模試別のフォルダに入っていないとき）
#	cat $ORG_FILE > ${basedir}/${targetDTname}
#	exit
#else if ((! -f ${ORG_FILE}-old)&&(-f ${otherexam})&&(-f ${otherexam2})) then
	#-- 他模試のマスタがすべてすでに存在するとき(受信が1回目)
	cat $ORG_FILE >> ${basedir}/${targetDTname}
	exit
#else if ((! -f ${ORG_FILE}-old)&&((! -f ${otherexam})||(! -f ${otherexam2}))) then
	#-- 他模試のマスタどれかがすでに存在するとき
#	cat $ORG_FILE >> ${basedir}/${targetDTname}
#	exit
#else if ((-f ${ORG_FILE}-old)&&(! -f ${otherexam}-old)&&(! -f ${otherexam2}-old)) then
	#-- 他模試のマスタは受信が1回目に対し、対象模試のマスタは2回目のとき
#	cat $ORG_FILE > ${basedir}/${targetDTname}
#	exit
#else if ((-f ${ORG_FILE}-old)&&((-f ${otherexam}-old)||(-f ${otherexam2}-old))) then
	#-- 他模試も対象模試もマスタの2回目受信のとき
#	cat $ORG_FILE >> ${basedir}/${targetDTname}
#endif
	
exit

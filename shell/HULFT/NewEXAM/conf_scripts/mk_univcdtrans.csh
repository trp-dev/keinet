#! /bin/csh

####### kei-Navi用のマスタ09,03区分作成＆校内成績処理システム用のマスタ作成
####### 09区分時は校内成績処理システム用に作成

set dirpath = "/keinavi/HULFT/NewEXAM"
set filedir = $1               ###ファイル格納フォルダ()
set examyear = $2              ###マスタの年度
set examdiv = $3               ###マスタの模試区分

set oldyear = $examyear

set File = "UNIVCDTRANS_ORD" 
set KO_File = "KO_UNIVCDTRANS_ORD"
set FileP = "${dirpath}/${filedir}"

## 模試区分が00,04区分時（Kei-Navi用）
if (($examdiv == 00)||($examdiv == 04)) then
	if (-f ${FileP}/$File) then
		mv ${FileP}/$File ${FileP}/${File}-old
	endif

	if ($examdiv == 0) then
		@ oldyear --
		set changestr = "s/^$examyear$examdiv/${oldyear}9/g"
	else if ($examdiv == 4) then
		set changestr = "s/^$examyear$examdiv/${examyear}3/g"
	endif

	## kei-navi用のデータを03区分または09区分として登録するために作成
	sed -e ${changestr} ${FileP}/$File-old > ${FileP}/$File
	chmod 664 ${FileP}/$File

endif


## 模試区分が04区分時00区分で作成（校内成績処理システム用）
if ($examdiv == 04) then
	## すでに前区分マスタが存在するときはファイル名変更し待機しておく
	if (-f ${FileP}/${KO_File}) then
		mv ${FileP}/${KO_File} ${FileP}/${KO_File}-old
	endif

	set common = "s/^$examyear$examdiv/${examyear}00/g"

	if (-f ${FileP}/${File}) then
		## 00区分のマスタ作成
		sed -e ${common3} ${FileP}/$File  > ${FileP}/${KO_File}
		chmod 664 $Filediv
	endif
endif

if (($examdiv == 05)||($examdiv == 06)||($examdiv == 07)||($examdiv == 08)) then
	## 模試区分が04区分～08区分時
	cp -p ${FileP}/$File ${FileP}/${KO_File}
else if ($examdiv == 09) then
	## 模試区分が09区分は校内成績用のみのためファイル名変更
	mv ${FileP}/$File ${FileP}/${KO_File}
endif
 	

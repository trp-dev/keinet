#!/usr/bin/perl

#--- 設問マスタの設問内容１（6文字）に設問内容２（16文字）のデータを適用
#--- 新テストのみ対象 (新テストのみ設問内容１は空でデータがホストからくるため)

$ORG_FILE = "@ARGV[0]/EXAMQUESTION";
$OUT_FILE = "@ARGV[0]/EXAMQUESTION-chg";

if(! open(IN, "$ORG_FILE")){
        print "FILE OPEN ERROR!\n";
        exit;
}
if(! open(OUT, ">$OUT_FILE") ){
        close(IN);
        exit;
}

while(<IN>){
        chomp($_);
	#年度、模試コード、科目コード、設問番号、表示番号
        $header  = substr($_,   0, 14); 
        $strrec01 = substr($_,  14, 12);
	# 設問内容２から6文字切り取った文字列
        $strrec02 = substr($_,  26, 12); 
        $strrec03 = substr($_,  26, );

        $outSTR = "$header" .
                $strrec02 .
                $strrec03;
        print OUT "$outSTR\n";
}
close(OUT);
close(IN);

#print "CSV style change OK!!\n";
exit;

#! /bin/csh

####### 大学マスタの03,09区分と昨年00区分作成＆校内成績処理システム用の大学マスタ作成
####### 09区分と昨年00区分の作成は00区分時のみ 03区分は04区分時のみ

set dirpath = "/keinavi/HULFT/NewEXAM"
set filedir = $1
set examyear = $2
set examdiv = $3

set oldyear = $examyear

set File = $4
set KO_File = "KO_HS3_UNIV_NAME"
set File00 = "${dirpath}/${filedir}/${File}-lastyear00"
set Filediv = "${dirpath}/${filedir}/${File}-lastdiv"

if ($examdiv == 00) then
	if (-f $File00) then
		mv $File00 ${File00}-old
	endif
	if (-f $Filediv) then
		mv $Filediv ${Filediv}-old
	endif


	@ oldyear --
	set common = "s/^$examyear$examdiv/$oldyear$examdiv/g"
	set common2 = "s/^$examyear$examdiv/${oldyear}09/g"

	if (-f ${dirpath}/${filedir}/${File}) then
		sed -e ${common} ${dirpath}/${filedir}/$File  > $File00
		chmod 664 $File00
		
		if (${File} == "HS3_UNIV_NAME") then
			sed -e ${common2} ${dirpath}/${filedir}/$File  > $Filediv
			chmod 664 $Filediv
		endif

		if (-f ${File} == "HS12_UNIV") then
			mv ${dirpath}/${filedir}/${File} ${dirpath}/${filedir}/${File}-org
			mv $File00 ${dirpath}/${filedir}/${File}
		endif
	endif
endif

if ($examdiv == 04) then
	if (-f $Filediv) then
		mv $Filediv ${Filediv}-old
	endif

	set common = "s/^$examyear$examdiv/${examyear}03/g"

	if (-f ${dirpath}/${filedir}/${File}) then
		sed -e ${common} ${dirpath}/${filedir}/$File  > $Filediv
		chmod 664 $Filediv
	endif
endif

if (${File} == "HS3_UNIV_NAME") then
	set ko_univ = "s/^200501/$examyear$examdiv/g"
	if (-f ${dirpath}/${filedir}/${File}) then
		sed -e ${ko_univ} /keinavi/JOBNET/${KO_File}-org  > ${dirpath}/${filedir}/${KO_File}
		chmod 664 ${dirpath}/${filedir}/${KO_File}
	endif
endif

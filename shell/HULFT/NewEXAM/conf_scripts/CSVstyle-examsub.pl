#!/usr/bin/perl
$ORG_FILE = "/keinavi/HULFT/NewEXAM/@ARGV[0]/EXAMSUBJECT";
$OUT_FILE = "/keinavi/HULFT/NewEXAM/@ARGV[0]/EXAMSUBJECT.csv";

if(! open(IN, "$ORG_FILE")){
        print "FILE OPEN ERROR!\n";
        exit;
}
if(! open(OUT, ">$OUT_FILE") ){
        close(IN);
        exit;
}

while(<IN>){
        chomp($_);
        $header  = substr($_,   0, 4);
        $subrec01 = substr($_,  4, 2);
        $subrec02 = substr($_,  6, 4);
	if ($subrec02 == "7910") {
		next;
	}	
        $subrec03 = substr($_,  10, 20);
        $subrec04 = substr($_,  30, 6);
        $subrec05 = substr($_,  36, );

        $outSTR = "$header" . ','.
                $subrec01 . ','.
                $subrec02 . ','.
                $subrec03 . ','.
                $subrec04 . ','.
                $subrec05 . ','.
                $subrec02;
        print OUT "$outSTR\n";
}
close(OUT);
close(IN);

#print "CSV style change OK!!\n";
exit;

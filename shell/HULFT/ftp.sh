#! /bin/sh


# パラメータ構成
# 第一：転送先サーバの転送ファイル配置フォルダのフルパス 
# 第二：転送元サーバの転送ファイル配置フォルダのフルパス 
# 第三：転送ファイル名　

if [ $# -lt 3 ]
then
	echo "parameter error"
	exit
fi

echo "$1 $2 $3"

if [ $# = 4 ]
then
	luser="kei-navi"
	pass="39ra8ma"
	hostip=$4
elif [ $# = 5 ]
then
        luser="orauser"
        pass="0ra9le"
        hostip=$4
else 
	luser="orauser"
	pass="0ra9le"
	hostip="10.1.4.218"
fi

ftp -n ${hostip} << EOF
user ${luser} ${pass}
bin
cd $1 
lcd $2
put ${3}*
quit
EOF

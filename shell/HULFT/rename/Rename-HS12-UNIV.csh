#! /bin/csh

set HULFTdir = /keinavi/HULFT/
set FileName = HS12_UNIV
set FilePath = ${HULFTdir}${FileName}

set Da = `date +%Y%m%d%H%M%S`

echo "$Da -----   Rename command Start" >> /keinavi/HULFT/rename/Rename.log

#set ReName = ${FilePath}${Da}
set ReName = ${HULFTdir}NewEXAM/${FileName}${Da}

if (-f $FilePath) then
	echo "$Da HS12_UNIV File Rename Start" >> /keinavi/HULFT/rename/Rename.log
	mv $FilePath $ReName
	exit
endif

echo "$Da  -- Not Find ReName File($FilePath)" >> /keinavi/HULFT/rename/ERR.txt 

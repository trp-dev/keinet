#! /bin/csh

set HULFTdir = /keinavi/HULFT/
set FileName = IRREGULARFILE
set FilePath = ${HULFTdir}${FileName}
set FCopypath = /home/navikdc/PCUnivMaster

set Da = `date +%Y%m%d%H%M%S`
set ymdStr = `date +%Y%m%d`

echo "$Da -----   Rename command Start" >> /keinavi/HULFT/rename/Rename.log

set ReName = ${HULFTdir}NewEXAM/${FileName}${Da}

if (-f $FilePath) then
	if (! -d ${FCopypath}/${ymdStr}) then
		mkdir ${FCopypath}/${ymdStr}
		chmod 775 ${FCopypath}/${ymdStr}
	endif
        if (-f ${FCopypath}/${ymdStr}/${FileName}) then
                mv ${FCopypath}/${ymdStr}/${FileName} ${FCopypath}/${ymdStr}/${FileName}-old
        endif
	cp -p $FilePath ${FCopypath}/${ymdStr}/${FileName}

	echo "$Da $FileName File Rename Start" >> /keinavi/HULFT/rename/Rename.log
	mv $FilePath $ReName
	exit
endif

echo "$Da  -- Not Find ReName File($FilePath)" >> /keinavi/HULFT/rename/ERR.txt 

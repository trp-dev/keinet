#! /bin/csh 

set HULFTdir = /keinavi/HULFT/
set Filedir = /home/navikdc/PDF
set movetmp = "${Filedir}"
#HULFT環境変数から配信ファイル名(フルパス)を取得
set FilePath = $FILENM
set FileName = `echo $FilePath | sed -e s'/^.\{15\}//'g`

set Da = `date +%Y%m%d%H%M%S`
set Day = `date +%Y`
set Dam = `date +%m`


if (-f $FilePath) then
	#旧ファイルの有無確認
	if (-f ${movetmp}/$FileName) then
		#ファイルが存在すれば1世代のみバックアップをとる
		if (-f ${movetmp}/old-${FileName}) then
			\rm ${movetmp}/old-${FileName}
		endif
		mv ${movetmp}/$FileName ${movetmp}/old-${FileName}
	endif
	mv $FilePath ${movetmp}/$FileName

#	#zipファイルの解凍
#	cd ${movetmp}
#	/usr/bin/unzip ${FileName} -d ${movetmp}
#	#rm -f $FileName

	exit
endif


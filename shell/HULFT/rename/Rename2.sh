#! /bin/sh

export HULFTdir=/keinavi/HULFT/
export FilePath=$FILENM
export FileName=`basename $FilePath`

export Da=`date +%Y%m%d%H%M%S`

echo "$Da -----   Rename command Start" >> /keinavi/HULFT/rename/Rename.log

export FReName=${FileName}${Da}

if [ -f $FilePath ]
then
	echo "$Da $FileName File Rename Start" >> /keinavi/HULFT/rename/Rename.log
	
	mv $FilePath ${HULFTdir}${FReName}
	exit
fi

echo "$Da  -- Not Find ReName File($FilePath)" >> /keinavi/HULFT/rename/ERR.txt

#! /bin/csh

set Newdir = /keinavi/HULFT/NewEXAM/
set FilePath = $FILENM
set FileName = `echo $FilePath | sed -e s'/^.\{15\}//'g`

set Da = `date +%Y%m%d%H%M%S`

echo "$Da -----   Rename command Start" >> /keinavi/HULFT/rename/Rename.log

set ReName = ${FileName}${Da}
#set ReName2 = ${FilePath}${Da}$FILEID

if (-f $FilePath) then
	echo "$Da $FileName File Rename Start" >> /keinavi/HULFT/rename/Rename.log

	mv $FilePath ${Newdir}${ReName}
	exit
endif

echo "$Da  -- Not Find ReName File($FilePath)" >> /keinavi/HULFT/rename/ERR.txt 

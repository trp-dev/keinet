#! /bin/csh

set HULFTdir = /keinavi/HULFT/
set FileName = INDIVIDUALRECORD_FD
set FilePath = ${HULFTdir}${FileName}

set Da = `date +%Y%m%d%H%M%S`

echo "$Da -----   Rename command Start" >> /keinavi/HULFT/rename/Rename.log

set ReName = ${FilePath}${Da}

if (-f $FilePath) then
	echo "$Da $FileName File Rename Start" >> /keinavi/HULFT/rename/Rename.log
	mv $FilePath $ReName
	exit
endif

echo "$Da  -- Not Find ReName File($FilePath)" >> /keinavi/HULFT/rename/ERR.txt 

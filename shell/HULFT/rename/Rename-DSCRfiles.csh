#! /bin/csh 

set HULFTdir = /keinavi/HULFT/
set Filedir = ${HULFTdir}dscr_files
#HULFT環境変数から配信ファイル名(フルパス)を取得
set FilePath = $FILENM
set FileName = `echo $FilePath | sed -e s'/^.\{15\}//'g`

set Da = `date +%Y%m%d%H%M%S`
set Day = `date +%Y`
set Dam = `date +%m`

#echo "$Da -----   move command Start" >> /keinavi/HULFT/rename/Rename.log

#set ReName = ${FilePath}${Da}
#set ReName2 = ${FilePath}${Da}$FILEID

if (($Dam == 01)||($Dam == 02)) then
	@ Day --
endif

if (-f $FilePath) then
#	echo "$Da $FileName File move Start" >> /keinavi/HULFT/rename/Rename.log
	set movetmp = ${Filedir}/${Day}-tmp
	#HULFT転送後のファイル移動フォルダの確認
	if (! -d ${movetmp}) then
		mkdir $movetmp
	endif
	#旧ファイルの有無確認
	if (-f ${movetmp}/$FileName) then
		#ファイルが存在すれば1世代のみバックアップをとる
		if (-f ${movetmp}/old-${FileName}) then
			\rm ${movetmp}/old-${FileName}
		endif
		mv ${movetmp}/$FileName ${movetmp}/old-${FileName}
	endif
	mv $FilePath ${movetmp}/$FileName

	echo "$Day" > ${Filedir}/storeflg

	exit
endif

#echo "$Da  -- Not Find ReName File($FilePath)" >> /keinavi/HULFT/rename/ERR.txt 

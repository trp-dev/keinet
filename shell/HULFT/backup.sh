#!/bin/sh


#パラメータチェック
if [ "$1" = "" ]; then
    echo "パラメータ１：年度が未設定です。"
    exit 1
fi
expr "$1" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ１：年度が不正です。【$1】"
    exit 1
fi


#変数定義
year=$1
cur=$2
d_year=$(($year - 2))
b_year=$(($year - 1))
src=/HULFT/keinavi
dst=/u01

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "satu05:模試データ退避処理 start."
echo ""
echo ""

echo "******************************* 確認してください *******************************"
echo ""
echo " step1"
echo "   削除：[$dst/$d_year]"
echo ""
echo " step2"
echo "   作成：[$dst/$year/DATA]"
echo "   退避：[$src/DATA/$b_year*]    -> [$dst/$year/DATA/$b_year*.tar.gz]"
echo ""
echo " step3"
echo "   作成：[$dst/$year/NewEXAM]"
echo "   退避：[$src/NewEXAM/$b_year*] -> [$dst/$year/NewEXAM/$b_year*.tar.gz]"
echo ""
echo "******************************* 確認してください *******************************"
echo ""
echo ""

read -p "処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    echo ""
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "satu05:模試データ退避処理 end."
    exit 1
fi


#step1:前々年のディレクトリを削除
echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "step1 start."
echo ""
rm -rfv $dst/$d_year
echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "step1 end."
echo ""


#step2:DATAを退避
echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "step2 start."
echo ""
#DATAディレクトリ作成＆権限付与
mkdir -pv $dst/$year/DATA
chmod 777 -Rv $dst/$year/DATA

#DATAディレクトリに移動
cur=$src/DATA
cd $cur

#DATAディレクトリを検索
array1=`find $cur -maxdepth 1 -type d -name "$b_year*" -printf '%f\n'`
for a1 in $array1; do
    tar cfz $dst/$year/DATA/$a1.tar.gz ./$a1 --remove-files 
    echo "[$cur/$a1] -> [$dst/$year/DATA/$a1.tar.gz] is successful."
done

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "step2 end."
echo ""


#step3:NewEXAMを退避
echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "step3 start."
echo ""
#NewEXAMディレクトリ作成＆権限付与
mkdir -pv $dst/$year/NewEXAM
chmod 777 -Rv $dst/$year/NewEXAM

#NewEXAMディレクトリに移動
cur=$src/NewEXAM
cd $cur

#NewEXAMディレクトリを検索
array2=`find $cur -maxdepth 1 -type d -name "20*" -printf '%f\n'`
for a2 in $array2; do
    c_year=`date "+%Y" -r ./$a2`
    if [ $year -gt $c_year ]; then
        tar cfz $dst/$year/NewEXAM/$a2.tar.gz ./$a2 --remove-files 
        echo "[$cur/$a2] -> [$dst/$year/NewEXAM/$a2.tar.gz] is successful."
    fi
done

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "step3 end."
echo ""


cd $src
echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "satu05:模試データ退避処理 end."

exit 0

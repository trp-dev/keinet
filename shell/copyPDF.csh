#! /bin/csh 

###### 統計集追加ＰＤＦファイルの本番機アップスクリプト
###### 定義模試分のファイルをファイルサーバからコピー

set nowdate = `date +%Y%m%d`
set resullog = "/home/kei-navi/resul.log"
set outlogs = "/home/kei-navi/copy.log"

if ($#argv <= 0) then
	#公開日
	set opendate = (20061113)
	#年度
	set Dyear = (2006)
	#模試コード
	set Dexam = (06)
else
	set Dyear = $1
	set Dexam = $2
	if ($#Dyear != $#Dexam) then
		echo "--- ERROR：定義ファイルのフォーマットに誤りがあります ---"
		echo "1" > $resullog
		exit 30
	endif
endif

set errsflag = 0
set count = 1
while ($count <= $#Dexam)

	if ($#argv <= 0) then
		echo "現在は $nowdate"
		echo "現在は $nowdate" >> $outlogs
		if ($nowdate != $opendate[$count]) then
			@ count ++
			continue
		endif
	endif
	if (! -d /keinavi/freemenu/statistics) then
		mkdir /keinavi/freemenu/statistics
	endif
	cd /keinavi/freemenu/statistics

	echo "取得 年度：$Dyear[$count], 模試：$Dexam[$count]"
	echo " $nowdate  取得 年度：$Dyear[$count], 模試：$Dexam[$count]" >> $outlogs
	/home/kei-navi/ftp.sh /home/navikdc/PDF/statistics/$Dyear[$count]$Dexam[$count] /keinavi/freemenu/statistics statistics$Dyear[$count]$Dexam[$count].tar 10.1.4.177
	#-- 転送ファイル解凍
	echo " ファイルの展開開始... "
	tar -xf statistics$Dyear[$count]$Dexam[$count].tar 
	echo " ファイルの展開終了＆展開ファイル削除: statistics$Dyear[$count]$Dexam[$count].tar"
	\rm -rf statistics$Dyear[$count]$Dexam[$count].tar

	#-- 2年前データの削除
	set old2year = `expr $Dyear[$count] - 2`
	set pdfdir = `find /keinavi/freemenu/statistics -path "*/${old2year}/$Dexam[$count]" -print`
	set delcount = 1
	while ($delcount <= $#pdfdir)
		if (-d $pdfdir[$delcount]) then
			\rm -rf $pdfdir[$delcount]

			echo " 過年度データ削除 対象：$pdfdir[$delcount]"
			echo " 過年度データ削除 対象：$pdfdir[$delcount]" >> $outlogs
		endif

		@ delcount ++
	end




	@ count ++
end
if ($errsflag == 1) then
	echo "30" > $resullog
	chmod 666 $resullog
	exit 30
endif
echo "0" > $resullog
chmod 666 $resullog
exit 0

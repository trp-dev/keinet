#!/bin/sh

export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.3.7-0.el7_6.x86_64

export LIB_DIR=lib
export CP=bin
export CP=$CP:$LIB_DIR/keinavi.jar
export CP=$CP:$LIB_DIR/totec.jar
export CP=$CP:$LIB_DIR/ojdbc6.jar
export CP=$CP:$LIB_DIR/commons-configuration-1.1.jar
export CP=$CP:$LIB_DIR/commons-logging.jar
export CP=$CP:$LIB_DIR/log4j-1.2.8.jar
export CP=$CP:$LIB_DIR/commons-lang-2.4.jar
export CP=$CP:$LIB_DIR/commons-dbutils-1.0.jar
export CP=$CP:$LIB_DIR/JSPDev.jar

cd /keinavi/bat/Recount/
$JAVA_HOME/bin/java -cp $CP recount.RecountBatch


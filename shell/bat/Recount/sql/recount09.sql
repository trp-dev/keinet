DELETE /*+ ORDERED */ FROM 
	subrecord_c rc
WHERE
	(rc.examyear, rc.examcd, rc.bundlecd, rc.grade, rc.class, rc.subcd) IN (
		SELECT
			re.examyear,
			re.examcd,
			re.bundlecd,
			re.grade,
			re.class,
			re.combisubcd
		FROM
			v_subrecount re
		WHERE
			re.examyear = ?
		AND
			re.examcd = ?
		AND
			re.bundlecd = ?)

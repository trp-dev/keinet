INSERT INTO
	ratingnumber_c
SELECT
	rc.examyear,
	rc.examcd,
	rc.bundlecd,
	rc.grade,
	rc.class,
	rc.ratingdiv,
	rc.univcountingdiv,
	rc.univcd,
	rc.facultycd,
	rc.deptcd,
	rc.agendacd,
	'0',
	rc.totalcandidatenum,
	rc.firstcandidatenum,
	rc.ratingnum_a,
	rc.ratingnum_b,
	rc.ratingnum_c,
	rc.ratingnum_d,
	rc.ratingnum_e
FROM
	ratingnumber_c rc
WHERE
	(rc.examyear, rc.examcd, rc.bundlecd, rc.grade, rc.class, rc.univcd) IN (
		SELECT
			re.year,
			re.examcd,
			re.bundlecd,
			re.grade,
			re.class,
			re.univcd
		FROM
			ratingnumberrecount re
		WHERE
			re.year = ?
		AND
			re.examcd = ?
		AND
			re.bundlecd = ?)

DELETE /*+ ORDERED */ FROM 
	examtakenum_c nc
WHERE
	(nc.examyear, nc.examcd, nc.bundlecd, nc.grade, nc.class) IN (
		SELECT
			re.examyear,
			re.examcd,
			re.bundlecd,
			re.grade,
			re.class
		FROM
			v_subrecount re
		WHERE
			re.examyear = ?
		AND
			re.examcd = ?
		AND
			re.bundlecd = ?)

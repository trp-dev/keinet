DELETE /*+ ORDERED */ FROM
	ratingnumber_c rc
WHERE
	(rc.examyear, rc.examcd, rc.bundlecd, rc.grade, rc.class, rc.univcd) IN (
		SELECT
			re.year,
			re.examcd,
			re.bundlecd,
			re.grade,
			re.class,
			re.univcd
		FROM
			ratingnumberrecount re
		WHERE
			re.year = ?
		AND
			re.examcd = ?
		AND
			re.bundlecd = ?)

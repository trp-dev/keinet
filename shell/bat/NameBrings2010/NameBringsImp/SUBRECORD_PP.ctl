------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 科目別成績（Ｐ保護対象）
--  テーブル名      : SUBRECORD_PP
--  データファイル名: SUBRECORD_PP
--  機能            : 名寄せ処理で作成された科目別成績（Ｐ保護対象）データを取り込む
--  作成日          : 2004/11/09
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
APPEND
INTO TABLE SUBRECORD_PP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
        EXAMYEAR,
        EXAMCD,
        ANSWERSHEET_NO,
        SUBCD,
        SCHOOLCD,
        GRADE,
        CLASS,
        SCORE,
        CVSSCORE,
        S_RANK,
        A_RANK,
        S_DEVIATION,
        A_DEVIATION
)

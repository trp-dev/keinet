------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 志望校評価(個人）
--  テーブル名      : CANDIDATERATING
--  データファイル名: CANDIDATERATING
--  機能            : 名寄せ処理で作成された志望校評価(個人）データを取り込む
--  作成日          : 2004/11/09
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
APPEND
INTO TABLE CANDIDATERATING
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
     INDIVIDUALID,
     EXAMYEAR,
     EXAMCD,
     CANDIDATERANK,
     UNIVCD,
     FACULTYCD,
     DEPTCD,
     BUNDLECD,
     RATINGCVSSCORE,
     RATINGDEVIATION,
     RATINGPOINT,
     CENTERRATING,
     SECONDRATING,
     TOTALRATING,
     TOTALCANDIDATENUM,
     TOTALCANDIDATERANK
)

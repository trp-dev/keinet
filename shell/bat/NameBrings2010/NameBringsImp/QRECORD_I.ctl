------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 設問別成績(個人）
--  テーブル名      : QRECORD_I
--  データファイル名: QRECORD_I
--  機能            : 名寄せ処理で作成された設問別成績(個人）データを取り込む
--  作成日          : 2004/11/09
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
APPEND
INTO TABLE QRECORD_I
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
    INDIVIDUALID,
    EXAMYEAR,
    EXAMCD,
    SUBCD,
    QUESTION_NO,
    BUNDLECD,
    QSCORE,
    A_DEVIATION,
    SCORERATE
)

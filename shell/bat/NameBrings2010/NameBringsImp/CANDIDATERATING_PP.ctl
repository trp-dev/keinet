------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 志望校評価（Ｐ保護対象）
--  テーブル名      : CANDIDATERATING_PP
--  データファイル名: CANDIDATERATING_PP
--  機能            : 名寄せ処理で作成された志望校評価（Ｐ保護対象）データを取り込む
--  作成日          : 2004/11/09
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
APPEND
INTO TABLE CANDIDATERATING_PP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
        EXAMYEAR,
        EXAMCD,
        ANSWERSHEET_NO,
        CANDIDATERANK,
        UNIVCD,
        FACULTYCD,
        DEPTCD,
        SCHOOLCD,
        GRADE,
        CLASS,
        RATINGCVSSCORE,
        RATINGDEVIATION,
        RATINGPOINT,
        CENTERRATING,
        SECONDRATING,
        TOTALRATING,
        TOTALCANDIDATENUM,
        TOTALCANDIDATERANK
)

------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 設問別成績（Ｐ保護対象）
--  テーブル名      : QRECORD_PP
--  データファイル名: QRECORD_PP
--  機能            : 名寄せ処理で作成された設問別成績（Ｐ保護対象）データを取り込む
--  作成日          : 2004/11/09
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
APPEND
INTO TABLE QRECORD_PP
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
       EXAMYEAR,
       EXAMCD,
       ANSWERSHEET_NO,
       SUBCD,
       QUESTION_NO,
       SCHOOLCD,
       GRADE,
       CLASS,
       QSCORE,
       A_DEVIATION,
       SCORERATE
)

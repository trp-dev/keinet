------------------------------------------------------------------------------
--  システム名      : KEI-NETサービスウェアリニューアル
--  概念ファイル名  : 科目別成績(個人）
--  テーブル名      : SUBRECORD_I
--  データファイル名: SUBRECORD_I
--  機能            : 名寄せ処理で作成された科目別成績(個人）データを取り込む
--  作成日          : 2004/11/09
--  修正日          : 
------------------------------------------------------------------------------
LOAD DATA
CHARACTERSET JA16SJIS
APPEND
INTO TABLE SUBRECORD_I
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  
       INDIVIDUALID,
       EXAMYEAR,
       EXAMCD,
       SUBCD,
       BUNDLECD,
       SCORE,
       CVSSCORE,
       S_RANK,
       A_RANK,
       S_DEVIATION,
       A_DEVIATION
)

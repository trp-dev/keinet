#!/bin/sh

export JAVA=/usr/lib/jvm/java-11-openjdk-11.0.3.7-0.el7_6.x86_64/bin
export CLASSPATH=/keinavi/bat/NameBrings2010/keinavi/WEB-INF/classes:/keinavi/bat/NameBrings2010/keinavi/WEB-INF/lib/classes12.jar:/keinavi/bat/NameBrings2010/keinavi/WEB-INF/lib/JSPDev.jar:/keinavi/bat/NameBrings2010/keinavi/WEB-INF/lib/commons-dbutils-1.0.jar

cd /keinavi/bat/NameBrings2010/keinavi/
/usr/lib/jvm/java-11-openjdk-11.0.3.7-0.el7_6.x86_64/bin/java jp.co.fj.keinavi.beans.admin.recount.ReCountBean

chmod 664 /keinavi/bat/NameBrings2010/log/*

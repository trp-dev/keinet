#!/bin/sh

export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.3.7-0.el7_6.x86_64

export CLASSPATH=/keinavi/bat/createProfile/keinavi/WEB-INF/classes:/keinavi/bat/createProfile/keinavi/WEB-INF/lib/ojdbc6.jar:/keinavi/bat/createProfile/keinavi/WEB-INF/lib/JSPDev.jar:/keinavi/bat/createProfile/keinavi/WEB-INF/lib/commons-dbutils-1.0.jar:/keinavi/bat/createProfile/keinavi/WEB-INF/lib/commons-lang-2.0.jar:/keinavi/bat/createProfile/keinavi/WEB-INF/lib/servlet-api.jar:/keinavi/bat/createProfile/keinavi/WEB-INF/lib/totec-1.0.jar:/keinavi/bat/createProfile/keinavi/WEB-INF/lib/commons-configuration-1.1.jar:/keinavi/bat/createProfile/keinavi/WEB-INF/lib/*
cd /keinavi/bat/createProfile/keinavi/

$JAVA_HOME/bin/java -Xmx256m -Xms256m jp.co.fj.keinavi.beans.profile.ProfileCopyBean $1 


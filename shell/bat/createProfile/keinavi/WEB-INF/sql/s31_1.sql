SELECT
	NVL(numbers, -999),
	NVL(avgpnt, -999),
	NVL(avgdeviation, -999)
FROM
	subrecord_s
WHERE
	examyear = ?
AND
	examcd = ?
AND
	bundlecd = ?
AND
	subcd = ?

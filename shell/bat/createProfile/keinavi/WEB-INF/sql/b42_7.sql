SELECT
	/* CEFRîñ.bdeqxR[h */
	ci.cefrlevelcd,
	/* CEFRîñ.bdeqxZk¼ */
	CASE WHEN ci.cefrlevelcd = '99' THEN 'Èµ' ELSE ci.cerflevelname_abbr END AS cerflevelname_abbr,
	/* CEFRîñ.ÀÑ */
	ci.sort AS ci_sort,
	/* ¼Ì */
	'iSj' AS bundlename,
	/* pêFè±ÊEvCEFRæ¾óµiSj.l */
	ecasa.numbers AS s_numbers,
	/* pêFè±ÊEvCEFRæ¾óµiSj.\¬ä */
	ecasa.compratio AS s_compratio
FROM
	/* Í}X^TBL */
	examination en
	/* CEFRîñTBL */
	INNER JOIN (
		SELECT
			ci.eventyear,
			ci.examdiv,
			ci.cefrlevelcd,
			ci.cerflevelname_abbr,
			ci.sort
		FROM
			cefr_info ci
		UNION ALL
		SELECT
			ci.eventyear,
			ci.examdiv,
			'ZZ' AS cefrlevelcd,
			'v' AS cerflevelname_abbr,
			-1 AS sort
		FROM
			cefr_info ci
		GROUP BY
			ci.eventyear,
			ci.examdiv) ci
	ON
		ci.eventyear = en.examyear
	AND
		ci.examdiv = en.examdiv
	/* pêFè±ÊEvCEFRæ¾óµiSjTBL */
	LEFT JOIN engpt_cefracqstatus_a ecasa
	ON
		ecasa.examyear = en.examyear
	AND
		ecasa.examcd = en.examcd
	AND
		ecasa.engptcd = ? /* 1 */
	AND
		ecasa.engptlevelcd = ? /* 2 */
	AND
		ecasa.cefrlevelcd = ci.cefrlevelcd
WHERE
		en.examyear = ? /* 3 */
	AND
		en.examcd   = ? /* 4 */
ORDER BY
	ci_sort

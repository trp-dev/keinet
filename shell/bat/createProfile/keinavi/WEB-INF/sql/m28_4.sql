UPDATE historyinfo SET
	grade = ?,
	class = ?,
	class_no = ?
WHERE
	individualid = ?
AND
	year = ?

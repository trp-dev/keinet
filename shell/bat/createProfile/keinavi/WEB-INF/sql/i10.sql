UPDATE 
	examplanuniv 
SET 
	examplandate1 = ? 
	,examplandate2 = ? 
	,examplandate3 = ? 
	,examplandate4 = ? 
	,examplandate5 = ? 
	,examplandate6 = ? 
	,examplandate7 = ? 
	,examplandate8 = ? 
	,examplandate9 = ? 
	,examplandate10 = ? 
	,proventexamsite1 = ? 
	,proventexamsite2 = ? 
	,proventexamsite3 = ? 
	,proventexamsite4 = ? 
	,proventexamsite5 = ? 
	,proventexamsite6 = ? 
	,proventexamsite7 = ? 
	,proventexamsite8 = ? 
	,proventexamsite9 = ? 
	,proventexamsite10 = ? 
WHERE 
	individualid = ? 
AND 
	univcd = ? 
AND 
	facultycd = ? 
AND 
	deptcd = ? 
AND 
	entexammodecd = ? 
AND 
	entexamdiv1 = ? 
AND 
	entexamdiv2 = ? 
AND 
	entexamdiv3 = ? 

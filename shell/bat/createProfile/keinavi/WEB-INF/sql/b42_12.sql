SELECT
	/* CEFRîñ.bdeqxR[h */
	ci.cefrlevelcd,
	/* CEFRîñ.bdeqxZk¼ */
	CASE WHEN ci.cefrlevelcd = '99' THEN 'Èµ' ELSE ci.cerflevelname_abbr END AS cerflevelname_abbr,
	/* CEFRîñ.ÀÑ */
	ci.sort AS ci_sort,
	/* wZ}X^.ê¼ */
	sc.bundlename,
	/* CEFRæ¾óµiZj.l */
	cass.numbers AS numbers,
	/* CEFRæ¾óµiZj.\¬ä */
	cass.compratio AS compratio
FROM
	/* Í}X^TBL */
	examination en
	/* wZ}X^ */
	INNER JOIN school sc
	ON
		sc.bundlecd = ? /* '1' */
	/* CEFRîñTBL */
	INNER JOIN (
		SELECT
			ci.eventyear,
			ci.examdiv,
			ci.cefrlevelcd,
			ci.cerflevelname_abbr,
			ci.sort
		FROM
			cefr_info ci
		UNION ALL
		SELECT
			ci.eventyear,
			ci.examdiv,
			'ZZ' AS cefrlevelcd,
			'v' AS cerflevelname_abbr,
			-1 AS sort
		FROM
			cefr_info ci
		GROUP BY
			ci.eventyear,
			ci.examdiv) ci
	ON
		en.examyear = ci.eventyear
	AND
		en.examdiv = ci.examdiv
	/* CEFRæ¾óµiwZjTBL */
	LEFT JOIN cefracquisitionstatus_s cass
	ON
		cass.examyear = ci.eventyear
	AND
		cass.examcd = en.examcd
	AND
		cass.cefrlevelcd = ci.cefrlevelcd
	AND
		cass.bundlecd = sc.bundlecd
WHERE
		en.examyear = ? /* 2 */
	AND
		en.examcd   = ? /* 3 */
ORDER BY
	ci.sort

SELECT
	ri.examyear examyear,
	ri.examcd examcd,
	NVL(ri.cntgrade, ri.grade) grade,
	NVL2(ri.cntgrade, ri.cntclass, ri.class) class
FROM
	individualrecord ri
WHERE
	ri.s_individualid = ?
AND	
	NOT EXISTS (
		SELECT
			'1'
		FROM
			examination e
		WHERE
			e.examyear = ri.examyear
		AND
			e.examcd = ri.examcd
		AND
			e.examtypecd = '32'
		)
AND
	EXISTS (
		SELECT
			1
		FROM
			historyinfo h
		WHERE
			h.individualid = ?
		AND
			h.year = ri.examyear
		AND
			(h.grade <> NVL(ri.cntgrade, ri.grade) OR h.class <> NVL2(ri.cntgrade, ri.cntclass, ri.class)))
UNION ALL
SELECT /*+ ORDERED USE_NL(ri h) */
	ri.examyear,
	ri.examcd,
	h.grade,
	h.class
FROM
	individualrecord ri
INNER JOIN
	historyinfo h
ON
	h.individualid = ?
AND
	h.year = ri.examyear
WHERE
	ri.s_individualid = ?
AND	
	NOT EXISTS (
		SELECT
			'1'
		FROM
			examination e
		WHERE
			e.examyear = ri.examyear
		AND
			e.examcd = ri.examcd
		AND
			e.examtypecd = '32'
		)
AND
	(h.grade <> NVL(ri.cntgrade, ri.grade) OR h.class <> NVL2(ri.cntgrade, ri.cntclass, ri.class))

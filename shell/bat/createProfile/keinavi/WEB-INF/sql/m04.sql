SELECT
	ri.answersheet_no,

	ri.examyear - 1,
	b.grade,
	UPPER(b.class),
	b.class_no,

	ri.examyear,
	a.grade,
	UPPER(a.class),
	a.class_no,
	DECODE(a.sex, '1', '1', '2', '2', '9'),
	TRIM(a.name_kana),
	TRIM(a.name_kanji),
	CASE
		WHEN
			LENGTH(a.birthday) = 8
		AND
			SUBSTR(a.birthday, 5, 2) <= 12 
		AND
			SUBSTR(a.birthday, 7, 2) <= 31 
		THEN
			TO_CHAR(TO_DATE(a.birthday,'YYYYMMDD'), 'YYYY/MM/DD')
		ELSE
			NULL
	END CASE,
	TRIM(a.tel_no),

	ri.examyear,
	ri.grade,
	UPPER(ri.class),
	ri.class_no,
	DECODE(ri.sex, '1', '1', '2', '2', '9'),
	TRIM(ri.name_kana),
	TRIM(ri.name_kanji),
	CASE
		WHEN
			LENGTH(ri.birthday) = 8
		AND
			SUBSTR(ri.birthday, 5, 2) <= 12 
		AND
			SUBSTR(ri.birthday, 7, 2) <= 31 
		THEN
			TO_CHAR(TO_DATE(ri.birthday,'YYYYMMDD'), 'YYYY/MM/DD')
		ELSE
			NULL
	END CASE,
	TRIM(ri.tel_no),
	ri.s_individualid

FROM
	individualrecord ri,

	/* 今年度生徒情報 */
	(
		SELECT /*+ ORDERED USE_NL(b h) */
			h.year year,
			b.individualid individualid,
			h.grade grade,
			h.class class,	
			h.class_no class_no,
			b.sex sex,
			b.name_kana name_kana,
			b.name_kanji name_kanji,
			b.birthday birthday,
			b.tel_no tel_no
		FROM
			basicinfo b,
			historyinfo h
		WHERE
			b.individualid = h.individualid
	) a,

	/* 昨年度生徒情報 */
	(
		SELECT
			h.year year,
			h.individualid individualid,
			h.grade grade,
			h.class class,	
			h.class_no class_no
		FROM
			historyinfo h
	) b

WHERE
	ri.privercyflg = '0'
AND
	a.individualid(+) = ri.s_individualid
AND
	a.year(+) = ri.examyear
AND
	b.individualid(+) = ri.s_individualid
AND
	b.year(+) = ri.examyear - 1

AND ri.bundlecd IN (#)

AND ri.examyear = ?

AND ri.examcd = ?

AND ri.answersheet_no = ?

AND ri.grade = ?

AND UPPER(ri.class) = ?

ORDER BY
	ri.class,
	ri.class_no

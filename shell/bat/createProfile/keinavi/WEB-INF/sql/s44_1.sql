SELECT
	et.examyear,
	NVL(rs.totalcandidatenum, -999),
	NVL(rs.firstcandidatenum, -999),
	NVL(rs.ratingnum_a, -999),
	NVL(rs.ratingnum_b, -999),
	NVL(rs.ratingnum_c, -999),
	NVL(rs.ratingnum_d, -999),
	NVL(rs.ratingnum_e, -999)
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START */
	/* 2019/09/30 QQ) 共通テスト対応 ADD START */
	/* NVL(rs.eng_ratingnum_a, -999), */
	/* NVL(rs.eng_ratingnum_b, -999), */
	/* NVL(rs.eng_ratingnum_c, -999), */
	/* NVL(rs.eng_ratingnum_d, -999), */
	/* NVL(rs.eng_ratingnum_e, -999)  */
	/* 2019/09/30 QQ) 共通テスト対応 ADD END */
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END   */
FROM
	examcdtrans et
LEFT OUTER JOIN
	existsexam ee
ON
	ee.examyear = et.examyear
AND
	ee.examcd = et.examcd
LEFT OUTER JOIN
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START */
	/* 2019/09/30 QQ) 共通テスト対応 UPD START */
	/* v_ratingnumber_s rs */
	/* 2019/09/30 QQ) 共通テスト対応 UPD END */
	ratingnumber_s rs
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END   */
ON
	rs.examyear = ee.examyear
AND
	rs.examcd = ee.examcd
AND
	rs.bundlecd = ? /* 1 */
AND
	rs.studentdiv = ? /* 2 */
AND
	rs.univcountingdiv = ? /* 3 */
AND
	rs.univcd = ? /* 4 */
AND
	rs.facultycd = ? /* 5 */
AND
	rs.deptcd = ? /* 6 */
AND
	rs.agendacd = ? /* 7 */
AND
	rs.ratingdiv = ? /* 8 */
WHERE
	et.examyear IN (#)
AND
	et.curexamcd = ? /* 9 */
ORDER BY
	et.examyear DESC

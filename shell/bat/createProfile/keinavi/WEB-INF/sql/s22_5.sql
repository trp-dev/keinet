SELECT
	eventyear,
	examcd,
	cefrlevelcd,
	cerflevelname,
	sort,
	numbers1,
	compratio1,
	flg1,
	examyear2,
	numbers2,
	compratio2,
	examyear3,
	numbers3,
	compratio3,
	examyear4,
	numbers4,
	compratio4,
	examyear5,
	numbers5,
	compratio5
FROM (
	/* eCEFRxÌlA\¬ä */
	SELECT
		t1.eventyear,
		t1.examcd,
		t1.cefrlevelcd,
		t1.cerflevelname,
		t1.sort,
		t1.numbers AS numbers1,
		t1.compratio AS compratio1,
		t1.flg AS flg1,
		? /* '1' */  AS examyear2,
		t2.numbers AS numbers2,
		t2.compratio AS compratio2,
		? /* '2' */  AS examyear3,
		t3.numbers AS numbers3,
		t3.compratio AS compratio3,
		? /* '3' */  AS examyear4,
		t4.numbers AS numbers4,
		t4.compratio AS compratio4,
		? /* '4' */  AS examyear5,
		t5.numbers AS numbers5,
		t5.compratio AS compratio5
	FROM
		/* Nx */
		(SELECT
			/* ÍNx */
			ci.eventyear,
			/* bdeqxR[h */
			ci.cefrlevelcd,
			/* bdeqxZk¼ */
			CASE WHEN ci.cefrlevelcd = '99' THEN 'Èµ' ELSE ci.cerflevelname_abbr END AS cerflevelname,
			/* ÀÑ */
			ci.sort,
			/* ÍR[h */
			en.examcd,
			/* l */
			cass.numbers,
			/* \¬ä */
			cass.compratio,
			/* NxÌó±L³tO */
			CASE WHEN cass.bundlecd IS NULL THEN '0' ELSE '1' END AS flg
		FROM
			/* CEFRîñTBL */
			cefr_info ci
			/* Í}X^TBL */
			INNER JOIN examination en
			ON
				en.examyear = ci.eventyear
			AND
				en.examdiv = ci.examdiv
			AND
				en.examcd = ? /* '5' */
			/* CEFRæ¾óµiwZjTBL */
			LEFT JOIN cefracquisitionstatus_s cass
			ON
				cass.examyear = ci.eventyear
			AND
				cass.examcd = en.examcd
			AND
				cass.cefrlevelcd = ci.cefrlevelcd
			AND
				cass.bundlecd = ? /* '6' */
		WHERE
			ci.eventyear = ? /* '7' */
		ORDER BY
			ci.sort
		) t1
		/* ßNx1 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				cass.examyear,
				/* ÍR[h */
				cass.examcd,
				/* bdeqxR[h */
				cass.cefrlevelcd,
				/* l */
				cass.numbers,
				/* \¬ä */
				cass.compratio
			FROM
				/* CEFRæ¾óµiwZjTBL */
				cefracquisitionstatus_s cass
			WHERE
				cass.bundlecd = ? /* '8' */
			AND
				cass.examyear = ? /* '9' */
		) t2
		ON
			t2.examcd = t1.examcd
		AND
			t2.cefrlevelcd = t1.cefrlevelcd
		/* ßNx2 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				cass.examyear,
				/* ÍR[h */
				cass.examcd,
				/* bdeqxR[h */
				cass.cefrlevelcd,
				/* l */
				cass.numbers,
				/* \¬ä */
				cass.compratio
			FROM
				/* CEFRæ¾óµiwZjTBL */
				cefracquisitionstatus_s cass
			WHERE
				cass.bundlecd = ? /* '10' */
			AND
				cass.examyear = ? /* '11' */
		) t3
		ON
			t3.examcd = t1.examcd
		AND
			t3.cefrlevelcd = t1.cefrlevelcd
		/* ßNx3 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				cass.examyear,
				/* ÍR[h */
				cass.examcd,
				/* bdeqxR[h */
				cass.cefrlevelcd,
				/* l */
				cass.numbers,
				/* \¬ä */
				cass.compratio
			FROM
				/* CEFRæ¾óµiwZjTBL */
				cefracquisitionstatus_s cass
			WHERE
				cass.bundlecd = ? /* '12' */
			AND
				cass.examyear = ? /* '13' */
		) t4
		ON
			t4.examcd = t1.examcd
		AND
			t4.cefrlevelcd = t1.cefrlevelcd
		/* ßNx4 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				cass.examyear,
				/* ÍR[h */
				cass.examcd,
				/* bdeqxR[h */
				cass.cefrlevelcd,
				/* l */
				cass.numbers,
				/* \¬ä */
				cass.compratio
			FROM
				/* CEFRæ¾óµiwZjTBL */
				cefracquisitionstatus_s cass
			WHERE
				cass.bundlecd = ? /* '14' */
			AND
				cass.examyear = ? /* '15' */
		) t5
		ON
			t5.examcd = t1.examcd
		AND
			t5.cefrlevelcd = t1.cefrlevelcd
) UNION ALL (
	/* læ¾ */
	SELECT
		t1.examyear AS eventyear,
		t1.examcd,
		t1.cefrlevelcd,
		t1.cerflevelname,
		t1.sort,
		t1.numbers AS numbers1,
		t1.compratio AS compratio1,
		t1.flg AS flg1,
		? /* '16' */ AS examyear2,
		t2.numbers AS numbers2,
		t2.compratio AS compratio2,
		? /* '17' */ AS examyear3,
		t3.numbers AS numbers3,
		t3.compratio AS compratio3,
		? /* '18' */ AS examyear4,
		t4.numbers AS numbers4,
		t4.compratio AS compratio4,
		? /* '19' */ AS examyear5,
		t5.numbers AS numbers5,
		t5.compratio AS compratio5
	FROM
		/* Nx */
		(SELECT
			/* ÍNx */
			cass.examyear,
			/* bdeqxR[h */
			cass.cefrlevelcd,
			/* bdeqxZk¼ */
			'' AS cerflevelname,
			/* ÀÑ */
			999 AS sort,
			/* ÍR[h */
			cass.examcd,
			/* l */
			cass.numbers,
			/* \¬ä */
			cass.compratio,
			/* NxÌó±L³tO */
			CASE WHEN cass.bundlecd IS NULL THEN '0' ELSE '1' END AS flg
		FROM
			/* CEFRæ¾óµiwZjTBL */
			cefracquisitionstatus_s cass
		WHERE
			cass.bundlecd = ? /* '20' */
		AND
			cass.examyear = ? /* '21' */
		AND
			/* bdeqxR[h = ZZ:vR[h */
			cass.cefrlevelcd = 'ZZ'
		) t1
		/* ßNx1 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				cass.examyear,
				/* bdeqxR[h */
				cass.cefrlevelcd,
				/* l */
				cass.numbers,
				/* \¬ä */
				cass.compratio
			FROM
				/* CEFRæ¾óµiwZjTBL */
				cefracquisitionstatus_s cass
			WHERE
				cass.bundlecd = ? /* '22' */
			AND
				cass.examyear = ? /* '23' */
			AND
				/* bdeqxR[h = ZZ:vR[h */
				cass.cefrlevelcd = 'ZZ'
		) t2
		ON
			t2.cefrlevelcd = t1.cefrlevelcd
		/* ßNx2 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				cass.examyear,
				/* bdeqxR[h */
				cass.cefrlevelcd,
				/* l */
				cass.numbers,
				/* \¬ä */
				cass.compratio
			FROM
				/* CEFRæ¾óµiwZjTBL */
				cefracquisitionstatus_s cass
			WHERE
				cass.bundlecd = ? /* '24' */
			AND
				cass.examyear = ? /* '25' */
			AND
				/* bdeqxR[h = ZZ:vR[h */
				cass.cefrlevelcd = 'ZZ'
		) t3
		ON
			t3.cefrlevelcd = t1.cefrlevelcd
		/* ßNx3 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				cass.examyear,
				/* bdeqxR[h */
				cass.cefrlevelcd,
				/* l */
				cass.numbers,
				/* \¬ä */
				cass.compratio
			FROM
				/* CEFRæ¾óµiwZjTBL */
				cefracquisitionstatus_s cass
			WHERE
				cass.bundlecd = ? /* '26' */
			AND
				cass.examyear = ? /* '27' */
			AND
				/* bdeqxR[h = ZZ:vR[h */
				cass.cefrlevelcd = 'ZZ'
		) t4
		ON
			t4.cefrlevelcd = t1.cefrlevelcd
		/* ßNx4 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				cass.examyear,
				/* bdeqxR[h */
				cass.cefrlevelcd,
				/* l */
				cass.numbers,
				/* \¬ä */
				cass.compratio
			FROM
				/* CEFRæ¾óµiwZjTBL */
				cefracquisitionstatus_s cass
			WHERE
				cass.bundlecd = ? /* '28' */
			AND
				cass.examyear = ? /* '29' */
			AND
				/* bdeqxR[h = ZZ:vR[h */
				cass.cefrlevelcd = 'ZZ'
		) t5
		ON
			t5.cefrlevelcd = t1.cefrlevelcd
)
ORDER BY
	sort

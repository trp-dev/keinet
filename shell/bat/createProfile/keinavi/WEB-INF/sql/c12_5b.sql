SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(ri PK_SUBRECORD_I) */
	NVL(ri.s_rank, -999) s_rank,
	h.grade grade,
	h.class class,
	h.class_no class_no,
	b.name_kana,
	b.sex,
	NVL(ri.score, -999),
	NVL(ri.s_deviation, -999) s_deviation,
	NVL(ri.a_deviation, -999),
	ri.academiclevel
FROM
	basicinfo b,
	historyinfo h,
	subrecord_i ri
WHERE
	b.schoolcd = ? /* 1 */
AND
	h.individualid = b.individualid
AND
	h.year = ? /* 2 */
AND
	h.grade = ? /* 3 */
AND
	ri.individualid = h.individualid
AND
	ri.examyear = ? /* 4 */
AND
	ri.examcd = ? /* 5 */
AND
	ri.subcd = ? /* 6 */
AND
	h.class IN (#)
UNION ALL

SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(ri PK_SUBRECORD_I) */
	NVL(ri.s_rank, -999) s_rank,
	h.grade,
	h.class,
	h.class_no,
	b.name_kana,
	b.sex,
	NVL(ri.score, -999),
	NVL(ri.s_deviation, -999),
	NVL(ri.a_deviation, -999),
	ri.academiclevel
FROM
	basicinfo b,
	historyinfo h,
	class_group cg,
	subrecord_i ri
WHERE
	b.schoolcd = ? /* 7 */
AND
	h.individualid = b.individualid
AND
	h.year = ? /* 8 */
AND
	cg.year = h.year
AND
	cg.bundlecd = b.schoolcd
AND
	cg.grade = h.grade
AND
	cg.class = h.class
AND
	cg.classgcd IN (#)
AND
	ri.individualid = h.individualid
AND
	ri.examyear = ? /* 9 */
AND
	ri.examcd = ? /* 10 */
AND
	ri.subcd = ? /* 11 */

ORDER BY
	s_rank,
	grade DESC,
	class,
	class_no

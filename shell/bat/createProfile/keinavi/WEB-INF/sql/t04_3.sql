SELECT
	0,
	rs.examyear,
	rs.examcd,
	e.examname,
	3,
	p.prefcd,
	p.prefname,
	rs.bundlecd,
	s.bundlename,
	99,
	'99',
	rs.ratingdiv RATINGDIV,
	rs.univcountingdiv UNIVCOUNTINGDIV,
	rs.univcd UNIVCD,
	rs.facultycd FACULTYCD,
	rs.deptcd DEPTCD,
	u.choicecd5 CHOICECD5,
	DECODE(
		rs.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.uniname_abbr)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rs.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rs.univcd
		 ),
		u.uniname_abbr
	) univname_abbr,
	DECODE(
		rs.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.facultyname_abbr)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rs.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rs.univcd
			AND
				u.facultycd = rs.facultycd),
		u.facultyname_abbr
	),
	u.deptname_abbr,
	rs.agendacd AGENDACD,
	rs.studentdiv STUDENTDIV,
	rs.totalcandidatenum,
	rs.firstcandidatenum,
	rs.ratingnum_a,
	rs.ratingnum_b,
	rs.ratingnum_c,
	rs.ratingnum_d,
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD START */
/*	rs.ratingnum_e, */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� ADD START */
/*	rs.eng_ratingnum_a, */
/*	rs.eng_ratingnum_b, */
/*	rs.eng_ratingnum_c, */
/*	rs.eng_ratingnum_d, */
/*	rs.eng_ratingnum_e */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� ADD END */
	rs.ratingnum_e
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD END */
	/* 2016/03/02 QQ)Nishiyama ��K�͉��C ADD START */
	/* ����w�P�� */
	/* ��w�敪 */
	, DECODE(
		rs.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.unidiv)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rs.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rs.univcd
		 ),
		u.unidiv
	) unidiv_sort,
	/* ��w���J�i */
	DECODE(
		rs.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.univname_kana)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rs.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rs.univcd
		 ),
		u.univname_kana
	) univname_kana_sort,
	/* ���w���P�� */
	/* ��w��ԋ敪 */
	DECODE(
		rs.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.uninightdiv)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rs.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rs.univcd
			AND
				u.facultycd = rs.facultycd
		 ),
		u.uninightdiv
	) uninightdiv_sort,
	/* �w�����e�R�[�h */
	DECODE(
		rs.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.facultyconcd)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rs.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rs.univcd
			AND
				u.facultycd = rs.facultycd
		 ),
		u.facultyconcd
	) facultyconcd_sort
	/* 2016/03/02 QQ)Nishiyama ��K�͉��C ADD END */
FROM
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD START */
	/* ratingnumber_s rs */
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD START */
/*	v_ratingnumber_s rs */
	ratingnumber_s rs
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD END */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD END */
INNER JOIN
	school s
ON
	s.bundlecd = rs.bundlecd
INNER JOIN
	prefecture p
ON
	p.prefcd = s.facultycd
INNER JOIN
	examination e
ON
	e.examyear = rs.examyear
AND
	e.examcd = rs.examcd
LEFT OUTER JOIN
	univmaster_basic u
ON
	u.eventyear = rs.examyear
AND
	u.examdiv = e.examdiv
AND
	u.univcd = rs.univcd
AND
	u.facultycd = rs.facultycd
AND
	u.deptcd = rs.deptcd
WHERE
	rs.examyear = ?
AND
	rs.examcd = ?
AND
	rs.bundlecd = ?
ORDER BY
	RATINGDIV,
	UNIVCOUNTINGDIV,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	unidiv_sort,
	univname_kana_sort,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
	UNIVCD,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C UPD START */
	/* FACULTYCD, */
	uninightdiv_sort,
	facultyconcd_sort,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C UPD END */
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	NVL(u.UNIGDIV, ''),
	NVL(u.DEPTSERIAL_NO, ''),
	NVL(u.SCHEDULESYS, ''),
	NVL(u.SCHEDULESYSBRANCHCD, ''),
	NVL(u.DEPTNAME_KANA, ''),
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
	DEPTCD,
	AGENDACD,
	STUDENTDIV

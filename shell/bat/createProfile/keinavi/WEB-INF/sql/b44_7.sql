SELECT
	'（' || p.prefname || '）' bundlename,
	et.examyear examyear,
	NVL(rp.totalcandidatenum, -999) totalcandidatenum,
	NVL(rp.firstcandidatenum, -999) firstcandidatenum,
	NVL(rp.ratingnum_a, -999) ratingnum_a,
	NVL(rp.ratingnum_b, -999) ratingnum_b,
	NVL(rp.ratingnum_c, -999) ratingnum_c,
	NVL(rp.ratingnum_d, -999) ratingnum_d,
	NVL(rp.ratingnum_e, -999) ratingnum_e
/* 2019/11/26 QQ)nagai 英語認定試験延期対応 DEL START */
	/* 2019/09/30 QQ) 共通テスト対応 ADD START */
	/* NVL(rp.eng_ratingnum_a, -999) eng_ratingnum_a, */
	/* NVL(rp.eng_ratingnum_b, -999) eng_ratingnum_b, */
	/* NVL(rp.eng_ratingnum_c, -999) eng_ratingnum_c, */
	/* NVL(rp.eng_ratingnum_d, -999) eng_ratingnum_d, */
	/* NVL(rp.eng_ratingnum_e, -999) eng_ratingnum_e */
	/* 2019/09/30 QQ) 共通テスト対応 ADD END */
/* 2019/11/26 QQ)nagai 英語認定試験延期対応 DEL END   */
FROM
	examcdtrans et
INNER JOIN
	prefecture p
ON
	p.prefcd = ? /* 1 */
LEFT OUTER JOIN
/* 2019/11/26 QQ)nagai 英語認定試験延期対応 DEL START */
	/* 2019/09/30 QQ) 共通テスト対応 UPD START */
	/* v_ratingnumber_p rp */
	/* 2019/09/30 QQ) 共通テスト対応 UPD END */
    ratingnumber_p rp
/* 2019/11/26 QQ)nagai 英語認定試験延期対応 DEL END   */
ON
	rp.examyear = et.examyear
AND
	rp.examcd = et.examcd
AND
	rp.ratingdiv = ? /* 2 */
AND
	rp.univcountingdiv = ? /* 3 */
AND
	rp.univcd = ? /* 4 */
AND
	rp.facultycd = ? /* 5 */
AND
	rp.deptcd = ? /* 6 */
AND
	rp.agendacd = ? /* 7 */
AND
	rp.studentdiv = ? /* 8 */
AND
	rp.prefcd = p.prefcd
AND
	p.prefcd = rp.prefcd
WHERE
	et.examyear IN (#)
AND
	et.curexamcd = ? /* 9 */
ORDER BY
	examyear DESC,
	totalcandidatenum DESC

SELECT
    z.devzonecd devzonecd,
    NVL(z.highlimit, -999) highlimit,
    NVL(z.lowlimit, -999) lowlimit,
    NVL(d.reachlevel, e.reachlevel) reachlevel,
    NVL(d.disp_str, e.disp_str) disp_str
FROM
    scorezone_j z
LEFT OUTER JOIN
    reachlevel r
ON
    r.examyear = ?
AND
    r.examcd = z.examcd
AND
    r.subcd = ?
AND
    r.devzonecd = z.devzonecd
LEFT OUTER JOIN
    reachlevel_display d
ON
    d.examcd = r.examcd
AND
    d.subcd = r.subcd
AND
    d.reachlevel = r.reachlevel
LEFT OUTER JOIN
    reachlevel_display e
ON
    (e.examcd, e.subcd, e.reachlevel) IN (
SELECT
    r.examcd,
    r.subcd,
    MIN(r.reachlevel)
FROM
    reachlevel r
WHERE
    r.examyear = ?
AND
    r.examcd = ?
AND
    r.subcd = ?
GROUP BY
    r.examcd,
    r.subcd)
WHERE
    z.examcd = ?
ORDER BY
    devzonecd DESC

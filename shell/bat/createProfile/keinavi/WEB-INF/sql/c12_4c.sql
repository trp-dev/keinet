SELECT
	es.subcd,
	es.subname,
	es.suballotpnt,
	NVL(SUM(rc.NUMBERS),-999),
	es.dispsequence
FROM
	v_i_cmexamsubject es

LEFT OUTER JOIN
	subrecord_c rc
ON
	rc.examyear = es.examyear
AND
	rc.examcd = es.examcd
AND
	rc.subcd = es.subcd
AND
	rc.bundlecd = ? /* 1 */
AND
	rc.grade = ? /* 2 */
AND
	rc.class IN(#)

WHERE
	es.examyear = ? /* 3 */
AND
	es.examcd = ? /* 4 */
GROUP BY
	es.subcd,
	es.subname,
	es.suballotpnt,
	es.dispsequence
ORDER BY
	es.dispsequence

SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(cr PK_CANDIDATERATING) */
	h.grade,
	h.class class,
	h.class_no class_no,
	b.name_kana,
	b.sex,
	cr.deptcd deptcd,
	u.deptname_abbr,
	' ',
	cr.centerrating centerrating,
	cr.secondrating secondrating,
	cr.totalrating,
	cr.individualid,
	cr.candidaterank,
	DECODE (h.grade, 3, 1, 1, 3, h.grade) grade_seq,
	DECODE(cr.centerrating, '��', 'A', '��', 'B', '��', 'C', '��', 'D', cr.centerrating) center_seq,
	DECODE(cr.secondrating, '��', 'A', '��', 'B', '��', 'C', '��', 'D', cr.secondrating) second_seq,
	u.deptsortkey
/* 2019/11/21 QQ)Tanioka �p��F�莎�������Ή� DEL START */
	/* 2019/09/06 QQ)Oosaki ���ʃe�X�g�Ή� ADD START */
/*	,cr.centerrating_engpt */
/*	,cr.app_qual_judge */
/*	,cr.app_qual_notice */
	/* 2019/09/06 QQ)Oosaki ���ʃe�X�g�Ή� ADD END */
/* 2019/11/21 QQ)Tanioka �p��F�莎�������Ή� DEL END */
FROM
	basicinfo b,
	historyinfo h,
	candidaterating cr,
	h12_univmaster u
WHERE
	cr.examyear = ? /* 1 */
AND
	cr.examcd = ? /* 2 */
AND
	cr.individualid = b.individualid
AND
	b.schoolcd = ? /* 3 */
AND
	h.year = ? /* 4 */
AND
	h.grade = ? /* 5 */
AND
	h.class = ? /* 6 */
AND
	h.individualid = b.individualid
AND
	u.eventyear = cr.examyear
AND
	u.examdiv = ? /* 7 */
AND
	u.univcd = cr.univcd
AND
	u.facultycd = cr.facultycd
AND
	u.deptcd = cr.deptcd
AND
	cr.univcd = ? /* 8 */
AND
	cr.facultycd = ? /* 9 */
# /* �i���ݏ��� */

UNION ALL

SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(cr PK_CANDIDATERATING) */
	h.grade,
	h.class,
	h.class_no,
	b.name_kana,
	b.sex,
	cr.deptcd,
	u.deptname_abbr,
	' ',
	cr.centerrating,
	cr.secondrating,
	cr.totalrating,
	cr.individualid,
	cr.candidaterank,
	DECODE (h.grade, 3, 1, 1, 3, h.grade) grade_seq,
	DECODE(cr.centerrating, '��', 'A', '��', 'B', '��', 'C', '��', 'D', cr.centerrating) center_seq,
	DECODE(cr.secondrating, '��', 'A', '��', 'B', '��', 'C', '��', 'D', cr.secondrating) second_seq,
	u.deptsortkey
/* 2019/11/21 QQ)Tanioka �p��F�莎�������Ή� DEL START */
	/* 2019/09/06 QQ)Oosaki ���ʃe�X�g�Ή� ADD START */
/*	,cr.centerrating_engpt */
/*	,cr.app_qual_judge */
/*	,cr.app_qual_notice */
	/* 2019/09/06 QQ)Oosaki ���ʃe�X�g�Ή� ADD END */
/* 2019/11/21 QQ)Tanioka �p��F�莎�������Ή� DEL END */
FROM
	basicinfo b,
	historyinfo h,
	class_group cg,
	candidaterating cr,
	h12_univmaster u
WHERE
	cr.examyear = ? /* 10 */
AND
	cr.examcd = ? /* 11 */
AND
	cr.individualid = b.individualid
AND
	b.schoolcd = ? /* 12 */
AND
	h.year = ? /* 13 */
AND
	h.grade = cg.grade
AND
	h.class = cg.class
AND
	cg.year = h.year
AND
	cg.bundlecd = b.schoolcd
AND
	cg.classgcd = ? /* 14 */
AND
	h.individualid = b.individualid
AND
	u.eventyear = cr.examyear
AND
	u.examdiv = ? /* 15 */
AND
	u.univcd = cr.univcd
AND
	u.facultycd = cr.facultycd
AND
	u.deptcd = cr.deptcd
AND
	cr.univcd = ? /* 16 */
AND
	cr.facultycd = ? /* 17 */
# /* �i���ݏ��� */

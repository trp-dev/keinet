SELECT
	studentdiv,
	univcd,
	univname_abbr,
	facultycd,
	facultyname_abbr,
	deptcd,
	deptname_abbr,
	agendacd,
	schedulename,
	ratingdiv
FROM (
	SELECT
		ra.studentdiv studentdiv,
		ra.univcd univcd,
		u.uniname_abbr univname_abbr,
		ra.facultycd facultycd,
		NULL facultyname_abbr,
		ra.deptcd deptcd,
		NULL deptname_abbr,
		ra.agendacd agendacd,
		s.schedulename schedulename,
		ra.ratingdiv ratingdiv,
		c.dispsequence dispsequence,
		c.univdiv univdiv,
		u.facultycd facultycd1,
		u.deptcd deptcd1,
		FIRST_VALUE(u.facultycd) OVER (PARTITION BY ra.univcd) facultycd2,
		FIRST_VALUE(u.deptcd) OVER (PARTITION BY ra.univcd) deptcd2
		/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
		 , u.univname_kana
		/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
	FROM
		ratingnumber_a ra,
		schedule s,
		countuniv c,
		univmaster_basic u
	WHERE
		ra.examyear = ? /* 1 */
	AND
		ra.examcd = ? /* 2 */
	AND
		ra.univcountingdiv = ? /* 3 */
	AND
		s.schedulecd = ra.agendacd
	AND
		c.univcd = ra.univcd
	AND
		u.univcd = c.univcd
	AND
		u.eventyear = ra.examyear
	AND
		u.examdiv = ? /* 4 */
	AND
		ra.studentdiv IN (#)
	AND
		ra.ratingdiv IN (#)
	/* SCHEDULE CONDITION_A */
)
WHERE
	facultycd1 = facultycd2
AND
	deptcd1 = deptcd2
/* 2016/01/21 QQ)Nishiyama ��K�͉��C DEL START */
/*
ORDER BY
	studentdiv,
	univdiv,
	dispsequence,
	agendacd,
	ratingdiv
 */
/* 2016/01/21 QQ)Nishiyama ��K�͉��C DEL END */

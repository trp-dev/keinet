UPDATE
	individualrecord ri
SET
	(ri.s_individualid, ri.cntgrade, ri.cntclass) = (
		SELECT
			h.individualid,
			h.grade,
			h.class
		FROM
			historyinfo h
		WHERE
			h.individualid = ?
		AND
			h.year = ri.examyear)
WHERE
	ri.examyear = ?
AND
	ri.examcd = ?
AND
	ri.answersheet_no = ?

SELECT
	cc.grade,
	SUBSTR(cc.class, 1, 2),
	SUBSTR(cc.class, 1, 2),
	cc.dispsequence dispsequence
FROM
	countcompclass cc
WHERE
	LENGTH(NVL(TRIM(cc.class), '  ')) <= 2

UNION ALL

SELECT
	cc.grade,
	cc.class,
	cn.classgname,
	cc.dispsequence
FROM
	countcompclass cc,
	classgroup cn
WHERE
	cn.classgcd = cc.class

ORDER BY
	dispsequence

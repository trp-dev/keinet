SELECT
	p.bundlename,
	NVL(p.numbers, -999),
	NVL(p.avgpnt, -999),
	NVL(p.avgdeviation, -999),
	NVL(rs.numbers, -999),
	NVL(rs.avgpnt, -999),
	NVL(rs.avgdeviation, -999)
FROM (
	SELECT
		rs.numbers numbers,
		rs.avgpnt avgpnt,
		rs.avgdeviation avgdeviation,
		s.bundlecd bundlecd,
		s.bundlename bundlename,
		s.newgetdate newgetdate,
		s.stopdate stopdate,
		TO_CHAR(et.examyear - 1) lastexamyear,
		et.curexamcd lastexamcd,
		st.cursubcd lastsubcd
	FROM
		examcdtrans et
	INNER JOIN
		school s
	ON
		s.bundlecd = ?
	LEFT OUTER JOIN
		examination e
	ON
		e.examyear = et.examyear
	AND
		e.examcd = et.examcd
	AND
		NVL2(s.newgetdate, s.newgetdate || '0000', '000000000000') <= e.out_dataopendate
	AND
		NVL2(s.stopdate, s.stopdate || '0000', '999999999999') > e.out_dataopendate
	LEFT OUTER JOIN
		subcdtrans st
	ON
		st.examyear = et.examyear
	AND
		st.examcd = et.curexamcd
	AND
		st.cursubcd = ?
	LEFT OUTER JOIN
		subrecord_s rs
	ON
		rs.examyear = e.examyear
	AND
		rs.examcd = e.examcd
	AND
		rs.subcd = st.subcd
	AND
		rs.bundlecd = s.bundlecd
	WHERE
		et.examyear = (? + 1)
	AND
		et.curexamcd = ?) p
LEFT OUTER JOIN
	examination e
ON
	e.examyear = p.lastexamyear
AND
	e.examcd = p.lastexamcd
AND
	NVL2(p.newgetdate, p.newgetdate || '0000', '000000000000') <= e.out_dataopendate
AND
	NVL2(p.stopdate, p.stopdate || '0000', '999999999999') > e.out_dataopendate
LEFT OUTER JOIN
	subrecord_s rs
ON
	rs.examyear = e.examyear
AND
	rs.examcd = e.examcd
AND
	rs.subcd = ?
AND
	rs.bundlecd = p.bundlecd

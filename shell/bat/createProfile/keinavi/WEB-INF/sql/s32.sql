SELECT
	NVL(highlimit, -999) highlimit,
	NVL(lowlimit, -999) lowlimit,
	MAX(numbers),
	MAX(compratio)
FROM (
	SELECT
		highlimit,
		lowlimit,
		-999 numbers,
		-999 compratio
	FROM
		deviationzone

	UNION ALL 

	SELECT
		d.highlimit,
		d.lowlimit,
		dc.numbers,
		dc.compratio
	FROM
		deviationzone d,
		subdistrecord_c dc
	WHERE
		dc.examyear = ? /* 1 */
	AND
		dc.examcd = ? /* 2 */
	AND
		dc.bundlecd = ? /* 3 */
	AND
		dc.subcd = ? /* 4 */
	AND
		dc.grade = ? /* 5 */
	AND
		dc.class = ? /* 6 */
	AND
		dc.devzonecd = d.devzonecd

	UNION ALL 

	SELECT
		d.highlimit,
		d.lowlimit,
		SUM(dc.numbers),
		ROUND(SUM(dc.numbers) / a.numbers * 100,2)
	FROM
		deviationzone d,
		subdistrecord_c dc,
		class_group cg,
		(
			SELECT
				SUM(rc.numbers) numbers
			FROM
				subrecord_c rc,
				class_group cg
			WHERE
				rc.examyear = ? /* 7 */
			AND
				rc.examcd = ? /* 8 */
			AND
				rc.bundlecd = ? /* 9 */
			AND
				rc.subcd = ? /* 10 */
			AND
				rc.grade = ? /* 11 */
			AND
				rc.class = cg.class
			AND
				rc.grade = cg.grade
			AND
				cg.classgcd = ? /* 12 */
				
		) a
		
	WHERE
		dc.examyear = ? /* 13 */
	AND
		dc.examcd = ? /* 14 */
	AND
		dc.bundlecd = ? /* 15 */
	AND
		dc.subcd = ? /* 16 */
	AND
		dc.grade = ? /* 17 */
	AND
		dc.grade = cg.grade
	AND
		dc.class = cg.class
	AND
		cg.classgcd = ? /* 18 */
	AND
		dc.devzonecd = d.devzonecd
	GROUP BY
		d.highlimit,
		d.lowlimit,
		a.numbers
	)
GROUP BY
	highlimit,
	lowlimit
ORDER BY
	lowlimit DESC

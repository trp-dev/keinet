SELECT
	e.examyear,
	e.examcd,
	e.examname,
	e.examtypecd,
	RANK() OVER(ORDER BY e.out_dataopendate DESC, e.dispsequence)
FROM
	examination e
WHERE
	e.examyear >= ?
AND
	e.out_dataopendate < TO_CHAR(SYSDATE, 'YYYYMMDDHH24MI')
ORDER BY
	e.examyear DESC, e.dispsequence, e.out_dataopendate

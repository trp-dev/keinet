INSERT INTO
	ko_attribute
SELECT
	?,
	d.attributecd,
	d.attributename,
	d.attributename,
	d.attrivariableflg
FROM
	ko_dafaultattribute d
WHERE
	NOT EXISTS (
		SELECT
			1
		FROM
			ko_attribute a
		WHERE
			a.schoolcd = ?
		AND
			a.attributecd = d.attributecd)

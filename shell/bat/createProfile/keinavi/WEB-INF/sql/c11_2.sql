SELECT
	es.subcd,
	es.subname,
	es.suballotpnt,
	NVL(a.numbers, -999),
	NVL(b.avgpnt, -999),
	NVL(a.avgdeviation, -999),
	NVL(b.numbers, -999),
	NVL(a.avgpnt, -999),
	NVL(b.avgdeviation, -999)
FROM
	examsubject es,
	(
		SELECT
			subcd,
			numbers,
			avgpnt,
			avgdeviation
		FROM
			subrecord_c
		WHERE
			examyear = ? /* 1 */
		AND
			examcd = ? /* 2 */
		AND
			bundlecd = ? /* 3 */
		AND
			grade = ? /* 4 */
		AND
			class = ? /* 5 */
		
		UNION ALL
		
		SELECT
			subcd,
			numbers,
			avgpnt,
			avgdeviation
		FROM (
			SELECT
				rc.subcd subcd,
				SUM(rc.numbers) OVER (PARTITION BY rc.subcd, ra.studentdiv) numbers,
				ROUND (
					SUM(rc.numbers * rc.avgpnt)
						OVER (PARTITION BY rc.subcd, ra.studentdiv)
					/ SUM(rc.numbers)
						OVER (PARTITION BY rc.subcd, ra.studentdiv),
					1
				) avgpnt,
				ROUND (
					(
						ROUND(
							SUM(rc.numbers * rc.avgpnt)
								OVER (PARTITION BY rc.subcd, ra.studentdiv) /
							SUM(rc.numbers)
								OVER (PARTITION BY rc.subcd, ra.studentdiv),
							1
						)
						- ra.avgpnt
					) / ra.stddeviation * 10 + 50,
					1
				) avgdeviation,
				ra.studentdiv studentdiv1,
				MIN (studentdiv) OVER (PARTITION BY rc.subcd) studentdiv2,
				rc.grade grade1,
				FIRST_VALUE(rc.grade) OVER (PARTITION BY rc.subcd ORDER BY ra.studentdiv DESC) grade2,
				rc.class class1,
				FIRST_VALUE(rc.class) OVER (PARTITION BY rc.subcd ORDER BY ra.studentdiv DESC) class2
			FROM
				subrecord_c rc,
				subrecord_a ra,
				class_group cg
			WHERE
				rc.examyear = ? /* 6 */
			AND
				rc.examcd = ? /* 7 */
			AND
				rc.bundlecd = ? /* 8 */
			AND
				rc.grade = cg.grade
			AND
				rc.class = cg.class
			AND
				cg.classgcd = ? /* 9 */
			AND
				ra.examyear = rc.examyear
			AND
				ra.examcd = rc.examcd
			AND
				ra.subcd = rc.subcd
		)
		WHERE
			grade1 = grade2
		AND
			class1 = class2
		AND
			studentdiv1 = studentdiv2
	) a,
	(
		SELECT
			subcd,
			numbers,
			avgpnt,
			avgdeviation
		FROM
			subrecord_s
		WHERE
			examyear = ? /* 10 */
		AND
			examcd = ? /* 11 */
		AND
			bundlecd = ? /* 12 */
	) b
WHERE
	es.examyear = ? /* 13 */
AND
	es.examcd = ? /* 14 */
AND
	es.subcd IN (#)
AND
	a.subcd(+) = es.subcd
AND
	b.subcd(+) = es.subcd
ORDER BY
	es.dispsequence

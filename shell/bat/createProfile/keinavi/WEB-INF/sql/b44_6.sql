SELECT
	TRIM('（全国）') bundlename,
	et.examyear examyear,
	NVL(ra.totalcandidatenum, -999) totalcandidatenum,
	NVL(ra.firstcandidatenum, -999) firstcandidatenum,
	NVL(ra.ratingnum_a, -999) ratingnum_a,
	NVL(ra.ratingnum_b, -999) ratingnum_b,
	NVL(ra.ratingnum_c, -999) ratingnum_c,
	NVL(ra.ratingnum_d, -999) ratingnum_d,
	NVL(ra.ratingnum_e, -999) ratingnum_e
/* 2019/11/26 QQ)nagai 英語認定試験延期対応 DEL START */
	/* 2019/09/30 QQ) 共通テスト対応 ADD START */
	/* NVL(ra.eng_ratingnum_a, -999) eng_ratingnum_a, */
	/* NVL(ra.eng_ratingnum_b, -999) eng_ratingnum_b, */
	/* NVL(ra.eng_ratingnum_c, -999) eng_ratingnum_c, */
	/* NVL(ra.eng_ratingnum_d, -999) eng_ratingnum_d, */
	/* NVL(ra.eng_ratingnum_e, -999) eng_ratingnum_e */
	/* 2019/09/30 QQ) 共通テスト対応 ADD END */
/* 2019/11/26 QQ)nagai 英語認定試験延期対応 DEL DEL   */
FROM
	examcdtrans et
LEFT OUTER JOIN
/* 2019/11/26 QQ)nagai 英語認定試験延期対応 DEL START */
	/* 2019/09/30 QQ) 共通テスト対応 UPD START */
	/* v_ratingnumber_a ra */
	/* 2019/09/30 QQ) 共通テスト対応 UPD END */
    ratingnumber_a ra
/* 2019/11/26 QQ)nagai 英語認定試験延期対応 DEL END   */
ON
	ra.examyear = et.examyear
AND
	ra.examcd = et.examcd
AND
	ra.ratingdiv = ? /* 1 */
AND
	ra.univcountingdiv = ? /* 2 */
AND
	ra.univcd = ? /* 3 */
AND
	ra.facultycd = ? /* 4 */
AND
	ra.deptcd = ? /* 5 */
AND
	ra.agendacd = ? /* 6 */
AND
	ra.studentdiv = ? /* 7 */
WHERE
	et.examyear IN (#)
AND
	et.curexamcd = ? /* 8 */
ORDER BY
	examyear DESC,
	totalcandidatenum DESC

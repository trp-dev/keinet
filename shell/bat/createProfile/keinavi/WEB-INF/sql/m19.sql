SELECT
	(SELECT cg.classgname FROM classgroup cg WHERE cg.classgcd = ?),
	(SELECT cg.com FROM classgroup cg WHERE cg.classgcd = ?),
	t.class class,
	t.count,
	NVL2(c.year, 1, 0)
FROM (
SELECT /*+ ORDERED INDEX(b BASICINFO_IDX) INDEX(h PK_HISTORYINFO) USE_NL(b h) */
	h.year year,
	b.schoolcd schoolcd,
	h.grade grade,
	h.class class,
	COUNT(b.individualid) count
FROM
	basicinfo b
INNER JOIN
	historyinfo h
ON
	h.individualid = b.individualid
AND
	h.year = ?
AND
	h.grade = ?
WHERE
	b.schoolcd = ?
GROUP BY
	h.year, b.schoolcd, h.grade, h.class
) t
LEFT OUTER JOIN
	class_group c
ON
	c.year = t.year
AND
	c.bundlecd = t.schoolcd
AND
	c.grade = t.grade
AND
	c.class = t.class
AND
	c.classgcd = ?
ORDER BY
	class

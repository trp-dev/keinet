SELECT
	rc.subcd,
	es.subname,
	es.suballotpnt,
	NVL(rc.numbers, -999),
	es.dispsequence
FROM
	subrecord_c rc,
	examsubject es
WHERE
	es.examyear = ? /* 1 */
AND
	es.examcd = ? /* 2 */
AND
	rc.examyear = es.examyear
AND
	rc.examcd = es.examcd
AND
	rc.subcd = es.subcd
AND
	rc.bundlecd = ? /* 3 */
AND
	rc.grade = ? /* 4 */
AND
	rc.class = ? /* 5 */

UNION ALL

SELECT
	rc.subcd,
	es.subname,
	es.suballotpnt,
	SUM(rc.numbers),
	es.dispsequence
FROM
	subrecord_c rc,
	examsubject es,
	class_group cg
WHERE

	es.examyear = ? /* 6 */
AND
	es.examcd = ? /* 7 */
AND
	rc.examyear = es.examyear
AND
	rc.examcd = es.examcd
AND
	rc.subcd = es.subcd
AND
	rc.bundlecd = ? /* 8 */
AND
	rc.grade = cg.grade
AND
	rc.class = cg.class
AND
	cg.classgcd = ? /* 9 */
GROUP BY
	rc.subcd,
	es.subname,
	es.suballotpnt,
	es.dispsequence
ORDER BY 5

INSERT INTO
	countchargeclass
SELECT
	cg.grade,
	cg.class,
	1
FROM
	class_group cg
WHERE
	cg.year = ?
AND
	cg.bundlecd = ?
AND
	cg.grade = ?
AND
	cg.classgcd = ?
AND
	NOT EXISTS (
		SELECT
			c.rowid
		FROM
			countchargeclass c
		WHERE
			c.grade = cg.grade
		AND
			c.class = cg.class
	)

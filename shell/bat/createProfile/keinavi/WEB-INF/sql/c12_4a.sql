SELECT
	es.subcd,
	es.subname,
	es.suballotpnt,
	NVL(rc.numbers, -999),
	es.dispsequence
FROM
	v_i_cmexamsubject es

LEFT OUTER JOIN
	subrecord_c rc
ON
	rc.examyear = es.examyear
AND
	rc.examcd = es.examcd
AND
	rc.subcd = es.subcd
AND
	rc.bundlecd = ? /* 1 */
AND
	rc.grade = ? /* 2 */
AND
	rc.class = ? /* 3 */

WHERE
	es.examyear = ? /* 4 */
AND
	es.examcd = ? /* 5 */

ORDER BY
	es.dispsequence

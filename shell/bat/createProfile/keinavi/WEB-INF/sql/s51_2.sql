SELECT
	NVL(rs.numbers, -999),
	NVL(rs.avgpnt, -999),
	NVL(rs.avgdeviation, -999)
FROM
	examcdtrans et
INNER JOIN
	existsexam ee
ON
	ee.examyear = et.examyear
AND
	ee.examcd = et.examcd
INNER JOIN
	subcdtrans st
ON
	st.examyear = et.examyear
AND
	st.examcd = et.curexamcd
AND
	st.cursubcd = ?
INNER JOIN
	subrecord_s rs
ON
	rs.examyear = ee.examyear
AND
	rs.examcd = ee.examcd
AND
	rs.bundlecd = ?
AND
	rs.subcd = st.subcd
WHERE
	et.examyear = ?
AND
	et.curexamcd = ?

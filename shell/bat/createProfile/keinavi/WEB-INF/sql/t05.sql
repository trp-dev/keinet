SELECT
	class,
	class_no,
	sex,
	name_kana,
	NULL,
	NULL,
	univcd || facultycd || deptcd,
	univname_abbr || NVL2(facultyname_abbr, '�E', '') || facultyname_abbr || NVL2(deptname_abbr, '�E', '') || deptname_abbr,
	NULL,
	DECODE(
		entexammodecd,
		'01', DECODE(entexamdiv2, 1, 2, 2, 3, 2),
		'02', 9,
		'03', 5,
		'04', 7,
		'05', 4,
		2
	),
	NULL,
	NULL
FROM
	(	
		SELECT /*+ ORDERED USE_NL(t, u) */
			t.individualid individualid,
			t.sex sex,
			t.name_kana name_kana,
			t.grade grade,
			t.class class,
			t.class_no class_no,
			t.univcd univcd,
			t.facultycd facultycd,
			t.deptcd deptcd,
			t.entexammodecd entexammodecd,
			t.entexamdiv2 entexamdiv2,
			u.univname_abbr univname_abbr,
			u.facultyname_abbr facultyname_abbr,
			u.deptname_abbr deptname_abbr,
			RANK() OVER (PARTITION BY t.individualid ORDER BY t.sucanndate DESC, t.examplandate DESC, t.univcd, t.facultycd, t.deptcd, t.entexammodecd) rank
		FROM
			(
				SELECT /*+ ORDERED USE_NL(t, s) */
					t.individualid individualid,
					t.sex sex,
					t.name_kana name_kana,
					t.grade grade,
					t.class class,
					t.class_no class_no,
					t.univcd univcd,
					t.facultycd facultycd,
					t.deptcd deptcd,
					t.entexammodecd entexammodecd,
					t.entexamdiv1 entexamdiv1,
					t.entexamdiv2 entexamdiv2,
					t.entexamdiv3 entexamdiv3,
					t.examplandate examplandate,
					FIRST_VALUE(t.entexamdiv1) OVER(PARTITION BY t.individualid, t.univcd, t.facultycd, t.deptcd, t.entexammodecd ORDER BY CONVERT_MMDD(s.sucanndate) DESC, t.examplandate DESC, t.entexamdiv1, t.entexamdiv2, t.entexamdiv3) f_entexamdiv1,
					FIRST_VALUE(t.entexamdiv2) OVER(PARTITION BY t.individualid, t.univcd, t.facultycd, t.deptcd, t.entexammodecd ORDER BY CONVERT_MMDD(s.sucanndate) DESC, t.examplandate DESC, t.entexamdiv1, t.entexamdiv2, t.entexamdiv3) f_entexamdiv2,
					FIRST_VALUE(t.entexamdiv3) OVER(PARTITION BY t.individualid, t.univcd, t.facultycd, t.deptcd, t.entexammodecd ORDER BY CONVERT_MMDD(s.sucanndate) DESC, t.examplandate DESC, t.entexamdiv1, t.entexamdiv2, t.entexamdiv3) f_entexamdiv3,
					CONVERT_MMDD(s.sucanndate) sucanndate
				FROM
					(
						SELECT /*+ ORDERED USE_NL(b h) USE_NL(b e) INDEX(b BASICINFO_IDX) */
							b.individualid individualid,
							b.sex sex,
							b.name_kana name_kana,
							h.grade grade,
							h.class class,
							h.class_no class_no,
							NVL2(u.newuniv8cd, SUBSTR(u.newuniv8cd, 1, 4), e.univcd) univcd,
							NVL2(u.newuniv8cd, SUBSTR(u.newuniv8cd, 5, 2), e.facultycd) facultycd,
							NVL2(u.newuniv8cd, SUBSTR(u.newuniv8cd, 7, 4), e.deptcd) deptcd,
							e.entexammodecd entexammodecd,
							e.entexamdiv1 entexamdiv1,
							e.entexamdiv2 entexamdiv2,
							e.entexamdiv3 entexamdiv3,
							LATEST_MMDD(
								e.examplandate1, e.examplandate2,
								e.examplandate3, e.examplandate4,
								e.examplandate5, e.examplandate6,
								e.examplandate7, e.examplandate8,
								e.examplandate9, e.examplandate10) examplandate,
							m.maxyear
						FROM
							basicinfo b,
							historyinfo h,
							countchargeclass c,
							examplanuniv e,
							univcdhookup u,
							(SELECT MAX(year) maxyear FROM scheduledetail) m
						WHERE
							b.schoolcd = ?
						AND
							h.year = ?
						AND
							h.individualid = b.individualid
						AND
							h.grade >= 3
						AND
							h.grade = c.grade
						AND
							h.class = c.class
						AND
							e.individualid = b.individualid
						AND
							u.year(+) = e.year
						AND
							u.olduniv8cd(+) = e.univcd || e.facultycd || e.deptcd
						AND
							u.oldexamdiv(+) = e.examdiv
					) t
				LEFT OUTER JOIN
					scheduledetail s
				ON
					s.year = t.maxyear
				AND
					s.univcd = t.univcd
				AND
					s.facultycd = t.facultycd
				AND
					s.deptcd = t.deptcd
				AND
					s.entexamdiv1 = t.entexamdiv1
				AND
					s.entexamdiv2 = t.entexamdiv2
				AND
					s.entexamdiv3 = t.entexamdiv3
				WHERE
					t.univcd <> '9999'
				AND
					t.facultycd <> '99'
				AND
					t.deptcd <> '9999'
			) t,
			univmaster_basic u
		WHERE
			t.entexamdiv1 = t.f_entexamdiv1
		AND
			t.entexamdiv2 = t.f_entexamdiv2
		AND
			t.entexamdiv3 = t.f_entexamdiv3
		AND
			u.eventyear = (SELECT MAX(eventyear) FROM univmaster_basic)
		AND
			u.examdiv = (
				SELECT
					MAX(examdiv)
				FROM
					univmaster_basic
				WHERE
					eventyear = (SELECT MAX(eventyear) FROM univmaster_basic)
			)
		AND
			u.univcd = t.univcd
		AND
			u.facultycd = t.facultycd
		AND
			u.deptcd = t.deptcd
	)
WHERE
	rank <= 15
ORDER BY
	grade, class, class_no, sex, name_kana, rank

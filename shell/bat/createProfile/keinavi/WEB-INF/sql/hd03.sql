INSERT INTO
	ko_attributeitem
SELECT
	?,
	d.attributecd,
	d.attriitemcd,
	d.attriitemname,
	d.dispsequence
FROM
	ko_dafaultattributeitem d
WHERE
	NOT EXISTS (
		SELECT
			1
		FROM
			ko_attributeitem a
		WHERE
			a.schoolcd = ?
		AND
			a.attributecd = d.attributecd
		AND
			a.attriitemcd = d.attriitemcd)

SELECT /*+ ORDERED USE_NL(b h) USE_NL(ri ir) INDEX(b PK_BASICINFO) INDEX(ri SUBRECORD_I_2010_IDX) INDEX(ir S_INDIVIDUALID_IDX) */
	ir.answersheet_no,
	ri.examyear,
	ri.examcd,
	b.schoolcd,
	h.grade,
	h.class,
	h.class_no,
	b.name_kana,
	ri.subcd,
	es.subname,
	es.suballotpnt, /* 11 */
	ri.cvsscore, /* 12 */
	ri.score,
	NVL(ri.a_deviation, 0),
	es.dispsequence
FROM
	subrecord_i ri,
	basicinfo b,
	historyinfo h,
	individualrecord ir,
	examsubject es
WHERE
	ri.examyear = ?
AND
	ri.examcd = ?
AND
	ri.bundlecd = ?
AND
	b.individualid = ri.individualid
AND
	h.year = ri.examyear
AND
	h.individualid = ri.individualid
AND
	ir.examyear = ri.examyear
AND
	ir.examcd = ri.examcd
AND
	ir.s_individualid = ri.individualid
AND
	es.examyear(+) = ri.examyear
AND
	es.examcd(+) = ri.examcd
AND
	es.subcd(+) = ri.subcd

ORDER BY
	h.grade,
	h.class,
	h.class_no
INSERT INTO
	subdistrecord_c
SELECT /*+ USE_NL(ri rc) */
	ri.examyear,
	ri.examcd,
	ri.bundlecd,
	ri.grade,
	ri.class,
	ri.subcd,
	dz.devzonecd,
	COUNT(ri.individualid),
	ROUND(COUNT(ri.individualid) / rc.numbers * 100, 1)
FROM (
	SELECT
		ri.individualid individualid,
		ri.examyear examyear,
		ri.examcd examcd,
		ri.bundlecd bundlecd,
		re.grade grade,
		re.class class,
		re.combisubcd subcd,
		ri.score score
	FROM
		v_subrecount re
	INNER JOIN
		subrecord_i ri
	ON
		ri.examyear = re.examyear
	AND
		ri.examcd = re.examcd
	AND
		ri.subcd = re.subcd
	AND
		ri.bundlecd = re.bundlecd
	WHERE
		re.examyear = ?
	AND
		re.examcd = ?
	AND
		re.bundlecd = ?
	AND
		EXISTS (
			SELECT
				1
			FROM
				historyinfo hi
			WHERE
				hi.individualid = ri.individualid
			AND
				hi.year = ri.examyear
			AND
				hi.grade = re.grade
			AND
				hi.class = re.class)
	UNION ALL
	SELECT
		pp.answersheet_no,
		pp.examyear,
		pp.examcd,
		pp.schoolcd,
		pp.grade,
		pp.class,
		re.combisubcd,
		pp.score
	FROM
		v_subrecount re
	INNER JOIN
		subrecord_pp pp
	ON
		pp.examyear = re.examyear
	AND
		pp.examcd = re.examcd
	AND
		pp.subcd = re.subcd
	AND
		pp.schoolcd = re.bundlecd
	AND
		pp.grade = re.grade
	AND
		pp.class = re.class
	WHERE
		re.examyear = ?
	AND
		re.examcd = ?
	AND
		re.bundlecd = ?) ri
INNER JOIN
	scorezone dz
ON
	NVL(dz.highlimit, 9999.9) >= ri.score
AND
	NVL(dz.lowlimit, -9999.9) <= ri.score
INNER JOIN
	subrecord_c rc
ON
	rc.examyear = ri.examyear
AND
	rc.examcd = ri.examcd
AND
	rc.bundlecd = ri.bundlecd
AND
	rc.grade = ri.grade
AND
	rc.class = ri.class
AND
	rc.subcd = ri.subcd
GROUP BY
	ri.examyear,
	ri.examcd,
	ri.bundlecd,
	ri.grade,
	ri.class,
	ri.subcd,
	dz.devzonecd,
	rc.numbers

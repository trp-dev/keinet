SELECT
	s.bundlename,
	NVL(rs.numbers, -999),
	NVL(rs.avgscorerate, -999)
FROM
	school s

LEFT OUTER JOIN
	school b
ON
	b.bundlecd = s.bundlecd
AND
	NVL(b.newgetdate, '00000000') <= SUBSTR(?, 1, 8) /* 1 */
AND
	NVL(b.stopdate, '99999999') > SUBSTR(?, 1, 8) /* 2 */

LEFT OUTER JOIN
	qrecord_s rs

ON
	rs.examyear = ? /* 3 */
AND
	rs.examcd = ? /* 4 */
AND
	rs.subcd = ? /* 5 */
AND
	rs.question_no = ? /* 6 */
AND
	rs.bundlecd = b.bundlecd

WHERE
	s.bundlecd = ? /* 7 */

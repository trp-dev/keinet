SELECT /*+ ORDERED */
	e.examyear examyear,
	e.examcd,
	e.examname,
	e.examtypecd,
	e.tergetgrade,
	e.out_dataopendate,
	e.dispsequence dispsequence
FROM (
	SELECT /*+ ORDERED INDEX(b BASICINFO_IDX) USE_NL(b h) USE_NL(h cc) FULL(cc) USE_NL(b ri) */
		ri.examyear examyear,
		ri.examcd examcd
	FROM
		basicinfo b
	INNER JOIN
		historyinfo h
	ON
		h.individualid = b.individualid
	AND
		h.year = ?
	INNER JOIN
		countchargeclass cc
	ON
		cc.grade = h.grade
	AND
		cc.class = h.class
	INNER JOIN
		subrecord_i ri
	ON
		ri.individualid = b.individualid
	WHERE
		b.schoolcd = ?
	UNION
	SELECT /*+ ORDERED INDEX(b BASICINFO_IDX) USE_NL(b h) USE_NL(h cg) USE_NL(cg cc) FULL(cc) USE_NL(b ri) */
		ri.examyear,
		ri.examcd
	FROM
		basicinfo b
	INNER JOIN
		historyinfo h
	ON
		h.individualid = b.individualid
	AND
		h.year = ?
	INNER JOIN
		countchargeclass cc
	ON
		cc.grade = h.grade
	AND
		LENGTH(cc.class) > 2
	INNER JOIN
		class_group cg
	ON
		cg.year = h.year
	AND
		cg.bundlecd = b.schoolcd
	AND
		cg.grade = cc.grade
	AND
		cg.class = h.class
	AND
		cg.classgcd = cc.class
	INNER JOIN
		subrecord_i ri
	ON
		ri.individualid = b.individualid
	WHERE
		b.schoolcd = ?) t
INNER JOIN
	examination e
ON
	e.examyear = t.examyear
AND
	e.examcd = t.examcd
INNER JOIN
	school s
ON
	s.bundlecd = ?
AND
	NVL(s.newgetdate, '00000000') <= SUBSTR(e.out_dataopendate, 1, 8)
AND
	NVL(s.stopdate, '99999999') > SUBSTR(e.out_dataopendate, 1, 8)
AND
	SUBSTR(e.out_dataopendate, 1, 8) <= ?
AND
	e.examyear <= ?
ORDER BY
	examyear DESC,
	dispsequence

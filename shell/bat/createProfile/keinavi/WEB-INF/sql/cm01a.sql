SELECT /*+ ORDERED INDEX(b BASICINFO_IDX) INDEX(h PK_HISTORYINFO) USE_NL(b h) */
	h.year year,
	h.grade,
	h.class,
	h.class || '�N���X' classname,
	COUNT(b.individualid),
	DECODE (h.grade, 3, 1, 1, 3, h.grade) grade_seq
FROM
	basicinfo b
INNER JOIN
	historyinfo h
ON
	h.individualid = b.individualid
AND
	h.year IN (#)
WHERE
	b.schoolcd = ? /* 1 */
AND
	(
		 NOT EXISTS (
			SELECT
				c.rowid
			FROM
				login_charge c
			WHERE
				c.schoolcd = b.schoolcd
			AND
				c.loginid = ?
		) OR EXISTS (
			SELECT
				c.rowid
			FROM
				login_charge c
			WHERE
				c.schoolcd = b.schoolcd
			AND
				c.loginid = ?
			AND
				c.year = h.year
			AND
				c.grade = h.grade
			AND
				c.class = h.class
		)
	)
GROUP BY
	h.year,
	h.grade,
	h.class

SELECT
	*
FROM
(
/*--- 校内全体 ---*/
SELECT
	 /* 集計用比較対象クラス.クラス */
	 '校内全体' AS class
	 /* クラス表示順 */
	,0 AS dispsequence
	 /* 英語参加試験別・合計CEFR取得状況（高校）.人数 */
	,ecass.numbers AS numbers
	 /* 英語参加試験別・合計CEFR取得状況（高校）.構成比 */
	,ecass.compratio AS compratio
	 /* 受験有無フラグ */
	,CASE WHEN
		NVL((
			SELECT COUNT(*)
			FROM
				engpt_cefracqstatus_s f
			WHERE
				f.EXAMYEAR = ex.EXAMYEAR
			AND
				f.EXAMCD = ex.EXAMCD
			AND
				f.BUNDLECD = ? /* 1 */
			AND
				f.ENGPTCD = ? /* 2 */
			AND
				f.ENGPTLEVELCD = ? /* 3 */
	 	),0) > 0 THEN '1' ELSE '0' END AS flg
FROM
	examination ex
LEFT JOIN
	engpt_cefracqstatus_s ecass
ON
	ecass.EXAMYEAR = ex.EXAMYEAR
AND
	ecass.EXAMCD = ex.EXAMCD
AND
	ecass.BUNDLECD = ? /* 4 */
AND
	ecass.ENGPTCD = ? /* 5 */
AND
	ecass.ENGPTLEVELCD = ? /* 6 */
AND
	ecass.CEFRLEVELCD = ? /* 7 */
WHERE
	ex.EXAMYEAR = ? /* 8 */
AND
	ex.EXAMCD = ? /* 9 */

UNION ALL
/*--- 選択している個別クラス ---*/

SELECT
	 /* 集計用比較対象クラス.クラス */
	 TRIM(ccc.CLASS) AS class
	 /* クラス表示順 */
	,ccc.DISPSEQUENCE AS dispsequence
	 /* 英語参加試験別・合計CEFR取得状況（高校）.人数 */
	,ecasc.numbers AS numbers
	 /* 英語参加試験別・合計CEFR取得状況（高校）.構成比 */
	,ecasc.compratio AS compratio
	 /* 受験有無フラグ */
	,CASE WHEN ecasc.EXAMYEAR IS NULL THEN '0' ELSE '1' END AS flg
FROM
	countcompclass ccc
INNER JOIN
	examination ex
ON
	ex.EXAMYEAR = ? /* 10 */
AND
	ex.EXAMCD = ? /* 11 */
LEFT JOIN
	engpt_cefracqstatus_c ecasc
ON
	ecasc.EXAMYEAR = ex.EXAMYEAR
AND
	ecasc.EXAMCD = ex.EXAMCD
AND
	ecasc.BUNDLECD = ? /* 12 */
AND
	ecasc.ENGPTCD = ? /* 13 */
AND
	ecasc.ENGPTLEVELCD = ? /* 14 */
AND
	ecasc.CEFRLEVELCD = ? /* 15 */
AND
	ecasc.GRADE = ccc.GRADE
AND
	ecasc.CLASS = ccc.CLASS
WHERE NOT EXISTS (
	SELECT *
	FROM
		class_group cg
	WHERE
		cg.YEAR = ex.EXAMYEAR
	AND cg.BUNDLECD = ? /* 16 */
	AND cg.GRADE = ccc.GRADE
	AND cg.CLASSGCD = ccc.CLASS
)

UNION ALL

/*--- 選択している複合クラス ---*/
SELECT
	 /* 集計用比較対象クラス.クラス */
	 TRIM(MAX(gp.CLASSGNAME)) AS class
	 /* クラス表示順 */
	,MAX(ccc.DISPSEQUENCE) AS dispsequence
	 /* 英語参加試験別・合計CEFR取得状況（高校）.人数 */
	,SUM(ecasc.numbers) AS numbers
	 /* 英語参加試験別・合計CEFR取得状況（高校）.構成比 */
	,SUM(ecasc.compratio) AS compratio
	 /* 受験有無フラグ */
	,CASE WHEN MAX(ecasc.EXAMYEAR) IS NULL THEN '0' ELSE '1' END AS flg
FROM
	countcompclass ccc
INNER JOIN
	examination ex
ON
	ex.EXAMYEAR = ? /* 17 */
AND
	ex.EXAMCD = ? /* 18 */
INNER JOIN
	class_group cg
ON
	cg.YEAR = ex.EXAMYEAR
AND
	cg.BUNDLECD = ? /* 19 */
AND
	cg.GRADE = ccc.GRADE
AND
	cg.CLASSGCD = ccc.CLASS
INNER JOIN
	classgroup gp
ON
	gp.CLASSGCD = cg.CLASSGCD
LEFT JOIN
	engpt_cefracqstatus_c ecasc
ON
	ecasc.EXAMYEAR = ex.EXAMYEAR
AND
	ecasc.EXAMCD = ex.EXAMCD
AND
	ecasc.BUNDLECD = ? /* 20 */
AND
	ecasc.ENGPTCD = ? /* 21 */
AND
	ecasc.ENGPTLEVELCD = ? /* 22 */
AND
	ecasc.CEFRLEVELCD = ? /* 23 */
AND
	ecasc.GRADE = ccc.GRADE
AND
	ecasc.CLASS = cg.CLASS
GROUP BY
	 ccc.GRADE
	,ccc.CLASS
ORDER BY
	dispsequence
)
WHERE
	/* 校内全体を含めて21まで出力 */
	ROWNUM <= 21

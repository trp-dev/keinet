SELECT /*+ ORDERED */
	ri.answersheet_no answersheet_no,
	ri.examyear examyear,
	ri.examcd examcd,
	ri.grade grade,
	UPPER(ri.class) class,
	ri.class_no class_no,
	ri.sex sex,
	TRIM(ri.name_kana) name_kana,
	TRIM(ri.name_kanji) name_kanji,
	CONVERT_BIRTHDAY(ri.birthday) birthday,
	TRIM(ri.tel_no) tel_no,
	ri.s_individualid,
	DECODE(ri.grade, 3, 1, 1, 3, ri.grade) grade_seq
FROM
	school s
INNER JOIN
	individualrecord ri
ON
	ri.bundlecd = s.bundlecd
AND
	ri.examyear = ?
AND
	ri.examcd = ?
INNER JOIN
	examination e
ON
	e.examyear = ri.examyear
AND
	e.examcd = ri.examcd
AND
	e.examyear >= TO_CHAR(ADD_MONTHS(SYSDATE, -27), 'YYYY')
AND
	e.out_dataopendate <= TO_CHAR(SYSDATE, 'YYYYMMDD') || '0000'
AND
	NVL2(s.newgetdate, s.newgetdate || '0000', '000000000000') <= e.out_dataopendate
AND
	NVL2(s.stopdate, s.stopdate || '0000', '999999999999') > e.out_dataopendate
WHERE
	(s.bundlecd = ? OR s.parentschoolcd = ?)
AND
	ri.grade = ?
AND
	UPPER(ri.class) = ?
AND
	ri.privercyflg = '0'

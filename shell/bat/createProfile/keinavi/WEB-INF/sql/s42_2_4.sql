SELECT
	NVL(d.highlimit, -999) highlimit,
	NVL(d.lowlimit, -999) lowlimit,
	NVL(rs.numbers, -999) numbers,
	NVL(rs.compratio, -999) compratio
FROM
	deviationzone d
LEFT OUTER JOIN
	subdistrecord_s rs
ON
	rs.examyear = ?
AND
	rs.examcd = ?
AND
	rs.devzonecd = d.devzonecd
AND
	rs.bundlecd = ?
AND
	rs.subcd = ?
ORDER BY
	lowlimit DESC

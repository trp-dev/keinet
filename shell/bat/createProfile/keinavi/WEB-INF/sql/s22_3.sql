SELECT
	e.examyear examyear,
	NVL(rs.numbers, -999),
	NVL(rs.avgpnt, -999),
	NVL(rs.avgdeviation, -999)
FROM
	examcdtrans e
LEFT OUTER JOIN
	existsexam ee
ON
	ee.examyear = e.examyear
AND
	ee.examcd = e.examcd
LEFT OUTER JOIN
	subcdtrans st
ON
	st.examyear = ee.examyear
AND
	st.examcd = e.curexamcd
AND
	st.cursubcd = ? /* 1 */
LEFT OUTER JOIN
	subrecord_s rs
ON
	rs.examyear = e.examyear
AND
	rs.examcd = e.examcd
AND
	rs.subcd = st.subcd
AND
	rs.bundlecd = ? /* 2 */
WHERE
	e.examyear IN (#)
AND
	e.curexamcd = ?/* 3 */
ORDER BY
	examyear DESC

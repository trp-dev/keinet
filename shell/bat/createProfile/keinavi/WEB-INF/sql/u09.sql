UPDATE
	loginid_manage 
SET
	loginname = ?
WHERE
	schoolcd = ?
AND
	loginid = ?
AND
	manage_flg <> '1'

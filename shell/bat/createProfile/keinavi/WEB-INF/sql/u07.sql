UPDATE
	loginid_manage a
SET
	a.loginid = ?
WHERE
	a.schoolcd = ?
AND
	a.loginid = ?
AND
	NOT EXISTS (
		SELECT
			1
		FROM
			loginid_manage m
		WHERE
			m.schoolcd = a.schoolcd
		AND
			m.loginid = ?)

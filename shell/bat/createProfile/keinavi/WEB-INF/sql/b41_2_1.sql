SELECT
	TRIM('�i�S���j'),
	NVL(p.numbers, -999),
	NVL(p.avgpnt, -999),
	NVL(p.avgdeviation, -999),
	NVL(ra.numbers, -999),
	NVL(ra.avgpnt, -999),
	NVL(ra.avgdeviation, -999)
FROM (
	SELECT
		ra.numbers numbers,
		ra.avgpnt avgpnt,
		ra.avgdeviation avgdeviation,
		ra.studentdiv studentdiv,
		TO_CHAR(et.examyear - 1) lastexamyear,
		et.curexamcd lastexamcd,
		st.cursubcd lastsubcd
	FROM
		examcdtrans et
	LEFT OUTER JOIN
		subcdtrans st
	ON
		st.examyear = et.examyear
	AND
		st.examcd = et.curexamcd
	AND
		st.cursubcd = ?
	LEFT OUTER JOIN
		subrecord_a ra
	ON
		ra.examyear = et.examyear
	AND
		ra.examcd = et.examcd
	AND
		ra.subcd = st.subcd
	AND
		ra.studentdiv = ?
	WHERE
		et.examyear = (? + 1)
	AND
		et.curexamcd = ?) p
LEFT OUTER JOIN
	subrecord_a ra
ON
	ra.examyear = p.lastexamyear
AND
	ra.examcd = p.lastexamcd
AND
	ra.subcd = ?
AND
	ra.studentdiv = ?

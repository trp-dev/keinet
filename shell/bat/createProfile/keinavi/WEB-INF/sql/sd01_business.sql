SELECT
	uf.uploadfile_id,
	uf.schoolcd,
	uf.loginid,
	uf.update_date,
	uf.uploadfilename,
	uf.serverfilename,
	uf.filetype_id,
	uf.upload_comment,
	uf.status,
	uf.last_download_date,
	s.bundlename,
	s.bundlename_kana,
	ufk.filetype_name,
	ufk.filetype_abbrname
FROM
	uploadfile uf
LEFT JOIN
	uploadfilekind ufk
ON
	uf.filetype_id = ufk.filetype_id
LEFT JOIN
	school s
ON
	s.bundlecd = uf.schoolcd
WHERE
	uf.status = '1'
AND
	uf.update_date > ADD_MONTHS(SYSDATE, -?)
AND
	uf.schoolcd IN (
		SELECT 
			cs.schoolcd 
		FROM 
			correspondingschool  cs
		WHERE
			EXISTS(
				SELECT 
					'1'
				FROM
					sectorsorting ss
				WHERE 
					cs.sectorsortingcd = ss.sectorsortingcd
				AND
					ss.hsbusidivcd = '1' 
				AND 
					ss.sectorcd = ?
			)			
		)
ORDER BY 
	/* CONDITION */

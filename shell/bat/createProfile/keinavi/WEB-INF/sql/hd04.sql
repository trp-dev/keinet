INSERT INTO
	ko_examtype
SELECT
	?,
	d.examtypecd,
	d.examtypename,
	d.examdivcd
FROM
	ko_examtypeini d
WHERE
	NOT EXISTS (
		SELECT
			1
		FROM
			ko_examtype a
		WHERE
			a.schoolcd = ?
		AND
			a.examtypecd = d.examtypecd)

SELECT
	tn.eventyear examyear, 
	MAX(tn.examdiv)
FROM 
	univmaster_basic tn 
WHERE 
	tn.eventyear = (
		SELECT 
			MAX(tn2.eventyear) 
		FROM 
			univmaster_basic tn2
	)
GROUP BY tn.eventyear

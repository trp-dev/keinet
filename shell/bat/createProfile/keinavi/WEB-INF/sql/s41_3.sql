SELECT
	s.bundlename,
	NVL(rs.numbers, -999),
	NVL(rs.avgpnt, -999),
	NVL(rs.avgdeviation, -999)
FROM
	subrecord_s rs,
	school s,
	examination e
WHERE
	rs.examyear(+) = ? /* 1 */
AND
	rs.examcd(+) = ? /* 2 */
AND
	rs.bundlecd(+) = s.bundlecd
AND
	rs.subcd(+) = ? /* 3 */
AND
	s.bundlecd = ? /* 4 */
AND
	e.examyear = ? /* 5 */
AND
	e.examcd = ? /* 6 */
AND
	NVL(s.newgetdate, '00000000') <= SUBSTR(e.out_dataopendate, 1, 8)
AND
	NVL(s.stopdate, '99999999') > SUBSTR(e.out_dataopendate, 1, 8)

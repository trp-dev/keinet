SELECT /*+ ORDERED USE_NL(b h) USE_NL(cr ri) INDEX(b BASICINFO_IDX) INDEX(ri S_INDIVIDUALID_IDX) INDEX(cr CANDIDATERATING_2010_IDX) */
	ri.answersheet_no answersheet_no,
	cr.candidaterank candidaterank,
	cr.univcd || cr.facultycd || cr.deptcd,
	' ' superabbrname,
	cr.facultycd,
	cr.deptcd,
	cr.ratingcvsscore,
	cr.ratingdeviation,
	cr.centerrating,
	cr.secondrating,
	cr.ratingpoint,
	cr.totalrating,
	u.unigdiv schedulecd
FROM
	candidaterating cr,
	basicinfo b,
	historyinfo h,
	individualrecord ri,
	univmaster_basic u

WHERE
	cr.examyear = ?
AND
	cr.examcd = ?
AND
	cr.bundlecd = ?
AND
	b.individualid = cr.individualid
AND
	h.individualid = cr.individualid
AND
	h.year = cr.examyear
AND
	ri.s_individualid = cr.individualid
AND
	ri.examyear = cr.examyear
AND
	ri.examcd = cr.examcd
AND
	u.eventyear = cr.examyear
AND
	u.examdiv = ?
AND
	u.univcd = cr.univcd
AND
	u.facultycd = cr.facultycd
AND
	u.deptcd = cr.deptcd

ORDER BY
	answersheet_no,
	candidaterank

SELECT
	m.univcd univcd,
	m.univname_abbr,
	m.univdivcd,
	m.prefcd_hq,
	NVL(rs.totalcandidatenum, 0) totalcandidatenum
FROM 
	(
		SELECT
			u.eventyear examyear,
			u.univcd univcd,
			u.uniname_abbr univname_abbr,
			u.unidiv univdivcd,
			u.prefcd_examho prefcd_hq,
			u.facultycd facultycd1,
			u.deptcd deptcd1,
			FIRST_VALUE (u.facultycd) OVER (PARTITION BY u.univcd) facultycd2,
			FIRST_VALUE (u.deptcd) OVER (PARTITION BY u.univcd) deptcd2
			/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD START */
			 , u.univname_kana
			/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD END */
		FROM
			univmaster_basic u
		WHERE
			u.eventyear = ? /* 1 */
		AND
			u.examdiv = ? /* 2 */
		# /* �i���ݏ��� */
	) m,
	ratingnumber_s rs
WHERE
	m.facultycd1 = m.facultycd2
AND
	m.deptcd1 = m.deptcd2
AND
	rs.univcd(+) = m.univcd
AND
	rs.examyear(+) = m.examyear
AND
	rs.examcd(+) = ?
AND
	rs.ratingdiv(+) = ?
AND
	rs.bundlecd(+) = ?
AND
	rs.univcountingdiv(+) = '0'
AND
	rs.studentdiv(+) = '0'
AND
	rs.facultycd(+) = 'ZZ'
AND
	rs.deptcd(+) = 'ZZZZ'
AND
	rs.agendacd(+) = 'Z'

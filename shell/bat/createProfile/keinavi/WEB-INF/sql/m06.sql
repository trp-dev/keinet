UPDATE
	individualrecord ri
SET
	ri.grade = ?, /* 1 */
	ri.class = ?, /* 2 */
	ri.class_no = ?, /* 3 */
	ri.sex = ?, /* 4 */
	ri.name_kanji = ?, /* 5 */
	ri.name_kana = ?, /* 6 */
	ri.birthday = ?, /* 7 */
	ri.tel_no = ?, /* 8 */
	ri.s_individualid = ?, /* 9 */
	ri.s_statusflg = '1'
WHERE
	ri.examyear = ? /* 10 */
AND
	ri.examcd = ? /* 11 */
AND
	ri.answersheet_no = ? /* 12 */

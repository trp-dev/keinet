SELECT /*+ ORDERED INDEX(bi BASICINFO_IDX) INDEX(ep PK_EXAMPLANUNIV) USE_NL(bi ep) USE_NL(ep uh) */ 
	ep.individualid individualid,
	ep.univcd oldunivcd,
	ep.facultycd oldfacultycd,
	ep.deptcd olddeptcd,
	ep.entexammodecd entexammodecd,
	ep.entexamdiv1 entexamdiv1,
	ep.entexamdiv2 entexamdiv2,
	ep.entexamdiv3 entexamdiv3,
	NVL(SUBSTR(uh.newuniv8cd, 1, 4), ep.univcd) newunivcd,
	NVL(SUBSTR(uh.newuniv8cd, 5, 2), ep.facultycd) newFacultycd,
	NVL(SUBSTR(uh.newuniv8cd, 7, 4), ep.deptcd) newdeptcd
FROM
	basicinfo bi
INNER JOIN
	examplanuniv ep
ON
	ep.individualid = bi.individualid
AND
	(ep.year <> ? OR ep.examdiv <> ?)
LEFT OUTER JOIN
	univcdhookup uh
ON
	uh.year = ep.year
AND
	uh.oldexamdiv = ep.examdiv
AND
	uh.olduniv8cd = (ep.univcd || ep.facultycd || ep.deptcd)
INNER JOIN
	univmaster_basic un
ON
	un.eventyear = ?
AND
	un.examdiv = ?
AND
	un.univcd = NVL(SUBSTR(uh.newuniv8cd, 1, 4), ep.univcd)
AND
	un.facultycd = NVL(SUBSTR(uh.newuniv8cd, 5, 2), ep.facultycd)
AND
	un.deptcd = NVL(SUBSTR(uh.newuniv8cd, 7, 4), ep.deptcd)
WHERE
	bi.schoolcd = ?
ORDER BY
	individualid, oldunivcd, oldfacultycd, olddeptcd, entexammodecd, entexamdiv1, entexamdiv2, entexamdiv3

SELECT
  APPLICATION_ID
  , EXAMYEAR
  , EXAMCD
  , EXAMNAME
  , BUNDLECD
  , BUNDLENAME
  , DECODE(PACTDIV, 1, '�_��Z', '��_��Z')
  , TEACHER_NAME
  , APP_COMMENT
  , APP_DATE
  , LAST_DOWNLOAD_DATE
  , APP_STATUS_CODE
  , APP_STATUS_NAME
  , CASE
    WHEN SYSDATE >= OPENDATE
    THEN NULL
    ELSE OPENDATE
    END AS OPENDATE
  , PASSWORD
  , TERGETGRADE
  , DISPSEQUENCE
FROM
  (
    SELECT
      A.APPLICATION_ID
      , E.EXAMYEAR
      , CASE
        WHEN E.EXAMTYPECD = '32'
        THEN CASE
          WHEN SUBSTR(A.EXAMCD, 3, 2) = '_1'
          THEN E.EXAMNAME || '�i�����񓚂P�j'
          WHEN SUBSTR(A.EXAMCD, 3, 2) = '_2'
          THEN E.EXAMNAME || '�i�����񓚂Q�j'
          ELSE E.EXAMNAME || '�i�l���сj'
          END
        ELSE E.EXAMNAME
        END AS EXAMNAME
      , A.EXAMCD
      , E.DISPSEQUENCE
      , E.TERGETGRADE
      , S.BUNDLECD
      , S.BUNDLENAME
      , (
        SELECT
          1
        FROM
          DUAL
        WHERE
          EXISTS(
            SELECT
              1
            FROM
              PACTSCHOOL P
            WHERE
              (
                P.SCHOOLCD = S.BUNDLECD
                OR P.SCHOOLCD = S.PARENTSCHOOLCD
              )
              AND P.PACTDIV = '1'
              AND (
                P.PACTTERM IS NULL
                OR TO_DATE(P.PACTTERM, 'YYYYMMDD') >= SYSDATE
              )
          )
      ) AS PACTDIV
      , A.TEACHER_NAME
      , A.APP_COMMENT
      , A.APP_DATE
      , A.LAST_DOWNLOAD_DATE
      , T.APP_STATUS_CODE
      , T.APP_STATUS_NAME
      , TO_DATE(E.OUT_DATAOPENDATE, 'YYYYMMDDHH24MI') AS OPENDATE
      , A.PASSWORD
    FROM
      EXDATA_APP A
      INNER JOIN EXAMINATION E
        ON E.EXAMYEAR = A.EXAMYEAR
        AND E.EXAMCD = SUBSTR(A.EXAMCD, 1, 2)
      INNER JOIN SCHOOL S
        ON S.BUNDLECD = A.BUNDLECD
      INNER JOIN EXDATA_APP_STATUS T
        ON T.APP_STATUS_CODE = A.APP_STATUS_CODE
    WHERE
      A.CANCEL_FLG IS NULL
      AND A.EXAMYEAR = ?
      AND (A.LAST_DOWNLOAD_DATE IS NULL OR ? = '2')
      AND EXISTS(
        SELECT
          1
        FROM
          EXDATA_APP_SECT S
        WHERE
          S.APPLICATION_ID = A.APPLICATION_ID
          AND S.APP_SECTORCD IN (#)
      )
  )
ORDER BY

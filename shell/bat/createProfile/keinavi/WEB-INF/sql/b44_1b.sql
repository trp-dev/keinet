SELECT DISTINCT
	rp.studentdiv studentdiv,
	rp.univcd univcd,
	u.uniname_abbr univname_abbr,
	rp.facultycd facultycd,
	NULL facultyname_abbr,
	rp.deptcd deptcd,
	NULL deptname_abbr,
	rp.agendacd agendacd,
	s.schedulename schedulename,
	rp.ratingdiv ratingdiv,
	c.univdiv univdiv,
	c.dispsequence dispsequence
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	 , u.univname_kana
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
FROM
	univmaster_basic u,
	ratingnumber_p rp,
	schedule s,
	countuniv c
WHERE
	rp.examyear = ? /* 1 */
AND
	rp.examcd = ? /* 2 */
AND
	rp.univcountingdiv = ? /* 3 */
AND
	s.schedulecd = rp.agendacd
AND
	c.univcd = rp.univcd
AND
	u.univcd = rp.univcd
AND
	u.eventyear = rp.examyear
AND
	u.examdiv = ? /* 4 */
AND
	rp.studentdiv IN (#)
AND
	rp.prefcd IN (#)
AND
	rp.ratingdiv IN (#)
/* SCHEDULE CONDITION_P */

UNION

SELECT DISTINCT
	rs.studentdiv studentdiv,
	rs.univcd univcd,
	u.uniname_abbr univname_abbr,
	rs.facultycd facultycd,
	NULL facultyname_abbr,
	rs.deptcd deptcd,
	NULL deptname_abbr,
	rs.agendacd agendacd,
	s.schedulename schedulename,
	rs.ratingdiv ratingdiv,
	c.univdiv univdiv,
	c.dispsequence dispsequence
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	 , u.univname_kana
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
FROM
	univmaster_basic u,
	ratingnumber_s rs,
	schedule s,
	countuniv c
WHERE
	rs.examyear = ? /* 1 */
AND
	rs.examcd = ? /* 2 */
AND
	rs.univcountingdiv = ? /* 3 */
AND
	s.schedulecd = rs.agendacd
AND
	c.univcd = rs.univcd
AND
	u.univcd = rs.univcd
AND
	u.eventyear = rs.examyear
AND
	u.examdiv = ? /* 4 */
AND
	rs.studentdiv IN (#)
AND
	rs.bundlecd IN (#)
AND
	rs.ratingdiv IN (#)
/* SCHEDULE CONDITION_S */

/* 2016/01/21 QQ)Nishiyama ��K�͉��C DEL START */
/*
ORDER BY
	studentdiv,
	univdiv,
	dispsequence,
	agendacd,
	ratingdiv
 */
/* 2016/01/21 QQ)Nishiyama ��K�͉��C DEL END */

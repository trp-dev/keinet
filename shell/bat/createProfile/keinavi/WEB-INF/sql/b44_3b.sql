SELECT DISTINCT
	rp.studentdiv studentdiv,
	rp.univcd univcd,
	u.uniname_abbr univname_abbr,
	rp.facultycd facultycd,
	u.facultyname_abbr,
	rp.deptcd deptcd,
	NULL deptname_abbr,
	rp.agendacd agendacd,
	s.schedulename schedulename,
	rp.ratingdiv ratingdiv,
	c.univdiv univdiv,
	c.dispsequence dispsequence
	/* 2016/01/20 QQ)Nishiyama ��K�͉��C ADD START */
	  , u.univname_kana
	  , u.uninightdiv
	  , u.facultyconcd
	/* 2016/01/20 QQ)Nishiyama ��K�͉��C DEL END */
FROM
	univmaster_basic u,
/* 2019/11/26 QQ)nagai �p��F�莎�������Ή� UPD START */
	/* 2019/09/30 QQ) ���ʃe�X�g�Ή� UPD START */
	/* v_ratingnumber_p rp, */
	/* 2019/09/30 QQ) ���ʃe�X�g�Ή� UPD END */
    ratingnumber_p rp,
/* 2019/11/26 QQ)nagai �p��F�莎�������Ή� UPD END   */
	schedule s,
	countuniv c
WHERE
	rp.examyear = ? /* 1 */
AND
	rp.examcd = ? /* 2 */
AND
	rp.univcountingdiv = ? /* 3 */
AND
	s.schedulecd = rp.agendacd
AND
	c.univcd = rp.univcd
AND
	u.univcd = rp.univcd
AND
	u.facultycd = rp.facultycd
AND
	u.eventyear = rp.examyear
AND
	u.examdiv = ? /* 4 */
AND
	rp.studentdiv IN (#)
AND
	rp.prefcd IN (#)
AND
	rp.ratingdiv IN (#)
/* SCHEDULE CONDITION_P */

UNION

SELECT DISTINCT
	rs.studentdiv studentdiv,
	rs.univcd univcd,
	u.uniname_abbr univname_abbr,
	rs.facultycd facultycd,
	u.facultyname_abbr,
	rs.deptcd deptcd,
	NULL deptname_abbr,
	rs.agendacd agendacd,
	s.schedulename schedulename,
	rs.ratingdiv ratingdiv,
	c.univdiv univdiv,
	c.dispsequence dispsequence
	/* 2016/01/20 QQ)Nishiyama ��K�͉��C ADD START */
	 , u.univname_kana
	 , u.uninightdiv
	 , u.facultyconcd
	/* 2016/01/20 QQ)Nishiyama ��K�͉��C ADD END */
FROM
	univmaster_basic u,
/* 2019/11/26 QQ)nagai �p��F�莎�������Ή� UPD START */
	/* 2019/09/30 QQ) ���ʃe�X�g�Ή� UPD START */
	/* v_ratingnumber_s rs, */
	/* 2019/09/30 QQ) ���ʃe�X�g�Ή� UPD END */
	ratingnumber_s rs,
/* 2019/11/26 QQ)nagai �p��F�莎�������Ή� UPD END   */
	schedule s,
	countuniv c
WHERE
	rs.examyear = ? /* 1 */
AND
	rs.examcd = ? /* 2 */
AND
	rs.univcountingdiv = ? /* 3 */
AND
	s.schedulecd = rs.agendacd
AND
	c.univcd = rs.univcd
AND
	u.univcd = rs.univcd
AND
	u.facultycd = rs.facultycd
AND
	u.eventyear = rs.examyear
AND
	u.examdiv = ? /* 4 */
AND
	rs.studentdiv IN (#)
AND
	rs.bundlecd IN (#)
AND
	rs.ratingdiv IN (#)
/* SCHEDULE CONDITION_S */

/* 2016/01/21 QQ)Nishiyama ��K�͉��C DEL START */
/*
ORDER BY
	studentdiv,
	univdiv,
	dispsequence,
	facultycd,
	agendacd,
	ratingdiv
 */
/* 2016/01/21 QQ)Nishiyama ��K�͉��C DEL END */

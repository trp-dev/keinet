SELECT /*+ ORDERED USE_NL(b h) USE_NL(b ri) INDEX(b BASICINFO_IDX) INDEX(ri PK_SUBRECORD_I) */
    ri.individualid individualid,
    ri.examyear,
    ri.examcd,
    b.schoolcd,
    h.grade grade,
    h.class class,
    h.class_no class_no,
    b.name_kana,
    ri.subcd,
    es.subname,
    es.suballotpnt, /* 11 */
    ri.cvsscore, /* 12 */
    ri.score,
    ri.a_deviation,
    ri.academiclevel,
    es.dispsequence,
    NVL(ri.scope, '9'),
    ir.EXAMTYPE,
    ir.BUNRICODE
FROM
    basicinfo b,
    historyinfo h,
    countchargeclass c,
    subrecord_i ri,
    examsubject es,
    individualrecord ir
WHERE
    b.schoolcd = ?
AND
    h.year = ?
AND
    h.individualid = b.individualid
AND
    h.grade = c.grade
AND
    h.class = c.class
AND
    ri.examyear = h.year
AND
    ri.examcd = ?
AND
    ri.individualid = h.individualid
AND
    es.examyear(+) = ri.examyear
AND
    es.examcd(+) = ri.examcd
AND
    es.subcd(+) = ri.subcd
AND
    ri.individualid = ir.s_individualid
AND
    ri.examcd = ir.examcd
AND
    ri.examyear = ir.EXAMYEAR

ORDER BY
    grade,
    class,
    class_no,
/* 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START */
    /*individualid*/
    individualid,
    dispsequence
/* 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END */

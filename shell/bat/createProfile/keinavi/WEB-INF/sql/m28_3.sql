UPDATE basicinfo SET
	name_kana = ?,
	name_kanji = ?,
	sex = ?,
	birthday = ?,
	tel_no = ?
WHERE
	individualid = ?
AND
	schoolcd = ?

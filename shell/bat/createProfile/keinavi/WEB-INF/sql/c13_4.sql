SELECT
	v.subcd,
	v.subname,
	v.suballotpnt
FROM
	v_i_cmexamsubject v
WHERE
	v.examyear = ? /* 1 */
AND
	v.examcd = ? /* 2 */
AND
	(
			(v.subcd IN (#)
				AND NOT EXISTS (
					SELECT es.rowid FROM examsubject es
					WHERE es.examyear = v.examyear
					AND es.examcd = v.examcd
					AND es.fusion_subcd = v.subcd))
		OR
			v.fusion_subcd IN(#)
	)
ORDER BY
	v.dispsequence

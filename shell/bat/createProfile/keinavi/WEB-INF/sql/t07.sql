SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(s PK_SUBACADEMIC_I) */
	s.individualid individualid,
	s.examyear,
	s.examcd,
	b.homeschoolcd,
	h.grade grade,
	h.class class,
	h.class_no class_no,
	b.name_kana,
	i.examtype,
	i.bunricode,

	s.sortorder sortorder,
	s.subcd,
	e.subname,
	s.know_skill_scorerate,
	s.think_judge_scorerate,
	s.expression_scorerate

FROM
	basicinfo b,
	historyinfo h,
	countchargeclass c,

	subacademic_i s

	INNER JOIN
		examsubject e
	ON
		e.examyear = s.examyear
	AND
		e.examcd = s.examcd
	AND
		e.subcd = s.subcd

	INNER JOIN
		individualrecord i
	ON
		i.examyear = s.examyear
	AND
		i.examcd = s.examcd

WHERE
	b.schoolcd = ?

AND
	h.individualid = b.individualid
AND
	h.year = ?
AND
	h.grade = c.grade
AND
	h.class = c.class

AND
	s.individualid = h.individualid
AND
	s.examyear = h.year
AND
	s.examcd = ?

AND
	e.examyear = s.examyear
AND
	e.examcd = s.examcd
AND
	e.subcd = s.subcd

AND
	i.s_individualid = s.individualid
AND
	i.examyear = e.examyear
AND
	i.examcd = e.examcd
AND
	i.bundlecd = b.schoolcd
AND
	i.grade = h.grade
AND
	i.class = h.class
AND
	i.class_no = h.class_no

ORDER BY
	grade,
	class,
	class_no,
	individualid,
	sortorder
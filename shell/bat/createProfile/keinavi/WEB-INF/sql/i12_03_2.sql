SELECT /*+ INDEX(si pk_subrecord_i) */
	es.subname,
	es.suballotpnt,
	NVL(si.score, -999) sscore,
	NVL(TO_CHAR(ROUND((si.score/ es.suballotpnt) * 100, 1), '999.9'), -999.0) scorerate, 
	NVL(TO_CHAR(sdze_a.avgscorerate, '999.9'), -999.0) avgscorerate_level,
	NVL(ss.avgpnt, -999.0) avgpnt_s,
	NVL(ss.avgscorerate, -999.0) avgscorerate_s,
	NVL(sa.avgpnt, -999.0) avgpnt_a,
	NVL(sa.avgscorerate, -999.0) avgscorerate_a
FROM (
	SELECT
		x.examyear,
		x.examcd,
		x.subname,
		x.suballotpnt,
		x.subcd,
		CASE
			WHEN e.examtypecd IN ('02','91') THEN NVL(t2.combisubcd, x.subcd)
			ELSE x.subcd
		END rplsubcd
	FROM
		v_i_qexamsubject x
	INNER JOIN
		examination e
	ON
		x.examyear = e.examyear
	AND
		x.examcd = e.examcd
	LEFT JOIN
	    	subjectcombi t1
	ON
	    	t1.subcd = x.subcd
	LEFT JOIN
		subjectcombi t2
	ON
		t2.combisubcd = t1.combisubcd	
) es
LEFT JOIN 
	subrecord_i si
ON 
	si.examyear = es.examyear
AND 
	si.examcd = es.examcd
AND 
	si.subcd = es.subcd
AND 
	si.individualid = ? /* 1 */
LEFT JOIN 
	subdevzonerecord_a sdze_a
ON 
	sdze_a.examyear = es.examyear 
AND
	sdze_a.examcd = es.examcd 
AND
	sdze_a.subcd = es.subcd 
AND
	sdze_a.devzonecd = ?  /* 2 */
LEFT JOIN 
	subrecord_s ss
ON 
	ss.examyear = es.examyear
AND 
	ss.examcd = es.examcd
AND 
	ss.subcd = es.rplsubcd
AND 
	ss.bundlecd = ? /* 3 */
LEFT JOIN (
	SELECT
		examyear,
		examcd,
		subcd,
		avgpnt,
		avgscorerate
	FROM
		subrecord_a
	WHERE
		studentdiv = (
			SELECT
				min(studentdiv)
			FROM
				subrecord_a
			WHERE
				examyear = ? 
			AND
				examcd = ?
		)
) sa
ON 
	sa.examyear = es.examyear
AND 
	sa.examcd = es.examcd
AND 
	sa.subcd = es.subcd
WHERE
	es.examyear = ? 
AND
	es.examcd = ? 
AND
	es.subcd = ?

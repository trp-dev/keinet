SELECT
	subcd,
	subname,
	numbers
FROM
	(
		SELECT
			es.subcd subcd,
			es.subname subname,
			NVL(ra.numbers, 0) numbers,
			NVL(ra.studentdiv, 0) studentdiv1,
			NVL(MIN(ra.studentdiv) OVER (PARTITION BY ra.subcd), 0) studentdiv2
		FROM
			v_sm_cmexamsubject es,
			subrecord_a ra
		WHERE
			es.examyear = ?
		AND
			es.examcd = ?
		AND
			ra.examyear(+) = es.examyear
		AND
			ra.examcd(+) = es.examcd
		AND
			ra.subcd(+) = es.subcd
	)
WHERE
	studentdiv1 = studentdiv2

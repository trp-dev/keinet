SELECT
	s.bundlename,
	et.examyear examyear,
	rs.subcd subcd,
	NVL(rs.numbers, -999) numbers,
	NVL(rs.avgpnt, -999) avgpnt,
	NVL(rs.avgdeviation, -999) avgdevitaion,
	et.examcd examcd
FROM
	examcdtrans et
INNER JOIN
	school s
ON
	s.bundlecd = ?
LEFT OUTER JOIN
	examination e
ON
	e.examyear = et.examyear
AND
	e.examcd = et.examcd
AND
	NVL2(s.newgetdate, s.newgetdate || '0000', '000000000000') <= e.out_dataopendate
AND
	NVL2(s.stopdate, s.stopdate || '0000', '999999999999') > e.out_dataopendate
LEFT OUTER JOIN
	subcdtrans st
ON
	st.examyear = e.examyear
AND
	st.examcd = et.curexamcd
AND
	st.cursubcd = ?
LEFT OUTER JOIN
	subrecord_s rs
ON
	rs.examyear = e.examyear
AND
	rs.examcd = e.examcd
AND
	rs.subcd = st.subcd
AND
	rs.bundlecd = s.bundlecd
WHERE
	et.examyear IN(#)
AND
	et.curexamcd = ?
ORDER BY
	examyear DESC

SELECT
	es.subcd,
	es.subname,
	es.suballotpnt,
	NVL(rs.numbers, -999),
	NVL(rs.avgpnt, -999),
	NVL(rs.avgdeviation, -999)
FROM
	subrecord_s rs,
	examsubject es
WHERE
	es.examyear = ? /* 1 */
AND
	es.examcd = ? /* 2 */
AND
	es.subcd IN (#)
AND
	rs.examyear(+) = es.examyear
AND
	rs.examcd(+) = es.examcd
AND
	rs.bundlecd(+) = ? /* 3 */
AND
	rs.subcd(+) = es.subcd
ORDER BY
	es.dispsequence

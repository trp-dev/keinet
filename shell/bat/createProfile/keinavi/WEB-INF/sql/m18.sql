SELECT
	a.classgcd,
	a.classgname,
	a.com,
	NVL(b.numbers, 0)
FROM (
	SELECT /*+ ORDERED */
		c.classgcd classgcd,
		c.classgname classgname,
		c.com com
	FROM
		class_group cg
	INNER JOIN
		classgroup c
	ON
		c.classgcd = cg.classgcd
	WHERE
		cg.year = ?
	AND
		cg.bundlecd = ?
	AND
		cg.grade = ?
	GROUP BY
		c.classgcd,
		c.classgname,
		c.com) a
LEFT OUTER JOIN (
	SELECT /*+ ORDERED INDEX(b BASICINFO_IDX) INDEX(h PK_HISTORYINFO) USE_NL(b h) USE_NL(b cg) */
		cg.classgcd classgcd,
		COUNT(*) numbers
	FROM
		basicinfo b
	INNER JOIN
		historyinfo h
	ON
		h.individualid = b.individualid
	AND
		h.year = ?
	AND
		h.grade = ?
	INNER JOIN
		class_group cg
	ON
		cg.year = h.year
	AND
		cg.bundlecd = b.schoolcd
	AND
		cg.grade = h.grade
	AND
		cg.class = h.class
	WHERE
		b.schoolcd = ?
	GROUP BY
		cg.classgcd) b
ON
	b.classgcd = a.classgcd

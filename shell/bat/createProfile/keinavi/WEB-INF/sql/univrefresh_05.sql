DELETE /*+ INDEX(ep PK_EXAMPLANUNIV) */
FROM
	examplanuniv ep
WHERE
	ep.univcd >= '1000'
AND
	ep.individualid IN (
		SELECT
			bi.individualid
		FROM
			basicinfo bi
		WHERE
			bi.schoolcd = ?
	)
AND
	NOT EXISTS (
		SELECT
			1
		FROM 
			univmaster_basic un 
		WHERE 
			un.eventyear = ep.year
		AND
			un.examdiv = ep.examdiv
		AND
			un.univcd = ep.univcd
		AND
			un.facultycd = ep.facultycd
		AND
			un.deptcd = ep.deptcd
	)

SELECT /*+ ORDERED USE_NL(bi hi) */
	hi.grade,
	hi.class,
	hi.class_no,
	bi.name_kana
FROM
	basicinfo bi
LEFT OUTER JOIN
	historyinfo hi
ON
	hi.individualid = bi.individualid
AND
	hi.year = ?
WHERE
	bi.individualid = ?

SELECT
	rc.grade grade,
	rc.class class,
	DECODE(rc.grade, 3, 1, 1, 3, rc.grade) grade_seq
FROM
	subrecord_c rc
WHERE
	rc.examyear = ?
AND
	rc.examcd = ?
AND
	rc.bundlecd = ?
GROUP BY
	rc.grade,
	rc.class
ORDER BY
	grade_seq,
	class

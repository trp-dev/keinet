SELECT /*+ ORDERED */
	h.year year,
	h.grade,
	h.class,
	h.class_no,
	b.name_kana
FROM
	basicinfo b
INNER JOIN
	historyinfo h
ON
	h.individualid = b.individualid
WHERE
	b.schoolcd = ?
AND
	b.individualid = ?
ORDER BY
	year DESC

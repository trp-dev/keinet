SELECT /*+ ORDERED INDEX(ri PK_SUBRECORD_I) */
    c.coursecd,
    c.coursename,
    es.subcd,
    es.subaddrname,
    NVL(ri.a_deviation, -999),
    NVL(a.a_deviation, -999),
    NVL(ri.a_deviation - a.a_deviation, -999),
    NVL(ri.score, -999),
    NVL(a.score, -999),
    ri.academiclevel,
    getcharflagvalue(es.notdisplayflg, 5) basicflg
FROM
    subrecord_i ri,
    examsubject es,
    course c,
    (
        SELECT /*+ ORDERED INDEX(ri PK_SUBRECORD_I) */
            ri.subcd,
            ri.a_deviation,
            ri.score
        FROM
            subrecord_i ri,
            existsexam ee
        WHERE
            ri.examyear = ? /* 1 */
        AND
            ri.examcd = ? /* 2 */
        AND
            ri.individualid = ? /* 3 */
        AND
            ee.examyear = ri.examyear
        AND
            ee.examcd = ri.examcd
    ) a
WHERE
    ri.examyear = ? /* 4 */
AND
    ri.examcd = ? /* 5 */
AND
    ri.individualid = ? /* 6 */
AND
    es.examyear = ri.examyear
AND
    es.examcd = ri.examcd
AND
    getcharflagvalue(es.notdisplayflg, 3) = '0'
AND
    ri.subcd = es.subcd
AND
    a.subcd(+) = es.subcd
AND
    c.year = es.examyear
AND
    c.coursecd = SUBSTR(es.subcd, 1, 1) || '000'
ORDER BY
    c.coursecd,
    basicflg,
    case when ri.examcd in ('01','02','03','04','38') then
        case when ri.scope=9 or ri.scope is null then es.dispsequence
        else case substr(ri.subcd,1,1) when '4' then 4000
                                       when '5' then 5000
             else es.dispsequence
             end
        end
    else es.dispsequence
    end ASC,
    ri.scope desc

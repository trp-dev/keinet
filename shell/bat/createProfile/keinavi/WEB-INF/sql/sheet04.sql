SELECT
	rs.subcd
FROM
	subrecord_s rs
INNER JOIN
	v_sm_cmexamsubject es
ON
	es.examyear = rs.examyear
AND
	es.examcd = rs.examcd
AND
	es.subcd = rs.subcd
WHERE
	rs.examyear = ?
AND
	rs.examcd = ?
AND
	rs.bundlecd = ?

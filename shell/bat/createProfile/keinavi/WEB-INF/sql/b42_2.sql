SELECT
	'�i' || p.prefname || '�j' bundlename,
	et.examyear examyear,
	NVL(rp.numbers, -999) numbers,
	NVL(rp.avgpnt, -999) avgpnt,
	NVL(rp.avgdeviation, -999) avgdeviation,
	rp.subcd subcd,
	rp.examcd examcd
FROM
	examcdtrans et
INNER JOIN
	prefecture p
ON
	p.prefcd = ?
LEFT OUTER JOIN
	subcdtrans st
ON
	st.examyear = et.examyear
AND
	st.examcd = et.curexamcd
AND
	st.cursubcd = ?
LEFT OUTER JOIN
	subrecord_p rp
ON
	rp.examyear = et.examyear
AND
	rp.examcd = et.examcd
AND
	rp.subcd = st.subcd
AND
	rp.prefcd = p.prefcd
WHERE
	et.examyear IN (#)
AND
	et.curexamcd = ?
ORDER BY
	examyear DESC

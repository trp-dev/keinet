 SELECT  
	 scheduledetail.univcd,  
	 scheduledetail.facultycd,  
	 scheduledetail.deptcd,  
	 univmaster_basic.uniname_abbr univname_abbr,  
	 univmaster_basic.facultyname_abbr,  
	 univmaster_basic.deptname_abbr,  
	 NVL(univmaster_basic.deptsortkey, ' ') deptsortkey,  
	 univmaster_basic.eventyear examyear,  
	 univmaster_basic.examdiv,  
	 entexammode.entexammodecd,  
	 entexammode.entexammodename,  
	 scheduledetail.SCHEDULEBRANCHNAME,
	 scheduledetail.ENTEXAMDIV_1_2ORDER,  
	 scheduledetail.SCHOOLPROVENTDIV,  
	 TRIM(scheduledetail.ENTEXAMDIV_1_2TERM) ENTEXAMDIV_1_2TERM,  
	 scheduledetail.ENTEXAMINPLEDATE1_1,  
	 scheduledetail.ENTEXAMINPLEDATE1_2,  
	 scheduledetail.ENTEXAMINPLEDATE1_3,  
	 scheduledetail.ENTEXAMINPLEDATE1_4,   
	 scheduledetail.ENTEXAMINPLEDATE2_1,  
	 scheduledetail.ENTEXAMINPLEDATE2_2,  
	 scheduledetail.ENTEXAMINPLEDATE2_3,  
	 scheduledetail.ENTEXAMINPLEDATE2_4,   
	 scheduledetail.PLEDATEDIV1_1,  
	 scheduledetail.PLEDATEDIV1_2,  
	 scheduledetail.PLEDATEDIV1_3,   
	 scheduledetail.PLEDATEDIV2_1,  
	 scheduledetail.PLEDATEDIV2_2,  
	 scheduledetail.PLEDATEDIV2_3, 
	 scheduledetail.PLEDATEUNITEDIV,
	 guideremarks.remarks guideremarks,  
	 univmaster_basic.unidiv univdivcd,  
	 univmaster_basic.unigdiv schedulecd,  
	 univmaster_basic.prefcd_examho prefcd_hq  
/* 2016/01/15 QQ)Nishiyama ��K�͉��C ADD START */
	 , NVL(univmaster_info.KANA_NUM, ' ') kana_num
	 , univmaster_basic.UNINIGHTDIV
	 , univmaster_basic.FACULTYCONCD
	 , univmaster_basic.DEPTSERIAL_NO
	 , univmaster_basic.SCHEDULESYS
	 , univmaster_basic.SCHEDULESYSBRANCHCD
	 , univmaster_basic.DEPTNAME_KANA
/* 2016/01/15 QQ)Nishiyama ��K�͉��C ADD END */
 FROM  
 	examscheduledetail scheduledetail  
 INNER JOIN  
 	univmaster_basic  
 ON 
	univmaster_basic.eventyear = scheduledetail.year  
 AND
 	univmaster_basic.examdiv = scheduledetail.examdiv 
 AND 
	univmaster_basic.univcd = scheduledetail.univcd  
 AND 
	univmaster_basic.facultycd = scheduledetail.facultycd  
 AND 
	univmaster_basic.deptcd = scheduledetail.deptcd  
 LEFT JOIN  
 	guideremarks  
 ON 
	scheduledetail.univcd = guideremarks.univcd  
 AND 
	scheduledetail.facultycd = guideremarks.facultycd  
 AND 
	scheduledetail.deptcd = guideremarks.deptcd  
 LEFT JOIN  
 	entexammode  
 ON 
	entexammode.entexammodecd = '01'  
/* 2016/01/15 QQ)Nishiyama ��K�͉��C ADD START */
 LEFT JOIN  
 	univmaster_info  
 ON 
	univmaster_basic.eventyear = univmaster_info.eventyear  
 AND 
	univmaster_basic.univcd = univmaster_info.univcd  
/* 2016/01/15 QQ)Nishiyama ��K�͉��C ADD END */
 WHERE  
 	scheduledetail.year = ?
 AND 
	scheduledetail.examdiv = ?
 AND	
 	scheduledetail.univcd = ?
 AND 
	scheduledetail.facultycd = ?
 AND 
	scheduledetail.deptcd = ?


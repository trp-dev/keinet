SELECT
	/* CEFR���.�b�d�e�q���x���R�[�h */
	ci.cefrlevelcd,
	/* CEFR���.�b�d�e�q���x���Z�k�� */
	CASE WHEN ci.cefrlevelcd = '99' THEN '�Ȃ�' ELSE ci.cerflevelname_abbr END AS cerflevelname_abbr,
	/* CEFR���.���я� */
	ci.sort AS ci_sort
FROM
	/* CEFR���TBL */
	(
		SELECT
			ci.eventyear,
			ci.examdiv,
			ci.cefrlevelcd,
			ci.cerflevelname_abbr,
			ci.sort
		FROM
			cefr_info ci
		UNION ALL
		SELECT
			ci.eventyear,
			ci.examdiv,
			'ZZ' AS cefrlevelcd,
			'���l��' AS cerflevelname_abbr,
			0 AS sort
		FROM
			cefr_info ci
		GROUP BY
			ci.eventyear,
			ci.examdiv) ci
WHERE
	ci.eventyear = ? /* '1' */
AND
	ci.examdiv = ? /* '2' */
ORDER BY
	ci_sort

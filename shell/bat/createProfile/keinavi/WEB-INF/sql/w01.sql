SELECT
	p.profileid,
	p.profilename,
	f.profilefid,
	f.profilefname,
	p.com,
	p.createdate,
	p.renewaldate,
	p.updngflg,
	p.userid,
	p.bundlecd,
	s.bundlename,
	p.sectorsortingcd,
	NVL2(p.userid, NVL(m.loginname, '---'), '�͍��m'),
	o.parameter
FROM
	profile p
INNER JOIN
	profilefolder f
ON
	f.profilefid = p.profilefid
INNER JOIN
	profileparam o
ON
	o.profileid = p.profileid
LEFT OUTER JOIN
	school s
ON
	s.bundlecd = p.bundlecd
LEFT OUTER JOIN
	loginid_manage m
ON
	m.schoolcd = ?
AND
	m.loginid = p.userid
WHERE
	p.profileid = ?

SELECT DISTINCT
	/* Fè±îñ}X^.QÁ±R[h */
	ei.engptcd,
	/* Fè±îñ}X^.QÁ±Zk¼ */
	ei.engptname_abbr,
	/* Fè±îñ}X^.x ètO */
	ei.levelflg,
	/* Fè±îñ}X^.ÀÑ */
	ei.sort AS ei_sort,
	/* Fè±Ú×îñ}X^.QÁ±xR[h */
	edi.engptlevelcd,
	/* Fè±Ú×îñ}X^.QÁ±xZk¼ */
	edi.engptlevelname_abbr,
	/* Fè±Ú×îñ}X^.ÀÑ */
	edi.sort AS edi_sort,
	/* ó±L³tO */
	CASE WHEN ecass.examyear IS NULL THEN '0' ELSE '1' END AS flg_s,
	CASE WHEN ecasp.examyear IS NULL THEN '0' ELSE '1' END AS flg_p,
	CASE WHEN ecasa.examyear IS NULL THEN '0' ELSE '1' END AS flg_a,
	/* ±¼ */
  	CASE
       	WHEN ei.levelflg = 0 THEN TRIM(ei.ENGPTNAME_ABBR)
       	ELSE                      TRIM(ei.ENGPTNAME_ABBR) || ' ' || edi.ENGPTLEVELNAME_ABBR
   	END AS exam_name
FROM
	/* Í}X^TBL */
	examination en
	/* Fè±îñ}X^TBL */
	INNER JOIN engptinfo ei
	ON
		ei.eventyear = en.EXAMYEAR
	AND
		ei.examdiv = en.EXAMDIV
	/* Fè±Ú×îñ}X^TBL */
	INNER JOIN engptdetailinfo edi
	ON
		edi.eventyear = ei.eventyear
	AND
		edi.examdiv = ei.examdiv
	AND
		edi.engptcd = ei.engptcd
	/* pêFè±ÊEvCEFRæ¾óµiwZjTBL */
	LEFT JOIN engpt_cefracqstatus_s ecass
	ON
		ecass.examyear = ei.eventyear
	AND
		ecass.examcd = en.examcd
	AND
		ecass.engptcd = ei.engptcd
	AND
		ecass.bundlecd IN (#) /* 1 */
	AND
		ecass.engptlevelcd = edi.engptlevelcd
	/* pêFè±ÊEvCEFRæ¾óµi§àjTBL */
	LEFT JOIN engpt_cefracqstatus_p ecasp
	ON
		ecasp.examyear = ei.eventyear
	AND
		ecasp.examcd = en.examcd
	AND
		ecasp.engptcd = ei.engptcd
	AND
		ecasp.engptlevelcd = edi.engptlevelcd
    AND
        ecasp.prefcd IN (#) /* 2 */

	/* pêFè±ÊEvCEFRæ¾óµiSjTBL */
	LEFT JOIN engpt_cefracqstatus_a ecasa
	ON
		ecasa.examyear = ei.eventyear
	AND
		ecasa.examcd = en.examcd
	AND
		ecasa.engptcd = ei.engptcd
	AND
		ecasa.engptlevelcd = edi.engptlevelcd
WHERE
	en.EXAMYEAR = ? /* '1' */
AND
	en.EXAMCD = ? /* '2' */
ORDER BY
	ei_sort,
	edi_sort

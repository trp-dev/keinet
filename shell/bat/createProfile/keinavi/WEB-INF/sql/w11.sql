SELECT
	p.profileid
FROM
	profile p
INNER JOIN
	profilefolder f
ON
	f.profilefid = p.profilefid
AND
	f.profilefname = ?
WHERE
	p.profilename = ?
AND
	p.bundlecd = ?
AND
	p.sectorsortingcd IS NULL
ORDER BY
	p.renewaldate DESC

SELECT
	NVL(d.highlimit, -999),
	NVL(d.lowlimit, -999) lowlimit,
	NVL(rs.numbers, -999),
	NVL(rs.compratio, -999)
FROM
	deviationzone d,
	subdistrecord_s rs
WHERE
	rs.examyear(+) = ?
AND
	rs.examcd(+) = ?
AND
	rs.bundlecd(+) = ?
AND
	rs.subcd(+) = ?
AND
	rs.devzonecd(+) = d.devzonecd
ORDER BY
	lowlimit DESC

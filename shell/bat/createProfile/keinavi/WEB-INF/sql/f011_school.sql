SELECT 
	uf.uploadfile_id, 
	uf.schoolcd, 
	uf.loginid, 
	uf.update_date, 
	uf.uploadfilename, 
	uf.serverfilename, 
	uf.filetype_id, 
	uf.upload_comment, 
	uf.status,
	uf.last_download_date,
	ufk.filetype_name,
	ufk.filetype_abbrname
FROM 
	uploadfile uf
LEFT JOIN
	uploadfilekind ufk
ON
	uf.filetype_id = ufk.filetype_id
WHERE 
	uf.schoolcd = ? 
AND 
	uf.status <> '9' 
AND
	uf.update_date > ADD_MONTHS(SYSDATE, -?)
ORDER BY 
	uf.update_date DESC,
	uf.uploadfilename ASC

SELECT
	d.loginid,
	NVL2(d.loginid, NVL(m.loginname, '---'), '�͍��m'),
	TO_DATE(d.time, 'YYYYMMDDHH24MI')
FROM
	declaration d
LEFT OUTER JOIN
	loginid_manage m
ON
	m.schoolcd = d.schoolcd
AND
	m.loginid = d.loginid
WHERE
	d.schoolcd = ?

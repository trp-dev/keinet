SELECT /*+ USE_NL(ra rs) */
	0,
	ra.examyear,
	ra.examcd,
	e.examname,
	1,
	'99',
	TRIM('�S��'),
	'99999',
	TRIM('('|| '�S��' ||')'),
	99,
	'99',
	ra.ratingdiv RATINGDIV,
	ra.univcountingdiv UNIVCOUNTINGDIV,
	ra.univcd UNIVCD,
	ra.facultycd FACULTYCD,
	ra.deptcd DEPTCD,
	u.choicecd5 CHOICECD5,
	DECODE(
		ra.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.uniname_abbr)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = ra.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = ra.univcd
		 ),
		u.uniname_abbr
	) univname_abbr,
	DECODE(
		ra.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.facultyname_abbr)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = ra.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = ra.univcd
			AND
				u.facultycd = ra.facultycd),
		u.facultyname_abbr
	),
	u.deptname_abbr,
	ra.agendacd AGENDACD,
	ra.studentdiv STUDENTDIV,
	ra.totalcandidatenum,
	ra.firstcandidatenum,
	ra.ratingnum_a,
	ra.ratingnum_b,
	ra.ratingnum_c,
	ra.ratingnum_d,
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD START */
/*	ra.ratingnum_e,*/
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� ADD START */
/*	ra.eng_ratingnum_a,*/
/*	ra.eng_ratingnum_b,*/
/*	ra.eng_ratingnum_c,*/
/*	ra.eng_ratingnum_d,*/
/*	ra.eng_ratingnum_e*/
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� ADD END */
	ra.ratingnum_e
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD END */
	/* 2016/03/02 QQ)Nishiyama ��K�͉��C ADD START */
	/* ����w�P�� */
	/* ��w�敪 */
	, DECODE(
		ra.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.unidiv)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = ra.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = ra.univcd
		 ),
		u.unidiv
	) unidiv_sort,
	/* ��w���J�i */
	DECODE(
		ra.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.univname_kana)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = ra.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = ra.univcd
		 ),
		u.univname_kana
	) univname_kana_sort,
	/* ���w���P�� */
	/* ��w��ԋ敪 */
	DECODE(
		ra.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.uninightdiv)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = ra.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = ra.univcd
			AND
				u.facultycd = ra.facultycd
		 ),
		u.uninightdiv
	) uninightdiv_sort,
	/* �w�����e�R�[�h */
	DECODE(
		ra.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.facultyconcd)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = ra.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = ra.univcd
			AND
				u.facultycd = ra.facultycd
		 ),
		u.facultyconcd
	) facultyconcd_sort
	/* 2016/03/02 QQ)Nishiyama ��K�͉��C ADD END */
FROM
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD START */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD START */
	/* v_ratingnumber_s rs */
/*	v_ratingnumber_s rs */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD END */
/*	INNER JOIN */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD START */
	/* v_ratingnumber_a ra */
/*	v_ratingnumber_a ra */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD END */
	ratingnumber_s rs
INNER JOIN
	ratingnumber_a ra
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD END */
ON
	ra.examyear = rs.examyear
AND
	ra.examcd = rs.examcd
AND
	ra.ratingdiv = rs.ratingdiv
AND
	ra.univcountingdiv = rs.univcountingdiv
AND
	ra.univcd = rs.univcd
AND
	ra.facultycd = rs.facultycd
AND
	ra.deptcd = rs.deptcd
AND
	ra.agendacd = rs.agendacd
AND
	ra.studentdiv = rs.studentdiv
INNER JOIN
	examination e
ON
	e.examyear = rs.examyear
AND
	e.examcd = rs.examcd
LEFT OUTER JOIN
	univmaster_basic u
ON
	u.eventyear = ra.examyear
AND
	u.examdiv = e.examdiv
AND
	u.univcd = ra.univcd
AND
	u.facultycd = ra.facultycd
AND
	u.deptcd = ra.deptcd
WHERE
	rs.examyear = ?
AND
	rs.examcd = ?
AND
	rs.bundlecd = ?
ORDER BY
	RATINGDIV,
	UNIVCOUNTINGDIV,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	unidiv_sort,
	univname_kana_sort,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
	UNIVCD,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C UPD START */
	/* FACULTYCD, */
	uninightdiv_sort,
	facultyconcd_sort,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C UPD END */
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	NVL(u.UNIGDIV, ''),
	NVL(u.DEPTSERIAL_NO, ''),
	NVL(u.SCHEDULESYS, ''),
	NVL(u.SCHEDULESYSBRANCHCD, ''),
	NVL(u.DEPTNAME_KANA, ''),
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
	DEPTCD,
	AGENDACD,
	STUDENTDIV

/*
 * S12_11_共通テスト英語認定試験CEFR取得状況 全試験
 * 
 *  SQL結果
 *    横方向:高校、県、全国の人数・構成比、受験有無フラグ
 *    縦方向:CEFRレベルコード
 *    改行:CEFRレベルコードの切り替わり
 */
SELECT
	/* CEFR情報.ＣＥＦＲレベルコード */
	ci.cefrlevelcd,
	/* CEFR情報.ＣＥＦＲレベル短縮名 */
	CASE WHEN ci.cefrlevelcd = '99' THEN 'なし' ELSE ci.cerflevelname_abbr END AS cerflevelname_abbr,
	/* 学校マスタ.一括名 */
	sc.bundlename,
	/* 県マスタ.県名 */
	pre.prefname,
	/* CEFR取得状況（高校）.人数 */
	cass.numbers AS s_numbers,
	/* CEFR取得状況（高校）.構成比 */
	cass.compratio AS s_compratio,
	/* CEFR取得状況（県）.人数 */
	casp.numbers AS p_numbers,
	/* CEFR取得状況（県）.構成比 */
	casp.compratio AS p_compratio,
	/* CEFR取得状況（全国）.人数 */
	casa.numbers AS a_numbers,
	/* CEFR取得状況（全国）.構成比 */
	casa.compratio AS a_compratio,
	/* 受験有無フラグ */
	CASE WHEN cass.bundlecd IS NULL THEN '0' ELSE '1' END AS flg
FROM
	/* CEFR情報TBL */
	(
		SELECT
			ci.eventyear,
			ci.examdiv,
			ci.cefrlevelcd,
			ci.cerflevelname_abbr,
			ci.sort
		FROM
			cefr_info ci
		UNION ALL
		SELECT
			ci.eventyear,
			ci.examdiv,
			'ZZ' AS cefrlevelcd,
			'合計' AS cerflevelname_abbr,
			999 AS sort
		FROM
			cefr_info ci
		GROUP BY
			ci.eventyear,
			ci.examdiv) ci
	/* 模試マスタTBL */
	INNER JOIN examination en
	ON
		en.examyear = ci.eventyear
	AND
		en.examdiv = ci.examdiv
	AND
		en.examcd = ? /* '1' */
	/* 学校マスタ */
	INNER JOIN school sc
	ON
		sc.bundlecd = ? /* '2' */
	/* 県マスタ */
	INNER JOIN prefecture pre
	ON
		pre.prefcd = sc.facultycd
	/* CEFR取得状況（全国）TBL */
	LEFT JOIN cefracquisitionstatus_a casa
	ON
		casa.examyear = ci.eventyear
	AND
		casa.examcd = en.examcd
	AND
		casa.cefrlevelcd = ci.cefrlevelcd
	/* CEFR取得状況（県）TBL */
	LEFT JOIN cefracquisitionstatus_p casp
	ON
		casp.examyear = ci.eventyear
	AND
		casp.examcd = en.examcd
	AND
		casp.cefrlevelcd = ci.cefrlevelcd
	AND
		casp.prefcd = sc.facultycd
	/* CEFR取得状況（学校）TBL */
	LEFT JOIN cefracquisitionstatus_s cass
	ON
		cass.examyear = ci.eventyear
	AND
		cass.examcd = en.examcd
	AND
		cass.cefrlevelcd = ci.cefrlevelcd
	AND
		cass.bundlecd = ? /* '3' */
WHERE
	ci.EVENTYEAR = ? /* '4' */
ORDER BY
	ci.SORT

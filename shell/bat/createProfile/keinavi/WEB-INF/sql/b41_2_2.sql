SELECT
	'�i' || p.prefname || '�j',
	NVL(p.numbers, -999),
	NVL(p.avgpnt, -999),
	NVL(p.avgdeviation, -999),
	NVL(rp.numbers, -999),
	NVL(rp.avgpnt, -999),
	NVL(rp.avgdeviation, -999)
FROM (
	SELECT
		rp.numbers numbers,
		rp.avgpnt avgpnt,
		rp.avgdeviation avgdeviation,
		p.prefcd prefcd,
		p.prefname prefname,
		TO_CHAR(et.examyear - 1) lastexamyear,
		et.curexamcd lastexamcd,
		st.cursubcd lastsubcd
	FROM
		examcdtrans et
	INNER JOIN
		prefecture p
	ON
		p.prefcd = ?
	LEFT OUTER JOIN
		subcdtrans st
	ON
		st.examyear = et.examyear
	AND
		st.examcd = et.curexamcd
	AND
		st.cursubcd = ?
	LEFT OUTER JOIN
		subrecord_p rp
	ON
		rp.examyear = et.examyear
	AND
		rp.examcd = et.examcd
	AND
		rp.subcd = st.subcd
	AND
		rp.prefcd = p.prefcd
	WHERE
		et.examyear = (? + 1)
	AND
		et.curexamcd = ?) p
LEFT OUTER JOIN
	subrecord_p rp
ON
	rp.examyear = p.lastexamyear
AND
	rp.examcd = p.lastexamcd
AND
	rp.subcd = ?
AND
	rp.prefcd = p.prefcd

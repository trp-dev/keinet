SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(h PK_HISTORYINFO) */
	h.year year,
	h.grade,
	DECODE(h.grade, 3, 1, 1, 3, h.grade) grade_seq
FROM
	basicinfo b,
	historyinfo h
WHERE
	b.individualid = h.individualid
AND
	b.schoolcd = ?
UNION
SELECT
	cg.year,
	TO_NUMBER(cg.grade),
	DECODE(cg.grade, 3, 1, 1, 3, TO_NUMBER(cg.grade))
FROM
	class_group cg
WHERE
	cg.bundlecd = ?
ORDER BY
	year DESC,
	grade_seq

SELECT /*+ USE_NL(rs t) */
	0,
	t.examyear,
	t.examcd,
	e.examname,
	3,
	p.prefcd PREFCD,
	p.prefname,
	s.bundlecd BUNDLECD,
	s.bundlename,
	99,
	'99',
	t.ratingdiv RATINGDIV,
	t.univcountingdiv UNIVCOUNTINGDIV,
	t.univcd UNIVCD,
	t.facultycd FACULTYCD,
	t.deptcd DEPTCD,
	u.choicecd5 CHOICECD5,
	DECODE(
		t.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.uniname_abbr)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = t.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = t.univcd
		 ),
		u.uniname_abbr
	) univname_abbr,
	DECODE(
		t.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.facultyname_abbr)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = t.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = t.univcd
			AND
				u.facultycd = t.facultycd),
		u.facultyname_abbr
	),
	u.deptname_abbr,
	t.agendacd AGENDACD,
	t.studentdiv STUDENTDIV,
	t.totalcandidatenum,
	t.firstcandidatenum,
	t.ratingnum_a,
	t.ratingnum_b,
	t.ratingnum_c,
	t.ratingnum_d,
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD START */
/*	t.ratingnum_e, */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� ADD START */
/*	t.eng_ratingnum_a, */
/*	t.eng_ratingnum_b, */
/*	t.eng_ratingnum_c, */
/*	t.eng_ratingnum_d, */
/*	t.eng_ratingnum_e */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� ADD END */
	t.ratingnum_e
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD END */
	/* 2016/03/02 QQ)Nishiyama ��K�͉��C ADD START */
	/* ����w�P�� */
	/* ��w�敪 */
	, DECODE(
		t.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.unidiv)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = t.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = t.univcd
		 ),
		u.unidiv
	) unidiv_sort,
	/* ��w���J�i */
	DECODE(
		t.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.univname_kana)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = t.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = t.univcd
		 ),
		u.univname_kana
	) univname_kana_sort,
	/* ���w���P�� */
	/* ��w��ԋ敪 */
	DECODE(
		t.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.uninightdiv)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = t.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = t.univcd
			AND
				u.facultycd = t.facultycd
		 ),
		u.uninightdiv
	) uninightdiv_sort,
	/* �w�����e�R�[�h */
	DECODE(
		t.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.facultyconcd)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = t.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = t.univcd
			AND
				u.facultycd = t.facultycd
		 ),
		u.facultyconcd
	) facultyconcd_sort
	/* 2016/03/02 QQ)Nishiyama ��K�͉��C ADD END */
FROM
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD START */
	/* ratingnumber_s rs */
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD START */
/*	v_ratingnumber_s rs */
	ratingnumber_s rs
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD END */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD END */
INNER JOIN
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD START */
	/* ratingnumber_s t */
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD START */
/*	v_ratingnumber_s t */
	ratingnumber_s t
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD END */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD END */
ON
	t.examyear = rs.examyear
AND
	t.examcd = rs.examcd
AND
	t.ratingdiv = rs.ratingdiv
AND
	t.univcountingdiv = rs.univcountingdiv
AND
	t.univcd = rs.univcd
AND
	t.facultycd = rs.facultycd
AND
	t.deptcd = rs.deptcd
AND
	t.agendacd = rs.agendacd
AND
	t.studentdiv = rs.studentdiv
AND
	t.bundlecd IN (#)
INNER JOIN
	examination e
ON
	e.examyear = t.examyear
AND
	e.examcd = t.examcd
INNER JOIN
	school s
ON
	s.bundlecd = t.bundlecd
AND
	NVL(s.newgetdate, '00000000') <= SUBSTR(e.out_dataopendate, 1, 8)
AND
	NVL(s.stopdate, '99999999') > SUBSTR(e.out_dataopendate, 1, 8)
INNER JOIN
	prefecture p
ON
	p.prefcd = s.facultycd
LEFT OUTER JOIN
	univmaster_basic u
ON
	u.eventyear = t.examyear
AND
	u.examdiv = e.examdiv
AND
	u.univcd = t.univcd
AND
	u.facultycd = t.facultycd
AND
	u.deptcd = t.deptcd
WHERE
	rs.examyear = ?
AND
	rs.examcd = ?
AND
	rs.bundlecd = ?
ORDER BY
	PREFCD,
	BUNDLECD,
	RATINGDIV,
	UNIVCOUNTINGDIV,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	unidiv_sort,
	univname_kana_sort,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
	UNIVCD,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C UPD START */
	/* FACULTYCD, */
	uninightdiv_sort,
	facultyconcd_sort,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C UPD END */
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	NVL(u.UNIGDIV, ''),
	NVL(u.DEPTSERIAL_NO, ''),
	NVL(u.SCHEDULESYS, ''),
	NVL(u.SCHEDULESYSBRANCHCD, ''),
	NVL(u.DEPTNAME_KANA, ''),
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
	DEPTCD,
	AGENDACD,
	STUDENTDIV

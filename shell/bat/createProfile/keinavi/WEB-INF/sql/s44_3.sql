SELECT
	'（' || p.prefname || '）',
	et.examyear,
	NVL(rp.totalcandidatenum, -999),
	NVL(rp.firstcandidatenum, -999),
	NVL(rp.ratingnum_a, -999),
	NVL(rp.ratingnum_b, -999),
	NVL(rp.ratingnum_c, -999),
	NVL(rp.ratingnum_d, -999),
	NVL(rp.ratingnum_e, -999)
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START */
	/* 2019/09/30 QQ) 共通テスト対応 ADD START */
	/* NVL(rp.eng_ratingnum_a, -999), */
	/* NVL(rp.eng_ratingnum_b, -999), */
	/* NVL(rp.eng_ratingnum_c, -999), */
	/* NVL(rp.eng_ratingnum_d, -999), */
	/* NVL(rp.eng_ratingnum_e, -999)  */
	/* 2019/09/30 QQ) 共通テスト対応 ADD END */
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END   */
FROM
	examcdtrans et
CROSS JOIN
	prefecture p
LEFT OUTER JOIN
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START */
	/* 2019/09/30 QQ) 共通テスト対応 UPD START */
	/* v_ratingnumber_p rp */
	/* 2019/09/30 QQ) 共通テスト対応 UPD END */
	ratingnumber_p rp
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END   */
ON
	rp.examyear = et.examyear
AND
	rp.examcd = et.examcd
AND
	rp.studentdiv = ? /* 1 */
AND
	rp.univcountingdiv = ? /* 2 */
AND
	rp.univcd = ? /* 3 */
AND
	rp.facultycd = ? /* 4 */
AND
	rp.deptcd = ? /* 5 */
AND
	rp.agendacd = ? /* 6 */
AND
	rp.ratingdiv = ? /* 7 */
AND
	rp.prefcd = p.prefcd
WHERE
	et.examyear IN (#)
AND
	et.curexamcd = ? /* 8 */
AND
	p.prefcd = ? /* 9 */
ORDER BY
	et.examyear DESC

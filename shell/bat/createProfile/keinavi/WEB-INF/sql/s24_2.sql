SELECT
	studentdiv,
	univcd,
	univname_abbr,
	facultycd,
	facultyname_abbr,
	deptcd,
	deptname_abbr,
	agendacd,
	schedulename,
	ratingdiv
FROM (
	SELECT
		rs.studentdiv studentdiv,
		rs.univcd univcd,
		u.uniname_abbr univname_abbr,
		rs.facultycd facultycd,
		NULL facultyname_abbr,
		rs.deptcd deptcd,
		NULL deptname_abbr,
		rs.agendacd agendacd,
		NULL schedulename,
		rs.ratingdiv ratingdiv,
		c.dispsequence dispsequence,
		c.univdiv univdiv,
		u.facultycd facultycd1,
		u.deptcd deptcd1,
		FIRST_VALUE(u.facultycd) OVER (PARTITION BY rs.univcd) facultycd2,
		FIRST_VALUE(u.deptcd) OVER (PARTITION BY rs.univcd) deptcd2
		/* 2016/01/20 QQ)Nishiyama ��K�͉��C ADD START */
		 , u.univname_kana
		/* 2016/01/20 QQ)Nishiyama ��K�͉��C ADD END */
	FROM
		ratingnumber_s rs,
		countuniv c,
		univmaster_basic u
	WHERE
		rs.examyear = ? /* 1 */
	AND
		rs.examcd = ? /* 2 */
	AND
		rs.bundlecd = ? /* 3 */
	AND
		rs.univcountingdiv = ? /* 4 */
	AND
		c.univcd = rs.univcd
	AND
		u.univcd = c.univcd
	AND
		u.eventyear = rs.examyear
	AND
		u.examdiv = ? /* 5 */
	AND
		rs.studentdiv IN (#)
	AND
		rs.ratingdiv IN (#)
)
WHERE
	facultycd1 = facultycd2
AND
	deptcd1 = deptcd2
/* 2016/01/20 QQ)Nishiyama ��K�͉��C DEL START */
/*
ORDER BY
	studentdiv,
	univdiv,
	dispsequence,
	ratingdiv
 */
/* 2016/01/20 QQ)Nishiyama ��K�͉��C DEL END */

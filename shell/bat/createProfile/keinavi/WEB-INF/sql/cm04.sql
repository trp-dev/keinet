SELECT
	numbers
FROM
	examtakenum_c
WHERE
	examyear = ?
AND
	examcd = ?
AND
	bundlecd = ?
AND
	grade = ?
AND
	class = ?

UNION ALL

SELECT
	NVL(SUM(rc.numbers), 0)
FROM
	examtakenum_c rc,
	class_group cg
WHERE
	rc.examyear = ?
AND
	rc.examcd = ?
AND
	rc.bundlecd = ?
AND
	rc.grade = cg.grade
AND
	rc.class = cg.class
AND
	cg.classgcd = ?

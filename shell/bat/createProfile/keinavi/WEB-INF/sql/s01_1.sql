SELECT
	a.numbers,
	b.numbers,
	c.numbers
FROM
	(SELECT
		numbers
	FROM
		examtakenum_a
	WHERE
		examyear = ? AND
		examcd = ?  AND
		studentdiv = '0'
	) a,
	(SELECT
		numbers
	FROM
		examtakenum_a
	WHERE
		examyear = ? AND
		examcd = ?  AND
		studentdiv = '1'
	) b,
	(SELECT
		numbers
	FROM
		examtakenum_a
	WHERE
		examyear = ? AND
		examcd = ?  AND
		studentdiv = '2'
	) c

UPDATE
	loginid_manage 
SET
	loginname = ?,
	mainte_flg = ?,
	func1_flg = ?,
	func2_flg = ?,
	func3_flg = ?,
	func4_flg = ?
WHERE
	schoolcd = ?
AND
	loginid = ?
AND
	manage_flg <> '1'

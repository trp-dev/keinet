SELECT
	NVL(d.highlimit, -999),
	NVL(d.lowlimit, -999) lowlimit,
	NVL(rp.numbers, -999),
	NVL(rp.compratio, -999)
FROM
	deviationzone d,
	subdistrecord_p rp
WHERE
	rp.examyear(+) = ?
AND
	rp.examcd(+) = ?
AND
	rp.prefcd(+) = ?
AND
	rp.subcd(+) = ?
AND
	rp.devzonecd(+) = d.devzonecd
ORDER BY
	lowlimit DESC

SELECT
	e.examyear,
	e.examcd,
	e.examname_abbr,
	e.inpledate,
	DECODE(?, 1, in_dataopendate, out_dataopendate) /* 1 */
FROM
	examination e,
	existsexam ee
WHERE
	e.examtypecd = ?
AND
	e.examyear = ?
AND
	e.tergetgrade = ?
AND
	e.out_dataopendate <= ?
AND
	ee.examyear = e.examyear
AND
	ee.examcd = e.examcd
ORDER BY
	e.out_dataopendate DESC

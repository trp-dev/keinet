SELECT
	cc.grade,
	SUBSTR(cc.class, 1, 2),
	SUBSTR(cc.class, 1, 2),
	NVL(rc.numbers, -999),
	NVL(rc.avgpnt, -999),
	NVL(rc.avgdeviation, -999),
	cc.dispsequence dispsequence,
	cc.dispgraph
FROM
	countcompclass cc

LEFT OUTER JOIN
	subrecord_c rc
ON
	rc.examyear = ? /* 1 */
AND
	rc.examcd = ? /* 2 */
AND
	rc.bundlecd = ? /* 3 */
AND
	rc.subcd = ? /* 4 */
AND
	rc.grade = cc.grade
AND
	rc.class = cc.class

WHERE
	LENGTH(NVL(TRIM(cc.class), '  ')) <= 2

UNION ALL

SELECT
	cc.grade,
	cn.classgcd,
	cn.classgname,
	NVL(SUM(rc.numbers), -999),
	NVL(ROUND(SUM(rc.avgpnt * rc.numbers) / SUM(rc.numbers), 1), -999),
	NVL(ROUND((ROUND(SUM(rc.avgpnt * rc.numbers) / SUM(rc.numbers), 1) - ra.avgpnt) / ra.stddeviation * 10 + 50, 1), -999),
	cc.dispsequence,
	cc.dispgraph
FROM
	countcompclass cc

INNER JOIN
	class_group cg
ON
	cg.classgcd = cc.class

INNER JOIN
	classgroup cn
ON
	cn.classgcd = cg.classgcd

LEFT OUTER JOIN
	(
		SELECT
			ra.examyear examyear,
			ra.examcd examcd,
			MIN(ra.studentdiv) studentdiv
		FROM
			subrecord_a ra
		GROUP BY
			ra.examyear,
			ra.examcd
	) a
ON
	a.examyear = ? /* 5 */
AND
	a.examcd = ? /* 6 */

LEFT OUTER JOIN
	subrecord_a ra
ON
	ra.examyear = a.examyear
AND
	ra.examcd = a.examcd
AND
	ra.subcd = ? /* 7 */
AND
	ra.studentdiv = a.studentdiv

LEFT OUTER JOIN
	subrecord_c rc
ON
	rc.examyear = ? /* 8 */
AND
	rc.examcd = ? /* 9 */
AND
	rc.bundlecd = ? /* 10 */
AND
	rc.subcd = ? /* 11 */
AND
	rc.grade = cg.grade
AND
	rc.class = cg.class

GROUP BY
	cc.grade,
	cn.classgcd,
	cn.classgname,
	ra.avgpnt,
	ra.stddeviation,
	cc.dispsequence,
	cc.dispgraph

ORDER BY
	dispsequence

SELECT /*+ ORDERED INDEX(ri PK_SUBRECORD_I) */
	d.grade grade,
	d.class class,
	d.class_no class_no,
	d.name_kana,
	d.sex,
	es.suballotpnt,
	NVL(ri.score, -999),
	NVL(ri.a_deviation, -999) a_deviation1,
	a.suballotpnt,
	NVL(a.score, -999),
	NVL(a.a_deviation, -999) a_deviation2,
	c.suballotpnt,
	NVL(c.score, -999),
	NVL(c.a_deviation, -999) a_deviation3,
	NVL(ri.a_deviation - a.a_deviation, 0) diff,
	ri.academiclevel academiclevel1,
	a.academiclevel academiclevel2,
	c.academiclevel academiclevel3
FROM
	(
		SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) */
			b.individualid individualid,
			h.grade grade,
			h.class class,
			h.class_no class_no,
			b.name_kana name_kana,
			b.sex sex
		FROM
			basicinfo b,
			historyinfo h
		WHERE
			b.schoolcd = ? /* 1 */
		AND
			h.individualid = b.individualid
		AND
			h.year = ? /* 2 */
		AND
			h.grade = ? /* 3 */
		AND
			h.class = ? /* 4 */
		
		UNION

		SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) */
			b.individualid individualid,
			h.grade grade,
			h.class class,
			h.class_no class_no,
			b.name_kana name_kana,
			b.sex sex
		FROM
			basicinfo b,
			historyinfo h,
			class_group cg
		WHERE
			b.schoolcd = ? /* 5 */
		AND
			h.individualid = b.individualid
		AND
			h.year = ? /* 6 */
		AND
			cg.grade = h.grade
		AND
			cg.class = h.class
		AND
			cg.classgcd = ? /* 7 */
		AND
			cg.year = h.year
		AND
			cg.bundlecd = b.schoolcd
	) d,
	examsubject es,
	subrecord_i ri,
	(
		SELECT /*+ ORDERED INDEX(ri SUBRECORD_I_2010_IDX) */
			ri.individualid individualid,
			es.suballotpnt suballotpnt,
			ri.score score,
			ri.a_deviation a_deviation,
			ri.academiclevel academiclevel
		FROM
			examsubject es,
			existsexam ee,
			subrecord_i ri
		WHERE
			es.examyear = ? /* 8 */
		AND
			es.examcd = ? /* 9 */
		AND
			es.subcd = ? /* 10 */
		AND
			ee.examyear = es.examyear
		AND
			ee.examcd = es.examcd
		AND
			ri.examyear = es.examyear
		AND
			ri.examcd = es.examcd
		AND
			ri.subcd = es.subcd
	) a,
	(
		SELECT /*+ ORDERED INDEX(ri SUBRECORD_I_2010_IDX) */
			ri.individualid individualid,
			es.suballotpnt suballotpnt,
			ri.score score,
			ri.a_deviation a_deviation,
			ri.academiclevel academiclevel
		FROM
			examsubject es,
			existsexam ee,
			subrecord_i ri
		WHERE
			es.examyear = ? /* 11 */
		AND
			es.examcd = ? /* 12 */
		AND
			es.subcd = ? /* 13 */
		AND
			ee.examyear = es.examyear
		AND
			ee.examcd = es.examcd
		AND
			ri.examyear = es.examyear
		AND
			ri.examcd = es.examcd
		AND
			ri.subcd = es.subcd
	) c
WHERE
	es.examyear = ? /* 14 */
AND
	es.examcd = ? /* 15 */
AND
	es.subcd = ? /* 16 */
AND
	ri.examyear = es.examyear
AND
	ri.examcd = es.examcd
AND
	ri.subcd = es.subcd
AND
	ri.individualid = d.individualid
AND
	a.individualid(+) = ri.individualid
AND
	c.individualid(+) = ri.individualid

SELECT
	es.subcd,
	es.subname,
	es.suballotpnt,
	eq.display_no,
	eq.questionname1,
	eq.qallotpnt,
	eq.question_no
FROM
	v_sm_qexamsubject es,
	examquestion eq
WHERE
	es.examyear = ? /* 1 */
AND
	es.examcd = ? /* 2 */
AND
	es.subcd < '7000'
AND
	es.subcd IN (#)
AND
	eq.examyear(+) = es.examyear
AND
	eq.examcd(+) = es.examcd
AND
	eq.subcd(+) = es.subcd
ORDER BY
	es.dispsequence,
	eq.display_no,
	eq.question_no

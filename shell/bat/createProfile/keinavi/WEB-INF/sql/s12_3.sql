/*
 * S12_11_共通テスト英語認定試験CEFR取得状況 各試験
 * 
 *  SQL結果
 *    横方向:高校、県、全国の人数・構成比、受験有無フラグ
 *    縦方向:試験コード、試験レベルコード、CEFRレベルコード
 *    改表:試験コード、試験レベルコードの切り替わり
 *    改行:試験コード、試験レベルコード、CEFRレベルコードの切り替わり
 */
SELECT
	/* 認定試験情報マスタ.参加試験コード */
	ei.engptcd,
	/* 認定試験情報マスタ.参加試験短縮名 */
	ei.engptname_abbr,
	/* 認定試験情報マスタ.レベルありフラグ */
	ei.levelflg,
	/* 認定試験詳細情報マスタ.参加試験レベルコード */
	edi.engptlevelcd,
	/* 認定試験詳細情報マスタ.参加試験レベル短縮名 */
	edi.engptlevelname_abbr,
	/* CEFR情報.ＣＥＦＲレベルコード */
	ci.cefrlevelcd,
	/* CEFR情報.ＣＥＦＲレベル短縮名 */
	CASE WHEN ci.cefrlevelcd = '99' THEN 'なし' ELSE ci.cerflevelname_abbr END AS cerflevelname_abbr,
	/* 学校マスタ.一括名 */
	sc.bundlename,
	/* 県マスタ.県名 */
	pre.prefname,
	/* 英語認定試験別・合計CEFR取得状況（高校）.人数 */
	ecass.numbers AS s_numbers,
	/* 英語認定試験別・合計CEFR取得状況（高校）.構成比 */
	ecass.compratio AS s_compratio,
	/* 英語認定試験別・合計CEFR取得状況（県）.人数 */
	ecasp.numbers AS p_numbers,
	/* 英語認定試験別・合計CEFR取得状況（県）.構成比 */
	ecasp.compratio AS p_compratio,
	/* 英語認定試験別・合計CEFR取得状況（全国）.人数 */
	ecasa.numbers AS a_numbers,
	/* 英語認定試験別・合計CEFR取得状況（全国）.構成比 */
	ecasa.compratio AS a_compratio,
	/* 受験有無フラグ */
	CASE WHEN ecass.bundlecd IS NULL THEN '0' ELSE '1' END AS flg
FROM
	/* 認定試験情報マスタTBL */
	engptinfo ei
	/* 認定試験詳細情報マスタTBL */
	INNER JOIN engptdetailinfo edi
	ON
		edi.eventyear = ei.eventyear
	AND
		edi.examdiv = ei.examdiv
	AND
		edi.engptcd = ei.engptcd
	/* CEFR情報TBL */
	INNER JOIN (
		/* 各CEFRレベル毎の情報 */
		SELECT
			ci.eventyear,
			ci.examdiv,
			ci.cefrlevelcd,
			ci.cerflevelname_abbr,
			ci.sort
		FROM
			cefr_info ci
		UNION ALL
		/* 合計行 */
		SELECT
			ci.eventyear,
			ci.examdiv,
			'ZZ' AS cefrlevelcd,
			'合計' AS cerflevelname_abbr,
			999 AS sort
		FROM
			cefr_info ci
		GROUP BY
			ci.eventyear,
			ci.examdiv) ci
	ON
		ci.eventyear = ei.eventyear
	AND
		ci.examdiv = ei.examdiv
	/* 模試マスタTBL */
	INNER JOIN examination en
	ON
		en.examyear = ei.eventyear
	AND
		en.examdiv = ei.examdiv
	AND
		en.examcd = ? /* '1' */
	/* 学校マスタ */
	INNER JOIN school sc
	ON
		sc.bundlecd = ? /* '2' */
	/* 県マスタ */
	INNER JOIN prefecture pre
	ON
		pre.prefcd = sc.facultycd
	/* 英語認定試験別・合計CEFR取得状況（全国）TBL */
	LEFT JOIN engpt_cefracqstatus_a ecasa
	ON
		ecasa.examyear = ei.eventyear
	AND
		ecasa.examcd = en.examcd
	AND
		ecasa.engptcd = ei.engptcd
	AND
		ecasa.engptlevelcd = edi.engptlevelcd
	AND
		ecasa.cefrlevelcd = ci.cefrlevelcd
	/* 英語認定試験別・合計CEFR取得状況（県）TBL */
	LEFT JOIN engpt_cefracqstatus_p ecasp
	ON
		ecasp.examyear = ei.eventyear
	AND
		ecasp.examcd = en.examcd
	AND
		ecasp.engptcd = ei.engptcd
	AND
		ecasp.engptlevelcd = edi.engptlevelcd
	AND
		ecasp.cefrlevelcd = ci.cefrlevelcd
	AND
		ecasp.prefcd = sc.facultycd
	/* 英語認定試験別・合計CEFR取得状況（学校）TBL */
	LEFT JOIN engpt_cefracqstatus_s ecass
	ON
		ecass.examyear = ei.eventyear
	AND
		ecass.examcd = en.examcd
	AND
		ecass.engptcd = ei.engptcd
	AND
		ecass.engptlevelcd = edi.engptlevelcd
	AND
		ecass.cefrlevelcd = ci.cefrlevelcd
	AND
		ecass.bundlecd = ? /* '3' */
WHERE
	ei.EVENTYEAR = ? /* '4' */
ORDER BY
	ei.SORT,
	edi.SORT,
	ci.SORT

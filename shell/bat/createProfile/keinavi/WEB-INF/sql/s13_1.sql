SELECT /*+ ORDERED */
	es.subcd,
	es.subname,
	es.suballotpnt,
	eq.display_no,
	eq.questionname1,
	eq.qallotpnt,
	NVL(rs.numbers, -999),
	NVL(rs.avgscorerate, -999),
	NVL(rp.numbers, -999),
	NVL(rp.avgscorerate, -999),
	NVL(ra.numbers, -999),
	NVL(ra.avgscorerate, -999),
	NVL(zs.numbers, -999),
	NVL(zs.avgscorerate, -999),
	NVL(za.numbers, -999),
	NVL(za.avgscorerate, -999),
	NVL(zb.numbers, -999),
	NVL(zb.avgscorerate, -999),
	NVL(zc.numbers, -999),
	NVL(zc.avgscorerate, -999),
	NVL(zd.numbers, -999),
	NVL(zd.avgscorerate, -999),
	NVL(ze.numbers, -999),
	NVL(ze.avgscorerate, -999),
	NVL(zf.numbers, -999),
	NVL(zf.avgscorerate, -999),
	NVL(ys.numbers, -999),
	NVL(ys.avgscorerate, -999),
	NVL(ya.numbers, -999),
	NVL(ya.avgscorerate, -999),
	NVL(yb.numbers, -999),
	NVL(yb.avgscorerate, -999),
	NVL(yc.numbers, -999),
	NVL(yc.avgscorerate, -999),
	NVL(yd.numbers, -999),
	NVL(yd.avgscorerate, -999),
	NVL(ye.numbers, -999),
	NVL(ye.avgscorerate, -999),
	NVL(yf.numbers, -999),
	NVL(yf.avgscorerate, -999)
FROM
	v_sm_qexamsubject es

LEFT OUTER JOIN
	examquestion eq
ON
	eq.examyear = es.examyear
AND
	eq.examcd = es.examcd
AND
	eq.subcd = es.subcd

LEFT OUTER JOIN
	qrecord_a ra
ON
	ra.examyear = eq.examyear
AND
	ra.examcd = eq.examcd
AND
	ra.subcd = eq.subcd
AND
	ra.question_no = eq.question_no

LEFT OUTER JOIN
	qrecord_p rp
ON
	rp.examyear = eq.examyear
AND
	rp.examcd = eq.examcd
AND
	rp.subcd = eq.subcd
AND
	rp.question_no = eq.question_no
AND
	rp.prefcd = ? /* 1 */

LEFT OUTER JOIN
	qrecord_s rs
ON
	rs.examyear = eq.examyear
AND
	rs.examcd = eq.examcd
AND
	rs.subcd = eq.subcd
AND
	rs.question_no = eq.question_no
AND
	rs.bundlecd = ? /* 2 */

LEFT OUTER JOIN
	qrecordzone zs
ON
	zs.examyear = rs.examyear
AND
	zs.examcd = rs.examcd
AND
	zs.subcd = rs.subcd
AND
	zs.question_no = rs.question_no
AND
	zs.bundlecd = rs.bundlecd
AND
	zs.devzonecd = 'S'

LEFT OUTER JOIN
	qrecordzone za
ON
	za.examyear = rs.examyear
AND
	za.examcd = rs.examcd
AND
	za.subcd = rs.subcd
AND
	za.question_no = rs.question_no
AND
	za.bundlecd = rs.bundlecd
AND
	za.devzonecd = 'A'

LEFT OUTER JOIN
	qrecordzone zb
ON
	zb.examyear = rs.examyear
AND
	zb.examcd = rs.examcd
AND
	zb.subcd = rs.subcd
AND
	zb.question_no = rs.question_no
AND
	zb.bundlecd = rs.bundlecd
AND
	zb.devzonecd = 'B'

LEFT OUTER JOIN
	qrecordzone zc
ON
	zc.examyear = rs.examyear
AND
	zc.examcd = rs.examcd
AND
	zc.subcd = rs.subcd
AND
	zc.question_no = rs.question_no
AND
	zc.bundlecd = rs.bundlecd
AND
	zc.devzonecd = 'C'

LEFT OUTER JOIN
	qrecordzone zd
ON
	zd.examyear = rs.examyear
AND
	zd.examcd = rs.examcd
AND
	zd.subcd = rs.subcd
AND
	zd.question_no = rs.question_no
AND
	zd.bundlecd = rs.bundlecd
AND
	zd.devzonecd = 'D'

LEFT OUTER JOIN
	qrecordzone ze
ON
	ze.examyear = rs.examyear
AND
	ze.examcd = rs.examcd
AND
	ze.subcd = rs.subcd
AND
	ze.question_no = rs.question_no
AND
	ze.bundlecd = rs.bundlecd
AND
	ze.devzonecd = 'E'

LEFT OUTER JOIN
	qrecordzone zf
ON
	zf.examyear = rs.examyear
AND
	zf.examcd = rs.examcd
AND
	zf.subcd = rs.subcd
AND
	zf.question_no = rs.question_no
AND
	zf.bundlecd = rs.bundlecd
AND
	zf.devzonecd = 'F'

LEFT OUTER JOIN
	qrecordzone_a ys
ON
	ys.examyear = eq.examyear
AND
	ys.examcd = eq.examcd
AND
	ys.subcd = eq.subcd
AND
	ys.question_no = eq.question_no
AND
	ys.devzonecd = 'S'

LEFT OUTER JOIN
	qrecordzone_a ya
ON
	ya.examyear = eq.examyear
AND
	ya.examcd = eq.examcd
AND
	ya.subcd = eq.subcd
AND
	ya.question_no = eq.question_no
AND
	ya.devzonecd = 'A'

LEFT OUTER JOIN
	qrecordzone_a yb
ON
	yb.examyear = eq.examyear
AND
	yb.examcd = eq.examcd
AND
	yb.subcd = eq.subcd
AND
	yb.question_no = eq.question_no
AND
	yb.devzonecd = 'B'

LEFT OUTER JOIN
	qrecordzone_a yc
ON
	yc.examyear = eq.examyear
AND
	yc.examcd = eq.examcd
AND
	yc.subcd = eq.subcd
AND
	yc.question_no = eq.question_no
AND
	yc.devzonecd = 'C'

LEFT OUTER JOIN
	qrecordzone_a yd
ON
	yd.examyear = eq.examyear
AND
	yd.examcd = eq.examcd
AND
	yd.subcd = eq.subcd
AND
	yd.question_no = eq.question_no
AND
	yd.devzonecd = 'D'

LEFT OUTER JOIN
	qrecordzone_a ye
ON
	ye.examyear = eq.examyear
AND
	ye.examcd = eq.examcd
AND
	ye.subcd = eq.subcd
AND
	ye.question_no = eq.question_no
AND
	ye.devzonecd = 'E'

LEFT OUTER JOIN
	qrecordzone_a yf
ON
	yf.examyear = eq.examyear
AND
	yf.examcd = eq.examcd
AND
	yf.subcd = eq.subcd
AND
	yf.question_no = eq.question_no
AND
	yf.devzonecd = 'F'

WHERE
	es.examyear = ? /* 3 */
AND
	es.examcd = ? /* 4 */
AND
	es.subcd < '7000'
AND
	es.subcd IN (#)

ORDER BY
	es.dispsequence,
	eq.display_no,
	eq.question_no

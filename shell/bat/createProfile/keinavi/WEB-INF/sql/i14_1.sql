SELECT 
	NVL(HP.DISTRICTPROVISO,NULL) DISTRICTPROVISO,
	NVL(HP.SEARCHSTEMMADIV,NULL) SEARCHSTEMMADIV,    
	NVL(HP.STEMMAPROVISO,NULL) STEMMAPROVISO,  
	NVL(HP.DESCIMPOSEDIV,NULL) DESCIMPOSEDIV,      
	NVL(HP.SUBIMPOSEDIV,NULL) SUBIMPOSEDIV,     
	NVL(HP.IMPOSESUB,NULL) IMPOSESUB,    
	NVL(HP.SCHOOLDIVPROVISO,NULL) SCHOOLDIVPROVISO,    
	NVL(HP.STARTDATEPROVISO,NULL) STARTDATEPROVISO,   
	NVL(HP.ENDDATEPROVISO,NULL) ENDDATEPROVISO, 
	NVL(HP.RAINGDIV,NULL) RAINGDIV, 
	NVL(HP.RAINGPROVISO,NULL) RAINGPROVISO,        
	NVL(HP.OUTOFTERGETPROVISO,NULL) OUTOFTERGETPROVISO
FROM HANGUPPROVISO HP 
WHERE 
	HP.INDIVIDUALID = ?
SELECT
	/*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(ri PK_SUBRECORD_I)*/
	h.grade, 
	h.CLASS, 
	h.class_no, 
	b.name_kana, 
	b.sex, 
	NVL(ri.score, - 999), 
	NVL(ROUND((ri.score / es.suballotpnt) * 100, 2), -999),
	ri.s_rank 
FROM
	basicinfo b, 
	historyinfo h, 
	subrecord_i ri,
	examsubject es
WHERE
	b.schoolcd = ? AND /* 1 */
	h.individualid = b.individualid AND
	h.YEAR = ? AND /* 2 */
	h.grade = ? AND /* 3 */
	h.CLASS = ? AND /* 4 */
	ri.individualid = h.individualid AND
	ri.examyear = ? AND /* 5 */
	ri.examcd = ? AND /* 6 */
	ri.subcd = ? AND /* 7 */
	ri.examyear = es.examyear AND
	ri.examcd = es.examcd AND
	ri.subcd = es.subcd

UNION ALL 

SELECT
	/*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(ri PK_SUBRECORD_I)*/
	h.grade, 
	h.CLASS, 
	h.class_no, 
	b.name_kana, 
	b.sex, 
	NVL(ri.score, - 999), 
	NVL(ROUND((ri.score / es.suballotpnt) * 100, 2), -999),
	ri.s_rank 
FROM
	basicinfo b, 
	historyinfo h, 
	class_group cg, 
	subrecord_i ri,
	examsubject es
WHERE
	b.schoolcd = ? AND /* 8 */
	h.individualid = b.individualid AND
	h.YEAR = ? AND /* 9 */
	cg.YEAR = h.YEAR AND
	cg.bundlecd = b.schoolcd AND
	cg.grade = h.grade AND
	cg.CLASS = h.CLASS AND
	cg.classgcd = ? AND /* 10 */
	ri.individualid = h.individualid AND
	ri.examyear = ? AND /* 11 */
	ri.examcd = ? AND /* 12 */
	ri.subcd = ? AND /* 13 */
	ri.examyear = es.examyear AND
	ri.examcd = es.examcd AND
	ri.subcd = es.subcd

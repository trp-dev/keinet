SELECT
	univcd,
	univname_abbr,
	univdivcd,
	prefcd_hq,
	totalcandidatenum
	/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD START */
	 , univname_kana
	/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD END */
FROM (
	SELECT
		u.univcd univcd,
		u.uniname_abbr univname_abbr,
		u.unidiv univdivcd,
		u.prefcd_examho prefcd_hq,
		NVL(ra.totalcandidatenum, 0) totalcandidatenum,
		u.facultycd facultycd1,
		FIRST_VALUE (u.facultycd) OVER (PARTITION BY u.univcd) facultycd2,
		u.deptcd deptcd1,
		FIRST_VALUE (u.deptcd) OVER (PARTITION BY u.univcd) deptcd2
		/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD START */
		 , u.univname_kana
		/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD END */
	FROM
		univmaster_basic u,
		ratingnumber_a ra
	WHERE
		u.eventyear = ? /* 1 */
	AND
		u.examdiv = ? /* 2 */
	AND
		u.univcd IN (#)
	AND
		ra.examyear(+) = u.eventyear
	AND
		ra.examcd(+) = ? /* 3 */
	AND
		ra.ratingdiv(+) = ? /* 4 */
	AND
		ra.univcountingdiv(+) = '0'
	AND
		ra.studentdiv(+) = '0'
	AND
		ra.univcd(+) = u.univcd
)
WHERE
	facultycd1 = facultycd2
AND
	deptcd1 = deptcd2
	
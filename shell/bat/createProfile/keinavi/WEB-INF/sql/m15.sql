INSERT INTO
	ratingnumberrecount
SELECT DISTINCT
	ca.examyear,
	ca.examcd,
	ca.bundlecd,
	?,
	?,
	ca.univcd,
	ca.facultycd,
	ca.deptcd
FROM
	candidaterating ca
WHERE
	ca.individualid = ?
AND
	ca.examyear = ?
AND	
	NOT EXISTS (
		SELECT
			'1'
		FROM
			examination e
		WHERE
			e.examyear = ca.examyear
		AND
			e.examcd = ca.examcd
		AND
			e.examtypecd = '32'
		)
AND
	NOT EXISTS (
		SELECT
			1
		FROM
			ratingnumberrecount re
		WHERE
			re.year = ca.examyear
		AND
			re.examcd = ca.examcd
		AND
			re.bundlecd = ca.bundlecd
		AND
			re.grade = ?
		AND
			re.class = ?
		AND
			re.univcd = ca.univcd
		AND
			re.facultycd = ca.facultycd
		AND
			re.deptcd = ca.deptcd
	)
GROUP BY
	ca.examyear,
	ca.examcd,
	ca.bundlecd,
	ca.univcd,
	ca.facultycd,
	ca.deptcd

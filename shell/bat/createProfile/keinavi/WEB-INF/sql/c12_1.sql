SELECT /*+ INDEX(b BASICINFO_IDX) */
	b.individualid individualid,
	h.grade grade,
	h.class class,
	h.class_no class_no,
	b.name_kana name_kana,
	b.sex sex
FROM
	basicinfo b,
	historyinfo h
WHERE
	b.schoolcd = ? /* 1 */
AND
	h.individualid = b.individualid
AND
	h.year = ? /* 2 */
AND
	h.grade = ? /* 3 */
AND
	h.class = ? /* 4 */
AND
	EXISTS (
		SELECT
			ri.rowid
		FROM
			subrecord_i ri
		WHERE
			ri.individualid = b.individualid
		AND
			ri.examyear = ? /* 5 */
		AND
			ri.examcd = ? /* 6 */
	)

UNION ALL

SELECT /*+ INDEX(b BASICINFO_IDX) */
	b.individualid individualid,
	h.grade grade,
	h.class class,
	h.class_no class_no,
	b.name_kana name_kana,
	b.sex sex
FROM
	basicinfo b,
	historyinfo h,
	class_group cg
WHERE
	b.schoolcd = ? /* 7 */
AND
	h.individualid = b.individualid
AND
	h.year = ? /* 8 */
AND
	h.grade = cg.grade
AND
	h.class = cg.class
AND
	cg.year = h.year
AND
	cg.classgcd = ? /* 9 */
AND
	cg.bundlecd = b.schoolcd
AND
	EXISTS (
		SELECT
			ri.rowid
		FROM
			subrecord_i ri
		WHERE
			ri.individualid = b.individualid
		AND
			ri.examyear = ? /* 10 */
		AND
			ri.examcd = ? /* 11 */
	)

ORDER BY
	class,
	class_no

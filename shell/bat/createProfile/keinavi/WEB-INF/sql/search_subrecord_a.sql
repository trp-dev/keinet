SELECT
	a.examyear,
	a.examcd,
	a.subcd,
	a.studentdiv,
	a.avgpnt,
	a.avgdeviation,
	a.avgscorerate,
	a.stddeviation,
	a.highestpnt,
	a.lowestpnt,
	a.numbers
FROM
	subrecord_a a
WHERE
	a.examyear = ?
AND
	a.examcd = ?


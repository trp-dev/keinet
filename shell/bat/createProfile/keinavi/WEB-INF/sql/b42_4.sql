SELECT
	NVL(d.highlimit, -999),
	NVL(d.lowlimit, -999) lowlimit,
	NVL(ra.numbers, -999),
	NVL(ra.compratio, -999)
FROM
	deviationzone d,
	subdistrecord_a ra
WHERE
	ra.examyear(+) = ? /* 1 */
AND
	ra.examcd(+) = ? /* 2 */
AND
	ra.subcd(+) = ? /* 3 */
AND
	ra.devzonecd(+) = d.devzonecd
ORDER BY
	lowlimit DESC

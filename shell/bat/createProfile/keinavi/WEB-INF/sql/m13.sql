SELECT
	1
FROM
	DUAL
WHERE
	EXISTS (
		SELECT
			si.individualid
		FROM
			subrecord_i si
		WHERE
			si.individualid = ?
		AND
			ROWNUM <= 1
	)

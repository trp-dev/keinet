SELECT
	e.examname examname,
	e.inpledate inpledate,
	e.dispsequence dispsequence
FROM
	examination e
WHERE
	(e.examyear, e.examcd) IN (
		SELECT
			ri.examyear,
			ri.examcd
		FROM
			individualrecord ri
		WHERE
			ri.s_individualid = ?
		INTERSECT
		SELECT
			ri.examyear,
			ri.examcd
		FROM
			individualrecord ri
		WHERE
			ri.s_individualid = ?)
ORDER BY
	inpledate DESC,
	dispsequence

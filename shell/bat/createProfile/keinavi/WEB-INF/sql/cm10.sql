SELECT
	ns.numbers
FROM
	examcdtrans et
INNER JOIN
	examtakenum_s ns
ON
	ns.examyear = et.examyear
AND
	ns.examcd = et.examcd
AND
	ns.bundlecd = ?
WHERE
	et.examyear = ?
AND
	et.curexamcd = ?

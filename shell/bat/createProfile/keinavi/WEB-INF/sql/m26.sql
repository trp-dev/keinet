SELECT
	NVL(ri.cntgrade, ri.grade),
	NVL2(ri.cntgrade, ri.cntclass, ri.class)
FROM
	individualrecord ri
WHERE
	ri.examyear = ?
AND
	ri.examcd = ?
AND
	ri.answersheet_no = ?
AND	
	NOT EXISTS (
		SELECT
			'1'
		FROM
			examination e
		WHERE
			e.examyear = ri.examyear
		AND
			e.examcd = ri.examcd
		AND
			e.examtypecd = '32'
		)
AND
	EXISTS (
		SELECT
			1
		FROM
			historyinfo h
		WHERE
			h.individualid = ?
		AND
			h.year = ri.examyear
		AND
			(h.grade <> NVL(ri.cntgrade, ri.grade) OR h.class <> NVL2(ri.cntgrade, ri.cntclass, ri.class)))
UNION ALL
SELECT
	h.grade,
	h.class
FROM
	historyinfo h
WHERE
	h.individualid = ?
AND
	h.year = ?
AND
	EXISTS (
		SELECT
			1
		FROM
			individualrecord ri
		WHERE
			ri.examyear = h.year
		AND
			ri.examcd = ?
		AND
			ri.answersheet_no = ?
		AND	
			NOT EXISTS (
				SELECT
					'1'
				FROM
					examination e
				WHERE
					e.examyear = ri.examyear
				AND
					e.examcd = ri.examcd
				AND
					e.examtypecd = '32'
				)
		AND
			(NVL(ri.cntgrade, ri.grade) <> h.grade OR NVL2(ri.cntgrade, ri.cntclass, ri.class) <> h.class))

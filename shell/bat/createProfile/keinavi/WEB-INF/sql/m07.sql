SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) */
	COUNT(h.individualid)
FROM
	basicinfo b,
	historyinfo h
WHERE
	b.schoolcd = ? /* 1 */
AND
	h.individualid = b.individualid
AND
	h.year = ? /* 2 */
AND
	h.grade = ? /* 3 */
AND
	EXISTS(
		SELECT
			rowid
		FROM
			class_group cg
		WHERE
			cg.year = h.year
		AND
			cg.bundlecd = b.schoolcd
		AND
			cg.grade = h.grade
		AND
			cg.class = h.class
		AND 
			cg.classgcd = ? /* 4 */
	)

SELECT
	TRIM('�i�S���j'),
	NVL(ra.numbers, -999),
	NVL(ra.avgscorerate, -999)
FROM
	examination e

LEFT OUTER JOIN
	qrecord_a ra
ON
	ra.examyear = e.examyear
AND
	ra.examcd = e.examcd
AND
	ra.subcd = ? /* 1 */
AND
	ra.question_no = ? /* 2 */

WHERE
	e.examyear = ? /* 3 */
AND
	e.examcd = ? /* 4 */

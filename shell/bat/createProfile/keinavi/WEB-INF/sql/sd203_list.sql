SELECT
  APPLICATION_ID
  , EXAMYEAR
  , EXAMNAME
  , BUNDLECD
  , BUNDLENAME
  , DECODE(PACTDIV, 1, '�_��Z', '��_��Z')
  , TEACHER_NAME
  , APP_COMMENT
  , APP_DATE
  , SECTORNAME
  , EXAMCD
  , DISPSEQUENCE
FROM
  (
    SELECT
      A.APPLICATION_ID
      , E.EXAMYEAR
      , CASE
        WHEN E.EXAMTYPECD = '32'
        THEN CASE
          WHEN SUBSTR(A.EXAMCD, 3, 2) = '_1'
          THEN E.EXAMNAME || '�i�����񓚂P�j'
          WHEN SUBSTR(A.EXAMCD, 3, 2) = '_2'
          THEN E.EXAMNAME || '�i�����񓚂Q�j'
          ELSE E.EXAMNAME || '�i�l���сj'
          END
        ELSE E.EXAMNAME
        END AS EXAMNAME
      , A.EXAMCD
      , E.DISPSEQUENCE
      , S.BUNDLECD
      , S.BUNDLENAME
      , (
        SELECT
          1
        FROM
          DUAL
        WHERE
          EXISTS(
            SELECT
              1
            FROM
              PACTSCHOOL P
            WHERE
              (
                P.SCHOOLCD = S.BUNDLECD
                OR P.SCHOOLCD = S.PARENTSCHOOLCD
              )
              AND P.PACTDIV = '1'
              AND (
                P.PACTTERM IS NULL
                OR TO_DATE(P.PACTTERM, 'YYYYMMDD') >= SYSDATE
              )
          )
      ) AS PACTDIV
      , A.TEACHER_NAME
      , A.APP_COMMENT
      , A.APP_DATE
      , G.SECTORCD
      , G.SECTORNAME
      , FIRST_VALUE(G.SECTORNAME) OVER(
        PARTITION BY
          A.APPLICATION_ID
        ORDER BY
          G.SECTORNAME
      ) AS SECTORSORTKEY
    FROM
      EXDATA_APP A
      INNER JOIN EXAMINATION E
        ON E.EXAMYEAR = A.EXAMYEAR
        AND E.EXAMCD = SUBSTR(A.EXAMCD, 1, 2)
      INNER JOIN SCHOOL S
        ON S.BUNDLECD = A.BUNDLECD
      INNER JOIN EXDATA_APP_SECT C
        ON C.APPLICATION_ID = A.APPLICATION_ID
      INNER JOIN SECTORSORTING G
        ON G.SECTORCD = C.APP_SECTORCD
    WHERE
      A.APP_STATUS_CODE = ?
      AND A.CANCEL_FLG IS NULL
      AND (
        EXISTS(
          SELECT
            1
          FROM
            EXDATA_APP_SECT SECT
          WHERE
            SECT.APPLICATION_ID = A.APPLICATION_ID
            AND SECT.APP_SECTORCD IN (#)
        )
        OR A.APP_STATUS_CODE != '00'
      )
  )
ORDER BY

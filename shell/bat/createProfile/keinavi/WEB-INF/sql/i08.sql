SELECT
    t1.year,
    t1.examdiv,
    t1.univcd,
    t1.facultycd,
    t1.deptcd,
    nvl(t2.deptsortkey, ' ') deptsortkey,
    t1.entexammodecd,
    t1.remarks,
    t1.entexamdiv1,
    t1.entexamdiv2,
    TRIM(t1.entexamdiv3) entexamdiv3,
    t1.examplandate1,
    t1.examplandate2,
    t1.examplandate3,
    t1.examplandate4,
    t1.examplandate5,
    t1.examplandate6,
    t1.examplandate7,
    t1.examplandate8,
    t1.examplandate9,
    t1.examplandate10,
    t1.proventexamsite1,
    t1.proventexamsite2,
    t1.proventexamsite3,
    t1.proventexamsite4,
    t1.proventexamsite5,
    t1.proventexamsite6,
    t1.proventexamsite7,
    t1.proventexamsite8,
    t1.proventexamsite9,
    t1.proventexamsite10,
    t2.uniname_abbr univname_abbr,
    t2.facultyname_abbr,
    t2.deptname_abbr,
    t4.remarks guideremarks,
    t5.entexammodename,
    t2.unidiv univdivcd,
    t2.prefcd_examho prefcd_hq
/* 2016/01/15 QQ)Nishiyama ��K�͉��C ADD START */
	 , NVL(t6.KANA_NUM, ' ') kana_num
	 , t2.UNINIGHTDIV
	 , t2.FACULTYCONCD
	 , t2.UNIGDIV schedulecd
	 , t2.DEPTSERIAL_NO
	 , t2.SCHEDULESYS
	 , t2.SCHEDULESYSBRANCHCD
	 , t2.DEPTNAME_KANA
/* 2016/01/15 QQ)Nishiyama ��K�͉��C ADD END */

FROM
    examplanuniv t1
INNER JOIN
    univmaster_basic t2
ON
    t1.year = t2.eventyear
AND
    t1.examdiv = t2.examdiv
AND
    t1.univcd = t2.univcd
AND
    t1.facultycd = t2.facultycd
AND
    t1.deptcd = t2.deptcd
LEFT JOIN
    guideremarks t4
ON
    t1.univcd = t4.univcd
AND
    t1.facultycd = t4.facultycd
AND
    t1.deptcd = t4.deptcd
LEFT JOIN
    entexammode t5
ON
    t1.entexammodecd = t5.entexammodecd
/* 2016/01/15 QQ)Nishiyama ��K�͉��C ADD START */
 LEFT JOIN  
 	univmaster_info  t6
 ON 
	t2.eventyear = t6.eventyear  
 AND 
	t2.univcd = t6.univcd  
/* 2016/01/15 QQ)Nishiyama ��K�͉��C ADD END */
WHERE
    t1.individualid = ?

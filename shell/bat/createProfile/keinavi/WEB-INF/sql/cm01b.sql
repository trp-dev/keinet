SELECT /*+ ORDERED INDEX(b BASICINFO_IDX) INDEX(h PK_HISTORYINFO) USE_NL(b h) USE_NL(cg b) */
	h.year year,
	h.grade,
	cg.classgcd,
	cn.classgname || '�N���X',
	COUNT(b.individualid),
	DECODE (h.grade, 3, 1, 1, 3, h.grade) grade_seq
FROM
	basicinfo b
INNER JOIN
	historyinfo h
ON
	h.individualid = b.individualid
AND
	h.year IN (#)
INNER JOIN
	class_group cg
ON
	cg.year = h.year
AND
	cg.bundlecd = b.schoolcd
AND
	cg.grade = h.grade
AND
	cg.class = h.class
INNER JOIN
	classgroup cn
ON
	cn.classgcd = cg.classgcd
WHERE
	b.schoolcd = ? /* 4 */
AND
	(
 		NOT EXISTS (
			SELECT
				rowid
			FROM
				login_charge c
			WHERE
				c.schoolcd = b.schoolcd
			AND
				c.loginid = ?
		) OR (
			SELECT
				count(t.rowid)
			FROM
				class_group t
			WHERE
				t.year = cg.year
			AND
				t.bundlecd = cg.bundlecd
			AND
				t.grade = cg.grade
			AND
				t.classgcd = cg.classgcd
		) = (
			SELECT
				count(t.rowid)
			FROM
				class_group t
			INNER JOIN
				login_charge u
			ON
				u.schoolcd = t.bundlecd
			AND
				u.loginid = ?
			AND
				u.year = t.year
			AND
				u.grade = t.grade
			AND
				u.class = t.class
			WHERE
				t.year = cg.year
			AND
				t.bundlecd = cg.bundlecd
			AND
				t.grade = cg.grade
			AND
				t.classgcd = cg.classgcd
		)
	)
GROUP BY
	h.year,
	h.grade,
	cg.classgcd,
	cn.classgname

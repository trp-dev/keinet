SELECT /*+ ORDERED */
	ri.examyear examyear,
	ri.examcd examcd,
	ri.grade grade,
	UPPER(ri.class) class,
	DECODE(ri.grade, 3, 1, 1, 3, ri.grade) grade_seq,
	e.examname examname,
	e.inpledate inpledate,
	e.dispsequence dispsequence
FROM
	school s
INNER JOIN
	individualrecord ri
ON
	ri.bundlecd = s.bundlecd
INNER JOIN
	examination e
ON
	e.examyear = ri.examyear
AND
	e.examcd = ri.examcd
AND
	e.examyear >= TO_CHAR(ADD_MONTHS(SYSDATE, -27), 'YYYY')
AND
	e.out_dataopendate <= TO_CHAR(SYSDATE, 'YYYYMMDD') || '0000'
AND
	NVL2(s.newgetdate, s.newgetdate || '0000', '000000000000') <= e.out_dataopendate
AND
	NVL2(s.stopdate, s.stopdate || '0000', '999999999999') > e.out_dataopendate
WHERE
	s.bundlecd = ? OR s.parentschoolcd = ?
GROUP BY
	ri.examyear,
	ri.examcd,
	ri.grade,
	ri.class,
	UPPER(ri.class),
	e.examname,
	e.inpledate,
	e.dispsequence
ORDER BY
	examyear DESC,
	grade_seq,
	class,
	inpledate DESC,
	dispsequence

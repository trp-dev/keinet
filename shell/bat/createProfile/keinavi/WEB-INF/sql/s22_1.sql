SELECT
	es.subcd,
	es.subname,
	es.suballotpnt
FROM
	examsubject es
WHERE
	es.examyear = ? /* 1 */
AND
	es.examcd = ? /* 2 */
AND
	es.subcd IN (#)
ORDER BY
	es.dispsequence

SELECT
	'�i' || p.prefname ||'�j',
	NVL(rp.numbers, -999),
	NVL(rp.avgscorerate, -999)
FROM
	qrecord_p rp,
	prefecture p
WHERE
	rp.examyear(+) = ? /* 1 */
AND
	rp.examcd(+) = ? /* 2 */
AND
	rp.prefcd(+) = p.prefcd
AND
	rp.subcd(+) = ? /* 3 */
AND
	rp.question_no(+) = ? /* 4 */
AND
	p.prefcd = ? /* 5 */

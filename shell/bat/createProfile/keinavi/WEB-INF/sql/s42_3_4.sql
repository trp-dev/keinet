SELECT
	s.bundlecd bundlecd,
	s.bundlename bundlename,
	st.examyear examyear,
	rs.subcd subcd,
	NVL(rs.avgpnt, -999) avgpnt,
	NVL(rs.numbers, -999) numbers
FROM
	school s
CROSS JOIN
	subcdtrans st
INNER JOIN
	examination e
ON
	e.examyear = st.examyear
AND
	e.examcd = st.examcd
LEFT OUTER JOIN
	school c
ON
	c.bundlecd = s.bundlecd
AND
	NVL2(c.newgetdate, c.newgetdate || '0000', '000000000000') <= e.out_dataopendate
AND
	NVL2(c.stopdate, c.stopdate || '0000', '999999999999') > e.out_dataopendate
LEFT OUTER JOIN
	subrecord_s rs
ON
	rs.examyear = st.examyear
AND
	rs.examcd = st.examcd
AND
	rs.subcd = st.subcd
AND
	rs.bundlecd = c.bundlecd
WHERE
	s.bundlecd = ?
AND
	st.examyear IN (#)
AND
	st.examcd = ?
AND
	st.cursubcd = ?
ORDER BY
	examyear DESC

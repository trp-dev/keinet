/* ¼ÌÆ©ZÌ¶kªó¯½±ÌÝÌêÌpê±R[hæ¾ */
SELECT
    s.BUNDLENAME
  , p.PREFCD
  , p.PREFNAME
  , only_me.ENGPTCD
  /* ± */
  ,( SELECT
          COUNT(*)
     FROM
          EXAMINATION exam /* Í}X^ */
        , ENGPTINFO   engi /* Fè±îñ}X^ */
     WHERE
          exam.EXAMYEAR = ? /* 1 */
     AND exam.EXAMCD   = ? /* 2 */
     AND exam.EXAMYEAR = engi.EVENTYEAR
     AND exam.EXAMDIV  = engi.EXAMDIV
   ) EXAM_COUNT
FROM
    SCHOOL     s  /* wZ}X^ */
LEFT OUTER JOIN
    (SELECT
         EXAMYEAR
       , EXAMCD
       , BUNDLECD
       , ENGPTCD
     FROM
         ENGPT_CEFRACQSTATUS_S
     GROUP BY
         EXAMYEAR
       , EXAMCD
       , BUNDLECD
       , ENGPTCD
     ) only_me    /* pêFè±ÊEvCEFRæ¾óµiZj */
    ON   s.BUNDLECD = only_me.BUNDLECD
    AND  only_me.EXAMYEAR = ? /* 3 */
    AND  only_me.EXAMCD   = ? /* 4 */

LEFT OUTER JOIN
    PREFECTURE p  /* §}X^ */
    ON    s.FACULTYCD = p.PREFCD
WHERE
    s.BUNDLECD = ? /* 5 */

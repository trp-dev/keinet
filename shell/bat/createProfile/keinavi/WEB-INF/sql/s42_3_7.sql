SELECT /*+ ORDERED */
	z.devzonecd devzonecd,
	NVL(z.highlimit, -999) highlimit,
	NVL(z.lowlimit, -999) lowlimit,
	NVL(d.numbers, -999) numbers
FROM
	scorezone_j z
INNER JOIN
	subdistrecord_s d
ON
	d.examyear = ?
AND
	d.examcd = z.examcd
AND
	d.subcd = ?
AND
	d.bundlecd = ?
AND
	d.devzonecd = z.devzonecd
WHERE
	z.examcd = ?

SELECT
	p.schoolcd,
	s.bundlename,
	p.userid,
	NVL(m.defaultpwd, p.defaultpwd),
	NvL(m.loginpwd, p.loginpwd),
	p.cert_userdiv,
	p.pactdiv,
	p.pactterm,
	p.cert_beforedata,
	p.issued_beforedate,
	NVL(p.maxsession, '0'),
	NVL(p.featprovflg, '0'),
	p.registrydate,
	p.renewaldate,
	m.loginid
FROM
	pactschool p
LEFT OUTER JOIN
	school s
ON
	s.bundlecd = p.schoolcd
LEFT OUTER JOIN
	loginid_manage m
ON
	m.schoolcd = p.schoolcd
AND
	m.manage_flg = '1'
AND
	p.pactdiv in ('1','2')
WHERE
	p.userid= ?
ORDER BY
	NVL(m.loginid, 0) DESC

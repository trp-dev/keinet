SELECT /*+ ORDERED */
	h.individualid individualid,
	h.year year,
	h.grade grade,
	h.class class,
	h.class_no class_no,
	b.sex sex,
	b.name_kana name_kana
FROM
	basicinfo b
INNER JOIN
	historyinfo h
ON
	h.individualid = b.individualid
AND
	h.year = ?
AND
	h.grade = ?
AND
	h.class = ?
WHERE
	b.schoolcd = ?
AND
	b.notdisplayflg IS NULL

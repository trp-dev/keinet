SELECT
	NVL(d.highlimit, -999) highlimit,
	NVL(d.lowlimit, -999) lowlimit,
	NVL(rp.numbers, -999) numbers,
	NVL(rp.compratio, -999) compratio
FROM
	deviationzone d
LEFT OUTER JOIN
	subdistrecord_p rp
ON
	rp.examyear = ?
AND
	rp.examcd = ?
AND
	rp.devzonecd = d.devzonecd
AND
	rp.prefcd = ?
AND
	rp.subcd = ?
ORDER BY
	lowlimit DESC

SELECT
	TRIM('�i�S���j'),
	NVL(ra.numbers, -999),
	NVL(ra.avgscorerate, -999)
FROM
	qrecord_a ra,
	examination e
WHERE
	e.examyear = ? /* 1 */
AND
	e.examcd = ? /* 2 */
AND
	ra.examyear(+) = e.examyear
AND
	ra.examcd(+) = e.examcd
AND
	ra.subcd(+) = ? /* 3 */
AND
	ra.question_no(+) = ? /* 4 */

SELECT
	NVL(QRZA.AVGSCORERATE, - 999.0) AVGSCORERATE_LEVEL 
FROM
	QRECORDZONE_A QRZA 
WHERE
	QRZA.EXAMYEAR = ? AND /* 模試年度 */
	QRZA.EXAMCD = ? AND /* 模試コード */
	QRZA.SUBCD = ? AND /* 科目コード */
	QRZA.QUESTION_NO = ? AND /* 設問番号 */
	QRZA.DEVZONECD = ? /* 学力レベル */
	

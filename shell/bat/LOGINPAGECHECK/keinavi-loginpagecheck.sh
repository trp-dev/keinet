#!/bin/sh
TGT='satu02test satu01 satu02 satu03';
LOGFILE='/keinavi/bat/LOGINPAGECHECK/keinavi-loginpagecheck.log'
THRESHOLD_SIZE="20000"

sleep 10

date > $LOGFILE
echo "Kei-Navi LoginPageCheck start" >> $LOGFILE

for svr in $TGT; do

         VALUE=`curl -I http://$svr/Login | grep Content-Length | awk '{ print $2 }' | tr -d "\r" `
         echo $VALUE

         if [ $VALUE -gt $THRESHOLD_SIZE ];then
                 echo "http://$svr/Login OK" >>  $LOGFILE

         else
                 echo "http://$svr/Login NG" >>  $LOGFILE

         fi
         sleep 1
done

INSERT INTO
	subcdtrans
SELECT
	ayear,
	?,
	asubcd,
	?
FROM
	(
		SELECT
			es.examyear ayear,
			es.subcd asubcd,
			es.examyear - 1 byear,
			NVL(st.oldsubcd, es.subcd) bsubcd
		FROM
			examsubject es,
			subjecttrans st
		WHERE
			es.examcd = ?
		AND
			st.examyear(+) = es.examyear
		AND
			st.examcd(+) = es.examcd
		AND
			st.newsubcd(+) = es.subcd
)
START WITH
	ayear = ?
AND
	asubcd = ?

CONNECT BY
	PRIOR byear = ayear
AND
	PRIOR bsubcd = asubcd

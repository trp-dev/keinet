#!/bin/bash

title="定型03_過年度データ削除_step2（個人基本情報データ削除処理）"
read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

# 共通変数
# set environment
. /keinavi/JOBNET/config/env.sh

# 変数定義
year=`date "+%Y"`
delyear=$(($year - 3))

cur=$(cd $(dirname $0); pwd)
base=/keinavi/bat/teikei
in=$base/work/delinfo_A.txt
log=$base/log/03_step2_$today.log

exec 1> >(tee -a $log)

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

# ファイル確認
if [ ! -f $in ]; then
	echo "対象年度: [ ${delyear} ] 個人基本情報データは削除されています。"
	exit 1
fi


# 個人基本情報データ削除
echo ">>> 対象年度: [ ${delyear} ] 個人基本情報データ削除処理を開始します。"
sqlplus -s ${db_uid}/${db_pwd}@${db_tns} @${base}/sql/03_step2.sql ${delyear}
echo "<<< 対象年度: [ ${delyear} ] 個人基本情報データの削除完了しました。" 
echo ""

# 個人成績データ削除定義ファイルの削除
rm -fv $in

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."
echo ""

exit

#!/bin/bash

cur=$(cd $(dirname $0); pwd)
input=/tmp/output.tsv
title="定型06_不要データ削除_step3（個人データ-削除）"

read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

# set environment
. /keinavi/JOBNET/config/env.sh

# 個人データの削除
while read year exam bundle ansno invno pri bas his
do
	echo "対象年度: [ ${year} ] 対象模試: [ ${exam} ] 高校コード:[ ${bundle} ] 解答用紙番号: [ ${ansno} ] 個人ID: [ ${invno} ] >>>"
    /keinavi/JOBNET/common/JNRZSQLEXE2.sh ${cur}/sql/06_step3.sql $year $exam $bundle $ansno $invno $pri $bas $his
	echo "<<< 対象年度: [ ${year} ] 対象模試: [ ${exam} ] 高校コード:[ ${bundle} ] 解答用紙番号: [ ${ansno} ] 個人ID: [ ${invno} ]"
    echo ""
done < $input

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

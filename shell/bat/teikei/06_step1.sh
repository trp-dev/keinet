#!/bin/bash

# パラメータチェック
if [ "$1" = "" ]; then
    echo "パラメータ１：年度が未設定です。"
    exit 1
fi
expr "$1" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ１：年度が不正です。【$1】"
    exit 1
fi

if [ "$2" = "" ]; then
    echo "パラメータ２：模試コードが未設定です。"
    exit 1
fi
expr "$2" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ２：模試コードが不正です。【$2】"
    exit 1
fi

cur=$(cd $(dirname $0); pwd)
title="定型06_不要データ削除_step1（バックアップ）"

read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""
year=$1
exam=$2
con=oracle/oracle@S11DBA_BATCH2
scm=KEINAVI
dir="directory=KEINAVI_DUMP"
opt="content=data_only reuse_dumpfiles=y"
tables1="EXAMTAKENUM_S EXAMTAKENUM_C SUBRECORD_S SUBRECORD_C SUBDISTRECORD_S SUBDISTRECORD_C SUBDEVZONERECORD_S QRECORD_S QRECORD_C QRECORDZONE MARKQDEVZONEANSRATE_S RATINGNUMBER_S RATINGNUMBER_C MARKQSCORERATE SHOQUESTIONRECORD_S"
tables2="BASICINFO HISTORYINFO CANDIDATERATING CANDIDATERATING_PP INDIVIDUALRECORD INDIVIDUALRECORD_TMP QRECORD_I QRECORD_PP SUBACADEMIC_I SUBACADEMIC_I_PP SUBRECORD_I SUBRECORD_PP"
wheres=(BASICINFO HISTORYINFO)
query="query=\"where EXAMYEAR='${year}' and EXAMCD='${exam}'\""

# 指定テーブル分ループ(集計データ)
if [ "$3" != "1" ]; then
    for tbl in $tables1; do
        # エクスポート
        echo "【$scm.$tbl】>>>"
        expdp $con $dir tables=$scm.$tbl dumpfile=$scm.$tbl.dmp logfile=$scm.$tbl"_"EXP.log $opt $query
        ret=$?
        echo "<<<【$scm.$tbl】"
        echo ""
        if [ "$ret" != "0" ]; then
            echo "【$scm.$tbl】のエクスポートでエラーが発生しました、処理を中断します。"
            echo ""
            echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} error."
            exit 1
        fi
    done
fi

# 指定テーブル分ループ(個人データ)
for tbl in $tables2; do
    # queryを付与するか
    if printf '%s\n' "${wheres[@]}" | grep -qx "${tbl}"; then
        where=""
    else
        where=$query
    fi
    # エクスポート
    echo "【$scm.$tbl】>>>"
    expdp $con $dir tables=$scm.$tbl dumpfile=$scm.$tbl.dmp logfile=$scm.$tbl"_"EXP.log $opt $where
    ret=$?
    echo "<<<【$scm.$tbl】"
    echo ""
    if [ "$ret" != "0" ]; then
        echo "【$scm.$tbl】のエクスポートでエラーが発生しました、処理を中断します。"
        echo ""
        echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} error."
        exit 1
    fi
done

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

#!/bin/bash

cur=$(cd $(dirname $0); pwd)
input=/tmp/target.tsv
output=/tmp/output.tsv
title="定型06_不要データ削除_step3（対象データ抽出）"

read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

# set environment
. /keinavi/JOBNET/config/env.sh

# 対象ファイルの受信
rm -f $output
count=0
sum=0
while read year exam bundle from to
do
	echo -n "対象年度: [ ${year} ] 対象模試: [ ${exam} ] 高校コード:[ ${bundle} ] 解答用紙番号: [ ${from} ～ ${to} ] >>> "
    /keinavi/JOBNET/common/JNRZSQLEXE2.sh ${cur}/sql/06_step2.sql $year $exam $bundle $from $to>>$output
    all=`cat $output | wc -l`
    count=$((all - sum))
    sum=$((sum + count))
	echo "$(printf "%3d" $count) 件"
done < $input

# 結果表示
echo ""
echo "$output >>>"
cat $output
echo "<<<$output"
echo "$(printf "%4d" `cat $output | wc -l `) 件出力しました。"
echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

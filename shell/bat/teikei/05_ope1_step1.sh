#!/bin/bash

title="定型05_高校コード置換_Kei-Navi個人・集計データ更新_step1（対象登録）"

read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""


# 環境設定
cur=$(cd $(dirname $0); pwd)


# 対象データ抽出
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${cur}/sql/05_ope1_step1.sql

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

#!/bin/bash

title="定型24_共通高2・高2記述大学マスタ登録バッチ切替(登録準備)"

read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""
hosts="satu04test satu04"
for i in ${hosts}; do
ssh ora_ins_account@$i /bin/bash <<EOC
        echo ""
        echo "${i} >>>"

        cd /keinavi/JOBNET/JNRZ0552
        cp -av JNRZ0552_02.sql.kou2mark_desc JNRZ0552_02.sql
        diff -s JNRZ0552_02.sql.kou2mark_desc JNRZ0552_02.sql
        ls -l JNRZ0552_02.sql JNRZ0552_02.sql.kou2mark_desc JNRZ0552_02.sql.normal
        echo ""


        cd /keinavi/JOBNET/JNRZ0553
        cp -av JNRZ0553_11.sql.kou2mark_desc JNRZ0553_11.sql
        diff -s JNRZ0553_11.sql.kou2mark_desc JNRZ0553_11.sql
        ls -l JNRZ0553_11.sql JNRZ0553_11.sql.kou2mark_desc JNRZ0553_11.sql.normal
        echo ""


        cd /keinavi/JOBNET/JNRZ0554
        cp -av JNRZ0554_02.sql.kou2mark_desc JNRZ0554_02.sql
        diff -s JNRZ0554_02.sql.kou2mark_desc JNRZ0554_02.sql
        ls -l JNRZ0554_02.sql JNRZ0554_02.sql.kou2mark_desc JNRZ0554_02.sql.normal
        echo ""


        cd /keinavi/JOBNET/JNRZ0556
        cp -av JNRZ0556_02.sql.kou2mark_desc JNRZ0556_02.sql
        diff -s JNRZ0556_02.sql.kou2mark_desc JNRZ0556_02.sql
        ls -l JNRZ0556_02.sql JNRZ0556_02.sql.kou2mark_desc JNRZ0556_02.sql.normal
        echo ""


        cd /keinavi/JOBNET/JNRZ0559
        cp -av JNRZ0559_02.sql.kou2mark_desc JNRZ0559_02.sql
        diff -s JNRZ0559_02.sql.kou2mark_desc JNRZ0559_02.sql
        ls -l JNRZ0559_02.sql JNRZ0559_02.sql.kou2mark_desc JNRZ0559_02.sql.normal

        echo "<<< ${i}"
        echo ""
EOC
done

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

#!/bin/bash

# パラメータチェック
if [ "$1" = "" ]; then
    echo "パラメータ１：処理区分が未設定です。"
    exit 1
fi
if [ "$1" = "1" ] || [ "$1" = "2" ]; then
    echo "" > /dev/null
else
    echo "パラメータ１：処理区分が不正です。【$1】"
    exit 1
fi

title="定型21_解約校の契約中データ削除・廃止コードのレコード削除・ひな型一括登録_step2（廃止コードのデータ削除）"
read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

# set environment
. /keinavi/JOBNET/config/env.sh

cur=$(cd $(dirname $0); pwd)
input=/keinavi/bat/teikei/work/target.tsv

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

while read col1 col2 col3
do
	list=(${col2//,/ })
	echo ">>> 対象高校コード"
	for cd in ${list[@]}; do
		echo "  ${cd//\'/}"
	done
	echo "<<< 対象高校コード ${#list[*]}件"
	echo ""
	echo ">>> 対象テーブル"
    /keinavi/JOBNET/common/JNRZSQLEXE2.sh ${cur}/sql/21_step2.sql \"$1\"
	echo "<<< 対象テーブル"
done < $input

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

#!/bin/bash

title="定型21_解約校の契約中データ削除・廃止コードのレコード削除・ひな型一括登録_step0（バックアップ）"
read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

# 共通変数
. /keinavi/JOBNET/config/env.sh

cur=$(cd $(dirname $0); pwd)
con=oracle/oracle@$db_tns
scm=$db_uid
dir="directory=KEINAVI_DUMP"
opt="content=data_only reuse_dumpfiles=y"
tables="CLASS_GROUP CLASSGROUP LOGIN_CHARGE LOGINID_MANAGE PACTSCHOOL PROFILE PROFILEFOLDER"

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

# 指定テーブル分ループ
for tbl in $tables; do
    # エクスポート
    echo ">>> [$scm.$tbl]"
    expdp $con $dir tables=$scm.$tbl dumpfile=$scm.$tbl.dmp logfile=$scm.$tbl"_"EXP.log $opt
    ret=$?
    echo "<<< [$scm.$tbl]"
    echo ""
    if [ "$ret" != "0" ]; then
        echo "[$scm.$tbl] のエクスポートでエラーが発生しました、処理を中断します。"
        echo ""
        echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} error."
        exit 1
    fi
done

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

#!/bin/bash

# パラメータチェック
if [ "$1" = "" ]; then
    echo "パラメータ１：今年度が未設定です。"
    exit 1
fi
expr "$1" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ１：今年度が不正です。【$1】"
    exit 1
fi

# パラメータチェック
if [ "$2" = "" ]; then
    echo "パラメータ２：前年度が未設定です。"
    exit 1
fi
expr "$2" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ２：前年度が不正です。【$2】"
    exit 1
fi

cur=$(cd $(dirname $0); pwd)
title="定型19_大学コード紐付けマスタ登録"

read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

# set environment
. /keinavi/JOBNET/config/env.sh

# 大学コード紐付けマスタ登録
sqlplus -s ${db_uid}/${db_pwd}@${db_tns} @${cur}/sql/19_step1.sql "$1" "$2"

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

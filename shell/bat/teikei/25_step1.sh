#!/bin/bash

# パラメータチェック
if [ "$1" = "" ]; then
    echo "パラメータ１：作業日が未設定です。"
    exit 1
fi
expr "$1" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ１：作業日が不正です。【$1】"
    exit 1
fi

title="定型25-1_入試結果調査5月実施分(公表用大学マスタ09区分HULFT受信・退避)"
read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

# set environment
. /keinavi/JOBNET/config/env.sh
cur=$(cd $(dirname $0); pwd)
hulft=/usr/local/HULFT/bin/utlrecv
host=k03308f6gp
yyyy=$((`date "+%Y"` - 1))
input=$cur/prop/25_step1.tsv
target=/keinavi/HULFT/NewEXAM/Univmaster/${yyyy}"09_"$1

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""


# 対象ファイルの受信
cd /keinavi/HULFT
echo ">>> 対象ファイルの受信"
while read id tbl name
do
    if [ "$id" != "" ]; then
        echo -n "${id} ... "
        _started_at=$(date +'%s.%3N')
        # HULFT受信
        ${hulft} -f ${id} -h ${host}  -sync -w 300
        #touch /keinavi/HULFT/NewEXAM/$tbl`date "+%Y%m%d%H%M%S"`
        if [ $? -ne 0 ]; then
            echo "[${id}]受信でエラーが発生しました、処理を中断します。"
        fi
        # 完了時刻を取得
        _ended_at=$(date +'%s.%3N')
        # 経過時間を計算
        _elapsed=$(echo "scale=3; $_ended_at - $_started_at" | bc)
        echo "【${tbl}】[`printf "%.3f" ${_elapsed}`s] finished."
    fi
done < $input
echo "<<< 対象ファイルの受信"
echo ""

# 0バイトファイルの削除
echo ">>> 0バイトファイルの削除"
. /keinavi/JOBNET/Fget/delete_size0.sh
echo "<<< 0バイトファイルの削除"
echo ""

# 登録用ファイルの移動
echo ">>> 登録用ファイルの移動"
mkdir -v ${target}
chmod -v 777 ${target}
chown -v orauser.ora_ins_group ${target}
cd /keinavi/HULFT/NewEXAM/
mv -fv UNIV* PUB_* JH0* DB* ${target}
echo "<<< 登録用ファイルの移動"
echo ""

# ファイル名の変更
echo ">>> ファイル名の変更"
cd ${target}
. /home/kei-navi/cutDatetime.sh ./ "" 1
echo "<<< ファイル名の変更"
echo ""

# 権限・所有者の変更
echo ">>> 権限・所有者の変更"
chmod -v 777 ./*
chown -v orauser.ora_ins_group ./*
echo "<<< 権限・所有者の変更"

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

#!/bin/bash

title="定型03_過年度データ削除_step1（件数取得）"
read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""$name
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

# 共通変数
# set environment
. /keinavi/JOBNET/config/env.sh

# 変数定義
year=`date "+%Y"`
delyear=$(($year - 3))

cur=$(cd $(dirname $0); pwd)
base=/keinavi/bat/teikei
mst=$base/prop/03_step1_mast.lst
trn=$base/prop/03_step1_tran.lst
tmp=$base/work/tmp.sql
out=$base/work/count_$today.tsv
log=$base/log/03_step1_$today.log

exec 1> >(tee -a $log)


echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""


# マスタ件数の出力
rm -f $out
while read tbl sql
do
	echo -n "> $tbl"
	echo "set heading off">$tmp
	echo "set verify off">>$tmp
	echo "set trimspool on">>$tmp
	echo "set feedback off">>$tmp
	echo "set linesize 10000">>$tmp
	echo "set pagesize 0">>$tmp
	echo "${sql}">>$tmp
	echo "EXIT">>$tmp
	# SQLを実行
	sqlplus -s ${db_uid}/${db_pwd}@${db_tns} @${tmp} ${delyear}>>$out
	echo " finished."
done < $mst
echo ""

# 削除
rm -f $tmp

# トラン件数の出力
while read tbl col
do
	echo -n "> $tbl"
	# SQLを実行
	sqlplus -s ${db_uid}/${db_pwd}@${db_tns} @${base}/sql/03_step1.sql ${tbl} ${col}>>$out
	echo " finished."
done < $trn
echo ""

# コンソールに表示
echo ">>> `readlink -f $out`"
cat $out
echo "<<< `readlink -f $out`"

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."
echo ""

exit

#!/bin/bash

cur=$(cd $(dirname $0); pwd)
input=/tmp/target.tsv
hosts="satu01 satu02 satu03 satu02test"
title="定型06_不要データ削除_step6（ファイル削除）"
year=$1
exam=$2

read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

# set environment
. /keinavi/JOBNET/config/env.sh

# ファイルの削除
del1=""
del2=""
while read year exam bundle from to
do
    del1="${del1} /keinavi/freemenu/exam/${year}/${exam}/${bundle}.csv"
    del2="${del2} /keinavi/freemenu/ability/${year}/${exam}/${bundle}.csv"
done < $input
for host in $hosts; do
    echo "host: [ ${host} ] >>>"
    ssh $host "rm -fv ${del1}"
    ssh $host "ls -l ${del1}"
    echo ""
    ssh $host "rm -fv ${del2}"
    ssh $host "ls -l ${del2}"
    echo "<<< host: [ ${host} ]"
    echo ""
done

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

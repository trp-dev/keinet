#!/bin/bash

# パラメータチェック
if [ "$1" = "" ]; then
    echo "パラメータ１：年度が未設定です。"
    exit 1
fi
expr "$1" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ１：年度が不正です。【$1】"
    exit 1
fi

if [ "$2" = "" ]; then
    echo "パラメータ２：模試区分が未設定です。"
    exit 1
fi
expr "$2" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ２：模試区分が不正です。【$2】"
    exit 1
fi

title="定型08_模試判定用大学マスタ作成(IMPORT)"

read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
	echo "中断しました。"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

# 変数定義
year=$1
div=$2
con=oracle/oracle@S11DBA_BATCH2
fscm=KEINAVI_STG
tscm=MOSHIHANTEI
dir="directory=KEINAVI_DUMP"
opt="content=data_only table_exists_action=truncate"
tables="EXAMINATION EXAMSUBJECT GUIDEREMARKS GUIDESUBSTRING JH01_KAMOKU1 JH01_KAMOKU2 JH01_KAMOKU3 JH01_KAMOKU4 PREFECTURE RANK UNIVMASTER_BASIC UNIVMASTER_CHOICESCHOOL UNIVMASTER_COURSE UNIVMASTER_INFO UNIVMASTER_OTHERNAME UNIVMASTER_SELECTGPCOURSE UNIVMASTER_SELECTGROUP UNIVMASTER_SUBJECT UNIVSTEMMA"

# インポート
for tbl in $tables; do
    echo "【$tscm.$tbl】>>>"
    impdp $con $dir remap_schema=$fscm:$tscm tables=$fscm.$tbl dumpfile=$fscm.$tbl"_"$year$div.dmp logfile=$tscm.$tbl"_"IMP.log $opt
    ret=$?
    echo "<<<【$tscm.$tbl】"
    echo ""
    if [ "$ret" != "0" ]; then
        echo "【$tscm.$tbl】のインポートでエラーが発生しました、処理を中断します。"
        echo ""
        echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} error."
        exit 1
    fi
done

# 件数確認
count=`sqlplus -s $tscm/39ra8ma@S11DBA_BATCH2 <<EOF
set heading off
set verify off
set trimspool on
set feedback off
set linesize 10000
set pagesize 0
select count(*) from UNIVMASTER_BASIC where EVENTYEAR='$year' and EXAMDIV='$div';
EOF`
echo "tscm.UNIVMASTER_BASIC【${year}年${div}区分】：${count}件"

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

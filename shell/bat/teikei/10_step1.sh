#!/bin/sh


# パラメータチェック
if [ "$1" = "" ]; then
    echo "パラメータ１：年度が未設定です。"
    exit 1
fi
expr "$1" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ１：年度が不正です。【$1】"
    exit 1
fi

if [ "$2" = "" ]; then
    echo "パラメータ２：リサーチデータ受信日が未設定です。"
    exit 1
fi
if [ ! -d "/keinavi/HULFT/NewEXAM/${1}38_${2}_4" ]; then
    echo "パラメータ２：リサーチデータ受信日が存在しません。【$2】"
    exit 1
fi

# set environment
. /keinavi/JOBNET/config/env.sh

title="定型10_共通高2・高2記述大学マスタハイブリッド(Loader→07区分→09区分)"
src="/keinavi/HULFT/NewEXAM/${1}38_${2}_4"
dst=/keinavi/HULFT/WorkDir
back=/home/orauser
#環境変数設定
export JOBNAME=JNRZ0551
export CSVFILE=UNIVMASTER_BASIC
export SRCDIR=/keinavi/HULFT/WorkDir
export WORKDIR=/keinavi/JOBNET/$JOBNAME

read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

# 不要ファイルの削除
echo "[不要ファイルの削除] >>>"
rm -fv $dst/*-old*
echo "<<< [不要ファイルの削除]"
echo ""

# 残存ファイルのバックアップ
echo "[残存ファイルのバックアップ] >>>"
files=$(find ${dst} -type f ! -name "SCHOOL" -and ! -name "PACTSCHOOL_SELECTLIST")
for fp in $files; do
    #ファイル名を取得
    file=$(basename $fp)
    #ディレクトリが存在しない場合
    if [ ! -d $back/$today ]; then
        #ディレクトリを作成
        mkdir -pv $back/$today
        chmod -v 766 $back/$today
    fi
    #ファイルの移動
    mv -fv $dst/$file $back/$today
done
echo "<<< [残存ファイルのバックアップ]"
echo ""

echo "[UNIVMASTER_BASICのコピー] >>>"
scp -p orauser@satu04:${src}/UNIVMASTER_BASIC ${dst}
chown -v orauser.ora_ins_group ${dst}/UNIVMASTER_BASIC
chmod -v 777 ${dst}/UNIVMASTER_BASIC
ls -l ${dst}
echo "<<< [UNIVMASTER_BASICのコピー]"
echo ""

#SQL*PLUSテンポラリテーブル作成
echo "[UNIVMASTER_BASICの取込] >>>"
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ0551_01.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    /keinavi/JOBNET/common/JNRZ0ERR.sh
    echo テンポラリテーブル作成処理でエラーが発生しました
    exit 30
fi


#SQL*LOADERシェル実行
/keinavi/JOBNET/common/JNRZ0IMP.sh

#SQL*LOADER戻り値チェック(0以外は異常終了)
RET=$?
if [ $RET -ne 0 ]
then
    if [ $RET -le 10 ]
    then
        #SQL*PLUSテンポラリテーブル削除
        /keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ0551_04.sql
        echo 処理対象ファイルなし（正常終了）
        exit 0
    else
        /keinavi/JOBNET/common/JNRZ0ERR.sh
        echo SQL*LOADERでエラーが発生しました
        exit 30
    fi
fi


#SQL*PLUSデータインポート->テンポラリテーブル削除
/keinavi/JOBNET/common/JNRZSQLEXE2.sh ${WORKDIR}/JNRZ0551_02.sql.hybrid $1

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    /keinavi/JOBNET/common/JNRZ0ERR.sh
    echo データのインポート処理でエラーが発生しました
    exit 30
fi

#SQL*PLUSデータインポート->テンポラリテーブル削除
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${WORKDIR}/JNRZ0551_03.sql

#SQL*PLUS戻り値チェック(0以外は異常終了)
if [ $? -ne 0 ]
then
    /keinavi/JOBNET/common/JNRZ0ERR.sh
    echo データのインポート処理でエラーが発生しました
    exit 30
fi

#正常終了
/keinavi/JOBNET/common/JNRZ0END.sh
echo "<<< [UNIVMASTER_BASICの取込]"
echo ""
echo "データのインポート処理が正常に終了しました。"

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

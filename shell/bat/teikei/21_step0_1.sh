#!/bin/bash

title="定型21_解約校の契約中データ削除・廃止コードのレコード削除・ひな型一括登録_step0（削除対象データの登録）"
read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

# set environment
. /keinavi/JOBNET/config/env.sh

cur=$(cd $(dirname $0); pwd)
input=/keinavi/bat/teikei/work/target.tsv
c1=0
c2=0
c3=0

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

# データ削除
/keinavi/JOBNET/common/JNRZSQLEXE2.sh ${cur}/sql/21_step0_1.sql

while read kbn cd
do
    /keinavi/JOBNET/common/JNRZSQLEXE2.sh ${cur}/sql/21_step0_2.sql \"${kbn}\" \"${cd}\"
	if [ "${kbn}" = "1" ]; then
		c1=$(($c1 + 1))
	fi
	if [ "${kbn}" = "2" ]; then
		c2=$(($c2 + 1))
	fi
	if [ "${kbn}" = "3" ]; then
		c3=$(($c3 + 1))
	fi
done < $input
echo ">>> step1:解約校のデータ削除     ${c1}件"
echo ">>> step2:廃止コードのデータ削除 ${c2}件"
echo ">>> step3:ひな型一括登録         ${c3}件"

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

set SERVEROUTPUT ON FORMAT WRAPPED
set LINESIZE 1024
set HEAD off
set FEED off
set verify off
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
VARIABLE vEXIT NUMBER;

DECLARE
	cnt					number  := 0;
-- メイン処理
BEGIN

	:vEXIT := 0;

	if '&1' = '1' THEN
		-- 対象件数取得
		select count(1) into cnt from PACTSCHOOL where exists (select 1 from TEIKEI_21 b where b.KBN = '2' and PACTSCHOOL.SCHOOLCD = b.BUNDLECD);
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('PACTSCHOOL', 14) || ' ] 対象件数 = [ ' || LPAD(TO_CHAR(cnt, 'fm9G999'), 5) || ' ]');
		select count(1) into cnt from LOGINID_MANAGE where exists (select 1 from TEIKEI_21 b where b.KBN = '2' and LOGINID_MANAGE.SCHOOLCD = b.BUNDLECD);
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('LOGINID_MANAGE', 14) || ' ] 対象件数 = [ ' || LPAD(TO_CHAR(cnt, 'fm9G999'), 5) || ' ]');
	else
		-- データ削除
		delete from PACTSCHOOL where exists (select 1 from TEIKEI_21 b where b.KBN = '2' and PACTSCHOOL.SCHOOLCD = b.BUNDLECD);
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('PACTSCHOOL', 14) || ' ] 削除件数 = [ ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm9G999'), 5) || ' ]');
		delete from LOGINID_MANAGE where exists (select 1 from TEIKEI_21 b where b.KBN = '2' and LOGINID_MANAGE.SCHOOLCD = b.BUNDLECD);
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('LOGINID_MANAGE', 14) || ' ] 削除件数 = [ ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm9G999'), 5) || ' ]');
		commit;
	end if;

-- 例外処理
EXCEPTION
	WHEN OTHERS THEN
		:vEXIT := -1;
		DBMS_OUTPUT.PUT_LINE('!!! 廃止コードのデータ削除処理でエラーが発生しました !!!');
		DBMS_OUTPUT.PUT_LINE('  >> ' || SQLERRM);
		ROLLBACK;
END;
/
EXIT :vEXIT;

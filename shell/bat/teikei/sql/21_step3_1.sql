set SERVEROUTPUT ON FORMAT WRAPPED
set LINESIZE 1024
set HEAD off
set FEED off
set verify off
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
VARIABLE vEXIT NUMBER;

DECLARE
	cnt					number  := 0;
-- メイン処理
BEGIN

	:vEXIT := 0;

	if '&1' = '1' THEN
		-- 対象件数取得
		select count(1) into cnt from PROFILE where exists (select 1 from TEIKEI_21 b where b.KBN = '3' and PROFILE.BUNDLECD = b.BUNDLECD);
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('PROFILE', 14) || ' ] 対象件数 = [ ' || LPAD(TO_CHAR(cnt, 'fm9G999'), 5) || ' ]');
		select count(1) into cnt from PROFILEFOLDER where exists (select 1 from TEIKEI_21 b where b.KBN = '3' and PROFILEFOLDER.BUNDLECD = b.BUNDLECD);
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('PROFILEFOLDER', 14) || ' ] 対象件数 = [ ' || LPAD(TO_CHAR(cnt, 'fm9G999'), 5) || ' ]');
	else
		-- データ削除
		delete from PROFILE where exists (select 1 from TEIKEI_21 b where b.KBN = '3' and PROFILE.BUNDLECD = b.BUNDLECD);
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('PROFILE', 14) || ' ] 削除件数 = [ ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm9G999'), 5) || ' ]');
		delete from PROFILEFOLDER where exists (select 1 from TEIKEI_21 b where b.KBN = '3' and PROFILEFOLDER.BUNDLECD = b.BUNDLECD);
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('PROFILEFOLDER', 14) || ' ] 削除件数 = [ ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm9G999'), 5) || ' ]');
		commit;
	end if;

-- 例外処理
EXCEPTION
	WHEN OTHERS THEN
		:vEXIT := -1;
		DBMS_OUTPUT.PUT_LINE('!!! ひな型一括登録のデータ削除処理でエラーが発生しました !!!');
		DBMS_OUTPUT.PUT_LINE('  >> ' || SQLERRM);
		ROLLBACK;
END;
/
EXIT :vEXIT;

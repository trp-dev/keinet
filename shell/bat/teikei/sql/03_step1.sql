set heading off
set verify off
set trimspool on
set feedback off
set linesize 10000
set pagesize 0
set colsep '	'

select '&1' || chr(9) || &2 || chr(9) || COUNT(1)
  from &1
 group by &2
 order by &2;

EXIT

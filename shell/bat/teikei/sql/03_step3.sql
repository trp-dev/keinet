set SERVEROUTPUT ON FORMAT WRAPPED
set LINESIZE 1024
set HEAD off
set FEED off
set verify off
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
begin
    delete from &1 where EXAMYEAR='&2' and EXAMCD='&3';
    DBMS_OUTPUT.PUT_LINE(RPAD(TO_CHAR(SQL%ROWCOUNT, 'fm999G999G999'), 12) || '行が削除されました。');
    commit;
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('!!! データ削除処理でエラーが発生しました !!!');
        DBMS_OUTPUT.PUT_LINE('  >> ' || SQLERRM);
        ROLLBACK;
end;
/
EXIT;

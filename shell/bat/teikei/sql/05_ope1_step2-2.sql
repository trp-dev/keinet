SET SERVEROUTPUT ON FORMAT WRAPPED
SET LINESIZE 1024
set verify off
SET HEAD OFF
SET FEED OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
VARIABLE vEXIT NUMBER;

DECLARE
	summary             VARCHAR(30) := NULL;
	dup_cnt             NUMBER := 0;
	FUNCTION CHECK_COUNT(to_bundlecd REPLACE_BUNDLECD.TO_BUNDLECD%TYPE, summary REPLACE_BUNDLECD.SUMMARY_ONLY%TYPE, cur_table REPLACE_TABLE%ROWTYPE) RETURN NUMBER IS
		year                CHAR(4);
		examcd              CHAR(2);
		bundlecd            CHAR(6);
		cnt                 NUMBER := 0;
		query               LONG := null;
		total               NUMBER := 0;
		TYPE cur_typ        IS REF CURSOR;
		cur_target          cur_typ;
	BEGIN

		-- 置換対象が集計データのみで、個人データテーブルは処理対象から除外する
		IF summary = '1' AND cur_table.PERSON_FLG = '1' THEN
			RETURN 0;
		END IF;

		-- 重複分クエリーを生成
		query := NULL;
		query := query || 'SELECT EXAMYEAR, EXAMCD, ' || cur_table.COLUMN_NAME || ', COUNT(*)';
		query := query || '  FROM ' || cur_table.TABLE_NAME || ' t';
		query := query || ' WHERE EXISTS (';
		query := query || '            SELECT 1';
		query := query || '              FROM REPLACE_TARGET s';
		query := query || '             WHERE s.EXAMYEAR = t.EXAMYEAR';
		query := query || '               AND s.TO_BUNDLECD = t.' || cur_table.COLUMN_NAME;
		query := query || '               AND s.EXAMCD   = t.EXAMCD';
		query := query || '       )';
		query := query || '   AND t.' || cur_table.COLUMN_NAME || ' = :BUNDLECD';
		-- テーブル固有の条件を追加
		IF cur_table.ADD_WHERE IS NOT NULL THEN
			query := query || ' AND t.' || cur_table.ADD_WHERE;
		END IF;
		query := query || ' GROUP BY EXAMYEAR, EXAMCD, ' || cur_table.COLUMN_NAME;
		--DBMS_OUTPUT.PUT_LINE(query);

		-- カーソルオープン
		OPEN cur_target FOR query USING to_bundlecd;
		<<RECORD_LOOP>>
		LOOP

			-- フェッチ
			FETCH cur_target INTO year, examcd, bundlecd, cnt;
			EXIT RECORD_LOOP WHEN cur_target%NOTFOUND;
			-- 重複分を加算
			total := total + 1;
			-- 重複分をログに出力
			DBMS_OUTPUT.PUT_LINE('  >> [ ' || LPAD(cur_table.TABLE_NAME, 21) || ' ] に重複レコード有り！！ > 対象年度 = [ ' || year || ' ] 高校コード = [ ' || bundlecd || ' ] 対象模試 = [ ' || examcd || ' ] 件数 = [ ' || TO_CHAR(cnt, '9G999') || ' ]');
			:vEXIT := 1;

		END LOOP RECORD_LOOP;
		-- カーソルクローズ
		CLOSE cur_target;

		-- 結果を返却
		RETURN total;
	END;
-- メイン処理
BEGIN

	:vEXIT := 0;

	dup_cnt := 0;
	-- 置換対象テーブル読み込み
	FOR cur_table IN (
		SELECT *
		  FROM REPLACE_TABLE
		)
	LOOP

		-- 各テーブルの件数をチェック
		dup_cnt := dup_cnt + CHECK_COUNT('&1', '&2', cur_table);

	END LOOP;
	IF dup_cnt = 0 THEN
		DBMS_OUTPUT.PUT_LINE('  >> 重複レコード無し');
	END IF;

-- 例外処理
EXCEPTION
	WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE('!!! 高校コード置換 （ チェック ） 処理にてエラーが発生しました !!!');
		DBMS_OUTPUT.PUT_LINE('  >> ' || SQLERRM);
END;
/
EXIT :vEXIT;

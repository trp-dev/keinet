SET SERVEROUTPUT ON FORMAT WRAPPED
SET LINESIZE 1024
set verify off
SET HEAD OFF
SET FEED OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
VARIABLE vEXIT NUMBER;

DECLARE
	summary             VARCHAR(30) := NULL;
	query               LONG := NULL;
	PROCEDURE EXEC_REPLACE(cur_main REPLACE_BUNDLECD%ROWTYPE) IS
		query               LONG := NULL;
	BEGIN

		-- 置換対象テーブル読み込み
		FOR cur_table IN (
			SELECT *
			  FROM REPLACE_TABLE
			)
		LOOP

			-- 置換対象が集計データのみで、個人データテーブルは処理対象から除外する
			IF cur_main.SUMMARY_ONLY = '1' AND cur_table.PERSON_FLG = '1' THEN
				CONTINUE;
			END IF;

			-- 更新クエリーを生成
			query := NULL;
			query := query || 'UPDATE ' || cur_table.TABLE_NAME || ' t';
			query := query || '   SET ' || cur_table.COLUMN_NAME || ' = :TO_BUNDLECD';
			query := query || ' WHERE EXISTS (';
			query := query || '            SELECT 1';
			query := query || '              FROM REPLACE_TARGET s';
			query := query || '             WHERE s.EXAMYEAR = t.EXAMYEAR';
			query := query || '               AND s.FROM_BUNDLECD = t.' || cur_table.COLUMN_NAME;
			query := query || '               AND s.EXAMCD   = t.EXAMCD';
			query := query || '               AND t.' || cur_table.COLUMN_NAME || ' = :FROM_BUNDLECD';
			query := query || '       )';
			-- テーブル固有の条件を追加
			IF cur_table.ADD_WHERE IS NOT NULL THEN
				query := query || ' AND t.' || cur_table.ADD_WHERE;
			END IF;

			-- 置換
			EXECUTE IMMEDIATE query USING cur_main.TO_BUNDLECD, cur_main.FROM_BUNDLECD;
			DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD(cur_table.TABLE_NAME, 21) || ' ] COLUMN:[ ' || LPAD(cur_table.COLUMN_NAME, 12) || ' ]を置換  件数 = [ ' || TO_CHAR(SQL%ROWCOUNT, '99999') || ' ]');
		END LOOP;

	END;
-- メイン処理
BEGIN

	:vEXIT := 0;

	-- 高校コード置換対象読み込み
	FOR cur_main IN (
		SELECT *
		  FROM REPLACE_BUNDLECD
		 WHERE FROM_BUNDLECD = '&1'
		   AND TO_BUNDLECD = '&2'
		   AND BEGIN_EXAMYEAR = '&3'
		   AND END_EXAMYEAR = '&4'
		)
	LOOP

		-- 各テーブルの高校コードを置換
		EXEC_REPLACE(cur_main);

		-- 置換対象が集計データのみの場合、学籍基本情報は更新しない
		IF cur_main.SUMMARY_ONLY = '1' THEN
			CONTINUE;
		END IF;

		-- 学籍基本情報の置換
		IF cur_main.GRADE IS NULL THEN
			-- BASICINFOの置換(SCHOOLCD)
			UPDATE BASICINFO
			   SET SCHOOLCD = cur_main.TO_BUNDLECD
			 WHERE SCHOOLCD = cur_main.FROM_BUNDLECD;
			DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('BASICINFO', 21) || ' ] COLUMN:[ ' || LPAD('SCHOOLCD', 12) || ' ]を置換  件数 = [ ' || TO_CHAR(SQL%ROWCOUNT, '9G999G999') || ' ]');

			-- BASICINFOの置換(HOMESCHOOLCD)
			UPDATE BASICINFO
			   SET HOMESCHOOLCD = cur_main.TO_BUNDLECD
			 WHERE HOMESCHOOLCD = cur_main.FROM_BUNDLECD;
			DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('BASICINFO', 21) || ' ] COLUMN:[ ' || LPAD('HOMESCHOOLCD', 12) || ' ]を置換  件数 = [ ' || TO_CHAR(SQL%ROWCOUNT, '9G999G999') || ' ]');
		ELSE
			-- BASICINFOの置換(SCHOOLCD)
			query := NULL;
			query := query || 'UPDATE BASICINFO b';
			query := query || '   SET SCHOOLCD = :TO_BUNDLECD';
			query := query || ' WHERE EXISTS (';
			query := query || '    SELECT 1';
			query := query || '    FROM REPLACE_TARGET s';
			query := query || '   INNER JOIN HISTORYINFO h';
			query := query || '      ON s.EXAMYEAR = h.YEAR';
			query := query || '     AND h.GRADE = :GRADE';
			query := query || '   WHERE b.SCHOOLCD = s.FROM_BUNDLECD';
			query := query || '     AND b.INDIVIDUALID = h.INDIVIDUALID';
			query := query || '     AND b.SCHOOLCD = :FROM_BUNDLECD)';
			EXECUTE IMMEDIATE query USING cur_main.TO_BUNDLECD, cur_main.GRADE, cur_main.FROM_BUNDLECD;
			DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('BASICINFO', 21) || ' ] COLUMN:[ ' || LPAD('SCHOOLCD', 12) || ' ]を置換  件数 = [ ' || TO_CHAR(SQL%ROWCOUNT, '9G999G999') || ' ]');

			-- BASICINFOの置換(HOMESCHOOLCD)
			query := NULL;
			query := query || 'UPDATE BASICINFO b';
			query := query || '   SET HOMESCHOOLCD = :TO_BUNDLECD';
			query := query || ' WHERE EXISTS (';
			query := query || '    SELECT 1';
			query := query || '    FROM REPLACE_TARGET s';
			query := query || '   INNER JOIN HISTORYINFO h';
			query := query || '      ON s.EXAMYEAR = h.YEAR';
			query := query || '     AND h.GRADE = :GRADE';
			query := query || '   WHERE b.HOMESCHOOLCD = s.FROM_BUNDLECD';
			query := query || '     AND b.INDIVIDUALID = h.INDIVIDUALID';
			query := query || '     AND b.HOMESCHOOLCD = :FROM_BUNDLECD)';
			EXECUTE IMMEDIATE query USING cur_main.TO_BUNDLECD, cur_main.GRADE, cur_main.FROM_BUNDLECD;
			DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('BASICINFO', 21) || ' ] COLUMN:[ ' || LPAD('HOMESCHOOLCD', 12) || ' ]を置換  件数 = [ ' || TO_CHAR(SQL%ROWCOUNT, '9G999G999') || ' ]');
		END IF;

		-- クラスグループの置換
		UPDATE CLASS_GROUP 
		   SET BUNDLECD = cur_main.TO_BUNDLECD
		 WHERE BUNDLECD = cur_main.FROM_BUNDLECD;
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('CLASS_GROUP', 21) || ' ] COLUMN:[ ' || LPAD('BUNDLECD', 12) || ' ]を置換  件数 = [ ' || TO_CHAR(SQL%ROWCOUNT, '9G999G999') || ' ]');

		COMMIT;
	END LOOP;

	COMMIT;

-- 例外処理
EXCEPTION
	WHEN OTHERS THEN
		:vEXIT := -1;
		DBMS_OUTPUT.PUT_LINE('!!! 高校コード置換処理にてエラーが発生しました !!!');
		DBMS_OUTPUT.PUT_LINE('  >> ' || SQLERRM);
		ROLLBACK;
END;
/
EXIT :vEXIT;

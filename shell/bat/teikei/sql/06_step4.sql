set SERVEROUTPUT ON FORMAT WRAPPED
set LINESIZE 1024
set HEAD off
set FEED off
set verify off
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
VARIABLE vEXIT NUMBER;

DECLARE
	pEXAMYEAR			char(4) := '&1';
	pEXAMCD				char(2) := '&2';
	pBUNDLECD			char(5) := '&3';
	cnt					number  := 0;
-- メイン処理
BEGIN

	:vEXIT := 0;

	-- 集計データ対象件数取得
	select count(1) into cnt from EXAMTAKENUM_C where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('EXAMTAKENUM_C', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	select count(1) into cnt from EXAMTAKENUM_S where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('EXAMTAKENUM_S', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	select count(1) into cnt from MARKQDEVZONEANSRATE_S where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('MARKQDEVZONEANSRATE_S', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	select count(1) into cnt from MARKQSCORERATE where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('MARKQSCORERATE', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	select count(1) into cnt from QRECORD_C where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('QRECORD_C', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	select count(1) into cnt from QRECORD_S where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('QRECORD_S', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	select count(1) into cnt from QRECORDZONE where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('QRECORDZONE', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	select count(1) into cnt from RATINGNUMBER_C where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('RATINGNUMBER_C', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	select count(1) into cnt from RATINGNUMBER_S where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('RATINGNUMBER_S', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	select count(1) into cnt from SHOQUESTIONRECORD_S where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('SHOQUESTIONRECORD_S', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	select count(1) into cnt from SUBDEVZONERECORD_S where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('SUBDEVZONERECORD_S', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	select count(1) into cnt from SUBDISTRECORD_C where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('SUBDISTRECORD_C', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	select count(1) into cnt from SUBDISTRECORD_S where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('SUBDISTRECORD_S', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	select count(1) into cnt from SUBRECORD_C where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('SUBRECORD_C', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	select count(1) into cnt from SUBRECORD_S where EXAMYEAR = pEXAMYEAR and EXAMCD = pEXAMCD and BUNDLECD = pBUNDLECD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('SUBRECORD_S', 21) || ' ] 対象件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');

-- 例外処理
EXCEPTION
	WHEN OTHERS THEN
		:vEXIT := -1;
		DBMS_OUTPUT.PUT_LINE('!!! 集計データ-対象取得処理でエラーが発生しました !!!');
		DBMS_OUTPUT.PUT_LINE('  >> ' || SQLERRM);
		ROLLBACK;
END;
/
EXIT :vEXIT;

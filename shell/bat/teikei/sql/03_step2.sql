set SERVEROUTPUT ON FORMAT WRAPPED
set LINESIZE 1024
set HEAD off
set FEED off
set verify off
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
declare
begin
	/* 面談メモの削除 */
	delete from INTERVIEW where INDIVIDUALID in (select distinct INDIVIDUALID from HISTORYINFO where YEAR='&1' MINUS
	select distinct INDIVIDUALID from HISTORYINFO where INDIVIDUALID in (select INDIVIDUALID from HISTORYINFO where NOT YEAR='&1') and YEAR='&1');
	DBMS_OUTPUT.PUT_LINE('  [ INTERVIEW     ] >>> ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm999G999'), 7) || '行が削除されました。');
	
	/* こだわり条件の削除 */
	delete from HANGUPPROVISO where INDIVIDUALID in (select distinct INDIVIDUALID from HISTORYINFO where YEAR='&1' MINUS
	select distinct INDIVIDUALID from HISTORYINFO where INDIVIDUALID in (select INDIVIDUALID from HISTORYINFO where NOT YEAR='&1') and YEAR='&1');
	DBMS_OUTPUT.PUT_LINE('  [ HANGUPPROVISO ] >>> ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm999G999'), 7) || '行が削除されました。');

	/* 判定履歴トランの削除 */
	delete from DETERMHISTORY where INDIVIDUALID in (select distinct INDIVIDUALID from HISTORYINFO where YEAR='&1' MINUS
	select distinct INDIVIDUALID from HISTORYINFO where INDIVIDUALID in (select INDIVIDUALID from HISTORYINFO where NOT YEAR='&1') and YEAR='&1');
	DBMS_OUTPUT.PUT_LINE('  [ DETERMHISTORY ] >>> ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm999G999'), 7) || '行が削除されました。');

	/* 受験予定大学の削除 */
	delete from EXAMPLANUNIV where INDIVIDUALID in (select distinct INDIVIDUALID from HISTORYINFO where YEAR='&1' MINUS
	select distinct INDIVIDUALID from HISTORYINFO where INDIVIDUALID in (select INDIVIDUALID from HISTORYINFO where NOT YEAR='&1') and YEAR='&1');
	DBMS_OUTPUT.PUT_LINE('  [ EXAMPLANUNIV  ] >>> ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm999G999'), 7) || '行が削除されました。');
	commit;

	/* 学籍基本情報の削除 */
	delete from BASICINFO where INDIVIDUALID in (select distinct INDIVIDUALID from HISTORYINFO where YEAR='&1' MINUS
	select distinct INDIVIDUALID from HISTORYINFO where INDIVIDUALID in (select INDIVIDUALID from HISTORYINFO where NOT YEAR='&1') and YEAR='&1');
	DBMS_OUTPUT.PUT_LINE('  [ BASICINFO     ] >>> ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm999G999'), 7) || '行が削除されました。');
	commit;

	/* 学籍履歴情報の削除 */
	delete from HISTORYINFO where YEAR='&1';
	DBMS_OUTPUT.PUT_LINE('  [ HISTORYINFO   ] >>> ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm999G999'), 7) || '行が削除されました。');
	commit;

	/* 複合クラスの削除 */
	delete from CLASSGROUP where CLASSGCD in (select CLASSGCD from CLASS_GROUP where YEAR='&1');
	DBMS_OUTPUT.PUT_LINE('  [ CLASSGROUP    ] >>> ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm999G999'), 7) || '行が削除されました。');

	/* クラス－複合クラス対応の削除 */
	delete from CLASS_GROUP where YEAR='&1';
	DBMS_OUTPUT.PUT_LINE('  [ CLASS_GROUP   ] >>> ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm999G999'), 7) || '行が削除されました。');
	commit;
end;
/
EXIT;

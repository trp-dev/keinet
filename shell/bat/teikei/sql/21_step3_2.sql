set heading off
set verify off
set trimspool on
set feedback off
set linesize 10000
set pagesize 0
set colsep ','

select SCHOOLCD || ',' || USERID as col
  from PACTSCHOOL 
 where exists (select 1 from TEIKEI_21 b where b.KBN = '3' and PACTSCHOOL.SCHOOLCD = b.BUNDLECD)
 order by SCHOOLCD;

EXIT

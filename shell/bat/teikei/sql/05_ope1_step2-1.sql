set heading off
set verify off
set trimspool on
set feedback off
set linesize 10000
set pagesize 0
set colsep '	'
select
    i.BEGIN_EXAMYEAR || CHR(9) ||
    i.END_EXAMYEAR || CHR(9) ||
    i.FROM_BUNDLECD || CHR(9) ||
    i.TO_BUNDLECD || CHR(9) ||
    NVL(i.EXAMCD, 'すべて') || CHR(9) ||
    i.SUMMARY_ONLY as col
  from
    REPLACE_BUNDLECD i;
EXIT

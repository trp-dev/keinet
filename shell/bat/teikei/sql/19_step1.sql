set SERVEROUTPUT ON FORMAT WRAPPED
set LINESIZE 1024
set HEAD off
set FEED off
set verify off
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
VARIABLE vEXIT NUMBER;

DECLARE
	cnt					number  := 0;
-- メイン処理
BEGIN

	:vEXIT := 0;

	select count(*)
	  into cnt
	  from UNIVCDHOOKUP
	 where YEAR in ('&1', '&2');
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ UNIVCDHOOKUP ] 登録前件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');
	
	insert into UNIVCDHOOKUP
	select distinct
			  '&1'                            as YEAR
			, '00'                            as OLDEXAMDIV
			, a.NEWUNIV8CD                    as OLDUNIV8CD
			, nvl(b.NEWUNIV8CD, a.NEWUNIV8CD) as NEWUNIV8CD 
		from
			( 
				select
						aa.NEWUNIV8CD 
					from
						UNIVCDHOOKUP aa 
					where
						aa.year = '&2' 
						and aa.OLDEXAMDIV = '08'
			) a 
			left join ( 
				select
						bb.OLDUNIV8CD
						, min(bb.NEWUNIV8CD) as NEWUNIV8CD 
					from
						UNIVCDTRANS_ORD bb 
					where
						year = '&1' 
						and EXAMCD = '01'
					group by bb.OLDUNIV8CD
			) b 
				on a.NEWUNIV8CD = b.OLDUNIV8CD;
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ UNIVCDHOOKUP ] 登録件数   = [ ' || TO_CHAR(SQL%ROWCOUNT, '9999999') || ' ]');

	select count(*)
	  into cnt
	  from UNIVCDHOOKUP
	 where YEAR in ('&1', '&2');
	DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ UNIVCDHOOKUP ] 登録後件数 = [ ' || TO_CHAR(cnt, '9999999') || ' ]');

	commit;

-- 例外処理
EXCEPTION
	WHEN OTHERS THEN
		:vEXIT := -1;
		DBMS_OUTPUT.PUT_LINE('!!! 大学コード紐付けマスタ-登録処理でエラーが発生しました !!!');
		DBMS_OUTPUT.PUT_LINE('  >> ' || SQLERRM);
		ROLLBACK;
END;
/
EXIT :vEXIT;

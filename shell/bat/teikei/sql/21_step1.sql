set SERVEROUTPUT ON FORMAT WRAPPED
set LINESIZE 1024
set HEAD off
set FEED off
set verify off
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
VARIABLE vEXIT NUMBER;

DECLARE
	cnt					number  := 0;
-- メイン処理
BEGIN

	:vEXIT := 0;

	if '&1' = '1' THEN
		-- 対象件数取得
		select count(1) into cnt from LOGINID_MANAGE where exists (select 1 from TEIKEI_21 b where b.KBN = '1' and LOGINID_MANAGE.SCHOOLCD = b.BUNDLECD) and LOGINID_MANAGE.LOGINNAME <> 'システム管理者' and LOGINID_MANAGE.LOGINID NOT IN ('user1', 'user2', 'user3', 'user4');
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('LOGINID_MANAGE', 14) || ' ] 対象件数 = [ ' || LPAD(TO_CHAR(cnt, 'fm9G999'), 5) || ' ]');
		select count(1) into cnt from PROFILE where exists (select 1 from TEIKEI_21 b where b.KBN = '1' and PROFILE.BUNDLECD = b.BUNDLECD) and SECTORSORTINGCD is null;
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('PROFILE', 14) || ' ] 対象件数 = [ ' || LPAD(TO_CHAR(cnt, 'fm9G999'), 5) || ' ]');
		select count(1) into cnt from PROFILEFOLDER where exists (select 1 from TEIKEI_21 b where b.KBN = '1' and PROFILEFOLDER.BUNDLECD = b.BUNDLECD) and SECTORSORTINGCD is null;
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('PROFILEFOLDER', 14) || ' ] 対象件数 = [ ' || LPAD(TO_CHAR(cnt, 'fm9G999'), 5) || ' ]');
		select count(1) into cnt from CLASSGROUP m where exists(select 1 from CLASS_GROUP s where exists (select 1 from TEIKEI_21 b where b.KBN = '1' and s.BUNDLECD = b.BUNDLECD) and m.CLASSGCD = s.CLASSGCD);
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('CLASSGROUP', 14) || ' ] 対象件数 = [ ' || LPAD(TO_CHAR(cnt, 'fm9G999'), 5) || ' ]');
		select count(1) into cnt from CLASS_GROUP where exists (select 1 from TEIKEI_21 b where b.KBN = '1' and CLASS_GROUP.BUNDLECD = b.BUNDLECD);
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('CLASS_GROUP', 14) || ' ] 対象件数 = [ ' || LPAD(TO_CHAR(cnt, 'fm9G999'), 5) || ' ]');
		select count(1) into cnt from LOGIN_CHARGE where exists (select 1 from TEIKEI_21 b where b.KBN = '1' and LOGIN_CHARGE.SCHOOLCD = b.BUNDLECD);
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('LOGIN_CHARGE', 14) || ' ] 対象件数 = [ ' || LPAD(TO_CHAR(cnt, 'fm9G999'), 5) || ' ]');
	else
		-- データ削除
		delete from LOGINID_MANAGE where exists (select 1 from TEIKEI_21 b where b.KBN = '1' and LOGINID_MANAGE.SCHOOLCD = b.BUNDLECD) and LOGINID_MANAGE.LOGINNAME <> 'システム管理者' and LOGINID_MANAGE.LOGINID NOT IN ('user1', 'user2', 'user3', 'user4');
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('LOGINID_MANAGE', 14) || ' ] 削除件数 = [ ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm9G999'), 5) || ' ]');
		delete from PROFILE where exists (select 1 from TEIKEI_21 b where b.KBN = '1' and PROFILE.BUNDLECD = b.BUNDLECD) and SECTORSORTINGCD is null;
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('PROFILE', 14) || ' ] 削除件数 = [ ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm9G999'), 5) || ' ]');
		delete from PROFILEFOLDER where exists (select 1 from TEIKEI_21 b where b.KBN = '1' and PROFILEFOLDER.BUNDLECD = b.BUNDLECD) and SECTORSORTINGCD is null;
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('PROFILEFOLDER', 14) || ' ] 削除件数 = [ ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm9G999'), 5) || ' ]');
		delete from CLASSGROUP m where exists(select 1 from CLASS_GROUP s where exists (select 1 from TEIKEI_21 b where b.KBN = '1' and s.BUNDLECD = b.BUNDLECD) and m.CLASSGCD = s.CLASSGCD);
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('CLASSGROUP', 14) || ' ] 削除件数 = [ ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm9G999'), 5) || ' ]');
		delete from CLASS_GROUP where exists (select 1 from TEIKEI_21 b where b.KBN = '1' and CLASS_GROUP.BUNDLECD = b.BUNDLECD);
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('CLASS_GROUP', 14) || ' ] 削除件数 = [ ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm9G999'), 5) || ' ]');
		delete from LOGIN_CHARGE where exists (select 1 from TEIKEI_21 b where b.KBN = '1' and LOGIN_CHARGE.SCHOOLCD = b.BUNDLECD);
		DBMS_OUTPUT.PUT_LINE('  >> TABLE:[ ' || LPAD('LOGIN_CHARGE', 14) || ' ] 削除件数 = [ ' || LPAD(TO_CHAR(SQL%ROWCOUNT, 'fm9G999'), 5) || ' ]');
		commit;
	end if;

-- 例外処理
EXCEPTION
	WHEN OTHERS THEN
		:vEXIT := -1;
		DBMS_OUTPUT.PUT_LINE('!!! 解約校の契約中データ削除処理でエラーが発生しました !!!');
		DBMS_OUTPUT.PUT_LINE('  >> ' || SQLERRM);
		ROLLBACK;
END;
/
EXIT :vEXIT;

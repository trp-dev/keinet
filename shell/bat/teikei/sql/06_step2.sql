set heading off
set verify off
set trimspool on
set feedback off
set linesize 10000
set pagesize 0
set colsep '	'
select
    i.EXAMYEAR || CHR(9) ||
    i.EXAMCD || CHR(9) ||
    i.BUNDLECD || CHR(9) ||
    i.ANSWERSHEET_NO || CHR(9) ||
    i.S_INDIVIDUALID || CHR(9) ||
    i.PRIVERCYFLG || CHR(9) ||
    case when o.EXAMYEAR is null then '1' 
    else '0' end || CHR(9) ||
    case when o.EXAMYEAR is null or i.EXAMYEAR <> o.EXAMYEAR then '1' 
    else '0' end as col
  from
    INDIVIDUALRECORD i 
    left join ( 
      select
          max(ii.EXAMYEAR) as EXAMYEAR
          , ii.S_INDIVIDUALID 
        from
          INDIVIDUALRECORD ii 
          inner join ( 
            select
                EXAMYEAR
                , EXAMCD
                , S_INDIVIDUALID 
              from
                INDIVIDUALRECORD i 
              where
                EXAMYEAR = '&1' 
                and EXAMCD = '&2' 
                and BUNDLECD = '&3' 
                and ANSWERSHEET_NO between '&4' and '&5'
          ) t 
            on ii.S_INDIVIDUALID = t.S_INDIVIDUALID 
        where
          ii.EXAMYEAR || ii.EXAMCD <> '&1&2' 
        group by
          ii.S_INDIVIDUALID
    ) o 
      on i.S_INDIVIDUALID = o.S_INDIVIDUALID 
  where
    i.EXAMYEAR = '&1' 
    and i.EXAMCD = '&2' 
    and i.BUNDLECD = '&3' 
    and i.ANSWERSHEET_NO between '&4' and '&5';
EXIT

set verify off

-- 登録した09区分のデータを10年前に更新
update UNIVMASTER_BASIC set EVENTYEAR='&2' where EVENTYEAR='&1' AND EXAMDIV='09';

-- 不要分を削除(登録前)
delete from UNIVMASTER_BASIC where EVENTYEAR='&2' and EXAMDIV='09' and UNIVCD >= '8000' and UNIVCD not in ('8517', '8518', '8530', '8611', '8612', '8613', '8614', '8615');

-- ハイブリッド用のテーブルを削除
drop table UNIVMASTER_BASIC_HYBRID purge;

-- ハイブリッド用のテーブルを作成し、09区分のデータを登録
create table UNIVMASTER_BASIC_HYBRID as
select univ.*
  from (select * from UNIVMASTER_BASIC where EVENTYEAR='&2' and EXAMDIV='09') univ             --- ★10年前の09区分とする。
 inner join (
  select UNIVCD, FACULTYCD, DEPTCD from UNIVMASTER_BASIC where EVENTYEAR='&2' and EXAMDIV='09' --- ★10年前の09区分とする。
  minus
  select UNIVCD, FACULTYCD, DEPTCD from UNIVMASTER_BASIC where EVENTYEAR='&1' and EXAMDIV='08' --- ★今年度の08区分とする。
 ) diff
    on univ.UNIVCD = diff.UNIVCD
   and univ.FACULTYCD = diff.FACULTYCD
   and univ.DEPTCD = diff.DEPTCD;

-- 不要分を削除(登録後)
delete from UNIVMASTER_BASIC_HYBRID where EVENTYEAR='&2' and EXAMDIV='09' and EXAMUSEDIV='0' and PURSUEUSEDIV='1';

-- 10年前→当年度、09区分→08区分に更新
update UNIVMASTER_BASIC_HYBRID set EVENTYEAR='&1', EXAMDIV='08' where EVENTYEAR='&2' and EXAMDIV='09';

-- UNIVMASTER_BASICにハイブリッド用のデータを登録
insert into UNIVMASTER_BASIC select * from UNIVMASTER_BASIC_HYBRID where EVENTYEAR='&1' and EXAMDIV='08';

-- 件数確認
select EXAMUSEDIV, PURSUEUSEDIV, count(*) as count from UNIVMASTER_BASIC where EVENTYEAR='&1' and EXAMDIV='08' group by EXAMUSEDIV,PURSUEUSEDIV;

commit;

exit;

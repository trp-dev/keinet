#!/bin/bash

# パラメータチェック
if [ "$1" = "" ]; then
    echo "パラメータ１：処理サイクルが未設定です。"
    exit 1
fi
if [ "$1" = "1" ] || [ "$1" = "2" ] || [ "$1" = "3" ]; then
    echo "" > /dev/null
else
    echo "パラメータ１：処理サイクルが不正です。【$1】"
    exit 1
fi

title="定型03_過年度データ削除_step3（個人成績データ削除処理）"
read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

# 共通変数
# set environment
. /keinavi/JOBNET/config/env.sh

# 変数定義
year=`date "+%Y"`
delyear=$(($year - 3))

cur=$(cd $(dirname $0); pwd)
base=/keinavi/bat/teikei
in1=$base/work/exam_B.lst
in2=$base/work/table_B_$1.lst
log=$base/log/03_step3_$today.log

exec 1> >(tee -a $log)

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

# ファイル確認
if [ ! -f $in1 ]; then
	echo "対象年度: [ ${delyear} ] 個人成績データは削除されています。"
	exit 1
fi


# 個人成績データ削除
while read examcd
do
	while read table
	do
		echo -n "テーブル:[ ${table} ] 対象年度: [ ${delyear} ] 対象模試: [ ${examcd} ] >>>"
		# SQLを実行
		sqlplus -s ${db_uid}/${db_pwd}@${db_tns} @${base}/sql/03_step3.sql ${table} ${delyear} ${examcd}
		echo ""
	done < $in2
done < $in1

# 個人成績データ削除定義ファイルの削除
rm -fv $in2
echo ""

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."
echo ""

exit

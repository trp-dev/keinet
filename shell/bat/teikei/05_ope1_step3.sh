#!/bin/bash

title="定型05_高校コード置換_Kei-Navi個人・集計データ更新_step2（高校コード置換）"

read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""


# 環境設定
cur=$(cd $(dirname $0); pwd)
target=/tmp/output.tsv

# 対象データ抽出
/keinavi/JOBNET/common/JNRZSQLEXE.sh ${cur}/sql/05_ope1_step2-1.sql>>$target

while read year_s year_e bundle_s bundle_e exam summ
do
	if [ "${summ}" == "0" ]; then
		summary="個人・集計データ"
	else
		summary="集計データ"
	fi

	echo "■高校コード = [ ${bundle_s} -> ${bundle_e} ] 対象年度 = [ ${year_s} ～ ${year_e} ] 対象模試  = [ ${exam} ] 置換対象 = [ ${summary} ] >>>"
	/keinavi/JOBNET/common/JNRZSQLEXE2.sh ${cur}/sql/05_ope1_step3.sql ${year_s} ${year_e} ${bundle_s} ${bundle_e}
	echo ""
	echo ""

done < $target

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

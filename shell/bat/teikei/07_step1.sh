#!/bin/bash

title="定型07_利用者ID変更対応_step1（バックアップ）"
read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

# 共通変数
# set environment
. /keinavi/JOBNET/config/env.sh

cur=$(cd $(dirname $0); pwd)
con=oracle/oracle@S11DBA_BATCH2
scm=${db_uid}
dir="directory=KEINAVI_DUMP"
opt="content=data_only reuse_dumpfiles=y"
tables="LOGINID_MANAGE"

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

# 指定テーブル分ループ
for tbl in $tables; do
    # エクスポート
    echo "[$scm.$tbl] >>>"
    expdp $con $dir tables=$scm.$tbl dumpfile=$scm.$tbl.dmp logfile=$scm.$tbl"_"EXP.log $opt
    ret=$?
    echo "<<< [$scm.$tbl]"
    echo ""
    if [ "$ret" != "0" ]; then
        echo "[$scm.$tbl] のエクスポートでエラーが発生しました、処理を中断します。"
        echo ""
        echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} error."
        exit 1
    fi
done

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

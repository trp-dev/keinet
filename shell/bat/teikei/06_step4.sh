#!/bin/bash

cur=$(cd $(dirname $0); pwd)
input=/tmp/target.tsv
title="定型06_不要データ削除_step4（集計データ-件数確認）"

read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

# set environment
. /keinavi/JOBNET/config/env.sh

# 集計データの対象取得
while read year exam bundle from to
do
	echo "対象年度: [ ${year} ] 対象模試: [ ${exam} ] 高校コード:[ ${bundle} ] >>>"
    /keinavi/JOBNET/common/JNRZSQLEXE2.sh ${cur}/sql/06_step4.sql $year $exam $bundle
	echo "<<< 対象年度: [ ${year} ] 対象模試: [ ${exam} ] 高校コード:[ ${bundle} ]"
    echo ""
done < $input

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

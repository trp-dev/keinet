#!/bin/bash

# パラメータチェック
if [ "$1" = "" ]; then
    echo "パラメータ１：処理サイクルが未設定です。"
    exit 1
fi
if [ "$1" = "1" ] || [ "$1" = "2" ] || [ "$1" = "3" ]; then
    echo "" > /dev/null
else
    echo "パラメータ１：処理サイクルが不正です。【$1】"
    exit 1
fi

title="定型03_過年度データ削除_step0（準備処理）"
read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

# 共通変数
# set environment
. /keinavi/JOBNET/config/env.sh

# 変数定義
year=`date "+%Y"`
delyear1=$(($year - 3))
delyear2=$(($year - 7))

cur=$(cd $(dirname $0); pwd)
base=/keinavi/bat/teikei
tbl=$base/prop/03_table.lst
out1=$base/work/table_B_$1.lst
out2=$base/work/table_C_$1.lst
out3=$base/work/exam_B.lst
out4=$base/work/exam_C.lst
out5=$base/work/delinfo_A.txt
log=$base/log/03_step0_$today.log

con=oracle/oracle@$db_tns
scm=$db_uid
dir="directory=KEINAVI_DUMP"
opt="content=data_only reuse_dumpfiles=y"

exec 1> >(tee -a $log)

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

# 個人データ削除定義ファイル作成
if [ -f $out1 ]; then
	rm -f $out1
fi
if [ -f $out2 ]; then
	rm -f $out2
fi
while read kbn cycle tbl
do
	if [ "$cycle" = "$1" ]; then
		# 個人基本情報データ
		if [ "$kbn" = "A" ]; then
			tbls=$tbls" "$tbl
		fi
		# 個人成績データ
		if [ "$kbn" = "B" ]; then
			tbls=$tbls" "$tbl
			echo $tbl>>$out1
		fi
		# 集計データ
		if [ "$kbn" = "C" ]; then
			tbls=$tbls" "$tbl
			echo $tbl>>$out2
		fi
	fi
done < $tbl

echo ""
echo "個人成績データ削除定義ファイルを作成しました。 >>> $out1"
cat $out1
echo ""
echo "集計データ削除定義ファイルを作成しました。 >>> $out2"
cat $out2
echo ""

# 個人成績データ削除用対象模試一覧取得
if [ ! -f $out3 ]; then
	sqlplus -s ${db_uid}/${db_pwd}@${db_tns} @${base}/sql/03_step0.sql ${delyear1}>$out3
	echo "個人成績データ削除用対象模試一覧ファイルを作成しました。 >>> $out3"
	cat $out3
	echo ""
fi
# 集計データ削除用対象模試一覧取得
if [ ! -f $out4 ]; then
	sqlplus -s ${db_uid}/${db_pwd}@${db_tns} @${base}/sql/03_step0.sql ${delyear2}>$out4
	echo "集計データ削除用対象模試一覧ファイルを作成しました。 >>> $out4"
	cat $out4
	echo ""
fi

if [ "$1" = "1" ]; then
	touch $out5
	echo "基本情報削除用定義ファイルを作成しました。 >>> $out5"
	echo ""
fi

# エクスポート
echo ">>> [対象テーブルEXPORT]"
for tbl in $tbls; do
    echo ">>>【${scm}.${tbl}】"
    expdp ${con} ${dir} tables=${scm}.${tbl} dumpfile=${scm}.${tbl}.dmp logfile=${scm}.${tbl}"_"EXP.log ${opt}
    ret=$?
    echo "<<<【$scm.$tbl】"
    echo ""
    if [ "$ret" != "0" ]; then
        echo "【$scm.$tbl】のエクスポートでエラーが発生しました、処理を中断します。"
        echo ""
        echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} error."
        exit 1
    fi
done
echo "<<< [対象テーブルEXPORT]"

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."
echo ""

exit

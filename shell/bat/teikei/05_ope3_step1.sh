#!/bin/bash

title="定型05_高校コード置換_学校マスタ新規取得日変更"

read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""


now=$(date "+%Y%m%d")
base=$(cd $(dirname $0); pwd)
rows=$(cat /tmp/addSql.txt | wc -l)
rows=$((rows+5))

cd /keinavi/JOBNET/JNRZ0620


# ファイルをバックアップ
echo "既存ファイルをバックアップ"
cp -av options.sql options.sql.$now


# 追加分をoptions.sqlに結合
echo ""
echo "SQLを追加"
cat options.sql /tmp/addSql.txt>options.sql.edit


# EXITを削除
sed -i -e 's/EXIT//g' options.sql.edit


# EXITを末尾に追加
sed -i '$a EXIT' options.sql.edit
echo ""


# ファイルを移動
echo "編集を確定"
mv -fv options.sql.edit options.sql
echo ""


# 旧ファイルの末尾
echo "↓↓↓旧ファイルの末尾↓↓↓"
tail -n5 options.sql.$now
echo "↑↑↑旧ファイルの末尾↑↑↑"
echo ""


# 新ファイルの末尾
echo "↓↓↓新ファイルの末尾↓↓↓"
tail -n$rows options.sql
echo "↑↑↑新ファイルの末尾↑↑↑"
echo ""
echo "ファイルの末尾に正しく追加されているか確認してください。"

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

#!/bin/sh

title="定型09_模試判定用大学マスタ参照先変更(検証機：hybrid→normal)"

read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

src="/keinavi/bat/teikei/prop/"
dst="/usr/local/tomcat/webapps2/inet/WEB-INF/classes/jp/co/fj/keinavi/resources/"

hosts="satu01test satu02test satu03test"
for host in $hosts; do
    echo "***** 【$host】に通常版のcommon.propertiesをコピー *****"
    scp -p ${src}common.properties.test kei-navi@$host:${dst}common.properties 
    ssh $host "cat ${dst}common.properties | grep NDBConnUserID"
done

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

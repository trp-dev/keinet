#!/bin/bash

title="定型10_共通高2・高2記述大学マスタハイブリッド(09区分→08区分)"

read -p "【${title}】処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
    echo "中断しました。"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} start."
echo ""

# set environment
. /keinavi/JOBNET/config/env.sh

cur=$(cd $(dirname $0); pwd)

# sqlを実行
sqlplus -s $db_uid/$db_pwd@$db_tns @$cur/sql/10.sql $1 $2

echo ""
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "${title} end."

exit 0

#!/bin/sh

HTTPD="/usr/local/apache2/bin/httpd"
CONF="/usr/local/apache2/conf"

ACMD="$1"

interval=10
pidfile=/var/run/httpd2.pid

#apacheプロセスIDの取得
org=`ps aux|grep httpd|grep root|grep -v grep|awk '{print $2}'`

#apacheシャットダウンの実行
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "apache shutdown start. pid=$org"
${HTTPD} -k stop
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "waiting for $interval sec(s) for apache shutdown process."
sleep $interval


#apacheのプロセス確認
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "check apache process."
pid=`ps aux|grep httpd|grep root|grep -v grep|awk '{print $2}'`
#プロセスが残っている場合
if [ x"$pid" != x ]; then
    #killコマンド(デフォルト)で終了
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "apache is still working. Now send it SIGTERM. pid=$pid"
    kill -TERM $pid
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "waiting for $interval sec(s) for apache shutdown process."
    sleep $interval

    #apacheのプロセス再確認
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "check apache process."
    for i in `ps aux|grep httpd|grep -v grep|awk '{print $2}'` 
    do
        #プロセスが残っている場合
        echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "apache is still working. Now send it SIGKILL. pid=$i"
        kill -KILL $i
    done
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "waiting for $interval sec(s) for apache shutdown process."
    sleep $interval
fi


#apacheのプロセスを再確認
pid=`ps aux|grep httpd|grep root|grep -v grep|awk '{print $2}'`
#プロセスが残っている場合
if [ x"$pid" != x ]; then
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "apache shutdown failed."
    exit 1
else
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "apache shutdown success. pid=$org"
    rm -rf $pidfile
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "apache startup ${ACMD} start."
if [ "${ACMD}" = "normal" ] ; then
    ${HTTPD} -k start -f ${CONF}/httpd.conf
    break
fi
if [ "${ACMD}" = "mainte" ] ; then
    ${HTTPD} -k start -f ${CONF}/httpd.conf.mainte
    break
fi
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "waiting for $interval sec(s) for apache startup process."
sleep $interval

#apacheのプロセスを再確認
pid=`ps aux|grep httpd|grep root|grep -v grep|awk '{print $2}'`
#プロセスが残っている場合
if [ x"$pid" != x ]; then
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "apache startup ${ACMD} success. pid=$pid"
    ERROR=0
else
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "apache startup ${ACMD} failed."
    ERROR=1
fi
exit $ERROR

#! /bin/sh


# パラメータ構成
# 第一：転送元サーバの転送ファイル配置フォルダのフルパス
# 第二：転送先ーバの転送ファイル配置フォルダのフルパス
# 第三：転送ファイル名
# 第四：転送元サーバアドレス

if [ $# -lt 4 ]
then
        echo "parameter error"
        exit
fi

echo "$1 $2 $3 $4"

if [ $# = 5 ]
then
        luser="ora_ins_account"
else
        luser="orauser"
fi

ftp -n $4 << EOF
user ${luser} 0ra9le
bin
cd $1
lcd $2
get ${3}
quit
EOF

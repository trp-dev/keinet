#!/usr/bin/perl
########################################
# 集計ログお掃除スクリプト
#  前日のアクセスがなかったときにも作成される
#　アクセスログを削除するスクリプト
#

use Time::Local;

######################################################
# 設定パラメータ

#@targetSVR = ("satu01test","satu02test","satu03test", " ");        # 対象サーバリスト
@targetSVR = ("satu01", "satu02", "satu03"); # 対象サーバリスト
$baseDIR = "/keinavi/help_user/Log/";	# 基本ディクレトリ位置
$baseFILE1 = "access_KeiNavi_";		# 基本ファイル名
$baseFILE2 = "access_Kounai_";		# 基本ファイル名
$baseFILE3 = "access_inet_";		# 基本ファイル名
$baseFILE4 = "access_minet_";		# 基本ファイル名

$headerFILE = "/home/kei-navi/header_accessLog.txt";

#
######################################################

my @IN_FILE_LIST;

######################################################
# 集計対象のファイル情報を取得
#

#
# １日前の日付を取得
#
($mday, $mon, $year) = (localtime(time - 1*24*60*60))[3..5];
$year += 1900;
$mon += 1;
$targetString = sprintf("%04d%02d%02d", $year, $mon, $mday);
#print "yesterday: $targetString\n";	# debug


#
# 読み込みファイルリストの作成
#
$fileCounter = 0;
for($ii = 0; $ii < $#targetSVR + 1; $ii++){
	$IN_FILE_LIST[$fileCounter] = $baseDIR . $baseFILE1
				. $targetSVR[$ii] . "_" . $targetString . ".log.tmp";
	$fileCounter++;

	$IN_FILE_LIST[$fileCounter] = $baseDIR . $baseFILE2
				. $targetSVR[$ii] . "_" . $targetString . ".log.tmp";
	$fileCounter++;

	$IN_FILE_LIST[$fileCounter] = $baseDIR . $baseFILE3
				. $targetSVR[$ii] . "_" . $targetString . ".log.tmp";
	$fileCounter++;

	$IN_FILE_LIST[$fileCounter] = $baseDIR . $baseFILE4
				. $targetSVR[$ii] . "_" . $targetString . ".log.tmp";
	$fileCounter++;
}
#print "IN_FILE_LIST: [@IN_FILE_LIST]\n";	# debug


#
# アクセスログファイルの中身が当日の内容のものであるかをチェック
# 当日のものでなければ削除
#
for($ii = 0; $ii < $#IN_FILE_LIST + 1; $ii++){
	if($IN_FILE_LIST[$ii] eq ""){
		# ファイルがなければとばす
		next;
	}

	if(! open(IN, "$IN_FILE_LIST[$ii]")){
		# オープンエラーならとばす！？
		print "$IN_FILE_LIST[$ii] OPEN ERROR!\n";
		next;
	}
	
	$counter = 0;
	$deleteFLG = 0;
	while(<IN>){
		# 1行目はとばす
		if($counter == 0){
			$counter++;
			next;
		}

		# 2行目に対してチェック
		chomp($_);
		($id, $acctime, $ipaddr, $domain, $service, $schoolid, $schoolname, $userid, 
			$prjcode, $userkind, $screenid, $execcode, $tmp) = split(/,/, $_, 13);
		
		($checkString, $checkTime) = split(/ /, $acctime, 2);
		$checkString =~ s/\///g;
		$checkString =~ s/\-//g;

		if($checkString ne $targetString){
			$deleteFLG = 1;
		}
print "[$checkString, $targetString, $deleteFLG]\n";	# debug

		last;
	}
	close(IN);

	$FINAL_FILENAME = $IN_FILE_LIST[$ii];
	$FINAL_FILENAME =~ s/\.log\.tmp/\.log/g;

	if($deleteFLG == 1){
		# 当日のアクセスログではないので空ファイルを作成
		unlink($IN_FILE_LIST[$ii]);
		open(NEW, ">$IN_FILE_LIST[$ii]");
		close(NEW);
	}

#print "\t[cat $headerFILE $IN_FILE_LIST[$ii] > $FINAL_FILENAME]\n";	# debug
	`cat $headerFILE $IN_FILE_LIST[$ii] > $FINAL_FILENAME`;
	unlink($IN_FILE_LIST[$ii]);
}


print "\nFINISH!\n";
exit;


#!/usr/bin/perl
########################################
# 集計ログ作成スクリプト
#  モバイル版模試判定用
#

use Time::Local;

######################################################
# 設定パラメータ

@targetSVR = ("satu04");                 # 収集対象サーバリスト
$baseDIR = "/keinavi/help_user/Log/";   # 基本ディクレトリ位置
$baseFILE = "access_minet_";                    # 基本ファイル名
$outBaseFILE = "summary_MInetmoshi_";   # 出力ファイル基本名

#
######################################################

### 集計ログデータ格納ハッシュ
%userLOG;   # base
%userLOG1;      # top画面
%userLOG2;      # マーク模試成績入力画面
%userLOG3;      # 記述模試成績入力画面


my @IN_FILE_LIST;
my $OUT_FILE;

######################################################
# 集計対象のファイル情報を取得
#

#
# 今日の日付を取得
#
($mday, $mon, $year) = (localtime(time))[3..5];
$year += 1900;
$mon += 1;
#print "today: $year$mon$mday\n";       # debug


#
#  1-15日なら先月後半のログを集計対象とする
# 16-31日なら同月前半のログを集計対象とする
#
if($mday <= 15){
        $targetYear= $year;
        $targetMon = $mon - 1;
        $targetDay = "2nd";

        # 今日が1/1-15なら集計対象は前年
        if($mon == 1){
                $targetYear -= 1;
                $targetMon = 12;
        }
}
else{
        $targetYear= $year;
        $targetMon = $mon;
        $targetDay = "1st";
}
@targetDATE = collect_target($targetYear, $targetMon, $targetDay);
#@targetDATE = collect_target(2006, 12, "1st"); # debug
#print "target: @targetDATE\n"; # debug


#
# 読み込みファイルリストの作成
#
$fileCounter = 0;
for($ii = 0; $ii < $#targetDATE + 1; $ii++){
        for($jj = 0; $jj < $#targetSVR + 1; $jj++){
                $IN_FILE_LIST[$fileCounter] = $baseDIR . $baseFILE
                                        . $targetSVR[$jj] . "_" . $targetDATE[$ii] . ".log";
                $fileCounter++;
        }
}
#print "IN_FILE_LIST: [@IN_FILE_LIST]\n";       # debug


#
# 出力ファイル名の作成
#
$startDATE = $targetDATE[0];
$endDATE   = substr($targetDATE[$#targetDATE], 4);
$OUT_FILE = $baseDIR . $outBaseFILE . $startDATE . "-" . $endDATE . ".csv";
#print "OUT_FILE: $OUT_FILE\n"; # debug


for($ii = 0; $ii < $#IN_FILE_LIST + 1; $ii++){
        if($IN_FILE_LIST[$ii] eq ""){
                # ファイルがなければとばす
                next;
        }

        if(! open(IN, "$IN_FILE_LIST[$ii]")){
                # オープンエラーならとばす！？
                print "$IN_FILE_LIST[$ii] OPEN ERROR!\n";
                next;
        }

        while(<IN>){
                chomp($_);
                ($id, $acctime, $ipaddr, $domain, $service, $schoolid, $schoolname, $userid,
                        $prjcode, $userkind, $screenid, $execcode, $tmp) = split(/,/, $_, 13);

                ### 集計ルールはここに記載 (start) ###

                if($service ne "22"){   # モバイル版模試判定以外のログは無視
                        next;
                }
                if( ($screenid eq "mj001") && ($execcode == 1)){
                        $userLOG{$ipaddr}++;
                        $userLOG1{$ipaddr}++;
                }
                elsif( ($screenid eq "mj101") && ($execcode == 1)){
                        $userLOG{$ipaddr}++;
                        $userLOG2{$ipaddr}++;
                }
                elsif( ($screenid eq "mj102") && ($execcode == 1)){
                        $userLOG{$ipaddr}++;
                        $userLOG3{$ipaddr}++;
                }

                ### 集計ルールはここに記載 (end) ###
        }
        close(IN);
}

###########################################################
# ファイルへ集計ログを出力
#
if(! open(OUT, ">$OUT_FILE") ){
        print "$OUT_FILE OPEN ERROR!\n";
        exit;
}

# ヘッダ出力
#
print OUT "IPアドレス,トップ画面,成績入力画面（マーク模試）,成績入力画面（記述模試）\r\n";

# 集計結果の出力
#
foreach $key (sort keys %userLOG){
        print OUT "$key,$userLOG1{$key},$userLOG2{$key},$userLOG3{$key}\r\n";
}
close(OUT);

print "\nFINISH!\n";
exit;


sub get_tm {
        my $year = shift;
        my $month = shift;
        my $day = shift;
        return timelocal(0, 0, 0, $day, $month - 1, $year -1900);
}

sub show_date {
        my $tm = shift;
        my ($day, $month, $year) = (localtime($tm))[3..5];
        return sprintf("%04d%02d%02d", $year+1900, $month+1, $day);
}

sub collect_target {
        my ($year, $month, $day) = @_;
        my (@mlast) = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        my @targetLIST;

        my $counter = 0;
        my $dateString;

        if($day eq "1st"){
                for($i = 1; $i <= 15; $i++){
                        $dateString = sprintf("%04d%02d%02d", $year, $month, $i);
                        $targetLIST[$counter] = $dateString;
                        $counter++;
                }
        }
        elsif($day eq "2nd"){
                if( ($month == 2) &&
                        ( (($year % 4 == 0) && ($year % 100 != 0)) || ($year % 400 == 0) ) ){
                        $mlast[1]++;
                }

                for($i = 16; $i <= $mlast[$month - 1]; $i++){
                        $dateString = sprintf("%04d%02d%02d", $year, $month, $i);
                        $targetLIST[$counter] = $dateString;
                        $counter++;
                }
        }
        else{
        }

        return @targetLIST;
}


#!/usr/bin/perl
########################################
# 集計ログ作成スクリプト
#  Kei-Navi 非契約校用集計ログ
#

use Time::Local;

######################################################
# 設定パラメータ

#@targetSVR = ("satu01test","satu02test","satu03test", " ");               # 収集対象サーバリスト
@targetSVR = ("satu01", "satu02", "satu03");            # 収集対象サーバリスト
$baseDIR = "/keinavi/help_user/Log/";           # 基本ディクレトリ位置
$baseFILE= "access_KeiNavi_";                   # 基本ファイル名
$outBaseFILE= "summary_KeiNavi_Hikeiyaku_";     # 出力ファイル基本名

$pactschoolFILE = "/home/kei-navi/PACTSCHOOL/pactschool.txt"; # 契約校リストファイル

#
######################################################

### 集計ログデータ格納ハッシュ
%COL_daiko;
%COL_kjcd;
%COL_login;
%COL_kounai;
%COL_class;
%COL_kojin;
%COL_text;
%COL_mainte;
%COL_dl1;
%COL_dl2;

my @IN_FILE_LIST;
my $OUT_FILE;

######################################################
# 契約校マスタから非契約校リストを取得
#

if(! open(PACT, "$pactschoolFILE")){
        #file open error
        print "file open error! [$pactschoolFILE]\n";
        exit;
}
while(<PACT>){
        chomp($_);
        ($pactUID, $pactSID, $pactDIV) = split(/,/, $_, 3);

        $pactUID =~ s/ //g;
        $pactSID =~ s/ //g;
        $pactDIV =~ s/ //g;

        if(! (($pactDIV == 2) || ($pactDIV == 3) || ($pactDIV == 4)) ){
                next;
        }

#print "[$pactUID],\t[$pactSID],\t[$pactDIV]\n";        # debug

        $COL_login{$pactSID} = 0;
        $COL_dl1{$pactSID}   = 0;
        $COL_dl2{$pactSID}   = 0;
}
close(PACT);


######################################################
# 集計対象のファイル情報を取得
#

#
# 今日の日付を取得
#
($mday, $mon, $year) = (localtime(time))[3..5];
$year += 1900;
$mon += 1;
#print "today: $year$mon$mday\n";       # debug


#
#  1-15日なら先月後半のログを集計対象とする
# 16-31日なら同月前半のログを集計対象とする
#
if($mday <= 15){
        $targetYear= $year;
        $targetMon = $mon - 1;
        $targetDay = "2nd";

        # 今日が1/1-15なら集計対象は前年
        if($mon == 1){
                $targetYear -= 1;
                $targetMon   = 12;
        }
}
else{
        $targetYear= $year;
        $targetMon = $mon;
        $targetDay = "1st";
}
@targetDATE = collect_target($targetYear, $targetMon, $targetDay);
#@targetDATE = collect_target(2006, 12, "1st"); # debug
#print "target: @targetDATE\n"; # debug


#
# 読み込みファイルリストの作成
#
$fileCounter = 0;
for($ii = 0; $ii < $#targetDATE + 1; $ii++){
        for($jj = 0; $jj < $#targetSVR + 1; $jj++){
                $IN_FILE_LIST[$fileCounter] = $baseDIR . $baseFILE
                                        . $targetSVR[$jj] . "_" .  $targetDATE[$ii] . ".log";
                $fileCounter++;
        }
}
#print "IN_FILE_LIST: [@IN_FILE_LIST]\n";       # debug


#
# 出力ファイル名の作成
#
$startDATE = $targetDATE[0];
$endDATE   = substr($targetDATE[$#targetDATE], 4);
$OUT_FILE = $baseDIR . $outBaseFILE . $startDATE . "-" . $endDATE . ".csv";
#print "OUT_FILE: $OUT_FILE\n"; # debug


for($ii = 0; $ii < $#IN_FILE_LIST + 1; $ii++){
        if($IN_FILE_LIST[$ii] eq ""){
                # ファイルがなければとばす
                next;
        }

        if(! open(IN, "$IN_FILE_LIST[$ii]")){
                # オープンエラーならとばす！？
                print "$IN_FILE_LIST[$ii] OPEN ERROR!\n";
                next;
        }

        while(<IN>){
                chomp($_);
                ($id, $acctime, $ipaddr, $domain, $service, $schoolid, $schoolname, $userid,
                        $prjcode, $userkind, $screenid, $execcode, $tmp) = split(/,/, $_, 13);

                ### 集計ルールはここに記載 (start) ###

                if($service ne "02"){
                        next;
                }
#print "$_\n";  #debug

                if($execcode == 101){
                        $COL_login{$schoolid}++;
                }
                elsif($execcode == 421){
                        $COL_dl1{$schoolid}++;
                }
                elsif($execcode == 423){
                        $COL_dl2{$schoolid}++;
                }

                ### 集計ルールはここに記載 (end) ###
        }
        close(IN);
}

###########################################################
# ファイルへ集計ログを出力
#
if(! open(OUT, ">$OUT_FILE") ){
        print "$OUT_FILE OPEN ERROR!\n";
        exit;
}

# ヘッダ出力
#
print OUT "代行フラグ,河合塾部門コード,学校コード,ログイン回数,校内成績分析,クラス成績分析,個人成績分析,テキスト出力,メンテナンス,ダウンロード(統計集),ダウンロード(高校別)\r\n";

# 集計結果の出力
#
foreach $key (sort keys %COL_login){
        if($COL_dl1{$key} eq ""){       $COL_dl1{$key} = 0; }
        if($COL_dl2{$key} eq ""){       $COL_dl2{$key} = 0; }

        print OUT ",-,$key,$COL_login{$key},0,0,0,0,0,$COL_dl1{$key},$COL_dl2{$key}\r\n";
}
close(OUT);

print "\nFINISH!\n";
exit;



sub collect_target {
        my ($year, $month, $day) = @_;
        my (@mlast) = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        my @targetLIST;

        my $counter = 0;
        my $dateString;

        if($day eq "1st"){
                for($i = 1; $i <= 15; $i++){
                        $dateString = sprintf("%04d%02d%02d", $year, $month, $i);
                        $targetLIST[$counter] = $dateString;
                        $counter++;
                }
        }
        elsif($day eq "2nd"){
                if( ($month == 2) &&
                        ( (($year % 4 == 0) && ($year % 100 != 0)) || ($year % 400 == 0) ) ){
                        $mlast[1]++;
                }

                for($i = 16; $i <= $mlast[$month - 1]; $i++){
                        $dateString = sprintf("%04d%02d%02d", $year, $month, $i);
                        $targetLIST[$counter] = $dateString;
                        $counter++;
                }
        }
        else{
        }

        return @targetLIST;
}

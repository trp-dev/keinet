#!/bin/bash

interval=10
CATALINA_HOME=/usr/local/tomcat
pidfile=$CATALINA_HOME/temp/tomcat.pid

#模試判定のwar展開
sh /home/kei-navi/deploy/ideploy.sh
#Kei-Naviのwar展開
sh /home/kei-navi/deploy/kdeploy.sh

#tomcatプロセスIDの取得
org=`ps ax|grep catalina|grep java|grep $CATALINA_HOME/|awk '{print $1}'`
if [ x"$org" = x ]; then
   echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "tomcat already shutdown."
   exit 0
fi

#tomcatシャットダウンの実行
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "tomcat shutdown start. pid=$org"
$CATALINA_HOME/bin/shutdown.sh
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "waiting for $interval sec(s) for tomcat shutdown process."
sleep $interval


#tomcatのプロセス確認
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "check tomcat process."
pid=`ps ax|grep catalina|grep java|grep $CATALINA_HOME/|awk '{print $1}'`
#プロセスが残っている場合
if [ x"$pid" != x ]; then
    #killコマンド(デフォルト)で終了
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "tomcat is still working. Now send it SIGTERM. pid=$pid"
    kill -TERM $pid
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "waiting for $interval sec(s) for tomcat shutdown process."
    sleep $interval

    #tomcatのプロセス再確認
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "retry check tomcat process."
    pid=`ps ax|grep catalina|grep java|grep $CATALINA_HOME/|awk '{print $1}'`
    #プロセスが残っている場合
    if [ x"$pid" != x ]; then
        echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "tomcat is still working. Now send it SIGKILL. pid=$pid"
        kill -KILL $pid
        echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "waiting for $interval sec(s) for tomcat shutdown process. pid=$pid"
        sleep $interval
    fi
fi


#tomcatのプロセスを再確認
pid=`ps ax|grep catalina|grep java|grep $CATALINA_HOME/|awk '{print $1}'`
#プロセスが残っている場合
if [ x"$pid" != x ]; then
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "tomcat shutdown failed."
    echo "exitcode:1"
else
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "tomcat shutdown success. pid=$org"
    echo "exitcode:0"
fi

#tomcatの起動
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "tomcat startup start."
$CATALINA_HOME/bin/startup.sh
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "waiting for $interval sec(s) for tomcat startup process."
sleep $interval

#tomcatのプロセスを再確認
pid=`ps ax|grep catalina|grep java|grep $CATALINA_HOME/|awk '{print $1}'`
#プロセスが残っている場合
if [ x"$pid" != x ]; then
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "tomcat startup success. pid=$pid"
    echo "exitcode:0"
    exit 1
else
    echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "tomcat startup failed."
    echo "exitcode:1"
    exit 0
fi

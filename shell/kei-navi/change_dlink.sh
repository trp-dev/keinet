#!/bin/bash

year=$(($(date "+%Y")-1))
from=$1
to=$2
ptn="\([0-9]\{4\}\)\/\([0-9]\{1,2\}\)\/\([0-9]\{1,2\}\) \([0-9]\{1,2\}\):\([0-9]\{1,2\}\)"


# 移動
cd /keinavi/ftp/freemenu/data/conf


# バックアップ
cp -a dlink.dat dlink.dat.$year


# 変更前を表示
echo "******************************  before  ******************************"
more dlink.dat
echo "******************************  before  ******************************"

echo ""
echo ""


# 日付を置換
sed -i -e "s@UseStartDate=$ptn@UseStartDate=$from@g" -e "s@UseEndDate=$ptn@UseEndDate=$to@g" dlink.dat


# 変更後を表示
echo "******************************  after   ******************************"
more dlink.dat
echo "******************************  after   ******************************"

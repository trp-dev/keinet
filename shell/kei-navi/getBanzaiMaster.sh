#!/bin/sh

#パラメータチェック
if [ "$1" = "" ]; then
    echo "年度を指定してください。(yyyy)"
	read year
else
	year=$1
fi
expr "$year" + 1 >&/dev/null
if [ $? -lt 2 ]; then
    echo "" > /dev/null
else
    echo "パラメータ１：年度(yyyy)が不正です。【$year】"
    exit 1
fi

if [ "$2" = "" ]; then
	echo "作業日を指定してください。(yyyyMMdd)"
	read ymd
else
	ymd=$2
fi
date -d $ymd >/dev/null 2>&1
expr "$ymd" + 1 >&/dev/null
if [ $? -eq 1 ]; then
    echo "パラメータ２：作業日(yyyyMMdd)が不正です。【$ymd】"
    exit 1
fi

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "インターネットバンザイ:マスタ取得 start."
echo ""
echo ""

#変数定義
base=/home/kei-navi/banzai
now=`date "+%Y%m%d"`
dst=$base/data

echo "******************************* 確認してください *******************************"
echo ""
echo " 年度  ： "$year
echo " 作業日： "$ymd
echo ""
echo " step1 ： ファイル取得 -> [${dst}]"
echo ""
echo " step2 ： ファイル圧縮 -> [${base}/${now}_data.tar.gz]"
echo ""
echo "******************************* 確認してください *******************************"
echo ""
echo ""

read -p "処理を実行します。 (y/N): " yn
echo ""
if [ ! "$yn" = "y" -a ! "$yn" = "Y" ]; then
	echo "中断しました。"
	echo ""
	echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "インターネットバンザイ:マスタ取得 end."
	exit 1
fi
cd $base

#step1
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "step1 start."

#ディレクトリを作成
if [ ! -d $dst ]; then
	mkdir -pv $dst
else
	rm -fv $dst/*
fi
echo ""

#ファイルを取得
cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/EXAMSCHEDULEDETAIL ${dst}/EXAMSCHEDULEDETAIL
cp -av /keinavi/help_user/MasterMainte/Get/EXAMSUBJECT.csv ${dst}/EXAMSUBJECT.csv
cp -av /keinavi/HULFT/NewEXAM/jcwq1050.txt$ymd* ${dst}/jcwq1050.txt
cp -av /keinavi/HULFT/NewEXAM/PC_MOSHI_TMP/${year}07/JH01_KAMOKU1 ${dst}/JH01_KAMOKU1
cp -av /keinavi/HULFT/NewEXAM/PC_MOSHI_TMP/${year}07/JH01_KAMOKU2 ${dst}/JH01_KAMOKU2
cp -av /keinavi/HULFT/NewEXAM/PC_MOSHI_TMP/${year}07/JH01_KAMOKU3 ${dst}/JH01_KAMOKU3
cp -av /keinavi/HULFT/NewEXAM/PC_MOSHI_TMP/${year}07/JH01_KAMOKU4 ${dst}/JH01_KAMOKU4
cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/PUB_UNIVMASTER_CHOICESCHOOL ${dst}/PUB_UNIVMASTER_CHOICESCHOOL
cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/PUB_UNIVMASTER_COURSE ${dst}/PUB_UNIVMASTER_COURSE
cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/PUB_UNIVMASTER_SELECTGPCOURSE ${dst}/PUB_UNIVMASTER_SELECTGPCOURSE
cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/PUB_UNIVMASTER_SELECTGROUP ${dst}/PUB_UNIVMASTER_SELECTGROUP
cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/PUB_UNIVMASTER_SUBJECT ${dst}/PUB_UNIVMASTER_SUBJECT
cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/UNIVMASTER_BASIC ${dst}/UNIVMASTER_BASIC
cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/UNIVMASTER_CHOICESCHOOL ${dst}/UNIVMASTER_CHOICESCHOOL
cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/UNIVMASTER_COURSE ${dst}/UNIVMASTER_COURSE
cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/UNIVMASTER_INFO ${dst}/UNIVMASTER_INFO
cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/UNIVMASTER_SUBJECT ${dst}/UNIVMASTER_SUBJECT
cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/UNIVMASTER_SELECTGPCOURSE ${dst}/UNIVMASTER_SELECTGPCOURSE
cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/UNIVMASTER_SELECTGROUP ${dst}/UNIVMASTER_SELECTGROUP
cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/UNIVSTEMMA ${dst}/UNIVSTEMMA

#UNIV_PASS_FAIL_COUNTが存在しない場合、HULFT集信で取得する
if [ ! -f /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/UNIV_PASS_FAIL_COUNT ]; then
 	/usr/local/HULFT/bin/utlrecv -f RZRG5390 -h k03308f6gp -sync -w 300
	cp -av /keinavi/HULFT/NewEXAM/UNIV_PASS_FAIL_COUNT* ${dst}/UNIV_PASS_FAIL_COUNT
	rm -fv /keinavi/HULFT/NewEXAM/UNIV_PASS_FAIL_COUNT*
else
	cp -av /keinavi/HULFT/NewEXAM/${year}38_${ymd}_4/UNIV_PASS_FAIL_COUNT ${dst}/UNIV_PASS_FAIL_COUNT
fi
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "step1 end."
echo ""

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "step2 start."
cd $dst
tar zcfv ${base}/${now}_data.tar.gz ./
echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "step2 end."
echo ""

echo `date "+%Y/%m/%d %H:%M:%S.%3N"` "インターネットバンザイ:マスタ取得 end."
exit 0

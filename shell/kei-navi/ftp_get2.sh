#!/bin/sh


# パラメータ構成
# 第1：取得先IPアドレス(ホスト名)
# 第2：受信元サーバの受信ファイル配置フォルダのフルパス
# 第3：受信先ーバの受信ファイル配置フォルダのフルパス
# 第4：受信ファイル名
# 第5：ヘッダファイル名

if [ $# -lt 5 ]
then
        echo "parameter error"
        exit
fi

#echo "$1 $2 $3 $4 $5"

#１日前の日付を取得
yesterday=`date +%Y%m%d`
#echo "$yesterday"
putFILE=${4}_${1}_${yesterday}.log.tmp
makeFILE=${4}_${1}_${yesterday}.log

#
# FTPにてファイルを取得
#
ftp -n $1 << EOF
user kei-navi 39ra8ma
bin
cd $2
lcd $3
get ${4} $putFILE
quit
EOF

#
# ヘッダを追記
#
#cd $3
#cat $5 $putFILE > $makeFILE
#rm $putFILE


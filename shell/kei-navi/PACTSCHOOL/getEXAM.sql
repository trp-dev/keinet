set linesize 1000
set pagesize 0
set trimspool on
set colsep ','

spool /home/kei-navi/PACTSCHOOL/exam_raw
select *
from
 examination
where
 TO_NUMBER(examyear) >= '2011'
order by examyear, examcd;
spool off

exit


set linesize 1000
set pagesize 0
set trimspool on
set colsep ','

spool /home/kei-navi/PACTSCHOOL/qrecord_a_raw
select *
from
 qrecord_a
where
 TO_NUMBER(examyear) >= '2011'
order by examyear, examcd, subcd, question_no;
spool off

exit


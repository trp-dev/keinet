set linesize 1000
set pagesize 0
set trimspool on
set colsep ','

spool /home/kei-navi/PACTSCHOOL/NoPactschool.txt

select
 mng.schoolcd,
 mng.loginid,
 mng.loginname,
 mng.loginpwd,
 mng.defaultpwd
from
 pactschool pa,
 loginid_manage mng
where
 pa.pactdiv = '2' and
 pa.schoolcd = mng.schoolcd and
 mng.loginname = 'システム管理者'

spool off

-- spool /home/kei-navi/PACTSCHOOL/login_manage.txt
-- select schoolcd, loginid from loginid_manage order by schoolcd, loginid;
-- spool off

exit


set linesize 1000
set pagesize 0
set trimspool on
set colsep ','

spool /home/kei-navi/PACTSCHOOL/qrecord_s_raw
select *
from
 qrecord_s
where
 TO_NUMBER(examyear) >= '2011'
order by examyear, examcd, bundlecd, subcd, question_no;
spool off

exit


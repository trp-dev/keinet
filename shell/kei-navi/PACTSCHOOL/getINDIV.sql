set linesize 1000
set pagesize 0
set trimspool on
set colsep ','

spool /home/kei-navi/PACTSCHOOL/studentinfo_raw
select
 EXAMYEAR      ,
 EXAMCD        ,
 ANSWERSHEET_NO,
 CUSTOMER_NO   ,
 APPLIDATE     ,
 BUNDLECD      ,
 GRADE         ,
 CLASS         ,
 CLASS_NO      ,
 NAME_KANA     ,
 SEX           ,
 TEL_NO        ,
 BIRTHDAY      ,
 S_INDIVIDUALID
from
 indiv_change_info
where
 TO_NUMBER(examyear) >= '2012' and
 TO_NUMBER(examyear) >= (TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, -3),'YYYY')) - 2) and
 examcd in ('05','06','07','10','65','75','61','62','63','71','72','73','74') and
 createdate >= trunc(sysdate-1,'DD') and
 createdate < trunc(sysdate,'DD');
spool off

exit


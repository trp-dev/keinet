set linesize 1000
set pagesize 0
set trimspool on
set colsep ','

spool /home/kei-navi/PACTSCHOOL/pactschool.txt
select userid, schoolcd, pactdiv from pactschool order by schoolcd;
spool off

-- spool /home/kei-navi/PACTSCHOOL/login_manage.txt
-- select schoolcd, loginid from loginid_manage order by schoolcd, loginid;
-- spool off

exit


#!/usr/bin/perl
################################################
# 
#

$ORG_FILE = "/home/kei-navi/PACTSCHOOL/studentinfo_raw.lst";
#$OUT_FILE = "/home/kei-navi/PACTSCHOOL/studentinfo.csv";
$OUT_FILE = $ARGV[0];

if(! open(IN, "$ORG_FILE")){
	print "FILE OPEN ERROR!\n";
	exit;
}
if(! open(OUT, ">$OUT_FILE") ){
        close(IN);
        exit;
}

# ヘッダ出力
print OUT "年度,模試コード,解答用紙番号,顧客番号,申込区分,一括校コード,学年,クラス,クラス番号,カナ氏名,性別,電話番号,生年月日,個人ID\n";

while(<IN>){
	chomp($_);
	@splitstr = split(/,/, $_, 14);
	
	for($ii=0; $ii<$#splitstr; $ii++){
		$splitstr[$ii] =~ s/^ *//;
		$splitstr[$ii] =~ s/ *$//;
	}
	
	# ファイル出力
	if($splitstr[13] ne ""){
		print OUT "$splitstr[0],$splitstr[1],$splitstr[2],$splitstr[3],$splitstr[4],$splitstr[5],$splitstr[6],$splitstr[7],$splitstr[8],$splitstr[9],$splitstr[10],$splitstr[11],$splitstr[12],$splitstr[13]\n";
	}
}
close(OUT);
close(IN);

print "FINISH!\n";
exit;


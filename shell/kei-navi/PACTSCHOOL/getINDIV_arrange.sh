#!/bin/sh

# 出力ファイル名
export OUT_FILE1=/home/kei-navi/PACTSCHOOL/studentinfo.csv.tmp
export OUT_FILE2=/home/kei-navi/PACTSCHOOL/studentinfo.csv

# ファイル出力
/home/kei-navi/PACTSCHOOL/getINDIV_arrange.pl $OUT_FILE1 > /dev/null 2>&1

# 文字コード変換
iconv -f UTF-8 -t EUCJP-WIN $OUT_FILE1 -o $OUT_FILE2

if [ $? -ne 0 ]
then
        echo 文字コード変換処理に失敗しました。
        exit 1
fi

exit

#!/usr/bin/perl
################################################
# 
#

$ORG_FILE = "/home/kei-navi/PACTSCHOOL/exam_raw.lst";
$OUT_FILE = "/home/kei-navi/PACTSCHOOL/exam.csv";

if(! open(IN, "$ORG_FILE")){
	print "FILE OPEN ERROR!\n";
	exit;
}
if(! open(OUT, ">$OUT_FILE") ){
        close(IN);
        exit;
}

# ヘッダ出力
print OUT "年度,\n";

while(<IN>){
	chomp($_);
	@splitstr = split(/,/, $_, 15);
	
	for($ii=0; $ii<$#splitstr+1; $ii++){
		$splitstr[$ii] =~ s/^ *//;
		$splitstr[$ii] =~ s/ *$//;
	}
	
	# ファイル出力
	if($splitstr[13] ne ""){
		print OUT "$splitstr[0],$splitstr[1],$splitstr[2],$splitstr[3],$splitstr[4],$splitstr[5],$splitstr[6],$splitstr[7],$splitstr[8],$splitstr[9],$splitstr[10],$splitstr[11],$splitstr[12],$splitstr[13],$splitstr[14]\n";
	}
}
close(OUT);
close(IN);

print "FINISH!\n";
exit;


#!/usr/bin/perl
################################################
# 
#

$ORG_FILE = "/home/kei-navi/PACTSCHOOL/qrecord_a_raw.lst";
$OUT_FILE = "/home/kei-navi/PACTSCHOOL/qrecord_a.csv";

if(! open(IN, "$ORG_FILE")){
	print "FILE OPEN ERROR!\n";
	exit;
}
if(! open(OUT, ">$OUT_FILE") ){
        close(IN);
        exit;
}

# ヘッダ出力
print OUT "年度,\n";

while(<IN>){
	chomp($_);
	@splitstr = split(/,/, $_, 7);
	
	for($ii=0; $ii<$#splitstr+1; $ii++){
		$splitstr[$ii] =~ s/^ *//;
		$splitstr[$ii] =~ s/ *$//;
	}
	
	# ファイル出力
	if($splitstr[0] ne ""){
		print OUT "$splitstr[0],$splitstr[1],$splitstr[2],$splitstr[3],$splitstr[4],$splitstr[5],$splitstr[6]\n";
	}
}
close(OUT);
close(IN);

print "FINISH!\n";
exit;


#!/bin/sh

rsync -avz --delete /keinavi/freemenu/exam/ satu02:/keinavi/freemenu/exam/
rsync -avz --delete /keinavi/freemenu/exam/ satu03:/keinavi/freemenu/exam/
rsync -avz --delete /keinavi/freemenu/exam/ satu02test:/keinavi/freemenu/exam/

rsync -avz --delete /keinavi/freemenu/ability/ satu02:/keinavi/freemenu/ability/
rsync -avz --delete /keinavi/freemenu/ability/ satu03:/keinavi/freemenu/ability/
rsync -avz --delete /keinavi/freemenu/ability/ satu02test:/keinavi/freemenu/ability/

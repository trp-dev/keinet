#!/bin/bash

base=/usr/local/tomcat/webapps2/keinavi
host=satu01test

# keinet.jspをsatu01testからコピー
rsync -avz $host:$base/jsp/profile/keinet.jsp $base/jsp/profile/keinet.jsp.tyousa

# f004.jspをsatu01testからコピー
rsync -avz $host:$base/jsp/freemenu/f004.jsp $base/jsp/freemenu/f004.jsp.tyousa

# f006.jspをsatu01testからコピー
rsync -avz $host:$base/jsp/freemenu/f006.jsp $base/jsp/freemenu/f006.jsp.tyousa

# f010.jspをsatu01testからコピー
rsync -avz $host:$base/jsp/freemenu/f010.jsp $base/jsp/freemenu/f010.jsp.tyousa

# kojinzyouhou.htmlをsatu01testからコピー
rsync -avz $host:$base/kojinzyouhou.html $base/kojinzyouhou.html.tyousa


# keinet.jspを入試調査結果向けに差し替え
cd /usr/local/tomcat/webapps2/keinavi/jsp/profile/
cp -pv keinet.jsp.tyousa keinet.jsp
touch keinet.jsp
pwd
ls -l keinet*

# f004.jspを入試調査結果向けに差し替え
cd /usr/local/tomcat/webapps2/keinavi/jsp/freemenu
cp -pv f004.jsp.tyousa f004.jsp
touch f004.jsp
pwd
ls -l f004*

# f006.jspを差し替え
cp -pv f006.jsp.tyousa f006.jsp
touch f006.jsp
pwd
ls -l f006*

# f010.jspを差し替え
cp -pv f010.jsp.tyousa f010.jsp
touch f010.jsp
pwd
ls -l f010*

# kojinzyouhou.htmlを差し替え
cd /usr/local/tomcat/webapps2/keinavi/
cp -pv kojinzyouhou.html.tyousa kojinzyouhou.html
pwd
ls -l kojinzyouhou.html*


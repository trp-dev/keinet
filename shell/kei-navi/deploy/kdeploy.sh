#!/bin/bash

cd /home/kei-navi/deploy/

#ファイルが置いてない場合、log4j.xmlとcommon.propertiesを現在稼働中のファイルをそのまま戻す
if [ ! -e "inet/log4j.xml" ]; then
        \cp -fv /usr/local/tomcat/webapps2/keinavi/WEB-INF/config/log4j.xml inet/log4j.xml
fi
if [ ! -e "inet/common.properties" ]; then
        \cp -fv /usr/local/tomcat/webapps2/keinavi/WEB-INF/classes/jp/co/fj/keinavi/resources/common.properties inet/common.properties
fi

if [ -e "inet.war" ]; then
        echo unzip keinavi.war start
        unzip -o -d /usr/local/tomcat/webapps2/keinavi/ keinavi.war
        \mv -fv keinavi.war keinavi.war.now
        echo unzip keinavi.war end

        echo copy log4j.xml start
        \cp -af keinavi/log4j.xml /usr/local/tomcat/webapps2/keinavi/WEB-INF/config/
        echo copy log4j.xml end

        echo copy common.properties start
        \cp -af keinavi/common.properties /usr/local/tomcat/webapps2/keinavi/WEB-INF/classes/jp/co/fj/keinavi/resources/
        echo copy common.properties end

        echo rsync satu02 start
        rsync -avz --delete /usr/local/tomcat/webapps2/keinavi/ kei-navi@satu02:/usr/local/tomcat/webapps2/keinavi/
        echo rsync satu02 end

        echo rsync satu03 start
        rsync -avz --delete /usr/local/tomcat/webapps2/keinavi/ kei-navi@satu03:/usr/local/tomcat/webapps2/keinavi/
        echo rsync satu03 end

        echo rsync satu02test start
        rsync -avz --delete /usr/local/tomcat/webapps2/keinavi/ kei-navi@satu02test:/usr/local/tomcat/webapps2/keinavi/
        echo rsync satu02test end
fi

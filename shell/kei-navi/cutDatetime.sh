#!/bin/bash

if [ ! -d "$1" ]; then
	echo "ディレクトリを指定してください。[$1]"
	exit 1
fi
target=$1
reject=$2
delopt=$3

array=($(ls -t $1 | egrep '*[0-9]{14}$'))
for f in ${array[@]}; do
	src=${target}${f}
	dst=${target}${f::-14}
	if [ -f "$dst" ]; then
		if [ "$delopt" == "1" ]; then
			rm -fv ${src}
		else
			echo "同名のファイルが存在します。[${src} -> ${dst}]"
		fi
	else
		flg="0"
		for r in ${reject}; do
			if [ "${target}${r}" = "${dst}" ]; then
				flg="1"
				break
			fi
		done
		if [ ${flg} = "0" ]; then 
			mv -fv ${src} ${dst}
		fi
	fi	
done

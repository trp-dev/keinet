#!/bin/bash

# 「Kei-Navi入試情報／お知らせ」画面
# ディレクトリ移動
cd /usr/local/tomcat/webapps2/keinavi/jsp/profile

# ファイルのコピー
cp -pv keinet.jsp.tyousa keinet.jsp

# 日付更新
touch keinet.jsp

pwd
ls -l keinet.jsp*

# 「高校別データダウンロードサブメニュー」
# ディレクトリ移動
cd /usr/local/tomcat/webapps2/keinavi/jsp/freemenu

# ファイルのコピー
cp -pv f004.jsp.normal f004.jsp

# 日付更新
touch f004.jsp

pwd
ls -l f004.jsp*

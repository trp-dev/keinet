package jp.co.fj.keinavi.beans.txt_out;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.KNPropertyCtrl;
import jp.fujitsu.keinet.mst.db.util.QueryLoader;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import com.fjh.db.DBManager;

class OutputExamSeiseki3Test {

    @Before
    public void before() {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
    }

    @Test
    void testExecute01() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        DBManager.create(KNCommonProperty.getNDBSID(), KNCommonProperty.getNDBDriver(), KNCommonProperty.getNDBURL(), KNCommonProperty.getNDBConnUserID(), KNCommonProperty.getNDBConnUserPass(), KNCommonProperty.getNDBPoolCountLimit(),
                Integer.parseInt(KNCommonProperty.getConnectionWaitTime()));
        QueryLoader.getInstance().setPath("D:/Projects/Kawai/keinet_project/workspace/keinavi/src/main/webapp/WEB-INF/sql");
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("03");
        exam.setExamTypeCD("01");
        exam.setExamDiv("05");
        LoginSession login = new LoginSession();
        login.setUserID("07151");
        ArrayList list = new ArrayList<String>();
        list.add("D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test");
        Profile profile = new Profile();
        Map map = new HashMap();
        Map map1402 = new HashMap();
        Map map1403 = new HashMap();
        Short i = 1;

        map1402.put("1402", i);
        map1403.put("1403", "1,2,3,4,5,6,7,8,9,10");
        map.put("0501", new String[] { "01", "02" });
        map.put("0506", "2021");
        map.put("050100", map1402);
        map.put("050303", map1403);
        profile.setCategoryMap(map);
        txt.setConnection(null, DBManager.getConnectionPool("ndb"));
        txt.setExam(exam);
        txt.setLogin(login);
        txt.setProfile(profile);
        txt.setOutfileList(list);

        txt.execute();

    }

    @Test
    void test2021_01() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("01");
        exam.setExamTypeCD("01");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster01_2020.properties", result);

    }

    @Test
    void test2021_02() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("05");
        exam.setExamTypeCD("02");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster02_2020.properties", result);

    }

    @Test
    void test2021_03() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("61");
        exam.setExamTypeCD("04");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster03_2020.properties", result);

    }

    @Test
    void test2021_04() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("41");
        exam.setExamTypeCD("15");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster04_2020.properties", result);

    }

    @Test
    void test2021_05() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("43");
        exam.setExamTypeCD("16");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster05_2020.properties", result);

    }

    @Test
    void test2021_06() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("12");
        exam.setExamTypeCD("10");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster06_2020.properties", result);

    }

    @Test
    void test2021_07() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("21");
        exam.setExamTypeCD("14");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster07_2020.properties", result);

    }

    @Test
    void test2021_08() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("22");
        exam.setExamTypeCD("13");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster08_2020.properties", result);

    }

    @Test
    void test2021_09() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("18");
        exam.setExamTypeCD("12");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster09_2020.properties", result);

    }

    @Test
    void test2021_10() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("15");
        exam.setExamTypeCD("11");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster10_2020.properties", result);

    }

    @Test
    void test2021_11() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("24");
        exam.setExamTypeCD("01");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster11_2020.properties", result);

    }

    @Test
    void test2021_12() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("25");
        exam.setExamTypeCD("02");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster12_2020.properties", result);

    }

    @Test
    void test2021_13() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("26");
        exam.setExamTypeCD("04");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster13_2020.properties", result);

    }

    @Test
    void test2021_14() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("27");
        exam.setExamTypeCD("15");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster14_2020.properties", result);

    }

    @Test
    void test2021_15() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("30");
        exam.setExamTypeCD("16");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster15_2020.properties", result);

    }

    @Test
    void test2021_16() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("29");
        exam.setExamTypeCD("10");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster16_2020.properties", result);

    }

    @Test
    void test2021_17() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("32");
        exam.setExamTypeCD("14");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster17_2020.properties", result);

    }

    @Test
    void test2020_18() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2020");
        exam.setExamCD("31");
        exam.setExamTypeCD("13");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster18_2020.properties", result);

    }

    @Test
    void test2021_18() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("31");
        exam.setExamTypeCD("13");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster18_2021.properties", result);

    }

    @Test
    void test2021_19() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("38");
        exam.setExamTypeCD("12");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster19_2020.properties", result);

    }

    @Test
    void test2021_20() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("28");
        exam.setExamTypeCD("11");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster20_2020.properties", result);

    }

    @Test
    void test2021_21() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("01");
        exam.setExamTypeCD("32");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster21_2020.properties", result);

    }

    @Test
    void test2021_22() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("75");
        exam.setExamTypeCD("02");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster22_2020.properties", result);

    }

    @Test
    void test2021_23() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("76");
        exam.setExamTypeCD("04");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster23_2020.properties", result);

    }

    @Test
    void test2021_24() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("77");
        exam.setExamTypeCD("15");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster24_2020.properties", result);

    }

    @Test
    void test2021_xx() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2021");
        exam.setExamCD("99");
        exam.setExamTypeCD("01");

        String result = null;
        try {
            result = txt.getPropertyFileName(exam);
        } catch (Throwable e) {
            result = e.getMessage();
            assertEquals("不正な模試コードです。99", result);
        }

    }

    @Test
    void test2018() throws Exception {
        KNPropertyCtrl.FilePath = "D:/Projects/Kawai/keinet_project/workspace/keinavi/src/test/java";
        OutputExamSeiseki3 txt = new OutputExamSeiseki3();
        ExamData exam = new ExamData();
        exam.setExamYear("2018");
        exam.setExamCD("01");
        exam.setExamTypeCD("01");

        String result = txt.getPropertyFileName(exam);
        assertEquals("v3_subjectmaster01.properties", result);

    }

}

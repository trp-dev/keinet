package jp.co.fj.freemenu.forms;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 *
 * 成績統計資料集ダウンロードのアクションフォーム
 * 
 * 2008.03.06	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class F002Form extends BaseForm {
	
	// データ種別
	private String dataType;
	
	// 地区コード
	private String distCd;
	
	// 国私区分
	private String divisionCd;
		
	// ダウンロード対象データ種別
	private String downDataType;
	
	// ダウンロード対象模試年度
	private String downExamYear;
	
	// ダウンロード対象模試コード
	private String downExamCd;
	
	// ダウンロード対象地区コード
	private String downDistCd;
	
	// ダウンロード対象科目コード
	private String downSubCd;
	
	// ダウンロード対象国私区分
	private String downDivCd;	
	
	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	/**
	 * @return distCd
	 */
	public String getDistCd() {
		return distCd;
	}

	/**
	 * @param pDistCd 設定する distCd
	 */
	public void setDistCd(String pDistCd) {
		distCd = pDistCd;
	}

	/**
	 * @return dataType
	 */
	public String getDataType() {
		return dataType;
	}
	
	/**
	 * @param pDataType 設定する dataType
	 */
	public void setDataType(String pDataType) {
		dataType = pDataType;
	}
	
	/**
	 * @return divisionCd
	 */
	public String getDivisionCd() {
		return divisionCd;
	}
	
	/**
	 * @param pDivisionCd 設定する divisionCd
	 */
	public void setDivisionCd(String pDivisionCd) {
		divisionCd = pDivisionCd;
	}

	/**
	 * @return downDataType
	 */
	public String getDownDataType() {
		return downDataType;
	}

	/**
	 * @param pDownDataType 設定する downDataType
	 */
	public void setDownDataType(String pDownDataType) {
		downDataType = pDownDataType;
	}

	/**
	 * @return downDistCd
	 */
	public String getDownDistCd() {
		return downDistCd;
	}

	/**
	 * @param pDownDistCd 設定する downDistCd
	 */
	public void setDownDistCd(String pDownDistCd) {
		downDistCd = pDownDistCd;
	}

	/**
	 * @return downDivCd
	 */
	public String getDownDivCd() {
		return downDivCd;
	}

	/**
	 * @param pDownDivCd 設定する downDivCd
	 */
	public void setDownDivCd(String pDownDivCd) {
		downDivCd = pDownDivCd;
	}

	/**
	 * @return downExamCd
	 */
	public String getDownExamCd() {
		return downExamCd;
	}

	/**
	 * @param pDownExamCd 設定する downExamCd
	 */
	public void setDownExamCd(String pDownExamCd) {
		downExamCd = pDownExamCd;
	}

	/**
	 * @return downExamYear
	 */
	public String getDownExamYear() {
		return downExamYear;
	}

	/**
	 * @param pDownExamYear 設定する downExamYear
	 */
	public void setDownExamYear(String pDownExamYear) {
		downExamYear = pDownExamYear;
	}

	/**
	 * @return downSubCd
	 */
	public String getDownSubCd() {
		return downSubCd;
	}

	/**
	 * @param pDownSubCd 設定する downSubCd
	 */
	public void setDownSubCd(String pDownSubCd) {
		downSubCd = pDownSubCd;
	}

}

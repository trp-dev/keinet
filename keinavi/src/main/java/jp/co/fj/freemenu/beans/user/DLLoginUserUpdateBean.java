package jp.co.fj.freemenu.beans.user;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.beans.user.AdminUserUpdateBean;
import jp.co.fj.keinavi.data.user.LoginUserData;
import jp.co.fj.keinavi.util.message.MessageLoader;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 管理者情報更新Bean　※DLサービス校用
 * 
 * 2008.12.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class DLLoginUserUpdateBean extends AdminUserUpdateBean {

	// 学校コード
	private final String schoolCd;
	// 利用者データ
	private final LoginUserData loginUserData;
	
	/**
	 * コンストラクタ
	 */
	public DLLoginUserUpdateBean(final String pSchoolCd,
			final LoginUserData pLoginUserData) {
		super(pSchoolCd, pLoginUserData);
		schoolCd = pSchoolCd;
		loginUserData = pLoginUserData;
	}
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// 同じIDが契約校マスタにあってはならない
		if (existsPactSchool()) {
			throw new KNBeanException(MessageLoader.getInstance().getMessage("u006a"));
		}
		
		// 利用者ID更新
		if (!loginUserData.getLoginId().equals(
				loginUserData.getOriginalLoginId())) {

			//「admin」は不可
			if ("admin".equals(loginUserData.getLoginId())) {
				throw new KNBeanException("利用者IDに「admin」は登録できません。");
			}

			updateLoginIdManage();
		}

		// 利用者名更新
		if (!loginUserData.isManager()) {
			updateLoginName();
		}
	}

	private void updateLoginName() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("u09"));
			
			// 利用者名
			ps.setString(1, loginUserData.getLoginName());
			// 学校コード
			ps.setString(2, schoolCd);
			// 利用者ID（変更後）
			ps.setString(3, loginUserData.getLoginId());
			
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

}

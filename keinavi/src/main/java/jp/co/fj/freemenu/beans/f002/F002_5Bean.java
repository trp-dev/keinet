package jp.co.fj.freemenu.beans.f002;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.freemenu.util.PathUtil;

/**
 *
 * 成績統計資料集ダウンロード用のBean
 * 志望大学・学部・学科別成績分布表
 * 
 * 2008.03.05	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class F002_5Bean extends F002Bean {

	// 専用地区リスト
	private List areaList;
	
	// ファイル存在フラグ保持マップ
	// key ... 地区コード
	// value ... Boolean
	private Map fileMap;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		fileMap = new HashMap();

		// F002_5専用の地区リストを作る
		createF002_5AreaList();
		
		// 模試コードまでのディレクトリ
		final File dir = getExamDir();
		
		for (Iterator ite = getAreaList().iterator(); ite.hasNext();) {
			final String[] area = (String[]) ite.next();
			final String distCd = area[0];
			final File file = new File(dir, createFileName("pdf", distCd));
			fileMap.put(distCd, Boolean.valueOf(file.exists()));
		}
		
		// 全国版は特別処理
		final String fileName;
		
		// 短大の場合
		if (isJunior()) {
			fileName = createFileName("pdf", "00");
		// 短大以外
		} else {
			fileName = createFileName("zip", "00pdf");
		}

		final File file = new File(dir, fileName);
		fileMap.put("00", Boolean.valueOf(file.exists()));
	}

	/**
	 * @see jp.co.fj.freemenu.beans.f002.F002Bean#getDownloadFile()
	 */
	public File getDownloadFile() {
		
		// 模試コードまでのディレクトリ
		final File dir = getExamDir();

		final String fileName;
		
		// 全国版
		if ("00".equals(getTargetDistCd())) {
			// 短大の場合
			if (isJunior()){
				fileName = createFileName("pdf", "00");
			// それ以外
			} else {
				fileName = createFileName("zip", "00pdf");	
			}
		// 各地区版
		} else {
			fileName = createFileName("pdf", getTargetDistCd());
		}
		
		return new File(dir, fileName);
	}
	
	// 短大を選択しているか
	private boolean isJunior() {
		
		return "05".equals(getTargetDivCd()) || "08".equals(
				getTargetDivCd()) || "03".equals(getTargetDivCd());
	}
	
	private String createFileName(final String type, final String distCd) {
		
		return PathUtil.makeFileName(
				type, "ure", getTargetExamCd() + getTargetDivCd() + distCd);
	}
	
	private void createF002_5AreaList() {
		
		areaList = new ArrayList(11);
		
		for (Iterator ite = getAreaListIterator(); ite.hasNext();) {

			final String[] data = (String[]) ite.next();
			
			// 海外は入れない
			if (!"12".equals(data[0])) {
				areaList.add(data);
			}
			
			// 関東地区の次に東京地区を入れる
			if ("03".equals(data[0])) {
				areaList.add(new String[]{"11", "東京地区"});
			}
		}
	}

	/**
	 * @return fileMap
	 */
	public Map getFileMap() {
		return fileMap;
	}

	/**
	 * @return areaList
	 */
	public List getAreaList() {
		return areaList;
	}

}

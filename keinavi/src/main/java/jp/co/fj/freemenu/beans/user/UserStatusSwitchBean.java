package jp.co.fj.freemenu.beans.user;

import java.sql.PreparedStatement;

import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 利用者状態切り替えBean
 * 
 * 2008.12.24	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class UserStatusSwitchBean extends DefaultBean {

	// 学校コード
	private final String schoolCd;
	// 利用者ID
	private final String loginId;
	// 状態
	private final String status;
	
	/**
	 * コンストラクタ
	 */
	public UserStatusSwitchBean(final String pSchoolCd,
			final String pLoginId, final String pStatus) {
		schoolCd = pSchoolCd;
		loginId = pLoginId;
		status = pStatus;
	}
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("u10"));
			
			// 状態
			ps.setString(1, status);
			// 学校コード
			ps.setString(2, schoolCd);
			// 利用者ID
			ps.setString(3, loginId);
			
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

}

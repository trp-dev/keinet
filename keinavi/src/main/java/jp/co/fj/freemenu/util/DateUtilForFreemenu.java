/*
 * 作成日: 2004/09/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.data.ExamData;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class DateUtilForFreemenu {

	/**
	 * 
	 * @param date
	 * @return
	 */
	public String changeSlashDate(String date) {
		
		StringBuffer buf = new StringBuffer();
		buf.append(date.substring(0,4));
		buf.append("/");
		buf.append(date.substring(4,6));
		buf.append("/");
		buf.append(date.substring(6,8));
		return buf.toString();
	}
	
	/**
	 * 
	 * @param ldate
	 * @return
	 */
	public List changeSlashDate(List ldate) {
		
		List lst = new ArrayList();
		Iterator it = ldate.iterator();
		String date = null;
		while (it.hasNext()) {
			ExamData examdata = (ExamData)it.next();
			date = examdata.getOutDataOpenDate();
			examdata.setOutDataOpenDate(changeSlashDate(date));
			lst.add(examdata);
		}
		return lst;
	}
	
}

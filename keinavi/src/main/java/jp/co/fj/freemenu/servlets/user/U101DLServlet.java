package jp.co.fj.freemenu.servlets.user;

import jp.co.fj.keinavi.servlets.user.U101Servlet;

/**
 *
 * 利用者情報確認画面サーブレット
 * ※DLサービス校用
 * 
 * 2008.12.24	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U101DLServlet extends U101Servlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.user.U101Servlet#getJspId()
	 */
	protected String getJspId() {
		return "u101_dl";
	}
	
}

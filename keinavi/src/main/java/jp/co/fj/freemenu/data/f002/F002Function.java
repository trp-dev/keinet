package jp.co.fj.freemenu.data.f002;

import java.io.File;

/**
 *
 * 成績統計資料集ダウンロードの機能を表すデータクラス
 * 
 * 2008.03.05	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class F002Function {

	// 機能ID
	private final String functionId;
	
	// 機能名
	private final String functionName;
	
	// データファイル格納ディレクトリ
	private final File dataFileDir;

	/**
	 * @param pFunctionId 機能ID
	 * @param pFunctionName 機能名
	 * @param pDataFileDir データファイル格納ディレクトリ
	 */
	public F002Function(final String pFunctionId,
			final String pFunctionName,
			final File pDataFileDir) {
		functionId = pFunctionId;
		functionName = pFunctionName;
		dataFileDir = pDataFileDir;
	}

	/**
	 * @return functionId
	 */
	public String getFunctionId() {
		return functionId;
	}

	/**
	 * @return functionName
	 */
	public String getFunctionName() {
		return functionName;
	}

	/**
	 * @return dataFileDir
	 */
	public File getDataFileDir() {
		return dataFileDir;
	}
	
}

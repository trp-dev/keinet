/*
 * 作成日: 2004/12/20
 */
package jp.co.fj.freemenu.data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 模試成績セッション
 * 画面に表示する模試・一括コードのセットを保持するクラス
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */
public class ExamScoreSession implements Serializable {

    public static final String SESSION_KEY = "ExamScoreSession";
    
    /** 年度リスト */
    private List years;
    /** データマップ */
    private Map examMap;
    
    /**
     * @return examMap を戻す。
     */
    public Map getExamMap() {
        return examMap;
    }
    /**
     * @param examMap examMap を設定する。
     */
    public void setExamMap(Map examMap) {
        this.examMap = examMap;
    }
    /**
     * @return years を戻す。
     */
    public List getYears() {
        return years;
    }
    /**
     * @param years years を設定する。
     */
    public void setYears(List years) {
        this.years = years;
    }
}

/*
 * 作成日: 2004/09/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.util;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class MailUtility {
	
	/**
	 * アップロード処理情報を付加して、メールを送信する
	 * @param uploadDate
	 * @param sName
	 * @param sUID
	 * @param sfilename
	 */
	public void sendEMail(String uploadDate, String sName, String sUID, String sfilename) {
		
		Session sessSmtp = null;
		Transport transport = null;
		try {
			Properties props = System.getProperties();
			props.put("mail.smtp.host","/*SMTPサーバ名*/");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "25");
			sessSmtp = Session.getDefaultInstance(props, null);
			transport = sessSmtp.getTransport("smtp");
			// ユーザ確認
			transport.connect("/*SMTPサーバ名*/","/*ユーザ名*/","/*パスワード*/");
			MimeMessage mimeMessage = new MimeMessage(sessSmtp);
			// 送信元メールアドレスと送信者名を指定
			mimeMessage.setFrom(new InternetAddress("xx@xx.co.jp","Kei-Naviテスト","iso-2022-jp"));
			// 送信先メールアドレスを指定
			mimeMessage.setRecipients(Message.RecipientType.TO,"xx@xxhogehoge.co.jp");
			// メールのタイトルを指定
			mimeMessage.setSubject("Hello World", "iso-2022-jp");
			// メールの内容を指定
			mimeMessage.setText(getBodyText(uploadDate, sName, sUID, sfilename),"iso-2022-jp");
			// メールの形式を指定
			mimeMessage.setHeader("Context-Type","text/plain");
			// 送信日を指定
			mimeMessage.setSentDate(new Date());
			// 送信実行
			Transport.send(mimeMessage);
		} catch (UnsupportedEncodingException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

	/**
	 * メール本文作成
	 * @param uploadDate
	 * @param sName
	 * @param sUID
	 * @param sfilename
	 * @return
	 */
	private String getBodyText(String uploadDate, String sName, String sUID, String sfilename) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}
	
}

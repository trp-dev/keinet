/**
 * 
 */
package jp.co.fj.freemenu.beans.f002;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.freemenu.beans.CourseSubjectBean;
import jp.co.fj.freemenu.util.PathUtil;
import jp.co.fj.freemenu.util.StringUtil;

/**
 * 
 * 成績統計資料集ダウンロード用のBean
 * 受験型別・科目別得点分布表
 * 
 * 2008.04.15	[新規作成]
 * 
 * @author fujito-urakawa
 * @version 1.0
 *
 */
public class F002_8Bean extends F002Bean {

	// 既存モジュール：型・科目情報Bean
	private CourseSubjectBean bean;
	
	// ファイル存在フラグ保持マップ
	// key ... 科目コード
	// value ... Boolean
	private Map fileMap;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		final StringUtil util = new StringUtil();
		// ファイルから科目コードを取得する[科目]
		final ArrayList lsub = util.getSubjectListFromPdf(getExamDir().getPath(), 
											"TOKUTENBUNPU", getTargetExamCd(), "pdf");
		// ファイルから科目コードを取得する[型]
		final ArrayList lpat = util.getPatternListFromPdf(getExamDir().getPath(), 
											"TOKUTENBUNPU", getTargetExamCd(), "pdf");
		
		bean = new CourseSubjectBean();
		bean.setConnection(null, conn);
		bean.setYear(getTargetExamYear());
		bean.setExamcd(getTargetExamCd());
		// ファイルが存在する科目コード（科目）
		bean.setTmpSubList(lsub);
		// ファイルが存在する科目コード（型）
		bean.setTmpPatList(lpat);
		bean.execute2();

		fileMap = new HashMap();
		
		// 模試コードまでのディレクトリ
		final File dir = getExamDir();
		
		// 型をチェック
		createFileMap(dir, getTypeList().iterator());
		
		// 科目をチェック
		createFileMap(dir, getSubjectList().iterator());
		
		// 全科目は特別処理
		final File file = new File(dir, createFileName("zip", "0000pdf"));
		fileMap.put("0000", Boolean.valueOf(file.exists()));
	}
	
	/**
	 * @see jp.co.fj.freemenu.beans.f002.F002Bean#getDownloadFile()
	 */
	public File getDownloadFile() {
		
		// 模試コードまでのディレクトリ
		final File dir = getExamDir();

		final String fileName;
		if ("0000".equals(getTargetSubCd())) {
			fileName = createFileName("zip", "0000pdf");
		} else {
			fileName = createFileName("pdf", getTargetSubCd());
		}
		
		return new File(dir, fileName);
		
	}
	
	private void createFileMap(final File dir, final Iterator ite) {
		
		for (;ite.hasNext();) {
			final String[] data = (String[]) ite.next();
			final String subCd = data[0];
			final File file = new File(dir, createFileName("pdf", subCd));
			fileMap.put(subCd, Boolean.valueOf(file.exists()));
		}
	}
	
	protected String createFileName(final String type, final String subCd) {

		return PathUtil.makeFileName(type, "TOKUTENBUNPU",
				getTargetExamCd() + subCd);
	}
	
	/**
	 * @return fileMap
	 */
	public Map getFileMap() {
		return fileMap;
	}

	/**
	 * @return 型情報リスト
	 */
	public List getTypeList() {
		return bean.getLp();
	}

	/**
	 * @return 科目情報リスト
	 */
	public List getSubjectList() {
		return bean.getLcs();
	}
	
	/**
	 * @see jp.co.fj.freemenu.beans.f002.F002Bean#needConnection()
	 */
	public boolean needConnection() {
		return true;
	}

}

package jp.co.fj.freemenu.servlets;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.freemenu.beans.AreaBean;
import jp.co.fj.freemenu.beans.f002.F002Bean;
import jp.co.fj.freemenu.beans.f002.F002BeanFactory;
import jp.co.fj.freemenu.beans.f002.F002ExamBean;
import jp.co.fj.freemenu.beans.f002.F002ExamFunctionBean;
import jp.co.fj.freemenu.beans.f002.F002FunctionListBean;
import jp.co.fj.freemenu.data.f002.F002ExamData;
import jp.co.fj.freemenu.forms.F002Form;
import jp.co.fj.freemenu.util.DownloadUtil;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.message.MessageLoader;

/**
 *
 * 成績統計資料集ダウンロードのサーブレット
 * 
 * 2008.03.06	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class F002Servlet extends DefaultHttpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(final HttpServletRequest request, 
			final HttpServletResponse response)
			throws ServletException, IOException {

		// JSPへの遷移
		if ("f002".equals(getForward(request))) {
			executeJsp(request, response);
		
		// ファイルダウンロード
		} else if ("f002_download".equals(getForward(request))) {
			executeDownload(request, response);
				
		// 他のサーブレットへの転送
		} else {
			
			// 成績統計資料集ダウンロードから抜けるならセッションの掃除
			if (!getForward(request).startsWith("f002")) {
				final HttpSession session = request.getSession(false);
				session.removeAttribute("ExamList");
				session.removeAttribute("AreaList");
				session.removeAttribute("FromTopMenu");
			}
			
			dispatch(request, response);
		}
	}
	
	private void executeJsp(final HttpServletRequest request, 
			final HttpServletResponse response)
			throws ServletException, IOException {
		
		// HTTPセッション
		final HttpSession session = request.getSession(false);

		// アクションフォーム
		final F002Form form = (F002Form) getActionForm(
				request, "jp.co.fj.freemenu.forms.F002Form");

		// 初回アクセス
		if (session.getAttribute("ExamList") == null) {
			initSession(request, session, form);
		}
		
		// F002Bean
		if (form.getDataType() != null) {
			request.setAttribute("F002Bean",
					createF002Bean(request, form));
		}
		
		request.setAttribute("form", form);
		// 「表の見方」URLをセット
		request.setAttribute("hyo_ref", KNCommonProperty.getStringValue("DLRefURL"));
		
		forward2Jsp(request, response);
	}
	
	private void executeDownload(final HttpServletRequest request, 
			final HttpServletResponse response)
			throws ServletException, IOException {

		// アクションフォーム
		final F002Form form = (F002Form) getActionForm(
				request, "jp.co.fj.freemenu.forms.F002Form");
		
		// ダウンロード対象ファイル
		final File file = getDownloadFile(form);
		
		// ダウンロード実行
		DownloadUtil.downFile(response, file.getAbsolutePath(), file.getName());

		// アクセスログ
		actionLog(request, "421",
				form.getDownExamYear() + "," + form.getDownExamCd() + "," + file.getName());
	}
	
	private File getDownloadFile(final F002Form form) {
		
		final String functionId = form.getDownDataType();

		final F002Bean bean = F002BeanFactory.create(functionId);
		bean.setFunction(getF002FunctionListBean().getFunction(functionId));
		bean.setTargetExamYear(form.getDownExamYear());
		bean.setTargetExamCd(form.getDownExamCd());
		bean.setTargetDistCd(form.getDownDistCd());
		bean.setTargetDivCd(form.getDownDivCd());
		bean.setTargetSubCd(form.getDownSubCd());
		
		return bean.getDownloadFile();
	}

	private F002Bean createF002Bean(final HttpServletRequest request,
			final F002Form form) throws ServletException {
		
		final String functionId = form.getDataType();
		final F002Bean bean = F002BeanFactory.create(functionId);
		Connection con = null;
		try {
			// DBコネクションを使うかどうかはBeanによる
			if (bean.needConnection()) {
				con = getConnectionPool(request);
				bean.setConnection(null, con);
			}
			bean.setFunction(getF002FunctionListBean().getFunction(functionId));
			bean.setTargetExamYear(form.getTargetYear());
			bean.setTargetExamCd(form.getTargetExam());
			bean.setTargetDistCd(form.getDistCd());
			bean.setTargetDivCd(form.getDivisionCd());
			bean.setAreaList(getAreaList(request));
			bean.execute();
			return bean;
		} catch (final Exception e) {
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
	}

	// 初回アクセス時の処理
	private void initSession(final HttpServletRequest request, final HttpSession session,
			final F002Form form) throws ServletException {

		// 機能リスト
		final List functionList = getFunctionList();
		
		// 取得開始年度
		final String fromYear = getFromYear();

		// 模試リスト生成
		final F002ExamBean examBean = createExamBean(request, functionList, fromYear);
		session.setAttribute("ExamList", examBean.getExamList());
		
		// 地区リスト生成
		final AreaBean areaBean = createAreaBean(request);
		session.setAttribute("AreaList", areaBean.getAreaData());
		
		// 対象年度・対象模試の初期値
		final F002ExamData data = examBean.getLatestExamData();
		if (data != null) {
			form.setTargetYear(data.getExamYear());
			form.setTargetExam(data.getExamCD());
		}
		
		// 地区コードの初期値
		form.setDistCd(areaBean.getSchoolID());

		// 戻り先を覚えておく
		if ("w002".equals(getBackward(request)) || "w008".equals(getBackward(request))) {
			session.setAttribute("FromTopMenu", getBackward(request));
		}
	}
	
	// 機能リストを取得する
	private List getFunctionList() {
		
		final F002FunctionListBean bean = getF002FunctionListBean();
		return bean.getFunctionList();
	}
	
	private F002FunctionListBean getF002FunctionListBean() {

		return (F002FunctionListBean) getServletContext(
				).getAttribute("F002FunctionListBean");
	}

	// 取得開始年度
	private String getFromYear() {

		// 現年度 - 2
		return (Integer.parseInt(KNUtil.getCurrentYear()) - 2) + "";
	}
	
	private F002ExamBean createExamBean(final HttpServletRequest request,
			final List functionList, final String fromYear) throws ServletException {
		
		// 模試機能マップを作る
		final Map examFunctionMap = createExamFunctionMap(functionList, fromYear);
		
		// ログイン情報
		final LoginSession login = getLoginSession(request);
		
		// Beanインスタンス
		final F002ExamBean bean = new F002ExamBean();
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			bean.setConnection(null, con);
			bean.setFromYear(fromYear);
			// ログインしていない場合も考慮
			if (login != null) {
				bean.setHelpdesk(login.isHelpDesk());
			}
			bean.setExamFunctionMap(examFunctionMap);
			bean.execute();
		} catch (final Exception e) {
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
		
		return bean;
	}

	private Map createExamFunctionMap(final List functionList,
			final String fromYear) throws ServletException {
		
		try {
			final F002ExamFunctionBean bean = new F002ExamFunctionBean();
			bean.setFunctionList(functionList);
			bean.setFromYear(fromYear);
			bean.execute();
			return bean.getExamFunctionMap();
		} catch (final Exception e) {
			throw createServletException(e);
		}
	}

	private AreaBean createAreaBean(
			final HttpServletRequest request) throws ServletException {
		
		// ログイン情報
		final LoginSession login = getLoginSession(request);
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			final AreaBean bean = new AreaBean();
			bean.setConnection(null, con);
			// ログインしていない場合も考慮
			if (login != null) {
				bean.setSchoolID(login.getUserID());
			}
			bean.execute();
			return bean;
		} catch (final Exception e) {
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
	}

	private List getAreaList(final HttpServletRequest request) {
		
		return (List) request.getSession(false).getAttribute("AreaList");
	}

	/**
	 * @throws KNServletException ログインチェック例外
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#isLogin(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	protected boolean isLogin(final HttpServletRequest request) throws Exception {
		
		HttpSession session = request.getSession(true);
		
		// 直接アクセス限定のダミーセッションキー
		final String sessionKey = "F002Servlet.LoginSession";
		
		// 通常セッション情報あり
		if (session.getAttribute(LoginSession.SESSION_KEY) != null) {
			return super.isLogin(request);
			
		// 直接アクセス限定のダミーあり
		} else if (session.getAttribute(sessionKey) != null) {
			return true;
			
		// セッション情報が無いのでチェック処理
		} else {
			if (!isAllow(request)) {
				KNServletException e = new KNServletException(
						"IP・REFERER認証エラー: "
						+ "(" + getRemoteAddr(request) + ")"
						+ "(" + request.getHeader("REFERER") + ")");
				e.setErrorMessage(MessageLoader.getInstance().getMessage("w033a"));
				e.setErrorCode("1");
				e.setClose(true);
				throw e;
			} else {
				session.setAttribute(sessionKey, Boolean.TRUE);
				return true;
			}
		}
		
	}
	
	/**
	 * ログインなしでの遷移を許容するかをチェックする
	 * @return true：許容する false：許容しない
	 */
	private boolean isAllow(final HttpServletRequest request) {
		
		// IPチェック
		if (isAllowIP(request)) return true;
		// リファラチェック
		if (isAllowReferer(request)) return true;
		// 上記以外はfalse
		return false;
	}
	
	/**
	 * ログインなしを許容するIPからの接続か
	 * @return
	 */
	private boolean isAllowIP(final HttpServletRequest request){
		
		// requestを送ったIPを取得
		final String sLocalIP = getRemoteAddr(request);
		// ログインなし許容IP
		final String sAllowIP = KNCommonProperty.getStringValue("DLAllowIP");
		final String[] array = sAllowIP.split(",");
		
		for(int i=0; i < array.length; i++ ) {
			if(array[i].equals(sLocalIP)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * ログインなしを許容するリファラからの接続か
	 * @return
	 */
	private boolean isAllowReferer(final HttpServletRequest request){
		
		// リファラチェック
		final String sAllowReferer = KNCommonProperty.getStringValue("DLAllowReferer");
		final String[] array = sAllowReferer.split(",");
		
		// 設定ファイル値が「空」
		if ("".equals(sAllowReferer)) return true;
		
		//REFERERが存在しない
		if (request.getHeader("REFERER") == null ) return false;
		
		// 設定ファイル値で開始されている
		for (int i = 0; i < array.length; i++ ) {
			if( request.getHeader("REFERER").startsWith(array[i])) {
				return true;
			}
		}
		
		// １．設定ファイル値が「空」でない
		// ２．REFERERが存在する
		// ３．REFERERが設定ファイル値で開始されていない
		return false;
			

	}
	
}

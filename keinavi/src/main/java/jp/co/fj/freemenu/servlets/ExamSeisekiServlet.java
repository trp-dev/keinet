/*
 * 作成日: 2004/09/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.servlets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.freemenu.data.CreateStatus;
import jp.co.fj.keinavi.data.sheet.SweeperSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.KNCommonProperty;


public class ExamSeisekiServlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		// HTTPセッション
		HttpSession session = request.getSession(false);
		// 作成状況
		CreateStatus status = (CreateStatus) session.getAttribute(CreateStatus.SESSION_KEY);
		// 掃除屋さん
		SweeperSession sweeper = (SweeperSession) session.getAttribute(SweeperSession.SESSION_KEY);

		// contentTypeを出力
		response.setContentType("text/csv");
		// ファイル名の送信
		response.setHeader(
			"Content-disposition",
			"attachment; filename=\"" + status.getFilename() + "\""
		);
		
		BufferedOutputStream out = null;
		BufferedInputStream in = null;
		File file = null;
		try {	
		    // ダイレクトダウンロード
		    if (status.isDirect()) {
		        
		    // 個別生成
		    } else {
				file =
				    new File(
						KNCommonProperty.getKNTempPath()
						+ File.separator
						+ status.getSessionKey()
						+ File.separator
						+ status.getFilename()
					);
		    }
			
			in = new BufferedInputStream(new FileInputStream(file));
			out = new BufferedOutputStream(response.getOutputStream());
		
			int data;
			while((data = in.read()) != -1) {
				out.write(data);
			}

		} catch (FileNotFoundException e) {
			throw new ServletException(e.getMessage());
			
		// ダイアログでキャンセルを押されると
		// ClientAbortExceptionが発生するので例外は無視する
		} catch (Exception e) {

		} finally {
			// OutputStreamを閉じる			
			if (out != null) try { out.close(); } catch (Exception e) {}

			// InputStreamを閉じる
			if (in != null) in.close();

			// 掃除
			if (!status.isDirect()) {
				file.delete();
				sweeper.deleteKey(status.getSessionKey());
			}
		} 
	}
	
}

package jp.co.fj.freemenu.beans.f002;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.freemenu.data.f002.F002Function;

import com.fjh.beans.DefaultBean;

/**
 *
 * 成績統計資料集ダウンロード用の基本Bean
 * 
 * 2008.03.05	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public abstract class F002Bean extends DefaultBean {

	// 機能データ
	private F002Function function;

	// 地区リスト
	private List areaList;
	
	// 対象模試年度
	private String targetExamYear;
	
	// 対象模試コード
	private String targetExamCd;
	
	// 対象地区コード
	private String targetDistCd;
	
	// 対象国私区分
	private String targetDivCd;
	
	// 対象科目コード
	private String targetSubCd;	
		
	/**
	 * @return ダウンロード対象のファイルを返す
	 */
	public abstract File getDownloadFile();

	/**
	 * @return 模試年度コードまでのデータファイルディレクトリ
	 */
	public File getYearDir() {
		
		return new File(function.getDataFileDir(), getTargetExamYear());
	}
	
	/**
	 * @return 模試コードまでのデータファイルディレクトリ
	 */
	public File getExamDir() {
		
		return new File(getYearDir(), getTargetExamCd());
	}
	
	/**
	 * @return 地区コードまでのデータファイルディレクトリ
	 */
	public File getDistDir() {
		
		return new File(getExamDir(), getTargetDistCd());
	}

	/**
	 * @return areaList 地区リストのIterator
	 */
	public Iterator getAreaListIterator() {
		
		return areaList.iterator();
	}

	/**
	 * @param pAreaList 設定する areaList
	 */
	public void setAreaList(List pAreaList) {
		areaList = pAreaList;
	}

	/**
	 * @param pFunction 設定する function
	 */
	public void setFunction(F002Function pFunction) {
		function = pFunction;
	}
	
	/**
	 * @return DBコネクションが処理に必要であるか
	 */
	public boolean needConnection() {
		return false;
	}

	/**
	 * @return targetDistCd
	 */
	public String getTargetDistCd() {
		return targetDistCd;
	}

	/**
	 * @param pTargetDistCd 設定する targetDistCd
	 */
	public void setTargetDistCd(String pTargetDistCd) {
		targetDistCd = pTargetDistCd;
	}

	/**
	 * @return targetDivCd
	 */
	public String getTargetDivCd() {
		return targetDivCd;
	}

	/**
	 * @param pTargetDivCd 設定する targetDivCd
	 */
	public void setTargetDivCd(String pTargetDivCd) {
		targetDivCd = pTargetDivCd;
	}

	/**
	 * @return targetExamCd
	 */
	public String getTargetExamCd() {
		return targetExamCd;
	}

	/**
	 * @param pTargetExamCd 設定する targetExamCd
	 */
	public void setTargetExamCd(String pTargetExamCd) {
		targetExamCd = pTargetExamCd;
	}

	/**
	 * @return targetExamYear
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @param pTargetExamYear 設定する targetExamYear
	 */
	public void setTargetExamYear(String pTargetExamYear) {
		targetExamYear = pTargetExamYear;
	}

	/**
	 * @return targetSubCd
	 */
	public String getTargetSubCd() {
		return targetSubCd;
	}

	/**
	 * @param pTargetSubCd 設定する targetSubCd
	 */
	public void setTargetSubCd(String pTargetSubCd) {
		targetSubCd = pTargetSubCd;
	}
	
}

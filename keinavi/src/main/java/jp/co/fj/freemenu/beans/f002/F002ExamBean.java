package jp.co.fj.freemenu.beans.f002;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.freemenu.data.f002.F002ExamData;
import jp.co.fj.freemenu.util.StringUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 成績統計資料集ダウンロードの
 * 模試情報を作成するBean
 *
 * 2008.03.06	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class F002ExamBean extends DefaultBean {

	// 模試名変換ユーティリティ
	private static final StringUtil STRUTIL = new StringUtil();

	// 入力：取得開始年度
	private String fromYear;

	// 入力：模試機能マップ
	// key   ... 模試年度＋模試コード
	// value ... 機能IDリスト
	private Map examFunctionMap;

	// 入力：ヘルプデスクであるか
	private boolean isHelpdesk = false;

	// 出力：模試データリスト
	private List examList;

	// 出力：最新模試データ
	private F002ExamData latestExamData;

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// 全ての模試を取得
		final List list = createExamList();

		// 模試データリストを作る
		createExamList(list);
	}

	private List createExamList() throws SQLException {

		final List list = new ArrayList();

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(createQuery());
			ps.setString(1, fromYear);
			rs = ps.executeQuery();
			while (rs.next()) {

				final F002ExamData data = new F002ExamData();
				data.setExamYear(rs.getString(1));
				data.setExamCD(rs.getString(2));
				data.setExamName(STRUTIL.editExamName(rs.getString(3)));
				data.setExamTypeCD(rs.getString(4));
				list.add(data);

				// 最新模試は画面表示の初期値に使うので保持する
				if (rs.getInt(5) == 1) {
					latestExamData = data;
				}
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}

		return list;
	}

	private String createQuery() throws SQLException {

		final Query query = QueryLoader.getInstance().load("f04");

		if (isHelpdesk) {
			query.replaceAll("out_dataopendate", "in_dataopendate");
		}

		return query.toString();
	}

	private void createExamList(final List list) {

		examList = new ArrayList();

		// 機能IDが存在する模試だけを詰める
		for (Iterator ite = list.iterator(); ite.hasNext();) {
			final F002ExamData data = (F002ExamData) ite.next();
			final List functionList = (List) examFunctionMap.get(data.getExamId());
			if (functionList != null) {
				data.setFunctionList(functionList);
				examList.add(data);
			}
		}
	}

	/**
	 * @return examList
	 */
	public List getExamList() {
		return examList;
	}

	/**
	 * @return latestExamData
	 */
	public F002ExamData getLatestExamData() {
		return latestExamData;
	}

	/**
	 * @param pExamFunctionMap 設定する examFunctionMap
	 */
	public void setExamFunctionMap(Map pExamFunctionMap) {
		examFunctionMap = pExamFunctionMap;
	}

	/**
	 * @param pFromYear 設定する fromYear
	 */
	public void setFromYear(String pFromYear) {
		fromYear = pFromYear;
	}

	/**
	 * @param pHelpdesk 設定する helpdesk
	 */
	public void setHelpdesk(boolean pIsHelpdesk) {
		isHelpdesk = pIsHelpdesk;
	}

}

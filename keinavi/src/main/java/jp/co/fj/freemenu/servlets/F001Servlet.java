package jp.co.fj.freemenu.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.KNCommonProperty;

/**
 * 
 * 2005.10.18	Yoshimoto KAWAI - TOTEC
 * 				アクセスログ対応
 * 
 * @author fujito-urakawa
 * 
 */
public class F001Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		
		final HttpSession session = request.getSession();
		
		try {
			String sDLRoot = KNCommonProperty.getDLSDataPath();
			session.setAttribute("sDLRoot", sDLRoot);
			String sUPRoot = KNCommonProperty.getDLSPOBDataPath();
			session.setAttribute("sUPRoot", sUPRoot);
		} catch (Exception e) {
			throw new ServletException(e.getMessage());
		}
		
		// JSP
		if ("w002".equals(getForward(request)) || "f001".equals(getForward(request))) {
			
			//模試受付システム用情報作成
			createMoshiSys(request);
			super.forward(request, response, JSP_F001);

		// 不明
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

/*
 * 作成日: 2004/09/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.servlets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.freemenu.data.ExamScoreData;
import jp.co.fj.freemenu.data.ExamScoreSession;
import jp.co.fj.freemenu.forms.FileDownloadForm;
import jp.co.fj.freemenu.util.DownloadUtil;
import jp.co.fj.freemenu.util.PathUtil;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.log.KNLog;

/**
 * @author fujito-urakawa
 *
 * 2005.8.11 	Totec) T.Yamada 	[1]F002_5追加
 */
public class FileDownloadServlet extends DefaultHttpServlet {

    // 2019/10/16 QQ)Ooseto 共通テスト対応 ADD START
    KNLog log = KNLog.getInstance(null,null,null);
    // 2019/10/16 QQ)Ooseto 共通テスト対応 ADD END

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		// HTTPセッション
		final HttpSession session = request.getSession(false);

		FileDownloadForm form = null;
		String sDLRoot = null;
		String sDLResRoot = null;
		try {
			sDLRoot = KNCommonProperty.getDLSDataPath();
			sDLResRoot = KNCommonProperty.getDLSPOBDataPath();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		try {
			form = (FileDownloadForm)super.getActionForm(request,
				"jp.co.fj.freemenu.forms.FileDownloadForm");
		}
		catch (Exception e) {
			throw new ServletException(e);
		}

		// [2004/10/27 nino]
		// 模試成績ダウンロードはその都度、成績データを作成してダウンロードする
		if ("f005_1".equals(form.getFlag()) || "f005_2".equals(form.getFlag()) ) {

		    // セキュリティチェック
		    // 自校の一括コードでなければならない
		    ExamScoreSession score = (ExamScoreSession)
		    	session.getAttribute(ExamScoreSession.SESSION_KEY);

		    List container = (List) score.getExamMap().get(form.getDecnendo());

	        if (!container.contains(
	                new ExamScoreData
	                	(form.getDecnendo(), form.getExamCD(), form.getSchoolCD()))) {
			    throw new ServletException(
                    "不正な一括コードの指定です。" + form.getSchoolCD());
	        }

			// contentTypeを出力
			response.setContentType("application/octet-stream-dummy");
			// ファイル名の送信
			response.setHeader(
				"Content-Disposition",
				"inline; filename=\"" + getFileName(form) + "\""
			);

			BufferedOutputStream out = null;
			BufferedInputStream in = null;
			try {
			    // データファイルパス
			    String path;

			    // 2019/09/12 QQ)Ooseto 共通テスト対応 UPD START
			    // センターリサーチ
			    if ("38".equals(form.getExamCD())) {
			        // 2019/09/19 QQ)Seo 共通テスト対応 UPD START
//			        if("gy".equals(form.getHsgy())) path = KNCommonProperty.getCRAbilityDLPath();
                                if("GY".equals(form.getHsgy())) path = KNCommonProperty.getCRAbilityDLPath();
	                        // 2019/09/19 QQ)Seo 共通テスト対応 UPD END
			        else path = KNCommonProperty.getCRExamScoreDLPath();
			    // それ以外
			    } else {
                                // 2019/09/19 QQ)Seo 共通テスト対応 UPD START
//                                if("gy".equals(form.getHsgy())) path = KNCommonProperty.getAbilityDLPath();
                                if("GY".equals(form.getHsgy())) path = KNCommonProperty.getAbilityDLPath();
                                // 2019/09/19 QQ)Seo 共通テスト対応 UPD END
                                else path = KNCommonProperty.getExamScoreDLPath();
			    }
			    // 2019/09/12 QQ)Ooseto 共通テスト対応 UPD END

		        File file =
				    new File(
						path + File.separator
						+ form.getDecnendo() + File.separator
						+ form.getExamCD() + File.separator
						+ form.getSchoolCD() + ".csv"
					);

				in = new BufferedInputStream(new FileInputStream(file));
				out = new BufferedOutputStream(response.getOutputStream());

				int data;
				while((data = in.read()) != -1) {
					out.write(data);
				}

				//	アクセスログ
				//	高校別データダウンロードログ
				String info = form.getDecnendo() + "," + form.getExamCD() + "," + getFileName(form);
				actionLog(request, "423", info);
			} catch (FileNotFoundException e) {
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
			    //throw new ServletException(e.getMessage());
			    log.Err("","データファイルエラー",e.toString());
			    throw new ServletException(e);
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END

			// ダイアログでキャンセルを押されると
			// ClientAbortExceptionが発生するので例外は無視する
			} catch (Exception e) {
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 ADD START
			    log.Err("","ダイアログキャンセル",e.toString());
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 ADD END
			} finally {
				// OutputStreamを閉じる
				if (out != null) try { out.close(); } catch (Exception e) {}
				// InputStreamを閉じる
				if (in != null) in.close();
			}

		} else {
			// ファイルパス
			String filepath = getRootPath(form, sDLRoot, sDLResRoot);
			String filename = getFileName(form);
			// ダウンロード処理実行
			DownloadUtil.downFile(response, filepath + filename, filename);
			//	アクセスログ
			//	大学入試センター試験分析データ集ダウンロードログ
			if ("f003_1".equals(form.getFlag()) || "f003_2".equals(form.getFlag())) {
				actionLog(request, "422", filename);
			}
		}
	}

	/**
	 * DLするファイルが存在するパスを取得
	 * @param form
	 * @return
	 */
	private String getRootPath(FileDownloadForm form, String rootPath, String resRootPath) {

		//flagの情報でパス名取得を分岐
		String filepath = null;
		if ("f003_1".equals(form.getFlag())) {
			// 大学・学部・学科別学力分布
			filepath = PathUtil.getDistRootPath(rootPath, form.getDecnendo(), form.getDivisionCD());
		}
		else if ("f006".equals(form.getFlag())) {
			// 調査票データ
			filepath = PathUtil.getResearchRootPath(resRootPath, form.getType());
		}
		else {
			// 高校別成績データダウンロード
			filepath = PathUtil.getHSRootPath(rootPath, form.getDecnendo(), form.getSchoolCD(), form.getGradeCD());
		}
		return filepath;
	}

	/**
	 * DLするファイル名を取得
	 * @param form
	 * @return
	 */
	private String getFileName(FileDownloadForm form) {
		// TODO 自動生成されたメソッド・スタブ
		//flagの情報でファイル名取得を分岐
		String filename = null;
		if ("f003_1".equals(form.getFlag())) {
			// 全国版
			if ("00".equals(form.getAreaCD())) {
				// 短大の場合
				if ("03".equals(form.getDivisionCD())) {
					filename = PathUtil.makeFileName(
							"pdf",
							"univ",
							form.getDivisionCD()+form.getAreaCD());
				}
				// その他の場合
				else {
					filename = PathUtil.makeFileName(
							"zip",
							"univ",
							form.getDivisionCD()+form.getAreaCD()+form.getType());
				}
			}
			// 各地区版
			else {
				filename = PathUtil.makeFileName(
							form.getType(),
							"univ",
							form.getDivisionCD()+form.getAreaCD());
			}

		}
		else if ("f006".equals(form.getFlag())) {
			// 調査票データ
			filename = PathUtil.makeResearchFileName(
							form.getType(),
							form.getExeName(),
							form.getSchoolCD());
		}
		else {
			// 高校別成績データダウンロード(高校＋学年＋模試)
			// 受験学力測定テスト（調査回答１）
			if (form.getExamCD().endsWith("_1")) {
				filename = PathUtil.makeFileName(
						form.getType(),
						"SG1_",
						form.getSchoolCD() + form.getGradeCD()
						+ form.getExamCD().substring(0, 2));
			// 受験学力測定テスト（調査回答２）
			} else if (form.getExamCD().endsWith("_2")) {
				filename = PathUtil.makeFileName(
						form.getType(),
						"SG2_",
						form.getSchoolCD() + form.getGradeCD()
						+ form.getExamCD().substring(0, 2));
			// それ以外
			} else {
			    //  2019/09/12 QQ)Ooseto 共通テスト対応 UPD START
//				filename = PathUtil.makeFileName(
//						form.getType(),
//						"hs",
//						form.getSchoolCD()+form.getGradeCD()+form.getExamCD());
			    filename = PathUtil.makeFileName(
			            form.getType(),
			            form.getHsgy(),
			            form.getSchoolCD() + form.getGradeCD() + form.getExamCD());
			    //  2019/09/12 QQ)Ooseto 共通テスト対応 UPD END
			}
		}
		return filename;
	}

}

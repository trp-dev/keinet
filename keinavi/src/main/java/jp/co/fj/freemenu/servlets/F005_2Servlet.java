/*
 * 作成日: 2004/09/03
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.freemenu.data.ExamScoreSession;
import jp.co.fj.freemenu.forms.F005_2Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class F005_2Servlet extends DefaultHttpServlet {
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	    // アクションフォーム
	    F005_2Form form = (F005_2Form) super.getActionForm(request,
			"jp.co.fj.freemenu.forms.F005_2Form");
		// 模試成績セッション
	    ExamScoreSession score = (ExamScoreSession)
	    	request.getSession(false).getAttribute(ExamScoreSession.SESSION_KEY);
	    
		// 模試データをソートする
	    List container = new ArrayList();
	    container.addAll((Collection) score.getExamMap().get(form.getNendo()));
	    Collections.sort(container);
	    
	    request.setAttribute("ExamDataList", container);
	    request.setAttribute("form", form);

	    if ("f005_2".equals(super.getForward(request))) {
			super.forward(request, response, JSP_F005_2);
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

/*
 * 作成日: 2004/09/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.servlets;

import java.io.IOException;
import java.io.File;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.freemenu.forms.F008Form;
import jp.co.fj.freemenu.util.MailUtility;
import jp.co.fj.freemenu.util.UploadUtil;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.date.FullDateUtil;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class F008Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		
		F008Form form = null;
		String forward = "";
		LoginSession ls = super.getLoginSession(request);
		UploadUtil uputil = new UploadUtil();
		HttpSession session = request.getSession();
			
		try {
			form = (F008Form)super.getActionForm(request,
			"jp.co.fj.freemenu.forms.F008Form");
		} catch (Exception e) {
			throw new ServletException(e);
		}
		// TODO 自動生成されたメソッド・スタブ
		// multipartの場合requestから直接forwardを取得できない為、formから取得
		if ("f008".equals(form.getForward()) || "f008".equals(form.getForward())) {
			//ファイルアップロード処理を行う
			try {
				String path = KNCommonProperty.getDLSPOBDataPath() + 
								"data"+ File.separator + "examdata" + File.separator;
				forward = uputil.makeUploadFile(form.getFile(), form.getFilename(), path, ls.getUserID());
			} catch (Exception e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
				throw new ServletException(e);
			}
			// アップロード成功
			if(forward.equals("f008")) {
				// メール送信処理
				MailUtility mail = new MailUtility();
				FullDateUtil du = new FullDateUtil();
				//mail.sendEMail(du.date2String(), ls.getUserName(), ls.getUserID(), uputil.getFileName(form.getFilename()));
				super.forward(request, response, JSP_F008);
			}
			// アップロード失敗（通信：ファイル書き込み）
			else if (forward.equals("f009_1")){
				super.forward(request, response, "/UploadFailure1");
			}
			// アップロード失敗（ファイルサイズ＝０）
			else {
				super.forward(request, response, "/UploadFailure2");
			}
		}
		else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}
}

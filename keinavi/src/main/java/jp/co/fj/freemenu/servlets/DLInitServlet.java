package jp.co.fj.freemenu.servlets;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import jp.co.fj.freemenu.beans.f002.F002FunctionListBean;
import jp.co.fj.keinavi.util.log.KNAccessLog;

/**
 *
 * ダウンロードメニュー用の初期化処理を行なうサーブレット
 * 
 * 2008.03.05	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class DLInitServlet extends HttpServlet {

 	/**
	 * @see javax.servlet.Servlet#init(javax.servlet.ServletConfig)
	 */
	public void init(final ServletConfig config) throws ServletException {
		
		// F002の機能リストBeanをロード
		loadF002FunctionListBean(config.getServletContext());

		// ログ
		KNAccessLog.lv1(null, null, null, null, null, "INITIALIZED DLMENU");
	}

	private void loadF002FunctionListBean(final ServletContext context) {
		
		try {
			final F002FunctionListBean bean = new F002FunctionListBean();
			bean.execute();
			context.setAttribute("F002FunctionListBean", bean);
		} catch (final Exception e) {
			createServletException(e);
		}
	}

	/**
	 * @see javax.servlet.GenericServlet#destroy()
	 */
	public void destroy() {
		
		super.destroy();
		
		// ログ
		KNAccessLog.lv1(null, null, null, null, null, "DESTROYED DLMENU");
	}
	
	protected ServletException createServletException(final Exception e) {
		
		final ServletException s = new ServletException(e.getMessage());
		s.initCause(e);
		return s;
	}

}

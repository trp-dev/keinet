package jp.co.fj.freemenu.beans.f002;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import jp.co.fj.freemenu.util.PathUtil;

/**
 *
 * 成績統計資料集ダウンロード用のBean
 * 国公立大学学部別・高校別志望状況
 * 
 * 2008.03.05	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class F002_4Bean extends F002_1Bean {

	// ファイル存在フラグ保持マップ
	// key ... 地区コード
	// value ... Boolean
	private Map fileMap;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		fileMap = new HashMap();
		
		// 模試コードまでのディレクトリ
		final File dir = getExamDir();
		
		for (Iterator ite = getAreaListIterator(); ite.hasNext();) {
			final String[] area = (String[]) ite.next();
			final String distCd = area[0];
			final File file = new File(dir, createFileName("pdf", distCd));
			fileMap.put(distCd, Boolean.valueOf(file.exists()));
		}
		
		// 全国版は特別処理
		final File file = new File(dir, createFileName("zip", "00pdf"));
		fileMap.put("00", Boolean.valueOf(file.exists()));
	}

	/**
	 * @see jp.co.fj.freemenu.beans.f002.F002_1Bean#createFileName(
	 * 			java.lang.String, java.lang.String)
	 */
	protected String createFileName(final String type, final String distCd) {
		
		return PathUtil.makeFileName(
				type, "can", getTargetExamCd() + distCd);
	}

	/**
	 * @return fileMap
	 */
	public Map getFileMap() {
		return fileMap;
	}

}

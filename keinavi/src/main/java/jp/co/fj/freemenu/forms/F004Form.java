/*
 * 作成日: 2004/09/07
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.forms;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class F004Form extends BaseForm {

	private String forward = null;
	private String backward = null;
	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ

	}

	/**
	 * @return
	 */
	public String getBackward() {
		return backward;
	}

	/**
	 * @return
	 */
	public String getForward() {
		return forward;
	}

	/**
	 * @param string
	 */
	public void setBackward(String string) {
		backward = string;
	}

	/**
	 * @param string
	 */
	public void setForward(String string) {
		forward = string;
	}

}

package jp.co.fj.freemenu.servlets;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.freemenu.beans.HsdlPropertyBean;
import jp.co.fj.freemenu.forms.F004Form;
import jp.co.fj.keinavi.beans.UploadFileBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.KNCommonProperty;

/**
 *
 * 
 * 
 * 2004.08.20	Fujito URAKAWA - TOTEC
 *				[新規作成]
 *
 * <2010年度マーク高２模試対応>
 * 2011.01.24 	Tomohisa YAMADA - TOTEC
 * 			アップロード機能
 *
 * @author Fujito URAKAWA - TOTEC
 * @version 1.0
 * 
 */
public class F004Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ
		HttpSession session = request.getSession();
		Connection con = null;
		HsdlPropertyBean hsbean = null;
		
		F004Form form = null;
		try {
			session.setAttribute("sDLResRoot",KNCommonProperty.getDLSPOBDataPath());
			session.setAttribute("sUPRoot",KNCommonProperty.getDLSPOBDataPath());
		} catch (Exception e2) {
			// TODO 自動生成された catch ブロック
			throw new ServletException(e2);
		}
		
		try {
			form = (F004Form)super.getActionForm(request,
				"jp.co.fj.freemenu.forms.F004Form");
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			throw new ServletException(e);
		}
		
		// w002orw008からきた場合には、KBKEYを更新
		if ("w002".equals(form.getBackward()) || "w008".equals(form.getBackward())) {
			// ------------------------------------------------------------
			// 2004.11.15
			// Yoshimoto KAWAI - Totec
			// DBKEYは書き換えない
			// 無料メニューも体験版以外は通常DBとなるため
			////DBKEYの更新
			// ------------------------------------------------------------
			//LoginSession ls = super.getLoginSession(request);
			try {
				//ls.setDbKey(KNCommonProperty.getFDBSID());
				//LoginSessionの更新
				//session.setAttribute(LoginSession.SESSION_KEY, ls);
				//セッションにトップメニューから来た情報を追加
				session.setAttribute("FromTopMenu", form.getBackward());
			} catch (Exception e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}
		}
		
		if ("f004".equals(super.getForward(request)) || "f004".equals(form.getForward())) {
			try {
				con = super.getConnectionPool(request);
				con.setAutoCommit(false);
				
				hsbean = new HsdlPropertyBean();
				hsbean.setConnection(null, con);
				LoginSession ls = (LoginSession)session.getAttribute(LoginSession.SESSION_KEY);
				hsbean.setHscd(ls.getUserID());
				// 環境変数を取得する(パスはpropertiesファイル内にセット)
				String path = KNCommonProperty.getDLSPOBDataPath();
				hsbean.setPath(path + "data" + File.separator 
								+ "conf" + File.separator + "dlink.dat");
				hsbean.execute();
				
				//ウィルスチェック履歴
				request.setAttribute(
						"virusDetectedBean", 
						searchVirusDetected(con, getLoginSession(request)));
				
			} catch (Exception e) {
				// TODO 自動生成された catch ブロック
				throw new ServletException(e);
			}
			finally {
				super.releaseConnectionPool(request, con);
			}

			session.setAttribute("HSDLBean", hsbean);
			super.forward(request, response, JSP_F004);
		}
		else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}
	
	/**
	 * ウィルス検知ファイル一覧を検索
	 * @param con
	 * @param loginSession
	 * @return
	 * @throws SQLException
	 */
	private UploadFileBean searchVirusDetected(
			Connection con, LoginSession loginSession) throws SQLException {
		UploadFileBean ufb = new UploadFileBean(loginSession);
		ufb.setConnection(null, con);
		ufb.searchVirusDetected();
		return ufb;
	}

}

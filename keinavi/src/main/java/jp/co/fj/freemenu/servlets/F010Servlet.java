package jp.co.fj.freemenu.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.freemenu.forms.F010Form;
import jp.co.fj.keinavi.beans.UploadFileBean;
import jp.co.fj.keinavi.beans.UploadFileKindBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 * 公開ファイルアップロード画面サーブレット
 * 
 * 2011.01.18	[新規作成]
 * 
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class F010Servlet extends DefaultHttpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {
		
		// アクションフォーム
		final F010Form form = (F010Form) getActionForm(
				request, "jp.co.fj.freemenu.forms.F010Form");
		
		final LoginSession loginSession = getLoginSession(request);
		
		// JSPへの遷移処理
		if ("f010".equals(form.getForward())) {

			Connection con = null;
			try {

				con = getConnectionPool(request);
									
				//ファイルがアップロードされた
				if (form.getFile() != null) {
					
					//入力エラー
					if (form.getErrorCount() > 0) {
						display(request, response, con, form);
					}
					else {
					
						UploadFileBean ufb = null;
						try {
							con.setAutoCommit(false);
							//ファイルのアップロードを実行
							ufb = new UploadFileBean(
									form.getFile(), 
									form.getFileName(), 
									form.getFileTypeID(), 
									form.getComments(), 
									loginSession);
							ufb.setConnection(null, con);
							ufb.upload();
							con.commit();
						} 
						//アップロード失敗
						catch (Exception e) {
							form.setUnsuccessful();
							rollback(con);
							ufb.deleteServerFile();
							e.printStackTrace();
						}
						request.setAttribute("form", form);
						super.forward(request, response, JSP_F010_RESULT);
					}
					
				} 
				//画面表示
				else {
					display(request, response, con, form);
				}

			} catch (SQLException e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

		// 不明なら転送
		} else {			
			super.dispatch(request, response);
		}
	}
	
	/**
	 * アップロードファイル種類一覧を検索する。
	 * @param conn
	 * @return
	 * @throws SQLException
	 */
	private UploadFileKindBean searchUploadFileKind(Connection conn) throws SQLException {
		UploadFileKindBean ufkb = new UploadFileKindBean();
		ufkb.setConnection(null, conn);
		ufkb.searchAll();
		return ufkb;
	}
	
	private void display(HttpServletRequest request,
			HttpServletResponse response, Connection con, F010Form form) throws SQLException, ServletException, IOException {
		request.setAttribute("uploadFileKindBean", searchUploadFileKind(con));
		request.setAttribute("form", form);
		super.forward(request, response, JSP_F010);
	}

}

/*
 * 作成日: 2004/09/01
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.beans;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.fjh.beans.DefaultBean;

/**
 * @author Totec) F.Urakawa
 * 
 * 2005.8.26 	Totec) T.Yamada 	[1]東京地区追加と海外削除メソッドの追加
 * 2005.9.26 	Totec) T.Yamada 	[2]営業部でログインしたときに学校ＩＤがないときに起こるnullpointerの不具合修正
 *
 */
public class AreaBean extends DefaultBean implements Cloneable{
	
	private ArrayList areaData = new ArrayList();
	private String[] area = new String[2];
	private String SchoolID = null;
	
	/**
	 * [1] add
	 * コンストラクタ
	 */
	public AreaBean(){
		this.areaData = new ArrayList();
		this.area = new String[2];
		this.SchoolID = null;
	}
	
	/**
	 * [1] add
	 * コンストラクタ
	 * @param list
	 * @param id
	 */
	public AreaBean(java.util.ArrayList list, String id){
		this.areaData = list;
		this.SchoolID = id;
	}
	
	/**
	 * [1] add
	 * ディープコピー
	 */
	public Object clone(){
		//return new AreaBean(new ArrayList(this.areaData), new String(this.SchoolID));//[2] del
		//[2] add start
		AreaBean temp = new AreaBean();
		if(this.areaData != null) temp.setAreaData(new ArrayList(this.areaData));
		if(this.SchoolID != null) temp.setSchoolID(new String(this.SchoolID));
		return temp;
		//[2] add end
	}
	
	/**
	 * [1] add
	 * 作成されたareaDataにAreaを追加する
	 */
	public void addArea(int index, String[] area){
		this.areaData.add(index, area);
	}
	
	/**
	 * [1] add
	 * 作成されたareaDataからAreaを削除する
	 */
	public void removeArea(String[] area){
		for(int i=0; i<this.areaData.size(); i++){
			String[] thisArea = (String[]) this.areaData.get(i);
			if(thisArea[0].equals(area[0]) && thisArea[1].equals(area[1])){
				this.areaData.remove(i);	
			}
		}
	}
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// TODO 自動生成されたメソッド・スタブ
		ResultSet rs;
		PreparedStatement ps;
		
		StringBuffer query = new StringBuffer(); // SQL
		query.append("select districtcd from PREFECTURE where prefcd = ");
		query.append("(select facultycd from school where bundlecd = ?)");
		ps = conn.prepareStatement(query.toString());
		ps.setString(1, SchoolID);
		rs = ps.executeQuery();
		if (rs.next()) {
			setSchoolID(rs.getString(1));
			ps.close();
			rs.close();
		}
		else {
			ps.close();
			rs.close();
		}
		
		query = new StringBuffer(); // SQL
		query.append("select districtcd,districtname from district ");
		query.append("where districtcd <> 99 order by districtcd asc");
		ps = conn.prepareStatement(query.toString());
		rs = ps.executeQuery();
		while(rs.next()) {
			area[0] = rs.getString(1);
			area[1] = rs.getString(2);
			areaData.add(area);
			area = new String[2];
		}
		ps.close();
		rs.close();

	}


	/**
	 * @return
	 */
	public String getSchoolID() {
		return SchoolID;
	}


	/**
	 * @param string
	 */
	public void setSchoolID(String string) {
		SchoolID = string;
	}

	
	/**
	 * @return
	 */
	public ArrayList getAreaData() {
		return areaData;
	}

	/**
	 * @param list
	 */
	public void setAreaData(ArrayList list) {
		areaData = list;
	}

}

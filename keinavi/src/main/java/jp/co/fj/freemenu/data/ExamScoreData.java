package jp.co.fj.freemenu.data;

import java.text.SimpleDateFormat;
import java.util.Date;

import jp.co.fj.keinavi.data.ExamData;

/**
 *
 * 模試成績データ
 * 
 * 
 * 2004.12.20	[新規作成]
 * 
 * 2007.02.21	既存の模試データを継承する形に変更
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class ExamScoreData extends ExamData {

	// データ開放日
    private Date dataOpenDate;
    // 一括コード
    private String bundleCd;
    
    /**
     * コンストラクタ
     */
    public ExamScoreData() {
        super();
    }

    /**
     * コンストラクタ
     * 
     * @param pExamYear 模試年度
     * @param pExamCd 模試コード
     * @param pBundleCd 一括コード
     */
    public ExamScoreData(String pExamYear, String pExamCd, String pBundleCd) {
        super(pExamYear, pExamCd);
        bundleCd = pBundleCd;
    }

    /**
     * @see jp.co.fj.keinavi.data.ExamData#compareTo(java.lang.Object)
     */
    public int compareTo(final Object o) {
    	
        final ExamScoreData data = (ExamScoreData) o;
        
		// データ開放日の降順
        int result = data.dataOpenDate.compareTo(this.dataOpenDate);
        if (result == 0) {
        	// 一括コードの昇順
        	result = data.bundleCd.compareTo(this.bundleCd);
        	if (result == 0) {
            	// 模試コードの昇順でソートする
        		return getExamCd().compareTo(data.getExamCd());
        	} else {
        		return result;
        	}
        } else {
        	return result;
        }
    }

    /**
     * @see jp.co.fj.keinavi.data.ExamData#equals(java.lang.Object)
     */
    public boolean equals(final Object obj) {
    	
    	if (!(obj instanceof ExamScoreData)) {
    		return false;
    	}
    	
        final ExamScoreData data = (ExamScoreData) obj;
        
        if (data.bundleCd == null) {
        	return false;
        }
 
        return super.equals(obj) && data.getBundleCd().equals(getBundleCd());
    }

	/**
	 * @see jp.co.fj.keinavi.data.ExamData#getDataOpenDate()
	 */
	public String getDataOpenDate() {
		return new SimpleDateFormat("yyyy/MM/dd").format(dataOpenDate);
	}

	/**
	 * @return examCd を戻す。
	 */
	public String getExamCd() {
		return super.getExamCD();
	}

	/**
	 * @param pExamCd pExamCd を設定する。
	 */
	public void setExamCd(final String pExamCd) {
		super.setExamCD(pExamCd);
	}

	/**
	 * @param dataOpenDate dataOpenDate を設定する。
	 */
	public void setDataOpenDate(Date dataOpenDate) {
		this.dataOpenDate = dataOpenDate;
	}

	/**
	 * @return bundleCd
	 */
	public String getBundleCd() {
		return bundleCd;
	}

	/**
	 * @param pBundleCd 設定する bundleCd
	 */
	public void setBundleCd(String pBundleCd) {
		bundleCd = pBundleCd;
	}
	
}

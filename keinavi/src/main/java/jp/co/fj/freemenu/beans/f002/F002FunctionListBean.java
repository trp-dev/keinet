package jp.co.fj.freemenu.beans.f002;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.freemenu.data.f002.F002Function;
import jp.co.fj.keinavi.util.KNCommonProperty;

import com.fjh.beans.DefaultBean;

/**
 *
 * 成績統計資料集ダウンロードの機能一覧Bean
 * 
 * 2008.03.05	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class F002FunctionListBean extends DefaultBean {

	private List functionList;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		// 将来的にDBから取得しても良いかも・・？？
		// Listにaddした順番どおりに画面には表示される
		
		functionList = new ArrayList(17);
		
		functionList.add(new F002Function("F002_6",
				"総合成績表", createDirFrom6("sogo")));
		
		functionList.add(new F002Function("F002_7",
				"科目別成績表", createDirFrom6("kamokubetu")));
		
		functionList.add(new F002Function("F002_8",
				"受験型別・科目別得点分布表", createDirFrom6("tokutenbunpu")));
		
		functionList.add(new F002Function("F002_9",
				"教科科目別設問別成績表", createDirFrom6("setumon")));
		
		functionList.add(new F002Function("F002_10",
				"マーク式設問別正答率", createDirFrom6("mark_setumon")));
		
		functionList.add(new F002Function("F002_15",
				"都道府県別・科目別成績一覧表", createDirFrom6("todofuken_kamoku")));
		
		functionList.add(new F002Function("F002_13",
				"レベル別設問別正答率表", createDirFrom6("level_que")));
		
		functionList.add(new F002Function("F002_14",
				"レベル別設問別成績表", createDirFrom6("level_hikaku")));
		
		functionList.add(new F002Function("F002_12",
				"文理別・現卒別成績表", createDirFrom6("bunri_level")));
		
		functionList.add(new F002Function("F002_11",
				"型別・現卒別成績表", createDirFrom6("bunri_gensotu")));
		
		functionList.add(new F002Function("F002_16",
				"合格可能性評価基準一覧", createDirFrom6("gokakuhyoka")));
		
		functionList.add(new F002Function("F002_5",
				"志望大学・学部・学科別成績分布表", createDir("univresult")));
		
		functionList.add(new F002Function("F002_1",
				"高校別総合成績一覧表", createDir("allresult")));
		
		functionList.add(new F002Function("F002_2",
				"型／科目別・高校別成績一覧表", createDir("allpattern")));
		
		functionList.add(new F002Function("F002_3",
				"高校別設問別成績一覧表", createDir("question")));
		
		functionList.add(new F002Function("F002_4",
				"国公立大学学部別・高校別志望状況", createDir("allcandidate")));
		
		functionList.add(new F002Function("F002_17",
				"高校別参加人数表", createDirFrom6("sankaninzu")));
				
	}

	private File createDir(final String name) {
		
		return new File(KNCommonProperty.getStringValue("DLSDataPath"), name);
	}
	
	private File createDirFrom6(final String name) {
		
		return new File(KNCommonProperty.getStringValue("DLSDataPath2"), name);
	}
	
	/**
	 * @param id 機能ID
	 * @return 機能データクラス
	 */
	public F002Function getFunction(final String id) {
		
		for (Iterator ite = functionList.iterator(); ite.hasNext();) {
			final F002Function function = (F002Function) ite.next();
			if (function.getFunctionId().equals(id)) {
				return function;
			}
		}

		throw new RuntimeException("不正な機能IDの指定です。" + id);
	}
	
	/**
	 * @return functionList
	 */
	public List getFunctionList() {
		return functionList;
	}

}

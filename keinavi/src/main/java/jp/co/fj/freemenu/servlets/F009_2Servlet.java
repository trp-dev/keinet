/*
 * 作成日: 2004/09/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class F009_2Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ
		super.forward(request, response, JSP_F009_2);

	}

}

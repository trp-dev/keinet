package jp.co.fj.freemenu.servlets.user;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.freemenu.beans.user.UserStatusSwitchBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.user.U001Form;
import jp.co.fj.keinavi.servlets.user.U001Servlet;

/**
 *
 * 利用者管理一覧画面サーブレット
 * ※DLサービス校用
 * 
 * 2008.12.24	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U001DLServlet extends U001Servlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		// アクションフォーム
		final U001Form form = (U001Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.user.U001Form");
		
		// 転送先がJSP
		if ("u001_dl".equals(getForward(request))) {
			
			// ログインセッション
			final LoginSession login = getLoginSession(request);
			
			// 転送元がJSP
			if ("u001_dl".equals(getBackward(request))) {
				
				// パスワード初期化
				if ("2".equals(form.getActionMode())) {
					initPassword(request, login, form);
					actionLog(request, "602");
				// 状態切り替え
				} else if ("3".equals(form.getActionMode()) || "4".equals(form.getActionMode())) {
					switchStatus(request, login, form);
				// 不正
				} else {
					throw new ServletException(
							"不正な動作モードです。" + form.getActionMode());
				}
			}
			
			// 利用者情報Beanをセットアップ
			setupLoginUserBean(request, login);
			
			forward2Jsp(request, response);
		
		// 不明なら転送
		} else {
			dispatch(request, response);
		}
	}
	
	/**
	 * 利用者の状態を変更する
	 * 
	 * @param request
	 * @param login
	 * @throws ServletException 
	 */
	protected void switchStatus(final HttpServletRequest request,
			final LoginSession login, final U001Form form) throws ServletException {
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);
			
			final String status = "3".equals(form.getActionMode()) ? "1" : "2";
			final UserStatusSwitchBean bean = new UserStatusSwitchBean(
					login.getUserID(), form.getTargetLoginId(), status);
			bean.setConnection(null, con);
			bean.execute();
			
			con.commit();
			
		} catch (final Exception e) {
			rollback(con);
			throw new ServletException(e);
			
		} finally {
			releaseConnectionPool(request, con);				
		}
	}

}

package jp.co.fj.freemenu.data.f002;

import java.util.List;

import jp.co.fj.keinavi.data.ExamData;

/**
 *
 * 成績統計資料集ダウンロードの
 * 模試データクラス
 * 模試に紐付いている機能IDリストを保持する
 * 
 * 
 * 2004.12.20	[新規作成]
 * 
 * 2007.02.21	既存の模試データを継承する形に変更
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class F002ExamData extends ExamData {

	// 機能IDリスト
	private List functionList;

	/**
	 * @return functionList
	 */
	public List getFunctionList() {
		return functionList;
	}

	/**
	 * @param pFunctionList 設定する functionList
	 */
	public void setFunctionList(List pFunctionList) {
		functionList = pFunctionList;
	}
	
}

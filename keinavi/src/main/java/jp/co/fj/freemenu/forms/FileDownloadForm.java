/*
 * 作成日: 2004/09/14
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.forms;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class FileDownloadForm extends BaseForm {

	private String flag = null;
	private String type = null;
	private String decnendo = null;
	private String schoolCD = null;
	private String gradeCD = null;
	private String patternCD = null;
	private String examCD = null;
	private String areaCD = null;
	private String divisionCD = null;
	private String exeName = null;
	// 2019/09/12 QQ)Ooseto 共通テスト対応 ADD START
	private String hsgy = null;
	// 2019/09/12 QQ)Ooseto 共通テスト対応 ADD END
	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ

	}

	/**
	 * @return
	 */
	public String getAreaCD() {
		return areaCD;
	}

	/**
	 * @return
	 */
	public String getDivisionCD() {
		return divisionCD;
	}

	/**
	 * @return
	 */
	public String getExamCD() {
		return examCD;
	}

	/**
	 * @return
	 */
	public String getFlag() {
		return flag;
	}

	/**
	 * @return
	 */
	public String getGradeCD() {
		return gradeCD;
	}


	/**
	 * @return
	 */
	public String getPatternCD() {
		return patternCD;
	}

	/**
	 * @return
	 */
	public String getSchoolCD() {
		return schoolCD;
	}

	/**
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param string
	 */
	public void setAreaCD(String string) {
		areaCD = string;
	}

	/**
	 * @param string
	 */
	public void setDivisionCD(String string) {
		divisionCD = string;
	}

	/**
	 * @param string
	 */
	public void setExamCD(String string) {
		examCD = string;
	}

	/**
	 * @param string
	 */
	public void setFlag(String string) {
		flag = string;
	}

	/**
	 * @param string
	 */
	public void setGradeCD(String string) {
		gradeCD = string;
	}


	/**
	 * @param string
	 */
	public void setPatternCD(String string) {
		patternCD = string;
	}

	/**
	 * @param string
	 */
	public void setSchoolCD(String string) {
		schoolCD = string;
	}

	/**
	 * @param string
	 */
	public void setType(String string) {
		type = string;
	}

	/**
	 * @return
	 */
	public String getExeName() {
		return exeName;
	}

	/**
	 * @param string
	 */
	public void setExeName(String string) {
		exeName = string;
	}

	/**
	 * @return
	 */
	public String getDecnendo() {
		return decnendo;
	}

	/**
	 * @param string
	 */
	public void setDecnendo(String string) {
		decnendo = string;
	}

	// 2019/09/12 QQ)Ooseto 共通テスト対応 ADD START
        /**
         * @return
         */
	public String getHsgy() {
            return hsgy;
        }
        /**
         * @param string
         */
	public void setHsgy(String string) {
	    hsgy = string;
	}
	// 2019/09/12 QQ)Ooseto 共通テスト対応 ADD END

}

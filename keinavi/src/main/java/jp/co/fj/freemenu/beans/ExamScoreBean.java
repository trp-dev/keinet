/*
 * 作成日: 2004/12/20
 */
package jp.co.fj.freemenu.beans;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import jp.co.fj.freemenu.data.ExamScoreData;
import jp.co.fj.freemenu.data.ExamScoreSession;
import jp.co.fj.freemenu.util.StringUtil;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import com.fjh.beans.DefaultBean;

/**
 * 模試成績セッションを作る
 *
 * 2005.03.29	Yoshimoto KAWAI - Totec
 *				年度が複数の時に ConcurrentModificationException が
 *　　　　　　　発生する場合があったのをFix
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */
public class ExamScoreBean extends DefaultBean {

    // 模試成績セッション
    private final ExamScoreSession examScoreSession = new ExamScoreSession();
    // 模試名変換クラス
    private final StringUtil util = new StringUtil();
    // 学校コード
    private String schoolCd;
    // データパス
    private String nDataPath;
    // データパス（センターリサーチ）
    private String cDataPath;
    // ヘルプデスクかどうか
    private boolean helpDesk;
    // 2019/09/12 QQ)Ooseto 共通テスト対応 ADD START
    // 学力要素別データパス
    private String nAbilityDataPath;
    // 学力要素別データパス（センターリサーチ）
    private String cAbilityDataPath;
    // 2019/09/12 QQ)Ooseto 共通テスト対応 ADD END

    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            final Map map = new HashMap(); // データマップ

            final Query query = QueryLoader.getInstance().load("f03");

            // 2019/09/24 QQ)Ooseto 共通テスト対応 ADD START
            if (StringUtils.isBlank(this.getNDataPath()) || StringUtils.isBlank(this.getNAbilityDataPath()))
                throw new IllegalArgumentException("模試成績データファイルのパスを設定してください。");

            if (StringUtils.isBlank(this.getCDataPath()) || StringUtils.isBlank(this.getCAbilityDataPath()))
                throw new IllegalArgumentException("リサーチデータファイルのパスを設定してください。");
            // 2019/09/24 QQ)Ooseto 共通テスト対応 ADD END

            // ヘルプデスクなら内部データ開放日
            if (isHelpDesk())
                query.replaceAll("out_dataopendate", "in_dataopendate");

            ps = conn.prepareStatement(query.toString());
            ps.setString(1, getSchoolCd());
            ps.setString(2, getSchoolCd());

            rs = ps.executeQuery();
            while (rs.next()) {
                final ExamScoreData data = createExamScoreData(rs);

                // 2019/09/24 QQ)Ooseto 共通テスト対応 ADD START
                // ファイル存在チェック
                String path;
                String pathAbi;
                // センターリサーチ
                if ("38".equals(data.getExamCd())) {
                    path = this.getCDataPath();
                    pathAbi = this.getCAbilityDataPath();
                } else {
                    path = this.getNDataPath();
                    pathAbi = this.getNAbilityDataPath();
                }
                File file =
                        new File(path + File.separator
                      + data.getExamYear() + File.separator
                      + data.getExamCd() + File.separator
                      + data.getBundleCd() + ".csv");
                File fileAbi =
                        new File(pathAbi + File.separator
                      + data.getExamYear() + File.separator
                      + data.getExamCd() + File.separator
                      + data.getBundleCd() + ".csv");

                if (file.exists() && fileAbi.exists()) data.setFileExists("1");
                else if (file.exists() && !fileAbi.exists()) data.setFileExists("2");
                else if (!file.exists() && fileAbi.exists()) data.setFileExists("3");
                else data.setFileExists("4");
                // 2019/09/24 QQ)Ooseto 共通テスト対応 ADD END

                // 年度単位の入れ物が無ければ作る
                List container = (List) map.get(data.getExamYear());
                if (container == null) {
                	container = new ArrayList();
                    map.put(data.getExamYear(), container);
                }

                // 入れ物にいれる
                container.add(data);

                // 受験学力測定テスト対応
                if (KNUtil.isAbilityExam(data)) {
                	// 模試名改変
                	data.setExamName(data.getExamName() + "（個人成績）");
                	// 調査回答１
                	final ExamScoreData data1 = createExamScoreData(rs);
                	data1.setExamCd(data1.getExamCd() + "_1");
                	data1.setExamName(data1.getExamName() + "（調査回答１）");
                	container.add(data1);
                	// 調査回答１
                	final ExamScoreData data2 = createExamScoreData(rs);
                	data2.setExamCd(data2.getExamCd() + "_2");
                	data2.setExamName(data2.getExamName() + "（調査回答２）");
                	container.add(data2);
                }
            }

            getExamScoreSession().setExamMap(map);
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }

        // ファイル存在チェック
        {
            // 2019/09/24 QQ)Ooseto 共通テスト対応 DEL START
//            if (this.getNDataPath() == null)
//                throw new IllegalArgumentException("模試成績データファイルのパスを設定してください。");
//
//            if (this.getCDataPath() == null)
//                throw new IllegalArgumentException("センターリサーチデータファイルのパスを設定してください。");
            // 2019/09/24 QQ)Ooseto 共通テスト対応 DEL END

            List delete = new LinkedList(); // 削除する年度リスト
            Iterator it = this.getExamScoreSession().getExamMap().keySet().iterator();
            while (it.hasNext()) {
                String key = (String) it.next();

                List container = (List) this.getExamScoreSession().getExamMap().get(key);
                Iterator ite = container.iterator();
                while (ite.hasNext()) {
                    ExamScoreData data = (ExamScoreData) ite.next();
                    // 2019/09/24 QQ)Ooseto 共通テスト対応 UPD START
                    if(data.getFileExists().equals("4"))ite.remove();
//                    String path;
//                    // センターリサーチ
//                    if ("38".equals(data.getExamCd())) {
//                        path = this.getCDataPath();
//                    } else {
//                        path = this.getNDataPath();
//                    }
//
//                    File file =
//                        new File(
//                            path + File.separator
//                            + data.getExamYear() + File.separator
//                            + data.getExamCd() + File.separator
//                            + data.getBundleCd() + ".csv");
//
//                    // なければ消す
//                    if (!file.exists()) ite.remove();
                }
                // 2019/09/24 QQ)Ooseto 共通テスト対応 UPD END

                // 空なら消す
                //if (container.size() == 0) this.getExamScoreSession().getExamMap().remove(key);
                if (container.size() == 0) delete.add(key);
            }

            // 空の年度は消す
            Iterator ite = delete.iterator();
            while (ite.hasNext()) {
            	String key = (String) ite.next();
            	this.getExamScoreSession().getExamMap().remove(key);
            }
        }

        // 年度のリストを作る
        {
            List years = new ArrayList();
            years.addAll(this.getExamScoreSession().getExamMap().keySet());
            Collections.sort(years);
            Collections.reverse(years);
            this.getExamScoreSession().setYears(years);
        }

    }

    // ResultSetからデータクラスを作る
	private ExamScoreData createExamScoreData(final ResultSet rs) throws SQLException {
		final ExamScoreData data = new ExamScoreData();
		data.setExamYear(rs.getString(1));
		data.setExamCd(rs.getString(2));
		data.setExamName(util.editExamName(rs.getString(3)));
		data.setDataOpenDate(rs.getDate(4));
		data.setTargetGrade(rs.getString(5));
		data.setBundleCd(rs.getString(6));
		data.setExamTypeCD(rs.getString(7));
		// 2019/09/24 QQ)Ooseto 共通テスト対応 ADD START
		data.setFileExists("4");
		// 2019/09/24 QQ)Ooseto 共通テスト対応 ADD END
		return data;
	}

    /**
	 * @return cDataPath を戻す。
	 */
	public String getCDataPath() {
		return cDataPath;
	}

	/**
	 * @param dataPath cDataPath を設定する。
	 */
	public void setCDataPath(String dataPath) {
		cDataPath = dataPath;
	}

	/**
	 * @return nDataPath を戻す。
	 */
	public String getNDataPath() {
		return nDataPath;
	}

	/**
	 * @param dataPath nDataPath を設定する。
	 */
	public void setNDataPath(String dataPath) {
		nDataPath = dataPath;
	}

	/**
	 * @return examScoreSession を戻す。
	 */
	public ExamScoreSession getExamScoreSession() {
		return examScoreSession;
	}

	/**
	 * @return schoolCd を戻す。
	 */
	public String getSchoolCd() {
		return schoolCd;
	}

	/**
	 * @param schoolCd schoolCd を設定する。
	 */
	public void setSchoolCd(String schoolCd) {
		this.schoolCd = schoolCd;
	}

	/**
	 * @return helpDesk を戻す。
	 */
	public boolean isHelpDesk() {
		return helpDesk;
	}

	/**
	 * @param helpDesk helpDesk を設定する。
	 */
	public void setHelpDesk(boolean helpDesk) {
		this.helpDesk = helpDesk;
	}

	// 2019/09/12 QQ)Ooseto 共通テスト対応 ADD START
	/**
	 * @return nAbilityDataPath を戻す。
	 */
	public String getNAbilityDataPath() {
	    return nAbilityDataPath;
	}
	/**
	 * @param dataPath nAbilityDataPath を設定する。
	 */
	public void setNAbilityDataPath(String dataPath) {
	    nAbilityDataPath = dataPath;
	}
	/**
	 * @return cAbilityDataPath を戻す。
	 */
        public String getCAbilityDataPath() {
            return cAbilityDataPath;
        }
        /**
         * @param dataPath cAbilityDataPath を設定する。
         */
        public void setCAbilityDataPath(String dataPath) {
            cAbilityDataPath = dataPath;
        }
	// 2019/09/12 QQ)Ooseto 共通テスト対応 ADD END


}

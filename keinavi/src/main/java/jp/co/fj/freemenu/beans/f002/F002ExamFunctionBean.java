package jp.co.fj.freemenu.beans.f002;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import jp.co.fj.freemenu.data.f002.F002Function;

import com.fjh.beans.DefaultBean;

/**
 *
 * 成績統計資料集ダウンロードで
 * 模試ごとの機能リストを取得するBean
 * 
 * 2008.03.05	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class F002ExamFunctionBean extends DefaultBean {

	// 入力：機能リスト
	private List functionList;
	
	// 入力：取得開始年度
	private String fromYear;
	
	// 出力：模試機能マップ
	// key   ... 模試年度＋模試コード
	// value ... 機能IDリスト
	private Map examFunctionMap;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		examFunctionMap = new HashMap();
		
		for (Iterator ite = functionList.iterator(); ite.hasNext();) {
			final F002Function function = (F002Function) ite.next();
			final File dir = function.getDataFileDir();
			
			// ディレクトリが存在しなければここまで
			if (!dir.exists()) {
				continue;
			}

			// 模試年度読み取り
			readExamYear(dir, function.getFunctionId());
		}
	}

	private void readExamYear(final File dir, final String functionId) {
		
		final File[] files = dir.listFiles();

		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				if (fromYear.compareTo(files[i].getName()) <= 0) {
					// 模試コード読み取り
					if (files[i].isDirectory()) {
						readExamCd(files[i], functionId);
					}
				}
			}
		}
	}

	private void readExamCd(final File dir, final String functionId) {
		
		final File[] files = dir.listFiles();

		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				// 模試年度＋模試コード
				final String key = dir.getName() + files[i].getName();

				List functionList = (List) examFunctionMap.get(key);
				if (functionList == null) {
					functionList = new LinkedList();
					examFunctionMap.put(key, functionList);
				}
				functionList.add(functionId);
			}
		}
	}

	/**
	 * @return examFunctionMap
	 */
	public Map getExamFunctionMap() {
		return examFunctionMap;
	}

	/**
	 * @param pFunctionList 設定する functionList
	 */
	public void setFunctionList(List pFunctionList) {
		functionList = pFunctionList;
	}

	/**
	 * @param pFromYear 設定する fromYear
	 */
	public void setFromYear(String pFromYear) {
		fromYear = pFromYear;
	}

}

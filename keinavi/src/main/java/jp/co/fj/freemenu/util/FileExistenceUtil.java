/*
 * 作成日: 2004/09/15
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.freemenu.beans.AreaBean;
import jp.co.fj.keinavi.data.ExamData;

/**
 * @author fujito-urakawa
 *
 * 2005.8.11 	Totec) T.Yamada 	[1]F002__5用メソッド追加
 */
public class FileExistenceUtil {
	
	/**
	 * 大学入試センター試験分析データ集ファイルが存在するか
	 * @param sRoot
	 * @param sYear
	 * @param sDivisionCD
	 * @param ab
	 * @return
	 */
	public ArrayList checkFileExistForF003_1(String sRoot, String sYear, String sDivisionCD, AreaBean ab) {

		ArrayList list = new ArrayList();
		ArrayList larea = ab.getAreaData();
		boolean[] bool = null; 
		File file1 = null;
		String[] area = null;
		String filepath = PathUtil.getDistRootPath(sRoot, sYear, sDivisionCD);
		String pdfname = null;

		for (int i = 0; i < larea.size(); i++) {
			area = (String[])larea.get(i);
			// ファイル名作成
			pdfname = PathUtil.makeFileName("pdf", "univ", sDivisionCD+area[0]);
			file1 = new File(filepath + pdfname);
			bool = new boolean[1];
			bool[0] = file1.exists();
			// ファイル有無確認情報をセット
			list.add(bool);
		}

		// 全国版は特別処理
		//ファイル名作成
		// 短大の場合
		if ("03".equals(sDivisionCD)) {
			pdfname = PathUtil.makeFileName("pdf", "univ", sDivisionCD+"00");
		}
		// 短大以外
		else {
			pdfname = PathUtil.makeFileName("zip", "univ", sDivisionCD+"00pdf");
		}
		file1 = new File(filepath + pdfname);
		bool = new boolean[2];
		bool[0] = file1.exists();
		// ファイル有無確認情報をセット
		list.add(bool);

		return list;
	}
	
	/**
	 * 高校別成績データが存在するか（学年別）
	 * @param sRoot
	 * @param sYear
	 * @param sSchoolCD
	 * @param sGradeCD
	 * @param lexam
	 * @param lcb
	 * @return
	 */
	public ArrayList checkFileExistForF005_1(String sRoot, String sYear, String sSchoolCD, String sGradeCD, ArrayList lexam, ArrayList lbc) {
		
		ArrayList list = new ArrayList();
		ExamData data = null;
		boolean[] bool = null; 
		File file1 = null;
		String filepath = null;
		String pdfname = null;

		for (int i = 0; i < lexam.size(); i++) {
			data = (ExamData)lexam.get(i);
			// ファイル名作成
			filepath = PathUtil.getHSRootPath(sRoot, sYear, sSchoolCD, sGradeCD);
			pdfname = PathUtil.makeFileName("csv", "hs", sSchoolCD+sGradeCD+data.getExamCD());
			file1 = new File(filepath + pdfname);
			bool = new boolean[1];
			bool[0] = file1.exists();
			// ファイル有無確認情報をセット
			list.add(bool);
			// 一括コード分を検索
			for (int j = 0; j < lbc.size(); j++) {
				String[] sbc = (String[])lbc.get(j);
				filepath = PathUtil.getHSRootPath(sRoot, sYear, sbc[0], sGradeCD);
				pdfname = PathUtil.makeFileName("csv", "hs", sbc[0]+sGradeCD+data.getExamCD());
				file1 = new File(filepath + pdfname);
				bool = new boolean[1];
				bool[0] = file1.exists();
				// ファイル有無確認情報をセット
				list.add(bool);
			}
		}
		return list;
	}

	/**
	 * 高校別成績データが存在するか（日付順）
	 * @param sRoot
	 * @param sYear
	 * @param sSchoolCD
	 * @param lexam
	 * @param lbc
	 * @return
	 */
	public ArrayList checkFileExistForF005_2(String sRoot, String sYear, String sSchoolCD, List lexam, List lbc) {
		
		ArrayList list = new ArrayList();
		ExamData data = null;
		boolean[] bool = null; 
		File file1 = null;
		String filepath = null;
		String pdfname = null;
		
		for (int i = 0; i < lexam.size(); i++) {
			data = (ExamData)lexam.get(i);
			// ファイル名作成
			filepath = PathUtil.getHSRootPath(sRoot, sYear, sSchoolCD, data.getTargetGrade());
			pdfname = PathUtil.makeFileName("csv", "hs", sSchoolCD+data.getTargetGrade()+data.getExamCD());
			file1 = new File(filepath + pdfname);
			bool = new boolean[1];
			bool[0] = file1.exists();
			// ファイル有無確認情報をセット
			list.add(bool);
			// 一括区分コードを検索
			for (int j = 0; j < lbc.size(); j++) {
				String[] sbc = (String[])lbc.get(j);
				filepath = PathUtil.getHSRootPath(sRoot, sYear, sbc[0], data.getTargetGrade());
				pdfname = PathUtil.makeFileName("csv", "hs", sbc[0]+data.getTargetGrade()+data.getExamCD());
				file1 = new File(filepath + pdfname);
				bool = new boolean[1];
				bool[0] = file1.exists();
				// ファイル有無確認情報をセット
				list.add(bool);
			}
		}
		
		return list;
	}
	
	/**
	 * 引数の年度、模試コードの帳票が存在する
	 * @param year
	 * @param examCD
	 * @return
	 */
	public boolean isExistExam(String year, String examCD, Map examMap) {
		String[] buf = (String[])examMap.get(year);
		if (buf == null) {
			return false;
		}
		for (int i = 0; i < buf.length; i++) {
			if (buf[i].equals(examCD)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * ディレクトリパスから対象年度を取得する
	 * @param path Rootパスを取得
	 * @param flag ファイルパターン判別フラグ
	 */
	public ArrayList getExistYear(String path) {

		StringBuffer buf = new StringBuffer(path);
		File f = new File(buf.toString());
		// カレントに存在するフォルダ or ファイルを取得
		String[] lst = f.list();
		ArrayList tmp = new ArrayList();

		for (int i = 0; i < lst.length; i++) {
			try {
				Integer.parseInt(lst[i]);
				tmp.add(lst[i]);
			} catch (NumberFormatException e) {}
		}
		
		//ソート
		Collections.sort(tmp);
		// 降順
		Collections.reverse(tmp);
		
		return tmp;
	}

	/**
	 * 調査票ダウンロード画面のダウンロードファイルが存在するかどうか
	 * @param sRoot
	 * @param sUserCD
	 * @param lbundleCD
	 */
	public ArrayList checkFileExistForF006(String sRoot, String sUserCD, List lbundleCD) {

		ArrayList anslist = new ArrayList();
		
		String filepath = PathUtil.getResearchRootPath(sRoot, sUserCD);
		String filename = null;
		filename = PathUtil.makeResearchFileName("txt", null, sUserCD);
		File f = new File(filepath + filename);
		anslist.add(new Boolean(f.exists()));
		
		Iterator it = lbundleCD.iterator();
		while(it.hasNext()) {
			String[] bundleCD = (String[])it.next();
			filename = PathUtil.makeResearchFileName("txt", null, bundleCD[0]);
			f = new File(filepath + filename);
			anslist.add(new Boolean(f.exists()));
		}
		
		return anslist;
	}
	
	/**
	 * 少なくともひとつはファイルが存在する
	 * @param lFileExist1
	 */
	public Boolean isAtLeastExist(ArrayList lFileExist) {
		
		Iterator it = lFileExist.iterator();
		while (it.hasNext()) {
			boolean[] b = (boolean[])it.next();
			if (b[0]) {
				return new Boolean(true);
			}
		}
		return new Boolean(false);
	}
}

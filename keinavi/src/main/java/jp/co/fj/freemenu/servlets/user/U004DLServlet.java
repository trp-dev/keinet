package jp.co.fj.freemenu.servlets.user;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.freemenu.beans.user.DLLoginUserUpdateBean;
import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.user.LoginUserData;
import jp.co.fj.keinavi.forms.user.U004Form;
import jp.co.fj.keinavi.servlets.user.U002Servlet;

/**
 *
 * 利用者管理−登録完了画面サーブレット
 * ※DLサービス校用
 * 
 * 2008.12.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U004DLServlet extends U002Servlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// 転送元がu001なら参照モード
		if ("u001_dl".equals(getBackward(request))) {
		
			// HTTPセッション
			final HttpSession session = request.getSession(false);
			// ログインセッション
			final LoginSession login = getLoginSession(request);
			// アクションフォーム
			final U004Form form = (U004Form) getActionForm(
					request, "jp.co.fj.keinavi.forms.user.U004Form");
			// 利用者データをセッションに入れる
			session.setAttribute(LoginUserData.SESSION_KEY, 
					getLoginUserData(request, login, form.getTargetLoginId()));

			forward2Jsp(request, response);
		
		// 転送先がJSP
		} else if ("u004_dl".equals(getForward(request))) {
			
			// HTTPセッション
			final HttpSession session = request.getSession(false);
			// ログインセッション
			final LoginSession login = getLoginSession(request);
			// 利用者データ
			final LoginUserData data = (LoginUserData) session.getAttribute(LoginUserData.SESSION_KEY);
			
			try {
				// 更新処理
				update(request, login, data);
				//	アクセスログ
				actionLog(request, "702");
			// 認識しているエラー
			} catch (final KNBeanException e) {
				request.setAttribute("ErrorMessage", e.getMessage());
				forward2Jsp(request, response, "u002_dl");
				return;
			}
			
			forward2Jsp(request, response);
			
		// 不明なら転送
		} else {
			dispatch(request, response);
		}
	}
	
	/**
	 * 更新処理
	 * 
	 * @param request
	 * @throws ServletException
	 */
	private void update(final HttpServletRequest request,
			final LoginSession login,
			final LoginUserData data) throws ServletException, KNBeanException {
	
		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);
			
			final DLLoginUserUpdateBean bean = new DLLoginUserUpdateBean(
					login.getUserID(), data);
			bean.setConnection(null, con);
			bean.execute();
			con.commit();
			
			// 自分ならセッション情報書き換え
			if (!login.isHelpDesk() && login.getAccount().equals(data.getOriginalLoginId())) {
				login.setAccount(data.getLoginId());
			}
			
		// 認識しているエラー
		} catch (final KNBeanException e) {
			rollback(con);
			throw e;
		// それ以外のエラー
		} catch (final Exception e) {
			rollback(con);
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);				
		}
	}

}

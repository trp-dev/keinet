/*
 * 作成日: 2004/09/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.forms;

import com.fjh.forms.ActionForm;



/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class F008Form extends ActionForm {

	private String filename = null;
	private byte[] file;
	private String forward = null;
	private String backward = null;
	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ

	}


	/**
	 * @return
	 */
	public byte[] getFile() {
		return file;
	}

	/**
	 * @return
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @param bs
	 */
	public void setFile(byte[] bs) {
		file = bs;
	}

	/**
	 * @param string
	 */
	public void setFilename(String string) {
		filename = string;
	}

	/**
	 * @return
	 */
	public String getBackward() {
		return backward;
	}

	/**
	 * @return
	 */
	public String getForward() {
		return forward;
	}

	/**
	 * @param string
	 */
	public void setBackward(String string) {
		backward = string;
	}

	/**
	 * @param string
	 */
	public void setForward(String string) {
		forward = string;
	}

}

/*
 * 作成日: 2004/09/09
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.util;
import java.util.LinkedList;
import java.util.Iterator;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class DownloadUtil {
	
	// 拡張子contentType対応テーブル
	private final static String contentTypeTable[][] = {
	{"pdf", "message/pdf"},
	{"csv", "text/csv"},
	{"txt", "text/plain"}
	};
	private final static int EXTENTION=0;
	private final static int CONTENT_TYPE=1;
	
	private static String headerName = "Content-disposition";
	private static String headerValue	= "attachment; filename=";

	private String delcmd = "DEL";	// ファイル削除コマンド
	private String retOK = "OK";	// 通信結果（正常）
	private String retNG = "NG";	// 通信結果（異常）

	private static int noerror = 0;		// 正常終了
	private static int errcom = 1;		// 通信エラー

	/*
	 * 	ファイルダウンロード
	 * 		HttpServletResponse response: サーブレット応答
	 * 		String downfile: ダウンロードファイル名（フルパス）
	 * 		String downname: ダウンロードファイル名
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public static int downFile(HttpServletResponse response, String downfile, String downname) {

		// 拡張子からContentTypeを獲得
		String contentType = getContentType(downname);
		// contentTypeを出力
		response.setContentType(contentType);
		// ファイル名の送信
		response.setHeader(headerName, headerValue + "\"" + downname + "\"");

		try {	
			FileInputStream fin = new FileInputStream(downfile);
			BufferedInputStream bin = new BufferedInputStream(fin);
			BufferedOutputStream bout = new BufferedOutputStream(response.getOutputStream());
		
			int data;
			while((data = bin.read()) != -1) {
				bout.write(data);
			}
				
			bin.close();
			bout.close();
		} catch (IOException e) {
			return errcom;
		}

		return noerror;

	}

	/*
 	 * @author A.Ninomiya
	 *
	 * 	データダウンロード
	 * 		HttpServletResponse response: サーブレット応答
	 * 		LinkedList dataList: ダウンロードデータリスト
	 * 		String downname    : ダウンロードファイル名
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public static int downData(HttpServletResponse response, LinkedList dataList, String downname ) {

		// 拡張子からContentTypeを獲得
		String contentType = getContentType(downname);
		// contentTypeを出力
		response.setContentType(contentType);
		// ファイル名の送信
		response.setHeader(headerName, headerValue + "\"" + downname + "\"");

		try {	
			BufferedOutputStream bout = new BufferedOutputStream(response.getOutputStream());
		
			byte[] bytData = null;
			Iterator ite = dataList.iterator();
			while (ite.hasNext()) {
				bytData = (ite.next().toString()).getBytes("MS932");
				for (int i=0; i < bytData.length; i++){
					bout.write(bytData[i]);
				}
			}
			bout.close();
		} catch (IOException e) {
			return errcom;
		}
		return noerror;
	}

	
	/**
	 * ファイル名によって、Content-Typeを変更する
	 * @param fileName
	 * @return
	 */
	private static String getContentType(String fileName) {
		String extention = getExtention(fileName);
		for (int j=0; j < contentTypeTable.length; j++) {
			if (contentTypeTable[j][EXTENTION].equalsIgnoreCase(extention)) {
				return contentTypeTable[j][CONTENT_TYPE];
			}
		}
		return "application/octet-stream";
	}
	
	/**
	 * ファイル名から拡張子を取り出す
	 * @param fileName
	 * @return
	 */
	private static String getExtention(String fileName) {
		int idx = fileName.lastIndexOf('.');
		if (idx!=-1) return fileName.substring(idx+1, fileName.length());
	 	return "";
	}


	
}

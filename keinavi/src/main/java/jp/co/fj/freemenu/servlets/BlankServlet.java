/*
 * 作成日: 2004/10/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.servlets.DefaultLoginServlet;
import jp.co.fj.keinavi.util.KNCommonProperty;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class BlankServlet extends DefaultLoginServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ
		
		String guestUID = null;
		// CommonリソースからguestユーザのuserIDを取得する
		try {
			guestUID = KNCommonProperty.getTrialUserID();
		} catch (Exception e) {
			throw new ServletException(e.getMessage());
		}
		request.setAttribute("guestUID", guestUID);
		// Blankページへ遷移
		super.forward(request, response, JSP_TrialLogin);
	}

}

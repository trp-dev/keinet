/*
 * 作成日: 2004/09/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.beans;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.fjh.beans.DefaultBean;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class BundlecdBean extends DefaultBean {

	private List bundle = new ArrayList();
	private String[] data = null;
	private String schoolcd = null;
	private int length;
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// TODO 自動生成されたメソッド・スタブ
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ps = conn.prepareStatement(getSQL(schoolcd));
		rs = ps.executeQuery();
		
		while(rs.next()) {
			data = new String[2];
			data[0] = rs.getString(1);
			data[1] = rs.getString(2);
			bundle.add(data);
		}
		// Listのサイズ
		length = bundle.size();
		
		if (ps != null) {ps.close();}
		if (rs != null) {rs.close();}
	}
	
	/**
	 * 一括コードを得るSQLを取得
	 * @param schlcd
	 * @return
	 */
	private String getSQL(String schlcd) {
		
		StringBuffer sql = new StringBuffer();
		
		sql.append(" SELECT S.BUNDLECD, S.BUNDLENAME FROM SCHOOL S ");
		sql.append(" WHERE S.PARENTSCHOOLCD = ");
		sql.append(schlcd);
		sql.append(" ORDER BY BUNDLECD ");
		
		return sql.toString();
	}

	/**
	 * @return
	 */
	public List getBundle() {
		return bundle;
	}

	/**
	 * @return
	 */
	public String[] getData() {
		return data;
	}

	/**
	 * @return
	 */
	public String getSchoolcd() {
		return schoolcd;
	}

	/**
	 * @param list
	 */
	public void setBundle(List list) {
		bundle = list;
	}

	/**
	 * @param strings
	 */
	public void setData(String[] strings) {
		data = strings;
	}

	/**
	 * @param string
	 */
	public void setSchoolcd(String string) {
		schoolcd = string;
	}

	/**
	 * @return
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @param i
	 */
	public void setLength(int i) {
		length = i;
	}

}

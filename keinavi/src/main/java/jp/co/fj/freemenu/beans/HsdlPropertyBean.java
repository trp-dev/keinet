/*
 * 作成日: 2004/09/02
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.beans;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;


import com.fjh.beans.DefaultBean;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class HsdlPropertyBean extends DefaultBean {
	
	// ダウンロードファイルリンク名
	private ArrayList ldlfname = new ArrayList();
	// 公開開始日と公開終了日
	private String[] date = new String[2];
	// アップロード終了通知メールアドレス
	private String address = new String();
	// システムファイル名
	private ArrayList lsystemfile = new ArrayList();
	// システムファイルリンク名
	private ArrayList llsystemfile = new ArrayList();
	// 一括コード
	private ArrayList lhscd = new ArrayList();
	// 高校コード
	private String hscd = null;
	// ファイルパス
	private String path = null;
	// 現在公開日かどうか
	private boolean open = true;
	// リンク名とリンクファイルのセット
	private String system[] = null;
	// セット格納用List
	private ArrayList lsol = new ArrayList();
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// TODO 自動生成されたメソッド・スタブ
		
		// 一括コード取得
		StringBuffer query = new StringBuffer();
		query.append("select bundlecd from SCHOOL where PARENTSCHOOLCD = ?");
		PreparedStatement ps = conn.prepareStatement(query.toString());
		ps.setString(1, hscd);
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			lhscd.add(rs.getString(1));
		}
		ps.close();
		rs.close();
		
		// パラメータファイルから情報取得
		fromParameterFile(path);
		// リンクファイル名とシステムファイル名の数が不一致
		if (llsystemfile.size() != lsystemfile.size()) {
			throw new Exception();
		}
		else {
			for (int i = 0; i < llsystemfile.size(); i++) {
				system = new String[2];
				system[0] = (String)llsystemfile.get(i);
				system[1] = (String)lsystemfile.get(i);
				lsol.add(system);
			}
		}
		// 公開日内か判定
		judgeOpen(date);
	}

	/**
	 * 公開日かどうか判定
	 * @param date
	 */
	private void judgeOpen(String[] date) throws Exception{
		// TODO 自動生成されたメソッド・スタブ
		DateFormat df = DateFormat.getInstance();
		Date beforeDate = null;
		Date afterDate = null;
		// 現在の日付を取得
		Date nowDate = new Date();

		//開始・終了日の何れかがnullならNG
		if((date[0]==null) || (date[1]==null)){
			open = false;
			return;
		}

		beforeDate = df.parse(date[0]);
		afterDate = df.parse(date[1]);

		//日付の計算(現在表示期間内かどうか)
		if (beforeDate.after(nowDate)) {
			open = false;
			return;
		}
		if (afterDate.before(nowDate)) {
			open = false;
			return;
		}
		open = true;
	}

	/**
	 * パラメータファイル格納パス
	 * @param path
	 */
	private void fromParameterFile(String path) throws IOException {
		// TODO 自動生成されたメソッド・スタブ
		ArrayList list = new ArrayList();
		BufferedReader br = null;
		try {
			br =
				new BufferedReader(
					new InputStreamReader(new FileInputStream(path)));
					
			String line = null;
			// ファイル読み込み
			while ((line = br.readLine()) != null) {
				//list.add(line);
				if (line.indexOf("#") == 0) {
					continue;
				}
			
				if (line.indexOf("UseStartDate") >= 0) {
					date[0] = line.substring(line.indexOf("=") + 1, line.length());
				}
				else if(line.indexOf("UseEndDate") >= 0){
					date[1] = line.substring(line.indexOf("=") + 1, line.length());
				}
				else if(line.indexOf("NoticeAddr") >= 0) {
					setAddress(line.substring(line.indexOf("=") + 1, line.length()));
				}
				else if(line.indexOf("DataFileLink") >= 0){
					ldlfname.add(line.substring(line.indexOf("=") + 1, line.length()));
				}
				else if(line.indexOf("SystemFileName") >= 0) {
					lsystemfile.add(line.substring(line.indexOf("=") + 1, line.length()));
				}
				else if(line.indexOf("SystemFileLink") >= 0){
					llsystemfile.add(line.substring(line.indexOf("=") + 1, line.length()));
				}
				else {
					// ごみ情報は除く
				}
			}
		} catch (FileNotFoundException e) {
			//e.printStackTrace();

		} finally {
			if (br != null) br.close();
		}
	}

	/**
	 * @return
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @return
	 */
	public String[] getDate() {
		return date;
	}

	/**
	 * @return
	 */
	public ArrayList getLdlfname() {
		return ldlfname;
	}

	/**
	 * @return
	 */
	public ArrayList getLsystemfile() {
		return lsystemfile;
	}

	/**
	 * @param string
	 */
	public void setAddress(String string) {
		address = string;
	}

	/**
	 * @param strings
	 */
	public void setDate(String[] strings) {
		date = strings;
	}

	/**
	 * @param list
	 */
	public void setLdlfname(ArrayList list) {
		ldlfname = list;
	}

	/**
	 * @param list
	 */
	public void setLsystemfile(ArrayList list) {
		lsystemfile = list;
	}

	/**
	 * @return
	 */
	public ArrayList getLhscd() {
		return lhscd;
	}

	/**
	 * @param list
	 */
	public void setLhscd(ArrayList list) {
		lhscd = list;
	}

	/**
	 * @return
	 */
	public String getHscd() {
		return hscd;
	}

	/**
	 * @param string
	 */
	public void setHscd(String string) {
		hscd = string;
	}

	

	/**
	 * @return
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param string
	 */
	public void setPath(String string) {
		path = string;
	}

	/**
	 * @return
	 */
	public ArrayList getLlsystemfile() {
		return llsystemfile;
	}

	/**
	 * @param list
	 */
	public void setLlsystemfile(ArrayList list) {
		llsystemfile = list;
	}

	/**
	 * @return
	 */
	public boolean isOpen() {
		return open;
	}

	/**
	 * @param b
	 */
	public void setOpen(boolean b) {
		open = b;
	}

	/**
	 * @return
	 */
	public ArrayList getLsol() {
		return lsol;
	}

	/**
	 * @return
	 */
	public String[] getSystem() {
		return system;
	}

	/**
	 * @param list
	 */
	public void setLsol(ArrayList list) {
		lsol = list;
	}

	/**
	 * @param strings
	 */
	public void setSystem(String[] strings) {
		system = strings;
	}

}

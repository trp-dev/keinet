package jp.co.fj.freemenu.util;

import java.io.File;

/**
 *
 * @author Totec) F.Urakawa
 *
 * 2005.08.12 	Totec) T.Yamada 	[1]F002_5追加
 * 2020.02.14	FJ）N.Tanaka	調査用ファイル名変更
 *
 */
public class PathUtil {

	/**
	 * 高校別成績データのルートパス取得
	 * @param sRoot
	 * @param sYear
	 * @param sSchoolCD
	 * @param sGrade
	 * @return
	 */
	public static String getHSRootPath(String sRoot, String sYear, String sSchoolCD, String sGradeCD) {

		//---------------------------------------------------------------
		// [2004/11/02 nino] 高校別成績データについては直接データを
		//                   ダウンロードする方式に変更
		//---------------------------------------------------------------
		StringBuffer sPath = new StringBuffer();
		sPath.append(sRoot);
		// [2004/11/10 nino] "freemenu"までpathで設定
		//sPath.append("freemenu");
		//sPath.append(File.separator);
		sPath.append(sYear);
		sPath.append(File.separator);
		sPath.append("highschool");
		sPath.append(File.separator);
		sPath.append(sSchoolCD);
		sPath.append(File.separator);
		sPath.append(sGradeCD);
		sPath.append(File.separator);

		return sPath.toString();
	}

	/**
	 * 大学・学部・学科別学力分布のルートパス取得
	 * @param sRoot
	 * @param sYear
	 * @param sDivisionCD
	 * @return
	 */
	public static String getDistRootPath(String sRoot, String sYear, String sDivisionCD) {

		StringBuffer sPath = new StringBuffer();
		sPath.append(sRoot);
		//sPath.append("freemenu");
		//sPath.append(File.separator);
		//---------------------------------------------------------------
		// [2004/11/02 nino] フォルダ構成変更
		// FLHOME/university/year(4桁)/大学区分コード/
		//---------------------------------------------------------------
		//sPath.append(sYear);
		//sPath.append(File.separator);
		//sPath.append("university");
		//sPath.append(File.separator);
		//sPath.append(sDivisionCD);
		//sPath.append(File.separator);
		//---------------------------------------------------------------
		sPath.append("university");
		sPath.append(File.separator);
		sPath.append(sYear);
		sPath.append(File.separator);
		sPath.append(sDivisionCD);
		sPath.append(File.separator);
		//---------------------------------------------------------------

		return sPath.toString();
	}

	/**
	 * ファイル名作成
	 * @param type
	 * @param head
	 * @param cd
	 * @return
	 */
	public static String makeFileName(String type, String head, String cd) {
		// TODO 自動生成されたメソッド・スタブ
		return head + cd + "." + type;
	}

	/**
	 * 調査票データルートパス
	 * @param rootresearch
	 * @param string
	 * @return
	 */
	public static String getResearchRootPath(String rootresearch, String string) {
		// TODO 自動生成されたメソッド・スタブ
		StringBuffer sPath = new StringBuffer();

		sPath.append(rootresearch);
		//sPath.append(File.separator);
		if ("exe".equals(string)) {
			//sPath.append("freemenu");
			//sPath.append(File.separator);
			sPath.append("data");
			sPath.append(File.separator);
			sPath.append("system");
		}
		else {
			//sPath.append("freemenu");
			//sPath.append(File.separator);
			sPath.append("data");
			sPath.append(File.separator);
			sPath.append("format");
		}
		sPath.append(File.separator);

		return sPath.toString();
	}

	/**
	 * 調査用ファイル名作成
	 * @param string
	 * @param form
	 * @return
	 */
	public static String makeResearchFileName(String sType, String sName, String sSchoolCD) {
		// TODO 自動生成されたメソッド・スタブ
		String tmp = "";
		if ("exe".equals(sType)) {
			tmp = sName;
		}
		else {
//			tmp = "es" + sSchoolCD + ".txt";

//2020.02.14	調査用ファイル名変更 MOD STA
//			tmp = "ES" + sSchoolCD + ".zip";
			tmp = sSchoolCD + ".zip";
//2020.02.14	調査用ファイル名変更 MOD END
		}
		return tmp;
	}


}
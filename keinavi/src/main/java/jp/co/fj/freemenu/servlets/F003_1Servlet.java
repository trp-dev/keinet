/*
 * 作成日: 2004/09/02
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.servlets;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.freemenu.beans.AreaBean;
import jp.co.fj.freemenu.forms.F003_1Form;
import jp.co.fj.freemenu.util.FileExistenceUtil;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.KNCommonProperty;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class F003_1Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ
		//セッション
		HttpSession session = request.getSession();
		//ログインユーザの地区コードを取得
		AreaBean ab = new AreaBean();
		//ログインセッション
		LoginSession ls = super.getLoginSession(request);
		ExamData data = null;
		Connection con = null;
		// アクションフォーム
		F003_1Form form = null;
		// ファイル存在確認
		ArrayList FileExist = null;
		
		try {
			form = (F003_1Form)super.getActionForm(request,
			"jp.co.fj.freemenu.forms.F003_1Form");
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			throw new ServletException(e);
		}
		
		if ("f003_1".equals(super.getForward(request))) {
			try {
				con = super.getConnectionPool(request);
				ab.setConnection(null, con);
				ab.setSchoolID(ls.getUserID());
				ab.execute();
				// 大学・学部・学科別の時のみ地区に「東京」を加える
				ArrayList al = (ArrayList)ab.getAreaData();
				Iterator it = al.iterator();
				int i = 0;
				while(it.hasNext()) {
					String[] ad = (String[])it.next();
					i++;
					if ("関東地区".equals(ad[1])) {
						String[] str = {"11","東京地区"};
						al.add(i, str);
						break;
					}
					
				}
				// 編集後のAreaDataをセット
				ab.setAreaData(al);
				
				// 最新の年度データを取得
				ExamSession es = new ExamSession();
				// 模試データを取得
				
				// ------------------------------------------------------------
				// 2005.01.13
				// Yoshimoto KAWAI - Totec
				// 現在の年度はファイルが存在する年度で最新のもの
				//今日の日付
				//String today = new MiddleDateUtil().date2String();
				// 現在の年度
				//String current = today.substring(0, 4);
				//form.setTargetYear(current);
				String sRoot = KNCommonProperty.getDLSDataPath();
				{
					File dir = new File(sRoot + File.separator + "university");
					if (dir.isDirectory()) {
						File[] list = dir.listFiles();
						// データがなければとりあえずダミー
						if (list.length == 0) {
							form.setTargetYear("9999");
						} else {
							List container = new ArrayList();
							for (int j=0; j<list.length; j++) {
								if (list[j].isDirectory())
									container.add(list[j].getName());
							}
							Collections.sort(container);
							form.setTargetYear(
									(String) container.get(container.size() - 1));
						}
					
					// ディレクトリでなければ例外
					} else {
						throw new InternalError(
								"DLSDataPathを正しく設定してください。: " + sRoot);
					}
				}
				// ------------------------------------------------------------
				
				//ファイル存在確認
				FileExistenceUtil feutil = new FileExistenceUtil();
				FileExist = new ArrayList();
				FileExist = feutil.checkFileExistForF003_1(sRoot, form.getTargetYear(), form.getDivision(), ab);
				
				
			} catch (Exception e) {
				// TODO 自動生成された catch ブロック
				throw new ServletException(e);
			}
			finally {
				super.releaseConnectionPool(request, con);
			}
			request.setAttribute("form", form);
			request.setAttribute("FileExist",FileExist);
			session.setAttribute("AreaBean", ab);
			super.forward(request, response, JSP_F003_1);
		}
		else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
		
	}

}

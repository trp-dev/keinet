/*
 * 作成日: 2004/08/23
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.freemenu.beans.BundlecdBean;
import jp.co.fj.freemenu.util.FileExistenceUtil;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.KNCommonProperty;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class F006Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ
		
		LoginSession ls = super.getLoginSession(request);
		Connection con = null;
		HttpSession session = request.getSession();
		BundlecdBean bcb = null;
		/*
		// 調査票DLルートパスを取得
		try {
			session.setAttribute("sDLResRoot",KNCommonProperty.getDLSPOBDataPath());
		} catch (Exception e1) {
			// TODO 自動生成された catch ブロック
			throw new ServletException(e1);
		}
		*/
		try {
			// sessionにBCBeanが格納されていない場合、一括コードを取得
			con = super.getConnectionPool(request);
			con.setAutoCommit(false);
			bcb = new BundlecdBean();
			bcb.setConnection(null, con);
			bcb.setSchoolcd(ls.getUserID());
			bcb.execute();
			session.setAttribute("BCBean", bcb);
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			throw new ServletException(e);
		}
		finally {
			super.releaseConnectionPool(request, con);
		}
		
		String sRoot = null;
		try {
			sRoot = KNCommonProperty.getDLSPOBDataPath();
		} catch (Exception e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}
		
		FileExistenceUtil feu = new FileExistenceUtil();
		ArrayList lexist = feu.checkFileExistForF006(sRoot, ls.getUserID(), bcb.getBundle());
		request.setAttribute("FileExist",lexist);
		
		if ("f006".equals(super.getForward(request))) {
			super.forward(request, response, JSP_F006);
		}
		else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

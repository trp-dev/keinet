package jp.co.fj.freemenu.beans.f002;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.freemenu.beans.CourseSubjectBean;
import jp.co.fj.freemenu.util.PathUtil;

/**
 *
 * 成績統計資料集ダウンロード用のBean
 * 高校別設問別成績一覧表
 * 
 * 2008.03.05	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class F002_3Bean extends F002_2Bean {

	// 既存モジュール：型・科目情報Bean
	private CourseSubjectBean bean;
	
	// ファイル存在フラグ保持マップ
	// key ... 科目コード
	// value ... Boolean
	private Map fileMap;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		bean = new CourseSubjectBean();
		bean.setConnection(null, conn);
		bean.setYear(getTargetExamYear());
		bean.setExamcd(getTargetExamCd());
		bean.execSubjectData();

		fileMap = new HashMap();

		// 地区コードまでのディレクトリ
		final File dir = getDistDir();
		
		// 科目をチェック
		for (Iterator ite = getSubjectList().iterator(); ite.hasNext();) {
			final String[] data = (String[]) ite.next();
			final String subCd = data[0];
			final File file = new File(dir, createFileName("pdf", subCd));
			fileMap.put(subCd, Boolean.valueOf(file.exists()));
		}
		
		// 全科目は特別処理
		final File file = new File(dir, createFileName("zip", "0000pdf"));
		fileMap.put("0000", Boolean.valueOf(file.exists()));
	}
	
	/**
	 * @see jp.co.fj.freemenu.beans.f002.F002_2Bean#createFileName(
	 * 			java.lang.String, java.lang.String)
	 */
	protected String createFileName(final String type, final String subCd) {
		
		return PathUtil.makeFileName(type, "que",
				getTargetExamCd() + getTargetDistCd() + subCd);
	}
	
	/**
	 * @return fileMap
	 */
	public Map getFileMap() {
		return fileMap;
	}

	/**
	 * @return 科目情報リスト
	 */
	public List getSubjectList() {
		return bean.getLcs();
	}

	/**
	 * @see jp.co.fj.freemenu.beans.f002.F002Bean#needConnection()
	 */
	public boolean needConnection() {
		return true;
	}

}

/*
 * 作成日: 2004/09/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
//import java.io.File;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class UploadUtil {

	/**
	 * @param bs
	 * @param filepath
	 * @param rtpath
	 */
	public synchronized String makeUploadFile(byte[] bs, String filepath, String rtpath, String sSchoolCD) throws Exception{
		// TODO 自動生成されたメソッド・スタブ
		String filename = getFileName(filepath);
		FileOutputStream out = null;
		String forward = "";
		try {
			// ファイルサイズ＝０
			if (bs.length == 0) {
				return "f009_2";
			}
			File f = new File(rtpath + sSchoolCD);
			f.mkdirs();
			out = new FileOutputStream(f.getPath() + File.separator + filename);
			out.write(bs);
			forward = "f008";
		} catch (FileNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			forward = "f009_1";
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			forward = "f009_1";
		}
		finally {
			if (out != null) {out.close();}
		}
	
		return forward;
		
	}

	/**
	 * ファイルパスからファイル名を取得する
	 * @param filepath
	 * @return
	 */
	public String getFileName(String filepath) {
		// TODO 自動生成されたメソッド・スタブ
		//String separator = System.getProperty("file.separator");
		int pos = filepath.lastIndexOf("\\");
		if (pos == -1) {
			pos = filepath.lastIndexOf("/");
		}
		return filepath.substring(pos+1, filepath.length());
	}

	

}

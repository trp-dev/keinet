package jp.co.fj.freemenu.servlets.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.user.LoginUserData;
import jp.co.fj.keinavi.forms.user.U002Form;
import jp.co.fj.keinavi.servlets.user.U002Servlet;

/**
 *
 * 利用者管理−登録編集画面サーブレット
 * ※DLサービス校用
 * 
 * 2008.12.24	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U002DLServlet extends U002Servlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		// HTTPセッション
		final HttpSession session = request.getSession(false);
		// アクションフォーム
		final U002Form form = (U002Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.user.U002Form");
		
		// 転送元がJSP
		if ("u002_dl".equals(getBackward(request))) {
			// フォームの値をセッションに保持する
			refrectFormValue((LoginUserData) session.getAttribute("LoginUserData"), form);
		}
		
		// 転送先がJSP
		if ("u002_dl".equals(getForward(request))) {
			
			// ログインセッション
			final LoginSession login = getLoginSession(request);

			// 転送元が一覧画面であるなら初期化処理をする
			if ("u001_dl".equals(getBackward(request))) {
				
				// 編集しかないのでDBからとってくる
				final LoginUserData data = getLoginUserData(
						request, login, form.getTargetLoginId());
				
				// セッションに入れる
				session.setAttribute(LoginUserData.SESSION_KEY, data);
			}
			
			request.setAttribute("form", form);
			
			forward2Jsp(request, response);
		
		// 不明なら転送
		} else {
			dispatch(request, response);
		}
	}

}

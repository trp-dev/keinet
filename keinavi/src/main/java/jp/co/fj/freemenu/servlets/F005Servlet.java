/*
 * 作成日: 2004/08/23
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.servlets;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.freemenu.beans.ExamScoreBean;
import jp.co.fj.freemenu.data.ExamScoreSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.KNCommonProperty;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class F005Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// JSP
	    if ("f005".equals(super.getForward(request))) {

		    HttpSession session = request.getSession();
			LoginSession login = super.getLoginSession(request);

			Connection con = null;
			try {
				con = super.getConnectionPool(request);
				con.setAutoCommit(false);

				ExamScoreBean bean = new ExamScoreBean();
				bean.setConnection(null, con);
				bean.setSchoolCd(login.getUserID());
				bean.setNDataPath(KNCommonProperty.getExamScoreDLPath());
				bean.setCDataPath(KNCommonProperty.getCRExamScoreDLPath());
				// 2019/09/12 QQ)Ooseto 共通テスト対応 ADD START
				bean.setNAbilityDataPath(KNCommonProperty.getAbilityDLPath());
				bean.setCAbilityDataPath(KNCommonProperty.getCRAbilityDLPath());
				// 2019/09/12 QQ)Ooseto 共通テスト対応 ADD END
				bean.setHelpDesk(login.isHelpDesk());
				bean.execute();

				ExamScoreSession score = bean.getExamScoreSession();

				// 模試データがなければ前に戻る
				if (score.getYears().size() == 0) {
					request.setAttribute("NOTEXISTEXAM", "NOTEXISTEXAM");
					super.forward(request, response, JSP_F004);
					return;
				}

				// セッションに格納
				session.setAttribute(ExamScoreSession.SESSION_KEY, score);

			}catch (Exception e) {
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
			    //throw new ServletException(e.getMessage());
			    throw new ServletException(e);
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			} finally {
				super.releaseConnectionPool(request, con);
			}

			super.forward(request, response, JSP_F005);

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

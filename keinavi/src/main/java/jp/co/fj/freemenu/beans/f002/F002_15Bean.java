/**
 * 
 */
package jp.co.fj.freemenu.beans.f002;

import java.io.File;
import java.sql.SQLException;

import jp.co.fj.freemenu.util.PathUtil;

/**
 * 
 * 成績統計資料集ダウンロード用のBean
 * 都道府県別・科目別成績一覧表
 * 
 * 2008.04.15	[新規作成]
 * 
 * @author fujito-urakawa
 * @version 1.0
 *
 */
public class F002_15Bean extends F002Bean {

	// ファイル存在フラグ保持フラグ
	// value ... Boolean
	private Boolean exist;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		// 模試コードまでのディレクトリ
		final File dir = getExamDir();
		final File file = new File(dir, createFileName("pdf"));
		exist = Boolean.valueOf(file.exists());

	}
	
	/**
	 * @see jp.co.fj.freemenu.beans.f002.F002Bean#getDownloadFile()
	 */
	public File getDownloadFile() {

		// 模試コードまでのディレクトリ
		final File dir = getExamDir();
		
		final String fileName;
		fileName = createFileName("pdf");
		return new File(dir, fileName);
	}

	private String createFileName(String type) {
		
		return PathUtil.makeFileName(type, "TODOFUKEN_KAMOKU", getTargetExamCd());
	}


	/**
	 * ファイル存在フラグのGetter
	 * @return
	 */
	public Boolean getExist() {
		return exist;
	}

}

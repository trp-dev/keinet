/*
 * 作成日: 2004/09/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.beans;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.sql.SQLUtil;
import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CourseSubjectBean extends DefaultBean {
	
	private static final Properties props;
	
	static {
		props = new Properties();
		try {
			props.load(CourseSubjectBean.class.getClassLoader().getResourceAsStream(
					"jp/co/fj/freemenu/resources/pattern.properties"));
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	private final String[] sPattern1 = {"01","02","03","04","66"};
	private final String[] sPattern2 = {"05","06","07","10","67"};
	private final String[] sPattern3 = {"09"};
	private final String[] sPattern4 = {"61","62"};
	private final String[] sPattern5 = {"71","72","73","74","76"};
	private final String[] sPattern6 = {"63"};
	private final String[] sPattern7 = {"75"};
	private final String[] sPattern8 = {"65"};
	private final String[] sPattern9 = {"77"};
	
	// 入力：模試年度
	private String year = null;

	// 入力：模試コード
	private String examcd = null;
	
	// 入力：存在するファイルの科目コード（科目）
	private ArrayList tmpSubList = null;
	
	// 入力：存在するファイルの科目コード（型）
	private ArrayList tmpPatList = null;
	
	// 出力：教科・科目情報リスト
	private List lcs = null;
	
	// 出力：型情報リスト
	private List lp = null;

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		lcs = new ArrayList();
		lp = new ArrayList();
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(getCrsSubData());
			ps.setString(1, year);
			ps.setString(2, examcd);
			ps.setString(3, year);
			ps.setString(4, year);
			ps.setString(5, examcd);
			ps.setString(6, year);
			rs = ps.executeQuery();
			while (rs.next()) {
				// beanに詰める
				String[] lcsdata = new String[4];
				lcsdata[0] = rs.getString(1);
				lcsdata[1] = rs.getString(2);
				lcsdata[2] = rs.getString(3);
				lcsdata[3] = rs.getString(4);
				lcs.add(lcsdata);
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
		
		// 型情報を取得する
		ArrayList tmpList = new ArrayList();
		String PatternCode = (String) props.get("FMPATTERNCODE" + getSpecialPattern());
		String[] code = CollectionUtil.splitComma(PatternCode);
		// リストに科目コードをセット
		for (int i = 0; i < code.length; i++) {
			tmpList.add(code[i]);
		}
		
		try {
			ps = conn.prepareStatement(getPatternData(tmpList));
			ps.setString(1, year);
			ps.setString(2, examcd);
			rs = ps.executeQuery();
			while (rs.next()) {
				String[] lpdata = new String[2];
				lpdata[0] = rs.getString(1);
				lpdata[1] = rs.getString(2);
				lp.add(lpdata);
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	public void execSubjectData() throws Exception {
		
		lcs = new ArrayList();
		
		// 科目情報を取得する
		ArrayList tmpList = new ArrayList();
		
		String PatternCode = null;
		final String SpExamCD2 = (String) props.get("F002_3SPEXAMCD2");
		final String SpExamCD3 = (String) props.get("F002_3SPEXAMCD3");
		final String SpExamCD4 = (String) props.get("F002_3SPEXAMCD4");
		if (SpExamCD2.equals(examcd)) {
			PatternCode = (String)props.get("F002_3SUBJECTCD2");
		} else if (SpExamCD3.equals(examcd)) {
			PatternCode = (String)props.get("F002_3SUBJECTCD3");
		} else if (SpExamCD4.equals(examcd)) {
			PatternCode = (String)props.get("F002_3SUBJECTCD4");
		} else {
			PatternCode = (String)props.get("F002_3SUBJECTCD1");
		}
		String[] code = CollectionUtil.splitComma(PatternCode);
		// リストに科目コードをセット
		for (int i = 0; i < code.length; i++) {
			tmpList.add(code[i]);
		}
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(getSubjectData(tmpList));
			ps.setString(1, year);
			ps.setString(2, examcd);
			rs = ps.executeQuery();
			while (rs.next()) {
				// beanに詰める
				String[] lcsdata = new String[4];
				lcsdata[0] = rs.getString(1);
				lcsdata[1] = "";
				lcsdata[2] = rs.getString(2);
				lcsdata[3] = "";
				lcs.add(lcsdata);
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	/**
	 * F002_8、F002_9、F002_10、F002_11、F002_12、F002_13、F002_14
	 * の型・科目情報を取得する
	 * @throws Exception
	 */
	public void execute2() throws Exception {
		
		lcs = new ArrayList();
		lp = new ArrayList();
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		// 科目情報を取得する
		try {
			if (tmpSubList.size() > 0) {
				ps = conn.prepareStatement(getCrsSubData2(tmpSubList));
				ps.setString(1, year);
				ps.setString(2, examcd);
				ps.setString(3, year);
				ps.setString(4, year);
				ps.setString(5, examcd);
				ps.setString(6, year);
				rs = ps.executeQuery();
				while (rs.next()) {
					// beanに詰める
					String[] lcsdata = new String[4];
					lcsdata[0] = rs.getString(1);
					lcsdata[1] = rs.getString(2);
					lcsdata[2] = rs.getString(3);
					lcsdata[3] = rs.getString(4);
					lcs.add(lcsdata);
				}
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
		// 型情報を取得する
		try {
			if (tmpPatList.size() > 0) {
				ps = conn.prepareStatement(getPatternData(tmpPatList));
				ps.setString(1, year);
				ps.setString(2, examcd);
				rs = ps.executeQuery();
				while (rs.next()) {
					String[] lpdata = new String[2];
					lpdata[0] = rs.getString(1);
					lpdata[1] = rs.getString(2);
					lp.add(lpdata);
				}
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
		
	}


	/**
	 * ファイルが存在する科目情報のみを取得する
	 * @param tmpSubList2
	 * @return
	 */
	private String getCrsSubData2(ArrayList tmpSubList2) {
		
		final StringBuffer sql = new StringBuffer();
		
		sql.append(" select  a.subcd, a.newcourse, a.subname, b.count, a.newcoursecd, a.dispsequence from "); 
		sql.append(" ( ");
		sql.append(" select distinct examsubject.subcd,course.coursename ,examsubject.subname ,examsubject.subaddrname, course.coursecd," );
		sql.append(" DECODE(substr(examsubject.subcd,0,2), ");
		sql.append(" '10','外国語', ");
		sql.append(" '11','外国語', ");
		sql.append(" '12','外国語', ");
		sql.append(" '13','外国語', ");
		sql.append(" '14','外国語', ");
		sql.append(" '15','外国語', ");
		sql.append(" '16','外国語', ");
		sql.append(" '17','外国語', ");
		sql.append(" '18','外国語', ");
		sql.append(" '19','外国語', ");
		sql.append(" '51','公民', ");
		sql.append(" '52','地理歴史', ");
		sql.append(" '53','地理歴史', ");
		sql.append(" '54','地理歴史', ");
		sql.append(" '55','公民', ");
		sql.append(" '56','公民', ");
		sql.append(" '57','公民', ");
		sql.append(" course.coursename) \"NEWCOURSE\", ");
		sql.append(" DECODE(substr(examsubject.subcd,0,2), ");
		sql.append(" '51','5100', ");
		sql.append(" '52','5000', ");
		sql.append(" '53','5000', ");
		sql.append(" '54','5000', ");
		sql.append(" '55','5100', ");
		sql.append(" '56','5100', ");
		sql.append(" '57','5100', ");
		sql.append(" course.coursecd) \"NEWCOURSECD\", ");
		sql.append(" examsubject.dispsequence ");
 		sql.append(" from course,examsubject ");
 		sql.append(" where substr(examsubject.subcd,0,1) = substr(course.coursecd,0,1) ");
 		sql.append(" and examyear = ? ");
 		sql.append(" and examcd = ? ");
 		sql.append(" and subcd < '7000' ");
 		// ↓＃３ 2004.11.17 F.Urakawa 高１、プレの時には出さない科目を条件に加える
 		sql.append(getLimitSubcd2(tmpSubList2));
 		// ↑＃３ 2004.11.17 F.Urakawa 高１、プレの時には出さない科目を条件に加える
		sql.append(" and year= ? ");
		sql.append(" order by newcoursecd ");
		sql.append(" ) a ");
		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select NEWCOURSECD, count(NEWCOURSECD) as count from ( ");
		sql.append(" select distinct examsubject.subcd, ");
		sql.append(" course.coursename, ");
		sql.append(" examsubject.subname, ");
		sql.append(" course.coursecd, ");
		sql.append(" DECODE(substr(examsubject.subcd,0,2), ");
		sql.append(" '51','5100', ");
		sql.append(" '52','5000', ");
		sql.append(" '53','5000', ");
		sql.append(" '54','5000', ");
		sql.append(" '55','5100', ");
		sql.append(" '56','5100', ");
		sql.append(" '57','5100', ");
		sql.append(" course.coursecd) \"NEWCOURSECD\" ");
		sql.append(" from course,examsubject  ");
		sql.append(" where substr(examsubject.subcd,0,1) = substr(course.coursecd,0,1) ");
		sql.append(" and examyear = ? ");
		sql.append(" and examcd = ? ");
		sql.append(" and subcd < '7000' ");
		//↓＃３ 2004.11.17 F.Urakawa 高１、プレの時には出さない科目を条件に加える
		sql.append(getLimitSubcd2(tmpSubList2));
		// ↑＃３ 2004.11.17 F.Urakawa 高１、プレの時には出さない科目を条件に加える
		sql.append(" and year = ? ");
		sql.append(" order by course.coursecd) ");
		sql.append(" group by NEWCOURSECD ");
		sql.append(" ) b ");
		sql.append(" on a.newcoursecd=b.newcoursecd ");
		sql.append(" order by newcoursecd,dispsequence ");
		
		return sql.toString();
		
	}

	/**
	 * 科目コードのSQL条件文を作成する
	 * @param tmpSubList2
	 * @return
	 */
	private String getLimitSubcd2(ArrayList tmpSubList2) {
		
		StringBuffer query = new StringBuffer();
			
		query.append(" AND EXAMSUBJECT.SUBCD");
		query.append(" IN (");
		query.append(SQLUtil.concatComma((String[])tmpSubList2.toArray(new String[tmpSubList2.size()])));
		query.append(")");
		
		return query.toString();
	}

	/**
	 * 科目情報を取得する
	 * @param year
	 * @return
	 */
	private String getSubjectData(ArrayList lsubcd) {
		
		final StringBuffer sql = new StringBuffer();
		
		sql.append(" SELECT SUBCD,SUBNAME FROM EXAMSUBJECT ");
		sql.append(" WHERE  SUBCD < '7000'  ");
		sql.append(" AND EXAMYEAR = ? ");
		sql.append(" AND (");
		sql.append(getSubCD(lsubcd));
		sql.append(" ) ");
		sql.append(" AND EXAMCD = ? ");
		sql.append(" ORDER BY DISPSEQUENCE ASC ");
		
		return sql.toString();
	}
	
	/**
	 * 型情報を取得する
	 * @param year
	 * @return
	 */
	private String getPatternData(ArrayList lsubcd) {

		final StringBuffer sql = new StringBuffer();
		
		sql.append(" SELECT SUBCD,SUBNAME FROM EXAMSUBJECT ");
		sql.append(" WHERE  SUBCD >= '7000'  ");
		sql.append(" AND EXAMYEAR = ? ");
		sql.append(" AND (");
		sql.append(getSubCD(lsubcd));
		sql.append(" ) ");
		sql.append(" AND EXAMCD = ? ");
		sql.append(" ORDER BY DISPSEQUENCE ASC ");
		
		return sql.toString();
	}

	/**
	 * SQLの制約を取得
	 * @param lsubcd
	 * @return
	 */
	private String getSubCD(ArrayList lsubcd) {
		
		Iterator it = lsubcd.iterator();
		StringBuffer sql = new StringBuffer();
		int cnt = 0;
		while (it.hasNext()) {
			String subcd = (String)it.next();
			sql.append("SUBCD = '");
			sql.append( subcd );
			sql.append("'");
			if (lsubcd.size()-1 != cnt) {
				sql.append(" OR ");
			}
			cnt++;
		}
		return sql.toString();
	}


	/**
	 * 科目と教科の情報を取得する
	 * @param year
	 * @param examcd
	 * @return
	 */
	private String getCrsSubData() {
		
		final StringBuffer sql = new StringBuffer();
		
		sql.append(" select  a.subcd, a.newcourse, a.subname, b.count, a.newcoursecd, a.dispsequence from "); 
		sql.append(" ( ");
		sql.append(" select distinct examsubject.subcd,course.coursename ,examsubject.subname ,examsubject.subaddrname, course.coursecd," );
		sql.append(" DECODE(substr(examsubject.subcd,0,2), ");
		sql.append(" '10','外国語', ");
		sql.append(" '11','外国語', ");
		sql.append(" '12','外国語', ");
		sql.append(" '13','外国語', ");
		sql.append(" '14','外国語', ");
		sql.append(" '15','外国語', ");
		sql.append(" '16','外国語', ");
		sql.append(" '17','外国語', ");
		sql.append(" '18','外国語', ");
		sql.append(" '19','外国語', ");
		sql.append(" '51','公民', ");
		sql.append(" '52','地理歴史', ");
		sql.append(" '53','地理歴史', ");
		sql.append(" '54','地理歴史', ");
		sql.append(" '55','公民', ");
		sql.append(" '56','公民', ");
		sql.append(" '57','公民', ");
		sql.append(" course.coursename) \"NEWCOURSE\", ");
		sql.append(" DECODE(substr(examsubject.subcd,0,2), ");
		sql.append(" '51','5100', ");
		sql.append(" '52','5000', ");
		sql.append(" '53','5000', ");
		sql.append(" '54','5000', ");
		sql.append(" '55','5100', ");
		sql.append(" '56','5100', ");
		sql.append(" '57','5100', ");
		sql.append(" course.coursecd) \"NEWCOURSECD\", ");
		sql.append(" examsubject.dispsequence ");
 		sql.append(" from course,examsubject ");
 		sql.append(" where substr(examsubject.subcd,0,1) = substr(course.coursecd,0,1) ");
 		sql.append(" and examyear = ? ");
 		sql.append(" and examcd = ? ");
 		sql.append(" and subcd < '7000' ");
 		// ↓＃３ 2004.11.17 F.Urakawa 高１、プレの時には出さない科目を条件に加える
 		sql.append(getLimitSubcd());
 		// ↑＃３ 2004.11.17 F.Urakawa 高１、プレの時には出さない科目を条件に加える
		sql.append(" and year= ? ");
		sql.append(" order by newcoursecd ");
		sql.append(" ) a ");
		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select NEWCOURSECD, count(NEWCOURSECD) as count from ( ");
		sql.append(" select distinct examsubject.subcd, ");
		sql.append(" course.coursename, ");
		sql.append(" examsubject.subname, ");
		sql.append(" course.coursecd, ");
		sql.append(" DECODE(substr(examsubject.subcd,0,2), ");
		sql.append(" '51','5100', ");
		sql.append(" '52','5000', ");
		sql.append(" '53','5000', ");
		sql.append(" '54','5000', ");
		sql.append(" '55','5100', ");
		sql.append(" '56','5100', ");
		sql.append(" '57','5100', ");
		sql.append(" course.coursecd) \"NEWCOURSECD\" ");
		sql.append(" from course,examsubject  ");
		sql.append(" where substr(examsubject.subcd,0,1) = substr(course.coursecd,0,1) ");
		sql.append(" and examyear = ? ");
		sql.append(" and examcd = ? ");
		sql.append(" and subcd < '7000' ");
		//↓＃３ 2004.11.17 F.Urakawa 高１、プレの時には出さない科目を条件に加える
		sql.append(getLimitSubcd());
		// ↑＃３ 2004.11.17 F.Urakawa 高１、プレの時には出さない科目を条件に加える
		sql.append(" and year = ? ");
		sql.append(" order by course.coursecd) ");
		sql.append(" group by NEWCOURSECD ");
		sql.append(" ) b ");
		sql.append(" on a.newcoursecd=b.newcoursecd ");
		sql.append(" order by newcoursecd,dispsequence ");
		
		return sql.toString();
	}
	
	/**
	 * 型情報をpropertyファイルから取得するか
	 * @param examcd
	 * @return
	 */
	private String getSpecialPattern() {
	
		for (int i = 0; i < sPattern1.length; i++) {
			if (examcd.equals(sPattern1[i])) {
				return "1";
			}
		}
		for (int i = 0; i < sPattern2.length; i++) {
			if (examcd.equals(sPattern2[i])) {
				return "2";
			}
		}
		for (int i = 0; i < sPattern3.length; i++) {
			if (examcd.equals(sPattern3[i])) {
				return "3";
			}
		}
		for (int i = 0; i < sPattern4.length; i++) {
			if (examcd.equals(sPattern4[i])) {
				return "4";
			}
		}
		for (int i = 0; i < sPattern5.length; i++) {
			if (examcd.equals(sPattern5[i])) {
				return "5";
			}
		}
		for (int i = 0; i < sPattern6.length; i++) {
			if (examcd.equals(sPattern6[i])) {
				return "6";
			}
		}
		for (int i = 0; i < sPattern7.length; i++) {
			if (examcd.equals(sPattern7[i])) {
				return "7";
			}
		}
		for (int i = 0; i < sPattern8.length; i++) {
			if (examcd.equals(sPattern8[i])) {
				return "8";
			}
		}
		for (int i = 0; i < sPattern9.length; i++) {
			if (examcd.equals(sPattern9[i])) {
				return "9";
			}
		}
		return "0";
	}

	/**
	 * @param string
	 */
	public void setYear(String string) {
		year = string;
	}

	/**
	 * @return
	 */
	public List getLcs() {
		return lcs;
	}

	/**
	 * @return
	 */
	public List getLp() {
		return lp;
	}

	/**
	 * @param string
	 */
	public void setExamcd(String string) {
		examcd = string;
	}
	
	/**
	 * #3マーク、プレマークのときには、
	 * 物理�TA(4260)、化学�TA(4360)、生物�TA(4460)、地学�TA(4560)、日本史A(5260)、世界史A(5360)、地理A(5460)
	 * を出力しない
	 * 2004.11.24 F.Urakawa 全統記述模試１〜３回、全統私大模試１〜２回、全統記述高２模試は
	 * 						数学集計科目が異なるため、処理を追加
	 * @param examcd
	 * @return
	 */
	private String getLimitSubcd() {
	
		final StringBuffer query = new StringBuffer();
		
		if ("03".equals(examcd) || "04".equals(examcd)) {
			query.append(" AND EXAMSUBJECT.SUBCD");
			query.append(" NOT IN (");
			query.append(" '4260', ");	//物理�TA
			query.append(" '4360', ");	//化学�TA
			query.append(" '4460', ");	//生物�TA
			query.append(" '4560' ");		//地学�TA
			query.append(" ) ");
		}
		
		if ("05".equals(examcd) || "06".equals(examcd)
				|| "07".equals(examcd) || "09".equals(examcd) || "10".equals(examcd)) {
			query.append(" AND SUBCD ");
			query.append(" NOT IN (");
			query.append(" '2160', ");	//数�T
			query.append(" '2260', ");	//数�TA
			query.append(" '2460', ");	//数�UA
			query.append(" '2560', ");	//数�UB
			query.append(" '2660', ");	//数�VB
			query.append(" '2760' ");		//数�VC
			query.append(" ) ");
		}
	
		if ("67".equals(examcd)) {
			query.append(" AND SUBCD ");
			query.append(" NOT IN (");
			query.append(" '2160', ");	//数�T
			query.append(" '2260', ");	//数�TA
			query.append(" '2460', ");	//数�UA
			query.append(" '2560' ");		//数�UB
			query.append(" ) ");
		}
	
		return query.toString();
	}

	/**
	 * @param tmpSubList the tmpSubList to set
	 */
	public void setTmpSubList(ArrayList tmpSubList) {
		this.tmpSubList = tmpSubList;
	}

	/**
	 * @param tmpPatList the tmpPatList to set
	 */
	public void setTmpPatList(ArrayList tmpPatList) {
		this.tmpPatList = tmpPatList;
	}

}

package jp.co.fj.freemenu.servlets.user;

import jp.co.fj.keinavi.servlets.user.U102Servlet;

/**
 *
 * 利用者パスワード変更画面サーブレット
 * ※DLサービス校用
 * 
 * 2008.12.24	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U102DLServlet extends U102Servlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.user.U102Servlet#getJspId()
	 */
	protected String getJspId() {
		return "u102_dl";
	}
	
}

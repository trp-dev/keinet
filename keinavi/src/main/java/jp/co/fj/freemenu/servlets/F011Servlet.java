package jp.co.fj.freemenu.servlets;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.freemenu.forms.F011Form;
import jp.co.fj.keinavi.beans.UploadFileBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 * アップロードファイル履歴画面サーブレット
 * 
 * 2011.01.18	[新規作成]
 * 
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class F011Servlet extends DefaultHttpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {
		
		// アクションフォーム
		final F011Form form = (F011Form) getActionForm(
				request, "jp.co.fj.freemenu.forms.F011Form");
		
		final LoginSession loginSession = getLoginSession(request);
		
		// JSPへの遷移処理
		if ("f011".equals(form.getForward())) {

			Connection con = null;
			try {

				con = getConnectionPool(request);
				
				request.setAttribute("safeFileBean", searchList(con, loginSession));

				super.forward(request, response, JSP_F011);

			} catch (Exception e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

		// 不明なら転送
		} else {			
			super.dispatch(request, response);
		}
	}
	
	/**
	 * アップロードファイル履歴を検索する。
	 * @param con
	 * @param loginSession
	 * @return
	 * @throws Exception
	 */
	private UploadFileBean searchList(Connection con, LoginSession loginSession) throws Exception {
		UploadFileBean ufb = new UploadFileBean(loginSession);
		ufb.setConnection(null, con);
		ufb.searchSafeBySchool();
		return ufb;
	}
	
}

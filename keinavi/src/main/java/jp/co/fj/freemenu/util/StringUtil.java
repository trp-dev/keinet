/*
 * 作成日: 2004/10/18
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.util;

import java.io.File;
import java.util.ArrayList;

import jp.co.fj.keinavi.util.log.KNLog;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class StringUtil {
	
	/**
	 * 模試名「＊＊＊＊年度全統○○○模試」→「全統○○○模試」に変更
	 * @param sExamName
	 */
	public String editExamName(String sExamName) {
		// TODO 自動生成されたメソッド・スタブ
		String retstr = null;
		// 「年度」という文字列の長さを取得
		int len = "年度".length();
		retstr = sExamName.substring(sExamName.indexOf("年度") + len);
		return retstr;
	
	}
	
	
	/**
	 * PDFファイルから科目コード(科目)を取得する
	 * @param fullpath	指定フォルダ（絶対パス）
	 * @param start		ファイル名開始文字
	 * @param examcd	模試コード
	 * @param end		ファイル拡張子
	 * @return		科目コード一覧
	 */
	public ArrayList getSubjectListFromPdf(String fullpath, String start, String examcd, String end) {
		
		final File f = new File(fullpath);
		final String[] fs = f.list();
		
		if (fs == null) {
			return new ArrayList();
		}
		
		// 科目コードの長さ
		final int isubcdlen = 4;
		String subcd = null;
		
		ArrayList lsubcd = new ArrayList();
		
		for (int i = 0; i < fs.length; i++) {
			// start文字列で開始し、end文字列で終了しているファイル
			if (fs[i].startsWith(start) && fs[i].endsWith(end) ) {
				subcd = fs[i].substring(start.length()+examcd.length(),
								start.length()+examcd.length()+isubcdlen);
				try {
					if (Integer.parseInt(subcd) < 7000) {
						lsubcd.add(subcd);
					}
				} catch (NumberFormatException e) {
					// 数値に変換できない場合には、科目コードとして認識しない。
					KNLog.Err(null, null, null, null, null, null, "科目コード変換に失敗しました：[" + fs[i] +"]");
				}
			}
			
		}
		
		return lsubcd;
	
	}
	
	/**
	 * PDFファイルから科目コード(型)を取得する
	 * @param fullpath	指定フォルダ（絶対パス）
	 * @param start		ファイル名開始文字
	 * @param examcd	模試コード
	 * @param end		ファイル拡張子
	 * @return		科目コード一覧
	 */
	public ArrayList getPatternListFromPdf(String fullpath, String start, String examcd, String end) {
		
		final File f = new File(fullpath);
		final String[] fs = f.list();
		
		if (fs == null) {
			return new ArrayList();
		}
		
		final int ipatcdlen = 4;
		String subcd = null;
		
		ArrayList lpatcd = new ArrayList();
		
		for (int i = 0; i < fs.length; i++) {
			// start文字列で開始し、end文字列で終了しているファイル
			if (fs[i].startsWith(start) && fs[i].endsWith(end) ) {
				subcd = fs[i].substring(start.length()+examcd.length(),
								start.length()+examcd.length()+ipatcdlen);
				try {
					if (Integer.parseInt(subcd) >= 7000) {
						lpatcd.add(subcd);
					}
				} catch (NumberFormatException e) {
					// 数値に変換できない場合には、科目コードとして認識しない。
					KNLog.Err(null, null, null, null, null, null, "科目コード変換に失敗しました：[" + fs[i] +"]");
				}
				
			}
			
		}
		
		return lpatcd;
	
	}
}

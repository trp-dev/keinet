package jp.co.fj.freemenu.beans.f002;

/**
 *
 * 成績統計資料集ダウンロードのBeanファクトリ
 * 
 * 2008.03.05	[新規作成]
 * 2008.04.11	PDF追加提供に伴う改修
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class F002BeanFactory {

	/**
	 * @param functionId 機能ID
	 * @return 指定した機能IDのBean
	 */
	public static F002Bean create(final String functionId) {
		
		if ("F002_1".equals(functionId)) {
			return new F002_1Bean();
			
		} else if ("F002_2".equals(functionId)) {
			return new F002_2Bean();
			
		} else if ("F002_3".equals(functionId)) {
			return new F002_3Bean();
			
		} else if ("F002_4".equals(functionId)) {
			return new F002_4Bean();
			
		} else if ("F002_5".equals(functionId)) {
			return new F002_5Bean();
			
		} else if ("F002_6".equals(functionId)) {
			return new F002_6Bean();
			
		} else if ("F002_7".equals(functionId)) {
			return new F002_7Bean();
			
		} else if ("F002_8".equals(functionId)) {
			return new F002_8Bean();
			
		} else if ("F002_9".equals(functionId)) {
			return new F002_9Bean();
			
		} else if ("F002_10".equals(functionId)) {
			return new F002_10Bean();
			
		} else if ("F002_11".equals(functionId)) {
			return new F002_11Bean();
			
		} else if ("F002_12".equals(functionId)) {
			return new F002_12Bean();
			
		} else if ("F002_13".equals(functionId)) {
			return new F002_13Bean();
			
		} else if ("F002_14".equals(functionId)) {
			return new F002_14Bean();
			
		} else if ("F002_15".equals(functionId)) {
			return new F002_15Bean();
			
		} else if ("F002_16".equals(functionId)) {
			return new F002_16Bean();
			
		} else if ("F002_17".equals(functionId)) {
			return new F002_17Bean();
			
		} else {
			throw new RuntimeException("不正な機能IDです。" + functionId);
		}
	}
	

}

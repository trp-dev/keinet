/*
 * 作成日: 2004/11/24
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.data;

import java.io.Serializable;

/**
 * ファイル作成状況保持クラス
 * 
 * @author Yoshimoto KAWAI - Totec
 */
public class CreateStatus implements Serializable {

	public static final String SESSION_KEY = "CreateStatus";

	private String filename; // ファイル名
	private String sessionKey; // セッションキー
	private boolean created; // 作成完了かどうか
	private boolean error; // エラー発生かどうか
	private boolean direct; // ダイレクトダウンロードかどうか

	/**
	 * @return
	 */
	public boolean isCreated() {
		return created;
	}

	/**
	 * @return
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @param b
	 */
	public void setCreated(boolean b) {
		created = b;
	}

	/**
	 * @return
	 */
	public boolean isError() {
		return error;
	}

	/**
	 * @param b
	 */
	public void setError(boolean b) {
		error = b;
	}

	/**
	 * @return
	 */
	public String getSessionKey() {
		return sessionKey;
	}

	/**
	 * @param string
	 */
	public void setSessionKey(String string) {
		sessionKey = string;
	}

	/**
	 * @param string
	 */
	public void setFilename(String string) {
		filename = string;
	}

    /**
     * @return direct を戻す。
     */
    public boolean isDirect() {
        return direct;
    }
    /**
     * @param direct direct を設定する。
     */
    public void setDirect(boolean direct) {
        this.direct = direct;
    }
}

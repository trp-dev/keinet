/*
 * 作成日: 2004/09/03
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.forms;

import jp.co.fj.keinavi.forms.BaseForm;


/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class F003_1Form extends BaseForm {

	private String division = null;
	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ

	}

	/**
	 * @return
	 */
	public String getDivision() {
		return division;
	}

	/**
	 * @param string
	 */
	public void setDivision(String string) {
		division = string;
	}

}

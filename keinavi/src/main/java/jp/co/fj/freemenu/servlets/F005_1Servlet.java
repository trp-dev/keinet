/*
 * 作成日: 2004/09/03
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.freemenu.data.ExamScoreData;
import jp.co.fj.freemenu.data.ExamScoreSession;
import jp.co.fj.freemenu.forms.F005_1Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class F005_1Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	    // アクションフォーム
	    F005_1Form form = (F005_1Form) super.getActionForm(request,
			"jp.co.fj.freemenu.forms.F005_1Form");
		// 模試成績セッション
	    ExamScoreSession score = (ExamScoreSession)
	    	request.getSession(false).getAttribute(ExamScoreSession.SESSION_KEY);
	    
		// 模試データをソートする
		List list = (List) score.getExamMap().get(form.getNendo());
		ArrayList grade1Exam = new ArrayList();
		ArrayList grade2Exam = new ArrayList();
		ArrayList grade3Exam = new ArrayList();
		// 各学年の模試データ
		getGradeExamData(list, grade1Exam, grade2Exam, grade3Exam);	
		// 学年ごとにセットする
		request.setAttribute("ExamDataGrade1", grade1Exam);
		request.setAttribute("ExamDataGrade2", grade2Exam);
		request.setAttribute("ExamDataGrade3", grade3Exam);
		
		request.setAttribute("form",form);

		if ("f005_1".equals(super.getForward(request))) {
			super.forward(request, response, JSP_F005_1);
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

	/**
	 * 学年ごとの模試データを取得する
	 * @param list
	 * @param exam1 1年生対象模試
	 * @param exam2 2年生対象模試
	 * @param exam3 3年生対象模試
	 * @param grade
	 * @return
	 */
	private void getGradeExamData(List list, List exam1, List exam2, List exam3) {
		Iterator it = list.iterator();
		while(it.hasNext()) {
			ExamScoreData data = (ExamScoreData) it.next();

			// 1年
			if ("01".equals(data.getTargetGrade())) exam1.add(data);
			// 2年
			else if ("02".equals(data.getTargetGrade())) exam2.add(data);
			// 3年
			else exam3.add(data);
		}
	}
}

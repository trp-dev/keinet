package jp.co.fj.freemenu.forms;

import java.io.IOException;

import jp.co.fj.keinavi.forms.BaseUploadForm;
import jp.co.fj.keinavi.util.DataValidator;
import jp.co.fj.keinavi.util.message.MessageLoader;

/**
 *
 * 高等学校作成ファイルアップロード画面フォーム
 * 
 * 2011.01.18	[新規作成]
 * 
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class F010Form  extends BaseUploadForm {
	
	//アップロード成功または失敗
	private boolean successful = true;
	
	//アップロードファイル種類ID
	private String fileTypeID = "01";
	
	//コメント
	private String comments;
	
	/**
	 * 入力チェック
	 */
	public void validate() {
		//ファイルアップロード時のみチェック
		if (getFile() != null) {
			if (fileTypeID == null) {
				setErrorMessage("ファイル種類を選択してください。");
			}
			else if (DataValidator.getInstance().hasJapanese(getFileName())) {
				try {
					setErrorMessage(MessageLoader.getInstance().getMessage("f001a"));
				} catch (IOException e) {
					e.printStackTrace();
					throw new InternalError(e.getMessage());
				}
			}
			else if (comments.length() > 0) {
				if (!DataValidator.getInstance().isValidByteLength(comments, 0, 180)) {
					setErrorMessage("コメントは全角９０文字までです。");
				}
				else if (DataValidator.getInstance().hasPlatformDependent(comments)) {
					try {
						setErrorMessage(MessageLoader.getInstance().getMessage("f002a"));
					} catch (IOException e) {
						e.printStackTrace();
						throw new InternalError(e.getMessage());
					}
				}
			}
		}
	}

	public boolean isSuccessful() {
		return successful;
	}

	public void setUnsuccessful() {
		this.successful = false;
	}

	public String getFileTypeID() {
		return fileTypeID;
	}

	public void setFileTypeID(String fileTypeID) {
		this.fileTypeID = fileTypeID;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	

}

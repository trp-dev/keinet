/*
 * 作成日: 2004/08/20
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.freemenu.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.freemenu.forms.F003Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.KNCommonProperty;

/**
 * @author fujito-urakawa
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class F003Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ
		
		F003Form form = null;

		//DLRootパスを取得(トップ画面 or プロファイル選択画面から)
		HttpSession session = request.getSession();
		if (session.getAttribute("sDLRoot") == null) {
			try {
				session.setAttribute("sDLRoot",
					KNCommonProperty.getDLSDataPath());
			} catch (Exception e1) {
				// TODO 自動生成された catch ブロック
				throw new ServletException(e1);
			}
		}
		
		try {
			form = (F003Form)super.getActionForm(request,
				"jp.co.fj.freemenu.forms.F003Form");
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			throw new ServletException(e);
		}
		
		if ("w002".equals(form.getBackward()) || "w008".equals(form.getBackward())) {
			// ------------------------------------------------------------
			// 2004.11.15
			// Yoshimoto KAWAI - Totec
			// DBKEYは書き換えない
			// 無料メニューも体験版以外は通常DBとなるため
			////DBKEYの更新
			// ------------------------------------------------------------
			//LoginSession ls = super.getLoginSession(request);
			try {
				//ls.setDbKey(KNCommonProperty.getFDBSID());
				//LoginSessionの更新
				//session.setAttribute(LoginSession.SESSION_KEY, ls);
				//セッションにトップメニューから来た情報を追加
				session.setAttribute("FromTopMenu", form.getBackward());
			} catch (Exception e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}
		}
		
		if ("f003".equals(super.getForward(request))) {
			super.forward(request, response, JSP_F003);
		}
		else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

/**
 * 個人成績分析−志望大学判定一覧
 * @author Ito.Y
 */
 
package jp.co.fj.keinavi.excel.personal;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.personal.*;
import jp.co.fj.keinavi.util.log.*;

public class I14 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean i14( I11Item i11Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog) {

		KNLog log = KNLog.getInstance(null,null,null);

		try{
			//I11Itemから各帳票を出力
			int ret = 0;
			
			log.Ep("I14","I14帳票作成開始","");
			I14_01 exceledit1 = new I14_01();
			ret = exceledit1.i14_01EditExcel( i11Item, outfilelist, saveFlg, UserID );
			if (ret!=0) {
				log.Err("0"+ret+"I14","帳票作成エラー","");
				return false;
			}
			log.Ep("I14","I14帳票作成終了","");
			sheetLog.add("I14_01");

		} catch(Exception e) {
			e.printStackTrace();
			log.Err("99I14","帳票作成エラー",e.toString());
			return false;
		}
		return true;
	}

}
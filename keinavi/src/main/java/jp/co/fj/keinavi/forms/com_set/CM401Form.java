package jp.co.fj.keinavi.forms.com_set;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM401Form extends BaseForm {

	// 選択方法
	private String selection = null;
	// 志望大学コードの配列
	private String univ[] = null;

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 
		
	}

	/**
	 * @return
	 */
	public String getSelection() {
		return selection;
	}

	/**
	 * @return
	 */
	public String[] getUniv() {
		return univ;
	}

	/**
	 * @param string
	 */
	public void setSelection(String string) {
		selection = string;
	}

	/**
	 * @param strings
	 */
	public void setUniv(String[] strings) {
		univ = strings;
	}

}

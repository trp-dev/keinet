package jp.co.fj.keinavi.beans.admin.application;
/*
 * $(#) $ld$
 *
 * All Rights Reserved, Copyright(c) 2004 FUJITSU KANSAI-CHUBU NETTECH LIMITED.
 *
 */

import jp.co.fj.keinavi.beans.admin.name.SystemLogWriter;
/**
 *
 * コネクションプーリング用クラス
 *
 * @author $Author$
 * @author Yukari OKUMURA
 * @version $Revision: 1.00 $, $Date: 2004/03/31 17:30:00 $
 * @since 1.00
 *
 */
import java.sql.*;
import java.util.*;


public class DbConnectionPool {

    // ログインID
	private String _loginId;
	// パスワード
	private String _password;
	// URL
	private String _url;
	// ドライバ名
	private String _driver;
	// コネクション最大接続数
	private int _maxConnection;
	// コネクション貸し出し接続数
	private int checkedOut;
	// コネクションプール
	private Vector connectionPool = new Vector();
	// コネクションプーリングクラスインスタンス
	private static DbConnectionPool instance;

	/**
	 *
	 * プーリングクラスのインスタンスを取得
	 *
	 * @return DbConnectionPool instance
	 */
	public static synchronized DbConnectionPool getInstance() {
		if (instance == null) {
			instance = new DbConnectionPool();
		}
		return instance;
	}

	/**
	 *
	 * コンストラクタ コネクションプーリングの初期化
	 *
	 * @return void
	 */
	private DbConnectionPool() {
		this._driver = DbAccessInf.getDriver();
		this._url = DbAccessInf.getURL();
		this._loginId = DbAccessInf.getLoginId();
		this._password = DbAccessInf.getPassWord();
		this._maxConnection = 10;
	}

	/**
	 *
	 * コネクションの取得
	 *
	 * @return DBコネクションを返します
	 * @throws java.lang.ClassNotFoundException ドライバが見つからない時スローされます。
	 * @exception java.lang.ClassNotFoundException ドライバが見つからない時スローされます。
	 * @see  java.lang.ClassNotFoundException
	 * @throws java.sql.SQLException コネクション取得に失敗した場合スローされます。
	 * @exception java.sql.SQLException コネクション取得に失敗した場合スローされます。
	 * @see  java.sql.SQLException
	 */
	public synchronized Connection getConnection() throws ClassNotFoundException,SQLException {
		Connection con = null;
		SystemLogWriter.printLog(
			this,
			"コネクション数：" + connectionPool.size(),
			SystemLogWriter.DEBUG);
		if (connectionPool.size() > 0) {
			con = (Connection) connectionPool.firstElement();
			connectionPool.removeElementAt(0);
			try {
				if (con.isClosed()) {
					con = getConnection();
				}
			} catch (SQLException e) {
				con = getConnection();
			}
		} else if (_maxConnection == 0 || checkedOut < _maxConnection) {
			con = newConnection();
		}
		if (con != null) {
			checkedOut++;
		}
		return con;
	}


	/**
	 *
	 * 新規にコネクションを作成
	 *
	 * @return con コネクション
	 * @throws java.lang.ClassNotFoundException ドライバが見つからない時スローされます。
	 * @exception java.lang.ClassNotFoundException ドライバが見つからない時スローされます。
	 * @see  java.lang.ClassNotFoundException
	 * @throws java.sql.SQLException コネクション取得に失敗した場合スローされます。
	 * @exception java.sql.SQLException コネクション取得に失敗した場合スローされます。
	 * @see  java.sql.SQLException
	 */
	private Connection newConnection() throws ClassNotFoundException,SQLException {
		Class.forName(_driver);
		return DriverManager.getConnection(_url, _loginId, _password);
	}

	/**
	 *
	 * コネクションを返却
	 *
	 * @param con DBコネクション
	 * @return void
	 */
	public synchronized void freeConnection(Connection con) {
		connectionPool.addElement(con);
		checkedOut--;
		notifyAll();
	}


	/**
	 *
	 * 全てのコネクションを開放
	 *
	 * @return void
	 */
	public synchronized void release() {
		Enumeration enumConnections = connectionPool.elements();
		while (enumConnections.hasMoreElements()) {
			Connection con = (Connection) enumConnections.nextElement();
			try {
				con.close();
			} catch (SQLException e) {
			}
		}
		connectionPool.removeAllElements();
	}



    // 使用例
	public static void main(String[] args) {
		DbConnectionPool dbc = null;
		Connection con = null;
		try {
			dbc = DbConnectionPool.getInstance();
			con = dbc.getConnection();
			dbc.freeConnection(con);
			dbc.release();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
	}
}

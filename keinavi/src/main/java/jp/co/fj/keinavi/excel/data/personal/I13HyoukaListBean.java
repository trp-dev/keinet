package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
//import java.util.ArrayList;
/**
 * �l���ѕ��́|�l���ѕ\�|�A���l���ѕ\ �u�]��w�ʕ]���f�[�^���X�g
 * �쐬��: 2004/07/20
 * @author	A.Iwata
 */
public class I13HyoukaListBean {
	//�u�]����
	private String strShiboRank = "";
	//��w��
	private String strDaigakuMei = "";
	//�w��
	private String strGakubuMei = "";
	//�w��
	private String strGakkaMei = "";
	//���
	private int intTeiin = 0;
	//����M��
	private String strTeiinRel = "";
	//�Z���^�[�����@�]��
	private String strHyoukaMark = "";
	//�ʎ����@�]��
	private String strHyoukaKijyutsu = "";
	//�����]��
	private String strHyoukaAll = "";

	// 2019/11/21 QQ)Ooseto �p��F�莎�������Ή� DEL START
	// 2019/08/05 Hics)Ueta ���ʃe�X�g�Ή� ADD START
	//�Z���^�[�����@�]���i�F�莎���܂܂Ȃ��j
	//private String strHyoukaMark_engpt = "";
	// 2019/08/05 Hics)Ueta ���ʃe�X�g�Ή� ADD END

	// 2019/09/18 QQ)Tanouchi ���ʃe�X�g�Ή� ADD START
	// �p�ꎑ�i�v���\�o�莑�i����
	//private String hyoukaJodge = "";
	// �p�ꎑ�i�v���\�o����Ή����m
	//private String hyoukaNotice = "";
	// 2019/09/18 QQ)Tanouchi ���ʃe�X�g�Ή� ADD END
	// 2019/11/21 QQ)Ooseto �p��F�莎�������Ή� DEL END

	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrShiboRank() {
		return this.strShiboRank;
	}
	public String getStrDaigakuMei() {
		return this.strDaigakuMei;
	}
	public String getStrGakubuMei() {
		return this.strGakubuMei;
	}
	public String getStrGakkaMei() {
		return this.strGakkaMei;
	}
	public int getIntTeiin() {
		return this.intTeiin;
	}
	public String getStrTeiinRel() {
		return this.strTeiinRel;
	}
	public String getStrHyoukaMark() {
		return this.strHyoukaMark;
	}
	public String getStrHyoukaKijyutsu() {
		return this.strHyoukaKijyutsu;
	}
	public String getStrHyoukaAll() {
		return this.strHyoukaAll;
	}
	// 2019/11/21 QQ)Ooseto �p��F�莎�������Ή� DEL START
	// 2019/08/05 Hics)Ueta ���ʃe�X�g�Ή� ADD START
//	public String getStrHyoukaMark_engpt() {
//		return this.strHyoukaMark_engpt;
//	}
	// 2019/08/05 Hics)Ueta ���ʃe�X�g�Ή� ADD END

	// 2019/09/18 QQ)Tanouchi ���ʃe�X�g�Ή� ADD START
//	public String getHyoukaJodge() {
//	return hyoukaJodge;
//	}
//	public String getHyoukaNotice() {
//		return hyoukaNotice;
//	}
	// 2019/09/18 QQ)Tanouchi ���ʃe�X�g�Ή� ADD END
	// 2019/11/21 QQ)Ooseto �p��F�莎�������Ή� DEL END

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrShiboRank(String strShiboRank) {
		this.strShiboRank = strShiboRank;
	}
	public void setStrDaigakuMei(String strDaigakuMei) {
		this.strDaigakuMei = strDaigakuMei;
	}
	public void setStrGakubuMei(String strGakubuMei) {
		this.strGakubuMei = strGakubuMei;
	}
	public void setStrGakkaMei(String strGakkaMei) {
		this.strGakkaMei = strGakkaMei;
	}
	public void setIntTeiin(int intTeiin) {
		this.intTeiin = intTeiin;
	}
	public void setStrTeiinRel(String strTeiinRel) {
		this.strTeiinRel = strTeiinRel;
	}
	public void setStrHyoukaMark(String strHyoukaMark) {
		this.strHyoukaMark = strHyoukaMark;
	}
	public void setStrHyoukaKijyutsu(String strHyoukaKijyutsu) {
		this.strHyoukaKijyutsu = strHyoukaKijyutsu;
	}
	public void setStrHyoukaAll(String strHyoukaAll) {
		this.strHyoukaAll = strHyoukaAll;
	}

	// 2019/11/21 QQ)Ooseto �p��F�莎�������Ή� DEL START
	// 2019/08/05 Hics)Ueta ���ʃe�X�g�Ή� ADD START
//	 public void setStrHyoukaMark_engpt(String strHyoukaMark_engpt) {
//		 this.strHyoukaMark_engpt = strHyoukaMark_engpt;
//	}
	// 2019/08/05 Hics)Ueta ���ʃe�X�g�Ή� ADD END
	// 2019/09/18 QQ)Tanouchi ���ʃe�X�g�Ή� ADD START
//	public void setHyoukaJodge(String hyoukaJodge) {
//		this.hyoukaJodge = hyoukaJodge;
//	}
//	public void setHyoukaNotice(String hyoukaNotice) {
//		this.hyoukaNotice = hyoukaNotice;
//	}
	// 2019/09/18 QQ)Tanouchi ���ʃe�X�g�Ή� ADD END
	// 2019/11/21 QQ)Ooseto �p��F�莎�������Ή� DEL END

}

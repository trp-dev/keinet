package jp.co.fj.keinavi.excel.data.school;

/**
 * �u�]��w�]���ʐl���i�Z�����сj�]���ʐl���f�[�^���X�g
 * �쐬��: 2004/07/08
 * @author	T.Kuzuno
 */
public class S34HyoukaNinzuListBean {
	//�N�x
	private String strNendo = "";
	//���u�]
	private int intSoshibo = 0;
	//��P�u�]
	private int intDai1shibo = 0;
	//�]���ʐl��A
	private int intHyoukaA = 0;
	//�]���ʐl��B
	private int intHyoukaB = 0;
	//�]���ʐl��C
	private int intHyoukaC = 0;

	// 2019/11/19 QQ)Ooseto �p��F�莎�������Ή� DEL START
	// 2019/09/30 QQ) ���ʃe�X�g�Ή� ADD START
	//�]���ʐl��A
	//private int intHyoukaA_Hukumu = 0;
	//�]���ʐl��B
	//private int intHyoukaB_Hukumu = 0;
	//�]���ʐl��C
	//private int intHyoukaC_Hukumu = 0;
	// 2019/09/30 QQ) ���ʃe�X�g�Ή� ADD END
	// 2019/11/19 QQ)Ooseto �p��F�莎�������Ή� DEL END

	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrNendo() {
		return this.strNendo;
	}
	public int getIntSoushibo() {
		return this.intSoshibo;
	}
	public int getIntDai1shibo() {
		return this.intDai1shibo;
	}
	public int getIntHyoukaA() {
		return this.intHyoukaA;
	}
	public int getIntHyoukaB() {
		return this.intHyoukaB;
	}
	public int getIntHyoukaC() {
		return this.intHyoukaC;
	}

	// 2019/11/19 QQ)Ooseto �p��F�莎�������Ή� DEL START
	// 2019/09/30 QQ) ���ʃe�X�g�Ή� ADD START
//	public int getIntHyoukaA_Hukumu() {
//		return this.intHyoukaA_Hukumu;
//	}
//	public int getIntHyoukaB_Hukumu() {
//		return this.intHyoukaB_Hukumu;
//	}
//	public int getIntHyoukaC_Hukumu() {
//		return this.intHyoukaC_Hukumu;
//	}
	// 2019/09/30 QQ) ���ʃe�X�g�Ή� ADD END
	// 2019/11/19 QQ)Ooseto �p��F�莎�������Ή� DEL END

	/*---------------*/
	/* Set */
	/*---------------*/
	public void setStrNendo(String strNendo) {
		this.strNendo = strNendo;
	}
	public void setIntSoushibo(int intSoshibo) {
		this.intSoshibo = intSoshibo;
	}
	public void setIntDai1shibo(int intDai1shibo) {
		this.intDai1shibo = intDai1shibo;
	}
	public void setIntHyoukaA(int intHyoukaA) {
		this.intHyoukaA = intHyoukaA;
	}
	public void setIntHyoukaB(int intHyoukaB) {
		this.intHyoukaB = intHyoukaB;
	}
	public void setIntHyoukaC(int intHyoukaC) {
		this.intHyoukaC = intHyoukaC;
	}

	// 2019/11/19 QQ)Ooseto �p��F�莎�������Ή� DEL START
	// 2019/09/30 QQ) ���ʃe�X�g�Ή� ADD START
//	public void setIntHyoukaA_Hukumu(int intHyoukaA) {
//		this.intHyoukaA_Hukumu = intHyoukaA;
//	}
//	public void setIntHyoukaB_Hukumu(int intHyoukaB) {
//		this.intHyoukaB_Hukumu = intHyoukaB;
//	}
//	public void setIntHyoukaC_Hukumu(int intHyoukaC) {
//		this.intHyoukaC_Hukumu = intHyoukaC;
//	}
	// 2019/09/30 QQ) ���ʃe�X�g�Ή� ADD END
	// 2019/11/19 QQ)Ooseto �p��F�莎�������Ή� DEL END
}

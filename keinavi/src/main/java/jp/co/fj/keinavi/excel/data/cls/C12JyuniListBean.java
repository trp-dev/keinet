package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−順位表データリスト
 * 作成日: 2004/07/14
 * @author	H.Fujimoto
 */
public class C12JyuniListBean {
	//学年
	private String strGrade = "";
	//クラス
	private String strClass = "";
	//型・科目リスト
	private ArrayList c12KmkList = new ArrayList();

	/*----------*/
	/* Get      */
	/*----------*/
	
	public ArrayList getC12KmkList() {
		return this.c12KmkList;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrGrade() {
		return this.strGrade;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	
	public void setC12KmkList(ArrayList c12KmkList) {
		this.c12KmkList = c12KmkList;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}

}
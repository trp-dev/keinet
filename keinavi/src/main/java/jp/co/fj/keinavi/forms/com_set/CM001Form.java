package jp.co.fj.keinavi.forms.com_set;

import java.util.HashMap;
import java.util.Map;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM001Form extends BaseForm {

	// セッションキー
	public static final String SESSION_KEY = "CommonForm";

	// 個別アクションフォームの入れ物
	private final Map actionForms = new HashMap();	

	private String changed; // 変更があったかどうか
	private String mode; // 登録モード（1が指定されたら閉じる）
	private String check; // フォームのチェックを行うかどうか
	private String save; // 保存したかどうか

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	/**
	 * 個別アクションフォームをクリアする
	 */
	public void clearActionForms() {
		this.actionForms.clear();
	}

	/**
	 * @return
	 */
	public Map getActionForms() {
		return this.actionForms;
	}

	/**
	 * @return
	 */
	public Object getActionForm(String key) {
		return actionForms.get(key);
	}

	/**
	 * @param map
	 */
	public void setActionForm(String key, Object value) {
		actionForms.put(key, value);
	}

	/**
	 * @return
	 */
	public String getChanged() {
		return changed;
	}

	/**
	 * @param string
	 */
	public void setChanged(String string) {
		changed = string;
	}

	/**
	 * @return
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param string
	 */
	public void setMode(String string) {
		mode = string;
	}

	/**
	 * @return
	 */
	public String getCheck() {
		return check;
	}

	/**
	 * @param string
	 */
	public void setCheck(String string) {
		check = string;
	}
	/**
	 * @return
	 */
	public String getSave() {
		return save;
	}

	/**
	 * @param string
	 */
	public void setSave(String string) {
		save = string;
	}

}

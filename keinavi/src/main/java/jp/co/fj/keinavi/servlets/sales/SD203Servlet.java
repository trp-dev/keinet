package jp.co.fj.keinavi.servlets.sales;

import jp.co.fj.keinavi.interfaces.SpecialAppliMenu;
import jp.co.fj.keinavi.interfaces.SpecialAppliStatus;
import jp.co.fj.keinavi.servlets.sales.base.BaseAcceptServlet;

/**
 * 
 * 特例成績データ作成承認（営業部）画面のサーブレットです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD203Servlet extends BaseAcceptServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = 2391177036441296740L;

	/**
	 * @see jp.co.fj.keinavi.servlets.sales.base.BaseSpecialAppliServlet#getMenu()
	 */
	protected SpecialAppliMenu getMenu() {
		return SpecialAppliMenu.ACCEPT_EIGYO;
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.sales.base.BaseAcceptServlet#getUnAcceptedStatus()
	 */
	protected SpecialAppliStatus getUnAcceptedStatus() {
		return SpecialAppliStatus.PENDING_EIGYO;
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.sales.base.BaseAcceptServlet#getAcceptedStatus()
	 */
	protected SpecialAppliStatus getAcceptedStatus() {
		return SpecialAppliStatus.PENDING_KIKAKU;
	}

}

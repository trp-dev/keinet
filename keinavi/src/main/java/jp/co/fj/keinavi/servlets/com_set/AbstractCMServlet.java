package jp.co.fj.keinavi.servlets.com_set;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CommonFormCreator;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 * 共通項目設定基本サーブレット
 * 
 * 2004.7.27	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public abstract class AbstractCMServlet extends DefaultHttpServlet {

	// 非表示となる学校コードのマップ
	// （全国・都道府県）
	public static final Map hiddenCodeMap = new HashMap();

	static {
		hiddenCodeMap.put("99999", "1");
		
		for (int i=1; i<=9; i++) {
			hiddenCodeMap.put("0" + i + "999", "1");
		}
		
		for (int i=10; i<=47; i++) {
			hiddenCodeMap.put(i + "999", "1");
		}
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response
			) throws ServletException, IOException {
		
		// 転送元がNULLまたは共通項目設定以外なら
		// アクションフォームを初期化する
		if (getBackward(request) == null
				|| !getBackward(request).startsWith("cm")) {
			request.getSession(false).setAttribute(
				CM001Form.SESSION_KEY,
				CommonFormCreator.createCommonForm(request)
			);
		}

		// 模試セッション
		final ExamSession examSession = getExamSession(request);

		// 対象模試をセットする
		final CM001Form form = getCommonForm(request);
		request.setAttribute("Exam", examSession.getExamData(
				form.getTargetYear(), form.getTargetExam()));
	}

	/**
	 * セッションから共通アクションフォームを取得する
	 * @return
	 */
	protected CM001Form getCommonForm(final HttpServletRequest request) {
		return (CM001Form) request.getSession(
				false).getAttribute(CM001Form.SESSION_KEY);
	}

}

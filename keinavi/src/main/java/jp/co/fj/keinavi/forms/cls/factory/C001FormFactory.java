/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.cls.factory;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.cls.C001Form;
import jp.co.fj.keinavi.util.CollectionUtil;

import com.fjh.forms.ActionForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class C001FormFactory extends AbstractCFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		// アクションフォーム
		C001Form form = super.createC001Form(request);
		//プロファイルにフォームの値をセットする
		Profile profile = (Profile)request.getSession().getAttribute(Profile.SESSION_KEY);
		Set outItem = CollectionUtil.array2Set(form.getOutItem());		
		//偏差値分布
		if( profile.getItemMap("030101").get("1301") != null){
			if ( ((Short)profile.getItemMap("030101").get("1301")).shortValue() == 1){
				outItem.add("CondDev"); 
			}
		}
		//個人成績
		if( profile.getItemMap("030102").get("1301") != null){
			if ( ((Short)profile.getItemMap("030102").get("1301")).shortValue() == 1){
				outItem.add("CondPersonal");
			}
		}
		//個人成績推移
		if( profile.getItemMap("030103").get("1301") != null){
			if ( ((Short)profile.getItemMap("030103").get("1301")).shortValue() == 1){
				outItem.add("CondTrans");
			}
		} 
		//設問別個人成績
		if( profile.getItemMap("030104").get("1301") != null){
			if ( ((Short)profile.getItemMap("030104").get("1301")).shortValue() == 1){
				outItem.add("CondQue");
			} 
		}
		//志望大学別個人評価
		if( profile.getItemMap("030105").get("1301") != null){
			if ( ((Short)profile.getItemMap("030105").get("1301")).shortValue() == 1){
				outItem.add("CondUniv");
			}
		} 
		
		//formに値をセット
		form.setOutItem( (String[])outItem.toArray(new String[0]) );
		
		return form;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
		C001Form form = (C001Form)request.getAttribute("form");
		//一括出力対象
		Set outItem = CollectionUtil.array2Set(form.getOutItem());
		//プロファイルにフォームの値をセットする
		Profile profile = (Profile)request.getSession().getAttribute(Profile.SESSION_KEY);
		
		//偏差値分布
		if (outItem.contains("CondDev")) profile.getItemMap("030101").put("1301", Short.valueOf("1"));
									else profile.getItemMap("030101").put("1301", Short.valueOf("0")); 
		//個人成績
		if (outItem.contains("CondPersonal")) profile.getItemMap("030102").put("1301", Short.valueOf("1"));
									else profile.getItemMap("030102").put("1301", Short.valueOf("0")); 
		//個人成績推移
		if (outItem.contains("CondTrans")) profile.getItemMap("030103").put("1301", Short.valueOf("1"));
									else profile.getItemMap("030103").put("1301", Short.valueOf("0")); 
		//設問別個人成績
		if (outItem.contains("CondQue")) profile.getItemMap("030104").put("1301", Short.valueOf("1"));
									else profile.getItemMap("030104").put("1301", Short.valueOf("0"));
		//志望大学別個人評価
		if (outItem.contains("CondUniv")) profile.getItemMap("030105").put("1301", Short.valueOf("1"));
									else profile.getItemMap("030105").put("1301", Short.valueOf("0")); 
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap("030101");
	}

}

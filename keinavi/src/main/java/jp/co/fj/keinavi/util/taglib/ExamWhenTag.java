package jp.co.fj.keinavi.util.taglib;

import javax.servlet.jsp.JspException;

/**
 *
 * 指定の試験が特定の試験の種類かを判断するためのカスタムタグ
 * 
 * 2006/08/25    [新規作成]
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class ExamWhenTag extends ExamIfTag {
    
    /**
     * 親の choose タグ
     */
    KNChooseTag choose;
    
    /**
     * @see javax.servlet.jsp.tagext.Tag#doStartTag()
     */
    public int doStartTag() throws JspException {
        
        this.choose = (KNChooseTag) getParent();
        
        if (!choose.gainPermission()) {
            return SKIP_BODY;
        }
        
        result = condition();
        
        if (result) {
            
            choose.subtagSucceeded();
            
            return EVAL_BODY_INCLUDE;
        }

        return SKIP_BODY;
    }

}

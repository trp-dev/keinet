package jp.co.fj.keinavi.excel.data.business;

import java.util.ArrayList;
/**
 * 校内成績分析−他校比較−偏差値分布データクラス
 * 作成日: 2004/07/19
 * @author	A.Iwata
 */
public class B42Item {
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//表フラグ
	private int intHyouFlg = 0;
	//構成比フラグ
	private int intKoseihiFlg = 0;
	//過年度表示フラグ
	private int intNendoFlg = 0;
	// 2019/09/05 QQ)Tanioka 共通テスト対応 ADD START
	//共通テスト英語認定試験CEFR取得状況フラグ
	private int intCheckBoxFlg = 0;
	//受験者がいる試験を対象にするフラグ
	private int intTargetCheckBoxFlg = 0;
	// 2019/09/05 QQ)Tanioka 共通テスト対応 ADD END
	//セキュリティスタンプフラグ
	private int intSecuFlg = 0;
	//データリスト
	private ArrayList b42List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;

	// 2019/09/30 QQ) 共通テスト対応 ADD START
	private B42CefrItem iteB42Cefr = new B42CefrItem();
	// 2019/09/30 QQ) 共通テスト対応 ADD END



	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public int getIntHyouFlg() {
		return this.intHyouFlg;
	}
	public int getIntKoseihiFlg() {
		return this.intKoseihiFlg;
	}
	public int getIntNendoFlg() {
		return this.intNendoFlg;
	}
	// 2019/09/05 QQ)Tanioka 共通テスト対応 ADD START
	public int getIntCheckBoxFlg() {
		return this.intCheckBoxFlg;
	}
	public int getIntTargetCheckBoxFlg() {
		return this.intTargetCheckBoxFlg;
	}
	// 2019/09/05 QQ)Tanioka 共通テスト対応 ADD END
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public ArrayList getB42List() {
		return this.b42List;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}
	// 2019/09/30 QQ) 共通テスト対応 ADD START
	public B42CefrItem getIteB42Cefr() {
		return iteB42Cefr;
	}
	// 2019/09/30 QQ) 共通テスト対応 ADD END

	/*---------------*/
	/* Set */
	/*---------------*/
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setIntHyouFlg(int intHyouFlg) {
		this.intHyouFlg = intHyouFlg;
	}
	public void setIntKoseihiFlg(int intKoseihiFlg) {
		this.intKoseihiFlg = intKoseihiFlg;
	}
	public void setIntNendoFlg(int intNendoFlg) {
		this.intNendoFlg = intNendoFlg;
	}
	// 2019/09/05 QQ)Tanioka 共通テスト対応 ADD START
	public void setIntCheckBoxFlg(int intCheckBoxFlg) {
		this.intCheckBoxFlg = intCheckBoxFlg;
	}
	public void setIntTargetCheckBoxFlg(int intTargetCheckBoxFlg) {
		this.intTargetCheckBoxFlg = intTargetCheckBoxFlg;
	}
	// 2019/09/05 QQ)Tanioka 共通テスト対応 ADD END
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setB42List(ArrayList b42List) {
		this.b42List = b42List;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}
	// 2019/09/30 QQ) 共通テスト対応 ADD START
	public void setIteB42Cefr(B42CefrItem iteB42Cefr) {
		this.iteB42Cefr = iteB42Cefr;
	}
	// 2019/09/30 QQ) 共通テスト対応 ADD END

}

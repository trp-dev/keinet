/*
 * 作成日: 2004/07/09
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.com_set;

import java.io.Serializable;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class SubjectData implements Serializable, Cloneable {

	private String subjectCD; // 型・科目コード
	private short graphDisp; 	// グラフ表示
	
	/**
	 * @return
	 */
	public short getGraphDisp() {
		return graphDisp;
	}

	/**
	 * @return
	 */
	public String getSubjectCD() {
		return subjectCD;
	}

	/**
	 * @param s
	 */
	public void setGraphDisp(short s) {
		graphDisp = s;
	}

	/**
	 * @param string
	 */
	public void setSubjectCD(String string) {
		subjectCD = string;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public Object clone() {
		SubjectData data = new SubjectData();
		data.setSubjectCD(this.subjectCD);
		data.setGraphDisp(this.graphDisp);
		return data;
	}

}

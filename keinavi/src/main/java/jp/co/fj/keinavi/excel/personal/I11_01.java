/**
 * 個人成績分析−面談シート
 * 	Excelファイル編集
 * 作成日: 2004/09/13
 * @author	Ito.Y
 *
 * 2009.11.25   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 */

package jp.co.fj.keinavi.excel.personal;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.personal.I11ChartListBean;
import jp.co.fj.keinavi.excel.data.personal.I11Item;
import jp.co.fj.keinavi.excel.data.personal.I11KyokaHensaListBean;
import jp.co.fj.keinavi.excel.data.personal.I11KyokaSuiiListBean;
import jp.co.fj.keinavi.excel.data.personal.I11ListBean;
import jp.co.fj.keinavi.excel.data.personal.I13HyoukaListBean;
import jp.co.fj.keinavi.excel.data.personal.I13KyokaSeisekiListBean;
import jp.co.fj.keinavi.excel.data.personal.I13SeisekiListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class I11_01 {

    private int	noerror		= 0;	// 正常終了
    private int	errfread	= 1;	// ファイルreadエラー
    private int	errfwrite	= 2;	// ファイルwriteエラー
    private int	errfdata	= 3;	// データ設定エラー

    private CM		cm			= new CM();	// 共通関数用クラス インスタンス

    //2005.04.26 Add 新テスト対応
    //final private String	masterfile		= "I11_01";	// ファイル名
    final private String 	masterfile0 	= "I11_01";	// ファイル名１(新テスト対応用)
    final private String 	masterfile1 	= "I11_01";	// ファイル名２(新テスト対応用)
    private String masterfile = "";

/*
 * 	Excel編集メイン
 * 		I11Item i11Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
     public int i11_01EditExcel(I11Item i11Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
        KNLog log = KNLog.getInstance(null,null,null);

        //2005.04.26 Add 新テスト対応
        //テンプレートの決定
        if (i11Item.getIntMenSheetShubetsuFlg() == 1){
            masterfile = masterfile1;
        } else{
            masterfile = masterfile0;
        }


        try {

            // データセット
            ArrayList	i11List			= i11Item.getI11List();
            Iterator	itr				= i11List.iterator();

            int		fileIndex		= 1;	// ファイルカウンター
            int		maxSheetIndex	= 0;	// シートカウンター

            // マスタExcel読み込み
            HSSFWorkbook	workbook	= null;
            HSSFSheet		workSheet	= null;
            HSSFRow			workRow		= null;
            HSSFCell		workCell	= null;

            // 基本ファイルを読込む
            I11ListBean i11ListBean = new I11ListBean();

            /** データリスト **/
            while( itr.hasNext() ) {
                i11ListBean = (I11ListBean) itr.next();

                // マスタExcel読み込み
                workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
                if( workbook==null ){
                    return errfread;
                }

                // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
            	// 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD START
                // 国語記述用のスタイルを作成
                //HSSFCellStyle cellStyle = this.createStyle(workbook);
                // 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD END
                // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END

                // データセットするシートの選択
                workSheet = workbook.getSheet( Integer.toString(maxSheetIndex+1) );

                // ヘッダ右側に帳票作成日時を表示する
                cm.setHeader(workbook, workSheet);

                // セキュリティスタンプセット
                //2005.01.12 Update 表示列を＋２する
                // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                //String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntMenSheetSecuFlg() ,92 ,95 );
                //workCell = cm.setCell( workSheet, workRow, workCell, 0, 92 );
                //String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntMenSheetSecuFlg() ,103 ,105 );
                //workCell = cm.setCell( workSheet, workRow, workCell, 0, 103 );
                String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntMenSheetSecuFlg() ,98 ,101 );
                workCell = cm.setCell( workSheet, workRow, workCell, 0, 98 );
                // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                workCell.setCellValue(secuFlg);

                // 学校名セット
                workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
                workCell.setCellValue( "学校名　：" + cm.toString(i11ListBean.getStrGakkomei()) + "　学年：" + cm.toString(i11ListBean.getStrGrade())
                + "　クラス名：" + cm.toString(i11ListBean.getStrClass()) + "　クラス番号：" + cm.toString(i11ListBean.getStrClassNum()) );

                // 氏名・性別セット
                String strSex = "";
                if( cm.toString(i11ListBean.getStrSex()).equals("1") ){
                    strSex = "男";
                }else if( cm.toString(i11ListBean.getStrSex()).equals("2") ){
                    strSex = "女";
                }else{
                    strSex = "不明";
                }
                workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );

                if( !cm.toString(i11ListBean.getStrShimei()).equals("")){
                    workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrShimei()) + "（" + cm.toString(i11ListBean.getStrKana()) + "）　性別：" + strSex );
                }else{
                    workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrKana()) + "　性別：" + strSex );
                }

                //受験学力測定テストの注釈
                // 2019/9/18 QQ)Tanouchi 共通テスト対応 DEL START
                //cm.setCell(workSheet, workRow, workCell, 3, 95).setCellValue(PersonalUtil.SeqRec.remark2Jpns(i11ListBean.getI13SeisekiList()));
                // 2019/9/18 QQ)Tanouchi 共通テスト対応 DEL START

                /** 連続個人成績表リスト **/
                // データセット
                ArrayList i13SeisekiList = i11ListBean.getI13SeisekiList();
                Iterator itrI13Seiseki = i13SeisekiList.iterator();

                int kyokaRow = 0;
                while(itrI13Seiseki.hasNext()){

                    //2004.10.17 add
                    int existFlg = 0;
                    //add end

                    I13SeisekiListBean i13seisekiListBean = (I13SeisekiListBean)itrI13Seiseki.next();
                    // 2019/09/18 QQ)Tanouchi 共通テスト対応 DEL START
                    ////受験学力テスト
                    //if (KNUtil.TYPECD_ABILITY.equals(i13seisekiListBean.getStrMshTypeCd())) {
                    //
                    //    workCell = cm.setCell(workSheet, workRow, workCell, 7 + 3 * kyokaRow, 2);
                    //    workCell.setCellValue("スコア");
                    //
                    //    workCell = cm.setCell(workSheet, workRow, workCell, 8 + 3 * kyokaRow, 2);
                    //    workCell.setCellValue("到達エリア");
                    //}
                    // 2019/09/18 QQ)Tanouchi 共通テスト対応 DEL END
                    //2004.10.17 update
                    // 対象模試セット
                    String moshi = cm.setTaisyouMoshi( i13seisekiListBean.getStrMshDate() );	// 模試月取得

                    /** 教科別成績リスト **/
                    // データセット
                    ArrayList i13KyokaSeisekiList = i13seisekiListBean.getI13KyokaSeisekiList();
                    Iterator itrI13KyokaSeiseki = i13KyokaSeisekiList.iterator();

                    String strKykHoji = "";
                    int kyokaTabCol = 0;
                    int kyokaCol = 0;
                    int kyokaCnt = 0;
                    while(itrI13KyokaSeiseki.hasNext()){

                        existFlg = 1;

                        I13KyokaSeisekiListBean i13KyokaSeisekiListBean = (I13KyokaSeisekiListBean)itrI13KyokaSeiseki.next();

                        // 教科コードによって判定 → 2005.01.06 小論文は出力しない
                        String strKyouka = i13KyokaSeisekiListBean.getStrKyokaCd()+" ";
                        strKyouka = strKyouka.substring(0,1);
                        if( strKyouka.equals("6") == false ){
                            if( !strKyouka.equals(strKykHoji) ){
                                switch( Integer.parseInt( strKyouka ) ){
                                    case 1:	//英語
                                        kyokaTabCol = 7;
                                        kyokaCnt = 3;
                                        break;
                                    case 2:	//数学
                                        kyokaTabCol = 19;
                                        kyokaCnt = 3;
                                        break;
                                    case 3:	//国語
                                        kyokaTabCol = 31;
                                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                        // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                                        ////受験学力テスト
                                        //if (KNUtil.TYPECD_ABILITY.equals(i13seisekiListBean.getStrMshTypeCd())) {
                                        //    kyokaCnt = 2;
                                        //} else {
                                        //    kyokaCnt = 1;
                                        //}
                                    	//kyokaCnt = 2;
                                    	kyokaCnt = 1;
                                    	// 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                                    	// 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                        break;
                                    case 4:	//理科
                                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                    	// 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                                        kyokaTabCol = 35;
                                        //kyokaTabCol = 39;
                                        // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                        kyokaCnt = 3;
                                        break;
                                    case 5:	//地歴公民
                                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                    	// 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                                        kyokaTabCol = 47;
                                        //kyokaTabCol = 51;
                                        // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                        kyokaCnt = 2;
                                        break;
                                    case 7:	//総合
                                    case 8:	//総合
                                    case 9:	//総合
                                        kyokaTabCol = 3;
                                        kyokaCnt = 1;
                                        break;
                                }
                                kyokaCol = 0;
                                strKykHoji = strKyouka;
                            }

                            if( kyokaCnt > 0 ){
                                //理科の先頭科目が基礎科目なら、先頭は空欄にする
                                if ("4".equals(strKyouka) && kyokaCol == 0 && "1".equals(i13KyokaSeisekiListBean.getBasicFlg())) {
                                    kyokaCol++;
                                }

                                // 科目名セット
                                workCell = cm.setCell( workSheet, workRow, workCell, 6+3*kyokaRow, kyokaTabCol+4*kyokaCol );
                                workCell.setCellValue( i13KyokaSeisekiListBean.getStrKmkmei() );



                                // 得点セット
                                // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
                                // 国語記述なら得点のみ出力する
//                                if(("3915").equals(i13KyokaSeisekiListBean.getSubCd())) {
//                            		workCell = cm.setCell( workSheet, workRow, workCell, 7+3*kyokaRow, kyokaTabCol+4*kyokaCol );
//                            		//// 出力する際セルの中央に寄せる
//                            		workCell.setCellStyle(cellStyle);
//                                    workCell.setCellValue( i13KyokaSeisekiListBean.getKokugoTokuten() );
//                                //    workCell.setCenter;
//                                }else {
                                // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END
                                // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                if(i13KyokaSeisekiListBean.getIntTokuten() != -999){
                                		workCell = cm.setCell( workSheet, workRow, workCell, 7+3*kyokaRow, kyokaTabCol+4*kyokaCol );
                                		workCell.setCellValue( i13KyokaSeisekiListBean.getIntTokuten() );
                                }

                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                                // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
//                                if(("01").equals(i13KyokaSeisekiListBean.getExcdtype())) {
//                                	workCell = cm.setCell( workSheet, workRow, workCell, 6+3*kyokaRow, 15 );
//                                    workCell.setCellValue( "CEFR" );
//                                    workCell = cm.setCell( workSheet, workRow, workCell, 7+3*kyokaRow, 15 );
//                            		workCell.setCellStyle(cellStyle);
//                                    workCell.setCellValue( i13KyokaSeisekiListBean.getCefrScore() );
//                                }
                                // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END

                                // 偏差値セット
                                if(i13KyokaSeisekiListBean.getFloHensa() != -999.0){
                                    workCell = cm.setCell( workSheet, workRow, workCell, 8+3*kyokaRow, kyokaTabCol+4*kyokaCol );
                                    // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                                    //if (KNUtil.TYPECD_ABILITY.equals(i13seisekiListBean.getStrMshTypeCd())) {
                                    //    workCell.setCellValue(KNUtil.deviationToCenterReachArea(i13KyokaSeisekiListBean.getFloHensa()));
                                    //} else {
                                    //    workCell.setCellValue( i13KyokaSeisekiListBean.getFloHensa() );
                                    //}
                                    workCell.setCellValue( i13KyokaSeisekiListBean.getFloHensa() );
                                    // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                                }

                                // 学力レベルセット
                                if(!"".equals(i13KyokaSeisekiListBean.getStrScholarLevel())) {
                                    workCell = cm.setCell( workSheet, workRow, workCell, 8+3*kyokaRow, kyokaTabCol+4*kyokaCol+3 );
                                    // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                                    //if (!KNUtil.TYPECD_ABILITY.equals(i13seisekiListBean.getStrMshTypeCd())) {
                                    //    workCell.setCellValue(i13KyokaSeisekiListBean.getStrScholarLevel());
                                    //}
                                    workCell.setCellValue(i13KyokaSeisekiListBean.getStrScholarLevel());
                                    // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                                }

                            }
                            // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
                            // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
//                            }
                            // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END
                            // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END
                            kyokaCol++;
                            kyokaCnt--;
                        }
                    }

                    /** 志望大学別評価データリスト **/
                    // データセット
                    ArrayList i13HyoukaList = i13seisekiListBean.getI13HyoukaList();
                    Iterator itrI13Hyouka = i13HyoukaList.iterator();

                    int shiboRow = 0;
                    int shiboCol = 0;
                    while(itrI13Hyouka.hasNext()){
                        I13HyoukaListBean i13HyoukaListBean = (I13HyoukaListBean)itrI13Hyouka.next();

                        // 大学名セット
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                        // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                        //workCell = cm.setCell( workSheet, workRow, workCell, 6+shiboRow+3*kyokaRow, 56+19*shiboCol );
                        //workCell = cm.setCell( workSheet, workRow, workCell, 6+shiboRow+3*kyokaRow, 60+22*shiboCol );
                        workCell = cm.setCell( workSheet, workRow, workCell, 6+shiboRow+3*kyokaRow, 56+23*shiboCol );
                        // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                        workCell.setCellValue( i13HyoukaListBean.getStrDaigakuMei() );

                        // 学部名セット
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                        // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                        //workCell = cm.setCell( workSheet, workRow, workCell, 6+shiboRow+3*kyokaRow, 61+20*shiboCol );
                        //workCell = cm.setCell( workSheet, workRow, workCell, 6+shiboRow+3*kyokaRow, 65+23*shiboCol );
                        workCell = cm.setCell( workSheet, workRow, workCell, 6+shiboRow+3*kyokaRow, 61+24*shiboCol );
                        // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                        workCell.setCellValue( i13HyoukaListBean.getStrGakubuMei() );

                        // 学科名セット
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                        // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                        //workCell = cm.setCell( workSheet, workRow, workCell, 6+shiboRow+3*kyokaRow, 65+21*shiboCol );
                        //workCell = cm.setCell( workSheet, workRow, workCell, 6+shiboRow+3*kyokaRow, 69+24*shiboCol );
                        workCell = cm.setCell( workSheet, workRow, workCell, 6+shiboRow+3*kyokaRow, 65+25*shiboCol );
                        // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                        workCell.setCellValue( i13HyoukaListBean.getStrGakkaMei() );

                        // 評価セット
                        if( cm.toString(i13HyoukaListBean.getStrHyoukaMark()).equals("") &&
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
						// 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
//                        	cm.toString(i13HyoukaListBean.getStrHyoukaMark_engpt()).equals("") &&
//                        	cm.toString(i13HyoukaListBean.getHyoukaJodge()).equals("") &&
//                        	cm.toString(i13HyoukaListBean.getHyoukaNotice()).equals("") &&
						// 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END
                        	// 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
                            cm.toString(i13HyoukaListBean.getStrHyoukaKijyutsu()).equals("") &&
                            cm.toString(i13HyoukaListBean.getStrHyoukaAll()).equals("") ){
                        }else{
                        	// 評価に半角スペースを入れる処理
                            String hyoka = "";
                            if( cm.toString(i13HyoukaListBean.getStrHyoukaMark()).equals("") == false ){
                            	// 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                                //hyoka = i13HyoukaListBean.getStrHyoukaMark() + " :" ;
                            	hyoka = cm.padRight(i13HyoukaListBean.getStrHyoukaMark(),"UTF-8",2) + ":";
                            	// 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                            }else{
                            	// 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                                //hyoka = "　:" ;
                            	hyoka = " " + " :" ;
                            	// 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                            }

                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                            // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
//                            if( cm.toString(i13HyoukaListBean.getStrHyoukaMark_engpt()).equals("")  == false ){
//                           		hyoka += cm.padRight(i13HyoukaListBean.getStrHyoukaMark_engpt(),"UTF-8",2);
//                            }else{
//                            	hyoka += "  ";
//                            }
//                            if( cm.toString(i13HyoukaListBean.getHyoukaJodge()).equals("")  == false ){
//                            	hyoka += " " + i13HyoukaListBean.getHyoukaJodge();
//                            }else{
//                            	hyoka += "   ";
//                            }
//                            if( cm.toString(i13HyoukaListBean.getHyoukaNotice()).equals("")  == false ){
//                            	hyoka += cm.padRight(i13HyoukaListBean.getHyoukaNotice(),"UTF-8",2) + ":";
//                            }else{
//                            	hyoka += "  :";
//                            }
                            // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END
                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END

                            if( cm.toString(i13HyoukaListBean.getStrHyoukaKijyutsu()).equals("") == false ){
                            	// 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                                //hyoka += i13HyoukaListBean.getStrHyoukaKijyutsu() + " :" ;
                            	hyoka += cm.padRight(i13HyoukaListBean.getStrHyoukaKijyutsu(),"UTF-8",2) + ":";
                            	// 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                            }else{
                            	// 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                                //hyoka += "　:" ;
                                hyoka += "  :" ;
                                // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                            }

                            if( cm.toString(i13HyoukaListBean.getStrHyoukaAll()).equals("") == false ){
                            	// 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                                //hyoka += i13HyoukaListBean.getStrHyoukaAll() ;
                            	hyoka += cm.padRight(i13HyoukaListBean.getStrHyoukaAll(), "UTF-8",2);
                            	// 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                            }else{
                            	// 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                                //hyoka += " " ;
                            	hyoka += "  " ;
                            	// 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                            }
                            // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                            // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD START
                            //workCell = cm.setCell( workSheet, workRow, workCell, 6+shiboRow+3*kyokaRow, 69+22*shiboCol );
                            //workCell = cm.setCell( workSheet, workRow, workCell, 6+shiboRow+3*kyokaRow, 73+25*shiboCol );
                            workCell = cm.setCell( workSheet, workRow, workCell, 6+shiboRow+3*kyokaRow, 69+25*shiboCol );
                            // 2019/09/18 QQ)Tanouchi 共通テスト対応 UPD END
                            // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                            workCell.setCellValue( hyoka );
                        }

                        shiboRow++;
                        if(shiboRow >= 3){
                            shiboRow = 0;
                            shiboCol++;
                            if(shiboCol >= 2){
                                break;
                            }
                        }
                    }
                    if(existFlg == 1){
                        workCell = cm.setCell( workSheet, workRow, workCell, 6+3*kyokaRow, 0 );
                        workCell.setCellValue( cm.toString(i13seisekiListBean.getStrMshmei()) + moshi );
                        kyokaRow++;
                    }
                    if(kyokaRow >= 7){
                        break;
                    }
                }

                /** 教科別成績推移グラフデータリスト **/
                // データセット
                ArrayList i11KyokaSuiiList = i11ListBean.getI11KyokaSuiiList();
                Iterator itrI11Kyouka = i11KyokaSuiiList.iterator();

                int suiCol = 0;
                while(itrI11Kyouka.hasNext()){
                    I11KyokaSuiiListBean i11KyokaSuiiListBean = (I11KyokaSuiiListBean) itrI11Kyouka.next();

                    // 模試名セット
                    //2004.10.19 update
                    String moshi = cm.setTaisyouMoshi( i11KyokaSuiiListBean.getStrMshDate() );	// 模試月取得
                    workCell = cm.setCell( workSheet, workRow, workCell, 52, 7+6*suiCol );
                    workCell.setCellValue( cm.toString(i11KyokaSuiiListBean.getStrMshmei()) + moshi );
                    //update end

                    /** 教科別偏差リスト **/
                    // データセット
                    ArrayList i11KyokaHensa = i11KyokaSuiiListBean.getI11KyokaHensaList();
                    Iterator itrI11Hensa = i11KyokaHensa.iterator();

                    int suiRow = 0;
                    while(itrI11Hensa.hasNext()){
                        I11KyokaHensaListBean i11KyokaHensaListBean = (I11KyokaHensaListBean)itrI11Hensa.next();

                        // 偏差値セット
                        if( i11KyokaHensaListBean.getFloHensa() != -999.0 ){
                            workCell = cm.setCell( workSheet, workRow, workCell, 55+suiRow, 7+6*suiCol );
                            workCell.setCellValue( i11KyokaHensaListBean.getFloHensa() );
                        }

                        // 学力レベルセット
                        if( !"".equals(i11KyokaHensaListBean.getStrScholarLevel())){
                            workCell = cm.setCell( workSheet, workRow, workCell, 55+suiRow, 12+6*suiCol );
                            workCell.setCellValue( i11KyokaHensaListBean.getStrScholarLevel() );
                        }

                        suiRow++;
                        if(suiRow >= 6){
                            break;
                        }
                    }
                    suiCol++;
                    if(suiCol >= 7){
                        break;
                    }
                }

                /** バランスチャートデータリスト **/
                // データセット
                ArrayList i11ChartList = i11ListBean.getI11ChartList();
                Iterator itrI11Chart = i11ChartList.iterator();

                int chrtRow = 0;
                while(itrI11Chart.hasNext()){
                    I11ChartListBean i11ChartListBean = (I11ChartListBean) itrI11Chart.next();

                    // 模試名セット
                    //2004.10.19 update
                    String moshi = cm.setTaisyouMoshi( i11ChartListBean.getStrMshDate() );	// 模試月取得
                    workCell = cm.setCell( workSheet, workRow, workCell, 57+chrtRow, 51 );
                    workCell.setCellValue( cm.toString(i11ChartListBean.getStrMshmei()) + moshi );
                    //update end

                    /** 教科別偏差リスト **/
                    // データセット
                    ArrayList i11KyokaHensa = i11ChartListBean.getI11KyokaHensaList();
                    Iterator itrI11Hensa = i11KyokaHensa.iterator();

                    int chrtCol = 0;
                    while(itrI11Hensa.hasNext()){
                        I11KyokaHensaListBean i11KyokaHensaListBean = (I11KyokaHensaListBean)itrI11Hensa.next();

                        // 偏差値セット
                        if( i11KyokaHensaListBean.getFloHensa() != -999.0 ){
                            workCell = cm.setCell( workSheet, workRow, workCell, 57+chrtRow, 66+6*chrtCol );
                            workCell.setCellValue( i11KyokaHensaListBean.getFloHensa() );
                        }
                        // 学力レベルセット
                        if( !"".equals(i11KyokaHensaListBean.getStrScholarLevel())){
                            workCell = cm.setCell( workSheet, workRow, workCell, 57+chrtRow, 66+6*chrtCol+5 );
                            workCell.setCellValue( i11KyokaHensaListBean.getStrScholarLevel() );
                        }

                        chrtCol++;
                        if(chrtCol >= 5){
                            break;
                        }
                    }
                    chrtRow++;
                    if(chrtRow >= 4){
                        break;
                    }
                }

                // Excelファイル保存
                String strFileName = masterfile + "_" + cm.toString(i11ListBean.getStrGrade()) + cm.toString(i11ListBean.getStrClass()) + cm.toString(i11ListBean.getStrClassNum());
                boolean bolRet = false;
                if( (itr.hasNext()==false)&&(fileIndex == 1) ){
                    bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, strFileName, maxSheetIndex+1);
                }else{
                    bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, strFileName, maxSheetIndex+1);
                }
                fileIndex++;

                if( bolRet == false ){
                    return errfwrite;
                }

            }

        } catch(Exception e) {
            log.Err("I11_01","データセットエラー",e.toString());
            return errfdata;
        }

        return noerror;
    }


     // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
 	// 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD START
//     /**
//      * スタイルを作成する
//      * @param  workbook - ワークブック
//      * @return style    - スタイル
//      */
//     private HSSFCellStyle createStyle(HSSFWorkbook workbook) {
//    	 // スタイルを新規作成
//    	 HSSFCellStyle style = workbook.createCellStyle();
//
//         // フォントを定義する
//    	 HSSFFont font = workbook.createFont();
//         // フォント名
//         font.setFontName("ＭＳ ゴシック");
//         // フォントサイズ
//         font.setFontHeightInPoints( (short)10 );
//         // 設定
//         style.setFont(font);
//         // 中央揃え
//         style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
//
//         return style;
//     }
     // 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD END
     // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END

}
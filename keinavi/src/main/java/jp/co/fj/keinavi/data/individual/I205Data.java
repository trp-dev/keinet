/*
 * 作成日: 2004/08/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class I205Data {
	
	//出力用データ
	private String univCd;					//大学コード
	private String univName_Abbr;			//大学名
	private String facultyCd;				//学部コード
	private String facultyName_Abbr;		//学部
	private String deptCd;					//学科コード
	private String deptName_Abbr;			//学科

	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getDeptName_Abbr() {
		return deptName_Abbr;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getFacultyName_Abbr() {
		return facultyName_Abbr;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @return
	 */
	public String getUnivName_Abbr() {
		return univName_Abbr;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setDeptName_Abbr(String string) {
		deptName_Abbr = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyName_Abbr(String string) {
		facultyName_Abbr = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

	/**
	 * @param string
	 */
	public void setUnivName_Abbr(String string) {
		univName_Abbr = string;
	}

}

package jp.co.fj.keinavi.servlets.school;

import java.io.IOException;
import java.sql.Connection;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.com_set.ComSetStatusBean;
import jp.co.fj.keinavi.beans.com_set.CompClassBean;
import jp.co.fj.keinavi.beans.com_set.SubjectBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.AbstractActionFormFactory;
import jp.co.fj.keinavi.forms.school.S001Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * 
 * 2005.05.29	Yoshimoto KAWAI - Totec
 * 				ComSetStatusBeanのセットメンバ追加対応
 * 
 * @author kawai
 *
 */
public class S001Servlet extends AbstractSServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// ログイン情報
		final LoginSession login = getLoginSession(request);
		// プロファイル
		final Profile profile = getProfile(request);
		// 模試セッション
		final ExamSession examSession = getExamSession(request);
		// フィルタリングを有効にするためリクエストにセットする
		request.setAttribute(ExamSession.SESSION_KEY, examSession);
		
		// アクションフォームの取得
		final S001Form form = (S001Form) getActionForm(request,
						"jp.co.fj.keinavi.forms.school.S001Form");
		request.setAttribute("form", form);

		// 校内成績分析トップからの遷移なら対象年度と対象模試を更新
		if (AbstractSServlet.topSet.contains(super.getBackward(request))) {
			profile.setTargetYear(form.getTargetYear());
			profile.setTargetExam(form.getTargetExam());
		// そうでなければ設定値をロード
		} else {
			form.setTargetYear(profile.getTargetYear());
			form.setTargetExam(profile.getTargetExam());
		}

		// トップ画面からの遷移
		if (AbstractSServlet.topSet.contains(super.getBackward(request))) {
			// 変更フラグが立っている必要がある
			if ("1".equals(form.getChanged())) {
				AbstractActionFormFactory
					.getFactory(super.getBackward(request))
					.restore(request);
				profile.setChanged(true);				
			}

		// そうでなければ座標を初期化する
		} else {
			form.setScrollX("0");
			form.setScrollY("0");			
		}

		// 転送先がトップ画面
		if (topSet.contains(getForward(request))) {
			// 対象模試
			ExamData exam = examSession.getExamData(form.getTargetYear(), form.getTargetExam());

			// 対象模試が存在しなければリストの最上位
			if (exam == null) {
				exam = (ExamData) examSession.getExamList(form.getTargetYear()).get(0);
				form.setTargetYear(exam.getExamYear());
				form.setTargetExam(exam.getExamCD());
			}				

			Connection con = null; // DBコネクション
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(false);

				// 共通項目設定プロファイル
				Map cmItem = profile.getItemMap(IProfileCategory.CM);

				// 個別選択なら型のプロファイルを初期化
				if (new Short("2").equals(cmItem.get(IProfileItem.TYPE_SELECTION))) {
					SubjectBean bean = new SubjectBean(login);
					bean.setConnection(null, con);
					bean.setMode("1");
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.execute();
					
					ProfileUtil.getTypeValue(
						profile,
						examSession,
						exam,
						bean,
						true
					);
				}

				// 個別選択なら科目のプロファイルを初期化
				if (new Short("2").equals(cmItem.get(IProfileItem.COURSE_SELECTION))) {
					SubjectBean bean = new SubjectBean(login);
					bean.setConnection(null, con);
					bean.setMode("2");
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.execute();

					ProfileUtil.getCourseValue(
						profile,
						examSession,
						exam,
						bean,
						true
					);
				}

				// 個別選択なら比較対象クラスのプロファイルを初期化
				if (new Short("2").equals(cmItem.get(IProfileItem.CLASS_SELECTION))) {
					CompClassBean bean = new CompClassBean();
					bean.setConnection(null, con);
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.setSchoolCD(login.getUserID());
					bean.execute();

					ProfileUtil.getCompClassValue(
						profile,
						examSession,
						exam,
						bean,
						true
					);
				}

				// 共通項目設定状況Bean
				{
					ComSetStatusBean bean = new ComSetStatusBean();
					bean.setConnection(null, con);
					bean.setProfile(profile);
					bean.setExam(exam);			
					bean.setMode(1);
					bean.setSales(login.isSales());
					bean.setLogin(login);
					bean.setExamSession(examSession);
					bean.execute();
					request.setAttribute("ComSetStatusBean", bean);
				}
	
				// 科目カウントBean
				request.setAttribute("SubjectCountBean",
						createSubjectCountBean(
								con, login.getUserID(),
								form.getTargetYear(), form.getTargetExam()));
				
				// 一時表クリア
				con.commit();
				
			} catch (final Exception e) {
				rollback(con);
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			// 対象模試が新テストかどうか
			request.setAttribute(
				"NewExam",
				new Boolean(KNUtil.isNewExam(exam))
			);
			
			// 校内成績分析トップ画面へ遷移
			forward(request, response, JSP_S001);

		// 不明なら転送
		} else {
			// requestスコープの模試セッションは消しておく
			request.removeAttribute(ExamSession.SESSION_KEY);

			forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

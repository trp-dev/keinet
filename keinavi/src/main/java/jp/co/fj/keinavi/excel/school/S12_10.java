/**
 * 校内成績分析−校内成績　偏差値帯別構成比グラフ（10.0ピッチ）
 * 	Excelファイル編集
 * 作成日: 2004/07/13
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S12BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S12Item;
import jp.co.fj.keinavi.excel.data.school.S12ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S12_10 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
//	private String		strArea			= "A1:AH52";	//印刷範囲
	
	final private String masterfile0 = "S12_10";
	final private String masterfile1 = "S12_10";
	private String masterfile = "";
	final private int intMaxSheetSr = 10;	// 最大シート数の値を入れる

/*
 * 	Excel編集メイン
 * 		S12Item s12Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s12_10EditExcel(S12Item s12Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s12Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S12ListBean s12ListBean = new S12ListBean();
		
		try {
			int		row					= 0;		// 行
			int		col					= 0;		// 列
			float		koseihi				= 0;		// *作成用
			int		setRow				= 0;		// *セット用
			int		kmkCnt				= 0;		// 型・科目数カウンタ
			int		intMaxSheetIndex	= 1;		// シートカウンタ
			int		intBookCngCount		= 0;		// ファイルカウンタ
			boolean	bolSheetCngFlg		= true;	// 改シートフラグ
			boolean	bolBookCngFlg		= true;	// 改ファイルフラグ
			boolean	bolkmkCngFlg		= true;	// 型→科目切替わりフラグ
//add 2004/10/25 T.Sakai データ0件対応
			int		dispKmkFlgCnt		= 0;		// グラフ表示フラグカウンタ
//add end
//add 2004/10/25 T.Sakai セル計算対応
			float		ruikeiKoseihi		= 0;		// 累計構成比
			float		koseihi60			= 0;		// 偏差値60以上の構成比
			float		koseihi50			= 0;		// 偏差値50以上の構成比
//add end
			// データセット
			ArrayList s12List = s12Item.getS12List();
			Iterator itr = s12List.iterator();
			
			while( itr.hasNext() ) {
				s12ListBean = (S12ListBean)itr.next();
				
				if ( s12ListBean.getIntDispKmkFlg()==1 ) {
					//型から科目に変わる時のチェック
					if ( s12ListBean.getStrKmkCd().compareTo("7000") < 0 ) {
						if (bolkmkCngFlg) {
							bolSheetCngFlg = true;
							bolkmkCngFlg = false;
						} else {
							if (kmkCnt>=11) {
								bolSheetCngFlg = true;
							}
						}
					} else {
						if (kmkCnt>=11) {
							bolSheetCngFlg = true;
						}
					}
					if (bolSheetCngFlg && intMaxSheetIndex>intMaxSheetSr) {
						bolBookCngFlg = true;
					}
					if (bolSheetCngFlg) {
						if (bolBookCngFlg) {
							// Excelファイル保存
							if (intMaxSheetIndex!=1) {
								boolean bolRet = false;
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex-1);
								intBookCngCount++;
								if( bolRet == false ){
									return errfwrite;					
								}
							}
							//マスタExcel読み込み
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook==null ){
								return errfread;
							}
							intMaxSheetIndex = 1;
						}
						// データセットするシートの選択
						workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex));
						intMaxSheetIndex++;

						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);
						
						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, s12Item.getIntSecuFlg() ,32 ,33 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
						workCell.setCellValue(secuFlg);
						
						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
						workCell.setCellValue( "学校名　：" + cm.toString(s12Item.getStrGakkomei()) );
				
						// 模試月取得
						String moshi =cm.setTaisyouMoshi( s12Item.getStrMshDate() );
						// 対象模試セット
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s12Item.getStrMshmei()) + moshi);

						bolSheetCngFlg = false;
						bolBookCngFlg = false;
						kmkCnt = 0;
						col = 1;
					}
					row = 39;
					
					// 型・科目名セット
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					workCell.setCellValue( s12ListBean.getStrKmkmei() );
					// 配点セット
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					if ( !cm.toString(s12ListBean.getStrHaitenKmk()).equals("") ) {
						workCell.setCellValue( "（" + s12ListBean.getStrHaitenKmk() + "）" );
					}
					
					row = 42;
					
					// 基本ファイルを読込む
					S12BnpListBean s12BnpListBean = new S12BnpListBean();
					
					// 偏差値データセット
					ArrayList s12BnpList = s12ListBean.getS12BnpList();
					Iterator itrBnp = s12BnpList.iterator();
					
					float sumKoseihi	= 0;
					float min			= 70.0f;
					
					setRow = 0;
//add 2004/10/25 T.Sakai セル計算対応
					ruikeiKoseihi = 0;
					koseihi60 = 0;
					koseihi50 = 0;
//add end
					
					while( itrBnp.hasNext() ) {
						s12BnpListBean = (S12BnpListBean)itrBnp.next();
						if ( s12BnpListBean.getFloBnpMin()==min ) {
							if ( s12BnpListBean.getFloKoseihi() != -999.0 ) {
								sumKoseihi = sumKoseihi + s12BnpListBean.getFloKoseihi();
							}
//add 2004/10/25 T.Sakai セル計算対応
							// 累計構成比セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
							ruikeiKoseihi = ruikeiKoseihi + sumKoseihi;
							workCell.setCellValue( ruikeiKoseihi );
							if (s12BnpListBean.getFloBnpMin()==60.0f) {
								koseihi60 = ruikeiKoseihi;
							}
							if (s12BnpListBean.getFloBnpMin()==50.0f) {
								koseihi50 = ruikeiKoseihi;
							}
//add end
							// 構成比・校内セット
							workCell = cm.setCell( workSheet, workRow, workCell, row++, col+1 );
							workCell.setCellValue( sumKoseihi );
							// *作成用
							if ( koseihi < sumKoseihi ) {
								koseihi = sumKoseihi;
								setRow = row - 1;
							}
							sumKoseihi = 0;
							min = min - 10.0f;
						} else {
							if ( s12BnpListBean.getFloKoseihi() != -999.0 ) {
								sumKoseihi = sumKoseihi + s12BnpListBean.getFloKoseihi();
							}
						}
					}
//add 2004/10/25 T.Sakai セル計算対応
					// 累計構成比セット
					workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
					ruikeiKoseihi = ruikeiKoseihi + sumKoseihi;
					workCell.setCellValue( ruikeiKoseihi );
//add end
					// 構成比・校内セット
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col+1 );
					workCell.setCellValue( sumKoseihi );
					// *作成用
					if ( koseihi < sumKoseihi ) {
						koseihi = sumKoseihi;
						setRow = row - 1;
					}
					// *セット
					workCell = cm.setCell( workSheet, workRow, workCell, setRow, col );
					if ( setRow!=0 ) {
						workCell.setCellValue("*");
					}
					
					// 人数（高校）セット
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					if ( s12ListBean.getIntNinzu() != -999 ) {
						workCell.setCellValue( s12ListBean.getIntNinzu() );
					}
					// 平均点（高校）セット
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					if ( s12ListBean.getFloHeikin() != -999.0 ) {
						workCell.setCellValue( s12ListBean.getFloHeikin() );
					}
					// 平均偏差値（高校）セット
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					if ( s12ListBean.getFloHensa() != -999.0 ) {
						workCell.setCellValue( s12ListBean.getFloHensa() );
					}
//add 2004/10/25 T.Sakai セル計算対応
					// 偏差値60.0以上の構成比セット
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					workCell.setCellValue( koseihi60 );
					// 偏差値50.0以上の構成比セット
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					workCell.setCellValue( koseihi50 );
//add end
					kmkCnt++;
					col = col + 3;
					koseihi = 0;
//add 2004/10/25 T.Sakai データ0件対応
					dispKmkFlgCnt++;
//add end
				}
			}
//add 2004/10/25 T.Sakai データ0件対応
			if ( s12List.size()==0 || dispKmkFlgCnt==0 ) {
				//マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				intMaxSheetIndex = 1;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex));
				intMaxSheetIndex++;
				
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s12Item.getIntSecuFlg() ,32 ,33 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
				workCell.setCellValue(secuFlg);
				
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s12Item.getStrGakkomei()) );
				
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s12Item.getStrMshDate() );
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s12Item.getStrMshmei()) + moshi);
			}
//add end
			// Excelファイル保存
			boolean bolRet = false;
			if(intBookCngCount != 0){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex-1);
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex-1);
			}

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("S12_10","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

}
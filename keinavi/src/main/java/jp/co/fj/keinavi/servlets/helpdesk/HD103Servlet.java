package jp.co.fj.keinavi.servlets.helpdesk;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.csv.CSVLine;
import jp.co.fj.keinavi.beans.csv.CSVReader;
import jp.co.fj.keinavi.beans.helpdesk.HelpDeskRegistBean;
import jp.co.fj.keinavi.forms.helpdesk.HD103Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 *
 * 2005.10.13	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 * @author nino
 *
 */
public class HD103Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		// アクションフォーム
		HD103Form hd103Form = null;
		// requestから取得する
		try {
			hd103Form = (HD103Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.helpdesk.HD103Form");
		} catch (Exception e) {
			throw new ServletException(e);
		}

		if(("hd103").equals(getForward(request)) || getForward(request) == null) {

			// 登録判定
			boolean judge = true;
			int lineNo = 0;

			Connection con = null; // コネクション
			try {

				con = super.getConnectionPool(request); 	// コネクション取得
				con.setAutoCommit(false);

				HelpDeskRegistBean bean = new HelpDeskRegistBean();
				bean.setConnection(null, con);

				// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START
				bean.setContent(hd103Form.getContent());

				// 年次登録を選択
				if ("1".equals(hd103Form.getContent())) {

					// エラー通知パターン
					bean.setErrPattern("2");

					// 契約更新データの削除
					bean.setIsDeleteContract(true);
					bean.execute();
					bean.setIsDeleteContract(false);

				} else {
					// エラー通知パターン
					bean.setErrPattern("1");
				}
				// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END

				// [2004/09/30] 削除処理は必要ない（登録・変更のみ）
				// 契約校登録済リストを取得
				//bean.SelListPacSchool(con);

				CSVReader reader = new CSVReader(new String(hd103Form.getFileName()));
				CSVLine line = null;
				while ((line = reader.readLine()) != null) {
					lineNo++;
					String[] items = line.getItems();
					if (items.length == 11) {
						bean.setHD103Form(hd103Form);

						bean.setLineNo(lineNo);
						bean.setSchoolCode(items[0]);
						bean.setUserId(items[1]);
						bean.setFirstPass(items[2]);
						bean.setUserKubun(items[3]);
						bean.setContractKubun(items[4]);
						bean.setContractTerm(items[5]);
						bean.setValidityTerm(items[6]);
						bean.setSessionNumber(items[7]);
						bean.setPostFunction(items[8]);
						bean.setRgdate(items[9]);
						bean.setUpdate(items[10]);
						bean.execute();
					} else {
						bean.setErrorList(lineNo, "", "", "項目数が違います。");
					}
				}
				if(lineNo == 0){
					bean.setErrorList(lineNo, "", "", "ファイルが読み込めませんでした。");
				}

				// エラーリストが存在すれば更新しない
				if ( bean.getErrorList().size() > 0 ) {
					judge = false;
				}

				// [2004/09/30] 削除処理は必要ない（登録・変更のみ）
				// エラーがなければ登録済リストを元にCSVに存在しないユーザを削除
				//if(judge){
				//	judge = bean.DeletePactSchoolUserId(con);
				//}

				if(judge){

					// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START
					// ログ出力
					bean.setIsMainteLog(true);
					bean.execute();
					bean.setIsMainteLog(false);
					// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END

					con.commit();
				} else {
					rollback(con);
				}

				request.setAttribute("HelpDeskRegistBean", bean);

			} catch (Exception e) {
				rollback(con);
				throw new ServletException(e.toString());
			} finally {
				super.releaseConnectionPool(request, con); // コネクション解放
			}


			// アクションフォーム
			request.setAttribute("hd103Form", hd103Form);

			if (judge) {
				// 一括登録完了画面
				super.forward(request, response, JSP_HD104);
			} else {
				// 一括登録エラー画面
				super.forward(request, response, JSP_HD103);
			}

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#getBackward(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	protected String getBackward(final HttpServletRequest request) {
		request.getClass();
		return "hd102";
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#getForward(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	protected String getForward(final HttpServletRequest request) {
		request.getClass();
		return "hd103";
	}

}

package jp.co.fj.keinavi.servlets.maintenance;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.beans.admin.IndivChangeInfoTableAccess;
import jp.co.fj.keinavi.beans.admin.LastDateBean;
import jp.co.fj.keinavi.beans.maintenance.SelectedStudentBean;
import jp.co.fj.keinavi.beans.maintenance.StudentUpdateBean;
import jp.co.fj.keinavi.beans.recount.RecountLockBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.maintenance.KNStudentData;
import jp.co.fj.keinavi.forms.maintenance.M105Form;
import jp.co.fj.keinavi.util.KNUtil;

import org.apache.commons.lang.StringUtils;

/**
 *
 * 生徒情報編集画面サーブレット
 * 
 * 2005.11.24	[新規作成]
 * 2006.11.02	2006年度改修
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M105Servlet extends M102Servlet {
	
	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {
		
		// アクションフォーム
		final M105Form form = (M105Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.maintenance.M105Form");

		// 主従切替
		if (form.isExchangeMode()) {
			getSelectedStudentBean(request).exchange();
		// 従情報削除
		} else if (form.isRemoveMode()) {
			getSelectedStudentBean(request).removeSubStudent();
		}
		
		// JSPへの遷移処理
		if ("m105".equals(getForward(request))) {
			
			// 生徒一覧・受験届修正からの遷移または主従切替ならフォーム初期化
			if ("m101".equals(getBackward(request))
					|| "m201".equals(getBackward(request))
					|| form.isExchangeMode()) {
				syncSelectedStudent(request); // 選択済み生徒を同期
				form.init(getMainStudentData(request));
			}
			readyForward(request, form);
			forward2Jsp(request, response);

		// 不明なら転送
		} else {
			
			// 主情報のみ変更
			if (form.isRegistMode()) {
				// 更新処理
				update(request, form);
				// 成功
				if (getErrorMessage(request) == null) {
					initStudentSession(request);
					actionLog(request, "802");
				// 失敗
				} else {
					readyForward(request, form);
					forward2Jsp(request, response, "m105");
					return;
				}
			// 登録・統合
			} else if ("m107".equals(getForward(request))) {
				// 強制登録でなければ重複チェック
				if (!"1".equals(form.getForcingFlag())
						&& getErrorMessage(request) == null) {
					validateDuplication(request, form);
				}
				// エラー発生なら元の画面へ
				if (getErrorMessage(request) != null) {
					readyForward(request, form);
					forward2Jsp(request, response, "m105");
					return;
				}
			}
			
			dispatch(request, response);
		}
	}
	
	// JSP転送の準備処理
	private void readyForward(final HttpServletRequest request,
			M105Form form) throws ServletException {
		request.setAttribute("CurrentYear", KNUtil.getCurrentYear());
		request.setAttribute("form", form);
	}
	
	// 生徒情報の更新処理
	private void update(final HttpServletRequest request,
			final M105Form form) throws ServletException {
		
		// ログイン情報
		final LoginSession login = getLoginSession(request);
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);
			
			// 再集計ロック失敗
			if (!RecountLockBean.lock(con, login.getUserID())) {
				con.rollback();
				request.setAttribute("ErrorMessage",
						RecountLockBean.getErrorMessage());
			} else {
				// データ更新日
				LastDateBean.setLastDate1(con, login.getUserID());

				// DB更新
				final StudentUpdateBean bean = new StudentUpdateBean(
						login.getUserID());
				bean.setConnection(null, con);
				bean.setStudent(form.toStudentData());
				// 強制登録でなければ重複チェック
				if (!"1".equals(form.getForcingFlag())) {
					bean.validateDuplication(null);
				}
				// 実行
				bean.execute();
				// 統合処理はないので集計学年・クラスを更新
				bean.updateCntGrade();
				
				/* 更新：変更成績情報テーブル */
				IndivChangeInfoTableAccess.updateIndividual(con, form.getTargetIndividualId());
				
				con.commit();
			}
			
		// 想定されているエラー
		} catch (final KNBeanException e) {
			rollback(con);
			request.setAttribute("ErrorMessage", e.getMessage());
			request.setAttribute("ErrorCode", e.getErrorCode());
		} catch (final Exception e) {
			rollback(con);
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
	}

	// 履歴情報の重複チェックを行う
	private void validateDuplication(final HttpServletRequest request,
			final M105Form form) throws ServletException {
		
		// ログイン情報
		final LoginSession login = getLoginSession(request);
		
		Connection con = null;
		try {
			con = getConnectionPool(request);

			final StudentUpdateBean bean = new StudentUpdateBean(
					login.getUserID());
			bean.setConnection(null, con);
			bean.setStudent(form2OverwrittenData(
					getSelectedStudentBean(request), form));
			bean.validateDuplication(
					getSubStudentData(request).getIndividualId());
		// 想定されているエラー
		} catch (final KNBeanException e) {
			request.setAttribute("ErrorMessage", e.getMessage());
			request.setAttribute("ErrorCode", e.getErrorCode());
		} catch (final Exception e) {
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
	}
	
	// 従情報で上書きした生徒データを返す
	protected KNStudentData form2OverwrittenData(
			final SelectedStudentBean bean,
			final M105Form form) {
		
		final KNStudentData student = form.toStudentData();
		
		// かな氏名を上書き
		if (!StringUtils.isEmpty(form.getOverwriteNameKana())) {
			final KNStudentData data = bean.getStudentData(
					form.getOverwriteNameKana());
			student.setNameKana(data.getNameKana());
		}
		
		// 漢字氏名を上書き
		if (!StringUtils.isEmpty(form.getOverwriteNameKanji())) {
			final KNStudentData data = bean.getStudentData(
					form.getOverwriteNameKanji());
			student.setNameKanji(data.getNameKanji());
		}
		
		// 生年月日を上書き
		if (!StringUtils.isEmpty(form.getOverwriteBirthday())) {
			final KNStudentData data = bean.getStudentData(
					form.getOverwriteBirthday());
			student.setBirthday(data.getBirthday());
		}
		
		// 電話番号を上書き
		if (!StringUtils.isEmpty(form.getOverwriteTelNo())) {
			final KNStudentData data = bean.getStudentData(
					form.getOverwriteTelNo());
			student.setTelNo(data.getTelNo());
		}
		
		return student;
	}

}

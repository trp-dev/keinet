package jp.co.fj.keinavi.servlets.com_set;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.AbstractActionFormFactory;
import jp.co.fj.keinavi.forms.com_set.CM001Form;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM002Servlet extends AbstractCMServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 共通処理
		super.execute(request, response);

		// ログイン情報
		LoginSession login = super.getLoginSession(request);
		// プロファイル
		Profile profile = super.getProfile(request);
		// 共通アクションフォーム - session scope
		CM001Form scform = super.getCommonForm(request);
		// 共通アクションフォーム - request scope
		CM001Form rcform = (CM001Form)
			super.getActionForm(request, "jp.co.fj.keinavi.forms.com_set.CM001Form");

		// JSP
		if ("cm002".equals(super.getForward(request))) {
			// プロファイルに値を戻す
			AbstractActionFormFactory.getFactory("cm101").restore(request);
			AbstractActionFormFactory.getFactory("cm201").restore(request);
			AbstractActionFormFactory.getFactory("cm401").restore(request);
			AbstractActionFormFactory.getFactory("cm601").restore(request);

			// 営業でなければ比較対象年度・比較対象クラスも
			if (!login.isSales()) {
				AbstractActionFormFactory.getFactory("cm301").restore(request);
				AbstractActionFormFactory.getFactory("cm501").restore(request);
			}

			// プロファイルを変更したのでフラグを立てておく
			profile.setChanged(true);
			// 変更フラグは落とす
			scform.setChanged("0");
			// 保存フラグを立てる
			scform.setSave("1");

			// 閉じる
			if ("1".equals(rcform.getMode())) {		
				super.forward(request, response, JSP_CLOSE);
			// 完了画面
			} else {
				super.forward(request, response, JSP_CM002);
			}
		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

/*
 * 作成日: 2004/08/10
 *
 */
package jp.co.fj.keinavi.forms.individual;

/**
 * 
 * @author Totec) T.Yamada
 *
 * 2005.6.20 	T.Yamada 	[1]カレンダー横移動用ボタン追加に伴う修正
 * 
 */
public class I306Form extends IOnJudgeForm{

	/**
	 * 右ボタン
	 */
	public static final String DATE_FORWARD = "1";//[1] add
	
	/**
	 * 左ボタン
	 */
	public static final String DATE_BACKWARD = "-1";//[1] add

	/**
	 * 更新ボタン 
	 */
	public static final String DATE_REFRESH = "0";//[1] add
	
	/**
	 * 最大表示日を超えているか
	 */
	private boolean max = false;//[1] add
	
	/**
	 * 最小表示日を超えているか
	 */
	private boolean min = false;//[1] add

	/**
	 * 生徒ID
	 */
	private String targetPersonId;
	
	/**
	 * 
	 */
	private String year;
	
	/**
	 * コンボボックスで指定された月
	 */
	private String month;
	
	/**
	 * コンボボックスで指定された日
	 */
	private String date;
	
	/**
	 * 現在表示中の月(左右ボタンの押下時に使用)
	 */
	private String originalMonth;//[1] add
	
	/**
	 * 現在表示中の日(左右ボタンの押下時に使用)
	 */
	private String originalDate;//[1] add
	
	/**
	 * 右ボタン=rightDay、左ボタン=leftDay
	 */
	private String refType;//[1] add
	
	/**
	 * 個人成績分析共通-印刷対象生徒（判定対象生徒)
	 */
	private String printStudent;
	
	/**
	 * セキュリティスタンプ
	 */
	private String printStamp;
	
	/**
	 * 出力フォーマット
	 */
	private String textFormat[];
	
	/**
	 * 印刷ボタン押下時パラメータ
	 */
	private String printStatus;
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.individual.ICommonForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ
		
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @return
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @return
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param string
	 */
	public void setDate(String string) {
		date = string;
	}

	/**
	 * @param string
	 */
	public void setMonth(String string) {
		month = string;
	}

	/**
	 * @return
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param string
	 */
	public void setYear(String string) {
		year = string;
	}

	/**
	 * @return
	 */
	public String getPrintStamp() {
		return printStamp;
	}

	/**
	 * @param string
	 */
	public void setPrintStamp(String string) {
		printStamp = string;
	}

	/**
	 * @return
	 */
	public String getPrintStudent() {
		return printStudent;
	}

	/**
	 * @param string
	 */
	public void setPrintStudent(String string) {
		printStudent = string;
	}

	/**
	 * @return
	 */
	public String[] getTextFormat() {
		return textFormat;
	}

	/**
	 * @param strings
	 */
	public void setTextFormat(String[] strings) {
		textFormat = strings;
	}

	/**
	 * @return
	 */
	public String getPrintStatus() {
		return printStatus;
	}

	/**
	 * @param string
	 */
	public void setPrintStatus(String string) {
		printStatus = string;
	}

	/**
	 * @return
	 */
	public String getOriginalDate() {
		return originalDate;
	}

	/**
	 * @return
	 */
	public String getOriginalMonth() {
		return originalMonth;
	}

	/**
	 * @return
	 */
	public String getRefType() {
		return refType;
	}

	/**
	 * @param string
	 */
	public void setOriginalDate(String string) {
		originalDate = string;
	}

	/**
	 * @param string
	 */
	public void setOriginalMonth(String string) {
		originalMonth = string;
	}

	/**
	 * @param string
	 */
	public void setRefType(String string) {
		refType = string;
	}

	/**
	 * @return
	 */
	public boolean isMax() {
		return max;
	}

	/**
	 * @return
	 */
	public boolean isMin() {
		return min;
	}

	/**
	 * @param b
	 */
	public void setMax(boolean b) {
		max = b;
	}

	/**
	 * @param b
	 */
	public void setMin(boolean b) {
		min = b;
	}

}

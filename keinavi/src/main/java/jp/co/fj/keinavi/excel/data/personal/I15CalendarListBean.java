package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
import java.util.ArrayList;
/**
 * Βl¬ΡͺΝ|σ±pΏ|σ±εwXPW[\ σ±εwΚXPW[
 * μ¬ϊ: 2004/07/21
 * @author	A.Iwata
 */
public class I15CalendarListBean {
	//εwΌ
	private String strDaigakuMei = "";
	//w
	private String strGakubuMei = "";
	//wΘ
	private String strGakkaMei = "";
//S 2004.09.03 A.Hasegawa
  	//‘p^[tO
  	private int intPluralFlg = 0;
//E 2004.09.03
	//εwζͺ
	private String strDaigakuKbn = "";
	//ϊφR[h
	private String strNittei = "";
//	//ό±ζͺ
//	private int intShikenkbn = 0;
//S 2004.9.9 C.Murata	
//	//}Τ
//	private int intSeqNo = 0;
//E 2004.9.9 C.Murata
//	//nϋ±n
//	private String strShikenti = "";
	//nϋ±n1-10
	private String strShikenti1 = "";
	private String strShikenti2 = "";
	private String strShikenti3 = "";
	private String strShikenti4 = "";
	private String strShikenti5 = "";
	private String strShikenti6 = "";
	private String strShikenti7 = "";
	private String strShikenti8 = "";
	private String strShikenti9 = "";
	private String strShikenti10 = "";
	//σ±ϊ1-10
	private String strJukenbi1 = "";
	private String strJukenbi2 = "";
	private String strJukenbi3 = "";
	private String strJukenbi4 = "";
	private String strJukenbi5 = "";
	private String strJukenbi6 = "";
	private String strJukenbi7 = "";
	private String strJukenbi8 = "";
	private String strJukenbi9 = "";
	private String strJukenbi10 = "";
	//oθχΨϊ
	private String strSyutsuganbi = "";
	//i­\ϊ
	private String strGoukakubi = "";
	//όwθ±χΨϊ
	private String strTetsudukibi = "";
	//Z^[{[_[Ύ_¦
	private float floBorderTokuritsu = 0;
	//οΥNΞ·l
	private float floRankHensa = 0;
	//οΥNΌ
	private String strRankName;
	//iΒ\«»θXg
	private ArrayList i15HyoukaList = new ArrayList();
	//»θπXg
	private ArrayList i15HanteiRirekiList = new ArrayList();
//	//pκ
//	private int intGaikokugo = 0;
//	//w
//	private int intSugaku = 0;
//	//κ
//	private int intKokugo = 0;
//	//Θ
//	private int intRika = 0;
//	//nπ
//	private int intChireki = 0;
//	//φ―
//	private int intKomin = 0;
//	//»ΜΌ
//	private int intSonota = 0;
	//Q³Θz_
	private String strCourseAllot = "";

	//υl
	private String strBiko = "";

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrDaigakuMei() {
		return this.strDaigakuMei;
	}
	public String getStrGakubuMei() {
		return this.strGakubuMei;
	}
	public String getStrGakkaMei() {
		return this.strGakkaMei;
	}
//S 2004.09.03 A.Hasegawa
  	public int getintPluralFlg() {
	  	return this.intPluralFlg;
  	}
//E 2004.09.03
	public String getStrDaigakuKbn() {
		return this.strDaigakuKbn;
	}
//	public int getIntShikenkbn() {
//		return this.intShikenkbn;
//	}
//	public int getIntSeqNo() {
//		return this.intSeqNo;
//	}
	public String getStrShikenti1() {
		return this.strShikenti1;
	}
	public String getStrShikenti2() {
		return this.strShikenti2;
	}
	public String getStrShikenti3() {
		return this.strShikenti3;
	}
	public String getStrShikenti4() {
		return this.strShikenti4;
	}
	public String getStrShikenti5() {
		return this.strShikenti5;
	}
	public String getStrShikenti6() {
		return this.strShikenti6;
	}
	public String getStrShikenti7() {
		return this.strShikenti7;
	}
	public String getStrShikenti8() {
		return this.strShikenti8;
	}
	public String getStrShikenti9() {
		return this.strShikenti9;
	}
	public String getStrShikenti10() {
		return this.strShikenti10;
	}
	public String getStrJukenbi1() {
		return this.strJukenbi1;
	}
	public String getStrJukenbi2() {
		return this.strJukenbi2;
	}
	public String getStrJukenbi3() {
		return this.strJukenbi3;
	}
	public String getStrJukenbi4() {
		return this.strJukenbi4;
	}
	public String getStrJukenbi5() {
		return this.strJukenbi5;
	}
	public String getStrJukenbi6() {
		return this.strJukenbi6;
	}
	public String getStrJukenbi7() {
		return this.strJukenbi7;
	}
	public String getStrJukenbi8() {
		return this.strJukenbi8;
	}
	public String getStrJukenbi9() {
		return this.strJukenbi9;
	}
	public String getStrJukenbi10() {
		return this.strJukenbi10;
	}
	public String getStrSyutsuganbi() {
		return this.strSyutsuganbi;
	}
	public String getStrGoukakubi() {
		return this.strGoukakubi;
	}
	public String getStrTetsudukibi() {
		return this.strTetsudukibi;
	}
	public float getFloBorderTokuritsu() {
		return this.floBorderTokuritsu;
	}
	public float getFloRankHensa() {
		return this.floRankHensa;
	}
	public ArrayList getI15HyoukaList() {
		return this.i15HyoukaList;
	}
	public ArrayList getI15HanteiRirekiList() {
		return this.i15HanteiRirekiList;
	}
//	public int getIntGaikokugo() {
//		return this.intGaikokugo;
//	}
//	public int getIntSugaku() {
//		return this.intSugaku;
//	}
//	public int getIntKokugo() {
//		return this.intKokugo;
//	}
//	public int getIntRika() {
//		return this.intRika;
//	}
//	public int getIntChireki() {
//		return this.intChireki;
//	}
//	public int getIntKomin() {
//		return this.intKomin;
//	}
//	public int getIntSonota() {
//		return this.intSonota;
//	}
	public String getStrBiko() {
		return this.strBiko;
	}
	public int getIntPluralFlg() {
		return this.intPluralFlg;
	}
	public String getStrNittei() {
		return this.strNittei;
	}
	public String getStrCourseAllot() {
		return this.strCourseAllot;
	}

		
	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrDaigakuMei(String strDaigakuMei) {
		this.strDaigakuMei = strDaigakuMei;
	}
	public void setStrGakubuMei(String strGakubuMei) {
		this.strGakubuMei = strGakubuMei;
	}
	public void setStrGakkaMei(String strGakkaMei) {
		this.strGakkaMei = strGakkaMei;
	}
//S 2004.09.03 A.Hasegawa
  	public void setintPluralFlg(int intPluralFlg) {
	  	this.intPluralFlg = intPluralFlg;
  	}
//E 2004.09.03
	public void setStrDaigakuKbn(String strDaigakuKbn) {
		this.strDaigakuKbn = strDaigakuKbn;
	}
//	public void setIntShikenkbn(int intShikenkbn) {
//		this.intShikenkbn = intShikenkbn;
//	}
//	public void setIntSeqNo(int intSeqNo) {
//		this.intSeqNo = intSeqNo;
//	}
//	public void setStrShikenti(String strShikenti) {
//		this.strShikenti = strShikenti;
//	}
	public void setStrShikenti1(String strShikenti1) {
		this.strShikenti1 = strShikenti1;
	}
	public void setStrShikenti2(String strShikenti2) {
		this.strShikenti2 = strShikenti2;
	}
	public void setStrShikenti3(String strShikenti3) {
		this.strShikenti3 = strShikenti3;
	}
	public void setStrShikenti4(String strShikenti4) {
		this.strShikenti4 = strShikenti4;
	}
	public void setStrShikenti5(String strShikenti5) {
		this.strShikenti5 = strShikenti5;
	}
	public void setStrShikenti6(String strShikenti6) {
		this.strShikenti6 = strShikenti6;
	}
	public void setStrShikenti7(String strShikenti7) {
		this.strShikenti7 = strShikenti7;
	}
	public void setStrShikenti8(String strShikenti8) {
		this.strShikenti8 = strShikenti8;
	}
	public void setStrShikenti9(String strShikenti9) {
		this.strShikenti9 = strShikenti9;
	}
	public void setStrShikenti10(String strShikenti10) {
		this.strShikenti10 = strShikenti10;
	}
	public void setStrJukenbi1(String strJukenbi1) {
		this.strJukenbi1 = strJukenbi1;
	}
	public void setStrJukenbi2(String strJukenbi2) {
		this.strJukenbi2 = strJukenbi2;
	}
	public void setStrJukenbi3(String strJukenbi3) {
		this.strJukenbi3 = strJukenbi3;
	}
	public void setStrJukenbi4(String strJukenbi4) {
		this.strJukenbi4 = strJukenbi4;
	}
	public void setStrJukenbi5(String strJukenbi5) {
		this.strJukenbi5 = strJukenbi5;
	}
	public void setStrJukenbi6(String strJukenbi6) {
		this.strJukenbi6 = strJukenbi6;
	}
	public void setStrJukenbi7(String strJukenbi7) {
		this.strJukenbi7 = strJukenbi7;
	}
	public void setStrJukenbi8(String strJukenbi8) {
		this.strJukenbi8 = strJukenbi8;
	}
	public void setStrJukenbi9(String strJukenbi9) {
		this.strJukenbi9 = strJukenbi9;
	}
	public void setStrJukenbi10(String strJukenbi10) {
		this.strJukenbi10 = strJukenbi10;
	}
	public void setStrSyutsuganbi(String strSyutsuganbi) {
		this.strSyutsuganbi = strSyutsuganbi;
	}
	public void setStrGoukakubi(String strGoukakubi) {
		this.strGoukakubi = strGoukakubi;
	}
	public void setStrTetsudukibi(String strTetsudukibi) {
		this.strTetsudukibi = strTetsudukibi;
	}
	public void setFloBorderTokuritsu(float floBorderTokuritsu) {
		this.floBorderTokuritsu = floBorderTokuritsu;
	}
	public void setFloRankHensa(float floRankHensa) {
		this.floRankHensa = floRankHensa;
	}
	public void setI15HyoukaList(ArrayList i15HyoukaList) {
		this.i15HyoukaList = i15HyoukaList;
	}
	public void setI15HanteiRirekiList(ArrayList i15HanteiRirekiList) {
		this.i15HanteiRirekiList = i15HanteiRirekiList;
	}
//	public void setIntGaikokugo(int intGaikokugo) {
//		this.intGaikokugo = intGaikokugo;
//	}
//	public void setIntSugaku(int intSugaku) {
//		this.intSugaku = intSugaku;
//	}
//	public void setIntKokugo(int intKokugo) {
//		this.intKokugo = intKokugo;
//	}
//	public void setIntRika(int intRika) {
//		this.intRika = intRika;
//	}
//	public void setIntChireki(int intChireki) {
//		this.intChireki = intChireki;
//	}
//	public void setIntKomin(int intKomin) {
//		this.intKomin = intKomin;
//	}
//	public void setIntSonota(int intSonota) {
//		this.intSonota = intSonota;
//	}
	public void setStrBiko(String strBiko) {
		this.strBiko = strBiko;
	}
	public void setIntPluralFlg(int intPluralFlg) {
		this.intPluralFlg = intPluralFlg;
	}
	public void setStrNittei(String strNittei) {
		this.strNittei = strNittei;
	}
	public void setStrCourseAllot(String strCourseAllot) {
		this.strCourseAllot = strCourseAllot;
	}
	/**
	 * @return strRankName πί΅ά·B
	 */
	public String getStrRankName() {
		return strRankName;
	}
	/**
	 * @param pStrRankName έθ·ι strRankNameB
	 */
	public void setStrRankName(String pStrRankName) {
		strRankName = pStrRankName;
	}

}

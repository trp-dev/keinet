/**
 * 校内成績分析−過回比較　偏差値分布
 * 	Excelファイル編集
 * 作成日: 2004/08/16
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S52BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S52Item;
import jp.co.fj.keinavi.excel.data.school.S52ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S52_02 {

	private int noerror	= 0;		// 正常終了
	private int errfread	= 1;		// ファイルreadエラー
	private int errfwrite	= 2;		// ファイルwriteエラー
	private int errfdata	= 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	final private String	masterfile0	= "S52_02";		// ファイル名
	final private String	masterfile1	= "NS52_02";	// ファイル名
	private String	masterfile	= "";					// ファイル名
	final private int[]	tabRow		= {4,33};		// 表の基準点
	final private int[]	tabCol		= {1,6,11,16,21,26,31};	// 表の基準点


/*
 * 	Excel編集メイン
 * 		S52Item s52Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s52_02EditExcel(S52Item s52Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("S52_02","S52_02帳票作成開始","");

		// マスタExcel読み込み
		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;


		//テンプレートの決定
		if (s52Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		//マスタExcel読み込み
		workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
		if( workbook==null ){
			return errfread;
		}
		
		// 基本ファイルを読込む
		S52ListBean s52ListBean = new S52ListBean();
		
		try {
			
			// データセット
			ArrayList	s52List			= s52Item.getS52List();
			Iterator	itr				= s52List.iterator();
			int		maxSheetIndex	= 0;	//シートカウンター
			int		intMaxSheetSr	= 50;	//MAXシート数
			int		intBookCngCount	= 1;	// ブックカウンター
			int		row				= 0;	// 行
			int 		setRow			= -1;	// *セット用
			int 		ninzu			= 0;	// *作成用
			int 		maxNo			= 19;	// 最大表示件数
			int		hyouCnt			= 0;	// 値範囲：０〜１　(表カウンター)
			int		kakaiCnt		= 0;	// 回カウンター
			int		kmkCd			= 0;	// 科目コード保持
			String		moshi			= "";	// 模試月
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	kmkmeiFlg		= true;// true:型・科目名表示
//add 2004/10/27 T.Sakai セル計算対応
			int		ruikeiNinzu		= 0;	// 累計人数
			float		ruikeiKoseihi	= 0;	// 累計構成比
//add end

			while( itr.hasNext() ) {
				s52ListBean = (S52ListBean)itr.next();

				// 基本ファイルを読み込む
				S52BnpListBean		s52BnpListBean		= new S52BnpListBean();

				//データの保持
				ArrayList	s52BnpList		= s52ListBean.getS52BnpList();
				Iterator	itrS52Bnp		= s52BnpList.iterator();

				// カウンターチェック
				if (kakaiCnt!=0){
					if ( ( (Integer.parseInt(s52ListBean.getStrKmkCd()) != kmkCd )||(kakaiCnt>=7) )){
						kakaiCnt = 0;
						kmkmeiFlg=true;
						hyouCnt++;
						if(hyouCnt >= 2){
							hyouCnt = 0;
							bolSheetCngFlg =true;
						}
					}
				}

				//型･科目が２個になったとき、下段で型から科目の表示に変わるときは改シート
				if ( bolSheetCngFlg ) {
					if(maxSheetIndex >= intMaxSheetSr){
						// Excelファイル保存
						boolean bolRet = false;
						bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, maxSheetIndex);

						if( bolRet == false ){
							return errfwrite;					
						}

						//マスタExcel読み込み
						workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
						if( workbook==null ){
							return errfread;
						}
						intBookCngCount++;
						maxSheetIndex = 0;
						
					}

					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);
					maxSheetIndex++;

					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);

					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, s52Item.getIntSecuFlg() ,34 ,35 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 34 );
					workCell.setCellValue(secuFlg);

					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
					workCell.setCellValue( "学校名　：" + cm.toString(s52Item.getStrGakkomei()) );

					// 対象模試セット
					moshi =cm.setTaisyouMoshi( s52Item.getStrMshDate() );	// 模試月取得
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s52Item.getStrMshmei()) + moshi);

					//　改表フラグをFalseにする
					bolSheetCngFlg	=false;
				}

				// 型名・配点セット
				if(kmkmeiFlg){
					String haiten = "";
					if ( !cm.toString(s52ListBean.getStrHaitenKmk()).equals("") ) {
						haiten = "（" + s52ListBean.getStrHaitenKmk() + "）";
					}
					workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt], 0 );
					workCell.setCellValue( "型・科目：" + cm.toString(s52ListBean.getStrKmkmei()) + haiten );
					kmkCd=Integer.parseInt(s52ListBean.getStrKmkCd());

					kmkmeiFlg=false;
				}

				// 模試名セット
				moshi =cm.setTaisyouMoshi( s52ListBean.getStrMshDate() );	// 模試月取得
				workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+1, tabCol[kakaiCnt] );
				workCell.setCellValue( cm.toString(s52ListBean.getStrMshmei()) + moshi);

				// 合計人数セット
				if ( s52ListBean.getIntNinzu() != -999 ) {
					workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+23, tabCol[kakaiCnt] );
					workCell.setCellValue( s52ListBean.getIntNinzu() );
				}

				// 平均点セット
				if ( s52ListBean.getFloHeikin() != -999.0 ) {
					workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+24, tabCol[kakaiCnt] );
					workCell.setCellValue( s52ListBean.getFloHeikin() );
				}

				// 平均偏差セット
				if ( s52ListBean.getFloHensa() != -999.0 ) {
					workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+25, tabCol[kakaiCnt] );
					workCell.setCellValue( s52ListBean.getFloHensa() );
				}

				// 偏差値データセット
				row	= 0;
//add 2004/10/27 T.Sakai セル計算対応
				ruikeiNinzu = 0;
				ruikeiKoseihi = 0;
//add end
				
				while(itrS52Bnp.hasNext()){
					s52BnpListBean	= (S52BnpListBean) itrS52Bnp.next();

					// 人数セット
					if ( s52BnpListBean.getIntNinzu() != -999 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+4+row, tabCol[kakaiCnt]+1);
						workCell.setCellValue( s52BnpListBean.getIntNinzu() );

						// *セット準備
						if( s52BnpListBean.getIntNinzu() > ninzu ){
							ninzu = s52BnpListBean.getIntNinzu();
							setRow = row;
						}
					}

					// 構成比セット
					if ( s52BnpListBean.getFloKoseihi() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+4+row, tabCol[kakaiCnt]+3);
						workCell.setCellValue( s52BnpListBean.getFloKoseihi() );
					}
//add 2004/10/27 T.Sakai セル計算対応
					// 累計人数セット
					workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+4+row, tabCol[kakaiCnt]+2);
					if ( s52BnpListBean.getIntNinzu() != -999 ) {
						ruikeiNinzu = ruikeiNinzu + s52BnpListBean.getIntNinzu();
					}
					workCell.setCellValue( ruikeiNinzu );
					// 累計構成比セット
					workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+4+row, tabCol[kakaiCnt]+4);
					if ( s52BnpListBean.getFloKoseihi() != -999.0 ) {
						ruikeiKoseihi = ruikeiKoseihi + s52BnpListBean.getFloKoseihi();
					}
					workCell.setCellValue( ruikeiKoseihi );

					// 得点用のときは処理しない
					if (s52Item.getIntShubetsuFlg() == 1){
					} else{
						if (s52BnpListBean.getFloBnpMin()==60.0f) {
							// 偏差値60以上の人数セット
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+26, tabCol[kakaiCnt] );
							workCell.setCellValue( ruikeiNinzu );
							// 偏差値60以上の構成比セット
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+26, tabCol[kakaiCnt]+3 );
							workCell.setCellValue( ruikeiKoseihi );
						}
						if (s52BnpListBean.getFloBnpMin()==50.0f) {
							// 偏差値50以上の人数セット
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+27, tabCol[kakaiCnt] );
							workCell.setCellValue( ruikeiNinzu );
							// 偏差値50以上の構成比セット
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+27, tabCol[kakaiCnt]+3 );
							workCell.setCellValue( ruikeiKoseihi );
						}
					}
//add end

					row++;

					// *セット
					if(row >=maxNo){
						if(setRow!=-1){
							workCell = cm.setCell( workSheet, workRow, workCell,tabRow[hyouCnt]+4+setRow, tabCol[kakaiCnt]);
							workCell.setCellValue("*");
						}
						ninzu=0;
						setRow=-1;
					}
				}
				kakaiCnt++;
			}

			// Excelファイル保存
			boolean bolRet = false;
			if(intBookCngCount == 1){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
			}else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, maxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("S52_02","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S52_02","S52_02帳票作成終了","");
		return noerror;
	}

}
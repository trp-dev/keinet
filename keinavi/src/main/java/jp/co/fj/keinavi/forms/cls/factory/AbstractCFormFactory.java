/*
 * 作成日: 2004/07/16
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.cls.factory;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.com_set.ComSubjectData;
import jp.co.fj.keinavi.data.com_set.SubjectData;
import jp.co.fj.keinavi.forms.AbstractActionFormFactory;
import jp.co.fj.keinavi.forms.cls.C001Form;
import jp.co.fj.keinavi.forms.cls.CMaxForm;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * クラス成績分析のアクションフォーム生成時に
 * 共通する処理を記述する。
 *
 * 2005.02.15 Yoshimoto KAWAI - Totec
 *            前画面の対象模試を引き継ぐように変更
 *
 * 2005.04.05	Yoshimoto KAWAI - Totec
 *  			過年度の表示対応
 *
 * @author kawai
 *
 * */
public abstract class AbstractCFormFactory extends AbstractActionFormFactory {

	/**
	 * CMaxFormを生成する
	 * @param request
	 * @return
	 */
	protected CMaxForm createCMaxForm(HttpServletRequest request) {
		CMaxForm form = new CMaxForm();

		// 模試セッション
		ExamSession examSession = (ExamSession) request.getAttribute(ExamSession.SESSION_KEY);

		// 対象模試
		ExamData exam =
			examSession.getExamData(
				request.getParameter("targetYear"),
				request.getParameter("targetExam")
			);

		// 対象模試がなければ最新にする
		if (exam == null) {
			exam = KNUtil.getLatestExamData(examSession);
		}

		// さらになければ空オブジェクト
		if (exam == null) {
			exam = new ExamData();
		}

		// 対象年度・対象模試
		form.setTargetYear(exam.getExamYear());
		form.setTargetExam(exam.getExamCD());
		// スクロール座標を初期化する
		form.setScrollX("0");
		form.setScrollY("0");

		return form;
	}

	/**
	 * C001Formを生成する
	 * @param request
	 * @return
	 */
	protected C001Form createC001Form(HttpServletRequest request) {
//		C001Form form = new C001Form();
//		// 対象年度をセット
//		form.setTargetYear(super.getTargetYear(request));
//		// 対象模試をセット
//		form.setTargetExam(super.getTargetExam(request));
//
//		return form;

		return (C001Form) request.getAttribute("form");
	}

	//0102,0103 型・共通項目設定利用
	protected void setCommonTypeAndAnalyzeType( Map map, CMaxForm form){
		map.put(IProfileItem.TYPE_USAGE, Short.valueOf(form.getCommonType()));

		// 個別設定
		if ("0".equals(form.getCommonType())) {
			ComSubjectData data = (ComSubjectData) map.get(IProfileItem.TYPE_IND);

			// なければ作る
			if (data == null) {
				data = new ComSubjectData();
				data.setISubjectData(new ArrayList());
				map.put(IProfileItem.TYPE_IND, data);

			// あるならクリア
			} else {
				data.getISubjectData().clear();
			}

			if (form.getAnalyzeType() != null) {
				Set graph = CollectionUtil.array2Set(form.getGraphType());

				for (int i=0; i<form.getAnalyzeType().length; i++) {
					SubjectData s = new SubjectData();

					s.setSubjectCD(form.getAnalyzeType()[i]);

					if (graph.contains(form.getAnalyzeType()[i])) s.setGraphDisp((short)1);
					else s.setGraphDisp((short)0);

					data.getISubjectData().add(s);
				}
			}
		}
	}

	//0102,0103 型・共通項目設定利用
	public void getCommonTypeAndAnalyzeType( Map map, CMaxForm form){
		form.setCommonType(map.get(IProfileItem.TYPE_USAGE).toString());

		// 個別設定
		ComSubjectData data = (ComSubjectData) map.get(IProfileItem.TYPE_IND);

		if (data != null) {
			form.setAnalyzeType(ProfileUtil.getSubjectIndValue(data, false));
			form.setGraphType(ProfileUtil.getSubjectIndValue(data, true));
		}
	}

	//0202,0203 科目・共通項目設定利用
	protected void setCommonCourseAndAnalyzeCourse( Map map, CMaxForm form){
		map.put(IProfileItem.COURSE_USAGE, Short.valueOf(form.getCommonCourse()));

		// 個別設定
		if ("0".equals(form.getCommonCourse())) {
			ComSubjectData data = (ComSubjectData) map.get(IProfileItem.COURSE_IND);

			// なければ作る
			if (data == null) {
				data = new ComSubjectData();
				data.setISubjectData(new ArrayList());
				map.put(IProfileItem.COURSE_IND, data);

			// あるならクリア
			} else {
				data.getISubjectData().clear();
			}

			if (form.getAnalyzeCourse() != null) {
				Set graph = CollectionUtil.array2Set(form.getGraphCourse());

				for (int i=0; i<form.getAnalyzeCourse().length; i++) {
					SubjectData s = new SubjectData();

					s.setSubjectCD(form.getAnalyzeCourse()[i]);
					if (graph.contains(form.getAnalyzeCourse()[i])) s.setGraphDisp((short)1);
					else s.setGraphDisp((short)0);

					data.getISubjectData().add(s);
				}
			}
		}
	}

	//0202,0203 科目・共通項目設定利用
	public void getCommonCourseAndAnalyzeCourse( Map map, CMaxForm form){
		form.setCommonCourse(map.get(IProfileItem.COURSE_USAGE).toString());

		// 個別設定
		ComSubjectData data = (ComSubjectData) map.get(IProfileItem.COURSE_IND);

		if (data != null) {
			form.setAnalyzeCourse(ProfileUtil.getSubjectIndValue(data, false));
			form.setGraphCourse(ProfileUtil.getSubjectIndValue(data, true));
		}
	}

	//0302  志望大学選択方法
	protected void setChoiceUnivMode( Map map, CMaxForm form ){
		Set anaItem = CollectionUtil.array2Set(form.getAnalyzeItem());
		if( anaItem.contains("univ") ){
			map.put("0302", new Short( (short)(Short.valueOf(form.getChoiceUnivMode()).shortValue() + 10 )));
		} else {
			if( form.getChoiceUnivMode() != null ){
				map.put( "0302", Short.valueOf(form.getChoiceUnivMode()) );
			} else {
				if( map.get("0302") != null ){
					map.put( "0302", new Short( (short)(Short.valueOf(map.get("0302").toString()).shortValue() % 10)));
				}
			}
		}
	}

	//0302  志望大学選択方法取得
	protected void getChoiceUnivMode( Map map, CMaxForm form ){
		if( map.get("0302") != null ){
			if( ((Short)map.get("0302")).shortValue() >= 10 ){
				Set anaItem = CollectionUtil.array2Set(form.getAnalyzeItem());
				anaItem.add("univ");
				form.setAnalyzeItem( (String[])anaItem.toArray(new String[0]) );
				form.setChoiceUnivMode(String.valueOf( ((Short)map.get("0302")).shortValue() - 10) );
			} else {
				form.setChoiceUnivMode( map.get("0302").toString() );
			}
		}
	}

	//0303  志望者数
	protected void setCandRange( Map map, CMaxForm form ){
		if( form.getCandRange() != null){
			map.put("0303", Short.valueOf(form.getCandRange()));
		}
	}

	//0303  志望者数取得
	protected void getCandRange( Map map, CMaxForm form ){
		if( map.get("0303") != null ){
			form.setCandRange(map.get("0303").toString());
		}
	}

	//0304  評価基準
	protected void setEvaluation( Map map, CMaxForm form ){
//		Set evaItem = CollectionUtil.array2Set(form.getEvaluation());
//		String[] ev = new String[5];
//		if( evaItem.contains("a") ) ev[0] = "1";
//				else ev[0] = "0";
//		if( evaItem.contains("b") ) ev[1] = "1";
//				else ev[1] = "0";
//		if( evaItem.contains("c") ) ev[2] = "1";
//				else ev[2] = "0";
//		if( evaItem.contains("d") ) ev[3] = "1";
//				else ev[3] = "0";
//		if( evaItem.contains("e") ) ev[4] = "1";
//				else ev[4] = "0";
//		String evaStr = ev[0]+","+ev[1]+","+ev[2]+","+ev[3]+","+ev[4];
//		map.put("0304", evaStr );

		map.put("0304", form.getEvaluation());
	}

	//0304  評価基準取得
	protected void getEvaluation( Map map, CMaxForm form ){
//		String[] items = {"a","b","c","d","e"};
//		String evaStr = (String)map.get("0304");
//		Set evaItem = new HashSet();
//		if( (evaStr != null) && (!evaStr.equals("")) ){
//			int i=0;
//			for( StringTokenizer st = new StringTokenizer( evaStr, ","); st.hasMoreTokens(); ){
//				if( "1".equals(st.nextToken()) ){
//					evaItem.add(items[i]);
//				}
//				i++;
//			}
//		}
//		form.setEvaluation( (String[])evaItem.toArray(new String[0]) );

		form.setEvaluation((String[])map.get("0304"));
	}


	//0305  志願者の表示順序
	protected void setCandidateOrder( Map map, CMaxForm form ){
		map.put("0305", Short.valueOf(form.getCandidateOrder()) );
	}

	//0305  志願者の表示順序取得
	protected void getCandidateOrder( Map map, CMaxForm form ){
		if( map.get("0305") != null){
			form.setCandidateOrder(map.get("0305").toString());
		}
	}

	//0306 志望大学表示
	protected void setDispChoiceUniv( Map map, CMaxForm form){
		map.put("0306", Short.valueOf(form.getDispChoiceUniv()));
	}

	//0306 志望大学表示取得
	protected void getDispChoiceUniv( Map map, CMaxForm form){
		if( map.get("0306") != null){
			form.setDispChoiceUniv(map.get("0306").toString());
		}
	}

	//0308  国私区分
	protected void setPrintUnivDiv( Map map, CMaxForm form){
		map.put("0308", Short.valueOf(form.getPrintUnivDiv()));
	}

	//0308  国私区分取得
	protected void getPrintUnivDiv( Map map, CMaxForm form){
		if( map.get("0308") != null){
			form.setPrintUnivDiv(map.get("0308").toString());
		}
	}

	//0309  大学集計区分
	protected void setTotalUnivDiv( Map map, CMaxForm form){
		map.put("0309", Short.valueOf(form.getTotalUnivDiv()));
	}

	//0309  大学集計区分取得
	protected void getTotalUnivDiv( Map map, CMaxForm form){
		if( map.get("0309") != null){
			form.setTotalUnivDiv(map.get("0309").toString());
		}
	}

	//0310  大学の表示順序
	protected void setUnivOrder( Map map, CMaxForm form){
		map.put("0310", Short.valueOf(form.getUnivOrder()));
	}

	//0310  大学の表示順序取得
	protected void getUnivOrder( Map map, CMaxForm form){
		if( map.get("0310") != null){
			form.setUnivOrder(map.get("0310").toString());
		}
	}

	//0311  表示対象（志望大学）
	protected void setUnivStudent( Map map, CMaxForm form){
		map.put("0311",form.getDispUnivStudent());
	}
	protected void getUnivStudent( Map map, CMaxForm form){
		if (map.containsKey("0311")) {
			form.setDispUnivStudent((String[]) map.get("0311"));
		} else {
			form.setDispUnivStudent(new String[]{"1"});
		}
	}

	//0312  日程
	protected void setUnivSchedule(Map map, CMaxForm form) {
		if (form.getDispUnivSchedule() == null) {
			map.put("0312", null);
		} else {
			map.put("0312", new Short(form.getDispUnivSchedule()));
		}
	}
	protected void getUnivSchedule(Map map, CMaxForm form) {
		if (map.get("0312") == null) {
			form.setDispUnivSchedule(null);
		} else {
			form.setDispUnivSchedule("1");
		}
	}

	//0313  評価区分
	protected void setUnivRating(Map map, CMaxForm form) {
		map.put("0313", form.getDispUnivRating());
	}
	protected void getUnivRating(Map map, CMaxForm form) {
		form.setDispUnivRating((String[]) map.get("0313"));
	}

	//0502 クラスの表示順序取得
	protected void getClassOrder( Map map, CMaxForm form){
		if( map.get("0502") != null){
			form.setClassOrder(map.get("0502").toString());
		}
	}

	//0502 クラスの表示順序
	protected void setClassOrder( Map map, CMaxForm form){
		map.put("0502", Short.valueOf(form.getClassOrder()));
	}


	//0601 表の設定
	protected void setChart( Map map, CMaxForm form){
		Set formItem = CollectionUtil.array2Set(form.getFormatItem());
		if( formItem.contains("Chart")){
			map.put("0601", Short.valueOf("1"));
		} else {
			map.put("0601", Short.valueOf("0"));
		}
	}

	//0601 表の設定取得
	protected void getChart( Map map, CMaxForm form){
		if( map.get("0601") != null){
			if( ((Short)map.get("0601")).shortValue() == 1){
				Set formItem = CollectionUtil.array2Set(form.getFormatItem());
				formItem.add("Chart");
				form.setFormatItem( (String[])formItem.toArray(new String[0]) );
			}
		}
	}

	//0602 表の設定
	protected void setChartWithDispRatio( Map map, CMaxForm form){
		Set formItem = CollectionUtil.array2Set(form.getFormatItem());
		if( formItem.contains("Chart")){
			map.put("0602", new Short( (short)(Short.valueOf(form.getDispRatio()).shortValue() + 10) ) );
		} else {
			if( form.getDispRatio() != null ){
				map.put("0602", Short.valueOf(form.getDispRatio()));
			} else {
				if( map.get("0602") != null ){
					map.put( "0602", new Short( (short)(Short.valueOf(map.get("0602").toString()).shortValue() % 10)));
				}
			}
		}
	}

	//0602 表の設定取得
	protected void getChartWithDispRatio( Map map, CMaxForm form){
		if( map.get("0602") != null){
			if( ((Short)map.get("0602")).shortValue() > 10){
				Set formItem = CollectionUtil.array2Set(form.getFormatItem());
				formItem.add("Chart");
				form.setFormatItem( (String[])formItem.toArray(new String[0]) );
				form.setDispRatio(String.valueOf(((Short)map.get("0602")).shortValue() -10));
			} else {
				form.setDispRatio( map.get("0602").toString() );
			}
		}
	}

	//0701 グラフの設定
	protected void setGraph( Map map, CMaxForm form){
		Set formItem = CollectionUtil.array2Set(form.getFormatItem());
		if( formItem.contains("Graph")){
			map.put("0701", Short.valueOf("1"));
		} else {
			map.put("0701", Short.valueOf("0"));
		}
	}

	//0701 グラフの設定取得
	protected void getGraph( Map map, CMaxForm form){
		if( map.get("0701") != null){
			if( ((Short)map.get("0701")).shortValue() == 1){
				Set formItem = CollectionUtil.array2Set(form.getFormatItem());
				formItem.add("Graph");
				form.setFormatItem( (String[])formItem.toArray(new String[0]) );
			}
		}
	}

	//0702 偏差値帯別人数積み上げグラフ
	protected void setGraphCompRatio( Map map, CMaxForm form){
		Set optionItem = CollectionUtil.array2Set(form.getOptionItem());
		if( optionItem.contains("GraphCompRatio") ){
			map.put("0702", new Short((short)(Short.valueOf(form.getPitchCompRatio()).shortValue()+10)));
		} else {
			if( form.getPitchCompRatio() != null){
				map.put("0702", Short.valueOf(form.getPitchCompRatio()) );
			} else {
				if( map.get("0702") != null ){
					map.put( "0702", new Short( (short)(Short.valueOf(map.get("0702").toString()).shortValue() % 10)));
				}
			}
		}
	}

	//0702 偏差値帯別人数積み上げグラフ取得
	protected void getGraphCompRatio( Map map, CMaxForm form){
		if( map.get("0702") != null){
			if( (((Short)map.get("0702")).shortValue() - 10) > 0){
				Set optionItem = CollectionUtil.array2Set(form.getOptionItem());
				optionItem.add("GraphCompRatio");
				form.setOptionItem( (String[])optionItem.toArray(new String[0]));
				form.setPitchCompRatio(String.valueOf(((Short)map.get("0702")).shortValue() -10));
			} else {
				form.setPitchCompRatio(map.get("0702").toString());
			}
		}
	}

	//0703 偏差値帯別度数分布グラフ
	protected void setGraphDistWithAxis( Map map, CMaxForm form){
		Set formItem = CollectionUtil.array2Set(form.getFormatItem());
		if( formItem.contains("GraphDist")){
			map.put("0703", new Short( (short)(Short.valueOf(form.getAxis()).shortValue() +10) ));
		} else {
			if( form.getAxis() != null ){
				map.put("0703", Short.valueOf(form.getAxis()) );
			} else {
				if( map.get("0703") != null ){
					map.put( "0703", new Short( (short)(Short.valueOf(map.get("0703").toString()).shortValue() % 10)));
				}
			}
		}
	}

	//0703 偏差値帯別度数分布グラフ取得
	protected void getGraphDistWithAxis( Map map, CMaxForm form){
		if( map.get("0703") != null){
			if( ((Short)map.get("0703")).shortValue() > 10 ){
				Set formItem = CollectionUtil.array2Set(form.getFormatItem());
				formItem.add("GraphDist");
				form.setFormatItem( (String[])formItem.toArray(new String[0]) );
				form.setAxis(String.valueOf((((Short)map.get("0703")).shortValue() - 10 )));
			} else {
				form.setAxis(map.get("0703").toString());
			}
		}
	}

	//0704 偏差値帯別度数分布グラフ
	protected void setGraphDist( Map map, CMaxForm form){
		Set formItem = CollectionUtil.array2Set(form.getFormatItem());
		if( formItem.contains("GraphDist")){
			map.put("0704", Short.valueOf("1"));
		} else {
			map.put("0704", Short.valueOf("0"));
		}
	}

	//0704 偏差値帯別度数分布グラフ取得
	protected void getGraphDist( Map map, CMaxForm form){
		if( map.get("0704") != null){
			if( ((Short)map.get("0704")).shortValue() == 1){
				Set formItem = CollectionUtil.array2Set(form.getFormatItem());
				formItem.add("GraphDist");
				form.setFormatItem( (String[])formItem.toArray(new String[0]) );
			}
		}
	}

	//0705 偏差値帯別人数積み上げグラフ
	protected void setGraphBuildup( Map map, CMaxForm form){
		Set optionItem = CollectionUtil.array2Set(form.getOptionItem());
		if( optionItem.contains("GraphBuildup") ){
			map.put("0705", new Short( (short)(Short.valueOf(form.getDivPitch()).shortValue() + 10) ) );
		} else {
			if( form.getDivPitch() != null ){
				map.put("0705", Short.valueOf(form.getDivPitch()) );
			} else {
				if( map.get("0705") != null ){
					map.put( "0705", new Short( (short)(Short.valueOf(map.get("0705").toString()).shortValue() % 10)));
				}
			}
		}
	}

	//0705 偏差値帯別人数積み上げグラフ取得
	protected void getGraphBuildup( Map map, CMaxForm form){
		if( map.get("0705") != null){
			if( ((Short)map.get("0705")).shortValue() > 10){
				Set optionItem = CollectionUtil.array2Set(form.getOptionItem());
				optionItem.add("GraphBuildup");
				form.setOptionItem( (String[])optionItem.toArray(new String[0]) );
				form.setDivPitch(String.valueOf((((Short)map.get("0705")).shortValue() - 10 )));
			} else {
				form.setDivPitch(map.get("0705").toString());
			}
		}
	}

	//0802 生徒の表示順序取得
	protected void getStrudentOrder( Map map, CMaxForm form){
		if( map.get("0802") != null){
			form.setStrudentOrder(map.get("0802").toString());
		}
	}

	//0802 生徒の表示順序
	protected void setStrudentOrder( Map map, CMaxForm form){
		map.put("0802", Short.valueOf(form.getStrudentOrder()));
	}

	//1001 偏差値取得
	protected void getDeviation( Map map, CMaxForm form){
		if( map.get("1001") != null){
			if( ((Short)map.get("1001")).shortValue() >= 10){
				Set anaItem = CollectionUtil.array2Set(form.getAnalyzeItem());
				anaItem.add("deviation");
				form.setAnalyzeItem( (String[])anaItem.toArray(new String[0]));
			}
			if( (((Short)map.get("1001")).shortValue() % 10) == 0){
				form.setDeviation("");
			} else {
				form.setDeviation(String.valueOf(((Short)map.get("1001")).shortValue() % 10));
			}
		}
	}

	//1001 偏差値
	protected void setDeviation( Map map, CMaxForm form){
		Set anaItem = CollectionUtil.array2Set(form.getAnalyzeItem());
		if( anaItem.contains("deviation")){
			map.put("1001", new Short((short)(Short.valueOf(form.getDeviation()).shortValue() + 10)));
		} else {
			if( form.getDeviation() != null ){
				map.put("1001", Short.valueOf(form.getDeviation()) );
			} else {
				if( map.get("1001") != null ){
					map.put( "1001", new Short( (short)(Short.valueOf(map.get("1001").toString()).shortValue() % 10)));
				}
			}
		}
	}

	//1303 印刷対象クラス
	protected void getPrintClass( Map map, CMaxForm form){
		form.setPrintClass((String)map.get("1303"));
	}

	//1303 印刷対象クラス取得
	protected void setPrintClass( Map map, CMaxForm form){
		if ("".equals(form.getPrintClass())){
			map.put("1303", null);
		} else {
			map.put("1303", form.getPrintClass());
		}
	}

	//1305 セキュリティスタンプ取得
	protected void getStamp( Map map, CMaxForm form){
		if( map.get("1305") != null){
			form.setStamp(map.get("1305").toString());
		}
	}

	//1305 セキュリティスタンプ
	protected void setStamp( Map map, CMaxForm form){
		map.put("1305", Short.valueOf(form.getStamp()));
	}


        //1501 偏差値帯別人数積み上げグラフ取得
        protected void getRankOrder( Map map, CMaxForm form){
                if( map.get("1501") != null){
                        if( ((Short)map.get("1501")).shortValue() > 10){
                                Set optionItem = CollectionUtil.array2Set(form.getOptionItem());
                                optionItem.add("RankOrder");
                                form.setOptionItem( (String[])optionItem.toArray(new String[0]) );
                                form.setOrderPitch(String.valueOf((((Short)map.get("1501")).shortValue() - 10 )));
                        } else {
                                form.setOrderPitch(map.get("1501").toString());
                        }
                }
        }

        //1501 偏差値帯別人数積み上げグラフ
        protected void setRankOrder( Map map, CMaxForm form){
                Set optionItem = CollectionUtil.array2Set(form.getOptionItem());
                if( optionItem.contains("RankOrder") ){
                        map.put("1501", new Short( (short)(Short.valueOf(form.getOrderPitch()).shortValue() + 10) ) );
                } else {
                        if( form.getOrderPitch() != null ){
                                map.put("1501", Short.valueOf(form.getOrderPitch()) );
                        } else {
                                if( map.get("1501") != null ){
                                        map.put( "1501", new Short( (short)(Short.valueOf(map.get("1501").toString()).shortValue() % 10)));
                                }
                        }
                }
        }

	//1502 型・科目別比較取得
	protected void getCompPrev( Map map, CMaxForm form){
		if( map.get("1502") != null){
			if( (((Short)map.get("1502")).shortValue()) > 10){
				Set optionItem = CollectionUtil.array2Set(form.getOptionItem());
				optionItem.add("CompPrev");
				form.setOptionItem( (String[])optionItem.toArray(new String[0]) );
				form.setCompCount(String.valueOf(((Short)map.get("1502")).shortValue() - 10));
			} else {
				form.setCompCount(map.get("1502").toString());
			}
		}
	}

	//1502 型・科目別比較
	protected void setCompPrev( Map map, CMaxForm form){
		Set optionItem = CollectionUtil.array2Set(form.getOptionItem());
		if( optionItem.contains("CompPrev") ){
			map.put("1502", new Short((short)(Short.valueOf(form.getCompCount()).shortValue() + 10)));
		} else {
			if( form.getCompCount() != null ){
				map.put("1502", Short.valueOf(form.getCompCount().toString()));
			} else {
				if( map.get("1502") != null ){
					map.put( "1502", new Short( (short)(Short.valueOf(map.get("1502").toString()).shortValue() % 10)));
				}
			}
		}
	}

	//1503 変動幅
	protected void setFluctRange( Map map, CMaxForm form){
		map.put("1503", Short.valueOf(form.getFluctRange()));
	}

	//1503 変動幅取得
	protected void getFluctRange( Map map, CMaxForm form){
		if( map.get("1503") != null){
			form.setFluctRange(map.get("1503").toString());
		}
	}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//	// 2019/09/06 QQ)Tanioka 共通テスト英語認定試験CEFR取得状況 ADD START
//	//1602 共通テスト英語認定試験CEFR取得状況
//	protected void setOptionCheckBox( Map map, CMaxForm form){
//		Set optionItem = CollectionUtil.array2Set(form.getOptionItem());
//		Short value = null;
//
//		// あり
//		if (optionItem.contains("OptionCheckBox")) {
//			if ("11".equals(form.getTargetCheckBox())) value = new Short("11");
//			else if (null == form.getTargetCheckBox() || "12".equals(form.getTargetCheckBox())) value = new Short("12");
//			else throw new IllegalArgumentException("共通テスト英語認定試験CEFR取得状況の値が不正です。");
//		// なし
//		} else {
//			if ("11".equals(form.getTargetCheckBox())) value = new Short("1");
//			else if (null == form.getTargetCheckBox() || "12".equals(form.getTargetCheckBox())) value = new Short("2");
//			else throw new IllegalArgumentException("共通テスト英語認定試験CEFR取得状況の値が不正です。");
//		}
//
//		map.put(IProfileItem.OPTION_CHECK_BOX, value);
//	}
//
//	//1602 共通テスト英語認定試験CEFR取得状況取得
//	protected void getOptionCheckBox( Map map, CMaxForm form){
//		if (map.get("1602") != null) {
//			short value = ((Short) map.get("1602")).shortValue();
//			Set optionItem = CollectionUtil.array2Set(form.getOptionItem());
//			switch (value) {
//				case 1 :
//				case 2 :
//					break;
//				case 11 :
//					optionItem.add("OptionCheckBox");
//					break;
//				case 12 :
//					optionItem.add("OptionCheckBox");
//					break;
//			}
//// 2019/10/15 QQ)Tanioka CEFRのチェックが外れる障害の修正 ADD START
//                        form.setOptionItem( (String[])optionItem.toArray(new String[0]));
//// 2019/10/15 QQ)Tanioka CEFRのチェックが外れる障害の修正 ADD END
//			form.setTargetCheckBox(Short.toString(value));
//			// 初期値
//		} else {
//			map.put(IProfileItem.OPTION_CHECK_BOX, new Short("1"));
//			form.setTargetCheckBox("1");
//		}
//	}
//	// 2019/09/06 QQ)Tanioka 共通テスト英語認定試験CEFR取得状況 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

	// 1102 過年度の表示
	protected void setDispUnivYear(Map map, CMaxForm form) {
		map.put("1102", form.getDispUnivYear());
	}

	// 1102 過年度の表示
	protected void getDispUnivYear(Map map, CMaxForm form) {
		form.setDispUnivYear((String[]) map.get("1102"));
	}
}

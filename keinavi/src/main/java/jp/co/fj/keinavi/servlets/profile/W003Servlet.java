package jp.co.fj.keinavi.servlets.profile;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.ExamBean;
import jp.co.fj.keinavi.beans.profile.CountingDivBean;
import jp.co.fj.keinavi.beans.profile.ProfileBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.CountingDivData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.profile.W003Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.log.KNLog;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class W003Servlet extends DefaultHttpServlet {

	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// ログイン情報
		LoginSession login = super.getLoginSession(request);

		// アクションフォーム
		W003Form form = (W003Form)super.getActionForm(request,
			"jp.co.fj.keinavi.forms.profile.W003Form");

		if ("w003".equals(super.getForward(request))) {
			// DBコネクション
			Connection con = null;
			try {
				con = super.getConnectionPool(request);

				// 集計区分Beanを作る
				CountingDivBean c = null;
				if (login.isCountingDiv()) {
					c = new CountingDivBean();
					c.setSchoolCD(login.getUserID());
					c.setConnection(null, con);
					c.execute();
					request.setAttribute("CountingDivBean", c);
				}

				// プロファイル選択画面からの遷移ならセッションを初期化
				if ("w002".equals(getBackward(request))) {
					// プロファイルセッションを作る
					{
						ProfileBean bean = new ProfileBean();
						bean.setLoginSession(login);
						bean.setBundleCD(form.getCountingDivCode());
						bean.execute();

						Profile profile = bean.getProfile();
						profile.setDbKey(login.getDbKey());

						// 集計区分名称を入れておく
						if (c != null) {
							int index = c.getCountingDivList().indexOf(
								new CountingDivData(form.getCountingDivCode()));
							profile.setBundleName((
								(CountingDivData)c.getCountingDivList().get(index)
									).getCountingDivName());
						}

						// セッションに入れる
						request.getSession().setAttribute(Profile.SESSION_KEY, profile);

						// 営業部はデフォルトでチェックが入る
						if (login.isSales()) {
							form.setFunction(new String[]{"school"});
						}
					}

					// 模試セッションを作る
					{
						ExamBean bean = new ExamBean();
						bean.setConnection(null, con);
						bean.setLoginSession(login);
						bean.setBundleCD(form.getCountingDivCode());
						bean.execute();
						request
							.getSession(false)
							.setAttribute(ExamSession.SESSION_KEY, bean.getExamSession());

						// 模試の初期値
						if (form.getTargetYear() == null) {
							// 最新の模試
							ExamData exam =
								KNUtil.getLatestExamData(super.getExamSession(request));
							form.setTargetYear(exam.getExamYear());
							form.setTargetExam(exam.getExamCD());
						}
					}
				}

				request.setAttribute("form", form);

				// 転送元がプロファイル選択画面ならログ出力
				if ("w002".equals(super.getBackward(request))) {
					// アクセスログ
/*
					KNLog.Lv2(
						request.getRemoteAddr(),
						login.getUserID(),
						login.getSectorSortingCD(),
						login.getAccount(),
						super.getBackward(request),
						202,
						null
					);
*/
				}

				super.forward(request, response, JSP_W003);

			// 既知のエラーなら前画面へ
			} catch (KNServletException e) {
				super.setErrorMessage(request, e);
				super.forward(request, response, "/ProfileSelect");
				
			} catch (Exception e) {
				throw new ServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

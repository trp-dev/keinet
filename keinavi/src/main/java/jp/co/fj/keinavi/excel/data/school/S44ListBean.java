package jp.co.fj.keinavi.excel.data.school;

import java.util.*;

/**
 * �쐬��: 2004/07/20
 * @author A.Iwata
 *
 * �Z�����ѕ��́|���Z��r�|�u�]��w�]���ʐl�� �f�[�^���X�g
 */
public class S44ListBean {
	//���������敪
	private String strGenKouKbn = "";
	//��w�R�[�h
	private String strDaigakuCd = "";
	//��w��
	private String strDaigakuMei = "";
	//�w���R�[�h
	private String strGakubuCd = "";
	//�w����
	private String strGakubuMei = "";
	//�w�ȃR�[�h
	private String strGakkaCd = "";
	//�w�Ȗ�
	private String strGakkaMei = "";
	//�����R�[�h
	private String strNtiCd = "";
	//����
	private String strNittei = "";
	//�]���敪
	private String strHyouka = "";
	//�w�Z�ʕ]���l���f�[�^���X�g
	private ArrayList s44GakkoList = new ArrayList();

	/*
	 * GET
	 */
	public String getStrGenKouKbn() {
		return this.strGenKouKbn;
	}
	public String getStrDaigakuCd() {
		return this.strDaigakuCd;
	}
	public String getStrDaigakuMei() {
		return this.strDaigakuMei;
	}
	public String getStrGakubuCd() {
		return this.strGakubuCd;
	}
	public String getStrGakubuMei() {
		return this.strGakubuMei;
	}
	public String getStrGakkaCd() {
		return this.strGakkaCd;
	}
	public String getStrGakkaMei() {
		return this.strGakkaMei;
	}
	public String getStrNtiCd() {
		return this.strNtiCd;
	}
	public String getStrNittei() {
		return this.strNittei;
	}
	public String getStrHyouka() {
		return this.strHyouka;
	}
	public ArrayList getS44GakkoList() {
		return this.s44GakkoList;
	}
	/*
	 * SET
	 */
	public void setStrGenKouKbn(String strGenKouKbn) {
		this.strGenKouKbn = strGenKouKbn;
	}
	public void setStrDaigakuCd(String strDaigakuCd) {
		this.strDaigakuCd = strDaigakuCd;
	}
	public void setStrDaigakuMei(String strDaigakuMei) {
		this.strDaigakuMei = strDaigakuMei;
	}
	public void setStrGakubuCd(String strGakubuCd) {
		this.strGakubuCd = strGakubuCd;
	}
	public void setStrGakubuMei(String strGakubuMei) {
		this.strGakubuMei = strGakubuMei;
	}
	public void setStrGakkaCd(String strGakkaCd) {
		this.strGakkaCd = strGakkaCd;
	}
	public void setStrGakkaMei(String strGakkaMei) {
		this.strGakkaMei = strGakkaMei;
	}
	public void setStrNtiCd(String strNtiCd) {
		this.strNtiCd = strNtiCd;
	}
	public void setStrNittei(String strNittei) {
		this.strNittei = strNittei;
	}
	public void setStrHyouka(String strHyouka) {
		this.strHyouka = strHyouka;
	}
	public void setS44GakkoList(ArrayList s44GakkoList) {
		this.s44GakkoList = s44GakkoList;
	}

}

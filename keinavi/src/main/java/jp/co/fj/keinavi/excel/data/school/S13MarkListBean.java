package jp.co.fj.keinavi.excel.data.school;

/**
 * 設問別成績（校内成績）マーク模試正答状況データリスト
 * 作成日: 2004/07/06
 * @author	T.Kuzuno
 * 
 *  * <2010年度改修>
 * 2010.01.21	Fujito URAKAWA - Totec
 *              設問名称追加表示／大学コード入力機能廃止対応
 *               
 */

public class S13MarkListBean {
	//型・科目名コード
	private String strKmkCd = "";
	//型・科目名
	private String strKmkmei = "";
	//配点（型・科目）
	private String strHaitenKmk = "";
	//受験人数
	private int intNinzu = 0;
	//解答番号
	private String strKaitoNo = "";
	//完答レコードフラグ
	private int intKantoRcFlg = 0;
	//完答問題フラグ
	private int intKantoFlg = 0;
	//配点（解答番号）
	private String strHaiten = "";
	//正答
	private String strSeito = "";
	//校内正答率
	private float floSeitoritsuHome = 0;
	//全国正答率
	private float floSeitoritsuAll = 0;
	//誤答パターン解答1
	private String strGoto1 = "";
	//誤答パターン正答率1
	private float floGoto1Markritsu = 0;
	//誤答パターン解答2
	private String strGoto2 = "";
	//誤答パターン正答率2
	private float floGoto2Markritsu = 0;
	//誤答パターン解答3
	private String strGoto3 = "";
	//誤答パターン正答率3
	private float floGoto3Markritsu = 0;
	// 大問・内容
	private String strDaimonNaiyo = "";
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public String getStrKaitoNo() {
		return this.strKaitoNo;
	}
	public int getIntKantoRcFlg() {
		return this.intKantoRcFlg;
	}
	public int getIntKantoFlg() {
		return this.intKantoFlg;
	}
	public String getStrHaiten() {
		return this.strHaiten;
	}
	public String getStrSeito() {
		return this.strSeito;
	}
	public float getFloSeitoritsuHome() {
		return this.floSeitoritsuHome;
	}
	public float getFloSeitoritsuAll() {
		return this.floSeitoritsuAll;
	}
	public String getStrGoto1() {
		return this.strGoto1;
	}
	public float getFloGoto1Markritsu() {
		return this.floGoto1Markritsu;
	}
	public String getStrGoto2() {
		return this.strGoto2;
	}
	public float getFloGoto2Markritsu() {
		return this.floGoto2Markritsu;
	}
	public String getStrGoto3() {
		return this.strGoto3;
	}
	public float getFloGoto3Markritsu() {
		return this.floGoto3Markritsu;
	}
	public String getStrDaimonNaiyo() {
		return strDaimonNaiyo;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setStrKaitoNo(String strKaitoNo) {
		this.strKaitoNo = strKaitoNo;
	}
	public void setIntKantoRcFlg(int intKantoRcFlg) {
		this.intKantoRcFlg = intKantoRcFlg;
	}
	public void setIntKantoFlg(int intKantoFlg) {
		this.intKantoFlg = intKantoFlg;
	}
	public void setStrHaiten(String strHaiten) {
		this.strHaiten = strHaiten;
	}
	public void setStrSeito(String strSeito) {
		this.strSeito = strSeito;
	}
	public void setFloSeitoritsuHome(float floSeitoritsuHome) {
		this.floSeitoritsuHome = floSeitoritsuHome;
	}
	public void setFloSeitoritsuAll(float floSeitoritsuAll) {
		this.floSeitoritsuAll = floSeitoritsuAll;
	}
	public void setStrGoto1(String strGoto1) {
		this.strGoto1 = strGoto1;
	}
	public void setFloGoto1Markritsu(float floGoto1Markritsu) {
		this.floGoto1Markritsu = floGoto1Markritsu;
	}
	public void setStrGoto2(String strGoto2) {
		this.strGoto2 = strGoto2;
	}
	public void setFloGoto2Markritsu(float floGoto2Markritsu) {
		this.floGoto2Markritsu = floGoto2Markritsu;
	}
	public void setStrGoto3(String strGoto3) {
		this.strGoto3 = strGoto3;
	}
	public void setFloGoto3Markritsu(float floGoto3Markritsu) {
		this.floGoto3Markritsu = floGoto3Markritsu;
	}
	public void setStrDaimonNaiyo(String strDaimonNaiyo) {
		this.strDaimonNaiyo = strDaimonNaiyo;
	}
}

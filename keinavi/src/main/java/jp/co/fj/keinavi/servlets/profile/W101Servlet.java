package jp.co.fj.keinavi.servlets.profile;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.com_set.CompClassBean;
import jp.co.fj.keinavi.beans.com_set.SubjectBean;
import jp.co.fj.keinavi.beans.profile.CountingDivBean;
import jp.co.fj.keinavi.beans.profile.ProfileFolderBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.profile.W101Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class W101Servlet extends DefaultHttpServlet {

	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if ("w101".equals(getForward(request))) {
			// 模試セッション
			ExamSession examSession = super.getExamSession(request);
			// ログイン情報
			LoginSession login = super.getLoginSession(request);
			// プロファイル
			Profile profile = super.getProfile(request);

			// アクションフォームの取得
			W101Form form = (W101Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.profile.W101Form");

			// 転送元がw10*でなければ呼び出し元をセットする
			if (!super.getBackward(request).startsWith("w10")) {
				form.setOrigin(super.getBackward(request));
			}

			// 対象模試がなければトップページの値を使う
			if (form.getTargetYear() == null || "".equals(form.getTargetYear())) {
				form.setTargetYear(profile.getTargetYear());
				form.setTargetExam(profile.getTargetExam());
			}

			Connection con = null; // DBコネクション
			try {
				con = super.getConnectionPool(request);

				// プロファイルフォルダBean
				{
					ProfileFolderBean bean = new ProfileFolderBean();
					bean.setConnection(null, con);
					bean.setLoginSession(login);
					bean.setBundleCD(profile.getBundleCD());
					bean.execute();
					request.setAttribute("ProfileFolderBean", bean);
				}

				// 集計区分Bean
				{
					CountingDivBean bean = new CountingDivBean();
					bean.setConnection(null, con);
					bean.setSchoolCD(login.getUserID());
					bean.execute();
					request.setAttribute("CountingDivBean", bean);
				}

				// 型Bean
				// Taglibでの評価用
				{
					SubjectBean bean = new SubjectBean(login);
					bean.setConnection(null, con);
					bean.setMode("1");
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.execute();
					request.setAttribute("TypeBean", bean);
				}

				// 科目Bean
				// Taglibでの評価用
				{
					SubjectBean bean = new SubjectBean(login);
					bean.setConnection(null, con);
					bean.setMode("2");
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.execute();
					request.setAttribute("CourseBean", bean);
				}
				
				// 比較対象クラスBean
				// Taglibでの評価用
				{
					CompClassBean bean = new CompClassBean();
					bean.setConnection(null, con);
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.setSchoolCD(login.getUserID());
					bean.execute();
					request.setAttribute("CompClassBean", bean);
				}

			} catch (Exception e) {
				throw createServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}

			// 現在時刻をセット
			request.setAttribute("now", new Date());

			// 現在の模試を取得
			ExamData exam = examSession.getExamData(form.getTargetYear(), form.getTargetExam());
			request.setAttribute("Exam", exam);

			// プロファイルに模試種類コードを保持する
			saveExamType(profile, exam);
			
			// アクションフォームをセットする
			request.setAttribute("form", form);

			super.forward(request, response, JSP_W101);

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);			
		}
	}

}

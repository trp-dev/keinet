package jp.co.fj.keinavi.excel.data.school;

/**import java.util.ArrayList;
/**
 * �Z�����ѕ��́|���Z��r�|���ъT���@�w�Z�f�[�^���X�g
 * �쐬��: 2004/07/16
 * @author	A.Iwata
 */
public class S41GakkoListBean {
	//�w�Z��
	private String strGakkomei = "";
	//���Z�p�O���t�\���t���O
	private int intDispGakkoFlg = 0;
	//�l��
	private int intNinzu = 0;
	//���ϓ_
	private float floHeikin = 0;
	//���ϕ΍��l
	private float floHensa = 0;
	
	
	/*----------*/
	/* Get      */
	/*----------*/

	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public int getIntDispGakkoFlg() {
		return this.intDispGakkoFlg;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public float getFloHeikin() {
		return this.floHeikin;
	}
	public float getFloHensa() {
		return this.floHensa;
	}


	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setIntDispGakkoFlg(int intDispGakkoFlg) {
		this.intDispGakkoFlg = intDispGakkoFlg;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setFloHeikin(float floHeikin) {
		this.floHeikin = floHeikin;
	}
	public void setFloHensa(float floHensa) {
		this.floHensa = floHensa;
	}
}

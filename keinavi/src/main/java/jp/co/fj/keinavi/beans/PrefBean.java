package jp.co.fj.keinavi.beans;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.fjh.beans.DefaultBean;

/**
 *
 * §Bean
 * 
 * 2004.07.14	[VKì¬]
 * 
 * 2006.02.21	Yoshimoto KAWAI - TOTEC
 * 				COÎ
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class PrefBean extends DefaultBean {

	private Map blockMap; // næR[h ¨ ®§R[hzñ
	private Map prefMap; // §R[h ¨ §¼
	private String[] blockCodeList; // næR[hzñ

	// Singleton
	private static PrefBean bean;

	/**
	 * RXgN^ 
	 */
	private PrefBean() {
	}

	/**
	 * s¹{§BeanÌCX^Xðæ¾·é
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static PrefBean getInstance(Connection con) throws SQLException {
		// NULLÈçú»
		if (bean == null) PrefBean.init(con);

		return bean;		
	}

	/**
	 * ÏÌú»ðs¤
	 * @param con
	 * @throws SQLException
	 */
	private static synchronized void init(Connection con) throws SQLException {
		
		// dummy...
		if (con != null) {
			con.getClass();
		}
		
		if (bean != null) {
			return;
		}

		bean = new PrefBean();

		Map b = new HashMap();
		// kC¹næ
		b.put("01", "01");
		// knæ
		b.put("02", new String[]{"02", "03", "04", "05", "06", "07"});
		// Önæ
		b.put("03", new String[]{"08", "09", "10", "11", "12", "13", "14"});
		// bMznæ
		b.put("04", new String[]{"15", "19", "20"});
		// k¤næ
		b.put("05", new String[]{"16", "17", "18"});
		// Cnæ
		b.put("06", new String[]{"21", "22", "23", "24"});
		// ßEnæ
		b.put("07", new String[]{"25", "26", "27", "28", "29", "30"});
		// næ
		b.put("08", new String[]{"31", "32", "33", "34", "35"});
		// lnæ
		b.put("09", new String[]{"36", "37", "38", "39"});
		// ãBE«ênæ
		b.put("10", new String[]{"40", "41", "42", "43", "44", "45", "46", "47"});

		bean.setBlockMap(b);

		Map p = new HashMap();
		p.put("01", "kC¹");
		p.put("02", "ÂX");
		p.put("03", "âè");
		p.put("04", "{é");
		p.put("05", "Hc");
		p.put("06", "R`");
		p.put("07", "");
		p.put("08", "ïé");
		p.put("09", "ÈØ");
		p.put("10", "Qn");
		p.put("11", "éÊ");
		p.put("12", "çt");
		p.put("13", "");
		p.put("14", "_Þì");
		p.put("15", "V");
		p.put("19", "R");
		p.put("20", "·ì");
		p.put("16", "xR");
		p.put("17", "Îì");
		p.put("18", "ä");
		p.put("21", "ò");
		p.put("22", "Ãª");
		p.put("23", "¤m");
		p.put("24", "Od");
		p.put("25", " ê");
		p.put("26", "s");
		p.put("27", "åã");
		p.put("28", "ºÉ");
		p.put("29", "ÞÇ");
		p.put("30", "aÌR");
		p.put("31", "¹æ");
		p.put("32", "ª");
		p.put("33", "ªR");
		p.put("34", "L");
		p.put("35", "Rû");
		p.put("36", "¿");
		p.put("37", "ì");
		p.put("38", "¤Q");
		p.put("39", "m");
		p.put("40", "ª");
		p.put("41", "²ê");
		p.put("42", "·è");
		p.put("43", "F{");
		p.put("44", "åª");
		p.put("45", "{è");
		p.put("46", "­");
		p.put("47", "«ê");
		p.put("48", "CO");

		bean.setPrefMap(p);

		List container = new ArrayList();
		Iterator ite = b.keySet().iterator();
		while (ite.hasNext()) {
			String key = (String) ite.next();
			container.add(key);
		}
		
		Collections.sort(container);		
		
		bean.setBlockCodeList((String[]) container.toArray(new String[0]));
	}
	
	/* (ñ Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
	}


	/**
	 * @return
	 */
	public String[] getBlockCodeList() {
		return blockCodeList;
	}

	/**
	 * @return
	 */
	public Map getBlockMap() {
		return blockMap;
	}

	/**
	 * @return
	 */
	public Map getPrefMap() {
		return prefMap;
	}

	/**
	 * @param strings
	 */
	public void setBlockCodeList(String[] strings) {
		blockCodeList = strings;
	}

	/**
	 * @param map
	 */
	public void setBlockMap(Map map) {
		blockMap = map;
	}

	/**
	 * @param map
	 */
	public void setPrefMap(Map map) {
		prefMap = map;
	}

}

/*
 * 作成日: 2004/09/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.helpdesk;

import com.fjh.forms.ActionForm;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class HD106Form extends ActionForm {

	private String[] certificates = null;    // 証明書発行対象


	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		//

	}


	/**
	 * @return
	 */
	public String[] getCertificates() {
		return certificates;
	}

	/**
	 * @param string
	 */
	public void setCertificates(String[] string) {
		certificates = string;
	}

}

package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;

/**
 * Zà¬ÑªÍ|Zà¬Ñ|Î·lªz S12_11_pêCEFR±Êæ¾óµ
 * ì¬ú: 2019/09/30
 * @author QQ
 *
 */
public class S12CefrMain {

    //wZ¼
    private String strBundleName = "";

    //§R[h
    private String strPrefCd = "";

    //s¹{§¼
    private String strPrefName = "";

    //Fè±
    private int intExamCount = 0;

    //©ó±ÎÛ@pê±R[hXg
    private ArrayList s12EngPtCdList = new ArrayList();

    //S±f[^Xg
    private ArrayList<S12CefrItem> s12AllList = new ArrayList();

    //e±f[^Xg
    private ArrayList<S12CefrItem>  s12EachList = new ArrayList();


    /*----------*/
    /* Get      */
    /*----------*/
    public String getStrBundleName() {
        return strBundleName;
    }
    public String getStrPrefCd() {
        return strPrefCd;
    }
    public String getStrPrefName() {
        return strPrefName;
    }

    public int getIntExamCount() {
        return intExamCount;
    }

    public ArrayList getS12EngPtCdList() {
        return s12EngPtCdList;
    }

    public ArrayList getS12AllList() {
        return s12AllList;
    }
    public ArrayList getS12EachList() {
        return s12EachList;
    }


    /*----------*/
    /* Set      */
    /*----------*/
    public void setStrBundleName(String strBundleName) {
        this.strBundleName = strBundleName;
    }
    public void setStrPrefCd(String strPrefCd) {
        this.strPrefCd = strPrefCd;
    }

    public void setStrPrefName(String strPrefName) {
        this.strPrefName = strPrefName;
    }

    public void setS12EngPtCdList(ArrayList s12EngPtCdList) {
        this.s12EngPtCdList = s12EngPtCdList;
    }
    public void setIntExamCount(int intExamCount) {
        this.intExamCount = intExamCount;
    }

    public void setS12AllList(ArrayList s12AllList) {
        this.s12AllList = s12AllList;
    }
    public void setS12EachList(ArrayList s12EachList) {
        this.s12EachList = s12EachList;
    }





}

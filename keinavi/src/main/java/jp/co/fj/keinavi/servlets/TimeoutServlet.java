package jp.co.fj.keinavi.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.util.log.KNLog;

/**
 * 
 * セッションタイムアウト用Servlet
 * 
 * 2011.08.22	Tomohisa Yamada - TOTEC
 * 				[新規作成]				
 * 
 * 
 */
public class TimeoutServlet extends DefaultHttpServlet {
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		
		//セッションを無効化する
		request.getSession(false).invalidate();
		
		//親にエラー処理を渡す。
		KNServletException e = createTimeoutException();
		
		LoginSession login = new LoginSession();
		KNLog.Err(
				getRemoteAddr(request),
				login.getUserID(),
				login.getSectorSortingCD(),
				login.getAccount(),
				e.getErrorCode(), 
				e.getMessage(),
				e);

		request.setAttribute("KNServletException", e);
			
		// エラー画面へ遷移する
		forward(request, response, JSP_ERROR);
	}
}

package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import jp.co.fj.keinavi.data.individual.PlanUnivData;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import com.fjh.beans.DefaultBean;

/**
 *
 * σ±\θεwΗΑ
 * 
 * <2010NxόC>
 * 2010.01.19	Tomohisa YAMADA - TOTEC
 * 				VKμ¬
 *
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class PlanUnivInsertBean extends DefaultBean {
	
	/**
	 * ΒlID
	 */
	private String individualid;
	
	/**
	 * εwR[h
	 */
	private String univcd;
	
	/**
	 * wR[h
	 */
	private String facultycd;
	
	/**
	 * wΘR[h
	 */
	private String deptcd;
	
	/**
	 * ό`ΤR[h
	 */
	private String entexammodecd;
	
	/**
	 * όζͺ1
	 */
	private int entexamdiv1;
	
	/**
	 * όζͺ2
	 */
	private int entexamdiv2;
	
	/**
	 * όζͺ3
	 */
	private int entexamdiv3;
	
	/**
	 * Nx
	 */
	private String year;
	
	/**
	 * Νζͺ
	 */
	private String examdiv;
	
	/**
	 * σ±\θϊ1
	 */
	private String examplandate1;
	
	/**
	 * σ±\θϊ2
	 */
	private String examplandate2;
	
	/**
	 * σ±\θϊ3
	 */
	private String examplandate3;
	
	/**
	 * σ±\θϊ4
	 */
	private String examplandate4;
	
	/**
	 * σ±\θϊ5
	 */
	private String examplandate5;
	
	/**
	 * σ±\θϊ6
	 */
	private String examplandate6;
	
	/**
	 * σ±\θϊ7
	 */
	private String examplandate7;
	
	/**
	 * σ±\θϊ8
	 */
	private String examplandate8;
	
	/**
	 * σ±\θϊ9
	 */
	private String examplandate9;
	
	/**
	 * σ±\θϊ10
	 */
	private String examplandate10;
	
	/**
	 * nϋ±n1
	 */
	private String proventexamsite1;
	
	/**
	 * nϋ±n2
	 */
	private String proventexamsite2;
	
	/**
	 * nϋ±n3
	 */
	private String proventexamsite3;
	
	/**
	 * nϋ±n4
	 */
	private String proventexamsite4;
	
	/**
	 * nϋ±n5
	 */
	private String proventexamsite5;
	
	/**
	 * nϋ±n6
	 */
	private String proventexamsite6;
	
	/**
	 * nϋ±n7
	 */
	private String proventexamsite7;
	
	/**
	 * nϋ±n8
	 */
	private String proventexamsite8;
	
	/**
	 * nϋ±n9
	 */
	private String proventexamsite9;
	
	/**
	 * nϋ±n10
	 */
	private String proventexamsite10;
	
	/**
	 * υl
	 */
	private String remarks;
	
	/**
	 * σ±\θεwf[^
	 * ¦‘π―Ιgp·ι
	 */
	private List plans;
	
	/**
	 * RXgN^
	 * ¦PXVp
	 */
	public PlanUnivInsertBean(
			String individualid,
			String univcd,
			String facultycd,
			String deptcd,
			String entexammodecd,
			int entexamdiv1,
			int entexamdiv2,
			int entexamdiv3,
			String year,
			String examdiv,
			String remarks,
			String examplandate1,
			String examplandate2,
			String examplandate3,
			String examplandate4,
			String examplandate5,
			String examplandate6,
			String examplandate7,
			String examplandate8,
			String examplandate9,
			String examplandate10,
			String proventexamsite1,
			String proventexamsite2,
			String proventexamsite3,
			String proventexamsite4,
			String proventexamsite5,
			String proventexamsite6,
			String proventexamsite7,
			String proventexamsite8,
			String proventexamsite9,
			String proventexamsite10) {
		this.individualid = individualid;
		this.univcd = univcd;
		this.facultycd = facultycd;
		this.deptcd = deptcd;
		this.entexammodecd = entexammodecd;
		this.entexamdiv1 = entexamdiv1;
		this.entexamdiv2 = entexamdiv2;
		this.entexamdiv3 = entexamdiv3;
		this.year = year;
		this.examdiv = examdiv;
		this.remarks = remarks;
		this.examplandate1 = examplandate1;
		this.examplandate2 = examplandate2;
		this.examplandate3 = examplandate3;
		this.examplandate4 = examplandate4;
		this.examplandate5 = examplandate5;
		this.examplandate6 = examplandate6;
		this.examplandate7 = examplandate7;
		this.examplandate8 = examplandate8;
		this.examplandate9 = examplandate9;
		this.examplandate10 = examplandate10;
		this.proventexamsite1 = proventexamsite1;
		this.proventexamsite2 = proventexamsite2;
		this.proventexamsite3 = proventexamsite3;
		this.proventexamsite4 = proventexamsite4;
		this.proventexamsite5 = proventexamsite5;
		this.proventexamsite6 = proventexamsite6;
		this.proventexamsite7 = proventexamsite7;
		this.proventexamsite8 = proventexamsite8;
		this.proventexamsite9 = proventexamsite9;
		this.proventexamsite10 = proventexamsite10;
	}
	
	/**
	 * RXgN^
	 * ¦‘XVp
	 */
	public PlanUnivInsertBean(
			List plans, 
			String id, 
			String year,
			String div, 
			String remarks) {
		this.plans = plans;
		this.individualid = id;
		this.year = year;
		this.examdiv = div;
		this.remarks = remarks;
	}
	
	/* (ρ Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		if (plans == null) {
			insert(individualid,
				univcd,
				facultycd,
				deptcd,
				entexammodecd,
				entexamdiv1,
				entexamdiv2,
				entexamdiv3,
				year,
				examdiv,
				remarks,
				examplandate1,
				examplandate2,
				examplandate3,
				examplandate4,
				examplandate5,
				examplandate6,
				examplandate7,
				examplandate8,
				examplandate9,
				examplandate10,
				proventexamsite1,
				proventexamsite2,
				proventexamsite3,
				proventexamsite4,
				proventexamsite5,
				proventexamsite6,
				proventexamsite7,
				proventexamsite8,
				proventexamsite9,
				proventexamsite10);
		}
		else {
			insertList();
		}
	}
	
	/**
	 * ‘XV
	 */
	private void insertList() throws Exception {
		
		Iterator ite = plans.iterator();
		
		while (ite.hasNext()) {
			
			PlanUnivData planData = (PlanUnivData) ite.next();
			
			insert(individualid,
				planData.getUnivCd(),
				planData.getFacultyCd(),
				planData.getDeptCd(),
				planData.getEntExamTypeCd(),
				planData.getSchedule().getEntExamDiv_1_2order(),
				planData.getSchedule().getSchoolProventDiv(),
				planData.getSchedule().getEntExamDiv_1_2term(),
				year,
				examdiv,
				remarks,
				planData.getExamPlanDates()[0],
				planData.getExamPlanDates()[1],
				planData.getExamPlanDates()[2],
				planData.getExamPlanDates()[3],
				planData.getExamPlanDates()[4],
				planData.getExamPlanDates()[5],
				planData.getExamPlanDates()[6],
				planData.getExamPlanDates()[7],
				planData.getExamPlanDates()[8],
				planData.getExamPlanDates()[9],
				planData.getProvEntExamSite()[0],
				planData.getProvEntExamSite()[1],
				planData.getProvEntExamSite()[2],
				planData.getProvEntExamSite()[3],
				planData.getProvEntExamSite()[4],
				planData.getProvEntExamSite()[5],
				planData.getProvEntExamSite()[6],
				planData.getProvEntExamSite()[7],
				planData.getProvEntExamSite()[8],
				planData.getProvEntExamSite()[9]);
		}
	}
	
	/**
	 * PXV
	 */
	private void insert(
			String individualid,
			String univcd,
			String facultycd,
			String deptcd,
			String entexammodecd,
			int entexamdiv1,
			int entexamdiv2,
			int entexamdiv3,
			String year,
			String examdiv,
			String remarks,
			String examplandate1,
			String examplandate2,
			String examplandate3,
			String examplandate4,
			String examplandate5,
			String examplandate6,
			String examplandate7,
			String examplandate8,
			String examplandate9,
			String examplandate10,
			String proventexamsite1,
			String proventexamsite2,
			String proventexamsite3,
			String proventexamsite4,
			String proventexamsite5,
			String proventexamsite6,
			String proventexamsite7,
			String proventexamsite8,
			String proventexamsite9,
			String proventexamsite10) throws Exception {
		
		PreparedStatement ps = null;
		
		try{
			ps = conn.prepareStatement(QueryLoader.getInstance().load("i13").toString());
			ps.setString(1, individualid);
			ps.setString(2, univcd);
			ps.setString(3, facultycd);
			ps.setString(4, deptcd);
			ps.setString(5, entexammodecd);
			ps.setString(6, StringUtils.leftPad(Integer.toString(entexamdiv1), 1, '0'));
			ps.setString(7, StringUtils.leftPad(Integer.toString(entexamdiv2), 1, '0'));
			ps.setString(8, StringUtils.leftPad(Integer.toString(entexamdiv3), 2, '0'));
			ps.setString(9, year);
			ps.setString(10, examdiv);
			ps.setString(11, remarks);
			ps.setString(12, examplandate1);
			ps.setString(13, examplandate2);
			ps.setString(14, examplandate3);
			ps.setString(15, examplandate4);
			ps.setString(16, examplandate5);
			ps.setString(17, examplandate6);
			ps.setString(18, examplandate7);
			ps.setString(19, examplandate8);
			ps.setString(20, examplandate9);
			ps.setString(21, examplandate10);
			ps.setString(22, proventexamsite1);
			ps.setString(23, proventexamsite2);
			ps.setString(24, proventexamsite3);
			ps.setString(25, proventexamsite4);
			ps.setString(26, proventexamsite5);
			ps.setString(27, proventexamsite6);
			ps.setString(28, proventexamsite7);
			ps.setString(29, proventexamsite8);
			ps.setString(30, proventexamsite9);
			ps.setString(31, proventexamsite10);
			
			ps.executeUpdate();
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

}

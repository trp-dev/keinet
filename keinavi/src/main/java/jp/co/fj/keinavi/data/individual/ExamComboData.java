package jp.co.fj.keinavi.data.individual;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.data.individual.ExamComboData.YearData.ExamData;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * 個人成績分析用試験コンボボックスデータ
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExamComboData implements Serializable{

    private static final long serialVersionUID = 6327834408100012757L;

    private List yearDatas;

    public ExamComboData(){
        yearDatas = new ArrayList();
    }
    public List getYearDatas(){
        return yearDatas;
    }
    public void setYearDatas(List list){
        yearDatas = list;
    }
    public void addYearData(YearData data){
        if(getYearDatas() == null)
            setYearDatas(new ArrayList());
        yearDatas.add(data);
    }
    public YearData get(String yearString){
        YearData data = null;
        if(getYearDatas() != null){
            for (Iterator it=getYearDatas().iterator(); it.hasNext();){
                YearData yearData = (YearData)it.next();
                if(yearData.getYear().equals(yearString))
                    data = yearData;
            }
        }
        return data;
    }
    public YearData getNewestYearData(){
        YearData data = null;
        int tempYear = 0;
        if(getYearDatas() != null){
            for (Iterator it=getYearDatas().iterator(); it.hasNext();){
                YearData yearData = (YearData)it.next();
                if(Integer.parseInt(yearData.getYear()) > tempYear){
                    data = yearData;
                    tempYear = Integer.parseInt(yearData.getYear());
                }
            }
        }
        return data;
    }
    public YearData getNewYearData(){
        return new YearData();
    }
    public String[] getExamStrings(){
        if(getYearDatas() != null && getYearDatas().size() > 0){
            String[] examStrings = new String[getYearDatas().size()];
            int yearCount = 0;
            for (Iterator it=getYearDatas().iterator(); it.hasNext();){
                YearData yearData = (YearData)it.next();
                examStrings[yearCount] = "examString["+yearCount+"] = new Array(";
                int examCount = 0;
                for (Iterator its=yearData.getExamDatas().iterator(); its.hasNext();){
                    YearData.ExamData examData = (YearData.ExamData)its.next();
                    if(examCount != 0)
                        examStrings[yearCount] += ",";
                    examStrings[yearCount] += "new Array(\""+examData.getExamCode()+"\", \""+examData.getExamName()+"\", \""+examData.getExamTypeCode()+"\")";
                    examCount ++;
                }
                examStrings[yearCount] += ");";
                yearCount ++;
            }
            return examStrings;
        }else{
            return new String[]{"",""};
        }
    }
    public String[] getPreExamStrings(){
        List preExamList = new ArrayList();
        int totalCount = 0;
        if(getYearDatas() != null && getYearDatas().size() > 0){
            for (Iterator it=getYearDatas().iterator(); it.hasNext();){
                YearData yearData =	(YearData)it.next();
                for (Iterator it2=yearData.getExamDatas().iterator(); it2.hasNext();){
                    YearData.ExamData examData = (YearData.ExamData)it2.next();
                    String preExamString = "";
                    if(examData.getPreExamDatas() != null && examData.getPreExamDatas().size() > 0){
                        preExamString = "preExamString["+(totalCount)+"] = new Array(";
                        int preExamCount = 0;
                        for (Iterator it3=examData.getPreExamDatas().iterator(); it3.hasNext();){
                            YearData.ExamData preExamData = (YearData.ExamData)it3.next();
                            if(preExamCount != 0)
                                preExamString += ",";
                            preExamString += "new Array(\""+preExamData.getExamCode()+"\", \""+preExamData.getExamName()+"\")";
                            preExamCount ++;
                        }
                        preExamString += ");";
                        preExamList.add(preExamString);
                        totalCount ++;
                    }
                }
            }
            return (String[]) preExamList.toArray(new String[0]);
        }else{
            return new String[]{"",""};
        }
    }
    public String getYearString(){
        String yearString = "yearString = new Array(";
        if(getYearDatas() == null || getYearDatas().size() < 1)
            return "";
        int count = 0;
        for (Iterator it=getYearDatas().iterator(); it.hasNext();){
            YearData yearData = (YearData)it.next();
            if(count != 0)
                yearString += ",";
            yearString += "new Array(\""+yearData.getYear()+"\", \""+yearData.getYear()+"\")";
            count ++;
        }
        yearString += ");";
        return yearString;
    }

    /**
     * @return 模試データ
     */
    public ExamData getExamData(final String examYear, final String examCd) {

        if (getYearDatas() != null) {
            for (final Iterator ite = getYearDatas().iterator(); ite.hasNext();) {
                final YearData yearData = (YearData) ite.next();
                if (yearData.getYear().equals(examYear)) {
                    return yearData.get(examCd);
                }
            }
        }

        return null;
    }

    /**
     * @return 模試データリスト
     */
    public List getExamList() {

        final List list = new ArrayList();

        if (getYearDatas() != null) {
            for (final Iterator it1 = getYearDatas().iterator(); it1.hasNext();) {
                final YearData yearData = (YearData) it1.next();
                list.addAll(yearData.getExamDatas());
            }
        }

        return list;
    }

    public class YearData implements Comparable,Serializable {

        private static final long serialVersionUID = 657366658515142293L;

        private String year;
        private List examDatas;

        /**
         * @see java.lang.Comparable#compareTo(java.lang.Object)
         */
        public int compareTo(Object o2) {
            YearData e2 = (YearData) o2;
            if (Integer.parseInt(this.getYear()) > Integer.parseInt(e2.getYear())) {
                return -1;
            } else if (Integer.parseInt(this.getYear()) < Integer.parseInt(e2.getYear())) {
                return 1;
            } else {
                return 0;
            }
        }

        public YearData(){
            examDatas = new ArrayList();
        }
        public ExamData getNewExamData(){
            return new ExamData();
        }
        public ExamData get(String examString){
            ExamData data = null;
            if(getExamDatas() != null){
                for (Iterator it=getExamDatas().iterator(); it.hasNext();){
                    ExamData examData = (ExamData)it.next();
                    if(examData.getExamCode().equals(examString))
                        data = examData;
                }
            }
            return data;
        }
        public ExamData getNewestExamData(){
            ExamData data = null;
            long tempSeq = 0;
            if(getExamDatas() != null){
                for (Iterator it=getExamDatas().iterator(); it.hasNext();){
                    ExamData examData = (ExamData)it.next();
                    if(Long.parseLong(examData.getOutDataOpenDate().trim()) > tempSeq){
                        data = examData;
                        tempSeq = Long.parseLong(examData.getOutDataOpenDate().trim());
                    }
                }
            }
            return data;
        }
        public String getYear(){
            return year;
        }
        public void setYear(String string){
            year = string;
        }
        public List getExamDatas(){
            return examDatas;
        }
        public void setExamDatas(Collection collection){
            examDatas = (List)collection;
        }
        public void addExamData(ExamData data){
            if(getExamDatas() == null)
                setExamDatas(new ArrayList());
            examDatas.add(data);
        }
        public void addExam(String year, String code, String name){
            if(getExamDatas() == null)
                setExamDatas(new ArrayList());
            examDatas.add(new ExamData(year, code, name));
        }
        public class ExamData implements Comparable,Serializable {

            private static final long serialVersionUID = -5210773303071927907L;

            private jp.co.fj.keinavi.data.ExamData examData = null;

            private String examYear;
            private String examCode;
            private String examName;
            private String examTypeCode;
            private String dispSequence;
            private String outDataOpenDate;
            private String inpleDate;
            private String examNameAbbr;
            private String targetGrade;

            private List preExamDatas;//<-こいつ自身を使おう！

            private ExamData(){
                preExamDatas = new ArrayList();
            }
            private ExamData(String year, String code, String name){
                setExamYear(year);
                setExamCode(code);
                setExamName(name);
            }
            public String getExamCode(){
                return examCode;
            }
            public void setExamCode(String string){
                examCode = string;
            }
            public String getExamName(){
                return examName;
            }
            public void setExamName(String string){
                examName = string;
            }
            public void setExamTypeCode(String string) {
                examTypeCode = string;
            }
            public void setPreExamDatas(List collection) {
                preExamDatas = collection;
            }
            public String getExamTypeCode() {
                return examTypeCode;
            }
            public List getPreExamDatas() {
                return preExamDatas;
            }
            public void addPreExamData(ExamData data){
                if(getPreExamDatas() == null)
                    setPreExamDatas(new ArrayList());
                preExamDatas.add(data);
            }
            public ExamData getPre(String examString){
                ExamData data = null;
                if(getPreExamDatas() != null){
                    for (Iterator it=getPreExamDatas().iterator(); it.hasNext();){
                        ExamData examData = (ExamData)it.next();
                        if(examData.getExamTypeCode().equals(examString))
                            data = examData;
                    }
                }
                return data;
            }
            public ExamData getNewestPreExamData(){
                ExamData data = null;
                long tempSeq = 0;
                if(getPreExamDatas() != null){
                    for (Iterator it=getPreExamDatas().iterator(); it.hasNext();){
                        ExamData examData = (ExamData)it.next();
                        if(Long.parseLong(examData.getOutDataOpenDate()) > tempSeq){
                            data = examData;
                            tempSeq = Long.parseLong(examData.getOutDataOpenDate());
                        }
                    }
                }
                return data;
            }
            public String getDispSequence() {
                return dispSequence;
            }
            public void setDispSequence(String string) {
                dispSequence = string;
            }

            /**
             * @return
             */
            public String getOutDataOpenDate() {
                return outDataOpenDate;
            }

            /**
             * @param string
             */
            public void setOutDataOpenDate(String string) {
                outDataOpenDate = string;
            }

            /* (非 Javadoc)
             * @see java.lang.Comparable#compareTo(java.lang.Object)
             */
            public int compareTo(Object o2) {
                ExamData e2 = (ExamData) o2;
                if (Integer.parseInt(this.getDispSequence()) < Integer.parseInt(e2.getDispSequence())) {
                    return -1;
                } else if (Integer.parseInt(this.getDispSequence()) > Integer.parseInt(e2.getDispSequence())) {
                    return 1;
                } else {
                    return 0;
                }
            }
            /**
             * @return examYear を戻します。
             */
            public String getExamYear() {
                return examYear;
            }
            /**
             * @param pExamYear 設定する examYear。
             */
            public void setExamYear(String pExamYear) {
                examYear = pExamYear;
            }
            /**
             * @return examNameAbbr を戻します。
             */
            public String getExamNameAbbr() {
                return examNameAbbr;
            }
            /**
             * @param pExamNameAbbr 設定する examNameAbbr。
             */
            public void setExamNameAbbr(String pExamNameAbbr) {
                examNameAbbr = pExamNameAbbr;
            }
            /**
             * @return inpleDate を戻します。
             */
            public String getInpleDate() {
                return inpleDate;
            }
            /**
             * @param pInpleDate 設定する inpleDate。
             */
            public void setInpleDate(String pInpleDate) {
                inpleDate = pInpleDate;
            }
            /**
             * @return targetGrade を戻します。
             */
            public String getTargetGrade() {
                return targetGrade;
            }
            /**
             * @param pTargetGrade 設定する targetGrade。
             */
            public void setTargetGrade(String pTargetGrade) {
                targetGrade = pTargetGrade;
            }

            public jp.co.fj.keinavi.data.ExamData toExamData() {

                if (examData == null) {
                    examData = new jp.co.fj.keinavi.data.ExamData();
                    examData.setExamYear(getExamYear());
                    examData.setExamCD(getExamCode());
                    examData.setExamTypeCD(getExamTypeCode());
                    examData.setExamName(getExamName());
                    examData.setExamNameAbbr(getExamNameAbbr());
                    examData.setOutDataOpenDate(getOutDataOpenDate());
                    examData.setInpleDate(getInpleDate());
                    examData.setTargetGrade(getTargetGrade());
                    examData.setDispSequence(new Integer(getDispSequence()).intValue());
                }

                return examData;
            }
        }
    }

    /**
     * @return パターンIに該当する模試リスト
     */
    public List getListI() {

        final List list = new ArrayList();

        for (final Iterator ite = getExamList().iterator(); ite.hasNext();) {

            final ExamData exam = (ExamData) ite.next();

            if (KNUtil.isPatternI(exam.toExamData())) {
                list.add(exam);
            }
        }

        return list;
    }

    /**
     * @return パターンIIに該当する模試リスト
     */
    public List getListII() {

        final List list = new ArrayList();

        for (final Iterator ite = getExamList().iterator(); ite.hasNext();) {

            final ExamData exam = (ExamData) ite.next();

            if (KNUtil.isPatternII(exam.toExamData())) {
                list.add(exam);
            }
        }

        return list;

    }

    /**
     * @return パターンIIIに該当する模試リスト
     */
    public List getListIII() {

        final List list = new ArrayList();

        for (final Iterator ite = getExamList().iterator(); ite.hasNext();) {

            final ExamData exam = (ExamData) ite.next();

            if (KNUtil.isPatternIII(exam.toExamData())) {
                list.add(exam);
            }
        }

        return list;

    }

    /**
     * @return パターンIVに該当する模試リスト
     */
    public List getListIV() {

        final List list = new ArrayList();

        for (final Iterator ite = getExamList().iterator(); ite.hasNext();) {

            final ExamData exam = (ExamData) ite.next();

            if (KNUtil.isPatternIV(exam.toExamData())) {
                list.add(exam);
            }
        }

        return list;

    }

    /**
     * @return 学力測定以外の模試リスト
     */
    public List getListNotAbility() {

        final List list = new ArrayList();

        for (final Iterator ite = getExamList().iterator(); ite.hasNext();) {

            final ExamData exam = (ExamData) ite.next();

            if (!KNUtil.isAbilityExam(exam.toExamData())) {
                list.add(exam);
            }
        }

        return list;
    }

}

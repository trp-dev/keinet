package jp.co.fj.keinavi.servlets.login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.DefaultLoginServlet;

/**
 *
 * ログアウトサーブレット
 * 
 * 2004/09/30	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class LogoutServlet extends DefaultLoginServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		final LoginSession login = getLoginSession(request);
		
		// アクセスログ
		actionLog(request, "110");

		// 体験版 or ログインしていない場合
		if (login == null || login.getTrialMode() == 1) {
			forward(request, response, JSP_CLOSE);
		// ヘルプデスク
		} else if (login.isHelpDesk()) {
			forward(request, response, "/HelpLogin");
		// 営業・校舎
		} else if (login.isBusiness() || login.isDeputy()) {
			forward(request, response, "/StaffLogin");
		// 高校（無料用？）
		} else if (login.isSimple()) {
			forward(request, response, "/DLLogin");
		// 高校
		} else if (login.isSchool()) {
			forward(request, response, "/Login");
		// 不明ならエラー	
		} else {
			throw new ServletException("ログイン画面の判定に失敗しました。");
		}
	}

}

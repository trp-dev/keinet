/*
 * 作成日: 2004/08/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class I301Data implements Comparable {
	
	private String univCd;				//大学コード
	private String univName_Abbr;		//大学
	private String facultyCd;			//学部コード
	private String facultyName_Abbr;	//学部
	private String deptCd;				//学科コード
	private String deptName_Abbr;		//学科
	private String prefName;			//所在地
	private String markGRating;		//マーク模試評価
	private String discriptionGRating;	//記述模試評価
	private String ratingPoint;		//総合評価ポイント
	private String totalRating;		//総合評価
	private String allotPntRate1;		//配点比率１
	private String allotPntRate2;		//配点比率２
	private String candidateRank; 		//志望順位

	
	private boolean isRegistered;		//既に登録されているか
	private boolean isExist;			//大学が存在するか
	
	/**
	 * 1.JSP側でチェックボックスのValueとして使う
	 * 2.詳細画面に渡すパラメターとして使う
	 * string[0] = 大学コード
	 * string[1] = 学部コード
	 * string[2] = 学科コード
	 * @return
	 */
	public String getUnivValue(){
		return getUnivCd()+","+getFacultyCd()+","+getDeptCd();
	}
	
	/**
	 * I205と互換性を保つために追加
	 * @return
	 */
	public String getUniqueKey(){
		return getUnivCd()+getFacultyCd()+getDeptCd();
	}
	
	/* (非 Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 * Collections.sort(list)でソートしましょう;
	 */
	public int compareTo(Object o) {
		I301Data data = (I301Data) o;
		if (Integer.parseInt(this.candidateRank) < Integer.parseInt(data.getCandidateRank())) {
			return -1;
		} else if (Integer.parseInt(this.candidateRank) > Integer.parseInt(data.getCandidateRank())) {
			return 1;
		} else {
			return 0;
		}
	}
	
	/**
	 * @return
	 */
	public String getDeptName_Abbr() {return deptName_Abbr;}

	/**
	 * @return
	 */
	public String getDiscriptionGRating() {return discriptionGRating;}

	/**
	 * @return
	 */
	public String getFacultyName_Abbr() {return facultyName_Abbr;}

	/**
	 * @return
	 */
	public String getMarkGRating() {return markGRating;}

	/**
	 * @return
	 */
	public String getPrefName() {return prefName;}
	/**
	 * @return
	 */
	public String getRatingPoint() {return ratingPoint;}

	/**
	 * @return
	 */
	public String getTotalRating() {return totalRating;}

	/**
	 * @return
	 */
	public String getUnivName_Abbr() {return univName_Abbr;}

	/**
	 * @param string
	 */
	public void setDeptName_Abbr(String string) {deptName_Abbr = string;}

	/**
	 * @param string
	 */
	public void setDiscriptionGRating(String string) {discriptionGRating = string;}

	/**
	 * @param string
	 */
	public void setFacultyName_Abbr(String string) {facultyName_Abbr = string;}

	/**
	 * @param string
	 */
	public void setMarkGRating(String string) {markGRating = string;}

	/**
	 * @param string
	 */
	public void setPrefName(String string) {prefName = string;}

	/**
	 * @param string
	 */
	public void setRatingPoint(String string) {ratingPoint = string;}

	/**
	 * @param string
	 */
	public void setTotalRating(String string) {totalRating = string;}

	/**
	 * @param string
	 */
	public void setUnivName_Abbr(String string) {univName_Abbr = string;}

	/**
	 * @return
	 */
	public boolean isRegistered() {return isRegistered;}

	/**
	 * @param b
	 */
	public void setRegistered(boolean b) {isRegistered = b;}

	/**
	 * @return
	 */
	public String getAllotPntRate1() {return allotPntRate1;}

	/**
	 * @return
	 */
	public String getAllotPntRate2() {return allotPntRate2;}

	/**
	 * @param string
	 */
	public void setAllotPntRate1(String string) {allotPntRate1 = string;}

	/**
	 * @param string
	 */
	public void setAllotPntRate2(String string) {allotPntRate2 = string;}

	/**
	 * @return
	 */
	public String getDeptCd() {return deptCd;}

	/**
	 * @return
	 */
	public String getFacultyCd() {return facultyCd;}

	/**
	 * @return
	 */
	public String getUnivCd() {return univCd;}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {deptCd = string;}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {facultyCd = string;}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {univCd = string;}



	/**
	 * @return
	 */
	public String getCandidateRank() {
		return candidateRank;
	}

	/**
	 * @param string
	 */
	public void setCandidateRank(String string) {
		candidateRank = string;
	}

	/**
	 * @return
	 */
	public boolean isExist() {
		return isExist;
	}

	/**
	 * @param b
	 */
	public void setExist(boolean b) {
		isExist = b;
	}

}

package jp.co.fj.keinavi.util;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * AES方式で暗号化を行うクラス
 * 
 * 2007.07.31	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class AesEncryptor {

	// 秘密鍵
	private String KEY = "kawaijuku keinet secret code";
	// 鍵の長さ（バイト長）
	private static final int KEY_LENGTH = 128 / 8;
	// 暗号化する文字列のバイト化に使う文字セット
	private static final String CHARSET = "EUC-JP";
	
	// Cipher
	private final Cipher cipher;
	
	/**
	 * コンストラクタ
	 */
	public AesEncryptor() {
		
		try {
			cipher = Cipher.getInstance("AES/CBC/NoPadding");
			cipher.init(Cipher.ENCRYPT_MODE, createKey());
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * コンストラクタ
	 * @param key（秘密鍵）
	 */
	public AesEncryptor(String key) {
		try {
			//秘密鍵を指定
			KEY = key;
			cipher = Cipher.getInstance("AES/CBC/NoPadding");
			cipher.init(Cipher.ENCRYPT_MODE, createKey());
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * @return 初期化ベクトル
	 */
	public byte[] getIv() {
		return cipher.getIV();
	}
	
	/**
	 * 文字列を暗号化する
	 * 
	 * @param value 暗号化する文字列
	 * @return 暗号化した結果
	 */
	public byte[] encrypt(final String value) {
		
		try {
			return cipher.doFinal(padding(value));
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	// 秘密鍵を作る
	private SecretKeySpec createKey() {
		final String key = DigestUtils.md5Hex(KEY).substring(0, KEY_LENGTH);
		return new SecretKeySpec(key.getBytes(), "AES");
	}
	
	private byte[] padding(final String value) throws UnsupportedEncodingException {

		final byte[] src = value.getBytes(CHARSET);
		
		if (src.length == 0) {
			return new byte[0];
		}
		
		final byte[] dest = new byte[src.length + (16 - src.length % 16)];
		Arrays.fill(dest, (byte) 0x00);
		System.arraycopy(src, 0, dest, 0, src.length);
		
		return dest;
	}
	
}

package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import jp.co.fj.keinavi.beans.NewestExamDivSearchBean;
import jp.co.fj.keinavi.data.individual.PlanUnivData;
import jp.co.fj.keinavi.data.individual.ScheduleDetailData;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.date.ShortDatePlusUtil;

import com.fjh.beans.DefaultBean;

/**
 *
 * 受験予定大学取得
 * 
 * 2004.08.25	Tomohisa YAMADA - TOTEC
 * 				[新規作成]
 * 
 * <2010年度改修>
 * 2009.12.03	Shoji HASE - Totec
 *              「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 * 　　　　     「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 *
 * 2009.01.20	Tomohisa YAMADA - TOTEC
 * 				入試日程変更対応
 *
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
abstract public class PlanUnivBean extends DefaultBean{
		
	/** 入力パラメター */
	//共通入力パラメター
	protected String individualId;
	//更新用入力パラメーター
	protected String[] univValues;
	private String[] provEntExamSite;
	//複製・削除用入力パラメター
	protected String univValue;
	//複製用パラメータ
	protected String updateUnivValue;
	//備考
	private String remarks;
	
	//エラー
	private String errorMessage;
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	abstract public void execute() throws SQLException, Exception;
	
	//登録済みか登録済みでないかを返します
	protected boolean execChackPlanUniv(
			String individualId, 
			String univCd, 
			String facultyCd, 
			String deptCd, 
			String entExamTypeCd) throws Exception {
		
		PlanUnivCountBean bean = 
			new PlanUnivCountBean(
					individualId, 
					univCd, 
					facultyCd, 
					deptCd, 
					entExamTypeCd);
		bean.setDiv1(1);
		bean.setDiv2(1);
		bean.setDiv3(1);
		bean.setConnection(null, conn);
		bean.execute();
		
		return bean.getCount() == 0;
	}

	
	//この人の受験予定大学にあるすべての受験大学　＋　登録したい大学　の件数をチェックします
	protected int execCountExamPlanUniv(String individualId, String univCd, String facultyCd, String deptCd) throws Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map map = new HashMap();
		int count = 0;
		
		try{
			//その人の受験予定大学をすべて取得する。
			StringBuffer strBuf = new StringBuffer();
			strBuf.append("SELECT UNIVCD, FACULTYCD, DEPTCD FROM EXAMPLANUNIV WHERE ");
			strBuf.append("INDIVIDUALID = '").append(individualId).append("' ");
			strBuf.append("GROUP BY UNIVCD, FACULTYCD, DEPTCD");
			
			ps = conn.prepareStatement(strBuf.toString());
			rs = ps.executeQuery();
			
			map.put(univCd + facultyCd + deptCd, univCd + "," + facultyCd + "," + deptCd);
			while(rs.next()) {
				String univData[] = new String[3];
				univData[0] = rs.getString("UNIVCD");
				univData[1] = rs.getString("FACULTYCD");
				univData[2] = rs.getString("DEPTCD");
				map.put((univData[0] + univData[1] + univData[0]), CollectionUtil.deSplitComma(univData));
			}
			if(ps != null) {ps.close();}
			if(rs != null) {rs.close();}
			
			count = map.values().size();
			
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(ps != null) {ps.close();}
				if(rs != null) {rs.close();}
			} catch(Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
		return count;
	}
	
	//受験予定大学情報のリストに必須のデータを追加します//大学学部学科／模試区分で一意になるレコードの必須のデフォルトデータを取得します。
	protected Collection setNecessaryData(Collection planDatas) {
		
		final int DEFAULT_ENTEXAMDIV1 = 1;
		final int DEFAULT_ENTEXAMDIV2 = 1;
		final int DEFAULT_ENTEXAMDIV3 = 1;
		
		PlanUnivData necessaryData = new PlanUnivData();
		
		int count = 0;
		//必須のデータがあるか？
		for (java.util.Iterator it=planDatas.iterator(); it.hasNext();) {
			PlanUnivData planData = (PlanUnivData)it.next();
			if(planData.getSchedule().getEntExamDiv_1_2order() == DEFAULT_ENTEXAMDIV1 && 
				planData.getSchedule().getSchoolProventDiv() == DEFAULT_ENTEXAMDIV2 && 
				planData.getSchedule().getEntExamDiv_1_2term() == DEFAULT_ENTEXAMDIV3) {
				//あるならＯＫ
				return planDatas;
			}
			//必須データが必要なときのために入れておく。
			if(count == 0) {
				necessaryData.setYear(planData.getYear());						//年度
				necessaryData.setExamDiv(planData.getExamDiv());				//模試区分
				necessaryData.setUnivCd(planData.getUnivCd());					//大学コード
				necessaryData.setFacultyCd(planData.getFacultyCd());			//学部コード
				necessaryData.setDeptCd(planData.getDeptCd());					//学科コード
				necessaryData.setEntExamTypeCd(planData.getEntExamTypeCd());	//入試形態コード
			} 
			count ++;
		}
		
		//ないなら新規作成して追加
		necessaryData.setSchedule(new ScheduleDetailData(
				DEFAULT_ENTEXAMDIV1, 
				DEFAULT_ENTEXAMDIV2, 
				DEFAULT_ENTEXAMDIV3));

		String[] dates = new String[10];
		String[] sites = new String[10];
		necessaryData.setExamPlanDates(dates);			//受験予定日
		necessaryData.setProvEntExamSite(sites);		//地方試験地
		planDatas.add(necessaryData);
		
		return planDatas;
	}
	
	/**
	 * インサート用の受験予定大学データを作成します。
	 * @param univValues
	 * @return Collection
	 */
	protected Collection createInsertUnivDatas(String[] univValues, String univValue) throws SQLException{
		//「チェックが入っている」
		Map map = new LinkedHashMap();
		
		/** univValueの展開 */
		//年度と模試区分は、高３大学マスタの最新を取得する。
		
		//フル	9999,99,99,01,1,1,1,2/2,2004,07,on,東京
		//新規	9999,99,99,01,1,1,1,2/2,YYYY,XX,on,東京
		//追加	1,1,1,9,28,東京
		//univValueの中身
		//[0]	大学コード		9999
		//[1]	学部コード		99
		//[2]	学科コード		99
		//[3]	入試形態		01
		//[4]	入試区分１		1
		//[5]	入試区分２		1
		//[6]	入試区分３		1
		//[7]	入試日			2/2
		//[8]	模試年度		2004
		//[9]	模試区分		07
		//[10]	チェックフラグ	on
		//[11]	地方試験地		東京
			
		//渡された配列分繰り返す
		for(int i = 0; i < univValues.length; i++){
			String univCd			 = null;	//大学
			String facultyCd		 = null;	//学部
			String deptCd			 = null;	//学科
			String entExamTypeCd	 = null;	//入試形態
			String entExamDiv1		 = null;	//区分１
			String entExamDiv2		 = null;	//区分２
			String entExamDiv3		 = null;	//区分３
			String examPlanDate		 = null;	//日程
			String provEntExamSite	 = null;	//試験地
			String examYear			 = null;
			String examDiv			 = null;
			
			String[] univData = CollectionUtil.splitComma(univValues[i]);
			String key = null;
			
			if(univData.length > 10) {
				//チェックボックスの処理
				if(univData[10].equals("on")) {
					univCd			 = univData[0];	//大学
					facultyCd		 = univData[1];	//学部
					deptCd			 = univData[2];	//学科
					entExamTypeCd	 = univData[3];	//入試形態
					entExamDiv1		 = univData[4];	//区分１
					entExamDiv2		 = univData[5];	//区分２
					entExamDiv3		 = univData[6];	//区分３	
					examPlanDate	 = univData[7];	//日程
					examYear		 = univData[8];	//年度
					examDiv			 = univData[9];	//模試区分
					if(univData.length > 11) {
						provEntExamSite	 = univData[11];	//試験地
					} else {
						
					}
				} else {
					continue;
				}
			} else if(univData.length > 9) {
				univCd			 = univData[0];	//大学
				facultyCd		 = univData[1];	//学部
				deptCd			 = univData[2];	//学科
				entExamTypeCd	 = univData[3];	//入試形態
				entExamDiv1		 = univData[4];	//区分１
				entExamDiv2		 = univData[5];	//区分２
				entExamDiv3		 = univData[6];	//区分３	
				examPlanDate	 = univData[7];	//日程
				examYear		 = univData[8];	//年度
				examDiv			 = univData[9];	//模試区分
			} else if(univData.length > 4) {
				//コンボボックスの処理(入試区分あり)
				//変更後　追加	1,1,1,9/28,東京
				//画面よりわたってきた(大学・学部・学科・入試形態)
				String[] univ4data = CollectionUtil.splitComma(univValue);
				univCd			 = univ4data[0];		//大学(初期表示時のもの)
				facultyCd		 = univ4data[1];		//学部(初期表示時のもの)
				deptCd			 = univ4data[2];		//学科(初期表示時のもの)
				entExamTypeCd	 = univ4data[3];		//入試形態
				entExamDiv1		 = univData[0];			//区分１
				entExamDiv2		 = univData[1];			//区分２
				entExamDiv3		 = univData[2];			//区分３
				examPlanDate	 = univData[3];			//日程
				provEntExamSite	 = univData[4];			//試験地
			} else {
				//コンボボックスの処理(入試区分なし)
				//追加	newtable,9/28,東京
				String[] univ4data = CollectionUtil.splitComma(univValue);
				univCd			 = univ4data[0];		//大学(初期表示時のもの)
				facultyCd		 = univ4data[1];		//学部(初期表示時のもの)
				deptCd			 = univ4data[2];		//学科(初期表示時のもの)
				entExamTypeCd	 = univ4data[3];		//入試形態
				entExamDiv1		 = "1";					//区分１
				entExamDiv2		 = "1";					//区分２
				entExamDiv3		 = "1";					//区分３
				examPlanDate	 = univData[1];			//日程
				provEntExamSite	 = univData[2];			//試験地
			}
			
			//キー値
			key = univCd + facultyCd + deptCd + 
				entExamTypeCd + entExamDiv1 + entExamDiv2 + entExamDiv3;
			
			//マップよりキー値のデータを取得
			PlanUnivData plan = (PlanUnivData)map.get(key);
			
			//無いなら作成
			if(plan == null) {
				plan = new PlanUnivData();
				plan.setYear(examYear);					//年度
				plan.setExamDiv(examDiv);				//模試区分
				plan.setUnivCd(univCd);					//大学コード
				plan.setFacultyCd(facultyCd);			//学部コード
				plan.setDeptCd(deptCd);					//学科コード
				plan.setEntExamTypeCd(entExamTypeCd);	//入試形態コード
				String[] str1 = new String[10];
				String[] str2 = new String[10];
				plan.setExamPlanDates(str1);			//受験予定日
				plan.setProvEntExamSite(str2);			//地方試験地
				
				plan.setSchedule(new ScheduleDetailData(
						Integer.parseInt(entExamDiv1), 
						Integer.parseInt(entExamDiv2), 
						Integer.parseInt(entExamDiv3)));
				
			}
			
			final int MAXCOUNT = 10;
				
			String[] planDates = plan.getExamPlanDates();	//登録されているデータ
			String[] examSites = plan.getProvEntExamSite();	//登録されているデータ
			
			if(!examPlanDate.equals("0/0")) {
				//無効値で無ければ日付を追加する1106
				for(int count = 0; count < MAXCOUNT; count++) {
					//受験予定日１〜１０で空きを探す、空きがあれば日付をいれ、試験地を入れてループ終了
					if(planDates[count] == null) {
						planDates[count] = ShortDatePlusUtil.getUnformattedString(examPlanDate); //受験予定日１〜１０
						examSites[count] = provEntExamSite;	//地方試験地１〜１０
						plan.setExamPlanDates(planDates);
						plan.setProvEntExamSite(examSites);
						break;
					}
					//既に１０個あるとき。
					if(count == (MAXCOUNT-1)) {
						setErrorMessage("日程が多すぎます\\n予定大学の日程は最大１０日です");
						return null;
					}
				}
			}
			//マップに追加
			map.put(key, plan);
		}
		
		Collection list = new ArrayList();
		for (java.util.Iterator it=map.values().iterator(); it.hasNext();) {
			PlanUnivData planData = (PlanUnivData)it.next();
			list.add(planData);
		}
		
		return list;
	}
	
	/**
	 * 引数で指定された、受験予定大学をInsertします。
	 * @param individualId
	 * @param planDatas
	 * @throws SQLException
	 */
	protected void execInsertPlanUniv(String individualId, Collection planDatas) throws Exception {
			
		//最新の年度、模試区分を取得
		NewestExamDivSearchBean newest = new NewestExamDivSearchBean();
		newest.setConnection(null, conn);
		newest.execute();

		//リスト回す
		for (java.util.Iterator it=planDatas.iterator(); it.hasNext();) {
			
			PlanUnivData planData = (PlanUnivData)it.next();
			
			PlanUnivInsertBean insert = 
				new PlanUnivInsertBean(
						individualId, 
						planData.getUnivCd(), 
						planData.getFacultyCd(), 
						planData.getDeptCd(), 
						planData.getEntExamTypeCd(), 
						planData.getSchedule().getEntExamDiv_1_2order(), 
						planData.getSchedule().getSchoolProventDiv(), 
						planData.getSchedule().getEntExamDiv_1_2term(), 
						newest.getExamYear(), 
						newest.getExamDiv(), 
						getRemarks(), 
						planData.getExamPlanDates()[0],
						planData.getExamPlanDates()[1],
						planData.getExamPlanDates()[2],
						planData.getExamPlanDates()[3],
						planData.getExamPlanDates()[4],
						planData.getExamPlanDates()[5],
						planData.getExamPlanDates()[6],
						planData.getExamPlanDates()[7],
						planData.getExamPlanDates()[8],
						planData.getExamPlanDates()[9],
						planData.getProvEntExamSite()[0],
						planData.getProvEntExamSite()[1],
						planData.getProvEntExamSite()[2],
						planData.getProvEntExamSite()[3],
						planData.getProvEntExamSite()[4],
						planData.getProvEntExamSite()[5],
						planData.getProvEntExamSite()[6],
						planData.getProvEntExamSite()[7],
						planData.getProvEntExamSite()[8],
						planData.getProvEntExamSite()[9]);
			insert.setConnection(null, conn);
			insert.execute();
		}

	}
		
	/**
	 * @return
	 */
	public String[] getProvEntExamSite() {
		return provEntExamSite;
	}

	/**
	 * @return
	 */
	public String getUnivValue() {
		return univValue;
	}

	/**
	 * @return
	 */
	public String[] getUnivValues() {
		return univValues;
	}

	/**
	 * @param strings
	 */
	public void setProvEntExamSite(String[] strings) {
		provEntExamSite = strings;
	}

	/**
	 * @param string
	 */
	public void setUnivValue(String string) {
		univValue = string;
	}

	/**
	 * @param strings
	 */
	public void setUnivValues(String[] strings) {
		univValues = strings;
	}

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

	/**
	 * @return
	 */
	public String getUpdateUnivValue() {
		return updateUnivValue;
	}

	/**
	 * @param string
	 */
	public void setUpdateUnivValue(String string) {
		updateUnivValue = string;
	}

	/**
	 * @return
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param string
	 */
	public void setErrorMessage(String string) {
		errorMessage = string;
	}

	/**
	 * @return
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param string
	 */
	public void setRemarks(String string) {
		remarks = string;
	}

}

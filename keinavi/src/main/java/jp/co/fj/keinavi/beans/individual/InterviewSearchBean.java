/*
 * 作成日: 2004/07/27
 *
 * 成績分析／成績推移グラフの偏差値データ取得用Bean
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.data.individual.InterviewData;
import jp.co.fj.keinavi.util.db.RecordProcessor;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import com.fjh.beans.DefaultSearchBean;



/**
 * @author
 *
 */
public class InterviewSearchBean extends DefaultSearchBean {
	
	private final String schoolCd; //学校コード
	private final String individualId; //個人ID
	
	/**
	 * コンストラクタ
	 * @param pSchoolCd 学校コード
	 * @param pIndividualId 個人ID
	 */
	public InterviewSearchBean(
			final String pSchoolCd, final String pIndividualId) {
        
		if (pSchoolCd == null) {
            throw new IllegalArgumentException("学校コードがnullです。");
        }
        if (pIndividualId == null) {
            throw new IllegalArgumentException("個人IDがnullです。");
        }
        
		schoolCd = pSchoolCd;
		individualId = pIndividualId;
	}
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//面談メモ情報取得
			ps = conn.prepareStatement(QueryLoader.getInstance().getQuery("i05"));
			ps.setString(1, individualId);
			ps.setString(2, schoolCd);
			ps.setString(3, individualId);
			
			rs = ps.executeQuery();
			while(rs.next()){
				InterviewData data = new InterviewData();
				data.setIndividualId(rs.getString("INDIVIDUALID"));
				data.setKoIndividualId(rs.getString("KO_INDIVIDUALID"));
				data.setFirstRegistrYdt(rs.getString("FIRSTREGISTRYDT"));
				data.setCreateDate(RecordProcessor.dateWSlash(
						rs.getString("CREATEDATE"), false));
				data.setTitle(rs.getString("TITLE"));
				data.setText(rs.getString("TEXT"));
				data.setRenewalDate(RecordProcessor.dateWSlash(
						rs.getString("RENEWALDATE"), false));
				recordSet.add(data);
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

}

package jp.co.fj.keinavi.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

/**
 *
 * Kei-Navi用ユーティリティクラス
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)HASE.Shoji
 * @author TOTEC)YAMADA.Tomohisa
 *
 */
public class KNUtil {

    /**
     * アラビア文字
     */
    public static final String[] ROMAN = {
         "�T"
        ,"�U"
        ,"�V"
        ,"�W"
        ,"�X"
        ,"�Y"
        ,"�Z"
    };

    /**
     *  センター到達エリア文字列
     */
    public static final String[] CENTERREACHAREA = {
         "S"
        ,"A"
        ,"B"
        ,"C"
        ,"D"
        ,"E"
        ,"F"
    };

    /**
     * センター予想得点率
     */
    public static final Map CENTER_EXSCORERATE_MAP = new HashMap();
    static {
        CENTER_EXSCORERATE_MAP.put("1", "90%〜");
        CENTER_EXSCORERATE_MAP.put("2", "80%〜");
        CENTER_EXSCORERATE_MAP.put("3", "70%〜");
        CENTER_EXSCORERATE_MAP.put("4", "60%〜");
        CENTER_EXSCORERATE_MAP.put("5", "50%〜");
        CENTER_EXSCORERATE_MAP.put("6", "40%〜");
        CENTER_EXSCORERATE_MAP.put("7", "〜39%");
    }

    /**
     *  センター到達エリア最下位（コード）
     */
    public static final String minCENTERREACHAREA = "7";

    /**
     * 受験学力テスト
     */
    public static final String TYPECD_ABILITY = "32";

    /**
     * マーク系
     */
    public static final String TYPECD_MARK = "01";

    /**
     * 記述系
     */
    public static final String TYPECD_WRTN = "02";

    /**
     * 私大模試
     */
    public static final String TYPECD_PRIVATE = "03";

    /**
     * 高1・2模試
     */
    public static final String TYPECD_UNIV12 = "04";

// 2019/10/03 QQ)Tanioka 共通テスト対応 ADD START
    /**
     * プライムステージ(プレステージ)
     */
    public static final String TYPECD_PSTAGE = "91";

    /**
     * プレ共通テスト(旧センタープレ)
     */
    public static final String CD_CENTERP = "04";
// 2019/10/03 QQ)Tanioka 共通テスト対応 ADD END

   /**
     * センターリサーチ
     */
    public static final String CD_CENTER = "38";

    /**
     * 高２マーク
     */
    public static final String CD_H2MARK = "66";

    /**
     * 高２記述
     */
    public static final String CD_H2WRTN_OLD = "67";

    /**
     * 高２記述
     */
    public static final String CD_H2WRTN_2006 = "65";

    /**
     * 高１プレステージ
     */
    public static final String CD_H1PSTAGE = "76";

    /**
     * 高２プレステージ
     */
    public static final String CD_H2PSTAGE = "77";

    /**
     * 高３プレステージ
     */
    public static final String CD_H3PSTAGE = "10";


    // getCurrentYear() 用のカレンダーインスタンス
    private static Calendar calendar;
    // プライバシー保護学校コードセット
    public static final Set PRIVACY_PROTECTION_SCHOOL;

    static {
        PRIVACY_PROTECTION_SCHOOL =
            Collections.unmodifiableSet(
                    CollectionUtil.array2Set(
                            CollectionUtil.splitComma(
                                    KNCommonProperty.getPrivacyProtectionSchool()
                            )
                    )
            );
    }

    // 第1解答科目対応模試（＃１〜＃３マーク、センタープレ、センターリサーチ）
    private static final Set ANS1ST_EXAM = new HashSet();
    static {
        ANS1ST_EXAM.add("01");
        ANS1ST_EXAM.add("02");
        ANS1ST_EXAM.add("03");
        ANS1ST_EXAM.add("04");
        ANS1ST_EXAM.add("38");
    }

    // オープン模試の模試種類コードセット
    private static final Set OPEN_EXAM_TYPE_SET = new HashSet();
    static {
        // 東大
        OPEN_EXAM_TYPE_SET.add("10");
        // 京大
        OPEN_EXAM_TYPE_SET.add("11");
        // 名大
        OPEN_EXAM_TYPE_SET.add("12");
        // 一橋大
        OPEN_EXAM_TYPE_SET.add("13");
        // 東工大
        OPEN_EXAM_TYPE_SET.add("14");
        // 北大
        OPEN_EXAM_TYPE_SET.add("15");
        // 東北大
        OPEN_EXAM_TYPE_SET.add("16");
        // 阪大
        OPEN_EXAM_TYPE_SET.add("17");
        // 広大
        OPEN_EXAM_TYPE_SET.add("18");
        // 九大
        OPEN_EXAM_TYPE_SET.add("19");
        // 神大
        OPEN_EXAM_TYPE_SET.add("1A");
        // 早大・慶大
        OPEN_EXAM_TYPE_SET.add("1D");
    }

    /**
     * 現在の年度を取得する
     * @return
     */
    public synchronized static String getCurrentYear() {
        // 初期化
        if (calendar == null) calendar = Calendar.getInstance();

        // 現在の時刻にする
        calendar.setTimeInMillis(System.currentTimeMillis());

        // 3月31日までなら前年度
        if (calendar.get(Calendar.MONTH) < 3) return String.valueOf(calendar.get(Calendar.YEAR) - 1);
        // 4月1日以降ならそのまま
        else return String.valueOf(calendar.get(Calendar.YEAR));

    }

    /**
     * 同年度で過去の同種の模試リストを取得する
     *
     * @param examSession 模試セッション
     * @param exam 評価基準模試
     * @return
     */
    public static List getPastExamList(final ExamSession examSession, final ExamData exam) {

        List container = new LinkedList(); // 入れ物

        // 模試がなければ評価しない
        if (exam == null) return container;

        // 評価基準年度の模試リスト
        List examList =  examSession.getExamList(exam.getExamYear());

        // 模試リストがなければ評価しない
        if (examList == null) return container;

        Iterator ite = examList.iterator();
        while (ite.hasNext()) {
            ExamData data = (ExamData) ite.next();

            // 模試の種類が同じかつ対象学年が同じかつ対象模試より前
            if (exam.getExamTypeCD().equals(data.getExamTypeCD())
                    && exam.getTargetGrade().equals(data.getTargetGrade())
                    && exam.getDataOpenDate().compareTo(data.getDataOpenDate()) > 0) {

                container.add(data);
            }
        }

        return container;
    }

    /**
     * 指定された年度の最新模試を取得する
     * @param examYear
     * @return
     */
    public static ExamData getLatestExamData(ExamSession examSession, String examYear) {
        // 模試データがある場合
        if (examSession.getYears() != null && examSession.getYears().length > 0) {
            // なければ最新の年度
            if (examYear == null || "".equals(examYear)) examYear = examSession.getYears()[0];
            // 新しい入れ物
            List container = new ArrayList();
            // 全て入れる
            container.addAll(examSession.getExamList(examYear));
            // データ開放日の降順でソート
            Collections.sort(container);

            return (ExamData) container.get(0);

        // 模試データがない場合
        } else {
            return null;
        }
    }

    /**
     * 最新模試を取得する
     * @param examYear
     * @return
     */
    public static ExamData getLatestExamData(ExamSession examSession) {
        return KNUtil.getLatestExamData(examSession, null);
    }

    /**
     * 一括コードから県コードに変換する
     * @param bundleCD
     * @return
     */
    public static String bundleCD2PrefCD(String bundleCD) {
        if (bundleCD == null) throw new IllegalArgumentException("一括コードがNULLです。");
        return bundleCD.substring(0, 2);
    }

    /**
     * 科目変換テーブルを構築する
     * @param con DBコネクション
     * @param examYear 模試年度
     * @param examCD 模試コード
     */
    public static void buildSubCDTransTable(
            final Connection con,
            final ExamData[] exam) throws SQLException {

        // 年度をまたいで重複している模試を取り除く
        List examList = new LinkedList();
        for (int i=0; i<exam.length; i++) {

            ExamData e = exam[i];
            for (int j=0; j<exam.length; j++) {
                if (exam[i].getExamCD().equals(exam[j].getExamCD())
                    && exam[i].getExamYear().compareTo(exam[j].getExamYear()) < 0) {
                        e = null;
                }
            }
            if (e != null && !examList.contains(e)) examList.add(e);
        }

        // 模試コード変換
        buildExamCdTransTable(con, (ExamData[]) examList.toArray(new ExamData[0]));

        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        try {
            // 対象模試の全科目コードを取得する
            StringBuffer query = new StringBuffer();
            query.append("SELECT es.subcd FROM examsubject es ");
            query.append("WHERE es.examyear =? AND es.examcd = ?");
            ps1 = con.prepareStatement(query.toString());

            // 科目変換データ
            ps2 = con.prepareStatement(QueryLoader.getInstance().load("sheet01").toString());

            Iterator ite = examList.iterator();
            while (ite.hasNext()) {
                ExamData e = (ExamData) ite.next();

                ps1.setString(1, e.getExamYear()); // 模試年度
                ps1.setString(2, e.getExamCD()); // 模試コード

                ps2.setString(1, e.getExamCD()); // 模試コード
                ps2.setString(3, e.getExamCD()); // 模試コード
                ps2.setString(4, e.getExamYear()); // 模試年度

                rs = ps1.executeQuery();
                while (rs.next()) {
                    ps2.setString(2, rs.getString(1)); // 科目コード
                    ps2.setString(5, rs.getString(1)); // 科目コード
                    ps2.execute();
                }
            }

        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(ps1);
            DbUtils.closeQuietly(ps2);
        }
    }

    /**
     * 模試コード変換テーブルのセットアップ
     *
     * @param con DBコネクション
     * @param exams 対象模試配列
     * @throws SQLException SQL例外
     */
    public static void buildExamCdTransTable(final Connection con,
            final ExamData[] exams) throws SQLException {

        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(QueryLoader.getInstance().getQuery("sheet06"));
            for (int i = 0; i < exams.length; i++) {
                ps.setString(1, exams[i].getExamCD());
                ps.setString(2, exams[i].getExamYear());
                ps.setString(3, exams[i].getExamCD());
                ps.executeUpdate();
            }
        } finally {
            DbUtils.closeQuietly(ps);
        }
    }

    /**
     * 新テストかどうか
     *
     * @param exam
     * @return
     */
    public static boolean isNewExam(ExamData exam) {
        if (exam == null) return false;
        return "31".equals(exam.getExamTypeCD());
    }

    /**
     * @param exam 模試データ
     * @return 模試がセンターリサーチであるかどうか
     */
    public static boolean isCenterResearch(final ExamData exam) {
        if (exam == null) {
            return false;
        } else {
            return CD_CENTER.equals(exam.getExamCD());
        }
    }

    /**
     * @param exam 模試データ
     * @return 受験学力測定テストであるか
     */
    public static boolean isAbilityExam(final ExamData exam) {

        if (exam == null) {
            return false;
        }

        return TYPECD_ABILITY.equals(exam.getExamTypeCD());
    }

    /**
     * @param exam
     * @return マーク系試験かどうか
     */
    public static boolean isMarkExam(final ExamData exam) {

        if (exam == null) {
            return false;
        }

        return TYPECD_MARK.equals(exam.getExamTypeCD());
    }

    /**
     * @param exam
     * @return 記述系試験かどうか
     */
    public static boolean isWrtnExam(final ExamData exam) {

        if (exam == null) {
            return false;
        }

        return TYPECD_WRTN.equals(exam.getExamTypeCD());
    }

// 2019/11/08 QQ)Tanioka 共通テスト対応 ADD START
     /**
      * @param exam
      * @return 記述系試験かどうか(設問別成績、学力要素別成績データ用)
      */
     public static boolean isWrtnExam2(final ExamData exam) {

         if (exam == null) {
             return false;
         }

         if(TYPECD_WRTN.equals(exam.getExamTypeCD()) || TYPECD_UNIV12.equals(exam.getExamTypeCD())) {
                 return true;
         }

         return false;
     }
// 2019/11/08 QQ)Tanioka 共通テスト対応 ADD END

    /**
     * @param exam
     * @return 私大模試かどうか
     */
    public static boolean isPrivateExam(final ExamData exam) {

        if (exam == null) {
            return false;
        }

        return TYPECD_PRIVATE.equals(exam.getExamTypeCD());
    }

    /**
     * @param exam
     * @return 高1・2模試かどうか
     */
    public static boolean isHs12Exam(final ExamData exam) {

        if (exam == null) {
            return false;
        }

        return TYPECD_UNIV12.equals(exam.getExamTypeCD());
    }

    /**
     * @param exam
     * @return 高1・2大学マスタ利用模試かどうか
     */
    public static boolean isUniv12Exam(final ExamData exam) {

        if (exam == null) {
            return false;
        }

        return "1".equals(exam.getMasterDiv());
    }

    /**
     * @param exam
     * @return 全統高2マーク模試かどうか
     */
    public static boolean isH2MarkExam(final ExamData exam) {

        if (exam == null) {
            return false;
        }

        return CD_H2MARK.equals(exam.getExamCD());
    }

    /**
     * @param exam
     * @return 高１プレステージ模試かどうか
     */
    public static boolean isH1PStage(final ExamData exam) {

        if (exam == null) {
            return false;
        }

        return CD_H1PSTAGE.equals(exam.getExamCD());
    }

    /**
     * @param exam
     * @return 高２プレステージ模試かどうか
     */
    public static boolean isH2PStage(final ExamData exam) {

        if (exam == null) {
            return false;
        }

        return CD_H2PSTAGE.equals(exam.getExamCD());
    }

    /**
     * @param exam
     * @return 高２プレステージ模試かどうか
     */
    public static boolean isH3PStage(final ExamData exam) {

        if (exam == null) {
            return false;
        }

        return CD_H3PSTAGE.equals(exam.getExamCD());
    }

    /**
     * @param exam
     * @return 全統高2記述模試かどうか
     */
    public static boolean isH2WrtnExam(final ExamData exam) {

        if (exam == null) {
            return false;
        }

        //2006年度以降
        if (exam.getExamYear().compareTo("2006") >= 0) {
            return CD_H2WRTN_2006.equals(exam.getExamCD());
        }
        else {
            return CD_H2WRTN_OLD.equals(exam.getExamCD());
        }
    }

// 2019/10/03 QQ)Tanioka 共通テスト対応 ADD START
    /**
     * @param exam
     * @return プレ共通テスト(旧センタープレ)系試験かどうか
     */
    public static boolean isCenterP(final ExamData exam) {

        if (exam == null) {
            return false;
        }

        return CD_CENTERP.equals(exam.getExamCD());
    }

    /**
     * @param exam
     * @return プライムステージ(プレステージ)系試験かどうか
     */
    public static boolean isPStage(final ExamData exam) {

        if (exam == null) {
            return false;
        }

        return TYPECD_PSTAGE.equals(exam.getExamTypeCD());
    }
// 2019/10/03 QQ)Tanioka 共通テスト対応 ADD END

    /**
     * @param exam
     * @return パターン I かどうか
     */
    public static boolean isPatternI(final ExamData exam) {

        if (exam == null) {
            return false;
        }

        //高１プレステージの場合は偽を返す
        if (isH1PStage(exam)) {
            return false;
        }

        //高２プレステージの場合は偽を返す
        if (isH2PStage(exam)) {
            return false;
        }

        //高３プレステージの場合は真を返す
        if (isH3PStage(exam)) {
            return true;
        }

        //マーク系 or 記述系 or 私大模試
        if (isMarkExam(exam) || isWrtnExam(exam) || isPrivateExam(exam)) {
            //マーク高2 and 記述高2 ではない
            if (!isH2MarkExam(exam) && !isH2WrtnExam(exam)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param exam
     * @return パターン II かどうか
     */
    public static boolean isPatternII(final ExamData exam) {

        //マーク系 or 記述系
        if (isMarkExam(exam) || isWrtnExam(exam)) {
            return true;
        }

        return false;
    }

    /**
     * @param exam
     * @return パターン III かどうか
     */
    public static boolean isPatternIII(final ExamData exam) {

        //センターリサーチ以外のマーク系
        if (isMarkExam(exam) && !isCenterResearch(exam)) {
            return true;
        }

        return false;
    }

    /**
     * @param exam
     * @return パターン IV かどうか
     */
    public static boolean isPatternIV(final ExamData exam) {

        //センターリサーチ、学力測定以外の全統試験（←校内対策）
        if (!isCenterResearch(exam) && !isAbilityExam(exam) && isZento(exam)) {
            return true;
        }

        return false;
    }

    /**
     * @param exam 試験データ
     * @return 全統模試かどうか
     */
    public static boolean isZento(final ExamData exam) {
        return isZento(exam.getExamCD());
    }

    /**
     * @param examCd 試験コード
     * @return 全統模試かどうか
     */
    public static boolean isZento(final String examCd) {
        return examCd.length() == 2;
    }

    /**
     * アラビア数字をローマ数字（全角）に変換する
     * @param i アラビア全角数字
     * @return ローマ数字
     */
    public static String arabicToRomanZen(final int i) {

        if (i > ROMAN.length || i < 1) {
            throw new IllegalArgumentException("解釈できないローマ数字です。" + i);
        }

        return ROMAN[i - 1];
    }

    /**
     * 偏差値を到達レベルに変換する
     * @param i 到達レベル ÷ 10
     * @return 到達レベル文字列
     */
    public static String deviationToLevel(final double d) {
        return arabicToRomanZen((int) (Math.round(d * 10)));
    }

    /**
     * アラビア数字をセンター到達エリア文字列に変換する
     * @param i アラビア数字
     * @return センター到達エリア文字列
     */
    public static String arabicToCenterReachArea(final int i) {

        if (i > CENTERREACHAREA.length || i < 1) {
            throw new IllegalArgumentException("解釈できないローマ数字です。" + i);
        }

        return CENTERREACHAREA[i - 1];
    }

    /**
     * 偏差値をセンター到達エリアに変換する
     * @param i センター到達エリア ÷ 10
     * @return センター到達エリア文字列
     */
    public static String deviationToCenterReachArea(final double d) {
        return arabicToCenterReachArea((int) (Math.round(d * 10)));
    }

    /**
     * 大学マスタを利用するQueryを書き換える
     *
     * @param query 書き換えるQuery
     * @param exam 対象模試
     * @return 書き換えたQuery
     */
    public static Query rewriteUnivQuery(final Query query, final ExamData exam) {

        // 高1・2大学マスタ利用なら書き換え
        if (isUniv12Exam(exam)) {
            query.replaceAll("univmaster_basic", "h12_univmaster"); // マスタ
            query.replaceAll("PK_UNIVMASTER_BASIC", "PK_H12_UNIVMASTER"); // INDEX
        }

        return query;
    }

    /**
     * （共通項目設定用）志望大学評価別人数の評価区分を取得する
     *
     * @param exam 対象模試
     * @return 評価区分
     */
    public static String getRatingDiv(final ExamData exam) {

        // 高1・2大学マスタ利用なら固定値
        if (isUniv12Exam(exam)) {
            return "2";
        } else {
            return exam.getDockingType();
        }
    }

    /**
     * @return 模試が第1解答科目対応模試であるかどうか
     */
    public static boolean isAns1st(String examcd) {
        if (examcd == null) {
            return false;
        } else {
            return ANS1ST_EXAM.contains(examcd);
        }
    }

    /**
     * @return 第1解答科目対応模試を返す
     */
    public static String[] getAns1st() {
        return (String[])ANS1ST_EXAM.toArray(new String[ANS1ST_EXAM.size()]);
    }

    /**
     * 模試がオープン模試かどうかを判定します。
     *
     * @param exam {@link ExamData}
     * @return 模試がオープン模試ならtrue
     */
    public static boolean isOpenExam(final ExamData exam) {
        return exam != null && OPEN_EXAM_TYPE_SET.contains(exam.getExamTypeCD());
    }

    /**
     * 情報誌用科目の配点文字列を総点とそれ以外に分割します。
     *
     * @param allot 配点文字列
     * @return 分割結果（[0]=総点、[1]=教科別配点）
     */
    public static String[] splitInfoAllot(String allot) {
        String[] result = new String[] { "", "" };
        if (allot.startsWith("総点")) {
            int index = allot.indexOf('／');
            if (index >= 0) {
                result[0] = allot.substring(2, index);
                result[1] = allot.substring(index + 1);
            } else {
                result[0] = allot.substring(2);
            }
        }
        return result;
    }

    /**
     * コンテキストパスを返します。<br>
     * <br>
     * ※クライアント認証なしドメイン対応で追加。<br>
     * クライアント認証なしドメイン（*.oo.kawai-juku.ac.jp）アクセスの際、
     * /keinaviのコンテキストパスが必要だが、Kei-Naviアプリはドメイン直下に配置されるため、
     * request.getContextPath() は空文字列が返る。<br>
     * そのため、クライアント認証なしドメインでのアクセスに限り、
     * 固定のコンテキストパス "/keinavi" を返すようにする。
     *
     * @param request {@link HttpServletRequest}
     * @return コンテキストパス
     */
    public static String getContextPath(HttpServletRequest request) {
        if (request.getServerName().endsWith(".oo.kawai-juku.ac.jp")) {
                return "/keinavi";
        } else {
                return request.getContextPath();
        }
    }

    /**
     * クライアントIPアドレスを返します。<br>
     * HTTPヘッダ「X-Forwarded-For」の値を優先的に利用し、
     * 「X-Forwarded-For」が未設定の場合に限り、{@link HttpServletRequest#getRemoteAddr()} の値を利用します。
     *
     * @param request {@link HttpServletRequest}
     * @return クライアントIPアドレス
     */
    public static String getRemoteAddr(HttpServletRequest request) {
        String xff = request.getHeader("X-Forwarded-For");
        return StringUtils.isEmpty(xff) ? request.getRemoteAddr() : xff;
    }

}

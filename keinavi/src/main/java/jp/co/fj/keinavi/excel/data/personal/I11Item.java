package jp.co.fj.keinavi.excel.data.personal;

import java.util.ArrayList;
/**
 * 個人成績分析−成績分析面談−連続個人成績表 データクラス
 * 作成日: 2004/07/20
 * @author	A.Iwata
 */
public class I11Item {
//	//今回模試コード
//	private String strBaseMshCd = "";
	//今回模試名
	private String strBaseMshmei = "";
	//今回模試実施基準日
	private String strBaseMshDate = "";
	//今回模試対象学年
	private String strBaseGakunen = "";
	//模試教科数
	private int intBaseMshKyokaSr = 0;
	//2004.10.18 Add
	//今回模試短縮名
	private String strMshmei = "";
	//Add End
	//面談シートフラグ
	private int intMenSheetFlg = 0;
	//面談シートセキュリティスタンプ
	private int intMenSheetSecuFlg = 0;
	//教科分析シートフラグ
	private int intKyoSheetFlg = 0;
	//教科分析シートセキュリティスタンプ
	private int intKyoSheetSecuFlg = 0;
	//科目別成績推移グラフフラグ
	private int intSuiiFlg = 0;
	//科目別成績推移グラフセキュリティスタンプ
	private int intSuiiSecuFlg = 0;
	//設問別成績グラフフラグ
	private int intSetsumonFlg = 0;
	//設問別成績グラフセキュリティスタンプ
	private int intSetsumonSecuFlg = 0;
	//連続個人成績表フラグ
	private int intSeisekiFlg = 0;
	//連続個人成績表セキュリティスタンプ
	private int intSeisekiSecuFlg = 0;
	//2004.10.14 Add
	//連続個人成績表出力フォーマット
	private int intSeisekiFormatFlg = 0;
	//Add End
	//志望大学判定一覧フラグ
	private int intHanteiFlg = 0;
	//志望校判定データセキュリティスタンプ
	private int intHanteiSecuFlg = 0;
	//受験大学スケジュールフラグ
	private int intScheduleFlg = 0;
	//受験大学スケジュール出力フォーマットフラグ
	private int intScheduleFormatFlg = 0;
	//受験大学スケジュールセキュリティスタンプ
	private int intScheduleSecuFlg = 0;
	//データリスト
	private ArrayList i11List = new ArrayList();
	//面談シート出力種別フラグ → 新テスト用に追加
	private int intMenSheetShubetsuFlg = 0;
	//教科分析シート出力種別フラグ → 新テスト用に追加
	private int intKyoSheetShubetsuFlg = 0;
	//科目別成績推移グラフ出力種別フラグ → 新テスト用に追加
	private int intSuiiShubetsuFlg = 0;
	//設問別成績推移グラフ出力種別フラグ → 新テスト用に追加
	private int intSetsumonShubetsuFlg = 0;
	//連続個人成績表出力種別フラグ → 新テスト用に追加
	private int intSeisekiShubetsuFlg = 0;
	//志望大学判定出力種別フラグ → 新テスト用に追加
	private int intHanteiShubetsuFlg = 0;
	//受験大学スケジュール出力種別フラグ → 新テスト用に追加
	private int intScheduleShubetsuFlg = 0;

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrBaseMshmei() {
		return this.strBaseMshmei;
	}
	public String getStrBaseMshDate() {
		return this.strBaseMshDate;
	}
	public int getIntMenSheetFlg() {
		return this.intMenSheetFlg;
	}
	public int getIntMenSheetSecuFlg() {
		return this.intMenSheetSecuFlg;
	}
	public int getIntKyoSheetFlg() {
		return this.intKyoSheetFlg;
	}
	public int getIntKyoSheetSecuFlg() {
		return this.intKyoSheetSecuFlg;
	}
	public int getIntSuiiFlg() {
		return this.intSuiiFlg;
	}
	public int getIntSuiiSecuFlg() {
		return this.intSuiiSecuFlg;
	}
	public int getIntSetsumonFlg() {
		return this.intSetsumonFlg;
	}
	public int getIntSetsumonSecuFlg() {
		return this.intSetsumonSecuFlg;
	}
	public int getIntSeisekiFlg() {
		return this.intSeisekiFlg;
	}
	public int getIntSeisekiSecuFlg() {
		return this.intSeisekiSecuFlg;
	}
	public int getIntHanteiFlg() {
		return this.intHanteiFlg;
	}
	public int getIntHanteiSecuFlg() {
		return this.intHanteiSecuFlg;
	}
	public int getIntScheduleFlg() {
		return this.intScheduleFlg;
	}
	public int getIntScheduleSecuFlg() {
		return this.intScheduleSecuFlg;
	}
	public ArrayList getI11List() {
		return this.i11List;
	}
	public String getStrBaseGakunen() {
		return this.strBaseGakunen;
	}
//	public String getStrBaseMshCd() {
//		return this.strBaseMshCd;
//	}
	public int getIntBaseMshKyokaSr() {
		return this.intBaseMshKyokaSr;
	}
	public int getIntScheduleFormatFlg() {
		return this.intScheduleFormatFlg;
	}
	public int getIntSeisekiFormatFlg() {
		return this.intSeisekiFormatFlg;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public int getIntMenSheetShubetsuFlg() {
		return this.intMenSheetShubetsuFlg;
	}
	public int getIntKyoSheetShubetsuFlg() {
		return this.intKyoSheetShubetsuFlg;
	}
	public int getIntSuiiShubetsuFlg() {
		return this.intSuiiShubetsuFlg;
	}
	public int getIntSetsumonShubetsuFlg() {
		return this.intSetsumonShubetsuFlg;
	}
	public int getIntSeisekiShubetsuFlg() {
		return this.intSeisekiShubetsuFlg;
	}
	public int getIntHanteiShubetsuFlg() {
		return this.intHanteiShubetsuFlg;
	}
	public int getIntScheduleShubetsuFlg() {
		return this.intScheduleShubetsuFlg;
	}

	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setStrBaseMshmei(String strBaseMshmei) {
		this.strBaseMshmei = strBaseMshmei;
	}
	public void setStrBaseMshDate(String strBaseMshDate) {
		this.strBaseMshDate = strBaseMshDate;
	}
	public void setIntMenSheetFlg(int intMenSheetFlg) {
		this.intMenSheetFlg = intMenSheetFlg;
	}
	public void setIntMenSheetSecuFlg(int intMenSheetSecuFlg) {
		this.intMenSheetSecuFlg = intMenSheetSecuFlg;
	}
	public void setIntKyoSheetFlg(int intKyoSheetFlg) {
		this.intKyoSheetFlg = intKyoSheetFlg;
	}
	public void setIntKyoSheetSecuFlg(int intKyoSheetSecuFlg) {
		this.intKyoSheetSecuFlg = intKyoSheetSecuFlg;
	}
	public void setIntSuiiFlg(int intSuiiFlg) {
		this.intSuiiFlg = intSuiiFlg;
	}
	public void setIntSuiiSecuFlg(int intSuiiSecuFlg) {
		this.intSuiiSecuFlg = intSuiiSecuFlg;
	}
	public void setIntSetsumonFlg(int intSetsumonFlg) {
		this.intSetsumonFlg = intSetsumonFlg;
	}
	public void setIntSetsumonSecuFlg(int intSetsumonSecuFlg) {
		this.intSetsumonSecuFlg = intSetsumonSecuFlg;
	}
	public void setIntSeisekiFlg(int intSeisekiFlg) {
		this.intSeisekiFlg = intSeisekiFlg;
	}
	public void setIntSeisekiSecuFlg(int intSeisekiSecuFlg) {
		this.intSeisekiSecuFlg = intSeisekiSecuFlg;
	}
	public void setIntHanteiFlg(int intHanteiFlg) {
		this.intHanteiFlg = intHanteiFlg;
	}
	public void setIntHanteiSecuFlg(int intHanteiSecuFlg) {
		this.intHanteiSecuFlg = intHanteiSecuFlg;
	}
	public void setIntScheduleFlg(int intScheduleFlg) {
		this.intScheduleFlg = intScheduleFlg;
	}
	public void setIntScheduleSecuFlg(int intScheduleSecuFlg) {
		this.intScheduleSecuFlg = intScheduleSecuFlg;
	}
	public void setI11List(ArrayList i11List) {
		this.i11List = i11List;
	}
	public void setStrBaseGakunen(String strBaseGakunen) {
		this.strBaseGakunen = strBaseGakunen;
	}
//	public void setStrBaseMshCd(String strBaseMshCd) {
//		this.strBaseMshCd = strBaseMshCd;
//	}
	public void setIntBaseMshKyokaSr(int intBaseMshKyokaSr) {
		this.intBaseMshKyokaSr = intBaseMshKyokaSr;
	}
	public void setIntScheduleFormatFlg(int intScheduleFormatFlg) {
		this.intScheduleFormatFlg = intScheduleFormatFlg;
	}
	public void setIntSeisekiFormatFlg(int intSeisekiFormatFlg) {
		this.intSeisekiFormatFlg = intSeisekiFormatFlg;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setIntMenSheetShubetsuFlg(int intMenSheetShubetsuFlg) {
		this.intMenSheetShubetsuFlg = intMenSheetShubetsuFlg;
	}
	public void setIntKyoSheetShubetsuFlg(int intKyoSheetShubetsuFlg) {
		this.intKyoSheetShubetsuFlg = intKyoSheetShubetsuFlg;
	}
	public void setIntSuiiShubetsuFlg(int intSuiiShubetsuFlg) {
		this.intSuiiShubetsuFlg = intSuiiShubetsuFlg;
	}
	public void setIntSetsumonShubetsuFlg(int intSetsumonShubetsuFlg) {
		this.intSetsumonShubetsuFlg = intSetsumonShubetsuFlg;
	}
	public void setIntSeisekiShubetsuFlg(int intSeisekiShubetsuFlg) {
		this.intSeisekiShubetsuFlg = intSeisekiShubetsuFlg;
	}
	public void setIntHanteiShubetsuFlg(int intHanteiShubetsuFlg) {
		this.intHanteiShubetsuFlg = intHanteiShubetsuFlg;
	}
	public void setIntScheduleShubetsuFlg(int intScheduleShubetsuFlg) {
		this.intScheduleShubetsuFlg = intScheduleShubetsuFlg;
	}

}

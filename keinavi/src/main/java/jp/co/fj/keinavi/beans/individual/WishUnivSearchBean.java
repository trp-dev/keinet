/*
 * 作成日: 2004/09/23
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Iterator;

import jp.co.fj.keinavi.data.individual.WishUnivData;

import com.fjh.beans.DefaultSearchBean;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 *
 * 2009/10/01 	S.Hase  	[2]「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 *                         　　　　「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 */
public class WishUnivSearchBean extends DefaultSearchBean {

	private String query = "" +
		"SELECT " +
		" /*+ INDEX(C PK_CANDIDATERATING) */ "+//12/5 sql hint
		"H.UNIDIV UNIVDIVCD" +
		//",U.STEMMACD" +
		",C.INDIVIDUALID,C.EXAMYEAR,C.EXAMCD,C.CANDIDATERANK,E.EXAMDIV,H.EVENTYEAR EXAMYEAR " +
		",H.UNIVCD,H.FACULTYCD,H.FACULTYCD,H.DEPTCD,P.PREFCD,P.DISTRICTCD " +
		"FROM CANDIDATERATING C INNER JOIN EXAMINATION E " +
		"ON C.EXAMYEAR = E.EXAMYEAR AND C.EXAMCD = E.EXAMCD " +
		"INNER JOIN UNIVMASTER_BASIC H ON C.EXAMYEAR = H.EVENTYEAR AND " +
		"C.UNIVCD = H.UNIVCD AND C.FACULTYCD = H.FACULTYCD AND C.DEPTCD = H.DEPTCD AND " +
		"E.EXAMDIV = H.EXAMDIV INNER JOIN PREFECTURE P ON H.PREFCD_EXAMHO = P.PREFCD " +
		//"INNER JOIN UNIVSTEMMA U ON  C.EXAMYEAR = U.YEAR AND C.UNIVCD = U.UNIVCD " +
		//"AND C.FACULTYCD = U.FACULTYCD AND C.DEPTCD = U.DEPTCD " +
		//"AND U.STEMMADIV = '3' "+//STEMMADIV(3) = 分野
		"WHERE 1=1 ";
		
	private String individualId;
	private String examYear;
	private String examCd;
		
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		if(individualId != null)
			query += "AND C.INDIVIDUALID = '"+individualId+"' "; 
		if(examYear != null)
			query += "AND C.EXAMYEAR = '"+examYear+"' ";
		if(examCd != null)
			query += "AND C.EXAMCD = '"+examCd+"' ";
		
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while(rs.next()){
				WishUnivData wud = new WishUnivData();
				wud.setIndividualId(rs.getString("INDIVIDUALID"));
				wud.setExamYear(rs.getString("EXAMYEAR"));
				wud.setExamCd(rs.getString("EXAMCD"));
				wud.setCandidateRank(rs.getString("CANDIDATERANK"));//志望順位
				wud.setExamDiv(rs.getString("EXAMDIV"));
				wud.setUnivCd(rs.getString("UNIVCD"));
				wud.setFacultyCd(rs.getString("FACULTYCD"));
				wud.setDeptCd(rs.getString("DEPTCD"));
				wud.setPrefCd(rs.getString("PREFCD"));//県コード
				wud.setDistrictCd(rs.getString("DISTRICTCD"));//地区コード
				wud.setUnivDivCd(rs.getString("UNIVDIVCD"));//大学区分コード-公私判別
				recordSet.add(wud);
			}
			//分野コードを付加する
			if(recordSet != null && recordSet.size() > 0){
				Iterator ite = recordSet.iterator();
				while(ite.hasNext()){
					WishUnivData wud = (WishUnivData) ite.next();
					UnivStemmaSearchBean ussb = new UnivStemmaSearchBean();
					ussb.setUnivCd(wud.getUnivCd());
					ussb.setFacultyCd(wud.getFacultyCd());
					ussb.setDeptCd(wud.getDeptCd());
					ussb.setConnection("", conn);
					ussb.execute();
					
					wud.setUnivStemmaList(ussb.getRecordSet());
				}
			}
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
		//ソート			
		if(recordSet != null && recordSet.size() > 0){
			Collections.sort(recordSet);	
		}
	}

	/**
	 * @return
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @param string
	 */
	public void setExamCd(String string) {
		examCd = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

}

package jp.co.fj.keinavi.servlets.com_set;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.PrefBean;
import jp.co.fj.keinavi.beans.com_set.SchoolBean;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM601Form;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM601Servlet extends AbstractCMServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 共通処理
		super.execute(request, response);

		// 共通アクションフォーム - session scope
		CM001Form scform = super.getCommonForm(request);
		// 個別アクションフォーム - session scope
		CM601Form siform = (CM601Form)scform.getActionForm("CM601Form");

		// JSP
		if ("cm601".equals(getForward(request))) {
			// DBコネクション
			Connection con = null;
			try {
				con = super.getConnectionPool(request);

				// 都道府県Bean
				PrefBean pref = PrefBean.getInstance(con);
				request.setAttribute("PrefBean", pref);

				// 高校Bean
				SchoolBean sb = new SchoolBean();
				sb.setConnection(null, con);
				sb.setSchoolCodeArray(siform.getSchool());
				sb.setTargetYear(scform.getTargetYear());
				sb.setTargetExam(scform.getTargetExam());
				sb.execute();
				request.setAttribute("SchoolBean", sb);
			} catch (Exception e) {
				throw new ServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}

			request.setAttribute("HiddenCodeMap", hiddenCodeMap);
			super.forward(request, response, JSP_CM601);
		// 不明なら転送
		} else {
			// アクションフォームを取得
			CM001Form rcform = null;
			CM601Form riform = null;
			try {
				rcform = (CM001Form)super.getActionForm(request,
					"jp.co.fj.keinavi.forms.com_set.CM001Form");
				riform = (CM601Form)super.getActionForm(request,
					"jp.co.fj.keinavi.forms.com_set.CM601Form");
			} catch (Exception e) {
				throw new ServletException(e);
			}
			// フォームの値をセッションに保持する
			siform.setSchool(riform.getSchool());
			siform.setGraph(riform.getGraph());
			// 変更フラグ
			scform.setChanged(rcform.getChanged());

			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

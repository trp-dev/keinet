package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
//import java.util.ArrayList;
/**
 * �l���ѕ��́|�󌱗p�����|�u�]�Z���� �u�]�Z�]�����X�g
 * �쐬��: 2004/09/09
 * @author	C.Murata
 */
public class I14HyoukaListBean {
	//�u�]����
	private int intShiboRank = 0;
	//�s���{��
	private String strKenmei = "";
	//��w�敪
	private String strDaigakuKbn = "";
	//��w��
	private String strDaigakuMei = "";
	//�w��
	private String strGakubuMei = "";
	//�w��
	private String strGakkaMei = "";
//	//�����p�^�[���t���O
//	private int intPluralFlg = 0;
	//�]�����_
	private int intHyoukaTokuten = 0;
	//�Z���^�[�]��
	private String strHyoukaCenter = "";
	//�]���΍��l
	private float floHyoukaHensa = 0;
	//�Q���]��
	private String strHyoukaKijyutsu = "";
	//�]���|�C���g
	private int intHyoukaPoint = 0;
	//�����]��
	private String strHyoukaAll = "";
	//�{�����ݒn
	private String strHonbuShozaichi = "";
	//�L�����p�X���ݒn
	private String strKoshaShozaichi = "";
	
	/**
	 * @GET
	 */
	public float getFloHyoukaHensa() {
		return this.floHyoukaHensa;
	}
	public int getIntHyoukaPoint() {
		return this.intHyoukaPoint;
	}
	public int getIntHyoukaTokuten() {
		return this.intHyoukaTokuten;
	}
//	public int getIntPluralFlg() {
//		return this.intPluralFlg;
//	}
	public String getStrDaigakuKbn() {
		return this.strDaigakuKbn;
	}
	public String getStrDaigakuMei() {
		return this.strDaigakuMei;
	}
	public String getStrGakkaMei() {
		return this.strGakkaMei;
	}
	public String getStrGakubuMei() {
		return this.strGakubuMei;
	}
	public String getStrHyoukaAll() {
		return this.strHyoukaAll;
	}
	public String getStrHyoukaCenter() {
		return this.strHyoukaCenter;
	}
	public String getStrHyoukaKijyutsu() {
		return this.strHyoukaKijyutsu;
	}
	public String getStrKenmei() {
		return this.strKenmei;
	}
	public int getIntShiboRank() {
		return this.intShiboRank;
	}
	public String getStrHonbuShozaichi() {
		return this.strHonbuShozaichi;
	}
	public String getStrKoshaShozaichi() {
		return this.strKoshaShozaichi;
	}


	/**
	 * @SET
	 */
	public void setFloHyoukaHensa(float floHyoukaHensa) {
		this.floHyoukaHensa = floHyoukaHensa;
	}
	public void setIntHyoukaPoint(int intHyoukaPoint) {
		this.intHyoukaPoint = intHyoukaPoint;
	}
	public void setIntHyoukaTokuten(int intHyoukaTokuten) {
		this.intHyoukaTokuten = intHyoukaTokuten;
	}
//	public void setIntPluralFlg(int intPluralFlg) {
//		this.intPluralFlg = intPluralFlg;
//	}
	public void setStrDaigakuKbn(String strDaigakuKbn) {
		this.strDaigakuKbn = strDaigakuKbn;
	}
	public void setStrDaigakuMei(String strDaigakuMei) {
		this.strDaigakuMei = strDaigakuMei;
	}
	public void setStrGakkaMei(String strGakkaMei) {
		this.strGakkaMei = strGakkaMei;
	}
	public void setStrGakubuMei(String strGakubuMei) {
		this.strGakubuMei = strGakubuMei;
	}
	public void setStrHyoukaAll(String strHyoukaAll) {
		this.strHyoukaAll = strHyoukaAll;
	}
	public void setStrHyoukaCenter(String strHyoukaCenter) {
		this.strHyoukaCenter = strHyoukaCenter;
	}
	public void setStrHyoukaKijyutsu(String strHyoukaKijyutsu) {
		this.strHyoukaKijyutsu = strHyoukaKijyutsu;
	}
	public void setStrKenmei(String strKenmei) {
		this.strKenmei = strKenmei;
	}
	public void setStrShiboRank(int intShiboRank) {
		this.intShiboRank = intShiboRank;
	}
	public void setStrHonbuShozaichi(String strHonbuShozaichi) {
		this.strHonbuShozaichi = strHonbuShozaichi;
	}
	public void setStrKoshaShozaichi(String strKoshaShozaichi) {
		this.strKoshaShozaichi = strKoshaShozaichi;
	}

}

package jp.co.fj.keinavi.forms.com_set;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM601Form extends BaseForm {

	// 比較対象高校
	private String[] school = null; 
	// グラフ表示対象
	private String[] graph = null;


	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * @return
	 */
	public String[] getGraph() {
		return graph;
	}

	/**
	 * @return
	 */
	public String[] getSchool() {
		return school;
	}

	/**
	 * @param strings
	 */
	public void setGraph(String[] strings) {
		graph = strings;
	}

	/**
	 * @param strings
	 */
	public void setSchool(String[] strings) {
		school = strings;
	}

}

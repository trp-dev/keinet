package jp.co.fj.keinavi.beans.maintenance;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 作業中宣言Bean
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class DeclarationBean extends DefaultBean implements Serializable {

	/** セッションキー */
	public static final String SESSION_KEY = "DeclarationBean";	
	
	// 学校コード
	private final String schoolCd;
	
	// 作業中の利用者ID
	private String loginId;
	// 作業中の利用者名
	private String loginName;
	// 作業中宣言日時
	private Timestamp time;
	
	/**
	 * コンストラクタ
	 */
	public DeclarationBean(final String schoolCd) {
		
		if (schoolCd == null) {
			throw new IllegalArgumentException("学校コードがNULLです。");
		}
		
		this.schoolCd = schoolCd;
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("m09").toString());
			ps.setString(1, schoolCd);
			rs = ps.executeQuery();
			if (rs.next()) {
				loginId = rs.getString(1);
				loginName = rs.getString(2);
				time = rs.getTimestamp(3);
			}
			
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * @return loginId を戻します。
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * @return loginName を戻します。
	 */
	public String getLoginName() {
		return loginName;
	}

	/**
	 * @return time を戻します。
	 */
	public Timestamp getTime() {
		return time;
	}

}

package jp.co.fj.keinavi.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import jp.co.fj.freemenu.util.DownloadUtil;

/**
 *
 * ファイル操作ユーティリティ
 * 
 * <2010年度マーク高2模試対応>
 * 2011.01.12	Tomohisa YAMADA
 * 				新規作成
 * 
 */
public class FileUtil {

	/**
	 * ファイルパスまたはファイル名から拡張子だけを抽出する。
	 * @param file 対象ファイルパス/対象ファイル名
	 * @return
	 */
	public static String extractExtension(String file) {
		
	    int point = file.lastIndexOf(".");
	    
	    if (point != -1) {
	        return file.substring(point + 1);
	    }
	    
	    return file;
	}
	
	/**
	 * ファイル名から拡張子を取り除いた名前を抽出する。
	 * @param file 対象ファイル名
	 * @return
	 */
	public static String dropExtension(String file) {

	    int point = file.lastIndexOf(".");
	    
	    if (point != -1) {
	        return file.substring(0, point);
	    } 
	    
	    return file;
	}
	
	/**
	 * ファイルパスからファイル名のみを抽出する。
	 * @param file 対象ファイルパス
	 * @return
	 */
	public static String extractFileName(String fileFullPath) {
		return new File(fileFullPath).getName();
	}
	
	/**
	 * ファイルの圧縮（zip形式）をする。
	 * @param zippedFilePath 名前を含む圧縮先ファイルのフルパス
	 * @param 圧縮対象のファイル群
	 * @throws Exception 
	 */
	public static synchronized File zip(
			String zippedFilePath, File[] files) throws Exception {
		File zippedFile = new File(zippedFilePath);
		
		ZipOutputStream os = null;
		InputStream is =  null;
		
		try {
			os = new ZipOutputStream(new FileOutputStream(zippedFile.getPath()));
			byte[] buf = new byte[1024];
			
			// ファイルリスト分処理
			for (int i = 0; i < files.length; i++) {
				os.putNextEntry(new ZipEntry(files[i].getName()));
				is = new FileInputStream(files[i].getPath());
				
				int len = 0;
				
				// 書き込み開始
				while ((len = is.read(buf)) != -1) {
					os.write(buf, 0, len);
				}
				os.closeEntry();
			}
			
			return zippedFile;
			
		} finally {
			if(is != null) 
					os.close();
			if (os != null) 
					os.close();
		}
	}
	
	/**
	 * ファイルアップロードを実行する。
	 * @param bs アップロードファイル
	 * @param serverDirectoryPath アップロード先ディレクトリ
	 * @param newFileName アップロード先ファイル名（リネーム）
	 * @throws FileNotFoundException エラー
	 * @throws IOException エラー
	 */
	public static synchronized File upload(
			byte[] bs, 
			String serverDirectoryPath, 			
			String newFileName) throws FileNotFoundException, IOException {
		File file = null;
		FileOutputStream out = null;
		try {
			File directory = new File(serverDirectoryPath);
			directory.mkdirs();
			file = new File(directory.getPath() + File.separator + newFileName);
			out = new FileOutputStream(file);
			//空ファイルの場合は中身は不要
			if (bs.length > 0) {
				out.write(bs);
			}
			return file;
		} finally {
			if (out != null) out.close();
		}
	}
	
	/**
	 * ファイルダウンロードを実行する。
	 * @param response
	 * @param fileFullPath ダウンロード対象ファイルのフルパス
	 * @param fileName ダウンロード時のファイル名称（リネーム）
	 * @return
	 */
	public static void download(
			HttpServletResponse response, String fileFullPath, String fileName) throws Exception {
		
		int result = DownloadUtil.downFile(response, fileFullPath, fileName);
		
		if (result != 0) {
			throw new IOException();
		}
	}
}

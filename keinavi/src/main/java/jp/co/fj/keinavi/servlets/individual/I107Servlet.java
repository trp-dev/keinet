/*
 * 作成日: 2004/07/29
 *
 * 成績分析／科目別成績推移グラフ表示用サーブレット
 *
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.graph.LineGraphApplet2;
import jp.co.fj.keinavi.beans.help.OnepointBean;
import jp.co.fj.keinavi.beans.individual.I107Bean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.PrevSuccessData;
import jp.co.fj.keinavi.data.individual.SubjectData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I107Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * @author Totec) K.Kondo
 *
 * バランスチャート(合格者平均)を表示する。
 * プロファイルからデータ取得するが、I107で再表示のときはフォームからデータ取得
 *
 * 2005.3.1 	Totec) K.Kondo 		[1]セッションからhelpdeskフラグを取得し、Beanに渡すように修正。
 * 2005.3.1 	Totec) K.Kondo 		[2]プロファイルより、クラスの現年度を取得する
 * 2005.8.23 	Totec) T.Yamada 	[3]印刷での対象模試がセンターリサーチの時のチェックボックス無効時の対応
 * 2016.1.15	QQ) K.Hisakawa		サーブレット化対応
 *
 */
public class I107Servlet extends IndividualServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		final HttpServletRequest request,
		final HttpServletResponse response)
		throws ServletException, IOException{

		super.execute(request, response);

		//フォームデータを取得
		final I107Form i107Form = (I107Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.individual.I107Form");

		//プロファイル情報
		final Profile profile = getProfile(request);
		Short studentDispGrade = (Short)profile.getItemMap(IProfileCategory.I_SCORE_COURSE_TRANS).get(IProfileItem.STUDENT_DISP_GRADE);
		String devRanges = (String)profile.getItemMap(IProfileCategory.I_SCORE_COURSE_TRANS).get(IProfileItem.DEV_RANGE);
		Short printStamp = (Short) profile.getItemMap(IProfileCategory.I_SCORE_COURSE_TRANS).get(IProfileItem.PRINT_STAMP);
		Short printStudent = (Short) profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);

		// ログイン情報 //[1] add
		final LoginSession login = getLoginSession(request);
		//個人共通
		final ICommonMap iCommonMap = getICommonMap(request);

		// 2016/01/15 QQ)Hisakawa 大規模改修 ADD START
        Collection cPrevSuccessDatas = null;
        Collection cSubDatas = null;
        int iTargetExamNum = 0;
        String sLineItemNameS = null;
        String sExamDeviations = null;
        String sExamDeviations2 = null;
        String sExamDeviations3 = null;
        int iExaminationDataSize = 0;
        String sExamNames = null;
        // 2016/01/15 QQ)Hisakawa 大規模改修 ADD END

		// 転送元がJSPなら対象模試情報を初期化
		if ("i107".equals(getBackward(request))) {
			initTargetExam(request);
		}

		// JSP転送
		if ("i107".equals(getForward(request))) {
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);

				//ワンポイントアドバイス
				OnepointBean onebean = new OnepointBean();
				onebean.setConnection(null, con);
				onebean.setScreenID("i107");
				onebean.execute();
				request.setAttribute("OnepointBean", onebean);

				I107Bean i107Bean = new I107Bean();
				i107Bean.setHelpflg(login.isHelpDesk());//[1] add
				i107Bean.setClassYear(ProfileUtil.getChargeClassYear(profile));//[2] add

				if(!"i107".equals(getBackward(request))
						|| !iCommonMap.getTargetPersonId().equals(i107Form.getTargetPersonId())
						|| isChangedMode(request) || isChangedExam(request)){


					if ("i107".equals(getBackward(request))) {

						// 生徒を切り替えたとき
						// モードを切り替えたとき
						// 対象模試を変えたとき
						if (i107Form.getTargetSubCode().equals("")) {
							/**前回表示した生徒の科目コードが空なので、科目を引き継がない**/
							i107Bean.setFirstAccess(true);
							//検索条件の設定
							i107Bean.setSearchCondition(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(),
														i107Form.getTargetPersonId(), studentDispGrade.toString());
							//検索実行
							i107Bean.setConnection("", con);
							i107Bean.execute();

							i107Form.setTargetSubCode(i107Bean.getTargetSubCd());	//対象科目コード
							i107Form.setTargetSubName(i107Bean.getTargetSubNm());	//対象科目名

						} else {
							/**前回表示した生徒の科目コードがあるので、それを引き継ぐ**/
							String[] ranks = new String[3];
							ranks[0] ="1";
							ranks[1] ="2";
							ranks[2] ="3";

							i107Bean.setFirstAccess(false);
							i107Bean.setSearchCondition(iCommonMap.getTargetExamYear(),	iCommonMap.getTargetExamCode(),
														i107Form.getTargetPersonId(), i107Form.getTargetGrade(), ranks);
							i107Bean.setTargetSubCd(GeneralUtil.toBlank(i107Form.getTargetSubCode()));
							i107Bean.setTargetSubNm(GeneralUtil.toBlank(i107Form.getTargetSubName()));

							//検索実行
							i107Bean.setConnection("", con);
							i107Bean.execute();
						}

						//コンボ初期選択：設定
						i107Form.setTargetCandidateRank1("1");	//第一志望
						i107Form.setTargetCandidateRank2("2");	//第二志望
						i107Form.setTargetCandidateRank3("3");	//第三志望

						//1.印刷対象生徒をフォームにセットしなおす
						String[] printTarget = {"0","0","0","0"};
						if(i107Form.getPrintTarget() != null){
							for(int i=0; i<i107Form.getPrintTarget().length; i++){
								printTarget[Integer.parseInt(i107Form.getPrintTarget()[i])] = "1";
							}
						}
						//[3] add start
						final String CENTER_EXAMCD = "38";
						if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
							printTarget[3] = "1";
						}
						//[3] add end
						i107Form.setPrintTarget(printTarget);

					// 初回アクセス
					} else {

						i107Bean.setFirstAccess(true);
						//検索条件の設定
						i107Bean.setSearchCondition(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(),
													i107Form.getTargetPersonId(), studentDispGrade.toString());
						//検索実行
						i107Bean.setConnection("", con);
						i107Bean.execute();

						//コンボ初期選択：設定
						i107Form.setTargetCandidateRank1("1");	//第一志望
						i107Form.setTargetCandidateRank2("2");	//第二志望
						i107Form.setTargetCandidateRank3("3");	//第三志望
						i107Form.setTargetSubCode(GeneralUtil.toBlank(i107Bean.getTargetSubCd()));	//対象科目コード
						i107Form.setTargetSubName(GeneralUtil.toBlank(i107Bean.getTargetSubNm()));	//対象科目名

						//個人成績分析共通-印刷対象生保存をセット
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
						//i107Form.setPrintStudent(printStudent.toString());
						i107Form.setPrintStudent(iCommonMap.getPrintStudent());
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END

						//1.印刷対象生徒をセット
						String printTarget = (String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET);
						i107Form.setPrintTarget(CollectionUtil.splitComma(printTarget));

						//2.表示する学年をセット
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
						//i107Form.setTargetGrade(studentDispGrade.toString());
						i107Form.setTargetGrade(iCommonMap.getTargetGrade());
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END

						//3.偏差値範囲をセット
						i107Form.setLowerRange(CollectionUtil.deconcatComma(devRanges)[0]);
						i107Form.setUpperRange(CollectionUtil.deconcatComma(devRanges)[1]);

						//4.セキュリティスタンプを保存
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
						//i107Form.setPrintStamp(printStamp.toString());
						i107Form.setPrintStamp(iCommonMap.getPrintStamp());
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
					}

				// 再表示アクセス
				} else {

					//志望順位
					String[] ranks = new String[3];
					if(i107Form.getTargetCandidateRank1() == null){
						ranks[0] ="1";
					}else{
						ranks[0] =i107Form.getTargetCandidateRank1();
					}

					if(i107Form.getTargetCandidateRank2() == null){
						ranks[1] ="2";
					}else{
						ranks[1] =i107Form.getTargetCandidateRank2();
					}

					if(i107Form.getTargetCandidateRank3() == null){
						ranks[2] ="3";
					}else{
						ranks[2] =i107Form.getTargetCandidateRank3();
					}

					i107Bean.setFirstAccess(false);
					i107Bean.setTargetSubCd(GeneralUtil.toBlank(i107Form.getTargetSubCode()));
					i107Bean.setTargetSubNm(GeneralUtil.toBlank(i107Form.getTargetSubName()));
					i107Bean.setSearchCondition(iCommonMap.getTargetExamYear(),	iCommonMap.getTargetExamCode(),
												i107Form.getTargetPersonId(), i107Form.getTargetGrade(), ranks);

					//1.印刷対象生徒をフォームにセットしなおす
					String[] printTarget = {"0","0","0","0"};
					if(i107Form.getPrintTarget() != null){
						for(int i=0; i<i107Form.getPrintTarget().length; i++){
							printTarget[Integer.parseInt(i107Form.getPrintTarget()[i])] = "1";
						}
					}
					//[3] add start
					final String CENTER_EXAMCD = "38";
					if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
						printTarget[3] = "1";
					}
					//[3] add ends
					i107Form.setPrintTarget(printTarget);

					//2.表示する学年を保存
					profile.getItemMap(IProfileCategory.I_SCORE_COURSE_TRANS).put(IProfileItem.STUDENT_DISP_GRADE, new Short(i107Form.getTargetGrade()));

					//3.偏差値範囲を保存
					String devRangeUL = CollectionUtil.deSplitComma(new String[]{i107Form.getLowerRange(),i107Form.getUpperRange()});
					profile.getItemMap(IProfileCategory.I_SCORE_COURSE_TRANS).put(IProfileItem.DEV_RANGE, devRangeUL);

					//検索実行
					i107Bean.setConnection("", con);
					i107Bean.execute();

					if(i107Form.getTargetSubCode() == null || i107Form.getTargetSubCode().equals("")) {
						i107Form.setTargetSubCode(i107Bean.getTargetSubCd());
					}
				}

				request.setAttribute("i107Bean", i107Bean);

                // 2016/01/15 QQ)Hisakawa 大規模改修 ADD START
				cPrevSuccessDatas = i107Bean.getPrevSuccessDatas();
				cSubDatas = i107Bean.getSubDatas();
				iTargetExamNum = i107Bean.getTargetExamNum();
				sLineItemNameS = i107Bean.getLineItemNameS();
				sExamDeviations = i107Bean.getExamDeviations();
				sExamDeviations2 = i107Bean.getExamDeviations2();
				sExamDeviations3 = i107Bean.getExamDeviations3();
				iExaminationDataSize = i107Bean.getExaminationDataSize();
				sExamNames = i107Bean.getExamNames();
                // 2016/01/15 QQ)Hisakawa 大規模改修 ADD END

			} catch(final Exception e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			request.setAttribute("form", i107Form);
			iCommonMap.setTargetPersonId(i107Form.getTargetPersonId());

            // 2016/01/15 QQ)Hisakawa 大規模改修 ADD START
			//大学
			String univName = "";
			String univDev = "";
			String dispSelect = "";	//グラフ表示／非表示
			int univCount = 0;

			for(java.util.Iterator it=cPrevSuccessDatas.iterator(); it.hasNext();) {
				PrevSuccessData pdata = (PrevSuccessData)it.next();
				if(univCount != 0) {
					univName += ",";
					univDev += ",";
				} else {
					univCount++;
				}
				univName += pdata.getUnivName();
				if(pdata.getSucAvgDev().equals("")) {
					univDev += "-999.0";
				} else {
					univDev += pdata.getSucAvgDev();
				}
			}

			java.util.Iterator cd = cSubDatas.iterator();
			boolean flag = false;
			while(cd.hasNext()) {
			    SubjectData data = (SubjectData)cd.next();
			    String temp = data.getSubjectCd();
			    if(temp.equals(i107Form.getTargetSubCode())) {
			        flag = true;
			        break;
			    }
			}

			if(flag==true){
			    dispSelect = "on";
			}else{
			    dispSelect = "off";
			}

			if(iExaminationDataSize <= 0){
			    dispSelect = "off";
            }

            LineGraphApplet2 App = new LineGraphApplet2();
            App.setParameter("examName", sExamNames);
            App.setParameter("dispSelect", dispSelect);
            App.setParameter("yTitle", "偏差値");
            App.setParameter("HensaZoneLow", String.valueOf((int)Double.parseDouble(i107Form.getLowerRange())));
            App.setParameter("HensaZoneHigh", String.valueOf((int)Double.parseDouble(i107Form.getUpperRange())));
            App.setParameter("BarItemNum", String.valueOf(cPrevSuccessDatas.size()));
            App.setParameter("LineItemNum", String.valueOf(iTargetExamNum));
            App.setParameter("LineItemName", univName);
            App.setParameter("LineItemNameS", sLineItemNameS);

            App.setParameter("examValue0", sExamDeviations);
            App.setParameter("examValue1", sExamDeviations2);
            App.setParameter("examValue2", sExamDeviations3);

            App.setParameter("univValue", univDev);
            App.setParameter("itemTitle", "□対象生徒偏差値,□合格者平均偏差値");
            App.setParameter("colorDAT", "1,0,2,15,4,5,13,14");
            App.setParameter("BarSize", "15");

            App.init();

            HttpSession session = request.getSession(true);

            session.setAttribute("GraphServlet", App);
            session.setAttribute("GraphType", "LineGraph2");

            App = null;
            // 2016/01/15 QQ)Hisakawa 大規模改修 ADD END

			super.forward(request, response, JSP_I107);

		// 不明ならServlet転送
		} else {
			//Forwordがsheetならプロファイルを保存する
			if ("sheet".equals(getForward(request))) {
				/** プロファイルデータをフォームにセットする */
				//分析モードなら個人成績分析共通-印刷対象生保存を保存
				if(iCommonMap.isBunsekiMode()){
					profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(i107Form.getPrintStudent()));
				}

				//1.印刷対象生徒を保存
				String[] printTarget = {"0","0","0","0"};
				if(i107Form.getPrintTarget() != null){
					for(int i=0; i<i107Form.getPrintTarget().length; i++){
						printTarget[Integer.parseInt(i107Form.getPrintTarget()[i])] = "1";
					}
				}
				//[3] add start
				final String CENTER_EXAMCD = "38";
				if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
					printTarget[3] = "1";
				}
				//[3] add end
				profile.getItemMap(IProfileCategory.I_SCORE_COMMON).put(IProfileItem.PRINT_TARGET, CollectionUtil.deSplitComma(printTarget));

				//2.表示する学年を保存
				profile.getItemMap(IProfileCategory.I_SCORE_COURSE_TRANS).put(IProfileItem.STUDENT_DISP_GRADE, new Short(i107Form.getTargetGrade()));

				//3.偏差値範囲を保存
				String devRangeUL = CollectionUtil.deSplitComma(new String[]{i107Form.getLowerRange(),i107Form.getUpperRange()});
				profile.getItemMap(IProfileCategory.I_SCORE_COURSE_TRANS).put(IProfileItem.DEV_RANGE, devRangeUL);

				//4.セキュリティスタンプを保存
				profile.getItemMap(IProfileCategory.I_SCORE_COURSE_TRANS).put(IProfileItem.PRINT_STAMP, new Short(i107Form.getPrintStamp()));
			} else {
				/** プロファイルデータをフォームにセットする */
				//分析モードなら個人成績分析共通-印刷対象生保存を保存
				if(iCommonMap.isBunsekiMode()){
					profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(i107Form.getPrintStudent()));
				}

				//1.印刷対象生徒を保存
				String[] printTarget = {"0","0","0","0"};
				if(i107Form.getPrintTarget() != null){
					for(int i=0; i<i107Form.getPrintTarget().length; i++){
						printTarget[Integer.parseInt(i107Form.getPrintTarget()[i])] = "1";
					}
				}
				//[3] add start
				final String CENTER_EXAMCD = "38";
				if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
					printTarget[3] = "1";
				}
				//[3] add end
				profile.getItemMap(IProfileCategory.I_SCORE_COMMON).put(IProfileItem.PRINT_TARGET, CollectionUtil.deSplitComma(printTarget));

			}

			iCommonMap.setTargetPersonId(i107Form.getTargetPersonId());
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD START
                        iCommonMap.setTargetGrade(i107Form.getTargetGrade());
                        iCommonMap.setPrintStamp(i107Form.getPrintStamp());
                        iCommonMap.setPrintStudent(i107Form.getPrintStudent());
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD END
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

package jp.co.fj.keinavi.beans.individual;

import java.util.List;

import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.keinavi.beans.individual.judgement.SearchUnivCompleteBean;

import org.apache.log4j.Logger;

import com.fjh.beans.DefaultBean;

/**
 *
 * 大学指定判定クラス
 *
 * 2004.09.23	Tomohisa YAMADA - Totec
 * 				新規作成
 *
 * 2005.04.08 	Tomohisa YAMADA - Totec
 * 				ScoreBeanの[35]に伴う修正
 *
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 *              センターリサーチ５段階評価対応
 *
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 *
 *
 */
public class JudgeByUnivBean extends DefaultBean{

	/**
     *
     */
    private static final long serialVersionUID = 8070336350760472081L;
    //IN PARAMETER
	private List univAllList;
	private String[][] univArray;
	//private boolean isUseNewLogic;//[1] del
	private Score scoreBean;

	// OUT PARAMETER
	private List recordSet;

	// 対象模試がセンタープレの場合、
	// 一般私大、短大の2次判定を＃３記述ではなく
	// センタープレの成績を優先利用するかどうかのフラグ
	// インターネット模試判定において「優先しない」とするため
	// デフォルト値はfalseとする
	private boolean precedeCenterPre = false;

	    private static final Logger logger = Logger.getLogger("judgement");

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// 選択した大学にマッチした大学のリストを取得
		final SearchUnivCompleteBean searchCompBean = new SearchUnivCompleteBean();
		searchCompBean.setSearchCodeArray(univArray);
		searchCompBean.setCompairData(univAllList);
		searchCompBean.execute();

		// ステップ2 : 判定の実行
		final JudgementManager judge = new JudgementManager(
				scoreBean, precedeCenterPre);
		judge.setUnivList(searchCompBean.getResultArrayList());
	        long st = System.currentTimeMillis();
		judge.execute();
	        long ed = System.currentTimeMillis();
	        if (logger.getLevel() != null) {
	            logger.info(String.format("%d,%d", searchCompBean.getResultArrayList().size(), ed - st));
	        }
		recordSet = judge.getResults();
	}

	/**
	 * センタープレでの成績を差し替えを有効化
	 */
	public void setPrecedeCenterPre() {
		precedeCenterPre = true;
	}

	/**
	 * @return
	 */
	public List getUnivAllList() {
		return univAllList;
	}

	/**
	 * @param list
	 */
	public void setUnivAllList(List list) {
		univAllList = list;
	}

	/**
	 * @return
	 */
	public String[][] getUnivArray() {
		return univArray;
	}

	/**
	 * @param strings
	 */
	public void setUnivArray(String[][] strings) {
		univArray = strings;
	}
//[1] del start
//	/**
//	 * @return
//	 */
//	public boolean isUseNewLogic() {
//		return isUseNewLogic;
//	}
//
//	/**
//	 * @param b
//	 */
//	public void setUseNewLogic(boolean b) {
//		isUseNewLogic = b;
//	}
//[1] del end
	/**
	 * @return
	 */
	public Score getScoreBean() {
		return scoreBean;
	}

	/**
	 * @param bean
	 */
	public void setScoreBean(Score bean) {
		scoreBean = bean;
	}

	/**
	 * @return
	 */
	public List getRecordSet() {
		return recordSet;
	}

	/**
	 * @param list
	 */
	public void setRecordSet(List list) {
		recordSet = list;
	}

}
/*
 * 作成日: 2004/09/09
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.StudentData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.data.sheet.ExtI11ListBean;
import jp.co.fj.keinavi.data.sheet.IPrintNumberCalculator;
import jp.co.fj.keinavi.data.sheet.PrintNumberDefinition;
import jp.co.fj.keinavi.data.sheet.SheetStatus;
import jp.co.fj.keinavi.excel.data.personal.I11Item;
import jp.co.fj.keinavi.excel.personal.Personal;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import com.fjh.beans.DefaultBean;

/**
 * 個人用：帳票出力を実行するクラス
 *
 * @author kawai
 * history
 *   Symbol Date       Person     Note
 * 2005.03.01 	K.Kondo 	[1]現年度をカレンダーから取得時、３月３１日以前は前年度とする日付比較が、うまく行われていなかったのを修正。
 * 2005.03.08 	K.Kondo 	[2]現年度指定の年度取得を、プロファイルの担当クラス年度に変更
 * 2005.04.08 	K.Kondo     [3]面談シート４を追加
 * 2005.04.08	K.Kondo		[4]偏差値／得点種別を追加
 * 2005.04.14   K.Kondo     [5]プロファイルに面談シート印刷無しが保存されている場合、面談シートフラグを「1」とする
 * 2005.04.21   K.Kondo     [6]センターリサーチ時に設問別成績グラフが選択されている場合の処理を追加
 * 2005.04.21   K.Kondo     [7]対象模試がマーク記述以外のときには、志望校判定は印刷しない
 * 2005.04.27   K.Kondo     [8]教科数を取得するとき、対象学年が３年生なら５教科ととするように修正（画面とあわせる）
 */
public class IndSheetExecutorBean extends DefaultBean {

	private String[] command; // アプレットパラメータ
	private SheetStatus status; // 出力状況
	private HttpSession session; // セッション
	private String sessionkey; // セッションキー

	private Profile profile; // プロファイル
	private ExamSession examSession; // 模試セッション
	private ExamData exam; // 模試データ
	private LoginSession login; // ログインセッション
	private KNSheetLog sheetLog; // Logger

	private ArrayList univAll;
	private Map univInfoMap;
	private String studentGrade ="";
	private boolean mendanFlg;
	private final I11Item data = new I11Item(); // データクラス

	/**
	 * コンストラクタ
	 */
	public IndSheetExecutorBean(SheetStatus status) {
		this.status = status;
	}

	private ICommonMap getICommonMap() {
		return (ICommonMap) this.session.getAttribute(ICommonMap.SESSION_KEY);
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		int printFlag = Integer.parseInt(command[1]); // 印刷フラグ
		String backward = command[6]; 	// 転送元

		// 個人情報の取得
		ICommonMap map = getICommonMap();

		// かんたん印刷プロファイル
		// 教科分析シートフラグ
		String [] KyoSheetFlg = CollectionUtil.splitComma((String) profile.getItemMap(
				IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_FORMS));

		// 面談シートフラグ
		Short MenSheet = (Short) profile.getItemMap(
				IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_SHEET);
		int MenSheetFlg = Integer.parseInt(MenSheet.toString());

		// 連続個人フラグ
		Short Report = (Short) profile.getItemMap(
				IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_REPORT);
		int ReportFlg = Integer.parseInt(Report.toString());

		// 志望大学判定フラグ
		Short ExamDoc = (Short) profile.getItemMap(
				IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_EXAM_DOC);
		int ExamDocFlg = Integer.parseInt(ExamDoc.toString());

		// 帳票種類
		Short textFileFormat = (Short) profile.getItemMap(
				IProfileCategory.I_PERSONAL_NOTE).get(IProfileItem.TEXT_FILE_FORMAT);

		// センターリサーチフラグ
		final boolean isResearch = KNUtil.CD_CENTER.equals(map.getTargetExamCode());

		// 受験学力測定テストフラグ
		final boolean isAbilityExam = KNUtil.TYPECD_ABILITY.equals(map.getTargetExamTypeCode());

		// 印刷生徒単位
		int PrintStudent = 0;
		mendanFlg = false;
		if("i204".equals(backward)){
			Short printStudent = (Short) profile.getItemMap(IProfileCategory.I_UNIV_RESULTS).get(IProfileItem.PRINT_STUDENT);
			PrintStudent = Integer.parseInt(printStudent.toString());
		}else{
			Short printStudent = (Short) profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);
			PrintStudent = Integer.parseInt(printStudent.toString());
		}

		// 一括印刷対象
		String[] PrintTarget = CollectionUtil.splitComma((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET));

		// 印刷枚数計算クラス
		final IPrintNumberCalculator calculator = new IPrintNumberCalculator(
				getPrintNumberDefinition(),
				map.getTargetExamTypeCode(),
				getStudentNumber(map, PrintStudent));

		List container = new LinkedList(); // Beanの入れ物

		// 一括印刷ボタンおされた処理
		if(!"i401".equals(backward) && (printFlag == 3 || printFlag == 4)) {
			for(int i=0; i< PrintTarget.length; i++){

				if(PrintTarget[i].equals("1")){
				switch (i) {

					case 0:
						container.add(new I13SheetBean());
						calculator.add("I13_01");
						break;

					case 1:
						if (!isAbilityExam) {
							container.add(new I12_01_1SheetBean());
							container.add(new I12_01_2SheetBean());
							container.add(new I12_01_3SheetBean());
							calculator.add("I12_01");
						}
						break;

					case 2:
						if (!isAbilityExam) {
							container.add(new I12_02SheetBean());
							calculator.add("I12_02");
						}
						break;

					case 3:
						if (!isAbilityExam && !isResearch){
							container.add(new I12_03SheetBean());
							calculator.add("I12_03");
						}
						break;
				}
			  }
			}
		}

		// 単品印刷・表示
		// かんたん印刷
		if (printFlag == 1 || printFlag == 2
				|| printFlag == 8 || "i401".equals(backward) || "i003".equals(backward)) {
			// 単品印刷ボタン
			if ("i101".equals(backward) || "i102".equals(backward)) {
				container.add(new I13SheetBean());

			} else if ("i103".equals(backward) || "i104".equals(backward) || "i106".equals(backward)) {

				container.add(new I12_01_1SheetBean());
				container.add(new I12_01_2SheetBean());
				container.add(new I12_01_3SheetBean());

			} else if ("i107".equals(backward)) {

				container.add(new I12_02SheetBean());

			} else if ("i108".equals(backward)) {

				container.add(new I12_03SheetBean());

			} else if ("i204".equals(backward)) {

				container.add(new I14SheetBean());

			} else if ("i302".equals(backward) || "i305".equals(backward) || "i306".equals(backward)) {

				// 志望校判定
				if ("i302".equals(backward) && Short.valueOf("2").equals(textFileFormat)) {
					container.add(new I14SheetBean());
				// 受験大学スケジュール表
				} else {
					container.add(new I15SheetBean());
				}


                        } else if ("i003".equals(backward)) {

                            final String selectGrade = String.valueOf(map.getSelectedGrade());
                            final Short targetGrade = (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.STUDENT_DISP_GRADE);
                            final String[] MendanSheetFlg = map.getInterviewSheet();

                            //センターリサーチは設問別成績グラフNG
                            if (isResearch) {
                                    KyoSheetFlg[2] = "0";
                            }

                            //受験学力測定テストは面談用帳票NG
                            if (isAbilityExam) {
                                    KyoSheetFlg = new String[]{"0", "0", "0"};
                            }

                            //面談シート1：連続個人成績表
                            if (ReportFlg == 1) {
                                container.add(new I13SheetBean());
                                if ("3".equals(selectGrade) &&  "1".equals(targetGrade.toString())) {
                                    calculator.add("I13_01");
                                } else  {
                                    calculator.add("I13_02");
                                }
                            }
                            //面談シート2：連続個人成績表＋教科別成績推移グラフ＋バランスチャート（合格者平均）
                            if ("1".equals(MendanSheetFlg[0])) {
                                MenSheetFlg = 5;
                                container.add(new I13SheetBean((short) MenSheetFlg));
                                if(KyoSheetFlg[0].equals("1")){
                                    //教科分析シート
                                    container.add(new I12_01_1SheetBean((short) MenSheetFlg));
                                }
                                container.add(new I12_01_2SheetBean((short) MenSheetFlg));
                                container.add(new I12_01_3SheetBean((short) MenSheetFlg));
                                calculator.add("I11_07");
                            }
                            //面談シート3：連続個人成績表＋教科別成績推移グラフ＋バランスチャート
                            if ("1".equals(MendanSheetFlg[1])) {
                                MenSheetFlg = 2;
                                container.add(new I13SheetBean((short) MenSheetFlg));
                                container.add(new I12_01_1SheetBean((short) MenSheetFlg));
                                if(KyoSheetFlg[0].equals("1")){
                                    //教科分析シート
                                    container.add(new I12_01_2SheetBean((short) MenSheetFlg));
                                }
                                container.add(new I12_01_3SheetBean((short) MenSheetFlg));
                                if (this.getCourseCount() > 3) {
                                    calculator.add("I11_01");
                                } else {
                                    calculator.add("I11_02");
                                }
                            }
                            if ("1".equals(MendanSheetFlg[0]) && "1".equals(MendanSheetFlg[1])) {
                                mendanFlg = true;
                            }
                            //教科分析シート
                            if (KyoSheetFlg[0].equals("1") && "0".equals(MendanSheetFlg[0]) && "0".equals(MendanSheetFlg[1])) {
                                container.add(new I12_01_1SheetBean());
                                container.add(new I12_01_2SheetBean());
                                container.add(new I12_01_3SheetBean());
                                calculator.add("I12_01");
                            }
                            //成績推移グラフ
                            if (KyoSheetFlg[1].equals("1")) {
                                container.add(new I12_02SheetBean());
                                calculator.add("I12_02");
                            }

                            //設問別成績グラフ
                            if (KyoSheetFlg[2].equals("1")) {
                                container.add(new I12_03SheetBean());
                                calculator.add("I12_03");
                            }
			} else if ("i401".equals(backward)) {
				//かんたん印刷

				//面談シート１の制限
				if(MenSheetFlg == 5 &&
					!(
						(map.getTargetExamTypeCode().equals("01") || map.getTargetExamTypeCode().equals("02") || map.getTargetExamTypeCode().equals("03"))
						&& !map.getTargetExamCode().equals("66") && !map.getTargetExamCode().equals("67")
					)
				) {
					MenSheetFlg = 1;
				}

				//センターリサーチは設問別成績グラフNG
				if (isResearch) {
					KyoSheetFlg[2] = "0";
				}

				//受験学力測定テストは面談用帳票NG
				if (isAbilityExam) {
					KyoSheetFlg = new String[]{"0", "0", "0"};
				}

				//志望大学判定の制限
				if(ExamDocFlg == 1 && !(map.getTargetExamTypeCode().equals("01") || map.getTargetExamTypeCode().equals("02"))) {
					ExamDocFlg = 0;
				}

				//if(MenSheetFlg == 2 || MenSheetFlg == 3 || MenSheetFlg == 4 || KyoSheetFlg[0].equals("1") || ReportFlg == 1){//[3] delete
				if(MenSheetFlg == 2 || MenSheetFlg == 3 || MenSheetFlg == 4 || MenSheetFlg == 5 || KyoSheetFlg[0].equals("1") || ReportFlg == 1){

					if(MenSheetFlg == 2){
						if(!KyoSheetFlg[0].equals("1")){
							//面談シート2
							container.add(new I13SheetBean());
							container.add(new I12_01_1SheetBean());
							container.add(new I12_01_3SheetBean());
						}
						if(KyoSheetFlg[0].equals("1")){
							//教科分析シート、//面談シート2両方
							container.add(new I13SheetBean());
							container.add(new I12_01_1SheetBean());
							container.add(new I12_01_2SheetBean());
							container.add(new I12_01_3SheetBean());
						}
						calculator.add("I11_01");
					}

					if(MenSheetFlg == 3){
						if(!KyoSheetFlg[0].equals("1")){
							//面談シート3
							container.add(new I13SheetBean());
							container.add(new I12_01_3SheetBean());
						}
						if(KyoSheetFlg[0].equals("1")){
							//面談シート3//教科分析シート
							container.add(new I13SheetBean());
							container.add(new I12_01_1SheetBean());
							container.add(new I12_01_2SheetBean());
							container.add(new I12_01_3SheetBean());
						}
						calculator.add("I11_03");
					}

					if(MenSheetFlg == 4){
						if(!KyoSheetFlg[0].equals("1")){
							//面談シート4
							container.add(new I13SheetBean());
							container.add(new I12_01_1SheetBean());
						}
						if(KyoSheetFlg[0].equals("1")){
							//面談シート4//教科分析シート
							container.add(new I13SheetBean());
							container.add(new I12_01_1SheetBean());
							container.add(new I12_01_2SheetBean());
							container.add(new I12_01_3SheetBean());
						}
						calculator.add("I11_05");
					}

					//[3] add start
					if(MenSheetFlg == 5){
						if(!KyoSheetFlg[0].equals("1")){
							//面談シート1
							container.add(new I13SheetBean());
							container.add(new I12_01_2SheetBean());
							container.add(new I12_01_3SheetBean());
						}
						if(KyoSheetFlg[0].equals("1")){
							//教科分析シート + 面談シート1
							container.add(new I13SheetBean());
							container.add(new I12_01_1SheetBean());
							container.add(new I12_01_2SheetBean());
							container.add(new I12_01_3SheetBean());
						}
						calculator.add("I11_07");
					}
					//[3] add end
				}


				//教科分析シート
				if (KyoSheetFlg[0].equals("1")) {
					if (MenSheetFlg == 1) {
						container.add(new I12_01_1SheetBean());
						container.add(new I12_01_2SheetBean());
						container.add(new I12_01_3SheetBean());
					}
					calculator.add("I12_01");
				}

				//連続個人成績表
				if (ReportFlg == 1) {
					if (MenSheetFlg == 1) {
						container.add(new I13SheetBean());
					}
					calculator.add("I13_01");
				}

				//科目別成績推移シート
				if (KyoSheetFlg[1].equals("1")) {
					container.add(new I12_02SheetBean());
					calculator.add("I12_02");
				}

				//設問別フラグ
				if (KyoSheetFlg[2].equals("1")) {
					container.add(new I12_03SheetBean());
					calculator.add("I12_03");
				}

				//志望大学判定一覧
				if (ExamDocFlg == 1) {
					container.add(new I14SheetBean());
					calculator.add("I14_01");
				}

			} else {
				throw new Exception("不正な画面IDの指定です。");
			}
		}

		// 印刷枚数をセット
		status.setSheetNumber(calculator.getPrintNumber());

		//[5] add start
		int MENSHEET_NO_PRINT = 1;
		if(MenSheetFlg == MENSHEET_NO_PRINT) {
			data.setIntMenSheetFlg(MENSHEET_NO_PRINT);
		}
		//[5] add end

		data.setStrBaseMshmei(exam.getExamName());						//今回の模試
		data.setStrMshmei(exam.getExamNameAbbr());						//今回の模試短縮名
		data.setStrBaseMshDate(exam.getInpleDate());					//模試実施日
		data.setStrBaseGakunen(exam.getTargetGrade());					//学年
		data.setIntBaseMshKyokaSr(this.getCourseCount());				//模試教科数


		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try{

			//項番２SQL＿選択生徒情報取得
			ArrayList IDList = new ArrayList();

			/**分析モードだったら**/
			if(map.isBunsekiMode()){
				if(PrintStudent == 2){
					//選択中の生徒一人
					IDList.add(map.getTargetPersonId());
				}else{
					//選択中の生徒全員
					for (java.util.Iterator it=map.getSelectedIndividuals().iterator(); it.hasNext();) {
						StudentData StudentData = (StudentData)it.next();
						IDList.add(StudentData.getStudentPersonId());
					}
				}
			}else{
				/**面談モードだったら**/
				//選択中の生徒一人
				IDList.add(map.getTargetPersonId());
			}

			Query query1 = QueryLoader.getInstance().load("i12_select_student");
			query1.setStringArray(1,(String[])IDList.toArray(new String[0])); 	// プロファイルIDリスト
			pstmt = conn.prepareStatement(query1.toString());
			pstmt.setString(1, profile.getBundleCD());							//一括コード
			pstmt.setString(2, this.getThisYear());							//今年の年度
			rs = pstmt.executeQuery();

			while(rs.next()){

				//データを詰める
				ExtI11ListBean data_list = new ExtI11ListBean();		// データクラス
				data_list.setStrGakkomei(rs.getString(2)); 			    // 学校名
				data_list.setStrGrade(rs.getString(3)); 				// 学年
				data_list.setStrClass(rs.getString(4)); 				// クラス名
				data_list.setStrClassNum(rs.getString(5)); 				// クラス番号
				data_list.setStrShimei(rs.getString(6)); 				// 氏名
				data_list.setStrKana(rs.getString(7)); 					// カナ氏名
				data_list.setStrSex(rs.getString(8)); 					// 性別
				data_list.setStrTelNo(rs.getString(9)); 				// 電話番号
				data_list.setIndividualid(rs.getString(1));				// 個人ID
				// 2020/02/07 QQ)Ooseto 共通テスト対応 ADD START
				data_list.setMenSheetFlg(Integer.toString(MenSheetFlg)); //面談シートフラグ
				// 2020/02/07 QQ)Ooseto 共通テスト対応 ADD END
				//データセット
				data.getI11List().add(data_list);

				//対象生徒
				studentGrade = rs.getString(3);

			}//個人情報

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}

		// 全て実行する
		this.status.setTotal(container.size() + 1); // 帳票作成のためひとつ増加
		Iterator ite = container.iterator();
		while (ite.hasNext()) {
			IndAbstractSheetBean bean = (IndAbstractSheetBean) ite.next();
			bean.setConnection(null, conn);
			bean.setAction(command[1]);
			bean.setProfile(profile);
			bean.setExamSession(examSession);
			bean.setExam(exam);
			bean.setStudentGrade(studentGrade);
			bean.setLogin(login);
			bean.setPastExam1(command[4]);
			bean.setPastExam2(command[5]);
			bean.setTargetExamCode1(command[7]);
			bean.setTargetExamCode2(command[8]);
			bean.setTargetExamCode3(command[9]);
			bean.setTargetCandidateRank1(command[10]);
			bean.setTargetCandidateRank2(command[11]);
			bean.setTargetCandidateRank3(command[12]);
//2019/07/09 QQ)Tanioka ダウンロード変更対応 DEL START
//			bean.setJudgeType(command[13]);
//2019/07/09 QQ)Tanioka ダウンロード変更対応 DEL END
			bean.setSession(session);
			bean.setBackward(backward);
			bean.setData(data);
			bean.setUnivAll(univAll);
			bean.setUnivInfoMap(univInfoMap);
			bean.setSheetLog(sheetLog);
			bean.execute();
			conn.commit();
			this.status.countUp();
		}
	}

	/**
	 * 帳票を作成する
	 * @return
	 */
	public boolean createSheet() {

		if (new Personal().personal(
				data,
				status.getOutfileList(),
				status.getTempfileList(),
				Integer.parseInt(command[1]),
				sessionkey,
				sheetLog,
				mendanFlg)) {

			status.countUp();
			return true;

		} else {
			return false;
		}
	}

	/**
	 * 対象模試模試の教科数を調べる
	 * @return
	 */
	private int getCourseCount() throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		int courseCount = 0;
		ICommonMap map = this.getICommonMap();

		try {
			Query query = QueryLoader.getInstance().load("i12_course_count");
			ps = conn.prepareStatement(query.toString());

			ps.setString(1, map.getTargetExamYear()); 	// 模試年度
			ps.setString(2, map.getTargetExamCode()); 	// 模試種類コード

			rs = ps.executeQuery();
//[8] delete start
//			while (rs.next()) {
//				courseCount = rs.getInt(1);
//			}
//[8] delete end
//[8] add start
			String targetGrade = "";
			while(rs.next()) {
				courseCount++;
				targetGrade = rs.getString("tergetgrade");
			}
			if(targetGrade.equals("03")) {
				courseCount = 5;
			}
//[8] add end
		} catch(Exception e) {
			 e.printStackTrace();
		} finally {
			if(ps != null) ps.close();
			if(rs != null) rs.close();
		}

		return courseCount;
	}

	/**
	 * 現年度を返します。
	 * @return 現年度
	 */
	protected String getThisYear() {
//[1] delete start
//		String thisYear = "";
//		Calendar cal1 = Calendar.getInstance();
//		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");//デートフォーマット
//		String nowYmd = sdf1.format(cal1.getTime());
//		int year = Integer.parseInt(nowYmd.substring(0, 4));
//		int monthDay = Integer.parseInt(nowYmd.substring(4, 8));
//		if(monthDay < 0401) {
//			thisYear = Integer.toString(year - 1);
//		} else {
//			thisYear = Integer.toString(year);
//		}
//		return thisYear;
//[1] delete end
//		return KNUtil.getCurrentYear();//[1] add//[2] delete
		return ProfileUtil.getChargeClassYear(profile);//[2] add
	}

	// 印刷生徒人数を取得する
	private int getStudentNumber(final ICommonMap map, final int printStudent) {

		// 分析モード
		if (map.isBunsekiMode()) {
			// 選択中の生徒一人
			if (printStudent == 2) {
				return 1;
			// 選択中の生徒全員
			} else {
				return map.getSelectedIndividuals().size();
			}
		// 面談モードはひとり
		} else {
			return 1;
		}
	}

	private PrintNumberDefinition getPrintNumberDefinition() {
		final ServletContext context = session.getServletContext();
		return (PrintNumberDefinition) context.getAttribute(
				"SheetNumberDefinition");
	}

	/**
	 * @param strings
	 */
	public void setCommand(String[] strings) {
		command = strings;
	}

	/**
	 * @param session
	 */
	public void setExamSession(ExamSession session) {
		examSession = session;
	}

	/**
	 * @param session
	 */
	public void setLogin(LoginSession session) {
		login = session;
	}

	/**
	 * @param profile
	 */
	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	/**
	 * @param data
	 */
	public void setExam(ExamData data) {
		exam = data;
	}

	/**
	 * @param session
	 */
	public void setSession(HttpSession session) {
		this.session = session;
	}

	/**
	 * @param list
	 */
	public void setUnivAll(ArrayList list) {
		univAll = list;
	}

	/**
	 * @param string
	 */
	public void setSessionkey(String string) {
		sessionkey = string;
	}

	/**
	 * @param pLog 設定する log。
	 */
	public void setLog(KNLog pLog) {
		// dummy
		pLog.getClass();
	}

	/**
	 * @param pSheetLog 設定する sheetLog。
	 */
	public void setSheetLog(KNSheetLog pSheetLog) {
		sheetLog = pSheetLog;
	}

	/**
	 * @param map
	 */
	public void setUnivInfoMap(Map map) {
		univInfoMap = map;
	}

}

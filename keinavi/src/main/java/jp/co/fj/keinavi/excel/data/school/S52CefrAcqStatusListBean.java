package jp.co.fj.keinavi.excel.data.school;

/**
 * Zà¬ÑªÍ|ßñär|Î·lªz CEFRæ¾óµf[^XgiS±j
 * ì¬ú: 2019/09/25
 * @author      DP)H.Nonaka
 */
public class S52CefrAcqStatusListBean {

    private String engptcd;                                 //pêQÁ±R[h
    private String engptname_abbr;                          //QÁ±Zk¼
    private String levelflg;                                //x ètO
    private int ei_sort;                                    //ÀÑ
    private String engptlevelcd;                            //pêQÁ±xR[h
    private String engptlevelname_abbr;                      //QÁ±xZk¼
    private int edi_sort;                                   //ÀÑ
    private String cefrlevelcd;                             //CEFRxR[h
    private String cerflevelname_abbr;                       //bdeqxZk¼
    private int ci_sort;                                    //ÀÑ
    private String examname;                                 //ßñÍ¼
    private int dispsequence;                               //ÀÑ
    private int numbers;                                    //l
    private float compratio;                                //\¬ä
    private String flg;                                      //ó±L³tO

    /*----------*/
    /* Get      */
    /*----------*/
    public String getEngptcd() {
        return engptcd;
    }
    public String getEngptname_abbr() {
        return engptname_abbr;
    }
    public String getLevelflg() {
        return levelflg;
    }
    public int getEi_sort() {
        return ei_sort;
    }
    public String getEngptlevelcd() {
        return engptlevelcd;
    }
    public String getEngptlevelname_abbr() {
        return engptlevelname_abbr;
    }
    public int getEdi_sort() {
        return edi_sort;
    }
    public String getCefrlevelcd() {
        return cefrlevelcd;
    }
    public String getCerflevelname_abbr() {
        return cerflevelname_abbr;
    }
    public int getCi_sort() {
        return ci_sort;
    }
    public String getExamname() {
        return examname;
    }
    public int getDispsequence() {
        return dispsequence;
    }
    public int getNumbers() {
        return numbers;
    }
    public float getCompratio() {
        return compratio;
    }
    public String getFlg() {
        return flg;
    }

    /*----------*/
    /* Set      */
    /*----------*/
    public void setEngptcd(String engptcd) {
        this.engptcd = engptcd;
    }
    public void setEngptname_abbr(String engptname_abbr) {
        this.engptname_abbr = engptname_abbr;
    }
    public void setLevelflg(String levelflg) {
        this.levelflg = levelflg;
    }
    public void setEi_sort(int ei_sort) {
        this.ei_sort = ei_sort;
    }
    public void setEngptlevelcd(String engptlevelcd) {
        this.engptlevelcd = engptlevelcd;
    }
    public void setEngptlevelname_abbr(String engptlevelname_abbr) {
        this.engptlevelname_abbr = engptlevelname_abbr;
    }
    public void setEdi_sort(int edi_sort) {
        this.edi_sort = edi_sort;
    }
    public void setCefrlevelcd(String cefrlevelcd) {
        this.cefrlevelcd = cefrlevelcd;
    }
    public void setCerflevelname_abbr(String cerflevelname_abbr) {
        this.cerflevelname_abbr = cerflevelname_abbr;
    }
    public void setCi_sort(int ci_sort) {
        this.ci_sort = ci_sort;
    }
    public void setExamname(String examname) {
        this.examname = examname;
    }
    public void setDispsequence(int dispsequence) {
        this.dispsequence = dispsequence;
    }
    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }
    public void setCompratio(float compratio) {
        this.compratio = compratio;
    }
    public void setFlg(String flg) {
        this.flg = flg;
    }
}

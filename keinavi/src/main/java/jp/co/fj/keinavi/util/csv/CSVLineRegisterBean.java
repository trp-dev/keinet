package jp.co.fj.keinavi.util.csv;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.util.csv.data.CSVLineRecord;

import com.fjh.beans.DefaultBean;

/**
 *
 * CSV行登録Bean
 * 
 * 2005.10.25	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public abstract class CSVLineRegisterBean extends DefaultBean {

	// ダイレクトコミットコールであるか
	private boolean isDirectCommit = false;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public final void execute() throws Exception {
		
		throw new IllegalAccessException(
				"このメソッドは利用できません。");
	}
	
	/**
	 * 行レコードを正規化する
	 * 
	 * @param record 正規化されていない行レコード
	 * @return 正規化した行レコード
	 */
	public abstract String[] normalizeRecord(String[] record);
	
	/**
	 * 全体のエラーチェック処理を行う
	 * 
	 * @param lineRecordList 行レコードオブジェクトのリスト
	 */
	public void validateAllRecord(final List lineRecordList) throws SQLException {
		
		// CheckStyle対策･･･
		lineRecordList.getClass();
	}

	/**
	 * 行レコードのエラーチェック処理を行う
	 * 
	 * @param record 行レコードオブジェクト
	 */
	public abstract void validateRecord(CSVLineRecord record) throws SQLException;
	
	/**
	 * 行レコードのタイプを判定する
	 * 
	 * @param record 行レコードオブジェクト
	 * @throws SQLException SQLエラーで発生する
	 */
	public abstract void judgeRecordType(
			CSVLineRecord record) throws SQLException;
	
	/**
	 * 行レコードの登録処理をする
	 * 
	 * @param record 行レコードオブジェクト
	 * @throws SQLException SQLエラーで発生する
	 */
	public abstract void registRecord(
			CSVLineRecord record) throws SQLException;
	
	/**
	 * 行レコードオブジェクトを作る
	 * [FactoryMethod]
	 * 独自のレコードオブジェクトにしたければオーバーライドすること
	 * 
	 * @param record 正規化された行レコード
	 * @param lineNo 行番号
	 * @return 行レコードオブジェクト
	 */
	public CSVLineRecord createLineRecord(final String[] record,
			final int lineNo) {
		
		return new CSVLineRecord(record, lineNo);
	}
	
	/**
	 * 削除レコードを選択する
	 * 削除を扱わなければオーバーライドしなくてもよい
	 * 
	 * @param lineRecordList 行レコードオブジェクトのリスト
	 * @return 削除レコードを含むかどうか
	 * @throws SQLException SQLエラーで発生する
	 */
	public List selectDeleteRecord(
			final List lineRecordList) throws SQLException {
	
		// CheckStyle対策･･･
		lineRecordList.getClass();
		
		return new ArrayList();
	}
	
	/**
	 * 削除処理を行う
	 * 
	 * @param deleteRecordList 削除する行レコードオブジェクトのリスト
	 * @throws SQLException SQLエラーで発生する
	 */
	public void deleteRecord(final List deleteRecordList) throws SQLException {
		// dummy...
		deleteRecordList.getClass();
	}
	
	/**
	 * 不要レコードのクリーン処理
	 * 
	 * @throws SQLException SQLエラーで発生する
	 */
	public void cleanRecord() throws SQLException {
	}

	/**
	 * @return isDirectCommit を戻します。
	 */
	public boolean isDirectCommit() {
		return isDirectCommit;
	}

	/**
	 * ダイレクトコミット状態にする
	 */
	public void setDirectCommit() {
		isDirectCommit = true;
	}

}

package jp.co.fj.keinavi.forms.maintenance;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 *
 * 作業中宣言画面アクションフォーム
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class X010Form extends BaseForm{
	
	// 動作モード
	private String actionMode;
	
	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {

	}
	
	/**
	 * @return
	 */
	public String getActionMode() {
		return actionMode;
	}

	/**
	 * @param string
	 */
	public void setActionMode(String string) {
		actionMode = string;
	}

}

/*
 * 作成日: 2004/11/24
 *
 *
 *プロファイルの複製を一括で行う。
 *
 *	コピーしたい学校の高校コード、ユーザIDをテキストファイルで読込み、
 *	指定の名前のフォルダを作成、
 *  作成フォルダにコピー元と同じプロファイル名でプロファイルをコピーする。
 *
 *	【使用方法】
 *		単独で起動する
 *		使用状況によって下記を変更する必要有り
 *		1. SQLローダー：SQLローダーのパス
 *		2. 接続先DB：プロファイルコピーを行いたいDBを指定する
 *		3. 入力テキストファイル：コピーしたい学校情報のテキストファイル名を指定する
 *								 フォーマット：学校毎に”高校コード,ユーザID<CR><LF>”
 *		4. コピー先作成フォルダ：作成するプロファイル名、コメントを指定する
 *		5. コピー元プロファイル：コピー元のプロファイルIDを指定する
 *		6. コピー元学校コード：コピー元の学校コードを指定する
 *
 *	【注意】
 *		同じフォルダ名、プロファイル名が存在しないこと
 *
 */
package jp.co.fj.keinavi.beans.profile;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.admin.application.Application;
import jp.co.fj.keinavi.beans.admin.application.DbAccessInf;
import jp.co.fj.keinavi.beans.admin.name.NameBringsBean;
import jp.co.fj.keinavi.beans.admin.name.SystemLogWriter;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 2005.10.26	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 * @author kawai2
 *
 */
public class ProfileCopyBean {

    private LoginSession login; // ログイン情報
    private Connection con; // DBコネクション

    public static void main(String[] args) throws SQLException, Exception {

        SystemLogWriter.setLogWrite(true);

        SystemLogWriter.printLog(NameBringsBean.class, "■■■■ プロファイルコピー処理を開始します。 ■■■■", SystemLogWriter.INFO);
        System.out.println("プロファイルコピー処理開始 " + getFormatedTimeString("yyyy/MM/dd HH:mm:ss:SS"));


        ProfileCopyBean bean = new ProfileCopyBean();
        try {

            if (args == null || args.length == 0) {
                System.out.println("パラメーターが未指定です。 ");
                System.exit(1);
            }

            File file = new File(args[0]);
            if (!file.exists()) {
                System.out.println("ファイルが存在しません。file=[" + args[0] + "]");
                System.exit(1);
            }

            // プロファイルコピー処理
            bean.execute(file);

        } finally {

            SystemLogWriter.printLog(NameBringsBean.class, "■■■■ プロファイルコピー処理を終了します。 ■■■■", SystemLogWriter.INFO);

            System.out.println("プロファイルコピー処理終了 " + getFormatedTimeString("yyyy/MM/dd HH:mm:ss:SS"));

            SystemLogWriter.setLogWrite(false);

            // ログファイルを解放する
            SystemLogWriter.close();
        }

    }

    /* (非 Javadoc)
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute(File file) throws SQLException, Exception {

        /** ログイン情報から読み込み **/
        login = new LoginSession();
        login.setUserMode(11);

        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try (BufferedReader ds = new BufferedReader(new InputStreamReader(new FileInputStream(file), "MS932"))) {
            // driverを読み込み
            Class.forName(DbAccessInf.getDriver());

            // connectionを取得
            con = DriverManager.getConnection(DbAccessInf.getURL(), DbAccessInf.getLoginId(), DbAccessInf.getPassWord());
            QueryLoader.getInstance().setPath(Application.getSqlPath());
            con.setAutoCommit(false);

            /** textから学校コード・ユーザーID読み込み **/
            String line_str = "";
            System.out.println("textファイル読み込み file=[" + file.getAbsolutePath() + "]");

            int count = 0;
            while((line_str = ds.readLine()) != null){

                String[] data = line_str.split(",");

                /** フォルダ作成 **/
                String bundlecd   = data[0];
                String userId     = data[1];
                System.out.println(String.format("RECORD:[%d]件目 bundleCd:[%s] userId:[%s]", ++count, bundlecd, userId));

                String folderName = "ひな型フォルダ";  				//新規フォルダ名【必要に応じて変更】
                String comment    = "";	//新規フォルダのコメント【必要に応じて変更】

                login.setLoginID(userId);

                ProfileFolderUpdateBean  upDateFolder = new ProfileFolderUpdateBean();
                upDateFolder.setConnection("", con);
                upDateFolder.setLoginSession(login);
                upDateFolder.setFolderID("");
                upDateFolder.setFolderName(folderName);
                upDateFolder.setComment(comment);
                upDateFolder.setBundleCD(bundlecd);
                upDateFolder.execute();

                /** ↑で作成したフォルダIDを取得する **/
                String query = "SELECT PROFILEFID FROM PROFILEFOLDER WHERE BUNDLECD = ? AND PROFILEFNAME = ?";

                pstmt = con.prepareStatement(query);
                pstmt.setString(1, bundlecd);
                pstmt.setString(2, folderName);
                rs = pstmt.executeQuery();

                String folderId = null;
                try{
                    if (rs.next()) {
                        folderId = rs.getString("PROFILEFID");
                    } else {
                        throw new Exception("フォルダIDの取得に失敗しました。");
                    }

                    if (rs.next()) throw new Exception("フォルダIDが複数あります。");
                }finally{
                    if(pstmt != null) pstmt.close();
                    if(rs != null) rs.close();
                }

                // プロファイルのコピー作成(コピー元プロファイルIDを指定し、それを複製する)
                // ２つ目の引数がコピー元プロファイルID【必要に応じて変更】
                // （複数プロファイルをコピーしたい場合は、必要な数だけProfileCopy()を呼ぶ
                ProfileCopy(folderId, "0400216",bundlecd);
                ProfileCopy(folderId, "0400217",bundlecd);
                ProfileCopy(folderId, "0400218",bundlecd);
                ProfileCopy(folderId, "0400220",bundlecd);
                ProfileCopy(folderId, "0400222",bundlecd);
                ProfileCopy(folderId, "0400223",bundlecd);
                ProfileCopy(folderId, "0400224",bundlecd);

            }//テキスト読み込みループ

            con.commit();

        } catch(Exception e) {
            if (con != null) con.rollback();
            e.printStackTrace();
            SystemLogWriter.printLog(NameBringsBean.class, e.getMessage(), SystemLogWriter.WARNING);
            System.out.println(e.getMessage());
        } finally {
            DbUtils.closeQuietly(con);
        }
    }

    private void ProfileCopy(String folderId, String profileId, String bundlecd) throws Exception{
        /** プロファイルのコピー作成 **/
        ProfileBean profileBean = new ProfileBean();
        profileBean.setConnection("", con);
        profileBean.setLoginSession(login);
        profileBean.setProfileID(profileId);
        profileBean.setBundleCD("24502");			// コピー元学校コード【必要に応じて変更】
        profileBean.execute();

        // 複製したプロファイルを書き換える
        Profile profile = profileBean.getProfile();
        profile.setProfileID(null);
        profile.setFolderID(folderId);
        profile.setUserID(null);
        profile.setBundleCD(bundlecd);

        // INSERT
        ProfileUpdateBean profileUpBean = new ProfileUpdateBean();
        profileUpBean.setConnection("", con);
        profileUpBean.setLoginSession(login);
        profileUpBean.setProfile(profile);
        profileUpBean.execute();

        System.out.println(String.format("  profileを作成しました。 profileId:[%s]", profile.getProfileID()));
    }

    /**
    *
    * タイムスタンプ
    *
    * @param index 書式
    * @return 現在の日付を設定された書式の文字型で返します。
    */
    public static String getFormatedTimeString(String index) {
            java.util.Date currentTime = java.util.Calendar.getInstance().getTime();
            java.text.DateFormat form = new java.text.SimpleDateFormat(index);
            String timeStr = form.format(currentTime);
            return timeStr;
    }

}

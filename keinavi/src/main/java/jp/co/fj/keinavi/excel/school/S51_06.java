/**
 * 校内成績分析−過回・クラス比較　成績概況グラフ
 * 	Excelファイル編集
 * 作成日: 2004/09/07
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S51ClassListBean;
import jp.co.fj.keinavi.excel.data.school.S51Item;
import jp.co.fj.keinavi.excel.data.school.S51ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S51_06 {

	private int noerror	= 0;		// 正常終了
	private int errfread	= 1;		// ファイルreadエラー
	private int errfwrite	= 2;		// ファイルwriteエラー
	private int errfdata	= 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	final private String	masterfile0	= "S51_06";		// ファイル名
	final private String	masterfile1	= "NS51_06";	// ファイル名
	private String	masterfile	= "";					// ファイル名
	final private int[]	tabCol		= {0,17};	// 表の基準点
	final private int[]	tabRow		= {44,47,50,53,56};	// 表の基準点
	private boolean bolBookCngFlg = true;			//改ファイルフラグ
	private boolean bolSheetCngFlg = true;			//改シートフラグ
	final private int intMaxSheetSr = 20;	//MAXシート数の値を入れる
	
/*
 * 	Excel編集メイン
 * 		S51Item s51Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s51_06EditExcel(S51Item s51Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		HSSFWorkbook		workbook		= null;
		HSSFSheet			workSheet		= null;
		HSSFRow				workRow			= null;
		HSSFCell			workCell		= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s51Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S51ListBean s51ListBean = new S51ListBean();
		
		try {
			int		maxSheetIndex	= 0;		// シートカウンター
			int		fileIndex		= 0;		// ファイルカウンター
			float		hensa			= 0;		// 最新模試の偏差値保持
			String		kmkCd			= "";		// 今年度の科目コード保持
			int		hyouCnt			= 0;		// 値範囲：０〜１(表カウンター)
			int		classCnt		= 0;		// 値範囲：０〜６(クラスカウンター)
			int		mshCnt			= 0;		// 値範囲：０〜６　(模試カウンター)
			int		maxMsh			= 0;		// 過回模試数
			boolean	setFlg			= true;	// *セット判断フラグ
			boolean	homeFlg			= true;	// 自校情報セット判断フラグ
			ArrayList	homeList		= new ArrayList();	//自校情報格納リスト
//add 2004/10/27 T.Sakai データ0件対応
			int		dispClassFlgCnt	= 0;		// グラフ表示フラグカウンタ
//add end
			
			// 型･科目データセット
			ArrayList	s51List		= s51Item.getS51List();
			Iterator	itr	= s51List.iterator();
			
			while( itr.hasNext() ) {
				s51ListBean = (S51ListBean)itr.next();
				
				if (s51ListBean.getIntDispKmkFlg()==1) {
					// 基本ファイルを読込む
					S51ClassListBean s51ClassListBean = new S51ClassListBean();
					
					// クラスデータセット
					ArrayList s51ClassList = s51ListBean.getS51ClassList();
					Iterator itrClass = s51ClassList.iterator();
					
					maxMsh = 0;
					
					//過回模試数を自校情報から取得
					while ( itrClass.hasNext() ){
						s51ClassListBean = (S51ClassListBean)itrClass.next();
						if ( cm.toString(s51ClassListBean.getStrGrade()).equals("") && cm.toString(s51ClassListBean.getStrClass()).equals("") ) {
							maxMsh++;
						}
					}
					itrClass = s51ClassList.iterator();
					
					setFlg = true;
					homeFlg = true;
					
					hensa = 0;
					classCnt = 0;
					String grd = "";
					String cls = "";
					homeList = new ArrayList();
	
					while ( itrClass.hasNext() ){
						s51ClassListBean = (S51ClassListBean)itrClass.next();
						
						if (s51ClassListBean.getIntDispClassFlg()==1) {
							// クラスカウント
							if ( (cm.toString(cls).equals("") && !cm.toString(s51ClassListBean.getStrClass()).equals("")) || (!cm.toString(cls).equals("") && (!cm.toString(grd).equals(cm.toString(s51ClassListBean.getStrGrade())) || !cm.toString(cls).equals(cm.toString(s51ClassListBean.getStrClass()))) ) ) {
								classCnt++;
							}
							if (classCnt>=5 && hyouCnt>=1) {
								// 同型・科目でクラスのデータが50シート目を超えたときファイル保存
								if( (!cm.toString(grd).equals(cm.toString(s51ClassListBean.getStrGrade())) || !cm.toString(cls).equals(cm.toString(s51ClassListBean.getStrClass())) )  && maxSheetIndex==intMaxSheetSr){
									// Excelファイル保存
									boolean bolRet = false;
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex+1, masterfile, maxSheetIndex);
									fileIndex++;
									if( bolRet == false ){
										return errfwrite;
									}
								}
							}
							//改表、改シートチェック
							if (classCnt>=5 && (!cm.toString(grd).equals(cm.toString(s51ClassListBean.getStrGrade())) || !cm.toString(cls).equals(cm.toString(s51ClassListBean.getStrClass()))) ) {
								hyouCnt++;
								classCnt = 0;
								homeFlg = true;
							}
							if ( hyouCnt>=2 ) {
								bolSheetCngFlg = true;
								hyouCnt = 0;
							}
							
							if(classCnt==0 && hyouCnt==0 && maxSheetIndex==intMaxSheetSr && bolSheetCngFlg){
								bolBookCngFlg = true;
							}
							//マスタExcel読み込み
							if(bolBookCngFlg){
								if((maxSheetIndex==intMaxSheetSr)||(maxSheetIndex==0)){
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook==null ){
										return errfread;
									}
									bolBookCngFlg = false;
									maxSheetIndex=0;
								}
							}
							if(bolSheetCngFlg){
								// データセットするシートの選択
								workSheet = workbook.getSheet(String.valueOf(maxSheetIndex+1));
								maxSheetIndex++;
				
								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);
			
								// セキュリティスタンプセット
								String secuFlg = cm.setSecurity( workbook, workSheet, s51Item.getIntSecuFlg() ,30 ,32 );
								workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
								workCell.setCellValue(secuFlg);
			
								// 学校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
								workCell.setCellValue( "学校名　：" + cm.toString(s51Item.getStrGakkomei()) );
			
								// 対象模試セット
								String moshi =cm.setTaisyouMoshi( s51Item.getStrMshDate() );	// 模試月取得
								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
								workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s51Item.getStrMshmei()) + moshi);
								
								//　改表フラグをFalseにする
								bolSheetCngFlg	=false;
							}
							
							// 型・科目名+配点セット
							if ( classCnt==0 ) {
								if ( !cm.toString(s51ListBean.getStrKmkCd()).equals(cm.toString(kmkCd)) || (classCnt==0 && hyouCnt==0 && homeFlg) ) {
									kmkCd = s51ListBean.getStrKmkCd();
									String haiten = "";
									if ( !cm.toString(s51ListBean.getStrHaitenKmk()).equals("") ) {
										haiten="(" + s51ListBean.getStrHaitenKmk() + ")";
									}
									workCell = cm.setCell( workSheet, workRow, workCell, 4, tabCol[hyouCnt] );
									workCell.setCellValue( "型・科目：" + cm.toString(s51ListBean.getStrKmkmei()) + haiten );
								}
							}
							
							//自校情報格納
							if ( cm.toString(s51ClassListBean.getStrGrade()).equals("") && cm.toString(s51ClassListBean.getStrClass()).equals("") ) {
								homeList.add(s51ClassListBean);
							}
							
							if ( classCnt==0 && homeFlg ) {
								//高校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[classCnt], tabCol[hyouCnt] );
								workCell.setCellValue( s51Item.getStrGakkomei() );
								//同型・科目で改表、改シートが発生したとき、最初のクラスデータには自校情報をセット
								if ( !cm.toString(s51ClassListBean.getStrGrade()).equals("") || !cm.toString(s51ClassListBean.getStrClass()).equals("") ) {
									// 自校データセット
									S51ClassListBean homeListBean = new S51ClassListBean();
									Iterator itrHome = homeList.iterator();
									int homeCol = 0;
									setFlg = true;
									while ( itrHome.hasNext() ){
										homeListBean = (S51ClassListBean)itrHome.next();
										// 模試名セット
										String moshi =cm.setTaisyouMoshi( homeListBean.getStrMshDate() );	// 模試月取得
										workCell = cm.setCell( workSheet, workRow, workCell, 43, tabCol[hyouCnt]+(maxMsh-homeCol)*2 );
										workCell.setCellValue( cm.toString(homeListBean.getStrMshmei()) + moshi );
										// 人数セット
										if ( homeListBean.getIntNinzu() != -999 ) {
											workCell = cm.setCell( workSheet, workRow, workCell, tabRow[classCnt], tabCol[hyouCnt]+(maxMsh-homeCol)*2+1 );
											workCell.setCellValue( homeListBean.getIntNinzu() );
										}
		
										// 平均点セット
										if ( homeListBean.getFloHeikin() != -999.0 ) {
											workCell = cm.setCell( workSheet, workRow, workCell, tabRow[classCnt]+1, tabCol[hyouCnt]+(maxMsh-homeCol)*2+1 );
											workCell.setCellValue( homeListBean.getFloHeikin() );
										}
		
										// 平均偏差値セット
										if ( homeListBean.getFloHensa() != -999.0 ) {
											workCell = cm.setCell( workSheet, workRow, workCell, tabRow[classCnt]+2, tabCol[hyouCnt]+(maxMsh-homeCol)*2+1 );
											workCell.setCellValue( homeListBean.getFloHensa() );
										}
										//最新模試の偏差値保持
										if ( homeListBean.getFloHensa()!=-999.0 && setFlg ) {
											hensa = homeListBean.getFloHensa();	
											setFlg = false;
										}
										// *セット
										if (homeListBean.getFloHensa() != -999.0) {
											if ( hensa < homeListBean.getFloHensa() ) {
												workCell = cm.setCell( workSheet, workRow, workCell, tabRow[classCnt]+2, tabCol[hyouCnt]+(maxMsh-homeCol)*2 );
												workCell.setCellValue("*");
											}
										}
										homeCol++;
									}
									classCnt++;
									// 自校情報セット後にはクラス名セット
									workCell = cm.setCell( workSheet, workRow, workCell, tabRow[classCnt], tabCol[hyouCnt] );
									if ( !cm.toString(s51ClassListBean.getStrGrade()).equals("") || !cm.toString(s51ClassListBean.getStrClass()).equals("") ) {
										workCell.setCellValue( cm.toString(s51ClassListBean.getStrGrade()) + "年 " + cm.toString(s51ClassListBean.getStrClass()) + "クラス");
									}
								}
								mshCnt = 0;
								setFlg = true;
							} else {
								if ( !cm.toString(grd).equals(cm.toString(s51ClassListBean.getStrGrade())) || !cm.toString(cls).equals(cm.toString(s51ClassListBean.getStrClass())) ) {
									// クラス名セット
									workCell = cm.setCell( workSheet, workRow, workCell, tabRow[classCnt], tabCol[hyouCnt] );
									if ( !cm.toString(s51ClassListBean.getStrGrade()).equals("") || !cm.toString(s51ClassListBean.getStrClass()).equals("") ) {
										workCell.setCellValue( cm.toString(s51ClassListBean.getStrGrade()) + "年 " + cm.toString(s51ClassListBean.getStrClass()) + "クラス");
									}
									mshCnt = 0;
									setFlg = true;
								}
							}
		
							
							if ( classCnt==0 && cm.toString(grd).equals(cm.toString(s51ClassListBean.getStrGrade())) && cm.toString(cls).equals(cm.toString(s51ClassListBean.getStrClass())) ) {
								// 模試名セット
								String moshi =cm.setTaisyouMoshi( s51ClassListBean.getStrMshDate() );	// 模試月取得
								workCell = cm.setCell( workSheet, workRow, workCell, 43, tabCol[hyouCnt]+(maxMsh-mshCnt)*2 );
								workCell.setCellValue( cm.toString(s51ClassListBean.getStrMshmei()) + moshi );
							}
							//最新模試の偏差値保持
							if ( s51ClassListBean.getFloHensa()!=-999.0 && setFlg ) {
								hensa = s51ClassListBean.getFloHensa();	
								setFlg = false;
							}
							
							// *セット
							if (s51ClassListBean.getFloHensa() != -999.0) {
								if ( hensa < s51ClassListBean.getFloHensa() ) {
									workCell = cm.setCell( workSheet, workRow, workCell, tabRow[classCnt]+2, tabCol[hyouCnt]+(maxMsh-mshCnt)*2 );
									workCell.setCellValue("*");
								}
							}
							
							// 人数セット
							if ( s51ClassListBean.getIntNinzu() != -999 ) {
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[classCnt], tabCol[hyouCnt]+1+(maxMsh-mshCnt)*2 );
								workCell.setCellValue( s51ClassListBean.getIntNinzu() );
							}
			
							// 平均点セット
							if ( s51ClassListBean.getFloHeikin() != -999.0 ) {
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[classCnt]+1, tabCol[hyouCnt]+1+(maxMsh-mshCnt)*2 );
								workCell.setCellValue( s51ClassListBean.getFloHeikin() );
							}
			
							// 平均偏差値セット
							if ( s51ClassListBean.getFloHensa() != -999.0 ) {
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[classCnt]+2, tabCol[hyouCnt]+1+(maxMsh-mshCnt)*2 );
								workCell.setCellValue( s51ClassListBean.getFloHensa() );
							}
							mshCnt++;
							homeFlg = false;
							grd = s51ClassListBean.getStrGrade();
							cls = s51ClassListBean.getStrClass();
//add 2004/10/27 T.Sakai データ0件対応
							dispClassFlgCnt++;
//add end
						}
					}
					hyouCnt++;
					
					// 50シート以上データが存在するときファイル保存
					if(hyouCnt >= 2 && itr.hasNext() && maxSheetIndex==intMaxSheetSr){
	
						// Excelファイル保存
						boolean bolRet = false;
						bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex+1, masterfile, maxSheetIndex);
						fileIndex++;
						if( bolRet == false ){
							return errfwrite;
						}
					}
				}
			}
//add 2004/10/27 T.Sakai データ0件対応
			if ( dispClassFlgCnt==0 ) {
				//マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				maxSheetIndex=0;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet(String.valueOf(maxSheetIndex+1));
				maxSheetIndex++;
				
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
			
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s51Item.getIntSecuFlg() ,30 ,32 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
				workCell.setCellValue(secuFlg);
			
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s51Item.getStrGakkomei()) );
			
				// 対象模試セット
				String moshi =cm.setTaisyouMoshi( s51Item.getStrMshDate() );	// 模試月取得
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s51Item.getStrMshmei()) + moshi);
			}
//add end
			// Excelファイル保存
			boolean bolRet = false;
			if(fileIndex != 0){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex+1, masterfile, maxSheetIndex);
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;					
			}
		
		} catch(Exception e) {
			log.Err("S51_06","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

}
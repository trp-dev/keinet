package jp.co.fj.keinavi.excel.school;

//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.io.FileInputStream;
//import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S44GakkoListBean;
import jp.co.fj.keinavi.excel.data.school.S44HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.school.S44Item;
import jp.co.fj.keinavi.excel.data.school.S44ListBean;

import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
//import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 * 作成日: 2004/09/06
 * @author Ito.Y
 *
 * 校内成績分析−過年度比較−志望大学評価別人数　大学（日程あり）出力処理
 * 
 */
public class S44_02 {
	
	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private String		strArea			= "A1:AH58";	//印刷範囲	
	private String		mastersheet 	= "tmp";		//テンプレートシート名
	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow		= 50;			//MAX行数
	private int		intMaxClassSr   = 5;			//MAXクラス数

	private int intDataStartRow = 8;					//データセット開始行
	private int intDataStartCol = 9;					//データセット開始列
	
	final private String masterfile0 = "S44_02" ;
	final private String masterfile1 = "S44_02" ;
	private String masterfile = "" ;
	
	final private String strCenterResearch = "38";

	/*
	 * 	Excel編集メイン
	 * 		S44Item S44Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int s44_02EditExcel(S44Item s44Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		//テンプレートの決定
		if (s44Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		/** セット処理 **/
		boolean bolBookCngFlg = true;			//改ファイルフラグ	
		boolean bolSheetCngFlg = true;			//改シートフラグ
			
		FileInputStream	fin = null;
		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFCell	workCell		= null;
		HSSFRow		workRow			= null;
		HSSFWorkbook	tmpworkbook = null;
		
		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intRow = 0;
		int intCol = 0;

		String strGenekiKbn = "";
		String strDaigakuCode = "";
		String strNtiCode = "";
		String strHyoukaKbn = "";
		String strNendo = "";
		
		//2004.09.27Add
		String strKokushiKbn = "";
		String strStKokushiKbn = "";
		//2004.09.27AddEnd

		int intLoopCnt = 0;
		int intNinzuListCnt = 0;
		int intSheetCngCount = 0;
		int intBookCngCount = 1;
		int intChangeSheetRow = 0;
		
		boolean bolDispHyoukaFlg = false;
		boolean bolDispNtiFlg = false;
		boolean bolDispDaigakuFlg = false;
		String strHyouka = "";
		
		String strDispTarget = "";
		
		ArrayList WorkBookList = new ArrayList();
		ArrayList WorkSheetList = new ArrayList();
		
		int intSheetCnt=0;
		
		int intBookIndex = -1;
		int intSheetIndex = -1;
		
		int intSheetMei = -1;
		
		int intSetRow = intDataStartRow;
		
		boolean bolClassDispFlg=true;
		
		KNLog log = KNLog.getInstance(null,null,null);
		ArrayList HyoukaTitleList = new ArrayList();
		
		// 基本ファイルを読込む
		S44ListBean s44ListBean = new S44ListBean();
		
		try{
			/** S44Item編集 **/
			s44Item  = editS44( s44Item );
			
			if(cm.toString(s44Item.getStrMshCd()).equals(cm.toString(strCenterResearch))){
				HyoukaTitleList.add(0,"◎");
				HyoukaTitleList.add(1,"○");
				HyoukaTitleList.add(2,"△");
			}else{
				HyoukaTitleList.add(0,"A");
				HyoukaTitleList.add(1,"B");
				HyoukaTitleList.add(2,"C");
			}

			log.Ep("S44_01","S44_01帳票作成開始","");

			// 基本ファイルを読込む
			ArrayList s44List = s44Item.getS44List();
			ListIterator itr = s44List.listIterator();
			
//			if( itr.hasNext() == false ){
//				log.Err("99S44_02","帳票作成エラー","");
//				return errfdata;
//			}

			/** 大学・日程・評価 **/
			// S44ListBean 
			while( itr.hasNext() ){
				s44ListBean = (S44ListBean)itr.next();
				
				// 大学名
				bolDispDaigakuFlg = false;
				if( !cm.toString(strDaigakuCode).equals(cm.toString(s44ListBean.getStrDaigakuCd()))){
					bolDispDaigakuFlg = true;
					strNtiCode = "";
					strHyoukaKbn = "";
					//大学名表示
					strDaigakuCode = s44ListBean.getStrDaigakuCd();
				}
				
				// 日程
				bolDispNtiFlg = false;
				if( !cm.toString(strNtiCode).equals(cm.toString(s44ListBean.getStrNtiCd())) ){
					bolDispNtiFlg = true;
					strHyoukaKbn = "";
					strNtiCode = s44ListBean.getStrNtiCd();
				}

				// 評価区分
				bolDispHyoukaFlg = false;
				if( !cm.toString(strHyoukaKbn).equals(cm.toString(s44ListBean.getStrHyouka())) ){
					bolDispHyoukaFlg = true;
					//評価区分表示
					strHyoukaKbn = s44ListBean.getStrHyouka();
					strHyouka = "";
					switch( Integer.parseInt(strHyoukaKbn) ){
						case 1:
							strHyouka = "センター";
							break;
						case 2:
							strHyouka = "2次・私大";
							break;
						case 3:
							strHyouka = "総合";
							break;
					}
				}

				// 表示対象
				if( !cm.toString(strGenekiKbn).equals(cm.toString(s44ListBean.getStrGenKouKbn())) ){
					strGenekiKbn = s44ListBean.getStrGenKouKbn();
					if( cm.toString(strGenekiKbn).equals("0") ){
						strDispTarget = "全体";
					}
					else if( cm.toString(strGenekiKbn).equals("1") ){
						strDispTarget = "現役生";
					}
					else if( cm.toString(strGenekiKbn).equals("2") ){
						strDispTarget = "高卒生";
					}

					if( bolSheetCngFlg == false ){
						bolSheetCngFlg = true;
					}
				}
				
				//2004.9.27 Add
				int intDaigakuCd = (int)Integer.parseInt(s44ListBean.getStrDaigakuCd());
				if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
					strKokushiKbn = "国公立大";
				}
				if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
					strKokushiKbn = "私立大";
				}
				if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
					strKokushiKbn = "短大";
				}
				if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
					strKokushiKbn = "その他";
				}
				
				if( (!cm.toString(strKokushiKbn).equals(cm.toString(strStKokushiKbn))) ){
					strStKokushiKbn = strKokushiKbn;
					bolSheetCngFlg = true;
				}
				//2004.9.27 Add End
				
				//クラス別評価人数リスト読み込み クラス名をExcelへセット
				ArrayList s44GakkoList = s44ListBean.getS44GakkoList();
				ListIterator itr2 = s44GakkoList.listIterator();

				// Listの取得
				S44GakkoListBean s44GakkoListBean = new S44GakkoListBean();
				S44GakkoListBean s44GakkoListBeanHomeData = new S44GakkoListBean();
				
				S44GakkoListBean HomeDataBean = new S44GakkoListBean();

				//クラス数取得
				int intClassSr = s44GakkoList.size();
				
				int intSheetSr = 0;
				if( (intClassSr - 1) % intMaxClassSr > 0 ){
					intSheetSr = (int)Math.ceil((intClassSr - 1) / intMaxClassSr) + 1;
				}else{
					intSheetSr = (int)Math.ceil((intClassSr - 1) / intMaxClassSr);
				}
				
				int intLoopCnt2 = 0;
				int intNendoCount = 0;
				intChangeSheetRow = 0;
				
				boolean bolHomeDataDispFlg=false; 
				/** 学校名・学年・クラス **/
				// S44GakkoListBean
				while( itr2.hasNext() ){
					String strClassmei = "";
					s44GakkoListBean = (S44GakkoListBean)itr2.next();

					// Listの取得
					ArrayList s44HyoukaNinzuList = s44GakkoListBean.getS44HyoukaNinzuList();
					S44HyoukaNinzuListBean s44HyoukaNinzuListBean = new S44HyoukaNinzuListBean();
					ListIterator itr3 = s44HyoukaNinzuList.listIterator();

					// 自校情報表示判定
					if( intLoopCnt2 == 0 ){
						bolHomeDataDispFlg = true;
						HomeDataBean = s44GakkoListBean;
						s44GakkoListBean = (S44GakkoListBean)itr2.next();
						intNendoCount = s44HyoukaNinzuList.size();
						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
						intLoopCnt2++;

						s44HyoukaNinzuList = s44GakkoListBean.getS44HyoukaNinzuList();
						s44HyoukaNinzuListBean = new S44HyoukaNinzuListBean();
						itr3 = s44HyoukaNinzuList.listIterator();
					}

					// 始点情報（Col）
					if( intLoopCnt2 % intMaxClassSr == 0 ){
						intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 5 );
					}else{
						intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 5 );
					}
					
//					intCol = intDataStartCol + ( ( intMaxClassSr - ( intLoopCnt2 % intMaxClassSr ) ) * 5 );

					/** 年度・志望者数 **/
					// S44HyoukaNinzuListBean
					int intHyoukaNinzuListIndex = 0;
					while( itr3.hasNext() ){
						s44HyoukaNinzuListBean = (S44HyoukaNinzuListBean)itr3.next();

						// 規定行数を表示したら線を引く
						if( (intSetRow  >= intChangeSheetRow) ||((bolSheetCngFlg == true)&&(intMaxSheetIndex != 0))){
							bolSheetCngFlg = true;
							for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
								int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
								workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
								workSheet = (HSSFSheet)WorkSheetList.get(intSindex);
								
								// 罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intSetRow, intSetRow, 0, 33, false, true, false, false );
								if( workSheet != null ){
									int intRemoveStartRow = intChangeSheetRow;
									if( intMaxRow + intDataStartRow > intRemoveStartRow ){
										for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
											//行削除処理
											workSheet.removeRow(workSheet.getRow(index));
										}
										//罫線出力（セルの上部に太線を出力）
										cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
														intRemoveStartRow, intRemoveStartRow, 0, 33, false, true, false, false);
									}
								}
			
							}

							// 保存処理
							if( ((HSSFWorkbook)WorkBookList.get(0)).getNumberOfSheets() > intMaxSheetSr ){
								tmpworkbook = (HSSFWorkbook)WorkBookList.get(0);
				
								boolean bolRet = false;
								// Excelファイル保存
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, tmpworkbook, UserID, intBookCngCount, masterfile, intMaxSheetSr);

								if( bolRet == false ){
									log.Err("0"+bolRet+"S44_02","帳票作成エラー","");
									return errfwrite;
								}
								intBookCngCount++;
								
								// 削除
								WorkBookList.remove(0);
								for(int a=0; a<intMaxSheetSr;a++){
									WorkSheetList.remove(0);
								}
								tmpworkbook=null;
								intBookIndex--;
								intSheetIndex -= intMaxSheetSr;
							}
						}

						// 改シート判定
						if( bolSheetCngFlg == true ){
							
							bolDispHyoukaFlg = true;
							bolDispDaigakuFlg = true;
							bolDispNtiFlg = true;
							
							intSetRow = intDataStartRow;
							bolClassDispFlg = true;
							bolHomeDataDispFlg = true;
							
							for( int intIndex = 0; intIndex < intSheetSr; intIndex ++ ){
								
								if( ( intMaxSheetIndex >= intMaxSheetSr ) || ( bolBookCngFlg == true ) ){
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook.equals(null) ){
										log.Err("99S44_02","帳票作成エラー","");
										return errfread;
									}
									WorkBookList.add(workbook);
									intBookIndex++;
					
									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
									workbook = (HSSFWorkbook)WorkBookList.get(intBookIndex);
								}
								
								//シート作成
								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);
								WorkSheetList.add(workSheet);
								intSheetIndex++;
								workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex);

								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);
				
								// セキュリティスタンプセット
								String secuFlg = cm.setSecurity( workbook, workSheet, s44Item.getIntSecuFlg(), 31, 33 );
								workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
								workCell.setCellValue(secuFlg);
				
								// 学校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
								workCell.setCellValue( "学校名　：" + s44Item.getStrGakkomei() );
		
								// 模試月取得
								String moshi =cm.setTaisyouMoshi( s44Item.getStrMshDate() );
								
								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
								workCell.setCellValue( "対象模試：" + s44Item.getStrMshmei() + moshi);
					
								//表示対象
								workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
								workCell.setCellValue( "表示対象：" + strDispTarget );
								
								//評価別人数 見出し行
								for( int i = 0; i <= intMaxClassSr; i++ ){
//								for( int i = 0; i <= intClassSr-1; i++ ){
									int intTitleSetCol = (i * 5) + ( intDataStartCol - 3 );
									
									//出願予定・第1志望
									if (strKokushiKbn == "国公立大") { 
										workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
										workCell.setCellValue( "出願予定" );
									} else {
										workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
										workCell.setCellValue( "第１志望" );
									}
								
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol );
									workCell.setCellValue( (String)HyoukaTitleList.get(0) );
									
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+1 );
									workCell.setCellValue( (String)HyoukaTitleList.get(1) );
									
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+2 );
									workCell.setCellValue( (String)HyoukaTitleList.get(2) );
								}
							
								intMaxSheetIndex++;		//シート数カウンタインクリメント
								bolSheetCngFlg = false;
						
								intLoopCnt = 0;
							}	//ForEnd
							
						}	//if( bolSheetCngFlg == true )
						
						intSheetCnt = (int)Math.ceil( (double)intLoopCnt2 / (double)intMaxClassSr );
						workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex -  (intSheetSr - intSheetCnt)  );

						int intSheetNo = intSheetIndex -  (intSheetSr - intSheetCnt);
						int intBIndex = (int)Math.ceil((double)(intSheetNo + 1) / (double)intMaxSheetSr );
						workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
										

						
						if( intCol == intDataStartCol ){
							
							//データセット
							//自校データ・大学名ほか表示
							//自校学校名
							workCell = cm.setCell( workSheet, workRow, workCell, 5, 4 );
							workCell.setCellValue(HomeDataBean.getStrGakkomei());
							//自校データ
							ListIterator itrHome = HomeDataBean.getS44HyoukaNinzuList().listIterator();
							int intLoopIndex = 0;
							while( itrHome.hasNext() ){
								S44HyoukaNinzuListBean HomeHyoukaNinzuListBean = (S44HyoukaNinzuListBean) itrHome.next();
								boolean ret = setData( workSheet, HomeHyoukaNinzuListBean, intSetRow + intLoopIndex , 4 );
								if( ret == false ){
									log.Err("0"+ret+"S44_02","帳票作成エラー","");
									return errfdata;
								}
								intLoopIndex++;
							}
							
							if( bolDispHyoukaFlg == true ){
								//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
															intSetRow, intSetRow, 2, 2, false, true, false, false);
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( strHyouka );
				
							}
							
							if( bolDispNtiFlg == true ){
								//罫線出力(セルの上部（日程の列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
															intSetRow, intSetRow, 1, 2, false, true, false, false);
								//日程表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
								workCell.setCellValue( s44ListBean.getStrNittei() );
				
							}

							if( bolDispDaigakuFlg == true ){
								//罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intSetRow, intSetRow, 0, 33, false, true, false, false );
								//大学名表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 0 );
								workCell.setCellValue(s44ListBean.getStrDaigakuMei());
							}
							
						}
					

						bolHomeDataDispFlg = false;

						boolean ret = setData( workSheet, s44HyoukaNinzuListBean, intSetRow + intHyoukaNinzuListIndex, intCol);
						if( ret != true){
							return errfdata;
						}
						else{
							intHyoukaNinzuListIndex++;
							intLoopCnt++;
						}
						
						//
						
					}	//WhileEnd( itr3 )
					
					
					//クラス名
					if( bolClassDispFlg == true ){
						String strGakko = s44GakkoListBean.getStrGakkomei();
						workCell = cm.setCell( workSheet, workRow, workCell, 5, intCol );
						workCell.setCellValue(strGakko);
					}
					intLoopCnt2++;
				}	//WhileEnd( itr2 )

				intSetRow += intNendoCount;
				bolClassDispFlg = false;

			}	//WhileEnd( itr )
			
			for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
				int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
				workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
				workSheet = (HSSFSheet)WorkSheetList.get(intSindex);
				
				//罫線出力(セルの上部に太線を引く）
				cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
													intSetRow, intSetRow, 0, 33, false, true, false, false );
				if( workSheet != null ){
					int intRemoveStartRow = intChangeSheetRow;
					if( intMaxRow + intDataStartRow > intRemoveStartRow ){
						for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
							//行削除処理
							workSheet.removeRow(workSheet.getRow(index));
						}
						//罫線出力（セルの上部に太線を出力）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
										intRemoveStartRow, intRemoveStartRow, 0, 33, false, true, false, false);
					}
				}
			
			}

			//ファイル保存
			for( int intBookSr = 0; intBookSr < WorkBookList.size(); intBookSr++ ){
				workbook = (HSSFWorkbook)WorkBookList.get(intBookSr);
				
				boolean bolRet = false;
				// Excelファイル保存
				if( (intBookCngCount != 1)||((intBookCngCount == 1)&&(WorkBookList.size()>1)) ){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, intMaxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}

				if( bolRet == false ){
					log.Err("0"+bolRet+"S44_02","帳票作成エラー","");
					return errfwrite;
				}
				intBookCngCount++;
			}
			
			//データ無し
			if ( WorkBookList.size() == 0 ){
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex = 1;


				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s44Item.getIntSecuFlg(), 31, 33 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
				workCell.setCellValue(secuFlg);
				
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s44Item.getStrGakkomei()) );
		
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s44Item.getStrMshDate() );
								
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
				workCell.setCellValue( "対象模試：" + s44Item.getStrMshmei() + moshi);

				//表示対象
				workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
				workCell.setCellValue( "表示対象：" );
								
				boolean bolRet = false;
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}
			
		}
		catch( Exception e ){
			e.printStackTrace();
			log.Err("99S44","帳票作成エラー",e.toString());
			return errfdata;
		}

		log.Ep("S44_02","S44_02帳票作成終了","");

		return noerror;
	}

	/**
	 * S44Item加算・編集
	 * @param s44Item
	 * @return
	 */
	private S44Item editS44( S44Item s44Item ) throws Exception{
		try{
			S44Item s44ItemEdt = new S44Item();
			String daigakuCd = "";
			String gakubuCd = "";
			String gakkaCd = "";
			String genekiKbn = "";
			boolean firstFlg = true;
			boolean centerFlg = false;
			boolean secondFlg = false;
			boolean totalFlg = false;

			ArrayList s44GakkoListAdd = new ArrayList();
			ArrayList s44GakkoListAddCnt = new ArrayList();
			ArrayList s44GakkoListAddSnd = new ArrayList();
			ArrayList s44GakkoListAddTtl = new ArrayList();
			ArrayList s44NinzuListAdd = new ArrayList();

			s44ItemEdt.setStrGakkomei( s44Item.getStrGakkomei() );
			s44ItemEdt.setStrMshmei( s44Item.getStrMshmei() );
			s44ItemEdt.setStrMshDate( s44Item.getStrMshDate() );
			s44ItemEdt.setStrMshCd( s44Item.getStrMshCd() );
			s44ItemEdt.setIntSecuFlg( s44Item.getIntSecuFlg() );
			s44ItemEdt.setIntDaiTotalFlg( s44Item.getIntDaiTotalFlg() );

			S44ListBean s44ListBeanAddCnt = new S44ListBean();
			S44ListBean s44ListBeanAddSnd = new S44ListBean();
			S44ListBean s44ListBeanAddTtl = new S44ListBean();
			/** 大学・学部・学科・日程・評価 **/
			ArrayList s44List = s44Item.getS44List();
			ArrayList s44ListEdt = new ArrayList();
			ListIterator itr = s44List.listIterator();
			while( itr.hasNext() ){
				S44ListBean s44ListBean = (S44ListBean) itr.next();
				S44ListBean s44ListBeanEdt = new S44ListBean();

				// 大学コードが変わる→小計の保存
//////////////////
//				if( !cm.toString(daigakuCd).equals( cm.toString(s44ListBean.getStrDaigakuCd()) ) ){
//////////////////
				if( (!cm.toString(daigakuCd).equals( cm.toString(s44ListBean.getStrDaigakuCd())))
					||( !cm.toString(genekiKbn).equals(cm.toString(s44ListBean.getStrGenKouKbn()))) ){
					genekiKbn = s44ListBean.getStrGenKouKbn();

					daigakuCd = s44ListBean.getStrDaigakuCd();

					if(centerFlg == true){
						s44ListBeanAddCnt.setS44GakkoList( s44GakkoListAddCnt );
						s44ListEdt.add( s44ListBeanAddCnt );
						s44GakkoListAddCnt = new ArrayList();
						centerFlg = false;
					}
					if(secondFlg == true){
						s44ListBeanAddSnd.setS44GakkoList( s44GakkoListAddSnd );
						s44ListEdt.add( s44ListBeanAddSnd );
						s44GakkoListAddSnd = new ArrayList();
						secondFlg = false;
					}
					if(totalFlg == true){
						s44ListBeanAddTtl.setS44GakkoList( s44GakkoListAddTtl );
						s44ListEdt.add( s44ListBeanAddTtl );
						s44GakkoListAddTtl = new ArrayList();
						totalFlg = false;
					}
					s44ListBeanAddCnt = new S44ListBean();
					s44ListBeanAddSnd = new S44ListBean();
					s44ListBeanAddTtl = new S44ListBean();
					s44NinzuListAdd = new ArrayList();
					firstFlg = false;
				}

				// 基本部分の移し変え
				s44ListBeanEdt.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
				s44ListBeanEdt.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
				s44ListBeanEdt.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
				s44ListBeanEdt.setStrGakubuCd( s44ListBean.getStrGakubuCd() );
				s44ListBeanEdt.setStrGakubuMei( s44ListBean.getStrGakubuMei() );
				s44ListBeanEdt.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
				s44ListBeanEdt.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
				s44ListBeanEdt.setStrNtiCd( s44ListBean.getStrNtiCd() );
				s44ListBeanEdt.setStrNittei( s44ListBean.getStrNittei() );
				s44ListBeanEdt.setStrHyouka( s44ListBean.getStrHyouka() );

				// 小計値の保存部
				switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
					case 1:
						s44ListBeanAddCnt.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanAddCnt.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanAddCnt.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanAddCnt.setStrGakubuCd( s44ListBean.getStrGakubuCd() );
						s44ListBeanAddCnt.setStrGakubuMei( s44ListBean.getStrGakubuMei() );
						s44ListBeanAddCnt.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanAddCnt.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanAddCnt.setStrNtiCd( "NTI-SYOU" );
						s44ListBeanAddCnt.setStrNittei( "大学計" );
						s44ListBeanAddCnt.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListAdd = deepCopyCl( s44GakkoListAddCnt );
						centerFlg = true;
						break;
					case 2:
						s44ListBeanAddSnd.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanAddSnd.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanAddSnd.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanAddSnd.setStrGakubuCd( s44ListBean.getStrGakubuCd() );
						s44ListBeanAddSnd.setStrGakubuMei( s44ListBean.getStrGakubuMei() );
						s44ListBeanAddSnd.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanAddSnd.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanAddSnd.setStrNtiCd( "NTI-SYOU" );
						s44ListBeanAddSnd.setStrNittei( "大学計" );
						s44ListBeanAddSnd.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListAdd = deepCopyCl( s44GakkoListAddSnd );
						secondFlg = true;
						break;
					case 3:
						s44ListBeanAddTtl.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanAddTtl.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanAddTtl.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanAddTtl.setStrGakubuCd( s44ListBean.getStrGakubuCd() );
						s44ListBeanAddTtl.setStrGakubuMei( s44ListBean.getStrGakubuMei() );
						s44ListBeanAddTtl.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanAddTtl.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanAddTtl.setStrNtiCd( "NTI-SYOU" );
						s44ListBeanAddTtl.setStrNittei( "大学計" );
						s44ListBeanAddTtl.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListAdd = deepCopyCl( s44GakkoListAddTtl );
						totalFlg = true;
						break;
				}

				// 
				ArrayList s44GakkoList = s44ListBean.getS44GakkoList();
				ArrayList s44GakkoListEdt = new ArrayList();
				ListIterator itrClass = s44GakkoList.listIterator();
				int b = 0;
				boolean firstFlg3 = true;
				if(s44GakkoListAdd.size() == 0){
					firstFlg3 = false;
				}
				/** 学校・学年・クラス **/
				while( itrClass.hasNext() ){
					S44GakkoListBean s44GakkoListBean = (S44GakkoListBean)itrClass.next();
					S44GakkoListBean s44GakkoListBeanEdt = new S44GakkoListBean();
					S44GakkoListBean s44GakkoListBeanAdd = null;

					// 移し変え
					s44GakkoListBeanEdt.setStrGakkomei( s44GakkoListBean.getStrGakkomei() );

					// 小計用データ呼び出し
					if(firstFlg3 == true){
						s44GakkoListBeanAdd = (S44GakkoListBean) s44GakkoListAdd.get(b);
						s44NinzuListAdd = deepCopyNz( s44GakkoListBeanAdd.getS44HyoukaNinzuList() );
					}else{
						s44GakkoListBeanAdd = new S44GakkoListBean();

						s44GakkoListBeanAdd.setStrGakkomei( s44GakkoListBean.getStrGakkomei() );
					}

					/** 年度・人数 **/
					ArrayList s44NinzuList = s44GakkoListBean.getS44HyoukaNinzuList();
					ArrayList s44NinzuListEdt = new ArrayList();
					ListIterator itrNinzu = s44NinzuList.listIterator();
					int a = 0;
					boolean firstFlg2 = true;
					if(s44NinzuListAdd.size() == 0){
						firstFlg2 = false;
					}
					while( itrNinzu.hasNext() ){
						S44HyoukaNinzuListBean s44NinzuBean = (S44HyoukaNinzuListBean)itrNinzu.next();
						S44HyoukaNinzuListBean s44NinzuBeanEdt = new S44HyoukaNinzuListBean();
						S44HyoukaNinzuListBean s44NinzuBeanAdd = null;

						// 移し変え
						s44NinzuBeanEdt.setStrNendo( s44NinzuBean.getStrNendo() );
						s44NinzuBeanEdt.setIntSoshibo( s44NinzuBean.getIntSoshibo() );
						s44NinzuBeanEdt.setIntDai1shibo( s44NinzuBean.getIntDai1shibo() );
						s44NinzuBeanEdt.setIntHyoukaA( s44NinzuBean.getIntHyoukaA() );
						s44NinzuBeanEdt.setIntHyoukaB( s44NinzuBean.getIntHyoukaB() );
						s44NinzuBeanEdt.setIntHyoukaC( s44NinzuBean.getIntHyoukaC() );

						s44NinzuListEdt.add( s44NinzuBeanEdt );

						// 加算処理
						if(firstFlg2 == true){
							s44NinzuBeanAdd = (S44HyoukaNinzuListBean) s44NinzuListAdd.get(a);
						}else{
							s44NinzuBeanAdd = new S44HyoukaNinzuListBean();

							s44NinzuBeanAdd.setStrNendo( s44NinzuBean.getStrNendo() );

//							s44NinzuBeanAdd.setIntSoshibo( -999 );
//							s44NinzuBeanAdd.setIntDai1shibo( -999 );
//							s44NinzuBeanAdd.setIntHyoukaA( -999 );
//							s44NinzuBeanAdd.setIntHyoukaB( -999 );
//							s44NinzuBeanAdd.setIntHyoukaC( -999 );

						}

						if(s44NinzuBean.getIntSoshibo()!=-999){
//							s44NinzuBeanAdd.setIntSoshibo( s44NinzuBeanAdd.getIntSoshibo() + s44NinzuBean.getIntSoshibo() );
							if(s44NinzuBeanAdd.getIntSoshibo()!=-999){
								s44NinzuBeanAdd.setIntSoshibo(s44NinzuBeanAdd.getIntSoshibo() + s44NinzuBean.getIntSoshibo());
							}else{
								s44NinzuBeanAdd.setIntSoshibo(s44NinzuBean.getIntSoshibo());
							}
						}
						if(s44NinzuBean.getIntDai1shibo()!=-999){
//							s44NinzuBeanAdd.setIntDai1shibo( s44NinzuBeanAdd.getIntDai1shibo() + s44NinzuBean.getIntDai1shibo() );
							if(s44NinzuBeanAdd.getIntDai1shibo()!=-999){
								s44NinzuBeanAdd.setIntDai1shibo(s44NinzuBeanAdd.getIntDai1shibo() + s44NinzuBean.getIntDai1shibo());
							}else{
								s44NinzuBeanAdd.setIntDai1shibo(s44NinzuBean.getIntDai1shibo());
							}
						}
						if(s44NinzuBean.getIntHyoukaA()!=-999){
//							s44NinzuBeanAdd.setIntHyoukaA( s44NinzuBeanAdd.getIntHyoukaA() + s44NinzuBean.getIntHyoukaA() );
							if(s44NinzuBeanAdd.getIntHyoukaA()!=-999){
								s44NinzuBeanAdd.setIntHyoukaA(s44NinzuBeanAdd.getIntHyoukaA() + s44NinzuBean.getIntHyoukaA());
							}else{
								s44NinzuBeanAdd.setIntHyoukaA(s44NinzuBean.getIntHyoukaA());
							}
						}
						if(s44NinzuBean.getIntHyoukaB()!=-999){
//							s44NinzuBeanAdd.setIntHyoukaB( s44NinzuBeanAdd.getIntHyoukaB() + s44NinzuBean.getIntHyoukaB() );
							if(s44NinzuBeanAdd.getIntHyoukaB()!=-999){
								s44NinzuBeanAdd.setIntHyoukaB(s44NinzuBeanAdd.getIntHyoukaB() + s44NinzuBean.getIntHyoukaB());
							}else{
								s44NinzuBeanAdd.setIntHyoukaB(s44NinzuBean.getIntHyoukaB());
							}
						}
						if(s44NinzuBean.getIntHyoukaC()!=-999){
//							s44NinzuBeanAdd.setIntHyoukaC( s44NinzuBeanAdd.getIntHyoukaC() + s44NinzuBean.getIntHyoukaC() );
							if(s44NinzuBeanAdd.getIntHyoukaC()!=-999){
								s44NinzuBeanAdd.setIntHyoukaC(s44NinzuBeanAdd.getIntHyoukaC() + s44NinzuBean.getIntHyoukaC());
							}else{
								s44NinzuBeanAdd.setIntHyoukaC(s44NinzuBean.getIntHyoukaC());
							}
						}

						if(firstFlg2 == true){
							s44NinzuListAdd.set(a,s44NinzuBeanAdd);
						}else{
							s44NinzuListAdd.add(s44NinzuBeanAdd);
						}
						a++;
					}

					// 格納
					s44GakkoListBeanEdt.setS44HyoukaNinzuList( s44NinzuListEdt );
					s44GakkoListEdt.add( s44GakkoListBeanEdt );

					// 計算結果格納
					s44GakkoListBeanAdd.setS44HyoukaNinzuList( deepCopyNz(s44NinzuListAdd) );
					s44NinzuListAdd = new ArrayList();
					if(firstFlg3 == true){
						s44GakkoListAdd.set(b,s44GakkoListBeanAdd);
					}else{
						s44GakkoListAdd.add(s44GakkoListBeanAdd);
					}
					b++;
				}
				// 格納
				s44ListBeanEdt.setS44GakkoList( s44GakkoListEdt );
				s44ListEdt.add( s44ListBeanEdt );
				switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
					case 1:
						s44GakkoListAddCnt = deepCopyCl( s44GakkoListAdd );
						break;
					case 2:
						s44GakkoListAddSnd = deepCopyCl( s44GakkoListAdd );
						break;
					case 3:
						s44GakkoListAddTtl = deepCopyCl( s44GakkoListAdd );
						break;
				}
				s44GakkoListAdd = new ArrayList();
			}


			// 大学コードが変わる→小計の保存
			if(centerFlg == true){
				s44ListBeanAddCnt.setS44GakkoList( s44GakkoListAddCnt );
				s44ListEdt.add( s44ListBeanAddCnt );
			}
			if(secondFlg == true){
				s44ListBeanAddSnd.setS44GakkoList( s44GakkoListAddSnd );
				s44ListEdt.add( s44ListBeanAddSnd );
			}
			if(totalFlg == true){
				s44ListBeanAddTtl.setS44GakkoList( s44GakkoListAddTtl );
					s44ListEdt.add( s44ListBeanAddTtl );
			}


			// 格納
			s44ItemEdt.setS44List( s44ListEdt );

			return s44ItemEdt;
		}catch(Exception e){
			e.toString();
			throw e;
		}
	}

	/**
	 * 
	 * @param s44NinzuList
	 * @return
	 */
	private ArrayList deepCopyNz(ArrayList s44NinzuList){
		ListIterator itrNinzu = s44NinzuList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrNinzu.hasNext()){
			S44HyoukaNinzuListBean s44NinzuBean = (S44HyoukaNinzuListBean)itrNinzu.next();
			S44HyoukaNinzuListBean s44NinzuBeanCopy = new S44HyoukaNinzuListBean();

			s44NinzuBeanCopy.setStrNendo( s44NinzuBean.getStrNendo() );
			s44NinzuBeanCopy.setIntSoshibo( s44NinzuBean.getIntSoshibo() );
			s44NinzuBeanCopy.setIntDai1shibo( s44NinzuBean.getIntDai1shibo() );
			s44NinzuBeanCopy.setIntHyoukaA( s44NinzuBean.getIntHyoukaA() );
			s44NinzuBeanCopy.setIntHyoukaB( s44NinzuBean.getIntHyoukaB() );
			s44NinzuBeanCopy.setIntHyoukaC( s44NinzuBean.getIntHyoukaC() );

			copyList.add( s44NinzuBeanCopy );
		}
		
		return copyList;
	}

	/**
	 * 
	 * @param s44GakkoList
	 * @return
	 */
	private ArrayList deepCopyCl(ArrayList s44GakkoList){
		ListIterator itrClass = s44GakkoList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrClass.hasNext()){
			S44GakkoListBean s44ClassBean = (S44GakkoListBean)itrClass.next();
			S44GakkoListBean s44ClassBeanCopy = new S44GakkoListBean();

			s44ClassBeanCopy.setStrGakkomei( s44ClassBean.getStrGakkomei() );
			s44ClassBeanCopy.setS44HyoukaNinzuList( deepCopyNz(s44ClassBean.getS44HyoukaNinzuList()) );

			copyList.add( s44ClassBeanCopy );
		}
		
		return copyList;
	}
	
	/*
	 * 	Excel編集
	 * 		HSSFWorkbook 	workbook: Excelワークブック
	 * 		ArrayList 		s44HyoukaNinzuList:評価別人数リスト
	 * 		int				intRow:開始行
	 * 		int				intCol:開始列
	 * 		戻り値: なし
	 */
	private boolean setData(HSSFSheet workSheet, S44HyoukaNinzuListBean s44HyoukaNinzuListBean, int intRow, int intCol ){

		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		int		maxSheetIndex	= 0;
		int		row = intRow;
		
		
		// 基本ファイルを読込む
		try{
			
			//模試年度
			workCell = cm.setCell( workSheet, workRow, workCell, row, 3);
			workCell.setCellValue( s44HyoukaNinzuListBean.getStrNendo() + "年度" );
			
			//全国総志望者数
			if( s44HyoukaNinzuListBean.getIntSoshibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol);
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntSoshibo() );
			}
			
			//志望者数（第一志望）
			if( s44HyoukaNinzuListBean.getIntDai1shibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 1);
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntDai1shibo() );
			}
			
			//評価別人数
			if( s44HyoukaNinzuListBean.getIntHyoukaA() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 2 );
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntHyoukaA() );
			}

			if( s44HyoukaNinzuListBean.getIntHyoukaB() != -999 ){			
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntHyoukaB() );
			}
			
			if( s44HyoukaNinzuListBean.getIntHyoukaC() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntHyoukaC() );
			}

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
}
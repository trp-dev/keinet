/*
 * 作成日: 2004/10/07
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.individual;

import java.util.ArrayList;
import java.util.Map;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.excel.data.personal.I11Item;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * 個人用：帳票出力基本クラス
 * 
 * @author kawai_yoshikazu
 * 
 * 2005.03.01 	K.Kondo 	[1]現年度をカレンダーから取得時、３月３１日以前は前年度とする日付比較が、うまく行われていなかったのを修正。
 * 2005.03.08 	K.Kondo 	[2]現年度指定の年度取得を、プロファイルの担当クラス年度に変更
 * 2005.04.18   K.Kondo     [3]対象模試を見て、新テストか否かを判断するメソッドを追加
 */
abstract public class IndAbstractSheetBean extends AbstractSheetBean {
	
	protected I11Item data; // データクラス
	protected String targetExamCode1; // 比較対象模試１
	protected String targetExamCode2; // 比較対象模試２
	protected String targetExamCode3; // 比較対象模試３
	protected String targetCandidateRank1; // 志望大学１
	protected String targetCandidateRank2; // 志望大学２
	protected String targetCandidateRank3; // 志望大学３
	protected String judgeType;			// 大学判定種類
	protected String studentGrade;		// 対象生徒の学年
	
	protected ArrayList univAll;
	protected Map univInfoMap;


	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		// 個人の帳票は一括出力IFしかないのでダミー実装
		return true;
	}

	/**
	 * ICommonMapを取得する
	 * @return
	 */
	protected ICommonMap getICommonMap() {
		return (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);
	}

	/**
	 * 現年度を返します。
	 * @return 現年度
	 */
	protected String getThisYear() {
//[1] delete start
//		String thisYear = "";
//		Calendar cal1 = Calendar.getInstance();
//		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");//デートフォーマット
//		String nowYmd = sdf1.format(cal1.getTime());
//		int year = Integer.parseInt(nowYmd.substring(0, 4));
//		int monthDay = Integer.parseInt(nowYmd.substring(4, 8));		
//		if(monthDay < 0401) {
//			thisYear = Integer.toString(year - 1);
//		} else {
//			thisYear = Integer.toString(year);
//		}
//		return thisYear;
//[1] delete end
//		return KNUtil.getCurrentYear();//[1] add//[2] delete
		return ProfileUtil.getChargeClassYear(profile);//[2] add
	}
	
	//[3] add start
	/**
	 * examTypeCd を元に新テストか否か判断して、新テストフラグを返します
	 * @return int
	 */
	protected int getNewTestFlg() {
		final String NEWTEST_EXAMTYPECD = "31";
		final int NEWTEST_FLG_TRUE = 1;
		final int NEWTEST_FLG_FALSE = 0;
		String examTypeCd = getICommonMap().getTargetExamTypeCode();
		if(examTypeCd.equals(NEWTEST_EXAMTYPECD)) {
			return NEWTEST_FLG_TRUE;
		}
		return NEWTEST_FLG_FALSE;
	}
	//[3] add end

	// 面談シート１が利用可能であるか
	protected boolean isAvailableSheet1() {
		return ("01".equals(exam.getExamTypeCD())
				|| "02".equals(exam.getExamTypeCD())
				|| "03".equals(exam.getExamTypeCD()))
				&& (!"66".equals(exam.getExamCD())
						&& !"67".equals(exam.getExamCD()));
	}
	
	/**
	 * @param item
	 */
	public void setData(I11Item item) {
		data = item;
	}

	/**
	 * @param string
	 */
	public void setTargetCandidateRank1(String string) {
		targetCandidateRank1 = string;
	}

	/**
	 * @param string
	 */
	public void setTargetCandidateRank2(String string) {
		targetCandidateRank2 = string;
	}

	/**
	 * @param string
	 */
	public void setTargetCandidateRank3(String string) {
		targetCandidateRank3 = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode1(String string) {
		targetExamCode1 = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode2(String string) {
		targetExamCode2 = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode3(String string) {
		targetExamCode3 = string;
	}

	/**
	 * @param list
	 */
	public void setUnivAll(ArrayList list) {
		univAll = list;
	}

	/**
	 * @param string
	 */
	public void setStudentGrade(String string) {
		studentGrade = string;
	}

	/**
	 * @param string
	 */
	public void setJudgeType(String string) {
		judgeType = string;
	}

	/**
	 * @param map
	 */
	public void setUnivInfoMap(Map map) {
		univInfoMap = map;
	}

}

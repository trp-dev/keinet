package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * 校内成績分析−他校比較−志望大学評価別人数 データクラス
 * 作成日: 2004/07/20
 * @author	A.Iwata
 */
public class S44Item {
	//学校名
	private String strGakkomei = "";
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//模試コード
	private String strMshCd = "";
	//大学集計区分
	private int intDaiTotalFlg = 0;
	//セキュリティスタンプフラグ
	private int intSecuFlg = 0;
	//データリスト
	private ArrayList s44List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public String getStrMshCd() {
		return this.strMshCd;
	}
	public int getIntDaiTotalFlg() {
		return this.intDaiTotalFlg;
	}
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public ArrayList getS44List() {
		return this.s44List;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setStrMshCd(String strMshCd) {
		this.strMshCd = strMshCd;
	}
	public void setIntDaiTotalFlg(int intDaiTotalFlg) {
		this.intDaiTotalFlg = intDaiTotalFlg;
	}
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setS44List(ArrayList s44List) {
		this.s44List = s44List;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}

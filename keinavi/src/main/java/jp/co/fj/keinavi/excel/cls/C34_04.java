package jp.co.fj.keinavi.excel.cls;

//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.ListIterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C34ClassListBean;
import jp.co.fj.keinavi.excel.data.cls.C34HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.cls.C34Item;
import jp.co.fj.keinavi.excel.data.cls.C34ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * 作成日: 2004/09/
 * @author A.Hasegawa
 *
 * クラス成績分析−過年度比較−志望大学評価別人数　大学（学部・日程あり）出力処理
 *
 * <2010年度改修>
 * 2010.01.06   Tomohisa YAMADA - Totec
 *              センター・リサーチ５段階評価対応
 *
 */
public class C34_04 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	    // ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow	= 50;			//MAX行数

	// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
	//2019/08/09 QQ)Tanouchi 特記事項対応 UPD START
	private int            intMaxClassSr   = 5;                    //MAXクラス数
	//private int		intMaxClassSr   = 3;			//MAXクラス数
	//2019/08/09 QQ)Tanouchi 特記事項対応 UPD END
	// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START

	private int intDataStartRow = 8;					//データセット開始行
	// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
	// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD START
	private int intDataStartCol = 10;					//データセット開始列
	//private int intDataStartCol = 13;					//データセット開始列
	// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD END
        private int MAXCOL = 34;
        //1クラスの項目数
        private int MAXITEM = 5;
        //1クラスの評価別項目数
        private int MAXHYOUKA = 3;
	// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END

	final private String masterfile0 = "C34_04" ;
	final private String masterfile1 = "C34_04" ;
	private String masterfile = "" ;

	final private String strCenterResearch = "38";

	/*
	 * 	Excel編集メイン
	 * 		C34Item C34Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int c34_04EditExcel(C34Item c34Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("C34_04","C34_04帳票作成開始","");

		//テンプレートの決定
		if (c34Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		/** セット処理 **/
		boolean bolBookCngFlg = true;			//改ファイルフラグ
		boolean bolSheetCngFlg = true;			//改シートフラグ

		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFCell	workCell		= null;
		HSSFRow		workRow			= null;
		HSSFWorkbook	tmpworkbook = null;

		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intCol = 0;

		String strGenekiKbn = "";
		String strDaigakuCode = "";
		String strGkbCode = "";
		String strNtiCode = "";
		String strHyoukaKbn = "";

		//2004.09.27Add
		String strKokushiKbn = "";
		String strStKokushiKbn = "";
		int intChangeSheetRow = 0;
		//2004.09.27AddEnd

		int intLoopCnt = 0;
		int intBookCngCount = 1;

		boolean bolDispHyoukaFlg = false;
		boolean bolDispGkbFlg = false;
		boolean bolDispNtiFlg = false;
		boolean bolDispDaigakuFlg = false;
		String strHyouka = "";

		String strDispTarget = "";

		ArrayList WorkBookList = new ArrayList();
		ArrayList WorkSheetList = new ArrayList();

		int intSheetCnt=0;

		int intBookIndex = -1;
		int intSheetIndex = -1;

		int intSetRow = intDataStartRow;

		boolean bolClassDispFlg=true;

		ArrayList HyoukaTitleList = new ArrayList();

		// 基本ファイルを読込む
		C34ListBean c34ListBean = new C34ListBean();

		try{
			/** C34Item編集 **/
			c34Item  = editC34( c34Item );

			HyoukaTitleList.add(0,"A");
			HyoukaTitleList.add(1,"B");
			HyoukaTitleList.add(2,"C");

			// 基本ファイルを読込む
			ArrayList c34List = c34Item.getC34List();
			ListIterator itr = c34List.listIterator();

//			if( itr.hasNext() == false ){
//				return errfdata;
//			}

			/** 大学・学部・日程・評価 **/
			// C34ListBean
			while( itr.hasNext() ){
				c34ListBean = (C34ListBean)itr.next();

				// 大学名
				bolDispDaigakuFlg = false;
				if( !cm.toString(strDaigakuCode).equals(cm.toString(c34ListBean.getStrDaigakuCd()))){
					bolDispDaigakuFlg = true;
					//大学名表示
					strDaigakuCode = c34ListBean.getStrDaigakuCd();
				}

				// 学部
				bolDispGkbFlg = false;
				if( !cm.toString(strGkbCode).equals(cm.toString(c34ListBean.getStrGakubuCd())) ){
					bolDispGkbFlg = true;
					strGkbCode = c34ListBean.getStrGakubuCd();
				}

				// 日程
				bolDispNtiFlg = false;
				if( !cm.toString(strNtiCode).equals(cm.toString(c34ListBean.getStrNtiCd())) ){
					bolDispNtiFlg = true;
					strNtiCode = c34ListBean.getStrNtiCd();
				}

				// 評価区分
				bolDispHyoukaFlg = false;
				if( !cm.toString(strHyoukaKbn).equals(cm.toString(c34ListBean.getStrHyouka())) ){
					bolDispHyoukaFlg = true;
					//評価区分表示
					strHyoukaKbn = c34ListBean.getStrHyouka();
					strHyouka = "";
					switch( Integer.parseInt(strHyoukaKbn) ){
						case 1:
							// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD START
							//strHyouka = "センター";
							strHyouka = "共テ";
							// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD END

							break;
						case 2:
							strHyouka = "2次・私大";
							break;
						case 3:
							strHyouka = "総合";
							break;
					}
				}

				// 表示対象
				if( !cm.toString(strGenekiKbn).equals(cm.toString(c34ListBean.getStrGenKouKbn())) ){
					strGenekiKbn = c34ListBean.getStrGenKouKbn();
					if( cm.toString(strGenekiKbn).equals("0") ){
						strDispTarget = "全体";
					}
					else if( cm.toString(strGenekiKbn).equals("1") ){
						strDispTarget = "現役生";
					}
					else if( cm.toString(strGenekiKbn).equals("2") ){
						strDispTarget = "高卒生";
					}

					if( bolSheetCngFlg == false ){
						bolSheetCngFlg = true;
					}
				}

				//2004.9.27 Add
				int intDaigakuCd = (int)Integer.parseInt(c34ListBean.getStrDaigakuCd());
				if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
					strKokushiKbn = "国公立大";
				}
				if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
					strKokushiKbn = "私立大";
				}
				if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
					strKokushiKbn = "短大";
				}
				if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
					strKokushiKbn = "その他";
				}

				if( !strKokushiKbn.equals(strStKokushiKbn) ){
					bolSheetCngFlg = true;
					strStKokushiKbn = strKokushiKbn;
				}
				//2004.9.27 Add End

				//クラス別評価人数リスト読み込み クラス名をExcelへセット
				ArrayList c34ClassList = c34ListBean.getC34ClassList();
				ListIterator itr2 = c34ClassList.listIterator();

				// Listの取得
				C34ClassListBean c34ClassListBean = new C34ClassListBean();
				C34ClassListBean HomeDataBean = new C34ClassListBean();

				//クラス数取得
				int intClassSr = c34ClassList.size();

//				int intSheetSr = (int)Math.ceil(intClassSr / intMaxClassSr);
//				int intSheetSr = intClassSr / intMaxClassSr;
//				if(intClassSr%intMaxClassSr!=0){
//					intSheetSr++;
//				}
				int intSheetSr = (intClassSr - 1) / intMaxClassSr;
				if((intClassSr - 1)%intMaxClassSr!=0){
					intSheetSr++;
				}

				int intLoopCnt2 = 0;
				int intNendoCount = 0;
				intChangeSheetRow = 0;

				/** 学校名・学年・クラス **/
				// C34ClassListBean
				while( itr2.hasNext() ){
					c34ClassListBean = (C34ClassListBean)itr2.next();

					// Listの取得
					ArrayList c34HyoukaNinzuList = c34ClassListBean.getC34HyoukaNinzuList();
					C34HyoukaNinzuListBean c34HyoukaNinzuListBean = new C34HyoukaNinzuListBean();

					if( intLoopCnt2 == 0 ){
						HomeDataBean = c34ClassListBean;
						c34ClassListBean = (C34ClassListBean)itr2.next();
						c34HyoukaNinzuList = c34ClassListBean.getC34HyoukaNinzuList();
						intNendoCount = c34HyoukaNinzuList.size();
						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
						intLoopCnt2++;
					}
					ListIterator itr3 = c34HyoukaNinzuList.listIterator();
//					ArrayList c34HyoukaNinzuList = c34ClassListBean.getC34HyoukaNinzuList();
//					C34HyoukaNinzuListBean c34HyoukaNinzuListBean = new C34HyoukaNinzuListBean();
//					ListIterator itr3 = c34HyoukaNinzuList.listIterator();
//
//					// 自校情報表示判定
//					if( intLoopCnt2 == 0 ){
//						bolHomeDataDispFlg = true;
//						HomeDataBean = c34ClassListBean;
//						c34ClassListBean = (C34ClassListBean)itr2.next();
//						intNendoCount = c34HyoukaNinzuList.size();
//						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
//						intLoopCnt2++;
//					}

					// 始点情報（Col）
					if( intLoopCnt2 % intMaxClassSr == 0 ){
					        // 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
						// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD START
						//intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 5 );
						//intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 8 );
						// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD END
						intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * MAXITEM );
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
					}else{
					        // 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
						// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD START
						//intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 5 );
						//intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 8 );
						// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD END
                                                intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * MAXITEM );
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
					}

//					intCol = intDataStartCol + ( ( intMaxClassSr - ( intLoopCnt2 % intMaxClassSr ) ) * 5 );

					/** 年度・志望者数 **/
					// C34HyoukaNinzuListBean
					int intHyoukaNinzuListIndex = 0;
					while( itr3.hasNext() ){
						c34HyoukaNinzuListBean = (C34HyoukaNinzuListBean)itr3.next();

						// 規定行数を表示したら線を引く
						if(( intSetRow  >= intChangeSheetRow )||((bolSheetCngFlg == true)&&(intMaxSheetIndex != 0))){
							bolSheetCngFlg = true;
							for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//								int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
								int intBIndex = (intSindex + 1) / intMaxSheetSr;
								if((intSindex + 1)%intMaxSheetSr!=0){
									intBIndex++;
								}
								workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
								workSheet = (HSSFSheet)WorkSheetList.get(intSindex);

								// 罫線出力(セルの上部に太線を引く）
								// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
								//2019/08/22 QQ)Tanouchi 共通テスト対応 UPD START
								//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//									intSetRow, intSetRow, 0, 34, false, true, false, false );
								//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//									intSetRow, intSetRow, 0, 36, false, true, false, false );
                                                                cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                                        intSetRow, intSetRow, 0, MAXCOL, false, true, false, false );
								//2019/08/22 QQ)Tanouchi 共通テスト対応 UPD END
								// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END

								if( workSheet != null ){
									int intRemoveStartRow = intChangeSheetRow;
									if( intMaxRow + intDataStartRow > intRemoveStartRow ){
										for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
											//行削除処理
											workSheet.removeRow(workSheet.getRow(index));
										}
										//罫線出力（セルの上部に太線を出力）
										// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
										//2019/08/22 QQ)Tanouchi 共通テスト対応 UPD START
										//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
										//				intRemoveStartRow, intRemoveStartRow, 0, 34, false, true, false, false);
										//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
										//				intRemoveStartRow, intRemoveStartRow, 0, 36, false, true, false, false);
                                                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
                                                                                        intRemoveStartRow, intRemoveStartRow, 0, MAXCOL, false, true, false, false);
										//2019/08/22 QQ)Tanouchi 共通テスト対応 UPD END
										// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
									}
								}

							}

							////////////////////保存処理//////////////////
							int intSheetNum = 0;
							switch( intSaveFlg ){
								case 1:
								case 5:
									intSheetNum = ((HSSFWorkbook)WorkBookList.get(0)).getNumberOfSheets()-1;
									break;
								case 2:
								case 3:
								case 4:
									intSheetNum = ((HSSFWorkbook)WorkBookList.get(0)).getNumberOfSheets()-2;
									break;
							}

							if( intSheetNum >= intMaxSheetSr ){
								tmpworkbook = (HSSFWorkbook)WorkBookList.get(0);

								boolean bolRet = false;
								// Excelファイル保存
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, tmpworkbook, UserID, intBookCngCount, masterfile, intSheetNum);

								if( bolRet == false ){
									return errfwrite;
								}
								intBookCngCount++;

								// 削除
								WorkBookList.remove(0);
								for(int a=0; a<intMaxSheetSr;a++){
									WorkSheetList.remove(0);
								}
								tmpworkbook=null;
								intBookIndex--;
								intSheetIndex -= intMaxSheetSr;
							}
						}

						// 改シート判定
						if( bolSheetCngFlg == true ){

							bolDispHyoukaFlg = true;
							bolDispDaigakuFlg = true;

							intSetRow = intDataStartRow;
							bolClassDispFlg = true;

							for( int intIndex = 0; intIndex < intSheetSr; intIndex ++ ){

								if( ( intMaxSheetIndex >= intMaxSheetSr ) || ( bolBookCngFlg == true ) ){
									// マスタExcel読み込み
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook==null ){
										return errfread;
									}
									WorkBookList.add(workbook);
									intBookIndex++;

									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
									workbook = (HSSFWorkbook)WorkBookList.get(intBookIndex);
								}

								//シート作成
								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);
								WorkSheetList.add(workSheet);
								intSheetIndex++;
								workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex);

								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);

								// セキュリティスタンプセット
								// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
								//2019/08/09 QQ)Tanouchi 共通テスト対応 UPD START
								//// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD START
								////String secuFlg = cm.setSecurity( workbook, workSheet, c34Item.getIntSecuFlg(), 32, 34 );
								////workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
								//String secuFlg = cm.setSecurity( workbook, workSheet, c34Item.getIntSecuFlg(), 50, 52 );
															//workCell = cm.setCell( workSheet, workRow, workCell, 0, 50 );
								//// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD END
								//String secuFlg = cm.setSecurity( workbook, workSheet, c34Item.getIntSecuFlg(), 34, 36 );
								//							workCell = cm.setCell( workSheet, workRow, workCell, 0, 34 );
                                                                String secuFlg = cm.setSecurity( workbook, workSheet, c34Item.getIntSecuFlg(), 32, 34 );
                                                                workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
								//2019/08/09 QQ)Tanouchi 共通テスト対応 UPD END
								// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
								workCell.setCellValue(secuFlg);

								// 学校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
								workCell.setCellValue( "学校名　：" + c34Item.getStrGakkomei() );

								// 模試月取得
								String moshi =cm.setTaisyouMoshi( c34Item.getStrMshDate() );

								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
								workCell.setCellValue( cm.getTargetExamLabel() + "：" + c34Item.getStrMshmei() + moshi);

								//表示対象
								workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
								workCell.setCellValue( "表示対象：" + strDispTarget );

								//評価別人数 見出し行
								for( int i = 0; i <= intMaxClassSr; i++ ){
								        // 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
									// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD START
									//int intTitleSetCol = (i * 5) + ( intDataStartCol - 3 );
									//int intTitleSetCol = (i * 8) + ( intDataStartCol - 6 );
									// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD END
									int intTitleSetCol = (i * MAXITEM) + ( intDataStartCol - MAXHYOUKA );
									// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END

									//出願予定・第1志望
									//2019/10/03 QQ)Oosaki 共通テスト対応 DEL START
									//// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD START
									////if (strKokushiKbn == "国公立大") {
									////	workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
									////	workCell.setCellValue( "出願予定" );
									////} else {
									////	workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
									////	workCell.setCellValue( "第１志望" );
									////}
									//workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
									//workCell.setCellValue( "第１志望" );
									//// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD END
									//2019/10/03 QQ)Oosaki 共通テスト対応 DEL END

									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol );
									workCell.setCellValue( (String)HyoukaTitleList.get(0) );

									// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
									// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD START
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+1 );
									//workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+2 );
									// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD END
									// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
									workCell.setCellValue( (String)HyoukaTitleList.get(1) );

									// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
									// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD START
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+2 );
									//workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+4 );
									// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD END
									// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
									workCell.setCellValue( (String)HyoukaTitleList.get(2) );
								}

								intMaxSheetIndex++;		//シート数カウンタインクリメント
								bolSheetCngFlg = false;

								intLoopCnt = 0;
							}	//ForEnd

							bolDispHyoukaFlg = true;
							bolDispNtiFlg = true;
							bolDispGkbFlg = true;
							bolDispDaigakuFlg = true;

						}	//if( bolSheetCngFlg == true )

//						intSheetCnt = (int)Math.ceil( (double)intLoopCnt2 / (double)intMaxClassSr );
						intSheetCnt = intLoopCnt2 / intMaxClassSr;
						if(intLoopCnt2%intMaxClassSr!=0){
							intSheetCnt++;
						}
						workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex -  (intSheetSr - intSheetCnt)  );

						int intSheetNo = intSheetIndex -  (intSheetSr - intSheetCnt);
//						int intBIndex = (int)Math.ceil((double)(intSheetNo + 1) / (double)intMaxSheetSr );
						int intBIndex = (intSheetNo + 1) / intMaxSheetSr;
						if((intSheetNo + 1)%intMaxSheetSr!=0){
							intBIndex++;
						}
						workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);

						if( intCol == intDataStartCol ){

							//データセット
							//自校データ・大学名ほか表示
							//自校学校名
							workCell = cm.setCell( workSheet, workRow, workCell, 5, 5 );
							workCell.setCellValue(HomeDataBean.getStrGakkomei());
							//自校データ
							ListIterator itrHome = HomeDataBean.getC34HyoukaNinzuList().listIterator();
							int intLoopIndex = 0;
							while( itrHome.hasNext() ){
								C34HyoukaNinzuListBean HomeHyoukaNinzuListBean = (C34HyoukaNinzuListBean) itrHome.next();
								boolean ret = setData( workSheet, HomeHyoukaNinzuListBean, intSetRow + intLoopIndex , 5 );
								if( ret == false ){
									return errfdata;
								}
								intLoopIndex++;
							}

							if( bolDispHyoukaFlg == true ){
								//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
															intSetRow, intSetRow, 3, 3, false, true, false, false);
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 3);
								workCell.setCellValue( strHyouka );

							}

							if( bolDispNtiFlg == true ){
								if( ((c34ListBean.getStrNittei() != null) && (! c34ListBean.getStrNittei().equals("")))
									|| (bolDispGkbFlg == true) ){
									//罫線出力(セルの上部（日程の列に標準の太さの線を引く）
									cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
																intSetRow, intSetRow, 2, 3, false, true, false, false);
								}
								//日程表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( c34ListBean.getStrNittei() );
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 3);
								workCell.setCellValue( strHyouka );

							}

							if( bolDispGkbFlg == true ){
								//罫線出力(セルの上部（学部の列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
															intSetRow, intSetRow, 1, 3, false, true, false, false);
								//学部表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
								workCell.setCellValue( c34ListBean.getStrGakubuMei() );
								//日程表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( c34ListBean.getStrNittei() );
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 3);
								workCell.setCellValue( strHyouka );

							}

							if( bolDispDaigakuFlg == true ){
								//罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
								//2019/08/09 QQ)Tanouchi 共通テスト対応 UPD START
								////2019/07/31 QQ)Tanioka 共通テスト対応 UPD START
																	////intSetRow, intSetRow, 0, 34, false, true, false, false );
																	//intSetRow, intSetRow, 0, 52, false, true, false, false );
								////2019/07/31 QQ)Tanioka 共通テスト対応 UPD END
																	//intSetRow, intSetRow, 0, 36, false, true, false, false );
								//2019/08/09 QQ)Tanouchi 共通テスト対応 UPD END
	                                                                 intSetRow, intSetRow, 0, MAXCOL, false, true, false, false );
								// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
								//大学名表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 0 );
								workCell.setCellValue(c34ListBean.getStrDaigakuMei());
								//学部表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
								workCell.setCellValue( c34ListBean.getStrGakubuMei() );
								//日程表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( c34ListBean.getStrNittei() );
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 3);
								workCell.setCellValue( strHyouka );
							}

						}

						boolean ret = setData( workSheet, c34HyoukaNinzuListBean, intSetRow + intHyoukaNinzuListIndex, intCol);
						if( ret != true){
							return errfdata;
						}
						else{
							intHyoukaNinzuListIndex++;
							intLoopCnt++;
						}

						//

					}	//WhileEnd( itr3 )


					//クラス名
					if( bolClassDispFlg == true ){
						String strGakunen = c34ClassListBean.getStrGrade();
						String strClass = c34ClassListBean.getStrClass();
						workCell = cm.setCell( workSheet, workRow, workCell, 5, intCol );
						workCell.setCellValue(strGakunen + "年 " + strClass + "クラス");
					}
					intLoopCnt2++;
				}	//WhileEnd( itr2 )

				intSetRow += intNendoCount;
				bolClassDispFlg = false;

//				log.It("C34_04","大学名：",c34ListBean.getStrDaigakuMei());

			}	//WhileEnd( itr )

			for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//				int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr ); /** 20040930 **/
				int intBIndex = (intSindex + 1) / intMaxSheetSr;
				if((intSindex + 1)%intMaxSheetSr!=0){
					intBIndex++;
				}
				workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
				workSheet = (HSSFSheet)WorkSheetList.get(intSindex);

				//罫線出力(セルの上部に太線を引く）
				// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
				//2019/08/09 QQ)Tanouchi 共通テスト対応 UPD START
				//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
				//									intSetRow, intSetRow, 0, 34, false, true, false, false );
				//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
				//									intSetRow, intSetRow, 0, 36, false, true, false, false );
                                cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                        intSetRow, intSetRow, 0, MAXCOL, false, true, false, false );
				//2019/08/09 QQ)Tanouchi 共通テスト対応 UPD END
				// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END

				if( workSheet != null ){
					int intRemoveStartRow = intChangeSheetRow;
					if( intMaxRow + intDataStartRow > intRemoveStartRow ){
						for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
							//行削除処理
							workSheet.removeRow(workSheet.getRow(index));
						}
						//罫線出力（セルの上部に太線を出力）
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
						//2019/08/09 QQ)Tanouchi 共通テスト対応 UPD START
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
						//				intRemoveStartRow, intRemoveStartRow, 0, 34, false, true, false, false);
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
						//                                intRemoveStartRow, intRemoveStartRow, 0, 36, false, true, false, false);
                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
                                                        intRemoveStartRow, intRemoveStartRow, 0, MAXCOL, false, true, false, false);
						//2019/08/09 QQ)Tanouchi 共通テスト対応 UPD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END

					}
				}
			}

			//ファイル保存
			for( int intBookSr = 0; intBookSr < WorkBookList.size(); intBookSr++ ){
				workbook = (HSSFWorkbook)WorkBookList.get(intBookSr);

				boolean bolRet = false;
				// Excelファイル保存
				if( (intBookCngCount != 1)||((intBookCngCount == 1)&&(WorkBookList.size()>1)) ){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, intMaxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}

			//データ無し
			if ( WorkBookList.size() == 0 ){
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex = 1;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);

				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, c34Item.getIntSecuFlg(), 32, 34 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
				workCell.setCellValue(secuFlg);

				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(c34Item.getStrGakkomei()) );

				// 模試月取得
				String moshi =cm.setTaisyouMoshi( c34Item.getStrMshDate() );

				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + c34Item.getStrMshmei() + moshi);

				//表示対象
				workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
				workCell.setCellValue( "表示対象：" );
			}

		}
		catch( Exception e ){
			log.Err("C34_04","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("C34_04","C34_04帳票作成終了","");
		return noerror;
	}

	/**
	 * C34Item加算・編集
	 * @param c34Item
	 * @return
	 */
	private C34Item editC34( C34Item c34Item ) throws Exception{
		try{
			C34Item		c34ItemEdt	= new C34Item();
			String		daigakuCd	= "";
			String		gakubuCd	= "";
			String 		genekiKbn 	= "";

			// 学部計用
			boolean	centerGKBFlg	= false;
			boolean	secondGKBFlg	= false;
			boolean	totalGKBFlg	= false;

			ArrayList c34ClassListGKBAdd		= new ArrayList();
			ArrayList c34ClassListGKBAddCnt	= new ArrayList();
			ArrayList c34ClassListGKBAddSnd	= new ArrayList();
			ArrayList c34ClassListGKBAddTtl	= new ArrayList();
			ArrayList c34NinzuListGKBAdd		= new ArrayList();

			C34ListBean c34ListBeanGKBAddCnt = new C34ListBean();
			C34ListBean c34ListBeanGKBAddSnd = new C34ListBean();
			C34ListBean c34ListBeanGKBAddTtl = new C34ListBean();

			// 日程計用
			boolean	zenCenterNTIFlg	= false;
			boolean	zenSecondNTIFlg	= false;
			boolean	zenTotalNTIFlg	= false;
			boolean	chuCenterNTIFlg	= false;
			boolean	chuSecondNTIFlg	= false;
			boolean	chuTotalNTIFlg	= false;
			boolean	kokCenterNTIFlg	= false;
			boolean	kokSecondNTIFlg	= false;
			boolean	kokTotalNTIFlg	= false;
			boolean	ipnCenterNTIFlg	= false;
			boolean	ipnSecondNTIFlg	= false;
			boolean	ipnTotalNTIFlg	= false;
			boolean	cenCenterNTIFlg	= false;
			boolean	cenSecondNTIFlg	= false;
			boolean	cenTotalNTIFlg	= false;

			ArrayList c34ClassListNTIAdd		= new ArrayList();
			ArrayList c34ClassListNTIAddZenCnt		= new ArrayList();
			ArrayList c34ClassListNTIAddZenSnd		= new ArrayList();
			ArrayList c34ClassListNTIAddZenTtl		= new ArrayList();
			ArrayList c34ClassListNTIAddChuCnt		= new ArrayList();
			ArrayList c34ClassListNTIAddChuSnd		= new ArrayList();
			ArrayList c34ClassListNTIAddChuTtl		= new ArrayList();
			ArrayList c34ClassListNTIAddKokCnt		= new ArrayList();
			ArrayList c34ClassListNTIAddKokSnd		= new ArrayList();
			ArrayList c34ClassListNTIAddKokTtl		= new ArrayList();
			ArrayList c34ClassListNTIAddIpnCnt		= new ArrayList();
			ArrayList c34ClassListNTIAddIpnSnd		= new ArrayList();
			ArrayList c34ClassListNTIAddIpnTtl		= new ArrayList();
			ArrayList c34ClassListNTIAddCenCnt		= new ArrayList();
			ArrayList c34ClassListNTIAddCenSnd		= new ArrayList();
			ArrayList c34ClassListNTIAddCenTtl		= new ArrayList();
			ArrayList c34NinzuListNTIAdd		= new ArrayList();

			C34ListBean c34ListBeanNTIAddZenCnt = new C34ListBean();
			C34ListBean c34ListBeanNTIAddZenSnd = new C34ListBean();
			C34ListBean c34ListBeanNTIAddZenTtl = new C34ListBean();
			C34ListBean c34ListBeanNTIAddChuCnt = new C34ListBean();
			C34ListBean c34ListBeanNTIAddChuSnd = new C34ListBean();
			C34ListBean c34ListBeanNTIAddChuTtl = new C34ListBean();
			C34ListBean c34ListBeanNTIAddKokCnt = new C34ListBean();
			C34ListBean c34ListBeanNTIAddKokSnd = new C34ListBean();
			C34ListBean c34ListBeanNTIAddKokTtl = new C34ListBean();
			C34ListBean c34ListBeanNTIAddIpnCnt = new C34ListBean();
			C34ListBean c34ListBeanNTIAddIpnSnd = new C34ListBean();
			C34ListBean c34ListBeanNTIAddIpnTtl = new C34ListBean();
			C34ListBean c34ListBeanNTIAddCenCnt = new C34ListBean();
			C34ListBean c34ListBeanNTIAddCenSnd = new C34ListBean();
			C34ListBean c34ListBeanNTIAddCenTtl = new C34ListBean();

			// 大学計用
			boolean	centerDGKFlg	= false;
			boolean	secondDGKFlg	= false;
			boolean	totalDGKFlg	= false;

			ArrayList c34ClassListDGKAdd		= new ArrayList();
			ArrayList c34ClassListDGKAddCnt		= new ArrayList();
			ArrayList c34ClassListDGKAddSnd		= new ArrayList();
			ArrayList c34ClassListDGKAddTtl		= new ArrayList();
			ArrayList c34NinzuListDGKAdd		= new ArrayList();

			C34ListBean c34ListBeanDGKAddCnt = new C34ListBean();
			C34ListBean c34ListBeanDGKAddSnd = new C34ListBean();
			C34ListBean c34ListBeanDGKAddTtl = new C34ListBean();

			c34ItemEdt.setStrGakkomei( c34Item.getStrGakkomei() );
			c34ItemEdt.setStrMshmei( c34Item.getStrMshmei() );
			c34ItemEdt.setStrMshDate( c34Item.getStrMshDate() );
			c34ItemEdt.setStrMshCd( c34Item.getStrMshCd() );
			c34ItemEdt.setIntSecuFlg( c34Item.getIntSecuFlg() );
			c34ItemEdt.setIntDaiTotalFlg( c34Item.getIntDaiTotalFlg() );


			/** 大学・学部・学科・日程・評価 **/
			ArrayList c34List		= c34Item.getC34List();
			ArrayList c34ListEdt	= new ArrayList();
			ListIterator itr = c34List.listIterator();
			while( itr.hasNext() ){
				C34ListBean c34ListBean		= (C34ListBean) itr.next();
				C34ListBean c34ListBeanEdt	= new C34ListBean();

				//-----------------------------------------------------------------------------------------
				//大学計・学部計を出力
				//-----------------------------------------------------------------------------------------
				if( !cm.toString(gakubuCd).equals( cm.toString(c34ListBean.getStrGakubuCd()) ) ||
						!cm.toString(daigakuCd).equals( cm.toString(c34ListBean.getStrDaigakuCd()))
						|| !cm.toString(genekiKbn).equals(cm.toString(c34ListBean.getStrGenKouKbn()))) {

					gakubuCd = c34ListBean.getStrGakubuCd();

					if(centerGKBFlg == true){
						c34ListBeanGKBAddCnt.setC34ClassList( c34ClassListGKBAddCnt );
						c34ListEdt.add( c34ListBeanGKBAddCnt );
						c34ClassListGKBAddCnt = new ArrayList();
						centerGKBFlg = false;
					}
					if(secondGKBFlg == true){
						c34ListBeanGKBAddSnd.setC34ClassList( c34ClassListGKBAddSnd );
						c34ListEdt.add( c34ListBeanGKBAddSnd );
						c34ClassListGKBAddSnd = new ArrayList();
						secondGKBFlg = false;
					}
					if(totalGKBFlg == true){
						c34ListBeanGKBAddTtl.setC34ClassList( c34ClassListGKBAddTtl );
						c34ListEdt.add( c34ListBeanGKBAddTtl );
						c34ClassListGKBAddTtl = new ArrayList();
						totalGKBFlg = false;
					}
					c34ListBeanGKBAddCnt = new C34ListBean();
					c34ListBeanGKBAddSnd = new C34ListBean();
					c34ListBeanGKBAddTtl = new C34ListBean();
					c34NinzuListGKBAdd = new ArrayList();
				}
				// 大学コードが変わる→日程計・大学計の保存
//////////////////
//				if( !cm.toString(daigakuCd).equals( cm.toString(c34ListBean.getStrDaigakuCd()) ) ){
//////////////////
				if( (!cm.toString(daigakuCd).equals( cm.toString(c34ListBean.getStrDaigakuCd())))
					||( !cm.toString(genekiKbn).equals(cm.toString(c34ListBean.getStrGenKouKbn()))) ){
					genekiKbn = c34ListBean.getStrGenKouKbn();

					daigakuCd = c34ListBean.getStrDaigakuCd();

					// 日程計
					if(zenCenterNTIFlg == true){
						c34ListBeanNTIAddZenCnt.setC34ClassList( c34ClassListNTIAddZenCnt );
						c34ListEdt.add( c34ListBeanNTIAddZenCnt );
						c34ClassListNTIAddZenCnt = new ArrayList();
						zenCenterNTIFlg = false;
					}
					if(zenSecondNTIFlg == true){
						c34ListBeanNTIAddZenSnd.setC34ClassList( c34ClassListNTIAddZenSnd );
						c34ListEdt.add( c34ListBeanNTIAddZenSnd );
						c34ClassListNTIAddZenSnd = new ArrayList();
						zenSecondNTIFlg = false;
					}
					if(zenTotalNTIFlg == true){
						c34ListBeanNTIAddZenTtl.setC34ClassList( c34ClassListNTIAddZenTtl );
						c34ListEdt.add( c34ListBeanNTIAddZenTtl );
						c34ClassListNTIAddZenTtl = new ArrayList();
						zenTotalNTIFlg = false;
					}
					if(chuCenterNTIFlg == true){
						c34ListBeanNTIAddChuCnt.setC34ClassList( c34ClassListNTIAddChuCnt );
						c34ListEdt.add( c34ListBeanNTIAddChuCnt );
						c34ClassListNTIAddChuCnt = new ArrayList();
						chuCenterNTIFlg = false;
					}
					if(chuSecondNTIFlg == true){
						c34ListBeanNTIAddChuSnd.setC34ClassList( c34ClassListNTIAddChuSnd );
						c34ListEdt.add( c34ListBeanNTIAddChuSnd );
						c34ClassListNTIAddChuSnd = new ArrayList();
						chuSecondNTIFlg = false;
					}
					if(chuTotalNTIFlg == true){
						c34ListBeanNTIAddChuTtl.setC34ClassList( c34ClassListNTIAddChuTtl );
						c34ListEdt.add( c34ListBeanNTIAddChuTtl );
						c34ClassListNTIAddChuTtl = new ArrayList();
						chuTotalNTIFlg = false;
					}
					if(kokCenterNTIFlg == true){
						c34ListBeanNTIAddKokCnt.setC34ClassList( c34ClassListNTIAddKokCnt );
						c34ListEdt.add( c34ListBeanNTIAddKokCnt );
						c34ClassListNTIAddKokCnt = new ArrayList();
						kokCenterNTIFlg = false;
					}
					if(kokSecondNTIFlg == true){
						c34ListBeanNTIAddKokSnd.setC34ClassList( c34ClassListNTIAddKokSnd );
						c34ListEdt.add( c34ListBeanNTIAddKokSnd );
						c34ClassListNTIAddKokSnd = new ArrayList();
						kokSecondNTIFlg = false;
					}
					if(kokTotalNTIFlg == true){
						c34ListBeanNTIAddKokTtl.setC34ClassList( c34ClassListNTIAddKokTtl );
						c34ListEdt.add( c34ListBeanNTIAddKokTtl );
						c34ClassListNTIAddKokTtl = new ArrayList();
						kokTotalNTIFlg = false;
					}
					if(ipnCenterNTIFlg == true){
						c34ListBeanNTIAddIpnCnt.setC34ClassList( c34ClassListNTIAddIpnCnt );
						c34ListEdt.add( c34ListBeanNTIAddIpnCnt );
						c34ClassListNTIAddIpnCnt = new ArrayList();
						ipnCenterNTIFlg = false;
					}
					if(ipnSecondNTIFlg == true){
						c34ListBeanNTIAddIpnSnd.setC34ClassList( c34ClassListNTIAddIpnSnd );
						c34ListEdt.add( c34ListBeanNTIAddIpnSnd );
						c34ClassListNTIAddIpnSnd = new ArrayList();
						ipnSecondNTIFlg = false;
					}
					if(ipnTotalNTIFlg == true){
						c34ListBeanNTIAddIpnTtl.setC34ClassList( c34ClassListNTIAddIpnTtl );
						c34ListEdt.add( c34ListBeanNTIAddIpnTtl );
						c34ClassListNTIAddIpnTtl = new ArrayList();
						ipnTotalNTIFlg = false;
					}
					if(cenCenterNTIFlg == true){
						c34ListBeanNTIAddCenCnt.setC34ClassList( c34ClassListNTIAddCenCnt );
						c34ListEdt.add( c34ListBeanNTIAddCenCnt );
						c34ClassListNTIAddCenCnt = new ArrayList();
						cenCenterNTIFlg = false;
					}
					if(cenSecondNTIFlg == true){
						c34ListBeanNTIAddCenSnd.setC34ClassList( c34ClassListNTIAddCenSnd );
						c34ListEdt.add( c34ListBeanNTIAddCenSnd );
						c34ClassListNTIAddCenSnd = new ArrayList();
						cenSecondNTIFlg = false;
					}
					if(cenTotalNTIFlg == true){
						c34ListBeanNTIAddCenTtl.setC34ClassList( c34ClassListNTIAddCenTtl );
						c34ListEdt.add( c34ListBeanNTIAddCenTtl );
						c34ClassListNTIAddCenTtl = new ArrayList();
						cenTotalNTIFlg = false;
					}

					// 大学計
					if(centerDGKFlg == true){
						c34ListBeanDGKAddCnt.setC34ClassList( c34ClassListDGKAddCnt );
						c34ListEdt.add( c34ListBeanDGKAddCnt );
						c34ClassListDGKAddCnt = new ArrayList();
						centerDGKFlg = false;
					}
					if(secondDGKFlg == true){
						c34ListBeanDGKAddSnd.setC34ClassList( c34ClassListDGKAddSnd );
						c34ListEdt.add( c34ListBeanDGKAddSnd );
						c34ClassListDGKAddSnd = new ArrayList();
						secondDGKFlg = false;
					}
					if(totalDGKFlg == true){
						c34ListBeanDGKAddTtl.setC34ClassList( c34ClassListDGKAddTtl );
						c34ListEdt.add( c34ListBeanDGKAddTtl );
						c34ClassListDGKAddTtl = new ArrayList();
						totalDGKFlg = false;
					}

					c34ListBeanNTIAddZenCnt = new C34ListBean();
					c34ListBeanNTIAddZenSnd = new C34ListBean();
					c34ListBeanNTIAddZenTtl = new C34ListBean();
					c34ListBeanNTIAddChuCnt = new C34ListBean();
					c34ListBeanNTIAddChuSnd = new C34ListBean();
					c34ListBeanNTIAddChuTtl = new C34ListBean();
					c34ListBeanNTIAddKokCnt = new C34ListBean();
					c34ListBeanNTIAddKokSnd = new C34ListBean();
					c34ListBeanNTIAddKokTtl = new C34ListBean();
					c34ListBeanNTIAddIpnCnt = new C34ListBean();
					c34ListBeanNTIAddIpnSnd = new C34ListBean();
					c34ListBeanNTIAddIpnTtl = new C34ListBean();
					c34ListBeanNTIAddCenCnt = new C34ListBean();
					c34ListBeanNTIAddCenSnd = new C34ListBean();
					c34ListBeanNTIAddCenTtl = new C34ListBean();
					c34NinzuListNTIAdd = new ArrayList();

					c34ListBeanDGKAddCnt = new C34ListBean();
					c34ListBeanDGKAddSnd = new C34ListBean();
					c34ListBeanDGKAddTtl = new C34ListBean();
					c34NinzuListDGKAdd = new ArrayList();
				}

				// 基本部分の移し変え
				c34ListBeanEdt.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
				c34ListBeanEdt.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
				c34ListBeanEdt.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
				c34ListBeanEdt.setStrGakubuCd( c34ListBean.getStrGakubuCd() );
				c34ListBeanEdt.setStrGakubuMei( c34ListBean.getStrGakubuMei() );
				c34ListBeanEdt.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
				c34ListBeanEdt.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
				c34ListBeanEdt.setStrNtiCd( c34ListBean.getStrNtiCd() );
				c34ListBeanEdt.setStrNittei( c34ListBean.getStrNittei() );
				c34ListBeanEdt.setStrHyouka( c34ListBean.getStrHyouka() );

				// 学部計値の保存部
				switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
					case 1:
						c34ListBeanGKBAddCnt.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanGKBAddCnt.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanGKBAddCnt.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanGKBAddCnt.setStrGakubuCd( c34ListBean.getStrGakubuCd() );
						c34ListBeanGKBAddCnt.setStrGakubuMei( c34ListBean.getStrGakubuMei() );
						c34ListBeanGKBAddCnt.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanGKBAddCnt.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanGKBAddCnt.setStrNtiCd( "NTI-GKB" );
						c34ListBeanGKBAddCnt.setStrNittei( "学部計" );
						c34ListBeanGKBAddCnt.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListGKBAdd = deepCopyCl( c34ClassListGKBAddCnt );
						centerGKBFlg = true;
						break;
					case 2:
						c34ListBeanGKBAddSnd.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanGKBAddSnd.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanGKBAddSnd.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanGKBAddSnd.setStrGakubuCd( c34ListBean.getStrGakubuCd() );
						c34ListBeanGKBAddSnd.setStrGakubuMei( c34ListBean.getStrGakubuMei() );
						c34ListBeanGKBAddSnd.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanGKBAddSnd.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanGKBAddSnd.setStrNtiCd( "NTI-GKB" );
						c34ListBeanGKBAddSnd.setStrNittei( "学部計" );
						c34ListBeanGKBAddSnd.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListGKBAdd = deepCopyCl( c34ClassListGKBAddSnd );
						secondGKBFlg = true;
						break;
					case 3:
						c34ListBeanGKBAddTtl.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanGKBAddTtl.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanGKBAddTtl.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanGKBAddTtl.setStrGakubuCd( c34ListBean.getStrGakubuCd() );
						c34ListBeanGKBAddTtl.setStrGakubuMei( c34ListBean.getStrGakubuMei() );
						c34ListBeanGKBAddTtl.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanGKBAddTtl.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanGKBAddTtl.setStrNtiCd( "NTI-GKB" );
						c34ListBeanGKBAddTtl.setStrNittei( "学部計" );
						c34ListBeanGKBAddTtl.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListGKBAdd = deepCopyCl( c34ClassListGKBAddTtl );
						totalGKBFlg = true;
						break;
				}

				// 大学計値の保存部
				switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
					case 1:
						c34ListBeanDGKAddCnt.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanDGKAddCnt.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanDGKAddCnt.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanDGKAddCnt.setStrGakubuCd( "NTI-DGK" );
						c34ListBeanDGKAddCnt.setStrGakubuMei( "大学計" );
						c34ListBeanDGKAddCnt.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanDGKAddCnt.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanDGKAddCnt.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanDGKAddCnt.setStrNittei( "" );
						c34ListBeanDGKAddCnt.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListDGKAdd = deepCopyCl( c34ClassListDGKAddCnt );
						centerDGKFlg = true;
						break;
					case 2:
						c34ListBeanDGKAddSnd.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanDGKAddSnd.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanDGKAddSnd.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanDGKAddSnd.setStrGakubuCd( "NTI-DGK" );
						c34ListBeanDGKAddSnd.setStrGakubuMei( "大学計" );
						c34ListBeanDGKAddSnd.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanDGKAddSnd.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanDGKAddSnd.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanDGKAddSnd.setStrNittei( "" );
						c34ListBeanDGKAddSnd.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListDGKAdd = deepCopyCl( c34ClassListDGKAddSnd );
						secondDGKFlg = true;
						break;
					case 3:
						c34ListBeanDGKAddTtl.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanDGKAddTtl.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanDGKAddTtl.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanDGKAddTtl.setStrGakubuCd( "NTI-DGK" );
						c34ListBeanDGKAddTtl.setStrGakubuMei( "大学計" );
						c34ListBeanDGKAddTtl.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanDGKAddTtl.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanDGKAddTtl.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanDGKAddTtl.setStrNittei( "" );
						c34ListBeanDGKAddTtl.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListDGKAdd = deepCopyCl( c34ClassListDGKAddTtl );
						totalDGKFlg = true;
						break;
				}

				// 日程計値の保存部
				if( cm.toString(c34ListBean.getStrNtiCd()).equals("D") ){		 // 前期
					switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
						case 1:
						c34ListBeanNTIAddZenCnt.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddZenCnt.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddZenCnt.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddZenCnt.setStrGakubuCd( "NTI-ZEN" );
						c34ListBeanNTIAddZenCnt.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddZenCnt.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddZenCnt.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddZenCnt.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddZenCnt.setStrNittei( "" );
						c34ListBeanNTIAddZenCnt.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddZenCnt );
						zenCenterNTIFlg = true;
							break;
						case 2:
						c34ListBeanNTIAddZenSnd.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddZenSnd.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddZenSnd.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddZenSnd.setStrGakubuCd( "NTI-ZEN" );
						c34ListBeanNTIAddZenSnd.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddZenSnd.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddZenSnd.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddZenSnd.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddZenSnd.setStrNittei( "" );
						c34ListBeanNTIAddZenSnd.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddZenSnd );
						zenSecondNTIFlg = true;
							break;
						case 3:
						c34ListBeanNTIAddZenTtl.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddZenTtl.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddZenTtl.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddZenTtl.setStrGakubuCd( "NTI-ZEN" );
						c34ListBeanNTIAddZenTtl.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddZenTtl.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddZenTtl.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddZenTtl.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddZenTtl.setStrNittei( "" );
						c34ListBeanNTIAddZenTtl.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddZenTtl );
						zenTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(c34ListBean.getStrNtiCd()).equals("E") ){	 // 中期
					switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
						case 1:
						c34ListBeanNTIAddChuCnt.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddChuCnt.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddChuCnt.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddChuCnt.setStrGakubuCd( "NTI-CHU" );
						c34ListBeanNTIAddChuCnt.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddChuCnt.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddChuCnt.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddChuCnt.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddChuCnt.setStrNittei( "" );
						c34ListBeanNTIAddChuCnt.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddChuCnt );
						chuCenterNTIFlg = true;
							break;
						case 2:
						c34ListBeanNTIAddChuSnd.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddChuSnd.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddChuSnd.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddChuSnd.setStrGakubuCd( "NTI-CHU" );
						c34ListBeanNTIAddChuSnd.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddChuSnd.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddChuSnd.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddChuSnd.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddChuSnd.setStrNittei( "" );
						c34ListBeanNTIAddChuSnd.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddChuSnd );
						chuSecondNTIFlg = true;
							break;
						case 3:
						c34ListBeanNTIAddChuTtl.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddChuTtl.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddChuTtl.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddChuTtl.setStrGakubuCd( "NTI-CHU" );
						c34ListBeanNTIAddChuTtl.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddChuTtl.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddChuTtl.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddChuTtl.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddChuTtl.setStrNittei( "" );
						c34ListBeanNTIAddChuTtl.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddChuTtl );
						chuTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(c34ListBean.getStrNtiCd()).equals("C") ){	 // 後期
					switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
						case 1:
						c34ListBeanNTIAddKokCnt.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddKokCnt.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddKokCnt.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddKokCnt.setStrGakubuCd( "NTI-KOK" );
						c34ListBeanNTIAddKokCnt.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddKokCnt.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddKokCnt.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddKokCnt.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddKokCnt.setStrNittei( "" );
						c34ListBeanNTIAddKokCnt.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddKokCnt );
						kokCenterNTIFlg = true;
							break;
						case 2:
						c34ListBeanNTIAddKokSnd.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddKokSnd.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddKokSnd.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddKokSnd.setStrGakubuCd( "NTI-KOK" );
						c34ListBeanNTIAddKokSnd.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddKokSnd.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddKokSnd.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddKokSnd.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddKokSnd.setStrNittei( "" );
						c34ListBeanNTIAddKokSnd.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddKokSnd );
						kokSecondNTIFlg = true;
							break;
						case 3:
						c34ListBeanNTIAddKokTtl.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddKokTtl.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddKokTtl.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddKokTtl.setStrGakubuCd( "NTI-KOK" );
						c34ListBeanNTIAddKokTtl.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddKokTtl.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddKokTtl.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddKokTtl.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddKokTtl.setStrNittei( "" );
						c34ListBeanNTIAddKokTtl.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddKokTtl );
						kokTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(c34ListBean.getStrNtiCd()).equals("N") ){	 // 一般型
					switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
						case 1:
						c34ListBeanNTIAddIpnCnt.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddIpnCnt.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddIpnCnt.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddIpnCnt.setStrGakubuCd( "NTI-IPN" );
						c34ListBeanNTIAddIpnCnt.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddIpnCnt.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddIpnCnt.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddIpnCnt.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddIpnCnt.setStrNittei( "" );
						c34ListBeanNTIAddIpnCnt.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddIpnCnt );
						ipnCenterNTIFlg = true;
							break;
						case 2:
						c34ListBeanNTIAddIpnSnd.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddIpnSnd.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddIpnSnd.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddIpnSnd.setStrGakubuCd( "NTI-IPN" );
						c34ListBeanNTIAddIpnSnd.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddIpnSnd.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddIpnSnd.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddIpnSnd.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddIpnSnd.setStrNittei( "" );
						c34ListBeanNTIAddIpnSnd.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddIpnSnd );
						ipnSecondNTIFlg = true;
							break;
						case 3:
						c34ListBeanNTIAddIpnTtl.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddIpnTtl.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddIpnTtl.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddIpnTtl.setStrGakubuCd( "NTI-IPN" );
						c34ListBeanNTIAddIpnTtl.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddIpnTtl.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddIpnTtl.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddIpnTtl.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddIpnTtl.setStrNittei( "" );
						c34ListBeanNTIAddIpnTtl.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddIpnTtl );
						ipnTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(c34ListBean.getStrNtiCd()).equals("1") ){	 // センター型
					switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
						case 1:
						c34ListBeanNTIAddCenCnt.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddCenCnt.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddCenCnt.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddCenCnt.setStrGakubuCd( "NTI-CEN" );
						c34ListBeanNTIAddCenCnt.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddCenCnt.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddCenCnt.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddCenCnt.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddCenCnt.setStrNittei( "" );
						c34ListBeanNTIAddCenCnt.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddCenCnt );
						cenCenterNTIFlg = true;
							break;
						case 2:
						c34ListBeanNTIAddCenSnd.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddCenSnd.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddCenSnd.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddCenSnd.setStrGakubuCd( "NTI-CEN" );
						c34ListBeanNTIAddCenSnd.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddCenSnd.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddCenSnd.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddCenSnd.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddCenSnd.setStrNittei( "" );
						c34ListBeanNTIAddCenSnd.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddCenSnd );
						cenSecondNTIFlg = true;
							break;
						case 3:
						c34ListBeanNTIAddCenTtl.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanNTIAddCenTtl.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanNTIAddCenTtl.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanNTIAddCenTtl.setStrGakubuCd( "NTI-CEN" );
						c34ListBeanNTIAddCenTtl.setStrGakubuMei( c34ListBean.getStrNittei() + "計" );
						c34ListBeanNTIAddCenTtl.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanNTIAddCenTtl.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanNTIAddCenTtl.setStrNtiCd( c34ListBean.getStrNtiCd() );
						c34ListBeanNTIAddCenTtl.setStrNittei( "" );
						c34ListBeanNTIAddCenTtl.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListNTIAdd = deepCopyCl( c34ClassListNTIAddCenTtl );
						cenTotalNTIFlg = true;
							break;
					}
				}

				//
				ArrayList c34ClassList		= c34ListBean.getC34ClassList();
				ArrayList c34ClassListEdt	= new ArrayList();
				ListIterator itrClass = c34ClassList.listIterator();
				int b = 0;
				boolean firstFlgGKB3 = true;
				boolean firstFlgDGK3 = true;
				boolean firstFlgNTI3 = true;
				if(c34ClassListGKBAdd.size() == 0){
					firstFlgGKB3 = false;
				}
				if(c34ClassListDGKAdd.size() == 0){
					firstFlgDGK3 = false;
				}
				if(c34ClassListNTIAdd.size() == 0){
					firstFlgNTI3 = false;
				}
				/** 学校・学年・クラス **/
				while( itrClass.hasNext() ){
					C34ClassListBean c34ClassListBean = (C34ClassListBean)itrClass.next();
					C34ClassListBean c34ClassListBeanEdt = new C34ClassListBean();
					C34ClassListBean c34ClassListBeanDGKAdd = null;
					C34ClassListBean c34ClassListBeanGKBAdd = null;
					C34ClassListBean c34ClassListBeanNTIAdd = null;

					// 移し変え
					c34ClassListBeanEdt.setStrGakkomei( c34ClassListBean.getStrGakkomei() );
					c34ClassListBeanEdt.setStrGrade( c34ClassListBean.getStrGrade() );
					c34ClassListBeanEdt.setStrClass( c34ClassListBean.getStrClass() );

					// 大学計用データ呼び出し
					if(firstFlgDGK3 == true){
						c34ClassListBeanDGKAdd = (C34ClassListBean) c34ClassListDGKAdd.get(b);
						c34NinzuListDGKAdd = deepCopyNz( c34ClassListBeanDGKAdd.getC34HyoukaNinzuList() );
					}else{
						c34ClassListBeanDGKAdd = new C34ClassListBean();

						c34ClassListBeanDGKAdd.setStrGakkomei( c34ClassListBean.getStrGakkomei() );
						c34ClassListBeanDGKAdd.setStrGrade( c34ClassListBean.getStrGrade() );
						c34ClassListBeanDGKAdd.setStrClass( c34ClassListBean.getStrClass() );
					}

					// 学部計用データ呼び出し
					if(firstFlgGKB3 == true){
						c34ClassListBeanGKBAdd = (C34ClassListBean) c34ClassListGKBAdd.get(b);
						c34NinzuListGKBAdd = deepCopyNz( c34ClassListBeanGKBAdd.getC34HyoukaNinzuList() );
					}else{
						c34ClassListBeanGKBAdd = new C34ClassListBean();

						c34ClassListBeanGKBAdd.setStrGakkomei( c34ClassListBean.getStrGakkomei() );
						c34ClassListBeanGKBAdd.setStrGrade( c34ClassListBean.getStrGrade() );
						c34ClassListBeanGKBAdd.setStrClass( c34ClassListBean.getStrClass() );
					}

					// 日程計用データ呼び出し
					if(firstFlgNTI3 == true){
						c34ClassListBeanNTIAdd = (C34ClassListBean) c34ClassListNTIAdd.get(b);
						c34NinzuListNTIAdd = deepCopyNz( c34ClassListBeanNTIAdd.getC34HyoukaNinzuList() );
					}else{
						c34ClassListBeanNTIAdd = new C34ClassListBean();

						c34ClassListBeanNTIAdd.setStrGakkomei( c34ClassListBean.getStrGakkomei() );
						c34ClassListBeanNTIAdd.setStrGrade( c34ClassListBean.getStrGrade() );
						c34ClassListBeanNTIAdd.setStrClass( c34ClassListBean.getStrClass() );
					}

					/** 年度・人数 **/
					ArrayList c34NinzuList = c34ClassListBean.getC34HyoukaNinzuList();
					ArrayList c34NinzuListEdt = new ArrayList();
					ListIterator itrNinzu = c34NinzuList.listIterator();
					int a = 0;
					boolean firstFlgDGK2 = true;
					boolean firstFlgGKB2 = true;
					boolean firstFlgNTI2 = true;
					if(c34NinzuListDGKAdd.size() == 0){
						firstFlgDGK2 = false;
					}
					if(c34NinzuListGKBAdd.size() == 0){
						firstFlgGKB2 = false;
					}
					if(c34NinzuListNTIAdd.size() == 0){
						firstFlgNTI2 = false;
					}
					while( itrNinzu.hasNext() ){
						C34HyoukaNinzuListBean c34NinzuBean = (C34HyoukaNinzuListBean)itrNinzu.next();
						C34HyoukaNinzuListBean c34NinzuBeanEdt = new C34HyoukaNinzuListBean();
						C34HyoukaNinzuListBean c34NinzuBeanDGKAdd = null;
						C34HyoukaNinzuListBean c34NinzuBeanGKBAdd = null;
						C34HyoukaNinzuListBean c34NinzuBeanNTIAdd = null;

						// 移し変え
						c34NinzuBeanEdt.setStrNendo( c34NinzuBean.getStrNendo() );
						c34NinzuBeanEdt.setIntSoshibo( c34NinzuBean.getIntSoshibo() );
						c34NinzuBeanEdt.setIntDai1shibo( c34NinzuBean.getIntDai1shibo() );
						c34NinzuBeanEdt.setIntHyoukaA( c34NinzuBean.getIntHyoukaA() );
						c34NinzuBeanEdt.setIntHyoukaB( c34NinzuBean.getIntHyoukaB() );
						c34NinzuBeanEdt.setIntHyoukaC( c34NinzuBean.getIntHyoukaC() );
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
//						c34NinzuBeanEdt.setIntSoshibo_Hukumu( c34NinzuBean.getIntSoshibo_Hukumu() );
//						c34NinzuBeanEdt.setIntDai1shibo_Hukumu( c34NinzuBean.getIntDai1shibo_Hukumu() );
//						c34NinzuBeanEdt.setIntHyoukaA_Hukumu( c34NinzuBean.getIntHyoukaA_Hukumu() );
//						c34NinzuBeanEdt.setIntHyoukaB_Hukumu( c34NinzuBean.getIntHyoukaB_Hukumu() );
//						c34NinzuBeanEdt.setIntHyoukaC_Hukumu( c34NinzuBean.getIntHyoukaC_Hukumu() );
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						c34NinzuListEdt.add( c34NinzuBeanEdt );

						// 加算処理(大学計用)
						if(firstFlgDGK2 == true){
							c34NinzuBeanDGKAdd = (C34HyoukaNinzuListBean) c34NinzuListDGKAdd.get(a);
						}else{
							c34NinzuBeanDGKAdd = new C34HyoukaNinzuListBean();

							c34NinzuBeanDGKAdd.setStrNendo( c34NinzuBean.getStrNendo() );
							c34NinzuBeanDGKAdd.setIntSoshibo( -999 );
							c34NinzuBeanDGKAdd.setIntDai1shibo( -999 );
							c34NinzuBeanDGKAdd.setIntHyoukaA( -999 );
							c34NinzuBeanDGKAdd.setIntHyoukaB( -999 );
							c34NinzuBeanDGKAdd.setIntHyoukaC( -999 );
							// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
							//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
//							c34NinzuBeanDGKAdd.setIntSoshibo_Hukumu( -999 );
//							c34NinzuBeanDGKAdd.setIntDai1shibo_Hukumu( -999 );
//							c34NinzuBeanDGKAdd.setIntHyoukaA_Hukumu( -999 );
//							c34NinzuBeanDGKAdd.setIntHyoukaB_Hukumu( -999 );
//							c34NinzuBeanDGKAdd.setIntHyoukaC_Hukumu( -999 );
							//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
							// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END
						}

						if(c34NinzuBean.getIntSoshibo()!=-999){
//							c34NinzuBeanDGKAdd.setIntSoshibo( c34NinzuBeanDGKAdd.getIntSoshibo() + c34NinzuBean.getIntSoshibo() );
							if(c34NinzuBeanDGKAdd.getIntSoshibo()!=-999){
								c34NinzuBeanDGKAdd.setIntSoshibo(c34NinzuBeanDGKAdd.getIntSoshibo() + c34NinzuBean.getIntSoshibo());
							}else{
								c34NinzuBeanDGKAdd.setIntSoshibo(c34NinzuBean.getIntSoshibo());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//総志望含む
//						if(c34NinzuBean.getIntSoshibo_Hukumu()!=-999){
//							if(c34NinzuBeanDGKAdd.getIntSoshibo_Hukumu()!=-999){
//								c34NinzuBeanDGKAdd.setIntSoshibo_Hukumu(c34NinzuBeanDGKAdd.getIntSoshibo_Hukumu() + c34NinzuBean.getIntSoshibo_Hukumu());
//							}else{
//								c34NinzuBeanDGKAdd.setIntSoshibo_Hukumu(c34NinzuBean.getIntSoshibo_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(c34NinzuBean.getIntDai1shibo()!=-999){
//							c34NinzuBeanDGKAdd.setIntDai1shibo( c34NinzuBeanDGKAdd.getIntDai1shibo() + c34NinzuBean.getIntDai1shibo() );
							if(c34NinzuBeanDGKAdd.getIntDai1shibo()!=-999){
								c34NinzuBeanDGKAdd.setIntDai1shibo(c34NinzuBeanDGKAdd.getIntDai1shibo() + c34NinzuBean.getIntDai1shibo());
							}else{
								c34NinzuBeanDGKAdd.setIntDai1shibo(c34NinzuBean.getIntDai1shibo());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//第1志望含む
//						if(c34NinzuBean.getIntDai1shibo_Hukumu()!=-999){
//							if(c34NinzuBeanDGKAdd.getIntDai1shibo_Hukumu()!=-999){
//								c34NinzuBeanDGKAdd.setIntDai1shibo_Hukumu(c34NinzuBeanDGKAdd.getIntDai1shibo_Hukumu() + c34NinzuBean.getIntDai1shibo_Hukumu());
//							}else{
//								c34NinzuBeanDGKAdd.setIntDai1shibo_Hukumu(c34NinzuBean.getIntDai1shibo_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(c34NinzuBean.getIntHyoukaA()!=-999){
//							c34NinzuBeanDGKAdd.setIntHyoukaA( c34NinzuBeanDGKAdd.getIntHyoukaA() + c34NinzuBean.getIntHyoukaA() );
							if(c34NinzuBeanDGKAdd.getIntHyoukaA()!=-999){
								c34NinzuBeanDGKAdd.setIntHyoukaA(c34NinzuBeanDGKAdd.getIntHyoukaA() + c34NinzuBean.getIntHyoukaA());
							}else{
								c34NinzuBeanDGKAdd.setIntHyoukaA(c34NinzuBean.getIntHyoukaA());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//A含む
//						if(c34NinzuBean.getIntHyoukaA_Hukumu()!=-999){
//							if(c34NinzuBeanDGKAdd.getIntHyoukaA_Hukumu()!=-999){
//								c34NinzuBeanDGKAdd.setIntHyoukaA_Hukumu(c34NinzuBeanDGKAdd.getIntHyoukaA_Hukumu() + c34NinzuBean.getIntHyoukaA_Hukumu());
//							}else{
//								c34NinzuBeanDGKAdd.setIntHyoukaA_Hukumu(c34NinzuBean.getIntHyoukaA_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(c34NinzuBean.getIntHyoukaB()!=-999){
//							c34NinzuBeanDGKAdd.setIntHyoukaB( c34NinzuBeanDGKAdd.getIntHyoukaB() + c34NinzuBean.getIntHyoukaB() );
							if(c34NinzuBeanDGKAdd.getIntHyoukaB()!=-999){
								c34NinzuBeanDGKAdd.setIntHyoukaB(c34NinzuBeanDGKAdd.getIntHyoukaB() + c34NinzuBean.getIntHyoukaB());
							}else{
								c34NinzuBeanDGKAdd.setIntHyoukaB(c34NinzuBean.getIntHyoukaB());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//B含む
//						if(c34NinzuBean.getIntHyoukaB_Hukumu()!=-999){
//							if(c34NinzuBeanDGKAdd.getIntHyoukaB_Hukumu()!=-999){
//								c34NinzuBeanDGKAdd.setIntHyoukaB_Hukumu(c34NinzuBeanDGKAdd.getIntHyoukaB_Hukumu() + c34NinzuBean.getIntHyoukaB_Hukumu());
//							}else{
//								c34NinzuBeanDGKAdd.setIntHyoukaB_Hukumu(c34NinzuBean.getIntHyoukaB_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(c34NinzuBean.getIntHyoukaC()!=-999){
//							c34NinzuBeanDGKAdd.setIntHyoukaC( c34NinzuBeanDGKAdd.getIntHyoukaC() + c34NinzuBean.getIntHyoukaC() );
							if(c34NinzuBeanDGKAdd.getIntHyoukaC()!=-999){
								c34NinzuBeanDGKAdd.setIntHyoukaC(c34NinzuBeanDGKAdd.getIntHyoukaC() + c34NinzuBean.getIntHyoukaC());
							}else{
								c34NinzuBeanDGKAdd.setIntHyoukaC(c34NinzuBean.getIntHyoukaC());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//C含む
//						if(c34NinzuBean.getIntHyoukaC_Hukumu()!=-999){
//							if(c34NinzuBeanDGKAdd.getIntHyoukaC_Hukumu()!=-999){
//								c34NinzuBeanDGKAdd.setIntHyoukaC_Hukumu(c34NinzuBeanDGKAdd.getIntHyoukaC_Hukumu() + c34NinzuBean.getIntHyoukaC_Hukumu());
//							}else{
//								c34NinzuBeanDGKAdd.setIntHyoukaC_Hukumu(c34NinzuBean.getIntHyoukaC_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(firstFlgDGK2 == true){
							c34NinzuListDGKAdd.set(a,c34NinzuBeanDGKAdd);
						}else{
							c34NinzuListDGKAdd.add(c34NinzuBeanDGKAdd);
						}

						// 加算処理(学部計用)
						if(firstFlgGKB2 == true){
							c34NinzuBeanGKBAdd = (C34HyoukaNinzuListBean) c34NinzuListGKBAdd.get(a);
						}else{
							c34NinzuBeanGKBAdd = new C34HyoukaNinzuListBean();

							c34NinzuBeanGKBAdd.setStrNendo( c34NinzuBean.getStrNendo() );
							c34NinzuBeanGKBAdd.setIntSoshibo( -999 );
							c34NinzuBeanGKBAdd.setIntDai1shibo( -999 );
							c34NinzuBeanGKBAdd.setIntHyoukaA( -999 );
							c34NinzuBeanGKBAdd.setIntHyoukaB( -999 );
							c34NinzuBeanGKBAdd.setIntHyoukaC( -999 );
							// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
							//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
//							c34NinzuBeanGKBAdd.setIntSoshibo_Hukumu( -999 );
//							c34NinzuBeanGKBAdd.setIntDai1shibo_Hukumu( -999 );
//							c34NinzuBeanGKBAdd.setIntHyoukaA_Hukumu( -999 );
//							c34NinzuBeanGKBAdd.setIntHyoukaB_Hukumu( -999 );
//							c34NinzuBeanGKBAdd.setIntHyoukaC_Hukumu( -999 );
							//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
							// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						}

						if(c34NinzuBean.getIntSoshibo()!=-999){
//							c34NinzuBeanGKBAdd.setIntSoshibo( c34NinzuBeanGKBAdd.getIntSoshibo() + c34NinzuBean.getIntSoshibo() );
							if(c34NinzuBeanGKBAdd.getIntSoshibo()!=-999){
								c34NinzuBeanGKBAdd.setIntSoshibo(c34NinzuBeanGKBAdd.getIntSoshibo() + c34NinzuBean.getIntSoshibo());
							}else{
								c34NinzuBeanGKBAdd.setIntSoshibo(c34NinzuBean.getIntSoshibo());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//総志望含む
//						if(c34NinzuBean.getIntSoshibo_Hukumu()!=-999){
//							if(c34NinzuBeanGKBAdd.getIntSoshibo_Hukumu()!=-999){
//								c34NinzuBeanGKBAdd.setIntSoshibo_Hukumu(c34NinzuBeanGKBAdd.getIntSoshibo_Hukumu() + c34NinzuBean.getIntSoshibo_Hukumu());
//							}else{
//								c34NinzuBeanGKBAdd.setIntSoshibo_Hukumu(c34NinzuBean.getIntSoshibo_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(c34NinzuBean.getIntDai1shibo()!=-999){
//							c34NinzuBeanGKBAdd.setIntDai1shibo( c34NinzuBeanGKBAdd.getIntDai1shibo() + c34NinzuBean.getIntDai1shibo() );
							if(c34NinzuBeanGKBAdd.getIntDai1shibo()!=-999){
								c34NinzuBeanGKBAdd.setIntDai1shibo(c34NinzuBeanGKBAdd.getIntDai1shibo() + c34NinzuBean.getIntDai1shibo());
							}else{
								c34NinzuBeanGKBAdd.setIntDai1shibo(c34NinzuBean.getIntDai1shibo());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//第１志望含む
//						if(c34NinzuBean.getIntDai1shibo_Hukumu()!=-999){
//							if(c34NinzuBeanGKBAdd.getIntDai1shibo_Hukumu()!=-999){
//								c34NinzuBeanGKBAdd.setIntDai1shibo_Hukumu(c34NinzuBeanGKBAdd.getIntDai1shibo_Hukumu() + c34NinzuBean.getIntDai1shibo_Hukumu());
//							}else{
//								c34NinzuBeanGKBAdd.setIntDai1shibo_Hukumu(c34NinzuBean.getIntDai1shibo_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(c34NinzuBean.getIntHyoukaA()!=-999){
//							c34NinzuBeanGKBAdd.setIntHyoukaA( c34NinzuBeanGKBAdd.getIntHyoukaA() + c34NinzuBean.getIntHyoukaA() );
							if(c34NinzuBeanGKBAdd.getIntHyoukaA()!=-999){
								c34NinzuBeanGKBAdd.setIntHyoukaA(c34NinzuBeanGKBAdd.getIntHyoukaA() + c34NinzuBean.getIntHyoukaA());
							}else{
								c34NinzuBeanGKBAdd.setIntHyoukaA(c34NinzuBean.getIntHyoukaA());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//A含む
//						if(c34NinzuBean.getIntHyoukaA_Hukumu()!=-999){
//							if(c34NinzuBeanGKBAdd.getIntHyoukaA_Hukumu()!=-999){
//								c34NinzuBeanGKBAdd.setIntHyoukaA_Hukumu(c34NinzuBeanGKBAdd.getIntHyoukaA_Hukumu() + c34NinzuBean.getIntHyoukaA_Hukumu());
//							}else{
//								c34NinzuBeanGKBAdd.setIntHyoukaA_Hukumu(c34NinzuBean.getIntHyoukaA_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(c34NinzuBean.getIntHyoukaB()!=-999){
//							c34NinzuBeanGKBAdd.setIntHyoukaB( c34NinzuBeanGKBAdd.getIntHyoukaB() + c34NinzuBean.getIntHyoukaB() );
							if(c34NinzuBeanGKBAdd.getIntHyoukaB()!=-999){
								c34NinzuBeanGKBAdd.setIntHyoukaB(c34NinzuBeanGKBAdd.getIntHyoukaB() + c34NinzuBean.getIntHyoukaB());
							}else{
								c34NinzuBeanGKBAdd.setIntHyoukaB(c34NinzuBean.getIntHyoukaB());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//B含む
//						if(c34NinzuBean.getIntHyoukaB_Hukumu()!=-999){
//							if(c34NinzuBeanGKBAdd.getIntHyoukaB_Hukumu()!=-999){
//								c34NinzuBeanGKBAdd.setIntHyoukaB_Hukumu(c34NinzuBeanGKBAdd.getIntHyoukaB_Hukumu() + c34NinzuBean.getIntHyoukaB_Hukumu());
//							}else{
//								c34NinzuBeanGKBAdd.setIntHyoukaB_Hukumu(c34NinzuBean.getIntHyoukaB_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(c34NinzuBean.getIntHyoukaC()!=-999){
//							c34NinzuBeanGKBAdd.setIntHyoukaC( c34NinzuBeanGKBAdd.getIntHyoukaC() + c34NinzuBean.getIntHyoukaC() );
							if(c34NinzuBeanGKBAdd.getIntHyoukaC()!=-999){
								c34NinzuBeanGKBAdd.setIntHyoukaC(c34NinzuBeanGKBAdd.getIntHyoukaC() + c34NinzuBean.getIntHyoukaC());
							}else{
								c34NinzuBeanGKBAdd.setIntHyoukaC(c34NinzuBean.getIntHyoukaC());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//C含む
//						if(c34NinzuBean.getIntHyoukaC_Hukumu()!=-999){
//							if(c34NinzuBeanGKBAdd.getIntHyoukaC_Hukumu()!=-999){
//								c34NinzuBeanGKBAdd.setIntHyoukaC_Hukumu(c34NinzuBeanGKBAdd.getIntHyoukaC_Hukumu() + c34NinzuBean.getIntHyoukaC_Hukumu());
//							}else{
//								c34NinzuBeanGKBAdd.setIntHyoukaC_Hukumu(c34NinzuBean.getIntHyoukaC_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(firstFlgGKB2 == true){
							c34NinzuListGKBAdd.set(a,c34NinzuBeanGKBAdd);
						}else{
							c34NinzuListGKBAdd.add(c34NinzuBeanGKBAdd);
						}

						// 加算処理(日程計用)
						if(firstFlgNTI2 == true){
							c34NinzuBeanNTIAdd = (C34HyoukaNinzuListBean) c34NinzuListNTIAdd.get(a);
						}else{
							c34NinzuBeanNTIAdd = new C34HyoukaNinzuListBean();

							c34NinzuBeanNTIAdd.setStrNendo( c34NinzuBean.getStrNendo() );
							c34NinzuBeanNTIAdd.setIntSoshibo( -999 );
							c34NinzuBeanNTIAdd.setIntDai1shibo( -999 );
							c34NinzuBeanNTIAdd.setIntHyoukaA( -999 );
							c34NinzuBeanNTIAdd.setIntHyoukaB( -999 );
							c34NinzuBeanNTIAdd.setIntHyoukaC( -999 );
							// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
							//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
//							c34NinzuBeanNTIAdd.setIntSoshibo_Hukumu( -999 );
//							c34NinzuBeanNTIAdd.setIntDai1shibo_Hukumu( -999 );
//							c34NinzuBeanNTIAdd.setIntHyoukaA_Hukumu( -999 );
//							c34NinzuBeanNTIAdd.setIntHyoukaB_Hukumu( -999 );
//							c34NinzuBeanNTIAdd.setIntHyoukaC_Hukumu( -999 );
							//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
							// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END
						}

						if(c34NinzuBean.getIntSoshibo()!=-999){
//							c34NinzuBeanNTIAdd.setIntSoshibo( c34NinzuBeanNTIAdd.getIntSoshibo() + c34NinzuBean.getIntSoshibo() );
							if(c34NinzuBeanNTIAdd.getIntSoshibo()!=-999){
								c34NinzuBeanNTIAdd.setIntSoshibo(c34NinzuBeanNTIAdd.getIntSoshibo() + c34NinzuBean.getIntSoshibo());
							}else{
								c34NinzuBeanNTIAdd.setIntSoshibo(c34NinzuBean.getIntSoshibo());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//総志望含む
//						if(c34NinzuBean.getIntSoshibo_Hukumu()!=-999){
//							if(c34NinzuBeanNTIAdd.getIntSoshibo_Hukumu()!=-999){
//								c34NinzuBeanNTIAdd.setIntSoshibo_Hukumu(c34NinzuBeanNTIAdd.getIntSoshibo_Hukumu() + c34NinzuBean.getIntSoshibo_Hukumu());
//							}else{
//								c34NinzuBeanNTIAdd.setIntSoshibo_Hukumu(c34NinzuBean.getIntSoshibo_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START

						if(c34NinzuBean.getIntDai1shibo()!=-999){
//							c34NinzuBeanNTIAdd.setIntDai1shibo( c34NinzuBeanNTIAdd.getIntDai1shibo() + c34NinzuBean.getIntDai1shibo() );
							if(c34NinzuBeanNTIAdd.getIntDai1shibo()!=-999){
								c34NinzuBeanNTIAdd.setIntDai1shibo(c34NinzuBeanNTIAdd.getIntDai1shibo() + c34NinzuBean.getIntDai1shibo());
							}else{
								c34NinzuBeanNTIAdd.setIntDai1shibo(c34NinzuBean.getIntDai1shibo());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//第1志望含む
//						if(c34NinzuBean.getIntDai1shibo_Hukumu()!=-999){
//							if(c34NinzuBeanNTIAdd.getIntDai1shibo_Hukumu()!=-999){
//								c34NinzuBeanNTIAdd.setIntDai1shibo_Hukumu(c34NinzuBeanNTIAdd.getIntDai1shibo_Hukumu() + c34NinzuBean.getIntDai1shibo_Hukumu());
//							}else{
//								c34NinzuBeanNTIAdd.setIntDai1shibo_Hukumu(c34NinzuBean.getIntDai1shibo_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(c34NinzuBean.getIntHyoukaA()!=-999){
//							c34NinzuBeanNTIAdd.setIntHyoukaA( c34NinzuBeanNTIAdd.getIntHyoukaA() + c34NinzuBean.getIntHyoukaA() );
							if(c34NinzuBeanNTIAdd.getIntHyoukaA()!=-999){
								c34NinzuBeanNTIAdd.setIntHyoukaA(c34NinzuBeanNTIAdd.getIntHyoukaA() + c34NinzuBean.getIntHyoukaA());
							}else{
								c34NinzuBeanNTIAdd.setIntHyoukaA(c34NinzuBean.getIntHyoukaA());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//A含む
//						if(c34NinzuBean.getIntHyoukaA_Hukumu()!=-999){
//							if(c34NinzuBeanNTIAdd.getIntHyoukaA_Hukumu()!=-999){
//								c34NinzuBeanNTIAdd.setIntHyoukaA_Hukumu(c34NinzuBeanNTIAdd.getIntHyoukaA_Hukumu() + c34NinzuBean.getIntHyoukaA_Hukumu());
//							}else{
//								c34NinzuBeanNTIAdd.setIntHyoukaA_Hukumu(c34NinzuBean.getIntHyoukaA_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(c34NinzuBean.getIntHyoukaB()!=-999){
//							c34NinzuBeanNTIAdd.setIntHyoukaB( c34NinzuBeanNTIAdd.getIntHyoukaB() + c34NinzuBean.getIntHyoukaB() );
							if(c34NinzuBeanNTIAdd.getIntHyoukaB()!=-999){
								c34NinzuBeanNTIAdd.setIntHyoukaB(c34NinzuBeanNTIAdd.getIntHyoukaB() + c34NinzuBean.getIntHyoukaB());
							}else{
								c34NinzuBeanNTIAdd.setIntHyoukaB(c34NinzuBean.getIntHyoukaB());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//B含む
//						if(c34NinzuBean.getIntHyoukaB_Hukumu()!=-999){
//							if(c34NinzuBeanNTIAdd.getIntHyoukaB_Hukumu()!=-999){
//								c34NinzuBeanNTIAdd.setIntHyoukaB_Hukumu(c34NinzuBeanNTIAdd.getIntHyoukaB_Hukumu() + c34NinzuBean.getIntHyoukaB_Hukumu());
//							}else{
//								c34NinzuBeanNTIAdd.setIntHyoukaB_Hukumu(c34NinzuBean.getIntHyoukaB_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(c34NinzuBean.getIntHyoukaC()!=-999){
//							c34NinzuBeanNTIAdd.setIntHyoukaC( c34NinzuBeanNTIAdd.getIntHyoukaC() + c34NinzuBean.getIntHyoukaC() );
							if(c34NinzuBeanNTIAdd.getIntHyoukaC()!=-999){
								c34NinzuBeanNTIAdd.setIntHyoukaC(c34NinzuBeanNTIAdd.getIntHyoukaC() + c34NinzuBean.getIntHyoukaC());
							}else{
								c34NinzuBeanNTIAdd.setIntHyoukaC(c34NinzuBean.getIntHyoukaC());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
						//C含む
//						if(c34NinzuBean.getIntHyoukaC_Hukumu()!=-999){
//							if(c34NinzuBeanNTIAdd.getIntHyoukaC_Hukumu()!=-999){
//								c34NinzuBeanNTIAdd.setIntHyoukaC_Hukumu(c34NinzuBeanNTIAdd.getIntHyoukaC_Hukumu() + c34NinzuBean.getIntHyoukaC_Hukumu());
//							}else{
//								c34NinzuBeanNTIAdd.setIntHyoukaC_Hukumu(c34NinzuBean.getIntHyoukaC_Hukumu());
//							}
//						}
						//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(firstFlgNTI2 == true){
							c34NinzuListNTIAdd.set(a,c34NinzuBeanNTIAdd);
						}else{
							c34NinzuListNTIAdd.add(c34NinzuBeanNTIAdd);
						}
						a++;
					}

					// 格納
					c34ClassListBeanEdt.setC34HyoukaNinzuList( c34NinzuListEdt );
					c34ClassListEdt.add( c34ClassListBeanEdt );

					// 大学計用
					c34ClassListBeanDGKAdd.setC34HyoukaNinzuList( deepCopyNz(c34NinzuListDGKAdd) );
					if(firstFlgDGK3 == true){
						c34ClassListDGKAdd.set(b,c34ClassListBeanDGKAdd);
					}else{
						c34ClassListDGKAdd.add(c34ClassListBeanDGKAdd);
					}
					// 学部計用
					c34ClassListBeanGKBAdd.setC34HyoukaNinzuList( deepCopyNz(c34NinzuListGKBAdd) );
					if(firstFlgGKB3 == true){
						c34ClassListGKBAdd.set(b,c34ClassListBeanGKBAdd);
					}else{
						c34ClassListGKBAdd.add(c34ClassListBeanGKBAdd);
					}
					// 日程計用
					c34ClassListBeanNTIAdd.setC34HyoukaNinzuList( deepCopyNz(c34NinzuListNTIAdd) );
					if(firstFlgNTI3 == true){
						c34ClassListNTIAdd.set(b,c34ClassListBeanNTIAdd);
					}else{
						c34ClassListNTIAdd.add(c34ClassListBeanNTIAdd);
					}
					c34NinzuListDGKAdd = new ArrayList();
					c34NinzuListGKBAdd = new ArrayList();
					c34NinzuListNTIAdd = new ArrayList();
					b++;
				}
				// 格納
				c34ListBeanEdt.setC34ClassList( c34ClassListEdt );
				c34ListEdt.add( c34ListBeanEdt );
				// 大学計用
				switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
					case 1:
						c34ClassListDGKAddCnt = deepCopyCl( c34ClassListDGKAdd );
						break;
					case 2:
						c34ClassListDGKAddSnd = deepCopyCl( c34ClassListDGKAdd );
						break;
					case 3:
						c34ClassListDGKAddTtl = deepCopyCl( c34ClassListDGKAdd );
						break;
				}
				// 学部計用
				switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
					case 1:
						c34ClassListGKBAddCnt = deepCopyCl( c34ClassListGKBAdd );
						break;
					case 2:
						c34ClassListGKBAddSnd = deepCopyCl( c34ClassListGKBAdd );
						break;
					case 3:
						c34ClassListGKBAddTtl = deepCopyCl( c34ClassListGKBAdd );
						break;
				}
				// 日程計用
				if( cm.toString(c34ListBean.getStrNtiCd()).equals("D") ){		 // 前期
					switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
						case 1:
							c34ClassListNTIAddZenCnt = deepCopyCl( c34ClassListNTIAdd );
							break;
						case 2:
							c34ClassListNTIAddZenSnd = deepCopyCl( c34ClassListNTIAdd );
							break;
						case 3:
							c34ClassListNTIAddZenTtl = deepCopyCl( c34ClassListNTIAdd );
							break;
					}
				}else if( cm.toString(c34ListBean.getStrNtiCd()).equals("E") ){	 // 中期
					switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
						case 1:
							c34ClassListNTIAddChuCnt = deepCopyCl( c34ClassListNTIAdd );
							break;
						case 2:
							c34ClassListNTIAddChuSnd = deepCopyCl( c34ClassListNTIAdd );
							break;
						case 3:
							c34ClassListNTIAddChuTtl = deepCopyCl( c34ClassListNTIAdd );
							break;
					}
				}else if( cm.toString(c34ListBean.getStrNtiCd()).equals("C") ){	 // 後期
					switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
						case 1:
							c34ClassListNTIAddKokCnt = deepCopyCl( c34ClassListNTIAdd );
							break;
						case 2:
							c34ClassListNTIAddKokSnd = deepCopyCl( c34ClassListNTIAdd );
							break;
						case 3:
							c34ClassListNTIAddKokTtl = deepCopyCl( c34ClassListNTIAdd );
							break;
					}
				}else if( cm.toString(c34ListBean.getStrNtiCd()).equals("N") ){	 // 一般型
					switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
						case 1:
							c34ClassListNTIAddIpnCnt = deepCopyCl( c34ClassListNTIAdd );
							break;
						case 2:
							c34ClassListNTIAddIpnSnd = deepCopyCl( c34ClassListNTIAdd );
							break;
						case 3:
							c34ClassListNTIAddIpnTtl = deepCopyCl( c34ClassListNTIAdd );
							break;
					}
				}else if( cm.toString(c34ListBean.getStrNtiCd()).equals("1") ){	 // センター型
					switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
						case 1:
							c34ClassListNTIAddCenCnt = deepCopyCl( c34ClassListNTIAdd );
							break;
						case 2:
							c34ClassListNTIAddCenSnd = deepCopyCl( c34ClassListNTIAdd );
							break;
						case 3:
							c34ClassListNTIAddCenTtl = deepCopyCl( c34ClassListNTIAdd );
							break;
					}
				}

				c34ClassListDGKAdd = new ArrayList();
				c34ClassListGKBAdd = new ArrayList();
				c34ClassListNTIAdd = new ArrayList();
			}
			//最後に小計・日程計・大学計の保存
			// 学部コードが変わる→学部計の保存
			if(centerGKBFlg == true){
				c34ListBeanGKBAddCnt.setC34ClassList( c34ClassListGKBAddCnt );
				c34ListEdt.add( c34ListBeanGKBAddCnt );
				c34ClassListGKBAddCnt = new ArrayList();
				centerGKBFlg = false;
			}
			if(secondGKBFlg == true){
				c34ListBeanGKBAddSnd.setC34ClassList( c34ClassListGKBAddSnd );
				c34ListEdt.add( c34ListBeanGKBAddSnd );
				c34ClassListGKBAddSnd = new ArrayList();
				secondGKBFlg = false;
			}
			if(totalGKBFlg == true){
				c34ListBeanGKBAddTtl.setC34ClassList( c34ClassListGKBAddTtl );
				c34ListEdt.add( c34ListBeanGKBAddTtl );
				c34ClassListGKBAddTtl = new ArrayList();
				totalGKBFlg = false;
			}

			// 大学コードが変わる→日程計・大学計の保存
			// 日程計
			if(zenCenterNTIFlg == true){
				c34ListBeanNTIAddZenCnt.setC34ClassList( c34ClassListNTIAddZenCnt );
				c34ListEdt.add( c34ListBeanNTIAddZenCnt );
				c34ClassListNTIAddZenCnt = new ArrayList();
				zenCenterNTIFlg = false;
			}
			if(zenSecondNTIFlg == true){
				c34ListBeanNTIAddZenSnd.setC34ClassList( c34ClassListNTIAddZenSnd );
				c34ListEdt.add( c34ListBeanNTIAddZenSnd );
				c34ClassListNTIAddZenSnd = new ArrayList();
				zenSecondNTIFlg = false;
			}
			if(zenTotalNTIFlg == true){
				c34ListBeanNTIAddZenTtl.setC34ClassList( c34ClassListNTIAddZenTtl );
				c34ListEdt.add( c34ListBeanNTIAddZenTtl );
				c34ClassListNTIAddZenTtl = new ArrayList();
				zenTotalNTIFlg = false;
			}
			if(chuCenterNTIFlg == true){
				c34ListBeanNTIAddChuCnt.setC34ClassList( c34ClassListNTIAddChuCnt );
				c34ListEdt.add( c34ListBeanNTIAddChuCnt );
				c34ClassListNTIAddChuCnt = new ArrayList();
				chuCenterNTIFlg = false;
			}
			if(chuSecondNTIFlg == true){
				c34ListBeanNTIAddChuSnd.setC34ClassList( c34ClassListNTIAddChuSnd );
				c34ListEdt.add( c34ListBeanNTIAddChuSnd );
				c34ClassListNTIAddChuSnd = new ArrayList();
				chuSecondNTIFlg = false;
			}
			if(chuTotalNTIFlg == true){
				c34ListBeanNTIAddChuTtl.setC34ClassList( c34ClassListNTIAddChuTtl );
				c34ListEdt.add( c34ListBeanNTIAddChuTtl );
				c34ClassListNTIAddChuTtl = new ArrayList();
				chuTotalNTIFlg = false;
			}
			if(kokCenterNTIFlg == true){
				c34ListBeanNTIAddKokCnt.setC34ClassList( c34ClassListNTIAddKokCnt );
				c34ListEdt.add( c34ListBeanNTIAddKokCnt );
				c34ClassListNTIAddKokCnt = new ArrayList();
				kokCenterNTIFlg = false;
			}
			if(kokSecondNTIFlg == true){
				c34ListBeanNTIAddKokSnd.setC34ClassList( c34ClassListNTIAddKokSnd );
				c34ListEdt.add( c34ListBeanNTIAddKokSnd );
				c34ClassListNTIAddKokSnd = new ArrayList();
				kokSecondNTIFlg = false;
			}
			if(kokTotalNTIFlg == true){
				c34ListBeanNTIAddKokTtl.setC34ClassList( c34ClassListNTIAddKokTtl );
				c34ListEdt.add( c34ListBeanNTIAddKokTtl );
				c34ClassListNTIAddKokTtl = new ArrayList();
				kokTotalNTIFlg = false;
			}
			if(ipnCenterNTIFlg == true){
				c34ListBeanNTIAddIpnCnt.setC34ClassList( c34ClassListNTIAddIpnCnt );
				c34ListEdt.add( c34ListBeanNTIAddIpnCnt );
				c34ClassListNTIAddIpnCnt = new ArrayList();
				ipnCenterNTIFlg = false;
			}
			if(ipnSecondNTIFlg == true){
				c34ListBeanNTIAddIpnSnd.setC34ClassList( c34ClassListNTIAddIpnSnd );
				c34ListEdt.add( c34ListBeanNTIAddIpnSnd );
				c34ClassListNTIAddIpnSnd = new ArrayList();
				ipnSecondNTIFlg = false;
			}
			if(ipnTotalNTIFlg == true){
				c34ListBeanNTIAddIpnTtl.setC34ClassList( c34ClassListNTIAddIpnTtl );
				c34ListEdt.add( c34ListBeanNTIAddIpnTtl );
				c34ClassListNTIAddIpnTtl = new ArrayList();
				ipnTotalNTIFlg = false;
			}
			if(cenCenterNTIFlg == true){
				c34ListBeanNTIAddCenCnt.setC34ClassList( c34ClassListNTIAddCenCnt );
				c34ListEdt.add( c34ListBeanNTIAddCenCnt );
				c34ClassListNTIAddCenCnt = new ArrayList();
				cenCenterNTIFlg = false;
			}
			if(cenSecondNTIFlg == true){
				c34ListBeanNTIAddCenSnd.setC34ClassList( c34ClassListNTIAddCenSnd );
				c34ListEdt.add( c34ListBeanNTIAddCenSnd );
				c34ClassListNTIAddCenSnd = new ArrayList();
				cenSecondNTIFlg = false;
			}
			if(cenTotalNTIFlg == true){
				c34ListBeanNTIAddCenTtl.setC34ClassList( c34ClassListNTIAddCenTtl );
				c34ListEdt.add( c34ListBeanNTIAddCenTtl );
				c34ClassListNTIAddCenTtl = new ArrayList();
				cenTotalNTIFlg = false;
			}

			// 大学計
			if(centerDGKFlg == true){
				c34ListBeanDGKAddCnt.setC34ClassList( c34ClassListDGKAddCnt );
				c34ListEdt.add( c34ListBeanDGKAddCnt );
				c34ClassListDGKAddCnt = new ArrayList();
				centerDGKFlg = false;
			}
			if(secondDGKFlg == true){
				c34ListBeanDGKAddSnd.setC34ClassList( c34ClassListDGKAddSnd );
				c34ListEdt.add( c34ListBeanDGKAddSnd );
				c34ClassListDGKAddSnd = new ArrayList();
				secondDGKFlg = false;
			}
			if(totalDGKFlg == true){
				c34ListBeanDGKAddTtl.setC34ClassList( c34ClassListDGKAddTtl );
				c34ListEdt.add( c34ListBeanDGKAddTtl );
				c34ClassListDGKAddTtl = new ArrayList();
				totalDGKFlg = false;
			}

			// 格納
			c34ItemEdt.setC34List( c34ListEdt );

			return c34ItemEdt;
		}catch(Exception e){
			throw e;

		}
	}

	/**
	 *
	 * @param c34NinzuList
	 * @return
	 */
	private ArrayList deepCopyNz(ArrayList c34NinzuList){
		ListIterator itrNinzu = c34NinzuList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrNinzu.hasNext()){
			C34HyoukaNinzuListBean c34NinzuBean = (C34HyoukaNinzuListBean)itrNinzu.next();
			C34HyoukaNinzuListBean c34NinzuBeanCopy = new C34HyoukaNinzuListBean();

			c34NinzuBeanCopy.setStrNendo( c34NinzuBean.getStrNendo() );
			c34NinzuBeanCopy.setIntSoshibo( c34NinzuBean.getIntSoshibo() );
			c34NinzuBeanCopy.setIntDai1shibo( c34NinzuBean.getIntDai1shibo() );
			c34NinzuBeanCopy.setIntHyoukaA( c34NinzuBean.getIntHyoukaA() );
			c34NinzuBeanCopy.setIntHyoukaB( c34NinzuBean.getIntHyoukaB() );
			c34NinzuBeanCopy.setIntHyoukaC( c34NinzuBean.getIntHyoukaC() );
			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
			//2019/08/20 QQ)Tanouchi 共通テスト対応 ADD START
//			c34NinzuBeanCopy.setIntSoshibo_Hukumu( c34NinzuBean.getIntSoshibo_Hukumu() );
//			c34NinzuBeanCopy.setIntDai1shibo_Hukumu( c34NinzuBean.getIntDai1shibo_Hukumu() );
//			c34NinzuBeanCopy.setIntHyoukaA_Hukumu( c34NinzuBean.getIntHyoukaA_Hukumu() );
//			c34NinzuBeanCopy.setIntHyoukaB_Hukumu( c34NinzuBean.getIntHyoukaB_Hukumu() );
//			c34NinzuBeanCopy.setIntHyoukaC_Hukumu( c34NinzuBean.getIntHyoukaC_Hukumu() );
			//2019/08/20 QQ)Tanouchi 共通テスト対応 ADD END
			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

			copyList.add( c34NinzuBeanCopy );
		}

		return copyList;
	}

	/**
	 *
	 * @param c34ClassList
	 * @return
	 */
	private ArrayList deepCopyCl(ArrayList c34ClassList){
		ListIterator itrClass = c34ClassList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrClass.hasNext()){
			C34ClassListBean c34ClassBean = (C34ClassListBean)itrClass.next();
			C34ClassListBean c34ClassBeanCopy = new C34ClassListBean();

			c34ClassBeanCopy.setStrClass( c34ClassBean.getStrClass() );
			c34ClassBeanCopy.setStrGakkomei( c34ClassBean.getStrGakkomei() );
			c34ClassBeanCopy.setStrGrade( c34ClassBean.getStrGrade() );
			c34ClassBeanCopy.setC34HyoukaNinzuList( deepCopyNz(c34ClassBean.getC34HyoukaNinzuList()) );

			copyList.add( c34ClassBeanCopy );
		}

		return copyList;
	}

	/*
	 * 	Excel編集
	 * 		HSSFWorkbook 	workbook: Excelワークブック
	 * 		ArrayList 		c34HyoukaNinzuList:評価別人数リスト
	 * 		int				intRow:開始行
	 * 		int				intCol:開始列
	 * 		戻り値: なし
	 */
	private boolean setData(HSSFSheet workSheet, C34HyoukaNinzuListBean c34HyoukaNinzuListBean, int intRow, int intCol ){

		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
		//2019/08/20 QQ)Tanouchi 共通テスト対応 ADD START
		//int Soshibo = 0;
		//int Dai1shibo = 0;
		//2019/08/20 QQ)Tanouchi 共通テスト対応 ADD END
		// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END
		int		row = intRow;


		// 基本ファイルを読込む
		try{

			//模試年度
			workCell = cm.setCell( workSheet, workRow, workCell, row, 4);
			workCell.setCellValue( c34HyoukaNinzuListBean.getStrNendo() + "年度" );

			//全国総志望者数
			if( c34HyoukaNinzuListBean.getIntSoshibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol);
				workCell.setCellValue( c34HyoukaNinzuListBean.getIntSoshibo() );
			}

			//志望者数（第一志望）
			if( c34HyoukaNinzuListBean.getIntDai1shibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 1);
				workCell.setCellValue( c34HyoukaNinzuListBean.getIntDai1shibo() );
			}

			//評価別人数
			if( c34HyoukaNinzuListBean.getIntHyoukaA() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 2 );
				workCell.setCellValue( c34HyoukaNinzuListBean.getIntHyoukaA() );
			}
			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
			//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
			//A含む
//			if( c34HyoukaNinzuListBean.getIntHyoukaA_Hukumu() != -999 ){
//				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
//				workCell.setCellValue( c34HyoukaNinzuListBean.getIntHyoukaA_Hukumu() );
//			}
			//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

			if( c34HyoukaNinzuListBean.getIntHyoukaB() != -999 ){
			        // 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
				// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD START
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
				//workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
				// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD END
				// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
				workCell.setCellValue( c34HyoukaNinzuListBean.getIntHyoukaB() );
			}
			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
			//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
			//B含む
//			if( c34HyoukaNinzuListBean.getIntHyoukaB_Hukumu() != -999 ){
//				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 5 );
//				workCell.setCellValue( c34HyoukaNinzuListBean.getIntHyoukaB_Hukumu() );
//			}
			//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

			if( c34HyoukaNinzuListBean.getIntHyoukaC() != -999 ){
			        // 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
				// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD START
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
				//workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 6 );
				// 2019/07/31 QQ)Tanioka 共通テスト対応 UPD END
				// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
				workCell.setCellValue( c34HyoukaNinzuListBean.getIntHyoukaC() );
			}
			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
			//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD START
			//C含む
//			if( c34HyoukaNinzuListBean.getIntHyoukaC_Hukumu() != -999 ){
//				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 7 );
//				workCell.setCellValue( c34HyoukaNinzuListBean.getIntHyoukaC_Hukumu() );
//			}
			//2019/08/16 QQ)Tanouchi 共通テスト対応 ADD END
			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

		}
		catch(Exception e){
			return false;
		}

		return true;
	}

}
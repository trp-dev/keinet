package jp.co.fj.keinavi.forms.sales;

import jp.co.fj.keinavi.forms.MethodInvokerForm;

/**
 * 
 * 特例成績データダウンロード画面のアクションフォームです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD207Form extends MethodInvokerForm {

	/** serialVersionUID */
	private static final long serialVersionUID = 1147848437299491473L;

	/** 対象年度 */
	private String examYear;

	/** 絞込み条件 */
	private String condition;

	/** ソートキー */
	private String sortKey;

	/** 申請ID */
	private String appId;

	/** 取消申請ID */
	private String cancelAppId;

	/**
	 * ソートキーを返します。
	 * 
	 * @return ソートキー
	 */
	public String getSortKey() {
		return sortKey;
	}

	/**
	 * ソートキーをセットします。
	 * 
	 * @param soryKey
	 *            ソートキー
	 */
	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}

	/**
	 * 申請IDを返します。
	 * 
	 * @return 申請ID
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * 申請IDをセットします。
	 * 
	 * @param appId
	 *            申請ID
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * 対象年度を返します。
	 * 
	 * @return 対象年度
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * 対象年度をセットします。
	 * 
	 * @param examYear
	 *            対象年度
	 */
	public void setExamYear(String examYear) {
		this.examYear = examYear;
	}

	/**
	 * 絞込み条件を返します。
	 * 
	 * @return 絞込み条件
	 */
	public String getCondition() {
		return condition;
	}

	/**
	 * 絞込み条件をセットします。
	 * 
	 * @param condition
	 *            絞込み条件
	 */
	public void setCondition(String condition) {
		this.condition = condition;
	}

	/**
	 * 取消申請IDを返します。
	 * 
	 * @return 取消申請ID
	 */
	public String getCancelAppId() {
		return cancelAppId;
	}

	/**
	 * 取消申請IDをセットします。
	 * 
	 * @param cancelAppId
	 *            取消申請ID
	 */
	public void setCancelAppId(String cancelAppId) {
		this.cancelAppId = cancelAppId;
	}

}

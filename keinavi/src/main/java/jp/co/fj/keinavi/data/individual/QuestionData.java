package jp.co.fj.keinavi.data.individual;

import java.io.Serializable;

/**
 * 作成日：08/03
 * @author kondo
 * 
 * 2009.11.04   Fujito URAKAWA - Totec
 *              「学力レベル」項目を生かした新メニュー対応 
 * 
 * 設問情報のデータクラスです。
 */
public class QuestionData implements Serializable{
	private String personId;			//個人ＩＤ
	private String examYear;			//模試年度
	private String examCode;			//模試コード
	private String subCode;			//科目コード
	private String questionNo;			//設問番号
	private String bundleCode;			//一括コード
	
	private String qscore;				//得点
	private String scoreRate;			//得点率
	
	private String display_no;			//表示番号
	private String questionName1;		//設問内容名１
	private String questionName2;		//設問内容名２
	private String qallotpnt;			//配点
	
	private String avgScoreRate_LEVEL;	//レベル別平均得点率
	
	private String avgPnt_A;			//全国平均点
	private String avgScoreRate_A;		//全国平均得点率
	private String avgPnt_S;			//校内平均点
	private String avgScoreRate_S;		//校内平均得点率

	public static QuestionData getEmptyQuestionData(){
		QuestionData data = new QuestionData();
		data.setPersonId(new String());
		data.setExamYear(new String());
		data.setExamCode(new String());
		data.setSubCode(new String());
		data.setQuestionNo(new String());
		data.setBundleCode(new String());
		data.setQscore(new String());
		data.setScoreRate(new String());
		data.setDisplay_no(new String());
		data.setQuestionName1(new String());
		data.setQuestionName2(new String());
		data.setQallotpnt(new String());
		data.setAvgScoreRate_LEVEL(new String());
		data.setAvgPnt_A(new String());
		data.setAvgScoreRate_A(new String());
		data.setAvgPnt_S(new String());
		data.setAvgScoreRate_S(new String());
		return data;
	}

	/**
	 * @return
	 */
	public String getAvgPnt_A() {
		return avgPnt_A;
	}

	/**
	 * @return
	 */
	public String getAvgPnt_S() {
		return avgPnt_S;
	}

	/**
	 * @return
	 */
	public String getAvgScoreRate_A() {
		return avgScoreRate_A;
	}

	/**
	 * @return
	 */
	public String getAvgScoreRate_S() {
		return avgScoreRate_S;
	}

	/**
	 * @return
	 */
	public String getBundleCode() {
		return bundleCode;
	}

	/**
	 * @return
	 */
	public String getDisplay_no() {
		return display_no;
	}

	/**
	 * @return
	 */
	public String getExamCode() {
		return examCode;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @return
	 */
	public String getPersonId() {
		return personId;
	}

	/**
	 * @return
	 */
	public String getQallotpnt() {
		return qallotpnt;
	}

	/**
	 * @return
	 */
	public String getQscore() {
		return qscore;
	}

	/**
	 * @return
	 */
	public String getQuestionName1() {
		return questionName1;
	}

	/**
	 * @return
	 */
	public String getQuestionName2() {
		return questionName2;
	}

	/**
	 * @return
	 */
	public String getQuestionNo() {
		return questionNo;
	}

	/**
	 * @return
	 */
	public String getScoreRate() {
		return scoreRate;
	}

	/**
	 * @return
	 */
	public String getSubCode() {
		return subCode;
	}

	/**
	 * @param string
	 */
	public void setAvgPnt_A(String string) {
		avgPnt_A = string;
	}

	/**
	 * @param string
	 */
	public void setAvgPnt_S(String string) {
		avgPnt_S = string;
	}

	/**
	 * @param string
	 */
	public void setAvgScoreRate_A(String string) {
		avgScoreRate_A = string;
	}

	/**
	 * @param string
	 */
	public void setAvgScoreRate_S(String string) {
		avgScoreRate_S = string;
	}

	/**
	 * @param string
	 */
	public void setBundleCode(String string) {
		bundleCode = string;
	}

	/**
	 * @param string
	 */
	public void setDisplay_no(String string) {
		display_no = string;
	}

	/**
	 * @param string
	 */
	public void setExamCode(String string) {
		examCode = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @param string
	 */
	public void setPersonId(String string) {
		personId = string;
	}

	/**
	 * @param string
	 */
	public void setQallotpnt(String string) {
		qallotpnt = string;
	}

	/**
	 * @param string
	 */
	public void setQscore(String string) {
		qscore = string;
	}

	/**
	 * @param string
	 */
	public void setQuestionName1(String string) {
		questionName1 = string;
	}

	/**
	 * @param string
	 */
	public void setQuestionName2(String string) {
		questionName2 = string;
	}

	/**
	 * @param string
	 */
	public void setQuestionNo(String string) {
		questionNo = string;
	}

	/**
	 * @param string
	 */
	public void setScoreRate(String string) {
		scoreRate = string;
	}

	/**
	 * @param string
	 */
	public void setSubCode(String string) {
		subCode = string;
	}

	public String getAvgScoreRate_LEVEL() {
		return avgScoreRate_LEVEL;
	}

	public void setAvgScoreRate_LEVEL(String string) {
		this.avgScoreRate_LEVEL = string;
	}

}
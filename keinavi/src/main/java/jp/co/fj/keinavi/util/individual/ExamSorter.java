/*
 * 作成日: 2004/10/20
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.individual;

import java.util.Comparator;

import jp.co.fj.keinavi.data.individual.ExaminationData;

/**
 * @author Administrator
 * history
 *   Symbol Date       Person     Note
 *   [1]    2005.02.02 kondo      change 外部データ開放日が同一の場合、模試コード順にソートする内部クラスを追加（内部データ開放日用も追加）
 */
public class ExamSorter {

	public class SortByOutDataOpenDate implements Comparator{
		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {
			ExaminationData d1 = (ExaminationData)o1;
			ExaminationData d2 = (ExaminationData)o2;

			long d1Date = Long.parseLong(d1.getOutDataOpenData());
			long d2Date = Long.parseLong(d2.getOutDataOpenData());
			if (d1Date > d2Date) {
				return -1;
			} else if (d1Date < d2Date) {
				return 1;
			} else {
				return 0;
			}
		}
	}
	public class SortByOutDataOpenDate2 implements Comparator{
		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {
			ExaminationData d1 = (ExaminationData)o1;
			ExaminationData d2 = (ExaminationData)o2;

			long d1Date = Long.parseLong(d1.getOutDataOpenData());
			long d2Date = Long.parseLong(d2.getOutDataOpenData());
			if (d1Date > d2Date) {
				return 1;
			} else if (d1Date < d2Date) {
				return -1;
			} else {
				return 0;
			}
		}
	}
	
	public class SortByInDataOpenDate implements Comparator{
		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {
			ExaminationData d1 = (ExaminationData)o1;
			ExaminationData d2 = (ExaminationData)o2;

			long d1Date = Long.parseLong(d1.getInDataOpenDate());
			long d2Date = Long.parseLong(d2.getInDataOpenDate());
			if (d1Date > d2Date) {
				return -1;
			} else if (d1Date < d2Date) {
				return 1;
			} else {
				return 0;
			}
		}
	}
	
	public class SortByInDataOpenDate2 implements Comparator{
		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {
			ExaminationData d1 = (ExaminationData)o1;
			ExaminationData d2 = (ExaminationData)o2;

			long d1Date = Long.parseLong(d1.getInDataOpenDate());
			long d2Date = Long.parseLong(d2.getInDataOpenDate());
			if (d1Date > d2Date) {
				return 1;
			} else if (d1Date < d2Date) {
				return -1;
			} else {
				return 0;
			}
		}
	}
	
	public class SortByDispSequence implements Comparator{
		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {
			ExaminationData d1 = (ExaminationData)o1;
			ExaminationData d2 = (ExaminationData)o2;
			int d1Date = Integer.parseInt(d1.getDispSequence());
			int d2Date = Integer.parseInt(d2.getDispSequence());
			if (d1Date < d2Date) {
				return -1;
			} else if (d1Date > d2Date) {
				return 1;
			} else {
				return 0;
			}
		}
		
	}
	public class ReverseByDispSequence implements Comparator{
		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {
			ExaminationData d1 = (ExaminationData)o1;
			ExaminationData d2 = (ExaminationData)o2;
			int d1Date = Integer.parseInt(d1.getDispSequence());
			int d2Date = Integer.parseInt(d2.getDispSequence());
			if (d1Date > d2Date) {
				return -1;
			} else if (d1Date < d2Date) {
				return 1;
			} else {
				return 0;
			}
		}
		
	}
	
//[1] add start
	public class SortByOutDataOpenDate3 implements Comparator{
		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {
			ExaminationData d1 = (ExaminationData)o1;
			ExaminationData d2 = (ExaminationData)o2;

			long d1Date = Long.parseLong(d1.getOutDataOpenData());
			long d2Date = Long.parseLong(d2.getOutDataOpenData());
			
			if (d1Date < d2Date) {
				return 1;
			} else if (d1Date > d2Date) {
				return -1;
			} else {
				if(d1.getExamCd().compareTo(d2.getExamCd()) < 0) {
					return 1;
				} else if (d1.getExamCd().compareTo(d2.getExamCd()) > 0) {
					return -1;
				}
				return 0;
			}
		}
	}
	
	public class SortByInDataOpenDate3 implements Comparator{
		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {
			ExaminationData d1 = (ExaminationData)o1;
			ExaminationData d2 = (ExaminationData)o2;

			long d1Date = Long.parseLong(d1.getInDataOpenDate());
			long d2Date = Long.parseLong(d2.getInDataOpenDate());
			
			if (d1Date < d2Date) {
				return 1;
			} else if (d1Date > d2Date) {
				return -1;
			} else {
				if(d1.getExamCd().compareTo(d2.getExamCd()) < 0) {
					return 1;
				} else if (d1.getExamCd().compareTo(d2.getExamCd()) > 0) {
					return -1;
				}
				return 0;
			}
		}
	}
//[1] add end
}

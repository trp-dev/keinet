package jp.co.fj.keinavi.beans.com_set;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.fj.keinavi.data.com_set.cm.ClassData;
import jp.co.fj.keinavi.data.com_set.cm.ClassGradeData;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 担当クラスBean
 * 
 * 
 * 2005.02.24	Yoshimoto KAWAI - Totec
 * 				年度対応
 * 
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 * 
 * @author kawai
 * 
 */
public class ChargeClassBean extends DefaultBean implements Serializable {

	/** セッションキー */
	public static final String SESSION_KEY = "ChargeClassBean";
	
	private final String schoolCd; // 学校コード
	private final String loginId; // 利用者ID
	private final List yearList = new ArrayList(); // 含まれる年度のリスト
	private final boolean isContainComposite; // 複合クラスを含むかどうか
	
	// 年度をキーとして学年別のクラスデータのリストを保持する
	private final Map classYearMap = new HashMap();

	/**
	 * コンストラクタ
	 * 
	 * @param cd
	 * @param id
	 * @param year
	 */
	public ChargeClassBean(final String schoolCd, final String loginId, final boolean isContainComposite) {
		this.schoolCd = schoolCd;
		this.loginId = loginId;
		this.isContainComposite = isContainComposite;
	}

	/**
	 * コンストラクタ
	 * 
	 * @param cd
	 * @param id
	 * @param year
	 */
	public ChargeClassBean(final String schoolCd, final String loginId) {
		
		this(schoolCd, loginId, true);
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */;
	public void execute() throws SQLException, Exception {

		// 年度セット
		final Set yearSet = new HashSet();
		// 有効年度配列（現年度・前年度・前々年度）
		final String[] years = createValidYears();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// 通常クラスのSQL
			final Query query = QueryLoader.getInstance().load("cm01a");
			query.setStringArray(1, years);
			
			// 複合クラスも含むなら UNION ALL でくっつける
			if (isContainComposite) {
				query.append("UNION ALL ");
				query.append(QueryLoader.getInstance().load("cm01b").toString());
				query.setStringArray(2, years);
			}
			
			// ソート条件
			query.append("ORDER BY year DESC, grade_seq, classname");
				
			ps = conn.prepareStatement(query.toString());
			ps.setString(1, schoolCd); // 学校コード
			ps.setString(2, loginId); // 利用者ID
			ps.setString(3, loginId); // 利用者ID
			
			if (isContainComposite) {
				ps.setString(4, schoolCd); // 学校コード
				ps.setString(5, loginId); // 利用者ID
				ps.setString(6, loginId); // 利用者ID
			}

			// 前ループの年度
			String preYear = null;
			// 前ループの学年
			int preGrade = -1;
			// 学年別データ
			ClassGradeData grade = null;
			// 学年別データの入れ物
			List gradeList = null;
			
			rs = ps.executeQuery();
			while (rs.next()) {
				final ClassData data = new ClassData();
				data.setYear(rs.getString(1));
				data.setGrade(rs.getInt(2));
				data.setKey(rs.getString(3));
				data.setClassName(rs.getString(4));
				data.setExaminees(rs.getInt(5));
				
				yearSet.add(rs.getString(1));
				
				// 年度が前レコードと同じ場合
				if (data.getYear().equals(preYear)) {
					
					// 学年が前レコードと異なる場合は入れ物を切り替える
					if (data.getGrade() != preGrade) {
						grade = new ClassGradeData(data.getGrade());
						gradeList.add(grade);
					}
					
					grade.addClassData(data);
					
				// 年度が前レコードと異なる場合
				} else {
					// 年度別の入れ物を作る
					gradeList = new ArrayList();
					grade = new ClassGradeData(data.getGrade());
					grade.addClassData(data);
					gradeList.add(grade);
					classYearMap.put(data.getYear(), gradeList);
				}
				
				preYear = data.getYear();
				preGrade = data.getGrade();
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
		
		// 年度リストを初期化する
		yearList.addAll(yearSet);
		Collections.sort(yearList);
		Collections.reverse(yearList); // 逆順
	}

	// 有効年度の配列を作る
	private String[] createValidYears() {
		final int current = Integer.parseInt(KNUtil.getCurrentYear());
		final String[] years = new String[3];
		for (int i = 0; i < years.length; i++) {
			years[i] = (current - i) + "";
		}
		return years;
	}

	/**
	 * @return yearList を戻します。
	 */
	public List getYearList() {
		return yearList;
	}

	/**
	 * @return classYearMap を戻します。
	 */
	public Map getClassYearMap() {
		return classYearMap;
	}

}

package jp.co.fj.keinavi.beans.sales;

import java.sql.PreparedStatement;

import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 特例成績データダウンロード画面の取消処理Beanです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD207CancelBean extends DefaultBean {

	/** serialVersionUID */
	private static final long serialVersionUID = -115384020397390892L;

	/** 申請ID */
	private final String appId;

	/**
	 * コンストラクタです。
	 */
	public SD207CancelBean(String appId) {
		this.appId = appId;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance()
					.load("sd207_cancel").toString());
			ps.setString(1, appId);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

}

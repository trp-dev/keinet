/*
 * 作成日: 2004/10/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.admin;

import java.io.Serializable;

/**
 * 生徒情報管理用クラスデータ
 * 
 * @author kawai
 */
public class MClassData implements Serializable {

	private String year; // 年度
	private String grade; // 学年
	private String className; // クラス

	/**
	 * コンストラクタ
	 */
	public MClassData() {
	}

	/**
	 * コンストラクタ
	 */
	public MClassData(String year, String grade, String className) {
		this.year = year;
		this.grade = grade;
		this.className = className;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		MClassData data = (MClassData) obj;

		return this.year.equals(data.year)
			&& this.grade.equals(data.grade)
			&& this.className.equals(data.className);
	}

	/**
	 * @return
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @return
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * @return
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param string
	 */
	public void setClassName(String string) {
		className = string;
	}

	/**
	 * @param string
	 */
	public void setGrade(String string) {
		grade = string;
	}

	/**
	 * @param string
	 */
	public void setYear(String string) {
		year = string;
	}

}

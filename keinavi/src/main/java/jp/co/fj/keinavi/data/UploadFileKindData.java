package jp.co.fj.keinavi.data;

/**
 *
 * アップロードファイル種類Data
 * 
 * <2010年度マーク高２模試対応>
 * 2011.01.24	Tomohisa YAMADA
 * 				新規作成
 * 
 */
public class UploadFileKindData {

	/**
	 * ファイル種類ID
	 */
	private String fileTypeID;
	
	/**
	 * ファイル種類名
	 */
	private String fileTypeName;
	
	/**
	 * ファイル種類短縮名
	 */
	private String fileTypeAbbrname;

	/**
	 * コンストラクタ
	 * @param fileTypeID
	 * @param fileTypeName
	 * @param fileTypeAbbrname
	 */
	public UploadFileKindData(String fileTypeID, String fileTypeName,
			String fileTypeAbbrname) {
		this.fileTypeID = fileTypeID;
		this.fileTypeName = fileTypeName;
		this.fileTypeAbbrname = fileTypeAbbrname;
	}

	public String getFileTypeID() {
		return fileTypeID;
	}

	public String getFileTypeName() {
		return fileTypeName;
	}

	public String getFileTypeAbbrname() {
		return fileTypeAbbrname;
	}
	
}

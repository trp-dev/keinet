package jp.co.fj.keinavi.forms.com_set;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM404Form extends BaseForm {

	// 県コードの配列
	private String[] pref = null;
	// 地区コードの配列
	private String[] block = null;
	// 国私区分コードの配列
	private String[] univDiv = null;
	// ボタン
	private String button = null;
	// 検索モード（大学名／大学コード）
	private String searchMode = null;
	// 大学名／大学コードの文字列
	private String searchStr = null;

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * @return
	 */
	public String getButton() {
		return button;
	}

	/**
	 * @return
	 */
	public String[] getPref() {
		return pref;
	}

	/**
	 * @return
	 */
	public String[] getUnivDiv() {
		return univDiv;
	}

	/**
	 * @param string
	 */
	public void setButton(String string) {
		button = string;
	}

	/**
	 * @param strings
	 */
	public void setPref(String[] strings) {
		pref = strings;
	}

	/**
	 * @param strings
	 */
	public void setUnivDiv(String[] strings) {
		univDiv = strings;
	}

	/**
	 * @return
	 */
	public String getSearchMode() {
		return searchMode;
	}

	/**
	 * @return
	 */
	public String getSearchStr() {
		return searchStr;
	}

	/**
	 * @param string
	 */
	public void setSearchMode(String string) {
		searchMode = string;
	}

	/**
	 * @param string
	 */
	public void setSearchStr(String string) {
		searchStr = string;
	}

	/**
	 * @return
	 */
	public String[] getBlock() {
		return block;
	}

	/**
	 * @param strings
	 */
	public void setBlock(String[] strings) {
		block = strings;
	}

}

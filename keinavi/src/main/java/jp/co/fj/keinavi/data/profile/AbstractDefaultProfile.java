/*
 * 作成日: 2004/08/11
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.profile;

import java.util.HashMap;
import java.util.Map;

import jp.co.fj.keinavi.interfaces.IProfileItem;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public abstract class AbstractDefaultProfile {

	// カテゴリマップ
	protected Map categoryMap = new HashMap();

	/**
	 * 一括出力対象のデフォルト値をセットする
	 * @param category
	 */
	protected void setPrintOutTarget(String category) {
		getItemMap(category).put(IProfileItem.PRINT_OUT_TARGET, new Short("0"));
	}

	/**
	 * 型のデフォルト値をセットする
	 * @param category
	 */
	protected void setType(String category) {
		getItemMap(category).put(IProfileItem.TYPE_USAGE, new Short("1"));
		//getItemMap(category).put(IProfileItem.TYPE_IND, new ComSubjectData());
	}

	/**
	 * 科目のデフォルト値をセットする
	 * @param category
	 */
	protected void setCourse(String category) {
		getItemMap(category).put(IProfileItem.COURSE_USAGE, new Short("1"));
		//getItemMap(category).put(IProfileItem.COURSE_IND, new ComSubjectData());
	}

	/**
	 * セキュリティスタンプのデフォルト値をセットする
	 * @param category
	 */
	protected void setStamp(String category) {
		getItemMap(category).put(IProfileItem.PRINT_STAMP, new Short("4"));
	}

	/**
	 * アイテムマップを取得する
	 * @param key
	 * @return
	 */
	protected Map getItemMap(String key) {
		// なければ作っておく
		if (!categoryMap.containsKey(key)) {
			categoryMap.put(key, new HashMap());
		}
		return (Map)categoryMap.get(key);
	}

	/**
	 * @return
	 */
	public Map getCategoryMap() {
		return categoryMap;
	}

}

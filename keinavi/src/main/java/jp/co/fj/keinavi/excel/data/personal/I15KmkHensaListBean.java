package jp.co.fj.keinavi.excel.data.personal;

/**
 * 個人成績分析−受験用資料−受験大学スケジュール表 教科別偏差値データリスト
 * 作成日: 2004/07/20
 * @author	A.Iwata
 *
 * 2009.12.01   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 *
 */
public class I15KmkHensaListBean {

    //教科コード
    private String strKyokaCd = "";
    //教科名
    private String strKyokamei = "";
    //型・科目名
    private String strKmkmei = "";
    //偏差値
    private float floHensa = 0;
    //学力レベル
    private String strScholarLevel = "";
    //基礎科目フラグ
    private String basicFlg;

    /*----------*/
    /* Get      */
    /*----------*/
    public String getStrKyokamei() {
        return this.strKyokamei;
    }
    public String getStrKmkmei() {
        return this.strKmkmei;
    }
    public float getFloHensa() {
        return this.floHensa;
    }
    public String getStrKyokaCd() {
        return this.strKyokaCd;
    }
    public String getStrScholarLevel() {
        return strScholarLevel;
    }
    public String getBasicFlg() {
        return basicFlg;
    }

    /*---------------*/
    /* Set */
    /*---------------*/
    public void setStrKyokamei(String strKyokamei) {
        this.strKyokamei = strKyokamei;
    }
    public void setStrKmkmei(String strKmkmei) {
        this.strKmkmei = strKmkmei;
    }
    public void setFloHensa(float floHensa) {
        this.floHensa = floHensa;
    }
    public void setStrKyokaCd(String strKyokaCd) {
        this.strKyokaCd = strKyokaCd;
    }
    public void setStrScholarLevel(String string) {
        this.strScholarLevel = string;
    }
    public void setBasicFlg(String basicFlg) {
        this.basicFlg = basicFlg;
    }

}

package jp.co.fj.keinavi.util.taglib;

/**
 *
 * keinavi 用に制限をなくした otherwise タグクラス
 * 
 * 2006/08/25    [新規作成]
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class KNOtherwiseTag extends ExamWhenTag {    
    
    protected boolean condition() {
        return true;
    }
    
}

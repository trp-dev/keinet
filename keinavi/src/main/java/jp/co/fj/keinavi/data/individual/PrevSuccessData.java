/*
 * 作成日: 2004/07/29
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

import java.io.Serializable;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class PrevSuccessData implements Serializable{
	
	private String examYear;
	private String univCd;
	private String facultyCd;
	private String deptCd;
	private String examTypeCd;
	private String subCd;
	private String sucNum;
	private String sucAvgDev;
	
	//テーブルにはないがあると便利
	private String univName;

	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getExamTypeCd() {
		return examTypeCd;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getSubCd() {
		return subCd;
	}

	/**
	 * @return
	 */
	public String getSucAvgDev() {
		return sucAvgDev;
	}

	/**
	 * @return
	 */
	public String getSucNum() {
		return sucNum;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @return
	 */
	public String getUnivName() {
		return univName;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setExamTypeCd(String string) {
		examTypeCd = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setSubCd(String string) {
		subCd = string;
	}

	/**
	 * @param string
	 */
	public void setSucAvgDev(String string) {
		sucAvgDev = string;
	}

	/**
	 * @param string
	 */
	public void setSucNum(String string) {
		sucNum = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

	/**
	 * @param string
	 */
	public void setUnivName(String string) {
		univName = string;
	}

}

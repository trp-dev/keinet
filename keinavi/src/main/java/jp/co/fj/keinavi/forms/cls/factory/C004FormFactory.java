/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.cls.factory;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.forms.cls.CMaxForm;

import com.fjh.forms.ActionForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class C004FormFactory extends AbstractCFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		// アクションフォーム
		CMaxForm form = super.createCMaxForm(request);
		// アイテムマップ
		Map map = getItemMap(request);
		
		//	型・共通項目設定利用
		getCommonTypeAndAnalyzeType( map, form );
		//  科目・共通項目設定利用
		getCommonCourseAndAnalyzeCourse( map, form );
		//  表
		getChart( map, form);
		//  型・科目別比較
		getCompPrev( map, form );
		//  生徒の表示順序
		getStrudentOrder( map, form);
		//  印刷対象クラス
		getPrintClass( map, form );
		//  変動幅
		getFluctRange( map, form );
		//  偏差値
		getDeviation( map, form );
		//  セキュリティスタンプ
		getStamp( map, form );


		return form;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
		CMaxForm form = (CMaxForm)request.getAttribute("form");
		// アイテムマップ
		Map map = getItemMap(request);
		
		// 型・科目別比較が有効な場合のみ評価する
		if (form.getOptionItem() != null && form.getOptionItem().length > 0) {
			// 型・共通項目設定利用		
			setCommonTypeAndAnalyzeType( map, form );
			//  科目・共通項目設定利用
			setCommonCourseAndAnalyzeCourse( map, form );
		}

		//  表
		setChart( map, form);
		//  型・科目別比較
		setCompPrev( map, form );
		//  生徒の表示順序
		setStrudentOrder( map, form);
		//  印刷対象クラス
		setPrintClass( map, form );
		//  変動幅
		setFluctRange( map, form );
		//  偏差値
		setDeviation( map, form );
		//  セキュリティスタンプ
		setStamp( map, form );
		
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap("030103");
	}

}

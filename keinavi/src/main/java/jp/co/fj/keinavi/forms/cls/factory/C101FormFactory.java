/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.cls.factory;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.cls.C001Form;
import jp.co.fj.keinavi.util.CollectionUtil;

import com.fjh.forms.ActionForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class C101FormFactory extends AbstractCFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		// アクションフォーム
		C001Form form = super.createC001Form(request);
		//プロファイルにフォームの値をセットする
		Profile profile = (Profile)request.getSession().getAttribute(Profile.SESSION_KEY);
		Set outItem = CollectionUtil.array2Set(form.getOutItem());
		//成績概況
		if( profile.getItemMap("030201").get("1301") != null){
			if ( ((Short)profile.getItemMap("030201").get("1301")).shortValue() == 1){
				outItem.add("CompScore");
			}
		} 
		//偏差値分布
		if( profile.getItemMap("030202").get("1301") != null){
			if ( ((Short)profile.getItemMap("030202").get("1301")).shortValue() == 1){
				outItem.add("CompDev");
			}
		}
		//設問別成績
		if( profile.getItemMap("030203").get("1301") != null){
			if ( ((Short)profile.getItemMap("030203").get("1301")).shortValue() == 1){
				outItem.add("CompQue");
			}
		}
		//志望大学評価別人数
		if( profile.getItemMap("030204").get("1301") != null){
			if ( ((Short)profile.getItemMap("030204").get("1301")).shortValue() == 1){
				outItem.add("CompUniv");
			}
		}
		form.setOutItem((String[])outItem.toArray(new String[0]));
		
		return form;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
		C001Form form = (C001Form)request.getAttribute("form");
		//プロファイルにフォームの値をセットする
		Profile profile = (Profile)request.getSession().getAttribute(Profile.SESSION_KEY);
		//一括出力対象
		Set outItem = CollectionUtil.array2Set(form.getOutItem());
		
		//成績概況
		if (outItem.contains("CompScore")) profile.getItemMap("030201").put("1301", Short.valueOf("1"));
									else profile.getItemMap("030201").put("1301", Short.valueOf("0")); 
		//偏差値分布
		if (outItem.contains("CompDev")) profile.getItemMap("030202").put("1301", Short.valueOf("1"));
									else profile.getItemMap("030202").put("1301", Short.valueOf("0")); 
		//設問別成績
		if (outItem.contains("CompQue")) profile.getItemMap("030203").put("1301", Short.valueOf("1"));
									else profile.getItemMap("030203").put("1301", Short.valueOf("0")); 
		//志望大学評価別人数
		if (outItem.contains("CompUniv")) profile.getItemMap("030204").put("1301", Short.valueOf("1"));
									else profile.getItemMap("030204").put("1301", Short.valueOf("0"));
		
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap("030201");
	}

}

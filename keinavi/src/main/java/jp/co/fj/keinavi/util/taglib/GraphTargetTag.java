package jp.co.fj.keinavi.util.taglib;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import jp.co.fj.keinavi.beans.graph.AbstractGraphCheckerBean;
import jp.co.fj.keinavi.beans.graph.GraphCheckerBeanFactory;

/**
 *
 * グラフ出力対象タグ
 * 
 * 2006.07.27	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class GraphTargetTag extends TagSupport {

	// 出力項目
	private String item;
	// 画面ID
	private String id;

	/**
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {
		
		try {
			pageContext.getOut().print(execute());
		} catch (final IOException e) {
			final JspException j = new JspException(e.getMessage());
			j.initCause(e);
			throw j;
		}

		return EVAL_PAGE;
	}
	
	private int execute() throws JspException {
		
		// HTTPリクエスト
		final HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
		// 機能ごとのBean
		final AbstractGraphCheckerBean bean = GraphCheckerBeanFactory.getInstance(id);

		// 一括出力画面と詳細画面の判断
		final boolean isBundle = id != null;

		// 一括出力画面かつ出力対象 もしくは 詳細画面 であるなら評価する
		if ((isBundle && bean.isBundleTarget(request)) || !isBundle) {
			// 型
			if ("type".equals(item)) {
				return bean.checkTypeConfig(request, isBundle);
			// 科目
			} else if ("course".equals(item)) {
				return bean.checkCourseConfig(request, isBundle);
			// 比較対象クラス
			} else if ("class".equals(item)) {
				return bean.checkClassConfig(request, isBundle);
			// 比較対象高校
			} else if ("school".equals(item)) {
				return bean.checkSchoolConfig(request, isBundle);
			}
		}
		
		return 0;
	}
	
	/**
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	public int doStartTag() throws JspException {
		return SKIP_BODY;
	}

	/**
	 * @param pId 設定する id。
	 */
	public void setId(String pId) {
		id = pId;
	}

	/**
	 * @param pItem 設定する item。
	 */
	public void setItem(String pItem) {
		item = pItem;
	}

}

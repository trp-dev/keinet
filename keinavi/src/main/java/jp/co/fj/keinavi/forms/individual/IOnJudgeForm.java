/*
 * 作成日: 2004/11/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * @author Administrator
 *
 * 2005.4.26 	K.Kondo 	[1]画面スクロール座標の追加
 */
public class IOnJudgeForm extends ActionForm{

	public static String SESSION_KEY = "form";

	/**
	 * 本当のforwardとbackward
	 */
	private String trueForward;
	private String trueBackward;

	/** 共通 */
	private String targetPersonId;//個人ＩＤ
	
	/** プロファイル */
	//1.選択中の生徒すべて 2 : 表示中の生徒一人
	private String judgeStudent;
	//セキュリティスタンプ
	private String printStamp;
	//	条件保存されていない生徒の処理
	private String indStudentUsage;
	
	/** I302 */
	//出力フォーマット
	private String textFormat[];
	
	/** I401 */
	private String targetYear;
	private String targetExam;
	private String targetExamYear;
	private String targetExamCode;
	private String targetExamName;
	//個人成績分析共通-印刷対象生徒（判定対象生徒)
	private String printStudent;
	//成績分析面談シート
	private String interviewSheet;
	//面談用帳票
	private String[] interviewForms;
	//個人成績用
	private String report;
	//受験用資料
	private String examDoc;
	//印刷ボタン押下時パラメータ
	private String printFlag;
	
	/** I204 アクション byUniv, byCond, byAuto */
	private String button;
	
	/** I204 大学指定の場合 */
	//選択されたカンマ区切りのunivを識別する文字列配列
	private String[] univValue;
	private String[] univValueName;
	// 検索モード（大学名／大学コード）
	private String searchMode = null;
	// 大学名／大学コードの文字列
	private String searchStr = null;
	// Char検索時のchar value
	private String charValue = null;
	
	/** I204 条件指定の場合 */
	private String[] block;//地区コード(特にとる必要はない)
	private String[] pref;//県コード
	private String searchStemmaDiv;//系統選択　中・文・理
	private String[] stemma2Code;//中系統コード
	private String[] stemma11Code;//小系統文系コード
	private String[] stemma10Code;//小系統理系コード
	private String[] stemmaCode;//分野コード
	private String descImposeDiv;// 1:（センター利用大）2次・独自試験を課さない 
	private String subImposeDiv;//試験科目課し区分
	private String[] imposeSub;//課し試験科目
	private String[] schoolDivProviso;//学校区分条件
	//private String startDateProviso;//入試開始日条件
	private String startMonth;
	private String startDate;
	//private String endDateProviso;//入試終了日条件
	private String endMonth;
	private String endDate;
	private String raingDiv;//評価試験区分
	private String[] raingProviso;//評価範囲条件
	private String[] outOfTergetProviso;//対象外条件
	private String dateSearchDiv;//入試日区分
	//系統コード(中系統コード)
	private String mstemma0011;//文・人文
	private String mstemma0021;
	private String mstemma0022;
	private String mstemma0023;
	private String mstemma0031;
	private String mstemma0032;
	private String mstemma0041;
	private String mstemma0042;
	private String mstemma0043;
	private String mstemma0051;
	private String mstemma0061;
	private String mstemma0071;
	private String mstemma0062;//芸術・体育・他
	//系統コード(小系統コード)
	private String sstemma01;//文学
	private String sstemma02;
	private String sstemma03;
	private String sstemma04;
	private String sstemma05;
	private String sstemma06;
	private String sstemma07;
	private String sstemma08;
	private String sstemma09;
	private String sstemma10;
	private String sstemma11;
	private String sstemma12;
	private String sstemma13;
	private String sstemma14;
	private String sstemma15;
	private String sstemma16;
	private String sstemma17;
	private String sstemma18;
	private String sstemma19;
	private String sstemma20;
	private String sstemma21;
	private String sstemma22;
	private String sstemma23;
	private String sstemma24;//芸術・体育・他
	//系統コード(分野コード)
	private String region0100;//日本文		
	private String region0200;		
	private String region0300;		
	private String region0400;		
	private String region0500;		
	private String region0600;		
	private String region0700;		
	private String region0800;		
	private String region0900;		
	private String region1000;		
	private String region1100;		
	private String region1200;		
	private String region1300;		
	private String region1400;		
	private String region1500;		
	private String region1600;		
	private String region1700;		
	private String region1800;		
	private String region1900;		
	private String region2000;		
	private String region2100;		
	private String region2200;		
	private String region2300;		
	private String region2400;		
	private String region2500;		
	private String region2600;		
	private String region2700;		
	private String region2800;		
	private String region2900;		
	private String region3000;		
	private String region3100;		
	private String region3200;		
	private String region3300;		
	private String region3400;		
	private String region3500;		
	private String region3600;		
	private String region3700;		
	private String region3800;		
	private String region3900;		
	private String region4000;		
	private String region4100;		
	private String region4200;		
	private String region4300;		
	private String region4400;		
	private String region4500;		
	private String region4600;		
	private String region4700;		
	private String region4800;		
	private String region4900;		
	private String region5000;		
	private String region5100;		
	private String region5200;		
	private String region5300;		
	private String region5400;		
	private String region5500;		
	private String region5600;		
	private String region5700;		
	private String region5800;		
	private String region5900;		
	private String region6000;		
	private String region6100;		
	private String region6200;		
	private String region6300;		
	private String region6400;		
	private String region6500;		
	private String region6600;		
	private String region6700;		
	private String region6800;		
	private String region6900;		
	private String region7000;		
	private String region7100;		
	private String region7200;		
	private String region7300;		
	private String region7400;		
	private String region7500;		
	private String region7600;		
	private String region7700;		
	private String region7800;//スポーツ・健康		
	private boolean mstemmaflg = false;//中系統フラグ
	private boolean stemmaAflg = false;//文系フラグ
	private boolean stemmaSflg = false;//理系フラグ
	private boolean regionflg01 = false;//文学
	private boolean regionflg02 = false;
	private boolean regionflg03 = false;
	private boolean regionflg04 = false;
	private boolean regionflg05 = false;
	private boolean regionflg06 = false;
	private boolean regionflg07 = false;
	private boolean regionflg08 = false;
	private boolean regionflg09 = false;
	private boolean regionflg10 = false;
	private boolean regionflg11 = false;
	private boolean regionflg12 = false;
	private boolean regionflg13 = false;
	private boolean regionflg14 = false;
	private boolean regionflg15 = false;
	private boolean regionflg16 = false;
	private boolean regionflg17 = false;
	private boolean regionflg18 = false;
	private boolean regionflg19 = false;
	private boolean regionflg20 = false;
	private boolean regionflg21 = false;
	private boolean regionflg22 = false;
	private boolean regionflg23 = false;
	private boolean regionflg24 = false;//芸術・体育・他
	//条件保存を行ったかどうか？フラグ
	private boolean saveflg = false;
	private String thisYear;//DatePicker用に必要
	
	private String scrollX;//[1] add サブミット前、表示座標Ｘ
	private String scrollY;//[1] add サブミット前、表示座標Ｙ	
	
	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ
		
	}

	/**
	 * @return
	 */
	public String[] getBlock() {
		return block;
	}

	/**
	 * @return
	 */
	public String getButton() {
		return button;
	}

	/**
	 * @return
	 */
	public String getDateSearchDiv() {
		return dateSearchDiv;
	}

	/**
	 * @return
	 */
	public String getDescImposeDiv() {
		return descImposeDiv;
	}

	/**
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @return
	 */
	public String getEndMonth() {
		return endMonth;
	}

	/**
	 * @return
	 */
	public String[] getImposeSub() {
		return imposeSub;
	}

	/**
	 * @return
	 */
	public String getIndStudentUsage() {
		return indStudentUsage;
	}

	/**
	 * @return
	 */
	public String getJudgeStudent() {
		return judgeStudent;
	}

	/**
	 * @return
	 */
	public String[] getOutOfTergetProviso() {
		return outOfTergetProviso;
	}

	/**
	 * @return
	 */
	public String[] getPref() {
		return pref;
	}

	/**
	 * @return
	 */
	public String getPrintStamp() {
		return printStamp;
	}

	/**
	 * @return
	 */
	public String getRaingDiv() {
		return raingDiv;
	}

	/**
	 * @return
	 */
	public String[] getRaingProviso() {
		return raingProviso;
	}

	/**
	 * @return
	 */
	public String[] getSchoolDivProviso() {
		return schoolDivProviso;
	}

	/**
	 * @return
	 */
	public String getSearchStemmaDiv() {
		return searchStemmaDiv;
	}

	/**
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @return
	 */
	public String getStartMonth() {
		return startMonth;
	}

	/**
	 * @return
	 */
	public String[] getStemma10Code() {
		return stemma10Code;
	}

	/**
	 * @return
	 */
	public String[] getStemma11Code() {
		return stemma11Code;
	}

	/**
	 * @return
	 */
	public String[] getStemma2Code() {
		return stemma2Code;
	}

	/**
	 * @return
	 */
	public String[] getStemmaCode() {
		return stemmaCode;
	}

	/**
	 * @return
	 */
	public String getSubImposeDiv() {
		return subImposeDiv;
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @return
	 */
	public String[] getUnivValue() {
		return univValue;
	}

	/**
	 * @return
	 */
	public String[] getUnivValueName() {
		return univValueName;
	}

	/**
	 * @param strings
	 */
	public void setBlock(String[] strings) {
		block = strings;
	}

	/**
	 * @param string
	 */
	public void setButton(String string) {
		button = string;
	}

	/**
	 * @param string
	 */
	public void setDateSearchDiv(String string) {
		dateSearchDiv = string;
	}

	/**
	 * @param string
	 */
	public void setDescImposeDiv(String string) {
		descImposeDiv = string;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string) {
		endDate = string;
	}

	/**
	 * @param string
	 */
	public void setEndMonth(String string) {
		endMonth = string;
	}

	/**
	 * @param strings
	 */
	public void setImposeSub(String[] strings) {
		imposeSub = strings;
	}

	/**
	 * @param string
	 */
	public void setIndStudentUsage(String string) {
		indStudentUsage = string;
	}

	/**
	 * @param string
	 */
	public void setJudgeStudent(String string) {
		judgeStudent = string;
	}

	/**
	 * @param strings
	 */
	public void setOutOfTergetProviso(String[] strings) {
		outOfTergetProviso = strings;
	}

	/**
	 * @param strings
	 */
	public void setPref(String[] strings) {
		pref = strings;
	}

	/**
	 * @param string
	 */
	public void setPrintStamp(String string) {
		printStamp = string;
	}

	/**
	 * @param string
	 */
	public void setRaingDiv(String string) {
		raingDiv = string;
	}

	/**
	 * @param strings
	 */
	public void setRaingProviso(String[] strings) {
		raingProviso = strings;
	}

	/**
	 * @param strings
	 */
	public void setSchoolDivProviso(String[] strings) {
		schoolDivProviso = strings;
	}

	/**
	 * @param string
	 */
	public void setSearchStemmaDiv(String string) {
		searchStemmaDiv = string;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string) {
		startDate = string;
	}

	/**
	 * @param string
	 */
	public void setStartMonth(String string) {
		startMonth = string;
	}

	/**
	 * @param strings
	 */
	public void setStemma10Code(String[] strings) {
		stemma10Code = strings;
	}

	/**
	 * @param strings
	 */
	public void setStemma11Code(String[] strings) {
		stemma11Code = strings;
	}

	/**
	 * @param strings
	 */
	public void setStemma2Code(String[] strings) {
		stemma2Code = strings;
	}

	/**
	 * @param strings
	 */
	public void setStemmaCode(String[] strings) {
		stemmaCode = strings;
	}

	/**
	 * @param string
	 */
	public void setSubImposeDiv(String string) {
		subImposeDiv = string;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @param strings
	 */
	public void setUnivValue(String[] strings) {
		univValue = strings;
	}

	/**
	 * @param strings
	 */
	public void setUnivValueName(String[] strings) {
		univValueName = strings;
	}

	/**
	 * @return
	 */
	public String getTrueBackward() {
		return trueBackward;
	}

	/**
	 * @return
	 */
	public String getTrueForward() {
		return trueForward;
	}

	/**
	 * @param string
	 */
	public void setTrueBackward(String string) {
		trueBackward = string;
	}

	/**
	 * @param string
	 */
	public void setTrueForward(String string) {
		trueForward = string;
	}

	/**
	 * @return
	 */
	public String getCharValue() {
		return charValue;
	}

	/**
	 * @return
	 */
	public String getMstemma0011() {
		return mstemma0011;
	}

	/**
	 * @return
	 */
	public String getMstemma0021() {
		return mstemma0021;
	}

	/**
	 * @return
	 */
	public String getMstemma0022() {
		return mstemma0022;
	}

	/**
	 * @return
	 */
	public String getMstemma0023() {
		return mstemma0023;
	}

	/**
	 * @return
	 */
	public String getMstemma0031() {
		return mstemma0031;
	}

	/**
	 * @return
	 */
	public String getMstemma0032() {
		return mstemma0032;
	}

	/**
	 * @return
	 */
	public String getMstemma0041() {
		return mstemma0041;
	}

	/**
	 * @return
	 */
	public String getMstemma0042() {
		return mstemma0042;
	}

	/**
	 * @return
	 */
	public String getMstemma0043() {
		return mstemma0043;
	}

	/**
	 * @return
	 */
	public String getMstemma0051() {
		return mstemma0051;
	}

	/**
	 * @return
	 */
	public String getMstemma0061() {
		return mstemma0061;
	}

	/**
	 * @return
	 */
	public String getMstemma0062() {
		return mstemma0062;
	}

	/**
	 * @return
	 */
	public String getMstemma0071() {
		return mstemma0071;
	}

	/**
	 * @return
	 */
	public boolean isMstemmaflg() {
		return mstemmaflg;
	}

	/**
	 * @return
	 */
	public String getRegion0100() {
		return region0100;
	}

	/**
	 * @return
	 */
	public String getRegion0200() {
		return region0200;
	}

	/**
	 * @return
	 */
	public String getRegion0300() {
		return region0300;
	}

	/**
	 * @return
	 */
	public String getRegion0400() {
		return region0400;
	}

	/**
	 * @return
	 */
	public String getRegion0500() {
		return region0500;
	}

	/**
	 * @return
	 */
	public String getRegion0600() {
		return region0600;
	}

	/**
	 * @return
	 */
	public String getRegion0700() {
		return region0700;
	}

	/**
	 * @return
	 */
	public String getRegion0800() {
		return region0800;
	}

	/**
	 * @return
	 */
	public String getRegion0900() {
		return region0900;
	}

	/**
	 * @return
	 */
	public String getRegion1000() {
		return region1000;
	}

	/**
	 * @return
	 */
	public String getRegion1100() {
		return region1100;
	}

	/**
	 * @return
	 */
	public String getRegion1200() {
		return region1200;
	}

	/**
	 * @return
	 */
	public String getRegion1300() {
		return region1300;
	}

	/**
	 * @return
	 */
	public String getRegion1400() {
		return region1400;
	}

	/**
	 * @return
	 */
	public String getRegion1500() {
		return region1500;
	}

	/**
	 * @return
	 */
	public String getRegion1600() {
		return region1600;
	}

	/**
	 * @return
	 */
	public String getRegion1700() {
		return region1700;
	}

	/**
	 * @return
	 */
	public String getRegion1800() {
		return region1800;
	}

	/**
	 * @return
	 */
	public String getRegion1900() {
		return region1900;
	}

	/**
	 * @return
	 */
	public String getRegion2000() {
		return region2000;
	}

	/**
	 * @return
	 */
	public String getRegion2100() {
		return region2100;
	}

	/**
	 * @return
	 */
	public String getRegion2200() {
		return region2200;
	}

	/**
	 * @return
	 */
	public String getRegion2300() {
		return region2300;
	}

	/**
	 * @return
	 */
	public String getRegion2400() {
		return region2400;
	}

	/**
	 * @return
	 */
	public String getRegion2500() {
		return region2500;
	}

	/**
	 * @return
	 */
	public String getRegion2600() {
		return region2600;
	}

	/**
	 * @return
	 */
	public String getRegion2700() {
		return region2700;
	}

	/**
	 * @return
	 */
	public String getRegion2800() {
		return region2800;
	}

	/**
	 * @return
	 */
	public String getRegion2900() {
		return region2900;
	}

	/**
	 * @return
	 */
	public String getRegion3000() {
		return region3000;
	}

	/**
	 * @return
	 */
	public String getRegion3100() {
		return region3100;
	}

	/**
	 * @return
	 */
	public String getRegion3200() {
		return region3200;
	}

	/**
	 * @return
	 */
	public String getRegion3300() {
		return region3300;
	}

	/**
	 * @return
	 */
	public String getRegion3400() {
		return region3400;
	}

	/**
	 * @return
	 */
	public String getRegion3500() {
		return region3500;
	}

	/**
	 * @return
	 */
	public String getRegion3600() {
		return region3600;
	}

	/**
	 * @return
	 */
	public String getRegion3700() {
		return region3700;
	}

	/**
	 * @return
	 */
	public String getRegion3800() {
		return region3800;
	}

	/**
	 * @return
	 */
	public String getRegion3900() {
		return region3900;
	}

	/**
	 * @return
	 */
	public String getRegion4000() {
		return region4000;
	}

	/**
	 * @return
	 */
	public String getRegion4100() {
		return region4100;
	}

	/**
	 * @return
	 */
	public String getRegion4200() {
		return region4200;
	}

	/**
	 * @return
	 */
	public String getRegion4300() {
		return region4300;
	}

	/**
	 * @return
	 */
	public String getRegion4400() {
		return region4400;
	}

	/**
	 * @return
	 */
	public String getRegion4500() {
		return region4500;
	}

	/**
	 * @return
	 */
	public String getRegion4600() {
		return region4600;
	}

	/**
	 * @return
	 */
	public String getRegion4700() {
		return region4700;
	}

	/**
	 * @return
	 */
	public String getRegion4800() {
		return region4800;
	}

	/**
	 * @return
	 */
	public String getRegion4900() {
		return region4900;
	}

	/**
	 * @return
	 */
	public String getRegion5000() {
		return region5000;
	}

	/**
	 * @return
	 */
	public String getRegion5100() {
		return region5100;
	}

	/**
	 * @return
	 */
	public String getRegion5200() {
		return region5200;
	}

	/**
	 * @return
	 */
	public String getRegion5300() {
		return region5300;
	}

	/**
	 * @return
	 */
	public String getRegion5400() {
		return region5400;
	}

	/**
	 * @return
	 */
	public String getRegion5500() {
		return region5500;
	}

	/**
	 * @return
	 */
	public String getRegion5600() {
		return region5600;
	}

	/**
	 * @return
	 */
	public String getRegion5700() {
		return region5700;
	}

	/**
	 * @return
	 */
	public String getRegion5800() {
		return region5800;
	}

	/**
	 * @return
	 */
	public String getRegion5900() {
		return region5900;
	}

	/**
	 * @return
	 */
	public String getRegion6000() {
		return region6000;
	}

	/**
	 * @return
	 */
	public String getRegion6100() {
		return region6100;
	}

	/**
	 * @return
	 */
	public String getRegion6200() {
		return region6200;
	}

	/**
	 * @return
	 */
	public String getRegion6300() {
		return region6300;
	}

	/**
	 * @return
	 */
	public String getRegion6400() {
		return region6400;
	}

	/**
	 * @return
	 */
	public String getRegion6500() {
		return region6500;
	}

	/**
	 * @return
	 */
	public String getRegion6600() {
		return region6600;
	}

	/**
	 * @return
	 */
	public String getRegion6700() {
		return region6700;
	}

	/**
	 * @return
	 */
	public String getRegion6800() {
		return region6800;
	}

	/**
	 * @return
	 */
	public String getRegion6900() {
		return region6900;
	}

	/**
	 * @return
	 */
	public String getRegion7000() {
		return region7000;
	}

	/**
	 * @return
	 */
	public String getRegion7100() {
		return region7100;
	}

	/**
	 * @return
	 */
	public String getRegion7200() {
		return region7200;
	}

	/**
	 * @return
	 */
	public String getRegion7300() {
		return region7300;
	}

	/**
	 * @return
	 */
	public String getRegion7400() {
		return region7400;
	}

	/**
	 * @return
	 */
	public String getRegion7500() {
		return region7500;
	}

	/**
	 * @return
	 */
	public String getRegion7600() {
		return region7600;
	}

	/**
	 * @return
	 */
	public String getRegion7700() {
		return region7700;
	}

	/**
	 * @return
	 */
	public String getRegion7800() {
		return region7800;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg01() {
		return regionflg01;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg02() {
		return regionflg02;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg03() {
		return regionflg03;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg04() {
		return regionflg04;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg05() {
		return regionflg05;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg06() {
		return regionflg06;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg07() {
		return regionflg07;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg08() {
		return regionflg08;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg09() {
		return regionflg09;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg10() {
		return regionflg10;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg11() {
		return regionflg11;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg12() {
		return regionflg12;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg13() {
		return regionflg13;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg14() {
		return regionflg14;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg15() {
		return regionflg15;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg16() {
		return regionflg16;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg17() {
		return regionflg17;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg18() {
		return regionflg18;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg19() {
		return regionflg19;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg20() {
		return regionflg20;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg21() {
		return regionflg21;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg22() {
		return regionflg22;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg23() {
		return regionflg23;
	}

	/**
	 * @return
	 */
	public boolean isRegionflg24() {
		return regionflg24;
	}

	/**
	 * @return
	 */
	public boolean isSaveflg() {
		return saveflg;
	}

	/**
	 * @return
	 */
	public String getSearchMode() {
		return searchMode;
	}

	/**
	 * @return
	 */
	public String getSearchStr() {
		return searchStr;
	}

	/**
	 * @return
	 */
	public String getSstemma01() {
		return sstemma01;
	}

	/**
	 * @return
	 */
	public String getSstemma02() {
		return sstemma02;
	}

	/**
	 * @return
	 */
	public String getSstemma03() {
		return sstemma03;
	}

	/**
	 * @return
	 */
	public String getSstemma04() {
		return sstemma04;
	}

	/**
	 * @return
	 */
	public String getSstemma05() {
		return sstemma05;
	}

	/**
	 * @return
	 */
	public String getSstemma06() {
		return sstemma06;
	}

	/**
	 * @return
	 */
	public String getSstemma07() {
		return sstemma07;
	}

	/**
	 * @return
	 */
	public String getSstemma08() {
		return sstemma08;
	}

	/**
	 * @return
	 */
	public String getSstemma09() {
		return sstemma09;
	}

	/**
	 * @return
	 */
	public String getSstemma10() {
		return sstemma10;
	}

	/**
	 * @return
	 */
	public String getSstemma11() {
		return sstemma11;
	}

	/**
	 * @return
	 */
	public String getSstemma12() {
		return sstemma12;
	}

	/**
	 * @return
	 */
	public String getSstemma13() {
		return sstemma13;
	}

	/**
	 * @return
	 */
	public String getSstemma14() {
		return sstemma14;
	}

	/**
	 * @return
	 */
	public String getSstemma15() {
		return sstemma15;
	}

	/**
	 * @return
	 */
	public String getSstemma16() {
		return sstemma16;
	}

	/**
	 * @return
	 */
	public String getSstemma17() {
		return sstemma17;
	}

	/**
	 * @return
	 */
	public String getSstemma18() {
		return sstemma18;
	}

	/**
	 * @return
	 */
	public String getSstemma19() {
		return sstemma19;
	}

	/**
	 * @return
	 */
	public String getSstemma20() {
		return sstemma20;
	}

	/**
	 * @return
	 */
	public String getSstemma21() {
		return sstemma21;
	}

	/**
	 * @return
	 */
	public String getSstemma22() {
		return sstemma22;
	}

	/**
	 * @return
	 */
	public String getSstemma23() {
		return sstemma23;
	}

	/**
	 * @return
	 */
	public String getSstemma24() {
		return sstemma24;
	}

	/**
	 * @return
	 */
	public boolean isStemmaAflg() {
		return stemmaAflg;
	}

	/**
	 * @return
	 */
	public boolean isStemmaSflg() {
		return stemmaSflg;
	}

	/**
	 * @return
	 */
	public String getThisYear() {
		return thisYear;
	}

	/**
	 * @param string
	 */
	public void setCharValue(String string) {
		charValue = string;
	}

	/**
	 * @param string
	 */
	public void setMstemma0011(String string) {
		mstemma0011 = string;
	}

	/**
	 * @param string
	 */
	public void setMstemma0021(String string) {
		mstemma0021 = string;
	}

	/**
	 * @param string
	 */
	public void setMstemma0022(String string) {
		mstemma0022 = string;
	}

	/**
	 * @param string
	 */
	public void setMstemma0023(String string) {
		mstemma0023 = string;
	}

	/**
	 * @param string
	 */
	public void setMstemma0031(String string) {
		mstemma0031 = string;
	}

	/**
	 * @param string
	 */
	public void setMstemma0032(String string) {
		mstemma0032 = string;
	}

	/**
	 * @param string
	 */
	public void setMstemma0041(String string) {
		mstemma0041 = string;
	}

	/**
	 * @param string
	 */
	public void setMstemma0042(String string) {
		mstemma0042 = string;
	}

	/**
	 * @param string
	 */
	public void setMstemma0043(String string) {
		mstemma0043 = string;
	}

	/**
	 * @param string
	 */
	public void setMstemma0051(String string) {
		mstemma0051 = string;
	}

	/**
	 * @param string
	 */
	public void setMstemma0061(String string) {
		mstemma0061 = string;
	}

	/**
	 * @param string
	 */
	public void setMstemma0062(String string) {
		mstemma0062 = string;
	}

	/**
	 * @param string
	 */
	public void setMstemma0071(String string) {
		mstemma0071 = string;
	}

	/**
	 * @param b
	 */
	public void setMstemmaflg(boolean b) {
		mstemmaflg = b;
	}

	/**
	 * @param string
	 */
	public void setRegion0100(String string) {
		region0100 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion0200(String string) {
		region0200 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion0300(String string) {
		region0300 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion0400(String string) {
		region0400 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion0500(String string) {
		region0500 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion0600(String string) {
		region0600 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion0700(String string) {
		region0700 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion0800(String string) {
		region0800 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion0900(String string) {
		region0900 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion1000(String string) {
		region1000 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion1100(String string) {
		region1100 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion1200(String string) {
		region1200 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion1300(String string) {
		region1300 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion1400(String string) {
		region1400 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion1500(String string) {
		region1500 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion1600(String string) {
		region1600 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion1700(String string) {
		region1700 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion1800(String string) {
		region1800 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion1900(String string) {
		region1900 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion2000(String string) {
		region2000 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion2100(String string) {
		region2100 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion2200(String string) {
		region2200 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion2300(String string) {
		region2300 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion2400(String string) {
		region2400 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion2500(String string) {
		region2500 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion2600(String string) {
		region2600 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion2700(String string) {
		region2700 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion2800(String string) {
		region2800 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion2900(String string) {
		region2900 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion3000(String string) {
		region3000 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion3100(String string) {
		region3100 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion3200(String string) {
		region3200 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion3300(String string) {
		region3300 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion3400(String string) {
		region3400 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion3500(String string) {
		region3500 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion3600(String string) {
		region3600 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion3700(String string) {
		region3700 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion3800(String string) {
		region3800 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion3900(String string) {
		region3900 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion4000(String string) {
		region4000 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion4100(String string) {
		region4100 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion4200(String string) {
		region4200 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion4300(String string) {
		region4300 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion4400(String string) {
		region4400 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion4500(String string) {
		region4500 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion4600(String string) {
		region4600 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion4700(String string) {
		region4700 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion4800(String string) {
		region4800 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion4900(String string) {
		region4900 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion5000(String string) {
		region5000 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion5100(String string) {
		region5100 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion5200(String string) {
		region5200 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion5300(String string) {
		region5300 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion5400(String string) {
		region5400 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion5500(String string) {
		region5500 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion5600(String string) {
		region5600 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion5700(String string) {
		region5700 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion5800(String string) {
		region5800 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion5900(String string) {
		region5900 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion6000(String string) {
		region6000 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion6100(String string) {
		region6100 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion6200(String string) {
		region6200 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion6300(String string) {
		region6300 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion6400(String string) {
		region6400 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion6500(String string) {
		region6500 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion6600(String string) {
		region6600 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion6700(String string) {
		region6700 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion6800(String string) {
		region6800 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion6900(String string) {
		region6900 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion7000(String string) {
		region7000 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion7100(String string) {
		region7100 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion7200(String string) {
		region7200 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion7300(String string) {
		region7300 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion7400(String string) {
		region7400 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion7500(String string) {
		region7500 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion7600(String string) {
		region7600 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion7700(String string) {
		region7700 = string;
	}

	/**
	 * @param string
	 */
	public void setRegion7800(String string) {
		region7800 = string;
	}

	/**
	 * @param b
	 */
	public void setRegionflg01(boolean b) {
		regionflg01 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg02(boolean b) {
		regionflg02 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg03(boolean b) {
		regionflg03 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg04(boolean b) {
		regionflg04 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg05(boolean b) {
		regionflg05 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg06(boolean b) {
		regionflg06 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg07(boolean b) {
		regionflg07 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg08(boolean b) {
		regionflg08 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg09(boolean b) {
		regionflg09 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg10(boolean b) {
		regionflg10 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg11(boolean b) {
		regionflg11 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg12(boolean b) {
		regionflg12 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg13(boolean b) {
		regionflg13 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg14(boolean b) {
		regionflg14 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg15(boolean b) {
		regionflg15 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg16(boolean b) {
		regionflg16 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg17(boolean b) {
		regionflg17 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg18(boolean b) {
		regionflg18 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg19(boolean b) {
		regionflg19 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg20(boolean b) {
		regionflg20 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg21(boolean b) {
		regionflg21 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg22(boolean b) {
		regionflg22 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg23(boolean b) {
		regionflg23 = b;
	}

	/**
	 * @param b
	 */
	public void setRegionflg24(boolean b) {
		regionflg24 = b;
	}

	/**
	 * @param b
	 */
	public void setSaveflg(boolean b) {
		saveflg = b;
	}

	/**
	 * @param string
	 */
	public void setSearchMode(String string) {
		searchMode = string;
	}

	/**
	 * @param string
	 */
	public void setSearchStr(String string) {
		searchStr = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma01(String string) {
		sstemma01 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma02(String string) {
		sstemma02 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma03(String string) {
		sstemma03 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma04(String string) {
		sstemma04 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma05(String string) {
		sstemma05 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma06(String string) {
		sstemma06 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma07(String string) {
		sstemma07 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma08(String string) {
		sstemma08 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma09(String string) {
		sstemma09 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma10(String string) {
		sstemma10 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma11(String string) {
		sstemma11 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma12(String string) {
		sstemma12 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma13(String string) {
		sstemma13 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma14(String string) {
		sstemma14 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma15(String string) {
		sstemma15 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma16(String string) {
		sstemma16 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma17(String string) {
		sstemma17 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma18(String string) {
		sstemma18 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma19(String string) {
		sstemma19 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma20(String string) {
		sstemma20 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma21(String string) {
		sstemma21 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma22(String string) {
		sstemma22 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma23(String string) {
		sstemma23 = string;
	}

	/**
	 * @param string
	 */
	public void setSstemma24(String string) {
		sstemma24 = string;
	}

	/**
	 * @param b
	 */
	public void setStemmaAflg(boolean b) {
		stemmaAflg = b;
	}

	/**
	 * @param b
	 */
	public void setStemmaSflg(boolean b) {
		stemmaSflg = b;
	}

	/**
	 * @param string
	 */
	public void setThisYear(String string) {
		thisYear = string;
	}

	/**
	 * @return
	 */
	public String getExamDoc() {
		return examDoc;
	}

	/**
	 * @return
	 */
	public String[] getInterviewForms() {
		return interviewForms;
	}

	/**
	 * @return
	 */
	public String getInterviewSheet() {
		return interviewSheet;
	}

	/**
	 * @return
	 */
	public String getPrintStudent() {
		return printStudent;
	}

	/**
	 * @return
	 */
	public String getReport() {
		return report;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * @return
	 */
	public String getTargetExamName() {
		return targetExamName;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @param string
	 */
	public void setExamDoc(String string) {
		examDoc = string;
	}

	/**
	 * @param strings
	 */
	public void setInterviewForms(String[] strings) {
		interviewForms = strings;
	}

	/**
	 * @param string
	 */
	public void setInterviewSheet(String string) {
		interviewSheet = string;
	}

	/**
	 * @param string
	 */
	public void setPrintStudent(String string) {
		printStudent = string;
	}

	/**
	 * @param string
	 */
	public void setReport(String string) {
		report = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamName(String string) {
		targetExamName = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * @return
	 */
	public String getPrintFlag() {
		return printFlag;
	}

	/**
	 * @param string
	 */
	public void setPrintFlag(String string) {
		printFlag = string;
	}

	/**
	 * @return
	 */
	public String[] getTextFormat() {
		return textFormat;
	}

	/**
	 * @param strings
	 */
	public void setTextFormat(String[] strings) {
		textFormat = strings;
	}

	/**
	 * @return
	 */
	public String getTargetExam() {
		return targetExam;
	}

	/**
	 * @return
	 */
	public String getTargetYear() {
		return targetYear;
	}

	/**
	 * @param string
	 */
	public void setTargetExam(String string) {
		targetExam = string;
	}

	/**
	 * @param string
	 */
	public void setTargetYear(String string) {
		targetYear = string;
	}
	
	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[1] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[1] add
	}
}

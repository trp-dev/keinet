package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * バランスチャート(合格者平均/過回模試グラフ)用フォーム
 * @author kondo
 *
 * 2005.4.26 	K.Kondo 	[1]画面スクロール座標の追加
 */
public class I105Form extends ActionForm {

	private String mode;				//モード（分析、面談）

	private String targetPersonId;		//個人ＩＤ
	private String targetExamCode;		//模試コード
	private String targetExamYear;		//模試年度
	
	private String lowerRange;				//偏差値範囲（下限）
	private String upperRange;				//偏差値範囲（上限）
	private String avgChoice;				//偏差値計算方法（平均、良い方）
	private boolean markexamflg;			//マーク模試だったらtrue
	private boolean dispflg;			//マーク模試だったらtrue
	
	private String scrollX;//[1] add サブミット前、表示座標Ｘ
	private String scrollY;//[1] add サブミット前、表示座標Ｙ
	
	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ
		
	}
	
	
	/**
	 * @return
	 */
	public String getAvgChoice() {
		return avgChoice;
	}

	/**
	 * @return
	 */
	public String getLowerRange() {
		return lowerRange;
	}

	/**
	 * @return
	 */
	public String getUpperRange() {
		return upperRange;
	}

	/**
	 * @param string
	 */
	public void setAvgChoice(String string) {
		avgChoice = string;
	}

	/**
	 * @param string
	 */
	public void setLowerRange(String string) {
		lowerRange = string;
	}

	/**
	 * @param string
	 */
	public void setUpperRange(String string) {
		upperRange = string;
	}
	
	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * @return
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param string
	 */
	public void setMode(String string) {
		mode = string;
	}

	/**
	 * @return
	 */
	public boolean isMarkexamflg() {
		return markexamflg;
	}

	/**
	 * @param b
	 */
	public void setMarkexamflg(boolean b) {
		markexamflg = b;
	}

	/**
	 * @return
	 */
	public boolean isDispflg() {
		return dispflg;
	}

	/**
	 * @param b
	 */
	public void setDispflg(boolean b) {
		dispflg = b;
	}
	
	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[1] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[1] add
	}
	
}

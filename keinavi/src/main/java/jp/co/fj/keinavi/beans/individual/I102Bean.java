/*
 *
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.CandidateRatingData;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.StudentData;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 個人成績表（志望大学別成績）専用検索Bean
 *
 * 2004.07.22 [新規作成]
 *
 *
 * @author Keisuke KONDO - TOTEC
 * @version 1.0
 *
 * [1]  2005.02.02 k.KONDO      外部データ開放日が同一の場合、模試コード順にソートするよう修正
 * [2]  2005.03.08 k.KONDO      現年度の年度条件を担当クラスの年度に変更
 * [3]  2006.08.24 t.YAMADA     試験一覧の表示順を「外部（内部）データ開放日」から「実施日」に変更
 * [4]  2009.09.30 s.HASE       「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 * 　　　　                     「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 *      2009.10.07 s.HASE       「HS12_UNIV：高１・２大学マスタ(判定＆志望校名称)」テーブルを
 * 　　　　                     「H12_UNIVMASTER：高１２用大学マスタ」テーブルに置き換え
 * [5]  2019.07.26 Hics)ueta    共通テスト対応
 */
public class I102Bean extends DefaultBean{

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8044350134439720098L;

    /**
     * 担当クラスの年度
     */
    private String classYear;//[2] add

    private String evalSql;

    private StudentData personData;

    /**
     * 個人ID
     */
    private String individualId;

    /**
     * 対象模試年度・対象模試コード
     */
    private String examYear;
    private String examCode;

    /**
     * 大学マスタ区分
     * 1:高１・２マスタ
     * 2:高３マスタ
     */
    private String masterDiv;

    /**
     * 表示する学年
     * 1: 現年 2: 全学年
     */
    private String studentDispGrade;

    /**
     * 模試の種類
     * 1:全ての種類
     * 2:同じ種類
     */
    private String examType;

    /**
     * 一般かヘルプデスクか
     * 一般：外部データ開放日
     * ヘルプデスク：内部データ開放日
     */
    private boolean helpflg;

    /**
     * 模試セッション
     */
    private ExamSession examSession;

    /* (非 Javadoc)
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws SQLException, Exception {
        personData = execEval();
    }

    /**
     * @param string
     * @param string2
     * @param string3
     */
    public void setSearchCondition(String individualId, String examYear, String examCode ,boolean targethelpflg) {
        //setSearchCondition(individualId, examYear, examCode, getStudentDispGrade(), getExamType(), isHelpflg());//[1] delete
        setSearchCondition(individualId, examYear, examCode, getStudentDispGrade(), getExamType(), targethelpflg);//[1] add
    }
    /**
     * @param string
     * @param string2
     * @param string3
     * @param string4
     * @param string5
     */
    public void setSearchCondition(String individualId, String examYear, String examCode, String studentDispGrade, String examType ,boolean helpflg) {
        setIndividualId(individualId);
        setExamYear(examYear);
        setExamCode(examCode);
        setStudentDispGrade(studentDispGrade);
        setExamType(examType);
        setHelpflg(helpflg);

        appendEvalSql(getEvalSqlBase());
        appendEvalSql(getPersonCondition(individualId));
        appendEvalSql(getPrevExamCondition(examYear, examCode));
        if(studentDispGrade.equals("1"))
            appendEvalSql(getCurrentYearCondition());
        if(examType.equals("2"))
            appendEvalSql(getSameTypeCondition(examYear, examCode));
        appendEvalSql(getEvalOrderBy(helpflg));
    }
    private void appendEvalSql(String string){
        if(getEvalSql() ==  null)
            setEvalSql("");
        evalSql += string;
    }
    private String getSameTypeCondition(String examYear, String examCode){
        String sqlOption = " AND A.EXAMTYPECD = "+
                        " (SELECT EXAMTYPECD FROM EXAMINATION WHERE EXAMYEAR = '"+examYear+"' AND EXAMCD = '"+examCode+"')";
        return sqlOption;
    }
    private String getPersonCondition(String id){
        return	" AND A.INDIVIDUALID = ?";
    }
    private String getCurrentYearCondition(){
        //String sqlOption = " AND A.EXAMYEAR = (SELECT MAX(EXAMYEAR) FROM EXAMINATION)";//[2] delete
        String sqlOption = " AND A.EXAMYEAR = " + classYear + "";//[2] add
        return sqlOption;
    }
    private String getEvalSqlBase(){
        String sql = "SELECT "+
        "A.INDIVIDUALID "+
        ",A.EXAMYEAR "+
        ",A.EXAMCD "+
        //1は高１２マスタ、2は高３マスタ
        ",DECODE(A.MASTERDIV, '1', H12.UNINAME_ABBR, '2', H3.UNINAME_ABBR, '不明') A "+
        ",DECODE(A.MASTERDIV, '1', H12.FACULTYNAME_ABBR, '2', H3.FACULTYNAME_ABBR, '不明') B "+
        ",DECODE(A.MASTERDIV, '1', H12.DEPTNAME_ABBR, '2', H3.DEPTNAME_ABBR, '不明') C "+
// 2019/12/03 QQ)Tanioka ボーダー取得元変更 MOD START
//        ",DECODE(A.MASTERDIV, '1', NULL, '2', H3.BORDERSCORE2) CEN_BORDER"+
        ",DECODE(A.MASTERDIV, '1', NULL, '2', H3.BORDERSCORENOEXAM2) CEN_BORDER"+
// 2019/12/03 QQ)Tanioka ボーダー取得元変更 MOD END
// 2019/12/03 QQ)Tanioka 英語認定試験延期対応 DEL START
//        ",DECODE(A.MASTERDIV, '1', NULL, '2', H3.BORDERSCORERATENOEXAM2) CEN_BORDER_NOEXA"+ //センターボーダー認定試験なし[5] add
//        // 2019/08/26 Hics)Ueta 共通テスト対応 ADD START
//        ",A.CENFULL"+ //満点認定試験あり
//        ",A.CENFULL_ENGPT "+ //満点認定試験なし
//        // 2019/08/26 Hics)Ueta 共通テスト対応 ADD END
// 2019/12/03 QQ)Tanioka 英語認定試験延期対応 DEL END
        ",A.CENFULL"+ //満点認定試験あり
        ",DECODE(A.MASTERDIV, '1', DECODE(R12.RANKCD, '15', 0, R12.RANKLLIMITS), '2', DECODE(R3.RANKCD, '15', 0, R3.RANKLLIMITS), NULL) RANKLLIMITS"+//ランク偏差値
        ",A.UNIVCD "+",A.FACULTYCD "+",A.DEPTCD "+
        ",A.CANDIDATERANK "+",A.RATINGCVSSCORE "+",A.RATINGDEVIATION "+",A.RATINGPOINT "+
// 2019/12/03 QQ)Tanioka 英語認定試験延期対応 DEL START
//        // 2019/08/26 Hics)Ueta 共通テスト対応 ADD START
//        ",A.RATINGCVSSCORE_ENGPT" + //評価得点認定試験あり
//        ",A.CENTERRATING_ENGPT "+ //共通テスト評価（英語認定試験含む）
//        ",A.APP_QUAL_JUDGE "+ //英語資格要件-出願資格判定
//        ",A.APP_QUAL_NOTICE "+ //英語資格要件-出願基準非対応告知
//        // 2019/08/26 Hics)Ueta 共通テスト対応 ADD END
// 2019/12/03 QQ)Tanioka 英語認定試験延期対応 DEL END
        ",A.CENTERRATING "+",A.SECONDRATING "+",A.TOTALRATING "+",A.TOTALCANDIDATENUM "+
        ",A.TOTALCANDIDATERANK "+
        ",A.EXAMDIV "+",A.MASTERDIV "+",A.EXAMNAME "+",A.EXAMTYPECD "+
        ",A.OUT_DATAOPENDATE "+",A.IN_DATAOPENDATE "+
        " ,a.inpledate " +
        " FROM "+
        " ( "+
            " SELECT "+
            " /*+ INDEX(R PK_CANDIDATERATING) */ "+//12/5 sql hint
            " R.INDIVIDUALID "+
            ",R.EXAMYEAR "+",R.EXAMCD "+",R.UNIVCD "+",R.FACULTYCD "+",R.DEPTCD "+
            ",R.CANDIDATERANK "+",R.RATINGCVSSCORE "+",R.RATINGDEVIATION "+",R.RATINGPOINT "+
// 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD START
//            // 2019/08/26 Hics)Ueta 共通テスト対応 ADD START
//            ",R.RATINGCVSSCORE_ENGPT" + //評価得点認定試験あり
//            // 2019/08/26 Hics)Ueta 共通テスト対応 ADD END
//            ",R.CENTERRATING "+
//            // 2019/08/26 Hics)Ueta 共通テスト対応 ADD START
//            ",R.CENTERRATING_ENGPT CENTERRATING_ENGPT"+//共通テスト評価（英語認定試験含む）
//            // 2019/08/26 Hics)Ueta 共通テスト対応 ADD END
//            ",R.SECONDRATING "+
//            ",R.TOTALRATING "+
//            // 2019/08/26 Hics)Ueta 共通テスト対応 ADD START
//            ",R.APP_QUAL_JUDGE "+//英語資格要件-出願資格判定
//            ",R.APP_QUAL_NOTICE APP_QUAL_NOTICE"+//英語資格要件-出願基準非対応告知
//            // 2019/08/26 Hics)Ueta 共通テスト対応 ADD END
//            ",R.TOTALCANDIDATENUM "+
//            ",R.TOTALCANDIDATERANK "+
//            // 2019/08/26 Hics)Ueta 共通テスト対応 ADD START
//            ",R.CENFULL "+//共通テスト満点値
//            ",R.CENFULL_ENGPT "+//共通テスト満点値（英語参加試験含む)
//            // 2019/08/26 Hics)Ueta 共通テスト対応 ADD END
            ",R.CENFULL "+//共通テスト満点値
            ",R.CENTERRATING "+",R.SECONDRATING "+",R.TOTALRATING "+",R.TOTALCANDIDATENUM "+
            ",R.TOTALCANDIDATERANK "+
// 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD END
            ",X.EXAMDIV "+",X.MASTERDIV "+",X.EXAMNAME "+",X.EXAMTYPECD "+
            ",X.OUT_DATAOPENDATE "+",X.IN_DATAOPENDATE "+
            " ,x.inpledate " +
            " FROM CANDIDATERATING R "+
            " INNER JOIN EXAMINATION X ON R.EXAMYEAR = X.EXAMYEAR AND R.EXAMCD = X.EXAMCD "+
        " ) A "+
        " LEFT JOIN UNIVMASTER_BASIC H3 ON A.EXAMYEAR = H3.EVENTYEAR AND A.EXAMDIV = H3.EXAMDIV AND A.UNIVCD = H3.UNIVCD AND A.FACULTYCD = H3.FACULTYCD AND A.DEPTCD = H3.DEPTCD "+
        " LEFT JOIN H12_UNIVMASTER H12 ON A.EXAMYEAR = H12.EVENTYEAR AND A.EXAMDIV = H12.EXAMDIV AND A.UNIVCD = H12.UNIVCD AND A.FACULTYCD = H12.FACULTYCD AND A.DEPTCD = H12.DEPTCD "+
        " LEFT JOIN RANK R12 ON H12.ENTEXAMRANK = R12.RANKCD"+
        " LEFT JOIN RANK R3 ON H3.ENTEXAMRANK = R3.RANKCD"+
        " WHERE 1 = 1";

        return sql;
    }
    /**
     * 過回模試に絞り込む条件
     * 過回判断は試験実地日ではなく外部データ開放日
     * @param examYear
     * @param examCode
     * @return
     */
    private String getPrevExamCondition(String examYear, String examCode){
        String sqlOption = "";
        if(helpflg) {
            sqlOption = " AND A.IN_DATAOPENDATE <="+
            " (SELECT IN_DATAOPENDATE FROM EXAMINATION WHERE EXAMYEAR = '"+examYear+"' AND EXAMCD = '"+examCode+"')";
        } else {
            sqlOption = " AND A.OUT_DATAOPENDATE <="+
            " (SELECT OUT_DATAOPENDATE FROM EXAMINATION WHERE EXAMYEAR = '"+examYear+"' AND EXAMCD = '"+examCode+"')";
        }
        return sqlOption;
    }

    /**
     * 表示順文字列を作成して返す
     * @param isHelpDesk ヘルプデスクかどうか
     * @return 表示順文字列
     */
    private String getEvalOrderBy(boolean isHelpDesk){

        final StringBuffer buf = new StringBuffer();

        buf.append(" ORDER BY");
        buf.append(" a.inpledate desc,");
        buf.append(" a.examcd desc,");
        buf.append(" a.candidaterank asc");

        return buf.toString();
    }

    private StudentData execEval() throws Exception{

        final String FULLSIZESPACE = "　";
        // 2019/08/27 Hics)Ueta 共通テスト対応 ADD START
        final String HALFSLASH = "/";
// 2019/12/03 QQ)Tanioka 英語認定試験延期対応 DEL START
//        final String HALFSIZESPACE = " ";
// 2019/12/03 QQ)Tanioka 英語認定試験延期対応 DEL END
        // 2019/08/27 Hics)Ueta 共通テスト対応 ADD END

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{

            pstmt = conn.prepareStatement(getEvalSql());
            pstmt.setString(1, individualId);

            rs = pstmt.executeQuery();

            StudentData personData = new StudentData();

            while (rs.next()) {

                ExaminationData examinationData = personData.getExaminationData(nullToEmpty(rs.getString("EXAMYEAR")), nullToEmpty(rs.getString("EXAMCD")));
                if(examinationData == null){
                    examinationData = new ExaminationData();
                    examinationData.setExamYear(nullToEmpty(rs.getString("EXAMYEAR")));
                    examinationData.setExamCd(nullToEmpty(rs.getString("EXAMCD")));
                    examinationData.setExamName(nullToEmpty(rs.getString("EXAMNAME")));
                    examinationData.setExamTypeCd(nullToEmpty(rs.getString("EXAMTYPECD")));
                    if(personData.getExaminationDatas() == null)
                        personData.setExaminationDatas(new ArrayList());
                    personData.getExaminationDatas().add(examinationData);
                }

                List candidateRatingDatas = (List) examinationData.getCandidateRatingDatas();
                if(candidateRatingDatas == null){
                    examinationData.setCandidateRatingDatas(new ArrayList());
                }
                CandidateRatingData candidateRatingData = new CandidateRatingData();
                candidateRatingData.setSuperAbbrName(nullToEmpty(rs.getString("A")) + FULLSIZESPACE + nullToEmpty(rs.getString("B")) + FULLSIZESPACE + nullToEmpty(rs.getString("C")));
                candidateRatingData.setUnivCd(nullToEmpty(rs.getString("UNIVCD")));
                candidateRatingData.setCandidateRank(nullToEmpty(rs.getString("CANDIDATERANK")));
// 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD START
//                // 2019/08/27 Hics)Ueta 共通テスト対応 UPD START
//                //candidateRatingData.setRatingCvsScore(nullToEmpty(rs.getString("RATINGCVSSCORE")));
//                if(rs.getString("RATINGCVSSCORE") != null ) {
//                candidateRatingData.setRatingCvsScore(nullToEmpty(rs.getString("RATINGCVSSCORE")) + HALFSLASH + nullToEmpty(rs.getString("CENFULL")));
//                }
//                // 2019/08/27 Hics)Ueta 共通テスト対応 UPD END
//                // 2019/08/27 Hics)Ueta 共通テスト対応 ADD START
//                if(rs.getString("RATINGCVSSCORE") != null ) {
//                candidateRatingData.setRatingCvsScore_engpt(nullToEmpty(rs.getString("RATINGCVSSCORE_ENGPT")) + HALFSLASH + nullToEmpty(rs.getString("CENFULL_ENGPT")));//[5] add 共通テスト満点英語含む
//                }
//                // 2019/08/27 Hics)Ueta 共通テスト対応 ADD END
//                candidateRatingData.setRatingDeviation(nullToEmptyF(rs.getString("RATINGDEVIATION")));
//                candidateRatingData.setRatingPoint(nullToEmpty(rs.getString("RATINGPOINT")));
//                // 2019/08/27 Hics)Ueta 共通テスト対応 UPD START
//                //candidateRatingData.setMarkGRating(nullToEmpty(rs.getString("CENTERRATING")));
//                candidateRatingData.setMarkGRating(cm.padRight(nullToEmpty(rs.getString("CENTERRATING")),"UTF-8",2));
//                // 2019/08/27 Hics)Ueta 共通テスト対応 UPD END
//                // 2019/08/27 Hics)Ueta 共通テスト対応 ADD START
//                candidateRatingData.setMarkGRating_engpt(cm.padRight(nullToEmpty(rs.getString("CENTERRATING_ENGPT")),"UTF-8",2) + HALFSIZESPACE + nullToEmpty(rs.getString("APP_QUAL_JUDGE")) + cm.padRight(nullToEmpty(rs.getString("APP_QUAL_NOTICE")),"UTF-8",2)) ;
//                // 2019/08/27 Hics)Ueta 共通テスト対応 ADD END
//                // 2019/08/27 Hics)Ueta 共通テスト対応 UPD START
//                //candidateRatingData.setDiscriptionGRating(nullToEmpty(rs.getString("SECONDRATING")));
//                //candidateRatingData.setTotalRating(nullToEmpty(rs.getString("TOTALRATING")));
//                candidateRatingData.setDiscriptionGRating(cm.padRight(nullToEmpty(rs.getString("SECONDRATING")),"UTF-8",2));
//                candidateRatingData.setTotalRating(cm.padRight(nullToEmpty(rs.getString("TOTALRATING")),"UTF-8",2));
//                // 2019/08/27 Hics)Ueta 共通テスト対応 UPD END
//                candidateRatingData.setTotalCandidateNum(nullToEmpty(rs.getString("TOTALCANDIDATENUM")));
//                candidateRatingData.setTotalCandidateRank(nullToEmpty(rs.getString("TOTALCANDIDATERANK")));
//                candidateRatingData.setCenBorder(nullToEmpty(rs.getString("CEN_BORDER")));
//                // 2019/08/27 Hics)Ueta 共通テスト対応 ADD START
//                candidateRatingData.setCenBorderNoexa(nullToEmpty(rs.getString("CEN_BORDER_NOEXA")));
//                // 2019/08/27 Hics)Ueta 共通テスト対応 ADD END
                if(rs.getString("RATINGCVSSCORE") != null ) {
                    candidateRatingData.setRatingCvsScore(nullToEmpty(rs.getString("RATINGCVSSCORE")) + HALFSLASH + nullToEmpty(rs.getString("CENFULL")));
                }
                candidateRatingData.setRatingDeviation(nullToEmptyF(rs.getString("RATINGDEVIATION")));
                candidateRatingData.setRatingPoint(nullToEmpty(rs.getString("RATINGPOINT")));
                candidateRatingData.setMarkGRating(nullToEmpty(rs.getString("CENTERRATING")));
                candidateRatingData.setDiscriptionGRating(nullToEmpty(rs.getString("SECONDRATING")));
                candidateRatingData.setTotalRating(nullToEmpty(rs.getString("TOTALRATING")));
                //candidateRatingData.setTotalCandidateNum(nullToEmpty(rs.getString("TOTALCANDIDATENUM")));
                //candidateRatingData.setTotalCandidateRank(nullToEmpty(rs.getString("TOTALCANDIDATERANK")));

                if(rs.getString("TOTALCANDIDATERANK") != null) {
                    candidateRatingData.setTotalCandidateRank(nullToEmpty(rs.getString("TOTALCANDIDATERANK")) + HALFSLASH + nullToEmpty(rs.getString("TOTALCANDIDATENUM")));
                }

                candidateRatingData.setCenBorder(nullToEmpty(rs.getString("CEN_BORDER")));
// 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD END
                candidateRatingData.setRankLimits("0".equals(rs.getString("RANKLLIMITS"))
                        ? JudgementConstants.Univ.RANK_STR_NONE : nullToEmptyF(rs.getString("RANKLLIMITS")));
                examinationData.getCandidateRatingDatas().add(candidateRatingData);
            }

            return personData;

        } finally {
            DbUtils.closeQuietly(null, pstmt, rs);
        }

    }
    /**
     * StringをFloatのフォーマットで返します。
     * @return
     */
    public String nullToEmptyF(String str) {
        if(str == null) {
            return "";
        } else{
                return String.valueOf(new Float(str).floatValue());
        }
    }
    /**
     * nullが来たら空白にする
     */
    private String nullToEmpty(String str) {
        if(str == null) {
            return "";
        }
        if(str.equals("null")) {
            return "";
        }
        return str;
    }
// 2019/12/03 QQ)Tanioka 英語認定試験延期対応 DEL START
//    // 2019/08/27 Hics)Ueta 共通テスト対応 ADD START
//    private CM		cm			= new CM();	// 共通関数用クラス インスタンス
//    // 2019/08/27 Hics)Ueta 共通テスト対応 ADD START
// 2019/12/03 QQ)Tanioka 英語認定試験延期対応 DEL END
    /**
     * @return
     */
    public String getExamCode() {
        return examCode;
    }

    /**
     * @return
     */
    public String getExamType() {
        return examType;
    }

    /**
     * @return
     */
    public String getExamYear() {
        return examYear;
    }

    /**
     * @return
     */
    public String getStudentDispGrade() {
        return studentDispGrade;
    }

    /**
     * @return
     */
    public String getIndividualId() {
        return individualId;
    }

    /**
     * @param string
     */
    public void setExamCode(String string) {
        examCode = string;
    }

    /**
     * @param string
     */
    public void setExamType(String string) {
        examType = string;
    }

    /**
     * @param string
     */
    public void setExamYear(String string) {
        examYear = string;
    }

    /**
     * @param string
     */
    public void setStudentDispGrade(String string) {
        studentDispGrade = string;
    }

    /**
     * @param string
     */
    public void setIndividualId(String string) {
        individualId = string;
    }

    /**
     * @return
     */
    public String getEvalSql() {
        return evalSql;
    }

    /**
     * @param string
     */
    public void setEvalSql(String string) {
        evalSql = string;
    }

    /**
     * @return
     */
    public StudentData getPersonData() {
        return personData;
    }

    /**
     * @param data
     */
    public void setPersonData(StudentData data) {
        personData = data;
    }

    /**
     * @return
     */
    public boolean isHelpflg() {
        return helpflg;
    }

    /**
     * @param b
     */
    public void setHelpflg(boolean b) {
        helpflg = b;
    }

    /**
     * @return
     */
    public ExamSession getExamSession() {
        return examSession;
    }

    /**
     * @param session
     */
    public void setExamSession(ExamSession session) {
        examSession = session;
    }

    /**
     * @return
     */
    public String getMasterDiv() {
        return masterDiv;
    }

    /**
     * @param string
     */
    public void setMasterDiv(String string) {
        masterDiv = string;
    }

    /**
     * @return
     */
    public String getClassYear() {
        return classYear;//[2] add
    }

    /**
     * @param string
     */
    public void setClassYear(String string) {
        classYear = string;//[2] add
    }

}

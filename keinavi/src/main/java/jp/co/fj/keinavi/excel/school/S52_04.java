/**
 * 校内成績分析−過回比較　偏差値分布
 * 	Excelファイル編集
 * 作成日: 2004/08/16
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S52BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S52Item;
import jp.co.fj.keinavi.excel.data.school.S52ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S52_04 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	final private int	intMaxSheetSr	= 40;			// 最大シート数	
	final private String	masterfile0		= "S52_04";		// ファイル名
	final private String	masterfile1		= "NS52_04";	// ファイル名
	private String	masterfile		= "";					// ファイル名

	/*
	 * 	Excel編集メイン
	 * 		S52Item s52Item: データクラス
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
 	public int s52_04EditExcel(S52Item s52Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("S52_04","S52_04帳票作成開始","");
		
		//テンプレートの決定
		if (s52Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			
			// データセット
			S52ListBean konkaiListBean	= new S52ListBean();
			ArrayList	s52List			= s52Item.getS52List();
			Iterator	itr				= s52List.iterator();
			int		col				= 0;	// 列
			int 		setCol			= -1;	// *セット用
			float 		koseihi			= 0;	// *作成用
			int 		maxNo			= 19;	// 最大表示件数
			int		kakaiCnt		= 0;	// 回カウンター
			int		maxSheetIndex	= 0;	// シートカウンター
			int		intBookCngCount	= 0;	// ブックカウンター
			int		kmkCd			= 0;	// 科目コード保持
			String		moshi			= "";	// 模試月
			boolean	bolSheetCngFlg	= true;// true:改シート実行　（改シートフラグ）
			boolean	bolBookCngFlg	= true;// true:改ブック実行　（改ブックフラグ）
			boolean	bolKonkaiFlg	= false;// true:今回のデータを表示（今回フラグ）
//add 2004/10/27 T.Sakai データ0件対応
			int		dispKmkFlgCnt	= 0;	// グラフ表示フラグカウンタ
//add end

			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			// 基本ファイルを読込む
			S52ListBean s52ListBean = new S52ListBean();

			while( itr.hasNext() ) {
				s52ListBean = (S52ListBean)itr.next();
				if ( s52ListBean.getIntDispKmkFlg()==1 ) {
//add 2004/10/27 T.Sakai データ0件対応
					dispKmkFlgCnt++;
//add end
					// 基本ファイルを読み込む
					S52BnpListBean		s52BnpListBean		= new S52BnpListBean();

					// カウンターチェック
					if (kakaiCnt!=0){
						if ( Integer.parseInt(s52ListBean.getStrKmkCd()) != kmkCd ){
							konkaiListBean = s52ListBean;
							kakaiCnt = 0;
							bolSheetCngFlg = true;
						}
						if (kakaiCnt>=5){
							kakaiCnt = 0;
							bolSheetCngFlg = true;
							bolKonkaiFlg = true;
						}

						// 改ブック処理
						if(bolSheetCngFlg == true){
							if(maxSheetIndex >= intMaxSheetSr){
								bolBookCngFlg = true;
							}
						}
				
						if( bolBookCngFlg == true){
							boolean bolRet = false;
							// Excelファイル保存
							if(intBookCngCount==0){
								if(itr.hasNext() == true){
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
								}
								else{
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
								}
							}else{
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
							}

							if( bolRet == false ){
								return errfwrite;
							}
							intBookCngCount++;
						}
					}else{
						// 保持
						konkaiListBean = s52ListBean;
					}
	
					
					if( bolBookCngFlg == true ){
						// マスタExcel読み込み
						workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
						if( workbook==null ){
							return errfread;
						}
						bolSheetCngFlg	= true;
						bolBookCngFlg	= false;
						maxSheetIndex	= 0;
					}
	
					if(bolSheetCngFlg==true){
						// データセットするシートの選択
						workSheet = workbook.getSheet(Integer.toString(maxSheetIndex+1));
						maxSheetIndex++;
	
						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);
	
						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, s52Item.getIntSecuFlg() ,41 ,42 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 41 );
						workCell.setCellValue(secuFlg);
	
						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
						workCell.setCellValue( "学校名　：" + cm.toString(s52Item.getStrGakkomei()) );
	
						// 対象模試セット
						moshi =cm.setTaisyouMoshi( s52Item.getStrMshDate() );	// 模試月取得
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s52Item.getStrMshmei()) + moshi);
	
						// 型名・配点セット
						String haiten = "";
						if ( !cm.toString(s52ListBean.getStrHaitenKmk()).equals("") ) {
							haiten = "（" + s52ListBean.getStrHaitenKmk() + "）";
						}
						workCell = cm.setCell( workSheet, workRow, workCell, 5, 0 );
						workCell.setCellValue( "型・科目：" + cm.toString(s52ListBean.getStrKmkmei()) + haiten );
						kmkCd=Integer.parseInt(s52ListBean.getStrKmkCd());
						
//add 2004/10/27 T.Sakai スペース対応
  						if (s52Item.getIntShubetsuFlg() == 1){
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
							workCell.setCellValue( "〜 19" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
							workCell.setCellValue( " 20〜\n 29" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
							workCell.setCellValue( " 30〜\n 39" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
							workCell.setCellValue( " 40〜\n 49" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
							workCell.setCellValue( " 50〜\n 59" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
							workCell.setCellValue( " 60〜\n 69" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
							workCell.setCellValue( " 70〜\n 79" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
							workCell.setCellValue( " 80〜\n 89" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
							workCell.setCellValue( " 90〜\n 99" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
							workCell.setCellValue( "100〜\n109" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
							workCell.setCellValue( "110〜\n119" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
							workCell.setCellValue( "120〜\n129" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
							workCell.setCellValue( "130〜\n139" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
							workCell.setCellValue( "140〜\n149" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
							workCell.setCellValue( "150〜\n159" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
							workCell.setCellValue( "160〜\n169" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
							workCell.setCellValue( "170〜\n179" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
							workCell.setCellValue( "180〜\n189" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
							workCell.setCellValue( "190〜" );
  						} else{
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
							workCell.setCellValue( "〜32.4" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
							workCell.setCellValue( "32.5〜\n34.9" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
							workCell.setCellValue( "35.0〜\n37.4" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
							workCell.setCellValue( "37.5〜\n39.9" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
							workCell.setCellValue( "40.0〜\n42.4" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
							workCell.setCellValue( "42.5〜\n44.9" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
							workCell.setCellValue( "45.0〜\n47.4" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
							workCell.setCellValue( "47.5〜\n49.9" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
							workCell.setCellValue( "50.0〜\n52.4" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
							workCell.setCellValue( "52.5〜\n54.9" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
							workCell.setCellValue( "55.0〜\n57.4" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
							workCell.setCellValue( "57.5〜\n59.9" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
							workCell.setCellValue( "60.0〜\n62.4" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
							workCell.setCellValue( "62.5〜\n64.9" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
							workCell.setCellValue( "65.0〜\n67.4" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
							workCell.setCellValue( "67.5〜\n69.9" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
							workCell.setCellValue( "70.0〜\n72.4" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
							workCell.setCellValue( "72.5〜\n74.9" );
							workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
							workCell.setCellValue( "75.0〜" );
  						}
//add end
						
						bolSheetCngFlg = false;

					}

					if(bolKonkaiFlg==true){
	
						//データの保持
						ArrayList	s52BnpList		= konkaiListBean.getS52BnpList();
						Iterator	itrS52Bnp		= s52BnpList.iterator();

						// 偏差値データセット
						col	= 0;
						while(itrS52Bnp.hasNext()){
							s52BnpListBean	= (S52BnpListBean) itrS52Bnp.next();
	
							// 模試名セット
							if(col == 0){
								moshi =cm.setTaisyouMoshi( konkaiListBean.getStrMshDate() );	// 模試月取得
								workCell = cm.setCell( workSheet, workRow, workCell, 51+kakaiCnt, 1 );
								workCell.setCellValue( cm.toString(konkaiListBean.getStrMshmei())+moshi );
								workCell = cm.setCell( workSheet, workRow, workCell, 56+kakaiCnt, 1 );
								workCell.setCellValue( cm.toString(konkaiListBean.getStrMshmei())+moshi );

								// 合計人数セット
								if ( konkaiListBean.getIntNinzu() != -999 ) {
									workCell = cm.setCell( workSheet, workRow, workCell, 51+kakaiCnt, 40 );
									workCell.setCellValue( konkaiListBean.getIntNinzu() );
								}

								// 平均点セット
								if ( konkaiListBean.getFloHeikin() != -999.0 ) {
									workCell = cm.setCell( workSheet, workRow, workCell, 51+kakaiCnt, 41 );
									workCell.setCellValue( konkaiListBean.getFloHeikin() );
								}

								// 平均偏差セット
								if ( konkaiListBean.getFloHensa() != -999.0 ) {
									workCell = cm.setCell( workSheet, workRow, workCell, 51+kakaiCnt, 42 );
									workCell.setCellValue( konkaiListBean.getFloHensa() );
								}
							}
	
							// 構成比セット
							if ( s52BnpListBean.getFloKoseihi() != -999.0 ) {
								workCell = cm.setCell( workSheet, workRow, workCell,  51+kakaiCnt, 39-2*col);
								workCell.setCellValue( s52BnpListBean.getFloKoseihi() );
	
								// *セット準備
								if( s52BnpListBean.getFloKoseihi() > koseihi ){
									koseihi = s52BnpListBean.getFloKoseihi();
									setCol = col;
								}
							}
	
							// 人数セット
							if ( s52BnpListBean.getIntNinzu() != -999 ) {
								workCell = cm.setCell( workSheet, workRow, workCell, 56+kakaiCnt, 39-2*col);
								workCell.setCellValue( s52BnpListBean.getIntNinzu() );
							}
	
							col++;
	
							// *セット
							if(col >=maxNo){
								if(setCol!=-1){
									workCell = cm.setCell( workSheet, workRow, workCell,  51+kakaiCnt, 38-2*setCol );
									workCell.setCellValue("*");
								}
								koseihi=0;
								setCol=-1;
							}
						}
						kakaiCnt++;
						bolKonkaiFlg = false;
					}
	
					//データの保持
					ArrayList	s52BnpList		= s52ListBean.getS52BnpList();
					Iterator	itrS52Bnp		= s52BnpList.iterator();

					// 偏差値データセット
					col	= 0;
					koseihi=0;
					while(itrS52Bnp.hasNext()){
						s52BnpListBean	= (S52BnpListBean) itrS52Bnp.next();
	
						// 模試名セット
						if(col == 0){
							moshi =cm.setTaisyouMoshi( s52ListBean.getStrMshDate() );	// 模試月取得
							workCell = cm.setCell( workSheet, workRow, workCell, 51+kakaiCnt, 1 );
							workCell.setCellValue( cm.toString(s52ListBean.getStrMshmei())+moshi );
							workCell = cm.setCell( workSheet, workRow, workCell, 56+kakaiCnt, 1 );
							workCell.setCellValue( cm.toString(s52ListBean.getStrMshmei())+moshi );

							// 合計人数セット
							if ( s52ListBean.getIntNinzu() != -999 ) {
								workCell = cm.setCell( workSheet, workRow, workCell, 51+kakaiCnt, 40 );
								workCell.setCellValue( s52ListBean.getIntNinzu() );
							}

							// 平均点セット
							if ( s52ListBean.getFloHeikin() != -999.0 ) {
								workCell = cm.setCell( workSheet, workRow, workCell, 51+kakaiCnt, 41 );
								workCell.setCellValue( s52ListBean.getFloHeikin() );
							}

							// 平均偏差セット
							if ( s52ListBean.getFloHensa() != -999.0 ) {
								workCell = cm.setCell( workSheet, workRow, workCell, 51+kakaiCnt, 42 );
								workCell.setCellValue( s52ListBean.getFloHensa() );
							}
						}
	
						// 構成比セット
						if ( s52BnpListBean.getFloKoseihi() != -999.0 ) {
							workCell = cm.setCell( workSheet, workRow, workCell,  51+kakaiCnt, 39-2*col);
							workCell.setCellValue( s52BnpListBean.getFloKoseihi() );
	
							// *セット準備
							if( s52BnpListBean.getFloKoseihi() > koseihi ){
								koseihi = s52BnpListBean.getFloKoseihi();
								setCol = col;
							}
						}
	
						// 人数セット
						if ( s52BnpListBean.getIntNinzu() != -999 ) {
							workCell = cm.setCell( workSheet, workRow, workCell, 56+kakaiCnt, 39-2*col);
							workCell.setCellValue( s52BnpListBean.getIntNinzu() );
						}
	
						col++;
	
						// *セット
						if(col >=maxNo){
							if(setCol!=-1){
								workCell = cm.setCell( workSheet, workRow, workCell,  51+kakaiCnt, 38-2*setCol );
								workCell.setCellValue("*");
							}
							koseihi=0;
							setCol=-1;
						}
					}
					kakaiCnt++;
				}

			}
//add 2004/10/27 T.Sakai データ0件対応
			if ( dispKmkFlgCnt==0 ) {
				// マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				bolBookCngFlg	= false;
				maxSheetIndex	= 0;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet(Integer.toString(maxSheetIndex+1));
				maxSheetIndex++;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);

				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s52Item.getIntSecuFlg() ,41 ,42 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 41 );
				workCell.setCellValue(secuFlg);

				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s52Item.getStrGakkomei()) );

				// 対象模試セット
				moshi =cm.setTaisyouMoshi( s52Item.getStrMshDate() );	// 模試月取得
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s52Item.getStrMshmei()) + moshi);
				
//add 2004/10/27 T.Sakai スペース対応
  				if (s52Item.getIntShubetsuFlg() == 1){
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
					workCell.setCellValue( "〜 19" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
					workCell.setCellValue( " 20〜\n 29" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
					workCell.setCellValue( " 30〜\n 39" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
					workCell.setCellValue( " 40〜\n 49" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
					workCell.setCellValue( " 50〜\n 59" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
					workCell.setCellValue( " 60〜\n 69" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
					workCell.setCellValue( " 70〜\n 79" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
					workCell.setCellValue( " 80〜\n 89" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
					workCell.setCellValue( " 90〜\n 99" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
					workCell.setCellValue( "100〜\n109" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
					workCell.setCellValue( "110〜\n119" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
					workCell.setCellValue( "120〜\n129" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
					workCell.setCellValue( "130〜\n139" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
					workCell.setCellValue( "140〜\n149" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
					workCell.setCellValue( "150〜\n159" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
					workCell.setCellValue( "160〜\n169" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
					workCell.setCellValue( "170〜\n179" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
					workCell.setCellValue( "180〜\n189" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
					workCell.setCellValue( "190〜" );
  				} else{
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
					workCell.setCellValue( "〜32.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
					workCell.setCellValue( "32.5〜\n34.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
					workCell.setCellValue( "35.0〜\n37.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
					workCell.setCellValue( "37.5〜\n39.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
					workCell.setCellValue( "40.0〜\n42.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
					workCell.setCellValue( "42.5〜\n44.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
					workCell.setCellValue( "45.0〜\n47.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
					workCell.setCellValue( "47.5〜\n49.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
					workCell.setCellValue( "50.0〜\n52.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
					workCell.setCellValue( "52.5〜\n54.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
					workCell.setCellValue( "55.0〜\n57.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
					workCell.setCellValue( "57.5〜\n59.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
					workCell.setCellValue( "60.0〜\n62.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
					workCell.setCellValue( "62.5〜\n64.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
					workCell.setCellValue( "65.0〜\n67.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
					workCell.setCellValue( "67.5〜\n69.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
					workCell.setCellValue( "70.0〜\n72.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
					workCell.setCellValue( "72.5〜\n74.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
					workCell.setCellValue( "75.0〜" );
  				}
//add end
			}
//add end
			if( bolBookCngFlg == false){
				boolean bolRet = false;
				// Excelファイル保存
				if(intBookCngCount!=0){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}
			
		} catch(Exception e) {
			log.Err("S52_04","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S52_04","S52_04帳票作成終了","");
		return noerror;
	}

}
/*
 * 作成日: 2004/09/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.news;

import com.fjh.forms.ActionForm;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class N002Form extends ActionForm {

	private String infoId = "";		// お知らせID
	private String displayDiv = "0";	// 表示区分 "0":お知らせ "1":Kei-Net情報

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * @return
	 */
	public String getInfoId() {
		return infoId;
	}

	/**
	 * @param string
	 */
	public void setInfoId(String string) {
		infoId = string;
	}

	/**
	 * @return
	 */
	public String getDisplayDiv() {
		return displayDiv;
	}

	/**
	 * @param string
	 */
	public void setDisplayDiv(String string) {
		displayDiv = string;
	}

}

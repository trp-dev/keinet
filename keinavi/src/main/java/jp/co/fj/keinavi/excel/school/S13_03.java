/**
 * 校内成績分析−校内成績　設問別成績
 * 	Excelファイル編集
 * 作成日: 2004/07/30
 * @author	H.Fujimoto
 *
 * <2010年度改修>
 * 2010.01.21	Fujito URAKAWA - Totec
 *              設問名称追加表示／大学コード入力機能廃止対応
 *
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S13Item;
import jp.co.fj.keinavi.excel.data.school.S13MarkListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S13_03 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	private boolean bolBookCngFlg = true;			//改ファイルフラグ
	private boolean bolSheetCngFlg = true;			//改シートフラグ

	final private String masterfile0 = "S13_03";
	final private String masterfile1 = "S13_03";
	private String masterfile = "";

	private boolean bolBookCngEndFlg = true;			//改ファイル終了フラグ


/*
 * 	Excel編集メイン
 * 		S13Item s13Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s13_03EditExcel(S13Item s13Item, ArrayList outfilelist, int intSaveFlg, String UserID ) {

		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;

		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intBookCngCount = 0;

		int row = 0;
		int col = 0;
		int setsumonCnt = 0;
		String kmk = "";

		// *作成用
		int intColCnt = 0;
		int Datarow[];
		int Datacol[];
		float DataSa[];

		float floHikaku = 0;
		float floSeitoritsu1 = 0;
		int cntritsu1 = 0;
		float floSeitoritsu2 = 0;
		int cntritsu2 = 0;
		float floSeitoritsu3 = 0;
		int cntritsu3 = 0;
		float floSeitoritsu4 = 0;
		int cntritsu4 = 0;
		float floSeitoritsu5 = 0;
		int cntritsu5 = 0;

		ArrayList WorkBooklist;
		ArrayList WorkSheetlist;
		ArrayList WorkRowlist;
		ArrayList WorkCelllist;

		HSSFWorkbook	workbookNow = null;
		HSSFSheet	workSheetNow		= null;
		HSSFRow		workRowNow			= null;
		HSSFCell	workCellNow		= null;

		HSSFWorkbook	workbookKari = null;

		KNLog log = KNLog.getInstance(null,null,null);

		//列チェック
		int intCol = 0;

		//テンプレートの決定
		if (s13Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S13MarkListBean s13MarkListBean = new S13MarkListBean();

		try {
			// データセット
			ArrayList s13MarkList = s13Item.getS13MarkList();
			Iterator itr = s13MarkList.iterator();

			log.Ep("S13_03","基本データデータセット開始","");

			WorkBooklist = new ArrayList();
			WorkSheetlist = new ArrayList();
			WorkRowlist = new ArrayList();
			WorkCelllist = new ArrayList();
			Datarow = new int[s13MarkList.size()];
			Datacol = new int[s13MarkList.size()];
			DataSa = new float[s13MarkList.size()];

			if( itr.hasNext() == false ){
				return errfdata;
			}

			while( itr.hasNext() ) {
				s13MarkListBean = (S13MarkListBean)itr.next();

				log.Ep("S13_03","マークシートデータセット開始","");
				//科目が変われば*出力＆問題数初期化
				if (!cm.toString(kmk).equals(cm.toString(s13MarkListBean.getStrKmkmei()))){
					//*出力

					//現在のファイル、シート情報の退避
					workbookNow = workbook;
					workSheetNow = workSheet;
					workRowNow = workRow;
					workCellNow = workCell;

					if (cntritsu1 >= 5) {
						for(int j=0; j<setsumonCnt; j++){
							if(DataSa[j] == floSeitoritsu1){
								workbook = (HSSFWorkbook)WorkBooklist.get(j);
								workSheet = (HSSFSheet)WorkSheetlist.get(j);
								workRow = (HSSFRow)WorkRowlist.get(j);
								workCell = (HSSFCell)WorkCelllist.get(j);
								row = Datarow[j];
								col = Datacol[j];
								workCell = cm.setCell( workSheet, workRow, workCell, row, col );
								workCell.setCellValue( "*" );
							}
						}
					}else if (cntritsu1 + cntritsu2 >= 5) {
						for(int j=0; j<setsumonCnt; j++){
							if(DataSa[j] == floSeitoritsu1 || DataSa[j] == floSeitoritsu2){
								workbook = (HSSFWorkbook)WorkBooklist.get(j);
								workSheet = (HSSFSheet)WorkSheetlist.get(j);
								workRow = (HSSFRow)WorkRowlist.get(j);
								workCell = (HSSFCell)WorkCelllist.get(j);
								row = Datarow[j];
								col = Datacol[j];
								workCell = cm.setCell( workSheet, workRow, workCell, row, col );
								workCell.setCellValue( "*" );
							}
						}
					}else if (cntritsu1 + cntritsu2 + cntritsu3 >= 5) {
						for(int j=0; j<setsumonCnt; j++){
							if(DataSa[j] == floSeitoritsu1 || DataSa[j] == floSeitoritsu2 || DataSa[j] == floSeitoritsu3){
								workbook = (HSSFWorkbook)WorkBooklist.get(j);
								workSheet = (HSSFSheet)WorkSheetlist.get(j);
								workRow = (HSSFRow)WorkRowlist.get(j);
								workCell = (HSSFCell)WorkCelllist.get(j);
								row = Datarow[j];
								col = Datacol[j];
								workCell = cm.setCell( workSheet, workRow, workCell, row, col );
								workCell.setCellValue( "*" );
							}
						}
					}else if (cntritsu1 + cntritsu2 + cntritsu3 + cntritsu4 >= 5) {
						for(int j=0; j<setsumonCnt; j++){
							if(DataSa[j] == floSeitoritsu1 || DataSa[j] == floSeitoritsu2 || DataSa[j] == floSeitoritsu3 || DataSa[j] == floSeitoritsu4){
								workbook = (HSSFWorkbook)WorkBooklist.get(j);
								workSheet = (HSSFSheet)WorkSheetlist.get(j);
								workRow = (HSSFRow)WorkRowlist.get(j);
								workCell = (HSSFCell)WorkCelllist.get(j);
								row = Datarow[j];
								col = Datacol[j];
								workCell = cm.setCell( workSheet, workRow, workCell, row, col );
								workCell.setCellValue( "*" );
							}
						}
					}else if (cntritsu1 + cntritsu2 + cntritsu3 + cntritsu4 + cntritsu5 >= 5) {
						for(int j=0; j<setsumonCnt; j++){
							if(DataSa[j] == floSeitoritsu1 || DataSa[j] == floSeitoritsu2 || DataSa[j] == floSeitoritsu3 || DataSa[j] == floSeitoritsu4 || DataSa[j] == floSeitoritsu5){
								workbook = (HSSFWorkbook)WorkBooklist.get(j);
								workSheet = (HSSFSheet)WorkSheetlist.get(j);
								workRow = (HSSFRow)WorkRowlist.get(j);
								workCell = (HSSFCell)WorkCelllist.get(j);
								row = Datarow[j];
								col = Datacol[j];
								workCell = cm.setCell( workSheet, workRow, workCell, row, col );
								workCell.setCellValue( "*" );
							}
						}
					}

					//ファイルをまたがっていればファイル保存
					if( bolBookCngEndFlg == false){
						if( intMaxSheetIndex!=0 ) {
							workbook = workbookKari;

							boolean bolRet = false;
							// Excelファイル保存
							if(itr.hasNext() == true){
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, 50);
							}
							else{
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, 50);
							}

							if( bolRet == false ){
								return errfwrite;
							}

							bolBookCngEndFlg = true;
							workbookKari = null;
						}
					}

					//現在のファイル、シート情報の復元
					workbook = workbookNow;
					workSheet = workSheetNow;
					workRow = workRowNow;
					workCell = workCellNow;

					WorkBooklist = new ArrayList();
					WorkSheetlist = new ArrayList();
					WorkRowlist = new ArrayList();
					WorkCelllist = new ArrayList();
					Datarow = new int[s13MarkList.size()];
					Datacol = new int[s13MarkList.size()];
					DataSa = new float[s13MarkList.size()];
					floHikaku = 0;
					floSeitoritsu1 = 0;
					cntritsu1 = 0;
					floSeitoritsu2 = 0;
					cntritsu2 = 0;
					floSeitoritsu3 = 0;
					cntritsu3 = 0;
					floSeitoritsu4 = 0;
					cntritsu4 = 0;
					floSeitoritsu5 = 0;
					cntritsu5 = 0;
					setsumonCnt = 0;
					intColCnt = 0;

					//改シート時のチェック
					//3列目の時
					if ( intCol==2 ) {
						intCol = 1;
						intColCnt++;
						bolSheetCngFlg = true;
						if (intMaxSheetIndex==50) {
							bolBookCngFlg = true;
						}
					//改列時
					}else{
						intCol++;
						intColCnt++;
						row = 8;
					}
				}
				//2019/09/02 QQ)oosaki 明細出力行数を変更
				////解答番号数が５０の倍数の時
				//if ( setsumonCnt == (50*intColCnt)) {
				//解答番号数が４８の倍数の時
				if ( setsumonCnt == (48*intColCnt)) {
				//2019/09/02 QQ)oosaki 明細出力行数を変更
					//改シート時のチェック
					//3列目の時
					if ( intCol==2 ) {
						intCol = 1;
						intColCnt++;
						bolSheetCngFlg = true;
						if (intMaxSheetIndex==50) {
							bolBookCngFlg = true;
						}
					//改列時
					}else{
						intCol++;
						intColCnt++;
						row = 8;
					}
				}

				//50シート以上で次ファイルを作成
				if( bolBookCngFlg == true){
					if( intMaxSheetIndex!=0 ) {
						intBookCngCount++;
						bolBookCngEndFlg = false;
						workbookKari = workbook;
					}
					// マスタExcel読み込み
					workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
					if( workbook==null ){
						return errfread;
					}


					bolBookCngFlg = false;
					bolSheetCngFlg = true;
					intMaxSheetIndex = 0;
				}

				if( bolSheetCngFlg == true ){
					//列が３列を超えると改シート
					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);

					intCol = 1;

					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);

					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, s13Item.getIntSecuFlg() ,29 ,31 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 29 );
					workCell.setCellValue(secuFlg);

					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( "学校名　：" + cm.toString(s13Item.getStrGakkomei()) );

					// 模試月取得
					String moshi =cm.setTaisyouMoshi( s13Item.getStrMshDate() );
					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
					workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s13Item.getStrMshmei()) + moshi);

					// 型・科目名＋配点セット
					String haiten = "";
					if ( !cm.toString(s13MarkListBean.getStrHaitenKmk()).equals("") ) {
						haiten = "（" + s13MarkListBean.getStrHaitenKmk() + "）";
					}
					workCell = cm.setCell( workSheet, workRow, workCell, 5, 0 );
					workCell.setCellValue( "型・科目：" + cm.toString(s13MarkListBean.getStrKmkmei()) + haiten );

					// 受験者数セット
					workCell = cm.setCell( workSheet, workRow, workCell, 5, 9 );
					if ( s13MarkListBean.getIntNinzu()!=-999 ) {
						workCell.setCellValue( "受験者数：" + s13MarkListBean.getIntNinzu() + "人" );
					} else {
						workCell.setCellValue( "受験者数：" );
					}

					bolSheetCngFlg = false;

					row = 8;
					col = 0;
					intMaxSheetIndex++;		//シート数カウンタインクリメント

				} else if(!cm.toString(kmk).equals(cm.toString(s13MarkListBean.getStrKmkmei()))) {
					//科目が変わって改列処理
					// 型・科目名＋配点セット
					String haiten = "";
					if ( !cm.toString(s13MarkListBean.getStrHaitenKmk()).equals("") ) {
						haiten = "（" + s13MarkListBean.getStrHaitenKmk() + "）";
					}
					workCell = cm.setCell( workSheet, workRow, workCell, 5, (intCol-1)*17 );
					workCell.setCellValue( "型・科目：" + cm.toString(s13MarkListBean.getStrKmkmei()) + haiten );

					// 受験者数セット
					workCell = cm.setCell( workSheet, workRow, workCell, 5, 9 + (intCol-1)*17 );
					if ( s13MarkListBean.getIntNinzu()!=-999 ) {
						workCell.setCellValue( "受験者数：" + s13MarkListBean.getIntNinzu() + "人" );
					} else {
						workCell.setCellValue( "受験者数：" );
					}
				}

				col = (intCol-1)*17;

				// 大問・内容セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col++ );
				if ( !cm.toString(s13MarkListBean.getStrDaimonNaiyo()).equals("") ) {
					workCell.setCellValue( s13MarkListBean.getStrDaimonNaiyo() );
				}

				// 完答問題フラグセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col++ );
				if (s13MarkListBean.getIntKantoRcFlg() == 0 && s13MarkListBean.getIntKantoFlg() == 1) {
					workCell.setCellValue( "●" );
				}else if (s13MarkListBean.getIntKantoRcFlg() == 1 && s13MarkListBean.getIntKantoFlg() == 0) {
					workCell.setCellValue( "完答" );
				}
				// 解答番号セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col++ );
				if (s13MarkListBean.getIntKantoRcFlg() == 0) {
					workCell.setCellValue( s13MarkListBean.getStrKaitoNo() );
				}
				// 配点セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col++ );
			    if (s13MarkListBean.getIntKantoFlg() == 0) {
					workCell.setCellValue( s13MarkListBean.getStrHaiten() );
				}
				// 正答セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col++ );
				if (s13MarkListBean.getIntKantoRcFlg() == 0) {
					workCell.setCellValue( s13MarkListBean.getStrSeito() );
				}
				// 校内正答率セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col++ );
				if ( s13MarkListBean.getFloSeitoritsuHome()!=-999.0 ) {
					workCell.setCellValue( s13MarkListBean.getFloSeitoritsuHome() );
				}
				// 全国正答率セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col++ );
				if ( s13MarkListBean.getFloSeitoritsuAll()!=-999.0 ) {
					workCell.setCellValue( s13MarkListBean.getFloSeitoritsuAll() );
				}

				// 全国差セット
				col++;
				floHikaku = s13MarkListBean.getFloSeitoritsuHome() - s13MarkListBean.getFloSeitoritsuAll();
				workCell = cm.setCell( workSheet, workRow, workCell, row, col++ );
				if ( s13MarkListBean.getFloSeitoritsuHome()!=-999.0 && s13MarkListBean.getFloSeitoritsuAll()!=-999.0 ) {
					workCell.setCellValue( floHikaku );
				}
				// *セット
				WorkBooklist.add(setsumonCnt,workbook);
				WorkSheetlist.add(setsumonCnt,workSheet);
				WorkRowlist.add(setsumonCnt,workRow);
				WorkCelllist.add(setsumonCnt,workCell);
				if (s13MarkListBean.getIntKantoFlg() == 0
				&& (s13MarkListBean.getFloSeitoritsuHome()!=-999.0 && s13MarkListBean.getFloSeitoritsuAll()!=-999.0 )) {
					Datarow[setsumonCnt] = row;
					DataSa[setsumonCnt] = floHikaku;
					Datacol[setsumonCnt] = col - 2;

					if (floHikaku < floSeitoritsu5 ) {
						if (floHikaku < floSeitoritsu4 ) {
							if (floHikaku < floSeitoritsu3 ) {
								if (floHikaku < floSeitoritsu2 ) {
									if (floHikaku < floSeitoritsu1 ) {
										floSeitoritsu5 = floSeitoritsu4;
										cntritsu5 = cntritsu4;
										floSeitoritsu4 = floSeitoritsu3;
										cntritsu4 = cntritsu3;
										floSeitoritsu3 = floSeitoritsu2;
										cntritsu3 = cntritsu2;
										floSeitoritsu2 = floSeitoritsu1;
										cntritsu2 = cntritsu1;
										floSeitoritsu1 = floHikaku;
										cntritsu1=1;
									}else if(floHikaku == floSeitoritsu1) {
										cntritsu1++;
									}else {
										floSeitoritsu5 = floSeitoritsu4;
										cntritsu5 = cntritsu4;
										floSeitoritsu4 = floSeitoritsu3;
										cntritsu4 = cntritsu3;
										floSeitoritsu3 = floSeitoritsu2;
										cntritsu3 = cntritsu2;
										floSeitoritsu2 = floHikaku;
										cntritsu2=1;
									}
								}else if(floHikaku == floSeitoritsu2){
									cntritsu2++;
								}else {
									floSeitoritsu5 = floSeitoritsu4;
									cntritsu5 = cntritsu4;
									floSeitoritsu4 = floSeitoritsu3;
									cntritsu4 = cntritsu3;
									floSeitoritsu3 = floHikaku;
									cntritsu3=1;
								}
							}else if (floHikaku == floSeitoritsu3){
								cntritsu3++;
							}else {
								floSeitoritsu5 = floSeitoritsu4;
								cntritsu5 = cntritsu4;
								floSeitoritsu4 = floHikaku;
								cntritsu4=1;
							}
						}else if(floHikaku == floSeitoritsu4){
							cntritsu4++;
						}else {
							floSeitoritsu5 = floHikaku;
							cntritsu5=1;
						}
					}else if (floHikaku == floSeitoritsu5) {
						cntritsu5++;
					}
				}else {
					Datarow[setsumonCnt] = row;
					Datacol[setsumonCnt] = col - 2;
					DataSa[setsumonCnt] = 0;
				}

				if (s13MarkListBean.getIntKantoRcFlg() == 0) {
					// 誤答パターン１セット
					workCell = cm.setCell( workSheet, workRow, workCell, row, col++ );
					if ( !cm.toString(s13MarkListBean.getStrGoto1()).equals("") ) {
						workCell.setCellValue( s13MarkListBean.getStrGoto1() + "−");
					} else {
						workCell.setCellValue( " −");
					}
					if (cm.toString(s13MarkListBean.getStrGoto1()).equals("N")) {
						workCell.setCellValue( "無−");
					}else if (cm.toString(s13MarkListBean.getStrGoto1()).equals("W")) {
						workCell.setCellValue( "W−");
					}
					// 誤答パターン１マーク率セット
					workCell = cm.setCell( workSheet, workRow, workCell, row, col++ );
					if ( s13MarkListBean.getFloGoto1Markritsu()!=-999.0 ) {
						workCell.setCellValue( s13MarkListBean.getFloGoto1Markritsu() );
					}

					// 誤答パターン２セット
					workCell = cm.setCell( workSheet, workRow, workCell, row, col++ );
					if ( !cm.toString(s13MarkListBean.getStrGoto2()).equals("") ) {
						workCell.setCellValue( s13MarkListBean.getStrGoto2() + "−" );
					} else {
						workCell.setCellValue( " −");
					}
					if (cm.toString(s13MarkListBean.getStrGoto2()).equals("N")) {
						workCell.setCellValue( "無−");
					}else if (cm.toString(s13MarkListBean.getStrGoto2()).equals("W")) {
						workCell.setCellValue( "W−");
					}
					// 誤答パターン２マーク率セット
					workCell = cm.setCell( workSheet, workRow, workCell, row, col++ );
					if ( s13MarkListBean.getFloGoto2Markritsu()!=-999.0 ) {
						workCell.setCellValue( s13MarkListBean.getFloGoto2Markritsu() );
					}
					// 誤答パターン３セット
					workCell = cm.setCell( workSheet, workRow, workCell, row, col++ );
					if ( !cm.toString(s13MarkListBean.getStrGoto3()).equals("") ) {
						workCell.setCellValue( s13MarkListBean.getStrGoto3() + "−" );
					} else {
						workCell.setCellValue( " −");
					}
					if (cm.toString(s13MarkListBean.getStrGoto3()).equals("N")) {
						workCell.setCellValue( "無−");
					}else if (cm.toString(s13MarkListBean.getStrGoto3()).equals("W")) {
						workCell.setCellValue( "W−");
					}
					// 誤答パターン３マーク率セット
					workCell = cm.setCell( workSheet, workRow, workCell, row, col++ );
					if ( s13MarkListBean.getFloGoto3Markritsu()!=-999.0 ) {
						workCell.setCellValue( s13MarkListBean.getFloGoto3Markritsu() );
					}
				}

				setsumonCnt++;
				row++;
				kmk = s13MarkListBean.getStrKmkmei();

			}

			log.Ep("S13_03","マークシートデータセット終了","");

			//最後の科目の*出力

			//現在のファイル、シート情報の退避
			workbookNow = workbook;
			workSheetNow = workSheet;
			workRowNow = workRow;
			workCellNow = workCell;

			if (cntritsu1 >= 5) {
				for(int j=0; j<setsumonCnt; j++){
					if(DataSa[j] == floSeitoritsu1){
						workbook = (HSSFWorkbook)WorkBooklist.get(j);
						workSheet = (HSSFSheet)WorkSheetlist.get(j);
						workRow = (HSSFRow)WorkRowlist.get(j);
						workCell = (HSSFCell)WorkCelllist.get(j);
						row = Datarow[j];
						col = Datacol[j];
						workCell = cm.setCell( workSheet, workRow, workCell, row, col );
						workCell.setCellValue( "*" );
					}
				}
			}else if (cntritsu1 + cntritsu2 >= 5) {
				for(int j=0; j<setsumonCnt; j++){
					if(DataSa[j] == floSeitoritsu1 || DataSa[j] == floSeitoritsu2){
						workbook = (HSSFWorkbook)WorkBooklist.get(j);
						workSheet = (HSSFSheet)WorkSheetlist.get(j);
						workRow = (HSSFRow)WorkRowlist.get(j);
						workCell = (HSSFCell)WorkCelllist.get(j);
						row = Datarow[j];
						col = Datacol[j];
						workCell = cm.setCell( workSheet, workRow, workCell, row, col );
						workCell.setCellValue( "*" );
					}
				}
			}else if (cntritsu1 + cntritsu2 + cntritsu3 >= 5) {
				for(int j=0; j<setsumonCnt; j++){
					if(DataSa[j] == floSeitoritsu1 || DataSa[j] == floSeitoritsu2 || DataSa[j] == floSeitoritsu3){
						workbook = (HSSFWorkbook)WorkBooklist.get(j);
						workSheet = (HSSFSheet)WorkSheetlist.get(j);
						workRow = (HSSFRow)WorkRowlist.get(j);
						workCell = (HSSFCell)WorkCelllist.get(j);
						row = Datarow[j];
						col = Datacol[j];
						workCell = cm.setCell( workSheet, workRow, workCell, row, col );
						workCell.setCellValue( "*" );
					}
				}
			}else if (cntritsu1 + cntritsu2 + cntritsu3 + cntritsu4 >= 5) {
				for(int j=0; j<setsumonCnt; j++){
					if(DataSa[j] == floSeitoritsu1 || DataSa[j] == floSeitoritsu2 || DataSa[j] == floSeitoritsu3 || DataSa[j] == floSeitoritsu4){
						workbook = (HSSFWorkbook)WorkBooklist.get(j);
						workSheet = (HSSFSheet)WorkSheetlist.get(j);
						workRow = (HSSFRow)WorkRowlist.get(j);
						workCell = (HSSFCell)WorkCelllist.get(j);
						row = Datarow[j];
						col = Datacol[j];
						workCell = cm.setCell( workSheet, workRow, workCell, row, col );
						workCell.setCellValue( "*" );
					}
				}
			}else if (cntritsu1 + cntritsu2 + cntritsu3 + cntritsu4 + cntritsu5 >= 5) {
				for(int j=0; j<setsumonCnt; j++){
					if(DataSa[j] == floSeitoritsu1 || DataSa[j] == floSeitoritsu2 || DataSa[j] == floSeitoritsu3 || DataSa[j] == floSeitoritsu4 || DataSa[j] == floSeitoritsu5){
						workbook = (HSSFWorkbook)WorkBooklist.get(j);
						workSheet = (HSSFSheet)WorkSheetlist.get(j);
						workRow = (HSSFRow)WorkRowlist.get(j);
						workCell = (HSSFCell)WorkCelllist.get(j);
						row = Datarow[j];
						col = Datacol[j];
						workCell = cm.setCell( workSheet, workRow, workCell, row, col );
						workCell.setCellValue( "*" );
					}
				}
			}

			//現在のファイル、シート情報の復元
			workbook = workbookNow;
			workSheet = workSheetNow;
			workRow = workRowNow;
			workCell = workCellNow;

			//最後の科目で改ファイルの時
			if(bolBookCngEndFlg == false){

				workbookNow = workbook;
				workbook = workbookKari;

				boolean bolRet = false;

				// Excelファイル保存
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, 50);

				if( bolRet == false ){
					return errfwrite;
				}

				bolBookCngEndFlg = true;
				workbook = workbookNow;
				workbookKari = null;
			}
//add 2004/10/25 T.Sakai データ0件対応
			if ( s13MarkList.size()==0 ) {
				// マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				bolBookCngFlg = false;

				// シートテンプレートのコピー
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex++;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);

				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s13Item.getIntSecuFlg() ,29 ,31 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 29 );
				workCell.setCellValue(secuFlg);

				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s13Item.getStrGakkomei()) );

				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s13Item.getStrMshDate() );
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s13Item.getStrMshmei()) + moshi);
			}
//add end
			if(bolBookCngFlg == false){

				// Excelファイル保存
				boolean bolRet = false;

				if(intBookCngCount > 0 || itr.hasNext() == true){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
				}
				else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;

				}
			}

		} catch(Exception e) {
			log.Err("S13_03","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S13_03","基本データデータセット終了","");
		return noerror;
	}
}
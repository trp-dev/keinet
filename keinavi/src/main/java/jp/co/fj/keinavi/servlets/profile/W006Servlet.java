package jp.co.fj.keinavi.servlets.profile;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.profile.CountingDivBean;
import jp.co.fj.keinavi.beans.profile.ProfileFolderBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.profile.W006Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class W006Servlet extends DefaultHttpServlet {

	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// ログイン情報
		LoginSession login = super.getLoginSession(request);
		// プロファイル
		Profile profile = super.getProfile(request);

		// アクションフォーム
		W006Form form = (W006Form) super.getActionForm(request,
			"jp.co.fj.keinavi.forms.profile.W006Form");
	
		// 転送元がw006なら名前・コメント・フォルダは保持
		if ("w006".equals(super.getBackward(request))) {
			profile.setProfileName(form.getProfileName());
			profile.setComment(form.getComment());
			profile.setFolderID(form.getCurrent());
		}

		// JSP
		if ("w006".equals(super.getForward(request))) {
			// DBコネクション
			Connection con = null;
			try {
				con = super.getConnectionPool(request);
				con.setAutoCommit(false);
				// 集計区分Beanを作る
				if (login.isCountingDiv()) {
					// 集計区分Bean
					CountingDivBean cdb = new CountingDivBean();
					cdb.setConnection(null, con);
					cdb.setSchoolCD(login.getUserID());
					cdb.execute();
					request.setAttribute("CountingDivBean", cdb);
				}

				// プロファイルフォルダBean
				ProfileFolderBean bean = new ProfileFolderBean();
				bean.setConnection(null, con);
				bean.setLoginSession(login);
				bean.setBundleCD(form.getCountingDivCode());
				bean.execute();
				request.setAttribute("ProfileFolderBean", bean);
			} catch (Exception e) {
				throw new ServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}

			// アクションフォームをセット
			request.setAttribute("form", form);
			// 現在時刻をセット
			request.setAttribute("now", new Date());

			super.forward(request, response, JSP_W006);

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

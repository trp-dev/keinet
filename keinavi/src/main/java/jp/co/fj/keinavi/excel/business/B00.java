/**
 * 校内成績分析−全国総合成績概況
 * 出力する帳票の判断
 * 作成日: 2004/07/30
 * @author	Ito.Y
 */
 
package jp.co.fj.keinavi.excel.business;

import java.util.*;

import jp.co.fj.keinavi.excel.data.business.*;
import jp.co.fj.keinavi.util.log.*;

public class B00 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean b00(B00Item b00Item, ArrayList outfilelist, int intSaveFlg, String UserID, KNSheetLog sheetLog) {
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		try{
			int ret = 0;
			//B00Itemから各帳票を出力
			if ( b00Item.getIntMshFlg()==0 ) {
				throw new Exception("B00 ERROR : フラグ異常");
			}
			if ( b00Item.getIntMshFlg()==1 ) {
				B00_01 exceledit = new B00_01();
				ret = exceledit.b00_01EditExcel( b00Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"B00_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("B00_01");
			}
			if ( b00Item.getIntMshFlg()==2 ) {
				B00_02 exceledit = new B00_02();
				ret = exceledit.b00_02EditExcel( b00Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"B00_02","帳票作成エラー","");
					return false;
				}
				sheetLog.add("B00_02");
			}
			
		} catch(Exception e) {
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
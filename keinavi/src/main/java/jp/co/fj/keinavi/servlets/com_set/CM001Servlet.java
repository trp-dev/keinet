package jp.co.fj.keinavi.servlets.com_set;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.com_set.CompClassBean;
import jp.co.fj.keinavi.beans.com_set.SubjectBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.com_set.CM001Form;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM001Servlet extends AbstractCMServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 共通処理
		super.execute(request, response);

		// 共通アクションフォーム - session scope
		CM001Form scform = super.getCommonForm(request);

		// 転送元がcm001
		if ("cm001".equals(super.getBackward(request))) {
			// 共通アクションフォーム - request scope
			CM001Form rcform = (CM001Form)
				super.getActionForm(request, "jp.co.fj.keinavi.forms.com_set.CM001Form");

			// 担当クラスが変更される可能性があるので
			// 保存フラグを保持する
			scform.setSave(rcform.getSave());
		}

		// JSP	
		if ("cm001".equals(super.getForward(request))) {
			// ログイン情報
			LoginSession login = super.getLoginSession(request);
			// プロファイル
			Profile profile = super.getProfile(request);

			Connection con = null; // DBコネクション
			try {
				con = super.getConnectionPool(request);

				// 型Bean
				// Taglibでの評価用
				{
					SubjectBean bean = new SubjectBean(login);
					bean.setConnection(null, con);
					bean.setMode("1");
					bean.setTargetYear(scform.getTargetYear());
					bean.setTargetExam(scform.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, scform.getTargetExam()));
					bean.execute();		
					request.setAttribute("TypeBean", bean);
				}
	
				// 科目Bean
				// Taglibでの評価用
				{
					SubjectBean bean = new SubjectBean(login);
					bean.setConnection(null, con);
					bean.setMode("2");
					bean.setTargetYear(scform.getTargetYear());
					bean.setTargetExam(scform.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, scform.getTargetExam()));
					bean.execute();
					request.setAttribute("CourseBean", bean);
				}
					
				// 比較対象クラスBean
				// Taglibでの評価用
				{
					CompClassBean bean = new CompClassBean();
					bean.setConnection(null, con);
					bean.setTargetYear(scform.getTargetYear());
					bean.setTargetExam(scform.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, scform.getTargetExam()));
					bean.setSchoolCD(login.getUserID());
					bean.execute();
					request.setAttribute("CompClassBean", bean);
				}

			} catch (Exception e) {
				throw createServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}

			super.forward(request, response, JSP_CM001);

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

package jp.co.fj.keinavi.beans.sheet.excel.business;

import java.util.List;

import jp.co.fj.keinavi.beans.sheet.common.ISheetData;
import jp.co.fj.keinavi.beans.sheet.excel.school.S42_15ExcelCreator;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15Data;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15SubjectListData;

/**
 *
 * 高校成績分析 - 高校間比較 - 偏差値分布
 * 受験学力測定テスト専用（今年度のみ）
 *
 * 2006.09.04	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class B42_07ExcelCreator extends S42_15ExcelCreator {

    /**
     * コンストラクタ
     *
     * @param pData
     * @param pSequenceId
     * @param pOutFileList
     * @param pPrintFlag
     * @throws Exception
     */
    public B42_07ExcelCreator(final ISheetData pData, final String pSequenceId,
            final List pOutFileList, final int pPrintFlag,
            final int pStartRowIndex, final int pMaxSchoolIndex) throws Exception {

        super(pData, pSequenceId, pOutFileList, pPrintFlag,
                pStartRowIndex, pMaxSchoolIndex);
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
     * 			#initSheet(org.apache.poi.hssf.usermodel.HSSFSheet)
     */
    protected void initSheet(final ISheetData pData) throws Exception {

        // データクラス
        final S42_15Data data = (S42_15Data) pData;
        // 当該シートの科目リストデータ
        final S42_15SubjectListData subData = getCurrentSubjectListData(data);

        // 作成日
        setCreateDate();
        // セキュリティスタンプ
        setSecurityStamp(data.getSecurityStamp(), 36, 37);
        // 対象模試
        setCellValue("A2", createTargetExamLabel(
                data.getExamName(), data.getInpleDate()));
        // 過年度の表示
        setCellValue("A3", createDispPastLavel(data.getDispPast()));
        // 科目
        setCellValue("A5", "科目：" + subData.getSubName());
        // センター到達エリア
        setCenterReachArea(subData.getReachLevelListData());
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
     * 			#getSheetId()
     */
    protected String getSheetId() {
        return "B42_07";
    }

}

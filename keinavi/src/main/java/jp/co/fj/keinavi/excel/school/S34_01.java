package jp.co.fj.keinavi.excel.school;

//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.ListIterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S34ClassListBean;
import jp.co.fj.keinavi.excel.data.school.S34HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.school.S34Item;
import jp.co.fj.keinavi.excel.data.school.S34ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * 作成日: 2004/07/13
 * @author C.Murata
 *
 * 校内成績分析−過年度比較−志望大学評価別人数　大学（日程なし）出力処理
 * 
 * <2010年度改修>
 * 2010.01.06   Tomohisa YAMADA - Totec
 *              センター・リサーチ５段階評価対応
 * 
 */
public class S34_01 {
	
	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow		= 50;			//MAX行数
	private int		intMaxClassSr   = 5;			//MAXクラス数

	private int intDataStartRow = 8;					//データセット開始行
	private int intDataStartCol = 8;					//データセット開始列
	
	final private String masterfile0 = "S34_01" ;
	final private String masterfile1 = "S34_01" ;
	private String masterfile = "" ;
	
	final private String strCenterResearch = "38";

	/*
	 * 	Excel編集メイン
	 * 		S34Item S34Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int s34_01EditExcel(S34Item s34Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("S34_01","S34_01帳票作成開始","");
		
		//テンプレートの決定
		if (s34Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		boolean bolBookCngFlg = true;			//改ファイルフラグ	
		boolean bolSheetCngFlg = true;			//改シートフラグ
			
		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFCell	workCell		= null;
		HSSFRow		workRow			= null;
		
		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intCol = 0;

		String strGenekiKbn = "";
		String strDaigakuCode = "";
		String strHyoukaKbn = "";
//		String strNendo = "";
		
		int intLoopCnt = 0;
		int intChangeSheetRow = 0;
//		int intNinzuListCnt = 0;
//		int intSheetCngCount = 0;
//		int intBookCngCount = -1;

		//2004.09.27Add
		String strKokushiKbn = "";
		String strStKokushiKbn = "";
		//2004.09.27AddEnd
		
		boolean bolDispHyoukaFlg = false;
		boolean bolDispDaigakuFlg = false;
		String strHyouka = "";
		
		String strDispTarget = "";
		
		ArrayList WorkBookList = new ArrayList();
		ArrayList WorkSheetList = new ArrayList();
		
		int intSheetCnt=0;
		
		int intBookIndex = -1;
		int intSheetIndex = -1;
		
//		int intSheetMei = -1;
		
		int intSetRow = intDataStartRow;
		
		boolean bolClassDispFlg=true;
		
		ArrayList HyoukaTitleList = new ArrayList();
		
		// 基本ファイルを読込む
		S34ListBean s34ListBean = new S34ListBean();
		
		try{
			
			HyoukaTitleList.add(0,"A");
			HyoukaTitleList.add(1,"B");
			HyoukaTitleList.add(2,"C");
			
			ArrayList s34List = s34Item.getS34List();
			ListIterator itr = s34List.listIterator();

			/**			
			//2004.9.28 Remove
			if( itr.hasNext() == false ){
				return errfdata;
			}
			//2004.9.28 Remove End
			 **/
			
			while( itr.hasNext() ){
				
				s34ListBean = (S34ListBean)itr.next();
				
				if( !cm.toString(strGenekiKbn).equals(cm.toString(s34ListBean.getStrGenKouKbn())) ){
					strGenekiKbn = s34ListBean.getStrGenKouKbn();
					if( cm.toString(strGenekiKbn).equals("0") ){
						strDispTarget = "全体";
					}
					else if( cm.toString(strGenekiKbn).equals("1") ){
						strDispTarget = "現役生";
					}
					else if( cm.toString(strGenekiKbn).equals("2") ){
						strDispTarget = "高卒生";
					}

					if( bolSheetCngFlg == false ){
						bolSheetCngFlg = true;
					}
				}
				
				bolDispHyoukaFlg = false;
				if( !cm.toString(strHyoukaKbn).equals(cm.toString(s34ListBean.getStrHyouka())) ){
					bolDispHyoukaFlg = true;
					//評価区分表示
					strHyoukaKbn = s34ListBean.getStrHyouka();
					strHyouka = "";
					switch( Integer.parseInt(strHyoukaKbn) ){
						case 1:
							strHyouka = "センター";
							break;
						case 2:
							strHyouka = "2次・私大";
							break;
						case 3:
							strHyouka = "総合";
							break;
					}
//					workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
//					workCell.setCellValue( strHyoka );
				
//					//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
//					cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
//												intSetRow, intSetRow, 1, 1, false, true, false, false);
				
				}

				//2004.9.27 Add
				int intDaigakuCd = (int)Integer.parseInt(s34ListBean.getStrDaigakuCd());
				if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
					strKokushiKbn = "国公立大";
				}
				if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
					strKokushiKbn = "私立大";
				}
				if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
					strKokushiKbn = "短大";
				}
				if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
					strKokushiKbn = "その他";
				}
				
				if( !strKokushiKbn.equals(strStKokushiKbn) ){
					bolSheetCngFlg = true;
					strStKokushiKbn = strKokushiKbn;
				}
				//2004.9.27 Add End
				
				bolDispDaigakuFlg = false;
				if( !cm.toString(strDaigakuCode).equals(cm.toString(s34ListBean.getStrDaigakuCd()))){
					bolDispDaigakuFlg = true;
					//大学名表示
					strDaigakuCode = s34ListBean.getStrDaigakuCd();
//					workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 0 );
//					workCell.setCellValue(s34ListBean.getStrDaigakuMei());
//					//罫線出力(セルの上部に太線を引く）
//					cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
//														intSetRow, intSetRow, 0, 32, false, true, false, false );
				}
				
				//クラス別評価人数リスト読み込み クラス名をExcelへセット
				ArrayList s34ClassList = s34ListBean.getS34ClassList();
				ListIterator itr2 = s34ClassList.listIterator();
				
				S34ClassListBean s34ClassListBean = new S34ClassListBean();
				S34ClassListBean HomeDataBean = new S34ClassListBean();

				//クラス数取得
				int intClassSr = s34ClassList.size();

//				int intSheetSr = (int)Math.ceil(intClassSr / intMaxClassSr);
//				int intSheetSr = intClassSr / intMaxClassSr;
//				if(intClassSr%intMaxClassSr!=0){
//					intSheetSr++;
//				}
				int intSheetSr = (intClassSr - 1) / intMaxClassSr;
				if((intClassSr - 1)%intMaxClassSr!=0){
					intSheetSr++;
				}
				
				int intLoopCnt2 = 0;
				int intNendoCount = 0;
				intChangeSheetRow = 0;
				
				while( itr2.hasNext() ){
					
					s34ClassListBean = (S34ClassListBean)itr2.next();

					ArrayList s34HyoukaNinzuList = s34ClassListBean.getS34HyoukaNinzuList();
					S34HyoukaNinzuListBean s34HyoukaNinzuListBean = new S34HyoukaNinzuListBean();
					
					if( intLoopCnt2 == 0 ){
						HomeDataBean = s34ClassListBean;
						s34ClassListBean = (S34ClassListBean)itr2.next();
						s34HyoukaNinzuList = s34ClassListBean.getS34HyoukaNinzuList();
						intNendoCount = s34HyoukaNinzuList.size();
						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
						intLoopCnt2++;
					}
					ListIterator itr3 = s34HyoukaNinzuList.listIterator();
					
					if( intLoopCnt2 % intMaxClassSr == 0 ){
						intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 5 );
					}else{
						intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 5 );
					}
					
//					intCol = intDataStartCol + ( ( intMaxClassSr - ( intLoopCnt2 % intMaxClassSr ) ) * 5 );
					

					int intHyoukaNinzuListIndex = 0;
					while( itr3.hasNext() ){
						
						s34HyoukaNinzuListBean = (S34HyoukaNinzuListBean)itr3.next();
						
						if(( intSetRow  >= intChangeSheetRow )||((bolSheetCngFlg == true)&&(intMaxSheetIndex != 0))){
							bolSheetCngFlg = true;
							for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//								int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
								int intBIndex = (intSindex + 1) / intMaxSheetSr;
								if((intSindex + 1)%intMaxSheetSr!=0){
									intBIndex++;
								}
								workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
								workSheet = (HSSFSheet)WorkSheetList.get(intSindex);
								
								//罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intSetRow, intSetRow, 0, 32, false, true, false, false );
								if( workSheet != null ){
									int intRemoveStartRow = intChangeSheetRow;
									if( intMaxRow + intDataStartRow > intRemoveStartRow ){
										for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
											//行削除処理
											workSheet.removeRow(workSheet.getRow(index));
										}
										//罫線出力（セルの上部に太線を出力）
										cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
														intRemoveStartRow, intRemoveStartRow, 0, 32, false, true, false, false);
									}
								}
							}
						}
						
						if( bolSheetCngFlg == true ){
							
							bolDispHyoukaFlg = true;
							bolDispDaigakuFlg = true;
														
							intSetRow = intDataStartRow;
							bolClassDispFlg = true;
							
							for( int intIndex = 0; intIndex < intSheetSr; intIndex ++ ){
								
								if( ( intMaxSheetIndex >= intMaxSheetSr ) || ( bolBookCngFlg == true ) ){
									// マスタExcel読み込み
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook==null ){
										return errfread;
									}
									WorkBookList.add(workbook);
									intBookIndex++;
					
									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
									workbook = (HSSFWorkbook)WorkBookList.get(intBookIndex);
								}
								
								//シート作成
								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);
								WorkSheetList.add(workSheet);
								intSheetIndex++;
								workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex);

								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);
				
								// セキュリティスタンプセット
								String secuFlg = cm.setSecurity( workbook, workSheet, s34Item.getIntSecuFlg(), 30, 32 );
								workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
								workCell.setCellValue(secuFlg);
				
								// 学校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
								workCell.setCellValue( "学校名　：" + cm.toString(s34Item.getStrGakkomei()) );
		
								// 模試月取得
								String moshi =cm.setTaisyouMoshi( s34Item.getStrMshDate() );
								
								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
								workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s34Item.getStrMshmei()) + moshi);
					
								//表示対象
								workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
								workCell.setCellValue( "表示対象：" + strDispTarget );
								
								//評価別人数 見出し行
								for( int i = 0; i <= intMaxClassSr; i++ ){
									int intTitleSetCol = (i * 5) + ( intDataStartCol - 3 );
									
									//出願予定・第1志望
									if (strKokushiKbn == "国公立大") { 
										workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
										workCell.setCellValue( "出願予定" );
									} else {
										workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
										workCell.setCellValue( "第１志望" );
									}
								
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol );
									workCell.setCellValue( (String)HyoukaTitleList.get(0) );
									
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+1 );
									workCell.setCellValue( (String)HyoukaTitleList.get(1) );
									
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+2 );
									workCell.setCellValue( (String)HyoukaTitleList.get(2) );
								}
							
								intMaxSheetIndex++;		//シート数カウンタインクリメント
								bolSheetCngFlg = false;
						
								intLoopCnt = 0;
							}	//ForEnd
							
						}	//if( bolSheetCngFlg == true )

//						intSheetCnt = (int)Math.ceil( (double)intLoopCnt2 / (double)intMaxClassSr );
						intSheetCnt = intLoopCnt2 / intMaxClassSr;
						if(intLoopCnt2%intMaxClassSr!=0){
							intSheetCnt++;
						}
						workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex -  (intSheetSr - intSheetCnt)  );

						int intSheetNo = intSheetIndex -  (intSheetSr - intSheetCnt);
//						int intBIndex = (int)Math.ceil((double)(intSheetNo + 1) / (double)intMaxSheetSr );
						int intBIndex = (intSheetNo + 1) / intMaxSheetSr;
						if((intSheetNo + 1)%intMaxSheetSr!=0){
							intBIndex++;
						}
						workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
										

						
						if( intCol == intDataStartCol ){
							
							//データセット
							//自校データ・大学名ほか表示
							//自校学校名
							if(HomeDataBean.getStrGakkomei()!=null){
								workCell = cm.setCell( workSheet, workRow, workCell, 5, 3 );
								workCell.setCellValue(HomeDataBean.getStrGakkomei());
							}
							//自校データ
							ListIterator itrHome = HomeDataBean.getS34HyoukaNinzuList().listIterator();

							int intLoopIndex = 0;
							while( itrHome.hasNext() ){
								S34HyoukaNinzuListBean HomeHyoukaNinzuListBean = (S34HyoukaNinzuListBean) itrHome.next();
								boolean ret = setData( workSheet, HomeHyoukaNinzuListBean, intSetRow + intLoopIndex , 3 );
								if( ret == false ){
									return errfdata;
								}
								intLoopIndex++;
							}
							
							if( bolDispHyoukaFlg == true ){
								//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
															intSetRow, intSetRow, 1, 1, false, true, false, false);
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
								workCell.setCellValue( strHyouka );
				
							}

							if( bolDispDaigakuFlg == true ){
								//罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intSetRow, intSetRow, 0, 32, false, true, false, false );
								//大学名表示
								if( s34ListBean.getStrDaigakuMei()!=null ){
									workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 0 );
									workCell.setCellValue(s34ListBean.getStrDaigakuMei());
								}
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
								workCell.setCellValue( strHyouka );
							}
							
						}

						boolean ret = setData( workSheet, s34HyoukaNinzuListBean, intSetRow + intHyoukaNinzuListIndex, intCol);
						if( ret != true){
							return errfdata;
						}
						else{
							intHyoukaNinzuListIndex++;
							intLoopCnt++;
						}
						
						//
						
					}	//WhileEnd( itr3 )
					
					
					//クラス名
					if( bolClassDispFlg == true ){
						String strGakunen = s34ClassListBean.getStrGrade();
						String strClass = s34ClassListBean.getStrClass();
						workCell = cm.setCell( workSheet, workRow, workCell, 5, intCol );
						workCell.setCellValue(cm.toString(strGakunen) + "年 " + cm.toString(strClass) + "クラス");
					}
					intLoopCnt2++;
				}	//WhileEnd( itr2 )

				intSetRow += intNendoCount;
				bolClassDispFlg = false;

				log.It("S34_01","大学名：",s34ListBean.getStrDaigakuMei());

			}	//WhileEnd( itr )
			
			for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//				int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr ); /** 20040930 **/
				int intBIndex = (intSindex + 1) / intMaxSheetSr;
				if((intSindex + 1)%intMaxSheetSr!=0){
					intBIndex++;
				}
				workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
				workSheet = (HSSFSheet)WorkSheetList.get(intSindex);
								
				//罫線出力(セルの上部に太線を引く）
				cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
													intSetRow, intSetRow, 0, 32, false, true, false, false );
				if( workSheet != null ){
					int intRemoveStartRow = intChangeSheetRow;
					if( intMaxRow + intDataStartRow > intRemoveStartRow ){
						for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
							//行削除処理
							workSheet.removeRow(workSheet.getRow(index));
						}
						//罫線出力（セルの上部に太線を出力）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
										intRemoveStartRow, intRemoveStartRow, 0, 32, false, true, false, false);
					}
				}
			
			}

			//ファイル保存
			for( int intBookSr = 0; intBookSr < WorkBookList.size(); intBookSr++ ){
				workbook = (HSSFWorkbook)WorkBookList.get(intBookSr);

				boolean bolRet = false;
				// Excelファイル保存
				if( WorkBookList.size() > 1 ){
					if( WorkBookList.size() - 1 == intBookSr ){
						bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookSr+1, masterfile, intMaxSheetIndex);
					}else{
						bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookSr+1, masterfile, intMaxSheetIndex);
					}
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
			}
			
			//データ無し
			if ( WorkBookList.size() == 0 ){
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex = 1;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s34Item.getIntSecuFlg(), 30, 32 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
				workCell.setCellValue(secuFlg);
				
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s34Item.getStrGakkomei()) );
		
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s34Item.getStrMshDate() );
								
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + s34Item.getStrMshmei() + moshi);

				//表示対象
				workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
				workCell.setCellValue( "表示対象：" );
			}
			
		}
		catch( Exception e ){
			log.Err("S34_01","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S34_01","S34_01帳票作成終了","");
		return noerror;
	}

			
		
	/*
	 * 	Excel編集
	 * 		HSSFWorkbook 	workbook: Excelワークブック
	 * 		ArrayList 		s34HyoukaNinzuList:評価別人数リスト
	 * 		int				intRow:開始行
	 * 		int				intCol:開始列
	 * 		戻り値: なし
	 */
	private boolean setData(HSSFSheet workSheet, S34HyoukaNinzuListBean s34HyoukaNinzuListBean, int intRow, int intCol ){

		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		int		row = intRow;
		
		
		// 基本ファイルを読込む
		try{
			
			//模試年度
			if( s34HyoukaNinzuListBean.getStrNendo()!=null ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, 2);
				workCell.setCellValue( s34HyoukaNinzuListBean.getStrNendo() + "年度" );
			}
			
			//全国総志望者数
			if( s34HyoukaNinzuListBean.getIntSoushibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol);
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntSoushibo() );
			}
			
			//志望者数（第一志望）
			if( s34HyoukaNinzuListBean.getIntDai1shibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 1);
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntDai1shibo() );
			}
			
			//評価別人数
			if( s34HyoukaNinzuListBean.getIntHyoukaA() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 2 );
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntHyoukaA() );
			}

			if( s34HyoukaNinzuListBean.getIntHyoukaB() != -999 ){			
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntHyoukaB() );
			}
			
			if( s34HyoukaNinzuListBean.getIntHyoukaC() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntHyoukaC() );
			}

		}
		catch(Exception e){
			return false;
		}

		return true;
	}
	
}
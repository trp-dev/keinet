//package kn.applet;

// 2016/01/15 QQ)Hisakawa 大規模改修 ADD START
package jp.co.fj.keinavi.beans.graph;
// 2016/01/15 QQ)Hisakawa 大規模改修 ADD END

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


/**
 * @author Yoshimoto KAWAI - Totec
 * 説明：個人成績分析 I107用アプレット
 * 作成日：2004/05/24
 * 変更：2004/07/07　ishiguro　横目盛数７に固定、フォント変更
 * 変更：2004/07/08　ishiguro　右上描画追加、折れ線グラフ点線に変更、棒グラフ追加
 * 変更：2004/08/24　ishiguro　空データをプロットしない処理を追加
 * 2005.4.7		T.Yamada	[4]折れ線グラフが描画スペースをはみ出す時の対処
 * 2016.1.15	QQ)Hisakawa	サーブレット化対応の為、 src/main/webapp/applets から移動
 */

//2016/02/26 QQ)Hisakawa 大規模改修 UPD START
//Linuxでアプレットクラスが処理出来ない
//public class LineGraphApplet2 extends Applet {
public class LineGraphApplet2 {
//2016/02/26 QQ)Hisakawa 大規模改修 UPD END

	int xUnit = 100;//よこ目盛幅
	int yUnit = 50;//たて目盛幅（init()にて再設定）※なぜかここで値が入っていないと正しく表示されません
	int yIndexNum;//Ｙ軸の目盛数（init()にて設定）
	int gTop = 20;//Ｙ軸の最上位置
	int gLeft = 50;//Ｘ軸の最左位置
	int gBottom = gTop + yUnit * 5;//Ｙ軸の最下位置
	int gRight = gLeft + xUnit * 7;//Ｘ軸の最右位置
	int yIncr=20;//y増分（右上欄用）
	int BarSize=10;//棒グラフのサイズ（右上欄用）（init()にて変更）
	int gap = 30;//Ｙ軸から1つ目の目盛までの距離
	int subLine = 5;//補助軸の長さ
	int LineItemNum = 0;//初期化
	int BarItemNum = 0;//初期化
	int HensaZoneLow = 30;//初期化
	int HensaZoneHigh = 70;//初期化
	int colorDat[];//色変更用
	String[] univValue;//大学偏差値（棒グラフ表示用）
	String  dispSelect="on";//グラフ表示、非表示の選択フラグ
	String   yTitle="";//Ｙ軸タイトル
	String[] itemTitle;//右上タイトル
	String[] LineItemNameS;//折れ線グラフ名
	String[] LineItemName;//折れ線グラフ名
	String[] examName;//模試名（Ｘ軸に表示する）
	String[] examValue;//折れ線描画のための値（一時使用）
	String[] examValue0;//折れ線グラフ描画のための値
	String[] examValue1;//折れ線グラフ描画のための値
	String[] examValue2;//折れ線グラフ描画のための値
//	boolean	itemState[];

	// 2016/01/15 QQ)Hisakawa 大規模改修 ADD START
    String sExamName = "";
    String sDispSelect = "";
    String sYTitle = "";
    String sHensaZoneLow = "";
    String sHensaZoneHigh = "";
    String sBarItemNum = "";
    String sLineItemNum = "";
    String sLineItemName = "";
    String sLineItemNameS = "";
    String sExamValue0 = "";
    String sExamValue1 = "";
    String sExamValue2 = "";
    String sUnivValue = "";
    String sItemTitle = "";
    String sColorDAT = "";
    String sBarSize = "";
    // 2016/01/15 QQ)Hisakawa 大規模改修 ADD END

	//色の作成
	Color lightBG = new Color(0.5F, 0.8F, 0.8F);//薄い青緑
	Color whiteGray = new Color(0.95F, 0.95F, 0.9F);//薄い灰色
	Color purple = new Color(0.6F, 0.2F, 0.8F);//紫
	Color darkGreen = new Color(0.5F, 0.6F, 0.2F);//濃い青緑
	Color lightBlue = new Color(0.5F, 0.6F, 0.7F);//青緑

	Color	fgColor = Color.black,//色定義
		bgColor = Color.white,
		med_lineColor = Color.lightGray,
		LineColor = Color.pink,
		colors[] = {
				Color.blue,
				Color.red,
				Color.green,
				Color.magenta,
				Color.orange,
				Color.cyan,
				Color.pink,
				Color.yellow,
				Color.white,
				Color.lightGray,
				Color.gray,
				Color.darkGray,
				Color.black,
				lightBlue,
				whiteGray,
				purple,
				lightBG,
				darkGreen
			};

	//折れ線の点線設定
	final static float dash0[] = {2.0f, 2.0f};
	BasicStroke dashed = new BasicStroke(1.0f,BasicStroke.CAP_BUTT,
										BasicStroke.JOIN_MITER,10.0f, dash0, 0.0f);
	BasicStroke dashed0 = new BasicStroke(2.0f,BasicStroke.CAP_BUTT,
										BasicStroke.JOIN_MITER,10.0f, dash0, 0.0f);
	final static float dash1[] = {20.0f, 1.0f};
	BasicStroke dashed1 = new BasicStroke(2.0f,BasicStroke.CAP_BUTT,
										BasicStroke.JOIN_MITER,10.0f, dash1, 0.0f);
	final static float dash2[] = {8.0f, 1.0f, 2.0f, 1.0f};
	BasicStroke dashed2 = new BasicStroke(2.0f,BasicStroke.CAP_BUTT,
										BasicStroke.JOIN_MITER,10.0f, dash2, 0.0f);
	final static float dash3[] = {6.0f, 2.0f, 5.0f, 1.0f};
	BasicStroke dashed3 = new BasicStroke(2.0f,BasicStroke.CAP_BUTT,
										BasicStroke.JOIN_MITER,10.0f, dash3, 0.0f);
	final static float dash4[] = {3.0f, 2.0f, 4.0f, 2.0f};
	BasicStroke dashed4 = new BasicStroke(2.0f,BasicStroke.CAP_BUTT,
										BasicStroke.JOIN_MITER,10.0f, dash4, 0.0f);

	//アプレット起動時読み込み
	public void init(){
		// 2016/02/26 QQ)Hisakawa 大規模改修 DEL START
        // アプレットの継承廃止により削除
		//this.setBackground(new Color(250, 250, 250));
		// 2016/02/26 QQ)Hisakawa 大規模改修 DEL END
		dispSelect = getParameter("dispSelect");
		yTitle = getParameter("yTitle");
		HensaZoneLow = Integer.parseInt(getParameter("HensaZoneLow"));
		HensaZoneHigh = Integer.parseInt(getParameter("HensaZoneHigh"));
		examName = splitComma(getParameter("examName"));
		LineItemName = splitComma(getParameter("LineItemName"));
		LineItemNameS = splitComma(getParameter("LineItemNameS"));
		LineItemNum = Integer.parseInt(getParameter("LineItemNum"));
		BarItemNum = Integer.parseInt(getParameter("BarItemNum"));
		itemTitle	= splitComma(getParameter("itemTitle"));//アイテムタイトル配列取得
		if( itemTitle.length == 0 ){
			itemTitle = new String[2];
			itemTitle[0] = "□パラメータ名itemTitle";
			itemTitle[1] = "□パラメータ名itemTitleカンマ区切り";
		}
		if(getParameter("colorDAT")!=null){
			colorDat	= CommatextToInt(getParameter("colorDAT"));
		}else{
			colorDat	= new int[10];
			for(int ci=0;ci<10;ci++)	colorDat[ci]=ci;
		}
		examValue0 = splitComma(getParameter("examValue0"));
		examValue1 = splitComma(getParameter("examValue1"));
		examValue2 = splitComma(getParameter("examValue2"));

		univValue = splitComma(getParameter("univValue"));
		if(getParameter("BarSize")!=null)
			BarSize = Integer.parseInt(getParameter("BarSize"));
		yIndexNum = (HensaZoneHigh - HensaZoneLow)/5 + 1;//Y目盛数
		yUnit=(gBottom - gTop) / yIndexNum;//Y目盛幅を目盛数にあわせて変更
	}

	public void change(String str) {
		examValue = splitComma(str);
		// 2016/02/26 QQ)Hisakawa 大規模改修 DEL START
        // アプレットの継承廃止により削除
		//repaint();
		// 2016/02/26 QQ)Hisakawa 大規模改修 DEL END
	}

	//描画メイン
	public void paint(Graphics g){
		g.setColor(fgColor);
		if(dispSelect.equals("off")){
			drawStrNoDisp(g);
		}else{
			drawLine(g);
			//drawLabel(g);//[4] del
			g.setFont(new Font("Dialog",Font.PLAIN,10));//[4] add
			drawUnderRect(g);
			drawLineTitle(g);
			for(int lineNum=0;lineNum<BarItemNum;lineNum++){//棒グラフ描画
				g.setColor(colors[colorDat[lineNum+3]]);
				drawBarGraph(g,lineNum);
				drawBarName(g,lineNum);
			}
			for(int lineNum=LineItemNum-1;lineNum>=0;lineNum--){//折れ線描画
				g.setColor(colors[colorDat[lineNum]]);
				drawPlot(g,lineNum);
				drawLineGraph(g,lineNum);
				drawLineName(g,lineNum);
			}
//			itemState = new boolean[BarItemNum];
			fillBottom(g);//[4] add
			drawLabel(g);//[4] add
		}
	}

	// チラツキ防止
//	public void update(Graphics g){
//	   paint(g);
//	}

	//カンマでデータを分割して配列に収納
	//T.Yamada 変更：カンマの間にデータがなければ、0.0を入れる
	private static String[] splitComma(String str){
		String DUMMY = "#";
		if (str == null) return new String[0];
		StringTokenizer tk = new StringTokenizer(str, ",", true);
		List list = new ArrayList();
		String preToken = DUMMY;
		while(tk.hasMoreTokens()){
			String temp = tk.nextToken();
			if((preToken.trim().equals(DUMMY) && temp.trim().equals(",")) || (temp.trim().equals(",") && preToken.trim().equals(",")) || (temp.trim().equals(",") && !tk.hasMoreTokens())){
				list.add("-999.0f");
			}else if(!temp.trim().equals(",")){
				list.add(temp);
			}
			preToken = temp;
		}
		return (String[])list.toArray(new String[0]);
	}
	/**
	 * [4] add
	 * Ｘ軸補助線以下のスペースをバックグラウンドカラーで
	 * 上塗りする。
	 */
	private void fillBottom(Graphics g){
		//色をバックグランドと同じに設定。
		g.setColor(new Color(250, 250, 250));
		//この座標の四角形を上記の色で塗りつぶす。
		//線まで塗りつぶさないように、四角形の座標を1ずらす
		g.fillRect(gLeft, gBottom+1, gRight, gBottom);
	}
	private void drawLine(Graphics g) {
		// X軸
		g.drawLine(gLeft, gBottom, gRight, gBottom);
		// Y軸
		g.drawLine(gLeft, gBottom, gLeft, gTop);
		// X軸の補助軸
		for (int i=0; i<examName.length; i++) {
			g.drawLine(gLeft + xUnit * i + gap, gBottom, gLeft + xUnit * i + gap, gBottom - subLine);
		}
		// Y軸の補助軸
		g.setColor(med_lineColor);
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(dashed);
		for (int i=0; i<yIndexNum; i++) {
//			g.drawLine(gLeft, gBottom - yUnit * (i+1), gLeft + subLine, gBottom - yUnit * (i+1));
			g2.drawLine(gLeft+1, gBottom - yUnit * (i+1), gLeft + xUnit*6 + gap, gBottom - yUnit * (i+1));
		}
	}
	//XY座標文字描画
	private void drawLabel(Graphics g) {
		g.setColor(fgColor);
		FontMetrics fm = g.getFontMetrics();
		// Y軸（偏差値）
		for (int i=0; i<yIndexNum; i++) {
			//String label = Double.toString((i + 2) * 10);
			String label = Integer.toString( HensaZoneLow + 5*i ) + ".0";
			g.drawString(label,
				gLeft - fm.stringWidth(label),
				gBottom - yUnit * (i+1) + fm.getHeight() / 3);
			if(i==yIndexNum-1){
				g.drawString(yTitle,
					gLeft - fm.stringWidth(yTitle),
					gBottom - yUnit * (i+1) - fm.getHeight()/2);
			}
		}
		// X軸（模試名）
		g.setFont(new Font("Dialog",Font.PLAIN,10));//フォント設定
		for (int i=0; i<examName.length; i++) {
			g.drawString(examName[i],
				gLeft - 2 + xUnit * i + gap - fm.getWidths()[0] / 2,
				gBottom + fm.getHeight());
		}
	}

	//棒グラフ描画: 値"0"のところは描画しない。
	private void drawBarGraph(Graphics g,int NUM){
//		int BarSize=5;//棒グラフ幅
		g.setColor(colors[colorDat[NUM+3]]);
		g.fillRect(gLeft + xUnit * (examName.length-1) + gap - BarSize/2 + BarSize*NUM,
				gBottom - yUnit - yUnit*((int)(Double.parseDouble(univValue[NUM]))-HensaZoneLow)/5 ,
				BarSize, yUnit + yUnit*((int)(Double.parseDouble(univValue[NUM]))-HensaZoneLow)/5);
	}

	//プロット。値”０”のところはプロットしない
	private void drawPlot(Graphics g,int NUM) {
		g.setColor(colors[colorDat[NUM]]);
		if(NUM==0) {examValue=examValue0;}
		if(NUM==1) {examValue=examValue1;}
		if(NUM==2) {examValue=examValue2;}

		int plotSize=6;//ここで点の大きさを指定する
		for (int i=0; i<examValue.length; i++) {
			if(Float.parseFloat(examValue[i])!=-999.0f){
				g.fillRect(gLeft + xUnit * i + gap - plotSize/2,
				gBottom - yUnit - yUnit*((int)(Double.parseDouble(examValue[i]))-HensaZoneLow)/5 - plotSize/2,
				plotSize, plotSize);
			}
		}
	}

	//折れ線描画。値"0"のところは描画しない。
	private void drawLineGraph(Graphics g,int NUM) {
		g.setColor(colors[colorDat[NUM]]);
		Graphics2D g2 = (Graphics2D) g;
		if(NUM==0) {examValue=examValue0;dashed=dashed0;}
		if(NUM==1) {examValue=examValue1;dashed=dashed1;}
		if(NUM==2) {examValue=examValue2;dashed=dashed2;}

		g2.setStroke(dashed);
		for (int i=0; i<examValue.length - 1; i++) {//次のプロットと線を引くため-1としている。
	//		if(i==0){
				while(Float.parseFloat(examValue[i])==-999.0f){//始点が見つかるまでまわす
					i++;
					if(i==examValue.length-1){break;}//始点がみつからない場合に対応
				}

//			}
			if(i==examValue.length-1){break;}//始点が終点だった場合に対応
			boolean lastPlotFlg = true;//最後の点の有無フラグ（true:有、false:無）
			if(Float.parseFloat(examValue[i])==-999.0f){//値に999が入っていた場合はプロットしない
				int startPoint = i;//始点の保存
				while(Float.parseFloat(examValue[i+1])==-999.0f){//無効値が連続する場合に対応
					if( i==(examValue.length - 2) ){
						lastPlotFlg = false;//false : 最後の点が無いため線を引かない
						break;
					}
					i++;
					System.out.println("NULL");
				}
				if( lastPlotFlg ){
					g.drawLine(gLeft + xUnit * startPoint + gap,
					gBottom - yUnit - yUnit * ((int)(Double.parseDouble(examValue[startPoint]))-HensaZoneLow)/5,
					gLeft + xUnit * (i + 1) + gap,
					gBottom - yUnit - yUnit*((int)(Double.parseDouble(examValue[i+1]))-HensaZoneLow)/5 );
				}
			}else{//有効値ならプロットするが、次の有効値が無ければ線は引かない
				if(Float.parseFloat(examValue[i+1])!=-999.0f){//次の値が無効値なら線を引かない
					g.drawLine(gLeft + xUnit * i + gap,
					gBottom - yUnit - yUnit*((int)(Double.parseDouble(examValue[i]))-HensaZoneLow)/5 ,
					gLeft + xUnit * (i + 1) + gap,
					gBottom - yUnit - yUnit*((int)(Double.parseDouble(examValue[i+1]))-HensaZoneLow)/5  );
				}else{//次の値が無効値なので、有効値が見つかるまでループ
					int startPoint = i;//始点の保存
					while(Float.parseFloat(examValue[i+1])==-999.0f){
						if( i==(examValue.length - 2) ){
							lastPlotFlg = false;//false : 最後の点が無いため線を引かない
							break;
						}
						i++;
					}
					if( lastPlotFlg ){//点が見つかったので線を引く
						g.drawLine(gLeft + xUnit * startPoint + gap,
						gBottom - yUnit - yUnit * ((int)(Double.parseDouble(examValue[startPoint]))-HensaZoneLow)/5,
						gLeft + xUnit * (i + 1) + gap,
						gBottom - yUnit - yUnit*((int)(Double.parseDouble(examValue[i+1]))-HensaZoneLow)/5 );
					}
				}
			}
		}

	}


	//右上欄の下地描画
	private void drawUnderRect(Graphics g){
		g.setColor(colors[colorDat[7]]);
		g.fillRect(gLeft + 100 * 7 - 40, yIncr + yIncr/4 - 5 , 130, yIncr * 12);
		g.setColor(colors[colorDat[6]]);
		g.fillRect(gLeft + 100 * 7 - 40, yIncr + yIncr/4 - 5 , 130, yIncr);
		g.setColor(colors[colorDat[6]]);
		g.fillRect(gLeft + 100 * 7 - 40, yIncr*5 + yIncr/4 - 5 , 130, yIncr);

	}

	//右上タイトル名描画
	private void drawLineTitle(Graphics g){
		g.setColor(bgColor);
		g.drawString(itemTitle[0],
					gLeft + 100 * 7 + gap -65, 1 * yIncr + yIncr/4 + 10);
		g.drawString(itemTitle[1],
					gLeft + 100 * 7 + gap -65, 5 * yIncr + yIncr/4 + 10);
	}
	//右上アイテム名描画(折れ線グラフ部)
	private void drawLineName(Graphics g, int num){
		Color dc = g.getColor();
		g.setColor(dc);
		g.drawLine(gLeft + 100 * 7 + gap -55, (num+2) * yIncr + 10,
				gLeft + 100 * 7 + gap - 35, (num+2) * yIncr + 10);
		g.setColor(fgColor);
		g.drawString(LineItemNameS[num],
				gLeft + 100 * 7 + gap - 25, (num+2) * yIncr + yIncr/4 + 10);
	}

	//右上アイテム名描画(棒グラフ部)
	private void drawBarName(Graphics g, int num){
		int[] BarLen = {22,10};//右上棒グラフ（横長さ、縦長さ）：お好みで調整してください。
		Color dc = g.getColor();
		//
		g.setColor(dc);
		g.fillRect(gLeft + 100 * 7 + gap -55, 6 * yIncr + (num * (yIncr + 20) + 10) - BarLen[1]/3,
					BarLen[0], BarLen[1]);
		g.setColor(fgColor);
		String name1 = LineItemName[num].substring(0, LineItemName[num].indexOf("  "));
		String name2 = LineItemName[num].substring(LineItemName[num].indexOf("  "), LineItemName[num].lastIndexOf("  "));
		String name3 = LineItemName[num].substring(LineItemName[num].lastIndexOf("  "), LineItemName[num].length());
		g.drawString(name1,
					gLeft + 100 * 7 + gap -25, 6 * yIncr + (num * (yIncr + 20) + 15));
		g.drawString(name2,
					gLeft + 100 * 7 + gap -25, 6 * yIncr + (num * (yIncr + 20) + 25));
		g.drawString(name3,
					gLeft + 100 * 7 + gap -25, 6 * yIncr + (num * (yIncr + 20) + 35));
		//g.drawString(LineItemName[num],	gLeft, 10);
	}

	private int[] CommatextToInt(String commatext){
		StringTokenizer tk = new StringTokenizer(commatext, ",");
		int		intN = tk.countTokens();
		int[]	tmp = new int[intN];
		//
		for(int ii = 0; ii < intN; ii++){
			Integer dd = new Integer(tk.nextToken());
			tmp[ii] = dd.intValue();
		}
		return tmp;
	}
	//表示データがないとき実行される
	private void drawStrNoDisp(Graphics g){
		g.setColor(Color.lightGray);
		g.setFont(new Font("Dialog",Font.PLAIN,20));//フォント設定
		g.drawString("表示するデータがありません",gLeft + gap, yIncr*3);
	}

	// 2016/01/15 QQ)Hisakawa 大規模改修 ADD START
    public void setParameter(String str, String strParam) {
        if(str == null) {
        }else if("examName".equals(str)){
        	sExamName = strParam;
        }else if("dispSelect".equals(str)){
        	sDispSelect = strParam;
        }else if("yTitle".equals(str)){
        	sYTitle = strParam;
        }else if("HensaZoneLow".equals(str)){
        	sHensaZoneLow = strParam;
        }else if("HensaZoneHigh".equals(str)){
        	sHensaZoneHigh = strParam;
        }else if("BarItemNum".equals(str)){
        	sBarItemNum = strParam;
        }else if("LineItemNum".equals(str)){
        	sLineItemNum = strParam;
        }else if("LineItemName".equals(str)){
        	sLineItemName = strParam;
        }else if("LineItemNameS".equals(str)){
        	sLineItemNameS = strParam;
        }else if("examValue0".equals(str)){
        	sExamValue0 = strParam;
        }else if("examValue1".equals(str)){
        	sExamValue1 = strParam;
        }else if("examValue2".equals(str)){
        	sExamValue2 = strParam;
        }else if("univValue".equals(str)){
        	sUnivValue = strParam;
        }else if("itemTitle".equals(str)){
        	sItemTitle = strParam;
        }else if("colorDAT".equals(str)){
        	sColorDAT = strParam;
        }else if("BarSize".equals(str)){
        	sBarSize = strParam;
        }
    }
    public String getParameter(String str) {
        String retStr = "";
        if(str == null) {
            retStr = "";
        }else if("dispSelect".equals(str)){
            retStr = sDispSelect;
        }else if("HensaZoneLow".equals(str)){
            retStr = sHensaZoneLow;
        }else if("HensaZoneHigh".equals(str)){
            retStr = sHensaZoneHigh;
        }else if("LineItemNum".equals(str)){
            retStr = sLineItemNum;
        }else if("LineItemName".equals(str)){
            retStr = sLineItemName;
        }else if("LineItemNameS".equals(str)){
            retStr = sLineItemNameS;
        }else if("yTitle".equals(str)){
            retStr = sYTitle;
        }else if("colorDAT".equals(str)){
            retStr = sColorDAT;
        }else if("examName".equals(str)){
            retStr = sExamName;
        }else if("examValue0".equals(str)){
            retStr = sExamValue0;
        }else if("examValue1".equals(str)){
            retStr = sExamValue1;
        }else if("examValue2".equals(str)){
            retStr = sExamValue2;
        }else if("BarSize".equals(str)){
            retStr = sBarSize;
        }else if("BarItemNum".equals(str)){
            retStr = sBarItemNum;
        }else if("univValue".equals(str)){
            retStr = sUnivValue;
        }else if("itemTitle".equals(str)){
            retStr = sItemTitle;
        }else{
            retStr = "";
        }

        return retStr;
    }
	// 2016/01/15 QQ)Hisakawa 大規模改修 ADD END
}

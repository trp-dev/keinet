package jp.co.fj.keinavi.excel.data.school;

//import org.apache.struts.action.ActionForm;
import java.util.ArrayList;
/**
 * �Z�����ѕ��́|���Z��r�|�΍��l���z �f�[�^���X�g
 * �쐬��: 2004/07/19
 * @author	A.Iwata
 */
public class S52ListBean {
	//�^�E�Ȗږ��R�[�h
	private String strKmkCd = "";
	//�^�E�Ȗږ�
	private String strKmkmei = "";
	//�^��Ȗڗp�O���t�\���t���O
	private int intDispKmkFlg = 0;
	//�͎���
	private String strMshmei = "";
	//�͎����{���
	private String strMshDate = "";
	//�z�_
	private String strHaitenKmk = "";
	//���v�l��
	private int intNinzu = 0;
	//���ϓ_
	private float floHeikin = 0;
	//���ϕ΍��l
	private float floHensa = 0;
	//�΍��l���z���X�g
	private ArrayList s52BnpList = new ArrayList();

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}
	public int getIntDispKmkFlg() {
		return this.intDispKmkFlg;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public float getFloHeikin() {
		return this.floHeikin;
	}
	public float getFloHensa() {
		return this.floHensa;
	}
	public ArrayList getS52BnpList() {
		return this.s52BnpList;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setIntDispKmkFlg(int intDispKmkFlg) {
		this.intDispKmkFlg = intDispKmkFlg;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setFloHeikin(float floHeikin) {
		this.floHeikin = floHeikin;
	}
	public void setFloHensa(float floHensa) {
		this.floHensa = floHensa;
	}
	public void setS52BnpList(ArrayList s52BnpList) {
		this.s52BnpList = s52BnpList;
	}
}

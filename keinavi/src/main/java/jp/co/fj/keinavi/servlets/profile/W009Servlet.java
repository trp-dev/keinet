package jp.co.fj.keinavi.servlets.profile;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.profile.ProfileFolderBean;
import jp.co.fj.keinavi.beans.profile.ProfileFolderUpdateBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.ProfileFolder;
import jp.co.fj.keinavi.forms.profile.W009Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.servlets.KNServletException;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class W009Servlet extends DefaultHttpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// アクションフォーム
		final W009Form form = (W009Form) getActionForm(request,
				"jp.co.fj.keinavi.forms.profile.W009Form");
		// ログイン情報
		final LoginSession login = getLoginSession(request);

		// 「登録して閉じる」
		if ("w009".equals(getBackward(request))) {
			Connection con = null; // DBコネクション
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(false);

				// プロファイルフォルダUpdateBean
				final ProfileFolderUpdateBean bean = new ProfileFolderUpdateBean();
				bean.setConnection(null, con);
				bean.setFolderID(form.getFolderID());
				bean.setFolderName(form.getFolderName());
				bean.setComment(form.getComment());
				bean.setLoginSession(login);
				bean.setBundleCD(form.getCountingDivCode());
				bean.execute();
				
				//	アクセスログ
				actionLog(request, "211", bean.getFolderID());
				
				con.commit();
				
			} catch (final KNServletException e) {
				rollback(con);
				throw e;
			} catch (final Exception e) {
				rollback(con);
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			forward(request, response, JSP_CLOSE);
		
		// 通常アクセス
		} else {
			Connection con = null; // DBコネクション
			try {
				con = getConnectionPool(request);

				// プロファイルフォルダBean
				final ProfileFolderBean bean = new ProfileFolderBean();
				bean.setConnection(null, con);
				bean.setLoginSession(login);
				bean.setBundleCD(form.getCountingDivCode());
				bean.execute();
				request.setAttribute("ProfileFolderBean", bean);

				// 編集対象となるフォルダ
				final ProfileFolder folder;
				int index = bean.getFolderList().indexOf(
						new ProfileFolder(form.getFolderID()));
				if (index >= 0) {
					folder = (ProfileFolder) bean.getFolderList().get(index);
				// なければ新規
				} else {
					folder = new ProfileFolder();
					folder.setFolderName("新規フォルダ");
					folder.setCreateDate(new Date());
					folder.setUpdateDate(folder.getCreateDate());
				}

				request.setAttribute("folder", folder);

			} catch (final Exception e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			request.setAttribute("form", form);

			forward(request, response, JSP_W009);
		}
	}

}

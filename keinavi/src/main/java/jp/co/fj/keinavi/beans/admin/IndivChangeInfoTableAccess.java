package jp.co.fj.keinavi.beans.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 変更成績情報テーブルアクセス
 * 
 * <2011年度答案閲覧システム連携>
 * 2011.08.29   Tomohisa Yamada - TOTEC
 * 				新規作成
 *
 */
public class IndivChangeInfoTableAccess {
	
	/**
	 * 個表データINSERT
	 * @param conn DBコネクション
	 * @param examYear 模試年度
	 * @param examCd 模試コード
	 * @param answerSheetNo 解答用紙番号
	 * @throws SQLException
	 */
	public static void updatePrimary(Connection conn, String examYear, String examCd, String answerSheetNo) throws SQLException {
		deletePrimary(conn, examYear, examCd, answerSheetNo);
		insertPrimary(conn, examYear, examCd, answerSheetNo);
	}
	
	/**
	 * 主従結合
	 * @param conn DBコネクション
	 * @param individualIdMain 主個人ID
	 * @param individualIdSub 従個人ID（変更前）
	 * @throws SQLException
	 */
	public static void integrateIndividual(Connection conn, String individualIdMain, String individualIdSub) throws SQLException {
		deleteIndividual(conn, individualIdSub);
		insertIndividual(conn, individualIdMain, individualIdSub);
	}
	
	/**
	 * 個表データINSERT
	 * @param conn DBコネクション
	 * @param individualId 個人ID
	 * @throws SQLException
	 */
	public static void updateIndividual(Connection conn, String individualId) throws SQLException {
		deleteIndividual(conn, individualId);
		insertIndividual(conn, individualId, individualId);
	}
	
	/**
	 * 連携テーブルDELETE
	 * @param conn DBコネクション
	 * @param individualId 個人ID
	 * @throws SQLException
	 */
	private static void deleteIndividual(Connection conn, String individualId) throws SQLException {
		deleteIndividual(conn, null, null, individualId);
	}

	
	/**
	 * 連携テーブルINSERT
	 * @param conn DBコネクション
	 * @param individualIdMain 主生徒の個人ID
	 * @param individualIdSub 従生徒の個人ID
	 * @throws SQLException
	 */
	private static void insertIndividual(Connection conn, String individualIdMain, String individualIdSub) throws SQLException {
		insertIndividual(conn, null, null, individualIdMain, individualIdSub);
	}
	
	/**
	 * 連携テーブルDELETE
	 * @param conn DBコネクション
	 * @param year 模試年度
	 * @param examCd 模試コード
	 * @param answerSheetNo 解答用紙番号
	 * @throws SQLException
	 */
	private static void deletePrimary(Connection conn, String year, String examCd, String answerSheetNo) throws SQLException {

		PreparedStatement ps = null;
		try {
			
			StringBuffer query = new StringBuffer();
			query.append(" DELETE FROM ");
			query.append("	indiv_change_info");
			query.append(" WHERE");
			query.append("	examyear = ?");
			query.append(" AND");
			query.append("	examcd = ?");
			query.append(" AND");
			query.append("	answersheet_no = ?");
			
			ps = conn.prepareStatement(query.toString());

			ps.setString(1, year);
			ps.setString(2, examCd);
			ps.setString(3, answerSheetNo);

			ps.executeUpdate();

		} catch (Exception e) {
			throw new SQLException(e.getMessage());
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	/**
	 * 連携テーブルDELETE
	 * @param conn DBコネクション
	 * @param examYear 模試年度
	 * @param examCd 模試コード
	 * @param individualId 個人ID
	 * @throws SQLException
	 */
	private static void deleteIndividual(Connection conn, String examYear, String examCd, String individualId) throws SQLException {

		PreparedStatement ps = null;

		try {
			
			StringBuffer query = new StringBuffer();
			query.append(" DELETE FROM ");
			query.append("	indiv_change_info");
			query.append(" WHERE");
			query.append("	s_individualid = ?");
			
			if (examYear != null) {
				query.append(" AND ");
				query.append(" examyear = ? ");
			}
			
			if (examCd != null) {
				query.append(" AND ");
				query.append(" examcd = ? ");
			}

			ps = conn.prepareStatement(query.toString());

			int index = 1;
			
			ps.setString(index++, individualId);
			
			if (examYear != null) {
				ps.setString(index++, examYear);
			}
			
			if (examCd != null) {
				ps.setString(index++, examCd);
			}

			ps.executeUpdate();

		} catch (Exception e) {
			throw new SQLException(e.getMessage());
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	/**
	 * 連携テーブルINSERT
	 * @param conn DBコネクション
	 * @param examYear 模試年度
	 * @param examCd 模試コード
	 * @param answerSheetNo 解答用紙番号
	 * @throws SQLException
	 */
	private static void insertPrimary(Connection conn, String examYear, String examCd, String answerSheetNo) throws SQLException {
		
		PreparedStatement ps = null;
		try {
						
			StringBuffer query = new StringBuffer();
			query.append(" INSERT INTO");
			query.append(" 	indiv_change_info ");
			query.append(" SELECT ");
			query.append(" 	 ir.examyear examyear ");
			query.append(" 	,ir.examcd examcd ");
			query.append(" 	,ir.answersheet_no answersheet_no ");
			query.append(" 	,ir.customer_no customer_no ");
			query.append(" 	,ir.applidate applidate ");
			query.append(" 	,ir.bundlecd bundlecd ");
			query.append(" 	,h.grade grade ");
			query.append(" 	,h.class class ");
			query.append(" 	,h.class_no class_no ");
			query.append(" 	,b.name_kana name_kana ");
			query.append(" 	,b.sex sex ");
			query.append(" 	,b.tel_no tel_no ");
			query.append(" 	,b.birthday birthday ");
			query.append(" 	,b.individualid s_individualid ");
			query.append(" 	,SYSTIMESTAMP createdate ");
			query.append(" FROM ");
			query.append(" 	individualrecord ir ");
			query.append(" INNER JOIN");
			query.append(" 	basicinfo b ");
			query.append(" ON ");
			query.append(" 	b.individualid = ir.s_individualid ");
			query.append(" INNER JOIN ");
			query.append(" 	historyinfo h ");
			query.append(" ON ");
			query.append(" 	h.individualid = b.individualid ");
			query.append(" AND ");
			query.append(" 	h.year = ir.examyear ");
			query.append(" WHERE ");
			query.append(" 	ir.examyear = ? ");
			query.append(" AND ");
			query.append(" 	ir.examcd = ? ");
			query.append(" AND ");
			query.append(" 	ir.answersheet_no = ? ");
			
			ps = conn.prepareStatement(query.toString());
			
			ps.setString(1, examYear);
			ps.setString(2, examCd);
			ps.setString(3, answerSheetNo);

			if (ps.executeUpdate() == 0)
				new SQLException("連携テーブルのINSERTに失敗しました。");

		} catch (Exception e) {
			throw new SQLException(e.getMessage());
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	/**
	 * 連携テーブルINSERT
	 * @param conn DBコネクション
	 * @param examYear 模試年度
	 * @param examCd 模試コード
	 * @param individualIdMain 主生徒の個人ID
	 * @param individualIdSub 従生徒の個人ID
	 * @throws SQLException
	 */
	private static void insertIndividual(Connection conn, String examYear, String examCd, String individualIdMain, String individualIdSub) throws SQLException {
		
		PreparedStatement ps = null;
		try {
			
			StringBuffer query = new StringBuffer();
			query.append(" INSERT INTO");
			query.append(" 	indiv_change_info ");
			query.append(" SELECT ");
			query.append(" 	 ir.examyear examyear ");
			query.append(" 	,ir.examcd examcd ");
			query.append(" 	,ir.answersheet_no answersheet_no ");
			query.append(" 	,ir.customer_no customer_no ");
			query.append(" 	,ir.applidate applidate ");
			query.append(" 	,ir.bundlecd bundlecd ");
			query.append(" 	,h.grade grade ");
			query.append(" 	,h.class class ");
			query.append(" 	,h.class_no class_no ");
			query.append(" 	,b.name_kana name_kana ");
			query.append(" 	,b.sex sex ");
			query.append(" 	,b.tel_no tel_no ");
			query.append(" 	,b.birthday birthday ");
			query.append(" 	,b.individualid s_individualid ");
			query.append(" 	,SYSTIMESTAMP createdate ");
			query.append(" FROM ");
			query.append(" 	individualrecord ir ");
			query.append(" INNER JOIN");
			query.append(" 	basicinfo b ");
			query.append(" ON ");
			query.append(" 	b.individualid = ? ");
			query.append(" INNER JOIN ");
			query.append(" 	historyinfo h ");
			query.append(" ON ");
			query.append(" 	h.individualid = b.individualid ");
			query.append(" AND ");
			query.append(" 	h.year = ir.examyear ");
			query.append(" WHERE ");
			query.append(" 	ir.s_individualid = ? ");
			
			if (examYear != null) {
				query.append(" AND ");
				query.append(" ir.examyear = ? ");
			}
			
			if (examCd != null) {
				query.append(" AND ");
				query.append(" ir.examcd = ? ");
			}

			ps = conn.prepareStatement(query.toString());
			
			int index = 1;
			
			ps.setString(index++, individualIdMain);
			ps.setString(index++, individualIdSub);
			
			if (examYear != null) {
				ps.setString(index++, examYear);
			}
			
			if (examCd != null) {
				ps.setString(index++, examCd);
			}

			if (ps.executeUpdate() == 0) {
				new SQLException("連携テーブルのINSERTに失敗しました。");
			}

		} catch (Exception e) {
			throw new SQLException(e.getMessage());
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

}

/*
 * 作成日: 2004/08/19
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class UnivData implements Comparable{
	
	private String univCd;
	private String facultyCd;
	private String deptCd;
	private String univNameAbbr;
	private String facultyNameAbbr;
	private String deptNameAbbr;
	private String superAbbrName;
	private String deptSortKey;
	
	/* (非 Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 * 大学・学部・学科ソートキー
	 */
	public int compareTo(Object o2) {
		UnivData u2 = (UnivData)o2;
		int univCd1 = Integer.parseInt(this.univCd);
		int univCd2 = Integer.parseInt(u2.getUnivCd());
		if(univCd1 < univCd2){
			return -1;
		}else if(univCd1 > univCd2){
			return 1;
		}else{
			int facultyCd1 = Integer.parseInt(this.facultyCd);
			int facultyCd2 = Integer.parseInt(u2.getFacultyCd());
			if(facultyCd1 < facultyCd2){
				return -1;
			}else if(facultyCd1 > facultyCd2){
				return 1;
			}else{
				if(deptSortKey.compareTo(u2.getDeptSortKey()) > 0){
					return 1;	
				}else if(deptSortKey.compareTo(u2.getDeptSortKey()) < 0){
					return -1;
				}else{
					return 0;
				}
			}
		}
	}
	
	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getDeptNameAbbr() {
		return deptNameAbbr;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getFacultyNameAbbr() {
		return facultyNameAbbr;
	}

	/**
	 * @return
	 */
	public String getSuperAbbrName() {
		return superAbbrName;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @return
	 */
	public String getUnivNameAbbr() {
		return univNameAbbr;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setDeptNameAbbr(String string) {
		deptNameAbbr = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyNameAbbr(String string) {
		facultyNameAbbr = string;
	}

	/**
	 * @param string
	 */
	public void setSuperAbbrName(String string) {
		superAbbrName = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

	/**
	 * @param string
	 */
	public void setUnivNameAbbr(String string) {
		univNameAbbr = string;
	}

	/**
	 * @return
	 */
	public String getDeptSortKey() {
		return deptSortKey;
	}

	/**
	 * @param string
	 */
	public void setDeptSortKey(String string) {
		deptSortKey = string;
	}

}

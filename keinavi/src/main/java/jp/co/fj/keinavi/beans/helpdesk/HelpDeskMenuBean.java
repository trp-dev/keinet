package jp.co.fj.keinavi.beans.helpdesk;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.beans.login.DeputyLoginBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.interfaces.ILogin;

import com.fjh.beans.DefaultBean;

/**
 * ヘルプデスクメニュー選択Bean
 * 
 * <2010年度マーク高２模試対応>
 * 2011.01.24	Tomohisa YAMADA
 * 				アップロード機能
 * 
 * @author kawai
 */
public class HelpDeskMenuBean extends DefaultBean {

	private LoginSession loginSession = new LoginSession(); // ログインセッション
	private String schoolCode;		// 学校コード
	private String buildingCode;	// 校舎部門コード
	private String businessCode;	// 営業部門コード
	private String	menu;			// メニュー選択番号 

	/**
	 * Bean実行
	 */
	public void execute() throws java.lang.Exception, java.sql.SQLException {

		// ログインセッション取得
		loginSession = getLoginSession();

		// 高校代行メニュー
		if ("1".equals(menu)) {
			DeputyLoginBean bean = new DeputyLoginBean();
			bean.setConnection(null, conn);
			bean.setSchoolCD(schoolCode);
			bean.setLoginSession(loginSession);
			bean.execute();
			
			loginSession.setUserMode(ILogin.HELP_SCHOOL);

		// 校舎、営業部メニュー
		} else if ("2".equals(menu) || "3".equals(menu) || "4".equals(menu)) {	
			Query query = new Query();
			query.append("SELECT  sectorsortingcd ");
			query.append("       ,sectorname      ");
			query.append("  FROM  sectorsorting   ");
			query.append(" WHERE  sectorcd    = ? ");
			query.append("   AND  hsbusidivcd = ? ");

			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				ps = conn.prepareStatement(query.toString());
				ps.setString(1, businessCode);
				
				// 営業
				if ("3".equals(menu) || "4".equals(menu)) {
					ps.setString(2, "1");
					loginSession.setUserMode(ILogin.HELP_SALES);
				// 校舎
				} else {
					ps.setString(2, "2");
					loginSession.setUserMode(ILogin.HELP_CRAM);
				}
				
				rs = ps.executeQuery();
				if (rs.next()) {
					loginSession.setSectorCd(businessCode);
					loginSession.setSectorSortingCD(rs.getString(1));
					loginSession.setUserName(rs.getString(2));
				}
			} finally {
				DbUtils.closeQuietly(rs);
				DbUtils.closeQuietly(ps);
			}
			
		// ユーザ管理
		} else {
			loginSession.setUserName("ヘルプデスク");
		}
	}


	/**
	 * @return
	 */
	public LoginSession getLoginSession() {
		return loginSession;
	}

	/**
	 * @param session
	 */
	public void setLoginSession(LoginSession session) {
		loginSession = session;
	}

	/**
	 * @return
	 */
	public String getSchoolCode() {
		return schoolCode;
	}

	/**
	 * @param string
	 */
	public void setSchoolCode(String string) {
		schoolCode = string;
	}

	/**
	 * @return
	 */
	public String getBuildingCode() {
		return buildingCode;
	}

	/**
	 * @param string
	 */
	public void setBuildingCode(String string) {
		buildingCode = string;
	}

	/**
	 * @return
	 */
	public String getBusinessCode() {
		return businessCode;
	}

	/**
	 * @param string
	 */
	public void setBusinessCode(String string) {
		businessCode = string;
	}

	/**
	 * @return
	 */
	public String getMenu() {
		return menu;
	}

	/**
	 * @param string
	 */
	public void setMenu(String string) {
		menu = string;
	}

}


/*
 * 作成日: 2004/07/23
 * 設問別成績推移グラフ表示用サーブレット
 *
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.graph.LineGraphAppletQ;
import jp.co.fj.keinavi.beans.individual.I109Bean;
import jp.co.fj.keinavi.beans.individual.PreExamSearchBean;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.I108Data;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.SubjectData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I109Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.individual.ExamSorter;

/**
 * FILE：I109Servlet<BR>
 * 説明：設問別成績推移グラフを表示する。<BR>
 * 　　　プロファイルから以下のデータを取得する<BR>
 * 　　　　１、セキュリティスタンプ<BR>
 * 　　　　２、印刷対象チェックボックスデータ<BR>
 * 　　　　３、選択中の生徒１りOR全員<BR>
 * 　　　個人共通セッションから以下のデータを取得する<BR>
 * 　　　　１、対象模試<BR>
 * 　　　対象模試のデータを科目ごとにセット
 * @author totec.ishiguro
 *
 * 2016.1.19	QQ)Hisakawa
 * 				 サーブレット化対応の為、 src/main/webapp/applets から移動
 */
public class I109Servlet extends IndividualServlet {

	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		//フォームデータの取得
		I109Form i109Form = null;
		try {
			i109Form = (I109Form)super.getActionForm(request, "jp.co.fj.keinavi.forms.individual.I109Form");
		} catch (Exception e) {
			KNServletException k = new KNServletException("0I109にてフォームデータの収得に失敗しました。" + e);
			k.setErrorCode("00I109010101");
			throw k;
		}
		//セッション情報
		HttpSession session = request.getSession();

		//プロファイル情報
		Profile profile = (Profile) session.getAttribute(Profile.SESSION_KEY);
		String devRanges = (String)profile.getItemMap(IProfileCategory.I_SCORE_BC).get(IProfileItem.DEV_RANGE);
		Short indMultiExam = (Short)profile.getItemMap(IProfileCategory.I_SCORE_BC).get(IProfileItem.IND_MULTI_EXAM);
		String bundleCd = profile.getBundleCD();
		//個人共通
		ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);
		//模試データ
		ExamSession examSession = (ExamSession) session.getAttribute(ExamSession.SESSION_KEY);
		// ログイン情報
		LoginSession login =
			(LoginSession) session.getAttribute(LoginSession.SESSION_KEY);

		// 2016/01/19 QQ)Hisakawa 大規模改修 ADD START
        Collection cSubDatas = null;
        Collection cData = null;
        // 2016/01/19 QQ)Hisakawa 大規模改修 ADD END

		if ("i109".equals(getForward(request))) {

			// セッションに保持している学力レベル表示用Mapを取得する
			HashMap levelmap = iCommonMap.getIndSubScholarLevels109();
			String levelkey = null;

			Connection con = null;
			try {
				con = getConnectionPool(request);

				I109Bean i109Bean = new I109Bean();
				i109Bean.setHelpflg(login.isHelpDesk());

				if(!"i109".equals(getBackward(request))) {
				//初回アクセス

					//この生徒の過去に受けた模試を出す
					PreExamSearchBean pesb = new PreExamSearchBean();
					pesb.setExamYear(iCommonMap.getTargetExamYear());
					pesb.setExamCd(iCommonMap.getTargetExamCode());
					pesb.setPersonId(iCommonMap.getTargetPersonId());
					pesb.setExamTypeCd(iCommonMap.getTargetExamTypeCode());
					pesb.setHelpflg(login.isHelpDesk());
					pesb.setConnection("", con);
					pesb.execute();

					List preExams = pesb.getRecordSet();
					ExaminationData rightPre = null;

					//外部データ・内部データ開放日でソート
					String preExamYear = "0";
					String preExamCd = "";
					String preExamType = "";
					if(preExams != null && preExams.size() > 0){
						if(login.isHelpDesk()){
							//ヘルプデスクの場合は内部データ開放日でソート
							Collections.sort(preExams, new ExamSorter().new SortByInDataOpenDate());
						}else{
							//一般の場合は内部データ開放日でソート
							Collections.sort(preExams, new ExamSorter().new SortByOutDataOpenDate());
						}
						rightPre = (ExaminationData)preExams.get(0);
						preExamYear = rightPre.getExamYear();
						preExamCd   = rightPre.getExamCd();
						preExamType = rightPre.getExamTypeCd();
					}

					//前の生徒の科目タブを引き継ぐ
					if(i109Form.getTargetSubCode().equals("")){
						/**前回表示した生徒の科目コードが空なので、科目を引き継がない**/
						i109Bean.setFirstAccess(true);
						i109Bean.setSearchConditionA(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(),preExamYear,preExamCd,
												iCommonMap.getTargetPersonId());
						i109Bean.setTargetBundleCd(bundleCd);
						i109Bean.setDispScholarLevel("");	// 科目コードが「""」なので、学力レベルは「""」とする
						i109Bean.setConnection("", con);
						i109Bean.execute();

						i109Form.setTargetSubCode(i109Bean.getTargetSubCode());
						i109Form.setTargetSubName(i109Bean.getTargetSubName());
					}else{
						/**前回表示した生徒の科目コードがあるので、それを引き継ぐ**/
						i109Bean.setFirstAccess(false);
						i109Bean.setTargetBundleCd(bundleCd);
						i109Bean.setTargetSubCode(i109Form.getTargetSubCode());
						i109Bean.setTargetSubName(i109Form.getTargetSubName());
						levelkey = iCommonMap.makeScholarLevelMapKey(iCommonMap.getTargetExamYear(),
								iCommonMap.getTargetExamCode(),	i109Form.getTargetPersonId(), i109Form.getTargetSubCode());
						i109Bean.setDispScholarLevel(selectScholarLevel(levelmap, levelkey));
						i109Bean.setSearchConditionA(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(),preExamYear,preExamCd,
												iCommonMap.getTargetPersonId());
						i109Bean.setTargetBundleCd(bundleCd);

						i109Bean.setConnection("", con);
						i109Bean.execute();

						//すでにキーが存在するかを調べる
						final String searchKey = iCommonMap.makeScholarLevelMapKey(
								i109Bean.getTargetExamYear(),
								i109Bean.getTargetExamCode(),
								i109Form.getTargetPersonId(),
								i109Form.getTargetSubCode());

						final String searchLevel =
							selectScholarLevel(levelmap, searchKey);

						//すでにキーが存在する場合は
						//学力レベルを変更して検索しなおす
						if (searchLevel != null) {
							i109Bean = new I109Bean();
							i109Bean.setHelpflg(login.isHelpDesk());
							i109Bean.setFirstAccess(false);
							i109Bean.setTargetBundleCd(bundleCd);
							i109Bean.setTargetSubCode(i109Form.getTargetSubCode());
							i109Bean.setTargetSubName(i109Form.getTargetSubName());
							i109Bean.setDispScholarLevel(searchLevel);
							i109Bean.setTargetScholarLevel(searchLevel);
							i109Bean.setSearchConditionB(
									iCommonMap.getTargetExamYear(),
									iCommonMap.getTargetExamCode(),
									iCommonMap.getTargetPersonId(),
									preExamYear,
									preExamCd);
							i109Bean.setConnection("", con);
							i109Bean.execute();
						}
					}

					// 再表示学力レベルをセット
					i109Form.setDispScholarLevel(i109Bean.getDispScholarLevel());
					//学力レベル一時格納用処理追加
					putICommonMapForScholarLevel(iCommonMap, i109Form, i109Bean);
				}
				else{
				//二回目以降

					i109Form.setTargetPersonId(iCommonMap.getTargetPersonId());

					i109Bean.setFirstAccess(false);

					i109Bean.setTargetBundleCd(bundleCd);
					i109Bean.setTargetSubCode(i109Form.getTargetSubCode());
					i109Bean.setTargetSubName(i109Form.getTargetSubName());

					i109Bean.setSearchConditionB(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(),
												iCommonMap.getTargetPersonId(), i109Form.getTargetExamYear(),
												i109Form.getTargetExamCode());
					// 再表示の際には、画面で設定した学力レベルをセットする。
					// 科目を変更した場合
					if(iCommonMap.getDispSubCd() != null &&
							!iCommonMap.getDispSubCd().equals(i109Form.getTargetSubCode())) {
						levelkey = iCommonMap.makeScholarLevelMapKey(i109Form.getTargetExamYear(),
								i109Form.getTargetExamCode(),	i109Form.getTargetPersonId(), i109Form.getTargetSubCode());
						i109Bean.setDispScholarLevel(selectScholarLevel(levelmap, levelkey));
					}
					// それ以外の場合
					else {
						i109Bean.setDispScholarLevel(i109Form.getDispScholarLevel());
					}
					i109Bean.setConnection("", con);
					i109Bean.execute();

					// 再表示学力レベルをセット
					i109Form.setDispScholarLevel(i109Bean.getDispScholarLevel());
					//学力レベル一時格納用処理追加
					putICommonMapForScholarLevel(iCommonMap, i109Form, i109Bean);
				}

				iCommonMap.setDispScholarLevel(i109Form.getDispScholarLevel());
				iCommonMap.setDispSubCd(i109Form.getTargetSubCode());
				request.setAttribute("i109Bean", i109Bean);

                // 2016/01/19 QQ)Hisakawa 大規模改修 ADD START
				cSubDatas = i109Bean.getSubDatas();
				cData = i109Bean.getRecordSet();
                // 2016/01/19 QQ)Hisakawa 大規模改修 ADD END

			}catch(Exception e){
				e.printStackTrace();
				KNServletException k = new KNServletException("0I109にてエラーが発生しました。"  + e);
				k.setErrorCode("00I109010101");
				throw k;
			} finally {
				try {if (con != null) releaseConnectionPool(request, con);
				} catch (Exception e) {
					KNServletException k = new KNServletException("0I109にてコネクションの解放に失敗しました。" + e);
					k.setErrorCode("00I109010101");
					throw k;
				}
			}

			request.setAttribute("form", i109Form);

			// 2016/01/19 QQ)Hisakawa 大規模改修 ADD START
			//科目タブ切り替え
		    String dispSelect = "";	//グラフ表示／非表示
		    String sLineItemName = "校内平均";
		    I108Data Data = null;

		    java.util.Iterator it=cData.iterator();

			Data = (I108Data) it.next();

		    if(iCommonMap.getTargetExamTypeCode() != "31") {
		    	sLineItemName += ",全国平均";
		    }

		    if(i109Form.getDispScholarLevel() != "") {
		    	sLineItemName += "," + i109Form.getDispScholarLevel() + "レベル";
		    }

		    java.util.Iterator cd = cSubDatas.iterator();
		    boolean flag = false;
		    while(cd.hasNext()) {
		        SubjectData data = (SubjectData)cd.next();
		        String temp = data.getSubjectCd();
		        if(temp.equals(i109Form.getTargetSubCode())) {
		            flag = true;
		            break;
		        }
		    }
		    if(flag==true){
		        dispSelect = "on";
		        i109Form.setDispSelect("on");
		    }else{
		        dispSelect = "off";
		        i109Form.setDispSelect("off");
		    }

            LineGraphAppletQ App = new LineGraphAppletQ();

            if(cData.size() != 0) {

	            App.setParameter("examName", String.valueOf(Data.getExamName()));
	            App.setParameter("dispSelect", dispSelect);
	            App.setParameter("yTitle", "得点率％");

	            if(iCommonMap.getTargetExamTypeCode() != "31") {
	            	App.setParameter("LineItemNum", "2");
	            } else {
	            	App.setParameter("LineItemNum", "1");
	            }

	            App.setParameter("LineItemNameS", "対象生徒");
	            App.setParameter("LineItemName", sLineItemName);
	            App.setParameter("examValueS", String.valueOf(Data.getExamValueS()));
	            App.setParameter("examValue0", String.valueOf(Data.getExamValue0()));

	            if(iCommonMap.getTargetExamTypeCode() != "31") {
	                App.setParameter("examValue1", String.valueOf(Data.getExamValue1()));
	            }

	            App.setParameter("examValueL", String.valueOf(Data.getExamValueL()));
	            App.setParameter("BarSize", "10");
	            App.setParameter("colorDAT", "1,0,13,14,13,14,2");

            } else {

	            App.setParameter("examName", "");
	            App.setParameter("dispSelect", "off");
	            App.setParameter("yTitle", "");
	            App.setParameter("LineItemNum", "0");
	            App.setParameter("LineItemNameS", "");
	            App.setParameter("LineItemName", "");
	            App.setParameter("examValueS", "");
	            App.setParameter("examValue0", "");
	            App.setParameter("examValue1", "");
	            App.setParameter("examValueL", "");
	            App.setParameter("BarSize", "10");
	            App.setParameter("colorDAT", "1,0,13,14,13,14,2");

            }

            App.init();

            session.setAttribute("GraphServlet", App);
            session.setAttribute("GraphType", "LineGraphQ");

            App = null;
            // 2016/01/19 QQ)Hisakawa 大規模改修 ADD END

			super.forward(request, response, JSP_I109);

		} else {

			//不明ならServlet転送
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

	/**
	 * ICommonMap内の学力レベルを選択する。
	 * @param levelMap2
	 * @param levelkey
	 * @param form
	 * @return
	 */
	protected String selectScholarLevel(HashMap lvlMap, String lvlkey) {

		String lvl = new String();

		// 画面表示学力レベルを保持してる場合（画面で選択した学力レベル）
		if (lvlMap.containsKey(lvlkey)) {
			lvl = (String)lvlMap.get(lvlkey);
		}
		// 画面表示学力レベルを保持していない場合（対象生徒の学力レベル）
		else {
			lvl = "";
		}

		return lvl;

	}

	/**
	 * ICommonMapに画面で選択した学力レベル情報を格納する。
	 * @param commap iCommonMap
	 * @param form I108画面から取得した亜短
	 * @param bean I108Bean
	 */
	protected void putICommonMapForScholarLevel(ICommonMap commap, I109Form form, I109Bean bean) {

		// 画面に表示する学力レベルが「""」ならば、セッションにセットしない。
		if ("".equals(form.getDispScholarLevel())) return;

		String key = new String();
		// レベル情報格納用キー作成
		key = commap.makeScholarLevelMapKey(
				bean.getTargetExamYear(),
				bean.getTargetExamCode(),
				form.getTargetPersonId(),
				form.getTargetSubCode());
		// セッションに格納されているレベル情報を取得する
		HashMap levelmap = commap.getIndSubScholarLevels109();
		// レベル情報再設定
		levelmap.put(key, bean.getDispScholarLevel());
		// 格納情報をセッションへセット
		commap.setIndSubScholarLevels109(levelmap);

	}
}

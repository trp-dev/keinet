package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 再集計対象を登録するBean
 * 
 * 2005.11.24	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class RecountRegisterBean extends DefaultBean {

	// 年度
	private String year;
	// 個人ID
	private String individualId;
	// 変更前学年
	private int beforeGrade = -1;
	// 変更前クラス
	private String beforeClassName;
	// 変更後学年
	private int afterGrade = -1;
	// 変更後クラス
	private String afterClassName;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		// 変更がなければここまで
		if (afterGrade == beforeGrade 
				&& afterClassName.equals(beforeClassName)) {
			return;
		}

		insertIntoSubRecount();
		insertIntoRatingnumberRecount();
	}

	protected void insertIntoSubRecount() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().load("m14").toString());
			
			ps.setString(3, individualId);
			ps.setString(4, year);
			
			// 変更前
			ps.setInt(1, beforeGrade);
			ps.setString(2, beforeClassName);
			ps.setInt(5, beforeGrade);
			ps.setString(6, beforeClassName);
			ps.executeUpdate();
			
			// 変更後
			ps.setInt(1, afterGrade);
			ps.setString(2, afterClassName);
			ps.setInt(5, afterGrade);
			ps.setString(6, afterClassName);
			ps.executeUpdate();
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	protected void insertIntoRatingnumberRecount() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().load("m15").toString());
			
			ps.setString(3, individualId);
			ps.setString(4, year);
			
			// 変更前
			ps.setInt(1, beforeGrade);
			ps.setString(2, beforeClassName);
			ps.setInt(5, beforeGrade);
			ps.setString(6, beforeClassName);
			ps.executeUpdate();
			
			// 変更後
			ps.setInt(1, afterGrade);
			ps.setString(2, afterClassName);
			ps.setInt(5, afterGrade);
			ps.setString(6, afterClassName);
			ps.executeUpdate();
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * @param afterClassName 設定する afterClassName。
	 */
	public void setAfterClassName(String afterClassName) {
		this.afterClassName = afterClassName;
	}

	/**
	 * @param afterGrade 設定する afterGrade。
	 */
	public void setAfterGrade(int afterGrade) {
		this.afterGrade = afterGrade;
	}

	/**
	 * @param year 設定する year。
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @param individualId 設定する individualId。
	 */
	public void setIndividualId(String individualId) {
		this.individualId = individualId;
	}

	/**
	 * @param pBeforeClassName 設定する beforeClassName。
	 */
	public void setBeforeClassName(String pBeforeClassName) {
		beforeClassName = pBeforeClassName;
	}

	/**
	 * @param pBeforeGrade 設定する beforeGrade。
	 */
	public void setBeforeGrade(int pBeforeGrade) {
		beforeGrade = pBeforeGrade;
	}

}

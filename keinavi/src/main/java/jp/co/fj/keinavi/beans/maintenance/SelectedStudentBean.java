package jp.co.fj.keinavi.beans.maintenance;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.keinavi.data.maintenance.KNStudentExtData;
import jp.co.fj.keinavi.util.KNUtil;

import org.apache.commons.lang.StringUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 選択済み生徒Bean
 * 
 * 2006.10.26	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class SelectedStudentBean extends DefaultBean {

	// 選択済み生徒リスト
	private final List studentList = new ArrayList();
	// 生徒情報取得Bean
	private final StudentListBean bean;
	
	// 主生徒個人ID
	private String mainIndividualId;
	// 従生徒個人ID
	private String subIndividualId;
	// 同期フラグ
	private boolean isSynchronized = true;
	
	/**
	 * @param pSchoolCd 学校コード
	 */
	public SelectedStudentBean(final String pSchoolCd) {
		bean = new StudentListBean(pSchoolCd);
		bean.setYear(KNUtil.getCurrentYear()); // 現年度固定
	}
	
	/**
	 * @see com.fjh.beans.DefaultBean#setConnection(
	 * 			java.lang.String, java.sql.Connection)
	 */
	public void setConnection(final String pName,
			final Connection pConn) {
		super.setConnection(pName, pConn);
		bean.setConnection(pName, pConn);
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		// 同期が取れているならここまで
		if (isSynchronized()) {
			return;
		}
		
		// クリア
		studentList.clear();
		
		// 主生徒
		if (mainIndividualId != null) {
			bean.setIndividualId(mainIndividualId);
			bean.execute();
			final KNStudentExtData data = bean.getFirstStudentData();
			if (data == null) {
				mainIndividualId = null;
			} else {
				data.setSelectedFlag(1);
				studentList.add(data);
			}
		}

		// 従生徒
		if (subIndividualId != null) {
			bean.setIndividualId(subIndividualId);
			bean.execute();
			final KNStudentExtData data = bean.getFirstStudentData();
			if (data == null) {
				subIndividualId = null;
			} else {
				data.setSelectedFlag(2);
				studentList.add(data);
			}
		}
		
		// 同期が取れた
		setSynchronized(true);
	}
	
	/**
	 * @return 選択済み生徒（主従生徒）の個人IDマップ
	 */
	public Map getSelectedStudentMap() {
		
		final Map map = new HashMap();
		
		for (final Iterator ite = studentList.iterator(
				); ite.hasNext();) {
			final KNStudentExtData data = (KNStudentExtData) ite.next();
			map.put(data.getIndividualId(), Boolean.TRUE);
		}
		
		return map;
	}

	/**
	 * 主従切替
	 * 
	 * @param id 新しく主生徒とする個人ID
	 */
	public void exchange() {
		mainIndividualId = null;
		subIndividualId = null;
		for (Iterator ite = studentList.iterator(); ite.hasNext();) {
			final KNStudentExtData data = (KNStudentExtData) ite.next();
			if (data.getSelectedFlag() == 1) {
				subIndividualId = data.getIndividualId();
				data.setSelectedFlag(2);
			} else {
				mainIndividualId = data.getIndividualId();
				data.setSelectedFlag(1);
			}
		}
	}
	
	/**
	 * 従生徒を削除する
	 * 
	 * @param id 削除する生徒の個人ID
	 */
	public void removeSubStudent() {
		subIndividualId = null;
		for (Iterator ite = studentList.iterator(); ite.hasNext();) {
			final KNStudentExtData data = (KNStudentExtData) ite.next();
			if (data.getSelectedFlag() == 2) {
				ite.remove();
				break;
			}
		}
	}
	
	/**
	 * @return mainStudent を戻します。
	 */
	public KNStudentExtData getMainStudentData() {
		return getStudentData(1);
	}

	/**
	 * @return subStudent を戻します。
	 */
	public KNStudentExtData getSubStudentData() {
		return getStudentData(2);
	}
	
	// 指定した選択フラグの生徒を取得する
	private KNStudentExtData getStudentData(final int selectedFlag) {
		for (final Iterator ite = studentList.iterator(); ite.hasNext();) {
			final KNStudentExtData data = (KNStudentExtData) ite.next();
			if (data.getSelectedFlag() == selectedFlag) {
				return data;
			}
		}
		return null;
	}
	
	/**
	 * @param id 個人ID
	 * @return Student を戻します。
	 */
	public KNStudentExtData getStudentData(final String id) {
		for (final Iterator ite = studentList.iterator(); ite.hasNext();) {
			final KNStudentExtData data = (KNStudentExtData) ite.next();
			if (data.getIndividualId().equals(id)) {
				return data;
			}
		}
		throw new InternalError("選択済み生徒の取得に失敗しました。" + id);
	}
	
	/**
	 * @param pMainIndividualId 設定する mainIndividualId。
	 */
	public void setMainIndividualId(final String pMainIndividualId) {
		
		// 値が異なるなら保持する
		if (!StringUtils.equals(mainIndividualId, pMainIndividualId)) {
			mainIndividualId = pMainIndividualId;
			setSynchronized(false);
		}
	}

	/**
	 * @param pSubIndividualId 設定する subIndividualId。
	 */
	public void setSubIndividualId(final String pSubIndividualId) {
		
		// 値が異なるなら保持する
		if (!StringUtils.equals(subIndividualId, pSubIndividualId)) {
			subIndividualId = pSubIndividualId;
			setSynchronized(false);
		}
	}

	/**
	 * @return isSynchronized を戻します。
	 */
	public boolean isSynchronized() {
		return isSynchronized;
	}

	/**
	 * @param pIsSynchronized 設定する isSynchronized。
	 */
	public void setSynchronized(boolean pIsSynchronized) {
		isSynchronized = pIsSynchronized;
	}

}

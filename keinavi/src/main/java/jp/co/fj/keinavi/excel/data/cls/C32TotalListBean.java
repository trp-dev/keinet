package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス比較−偏差値分布集計データリスト
 * 作成日: 2004/07/15
 * @author	H.Fujimoto
 */
public class C32TotalListBean {
	//学校名
	private String strGakkomei = "";
	//クラス名
	private String strClass = "";	
	//クラス用グラフ表示フラグ
	private int intDispClassFlg = 0;
	//合計人数
	private int intNinzu = 0;
	//平均点
	private float floHeikin = 0;
	//平均偏差値
	private float floHensa = 0;
	//偏差値分布データリスト
	private ArrayList c32BnpList = new ArrayList();
	
	/*----------*/
	/* Get      */
	/*----------*/
	
	public ArrayList getC32BnpList() {
		return this.c32BnpList;
	}
	public float getFloHeikin() {
		return this.floHeikin;
	}
	public float getFloHensa() {
		return this.floHensa;
	}
	public int getIntDispClassFlg() {
		return this.intDispClassFlg;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrGakkomei() {
		return this.strGakkomei;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setC32BnpList(ArrayList c32BnpList) {
		this.c32BnpList = c32BnpList;
	}
	public void setFloHeikin(float floHeikin) {
		this.floHeikin = floHeikin;
	}
	public void setFloHensa(float floHensa) {
		this.floHensa = floHensa;
	}
	public void setIntDispClassFlg(int intDispClassFlg) {
		this.intDispClassFlg = intDispClassFlg;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}

}
/*
 * 作成日: 2004/10/16
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class I108Data {

	/** for applet */
	private String examName;
	private String dispSelect;
	private String examValueL;
	private String examValueS;
	private String examValue0;
	private String examValue1;

	/** tableData */
	private String[][] tableData;

	// 2016/02/08 QQ)Nishiyama 大規模改修 ADD START
    /**
     * コンストラクタ
     */
    public I108Data() {
    }
	// 2016/02/08 QQ)Nishiyama 大規模改修 ADD END

	/**
	 * @return
	 */
	public String getDispSelect() {
		return dispSelect;
	}

	/**
	 * @return
	 */
	public String getExamName() {
		return examName;
	}

	/**
	 * @return
	 */
	public String getExamValue0() {
		return examValue0;
	}

	/**
	 * @return
	 */
	public String getExamValue1() {
		return examValue1;
	}

	/**
	 * @return
	 */
	public String getExamValueS() {
		return examValueS;
	}

	/**
	 * @return
	 */
	public String[][] getTableData() {
		return tableData;
	}

	/**
	 * @param string
	 */
	public void setDispSelect(String string) {
		dispSelect = string;
	}

	/**
	 * @param string
	 */
	public void setExamName(String string) {
		examName = string;
	}

	/**
	 * @param string
	 */
	public void setExamValue0(String string) {
		examValue0 = string;
	}

	/**
	 * @param string
	 */
	public void setExamValue1(String string) {
		examValue1 = string;
	}

	/**
	 * @param string
	 */
	public void setExamValueS(String string) {
		examValueS = string;
	}

	/**
	 * @param strings
	 */
	public void setTableData(String[][] strings) {
		tableData = strings;
	}

	public String getExamValueL() {
		return examValueL;
	}

	public void setExamValueL(String examValueL) {
		this.examValueL = examValueL;
	}

}

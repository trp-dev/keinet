package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.beans.NewestExamDivSearchBean;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

/**
 *
 * 受験予定大学検索（大学指定）
 * 
 * <2010年度改修>
 * 2010.01.19	Tomohisa YAMADA - TOTEC
 * 				新規作成
 *
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class PlanUnivSearchUnivBean extends PlanUnivSearchBean {
	
	/**
	 * 大学コード
	 */
	private String univCd;
	
	/**
	 * 学部コード
	 */
	private String facultyCd;
	
	/**
	 * 学科コード
	 */
	private String deptCd;
		
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		univs = setupList(search());
	}
	
	/**
	 * 検索
	 * @return
	 * @throws Exception
	 */
	private List search() throws Exception {
		
		//最新の模試年度と模試区分を設定
		NewestExamDivSearchBean news = new NewestExamDivSearchBean();
		news.setConnection(null, conn);
		news.execute();
		
		PreparedStatement ps1 = null;
		
		try {
			
			Query query = QueryLoader.getInstance().load("i07");
			ps1 = conn.prepareStatement(query.toString());
			ps1.setString(1, news.getExamYear());
			ps1.setString(2, news.getExamDiv());
			ps1.setString(3, univCd);
			ps1.setString(4, facultyCd);
			ps1.setString(5, deptCd);
			
			List result = searchPlans(ps1, news.getExamYear(), news.getExamDiv());
	
			//初回でない場合は受験予定大学データは取得せずに終了
			if (!isFirst) {
				return result;
			}
	
			//受験予定大学情報検索
			//�@の場合のときのみ実行が必要
			//�@の結果データとの結果比較を行って日程重複などをチェックする
			result.addAll(
					searchPlannedUnivs(
							news.getExamYear(), 
							news.getExamDiv(), 
							individualCd, 
							univCd, 
							facultyCd, 
							deptCd, 
							entExamModeCd));
			
			return result;
			
		} finally {
			DbUtils.closeQuietly(ps1);
		}
	}

	/**
	 * 大学コードを設定
	 * @param univCd
	 */
	public void setUnivCd(String univCd) {
		this.univCd = univCd;
	}

	/**
	 * 学部コードを設定
	 * @param facultyCd
	 */
	public void setFacultyCd(String facultyCd) {
		this.facultyCd = facultyCd;
	}

	/**
	 * 学科コードを設定
	 * @param deptCd
	 */
	public void setDeptCd(String deptCd) {
		this.deptCd = deptCd;
	}
	
	/**
	 * 大学コードを返す
	 * @return
	 */
	public String getUnivCd() {
		return this.univCd;
	}
	
	
}

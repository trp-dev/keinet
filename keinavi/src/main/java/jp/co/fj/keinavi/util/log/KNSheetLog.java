package jp.co.fj.keinavi.util.log;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import jp.co.fj.keinavi.data.LoginSession;

/**
 *
 * 帳票アクセスログクラス
 * 
 * 2006.11.08	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class KNSheetLog {

	// IPアドレス
	private final String remoteAddr;
	// ホスト名
	private final String remoteHost;
	// ログイン情報
	private final LoginSession login;
	// 帳票IDの入れ物
	private final Collection list = new HashSet();
	// 画面ID
	private final String screenId;
	// 印刷フラグ
	private final String printFlag;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pRemoteAddr IPアドレス
	 * @param pRemoteHost ホスト名
	 * @param pLogin ログイン情報
	 */
	public KNSheetLog(final String pRemoteAddr,
			final String pRemoteHost, final LoginSession pLogin,
			final String pScreenId, final String pPrintFlag) {
		remoteAddr = pRemoteAddr;
		remoteHost = pRemoteHost;
		login = pLogin;
		screenId = pScreenId;
		printFlag = pPrintFlag;
	}

	/**
	 * @param id 追加する帳票ID
	 */
	public void add(final String id) {
		list.add(id);
	}
	
	/**
	 * ログを出力する
	 */
	public void output() {
		
		final StringBuffer buff = new StringBuffer();
		for (Iterator ite = list.iterator(); ite.hasNext();) {
			if (buff.length() > 0) {
				buff.append(' ');
			}
			buff.append(ite.next());
		}

		// 実行条件コード
		final String code;
		// 表示
		if ("1".equals(printFlag)) {
			code = "301";
		// 印刷のみ
		} else if ("2".equals(printFlag)) {
			code = "302";
		// 一括印刷のみ
		} else if ("3".equals(printFlag)) {
			code = "402";
		// 一括印刷＆保存
		} else if ("4".equals(printFlag)) {
			code = "403";
		// 一括保存
		} else if ("5".equals(printFlag)) {
			// テキスト出力（一括出力）
			if ("t001".equals(screenId) || "t101".equals(screenId)) {
				code = "412";
			// テキスト出力（個別出力）
			} else if (screenId.startsWith("t")) {
				code = "411";
			// それ以外
			} else {
				code = "401";
			}
		// 印刷＆保存
		} else if ("8".equals(printFlag)) {
			code = "303";
		} else {
			throw new IllegalArgumentException(
					"印刷フラグが不正です。" + printFlag);
		}
		
		KNAccessLog.lv2(remoteAddr, remoteHost, login,
				screenId, code, buff.toString());
	}
	
}

package jp.co.fj.keinavi.judgement.beans;

import java.util.Iterator;
import java.util.List;

import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.kawaijuku.judgement.data.Examination;
import jp.co.fj.kawaijuku.judgement.util.JudgementProperties;
import jp.co.fj.kawaijuku.judgement.util.JudgementUtil;
import jp.co.fj.keinavi.data.SubRecordAData;
import jp.co.fj.keinavi.data.individual.SubRecordData;

/**
 *
 * Kei-Navi判定用得点クラス
 * ※ＤＢより成績データを取得するため
 * 数学�@データの作成ロジックを保持する
 *
 *
 * <2010年度改修>
 * 2010.04.08	Tomohisa YAMADA - Totec
 * 				新規作成
 * 
 * 
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 */
public class ScoreKeiNavi extends Score {
	
	//マーク模試用科目別成績（全国）
	private List nationalMarkRecs;
	
	/**
	 * コンストラクタ
	 * @param list マーク模試用科目別成績（全国）
	 */
	public ScoreKeiNavi(List list) {
		this.nationalMarkRecs = list;
	}
	
	/**
	 * コンストラクタ
	 * @param list マーク模試用科目別成績（全国）
	 * @param markExam マーク模試
	 * @param marks マーク成績
	 * @param writtenExam 記述模試
	 * @param writtens 記述成績
	 */
    public ScoreKeiNavi(
    		List list,
    		Examination markExam, 
    		List marks, 
    		Examination writtenExam, 
    		List writtens) {
    	//親コンストラクタ
    	super(
			markExam, 
			marks, 
			writtenExam, 
			writtens);
    	this.nationalMarkRecs = list;
    }
	
    /**
     * 実行
     */
	public void execute() throws Exception {

		//Kei-Navi用特殊処理
		prepare();
		
		super.execute();
	}

	/**
	 * 特殊処理
	 * @throws Exception
	 */
	private void prepare() throws Exception {
		
		//マーク系模試が設定されている場合
		//数学�@の偏差値チェック
		if (getMark() != null) {
			math();
		}
	}
	
	/**
	 * 数学�@の偏差値チェック
	 * @throws Exception
	 */
	private void math() throws Exception {
		
		//数学Ｉまたは数学ＩＡ
		SubRecordData base = null;
		
		//数学�@�Aが存在するかどうか
		boolean foundOneTwo= false;
		
		for (Iterator ite = getMark().iterator(); ite.hasNext();) {
			
			SubRecordData data = (SubRecordData) ite.next();
			
			//int型の科目コード
			final int subcd = Integer.parseInt(data.getSubCd());
			
			//数学Ｉまたは数学ＩＡ存在する方のデータを記憶する
			if (JudgementUtil.match(subcd, "mark.subcd.math1")
					|| JudgementUtil.match(subcd, "mark.subcd.math1A")) {			
				base = data;
			}
			
			//数学�@�Aが存在する場合かどうか
			if (JudgementUtil.match(subcd, "mark.subcd.mathOneTwo")) {
				foundOneTwo = true;
			}
		}

		//数学�@�Aが存在する場合は
		//数学Ｉまたは数学ＩＡをもとに数学�@データを作成する
		if (foundOneTwo) {
			
			//エラー
			if (base == null) {
				throw new InternalError("数学Ｉまたは数学ＩＡが存在しません");
			}
			
			getMark().add(createMathOne(searchMathOne(), base));
		}
		
	}
	
	/**
	 * 科目別成績（全国）から数学�@の情報を取得する
	 * @return
	 * @throws Exception
	 */
	private SubRecordAData searchMathOne() throws Exception {
		
		if (nationalMarkRecs == null) {
			throw new InternalError("科目別成績（全国）が設定されていません");
		}
		
		for (Iterator ite = nationalMarkRecs.iterator(); 
				ite.hasNext(); ) {
			
			SubRecordAData data = (SubRecordAData) ite.next();
			
			//数学�@データをかえす
			if (data.getSubcd().equals(
					JudgementProperties.getInstance().getSubRepcd("mark.subcd.mathOne"))) {
				return data;
			}
		}
		
		throw new InternalError("数学�@データが見つかりません");
	}
	
	/**
	 * 数学�@データの作成する
	 * @param mathOne
	 * @param base
	 * @return
	 */
	private SubRecordData createMathOne(SubRecordAData mathOne, SubRecordData base) {
		//数学�@データを作成
		//科目コード：数学�@
		//得点：数学Ｉまたは数学ＩＡ
		//ＣＶＳ得点：数学Ｉまたは数学ＩＡ
		//偏差値：数学Ｉまたは数学ＩＡからさん出する
		SubRecordData record = new SubRecordData();
		record.setSubCd(JudgementProperties.getInstance().getSubRepcd("mark.subcd.mathOne"));
		record.setScore(base.getScore());
		record.setCvsScore(base.getCvsScore());
		record.setCDeviation(Double.toString(
						JudgementUtil.calcDeviation(
								Integer.parseInt(base.getScore()), 
								mathOne.getAvgpnt(),
								mathOne.getStddeviation())));
		return record;
	}
}

/*
 * 校内成績分析−クラス比較　志望大学評価別人数
 * 作成日: 2004/09/
 * @author	A.Hasegawa
 */

package jp.co.fj.keinavi.excel.cls;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.cls.C34Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

/**
 * @author keinet
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class C34 {

	/*
	 * 出力帳票選択処理
	 * フラグにより帳票出力の判断をする。
	 * 		C34Item c34Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID:ユーザID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */

	 public boolean c34(C34Item c34Item, ArrayList outfilelist, int intSaveFlg, String UserID, KNSheetLog sheetLog) {

		KNLog log = KNLog.getInstance(null,null,null);
	 	try{
	 		int intRunFlg = c34Item.getIntDaiTotalFlg();
			int ret = 0;
			//C34Itemから各帳票を出力
			switch(intRunFlg) {
				case 1:	//大学（日程あり）
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL START
//					C34_02 exceledit02 = new C34_02();
//					ret = exceledit02.c34_02EditExcel(c34Item,outfilelist,intSaveFlg,UserID);
//					if (ret!=0) {
//						log.Err("0"+ret+"C34_02","帳票作成エラー","");
//						return false;
//					}
//					sheetLog.add("C34_02");
//					break;
//				case 2:	//大学（日程なし）
//					C34_01 exceledit01 = new C34_01();
//					ret = exceledit01.c34_01EditExcel(c34Item,outfilelist,intSaveFlg,UserID);
//					if (ret!=0) {
//						log.Err("0"+ret+"C34_01","帳票作成エラー","");
//						return false;
//					}
//					sheetLog.add("C34_01");
//					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL END
// 2019/10/02 QQ)Tanioka 廃止帳票 ADD START
				case 2:	//大学（日程なし）
				case 4:	//学部（日程あり）
// 2019/10/02 QQ)Tanioka 廃止帳票 ADD END
				case 3:	//学部（日程なし）
					C34_04 exceledit04 = new C34_04();
					ret = exceledit04.c34_04EditExcel(c34Item,outfilelist,intSaveFlg,UserID);
					if (ret!=0) {
						log.Err("0"+ret+"C34_04","帳票作成エラー","");
						return false;
					}
					sheetLog.add("C34_04");
					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL START
//				case 4:	//学部（日程あり）
//					C34_03 exceledit03 = new C34_03();
//					ret = exceledit03.c34_03EditExcel(c34Item,outfilelist,intSaveFlg,UserID);
//					if (ret!=0) {
//						log.Err("0"+ret+"C34_03","帳票作成エラー","");
//						return false;
//					}
//					sheetLog.add("C34_03");
//					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL END
				case 5:	//学科
					C34_05 exceledit05 = new C34_05();
					ret = exceledit05.c34_05EditExcel(c34Item,outfilelist,intSaveFlg,UserID);
					if (ret!=0) {
						log.Err("0"+ret+"C34_05","帳票作成エラー","");
						return false;
					}
					sheetLog.add("C34_05");
					break;

			}
	 	}
	 	catch(Exception e){
			log.Err("99C34","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
	 	}

	 	return true;

	 }

}

package jp.co.fj.keinavi.excel.data.school;

/**
 * 校内成績分析−過年度比較−偏差値分布 CEFR取得状況データリスト
 * 作成日: 2019/08/20
 * @author	QQ)H.Yamasaki
 */
public class S22CefrAcqStatusListBean {

	//模試年度
	private String examyear1 = "";
	//模試コード
	private String examcd = "";
	//CEFRレベルコード
	private String cefrlevelcd = "";
	//CEFRレベル名
	private String cerflevelname = "";
	//並び順
	private int cefrinfo_sort = 0;
	//人数
	private int numbers1 = 0;
	//構成比
	private float compratio1 = 0;
	//学校受験有無
	private String flg1 = "";
	//模試年度(過年1)
	private String examyear2 = "";
	//人数(過年1)
	private int numbers2 = 0;
	//構成比(過年1)
	private float compratio2 = 0;
	//模試年度(過年2)
	private String examyear3 = "";
	//人数(過年2)
	private int numbers3 = 0;
	//構成比(過年2)
	private float compratio3 = 0;
	//模試年度(過年3)
	private String examyear4 = "";
	//人数(過年3)
	private int numbers4 = 0;
	//構成比(過年3)
	private float compratio4 = 0;
	//模試年度(過年4)
	private String examyear5 = "";
	//人数(過年4)
	private int numbers5 = 0;
	//構成比(過年4)
	private float compratio5 = 0;


	/*----------*/
	/* Get      */
	/*----------*/
	public String getExamyear1() {
		return examyear1;
	}
	public String getExamcd() {
		return examcd;
	}
	public String getCefrlevelcd() {
		return cefrlevelcd;
	}
	public String getCerflevelname() {
		return cerflevelname;
	}
	public int getCefrinfo_sort() {
		return cefrinfo_sort;
	}
	public int getNumbers1() {
		return numbers1;
	}
	public float getCompratio1() {
		return compratio1;
	}
	public String getFlg1() {
		return flg1;
	}
	public String getExamyear2() {
		return examyear2;
	}
	public int getNumbers2() {
		return numbers2;
	}
	public float getCompratio2() {
		return compratio2;
	}
	public String getExamyear3() {
		return examyear3;
	}
	public int getNumbers3() {
		return numbers3;
	}
	public float getCompratio3() {
		return compratio3;
	}
	public String getExamyear4() {
		return examyear4;
	}
	public int getNumbers4() {
		return numbers4;
	}
	public float getCompratio4() {
		return compratio4;
	}
	public String getExamyear5() {
		return examyear5;
	}
	public int getNumbers5() {
		return numbers5;
	}
	public float getCompratio5() {
		return compratio5;
	}


	/*----------*/
	/* Set      */
	/*----------*/

	public void setExamyear1(String examyear) {
		this.examyear1 = examyear;
	}
	public void setExamcd(String examcd) {
		this.examcd = examcd;
	}
	public void setCefrlevelcd(String cefrlevelcd) {
		this.cefrlevelcd = cefrlevelcd;
	}
	public void setCerflevelname(String cerflevelname) {
		this.cerflevelname = cerflevelname;
	}
	public void setCefrinfo_sort(int cefrinfo_sort) {
		this.cefrinfo_sort = cefrinfo_sort;
	}
	public void setNumbers1(int numbers) {
		this.numbers1 = numbers;
	}
	public void setCompratio1(float compratio) {
		this.compratio1 = compratio;
	}
	public void setFlg1(String flg) {
		this.flg1 = flg;
	}
	public void setExamyear2(String examyear) {
		this.examyear2 = examyear;
	}
	public void setNumbers2(int numbers) {
		this.numbers2 = numbers;
	}
	public void setCompratio2(float compratio) {
		this.compratio2 = compratio;
	}
	public void setExamyear3(String examyear) {
		this.examyear3 = examyear;
	}
	public void setNumbers3(int numbers) {
		this.numbers3 = numbers;
	}
	public void setCompratio3(float compratio) {
		this.compratio3 = compratio;
	}
	public void setExamyear4(String examyear) {
		this.examyear4 = examyear;
	}
	public void setNumbers4(int numbers) {
		this.numbers4 = numbers;
	}
	public void setCompratio4(float compratio) {
		this.compratio4 = compratio;
	}
	public void setExamyear5(String examyear) {
		this.examyear5 = examyear;
	}
	public void setNumbers5(int numbers) {
		this.numbers5 = numbers;
	}
	public void setCompratio5(float compratio) {
		this.compratio5 = compratio;
	}
}

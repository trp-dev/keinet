package jp.co.fj.keinavi.servlets.com_set;

import java.io.IOException;
import java.sql.Connection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.PrefBean;
import jp.co.fj.keinavi.beans.UnivDivBean;
import jp.co.fj.keinavi.beans.com_set.UnivSearchBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM401Form;
import jp.co.fj.keinavi.forms.com_set.CM406Form;
import jp.co.fj.keinavi.util.CollectionUtil;

/**
 *
 * 共通項目設定 - 志望大学
 * 
 * 2004.07.27	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class CM406Servlet extends AbstractCMServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.com_set.AbstractCMServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException, IOException {
		
		// 共通処理
		super.execute(request, response);

		// アクションフォーム - request scope
		final CM001Form rcform = (CM001Form) getActionForm(request,
			"jp.co.fj.keinavi.forms.com_set.CM001Form");
		final CM406Form riform = (CM406Form) getActionForm(request,
			"jp.co.fj.keinavi.forms.com_set.CM406Form");

		// 共通アクションフォーム - session scope
		final CM001Form scform = getCommonForm(request);
		// 個別アクションフォーム - session scope
		final CM406Form siform = (CM406Form) scform.getActionForm("CM406Form");
		// 選択画面のアクションフォーム
		final CM401Form f401 = (CM401Form)scform.getActionForm("CM401Form");

		// ログイン情報
		final LoginSession login = getLoginSession(request);
		// プロファイル
		final Profile profile = getProfile(request);
		// 模試セッション
		final ExamSession	examSession = getExamSession(request);
		// 対象模試データ
		final ExamData exam = examSession.getExamData(
				scform.getTargetYear(), scform.getTargetExam());

		// 検索フォームからの遷移
		if ("cm404".equals(getBackward(request))) {
			// はじめは１ページ
			siform.setPage("1");
			// チェックもクリア
			siform.setUniv(null);
			// 検索条件をセッションに保持する
			siform.setPref(riform.getPref());
			siform.setBlock(riform.getBlock());
			siform.setUnivDiv(riform.getUnivDiv());
			siform.setButton(riform.getButton());
			siform.setSearchMode(riform.getSearchMode());
			siform.setSearchStr(riform.getSearchStr());

		} else {
			// 志望大学コードの追加／削除処理
			final String[] master = riform.getMaster();
			// 戻るボタンでの遷移
			if (master == null) {
				
			// それ以外
			} else {
				siform.setPage(riform.getPage());
				// requestで渡された志望大学コード
				Set rUniv = CollectionUtil.array2Set(riform.getUniv());
				// sessionに保存されていた志望大学コード
				List sUniv = CollectionUtil.array2List(siform.getUniv());
				for (int i=0; i<master.length; i++) {
					// チェックされている場合
					if (rUniv.contains(master[i])) {
						// なければ入れる 
						if (!sUniv.contains(master[i])) {
							sUniv.add(master[i]);
						}
					// チェックされていない場合
					} else {
						sUniv.remove(master[i]);
					}
				}
				siform.setUniv((String[])sUniv.toArray(new String[0]));
			}
			// 変更フラグ
			scform.setChanged(rcform.getChanged());
		}
		
		// JSP
		if ("cm406".equals(getForward(request))) {
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);

				// 都道府県Bean
				request.setAttribute("PrefBean", PrefBean.getInstance(con));

				// 大学区分Bean
				final UnivDivBean div = new UnivDivBean();
				div.setConnection(null, con);
				div.execute();
				request.setAttribute("UnivDivBean", div);

				// 大学検索Bean
				final UnivSearchBean bean = new UnivSearchBean();
				bean.setConnection(null, con);
				bean.setButton(siform.getButton());
				bean.setPref(siform.getPref());
				bean.setUnivDiv(siform.getUnivDiv());
				bean.setSearchMode(siform.getSearchMode());
				bean.setSearchStr(siform.getSearchStr());
				bean.setRegistedUniv(f401.getUniv());
				bean.setExam(exam);
				bean.setBundleCD(getBundleCd(login, profile, scform.getTargetExam()));
				bean.setPageSize(10); // ページサイズは10で固定
				bean.setPageNo(Integer.parseInt(siform.getPage())); // ページ番号
				bean.execute();
				
				request.setAttribute("SearchBean", bean);

			} catch (Exception e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			forward(request, response, JSP_CM406);

		// 不明なら転送
		} else {
			// 登録処理
			if ("1".equals(riform.getSave())) {
				final List container = new LinkedList();
				// 元々選択済みの大学コード
				container.addAll(CollectionUtil.array2List(f401.getUniv()));
				// 今回選択された学校コード
				container.addAll(CollectionUtil.array2List(siform.getUniv()));
				// 再設定する
				f401.setUniv((String[])container.toArray(new String[0]));				
			}
			forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

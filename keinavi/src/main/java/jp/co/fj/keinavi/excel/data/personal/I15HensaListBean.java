package jp.co.fj.keinavi.excel.data.personal;

import java.util.ArrayList;
/**
 * �l���ѕ��́|�󌱗p�����|�󌱑�w�X�P�W���[���\ �͎��ʕ΍��l�f�[�^���X�g
 * �쐬��: 2004/07/20
 * @author	A.Iwata
 */
public class I15HensaListBean {
	//�͎���
	private String strMshmei = "";
	//�͎����{���
	private String strMshDate = "";
	//���ȕʕ΍��l�f�[�^���X�g
	private ArrayList i15KmkHensaList = new ArrayList();
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public ArrayList getI15KmkHensaList() {
		return this.i15KmkHensaList;
	}

	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setI15KmkHensaList(ArrayList i15KmkHensaList) {
		this.i15KmkHensaList = i15KmkHensaList;
	}
}

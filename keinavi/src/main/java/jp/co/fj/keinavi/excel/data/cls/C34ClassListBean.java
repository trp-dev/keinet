package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;

/**
 * クラス比較−志望大学クラス別評価人数リスト
 * 作成日: 2004/08/06
 * @author	A.Iwata
 */
public class C34ClassListBean {
	//学校名
	private String strGakkomei = "";
	//学年
	private String strGrade = "";
	//クラス名
	private String strClass = "";
	//評価別人数データリスト
	private ArrayList c34HyoukaNinzuList = new ArrayList();
	
	/*----------*/
	/* Get      */
	/*----------*/
	
	public ArrayList getC34HyoukaNinzuList() {
		return this.c34HyoukaNinzuList;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrGrade() {
		return this.strGrade;
	}
	public String getStrGakkomei() {
		return this.strGakkomei;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setC34HyoukaNinzuList(ArrayList c34HyoukaNinzuList) {
		this.c34HyoukaNinzuList = c34HyoukaNinzuList;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}

}
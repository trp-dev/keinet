/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.school.factory;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import jp.co.fj.keinavi.forms.school.SMaxForm;
import jp.co.fj.keinavi.data.LoginSession;
import com.fjh.forms.ActionForm;

/**
 * 
 * 2005.04.05	Yoshimoto KAWAI - Totec
 * 				過年度の表示対応
 * 
 * 2005.04.11	Yoshimoto KAWAI - Totec
 * 				日程・評価区分対応
 * 
 * @author kawai
 * 
 */
public class S305FormFactory extends AbstractSFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
     * 志望大学別人数
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		// アクションフォーム
		SMaxForm form = super.createSMaxForm(request);
		// アイテムマップ
		Map item = getItemMap(request);
		// ログイン情報
		LoginSession login =
			(LoginSession)request.getSession().getAttribute(LoginSession.SESSION_KEY);
			
		CommonFormFactory commForm = new CommonFormFactory(form, item);
		commForm.setForm0309();
		commForm.setForm0310();
		commForm.setForm0311();
		commForm.setForm0312();
		commForm.setForm0313();
		commForm.setForm0402();
		commForm.setForm1102_2();
		commForm.setForm1305();
		commForm.setFormat();
		commForm.setOption();
		return commForm.getForm();
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
//		SMaxForm form = (SMaxForm)request.getSession().getAttribute(SMaxForm.SESSION_KEY);
		SMaxForm form = (SMaxForm)request.getAttribute("form");
		// アイテムマップ
		Map item = getItemMap(request);
		// ログイン情報
		LoginSession login =
			(LoginSession)request.getSession().getAttribute(LoginSession.SESSION_KEY);
			
		CommonItemFactory commItem = new CommonItemFactory(form, item);
		commItem.setItem0309();
		commItem.setItem0310();
		commItem.setItem0311();
		commItem.setItem0312();
		commItem.setItem0313();
		commItem.setItem0402();
		commItem.setItem1102_2();
		commItem.setItem1305();
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap("020504");
	}

}

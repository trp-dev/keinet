package jp.co.fj.keinavi.servlets.admin;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import jp.co.fj.keinavi.beans.admin.CompositeBean;
import jp.co.fj.keinavi.beans.admin.LastDateBean;
import jp.co.fj.keinavi.beans.admin.M302Bean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.admin.M302Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;


/**
 * 
 * 2005.04.05	Yoshimoto KAWAI - Totec
 * 				アクションフォームをrequestへセット
 * 
 * @author 二宮淳行
 * 
 */
public class M302Servlet extends DefaultHttpServlet implements ActionMode{

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 				 javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException, IOException {

		// ログイン情報
		final LoginSession login = getLoginSession(request);
		// アクションフォームの取得
		final M302Form form = (M302Form) super.getActionForm(request,
			"jp.co.fj.keinavi.forms.admin.M302Form");

		// JSP
		if ("m302".equals(form.getForward())) {

			final String[] id = StringUtils.split(form.getTargetClassgCd(), ',');
			
			Connection con = null;
			CompositeBean bean = null;
			try {
				con = getConnectionPool(request);

				bean = new CompositeBean();
				bean.setConnection(null, con);
				bean.setTargetClassgCd(id[0]);
				bean.setTargetYear(id[1]);
				bean.setTargetGrade(id[2]);
				bean.setSchoolCd(login.getUserID());
				bean.execute();
				
			} catch (final Exception e) {
				throw new ServletException(
						e.getClass() + ":" + e.getMessage(), e);
			} finally {
				releaseConnectionPool(request, con);
			}

			form.setTargetYear(id[1]);
			form.setTargetGrade(id[2]);
			
			request.setAttribute("form", form);
			request.setAttribute("CompositeBean", bean);
			
			forward2Jsp(request, response);

		// 不明なら転送
		} else {

			final String actionMode = form.getActionMode();
			
			Connection con = null;
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(false);
			
				// 登録
				if ((actionMode.equals(COMPOSI_UPDATE_MODE)
						|| actionMode.equals(REGIST_COMPOSI) )
						&& "m302".equals(form.getBackward())) {

					// ------------------------------------------------------------
					// 2004.11.14
					// Yoshimoto KAWAI - Totec
					// 更新日をセットする
					renewLastDate3(con, login.getUserID());
					// ------------------------------------------------------------		
					
					final String[] id = StringUtils.split(form.getTargetClassgCd(), ',');
					final M302Bean bean = new M302Bean();
					bean.setConn(con);
					bean.setActionMode(actionMode);
					bean.setGroupCode(id[0]);
					bean.setTargetYear(id[1]);
					bean.setTargetGrade(id[2]);
					bean.setPacakgeCode(login.getUserID());
					bean.setClassName(form.getClassName());
					bean.setComment(form.getComment());
					bean.setClassList(form.getClassList());
					bean.execute();
					
					if (bean.isJudgeAll()) {
						con.commit();
						// アクセスログ（登録）
						if (actionMode.equals(REGIST_COMPOSI)) {
							actionLog(request, "1001");
						}
						// アクセスログ（変更）
						if (actionMode.equals(COMPOSI_UPDATE_MODE)) {
							actionLog(request, "1002");
						}
					} else {
						con.rollback();
					}
				}

			} catch (final Exception e) {
				rollback(con);
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}
			
			dispatch(request, response);
		}
	}
	
	/**
	 * 複合クラスメニュー更新日をセットする
	 * 
	 * @param con
	 * @param schoolCd
	 * @throws SQLException
	 */
	protected void renewLastDate3(
			final Connection con, final String schoolCd) throws SQLException {
		
		LastDateBean.setLastDate3(con, schoolCd);
	}

}
/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.beans.sheet.NoQuestionException;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.data.sheet.SheetSchoolData;
import jp.co.fj.keinavi.excel.data.school.S43GakkoListBean;
import jp.co.fj.keinavi.excel.data.school.S43Item;
import jp.co.fj.keinavi.excel.data.school.S43ListBean;
import jp.co.fj.keinavi.excel.school.S43;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * 2009.10.09   Fujito URAKAWA - Totec
 *              「全体成績」追加対応
 * 
 * @author kawai
 *
 */
public class S43SheetBean extends AbstractSheetBean {

	// データクラス
	private final S43Item data = new S43Item();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		String[] code = super.getSubjectCDArray(); // 型・科目設定値
		List graph = super.getSubjectGraphList(); // 型・科目グラフ表示対象
		List compSchoolList = super.getCompSchoolList(); // 比較対象高校
		
		String tmp_kmkcd = "";	// 科目コードチェック用変数

		// 設問別成績存在チェック
		if (hasAllNoQuestionSubject(code)) {
			throw new NoQuestionException();
		}

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntHyouFlg(super.getIntFlag(IProfileItem.CHART)); // 表フラグ
		data.setIntGraphFlg(super.getIntFlag(IProfileItem.GRAPH)); // グラフフラグ
		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ

		PreparedStatement ps1 = null;
		PreparedStatement ps2a = null;
		PreparedStatement ps2b = null;
		PreparedStatement ps2c = null;
		PreparedStatement ps2d = null;
		PreparedStatement ps3 = null;	// 科目別人数リスト（全国）用カーソル
		PreparedStatement ps4 = null;	// 科目別人数リスト（県）用カーソル
		PreparedStatement ps5 = null;	// 科目別人数リスト（自校）用カーソル
		PreparedStatement ps6 = null;	// 科目別人数リスト（他校）用カーソル
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		try {
			// データリスト
			{
				Query query = QueryLoader.getInstance().load("s33_1");
				query.setStringArray(1, code); // 型・科目コード

				ps1 = conn.prepareStatement(query.toString());
				ps1.setString(1, exam.getExamYear()); // 模試年度
				ps1.setString(2, exam.getExamCD()); // 模試コード
			}

			// 設問別人数リスト（自校）
			ps2a = conn.prepareStatement(QueryLoader.getInstance().load("s33_2").toString());
			ps2a.setString(1, exam.getExamYear()); // 模試年度
			ps2a.setString(2, exam.getExamCD()); // 模試コード
			ps2a.setString(3, profile.getBundleCD()); // 一括コード		

			// 設問別人数リスト（全国）
			ps2b = conn.prepareStatement(QueryLoader.getInstance().load("s43_1").toString());
			ps2b.setString(1, exam.getExamYear()); // 模試年度
			ps2b.setString(2, exam.getExamCD()); // 模試コード
	
			// 設問別人数リスト（県）
			ps2c = conn.prepareStatement(QueryLoader.getInstance().load("s43_2").toString());
			ps2c.setString(1, exam.getExamYear()); // 模試年度
			ps2c.setString(2, exam.getExamCD()); // 模試コード
	
			// 設問別人数リスト（他校）
			{
				Query query = QueryLoader.getInstance().load("s43_3");
				super.outDate2InDate(query); // データ開放日
				
				ps2d = conn.prepareStatement(query.toString());
				ps2d.setString(1, exam.getExamYear()); // 模試年度
				ps2d.setString(2, exam.getExamCD()); // 模試コード
				ps2d.setString(6, exam.getExamYear()); // 模試年度
				ps2d.setString(7, exam.getExamCD()); // 模試コード
			}
			
			// 科目別人数リスト（全国）
			ps3 = conn.prepareStatement(QueryLoader.getInstance().load("s43_4").toString());
			ps3.setString(1, exam.getExamYear());
			ps3.setString(2, exam.getExamCD());
			ps3.setString(4, exam.getExamYear());
			ps3.setString(5, exam.getExamCD());
			
			// 科目別人数リスト（県）
			ps4 = conn.prepareStatement(QueryLoader.getInstance().load("s43_5").toString());
			ps4.setString(1, exam.getExamYear());
			ps4.setString(2, exam.getExamCD());
			
			// 科目別人数リスト（自校）
			ps5 = conn.prepareStatement(QueryLoader.getInstance().load("s43_6").toString());
			ps5.setString(1, exam.getExamYear());
			ps5.setString(2, exam.getExamCD());
			ps5.setString(3, profile.getBundleCD());
			
			// 科目別人数リスト（他校）
			{
				Query query = QueryLoader.getInstance().load("s43_7");
				super.outDate2InDate(query);
				
				ps6 = conn.prepareStatement(query.toString());
				ps6.setString(1, exam.getExamYear());
				ps6.setString(2, exam.getExamCD());
				ps6.setString(5, exam.getExamYear());
				ps6.setString(6, exam.getExamCD());
			}
			
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				if (!tmp_kmkcd.equals(rs1.getString(1))) {
					
					// 科目コードが異なる場合のみ、全体成績データをセットする
					putTotalSubjectData(rs1, ps3, ps4, ps5, ps6, graph, compSchoolList);
					
				}
				
				S43ListBean bean = new S43ListBean();
				bean.setStrKmkCd(rs1.getString(1));
				bean.setStrKmkmei(rs1.getString(2));
				bean.setIntDispKmkFlg(graph.contains(rs1.getString(1)) ? 1 : 0);
				bean.setStrHaitenKmk(rs1.getString(3));
				bean.setStrSetsuNo(rs1.getString(4));
				bean.setStrSetsuMei1(rs1.getString(5));
				bean.setStrSetsuHaiten(rs1.getString(6));
				data.getS43List().add(bean);

				// 科目コード
				ps2a.setString(4, bean.getStrKmkCd());
				ps2b.setString(3, bean.getStrKmkCd());
				ps2c.setString(3, bean.getStrKmkCd());
				ps2d.setString(3, bean.getStrKmkCd());
				// 設問番号
				ps2a.setInt(5, rs1.getInt(7));
				ps2b.setInt(4, rs1.getInt(7));
				ps2c.setInt(4, rs1.getInt(7));
				ps2d.setInt(4, rs1.getInt(7));
				
				// 自校
				{
					S43GakkoListBean g = new S43GakkoListBean();
					g.setStrGakkomei(profile.getBundleName());
					g.setIntDispGakkoFlg(1);
					bean.getS43GakkoList().add(g);

					rs2 = ps2a.executeQuery();

					if (rs2.next()) {
						g.setIntNinzu(rs2.getInt(1));
						g.setFloTokuritsu(rs2.getFloat(2));
					} else {
						g.setIntNinzu(-999);
						g.setFloTokuritsu(-999);
					}

					rs2.close();
				}

				// 比較対象高校分繰り返す
				Iterator ite = compSchoolList.iterator();
				while (ite.hasNext()) {
					SheetSchoolData school = (SheetSchoolData) ite.next();
		
					switch (school.getSchoolTypeCode()) {
						// 全国
						case 1:
							rs2 = ps2b.executeQuery();
							break;
							
						// 県
						case 2:
							ps2c.setString(5, school.getPrefCD()); // 県コード
							rs2 = ps2c.executeQuery();
							break;
						
						// 高校
						case 3:
							ps2d.setString(5, school.getSchoolCD()); // 一括コード
							rs2 = ps2d.executeQuery();
							break;
					}

					if (rs2.next()) {
						S43GakkoListBean g = new S43GakkoListBean();
						g.setStrGakkomei(rs2.getString(1));
						g.setIntDispGakkoFlg(school.getGraphFlag());
						g.setIntNinzu(rs2.getInt(2));
						g.setFloTokuritsu(rs2.getFloat(3));
						bean.getS43GakkoList().add(g);
					}
					rs2.close();
				}
				
				// 処理中の科目コードをセット
				tmp_kmkcd = rs1.getString(1);
			}
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2a);
			DbUtils.closeQuietly(ps2b);
			DbUtils.closeQuietly(ps2c);
			DbUtils.closeQuietly(ps2d);
			DbUtils.closeQuietly(ps3);
			DbUtils.closeQuietly(ps4);
			DbUtils.closeQuietly(ps5);
			DbUtils.closeQuietly(ps6);
		}
	}

	/**
	 * 設問別成績の先頭に「全体成績」データをセットする
	 * @param rs1
	 * @param ps3 科目別人数リスト（全国）
	 * @param ps4 科目別人数リスト（県）
	 * @param ps5 科目別人数リスト（自校）
	 * @param ps6 科目別人数リスト（他校）
	 * @param graph
	 * @param compSchoolList
	 * @throws SQLException
	 */
	private void putTotalSubjectData(ResultSet rs1,
			PreparedStatement ps3, PreparedStatement ps4, PreparedStatement ps5, PreparedStatement ps6, List graph,
			List compSchoolList) throws SQLException {
		
		S43ListBean bean = new S43ListBean();
		bean.setStrKmkCd(rs1.getString(1));
		bean.setStrKmkmei(rs1.getString(2));
		bean.setIntDispKmkFlg(graph.contains(rs1.getString(1)) ? 1 : 0);
		bean.setStrHaitenKmk(rs1.getString(3));
		bean.setStrSetsuNo("");							// 設問Noに「""」をセット
		bean.setStrSetsuMei1("全体成績");				// 設問名に「"全体成績"」をセット
		bean.setStrSetsuHaiten(rs1.getString(3));		// 配点に科目配点をセット
		data.getS43List().add(bean);
		
		ResultSet rs2 = null; // 自校
		ResultSet rs3 = null; // 他校
		
		try {
			// 科目コード
			ps3.setString(3, rs1.getString(1));	// 全国
			ps4.setString(3, rs1.getString(1));	// 県
			ps5.setString(4, rs1.getString(1));	// 自校
			ps6.setString(3, rs1.getString(1));	// 他校
			// 自校の科目別成績をセット
			{
				S43GakkoListBean g = new S43GakkoListBean();
				g.setStrGakkomei(profile.getBundleName());
				g.setIntDispGakkoFlg(1);
				bean.getS43GakkoList().add(g);
	
				rs2 = ps5.executeQuery();
	
				if (rs2.next()) {
					g.setIntNinzu(rs2.getInt(1));
					g.setFloTokuritsu(rs2.getFloat(2));
				} else {
					g.setIntNinzu(-999);
					g.setFloTokuritsu(-999);
				}
	
				rs2.close();
			}
			// 他校の科目別成績をセット
			{
				Iterator ite = compSchoolList.iterator();
				while (ite.hasNext()) {
					SheetSchoolData school = (SheetSchoolData) ite.next();
		
					switch (school.getSchoolTypeCode()) {
						// 全国
						case 1:
							rs3 = ps3.executeQuery();
							break;
							
						// 県
						case 2:
							ps4.setString(4, school.getPrefCD()); // 県コード
							rs3 = ps4.executeQuery();
							break;
						
						// 高校
						case 3:
							ps6.setString(4, school.getSchoolCD()); // 一括コード
							rs3 = ps6.executeQuery();
							break;
					}
	
					if (rs3.next()) {
						S43GakkoListBean g = new S43GakkoListBean();
						g.setStrGakkomei(rs3.getString(1));
						g.setIntDispGakkoFlg(school.getGraphFlag());
						g.setIntNinzu(rs3.getInt(2));
						g.setFloTokuritsu(rs3.getFloat(3));
						bean.getS43GakkoList().add(g);
					}
					rs3.close();
				}
			}
		}
		finally {
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(rs3);
		}
		
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S43_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_OTHER_QUE;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S43()
			.s43(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
	}

}

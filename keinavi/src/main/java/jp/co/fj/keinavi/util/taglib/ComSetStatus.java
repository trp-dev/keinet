/*
 * 作成日: 2004/07/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import jp.co.fj.keinavi.util.taglib.cmfactory.AbstractCMTagFactory;

/**
 * @author kawai
 *
 * 共通項目設定状況を表示するTaglib
 * 
 */
public class ComSetStatus extends TagSupport {

	private String item; // 出力項目
	private boolean temp; // 共通項目設定画面での値を評価するかどうか

	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {
		try {
			// Factory
			AbstractCMTagFactory factory = AbstractCMTagFactory.createAbstractCMTagFactory(item);
			
			// プロファイル設定画面
			if (temp) {
				pageContext.getOut().print(factory.createTempCMStatus(
						pageContext.getRequest(), pageContext.getSession()));
						
			// それ以外
			} else {
				pageContext.getOut().print(factory.createCMStatus(
						pageContext.getRequest(), pageContext.getSession()));
			}
		} catch (IOException e) {
			throw new JspException(e);
		}

		return EVAL_PAGE;
	}

	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	public int doStartTag() throws JspException {
		return SKIP_BODY;
	}

	/**
	 * @param string
	 */
	public void setItem(String string) {
		item = string;
	}

	/**
	 * @param b
	 */
	public void setTemp(boolean b) {
		temp = b;
	}

}

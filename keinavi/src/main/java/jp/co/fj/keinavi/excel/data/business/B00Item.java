package jp.co.fj.keinavi.excel.data.business;

import java.util.ArrayList;
/**
 * �Z�����ѕ��́|�S���������ъT���f�[�^�N���X
 * �쐬��: 2004/07/30
 * @author	Ito.Y
 */
public class B00Item {
	//�͎���
	private String strMshmei = "";
	//�󌱎ґ���
	private int intAllNinzu = 0;
	//����l��
	private int intGenNinzu = 0; 
	//�����l��
	private int intSotuNinzu = 0;
	//�͎��t���O
	private int intMshFlg = 0;
	//�^�ʐ��у��X�g
	ArrayList b00KataList = new ArrayList();
	//�Ȗڕʐ��у��X�g
	ArrayList b00KmkList = new ArrayList();
	//�o�͎�ʃt���O �� �V�e�X�g�p�ɒǉ�
	private int intShubetsuFlg = 0;

	
	/*----------*/
	/* Get      */
	/*----------*/
	
	public int getIntAllNinzu() {
		return this.intAllNinzu;
	}
	public int getIntGenNinzu() {
		return this.intGenNinzu;
	}
	public int getIntMshFlg() {
		return this.intMshFlg;
	}
	public int getIntSotuNinzu() {
		return this.intSotuNinzu;
	}
	public ArrayList getB00KataList() {
		return this.b00KataList;
	}
	public ArrayList getB00KmkList() {
		return this.b00KmkList;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setIntAllNinzu(int intAllNinzu) {
		this.intAllNinzu = intAllNinzu;
	}
	public void setIntGenNinzu(int intGenNinzu) {
		this.intGenNinzu = intGenNinzu;
	}
	public void setIntMshFlg(int intMshFlg) {
		this.intMshFlg = intMshFlg;
	}
	public void setIntSotuNinzu(int intSotuNinzu) {
		this.intSotuNinzu = intSotuNinzu;
	}
	public void setB00KataList(ArrayList b00KataList) {
		this.b00KataList = b00KataList;
	}
	public void setB00KmkList(ArrayList b00KmkList) {
		this.b00KmkList = b00KmkList;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}
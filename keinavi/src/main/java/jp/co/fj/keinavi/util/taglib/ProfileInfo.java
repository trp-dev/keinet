/*
 * 作成日: 2004/07/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.taglib;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import jp.co.fj.keinavi.beans.profile.ProfileFolderBean;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.data.profile.ProfileFolder;

/**
 * プロファイルに関する情報を表示するTaglib
 * 
 * @author kawai
 */
public class ProfileInfo extends TagSupport {

	private String item; // 出力項目
	private boolean deputy; // 営業（高校代行）で集計するかどうか

	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {
		String message = null; // メッセージ

		ProfileFolderBean bean =
			(ProfileFolderBean) pageContext.getRequest().getAttribute("ProfileFolderBean");			

		if (bean == null) throw new JspException("プロファイルフォルダBeanの取得に失敗しました。");
		
		// プロファイルフォルダの数
		if ("foldercount".equals(this.getItem())) {
			message = String.valueOf(this.countFolder(bean));

		// プロファイルの数（高校・営業・校舎作成）
		} else if ("profilecount".equals(this.getItem())){
			message = String.valueOf(this.countProfile(bean));		

		// エラー	
		} else {
			throw new JspException("出力項目の指定が不正です。");
		}
			
		try {
			pageContext.getOut().print(message);
		} catch (IOException e) {
			throw new JspException(e);
		}

		return EVAL_PAGE;
	}

	/**
	 * プロファイルフォルダの数をカウントする
	 * @return
	 */
	private int countFolder(ProfileFolderBean bean) {
		int count = 0;
		
		Iterator ite = bean.getFolderList().iterator();
		while (ite.hasNext()) {
			ProfileFolder folder = (ProfileFolder) ite.next();
			if (this.isDeputy() && folder.isDeputy()) count++;
			else if (!this.isDeputy() && !folder.isDeputy()) count++; 
		}
		
		return count;
	}

	/**
	 * プロファイルの数をカウントする
	 * @return
	 */
	private int countProfile(ProfileFolderBean bean) {
		int count = 0;
		
		Iterator ite1 = bean.getFolderList().iterator();
		while (ite1.hasNext()) {
			ProfileFolder folder = (ProfileFolder) ite1.next();

			Iterator ite2 = folder.getProfileList().iterator();
			while (ite2.hasNext()) {
				Profile profile = (Profile) ite2.next();
				
				if (this.isDeputy() && profile.isDeputy()) count++;
				else if (!this.isDeputy() && !profile.isDeputy()) count++; 
				
			}
		}
		
		return count;
	}

	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	public int doStartTag() throws JspException {
		return SKIP_BODY;
	}

	/**
	 * @param string
	 */
	public void setItem(String string) {
		item = string;
	}

	/**
	 * @return
	 */
	public boolean isDeputy() {
		return deputy;
	}

	/**
	 * @return
	 */
	public String getItem() {
		return item;
	}

	/**
	 * @param b
	 */
	public void setDeputy(boolean b) {
		deputy = b;
	}

}

package jp.co.fj.keinavi.beans.csv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.NoSuchElementException;
import java.util.Vector;

/**
 * CSVデータ読み込みクラス
 */
public class CSVReader {
	
	private static final char QUOTE 		= '"';
	private static final char COMMA 		= ',';
	private static final char BACKSLASH 	= '\\';
	private static final char TAB			= '\t';
	
	private String 		in;
	private StringReader 	reader;
	private BufferedReader br;
	private char				delimiter;	// 区切り文字
	private int					position;
	private int					maxPosition;
	private boolean				firstTime;
	private String[] 			items;		// 一行データ
	private Vector				vecItems;	// 一行データ格納Vector
	private String				str;		// 一行文字列
	
	
	/**
	 * CSVReader のインスタンスを生成。
	 * @param	in	インプットストリーム
	 */
	public CSVReader(String in) throws UnsupportedEncodingException{
		
		this.in 	= in;
		//reader	= new InputStreamReader(in);
		reader		= new StringReader(in);
		//reader      = new InputStreamReader( in, "SHIFT_JIS");
		br			= new BufferedReader(reader);
		position	= 0;
		str 		= "";
		delimiter	= COMMA;
		
	}
	
	
	/**
	 * 一行分の文字列を読み取りCSVLine クラスに設定。
	 */
	public CSVLine readLine()  throws Exception {
		
		vecItems 	= new Vector();	// データ格納用
		boolean flg = true;			// 読み込みフラグ(\n用)
		
		try {
			while(true) {
				
				int cnt = 0;
				
				if(flg) {
					str = br.readLine();
					
					// 取得した文字列がnullの場合
					if(str == null) {
						break;
					}
				}
				
				// "の数をカウント
				for(int i = 0; i < str.length(); i++) {
					char ch = str.charAt(i);
					
					if(ch == QUOTE) {
						cnt++;
					}
				}
				
				if(cnt % 2 != 0) {
					flg = false;
					String tmp;
					tmp = br.readLine();
					if( tmp != null ){
						str = str + "\n" + tmp;
					} else {
						str = str + "\n" + "\"";
						break;
					}
				} else {
					break;
				}
			}
			
			
			if(str != null) {
				// タブ文字が含まれていた場合
				str = str.replaceAll(String.valueOf(TAB), "\t");
				position	= 0;
				maxPosition = str.length();
				firstTime 	= true;
				
				while(position < maxPosition) {
					vecItems.add(nextToken());
				}
				
				items = (String[])vecItems.toArray(new String[vecItems.size()]);
			}
			
		} catch(Exception ex) {
			
			throw ex;
		}
		
		if(str != null){
		
			// 戻り値を返却
			return new CSVLine(items);
		}else{
			return null;
		}
	}
	
	
	/**
	 * 区切り文字を設定。
	 * @param	s	区切り文字
	 */
	public void setDelimiter(char ch) {
		delimiter = ch;
	}
	
	
	/**
	 * ストリームに関連するすべてのシステムリソースを解放。
	 */
	public void close() throws IOException  {
		try {
			reader.close();
			br.close();
		} catch (IOException  ie) {
			throw ie;
			//System.out.println("ERROR[close]：" + ex.toString());
		}
	}
	
	
	/**
	 * 次のカンマがある位置を返す。
	 * 見つからなかった場合は nextComma() == maxPosition となる。
	 * @param	index	検索を開始する位置
	 * @return	次のカンマがある位置。カンマがない場合は、文字列
	 * 			の長さの値
	 */
	private int nextComma(int index) {
		boolean insideQuote = false;
		while (index < maxPosition) {
			
			char ch = str.charAt(index);
			if (!insideQuote && ch == delimiter) {
				break;
			} else if (ch == QUOTE && !(index - 1 > 0 && str.charAt(index - 1) == '\\')) {
				insideQuote = !insideQuote;
			}
			
			index++;
		}
		return index;
	}
	
	
	/**
	 * 次の項目を返す。
	 * @return 次の項目
	 * @exception NoSuchElementException 項目が残っていない
	 */
	public String nextToken() {
		boolean insideQuote = false;
		CSVLine csvLine = new CSVLine();
		
		if (position >= maxPosition) {
			throw new NoSuchElementException("The item does not remain.");
		}
		
		if (!firstTime) {
			position++;
		}
		
		int current = position;
		firstTime 	= false;
		
		if (current >= maxPosition) {
			return "";
		}
		
		position = nextComma(position);
		
		StringBuffer buf = new StringBuffer(position - current);
		
		if (str.charAt(current) == QUOTE) {
			insideQuote = true;
			current++;
		}
		
		while (current < position) {
			char ch = str.charAt(current++);
			
			if (QUOTE == ch) {
				if (current < position && QUOTE == str.charAt(current)) {
					current ++;
				}
			}
			buf.append(ch);
		}
		
  		if (insideQuote && (buf.charAt(buf.length() - 1) == QUOTE) ) {
      		// ダブルクォートの前にスラッシュがあったらダブルクォートを削除しない
      		if((buf.length() <= 1) || (buf.charAt(buf.length() - 2) != BACKSLASH)){
        		buf.setLength(buf.length() - 1);
      		}
		}
		
		
		return csvLine.unwrap(buf.toString());
	}
	
	public static void main(String[] args) {
		try{
		CSVReader reader = new CSVReader("aaaaaaaaaaa,adadfa,afdafa,afda");
		
		CSVLine line = reader.readLine();
		
		String[] items = line.getItems();
		
		for( int i=0; i<items.length;i++){
			System.out.println(items[i]);
		}
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}
}
package jp.co.fj.keinavi.servlets.profile;

import java.io.IOException;
import java.io.InvalidClassException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.ExamBean;
import jp.co.fj.keinavi.beans.news.InformListBean;
import jp.co.fj.keinavi.beans.profile.CountingDivBean;
import jp.co.fj.keinavi.beans.profile.ProfileBean;
import jp.co.fj.keinavi.beans.profile.ProfileDeleteBean;
import jp.co.fj.keinavi.beans.profile.ProfileFolderBean;
import jp.co.fj.keinavi.beans.profile.ProfileFolderDeleteBean;
import jp.co.fj.keinavi.beans.profile.ProfileTransactionBean;
import jp.co.fj.keinavi.beans.profile.StandardProfileBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.CountingDivData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.profile.W002Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.message.MessageLoader;

/**
 * 
 * プロファイル選択画面サーブレット
 * 
 * 
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 * 
 * 
 * @author kawai
 * 
 */
public class W002Servlet extends DefaultHttpServlet {

	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// プロファイルセッションはクリア
		request.getSession(false).removeAttribute(Profile.SESSION_KEY);

		// アクションフォーム
		W002Form form = (W002Form)super.getActionForm(request,
			"jp.co.fj.keinavi.forms.profile.W002Form");

		// プロファイル選択画面またはエラーでの遷移
		if ("w002".equals(getForward(request))
			|| super.getErrorMessage(request) != null) {

			// セッション
			HttpSession session = request.getSession(false);
			// ログイン情報
			LoginSession login = super.getLoginSession(request);

			// プロファイルセッションは不要
			session.removeAttribute(Profile.SESSION_KEY);

			// 共通項目設定のフォームをセッションに入れておく
			session.setAttribute(CM001Form.SESSION_KEY, new CM001Form());

			// DBコネクション
			Connection con = null;
			try {
				con = super.getConnectionPool(request);
				con.setAutoCommit(false);

				// フォルダ削除
				if ("1".equals(form.getOperation())) {
					ProfileFolderDeleteBean bean = new ProfileFolderDeleteBean();
					bean.setConnection(null, con);
					bean.setFolderID(form.getCurrent());
					bean.setBundleCD(form.getCountingDivCode());
					bean.setLoginSession(login);
					bean.execute();
					// アクセスログ『プロファイルフォルダ削除』
					actionLog(request, "212", form.getCurrent());
				// プロファイル削除
				} else if ("3".equals(form.getOperation())) {
					ProfileDeleteBean bean = new ProfileDeleteBean();
					bean.setConnection(null, con);
					bean.setProfileID(form.getProfileID());
					bean.setBundleCD(form.getCountingDivCode());
					bean.setLoginSession(login);
					bean.execute();
					// アクセスログ『プロファイル削除』
					actionLog(request, "205", form.getProfileID());
				}

				// 集計区分Beanを作る
				if (login.isCountingDiv()) {
					CountingDivBean bean = new CountingDivBean();
					bean.setConnection(null, con);
					bean.setSchoolCD(login.getUserID());
					bean.execute();
					request.setAttribute("CountingDivBean", bean);

					// 集計区分の初期値
					if (form.getCountingDivCode() == null) {
						// 最初の集計区分を入れておく
						form.setCountingDivCode(
							((CountingDivData) bean.getCountingDivList().get(0))
								.getCountingDivCode()
						);
					}

					// 集計区分をログイン情報に保存
					int index = bean.getCountingDivList().indexOf(
							new CountingDivData(form.getCountingDivCode()));

					login.setBundleCd(form.getCountingDivCode());
					login.setBundleName(
						((CountingDivData)bean.getCountingDivList().get(index)).getCountingDivName());
				}
					
				// アクセスログ『親／副コード切替』
				if ("1".equals(form.getChangeCountingDivFlag())) {
					actionLog(request, "221", form.getCountingDivCode());
				}
				
				// プロファイルフォルダBean
				ProfileFolderBean bean = new ProfileFolderBean();
				bean.setConnection(null, con);
				bean.setLoginSession(login);
				bean.setBundleCD(form.getCountingDivCode());
				bean.execute();
				request.setAttribute("ProfileFolderBean", bean);

				//---------------------------------------------------
				// お知らせリスト作成
				//---------------------------------------------------
				InformListBean infbean = new InformListBean();
				infbean.setConnection(null, con);
				infbean.setUserID(login.getUserID()); 		// ユーザID
				infbean.setUserMode(login.getUserMode()); 	// ユーザーモード
				infbean.setDisplayDiv("0"); 				// 表示区分 "0":お知らせ
				infbean.execute();
				request.setAttribute("InformListBean", infbean);
				//---------------------------------------------------

				//---------------------------------------------------
				// Kei-Net注目情報リスト作成
				//---------------------------------------------------
				InformListBean keibean = new InformListBean();
				keibean.setConnection(null, con);
				keibean.setUserID(login.getUserID()); 		// ログインユーザID
				keibean.setUserMode(login.getUserMode()); 	// ユーザーモード
				keibean.setDisplayDiv("1"); 				// 表示区分 "1":Kei-Net注目情報
				keibean.execute();
				request.setAttribute("KeinetListBean", keibean);
				//---------------------------------------------------

				con.commit();
			} catch (final Exception e) {
				rollback(con);
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			// f001から来た場合には体験版
			if ("f001".equals(super.getBackward(request))) {
				login.setTrialMode((short) 1);
				session.setAttribute(LoginSession.SESSION_KEY, login);
			}
			
			//模試受付システム用情報作成
			createMoshiSys(request);

			// アクションフォーム
			request.setAttribute("form", form);

			super.forward(request, response, JSP_W002);

		// 不明なら転送
		} else {
			
			// 各メニュートップ画面へ遷移するための準備
			if ("w008".equals(getForward(request))
					|| "s001".equals(getForward(request))
					|| "c001".equals(getForward(request))
					|| "i001".equals(getForward(request))
					|| "t001".equals(getForward(request))) {

				try {
					prepare(request, form);
				// 模試が見つからない場合に発生
				// プロファイル選択画面に戻る
				} catch (final KNServletException e) {
					setErrorMessage(request, e);
					forward(request, response, "/ProfileSelect");
					return;
				}
				
				// アクセスログ『プロファイル選択』
				final Profile profile = getProfile(request);
				actionLog(request, "201",
						profile.getProfileID() + "," + profile.getBundleCD());
			}
			
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

	// トップ画面遷移の準備処理
	private void prepare(final HttpServletRequest request,
			final W002Form form) throws ServletException, IOException {

		// セッション
		final HttpSession session = request.getSession(false);
		// ログイン情報
		final LoginSession login = getLoginSession(request);

		// DBコネクション
		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);
			
			// プロファイル取得
			final Profile profile = getProfile(con, login, form);
			// セッションに入れる
			profile.setDbKey(login.getDbKey());
			session.setAttribute(Profile.SESSION_KEY, profile);
			// 模試セッション
			final ExamSession es = createExamSession(con, login, profile);
			session.setAttribute(ExamSession.SESSION_KEY, es);
			
			// 対象模試のデフォルト
			final ExamData exam = getDefaultExamData(profile, es);

			// 対象模試を保持する
			profile.setTargetYear(exam.getExamYear());
			profile.setTargetExam(exam.getExamCD());
			form.setTargetYear(profile.getTargetYear());
			form.setTargetExam(profile.getTargetExam());
			
			con.commit();
			
		// バージョンエラー
		} catch (final InvalidClassException e) {
			rollback(con);
			
			KNServletException k = new KNServletException(e.getMessage());
			k.initCause(e);
			k.setErrorCode("10W002069999");
			k.setErrorMessage(
					"プロファイルのバージョンエラーが発生しました。"
					+ "プロファイルを作成し直してください。");
			throw k;

		// 制限数オーバー
		} catch (final KNServletException e) {
			rollback(con);
			
			final KNServletException k = new KNServletException(
					e.getErrorMessage());
			
			// データなし
			if ("10W002990001".equals(e.getErrorCode())) {
				k.setErrorMessage(e.getErrorMessage());
			// フォルダ制限オーバー			
			} else if ("10W002999999".equals(e.getErrorCode())) {
				k.setErrorMessage(MessageLoader.getInstance().getMessage("w032a"));
			// プロファイル制限オーバー
			} else {
				k.setErrorMessage(MessageLoader.getInstance().getMessage("w031a"));
			}
			
			k.setErrorCode(e.getErrorCode());
			
			throw k;
		
		// その他の例外
		} catch (final Exception e) {
			rollback(con);
			throw createServletException(e);
			
		// コネクション解放
		} finally {
			releaseConnectionPool(request, con);
		}
	}

	// プロファイルを取得する
	private Profile getProfile(final Connection con,
			final LoginSession login,
			final W002Form form) throws Exception {
		
		// プロファイルが選択されていないなら
		// 標準プロファイル（ひな型プロファイル）を使う
		boolean isTempleate = "".equals(form.getProfileID());
		if (isTempleate) {
			StandardProfileBean bean = new StandardProfileBean();
			bean.setConnection(null, con);
			bean.setLoginSession(login);
			bean.setBundleCd(form.getCountingDivCode());
			bean.execute();
			form.setProfileID(bean.getProfileId());
		}
			
		// DBから取得する
		ProfileBean bean = new ProfileBean();
		bean.setConnection(null, con);
		bean.setLoginSession(login);
		bean.setProfileID(form.getProfileID());
		bean.setBundleCD(form.getCountingDivCode());
		bean.setRegardable(true);
		bean.execute();
		
		final Profile profile =  bean.getProfile();
		
		// ひな型ならフラグを立てる
		if (isTempleate) {
			profile.setTemplate();
		}
		
		// 上書き禁止モードに設定する
		if (profile.getOverwriteMode() == 0) {
			ProfileTransactionBean tran = new ProfileTransactionBean();
			tran.setConnection(null, con);
			tran.setProfileID(profile.getProfileID());
			tran.setMode("1");
			tran.execute();
		}
		
		return profile;
	}
	
	// 模試情報Beanを作る
	private ExamSession createExamSession(
			final Connection con, final LoginSession login,
			final Profile profile) throws Exception {
		
		ExamBean bean = new ExamBean();
		bean.setConnection(null, con);
		bean.setLoginSession(login);
		bean.setBundleCD(profile.getBundleCD());
		bean.execute();
		return bean.getExamSession();
	}

}

package jp.co.fj.keinavi.excel.business;

/**
 * �Z�����ѕ��́|�S���������ъT���E�Ȗڕʐ��у��X�g
 * �쐬��: 2004/08/20
 * @author	Ito.Y
 */
public class B43BestListBean {
	//�΍��l
	private float	floTokuritsu = 0;
	//�u�b�N���
	private int	intBookNum	= 0;
	//�V�[�g���
	private int	intSheetNum	= 0;
	//�ʒu���
	private int	intRow		= 0;

	/*----------*/
	/* Get      */
	/*----------*/
	public int getIntRow() {
		return intRow;
	}
	public int getIntSheetNum() {
		return intSheetNum;
	}
	public int getIntBookNum() {
		return intBookNum;
	}
	public float getFloTokuritsu() {
		return floTokuritsu;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setIntRow(int i) {
		intRow = i;
	}
	public void setIntSheetNum(int i) {
		intSheetNum = i;
	}
	public void setIntBookNum(int i) {
		intBookNum = i;
	}
	public void setFloTokuritsu(float f) {
		floTokuritsu = f;
	}

}
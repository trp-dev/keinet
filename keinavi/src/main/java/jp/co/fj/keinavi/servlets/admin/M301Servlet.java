package jp.co.fj.keinavi.servlets.admin;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import jp.co.fj.keinavi.beans.admin.CompositeBean;
import jp.co.fj.keinavi.beans.admin.LastDateBean;
import jp.co.fj.keinavi.beans.admin.M301Bean;
import jp.co.fj.keinavi.beans.admin.MGradeDataBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.admin.MGradeData;
import jp.co.fj.keinavi.forms.admin.M301Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 * 
 * 2004.08.31	[新規作成]
 * 
 *
 * @author Administrator
 * @version 1.0
 * 
 */
public class M301Servlet extends DefaultHttpServlet implements ActionMode {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 				 javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException, IOException {

		// アクションフォームの取得
		final M301Form form = (M301Form) getActionForm(request,
				"jp.co.fj.keinavi.forms.admin.M301Form");

		// 複合クラス管理以外からの遷移の場合は
		// フォーム値は初期化する
		if (!getBackward(request).startsWith("m3")) {
			form.setTargetYear(null);
		}
		
		// JSP
		if ("m301".equals(form.getForward())) {
			
			// ログイン情報
			final LoginSession login = getLoginSession(request);
			
			Connection con = null;
			CompositeBean bean = null;
			MGradeDataBean combo = null;
			try {
				con = super.getConnectionPool(request);
				con.setAutoCommit(false);

				// コンボデータ
				combo = new MGradeDataBean();
				combo.setConnection(null, con);
				combo.setSchoolCD(login.getUserID());
				combo.execute();

				// フォーム初期化
				if (StringUtils.isEmpty(form.getTargetYear())) {
					// コンボデータがある場合
					if (combo.getGradeDataList().size() > 0) {
						MGradeData data = (MGradeData) combo.getGradeDataList().get(0);						
						form.setTargetYear(data.getYear());
						form.setTargetGrade(data.getGrade());
						
					// ない場合はとりあえず0で初期化しておく
					} else {
						form.setTargetYear("0");						
						form.setTargetGrade("0");
					}
				}

				// 削除
				if (COMPOSI_DELETE_MODE.equals(form.getActionMode())
						&& "m301".equals(form.getBackward())) {
					delete(con, login.getUserID(), form);
					actionLog(request, "1003");
				}

				bean = new CompositeBean();
				bean.setConnection(null, con);
				bean.setTargetYear(form.getTargetYear());
				bean.setTargetGrade(form.getTargetGrade());
				bean.setSchoolCd(login.getUserID());
				bean.execute();
				
				con.commit();

			} catch (final Exception e) {
				rollback(con);
				throw new ServletException(
						e.getClass() + ":" + e.getMessage(), e);
			} finally {
				releaseConnectionPool(request, con);
			}

			request.setAttribute("form", form);
			request.setAttribute("Combo", combo);
			request.setAttribute("CompositeBean", bean);

			forward2Jsp(request, response);
			
		// 不明なら転送
		} else {
			dispatch(request, response);
		}
	}
	
	private void delete(final Connection con, final String schoolCd,
			final M301Form form) throws Exception {
		
		// データ更新日
		renewLastDate3(con, schoolCd);
		
		final String[] id = StringUtils.split(form.getTargetClassgCd(), ',');
		final M301Bean bean = new M301Bean();
		bean.setConnection(null, con);
		bean.setSchoolCd(schoolCd);
		bean.setTargetClassGCd(id[0]);
		bean.setTargetYear(id[1]);
		bean.setTargetGrade(id[2]);
		bean.execute();
	}
	
	/**
	 * 複合クラスメニュー更新日をセットする
	 * 
	 * @param con
	 * @param schoolCd
	 * @throws SQLException
	 */
	protected void renewLastDate3(
			final Connection con, final String schoolCd) throws SQLException {
		
		LastDateBean.setLastDate3(con, schoolCd);
	}
	
}

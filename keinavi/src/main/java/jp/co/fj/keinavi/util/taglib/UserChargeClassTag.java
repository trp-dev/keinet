package jp.co.fj.keinavi.util.taglib;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import jp.co.fj.keinavi.data.user.LoginUserData;
import jp.co.fj.keinavi.data.user.UserClassData;

/**
 *
 * 利用者の担当クラス設定状況を出力するTaglib
 * 
 * 2005.10.12	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class UserChargeClassTag extends TagSupport {

	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {
		
		// 利用者データ
		final LoginUserData data = (LoginUserData) pageContext.getAttribute("user");
		
		// 出力メッセージ
		final StringBuffer buff = new StringBuffer();
		
		// 無制限
		if (data.getChargeClassList().size() == 0) {
			buff.append("制限なし");
			
		} else {
			
			buff.append("<nobr>");
			
			String preYear = null;
			int preGrade = -1;
			
			for (final Iterator ite = data.getChargeClassList().iterator(); ite.hasNext();) {
				final UserClassData c = (UserClassData) ite.next();
				
				// 年度切り替わり
				if (!c.getYear().equals(preYear)) {
					if (preYear != null) {
						buff.append("<br>");
					}
					buff.append("<b>" + c.getYear() + "年度</b>");
				}
				
				// 学年切り替わり
				if (!c.getYear().equals(preYear)
						|| c.getGrade() != preGrade) {
					buff.append("<br>" + c.getGrade() + "年");
				} else {
					buff.append("・<wbr>");
				}
				
				// クラス
				buff.append(c.getClassName() + "クラス");

				preYear = c.getYear();
				preGrade = c.getGrade();
			}
			
			buff.append("</nobr>");
		}
			
		try {
			pageContext.getOut().print(buff.toString());
		} catch (IOException e) {
			throw new JspException(e);
		}

		return EVAL_PAGE;
	}

	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	public int doStartTag() throws JspException {
		return SKIP_BODY;
	}
}

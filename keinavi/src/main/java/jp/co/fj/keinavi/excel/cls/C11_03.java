/**
 * クラス成績分析−クラス成績概況−偏差値分布（人数積み上げグラフ2.5ピッチ）
 * 	Excelファイル編集
 * 作成日: 2004/09/03
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.cls;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C11BnpListBean;
import jp.co.fj.keinavi.excel.data.cls.C11Item;
import jp.co.fj.keinavi.excel.data.cls.C11KmkDataListBean;
import jp.co.fj.keinavi.excel.data.cls.C11ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class C11_03 {

	private int noerror	= 0;		// 正常終了
	private int errfread	= 1;		// ファイルreadエラー
	private int errfwrite	= 2;		// ファイルwriteエラー
	private int errfdata	= 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	final private String	masterfile0	= "C11_03";		// ファイル名
	final private String	masterfile1	= "NC11_03";	// ファイル名
	private String	masterfile	= "";					// ファイル名


	/*
	 * 	Excel編集メイン
	 * 		C11Item c11Item: データクラス
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
 	public int c11_03EditExcel(C11Item c11Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("C11_03","C11_03帳票作成開始","");
		
		//テンプレートの決定
		if (c11Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			// マスタExcel読み込み
			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;
		
			// 基本ファイルを読込む
			C11ListBean c11ListBean = new C11ListBean();
			
			// データセット
			ArrayList	c11List			= c11Item.getC11List();
			Iterator	itr				= c11List.iterator();
			int		row				= 0;	// 行
			int		col				= 0;	// 行
			int		maxSheetIndex	= 0;	// シートカウンター
			int		maxSheetNum		= 40;	// シートカウンター
			int		fileIndex		= 1;	// ファイルカウンター
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
			boolean	kataFlg			= true;// false:改行済(型・科目名変更済)　
			

			/** 学年・クラス情報 **/
			// c11Listのセット
			while( itr.hasNext() ) {
				c11ListBean = (C11ListBean)itr.next();

				//学年・クラスが変わる時
				row=0;
				col=0;
				bolSheetCngFlg = true;
				if(maxSheetIndex>=maxSheetNum){
					bolBookCngFlg = true;
				}

				// 基本ファイルを読み込む
				C11KmkDataListBean	c11KmkDataListBean	= new C11KmkDataListBean();

				//データの保持
				ArrayList	c11KmkDataList	= c11ListBean.getC11KmkDataList();
				Iterator	itrC11KmkData	= c11KmkDataList.iterator();

				/** 型・科目Data **/
				kataFlg	= true;
				while(itrC11KmkData.hasNext()){
					c11KmkDataListBean = (C11KmkDataListBean) itrC11KmkData.next();

					//グラフ表示の型・科目のみ処理をする
					if (c11KmkDataListBean.getIntDispKmkFlg() ==1 ) {
						log.It("C11_03","グラフあり：",c11KmkDataListBean.getStrKmkmei());

						//型から科目に変わる時のチェック
						if ( c11KmkDataListBean.getStrKmkCd().compareTo("7000") < 0 ) {
							//型・科目切替えの初回のみ処理する
							if (kataFlg){
								if((col!=0)){
									col=0;
									bolSheetCngFlg = true;
									if(maxSheetIndex>=maxSheetNum){
										bolBookCngFlg = true;
									}
									kataFlg	= false;
								}else{
									kataFlg	= false;
								}
							}
						}
	
						// 基本ファイルを読み込む
						C11BnpListBean	c11BnpListBean	= new C11BnpListBean();
	
						//データの保持
						ArrayList	c12BnpList	= c11KmkDataListBean.getC11BnpList();
						Iterator	itrC11Bnp	= c12BnpList.iterator();
	
	
						//マスタExcel読み込み
						if(bolBookCngFlg){
							if(maxSheetIndex>=maxSheetNum){
			
								// Excelファイル保存
								boolean bolRet = false;
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, maxSheetIndex);
								fileIndex++;
	
								if( bolRet == false ){
									return errfwrite;					
								}
							}
	
							if((maxSheetIndex>=maxSheetNum)||(maxSheetIndex==0)){
								workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
								if( workbook==null ){
									return errfread;
								}
								bolBookCngFlg = false;
								maxSheetIndex=0;
							}
						}
	
						if ( bolSheetCngFlg ) {
		
							// データセットするシートの選択
							workSheet = workbook.getSheet( Integer.toString(maxSheetIndex+1) );
							maxSheetIndex++;
	
							// ヘッダ右側に帳票作成日時を表示する
							cm.setHeader(workbook, workSheet);
	
							// セキュリティスタンプセット
							String secuFlg = cm.setSecurity( workbook, workSheet, c11Item.getIntSecuFlg() ,38 ,40 );
							workCell = cm.setCell( workSheet, workRow, workCell, 0, 38 );
							workCell.setCellValue(secuFlg);
	
							// 学校名セット
							String strGakkoMei	="学校名　：" + cm.toString(c11Item.getStrGakkomei()) 
												+ "　学年：" + cm.toString(c11ListBean.getStrGrade()) + "　クラス名：" + cm.toString(c11ListBean.getStrClass());
							workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
							workCell.setCellValue( strGakkoMei );
	
							// 対象模試セット
							String moshi =cm.setTaisyouMoshi( c11Item.getStrMshDate() );	// 模試月取得
							workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
							workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(c11Item.getStrMshmei()) + moshi);
	
							//　改シートフラグをFalseにする
							bolSheetCngFlg	=false;
							col=0;
						}
	
						// 型・科目名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 35, 1+2*col );
						workCell.setCellValue( c11KmkDataListBean.getStrKmkmei() );
	
						// 配点セット
						if(!cm.toString(c11KmkDataListBean.getStrHaitenKmk()).equals("")){
							workCell = cm.setCell( workSheet, workRow, workCell, 36, 1+2*col );
							workCell.setCellValue( "(" + cm.toString(c11KmkDataListBean.getStrHaitenKmk()) + ")" );
						}
	
						// 合計人数(クラス)セット
						if(c11KmkDataListBean.getIntNinzuCls() != -999){
							workCell = cm.setCell( workSheet, workRow, workCell, 57, 1+2*col );
							workCell.setCellValue( c11KmkDataListBean.getIntNinzuCls() );
						}
	
						// 平均点(クラス)セット
						if(c11KmkDataListBean.getFloHeikinCls() != -999.0){
							workCell = cm.setCell( workSheet, workRow, workCell, 58, 1+2*col );
							workCell.setCellValue( c11KmkDataListBean.getFloHeikinCls() );
						}
	
						// 平均偏差(クラス)セット
						if(c11KmkDataListBean.getFloHensaCls() != -999.0){
							workCell = cm.setCell( workSheet, workRow, workCell, 59, 1+2*col );
							workCell.setCellValue( c11KmkDataListBean.getFloHensaCls() );
						}
	
						/** 偏差値分布Data **/
						// 変数の初期化
						row = 0;
						int setRow = -1;
						int ninzu =0 ;
						while(itrC11Bnp.hasNext()){
							c11BnpListBean = (C11BnpListBean) itrC11Bnp.next();
	
	
							// 人数セット
							if(c11BnpListBean.getIntNinzu() != -999){
								workCell = cm.setCell( workSheet, workRow, workCell, 38+row, 2+2*col );
								workCell.setCellValue( c11BnpListBean.getIntNinzu() );
	
								// *セット準備
								if( c11BnpListBean.getIntNinzu() > ninzu ){
									ninzu = c11BnpListBean.getIntNinzu();
									setRow = row;
								}
							}
							row++;
	
							// *セット
							if(row >= 19){
								if(setRow!=-1){
									workCell = cm.setCell( workSheet, workRow, workCell, 38+setRow, 1+2*col );
									workCell.setCellValue("*");
								}
								ninzu=0;
								setRow=-1;
							}
						}
						col++;
						if(col >= 20){
							col=0;
							bolSheetCngFlg =true;
							if(maxSheetIndex>=maxSheetNum){
								bolBookCngFlg = true;
							}
						}
					}else{
						log.It("C11_03","グラフなし：",c11KmkDataListBean.getStrKmkmei());
					}
				}
			}
		
			// Excelファイル保存
			boolean bolRet = false;
			if(fileIndex != 1){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, maxSheetIndex);
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("C11_03","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("C11_03","C11_03帳票作成終了","");
		return noerror;
	}
}
/**
 * 校内成績分析−過回比較　成績概況（過回比較）
 * 出力する帳票の判断
 * 作成日: 2004/08/11
 * @author	T.Sakai
 */
 
package jp.co.fj.keinavi.excel.school;

import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.util.log.*;

public class S51 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean s51( S51Item s51Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		try{
			int ret = 0;
			//S51Itemから各帳票を出力
			if (s51Item.getIntHyouFlg()==0 && s51Item.getIntGraphFlg()==0 && s51Item.getIntNendoFlg()==0 && s51Item.getIntClassFlg()==0 && s51Item.getIntTakouFlg()==0 ){
				throw new Exception("S51 ERROR : フラグ異常 ");
			}
			if ( s51Item.getIntHyouFlg()==1 ) {
				log.Ep("S51_01","S51_01帳票作成開始","");
				S51_01 exceledit = new S51_01();
				ret = exceledit.s51_01EditExcel( s51Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S51_01","帳票作成エラー","");
					return false;
				}
				log.Ep("S51_01","S51_01帳票作成終了","");
				sheetLog.add("S51_01");
			}
			if ( s51Item.getIntGraphFlg()==1 ) {
				log.Ep("S51_02","S51_02帳票作成開始","");
				S51_02 exceledit = new S51_02();
				ret = exceledit.s51_02EditExcel( s51Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S51_02","帳票作成エラー","");
					return false;
				}
				log.Ep("S51_02","S51_02帳票作成終了","");
				sheetLog.add("S51_02");
			}
			if ( s51Item.getIntNendoFlg()==1 && ( s51Item.getIntHyouFlg()==1 || ( s51Item.getIntHyouFlg()==0 && s51Item.getIntGraphFlg()==0 ) ) ) {
				log.Ep("S51_03","S51_03帳票作成開始","");
				S51_03 exceledit = new S51_03();
				ret = exceledit.s51_03EditExcel( s51Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S51_03","帳票作成エラー","");
					return false;
				}
				log.Ep("S51_03","S51_03帳票作成終了","");
				sheetLog.add("S51_03");
			}
			if ( s51Item.getIntGraphFlg()==1 && s51Item.getIntNendoFlg()==1 ) {
				log.Ep("S51_04","S51_04帳票作成開始","");
				S51_04 exceledit = new S51_04();
				ret = exceledit.s51_04EditExcel( s51Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S51_04","帳票作成エラー","");
					return false;
				}
				log.Ep("S51_04","S51_04帳票作成終了","");
				sheetLog.add("S51_04");
			}
			if ( s51Item.getIntClassFlg()==1 && ( s51Item.getIntHyouFlg()==1 || ( s51Item.getIntHyouFlg()==0 && s51Item.getIntGraphFlg()==0 ) ) ) {
				log.Ep("S51_05","S51_05帳票作成開始","");
				S51_05 exceledit = new S51_05();
				ret = exceledit.s51_05EditExcel( s51Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S51_05","帳票作成エラー","");
					return false;
				}
				log.Ep("S51_05","S51_05帳票作成終了","");
				sheetLog.add("S51_05");
			}
			if ( s51Item.getIntGraphFlg()==1 && s51Item.getIntClassFlg()==1 ) {
				log.Ep("S51_06","S51_06帳票作成開始","");
				S51_06 exceledit = new S51_06();
				ret = exceledit.s51_06EditExcel( s51Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S51_06","帳票作成エラー","");
					return false;
				}
				log.Ep("S51_06","S51_06帳票作成終了","");
				sheetLog.add("S51_06");
			}
			if ( s51Item.getIntTakouFlg()==1 && ( s51Item.getIntHyouFlg()==1 || ( s51Item.getIntHyouFlg()==0 && s51Item.getIntGraphFlg()==0 ) ) ) {
				log.Ep("S51_07","S51_07帳票作成開始","");
				S51_07 exceledit = new S51_07();
				ret = exceledit.s51_07EditExcel( s51Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S51_07","帳票作成エラー","");
					return false;
				}
				log.Ep("S51_07","S51_07帳票作成終了","");
				sheetLog.add("S51_07");
			}
			if ( s51Item.getIntGraphFlg()==1 && s51Item.getIntTakouFlg()==1 ) {
				log.Ep("S51_08","S51_08帳票作成開始","");
				S51_08 exceledit = new S51_08();
				ret = exceledit.s51_08EditExcel( s51Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S51_08","帳票作成エラー","");
					return false;
				}
				log.Ep("S51_08","S51_08帳票作成終了","");
				sheetLog.add("S51_08");
			}
			
		} catch(Exception e) {
			log.Err("99S51","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
package jp.co.fj.keinavi.beans;


/**
 *
 * Bean例外
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class KNBeanException extends Exception {

	// エラーコード
	private final String errorCode;

	/**
	 * コンストラクタ
	 */
	public KNBeanException() {
		super();
		this.errorCode = "0";
	}

	/**
	 * コンストラクタ
	 * 
	 * @param message
	 */
	public KNBeanException(final String message) {
		super(message);
		this.errorCode = "0";
	}

	/**
	 * コンストラクタ
	 * 
	 * @param message
	 * @param errorCode
	 */
	public KNBeanException(final String message, final String errorCode) {
		super(message);
		this.errorCode = errorCode;
	}
	
	/**
	 * @return
	 */
	public String getErrorCode() {
		return errorCode;
	}
}

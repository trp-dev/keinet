package jp.co.fj.keinavi.servlets.maintenance;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.beans.admin.LastDateBean;
import jp.co.fj.keinavi.beans.maintenance.StudentInsertBean;
import jp.co.fj.keinavi.beans.maintenance.module.StudentDataValidator;
import jp.co.fj.keinavi.beans.recount.RecountLockBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.maintenance.KNStudentData;
import jp.co.fj.keinavi.forms.maintenance.M102Form;

/**
 *
 * 生徒情報新規登録画面サーブレット
 * 
 * 2005.11.24	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M102Servlet extends AbstractMServlet {
		
	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {
		
		// アクションフォーム
		final M102Form form = (M102Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.maintenance.M102Form");

		// JSPへの遷移処理
		if ("m102".equals(getForward(request))) {
			request.setAttribute("form", form);
			forward(request, response, JSP_M102_R);
			
		// 不明なら転送
		} else {
			
			// 登録処理
			if ("m101".equals(getForward(request))
					&& "m102".equals(getBackward(request))
					&& "1".equals(form.getActionMode())) {
				
				regist(request, form);
				
				// 成功
				if (getErrorMessage(request) == null) {
					initStudentSession(request);
					actionLog(request, "801");
				// 失敗
				} else {
					request.setAttribute("form", form);
					forward(request, response, JSP_M102_R);
					return;
				}
			}
			
			forward(request, response, SERVLET_DISPATCHER);
		}
	}
	
	// 生徒情報を登録する
	private void regist(final HttpServletRequest request,
			final M102Form form) throws ServletException {
		
		final LoginSession login = getLoginSession(request);
		
		Connection con = null;
		try {
			// 生徒データ
			final KNStudentData student = form.toStudentData();
			
			// データチェック
			StudentDataValidator.getInstance().validate(student);
			
			con = getConnectionPool(request);
			con.setAutoCommit(false);
			
			// 再集計ロック失敗
			if (!RecountLockBean.lock(con, login.getUserID())) {
				con.rollback();
				request.setAttribute("ErrorMessage",
						RecountLockBean.getErrorMessage());
			} else {
				// データ更新日
				LastDateBean.setLastDate1(con, login.getUserID());
				
				final StudentInsertBean bean = new StudentInsertBean(
						login.getUserID());
				bean.setConnection(null, con);
				bean.setStudent(student);
				bean.execute();
				
				con.commit();
			}
			
		// 想定されているエラー
		} catch (final KNBeanException e) {
			rollback(con);
			request.setAttribute("ErrorMessage", e.getMessage());
		} catch (final Exception e) {
			rollback(con);
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
	}

}

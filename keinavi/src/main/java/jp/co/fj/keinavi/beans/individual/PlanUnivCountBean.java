package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import com.fjh.beans.DefaultBean;

/**
 *
 * 受験予定大学カウント
 * 
 * <2010年度改修>
 * 2010.01.19	Tomohisa YAMADA - TOTEC
 * 				新規作成
 *
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class PlanUnivCountBean extends DefaultBean {
	
	/**
	 * 個人ID
	 */
	private String individualId;
	
	/**
	 * 大学コード
	 */
	private String univcd;
	
	/**
	 * 学部コード
	 */
	private String facultycd;
	
	/**
	 * 学科コード
	 */
	private String deptcd;
	
	/**
	 * 入試形態コード
	 */
	private String mode;
	
	/**
	 * 入試区分1
	 */
	private int div1 = 0;
	
	/**
	 * 入試区分2
	 */
	private int div2 = 0;
	
	/**
	 * 入試区分3
	 */
	private int div3 = 0;
	
	/**
	 * 結果件数
	 */
	private int count;
	
	/**
	 * コンストラクタ
	 * @param string
	 */
	public PlanUnivCountBean(
			String id,
			String univcd,
			String facultycd,
			String deptcd,
			String mode) {
		this.individualId = id;
		this.univcd = univcd;
		this.facultycd = facultycd;
		this.deptcd = deptcd;
		this.mode = mode;
	}
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{

			Query query = QueryLoader.getInstance().load("i12");
			
			if (div1 > 0) {
				query.append(" AND entexamdiv1 = '" + Integer.toString(div1) + "' ");
			}
			
			if (div2 > 0) {
				query.append(" AND entexamdiv1 = '" + Integer.toString(div2) + "' ");
			}
			
			if (div3 > 0) {
				query.append(" AND entexamdiv3 = '" + StringUtils.leftPad(Integer.toString(div3), 2, '0') + "' ");
			}
					
			ps = conn.prepareStatement(query.toString());
			ps.setString(1, individualId);
			ps.setString(2, univcd);
			ps.setString(3, facultycd);
			ps.setString(4, deptcd);
			ps.setString(5, mode);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				count = Integer.parseInt(rs.getString("COUNT"));
			}
			//エラー
			else {
				throw new Exception("カウントが取得できません");
			}
			
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}	
	
	/**
	 * カウントを返す
	 * @return
	 */
	public int getCount() {
		return count;
	}

	/**
	 * 入試区分1を設定する
	 * @param div1
	 */
	public void setDiv1(int div1) {
		this.div1 = div1;
	}

	/**
	 * 入試区分2を設定する
	 * @param div2
	 */
	public void setDiv2(int div2) {
		this.div2 = div2;
	}

	/**
	 * 入試区分3を設定する
	 * @param div3
	 */
	public void setDiv3(int div3) {
		this.div3 = div3;
	}

}

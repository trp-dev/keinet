/*
 * 作成日: 2004/07/01
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.com_set;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.data.com_set.cm.ClassData;
import jp.co.fj.keinavi.util.sql.QueryLoader;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CompClassBean extends AbstractComSetBean {

	private String[] keyArrayAll; // クラスキーの配列（全選択）
	private String[] keyArrayInd; // クラスキーの配列（個別選択）
	private String targetYear; // 対象年度
	private String targetExam; // 対象模試
	private String bundleCD; // 一括コード
	private String schoolCD; // 学校コード
	private Set keySet; // クラスキーセット

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */;
	public void execute() throws SQLException, Exception {
		// 一括コードがなければ処理しない
		if (this.bundleCD == null) return;
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("cm06").toString());
			ps.setString(1, targetYear); // 模試年度
			ps.setString(2, targetExam); // 模試コード
			ps.setString(3, bundleCD); // 一括コード
			ps.setString(4, targetYear); // 模試年度
			ps.setString(5, schoolCD); // 学校コード
			ps.setString(6, targetExam); // 模試コード
			ps.setString(7, bundleCD); // 一括コード

			rs = ps.executeQuery();
			while (rs.next()) {
				ClassData data = new ClassData();
				data.setKey(rs.getString(1));
				data.setGrade(rs.getInt(2));
				data.setClassName(rs.getString(3));
				data.setExaminees(rs.getInt(4));
				fullList.add(data);
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}

	}

	/**
	 * データの格納順を入れ替える
	 * フォーム値を呼び出し前にセットすること
	 */
	public void sort() {
		// 個別選択
		this.fullList = this.sort(this.getFullList(), this.keyArrayInd);
		// 全選択
		this.partList = this.sort(this.getPartList(), this.keyArrayAll);
	}
	
	/**
	 * データリストをソートする
	 */
	private List sort(List data, String[] key) {
		// フォーム値がなければここまで
		if (key == null) return data;

		List container = new ArrayList(); // 入れ物
		Set registed = new HashSet(); // 登録済みキーセット

		for (int i=0; i<key.length; i++) {
			int index = data.indexOf(new ClassData(key[i]));

			if (index >= 0) {
				ClassData c = (ClassData) data.get(index);
				container.add(c);
				registed.add(c.getKey());
			}
		}

		// 未登録の物を詰める
		Iterator ite = data.iterator();
		while (ite.hasNext()) {
			ClassData c = (ClassData) ite.next();

			// 登録済みならスキップ
			if (registed.contains(c.getKey())) continue;

			container.add(c);
		}

		return container;
	}
	

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.com_set.AbstractComBean#getPartList()
	 */
	public List getPartList() {
		if (partList == null) {
			partList = new ArrayList();
			Iterator ite = fullList.iterator();
			while (ite.hasNext()) {
				ClassData data = (ClassData)ite.next();
				
				String[] key = data.getKey().split(":", 2);
				
				// 受験者が１人以上かつ複合クラスを除く
				if (data.getExaminees() > 0 && key[1].length() <= 2)
					partList.add(data);
			}
		}
		return partList;
	}

	/**
	 * @return
	 */
	public Set getKeySet() {
		if (this.keySet == null) {
			this.keySet = new HashSet();
			
			Iterator ite = this.getFullList().iterator();
			while (ite.hasNext()) {
				ClassData data = (ClassData) ite.next();
				this.keySet.add(data.getKey());
			}
		}

		return this.keySet;
	}

	/**
	 * @param string
	 */
	public void setBundleCD(String string) {
		bundleCD = string;
	}

	/**
	 * @param string
	 */
	public void setTargetYear(String string) {
		targetYear = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExam(String string) {
		targetExam = string;
	}

	/**
	 * @param string
	 */
	public void setSchoolCD(String string) {
		schoolCD = string;
	}

	/**
	 * @param strings
	 */
	public void setKeyArrayAll(String[] strings) {
		keyArrayAll = strings;
	}

	/**
	 * @param strings
	 */
	public void setKeyArrayInd(String[] strings) {
		keyArrayInd = strings;
	}

}

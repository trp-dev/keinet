/*
 * 作成日: 2004/10/08
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.sheet;

import jp.co.fj.keinavi.excel.data.personal.I11ListBean;

/**
 * @author kawai_yoshikazu
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ExtI11ListBean extends I11ListBean {

	private String individualid; // 個人ID
	// 2020/02/07 QQ)Ooseto 共通テスト対応 ADD START
	private String menSheetFlg; // 面談シートフラグ
	// 2020/02/07 QQ)Ooseto 共通テスト対応 ADD END

	/**
	 * @return
	 */
	public String getIndividualid() {
		return individualid;
	}

	/**
	 * @param string
	 */
	public void setIndividualid(String string) {
		individualid = string;
	}

	// 2020/02/07 QQ)Ooseto 共通テスト対応 ADD START
        /**
         * @return
         */
        public String getMenSheetFlg() {
                return menSheetFlg;
        }

        /**
         * @param string
         */
        public void setMenSheetFlg(String string) {
            menSheetFlg = string;
        }
        // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD END
}

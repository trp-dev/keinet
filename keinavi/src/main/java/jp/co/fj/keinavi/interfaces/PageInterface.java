package jp.co.fj.keinavi.interfaces;

/**
 *
 * JSPファイル名を定義するインターフェース
 *
 *
 * 2005.8.11 	Totec) T.Yamada 	[1]F002_5ページ追加
 *
 * 2005.10.12	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 * <2010年度マーク高２模試対応>
 * 2011.01.24	Tomohisa YAMADA
 * 				アップロード機能
 *
 */
public interface PageInterface {

	// -----------------------------------------------------
	//大学データ未ロード時のエラー画面
	String JSP_ONLOAD_UNIV		="/jsp/individual/onLoadUniv.jsp";
	// 個人成績分析トップ画面
	String JSP_I001 			= "/jsp/individual/i001.jsp";
	String JSP_I002		 		= "/jsp/individual/i002.jsp";
	String JSP_I003		 		= "/jsp/individual/i003.jsp";
	String JSP_I401		 		= "/jsp/individual/i401.jsp";
	String JSP_I101		 		= "/jsp/individual/i101.jsp";
	String JSP_I102		 		= "/jsp/individual/i102.jsp";
	// 成績分析バランスチャート画面
	String JSP_I103 			= "/jsp/individual/i103.jsp";
	// 成績分析バランスチャート合格者平均画面
	String JSP_I104 			= "/jsp/individual/i104.jsp";
	// 成績分析バランスチャート過回模試グラフ画面
	String JSP_I105 			= "/jsp/individual/i105.jsp";
	// 成績分析　成績推移グラフ画面
	String JSP_I106 			= "/jsp/individual/i106.jsp";
	// 成績分析　科目別成績推移グラフ画面
	String JSP_I107 			= "/jsp/individual/i107.jsp";
	// 成績分析　設問別成績グラフ画面
	String JSP_I108				= "/jsp/individual/i108.jsp";
	// 成績分析　設問別成績過回グラフ画面
	String JSP_I109 			= "/jsp/individual/i109.jsp";
	//
	String JSP_I201 			= "/jsp/individual/i201.jsp";
	//
	String JSP_I202 			= "/jsp/individual/i202.jsp";
	//
	String JSP_I203 			= "/jsp/individual/i203.jsp";
	//
	String JSP_I204 			= "/jsp/individual/i204.jsp";
	//
	String JSP_I205 			= "/jsp/individual/i205.jsp";
	//
	String JSP_I206 			= "/jsp/individual/i206.jsp";
	// 個人手帳/スケジュール
	String JSP_I301 			= "/jsp/individual/i301.jsp";
	//
	String JSP_I302 			= "/jsp/individual/i302.jsp";
	//
	String JSP_I303 			= "/jsp/individual/i303.jsp";
	//
	String JSP_I304 			= "/jsp/individual/i304.jsp";
	//
	String JSP_I305 			= "/jsp/individual/i305.jsp";
	//個人手帳・スケジュール（カレンダー）
	String JSP_I306 			= "/jsp/individual/i306.jsp";
	//面談メモ
	String JSP_I501 			= "/jsp/individual/i501.jsp";
	//面談メモ(新規・編集)
	String JSP_I502 			= "/jsp/individual/i502.jsp";
	//判定処理中
	String JSP_IOnJudge			= "/jsp/individual/iOnJudge.jsp";
	//for testing ...
	String JSP_TEST 			= "/jsp/individual/test.jsp";

	//banzaiシステム
	static final String JSP_INDEX_PAGE                = "/JSP/Index.jsp";             // トップ画面
	static final String JSP_INPUT_SCORE_PAGE          = "/JSP/InputScore.jsp";        // 得点入力画面
	static final String JSP_SEARCH_SUCCESS_UNIV_PAGE  = "/JSP/SearchSuccessUniv.jsp"; // 合格可能大学検索画面
	static final String JSP_SEARCH_WISH_UNIV_PAGE     = "/JSP/SearchWishUniv.jsp";    // 志望大学検索画面
	static final String JSP_LIST_UNIV_PAGE            = "/JSP/ListUniv.jsp";          // 大学一覧画面
	static final String JSP_LIST_DEPT_PAGE            = "/JSP/ListDept.jsp";          // 学部一覧画面
	static final String JSP_LIST_RESULT_PAGE          = "/JSP/ListResult.jsp";        // 検索結果一覧画面
	static final String JSP_FAVORITE_PAGE             = "/JSP/Favorite.jsp";          // お気に入り画面
	static final String JSP_DETAIL_PAGE               = "/JSP/Detail.jsp";            // 詳細画面
	static final String JSP_ERROR_PAGE                = "/JSP/Error.jsp";             // エラー画面

	// 志望大学判定/トップ画面

	// --------------------------------------------------
	// 閉じる
	String JSP_CLOSE = "/jsp/shared_lib/close.jsp";
	// ダイアログ
	String JSP_DIALOG = "/jsp/shared_lib/dialog.jsp";
	// 印刷確認ダイアログ
	String JSP_PRINT_DIALOG = "/jsp/shared_lib/print_dialog.jsp";
	// エラーページ
	String JSP_ERROR = "/jsp/error/error.jsp";
	// --------------------------------------------------
	// 高校用ログイン画面
	String JSP_W001 = "/jsp/login/w001.jsp";
	// 職員用ログイン画面
	String JSP_WB001 = "/jsp/login/wb001.jsp";
	// 職員用メニュー選択画面（営業部かつチューター）
	String JSP_WB002 = "/jsp/login/wb002.jsp";
	// 職員用メニュー選択画面（営業部用）
	String JSP_WB003 = "/jsp/login/wb003.jsp";
	// 校舎用工事中JSP
	String JSP_UC = "/jsp/login/uc.jsp";
	// --------------------------------------------------
	// 高校作成ファイルダウンロード画面
	String JSP_SD101 = "/jsp/sales/sd101.jsp";
	// --------------------------------------------------
	// プロファイル選択画面
	String JSP_W002 = "/jsp/profile/w002.jsp";
	// プロファイル新規作成 STEP1
	String JSP_W003 = "/jsp/profile/w003.jsp";
	// プロファイル新規作成 STEP2
	String JSP_W004 = "/jsp/profile/w004.jsp";
	// プロファイル新規作成 STEP3
	String JSP_W005 = "/jsp/profile/w005.jsp";
	// プロファイル新規作成 STEP4
	String JSP_W006 = "/jsp/profile/w006.jsp";
	// プロファイル新規作成 完了
	String JSP_W007 = "/jsp/profile/w007.jsp";
	// プロファイル別専用トップページ
	String JSP_W008 = "/jsp/profile/w008.jsp";
	// プロファイルフォルダ編集
	String JSP_W009 = "/jsp/profile/w009.jsp";
	// プロファイル編集
	String JSP_W010 = "/jsp/profile/w010.jsp";
	// プロファイル保存/方法選択
	String JSP_W101 = "/jsp/profile/w101.jsp";
	// プロファイル別名保存
	String JSP_W102 = "/jsp/profile/w102.jsp";
	// プロファイル保存完了
	String JSP_W103 = "/jsp/profile/w103.jsp";
	// --------------------------------------------------
	// 共通項目設定トップ
	String JSP_CM001 = "/jsp/com_set/cm001.jsp";
	// 共通項目/設定完了
	String JSP_CM002 = "/jsp/com_set/cm002.jsp";
	// 共通項目設定/型
	String JSP_CM101 = "/jsp/com_set/cm101.jsp";
	// 共通項目設定/科目
	String JSP_CM201 = "/jsp/com_set/cm201.jsp";
	// 共通項目設定/比較対象年度
	String JSP_CM301 = "/jsp/com_set/cm301.jsp";
	// 共通項目設定/志望大学/選択
	String JSP_CM401 = "/jsp/com_set/cm401.jsp";
	// 共通項目設定/志望大学/検索
	String JSP_CM404 = "/jsp/com_set/cm404.jsp";
	// 共通項目設定/志望大学/追加
	String JSP_CM406 = "/jsp/com_set/cm406.jsp";
	// 共通項目設定/比較対象クラス
	String JSP_CM501 = "/jsp/com_set/cm501.jsp";
	// 共通項目設定/比較対象高校/選択
	String JSP_CM601 = "/jsp/com_set/cm601.jsp";
	// 共通項目設定/比較対象高校/検索
	String JSP_CM603 = "/jsp/com_set/cm603.jsp";
	// 共通項目設定/比較対象高校/追加
	String JSP_CM604 = "/jsp/com_set/cm604.jsp";
	// 担当クラス変更
	String JSP_CM701 = "/jsp/com_set/cm701.jsp";
	// 担当クラス変更（閉じる）
	String JSP_CM701_CLOSE = "/jsp/com_set/cm701_close.jsp";
	// --------------------------------------------------
	// 校内成績分析トップ
	String JSP_S001 = "/jsp/school/s001.jsp";
	// 詳細画面MAX版
	String JSP_SMAX = "/jsp/school/smax.jsp";
	// サンプル画面
	String JSP_S012 = "/jsp/school/s012.jsp";
	// --------------------------------------------------
	// クラス成績分析トップ
	String JSP_C001 = "/jsp/cls/c001.jsp";
	// 詳細画面MAX版
	String JSP_CMAX = "/jsp/cls/cmax.jsp";
	// --------------------------------------------------
	// メンテナンストップ
	String JSP_M001 = "/jsp/admin/m001.jsp";
	// 生徒情報管理一覧
	String JSP_M101 = "/jsp/admin/m101.jsp";
	// 生徒情報リスト
	String JSP_M101_LIST = "/jsp/admin/m101_list.jsp";
	// 生徒情報管理登録
	String JSP_M102 = "/jsp/admin/m102.jsp";
	// 生徒情報管理一括登録エラー
	String JSP_M103 = "/jsp/admin/m103.jsp";
	// 生徒情報管理一括登録完了
	String JSP_M104 = "/jsp/admin/m104.jsp";
	// 生徒情報管理編集
	String JSP_M105 = "/jsp/admin/m105.jsp";
	// 作業中宣言
	String JSP_X010 = "/jsp/maintenance/x010.jsp";
	// 作業中宣言ダイアログ
	String JSP_X011 = "/jsp/maintenance/x011.jsp";
	// 生徒情報管理一覧（改修版）
	String JSP_M101_R = "/jsp/maintenance/m101.jsp";
	// 生徒情報リスト（改修版）
	String JSP_M101_LIST_R = "/jsp/maintenance/m101_list.jsp";
	// 生徒情報管理登録（改修版）
	String JSP_M102_R = "/jsp/maintenance/m102.jsp";
	// 生徒情報管理一括登録エラー（改修版）
	String JSP_M103_R = "/jsp/maintenance/m103.jsp";
	// 生徒情報管理一括登録完了（改修版）
	String JSP_M104_R = "/jsp/maintenance/m104.jsp";
	// 生徒情報管理編集（改修版）
	String JSP_M105_R = "/jsp/maintenance/m105.jsp";
	// 生徒情報管理一括登録確認
	String JSP_M106_R = "/jsp/maintenance/m106.jsp";
	// --------------------------------------------------
	// 受験届修正/一覧
	String JSP_M201 = "/jsp/admin/m201.jsp";
	// 受験届修正/リスト
	String JSP_M201_LIST = "/jsp/admin/m201_list.jsp";
	// 受験届修正/修正
	String JSP_M202 = "/jsp/admin/m202.jsp";
	// --------------------------------------------------
	// 複合クラス管理/一覧
	String JSP_M301 = "/jsp/admin/m301.jsp";
	// 複合クラス管理/編集
	String JSP_M302 = "/jsp/admin/m302.jsp";
	// --------------------------------------------------
	// 生徒情報管理・利用者一覧
	String JSP_U001 = "/jsp/user/u001.jsp";
	// 生徒情報管理・利用者登録変更
	String JSP_U002 = "/jsp/user/u002.jsp";
	// 生徒情報管理・担当クラス設定
	String JSP_U003 = "/jsp/user/u003.jsp";
	// 生徒情報管理・登録完了
	String JSP_U004 = "/jsp/user/u004.jsp";
	// 利用者情報確認
	String JSP_U101 = "/jsp/user/u101.jsp";
	// 利用者パスワード変更
	String JSP_U102 = "/jsp/user/u102.jsp";
	// --------------------------------------------------
	// 転送サーブレット
	String SERVLET_DISPATCHER = "/DispatcherServlet";
	// --------------------------------------------------

	// 無料メニュー
	String JSP_F001 = "/jsp/freemenu/f001.jsp";
	// Trial（体験版表示用）
	String JSP_TrialLogin = "/jsp/freemenu/TrialLogin.jsp";
	// 成績統計資料集ダウンロード
	String JSP_F002 = "/jsp/freemenu/f002.jsp";
	// 成績統計資料集ダウンロード（高校別）
	String JSP_F002_1 = "/jsp/freemenu/f002_1.jsp";
	//成績統計資料集ダウンロード（型／科目別）
	 String JSP_F002_2 = "/jsp/freemenu/f002_2.jsp";
	// 成績統計資料集ダウンロード（高校別設問別）
	String JSP_F002_3 = "/jsp/freemenu/f002_3.jsp";
	// 成績統計資料集ダウンロード（国公立大学学部別・高校別志望状況）
	String JSP_F002_4 = "/jsp/freemenu/f002_4.jsp";
	//[1] add start
	// 成績統計資料集ダウンロード（志望大学・学部・学科別成績分布表）
	String JSP_F002_5 = "/jsp/freemenu/f002_5.jsp";
	//[1] add end
	// 大学入試センター試験分析データダウンロード
	String JSP_F003 = "/jsp/freemenu/f003.jsp";
	//大学入試センター試験分析データダウンロード(項目表示)
	 String JSP_F003_1 = "/jsp/freemenu/f003_1.jsp";
	// 私書箱サブメニュー
	String JSP_F004 = "/jsp/freemenu/f004.jsp";
	// 成績データダウンロード
	String JSP_F005 = "/jsp/freemenu/f005.jsp";
	//成績データダウンロード
	String JSP_F005_1 = "/jsp/freemenu/f005_1.jsp";
	//成績データダウンロード
	 String JSP_F005_2 = "/jsp/freemenu/f005_2.jsp";
	// 入試結果調査フォーマットダウンロード
	String JSP_F006 = "/jsp/freemenu/f006.jsp";
	// 入試結果調査データアップロード
	String JSP_F007 = "/jsp/freemenu/f007.jsp";
	// 成績結果調査データアップロード成功
	String JSP_F008 = "/jsp/freemenu/f008.jsp";
	// 成績結果調査データアップロード失敗（通信異常・ファイル書き込み異常）
	String JSP_F009_1 = "/jsp/freemenu/f009_1.jsp";
	// 成績結果調査データアップロード失敗（ファイルサイズ＝０）
	String JSP_F009_2 = "/jsp/freemenu/f009_2.jsp";
	// 公開ファイルアップロード
	String JSP_F010 = "/jsp/freemenu/f010.jsp";
	// 公開ファイルアップロード（結果）
	String JSP_F010_RESULT = "/jsp/freemenu/f010_result.jsp";
	// 公開ファイル一覧
	String JSP_F011 = "/jsp/freemenu/f011.jsp";
	// 模試成績データダウンロード
	String JSP_EXAM_SEISEKI = "/jsp/freemenu/examSeiseki.jsp";

	// -----------------------------------------------------
	// ヘルプトップ画面
	String JSP_H001             = "/jsp/help/h001.jsp";
	// カテゴリー一覧画面
	String JSP_H002             = "/jsp/help/h002.jsp";
	// ヘルプ詳細画面
	String JSP_H003             = "/jsp/help/h003.jsp";
	// ワンポイント詳細画面
	String JSP_H003P            = "/jsp/help/h003p.jsp";
	// ヘルプ検索結果
	String JSP_H004             = "/jsp/help/h004.jsp";
	// ヘルプ送信完了
	String JSP_H005             = "/jsp/help/h005.jsp";
	// お問い合わせ
	String JSP_H101             = "/jsp/help/h101.jsp";
	// お問い合わせ（詳細確認）
	String JSP_H102             = "/jsp/help/h102.jsp";
	// お問い合わせ（送信完了）
	String JSP_H103             = "/jsp/help/h103.jsp";

	// -----------------------------------------------------
	// ヘルプデスク  ログイン
	String JSP_HD001            = "/jsp/helpdesk/hd001.jsp";
	// ヘルプデスク  表示メニュー選択
	String JSP_HD002            = "/jsp/helpdesk/hd002.jsp";
	// ヘルプデスク  ユーザー管理（一覧）
	String JSP_HD101            = "/jsp/helpdesk/hd101.jsp";
	// ヘルプデスク  ユーザー管理（一括登録）
	String JSP_HD102            = "/jsp/helpdesk/hd102.jsp";
	// ヘルプデスク  ユーザー管理（一括登録エラー）
	String JSP_HD103            = "/jsp/helpdesk/hd103.jsp";
	// ヘルプデスク  ユーザー管理（一括登録完了）
	String JSP_HD104            = "/jsp/helpdesk/hd104.jsp";
	// ヘルプデスク  ユーザー管理（詳細参照）
	String JSP_HD105            = "/jsp/helpdesk/hd105.jsp";
	// ヘルプデスク  ユーザー管理（証明書発行）
	String JSP_HD106            = "/jsp/helpdesk/hd106.jsp";
	// 2015/10/21 QQ)Nishiyama デジタル証明書対応 ADD START
	// ヘルプデスク  ユーザー管理（ファイル登録）
	String JSP_HD107            = "/jsp/helpdesk/hd107.jsp";
	// 2015/10/21 QQ)Nishiyama デジタル証明書対応 ADD END
	// ヘルプデスク　ユーザー管理 (証明書発行スレッド専用) 2004/12/24
	String JSP_HDOnCert 		= "/jsp/helpdesk/hdOnCert.jsp";

	// -----------------------------------------------------
	// お知らせトップ画面
	String JSP_N001             = "/jsp/news/n001.jsp";
	// お知らせ詳細画面
	String JSP_N002             = "/jsp/news/n002.jsp";

	// テキスト出力トップ
	String JSP_T001="/jsp/txt_out/t001.jsp";
	// テキスト出力詳細
	String JSP_T002="/jsp/txt_out/t002.jsp";

}

package jp.co.fj.keinavi.beans.csv;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * CSVデータファイル書き込みクラス
 */
public class CSVWriter {
		
	// ファイル書込み用ストリーム
	private OutputStream 	out;
	// ヘッダー情報
	private String[] header;
	// 項目区切り文字
	private String delimiter;
	// ファイル書込みオブジェクト
	private PrintWriter prnWriter;
	
	/**
	 * 
	 * エクスポートファイルの初期化
	 * 
	 * @param out 出力ストリーム
	 * @param code エンコード指定
	 */
	public CSVWriter(OutputStream out, String code) throws UnsupportedEncodingException{
		prnWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(out, code)));
		this.out 	= out;
		delimiter 	= ",";
	}
	
	
	/**
	 * 
	 * エクスポートファイルの初期化
	 * 
	 * @param out 出力ストリーム
	 * @param header ヘッダー情報
	 * @param code エンコード指定
	 */
	public CSVWriter(OutputStream out, String[] header, String code) throws UnsupportedEncodingException{
		prnWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(out, code)));
		this.out 	= out;
		this.header = header;
		delimiter 	= ",";
		writeHeadLine();
	}


	/**
	 *
	 * ヘッダー情報をファイルに書き込む。
	 *
	 * @return void
	 */
	public void writeHeadLine() {
		StringBuffer stb = new StringBuffer();
		if(header!=null){
			for( int i=0; i<header.length; i++){
				 stb.append(header[i]+delimiter);
			}
		}
		prnWriter.println(stb);

	}
	
	/**
	 * 一行分のデータをファイルに書き込む。
	 * @param	line	CSVLineクラス
	 */
	public void writeLine(CSVLine line) {
		prnWriter.println(line.getLineNOQuoMark(delimiter));
		
	}
	
	
	/**
	 * 区切り文字を設定。
	 * @param	s	区切り文字
	 */
	public void setDelimiter(String s) {
		delimiter = s;
	}
	
	
	/**
	 * 出力ストリームをフラッシュ。
	 */
	public void flush() throws IOException {
		try {
			prnWriter.flush();
			out.flush();
		} catch(IOException ie) {
			throw ie;
		}
	}
	
	
	/**
	 * ストリームに関連するすべてのシステムリソースを解放。
	 */
	public void close() throws IOException {
		try {
			prnWriter.close();
			out.close();
		} catch(IOException ie) {
			throw ie;
		}
	}
}

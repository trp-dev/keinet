/*
 * 作成日: 2004/10/11
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.txt_out;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import jp.co.fj.keinavi.beans.csv.CSVLine;
import jp.co.fj.keinavi.beans.csv.CSVWriter;
import jp.co.fj.keinavi.util.CollectionUtil;

import com.fjh.beans.DefaultBean;

/**
 * @author 奥村ゆかり
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class OutputTextBean extends DefaultBean {
	/* データオブジェクト */
	private LinkedList outputTextList = null;
	/* 出力ファイル名 */
	private File outFile = null;
	/* ヘッダー情報 */
	private List headTextList = null;
	/* 出力項目対象 */
	private Integer outTarget[] = null;
	/* 出力ファイル種別 0:CSV　1:txt　*/
	private int outType = 0;

	public void execute() throws IOException {
		
		// 出力ファイル種別によって出力するファイル形式を
		// 変更します。
		
		if (outType == 1)
			doCsvExport();
		else
			doFixedExport();
	}

//	public static void main(String[] args) {
//		LinkedList textList = new LinkedList();
//
//		try {
//			TextInfoBean txt = new TextInfoBean();
//			txt.execute();
//		
//			for (int i = 0; i < 3; i++) {
//				LinkedHashMap map = new LinkedHashMap();
//				map.put(new Integer("1"), "aa");
//				map.put(new Integer("2"), "bb");
//				map.put(new Integer("3"), "cc");
//				map.put(new Integer("4"), "dd");
//				map.put(new Integer("5"), "ee");
//				map.put(new Integer("9"), "ii");
//				map.put(new Integer("10"), "");
//				map.put(new Integer("0"), null);
//				map.put(new Integer("13"), "nn");
//				map.put(new Integer("6"), "ff");
//				map.put(new Integer("7"), "gg");
//				map.put(new Integer("8"), "hh");
//				map.put(new Integer("10"), "jj");
//									
//				textList.add(map);
//			}
//
//			OutputTextBean out = new OutputTextBean();
//			out.setOutFile(new File("D:\\eclipse\\workspace\\out.txt"));
//			out.setOutputTextList(textList);
//			out.setHeadTextList(txt.getInfoList("t002"));
//			out.setOutTarget(new Integer[]{new Integer("10"),new Integer("12"),new Integer("2"),new Integer("13")});
//			out.setOutType(2);
//			out.execute();
//			
//		} catch (IOException ex) {
//			ex.printStackTrace();
//		}
//	}


	
	/**
	 * 	エクスポートファイル：ヘッダー情報生成
	 * 
	 * @return header ヘッダー情報
	 */
	protected String[] getHeader(List headerList){
		
		Integer target[] = getOutTarget();	
		String header[] = new String[target.length];
		Set outSet = CollectionUtil.array2Set( target);
		int count = 0;
		for(int i=0; i<headerList.size(); i++){
			Integer key = new Integer(((String[])headerList.get(i))[0]);
			if(outSet.contains(key)){
				header[count] = ((String[])headerList.get(i))[2];
				count++;
			}	
		}
		return header;
	}
	
	
	/**
	* 
	* 固定長ファイルの書込み。
	* 
	* @return void
	*/
	public void doFixedExport() throws IOException {
		
		PrintWriter prnWriter = null;
		try {
			// ディレクトリの作成
			File parent = new File(outFile.getParentFile().getPath());
			if(!parent.exists())parent.mkdirs();
			
			// ヘッダー情報の抽出
			String header[] = getHeader(getHeadTextList());
			// ファイル書き出しストリームを格納
			OutputStream out = new FileOutputStream(outFile.getPath(), false);
			prnWriter =
				new PrintWriter(
					new BufferedWriter(new OutputStreamWriter(out, "MS932")));
					
			// ヘッダーの書き込み
			for(int i=0; i<header.length; i++){
				prnWriter.print(header[i]+",");
				prnWriter.flush();
			}
			prnWriter.println();
			
			int count = 0;
			Set outSet = CollectionUtil.array2Set( outTarget);
			for (Iterator c = outputTextList.iterator(); c.hasNext();) {
				//LinkedHashMap textValue = (LinkedHashMap) c.next();
				TreeMap textValue= new TreeMap((Map) c.next());
				Set keyset = textValue.keySet();
				Iterator ite = keyset.iterator();
				/* レコードセット */
				while (ite.hasNext()) {
					Integer key = (Integer) ite.next();
					// 出力対象にセットされた項目ならば出力
					if(outSet.contains(key)){
						int byteSize = Integer.parseInt((
							(String[])this.getHeadTextList().get(key.intValue()-1))[3]);
						int valueJudge = Integer.parseInt((
							(String[])this.getHeadTextList().get(key.intValue()-1))[5]);

						String value = valueFixed((String) textValue.get(key), byteSize, valueJudge);
						prnWriter.print(value);
					}
					prnWriter.flush();
				}
				prnWriter.println();
				count++;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException("固定長ファイルの出力に失敗しました。:" + ex.getMessage());
			
		} finally {
			if(prnWriter!=null)prnWriter.flush();
			if(prnWriter!=null)prnWriter.close();

		}
	}

	/**
	* 
	* CSVファイルの書込み。
	* 
	* @return void
	*/
	public void doCsvExport() throws IOException {

		OutputStream inf_out = null;
		CSVWriter inf_cw = null;
		try {
			// ディレクトリの作成
			File parent = new File(outFile.getParentFile().getPath());
			if(!parent.exists())parent.mkdirs();
			// ヘッダー情報の抽出
			String header[] = getHeader(getHeadTextList());
			// CSVファイル書き出しストリームを格納
			inf_out = new FileOutputStream(outFile.getPath(), false);
			// CSV出力用オブジェクトを格納
			inf_cw = new CSVWriter(inf_out, header,"MS932");
			inf_cw.setDelimiter(",");

			String[] record = null;
			CSVLine inf_csvLine = null;
			Set outSet = CollectionUtil.array2Set( outTarget);
			int recSize = outSet.size();
			
			int count = 0;
			for (Iterator c = outputTextList.iterator(); c.hasNext();) {
				//LinkedHashMap textValue = (LinkedHashMap) c.next();
				TreeMap textValue= new TreeMap((Map) c.next());
				record = new String[recSize];
				Set keyset = textValue.keySet();
				Iterator ite = keyset.iterator();
				int recCount = 0;
				/* CSVレコードセット */
				while (ite.hasNext()) {
					Integer key = (Integer) ite.next();
					String value=(String) textValue.get(key);
					if(value==null)value="";
					// 出力対象にセットされた項目ならば出力
					if(outSet.contains(key)){
						record[recCount] = value; 
						recCount++;
					}
				}
				inf_csvLine = new CSVLine(record);
				inf_csvLine.setCount(recSize);
				inf_cw.writeLine(inf_csvLine);
				count++;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException("ＣＳＶファイルの出力に失敗しました。" + ex.getMessage());
		} finally {
			if(inf_cw!=null)inf_cw.flush();
			if(inf_cw!=null)inf_cw.close();
		}
	}

	/**
	 * 固定長文字列生成
	 * @param str 文字列
	 * @param index バイト数
	 * @param judge 数値か文字列か
	 */
	protected String valueFixed(String str, int index, int judge) {
		int idx = 0;
		String s = "";
		byte[] byt = null;
		int count = 0;

 		if (str != null) {
			try {
				byt = str.getBytes("MS932");
			} catch (UnsupportedEncodingException e) {
				//絶対に起こらないはずの例外
				e.printStackTrace();
				throw new IllegalStateException(e.getMessage());
			}
			count = byt.length;

			// [2004/10/29 nino]
			// 指定バイト数より文字列の長さが大きい場合はバイト数(index)分切り取る
			if (count > index) {
				// 最終バイトが漢字コード１バイト目の場合は最終バイトを切り捨てる
				// byte型  0〜127 → 0x00〜0x7f  -128〜-1 → 0x80〜0xff
				idx = index;
				int code = (int)byt[index-1];
				if ( code < 0 ) code += 256;
				if ( (0x81<= code && code <= 0x9f) || (0xe0 <= code && code <= 0xef) ) {
					idx--;
				}
				try {
					return new String(byt, 0, idx, "MS932");
				} catch (Exception e) {
					throw new InternalError(e.getMessage());
				}
			} else {
				int n = index - count;
				for (int i = 0; i < n; i++) {
					s += " ";
				}
				if(judge ==1) return str + s;
				else return s + str;
			}
		} else {
			for (int i = 0; i < index; i++) {
				s += " ";
			}
			return s;
		}

	}
	
	/**
	 * @param list
	 */
	public void setOutputTextList(LinkedList list) {
		outputTextList = list;
	}

	/**
	 * @param file
	 */
	public void setOutFile(File file) {
		outFile = file;
	}

	/**
	 * @return
	 */
	public int getOutType() {
		return outType;
	}

	/**
	 * @param i
	 */
	public void setOutType(int i) {
		outType = i;
	}



	/**
	 * @param list
	 */
	public void setHeadTextList(List list) {
		headTextList = list;
	}

	/**
	 * @return
	 */
	public List getHeadTextList() {
		return headTextList;
	}

	/**
	 * @return
	 */
	public LinkedList getOutputTextList() {
		return outputTextList;
	}

	/**
	 * @return
	 */
	public Integer[] getOutTarget() {
		return outTarget;
	}

	/**
	 * @param integers
	 */
	public void setOutTarget(Integer[] integers) {
		outTarget = integers;
	}

    /**
     * @return outFile を戻します。
     */
    public File getOutFile() {
        return outFile;
    }
}

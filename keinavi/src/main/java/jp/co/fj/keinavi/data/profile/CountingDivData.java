/*
 * 作成日: 2004/07/02
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.profile;

import java.io.Serializable;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CountingDivData implements Serializable {

	// 集計区分コード
	private String countingDivCode = null;
	// 集計区分名称
	private String countingDivName = null;
	// 集計区分名称カナ
	private String countingDivNameKana = null;

	/**
	 * コンストラクタ
	 */
	public CountingDivData() {
	}

	/**
	 * コンストラクタ
	 */
	public CountingDivData(String countingDivCode) {
		this.countingDivCode = countingDivCode;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return ((CountingDivData)obj).getCountingDivCode().equals(countingDivCode);
	}

	/**
	 * @return
	 */
	public String getCountingDivCode() {
		return countingDivCode;
	}

	/**
	 * @return
	 */
	public String getCountingDivName() {
		return countingDivName;
	}

	/**
	 * @return
	 */
	public String getCountingDivNameKana() {
		return countingDivNameKana;
	}

	/**
	 * @param string
	 */
	public void setCountingDivCode(String string) {
		countingDivCode = string;
	}

	/**
	 * @param string
	 */
	public void setCountingDivName(String string) {
		countingDivName = string;
	}

	/**
	 * @param string
	 */
	public void setCountingDivNameKana(String string) {
		countingDivNameKana = string;
	}

}

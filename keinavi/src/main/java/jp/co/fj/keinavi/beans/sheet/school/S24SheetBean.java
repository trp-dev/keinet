package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.excel.data.school.S24HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.school.S24Item;
import jp.co.fj.keinavi.excel.data.school.S24ListBean;
import jp.co.fj.keinavi.excel.school.S24;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 校内成績分析 - 過年度比較（志望大学評価別人数）
 *
 * 2004.08.04	[新規作成]
 *
 * 2005.04.06	Yoshimoto KAWAI - TOTEC
 * 				出力種別フラグ対応
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class S24SheetBean extends AbstractSheetBean {

	// データクラス
	private final S24Item data = new S24Item();

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// 比較対象年度
		final String[] years = getCompYearArray();
		// 大学集計区分
		final String univCountingDiv = getUnivCountngDiv();

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setStrMshCd(exam.getExamCD()); // 模試コード
		data.setIntDaiTotalFlg(getDaiTotalFlg()); // 大学集計区分
		data.setIntSecuFlg(getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(getOutputType()); // 出力種別フラグ

		// ワークテーブルのセットアップ
		insertIntoCountUniv();
		insertIntoExistsExam();
		insertIntoExamCdTrans();
		cleanExamCdTrans();

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		try {
			// データリスト
			ps1 = conn.prepareStatement(getRatingQuery().toString());
			ps1.setString(1, exam.getExamYear()); // 模試年度
			ps1.setString(2, exam.getExamCD()); // 模試コード
			ps1.setString(3, profile.getBundleCD()); // 一括コード
			ps1.setString(4, univCountingDiv); // 大学集計区分
			ps1.setString(5, exam.getExamDiv()); // 模試区分

			// 評価別人数データリスト
			final Query query = QueryLoader.getInstance().load("s24_6");
			query.setStringArray(1, years); // 比較対象年度

			ps2 = conn.prepareStatement(query.toString());
			ps2.setString(2, univCountingDiv); // 大学集計区分
			ps2.setString(8, profile.getBundleCD()); // 一括コード
			ps2.setString(10, univCountingDiv); // 大学集計区分
			ps2.setString(16, exam.getExamCD()); // 模試コード

			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				S24ListBean bean = new S24ListBean();
				bean.setStrGenKouKbn(rs1.getString(1));
				bean.setStrDaigakuCd(rs1.getString(2));
				bean.setStrDaigakuMei(rs1.getString(3));
				bean.setStrGakubuCd(rs1.getString(4));
				bean.setStrGakubuMei(rs1.getString(5));
				bean.setStrGakkaCd(rs1.getString(6));
				bean.setStrGakkaMei(rs1.getString(7));
				bean.setStrNtiCd(rs1.getString(8));
				bean.setStrNittei(rs1.getString(9));
				bean.setStrHyouka(rs1.getString(10));
				data.getS24List().add(bean);

				ps2.setString(1, bean.getStrGenKouKbn()); // 現役高卒区分
				ps2.setString(3, bean.getStrDaigakuCd()); // 大学コード
				ps2.setString(4, bean.getStrGakubuCd()); // 学部コード
				ps2.setString(5, bean.getStrGakkaCd()); // 学科コード
				ps2.setString(6, bean.getStrNtiCd()); // 日程コード
				ps2.setString(7, bean.getStrHyouka()); // 評価区分
				ps2.setString(9, bean.getStrGenKouKbn()); // 現役高卒区分
				ps2.setString(11, bean.getStrDaigakuCd()); // 大学コード
				ps2.setString(12, bean.getStrGakubuCd()); // 学部コード
				ps2.setString(13, bean.getStrGakkaCd()); // 学科コード
				ps2.setString(14, bean.getStrNtiCd()); // 日程コード
				ps2.setString(15, bean.getStrHyouka()); // 評価区分

				rs2 = ps2.executeQuery();
				while (rs2.next()) {
					S24HyoukaNinzuListBean n = new S24HyoukaNinzuListBean();
					n.setStrNendo(rs2.getString(1));
					n.setIntSoshiboAll(rs2.getInt(2));
					n.setIntSoshiboHome(rs2.getInt(3));
					n.setIntDai1shibo(rs2.getInt(4));
					n.setFloTokuritsuAll(rs2.getFloat(5));
					n.setFloTokuritsuHome(rs2.getFloat(6));
					n.setFloHensaAll(rs2.getFloat(7));
					n.setFloHensaHome(rs2.getFloat(8));
					n.setIntHyoukaA(rs2.getInt(9));
					n.setIntHyoukaB(rs2.getInt(10));
					n.setIntHyoukaC(rs2.getInt(11));
					n.setIntHyoukaD(rs2.getInt(12));
					n.setIntHyoukaE(rs2.getInt(13));

					// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 DEL START
					// 2019/09/30 QQ) 共通テスト対応 ADD START
//					n.setIntHyoukaA_Hukumu(rs2.getInt(14));
//					n.setIntHyoukaB_Hukumu(rs2.getInt(15));
//					n.setIntHyoukaC_Hukumu(rs2.getInt(16));
//					n.setIntHyoukaD_Hukumu(rs2.getInt(17));
//					n.setIntHyoukaE_Hukumu(rs2.getInt(18));
					// 2019/09/30 QQ) 共通テスト対応 ADD END
					// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 DEL START

					bean.getS24HyoukaNinzuList().add(n);
				}
				rs2.close();
			}
		} finally {
			DbUtils.closeQuietly(null, ps1, rs1);
			DbUtils.closeQuietly(null, ps2, rs2);
		}

		KNLog.Ep(null, null, null, null, "S24", "データクラス作成完了", "");
	}

	// 個別対応
	// 2008年度以降の高2記述模試
	private void cleanExamCdTrans() throws SQLException {

		// 2008年度より前の高2記述模試とは過年度比較不可
		if ("65".equals(exam.getExamCD()) && "2008".compareTo(exam.getExamYear()) < 1) {
			PreparedStatement ps = null;
			try {
				ps = conn.prepareStatement("DELETE FROM examcdtrans WHERE examyear < 2008");
				ps.executeUpdate();
			} finally {
				DbUtils.closeQuietly(ps);
			}
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S24_01");
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_PREV_UNIV;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S24().s24(data, outfileList, getAction(), sessionKey,sheetLog);
	}

}

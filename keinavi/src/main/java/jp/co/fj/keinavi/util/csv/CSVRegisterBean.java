package jp.co.fj.keinavi.util.csv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.util.csv.data.CSVLineRecord;
import jp.co.totec.util.CollectionUtil;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fjh.beans.DefaultBean;

/**
 *
 * CSV登録Bean
 * 
 * 2005.10.25	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class CSVRegisterBean extends DefaultBean implements Serializable {
	
	/** セッションキー */
	public static final String SESSION_KEY = "CSVRegisterBean";

	// CSV行レコードオブジェクトのリスト
	private final List lineRecordList = new ArrayList();
	// 削除レコードオブジェクトのリスト
	private final List deleteRecordList = new ArrayList();
	// 行登録Beanのインスタンス
	private final CSVLineRegisterBean bean;
	
	// 新規レコードカウンタ
	private int insertCounter = 0;
	// 更新レコードカウンタ
	private int updateCounter = 0;
	// 削除レコードカウンタ
	private int deleteCounter = 0;
	// コミット処理済かどうか
	private boolean isCommitted = false;
	// レコードのエラー件数
	private int errorSize = 0;
	
	/**
	 * コンストラクタ
	 * 
	 * @param data CSVデータ
	 * @param pBean 行登録Beanのインスタンス
	 * @param columnSize CSVファイルの列数
	 * @throws IOException CSVデータの解析に失敗したら発生する
	 */
	public CSVRegisterBean(final byte[] data, 
			final CSVLineRegisterBean pBean,
			final int columnSize) throws IOException {
		
		if (data == null) {
			throw new IllegalArgumentException(
					"CSVデータがNULLです。");
		}
		
		if (columnSize < 0) {
			throw new IllegalArgumentException(
					"列数は0以上で指定してください。");
		}

		this.bean = pBean;
		parse(data, columnSize);
	}

	/**
	 * コンストラクタ
	 * 
	 * @param data CSVデータ
	 * @param pBean 行登録Beanのインスタンス
	 * @param columnSize CSVファイルの列数
	 * @throws IOException CSVデータの解析に失敗したら発生する
	 */
	public CSVRegisterBean(final byte[] data, 
			final CSVLineRegisterBean pBean) throws IOException {
		
		if (data == null) {
			throw new IllegalArgumentException(
					"CSVデータがNULLです。");
		}

		this.bean = pBean;
		parse(data, 0);
	}

	/**
	 * CSVのバイトデータから行データオブジェクトのリストに変換する
	 * 
	 * @param data
	 * @param columnSize
	 * @throws  
	 * @throws IOException 
	 */
	private void parse(final byte[] data,
			final int columnSize) throws IOException {

		BufferedReader reader = null;
		try {
			
			reader = new BufferedReader(
					new StringReader(new String(data, "Windows-31J")));
		
			int lineNo = 1;
			String line;
			while ((line = reader.readLine()) != null) {
				
				// コメント行は飛ばす
				if (line.startsWith("#")) {
					lineNo++;
					continue;
				}
				
				final String[] record = CollectionUtil.splitCSVComma(line);
				
				if (columnSize > 0 && record.length < columnSize) {
					final String[] array = new String[columnSize];
					Arrays.fill(array, "");
					System.arraycopy(record, 0, array, 0, record.length);
					lineRecordList.add(
							bean.createLineRecord(bean.normalizeRecord(array),
									lineNo++));
				} else {
					lineRecordList.add(
							bean.createLineRecord(bean.normalizeRecord(record),
									lineNo++));
				}
			}
			
		} catch (final UnsupportedEncodingException e) {
			throw new InternalError(e.getClass() + ":" + e.getMessage());
		} finally {
			reader.close();
		}
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		// 行登録Beanにコネクションをセット
		bean.setConnection(null, conn);
		
		// 行単位のエラーチェック
		for (final Iterator ite = lineRecordList.iterator(); ite.hasNext();) {
			final CSVLineRecord record = (CSVLineRecord) ite.next();
			bean.validateRecord(record);
		}

		// 全体のエラーチェック
		bean.validateAllRecord(lineRecordList);
		
		// エラーをまとめる
		for (final Iterator ite = lineRecordList.iterator(); ite.hasNext();) {
			final CSVLineRecord record = (CSVLineRecord) ite.next();
			errorSize += record.getErrorDataList().size();
		}
		
		// エラーが発生していないならレコードタイプの判定をする
		if (!hasError()) {

			// 変更（更新・削除）があるかどうかのフラグ
			boolean hasChangeRecord = false;

			// 更新判定
			for (final Iterator ite = lineRecordList.iterator();
					ite.hasNext();) {
				
				final CSVLineRecord record = (CSVLineRecord) ite.next();
				
				// 新規か更新かどうかを判定する
				bean.judgeRecordType(record);
				
				// カウントを増やす
				if (record.isInsertRecord()) {
					insertCounter++;
				} else if (record.isUpdateRecord()) {
					updateCounter++;
				}
				
				// 更新レコードかどうか調べる
				if (!hasChangeRecord && record.isUpdateRecord()) {
					hasChangeRecord = true;
				}
			}

			// 削除判定
			deleteRecordList.addAll(bean.selectDeleteRecord(lineRecordList));
			deleteCounter = deleteRecordList.size();
			if (deleteCounter > 0 && !hasChangeRecord) {
				hasChangeRecord = true;
			}
			
			// 更新・削除がなければコミット処理
			if (!hasChangeRecord) {
				commitProcess(true);
			}
		}
	}

	/**
	 * コミット処理（INSERT、UPDATE処理）をする 
	 * 
	 * @throws SQLException SQLエラーで発生
	 */
	public void commitProcess() throws SQLException {
		// 行登録Beanのコネクションを上書きしておく
		bean.setConnection(null, conn);
		commitProcess(false);
	}

	// コミット処理（INSERT、UPDATE処理）をする 
	private void commitProcess(final boolean isDirectCall) throws SQLException {
		
		// ダイレクトコール（確認画面を経由しないコール）であるならフラグセット
		if (isDirectCall) {
			bean.setDirectCommit();
		}
		
		// 削除処理
		bean.deleteRecord(deleteRecordList);

		// 登録処理
		for (final Iterator ite = lineRecordList.iterator(); ite.hasNext();) {
			final CSVLineRecord record = (CSVLineRecord) ite.next();
			bean.registRecord(record);
			errorSize += record.getErrorDataList().size();
		}
		
		// 不要レコードクリーン
		bean.cleanRecord();
		
		isCommitted = !hasError();
	}

	/**
	 * @return コミット処理が行われたかどうか
	 */
	public boolean isCommitted() {
		return isCommitted;
	}
	
	/**
	 * @return レコードにエラーがあるかどうか
	 */
	public boolean hasError() {
		return errorSize > 0;
	}
	
	/**
	 * @return lineRecordList を戻します。
	 */
	public List getLineRecordList() {
		return lineRecordList;
	}
	
	/**
	 * @return deleteRecordList を戻します。
	 */
	public List getDeleteRecordList() {
		return deleteRecordList;
	}

	/**
	 * @return insertCounter を戻します。
	 */
	public int getInsertCounter() {
		return insertCounter;
	}
	
	/**
	 * @return updateCounter を戻します。
	 */
	public int getUpdateCounter() {
		return updateCounter;
	}
	
	/**
	 * @return deleteCounter を戻します。
	 */
	public int getDeleteCounter() {
		return deleteCounter;
	}

	/**
	 * @return errorSize を戻します。
	 */
	public int getErrorSize() {
		return errorSize;
	}

	/**
	 * @param deleteCounter 設定する deleteCounter。
	 */
	public void setDeleteCounter(final int deleteCounter) {
		this.deleteCounter = deleteCounter;
	}

	/**
	 * @param insertCounter 設定する insertCounter。
	 */
	public void setInsertCounter(final int insertCounter) {
		this.insertCounter = insertCounter;
	}

	/**
	 * @param updateCounter 設定する updateCounter。
	 */
	public void setUpdateCounter(final int updateCounter) {
		this.updateCounter = updateCounter;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return
				new ToStringBuilder(this)
						.append("InsertCounter", insertCounter)
						.append("UpdateCounter", updateCounter)
						.append("IsCommitted", isCommitted)
						.append("HasError", hasError())
						.toString();
	}

	/**
	 * @return bean を戻します。
	 */
	public CSVLineRegisterBean getCSVLineRegisterBean() {
		return bean;
	}

}

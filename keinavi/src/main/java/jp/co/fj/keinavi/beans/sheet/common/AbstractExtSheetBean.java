package jp.co.fj.keinavi.beans.sheet.common;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;

/**
 *
 * 拡張帳票データ作成Bean
 * 
 * 2005.10.31	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public abstract class AbstractExtSheetBean extends AbstractSheetBean {

	/**
	 * 帳票データを作成する
	 * 
	 * @throws Exception
	 */
	public abstract void execute() throws Exception;
	
	/**
	 * 帳票を作成する
	 * 
	 * @throws Exception
	 */
	protected abstract void create() throws Exception;
	
	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	public boolean createSheet() {

		try {
			create();
			return true;
		} catch (final Throwable e) {
			log.Err(null, e.getMessage(), e);
			return false;
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return null;
	}

	/**
	 * @param bundleCd 一括コード
	 * @return 「全国」の一括コードであるか
	 */
	protected boolean isWholeBundle(final String bundleCd) {
		return bundleCd.equals("99999");
	}
	
	/**
	 * @param bundleCd 一括コード
	 * @return 「県」の一括コードであるか
	 */
	protected boolean isPrefBundle(final String bundleCd) {
		return bundleCd.endsWith("999");
	}
	
	/**
	 * @param bundleCd 一括コード
	 * @return 「高校」の一括コードであるか
	 */
	protected boolean isSchoolBundle(final String bundleCd) {
		return !isWholeBundle(bundleCd) && !isPrefBundle(bundleCd);
	}
	
}

/**
 * 校内成績分析−過年度比較　偏差値帯別人数積上げ（5.0pt）
 * 	Excelファイル編集
 * 作成日: 2004/07/29
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S22BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S22Item;
import jp.co.fj.keinavi.excel.data.school.S22ListBean;
import jp.co.fj.keinavi.excel.data.school.S22TotalListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S22_06 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	final private int	intMaxSheetSr	= 20;			// 最大シート数	
	final private String	masterfile0		= "S22_06";		// ファイル名
	final private String	masterfile1		= "NS22_06";	// ファイル名
	private String	masterfile		= "";					// ファイル名
	final private int[]	tabCol			= {0,18};		// 表の基準点


/*
 * 	Excel編集メイン
 * 		S22Item s22Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s22_06EditExcel(S22Item s22Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("S22_06","S22_06帳票作成開始","");
		
		//テンプレートの決定
		if (s22Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			
			// データセット
			ArrayList	s22List			= s22Item.getS22List();
			Iterator	itr				= s22List.iterator();

			float		min				= 75.0f;// 偏差値
			int		row				= 0;	// 行
			int		writeCnt		= 0;	// 書き込みカウンター
			int		ninzu			= 0;	// *作成用
			int 		setRow			= -1;	// *セット用
			int		sumNinzu		= 0;	// 人数計算用
			int 		maxNen			= 5;	// 最大表示年度数
			int		hyouCnt			= 0;	// 値範囲：０〜１　(表カウンター)
			int		nendoCnt		= 0;	// 値範囲：０〜４　(年度カウンター)
			int		maxSheetIndex	= 0;	// シートカウンター
			int		intBookCngCount	= 1;	// ブックカウンター
			String		nendo			= "";	// 年度保持
			boolean	kataFlg			= true;// false:改行済(型・科目名変更済)　
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
//add 2004/10/26 T.Sakai データ0件対応
			int		dispKmkFlgCnt		= 0;		// グラフ表示フラグカウンタ
//add end
//add 2004/10/26 T.Sakai セル計算対応
			int		ruikeiNinzu			= 0;		// 累計人数
//add end
			
			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			// マスタExcel読み込み
			workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
			if( workbook==null ){
				return errfread;
			}

			// 基本ファイルを読込む
			S22ListBean s22ListBean = new S22ListBean();

			while( itr.hasNext() ) {
				s22ListBean = (S22ListBean)itr.next();
				if ( s22ListBean.getIntDispKmkFlg()==1 ) {
//add 2004/10/26 T.Sakai データ0件対応
					dispKmkFlgCnt++;
//add end
					// 基本ファイルを読み込む
					S22BnpListBean		s22BnpListBean		= new S22BnpListBean();
					S22TotalListBean	s22TotalListBean	= new S22TotalListBean();
	
					//データの保持
					ArrayList	s22BnpList		= s22ListBean.getS22BnpList();
					ArrayList	s22TotalList	= s22ListBean.getS22TotalList();
					Iterator	itrS22Bnp		= s22BnpList.iterator();
					Iterator	itrS22Total		= s22TotalList.iterator();
	
	
					//型から科目に変わる時のチェック
					if ( s22ListBean.getStrKmkCd().compareTo("7000") < 0 ) {
	
						//型・科目切替えの初回のみ処理する
						if (kataFlg){
							if(hyouCnt==1){
								hyouCnt	= 0;
								kataFlg	= false;
								bolSheetCngFlg	= true;
							}
							if(hyouCnt==0){
								kataFlg	= false;
							}
						}
					}
					
	
					if(bolSheetCngFlg){
						if( maxSheetIndex>=intMaxSheetSr ){
							// Excelファイル保存
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, maxSheetIndex);
							if( bolRet == false ){
								return errfwrite;					
							}
							intBookCngCount++;

							//マスタExcel読み込み
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook==null ){
								return errfread;
							}
							maxSheetIndex = 0;
						}
						// データセットするシートの選択
						workSheet = workbook.getSheet( Integer.toString(maxSheetIndex+1) );
						maxSheetIndex++;
	
						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);
	
						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, s22Item.getIntSecuFlg() ,32 ,33 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
						workCell.setCellValue(secuFlg);
	
						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
						workCell.setCellValue( "学校名　：" + cm.toString(s22Item.getStrGakkomei()) );
	
						// 対象模試セット
						String moshi =cm.setTaisyouMoshi( s22Item.getStrMshDate() );	// 模試月取得
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s22Item.getStrMshmei()) + moshi);
	
						bolSheetCngFlg =false;
					}
	
					// 型名・配点セット
					String haiten = "";
					if ( !cm.toString(s22ListBean.getStrHaitenKmk()).equals("") ) {
						haiten = "（" + s22ListBean.getStrHaitenKmk() + "）";
					}
					workCell = cm.setCell( workSheet, workRow, workCell, 6, tabCol[hyouCnt] );
					workCell.setCellValue( "型・科目：" + cm.toString(s22ListBean.getStrKmkmei()) + haiten );
	
					// 偏差値データセット
					row	= 0;
					if (s22Item.getIntShubetsuFlg() == 1){
						min = 180.0f;
					} else{
						min = 75.0f;
					}
					writeCnt=0;
					nendoCnt = 0;
					sumNinzu = 0;
//add 2004/10/26 T.Sakai セル計算対応
					ruikeiNinzu = 0;
//add end
					while(itrS22Bnp.hasNext()){
						s22BnpListBean	= (S22BnpListBean) itrS22Bnp.next();
	
						// 年度切り替え処理
						if(row != 0){
							if( !cm.toString(nendo).equals(cm.toString(s22BnpListBean.getStrNendo())) ){
								row=0;
//add 2004/10/26 T.Sakai セル計算対応
								ruikeiNinzu=0;
//add end
								writeCnt=0;
								if (s22Item.getIntShubetsuFlg() == 1){
									min = 180.0f;
								} else{
									min = 75.0f;
								}
								nendoCnt++;
								if(nendoCnt >= maxNen){
									break;
								}
							}
						}
	
						// 年度セット
						if(row == 0){
							if ( !cm.toString(s22BnpListBean.getStrNendo()).equals("") ) {
								workCell = cm.setCell( workSheet, workRow, workCell, 40, tabCol[hyouCnt]+1+3*nendoCnt );
								workCell.setCellValue( s22BnpListBean.getStrNendo() +"年度" );
							}
							nendo	= s22BnpListBean.getStrNendo();
						}
	
						// 人数セット
						if ( s22BnpListBean.getFloBnpMin()==min ) {
							if ( s22BnpListBean.getIntNinzu() != -999 ) {
								sumNinzu += s22BnpListBean.getIntNinzu();
							}
//add 2004/10/26 T.Sakai セル計算対応
							// 累計人数セット
							workCell = cm.setCell( workSheet, workRow, workCell,42+writeCnt, tabCol[hyouCnt]+3+3*nendoCnt);
							ruikeiNinzu = ruikeiNinzu + sumNinzu;
							workCell.setCellValue( ruikeiNinzu );

							// 得点用のときは処理しない
							if (s22Item.getIntShubetsuFlg() == 1){
							} else{
								if (s22BnpListBean.getFloBnpMin()==60.0f) {
									workCell = cm.setCell( workSheet, workRow, workCell,55, tabCol[hyouCnt]+1+3*nendoCnt);
									workCell.setCellValue( ruikeiNinzu );
								}
								if (s22BnpListBean.getFloBnpMin()==50.0f) {
									workCell = cm.setCell( workSheet, workRow, workCell,56, tabCol[hyouCnt]+1+3*nendoCnt);
									workCell.setCellValue( ruikeiNinzu );
								}
							}
//add end
							if ( sumNinzu != 0 ) {
								workCell = cm.setCell( workSheet, workRow, workCell,42+writeCnt, tabCol[hyouCnt]+2+3*nendoCnt);
								workCell.setCellValue( sumNinzu );
	
								// *セット準備
								if( sumNinzu > ninzu ){
									ninzu = sumNinzu;
									setRow = writeCnt;
								}
							}
							writeCnt++;
							sumNinzu = 0;
							if (s22Item.getIntShubetsuFlg() == 1){
								min -= 20.0f;
							} else{
								min -= 5.0f;
							}
						} else {
							if ( s22BnpListBean.getIntNinzu() != -999 ) {
								sumNinzu +=s22BnpListBean.getIntNinzu();
							}
							// *セット
							if(s22BnpListBean.getFloBnpMin() == -999.0){
//add 2004/10/26 T.Sakai セル計算対応
								// 累計人数セット
								workCell = cm.setCell( workSheet, workRow, workCell,42+writeCnt, tabCol[hyouCnt]+3+3*nendoCnt);
								ruikeiNinzu = ruikeiNinzu + sumNinzu;
								workCell.setCellValue( ruikeiNinzu );
//add end
								if ( sumNinzu != 0 ) {
									workCell = cm.setCell( workSheet, workRow, workCell,42+writeCnt, tabCol[hyouCnt]+2+3*nendoCnt);
									workCell.setCellValue( sumNinzu );
	
									// *セット準備
									if( sumNinzu > ninzu ){
										ninzu = sumNinzu;
										setRow = writeCnt;
									}
								}
								sumNinzu = 0;
	
								if(setRow != -1){
									workCell = cm.setCell( workSheet, workRow, workCell,42+setRow, tabCol[hyouCnt]+1+3*nendoCnt );
									workCell.setCellValue("*");
								}
								ninzu=0;
								setRow=-1;
							}
						}
	
						row++;
					}
	
					//　平均データセット
					nendoCnt	= 0;
					while(itrS22Total.hasNext()){
						s22TotalListBean	= (S22TotalListBean) itrS22Total.next();

						// 合計人数セット
						if ( s22TotalListBean.getIntTotalNinzu() != -999 ) {
							workCell = cm.setCell( workSheet, workRow, workCell,52, tabCol[hyouCnt]+1+3*nendoCnt );
							workCell.setCellValue( s22TotalListBean.getIntTotalNinzu() );
						}
	
						// 平均点セット
						if ( s22TotalListBean.getFloHeikin() != -999.0 ) {
							workCell = cm.setCell( workSheet, workRow, workCell,53, tabCol[hyouCnt]+1+3*nendoCnt );
							workCell.setCellValue( s22TotalListBean.getFloHeikin() );
						}
	
						// 平均偏差値セット
						if ( s22TotalListBean.getFloHensa() != -999.0 ) {
							workCell = cm.setCell( workSheet, workRow, workCell,54, tabCol[hyouCnt]+1+3*nendoCnt );
							workCell.setCellValue( s22TotalListBean.getFloHensa()); 
						}
	
						nendoCnt++;
						if(nendoCnt>=maxNen){
							break;
						}
					}
					hyouCnt++;
					if(hyouCnt>=2){
						hyouCnt = 0;
						bolSheetCngFlg= true;
					}
				}
			}
//add 2004/10/26 T.Sakai データ0件対応
			if ( s22List.size()==0 || dispKmkFlgCnt==0 ) {
				// データセットするシートの選択
				workSheet = workbook.getSheet( Integer.toString(maxSheetIndex+1) );
				maxSheetIndex++;
	
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
	
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s22Item.getIntSecuFlg() ,32 ,33 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
				workCell.setCellValue(secuFlg);
	
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s22Item.getStrGakkomei()) );
	
				// 対象模試セット
				String moshi =cm.setTaisyouMoshi( s22Item.getStrMshDate() );	// 模試月取得
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s22Item.getStrMshmei()) + moshi);
			}
//add end
			
			boolean bolRet = false;
			// Excelファイル保存
			if(intBookCngCount!=1){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, maxSheetIndex);
			}else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;
			}

			
		} catch(Exception e) {
			log.Err("S22_06","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S22_06","S22_06帳票作成終了","");
		return noerror;
	}

}
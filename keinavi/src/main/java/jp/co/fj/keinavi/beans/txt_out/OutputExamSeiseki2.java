package jp.co.fj.keinavi.beans.txt_out;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.util.KNUtil;

import org.apache.commons.lang.StringUtils;

/**
 * 模試別成績データ（新方式）
 * 
 * 2004.10.10 	Yoshimoto KAWAI - Totec
 * 				[新規作成]
 * 
 * <2010年度改修>
 * 2009.12.01   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応 
 *              
 * 2010.04.20	Tomohisa YAMADA - Totec
 * 				プレステージ模試対応
 *
 */
public class OutputExamSeiseki2 extends OutputExamSeiseki {
	
	// 実行関数
	public void execute() throws Exception {
		
		super.init();
		
		// 担当クラスを一時テーブルへ展開する
		super.setupCountChargeClass();
		
		OutPutParamSet set = new OutPutParamSet(login, exam, profile, "t104", sessionKey);

		List header = set.getHeader();
		File outPutFile = set.getOutPutFile();
		OutputTextBean out = new OutputTextBean();
		out.setOutFile(outPutFile);	
		out.setOutputTextList(getAllData());
		//---------------------------------------------
		// [2004/11/09 nino] ヘッダ部の配点名を再設定
		// [2004/11/01 nino] ヘッダ部の科目名を再設定
		//---------------------------------------------
		header = setExamName(header, exam.getExamCD());
		//---------------------------------------------

		// ------------------------------------------------------------
        // 2005.02.16
        // Yoshimoto KAWAI - Totec
		// 余白部分の項目名書き換え
		// センターリサーチの場合は
		// 現文換算→現文得点
		// 古文換算→古文得点
		// 漢文換算→漢文得点
		//
		// マーク系
		if ("01".equals(exam.getExamTypeCD())) {
		    ((String[]) header.get(146))[2] = "英語+L";
		    ((String[]) header.get(147))[2] = this.cr ? "現文得点" : "現文換算";
		    ((String[]) header.get(148))[2] = this.cr ? "古文得点" : "古文換算";
		    ((String[]) header.get(149))[2] = this.cr ? "漢文得点" : "漢文換算";
		    ((String[]) header.get(150))[2] = "余白1";
		    ((String[]) header.get(151))[2] = "余白2";
		    ((String[]) header.get(152))[2] = "余白3";
		    ((String[]) header.get(153))[2] = "余白4";
		    ((String[]) header.get(154))[2] = "余白5";
		// 記述系または高３プレステージ
		} else if ("02".equals(exam.getExamTypeCD()) 
				|| "03".equals(exam.getExamTypeCD())
				|| KNUtil.isH3PStage(exam)) {
		    ((String[]) header.get(150))[2] = "英リス選";
		    ((String[]) header.get(151))[2] = "物理選択";
		    ((String[]) header.get(152))[2] = "化学選択";
		    ((String[]) header.get(153))[2] = "生物選択";
		    ((String[]) header.get(154))[2] = "地学選択";
		}
        // ------------------------------------------------------------
		
        // ------------------------------------------------------------
		// 受験学力測定テスト対応
		if (KNUtil.isAbilityExam(exam)) {
			for (int i = 0; i < 23; i++) {
				final String num = StringUtils.leftPad((i + 1) + "", 2 , '0');
				((String[]) header.get(11 + 6 * i))[2] = "ス" + num;
				((String[]) header.get(12 + 6 * i))[2] = "到" + num;
			}
		}
        // ------------------------------------------------------------
		
		out.setHeadTextList(header);
		out.setOutTarget(fixTargets(set.getTarget()));
		out.setOutType(set.getOutType());
		out.execute();
		
		this.outfileList.add(outPutFile.getPath());
		sheetLog.add(outPutFile.getName());
	}
	
	/**
	 * プロファイル値が228以上を破棄する。
	 * @param targets プロファイル保存値
	 * @return 変換後値
	 */
	private Integer[] fixTargets(Integer[] targets) {
		List list = new LinkedList();		
		for (int i = 0; i < targets.length; i ++) {
			if (targets[i].intValue() > 227) {
				continue;
			}
			list.add(targets[i]);
		}
		return (Integer[]) list.toArray(new Integer[0]);
	}

	/**
	 * 結果を編集
	 * @param seisekiMap
	 * @param candMap
	 * @return
	 */
	protected LinkedList editData(LinkedHashMap seisekiMap, LinkedHashMap candMap) throws FileNotFoundException, IOException {

		// [2004/11/01 nino] ソート順を変更（学年、クラス、クラス番号）
		// 成績側の個人IDをソートする
		ArrayList mstList = new ArrayList();
		Set key = seisekiMap.keySet();
		Iterator it = key.iterator();
		while (it.hasNext()) {
			mstList.add((String)it.next());
		}
		// Collections.sort(mstList);  // ★[2004/11/01 nino] 個人IDでのソートをやめる★
		
		// 対象模試から表示順情報を取得する
		// [2004/11/10 nino] 模試科目1〜23で科目コード重複の対策でorderStr[][]を使用
		//                   orderStr[0] : ダミー
		//                   orderStr[1]〜orderStr[22] : 科目1〜科目22
		String[][] orderStr = new String[24][];
		getShowOrder(exam, orderStr);
		
		// 各Mapから個人IDをkeyとする情報を取得
		Iterator mstit = mstList.iterator();
		LinkedList returnList = new LinkedList();
		//最初の行をセット
		//setFirstLise(returnList, targetExam);
		while (mstit.hasNext()) {
			String indkey = (String)mstit.next();
			LinkedList seisekiList = (LinkedList)seisekiMap.get(indkey);
			LinkedList candList = null;
			if (candMap.containsKey(indkey)) {
				candList = (LinkedList)candMap.get(indkey);
			}
			else {
				candList = new LinkedList();
			}
			
			// 個人データをセット
			setLineData(seisekiList, candList, orderStr, returnList);
		}
		
		return returnList;
	}
	
	/**
	 * 模試ごとに参照するプロパティファイルを指定する
	 * @param exam 対象模試
	 * @return
	 */
	protected String getPropertyFileName(ExamData exam) {
		
		//「2010年度マーク高２模試」の場合のみ「n_subjectmaster26.properties」を使用する。
		if ("2010".equals(exam.getExamYear()) && "66".equals(exam.getExamCD())) {
			this.open = false;
			return "n_subjectmaster26.properties";
		}
		
		return "n_" + super.getPropertyFileName(exam);
	}

	/**
	 * ヘッダ科目名を取得
	 * @param targetExam
	 * @return
	 */
	protected String[] getHeadProperties(String targetExam) throws FileNotFoundException, IOException {

		// 設定を読み込む
		InputStream in = OutputExamSeiseki2.class.getResourceAsStream("head2.properties");

		if (in == null) {
			 throw new IllegalArgumentException("properties file not found.");
		}

		Properties prop = new Properties();
		try {
			 prop.load(in);
		} catch (IOException e) {
			 e.printStackTrace();
		}

		// ------------------------------------------------------------
		// 2005.02.16
		// Yoshimoto KAWAI - Totec
		// 項目名のグループ分け方法を変更
		//
		
		// 高１プレステージ
		if (KNUtil.isH1PStage(exam)) {
			return splitComma(prop.getProperty("HEADNAME13"));	
		}
		
		// 高２プレステージ
		if (KNUtil.isH2PStage(exam)) {
			return splitComma(prop.getProperty("HEADNAME14"));		
		}
		
		// 高３プレステージ
		if (KNUtil.isH3PStage(exam)) {
			return splitComma(prop.getProperty("HEADNAME15"));		
		}
		
		// マーク高２
		if ("66".equals(exam.getExamCD())) {
			
			//「2010年度マーク高２模試」の場合
			if ("2010".equals(exam.getExamYear())) {
				return splitComma(prop.getProperty("HEADNAME16"));
			}
			
			return splitComma(prop.getProperty("HEADNAME2"));		
			
		// その他マーク系
		} else if ("01".equals(exam.getExamTypeCD())){
			return splitComma(prop.getProperty("HEADNAME1"));
			
		// 高２記述
		} else if ("65".equals(exam.getExamCD()) ) {
			return splitComma(prop.getProperty("HEADNAME12"));

		// その他記述系
		} else if ("02".equals(exam.getExamTypeCD())){
			return splitComma(prop.getProperty("HEADNAME3"));

		// 私大
		} else if ("03".equals(exam.getExamTypeCD())){
			return splitComma(prop.getProperty("HEADNAME4"));
			
		// 高１記述
		} else if ("75".equals(exam.getExamCD())) {
			return splitComma(prop.getProperty("HEADNAME11"));

		// 高１
		} else if ("04".equals(exam.getExamTypeCD())
				&& "01".equals(exam.getTargetGrade())){
			return splitComma(prop.getProperty("HEADNAME5"));
			
		// 高２
		} else if ("04".equals(exam.getExamTypeCD())
				&& "02".equals(exam.getTargetGrade())){
			return splitComma(prop.getProperty("HEADNAME6"));
			
		// 模試種類コード04は1・2年生のみ
		} else if ("04".equals(exam.getExamTypeCD())) {
			throw new InternalError("マスタ不備：模試種類コード04は1・2年生のみ");
			
		// 受験学力測定テスト（高１用）
		} else if ("01".equals(exam.getTargetGrade()) && KNUtil.isAbilityExam(exam)){
			return splitComma(prop.getProperty("HEADNAME8"));

		// 受験学力測定テスト（高２用）
		} else if ("02".equals(exam.getTargetGrade()) && KNUtil.isAbilityExam(exam)){
			return splitComma(prop.getProperty("HEADNAME9"));

		// 受験学力測定テスト（高３用）
		} else if ("03".equals(exam.getTargetGrade()) && KNUtil.isAbilityExam(exam)){
			return splitComma(prop.getProperty("HEADNAME10"));

		// オープン系
		} else {
			return splitComma(prop.getProperty("HEADNAME7"));
			
		}
		// ------------------------------------------------------------
	}

	/**
	 * ヘッダ項目の配点（換算得点）、科目名をセット
	 * @param headList
	 * @param targetExam
	 * @param sExamTypeMark
	 */
	protected LinkedList setExamName(List headerList, String targetExam) throws FileNotFoundException, IOException {
		final int SEISEKISET = 6;			// 成績データ集合
		final int SUBLENGTH = 23;			// 一行に表示する成績データの個数
		final int HEADLENGTH = 8;			// 行ヘッダの長さ
		
		String[] headExam = getHeadProperties(targetExam);
		LinkedList headData = new LinkedList();
				
		// 科目名をセット
		String str	 = null;
		int iPos = 0;
		for(int listCnt=0; listCnt < headerList.size(); listCnt++){
			String[] data = (String[])headerList.get(listCnt);

			if ( HEADLENGTH <= listCnt && listCnt < HEADLENGTH + SEISEKISET * SUBLENGTH ) {

				iPos = (listCnt - HEADLENGTH) % SEISEKISET;
				if ( iPos == 1 && data.length > 2 ) {
					// 科目名
					str = data[2];
					str =  headExam[(int) ((listCnt - HEADLENGTH) / SEISEKISET)];
					data[2] = str;
				}

				// [2004/11/09 nino] 配点（記述模試の場合）／換算得点（マーク模試の場合）
				//                   デフォルトは"配01"〜"配22"
				if ( iPos == 2 && data.length > 2 ) {
					str = data[2];
					// ターゲット模試がマーク模試かをチェック
					if (this.mark
							&& !"05".equals(str.substring(1))
							&& !"21".equals(str.substring(1))
							&& !"22".equals(str.substring(1))
							&& !"23".equals(str.substring(1))){
						str = "換" + str.substring(1);
					}
					data[2] = str;
				}				
			}
			headData.add(data);
		}
		return headData;
	}

	/**
	 * 行情報をセット
	 * @param seisekiList 成績情報格納リスト（内部はString[10]）
	 * @param candList    志望校情報格納リスト（内部はString[13]）
	 * @param orderMap    表示順情報（key:科目コード、Object:String=表示位置１〜２２）
	 * @param orderStr    表示順情報（[2004/11/10 nino] 模試コード重複対応用）
	 * @param returnList 
	 */
	protected void setLineData(LinkedList seisekiList, LinkedList candList, String[][] orderStr, LinkedList returnList) {
		final int CANDIDATEPOS = 156;
		final int SEISEKISET = 6;
		final int HEADLENGTH = 8;			// 行ヘッダの長さ
		final int CANDSET = 8;			// 志望校データ集合
		
		// 配点／換算得点
		final int INDEX3;
		if (this.mark) INDEX3 = 10; // マーク系は換算得点
		else INDEX3 = 14; // それ以外は配点

		// マーク系の現古（科目７）の値
		String genko = null;
		// 採用した配点の値を覚えておく
		Map taken = new HashMap(); 
		
		// [2004/11/01 nino] 表示順序対応
		// 科目の位置で複数の科目が存在する場合は表示順序の若いものを表示する
		LinkedHashMap dispMap = new LinkedHashMap();	// 表示順序リスト

		LinkedHashMap lineMap = new LinkedHashMap();
		//行ヘッダを作成する
		String[] header = (String[])seisekiList.get(0);
		lineMap.put(new Integer("1"),header[0]);		// 通番（個人ID）
		lineMap.put(new Integer("2"),header[1]);		// 年度
		lineMap.put(new Integer("3"),header[2]);		// 模試コード
		lineMap.put(new Integer("4"),header[3]);		// 学校コード
		lineMap.put(new Integer("5"),header[4]);		// 学年
		lineMap.put(new Integer("6"),header[5]);		// クラス
		lineMap.put(new Integer("7"),header[6]);		// クラス番号
		lineMap.put(new Integer("8"),header[7]);		// カナ氏名

		// 記述系・私大の場合は不足情報のデフォルト値をセットしておく
		if (this.isWrtnOrPrivate) {
			lineMap.put(new Integer("151"), "9");
			lineMap.put(new Integer("152"), "9");
			lineMap.put(new Integer("153"), "9");
			lineMap.put(new Integer("154"), "9");
			lineMap.put(new Integer("155"), "9");					
		}
		
		Iterator seiIt = seisekiList.iterator();
		while(seiIt.hasNext()) {
			String[] seiseki = (String[])seiIt.next();

			// [2004/10/29 nino] SUBSTR(C.SUBCD,1,3) → SUBSTR(C.SUBCD,1,4) に変更
			//                   ※テキスト出力は科目コード４桁に変更
			//                     科目コードのkeyは３桁のまま判定
			//String subcd = seiseki[8];					// 科目コードを取得
//			String subcd;
//			if (seiseki[8].length() >= 3) {
//				subcd = seiseki[8].substring(0, 3);			// 科目コード先頭3桁を取得
//			}else{
//				subcd = seiseki[8];							// 科目コードを取得
//			}
			// ------------------------------------------------------------
			// 2004.12.01
			// Yoshimoto KAWAI - Totec
			// オープン系は４桁のままにする
			String subcd;
			if (this.open) {
				subcd = seiseki[8];							// 科目コードを取得
			} else {
				subcd = seiseki[8].substring(0, 3);			// 科目コード先頭3桁を取得
			}
			// ------------------------------------------------------------

			//------------------------------------------------------------------------------
			// [2004/11/10 nino] 模試コード重複対応
			//↓----------------------------------------------------------------------------
			//if (orderMap.containsKey(subcd)) {
			//	String pos = (String)orderMap.get(subcd);		// その科目の表示位置を取得
			//	// [nino 2004/10/25] 表示位置"9"からセットするため(-1)する
			//	// int iPos = new Integer(pos).intValue();		// 表示位置をkeyにMapに格納
			//	int iPos = new Integer(pos).intValue() - 1;		// 表示位置をkeyにMapに格納
			//------------------------------------------------------------------------------
			int subPos = 0;									// 表示位置初期化（ダミー位置）
			while(subPos < orderStr.length) {
				subPos++;
				subPos = getSubjectPos(orderStr,subcd,subPos);	// その科目の表示位置を取得
				if (subPos >= orderStr.length) {
					break;
				}
				int iPos = subPos - 1;							// 表示位置をkeyにMapに格納
				//↑----------------------------------------------------------------------------

				// ------------------------------------------------------------
				// 2005.02.15
				// Yoshimoto KAWAI - Totec
				// 特定の科目名をセットする
				setSubjectName(seiseki);
				// ------------------------------------------------------------

				//----------------------------------------------------------------------------------------	
				// [2004/11/01 nino] 表示順序対応
				//↓--------------------------------------------------------------------------------------

				if ( seiseki[13] == null ){
					seiseki[13] = seiseki[8];
				}
				if (!dispMap.containsKey(new Integer(iPos))) {
					dispMap.put(new Integer(iPos),new String("10000"));
				}
				int dispSequence = Integer.valueOf(dispMap.get(new Integer(iPos)).toString()).intValue();
				if ( dispSequence > Integer.valueOf(seiseki[13]).intValue() ) {
					// 表示順序格納
					dispMap.put(new Integer(iPos), seiseki[13]);	
					//↑--------------------------------------------------------------------------------------	

					lineMap.put(new Integer(iPos * SEISEKISET + HEADLENGTH + 1 ), seiseki[8]);
					lineMap.put(new Integer(iPos * SEISEKISET + HEADLENGTH + 2 ), seiseki[9]);
					lineMap.put(new Integer(iPos * SEISEKISET + HEADLENGTH + 3 ), seiseki[INDEX3]);
					lineMap.put(new Integer(iPos * SEISEKISET + HEADLENGTH + 4 ), seiseki[11]);
					lineMap.put(new Integer(iPos * SEISEKISET + HEADLENGTH + 5 ), seiseki[12]);
					lineMap.put(new Integer(iPos * SEISEKISET + HEADLENGTH + 6 ), seiseki[16]);	// 学力レベル
					
					// 配点で採用された科目は覚えておく
					taken.put(new Integer(iPos * SEISEKISET + HEADLENGTH + 3 ), seiseki[14]);
				}
			}
			
			// マーク系のみ対応
			if (this.mark) {
			    
				// リスニング偏差値
				if ("119".equals(subcd)) {
					lineMap.put(new Integer("147"), seiseki[12]);
				}

				// 換算得点の処理
				String value;
				if (this.cr) value = seiseki[11]; // センタリサーチは得点
				else value = seiseki[10]; // それ以外は換算得点
				// 現文換算
				if ("312".equals(subcd) || "317".equals(subcd) || "357".equals(subcd)) {
					lineMap.put(new Integer("148"), value);
					genko = calculateCVSScore(genko, seiseki[10]);
				}
				// 古文換算
				if ("327".equals(subcd) || "367".equals(subcd) || "322".equals(subcd)) {
					lineMap.put(new Integer("149"), value);
					genko = calculateCVSScore(genko, seiseki[10]);
				}
				// 漢文換算
				if ("337".equals(subcd) || "377".equals(subcd) || "332".equals(subcd)) {
					lineMap.put(new Integer("150"), value);
				}
				
			// 記述系・私大のみの対応
			} else if (this.isWrtnOrPrivate) {
				// 英語不足
				if ("111".equals(subcd)) {
					lineMap.put(new Integer("151"), seiseki[15]);
					
				// 物理不足
				} else if ("428".equals(subcd)) {
					lineMap.put(new Integer("152"), seiseki[15]);
					
				// 化学不足
				} else if ("438".equals(subcd)) {
					lineMap.put(new Integer("153"), seiseki[15]);
					
				// 生物不足
				} else if ("448".equals(subcd)) {
					lineMap.put(new Integer("154"), seiseki[15]);
					
				// 地学不足
				} else if ("458".equals(subcd)) {
					lineMap.put(new Integer("155"), seiseki[15]);
				}					
			}
		}
		
		// マーク系のみの対応
		if (this.mark) {
			// 現古の値を書き換える
			lineMap.put(new Integer("47"), genko);
			// 数学３は換算得点ではなく配点を入れる
			lineMap.put(new Integer("35"), taken.get(new Integer("35")));	
			// 総合１は換算得点ではなく配点を入れる
			lineMap.put(new Integer("131"), taken.get(new Integer("131")));	
			// 総合２は換算得点ではなく配点を入れる
			lineMap.put(new Integer("137"), taken.get(new Integer("137")));	
			// 総合３は換算得点ではなく配点を入れる
			lineMap.put(new Integer("143"), taken.get(new Integer("143")));	
		}
		
		Iterator candIt = candList.iterator();
		while(candIt.hasNext()) {
			String[] cand = (String[])candIt.next();
			String candcd = cand[1];					// 志望順を取得
			int iCandPos = new Integer(candcd).intValue() - 1;	// 表示位置をkeyにMapを格納
			lineMap.put(new Integer(iCandPos * CANDSET + CANDIDATEPOS), cand[11]);	// 大学コード(5桁)
			lineMap.put(new Integer(iCandPos * CANDSET + CANDIDATEPOS + 1), cand[2]);
			lineMap.put(new Integer(iCandPos * CANDSET + CANDIDATEPOS + 2), cand[3]);
			lineMap.put(new Integer(iCandPos * CANDSET + CANDIDATEPOS + 3), cand[6]);
			lineMap.put(new Integer(iCandPos * CANDSET + CANDIDATEPOS + 4), cand[7]);
			lineMap.put(new Integer(iCandPos * CANDSET + CANDIDATEPOS + 5), cand[8]);
			lineMap.put(new Integer(iCandPos * CANDSET + CANDIDATEPOS + 6), cand[9]);
			lineMap.put(new Integer(iCandPos * CANDSET + CANDIDATEPOS + 7), cand[10]);
		}
		
		// 不足分のkeyをセット
		setLackKey(lineMap, 227);
		// Mapをソート
		
		// 行情報を追加
		returnList.add(lineMap);
	}

	/**
	 * 現古の換算得点を計算して返す
	 */
	private String calculateCVSScore(final String genko, final String value) {
		if (value == null) return genko;
		if (genko == null) 	return value;

		return String.valueOf(Integer.parseInt(value) + Integer.parseInt(genko));
	}
	
}

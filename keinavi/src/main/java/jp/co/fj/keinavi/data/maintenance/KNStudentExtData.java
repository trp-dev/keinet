package jp.co.fj.keinavi.data.maintenance;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.ComparatorUtils;
import org.apache.commons.collections.comparators.NullComparator;

/**
 *
 * 拡張生徒情報データ
 * 
 * 2006.10.06	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class KNStudentExtData extends KNStudentData {

	// 受験模試マップ
	private Map takenExamMap = new HashMap();
	
	// 学年ソートキー（前々年度）
	private String gradeKey1;
	// 学年ソートキー（前年度）
	private String gradeKey2;
	// 学年ソートキー（今年度）
	private String gradeKey3;
	// 選択フラグ
	// 1 ... 主
	// 2 ... 従
	// 3 ... なし
	private int selectedFlag = 3;
	// 識別マーク
	private String cmark;
	
	// コンストラクタ
	private KNStudentExtData() {
		super(0);
	}
	
	/**
	 * コンストラクタ
	 * 
	 * @param pYearDiff 現年度からの差分
	 */
	public KNStudentExtData(int pYearDiff) {
		super(pYearDiff);
	}

	/**
	 * 受験模試を追加する
	 * 
	 * @param examYear 模試年度
	 * @param examCd 模試コード
	 */
	public void addTakenExam(final String examYear, final String examCd) {
		if (examYear != null && examCd != null) {
			takenExamMap.put(examYear + examCd, Boolean.TRUE);
		}
	}

	/**
	 * @param pTakenExamMap 設定する takenExamMap。
	 */
	public void setTakenExamMap(Map pTakenExamMap) {
		takenExamMap = pTakenExamMap;
	}

	/**
	 * @return takenExamMap を戻します。
	 */
	public Map getTakenExamMap() {
		return takenExamMap;
	}

	/**
	 * @return gradeKey1 を戻します。
	 */
	public String getGradeKey1() {
		return gradeKey1;
	}

	/**
	 * @param pGradeKey1 設定する gradeKey1。
	 */
	public void setGradeKey1(String pGradeKey1) {
		gradeKey1 = pGradeKey1;
	}

	/**
	 * @return gradeKey2 を戻します。
	 */
	public String getGradeKey2() {
		return gradeKey2;
	}

	/**
	 * @param pGradeKey2 設定する gradeKey2。
	 */
	public void setGradeKey2(String pGradeKey2) {
		gradeKey2 = pGradeKey2;
	}

	/**
	 * @return gradeKey3 を戻します。
	 */
	public String getGradeKey3() {
		return gradeKey3;
	}

	/**
	 * @param pGradeKey3 設定する gradeKey3。
	 */
	public void setGradeKey3(String pGradeKey3) {
		gradeKey3 = pGradeKey3;
	}
	
	/**
	 * @return selectedFlag を戻します。
	 */
	public int getSelectedFlag() {
		return selectedFlag;
	}

	/**
	 * @param pSelectedFlag 設定する selectedFlag。
	 */
	public void setSelectedFlag(int pSelectedFlag) {
		selectedFlag = pSelectedFlag;
	}

	/**
	 * @return cmark を戻します。
	 */
	public String getCmark() {
		return cmark;
	}

	/**
	 * @param pCmark 設定する cmark。
	 */
	public void setCmark(String pCmark) {
		cmark = pCmark;
	}

	// 学年・クラス・クラス番号Comparator
	public static Comparator getBaseComparator(final int yearDiff) {
		
		final List list = new ArrayList();
		
		// 今年度
		if (yearDiff == 0) {
			list.add(new KNStudentExtData().new CurrentGradeComparator());
			list.add(new KNStudentExtData().new CurrentClassNameComparator());
			list.add(new KNStudentExtData().new CurrentClassNoComparator());
		// 前年度
		} else if (yearDiff == 1) {
			list.add(new KNStudentExtData().new LastGradeComparator());
			list.add(new KNStudentExtData().new LastClassNameComparator());
			list.add(new KNStudentExtData().new LastClassNoComparator());
		// 前々年度
		} else if (yearDiff == 2) {
			list.add(new KNStudentExtData().new BeforeLastGradeComparator());
			list.add(new KNStudentExtData().new BeforeLastClassNameComparator());
			list.add(new KNStudentExtData().new BeforeLastClassNoComparator());
		} else {
			throw new IllegalArgumentException(
					"年度差分が不正です。" + yearDiff);
		}
		
		// 個人IDで一意となる
		list.add(new KNStudentExtData().new IndividualIdComparator());
		
		return ComparatorUtils.chainedComparator(list);
	}

	// かな氏名Comparator
	public static Comparator getNameKanaComparator() {
		return ComparatorUtils.chainedComparator(
				new KNStudentExtData().new NameKanaComparator(),
				new KNStudentExtData().new IndividualIdComparator());
	}
	
	// 漢字氏名Comparator
	public static Comparator getNameKanjiComparator() {
		return ComparatorUtils.chainedComparator(
				new KNStudentExtData().new NameKanjiComparator(),
				new KNStudentExtData().new IndividualIdComparator());
	}
	
	// 生年月日Comparator
	public static Comparator getBirthdayComparator() {
		return ComparatorUtils.chainedComparator(
				new KNStudentExtData().new BirthdayComparator(),
				new KNStudentExtData().new IndividualIdComparator());
	}
	
	// 電話番号Comparator
	public static Comparator getTelNoComparator() {
		return ComparatorUtils.chainedComparator(
				new KNStudentExtData().new TelNoComparator(),
				new KNStudentExtData().new IndividualIdComparator());
	}
		
	// 基本Comparator
	private abstract class BaseComparator extends NullComparator {

		/**
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(final Object pSelf, final Object pTarget) {
			final KNStudentExtData self = (KNStudentExtData) pSelf;
			final KNStudentExtData target = (KNStudentExtData) pTarget;
			return super.compare(getObject(self), getObject(target));
		}

		// 比較対象オブジェクトを返す
		public abstract Object getObject(KNStudentExtData data);
		
	}

	// 学年（前々年度）のComparator
	private class BeforeLastGradeComparator extends BaseComparator {

		/**
		 * @see jp.co.fj.keinavi.data.maintenance.KNStudentExtData.BaseComparator#getObject(
		 * 			jp.co.fj.keinavi.data.maintenance.KNStudentExtData)
		 */
		public Object getObject(final KNStudentExtData data) {
			return data.getGradeKey1();
		}
		
	}

	// クラス（前々年度）のComparator
	private class BeforeLastClassNameComparator extends BaseComparator {

		/**
		 * @see jp.co.fj.keinavi.data.maintenance.KNStudentExtData.BaseComparator#getObject(
		 * 			jp.co.fj.keinavi.data.maintenance.KNStudentExtData)
		 */
		public Object getObject(final KNStudentExtData data) {
			return data.getBeforeLastClassName();
		}
		
	}

	// クラス番号（前々年度）のComparator
	private class BeforeLastClassNoComparator extends BaseComparator {

		/**
		 * @see jp.co.fj.keinavi.data.maintenance.KNStudentExtData.BaseComparator#getObject(
		 * 			jp.co.fj.keinavi.data.maintenance.KNStudentExtData)
		 */
		public Object getObject(final KNStudentExtData data) {
			return data.getBeforeLastClassNo();
		}
		
	}

	// 学年（前年度）のComparator
	private class LastGradeComparator extends BaseComparator {

		/**
		 * @see jp.co.fj.keinavi.data.maintenance.KNStudentExtData.BaseComparator#getObject(
		 * 			jp.co.fj.keinavi.data.maintenance.KNStudentExtData)
		 */
		public Object getObject(final KNStudentExtData data) {
			return data.getGradeKey2();
		}
		
	}

	// クラス（前年度）のComparator
	private class LastClassNameComparator extends BaseComparator {

		/**
		 * @see jp.co.fj.keinavi.data.maintenance.KNStudentExtData.BaseComparator#getObject(
		 * 			jp.co.fj.keinavi.data.maintenance.KNStudentExtData)
		 */
		public Object getObject(final KNStudentExtData data) {
			return data.getLastClassName();
		}
		
	}

	// クラス番号（前年度）のComparator
	private class LastClassNoComparator extends BaseComparator {

		/**
		 * @see jp.co.fj.keinavi.data.maintenance.KNStudentExtData.BaseComparator#getObject(
		 * 			jp.co.fj.keinavi.data.maintenance.KNStudentExtData)
		 */
		public Object getObject(final KNStudentExtData data) {
			return data.getLastClassNo();
		}
			
	}

	// 学年（今年度）のComparator
	private class CurrentGradeComparator extends BaseComparator {

		/**
		 * @see jp.co.fj.keinavi.data.maintenance.KNStudentExtData.BaseComparator#getObject(
		 * 			jp.co.fj.keinavi.data.maintenance.KNStudentExtData)
		 */
		public Object getObject(final KNStudentExtData data) {
			return data.getGradeKey3();
		}
		
	}

	// クラス（今年度）のComparator
	private class CurrentClassNameComparator extends BaseComparator {

		/**
		 * @see jp.co.fj.keinavi.data.maintenance.KNStudentExtData.BaseComparator#getObject(
		 * 			jp.co.fj.keinavi.data.maintenance.KNStudentExtData)
		 */
		public Object getObject(final KNStudentExtData data) {
			return data.getCurrentClassName();
		}
		
	}

	// クラス番号（今年度）のComparator
	private class CurrentClassNoComparator extends BaseComparator {

		/**
		 * @see jp.co.fj.keinavi.data.maintenance.KNStudentExtData.BaseComparator#getObject(
		 * 			jp.co.fj.keinavi.data.maintenance.KNStudentExtData)
		 */
		public Object getObject(final KNStudentExtData data) {
			return data.getCurrentClassNo();
		}
		
	}

	// かな氏名のComparator
	private class NameKanaComparator extends BaseComparator {
		
		/**
		 * @see jp.co.fj.keinavi.data.maintenance.KNStudentExtData.BaseComparator#getObject(
		 * 			jp.co.fj.keinavi.data.maintenance.KNStudentExtData)
		 */
		public Object getObject(final KNStudentExtData data) {
			return data.getNameHiragana();
		}
		
	}

	// 漢字氏名のComparator
	private class NameKanjiComparator extends BaseComparator {

		/**
		 * @see jp.co.fj.keinavi.data.maintenance.KNStudentExtData.BaseComparator#getObject(
		 * 			jp.co.fj.keinavi.data.maintenance.KNStudentExtData)
		 */
		public Object getObject(final KNStudentExtData data) {
			return data.getNameKanji();
		}
		
	}

	// 生年月日のComparator
	private class BirthdayComparator extends BaseComparator {

		/**
		 * @see jp.co.fj.keinavi.data.maintenance.KNStudentExtData.BaseComparator#getObject(
		 * 			jp.co.fj.keinavi.data.maintenance.KNStudentExtData)
		 */
		public Object getObject(final KNStudentExtData data) {
			return data.getBirthday();
		}
		
	}

	// 電話番号のComparator
	private class TelNoComparator extends BaseComparator {
		
		/**
		 * @see jp.co.fj.keinavi.data.maintenance.KNStudentExtData.BaseComparator#getObject(
		 * 			jp.co.fj.keinavi.data.maintenance.KNStudentExtData)
		 */
		public Object getObject(final KNStudentExtData data) {
			return data.getTelNo();
		}
		
	}

	// 個人IDのComparator
	private class IndividualIdComparator extends BaseComparator {

		/**
		 * @see jp.co.fj.keinavi.data.maintenance.KNStudentExtData.BaseComparator#getObject(
		 * 			jp.co.fj.keinavi.data.maintenance.KNStudentExtData)
		 */
		public Object getObject(final KNStudentExtData data) {
			return data.getIndividualId();
		}
		
	}

}

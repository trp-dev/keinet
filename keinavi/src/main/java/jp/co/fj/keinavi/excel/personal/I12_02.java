/**
 * 個人成績分析−面談シート　科目別成績推移グラフ
 * 	Excelファイル編集
 * 作成日: 2004/09/15
 * @author	H.Fujimoto
 * 
 * 2009.11.30   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 */

package jp.co.fj.keinavi.excel.personal;

import java.io.*;
import java.util.*;
import java.text.*;

import jp.co.fj.keinavi.excel.data.personal.*;
import jp.co.fj.keinavi.excel.cm.*;

import org.apache.poi.hssf.usermodel.*;
//import org.apache.poi.poifs.filesystem.*;
import jp.co.fj.keinavi.util.log.*;

public class I12_02 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	final private int	intMaxSheetSr	= 5;			// 最大シート数	
	final private String	strArea			= "A1:R67";	    // 印刷範囲	

	//2005.04.26 Add 新テスト対応	
	//final private String	masterfile		= "I12_02";	// ファイル名
	final private String 	masterfile0 	= "I12_02";	// ファイル名１(新テスト対応用)
	final private String 	masterfile1 	= "I12_02";	// ファイル名２(新テスト対応用)
	private String masterfile = "";


	private boolean bolSheetCngFlg = true;			//改シートフラグ
	int		intMaxSheetIndex	= 1;				//シート数

/*
 * 	Excel編集メイン
 * 		I11Item i11Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
	public int i12_02EditExcel(I11Item i11Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		
		NumberFormat nf = NumberFormat.getInstance();
		KNLog log = KNLog.getInstance(null,null,null);

		//2005.04.26 Add 新テスト対応
		//テンプレートの決定
		if (i11Item.getIntSuiiShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}


		try {
			
			// データセット
			ArrayList	i11List			= i11Item.getI11List();
			Iterator	itr				= i11List.iterator();

			int		fileIndex		= 1;	// ファイルカウンター

			// マスタExcel読み込み
			FileInputStream	fin			= null;
			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			// 基本ファイルを読込む
			I11ListBean i11ListBean = new I11ListBean();

			log.Ep("I12_02","基本データデータセット開始","");

			/** データリスト **/
			while( itr.hasNext() ) {
				fileIndex = 1;
				i11ListBean = (I11ListBean) itr.next();
				intMaxSheetIndex = 1;
				bolSheetCngFlg = true;
				
				// マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook.equals(null) ){
					return errfread;
				}
				
				ArrayList i12KmkSuiiList = i11ListBean.getI12KmkSuiiList();
				Iterator itrI12KmkSuii = i12KmkSuiiList.iterator();
				
				// 基本ファイルを読込む
				I12KmkSuiiListBean i12KmkSuiiListBean = new I12KmkSuiiListBean();
				
				int row = 0;			//行
				int col = 0;			//列
				String kmk = "";		//科目チェック用
				int kmkCnt = 0;		//型・科目数 

				log.Ep("I12_02","科目データデータセット開始","");
				
				//2004.10.21 Add ０件対応
				if( itrI12KmkSuii.hasNext() == false ){
					// データセットするシートの選択
					workSheet = workbook.getSheet(String.valueOf(1));

					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);

					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntSuiiSecuFlg() ,30 ,31 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
					workCell.setCellValue(secuFlg);

					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
					workCell.setCellValue( "学校名　：" + i11ListBean.getStrGakkomei() + "　学年：" + i11ListBean.getStrGrade() + "　クラス名：" + i11ListBean.getStrClass() + "　クラス番号：" + i11ListBean.getStrClassNum() );

					// 氏名・性別セット	
					String strSex = "";
					if( cm.toString(i11ListBean.getStrSex()).equals("1") ){
						strSex = "男";
					}else if( cm.toString(i11ListBean.getStrSex()).equals("2") ){
						strSex = "女";
					}else{
						strSex = "不明";
					}
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );

					if( !cm.toString(i11ListBean.getStrShimei()).equals("")){
						workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrShimei()) + "（" + cm.toString(i11ListBean.getStrKana()) + "）　性別：" + strSex );
					}else{
						workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrKana()) + "　性別：" + strSex );
					}


					// 模試月取得
					String moshi =cm.setTaisyouMoshi( i11Item.getStrBaseMshDate() );
					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
					workCell.setCellValue( "対象模試：" + i11Item.getStrBaseMshmei() + moshi);
				}

				/** データリスト **/
				while( itrI12KmkSuii.hasNext() ) {
					i12KmkSuiiListBean = (I12KmkSuiiListBean)itrI12KmkSuii.next();
					//科目が変わる時のチェック
					if ( !kmk.equals(i12KmkSuiiListBean.getStrKmkmei()) ) {
						kmkCnt++;
						if (kmkCnt == 5) {
							bolSheetCngFlg = true;
							kmkCnt = 1;
						}
					}

					//４表毎に改シート
					if (bolSheetCngFlg == true) {
						// データセットするシートの選択
						workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex));
						intMaxSheetIndex++;

						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);

						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntSuiiSecuFlg() ,30 ,31 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
						//Update End
						workCell.setCellValue(secuFlg);

						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
						workCell.setCellValue( "学校名　：" + i11ListBean.getStrGakkomei() + "　学年：" + i11ListBean.getStrGrade() + "　クラス名：" + i11ListBean.getStrClass() + "　クラス番号：" + i11ListBean.getStrClassNum() );

						// 氏名・性別セット	
						String strSex = "";
						if( cm.toString(i11ListBean.getStrSex()).equals("1") ){
							strSex = "男";
						}else if( cm.toString(i11ListBean.getStrSex()).equals("2") ){
							strSex = "女";
						}else{
							strSex = "不明";
						}
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );

						if( !cm.toString(i11ListBean.getStrShimei()).equals("")){
							workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrShimei()) + "（" + cm.toString(i11ListBean.getStrKana()) + "）　性別：" + strSex );
						}else{
							workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrKana()) + "　性別：" + strSex );
						}


						// 模試月取得
						String moshi =cm.setTaisyouMoshi( i11Item.getStrBaseMshDate() );
						// 対象模試セット
						workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
						workCell.setCellValue( "対象模試：" + i11Item.getStrBaseMshmei() + moshi);

						bolSheetCngFlg = false;
					}
					
					if (kmkCnt == 1) {
						// 型・科目
						workCell = cm.setCell( workSheet, workRow, workCell, 6, 0 );
						workCell.setCellValue( i12KmkSuiiListBean.getStrKmkmei());
						row = 24;
						col = 2;						
					}else if (kmkCnt == 2) {
						// 型・科目
						workCell = cm.setCell( workSheet, workRow, workCell, 6, 16 );
						workCell.setCellValue( i12KmkSuiiListBean.getStrKmkmei());
						row = 24;
						col = 18;						
					}else if (kmkCnt == 3) {
						// 型・科目
						workCell = cm.setCell( workSheet, workRow, workCell, 37, 0 );
						workCell.setCellValue( i12KmkSuiiListBean.getStrKmkmei());
						row = 55;
						col = 2;						
					}else if (kmkCnt == 4) {
						// 型・科目
						workCell = cm.setCell( workSheet, workRow, workCell, 37, 16 );
						workCell.setCellValue( i12KmkSuiiListBean.getStrKmkmei());
						row = 55;
						col = 18;						
					}

					// 模試別偏差値リストセット
					ArrayList i12MshHensaList = i12KmkSuiiListBean.getI12MshHensaList();
					Iterator itrI12MshHensa = i12MshHensaList.iterator();

					I12MshHensaListBean i12MshHensaListBean = new I12MshHensaListBean();

					int mosiCnt = 0;
					
					log.Ep("I12_02","模試別偏差値リストデータデータセット開始","");

					//模試別偏差値リストを読み込む
					while(itrI12MshHensa.hasNext()){
						i12MshHensaListBean = (I12MshHensaListBean) itrI12MshHensa.next();

						// 模試月取得
						String moshi =cm.setTaisyouMoshi( i12MshHensaListBean.getStrMshDate() );
						// 対象模試セット
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+2*mosiCnt );
						workCell.setCellValue( i12MshHensaListBean.getStrMshmei() + moshi);

						//模試名
						workCell = cm.setCell( workSheet, workRow, workCell, row+4, col-2 );
						workCell.setCellValue( "個人");

						//得点
						if( i12MshHensaListBean.getIntTokuten() != -999 ){
							workCell = cm.setCell( workSheet, workRow, workCell, row+4, col+2*mosiCnt );
							workCell.setCellValue( i12MshHensaListBean.getIntTokuten());
						}
					
						//偏差値
						if( i12MshHensaListBean.getFloHensa() != -999.0 ){
							workCell = cm.setCell( workSheet, workRow, workCell, row+5, col+2*mosiCnt );
							workCell.setCellValue( i12MshHensaListBean.getFloHensa());
						}
						
						//学力レベル
						if( !"".equals(i12MshHensaListBean.getStrScholarLevel())){
							workCell = cm.setCell( workSheet, workRow, workCell, row+5, col+2*mosiCnt+1 );
							workCell.setCellValue( i12MshHensaListBean.getStrScholarLevel());
						}

						// 志望大学別リスト
						I12KmkShiboListBean i12KmkShiboListBean = new I12KmkShiboListBean();

						ArrayList i12KmkShiboList = i12MshHensaListBean.getI12KmkShiboList();
						Iterator itrI12KmkShibo = i12KmkShiboList.iterator();

						int ShiboCnt = 1;
						//志望大学別リストを読み込む
						while(itrI12KmkShibo.hasNext()){
							i12KmkShiboListBean = (I12KmkShiboListBean) itrI12KmkShibo.next();
							
							
							if( ShiboCnt <= 3 ){
								if (mosiCnt == 0) {
								// 志望大学名
									workCell = cm.setCell( workSheet, workRow, workCell, row+4+ShiboCnt*2, col-2 );
									workCell.setCellValue( cm.toString(i12KmkShiboListBean.getStrDaigakuMei()) + " " + cm.toString(i12KmkShiboListBean.getStrGakubuMei()) + " " + cm.toString(i12KmkShiboListBean.getStrGakkaMei()));
								}
							
								if( i12KmkShiboListBean.getIntTokuten() != -999 ){
									// 得点
									workCell = cm.setCell( workSheet, workRow, workCell, row+4+ShiboCnt*2, col+2*mosiCnt );
									workCell.setCellValue( i12KmkShiboListBean.getIntTokuten());
								}
							
								if( i12KmkShiboListBean.getFloHensa() != -999.0 ){
									// 偏差値
									workCell = cm.setCell( workSheet, workRow, workCell, row+5+ShiboCnt*2, col+2*mosiCnt );
									workCell.setCellValue( i12KmkShiboListBean.getFloHensa());
								}
							}


							ShiboCnt++;
						}
						mosiCnt++;
					}
					log.Ep("I12_02","模試別偏差値リストデータデータセット終了","");
	
					kmk = i12KmkSuiiListBean.getStrKmkmei();		
				}

				log.Ep("I12_02","科目データデータセット開始","");

				// シートの選択
				workbook.getSheet("1").setSelected(true);
	
				// Excelファイル保存
				boolean bolRet = false;
				String strFileName = masterfile + "_" + cm.toString(i11ListBean.getStrGrade()) + cm.toString(i11ListBean.getStrClass()) + cm.toString(i11ListBean.getStrClassNum());
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, strFileName, intMaxSheetIndex-1);
				fileIndex++;

				if( bolRet == false ){
					return errfwrite;					
				}
				
			}

		} catch(Exception e) {
			log.Err("I12_02","データセットエラー",e.toString());
			return errfdata;
		}
		log.Ep("I12_02","基本データデータセット終了","");
		return noerror;
	}

}
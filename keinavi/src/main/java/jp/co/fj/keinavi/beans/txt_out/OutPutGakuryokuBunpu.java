package jp.co.fj.keinavi.beans.txt_out;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Connection;

import jp.co.fj.keinavi.beans.security.IMenuDefine;
import jp.co.fj.keinavi.beans.security.MenuSecurityBean;
import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import java.io.File;
import java.sql.ResultSet;

/**
 *
 * テキスト出力：集計データ：偏差値分布
 * 
 * 2004.10.18	[新規作成]
 * 
 *
 * @author 二宮淳行 - TOTEC
 * @version 1.0
 * 
 */
public class OutPutGakuryokuBunpu extends AbstractSheetBean{

	// 対象年度
	private String targetYear = null;
	// 対象模試
	private String targetExam = null;
	// 学校コード
	private String schoolCd = null;
	// ヘッダー情報
	private List header = null;
	
	// 他校コード
	private String OtherSchools = null;
	// 他校比較
	private int schoolDefFlg = 0;
	// クラス比較
	private int classDefFlg = 0;
	// 出力テキスト種別
	private int outType = 0;
	// 出力対象項目
	private Integer target[] = null;

	// 出力ファイル
	private File outPutFile = null;
	// コネクション
	private Connection con;
	
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return true;
	}

	public void execute()throws IOException, SQLException, Exception{

		OutPutParamSet set = new OutPutParamSet(login, exam, profile, "t003",sessionKey);
		
		final MenuSecurityBean menuSecurity = getMenuSecurityBean();
		schoolDefFlg = menuSecurity.isValid(IMenuDefine.MENU_TEXT_OTHER) ? set.getSchoolDefFlg() : 0;
		classDefFlg = menuSecurity.isValid(IMenuDefine.MENU_TEXT_CLASS) ? set.getClassDefFlg() : 0;
		
		targetYear = set.getTargetYear();
		targetExam = set.getTargetExam();
		schoolCd = profile.getBundleCD();
		header = set.getHeader();
		OtherSchools = set.getOtherSchools();
		outType =set.getOutType();
		target = set.getTarget();
		outPutFile =set.getOutPutFile();
		con = super.conn;
		
		OutputTextBean out = new OutputTextBean();
		out.setOutFile(outPutFile);	
		out.setOutputTextList(getGakuryokuBunpu());
		out.setHeadTextList(header);
		out.setOutTarget(target);
		out.setOutType(outType);
		out.execute();

		this.outfileList.add(outPutFile.getPath());
		sheetLog.add(outPutFile.getName());
		/*---- テスト用 ---
		File file = null;
		if(outType ==1) file = new File("D:\\Temp\\GakuryokuBunpu.csv");
		else file = new File("D:\\Temp\\GakuryokuBunpu.txt");
		TextInfoBean txt = new TextInfoBean();
		txt.execute();

		OutputTextBean out = new OutputTextBean();
		out.setOutFile(file);
		out.setOutputTextList(getGakuryokuBunpu());
		out.setHeadTextList(txt.getInfoList("t003"));
		out.setOutTarget(target);
		out.setOutType(outType);
		out.execute();
		----*/

	}
	
	
	/**
	 * 学力分布成績を取得
	 * 
	 * @return list 学力分布成績のレコードオブジェクト
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public LinkedList getGakuryokuBunpu()
		throws SQLException {
		
		// センターリサーチかどうか
		boolean cr = "38".equals(targetExam);
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		LinkedList list = new LinkedList();
		String[] rsData = new String[19];		// 読み込みデータ
		String[] listData = new String[53];	// リストデータ

		int    devCount=0;
		// キー項目
		String defFlg   ="";
		String unit     ="";
		String examYear ="";
		String examCD   ="";
		String bundleCD ="";
		String grade    ="";
		String clas     ="";
		String subcd    ="";
		// キー項目記憶
		String p_defFlg   ="";
		String p_unit     ="";
		String p_examYear ="";
		String p_examCD   ="";
		String p_bundleCD ="";
		String p_grade    ="";
		String p_clas     ="";
		String p_subcd    ="";

		String query = ""
		+" SELECT 0 DEFFLG, d.EXAMYEAR EXAMYEAR, d.EXAMCD EXAMCD, em.EXAMNAME, "
		// 全国の場合、都道府県名：'全国'  学校名：'(全国)' とする
		// +"        1 UNIT,'99' PREFCD, p.PREFNAME,                "
		// +"        '99999' BUNDLECD, TO_CHAR('('|| p.PREFNAME ||')'), 99 GRADE, '99' CLASS, "
		+"        1 UNIT,'99' PREFCD, '全国',                    "
		+"        '99999' BUNDLECD, TO_CHAR('('|| '全国' ||')'), 99 GRADE, '99' CLASS, "
		+"        es.SUBCD SUBCD, es.SUBNAME, es.SUBALLOTPNT,    "
		+"        d.DEVZONECD DEVZONECD, d.NUMBERS, d.COMPRATIO, "
		+"        s.NUMBERS  TOTALNUMBERS, s.AVGDEVIATION        "
		+" FROM   SUBDISTRECORD_A d, SUBRECORD_A s,              "
		+"       (SELECT  EXAMYEAR, EXAMCD, SUBCD, PREFCD        "
		+"         FROM   SUBDISTRECORD_P                        "
		+"         WHERE  EXAMYEAR = '" + targetYear + "'"
		+"           AND  EXAMCD   = '" + targetExam + "'"
		+"        GROUP BY EXAMYEAR, EXAMCD, SUBCD, PREFCD) dp,  "
		+"        EXAMINATION em, V_SM_CMEXAMSUBJECT es, PREFECTURE p   "
		+" WHERE  d.EXAMYEAR   = '" + targetYear + "'"
		+"   AND  d.EXAMCD     = '" + targetExam + "'"
		+"   AND  d.EXAMYEAR   =em.EXAMYEAR "
		+"   AND  d.EXAMCD     =em.EXAMCD   "
		+"   AND  d.EXAMYEAR   =es.EXAMYEAR "
		+"   AND  d.EXAMCD     =es.EXAMCD   "
		+"   AND  d.SUBCD      =es.SUBCD    "
		+"   AND  d.EXAMYEAR   =dp.EXAMYEAR "
		+"   AND  d.EXAMCD     =dp.EXAMCD   "
		+"   AND  d.SUBCD      =dp.SUBCD    "
		+"   AND  d.EXAMYEAR   = s.EXAMYEAR "
		+"   AND  d.EXAMCD     = s.EXAMCD   "
		+"   AND  d.SUBCD      = s.SUBCD    "
		+"   AND  '0'          = s.STUDENTDIV"
		+"   AND  dp.PREFCD    = p.PREFCD   "
		+"   AND  dp.PREFCD IN (SELECT FACULTYCD FROM SCHOOL sc WHERE BUNDLECD = '" + schoolCd + "') "
		+" UNION ALL"
		+" SELECT 0 DEFFLG, d.EXAMYEAR EXAMYEAR, d.EXAMCD EXAMCD, em.EXAMNAME, "
		+"        2 UNIT, d.PREFCD PREFCD, p.PREFNAME,"
		+"        d.PREFCD || '999' BUNDLECD,TO_CHAR('('|| p.PREFNAME ||')'), 99 GRADE,'99' CLASS,"
		+"        es.SUBCD SUBCD, es.SUBNAME, es.SUBALLOTPNT,    "
		+"        d.DEVZONECD DEVZONECD, d.NUMBERS, d.COMPRATIO, "
		+"        s.NUMBERS  TOTALNUMBERS, s.AVGDEVIATION        "
		+" FROM   SUBDISTRECORD_P d, SUBRECORD_P s, "
		+"        EXAMINATION em, V_SM_CMEXAMSUBJECT es, PREFECTURE p   "
		+" WHERE  d.EXAMYEAR   = '" + targetYear + "'"
		+"   AND  d.EXAMCD     = '" + targetExam + "'"
		+"   AND  d.EXAMYEAR   =em.EXAMYEAR "
		+"   AND  d.EXAMCD     =em.EXAMCD   "
		+"   AND  d.EXAMYEAR   =es.EXAMYEAR "
		+"   AND  d.EXAMCD     =es.EXAMCD   "
		+"   AND  d.SUBCD      =es.SUBCD    "
		+"   AND  d.EXAMYEAR   = s.EXAMYEAR "
		+"   AND  d.EXAMCD     = s.EXAMCD   "
		+"   AND  d.PREFCD     = s.PREFCD   "
		+"   AND  d.SUBCD      = s.SUBCD    "
		+"   AND  d.PREFCD     = p.PREFCD   "
		+"   AND  d.PREFCD IN (SELECT FACULTYCD FROM SCHOOL sc WHERE BUNDLECD = '" + schoolCd + "') "
		+" UNION ALL"
		+" SELECT 0 DEFFLG, d.EXAMYEAR EXAMYEAR, d.EXAMCD EXAMCD, em.EXAMNAME, "
		+"        3 UNIT, sc.FACULTYCD PREFCD, p.PREFNAME,       "
		+"        d.BUNDLECD BUNDLECD, sc.BUNDLENAME, 99 GRADE,'99' CLASS,"
		+"        es.SUBCD SUBCD, es.SUBNAME, es.SUBALLOTPNT,    "
		+"        d.DEVZONECD DEVZONECD, d.NUMBERS, d.COMPRATIO, "
		+"        s.NUMBERS  TOTALNUMBERS, s.AVGDEVIATION        "
		+" FROM   SUBDISTRECORD_S d, SUBRECORD_S s,              "
		+"        EXAMINATION em, V_SM_CMEXAMSUBJECT es, SCHOOL sc, PREFECTURE p "
		+" WHERE  d.EXAMYEAR   = '" + targetYear + "'"
		+"   AND  d.EXAMCD     = '" + targetExam + "'"
		+"   AND  d.BUNDLECD IN ('" + schoolCd   + "') "
		+"   AND  d.EXAMYEAR   =em.EXAMYEAR "
		+"   AND  d.EXAMCD     =em.EXAMCD   "
		+"   AND  d.EXAMYEAR   =es.EXAMYEAR "
		+"   AND  d.EXAMCD     =es.EXAMCD   "
		+"   AND  d.SUBCD      =es.SUBCD    "
		+"   AND  d.EXAMYEAR   = s.EXAMYEAR "
		+"   AND  d.EXAMCD     = s.EXAMCD   "
		+"   AND  d.BUNDLECD   = s.BUNDLECD "
		+"   AND  d.SUBCD      = s.SUBCD    "
		+"   AND  sc.BUNDLECD  = d.BUNDLECD "
		+"   AND  sc.FACULTYCD = p.PREFCD   ";
		if(classDefFlg == 1){
			query+=" UNION ALL"
			+" SELECT 0 DEFFLG, d.EXAMYEAR EXAMYEAR, d.EXAMCD EXAMCD, em.EXAMNAME, "
			+"        4 UNIT, sc.FACULTYCD PREFCD, p.PREFNAME,       "
			+"        d.BUNDLECD BUNDLECD, sc.BUNDLENAME, d.GRADE GRADE, d.CLASS CLASS,"
			+"        es.SUBCD SUBCD, es.SUBNAME, es.SUBALLOTPNT,    "
			+"        d.DEVZONECD DEVZONECD, d.NUMBERS, d.COMPRATIO, "
			+"        s.NUMBERS  TOTALNUMBERS, s.AVGDEVIATION        "
			+" FROM   SUBDISTRECORD_C d, SUBRECORD_C s,              "
			+"        EXAMINATION em, V_SM_CMEXAMSUBJECT es, SCHOOL sc, PREFECTURE p "
			+" WHERE  d.EXAMYEAR   = '" + targetYear + "'"
			+"   AND  d.EXAMCD     = '" + targetExam + "'"
			+"   AND  d.BUNDLECD IN ('" + schoolCd   + "') "
			+"   AND  d.EXAMYEAR   =em.EXAMYEAR "
			+"   AND  d.EXAMCD     =em.EXAMCD   "
			+"   AND  d.EXAMYEAR   =es.EXAMYEAR "
			+"   AND  d.EXAMCD     =es.EXAMCD   "
			+"   AND  d.SUBCD      =es.SUBCD    "
			+"   AND  d.EXAMYEAR   = s.EXAMYEAR "
			+"   AND  d.EXAMCD     = s.EXAMCD   "
			+"   AND  d.BUNDLECD   = s.BUNDLECD "
			+"   AND  d.GRADE      = s.GRADE    "
			+"   AND  d.CLASS      = s.CLASS    "
			+"   AND  d.SUBCD      = s.SUBCD    "
			+"   AND  sc.BUNDLECD  = d.BUNDLECD "
			+"   AND  sc.FACULTYCD = p.PREFCD   ";
		}
		if(schoolDefFlg==1){
			query+=" UNION ALL"
			+" SELECT 1 DEFFLG, d.EXAMYEAR EXAMYEAR, d.EXAMCD EXAMCD, em.EXAMNAME, "
			+"        3 UNIT, sc.FACULTYCD PREFCD, p.PREFNAME,       "
			+"        d.BUNDLECD BUNDLECD, sc.BUNDLENAME, 99 GRADE,'99' CLASS, "
			+"        es.SUBCD SUBCD, es.SUBNAME, es.SUBALLOTPNT,    "
			+"        d.DEVZONECD DEVZONECD, d.NUMBERS, d.COMPRATIO, "
			+"        s.NUMBERS  TOTALNUMBERS, s.AVGDEVIATION        "
			+" FROM   SUBDISTRECORD_S d, SUBRECORD_S s,              "
			+"        EXAMINATION em, V_SM_CMEXAMSUBJECT es, SCHOOL sc, PREFECTURE p "
			+" WHERE  d.EXAMYEAR   = '" + targetYear + "'"
			+"   AND  d.EXAMCD     = '" + targetExam + "'"
			+"   AND  d.BUNDLECD IN (" + OtherSchools + ")"
			+"   AND  d.EXAMYEAR   =em.EXAMYEAR "
			+"   AND  d.EXAMCD     =em.EXAMCD   "
			+"   AND  d.EXAMYEAR   =es.EXAMYEAR "
			+"   AND  d.EXAMCD     =es.EXAMCD   "
			+"   AND  d.SUBCD      =es.SUBCD    "
			+"   AND  d.EXAMYEAR   = s.EXAMYEAR "
			+"   AND  d.EXAMCD     = s.EXAMCD   "
			+"   AND  d.BUNDLECD   = s.BUNDLECD "
			+"   AND  d.SUBCD      = s.SUBCD    "
			+"   AND  sc.BUNDLECD  = d.BUNDLECD "
			+"   AND  sc.FACULTYCD = p.PREFCD   ";
		}
//		if(schoolDefFlg==1 && classDefFlg == 1){
//			query+=" UNION ALL"
//			+" SELECT 1 DEFFLG, d.EXAMYEAR EXAMYEAR, d.EXAMCD EXAMCD, em.EXAMNAME, "
//			+"        4 UNIT, sc.FACULTYCD PREFCD, p.PREFNAME,       "
//			+"        d.BUNDLECD BUNDLECD, sc.BUNDLENAME, d.GRADE GRADE, d.CLASS CLASS, "
//			+"        es.SUBCD SUBCD, es.SUBNAME, es.SUBALLOTPNT,    "
//			+"        d.DEVZONECD DEVZONECD, d.NUMBERS, d.COMPRATIO, "
//			+"        s.NUMBERS  TOTALNUMBERS, s.AVGDEVIATION        "
//			+" FROM   SUBDISTRECORD_C d, SUBRECORD_C s,              "
//			+"        EXAMINATION em, EXAMSUBJECT es, SCHOOL sc, PREFECTURE p "
//			+" WHERE  d.EXAMYEAR   = '" + targetYear + "'"
//			+"   AND  d.EXAMCD     = '" + targetExam + "'"
//			+"   AND  d.BUNDLECD IN (" + OtherSchools + ")"
//			+"   AND  d.EXAMYEAR   =em.EXAMYEAR "
//			+"   AND  d.EXAMCD     =em.EXAMCD   "
//			+"   AND  d.EXAMYEAR   =es.EXAMYEAR "
//			+"   AND  d.EXAMCD     =es.EXAMCD   "
//			+"   AND  d.SUBCD      =es.SUBCD    "
//			+"   AND  d.EXAMYEAR   = s.EXAMYEAR "
//			+"   AND  d.EXAMCD     = s.EXAMCD   "
//			+"   AND  d.BUNDLECD   = s.BUNDLECD "
//			+"   AND  d.GRADE      = s.GRADE    "
//			+"   AND  d.CLASS      = s.CLASS    "
//			+"   AND  d.SUBCD      = s.SUBCD    "
//			+"   AND  sc.BUNDLECD  = d.BUNDLECD "
//			+"   AND  sc.FACULTYCD = p.PREFCD   ";
//		}
		query+=" ORDER BY  DEFFLG, EXAMYEAR, EXAMCD, UNIT, PREFCD, BUNDLECD, GRADE, CLASS, SUBCD, DEVZONECD ";
		try {
			stmt = con.prepareStatement(query);
			rs = stmt.executeQuery();
            
			while (rs.next()) {
				
				// SELECTしたデータを一時変数に格納
				for (int i = 0; i < rsData.length; i++) {
					// 構成比、平均偏差値の小数点付き数値はgetDouble()で取得
					if ( i == 16 || i == 18 ) {
						if (rs.getString( i + 1 ) == null ) {
							rsData[i] = rs.getString( i + 1 );
						} else {
							rsData[i] = String.valueOf(rs.getDouble( i + 1 ));
						}
					} else {
						rsData[i] = rs.getString( i + 1 );
					}
				}

				// キー項目をセット
				defFlg   =rsData[ 0];	// 出力順 
				
				// センターリサーチなら年度+1
				if (cr) {
					rsData[1] = String.valueOf(Integer.parseInt(rsData[1]) + 1);
				}

				examYear =rsData[ 1];	// 模試年度
				examCD   =rsData[ 2];	// 模試コード
				unit     =rsData[ 4];	// 集計単位
				bundleCD =rsData[ 7];	// 一括コード
				grade    =rsData[ 9];	// 学年
				clas     =rsData[10];	// クラス
				subcd    =rsData[11];	// 科目コード

				// キー項目の内容に変化があればデータを格納＆初期化
				if (!p_defFlg.equals(defFlg) ||
					!p_examYear.equals(examYear) ||
					!p_examCD.equals(examCD) ||
				    !p_unit.equals(unit) || 
				    !p_bundleCD.equals(bundleCD) ||	
					!p_grade.equals(grade) ||
					!p_clas.equals(clas) ||	
					!p_subcd.equals(subcd) ) {
					
					// リストデータに格納
					if (devCount > 0) {
						setListData(listData, list);
					}

					// データ初期化
					devCount = 0;
					for (int i = 0; i < listData.length; i++) {
						// 分布、構成比はゼロをセット
						if ( 13 <= i && i <= 31) {
							listData[i] = "0";
						} else if ( 32 <= i && i <= 50) {
							listData[i] = "0.0";
						} else {
							listData[i] = "";
						}
					}

					// 年度〜型・科目配点までをセット
					for (int i = 0; i < 13; i++) {
						listData[i] = rsData[i+1];
					}
					// 合計人数、平均偏差値をセット
					listData[51] = rsData[17];
					listData[52] = rsData[18];
				}
				// 格納可能カウンターのカウントアップ
				devCount++;

				// キー項目記憶
				p_defFlg   =defFlg;
				p_examYear =examYear;
				p_examCD   =examCD;
				p_unit     =unit;
				p_bundleCD =bundleCD;
				p_grade    =grade;
				p_clas     =clas;
				p_subcd    =subcd;
				
				// 標準偏差値帯
				int devZoneCD = Integer.valueOf(rsData[14]).intValue();
				// コード"01"〜"19"の範囲
				if ( 1 <= devZoneCD && devZoneCD <= 19 ) {
					// 分布セット
					listData[12 + devZoneCD] = rsData[15];
					// 構成セット
					listData[31 + devZoneCD] = rsData[16];
				}

			}
			// 格納可能データがあればリストデータに格納
			if (devCount > 0) {
				setListData(listData, list);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new SQLException("学力分布成績の抽出に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return list;
	}


	/**
	 * 一時的に記憶したリストデータを格納する
	 *
	 * @param 	listData	リストデータ
	 *          list        格納するリスト
	 *
	 */
	private void setListData(String[] listData, LinkedList list) {
		LinkedHashMap data = new LinkedHashMap();
		for (int i = 0; i<listData.length; i++) {
			data.put( new Integer(i+1), listData[i]);
		}
		list.add(data);
	}

	/**
	 * @param connection
	 */
	public void setCon(Connection connection) {
		con = connection;
	}

	/**
	 * @param string
	 */
	public void setOtherSchools(String string) {
		OtherSchools = string;
	}

	/**
	 * @param string
	 */
	public void setSchoolCd(String string) {
		schoolCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExam(String string) {
		targetExam = string;
	}

	/**
	 * @param string
	 */
	public void setTargetYear(String string) {
		targetYear = string;
	}

	/**
	 * @param list
	 */
	public void setHeader(LinkedList list) {
		header = list;
	}

	/**
	 * @param i
	 */
	public void setClassDefFlg(int i) {
		classDefFlg = i;
	}

	/**
	 * @param i
	 */
	public void setOutType(int i) {
		outType = i;
	}

	/**
	 * @param i
	 */
	public void setSchoolDefFlg(int i) {
		schoolDefFlg = i;
	}


	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

}
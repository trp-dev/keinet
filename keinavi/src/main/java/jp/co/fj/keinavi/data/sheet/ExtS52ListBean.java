/*
 * 作成日: 2004/09/24
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.sheet;

import jp.co.fj.keinavi.excel.data.school.S52ListBean;

/**
 * 校内成績分析・過回比較・偏差値分布
 * データリスト
 * 
 * @author kawai
 */
public class ExtS52ListBean extends S52ListBean implements Comparable {

	private String dataOpenDate; // データ開放日
	private int subDispSeq; // 型・科目の表示順序
	private String examYear; // 模試年度

	/* (非 Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		// 型・科目の表示順の昇順・模試年度の降順・データ開放日の降順でソートする
		ExtS52ListBean obj = (ExtS52ListBean) o;

		if (this.getSubDispSeq() > obj.getSubDispSeq()) {
			return 1;

		} else if (this.getSubDispSeq() < obj.getSubDispSeq()) {
			return -1;

		} else {
			int result = obj.getExamYear().compareTo(this.getExamYear());
			
			if (result == 0) {
				return obj.getDataOpenDate().compareTo(this.getDataOpenDate());

			} else {
				return result;
			}
		}
	}

	/**
	 * @return
	 */
	public int getSubDispSeq() {
		return subDispSeq;
	}

	/**
	 * @param i
	 */
	public void setSubDispSeq(int i) {
		subDispSeq = i;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @return
	 */
	public String getDataOpenDate() {
		return dataOpenDate;
	}

	/**
	 * @param string
	 */
	public void setDataOpenDate(String string) {
		dataOpenDate = string;
	}

}

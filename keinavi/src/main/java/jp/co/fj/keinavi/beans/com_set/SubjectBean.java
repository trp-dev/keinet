package jp.co.fj.keinavi.beans.com_set;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.com_set.cm.Subject;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 型・科目データリスト生成Bean
 * 
 * 
 * 2006.02.02	Yoshimoto KAWAI - TOTEC
 * 				校内成績処理システムで利用するために
 * 				ログインセッションを受け取るようにした
 * 
 * @author kawai
 */
public class SubjectBean extends AbstractComSetBean {

	private final LoginSession login;

	private String targetYear; // 対象年度
	private String targetExam; // 対象模試
	private String bundleCD; // 一括コード
	private String mode; // 型か科目か
	private Set codeSet; // 型・科目コードセット
		
	/**
	 * @param l ログインセッション
	 */
	public SubjectBean(final LoginSession l) {
		this.login = l;
	}
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// 営業部
			if (login.isSales()) {
				Query query = QueryLoader.getInstance().load("cm09_1");
				
				if ("1".equals(mode)) query.append("AND subcd >= 7000 "); // 型
				else query.append("AND subcd < 7000 "); // 科目

				query.append("ORDER BY subcd");

				ps = conn.prepareStatement(query.toString());
				ps.setString(1, targetYear); // 模試年度
				ps.setString(2, targetExam); // 模試コード

			// 一般
			} else {
				Query query = QueryLoader.getInstance().load("cm09_2");
			
				if ("1".equals(mode)) query.append("AND es.subcd >= 7000 "); // 型
				else query.append("AND es.subcd < 7000 "); // 科目
			
				query.append("ORDER BY es.subcd");

				ps = conn.prepareStatement(query.toString());
				ps.setString(1, targetYear); // 模試年度
				ps.setString(2, targetExam); // 模試コード
				ps.setString(3, bundleCD); // 一括コード
			}

			rs = ps.executeQuery();
			while (rs.next()) {
				Subject data = new Subject();
				data.setSubCD(rs.getString(1));
				data.setSubName(rs.getString(2));
				data.setExaminees(rs.getInt(3));
				fullList.add(data);
			}
			
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.com_set.AbstractComBean#getPartList()
	 */
	public List getPartList() {
		if (partList == null) {
			partList = new ArrayList();
			Iterator ite = fullList.iterator();
			while (ite.hasNext()) {
				Subject data = (Subject)ite.next();
				if (data.getExaminees() > 0) partList.add(data);
			}
		}
		return partList;
	}

	/**
	 * @return
	 */
	public Set getCodeSet() {
		if (this.codeSet == null) {
			this.codeSet = new HashSet();
			
			Iterator ite = this.getFullList().iterator();
			while (ite.hasNext()) {
				Subject data = (Subject) ite.next();
				this.codeSet.add(data.getSubCD());
			}
		}
		return this.codeSet;
	}

	/**
	 * @param string
	 */
	public void setBundleCD(String string) {
		bundleCD = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExam(String string) {
		targetExam = string;
	}

	/**
	 * @param string
	 */
	public void setTargetYear(String string) {
		targetYear = string;
	}

	/**
	 * @param string
	 */
	public void setMode(String string) {
		mode = string;
	}

}

/*
 * 作成日: 2004/07/29
 *
 * 成績分析／科目別成績推移グラフの偏差値データ取得用Bean
 * 
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.data.ExamData;
//import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.IExamData;
import jp.co.fj.keinavi.data.individual.PrevSuccessData;
//import jp.co.fj.keinavi.data.individual.SubRecordData;
import jp.co.fj.keinavi.data.individual.SubRecordIData;
import jp.co.fj.keinavi.data.individual.SubjectData;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;

import com.fjh.beans.DefaultBean;

/**
 * バランスチャート(科目別成績推移グラフ)<BR>
 * DBより偏差値を取得し配列に入れる。
 * @author kondo
 * 
 * 2005.03.01 	K.Kondo 	[1]現年度をカレンダーから取得時、３月３１日以前は前年度とする日付比較が、うまく行われていなかったのを修正。
 * 2005.03.01 	K.Kondo 	[2]科目変換に、模試コードを追加。
 * 2005.03.08 	K.Kondo 	[3]対象生徒が何年生かを調べるときに使う年度をクラス年度に変更
 * 2005.03.08 	K.Kondo 	[4]現年度指定の条件に使用される年度を、担当クラスの年度に変更
 * 2005.09.16 	K.Kondo 	[5]科目取得の際EXAMSUBJECTではなく、Viewを参照するように修正
 * 2009.11.20   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応 
 * 2010.01.18 	Shoji HASE - Totec
 *              「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 *              「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 */
public class I107Bean extends DefaultBean {
	
	private static final long serialVersionUID = 1081719753781007185L;
	
	public static final String DEFAULT_RANK1 = "1";
	public static final String DEFAULT_RANK2 = "2";
	public static final String DEFAULT_RANK3 = "3";
	public static final String[] DEFAULT_RANKS = {DEFAULT_RANK1,DEFAULT_RANK2,DEFAULT_RANK3};
	
	private final String TARGET_PERSON = "対象生徒（偏差値/学力レベル）";
	private final String YEAR = "年度";
	private final int MAX_EXAM_NUM = 7;
	
	//模試ＳＱＬ
	private String SQL_BASE = "";
	
	//コンボボックスＳＱＬ
	private String COMBO_SQL_BASE = 
	"SELECT " + 
	" /*+ INDEX(CR PK_CANDIDATERATING) */ "+//12/5 sql hint
	"CR.CANDIDATERANK" + 	//志望順位
	",HS.UNINAME_ABBR UNIVNAME_ABBR" + //大学名短縮
	",HS.FACULTYNAME_ABBR" + //学部名短縮
	",HS.DEPTNAME_ABBR" + 	//学科名短縮
	" FROM EXAMINATION EX ";
	
	private String SUBNUM_SQL_BASE = "";
  
	private String moshiSql;					//模試ＳＱＬ
	
	private String comboSql;					//コンボＳＱＬ
	private String subNumSql;					//科目ＳＱＬ

	private String targetGrade;				//対象の学年（all：全て / now：現年度）
	private String targetSubNm;				//対象の科目名
	private String targetSubCd;				//対象の科目コード
	private String targetExamYear;				//対象の模試年度
	
	private String targetExamYear2;			//対象模試年度２
	private String targetExamYear3;			//対象模試年度３
	
	private String dispTargetExamYear;			//表示用対象年度
	
	private int targetExamNum;				//何年度分あるか
	
	
	private String targetExamCode;				//対象の模試コード
	private String targetExamTypeCode;			//模試種類コード
	private String targetExamGrade;			//対象模試の対象学年
	
	private String[] targetCandidateRanks;		//再表示の時に使う＝2回目以降のアクセス
	private String personId;					//個人ＩＤ
	private int historyGrade;				//履歴からとった対象年度の対象の人の学年
	
	private int examinationDataSize;			//模試リストの模試個数
	
	private boolean isFirstAccess;			//初回アクセスフラグ
	
	private Collection subDatas;				//科目リスト
	private Collection comboxDatas;			//志望校リスト

	private Collection examinationDatas;		//模試リスト
	private Collection examinationDatas2;		//模試リスト２
	private Collection examinationDatas3;		//模試リスト３
	
	private Collection prevSuccessDatas;		//過年度合格者平均リスト
	
	private boolean helpflg;
	private String classYear;//クラスの年度//[3] add
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		try{			
			if(getExaminationDatas() == null) 	{setExaminationDatas(new ArrayList());}
			if(getExaminationDatas2() == null) {setExaminationDatas2(new ArrayList());}
			if(getExaminationDatas3() == null) {setExaminationDatas3(new ArrayList());}
					
			conn.setAutoCommit(false);
			//KNUtil.buildSubCDTransTable(conn, getTargetExamYear(), getTargetExamCode());//[2] delete
			KNUtil.buildSubCDTransTable(conn, getExamDatas());//[2] add
			
			//対象模試の情報を取得します
			IExamData examData = getTargetExamData(getTargetExamYear(), getTargetExamCode());
			setTargetExamTypeCode(examData.getExamTypeCd());
			setTargetExamGrade(examData.getTergetGrade());
			
			//学年を取得します。
			setHistoryGrade(execHistoryGrade(getPersonId()));
			//科目名＆科目コード取得
			setSubDatas(execSubNames(getTargetExamYear(), getTargetExamCode(), getPersonId()));
			
			//模試ＳＱＬを作成
			setMoshiSql(getExamCondition(getTargetGrade()));
			
			//模試取得
			if(getTargetGrade().equals("1")){
				//現学年
				setExaminationDatas(execDeviations());	
			} else {
				//全学年
				execDeviationAll();
			}
			
			//過去合格者平均取得
			setPrevSuccessDatas(execUniversityDatas());
			//コンボ取得
			setComboxDatas(execComboBoxDatas());
			
			conn.commit();
		}catch(Exception e){
			conn.rollback();
			e.printStackTrace();
			throw new SQLException("i107BeanSQLに失敗しました。");
		} finally {}

	}
	
	/**
	 * 初回アクセス時
	 * @param String year 対象模試年度
	 * @param String code 対象模試コード
	 * @param String id 対象個人ＩＤ
	 * @param String grade 対象学年
	 */
	public void setSearchCondition(String examYear, String code, String id, String grade){
		//検索条件の設定
		setTargetExamYear(examYear);	//対象模試年度
		setTargetExamCode(code);		//対象模試コード	
		setPersonId(id);				//対象個人ＩＤ
		setTargetGrade(grade);			//対象学年
		
		//初回アクセス時は存在しない
		String[] ranks = new String[3];
		ranks[0] = "1";
		ranks[1] = "2";
		ranks[2] = "3";
		setTargetCandidateRanks(ranks);
		
		//コンボボックスＳＱＬの作成
		appendComboSql(COMBO_SQL_BASE);
		appendComboSql(getComboSqlCondition(getPersonId(), getTargetExamYear(), getTargetExamCode()));

		//科目リスト取得ＳＱＬ
		setSubNumSql(SUBNUM_SQL_BASE);
		
	}
	/**
	 * 二回目以降のアクセス時
	 * @param String year 対象模試年度
	 * @param String code 対象コード
	 * @param String id 対象個人ＩＤ
	 * @param String grade 検索対象の学年（all / now）
	 * @param Stirng[] ranks 検索対象の志望ランク
	 */
	public void setSearchCondition(String examYear, String code, String id, String grade, String[] ranks){
		//コンボの内容を設定
		setTargetExamYear(examYear);	//対象模試年度
		setTargetExamCode(code);	//対象模試コード
		setPersonId(id);			//対象個人ＩＤ
		setTargetGrade(grade);		//対象学年
		
		//二回目以降のアクセスにて存在する
		ArrayList temp = new ArrayList();
		for(int i=0; i<ranks.length; i++){
			if(ranks[i] != null && !ranks[i].trim().equals("") && !ranks[i].trim().equals("null")){
				temp.add(ranks[i]);
			}
		}
		if(temp.size() > 0){
			setTargetCandidateRanks((String[]) temp.toArray(new String[0]));
		}
		
		//コンボボックスＳＱＬの作成
		appendComboSql(COMBO_SQL_BASE);
		appendComboSql(getComboSqlCondition(getPersonId(), getTargetExamYear(), getTargetExamCode()));		
		
		//科目リスト取得ＳＱＬの作成
		setSubNumSql(SUBNUM_SQL_BASE);
	}
	
	private void appendComboSql(String string){
		if(getComboSql() == null){
			setComboSql("");
		}
		comboSql += string;
	}
	
	/**
	 * コンボボックスＳＱＬの条件を設定
	 * @param string1
	 * @param string2
	 * @param string3
	 * @return
	 */
	private String getComboSqlCondition(String id, String year, String cd){
		
		String sqlOption =
		
		" INNER JOIN CANDIDATERATING CR" +
		" ON CR.INDIVIDUALID = ?"+
		" AND EX.EXAMYEAR = '"+year+"'" + 
		" AND EX.EXAMCD = '" + cd + "'"+
		" AND CR.EXAMYEAR = EX.EXAMYEAR" +
		" AND CR.EXAMCD = EX.EXAMCD" + 
		" LEFT JOIN UNIVMASTER_BASIC HS" +
		" ON HS.EVENTYEAR = EX.EXAMYEAR" +
		" AND HS.EXAMDIV = EX.EXAMDIV" +
		" AND HS.UNIVCD = CR.UNIVCD" +
		" AND HS.FACULTYCD = CR.FACULTYCD" + 
		" AND HS.DEPTCD = CR.DEPTCD" + 
		" WHERE EX.EXAMTYPECD IN ('01','02','03')" +
		" AND EX.EXAMCD NOT IN ('66', '67')" + 
		" ORDER BY CR.CANDIDATERANK";
		
		return sqlOption;
	}
	
	/**
	 * 対象模試、対象生徒が過去に受けた模試の条件を設定
	 * 
	 * @param id		個人ＩＤ
	 * @param year		対象模試年度
	 * @param cd		対象模試コード
	 * @param subcd	対象科目コード
	 * @return 模試ＳＱＬ
	 */
	private String getExamCondition(String student) {
		String DATAOPENDATE = null;
		//setHelpflg(false);//[1] delete
		if(isHelpflg()) {
			DATAOPENDATE = "IN_DATAOPENDATE";
		} else {
			DATAOPENDATE = "OUT_DATAOPENDATE";
		}
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT ");
		strBuf.append(" /*+ INDEX(SI PK_SUBRECORD_I) */ ");//12/5 sql hint
		strBuf.append("EX.EXAMYEAR, ");
		strBuf.append("EX.EXAMCD, ");
		strBuf.append("EX.EXAMTYPECD, ");
		strBuf.append("EX.EXAMNAME_ABBR, ");
		strBuf.append("EX.TERGETGRADE, ");
		strBuf.append("ST.CURSUBCD, ");
		strBuf.append("ST.SUBCD, ");
		strBuf.append("SI.A_DEVIATION, ");
		strBuf.append("SI.ACADEMICLEVEL ");
		strBuf.append("FROM ");
		strBuf.append("EXAMINATION EX ");
		strBuf.append("INNER JOIN ");
		strBuf.append("(SELECT /*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */ EXAMYEAR, EXAMCD FROM SUBRECORD_I WHERE INDIVIDUALID = ? GROUP BY EXAMYEAR, EXAMCD) SB ");//12/5 sql hint
		strBuf.append("ON EX.EXAMYEAR = SB.EXAMYEAR ");
		strBuf.append("AND EX.EXAMCD = SB.EXAMCD ");
		strBuf.append("AND EX.").append(DATAOPENDATE).append(" <= (SELECT ").append(DATAOPENDATE).append(" FROM EXAMINATION WHERE EXAMYEAR = ? AND EXAMCD = ?) ");
		strBuf.append("AND EX.EXAMTYPECD = (SELECT EXAMTYPECD FROM EXAMINATION WHERE EXAMYEAR = ? AND EXAMCD = ?) ");
		strBuf.append("LEFT JOIN ");
		strBuf.append("SUBCDTRANS ST ");
		//strBuf.append("ON ST.YEAR = EX.EXAMYEAR ");//[2] delete
		strBuf.append("ON ST.EXAMYEAR = EX.EXAMYEAR ");//[2] add
		strBuf.append("AND ST.EXAMCD = ? ");//[2] add
		strBuf.append("AND ST.CURSUBCD = ? ");
		strBuf.append("LEFT JOIN ");
		strBuf.append("SUBRECORD_I SI ");
		strBuf.append("ON SI.EXAMYEAR = EX.EXAMYEAR ");
		strBuf.append("AND SI.EXAMCD = EX.EXAMCD ");
		strBuf.append("AND SI.INDIVIDUALID = ? ");
		strBuf.append("AND SI.SUBCD = ST.SUBCD ");
		if(student.equals("1")) {
			strBuf.append("WHERE EX.EXAMYEAR = ? ");	//セッション（現年度）
		}
		strBuf.append("ORDER BY ");
		strBuf.append("EX.").append(DATAOPENDATE).append(" ");
		return strBuf.toString();
	}
//	[2]	
	private String getNowExamCondition(String year, String id, String cd) {
		String DATAOPENDATE = null;
		if(isHelpflg()) {
			DATAOPENDATE = "IN_DATAOPENDATE";
		} else {
			DATAOPENDATE = "OUT_DATAOPENDATE";
		}
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT ");
		strBuf.append("EXAMNAME_ABBR,");
		strBuf.append("EXAMCD,");
		strBuf.append("EXAMTYPECD,");
		strBuf.append("TERGETGRADE ");
		strBuf.append("FROM ");
		strBuf.append("EXAMINATION ");
		strBuf.append("WHERE ");
		strBuf.append("(EXAMYEAR,EXAMCD) IN (");
		strBuf.append("SELECT ");
		strBuf.append("/*+ INDEX(SI PK_SUBRECORD_I) */ ");
		strBuf.append("EX.EXAMYEAR,EX.EXAMCD ");
		strBuf.append("FROM ");
		strBuf.append("EXAMINATION EX ");
		strBuf.append("INNER JOIN ");
		strBuf.append("SUBRECORD_I SI ");
		strBuf.append("ON ");
		strBuf.append("EX.").append(DATAOPENDATE).append(" <= (SELECT ").append(DATAOPENDATE).append(" FROM EXAMINATION WHERE EXAMYEAR = '").append(year).append("' AND EXAMCD = '").append(cd).append("') ");
		strBuf.append("AND SI.INDIVIDUALID = ? ");
		strBuf.append("AND EX.EXAMTYPECD = (SELECT EXAMTYPECD FROM EXAMINATION WHERE EXAMYEAR = '").append(year).append("' AND EXAMCD = '").append(cd).append("') ");
		strBuf.append("AND EX.EXAMYEAR = SI.EXAMYEAR ");
		strBuf.append("AND EX.EXAMCD = SI.EXAMCD ");
		strBuf.append("GROUP BY EX.EXAMYEAR,EX.EXAMCD ");
		strBuf.append(") ");
		strBuf.append("ORDER BY TERGETGRADE ASC, ").append(DATAOPENDATE).append(" ASC ");
//		String DATAOPENDATE = null;
//		//setHelpflg(false);//[1] delete
//		if(isHelpflg()) {
//			DATAOPENDATE = "IN_DATAOPENDATE";
//		} else {
//			DATAOPENDATE = "OUT_DATAOPENDATE";
//		}
//		
//		String lastYear = Integer.toString(Integer.parseInt(year) -1); //前年度を取得
//		
//		String condition = 
//		"SELECT " + 
//			"EXAMINATION.EXAMNAME_ABBR, " + 
//			"EXAMINATION.EXAMCD, " + 
//			"EXAMINATION.DISPSEQUENCE, " +
//			"EXAMINATION.EXAMYEAR, " +
//			"EXAMINATION.EXAMTYPECD, " +
//			"EXAMINATION.TERGETGRADE " + 
//
//		"FROM EXAMINATION " + 
//		"WHERE " + 
//			"((EXAMINATION.EXAMYEAR = '" + year + "' AND EXAMINATION.TERGETGRADE <> '02') OR " + 
//			"(EXAMINATION.EXAMYEAR = '" + lastYear + "' AND EXAMINATION.TERGETGRADE  = '02' AND EXAMINATION.EXAMTYPECD IN ('01', '02'))) " + 
//			"AND EXAMINATION.EXAMCD IN (" + 
//				"SELECT " + 
//					"T1.EXAMCD " + 
//				"FROM " + 
//					"(" + 
//						//これはこの人が、受けた全模試です。
//						"SELECT " + 
//							"/*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */"+
//							"EXAMINATION.EXAMTYPECD, " + 
//							"EXAMINATION." +DATAOPENDATE+ ", " + 
//							"EXAMINATION.EXAMYEAR, " + 
//							"EXAMINATION.EXAMCD " + 
//						"FROM " + 
//							"SUBRECORD_I, " + 
//							"EXAMINATION " + 
//						"WHERE " + 
//							"SUBRECORD_I.EXAMYEAR = EXAMINATION.EXAMYEAR " + 
//							"AND SUBRECORD_I.EXAMCD = EXAMINATION.EXAMCD " + 
//							"AND SUBRECORD_I.INDIVIDUALID = '" + id + "' " + 
//						"GROUP BY " + 
//							"EXAMINATION.EXAMTYPECD, " + 
//							"EXAMINATION." +DATAOPENDATE+ ", " + 
//							"EXAMINATION.EXAMYEAR, " + 
//							"EXAMINATION.EXAMCD " + 
//					") T1 , " + 
//					"(" + 
//						//この人が受けたこの模試
//						"SELECT " + 
//							"EXAMINATION.EXAMTYPECD, " + 
//							"EXAMINATION." +DATAOPENDATE+ ", " + 
//							"EXAMINATION.EXAMYEAR " + 
//						"FROM " + 
//							"EXAMINATION " + 
//						"WHERE " + 
//							"EXAMINATION.EXAMYEAR = '" + year + "' " + 
//							"AND EXAMINATION.EXAMCD = '" + cd + "' " + 
//					") T2 " + 
//				"WHERE " + 
//					//この模試より前
//					"T1." +DATAOPENDATE+ " <= T2." +DATAOPENDATE+ " " + 
//					//この模試と同じ種類
//					"AND T1.EXAMTYPECD = T2.EXAMTYPECD " + 
//				"GROUP BY T1.EXAMCD " + 
//			")" + 
//		//"ORDER BY EXAMINATION.DISPSEQUENCE DESC";//[1] delete
//		"ORDER BY EXAMINATION." + DATAOPENDATE + "  ASC";//[1] add		
//		return condition;
		return strBuf.toString();
	}
//	[2]
	
	/**
	 * 二個目
	 * @return
	 */
	private String getUnivConditionEx() {
		//緊急２０００番対応版
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT ");
		strBuf.append(" /*+ INDEX(CR PK_CANDIDATERATING) */ ");//12/5 sql hint
		strBuf.append("CR.CANDIDATERANK, ");
		strBuf.append("HS.UNINAME_ABBR UNIVNAME_ABBR, ");
		strBuf.append("HS.FACULTYNAME_ABBR, ");
		strBuf.append("HS.DEPTNAME_ABBR, ");
		strBuf.append("PS.SUBCD, ");
		strBuf.append("PS.SUCCESSAVGDEV ");
		strBuf.append("FROM ");
		strBuf.append("EXAMINATION EX ");
		strBuf.append("INNER JOIN ");
		strBuf.append("CANDIDATERATING CR ");
		strBuf.append("ON EX.EXAMYEAR = ? ");//模試年度
		strBuf.append("AND EX.EXAMCD = ? ");//模試コード
		strBuf.append("AND EX.EXAMTYPECD IN ('01','02','03') ");
		strBuf.append("AND EX.EXAMCD NOT IN ('66','67') ");
		strBuf.append("AND CR.INDIVIDUALID = ? ");//個人ID
		strBuf.append("AND CR.EXAMYEAR = EX.EXAMYEAR ");
		strBuf.append("AND CR.EXAMCD = EX.EXAMCD ");
		strBuf.append("AND CR.CANDIDATERANK IN (#) ");//志望順位
		strBuf.append("LEFT JOIN ");
		strBuf.append("UNIVMASTER_BASIC HS ");
		strBuf.append("ON HS.EVENTYEAR = EX.EXAMYEAR ");
		strBuf.append("AND HS.EXAMDIV = EX.EXAMDIV ");
		strBuf.append("AND HS.UNIVCD = CR.UNIVCD ");
		strBuf.append("AND HS.FACULTYCD = CR.FACULTYCD ");
		strBuf.append("AND HS.DEPTCD = CR.DEPTCD ");
		strBuf.append("LEFT JOIN ");
		strBuf.append("PREVSUCCESS PS ");
		strBuf.append("ON PS.EXAMYEAR = TO_NUMBER(EX.EXAMYEAR) -1 ");
		strBuf.append("AND PS.UNIVCD = CR.UNIVCD ");
		strBuf.append("AND PS.FACULTYCD = CR.FACULTYCD ");
		strBuf.append("AND PS.DEPTCD = CR.DEPTCD ");
		strBuf.append("AND PS.EXAMTYPECD = EX.EXAMTYPECD ");
		strBuf.append("AND PS.SUBCD = ? ");//科目コード
		strBuf.append("ORDER BY CR.CANDIDATERANK ");
		return strBuf.toString();
	}
	
	/**
	 * 志望大学過去合格者平均をＤＢより取得
	 * 
	 * @param id		個人ＩＤ
	 * @param year		対象模試年度
	 * @param cd		対象模試コード
	 * @param runks	対象志望順位リスト
	 * @param subcd	対象科目コード
	 * @return String SQL
	 */
	private String getUnivCondition() {
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT ");
		strBuf.append(" /*+ INDEX(CR PK_CANDIDATERATING) */ ");//12/5 sql hint
		strBuf.append("CR.CANDIDATERANK, ");
		strBuf.append("HS.UNINAME_ABBR UNIVNAME_ABBR, ");
		strBuf.append("HS.FACULTYNAME_ABBR, ");
		strBuf.append("HS.DEPTNAME_ABBR, ");
		strBuf.append("ST.CURSUBCD, ");
		strBuf.append("ST.SUBCD, ");
		strBuf.append("PS.SUCCESSAVGDEV ");
		strBuf.append("FROM ");
		strBuf.append("EXAMINATION EX ");
		strBuf.append("INNER JOIN ");
		strBuf.append("SUBCDTRANS ST ");
//		strBuf.append("ON ST.YEAR = TO_NUMBER(EX.EXAMYEAR) -1 ");//[2] delete
		strBuf.append("ON ST.EXAMYEAR = TO_NUMBER(EX.EXAMYEAR) -1 ");//[2] add
		strBuf.append("AND ST.CURSUBCD = ? ");
		strBuf.append("AND ST.EXAMCD = ? ");//[2] add
		strBuf.append("INNER JOIN ");
		strBuf.append("CANDIDATERATING CR ");
		strBuf.append("ON EX.EXAMYEAR = ? ");//模試年度
		strBuf.append("AND EX.EXAMCD = ? ");//模試コード
		strBuf.append("AND EX.EXAMTYPECD IN ('01','02','03') ");
		strBuf.append("AND EX.EXAMCD NOT IN ('66','67') ");
		strBuf.append("AND CR.INDIVIDUALID = ? ");//個人ID
		strBuf.append("AND CR.EXAMYEAR = EX.EXAMYEAR ");
		strBuf.append("AND CR.EXAMCD = EX.EXAMCD ");
		strBuf.append("AND CR.CANDIDATERANK IN (#) ");//志望順位
		strBuf.append("LEFT JOIN ");
		strBuf.append("UNIVMASTER_BASIC HS ");
		strBuf.append("ON HS.EVENTYEAR = EX.EXAMYEAR ");
		strBuf.append("AND HS.EXAMDIV = EX.EXAMDIV ");
		strBuf.append("AND HS.UNIVCD = CR.UNIVCD ");
		strBuf.append("AND HS.FACULTYCD = CR.FACULTYCD ");
		strBuf.append("AND HS.DEPTCD = CR.DEPTCD ");
		strBuf.append("LEFT JOIN ");
		strBuf.append("PREVSUCCESS PS ");
		strBuf.append("ON PS.EXAMYEAR = TO_NUMBER(EX.EXAMYEAR) -1 ");
		strBuf.append("AND PS.UNIVCD = CR.UNIVCD ");
		strBuf.append("AND PS.FACULTYCD = CR.FACULTYCD ");
		strBuf.append("AND PS.DEPTCD = CR.DEPTCD ");
		strBuf.append("AND PS.EXAMTYPECD = EX.EXAMTYPECD ");
		//strBuf.append("AND PS.SUBCD = ? ");//科目コード
		strBuf.append("AND PS.SUBCD = ST.SUBCD ");//科目コード
		strBuf.append("ORDER BY CR.CANDIDATERANK ");
		return strBuf.toString();
	}
	
	/**
	 * 科目一覧
	 * この年のこの模試の科目コードと名称をすべて取得
	 * 
	 * @param String year	対象模試の年度
	 * @param String code	対象模試のコード
	 * @param String id	個人ＩＤ
	 * @return
	 */
	private Collection execSubNames(String year, String code, String id) throws Exception {
		//[5] 修正：共通項目に表示する科目のViewに存在しない科目はタブに出さない
		String kmkGet = 
			"SELECT " +
			" /*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */ "+
			//"EXAMSUBJECT.SUBCD, " +//[5] delete
			//"EXAMSUBJECT.SUBNAME " +//[5] delete
			"V_I_CMEXAMSUBJECT.SUBCD, " +//[5] add
			"V_I_CMEXAMSUBJECT.SUBNAME " +//[5] add
			"FROM " +
			//"SUBRECORD_I INNER JOIN EXAMSUBJECT ON SUBRECORD_I.SUBCD = EXAMSUBJECT.SUBCD " +//[5] delete
			//"AND SUBRECORD_I.EXAMYEAR = EXAMSUBJECT.EXAMYEAR "+//[5] delete
			//"AND SUBRECORD_I.EXAMCD = EXAMSUBJECT.EXAMCD "+//[5] delete
			"SUBRECORD_I INNER JOIN V_I_CMEXAMSUBJECT ON SUBRECORD_I.SUBCD = V_I_CMEXAMSUBJECT.SUBCD " +//[5] add
			"AND SUBRECORD_I.EXAMYEAR = V_I_CMEXAMSUBJECT.EXAMYEAR "+//[5] add
			"AND SUBRECORD_I.EXAMCD = V_I_CMEXAMSUBJECT.EXAMCD "+//[5] add
			"WHERE "+
			"SUBRECORD_I.EXAMYEAR = '"+ year +"' " +
			"AND SUBRECORD_I.EXAMCD = '"+ code + "' " +
			"AND SUBRECORD_I.INDIVIDUALID  = ? " ;
			if(getTargetGrade().equals("1")) {
				//kmkGet += "AND EXAMSUBJECT.examyear = '" + getThisYear() + "' ";//[5] delete
				kmkGet += "AND V_I_CMEXAMSUBJECT.examyear = '" + getThisYear() + "' ";//[5] add
			}
			kmkGet += "ORDER BY " +
			//"EXAMSUBJECT.DISPSEQUENCE ASC";//[5] delete
			"V_I_CMEXAMSUBJECT.DISPSEQUENCE ASC";//[5] add
			
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = conn.prepareStatement(kmkGet);
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			
			Collection subDatas = new ArrayList();
			int subCount = 0;
			while(rs.next()){

				SubjectData subData = new SubjectData();
				subData.setSubjectName(nullToEmpty(rs.getString("SUBNAME")));

				subData.setSubjectCd(nullToEmpty(rs.getString("SUBCD")));
				//初回検索時は、初期表示するためのサブコードを初期化
				if(isFirstAccess() && subCount == 0) {
					setTargetSubCd(subData.getSubjectCd());	
					setTargetSubNm(subData.getSubjectName());
				}
				//2回目なら画面から来た科目コードから科目名を探す
				if(isFirstAccess() == false){
					if(getTargetSubCd() == null || getTargetSubCd().equals("")) {
						if(Integer.parseInt(nullToEmpty(rs.getString("SUBCD")).substring(0, 1)) > 6) {
						} else {
							setTargetSubCd(subData.getSubjectCd());	
							setTargetSubNm(subData.getSubjectName());
						}
					} else if(getTargetSubCd().equals(nullToEmpty(rs.getString("SUBCD")))){
						setTargetSubCd(subData.getSubjectCd());	
						setTargetSubNm(subData.getSubjectName());
					}
				}

				if(Integer.parseInt(nullToEmpty(rs.getString("SUBCD")).substring(0, 1)) > 6) {
					//総合だったら追加しない
				} else {
					//総合ではない
					subDatas.add(subData);
					subCount++;
				}
			}
			if(pstmt != null) {pstmt.close();}
			if(rs != null) {rs.close();}
			
			return subDatas;
			
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}
	
	/**
	 * 対象模試の模試の情報を返します。
	 * @param year
	 * @param cd
	 * @return ExaminationData
	 */
	private IExamData getTargetExamData(String year, String cd) throws Exception{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		IExamData examData = new IExamData();
		String query = "SELECT TERGETGRADE, EXAMTYPECD FROM EXAMINATION WHERE EXAMYEAR = ? AND EXAMCD = ?";
		
		try {
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, year);
			pstmt.setString(2, cd);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				examData.setExamTypeCd(rs.getString("EXAMTYPECD"));
				examData.setTergetGrade(rs.getString("TERGETGRADE"));
			}
			if(pstmt != null) {pstmt.close();}
			if(rs != null) {rs.close();}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(pstmt != null) {pstmt.close();}
			if(rs != null){rs.close();}
		}
		return examData;
	}
	
	/**
	 * 志望大学のリストをＤＢより取得する。
	 * 
	 * @return Collection 志望大学リスト
	 * @throws SQLException
	 */
	private Collection execComboBoxDatas() throws Exception{
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String FULLSIZESPACE = "　";
		try{
			pstmt = conn.prepareStatement(getComboSql());
			pstmt.setString(1, personId);

			rs = pstmt.executeQuery();//ＳＱＬ実行
			Collection boxDatas = new ArrayList();
			//取得データをAttayListにつめる
			while(rs.next()){
				//大学情報
				IExamData niversityData = new IExamData();
				//志望順位の設定
				niversityData.setExamCd(nullToEmpty(rs.getString("CANDIDATERANK")));
				//志望大学名の設定
				niversityData.setExamName(
				nullToEmpty(rs.getString("UNIVNAME_ABBR")) + FULLSIZESPACE +
				nullToEmpty(rs.getString("FACULTYNAME_ABBR")) + FULLSIZESPACE + 
				nullToEmpty(rs.getString("DEPTNAME_ABBR"))
				);
				//名称の設定
				boxDatas.add(niversityData);
				//最大９個制限
				if(boxDatas.size() == 9) {
					break;
				}
			}
			if(pstmt != null) {pstmt.close();}
			if(rs != null) {rs.close();}
			
			return boxDatas;
			
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}
	
	/**
	 * 過年度合格者成績リストをＤＢより取得する。
	 * 
	 * @return Collection 過年度合格者成績リスト
	 * @throws SQLException
	 */
	private Collection execUniversityDatas() throws Exception {
		String FULLSIZESPACE = "  ";//アプレット対応のため半角スペース×２に修正
		
		//特殊パターン対応（過去合格者平均のみ適応）
		//以下の科目コードのときは、数学を使う
		boolean exFlg = false;
		String targetSubCd = nullToEmpty(getTargetSubCd());
		
		if(getTargetExamTypeCode().equals("01")) {
			if(targetSubCd.equals("2380")) {
				targetSubCd = "2000";
				exFlg = true;
			} else if(targetSubCd.equals("2480")){
				targetSubCd = "2000";
				exFlg = true;
			} else if(targetSubCd.equals("2580")){
				targetSubCd = "2000";
				exFlg = true;
			}
		}
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			Query query = null;
			if(exFlg) {
				query = new Query(getUnivConditionEx());
				query.setStringArray(1, getTargetCandidateRanks());
				pstmt = conn.prepareStatement(query.toString());
				//pstmt.setString(1, getTargetSubCd());
				pstmt.setString(1, getTargetExamYear());
				pstmt.setString(2, getTargetExamCode());
				pstmt.setString(3, getPersonId());
				pstmt.setString(4, targetSubCd);
			} else {
				query = new Query(getUnivCondition());
				query.setStringArray(1, getTargetCandidateRanks());
				pstmt = conn.prepareStatement(query.toString());
				//pstmt.setString(1, getTargetSubCd());
				pstmt.setString(1, targetSubCd);
				pstmt.setString(2, getTargetExamCode());//[2] add
				pstmt.setString(3, getTargetExamYear());//[2] add
				pstmt.setString(4, getTargetExamCode());//[2] add
				pstmt.setString(5, getPersonId());//[2] add
				//pstmt.setString(2, getTargetExamYear());//[2] delete
				//pstmt.setString(3, getTargetExamCode());//[2] delete
				//pstmt.setString(4, getPersonId());//[2] delete
			}
			rs = pstmt.executeQuery();

			Collection prevSuccessDatas = new ArrayList();
			
			while(rs.next()){
				PrevSuccessData prevSuccessData = new PrevSuccessData();
				prevSuccessData.setUnivName(nullToEmpty2(rs.getString("UNIVNAME_ABBR"))+FULLSIZESPACE+nullToEmpty2(rs.getString("FACULTYNAME_ABBR"))+FULLSIZESPACE+nullToEmpty2(rs.getString("DEPTNAME_ABBR")));
				if(exFlg) {
					prevSuccessData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
				} else {
					prevSuccessData.setSubCd(nullToEmpty(rs.getString("CURSUBCD")));
				}
				prevSuccessData.setSucAvgDev(GeneralUtil.nullToEmptyF(rs.getString("SUCCESSAVGDEV")));
				prevSuccessDatas.add(prevSuccessData);
			}
			if(pstmt != null) {pstmt.close();}
			if(rs != null) {rs.close();}
			return prevSuccessDatas;
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}
	
	/**
	 * 現年度を返します。
	 * @return 現年度
	 */
	protected String getThisYear() {
//[1] delete start
//		String thisYear = "";
//		Calendar cal1 = Calendar.getInstance();
//		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");//デートフォーマット
//		String nowYmd = sdf1.format(cal1.getTime());
//		int year = Integer.parseInt(nowYmd.substring(0, 4));
//		int monthDay = Integer.parseInt(nowYmd.substring(4, 8));		
//		if(monthDay < 0401) {
//			thisYear = Integer.toString(year - 1);
//		} else {
//			thisYear = Integer.toString(year);
//		}
//		return thisYear;
//[1] delete end
//		return KNUtil.getCurrentYear();//[1] add//[4] delete
		return getClassYear();//[4] add
	}
	
	/**
	 * 模試リストを取得します
	 * 
	 * @return Collection 模試リスト
	 * @throws Exception
	 */
	private Collection execDeviations() throws Exception{
		//1121
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = conn.prepareStatement(getMoshiSql());
			pstmt.setString(1, getPersonId());
			pstmt.setString(2, getTargetExamYear());
			pstmt.setString(3, getTargetExamCode());
			pstmt.setString(4, getTargetExamYear());
			pstmt.setString(5, getTargetExamCode());
//			pstmt.setString(6, getTargetSubCd());//[2] delete
//			pstmt.setString(7, getPersonId());//[2] delete 
//			pstmt.setString(8, getThisYear());//[2] delete 
			pstmt.setString(6, getTargetExamCode());//[2] add
			pstmt.setString(7, getTargetSubCd());//[2] add
			pstmt.setString(8, getPersonId());//[2] add
			pstmt.setString(9, getThisYear());//[2] add
			
			rs = pstmt.executeQuery();
			
			Collection examinationDatas = new ArrayList();
			//最大７個まで
			while(rs.next()) {
				//subrecord
				SubRecordIData subRecordData = new SubRecordIData();
				subRecordData.setCDeviation(GeneralUtil.nullToEmptyF(rs.getString("A_DEVIATION")));
				subRecordData.setScholarlevel(nullToEmpty(rs.getString("ACADEMICLEVEL")));
				subRecordData.setSubCd(nullToEmpty(rs.getString("CURSUBCD")));
				
				//subrecord -> list
				Collection subRecordDatas = new ArrayList();
				subRecordDatas.add(subRecordData);
				
				//list -> examination
				IExamData examinationData = new IExamData();
				examinationData.setExamYear(nullToEmpty(rs.getString("EXAMYEAR")));
				examinationData.setExamCd(nullToEmpty(rs.getString("EXAMCD")));
				examinationData.setExamName(nullToEmpty(rs.getString("EXAMNAME_ABBR")));
				examinationData.setExamTypeCd(nullToEmpty(rs.getString("EXAMTYPECD")));				
				examinationData.setSubRecordDatas(subRecordDatas);
				
				//７個要素を追加したら終了
				((List)examinationDatas).add(examinationData);
				if(examinationDatas.size() >= MAX_EXAM_NUM) {
					break;
				}
			}
			if(pstmt != null) {pstmt.close();}
			if(rs != null) {rs.close();}
		
			setExaminationDataSize(examinationDatas.size());
			if(getExaminationDataSize() != 0) {
				//テーブルの行数を指定
				setTargetExamNum(1);
			}
			
			for(; examinationDatas.size() < MAX_EXAM_NUM; ) {
				IExamData ex = new IExamData();
				Collection subs = new ArrayList();
				SubRecordIData sub = new SubRecordIData();
				subs.add(sub);
				ex.setSubRecordDatas(subs);
				examinationDatas.add(ex);
			}
			
			return examinationDatas;
		
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}
	
	/**
	 * ４年模試リストを取得します。
	 * @return 模試リスト
	 */
	private void execDeviationAll() throws Exception{
		
		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		try{
			Collection indexList = new ArrayList();	//見出しリスト
			boolean exExamFlg = true;					//例外処理フラグ
			
			//まずリストを作成
			pstmt1 = conn.prepareStatement(getNowExamCondition(getTargetExamYear(), getPersonId(), getTargetExamCode()));
			pstmt1.setString(1, personId);
			rs1 = pstmt1.executeQuery();
			
			//見出しリスト／ＭＡＸ７個
			while(rs1.next()) {
				IExamData examinationData = new IExamData();
				//examinationData.setExamYear(rs.getString("EXAMYEAR"));
				examinationData.setExamCd(nullToEmpty(rs1.getString("EXAMCD")));
				examinationData.setExamName(nullToEmpty(rs1.getString("EXAMNAME_ABBR")));
				//examinationData.setExamTypeCd(rs.getString("EXAMTYPECD"));
				
				String grade = nullToEmpty(rs1.getString("TERGETGRADE"));
				String type = nullToEmpty(rs1.getString("EXAMTYPECD"));
				
				//対象学年が２で、模試種類コードがマーク、記述で無いものが来たら、特殊パターンフラグを解除
				if(!(grade.equals("02") && (type.equals("01") || type.equals("02")))) {
					exExamFlg = false;
				}
				
				((List)indexList).add(examinationData);
				
				//７個要素を追加したら終了
				if(indexList.size() >= 7) {break;}
			}
			
			
			
			
			String baseYear = getTargetExamYear();		//テーブル表示基準年度（一番上の年度）
			//対象模試が、高２模試の場合
			if((getTargetExamTypeCode().equals("01") || getTargetExamTypeCode().equals("02")) && getTargetExamGrade().equals("02") && !exExamFlg) {
				baseYear = getNextYear(baseYear);
			}

			
			
			
			
			
			if(pstmt1 != null) {pstmt1.close();}
			if(rs1 != null) {rs1.close();}
			
			//見出しリストが対象学年２年の、マーク、記述のみの場合
			//--------------------模試の中身を取得----------------------//
			
			pstmt2 = conn.prepareStatement(getMoshiSql());
			pstmt2.setString(1, getPersonId());
			pstmt2.setString(2, getTargetExamYear());
			pstmt2.setString(3, getTargetExamCode());
			pstmt2.setString(4, getTargetExamYear());
			pstmt2.setString(5, getTargetExamCode());
			//pstmt2.setString(6, getTargetSubCd());//[2] delete
			//pstmt2.setString(7, getPersonId());//[2] delete
			pstmt2.setString(6, getTargetExamCode());//[2] add
			pstmt2.setString(7, getTargetSubCd());//[2] add
			pstmt2.setString(8, getPersonId());//[2] add
			rs2 = pstmt2.executeQuery();

			//模試情報リスト作成・初期化
			HashMap mappy = new HashMap();
			
			while(rs2.next()) {
				//subRecordData
				SubRecordIData subRecordData = new SubRecordIData();
				subRecordData.setCDeviation(GeneralUtil.nullToEmptyF(rs2.getString("A_DEVIATION")));
				subRecordData.setScholarlevel(nullToEmpty(rs2.getString("ACADEMICLEVEL")));
				subRecordData.setSubCd(nullToEmpty(rs2.getString("CURSUBCD")));
				
				//subRecordData -> list
				Collection subRecordDatas = new ArrayList();
				subRecordDatas.add(subRecordData);
				
				//list -> examnationData
				IExamData examData = new IExamData();
				examData.setExamYear(nullToEmpty(rs2.getString("EXAMYEAR")));
				examData.setExamCd(nullToEmpty(rs2.getString("EXAMCD")));
				examData.setExamName(nullToEmpty(rs2.getString("EXAMNAME_ABBR")));
				examData.setExamTypeCd(nullToEmpty(rs2.getString("EXAMTYPECD")));
				examData.setTergetGrade(nullToEmpty(rs2.getString("TERGETGRADE"))); 
				examData.setSubRecordDatas(subRecordDatas);
				
				//対象学年＝２年、かつ、模試種類がマークもしくは記述のとき、ひとつ上の年度(＋１年)に模試年度を変えてしまう。
				//追加：特殊パターンフラグがオフのときのみ
				if(examData.getTergetGrade().equals("02") && 
					(examData.getExamTypeCd().equals("01") || examData.getExamTypeCd().equals("02")) && !exExamFlg) {
					examData.setExamYear(getNextYear(examData.getExamYear()));
				}
				
				//対象年度より１年前、２年前の模試を取得
				int passYear = Integer.parseInt(baseYear) - Integer.parseInt(examData.getExamYear());
				if(passYear == 0) {
					//何もしない
					setDispTargetExamYear(baseYear);
				} else if(passYear == 1 && subRecordData.getCDeviation() != null) {
					setTargetExamYear2(getLastYear(baseYear));
				} else if(passYear == 2 && subRecordData.getCDeviation() != null) {
					setTargetExamYear3(getLastYear(getLastYear(baseYear)));
				}
				
				//コード＋年度をキーにマップに詰める。
				mappy.put(examData.getExamYear() + examData.getExamCd(), examData);//模試マップに追加
			}
			if(pstmt2 != null) {pstmt2.close();}
			if(rs2 != null) {rs2.close();}
			
			//------------------------------------模試リストに詰めていく----------------------------------------------//		
			
			//それぞれ模試リストが存在しない場合初期化
			if(isNull(getExaminationDatas()))	{setExaminationDatas(new ArrayList());}
			if(isNull(getExaminationDatas2()))	{setExaminationDatas2(new ArrayList());}
			if(isNull(getExaminationDatas3()))	{setExaminationDatas3(new ArrayList());}
			
			//見出しリストに合わせて、中身を模試リストを詰めていく。
			for (Iterator it=indexList.iterator(); it.hasNext();) {
				IExamData examData = (IExamData)it.next();
				
				//対象年度
				IExamData exam = (IExamData)mappy.get(baseYear + examData.getExamCd());
				if(isNull(exam)) {//無かったら、空を作る。
					exam = IExamData.getEmptyIExamData();
					//見出しをつける
					exam.setExamName(examData.getExamName());
					getExaminationDatas().add(exam);
				} else {
					//見出しをつける
					exam.setExamName(examData.getExamName());
					getExaminationDatas().add(exam);
				}
				
				//１年前
				if(!isNull(getTargetExamYear2())) {
					//模試取得
					IExamData ex = (IExamData)mappy.get(getLastYear(baseYear) + examData.getExamCd());
					if(isNull(ex)) {//無かったら、空を作る。
						getExaminationDatas2().add(IExamData.getEmptyIExamData());
					} else {
						getExaminationDatas2().add(ex);
					}
				}
				
				//２年前
				if(!isNull(getTargetExamYear3())) {
					//模試取得
					IExamData ex = (IExamData)mappy.get(getLastYear(getLastYear(baseYear)) + examData.getExamCd());
					if(isNull(ex)) {//無かったら、空を作る。
						getExaminationDatas3().add(IExamData.getEmptyIExamData());
					} else {
						getExaminationDatas3().add(ex);
					}
				}
			}
			
			setExaminationDataSize(examinationDatas.size());
			int MAX_EXAM_NUM = 7;
			
			//７個に足り無い分をリストに詰める。
			for(int i = indexList.size(); i < MAX_EXAM_NUM; i++) {
				//対象年度なので絶対ある
				getExaminationDatas().add(IExamData.getEmptyIExamData());
				//１年前の模試があるなら
				if(!isNull(getTargetExamYear2())) {
					getExaminationDatas2().add(IExamData.getEmptyIExamData());
				}
				//２年前の模試があるなら
				if(!isNull(getTargetExamYear3())) {
					getExaminationDatas3().add(IExamData.getEmptyIExamData());
				}
			}
			
			//何年度分あるかを入れておく。
			setTargetExamNum(1);
			//１年前のがある。
			if(!isNull(getTargetExamYear2())) {setTargetExamNum(getTargetExamNum() + 1);}
			//２年前のがある。
			if(!isNull(getTargetExamYear3())) {setTargetExamNum(getTargetExamNum() + 1);}
			
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt1 != null) {pstmt1.close();} 
			if(pstmt2 != null) {pstmt1.close();}
			if(rs1 != null) {rs1.close();}
			if(rs2 != null) {rs2.close();}
		}

	}
	
	/**
	 * 来年を返します。
	 * @param year
	 * @return 
	 */
	private String getNextYear(String year) {
		return Integer.toString(Integer.parseInt(year) +1);
	}
	
	/**
	 * 昨年を返します。
	 * @param year
	 * @return
	 */
	private String getLastYear(String year) {
		return Integer.toString(Integer.parseInt(year) -1);
	}
	
	/**
	 * nullか？
	 */
	private boolean isNull(Object obj) {
		if(obj == null) {
			return true;
		}
		return false;
	}
	
	/**
	 * アプレット用の科目名を取得します。
	 * @return 科目
	 */
	public String getLineItemNameS() {
		String item = null;
		//模試がひとつ
		if(getTargetExamNum() == 1) {

			//対象科目をそのまま返す
			return getTargetSubNm();
		}
		//対象年度
		item = getTargetSubNm() + "(" + getDispTargetExamYear() + ")" + YEAR;
		//１年前
		if(!isNull(getTargetExamYear2())) {
			if(!isNull(item)) {
				item += ",";
			}
			item += getTargetSubNm() + "(" + getTargetExamYear2() + ")" + YEAR;
		}
		//２年前
		if(!isNull(getTargetExamYear3())) {
			if(!isNull(item)) {
				item += ",";
			}
			item += getTargetSubNm() + "(" + getTargetExamYear3() + ")" + YEAR;
		}
		return item;
	}
	

	
	/**
	 * 対象年度に、対象生徒が何年生かを返します。
	 * @param year	対象年度
	 * @param id	生徒ＩＤ
	 * @return int 学年
	 * @throws Exception
	 */
	private int execHistoryGrade(String id) throws Exception{
		String gradeGet = "select HISTORYINFO.GRADE " +
			"from HISTORYINFO " +
			"where HISTORYINFO.INDIVIDUALID = ? " +
			//"and HISTORYINFO.YEAR = '" + year + "' ";//[3] delete
			"and HISTORYINFO.YEAR = '" + getClassYear() + "' ";//[3] add
			
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = conn.prepareStatement(gradeGet);
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();//SQL実行
			
			String grade = null;
			while(rs.next()) {
				grade = rs.getString("GRADE");
			}
			if(pstmt != null) {pstmt.close();}
			if(rs != null) {rs.close();}
			
			if(grade == null) {
				return 0;
			}
			
			return Integer.parseInt(grade.trim());
			
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
		
	}
	
//[2] add start
	/**
	 * 科目変換テーブル作成のための模試リストを作成する。
	 */
	private ExamData[] getExamDatas() throws Exception {
		Collection examDatas = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(getExamDatasSQL(getTargetExamYear(), getTargetExamCode(), getPersonId(), getTargetGrade()));
			ps.setString(1, personId);
			rs = ps.executeQuery();
			while(rs.next()) {
				ExamData examData = new ExamData();
				examData.setExamYear(rs.getString("EXAMYEAR"));
				examData.setExamCD(rs.getString("EXAMCD"));
				examDatas.add(examData);
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(ps != null) ps.close();
			if(rs != null) rs.close();
		}
		
		return (ExamData[])examDatas.toArray(new ExamData[examDatas.size()]);
	}
	
	/**
	 * 科目変換テーブル作成のための模試リストを取得するためのＳＱＬを作成する。
	 * @param year 対象模試年度
	 * @param cd 対象模試コード
	 * @param id 個人ＩＤ
	 * @param grade 年度フラグ
	 * @return
	 */
	private String getExamDatasSQL(String year, String cd, String id, String grade) {
		String DATAOPENDATE = null;
		if(isHelpflg()) {
			DATAOPENDATE = "IN_DATAOPENDATE";
		} else {
			DATAOPENDATE = "OUT_DATAOPENDATE";
		}
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT ");
		strBuf.append("/*+ INDEX(SI PK_SUBRECORD_I) */ ");
		strBuf.append("EX.EXAMYEAR ");
		strBuf.append(",EX.EXAMCD ");
		strBuf.append("FROM ");
		strBuf.append("EXAMINATION EX ");
		strBuf.append("INNER JOIN ");
		strBuf.append("SUBRECORD_I SI ");
		strBuf.append("ON ");
		strBuf.append("EX.").append(DATAOPENDATE).append(" <= (SELECT ").append(DATAOPENDATE).append(" FROM EXAMINATION WHERE EXAMYEAR = '").append(year).append("' AND EXAMCD = '").append(cd).append("') ");
		strBuf.append("AND SI.INDIVIDUALID = ? ");
		strBuf.append("AND EX.EXAMTYPECD = (SELECT EXAMTYPECD FROM EXAMINATION WHERE EXAMYEAR = '").append(year).append("' AND EXAMCD = '").append(cd).append("') ");
		strBuf.append("AND EX.EXAMYEAR = SI.EXAMYEAR ");
		strBuf.append("AND EX.EXAMCD = SI.EXAMCD ");
		if(grade.equals("1")) {
			strBuf.append("AND EX.EXAMYEAR = '").append(year).append("' ");
		}
		strBuf.append("GROUP BY EX.EXAMYEAR, EX.EXAMCD ");
		return strBuf.toString();
	}
//[2] add end
	
	/**
	 * アプレット用の模試名称配列を返します。
	 * @return
	 */
	public String getExamNames() {
		Collection collection = getExaminationDatas();
		String examNames = "";
		int count = 0;
		for (Iterator it=collection.iterator(); it.hasNext();) {
			IExamData examinationData = (IExamData)it.next();
			String examName = examinationData.getExamName();
			if(count != 0) {
				//カンマ区切り
				examNames += ",";
			}
			if(examName == null) {
				//名称
				examName = "";				
			}
			examNames += examName;
			if(count < getExaminationDataSize() -1) {count++;}
			else {break;} 
		}
		return examNames;
	}
	
	
	
	/**
	 * アプレット用の模試偏差値を返します。
	 * @return
	 */
	public String getExamDeviations() {
		//リスト１
		int count = 0;
		String examDevs = "";
		
		for (Iterator it=getExaminationDatas().iterator(); it.hasNext();) {
			String examDev = null; 
			IExamData examinationData = (IExamData)it.next();
			
			for(java.util.Iterator itr=examinationData.getSubRecordDatas().iterator(); itr.hasNext();) {
				SubRecordIData sdata = (SubRecordIData)itr.next();
				examDev = sdata.getCDeviation();
			}
			if(count != 0) {
				//カンマ区切り
				examDevs += ",";
			}
			if(examDev != null) {
				if(examDev.equals("")) {
					//データがないなら無効値
					examDev = "-999.0";
				}
			} else {
				examDev = "-999.0";
			}

			examDevs += examDev;
			if(count < getExaminationDataSize()-1) {count++;}
			else {break;} 
		}
		return examDevs;
	}
	
	public String getExamDeviations2() {
		//リスト１
		int count = 0;
		String examDevs = "";
		
		for (Iterator it=getExaminationDatas2().iterator(); it.hasNext();) {
			String examDev = null; 
			IExamData examinationData = (IExamData)it.next();
			
			for(java.util.Iterator itr=examinationData.getSubRecordDatas().iterator(); itr.hasNext();) {
				SubRecordIData sdata = (SubRecordIData)itr.next();
				examDev = sdata.getCDeviation();
			}
			
			if(count != 0) {
				//カンマ区切り
				examDevs += ",";
			}
		
		if(examDev != null) {
			if(examDev.equals("")) {
				//データがないなら無効値
				examDev = "-999.0";
			}
		} else {
			examDev = "-999.0";
		}
			examDevs += examDev;
			if(count < getExaminationDataSize()-1) {count++;}
			else {break;} 
		}
		return examDevs;
	}
	
	public String getExamDeviations3() {
		//リスト１
		int count = 0;
		String examDevs = "";
		
		for (Iterator it=getExaminationDatas3().iterator(); it.hasNext();) {
			String examDev = null; 
			IExamData examinationData = (IExamData)it.next();
			
			for(java.util.Iterator itr=examinationData.getSubRecordDatas().iterator(); itr.hasNext();) {
				SubRecordIData sdata = (SubRecordIData)itr.next();
				examDev = sdata.getCDeviation();
			}
			
			if(count != 0) {
				//カンマ区切り
				examDevs += ",";
			}
		if(examDev != null) {
			if(examDev.equals("")) {
				//データがないなら無効値
				examDev = "-999.0";
			}
		} else {
			examDev = "-999.0";
		}
			examDevs += examDev;
			if(count < getExaminationDataSize()-1) {count++;}
			else {break;} 
		}
		return examDevs;
	}
		
	public String getRowName1() {
		if(getTargetExamNum() == 1) {
			return TARGET_PERSON;
		} else {
			return TARGET_PERSON + "（" + getDispTargetExamYear() + YEAR +"）";
		}
	}
	public String getRowName2() {
		if(getTargetExamNum() == 1) {
			return TARGET_PERSON;
		} else {
			
			return TARGET_PERSON + "（" + getTargetExamYear2() + YEAR + "）";
		}
	}
	public String getRowName3() {
		if(getTargetExamNum() == 1) {
			return TARGET_PERSON;
		} else {
			
			return TARGET_PERSON + "（" + getTargetExamYear3() + YEAR + "）";
		}
	}
	
	/**
	 * nullが来たら空白にする
	 */
	private String nullToEmpty(String str) {
		if(str == null) {
			return "";
		}
		if(str.equals("null")) {
			return "";
		}
		return str;
	}
	
	/**
	 * nullが来たら全角の空白を入れる
	 */
	private String nullToEmpty2(String str) {
		if(str == null) {
			return "　　";
		}
		if(str.equals("null")) {
			return "";
		}
		return str;
	}
	/**
	 * @return
	 */
	public String getMoshiSql() {
		return moshiSql;
	}

	/**
	 * @return
	 */
	public String getPersonId() {
		return personId;
	}

	/**
	 * @return
	 */
	public String getSQL_BASE() {
		return SQL_BASE;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * 志望順位を返します
	 * @return
	 */
	public String[] getTargetCandidateRanks() {
		return targetCandidateRanks;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}
	
	/**
	 * @param string
	 */
	public void setMoshiSql(String string) {
		moshiSql = string;
	}

	/**
	 * @param string
	 */
	public void setPersonId(String string) {
		personId = string;
	}

	/**
	 * @param string
	 */
	public void setSQL_BASE(String string) {
		SQL_BASE = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param strings
	 */
	public void setTargetCandidateRanks(String[] strings) {
		targetCandidateRanks = strings;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * @return
	 */
	public String getComboSql() {
		return comboSql;
	}

	/**
	 * @param string
	 */
	public void setComboSql(String string) {
		comboSql = string;
	}

	/**
	 * @return
	 */
	public boolean isFirstAccess() {
		return isFirstAccess;
	}

	/**
	 * @param b
	 */
	public void setFirstAccess(boolean b) {
		isFirstAccess = b;
	}

	/**
	 * @return
	 */
	public Collection getComboxDatas() {
		return comboxDatas;
	}

	/**
	 * @param collection
	 */
	public void setComboxDatas(Collection collection) {
		comboxDatas = collection;
	}

	/**
	 * @return
	 */
	public String getSubNumSql() {
		return subNumSql;
	}

	/**
	 * @param string
	 */
	public void setSubNumSql(String string) {
		subNumSql = string;
	}

	/**
	 * @return
	 */
	public Collection getSubDatas() {
		return subDatas;
	}

	/**
	 * @param collection
	 */
	public void setSubDatas(Collection collection) {
		subDatas = collection;
	}
	
	/**
	 * 模試情報を取得します
	 * @return Collection
	 */
	public Collection getExaminationDatas() {
		return examinationDatas;
	}
	
	/**
	 * 模試情報を設定します
	 * @param Collection
	 */
	public void setExaminationDatas(Collection collection) {
		examinationDatas = collection;
	}

	/**
	 * 対象科目を設定します
	 * @param String subcd
	 */
	public void setTargetSubCd(String subcd) {
		targetSubCd = subcd;
	}
	
	/**
	 * 対象科目を返します
	 * @return String
	 */
	public String getTargetSubCd() {
		return targetSubCd;	
	}
	
	/**
	 * 過年度合格者成績リストを設定します
	 * @param Collection colection
	 */
	public void setPrevSuccessDatas(Collection colection) {
		prevSuccessDatas = colection;
	}
	/**
	 * 過年度合格者成績リストを返します
	 * @return Collection
	 */
	public Collection getPrevSuccessDatas() {
		return prevSuccessDatas;
	}
	/**
	 * @return
	 */
	public String getTargetSubNm() {
		return targetSubNm;
	}

	/**
	 * @param string
	 */
	public void setTargetSubNm(String string) {
		targetSubNm = string;
	}

	/**
	 * @return
	 */
	public String getTargetGrade() {
		return targetGrade;
	}

	/**
	 * @param string
	 */
	public void setTargetGrade(String string) {
		targetGrade = string;
	}

	/**
	 * @return
	 */
	public int getExaminationDataSize() {
		return examinationDataSize;
	}

	/**
	 * @param i
	 */
	public void setExaminationDataSize(int i) {
		examinationDataSize = i;
	}

	/**
	 * @return
	 */
	public int getHistoryGrade() {
		return historyGrade;
	}

	/**
	 * @param string
	 */
	public void setHistoryGrade(int string) {
		historyGrade = string;
	}

	/**
	 * @return
	 */
	public Collection getExaminationDatas2() {
		return examinationDatas2;
	}

	/**
	 * @return
	 */
	public Collection getExaminationDatas3() {
		return examinationDatas3;
	}

	/**
	 * @param collection
	 */
	public void setExaminationDatas2(Collection collection) {
		examinationDatas2 = collection;
	}

	/**
	 * @param collection
	 */
	public void setExaminationDatas3(Collection collection) {
		examinationDatas3 = collection;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear2() {
		return targetExamYear2;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear3() {
		return targetExamYear3;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear2(String string) {
		targetExamYear2 = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear3(String string) {
		targetExamYear3 = string;
	}

	/**
	 * @return
	 */
	public int getTargetExamNum() {
		return targetExamNum;
	}

	/**
	 * @param string
	 */
	public void setTargetExamNum(int num) {
		targetExamNum = num;
	}

	/**
	 * @return
	 */
	public boolean isHelpflg() {
		return helpflg;
	}

	/**
	 * @param b
	 */
	public void setHelpflg(boolean b) {
		helpflg = b;
	}

	/**
	 * @return
	 */
	public String getTargetExamTypeCode() {
		return targetExamTypeCode;
	}

	/**
	 * @param string
	 */
	public void setTargetExamTypeCode(String string) {
		targetExamTypeCode = string;
	}

	/**
	 * @return
	 */
	public String getTargetExamGrade() {
		return targetExamGrade;
	}

	/**
	 * @param string
	 */
	public void setTargetExamGrade(String string) {
		targetExamGrade = string;
	}

	/**
	 * @return
	 */
	public String getDispTargetExamYear() {
		return dispTargetExamYear;
	}

	/**
	 * @param string
	 */
	public void setDispTargetExamYear(String string) {
		dispTargetExamYear = string;
	}

	/**
	 * @param クラス年度
	 */
	public void setClassYear(String string) {
		classYear = string;
	}

	/**
	 * @return クラス年度
	 */
	public String getClassYear() {
		return classYear;
	}

}

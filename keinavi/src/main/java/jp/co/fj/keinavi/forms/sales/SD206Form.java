package jp.co.fj.keinavi.forms.sales;

import jp.co.fj.keinavi.forms.MethodInvokerForm;

/**
 * 
 * 特例成績データ　申請一覧確認（パスワード通知書ダウンロード）画面のアクションフォームです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD206Form extends MethodInvokerForm {

	/** serialVersionUID */
	private static final long serialVersionUID = -8841405894669409369L;

	/** 対象年度 */
	private String examYear;

	/** 絞込み条件 */
	private String condition;

	/** ソートキー */
	private String sortKey;

	/** 申請ID */
	private String appId;

	/**
	 * ソートキーを返します。
	 * 
	 * @return ソートキー
	 */
	public String getSortKey() {
		return sortKey;
	}

	/**
	 * ソートキーをセットします。
	 * 
	 * @param soryKey
	 *            ソートキー
	 */
	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}

	/**
	 * 申請IDを返します。
	 * 
	 * @return 申請ID
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * 申請IDをセットします。
	 * 
	 * @param appId
	 *            申請ID
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * 対象年度を返します。
	 * 
	 * @return 対象年度
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * 対象年度をセットします。
	 * 
	 * @param examYear
	 *            対象年度
	 */
	public void setExamYear(String examYear) {
		this.examYear = examYear;
	}

	/**
	 * 絞込み条件を返します。
	 * 
	 * @return 絞込み条件
	 */
	public String getCondition() {
		return condition;
	}

	/**
	 * 絞込み条件をセットします。
	 * 
	 * @param condition
	 *            絞込み条件
	 */
	public void setCondition(String condition) {
		this.condition = condition;
	}

}

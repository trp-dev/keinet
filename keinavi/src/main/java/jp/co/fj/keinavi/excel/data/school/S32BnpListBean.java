package jp.co.fj.keinavi.excel.data.school;

/**
 * �΍��l���z�i�N���X��r�j�f�[�^�N���X
 * �쐬��: 2004/06/28
 * @author	T.Sakai
 */
public class S32BnpListBean {
	//�΍��l�я���l
	private float floBnpMax = 0;
	//�΍��l�щ����l
	private float floBnpMin = 0;
	//�l��
	private int intNinzu = 0;
	//�\����
	private float floKoseihi = 0;
	
	/*----------*/
	/* Get      */
	/*----------*/
	public float getFloBnpMax() {
		return this.floBnpMax;
	}
	public float getFloBnpMin() {
		return this.floBnpMin;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public float getFloKoseihi() {
		return this.floKoseihi;
	}

	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setFloBnpMax(float floBnpMax) {
		this.floBnpMax = floBnpMax;
	}
	public void setFloBnpMin(float floBnpMin) {
		this.floBnpMin = floBnpMin;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setFloKoseihi(float floKoseihi) {
		this.floKoseihi = floKoseihi;
	}

}

package jp.co.fj.keinavi.beans;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.util.sql.Query;

import com.fjh.beans.DefaultSearchBean;

/**
 * 模試検索を実行する。
 * 
 * <2010年度マーク高２模試対応>
 * 2011.02.07	Tomohisa YAMADA - Totec
 * 				新規作成
 *
 */
public class ExamSearchBean extends DefaultSearchBean {
	
	//模試年度
	protected String examYear;
	
	//模試コード
	protected String examCD;
	
	/**
	 * コンストラクタ
	 * @param examYear
	 * @param examCD
	 */
	public ExamSearchBean(String examYear, String examCD) {
		this.examYear = examYear;
		this.examCD = examCD;
	}
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
				Query query = new Query("SELECT");
				query.append(" e.examyear");
				query.append(",e.examcd");
				query.append(",e.examname");
				query.append(",e.examname_abbr");
				query.append(",e.examtypecd");
				query.append(",e.examst");
				query.append(",e.examdiv");
				query.append(",e.tergetgrade");
				query.append(",e.inpledate");
				query.append(",e.in_dataopendate");
				query.append(",e.out_dataopendate");
				query.append(",e.dockingexamcd");
				query.append(",e.dockingtype");
				query.append(",e.dispsequence");
				query.append(",e.masterdiv");
				query.append(" FROM");
				query.append(" examination e ");
				query.append(" WHERE ");
				query.append(" e.examyear = ? ");
				query.append(" AND ");
				query.append(" e.examcd = ? ");
			
				ps = conn.prepareStatement(query.toString());
				ps.setString(1, examYear);
				ps.setString(2, examCD);

				rs = ps.executeQuery();
				while (rs.next()) {

					// 模試データを作る
					ExamData data = new ExamData();
					data.setExamYear(rs.getString("examyear"));
					data.setExamCD(rs.getString("examcd"));
					data.setExamName(rs.getString("examname"));
					data.setExamNameAbbr(rs.getString("examname_abbr"));
					data.setExamTypeCD(rs.getString("examtypecd"));
					data.setExamST(rs.getString("examst"));
					data.setExamDiv(rs.getString("examdiv"));
					data.setTargetGrade(rs.getString("tergetgrade"));
					data.setInpleDate(rs.getString("inpledate"));
					data.setOutDataOpenDate(rs.getString("in_dataopendate"));
					data.setInDataOpenDate(rs.getString("out_dataopendate"));
					data.setDockingExamCD(rs.getString("dockingexamcd"));
					data.setDockingType(rs.getString("dockingtype"));
					data.setDispSequence(rs.getInt("dispsequence"));
					data.setMasterDiv(rs.getString("masterdiv"));
					recordSet.add(data);
				}

		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 検索模試を返す。
	 * @return
	 */
	public ExamData getExam() {
		
		if (recordSet.size() != 1) {
			throw new InternalError("取得模試数が異常です。" + recordCount);
		}
		
		return (ExamData) recordSet.get(0);
	}
}

package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 生徒再表示Bean
 * 
 * 2006.11.01	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class StudentReDisplayBean extends DefaultBean {

	// 学校コード
	private final String schoolCd;
	// 対象個人ID
	private String[] individualId;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 処理する学校コード
	 */
	public StudentReDisplayBean(final String pSchoolCd) {
		schoolCd = pSchoolCd;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					"UPDATE basicinfo SET notdisplayflg = NULL "
					+ "WHERE schoolcd = ? AND individualid = ?");
			ps.setString(1, schoolCd);
			for (int i = 0; i < individualId.length; i++) {
				ps.setString(2, individualId[i]);
				ps.executeUpdate();
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * @param pIndividualId 設定する individualId。
	 */
	public void setIndividualId(String[] pIndividualId) {
		individualId = pIndividualId;
	}
	
}

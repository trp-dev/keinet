/**
 * 校内成績分析−クラス比較　成績概況（クラス比較）
 * 	Excelファイル編集
 * 作成日: 2004/07/26
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S31ClassListBean;
import jp.co.fj.keinavi.excel.data.school.S31Item;
import jp.co.fj.keinavi.excel.data.school.S31ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S31_01 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
	private boolean bolBookCngFlg = true;			//改ファイルフラグ
	private boolean bolSheetCngFlg = true;			//改シートフラグ
//add end

	final private String masterfile0 = "S31_01";
	final private String masterfile1 = "NS31_01";
	private String masterfile = "";
	final private int intMaxSheetSr = 50;	//MAXシート数の値を入れる

/*
 * 	Excel編集メイン
 * 		S31Item s31Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s31_01EditExcel(S31Item s31Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		int		intMaxSheetIndex	= 0;
		int		intFileIndex	= 1;
		
		//テンプレートの決定
		if (s31Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S31ListBean s31ListBean = new S31ListBean();

		try {
			
			// データセット
			ArrayList s31List = s31Item.getS31List();
			Iterator itr = s31List.iterator();
			int row = 0;				//行
			int col = 0;				//列
			int classCnt = 0;			//クラスカウンタ
			int kmkCnt = 0;			//科目カウンタ
			int hyouCnt = 0;			//表カウンタ（0…上段、1…下段）
			String kmk = "";			//科目チェック用
			int maxClass = 0;			//MAXクラス数
			boolean kmkFlg = true;	//型→科目変更フラグ
			boolean kataExistFlg = false;	//型有無フラグ

//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
			int		sheetListIndex	= 0;		//シートカウンター
			int		houjiNum		= 41;		//1シートに表示できるクラス数(自校＋クラス)
			int		sheetRowCnt		= 0;		//クラス用改シート格納用カウンタ
			ArrayList	workbookList	= new ArrayList();
			ArrayList	workSheetList	= new ArrayList();
//add end
			
			while( itr.hasNext() ) {
				s31ListBean = (S31ListBean)itr.next();
				
				//型有無チェック 2004.11.17 add
				if (s31ListBean.getStrKmkCd().compareTo("7000") >= 0) {
					kataExistFlg = true;
				}

				// 基本ファイルを読込む
				S31ClassListBean s31ClassListBean = new S31ClassListBean();
				
				// クラスデータセット
				ArrayList s31ClassList = s31ListBean.getS31ClassList();
				Iterator itrClass = s31ClassList.iterator();
				
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
				maxClass = 0;
				maxClass = s31ClassList.size();
				
				// クラス表示に必要なシート数の計算
				sheetRowCnt = (maxClass-1)/(houjiNum-1);
				if((maxClass-1)%(houjiNum-1)!=0){
					sheetRowCnt++;
				}
				if (sheetRowCnt==0) {
					sheetRowCnt++;
				}
				
				//科目が変わる時のチェック
				if ( !cm.toString(kmk).equals(cm.toString(s31ListBean.getStrKmkCd())) && !cm.toString(kmk).equals("") ) {
					//クラス数が20以下
					if ((maxClass-1)<=(houjiNum-1)/2) {
						if (kmkFlg==true) {
							//型があるときのみ型→科目のチェックをする 2004.11.17 add
							if (kataExistFlg == true){
								//型から科目に変わる時のチェック
								if (s31ListBean.getStrKmkCd().compareTo("7000") < 0) {
									col = 0;
									kmkCnt = 0;
									kmkFlg = false;
									//クラス数が20以下のときの改表、改シート、改ファイル判断
									if (hyouCnt==0) {
										hyouCnt = 1;
									} else if (hyouCnt==1) {
										hyouCnt = 0;
										bolSheetCngFlg = true;
										if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
											bolBookCngFlg = true;
										}
									}
								}
							}else{
								kmkFlg = false;
							}
							//型が10のときの改表、改シート、改ファイル判断
							if (kmkCnt==10) {
								col = 0;
								kmkCnt = 0;
								if (hyouCnt==0) {
									hyouCnt = 1;
								} else if (hyouCnt==1) {
									hyouCnt = 0;
									bolSheetCngFlg = true;
									if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
										bolBookCngFlg = true;
									}
								}
							}
						} else {
							//科目が10のときの改表、改シート、改ファイル判断
							if (kmkCnt==10) {
								col = 0;
								kmkCnt = 0;
								if (hyouCnt==0) {
									hyouCnt = 1;
								} else if (hyouCnt==1) {
									hyouCnt = 0;
									bolSheetCngFlg = true;
									if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
										bolBookCngFlg = true;
									}
								}
							}
						}
					} else {
						if (kmkFlg==true) {
							//型があるときのみ型→科目のチェックをする 2004.11.17 add
							if (kataExistFlg == true){
								//型から科目に変わる時のチェック
								if (s31ListBean.getStrKmkCd().compareTo("7000") < 0) {
									col = 0;
									kmkCnt = 0;
									bolSheetCngFlg = true;
									kmkFlg = false;
									if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
										bolBookCngFlg = true;
									}
								}
							}else{
								kmkFlg = false;
							}
							//型が10のとき改シート
							if (kmkCnt==10) {
								col = 0;
								kmkCnt = 0;
								bolSheetCngFlg = true;
								if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
									bolBookCngFlg = true;
								}
							}
						} else {
							//科目が10のとき改シート
							if (kmkCnt==10) {
								col = 0;
								kmkCnt = 0;
								bolSheetCngFlg = true;
								if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
									bolBookCngFlg = true;
								}
							}
						}
						hyouCnt = 0;
					}
				}
//add end
				
				int ninzu = 0;
				float heikin = 0;
				float hensa = 0;
				
				while ( itrClass.hasNext() ){
					s31ClassListBean = (S31ClassListBean)itrClass.next();
					
					if( bolBookCngFlg == true ){
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
						if (maxClass<=houjiNum){
							if(intMaxSheetIndex >= intMaxSheetSr){
								// Excelファイル保存
								boolean bolRet = false;
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intFileIndex, masterfile, intMaxSheetIndex);
								intFileIndex++;
								if( bolRet == false ){
									return errfwrite;					
								}
							}
						}
//add end			
						//マスタExcel読み込み
						workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
						if( workbook==null ){
							return errfread;
						}
						intMaxSheetIndex=0;
						bolBookCngFlg = false;
						workbookList.add(workbook);
					}
					//クラスが20以下のときは20型･科目ごと、クラスが21以上のときは10型･科目ごとに改シート
					if ( bolSheetCngFlg ) {
						if ((maxClass-1)<=(houjiNum-1)/2) {
							// シートテンプレートのコピー
							workSheet = workbook.cloneSheet(0);
							intMaxSheetIndex++;
						}else{
							// データセットするシートの選択
							if (kmkCnt==0) {
								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);
								workSheetList.add(sheetListIndex, workSheet);
								intMaxSheetIndex++;
								sheetListIndex++;
							} else {
								//次列用シートの呼び出し
								workSheet = (HSSFSheet)workSheetList.get(sheetListIndex-sheetRowCnt);
								sheetRowCnt--;
							}
						}
						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);
					
						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, s31Item.getIntSecuFlg() ,38 ,40 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 38 );
						workCell.setCellValue(secuFlg);
					
						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( "学校名　：" + cm.toString(s31Item.getStrGakkomei()) );

						// 模試月取得
						String moshi =cm.setTaisyouMoshi( s31Item.getStrMshDate() );
						// 対象模試セット
						workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
						workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s31Item.getStrMshmei()) + moshi);
						hyouCnt = 0;
						bolSheetCngFlg = false;
					}
					if (classCnt==0) {
						if ( hyouCnt==0 ) {
							row = 4;
						}
						if ( hyouCnt==1 ) {
							row = 33;
						}
					
						if ( kmkCnt==0 || !cm.toString(kmk).equals(cm.toString(s31ListBean.getStrKmkCd())) ) {
							// 型・科目名セット
							workCell = cm.setCell( workSheet, workRow, workCell, row++, col+1 );
							workCell.setCellValue( s31ListBean.getStrKmkmei() );
							// 配点セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
							if ( !cm.toString(s31ListBean.getStrHaitenKmk()).equals("") ) {
								workCell.setCellValue( "（" + s31ListBean.getStrHaitenKmk() + "）" );
							}
						}
						if ( hyouCnt==0 ) {
							row = 7;
						}
						if ( hyouCnt==1 ) {
							row = 36;
						}
						if (col==0) {
							// 学校名セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col );
							workCell.setCellValue( s31Item.getStrGakkomei() );
						}
						// 人数セット
						ninzu = s31ClassListBean.getIntNinzu();
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						if ( s31ClassListBean.getIntNinzu() != -999 ) {
							workCell.setCellValue( s31ClassListBean.getIntNinzu() );
						}
						// 平均点セット
						heikin = s31ClassListBean.getFloHeikin();
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
						if ( s31ClassListBean.getFloHeikin() != -999.0 ) {
							workCell.setCellValue( s31ClassListBean.getFloHeikin() );
						}
						// 平均偏差値セット
						hensa = s31ClassListBean.getFloHensa();
						workCell = cm.setCell( workSheet, workRow, workCell, row++, col+4 );
						if ( s31ClassListBean.getFloHensa() != -999.0 ) {
							workCell.setCellValue( s31ClassListBean.getFloHensa() );
						}
					} else {
						if((classCnt-1)%((houjiNum-1)/2)==0 && classCnt!=1){
							if ((maxClass-1)<=(houjiNum-1)/2) {
								if((classCnt-1)%((houjiNum-1)/2)==0){
									row = 4;
								}else{
									row = 33;
								}
							}else{
								if((classCnt-1)%(houjiNum-1)==0){
									row = 4;
								}else{
									row = 33;
								}
							}

							if ( kmkCnt==0 || !cm.toString(kmk).equals(cm.toString(s31ListBean.getStrKmkCd())) ) {
								// 型・科目名セット
								workCell = cm.setCell( workSheet, workRow, workCell, row++, col+1 );
								workCell.setCellValue( s31ListBean.getStrKmkmei() );
								// 配点セット
								if ( !cm.toString(s31ListBean.getStrHaitenKmk()).equals("") ) {
									workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
									workCell.setCellValue( "（" + s31ListBean.getStrHaitenKmk() + "）" );
								}
							}
							if ((maxClass-1)<=(houjiNum-1)/2) {
								if((classCnt-1)%((houjiNum-1)/2)==0){
									row = 7;
								}else{
									row = 36;
								}
							}else{
								if((classCnt-1)%(houjiNum-1)==0){
									row = 7;
								}else{
									row = 36;
								}
							}
							if (col==0) {
								// 学校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, row, col );
								workCell.setCellValue( s31Item.getStrGakkomei() );
							}
							// 人数セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
							if ( ninzu != -999 ) {
								workCell.setCellValue( ninzu );
							}
							// 平均点セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
							if ( heikin != -999.0 ) {
								workCell.setCellValue( heikin );
							}
							// 平均偏差値セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col+4 );
							if ( hensa != -999.0 ) {
								workCell.setCellValue( hensa );
							}
							row++;
							hyouCnt = 1;
						}
						if (col==0) {
							// 学年+クラス名セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col );
							workCell.setCellValue( cm.toString(s31ClassListBean.getStrGrade())+"年 "+cm.toString(s31ClassListBean.getStrClass())+"クラス" );
						}
						// 人数セット
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						if ( s31ClassListBean.getIntNinzu() != -999 ) {
							workCell.setCellValue( s31ClassListBean.getIntNinzu() );
						}
						// 平均点セット
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
						if ( s31ClassListBean.getFloHeikin() != -999.0 ) {
							workCell.setCellValue( s31ClassListBean.getFloHeikin() );
						}
						// *セット
						if( hensa != -999.0 ){
							if ( s31ClassListBean.getFloHensa() != -999.0 ) {
								if ( s31ClassListBean.getFloHensa() < hensa ) {
									workCell = cm.setCell( workSheet, workRow, workCell, row, col+3 );
									workCell.setCellValue("*");
								}
							}
						}
						// 平均偏差値セット
						workCell = cm.setCell( workSheet, workRow, workCell, row++, col+4 );
						if ( s31ClassListBean.getFloHensa() != -999.0 ) {
							workCell.setCellValue( s31ClassListBean.getFloHensa() );
						}
					}
					classCnt++;
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
					if (maxClass>houjiNum){
						if((classCnt-1)%(houjiNum-1)==0 && classCnt!=1){
							//クラスが改シート、改ファイルでまたぐとき
							if(itrClass.hasNext()){
								if(kmkCnt==0){
									if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
										bolBookCngFlg = true;
									}
								}
								bolSheetCngFlg = true;
							}
						}
					}
//add end
				}
				classCnt = 0;
				kmkCnt++;
				col = col + 4;
				kmk = s31ListBean.getStrKmkCd();
				
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
				if (maxClass>houjiNum){
					if ( kmkCnt==10 ) {
						col = 0;
						kmkCnt = 0;
					}
					bolSheetCngFlg = true;
					if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
						bolBookCngFlg = true;
					}
					// workbookListにWorkBookが２つ以上格納されている時は出力処理を実行
					int listSize = workbookList.size();
					if(listSize>1){
						if(kmkCnt == 0){
							workbook = (HSSFWorkbook)workbookList.get(0);

							// Excelファイル保存
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intFileIndex, masterfile, intMaxSheetSr);
							intFileIndex++;
							if( bolRet == false ){
								return errfwrite;
							}

							// ファイル出力したデータは削除
							workbookList.remove(0);

							// WorkBook・変数を書き込んでる途中のものに戻す
							workbook = (HSSFWorkbook)workbookList.get(0);
						}
					}
				}
//add end
			}
			
			// Excelファイル保存
			boolean bolRet = false;
			if(intFileIndex==1){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intFileIndex, masterfile, intMaxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("S31_01","データセットエラー",e.toString());
			return errfdata;
		}
		
		return noerror;
	}

}
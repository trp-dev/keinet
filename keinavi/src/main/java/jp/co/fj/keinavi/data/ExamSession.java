/*
 * 作成日: 2004/07/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 模試セッション
 * 
 * @author kawai
 */
public class ExamSession implements Serializable {

	// セッションキー
	public static final String SESSION_KEY = "ExamSession";

	private String[] years; // 年度の配列
	private Map examMap = new HashMap(); // 年度をキーとして模試データのリスト保持するマップ
	private Set publicExamSet = new HashSet(); // 今年度の公開済み模試セット

	/**
	 * 年度と模試コードから模試データを取得する
	 * @param examYear
	 * @param examCD
	 * @return
	 */
	public ExamData getExamData(String examYear, String examCD) {
		ExamData data = null;
		if (examYear != null && examCD != null) {
			if (examMap.containsKey(examYear)) {
				List list = (List)examMap.get(examYear);
				int index = list.indexOf(new ExamData(examYear, examCD));
				if (index >= 0) data = (ExamData)list.get(index);
			}
		}
		return data;
	}

	/**
	 * 指定した年度の模試リストを取得する
	 * @param year
	 * @return
	 */
	public List getExamList(String year) {
		return (List) this.getExamMap().get(year);
	}

	/**
	 * @return 模試リスト
	 */
	public List getExamList() {
		
		final List list = new ArrayList();

		for (int i = 0; i < years.length; i++) {
			list.addAll((List) examMap.get(years[i]));			
		}
		
		return list;
	}
	
	/**
	 * T.Yamada　追加
	 * 模試年度と模試コードで限定する付加条件SQL文を返す
	 * @return
	 */
	public String getExamOptionQuery(){
		String sqlOption = "";
		int count = 0;
		Set setty = this.getExamMap().entrySet();
		Iterator ite = setty.iterator();
		if(setty.size() > 0)
			sqlOption += " AND (";
		while(ite.hasNext()){
			java.util.Map.Entry me = (java.util.Map.Entry)ite.next();
			List examList = (List)me.getValue();
			ExamData[] eds = (ExamData[])examList.toArray(new ExamData[0]);
			for(int i=0; i<eds.length; i++){
				ExamData ed = eds[i];
				if(count > 0)
					sqlOption += " OR";
				sqlOption += " (E.EXAMYEAR = '"+ ed.getExamYear()+"' AND E.EXAMCD = '"+ ed.getExamCD()+"' )";
				count ++;
			}
		}
		if(setty.size() > 0)
			sqlOption += ")";
		return sqlOption;
	}
	
	/**
	 * @return
	 */
	public Map getExamMap() {
		return examMap;
	}

	/**
	 * @return
	 */
	public String[] getYears() {
		return years;
	}

	/**
	 * @param map
	 */
	public void setExamMap(Map map) {
		examMap = map;
	}

	/**
	 * @param strings
	 */
	public void setYears(String[] strings) {
		years = strings;
	}

	/**
	 * @return
	 */
	public Set getPublicExamSet() {
		return publicExamSet;
	}

	/**
	 * @param set
	 */
	public void setPublicExamSet(Set set) {
		publicExamSet = set;
	}

}

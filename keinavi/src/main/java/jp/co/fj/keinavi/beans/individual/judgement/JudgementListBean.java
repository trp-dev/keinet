package jp.co.fj.keinavi.beans.individual.judgement;

import jp.co.fj.kawaijuku.judgement.beans.JudgedResult;
import jp.co.fj.kawaijuku.judgement.beans.JudgedResultList;
import jp.co.fj.kawaijuku.judgement.beans.judge.Judgement;

/**
 * 
 * Kei-Navi用判定結果一覧作成クラス
 * 
 * 
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 *              マスタデータの持ち方変更
 *
 * @author Naoko FUJIA - 富士通北陸システムズ
 * @version 1.0
 *
 *
 */
public class JudgementListBean extends JudgedResultList {
	
	public static final String SESSION_KEY = "useListBean";

	/**
	 * JudgemnetBeanをDetailPageBeanに変換する
	 * @param j
	 * @return
	 */
	public JudgedResult wrap(Judgement j) {
		return new DetailPageBean(j);	    
	}

}

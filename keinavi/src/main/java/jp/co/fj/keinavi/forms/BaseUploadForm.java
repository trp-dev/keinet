package jp.co.fj.keinavi.forms;

/**
 *
 * ファイルアップロード画面の基本アクションフォーム
 * 
 * 2005.11.28	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
abstract public class BaseUploadForm extends BaseForm {

	// ファイル本体
	// <input type="file" name="file" > とする。
	private byte[] file;
	// ファイル名
	private String fileName;
	
	/**
	 * @return file を戻します。
	 */
	public byte[] getFile() {
		return file;
	}
	/**
	 * @param pFile 設定する file。
	 */
	public void setFile(final byte[] pFile) {
		this.file = pFile;
	}
	/**
	 * @return fileName を戻します。
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param pFileName 設定する fileName。
	 */
	public void setFileName(final String pFileName) {
		this.fileName = pFileName;
	}

}

package jp.co.fj.keinavi.util.taglib.list;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 * 指定されたサイズまでListに要素詰めるTaglib
 * 
 * @author kawai
 */
public class ListPadTag extends BodyTagSupport {
	
	// 変数名
	private String var = null;
	// size
	private String size = null;
	
	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	public int doStartTag() throws JspException {
		return (EVAL_BODY_BUFFERED);
	}
	
	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.IterationTag#doAfterBody()
	 */
	public int doAfterBody() throws JspException {
		return SKIP_BODY;
	}
	
	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {
		List container = (List) pageContext.getAttribute(getVar());
		if (bodyContent != null) {
			try {
				StringBuffer buff = new StringBuffer();
				BufferedReader reader = new BufferedReader(bodyContent.getReader());
				String line = null;
				while ((line = reader.readLine()) != null) {
					buff.append(line);
				}
				
				int max = Integer.parseInt(size);	
				
				for (int i=container.size(); i<max; i++) {
					container.add(buff.toString());
				}

			} catch (IOException e) {
				throw new JspException(e);
			}
		}
		return EVAL_PAGE;
	}

	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.BodyTag#doInitBody()
	 */
	public void doInitBody() throws JspException {
		super.doInitBody();
	}

	public String getVar() {
		return this.var;
	}
	public void setVar(String var) {
		this.var = var;
	}
	public String getSize() {
		return this.size;
	}
	public void setSize(String size) {
		this.size = size;
	}
}

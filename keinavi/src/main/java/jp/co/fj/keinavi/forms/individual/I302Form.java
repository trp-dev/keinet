package jp.co.fj.keinavi.forms.individual;

import org.apache.commons.lang.StringUtils;

/**
 *
 * 受験予定大学一覧画面 FormBean
 * 
 * 2004.08.01	[新規作成]
 * 
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class I302Form extends IOnJudgeForm {

	private String targetPersonId;

	private String button;

	private String[] univValue; // チェックボックス

	private String[] month;

	private String[] date;

	private String[] provEntExamSite; // テキスト

	private String univValue4Detail; // シングル(削除用)

	private String errorMessage;// エラーメッセージ

	// 個人成績分析共通-印刷対象生徒（判定対象生徒)
	private String printStudent;

	// セキュリティスタンプ
	private String printStamp;

	// 印刷ボタン押下時パラメータ
	private String printStatus;

	// 出力フォーマット
	private String textFormat[];
	
	// 帳票種類
	private String textFileFormat;

	/**
	 * @see jp.co.fj.keinavi.forms.individual.IOnJudgeForm#validate()
	 */
	public void validate() {}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @return
	 */
	public String[] getUnivValue() {
		return univValue;
	}

	/**
	 * @param strings
	 */
	public void setUnivValue(String[] strings) {
		univValue = strings;
	}

	/**
	 * @return
	 */
	public String[] getProvEntExamSite() {
		return provEntExamSite;
	}

	/**
	 * @param strings
	 */
	public void setProvEntExamSite(String[] strings) {
		provEntExamSite = strings;
	}

	/**
	 * @return
	 */
	public String getUnivValue4Detail() {
		return univValue4Detail;
	}

	/**
	 * @param string
	 */
	public void setUnivValue4Detail(String string) {
		univValue4Detail = string;
	}

	/**
	 * @return
	 */
	public String getButton() {
		return button;
	}

	/**
	 * @param string
	 */
	public void setButton(String string) {
		button = string;
	}

	/**
	 * @return
	 */
	public String getPrintStamp() {
		return printStamp;
	}

	/**
	 * @param string
	 */
	public void setPrintStamp(String string) {
		printStamp = string;
	}

	/**
	 * @return
	 */
	public String getPrintStudent() {
		return printStudent;
	}

	/**
	 * @param string
	 */
	public void setPrintStudent(String string) {
		printStudent = string;
	}

	/**
	 * @return
	 */
	public String[] getTextFormat() {
		return textFormat;
	}

	/**
	 * @param strings
	 */
	public void setTextFormat(String[] strings) {
		textFormat = strings;
	}

	/**
	 * @return
	 */
	public String getPrintStatus() {
		return printStatus;
	}

	/**
	 * @param string
	 */
	public void setPrintStatus(String string) {
		printStatus = string;
	}

	/**
	 * @return
	 */
	public String[] getDate() {
		return date;
	}

	/**
	 * @return
	 */
	public String[] getMonth() {
		return month;
	}

	/**
	 * @param strings
	 */
	public void setDate(String[] strings) {
		date = strings;
	}

	/**
	 * @param strings
	 */
	public void setMonth(String[] strings) {
		month = strings;
	}

	/**
	 * @return
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param string
	 */
	public void setErrorMessage(String string) {
		errorMessage = string;
	}

	/**
	 * @return textFileFormat
	 */
	public String getTextFileFormat() {
		
		// デフォルトは「志望校判定」
		if (StringUtils.isEmpty(textFileFormat)) {
			return "2";
		} else {
			return textFileFormat;
		}
	}

	/**
	 * @param pTextFileFormat 設定する textFileFormat
	 */
	public void setTextFileFormat(String pTextFileFormat) {
		textFileFormat = pTextFileFormat;
	}

}

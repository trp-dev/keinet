/**
 * 校内成績分析−過年度比較　偏差値帯別度数分布(構成比)
 * 	Excelファイル編集
 * 作成日: 2004/07/29
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S22BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S22Item;
import jp.co.fj.keinavi.excel.data.school.S22ListBean;
import jp.co.fj.keinavi.excel.data.school.S22TotalListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S22_04 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	final private int	intMaxSheetSr	= 40;			// 最大シート数	
	final private String	masterfile0		= "S22_04";		// ファイル名
	final private String	masterfile1		= "NS22_04";	// ファイル名
	private String	masterfile		= "";					// ファイル名


/*
 * 	Excel編集メイン
 * 		S22Item s22Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s22_04EditExcel(S22Item s22Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("S22_04","S22_04帳票作成開始","");
		
		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		//テンプレートの決定
		if (s22Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			int		col				= 0;	// 列
			int 		setCol			= -1;	// *セット用
			float 		kouseihi		= 0;	// *作成用
			int 		maxNo			= 19;	// 最大表示件数
			int 		maxNen			= 5;	// 最大表示年度数
			int		nendoCnt		= 0;	// 値範囲：０〜４　(年度カウンター)
			int		maxSheetIndex	= 0;	// シートカウンター
			int		intBookCngCount	= 0;	// ブックカウンター
			String		nendo			= "";	// 年度保持
			boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
//add 2004/10/26 T.Sakai データ0件対応
			int		dispKmkFlgCnt		= 0;		// グラフ表示フラグカウンタ
//add end

			// データセット
			ArrayList	s22List			= s22Item.getS22List();
			Iterator	itr				= s22List.iterator();

			// 基本ファイルを読込む
			S22ListBean s22ListBean = new S22ListBean();

			while( itr.hasNext() ) {
				s22ListBean = (S22ListBean)itr.next();
				if ( s22ListBean.getIntDispKmkFlg()==1 ) {
//add 2004/10/26 T.Sakai データ0件対応
					dispKmkFlgCnt++;
//add end
					// 基本ファイルを読み込む
					S22BnpListBean		s22BnpListBean		= new S22BnpListBean();
					S22TotalListBean	s22TotalListBean	= new S22TotalListBean();
	
					//データの保持
					ArrayList	s22BnpList		= s22ListBean.getS22BnpList();
					ArrayList	s22TotalList	= s22ListBean.getS22TotalList();
					Iterator	itrS22Bnp		= s22BnpList.iterator();
					Iterator	itrS22Total		= s22TotalList.iterator();
	
					
					if( bolBookCngFlg == true ){
						// マスタExcel読み込み
						workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
						if( workbook==null ){
							return errfread;
						}
						bolBookCngFlg	= false;
						maxSheetIndex	= 0;
					}
	
					// データセットするシートの選択
					workSheet = workbook.getSheet( Integer.toString(maxSheetIndex+1) );
					maxSheetIndex++;
	
					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);
	
					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, s22Item.getIntSecuFlg() ,41 ,42 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 41 );
					workCell.setCellValue(secuFlg);
	
					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
					workCell.setCellValue( "学校名　：" + cm.toString(s22Item.getStrGakkomei()) );
	
					// 対象模試セット
					String moshi =cm.setTaisyouMoshi( s22Item.getStrMshDate() );	// 模試月取得
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s22Item.getStrMshmei()) + moshi);
	
					// 型名・配点セット
					String haiten = "";
					if ( !cm.toString(s22ListBean.getStrHaitenKmk()).equals("") ) {
						haiten = "（" + s22ListBean.getStrHaitenKmk() + "）";
					}
					workCell = cm.setCell( workSheet, workRow, workCell, 5, 0 );
					workCell.setCellValue( "型・科目：" + cm.toString(s22ListBean.getStrKmkmei()) + haiten );
					
//add 2004/10/26 T.Sakai スペース対応
  					if (s22Item.getIntShubetsuFlg() == 1){
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
						workCell.setCellValue( "〜 19" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
						workCell.setCellValue( " 20〜\n 29" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
						workCell.setCellValue( " 30〜\n 39" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
						workCell.setCellValue( " 40〜\n 49" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
						workCell.setCellValue( " 50〜\n 59" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
						workCell.setCellValue( " 60〜\n 69" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
						workCell.setCellValue( " 70〜\n 79" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
						workCell.setCellValue( " 80〜\n 89" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
						workCell.setCellValue( " 90〜\n 99" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
						workCell.setCellValue( "100〜\n109" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
						workCell.setCellValue( "110〜\n119" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
						workCell.setCellValue( "120〜\n129" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
						workCell.setCellValue( "130〜\n139" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
						workCell.setCellValue( "140〜\n149" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
						workCell.setCellValue( "150〜\n159" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
						workCell.setCellValue( "160〜\n169" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
						workCell.setCellValue( "170〜\n179" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
						workCell.setCellValue( "180〜\n189" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
						workCell.setCellValue( "190〜" );
  					} else{
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
						workCell.setCellValue( "〜32.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
						workCell.setCellValue( "32.5〜\n34.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
						workCell.setCellValue( "35.0〜\n37.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
						workCell.setCellValue( "37.5〜\n39.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
						workCell.setCellValue( "40.0〜\n42.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
						workCell.setCellValue( "42.5〜\n44.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
						workCell.setCellValue( "45.0〜\n47.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
						workCell.setCellValue( "47.5〜\n49.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
						workCell.setCellValue( "50.0〜\n52.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
						workCell.setCellValue( "52.5〜\n54.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
						workCell.setCellValue( "55.0〜\n57.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
						workCell.setCellValue( "57.5〜\n59.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
						workCell.setCellValue( "60.0〜\n62.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
						workCell.setCellValue( "62.5〜\n64.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
						workCell.setCellValue( "65.0〜\n67.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
						workCell.setCellValue( "67.5〜\n69.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
						workCell.setCellValue( "70.0〜\n72.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
						workCell.setCellValue( "72.5〜\n74.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
						workCell.setCellValue( "75.0〜" );
  					}
//add end
					// 偏差値データセット
					col	= 0;
					nendoCnt = 0;
					while(itrS22Bnp.hasNext()){
						s22BnpListBean	= (S22BnpListBean) itrS22Bnp.next();
	
						// 年度切り替え処理
						if(col != 0){
							if( !cm.toString(nendo).equals(cm.toString(s22BnpListBean.getStrNendo())) ){
								col=0;
								nendoCnt++;
								if(nendoCnt >= maxNen){
									break;
								}
							}
						}
	
						// 年度セット
						if(col == 0){
							if ( !cm.toString(s22BnpListBean.getStrNendo()).equals("") ) {
								workCell = cm.setCell( workSheet, workRow, workCell, 51+nendoCnt, 1 );
								workCell.setCellValue( s22BnpListBean.getStrNendo() +"年度" );
								workCell = cm.setCell( workSheet, workRow, workCell, 56+nendoCnt, 1 );
								workCell.setCellValue( s22BnpListBean.getStrNendo() +"年度" );
							}
							nendo	= s22BnpListBean.getStrNendo();
						}
	
						// 構成比セット
						if ( s22BnpListBean.getFloKoseihi() != -999.0 ) {
							workCell = cm.setCell( workSheet, workRow, workCell, 51+nendoCnt, 39-2*col);
							workCell.setCellValue( s22BnpListBean.getFloKoseihi() );
	
							// *セット準備
							if( s22BnpListBean.getFloKoseihi() > kouseihi ){
								kouseihi = s22BnpListBean.getFloKoseihi();
								setCol = col;
							}
						}
	
						// 人数セット
						if ( s22BnpListBean.getIntNinzu() != -999 ) {
							workCell = cm.setCell( workSheet, workRow, workCell,56+nendoCnt, 39-2*col);
							workCell.setCellValue( s22BnpListBean.getIntNinzu() );
						}
	
						col++;
	
						// *セット
						if(col >=maxNo){
							if(setCol != -1){
								workCell = cm.setCell( workSheet, workRow, workCell,51+nendoCnt, 38-2*setCol );
								workCell.setCellValue("*");
							}
							kouseihi=0;
							setCol=-1;
						}
					}
	
					//　平均データセット
					nendoCnt	= 0;
					while(itrS22Total.hasNext()){
						s22TotalListBean	= (S22TotalListBean) itrS22Total.next();

						// 合計人数セット
						if ( s22TotalListBean.getIntTotalNinzu() != -999 ) {
							workCell = cm.setCell( workSheet, workRow, workCell,51+nendoCnt, 40 );
							workCell.setCellValue( s22TotalListBean.getIntTotalNinzu() );
						}
	
						// 平均点セット
						if ( s22TotalListBean.getFloHeikin() != -999.0 ) {
							workCell = cm.setCell( workSheet, workRow, workCell,51+nendoCnt, 41 );
							workCell.setCellValue( s22TotalListBean.getFloHeikin() );
						}
	
						// 平均偏差値セット
						if ( s22TotalListBean.getFloHensa() != -999.0 ) {
							workCell = cm.setCell( workSheet, workRow, workCell,51+nendoCnt, 42 );
							workCell.setCellValue( s22TotalListBean.getFloHensa()); 
						}
	
						nendoCnt++;
						if(nendoCnt>=maxNen){
							break;
						}
					}
					
					if(maxSheetIndex >= intMaxSheetSr){
						bolBookCngFlg = true;
					}
					
					if( bolBookCngFlg == true){
						boolean bolRet = false;
						// Excelファイル保存
						if(intBookCngCount==0){
							if(itr.hasNext() == true){
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
							}
							else{
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
							}
						}else{
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
						}
	
						if( bolRet == false ){
							return errfwrite;
						}
						intBookCngCount++;
					}
				}
			}
//add 2004/10/26 T.Sakai データ0件対応
			if ( s22List.size()==0 || dispKmkFlgCnt==0 ) {
				// マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				bolBookCngFlg	= false;
				maxSheetIndex	= 0;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet( Integer.toString(maxSheetIndex+1) );
				maxSheetIndex++;
				
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s22Item.getIntSecuFlg() ,41 ,42 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 41 );
				workCell.setCellValue(secuFlg);
				
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s22Item.getStrGakkomei()) );
				
				// 対象模試セット
				String moshi =cm.setTaisyouMoshi( s22Item.getStrMshDate() );	// 模試月取得
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s22Item.getStrMshmei()) + moshi);
				
//add 2004/10/26 T.Sakai スペース対応
  				if (s22Item.getIntShubetsuFlg() == 1){
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
					workCell.setCellValue( "〜 19" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
					workCell.setCellValue( " 20〜\n 29" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
					workCell.setCellValue( " 30〜\n 39" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
					workCell.setCellValue( " 40〜\n 49" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
					workCell.setCellValue( " 50〜\n 59" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
					workCell.setCellValue( " 60〜\n 69" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
					workCell.setCellValue( " 70〜\n 79" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
					workCell.setCellValue( " 80〜\n 89" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
					workCell.setCellValue( " 90〜\n 99" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
					workCell.setCellValue( "100〜\n109" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
					workCell.setCellValue( "110〜\n119" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
					workCell.setCellValue( "120〜\n129" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
					workCell.setCellValue( "130〜\n139" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
					workCell.setCellValue( "140〜\n149" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
					workCell.setCellValue( "150〜\n159" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
					workCell.setCellValue( "160〜\n169" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
					workCell.setCellValue( "170〜\n179" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
					workCell.setCellValue( "180〜\n189" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
					workCell.setCellValue( "190〜" );
  				} else{
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
					workCell.setCellValue( "〜32.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
					workCell.setCellValue( "32.5〜\n34.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
					workCell.setCellValue( "35.0〜\n37.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
					workCell.setCellValue( "37.5〜\n39.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
					workCell.setCellValue( "40.0〜\n42.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
					workCell.setCellValue( "42.5〜\n44.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
					workCell.setCellValue( "45.0〜\n47.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
					workCell.setCellValue( "47.5〜\n49.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
					workCell.setCellValue( "50.0〜\n52.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
					workCell.setCellValue( "52.5〜\n54.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
					workCell.setCellValue( "55.0〜\n57.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
					workCell.setCellValue( "57.5〜\n59.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
					workCell.setCellValue( "60.0〜\n62.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
					workCell.setCellValue( "62.5〜\n64.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
					workCell.setCellValue( "65.0〜\n67.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
					workCell.setCellValue( "67.5〜\n69.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
					workCell.setCellValue( "70.0〜\n72.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
					workCell.setCellValue( "72.5〜\n74.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
					workCell.setCellValue( "75.0〜" );
  				}
//add end
			}
//add end
			if( bolBookCngFlg == false){
				boolean bolRet = false;
				// Excelファイル保存
				if(intBookCngCount!=0){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}
			
		} catch(Exception e) {
			log.Err("S22_04","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S22_04","S22_04帳票作成終了","");
		return noerror;
	}

}
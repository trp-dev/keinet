package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 個人受験模試リストBean
 * 
 * 2006.10.10	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class MTakenExamListBean extends DefaultBean {

	private final String schoolCd;
	private final List takenExamList = new LinkedList();
	
	// 個人ID
	private String targetIndividualId;
	// ヘルプデスクかどうか
	private boolean isHelpDesk;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 学校コード
	 */
	public MTakenExamListBean(final String pSchoolCd) {
		schoolCd = pSchoolCd;
	}
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final Query query = QueryLoader.getInstance().load("m24");
			// ヘルプデスクなら内部データ開放日で評価する
			if (isHelpDesk) {
				query.replaceAll("out_dataopendate", "in_dataopendate");
			}
			ps = conn.prepareStatement(query.toString());
			ps.setString(1, targetIndividualId);
			ps.setString(2, schoolCd);
			rs = ps.executeQuery();
			while (rs.next()) {
				final ExamData data = new ExamData();
				data.setExamYear(rs.getString(1));
				data.setExamCD(rs.getString(2));
				data.setExamName(rs.getString(3));
				takenExamList.add(data);
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * @return takenExamList を戻します。
	 */
	public List getTakenExamList() {
		return takenExamList;
	}

	/**
	 * @param pTargetIndividualId 設定する targetIndividualId。
	 */
	public void setTargetIndividualId(final String pTargetIndividualId) {
		targetIndividualId = pTargetIndividualId;
	}

	/**
	 * @param pIsHelpDesk 設定する isHelpDesk。
	 */
	public void setHelpDesk(final boolean pIsHelpDesk) {
		isHelpDesk = pIsHelpDesk;
	}
	
}

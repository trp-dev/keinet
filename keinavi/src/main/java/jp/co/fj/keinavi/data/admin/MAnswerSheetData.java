/*
 * 作成日: 2004/10/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.admin;

import java.io.Serializable;

/**
 * 受験届データ
 * 
 * @author kawai
 */
public class MAnswerSheetData implements Serializable {

	String answerSheetNo; // 解答用紙番号

	//（前年度）生徒情報
	String year1; // 年度
	String grade1; // 学年
	String className1; // クラス
	String classNo1; // クラス番号

	//（今年度）生徒情報
	String year2; // 年度
	String grade2; // 学年
	String className2; // クラス
	String classNo2; // クラス番号
	String sex2; // 性別
	String nameKana2; // かな氏名
	String nameKanji2; // 漢字氏名
	String birthday2; // 誕生日
	String telNo2; // 電話番号

	// 受験届情報
	String year3; // 年度
	String grade3; // 学年
	String className3; // クラス
	String classNo3; // クラス番号
	String sex3; // 性別
	String nameKana3; // かな氏名
	String nameKanji3; // 漢字氏名
	String birthday3; // 誕生日
	String telNo3; // 電話番号
	String sIndividualid; // 学校用個人ID

	/**
	 * 受験届と生徒情報が一致するかどうか
	 * @return
	 */
	public boolean isIdentical() {
		return this.equal(this.grade3, this.grade2)
			&& this.equal(this.className3, this.className2)
			&& this.equal(this.classNo3, this.classNo2)
			&& this.equal(this.sex3, this.sex2)
			&& this.equal(this.nameKana3, this.nameKana2)
			&& this.equal(this.birthday3, this.birthday2)
			&& this.equal(this.telNo3, this.telNo2);
	}

	/**
	 * ２つの文字列を比較する
	 * NULL同士ならtrue
	 * @param s1
	 * @param s2
	 * @return
	 */
	private boolean equal(String s1, String s2) {
		if (s1 == null) {
			return s2 == null;			
		} else {
			return s1.equals(s2);
		}
	}

	/**
	 * @return
	 */
	public String getAnswerSheetNo() {
		return answerSheetNo;
	}

	/**
	 * @return
	 */
	public String getBirthday2() {
		return birthday2;
	}

	/**
	 * @return
	 */
	public String getBirthday3() {
		return birthday3;
	}

	/**
	 * @return
	 */
	public String getClassName1() {
		return className1;
	}

	/**
	 * @return
	 */
	public String getClassName2() {
		return className2;
	}

	/**
	 * @return
	 */
	public String getClassName3() {
		return className3;
	}

	/**
	 * @return
	 */
	public String getClassNo1() {
		return classNo1;
	}

	/**
	 * @return
	 */
	public String getClassNo2() {
		return classNo2;
	}

	/**
	 * @return
	 */
	public String getClassNo3() {
		return classNo3;
	}

	/**
	 * @return
	 */
	public String getGrade1() {
		return grade1;
	}

	/**
	 * @return
	 */
	public String getGrade2() {
		return grade2;
	}

	/**
	 * @return
	 */
	public String getGrade3() {
		return grade3;
	}

	/**
	 * @return
	 */
	public String getNameKana2() {
		return nameKana2;
	}

	/**
	 * @return
	 */
	public String getNameKana3() {
		return nameKana3;
	}

	/**
	 * @return
	 */
	public String getNameKanji3() {
		return nameKanji3;
	}

	/**
	 * @return
	 */
	public String getSex2() {
		return sex2;
	}

	/**
	 * @return
	 */
	public String getSex3() {
		return sex3;
	}

	/**
	 * @return
	 */
	public String getTelNo2() {
		return telNo2;
	}

	/**
	 * @return
	 */
	public String getTelNo3() {
		return telNo3;
	}

	/**
	 * @return
	 */
	public String getYear1() {
		return year1;
	}

	/**
	 * @return
	 */
	public String getYear2() {
		return year2;
	}

	/**
	 * @return
	 */
	public String getYear3() {
		return year3;
	}

	/**
	 * @param string
	 */
	public void setAnswerSheetNo(String string) {
		answerSheetNo = string;
	}

	/**
	 * @param string
	 */
	public void setBirthday2(String string) {
		birthday2 = string;
	}

	/**
	 * @param string
	 */
	public void setBirthday3(String string) {
		birthday3 = string;
	}

	/**
	 * @param string
	 */
	public void setClassName1(String string) {
		className1 = string;
	}

	/**
	 * @param string
	 */
	public void setClassName2(String string) {
		className2 = string;
	}

	/**
	 * @param string
	 */
	public void setClassName3(String string) {
		className3 = string;
	}

	/**
	 * @param string
	 */
	public void setClassNo1(String string) {
		classNo1 = string;
	}

	/**
	 * @param string
	 */
	public void setClassNo2(String string) {
		classNo2 = string;
	}

	/**
	 * @param string
	 */
	public void setClassNo3(String string) {
		classNo3 = string;
	}

	/**
	 * @param string
	 */
	public void setGrade1(String string) {
		grade1 = string;
	}

	/**
	 * @param string
	 */
	public void setGrade2(String string) {
		grade2 = string;
	}

	/**
	 * @param string
	 */
	public void setGrade3(String string) {
		grade3 = string;
	}

	/**
	 * @param string
	 */
	public void setNameKana2(String string) {
		nameKana2 = string;
	}

	/**
	 * @param string
	 */
	public void setNameKana3(String string) {
		nameKana3 = string;
	}

	/**
	 * @param string
	 */
	public void setNameKanji3(String string) {
		nameKanji3 = string;
	}

	/**
	 * @param string
	 */
	public void setSex2(String string) {
		sex2 = string;
	}

	/**
	 * @param string
	 */
	public void setSex3(String string) {
		sex3 = string;
	}

	/**
	 * @param string
	 */
	public void setTelNo2(String string) {
		telNo2 = string;
	}

	/**
	 * @param string
	 */
	public void setTelNo3(String string) {
		telNo3 = string;
	}

	/**
	 * @param string
	 */
	public void setYear1(String string) {
		year1 = string;
	}

	/**
	 * @param string
	 */
	public void setYear2(String string) {
		year2 = string;
	}

	/**
	 * @param string
	 */
	public void setYear3(String string) {
		year3 = string;
	}

	/**
	 * @return
	 */
	public String getNameKanji2() {
		return nameKanji2;
	}

	/**
	 * @param string
	 */
	public void setNameKanji2(String string) {
		nameKanji2 = string;
	}

	/**
	 * @return
	 */
	public String getSIndividualid() {
		return sIndividualid;
	}

	/**
	 * @param string
	 */
	public void setSIndividualid(String string) {
		sIndividualid = string;
	}

}

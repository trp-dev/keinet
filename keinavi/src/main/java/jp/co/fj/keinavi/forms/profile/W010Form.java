/*
 * 作成日: 2004/06/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.profile;

import com.fjh.forms.ActionForm;


/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class W010Form extends ActionForm {

	private String profileID; // プロファイルID
	private String profileName; // プロファイル名称
	private String comment; // コメント
	private String current; // カレントフォルダ
	private String countingDivCode; // 集計区分コード
	private String pub; // 公開するかどうか

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	/**
	 * @return
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @return
	 */
	public String getCountingDivCode() {
		return countingDivCode;
	}

	/**
	 * @return
	 */
	public String getCurrent() {
		return current;
	}

	/**
	 * @return
	 */
	public String getProfileID() {
		return profileID;
	}

	/**
	 * @return
	 */
	public String getProfileName() {
		return profileName;
	}

	/**
	 * @param string
	 */
	public void setComment(String string) {
		comment = string;
	}

	/**
	 * @param string
	 */
	public void setCountingDivCode(String string) {
		countingDivCode = string;
	}

	/**
	 * @param string
	 */
	public void setCurrent(String string) {
		current = string;
	}

	/**
	 * @param string
	 */
	public void setProfileID(String string) {
		profileID = string;
	}

	/**
	 * @param string
	 */
	public void setProfileName(String string) {
		profileName = string;
	}

	/**
	 * @return
	 */
	public String getPub() {
		return pub;
	}

	/**
	 * @param string
	 */
	public void setPub(String string) {
		pub = string;
	}

}

/**
 * 校内成績分析−過回比較　成績概況
 * 	Excelファイル編集
 * 作成日: 2004/08/18
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S51Item;
import jp.co.fj.keinavi.excel.data.school.S51KakaiListBean;
import jp.co.fj.keinavi.excel.data.school.S51ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S51_01 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー
	
	private CM		cm			= new CM();	//共通関数用クラス インスタンス
	
	final private String	masterfile0	= "S51_01";						// ファイル名
	final private String	masterfile1	= "NS51_01";					// ファイル名
	private String	masterfile	= "";									// ファイル名
//	final private String	strArea		= "A1:AO50";					// 印刷範囲
	final private int[]	tabRow		= {4,16,28,40};					// 表の基準点
	final private int[]	tabCol		= {1,5,9,13,17,21,25,29,33,37};	// 表の基準点


/*
 * 	Excel編集メイン
 * 		S51Item s51Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
	public int s51_01EditExcel(S51Item s51Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		HSSFWorkbook		workbook		= null;
		HSSFSheet			workSheet		= null;
		HSSFRow				workRow			= null;
		HSSFCell			workCell		= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s51Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S51ListBean s51ListBean = new S51ListBean();
	
		try {
			int		row					= 0;		// 行
			int		intMaxSheetIndex	= 0;		// シートカウンター
			float		hensa				= 0;		// 最新模試の偏差値保持
			int		kmkCd				= 0;		// 今年度の科目コード保持
			int		hyouCnt				= 0;		// 値範囲：０〜５(表カウンター)
			int		kmkCnt				= 0;		// 値範囲：０〜９(型・科目カウンター)
			boolean	kataFlg				= true;	// false:改行済(型・科目名変更済)　
			boolean	bolSheetCngFlg		= true;	// 改シートフラグ

			//マスタExcel読み込み
			workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
			if( workbook==null ){
				return errfread;
			}
			
			// データセット
			ArrayList	s51List	= s51Item.getS51List();
			Iterator	itr		= s51List.iterator();
			
			while( itr.hasNext() ) {
				s51ListBean = (S51ListBean)itr.next();
				
				//項目変更
				if(( row > 0 )&&( row < 7 )){
					if(kmkCd != Integer.parseInt(s51ListBean.getStrKmkCd())){
						//下の改項目・改表処理を通るようにする
						row = 7;
					}
				}
				if( row >= 7 ){
					row = 0;
					kmkCnt++;
					//改表
					if( kmkCnt >= 10 ){
						kmkCnt = 0;
						hyouCnt++;
					}
				}

				//型から科目に変わる時のチェック
				if ( s51ListBean.getStrKmkCd().compareTo("7000") < 0 ) {
					if( kataFlg ){
						if( kmkCnt > 0 ){
							kmkCnt = 0;
							hyouCnt++;
						}
					}
					kataFlg	= false;
				}
				
				//改ファイルチェック
				if ( hyouCnt >= 4 ) {
					bolSheetCngFlg = true;
					hyouCnt = 0;
				}
				
				if(bolSheetCngFlg){
					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);
					intMaxSheetIndex++;
		
					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);
		
					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, s51Item.getIntSecuFlg() ,38 ,40 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 38 );
					workCell.setCellValue(secuFlg);
		
					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( "学校名　：" + cm.toString(s51Item.getStrGakkomei()) );
		
			
					// 対象模試セット
					String moshi =cm.setTaisyouMoshi( s51Item.getStrMshDate() );	// 模試月取得
					workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
					workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s51Item.getStrMshmei()) + moshi);
					bolSheetCngFlg = false;
				}
				
				if( row == 0 ){
					// 型・科目名セット
					workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt], tabCol[kmkCnt] );
					workCell.setCellValue( s51ListBean.getStrKmkmei() );

					// 配点セット
					if ( !cm.toString(s51ListBean.getStrHaitenKmk()).equals("") ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+1, tabCol[kmkCnt] );
						workCell.setCellValue("(" + s51ListBean.getStrHaitenKmk() + ")" );
					}
				}
				
				// 基本ファイルを読込む
				S51KakaiListBean s51KakaiListBean = new S51KakaiListBean();
				
				// 過回データセット
				ArrayList s51KakaiList = s51ListBean.getS51KakaiList();
				Iterator itrKakai = s51KakaiList.iterator();
				
				hensa = 0;
				
				while ( itrKakai.hasNext() ){
					s51KakaiListBean = (S51KakaiListBean)itrKakai.next();
					
					if(kmkCnt == 0){
						// 模試名セット
						String moshi =cm.setTaisyouMoshi( s51KakaiListBean.getStrMshDate() );	// 模試月取得
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, 0 );
						workCell.setCellValue( cm.toString(s51KakaiListBean.getStrMshmei()) + moshi );
					}
					
					if( row == 0 ){
						//最新模試の偏差値保持
						hensa = s51KakaiListBean.getFloHensa();
					}
					
					// 人数セット
					if ( s51KakaiListBean.getIntNinzu() != -999 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[kmkCnt] );
						workCell.setCellValue( s51KakaiListBean.getIntNinzu() );
					}

					// 平均点セット
					if ( s51KakaiListBean.getFloHeikin() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[kmkCnt]+1 );
						workCell.setCellValue( s51KakaiListBean.getFloHeikin() );
					}

					// 平均偏差値セット
					if ( s51KakaiListBean.getFloHensa() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[kmkCnt]+3 );
						workCell.setCellValue( s51KakaiListBean.getFloHensa() );
					}

					// *セット
					if (s51KakaiListBean.getFloHensa() != -999.0) {
						if ( hensa < s51KakaiListBean.getFloHensa() ) {
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[kmkCnt]+2 );
							workCell.setCellValue("*");
						}
					}
					
					row++;
				}
			}
	
			// Excelファイル保存
			boolean bolRet = false;
			bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);

			if( bolRet == false ){
				return errfwrite;					
			}
		
		} catch(Exception e) {
			log.Err("S51_01","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

}

package jp.co.fj.keinavi.beans.sheet.common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.sales.data.SD01_01Data;
import jp.co.fj.keinavi.data.sheet.ExcelName;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.NestableException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFName;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.contrib.HSSFRegionUtil;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

/**
 *
 * エクセル帳票作成基本クラス
 *
 * 2005.10.31	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public abstract class BaseExcelCreator extends BaseSheetCreator {

	// ファイル名に使う日時フォーマッタ
	private static final Format DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmssSS");
	// 作成日時フォーマッタ
	private static final Format CREATE_DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd");

	// 帳票データ
	private final ISheetData data;
	// 印刷フラグ
	private final int printFlag;
	// 帳票種別
	// 1 ... 表のみ・マクロなし
	// 2 ... 表のみ・マクロあり
	// 3 ... グラフ・マクロなし
	// 4 ... グラフ・マクロあし
	private final int sheetType;
	// 印刷帳票かどうか
	private final boolean isPrintingSheet;
	// １ブックあたりの最大シート数
	private final int maxSheetCount;
	// 作成日
	private final String createDate;

	// セルコピー設定
	private String cellCopySrc;
	// 一時ディレクトリのパス
	private String tempDirPath;
	// 最初のワークブックの保存名
	private String firstWorkbookName;
	// 現在のシートに変更を加えられたかどうか
	private boolean isModifiedSheet = true;
	// ワークブックのカウント
	private int workbookCount = 0;
	// シートのカウント
	private int sheetCount = 0;
	// 現在のワークブック
	private HSSFWorkbook book;
	// 現在のシート
	private HSSFSheet sheet;

	// 2019/10/16 QQ)Ooseto 共通テスト対応 ADD START
	KNLog log = KNLog.getInstance(null,null,null);
	// 2019/10/16 QQ)Ooseto 共通テスト対応 ADD END

	/**
	 * コンストラクタ
	 *
	 * @param pData 帳票データクラス
	 * @param pSequenceId シーケンスID
	 * @param pOutFileList 出力ファイルリスト
	 * @param pPrintFlag 印刷フラグ
	 * @param pMaxSheetCount 最大シート数
	 */
	public BaseExcelCreator(final ISheetData pData,
			final String pSequenceId, final List pOutFileList,
			final int pPrintFlag, final int pMaxSheetCount) {

		super(pData, pSequenceId, pOutFileList);

		this.data = pData;
		this.printFlag = pPrintFlag;
		this.isPrintingSheet = pPrintFlag == 2 || pPrintFlag == 3 || pPrintFlag == 4 || pPrintFlag == 8;
		this.maxSheetCount = pMaxSheetCount;
		this.createDate = CREATE_DATE_FORMAT.format(new Date());

		// グラフなし・マクロなし帳票
		if (!isGraphSheet() && !isPrintingSheet) {
			this.sheetType = 1;

		// グラフなし・マクロあり帳票
		} else if (!isGraphSheet() && isPrintingSheet) {
			this.sheetType = 2;

		// グラフあり・マクロなし帳票
		} else if (isGraphSheet() && !isPrintingSheet) {
			this.sheetType = 3;

		// グラフあり・マクロあり帳票
		} else {
			this.sheetType = 4;
		}
	}

	/**
	 * コンストラクタ
	 *
	 * @param pData 帳票データクラス
	 * @param pSequenceId シーケンスID
	 * @param pOutFileList 出力ファイルリスト
	 * @param pPrintFlag 印刷フラグ
	 * @param pMaxSheetCount 最大シート数
	 * @param pCellCopySrc セルコピー設定
	 */
	public BaseExcelCreator(final ISheetData pData,
			final String pSequenceId, final List pOutFileList,
			final int pPrintFlag, final int pMaxSheetCount,
			final String pCellCopySrc) {

		this(pData, pSequenceId, pOutFileList, pPrintFlag, pMaxSheetCount);
		this.cellCopySrc = pCellCopySrc;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.BaseSheetCreator#initSheet()
	 */
	protected void initSheet() throws Exception {

		// 一時フォルダを作る
		tempDirPath = KNCommonProperty.getKNTempPath() + File.separator + getSequenceId();

		final File temp = new File(tempDirPath);
		if (!temp.isDirectory() && !temp.mkdir()) {
			throw new IOException("一時フォルダの作成に失敗しました。" + tempDirPath);
		}

		// ワークブックを開く
		openWorkbook();
		breakSheet();
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.BaseSheetCreator#destroySheet()
	 */
	protected void destroySheet() throws Exception {

		saveWorkbook();
	}

	/**
	 * ヘッダの出力など、改シート時に行うシート初期化処理をする
	 */
	protected abstract void initSheet(ISheetData pData) throws Exception;

	/**
	 * 帳票IDを返す
	 *
	 * @return
	 */
	protected abstract String getSheetId();

	/**
	 * グラフ帳票であるかどうか
	 *
	 * @return
	 */
	protected abstract boolean isGraphSheet();

	/**
	 * 「名前ボックス」の表記（A4など）で現在のシートからセルを取得する
	 *
	 * @param position
	 * @return
	 */
	protected HSSFCell getCell(final String position) {

		int index = 0;
		final char[] array = position.toCharArray();
		for (int i = 0; i < array.length; i++) {
			if (Character.isDigit(array[i])) break;
			index++;
		}

		final String col = position.substring(0, index);
		final int rownum = Integer.parseInt(position.substring(index)) - 1;

		// 26進数？の変換処理
		int cellnum = -1;
		for (int i = 0; i < col.length(); i++) {
			cellnum += ((char) col.charAt(i) - 64) * Math.pow(26, col.length() - i - 1);
		}

		return getCell(rownum, cellnum);
	}

	/**
	 * 現在のシートからセルを取得する
	 *
	 * @param rownum
	 * @param cellnum
	 * @return
	 */
	protected HSSFCell getCell(final int rownum, final int cellnum) {

		HSSFRow row = sheet.getRow(rownum);
		if (row == null) row = sheet.createRow(rownum);
		HSSFCell cell = row.getCell((short) cellnum);
		if (cell == null) cell = row.createCell((short) cellnum);
		cell.setEncoding(HSSFCell.ENCODING_UTF_16);
		return cell;
	}

	/**
	 * セルに文字列の値をセットする
	 *
	 * @param position
	 * @param value
	 */
	protected void setCellValue(final String position, final String value) {
		setCellValue(getCell(position), value);
	}

	/**
	 * セルに文字列の値をセットする
	 *
	 * @param rownum
	 * @param cellnum
	 * @param value
	 */
	protected void setCellValue(final int rownum, final int cellnum, final String value) {
		setCellValue(getCell(rownum, cellnum), value);
	}

	/**
	 * セルに文字列の値をセットする
	 *
	 * @param cell
	 * @param value
	 */
	protected void setCellValue(final HSSFCell cell, final String value) {

		if (value == null || value.length() == 0 || "-999".equals(value)) {
			cell.setCellValue("");
		} else {
			cell.setCellValue(value);
		}
	}

	/**
	 * セルに数値をセットする
	 *
	 * @param position
	 * @param value
	 */
	protected void setCellValue(final String position, final double value) {
		setCellValue(getCell(position), value);
	}

	/**
	 * セルに数値をセットする
	 *
	 * @param rownum
	 * @param cellnum
	 * @param value
	 */
	protected void setCellValue(final int rownum, final int cellnum, final double value) {
		setCellValue(getCell(rownum, cellnum), value);
	}

	/**
	 * セルに数値をセットする
	 *
	 * @param cell
	 * @param value
	 */
	protected void setCellValue(final HSSFCell cell, final double value) {

		if ((int) value == -999) {
			cell.setCellValue("");
		} else {
			cell.setCellValue(value);
		}
	}

	/**
	 * 水平方向にセルのスタイルをコピーする
	 *
	 * @param fromRownum コピー元行位置
	 * @param toRownum コピー先行位置
	 * @param startCellnum コピー開始列位置
	 * @param size コピーするセル数
	 */
	protected void copyHorizonCellStyle(final int fromRownum,
			final int toRownum, final int startCellnum, final int size) {

		for (int i = 0; i < size; i++) {
			getCell(toRownum, startCellnum + i).setCellStyle(
					getCell(fromRownum, startCellnum + i).getCellStyle());
		}
	}

	/**
	 * セキュリティスタンプをセットする
	 *
	 * @param flag セキュリティスタンプフラグ
	 * @param startCellnum 開始列
	 * @param endCellnum 終了列
	 * @throws NestableException
	 * 		Thrown if the CellStyle can't be changed properly.
	 */
	protected void setSecurityStamp(final int flag, final int startCellnum,
			final int endCellnum) throws NestableException {

		final String securityStamp;

		if (flag == 1) {
			securityStamp = "機密";
		} else if (flag == 2) {
			securityStamp = "部内限り";
		} else if (flag == 3) {
			securityStamp = "校内限り";
		} else if (flag == 4) {
			securityStamp = "取扱注意";
		} else if (flag == 5) {
			securityStamp = "";
		} else if (flag == 11) {
			securityStamp = "機密";
		} else if (flag == 12) {
			securityStamp = "部外秘";
		} else if (flag == 13) {
			securityStamp = "塾外秘";
		} else if (flag == 14) {
			securityStamp = "取扱注意";
		} else {
			securityStamp = "";
		}

		if (securityStamp.length() == 0) {
			return;
		} else {
			setCellValue(0, startCellnum, securityStamp);
		}

		// 罫線設定（範囲指定）
		final Region region = new Region(
				0, (short) startCellnum, 0, (short) endCellnum);
		// 範囲指定による太線設定
		HSSFRegionUtil.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM,
				region, getSheet(), getBook());
		HSSFRegionUtil.setBorderTop(HSSFCellStyle.BORDER_MEDIUM,
				region, getSheet(), getBook());
		HSSFRegionUtil.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM,
				region, getSheet(), getBook());
		HSSFRegionUtil.setBorderRight(HSSFCellStyle.BORDER_MEDIUM,
				region, getSheet(), getBook());
		// 範囲指定による線色設定
		HSSFRegionUtil.setBottomBorderColor(HSSFColor.RED.index,
				region, getSheet(), getBook());
		HSSFRegionUtil.setTopBorderColor(HSSFColor.RED.index,
				region, getSheet(), getBook());
		HSSFRegionUtil.setLeftBorderColor(HSSFColor.RED.index,
				region, getSheet(), getBook());
		HSSFRegionUtil.setRightBorderColor(HSSFColor.RED.index,
				region, getSheet(), getBook());
	}

	/**
	 * 帳票作成日時をセットする
	 */
	protected void setCreateDate() {

		sheet.getHeader().setRight(
				HSSFHeader.font("ＭＳ ゴシック", "標準") + createDate);
	}

	/**
	 * 指定されたカウントのワークブックの保存名を作る。
	 *
	 * 改ワークブックをしていない場合は「帳票ID.xls」
	 * 改ワークブックをしている場合は「帳票ID_NN（01からの連番）.xls」
	 *
	 * これ以外の形式で保存したければこのメソッドをオーバーライドすること
	 *
	 * @param count
	 * @return
	 */
	protected String createSaveName(final int count) {

		// 複数なら連番を付加する
		if (workbookCount > 1) {
			return createBeaseName() + "_" + StringUtils.right(
					"00" + count, 2) + ".xls";
		} else {
			return createBeaseName() + ".xls";
		}
	}

	/**
	 * @param id 帳票ID
	 * @return 保存ファイル名のベース
	 */
	protected String createBeaseName() {
		// 2019/09/09 QQ)Tanioka 出力ファイル名変更 MOD START
		//return getExcelName() + "_" + getTimeStamp();

		String fileName = getTimeStamp();

		try {
			SD01_01Data localData = (SD01_01Data) this.data;
			if ( localData != null ) {
				fileName = getExcelName();
				fileName = fileName.replace( "##", localData.getExamCd() ) ;
				fileName = fileName.replace( "*****", localData.getBundleCd() ) ;
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD START
//			    fileName = fileName.replace( "yyyymmddhhmmss", this.getTimeStamp() ) ;
			    fileName = fileName.replace( "yyyymmddhhmmssSSS", this.getTimeStamp() ) ;
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD END
			}
		} catch (Exception e) {
		    // 2019/10/16 QQ)Ooseto 共通テスト対応 ADD START
		    log.Err("","保存ファイル名作成エラー",e.toString());
		    // 2019/10/16 QQ)Ooseto 共通テスト対応 ADD END
		}

		return fileName;
		// 2019/09/09 QQ)Tanioka 出力ファイル名変更 MOD END
	}

	/**
	 * @return この帳票のエクセルファイル名
	 */
	protected String getExcelName() {
		return ExcelName.getInstance().getName(getSheetId());
	}

	/**
	 * @return yyyyMMddHHmmssSSSフォーマットのタイムスタンプ
	 */
	protected String getTimeStamp() {
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD START
//		return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD END
	}

	/**
	 * ワークブックを開く
	 *
	 * @throws Exception
	 */
	private void openWorkbook() throws Exception {

		final String template;

		// 印刷帳票であるならマクロテンプレート
		if (isPrintingSheet) {
			template = getSheetId() + "P.xls";
		} else {
			template = getSheetId() + ".xls";
		}

		final String filename = KNCommonProperty.getKNDataPath() +
				File.separator + template;

		book = openWorkbook(filename);
		workbookCount++;
		sheetCount = 0;
	}

	private HSSFWorkbook openWorkbook(final String filename) throws Exception {

		BufferedInputStream in = null;
		try {
			in = new BufferedInputStream(new FileInputStream(filename));

			if (in == null) {
				throw new FileNotFoundException(filename);
			}

			return new HSSFWorkbook(in);

		} finally {
			if (in != null) in.close();
		}
	}

	/**
	 * 現在のワークブックを保存する
	 * @throws Exception
	 */
	private void saveWorkbook() throws Exception {

		// 現在のシートに変更がなければ破棄する
		if (!isModifiedSheet) {
			destroyCurrentSheet();
		}

		// 印刷帳票であるならパラメータシートを
		// セットアップする
		if (isPrintingSheet) {
			setupParamSheet();
		}

		// 不要シートの削除
		// グラフなし・マクロなし帳票
		if (sheetType == 1) {
			book.removeSheetAt(0);

		// グラフなし・マクロあり帳票
		} else if (sheetType == 2) {
			book.removeSheetAt(1);

		// グラフあり・マクロなし帳票
		} else if (sheetType == 3) {
			cleanWorkbook(0);

		// グラフあり・マクロあり帳票
		} else {
			cleanWorkbook(1);
		}

		writeWorkbook();
	}

	/**
	 * ワークブックをファイルに書き出す
	 *
	 * @param wb
	 * @throws IOException
	 */
	private void writeWorkbook() throws Exception {

		// シートがなければ書き出さない
		final int count = book.getNumberOfSheets();

		// 印刷帳票はパラメータシートがある
		if (isPrintingSheet && count == 1) {
			return;
		// それ以外
		} else if (!isPrintingSheet && count == 0) {
			return;
		}

		// ファイル名
		final String filename;
		//「保存」の場合
		if (printFlag == 5) {
			// 2019/09/09 QQ)Tanioka 出力ファイル名変更 MOD START
			//filename = tempDirPath + File.separator
			//		+ createSaveName(workbookCount);
			filename = tempDirPath + File.separator
					+ createSaveName(workbookCount);

			// 2019/09/09 QQ)Tanioka 出力ファイル名変更 MOD END
		// それ以外
		} else {
			// 2019/09/09 QQ)Tanioka 出力ファイル名変更 MOD START
			filename = tempDirPath + File.separator
					+ DATE_FORMAT.format(new Date()) + ".xls";

			// 2019/09/09 QQ)Tanioka 出力ファイル名変更 MOD END
		}

		// 最初のワークブック名は保持しておく
		if (workbookCount == 1) {
			firstWorkbookName = filename;
		}

		addOutFile(filename);
		writeWorkbook(filename);

		// 印刷帳票でワークブックが複数の場合の処理
		// 最初のワークブック名を「帳票ID_01.xls」とする処理
		if (workbookCount == 2 && isPrintingSheet) {
			book = openWorkbook(firstWorkbookName);
			sheet = book.getSheetAt(0);
			setCellValue("B3", createSaveName(1));
			writeWorkbook(firstWorkbookName);
			book = null;
			sheet = null;
		}

		//「保存」でワークブックが複数の場合の処理
		if (workbookCount == 2 && printFlag == 5) {
			final String destFilename = tempDirPath
					+ File.separator + createSaveName(1);
			new File(firstWorkbookName).renameTo(new File(destFilename));
			replaceOutFile(firstWorkbookName, destFilename);
		}
	}

	/**
	 * 指定した保存名で現在のワークブックを書き出す
	 *
	 * @param filename
	 * @throws IOException
	 */
	private void writeWorkbook(final String filename) throws IOException {

		BufferedOutputStream out = null;
		try {
			out = new BufferedOutputStream(
					new FileOutputStream(filename));

			if (out == null) {
				throw new FileNotFoundException(filename);
			}

			book.write(out);
		} finally{
			if (out != null) out.close();
		}
	}

	/**
	 * 現在のワークブックから不要なシートを消す
	 */
	private void cleanWorkbook(final int padding) {

		// 無効な名前の定義を消す
		for (int i = book.getNumberOfNames() - 1; i >= 0; i--) {

			final HSSFName name = book.getNameAt(i);

			try {

				// これから消すシートの定義は消す
				if (Integer.parseInt(name.getSheetName()) > sheetCount) {
					book.removeName(i);
				}

			// 運用時にはあってはならないエラー
			// シート名は必ず数字（連番）で定義されているはず
			} catch (final NumberFormatException e) {
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
//			    throw new InternalError(
//						"名前定義においてシート名が数字以外で定義されています。"
//						+ name.getSheetName());
			    throw new InternalError(e);
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			}
		}

		// シートを消す
		for (int i = book.getNumberOfSheets() - 1; i >= sheetCount + padding; i--) {
			book.removeSheetAt(i);
		}
	}

	/**
	 * パラメータシートのセットアップ
	 *
	 * @throws IOException
	 */
	private void setupParamSheet() throws IOException {

		// パラメータシートは先頭にある
		sheet = book.getSheetAt(0);
		// 保存実行指示
		getCell("B2").setCellValue(printFlag == 4 || printFlag == 8 ? "1" : "0");
		// 保存ブック名
		getCell("B3").setCellValue(createSaveName(workbookCount));
		// 印刷実行指示
		getCell("B4").setCellValue("1");
		// セルコピー設定
		if (cellCopySrc != null) {
			getCell("B5").setCellValue(cellCopySrc);
		}
	}

	/**
	 * 改シート処理を行う
	 *
	 * @throws IOException
	 */
	protected void breakSheet() throws Exception {

		// 現在のシートに変更がなければ破棄する
		if (!isModifiedSheet) {
			destroyCurrentSheet();
		}

		// 最大シート数になっているなら改ワークブック
		if (sheetCount == maxSheetCount) {
			saveWorkbook();
			openWorkbook();
		}

		sheetCount++;
		isModifiedSheet = false;

		// グラフなし・マクロなし帳票
		if (sheetType == 1) {
			book.cloneSheet(0);
			book.setSheetName(sheetCount, String.valueOf(sheetCount));
			sheet = book.getSheetAt(sheetCount);

		// グラフなし・マクロあり帳票
		} else if (sheetType == 2) {
			book.cloneSheet(1);
			book.setSheetName(sheetCount + 1, String.valueOf(sheetCount));
			sheet = book.getSheetAt(sheetCount + 1);

		// グラフあり・マクロなし帳票
		} else if (sheetType == 3) {
			sheet = book.getSheetAt(sheetCount - 1);

		// グラフあり・マクロあり帳票
		} else {
			sheet = book.getSheetAt(sheetCount);
		}

		initSheet(data);
	}

	/**
	 * 現在のシートを破棄する
	 */
	private void destroyCurrentSheet() {

		// 表のみ帳票は現在のシートを削除する
		if (sheetType < 3) {
			book.removeSheetAt(book.getNumberOfSheets() -1);
		}

		sheetCount--;
		isModifiedSheet = true;
	}

	/**
	 * @return sheet を戻します。
	 */
	protected HSSFSheet getSheet() {
		return sheet;
	}

	/**
	 * @return book を戻します。
	 */
	protected HSSFWorkbook getBook() {
		return book;
	}

	/**
	 * 現在のシートに変更を加えるタイミングで呼ぶ
	 */
	protected void setModifiedSheet() {
		this.isModifiedSheet = true;
	}

	/**
	 * @return
	 */
	protected HSSFCellStyle createCellStyle() {

		 return book.createCellStyle();
	}

}

/**
 * 高校成績分析−高校間比較
 * 	Excelファイル編集
 * 作成日: 2004/08/19
 * @author	Ito.Y
 */
 
package jp.co.fj.keinavi.excel.business;

import java.util.*;

import jp.co.fj.keinavi.excel.data.business.*;
import jp.co.fj.keinavi.util.log.*;

public class B45 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean b45(B45Item b45Item, ArrayList outfilelist, int intSaveFlg, String UserID, KNSheetLog sheetLog) {
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		try{
			int ret = 0;
			//B45Itemから各帳票を出力
			B45_01 exceledit = new B45_01();
			ret = exceledit.b45_01EditExcel( b45Item, outfilelist, intSaveFlg, UserID );
			if (ret!=0) {
				log.Err("0"+ret+"B45_01","帳票作成エラー","");
				return false;
			}
			sheetLog.add("B45_01");
						
		} catch(Exception e) {
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
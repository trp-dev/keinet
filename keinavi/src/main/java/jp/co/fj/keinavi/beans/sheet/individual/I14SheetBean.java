package jp.co.fj.keinavi.beans.sheet.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import jp.co.fj.kawaijuku.judgement.data.UnivInfoBean;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.beans.individual.TakenExamSearchBean;
import jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean;
import jp.co.fj.keinavi.beans.individual.judgement.JudgementListBean;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.sheet.ExtI11ListBean;
import jp.co.fj.keinavi.excel.data.personal.I14HanteiListBean;
import jp.co.fj.keinavi.excel.data.personal.I14HyoukaListBean;
import jp.co.fj.keinavi.excel.data.personal.I14ShibouHanteiListBean;
import jp.co.fj.keinavi.excel.data.personal.I14TokutenListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.servlets.individual.BaseJudgeThread;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.db.RecordProcessor;
import jp.co.fj.keinavi.util.individual.ExamSorter;
import jp.co.fj.keinavi.util.individual.IPropsLoader;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

/**
 * 個人成績分析−成績分析面談 志望大学判定データリスト印刷用Bean
 *
 *
 * 2005.02.23   Tomohisa YAMADA - Totec
 * 				[1]判定結果をDetailPageBeanに一本化する
 *
 * 2005.02.24   Keisuke KONDO - Totec
 * 				[2]総点取得メソッドをgetWeight()に変更した
 *
 * 2005.04.11   Keisuke KONDO - Totec
 * 				[3]志望校評価と、志望校判定にキャンパス所在地（校舎県名）を追加
 *
 * 2005.04.14   Keisuke KONDO - Totec
 * 				[4]対象模試が新テストだった場合、種別フラグに"1"を入れるように修正
 *
 * 2005.09.16   Keisuke KONDO - Totec
 * 				[5]科目取得の際EXAMSUBJECTではなく、Viewを参照するように修正
 *
 * <2006年度対応>
 * 2006.05.12   Tomohisa YAMADA - Totec
 * 				[6]配点比（学科＋その他）
 *
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 *              ・センター・リサーチ５段階評価対応
 *              ・判定処理の隠蔽
 *
 * 2009.11.30   Tomohisa YAMADA - Totec
 *              ・5教科判定対応
 *
 * 2009.12.01   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 *
 * 2010.01.18   Shoji HASE - Totec
 *              ・「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 * 　       　　　「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 *  　　　　　　・UNIVDIVCD(１桁)→UNIDIV（２桁）変更にともない、条件分岐定数の変更
 *
 * <2010年度＃マーク対応（判定軽量化）>
 * 2010.10.25	Tomohisa YAMADA - Totec
 * 				公表用データを随時取得
 *
 */
public class I14SheetBean extends IndAbstractSheetBean {

    /** serialVersionUID */
    private static final long serialVersionUID = -7659509316594779849L;

    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws Exception {

        /**プロファイル関連**/
        if(!"i401".equals(backward)){

            //11Itemにデータセット
            //判定シートフラグ
            data.setIntHanteiFlg(1);
            //判定シートセキュリティフラグ
            data.setIntHanteiSecuFlg(((Short) profile.getItemMap(IProfileCategory.I_UNIV_RESULTS).get(IProfileItem.PRINT_STAMP)).shortValue());
            data.setIntHanteiShubetsuFlg(getNewTestFlg());//[4] add 新テストフラグ（志望大学判定シート）

        }else{
            /**i401簡単印刷の場合**/
            Short ExamDoc = (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_EXAM_DOC);
            int ExamDocFlg = Integer.parseInt(ExamDoc.toString());

            //11Itemにデータセット
            data.setIntHanteiFlg(ExamDocFlg);
            //判定シートセキュリティフラグ
            data.setIntHanteiSecuFlg(((Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.PRINT_STAMP)).shortValue());
            data.setIntHanteiShubetsuFlg(getNewTestFlg());//[4] add 新テストフラグ（志望大学判定シート）
        }

        // 判定結果
        final Map judgedMap;
        if ("i204".equals(backward)) {
            judgedMap = BaseJudgeThread.getScreenJudgedMap(session);
        } else {
            judgedMap = BaseJudgeThread.getSheetJudgedMap(session);
        }

        //下記条件の時に値を格納
        //・判定シートフラグ == 1
        if (data.getIntHanteiFlg() == 1) {
            PreparedStatement ps1 = null;
            PreparedStatement ps2 = null;
            PreparedStatement ps3 = null;
            PreparedStatement ps4 = null;
            PreparedStatement ps5 = null;
            PreparedStatement ps6 = null;
            PreparedStatement ps7 = null;
            PreparedStatement ps8 = null;
            PreparedStatement ps9 = null;
            ResultSet rs1 = null;
            ResultSet rs2 = null;
            ResultSet rs3 = null;
            ResultSet rs4 = null;
            ResultSet rs5 = null;
            ResultSet rs6 = null;
            ResultSet rs7 = null;
            ResultSet rs8 = null;
            ResultSet rs9 = null;

            //１．全生徒
            for (java.util.Iterator it=data.getI11List().iterator(); it.hasNext();) {

                try {
                        ExtI11ListBean printData = (ExtI11ListBean) it.next();

                        // 対象模試情報
                        final String examYear;
                        final String examCd;
                        // 受験予定大学一覧での印刷は「生徒が受けた最新模試」
                        if ("i302".equals(backward)) {
                            final ExaminationData targetExam = getLatestExam(
                                    printData.getIndividualid());
                            examYear = targetExam.getExamYear();
                            examCd = targetExam.getExamCd();
                            printData.setStrBaseMshmei(targetExam.getExamName());
                            printData.setStrBaseMshDate(targetExam.getInpleDate());
                        // それ以外は対象模試
                        } else {
                            examYear = exam.getExamYear();
                            examCd = exam.getExamCD();
                            printData.setStrBaseMshmei(exam.getExamName());
                            printData.setStrBaseMshDate(exam.getInpleDate());
                        }

                        I14HanteiListBean judge = new I14HanteiListBean();

                        ps1 = conn.prepareStatement(candidateDataSql());
                        ps1.setString(1, printData.getIndividualid());
                        ps1.setString(2, examYear);
                        ps1.setString(3, examCd);
                        rs1 = ps1.executeQuery();

                        while(rs1.next()) {
                            I14HyoukaListBean ls1 = new I14HyoukaListBean();
                            ls1.setStrShiboRank(rs1.getInt("CANDIDATERANK"));
                            ls1.setStrKenmei(rs1.getString("PREFNAME"));
                            ls1.setStrHonbuShozaichi(rs1.getString("PREFNAME"));//[3] add
                            ls1.setStrKoshaShozaichi(rs1.getString("PREFNAME_SH"));//[3] add
                            if(!GeneralUtil.toBlank(rs1.getString("UNIVDIVCD")).equals("")){
                                if(rs1.getString("UNIVDIVCD").equals("01") || rs1.getString("UNIVDIVCD").equals("02")){
                                    ls1.setStrDaigakuKbn(IPropsLoader.getInstance().getMessage("STRING_SCHOOLDIV_1"));//国立大学
                                }else if(rs1.getString("UNIVDIVCD").equals("03")){
                                    ls1.setStrDaigakuKbn(IPropsLoader.getInstance().getMessage("STRING_SCHOOLDIV_2"));//私立大学
                                }else if(rs1.getString("UNIVDIVCD").equals("04") || rs1.getString("UNIVDIVCD").equals("05")){
                                    ls1.setStrDaigakuKbn(IPropsLoader.getInstance().getMessage("STRING_SCHOOLDIV_3"));//短大
                                }else if(rs1.getString("UNIVDIVCD").equals("06") || rs1.getString("UNIVDIVCD").equals("07") || rs1.getString("UNIVDIVCD").equals("08") || rs1.getString("UNIVDIVCD").equals("09")){
                                    ls1.setStrDaigakuKbn(IPropsLoader.getInstance().getMessage("STRING_SCHOOLDIV_4"));//その他
                                }
                            }
                            ls1.setStrDaigakuMei(rs1.getString("UNIVNAME_ABBR"));
                            ls1.setStrGakubuMei(rs1.getString("FACULTYNAME_ABBR"));
                            ls1.setStrGakkaMei(rs1.getString("DEPTNAME_ABBR"));
                            ls1.setIntHyoukaTokuten(rs1.getInt("RATINGCVSSCORE"));
                            ls1.setStrHyoukaCenter(rs1.getString("CENTERRATING"));
                            ls1.setFloHyoukaHensa(rs1.getFloat("RATINGDEVIATION"));
                            ls1.setStrHyoukaKijyutsu(rs1.getString("SECONDRATING"));
                            ls1.setIntHyoukaPoint(rs1.getInt("RATINGPOINT"));
                            ls1.setStrHyoukaAll(rs1.getString("TOTALRATING"));
                            judge.getI14HyoukaList().add(ls1);
                        }
                        if (rs1 != null) {rs1.close();}
                        if (ps1 != null) {ps1.close();}

                        //教科別成績
                        ps2 = conn.prepareStatement(recordDataSql());
                        ps2.setString(1, printData.getIndividualid());
                        ps2.setString(2, examYear);
                        ps2.setString(3, examCd);
                        ps2.setString(4, printData.getIndividualid());
                        ps2.setString(5, examYear);
                        ps2.setString(6, examCd);
                        rs2 = ps2.executeQuery();

                        while(rs2.next()) {
                            I14TokutenListBean recordData = new I14TokutenListBean();
                            recordData.setStrKyokaCd(rs2.getString("COURSECD"));
                            recordData.setStrKmkmei(rs2.getString("SUBADDRNAME"));
                            recordData.setIntTokuten(rs2.getInt("SCORE"));
                            recordData.setFloHensa(rs2.getFloat("A_DEVIATION"));
                            recordData.setStrScholarLevel(rs2.getString("ACADEMICLEVEL"));
                            recordData.setBasicFlg(rs2.getString("BASICFLG"));
                            judge.getI14TokutenList().add(recordData);
                        }
                        if (rs2 != null) {rs2.close();}
                        if (ps2 != null) {ps2.close();}
                        //志望校判定用

                        //セッションに格納してある特定の生徒の結果一覧を取得
                        JudgementListBean jlb = (JudgementListBean) judgedMap.get(printData.getIndividualid());
                        //必要なパラメタのチェック
                        ArrayList univNameList = new ArrayList();
                        if(jlb != null && jlb.getRecordSet() != null){
                        int DispNo = 1;//順位
                        for(int i=0;i<(jlb.getRecordSet()).size();i++){

                                //[1] del start
//								JudgementBean bean = (JudgementBean)((jlb.getSort().getList()).get(i));

                                // 大学判定結果
//								DetailPageBean makeBean = new DetailPageBean();
//								makeBean.setBean(bean);
//								makeBean.setScoreFlg(jlb.getScoreFlg());//得点フラグの設定
//								makeBean.setDetailPageFlg(0);
//								try {
//								 	makeBean.execute();
//								}catch(Exception e){
//								 	throw e;
//								}
                                //[1] del end

                                DetailPageBean makeBean = (DetailPageBean)((jlb.getRecordSet()).get(i));//[1] add

                                I14ShibouHanteiListBean candidateData = new I14ShibouHanteiListBean();
                                candidateData.setStrShiboRank(Integer.toString(DispNo));					//順位
                                candidateData.setStrKenmei(makeBean.getPrefName());							//都道府県名
                                candidateData.setStrHonbuShozaichi(makeBean.getPrefName());//[3] add 本部県名
                                candidateData.setStrKoshaShozaichi(makeBean.getPrefName_sh());//[3] add 校舎県名

                                if(!GeneralUtil.toBlank(makeBean.getUnivDivCd()).equals("")){
                                    if(makeBean.getUnivDivCd().equals("01") || makeBean.getUnivDivCd().equals("02")){
                                        candidateData.setStrDaigakuKbn(IPropsLoader.getInstance().getMessage("STRING_SCHOOLDIV_1"));//国立大学
                                    }else if(makeBean.getUnivDivCd().equals("03")){
                                        candidateData.setStrDaigakuKbn(IPropsLoader.getInstance().getMessage("STRING_SCHOOLDIV_2"));//私立大学
                                    }else if(makeBean.getUnivDivCd().equals("04") || makeBean.getUnivDivCd().equals("05")){
                                        candidateData.setStrDaigakuKbn(IPropsLoader.getInstance().getMessage("STRING_SCHOOLDIV_3"));//短大
                                    }else if(makeBean.getUnivDivCd().equals("06") || makeBean.getUnivDivCd().equals("07") || makeBean.getUnivDivCd().equals("08") || makeBean.getUnivDivCd().equals("09")){
                                        candidateData.setStrDaigakuKbn(IPropsLoader.getInstance().getMessage("STRING_SCHOOLDIV_4"));//その他
                                    }
                                }
                                candidateData.setStrDaigakuMei(makeBean.getUnivName());						//大学名
                                candidateData.setStrGakubuMei(makeBean.getDeptName());						//学部名
                                candidateData.setStrGakkaMei(makeBean.getSubName());						//学科名
                                candidateData.setStrBoshuNinzu(makeBean.getCapacity());						//募集人数
                                candidateData.setStrExamCapaRel(makeBean.getBean().getUniv().getExamCapaRel()); //定員信頼

                                if(makeBean.getC_score() == -1){
                                    candidateData.setIntHyoukaTokuten(-999);									//センター試験　評価点
                                }else{
                                    candidateData.setIntHyoukaTokuten(makeBean.getC_score());	//センター試験　評価点
                                }

                                candidateData.setStrHyoukaMark(makeBean.getCLetterJudgement());					//センター試験　評価
                                if(makeBean.getS_score() == -1){
                                    candidateData.setFloKijyutsuHensa(-999);									//個別試験　評価偏差値
                                }else{
                                    String S_score = String.valueOf(makeBean.getS_score());
                                    candidateData.setFloKijyutsuHensa(Float.valueOf(S_score).floatValue());					//個別試験　評価偏差値
                                }

                                candidateData.setStrHyoukaKijyutsu(makeBean.getSLetterJudgement());				//個別試験　評価

                                if(makeBean.getT_score() == -1){
                                    candidateData.setIntPointAll(-999);											//総合ポイント
                                }else{
                                    candidateData.setIntPointAll(makeBean.getT_score());						//総合ポイント
                                }

                                candidateData.setStrHyoukaAll(makeBean.getEvalTotal().getLetter());					//総合評価

                                //ボーダーが無効値の場合は-999を設定
                                if (makeBean.getBorderPointStr().length() == 0) {
                                    candidateData.setIntBorderTen(-999);
                                }
                                else {
                                    candidateData.setIntBorderTen(Integer.parseInt(makeBean.getBorderPointStr()));
                                }

                                if(makeBean.getCenScoreRate() == 99){
                                    candidateData.setFloBorderTokuritsu(-999);									//ボーダー得点率
                                }else{
                                    candidateData.setFloBorderTokuritsu((float)makeBean.getCenScoreRate());	//ボーダー得点率
                                }

                                if(makeBean.getRank()==JudgementConstants.Univ.RANK_FORCE) {
                                    candidateData.setFloRankHensa(0);
                                }else if(makeBean.getRankLLimits() == 99){
                                    candidateData.setFloRankHensa(-999);										//難易ランク偏差値
                                }else{
                                    candidateData.setFloRankHensa((float)makeBean.getRankLLimits());			//難易ランク偏差値
                                }

                                //公表科目のために同じロジックを使うためコピー
                                 //JudgementBean copiedBean = (JudgementBean) bean.clone();//[1] del

                                //情報誌用科目データ作成
                                //強制的に最初の枝番で配点を出す（仕様）
                                SortedMap map = (SortedMap) univInfoMap.get(makeBean.getUniqueKey());

                                //情報誌用科目データが存在する場合のみ実行
                                if(map != null){
                                    UnivInfoBean infoBean = (UnivInfoBean) map.values().iterator().next();

                                    candidateData.setIntPluralFlg(map.size() > 1 ? 1 : 0); //パターンフラグ
                                    candidateData.setStrShikenKmkCenter(infoBean.getC_subString()); //試験科目（センター）
                                    candidateData.setStrShikenKmk2(infoBean.getS_subString()); //試験科目（２次）

                                    if (infoBean.isCExist()) {
                                        String[] allot1 = KNUtil.splitInfoAllot(infoBean.getAllot1());
                                        candidateData.setStrSoutenCenter(allot1[0]); //総点（センター）
                                        candidateData.setStrCourseAllotCenter(allot1[1]); //教科配点（センター）
                                    }

                                    if (infoBean.isSExist()) {
                                        String[] allot2 = KNUtil.splitInfoAllot(infoBean.getAllot2());
                                        candidateData.setStrSouten2(allot2[0]); //総点（２次）
                                        candidateData.setStrCourseAllot2(allot2[1]); //教科配点（２次）
                                    }
                                }

                                candidateData.setStrSyutsuganbi(makeBean.getAppliDeadLine());				//出願日
                                candidateData.setStrShikenbi(makeBean.getGuideRemarks());					//試験日
                                candidateData.setStrGoukakubi(makeBean.getSucAnnDate());					//合格日
                                candidateData.setStrTetsudukibi(makeBean.getProceDeadLine());				//締切日
                                judge.getI14ShibouHanteiList().add(candidateData);

                            //判定条件(こだわり以外)に使用するリスト
                            if(!univNameList.contains(makeBean.getUnivName())){
                                //大学名リスト作成
                                univNameList.add(makeBean.getUnivName());
                            }

                            //順位
                            DispNo ++;
                          }
                      }

                    /**オマカセ判定**/
                    if("i401".equals(backward) || judgeType.equals("byAuto")){
                        //オマカセ判定全て無効値
                        judge.setStrArea(null);
                        judge.setStrKeitou(null);
                        judge.setIntAssignFlg(-999);
                        judge.setIntSecondFlg(-999);
                        judge.setStrGakkoKbn(null);
                        judge.setStrNyushibiStart(null);
                        judge.setStrNyushibiEnd(null);
                        judge.setIntHyoukaHanniFlg(-999);
                        judge.setStrHyoukaHanni(null);
                        judge.setStrTaisyougai(null);
                        judge.setStrNijiKmk(null);
                    }

                    /**こだわり判定**/
                    if(judgeType.equals("byCond")){

                    //基本的な判定条件取得
                    ps3 = conn.prepareStatement(QueryLoader.getInstance().load("i14_1").toString());
                    ps3.setString(1, printData.getIndividualid());
                    rs3 = ps3.executeQuery();

                    StringBuffer prefname = new StringBuffer();			//都道府県
                    StringBuffer imposeSub = new StringBuffer();			//二次科目
                    StringBuffer stemmadiv = new StringBuffer();			//系統

                    while(rs3.next()){
                        Query query = QueryLoader.getInstance().load("i14_2");
                        if(!GeneralUtil.toBlank(rs3.getString("DISTRICTPROVISO")).equals("")){
                            query.setStringArray(1,CollectionUtil.deconcatComma(rs3.getString("DISTRICTPROVISO")));
                            ps4 = conn.prepareStatement(query.toString());
                            rs4 = ps4.executeQuery();

                        int prefCount = 0;

                            while(rs4.next()){
                                //都道府県を作る
                                if(!GeneralUtil.toBlank(rs4.getString("PREFNAME")).equals("")){
                                    if(prefCount != 0){
                                        prefname.append("、");
                                    }
                                    prefname.append(GeneralUtil.toBlank(rs4.getString("PREFNAME")));
                                    prefCount ++;
                                }
                            }
                        }
                        /**学校区分**/
                        StringBuffer schoolDiv = new StringBuffer();

                        String[] schoolDivName = CollectionUtil.splitComma(GeneralUtil.toBlank(rs3.getString("SCHOOLDIVPROVISO")));
                        int schoolCount = 0;

                        for (int i = 0; i < schoolDivName.length; i++) {
                            if(!schoolDivName[i].equals("")){

                                if(schoolCount != 0){
                                    schoolDiv.append("、");
                                }
                                switch (Integer.parseInt(schoolDivName[i])) {
                                case 1:
                                schoolDiv.append(IPropsLoader.getInstance().getMessage("STRING_UNIVDIVNAME_1"));
                                schoolCount ++;
                                break;

                                case 2:
                                schoolDiv.append(IPropsLoader.getInstance().getMessage("STRING_UNIVDIVNAME_2"));
                                schoolCount ++;
                                break;

                                case 3:
                                schoolDiv.append(IPropsLoader.getInstance().getMessage("STRING_UNIVDIVNAME_3"));
                                schoolCount ++;
                                break;

                                case 4:
                                schoolDiv.append(IPropsLoader.getInstance().getMessage("STRING_UNIVDIVNAME_4"));
                                schoolCount ++;
                                break;

                                case 5:
                                schoolDiv.append(IPropsLoader.getInstance().getMessage("STRING_UNIVDIVNAME_5"));
                                schoolCount ++;
                                break;

                                case 6:
                                schoolDiv.append(IPropsLoader.getInstance().getMessage("STRING_UNIVDIVNAME_6"));
                                schoolCount ++;
                                break;

                                case 7:
                                schoolDiv.append(IPropsLoader.getInstance().getMessage("STRING_UNIVDIVNAME_7"));
                                schoolCount ++;
                                break;

                                case 8:
                                schoolDiv.append(IPropsLoader.getInstance().getMessage("STRING_UNIVDIVNAME_8"));
                                schoolCount ++;
                                break;

                                case 9:
                                schoolDiv.append(IPropsLoader.getInstance().getMessage("STRING_UNIVDIVNAME_9"));
                                schoolCount ++;
                                break;

                                case 10:
                                schoolDiv.append(IPropsLoader.getInstance().getMessage("STRING_UNIVDIVNAME_10"));
                                schoolCount ++;
                                break;

                                case 11:
                                schoolDiv.append(IPropsLoader.getInstance().getMessage("STRING_UNIVDIVNAME_11"));
                                schoolCount ++;
                                break;

                                }

                            }
                        }

                        /**以下の試験科目を課す、課さない**/
                        if(rs3.getInt("DESCIMPOSEDIV") != 1){

                            String[] imposeSubName = CollectionUtil.splitComma(GeneralUtil.toBlank(rs3.getString("IMPOSESUB")));
                            int imposeCount = 0;

                            for (int i = 0; i < imposeSubName.length; i++) {
                                if(!imposeSubName[i].equals("")){
                                    if(imposeCount != 0){
                                        imposeSub.append("、");
                                    }

                                    if(imposeSubName[i].equals("english")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_1"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("math1")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_2"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("math1A")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_3"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("math2A")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_4"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("math2B")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_5"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("math3B")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_6"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("math3C")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_7"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("japaneseLit")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_8"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("jClassics")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_9"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("japanese")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_10"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("physics")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_11"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("chemistry")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_12"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("biology")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_13"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("earthScience")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_14"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("jHistory")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_15"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("wHistory")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_16"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("geography")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_17"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("ethic")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_18"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("politics")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_19"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("essay")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_20"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("comprehensive")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_21"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("interview")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_22"));
                                        imposeCount ++;
                                    }

                                    if(imposeSubName[i].equals("performance")){
                                        imposeSub.append(IPropsLoader.getInstance().getMessage("STRING_SUBNAME_23"));
                                        imposeCount ++;
                                    }

                                }
                            }
                        }


                        /**検索系統区分が、中分類の場合**/
                        if(GeneralUtil.toBlank(rs3.getString("SEARCHSTEMMADIV")).equals("1")){
                            //中系統の場合
                            StringBuffer StringStemma = new StringBuffer();
                            Query query1 = QueryLoader.getInstance().load("i14_3");
                            if(!GeneralUtil.toBlank(rs3.getString("STEMMAPROVISO")).equals("")){

                                String[] stemmdiv = CollectionUtil.splitComma(GeneralUtil.toBlank(rs3.getString("STEMMAPROVISO")));
                                int Count = 0;

                                for(int i = 0; i < stemmdiv.length; i++){
                                    if(Count != 0){
                                        StringStemma.append(",");
                                    }
                                    StringStemma.append(stemmdiv[i].substring(2,4));
                                    Count ++;
                                }

                                query1.setStringArray(1,(CollectionUtil.deconcatComma(StringStemma.toString())));
                                ps5 = conn.prepareStatement(query1.toString());
                                rs5 = ps5.executeQuery();

                            int stemmCount = 0;

                                while(rs5.next()){
                                    //系統リスト作成
                                    if(!GeneralUtil.toBlank(rs5.getString("MSTEMMANAME")).equals("")){
                                        if(stemmCount != 0){
                                            stemmadiv.append("、");
                                        }
                                        stemmadiv.append(rs5.getString("MSTEMMANAME").trim());

                                        stemmCount ++;
                                    }
                                }
                            }
                            if (rs5 != null) {rs5.close();}
                            if (rs5 != null) {rs5.close();}

                        }else{
                            if(GeneralUtil.toBlank(rs3.getString("SEARCHSTEMMADIV")).equals("2") || GeneralUtil.toBlank(rs3.getString("SEARCHSTEMMADIV")).equals("3")){
                                //小系統の場合
                                Query query2 = QueryLoader.getInstance().load("i14_4");
                                if(!GeneralUtil.toBlank(rs3.getString("STEMMAPROVISO")).equals("")){
                                    query2.setStringArray(1,CollectionUtil.deconcatComma(rs3.getString("STEMMAPROVISO")));

                                ps6 = conn.prepareStatement(query2.toString());
                                rs6 = ps6.executeQuery();

                                int stemmCount = 0;
                                    while(rs6.next()){
                                        if(!GeneralUtil.toBlank(rs6.getString("REGIONNAME")).equals("")){
                                            //系統リスト作成
                                            if(stemmCount != 0){
                                                stemmadiv.append("、");
                                            }
                                            stemmadiv.append(rs6.getString("REGIONNAME").trim());

                                            stemmCount ++;
                                        }
                                    }
                                    if (rs6 != null) {rs6.close();}
                                    if (rs6 != null) {rs6.close();}
                                }
                            }
                        }

                        /**評価範囲をばらしAとかBにする**/
                        StringBuffer raingProviso = new StringBuffer();
                        String[] raingProvisoName = CollectionUtil.splitComma(GeneralUtil.toBlank(rs3.getString("RAINGPROVISO")));

                        int raingCount = 0;

                        for (int i = 0; i < raingProvisoName.length; i++) {

                            if(!raingProvisoName[i].equals("")){

                                if(raingCount != 0){
                                    raingProviso.append("、");
                                }

                                switch (Integer.parseInt(raingProvisoName[i])) {
                                    case 1:
                                    raingProviso.append(IPropsLoader.getInstance().getMessage("STRING_RAINGPROVISO_A"));
                                    raingCount ++;
                                    break;

                                    case 2:
                                    raingProviso.append(IPropsLoader.getInstance().getMessage("STRING_RAINGPROVISO_B"));
                                    raingCount ++;
                                    break;

                                    case 3:
                                    raingProviso.append(IPropsLoader.getInstance().getMessage("STRING_RAINGPROVISO_C"));
                                    raingCount ++;
                                    break;

                                    case 4:
                                    raingProviso.append(IPropsLoader.getInstance().getMessage("STRING_RAINGPROVISO_D"));
                                    raingCount ++;
                                    break;

                                    case 5:
                                    raingProviso.append(IPropsLoader.getInstance().getMessage("STRING_RAINGPROVISO_E"));
                                    raingCount ++;
                                    break;

                                }

                            }
                        }

                        /**対象外(女子大・夜間)**/
                        StringBuffer outTarget = new StringBuffer();
                        String[] outTargetName = CollectionUtil.splitComma(GeneralUtil.toBlank(rs3.getString("OUTOFTERGETPROVISO")));
                        int outTargetCount = 0;
                        for(int i = 0; i < outTargetName.length; i++){

                            if(outTargetCount != 0){
                                outTarget.append("、");
                            }

                            if(!outTargetName[i].equals("")){
                            switch (Integer.parseInt(outTargetName[i])) {
                                case 1:
                                outTarget.append(IPropsLoader.getInstance().getMessage("STRING_FEMALE_SCHOOL"));
                                outTargetCount ++;
                                break;

                                case 2:
                                outTarget.append(IPropsLoader.getInstance().getMessage("STRING_TWO_SCHOOL"));
                                outTargetCount ++;
                                break;

                                case 3:
                                outTarget.append(IPropsLoader.getInstance().getMessage("STRING_NIGHT_SCHOOL"));
                                outTargetCount ++;
                                break;

                            }

                            }

                        }

                        //rs.3
                        judge.setIntAssignFlg(rs3.getInt("SUBIMPOSEDIV"));																	//判定条件 課す課さないフラグ
                        judge.setIntSecondFlg(rs3.getInt("DESCIMPOSEDIV"));																	//判定条件 (センタ利用大)2次･独自試験を課さない
                        judge.setStrGakkoKbn(schoolDiv.toString());																			//学校区分
                        judge.setStrNyushibiStart(RecordProcessor.dateWSlash(rs3.getString("STARTDATEPROVISO"),false).replace(',' ,'、'));	//入試日MM/DDにする
                        judge.setStrNyushibiEnd(RecordProcessor.dateWSlash(rs3.getString("ENDDATEPROVISO"),false).replace(',' ,'、'));		//入試日終わりMM/DDにする
                        judge.setIntHyoukaHanniFlg(rs3.getInt("RAINGDIV"));																	//評価区分
                        judge.setStrHyoukaHanni(raingProviso.toString());																	//評価範囲
                        judge.setStrTaisyougai(outTarget.toString());																		//対象外
                        //rs.2地区リスト
                        judge.setStrArea(prefname.toString());																				//対象地区
                        //rs.3系統リスト
                        judge.setStrKeitou(stemmadiv.toString());																			//学部系統
                        //rs.4独自試験をフラグが１の時
                        judge.setStrNijiKmk(imposeSub.toString());

                        //こだわり条件なので大学名null
                        judge.setStrDaigaku(null);
                    }


                }//こだわり判定
                if (ps3 != null) {ps3.close();}
                if (rs3 != null) {rs3.close();}

                /**こだわり判定(大学指定)**/
                if(judgeType.equals("byUniv")){
                    //大学名のみ出力他はnull
                    judge.setStrDaigaku(CollectionUtil.deSplitComma((String[])univNameList.toArray(new String[0])).replace(',' ,'、'));

                    judge.setIntAssignFlg(-999);
                    judge.setIntSecondFlg(-999);
                    judge.setStrGakkoKbn(null);
                    judge.setStrNyushibiStart(null);
                    judge.setStrNyushibiEnd(null);
                    judge.setIntHyoukaHanniFlg(-999);
                    judge.setStrHyoukaHanni(null);
                    judge.setStrTaisyougai(null);
                    judge.setStrArea(null);
                    judge.setStrKeitou(null);
                    judge.setStrNijiKmk(null);
                }


                printData.getI14HanteiList().add(judge);

                } finally {
                    if (ps1 != null) {ps1.close();}
                    if (ps2 != null) {ps2.close();}
                    if (ps3 != null) {ps3.close();}
                    if (ps4 != null) {ps4.close();}
                    if (ps5 != null) {ps5.close();}
                    if (ps6 != null) {ps6.close();}
                    if (ps7 != null) {ps7.close();}
                    if (ps8 != null) {ps8.close();}
                    if (ps9 != null) {ps9.close();}
                    if (rs1 != null) {rs1.close();}
                    if (rs2 != null) {rs2.close();}
                    if (rs3 != null) {rs3.close();}
                    if (rs4 != null) {rs4.close();}
                    if (rs5 != null) {rs5.close();}
                    if (rs6 != null) {rs6.close();}
                    if (rs7 != null) {rs7.close();}
                    if (rs8 != null) {rs8.close();}
                    if (rs9 != null) {rs9.close();}
                }
            }//個人
        }
    }

    // 指定した生徒の受験した最新模試を取得
    private ExaminationData getLatestExam(
            final String individualId) throws Exception {

        final TakenExamSearchBean bean = new TakenExamSearchBean();
        bean.setConnection(null, conn);
        bean.setIndividualId(individualId);
        bean.setExamSession(examSession);

        // 担当クラス年度よりも以前のものに限定する
        bean.setLimitedYear(ProfileUtil.getChargeClassYear(profile));

        // 対象を模試区分01,02に限定する
        bean.setExamTypeCds(new String[]{
                IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_MARK"),
                IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_WRTN")});

        // センターリサーチ・高２模試を除く
        bean.setExcludeExamCds(new String[]{
                IPropsLoader.getInstance().getMessage("CODE_EXAM_CENTERRESEARCH"),
                IPropsLoader.getInstance().getMessage("CODE_EXAM_MARK_2NDGRADE"),
                IPropsLoader.getInstance().getMessage("CODE_EXAM_WRTN_2NDGRADE")});
        bean.execute();

        final List results = bean.getRecordSetAll();

        if (!results.isEmpty()) {
            // 外部開放日順にソート
            Collections.sort(results, new ExamSorter().new SortByOutDataOpenDate());
            // この生徒が受けた全ての模試の一番最初が最新の模試
            return (ExaminationData) results.get(0);
        } else {
            ExaminationData exam = new ExaminationData();
            exam.setExamYear("");
            exam.setExamCd("");
            return exam;
        }
    }

    //志望大学評価SQL
    private String candidateDataSql() {
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("SELECT ");
        strBuf.append(" /*+ INDEX(CR PK_CANDIDATERATING) */ ");//12/5 sql hint
        strBuf.append("NVL(CR.CANDIDATERANK, -999) CANDIDATERANK, ");
        strBuf.append("P.PREFNAME, ");
        strBuf.append("P2.PREFNAME PREFNAME_SH, ");//[3] add
        strBuf.append("HS.UNIDIV UNIVDIVCD, ");
        strBuf.append("HS.UNINAME_ABBR UNIVNAME_ABBR, ");
        strBuf.append("HS.FACULTYNAME_ABBR, ");
        strBuf.append("HS.DEPTNAME_ABBR, ");
        strBuf.append("NVL(CR.RATINGCVSSCORE, -999) RATINGCVSSCORE, ");
        strBuf.append("NVL(CR.RATINGDEVIATION, -999.0) RATINGDEVIATION, ");
        strBuf.append("NVL(CR.RATINGPOINT, -999) RATINGPOINT, ");
        strBuf.append("CR.CENTERRATING, ");
        strBuf.append("CR.SECONDRATING, ");
        strBuf.append("CR.TOTALRATING ");
        strBuf.append("FROM ");
        strBuf.append("EXAMINATION EX ");
        strBuf.append("INNER JOIN ");
        strBuf.append("CANDIDATERATING CR ");
        strBuf.append("ON EX.EXAMYEAR = CR.EXAMYEAR ");
        strBuf.append("AND EX.EXAMCD = CR.EXAMCD ");
        strBuf.append("LEFT JOIN ");
        strBuf.append("UNIVMASTER_BASIC HS ");
        strBuf.append("ON HS.EVENTYEAR = CR.EXAMYEAR ");
        strBuf.append("AND HS.EXAMDIV = EX.EXAMDIV ");
        strBuf.append("AND HS.UNIVCD = CR.UNIVCD ");
        strBuf.append("AND HS.FACULTYCD = CR.FACULTYCD ");
        strBuf.append("AND HS.DEPTCD = CR.DEPTCD ");
        strBuf.append("LEFT JOIN ");
        strBuf.append("PREFECTURE P ");
        strBuf.append("ON P.PREFCD = HS.PREFCD_EXAMHO ");
        strBuf.append("LEFT JOIN PREFECTURE P2 ON P2.PREFCD = HS.PREFCD_EXAMSH ");//[3] add
        strBuf.append("WHERE ");
        strBuf.append("CR.INDIVIDUALID = ? ");
        strBuf.append("AND CR.EXAMYEAR = ? ");
        strBuf.append("AND CR.EXAMCD = ? ");
        strBuf.append("ORDER BY ");
        strBuf.append("CR.CANDIDATERANK ");
        return strBuf.toString();
    }

    //教科別成績SQL
    private String recordDataSql() {
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("SELECT ");
        strBuf.append(" /*+ INDEX(SI PK_SUBRECORD_I) */ ");//12/5 sql hint
        strBuf.append("C.COURSECD, ");
        strBuf.append("ES.SUBADDRNAME, ");
        strBuf.append("ES.DISPSEQUENCE, ");
        strBuf.append("NVL(SI.SCORE, -999) SCORE, ");
        strBuf.append("NVL(SI.A_DEVIATION, -999.0) A_DEVIATION, ");
        strBuf.append("SI.ACADEMICLEVEL ACADEMICLEVEL, ");
        strBuf.append("1 SORT_NO, ");
        strBuf.append("ES.DISPSEQUENCE DISP, ");
        strBuf.append("SI.SCOPE, ");
        strBuf.append("NULL BASICFLG ");
        strBuf.append("FROM ");
        //strBuf.append("EXAMSUBJECT ES ");//[5] delete
        strBuf.append("V_I_CMEXAMSUBJECT ES ");//[5] add
        strBuf.append("LEFT JOIN ");
        strBuf.append("SUBRECORD_I SI ");
        strBuf.append("ON ES.SUBCD = SI.SUBCD ");
        strBuf.append("AND ES.EXAMYEAR = SI.EXAMYEAR ");
        strBuf.append("AND ES.EXAMCD = SI.EXAMCD ");
        strBuf.append("INNER JOIN ");
        strBuf.append("COURSE C ");
        strBuf.append("ON C.YEAR = SI.EXAMYEAR ");
        strBuf.append("AND C.COURSECD = (SUBSTR(SI.SUBCD, 0, 1) || '000') ");
        strBuf.append("INNER JOIN ");
        strBuf.append("EXAMINATION EX ");
        strBuf.append("ON EX.EXAMYEAR = SI.EXAMYEAR ");
        strBuf.append("AND EX.EXAMCD = SI.EXAMCD ");
        strBuf.append("WHERE ");
        strBuf.append("SI.INDIVIDUALID = ? ");
        strBuf.append("AND SI.EXAMYEAR = ? ");
        strBuf.append("AND SI.EXAMCD = ? ");
        strBuf.append("AND SI.SUBCD >= '7000' ");
        strBuf.append("UNION ALL ");
        strBuf.append("SELECT ");
        strBuf.append(" /*+ INDEX(SI PK_SUBRECORD_I) */ ");//12/5 sql hint
        strBuf.append("C.COURSECD, ");
        strBuf.append("ES.SUBADDRNAME, ");
        strBuf.append("ES.DISPSEQUENCE, ");
        strBuf.append("SI.SCORE, ");
        strBuf.append("SI.A_DEVIATION, ");
        strBuf.append("SI.ACADEMICLEVEL, ");
        strBuf.append("2 SORT_NO, ");
        strBuf.append("CASE WHEN SI.EXAMCD IN ('01','02','03','04','38') THEN ");
        strBuf.append("    CASE WHEN SI.SCOPE=9 OR SI.SCOPE IS NULL THEN ES.DISPSEQUENCE ");
        strBuf.append("    ELSE CASE SUBSTR(SI.SUBCD,1,1) WHEN '4' THEN 4000 ");
        strBuf.append("                                   WHEN '5' THEN 5000 ");
        strBuf.append("         ELSE ES.DISPSEQUENCE ");
        strBuf.append("         END ");
        strBuf.append("    END ");
        strBuf.append("ELSE ES.DISPSEQUENCE ");
        strBuf.append("END DISP, ");
        strBuf.append("SI.SCOPE, ");
        strBuf.append("GETCHARFLAGVALUE(ES.NOTDISPLAYFLG, 5) BASICFLG ");
        strBuf.append("FROM ");
        strBuf.append("EXAMSUBJECT ES ");
        strBuf.append("LEFT JOIN ");
        strBuf.append("SUBRECORD_I SI ");
        strBuf.append("ON ES.SUBCD = SI.SUBCD ");
        strBuf.append("AND ES.EXAMYEAR = SI.EXAMYEAR ");
        strBuf.append("AND ES.EXAMCD = SI.EXAMCD ");
        strBuf.append("INNER JOIN ");
        strBuf.append("COURSE C ");
        strBuf.append("ON C.YEAR = SI.EXAMYEAR ");
        strBuf.append("AND C.COURSECD = (SUBSTR(SI.SUBCD, 0, 1) || '000') ");
        strBuf.append("AND C.COURSECD <= '5000' ");
        strBuf.append("INNER JOIN EXAMINATION EX ");
        strBuf.append("ON EX.EXAMYEAR = SI.EXAMYEAR ");
        strBuf.append("AND EX.EXAMCD = SI.EXAMCD ");
        strBuf.append("WHERE ");
        strBuf.append("SI.INDIVIDUALID = ? ");
        strBuf.append("AND SI.EXAMYEAR = ? ");
        strBuf.append("AND SI.EXAMCD = ? ");
        strBuf.append("AND SI.SUBCD < '6000' ");
        strBuf.append("AND GETCHARFLAGVALUE(ES.NOTDISPLAYFLG, 3) = '0' ");
        strBuf.append("ORDER BY ");
        strBuf.append("SORT_NO ASC, ");
        strBuf.append("COURSECD ASC, ");
        strBuf.append("BASICFLG ASC, ");
        strBuf.append("DISP ASC, ");
        strBuf.append("SCOPE DESC ");
        return strBuf.toString();
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
     */
    protected String getCategory() {
        return IProfileCategory.I_SCORE_QUE;
    }

}
/**
 * 校内成績分析−他校比較　偏差値分布
 * 	Excelファイル編集
 * 作成日: 2004/08/11
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.school;

import java.io.*;
import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.excel.cm.*;
import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.*;

public class S42_03 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	final private int	intMaxSheetSr	= 50;			// 最大シート数	
	final private String	strArea			= "A1:AK64";	// 印刷範囲	
	final private String	masterfile0		= "S42_03";		// ファイル名
	final private String	masterfile1		= "NS42_03";	// ファイル名
	private String	masterfile		= "";					// ファイル名
	final private int[]	tabRow			= {5,35};		// 表の基準点
	final private int[]	tabCol			= {1,4,7,10,13,16,19,22,25,28,31,34};	// 表の基準点

/*
 * 	Excel編集メイン
 * 		S42Item s42Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s42_03EditExcel(S42Item s42Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("S42_03","S42_03帳票作成開始","");
		
		//テンプレートの決定
		if (s42Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			
			// データセット
			ArrayList	s42List			= s42Item.getS42List();
			Iterator	itr				= s42List.iterator();
			int		row				= 0;	// 行
			int 		setRow			= -1;	// *セット用
			int 		ninzu			= 0;	// *作成用
			int 		maxNo			= 19;	// 最大表示件数
			int		hyouCnt			= 0;	// 値範囲：０〜１　(表カウンター)
			int		gakkoCnt		= 0;	// 値範囲：０〜９　(学校カウンター)
			int		maxSheetIndex	= 0;	// シートカウンター
			int		intBookCngCount	= 0;	// ブックカウンター
			int		kmkCd			= 0;	// 科目コード保持
			boolean	kataFlg			= true;// false:改行済(型・科目名変更済)　
			boolean	bolGakkouFlg	= true;// true:学校情報表示　（学校情報表示フラグ）
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
			boolean	bolFirstDataFlg	= true;// true:List.Next()を実行
			int		intFirstCnt		= 0;	// 読み飛ばし用カウンター
			int		intJikouCnt		= 0;	// 過年度分自校情報表示カウンター
//add 2004/10/27 T.Sakai セル計算対応
			int		ruikeiNinzu		= 0;	// 累計人数
//add end

			FileInputStream	fin			= null;
			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			// 基本ファイルを読込む
			S42ListBean s42ListBean = new S42ListBean();

			while( itr.hasNext() ) {
				s42ListBean = (S42ListBean)itr.next();

				// 基本ファイルを読み込む
				S42GakkoListBean	s42GakkoListBean	= new S42GakkoListBean();

				//データの保持
				ArrayList	s42GakkoList		= s42ListBean.getS42GakkoList();
				Iterator	itrS42Gakko		= s42GakkoList.iterator();
	
				// 型・科目の最初のデータ(=学校データ)
				bolFirstDataFlg=true;


				//　学校データセット
				gakkoCnt = 0;
				while(itrS42Gakko.hasNext()){

					if( bolBookCngFlg == true ){
						// マスタExcel読み込み
						workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
						if( workbook==null ){
							return errfread;
						}
						bolSheetCngFlg	= true;
						bolBookCngFlg	= false;
						maxSheetIndex	= 0;
					}

					if(bolSheetCngFlg){
						// シートテンプレートのコピー
						workSheet = workbook.cloneSheet(0);
						maxSheetIndex++;

						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);

						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, s42Item.getIntSecuFlg() ,35 ,36 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 35 );
						workCell.setCellValue(secuFlg);

						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
						workCell.setCellValue( "学校名　　　：" + cm.toString(s42Item.getStrGakkomei()) );

						// 対象模試セット
						String moshi =cm.setTaisyouMoshi( s42Item.getStrMshDate() );	// 模試月取得
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( "対象模試　　：" + cm.toString(s42Item.getStrMshmei()) + moshi);

						kataFlg=true;
						bolSheetCngFlg=false;
					}

					if(kmkCd!=Integer.parseInt(s42ListBean.getStrKmkCd())){
						kataFlg=true;
					}
					// 型名・配点セット
					if(kataFlg){
						String haiten = "";
						if ( !cm.toString(s42ListBean.getStrHaitenKmk()).equals("") ) {
							haiten = "（" + s42ListBean.getStrHaitenKmk() + "）";
						}
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt], 0 );
						workCell.setCellValue( "型・科目：" + cm.toString(s42ListBean.getStrKmkmei()) + haiten );

						kmkCd=Integer.parseInt(s42ListBean.getStrKmkCd());
						bolGakkouFlg=true;
						kataFlg=false;
					}

					if(bolGakkouFlg){
						s42GakkoListBean	= (S42GakkoListBean) s42GakkoList.get(intJikouCnt%3);
						if(intJikouCnt%3==2){
							bolGakkouFlg=false;
						}
						intJikouCnt++;

						// 学校情報読み飛ばし
						if(bolFirstDataFlg){
							s42GakkoListBean	= (S42GakkoListBean) itrS42Gakko.next();
							if(intFirstCnt%3==2){
								bolFirstDataFlg=false;
							}
							intFirstCnt++;
						}
					}else{
						s42GakkoListBean	= (S42GakkoListBean) itrS42Gakko.next();
					}

					S42BnpListBean s42BnpListBean = new S42BnpListBean();
					ArrayList	s42BnpList		= s42GakkoListBean.getS42BnpList();
					Iterator	itrS42Bnp;
					itrS42Bnp		= s42BnpList.iterator();

					// 偏差値データセット
					row=0;
//add 2004/10/27 T.Sakai セル計算対応
					ruikeiNinzu = 0;
//add end
					
					while(itrS42Bnp.hasNext()){
						s42BnpListBean = (S42BnpListBean) itrS42Bnp.next();

						if(row==0){
							if(gakkoCnt%3==0){
								// 高校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+1, tabCol[gakkoCnt] );
								workCell.setCellValue( s42GakkoListBean.getStrGakkomei() );

							}

							// 年度セット
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+2, tabCol[gakkoCnt] );
							workCell.setCellValue( s42GakkoListBean.getStrNendo()+"年度" );

							// 合計人数セット
							if(s42GakkoListBean.getIntNinzu()!=-999){
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+24, tabCol[gakkoCnt] );
								workCell.setCellValue( s42GakkoListBean.getIntNinzu() );
							}

							// 平均点セット
							if(s42GakkoListBean.getFloHeikin()!=-999.0){
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+25, tabCol[gakkoCnt] );
								workCell.setCellValue( s42GakkoListBean.getFloHeikin() );
							}

							// 平均偏差値セット
							if(s42GakkoListBean.getFloHensa()!=-999.0){
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+26, tabCol[gakkoCnt] );
								workCell.setCellValue( s42GakkoListBean.getFloHensa() );
							}
						}

						// 人数セット
						if(s42BnpListBean.getIntNinzu()!=-999){
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+row+5, tabCol[gakkoCnt]+1 );
							workCell.setCellValue( s42BnpListBean.getIntNinzu() );

							// *セット準備
							if( s42BnpListBean.getIntNinzu() > ninzu ){
								ninzu = s42BnpListBean.getIntNinzu();
								setRow = row;
							}
						}
//add 2004/10/27 T.Sakai セル計算対応
						// 累計人数セット
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+row+5, tabCol[gakkoCnt]+2 );
						if ( s42BnpListBean.getIntNinzu() != -999 ) {
							ruikeiNinzu = ruikeiNinzu + s42BnpListBean.getIntNinzu();
						}
						workCell.setCellValue( ruikeiNinzu );

						// 得点用のときは処理しない
						if (s42Item.getIntShubetsuFlg() == 1){
						} else{
							if (s42BnpListBean.getFloBnpMin()==60.0f) {
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+27, tabCol[gakkoCnt] );
								workCell.setCellValue( ruikeiNinzu );
							}
							if (s42BnpListBean.getFloBnpMin()==50.0f) {
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+28, tabCol[gakkoCnt] );
								workCell.setCellValue( ruikeiNinzu );
							}
						}
//add end
						
						row++;

						// *セット
						if(row >=19){
							if(setRow!=-1){
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+setRow+5, tabCol[gakkoCnt] );
								workCell.setCellValue("*");
							}
							ninzu=0;
							setRow=-1;
							break;
						}

					}

					gakkoCnt++;
					if(gakkoCnt>=12){
						gakkoCnt=0;
						hyouCnt++;
						if(hyouCnt>1){
							bolSheetCngFlg=true;
							hyouCnt=0;
						}
					}

					if(bolSheetCngFlg){
						//
						if((maxSheetIndex >= intMaxSheetSr)&&(itrS42Gakko.hasNext())){
							bolBookCngFlg = true;
						}

				
						if( bolBookCngFlg == true){
							boolean bolRet = false;
							// Excelファイル保存
							if(intBookCngCount==0){
								if(itr.hasNext() == true){
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
								}
								else{
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
								}
							}else{
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
							}

							if( bolRet == false ){
								return errfwrite;
							}
							intBookCngCount++;
						}
					}
				}

				if(gakkoCnt!=0){
					hyouCnt++;
					gakkoCnt=0;
					if(hyouCnt>1){
						bolSheetCngFlg=true;
						hyouCnt=0;
					}
				}

				if(bolSheetCngFlg){
					//
					if(maxSheetIndex >= intMaxSheetSr){
						bolBookCngFlg = true;
					}

				
					if( bolBookCngFlg == true){
						boolean bolRet = false;
						// Excelファイル保存
						if(intBookCngCount==0){
							if(itr.hasNext() == true){
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
							}
							else{
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
							}
						}else{
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
						}

						if( bolRet == false ){
							return errfwrite;
						}
						intBookCngCount++;
					}
				}

			}

			if( bolBookCngFlg == false){
				boolean bolRet = false;
				// Excelファイル保存
				if(intBookCngCount!=0){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}
			
		} catch(Exception e) {
			log.Err("S42_03","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S42_03","S42_03帳票作成終了","");
		return noerror;
	}

}
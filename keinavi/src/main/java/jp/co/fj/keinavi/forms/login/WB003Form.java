/*
 * 作成日: 2004/06/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.login;

import com.fjh.forms.ActionForm;



/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class WB003Form extends ActionForm {

	// 利用メニュー
	private String menu = null;
	// 部門コード
	private String sectorCD = null;
	// ＩＤ
	private String account = null;
	// 学校コード
	private String schoolCD = null;
	 
	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	/**
	 * @return
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @return
	 */
	public String getMenu() {
		return menu;
	}


	/**
	 * @param string
	 */
	public void setAccount(String string) {
		account = string;
	}

	/**
	 * @param string
	 */
	public void setMenu(String string) {
		menu = string;
	}


	/**
	 * @return
	 */
	public String getSchoolCD() {
		return schoolCD;
	}

	/**
	 * @param string
	 */
	public void setSchoolCD(String string) {
		schoolCD = string;
	}

	/**
	 * @return sectorCD を戻します。
	 */
	public String getSectorCD() {
		return sectorCD;
	}

	/**
	 * @param pSectorCD 設定する sectorCD。
	 */
	public void setSectorCD(String pSectorCD) {
		sectorCD = pSectorCD;
	}

}

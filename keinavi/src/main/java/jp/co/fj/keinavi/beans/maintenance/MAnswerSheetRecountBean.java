package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 受験届修正メニューにおいて
 * 再集計対象を登録するBean
 *
 * 2006.10.30	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class MAnswerSheetRecountBean extends DefaultBean {

	// 模試年度
	private String examYear;
	// 模試コード
	private String examCd;
	// 解答用紙番号
	private String answerSheetNo;
	// 変更後個人ID
	private String afterIndividualId;
	// 再集計対象リスト
	private final List targetList = new ArrayList();

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// 再集計対象を初期化
		initTargetList();

		// 再集計対象があるなら登録
		if (!targetList.isEmpty()) {
			registSubRecount();
			registRatingNumberRecount();
			targetList.clear();
		}
	}

	// 再集計対象を初期化する
	protected void initTargetList() throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().getQuery("m26"));
			ps.setString(1, examYear);
			ps.setString(2, examCd);
			ps.setString(3, answerSheetNo);
			ps.setString(4, afterIndividualId);
			ps.setString(5, afterIndividualId);
			ps.setString(6, examYear);
			ps.setString(7, examCd);
			ps.setString(8, answerSheetNo);
			rs = ps.executeQuery();
			while (rs.next()) {
				addTarget(examYear, examCd, rs.getInt(1), rs.getString(2));
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	// 再集計対象データとして追加する
	protected void addTarget(final String pExamYear,
			final String pExamCd, final int pGrade, final String pClassName) {
		targetList.add(new RecountData(pExamYear, pExamCd, pGrade, pClassName));
	}

	// 科目再集計対象を登録する
	private void registSubRecount() throws SQLException {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().getQuery("m08_1"));
			ps.setString(9, afterIndividualId);
			for (final Iterator ite = targetList.iterator(); ite.hasNext();) {
				final RecountData data = (RecountData) ite.next();
				ps.setString(1, data.getExamYear());
				ps.setString(2, data.getExamCd());
				ps.setInt(3, data.getGrade());
				ps.setString(4, data.getClassName());
				ps.setString(5, data.getExamYear());
				ps.setString(6, data.getExamCd());
				ps.setInt(7, data.getGrade());
				ps.setString(8, data.getClassName());
				ps.executeUpdate();
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	// 志望大学再集計対象を登録する
	private void registRatingNumberRecount() throws SQLException {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().getQuery("m08_2"));
			ps.setString(5, afterIndividualId);
			for (final Iterator ite = targetList.iterator(); ite.hasNext();) {
				final RecountData data = (RecountData) ite.next();
				ps.setInt(1, data.getGrade());
				ps.setString(2, data.getClassName());
				ps.setString(3, data.getExamYear());
				ps.setString(4, data.getExamCd());
				ps.setInt(6, data.getGrade());
				ps.setString(7, data.getClassName());
				ps.executeUpdate();
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	// 再集計対象データクラス
	public class RecountData {

		// 模試年度
		private final String examYear;
		// 模試コード
		private final String examCd;
		// 学年
		private final int grade;
		// クラス名
		private final String className;

		// コンストラクタ
		private RecountData(final String pExamYear,
				final String pExamCd,
				final int pGrade,
				final String pClassName) {
			examYear = pExamYear;
			examCd = pExamCd;
			grade = pGrade;
			className = pClassName;
		}

		/**
		 * @return className を戻します。
		 */
		public String getClassName() {
			return className;
		}

		/**
		 * @return examCd を戻します。
		 */
		public String getExamCd() {
			return examCd;
		}

		/**
		 * @return examYear を戻します。
		 */
		public String getExamYear() {
			return examYear;
		}

		/**
		 * @return grade を戻します。
		 */
		public int getGrade() {
			return grade;
		}

	}

	/**
	 * @param pAfterIndividualId 設定する afterIndividualId。
	 */
	public void setAfterIndividualId(String pAfterIndividualId) {
		afterIndividualId = pAfterIndividualId;
	}

	/**
	 * @param pExamCd 設定する examCd。
	 */
	public void setExamCd(String pExamCd) {
		examCd = pExamCd;
	}

	/**
	 * @param pExamYear 設定する examYear。
	 */
	public void setExamYear(String pExamYear) {
		examYear = pExamYear;
	}

	/**
	 * @param pAnswerSheetNo 設定する answerSheetNo。
	 */
	public void setAnswerSheetNo(String pAnswerSheetNo) {
		answerSheetNo = pAnswerSheetNo;
	}

}

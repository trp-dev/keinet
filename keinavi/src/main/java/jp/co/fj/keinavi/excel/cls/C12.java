/**
 * クラス成績分析−クラス成績概況
 * 出力する帳票の判断
 * 作成日: 2004/08/31
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.cls;
import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.cls.C12Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

public class C12 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean c12( C12Item c12Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {

		KNLog log = KNLog.getInstance(null,null,null);

		try{

			//C12Itemから各帳票を出力
			int ret = 0;
			if ((c12Item.getIntHyouFlg()==0)&&( c12Item.getIntJyuniFlg()==0 )) {
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
			    //throw new Exception("C12 ERROR : フラグ異常");
			    throw new IllegalStateException("C12 ERROR : フラグ異常");
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			}
			if( (c12Item.getIntHyouFlg()==1)&&(c12Item.getIntShiboFlg()==2) ){
				C12_01 exceledit = new C12_01();
				ret = exceledit.c12_01EditExcel( c12Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C12_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C12_01");
			}
			if( (c12Item.getIntHyouFlg()==1)&&(c12Item.getIntShiboFlg()==1) ){
				C12_02 exceledit = new C12_02();
				ret = exceledit.c12_02EditExcel( c12Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C12_02","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C12_02");
			}
					if( c12Item.getIntJyuniFlg()==1 && c12Item.getIntRankFlg()==1 ){
						C12_03 exceledit = new C12_03();
						ret = exceledit.C12_03EditExcel( c12Item, outfilelist, saveFlg, UserID );
						if (ret!=0) {
							log.Err("0"+ret+"C12_03","帳票作成エラー","");
							return false;
						}
						sheetLog.add("C12_03");
					}
					if( c12Item.getIntJyuniFlg()==1 && c12Item.getIntRankFlg()==2 ){
						//2019/09/06 QQ)Tanouchi 共通テスト対応 UPD START
						//C12_04 exceledit = new C12_04();
						//ret = exceledit.c12_04EditExcel( c12Item, outfilelist, saveFlg, UserID );
						C12_03 exceledit = new C12_03();
						ret = exceledit.C12_03EditExcel( c12Item, outfilelist, saveFlg, UserID );
						//2019/09/06 QQ)Tanouchi 共通テスト対応 UPD END
						if (ret!=0) {
							//2019/09/06 QQ)Tanouchi 共通テスト対応 UPD START
							//log.Err("0"+ret+"C12_04","帳票作成エラー","");
							log.Err("0"+ret+"C12_03","帳票作成エラー","");
							//2019/09/06 QQ)Tanouchi 共通テスト対応 UPD END
							return false;
						}
						//2019/09/06 QQ)Tanouchi 共通テスト対応 UPD START
						//sheetLog.add("C12_04");
						sheetLog.add("C12_03");
						//2019/09/06 QQ)Tanouchi 共通テスト対応 UPD END
					}

		} catch(Exception e) {
			log.Err("99C12","データセットエラー",e.toString());
			return false;
		}
		return true;
	}

}
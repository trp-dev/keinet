package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.excel.data.school.S22BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S22Item;
import jp.co.fj.keinavi.excel.data.school.S22ListBean;
import jp.co.fj.keinavi.excel.data.school.S22TotalListBean;
import jp.co.fj.keinavi.excel.school.S22;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 校内成績分析 - 過年度比較 - 偏差値分布
 *
 *
 * 2004.08.04	[新規作成]
 *
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 *
 * 2005.04.21	Yoshimoto KAWAI - Totec
 * 				新テスト対応
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class S22SheetBean extends AbstractSheetBean {

	// データクラス
	private final S22Item data = new S22Item();

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		final String[] code = getSubjectCDArray(); // 型・科目設定値
		final List graph = getSubjectGraphList(); // 型・科目グラフ表示対象
		final String[] years = getCompYearArray(); // 比較対象年度

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntSecuFlg(getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(getOutputType()); // 出力種別フラグ

		// 表
		switch (getIntFlag(IProfileItem.CHART_COMP_RATIO)) {
			case  1: data.setIntHyouFlg(0); data.setIntKoseihiFlg(1); break;
			case  2: data.setIntHyouFlg(0); data.setIntKoseihiFlg(2); break;
			case 11: data.setIntHyouFlg(1); data.setIntKoseihiFlg(1); break;
			case 12: data.setIntHyouFlg(1); data.setIntKoseihiFlg(2); break;
		}

		// 偏差値帯別度数分布グラフ
		switch (getIntFlag(IProfileItem.GRAPH_DIST)) {
			case  1: data.setIntBnpGraphFlg(0); data.setIntAxisFlg(1); break;
			case  2: data.setIntBnpGraphFlg(0); data.setIntAxisFlg(2); break;
			case 11: data.setIntBnpGraphFlg(1); data.setIntAxisFlg(1); break;
			case 12: data.setIntBnpGraphFlg(1); data.setIntAxisFlg(2); break;
		}

		// 偏差値帯別人数積み上げグラフ
		switch (getIntFlag(IProfileItem.GRAPH_BUILDUP)) {
			case  1: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(1); break;
			case  2: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(2); break;
			case  3: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(3); break;
			case 11: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(1); break;
			case 12: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(2); break;
			case 13: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(3); break;
		}

		// 偏差値帯別構成比グラフ
		switch (getIntFlag(IProfileItem.GRAPH_COMP_RATIO)) {
			case  1: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(1); break;
			case  2: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(2); break;
			case  3: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(3); break;
			case 11: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(1); break;
			case 12: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(2); break;
			case 13: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(3); break;
		}

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//		// 2019/07/26 QQ)Tanioka 共通テスト対応 ADD START
//		// 共通テスト英語認定試験CEFR取得状況
//		switch (super.getIntFlag(IProfileItem.OPTION_CHECK_BOX)) {
//			case  1: data.setIntCheckBoxFlg(0); data.setIntTargetCheckBoxFlg(1); break;
//			case  2: data.setIntCheckBoxFlg(0); data.setIntTargetCheckBoxFlg(2); break;
//			case 11: data.setIntCheckBoxFlg(1); data.setIntTargetCheckBoxFlg(1); break;
//			case 12: data.setIntCheckBoxFlg(1); data.setIntTargetCheckBoxFlg(2); break;
//		}
//		// 2019/07/26 QQ)Tanioka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		// ワークテーブルのセットアップ
		insertIntoExistsExam();
		insertIntoSubCDTrans(new ExamData[]{exam});

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD START
//		PreparedStatement ps4 = null;
//		PreparedStatement ps5 = null;
//// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD START
//		ResultSet rs4 = null;
//		ResultSet rs5 = null;
//// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
		try {
			List c1 = new LinkedList(); // 型の入れ物
			List c2 = new LinkedList(); // 科目の入れ物

			// データリストの取得
			Query query1 = QueryLoader.getInstance().load("s22_1");
			query1.setStringArray(1, code); // 型・科目コード

			ps1 = conn.prepareStatement(query1.toString());
			ps1.setString(1, exam.getExamYear()); // 対象年度
			ps1.setString(2, exam.getExamCD()); // 対象模試

			// 偏差値分布データリスト
			//Query query2 = QueryLoader.getInstance().load("s22_2");
			Query query2 = super.getQueryWithDeviationZone("s22_2");
			query2.setStringArray(1, years); // 比較対象年度

			ps2 = conn.prepareStatement(query2.toString());
			ps2.setString(1, exam.getExamCD()); // 模試コード
			ps2.setString(3, profile.getBundleCD()); // 一括コード

			// 集計データリスト
			Query query3 = QueryLoader.getInstance().load("s22_3");
			query3.setStringArray(1, years); // 比較対象年度

			ps3 = conn.prepareStatement(query3.toString());
			ps3.setString(2, profile.getBundleCD()); // 一括コード
			ps3.setString(3, exam.getExamCD()); // 模試コード

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/08/20 共通テスト対応 ADD START
//			// 比較対象年度並び替え
//			Arrays.sort(years);
//			List list = Arrays.asList(years);
//			Collections.reverse(list);
//
//			// 認定試験別・合計CEFR取得状況リスト
//			Query query4 = QueryLoader.getInstance().load("s22_4");
//			ps4 = conn.prepareStatement(query4.toString());
//
//			int cnt = 1;
//
//			// select句設定 年度は固定
//                        Iterator itr = list.listIterator();
//                        while(cnt <= 4) {
//                            if(!itr.hasNext()) {
//                                ps4.setString(cnt, ""); // 対象年度
//                                cnt++;
//                            }else {
//                                String year = String.valueOf(itr.next());
//                                if (!exam.getExamYear().equals(year)) {
//                                        ps4.setString(cnt, year); // 対象年度
//                                        cnt++;
//                                }
//                            }
//                        }
//
//			ps4.setString(5, exam.getExamCD()); // 対象模試
//			cnt++;
//			ps4.setString(6, profile.getBundleCD()); // 一括コード
//			cnt++;
//			ps4.setString(7, exam.getExamYear()); // 対象年度
//			cnt++;
//
//			// 過年度設定
//                        itr = list.listIterator();
//			while(cnt <= 15) {
//                            if(!itr.hasNext()) {
//                                ps4.setString(cnt, profile.getBundleCD()); // 一括コード
//                                cnt++;
//                                ps4.setString(cnt, ""); // 対象年度
//                                cnt++;
//                            }else {
//				String year = String.valueOf(itr.next());
//
//				if (!exam.getExamYear().equals(year)) {
//					ps4.setString(cnt, profile.getBundleCD()); // 一括コード
//					cnt++;
//					ps4.setString(cnt, year); // 対象年度
//					cnt++;
//				}
//                            }
//			}
//
//			// select句設定 年度は固定 (合計行)
//                        itr = list.listIterator();
//                        while(cnt <= 19) {
//                            if(!itr.hasNext()) {
//                                ps4.setString(cnt, ""); // 対象年度
//                                cnt++;
//                            }else {
//                                String year = String.valueOf(itr.next());
//
//                                if (!exam.getExamYear().equals(year)) {
//                                        ps4.setString(cnt, year); // 対象年度
//                                        cnt++;
//                                }
//                            }
//                        }
//
//                        ps4.setString(20, profile.getBundleCD()); // 一括コード
//                        cnt++;
//                        ps4.setString(21, exam.getExamYear()); // 対象年度
//                        cnt++;
//
//			// 合計行設定
//			itr = list.listIterator();
//			while(cnt <= 29) {
//                            if(!itr.hasNext()) {
//                                ps4.setString(cnt, profile.getBundleCD()); // 一括コード
//                                cnt++;
//                                ps4.setString(cnt, ""); // 対象年度
//                                cnt++;
//                            }else {
//                                String year = String.valueOf(itr.next());
//
//                                if (!exam.getExamYear().equals(year)) {
//                                    ps4.setString(cnt, profile.getBundleCD()); // 一括コード
//                                    cnt++;
//                                    ps4.setString(cnt, year); // 対象年度
//                                    cnt++;
//                                }
//                            }
//			}
//
//			// CEFR取得状況リスト
//			Query query5 = QueryLoader.getInstance().load("s22_5");
//			ps5 = conn.prepareStatement(query5.toString());
//
//			// 過年度設定
//			cnt = 1;
//
//                        // select句設定 年度は固定
//                        itr = list.listIterator();
//                        while(cnt <= 4) {
//                            if(!itr.hasNext()) {
//                                ps5.setString(cnt, ""); // 対象年度
//                                cnt++;
//                            }else {
//                                String year = String.valueOf(itr.next());
//                                if (!exam.getExamYear().equals(year)) {
//                                        ps5.setString(cnt, year); // 対象年度
//                                        cnt++;
//                                }
//                            }
//                        }
//
//                        ps5.setString(5, exam.getExamCD()); // 対象模試
//                        cnt++;
//                        ps5.setString(6, profile.getBundleCD()); // 一括コード
//                        cnt++;
//                        ps5.setString(7, exam.getExamYear()); // 対象年度
//                        cnt++;
//
//                        itr = list.listIterator();
//			while(cnt <= 15) {
//			    if(!itr.hasNext()) {
//                                ps5.setString(cnt, profile.getBundleCD()); // 一括コード
//                                cnt++;
//                                ps5.setString(cnt, ""); // 対象年度
//                                cnt++;
//			    }else {
//				String year = String.valueOf(itr.next());
//
//				if (!exam.getExamYear().equals(year)) {
//					ps5.setString(cnt, profile.getBundleCD()); // 一括コード
//					cnt++;
//					ps5.setString(cnt, year); // 対象年度
//					cnt++;
//				}
//			    }
//			}
//
//                        // select句設定 年度は固定 (合計行)
//                        itr = list.listIterator();
//                        while(cnt <= 19) {
//                            if(!itr.hasNext()) {
//                                ps5.setString(cnt, ""); // 対象年度
//                                cnt++;
//                            }else {
//                                String year = String.valueOf(itr.next());
//
//                                if (!exam.getExamYear().equals(year)) {
//                                        ps5.setString(cnt, year); // 対象年度
//                                        cnt++;
//                                }
//                            }
//                        }
//
//                        ps5.setString(20, profile.getBundleCD()); // 一括コード
//                        cnt++;
//                        ps5.setString(21, exam.getExamYear()); // 対象年度
//                        cnt++;
//
//			// 合計行設定
//			itr = list.listIterator();
//			while(cnt <= 29) {
//                            if(!itr.hasNext()) {
//                                ps5.setString(cnt, profile.getBundleCD()); // 一括コード
//                                cnt++;
//                                ps5.setString(cnt, ""); // 対象年度
//                                cnt++;
//                            }else {
//                                String year = String.valueOf(itr.next());
//
//                                if (!exam.getExamYear().equals(year)) {
//                                    ps5.setString(cnt, profile.getBundleCD()); // 一括コード
//                                    cnt++;
//                                    ps5.setString(cnt, year); // 対象年度
//                                    cnt++;
//                                }
//                            }
//			}
//// 2019/08/20 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

			rs1 = ps1.executeQuery();

			while (rs1.next()) {
				// データBean
				S22ListBean bean = new S22ListBean();
				bean.setStrKmkCd(rs1.getString(1));
				bean.setStrKmkmei(rs1.getString(2));
				bean.setIntDispKmkFlg(graph.contains(rs1.getString(1)) ? 1 : 0);
				bean.setStrHaitenKmk(rs1.getString(3));

				if (rs1.getInt(1) >= 7000) c1.add(bean); // 型
				else c2.add(bean); // 科目

				// 科目コード
				ps2.setString(2, bean.getStrKmkCd());
				ps3.setString(1, bean.getStrKmkCd());

				rs2 = ps2.executeQuery();
				while (rs2.next()) {
					S22BnpListBean b = new S22BnpListBean();
					b.setStrNendo(rs2.getString(1));
					b.setFloBnpMax(rs2.getFloat(2));
					b.setFloBnpMin(rs2.getFloat(3));
					b.setIntNinzu(rs2.getInt(4));
					b.setFloKoseihi(rs2.getFloat(5));
					bean.getS22BnpList().add(b);
				}
				rs2.close();

				rs3 = ps3.executeQuery();
				while (rs3.next()) {
					S22TotalListBean t = new S22TotalListBean();
					t.setStrNendo(rs3.getString(1));
					t.setIntTotalNinzu(rs3.getInt(2));
					t.setFloHeikin(rs3.getFloat(3));
					t.setFloHensa(rs3.getFloat(4));
					bean.getS22TotalList().add(t);
				}
				rs3.close();
			}

			// 型・科目の順番に詰める
			data.getS22List().addAll(c1);
			data.getS22List().addAll(c2);
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD START
//			rs4 = ps4.executeQuery();
//			while (rs4.next()) {
//				S22PtCefrAcqStatusListBean pcaslb = new S22PtCefrAcqStatusListBean();
//				pcaslb.setExamyear(rs4.getString(1));
//				pcaslb.setEngptcd(rs4.getString(2));
//				pcaslb.setEngptname_abbr(rs4.getString(3));
//				pcaslb.setLevelflg(rs4.getString(4));
//				pcaslb.setEngptinfo_sort(rs4.getInt(5));
//				pcaslb.setEngptlevelcd(rs4.getString(6));
//				pcaslb.setEngptlevelname_abbr(rs4.getString(7));
//				pcaslb.setEngptdetailinfo_sort(rs4.getInt(8));
//				pcaslb.setCefrlevelcd(rs4.getString(9));
//				pcaslb.setCerflevelname_abbr(rs4.getString(10));
//				pcaslb.setCefrinfo_sort(rs4.getInt(11));
//				pcaslb.setNumbers1(rs4.getInt(12));
//				pcaslb.setCompratio1(rs4.getFloat(13));
//				pcaslb.setFlg1(rs4.getString(14));
//				pcaslb.setExamyear2(rs4.getString(15));
//				pcaslb.setNumbers2(rs4.getInt(16));
//				pcaslb.setCompratio2(rs4.getFloat(17));
//				pcaslb.setExamyear3(rs4.getString(18));
//				pcaslb.setNumbers3(rs4.getInt(19));
//				pcaslb.setCompratio3(rs4.getFloat(20));
//				pcaslb.setExamyear4(rs4.getString(21));
//				pcaslb.setNumbers4(rs4.getInt(22));
//				pcaslb.setCompratio4(rs4.getFloat(23));
//				pcaslb.setExamyear5(rs4.getString(24));
//				pcaslb.setNumbers5(rs4.getInt(25));
//				pcaslb.setCompratio5(rs4.getFloat(26));
//				data.getS22PtCefrAcqStatusList().add(pcaslb);
//			}
//
//			rs5 = ps5.executeQuery();
//			while (rs5.next()) {
//				S22CefrAcqStatusListBean caslb = new S22CefrAcqStatusListBean();
//				caslb.setExamyear1(rs5.getString(1));
//				caslb.setExamcd(rs5.getString(2));
//				caslb.setCefrlevelcd(rs5.getString(3));
//				caslb.setCerflevelname(rs5.getString(4));
//				caslb.setCefrinfo_sort(rs5.getInt(5));
//				caslb.setNumbers1(rs5.getInt(6));
//				caslb.setCompratio1(rs5.getFloat(7));
//				caslb.setFlg1(rs5.getString(8));
//				caslb.setExamyear2(rs5.getString(9));
//				caslb.setNumbers2(rs5.getInt(10));
//				caslb.setCompratio2(rs5.getFloat(11));
//				caslb.setExamyear3(rs5.getString(12));
//				caslb.setNumbers3(rs5.getInt(13));
//				caslb.setCompratio3(rs5.getFloat(14));
//				caslb.setExamyear4(rs5.getString(15));
//				caslb.setNumbers4(rs5.getInt(16));
//				caslb.setCompratio4(rs5.getFloat(17));
//				caslb.setExamyear5(rs5.getString(18));
//				caslb.setNumbers5(rs5.getInt(19));
//				caslb.setCompratio5(rs5.getFloat(20));
//				data.getS22CefrAcqStatusList().add(caslb);
//			}
//// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(rs3);
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD START
//			DbUtils.closeQuietly(rs4);
//			DbUtils.closeQuietly(rs5);
//// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(ps3);
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD START
//			DbUtils.closeQuietly(ps4);
//			DbUtils.closeQuietly(ps5);
//// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S22_01");
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_PREV_DEV;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S22().s22(data, outfileList, getAction(), sessionKey,sheetLog);
	}

}

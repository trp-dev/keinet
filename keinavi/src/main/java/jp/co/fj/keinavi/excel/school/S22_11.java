/**
 * 校内成績分析−過年度比較　偏差値分布
 * 	Excelファイル編集
 * 作成日: 2019/08/20
 * @author      QQ)H.Yamasaki
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.cm.CefrCommon;
import jp.co.fj.keinavi.excel.cm.CefrCommon.SIDE;
import jp.co.fj.keinavi.excel.data.school.S22CefrAcqStatusListBean;
import jp.co.fj.keinavi.excel.data.school.S22Item;
import jp.co.fj.keinavi.excel.data.school.S22PtCefrAcqStatusListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S22_11 {

	private final int noerror = 0; // 正常終了
	private final int errfread = 1; // ファイルreadエラー
	private final int errfwrite = 2; // ファイルwriteエラー
	private final int errfdata = 3; // データ設定エラー

	private CM cm = new CM(); // 共通関数用クラス インスタンス
	private CefrCommon cefrCm = null;

	private String masterfile = "S22_11"; // ファイル名

	private static final int MAX_ROW_COUNT = 8; // シート内最大行数
	private static final int MAX_YEAR_COUNT = 5; // 年度表示最大数

	private final int MAX_COL_COUNT = 14; // 表内最大CEFR列数
	private final String TOTAL_EXAM_NAME = "全試験";
	private final String TOTAL_PERSON_NAME = "延人数";

	private int intSecuFlg = 0; // セキュリティスタンプフラグ
	private String strGakkomei = ""; // 学校名
	private String strMshmei = ""; // 模試名
	private String strMshDate = ""; // 模試実施基準日

	private String[] nendo = null; // 年度

	// Enum(Excel座標 列,行)
	public enum CURSOL {
		SECURITY_STAMP(29, 0),
		SCHOOL_NAME(0, 3),
		ALL_EXAM_TABLE_COPY_START(0, 1),
		ALL_EXAM_TABLE_COPY_END(15, 7),
		EXAM_TABLE_COPY_A_START(0, 1),
		EXAM_TABLE_COPY_A_END(15, 7),
		EXAM_TABLE_COPY_B_START(1, 1),
		EXAM_TABLE_COPY_B_END(15, 7),
		EXAM_TABLE_PASTE_START(0, 7),
		EXAM_TABLE_LINE_A1_START(1, 7),
		EXAM_TABLE_LINE_A2_START(1, 16),
		EXAM_TABLE_LINE_A3_START(1, 25),
		EXAM_TABLE_LINE_A4_START(1, 34),
		EXAM_TABLE_LINE_A5_START(1, 43),
		EXAM_TABLE_LINE_A6_START(1, 52),
		EXAM_TABLE_LINE_A7_START(1, 61),
		EXAM_TABLE_LINE_A8_START(1, 70),
		EXAM_TABLE_LINE_B1_START(16, 7),
		EXAM_TABLE_LINE_B2_START(16, 16),
		EXAM_TABLE_LINE_B3_START(16, 25),
		EXAM_TABLE_LINE_B4_START(16, 34),
		EXAM_TABLE_LINE_B5_START(16, 43),
		EXAM_TABLE_LINE_B6_START(16, 52),
		EXAM_TABLE_LINE_B7_START(16, 61),
		EXAM_TABLE_LINE_B8_START(16, 70),
		TERMINAL(0,0);

		private final int col;
		private final int row;

		private CURSOL(int col, int row) {
			this.col = col;
			this.row = row;
		}

		public int getCol() {
			return this.col;
		}

		public int getRow() {
			return this.row;
		}
	}

/*
 * 	Excel編集メイン
 * 		S22Item s22Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s22_11EditExcel(S22Item s22Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("S22_11","S22_11帳票作成開始","");

		HSSFWorkbook workbook = null;

		// マスタExcel読み込み
		workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
		if( workbook==null ){
			return errfread;
		}

		try {

			// Excel認定試験CEFR取得状況共通関数インスタンス生成
			cefrCm = new CefrCommon(workbook);

			// シート内最大行数
			cefrCm.setMaxRowCount(MAX_ROW_COUNT);
			// 年度表示最大数
			cefrCm.setMaxYearCount(MAX_YEAR_COUNT);
			// テンプレートシート指定
			cefrCm.setTemplateWorkbook();

			// セキュリティスタンプフラグ
			this.intSecuFlg = s22Item.getIntSecuFlg();
			// 学校名
			this.strGakkomei = s22Item.getStrGakkomei();
			// 模試名
			this.strMshmei = s22Item.getStrMshmei();
			// 模試実施基準日
			this.strMshDate = s22Item.getStrMshDate();
			// 自校受験者がいる試験を対象にするフラグ
			cefrCm.setIntTargetCheckBoxFlg(s22Item.getIntTargetCheckBoxFlg());

			// 新ページ作成処理
			this.newCreateSheet(cm);

			// CEFR取得状況データセット
			ArrayList s22CefrAcqStatusList = s22Item.getS22CefrAcqStatusList();
			// 認定試験別・合計CEFR取得状況データセット
			ArrayList s22PtCefrAcqStatusList = s22Item.getS22PtCefrAcqStatusList();

			if (s22CefrAcqStatusList.size() > 0
				|| s22PtCefrAcqStatusList.size() > 0) {

				// CEFR取得状況枠設定
				this.copyCefrAcqStatusFrame(cm);
				// CEFR取得状況表年度設定
				this.setCefrNendo(s22CefrAcqStatusList, cm);
				// CEFR取得状況表試験名
				this.setCefrExamName(cm);
				// CEFR取得状況表データ設定
				this.setCefrData(s22CefrAcqStatusList, cm);

				// 認定試験別・合計CEFR取得状況表作成
				this.setPtCefrAcqStatus(s22PtCefrAcqStatusList, cm);

				// テンプレートシート削除
				if( workbook.getSheetIndex(CefrCommon.templateSheetName) >= 0){
					workbook.removeSheetAt(workbook.getSheetIndex(CefrCommon.templateSheetName));
				}

				//2019/09/25 QQ)Oosaki アクティブシートを選択 ADD START
                                workbook.getSheetAt(1).setSelected(true);
                                //2019/09/25 QQ)Oosaki アクティブシートを選択 ADD END

				// Excelファイル保存
				boolean bolRet = false;
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, cefrCm.getWorkbook(), UserID, 0, masterfile, cefrCm.getMaxSheetIndex());

				if( bolRet == false ){
					return errfwrite;
				}
			}
		} catch(Exception e) {
			log.Err("S22_11","データセットエラー",e.toString());
			e.printStackTrace();
			return errfdata;
		}

		log.Ep("S22_11","S22_11帳票作成終了","");
		return noerror;
	}

	/**
	 * <DL>
	 * <DT>全試験　表貼り付け
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 */
	public void copyCefrAcqStatusFrame(CM cm) {

		// テンプレートカーソル位置設定
		int tempCurColS = CURSOL.ALL_EXAM_TABLE_COPY_START.getCol();
		int tempCurRowS = CURSOL.ALL_EXAM_TABLE_COPY_START.getRow();
		int tempCurColE = CURSOL.ALL_EXAM_TABLE_COPY_END.getCol();
		int tempCurRowE = CURSOL.ALL_EXAM_TABLE_COPY_END.getRow();

		// 貼り付けカーソル位置
		int pasteCurCol = CURSOL.EXAM_TABLE_PASTE_START.getCol();
		int pasteCurRow = CURSOL.EXAM_TABLE_PASTE_START.getRow();

		// 表テンプレートコピー
		cm.copyCellArea(cefrCm.getWorkSheet(), cefrCm.getTemplateWorkbook(), tempCurColS, tempCurRowS, tempCurColE, tempCurRowE, pasteCurCol, pasteCurRow);
	}

	/**
	 * <DL>
	 * <DT>CEFR取得状況表試験名設定
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 */
	public void setCefrExamName(CM cm) {

		// カーソル位置初期化
		this.setInitCursolPosition();

		cefrCm.setTableName(cm, this.TOTAL_EXAM_NAME);
	}

	/**
	 * <DL>
	 * <DT>CEFR取得状況表年度設定
	 * </DL>
	 * @param list
	 * @param cm Excel共通関数インスタンス
	 */
	public void setCefrNendo(ArrayList list, CM cm) {

		Iterator itrList = list.iterator();
		while( itrList.hasNext() ) {

			S22CefrAcqStatusListBean bean = (S22CefrAcqStatusListBean)itrList.next();

			// カーソル位置指定
			cefrCm.setCurCol(0);
			cefrCm.setCurRow(9);

			if (StringUtils.isNotEmpty(bean.getExamyear1())) {
				cefrCm.setCellValueAddRowCountUp(cm, bean.getExamyear1() + "年度");
			}
			if (StringUtils.isNotEmpty(bean.getExamyear2())) {
				cefrCm.setCellValueAddRowCountUp(cm, bean.getExamyear2() + "年度");
			}
			if (StringUtils.isNotEmpty(bean.getExamyear3())) {
				cefrCm.setCellValueAddRowCountUp(cm, bean.getExamyear3() + "年度");
			}
			if (StringUtils.isNotEmpty(bean.getExamyear4())) {
				cefrCm.setCellValueAddRowCountUp(cm, bean.getExamyear4() + "年度");
			}
			if (StringUtils.isNotEmpty(bean.getExamyear5())) {
				cefrCm.setCellValueAddRowCountUp(cm, bean.getExamyear5() + "年度");
			}
		}
	}

	/**
	 * <DL>
	 * <DT>CEFR取得状況表データ設定
	 * </DL>
	 * @param list
	 * @param cm Excel共通関数インスタンス
	 */
	public void setCefrData(ArrayList list, CM cm) {

		if (list.size() > 0) {

			// カーソル位置初期化
			this.setInitCursolPosition();
			cefrCm.setCurCol(cefrCm.getInitialCurCol() + 1);

			Iterator itrList = list.iterator();
			while( itrList.hasNext() ) {

				S22CefrAcqStatusListBean bean = (S22CefrAcqStatusListBean)itrList.next();

				// 表内最大CEFR列数を超える場合、処理中断
				if (cefrCm.getCurCol() - cefrCm.getInitialCurCol() > this.MAX_COL_COUNT
					&& !cefrCm.totalCefrCd.equals(bean.getCefrlevelcd())) {
					continue;
				}

				// CEFRレベル　タイトル
				this.setTableCefrName(cm, bean.getCerflevelname());

				// 2019/10/10 QQ)Ooseto 共通テスト対応 UPD START
				// CEFRレベル別　受験者数
				//cefrCm.setCerfNumber2(cm, bean.getCefrlevelcd(), bean.getNumbers1(), bean.getCompratio1(), bean.getFlg1());
				//cefrCm.setCerfNumber2(cm, bean.getCefrlevelcd(), bean.getNumbers2(), bean.getCompratio2(), bean.getFlg1());
				//cefrCm.setCerfNumber2(cm, bean.getCefrlevelcd(), bean.getNumbers3(), bean.getCompratio3(), bean.getFlg1());
				//cefrCm.setCerfNumber2(cm, bean.getCefrlevelcd(), bean.getNumbers4(), bean.getCompratio4(), bean.getFlg1());
				//cefrCm.setCerfNumber2(cm, bean.getCefrlevelcd(), bean.getNumbers5(), bean.getCompratio5(), bean.getFlg1());
                                cefrCm.setCerfNumber(cm, bean.getCefrlevelcd(), bean.getNumbers1(), bean.getCompratio1(), bean.getFlg1());
                                cefrCm.setCerfNumber(cm, bean.getCefrlevelcd(), bean.getNumbers2(), bean.getCompratio2(), bean.getFlg1());
                                cefrCm.setCerfNumber(cm, bean.getCefrlevelcd(), bean.getNumbers3(), bean.getCompratio3(), bean.getFlg1());
                                cefrCm.setCerfNumber(cm, bean.getCefrlevelcd(), bean.getNumbers4(), bean.getCompratio4(), bean.getFlg1());
                                cefrCm.setCerfNumber(cm, bean.getCefrlevelcd(), bean.getNumbers5(), bean.getCompratio5(), bean.getFlg1());

				// カーソル位置指定
				cefrCm.setCurCol(cefrCm.getCurCol() + 1);
				cefrCm.setCurRow(cefrCm.getInitialCurRow() + 2);

				// CEFRレベル別　構成比
				//cefrCm.setCerfCompratio2(cm, bean.getCefrlevelcd(), bean.getNumbers1(), bean.getCompratio1(), bean.getFlg1());
				//cefrCm.setCerfCompratio2(cm, bean.getCefrlevelcd(), bean.getNumbers2(), bean.getCompratio2(), bean.getFlg1());
				//cefrCm.setCerfCompratio2(cm, bean.getCefrlevelcd(), bean.getNumbers3(), bean.getCompratio3(), bean.getFlg1());
				//cefrCm.setCerfCompratio2(cm, bean.getCefrlevelcd(), bean.getNumbers4(), bean.getCompratio4(), bean.getFlg1());
				//cefrCm.setCerfCompratio2(cm, bean.getCefrlevelcd(), bean.getNumbers5(), bean.getCompratio5(), bean.getFlg1());
                                cefrCm.setCerfCompratio(cm, bean.getCefrlevelcd(), bean.getNumbers1(), bean.getCompratio1(), bean.getFlg1());
                                cefrCm.setCerfCompratio(cm, bean.getCefrlevelcd(), bean.getNumbers2(), bean.getCompratio2(), bean.getFlg1());
                                cefrCm.setCerfCompratio(cm, bean.getCefrlevelcd(), bean.getNumbers3(), bean.getCompratio3(), bean.getFlg1());
                                cefrCm.setCerfCompratio(cm, bean.getCefrlevelcd(), bean.getNumbers4(), bean.getCompratio4(), bean.getFlg1());
                                cefrCm.setCerfCompratio(cm, bean.getCefrlevelcd(), bean.getNumbers5(), bean.getCompratio5(), bean.getFlg1());
				// 2019/10/10 QQ)Ooseto 共通テスト対応 UPD END

				// カーソル位置指定
				cefrCm.setCurCol(cefrCm.getCurCol() + 1);
			}
		}
	}

	/**
	 * <DL>
	 * <DT>カーソル位置初期設定
	 * </DL>
	 */
	public void setInitCursolPosition() {

		// 行位置の決定
		switch(cefrCm.getMaxRowIndex()) {
			case 0:
				cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A1_START.getRow());
				break;
			case 1:
				cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A2_START.getRow());
				break;
			case 2:
				cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A3_START.getRow());
				break;
			case 3:
				cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A4_START.getRow());
				break;
			case 4:
				cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A5_START.getRow());
				break;
			case 5:
				cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A6_START.getRow());
				break;
			case 6:
				cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A7_START.getRow());
				break;
			case 7:
				cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A8_START.getRow());
				break;
		}

		// 列位置の決定
		if (CefrCommon.SIDE.A.equals(cefrCm.getSide())) {
			cefrCm.setInitialCurCol(CURSOL.EXAM_TABLE_LINE_A1_START.getCol());
		} else {
			cefrCm.setInitialCurCol(CURSOL.EXAM_TABLE_LINE_B1_START.getCol());
		}

		// カーソル位置指定
		cefrCm.setCurCol(cefrCm.getInitialCurCol());
		cefrCm.setCurRow(cefrCm.getInitialCurRow());
	}

	/**
	 * <DL>
	 * <DT>CEFRレベル　タイトル設定
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 * @param str CEFRレベル　タイトル
	 */
	public void setTableCefrName(CM cm, String str) {
		// カーソル位置指定
		cefrCm.setCurRow(cefrCm.getInitialCurRow() + 1);

		if (StringUtils.isNotEmpty(str)) {
		    // 2019/11/07 QQ)Ooseto 共通テスト対応 UPD START
		    cefrCm.setCellValueAddRowCountUp(cm, str);
		    //cefrCm.setCellValueAddRowCountUp(cm, str.trim());
		    // 2019/11/07 QQ)Ooseto 共通テスト対応 UPD END
		} else {
			cefrCm.setCurRow(cefrCm.getCurRow() + 1);
		}
	}

	/**
	 * <DL>
	 * <DT>年度配列作成
	 * <DD>DB検索結果全件を調べ、年度を抽出し配列に格納する
	 * </DL>
	 * @param list
	 */
	public void createNendoArray(ArrayList list) {

		nendo = new String[cefrCm.getMaxYearCount()];

		Iterator itrList = list.iterator();
		while( itrList.hasNext() ) {

			S22PtCefrAcqStatusListBean bean = (S22PtCefrAcqStatusListBean)itrList.next();

			if (StringUtils.isNotEmpty(bean.getExamyear())
				&& StringUtils.isEmpty(nendo[0])) {
				nendo[0] = bean.getExamyear();
			}
			if (StringUtils.isNotEmpty(bean.getExamyear2())
				&& StringUtils.isEmpty(nendo[1])) {
				nendo[1] = bean.getExamyear2();
			}
			if (StringUtils.isNotEmpty(bean.getExamyear3())
				&& StringUtils.isEmpty(nendo[2])) {
				nendo[2] = bean.getExamyear3();
			}
			if (StringUtils.isNotEmpty(bean.getExamyear4())
				&& StringUtils.isEmpty(nendo[3])) {
				nendo[3] = bean.getExamyear4();
			}
			if (StringUtils.isNotEmpty(bean.getExamyear5())
				&& StringUtils.isEmpty(nendo[4])) {
				nendo[4] = bean.getExamyear5();
			}
		}
	}

	/**
	 * <DL>
	 * <DT>年度設定
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 */
	public void setTableNendo(CM cm) {
		// カーソル位置指定
		cefrCm.setCurCol(cefrCm.getInitialCurCol() - 1);
		cefrCm.setCurRow(cefrCm.getInitialCurRow() + 2);

		if (StringUtils.isNotEmpty(this.nendo[0])) {
			cefrCm.setCellValueAddRowCountUp(cm, this.nendo[0] + "年度");
		} else {
			cefrCm.setCurRow(cefrCm.getCurRow() + 1);
		}
		if (StringUtils.isNotEmpty(this.nendo[1])) {
			cefrCm.setCellValueAddRowCountUp(cm, this.nendo[1] + "年度");
		} else {
			cefrCm.setCurRow(cefrCm.getCurRow() + 1);
		}
		if (StringUtils.isNotEmpty(this.nendo[2])) {
			cefrCm.setCellValueAddRowCountUp(cm, this.nendo[2] + "年度");
		} else {
			cefrCm.setCurRow(cefrCm.getCurRow() + 1);
		}
		if (StringUtils.isNotEmpty(this.nendo[3])) {
			cefrCm.setCellValueAddRowCountUp(cm, this.nendo[3] + "年度");
		} else {
			cefrCm.setCurRow(cefrCm.getCurRow() + 1);
		}
		if (StringUtils.isNotEmpty(this.nendo[4])) {
			cefrCm.setCellValueAddRowCountUp(cm, this.nendo[4] + "年度");
		} else {
			cefrCm.setCurRow(cefrCm.getCurRow() + 1);
		}
	}

	/**
	 * <DL>
	 * <DT>改表キー作成
	 * <DD>参加試験コード + ":" + 参加試験レベルコード
	 * </DL>
	 * @param bean DB検索結果Bean
	 * @return 生成キー
	 */
	private String createKeyTable(S22PtCefrAcqStatusListBean bean) {
		return bean.getEngptcd() + ":" + bean.getEngptlevelcd();
	}

	/**
	 * <DL>
	 * <DT>改行キー作成
	 * <DD>参加試験コード
	 * </DL>
	 * @param bean DB検索結果Bean
	 * @return 生成キー
	 */
	private String createKeyRow(S22PtCefrAcqStatusListBean bean) {
		return bean.getEngptcd();
	}

	/**
	 * <DL>
	 * <DT>表テンプレートコピー処理
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 */
	private void copyTableTemplate(CM cm) {

		// テンプレートカーソル位置設定
		int tempCurColS = CURSOL.EXAM_TABLE_COPY_A_START.getCol();
		int tempCurRowS = CURSOL.EXAM_TABLE_COPY_A_START.getRow();
		int tempCurColE = CURSOL.EXAM_TABLE_COPY_A_END.getCol();
		int tempCurRowE = CURSOL.EXAM_TABLE_COPY_A_END.getRow();
		if (!SIDE.A.equals(cefrCm.getSide())) {
			tempCurColS = CURSOL.EXAM_TABLE_COPY_B_START.getCol();
			tempCurColE = CURSOL.EXAM_TABLE_COPY_B_END.getCol();
		}

		// 貼り付けカーソル位置
		int pasteCurCol = cefrCm.getCurCol();
		int pasteCurRow = cefrCm.getCurRow();
		if (SIDE.A.equals(cefrCm.getSide())) {
			pasteCurCol--;
		}

		// 表テンプレートコピー
		cm.copyCellArea(cefrCm.getWorkSheet(), cefrCm.getTemplateWorkbook(), tempCurColS, tempCurRowS, tempCurColE, tempCurRowE, pasteCurCol, pasteCurRow);
	}

	/**
	 * <DL>
	 * <DT>認定試験別・合計CEFR取得状況表作成
	 * </DL>
	 * @param list
	 * @param cm Excel共通関数インスタンス
	 */
	public void setPtCefrAcqStatus(ArrayList list, CM cm) {

		String nowKeyTable = "";
		String nowKeyRow = "";
		boolean bolTableHeaderFlg = false;
		boolean bolTableDataFlg = false;

		if (list.size() > 0) {

			// 年度配列作成
			this.createNendoArray(list);

			Iterator itrList = list.iterator();
			while( itrList.hasNext() ) {

				S22PtCefrAcqStatusListBean bean = (S22PtCefrAcqStatusListBean)itrList.next();

				// 改表キーによる改表チェック
				nowKeyTable = this.createKeyTable(bean);
				boolean retCheckKeyTable = cefrCm.checkKeyTable(nowKeyTable);

                                // データが存在しない英語認定試験は表示しない
                                // ※当年度自校受験者がいる試験を対象にする場合のみ
                                boolean viewFlag = true;
                                if(bean.getFlg1().equals("0")) {
                                    viewFlag = false;
                                }
                                if(!cefrCm.checkIntTargetCheckBoxFlg()) {
                                    viewFlag = true;
                                }
// 2019/10/08 QQ)Tanioka データが無くてもCEFRレベルを表示するよう修正 ADD START
                                if(bolTableHeaderFlg && retCheckKeyTable) {
                                        viewFlag = true;
                                }
// 2019/10/08 QQ)Tanioka データが無くてもCEFRレベルを表示するよう修正 ADD END

				if (!retCheckKeyTable && viewFlag) {
					// 改表処理

					bolTableHeaderFlg = false;
					bolTableDataFlg = false;

					// 改行キーによる改行チェック
					nowKeyRow = this.createKeyRow(bean);
					boolean retCheckKeyRow = cefrCm.checkKeyRow(nowKeyRow);

					// 表配置に伴う改行チェック
					boolean retCheckTableSide = cefrCm.checkTableSide();

					if (retCheckKeyRow && retCheckTableSide) {
						// 改行は実施しない
						cefrCm.setTableSide(SIDE.B);

					} else {
						// 改行キー相違により改行実施
						cefrCm.setTableSide(SIDE.A);

						// 改ページ判定
						boolean retCheckNewPage = cefrCm.checkNewPage();
						if (!retCheckNewPage) {
							// 改ページ実施
							this.newCreateSheet(cm);
						}
					}

					// カーソル位置初期化
					this.setInitCursolPosition();

					// 表テンプレートコピー
					this.copyTableTemplate(cm);
				}

				// 英語認定試験名・試験レベル名
				if (!bolTableHeaderFlg && viewFlag) {
					// フラグOFF時、値を設定
					String str = bean.getEngptname_abbr().trim();
					if (StringUtils.isNotEmpty(bean.getLevelflg())
						&& bean.getLevelflg().equals("1")) {
						// レベルありフラグONの場合、「認定試験名の短縮名 ＋ 半角SP ＋ 試験レベル名の短縮名」を設定
						str = str.concat(" ").concat(bean.getEngptlevelname_abbr().trim());
					}
					cefrCm.setTableName(cm, str);

					// 延人数設定
					cefrCm.setTableHeaderPerson(cm, this.TOTAL_PERSON_NAME);

					// 年度表を設定
					if (SIDE.A.equals(cefrCm.getSide())) {
						this.setTableNendo(cm);
					}

					bolTableHeaderFlg = true;
				}

				// 表内で初回データ設定時はカーソル位置を指定
				if (!bolTableDataFlg && viewFlag) {
					cefrCm.setCurCol(cefrCm.getInitialCurCol() + 1);
					bolTableDataFlg = true;
				}

				// 合計行以外で、表内最大CEFR列数を超える場合、処理スキップ
				if (cefrCm.getCurCol() - cefrCm.getInitialCurCol() > MAX_COL_COUNT
					&& !cefrCm.totalCefrCd.equals(bean.getCefrlevelcd())) {
					continue;
				}

                                if(viewFlag) {
                                    // CEFRレベル　タイトル
                                    this.setTableCefrName(cm, bean.getCerflevelname_abbr());

                                    // 2019/10/10 QQ)Ooseto 共通テスト対応 UPD START
                                    // CEFRレベル別　受験者数
                                    //cefrCm.setCerfNumber2(cm, bean.getCefrlevelcd(), bean.getNumbers1(), bean.getCompratio1(), bean.getFlg1());
                                    //cefrCm.setCerfNumber2(cm, bean.getCefrlevelcd(), bean.getNumbers2(), bean.getCompratio2(), bean.getFlg1());
                                    //cefrCm.setCerfNumber2(cm, bean.getCefrlevelcd(), bean.getNumbers3(), bean.getCompratio3(), bean.getFlg1());
                                    //cefrCm.setCerfNumber2(cm, bean.getCefrlevelcd(), bean.getNumbers4(), bean.getCompratio4(), bean.getFlg1());
                                    //cefrCm.setCerfNumber2(cm, bean.getCefrlevelcd(), bean.getNumbers5(), bean.getCompratio5(), bean.getFlg1());
                                    cefrCm.setCerfNumber(cm, bean.getCefrlevelcd(), bean.getNumbers1(), bean.getCompratio1(), bean.getFlg1());
                                    cefrCm.setCerfNumber(cm, bean.getCefrlevelcd(), bean.getNumbers2(), bean.getCompratio2(), bean.getFlg1());
                                    cefrCm.setCerfNumber(cm, bean.getCefrlevelcd(), bean.getNumbers3(), bean.getCompratio3(), bean.getFlg1());
                                    cefrCm.setCerfNumber(cm, bean.getCefrlevelcd(), bean.getNumbers4(), bean.getCompratio4(), bean.getFlg1());
                                    cefrCm.setCerfNumber(cm, bean.getCefrlevelcd(), bean.getNumbers5(), bean.getCompratio5(), bean.getFlg1());

                                    // カーソル位置指定
                                    cefrCm.setCurCol(cefrCm.getCurCol() + 1);
                                    cefrCm.setCurRow(cefrCm.getInitialCurRow() + 2);

                                    // CEFRレベル別　構成比
                                    //cefrCm.setCerfCompratio2(cm, bean.getCefrlevelcd(), bean.getNumbers1(), bean.getCompratio1(), bean.getFlg1());
                                    //cefrCm.setCerfCompratio2(cm, bean.getCefrlevelcd(), bean.getNumbers2(), bean.getCompratio2(), bean.getFlg1());
                                    //cefrCm.setCerfCompratio2(cm, bean.getCefrlevelcd(), bean.getNumbers3(), bean.getCompratio3(), bean.getFlg1());
                                    //cefrCm.setCerfCompratio2(cm, bean.getCefrlevelcd(), bean.getNumbers4(), bean.getCompratio4(), bean.getFlg1());
                                    //cefrCm.setCerfCompratio2(cm, bean.getCefrlevelcd(), bean.getNumbers5(), bean.getCompratio5(), bean.getFlg1());
                                    cefrCm.setCerfCompratio(cm, bean.getCefrlevelcd(), bean.getNumbers1(), bean.getCompratio1(), bean.getFlg1());
                                    cefrCm.setCerfCompratio(cm, bean.getCefrlevelcd(), bean.getNumbers2(), bean.getCompratio2(), bean.getFlg1());
                                    cefrCm.setCerfCompratio(cm, bean.getCefrlevelcd(), bean.getNumbers3(), bean.getCompratio3(), bean.getFlg1());
                                    cefrCm.setCerfCompratio(cm, bean.getCefrlevelcd(), bean.getNumbers4(), bean.getCompratio4(), bean.getFlg1());
                                    cefrCm.setCerfCompratio(cm, bean.getCefrlevelcd(), bean.getNumbers5(), bean.getCompratio5(), bean.getFlg1());
                                    // 2019/10/10 QQ)Ooseto 共通テスト対応 UPD END

                                    // カーソル位置指定
                                    cefrCm.setCurCol(cefrCm.getCurCol() + 1);
                                }
			}
		}
	}

 	/**
 	 * <DL>
	 * <DT>改ページ処理
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 */
 	public void newCreateSheet(CM cm) {

		cefrCm.initSheetSetting();

		// ヘッダ右側に帳票作成日時を表示する
		cm.setHeader(cefrCm.getWorkbook(), cefrCm.getWorkSheet());

		// セキュリティスタンプセット
		cefrCm.setCurCol(CURSOL.SECURITY_STAMP.getCol());
		cefrCm.setCurRow(CURSOL.SECURITY_STAMP.getRow());
		String secuFlg = cm.setSecurity(cefrCm.getWorkbook(), cefrCm.getWorkSheet(), intSecuFlg, cefrCm.getCurCol(), cefrCm.getCurCol() + 1);
		cefrCm.setCellValueAddRowCountUp(cm, secuFlg);

		// 学校名セット
		cefrCm.setCurCol(CURSOL.SCHOOL_NAME.getCol());
		cefrCm.setCurRow(CURSOL.SCHOOL_NAME.getRow());
		cefrCm.setCellValueAddRowCountUp(cm, "学校名　：" + cm.toString(strGakkomei));

		// 対象模試セット
		String moshi = cm.setTaisyouMoshi(strMshDate); // 模試月取得
		cefrCm.setCellValueAddRowCountUp(cm, cm.getTargetExamLabel() + "：" + cm.toString(strMshmei) + moshi);

		// 表示対象セット
		String cefrMySchoolOnlyLabel = "";
		if (cefrCm.checkIntTargetCheckBoxFlg()) {
			cefrMySchoolOnlyLabel = cm.getCefrMySchoolOnlyLabel();
		}
		cefrCm.setCellValueAddRowCountUp(cm, cm.getCefrTargetLabel() + "：" + cefrMySchoolOnlyLabel);
 	}
}
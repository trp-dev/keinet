/**
 * 個人成績分析−面談シート　教科分析シート
 * 	Excelファイル編集
 * 作成日: 2004/09/15
 * @author	H.Fujimoto
 * 
 * 2009.11.30   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 */

package jp.co.fj.keinavi.excel.personal;

import java.io.FileInputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.personal.I11ChartListBean;
import jp.co.fj.keinavi.excel.data.personal.I11GouChartListBean;
import jp.co.fj.keinavi.excel.data.personal.I11Item;
import jp.co.fj.keinavi.excel.data.personal.I11KyokaHensaListBean;
import jp.co.fj.keinavi.excel.data.personal.I11KyokaSuiiListBean;
import jp.co.fj.keinavi.excel.data.personal.I11ListBean;
import jp.co.fj.keinavi.excel.data.personal.I11ShiboGouListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class I12_01 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	final private int	intMaxSheetSr	= 50;			// 最大シート数	
	final private String	strArea			= "A1:BV63";	// 印刷範囲	


	//2005.04.26 Add 新テスト対応	
	//final private String	masterfile		= "I12_01";	// ファイル名
	final private String 	masterfile0 	= "I12_01";	// ファイル名１(新テスト対応用)
	final private String 	masterfile1 	= "I12_01";	// ファイル名２(新テスト対応用)
	private String masterfile = "";


/*
 * 	Excel編集メイン
 * 		I11Item i11Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int i12_01EditExcel(I11Item i11Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		
		NumberFormat nf = NumberFormat.getInstance();
		KNLog log = KNLog.getInstance(null,null,null);

		//2005.04.26 Add 新テスト対応
		//テンプレートの決定
		if (i11Item.getIntKyoSheetShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}


		try {
			
			// データセット
			ArrayList	i11List			= i11Item.getI11List();
			Iterator	itr				= i11List.iterator();

			int		fileIndex		= 1;	// ファイルカウンター
			int		maxSheetIndex	= 1;	// シートカウンター

			// マスタExcel読み込み
			FileInputStream	fin			= null;
			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			// 基本ファイルを読込む
			I11ListBean i11ListBean = new I11ListBean();

			log.Ep("I12_01","基本データデータセット開始","");

			/** データリスト **/
			while( itr.hasNext() ) {
				fileIndex = 1;
				i11ListBean = (I11ListBean) itr.next();
	
				// マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}

				// データセットするシートの選択
				workSheet = workbook.getSheet(String.valueOf(maxSheetIndex));

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);

				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntKyoSheetSecuFlg() ,71 ,73 );
				//Update End
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 71 );
				workCell.setCellValue(secuFlg);

				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　：" + i11ListBean.getStrGakkomei() + "　学年：" + i11ListBean.getStrGrade() + "　クラス名：" + i11ListBean.getStrClass() + "　クラス番号：" + i11ListBean.getStrClassNum() );

				// 氏名・性別セット	
				String strSex = "";
				if( cm.toString(i11ListBean.getStrSex()).equals("1") ){
					strSex = "男";
				}else if( cm.toString(i11ListBean.getStrSex()).equals("2") ){
					strSex = "女";
				}else{
					strSex = "不明";
				}
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );

				if( !cm.toString(i11ListBean.getStrShimei()).equals("")){
					workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrShimei()) + "（" + cm.toString(i11ListBean.getStrKana()) + "）　性別：" + strSex );
				}else{
					workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrKana()) + "　性別：" + strSex );
				}


				/** バランスチャートデータリスト **/
				// データセット
				ArrayList i11ChartList = i11ListBean.getI11ChartList();
				Iterator itrI11Chart = i11ChartList.iterator();

				int chrtRow = 0;

				log.Ep("I12_01","バランスチャートデータセット開始","");

				while(itrI11Chart.hasNext()){
					I11ChartListBean i11ChartListBean = (I11ChartListBean) itrI11Chart.next();

					// 模試名セット
					String moshi = cm.setTaisyouMoshi(i11ChartListBean.getStrMshDate());
					workCell = cm.setCell( workSheet, workRow, workCell, 27+chrtRow, 1 );
					workCell.setCellValue( i11ChartListBean.getStrMshmei() + moshi );

					//１教科複数受験フラグ
					workCell = cm.setCell( workSheet, workRow, workCell, 31, 36 );
					if (i11ChartListBean.getIntKyokaDispFlg() == 1) {
						workCell.setCellValue("グラフの表示方法：教科内の平均");
					}else if (i11ChartListBean.getIntKyokaDispFlg() == 2) {
						workCell.setCellValue("グラフの表示方法：教科内の良い方");
					}else if (i11ChartListBean.getIntKyokaDispFlg() == 3) {
						workCell.setCellValue("グラフの表示方法：教科内の第1解答科目");
					}

					/** 教科別偏差リスト **/
					// データセット
					ArrayList i11KyokaHensa = i11ChartListBean.getI11KyokaHensaList();
					Iterator itrI11Hensa = i11KyokaHensa.iterator();

					int chrtCol = 0;
					while(itrI11Hensa.hasNext()){
						I11KyokaHensaListBean i11KyokaHensaListBean = (I11KyokaHensaListBean)itrI11Hensa.next();

						// 偏差値セット
						if( i11KyokaHensaListBean.getFloHensa() != -999.0 ){
							workCell = cm.setCell( workSheet, workRow, workCell, 27+chrtRow, 17+4*chrtCol );
							workCell.setCellValue( i11KyokaHensaListBean.getFloHensa() );
						}
						
						// 学力レベルセット
						if( !"".equals(i11KyokaHensaListBean.getStrScholarLevel()) ){
							workCell = cm.setCell( workSheet, workRow, workCell, 27+chrtRow, 20+4*chrtCol );
							workCell.setCellValue( i11KyokaHensaListBean.getStrScholarLevel() );
						}
						
						chrtCol++;
						if(chrtCol >= 5){
							break;
						}
					}
					chrtRow++;
					if(chrtRow >= 4){
						break;
					}
				}
				
				log.Ep("I12_01","バランスチャートデータセット終了","");

				/** バランスチャート(合格者平均)データリスト **/
				// データセット
				ArrayList i11GouChartList = i11ListBean.getI11GouChartList();
				Iterator itrI11GouChart = i11GouChartList.iterator();

				int gouchrtCol = 0;

				log.Ep("I12_01","バランスチャート（合格者平均）データセット開始","");

				while(itrI11GouChart.hasNext()){
					I11GouChartListBean i11GouChartListBean = (I11GouChartListBean) itrI11GouChart.next();
					
					if ( cm.toString(i11GouChartListBean.getStrKyokamei()).equals("英語") ){
						gouchrtCol = 0;
					}else if ( cm.toString(i11GouChartListBean.getStrKyokamei()).equals("数学") ){
					gouchrtCol = 1;
					}else if ( cm.toString(i11GouChartListBean.getStrKyokamei()).equals("国語") ){
					gouchrtCol = 2;
					}else if ( cm.toString(i11GouChartListBean.getStrKyokamei()).equals("理科") ){
					gouchrtCol = 3;
					}else if ( cm.toString(i11GouChartListBean.getStrKyokamei()).equals("地歴公民") ){
					gouchrtCol = 4;
					}else{
						break;
					}

					//１教科複数受験フラグ
					workCell = cm.setCell( workSheet, workRow, workCell, 31, 73 );
					if (i11GouChartListBean.getIntKyokaDispFlg() == 1) {
						workCell.setCellValue("グラフの表示方法：教科内の平均");
					}else if (i11GouChartListBean.getIntKyokaDispFlg() == 2) {
						workCell.setCellValue("グラフの表示方法：教科内の良い方");
					}else if (i11GouChartListBean.getIntKyokaDispFlg() == 3) {
						workCell.setCellValue("グラフの表示方法：教科内の第1解答科目");
					}
					
					//模試名
					String moshi =cm.setTaisyouMoshi( i11Item.getStrBaseMshDate() );
					workCell = cm.setCell( workSheet, workRow, workCell, 27, 38 );
					workCell.setCellValue( cm.toString(i11Item.getStrMshmei()) + moshi );
					
					//偏差値
					if( i11GouChartListBean.getFloMshHensa() != -999.0 ){
						workCell = cm.setCell( workSheet, workRow, workCell, 27, 54+4*gouchrtCol );
						workCell.setCellValue( i11GouChartListBean.getFloMshHensa());
					}
					
					//学力レベル
					if( !"".equals(i11GouChartListBean.getStrScholarLevel()) ){
						workCell = cm.setCell( workSheet, workRow, workCell, 27, 57+4*gouchrtCol );
						workCell.setCellValue( i11GouChartListBean.getStrScholarLevel());
					}

					/** 教科別偏差リスト **/
					// データセット
					ArrayList i11ShiboGou = i11GouChartListBean.getI11ShiboGouList();
					Iterator itrI11ShiboGou = i11ShiboGou.iterator();

					int gouchrtRow = 0;
					while(itrI11ShiboGou.hasNext()){
						I11ShiboGouListBean i11ShiboGouListBean = (I11ShiboGouListBean)itrI11ShiboGou.next();

						//模試名・大学名+学部名+学科名
						workCell = cm.setCell( workSheet, workRow, workCell, 28+gouchrtRow, 38 );
						workCell.setCellValue( cm.toString(i11ShiboGouListBean.getStrDaigakuMei()) + " " + cm.toString(i11ShiboGouListBean.getStrGakubuMei()) + " " + cm.toString(i11ShiboGouListBean.getStrGakkaMei()));

						// 偏差値セット
						if( i11ShiboGouListBean.getFloHensa() != -999.0 ){
							workCell = cm.setCell( workSheet, workRow, workCell, 28+gouchrtRow, 54+4*gouchrtCol );
							workCell.setCellValue( i11ShiboGouListBean.getFloHensa() );
						}
						
						gouchrtRow++;
						if(gouchrtRow >= 3){
							break;
						}
					}
				}

				log.Ep("I12_01","バランスチャート（合格者平均）データセット終了","");

				/** 教科別成績推移グラフデータリスト **/
				// データセット
				ArrayList i11KyokaSuiiList = i11ListBean.getI11KyokaSuiiList();
				Iterator itrI11Kyouka = i11KyokaSuiiList.iterator();

				int suiCol = 0;

				log.Ep("I12_01","教科別成績推移グラフデータセット開始","");

				while(itrI11Kyouka.hasNext()){
					I11KyokaSuiiListBean i11KyokaSuiiListBean = (I11KyokaSuiiListBean) itrI11Kyouka.next();

					// 模試名セット
					String moshi = cm.setTaisyouMoshi(i11KyokaSuiiListBean.getStrMshDate());
					workCell = cm.setCell( workSheet, workRow, workCell, 54, 9+4*suiCol );
					workCell.setCellValue( i11KyokaSuiiListBean.getStrMshmei() + moshi );

					//１教科複数受験フラグ
					workCell = cm.setCell( workSheet, workRow, workCell, 63, 36 );
					if (i11KyokaSuiiListBean.getIntKyokaDispFlg() == 1) {
						workCell.setCellValue("グラフの表示方法：教科内の平均");
					}else if (i11KyokaSuiiListBean.getIntKyokaDispFlg() == 2) {
						workCell.setCellValue("グラフの表示方法：教科内の良い方");
					}else if (i11KyokaSuiiListBean.getIntKyokaDispFlg() == 3) {
						workCell.setCellValue("グラフの表示方法：教科内の第1解答科目");
					}

					/** 教科別偏差リスト **/
					// データセット
					ArrayList i11KyokaHensa = i11KyokaSuiiListBean.getI11KyokaHensaList();
					Iterator itrI11Hensa = i11KyokaHensa.iterator();

					int suiRow = 0;
					while(itrI11Hensa.hasNext()){
						I11KyokaHensaListBean i11KyokaHensaListBean = (I11KyokaHensaListBean)itrI11Hensa.next();

						// 偏差値セット
						if( i11KyokaHensaListBean.getFloHensa() != -999.0 ){
							workCell = cm.setCell( workSheet, workRow, workCell, 57+suiRow, 9+4*suiCol );
							workCell.setCellValue( i11KyokaHensaListBean.getFloHensa() );
						}
						
						// 学力レベルセット
						if( !"".equals(i11KyokaHensaListBean.getStrScholarLevel()) ){
							workCell = cm.setCell( workSheet, workRow, workCell, 57+suiRow, 12+4*suiCol );
							workCell.setCellValue( i11KyokaHensaListBean.getStrScholarLevel() );
						}

						suiRow++;
						if(suiRow >= 6){
							break;
						}
					}
					suiCol++;
					if(suiCol >= 7){
						break;
					}
				}

				log.Ep("I12_01","教科別成績推移グラフデータセット終了","");

				// シートの選択
				workbook.getSheet("1").setSelected(true);
	
				// Excelファイル保存
				boolean bolRet = false;
				String strFileName = masterfile + "_" + cm.toString(i11ListBean.getStrGrade()) + cm.toString(i11ListBean.getStrClass()) + cm.toString(i11ListBean.getStrClassNum());
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, strFileName, maxSheetIndex-1);
				fileIndex++;

				if( bolRet == false ){
					return errfwrite;					
				}
				
			}

		} catch(Exception e) {
			log.Err("I12_01","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("I12_01","基本データデータセット終了","");
		return noerror;
	}

}
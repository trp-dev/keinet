package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * 志望大学評価別人数（校内成績）データリスト
 * 作成日: 2004/07/08
 * @author	T.Kuzuno
 */
public class S34ListBean {
	//現役高卒区分
	private String strGenKouKbn = "";
	//大学コード
	private String strDaigakuCd = "";
	//大学名
	private String strDaigakuMei = "";
	//学部コード
	private String strGakubuCd = "";
	//学部名
	private String strGakubuMei = "";
	//学科コード
	private String strGakkaCd = "";
	//学科名
	private String strGakkaMei = "";
	//日程コード
	private String strNtiCd = "";
	//日程
	private String strNittei = "";
	//評価区分
	private String strHyouka = "";
	//クラス別評価人数リスト
	private ArrayList s34ClassList = new ArrayList();
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrDaigakuCd() {
		return this.strDaigakuCd;
	}
	public String getStrDaigakuMei() {
		return this.strDaigakuMei;
	}
	public String getStrGakkaCd() {
		return this.strGakkaCd;
	}
	public String getStrGakkaMei() {
		return this.strGakkaMei;
	}
	public String getStrGakubuCd() {
		return this.strGakubuCd;
	}
	public String getStrGakubuMei() {
		return this.strGakubuMei;
	}
	public String getStrGenKouKbn() {
		return this.strGenKouKbn;
	}
	public String getStrHyouka() {
		return this.strHyouka;
	}
	public String getStrNtiCd() {
		return this.strNtiCd;
	}
	public String getStrNittei() {
		return this.strNittei;
	}
	public ArrayList getS34ClassList() {
		return this.s34ClassList;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrDaigakuCd(String strDaigakuCd) {
		this.strDaigakuCd = strDaigakuCd;
	}
	public void setStrDaigakuMei(String strDaigakuMei) {
		this.strDaigakuMei = strDaigakuMei;
	}
	public void setStrGakkaCd(String strGakkaCd) {
		this.strGakkaCd = strGakkaCd;
	}
	public void setStrGakkaMei(String strGakkaMei) {
		this.strGakkaMei = strGakkaMei;
	}
	public void setStrGakubuCd(String strGakubuCd) {
		this.strGakubuCd = strGakubuCd;
	}
	public void setStrGakubuMei(String strGakubuMei) {
		this.strGakubuMei = strGakubuMei;
	}
	public void setStrGenKouKbn(String strGenKouKbn) {
		this.strGenKouKbn = strGenKouKbn;
	}
	public void setStrHyouka(String strHyouka) {
		this.strHyouka = strHyouka;
	}
	public void setStrNtiCd(String strNtiCd) {
		this.strNtiCd = strNtiCd;
	}
	public void setStrNittei(String strNittei) {
		this.strNittei = strNittei;
	}
	public void setS34ClassList(ArrayList s34ClassList) {
		this.s34ClassList = s34ClassList;
	}
}

package jp.co.fj.keinavi.beans.individual;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import jp.co.fj.kawaijuku.judgement.beans.SubjectStateBean;
import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.kawaijuku.judgement.data.Univ;
import jp.co.fj.keinavi.data.individual.UnivKeiNavi;

/**
 *
 * 二次課し科目チェック
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SearchImposeSubBean {

    /**
     * 二次使用科目の条件がそろった
     * 枝番のリスト
     */
    ArrayList S_branch = new ArrayList();

    /**
     * 検索結果JudgementオブジェクトのArray
     */
    private List judgedList = new ArrayList();
    public void setJudgements(List judgements){this.judgedList = judgements;}
    public List getJudgements(){return this.judgedList;}

    /**
     * 全大学一覧
     */
    private List univAll;
    public void setUnivAll(List univAll){this.univAll = univAll;}
    public List getUnivAll(){return this.univAll;}

    /**
     * 入力された得点
     */
    private Score score;
    public void setScore(Score score){this.score = score;}
    public Score getScore(){return this.score;}

    private String[] subjectArray;

    public void setSubjectArray(String[] ss) {
        subjectArray = ss;
    }

    /**
     * 二次を課すをチェックしたかどうか
     * true:課す false:課さない
     */
    private boolean isImpose2nd;
    public void setImpose2nd(boolean b){this.isImpose2nd = b;}
    public boolean isImpose2nds(){return this.isImpose2nd;}

    /**
     * 科目を課す・課さない
     */
    private boolean isImposeSub;
    public void setImposeSub(boolean b){this.isImposeSub = b;}
    public boolean isImposeSub(){return this.isImposeSub;}

    // 対象模試がセンタープレの場合、
    // 一般私大、短大の2次判定を＃３記述ではなく
    // センタープレの成績を優先利用するかどうか
    // インターネット模試判定において「優先しない」とするため
    // デフォルト値はfalseとする
    private boolean precedeCenterPre = false;

    /**
     * 実行メソッド
     */
    public void execute() throws Exception {

        // 判定処理マネージャ
        final JudgementManager judge = new JudgementManager(
                score, precedeCenterPre);

        // 大学検索を一件ずつ行う。
        ListIterator it = univAll.listIterator();
        while (it.hasNext()) {
            UnivKeiNavi univ = (UnivKeiNavi) it.next();

            // 2回目以降は初期化
            if (S_branch.size() > 0) {
                S_branch.clear();
            }

            if (querySecondSubject(univ)) { //二次科目検索
                //※シングルで実行
                judge.setUniv(univ);
                judge.setBranch(S_branch);
                judge.execute();
                judgedList.add(judge.getResult());
            }
        }
    }

    /**
     * センタープレでの成績を差し替えを有効化
     */
    public void setPrecedeCenterPre() {
        precedeCenterPre = true;
    }

    /**
     * 二次使用科目チェック
     * @param <code>univ</code> 大学クラス
     * @return true:二次使用科目チェックOK , false:二次使用科目チェックNG
     * @throws Exception
     */
    private boolean querySecondSubject(UnivKeiNavi univ) throws Exception {

        //二次科目チェックインスタンスを作成
        SubjectStateBean state = new SubjectStateBean((Univ)univ);

        //「二次試験なし」がチェックされている
        if (!this.isImpose2nd) {
            state.setNotImposing2nd(true);
        }
        //「二次試験なし」がチェックされていない
        //※＝二次科目あり
        else {
//
//			state.setNotImposing2nd(false);
            //指定の科目を設定
            if (subjectArray != null) {
                setupSubjects(state);
            }

            //指定の「科目を課す」
            if (this.isImposeSub) {
                state.setImposingSubject(true);
            }
//			//指定の「科目を課さない」
//			else {
//				state.setImposingSubject(false);
//			}
        }

        boolean result = state.execute();

        //採用された枝番を設定
        S_branch.addAll(state.getBranches());

        return result;
    }

    /**
     * setArrayList String[]→ArrayList[]に値を設定する
     * 科目名がKEINAVIとBANZAIでは一致していないので
     * すべて対応させる必要がある
     * @param strArr  設定する配列
     * @param listArr 設定対象配列
     * @return　strArrとintArrを結合したint配列
     */
    public void setupSubjects(SubjectStateBean state){

        for (int i = 0; i < subjectArray.length; i++) {

            //理科
            if (subjectArray[i].equals("physics")) {
                state.allow_physics();
            }
            else if (subjectArray[i].equals("chemistry")) {
                state.allow_chemistry();
            }
            else if (subjectArray[i].equals("biology")) {
                state.allow_biology();
            }
            else if (subjectArray[i].equals("earthScience")) {
                state.allow_earth();
            }
            else if (subjectArray[i].equals("physicsBasic")) {
                state.allow_physicsBasic();
            }
            else if (subjectArray[i].equals("chemistryBasic")) {
                state.allow_chemistryBasic();
            }
            else if (subjectArray[i].equals("biologyBasic")) {
                state.allow_biologyBasic();
            }
            else if (subjectArray[i].equals("earthScienceBasic")) {
                state.allow_earthBasic();
            }
            else if(subjectArray[i].equals("math1")){
                state.allow_mathI();
            }
            else if (subjectArray[i].equals("math1A")) {
                state.allow_mathIA();
            }
            else if (subjectArray[i].equals("math2A")) {
                state.allow_mathIIA();
            }
            else if (subjectArray[i].equals("math2B")) {
                state.allow_mathIIB();
            }
            else if (subjectArray[i].equals("math3B")) {
                state.allow_mathIII();
            }
            else if(subjectArray[i].equals("japaneseLit")){
                state.allow_japanese1();
            }
            else if (subjectArray[i].equals("jClassics")) {
                state.allow_japanese2();
            }
            else if (subjectArray[i].equals("japanese")) {
                state.allow_japanese3();
            }
            else if (subjectArray[i].equals("english")){
                state.allow_english();
            }
            else if (subjectArray[i].equals("jHistory")){
                state.allow_jHistroy();
            }
            else if (subjectArray[i].equals("wHistory")){
                state.allow_wHistroy();
            }
            else if (subjectArray[i].equals("geography")){
                state.allow_geography();
            }
            else if (subjectArray[i].equals("politics")){
                state.allow_politics();
            }
            else if (subjectArray[i].equals("comprehensive")){
                state.allow_comprehensive();
            }
            else if (subjectArray[i].equals("interview")){
                state.allow_interview();
            }
            else if (subjectArray[i].equals("performance")){
                state.allow_performance();
            }
            else if (subjectArray[i].equals("essay")){
                state.allow_essay();
            }
            else if (subjectArray[i].equals("ethic")){
                state.allow_ethic();
            }
            else{
                throw new InternalError("存在しない科目が検出されました");
            }
            //c_socialStudies 現社はない!
        }
    }
}


/*
 * 作成日: 2004/07/16
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import jp.co.fj.keinavi.util.db.RecordProcessor;

/**
 * @author
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ExMiddleDatePlusUtil extends DateUtil {

	// SimpleDateFormatインスタンス
	private static SimpleDateFormat format; 

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.date.DateUtil#getInstance()
	 */
	public SimpleDateFormat factoryMethod() {
		if (format == null) format = new SimpleDateFormat("yy/M/d");
		return format;
	}
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.date.DateUtil#string2Date(java.lang.String)
	 * スラッシュ等が入った特殊な型はSimpleDateFormatでparseできない。
	 */
	public Date string2Date(String string) throws ParseException {
		int year =  Calendar.getInstance().get(Calendar.YEAR);
		int month = Integer.parseInt(RecordProcessor.getMonthDigit(string));
		int date = Integer.parseInt(RecordProcessor.getDateDigit(string));
		GregorianCalendar ct = new GregorianCalendar(year, month-1 , date);
		return ct.getTime();
	}
}

package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;

/**
 * �N���X���ъT���|�l���ѐ��ڌ^�E�Ȗڃ��X�g
 * �쐬��: 2004/07/14
 * @author	H.Fujimoto
 */
public class C13KmkListBean {
	//�^�E�ȖڃR�[�h
	private String strKmkCd = "";
	//�^�E�Ȗږ�
	private String strKmkmei = "";
	//�^�E�Ȗڕʐ��уf�[�^���X�g
	private ArrayList c13KmkSeisekiList = new ArrayList();


	/*----------*/
	/* Get      */
	/*----------*/
	
	public ArrayList getC13KmkSeisekiList() {
		return this.c13KmkSeisekiList;
	}
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setC13KmkSeisekiList(ArrayList c13KmkSeisekiList) {
		this.c13KmkSeisekiList = c13KmkSeisekiList;
	}
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}

}
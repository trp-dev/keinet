package jp.co.fj.keinavi.beans.login;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.login.PactSchoolData;
import jp.co.fj.keinavi.interfaces.ILogin;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.date.ShortDateUtil;
import jp.co.fj.keinavi.util.message.MessageLoader;
import jp.co.fj.keinavi.util.sql.QueryLoader;

/**
 *
 * 資産認証処理Beanです。<br>
 * ※ {@link SchoolLoginBean} の処理をコピペして実装。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class AssetLoginBean extends DefaultBean {

    /** serialVersionUID */
    private static final long serialVersionUID = -2128778637058916751L;

    /** 学校ID */
    private String schoolId;

    /** 利用者ID */
    private String userId;

    /** パスワード */
    private String password;

    /**
     * {@inheritDoc}
     */
    public void execute() throws SQLException, Exception {

        /* ゲストアカウントはログイン不可とする */
        if (KNCommonProperty.getTrialUserID().equals(schoolId)) {
            throw createServletException("ゲストアカウントでは資産認証を行えません。", getFailureMessage());
        }

        /* 今日の日付 */
        final String today = new ShortDateUtil().date2String();

        /* 契約校マスタを取得する */
        PactSchoolData pact = new PactSchoolData();
        PreparedStatement ps1 = null;
        ResultSet rs1 = null;
        try {
            ps1 = conn.prepareStatement(QueryLoader.getInstance().load("w04").toString());
            ps1.setString(1, schoolId);
            rs1 = ps1.executeQuery();
            if (rs1.next()) {
                /* 資産認証に必要なデータのみ設定する */
                pact.setSchoolCD(rs1.getString(1));
                pact.setPactDiv(rs1.getShort(2));
                //pact.setCertCN(rs1.getString(3));
                //pact.setCertOU(rs1.getString(4));
                //pact.setCertO(rs1.getString(5));
                //pact.setCertL(rs1.getString(6));
                //pact.setCertST(rs1.getString(7));
                //pact.setCertC(rs1.getString(8));
                //pact.setCertUserDiv(rs1.getShort(9));
                //pact.setPasswd(rs1.getString(10));
                //pact.setFeatprovMode(rs1.getShort(11));
                //pact.setValidDate(rs1.getString(12));
                //pact.setCertFlag(rs1.getString(13));
                pact.setPactTerm(rs1.getString(14));
            } else {
                throw createServletException("契約校データの取得に失敗しました。", getFailureMessage());
            }
        } finally {
            DbUtils.closeQuietly(null, ps1, rs1);
        }

        /* 契約期限をチェックする */
        if (pact.getPactTerm() == null || today.compareTo(pact.getPactTerm()) > 0) {
            throw createServletException("契約期限が切れています。", "契約期限が切れています。");
        }

        LoginSession loginSession = new LoginSession();

        /* 学校マスタを取得する */
        PreparedStatement ps2 = null;
        ResultSet rs2 = null;
        try {
            ps2 = conn.prepareStatement("SELECT stopdate, newgetdate FROM school WHERE bundlecd = ?");
            ps2.setString(1, pact.getSchoolCD());
            rs2 = ps2.executeQuery();

            if (rs2.next()) {
                /* 停止日をチェックする */
                if (rs2.getString(1) != null && rs2.getString(1).compareTo(today + "0000") < 0) {
                    throw createServletException("停止日を過ぎています。", getFailureMessage());
                }

                /* 新規取得日をチェックする */
                if (rs2.getString(2) != null && rs2.getString(2).compareTo(today + "0000") >= 0) {
                    throw createServletException("新規取得日を過ぎていません。", getFailureMessage());
                }

                loginSession.setLoginID(schoolId);
                loginSession.setAccount(userId);
                loginSession.setUserID(pact.getSchoolCD());
                loginSession.setUserMode(ILogin.SCHOOL_NORMAL);
                loginSession.setPactDiv(pact.getPactDiv());
            } else {
                throw createServletException("学校マスタの取得に失敗しました。", getFailureMessage());
            }
        } finally {
            DbUtils.closeQuietly(null, ps2, rs2);
        }

        /* 利用者認証を行う */
        final UserAuthBean bean = new UserAuthBean(loginSession, password);
        bean.setConnection(null, conn);
        bean.execute();

        /* 非契約校の有効・無効チェックを行う */
        if (!loginSession.isPact() && !loginSession.isMaintainer()) {
            throw createServletException("有効利用者チェックエラー", "入力された学校ID・利用者IDではKei-Naviにログインできません。\\n\\n"
                    + "お手数ですが「ログイン」ボタン横から「よくあるご質問（FAQ）」\\nをご参照ください。\\n\\n");
        }
    }

    /**
     * 認証失敗時のエラーメッセージを取得します。
     *
     * @return 認証失敗時のエラーメッセージ
     * @throws IOException IO例外
     */
    private String getFailureMessage() throws IOException {
        return MessageLoader.getInstance().getMessage("w028a");
    }

    /**
     * ログイン例外オブジェクトを生成します。
     *
     * @param logMessage ログに出力するメッセージ（さらに学校ID、利用者IDを付加して出力）
     * @param errorMessage 画面に出力するエラーメッセージ
     * @return {@link KNServletException}
     */
    private KNServletException createServletException(final String logMessage, final String errorMessage) {

        final StringBuffer buff = new StringBuffer();
        buff.append(schoolId);
        buff.append(" ");
        if (userId != null) {
            buff.append(userId);
            buff.append(" ");
        }
        buff.append(logMessage);

        final KNServletException e = new KNServletException(buff.toString());
        e.setErrorCode("1");
        e.setErrorMessage(errorMessage);
        return e;
    }

    /**
     * 学校IDをセットします。
     *
     * @param schoolId 学校ID
     */
    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    /**
     * 利用者IDをセットします。
     *
     * @param userId 利用者ID
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * パスワードをセットします。
     *
     * @param password パスワード
     */
    public void setPassword(String password) {
        this.password = password;
    }

}

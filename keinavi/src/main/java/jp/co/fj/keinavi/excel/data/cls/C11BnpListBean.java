package jp.co.fj.keinavi.excel.data.cls;

/**
 * �N���X���ъT���|�΍��l���z���X�g
 * �쐬��: 2004/07/13
 * @author	H.Fujimoto
 */
public class C11BnpListBean {
	//�΍��l�я���l
	private float floBnpMax = 0;
	//�΍��l�щ����l
	private float floBnpMin = 0;
	//�l��
	private int intNinzu = 0;

	/*----------*/
	/* Get      */
	/*----------*/
	
	public float getFloBnpMax() {
		return this.floBnpMax;
	}
	public float getFloBnpMin() {
		return this.floBnpMin;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	
	public void setFloBnpMax(float floBnpMax) {
		this.floBnpMax = floBnpMax;
	}
	public void setFloBnpMin(float floBnpMin) {
		this.floBnpMin = floBnpMin;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}

}
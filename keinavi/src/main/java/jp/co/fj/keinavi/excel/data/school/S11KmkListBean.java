package jp.co.fj.keinavi.excel.data.school;

/**
 * �Z�����с|���ъT���@�Ȗڕʃf�[�^���X�g
 * �쐬��: 2004/06/28
 * @author	T.Sakai
 */
public class S11KmkListBean {
	//�ȖڃR�[�h
	private String strKmkCd = "";
	//�Ȗږ�
	private String strKmkmei = "";
	//�z�_
	private String strHaitenKmk = "";
	//�l���i���Z�j
	private int intNinzuHome = 0;
	//�l���i���j
	private int intNinzuKen = 0;
	//�l���i�S���j
	private int intNinzuAll = 0;
	//���ϓ_�i���Z�j
	private float floHeikinHome = 0;
	//���ϓ_�i���j
	private float floHeikinKen = 0;
	//���ϓ_�i�S���j
	private float floHeikinAll = 0;
	//���ϕ΍��l�i���Z�j
	private float floHensaHome = 0;
	//���ϕ΍��l�i���j
	private float floHensaKen = 0;
	//���ϕ΍��l�i�S���j
	private float floHensaAll = 0;
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public int getIntNinzuHome() {
		return this.intNinzuHome;
	}
	public int getIntNinzuKen() {
		return this.intNinzuKen;
	}
	public int getIntNinzuAll() {
		return this.intNinzuAll;
	}
	public float getFloHeikinHome() {
		return this.floHeikinHome;
	}
	public float getFloHeikinKen() {
		return this.floHeikinKen;
	}
	public float getFloHeikinAll() {
		return this.floHeikinAll;
	}
	public float getFloHensaHome() {
		return this.floHensaHome;
	}
	public float getFloHensaKen() {
		return this.floHensaKen;
	}
	public float getFloHensaAll() {
		return this.floHensaAll;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setIntNinzuHome(int intNinzuHome) {
		this.intNinzuHome = intNinzuHome;
	}
	public void setIntNinzuKen(int intNinzuKen) {
		this.intNinzuKen = intNinzuKen;
	}
	public void setIntNinzuAll(int intNinzuAll) {
		this.intNinzuAll = intNinzuAll;
	}
	public void setFloHeikinHome(float floHeikinHome) {
		this.floHeikinHome = floHeikinHome;
	}
	public void setFloHeikinKen(float floHeikinKen) {
		this.floHeikinKen = floHeikinKen;
	}
	public void setFloHeikinAll(float floHeikinAll) {
		this.floHeikinAll = floHeikinAll;
	}
	public void setFloHensaHome(float floHensaHome) {
		this.floHensaHome = floHensaHome;
	}
	public void setFloHensaKen(float floHensaKen) {
		this.floHensaKen = floHensaKen;
	}
	public void setFloHensaAll(float floHensaAll) {
		this.floHensaAll = floHensaAll;
	}
}

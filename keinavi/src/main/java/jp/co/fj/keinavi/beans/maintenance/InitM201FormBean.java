package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.forms.maintenance.M201Form;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 受験届修正フォームの初期化処理を行うBean
 * 
 * 2006.11.07	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class InitM201FormBean extends DefaultBean {

	// フォームオブジェクト
	private M201Form form;

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		getHistoryinfo();
		getIndividualRecord();
	}

	// 学籍履歴の情報を取得する
	private void getHistoryinfo() throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT grade, class "
					+ "FROM historyinfo "
					+ "WHERE individualid = ? AND year = ?");
			ps.setString(1, form.getInitIndividualId());
			ps.setString(2, form.getInitExamYear());
			rs = ps.executeQuery();
			if (rs.next()) {
				form.setTargetYear1(form.getInitExamYear());
				form.setTargetGrade1(rs.getString(1));
				form.setTargetClass1(rs.getString(2));
				form.setTargetStudentId(form.getInitIndividualId());
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	// 個表データの情報を取得する
	private void getIndividualRecord() throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT grade, UPPER(class), answersheet_no "
					+ "FROM individualrecord "
					+ "WHERE s_individualid = ? "
					+ "AND examyear = ? AND examcd = ?");
			ps.setString(1, form.getInitIndividualId());
			ps.setString(2, form.getInitExamYear());
			ps.setString(3, form.getInitExamCd());
			rs = ps.executeQuery();
			if (rs.next()) {
				form.setTargetYear2(form.getInitExamYear());
				form.setTargetGrade2(rs.getString(1));
				form.setTargetClass2(rs.getString(2));
				form.setTargetSheetId(form.getInitExamYear() + ","
						+ form.getInitExamCd() + "," + rs.getString(3));
				form.setTargetExam(form.getInitExamCd());
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * @param pForm 設定する form。
	 */
	public void setForm(M201Form pForm) {
		form = pForm;
	}
	
}

/**
 * 校内成績分析−全国総合成績概況(高３模試)
 * 	Excelファイル編集
 * 作成日: 2004/07/16
 * @author	H.Fujimoto
 */

package jp.co.fj.keinavi.excel.school;

import java.io.*;
import java.util.*;
import java.text.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.excel.cm.*;

import org.apache.poi.hssf.usermodel.*;
import jp.co.fj.keinavi.util.log.*;

public class S01_01 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
//	private String		strArea			= "A1:N79";	//印刷範囲	
	
	final private String masterfile0 = "S01_01";
	final private String masterfile1 = "S01_01";
	private String masterfile = "";

/*
 * 	Excel編集メイン
 * 		S01Item s01Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s01_01EditExcel(S01Item s01Item, ArrayList outfilelist, int intSaveFlg, String UserID ) {
		
		FileInputStream	fin			= null;
		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		int			intMaxSheetIndex	= 0;	//シートカウンタ
		int			kmkMaxSheetCnt		= 1;	//科目用シートカウンタ
		int			kataCnt				= 0;	//型カウンタ
		int			kmkCnt				= 0;	//科目カウンタ
		NumberFormat nf = NumberFormat.getInstance();
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s01Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		//マスタExcel読み込み
		workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
		if( workbook==null ){
			return errfread;
		}

		// 基本ファイルを読込む
		S01KataListBean s01KataListBean = new S01KataListBean();
		S01KmkListBean s01KmkListBean = new S01KmkListBean();

		try {	
			// 型データセット
			ArrayList s01KataList = s01Item.getS01KataList();
			Iterator itrKata = s01KataList.iterator();
			int row = 10;
			
			log.Ep("S01_01","型データセット開始","");
			
			while( itrKata.hasNext() ) {
				s01KataListBean = (S01KataListBean)itrKata.next();
				if (kataCnt == 0) {
					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);
					intMaxSheetIndex++;
					row = 10;
					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);

					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( cm.toString(s01Item.getStrMshmei()) + "　成績表");
			
					// 受験者数セット
					workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
					workCell.setCellValue( "受験者総数：" + nf.format(s01Item.getIntAllNinzu()) + "名　（現役：" + nf.format(s01Item.getIntGenNinzu()) + "名／高卒：" + nf.format(s01Item.getIntSotuNinzu()) + "名）");
				}
				// 型名セット
				log.It("S01_01","科目名",s01KataListBean.getStrKmkmei());
				workCell = cm.setCell( workSheet, workRow, workCell, row, 0 );
				workCell.setCellValue( s01KataListBean.getStrKmkmei() );
				// 配点セット
				log.It("S01_01","配点",s01KataListBean.getStrHaitenKmk());
				workCell = cm.setCell( workSheet, workRow, workCell, row, 1 );
				if ( !cm.toString(s01KataListBean.getStrHaitenKmk()).equals("") ) {
					workCell.setCellValue( Integer.parseInt( s01KataListBean.getStrHaitenKmk() ) );
				}
				// 平均点・全体セット
				log.It("S01_01","平均点・全体",String.valueOf(s01KataListBean.getFloHeikinAll()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 2 );
				if ( s01KataListBean.getFloHeikinAll() != -999.0 ) {
					workCell.setCellValue( s01KataListBean.getFloHeikinAll() );
				}
				// 平均点・現役セット
				log.It("S01_01","平均点・現役",String.valueOf(s01KataListBean.getFloHeikinGen()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 3 );
				if ( s01KataListBean.getFloHeikinGen() != -999.0 ) {
					workCell.setCellValue( s01KataListBean.getFloHeikinGen() );
				}
				// 平均点・高卒セット
				log.It("S01_01","",String.valueOf(s01KataListBean.getFloHeikinSotu()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 4 );
				if ( s01KataListBean.getFloHeikinSotu() != -999.0 ) {
					workCell.setCellValue( s01KataListBean.getFloHeikinSotu() );
				}
				// 標準偏差セット
				log.It("S01_01","平均点・高卒",String.valueOf(s01KataListBean.getFloStdHensa()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 5 );
				if ( s01KataListBean.getFloStdHensa() != -999.0 ) {
					workCell.setCellValue( s01KataListBean.getFloStdHensa() );
				}
				// 平均偏差値・全国セット
				log.It("S01_01","平均偏差値・全国",String.valueOf(s01KataListBean.getFloHensaAll()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 6 );
				if ( s01KataListBean.getFloHensaAll() != -999.0 ) {
					workCell.setCellValue( s01KataListBean.getFloHensaAll() );
				}
				// 平均偏差値・現役セット
				log.It("S01_01","平均偏差値・現役",String.valueOf(s01KataListBean.getFloHensaGen()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 7 );
				if ( s01KataListBean.getFloHensaGen() != -999.0 ) {
					workCell.setCellValue( s01KataListBean.getFloHensaGen() );
				}
				// 平均偏差値・高卒セット
				log.It("S01_01","平均偏差値・高卒",String.valueOf(s01KataListBean.getFloHensaSotu()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 8 );
				if ( s01KataListBean.getFloHensaSotu() != -999.0 ) {
					workCell.setCellValue( s01KataListBean.getFloHensaSotu() );
				}
				// 最高点
				log.It("S01_01","最高点",String.valueOf(s01KataListBean.getIntMaxTen()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 9 );
				if ( s01KataListBean.getIntMaxTen() != -999 ) {
					workCell.setCellValue( s01KataListBean.getIntMaxTen() );
				}
				// 最低点
				log.It("S01_01","最低点",String.valueOf(s01KataListBean.getIntMinTen()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 10 );
				if ( s01KataListBean.getIntMinTen() != -999.0 ) {
					workCell.setCellValue( s01KataListBean.getIntMinTen() );
				}
				// 人数・全国セット
				log.It("S01_01","人数・全国",String.valueOf(s01KataListBean.getIntNinzuAll()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 11 );
				if ( s01KataListBean.getIntNinzuAll() != -999 ) {
					workCell.setCellValue( s01KataListBean.getIntNinzuAll() );
				}
				// 人数・現役セット
				log.It("S01_01","人数・現役",String.valueOf(s01KataListBean.getIntNinzuGen()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 12 );
				if ( s01KataListBean.getIntNinzuGen() != -999 ) {
					workCell.setCellValue( s01KataListBean.getIntNinzuGen() );
				}
				// 人数・高卒セット
				log.It("S01_01","人数・高卒",String.valueOf(s01KataListBean.getIntNinzuSotu()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 13 );
				if ( s01KataListBean.getIntNinzuSotu()!= -999 ) {
					workCell.setCellValue( s01KataListBean.getIntNinzuSotu() );
				}
				row++;
				kataCnt++;
				log.It("S01_01","型カウント",String.valueOf(kataCnt));
				if (kataCnt >= 20) {
					kataCnt = 0;
				}
			}
			log.Ep("S01_01","型データセット終了","");

			// 科目データセット
			ArrayList s01KmkList = s01Item.getS01KmkList();
			Iterator itrKmk = s01KmkList.iterator();
			row = 39;
			
			log.Ep("S01_01","科目データセット開始","");
			
			if ( intMaxSheetIndex!=0 ) {
				if (intSaveFlg==1 || intSaveFlg==5) {
					workSheet = workbook.getSheetAt(kmkMaxSheetCnt);
				} else {
					workSheet = workbook.getSheetAt(kmkMaxSheetCnt+1);
				}
			}
			
			while( itrKmk.hasNext() ) {
				s01KmkListBean = (S01KmkListBean)itrKmk.next();
				if (kmkCnt == 0) {
					if (kmkMaxSheetCnt>intMaxSheetIndex) {
						// シートテンプレートのコピー
						workSheet = workbook.cloneSheet(0);
						intMaxSheetIndex++;
						kmkMaxSheetCnt++;
					} else {
						if (intSaveFlg==1 || intSaveFlg==5) {
							workSheet = workbook.getSheetAt(kmkMaxSheetCnt);
						} else {
							workSheet = workbook.getSheetAt(kmkMaxSheetCnt+1);
						}
						kmkMaxSheetCnt++;
					}
					row = 39;
					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);

					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( cm.toString(s01Item.getStrMshmei()) + "　成績表");
			
					// 受験者数セット
					workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
					workCell.setCellValue( "受験者総数：" + nf.format(s01Item.getIntAllNinzu()) + "名　（現役：" + nf.format(s01Item.getIntGenNinzu()) + "名／高卒：" + nf.format(s01Item.getIntSotuNinzu()) + "名）");
				}
				// 科目名セット
				log.It("S01_01","科目名",s01KmkListBean.getStrKmkmei());
				workCell = cm.setCell( workSheet, workRow, workCell, row, 0 );
				workCell.setCellValue( s01KmkListBean.getStrKmkmei() );
				// 配点セット
				log.It("S01_01","配点",s01KmkListBean.getStrHaitenKmk());
				workCell = cm.setCell( workSheet, workRow, workCell, row, 1 );
				if ( !cm.toString(s01KmkListBean.getStrHaitenKmk()).equals("") ) {
					workCell.setCellValue( Integer.parseInt( s01KmkListBean.getStrHaitenKmk() ) );
				}
				// 平均点・全体セット
				log.It("S01_01","平均点・全体",String.valueOf(s01KmkListBean.getFloHeikinAll()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 2 );
				if ( s01KmkListBean.getFloHeikinAll() != -999.0 ) {
					workCell.setCellValue( s01KmkListBean.getFloHeikinAll() );
				}
				// 平均点・現役セット
				log.It("S01_01","平均点・現役",String.valueOf(s01KmkListBean.getFloHeikinGen()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 3 );
				if ( s01KmkListBean.getFloHeikinGen() != -999.0 ) {
					workCell.setCellValue( s01KmkListBean.getFloHeikinGen() );
				}
				// 平均点・高卒セット
				log.It("S01_01","",String.valueOf(s01KmkListBean.getFloHeikinSotu()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 4 );
				if ( s01KmkListBean.getFloHeikinSotu() != -999.0 ) {
					workCell.setCellValue( s01KmkListBean.getFloHeikinSotu() );
				}
				// 標準偏差セット
				log.It("S01_01","平均点・高卒",String.valueOf(s01KmkListBean.getFloStdHensa()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 5 );
				if ( s01KmkListBean.getFloStdHensa() != -999.0 ) {
					workCell.setCellValue( s01KmkListBean.getFloStdHensa() );
				}
				// 平均偏差値・全国セット
				log.It("S01_01","平均偏差値・全国",String.valueOf(s01KmkListBean.getFloHensaAll()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 6 );
				if ( s01KmkListBean.getFloHensaAll() != -999.0 ) {
					workCell.setCellValue( s01KmkListBean.getFloHensaAll() );
				}
				// 平均偏差値・現役セット
				log.It("S01_01","平均偏差値・現役",String.valueOf(s01KmkListBean.getFloHensaGen()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 7 );
				if ( s01KmkListBean.getFloHensaGen() != -999.0 ) {
					workCell.setCellValue( s01KmkListBean.getFloHensaGen() );
				}
				// 平均偏差値・高卒セット
				log.It("S01_01","平均偏差値・高卒",String.valueOf(s01KmkListBean.getFloHensaSotu()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 8 );
				if ( s01KmkListBean.getFloHensaSotu() != -999.0 ) {
					workCell.setCellValue( s01KmkListBean.getFloHensaSotu() );
				}
				// 最高点
				log.It("S01_01","最高点",String.valueOf(s01KmkListBean.getIntMaxTen()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 9 );
				if ( s01KmkListBean.getIntMaxTen() != -999 ) {
					workCell.setCellValue( s01KmkListBean.getIntMaxTen() );
				}
				// 最低点
				log.It("S01_01","最低点",String.valueOf(s01KmkListBean.getIntMinTen()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 10 );
				if ( s01KmkListBean.getIntMinTen() != -999 ) {
					workCell.setCellValue( s01KmkListBean.getIntMinTen() );
				}
				// 人数・全国セット
				log.It("S01_01","人数・全国",String.valueOf(s01KmkListBean.getIntNinzuAll()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 11 );
				if ( s01KmkListBean.getIntNinzuAll() != -999 ) {
					workCell.setCellValue( s01KmkListBean.getIntNinzuAll() );
				}
				// 人数・現役セット
				log.It("S01_01","人数・現役",String.valueOf(s01KmkListBean.getIntNinzuGen()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 12 );
				if ( s01KmkListBean.getIntNinzuGen() != -999 ) {
					workCell.setCellValue( s01KmkListBean.getIntNinzuGen() );
				}
				// 人数・高卒セット
				log.It("S01_01","人数・高卒",String.valueOf(s01KmkListBean.getIntNinzuSotu()));
				workCell = cm.setCell( workSheet, workRow, workCell, row, 13 );
				if ( s01KmkListBean.getIntNinzuSotu() != -999 ) {
					workCell.setCellValue( s01KmkListBean.getIntNinzuSotu() );
				}
				row++;
				kmkCnt++;
				log.It("S01_01","科目カウント",String.valueOf(kmkCnt));
				if (kmkCnt >= 40) {
					kmkCnt = 0;
				}
			}
			log.Ep("S01_01","科目データセット終了","");
//add 2004/10/25 T.Sakai データ0件対応
			if ( s01KataList.size()==0 && s01KmkList.size()==0 ) {
				// シートテンプレートのコピー
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex++;
				row = 10;
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.toString(s01Item.getStrMshmei()) + "　成績表");
				
				// 受験者数セット
				workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
				workCell.setCellValue( "受験者総数：" + nf.format(s01Item.getIntAllNinzu()) + "名　（現役：" + nf.format(s01Item.getIntGenNinzu()) + "名／高卒：" + nf.format(s01Item.getIntSotuNinzu()) + "名）");
			}
//add end
			// Excelファイル保存
			boolean bolRet = false;
			bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch( Exception e ) {
			log.Err("S01_01","データセットエラー",e.toString());
			return errfdata;
		}
		
		return noerror;
	}

}

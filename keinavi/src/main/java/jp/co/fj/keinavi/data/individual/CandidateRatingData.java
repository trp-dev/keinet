/*
 * 作成日: 2004/07/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

import java.io.Serializable;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CandidateRatingData implements Serializable{

	private String individualId;
	private String examYear;
	private String examCd;
	private String candidateRank;
	private String univCd;
	private String facultyCd;
	private String deptCd;
	private String bundleCd;
	private String ratingCvsScore;
	private String ratingDeviation;
	private String ratingPoint;
	private String markGRating;
	private String discriptionGRating;
	private String totalRating;
	private String totalCandidateNum;
	private String totalCandidateRank;
	//テーブルにはないが必要
	private String superAbbrName;
	private String cenBorder;
	private String rankLimits;
	//2019/08/27 Hics)Ueta 共通テスト対応 ADD START
	private String cenBorderNoexa;
	private String entexamfullpntCenter;
	private String entexamfullpntCenterNoexa;
	private String ratingCvsScore_engpt;
	private String markGRating_engpt;
	//2019/08/27 Hics)Ueta 共通テスト対応 ADD END


	private HS3UnivNameData hs3UnivNameData;

	public String getCenterRating(){
		return markGRating;
	}
	public void setCenterRating(String string){
		markGRating = string;
	}
	public String getSecondRating(){
		return discriptionGRating;
	}
	public void setSecondRating(String string){
		discriptionGRating = string;
	}

	/**
	 * @return
	 */
	public String getBundleCd() {
		return bundleCd;
	}

	/**
	 * @return
	 */
	public String getCandidateRank() {
		return candidateRank;
	}

	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getDiscriptionGRating() {
		return discriptionGRating;
	}

	/**
	 * @return
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @return
	 */
	public String getMarkGRating() {
		return markGRating;
	}

	/**
	 * @return
	 */
	public String getRatingCvsScore() {
		return ratingCvsScore;
	}

	/**
	 * @return
	 */
	public String getRatingDeviation() {
		return ratingDeviation;
	}

	/**
	 * @return
	 */
	public String getRatingPoint() {
		return ratingPoint;
	}

	/**
	 * @return
	 */
	public String getTotalCandidateNum() {
		return totalCandidateNum;
	}

	/**
	 * @return
	 */
	public String getTotalCandidateRank() {
		return totalCandidateRank;
	}

	/**
	 * @return
	 */
	public String getTotalRating() {
		return totalRating;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	//2019/08/27 Hics)Ueta 共通テスト対応 ADD START
	/**
	 * @return
	 */
	public String getCenBorderNoexa() {
		return cenBorderNoexa;
	}

	/**
	 * @return
	 */
	public String getEntexamfullpntCenter() {
		return entexamfullpntCenter;
	}

	/**
	 * @return
	 */
	public String getEntexamfullpntCenterNoexa() {
		return entexamfullpntCenterNoexa;
	}

	/**
	 * @return
	 */
	public String getRatingCvsScore_engpt() {
		return ratingCvsScore_engpt;
	}

	/**
	 * @return
	 */
	public String getCenterRating_engpt(){
		return markGRating_engpt;
	}

	/**
	 * @return
	 */
	public String getMarkGRating_engpt() {
		return markGRating_engpt;
	}
	//2019/08/27 Hics)Ueta 共通テスト対応 ADD END

	/**
	 * @param string
	 */
	public void setBundleCd(String string) {
		bundleCd = string;
	}

	/**
	 * @param string
	 */
	public void setCandidateRank(String string) {
		candidateRank = string;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setDiscriptionGRating(String string) {
		discriptionGRating = string;
	}

	/**
	 * @param string
	 */
	public void setExamCd(String string) {
		examCd = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

	/**
	 * @param string
	 */
	public void setMarkGRating(String string) {
		markGRating = string;
	}

	/**
	 * @param string
	 */
	public void setRatingCvsScore(String string) {
		ratingCvsScore = string;
	}

	/**
	 * @param string
	 */
	public void setRatingDeviation(String string) {
		ratingDeviation = string;
	}

	/**
	 * @param string
	 */
	public void setRatingPoint(String string) {
		ratingPoint = string;
	}

	/**
	 * @param string
	 */
	public void setTotalCandidateNum(String string) {
		totalCandidateNum = string;
	}

	/**
	 * @param string
	 */
	public void setTotalCandidateRank(String string) {
		totalCandidateRank = string;
	}

	/**
	 * @param string
	 */
	public void setTotalRating(String string) {
		totalRating = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}


	/**
	 * @return
	 */
	public String getSuperAbbrName() {
		return superAbbrName;
	}

	/**
	 * @param string
	 */
	public void setSuperAbbrName(String string) {
		superAbbrName = string;
	}

	/**
	 * @return
	 */
	public String getCenBorder() {
		return cenBorder;
	}

	/**
	 * @return
	 */
	public String getRankLimits() {
		return rankLimits;
	}

	/**
	 * @param string
	 */
	public void setCenBorder(String string) {
		cenBorder = string;
	}

	/**
	 * @param string
	 */
	public void setRankLimits(String string) {
		rankLimits = string;
	}

	//2019/08/27 Hics)Ueta 共通テスト対応 ADD START
	/**
	 * @param string
	 */
	public void setCenBorderNoexa(String string) {
		cenBorderNoexa = string;
	}

	/**
	 * @param string
	 */
	public void setEntexamfullpntCenter(String string) {
		entexamfullpntCenter = string;
	}

	/**
	 * @param string
	 */
	public void setEntexamfullpntCenterNoexa(String string) {
		entexamfullpntCenterNoexa = string;
	}

	/**
	 * @param string
	 */
	public void setRatingCvsScore_engpt(String ratingCvsScore_engpt) {
		this.ratingCvsScore_engpt = ratingCvsScore_engpt;
	}

	/**
	 * @param string
	 */
	public void setCenterRating_engpt(String string){
		markGRating_engpt = string;
	}

	/**
	 * @param string
	 */
	public void setMarkGRating_engpt(String string) {
		markGRating_engpt = string;
	}
	//2019/08/27 Hics)Ueta 共通テスト対応 ADD END
}

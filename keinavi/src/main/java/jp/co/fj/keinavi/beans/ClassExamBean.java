package jp.co.fj.keinavi.beans;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.com_set.ChargeClassData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * クラス成績分析用の模試セッションを作るBean
 * 
 * 2004.07.14	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class ClassExamBean extends DefaultBean 
		implements IProfileCategory, IProfileItem {

	// 公開済み模試コードセット
	private final Set publicExamSet;
	
	// 模試セッション
	private ExamSession examSession = new ExamSession();
	// ログインセッション
	private LoginSession loginSession;
	// プロファイル
	private Profile profile;
	
	/**
	 * @param pSet 公開済み模試コードセット
	 */
	public ClassExamBean(final Set pSet) {
		this.publicExamSet = pSet;
	}
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		// 一時表セットアップ
		if (!inserIntoCountChargeClass()) {
			// 失敗ならここまで
			examSession.setYears(new String[0]);
			return;
		}
		
		// 担当クラスの模試情報を取得する
		setupClassExamInfo();
		// 今年度の公開済み模試を取得する
		setupPublicExamInfo();
		// 年度のリストを作る
		setupYearList();
	}

	private boolean inserIntoCountChargeClass() throws SQLException {

		// 担当クラスリスト
		final List charge = (List) profile.getItemMap(CM).get(CLASS);

		// 担当クラスがなければここまで
		if (charge.isEmpty()) {
			return false;
		}

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					"INSERT INTO countchargeclass VALUES(?, ?, 0)");
			for (final Iterator ite = charge.iterator(); ite.hasNext();) {
				final ChargeClassData data = (ChargeClassData) ite.next();
				ps.setInt(1, data.getGrade());
				ps.setString(2, data.getClassName());
				ps.executeUpdate();
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}

		return true;
	}
	
	private void setupClassExamInfo() throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// SQL
			final Query query = QueryLoader.getInstance().load("w06");
				
			// ヘルプデスクなら内部データ開放日
			if (loginSession.isHelpDesk()) {
				query.replaceAll("out_dataopendate", "in_dataopendate");
			}
			
			ps = conn.prepareStatement(query.toString());
			// 一括コード
			ps.setString(1, profile.getBundleCD());
			// 担当クラスの年度
			ps.setString(2, ProfileUtil.getChargeClassYear(profile));
			// 学校コード
			ps.setString(3, loginSession.getUserID());

			// 模試マップをセットアップ
			rs = ps.executeQuery();
			while (rs.next()) {
				
				// 模試年度
				final String examYear = rs.getString(1);

				// 模試マップから年度をキーとして模試リストを取得する
				final List list;
				if (examSession.getExamMap().containsKey(examYear)) {
					list = (List) examSession.getExamList(examYear);
				// なければ作る
				} else {
					list = new ArrayList();
					examSession.getExamMap().put(examYear, list);
				}

				// 模試データを作る
				final ExamData data = new ExamData();
				data.setExamYear(rs.getString(1));
				data.setExamCD(rs.getString(2));
				data.setExamName(rs.getString(3));
				data.setExamNameAbbr(rs.getString(4));
				data.setExamTypeCD(rs.getString(5));
				data.setExamST(rs.getString(6));
				data.setExamDiv(rs.getString(7));
				data.setTargetGrade(rs.getString(8));
				data.setInpleDate(rs.getString(9));
				data.setDataOpenDate(rs.getString(10));
				data.setOutDataOpenDate(rs.getString(10));
				data.setInDataOpenDate(rs.getString(10));
				data.setDockingExamCD(rs.getString(11));
				data.setDockingType(rs.getString(12));
				data.setDispSequence(rs.getInt(13));
				data.setMasterDiv(rs.getString(14));
				list.add(data);	
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	private void setupPublicExamInfo() throws SQLException {
		// 校内の値を引き継ぐ
		examSession.getPublicExamSet().addAll(publicExamSet);
	}

	private void setupYearList() {
		
		final List list = new ArrayList(
				examSession.getExamMap().keySet());
		// ソート
		Collections.sort(list);
		// 降順
		Collections.reverse(list);
		// セット
		examSession.setYears(
				(String[]) list.toArray(new String[list.size()]));
	}
	
	/**
	 * @return examSession を戻します。
	 */
	public ExamSession getExamSession() {
		return examSession;
	}

	/**
	 * @param pLoginSession 設定する loginSession。
	 */
	public void setLoginSession(final LoginSession pLoginSession) {
		loginSession = pLoginSession;
	}

	/**
	 * @param pProfile 設定する profile。
	 */
	public void setProfile(final Profile pProfile) {
		profile = pProfile;
	}

}

package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.keinavi.beans.sheet.common.AbstractExtSheetBean;
import jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator;
import jp.co.fj.keinavi.beans.sheet.common.ISheetData;
import jp.co.fj.keinavi.beans.sheet.excel.school.S42_15ExcelCreator;
import jp.co.fj.keinavi.beans.sheet.excel.school.S42_16ExcelCreator;
import jp.co.fj.keinavi.beans.sheet.excel.school.S42_17ExcelCreator;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15BundleListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15Data;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15ReachLevelListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15ScoreZoneListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15SubjectListData;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.sheet.SheetSchoolData;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 校内成績分析 - 他校比較 - 偏差値分布
 * 受験学力測定テスト用のSheetBean
 *
 * 2006.08.24	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class S42AbilitySheetBean extends AbstractExtSheetBean
        implements IProfileCategory, IProfileItem {

    // 帳票データ
    private final S42_15Data data = (S42_15Data) createData();
    // PreparedStatementの入れ物
    // キャッシュ機能は別クラスにまとめたいが・・・
    private final Map statementCache = new HashMap();

    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws Exception {

        // 科目変換テーブルのセットアップ
        insertIntoSubCDTrans(new ExamData[]{exam});
        // 基本情報のセット
        setBasicInfo();
        // 科目情報のセット
        setSubjectInfo();
        // センター到達エリアのセット
        setCenterInfo();
        // 一括データのセット
        setBundleInfo();
        // スコア帯データのセット
        setScoreInfo();
    }

    /**
     * @return データを取得する一括データリスト
     */
    protected List createCompSchoolList() {
        // 自校＋比較対象高校設定値
        // 自校が先頭に来る
        final List list = super.getCompSchoolList();
        list.add(0, new SheetSchoolData(profile.getBundleCD()));
        return list;
    }

    // 出力する年度配列を作る
    private String[] createYears() {

        // 今年度のみ
        if (data.getDispPast() == 1) {
            return new String[] { exam.getExamYear() };
        // 前年度まで
        } else if (data.getDispPast() == 2) {
            final int current = Integer.parseInt(exam.getExamYear());
            return new String[] {
                exam.getExamYear(),
                String.valueOf(current - 1)
            };
        // 前々年度まで
        } else if (data.getDispPast() == 3) {
            final int current = Integer.parseInt(exam.getExamYear());
            return new String[] {
                exam.getExamYear(),
                String.valueOf(current - 1),
                String.valueOf(current - 2)
            };
        // 不正
        } else {
            throw new IllegalArgumentException(
                    "過年度の表示の設定値が不正です。");
        }
    }

    // 基本情報のセット
    private void setBasicInfo() {
        // 一括コード
        data.setBundleCd(profile.getBundleCD());
        // 学校名
        data.setBundleName(profile.getBundleName());
        // 模試年度
        data.setExamYear(exam.getExamYear());
        // 模試コード
        data.setExamCd(exam.getExamCD());
        // 模試名
        data.setExamName(exam.getExamName());
        // 実施日
        data.setInpleDate(exam.getInpleDate());
        // 過年度の表示
        data.setDispPast(getIntFlag(PREV_DISP));
        // セキュリティスタンプ
        data.setSecurityStamp(getIntFlag(PRINT_STAMP));
    }

    // 科目情報のセット
    protected void setSubjectInfo() throws Exception {

        // 型・科目設定値
        final String[] subCdArray = getSubjectCDArray();
        if (subCdArray.length == 0) {
            throw new Exception("成績データがありません。");
        }

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            final Query query = QueryLoader.getInstance().load("s22_1");
            // 科目コードをセット
            query.setStringArray(1, subCdArray);

            ps = conn.prepareStatement(query.toString());
            // 模試年度
            ps.setString(1, data.getExamYear());
            // 模試コード
            ps.setString(2, data.getExamCd());

            rs = ps.executeQuery();
            while (rs.next()) {
                final S42_15SubjectListData s = data.createSubjectListData();
                s.setSubCd(rs.getString(1));
                s.setSubName(rs.getString(2));
                data.addSubjectListData(s);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    // センター到達エリア情報を取得してセットする
    protected void setCenterInfo() throws SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(
                    QueryLoader.getInstance().getQuery("s42_3_1"));
            // 模試年度
            ps.setString(1, data.getExamYear());
            ps.setString(3, data.getExamYear());
            // 模試コード
            ps.setString(4, data.getExamCd());
            ps.setString(6, data.getExamCd());

            // 全ての科目について取得する
            for (final Iterator ite = data.getSubjectListData(
                    ).iterator(); ite.hasNext();) {

                final S42_15SubjectListData s = (
                        S42_15SubjectListData) ite.next();

                // 科目コード
                ps.setString(2, s.getSubCd());
                ps.setString(5, s.getSubCd());

                rs = ps.executeQuery();
                while (rs.next()) {
                    final S42_15ReachLevelListData r = data.createReachLevelListData();
                    r.setDevZoneCd(rs.getString(1));
                    r.setHighLimit(rs.getDouble(2));
                    r.setLowLimit(rs.getDouble(3));
                    r.setCenterReachArea(rs.getString(4));
                    r.setDispStr_CenterReachArea(rs.getString(5));
                    r.setCenterExScoreRate((String)KNUtil.CENTER_EXSCORERATE_MAP.get(rs.getString(4)));
                    s.addReachLevelListData(r);
                }
                rs.close();
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    // 一括データのセット
    private void setBundleInfo() throws SQLException {

        // 比較対象高校リスト
        final List schoolList = createCompSchoolList();

        try {
            for (final Iterator ite = schoolList.iterator(
                    ); ite.hasNext();) {

                final SheetSchoolData school = (SheetSchoolData) ite.next();
                final String bundleCd = school.getSchoolCD();

                // Statementは種別により異なる
                final PreparedStatement ps;
                // 全国
                if (isWholeBundle(bundleCd)) {
                    ps = getBundleWholeStatement();
                    // データセット
                    setBundleInfo(ps, 3);
                // 県
                } else if (isPrefBundle(bundleCd)) {
                    ps = getBundlePrefStatement();
                    // 県コード
                    ps.setString(1, bundleCd.substring(0, 2));
                    // データセット
                    setBundleInfo(ps, 3);
                // 高校
                } else {
                    ps = getBundleSchoolStatement();
                    // 一括コード
                    ps.setString(1, bundleCd);
                    // データセット
                    setBundleInfo(ps, 3);
                }
            }
        } finally {
            closeAllStatement();
        }
    }

    // 高校リスト取得Statement（全国）
    private PreparedStatement getBundleWholeStatement() throws SQLException {

        PreparedStatement ps = getPreparedStatement("WHOLE");

        // 初期化
        if (ps == null) {
            ps = createBundleStatement("s42_3_2");
            // 現役高卒区分
            ps.setString(1, getMinStudentDiv());
            // 模試コード
            ps.setString(2, data.getExamCd());
            // キャッシュする
            statementCache.put("WHOLE", ps);
        }

        return ps;
    }

    // 高校リスト取得Statement（県）
    private PreparedStatement getBundlePrefStatement() throws SQLException {

        PreparedStatement ps = getPreparedStatement("PREF");

        // 初期化
        if (ps == null) {
            ps = createBundleStatement("s42_3_3");
            // 模試コード
            ps.setString(2, data.getExamCd());
            // キャッシュする
            statementCache.put("PREF", ps);
        }

        return ps;
    }

    // 高校リスト取得Statement（高校）
    private PreparedStatement getBundleSchoolStatement() throws SQLException {

        PreparedStatement ps = getPreparedStatement("SCHOOL");

        // 初期化
        if (ps == null) {
            ps = createBundleStatement("s42_3_4");
            // 模試コード
            ps.setString(2, data.getExamCd());
            // キャッシュする
            statementCache.put("SCHOOL", ps);
        }

        return ps;
    }

    // 高校リスト取得Statement（共通処理）
    private PreparedStatement createBundleStatement(
            final String id) throws SQLException {

        final Query query = QueryLoader.getInstance().load(id);

        // 出力対象年度
        query.setStringArray(1, createYears());
        // データ開放日書き換え
        outDate2InDate(query);

        return conn.prepareStatement(query.toString());
    }

    private void setBundleInfo(final PreparedStatement ps,
            final int index) throws SQLException {

        ResultSet rs = null;
        try {
            // 全ての科目について取得する
            for (final Iterator ite = data.getSubjectListData(
                    ).iterator(); ite.hasNext();) {

                final S42_15SubjectListData s = (
                        S42_15SubjectListData) ite.next();

                // 科目コード
                ps.setString(index, s.getSubCd());

                rs = ps.executeQuery();
                while (rs.next()) {
                    final S42_15BundleListData b = data.createBundleListData();
                    b.setBundleCd(rs.getString(1));
                    b.setBundleName(rs.getString(2));
                    b.setExamYear(rs.getString(3));
                    b.setSubCd(rs.getString(4));
                    b.setAvgPnt(rs.getDouble(5));
                    b.setNumbers(rs.getInt(6));
                    s.addBundleListData(b);
                }
                rs.close();
            }
        } finally {
            DbUtils.closeQuietly(rs);
        }
    }

    // スコア帯リストデータをセットする
    private void setScoreInfo() throws SQLException {

        try {
            for (final Iterator ite = data.getSubjectListData(
                    ).iterator(); ite.hasNext();) {
                final S42_15SubjectListData s = (
                        S42_15SubjectListData) ite.next();
                setScoreInfo(s.getBundleListData());
            }
        } finally {
            closeAllStatement();
        }
    }

    private void setScoreInfo(
            final List bundleListData) throws SQLException {

        for (final Iterator ite = bundleListData.iterator(
                ); ite.hasNext();) {

            final S42_15BundleListData b = (S42_15BundleListData) ite.next();

            // 科目コードが無ければ処理しない
            // ※成績が存在しない過年度など
            if (b.getSubCd() == null) {
                continue;
            }

            // Statementは種別により異なる
            final PreparedStatement ps;
            // 全国
            if (isWholeBundle(b.getBundleCd())) {
                ps = getScoreWholeStatement();
                // 模試年度
                ps.setString(1, b.getExamYear());
                // 科目コード
                ps.setString(2, b.getSubCd());
            // 県
            } else if (isPrefBundle(b.getBundleCd())) {
                ps = getScorePrefStatement();
                // 模試年度
                ps.setString(1, b.getExamYear());
                // 科目コード
                ps.setString(2, b.getSubCd());
                // 県コード
                ps.setString(3, b.getBundleCd().substring(0, 2));
            // 高校
            } else {
                ps = getScoreSchoolStatement();
                // 模試年度
                ps.setString(1, b.getExamYear());
                // 科目コード
                ps.setString(2, b.getSubCd());
                // 一括コード
                ps.setString(3, b.getBundleCd());
            }

            ResultSet rs = null;
            try {
                rs = ps.executeQuery();
                while (rs.next()) {
                    final S42_15ScoreZoneListData s = data.createScoreZoneListData();
                    s.setDevZoneCd(rs.getString(1));
                    s.setHighLimit(rs.getDouble(2));
                    s.setLowLimit(rs.getDouble(3));
                    s.setNumbers(rs.getInt(4));
                    b.addScoreZoneListData(s);
                }
            } finally {
                DbUtils.closeQuietly(rs);
            }
        }
    }

    // スコア帯リスト取得Statement（全国）
    private PreparedStatement getScoreWholeStatement() throws SQLException {

        PreparedStatement ps = getPreparedStatement("WHOLE");

        // 初期化
        if (ps == null) {
            ps = conn.prepareStatement(
                    QueryLoader.getInstance().getQuery("s42_3_5"));
            // 模試コード
            ps.setString(3, data.getExamCd());
            // キャッシュする
            statementCache.put("WHOLE", ps);
        }

        return ps;
    }

    // スコア帯リスト取得Statement（県）
    private PreparedStatement getScorePrefStatement() throws SQLException {

        PreparedStatement ps = getPreparedStatement("PREF");

        // 初期化
        if (ps == null) {
            ps = conn.prepareStatement(
                    QueryLoader.getInstance().getQuery("s42_3_6"));
            // 模試コード
            ps.setString(4, data.getExamCd());
            // キャッシュする
            statementCache.put("PREF", ps);
        }

        return ps;
    }

    // スコア帯リスト取得Statement（高校）
    private PreparedStatement getScoreSchoolStatement() throws SQLException {

        PreparedStatement ps = getPreparedStatement("SCHOOL");

        // 初期化
        if (ps == null) {
            ps = conn.prepareStatement(
                    QueryLoader.getInstance().getQuery("s42_3_7"));
            // 模試コード
            ps.setString(4, data.getExamCd());
            // キャッシュする
            statementCache.put("SCHOOL", ps);
        }

        return ps;
    }

    // キャッシュしているPreparedStatementを返す
    private PreparedStatement getPreparedStatement(final String id) {
        return (PreparedStatement) statementCache.get(id);
    }

    // キャッシュしているStatementをすべて閉じる
    private void closeAllStatement() {

        for (final Iterator ite = statementCache.values(
                ).iterator(); ite.hasNext();) {
            final Statement ps = (Statement) ite.next();
            DbUtils.closeQuietly(ps);
        }
        statementCache.clear();
    }

    /**
     * @return データクラス
     */
    protected ISheetData createData() {
        return new S42_15Data();
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
     */
    protected double calcNumberOfPrint() {
        return getNumberOfPrint("S42_01");
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
     */
    protected String getCategory() {
        return S_OTHER_DEV;
    }

    /**
     * @return data を戻します。
     */
    protected S42_15Data getData() {
        return data;
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.AbstractExtSheetBean#create()
     */
    protected void create() throws Exception {

        final BaseExcelCreator creator;

        // 今年度のみ
        if (data.getDispPast() == 1) {
            creator = new S42_15ExcelCreator(
                    data, sessionKey, outfileList, getAction(), 8, 11);
            sheetLog.add("S42_15");
        // 前年度まで
        } else if (data.getDispPast() == 2) {
            creator = new S42_16ExcelCreator(
                    data, sessionKey, outfileList, getAction(), 9, 12);
            sheetLog.add("S42_16");
        // 前々年度まで
        } else {
            creator = new S42_17ExcelCreator(
                    data, sessionKey, outfileList, getAction(), 9, 12);
            sheetLog.add("S42_17");
        }

        creator.execute();
    }

}

package jp.co.fj.keinavi.forms.maintenance;

import jp.co.fj.keinavi.data.maintenance.KNClassData;
import jp.co.fj.keinavi.forms.BaseForm;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * 生徒情報一覧画面アクションフォーム
 * 
 * 2005.10.19	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M101Form extends BaseForm {

	// 動作モード
	private String actionMode;
	// 対象年度
	private String targetYear;
	// 対象学年
	private String targetGrade;
	// 対象クラス
	private String targetClass;
	// 対象個人ID
	private String targetIndividualId;
	// 絞り込み条件
	// 0 ... 指定なし
	// 1 ... かな氏名
	private String conditionSelect = "0";
	// 絞り込み条件（テキスト）
	private String conditionText = "";
	// 絞り込み条件（サブ条件）
	// 1 ... と一致
	// 2 ... から始まる
	// 3 ... を含む
	// 4 ... あいまい検索
	private String conditionSubSelect = "1";
	// 表示形式
	// 1 ... 生徒情報
	// 2 ... 模試結合状況
	private String displayStyle = "1";
	//「従」とする個人ID
	private String targetSubIndividualId;
	// 仮受験者
	private String[] tempExaminee = new String[0];
	// （画面内の）登録済み仮受験者
	private String[] registedTempExaminee = new String[0];
	// ソートキー
	// 1 ... 学年・クラス・クラス番号（前々年度）
	// 2 ... 学年・クラス・クラス番号（前年度）
	// 3 ... 学年・クラス・クラス番号（今年度）
	// 4 ... かな氏名
	// 5 ... 漢字氏名
	// 6 ... 生年月日
	// 7 ... 電話番号
	// ※降順は+10
	private String sortKey = "3";
	// 模試表示ページ
	private int pageNo = 1;
	
	/**
	 * フォームの初期化処理を行う
	 * 
	 * @param data クラスデータ
	 */
	public void init(final KNClassData data) {
		if (data == null) {
			setTargetYear(null);
			setTargetGrade("all");
			setTargetClass("all");
		} else {
			setTargetYear(data.getYear());
			setTargetGrade(data.getGrade() + "");
			setTargetClass(data.getClassName());
			// ソートキーの初期値は選択年度の学年・クラス・クラス番号
			final int cYear = Integer.parseInt(KNUtil.getCurrentYear());
			final int dYear = Integer.parseInt(data.getYear());
			setSortKey((3 - cYear + dYear) + "");
		}
	}
	
	/**
	 * @return pageNo を戻します。
	 */
	public int getPageNo() {
		return pageNo;
	}
	
	/**
	 * @param pPageNo 設定する pageNo。
	 */
	public void setPageNo(final String pPageNo) {
		pageNo = Integer.parseInt(pPageNo);
	}
	
	/**
	 * 表示ページを左に移動する
	 */
	public void toLeftPage() {
		pageNo--;
	}
	
	/**
	 * 表示ページを右に移動する
	 */
	public void toRightPage() {
		pageNo++;
	}
	
	/**
	 * @return actionMode を戻します。
	 */
	public String getActionMode() {
		return actionMode;
	}
	/**
	 * @param s 設定する actionMode。
	 */
	public void setActionMode(final String s) {
		this.actionMode = s;
	}
	/**
	 * @return targetClass を戻します。
	 */
	public String getTargetClass() {
		return targetClass;
	}
	/**
	 * @param s 設定する targetClass。
	 */
	public void setTargetClass(final String s) {
		this.targetClass = s;
	}
	/**
	 * @return targetGrade を戻します。
	 */
	public String getTargetGrade() {
		return targetGrade;
	}
	/**
	 * @param s 設定する targetGrade。
	 */
	public void setTargetGrade(final String s) {
		this.targetGrade = s;
	}
	/**
	 * @return targetIndividualId を戻します。
	 */
	public String getTargetIndividualId() {
		return targetIndividualId;
	}
	/**
	 * @param s 設定する targetIndividualId。
	 */
	public void setTargetIndividualId(final String s) {
		this.targetIndividualId = s;
	}
	/**
	 * @return targetYear を戻します。
	 */
	public String getTargetYear() {
		return targetYear;
	}
	/**
	 * @param s 設定する targetYear。
	 */
	public void setTargetYear(final String s) {
		this.targetYear = s;
	}
	public void validate() {
	}
	/**
	 * @return conditionSelect を戻します。
	 */
	public String getConditionSelect() {
		return conditionSelect;
	}
	/**
	 * @param pConditionSelect 設定する conditionSelect。
	 */
	public void setConditionSelect(String pConditionSelect) {
		conditionSelect = pConditionSelect;
	}
	/**
	 * @return conditionText を戻します。
	 */
	public String getConditionText() {
		return conditionText;
	}
	/**
	 * @param pConditionText 設定する conditionText。
	 */
	public void setConditionText(String pConditionText) {
		conditionText = pConditionText;
	}
	/**
	 * @return displayStyle を戻します。
	 */
	public String getDisplayStyle() {
		return displayStyle;
	}
	/**
	 * @param pDisplayStyle 設定する displayStyle。
	 */
	public void setDisplayStyle(String pDisplayStyle) {
		displayStyle = pDisplayStyle;
	}
	/**
	 * @return targetSubIndividualId を戻します。
	 */
	public String getTargetSubIndividualId() {
		return targetSubIndividualId;
	}
	/**
	 * @param pTargetSubIndividualId 設定する targetSubIndividualId。
	 */
	public void setTargetSubIndividualId(String pTargetSubIndividualId) {
		targetSubIndividualId = pTargetSubIndividualId;
	}
	/**
	 * @return sortKey を戻します。
	 */
	public String getSortKey() {
		return sortKey;
	}
	/**
	 * @param pSortKey 設定する sortKey。
	 */
	public void setSortKey(String pSortKey) {
		sortKey = pSortKey;
	}
	/**
	 * @return registedTempExaminee を戻します。
	 */
	public String[] getRegistedTempExaminee() {
		return registedTempExaminee;
	}
	/**
	 * @param pRegistedTempExaminee 設定する registedTempExaminee。
	 */
	public void setRegistedTempExaminee(String[] pRegistedTempExaminee) {
		registedTempExaminee = pRegistedTempExaminee;
	}
	/**
	 * @return tempExaminee を戻します。
	 */
	public String[] getTempExaminee() {
		return tempExaminee;
	}
	/**
	 * @param pTempExaminee 設定する tempExaminee。
	 */
	public void setTempExaminee(String[] pTempExaminee) {
		tempExaminee = pTempExaminee;
	}
	/**
	 * @return conditionSubSelect を戻します。
	 */
	public String getConditionSubSelect() {
		return conditionSubSelect;
	}
	/**
	 * @param pConditionSubSelect 設定する conditionSubSelect。
	 */
	public void setConditionSubSelect(String pConditionSubSelect) {
		conditionSubSelect = pConditionSubSelect;
	}

}

/*
 * 作成日: 2004/11/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.individual.GuideRemarksData;

import com.fjh.beans.DefaultSearchBean;

/**
 * @author T.Yamada
 * ガイドライン備考の取得
 */
public class GuideRemarksSearchBean extends DefaultSearchBean{

	private String univCd;
	private String facultyCd;
	private String deptCd;

	private String query = ""+
	"SELECT "+
	"UNIVCD "+",FACULTYCD "+",DEPTCD "+",REMARKS "+
	"FROM GUIDEREMARKS WHERE 1 = 1 ";

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		if(univCd != null){
			query += "AND UNIVCD = '"+univCd+"' ";
		}
		if(facultyCd != null){
			query += "AND FACULTYCD = '"+facultyCd+"' ";
		}
		if(deptCd != null){
			query += "AND DEPTCD = '"+deptCd+"' ";
		}
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while(rs.next()){
				GuideRemarksData grd = new GuideRemarksData();
				grd.setUnivCd(rs.getString("UNIVCD"));
				grd.setFacultyCd(rs.getString("FACULTYCD"));
				grd.setDeptCd(rs.getString("DEPTCD"));
				grd.setRemarks(rs.getString("REMARKS"));
				
				recordSet.add(grd);
			}
		}catch(SQLException sqle){
			System.out.println(query);
			throw sqle;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
		
	}

	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

}

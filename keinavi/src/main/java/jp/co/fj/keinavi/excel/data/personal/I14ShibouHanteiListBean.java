package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
//import java.util.ArrayList;
/**
 * �l���ѕ��́|�󌱗p�����|�u�]�Z���� �u�]�Z����p�f�[�^���X�g
 * �쐬��: 2004/07/21
 * @author	A.Iwata
 */
public class I14ShibouHanteiListBean {
	//�u�]����
	private String strShiboRank = "";
	//�s���{��
	private String strKenmei = "";
	//��w�敪
	private String strDaigakuKbn = "";
	//��w��
	private String strDaigakuMei = "";
	//�w��
	private String strGakubuMei = "";
	//�w��
	private String strGakkaMei = "";
//S 2004.09.03 A.Hasegawa
	//�����p�^�[���t���O
	private int intPluralFlg = 0;
//E 2004.09.03
	//��W�l��
	private String strBoshuNinzu = "";
	//����M��
	private String strExamCapaRel = "";
	//�Z���^�[�����@�]�����_
	private int intHyoukaTokuten = 0;
	//�Z���^�[�����@�]��
	private String strHyoukaMark = "";
	//�ʎ����@�]���΍��l
	private float floKijyutsuHensa = 0;
	//�ʎ����@�]��
	private String strHyoukaKijyutsu = "";
	//�����|�C���g
	private int intPointAll = 0;
	//�����]��
	private String strHyoukaAll = "";
	//�{�[�_�[���_
	private int intBorderTen = 0;
	//�{�[�_�[���_��
	private float floBorderTokuritsu = 0;
	//��Ճ����N�΍��l
	private float floRankHensa = 0;
	//�����Ȗځi�Z���^�[�j
	private String strShikenKmkCenter = "";
	//�����Ȗځi�Q���j
	private String strShikenKmk2 = "";
//	//���_�i�Z���^�[�j
//	private int intSoutenCenter = 0;
//	//���_�i�Q���j
//	private int intSouten2 = 0;
//	//�z�_ �O�i�Z���^�[�j
//	private int intGaikokugoCenter = 0;
//	//�z�_ �O�i�Q���j
//	private int intGaikokugo2 = 0;
//	//�z�_ ���i�Z���^�[�j
//	private int intSugakuCenter = 0;
//	//�z�_ ���i�Q���j
//	private int intSugaku2 = 0;
//	//�z�_ ���i�Z���^�[�j
//	private int intKokugoCenter = 0;
//	//�z�_ ���i�Q���j
//	private int intKokugo2 = 0;
//	//�z�_ ���i�Z���^�[�j
//	private int intRikaCenter = 0;
//	//�z�_ ���i�Q���j
//	private int intRika2 = 0;
//	//�z�_ �n�i�Z���^�[�j
//	private int intChirekiCenter = 0;
//	//�z�_ �n�i�Q���j
//	private int intChireki2 = 0;
//	//�z�_ ���i�Z���^�[�j
//	private int intKominCenter = 0;
//	//�z�_ ���i�Q���j
//	private int intKomin2 = 0;
//	//�z�_ ���̑�
//	private int intSonota = 0;
	//���_�i�Z���^�[�j
	private String strSoutenCenter = "";
	//���_�i�Q���j
	private String strSouten2 = "";
	//���ȕʔz�_�i�Z���^�[�j
	private String strCourseAllotCenter = "";
	//���ȕʔz�_�i�Q���j
	private String strCourseAllot2 = "";
	//�o����ؓ�
	private String strSyutsuganbi = "";
//S 2004.09.03 A.Hasegawa
//	//�����敪1
//	private String strShikenKbn1 = "";
//	//�����敪2
//	private String strShikenKbn2 = "";
//	//�����敪3
//	private String strShikenKbn3 = "";
//	//������1
//	private String strShikenbi1 = "";
//	//������2
//	private String strShikenbi2 = "";
//	//������3
//	private String strShikenbi3 = "";
	//������
	private String strShikenbi = "";
//E 2004.09.03
	//���i���\��
	private String strGoukakubi = "";
	//���w�葱���ؓ�
	private String strTetsudukibi = "";
	//�{�����ݒn
	private String strHonbuShozaichi = "";
	//�L�����p�X���ݒn
	private String strKoshaShozaichi = "";

	

	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrShiboRank() {
		return this.strShiboRank;
	}
	public String getStrKenmei() {
		return this.strKenmei;
	}
	public String getStrDaigakuKbn() {
		return this.strDaigakuKbn;
	}
	public String getStrDaigakuMei() {
		return this.strDaigakuMei;
	}
	public String getStrGakubuMei() {
		return this.strGakubuMei;
	}
	public String getStrGakkaMei() {
		return this.strGakkaMei;
	}	
	public String getStrBoshuNinzu() {
		return this.strBoshuNinzu;
	}
	public String getStrExamCapaRel() {
		return this.strExamCapaRel;
	}
	public int getIntHyoukaTokuten() {
		return this.intHyoukaTokuten;
	}
	public String getStrHyoukaMark() {
		return this.strHyoukaMark;
	}
	public float getFloKijyutsuHensa() {
		return this.floKijyutsuHensa;
	}	
	public String getStrHyoukaKijyutsu() {
		return this.strHyoukaKijyutsu;
	}
	public int getIntPointAll() {
		return this.intPointAll;
	}
	public String getStrHyoukaAll() {
		return this.strHyoukaAll;
	}
	public int getIntBorderTen() {
		return this.intBorderTen;
	}
	public float getFloBorderTokuritsu() {
		return this.floBorderTokuritsu;
	}
	public float getFloRankHensa() {
		return this.floRankHensa;
	}
	public String getStrShikenKmkCenter() {
		return this.strShikenKmkCenter;
	}
	public String getStrShikenKmk2() {
		return this.strShikenKmk2;
	}
//	public int getIntSoutenCenter() {
//		return this.intSoutenCenter;
//	}
//	public int getIntSouten2() {
//		return this.intSouten2;
//	}
//	public int getIntGaikokugoCenter() {
//		return this.intGaikokugoCenter;
//	}
//	public int getIntGaikokugo2() {
//		return this.intGaikokugo2;
//	}
//	public int getIntSugakuCenter() {
//		return this.intSugakuCenter;
//	}
//	public int getIntSugaku2() {
//		return this.intSugaku2;
//	}
//	public int getIntKokugoCenter() {
//		return this.intKokugoCenter;
//	}
//	public int getIntKokugo2() {
//		return this.intKokugo2;
//	}
//	public int getIntRikaCenter() {
//		return this.intRikaCenter;
//	}
//	public int getIntRika2() {
//		return this.intRika2;
//	}
//	public int getIntChirekiCenter() {
//		return this.intChirekiCenter;
//	}
//	public int getIntChireki2() {
//		return this.intChireki2;
//	}
//	public int getIntKominCenter() {
//		return this.intKominCenter;
//	}
//	public int getIntKomin2() {
//		return this.intKomin2;
//	}
//	public int getIntSonota() {
//		return this.intSonota;
//	}
//	public String getStrSyutsuganbi() {
//		return this.strSyutsuganbi;
//	}
//S 2004.09.03 A.Hasegawa
//	public String getStrShikenKbn1() {
//		return this.strShikenKbn1;
//	}
//	public String getStrShikenKbn2() {
//		return this.strShikenKbn2;
//	}
//	public String getStrShikenKbn3() {
//		return this.strShikenKbn3;
//	}
//	public String getStrShikenbi1() {
//		return this.strShikenbi1;
//	}
//	public String getStrShikenbi2() {
//		return this.strShikenbi2;
//	}
//	public String getStrShikenbi3() {
//		return this.strShikenbi3;
//	}
	public String getStrShikenbi() {
		return this.strShikenbi;
	}
//E 2004.09.03
	public String getStrGoukakubi() {
		return this.strGoukakubi;
	}
	public String getStrTetsudukibi() {
		return this.strTetsudukibi;
	}
	public int getIntPluralFlg() {
		return this.intPluralFlg;
	}
	public String getStrSyutsuganbi() {
		return this.strSyutsuganbi;
	}
	public String getStrSouten2() {
		return this.strSouten2;
	}
	public String getStrSoutenCenter() {
		return this.strSoutenCenter;
	}
	public String getStrHonbuShozaichi() {
		return this.strHonbuShozaichi;
	}
	public String getStrKoshaShozaichi() {
		return this.strKoshaShozaichi;
	}
	public String getStrCourseAllotCenter() {
		return this.strCourseAllotCenter;
	}
	public String getStrCourseAllot2() {
		return this.strCourseAllot2;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrShiboRank(String strShiboRank) {
		this.strShiboRank = strShiboRank;
	}
	public void setStrKenmei(String strKenmei) {
		this.strKenmei = strKenmei;
	}
	public void setStrDaigakuKbn(String strDaigakuKbn) {
		this.strDaigakuKbn = strDaigakuKbn;
	}
	public void setStrDaigakuMei(String strDaigakuMei) {
		this.strDaigakuMei = strDaigakuMei;
	}
	public void setStrGakubuMei(String strGakubuMei) {
		this.strGakubuMei = strGakubuMei;
	}
	public void setStrGakkaMei(String strGakkaMei) {
		this.strGakkaMei = strGakkaMei;
	}
	public void setStrBoshuNinzu(String strBoshuNinzu) {
		this.strBoshuNinzu = strBoshuNinzu;
	}
	public void setStrExamCapaRel(String strExamCapaRel) {
		this.strExamCapaRel = strExamCapaRel;
	}
	public void setIntHyoukaTokuten(int intHyoukaTokuten) {
		this.intHyoukaTokuten = intHyoukaTokuten;
	}
	public void setStrHyoukaMark(String strHyoukaMark) {
		this.strHyoukaMark = strHyoukaMark;
	}
	public void setFloKijyutsuHensa(float floKijyutsuHensa) {
		this.floKijyutsuHensa = floKijyutsuHensa;
	}
	public void setStrHyoukaKijyutsu(String strHyoukaKijyutsu) {
		this.strHyoukaKijyutsu = strHyoukaKijyutsu;
	}
	public void setIntPointAll(int intPointAll) {
		this.intPointAll = intPointAll;
	}
	public void setStrHyoukaAll(String strHyoukaAll) {
		this.strHyoukaAll = strHyoukaAll;
	}
	public void setIntBorderTen(int intBorderTen) {
		this.intBorderTen = intBorderTen;
	}
	public void setFloBorderTokuritsu(float floBorderTokuritsu) {
		this.floBorderTokuritsu = floBorderTokuritsu;
	}
	public void setFloRankHensa(float floRankHensa) {
		this.floRankHensa = floRankHensa;
	}
	public void setStrShikenKmkCenter(String strShikenKmkCenter) {
		this.strShikenKmkCenter = strShikenKmkCenter;
	}
	public void setStrShikenKmk2(String strShikenKmk2) {
		this.strShikenKmk2 = strShikenKmk2;
	}
//	public void setIntSoutenCenter(int intSoutenCenter) {
//		this.intSoutenCenter = intSoutenCenter;
//	}
//	public void setIntSouten2(int intSouten2) {
//		this.intSouten2 = intSouten2;
//	}
//	public void setIntGaikokugoCenter(int intGaikokugoCenter) {
//		this.intGaikokugoCenter = intGaikokugoCenter;
//	}
//	public void setIntGaikokugo2(int intGaikokugo2) {
//		this.intGaikokugo2 = intGaikokugo2;
//	}
//	public void setIntSugakuCenter(int intSugakuCenter) {
//		this.intSugakuCenter = intSugakuCenter;
//	}
//	public void setIntSugaku2(int intSugaku2) {
//		this.intSugaku2 = intSugaku2;
//	}
//	public void setIntKokugoCenter(int intKokugoCenter) {
//		this.intKokugoCenter = intKokugoCenter;
//	}
//	public void setIntKokugo2(int intKokugo2) {
//		this.intKokugo2 = intKokugo2;
//	}
//	public void setIntRikaCenter(int intRikaCenter) {
//		this.intRikaCenter = intRikaCenter;
//	}
//	public void setIntRika2(int intRika2) {
//		this.intRika2 = intRika2;
//	}
//	public void setIntChirekiCenter(int intChirekiCenter) {
//		this.intChirekiCenter = intChirekiCenter;
//	}
//	public void setIntChireki2(int intChireki2) {
//		this.intChireki2 = intChireki2;
//	}
//	public void setIntKominCenter(int intKominCenter) {
//		this.intKominCenter = intKominCenter;
//	}
//	public void setIntKomin2(int intKomin2) {
//		this.intKomin2 = intKomin2;
//	}
//	public void setIntSonota(int intSonota) {
//		this.intSonota = intSonota;
//	}
	public void setStrSyutsuganbi(String strSyutsuganbi) {
		this.strSyutsuganbi = strSyutsuganbi;
	}
//S 2004.09.03 A.Hasegawa
//	public void setStrShikenKbn1(String strShikenKbn1) {
//		this.strShikenKbn1 = strShikenKbn1;
//	}
//	public void setStrShikenKbn2(String strShikenKbn2) {
//		this.strShikenKbn2 = strShikenKbn2;
//	}
//	public void setStrShikenKbn3(String strShikenKbn3) {
//		this.strShikenKbn3 = strShikenKbn3;
//	}
//	public void setStrShikenbi1(String strShikenbi1) {
//		this.strShikenbi1 = strShikenbi1;
//	}
//	public void setStrShikenbi2(String strShikenbi2) {
//		this.strShikenbi2 = strShikenbi2;
//	}
//	public void setStrShikenbi3(String strShikenbi3) {
//		this.strShikenbi3 = strShikenbi3;
//	}
	public void setStrShikenbi(String strShikenbi) {
		this.strShikenbi = strShikenbi;
	}
//E 2004.09.03
	public void setStrGoukakubi(String strGoukakubi) {
		this.strGoukakubi = strGoukakubi;
	}
	public void setStrTetsudukibi(String strTetsudukibi) {
		this.strTetsudukibi = strTetsudukibi;
	}
	public void setIntPluralFlg(int intPluralFlg) {
		this.intPluralFlg = intPluralFlg;
	}
	public void setStrSouten2(String strSouten2) {
		this.strSouten2 = strSouten2;
	}
	public void setStrSoutenCenter(String strSoutenCenter) {
		this.strSoutenCenter = strSoutenCenter;
	}
	public void setStrHonbuShozaichi(String strHonbuShozaichi) {
		this.strHonbuShozaichi = strHonbuShozaichi;
	}
	public void setStrKoshaShozaichi(String strKoshaShozaichi) {
		this.strKoshaShozaichi = strKoshaShozaichi;
	}
	public void setStrCourseAllotCenter(String strCourseAllotCenter) {
		this.strCourseAllotCenter = strCourseAllotCenter;
	}
	public void setStrCourseAllot2(String strCourseAllot2) {
		this.strCourseAllot2 = strCourseAllot2;
	}

}

/*
 * 作成日: 2004/08/18
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.ExaminationData;

import com.fjh.beans.DefaultBean;

/**
 * @author Administrator
 *
 * この生徒が受けた最新の模試とそのドッキング先の模試をサイズ２の
 * ExaminationDataの配列として返します。
 *
 */
public class NewestExamsBean extends DefaultBean{

	/** 出力パラメター */
	private ExaminationData examinationData;//最新模試
	
	/** 入力パラメター */
	private ExamSession examSession;
	private String examTypeCd;
	private String[] excludeExamCds;

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		String query = null;
		if(examTypeCd == null && examSession != null)
			query = getQuery(examSession, null, excludeExamCds);
		else if(examTypeCd != null && examSession != null)
			query = getQuery(examSession, examTypeCd, excludeExamCds);
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();

			while(rs.next()){
				examinationData = new ExaminationData();
				examinationData.setExamYear(rs.getString("EXAMYEAR"));
				examinationData.setExamCd(rs.getString("EXAMCD"));
				examinationData.setExamName(rs.getString("EXAMNAME"));
				examinationData.setExamTypeCd(rs.getString("EXAMTYPECD"));
				examinationData.setInpleDate(rs.getString("INPLEDATE"));
			}
			
		}catch(SQLException sqle){
			sqle.printStackTrace();
			throw sqle;
		}catch(Exception e){
			e.printStackTrace();
			throw e;	
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	private String getQuery(ExamSession eSession, String examTypeCd, String[] excludeExamCds){
		String rtStr = "";
		//省く模試があれば条件に追加
		String excludeOption = "";
		if(excludeExamCds != null && excludeExamCds.length > 0){
			for(int i=0; i<excludeExamCds.length; i++){
				excludeOption += "AND E.EXAMCD != '"+excludeExamCds[i]+"' ";
			}
		}
		rtStr += "SELECT E.EXAMYEAR, E.EXAMCD, E.EXAMNAME, E.EXAMTYPECD, E.INPLEDATE FROM ( " +
		"SELECT E.* FROM EXAMINATION E WHERE 1=1 "+
		eSession.getExamOptionQuery()+excludeOption;
		if(examTypeCd != null)
			rtStr += "AND E.EXAMTYPECD = '"+examTypeCd+"' ";
		rtStr += ") E WHERE E.OUT_DATAOPENDATE = (SELECT MAX(OUT_DATAOPENDATE) FROM ( " +
		"SELECT E.* FROM EXAMINATION E WHERE 1=1 " +
		eSession.getExamOptionQuery()+excludeOption;
		if(examTypeCd != null)
			rtStr += "AND E.EXAMTYPECD = '"+examTypeCd+"'";
		rtStr += "))";
		return rtStr;
	}


	/**
	 * @return
	 */
	public ExamSession getExamSession() {
		return examSession;
	}

	/**
	 * @param session
	 */
	public void setExamSession(ExamSession session) {
		examSession = session;
	}

	/**
	 * @return
	 */
	public ExaminationData getExaminationData() {
		return examinationData;
	}

	/**
	 * @return
	 */
	public String getExamTypeCd() {
		return examTypeCd;
	}

	/**
	 * @param data
	 */
	public void setExaminationData(ExaminationData data) {
		examinationData = data;
	}

	/**
	 * @param string
	 */
	public void setExamTypeCd(String string) {
		examTypeCd = string;
	}

	/**
	 * @return
	 */
	public String[] getExcludeExamCds() {
		return excludeExamCds;
	}

	/**
	 * @param strings
	 */
	public void setExcludeExamCds(String[] strings) {
		excludeExamCds = strings;
	}

}

/*
 * 作成日: 2019/09/17
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.txt_out;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.util.sql.QueryLoader;


/**
 *
 * （記述系模試）学力要素別成績データテキスト出力
 *
 * @author
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class OutPutSubAcademic extends AbstractSheetBean{

	// 対象年度
	private String targetYear = null;
	// 対象模試
	private String targetExam = null;
	// 学校コード
	private String schoolCd = null;
	// ヘッダー情報
	private List header = null;

	// 出力テキスト種別
	private int outType = 0;
	// 出力対象項目
	private Integer target[] = null;

	// 出力ファイル
	private File outPutFile = null;
	// コネクション
	private Connection con;

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return true;
	}

	public void execute()throws IOException, SQLException, Exception{

		// 担当クラスを一時テーブルへ展開する
		super.setupCountChargeClass();

		OutPutParamSet set = new OutPutParamSet(login, exam, profile, "t106",sessionKey);

		targetYear = set.getTargetYear();
		targetExam = set.getTargetExam();
		schoolCd = set.getSchoolCd();
		header = set.getHeader();
		outType =set.getOutType();
		target = set.getTarget();
		outPutFile =set.getOutPutFile();
		con = super.conn;

		OutputTextBean out = new OutputTextBean();
		out.setOutFile(outPutFile);
		out.setOutputTextList(getSubAcademic());
		out.setHeadTextList(header);
		out.setOutTarget(target);
		out.setOutType(outType);
		out.execute();

		this.outfileList.add(outPutFile.getPath());
		sheetLog.add(outPutFile.getName());

	}


	/**
	 * （記述系模試）学力要素別成績を取得
	 *
	 * @return list （記述系模試）学力要素別成績のレコードオブジェクト
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public LinkedList getSubAcademic() throws SQLException {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		LinkedList list = new LinkedList();
		String[] rsData = new String[16];		// 読み込みデータ
		String[] listData = new String[40];	// リストデータ

		int    qnoCount=0;
		// キー項目
		String individualID ="";
		String examYear ="";
		String examCD   ="";
		String class_no    ="";
		// キー項目記憶
		String p_individualID ="";
		String p_examYear ="";
		String p_examCD   ="";
		String p_class_no    ="";

		try {
			stmt = con.prepareStatement(QueryLoader.getInstance().load("t07").toString());
			stmt.setString(1, this.schoolCd);
			stmt.setString(2, this.targetYear);
			stmt.setString(3, this.targetExam);

			rs = stmt.executeQuery();

			while (rs.next()) {

				// SELECTしたデータを一時変数に格納
				for (int i = 0; i < rsData.length; i++) {
					// 小数点以下ありの項目はgetDouble()で取得
					// 知識技能得点率、思考力判断力得点率、表現力得点率
					if ( i == 13 || i == 14 || i == 15 ) {
						if ( rs.getString(i + 1) == null ) {
							rsData[i] = rs.getString(i + 1);
						} else {
							rsData[i] = String.valueOf(rs.getDouble(i + 1));
						}
					} else {
						rsData[i] = rs.getString(i + 1);
					}
				}

				// キー項目をセット
				individualID=rsData[ 0];	// 個人ID
				examYear    =rsData[ 1];	// 模試年度
				examCD      =rsData[ 2];	// 模試コード
				class_no    =rsData[ 6];	// クラス番号

				// キー項目の内容に変化があればデータを格納＆初期化
				if (!p_individualID.equals(individualID) ||
					!p_examYear.equals(examYear) ||
					!p_examCD.equals(examCD) ||
					!p_class_no.equals(class_no) ) {

					// リストデータに格納
					if (qnoCount > 0) {
						setListData(listData, list);
					}

					// データ初期化
					qnoCount = 0;
					for (int i = 0; i < listData.length; i++) {
						listData[i] = null;
					}

					// 通番〜文理コードまでをセット
					for (int i = 0; i < 10; i++) {
						listData[i] = rsData[i];
					}
				}
				// 格納可能カウンターのカウントアップ
				qnoCount++;

				// キー項目記憶
				p_individualID=individualID;
				p_examYear    =examYear;
				p_examCD      =examCD;
				p_class_no    =class_no;

				// 科目番号
				int score_No = Integer.valueOf(rsData[10]).intValue();
				// 科目番号"1"〜"6"の範囲
				if ( 1 <= score_No && score_No <= 6 ) {
					// コード
					listData[10 + (score_No-1) * 5 + 0] = rsData[11];
					// 科目名
					listData[10 + (score_No-1) * 5 + 1] = rsData[12];
					// 知識技能得点率
					listData[10 + (score_No-1) * 5 + 2] = rsData[13];
					// 思考力判断力得点率
					listData[10 + (score_No-1) * 5 + 3] = rsData[14];
					// 表現力得点率
					listData[10 + (score_No-1) * 5 + 4] = rsData[15];
				}

			}
			// 格納可能データがあればリストデータに格納
			if (qnoCount > 0) {
				setListData(listData, list);
			}

		} catch (Exception e) {
		    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
		    //throw new SQLException(e.getMessage());
		    throw new SQLException(e);
		    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return list;
	}


	/**
	 * 一時的に記憶したリストデータを格納する
	 *
	 * @param 	listData	リストデータ
	 *          list        格納するリスト
	 *
	 */
	private void setListData(String[] listData, LinkedList list) {
		LinkedHashMap data = new LinkedHashMap();
		for (int i = 0; i<listData.length; i++) {
			data.put(new Integer(i+1), listData[i]);
		}
		list.add(data);
	}

	/**
	 * @param connection
	 */
	public void setCon(Connection connection) {
		con = connection;
	}

	/**
	 * @param string
	 */
	public void setSchoolCd(String string) {
		schoolCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExam(String string) {
		targetExam = string;
	}

	/**
	 * @param string
	 */
	public void setTargetYear(String string) {
		targetYear = string;
	}

	/**
	 * @param list
	 */
	public void setHeader(LinkedList list) {
		header = list;
	}

	/**
	 * @param i
	 */
	public void setOutType(int i) {
		outType = i;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	/**
	 * @return
	 */
	public Integer[] getTarget() {
		return target;
	}

	/**
	 * @param integers
	 */
	public void setTarget(Integer[] integers) {
		target = integers;
	}

}
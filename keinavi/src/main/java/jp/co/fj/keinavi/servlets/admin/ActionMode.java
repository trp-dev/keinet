/*
 * 作成日: 2004/09/24
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.admin;

/**
 * @author 奥村ゆかり
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public interface ActionMode {

	// メンテナンス再計算
	String RECOUNT_MODE = "0";
	
	// メンテナンストップの表示
	String TOP_VIEW_MODE = "1";
	// 生徒一覧の表示
	String STUDENT_VIEW_MODE = "2";
	// 受験届　一覧
	String EXAM_VIEW_MODE = "3";
	// 複合クラス　一覧
	String COMPOSI_VIEW_MODE = "4";
		
	// 生徒情報の編集
	String STUDENT_UPDATE_MODE = "5";
	// 生徒情報の削除
	String STUDENT_DELETE_MODE = "6";
	// 生徒情報の一覧
	String STUDENT_LIST_MODE = "16";
	// 生徒情報の登録
	String REGIST = "7";
	// 生徒情報の一括登録
	String REGIST_PACKAGE = "8";

	// 受験届　編集
	String EXAM_UPDATE_MODE = "9";
	// 受験届　リスト
	String EXAM_LIST_MODE = "17";
	// 受験届　結合候補一覧	
	String EXAM_UPDATE_VIEW_MODE = "10";
	// 受験届からの生徒情報の登録
	String REGIST_EXAM = "11";
	// 受験届　結合
	String COMBI_EXAM = "12";
	
	// 複合クラス情報の編集
	String COMPOSI_UPDATE_MODE = "13";
	// 複合クラス情報の削除
	String COMPOSI_DELETE_MODE = "14";
	// 複合クラス情報の登録
	String REGIST_COMPOSI = "15";
	
}

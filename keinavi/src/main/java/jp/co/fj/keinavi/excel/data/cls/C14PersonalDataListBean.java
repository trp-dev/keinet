package jp.co.fj.keinavi.excel.data.cls;

/**
 * クラス成績概況−設問別個人成績データリスト
 * 作成日: 2004/07/15
 * @author	H.Fujimoto
 */
public class C14PersonalDataListBean {
	//学年
	private String strGrade = "";
	//学年
	private String strClass = "";
	//クラス番号
	private String strClassNo = "";
	//氏名
	private String strKanaName = "";
	//性別
	private String strSex = "";
	//得点
	private int intTokuten = 0;
	//得点率
	private float floTokuritsu = 0;

	/*----------*/
	/* Get      */
	/*----------*/

	public float getFloTokuritsu() {
		return this.floTokuritsu;
	}
	public int getIntTokuten() {
		return this.intTokuten;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrClassNo() {
		return this.strClassNo;
	}
	public String getStrGrade() {
		return this.strGrade;
	}
	public String getStrKanaName() {
		return this.strKanaName;
	}
	public String getStrSex() {
		return this.strSex;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setFloTokuritsu(float floTokuritsu) {
		this.floTokuritsu = floTokuritsu;
	}
	public void setIntTokuten(int intTokuten) {
		this.intTokuten = intTokuten;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrClassNo(String strClassNo) {
		this.strClassNo = strClassNo;
	}
	public void setStrKanaName(String strKanaName) {
		this.strKanaName = strKanaName;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}
	public void setStrSex(String strSex) {
		this.strSex = strSex;
	}

}
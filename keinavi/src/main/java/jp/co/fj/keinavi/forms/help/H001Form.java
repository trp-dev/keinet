/*
 * 作成日: 2004/09/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.help;

import com.fjh.forms.ActionForm;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class H001Form extends ActionForm {

	// フリーワード検索とカテゴリー一覧の切り分けを「category」で行う。
	private String freeWord = "";		// フリーワード

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * @return
	 */
	public String getFreeWord() {
		return freeWord;
	}

	/**
	 * @param string
	 */
	public void setFreeWord(String string) {
		freeWord = string;
	}

}

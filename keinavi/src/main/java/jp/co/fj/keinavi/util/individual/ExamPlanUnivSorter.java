/*
 * 作成日: 2004/11/08
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.individual;

import java.util.Comparator;

import jp.co.fj.keinavi.data.individual.ExamPlanUnivData;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ExamPlanUnivSorter {
	
	public class SortByUFD implements Comparator{

		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 * 大学・学部・学科コード順
		 */
		public int compare(Object o1, Object o2) {
			ExamPlanUnivData d1 = (ExamPlanUnivData) o1;
			ExamPlanUnivData d2 = (ExamPlanUnivData) o2;
			int univCd1 = Integer.parseInt(d1.getUnivCd());
			int univCd2 = Integer.parseInt(d2.getUnivCd());
			int facultyCd1 = Integer.parseInt(d1.getFacultyCd());
			int facultyCd2 = Integer.parseInt(d2.getFacultyCd());
			int deptCd1 = Integer.parseInt(d1.getDeptCd());
			int deptCd2 = Integer.parseInt(d2.getDeptCd());
			if(univCd1 < univCd2){
				return -1;
			}else if(univCd1 > univCd2){
				return 1;
			}else{
				if(facultyCd1 < facultyCd2){
					return -1;
				}else if(facultyCd1 > facultyCd2){
					return 1;
				}else{
					if(deptCd1 < deptCd2){
						return -1;
					}else if(deptCd1 > deptCd2){
						return 1;
					}else{
						return 0;
					}
				}
			}
		}
		
	}
}

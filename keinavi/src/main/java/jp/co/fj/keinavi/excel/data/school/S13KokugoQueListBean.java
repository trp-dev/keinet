package jp.co.fj.keinavi.excel.data.school;

/**
 * 設問別成績（校内成績）小設問別正答状況（国語）データリスト
 * 作成日: 2009/10/20
 * @author	Fujito URAKAWA
 */
public class S13KokugoQueListBean {

	// 校内正答率1
	private float floSeitoritsuHome1 = 0;
	// 全国正答率1
	private float floSeitoritsuAll1 = 0;
	// 校内正答率2
	private float floSeitoritsuHome2 = 0;
	// 全国正答率2
	private float floSeitoritsuAll2 = 0;
	// 校内正答率3
	private float floSeitoritsuHome3 = 0;
	// 全国正答率3
	private float floSeitoritsuAll3 = 0;
	// 校内正答率4
	private float floSeitoritsuHome4 = 0;
	// 全国正答率4
	private float floSeitoritsuAll4 = 0;
	// 校内正答率5
	private float floSeitoritsuHome5 = 0;
	// 全国正答率5
	private float floSeitoritsuAll5 = 0;
	// 校内正答率6
	private float floSeitoritsuHome6 = 0;
	// 全国正答率6
	private float floSeitoritsuAll6 = 0;
	// 校内正答率7
	private float floSeitoritsuHome7 = 0;
	// 全国正答率7
	private float floSeitoritsuAll7 = 0;




	/*----------*/
	/* Get      */
	/*----------*/
	public float getFloSeitoritsuHome1() {
		return floSeitoritsuHome1;
	}
	public float getFloSeitoritsuAll1() {
		return floSeitoritsuAll1;
	}
	public float getFloSeitoritsuHome2() {
		return floSeitoritsuHome2;
	}
	public float getFloSeitoritsuAll2() {
		return floSeitoritsuAll2;
	}
	public float getFloSeitoritsuHome3() {
		return floSeitoritsuHome3;
	}
	public float getFloSeitoritsuAll3() {
		return floSeitoritsuAll3;
	}
	public float getFloSeitoritsuHome4() {
		return floSeitoritsuHome4;
	}
	public float getFloSeitoritsuAll4() {
		return floSeitoritsuAll4;
	}
	public float getFloSeitoritsuHome5() {
		return floSeitoritsuHome5;
	}
	public float getFloSeitoritsuAll5() {
		return floSeitoritsuAll5;
	}
	public float getFloSeitoritsuHome6() {
		return floSeitoritsuHome6;
	}
	public float getFloSeitoritsuAll6() {
		return floSeitoritsuAll6;
	}
	public float getFloSeitoritsuHome7() {
		return floSeitoritsuHome7;
	}
	public float getFloSeitoritsuAll7() {
		return floSeitoritsuAll7;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setFloSeitoritsuHome1(float floSeitoritsuHome1) {
		this.floSeitoritsuHome1 = floSeitoritsuHome1;
	}
	public void setFloSeitoritsuAll1(float floSeitoritsuAll1) {
		this.floSeitoritsuAll1 = floSeitoritsuAll1;
	}
	public void setFloSeitoritsuHome2(float floSeitoritsuHome2) {
		this.floSeitoritsuHome2 = floSeitoritsuHome2;
	}
	public void setFloSeitoritsuAll2(float floSeitoritsuAll2) {
		this.floSeitoritsuAll2 = floSeitoritsuAll2;
	}
	public void setFloSeitoritsuHome3(float floSeitoritsuHome3) {
		this.floSeitoritsuHome3 = floSeitoritsuHome3;
	}
	public void setFloSeitoritsuAll3(float floSeitoritsuAll3) {
		this.floSeitoritsuAll3 = floSeitoritsuAll3;
	}
	public void setFloSeitoritsuHome4(float floSeitoritsuHome4) {
		this.floSeitoritsuHome4 = floSeitoritsuHome4;
	}
	public void setFloSeitoritsuAll4(float floSeitoritsuAll4) {
		this.floSeitoritsuAll4 = floSeitoritsuAll4;
	}
	public void setFloSeitoritsuHome5(float floSeitoritsuHome5) {
		this.floSeitoritsuHome5 = floSeitoritsuHome5;
	}
	public void setFloSeitoritsuAll5(float floSeitoritsuAll5) {
		this.floSeitoritsuAll5 = floSeitoritsuAll5;
	}
	public void setFloSeitoritsuHome6(float floSeitoritsuHome6) {
		this.floSeitoritsuHome6 = floSeitoritsuHome6;
	}
	public void setFloSeitoritsuAll6(float floSeitoritsuAll6) {
		this.floSeitoritsuAll6 = floSeitoritsuAll6;
	}
	public void setFloSeitoritsuHome7(float floSeitoritsuHome7) {
		this.floSeitoritsuHome7 = floSeitoritsuHome7;
	}
	public void setFloSeitoritsuAll7(float floSeitoritsuAll7) {
		this.floSeitoritsuAll7 = floSeitoritsuAll7;
	}

}

package jp.co.fj.keinavi.servlets.helpdesk;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.auth.KnetCookieManager;
import jp.co.fj.keinavi.beans.helpdesk.HelpDeskMenuBean;
import jp.co.fj.keinavi.beans.helpdesk.SectorSortingBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.login.SectorSession;
import jp.co.fj.keinavi.forms.helpdesk.HD002Form;
import jp.co.fj.keinavi.servlets.DefaultLoginServlet;
import jp.co.fj.keinavi.servlets.KNServletException;

/**
 *
 * ヘルプデスクメニュー画面サーブレット
 * 
 * 2004.06.30	[新規作成]
 * 
 * <2010年度マーク高２模試対応>
 * 2011.02.23	Tomohisa YAMADA
 * 				アップロード機能
 * 
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class HD002Servlet extends DefaultLoginServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(
			final HttpServletRequest request,
			final HttpServletResponse response
			) throws ServletException, IOException {

		// セッション
		final HttpSession session = request.getSession();

		// ログイン情報
		final LoginSession login = getLoginSession(request);

		// アクションフォーム
		final HD002Form form = (HD002Form) getActionForm(request,
				"jp.co.fj.keinavi.forms.helpdesk.HD002Form");
		request.setAttribute("form", form);

		Connection con = null;

		// ログアウト
		if ("logout".equals(getForward(request))) {
			dispatch(request, response);
			
		// 転送先がメニュー選択画面
		} else if ("hd002".equals(getForward(request))) {

			try {
				con = getConnectionPool(request);
				SectorSortingBean bean = new SectorSortingBean();
				bean.setConnection(null, con);
				bean.execute();

				// セッションに入れる
				session.setAttribute(SectorSession.SESSION_KEY, bean.getSectorSession());

			} catch (Exception e) {
				throw new ServletException(e.toString());
			} finally {
				releaseConnectionPool(request, con);
			}

			// ヘルプデスクメニュー選択画面へ
			forward(request, response, JSP_HD002);

		// 転送元がメニュー選択画面
		} else if ("hd002".equals(getBackward(request))) {
	
			try {
				con = getConnectionPool(request);
				HelpDeskMenuBean bean = new HelpDeskMenuBean();
				bean.setConnection(null, con);
				bean.setMenu(form.getMenu());
				bean.setSchoolCode(form.getSchoolCode());
				bean.setBuildingCode(form.getBuildingCode());
				//高校作成ファイルダウンロード
				if ("4".equals(form.getMenu())) {
					bean.setBusinessCode(form.getSalesCode());
				}
				else {
					bean.setBusinessCode(form.getBusinessCode());
				}
				bean.setLoginSession(login);
				bean.execute();

				// 高校代行ならCookie作成
				if ("1".equals(form.getMenu())) {
					new KnetCookieManager().create(response, login);
				}

			// 元の画面に戻る場合
			} catch (final KNServletException e) {
				if (e.getErrorCode().startsWith("1")) {
					setErrorMessage(request, e);
				} else {
					throw e;
				}
			} catch (final Exception e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			// メニューセキュリティ
			setupMenuSecurity(request);

			// エラーメッセージがなければ各メニュー画面へ
			if (getErrorMessage(request) == null) {
				forward(request, response, SERVLET_DISPATCHER);
			// エラーメッセージがあるなら元の画面へ
			} else {
				forward(request, response, JSP_HD002);
			}
			
		// それ以外はJSPへ
		} else {
			forward(request, response, JSP_HD002);
		}
	}

}

package jp.co.fj.keinavi.excel.data.school;

/**
 * Zà¬ÑªÍ|ßNxär|Î·lªz Fè±ÊEvCEFRæ¾óµf[^Xg
 * ì¬ú: 2019/08/20
 * @author	QQ)H.Yamasaki
 */
public class S22PtCefrAcqStatusListBean {

	//ÍNx
	private String examyear = "";
	//pêFè±R[h
	private String engptcd = "";
	//pêFè±xR[h
	private String engptlevelcd = "";
	//CEFRxR[h
	private String cefrlevelcd = "";
	//x ètO
	private String levelflg = "";
	//Fè±Zk¼
	private String engptname_abbr = "";
	//Fè±xZk¼
	private String engptlevelname_abbr = "";
	// CEFRZk¼
	private String cerflevelname_abbr = "";
	//ÀÑ
	private int engptinfo_sort = 0;
	//ÀÑ
	private int engptdetailinfo_sort = 0;
	//ÀÑ
	private int cefrinfo_sort = 0;
	//l
	private int numbers1 = 0;
	//\¬ä
	private float compratio1 = 0;
	//wZó±L³
	private String flg1 = "";
	//ÍNx(ßN1)
	private String examyear2 = "";
	//l(ßN1)
	private int numbers2 = 0;
	//\¬ä(ßN1)
	private float compratio2 = 0;
	//ÍNx(ßN2)
	private String examyear3 = "";
	//l(ßN2)
	private int numbers3 = 0;
	//\¬ä(ßN2)
	private float compratio3 = 0;
	//ÍNx(ßN3)
	private String examyear4 = "";
	//l(ßN3)
	private int numbers4 = 0;
	//\¬ä(ßN3)
	private float compratio4 = 0;
	//ÍNx(ßN4)
	private String examyear5 = "";
	//l(ßN4)
	private int numbers5 = 0;
	//\¬ä(ßN4)
	private float compratio5 = 0;


	/*----------*/
	/* Get      */
	/*----------*/
	public String getExamyear() {
		return examyear;
	}
	public String getEngptcd() {
		return engptcd;
	}
	public String getEngptlevelcd() {
		return engptlevelcd;
	}
	public String getCefrlevelcd() {
		return cefrlevelcd;
	}
	public String getLevelflg() {
		return levelflg;
	}
	public String getEngptname_abbr() {
		return engptname_abbr;
	}
	public String getEngptlevelname_abbr() {
		return engptlevelname_abbr;
	}
	public String getCerflevelname_abbr() {
		return cerflevelname_abbr;
	}
	public int getEngptinfo_sort() {
		return engptinfo_sort;
	}
	public int getEngptdetailinfo_sort() {
		return engptdetailinfo_sort;
	}
	public int getCefrinfo_sort() {
		return cefrinfo_sort;
	}
	public int getNumbers1() {
		return numbers1;
	}
	public float getCompratio1() {
		return compratio1;
	}
	public String getFlg1() {
		return flg1;
	}
	public String getExamyear2() {
		return examyear2;
	}
	public int getNumbers2() {
		return numbers2;
	}
	public float getCompratio2() {
		return compratio2;
	}
	public String getExamyear3() {
		return examyear3;
	}
	public int getNumbers3() {
		return numbers3;
	}
	public float getCompratio3() {
		return compratio3;
	}
	public String getExamyear4() {
		return examyear4;
	}
	public int getNumbers4() {
		return numbers4;
	}
	public float getCompratio4() {
		return compratio4;
	}
	public String getExamyear5() {
		return examyear5;
	}
	public int getNumbers5() {
		return numbers5;
	}
	public float getCompratio5() {
		return compratio5;
	}


	/*----------*/
	/* Set      */
	/*----------*/

	public void setExamyear(String examyear) {
		this.examyear = examyear;
	}
	public void setEngptcd(String engptcd) {
		this.engptcd = engptcd;
	}
	public void setEngptlevelcd(String engptlevelcd) {
		this.engptlevelcd = engptlevelcd;
	}
	public void setCefrlevelcd(String cefrlevelcd) {
		this.cefrlevelcd = cefrlevelcd;
	}
	public void setLevelflg(String levelflg) {
		this.levelflg = levelflg;
	}
	public void setEngptname_abbr(String engptname_abbr) {
		this.engptname_abbr = engptname_abbr;
	}
	public void setEngptlevelname_abbr(String engptlevelname_abbr) {
		this.engptlevelname_abbr = engptlevelname_abbr;
	}
	public void setCerflevelname_abbr(String cerflevelname_abbr) {
		this.cerflevelname_abbr = cerflevelname_abbr;
	}
	public void setEngptinfo_sort(int engptinfo_sort) {
		this.engptinfo_sort = engptinfo_sort;
	}
	public void setEngptdetailinfo_sort(int engptdetailinfo_sort) {
		this.engptdetailinfo_sort = engptdetailinfo_sort;
	}
	public void setCefrinfo_sort(int cefrinfo_sort) {
		this.cefrinfo_sort = cefrinfo_sort;
	}
	public void setNumbers1(int numbers) {
		this.numbers1 = numbers;
	}
	public void setCompratio1(float compratio) {
		this.compratio1 = compratio;
	}
	public void setFlg1(String flg) {
		this.flg1 = flg;
	}
	public void setExamyear2(String examyear) {
		this.examyear2 = examyear;
	}
	public void setNumbers2(int numbers) {
		this.numbers2 = numbers;
	}
	public void setCompratio2(float compratio) {
		this.compratio2 = compratio;
	}
	public void setExamyear3(String examyear) {
		this.examyear3 = examyear;
	}
	public void setNumbers3(int numbers) {
		this.numbers3 = numbers;
	}
	public void setCompratio3(float compratio) {
		this.compratio3 = compratio;
	}
	public void setExamyear4(String examyear) {
		this.examyear4 = examyear;
	}
	public void setNumbers4(int numbers) {
		this.numbers4 = numbers;
	}
	public void setCompratio4(float compratio) {
		this.compratio4 = compratio;
	}
	public void setExamyear5(String examyear) {
		this.examyear5 = examyear;
	}
	public void setNumbers5(int numbers) {
		this.numbers5 = numbers;
	}
	public void setCompratio5(float compratio) {
		this.compratio5 = compratio;
	}
}

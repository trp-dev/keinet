package jp.co.fj.keinavi.forms.maintenance;

/**
 *
 * 非表示生徒一覧画面アクションフォーム
 * 
 * 2006.11.01	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M108Form extends M101Form {

	// 対象個人ID
	private String[] individualId;

	/**
	 * @return individualId を戻します。
	 */
	public String[] getIndividualId() {
		return individualId;
	}

	/**
	 * @param pIndividualId 設定する individualId。
	 */
	public void setIndividualId(String[] pIndividualId) {
		individualId = pIndividualId;
	}
	
}

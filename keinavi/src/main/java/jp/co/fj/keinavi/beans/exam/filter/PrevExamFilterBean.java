package jp.co.fj.keinavi.beans.exam.filter;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * 過年度比較用の模試フィルタ
 * 
 * 2006.08.23	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class PrevExamFilterBean implements IExamFilterBean {

	/**
	 * @see jp.co.fj.keinavi.util.taglib.exam.IExamFilterBean#execute(
	 * 			jp.co.fj.keinavi.data.LoginSession,
	 * 			jp.co.fj.keinavi.data.ExamSession,
	 * 			jp.co.fj.keinavi.data.ExamData)
	 */
	public boolean execute(final LoginSession login,
			final ExamSession examSession, final ExamData exam) {
		
		// 現年度
		final String current = KNUtil.getCurrentYear();
		// 前年度
		final String prev = String.valueOf(Integer.parseInt(current) - 1);
	
		// 現年度の模試なら無条件でOK
		if (current.equals(exam.getExamYear())) {
			return true;			
		}
		
		// 前年度の模試かつ現年度に同じ模試がなければOK
		if (prev.equals(exam.getExamYear()) 
				&& !examSession.getPublicExamSet().contains(exam.getExamCD())) {
			return true;		
		}

		return false;
	}

}

package jp.co.fj.keinavi.beans.sheet.business.data;

import jp.co.fj.keinavi.beans.sheet.school.data.S42_15SubjectListData;

/**
 *
 * 科目リストデータ
 * 
 * 2006.08.31	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class B42_07SubjectListData extends S42_15SubjectListData {
}

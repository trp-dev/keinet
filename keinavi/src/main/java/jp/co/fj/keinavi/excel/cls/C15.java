/**
 * クラス成績分析−クラス成績概況
 * 出力する帳票の判断
 * 作成日: 2004/08/31
 * @author	Ito.Y
 */
 
package jp.co.fj.keinavi.excel.cls;
import jp.co.fj.keinavi.util.log.*;

import java.util.*;

import jp.co.fj.keinavi.excel.data.cls.*;

public class C15 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean c15( C15Item c15Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {
		
		//C15Itemから各帳票を出力
		KNLog log = KNLog.getInstance(null,null,null);
		int ret = 0;
		C15_01 exceledit = new C15_01();
		ret = exceledit.c15_01EditExcel( c15Item, outfilelist, saveFlg, UserID );
		if (ret!=0) {
			log.Err("0"+ret+"C15_01","帳票作成エラー","");
			return false;
		}
		sheetLog.add("C15_01");

		return true;
	}

}
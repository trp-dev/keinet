/*
 *
 * 成績分析／成績推移グラフの偏差値データ取得用Bean
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import jp.co.fj.keinavi.data.individual.CourseData;
import jp.co.fj.keinavi.data.individual.IExamData;
import jp.co.fj.keinavi.data.individual.SubRecordIData;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNUtil;//[3] add

import com.fjh.beans.DefaultBean;

/**
 *
 * バランスチャートの偏差値データ取得用Bean
 * 
 * 2004/07/27    [新規作成]
 *
 * @author Keisuke KONDO - TOTEC
 * @version 1.0
 * 
 *   [1]    2005.01.21 kondo      change ２次 英語＋リスニング追加対応
 *   [2]    2005.01.21 kondo      change 配点200点の科目が複数ある場合最初の1つをとるように修正
 *   [3]    2005.03.01 kondo      現年度をカレンダーから取得時、３月３１日以前は前年度とする日付比較が、うまく行われていなかったのを修正。
 *   [4]    2005.03.08 kondo      現年度指定の条件に使用される年度を、担当クラスの年度に変更
 *   [5]    2005.09.16 kondo      change 科目取得の際EXAMSUBJECTではなく、Viewを参照するように修正
 * 2006.09.04     Totec)T.Yamada      [6]受験学力測定対応
 * 2009.11.20   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応 
 * 
 */
public class I106Bean extends DefaultBean {
	
	private static final long serialVersionUID = -6734869956158353588L;
	
	private String SQL_BASE = 
		"SELECT"+
		" /*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */ "+//12/5 sql hint
		" SUBRECORD_I.INDIVIDUALID"+
		" ,SUBRECORD_I.A_DEVIATION"+
		" ,SUBRECORD_I.SUBCD"+
		" ,SUBRECORD_I.ACADEMICLEVEL"+
		" ,EXAMINATION.EXAMNAME_ABBR"+
		" ,EXAMINATION.EXAMTYPECD"+
		" ,SUBRECORD_I.EXAMCD"+
		" ,SUBRECORD_I.EXAMYEAR"+
		//" ,EXAMSUBJECT.SUBALLOTPNT"+//[5] delete
		" ,V_I_CMEXAMSUBJECT.SUBALLOTPNT"+//[5] add
		" ,SUBRECORD_I.SCOPE"+
		" FROM SUBRECORD_I"+
		" INNER JOIN EXAMINATION ON EXAMINATION.EXAMYEAR = SUBRECORD_I.EXAMYEAR"+
		" AND EXAMINATION.EXAMCD = SUBRECORD_I.EXAMCD"+
		//" INNER JOIN EXAMSUBJECT ON EXAMSUBJECT.EXAMYEAR = SUBRECORD_I.EXAMYEAR"+//[5] delete
		//" AND SUBRECORD_I.EXAMCD = EXAMSUBJECT.EXAMCD"+//[5] delete
		//" AND SUBRECORD_I.SUBCD = EXAMSUBJECT.SUBCD";//[5] delete
		" INNER JOIN V_I_CMEXAMSUBJECT ON V_I_CMEXAMSUBJECT.EXAMYEAR = SUBRECORD_I.EXAMYEAR"+//[5] add
		" AND SUBRECORD_I.EXAMCD = V_I_CMEXAMSUBJECT.EXAMCD"+//[5] add
		" AND SUBRECORD_I.SUBCD = V_I_CMEXAMSUBJECT.SUBCD";//[5] add
	
	private String SQL_BASE_MOSHILIST = 
		"SELECT"+
		" /*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */ "+//12/5 sql hint
		" SUBRECORD_I.INDIVIDUALID"+
		" ,EXAMINATION.EXAMNAME_ABBR"+
		" ,EXAMINATION.EXAMTYPECD"+
		" ,EXAMINATION.EXAMCD"+
		" ,EXAMINATION.EXAMYEAR";
	
	private String SUBNUM_SQL_BASE = "";
  
	private String moshiSql;
	private String subNumSql;
	private String countSql;
	private String moshiListSql;
	
	private String targetExamYear;			//対象模試年度
	private String targetExamCode;			//対象模試コード
	private String[] deviationRanges;		//偏差値範囲
	private String personId;				//個人ＩＤ
	
	private String avgChoice;				// 教科複数受験フラグ（1:平均、2：良い方、第1解答科目）
	
	private String targetGrade;			//学年の検索方法
	private String targetExamType;			//模試種類の検索方法
	
	private int examinationDatasSize;		//模試リストの空で埋める前の大きさ。
	
	private String classYear;//担当クラス年度//[4] add
	
	private boolean isFirstAccess;
	private boolean helpflg;
	
	private Collection courseDatas;
	
	public int getCourseDataSize(){
		int size = 0;
		if(getCourseDatas() != null)
			size = getCourseDatas().size();
		return size;
	}
	
	private Collection examinationDatas;
	
	public int getExaminationDataSize(){
		int size = 0;
		if(getExaminationDatas() != null)
			size = getExaminationDatas().size();
		return size;	
	}
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		appendMoshiSql(getMoshiOrderBy());
		appendCountSql(getMoshiOrderBy());
		appendMoshiListSql(getMoshiListOrderBy());
		setExaminationDatas(execDeviations());
		setCourseDatas(execSubNames(getTargetExamYear(), getTargetExamCode()));
	}
	
	/**
	 * 初回アクセス時
	 * execute()を実行する前に条件をしていする
	 * @param string1
	 * @param string2
	 * @param stringss
	 * @param b
	 * @param string3
	 */
	public void setSearchCondition(
		String string1, 
		String string2, 
		String devRanges, 
		String string3,
		String string4,
		String string5
		){
		
		setTargetExamYear(string1);
		setTargetExamCode(string2);
		setDeviationRanges(IUtilBean.deconcatComma(devRanges));
		setPersonId(string3);
		setTargetGrade(string4);
		setTargetExamType(string5);
		
		//:::: SQL文の作成 ::::::::::
		appendMoshiSql(SQL_BASE);
		appendCountSql(SQL_BASE);
		appendMoshiListSql(SQL_BASE_MOSHILIST);
		appendMoshiListSql(getMoshiListSelect());
		
		if(getTargetGrade().equals("1")) {
			//表示する学年が、現在の年度のみ
			appendMoshiSql(getYearAndPersonCondition(getPersonId(), getThisYear()));

			appendCountSql(getYearAndPersonCondition(getPersonId(), getThisYear()));
			appendCountSql(get200count());
			
			appendMoshiListSql(getYearAndPersonCondition(getPersonId(), getThisYear()));
			
		} else {
			//表示する学年が、すべての年度
			appendMoshiSql(getPersonCondition(getPersonId()));
			
			appendCountSql(getPersonCondition(getPersonId()));
			appendCountSql(get200count());
			
			appendMoshiListSql(getPersonCondition(getPersonId()));
		}
		
		//対象模試より過去模試
		appendMoshiSql(getSequenceCondition(getTargetExamYear(), getTargetExamCode()));
		appendCountSql(getSequenceCondition(getTargetExamYear(), getTargetExamCode()));
		appendMoshiListSql(getSequenceCondition(getTargetExamYear(), getTargetExamCode()));
		if(getTargetExamType().equals("2")) {
			//表示する模試種類が、同系統のみ
			appendMoshiSql(getExamTypeCondition(getTargetExamYear(), getTargetExamCode()));
			appendCountSql(getExamTypeCondition(getTargetExamYear(), getTargetExamCode()));
			appendCountSql(get200count());
			appendMoshiListSql(getExamTypeCondition(getTargetExamYear(), getTargetExamCode()));
		} else {
			//表示する模試種類が、すべての種類
			//何もしない
		}
        
        //受験学力測定テストを省く
        final StringBuffer buf = new StringBuffer();
        
        buf.append(" AND examination.examtypecd NOT IN ( ");
        buf.append(" '" + KNUtil.TYPECD_ABILITY+ "' ");
        buf.append(" ) ");
        
        appendMoshiSql(buf.toString());
        appendCountSql(buf.toString());
        appendMoshiListSql(buf.toString());
        
        appendMoshiListSql(getMoshiListGroup());
        
		setSubNumSql(SUBNUM_SQL_BASE);
	}
	/**
	 * 二回目以降のアクセス時
	 * execute()を実行する前に条件をしていする
	 * @param string1
	 * @param string2
	 * @param stringss
	 * @param b
	 * @param string3
	 * @param string4 選択された模試コード３つ
	 */
/*
	public void setSearchCondition(
		String string1, String string2, 
		String[] stringss, String string3, String[] strings){
		
		setTargetExamCodes(strings);
		//setSearchCondition(string1, string2, stringss, string3);
		appendMoshiSql(getExamCdCondition(getTargetExamCode(), getTargetExamCodes()));
	}
*/

	/**
	 * メインデータを取得するSQL文にStringを付加する
	 * @param string
	 */
	private void appendMoshiSql(String string){
		if(getMoshiSql() == null)
			setMoshiSql("");
		moshiSql += string;
	}
	
	/**
	 * メインデータを取得するSQL文にStringを付加する
	 * @param string
	 */
	private void appendCountSql(String string){
		if(getCountSql() == null)
			setCountSql("");
		countSql += string;
	}
	
	/**
	 * 対象模試＋比較対象模試一覧を取得するSQL文にStringを付加する
	 * @param string
	 */
	private void appendMoshiListSql(String string){
		if(getMoshiListSql() == null)
			setMoshiListSql("");
		moshiListSql += string;
	}
	
	/**
	 * 対象模試＋比較対象模試リスト取得SQLのSELECT句追加分
	 */
	private String getMoshiListSelect(){
		
		String sqlOption ="";
		if(helpflg == true){
			sqlOption = " ,EXAMINATION.IN_DATAOPENDATE"+
					" FROM SUBRECORD_I"+
					" INNER JOIN EXAMINATION ON EXAMINATION.EXAMYEAR = SUBRECORD_I.EXAMYEAR"+
					" AND EXAMINATION.EXAMCD = SUBRECORD_I.EXAMCD";
		}else{
			sqlOption = " ,EXAMINATION.OUT_DATAOPENDATE"+
					" FROM SUBRECORD_I"+
					" INNER JOIN EXAMINATION ON EXAMINATION.EXAMYEAR = SUBRECORD_I.EXAMYEAR"+
					" AND EXAMINATION.EXAMCD = SUBRECORD_I.EXAMCD";
		}

		return sqlOption;
	}
	
	/**
	 * 対象模試＋比較対象模試リスト取得SQLのGROUP BY句追加分
	 */
	private String getMoshiListGroup(){
		
		String sqlOption = "";
		if(helpflg == true){
			sqlOption = " GROUP BY"+
			" SUBRECORD_I.INDIVIDUALID"+
			" ,EXAMINATION.EXAMNAME_ABBR"+
			" ,EXAMINATION.EXAMTYPECD"+
			" ,EXAMINATION.EXAMCD"+
			" ,EXAMINATION.EXAMYEAR"+
			" ,EXAMINATION.IN_DATAOPENDATE";
		}else{
			sqlOption = " GROUP BY"+
			" SUBRECORD_I.INDIVIDUALID"+
			" ,EXAMINATION.EXAMNAME_ABBR"+
			" ,EXAMINATION.EXAMTYPECD"+
			" ,EXAMINATION.EXAMCD"+
			" ,EXAMINATION.EXAMYEAR"+
			" ,EXAMINATION.OUT_DATAOPENDATE";
		}

		return sqlOption;
	}
	
	/**
	 * 指定の模試よりもOUT_DATAOPENDATEが大きいものを指定
	 * @param year
	 * @param cd
	 * @return
	 */
	private String getSequenceCondition(String year, String cd){
		
		String sqlOption ="";
		if(helpflg == true){
			sqlOption = "AND EXAMINATION.IN_DATAOPENDATE <= ("+
					" SELECT EXAMINATION.IN_DATAOPENDATE FROM EXAMINATION"+
					" WHERE EXAMINATION.EXAMYEAR = '"+year+"'"+
					" AND EXAMINATION.EXAMCD = '"+cd+"'"+
					" )";
		}else{
			sqlOption = "AND EXAMINATION.OUT_DATAOPENDATE <= ("+
					" SELECT EXAMINATION.OUT_DATAOPENDATE FROM EXAMINATION"+
					" WHERE EXAMINATION.EXAMYEAR = '"+year+"'"+
					" AND EXAMINATION.EXAMCD = '"+cd+"'"+
					" )";
		}

		return sqlOption;
	}

	/**科目配点200の物をカウントする
	 */
	private String get200count(){
		String sqlOption = 
							//" AND EXAMSUBJECT.SUBALLOTPNT = '200'"+//[1] delete
							//" AND (EXAMSUBJECT.SUBALLOTPNT = '200' OR EXAMSUBJECT.SUBCD = '1190') "+//[1] add//[5] delete
							//" AND ((SUBSTR(EXAMSUBJECT.SUBCD, 0, 1) = '1') OR (SUBSTR(EXAMSUBJECT.SUBCD, 0, 1) = '2') OR (SUBSTR(EXAMSUBJECT.SUBCD, 0, 1) = '3'))";//[5] delete
							" AND (V_I_CMEXAMSUBJECT.SUBALLOTPNT = '200' OR V_I_CMEXAMSUBJECT.SUBCD = '1190') "+//[5] add
							" AND ((SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '1') OR (SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '2') OR (SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '3'))";//[5] add
		return sqlOption;
	}
		
	/**
	 * 模試データ取得用ORDER BY
	 * @return
	 */
	private String getMoshiOrderBy(){
		String sqlOption = "";
		if(helpflg == true){
			sqlOption = ""+
			//" ORDER BY EXAMINATION.IN_DATAOPENDATE DESC, EXAMINATION.EXAMCD DESC, EXAMSUBJECT.DISPSEQUENCE ASC";//[5] delete
			" ORDER BY EXAMINATION.IN_DATAOPENDATE DESC, EXAMINATION.EXAMCD DESC, V_I_CMEXAMSUBJECT.DISPSEQUENCE ASC";//[5] add
		}else{
			sqlOption = ""+
			//" ORDER BY EXAMINATION.OUT_DATAOPENDATE DESC, EXAMINATION.EXAMCD DESC, EXAMSUBJECT.DISPSEQUENCE ASC";//[5] delete
			" ORDER BY EXAMINATION.OUT_DATAOPENDATE DESC, EXAMINATION.EXAMCD DESC, V_I_CMEXAMSUBJECT.DISPSEQUENCE ASC";//[5] add
		}
		return sqlOption;
	}
	
	/**
	 * 対象模試＋比較対象模試リスト取得SQLのORDER BY
	 * @return
	 */
	private String getMoshiListOrderBy(){
		String sqlOption = "";
		if(helpflg == true){
			sqlOption = ""+
			" ORDER BY EXAMINATION.IN_DATAOPENDATE DESC, EXAMINATION.EXAMCD DESC";
		}else{
			sqlOption = ""+
			" ORDER BY EXAMINATION.OUT_DATAOPENDATE DESC, EXAMINATION.EXAMCD DESC";
		}
		return sqlOption;
	}
	
	/**
	 * 模試年度とPERSONIDの指定
	 * @param string1
	 * @param string2
	 * @return
	 */
	private String getYearAndPersonCondition(String string1, String string2){
		String sqlOption = " WHERE SUBRECORD_I.INDIVIDUALID = ?"+
							" AND SUBRECORD_I.EXAMYEAR = '"+string2+"' ";
		return sqlOption;
	}
	
	/**
	 * PERSONIDの指定
	 * @param string1
	 * @return
	 */
	private String getPersonCondition(String string1){
		String sqlOption = " WHERE SUBRECORD_I.INDIVIDUALID = ?";
		return sqlOption;
	}
	
	/**
	 * 追加条件、対象もしと同じ種類コードを指定
	 * @param String examTypeCd
	 * @param String examYear
	 * @return
	 */
	private String getExamTypeCondition(String examYear, String examCd) {
		String sqlOption = " AND EXAMINATION.EXAMTYPECD = ( SELECT EXAMINATION.EXAMTYPECD FROM ";
		sqlOption += "EXAMINATION WHERE EXAMINATION.EXAMCD = '" +examCd+ "' AND ";
		sqlOption += "EXAMINATION.EXAMYEAR = '" +examYear+ "')";
		return sqlOption;
	}
	
	/**
	 * １．この年のこの模試の教科コードをすべて取得
	 * ２．取得した教科コードから教科名を取得
	 * @param string
	 * @param string2
	 * @return
	 */
	private Collection execSubNames(String year, String code) throws Exception {
		String nmGet = 
		//1215 kondo 追加 start
			"SELECT COURSE.COURSECD, COURSE.COURSENAME, EXAMINATION.TERGETGRADE " +
			" FROM COURSE, EXAMSUBJECT, EXAMINATION" +
			" WHERE" +
			" COURSE.COURSECD = RPAD(SUBSTR(EXAMSUBJECT.SUBCD, 1, 1), 4, 0)" +
			" AND EXAMSUBJECT.EXAMCD ='" + code + "'" +
			" AND EXAMSUBJECT.EXAMYEAR = '" + year + "'" +
			" AND EXAMINATION.EXAMCD = EXAMSUBJECT.EXAMCD "+
			" AND EXAMINATION.EXAMYEAR = EXAMSUBJECT.EXAMYEAR "+
			" AND COURSE.YEAR = EXAMSUBJECT.EXAMYEAR" +
			" GROUP BY COURSE.COURSECD, COURSE.COURSENAME, EXAMINATION.TERGETGRADE" +
			" ORDER BY COURSE.COURSECD";
		//1215 kondo 追加 end
		
		//1215 kondo 削除 start
		/*
			"SELECT COURSE.COURSECD, COURSE.COURSENAME" +
			" FROM COURSE, EXAMSUBJECT" +
			" WHERE" +
			" COURSE.COURSECD = RPAD(SUBSTR(EXAMSUBJECT.SUBCD, 1, 1), 4, 0)" +
			" AND EXAMSUBJECT.EXAMCD ='" + code + "'" +
			" AND EXAMSUBJECT.EXAMYEAR = '" + year + "'" +
			" AND COURSE.YEAR = EXAMSUBJECT.EXAMYEAR" +
			" GROUP BY COURSE.COURSECD, COURSE.COURSENAME" +
			" ORDER BY COURSE.COURSECD";
		*/
		//1215 kondo 削除 end
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
				
			pstmt = conn.prepareStatement(nmGet);
			rs = pstmt.executeQuery();
			
			//1215 kondo 追加 start
			int courseCount = 0;
			//1215 kondo 追加 end
			
			//教科をリストにつめる。
			Collection courseDatas = new ArrayList();
			while(rs.next()) {
				//1215 kondo 追加 start
				if(rs.getString("TERGETGRADE").equals("03")) {
					//対象学年が３年の場合、５教科を設定
					CourseData courseData0 = new CourseData();
					courseData0.setCourseName("総合");
					courseData0.setCourseCd("7000");
					courseDatas.add(courseData0);
					
					CourseData courseData1 = new CourseData();
					courseData1.setCourseName("英語");
					courseData1.setCourseCd("1000");
					courseDatas.add(courseData1);
					
					CourseData courseData2 = new CourseData();
					courseData2.setCourseName("数学");
					courseData2.setCourseCd("2000");
					courseDatas.add(courseData2);
					
					CourseData courseData3 = new CourseData();
					courseData3.setCourseName("国語");
					courseData3.setCourseCd("3000");
					courseDatas.add(courseData3);
					
					CourseData courseData4 = new CourseData();
					courseData4.setCourseName("理科");
					courseData4.setCourseCd("4000");
					courseDatas.add(courseData4);
					
					CourseData courseData5 = new CourseData();
					courseData5.setCourseName("地歴公民");
					courseData5.setCourseCd("5000");
					courseDatas.add(courseData5);
					break;
				}
				//1215 kondo 追加 end
				
				//1215 kondo 追加 start
				if(courseCount == 0) {
					CourseData courseAll = new CourseData();
					courseAll.setCourseName("総合");
					courseAll.setCourseCd("7000");
					((List)courseDatas).add(0, courseAll);
					courseCount++;
				}
				//1215 kondo 追加 end
				
				CourseData courseData = new CourseData();
				courseData.setCourseName(rs.getString("COURSENAME"));
				courseData.setCourseCd(rs.getString("COURSECD"));
				
				//1215 kondo 削除 start
				/*
				if(Integer.parseInt(rs.getString("COURSECD").substring(0, 1)) >= 7) {
					//総合だったら先頭に追加
					//総合は総合１とか総合２とかあるけども全て総合で固定
					if(Integer.parseInt(rs.getString("COURSECD").substring(0, 1)) < 8) {
						courseData.setCourseName("総合");
						((List)courseDatas).add(0, courseData);
					}
					
				} else {
				*/
				//1215 kondo 削除 end
				
					//総合ではない
					if(Integer.parseInt(rs.getString("COURSECD").substring(0, 1)) < 6) {
						courseDatas.add(courseData);
					}
				
				//1215 kondo 削除 start
				/*
				}
				*/
				//1215 kondo 削除 end
			}
			
			
			
			if(pstmt != null) {pstmt.close();}
			if(rs != null) {rs.close();}
			
			return courseDatas;
			
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}
	
	/**
	 * 偏差値データの取得
	 * @return　Collection
	 * @throws Exception
	 */
	private Collection execDeviations() throws Exception{	
		final String ELSUBCD = "1190"; //[1] add 英語＋リスニング
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PreparedStatement pstmt2 = null;
		ResultSet rs2 = null;
		PreparedStatement pstmt3 = null;
		ResultSet rs3 = null;
		try{

			// "第1解答模試を表示"選択時
			if ("3".equals(avgChoice)) {
				// 対象模試＋比較対象模試の一覧を取得
				pstmt3 = conn.prepareStatement(getMoshiListSql());
				pstmt3.setString(1, personId);
				rs3 = pstmt3.executeQuery();
	
				// 対象模試＋比較対象模試に第1解答科目が無い場合は"平均を表示"にする
				boolean ans1st = false;
				while(rs3.next()){
					if (KNUtil.isAns1st(rs3.getString("EXAMCD"))) {
						ans1st = true;
					}
				}
				if (!ans1st) {
					avgChoice = "1";
				}
				
				if(pstmt3 != null) pstmt3.close();
				if(rs3 != null) rs3.close();
			}
			
			pstmt = conn.prepareStatement(getCountSql());		
			pstmt.setString(1, personId);
			rs = pstmt.executeQuery();
			ArrayList examNameM200 = new ArrayList();
			ArrayList examNameJ200 = new ArrayList();
			ArrayList examNameE200 = new ArrayList();
			ArrayList examNameEL = new ArrayList(); //[1] add 英語＋リスニング
			String subIndex = "";
			while(rs.next()){
				//配点が200以上の数学・国語を持つ模試リスト作成
				if(rs.getString("SUBCD") != null){
					subIndex = rs.getString("SUBCD").substring(0, 1);
					
					if(subIndex.equals("2")){
						examNameM200.add(nullToEmpty(rs.getString("EXAMNAME_ABBR"))+nullToEmpty(rs.getString("EXAMYEAR"))+nullToEmpty(rs.getString("EXAMCD")));
					}else{
						if(subIndex.equals("3")){
							examNameJ200.add(nullToEmpty(rs.getString("EXAMNAME_ABBR"))+nullToEmpty(rs.getString("EXAMYEAR"))+nullToEmpty(rs.getString("EXAMCD")));
						}else{
							examNameE200.add(nullToEmpty(rs.getString("EXAMNAME_ABBR"))+nullToEmpty(rs.getString("EXAMYEAR"))+nullToEmpty(rs.getString("EXAMCD")));
							//[1] add start
							if(rs.getString("SUBCD").equals(ELSUBCD)){
								examNameEL.add(rs.getString("EXAMNAME_ABBR")); //英語＋リスニング
							}
							//[1] add end
						}
					}
				}
			}
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();	
			
			pstmt2 = conn.prepareStatement(getMoshiSql());
			pstmt2.setString(1, personId);
			rs2 = pstmt2.executeQuery();
			Map mappy = new LinkedHashMap();
			//総合フラグ
			boolean sogoflg =false;
			boolean mathflg =false;
			boolean japanflg =false;
			boolean engflg =false;
			while(rs2.next()){			
				IExamData examinationData = (IExamData) mappy.get(nullToEmpty(rs2.getString("EXAMYEAR")) + nullToEmpty(rs2.getString("EXAMCD")));
				if(examinationData == null){
					//マップの中にこの模試が存在しなければ新たに作成
					//同時に科目のコレクションの初期化
					examinationData = new IExamData();
					if(getExaminationDatas() == null)
						setExaminationDatas(new ArrayList());
					getExaminationDatas().add(examinationData);
					mappy.put(nullToEmpty(rs2.getString("EXAMYEAR")) + nullToEmpty(rs2.getString("EXAMCD")), examinationData);
					//模試が変わったので初期化
					sogoflg = false;
					mathflg = false;
					japanflg = false;
					engflg = false;
				}
				examinationData.setExamYear(nullToEmpty(rs2.getString("EXAMYEAR")));
				examinationData.setExamCd(nullToEmpty(rs2.getString("EXAMCD")));
				examinationData.setExamName(nullToEmpty(rs2.getString("EXAMNAME_ABBR")));
				examinationData.setExamTypeCd(nullToEmpty(rs2.getString("EXAMTYPECD")));
				
				Collection subRecordDatas = examinationData.getSubRecordDatas();
				if(subRecordDatas == null){
					subRecordDatas = new ArrayList();
					examinationData.setSubRecordDatas(subRecordDatas);
				}
				SubRecordIData subRecordData = new SubRecordIData();
				
				subIndex = "";				
				if(rs2.getString("SUBCD") != null){
					subIndex = rs2.getString("SUBCD").substring(0, 1);
				}			
				if(examNameE200.contains(nullToEmpty(rs2.getString("EXAMNAME_ABBR"))+nullToEmpty(rs2.getString("EXAMYEAR")) + nullToEmpty(rs2.getString("EXAMCD"))) 
						&& subIndex.equals("1") && engflg == false){
					//[1] add start
					//模試が英語＋Ｌリストに存在する場合、英語＋Ｌに限定
					if(examNameEL.contains(rs2.getString("EXAMNAME_ABBR"))) {
						if(nullToEmpty(rs2.getString("SUBCD")).equals(ELSUBCD)) {
							//英語＋Ｌ
							if(rs2.getString("A_DEVIATION") != null){
								subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));
								choiceScholarLevel(subRecordData, avgChoice, 
										rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
								subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));
								subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
								subRecordDatas.add(subRecordData);
							}
							engflg = true;
						}
					} else {
						if(nullToEmpty(rs2.getString("SUBALLOTPNT")).equals("200")) {
							//英語得点200得点
							if(rs2.getString("A_DEVIATION") != null){
								subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));
								choiceScholarLevel(subRecordData, avgChoice, 
										rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
								subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));
								subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
								subRecordDatas.add(subRecordData);
							}
							engflg = true;
						}
					}
				}else if(!examNameE200.contains(nullToEmpty(rs2.getString("EXAMNAME_ABBR"))+nullToEmpty(rs2.getString("EXAMYEAR"))+nullToEmpty(rs2.getString("EXAMCD"))) && subIndex.equals("1") && engflg == false){
					if(rs2.getString("A_DEVIATION") != null){
						subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));	//科目偏差値
						choiceScholarLevel(subRecordData, avgChoice, 
								rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
						subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));				//科目コード
						subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
						subRecordDatas.add(subRecordData);
					}
					engflg = true;
				}else if(examNameM200.contains(nullToEmpty(rs2.getString("EXAMNAME_ABBR"))+nullToEmpty(rs2.getString("EXAMYEAR")) + nullToEmpty(rs2.getString("EXAMCD"))) && nullToEmpty(rs2.getString("SUBALLOTPNT")).equals("200") && subIndex.equals("2") && mathflg == false){//[2] change
					if(rs2.getString("A_DEVIATION") != null){
						subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));	//科目偏差値
						choiceScholarLevel(subRecordData, avgChoice, 
								rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
						subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));				//科目コード
						subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
						subRecordDatas.add(subRecordData);
					}
					mathflg = true;//[2] add
				}else if(!examNameM200.contains(nullToEmpty(rs2.getString("EXAMNAME_ABBR"))+nullToEmpty(rs2.getString("EXAMYEAR"))+nullToEmpty(rs2.getString("EXAMCD"))) && subIndex.equals("2") && mathflg == false){
					if(rs2.getString("A_DEVIATION") != null){
						subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));	//科目偏差値
						choiceScholarLevel(subRecordData, avgChoice, 
								rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
						subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));				//科目コード
						subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
						subRecordDatas.add(subRecordData);
					}
					mathflg = true;
				}else if(examNameJ200.contains(nullToEmpty(rs2.getString("EXAMNAME_ABBR"))+nullToEmpty(rs2.getString("EXAMYEAR")) + nullToEmpty(rs2.getString("EXAMCD"))) && nullToEmpty(rs2.getString("SUBALLOTPNT")).equals("200") && subIndex.equals("3") && japanflg == false){//[2] change
					//国語配点200得点
					if(rs2.getString("A_DEVIATION") != null){
						subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));
						choiceScholarLevel(subRecordData, avgChoice, 
								rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
						subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));
						subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
						subRecordDatas.add(subRecordData);
					}
					japanflg = true;//[2] add
				}else if(!examNameJ200.contains(nullToEmpty(rs2.getString("EXAMNAME_ABBR"))+nullToEmpty(rs2.getString("EXAMYEAR")) + nullToEmpty(rs2.getString("EXAMCD"))) && subIndex.equals("3") && japanflg == false){
					//国語配点200ではない得点の表示順序1番目
					if(rs2.getString("A_DEVIATION") != null){		
						subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));
						choiceScholarLevel(subRecordData, avgChoice, 
								rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
						subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));
						subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
						subRecordDatas.add(subRecordData);
					}
					japanflg = true;
				}else if(subIndex.equals("4") || subIndex.equals("5") || subIndex.equals("6")){
					if(rs2.getString("A_DEVIATION") != null){
						subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));	//科目偏差値
						choiceScholarLevel(subRecordData, avgChoice, 
								rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
						subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));				//科目コード
						subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
						subRecordDatas.add(subRecordData);
					}
				}else if(subIndex.equals("7") && sogoflg == false){
					//総合は表示順の一番初め値を表示する
					if(rs2.getString("A_DEVIATION") != null){
						subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));	//科目偏差値
						choiceScholarLevel(subRecordData, avgChoice, 
								rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
						subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));				//科目コード
						subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
						subRecordDatas.add(subRecordData);
					}
					sogoflg = true;
				}
			}//while
			if(pstmt2 != null) {pstmt2.close();}
			if(rs2 != null) {rs2.close();}
			
			return orderStraight(mappy.values());
			//return mappy.values();
			
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
			if(pstmt2 != null) pstmt2.close();
			if(rs2 != null) rs2.close();
			if(pstmt3 != null) pstmt3.close();
			if(rs3 != null) rs3.close();
		}

	}
	/**
	 * このとき取得したデータが７つ以上であれば７つに
	 * 制限して、それ以下で残りを空白で埋める
	 * @param collection
	 * @return
	 */
	private Collection orderStraight(Collection collection){
		int DEFAULT_MAX_SIZE = 7;
		
		//コピーを作成する前に、正常取得できたものの数を控えておく
		setExaminationDatasSize(collection.size());
			
		//collectionの正常なコピーを作成する必要がある
		List data = new ArrayList();
		int count = 0;
		for (java.util.Iterator it=collection.iterator(); it.hasNext(); ) {
			if(count >= DEFAULT_MAX_SIZE) {
				break;
			}
			data.add(0,(IExamData)it.next());
			count++;
		}
		int difference = DEFAULT_MAX_SIZE - data.size();
		if(difference < 0){
		//値がdifference分多いから７にカットする
			//for(int i=0; i<DEFAULT_MAX_SIZE; i++){
			while(data.size() < DEFAULT_MAX_SIZE){
				data.remove(data.size());
			}
		}else if(difference > 0){
		//値がdifference分足りないので7に埋める
			while(data.size() < DEFAULT_MAX_SIZE){
				data.add(IExamData.getEmptyIExamData());
			}
		}
		return data;		
	}
	
	/**
	 * アプレット用、５教科リストを取得する。
	 * @return String 強化リスト
	 */
	public String getLineItemName() {
		
		String itemName = "";//戻り値
		boolean courseFlg = true;//教科初回フラグ(true:一番最初の教科 false:それ以外の教科)
		
		for (java.util.Iterator it=getCourseDatas().iterator(); it.hasNext();) {
			CourseData course = (CourseData)it.next();

			if(course.getCourseCd().substring(0, 1).equals("7") || course.getCourseCd().substring(0, 1).equals("8")
				|| course.getCourseCd().substring(0, 1).equals("9")) {
				//総合なら、追加しない。
			} else {
				if(!courseFlg) {itemName += ",";}
				itemName += course.getCourseName();
				courseFlg = false;
			}
		}
		return itemName;
	}
	
	/**
	 * 現年度を返します。
	 * @return 現年度
	 */
	public String getThisYear() {
//[3] delete start
//		String thisYear = "";
//		Calendar cal1 = Calendar.getInstance();
//		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");//デートフォーマット
//		String nowYmd = sdf1.format(cal1.getTime());
//		int year = Integer.parseInt(nowYmd.substring(0, 4));
//		int monthDay = Integer.parseInt(nowYmd.substring(4, 8));		
//		if(monthDay < 0401) {
//			thisYear = Integer.toString(year - 1);
//		} else {
//			thisYear = Integer.toString(year);
//		}
//		return thisYear;
//[3] delete end
//		return KNUtil.getCurrentYear();//[3] add//[4] delete
		return getClassYear();//[4] add
	}
	
	/**
	 * nullが来たら空白にする
	 */
	private String nullToEmpty(String str) {
		if(str == null) {
			return "";
		}
		if(str.equals("null")) {
			return "";
		}
		return str;
	}
	/**
	 * StringをFloatのフォーマットで返します。
	 * @return
	 */
	public String nullToEmptyF(String str) {
		if(str == null) {
			str = "";
			return str;
		} else{
				return String.valueOf(new Float(str).floatValue());
		}
	}
	/**
	 * @return
	 */
	public String[] getDeviationRanges() {
		return deviationRanges;
	}

	/**
	 * @return
	 */
	public String getMoshiSql() {
		return moshiSql;
	}

	/**
	 * @return
	 */
	public String getPersonId() {
		return personId;
	}

	/**
	 * @return
	 */
	public String getSQL_BASE() {
		return SQL_BASE;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @param strings
	 */
	public void setDeviationRanges(String[] strings) {
		deviationRanges = strings;
	}

	/**
	 * @param string
	 */
	public void setMoshiSql(String string) {
		moshiSql = string;
	}

	/**
	 * @param string
	 */
	public void setPersonId(String string) {
		personId = string;
	}

	/**
	 * @param string
	 */
	public void setSQL_BASE(String string) {
		SQL_BASE = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * @return
	 */
	public boolean isFirstAccess() {
		return isFirstAccess;
	}

	/**
	 * @param b
	 */
	public void setFirstAccess(boolean b) {
		isFirstAccess = b;
	}

	/**
	 * @return
	 */
	public Collection getExaminationDatas() {
		return examinationDatas;
	}

	/**
	 * @param collection
	 */
	public void setExaminationDatas(Collection collection) {
		examinationDatas = collection;
	}

	/**
	 * @return
	 */
	public String getSubNumSql() {
		return subNumSql;
	}

	/**
	 * @param string
	 */
	public void setSubNumSql(String string) {
		subNumSql = string;
	}

	/**
	 * 学年の検索方法を返します
	 * @return String
	 */
	public String getTargetGrade() {
		return targetGrade;
	}

	/**
	 * 学年の検索方法を設定します
	 * @param String string
	 */
	public void setTargetGrade(String string) {
		targetGrade = string;
	}

	/**
	 * 模試種類の検索方法を返します
	 * @return String
	 */
	public String getTargetExamType() {
		return targetExamType;
	}

	/**
	 * 模試種類の検索方法を設定します
	 * @param String string
	 */
	public void setTargetExamType(String string) {
		targetExamType = string;
	}
	
	
	/**
	 * @return
	 */
	public Collection getCourseDatas() {
		return courseDatas;
	}

	/**
	 * @param collection
	 */
	public void setCourseDatas(Collection collection) {
		courseDatas = collection;
	}

	/**
	 * @return
	 */
	public int getExaminationDatasSize() {
		return examinationDatasSize;
	}

	/**
	 * @param i
	 */
	public void setExaminationDatasSize(int i) {
		examinationDatasSize = i;
	}

	/**
	 * @return
	 */
	public String getCountSql() {
		return countSql;
	}

	/**
	 * @param string
	 */
	public void setCountSql(String string) {
		countSql = string;
	}

	/**
	 * @return
	 */
	public String getMoshiListSql() {
		return moshiListSql;
	}

	/**
	 * @param string
	 */
	public void setMoshiListSql(String string) {
		moshiListSql = string;
	}

	/**
	 * @return
	 */
	public boolean isHelpflg() {
		return helpflg;
	}

	/**
	 * @param b
	 */
	public void setHelpflg(boolean b) {
		helpflg = b;
	}

	/**
	 * @return
	 */
	public String getClassYear() {
		return classYear;//[4] add
	}

	/**
	 * @param string
	 */
	public void setClassYear(String string) {
		classYear = string;//[4] add
	}

	public void setAvgChoice(String avgChoice) {
		this.avgChoice = avgChoice;
	}
	
	public String getAvgChoice() {
		return avgChoice;
	}
	
	/**
	 * 画面の選択状態（平均値／良い方）で、出力する学力レベルを選択する。
	 * @param hensa
	 * @param flg
	 * @param avg
	 * @param lvl
	 */
	private void choiceScholarLevel(SubRecordIData data,
			final String flg, final String avg, final String lvl) {
		
		if ("1".equals(flg)) {
			//平均値の場合、偏差値から学力レベルを出力
			data.setScholarlevel(IExamData.makeScholarLevel(avg));
		}
		else {
			//良い方の場合、SQLから取得した学力レベルを出力
			data.setScholarlevel(GeneralUtil.toBlank(lvl));
		}
		
	}

	/**
	 * 第1解答科目模試かどうか
	 * @return
	 */
	public boolean isAns1stExam() {
		return KNUtil.isAns1st(getTargetExamCode());
	}

	/**
	 * 第1解答科目模試かどうか（対象模試＋比較対象模試）
	 * @return
	 */
	public boolean isAns1stExamCodes() {
		for (final Iterator ite = examinationDatas.iterator(); ite.hasNext();) {
			final IExamData data = (IExamData) ite.next();
			if (KNUtil.isAns1st(data.getExamCd())){
				return true;
			}
		}
		return false;
	}
}

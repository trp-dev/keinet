/*
 * 作成日: 2004/07/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.taglib.cmfactory;

import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;

import jp.co.fj.keinavi.beans.com_set.SubjectBean;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.com_set.ComSubjectData;
import jp.co.fj.keinavi.forms.BaseForm;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM101Form;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM101TagFactory extends AbstractCMTagFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.taglib.cmfactory.AbstractCMTagFactory#createCMStatus(javax.servlet.ServletRequest)
	 */
	public String createCMStatus(ServletRequest request, HttpSession session) throws JspException {
		Map item = super.getItemMap(session);
		BaseForm form = super.getBaseForm(request);
		ExamSession	examSession = super.getExamSession(session);

		// 全選択
		if (new Short("1").equals(item.get(IProfileItem.TYPE_SELECTION))) {
			return "受験生がいる全種類の型を対象にする";

		// 個別選択
		} else {
			// 型Bean
			SubjectBean bean = (SubjectBean) request.getAttribute("TypeBean");

			// 設定値を取得する
			ComSubjectData data = ProfileUtil.getTypeValue(
				super.getProfile(session),
				examSession,
				examSession.getExamData(form.getTargetYear(), form.getTargetExam()),
				bean,
				true);
			
			if (data != null && data.getISubjectData().size() > 0)
				return data.getISubjectData().size() + "種";
		}

		return "設定なし";
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.taglib.cmfactory.AbstractCMTagFactory#createTempCMStatus(javax.servlet.ServletRequest, javax.servlet.http.HttpSession)
	 */
	public String createTempCMStatus(ServletRequest request, HttpSession session) throws JspException {
		CM001Form f001 = super.getCommonForm(session);
		CM101Form f101 = (CM101Form) f001.getActionForm("CM101Form");
	
		// 個別アクションフォームがなければ設定画面を開いていないので
		// プロファイルの値を評価する
		if (f101 == null) {
			request.setAttribute("form", f001);
			return this.createCMStatus(request, session);

		// セッション設定値
		} else {
			// 全選択
			if ("1".equals(f101.getSelection())) {
				return "受験生がいる全種類の型を対象にする";			

			// 個別選択
			} else {
				if (f101.getAnalyze2() == null || f101.getAnalyze2().length == 0) {
					return "設定なし";
				} else {
					return f101.getAnalyze2().length + "種";
				}
			}			
		}
	}

}

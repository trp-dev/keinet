package jp.co.fj.keinavi.excel.data.school;

/**
 * �Z�����ѕ��́|�߉��r�|�΍��l���z �΍��l���z���X�g
 * �쐬��: 2004/07/20
 * @author	A.Iwata
 */
public class S52BnpListBean {
	//�΍��l�я���l
	private float floBnpMax = 0;
	//�΍��l�щ����l
	private float floBnpMin = 0;
	//�l��
	private int intNinzu = 0;
	//�\����
	private float floKoseihi = 0;

	
	/*----------*/
	/* Get      */
	/*----------*/
	public float getFloBnpMax() {
		return this.floBnpMax;
	}
	public float getFloBnpMin() {
		return this.floBnpMin;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public float getFloKoseihi() {
		return this.floKoseihi;
	}


	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setFloBnpMax(float floBnpMax) {
		this.floBnpMax = floBnpMax;
	}
	public void setFloBnpMin(float floBnpMin) {
		this.floBnpMin = floBnpMin;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setFloKoseihi(float floKoseihi) {
		this.floKoseihi = floKoseihi;
	}
}

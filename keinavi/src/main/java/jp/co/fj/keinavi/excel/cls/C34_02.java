package jp.co.fj.keinavi.excel.cls;

//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.ListIterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C34ClassListBean;
import jp.co.fj.keinavi.excel.data.cls.C34HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.cls.C34Item;
import jp.co.fj.keinavi.excel.data.cls.C34ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * 作成日: 2004/09/
 * @author A.Hasegawa
 *
 * クラス成績分析−過年度比較−志望大学評価別人数　大学（日程あり）出力処理
 * 
 * <2010年度改修>
 * 2010.01.06   Tomohisa YAMADA - Totec
 *              センター・リサーチ５段階評価対応
 * 
 */
public class C34_02 {
	
	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow		= 50;			//MAX行数
	private int		intMaxClassSr   = 5;			//MAXクラス数

	private int intDataStartRow = 8;					//データセット開始行
	private int intDataStartCol = 9;					//データセット開始列
	
	final private String masterfile0 = "C34_02" ;
	final private String masterfile1 = "C34_02" ;
	private String masterfile = "" ;
	
	final private String strCenterResearch = "38";

	/*
	 * 	Excel編集メイン
	 * 		C34Item C34Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int c34_02EditExcel(C34Item c34Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("C34_02","C34_02帳票作成開始","");

		//テンプレートの決定
		if (c34Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		/** セット処理 **/
		boolean bolBookCngFlg = true;			//改ファイルフラグ	
		boolean bolSheetCngFlg = true;			//改シートフラグ
			
		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFCell	workCell		= null;
		HSSFRow		workRow			= null;
		HSSFWorkbook	tmpworkbook = null;
		
		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intCol = 0;

		String strGenekiKbn = "";
		String strDaigakuCode = "";
		String strNtiCode = "";
		String strHyoukaKbn = "";

		//2004.09.27Add
		String strKokushiKbn = "";
		String strStKokushiKbn = "";
		int intChangeSheetRow = 0;
		//2004.09.27AddEnd
		
		int intLoopCnt = 0;
		int intBookCngCount = 1;
		
		boolean bolDispHyoukaFlg = false;
		boolean bolDispNtiFlg = false;
		boolean bolDispDaigakuFlg = false;
		String strHyouka = "";
		
		String strDispTarget = "";
		
		ArrayList WorkBookList = new ArrayList();
		ArrayList WorkSheetList = new ArrayList();
		
		int intSheetCnt=0;
		
		int intBookIndex = -1;
		int intSheetIndex = -1;
		
		int intSetRow = intDataStartRow;
		
		boolean bolClassDispFlg=true;
		
		ArrayList HyoukaTitleList = new ArrayList();
		
		// 基本ファイルを読込む
		C34ListBean c34ListBean = new C34ListBean();
		
		try{
			/** C34Item編集 **/
			c34Item  = editC34( c34Item );
			
			HyoukaTitleList.add(0,"A");
			HyoukaTitleList.add(1,"B");
			HyoukaTitleList.add(2,"C");

			// 基本ファイルを読込む
			ArrayList c34List = c34Item.getC34List();
			ListIterator itr = c34List.listIterator();
			
//			if( itr.hasNext() == false ){
//				return errfdata;
//			}

			/** 大学・日程・評価 **/
			// C34ListBean 
			while( itr.hasNext() ){
				c34ListBean = (C34ListBean)itr.next();
				
				// 大学名
				bolDispDaigakuFlg = false;
				if( !cm.toString(strDaigakuCode).equals(cm.toString(c34ListBean.getStrDaigakuCd()))){
					bolDispDaigakuFlg = true;
					//大学名表示
					strDaigakuCode = c34ListBean.getStrDaigakuCd();
				}
				
				// 日程
				bolDispNtiFlg = false;
				if( !cm.toString(strNtiCode).equals(cm.toString(c34ListBean.getStrNtiCd())) ){
					bolDispNtiFlg = true;
					strNtiCode = c34ListBean.getStrNtiCd();
				}

				// 評価区分
				bolDispHyoukaFlg = false;
				if( !cm.toString(strHyoukaKbn).equals(cm.toString(c34ListBean.getStrHyouka())) ){
					bolDispHyoukaFlg = true;
					//評価区分表示
					strHyoukaKbn = c34ListBean.getStrHyouka();
					strHyouka = "";
					switch( Integer.parseInt(strHyoukaKbn) ){
						case 1:
							strHyouka = "センター";
							break;
						case 2:
							strHyouka = "2次・私大";
							break;
						case 3:
							strHyouka = "総合";
							break;
					}
				}

				// 表示対象
				if( !cm.toString(strGenekiKbn).equals(cm.toString(c34ListBean.getStrGenKouKbn())) ){
					strGenekiKbn = c34ListBean.getStrGenKouKbn();
					if( cm.toString(strGenekiKbn).equals("0") ){
						strDispTarget = "全体";
					}
					else if( cm.toString(strGenekiKbn).equals("1") ){
						strDispTarget = "現役生";
					}
					else if( cm.toString(strGenekiKbn).equals("2") ){
						strDispTarget = "高卒生";
					}

					if( bolSheetCngFlg == false ){
						bolSheetCngFlg = true;
					}
				}

				//2004.9.27 Add
				int intDaigakuCd = (int)Integer.parseInt(c34ListBean.getStrDaigakuCd());
				if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
					strKokushiKbn = "国公立大";
				}
				if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
					strKokushiKbn = "私立大";
				}
				if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
					strKokushiKbn = "短大";
				}
				if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
					strKokushiKbn = "その他";
				}
				
				if( !strKokushiKbn.equals(strStKokushiKbn) ){
					bolSheetCngFlg = true;
					strStKokushiKbn = strKokushiKbn;
				}
				//2004.9.27 Add End
				
				//クラス別評価人数リスト読み込み クラス名をExcelへセット
				ArrayList c34ClassList = c34ListBean.getC34ClassList();
				ListIterator itr2 = c34ClassList.listIterator();

				// Listの取得
				C34ClassListBean c34ClassListBean = new C34ClassListBean();
				C34ClassListBean HomeDataBean = new C34ClassListBean();

				//クラス数取得
				int intClassSr = c34ClassList.size();

//				int intSheetSr = (int)Math.ceil(intClassSr / intMaxClassSr);
//				int intSheetSr = intClassSr / intMaxClassSr;
//				if(intClassSr%intMaxClassSr!=0){
//					intSheetSr++;
//				}
				int intSheetSr = (intClassSr - 1) / intMaxClassSr;
				if((intClassSr - 1)%intMaxClassSr!=0){
					intSheetSr++;
				}
				
				int intLoopCnt2 = 0;
				int intNendoCount = 0;
				intChangeSheetRow = 0;
				
				/** 学校名・学年・クラス **/
				// C34ClassListBean
				while( itr2.hasNext() ){
					c34ClassListBean = (C34ClassListBean)itr2.next();

					// Listの取得
					ArrayList c34HyoukaNinzuList = c34ClassListBean.getC34HyoukaNinzuList();
					C34HyoukaNinzuListBean c34HyoukaNinzuListBean = new C34HyoukaNinzuListBean();
					
					if( intLoopCnt2 == 0 ){
						HomeDataBean = c34ClassListBean;
						c34ClassListBean = (C34ClassListBean)itr2.next();
						c34HyoukaNinzuList = c34ClassListBean.getC34HyoukaNinzuList();
						intNendoCount = c34HyoukaNinzuList.size();
						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
						intLoopCnt2++;
					}
					ListIterator itr3 = c34HyoukaNinzuList.listIterator();
//					ArrayList c34HyoukaNinzuList = c34ClassListBean.getC34HyoukaNinzuList();
//					C34HyoukaNinzuListBean c34HyoukaNinzuListBean = new C34HyoukaNinzuListBean();
//					ListIterator itr3 = c34HyoukaNinzuList.listIterator();
//
//					// 自校情報表示判定
//					if( intLoopCnt2 == 0 ){
//						bolHomeDataDispFlg = true;
//						HomeDataBean = c34ClassListBean;
//						c34ClassListBean = (C34ClassListBean)itr2.next();
//						intNendoCount = c34HyoukaNinzuList.size();
//						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
//						intLoopCnt2++;
//					}

					// 始点情報（Col）
					if( intLoopCnt2 % intMaxClassSr == 0 ){
						intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 5 );
					}else{
						intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 5 );
					}
					
//					intCol = intDataStartCol + ( ( intMaxClassSr - ( intLoopCnt2 % intMaxClassSr ) ) * 5 );

					/** 年度・志望者数 **/
					// C34HyoukaNinzuListBean
					int intHyoukaNinzuListIndex = 0;
					while( itr3.hasNext() ){
						c34HyoukaNinzuListBean = (C34HyoukaNinzuListBean)itr3.next();

						// 規定行数を表示したら線を引く
						if(( intSetRow  >= intChangeSheetRow )||((bolSheetCngFlg == true)&&(intMaxSheetIndex != 0))){
							bolSheetCngFlg = true;
							for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//								int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
								int intBIndex = (intSindex + 1) / intMaxSheetSr;
								if((intSindex + 1)%intMaxSheetSr!=0){
									intBIndex++;
								}
								workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
								workSheet = (HSSFSheet)WorkSheetList.get(intSindex);
								
								// 罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intSetRow, intSetRow, 0, 33, false, true, false, false );
								if( workSheet != null ){
									int intRemoveStartRow = intChangeSheetRow;
									if( intMaxRow + intDataStartRow > intRemoveStartRow ){
										for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
											//行削除処理
											workSheet.removeRow(workSheet.getRow(index));
										}
										//罫線出力（セルの上部に太線を出力）
										cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
														intRemoveStartRow, intRemoveStartRow, 0, 33, false, true, false, false);
									}
								}
			
							}

							// 保存処理
							int intSheetNum = 0;
							switch( intSaveFlg ){
								case 1:
								case 5:
									intSheetNum = ((HSSFWorkbook)WorkBookList.get(0)).getNumberOfSheets()-1;
									break;
								case 2:
								case 3:
								case 4:
									intSheetNum = ((HSSFWorkbook)WorkBookList.get(0)).getNumberOfSheets()-2;
									break;
							}

							if( intSheetNum >= intMaxSheetSr ){
								tmpworkbook = (HSSFWorkbook)WorkBookList.get(0);
								
								boolean bolRet = false;
								// Excelファイル保存
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, tmpworkbook, UserID, intBookCngCount, masterfile, intSheetNum);

								if( bolRet == false ){
									return errfwrite;
								}
								intBookCngCount++;
								
								// 削除
								WorkBookList.remove(0);
								for(int a=0; a<intMaxSheetSr;a++){
									WorkSheetList.remove(0);
								}
								tmpworkbook=null;
								intBookIndex--;
								intSheetIndex -= intMaxSheetSr;
							}
						}

						// 改シート判定
						if( bolSheetCngFlg == true ){
							
							bolDispHyoukaFlg = true;
							bolDispDaigakuFlg = true;
														
							intSetRow = intDataStartRow;
							bolClassDispFlg = true;
							
							for( int intIndex = 0; intIndex < intSheetSr; intIndex ++ ){
								
								if( ( intMaxSheetIndex >= intMaxSheetSr ) || ( bolBookCngFlg == true ) ){
									// マスタExcel読み込み
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook==null ){
										return errfread;
									}
									WorkBookList.add(workbook);
									intBookIndex++;

									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
									workbook = (HSSFWorkbook)WorkBookList.get(intBookIndex);
								}
								
								//シート作成
								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);
								WorkSheetList.add(workSheet);
								intSheetIndex++;
								workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex);

								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);
				
								// セキュリティスタンプセット
								String secuFlg = cm.setSecurity( workbook, workSheet, c34Item.getIntSecuFlg(), 31, 33 );
								workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
								workCell.setCellValue(secuFlg);
				
								// 学校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
								workCell.setCellValue( "学校名　：" + cm.toString(c34Item.getStrGakkomei()) );
		
								// 模試月取得
								String moshi =cm.setTaisyouMoshi( c34Item.getStrMshDate() );
								
								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
								workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(c34Item.getStrMshmei()) + moshi);
					
								//表示対象
								workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
								workCell.setCellValue( "表示対象：" + strDispTarget );
								
								//評価別人数 見出し行
								for( int i = 0; i <= intMaxClassSr; i++ ){
									int intTitleSetCol = (i * 5) + ( intDataStartCol - 3 );
									
									//出願予定・第1志望
									if (strKokushiKbn == "国公立大") { 
										workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
										workCell.setCellValue( "出願予定" );
									} else {
										workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
										workCell.setCellValue( "第１志望" );
									}
								
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol );
									workCell.setCellValue( (String)HyoukaTitleList.get(0) );
									
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+1 );
									workCell.setCellValue( (String)HyoukaTitleList.get(1) );
									
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+2 );
									workCell.setCellValue( (String)HyoukaTitleList.get(2) );
								}
							
								intMaxSheetIndex++;		//シート数カウンタインクリメント
								bolSheetCngFlg = false;
						
								intLoopCnt = 0;
							}	//ForEnd
							
						}	//if( bolSheetCngFlg == true )

//						intSheetCnt = (int)Math.ceil( (double)intLoopCnt2 / (double)intMaxClassSr );
						intSheetCnt = intLoopCnt2 / intMaxClassSr;
						if(intLoopCnt2%intMaxClassSr!=0){
							intSheetCnt++;
						}
						workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex -  (intSheetSr - intSheetCnt)  );

						int intSheetNo = intSheetIndex -  (intSheetSr - intSheetCnt);
//						int intBIndex = (int)Math.ceil((double)(intSheetNo + 1) / (double)intMaxSheetSr );
						int intBIndex = (intSheetNo + 1) / intMaxSheetSr;
						if((intSheetNo + 1)%intMaxSheetSr!=0){
							intBIndex++;
						}
						workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
										

						
						if( intCol == intDataStartCol ){
							
							//データセット
							//自校データ・大学名ほか表示
							//自校学校名
							if( HomeDataBean.getStrGakkomei()!=null ){
								workCell = cm.setCell( workSheet, workRow, workCell, 5, 4 );
								workCell.setCellValue(HomeDataBean.getStrGakkomei());
							}
							//自校データ
							ListIterator itrHome = HomeDataBean.getC34HyoukaNinzuList().listIterator();
							int intLoopIndex = 0;
							while( itrHome.hasNext() ){
								C34HyoukaNinzuListBean HomeHyoukaNinzuListBean = (C34HyoukaNinzuListBean) itrHome.next();
								boolean ret = setData( workSheet, HomeHyoukaNinzuListBean, intSetRow + intLoopIndex , 4 );
								if( ret == false ){
									return errfdata;
								}
								intLoopIndex++;
							}
							
							if( bolDispHyoukaFlg == true ){
								//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
															intSetRow, intSetRow, 2, 2, false, true, false, false);
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( strHyouka );
				
							}
							
							if( bolDispNtiFlg == true ){
								//罫線出力(セルの上部（日程の列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
															intSetRow, intSetRow, 1, 2, false, true, false, false);
								//日程表示
								if( c34ListBean.getStrNittei()!=null ){
									workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
									workCell.setCellValue( c34ListBean.getStrNittei() );
								}
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( strHyouka );
				
							}

							if( bolDispDaigakuFlg == true ){
								//罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intSetRow, intSetRow, 0, 33, false, true, false, false );
								//大学名表示
								if( c34ListBean.getStrDaigakuMei()!=null ){
									workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 0 );
									workCell.setCellValue(c34ListBean.getStrDaigakuMei());
								}
								//日程表示
								if( c34ListBean.getStrNittei()!=null ){
									workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
									workCell.setCellValue( c34ListBean.getStrNittei() );
								}
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( strHyouka );
							}
							
						}
					
						boolean ret = setData( workSheet, c34HyoukaNinzuListBean, intSetRow + intHyoukaNinzuListIndex, intCol);
						if( ret != true){
							return errfdata;
						}
						else{
							intHyoukaNinzuListIndex++;
							intLoopCnt++;
						}
						
						//
						
					}	//WhileEnd( itr3 )
					
					
					//クラス名
					if( bolClassDispFlg == true ){
						String strGakunen = c34ClassListBean.getStrGrade();
						String strClass = c34ClassListBean.getStrClass();
						workCell = cm.setCell( workSheet, workRow, workCell, 5, intCol );
						workCell.setCellValue(cm.toString(strGakunen) + "年 " + cm.toString(strClass) + "クラス");
					}
					intLoopCnt2++;
				}	//WhileEnd( itr2 )

				intSetRow += intNendoCount;
				bolClassDispFlg = false;

				log.It("C34_02","大学名：",c34ListBean.getStrDaigakuMei());

			}	//WhileEnd( itr )
			
			for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//				int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr ); /** 20040930 **/
				int intBIndex = (intSindex + 1) / intMaxSheetSr;
				if((intSindex + 1)%intMaxSheetSr!=0){
					intBIndex++;
				}
				workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
				workSheet = (HSSFSheet)WorkSheetList.get(intSindex);
								
				//罫線出力(セルの上部に太線を引く）
				cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
													intSetRow, intSetRow, 0, 33, false, true, false, false );
				if( workSheet != null ){
					int intRemoveStartRow = intChangeSheetRow;
					if( intMaxRow + intDataStartRow > intRemoveStartRow ){
						for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
							//行削除処理
							workSheet.removeRow(workSheet.getRow(index));
						}
						//罫線出力（セルの上部に太線を出力）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
										intRemoveStartRow, intRemoveStartRow, 0, 33, false, true, false, false);
					}
				}
			
			}

			//ファイル保存
			for( int intBookSr = 0; intBookSr < WorkBookList.size(); intBookSr++ ){
				workbook = (HSSFWorkbook)WorkBookList.get(intBookSr);

				boolean bolRet = false;
				// Excelファイル保存
				if( (intBookCngCount != 1)||((intBookCngCount == 1)&&(WorkBookList.size()>1)) ){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, intMaxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}

			//データ無し
			if ( WorkBookList.size() == 0 ){
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex = 1;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, c34Item.getIntSecuFlg(), 31, 33 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
				workCell.setCellValue(secuFlg);
				
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(c34Item.getStrGakkomei()) );
		
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( c34Item.getStrMshDate() );
								
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + c34Item.getStrMshmei() + moshi);

				//表示対象
				workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
				workCell.setCellValue( "表示対象：" );
			}
			
		}
		catch( Exception e ){
			log.Err("C34_02","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("C34_02","C34_02帳票作成終了","");
		return noerror;
	}

	/**
	 * C34Item加算・編集
	 * @param c34Item
	 * @return
	 */
	private C34Item editC34( C34Item c34Item ) throws Exception{
		try{
			C34Item c34ItemEdt = new C34Item();
			String daigakuCd = "";
			String genekiKbn = "";
			boolean centerFlg = false;
			boolean secondFlg = false;
			boolean totalFlg = false;

			ArrayList c34ClassListAdd = new ArrayList();
			ArrayList c34ClassListAddCnt = new ArrayList();
			ArrayList c34ClassListAddSnd = new ArrayList();
			ArrayList c34ClassListAddTtl = new ArrayList();
			ArrayList c34NinzuListAdd = new ArrayList();

			c34ItemEdt.setStrGakkomei( c34Item.getStrGakkomei() );
			c34ItemEdt.setStrMshmei( c34Item.getStrMshmei() );
			c34ItemEdt.setStrMshDate( c34Item.getStrMshDate() );
			c34ItemEdt.setStrMshCd( c34Item.getStrMshCd() );
			c34ItemEdt.setIntSecuFlg( c34Item.getIntSecuFlg() );
			c34ItemEdt.setIntDaiTotalFlg( c34Item.getIntDaiTotalFlg() );

			C34ListBean c34ListBeanAddCnt = new C34ListBean();
			C34ListBean c34ListBeanAddSnd = new C34ListBean();
			C34ListBean c34ListBeanAddTtl = new C34ListBean();
			/** 大学・学部・学科・日程・評価 **/
			ArrayList c34List = c34Item.getC34List();
			ArrayList c34ListEdt = new ArrayList();
			ListIterator itr = c34List.listIterator();
			while( itr.hasNext() ){
				C34ListBean c34ListBean = (C34ListBean) itr.next();
				C34ListBean c34ListBeanEdt = new C34ListBean();

				// 大学コードが変わる→小計の保存
//////////////////
//				if( !cm.toString(daigakuCd).equals( cm.toString(c34ListBean.getStrDaigakuCd()) ) ){
//////////////////
				if( (!cm.toString(daigakuCd).equals( cm.toString(c34ListBean.getStrDaigakuCd())))
					||( !cm.toString(genekiKbn).equals(cm.toString(c34ListBean.getStrGenKouKbn()))) ){
					genekiKbn = c34ListBean.getStrGenKouKbn();

					daigakuCd = c34ListBean.getStrDaigakuCd();

					if(centerFlg == true){
						c34ListBeanAddCnt.setC34ClassList( c34ClassListAddCnt );
						c34ListEdt.add( c34ListBeanAddCnt );
						c34ClassListAddCnt = new ArrayList();
						centerFlg = false;
					}
					if(secondFlg == true){
						c34ListBeanAddSnd.setC34ClassList( c34ClassListAddSnd );
						c34ListEdt.add( c34ListBeanAddSnd );
						c34ClassListAddSnd = new ArrayList();
						secondFlg = false;
					}
					if(totalFlg == true){
						c34ListBeanAddTtl.setC34ClassList( c34ClassListAddTtl );
						c34ListEdt.add( c34ListBeanAddTtl );
						c34ClassListAddTtl = new ArrayList();
						totalFlg = false;
					}
					c34ListBeanAddCnt = new C34ListBean();
					c34ListBeanAddSnd = new C34ListBean();
					c34ListBeanAddTtl = new C34ListBean();
					c34NinzuListAdd = new ArrayList();
				}

				// 基本部分の移し変え
				c34ListBeanEdt.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
				c34ListBeanEdt.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
				c34ListBeanEdt.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
				c34ListBeanEdt.setStrGakubuCd( c34ListBean.getStrGakubuCd() );
				c34ListBeanEdt.setStrGakubuMei( c34ListBean.getStrGakubuMei() );
				c34ListBeanEdt.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
				c34ListBeanEdt.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
				c34ListBeanEdt.setStrNtiCd( c34ListBean.getStrNtiCd() );
				c34ListBeanEdt.setStrNittei( c34ListBean.getStrNittei() );
				c34ListBeanEdt.setStrHyouka( c34ListBean.getStrHyouka() );

				// 小計値の保存部
				switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
					case 1:
						c34ListBeanAddCnt.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanAddCnt.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanAddCnt.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanAddCnt.setStrGakubuCd( c34ListBean.getStrGakubuCd() );
						c34ListBeanAddCnt.setStrGakubuMei( c34ListBean.getStrGakubuMei() );
						c34ListBeanAddCnt.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanAddCnt.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanAddCnt.setStrNtiCd( "NTI-DGK" );
						c34ListBeanAddCnt.setStrNittei( "大学計" );
						c34ListBeanAddCnt.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListAdd = deepCopyCl( c34ClassListAddCnt );
						centerFlg = true;
						break;
					case 2:
						c34ListBeanAddSnd.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanAddSnd.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanAddSnd.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanAddSnd.setStrGakubuCd( c34ListBean.getStrGakubuCd() );
						c34ListBeanAddSnd.setStrGakubuMei( c34ListBean.getStrGakubuMei() );
						c34ListBeanAddSnd.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanAddSnd.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanAddSnd.setStrNtiCd( "NTI-DGK" );
						c34ListBeanAddSnd.setStrNittei( "大学計" );
						c34ListBeanAddSnd.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListAdd = deepCopyCl( c34ClassListAddSnd );
						secondFlg = true;
						break;
					case 3:
						c34ListBeanAddTtl.setStrGenKouKbn( c34ListBean.getStrGenKouKbn() );
						c34ListBeanAddTtl.setStrDaigakuCd( c34ListBean.getStrDaigakuCd() );
						c34ListBeanAddTtl.setStrDaigakuMei( c34ListBean.getStrDaigakuMei() );
						c34ListBeanAddTtl.setStrGakubuCd( c34ListBean.getStrGakubuCd() );
						c34ListBeanAddTtl.setStrGakubuMei( c34ListBean.getStrGakubuMei() );
						c34ListBeanAddTtl.setStrGakkaCd( c34ListBean.getStrGakkaCd() );
						c34ListBeanAddTtl.setStrGakkaMei( c34ListBean.getStrGakkaMei() );
						c34ListBeanAddTtl.setStrNtiCd( "NTI-DGK" );
						c34ListBeanAddTtl.setStrNittei( "大学計" );
						c34ListBeanAddTtl.setStrHyouka( c34ListBean.getStrHyouka() );
						c34ClassListAdd = deepCopyCl( c34ClassListAddTtl );
						totalFlg = true;
						break;
				}

				// 
				ArrayList c34ClassList = c34ListBean.getC34ClassList();
				ArrayList c34ClassListEdt = new ArrayList();
				ListIterator itrClass = c34ClassList.listIterator();
				int b = 0;
				boolean firstFlg3 = true;
				if(c34ClassListAdd.size() == 0){
					firstFlg3 = false;
				}
				/** 学校・学年・クラス **/
				while( itrClass.hasNext() ){
					C34ClassListBean c34ClassListBean = (C34ClassListBean)itrClass.next();
					C34ClassListBean c34ClassListBeanEdt = new C34ClassListBean();
					C34ClassListBean c34ClassListBeanAdd = null;

					// 移し変え
					c34ClassListBeanEdt.setStrGakkomei( c34ClassListBean.getStrGakkomei() );
					c34ClassListBeanEdt.setStrGrade( c34ClassListBean.getStrGrade() );
					c34ClassListBeanEdt.setStrClass( c34ClassListBean.getStrClass() );

					// 小計用データ呼び出し
					if(firstFlg3 == true){
						c34ClassListBeanAdd = (C34ClassListBean) c34ClassListAdd.get(b);
						c34NinzuListAdd = deepCopyNz( c34ClassListBeanAdd.getC34HyoukaNinzuList() );
					}else{
						c34ClassListBeanAdd = new C34ClassListBean();

						c34ClassListBeanAdd.setStrGakkomei( c34ClassListBean.getStrGakkomei() );
						c34ClassListBeanAdd.setStrGrade( c34ClassListBean.getStrGrade() );
						c34ClassListBeanAdd.setStrClass( c34ClassListBean.getStrClass() );
					}

					/** 年度・人数 **/
					ArrayList c34NinzuList = c34ClassListBean.getC34HyoukaNinzuList();
					ArrayList c34NinzuListEdt = new ArrayList();
					ListIterator itrNinzu = c34NinzuList.listIterator();
					int a = 0;
					boolean firstFlg2 = true;
					if(c34NinzuListAdd.size() == 0){
						firstFlg2 = false;
					}
					while( itrNinzu.hasNext() ){
						C34HyoukaNinzuListBean c34NinzuBean = (C34HyoukaNinzuListBean)itrNinzu.next();
						C34HyoukaNinzuListBean c34NinzuBeanEdt = new C34HyoukaNinzuListBean();
						C34HyoukaNinzuListBean c34NinzuBeanAdd = null;

						// 移し変え
						c34NinzuBeanEdt.setStrNendo( c34NinzuBean.getStrNendo() );
						c34NinzuBeanEdt.setIntSoshibo( c34NinzuBean.getIntSoshibo() );
						c34NinzuBeanEdt.setIntDai1shibo( c34NinzuBean.getIntDai1shibo() );
						c34NinzuBeanEdt.setIntHyoukaA( c34NinzuBean.getIntHyoukaA() );
						c34NinzuBeanEdt.setIntHyoukaB( c34NinzuBean.getIntHyoukaB() );
						c34NinzuBeanEdt.setIntHyoukaC( c34NinzuBean.getIntHyoukaC() );

						c34NinzuListEdt.add( c34NinzuBeanEdt );

						// 加算処理
						if(firstFlg2 == true){
							c34NinzuBeanAdd = (C34HyoukaNinzuListBean) c34NinzuListAdd.get(a);
						}else{
							c34NinzuBeanAdd = new C34HyoukaNinzuListBean();

							c34NinzuBeanAdd.setStrNendo( c34NinzuBean.getStrNendo() );
							c34NinzuBeanAdd.setIntSoshibo( -999 );
							c34NinzuBeanAdd.setIntDai1shibo( -999 );
							c34NinzuBeanAdd.setIntHyoukaA( -999 );
							c34NinzuBeanAdd.setIntHyoukaB( -999 );
							c34NinzuBeanAdd.setIntHyoukaC( -999 );
						}

						if(c34NinzuBean.getIntSoshibo()!=-999){
//							c34NinzuBeanAdd.setIntSoshibo( c34NinzuBeanAdd.getIntSoshibo() + c34NinzuBean.getIntSoshibo() );
							if(c34NinzuBeanAdd.getIntSoshibo()!=-999){
								c34NinzuBeanAdd.setIntSoshibo(c34NinzuBeanAdd.getIntSoshibo() + c34NinzuBean.getIntSoshibo());
							}else{
								c34NinzuBeanAdd.setIntSoshibo(c34NinzuBean.getIntSoshibo());
							}
						}
						if(c34NinzuBean.getIntDai1shibo()!=-999){
//							c34NinzuBeanAdd.setIntDai1shibo( c34NinzuBeanAdd.getIntDai1shibo() + c34NinzuBean.getIntDai1shibo() );
							if(c34NinzuBeanAdd.getIntDai1shibo()!=-999){
								c34NinzuBeanAdd.setIntDai1shibo(c34NinzuBeanAdd.getIntDai1shibo() + c34NinzuBean.getIntDai1shibo());
							}else{
								c34NinzuBeanAdd.setIntDai1shibo(c34NinzuBean.getIntDai1shibo());
							}
						}
						if(c34NinzuBean.getIntHyoukaA()!=-999){
//							c34NinzuBeanAdd.setIntHyoukaA( c34NinzuBeanAdd.getIntHyoukaA() + c34NinzuBean.getIntHyoukaA() );
							if(c34NinzuBeanAdd.getIntHyoukaA()!=-999){
								c34NinzuBeanAdd.setIntHyoukaA(c34NinzuBeanAdd.getIntHyoukaA() + c34NinzuBean.getIntHyoukaA());
							}else{
								c34NinzuBeanAdd.setIntHyoukaA(c34NinzuBean.getIntHyoukaA());
							}
						}
						if(c34NinzuBean.getIntHyoukaB()!=-999){
//							c34NinzuBeanAdd.setIntHyoukaB( c34NinzuBeanAdd.getIntHyoukaB() + c34NinzuBean.getIntHyoukaB() );
							if(c34NinzuBeanAdd.getIntHyoukaB()!=-999){
								c34NinzuBeanAdd.setIntHyoukaB(c34NinzuBeanAdd.getIntHyoukaB() + c34NinzuBean.getIntHyoukaB());
							}else{
								c34NinzuBeanAdd.setIntHyoukaB(c34NinzuBean.getIntHyoukaB());
							}
						}
						if(c34NinzuBean.getIntHyoukaC()!=-999){
//							c34NinzuBeanAdd.setIntHyoukaC( c34NinzuBeanAdd.getIntHyoukaC() + c34NinzuBean.getIntHyoukaC() );
							if(c34NinzuBeanAdd.getIntHyoukaC()!=-999){
								c34NinzuBeanAdd.setIntHyoukaC(c34NinzuBeanAdd.getIntHyoukaC() + c34NinzuBean.getIntHyoukaC());
							}else{
								c34NinzuBeanAdd.setIntHyoukaC(c34NinzuBean.getIntHyoukaC());
							}
						}

						if(firstFlg2 == true){
							c34NinzuListAdd.set(a,c34NinzuBeanAdd);
						}else{
							c34NinzuListAdd.add(c34NinzuBeanAdd);
						}
						a++;
					}

					// 格納
					c34ClassListBeanEdt.setC34HyoukaNinzuList( c34NinzuListEdt );
					c34ClassListEdt.add( c34ClassListBeanEdt );

					// 計算結果格納
					c34ClassListBeanAdd.setC34HyoukaNinzuList( deepCopyNz(c34NinzuListAdd) );
					c34NinzuListAdd = new ArrayList();
					if(firstFlg3 == true){
						c34ClassListAdd.set(b,c34ClassListBeanAdd);
					}else{
						c34ClassListAdd.add(c34ClassListBeanAdd);
					}
					b++;
				}
				// 格納
				c34ListBeanEdt.setC34ClassList( c34ClassListEdt );
				c34ListEdt.add( c34ListBeanEdt );
				switch( Integer.parseInt(c34ListBean.getStrHyouka()) ){
					case 1:
						c34ClassListAddCnt = deepCopyCl( c34ClassListAdd );
						break;
					case 2:
						c34ClassListAddSnd = deepCopyCl( c34ClassListAdd );
						break;
					case 3:
						c34ClassListAddTtl = deepCopyCl( c34ClassListAdd );
						break;
				}
				c34ClassListAdd = new ArrayList();
			}
			// 小計の保存
			if(centerFlg == true){
				c34ListBeanAddCnt.setC34ClassList( c34ClassListAddCnt );
				c34ListEdt.add( c34ListBeanAddCnt );
				c34ClassListAddCnt = new ArrayList();
				centerFlg = false;
			}
			if(secondFlg == true){
				c34ListBeanAddSnd.setC34ClassList( c34ClassListAddSnd );
				c34ListEdt.add( c34ListBeanAddSnd );
				c34ClassListAddSnd = new ArrayList();
				secondFlg = false;
			}
			if(totalFlg == true){
				c34ListBeanAddTtl.setC34ClassList( c34ClassListAddTtl );
				c34ListEdt.add( c34ListBeanAddTtl );
				c34ClassListAddTtl = new ArrayList();
				totalFlg = false;
			}

			// 格納
			c34ItemEdt.setC34List( c34ListEdt );

			return c34ItemEdt;
		}catch(Exception e){
			throw e;
		}
	}

	/**
	 * 
	 * @param c34NinzuList
	 * @return
	 */
	private ArrayList deepCopyNz(ArrayList c34NinzuList){
		ListIterator itrNinzu = c34NinzuList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrNinzu.hasNext()){
			C34HyoukaNinzuListBean c34NinzuBean = (C34HyoukaNinzuListBean)itrNinzu.next();
			C34HyoukaNinzuListBean c34NinzuBeanCopy = new C34HyoukaNinzuListBean();

			c34NinzuBeanCopy.setStrNendo( c34NinzuBean.getStrNendo() );
			c34NinzuBeanCopy.setIntSoshibo( c34NinzuBean.getIntSoshibo() );
			c34NinzuBeanCopy.setIntDai1shibo( c34NinzuBean.getIntDai1shibo() );
			c34NinzuBeanCopy.setIntHyoukaA( c34NinzuBean.getIntHyoukaA() );
			c34NinzuBeanCopy.setIntHyoukaB( c34NinzuBean.getIntHyoukaB() );
			c34NinzuBeanCopy.setIntHyoukaC( c34NinzuBean.getIntHyoukaC() );

			copyList.add( c34NinzuBeanCopy );
		}
		
		return copyList;
	}

	/**
	 * 
	 * @param c34ClassList
	 * @return
	 */
	private ArrayList deepCopyCl(ArrayList c34ClassList){
		ListIterator itrClass = c34ClassList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrClass.hasNext()){
			C34ClassListBean c34ClassBean = (C34ClassListBean)itrClass.next();
			C34ClassListBean c34ClassBeanCopy = new C34ClassListBean();

			c34ClassBeanCopy.setStrClass( c34ClassBean.getStrClass() );
			c34ClassBeanCopy.setStrGakkomei( c34ClassBean.getStrGakkomei() );
			c34ClassBeanCopy.setStrGrade( c34ClassBean.getStrGrade() );
			c34ClassBeanCopy.setC34HyoukaNinzuList( deepCopyNz(c34ClassBean.getC34HyoukaNinzuList()) );

			copyList.add( c34ClassBeanCopy );
		}
		
		return copyList;
	}
	
	/*
	 * 	Excel編集
	 * 		HSSFWorkbook 	workbook: Excelワークブック
	 * 		ArrayList 		c34HyoukaNinzuList:評価別人数リスト
	 * 		int				intRow:開始行
	 * 		int				intCol:開始列
	 * 		戻り値: なし
	 */
	private boolean setData(HSSFSheet workSheet, C34HyoukaNinzuListBean c34HyoukaNinzuListBean, int intRow, int intCol ){

		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		int		row = intRow;
		
		
		// 基本ファイルを読込む
		try{
			
			//模試年度
			if( c34HyoukaNinzuListBean.getStrNendo()!=null ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, 3);
				workCell.setCellValue( c34HyoukaNinzuListBean.getStrNendo() + "年度" );
			}
			
			//全国総志望者数
			if( c34HyoukaNinzuListBean.getIntSoshibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol);
				workCell.setCellValue( c34HyoukaNinzuListBean.getIntSoshibo() );
			}
			
			//志望者数（第一志望）
			if( c34HyoukaNinzuListBean.getIntDai1shibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 1);
				workCell.setCellValue( c34HyoukaNinzuListBean.getIntDai1shibo() );
			}
			
			//評価別人数
			if( c34HyoukaNinzuListBean.getIntHyoukaA() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 2 );
				workCell.setCellValue( c34HyoukaNinzuListBean.getIntHyoukaA() );
			}

			if( c34HyoukaNinzuListBean.getIntHyoukaB() != -999 ){			
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
				workCell.setCellValue( c34HyoukaNinzuListBean.getIntHyoukaB() );
			}
			
			if( c34HyoukaNinzuListBean.getIntHyoukaC() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
				workCell.setCellValue( c34HyoukaNinzuListBean.getIntHyoukaC() );
			}

		}
		catch(Exception e){
			return false;
		}

		return true;
	}
	
}
/*
 * 作成日: 2004/06/29
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.com_set.cm;

import java.io.Serializable;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class UnivData implements Serializable, Comparable {

	// 大学コード
	private String univCode = null;
	// 大学名
	private String univName = null;
	// 志望者数
	private int choiceNum = 0;
	// 国私区分
	private String univDiv = null;
	// 所在地
	private String location = null;
	// 登録済みかどうか
	private boolean registed = false;

	// 2016/01/18 QQ)Nishiyama 大規模改修 ADD START
	// 大学名カナ
	private String univNameKana = null;
	// 2016/01/18 QQ)Nishiyama 大規模改修 ADD END

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return ((UnivData)obj).getUnivCode().equals(univCode);
	}

	/* (非 Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		String univDiv = ((UnivData)o).getUnivDiv();
		if (Integer.parseInt(this.univDiv) > Integer.parseInt(univDiv)) return 1;
		else if (Integer.parseInt(this.univDiv) < Integer.parseInt(univDiv)) return -1;
		else return 0;
	}

	/**
	 *
	 */
	public UnivData() {
		super();
	}

	/**
	 *
	 */
	public UnivData(String univCode) {
		super();
		this.univCode = univCode;
	}

	/**
	 * @return
	 */
	public int getChoiceNum() {
		return choiceNum;
	}

	/**
	 * @return
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return
	 */
	public String getUnivCode() {
		return univCode;
	}

	/**
	 * @return
	 */
	public String getUnivDiv() {
		return univDiv;
	}

	/**
	 * @return
	 */
	public String getUnivName() {
		return univName;
	}

	/**
	 * @param i
	 */
	public void setChoiceNum(int i) {
		choiceNum = i;
	}

	/**
	 * @param string
	 */
	public void setLocation(String string) {
		location = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCode(String string) {
		univCode = string;
	}

	/**
	 * @param string
	 */
	public void setUnivDiv(String string) {
		univDiv = string;
	}

	/**
	 * @param string
	 */
	public void setUnivName(String string) {
		univName = string;
	}

	/**
	 * @return
	 */
	public boolean isRegisted() {
		return registed;
	}

	/**
	 * @param b
	 */
	public void setRegisted(boolean b) {
		registed = b;
	}


	// 2016/01/18 QQ)Nishiyama 大規模改修 ADD START
	/**
	 * @return
	 */
	public String getUnivNameKana() {
		return univNameKana;
	}

	/**
	 * @param string
	 */
	public void setUnivNameKana(String str) {
		univNameKana = str;
	}
	// 2016/01/18 QQ)Nishiyama 大規模改修 ADD END

}

/*
 * 作成日: 2004/08/20
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class EntExamModeData {
	
	// 入試形態コード
	// 01:一般 02:推薦 03:AO 04:特奨・給費生 05:その他
	private String entExamModeCd;	
	//入試形態名
	private String entExamModeName;
	
	/**
	 * @return
	 */
	public String getEntExamModeCd() {
		return entExamModeCd;
	}

	/**
	 * @return
	 */
	public String getEntExamModeName() {
		return entExamModeName;
	}

	/**
	 * @param string
	 */
	public void setEntExamModeCd(String string) {
		entExamModeCd = string;
	}

	/**
	 * @param string
	 */
	public void setEntExamModeName(String string) {
		entExamModeName = string;
	}

}

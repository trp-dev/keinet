package jp.co.fj.keinavi.servlets.maintenance;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.maintenance.ScoreExamListBean;
import jp.co.fj.keinavi.forms.maintenance.M109Form;

/**
 *
 * 情報不備生徒候補画面サーブレット
 * 
 * 2006.10.04	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M109Servlet extends AbstractMServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// アクションフォーム
		final M109Form form = (M109Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.maintenance.M109Form");

		// JSPへの遷移処理
		if ("m109".equals(getForward(request))) {
			request.setAttribute("form", form);
			forward2Jsp(request, response);
			
		// 不明なら転送
		} else {
			//「生徒表示」ボタンを押しているならフィルタリング
			if ("1".equals(form.getActionMode())) {
				final ScoreExamListBean	 bean = getScoreExamListBean(request);
				// 表示対象模試をセット
				bean.setTargetExam(form.getTargetExamId());
				// 表示対象をセット
				bean.setExaminee("1".equals(form.getExaminee()));
				bean.setTemporary("1".equals(form.getTemporary()));
				bean.setNotTake("1".equals(form.getNotTake()));
				// 生徒表示リスト初期化
				getStudentListBean(request).initDispStudentList(
						bean, getTempExamineeData(request),
						getStudentListComparator(form.getSortKey()));
			}
			dispatch(request, response);
		}
	}

}

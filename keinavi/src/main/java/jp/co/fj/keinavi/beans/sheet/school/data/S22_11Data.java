package jp.co.fj.keinavi.beans.sheet.school.data;

/**
 *
 * S22_11データクラス
 * 
 * 2007.07.26	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class S22_11Data extends S42_15Data {

	/**
	 * @return 科目リストデータのインスタンス
	 */
	public S42_15SubjectListData createSubjectListData() {
		return new S22_11SubjectListData();
	}

	/**
	 * @return センター到達指標リストデータのインスタンス
	 */
	public S42_15ReachLevelListData createReachLevelListData() {
		return new S22_11ReachLevelListData();
	}

	/**
	 * @return 過年度リストデータのインスタンス
	 */
	public S22_11YearListData createYearListData() {
		return new S22_11YearListData();
	}
	
	/**
	 * @return スコア帯リストデータのインスタンス
	 */
	public S42_15ScoreZoneListData createScoreZoneListData() {
		return new S22_11ScoreZoneListData();
	}
	
}

package jp.co.fj.keinavi.servlets.maintenance;

import java.sql.Connection;
import java.util.Comparator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.maintenance.DeclarationBean;
import jp.co.fj.keinavi.beans.maintenance.DeclarationUpdateBean;
import jp.co.fj.keinavi.beans.maintenance.KNClassListBean;
import jp.co.fj.keinavi.beans.maintenance.ScoreExamListBean;
import jp.co.fj.keinavi.beans.maintenance.SelectedStudentBean;
import jp.co.fj.keinavi.beans.maintenance.StudentListBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.maintenance.KNStudentExtData;
import jp.co.fj.keinavi.data.maintenance.TempExamineeData;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

import org.apache.commons.collections.ComparatorUtils;

/**
 *
 * メンテナンス画面基本サーブレット
 * 
 * 2006.10.26	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public abstract class AbstractMServlet extends DefaultHttpServlet {

	// 作業中宣言をロードする
	protected void loadDeclaration(
			final HttpServletRequest request) throws ServletException {
		final DeclarationBean bean = new DeclarationBean(getSchoolCd(request));
		execute(request, bean);
		request.getSession(false).setAttribute(DeclarationBean.SESSION_KEY, bean);
	}
	
	// 作業中宣言を上書きする
	protected void updateDeclaration(
			final HttpServletRequest request) throws ServletException {
		
		// 作業中宣言
		final DeclarationBean dec = (DeclarationBean) request.getSession(
				false).getAttribute(DeclarationBean.SESSION_KEY);
		
		// 作業中宣言がされていないならここまで
		if (dec.getTime() == null) {
			return;
		}
		
		// ログイン情報
		final LoginSession login = getLoginSession(request);
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);
			final DeclarationUpdateBean bean = new DeclarationUpdateBean(
					login.getUserID());
			bean.setConnection(null, con);
			bean.setLoginId(login.getAccount());
			bean.execute();
			con.commit();
		} catch (final Exception e) {
			rollback(con);
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
	}
	
	// 生徒情報セッションを初期化する
	protected void initStudentSession(
			final HttpServletRequest request) {
		final HttpSession session = request.getSession(false);
		// クラスと生徒一覧のセッションクリア
		session.removeAttribute("ClassListBean");
		session.removeAttribute("StudentSearchBean");
		// 選択済み生徒は同期が取れていない
		getSelectedStudentBean(request).setSynchronized(false);
	}
	
	// 校内成績が存在する模試リストを作る
	protected ScoreExamListBean createScoreExamListBean(
			final HttpServletRequest request) throws ServletException {

		final LoginSession login = getLoginSession(request);
		final ScoreExamListBean bean = new ScoreExamListBean(
				login.getUserID(), login.isHelpDesk());
		execute(request, bean);
		return bean;
	}

	// 生徒リストComparatorを取得する
	protected Comparator getStudentListComparator(final String sortKey) {
		
		final int key = Integer.parseInt(sortKey);
		final Comparator c;
		
		switch (key % 10) {
			// 履歴情報（前々年度）
			case 1:
				c = KNStudentExtData.getBaseComparator(2);
				break;
			// 履歴情報（前年度）
			case 2:
				c = KNStudentExtData.getBaseComparator(1);
				break;
			// 履歴情報（今年度）
			case 3:
				c = KNStudentExtData.getBaseComparator(0);
				break;
			// かな氏名
			case 4:
				c = KNStudentExtData.getNameKanaComparator();
				break;
			// 漢字氏名
			case 5:
				c = KNStudentExtData.getNameKanjiComparator();
				break;
			// 生年月日
			case 6:
				c = KNStudentExtData.getBirthdayComparator();
				break;
			// 電話番号
			case 7:
				c = KNStudentExtData.getTelNoComparator();
				break;
			// 未定義
			default:
				throw new IllegalArgumentException(
						"ソートキーが不正です。" + sortKey);	
		}
		
		// キーが10以上なら降順
		if (key > 10) {
			return ComparatorUtils.reversedComparator(c);
		// そうでなければ昇順
		} else {
			return c;
		}
	}
	
	// 選択済み生徒情報をDBと同期する
	protected void syncSelectedStudent(
			final HttpServletRequest request) throws ServletException {
		
		// 選択済み生徒情報
		final SelectedStudentBean bean = getSelectedStudentBean(request);
		
		// 同期が取れているならここまで
		if (bean.isSynchronized()) {
			return;
		// DBから情報取得
		} else {
			// 実行
			execute(request, bean);
		}
	}
	
	// 個人成績模試リストの取得
	protected ScoreExamListBean getScoreExamListBean(
			final HttpServletRequest request) {
		final HttpSession session = request.getSession(false);
		return (ScoreExamListBean) session.getAttribute("ScoreExamListBean");
	}

	// 個人成績模試リストの取得
	protected KNClassListBean getClassListBean(
			final HttpServletRequest request) {
		final HttpSession session = request.getSession(false);
		return (KNClassListBean) session.getAttribute("ClassListBean");
	}

	// 生徒情報リストの取得
	protected StudentListBean getStudentListBean(
			final HttpServletRequest request) {
		final HttpSession session = request.getSession(false);
		return (StudentListBean) session.getAttribute("StudentSearchBean");
	}

	// 仮受験者情報の取得
	protected TempExamineeData getTempExamineeData(
			final HttpServletRequest request) {
		final HttpSession session = request.getSession(false);
		return (TempExamineeData) session.getAttribute("TempExamineeData");
	}

	// 選択済み生徒情報の取得
	protected SelectedStudentBean getSelectedStudentBean(
			final HttpServletRequest request) {
		final HttpSession session = request.getSession(false);
		return (SelectedStudentBean) session.getAttribute("SelectedStudentBean");
	}
	
	// 主生徒データの取得
	protected KNStudentExtData getMainStudentData(
			final HttpServletRequest request) {
		return getSelectedStudentBean(request).getMainStudentData();
	}

	// 従生徒リストの取得
	protected KNStudentExtData getSubStudentData(
			final HttpServletRequest request) {
		return getSelectedStudentBean(request).getSubStudentData();
	}

}

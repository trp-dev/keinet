package jp.co.fj.keinavi.util.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;

import com.fjh.forms.ActionForm;

/**
 * 
 * エラーメッセージを表示するTaglibです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class ErrorMessageTag extends TagSupport {

	/** serialVersionUID */
	private static final long serialVersionUID = -8670636328008321171L;

	/** アクションフォーム名 */
	private String form;

	/** フィールド名 */
	private String field;

	/** セパレータ */
	private String separator;

	/**
	 * @see javax.servlet.jsp.tagext.TagSupport#doEndTag()
	 */
	public int doEndTag() throws JspException {

		ActionForm actionForm = (ActionForm) pageContext.getRequest()
				.getAttribute(form);

		try {
			for (int i = 0; i < actionForm.getErrorCount(); i++) {
				if (StringUtils.isEmpty(field)
						|| field.equals(actionForm.getErrorField(i))) {
					pageContext.getOut().print(actionForm.getErrorMessage(i));
					pageContext.getOut().print(
							StringUtils.defaultString(separator, "\\n"));
				}
			}
		} catch (Exception e) {
			throw new JspException(e);
		}

		return EVAL_PAGE;
	}

	/**
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	public int doStartTag() throws JspException {
		return SKIP_BODY;
	}

	/**
	 * アクションフォーム名をセットします。
	 * 
	 * @param form
	 *            アクションフォーム名
	 */
	public void setForm(String form) {
		this.form = form;
	}

	/**
	 * フィールド名をセットします。
	 * 
	 * @param field
	 *            フィールド名
	 */
	public void setField(String field) {
		this.field = field;
	}

	/**
	 * セパレータをセットします。
	 * 
	 * @param separator
	 *            セパレータ
	 */
	public void setSeparator(String separator) {
		this.separator = separator;
	}

}

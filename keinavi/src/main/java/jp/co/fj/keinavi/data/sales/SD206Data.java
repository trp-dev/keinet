package jp.co.fj.keinavi.data.sales;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * 特例成績データ　申請一覧確認（パスワード通知書ダウンロード）画面のリストデータです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD206Data {

	/** 申請ID */
	private String appId;

	/** 対象年度 */
	private String examYear;

	/** 対象模試 */
	private String examName;

	/** 対象営業部リスト */
	private final List sectorNameList = new ArrayList();

	/** 学校コード */
	private String schoolCd;

	/** 学校名 */
	private String schoolName;

	/** 契約区分 */
	private String pactDivName;

	/** 担当先生名 */
	private String teacherName;

	/** 申請理由 */
	private String appComment;

	/** 申請日時 */
	private Timestamp appDate;

	/** 最終ダウンロード日時 */
	private Timestamp downloadDate;

	/** 状態コード */
	private String statusCd;

	/** 状態名称 */
	private String statusName;

	/** データ開放日 */
	private Date openDate;

	/**
	 * 対象年度を返します。
	 * 
	 * @return 対象年度
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * 対象年度をセットします。
	 * 
	 * @param examYear
	 *            対象年度
	 */
	public void setExamYear(String examYear) {
		this.examYear = examYear;
	}

	/**
	 * 対象模試を返します。
	 * 
	 * @return 対象模試
	 */
	public String getExamName() {
		return examName;
	}

	/**
	 * 対象模試をセットします。
	 * 
	 * @param examName
	 *            対象模試
	 */
	public void setExamName(String examName) {
		this.examName = examName;
	}

	/**
	 * 学校コードを返します。
	 * 
	 * @return 学校コード
	 */
	public String getSchoolCd() {
		return schoolCd;
	}

	/**
	 * 学校コードをセットします。
	 * 
	 * @param schoolCd
	 *            学校コード
	 */
	public void setSchoolCd(String schoolCd) {
		this.schoolCd = schoolCd;
	}

	/**
	 * 学校名を返します。
	 * 
	 * @return 学校名
	 */
	public String getSchoolName() {
		return schoolName;
	}

	/**
	 * 学校名をセットします。
	 * 
	 * @param schoolName
	 *            学校名
	 */
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	/**
	 * 契約区分を返します。
	 * 
	 * @return 契約区分
	 */
	public String getPactDivName() {
		return pactDivName;
	}

	/**
	 * 契約区分をセットします。
	 * 
	 * @param pactDivName
	 *            契約区分
	 */
	public void setPactDivName(String pactDivName) {
		this.pactDivName = pactDivName;
	}

	/**
	 * 担当先生名を返します。
	 * 
	 * @return 担当先生名
	 */
	public String getTeacherName() {
		return teacherName;
	}

	/**
	 * 担当先生名をセットします。
	 * 
	 * @param teacherName
	 *            担当先生名
	 */
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	/**
	 * 申請理由を返します。
	 * 
	 * @return 申請理由
	 */
	public String getAppComment() {
		return appComment;
	}

	/**
	 * 申請理由をセットします。
	 * 
	 * @param appComment
	 *            申請理由
	 */
	public void setAppComment(String appComment) {
		this.appComment = appComment;
	}

	/**
	 * 申請日時を返します。
	 * 
	 * @return 申請日時
	 */
	public Timestamp getAppDate() {
		return appDate;
	}

	/**
	 * 申請日時をセットします。
	 * 
	 * @param appDate
	 *            申請日時
	 */
	public void setAppDate(Timestamp appDate) {
		this.appDate = appDate;
	}

	/**
	 * 申請IDを返します。
	 * 
	 * @return 申請ID
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * 申請IDをセットします。
	 * 
	 * @param appId
	 *            申請ID
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * 最終ダウンロード日時を返します。
	 * 
	 * @return 最終ダウンロード日時
	 */
	public Timestamp getDownloadDate() {
		return downloadDate;
	}

	/**
	 * 最終ダウンロード日時をセットします。
	 * 
	 * @param downloadDate
	 *            最終ダウンロード日時
	 */
	public void setDownloadDate(Timestamp downloadDate) {
		this.downloadDate = downloadDate;
	}

	/**
	 * 状態コードを返します。
	 * 
	 * @return 状態コード
	 */
	public String getStatusCd() {
		return statusCd;
	}

	/**
	 * 状態コードをセットします。
	 * 
	 * @param statusCd
	 *            状態コード
	 */
	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	/**
	 * 状態名称を返します。
	 * 
	 * @return 状態名称
	 */
	public String getStatusName() {
		return statusName;
	}

	/**
	 * 状態名称をセットします。
	 * 
	 * @param statusName
	 *            状態名称
	 */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	/**
	 * データ開放日を返します。
	 * 
	 * @return データ開放日
	 */
	public Date getOpenDate() {
		return openDate;
	}

	/**
	 * データ開放日をセットします。
	 * 
	 * @param openDate
	 *            データ開放日
	 */
	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	/**
	 * 対象営業部リストを返します。
	 * 
	 * @return 対象営業部リスト
	 */
	public List getSectorNameList() {
		return sectorNameList;
	}

}

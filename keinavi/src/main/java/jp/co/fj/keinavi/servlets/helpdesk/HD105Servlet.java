package jp.co.fj.keinavi.servlets.helpdesk;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.forms.helpdesk.HD105Form;
import jp.co.fj.keinavi.beans.helpdesk.UserDetailBean;
import jp.co.fj.keinavi.beans.user.AdminLoginIdInitializerBean;
import jp.co.fj.keinavi.beans.user.AdminPasswordInitializerBean;

/**
 * ユーザ管理（詳細参照）画面サーブレット
 * 
 * 2004.09.27	NINOMIYA - TOTEC
 * 				新規作成
 *
 * <2010年度マーク高２模試対応>
 * 2011.02.28	Tomohisa YAMADA - TOTEC
 * 				ヘルプデスクでid・pw初期化
 * 
 * @author NINOMIYA - TOTEC
 * @version 1.0
 */
public class HD105Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
	
		// アクションフォーム
		HD105Form hd105Form = null;

		try {
			hd105Form = (HD105Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.helpdesk.HD105Form");
		} catch (Exception e) {
			throw new ServletException(e);
		}
			
		if(("hd105").equals(getForward(request)) || getForward(request) == null) {
	
			Connection con = null;
			try {
				con = super.getConnectionPool(request);
				con.setAutoCommit(false);
				
				//更新処理
				if ("hd105".equals(getBackward(request))) {
					
					//1:利用者IDを初期化
					if ("1".equals(hd105Form.getActionMode())) {
						initializeLoginId(con, hd105Form);
					}
					//2:ログインパスワードを初期化
					else if ("2".equals(hd105Form.getActionMode())) {
						initializeLoginPwd(con, hd105Form);
					}
				}
				
				//ユーザ情報表示
				request.setAttribute("userDetailBean", 
						searchUserDetail(con, hd105Form));
				
				con.commit();
	
			} catch (Exception e) {
				rollback(con);
				throw new ServletException(e.toString());
			} finally {
				super.releaseConnectionPool(request, con);
			}
			
	
			// アクションフォーム
			request.setAttribute("hd105Form", hd105Form);
	
			super.forward(request, response, JSP_HD105);
		
		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}
	
	/**
	 * ユーザ詳細情報を検索する。
	 * @param con DBコネクション
	 * @param form フォーム
	 * @return ユーザ詳細情報
	 * @throws Exception
	 */
	private UserDetailBean searchUserDetail(Connection con, HD105Form form) throws Exception {
		UserDetailBean bean = new UserDetailBean();
		bean.setConnection(null, con);
		bean.setHD105Form(form);
		bean.execute();
		return bean;
	}
	
	/**
	 * ユーザ利用者ＩＤを初期化する。
	 * @param con DBコネクション
	 * @param form フォーム
	 * @throws Exception
	 */
	private void initializeLoginId(Connection con, HD105Form form) throws  Exception {
		AdminLoginIdInitializerBean lib = new AdminLoginIdInitializerBean(form.getUserId());
		lib.setConnection(null, con);
		lib.execute();
	}
	
	/**
	 * ログインパスワードを初期化する。
	 * @param con DBコネクション
	 * @param form フォーム
	 * @throws Exception
	 */
	private void initializeLoginPwd(Connection con, HD105Form form) throws  Exception {
		AdminPasswordInitializerBean pib = new AdminPasswordInitializerBean(form.getUserId());
		pib.setConnection(null, con);
		pib.execute();
	}
		
}

package jp.co.fj.keinavi.excel.data.business;

/**
 * Zà¬ÑªÍ|ZÔär|Î·lªz pêCEFR±Êæ¾óµ
 * ì¬ú: 2019/09/30
 * @author QQ)
 *
 */
public class B42CefrListBean {

        //wZ¼âS/§¼ÈÇÌ¼Ì
        private String strName = "";

        //l
        private int[] intNumbersList  = {0, 0, 0, 0, 0, 0, 0, 0};

        //\¬ä
        private float[] floCompRatioList = {0, 0, 0, 0, 0, 0, 0, 0};


        /*----------*/
        /* Get      */
        /*----------*/
        public String getStrName() {
            return strName;
        }

        public int[] getIntNumbersList() {
            return intNumbersList;
        }

        public int getIntNumbers(int idx) {
            return intNumbersList[idx];
        }

        public float[] getFloCompRatioList() {
            return floCompRatioList;
        }

        public float getFloCompRatio(int idx) {
            return floCompRatioList[idx];
        }


        /*--------------*/
        /* Set          */
        /*--------------*/
        public void setStrName(String strName) {
            this.strName = strName;
        }

        public void setIntNumbersList(int[] intNumbersList) {
            this.intNumbersList = intNumbersList;
        }

        public void setIntNumbers(int idx, int intNumbers) {
            this.intNumbersList[idx] = intNumbers;
        }

        public void setFloCompRatioList(float[] floCompRatioList) {
            this.floCompRatioList = floCompRatioList;
        }

        public void setFloCompRatio(int idx, float floCompRatio) {
            this.floCompRatioList[idx] = floCompRatio;
        }

}

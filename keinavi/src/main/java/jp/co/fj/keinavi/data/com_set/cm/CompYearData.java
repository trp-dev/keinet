/*
 * 作成日: 2004/06/29
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.com_set.cm;

import java.io.Serializable;

/**
 * 比較対象年度のデータを保持するクラス
 * 
 * @author kawai
 */
public class CompYearData implements Serializable {
	
	private final String year;
	
	private int examinees = 0; // 受験者数

	/**
	 * コンストラクタ
	 */
	public CompYearData(final String pYear) {
		this.year = pYear;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(final Object o) {
		return getYear().equals(((CompYearData) o).getYear());
	}

	/**
	 * @return year を戻します。
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @return
	 */
	public int getExaminees() {
		return examinees;
	}
	
	/**
	 * @param i
	 */
	public void setExaminees(int i) {
		examinees = i;
	}
	
}

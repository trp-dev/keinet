package jp.co.fj.keinavi.beans.maintenance.module;

import java.io.IOException;
import java.util.Calendar;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.data.maintenance.KNStudentData;
import jp.co.fj.keinavi.util.DataValidator;
import jp.co.fj.keinavi.util.message.MessageLoader;

/**
 *
 * 生徒情報データの妥当性チェックを行うクラス
 * 
 * 2005.10.25	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public final class StudentDataValidator {
	
	// Singleton
	private static final StudentDataValidator INSTANCE =
			new StudentDataValidator();
	// 今の年度
	private static final int CURRENT_YEAR = Calendar.getInstance().get(Calendar.YEAR);
	// 汎用検査クラス
	private final DataValidator validator = DataValidator.getInstance();
	
	// コンストラクタ
	private StudentDataValidator() {
	}
	
	/**
	 * StudentDataNormalizerのインスタンスを取得する
	 * 
	 * @return StudentDataNormalizerのインスタンス
	 */
	public static StudentDataValidator getInstance() {
		return INSTANCE;
	}
	
	/**
	 * 生徒情報データの検査を行う
	 * 
	 * @param data 生徒情報データ
	 * @throws IOException 
	 */
	public void validate(final KNStudentData data) throws KNBeanException, IOException {
		
		final MessageLoader message = MessageLoader.getInstance();
		
		// 今年度
		if (data.getGrade() > 0) {
			if (!isValidGrade(String.valueOf(data.getGrade()))) {
				throw new KNBeanException(
						"学年が不正です。" + data.getGrade());
			}
			if (!isValidClassName(data.getClassName())) {
				throw new KNBeanException(
						"クラス名が不正です。" + data.getClassName());
			}
			if (!isValidClassNo(data.getClassNo())) {
				throw new KNBeanException(
						"クラス番号が不正です。" + data.getClassNo());
			}
		}
		
		// 前年度
		if (data.getLastGrade() > 0) {
			if (!isValidGrade(String.valueOf(data.getLastGrade()))) {
				throw new KNBeanException(
						"前年度の学年が不正です。" + data.getLastGrade());
			}
			if (!isValidClassName(data.getLastClassName())) {
				throw new KNBeanException(
						"前年度のクラス名が不正です。" + data.getLastClassName());
			}
			if (!isValidClassNo(data.getLastClassNo())) {
				throw new KNBeanException(
						"前年度のクラス番号が不正です。" + data.getLastClassNo());
			}
		}
		
		// 前々年度
		if (data.getBeforeLastGrade() > 0) {
			if (!isValidGrade(String.valueOf(data.getBeforeLastGrade()))) {
				throw new KNBeanException(
						"前々年度の学年が不正です。" + data.getBeforeLastGrade());
			}
			if (!isValidClassName(data.getBeforeLastClassName())) {
				throw new KNBeanException(
						"前々年度のクラス名が不正です。" + data.getBeforeLastClassName());
			}
			if (!isValidClassNo(data.getBeforeLastClassNo())) {
				throw new KNBeanException(
						"前々年度のクラス番号が不正です。" + data.getBeforeLastClassNo());
			}
		}
		
		// 性別
		if (!isValidSex(data.getSex())) {
			throw new KNBeanException("正しい性別を入力してください。");
		}
		
		// かな氏名
		if (!isValidKanaName(data.getNameKana())) {
			throw new KNBeanException(message.getMessage("x006a"));
		}
		
		// 漢字氏名
		if (!isValidKanjiName(data.getNameKanji())) {
			throw new KNBeanException(message.getMessage("x007a"));
		}
		
		// 電話番号
		if (!isValidTelNo(data.getTelNo())) {
			throw new KNBeanException(message.getMessage("x009a"));
		}
	}
	
	/**
	 * 年度の妥当性をチェックする
	 * 
	 * @param year 年度
	 * @param current 現年度
	 * @return チェック結果
	 */
	public boolean isValidYear(final String year, final int current) {
		
		return validator.isValidRange(year, current - 2, current);
	}
	
	/**
	 * 個人IDの妥当性をチェックする
	 * 
	 * @param individualId 個人ID
	 * @return チェック結果
	 */
	public boolean isValidIndividualId(final String individualId) {
		
		return individualId.length() == 0
				|| validator.isValidRange(individualId, 99999999L, 9999999999L);
	}
	
	/**
	 * 学年の妥当性をチェックする
	 * 
	 * @param grade 学年
	 * @return チェック結果
	 */
	public boolean isValidGrade(final String grade) {
		
		return validator.isValidRange(grade, 1, 5);
	}
	
	/**
	 * クラス名の妥当性をチェックする
	 * 
	 * @param className クラス名
	 * @return チェック結果
	 */
	public boolean isValidClassName(final String className) {
		
		return validator.isValidByteLength(className, 1, 2)
			&& validator.isAlphanumeric(className);
	}
	
	/**
	 * クラス番号の妥当性をチェックする
	 * 
	 * @param classNo クラス番号
	 * @return チェック結果
	 */
	public boolean isValidClassNo(final String classNo) {
		
		return validator.isValidRange(classNo, 1, 99999)
			&& validator.isValidByteLength(classNo, 1, 5);
	}
	
	/**
	 * カナ氏名の妥当性をチェックする
	 * 
	 * @param nameKana 正規化後のカナ氏名
	 * @return チェック結果
	 */
	public boolean isValidKanaName(final String nameKana) {
		
		if (validator.isValidByteLength(nameKana, 1, 14)) {
			// 正規化した後なので全て半角カタカナか半角スペースのはず
			for (int i = 0; i < nameKana.length(); i++) {
				
				int c = nameKana.charAt(i);
				if (!((c >= 65382 && c <= 65439) || c == 32)) {
					return false;
				}
			}
			return !validator.isWQuoteOrComma(nameKana);
			
		} else {
			return false;
		}
	}
	
	/**
	 * 漢字氏名の妥当性をチェックする
	 * 
	 * @param nameKanji 漢字氏名
	 * @return チェック結果
	 */
	public boolean isValidKanjiName(final String nameKanji) {
		
		// 20バイト以内
		return validator.isValidByteLength(nameKanji, 0, 20)
				&& !validator.isWQuoteOrComma(nameKanji);
	}
	
	/**
	 * 性別の妥当性をチェックする
	 * 
	 * @param sex 性別
	 * @return チェック結果
	 */
	public boolean isValidSex(final String sex) {
		
		return "1".equals(sex) || "2".equals(sex);
	}
	
	/**
	 * 生年月日の妥当性をチェックする
	 * 
	 * @param birthday 生年月日
	 * @return チェック結果
	 */
	public boolean isValidBirthday(final String birthday) {
		
		if (birthday == null || birthday.length() == 0) {
			return false;
		}
		
		final String value;
		// スラッシュを含むならそのまま
		if (birthday.indexOf('/') >= 0) {
			value = birthday;
		// 含まないなら入れる
		} else {
			
			if (birthday.length() != 8) {
				return false;
			}
			
			value = birthday.substring(0, 4) + "/"
					+ birthday.substring(4, 6) + "/"
					+ birthday.substring(6, 8);
		}
		
		return validator.isValidDate(value)
			&& validator.isValidRange(value.substring(0, 4), 1900, CURRENT_YEAR);
	}

	/**
	 * 電話番号の妥当性をチェックする
	 * 
	 * @param telNo 正規化した電話番号
	 * @return チェック結果
	 */
	public boolean isValidTelNo(final String telNo) {
		
		if (telNo == null || "".equals(telNo)) {
			return true;
		} else if (telNo.length() > 11) {
			return false;
		}
		
		return validator.isPositiveInteger(telNo);
	}

}

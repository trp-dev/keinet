package jp.co.fj.keinavi.beans.profile.corrector;

import jp.co.fj.keinavi.beans.txt_out.TextInfoBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;

/**
 * プロファイルコレクタ６→７
 * 
 * <河合塾2010年度マーク高２模試対応>
 * 2011.05.20   Tomohisa YAMADA - Totec
 *              新規作成
 * 
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 */
public class Corrector7 implements ICorrector {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.profile.corrector.ICorrector#execute(jp.co.fj.keinavi.data.profile.Profile)
	 */
	public void execute(final Profile profile, final LoginSession login) {

		// 営業部以外の場合にのみ
		// テキスト出力（個人データ−模試別成績データ）を初期化する
		if (!login.isBusiness()) {
			StringBuffer selection = new StringBuffer((String)profile.getItemMap(T_IND_EXAM2).get(TEXT_OUT_ITEM));
			for (int i = 0; i < TextInfoBean.SELECTION24.length; i ++) {
				selection.append(",");
				selection.append(TextInfoBean.SELECTION24[i]);
			}
			profile.getItemMap(T_IND_EXAM2).put(TEXT_OUT_ITEM, selection.toString());
		}
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.profile.corrector.ICorrector#getVersion()
	 */
	public short getVersion() {
		return 7;
	}
	
}

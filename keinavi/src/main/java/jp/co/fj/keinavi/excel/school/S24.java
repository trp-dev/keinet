/*
 * 校内成績分析−過年度比較　志望大学評価別人数
 * 作成日: 2004/07/13
 * @author	C.Murata
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.school.S24Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

/**
 * @author keinet
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class S24 {

	/*
	 * 出力帳票選択処理
	 * フラグにより帳票出力の判断をする。
	 * 		S24Item s24Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID:ユーザID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */

	 public boolean s24(S24Item s24Item, ArrayList outfilelist, int intSaveFlg, String UserID, KNSheetLog sheetLog) {

		KNLog log = KNLog.getInstance(null,null,null);

	 	try{

	 		int intRunFlg = s24Item.getIntDaiTotalFlg();
	 		int ret = 0;

			//S24Itemから各帳票を出力
			switch(intRunFlg) {
				case 1:	//大学（日程あり）
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL START
//					log.Ep("S24_02","S24_02帳票作成開始","");
//					S24_02 exceledit02 = new S24_02();
//					ret = exceledit02.s24_02EditExcel(s24Item,outfilelist,intSaveFlg,UserID);
//					if( ret != 0 ){
//						log.Err("0"+ret+"S24_02","帳票作成エラー","");
//						return false;
//					}
//					log.Ep("S24_02","S24_02帳票作成終了","");
//					sheetLog.add("S24_02");
//					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL END
				case 2:	//大学（日程なし）
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL START
//					log.Ep("S24_01","S24_01帳票作成開始","");
//					S24_01 exceledit01 = new S24_01();
//					ret = exceledit01.s24_01EditExcel(s24Item,outfilelist,intSaveFlg,UserID);
//					if( ret != 0 ){
//						log.Err("0"+ret+"S24_01","帳票作成エラー","");
//						return false;
//					}
//					log.Ep("S24_01","S24_01帳票作成終了","");
//					sheetLog.add("S24_01");
//					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL END
				case 3:	//学部（日程あり）
// 2019/10/02 QQ)Tanioka 廃止帳票 ADD START
				case 4:
// 2019/10/02 QQ)Tanioka 廃止帳票 ADD END
					log.Ep("S24_04","S24_04帳票作成開始","");
					S24_04 exceledit04 = new S24_04();
					ret = exceledit04.s24_04EditExcel(s24Item,outfilelist,intSaveFlg,UserID);
					if( ret != 0 ){
						log.Err("0"+ret+"S24_04","帳票作成エラー","");
						return false;
					}
					log.Ep("S24_04","S24_04帳票作成終了","");
					sheetLog.add("S24_04");
					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL START
//				case 4:	//学部（日程なし）
//					log.Ep("S24_03","S24_03帳票作成開始","");
//					S24_03 exceledit03 = new S24_03();
//					ret = exceledit03.s24_03EditExcel(s24Item,outfilelist,intSaveFlg,UserID);
//					if( ret != 0 ){
//						log.Err("0"+ret+"S24_03","帳票作成エラー","");
//						return false;
//					}
//					log.Ep("S24_03","S24_03帳票作成終了","");
//					sheetLog.add("S24_03");
//					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL END
				case 5:	//学科
					log.Ep("S24_05","S24_05帳票作成開始","");
					S24_05 exceledit05 = new S24_05();
					ret = exceledit05.s24_05EditExcel(s24Item,outfilelist,intSaveFlg,UserID);
					if( ret != 0 ){
						log.Err("0"+ret+"S24_05","帳票作成エラー","");
						return false;
					}
					log.Ep("S24_05","S24_05帳票作成終了","");
					sheetLog.add("S24_05");
					break;
				default:
				    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
				    //throw new Exception("S24 ERROR : フラグ異常 ");
				    throw new IllegalStateException("S24 ERROR : フラグ異常");
				    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			}

	 	}
	 	catch(Exception e){
			log.Err("99S24","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
	 	}

	 	return true;

	 }

}

/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.com_set.factory;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.com_set.ComSubjectData;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM101Form;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.ProfileUtil;

import com.fjh.forms.ActionForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM101FormFactory extends AbstractCMFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		CM101Form form = new CM101Form();

		// 選択方式
		form.setSelection(
			super.getItemMap(request).get(IProfileItem.TYPE_SELECTION).toString()
		);

		// 分析・グラフ表示対象
		ComSubjectData data = ProfileUtil
			.getComTypeData(super.getProfile(request), super.getExamData(request));

		super.initSubjectForm(form, data);

		return form;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アイテムマップ
		Map item = super.getItemMap(request);
		// 共通アクションフォーム
		CM001Form f001 = super.getCommonForm(request);
		// 個別アクションフォーム
		CM101Form f101 = (CM101Form)f001.getActionForm("CM101Form");

		// アクションフォームがNULLなら画面が開かれていないので中止
		if (f101 == null) return;

		// 選択方式
		item.put(IProfileItem.TYPE_SELECTION, Short.valueOf(f101.getSelection()));

		// リスト
		List container = (List)item.get(IProfileItem.TYPE_COMMON);

		// 入れ物を取得する
		ComSubjectData data = new ComSubjectData(f001.getTargetYear(), f001.getTargetExam());
		int index = container.indexOf(data);
		if (index >= 0) data = (ComSubjectData)container.get(index);
		else container.add(data);
		
		super.reflectSubjectForm(f101, data);
	}

}

package jp.co.fj.keinavi.beans.exam.filter;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * オープン模試フィルタです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class OpenExamFilterBean implements IExamFilterBean {

    /**
     * {@inheritDoc}
     */
    public boolean execute(LoginSession login, ExamSession examSession, ExamData exam) {
        return !KNUtil.isOpenExam(exam);
    }

}

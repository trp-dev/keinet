/*
 * 作成日: 2004/09/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.help;

import com.fjh.forms.ActionForm;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class H003pForm extends ActionForm {

	private String helpId = "";		// ヘルプID


	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	
	/**
	 * @return
	 */
	public String getHelpId() {
		return helpId;
	}

	/**
	 * @param string
	 */
	public void setHelpId(String string) {
		helpId = string;
	}

}

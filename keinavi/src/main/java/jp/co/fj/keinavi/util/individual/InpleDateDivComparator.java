/*
 * 作成日: 2004/09/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.individual;

import jp.co.fj.keinavi.data.individual.NestedPlanUnivData;
import jp.co.fj.keinavi.util.db.RecordProcessor;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class InpleDateDivComparator implements java.util.Comparator {

	/* (非 Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(Object o1, Object o2) {
		NestedPlanUnivData.InpleDateDivData d1 = (NestedPlanUnivData.InpleDateDivData)o1;
		NestedPlanUnivData.InpleDateDivData d2 = (NestedPlanUnivData.InpleDateDivData)o2;
		if (trimSlash(d1.getEntExamInpleUniqueDate()) < trimSlash(d2.getEntExamInpleUniqueDate())) {
			return -1;
		} else if (trimSlash(d1.getEntExamInpleUniqueDate()) > trimSlash(d2.getEntExamInpleUniqueDate())) {
			return 1;
		} else {
			return 0;
		}
	}
	private int trimSlash(String string){
		int rt = -1;
		try{
			rt = Integer.parseInt(RecordProcessor.nonDateWSlash(string));
		}catch(NumberFormatException nfe){
			rt = -1;
		}
		return rt;
		/*
		StringBuffer buff = new StringBuffer(string);
		int index = buff.indexOf("/");
		buff.deleteCharAt(index);
		return Integer.parseInt(buff.toString());
		*/
	}

}

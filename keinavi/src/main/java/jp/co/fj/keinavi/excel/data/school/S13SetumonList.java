package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;

/**
 *  校内成績（設問別成績） 共通テスト国語記述問題評価別人数 設問別評価／設問 データリスト
 * 作成日: 2019/09/10
 * @author	QQ)Tanouchi
 */
public class S13SetumonList {

	//設問別評価／設問
        // 2019/11/28 QQ)Ooseto 国語記述設問変更対応 UPD START
	//校内-a評価 人数セット
	private int S_aHyoukaNin = 0;
	//校内-a評価 割合％セット
	private float S_aHyoukaWari = 0;
        //校内-a*評価 人数セット
        private int S_aAstHyoukaNin = 0;
        //校内-a*評価 割合％セット
        private float S_aAstHyoukaWari = 0;
	//校内-b評価 人数セット
	private int S_bHyoukaNin = 0;
	//校内-b評価 割合％セット
	private float S_bHyoukaWari = 0;
        //校内-b*評価 人数セット
        private int S_bAstHyoukaNin = 0;
        //校内-b*評価 割合％セット
        private float S_bAstHyoukaWari = 0;
	//校内-c評価 人数セット
	private int S_cHyoukaNin = 0;
	//校内-c評価 割合％セット
	private float S_cHyoukaWari = 0;
	//校内-d評価 人数セット
	//private int S_dHyoukaNin = 0;
	//校内-d評価 割合％セット
	//private float S_dHyoukaWari = 0;
	//全国-a評価 人数セット
	private int A_aHyoukaNin = 0;
	//全国-a評価 割合％セット
	private float A_aHyoukaWari = 0;
        //全国-a*評価 人数セット
        private int A_aAstHyoukaNin = 0;
        //全国-a*評価 割合％セット
        private float A_aAstHyoukaWari = 0;
	//全国-b評価 人数セット
	private int A_bHyoukaNin = 0;
	//全国-b評価 割合％セット
	private float A_bHyoukaWari = 0;
        //全国-b*評価 人数セット
        private int A_bAstHyoukaNin = 0;
        //全国-b*評価 割合％セット
        private float A_bAstHyoukaWari = 0;
	//全国-c評価 人数セット
	private int A_cHyoukaNin = 0;
	//全国-c評価 割合％セット
	private float A_cHyoukaWari = 0;
	//全国-d評価 人数セット
	//private int A_dHyoukaNin = 0;
	//全国-d評価 割合％セット
	//private float A_dHyoukaWari = 0;
	// 2019/11/28 QQ)Ooseto 国語記述設問変更対応 UPD END
	//設問番号
	private String Question_No = "";

	//成績表データリスト
	private ArrayList S13SetumonList = new ArrayList();

	/*----------*/
	/* Get      */
	/*----------*/

	// 2019/11/28 QQ)Ooseto 国語記述設問変更対応 UPD START
	public int getS_aHyoukaNin() {
		return S_aHyoukaNin;
	}
	public float getS_aHyoukaWari() {
		return S_aHyoukaWari;
	}
        public int getS_aAstHyoukaNin() {
                return S_aAstHyoukaNin;
        }
        public float getS_aAstHyoukaWari() {
                return S_aAstHyoukaWari;
        }
	public int getS_bHyoukaNin() {
		return S_bHyoukaNin;
	}
	public float getS_bHyoukaWari() {
		return S_bHyoukaWari;
	}
        public int getS_bAstHyoukaNin() {
                return S_bAstHyoukaNin;
        }
        public float getS_bAstHyoukaWari() {
                return S_bAstHyoukaWari;
        }
	public int getS_cHyoukaNin() {
		return S_cHyoukaNin;
	}
	public float getS_cHyoukaWari() {
		return S_cHyoukaWari;
	}
//	public int getS_dHyoukaNin() {
//		return S_dHyoukaNin;
//	}
//	public float getS_dHyoukaWari() {
//		return S_dHyoukaWari;
//	}

	public int getA_aHyoukaNin() {
		return A_aHyoukaNin;
	}
	public float getA_aHyoukaWari() {
		return A_aHyoukaWari;
	}
        public int getA_aAstHyoukaNin() {
                return A_aAstHyoukaNin;
        }
        public float getA_aAstHyoukaWari() {
                return A_aAstHyoukaWari;
        }
	public int getA_bHyoukaNin() {
		return A_bHyoukaNin;
	}
	public float getA_bHyoukaWari() {
		return A_bHyoukaWari;
	}
        public int getA_bAstHyoukaNin() {
                return A_bAstHyoukaNin;
        }
        public float getA_bAstHyoukaWari() {
                return A_bAstHyoukaWari;
        }
	public int getA_cHyoukaNin() {
		return A_cHyoukaNin;
	}
	public float getA_cHyoukaWari() {
		return A_cHyoukaWari;
	}
//	public int getA_dHyoukaNin() {
//		return A_dHyoukaNin;
//	}
//	public float getA_dHyoukaWari() {
//		return A_dHyoukaWari;
//	}
	// 2019/11/28 QQ)Ooseto 国語記述設問変更対応 UPD END

	public String getQuestion_No() {
		return Question_No;
	}

	public ArrayList getS13SetumonList() {
		return this.S13SetumonList;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	// 2019/11/28 QQ)Ooseto 国語記述設問変更対応 UPD START
	public void setS_aHyoukaNin(int s_aHyoukaNin) {
		this.S_aHyoukaNin = s_aHyoukaNin;
	}
	public void setS_aHyoukaWari(float s_aHyoukaWari) {
		this.S_aHyoukaWari = s_aHyoukaWari;
	}
        public void setS_aAstHyoukaNin(int s_aAstHyoukaNin) {
                this.S_aAstHyoukaNin = s_aAstHyoukaNin;
        }
        public void setS_aAstHyoukaWari(float s_aAstHyoukaWari) {
                this.S_aAstHyoukaWari = s_aAstHyoukaWari;
        }
	public void setS_bHyoukaNin(int s_bHyoukaNin) {
		this.S_bHyoukaNin = s_bHyoukaNin;
	}
	public void setS_bHyoukaWari(float s_bHyoukaWari) {
		this.S_bHyoukaWari = s_bHyoukaWari;
	}
        public void setS_bAstHyoukaNin(int s_bAstHyoukaNin) {
                this.S_bAstHyoukaNin = s_bAstHyoukaNin;
        }
        public void setS_bAstHyoukaWari(float s_bAstHyoukaWari) {
                this.S_bAstHyoukaWari = s_bAstHyoukaWari;
        }
	public void setS_cHyoukaNin(int s_cHyoukaNin) {
		this.S_cHyoukaNin = s_cHyoukaNin;
	}
	public void setS_cHyoukaWari(float s_cHyoukaWari) {
		this.S_cHyoukaWari = s_cHyoukaWari;
	}
//	public void setS_dHyoukaNin(int s_dHyoukaNin) {
//		this.S_dHyoukaNin = s_dHyoukaNin;
//	}
//	public void setS_dHyoukaWari(float s_dHyoukaWari) {
//		this.S_dHyoukaWari = s_dHyoukaWari;
//	}

	public void setA_aHyoukaNin(int a_aHyoukaNin) {
		this.A_aHyoukaNin = a_aHyoukaNin;
	}
	public void setA_aHyoukaWari(float a_aHyoukaWari) {
		this.A_aHyoukaWari = a_aHyoukaWari;
	}
        public void setA_aAstHyoukaNin(int a_aAstHyoukaNin) {
                this.A_aAstHyoukaNin = a_aAstHyoukaNin;
        }
        public void setA_aAstHyoukaWari(float a_aAstHyoukaWari) {
                this.A_aAstHyoukaWari = a_aAstHyoukaWari;
        }
	public void setA_bHyoukaNin(int a_bHyoukaNin) {
		this.A_bHyoukaNin = a_bHyoukaNin;
	}
	public void setA_bHyoukaWari(float a_bHyoukaWari) {
		this.A_bHyoukaWari = a_bHyoukaWari;
	}
        public void setA_bAstHyoukaNin(int a_bAstHyoukaNin) {
                this.A_bAstHyoukaNin = a_bAstHyoukaNin;
        }
        public void setA_bAstHyoukaWari(float a_bAstHyoukaWari) {
                this.A_bAstHyoukaWari = a_bAstHyoukaWari;
        }
	public void setA_cHyoukaNin(int a_cHyoukaNin) {
		this.A_cHyoukaNin = a_cHyoukaNin;
	}
	public void setA_cHyoukaWari(float a_cHyoukaWari) {
		this.A_cHyoukaWari = a_cHyoukaWari;
	}
//	public void setA_dHyoukaNin(int a_dHyoukaNin) {
//		this.A_dHyoukaNin = a_dHyoukaNin;
//	}
//	public void setA_dHyoukaWari(float a_dHyoukaWari) {
//		this.A_dHyoukaWari = a_dHyoukaWari;
//	}
	public void setQuestion_No(String question_No) {
		this.Question_No = question_No;
	}
	// 2019/11/28 QQ)Ooseto 国語記述設問変更対応 UPD END


	public void setS12SetumonList(ArrayList s13SetumonList) {
		this.S13SetumonList = s13SetumonList;
	}

}

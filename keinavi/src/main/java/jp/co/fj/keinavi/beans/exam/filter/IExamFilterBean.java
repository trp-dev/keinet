package jp.co.fj.keinavi.beans.exam.filter;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;

/**
 *
 * 模試フィルタリングBeanのインターフェース
 * 
 * 2006.08.23	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public interface IExamFilterBean {

	/**
	 * @param login ログイン情報
	 * @param examSession 模試セッション
	 * @param exam 模試データ
	 * @return 判定結果
	 */
	public boolean execute(LoginSession login, ExamSession examSession, ExamData exam);
	
}

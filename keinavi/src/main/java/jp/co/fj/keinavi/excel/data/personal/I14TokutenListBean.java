package jp.co.fj.keinavi.excel.data.personal;

/**
 * �l���ѕ��́|�󌱗p�����|�u�]�Z���� ���ȕʐ��у��X�g
 * �쐬��: 2004/09/09
 * @author	C.Murata
 *
 * 2009.11.30   Fujito URAKAWA - Totec
 *              �u�w�̓��x���v�ǉ��Ή�
 */
public class I14TokutenListBean {

    //���ȃR�[�h
    private String strKyokaCd = "";
    //�^�E�Ȗږ�
    private String strKmkmei = "";
    //���_
    private int intTokuten = 0;
    //�΍��l
    private float floHensa = 0;
    //�w�̓��x��
    private String strScholarLevel = "";
    //��b�Ȗڃt���O
    private String basicFlg;

    /**
     * @GET
     */
    public float getFloHensa() {
        return this.floHensa;
    }
    public int getIntTokuten() {
        return this.intTokuten;
    }
    public String getStrKmkmei() {
        return this.strKmkmei;
    }
    public String getStrKyokaCd() {
        return this.strKyokaCd;
    }
    public String getStrScholarLevel() {
        return strScholarLevel;
    }
    public String getBasicFlg() {
        return basicFlg;
    }

    /**
     * @SET
     */
    public void setFloHensa(float floHensa) {
        this.floHensa = floHensa;
    }
    public void setIntTokuten(int intTokuten) {
        this.intTokuten = intTokuten;
    }
    public void setStrKmkmei(String strKmkmei) {
        this.strKmkmei = strKmkmei;
    }
    public void setStrKyokaCd(String strKyokaCd) {
        this.strKyokaCd = strKyokaCd;
    }
    public void setStrScholarLevel(String string) {
        this.strScholarLevel = string;
    }
    public void setBasicFlg(String basicFlg) {
        this.basicFlg = basicFlg;
    }

}

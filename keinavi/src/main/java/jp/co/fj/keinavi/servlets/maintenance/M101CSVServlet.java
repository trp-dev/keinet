package jp.co.fj.keinavi.servlets.maintenance;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.maintenance.StudentListBean;
import jp.co.fj.keinavi.data.maintenance.KNStudentData;
import jp.co.totec.spreadsheet.CSVLineBuilder;
import jp.co.totec.spreadsheet.SpreadSheetWriter;
import jp.co.totec.spreadsheet.cell.DateCellData;
import jp.co.totec.spreadsheet.cell.StringCellData;

/**
 *
 * 生徒情報CSV出力サーブレット
 * 
 * 2005.11.24	[新規作成]
 * 2006.10.27	2006年度改修
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M101CSVServlet extends AbstractMServlet {

	// CSVヘッダ
	private static final String[] HEADER = new String[]{
			"#年度", "学年", "クラス", "クラス番号", "かな氏名",
			"漢字氏名", "性別", "生年月日", "電話番号", "個人ID",
	};
	
	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {
		
		// CSV出力処理
		if ("m101_csv".equals(getForward(request))) {

			// セッション情報
			final StudentListBean bean = getStudentListBean(request);
			
			// contentTypeを出力
			response.setContentType("application/octet-stream-dummy");
			// ファイル名の送信
			response.setHeader("Content-Disposition", "inline; filename=\""
					+ createFileName("pi", bean.getYear(),
							bean.getGrade(), bean.getClassName()) + "\"");

			// CSV出力クラスのインスタンスを生成
			final SpreadSheetWriter sheet = new SpreadSheetWriter(
					new BufferedWriter(
							new OutputStreamWriter(
									response.getOutputStream(), "Windows-31J")),
					new CSVLineBuilder(), HEADER);
				
			// セルのデータ型を決める
			setCellType(sheet);
			// 行ヘッダを書き込む
			sheet.writeHeader();
			// データを書き込む
			assign(sheet, getStudentListBean(request).getDispStudentList());
			// 出力
			sheet.flush();
			// アクセスログ
			actionLog(request, "805");
			
		// 不明ならエラー
		} else {
			throw new ServletException("不正なアクセスです。");
		}
	}

	// Bridgeメソッド
	private String createFileName(final String prefix, final String year,
			final int grade, final String className) {
		return createFileName(prefix, year, grade >= 0 ? grade + "" : "all",
				className == null ? "all" : className);
	}
	
	/**
	 * （注意）校内成績処理システムからも参照される
	 * 
	 * @param prefix ファイル名接頭辞
	 * @param year 年度
	 * @param grade 学年
	 * @param className クラス
	 * @return ファイル名
	 */
	public String createFileName(final String prefix, final String year,
			final String grade, final String className) {	
		return prefix + year.substring(2)
				+ ("all".equals(grade) ? "" : grade)
				+ ("all".equals(className) ? "" : className)
				+ "_" + new SimpleDateFormat("yyyyMMddHHmm").format(new Date())
				+ ".csv";
	}
	
	// セルのデータ型をセットする
	private void setCellType(final SpreadSheetWriter sheet) {
		sheet
			.addDataType(new StringCellData())
			.addDataType(new StringCellData())
			.addDataType(new StringCellData())
			.addDataType(new StringCellData())
			.addDataType(new StringCellData())
			.addDataType(new StringCellData())
			.addDataType(new StringCellData())
			.addDataType(new DateCellData(new SimpleDateFormat("yyyyMMdd")))
			.addDataType(new StringCellData())
			.addDataType(new StringCellData());
	}	
	
	// リストデータをCSVにセットする
	private void assign(final SpreadSheetWriter sheet, 
			final List list) throws IOException {
		
		for (final Iterator ite = list.iterator(); ite.hasNext();) {
			
			final KNStudentData data = (KNStudentData) ite.next();
			
			// 個人ID
			sheet.setValue(10, data.getIndividualId());
			// 年度
			sheet.setValue(1, data.getYear());
			// 学年
			sheet.setValue(2,
					data.getGrade() >= 0
							? String.valueOf(data.getGrade()) : "9");
			// クラス
			sheet.setValue(3, data.getClassName());
			// クラス番号
			sheet.setValue(4, data.getClassNo());
			// カナ氏名
			sheet.setValue(5, data.getNameKana());
			// 漢字氏名
			sheet.setValue(6, data.getNameKanji());
			// 性別
			sheet.setValue(7, data.getSex());
			// 生年月日
			sheet.setValue(8, data.getBirthday());
			// 電話番号
			sheet.setValue(9, data.getTelNo());
			
			sheet.writeLine();
		}
	}
	
}

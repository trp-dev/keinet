/*
 * 作成日: 2004/07/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.school.factory;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.forms.school.SMaxForm;

import com.fjh.forms.ActionForm;

/**
 * @author 奥村ゆかり
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 * 校内成績：偏差値分布：詳細
 */
public class S204FormFactory extends AbstractSFormFactory {
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 * 設問別成績
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		// アクションフォーム
		SMaxForm form = super.createSMaxForm(request);
		// アイテムマップ
		Map item = getItemMap(request);

		CommonFormFactory commForm = new CommonFormFactory(form, item);
		commForm.setForm0203();
		commForm.setForm0601();
		commForm.setForm0701();
		commForm.setForm0502();
		// 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
		// 2019/07/29 QQ)Tanioka 共通テスト対応 ADD START
		//commForm.setForm1701();
		//commForm.setForm1702();
		// 2019/07/29 QQ)Tanioka 共通テスト対応 ADD END
		// 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END
		commForm.setForm1305();
		commForm.setFormat();
		commForm.setOption();
		return commForm.getForm();
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
		//		SMaxForm form = (SMaxForm)request.getSession().getAttribute(SMaxForm.SESSION_KEY);
		SMaxForm form = (SMaxForm) request.getAttribute("form");

		// アイテムマップ
		Map item = getItemMap(request);

		CommonItemFactory commItem = new CommonItemFactory(form, item);
		commItem.setItem0203();
		commItem.setItem0601();
		commItem.setItem0701();
		commItem.setItem0502();
		// 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
		// 2019/07/29 QQ)Tanioka 共通テスト対応 ADD START
		//commItem.setItem1701();
		//commItem.setItem1702();
		// 2019/07/29 QQ)Tanioka 共通テスト対応 ADD END
		// 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END
		commItem.setItem1305();
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap("020403");
	}

}

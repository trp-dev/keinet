/*
 * 作成日: 2004/09/28
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.admin;

/**
 * @author 奥村ゆかり
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CompositeInfo {
	private String packageCode = null;
	private String clas = null;
	private String groupCode = null;
	private String className = null;
	private String comment = null;
	private String number = null;
	private boolean selectJudge = false;

	/**
	 * @return
	 */
	public String getClas() {
		return clas;
	}

	/**
	 * @return
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @return
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @return
	 */
	public String getGroupCode() {
		return groupCode;
	}

	/**
	 * @return
	 */
	public String getNumber() {
		return number;
	}



	/**
	 * @param string
	 */
	public void setClas(String string) {
		clas = string;
	}

	/**
	 * @param string
	 */
	public void setClassName(String string) {
		className = string;
	}

	/**
	 * @param string
	 */
	public void setComment(String string) {
		comment = string;
	}

	/**
	 * @param string
	 */
	public void setGroupCode(String string) {
		groupCode = string;
	}

	/**
	 * @param string
	 */
	public void setNumber(String string) {
		number = string;
	}

	/**
	 * @return
	 */
	public String getPackageCode() {
		return packageCode;
	}

	/**
	 * @param string
	 */
	public void setPackageCode(String string) {
		packageCode = string;
	}

	/**
	 * @return
	 */
	public boolean isSelectJudge() {
		return selectJudge;
	}

	/**
	 * @param b
	 */
	public void setSelectJudge(boolean b) {
		selectJudge = b;
	}

}

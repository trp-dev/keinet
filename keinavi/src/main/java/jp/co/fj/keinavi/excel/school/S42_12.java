/**
 * 校内成績分析−他校比較　偏差値分布-構成比グラフ（2.5pt）
 * 	Excelファイル編集
 * 作成日: 2004/08/11
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.school;

import java.io.*;
import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.excel.cm.*;
import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.*;

public class S42_12 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	final private int	intMaxSheetSr	= 40;			// 最大シート数	
	final private String	strArea			= "A1:AH63";	// 印刷範囲	
	final private String	masterfile0		= "S42_12";		// ファイル名
	final private String	masterfile1		= "NS42_12";	// ファイル名
	private String	masterfile		= "";					// ファイル名
	/*
	 * 	Excel編集メイン
	 * 		S42Item s42Item: データクラス
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
 	public int s42_12EditExcel(S42Item s42Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("S42_12","S42_12帳票作成開始","");
		
		//テンプレートの決定
		if (s42Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			
			// データセット
			ArrayList	s42List			= s42Item.getS42List();
			Iterator	itr				= s42List.iterator();
			int		row				= 0;	// 列
			int 		setRow			= -1;	// *セット用
			float 		koseihi			= 0;	// *作成用
			int		writeCnt		= 0;	// 書き込みカウンター
			float		sumKoseihi		= 0;	// 人数計算用
			float		min				= 75.0f;// 偏差基準値
			int		hyouCnt			= 0;	// 値範囲：０〜１　(表カウンター)
			int		gakkoCnt		= 0;	// 値範囲：０〜９　(学校カウンター)
			int		allGakkoCnt		= 0;	// 全学校カウンター
			int		maxGakkoCnt		= 0;	// 学校カウンター
			int		maxSheetIndex	= 0;	// シートカウンター
			int		intBookCngCount	= 0;	// ブックカウンター
			int		kmkCd			= 0;	// 科目コード保持
			boolean	kataFlg			= true;// false:改行済(型・科目名変更済)　
			boolean	bolFirstDataFlg	= true;// true:List.Next()を実行
			boolean	bolGakkouFlg	= true;// true:学校情報表示　（学校情報表示フラグ）
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
//add 2004/10/27 T.Sakai データ0件対応
			int		dispGakkoFlgCnt		= 0;		// グラフ表示フラグカウンタ
//add end
//add 2004/10/27 T.Sakai セル計算対応
			float		ruikeiKoseihi		= 0;		// 累計構成比
//add end

			FileInputStream	fin			= null;
			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			// 基本ファイルを読込む
			S42ListBean s42ListBean = new S42ListBean();

			while( itr.hasNext() ) {
				s42ListBean = (S42ListBean)itr.next();

				// 科目データ表示処理
				if ( s42ListBean.getIntDispKmkFlg()==1 ) {

					// 基本ファイルを読み込む
					S42GakkoListBean	s42GakkoListBean	= new S42GakkoListBean();
	
					//データの保持
					ArrayList	s42GakkoList		= s42ListBean.getS42GakkoList();
					Iterator	itrS42Gakko		= s42GakkoList.iterator();

					// 表示学校数取得
					maxGakkoCnt=0;
					while(itrS42Gakko.hasNext()){
						s42GakkoListBean	= (S42GakkoListBean) itrS42Gakko.next();
						if(s42GakkoListBean.getIntDispGakkoFlg()==1 || s42GakkoListBean.getIntDispGakkoFlg()==2){
							maxGakkoCnt++;
						}
					}
					itrS42Gakko		= s42GakkoList.iterator();
	
					bolFirstDataFlg=true;
	
					//　学校データセット
					gakkoCnt = 0;
					allGakkoCnt = 0;
					while(itrS42Gakko.hasNext()){
	
						if( bolBookCngFlg == true ){
							// マスタExcel読み込み
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook==null ){
								return errfread;
							}
							bolSheetCngFlg	= true;
							bolBookCngFlg	= false;
							maxSheetIndex	= 0;
						}
	
						if(bolSheetCngFlg){
							// データセットするシートの選択
							workSheet = workbook.getSheet(Integer.toString(maxSheetIndex+1));
							maxSheetIndex++;
	
							// ヘッダ右側に帳票作成日時を表示する
							cm.setHeader(workbook, workSheet);
	
							// セキュリティスタンプセット
							String secuFlg = cm.setSecurity( workbook, workSheet, s42Item.getIntSecuFlg() ,32 ,33 );
							workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
							workCell.setCellValue(secuFlg);
	
							// 学校名セット
							workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
							workCell.setCellValue( "学校名　　　：" + cm.toString(s42Item.getStrGakkomei()) );
	
							// 対象模試セット
							String moshi =cm.setTaisyouMoshi( s42Item.getStrMshDate() );	// 模試月取得
							workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
							workCell.setCellValue( "対象模試　　：" + cm.toString(s42Item.getStrMshmei()) + moshi);
	
							// 型名・配点セット
							String haiten = "";
							if ( !cm.toString(s42ListBean.getStrHaitenKmk()).equals("") ) {
								haiten = "（" + s42ListBean.getStrHaitenKmk() + "）";
							}
							workCell = cm.setCell( workSheet, workRow, workCell, 6, 0 );
							workCell.setCellValue( "型・科目：" + cm.toString(s42ListBean.getStrKmkmei()) + haiten );
							kmkCd=Integer.parseInt(s42ListBean.getStrKmkCd());
	
							bolSheetCngFlg=false;
							bolGakkouFlg=true;
						}
	
						if(bolGakkouFlg){
							s42GakkoListBean	= (S42GakkoListBean) s42GakkoList.get(0);
							bolGakkouFlg=false;

							if(bolFirstDataFlg){
								s42GakkoListBean	= (S42GakkoListBean) itrS42Gakko.next();
								bolFirstDataFlg=false;
							}
						}else{
							s42GakkoListBean	= (S42GakkoListBean) itrS42Gakko.next();
						}

						// 学校データ表示フラグ判定
						if (( s42GakkoListBean.getIntDispGakkoFlg()==1 )||( s42GakkoListBean.getIntDispGakkoFlg()==2 )) {
//add 2004/10/27 T.Sakai データ0件対応
							dispGakkoFlgCnt++;
//add end
							S42BnpListBean s42BnpListBean = new S42BnpListBean();
							ArrayList	s42BnpList	= s42GakkoListBean.getS42BnpList();
							Iterator	itrS42Bnp	= s42BnpList.iterator();
		
							// 偏差値データセット
							row=0;
							if (s42Item.getIntShubetsuFlg() == 1){
								min = 190.0f;
							} else{
								min = 75.0f;
							}
							writeCnt=0;
							sumKoseihi = 0;
//add 2004/10/27 T.Sakai セル計算対応
							ruikeiKoseihi = 0;
//add end
							
							while(itrS42Bnp.hasNext()){
								s42BnpListBean = (S42BnpListBean) itrS42Bnp.next();
		
								if(row==0){
									// 高校名セット
									workCell = cm.setCell( workSheet, workRow, workCell, 37, 1+3*gakkoCnt );
									workCell.setCellValue( s42GakkoListBean.getStrGakkomei() );
		
									// 合計人数セット
									if(s42GakkoListBean.getIntNinzu()!=-999){
										workCell = cm.setCell( workSheet, workRow, workCell, 58, 1+3*gakkoCnt );
										workCell.setCellValue( s42GakkoListBean.getIntNinzu() );
									}
		
									// 平均点セット
									if(s42GakkoListBean.getFloHeikin()!=-999.0){
										workCell = cm.setCell( workSheet, workRow, workCell, 59, 1+3*gakkoCnt );
										workCell.setCellValue( s42GakkoListBean.getFloHeikin() );
									}
		
									// 平均偏差値セット
									if(s42GakkoListBean.getFloHensa()!=-999.0){
										workCell = cm.setCell( workSheet, workRow, workCell, 60, 1+3*gakkoCnt );
										workCell.setCellValue( s42GakkoListBean.getFloHensa() );
									}
								}
			
								// 人数セット
								if ( s42BnpListBean.getFloBnpMin()==min ) {
									if ( s42BnpListBean.getFloKoseihi() != -999.0 ) {
										sumKoseihi = sumKoseihi + s42BnpListBean.getFloKoseihi();
									}
//add 2004/10/27 T.Sakai セル計算対応
									// 累計構成比セット
									workCell = cm.setCell( workSheet, workRow, workCell, 39+writeCnt, 3+3*gakkoCnt  );
									ruikeiKoseihi = ruikeiKoseihi + sumKoseihi;
									workCell.setCellValue( ruikeiKoseihi );

									// 得点用のときは処理しない
									if (s42Item.getIntShubetsuFlg() == 1){
									} else{
										if (s42BnpListBean.getFloBnpMin()==60.0f) {
											// 偏差値60以上の構成比セット
											workCell = cm.setCell( workSheet, workRow, workCell, 61, 1+3*gakkoCnt );
											workCell.setCellValue( ruikeiKoseihi );
										}
										if (s42BnpListBean.getFloBnpMin()==50.0f) {
											// 偏差値50以上の構成比セット
											workCell = cm.setCell( workSheet, workRow, workCell, 62, 1+3*gakkoCnt );
											workCell.setCellValue( ruikeiKoseihi );
										}
									}
//add end
									if ( sumKoseihi != 0 ) {
										workCell = cm.setCell( workSheet, workRow, workCell, 39+writeCnt, 2+3*gakkoCnt  );
										workCell.setCellValue( sumKoseihi );
			
										// *セット準備
										if( sumKoseihi > koseihi ){
											koseihi = sumKoseihi;
											setRow = writeCnt;
										}
//										writeCnt++;
									}
									writeCnt++;
									sumKoseihi = 0;
									if (s42Item.getIntShubetsuFlg() == 1){
										min -= 10.0f;
									} else{
										min -= 2.5f;
									}
								} else {
									if ( s42BnpListBean.getFloKoseihi() != -999.0 ) {
										sumKoseihi = sumKoseihi + s42BnpListBean.getFloKoseihi();
									}
			
									// *セット
									if(s42BnpListBean.getFloBnpMin() == -999.0){
//add 2004/10/27 T.Sakai セル計算対応
										// 累計構成比セット
										workCell = cm.setCell( workSheet, workRow, workCell, 39+writeCnt, 3+3*gakkoCnt  );
										ruikeiKoseihi = ruikeiKoseihi + sumKoseihi;
										workCell.setCellValue( ruikeiKoseihi );
//add end
										if ( sumKoseihi != 0 ) {
											workCell = cm.setCell( workSheet, workRow, workCell, 39+writeCnt, 2+3*gakkoCnt  );
											workCell.setCellValue( sumKoseihi );
			
											// *セット準備
											if( sumKoseihi > koseihi ){
												koseihi = sumKoseihi;
												setRow = writeCnt;
											}
//											writeCnt++;
										}
										sumKoseihi = 0;
		
										if(setRow!=-1){
											workCell = cm.setCell( workSheet, workRow, workCell, 39+setRow, 1+3*gakkoCnt  );
											workCell.setCellValue("*");
										}
										koseihi=0;
										setRow=-1;
									}
								}
		
								row++;
		
							}

							gakkoCnt++;
							allGakkoCnt++;
	
							// 以降に学校情報がなければループ脱出
							if(maxGakkoCnt==allGakkoCnt){
								break;
							}
	
							// データが存在する場合
							if(gakkoCnt>=11){
								gakkoCnt=0;
								allGakkoCnt--;
								bolSheetCngFlg=true;
							}

							// ファイルを閉じる処理
							if(bolSheetCngFlg){
								if((maxSheetIndex >= intMaxSheetSr)&&(itrS42Gakko.hasNext())){
									bolBookCngFlg = true;
								}
		
						
								if( bolBookCngFlg == true){
									boolean bolRet = false;
									// Excelファイル保存
									if(intBookCngCount==0){
										if(itr.hasNext() == true){
											bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
										}
										else{
											bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
										}
									}else{
										bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
									}
		
									if( bolRet == false ){
										return errfwrite;
									}
									intBookCngCount++;
								}
							}
						}

						// 学校が非表示指定(表示フラグ≠１)だった場合（エラー）
						bolGakkouFlg=false;
					}

					// ファイルを閉じる処理
					if(itr.hasNext()){
						gakkoCnt=0;
						bolSheetCngFlg=true;
	
						if(maxSheetIndex >= intMaxSheetSr){
							bolBookCngFlg = true;
						}
	
					
						if( bolBookCngFlg == true){
	
							boolean bolRet = false;
							// Excelファイル保存
							if(intBookCngCount==0){
								if(itr.hasNext() == true){
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
								}
								else{
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
								}
							}else{
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
							}
	
							if( bolRet == false ){
								return errfwrite;
							}
							intBookCngCount++;
						}
					}
				}

			}
//add 2004/10/27 T.Sakai データ0件対応
			if ( dispGakkoFlgCnt==0 ) {
				// マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				bolSheetCngFlg	= true;
				bolBookCngFlg	= false;
				maxSheetIndex	= 0;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet( Integer.toString(maxSheetIndex+1) );
				maxSheetIndex++;
	
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
	
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s42Item.getIntSecuFlg() ,32 ,33 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
				workCell.setCellValue(secuFlg);
	
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　　　：" + cm.toString(s42Item.getStrGakkomei()) );
	
				// 対象模試セット
				String moshi =cm.setTaisyouMoshi( s42Item.getStrMshDate() );	// 模試月取得
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "対象模試　　：" + cm.toString(s42Item.getStrMshmei()) + moshi);
			}
//add end
			// ファイルを閉じる処理
			if( bolBookCngFlg == false){

				boolean bolRet = false;
				// Excelファイル保存
				if(intBookCngCount!=0){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}
			
		} catch(Exception e) {
			log.Err("S42_12","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S42_12","S42_12帳票作成終了","");
		return noerror;
	}

}
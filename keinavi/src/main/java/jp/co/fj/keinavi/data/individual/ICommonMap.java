package jp.co.fj.keinavi.data.individual;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 *
 * 個人成績分析用 - セッション情報保持クラス
 *
 *
 * @author TOTEC)YAMADA.Tomoshisa
 * @author TOTEC)KONDO.Keisuke
 * @author TOTEC)URAKAWA.Fujito
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ICommonMap implements Serializable {

    public static final String SESSION_KEY = "iCommonMap";

    private static final long serialVersionUID = -1263206707599844705L;

    //現在のモード　分析・面談
    private String mode;

    //模試コンボボックス
    private ExamComboData examComboData;

    //現在選択中の模試データ
    private String targetPersonId;
    private String targetExamYear;
    private String targetExamCode;
    private String targetExamName;
    private String targetExamTypeCode;
    private String targetGrade;

    private String[] dispClasses;//[1] add
    private List dispStudents;//[1] add

    private String dispScholarLevel;

    private String dispSubCd;

    //「設問別成績」用の学力レベル保持データ
    private HashMap indSubScholarLevels = new HashMap();
    //「設問別成績（過回模試）」用の学力レベル保持データ
    private HashMap indSubScholarLevels109 = new HashMap();

    private String[] interviewSheet;

    // 2020/04/07 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD START
    //セキュリティスタンプ
    private String printStamp;
    //個人成績分析共通-印刷対象生徒（判定対象生徒)
    private String printStudent;
    //個人成績用
    private String report;
    //面談用帳票
    private String[] interviewForms;
    // 2020/04/07 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD END

    /**
     * 対象試験
     */
    private jp.co.fj.keinavi.data.ExamData examData;

    /**
     * 対象試験をセットする
     *
     * @param pExamYear 試験年度
     * @param pExamCode 試験コード
     */
    public void setTargetExam(
            final String pExamYear,
            final String pExamCode) {

        final jp.co.fj.keinavi.data.individual.ExamComboData.YearData.ExamData
            temp = getExamComboData().getExamData(pExamYear, pExamCode);

        setTargetExamYear(temp.getExamYear());
        setTargetExamCode(temp.getExamCode());
        setTargetExamTypeCode(temp.getExamTypeCode());
        setTargetExamName(temp.getExamName());

        examData = new jp.co.fj.keinavi.data.ExamData();
        examData.setExamYear(temp.getExamYear());
        examData.setExamCD(temp.getExamCode());
        examData.setExamTypeCD(temp.getExamTypeCode());
        examData.setExamName(temp.getExamName());
        examData.setExamNameAbbr(temp.getExamNameAbbr());
        examData.setOutDataOpenDate(temp.getOutDataOpenDate());
        examData.setInDataOpenDate(temp.getOutDataOpenDate());
        examData.setInpleDate(temp.getInpleDate());
        examData.setTargetGrade(temp.getTargetGrade());
        examData.setDispSequence(Integer.parseInt(temp.getDispSequence()));
    }

    /**
     * @return 対象試験
     */
    public jp.co.fj.keinavi.data.ExamData getExamData() {
        return examData;
    }

    //対象模試が存在するかどうか
    public boolean isTargetExamExist(){
        if(targetExamCode == null || targetExamCode.trim().equals("")){
            return false;
        }else{
            return true;
        }
    }

    //選択中の生徒の学年
    public int getSelectedGrade(){
        //最初の生徒の学年を返す（生徒はすべて同じ学年）
        return Integer.parseInt(((StudentData)selectedIndividuals.get(0)).getStudentGrade());
    }

    /**
     * 現在選択中の生徒のデータ(StudentDataクラスのリスト)
     */
    private List selectedIndividuals;

    /**
     * 現在選択中の生徒IDを文字列配列で返す
     * @return
     */
    public String[] getSelectedIndividualIds(){
        String[] selectedIndividulaIds = null;
        if(selectedIndividuals != null && selectedIndividuals.size() > 0){
            selectedIndividulaIds = new String[selectedIndividuals.size()];
            for(int i=0; i<selectedIndividuals.size(); i++){
                selectedIndividulaIds[i] = ((StudentData)selectedIndividuals.get(i)).getStudentPersonId();
            }
        }
        return selectedIndividulaIds;
    }

    /**
     * JSP表示用のサイズ取得メソッド
     * @return
     */
    public int getSelectedIndividualsSize(){
        int size = 0;
        if(selectedIndividuals != null)
            size = selectedIndividuals.size();
        return size;
    }

    /**
     * 現在が分析モードかどうか
     * ※デフォルトは分析モード
     * @return
     */
    public boolean isBunsekiMode(){
        return this.mode == null || "".equals(this.mode) || "0".equals(this.mode);
    }
    /**
     * 現在が面談モードかどうか
     * @return
     */
    public boolean isMendanMode(){
        return !isBunsekiMode();
    }

    /**
     * @return
     */
    public static String getSESSION_KEY() {
        return SESSION_KEY;
    }

    /**
     * @return
     */
    public ExamComboData getExamComboData() {
        return examComboData;
    }

    /**
     * @return
     */
    public String getMode() {
        return mode;
    }

    /**
     * @return
     */
    public List getSelectedIndividuals() {

        // 分析モード
        if (isBunsekiMode()) {
            return selectedIndividuals;

        // 面談モード
        // 面談している生徒だけ返す
        } else {
            final List list = new ArrayList(1);
            list.add(getTargetPerson());
            return list;
        }
    }

    /**
     * @return 画面で選択中の生徒
     */
    public StudentData getTargetPerson() {

        for (Iterator ite = selectedIndividuals.iterator(); ite.hasNext();) {
            final StudentData data = (StudentData) ite.next();
            if (data.getStudentPersonId().equals(getTargetPersonId())) {
                return data;
            }
        }

        return null;
    }

    /**
     * @return
     */
    public String getTargetExamCode() {
        return targetExamCode;
    }

    /**
     * @return
     */
    public String getTargetExamName() {
        return targetExamName;
    }

    /**
     * @return
     */
    public String getTargetExamTypeCode() {
        return targetExamTypeCode;
    }

    /**
     * @return
     */
    public String getTargetExamYear() {
        return targetExamYear;
    }

    /**
     * @return
     */
    public String getTargetPersonId() {
        return targetPersonId;
    }

    /**
     * @param data
     */
    public void setExamComboData(ExamComboData data) {
        examComboData = data;
    }

    /**
     * @param string
     */
    public void setMode(String string) {
        mode = string;
    }

    /**
     * @param list
     */
    public void setSelectedIndividuals(List list) {
        selectedIndividuals = list;
    }

    /**
     * @param string
     */
    public void setTargetExamCode(String pTargetExamCode) {
        if (pTargetExamCode != null) {
            targetExamCode = pTargetExamCode;
        }
     }

    /**
     * @param string
     */
    public void setTargetExamName(String pTargetExamName) {
        if (pTargetExamName != null) {
            targetExamName = pTargetExamName;
        }
    }

    /**
     * @param string
     */
    public void setTargetExamTypeCode(String string) {
            targetExamTypeCode = string;
    }

    /**
     * @param string
     */
    public void setTargetExamYear(String pTargetExamYear) {
        if (pTargetExamYear != null) {
            targetExamYear = pTargetExamYear;
        }
    }

    /**
     * @param string
     */
    public void setTargetPersonId(String string) {
        targetPersonId = string;
    }

    /**
     * @return
     */
    public String[] getDispClasses() {
        return dispClasses;//[1] add
    }

    /**
     * @return
     */
    public List getDispStudents() {
        return dispStudents;//[1] add
    }

    /**
     * @param strings
     */
    public void setDispClasses(String[] strings) {
        dispClasses = strings;//[1] add
    }

    /**
     * @param list
     */
    public void setDispStudents(List list) {
        dispStudents = list;//[1] add
    }

    public String getDispScholarLevel() {
        return dispScholarLevel;
    }

    public void setDispScholarLevel(String string) {
        this.dispScholarLevel = string;
    }

    /**
     * 「設問別成績」用の学力レベル保持データを返す
     * @return
     */
    public HashMap getIndSubScholarLevels() {
        return indSubScholarLevels;
    }

    /**
     * 「設問別成績」用の学力レベル保持データを設定する
     * @param indSubScholarLevels
     */
    public void setIndSubScholarLevels(HashMap indSubScholarLevels) {
        this.indSubScholarLevels = indSubScholarLevels;
    }

    /**
     * 「設問別成績（過回模試）」用の学力レベル保持データを返す
     * @return
     */
    public HashMap getIndSubScholarLevels109() {
        return indSubScholarLevels109;
    }

    /**
     * 「設問別成績（過回模試）」用の学力レベル保持データを設定する
     * @return
     */
    public void setIndSubScholarLevels109(HashMap data) {
        this.indSubScholarLevels109 = data;
    }

    /**
     * 画面で指定した学力レベルを取得するためのKeyを作成する。
     * @param examyear 模試年度
     * @param examcd 模試コード
     * @param indid 個人ID
     * @param subcd 科目コード
     * @return examyear + examcd + indid + subcd
     */
    public String makeScholarLevelMapKey(String examyear, String examcd, String indid, String subcd) {
        return examyear + examcd + indid + subcd;
    }

    public String getDispSubCd() {
        return dispSubCd;
    }

    public void setDispSubCd(String dispSubCd) {
        this.dispSubCd = dispSubCd;
    }

    public String getTargetGrade() {
        return targetGrade;
    }

    public void setTargetGrade(String targetGrade) {
        this.targetGrade = targetGrade;
    }

    public String[] getInterviewSheet() {
        return interviewSheet;
    }

    public void setInterviewSheet(String[] interviewSheet) {
        this.interviewSheet = interviewSheet;
    }

    // 2020/04/07 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD START
    public String getPrintStamp() {
        return printStamp;
    }

    public void setPrintStamp(String printStamp) {
        this.printStamp = printStamp;
    }

    public String getPrintStudent() {
        return printStudent;
    }

    public void setPrintStudent(String printStudent) {
        this.printStudent = printStudent;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String[] getInterviewForms() {
        return interviewForms;
    }

    public void setInterviewForms(String[] interviewForms) {
        this.interviewForms = interviewForms;
    }
    // 2020/04/07 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD END

}

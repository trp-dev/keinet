package jp.co.fj.keinavi.util.log;

import java.util.logging.*;
import java.io.*;

import jp.co.fj.keinavi.util.*;

/**
 *
 * アクセスログをファイルに出力するクラス
 *
 * @Date		2004/06/09
 * @author	TOTEC)Nishiyama
 */
public class AccessLogWriter extends KNLogWriter {

	/**
	 * アクセスログの名前取得
	 * 
	 * @return ログの名前
	 */
	public String getLogName() { return "Access"; }
	
	/**
	 * ログのファイルサイズ上限値を取得する。
	 * 
	 * @return ログのファイルサイズ上限値
	 */
	protected int getLogLimit() {
		int size = 10 * 1024 * 1024;
		try{
			size = KNCommonProperty.getAccLogFileSizeLimit();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return size;
	}
	
	/**
	 * ログのファイル履歴の上限値を取得する。
	 * 
	 * @return ログのファイル履歴上限値
	 */
	protected int getLogCount() {
		int count = 10;
		try{
			count = KNCommonProperty.getAccLogCountLimit();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	/**
	 * ログ出力オブジェクトをインスタンス化する
	 * 
	 * @return ログのインスタンス
	 */
	public static KNLogWriter factory() {

		try {
			KNLogWriter me = new AccessLogWriter();
			me.initLog();
			return me;
		}	catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * コンストラクタ
	 */
	public AccessLogWriter() {}

	/**
	 *
	 * アクセスログファイルへの出力
	 *
	 * @param rec ログレコードクラス
	 */
	protected void output(LogRecord rec) throws Exception {

		Logger logger = getLogger();
		rec.setLoggerName(getLogName());
		logger.log(rec);
	}

	/**
	 *
	 * 出力ファイル名の取得
	 *
	 * @return 出力ファイル名
	 */
	protected String getLogFileName() {
		String FilePath = getLogFilePath();
		return new File(FilePath, "access_%g-%u.log").getAbsolutePath();
	}

	/**
	 *
	 * 出力ファイルのパスの取得
	 *
	 * @return 出力ファイルパス
	 */
	protected String getLogFilePath() {
// ファイルセパレータの記述を環境依存しない形に修正
// 2004.09.21 Yoshimoto KAWAI - Totec

		// ファイルパスを返す
		String FilePath = getKNLOG();
		if (FilePath == null) {
//			FilePath = "%h\\";
			FilePath = "%h" + File.separator;
		}	else {
//			File target = new File(FilePath, "Log\\ALog\\");
			File target = new File(FilePath, "Log" + File.separator + "ALog" + File.separator);
			if ( (! target.exists()) || (! target.isDirectory()) ) {
//				FilePath = "%h\\";
				FilePath = "%h" + File.separator;
			} else {
				FilePath = target.getAbsolutePath();
			}
		}
		return FilePath;
	}
}

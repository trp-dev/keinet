/*
 * 作成日: 2004/09/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.helpdesk;

import java.io.Serializable;

/**
 * @author A.Ninomiya
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class UserList implements Serializable  {

	String      schoolCode      = null;     	// 学校コード
	String      schoolName      = null;     	// 学校名
	String      userId          = null;     	// ユーザID
	String      pactDivNm       = null;     	// 契約区分名
	String      userDivNm       = null;     	// ユーザ区分名
	String      renewalDate     = null;     	// 更新日
	String      pactTerm        = null;     	// 契約期限
	String      cert_BeforeDate = null;     	// 証明書有効期限
	String      issued_BeforeDate = null;   	// 発行済証明書有効期限
	String      changeBackColor = null;     	// 背景色変更区分
												//   0:ノーマル
												//   1:証明書未発行
												//   2:有効期限切れ
												//   3:証明書有効期限不一致

	// ---------------------------------------------
	/**
	 * @return
	 */
	public String getSchoolCode() {
		return schoolCode;
	}

	/**
	 * @param string
	 */
	public void setSchoolCode(String string) {
		schoolCode = string;
	}
	// ---------------------------------------------
	/**
	 * @return
	 */
	public String getSchoolName() {
		return schoolName;
	}

	/**
	 * @param string
	 */
	public void setSchoolName(String string) {
		schoolName = string;
	}
	// ---------------------------------------------
	/**
	 * @return
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}
	// ---------------------------------------------
	/**
	 * @return
	 */
	public String getUserDivNm() {
		return userDivNm;
	}

	/**
	 * @param string
	 */
	public void setUserDivNm(String string) {
		userDivNm = string;
	}
	// ---------------------------------------------
	/**
	 * @return
	 */
	public String getPactDivNm() {
		return pactDivNm;
	}

	/**
	 * @param string
	 */
	public void setPactDivNm(String string) {
		pactDivNm = string;
	}
	// ---------------------------------------------
	/**
	 * @return
	 */
	public String getRenewalDate() {
		return renewalDate;
	}

	/**
	 * @param string
	 */
	public void setRenewalDate(String string) {
		renewalDate = string;
	}
	// ---------------------------------------------
	/**
	 * @return
	 */
	public String getPactTerm() {
		return pactTerm;
	}

	/**
	 * @param string
	 */
	public void setPactTerm(String string) {
		pactTerm = string;
	}
	// ---------------------------------------------
	/**
	 * @return
	 */
	public String getCert_BeforeDate() {
		return cert_BeforeDate;
	}

	/**
	 * @param string
	 */
	public void setCert_BeforeDate(String string) {
		cert_BeforeDate = string;
	}
	// ---------------------------------------------
	/**
	 * @return
	 */
	public String getIssued_BeforeDate() {
		return issued_BeforeDate;
	}

	/**
	 * @param string
	 */
	public void setIssued_BeforeDate(String string) {
		issued_BeforeDate = string;
	}
	// ---------------------------------------------
	/**
	 * @return
	 */
	public String getChangeBackColor() {
		return changeBackColor;
	}

	/**
	 * @param string
	 */
	public void setChangeBackColor(String string) {
		changeBackColor = string;
	}
	// ---------------------------------------------
	

}

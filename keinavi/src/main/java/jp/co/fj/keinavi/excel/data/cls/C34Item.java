package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス比較−志望大学評価別人数データクラス
 * 作成日: 2004/07/16
 * @author	H.Fujimoto
 */
public class C34Item {
	//学校名
	private String strGakkomei = "";
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//模試コード
	private String strMshCd = "";
	//セキュリティスタンプ
	private int intSecuFlg = 0;
	//大学集計区分フラグ
	private int intDaiTotalFlg = 0;
	//データリスト
	private ArrayList c34List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;


	/*----------*/
	/* Get      */
	/*----------*/

	public ArrayList getC34List() {
		return this.c34List;
	}
	public int getIntDaiTotalFlg() {
		return this.intDaiTotalFlg;
	}
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrMshCd() {
		return this.strMshCd;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setC34List(ArrayList c34List) {
		this.c34List = c34List;
	}
	public void setIntDaiTotalFlg(int intDaiTotalFlg) {
		this.intDaiTotalFlg = intDaiTotalFlg;
	}
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrMshCd(String strMshCd) {
		this.strMshCd = strMshCd;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}
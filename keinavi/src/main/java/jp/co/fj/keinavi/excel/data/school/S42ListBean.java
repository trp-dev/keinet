package jp.co.fj.keinavi.excel.data.school;

//import org.apache.struts.action.ActionForm;
import java.util.ArrayList;
/**
 * �Z�����ѕ��́|���Z��r�|�΍��l���z �f�[�^���X�g
 * �쐬��: 2004/07/19
 * @author	A.Iwata
 */
public class S42ListBean {
	//�^�E�Ȗږ��R�[�h
	private String strKmkCd = "";
	//�^�E�Ȗږ�
	private String strKmkmei = "";
	//�^��Ȗڗp�O���t�\���t���O
	private int intDispKmkFlg = 0;
	//�z�_
	private String strHaitenKmk = "";
	//�w�Z���X�g
	private ArrayList s42GakkoList = new ArrayList();

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}
	public int getIntDispKmkFlg() {
		return this.intDispKmkFlg;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public ArrayList getS42GakkoList() {
		return this.s42GakkoList;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setIntDispKmkFlg(int intDispKmkFlg) {
		this.intDispKmkFlg = intDispKmkFlg;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setS42GakkoList(ArrayList s42GakkoList) {
		this.s42GakkoList = s42GakkoList;
	}
}

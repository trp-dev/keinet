package jp.co.fj.keinavi.beans.sheet.user;

import jp.co.fj.keinavi.beans.sheet.common.AbstractExtSheetBean;
import jp.co.fj.keinavi.beans.sheet.excel.user.U01_01ExcelCreator;
import jp.co.fj.keinavi.beans.sheet.user.data.U01_01Data;
import jp.co.fj.keinavi.beans.user.LoginUserBean;
import jp.co.fj.keinavi.forms.user.U001Form;

/**
 *
 * 利用者登録通知SheetBean
 * 
 * 2005.11.01	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U001SheetBean extends AbstractExtSheetBean {

	// 帳票データ
	private final U01_01Data data = new U01_01Data();
	
	// ログインユーザBean
	private LoginUserBean bean;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		bean = new LoginUserBean(login.getUserID());
		bean.setConnection(null, conn);
		
		// セッションからアクションフォームを取得する
		final U001Form form = (U001Form) session.getAttribute("U001Form");
		final String[] loginId = form.getLoginId();
		// もう使わないのでセッションから消す
		session.removeAttribute("U001Form");
	
		for (int i = 0; i < loginId.length; i++) {
			bean.setLoginId(loginId[i]);
			bean.getLoginUserList().clear();
			bean.execute();
			data.getU01_01ListData().addAll(bean.getLoginUserList());
		}
	}
	
	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.AbstractExtSheetBean#create()
	 */
	protected void create() throws Exception {
		
		new U01_01ExcelCreator(
				data, sessionKey, outfileList, getAction()).execute(); 
		sheetLog.add("U01_01");
	}

}

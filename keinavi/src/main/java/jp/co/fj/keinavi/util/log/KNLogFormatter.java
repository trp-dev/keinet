package jp.co.fj.keinavi.util.log;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

import java.lang.String;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;
import java.util.Date;

/**
 *
 * KNログフォーマットクラス
 *
 * @Date		2004/06/09
 * @author	TOTEC)Nishiyama
 */
public class KNLogFormatter extends Formatter {

	/**
	 * フォーマットパターン
	 */
	private final static DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
	private final static DecimalFormat tf = new DecimalFormat("00000000");

	/**
	 *
	 * ログレコードをフォーマットする
	 *
	 * @param rec ログレコード
	 * @return フォーマットされた文字列
	 */
	public String format(LogRecord rec) {

		StringBuffer msg = new StringBuffer(1024);
		msg.append(tf.format(rec.getThreadID()));
		msg.append(" ");
		msg.append(df.format(new Date(rec.getMillis())));
		msg.append(" ");
		msg.append(formatMessage(rec));
		msg.append("\n");
		return msg.toString();
	}
}
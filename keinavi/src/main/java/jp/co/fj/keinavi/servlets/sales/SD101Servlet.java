package jp.co.fj.keinavi.servlets.sales;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.forms.sales.SD101Form;
import jp.co.fj.keinavi.beans.UploadFileBean;

/**
 *
 * ファイルダウンロード画面サーブレット
 * 
 * <2010年度マーク高２模試対応>
 * 2011.01.24	Tomohisa YAMADA
 * 				新規作成
 * 
 */
public class SD101Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * #execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		// アクションフォーム
		SD101Form form = (SD101Form) super.getActionForm(request,
				"jp.co.fj.keinavi.forms.sales.SD101Form");
		
		if("sd101".equals(getForward(request))) {

			Connection con = null; // コネクション
			try {
				con = super.getConnectionPool(request);
				
				//ダウンロード
				if ("1".equals(form.getActionMode())) {
					//エラー
					if (form.getErrorCount() > 0) {
						display(request, response, con, form);
					}
					else {
						try {
							con.setAutoCommit(false);
							download(response, con, form);
							con.commit();
						} catch (Exception e) {
							con.rollback();
							throw e;
						}
					}
				} 
				//画面表示
				else {
					display(request, response, con, form);
				}

			} catch (Exception e) {
				e.printStackTrace();
				throw new ServletException(e.toString());
			} finally {
				super.releaseConnectionPool(request, con);
			}	
		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}
	
	/**
	 * ダウンロードの実行
	 * @param response
	 * @param con
	 * @param form
	 * @throws Exception
	 */
	private void download(
			HttpServletResponse response, Connection con, SD101Form form) throws Exception {
		UploadFileBean ufb = new UploadFileBean(form.getZipFileName(), form.getUploadFileID());
		ufb.setConnection(null, con);
		ufb.download(response);
	}
	
	private void display(HttpServletRequest request,
			HttpServletResponse response, Connection con, SD101Form form) throws Exception {
		
		UploadFileBean ufb = new UploadFileBean(form.getSalesCode());
		ufb.setConnection(null, con);
		ufb.searchSafeByBusiness(Integer.parseInt(form.getSortIndex()));

		request.setAttribute("downloadFileBean", ufb);
		request.setAttribute("form", form);
		
		super.forward(request, response, JSP_SD101);
	}

}

package jp.co.fj.keinavi.util.log;

import java.util.logging.*;

/**
 * エラーログをファイルに出力するクラス
 *
 * 2005.02.23 Yoshimoto KAWAI - Totec
 *            利用者ＩＤの出力に対応
 * 
 * @Date		2004/06/09
 * @author	TOTEC)Nishiyama
 */
public class ErrorLog extends Log {

	/**
	 * ログ出力オブジェクト
	 */
	protected static ErrorLogWriter elog = (ErrorLogWriter) ErrorLogWriter.factory();

	/**
	 * エラー種別
	 */
	public static final String LEVEL_INF = "INF";
	public static final String LEVEL_WAR = "WAR";
	public static final String LEVEL_ERR = "ERR";
		
	/**
	 * INFログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	protected static void Inf(String IPAddress,
							String ContractID,
							String EmployeeID,
							String account,
							String ErrCode,
							String ErrContents,
							String SupplementInfo) {

		LogRecord rec = new LogRecord(Level.INFO, "");
		setCallStack2Record(rec, Depth);
		_ErrLog(rec, ErrorLog.LEVEL_INF, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * INFログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param Supplement			補足情報
	 */
	protected static void Inf(String IPAddress,
							String ContractID,
							String EmployeeID,
							String account,
							String ErrCode,
							String ErrContents,
							Throwable Supplement) {

		LogRecord rec = new LogRecord(Level.INFO, "");
		setCallStack2Record(rec, Depth);
		_ErrLog(rec, ErrorLog.LEVEL_INF, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, Supplement);
	}

	/**
	 * WARログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	protected static void War(String IPAddress,
							String ContractID,
							String EmployeeID,
							String account,
							String ErrCode,
							String ErrContents,
							String SupplementInfo) {

		LogRecord rec = new LogRecord(Level.WARNING, "");
		setCallStack2Record(rec, Depth);
		_ErrLog(rec, ErrorLog.LEVEL_WAR, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * WARログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param Supplement			補足情報
	 */
	protected static void War(String IPAddress,
							String ContractID,
							String EmployeeID,
							String account,
							String ErrCode,
							String ErrContents,
							Throwable Supplement) {

		LogRecord rec = new LogRecord(Level.WARNING, "");
		setCallStack2Record(rec, Depth);
		_ErrLog(rec, ErrorLog.LEVEL_WAR, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, Supplement);
	}

	/**
	 * ERRログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	protected static void Err(String IPAddress,
							String ContractID,
							String EmployeeID,
							String account,
							String ErrCode,
							String ErrContents,
							String SupplementInfo) {

		LogRecord rec = new LogRecord(Level.SEVERE, "");
		setCallStack2Record(rec, Depth);
		_ErrLog(rec, ErrorLog.LEVEL_ERR, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * ERRログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param Supplement			補足情報
	 */
	protected static void Err(String IPAddress,
							String ContractID,
							String EmployeeID,
							String account,
							String ErrCode,
							String ErrContents,
							Throwable Supplement) {

		LogRecord rec = new LogRecord(Level.SEVERE, "");
		setCallStack2Record(rec, Depth);
		_ErrLog(rec, ErrorLog.LEVEL_ERR, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, Supplement);
	}

	/**
	 *
	 * エラーログを出力する(パターン1)
	 *
	 * @param rec						ログレコードクラス
	 * @param Mode						エラー種別
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	private static void _ErrLog(LogRecord rec,
								String Mode,
								String IPAddress,
								String ContractID,
								String EmployeeID,
								String account,
								String ErrCode,
								String ErrContents,
								String SupplementInfo) {
		try {
			__ErrLog(rec, Mode, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, SupplementInfo);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 *
	 * エラーログを出力する(パターン2)
	 *
	 * @param rec						ログレコードクラス
	 * @param Mode						エラー種別
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param Supplement			補足情報
	 */
	private static void _ErrLog(LogRecord rec,
								String Mode,
								String IPAddress,
								String ContractID,
								String EmployeeID,
								String account,
								String ErrCode,
								String ErrContents,
								Throwable Supplement) {
		try {
			// ログレコードに、例外を設定する。
			rec.setThrown(Supplement);
			
			// スタックトレースを取得する。
			String note = getStack(Supplement);

			__ErrLog(rec, Mode, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, note);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * エラーログを出力する
	 *
	 * @param rec						ログレコードクラス
	 * @param Mode						エラー種別
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	private static void __ErrLog(LogRecord rec,
								String Mode,
								String IPAddress,
								String ContractID,
								String EmployeeID,
								String account,
								String ErrCode,
								String ErrContents,
								String SupplementInfo) throws Exception {

		// エラーログを出力する
		String msg = Mode + " " +
					convert(IPAddress) + " " +
					convert(ContractID) + " " +
					convert(EmployeeID) + " " +
					convert(account) + " " +
					rec.getSourceClassName() + "." + rec.getSourceMethodName() + ": " +
					/*IntToHex8(*/convert(ErrCode)/*)*/ + " " +
					convert(ErrContents) + " " +
					convert(SupplementInfo);
		
		rec.setMessage(msg);
		elog.LogOutput(rec);
		
		// デバッグログを出力する。
		DebugLog.__DebugLog(rec);
	}
}
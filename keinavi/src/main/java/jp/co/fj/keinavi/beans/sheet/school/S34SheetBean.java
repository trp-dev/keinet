package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.excel.data.school.S34ClassListBean;
import jp.co.fj.keinavi.excel.data.school.S34HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.school.S34Item;
import jp.co.fj.keinavi.excel.data.school.S34ListBean;
import jp.co.fj.keinavi.excel.school.S34;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 校内成績分析 - クラス比較（志望大学評価別人数）
 *
 * 2004.08.04	[新規作成]
 *
 * 2005.04.05	Yoshimoto KAWAI - TOTEC
 * 				比較対象年度を過年度の表示から取得するように変更
 *
 * 2005.04.06	Yoshimoto KAWAI - TOTEC
 * 				出力種別フラグ対応
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class S34SheetBean extends AbstractSheetBean {

	// データクラス
	private final S34Item data = new S34Item();

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// 比較対象年度
		final String[] years = getUnivYearArray();
		// 大学集計区分
		final String univCountingDiv = getUnivCountngDiv();

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setStrMshCd(exam.getExamCD()); // 模試コード
		data.setIntDaiTotalFlg(getDaiTotalFlg()); // 大学集計区分
		data.setIntSecuFlg(getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(getOutputType()); // 出力種別フラグ

		// ワークテーブルのセットアップ
		insertIntoCountUniv();
		insertIntoExistsExam();
		insertIntoCountCompClass();
		insertIntoExamCdTrans();

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4a = null;
		PreparedStatement ps4b = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		ResultSet rs4 = null;
		try {
			// データリスト
			ps1 = conn.prepareStatement(getRatingQuery().toString());
			ps1.setString(1, exam.getExamYear()); // 模試年度
			ps1.setString(2, exam.getExamCD()); // 模試コード
			ps1.setString(3, profile.getBundleCD()); // 一括コード
			ps1.setString(4, univCountingDiv); // 大学集計区分
			ps1.setString(5, exam.getExamDiv()); // 模試区分

			// 評価別人数データリスト（自校）
			{
				final Query query = QueryLoader.getInstance().load("s34_1");
				query.setStringArray(1, years); // 比較対象年度

				ps2 = conn.prepareStatement(query.toString());
				ps2.setString(1, profile.getBundleCD()); // 一括コード
				ps2.setString(3, univCountingDiv); // 大学集計区分
				ps2.setString(9, exam.getExamCD()); // 模試コード

			}

			// 比較対象クラス
			ps3 = conn.prepareStatement(QueryLoader.getInstance().load("s34_2").toString());

			// 評価別人数データリスト（単体クラス）
			{
				final Query query = QueryLoader.getInstance().load("s34_3_1");
				query.setStringArray(1, years); // 比較対象年度

				ps4a = conn.prepareStatement(query.toString());
				ps4a.setString(1, profile.getBundleCD()); // 一括コード
				ps4a.setString(3, univCountingDiv); // 大学集計区分
				ps4a.setString(11, exam.getExamCD()); // 模試コード
			}

			// 評価別人数データリスト（複合クラス）
			{
				final Query query = QueryLoader.getInstance().load("s34_3_2");
				query.setStringArray(1, years); // 比較対象年度

				ps4b = conn.prepareStatement(query.toString());
				ps4b.setString(2, profile.getBundleCD()); // 一括コード
				ps4b.setString(4, univCountingDiv); // 大学集計区分
				ps4b.setString(10, exam.getExamCD()); // 模試コード
			}

			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				S34ListBean bean = new S34ListBean();
				bean.setStrGenKouKbn(rs1.getString(1));
				bean.setStrDaigakuCd(rs1.getString(2));
				bean.setStrDaigakuMei(rs1.getString(3));
				bean.setStrGakubuCd(rs1.getString(4));
				bean.setStrGakubuMei(rs1.getString(5));
				bean.setStrGakkaCd(rs1.getString(6));
				bean.setStrGakkaMei(rs1.getString(7));
				bean.setStrNtiCd(rs1.getString(8));
				bean.setStrNittei(rs1.getString(9));
				bean.setStrHyouka(rs1.getString(10));
				data.getS34List().add(bean);

				// 現役高卒区分
				ps2.setString(2, rs1.getString(1));
				ps4a.setString(2, rs1.getString(1));
				ps4b.setString(3, rs1.getString(1));
				// 大学コード
				ps2.setString(4, rs1.getString(2));
				ps4a.setString(4, rs1.getString(2));
				ps4b.setString(5, rs1.getString(2));
				// 学部コード
				ps2.setString(5, rs1.getString(4));
				ps4a.setString(5, rs1.getString(4));
				ps4b.setString(6, rs1.getString(4));
				// 学科コード
				ps2.setString(6, rs1.getString(6));
				ps4a.setString(6, rs1.getString(6));
				ps4b.setString(7, rs1.getString(6));
				// 日程コード
				ps2.setString(7, rs1.getString(8));
				ps4a.setString(7, rs1.getString(8));
				ps4b.setString(8, rs1.getString(8));
				// 評価区分
				ps2.setString(8, rs1.getString(10));
				ps4a.setString(8, rs1.getString(10));
				ps4b.setString(9, rs1.getString(10));

				// 自校
				{
					S34ClassListBean c = new S34ClassListBean();
					c.setStrGakkomei(profile.getBundleName());

					rs2 = ps2.executeQuery();
					while (rs2.next()) {
						S34HyoukaNinzuListBean n = new S34HyoukaNinzuListBean();
						n.setStrNendo(rs2.getString(1));
						n.setIntSoushibo(rs2.getInt(2));
						n.setIntDai1shibo(rs2.getInt(3));
						n.setIntHyoukaA(rs2.getInt(4));
						n.setIntHyoukaB(rs2.getInt(5));
						n.setIntHyoukaC(rs2.getInt(6));

						// 2019/11/19 QQ)Ooseto 英語認定試験延期対応 DEL START
						// 2019/09/30 QQ) 共通テスト対応 ADD START
						//n.setIntHyoukaA_Hukumu(rs2.getInt(7));
						//n.setIntHyoukaB_Hukumu(rs2.getInt(8));
						//n.setIntHyoukaC_Hukumu(rs2.getInt(9));
//                                                n.setIntHyoukaA_Hukumu(rs2.getInt(9));
//                                                n.setIntHyoukaB_Hukumu(rs2.getInt(10));
//                                                n.setIntHyoukaC_Hukumu(rs2.getInt(11));
						// 2019/09/30 QQ) 共通テスト対応 ADD END
                        // 2019/11/19 QQ)Ooseto 英語認定試験延期対応 DEL END

						c.getS34HyoukaNinzuList().add(n);
					}
					rs2.close();

					bean.getS34ClassList().add(c);
				}

				// クラス
				{
					rs3 = ps3.executeQuery();
					while (rs3.next()) {
						S34ClassListBean c = new S34ClassListBean();
						c.setStrGrade(rs3.getString(1));
						c.setStrClass(rs3.getString(3));
						bean.getS34ClassList().add(c);

						String classCode = rs3.getString(2);

						// 複合クラス
						if (classCode.length() == 7) {
							ps4b.setString(1, classCode);
							rs4 = ps4b.executeQuery();
						// 単体クラス
						} else {
							ps4a.setInt(9, rs3.getInt(1));
							ps4a.setString(10, classCode);
							rs4 = ps4a.executeQuery();
						}

						while (rs4.next())	{
							S34HyoukaNinzuListBean n = new S34HyoukaNinzuListBean();
							n.setStrNendo(rs4.getString(1));
							n.setIntSoushibo(rs4.getInt(2));
							n.setIntDai1shibo(rs4.getInt(3));
							n.setIntHyoukaA(rs4.getInt(4));
							n.setIntHyoukaB(rs4.getInt(5));
							n.setIntHyoukaC(rs4.getInt(6));

							// 2019/11/19 QQ)Ooseto 英語認定試験延期対応 DEL START
							// 2019/09/30 QQ) 共通テスト対応 ADD START
							//n.setIntHyoukaA_Hukumu(rs4.getInt(7));
							//n.setIntHyoukaB_Hukumu(rs4.getInt(8));
							//n.setIntHyoukaC_Hukumu(rs4.getInt(9));
//                                                        n.setIntHyoukaA_Hukumu(rs4.getInt(9));
//                                                        n.setIntHyoukaB_Hukumu(rs4.getInt(10));
//                                                        n.setIntHyoukaC_Hukumu(rs4.getInt(11));
							// 2019/09/30 QQ) 共通テスト対応 ADD END
                            // 2019/11/19 QQ)Ooseto 英語認定試験延期対応 DEL END

							c.getS34HyoukaNinzuList().add(n);
						}
						rs4.close();
					}
					rs3.close();
				}
			}
		} finally {
			DbUtils.closeQuietly(null, ps1, rs1);
			DbUtils.closeQuietly(null, ps2, rs2);
			DbUtils.closeQuietly(null, ps3, rs3);
			DbUtils.closeQuietly(rs4);
			DbUtils.closeQuietly(ps4a);
			DbUtils.closeQuietly(ps4b);
		}

		KNLog.Ep(null, null, null, null, "S34", "データクラス作成完了", "");
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S34_01");
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_CLASS_UNIV;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S34().s34(data, outfileList, getAction(), sessionKey, sheetLog);
	}

}

/*
 * 作成日: 2004/09/16
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.help;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.help.HelpList;
import jp.co.fj.keinavi.util.KNCommonProperty;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultSearchBean;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class HelpSearchBean extends DefaultSearchBean {

	private String freeWord = null; 	// フリーワード
	private String category = null;	// カテゴリ

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// TODO 自動生成されたメソッド・スタブ

		// SQLをセットアップ
		StringBuffer query = new StringBuffer();
		query.append("SELECT *    ");
		query.append(" FROM HELP ");
		// ------------------------------------------------------------
		// 2004.11.21
		// Yoshimoto KAWAI - Totec
		// ヘルプ本文で検索する
		query.append(" WHERE TITLEMEI LIKE '%' || ? || '%' " );
		//query.append(" WHERE TO_CHAR(TEXT) LIKE '%' || ? || '%' " );
		// ------------------------------------------------------------
		query.append(" ORDER BY RENEWALDATE DESC " );

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query.toString());
			ps.setString(1, this.freeWord);

			rs = ps.executeQuery();
			
			getPageNo();
			getEndCount();
			while (rs.next()) {
				HelpList list = new HelpList();
				list.setHelpId(rs.getString(1));
				list.setTitleStr(rs.getString(2));
				list.setCategory(rs.getString(3));
				list.setDispSequence(rs.getString(4));
				list.setUpdate(rs.getString(5));
				list.setExplan(getExplanLimit(rs.getString(6)));
				recordSet.add(list);
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}


	}


	/**
	 * 文字列からタグを外し設定ファイルで指定された
	 * 文字列数分の文字列を取得
	 *
	 ** @return  指定文字列数分の文字列
	 */
	public String getExplanLimit(String str) {
		String  helpString = null;
		int  helpCharLimit = 0;
		try {
			helpCharLimit = KNCommonProperty.getHelpCharLimit();
		} catch (Exception e) {
			new Exception(e);
		}
		// 文字列からタグを外す
		helpString = str.replaceAll("<.+?>", "");
		if (helpString.length() > helpCharLimit) {
			helpString = helpString.substring(0, helpCharLimit);
		}
		return helpString;
	}

	/**
	* フリーワード
	* @return
	*/
	public String getFreeWord() {
		return freeWord;
	}

	/**
	 * @param string
	 */
	public void setFreeWord(String string) {
		freeWord = string;
	}

	/**
	 * @return
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param string
	 */
	public void setCategory(String string) {
		category = string;
	}


}

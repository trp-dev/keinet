/*
 * 作成日: 2004/10/05
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.admin.name;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;

import org.apache.commons.dbutils.DbUtils;


/**
 * 名寄せ用DBアクセスクラス
 *
 *
 * <新過程対応>
 * 2013.04.23   森田雄一郎 - Totec
 *              insDualRec,updDualRec,SelWork
 *
 * @author 奥村ゆかり
 */
public class NameBringsTableAccess {

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 科目別成績の削除
     *
     * @param con DBコネクション
     * @param 個人ID studentId 年度 year 模試コード examCd 科目コード subCd
     * @return void
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void DeleteSubRecord_I(Connection con, String studentId, String year, String examCd) {
        try {
            PreparedStatement ps =
                (PreparedStatement) NameBringsBean.STATEMENT.get("DEL_SUBRECORD_I");

            if (ps == null) {
                String query =
                    "DELETE SUBRECORD_I WHERE INDIVIDUALID=? AND EXAMYEAR=? AND EXAMCD=?";

                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("DEL_SUBRECORD_I", ps);
            }

            ps.setString(1, studentId);
            ps.setString(2, year);
            ps.setString(3, examCd);
            ps.executeUpdate();

        } catch (Exception e) {
            SystemLogWriter.printLog(
                NameBringsTableAccess.class,
                e.getMessage(),
                SystemLogWriter.WARNING
            );
        }
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 設問別成績の削除
     *
     * @param con DBコネクション
     * @param 個人ID studentId 年度 year 模試コード examCd 科目コード subCd
     * @return void
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void DeleteQRecord_I(Connection con, String studentId, String year, String examCd) {

        try {
            PreparedStatement ps =
                (PreparedStatement) NameBringsBean.STATEMENT.get("DEL_QRECORD_I");

            if (ps == null) {
                String query =
                    "DELETE QRECORD_I"+
                    " WHERE INDIVIDUALID=? AND EXAMYEAR=? AND EXAMCD=?";
                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("DEL_QRECORD_I", ps);
            }
            ps.setString(1, studentId);
            ps.setString(2, year);
            ps.setString(3, examCd);
            ps.executeUpdate();

        } catch (Exception e) {
            SystemLogWriter.printLog(
                NameBringsTableAccess.class,
                e.getMessage(),
                SystemLogWriter.WARNING
            );
        }
    }


    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 志望校評価の削除
     *
     * @param con DBコネクション
     * @param 個人ID studentId 年度 year 模試コード examCd 科目コード subCd
     * @return void
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void DeleteCandiDate(Connection con, String studentId, String year, String examCd) {

        try {
            PreparedStatement ps =
                (PreparedStatement) NameBringsBean.STATEMENT.get("DEL_CANDIDATERATING");

            if (ps == null) {
                String query =
                    "DELETE CANDIDATERATING "+
                    "WHERE INDIVIDUALID=? AND EXAMYEAR=? "+
                    "AND EXAMCD=?";

                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("DEL_CANDIDATERATING", ps);
            }

            ps.setString(1, studentId);
            ps.setString(2, year);
            ps.setString(3, examCd);
            ps.executeUpdate();
        } catch (Exception e) {
            SystemLogWriter.printLog(
                NameBringsTableAccess.class,
                e.getMessage(),
                SystemLogWriter.WARNING
            );
        }
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 科目別成績（P保護）の削除
     *
     * @param con DBコネクション
     * @param 個人ID studentId 年度 year 模試コード examCd 科目コード subCd
     * @return void
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void DeleteSubRecord_PP(Connection con, String answerSheetNo, String year, String examCd) {
        try {
            PreparedStatement ps =
                (PreparedStatement) NameBringsBean.STATEMENT.get("DEL_SUBRECORD_PP");

            if (ps == null) {
                String query =
                    "DELETE SUBRECORD_PP WHERE ANSWERSHEET_NO=? AND EXAMYEAR=? AND EXAMCD=?";

                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("DEL_SUBRECORD_PP", ps);
            }

            ps.setString(1, answerSheetNo);
            ps.setString(2, year);
            ps.setString(3, examCd);
            ps.executeUpdate();

        } catch (Exception e) {
            SystemLogWriter.printLog(
                NameBringsTableAccess.class,
                e.getMessage(),
                SystemLogWriter.WARNING
            );
        }
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 設問別成績（P保護）の削除
     *
     * @param con DBコネクション
     * @param 個人ID studentId 年度 year 模試コード examCd 科目コード subCd
     * @return void
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void DeleteQRecord_PP(Connection con, String answerSheetNo, String year, String examCd) {

        try {
            PreparedStatement ps =
                (PreparedStatement) NameBringsBean.STATEMENT.get("DEL_QRECORD_PP");

            if (ps == null) {
                String query =
                    "DELETE QRECORD_PP"+
                    " WHERE ANSWERSHEET_NO=? AND EXAMYEAR=? AND EXAMCD=?";
                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("DEL_QRECORD_PP", ps);
            }
            ps.setString(1, answerSheetNo);
            ps.setString(2, year);
            ps.setString(3, examCd);
            ps.executeUpdate();

        } catch (Exception e) {
            SystemLogWriter.printLog(
                NameBringsTableAccess.class,
                e.getMessage(),
                SystemLogWriter.WARNING
            );
        }
    }


    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 志望校評価（P保護）の削除
     *
     * @param con DBコネクション
     * @param 個人ID studentId 年度 year 模試コード examCd 科目コード subCd
     * @return void
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void DeleteCandiDatePP(Connection con, String answerSheetNo, String year, String examCd) {

        try {
            PreparedStatement ps =
                (PreparedStatement) NameBringsBean.STATEMENT.get("DEL_CANDIDATERATING_PP");

            if (ps == null) {
                String query =
                    "DELETE CANDIDATERATING_PP "+
                    "WHERE ANSWERSHEET_NO=? AND EXAMYEAR=? "+
                    "AND EXAMCD=?";

                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("DEL_CANDIDATERATING_PP", ps);
            }

            ps.setString(1, answerSheetNo);
            ps.setString(2, year);
            ps.setString(3, examCd);
            ps.executeUpdate();
        } catch (Exception e) {
            SystemLogWriter.printLog(
                NameBringsTableAccess.class,
                e.getMessage(),
                SystemLogWriter.WARNING
            );
        }
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 国語記述評価の削除
     *
     * @param con DBコネクション
     * @param 個人ID studentId 年度 year 模試コード examCd 科目コード subCd
     * @return void
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void deleteKokugoDesc_I(Connection con, String studentId, String year, String examCd) {
        try {
            PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("DEL_KOKUGO_DESC_I");

            if (ps == null) {
                String query = "DELETE KOKUGO_DESC_I WHERE INDIVIDUALID=? AND EXAMYEAR=? AND EXAMCD=?";

                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("DEL_KOKUGO_DESC_I", ps);
            }

            ps.setString(1, studentId);
            ps.setString(2, year);
            ps.setString(3, examCd);
            ps.executeUpdate();

        } catch (Exception e) {
            SystemLogWriter.printLog(NameBringsTableAccess.class, e.getMessage(), SystemLogWriter.WARNING);
        }
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 国語記述評価（P保護）の削除
     *
     * @param con DBコネクション
     * @param 個人ID studentId 年度 year 模試コード examCd 科目コード subCd
     * @return void
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void deleteKokugoDesc_PP(Connection con, String answerSheetNo, String year, String examCd) {
        try {
            PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("DEL_KOKUGO_DESC_I_PP");

            if (ps == null) {
                String query = "DELETE KOKUGO_DESC_I_PP WHERE ANSWERSHEET_NO=? AND EXAMYEAR=? AND EXAMCD=?";
                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("DEL_KOKUGO_DESC_I_PP", ps);
            }

            ps.setString(1, answerSheetNo);
            ps.setString(2, year);
            ps.setString(3, examCd);
            ps.executeUpdate();

        } catch (Exception e) {
            SystemLogWriter.printLog(NameBringsTableAccess.class, e.getMessage(), SystemLogWriter.WARNING);
        }
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * CEFR成績の削除
     *
     * @param con DBコネクション
     * @param 個人ID studentId 年度 year 模試コード examCd 科目コード subCd
     * @return void
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void deleteCefrRecord_I(Connection con, String studentId, String year, String examCd) {
        try {
            PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("DEL_CEFR_RECORD_I");

            if (ps == null) {
                String query = "DELETE CEFR_RECORD_I WHERE INDIVIDUALID=? AND EXAMYEAR=? AND EXAMCD=?";

                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("DEL_CEFR_RECORD_I", ps);
            }

            ps.setString(1, studentId);
            ps.setString(2, year);
            ps.setString(3, examCd);
            ps.executeUpdate();

        } catch (Exception e) {
            SystemLogWriter.printLog(NameBringsTableAccess.class, e.getMessage(), SystemLogWriter.WARNING);
        }
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * CEFR成績（P保護）の削除
     *
     * @param con DBコネクション
     * @param 個人ID studentId 年度 year 模試コード examCd 科目コード subCd
     * @return void
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void deleteCefrRecord_PP(Connection con, String answerSheetNo, String year, String examCd) {
        try {
            PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("DEL_CEFR_RECORD_I_PP");

            if (ps == null) {
                String query = "DELETE CEFR_RECORD_I_PP WHERE ANSWERSHEET_NO=? AND EXAMYEAR=? AND EXAMCD=?";
                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("DEL_CEFR_RECORD_I_PP", ps);
            }

            ps.setString(1, answerSheetNo);
            ps.setString(2, year);
            ps.setString(3, examCd);
            ps.executeUpdate();

        } catch (Exception e) {
            SystemLogWriter.printLog(NameBringsTableAccess.class, e.getMessage(), SystemLogWriter.WARNING);
        }
    }


    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * CEFR成績の削除
     *
     * @param con DBコネクション
     * @param 個人ID studentId 年度 year 模試コード examCd 科目コード subCd
     * @return void
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void deleteAcademic_I(Connection con, String studentId, String year, String examCd) {
        try {
            PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("DEL_SUBACADEMIC_I");

            if (ps == null) {
                String query = "DELETE SUBACADEMIC_I WHERE INDIVIDUALID=? AND EXAMYEAR=? AND EXAMCD=?";

                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("DEL_SUBACADEMIC_I", ps);
            }

            ps.setString(1, studentId);
            ps.setString(2, year);
            ps.setString(3, examCd);
            ps.executeUpdate();

        } catch (Exception e) {
            SystemLogWriter.printLog(NameBringsTableAccess.class, e.getMessage(), SystemLogWriter.WARNING);
        }
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * CEFR成績（P保護）の削除
     *
     * @param con DBコネクション
     * @param 個人ID studentId 年度 year 模試コード examCd 科目コード subCd
     * @return void
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void deleteAcademic_PP(Connection con, String answerSheetNo, String year, String examCd) {
        try {
            PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("DEL_SUBACADEMIC_I_PP");

            if (ps == null) {
                String query = "DELETE SUBACADEMIC_I_PP WHERE ANSWERSHEET_NO=? AND EXAMYEAR=? AND EXAMCD=?";
                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("DEL_SUBACADEMIC_I_PP", ps);
            }

            ps.setString(1, answerSheetNo);
            ps.setString(2, year);
            ps.setString(3, examCd);
            ps.executeUpdate();

        } catch (Exception e) {
            SystemLogWriter.printLog(NameBringsTableAccess.class, e.getMessage(), SystemLogWriter.WARNING);
        }
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 個表データUPDATE
     *
     * @param con DBコネクション
     * @param keys [0]年度２桁 [1]学校コード [2]在卒校コード [3]年度 [4]学年 [5]クラス [6]クラス番号
     * 				[7]性別 [8]かな名前 [9]漢字名 [10]生年月日（年） [11]生年月日（月） [12]生年月日（日）
     * 				[13]電話番号 [14]前年度[15]個人ID
     * @return judge 再集計対象ならtrueを返す
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void UpdIndiviDualRec(
        Connection con,
        String individualId,
        String year,
        String examCd,
        String answersheet_no) throws SQLException {

        try {
            PreparedStatement ps =
                (PreparedStatement) NameBringsBean.STATEMENT.get("UPD_INDIVIDUALRECORD_STATUS");

            if (ps == null) {
                String query =
                    " UPDATE INDIVIDUALRECORD "
                        + " SET S_INDIVIDUALID=?,S_STATUSFLG=1"
                        + " WHERE EXAMYEAR=? AND EXAMCD=? AND ANSWERSHEET_NO=?";

                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("UPD_INDIVIDUALRECORD_STATUS", ps);
            }
            ps.setString(1, individualId);
            ps.setString(2, year);
            ps.setString(3, examCd);
            ps.setString(4, answersheet_no);

            if (ps.executeUpdate() == 0)
                throw new SQLException("個表データのUPDATEに失敗しました。");

        } catch (Exception e) {
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
            //throw new SQLException(e.getMessage());
            throw new SQLException(e);
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
        }
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 個表データINSERT
     *
     * @param con DBコネクション
     * @param insData 更新するデータ
     * @return count 更新件数
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void insDualRec(Connection con, HashMap insData) throws SQLException {

        try {
            PreparedStatement ps
                = (PreparedStatement) NameBringsBean.STATEMENT.get("INS_INDIVIDUALRECORD");

            if (ps == null) {
                String query =
                    " INSERT /*+ APPEND */ INTO INDIVIDUALRECORD"+
                    " (EXAMYEAR,EXAMCD,ANSWERSHEET_NO,HOMESCHOOLCD,BUNDLECD,"+
                    " GRADE,CLASS,CLASS_NO,ATTENDGRADE,"+
                    " NAME_KANA,NAME_KANJI,SEX,"+
                    " BIRTHDAY,TEL_NO,CUSTOMER_NO,STUDENT_NO,REGION," +
                    " SCHOOLHOUSE,APPLIDATE,PRIVERCYFLG,CNTGRADE,CNTCLASS,EXAMTYPE,BUNRICODE)"+
                    " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("INS_INDIVIDUALRECORD", ps);
            }

            ps.setString(1, (String)insData.get("EXAMYEAR"));
            ps.setString(2, (String)insData.get("EXAMCD"));
            ps.setString(3, (String)insData.get("ANSWERSHEET_NO"));
            ps.setString(4, (String)insData.get("HOMESCHOOLCD"));
            ps.setString(5, (String)insData.get("BUNDLECD"));
            ps.setString(6, (String)insData.get("GRADE"));
            ps.setString(7, (String)insData.get("CLASS"));
            ps.setString(8, (String)insData.get("CLASS_NO"));
            ps.setString(9, (String)insData.get("ATTENDGRADE"));
            ps.setString(10, (String)insData.get("NAME_KANA"));
            ps.setString(11, (String)insData.get("NAME_KANJI"));
            ps.setString(12, (String)insData.get("SEX"));
            ps.setString(13, (String)insData.get("BIRTHDAY"));
            ps.setString(14, (String)insData.get("TEL_NO"));
            ps.setString(15, (String)insData.get("CUSTOMER_NO"));
            ps.setString(16, (String)insData.get("STUDENT_NO"));
            ps.setString(17, (String)insData.get("REGION"));
            ps.setString(18, (String)insData.get("SCHOOLHOUSE"));
            ps.setString(19, (String)insData.get("APPLIDATE"));
            ps.setString(20, (String)insData.get("PRIVERCYFLG"));
            ps.setString(21, (String)insData.get("GRADE"));
            ps.setString(22, (String)insData.get("CLASS"));
            ps.setString(23, (String)insData.get("EXAMTYPE"));
            ps.setString(24, (String)insData.get("BUNRICODE"));

            if (ps.executeUpdate() == 0)
                new SQLException("個表データのINSERTに失敗しました。");

        } catch (Exception e) {
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
            //throw new SQLException(e.getMessage());
            throw new SQLException(e);
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
        }
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 個表データUPDATE
     *
     * @param con DBコネクション
     * @param updData 更新するデータ
     * @return count 更新件数
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void updDualRec(Connection con, HashMap updData, String rowid) throws SQLException {

        try {
            PreparedStatement ps
                = (PreparedStatement) NameBringsBean.STATEMENT.get("UPD_INDIVIDUALRECORD");

            if (ps == null) {
                String query =
                    " UPDATE INDIVIDUALRECORD"+
                    " SET HOMESCHOOLCD=?,"+
                    " BUNDLECD=?, ATTENDGRADE=?, TEL_NO=?,"+
                    " CUSTOMER_NO=?, STUDENT_NO=?, REGION=?,"+
                    " SCHOOLHOUSE=?, APPLIDATE=?, PRIVERCYFLG=?,"+
                    " CNTGRADE=?, CNTCLASS=?,"+
                    " EXAMTYPE=?, BUNRICODE=?"+
                    " WHERE ROWID=?";
                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("UPD_INDIVIDUALRECORD", ps);
            }

            ps.setString(1, (String)updData.get("HOMESCHOOLCD"));
            ps.setString(2, (String)updData.get("BUNDLECD"));
            ps.setString(3, (String)updData.get("ATTENDGRADE"));
            ps.setString(4, (String)updData.get("TEL_NO"));
            ps.setString(5, (String)updData.get("CUSTOMER_NO"));
            ps.setString(6, (String)updData.get("STUDENT_NO"));
            ps.setString(7, (String)updData.get("REGION"));
            ps.setString(8, (String)updData.get("SCHOOLHOUSE"));
            ps.setString(9, (String)updData.get("APPLIDATE"));
            ps.setString(10, (String)updData.get("PRIVERCYFLG"));
            ps.setString(11, (String)updData.get("GRADE"));
            ps.setString(12, (String)updData.get("CLASS"));
            ps.setString(13, (String)updData.get("EXAMTYPE"));
            ps.setString(14, (String)updData.get("BUNRICODE"));

            ps.setString(15, rowid);

            if (ps.executeUpdate() ==0)
                new SQLException("個表データのUPDATEに失敗しました。");

        } catch (Exception e) {
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
            //throw new SQLException(e.getMessage());
            throw new SQLException(e);
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
        }
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * ------------------------------
     * 個表ワークデータを抽出：
     * 模試マスタ外部開放日でソート
     * ------------------------------
     *
     * @param con DBコネクション
     * @return 個表データ
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static ResultSet SelWork(Connection con) throws SQLException {

        try {
            PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("SEL_INDIVIDUALRECORD_TMP");

            if (ps == null) {
                String query =
                    " SELECT t.ROWID, t.EXAMYEAR,t.EXAMCD,t.ANSWERSHEET_NO,t.HOMESCHOOLCD,t.BUNDLECD,"+
                    " t.GRADE,t.CLASS,t.CLASS_NO,t.ATTENDGRADE,"+
                    " t.NAME_KANA,t.NAME_KANJI,t.SEX," +
                    //[2004/10/28 nino] BIRTHDAY に'99999999'等の無効データがあった場合の対応
                    //" TO_NUMBER(TO_CHAR(TO_DATE(t.BIRTHDAY,'RRMMDD'),'YYYYMMDD')) BIRTHDAY,t.TEL_NO,t.CUSTOMER_NO,"+
                    " t.BIRTHDAY,t.TEL_NO,t.CUSTOMER_NO,"+
                    " t.STUDENT_NO,t.REGION,t.SCHOOLHOUSE,t.APPLIDATE,t.PRIVERCYFLG,"+
                    " t.SUBRECORD1,t.SUBRECORD2,t.SUBRECORD3,t.SUBRECORD4,t.SUBRECORD5,t.SUBRECORD6,"+
                    " t.SUBRECORD7,t.SUBRECORD8,t.SUBRECORD9,t.SUBRECORD10,t.SUBRECORD11,t.SUBRECORD12,"+
                    " t.SUBRECORD13,t.SUBRECORD14,t.SUBRECORD15,t.SUBRECORD16,t.SUBRECORD17,"+
                    " t.SUBRECORD18,t.SUBRECORD19,t.SUBRECORD20,t.SUBRECORD21,t.SUBRECORD22,"+
                    " t.SUBRECORD23,t.SUBRECORD24,t.SUBRECORD25,t.SUBRECORD26,t.JAPDESC,t.ENGPT,"+
                    " t.QUESTIONRECORD1,t.QUESTIONRECORD2,t.QUESTIONRECORD3,t.QUESTIONRECORD4,"+
                    " t.QUESTIONRECORD5,t.QUESTIONRECORD6,t.QUESTIONRECORD7,t.QUESTIONRECORD8,t.QUESTIONRECORD9,"+
                    " t.QUESTIONRECORD10,t.CANDIDATERATING1,t.CANDIDATERATING2,t.CANDIDATERATING3,t.CANDIDATERATING4,"+
                    " t.CANDIDATERATING5,t.CANDIDATERATING6,t.CANDIDATERATING7,t.CANDIDATERATING8,t.CANDIDATERATING9,"+
                    " t.ACADEMIC1,t.ACADEMIC2,t.ACADEMIC3,t.ACADEMIC4,t.ACADEMIC5,t.ACADEMIC6," +
                    " t.EXAMTYPE,t.BUNRICODE" +
                    " FROM INDIVIDUALRECORD_TMP t";
//		   個表ワークと模試マスタとの結合及びソートを削除 2004/11/04
//					" FROM INDIVIDUALRECORD_TMP t, EXAMINATION e"+
//					" WHERE t.EXAMYEAR=e.EXAMYEAR AND t.EXAMCD=e.EXAMCD "+
//					" ORDER BY e.OUT_DATAOPENDATE, e.EXAMCD, t.BUNDLECD";

                ps = con.prepareStatement(query);
                ps.setFetchSize(500);
                NameBringsBean.STATEMENT.put("SEL_INDIVIDUALRECORD_TMP", ps);
            }

            return ps.executeQuery();

        } catch (Exception e) {
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
            //throw new SQLException("個表ワークデータの取得に失敗しました。");
            throw new SQLException(e);
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
        }
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 個表ワークデータの削除
     *
     * @param con DBコネクション
     * @param 個人ID studentId 年度 year 模試コード examCd 科目コード subCd
     * @return void
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static void DelWork(Connection con, String rowid) throws SQLException {

        try {
            PreparedStatement ps
                = (PreparedStatement) NameBringsBean.STATEMENT.get("DEL_INDIVIDUALRECORD_TMP");

            if (ps == null) {
                ps = con.prepareStatement(
                    "DELETE INDIVIDUALRECORD_TMP WHERE ROWID = ?");
                NameBringsBean.STATEMENT.put("DEL_INDIVIDUALRECORD_TMP", ps);
            }

            ps.setString(1, rowid);

            if(ps.executeUpdate() == 0)
                throw new SQLException("個表ワークデータのDELETE処理に失敗しました。");

        } catch (Exception e) {
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
            //throw new SQLException(e.getMessage());
            throw new SQLException(e);
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
        }
    }

    /**
     * [2004/10/28 nino]
     * BIRTHDAY に'999999'等の無効データがあった場合の対応
     *
     * ６桁の誕生日を８桁に変換
     *
     * 年が00〜49の場合は20XX年  50〜99の場合は19XX年とする
     *
     * 2004.11.12
     * Yoshimoto KAWAI - Totec
     * NULL値が来た場合の対応を追加
     *
     * @param  birthDay("yymmdd")
     * @return birthDay("yyyymmdd")
     */
    static String getBirthDay(String birthDay) {

        try {
            if (birthDay == null) {
                return "";
            // 8桁ならそのまま
            } if (birthDay.length() == 8) {
                return birthDay;

            // 6桁なら変換する
            } else if(birthDay.length() == 6) {
                String year = birthDay.substring(0, 2);
                String month = birthDay.substring(2, 4);
                String day = birthDay.substring(4, 6);

                int yy = Integer.parseInt(year);
                int mm = Integer.parseInt(month);
                int dd = Integer.parseInt(day);

                // 月のチェック
                if (1 <= mm && mm <= 12 ) {} else return "";

                // 日のチェック
                switch (mm) {
                    // 2
                    case 2:
                        if (yy % 4 == 0) {
                            if (1 <= dd && dd <= 29 ) {} else return "";
                        } else {
                            if (1 <= dd && dd <= 28 ) {} else return "";
                        }
                        break;

                    // 4,6,9,11
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        if (1 <= dd && dd <= 30 ) {} else return "";
                        break;

                    // 1,3,4,7,8,10,12
                    default:
                        if (1 <= dd && dd <= 31 ) {} else return "";
                        break;
                }

                if(yy >= 50 ) return "19" + year + month + day;
                else return "20" + year + month + day;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 年度、模試コード、解答用紙番号で条件付けた受験情報を取得する。
     *
     * @param con DBコネクション
     * @param targetYear 対象年度  targetExam 対象模試コード　answerSheetNo 解答用紙番号
     * @return list レコード
     * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
     * @see java.sql.SQLException
     */
    static HashMap SelExamStudentInf(
        Connection con,
        String targetYear,
        String targetExam,
        String answerSheetNo)
        throws SQLException {

        ResultSet rs = null;
        HashMap record = null;
        try {
            PreparedStatement ps
                = (PreparedStatement) NameBringsBean.STATEMENT.get("SEL_INDIVIDUALRECORD");

            if (ps == null) {
                String query =
                    " SELECT"+
                    " ROWID IROWID," +
                    " S_INDIVIDUALID S_INDIVIDUALID,"+
                    " EXAMYEAR IYEAR,"+
                    " EXAMCD IEXAMCD,"+
                    " GRADE IGRADE,"+
                    " CLASS ICLASS,"+
                    " CLASS_NO ICLASS_NO,"+
                    " HOMESCHOOLCD IHOMESCHOOLCD,"+
                    " BUNDLECD IBUNDLECD,"+
                    " TRIM(NAME_KANA) INAME_KANA,"+
                    " TRIM(NAME_KANJI) INAME_KANJI,"+
                    " SEX ISEX,"+
                    " TEL_NO ITEL_NO,"+
                    " TRIM(BIRTHDAY) IBIRTHDAY,"+
                    " ANSWERSHEET_NO IANSWERSHEET_NO,"+
                    " CUSTOMER_NO ICUSTOMER_NO,"+
                    " NVL(CNTGRADE, GRADE) CNTGRADE,"+
                    " NVL2(CNTGRADE, CNTCLASS, CLASS) CNTCLASS"+
                    " FROM"+
                    " INDIVIDUALRECORD"+
                    " WHERE"+
                    " EXAMYEAR=? AND"+
                    " EXAMCD=? AND"+
                    " ANSWERSHEET_NO=?";

                ps = con.prepareStatement(query);
                NameBringsBean.STATEMENT.put("SEL_INDIVIDUALRECORD", ps);
            }
            ps.setString(1, targetYear);
            ps.setString(2, targetExam);
            ps.setString(3, answerSheetNo);

            rs = ps.executeQuery();
            while (rs.next()) {
                record = new HashMap();
                record.put("IROWID", rs.getString("IROWID"));
                record.put("S_INDIVIDUALID", rs.getString("S_INDIVIDUALID"));
                record.put("IYEAR", rs.getString("IYEAR"));
                record.put("IEXAMCD", rs.getString("IEXAMCD"));
                record.put("IGRADE", rs.getString("IGRADE"));
                record.put("ICLASS", rs.getString("ICLASS"));
                record.put("ICLASS_NO", rs.getString("ICLASS_NO"));
                record.put("IHOMESCHOOLCD", rs.getString("IHOMESCHOOLCD"));
                record.put("IBUNDLECD", rs.getString("IBUNDLECD"));
                record.put("INAME_KANA", rs.getString("INAME_KANA"));
                record.put("INAME_KANJI", rs.getString("INAME_KANJI"));
                record.put("ISEX", rs.getString("ISEX"));
                record.put("ITEL_NO", rs.getString("ITEL_NO"));
                record.put("IBIRTHDAY", rs.getString("IBIRTHDAY"));
                record.put("IANSWERSHEET_NO", rs.getString("IANSWERSHEET_NO"));
                record.put("ICUSTOMER_NO", rs.getString("ICUSTOMER_NO"));
                record.put("CNTGRADE", rs.getString("CNTGRADE"));
                record.put("CNTCLASS", rs.getString("CNTCLASS"));
            }

        } catch (Exception ex) {
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
            //throw new SQLException("受験情報の取得に失敗しました。");
            throw new SQLException(ex);
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END

        } finally {
            DbUtils.closeQuietly(rs);
        }

        return record;
    }


    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 科目再集計対象に加える
     *
     */
    public static void InsertSubCount(final Connection con, final String examYear,
            final String examCd, final String bundleCd, final int grade,
            final String className, final String subCd) throws SQLException {

        PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("SUBRECOUNT");
        if (ps == null) {
            ps = con.prepareStatement(
                "INSERT INTO subrecount "
                    + "SELECT ?,?,?,?,?,? FROM dual "
                    + "WHERE NOT EXISTS("
                    + "SELECT 1 FROM subrecount "
                    + "WHERE examyear = ? AND examcd = ? "
                    + "AND bundlecd = ? AND grade = ?"
                    + "AND class = ? AND subcd = ?)");

            NameBringsBean.STATEMENT.put("SUBRECOUNT", ps);
        }
        ps.setString(1, examYear);
        ps.setString(2, examCd);
        ps.setString(3, bundleCd);
        ps.setInt(4, grade);
        ps.setString(5, className);
        ps.setString(6, subCd);
        ps.setString(7, examYear);
        ps.setString(8, examCd);
        ps.setString(9, bundleCd);
        ps.setInt(10, grade);
        ps.setString(11, className);
        ps.setString(12, subCd);
        ps.executeUpdate();
    }

    /**
     * 注意：名寄せバッチからのみコール可能
     *
     * 志望大学評価別人数再集計対象に加える
     *
     */
    public static void InsertRatingNumberRecount(final Connection con,
            final String examYear, final String examCd, final String bundleCd,
            final int grade, final String className, final String univCd,
            final String facltyCd, final String deptCd) throws SQLException {

        PreparedStatement ps = (
                PreparedStatement) NameBringsBean.STATEMENT.get("RATINGNUMBERRECOUNT");
        if (ps == null) {
            ps = con.prepareStatement(
                "INSERT INTO ratingnumberrecount "
                    + "SELECT ?,?,?,?,?,?,?,? FROM dual "
                    + "WHERE NOT EXISTS ("
                    + "SELECT 1 FROM ratingnumberrecount "
                    + "WHERE year = ? AND examcd = ? "
                    + "AND bundlecd = ? AND grade = ? "
                    + "AND class = ? AND univcd = ? "
                    + "AND facultycd = ? AND deptcd = ?)");

            NameBringsBean.STATEMENT.put("RATINGNUMBERRECOUNT", ps);
        }
        ps.setString(1, examYear);
        ps.setString(2, examCd);
        ps.setString(3, bundleCd);
        ps.setInt(4, grade);
        ps.setString(5, className);
        ps.setString(6, univCd);
        ps.setString(7, facltyCd);
        ps.setString(8, deptCd);
        ps.setString(9, examYear);
        ps.setString(10, examCd);
        ps.setString(11, bundleCd);
        ps.setInt(12, grade);
        ps.setString(13, className);
        ps.setString(14, univCd);
        ps.setString(15, facltyCd);
        ps.setString(16, deptCd);
        ps.executeUpdate();
    }

    /**
     * Int値をセットする
     * @param ps
     * @param index
     * @param value
     * @throws SQLException
     */
    static void setInt(PreparedStatement ps, int index, String value) throws SQLException {
        if (value == null || "".equals(value)) ps.setNull(index, Types.INTEGER);
        else ps.setInt(index, Integer.parseInt(value));
    }

    /**
     * Double値をセットする
     * @param ps
     * @param index
     * @param value
     * @throws SQLException
     */
    static void setDouble(PreparedStatement ps, int index, String value) throws SQLException {
        if (value == null || "".equals(value)) ps.setNull(index, Types.DOUBLE);
        else ps.setDouble(index, Double.parseDouble(value) / 10);
    }

    /**
     * String値をセットする
     * @param ps
     * @param index
     * @param value
     * @throws SQLException
     */
    static void setString(PreparedStatement ps, int index, String value) throws SQLException {
        if (value == null || "".equals(value)) ps.setNull(index, Types.CHAR);
        else ps.setString(index, value);
    }

}

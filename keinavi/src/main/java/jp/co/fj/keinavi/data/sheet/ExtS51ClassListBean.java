/*
 * 作成日: 2004/09/24
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.sheet;

import jp.co.fj.keinavi.excel.data.school.S51ClassListBean;

/**
 * 校内成績分析・過回比較・成績概況
 * クラスデータリスト
 * 
 * @author kawai
 */
public class ExtS51ClassListBean extends S51ClassListBean implements Comparable {

	private String dataOpenDate; // データ開放日
	private int classDispSeq; // クラスの表示順序

	/* (非 Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		// クラスの表示順序の昇順・データ開放日の降順でソートする
		ExtS51ClassListBean obj = (ExtS51ClassListBean) o;

		if (this.getClassDispSeq() > obj.getClassDispSeq()) {
			return 1;

		} else if (this.getClassDispSeq() < obj.getClassDispSeq()) {
			return -1;

		} else {
			return obj.getDataOpenDate().compareTo(this.getDataOpenDate());
		}
	}

	/**
	 * @return
	 */
	public int getClassDispSeq() {
		return classDispSeq;
	}

	/**
	 * @param i
	 */
	public void setClassDispSeq(int i) {
		classDispSeq = i;
	}

	/**
	 * @return
	 */
	public String getDataOpenDate() {
		return dataOpenDate;
	}

	/**
	 * @param string
	 */
	public void setDataOpenDate(String string) {
		dataOpenDate = string;
	}

}

package jp.co.fj.keinavi.beans.sales;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.data.sales.SD206Data;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 特例成績データ　申請一覧確認（パスワード通知書ダウンロード）画面のリストBeanです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD206ListBean extends DefaultBean {

	/** serialVersionUID */
	private static final long serialVersionUID = 8364875569827868237L;

	/** 対象年度 */
	private final String examYear;

	/** 絞込み条件 */
	private final String condition;

	/** ソートキー */
	private final String sortKey;

	/** データリスト */
	private final List results = new ArrayList();

	/**
	 * コンストラクタです。
	 */
	public SD206ListBean(String examYear, String condition, String sortKey) {
		this.examYear = examYear;
		this.condition = condition;
		this.sortKey = sortKey;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(createQuery());
			ps.setString(1, examYear);
			ps.setString(2, condition);

			rs = ps.executeQuery();

			SD206Data data = null;
			while (rs.next()) {

				if (data == null || !data.getAppId().equals(rs.getString(1))) {
					data = new SD206Data();
					data.setAppId(rs.getString(1));
					data.setExamYear(rs.getString(2));
					data.setExamName(rs.getString(3));
					data.setSchoolCd(rs.getString(4));
					data.setSchoolName(rs.getString(5));
					data.setPactDivName(rs.getString(6));
					data.setTeacherName(rs.getString(7));
					data.setAppComment(rs.getString(8));
					data.setAppDate(rs.getTimestamp(9));
					data.setDownloadDate(rs.getTimestamp(10));
					data.setStatusCd(rs.getString(11));
					data.setStatusName(rs.getString(12));
					data.setOpenDate(rs.getDate(13));
					results.add(data);
				}

				data.getSectorNameList().add(rs.getString(14));
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * SQLを生成します。
	 */
	private String createQuery() throws SQLException {
		Query query = QueryLoader.getInstance().load("sd206_list");
		query.append(createOrderBy());
		return query.toString();
	}

	/**
	 * ORDER BY句を生成します。
	 */
	private String createOrderBy() {

		if ("1".equals(sortKey)) {
			/* 対象年度 */
			return "EXAMYEAR, APP_DATE DESC, BUNDLECD, APPLICATION_ID, SECTORNAME";
		} else if ("2".equals(sortKey)) {
			/* 対象模試 */
			return "DISPSEQUENCE, EXAMCD, APP_DATE DESC, BUNDLECD, APPLICATION_ID, SECTORNAME";
		} else if ("3".equals(sortKey)) {
			/* 申請営業部 */
			return "SECTORSORTKEY, APP_DATE DESC, BUNDLECD, APPLICATION_ID, SECTORNAME";
		} else if ("4".equals(sortKey)) {
			/* 学校コード */
			return "BUNDLECD, APP_DATE DESC, APPLICATION_ID, SECTORNAME";
		} else if ("5".equals(sortKey)) {
			/* 申請日時 */
			return "APP_DATE DESC, BUNDLECD, APPLICATION_ID, SECTORNAME";
		} else if ("6".equals(sortKey)) {
			/* 最終ダウンロード日時 */
			return "LAST_DOWNLOAD_DATE, APP_DATE DESC, BUNDLECD, APPLICATION_ID, SECTORNAME";
		} else if ("7".equals(sortKey)) {
			/* 状態 */
			return "APP_STATUS_CODE, APP_DATE DESC, BUNDLECD, APPLICATION_ID, SECTORNAME";
		} else {
			/* 不正なソートキーなら例外とする */
			throw new RuntimeException("ソートキーが不正です。" + sortKey);
		}
	}

	/**
	 * データリストを返します。
	 * 
	 * @return データリスト
	 */
	public List getResults() {
		return results;
	}

}

package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.data.individual.PersonComboData;
import jp.co.fj.keinavi.data.individual.StudentData;

import com.fjh.beans.DefaultBean;

public class I003Bean extends DefaultBean{
	
	private static final long serialVersionUID = -4726088895236557396L;
	
	private String schoolCd;
	private String[][] classs;
	private String[] personIds;
	
	private String studentSql;
	
	private PersonComboData personComboData;

	/* (�� Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		setPersonComboData(execStudentData());
	}
	
	public void setSearchCondition(String schoolCd, List classes, String personIds){
		
		setSchoolCd(schoolCd);
		setClasss(IUtilBean.convert2ClassArray(classes));
		setPersonIds(IUtilBean.deconcatComma(personIds));
		
		appendStudentSql(getStudentSqlBase());
		appendStudentSql(getSchoolCondition(getSchoolCd()));
		appendStudentSql(getPersonCondition(getPersonIds()));
	}
	private String getStudentSqlBase(){
		String sql = "SELECT" +
				" HISTORYINFO.INDIVIDUALID" +
				" ,HISTORYINFO.YEAR" +
				" ,HISTORYINFO.GRADE" +
				" ,HISTORYINFO.CLASS" +
				" ,HISTORYINFO.CLASS_NO" +
				" ,BASICINFO.NAME_KANA" +
				" ,BASICINFO.NAME_KANJI" +
				" FROM HISTORYINFO" +
				" INNER JOIN BASICINFO ON HISTORYINFO.INDIVIDUALID = BASICINFO.INDIVIDUALID" +
				" WHERE" +
				" HISTORYINFO.YEAR = (SELECT MAX(YEAR) FROM HISTORYINFO) ";
		return sql;
	}
	private void appendStudentSql(String string){
		if(getStudentSql() == null)
			setStudentSql("");
		studentSql += string;
	}
	private String getPersonCondition(String[] strings){
		String sqlOption = " AND (";
		for(int i = 0; i < strings.length; i ++){
			if(i != 0)
				sqlOption += " OR";
			sqlOption += " HISTORYINFO.INDIVIDUALID = ?";
		}
		sqlOption += " )";
		return sqlOption;
	}
	private String getSchoolCondition(String schoolCd){
		String sqlOption = " AND BASICINFO.SCHOOLCD = '"+schoolCd+"'";
		return sqlOption;
	}
	
	private PersonComboData execStudentData() throws Exception{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			//System.out.println(getStudentSql());			
			pstmt = conn.prepareStatement(getStudentSql());
			for(int i = 0; i < personIds.length; i ++){
				pstmt.setString(i + 1, personIds[i]);
			}
			rs = pstmt.executeQuery();
			PersonComboData personComboData = new PersonComboData();
			personComboData.setPersonDatas(new ArrayList());
			while(rs.next()){
				StudentData studentData = new StudentData();
				studentData.setStudentPersonId(rs.getString("INDIVIDUALID"));
				studentData.setStudentYear(rs.getString("YEAR"));
				studentData.setStudentGrade(rs.getString("GRADE"));
				studentData.setStudentClass(rs.getString("CLASS"));
				studentData.setStudentClassNo(rs.getString("CLASS_NO"));
				studentData.setStudentNameKana(rs.getString("NAME_KANA"));
				studentData.setStudentNameKanji(rs.getString("NAME_KANJI"));
				personComboData.getPersonDatas().add(studentData);
			}
			if(pstmt != null) {pstmt.close();}
			if(rs != null) {rs.close();}
			
			return personComboData;
			
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}	
	
	/**
	 * @return
	 */
	public String getStudentSql() {
		return studentSql;
	}

	/**
	 * @param string
	 */
	public void setStudentSql(String string) {
		studentSql = string;
	}
	/**
	 * @return
	 */
	public PersonComboData getPersonComboData() {
		return personComboData;
	}

	/**
	 * @param data
	 */
	public void setPersonComboData(PersonComboData data) {
		personComboData = data;
	}

	/**
	 * @return
	 */
	public String[][] getClasss() {
		return classs;
	}

	/**
	 * @return
	 */
	public String[] getPersonIds() {
		return personIds;
	}

	/**
	 * @return
	 */
	public String getSchoolCd() {
		return schoolCd;
	}

	/**
	 * @param strings
	 */
	public void setClasss(String[][] strings) {
		classs = strings;
	}

	/**
	 * @param strings
	 */
	public void setPersonIds(String[] strings) {
		personIds = strings;
	}

	/**
	 * @param string
	 */
	public void setSchoolCd(String string) {
		schoolCd = string;
	}

}

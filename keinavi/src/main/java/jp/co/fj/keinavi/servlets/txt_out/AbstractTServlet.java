package jp.co.fj.keinavi.servlets.txt_out;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.beans.exam.ExamFilterManager;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.servlets.cls.AbstractCServlet;

/**
 *
 * テキスト出力サーブレットの基本クラス
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class AbstractTServlet extends AbstractCServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = -1098741875538632641L;

    /**
     * {@inheritDoc}
     */
    protected ExamSession getClassExamSession(final HttpServletRequest request) throws ServletException {

        String forward = getForward(request);

        ExamSession examSession;
        if ("t001".equals(forward) || "t002".equals(forward) || "t003".equals(forward) || "t004".equals(forward)
                || "t005".equals(forward)) {
            /* 集計データトップ＆詳細画面なら模試セッションはそのまま */
            examSession = getExamSession(request);
        } else {
            /* そうでなければ模試セッションを再構築 */
            examSession = rebuildExamSession(request);
        }

        String id = forward;
        if ("t005".equals(forward)) {
            /* 志望大学評価別人数画面では、他校比較オプションがONの場合のみオープン模試を非表示にする */
            Profile profile = getProfile(request);
            Short flag = (Short) profile.getItemMap("050204").get("0403");
            if (flag == null || flag.intValue() != 1) {
                id = "t005_open";
            }
        }

        /* フィルタを通して返す */
        return ExamFilterManager.rebuild(id, getLoginSession(request), examSession);
    }

}

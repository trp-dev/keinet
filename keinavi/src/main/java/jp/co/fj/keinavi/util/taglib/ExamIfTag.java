package jp.co.fj.keinavi.util.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.taglibs.standard.tag.common.core.NullAttributeException;
import org.apache.taglibs.standard.tag.el.core.ExpressionUtil;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * 指定の試験が特定の試験の種類かを判断するためのカスタムタグ
 * 
 * 2006/08/25    [新規作成]
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class ExamIfTag extends TagSupport {
    
    /**
     * 受験学力測定
     */
    static final String ABILITY = "ability";
    
    /**
     * 新テスト
     */
    static final String NEW = "new";
    
    /**
     * センターリサーチ
     */
    static final String CENTER_RESEARCH = "center";
    
    /**
     * マーク系
     */
    static final String MARK = "mark";
    
    /**
     * パターン I
     */
    static final String PATTERN_I = "patternI";
    
    /**
     * パターン II
     */
    static final String PATTERN_II = "patternII";
    
    /**
     * パターン III
     */
    static final String PATTERN_III = "patternIII";
    
    /**
     * パターン IV
     */
    static final String PATTERN_IV = "patternIV";
    
    /**
     * 模試データ
     */
    ExamData exam;
    
    /**
     * ID
     */
    String id;
    
    /**
     * オペランド
     */
    String var;
    
    /**
     * var のスコープ
     */
    int scope;
    
    /**
     * 判定結果
     */
    boolean result = false;
    
    /**
     * Not として比較するかどうか
     */
    protected final boolean NOT = false;

    /**
     * @see javax.servlet.jsp.tagext.Tag#doStartTag()
     */
    public int doStartTag() throws JspException {        
        
        result = condition();
        
        if (result) {
          return EVAL_BODY_INCLUDE;
        }
        
        return SKIP_BODY;
    }
    
//    private void exposeVariables() {
//        if (var != null)
//            pageContext.setAttribute(var, exam, scope);
//    }
    
    /**
     * スコープの設定
     */
    public void setScope(String scope) {
        if (scope.equalsIgnoreCase("page"))
            this.scope = PageContext.PAGE_SCOPE;
        else if (scope.equalsIgnoreCase("request"))
            this.scope = PageContext.REQUEST_SCOPE;
        else if (scope.equalsIgnoreCase("session"))
            this.scope = PageContext.SESSION_SCOPE;
        else if (scope.equalsIgnoreCase("application"))
            this.scope = PageContext.APPLICATION_SCOPE;
    }

    /**
     * @see javax.servlet.jsp.tagext.Tag#release()
     */
    public void release() {
        super.release();
        init();
    }
    
    /**
     * プロパティの初期化
     */
    void init() {
        exam = null;
        id = this.var = null;
        scope = PageContext.PAGE_SCOPE;
    }
    
    /**
     * @param string キー値
     */
    public void setId(final String string) {
        id = string;
    }
    
    /**
     * @param string キー値
     */
    public void setVar(final String string) {
        this.var = string;
    }
    
    /**
     * 指定の試験タイプと合致したかどうか
     * @param pExam
     * @return
     */
    protected boolean isExamIdentified(final ExamData pExam) {
        
        boolean through = false;
        
        //受験学力測定
        if (ABILITY.equals(id)) {
            through = KNUtil.isAbilityExam(pExam);
        }        
        //新テスト
        else if (NEW.equals(id)) {
            through = KNUtil.isNewExam(pExam);
        }
        //センターリサーチ
        else if (CENTER_RESEARCH.equals(id)) {
            through = KNUtil.isCenterResearch(pExam);
        }
        //マーク系
        else if (MARK.equals(id)) {
            through = KNUtil.isMarkExam(pExam);
        }
        //パターンI
        else if (PATTERN_I.equals(id)) {
            through = KNUtil.isPatternI(pExam);
        }
        //パターンII
        else if (PATTERN_II.equals(id)) {
            through = KNUtil.isPatternII(pExam);
        }
        //パターンIII
        else if (PATTERN_III.equals(id)) {
            through = KNUtil.isPatternIII(pExam);
        }
        //パターンIV
        else if (PATTERN_IV.equals(id)) {
            through = KNUtil.isPatternIV(pExam);
        }
        //エラー
        else {
            throw new IllegalArgumentException("id が解決できません。" + id);
        }
        
        if (getNOT()) {
            return !through;
        }
        
        return through;
        
    }
    
    protected boolean evaluateSelf() throws JspTagException {
        return isExamIdentified(exam);
    }
    
    /**
     * 条件評価
     * @param pId テストＩＤ
     * @param pExam 試験
     * @return 真偽
     * @throws JspException 
     * @throws JspException 
     * @throws JspTagException 
     */
    protected boolean condition() throws JspTagException {

        //exposeVariables();
        
        //value を Object に変換
        try {
            evaluateExpressions();
        } catch (JspException ex) {
            throw new JspTagException(ex.toString());
        }
        
        return evaluateSelf();
    }
    
    /**
     * @throws JspException
     */
    protected void evaluateExpressions() throws JspException {
        try {
            exam = getExam(ExpressionUtil.evalNotNull(
                    "examIf", "var", var, Object.class, this, pageContext));
        } catch (NullAttributeException ex) {
        	// Null属性を許可する
        }
    }
    
    /**
     * 模試を変換する
     * @param object 試験データ
     * @return ExamData型データ
     * @throws JspTagException 
     */
    protected ExamData getExam(final Object object) throws JspTagException {
        
        //引数がExaminationDataならExamDataに変換する
        if (object instanceof ExaminationData) {
            return conv2ExamData((ExaminationData) object);
        }
        
        if (object instanceof ExamData) {
            return (ExamData) object;
        }
     
        throw new JspTagException("試験は次にいづれかの型でなければなりません：ExaminationData、ExamData");
    }
    
    /**
     * 試験を変換する
     * @param from 試験データ
     * @return ExamData型データ
     */
    private ExamData conv2ExamData(final ExaminationData from) {
        
        final ExamData exam = new ExamData();
        
        //必要であれば設定する属性を増やす
        exam.setExamYear(from.getExamYear());
        exam.setExamCD(from.getExamCd());
        exam.setExamTypeCD(from.getExamTypeCd());
        exam.setExamDiv(from.getExamDiv());
     
        return exam;
    }
    
    protected boolean getNOT() {
        return NOT;
    }

}

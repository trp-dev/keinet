package jp.co.fj.keinavi.forms.com_set;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM603Form extends BaseForm {

	// 県コードの配列
	private String[] pref = null;
	// 地区コードの配列
	private String[] block = null;
	// 公私区分コードの配列
	private String[] schoolDiv = null;
	// ランクの配列
	private String[] rank = null;
	// 受験人数
	private String examinees = null;
	// 表示対象（すべて／高校のみ／都道府県平均のみ）
	private String view = null;
	// 検索モード（高校名／高校コード）
	private String searchMode = null;
	// 高校名／高校コードの文字列
	private String searchStr = null;
	// Submitボタン
	private String button = null;

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * @return
	 */
	public String getExaminees() {
		return examinees;
	}

	/**
	 * @return
	 */
	public String[] getPref() {
		return pref;
	}

	/**
	 * @return
	 */
	public String[] getRank() {
		return rank;
	}

	/**
	 * @return
	 */
	public String[] getSchoolDiv() {
		return schoolDiv;
	}

	/**
	 * @return
	 */
	public String getSearchMode() {
		return searchMode;
	}

	/**
	 * @return
	 */
	public String getSearchStr() {
		return searchStr;
	}

	/**
	 * @return
	 */
	public String getView() {
		return view;
	}

	/**
	 * @param string
	 */
	public void setExaminees(String string) {
		examinees = string;
	}

	/**
	 * @param strings
	 */
	public void setPref(String[] strings) {
		pref = strings;
	}

	/**
	 * @param strings
	 */
	public void setRank(String[] strings) {
		rank = strings;
	}

	/**
	 * @param strings
	 */
	public void setSchoolDiv(String[] strings) {
		schoolDiv = strings;
	}

	/**
	 * @param string
	 */
	public void setSearchMode(String string) {
		searchMode = string;
	}

	/**
	 * @param string
	 */
	public void setSearchStr(String string) {
		searchStr = string;
	}

	/**
	 * @param string
	 */
	public void setView(String string) {
		view = string;
	}

	/**
	 * @return
	 */
	public String getButton() {
		return button;
	}

	/**
	 * @param string
	 */
	public void setButton(String string) {
		button = string;
	}

	/**
	 * @return
	 */
	public String[] getBlock() {
		return block;
	}

	/**
	 * @param strings
	 */
	public void setBlock(String[] strings) {
		block = strings;
	}

}

/*
 * 作成日: 2004/07/12
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.com_set;

import java.io.Serializable;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ComClassData implements Serializable, Comparable, Cloneable {
	
	private short grade; // 学年
	private String className; // クラス名
	private short graphDisp; // グラフ表示

	/**
	 * コンストラクタ
	 */
	public ComClassData(String key) {
		String[] value = key.split(":", 2);
		this.grade = Short.parseShort(value[0]);
		this.className = value[1];
	}

	/**
	 * コンストラクタ
	 */
	public ComClassData() {
	}

	/* (非 Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		if (this.getGrade() > ((ComClassData)o).getGrade()) {
			return 1;
		} else if (this.getGrade() > ((ComClassData)o).getGrade()) {
			return -1;		
		// 学年が同じならクラス名で比較
		} else {
			return this.getClassName().compareTo(((ComClassData)o).getClassName());
		}
	}

	/**
	 * データを一意に識別するキーを返す
	 * @return
	 */
	public String getKey() {
		return grade + ":" + className;
	}

	/**
	 * @return
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @return
	 */
	public short getGrade() {
		return grade;
	}

	/**
	 * @return
	 */
	public short getGraphDisp() {
		return graphDisp;
	}

	/**
	 * @param string
	 */
	public void setClassName(String string) {
		className = string;
	}

	/**
	 * @param s
	 */
	public void setGrade(short s) {
		grade = s;
	}

	/**
	 * @param s
	 */
	public void setGraphDisp(short s) {
		graphDisp = s;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public Object clone() {
		ComClassData data = new ComClassData();
		data.setGrade(this.grade);
		data.setClassName(this.className);
		data.setGraphDisp(this.graphDisp);
		return data;
	}

}

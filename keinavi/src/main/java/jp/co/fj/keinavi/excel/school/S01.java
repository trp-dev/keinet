/**
 * 校内成績分析−全国総合成績概況
 * 出力する帳票の判断
 * 作成日: 2004/07/16
 * @author	H.Fujimoto
 */
 
package jp.co.fj.keinavi.excel.school;

import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.util.log.*;

public class S01 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean s01(S01Item s01Item, ArrayList outfilelist, int intSaveFlg, String UserID, KNSheetLog sheetLog) {
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		try{
			int ret = 0;
			//S01Itemから各帳票を出力
			if ( s01Item.getIntMshFlg()==0 ) {
				throw new Exception("S01 ERROR : フラグ異常");
			}
			if ( s01Item.getIntMshFlg()==1 ) {
				log.Ep("S01_01","S01_01帳票作成開始","");
				S01_01 exceledit = new S01_01();
				ret = exceledit.s01_01EditExcel( s01Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S01_01","帳票作成エラー","");
					return false;
				}
				log.Ep("S01_01","S01_01帳票作成終了","");
				sheetLog.add("S01_01");
			}
			if ( s01Item.getIntMshFlg()==2 ) {
				log.Ep("S01_02","S01_02帳票作成開始","");
				S01_02 exceledit = new S01_02();
				ret = exceledit.s01_02EditExcel( s01Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S01_02","帳票作成エラー","");
					return false;
				}
				log.Ep("S01_02","S01_02帳票作成終了","");
				sheetLog.add("S01_02");
			}
			
		} catch(Exception e) {
			log.Err("99S01","帳票作成エラー",e.toString());
			return false;
		}
		return true;
	}

}
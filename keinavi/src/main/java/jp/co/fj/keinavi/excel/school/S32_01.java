/**
 * 校内成績分析−クラス比較　偏差値分布
 * 	Excelファイル編集
 * 作成日: 2004/08/05
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S32BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S32ClassListBean;
import jp.co.fj.keinavi.excel.data.school.S32Item;
import jp.co.fj.keinavi.excel.data.school.S32ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S32_01 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	final private int	intMaxSheetSr	= 50;			// 最大シート数	
	final private String	masterfile0		= "S32_01";		// ファイル名
	final private String	masterfile1		= "NS32_01";	// ファイル名
	private String	masterfile		= "";					// ファイル名
	final private int[]	tabRow			= {4,33};		// 表の基準点
	final private int[]	tabCol			= {1,4,7,10,13,16,19,22,25,28,31};	// 表の基準点

/*
 * 	Excel編集メイン
 * 		S32Item s32Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s32_01EditExcel(S32Item s32Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("S32_01","S32_01帳票作成開始","");
		
		//テンプレートの決定
		if (s32Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			
			// データセット
			ArrayList	s32List			= s32Item.getS32List();
			Iterator	itr				= s32List.iterator();
			int		row				= 0;	// 行
			int 		setRow			= -1;	// *セット用
			int 		ninzu			= 0;	// *作成用
			int		hyouCnt			= 0;	// 値範囲：０〜１　(表カウンター)
			int		classCnt		= 0;	// 値範囲：０〜９　(クラスカウンター)
			int		maxSheetIndex	= 0;	// シートカウンター
			int		intBookCngCount	= 0;	// ブックカウンター
			int		kmkCd			= 0;	// 科目コード保持
			boolean	kataFlg			= true;// false:改行済(型・科目名変更済)　
			boolean	bolGakkouFlg	= true;// true:学校情報表示　（学校情報表示フラグ）
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
			boolean	bolFirstDataFlg	= true;// true:List.Next()を実行
//add 2004/10/26 T.Sakai セル計算対応
			int		ruikeiNinzu		= 0;	// 累計人数
//add end

			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			// 基本ファイルを読込む
			S32ListBean s32ListBean = new S32ListBean();

			while( itr.hasNext() ) {
				s32ListBean = (S32ListBean)itr.next();

				// 基本ファイルを読み込む
				S32ClassListBean	s32ClassListBean	= new S32ClassListBean();

				//データの保持
				ArrayList	s32ClassList		= s32ListBean.getS32ClassList();
				Iterator	itrS32Class		= s32ClassList.iterator();
	
				// 型・科目の最初のデータ(=学校データ)
				bolFirstDataFlg=true;


				//　クラスデータセット
				classCnt = 0;
				while(itrS32Class.hasNext()){

					if( bolBookCngFlg == true ){
						// マスタExcel読み込み
						workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
						if( workbook==null ){
							return errfread;
						}
						bolSheetCngFlg	= true;
						bolBookCngFlg	= false;
						maxSheetIndex	= 0;
					}

					if(bolSheetCngFlg){
						// シートテンプレートのコピー
						workSheet = workbook.cloneSheet(0);
						maxSheetIndex++;

						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);

						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, s32Item.getIntSecuFlg() ,32 ,33 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
						workCell.setCellValue(secuFlg);

						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
						workCell.setCellValue( "学校名　　　：" + cm.toString(s32Item.getStrGakkomei()) );

						// 対象模試セット
						String moshi =cm.setTaisyouMoshi( s32Item.getStrMshDate() );	// 模試月取得
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( cm.getTargetExamLabel() + "　　：" + cm.toString(s32Item.getStrMshmei()) + moshi);

						kataFlg=true;
						bolSheetCngFlg=false;
					}

					if(kmkCd!=Integer.parseInt(s32ListBean.getStrKmkCd())){
						kataFlg=true;
					}
					// 型名・配点セット
					if(kataFlg){
						String haiten = "";
						if ( !cm.toString(s32ListBean.getStrHaitenKmk()).equals("") ) {
							haiten = "（" + s32ListBean.getStrHaitenKmk() + "）";
						}
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt], 0 );
						workCell.setCellValue( "型・科目：" + cm.toString(s32ListBean.getStrKmkmei()) + haiten );

						kmkCd=Integer.parseInt(s32ListBean.getStrKmkCd());
						bolGakkouFlg=true;
						kataFlg=false;
					}

					if(bolGakkouFlg){
						s32ClassListBean	= (S32ClassListBean) s32ClassList.get(0);

						// 学校情報読み飛ばし
						if(bolFirstDataFlg){
							s32ClassListBean	= (S32ClassListBean) itrS32Class.next();
							bolFirstDataFlg=false;
						}
					}else{
						s32ClassListBean	= (S32ClassListBean) itrS32Class.next();
					}

					S32BnpListBean s32BnpListBean = new S32BnpListBean();
					ArrayList	s32BnpList		= s32ClassListBean.getS32BnpList();
					Iterator	itrS32Bnp;
					itrS32Bnp		= s32BnpList.iterator();

					// 偏差値データセット
					row=0;
//add 2004/10/26 T.Sakai セル計算対応
					ruikeiNinzu = 0;
//add end
					
					while(itrS32Bnp.hasNext()){
						s32BnpListBean = (S32BnpListBean) itrS32Bnp.next();

						if(row==0){
							if(bolGakkouFlg){
								// 高校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+1, tabCol[classCnt] );
								workCell.setCellValue( s32Item.getStrGakkomei() );

								bolGakkouFlg=false;
							}else{
								// クラス名セット
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+1, tabCol[classCnt] );
								workCell.setCellValue( cm.toString(s32ClassListBean.getStrGrade())+"年 "+cm.toString(s32ClassListBean.getStrClass())+"クラス" );
							}

							// 合計人数セット
							if(s32ClassListBean.getIntNinzu()!=-999){
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+23, tabCol[classCnt] );
								workCell.setCellValue( s32ClassListBean.getIntNinzu() );
							}

							// 平均点セット
							if(s32ClassListBean.getFloHeikin()!=-999.0){
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+24, tabCol[classCnt] );
								workCell.setCellValue( s32ClassListBean.getFloHeikin() );
							}

							// 平均偏差値セット
							if(s32ClassListBean.getFloHensa()!=-999.0){
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+25, tabCol[classCnt] );
								workCell.setCellValue( s32ClassListBean.getFloHensa() );
							}
						}

						// 人数セット
						if(s32BnpListBean.getIntNinzu()!=-999){
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+row+4, tabCol[classCnt]+1 );
							workCell.setCellValue( s32BnpListBean.getIntNinzu() );

							// *セット準備
							if( s32BnpListBean.getIntNinzu() > ninzu ){
								ninzu = s32BnpListBean.getIntNinzu();
								setRow = row;
							}
						}
//add 2004/10/26 T.Sakai セル計算対応
						// 累計人数セット
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+row+4, tabCol[classCnt]+2 );
						if ( s32BnpListBean.getIntNinzu() != -999 ) {
							ruikeiNinzu = ruikeiNinzu + s32BnpListBean.getIntNinzu();
						}
						workCell.setCellValue( ruikeiNinzu );

						// 得点用のときは処理しない
						if (s32Item.getIntShubetsuFlg() == 1){
						} else{
							if (s32BnpListBean.getFloBnpMin()==60.0f) {
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+26, tabCol[classCnt] );
								workCell.setCellValue( ruikeiNinzu );
							}
							if (s32BnpListBean.getFloBnpMin()==50.0f) {
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+27, tabCol[classCnt] );
								workCell.setCellValue( ruikeiNinzu );
							}
						}
//add end
						
						row++;

						// *セット
						if(row >=19){
							if(setRow!=-1){
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+setRow+4, tabCol[classCnt] );
								workCell.setCellValue("*");
							}
							ninzu=0;
							setRow=-1;
							break;
						}

					}

					classCnt++;
					if(classCnt>=11){
						classCnt=0;
						hyouCnt++;
						if(hyouCnt>1){
							bolSheetCngFlg=true;
							hyouCnt=0;
						}
					}

					if(bolSheetCngFlg){
						//
						if((maxSheetIndex >= intMaxSheetSr)&&(itrS32Class.hasNext())){
							bolBookCngFlg = true;
						}

				
						if( bolBookCngFlg == true){
							boolean bolRet = false;
							// Excelファイル保存
							if(intBookCngCount==0){
								if(itr.hasNext() == true){
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
								}
								else{
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
								}
							}else{
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
							}

							if( bolRet == false ){
								return errfwrite;
							}
							intBookCngCount++;
						}
					}
				}

				if(classCnt!=0){
					hyouCnt++;
					classCnt=0;
					if(hyouCnt>1){
						bolSheetCngFlg=true;
						hyouCnt=0;
					}
				}

				if(bolSheetCngFlg){
					//
					if(maxSheetIndex >= intMaxSheetSr){
						bolBookCngFlg = true;
					}

				
					if( bolBookCngFlg == true){
						boolean bolRet = false;
						// Excelファイル保存
						if(intBookCngCount==0){
							if(itr.hasNext() == true){
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
							}
							else{
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
							}
						}else{
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
						}

						if( bolRet == false ){
							return errfwrite;
						}
						intBookCngCount++;
					}
				}

			}

			if( bolBookCngFlg == false){
				boolean bolRet = false;
				// Excelファイル保存
				if(intBookCngCount!=0){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
			}
			
		} catch(Exception e) {
			log.Err("S32_01","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S32_01","S32_01帳票作成終了","");
		return noerror;
	}

}
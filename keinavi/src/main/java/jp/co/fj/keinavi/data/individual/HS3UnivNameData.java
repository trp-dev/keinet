/*
 * 作成日: 2004/08/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class HS3UnivNameData {
	
	private String examYear;
	private String examDiv;
	private String univCd;
	private String facultyCd;
	private String deptCd;
	private String univDivCd;
	private String univName_Abbr;
	private String facultyName_Abbr;
	private String deptName_Abbr;
	private String superAbbrName;
	private String univName_Kana;
	private String facultyName_Kana;
	private String deptName_Kana;
	private String scheduleCd;
	private String prefCd_Hq;
	private String prefCd_Sh;
	private String cen_Concentrate;
	private String cen_Border;
	private String cen_Attention;
	private String cen_ScoreRate;
	private String bunriCd;
	private String capacity;
	private String coedDiv;
	private String nightFlg;
	private String rankCd;
	private String deptSortKey;
	
	/**
	 * @return
	 */
	public String getBunriCd() {
		return bunriCd;
	}

	/**
	 * @return
	 */
	public String getCapacity() {
		return capacity;
	}

	/**
	 * @return
	 */
	public String getCen_Attention() {
		return cen_Attention;
	}

	/**
	 * @return
	 */
	public String getCen_Border() {
		return cen_Border;
	}

	/**
	 * @return
	 */
	public String getCen_Concentrate() {
		return cen_Concentrate;
	}

	/**
	 * @return
	 */
	public String getCen_ScoreRate() {
		return cen_ScoreRate;
	}

	/**
	 * @return
	 */
	public String getCoedDiv() {
		return coedDiv;
	}

	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getDeptName_Abbr() {
		return deptName_Abbr;
	}

	/**
	 * @return
	 */
	public String getDeptName_Kana() {
		return deptName_Kana;
	}

	/**
	 * @return
	 */
	public String getDeptSortKey() {
		return deptSortKey;
	}

	/**
	 * @return
	 */
	public String getExamDiv() {
		return examDiv;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getFacultyName_Abbr() {
		return facultyName_Abbr;
	}

	/**
	 * @return
	 */
	public String getFacultyName_Kana() {
		return facultyName_Kana;
	}

	/**
	 * @return
	 */
	public String getNightFlg() {
		return nightFlg;
	}

	/**
	 * @return
	 */
	public String getPrefCd_Hq() {
		return prefCd_Hq;
	}

	/**
	 * @return
	 */
	public String getPrefCd_Sh() {
		return prefCd_Sh;
	}

	/**
	 * @return
	 */
	public String getRankCd() {
		return rankCd;
	}

	/**
	 * @return
	 */
	public String getScheduleCd() {
		return scheduleCd;
	}

	/**
	 * @return
	 */
	public String getSuperAbbrName() {
		return superAbbrName;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @return
	 */
	public String getUnivDivCd() {
		return univDivCd;
	}

	/**
	 * @return
	 */
	public String getUnivName_Abbr() {
		return univName_Abbr;
	}

	/**
	 * @return
	 */
	public String getUnivName_Kana() {
		return univName_Kana;
	}

	/**
	 * @param string
	 */
	public void setBunriCd(String string) {
		bunriCd = string;
	}

	/**
	 * @param string
	 */
	public void setCapacity(String string) {
		capacity = string;
	}

	/**
	 * @param string
	 */
	public void setCen_Attention(String string) {
		cen_Attention = string;
	}

	/**
	 * @param string
	 */
	public void setCen_Border(String string) {
		cen_Border = string;
	}

	/**
	 * @param string
	 */
	public void setCen_Concentrate(String string) {
		cen_Concentrate = string;
	}

	/**
	 * @param string
	 */
	public void setCen_ScoreRate(String string) {
		cen_ScoreRate = string;
	}

	/**
	 * @param string
	 */
	public void setCoedDiv(String string) {
		coedDiv = string;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setDeptName_Abbr(String string) {
		deptName_Abbr = string;
	}

	/**
	 * @param string
	 */
	public void setDeptName_Kana(String string) {
		deptName_Kana = string;
	}

	/**
	 * @param string
	 */
	public void setDeptSortKey(String string) {
		deptSortKey = string;
	}

	/**
	 * @param string
	 */
	public void setExamDiv(String string) {
		examDiv = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyName_Abbr(String string) {
		facultyName_Abbr = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyName_Kana(String string) {
		facultyName_Kana = string;
	}

	/**
	 * @param string
	 */
	public void setNightFlg(String string) {
		nightFlg = string;
	}

	/**
	 * @param string
	 */
	public void setPrefCd_Hq(String string) {
		prefCd_Hq = string;
	}

	/**
	 * @param string
	 */
	public void setPrefCd_Sh(String string) {
		prefCd_Sh = string;
	}

	/**
	 * @param string
	 */
	public void setRankCd(String string) {
		rankCd = string;
	}

	/**
	 * @param string
	 */
	public void setScheduleCd(String string) {
		scheduleCd = string;
	}

	/**
	 * @param string
	 */
	public void setSuperAbbrName(String string) {
		superAbbrName = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

	/**
	 * @param string
	 */
	public void setUnivDivCd(String string) {
		univDivCd = string;
	}

	/**
	 * @param string
	 */
	public void setUnivName_Abbr(String string) {
		univName_Abbr = string;
	}

	/**
	 * @param string
	 */
	public void setUnivName_Kana(String string) {
		univName_Kana = string;
	}

}

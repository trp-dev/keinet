package jp.co.fj.keinavi.beans.security.config;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 *
 * セキュリティ設定データクラス
 * 
 * 2005.09.27	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class SecurityConfig implements Serializable {

	// 利用者設定
	private Map userSecurityMap;
	// 画面設定
	private Map screenSecurityMap;
	// 機能定義リスト
	private final List functionList = new LinkedList();
	
	/**
	 * コンストラクタ
	 */
	public SecurityConfig() {
		super();
	}

	/**
	 * @return functionList を戻します。
	 */
	public List getFunctionList() {
		return functionList;
	}

	/**
	 * @return screenSecurityMap を戻します。
	 */
	public Map getScreenSecurityMap() {
		return screenSecurityMap;
	}

	/**
	 * @param screenSecurityMap 設定する screenSecurityMap。
	 */
	public void setScreenSecurityMap(Map screenSecurityMap) {
		this.screenSecurityMap = screenSecurityMap;
	}

	/**
	 * @return userSecurityMap を戻します。
	 */
	public Map getUserSecurityMap() {
		return userSecurityMap;
	}

	/**
	 * @param userSecurityMap 設定する userSecurityMap。
	 */
	public void setUserSecurityMap(Map userSecurityMap) {
		this.userSecurityMap = userSecurityMap;
	}

	public void addUserFunction(final String type, final String fid) {
		
		// 既に入れ物がある場合
		if (userSecurityMap.containsKey(type)) {
			List container = (List) userSecurityMap.get(type);
			container.add(fid);
			
		// 入れ物がなければ作る
		} else {
			List container = new LinkedList();
			container.add(fid);
			userSecurityMap.put(type, container);
		}
	}
	
	/**
	 * 機能設定を追加する
	 * 
	 * @param function
	 */
	public void addFunction(final Function function) {

		functionList.add(function);
	}
	
	/* (非 Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		
		return new ToStringBuilder(this)
			.append("User", userSecurityMap)
			.append("Screen", screenSecurityMap)
			.append("Function", functionList)
			.toString();
	}
	
}

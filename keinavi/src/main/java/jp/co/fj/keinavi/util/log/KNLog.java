package jp.co.fj.keinavi.util.log;

/**
 * Kei-Net共通ログクラス
 * 
 * 2005.02.23 Yoshimoto KAWAI - Totec
 *            利用者ＩＤの出力に対応
 * 
 * @Date		2004/06/07
 * @author	TOTEC)Nishiyama
 */
public class KNLog {
	
	/**
	 * IPアドレス
	 */
	private String ipAddress;
	
	/**
	 * 学校コード
	 */
	private String schoolCd;
	
	/**
	 * 部門分類コード
	 */
	private String sectorSortingCd;
	
	/**
	 * 利用者ＩＤ
	 */
	private String account;
	
	/**
	 * コンストラクタ
	 * @param ipAddress
	 * @param schoolCd
	 * @param sectorSortingCd
	 * @param account
	 */
	public KNLog(String ipAddress, String schoolCd, String sectorSortingCd,
			String account) {
		super();
		this.ipAddress = ipAddress;
		this.schoolCd = schoolCd;
		this.sectorSortingCd = sectorSortingCd;
		this.account = account;
	}

	/**
	 * インスタンスを取得する。
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @return ログのインスタンス
	 */
	public static KNLog getInstance(final String ipAddress,
									final String schoolCd,
									final String sectorSortingCd) {
		
		return new KNLog(ipAddress, schoolCd, sectorSortingCd, null);
	}
	
	/**
	 * インスタンスを取得する。
	 *
	 * @param IPAddress	 IPアドレス
	 * @param ContractID 契約ID
	 * @param EmployeeID 職員コード
	 * @param personalId 利用者ＩＤ
	 * @return ログのインスタンス
	 */
	public static KNLog getInstance(final String ipAddress,
									final String schoolCd,
									final String sectorSortingCd,
									final String account) {
		
		return new KNLog(ipAddress, schoolCd, sectorSortingCd, account);
	}
	
//	// ---------- ここから消す ----------------------------------------------------------------------	
//	
//	/**
//	 * アクセスログ（Lv1）を出力する
//	 *
//	 * @param IPAddress			IPアドレス
//	 * @param ContractID			契約ID
//	 * @param EmployeeID			職員コード
//	 * @param TransactionID	処理ID
//	 * @param ExecutiveID		実行条件
//	 * @param SupplementInfo	補足情報
//	 */
//	public static void Lv1(String IPAddress,
//						String ContractID,
//						String EmployeeID,
//						String TransactionID,
//						int ExecutiveID,
//						String SupplementInfo) {
//		AccessLog.Lv1(IPAddress, ContractID, EmployeeID, null, TransactionID, ExecutiveID, SupplementInfo);
//	}
//
//	/**
//	 * アクセスログ（Lv2）を出力する
//	 *
//	 * @param IPAddress			IPアドレス
//	 * @param ContractID			契約ID
//	 * @param EmployeeID			職員コード
//	 * @param TransactionID	処理ID
//	 * @param ExecutiveID		実行条件
//	 * @param SupplementInfo	補足情報
//	 */
//	public static void Lv2(String IPAddress,
//						String ContractID,
//						String EmployeeID,
//						String TransactionID,
//						int ExecutiveID,
//						String SupplementInfo) {
//		AccessLog.Lv2(IPAddress, ContractID, EmployeeID, null, TransactionID, ExecutiveID, SupplementInfo);
//	}
//	
//	/**
//	 * ERRログを出力する
//	 *
//	 * @param IPAddress			IPアドレス
//	 * @param ContractID			契約ID
//	 * @param EmployeeID			職員コード
//	 * @param ErrCode				エラーコード
//	 * @param ErrContents		エラー内容
//	 * @param SupplementInfo	補足情報
//	 */
//	public static void Err(String IPAddress,
//						String ContractID,
//						String EmployeeID,
//						String ErrCode,
//						String ErrContents,
//						String SupplementInfo) {
//		ErrorLog.Err(IPAddress, ContractID, EmployeeID, null, ErrCode, ErrContents, SupplementInfo);
//	}
//
//	/**
//	 * ERRログを出力する
//	 *
//	 * @param IPAddress			IPアドレス
//	 * @param ContractID			契約ID
//	 * @param EmployeeID			職員コード
//	 * @param ErrCode				エラーコード
//	 * @param ErrContents		エラー内容
//	 * @param Supplement			補足情報
//	 */
//	public static void Err(String IPAddress,
//						String ContractID,
//						String EmployeeID,
//						String ErrCode,
//						String ErrContents,
//						Throwable Supplement) {
//		ErrorLog.Err(IPAddress, ContractID, EmployeeID, null, ErrCode, ErrContents, Supplement);
//	}
//
//	/**
//	 * WARログを出力する
//	 *
//	 * @param IPAddress			IPアドレス
//	 * @param ContractID			契約ID
//	 * @param EmployeeID			職員コード
//	 * @param ErrCode				エラーコード
//	 * @param ErrContents		エラー内容
//	 * @param SupplementInfo	補足情報
//	 */
//	public static void War(String IPAddress,
//						String ContractID,
//						String EmployeeID,
//						String ErrCode,
//						String ErrContents,
//						String SupplementInfo) {
//		ErrorLog.War(IPAddress, ContractID, EmployeeID, null, ErrCode, ErrContents, SupplementInfo);
//	}
//
//	/**
//	 * WARログを出力する
//	 *
//	 * @param IPAddress			IPアドレス
//	 * @param ContractID			契約ID
//	 * @param EmployeeID			職員コード
//	 * @param ErrCode				エラーコード
//	 * @param ErrContents		エラー内容
//	 * @param Supplement			補足情報
//	 */
//	public static void War(String IPAddress,
//						String ContractID,
//						String EmployeeID,
//						String ErrCode,
//						String ErrContents,
//						Throwable Supplement) {
//		ErrorLog.War(IPAddress, ContractID, EmployeeID, null, ErrCode, ErrContents, Supplement);
//	}
//	
//	/**
//	 * INFログを出力する
//	 *
//	 * @param IPAddress			IPアドレス
//	 * @param ContractID			契約ID
//	 * @param EmployeeID			職員コード
//	 * @param ErrCode				エラーコード
//	 * @param ErrContents		エラー内容
//	 * @param SupplementInfo	補足情報
//	 */
//	public static void Inf(String IPAddress,
//						String ContractID,
//						String EmployeeID,
//						String ErrCode,
//						String ErrContents,
//						String SupplementInfo) {
//		ErrorLog.Inf(IPAddress, ContractID, EmployeeID, null, ErrCode, ErrContents, SupplementInfo);
//	}
//
//	/**
//	 * INFログを出力する
//	 *
//	 * @param IPAddress			IPアドレス
//	 * @param ContractID			契約ID
//	 * @param EmployeeID			職員コード
//	 * @param ErrCode				エラーコード
//	 * @param ErrContents		エラー内容
//	 * @param Supplement			補足情報
//	 */
//	public static void Inf(String IPAddress,
//						String ContractID,
//						String EmployeeID,
//						String ErrCode,
//						String ErrContents,
//						Throwable Supplement) {
//		ErrorLog.Inf(IPAddress, ContractID, EmployeeID, null, ErrCode, ErrContents, Supplement);
//	}
//
//	/**
//	 * PTログを出力する
//	 *
//	 * @param IPAddress			IPアドレス
//	 * @param ContractID			契約ID
//	 * @param EmployeeID			職員コード
//	 * @param ErrCode				エラーコード
//	 * @param ErrContents		エラー内容
//	 * @param SupplementInfo	補足情報
//	 */
//	public static void Pt(String IPAddress,
//						String ContractID,
//						String EmployeeID,
//						String ErrCode,
//						String ErrContents,
//						String SupplementInfo) {
//		DebugLog.Pt(IPAddress, ContractID, EmployeeID, null, ErrCode, ErrContents, SupplementInfo);
//	}
//
//	/**
//	 * PTログを出力する
//	 *
//	 * @param IPAddress			IPアドレス
//	 * @param ContractID			契約ID
//	 * @param EmployeeID			職員コード
//	 * @param ErrCode				エラーコード
//	 * @param ErrContents		エラー内容
//	 * @param Supplement			補足情報
//	 */
//	public static void Pt(String IPAddress,
//						String ContractID,
//						String EmployeeID,
//						String ErrCode,
//						String ErrContents,
//						Throwable Supplement) {
//		DebugLog.Pt(IPAddress, ContractID, EmployeeID, null, ErrCode, ErrContents, Supplement);	
//	}
//
//	/**
//	 * ITログを出力する
//	 *
//	 * @param IPAddress			IPアドレス
//	 * @param ContractID			契約ID
//	 * @param EmployeeID			職員コード
//	 * @param ErrCode				エラーコード
//	 * @param ErrContents		エラー内容
//	 * @param SupplementInfo	補足情報
//	 */
//	public static void It(String IPAddress,
//						String ContractID,
//						String EmployeeID,
//						String ErrCode,
//						String ErrContents,
//						String SupplementInfo) {
//		DebugLog.It(IPAddress, ContractID, EmployeeID, null, ErrCode, ErrContents, SupplementInfo);
//	}
//
//	/**
//	 * ITログを出力する
//	 *
//	 * @param IPAddress			IPアドレス
//	 * @param ContractID			契約ID
//	 * @param EmployeeID			職員コード
//	 * @param ErrCode				エラーコード
//	 * @param ErrContents		エラー内容
//	 * @param Supplement			補足情報
//	 */
//	public static void It(String IPAddress,
//						String ContractID,
//						String EmployeeID,
//						String ErrCode,
//						String ErrContents,
//						Throwable Supplement) {
//		DebugLog.It(IPAddress, ContractID, EmployeeID, null, ErrCode, ErrContents, Supplement);
//	}
//
//	/**
//	 * EPログを出力する
//	 *
//	 * @param IPAddress			IPアドレス
//	 * @param ContractID			契約ID
//	 * @param EmployeeID			職員コード
//	 * @param ErrCode				エラーコード
//	 * @param ErrContents		エラー内容
//	 * @param SupplementInfo	補足情報
//	 */
//	public static void Ep(String IPAddress,
//						String ContractID,
//						String EmployeeID,
//						String ErrCode,
//						String ErrContents,
//						String SupplementInfo) {
//		DebugLog.Ep(IPAddress, ContractID, EmployeeID, null, ErrCode, ErrContents, SupplementInfo);
//	}
//
//	/**
//	 * EPログを出力する
//	 *
//	 * @param IPAddress			IPアドレス
//	 * @param ContractID			契約ID
//	 * @param EmployeeID			職員コード
//	 * @param ErrCode				エラーコード
//	 * @param ErrContents		エラー内容
//	 * @param Supplement			補足情報
//	 */
//	public static void Ep(String IPAddress,
//						String ContractID,
//						String EmployeeID,
//						String ErrCode,
//						String ErrContents,
//						Throwable Supplement) {
//		DebugLog.Ep(IPAddress, ContractID, EmployeeID, null, ErrCode, ErrContents, Supplement);
//	}
//
//	// ---------- ここまで消す ----------------------------------------------------------------------	
	
	/**
	 * アクセスログ（Lv1）を出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param TransactionID	処理ID
	 * @param ExecutiveID		実行条件
	 * @param SupplementInfo	補足情報
	 */
	public static void Lv1(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String TransactionID,
						int ExecutiveID,
						String SupplementInfo) {
		AccessLog.Lv1(IPAddress, ContractID, EmployeeID, account, TransactionID, ExecutiveID, SupplementInfo);
	}

	/**
	 * アクセスログ（Lv2）を出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param TransactionID	処理ID
	 * @param ExecutiveID		実行条件
	 * @param SupplementInfo	補足情報
	 */
	public static void Lv2(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String TransactionID,
						int ExecutiveID,
						String SupplementInfo) {
		AccessLog.Lv2(IPAddress, ContractID, EmployeeID, account, TransactionID, ExecutiveID, SupplementInfo);
	}
	
	/**
	 * ERRログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	public static void Err(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String ErrCode,
						String ErrContents,
						String SupplementInfo) {
		ErrorLog.Err(IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * ERRログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param Supplement			補足情報
	 */
	public static void Err(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String ErrCode,
						String ErrContents,
						Throwable Supplement) {
		ErrorLog.Err(IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, Supplement);
	}

	/**
	 * WARログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	public static void War(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String ErrCode,
						String ErrContents,
						String SupplementInfo) {
		ErrorLog.War(IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * WARログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param Supplement			補足情報
	 */
	public static void War(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String ErrCode,
						String ErrContents,
						Throwable Supplement) {
		ErrorLog.War(IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, Supplement);
	}
	
	/**
	 * INFログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	public static void Inf(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String ErrCode,
						String ErrContents,
						String SupplementInfo) {
		ErrorLog.Inf(IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * INFログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param Supplement			補足情報
	 */
	public static void Inf(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String ErrCode,
						String ErrContents,
						Throwable Supplement) {
		ErrorLog.Inf(IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, Supplement);
	}

	/**
	 * PTログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	public static void Pt(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String ErrCode,
						String ErrContents,
						String SupplementInfo) {
		DebugLog.Pt(IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * PTログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param Supplement			補足情報
	 */
	public static void Pt(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String ErrCode,
						String ErrContents,
						Throwable Supplement) {
		DebugLog.Pt(IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, Supplement);	
	}

	/**
	 * ITログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	public static void It(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String ErrCode,
						String ErrContents,
						String SupplementInfo) {
		DebugLog.It(IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * ITログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param Supplement			補足情報
	 */
	public static void It(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String ErrCode,
						String ErrContents,
						Throwable Supplement) {
		DebugLog.It(IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, Supplement);
	}

	/**
	 * EPログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	public static void Ep(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String ErrCode,
						String ErrContents,
						String SupplementInfo) {
		DebugLog.Ep(IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * EPログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param Supplement			補足情報
	 */
	public static void Ep(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String ErrCode,
						String ErrContents,
						Throwable Supplement) {
		DebugLog.Ep(IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, Supplement);
	}
	
	/**
	 * アクセスログ（Lv1）を出力する
	 *
	 * @param TransactionID		処理ID
	 * @param ExecutiveID			実行条件
	 * @param SupplementInfo		補足情報
	 */
	public void Lv1(String TransactionID,
					int ExecutiveID,
					String SupplementInfo) {
		AccessLog.Lv1(ipAddress, schoolCd, sectorSortingCd, account, TransactionID, ExecutiveID, SupplementInfo);
	}

	/**
	 * アクセスログ（Lv2）を出力する
	 *
	 * @param TransactionID		処理ID
	 * @param ExecutiveID			実行条件
	 * @param SupplementInfo		補足情報
	 */
	public void Lv2(String TransactionID,
					int ExecutiveID,
					String SupplementInfo) {
		AccessLog.Lv2(ipAddress, schoolCd, sectorSortingCd, account, TransactionID, ExecutiveID, SupplementInfo);
	}
	
	/**
	 * ERRログを出力する
	 *
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	public void Err(String ErrCode,
					String ErrContents,
					String SupplementInfo) {
		ErrorLog.Err(ipAddress, schoolCd, sectorSortingCd, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * ERRログを出力する
	 *
	 * @param ErrCode			エラーコード
	 * @param ErrContents	エラー内容
	 * @param Supplement		補足情報
	 */
	public void Err(String ErrCode,
					String ErrContents,
					Throwable Supplement) {
		ErrorLog.Err(ipAddress, schoolCd, sectorSortingCd, account, ErrCode, ErrContents, Supplement);
	}

	/**
	 * WARログを出力する
	 *
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	public void War(String ErrCode,
					String ErrContents,
					String SupplementInfo) {
		ErrorLog.War(ipAddress, schoolCd, sectorSortingCd, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * WARログを出力する
	 *
	 * @param ErrCode			エラーコード
	 * @param ErrContents	エラー内容
	 * @param Supplement		補足情報
	 */
	public void War(String ErrCode,
					String ErrContents,
					Throwable Supplement) {
		ErrorLog.War(ipAddress, schoolCd, sectorSortingCd, account, ErrCode, ErrContents, Supplement);
	}
	
	/**
	 * INFログを出力する
	 *
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	public void Inf(String ErrCode,
					String ErrContents,
					String SupplementInfo) {
		ErrorLog.Inf(ipAddress, schoolCd, sectorSortingCd, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * INFログを出力する
	 *
	 * @param ErrCode			エラーコード
	 * @param ErrContents	エラー内容
	 * @param Supplement		補足情報
	 */
	public void Inf(String ErrCode,
					String ErrContents,
					Throwable Supplement) {
		ErrorLog.Inf(ipAddress, schoolCd, sectorSortingCd, account, ErrCode, ErrContents, Supplement);
	}

	/**
	 * PTログを出力する
	 *
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	public void Pt(String ErrCode,
					String ErrContents,
					String SupplementInfo) {
		DebugLog.Pt(ipAddress, schoolCd, sectorSortingCd, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * PTログを出力する
	 *
	 * @param ErrCode			エラーコード
	 * @param ErrContents	エラー内容
	 * @param Supplement		補足情報
	 */
	public void Pt(String ErrCode,
					String ErrContents,
					Throwable Supplement) {
		DebugLog.Pt(ipAddress, schoolCd, sectorSortingCd, account, ErrCode, ErrContents, Supplement);
	}

	/**
	 * ITログを出力する
	 *
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	public void It(String ErrCode,
					String ErrContents,
					String SupplementInfo) {
		DebugLog.It(ipAddress, schoolCd, sectorSortingCd, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * ITログを出力する
	 *
	 * @param ErrCode			エラーコード
	 * @param ErrContents	エラー内容
	 * @param Supplement		補足情報
	 */
	public void It(String ErrCode,
					String ErrContents,
					Throwable Supplement) {
		DebugLog.It(ipAddress, schoolCd, sectorSortingCd, account, ErrCode, ErrContents, Supplement);
	}

	/**
	 * EPログを出力する
	 *
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	public void Ep(String ErrCode,
					String ErrContents,
					String SupplementInfo) {
		DebugLog.Ep(ipAddress, schoolCd, sectorSortingCd, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * EPログを出力する
	 *
	 * @param ErrCode			エラーコード
	 * @param ErrContents	エラー内容
	 * @param Supplement		補足情報
	 */
	public void Ep(String ErrCode,
					String ErrContents,
					Throwable Supplement) {
		DebugLog.Ep(ipAddress, schoolCd, sectorSortingCd, account, ErrCode, ErrContents, Supplement);
	}
}
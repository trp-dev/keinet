package jp.co.fj.keinavi.data.com_set.cm;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * 学年別の担当クラスデータ
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class ClassGradeData {

	// 学年
	private final int grade;
	// 担当クラスデータのリスト
	private final List classList = new ArrayList();
	
	/**
	 * コンストラクタ
	 */
	public ClassGradeData(final int grade) {
		
		this.grade = grade;
	}

	/**
	 * 担当クラスデータを追加する
	 * 
	 * @param data
	 */
	public void addClassData(final ClassData data) {
		
		this.classList.add(data);
	}
	
	/**
	 * この学年の担当クラス数を取得する
	 * 
	 * @return
	 */
	public int getSize() {
		
		return classList.size();
	}
	
	/**
	 * この学年の担当クラス数の半分のサイズを取得する
	 * 
	 * @return
	 */
	public int getHalfSize() {
		
		return Math.round((float) classList.size() / 2);
	}
	
	/**
	 * @return classList を戻します。
	 */
	public List getClassList() {
		return classList;
	}

	/**
	 * @return grade を戻します。
	 */
	public int getGrade() {
		return grade;
	}

}

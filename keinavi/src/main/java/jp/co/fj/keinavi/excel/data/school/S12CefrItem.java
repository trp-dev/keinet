package jp.co.fj.keinavi.excel.data.school;

/**
 * Zà¬ÑªÍ|Zà¬Ñ|Î·lªz S12_11_pêCEFR±Êæ¾óµ
 * ì¬ú: 2019/09/30
 * @author QQ
 *
 */
public class S12CefrItem {

        //±¼
        private String strShortName = "";

        //CEFRx¼
        private String strCerfLevelNameAbbr = "";

        //pê±R[h
        private String strEngPtCd = "";

        //Zà@ó±Ò
        private int intNumbersS = 0;

        //Zà@\¬ä
        private float floCompRatioS = 0;

        //§à@ó±Ò
        private int intNumbersP = 0;

        //§à@\¬ä
        private float floCompRatioP = 0;

        //S@ó±Ò
        private int intNumbersA = 0;

        //S@\¬ä
        private float floCompRatioA = 0;


        /*----------*/
        /* Get      */
        /*----------*/
        public String getStrShortName() {
            return strShortName;
        }

        public String getStrCerfLevelNameAbbr() {
            return strCerfLevelNameAbbr;
        }



        public String getStrEngPtCd() {
            return strEngPtCd;
        }

        public int getIntNumbersS() {
            return intNumbersS;
        }

        public float getFloCompRatioS() {
            return floCompRatioS;
        }

        public int getIntNumbersP() {
            return intNumbersP;
        }

        public float getFloCompRatioP() {
            return floCompRatioP;
        }

        public int getIntNumbersA() {
            return intNumbersA;
        }

        public float getFloCompRatioA() {
            return floCompRatioA;
        }

        /*----------*/
        /* Set      */
        /*----------*/

        public void setStrShortName(String strShortName) {
            this.strShortName = strShortName;
        }

        public void setStrCerfLevelNameAbbr(String strCerfLevelNameAbbr) {
            this.strCerfLevelNameAbbr = strCerfLevelNameAbbr;
        }

        public void setStrEngPtCd(String strEngPtCd) {
            this.strEngPtCd = strEngPtCd;
        }


        public void setIntNumbersS(int intNumbersS) {
            this.intNumbersS = intNumbersS;
        }

        public void setFloCompRatioS(float floCompRatioS) {
            this.floCompRatioS = floCompRatioS;
        }

        public void setIntNumbersP(int intNumbersP) {
            this.intNumbersP = intNumbersP;
        }

        public void setFloCompRatioP(float floCompRatioP) {
            this.floCompRatioP = floCompRatioP;
        }

        public void setIntNumbersA(int intNumbersA) {
            this.intNumbersA = intNumbersA;
        }

        public void setFloCompRatioA(float floCompRatioA) {
            this.floCompRatioA = floCompRatioA;
        }



}

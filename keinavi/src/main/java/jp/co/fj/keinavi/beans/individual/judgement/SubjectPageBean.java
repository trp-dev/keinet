package jp.co.fj.keinavi.beans.individual.judgement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import jp.co.fj.kawaijuku.judgement.data.UnivData;

import com.fjh.beans.DefaultBean;

/**
 *
 * 公表科目表示用データです。
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KONDO.Keisuke
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SubjectPageBean extends DefaultBean {

    /** serialVersionUID */
    private static final long serialVersionUID = -4143541974730716758L;

    private String univName;
    private String facultyName;
    private String deptName;
    private String prefName;//所在地
    private String prefName_sh;//校舎所在地（県名）//[1] add
    private int capacity;//定員
    private String dateCorrelation;//入試日文字列

    /**
     * 大学情報
     */
    private UnivData univ;

    /**
     * DetailPageBeanを枝番ごとに保持する
     */
    private List detailPageList = new ArrayList();

    /**
     * @return 大学基本情報
     */
    public UnivData getUniv() {
        return univ;
    }

    /**
     * コンストラクタです。
     *
     * @deprecated <jsp:useBean>タグ用のコンストラクタ
     */
    public SubjectPageBean() {
    }

    /**
     * コンストラクタです。
     *
     * @param univ {@link UnivData}
     */
    public SubjectPageBean(UnivData univ) {
        this.univ = univ;
    }

    /**
     * @deprecated {@link #execute(Map)} に移行
     */
    public void execute() throws Exception {
    }

    /**
     * 情報誌用科目データから表示用の枝番リストを作ります。
     *
     * @param univInfoMap 情報誌用科目データマップ
     */
    public void execute(Map univInfoMap) throws Exception {

        setupParams();

        if (univInfoMap != null) {
            SortedMap map = (SortedMap) univInfoMap.get(getUniv().getUniqueKey());
            if (map != null) {
                getDetailPageList().addAll(map.values());
            }
        }
    }

    /**
     * 変数設定
     * @throws Exception
     */
    protected void setupParams() throws Exception {

        //既存処理を生かすために割り当て
        univName = univ.getUnivName();
        facultyName = univ.getDeptName();
        deptName = univ.getSubName();
        prefName = univ.getPrefName();
        prefName_sh = univ.getPrefName_sh();
        capacity = univ.getCapacity();
        dateCorrelation = univ.toInpleDateStrFull();
    }

    /**
     * @return
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * @return
     */
    public List getDetailPageList() {
        return detailPageList;
    }

    /**
     * @return
     */
    public String getFacultyName() {
        return facultyName;
    }

    /**
     * @return
     */
    public String getUnivName() {
        return univName;
    }

    /**
     * @return
     */
    public String getDateCorrelation() {
        return dateCorrelation;
    }

    /**
     * @return
     */
    public String getPrefName() {
        return prefName;
    }

    /**
     * @return
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * 校舎県名を返します
     */
    public String getPrefName_sh() {
        return prefName_sh;//[1] add
    }

    /**
     * 入試定員信頼性を返す。
     *
     * @return 入試定員信頼性
     */
    public String getExamCapaRel() {
        return univ.getExamCapaRel();
    }

}

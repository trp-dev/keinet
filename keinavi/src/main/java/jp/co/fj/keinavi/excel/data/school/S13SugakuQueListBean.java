package jp.co.fj.keinavi.excel.data.school;

/**
 * 設問別成績（校内成績）小設問別正答状況（数学）データリスト
 * 作成日: 2019/09/10
 * @author	Yuko NAGAI
 */
public class S13SugakuQueListBean {

	//科目コード
	private String strKmkCd = "";
	//型・科目名
	private String strKmkmei = "";
	//配点
	private String strHaiten = "";
	// 設問番号
	private String strMonnum = "";
	// 設問名称
	private String strQuestionName = "";
	// 校内平均点
	private float FloHomeAvgpnt = 0;
	// 全国平均点
	private float FloAllAvgpnt = 0;

	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return strKmkCd;
	}
	public String getStrKmkmei() {
		return strKmkmei;
	}
	public String getStrHaiten() {
		return strHaiten;
	}
	public String getStrMonnum() {
		return strMonnum;
	}
	public String getStrQuestionName() {
		return strQuestionName;
	}
	public float getFloHomeAvgpnt() {
		return FloHomeAvgpnt;
	}
	public float getFloAllAvgpnt() {
		return FloAllAvgpnt;
	}


	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setStrHaiten(String strHaiten) {
		this.strHaiten = strHaiten;
	}
	public void setStrMonnum(String strMonnum) {
		this.strMonnum = strMonnum;
	}
	public void setStrQuestionName(String strQuestionName) {
		this.strQuestionName = strQuestionName;
	}
	public void setFloHomeAvgpnt(float floHomeAvgpnt) {
		this.FloHomeAvgpnt = floHomeAvgpnt;
	}
	public void setFloAllAvgpnt(float floAllAvgpnt) {
		this.FloAllAvgpnt = floAllAvgpnt;
	}

}

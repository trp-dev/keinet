package jp.co.fj.keinavi.beans.user;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

import jp.co.fj.keinavi.data.user.LoginUserData;
import jp.co.fj.keinavi.data.user.UserClassData;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 利用者担当クラス登録Bean
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class LoginUserClassRegisterBean extends DefaultBean {

	// 学校コード
	private final String schoolCd;
	// 利用者データ
	private final LoginUserData loginUserData;

	/**
	 * コンストラクタ
	 */
	public LoginUserClassRegisterBean(final String schoolCd, final LoginUserData loginUserData) {
		
		if (schoolCd == null) {
			throw new IllegalArgumentException("学校コードがNULLです。");
		}
		
		if (loginUserData == null) {
			throw new IllegalArgumentException("利用者データがNULLです。");
		}
		
		this.schoolCd = schoolCd;
		this.loginUserData = loginUserData;
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		// DELETE
		deleteChargeClass();
		// INSERT
		insertChargeClass();
	}
	
	/**
	 * 利用者の担当クラス設定を削除する
	 * 
	 * @throws SQLException
	 */
	private void deleteChargeClass() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					"DELETE FROM login_charge "
					+ "WHERE schoolcd = ? AND loginid = ?");
			
			// 学校コード
			ps.setString(1, schoolCd);
			// 利用者ID
			ps.setString(2, loginUserData.getLoginId());
			
			ps.executeUpdate();

			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	/**
	 * 利用者の担当クラス設定を登録する
	 * 
	 * @throws SQLException
	 */
	private void insertChargeClass() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					"INSERT INTO login_charge VALUES(?,?,?,?,?)");
			
			// 学校コード
			ps.setString(1, schoolCd);
			// 利用者ID
			ps.setString(2, loginUserData.getLoginId());
			
			final Iterator ite = loginUserData.getChargeClassList().iterator();
			while (ite.hasNext()) {
				final UserClassData data = (UserClassData) ite.next();
				
				// 年度
				ps.setString(3, data.getYear());
				// 学年
				ps.setInt(4, data.getGrade());
				// クラス
				ps.setString(5, data.getClassName());
				
				ps.executeUpdate();
				
			}
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	
}

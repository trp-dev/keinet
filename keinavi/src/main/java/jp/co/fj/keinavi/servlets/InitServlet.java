package jp.co.fj.keinavi.servlets;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import jp.co.fj.kawaijuku.judgement.factory.FactoryManager;
import jp.co.fj.keinavi.beans.NewestExamDivSearchBean;
import jp.co.fj.keinavi.beans.recount.RecountChannel;
import jp.co.fj.keinavi.data.UnivMasterData;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.KNPropertyCtrl;
import jp.co.fj.keinavi.util.log.KNAccessLog;
import jp.co.fj.keinavi.util.message.MessageLoader;
import jp.co.fj.keinavi.util.sql.QueryLoader;
import jp.co.totec.config.ConfigResolver;
import jp.fujitsu.keinet.mst.db.factory.UnivFactoryDB;

import org.apache.commons.digester.Digester;
import org.apache.commons.digester.xmlrules.DigesterLoader;
import org.apache.log4j.xml.DOMConfigurator;

import com.fjh.db.DBManager;

public class InitServlet extends HttpServlet {

	private static final long serialVersionUID = 8246986512290213959L;

	/**
	 * @see javax.servlet.Servlet#init(javax.servlet.ServletConfig)
	 */
	public void init(final ServletConfig config) throws ServletException {

		super.init(config);

		// システムパスを初期化
		initPath(config.getServletContext());

		// セキュリティ設定をロード
		loadSecurityConfig(config.getServletContext());

		// 画面ID設定をロード
		loadForwardConfig(config.getServletContext());

		// JSPパス設定をロード
		loadJspConfig(config.getServletContext());

		// 印刷枚数設定をロード
		loadSheetNumberConfig(config.getServletContext());

		// 通常メニュー用コネクションプールの初期化
		initNDBConnection();

		// 無料メニュー用コネクションプールの初期化
		initFDBConnection();

		// 最新の大学マスタ情報をロード
		loadNewestExamDiv(config.getServletContext());

		//大学データロードスレッド
		UnivLoader univLoader = new UnivLoader();
		univLoader.start();

		// 再集計処理スレッド開始
		RecountChannel.getInstance().init(
				KNCommonProperty.getStringValue("NDBDriver"),
				KNCommonProperty.getStringValue("NDBURL"),
				KNCommonProperty.getStringValue("NDBConnUserID"),
				KNCommonProperty.getStringValue("NDBConnUserPass"));

		// ログ
		KNAccessLog.lv1(null, null, null, null, null, "INITIALIZED");
	}

	public void destroy() {

		super.destroy();

		try {
			DBManager.destroy();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// 再集計処理スレッド停止
		RecountChannel.getInstance().terminate();

		// ログ
		KNAccessLog.lv1(null, null, null, null, null, "DESTROYED");
	}

	/**
	 * 大学データロード用メソッド
	 * @throws Exception ロードエラー
	 */
	private void startUnivFactory() throws Exception{

        Connection con = null;
        try {
            /* DBは通常用で固定★この場合に限って直接取得 */
            Class.forName(KNCommonProperty.getNDBDriver());
            con = DriverManager.getConnection(KNCommonProperty.getNDBURL(), KNCommonProperty.getNDBConnUserID(),
                    KNCommonProperty.getNDBConnUserPass());

            FactoryManager manager = new FactoryManager(new UnivFactoryDB(con));

            /* 判定用大学マスタをロードする */
            System.out.println(">> 大学マスタの展開を開始");
            manager.load();

            System.out.println(">> 大学マスタの展開を終了");
            System.out.println(">> 展開数 : " + manager.getDatas().size());
            System.out.println(">> ローディング時間 " + manager.getLoadingTime());

            /* 情報誌用科目データをロードする */
            System.out.println(">> 情報誌用科目データの展開を開始");
            manager.loadUnivInfo();
            System.out.println(">> 情報誌用科目データの展開を終了");
            System.out.println(">> 展開数 : " + manager.getUnivInfoMap().size());
            System.out.println(">> ローディング時間 " + manager.getUnivInfoLoadingTime());

            getServletContext().setAttribute(UnivMasterData.class.getName(), new UnivMasterData(manager));
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

	/**
	 *
	 * @author T.Y
	 * 大学データロード用スレッド
	 */
	protected class UnivLoader extends Thread{
		public void run(){
			try{
				startUnivFactory();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	/**
	 * システムパスを初期化する
	 *
	 * @param context
	 * @throws ServletException
	 */
	protected void initPath(
			final ServletContext context) throws ServletException {

		// リソースファイルのパスをセット
		KNPropertyCtrl.FilePath = context.getRealPath(
				"/WEB-INF/classes/jp/co/fj/keinavi/resources");

		// SQLファイルがあるパスをセット
		QueryLoader.getInstance().setPath(context.getRealPath("/WEB-INF/sql"));

		// メッセージファイルがあるパスをセット
		MessageLoader.getInstance().setPath(context.getRealPath("/WEB-INF"));

		// 設定ファイルディレクトリをクラスパスに含める
		try {
			ConfigResolver.getInstance().init(
					context.getRealPath("/WEB-INF/config"));
		} catch (final IOException e) {
			throw createServletException(e);
		}

		// Log4j設定
		DOMConfigurator.configure(
				ConfigResolver.getInstance().findResource("log4j.xml"));
	}

	/**
	 * 通常メニュー用のDBコネクションを初期化する
	 */
	protected void initNDBConnection() {

		try {
			DBManager.create(
				KNCommonProperty.getNDBSID(),
				KNCommonProperty.getNDBDriver(),
				KNCommonProperty.getNDBURL(),
				KNCommonProperty.getNDBConnUserID(),
				KNCommonProperty.getNDBConnUserPass(),
				KNCommonProperty.getNDBPoolCountLimit(),
				Integer.parseInt(KNCommonProperty.getConnectionWaitTime())
			);
		} catch (final Exception e) {
			throw new InternalError(
					"NDBコネクションの初期化に失敗しました。" + e.getMessage());
		}

	}

	/**
	 * 無料メニュー用のDBコネクションを初期化する
	 */
	protected void initFDBConnection() {

		try {
			DBManager.create(
				KNCommonProperty.getFDBSID(),
				KNCommonProperty.getFDBDriver(),
				KNCommonProperty.getFDBURL(),
				KNCommonProperty.getFDBConnUserID(),
				KNCommonProperty.getFDBConnUserPass(),
				KNCommonProperty.getFDBPoolCountLimit(),
				Integer.parseInt(KNCommonProperty.getConnectionWaitTime())
			);
		} catch (final Exception e) {
			throw new InternalError(
					"FDBコネクションの初期化に失敗しました。" + e.getMessage());
		}
	}

	/**
	 * セキュリティ設定をロードする
	 *
	 * @param context
	 * @throws ServletException
	 */
	protected void loadSecurityConfig(final ServletContext context) throws ServletException {

		final String ruleFile = context.getRealPath("/WEB-INF/rule/security-config-rule.xml");
		final String configFile = context.getRealPath("/WEB-INF/security-config.xml");

		try {
			final Digester digester =
					DigesterLoader.createDigester(new File(ruleFile).toURL());
			context.setAttribute("SecurityConfig", digester.parse(new File(configFile)));

		} catch (final Exception e){
			throw new ServletException(e.getMessage());
		}
	}

	/**
	 * 画面ID設定をロードする
	 *
	 * @param context
	 * @throws ServletException
	 */
	protected void loadForwardConfig(final ServletContext context) throws ServletException {

		final String ruleFile = context.getRealPath("/WEB-INF/rule/forward-config-rule.xml");
		final String configFile = context.getRealPath("/WEB-INF/forward-config.xml");

		try {
			final Digester digester =
					DigesterLoader.createDigester(new File(ruleFile).toURL());
			context.setAttribute("ForwardMap", digester.parse(new File(configFile)));

		} catch (final Exception e){
			throw new ServletException(e.getMessage());
		}
	}

	/**
	 * JSPパス設定をロードする
	 *
	 * @param context サーブレットコンテキスト
	 * @throws ServletException 設定のロードに失敗したら発生する
	 */
	protected void loadJspConfig(
			final ServletContext context) throws ServletException {

		final String ruleFile = context.getRealPath(
				"/WEB-INF/rule/jsp-config-rule.xml");
		final String configFile = context.getRealPath(
				"/WEB-INF/jsp-config.xml");

		try {
			final Digester digester =
					DigesterLoader.createDigester(new File(ruleFile).toURL());
			context.setAttribute(
					"JspPathMap", digester.parse(new File(configFile)));

		} catch (final Exception e) {
			throw createServletException(e);
		}
	}

	/**
	 * 印刷枚数設定をロードする
	 *
	 * @param context サーブレットコンテキスト
	 * @throws ServletException 設定のロードに失敗したら発生する
	 */
	protected void loadSheetNumberConfig(
			final ServletContext context) throws ServletException {

		final String ruleFile = context.getRealPath(
				"/WEB-INF/rule/sheetnumber-config-rule.xml");
		final String configFile = context.getRealPath(
				"/WEB-INF/sheetnumber-config.xml");

		try {
			final Digester digester =
					DigesterLoader.createDigester(new File(ruleFile).toURL());
			context.setAttribute(
					"SheetNumberDefinition", digester.parse(new File(configFile)));

		} catch (final Exception e) {
			throw createServletException(e);
		}
	}

	/**
	 * 最新の大学マスタ情報をロードする
	 *
	 * @param context サーブレットコンテキスト
	 * @throws ServletException SQLエラー
	 */
	protected void loadNewestExamDiv(
			final ServletContext context) throws ServletException {

		final String dbKey = getDbKey();

		Connection con = null;
		try {
			con = DBManager.getConnectionPool(dbKey);

			NewestExamDivSearchBean bean = new NewestExamDivSearchBean();
			bean.setConnection(null, con);
			bean.execute();

			context.setAttribute("NewestExamDivSearchBean", bean);

		} catch (final Exception e) {
			throw createServletException(e);
		} finally {
			releaseConnectionPool(dbKey, con);
		}
	}

	protected String getDbKey() throws ServletException {

		try {
			return KNCommonProperty.getNDBSID();
		} catch (final Exception e) {
			throw createServletException(e);
		}
	}

	protected void releaseConnectionPool(
			final String dbKey, final Connection con) throws ServletException {

		try {
			DBManager.releaseConnectionPool(dbKey, con);
		} catch (final Exception e) {
			throw createServletException(e);
		}
	}

	protected ServletException createServletException(final Exception e) {

		final ServletException s = new ServletException(e.getMessage());
		s.initCause(e);
		return s;
	}

}

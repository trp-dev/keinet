//package jp.co.fj.keinavi.applets.individual;

// 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
package jp.co.fj.keinavi.beans.graph;
// 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * QQ)Hisakawa		2016.1.14	サーブレット化対応の為、 src/main/webapp/applets から移動
 */

//2016/02/26 QQ)Nishiyama 大規模改修 UPD START
// Linuxでアプレットクラスが処理出来ない
//public class BalanceChartApplet extends Applet{
public class BalanceChartApplet {
//2016/02/26 QQ)Nishiyama 大規模改修 UPD END

    int	x_center, y_center, radius, distance, dis_num,
        data_choicebox_xpos, data_choicebox_ypos,
        data_block_xpos, data_block_ypos,
        disp_selectbox_xpos, disp_selectbox_ypos;

//	String  choiceBoxName, choiceBoxNoSelect, checkBoxName;
//	Choice	choice01 = new Choice();

    String	dataSelect_str, dispSelect_str;
    boolean dataSelect_flag = true,
        dispSelect_flag = true;

    boolean	itemState[];
    String	selected_data = "";

    int	subjectNum, itemNum;
    String	subjectName[], itemName[], itemTitle[];
    int colorDat[];
    String	score[][];
    int	itemNameMaxLengthIndex = -1;

    int	minValue = 30,
        maxValue= 70;

    // 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
    String sDispZoneL = "";
    String sDispZoneH = "";
    String sSubjectNum = "";
    String sSubjectDAT = "";
    String sItemTITLE = "";
    String sItemNUM = "";
    String sDataSelect = "";
    String sDispSelect = "";
    String sItemDAT = "";
    String sDAT1 = "";
    String sDAT2 = "";
    String sDAT3 = "";
    String sDAT4 = "";
    String sColorDAT = "";
    // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

    //色の作成
    Color lightBG = new Color(0.5F, 0.8F, 0.8F);//薄い青緑
    Color whiteGray = new Color(0.95F, 0.95F, 0.9F);//薄い灰色
    Color purple = new Color(0.6F, 0.2F, 0.8F);//紫
    Color darkGreen = new Color(0.5F, 0.6F, 0.2F);//濃い青緑
    Color lightBlue = new Color(0.5F, 0.6F, 0.7F);//青緑

    Color	fgColor = Color.black,
        bgColor = Color.white,
        med_lineColor = Color.lightGray,
        colors[] = {
                Color.blue,
                Color.red,
                Color.green,
                Color.magenta,
                Color.orange,
                Color.cyan,
                Color.pink,
                Color.yellow,
                Color.white,
                Color.lightGray,
                Color.gray,
                Color.darkGray,
                Color.black,
                lightBlue,
                whiteGray,
                purple,
                lightBG,
                darkGreen
            };

    //************************************************
    //************************************************
    public void init(){
        int i, j;
        String	tmp_str;

        // 2016/02/26 QQ)Nishiyama 大規模改修 UPD START
        // アプレットの継承廃止により削除
        //this.setBackground(bgColor);
        //this.setLayout(null);
        // 2016/02/26 QQ)Nishiyama 大規模改修 UPD END

        x_center	= 280;
        y_center	= 170;
        radius		= 150;
        distance	= 5;

        ////////////////////////////////////////////////////////////////
        // get parameter
        //

        try {	dataSelect_str = getParameter("dataSelect");	}
        catch(Exception e) {	dataSelect_str = "on";		}

        try {	dispSelect_str = getParameter("dispSelect");	}
        catch(Exception e) {	dispSelect_str = "on";		}

//		choiceBoxNoSelect = getParameter("choiceBoxNoSelect");
//		checkBoxName  = getParameter("checkBoxName");
        minValue		= Integer.parseInt(getParameter("dispZoneL"));
        maxValue		= Integer.parseInt(getParameter("dispZoneH"));
        subjectNum	= Integer.parseInt(getParameter("subjectNUM"));
        subjectName	= CommatextToStrings(getParameter("subjectDAT"));
        itemTitle	= CommatextToStrings(getParameter("itemTITLE"));
        itemNum		= Integer.parseInt(getParameter("itemNUM"));
        itemName	= CommatextToStrings(getParameter("itemDAT"));
        if(getParameter("colorDAT")!=null){
            colorDat	= CommatextToInt(getParameter("colorDAT"));
        }else{
            colorDat	= new int[10];
            for(int ci=0;ci<10;ci++)	colorDat[ci]=ci;
        }
        int	maxLength = 0;
        for(i = 0; i < itemNum; i++){
            if(maxLength < itemName[i].length()){
                maxLength = itemName[i].length();
                itemNameMaxLengthIndex = i;
            }
        }


        score	= new String[itemNum][subjectNum];
        for(int ii = 0; ii < itemNum; ii++){
            String tmpStr = "DAT" + Integer.toString(ii + 1);
            if(getParameter(tmpStr)!=null){
//				score[ii] = CommatextToDoubles(getParameter(tmpStr));
                score[ii] = splitComma(getParameter(tmpStr));
            }else{
                ;
            }
        }

        ///////////////////////////////////////////////////////////////

        data_choicebox_xpos = x_center - 160;
        if(subjectNum > itemNum)
            data_choicebox_ypos = y_center + radius + 40 + 10 * subjectNum;
        else
            data_choicebox_ypos = y_center + radius + 40 + 10 * itemNum;
        data_block_xpos = 5;
        data_block_ypos = y_center + radius + 30;

        disp_selectbox_xpos = x_center - 160;
        if( dataSelect_flag )
            disp_selectbox_ypos = y_center + radius + 70 + 10 * subjectNum;
        else
            disp_selectbox_ypos = y_center + radius + 40 + 10 * subjectNum;

        ///////////////////////////////////////////////////////////////
        // disp or no-disp
        try{
            if(dataSelect_str.equals("on"))		dataSelect_flag = true;
            else					dataSelect_flag = false;
        }
        catch(Exception e){
            dataSelect_flag = false;
        }

        try{
            if(dispSelect_str.equals("on"))	dispSelect_flag = true;
            else					dispSelect_flag = false;
        }
        catch(Exception e){
            dispSelect_flag = true;
        }
        //
        ///////////////////////////////////////////////////////////////

        dis_num = (int) ((maxValue - minValue) / distance);

        itemState = new boolean[itemNum];
        for(i = 0; i < itemNum; i++){
            itemState[i] = true;
        }
    }

    //************************************************

/*	public void itemStateChanged(ItemEvent ie){
        int i;

        // select box
        if(ie.getSource() == choice01){
            selected_data = choice01.getSelectedItem();
        }

        repaint();
    }
*/
    //***********************************************
    public void paint(Graphics g){
        if(itemNum==0){
            drawStrNoDisp(g);
            return;
        }
        double	rad;
        FontMetrics	ft = g.getFontMetrics();

        // for drawing outline of polygon
        int	x_outline[] = new int[subjectNum];
        int	y_outline[] = new int[subjectNum];

        // for drawing item name
        int	x_pos[] = new int[subjectNum];
        int	y_pos[] = new int[subjectNum];
        int	len;

        // for drawing ChartGraph
        int	score_X[][] = new int[itemNum][subjectNum];
        int	score_Y[][] = new int[itemNum][subjectNum];

        // for drawing medium-line
        int	x_med[][] = new int[dis_num][subjectNum];
        int	y_med[][] = new int[dis_num][subjectNum];

        int i, j;
        String t_str;
        int[] jTmp = new int[itemNum];//各チャートITEMごとにいくつの頂点があるかを示す
        // get radian value
        rad = ((360.0 / (double)subjectNum) * Math.PI) / 180.0;

        // caluculation of (x, y) coordinate
        for (i = 0; i < subjectNum; i++) {
            x_outline[i] = x_center + (int)(Math.sin(rad * i) * radius);
            y_outline[i] = y_center - (int)(Math.cos(rad * i) * radius);
        }
        for (i = 0; i < dis_num; i++) {
            for (j = 0; j < subjectNum; j++) {
                x_med[i][j] = x_center +
                    (int)(Math.sin(rad * j) * radius * distance*(i+1) / (maxValue - minValue));
                y_med[i][j] = y_center -
                    (int)(Math.cos(rad * j) * radius * distance*(i+1) / (maxValue - minValue));
            }
        }
        //チャート描画用に頂点の値を設定する
        for (i = 0; i < itemNum; i++) {
            jTmp[i] = 0;
            for (j = 0; j < subjectNum; j++) {
                if(Float.parseFloat(score[i][j])!=-999.0f){
                    score_X[i][jTmp[i]] = x_center +
                        (int)(Math.sin(rad * j) * radius * (Float.parseFloat(score[i][j]) - minValue) / (maxValue - minValue));
                    score_Y[i][jTmp[i]] = y_center -
                        (int)(Math.cos(rad * j) * radius * (Float.parseFloat(score[i][j]) - minValue) / (maxValue - minValue));
                    jTmp[i]++;
                }else System.out.println("INFO:This subject data not exist(ITEM=" + i + " subject=" + j);
            }
        }
        // 目盛り線と目盛値の描画　draw medium-line
        int deltaX,deltaY,memoriLen=5;
        for(i = 0; i < dis_num; i++){
            g.setColor(med_lineColor);
//			g.drawPolygon(x_med[i], y_med[i], subjectNum);
//			g.setColor(colors[12]);
            for(j = 0;j < subjectNum;j++){
                deltaX = (memoriLen * (int)(Math.cos(j*rad)*10000))/10000;
                deltaY = (memoriLen * (int)(Math.sin(j*rad)*10000))/10000;
                g.drawLine(x_med[i][j]-deltaX, y_med[i][j]-deltaY,
                            x_med[i][j]+deltaX, y_med[i][j]+deltaY);
            }
            g.drawString(Integer.toString((int)((maxValue-minValue)*(i+1)/dis_num)+minValue)+".0",
                            x_med[i][0] - 24, y_med[i][0]);
        }
        // 偏差値(中心)の描画
        g.drawString(Integer.toString(minValue)+".0", x_center - 24, y_center);

        // 外枠線の描画　draw polygon
        g.setColor(fgColor);
        g.drawPolygon(x_outline, y_outline, subjectNum);

        // 中心から各頂点(教科)へのdraw line ( from each coordinate to center coordinate )
        g.setColor(fgColor);
        for (i = 0; i < subjectNum; i++) {
            g.drawLine(x_outline[i], y_outline[i], x_center, y_center);
        }

        // 教科名の描画　draw item name
        g.setColor(fgColor);
        for (i = 0; i < subjectNum; i++){
            if(x_outline[i] >= x_center){
                if(y_outline[i] <= y_center){
                    g.drawString(subjectName[i],
                        x_outline[i] + 5, y_outline[i] - 5);
                }
                else{
                    g.drawString(subjectName[i],
                        x_outline[i], y_outline[i] + 13);
                }
            }
            else{
                len = subjectName[i].length();
                if(y_outline[i] >= y_center){
                    g.drawString(subjectName[i],
//						x_outline[i] - len * 6, y_outline[i] + 13);
                        x_outline[i] - ft.stringWidth(subjectName[i]),
                        y_outline[i] + 13);
                }
                else{
                    g.drawString(subjectName[i],
//						x_outline[i] - len * 6, y_outline[i] - 5);
                        x_outline[i] - ft.stringWidth(subjectName[i]),
                        y_outline[i] - 5);
                }
            }
        }

        // 右上アイテム欄枠作成
        int yIncr=20;
        if( itemState[0] ){
            g.setColor(colors[colorDat[4]]);
            g.fillRect(x_center + radius + 60, y_center - radius - 15, 330, yIncr);
            g.setColor(colors[colorDat[5]]);
            g.fillRect(x_center + radius + 60, y_center - radius - 15 + yIncr, 330, yIncr);
            g.setColor(colors[colorDat[4]]);
            g.fillRect(x_center + radius + 60, y_center - radius - 15 + yIncr*2, 330, yIncr*2);
            g.setColor(colors[colorDat[5]]);
            g.fillRect(x_center + radius + 60, y_center - radius - 15 + yIncr*3, 330, yIncr*6);
        }
        // チャートと右上のアイテム名表示欄の描画　draw chartGraph
            g.setColor(bgColor);
            g.drawString(itemTitle[0],x_center + radius + 70, y_center - radius );
            g.drawString(itemTitle[1],x_center + radius + 70, y_center - radius + 2 * yIncr);
        for(i = 0; i < itemNum; i++){
            if( itemState[i] ){
                // チャート描画
                g.setColor(colors[colorDat[i]]);
                g.drawPolygon(score_X[i], score_Y[i], jTmp[i]);
                // アイテムリスト描画
                if(i==0){//対象描画
                    g.drawLine(x_center + radius + 70, y_center - radius + (i+1) * yIncr - 5,
                        x_center + radius + 80, y_center - radius + (i+1) * yIncr - 5);
                    g.setColor(fgColor);
                    g.drawString(itemName[i],
                        x_center + radius + 90, y_center - radius + (i+1) * yIncr);
                }else{//比較対照描画
                    g.drawLine(x_center + radius + 70, y_center - radius + (i+2) * yIncr - 5,
                        x_center + radius + 80, y_center - radius + (i+2) * yIncr - 5);
                    g.setColor(fgColor);
                    g.drawString(itemName[i],
                        x_center + radius + 90, y_center - radius + (i+2) * yIncr);
                }
            }
        }
/*		//チャート下 アイテム名と教科偏差値表の描画
        for(i = 0; i < itemNum; i++){
            if(! itemState[i] )	continue;

            String scoreString = "";
            for(j = 0; j < subjectNum; j++){
                scoreString += subjectName[j] + ": " + score[i][j] + "    ";
            }
            g.setColor(colors[colorDat[i]]);
            g.drawLine(x_center - radius - 105, y_center + radius + 18 + i * 15,
                x_center - radius + ft.stringWidth(itemName[itemNameMaxLengthIndex]) + 300, y_center + radius + 18 + i * 15);
            g.drawString(itemName[i] + "  ",
                x_center - radius - 100,
                y_center + radius + 30 + i * 15 );
            g.drawString(scoreString,
                x_center - radius - 100 +
                    ft.stringWidth(itemName[itemNameMaxLengthIndex]) + 30,
                y_center + radius + 30 + i * 15 );
        }

        // drawing selected data value
        // if not selected, then don't drawing
        if( (selected_data.equals("")) || (selected_data.equals(choiceBoxNoSelect)) ){
        }
        else{
            for(i = 0; i < itemNum; i++){
                if(selected_data.equals(itemName[i])){
                    g.setColor(colors[colorDat[itemNum]]);
                    g.drawString(selected_data,
                            data_block_xpos, data_block_ypos);
                    g.setColor(fgColor);
                    for(j = 0; j < subjectNum; j++){
                        String str;
                        if(Float.parseFloat(score[i][j]) < 100.0){
                            str = subjectName[j] + ":  "
                                + score[i][j];
                        }
                        else{
                            str = subjectName[j] + ": "
                                + score[i][j];
                        }
                        g.drawString(str,
                            data_block_xpos,
                            data_block_ypos + (j+1) * 10);
                    }
                    break;
                }
            }
        }
        */
    }

    //************************************************

    //カンマでデータを分割して配列に収納
    //T.Yamada 変更：カンマの間にデータがなければ、-999.0を入れる
    private static String[] splitComma(String str){
        String DUMMY = "#";
        if (str == null) return new String[0];
        StringTokenizer tk = new StringTokenizer(str, ",", true);
        List list = new ArrayList();
        String preToken = DUMMY;
        while(tk.hasMoreTokens()){
            String temp = tk.nextToken();
            if((preToken.trim().equals(DUMMY) && temp.trim().equals(",")) || (temp.trim().equals(",") && preToken.trim().equals(",")) || (temp.trim().equals(",") && !tk.hasMoreTokens())){
                list.add("-999.0f");
            }else if(!temp.trim().equals(",")){
                list.add(temp);
            }
            preToken = temp;
        }
        return (String[])list.toArray(new String[0]);
    }

    private String[] CommatextToStrings(String commatext){
        StringTokenizer tk = new StringTokenizer(commatext, ",");
        int		intN = tk.countTokens();
        String[]	tmp = new String[intN];
        //
        for(int ii = 0; ii < intN; ii++){
            tmp[ii] = new String(tk.nextToken());
        }
        return tmp;
    }

    private double[] CommatextToDoubles(String commatext){
        StringTokenizer tk = new StringTokenizer(commatext, ",");
        int		intN = tk.countTokens();
        double[]	tmp = new double[intN];
        //
        for(int ii = 0; ii < intN; ii++){
            Double dd = new Double(tk.nextToken());
            tmp[ii] = dd.doubleValue();
        }
        return tmp;
    }
    private int[] CommatextToInt(String commatext){
        StringTokenizer tk = new StringTokenizer(commatext, ",");
        int		intN = tk.countTokens();
        int[]	tmp = new int[intN];
        //
        for(int ii = 0; ii < intN; ii++){
            Integer dd = new Integer(tk.nextToken());
            tmp[ii] = dd.intValue();
        }
        return tmp;
    }
    private void drawStrNoDisp(Graphics g){
        g.setColor(Color.lightGray);
        g.setFont(new Font("Dialog",Font.PLAIN,20));//フォント設定
        g.drawString("表示するデータがありません",100, 100);
    }

    // 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
    public void setParameter(String str, String strParam) {
        if(str == null) {
        }else if("dispZoneL".equals(str)){
            sDispZoneL = strParam;
        }else if("dispZoneH".equals(str)){
            sDispZoneH = strParam;
        }else if("subjectNUM".equals(str)){
            sSubjectNum = strParam;
        }else if("subjectDAT".equals(str)){
            sSubjectDAT = strParam;
        }else if("itemTITLE".equals(str)){
            sItemTITLE = strParam;
        }else if("itemNUM".equals(str)){
            sItemNUM = strParam;
        }else if("dataSelect".equals(str)){
            sDataSelect = strParam;
        }else if("dispSelect".equals(str)){
            sDispSelect = strParam;
        }else if("itemDAT".equals(str)){
            sItemDAT = strParam;
        }else if("DAT1".equals(str)){
            sDAT1 = strParam;
        }else if("DAT2".equals(str)){
            sDAT2 = strParam;
        }else if("DAT3".equals(str)){
            sDAT3 = strParam;
        }else if("DAT4".equals(str)){
            sDAT4 = strParam;
        }else if("colorDAT".equals(str)){
            sColorDAT = strParam;
        }
    }
    public String getParameter(String str) {
        String retStr = "";
        if(str == null) {
            retStr = "";
        }else if("dispZoneL".equals(str)){
            retStr = sDispZoneL;
        }else if("dispZoneH".equals(str)){
            retStr = sDispZoneH;
        }else if("subjectNUM".equals(str)){
            retStr = sSubjectNum;
        }else if("subjectDAT".equals(str)){
            retStr = sSubjectDAT;
        }else if("itemTITLE".equals(str)){
            retStr = sItemTITLE;
        }else if("itemNUM".equals(str)){
            retStr = sItemNUM;
        }else if("dataSelect".equals(str)){
            retStr = sDataSelect;
        }else if("dispSelect".equals(str)){
            retStr = sDispSelect;
        }else if("itemDAT".equals(str)){
            retStr = sItemDAT;
        }else if("DAT1".equals(str)){
            retStr = sDAT1;
        }else if("DAT2".equals(str)){
            retStr = sDAT2;
        }else if("DAT3".equals(str)){
            retStr = sDAT3;
        }else if("DAT4".equals(str)){
            retStr = sDAT4;
        }else if("colorDAT".equals(str)){
            retStr = sColorDAT;
        }else{
            retStr = "";
        }

        return retStr;
    }
    // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END
}


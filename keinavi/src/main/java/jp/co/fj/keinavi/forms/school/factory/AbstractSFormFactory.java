/*
 * 作成日: 2004/07/16
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.school.factory;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.AbstractActionFormFactory;
import jp.co.fj.keinavi.forms.school.SMaxForm;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * 校内成績分析のアクションフォーム生成時に
 * 共通する処理を記述する。
 * 
 * 2005.02.15 Yoshimoto KAWAI - Totec
 *            対象模試は前画面を引き継ぐようにする
 * 
 * @author kawai
 *
 * */
public abstract class AbstractSFormFactory extends AbstractActionFormFactory {

	/**
	 * SMaxFormを生成する
	 * @param request
	 * @return
	 */
	protected SMaxForm createSMaxForm(final HttpServletRequest request) {
		final SMaxForm form = new SMaxForm();

		// 模試セッション
		final ExamSession examSession = (ExamSession) request.getAttribute(ExamSession.SESSION_KEY);

		// 対象模試
		ExamData exam =
			examSession.getExamData(
				request.getParameter("targetYear"),
				request.getParameter("targetExam")
			);
		
		// 対象模試がなければ最新にする
		if (exam == null) {
			exam = KNUtil.getLatestExamData(examSession);
		}

		// さらになければ空オブジェクト
		if (exam == null) {
			exam = new ExamData();
		}
		
		// 対象年度・対象模試
		form.setTargetYear(exam.getExamYear());
		form.setTargetExam(exam.getExamCD());
		// スクロール座標を初期化
		form.setScrollX("0");
		form.setScrollY("0");

		return form;
	}
	
	/**
	 * 全国総合成績の一括出力対象をセットする
	 * @param profile
	 * @param flag
	 */
	protected void setTotalOutItem(Profile profile, Set outItem) {
		ProfileUtil.setOutItem(profile, IProfileCategory.SCORE_TOTAL, outItem.contains("ScoreTotal"));
	}
	
}

package jp.co.fj.keinavi.servlets.maintenance;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.admin.LastDateBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.maintenance.M104Form;
import jp.co.fj.keinavi.util.csv.CSVRegisterBean;

/**
 *
 * 生徒情報一括登録確認画面サーブレット
 * 
 * 2005.11.28	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M106Servlet extends AbstractMServlet {
	
	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// HTTPセッション
		final HttpSession session = request.getSession(false);
		
		// JSPへの遷移
		if ("m104".equals(getForward(request))) {
			
			// ログイン情報
			final LoginSession login = getLoginSession(request);
			// アクションフォーム
			final M104Form form = (M104Form) getActionForm(
					request, "jp.co.fj.keinavi.forms.maintenance.M104Form");
			// CSV一括登録Bean
			final CSVRegisterBean bean = (CSVRegisterBean) session
					.getAttribute(CSVRegisterBean.SESSION_KEY);
			
			Connection con = null;
			try {
				con = getConnectionPool(request);
				
				// データ更新日
				LastDateBean.setLastDate1(con, login.getUserID());
				
				bean.setConnection(null, con);
				bean.commitProcess();
				
				// 登録処理が済んでいるならコミット
				if (bean.isCommitted()) {
					con.commit();
					actionLog(request, "801");
				} else {
					con.rollback();
				}
			} catch (final Exception e) {
				rollback(con);
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}
			
			request.setAttribute("form", form);
			request.setAttribute("CSVRegisterBean", bean);
			
			// コミットされたなら完了画面へ
			if (bean.isCommitted()) {
				initStudentSession(request);
				forward2Jsp(request, response, "m104");
			// エラーを含むならエラー画面へ
			} else if (bean.hasError()) {
				forward2Jsp(request, response, "m103");
			// 不明
			// ありえないはず
			} else {
				throw new ServletException("処理判定エラー");
			}
			
		// 不明なら転送
		} else {
			dispatch(request, response);
		}
		
		// セッションのCSVRegisterBeanを消す
		session.removeAttribute(CSVRegisterBean.SESSION_KEY);
	}
	
}

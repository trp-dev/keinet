package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.graph.BalanceChartApplet;
import jp.co.fj.keinavi.beans.help.OnepointBean;
import jp.co.fj.keinavi.beans.individual.I103Bean;
import jp.co.fj.keinavi.beans.individual.IUtilBean;
import jp.co.fj.keinavi.beans.individual.TakenExamSearchBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.IExamData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I103Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * バランスチャート表示用サーブレット
 *
 * 2004/06/28    [新規作成]
 *
 * @author Keisuke KONDO - TOTEC
 * @version 1.0
 *
 * 2005.8.28 Totec)T.Yamada    [1]印刷での対象模試がセンターリサーチの時のチェックボックス無効時の対応
 * 2006.9.04 Totec)T.Yamada    [2]受験学力測定対応
 * 2016.1.14 QQ)K.Hisakawa        サーブレット化対応
 *
 */
public class I103Servlet extends IndividualServlet {

    /**
     * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
     * javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    protected void execute(
        final HttpServletRequest request,
        final HttpServletResponse response)
        throws ServletException, IOException{

        super.execute(request, response);

        //フォームデータの取得
        final I103Form i103Form = (I103Form) getActionForm(
                request, "jp.co.fj.keinavi.forms.individual.I103Form");

        //プロファイル情報
        final Profile profile = getProfile(request);
        String devRanges = (String)profile.getItemMap(IProfileCategory.I_SCORE_BC).get(IProfileItem.DEV_RANGE);
        Short indMultiExam = (Short)profile.getItemMap(IProfileCategory.I_SCORE_BC).get(IProfileItem.IND_MULTI_EXAM);
        Short printStamp = (Short) profile.getItemMap(IProfileCategory.I_SCORE_BC).get(IProfileItem.PRINT_STAMP);
        Short printStudent = (Short) profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);

        // ログイン情報
        final LoginSession login = getLoginSession(request);
        //個人共通
        final ICommonMap iCommonMap = getICommonMap(request);

        // 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
        Collection cSubNames = null;
        Collection cExaminationDatas = null;
        boolean bDidTakeExam = false;
        // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

        // 転送元がJSPなら対象模試情報を初期化
        if ("i103".equals(getBackward(request))) {
            initTargetExam(request);
        }

        if ("i103".equals(getForward(request))) {

            Connection con = null;
            try {
                con = getConnectionPool(request);

                //■ワンポイントアドバイス
                request.setAttribute("OnepointBean", searchOnePointBean(con));

                //■比較対象試験を取得
                final TakenExamSearchBean takenBean
                    = searchSelectiveExams(
                        con,
                        i103Form.getTargetPersonId(),
                        iCommonMap.getExamData(),
                        login.isHelpDesk());

                request.setAttribute("TakenBean", takenBean);

                //ページ切替・生徒切替・モード切替・模試切替
                if(!"i103".equals(getBackward(request))
                        || !iCommonMap.getTargetPersonId().equals(i103Form.getTargetPersonId())
                        || isChangedMode(request) || isChangedExam(request)){

                    //比較対象模試（デフォルト）
                    final String[] selectives = setupSelectives(iCommonMap.getExamData(), takenBean);

                    i103Form.setTargetExamCode1(selectives[0]);
                    i103Form.setTargetExamCode2(selectives[1]);
                    i103Form.setTargetExamCode3(selectives[2]);

                    //■表示用Bean
                    final I103Bean i103Bean = new I103Bean();
                    i103Bean.setTargetExamYear(iCommonMap.getTargetExamYear());
                    i103Bean.setTargetExamCode(iCommonMap.getTargetExamCode());
                    i103Bean.setDeviationRanges(IUtilBean.deconcatComma(devRanges));
                    i103Bean.setPersonId(i103Form.getTargetPersonId());
                    i103Bean.setTargetExamCodes(new String[]{selectives[0], selectives[1], selectives[2]});
                    i103Bean.setHelpflg(login.isHelpDesk());
                    i103Bean.setConnection(null, con);
                    i103Bean.execute();

                    request.setAttribute("i103Bean", i103Bean);

                    // 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
                    cSubNames = i103Bean.getSubNames();
                    cExaminationDatas = i103Bean.getExaminationDatas();
                    bDidTakeExam = i103Bean.isDidTakeExam();
                    // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

                    //生徒切替・モード切替・模試切替
                    if ("i103".equals(getBackward(request))){

                        //1.印刷対象生徒をフォームにセットしなおす
                        String[] printTarget = {"0","0","0","0"};
                        if(i103Form.getPrintTarget() != null){
                            for(int i=0; i<i103Form.getPrintTarget().length; i++){
                                printTarget[Integer.parseInt(i103Form.getPrintTarget()[i])] = "1";
                            }
                        }
                        //[1] add start
                        final String CENTER_EXAMCD = "38";
                        if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
                            printTarget[3] = "1";
                        }
                        //[1] add end
                        i103Form.setPrintTarget(printTarget);

                        // 対象模試と比較対象模試が第1解答課目模試以外の場合、１教科複数受験は平均とする
                        if (!(KNUtil.isAns1st(iCommonMap.getTargetExamCode()) || KNUtil.isAns1st(selectives[0]) || KNUtil.isAns1st(selectives[1]) || KNUtil.isAns1st(selectives[2]))
                            && i103Form.getAvgChoice().equals("3")) {
                            i103Form.setAvgChoice("1");
                        }

                    // 初回アクセス
                    } else {

                        /** プロファイルデータをフォームにセットする */
                        //個人成績分析共通-印刷対象生保存をセット
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                        //i103Form.setPrintStudent(printStudent.toString());
                        i103Form.setPrintStudent(iCommonMap.getPrintStudent());
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END

                        //1.印刷対象生徒をセット
                        String printTarget = (String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET);
                        i103Form.setPrintTarget(CollectionUtil.splitComma(printTarget));

                        //2.偏差値範囲をセット
                        i103Form.setLowerRange(CollectionUtil.deconcatComma(devRanges)[0]);
                        i103Form.setUpperRange(CollectionUtil.deconcatComma(devRanges)[1]);

                        //3.教科複数受験を保存
                        // 対象模試と比較対象模試が第1解答課目模試以外の場合、１教科複数受験は平均とする
                        if (!(KNUtil.isAns1st(iCommonMap.getTargetExamCode()) || KNUtil.isAns1st(selectives[0]) || KNUtil.isAns1st(selectives[1]) || KNUtil.isAns1st(selectives[2]))
                                && indMultiExam.toString().equals("3")) {
                            i103Form.setAvgChoice("1");
                        } else {
                            i103Form.setAvgChoice(indMultiExam.toString());
                        }

                        //4.セキュリティスタンプセット
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                        //i103Form.setPrintStamp(printStamp.toString());
                        i103Form.setPrintStamp(iCommonMap.getPrintStamp());
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END

                    }

                // 再表示アクセス
                } else {

                    //■表示用Bean
                    final I103Bean i103Bean = new I103Bean();
                    i103Bean.setTargetExamYear(iCommonMap.getTargetExamYear());
                    i103Bean.setTargetExamCode(iCommonMap.getTargetExamCode());
                    i103Bean.setDeviationRanges(new String[]{i103Form.getLowerRange(), i103Form.getUpperRange()});
                    i103Bean.setPersonId(i103Form.getTargetPersonId());
                    i103Bean.setTargetExamCodes(new String[]{i103Form.getTargetExamCode1(), i103Form.getTargetExamCode2(), i103Form.getTargetExamCode3()});
                    i103Bean.setHelpflg(login.isHelpDesk());
                    i103Bean.setConnection(null, con);
                    i103Bean.execute();

                    request.setAttribute("i103Bean", i103Bean);

                    // 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
                    cSubNames = i103Bean.getSubNames();
                    cExaminationDatas = i103Bean.getExaminationDatas();
                    bDidTakeExam = i103Bean.isDidTakeExam();
                    // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

                    //1.印刷対象生徒をフォームにセットしなおす
                    String[] printTarget = {"0","0","0","0"};
                    if(i103Form.getPrintTarget() != null){
                        for(int i=0; i<i103Form.getPrintTarget().length; i++){
                            printTarget[Integer.parseInt(i103Form.getPrintTarget()[i])] = "1";
                        }
                    }
                    //[1] add start
                    final String CENTER_EXAMCD = "38";
                    if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
                        printTarget[3] = "1";
                    }
                    //[1] add end
                    i103Form.setPrintTarget(printTarget);

                    //2.偏差値範囲を保存
                    String devRangeUL = CollectionUtil.deSplitComma(new String[]{i103Form.getLowerRange(),i103Form.getUpperRange()});
                    profile.getItemMap(IProfileCategory.I_SCORE_BC).put(IProfileItem.DEV_RANGE, devRangeUL);

                    //3.教科複数受験を保存
                    profile.getItemMap(IProfileCategory.I_SCORE_BC).put(IProfileItem.IND_MULTI_EXAM, new Short(i103Form.getAvgChoice()));

                }

            } catch (final Exception e) {
                throw createServletException(e);
            } finally {
                releaseConnectionPool(request, con);
            }

            request.setAttribute("form", i103Form);
            iCommonMap.setTargetPersonId(i103Form.getTargetPersonId());

            // 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
            int subCount = 0;
            String strSubjectDAT = "";
            for (java.util.Iterator it=cSubNames.iterator(); it.hasNext();) {
              if(subCount != 0) {
                  strSubjectDAT = strSubjectDAT + ",";
                  strSubjectDAT = strSubjectDAT + String.valueOf(it.next());
              } else {
                  strSubjectDAT = String.valueOf(it.next());
              }
              subCount ++;
            }

            String moshiString = "";

            String[] dataStrings = new String[cExaminationDatas.size()];
            int moshiCount = 0;
            String dispSelect = "on";
            if(!bDidTakeExam) dispSelect = "off";

            for (java.util.Iterator it=cExaminationDatas.iterator(); it.hasNext();) {

              IExamData data = (IExamData)it.next();

              if(moshiCount != 0){
                moshiString += ",";
                dataStrings[moshiCount] += ",";
              }

              moshiString += data.getExamName();

              String lowRange = i103Form.getLowerRange();
              String upRange = "100.0";

              if(i103Form.getAvgChoice().equals("1")){
                if(cSubNames.size() > 3)
                  dataStrings[moshiCount] = GeneralUtil.toRangeLimit(data.getAvgEng(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getAvgMath(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getAvgJap(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getAvgSci(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getAvgSoc(), lowRange, upRange);
                else
                  dataStrings[moshiCount] =
                                GeneralUtil.toRangeLimit(data.getAvgEng(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getAvgMath(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getAvgJap(), lowRange, upRange);

              }else if(i103Form.getAvgChoice().equals("3")){
                if(cSubNames.size() > 3)
                  dataStrings[moshiCount] = GeneralUtil.toRangeLimit(data.getAvgEng(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getAvgMath(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getAvgJap(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getAns1stSci(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getAns1stSoc(), lowRange, upRange);
                else
                  dataStrings[moshiCount] =
                                GeneralUtil.toRangeLimit(data.getAvgEng(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getAvgMath(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getAvgJap(), lowRange, upRange);

              }else{
                if(cSubNames.size() > 3)
                  dataStrings[moshiCount] = GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getMaxSci(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getMaxSoc(), lowRange, upRange);
                else
                  dataStrings[moshiCount] =
                                GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);
              }
              moshiCount ++;
            }

            int iItemNUM = 0;
            if(dispSelect.equals("on") || cExaminationDatas.size() > 1){
                iItemNUM = cExaminationDatas.size();
            } else {
                iItemNUM = 0;
            }

            BalanceChartApplet App = new BalanceChartApplet();
            App.setParameter("dispZoneL", String.valueOf((int)Double.parseDouble(i103Form.getLowerRange())));
            App.setParameter("dispZoneH", String.valueOf((int)Double.parseDouble(i103Form.getUpperRange())));
            App.setParameter("subjectNUM", String.valueOf(cSubNames.size()));
            App.setParameter("subjectDAT", strSubjectDAT);
            App.setParameter("itemTITLE", "□対象模試,■比較対象模試");
            App.setParameter("itemNUM", String.valueOf(iItemNUM));
            App.setParameter("dispSelect", dispSelect);
            App.setParameter("itemDAT", moshiString);
            for(int i=0; i<dataStrings.length; i++){
                App.setParameter("DAT" + String.valueOf(i+1), dataStrings[i]);
            }
            App.setParameter("colorDAT", "1,0,2,15,13,14");

            App.init();

            HttpSession session = request.getSession(true);

            session.setAttribute("GraphServlet", App);
            session.setAttribute("GraphType", "BalanceChart");

            App = null;
            dataStrings = null;
            // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

            super.forward(request, response, JSP_I103);

        //不明ならServlet転送
        } else {

            // 印刷アクション
            if ("sheet".equals(getForward(request))) {

                /** フォームの値をプロファイルに保存　*/
                //分析モードなら個人成績分析共通-印刷対象生保存をセット
                if(iCommonMap.isBunsekiMode()){
                    profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(i103Form.getPrintStudent()));
                }

                //1.印刷対象生徒をセット
                String[] printTarget = {"0","0","0","0"};
                if(i103Form.getPrintTarget() != null){
                    for(int i=0; i<i103Form.getPrintTarget().length; i++){
                        printTarget[Integer.parseInt(i103Form.getPrintTarget()[i])] = "1";
                    }
                }
                //[1] add start
                final String CENTER_EXAMCD = "38";
                if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
                    printTarget[3] = "1";
                }
                //[1] add end
                profile.getItemMap(IProfileCategory.I_SCORE_COMMON).put(IProfileItem.PRINT_TARGET, CollectionUtil.deSplitComma(printTarget));

                //2.偏差値範囲を保存
                String devRangeUL = CollectionUtil.deSplitComma(new String[]{i103Form.getLowerRange(),i103Form.getUpperRange()});
                profile.getItemMap(IProfileCategory.I_SCORE_BC).put(IProfileItem.DEV_RANGE, devRangeUL);

                //3.教科複数受験を保存
                profile.getItemMap(IProfileCategory.I_SCORE_BC).put(IProfileItem.IND_MULTI_EXAM, new Short(i103Form.getAvgChoice()));

                //4.セキュリティスタンプを保存
                profile.getItemMap(IProfileCategory.I_SCORE_BC).put(IProfileItem.PRINT_STAMP, new Short(i103Form.getPrintStamp()));

            } else {

                /** フォームの値をプロファイルに保存　*/
                //分析モードなら個人成績分析共通-印刷対象生保存をセット
                if(iCommonMap.isBunsekiMode()){
                    profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(i103Form.getPrintStudent()));
                }

                //1.印刷対象生徒をセット
                String[] printTarget = {"0","0","0","0"};
                if(i103Form.getPrintTarget() != null){
                    for(int i=0; i<i103Form.getPrintTarget().length; i++){
                        printTarget[Integer.parseInt(i103Form.getPrintTarget()[i])] = "1";
                    }
                }
                //[1] add start
                final String CENTER_EXAMCD = "38";
                if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
                    printTarget[3] = "1";
                }
                //[1] add end
                profile.getItemMap(IProfileCategory.I_SCORE_COMMON).put(IProfileItem.PRINT_TARGET, CollectionUtil.deSplitComma(printTarget));

                //4.セキュリティスタンプを保存
                profile.getItemMap(IProfileCategory.I_SCORE_BC).put(IProfileItem.PRINT_STAMP, new Short(i103Form.getPrintStamp()));
            }

            iCommonMap.setTargetPersonId(i103Form.getTargetPersonId());
            // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD START
            iCommonMap.setPrintStamp(i103Form.getPrintStamp());
            iCommonMap.setPrintStudent(i103Form.getPrintStudent());
            // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD END
            super.forward(request, response, SERVLET_DISPATCHER);
        }
    }

    private TakenExamSearchBean searchSelectiveExams(
            final Connection con,
            final String individualId,
            final ExamData exam,
            final boolean isHelpDesk) throws Exception {

        final TakenExamSearchBean takens = new TakenExamSearchBean();
        takens.setConnection(null, con);
        takens.setIndividualId(individualId);
        takens.setExamYear(exam.getExamYear());
        //対象模試を省く
        takens.setExcludeExamCds(new String[]{exam.getExamCD()});
        //受験学力測定を省く
        takens.setExamTypeCDNot(new String[]{KNUtil.TYPECD_ABILITY});
        if (isHelpDesk) {
            takens.setInOpenDate(exam.getInDataOpenDate());
        } else {
            takens.setOutOpenDate(exam.getOutDataOpenDate());
        }
        takens.setOrder(1);
        takens.execute();

        return takens;
    }

    private OnepointBean searchOnePointBean(
            final Connection con) throws Exception {

        final OnepointBean onebean = new OnepointBean();
        onebean.setConnection(null, con);
        onebean.setScreenID("i103");
        onebean.execute();

        return onebean;
    }

    private String[] setupSelectives(final ExamData target, final TakenExamSearchBean takenBean) throws Exception {

        final String[] cds = {"", "", ""};

        final Iterator ite = takenBean.getRecordSet().iterator();

        int count = 0;

        while (ite.hasNext()) {

            IExamData exam = (IExamData) ite.next();

            //対象模試は選択模試に含めない
            if (target.getExamCD().equals(exam.getExamCd())) {
                continue;
            }

            if (count == 0) {
                cds[0] = exam.getExamCd();
            } else if (count == 1) {
                cds[1] = exam.getExamCd();
            } else if (count == 2) {
                cds[2] = exam.getExamCd();
            } else {
                break;
            }

            count ++;
        }

        return cds;
    }

}

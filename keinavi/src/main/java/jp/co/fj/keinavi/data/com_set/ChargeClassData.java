/*
 * 作成日: 2004/07/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.com_set;

import java.io.Serializable;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ChargeClassData implements Serializable, Comparable {

	// 学年
	private short grade = 0;
	// クラス名
	private String className = null;


	/**
	 * コンストラクタ
	 * @param grade
	 * @param className
	 */
	public ChargeClassData(short grade, String className) {
		this.grade = grade;
		this.className = className;
	}

	/**
	 * コンストラクタ
	 * @param grade
	 * @param className
	 */
	public ChargeClassData(String grade, String className) {
		this(Short.parseShort(grade), className);
	}

	/**
	 * コンストラクタ
	 * @param grade
	 * @param className
	 */
	public ChargeClassData(String[] str) {
		this(str[0], str[1]);
	}

	/**
	 * コンストラクタ
	 * @param grade
	 * @param className
	 */
	public ChargeClassData(String str) {
		this(str.split(":", 2));
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		
		final ChargeClassData data = (ChargeClassData) o;
		
		// 3, 2, 1, 4, 5, 9
		if (getGradeSeq() > data.getGradeSeq()){
			return 1;
		} else if (getGradeSeq() < data.getGradeSeq()) {
			return -1;
		} else {
			// クラスの昇順（通常クラスが優先）
			if (getClassName().length() > 2 && data.getClassName().length() < 3) {
				return 1;
			} else 	if (getClassName().length() < 3 && data.getClassName().length() > 2) {
				return -1;
			} else {
				return getClassName().compareTo(data.getClassName());
			}
		}
	}

	/**
	 * 学年のソートキーを返す
	 * 
	 * @return
	 */
	private int getGradeSeq() {
		
		switch (this.grade) {
			case 1: return 3;
			case 3: return 1;
			default: return this.grade;
		}
	}
	
	/**
	 * データを一意に識別するキーを返す
	 * @return
	 */
	public String getKey() {
		return grade + ":" + className;
	}

	/**
	 * @return
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @return
	 */
	public short getGrade() {
		return grade;
	}

	/**
	 * @param string
	 */
	public void setClassName(String string) {
		className = string;
	}

	/**
	 * @param s
	 */
	public void setGrade(short s) {
		grade = s;
	}

}

package jp.co.fj.keinavi.beans.graph;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * グラフ出力対象Beanのアダプタ
 * 
 * 2006.07.27	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class GraphCheckerBeanAdapter extends AbstractGraphCheckerBean {

	/**
	 * コンストラクタ
	 * 
	 * @param id プロファイルカテゴリID
	 */
	public GraphCheckerBeanAdapter(final String id) {
		super(id);
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.AbstractGraphTargetBean#checkTypeConfig(
	 * 				javax.servlet.http.HttpServletRequest)
	 */
	public int checkTypeConfig(HttpServletRequest request, boolean isBundle) {
		return 0;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.AbstractGraphTargetBean#checkCourseConfig(
	 * 				javax.servlet.http.HttpServletRequest)
	 */
	public int checkCourseConfig(HttpServletRequest request, boolean isBundle) {
		return 0;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.AbstractGraphTargetBean#checkClassConfig(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	public int checkClassConfig(HttpServletRequest request, boolean isBundle) {
		return 0;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.AbstractGraphTargetBean#checkSchoolConfig(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	public int checkSchoolConfig(HttpServletRequest request, boolean isBundle) {
		return 0;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.AbstractGraphCheckerBean#hasSubjectGraphSheet(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	public boolean hasSubjectGraphSheet(HttpServletRequest request) {
		return false;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.AbstractGraphCheckerBean#hasClassGraphSheet(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	public boolean hasClassGraphSheet(HttpServletRequest request) {
		// s402を除いて型・科目はセットになる
		return hasSubjectGraphSheet(request);
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.AbstractGraphCheckerBean#hasSchoolGraphSheet(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	public boolean hasSchoolGraphSheet(HttpServletRequest request) {
		// s402を除いて型・科目はセットになる
		return hasSubjectGraphSheet(request);
	}

}

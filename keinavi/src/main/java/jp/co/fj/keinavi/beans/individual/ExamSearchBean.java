/*
 * 作成日: 2004/09/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.ExaminationData;

import com.fjh.beans.DefaultSearchBean;

/**
 * @author T.Yamada
 * 大学データの取得
 */
public class ExamSearchBean extends DefaultSearchBean{

	/** 入力パラメタ */
	private String examYear;
	private String examCd;
	private ExamSession examSession;

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		String sql = "SELECT E.EXAMYEAR, E.EXAMCD, E.EXAMNAME, E.EXAMNAME_ABBR, "+
					"E.EXAMTYPECD, E.EXAMST, E.EXAMDIV, E.TERGETGRADE, E.INPLEDATE, "+
					"E.IN_DATAOPENDATE, E.OUT_DATAOPENDATE, E.DOCKINGEXAMCD, "+
					"E.DOCKINGTYPE, E.DISPSEQUENCE, E.MASTERDIV " +
					"FROM EXAMINATION E WHERE 1 = 1 ";
		
		if(examYear != null){
			sql += " AND E.EXAMYEAR = '"+getExamYear()+"' ";	
		}
		if(examCd != null){
			sql += " AND E.EXAMCD = '"+getExamCd()+"' ";
		}
		if(examSession != null){
			sql += examSession.getExamOptionQuery();
		}
		
		sql += "ORDER BY E.DISPSEQUENCE";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()){
				ExaminationData data = new ExaminationData();
				data.setExamYear(rs.getString("EXAMYEAR"));
				data.setExamCd(rs.getString("EXAMCD"));
				data.setExamName(rs.getString("EXAMNAME"));
				data.setExamNameAbbr(rs.getString("EXAMNAME_ABBR"));
				data.setExamTypeCd(rs.getString("EXAMTYPECD"));
				data.setExamSt(rs.getString("EXAMST"));
				data.setExamDiv(rs.getString("EXAMDIV"));
				data.setTergetGrade(rs.getString("TERGETGRADE"));
				data.setInpleDate(rs.getString("INPLEDATE"));
				data.setInDataOpenDate(rs.getString("IN_DATAOPENDATE"));
				data.setOutDataOpenData(rs.getString("OUT_DATAOPENDATE"));
				data.setDockingExamCd(rs.getString("DOCKINGEXAMCD"));
				data.setDockingType(rs.getString("DOCKINGTYPE"));
				data.setDispSequence(rs.getString("DISPSEQUENCE"));
				data.setMasterDiv(rs.getString("MASTERDIV"));

				recordSet.add(data);
			}
		}catch(SQLException sqle){
			System.out.println("sqlexception : "+sqle);
			System.out.println(sql);
			throw sqle;
		}catch(Exception e){
			System.out.println("exception : "+e);
			throw e;
		} finally {
			if (rs != null) rs.close();
			if (ps != null) ps.close();
		}
	}

	/**
	 * @return
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @param string
	 */
	public void setExamCd(String string) {
		examCd = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @return
	 */
	public ExamSession getExamSession() {
		return examSession;
	}

	/**
	 * @param session
	 */
	public void setExamSession(ExamSession session) {
		examSession = session;
	}

}

package jp.co.fj.keinavi.excel.data.school;

/**
 * 校内成績分析−他校比較−設問別成績 設問別人数リスト
 * 作成日: 2004/07/20
 * @author	A.Iwata
 */
public class S43GakkoListBean {
	//学校名
	private String strGakkomei = "";
	//高校用グラフ表示フラグ
	private int intDispGakkoFlg = 0;
	//受験人数
	private int intNinzu = 0;
	//得点率
	private float floTokuritsu = 0;
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public int getIntDispGakkoFlg() {
		return this.intDispGakkoFlg;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public float getFloTokuritsu() {
		return this.floTokuritsu;
	}

	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setIntDispGakkoFlg(int intDispGakkoFlg) {
		this.intDispGakkoFlg = intDispGakkoFlg;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setFloTokuritsu(float floTokuritsu) {
		this.floTokuritsu = floTokuritsu;
	}
}

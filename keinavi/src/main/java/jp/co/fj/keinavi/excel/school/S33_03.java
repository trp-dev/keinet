/**
 * 校内成績分析−クラス比較　設問別成績（クラス比較）
 *      Excelファイル編集
 * 作成日: 2019/09/04
 * @author      M.Ooseto
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S33Item;
import jp.co.fj.keinavi.excel.data.school.S33KokugoClassListBean;
import jp.co.fj.keinavi.excel.data.school.S33KokugoListBean;
import jp.co.fj.keinavi.excel.data.school.S33KokugoQuestionClassListBean;
import jp.co.fj.keinavi.excel.data.school.S33KokugoQuestionListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S33_03 {

    // 帳票作成結果
    // 0:正常終了
    // 1:ファイル読込みエラー
    // 2:ファイル書込みエラー
    // 3:データセットエラー
    private int noerror = 0;
    private int errfread = 1;
    private int errfwrite = 2;
    private int errfdata = 3;

    // 共通関数用クラス インスタンス
    private CM cm = new CM();

    // 帳票テンプレートファイル
    private String masterfile = "S33_03";

    // 出力固定値
    private String BUNDLENAME = "学校名　：";
    private String EXAMNAME = "対象模試：";
    private String CLASSNAME = "クラス";
    private String ERRORMESSAGE = "データセットエラー";

    // 出力項目位置
    public enum CURSOL {
        // 総合評価−校内全体
        KOKUGO_START(6,1),
        KOKUGO_END(6,12),
        // 総合評価−クラス
        KOKUGO_CLASS_START(7,0),
        KOKUGO_CLASS_END(26,12),

        // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD START
        // 問別評価−校内全体
        KOKUGO_QUESTION_START(30,1),
        //KOKUGO_QUESTION_END(30,24),
        KOKUGO_QUESTION_END(30,30),
        // 問別評価−クラス
        KOKUGO_QUESTION_CLASS_START(31,0),
        //KOKUGO_QUESTION_CLASS_END(50,24);
        KOKUGO_QUESTION_CLASS_END(50,30);
        // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD END

        private final int col;
        private final int row;

        private CURSOL(int row, int col) {
            this.col = col;
            this.row = row;
        }
        public int getCol() {
            return this.col;
        }
        public int getRow() {
            return this.row;
        }

    }

    /**
     * Excel編集メイン
     * @param s33Item データクラス
     * @param outfilelist 出力Excelファイル名（フルパス）
     * @param intSaveFlg 1:保存 2:印刷 3:保存／印刷
     * @param UserID ユーザーID
     * @return 帳票作成結果
     */
    public int s33_03EditExcel(S33Item s33Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

        KNLog log = KNLog.getInstance(null,null,null);
        HSSFWorkbook workbook = null;
        HSSFSheet workSheet = null;
        HSSFRow workRow = null;
        HSSFCell workCell = null;

        // マスタExcel読み込み
        workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);

        // マスタExcel読込みエラー
        if( workbook==null ){
            return errfread;
        }

        workSheet = workbook.cloneSheet(0);

        // データ検索結果取得
        S33KokugoListBean result = (S33KokugoListBean) s33Item.getS33KokugoList().get(0);

        try {
            // 行
            int row = 0;
            // 列
            int col = 0;

            int index = 0;

            // ヘッダ右側に帳票作成日時を表示する
            cm.setHeader(workbook, workbook.cloneSheet(0));

            // セキュリティスタンプセット
            // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD START
            //String secuFlg = cm.setSecurity(workbook, workSheet, s33Item.getIntSecuFlg() ,23 ,24);
            //workCell = cm.setCell(workSheet, workRow, workCell, 0, 23);
            String secuFlg = cm.setSecurity(workbook, workSheet, s33Item.getIntSecuFlg() ,29 ,30);
            workCell = cm.setCell(workSheet, workRow, workCell, 0, 29);
            workCell.setCellValue(secuFlg);
            // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD END

            // 学校名
            workCell = cm.setCell(workSheet, workRow, workCell, 1, 0);
            workCell.setCellValue(BUNDLENAME + result.getBundleName());

            // 対象模試
            workCell = cm.setCell(workSheet, workRow, workCell, 2, 0);
            workCell.setCellValue(EXAMNAME + result.getExamName());

            // 総合評価-校内全体
            row = CURSOL.KOKUGO_START.getRow();
            col = CURSOL.KOKUGO_START.getCol();
            // 受験者数出力
            setValue(workSheet, workRow, row, col, result.getNumbers(), 0, 0);
            col += 2;
            // 人数(割合)出力
            setValue(workSheet, workRow, row, col, result.getEvalANumbers(), result.getEvalACompratio(), 1);
            col += 2;
            setValue(workSheet, workRow, row, col, result.getEvalBNumbers(), result.getEvalBCompratio(), 1);
            col += 2;
            setValue(workSheet, workRow, row, col, result.getEvalCNumbers(), result.getEvalCCompratio(), 1);
            col += 2;
            setValue(workSheet, workRow, row, col, result.getEvalDNumbers(), result.getEvalDCompratio(), 1);
            col += 2;
            setValue(workSheet, workRow, row, col, result.getEvalENumbers(), result.getEvalECompratio(), 1);

            // 総合評価-クラス
            List <S33KokugoClassListBean> list1 = result.getS33KokugoClassList();
            index = 0;
            row = CURSOL.KOKUGO_CLASS_START.getRow();
            col = CURSOL.KOKUGO_CLASS_START.getCol();
            while(index < list1.size() && row <= CURSOL.KOKUGO_CLASS_END.getRow()) {
                S33KokugoClassListBean data = list1.get(index);
                // クラス名出力
                workCell = cm.setCell(workSheet, workRow, workCell, row, col);
                workCell.setCellValue(data.getKokugoClass() + CLASSNAME);
                col++;
                // 受験者数出力
                setValue(workSheet, workRow, row, col, data.getNumbers(), 0, 0);
                col += 2;
                // 人数(割合)出力
                setValue(workSheet, workRow, row, col, data.getEvalANumbers(), data.getEvalACompratio(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data.getEvalBNumbers(), data.getEvalBCompratio(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data.getEvalCNumbers(), data.getEvalCCompratio(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data.getEvalDNumbers(), data.getEvalDCompratio(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data.getEvalENumbers(), data.getEvalECompratio(), 1);

                col = CURSOL.KOKUGO_CLASS_START.getCol();
                row++;
                index++;
            }

            // 問別評価-校内全体
            List <S33KokugoQuestionListBean> list2 = result.getS33KokugoQuestionList();
            index = 0;
            row = CURSOL.KOKUGO_QUESTION_START.getRow();
            col = CURSOL.KOKUGO_QUESTION_START.getCol();
            while(index < list2.size() && row <= CURSOL.KOKUGO_QUESTION_END.getRow()) {
                S33KokugoQuestionListBean data = list2.get(index);
                // 人数(割合)出力
                // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD START
                setValue(workSheet, workRow, row, col, data.getEvalANumbers(), data.getEvalACompratio(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data.getEvalAAstNumbers(), data.getEvalAAstCompratio(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data.getEvalBNumbers(), data.getEvalBCompratio(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data.getEvalBAstNumbers(), data.getEvalBAstCompratio(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data.getEvalCNumbers(), data.getEvalCCompratio(), 1);
                col += 2;
                //setValue(workSheet, workRow, row, col, data.getEvalDNumbers(), data.getEvalDCompratio(), 1);
                //col += 2;
                // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD END

                // 1クラス分出力完了判定
                if(col > CURSOL.KOKUGO_QUESTION_CLASS_END.getCol()) {
                    col = CURSOL.KOKUGO_QUESTION_START.getCol();
                    row++;
                }
                index++;
            }

            // 問別評価-クラス
            List <S33KokugoQuestionClassListBean> list3 = result.getS33KokugoQuestionClassList();
            Collections.sort(list3, new comparableObject());
            int titleflg = 1;
            index = 0;
            row = CURSOL.KOKUGO_QUESTION_CLASS_START.getRow();
            col = CURSOL.KOKUGO_QUESTION_CLASS_START.getCol();
            while(index < list3.size() && row <= CURSOL.KOKUGO_QUESTION_CLASS_END.getRow()) {
                S33KokugoQuestionClassListBean data = list3.get(index);
                // クラス名出力
                if(titleflg == 1) {
                    workCell = cm.setCell(workSheet, workRow, workCell, row, col);
                    workCell.setCellValue(data.getKokugoClass() + CLASSNAME);
                    col++;
                    titleflg = 0;
                }
                // 人数(割合)出力
                // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD START
                setValue(workSheet, workRow, row, col, data.getEvalANumbers(), data.getEvalACompratio(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data.getEvalAAstNumbers(), data.getEvalAAstCompratio(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data.getEvalBNumbers(), data.getEvalBCompratio(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data.getEvalBAstNumbers(), data.getEvalBAstCompratio(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data.getEvalCNumbers(), data.getEvalCCompratio(), 1);
                col += 2;
                //setValue(workSheet, workRow, row, col, data.getEvalDNumbers(), data.getEvalDCompratio(), 1);
                //col += 2;
                // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD END

                // 1クラス分出力完了判定
                if(col > CURSOL.KOKUGO_QUESTION_CLASS_END.getCol()) {
                    col = CURSOL.KOKUGO_QUESTION_CLASS_START.getCol();
                    row++;
                    titleflg = 1;
                }
                index++;
            }

            // Excel書込み
            boolean bolRet = false;
            bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, 1);

            // Excel書込みエラー
            if( bolRet == false ){
                return errfwrite;
            }

        }catch(Exception e){
            log.Err(masterfile, ERRORMESSAGE, e.toString());
            return errfdata;
        }

        return noerror;

    }

    /**
     * Excelにデータ出力
     * @param workSheet Excelシート情報
     * @param workRow Excel行情報
     * @param row 行
     * @param col 列
     * @param number 人数
     * @param compratio 割合
     * @param flg 0:受験者数 1:人数(割合)
     */
    public void setValue(HSSFSheet workSheet, HSSFRow workRow, int row, int col, int number, float compratio,int flg) {

        HSSFCell workCell = null;

        if(flg == 0) {
            // 受験者数の表示
            if(number > 0) {
                workCell = cm.setCell(workSheet, workRow, workCell, row, col);
                workCell.setCellValue(number);
            }
        }
        else {
            // 人数、割合が0以外の場合は表示
            if(number > 0 && compratio > Float.parseFloat("0.0")) {
                workCell = cm.setCell(workSheet, workRow, workCell, row, col);
                workCell.setCellValue(number);
                workCell = cm.setCell(workSheet, workRow, workCell, row, col + 1 );
                workCell.setCellValue("(" + compratio + ")");
            }
        }

    }

    public class comparableObject implements Comparator<S33KokugoQuestionClassListBean> {
        /**
         * 表示順序、設問番号でソート
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        public int compare(S33KokugoQuestionClassListBean obj1, S33KokugoQuestionClassListBean obj2) {
            if(Integer.valueOf(obj1.getDispsequence()) < Integer.valueOf(obj2.getDispsequence())){
                return -1;
              }else if(Integer.valueOf(obj1.getDispsequence()) > Integer.valueOf(obj2.getDispsequence())){
                return 1;
              }else{
                  if(Integer.valueOf(obj1.getQuestionNo()) < Integer.valueOf(obj2.getQuestionNo())) {
                      return -1;
                  }else if(Integer.valueOf(obj1.getQuestionNo()) > Integer.valueOf(obj2.getQuestionNo())) {
                      return 1;
                  }else {
                      return 0;
                  }
              }
        }
    }

}

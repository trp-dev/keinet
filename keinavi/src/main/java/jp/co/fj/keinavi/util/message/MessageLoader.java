/*
 * 作成日: 2004/09/03
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.message;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * メッセージを管理するクラス
 *
 * @author kawai
 */
public class MessageLoader {

	// Singleton
	private static final MessageLoader instance = new MessageLoader();
	// 設定ファイル名
	private final String filename = "message.txt";

	private Map message; // メッセージマップ
	private String path; // 設定ファイルまでのパス

	/**
	 * コンストラクタ
	 */
	private MessageLoader() {
	}

	/**
	 * このクラスのインスタンスを返す
	 * @return Singletonなインスタンス
	 */
	public static MessageLoader getInstance() {
		return instance;
	}

        public Map getAllMessage() throws IOException {
            // 未ロードなら初期化
            if (message == null) {
                    this.init();
            }
            return message;
        }

        /**
	 * 指定されたメッセージを取得する
	 * @param key メッセージID
	 * @return SQL SQLオブジェクト
	 * @throws IOException
	 */
	public String getMessage(String key) throws IOException {
		// 未ロードなら初期化
		if (message == null) {
			this.init();
		}

		return (String) message.get(key);
	}

	/**
	 * メッセージマップの初期化をする
	 */
	private synchronized void init() throws IOException {
		// NULLなら初期化
		if (message == null) message = new HashMap();
		// あるなら中止
		else return;

		BufferedReader reader = new BufferedReader(
			new InputStreamReader(
				new FileInputStream(path + File.separator + filename), "MS932"
			)
		);

		try {
			String line;
			while ((line = reader.readLine()) != null) {
				int index = line.indexOf('=');

				if (index > 0) {
					message.put(
						line.substring(0, index),
						line.substring(index + 1, line.length())
					);
				}
			}
		} finally {
			if (reader != null) reader.close();
		}
	}

	/**
	 * @param string
	 */
	public void setPath(String string) {
		path = string;
	}

}

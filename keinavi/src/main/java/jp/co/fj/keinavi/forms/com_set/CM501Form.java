package jp.co.fj.keinavi.forms.com_set;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM501Form extends BaseForm {

	// 選択方法
	private String selection = null;
	// 比較対象となるクラス名の配列（全選択）
	private String[] compare1 = null;
	// 比較対象となるクラス名の配列（個別選択）
	private String[] compare2 = null;
	// グラフ表示対象となるクラス名の配列（全選択）
	private String[] graph1 = null;
	// グラフ表示対象となるクラス名の配列（個別選択）
	private String[] graph2 = null;

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * @return
	 */
	public String[] getCompare1() {
		return compare1;
	}

	/**
	 * @return
	 */
	public String[] getCompare2() {
		return compare2;
	}

	/**
	 * @return
	 */
	public String[] getGraph1() {
		return graph1;
	}

	/**
	 * @return
	 */
	public String[] getGraph2() {
		return graph2;
	}

	/**
	 * @return
	 */
	public String getSelection() {
		return selection;
	}

	/**
	 * @param strings
	 */
	public void setCompare1(String[] strings) {
		compare1 = strings;
	}

	/**
	 * @param strings
	 */
	public void setCompare2(String[] strings) {
		compare2 = strings;
	}

	/**
	 * @param strings
	 */
	public void setGraph1(String[] strings) {
		graph1 = strings;
	}

	/**
	 * @param strings
	 */
	public void setGraph2(String[] strings) {
		graph2 = strings;
	}

	/**
	 * @param string
	 */
	public void setSelection(String string) {
		selection = string;
	}

}

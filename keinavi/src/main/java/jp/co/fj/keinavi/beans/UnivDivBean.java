/*
 * 作成日: 2004/07/14
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.fjh.beans.DefaultBean;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class UnivDivBean extends DefaultBean {

	// 大学区分 → 大学区分名称変換マップ
	private static Map univDivMap = new HashMap();

	static {
		univDivMap.put("01", "国公立");
		univDivMap.put("02", "国公立");
		univDivMap.put("03", "私立");
		univDivMap.put("04", "短大");
		univDivMap.put("05", "短大");
		univDivMap.put("06", "その他");
		univDivMap.put("07", "その他");
		univDivMap.put("08", "その他");
		univDivMap.put("09", "その他");
	}
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
	}


	/**
	 * @return
	 */
	public Map getUnivDivMap() {
		return univDivMap;
	}

	/**
	 * @param map
	 */
	public void setUnivDivMap(Map map) {
		univDivMap = map;
	}
	
	/**
	 * 国公立大学か？
	 * @param c 大学区分コード
	 * @return 国公立大学の場合true、それ以外false
	 */
	public static boolean isKokkouritsuUniv(String c) {
		
		// 国立 or 公立
		if (isKokuritsuUniv(c) || isKouritsuUniv(c)) return true;
		else return false;
		
	}
	
	/**
	 * 短期大学か？
	 * @param c 大学区分コード
	 * @return 短期大学の場合true、それ以外false
	 */
	public static boolean isJnrUniv(String c) {
		
		// 国公立短大 or 私立短大
		if (isKoKKouritsuJnrUniv(c) || isShiritsuJnrUniv(c)) return true;
		else return false;
		
	}
	
	/**
	 * その他か？
	 * @param c 大学区分コード
	 * @return その他の場合true、
	 */
	public static boolean isOther(String c) {
		
		// 文科省外 or 文科省外短 or 専修・各種
		if (isOutOfMonka(c) || isOutOfMonkaJur(c) || isSpecial(c)) return true;
		else return false;
		
	}
	
	/**
	 * 国立大学か？
	 * @param c 大学区分コード
	 * @return 国立大学の場合にtrue、それ以外はfalse
	 */
	private static boolean isKokuritsuUniv(String c){
		
		if ("01".equals(c)) return true;
		else return false;
		
	}
	
	/**
	 * 公立大学か？
	 * @param c 大学区分コード
	 * @return 公立大学の場合にtrue、それ以外はfalse
	 */
	private static boolean isKouritsuUniv(String c){
		
		if ("02".equals(c)) return true;
		else return false;
		
	}
	
	/**
	 * 私立大学か？
	 * @param c 大学区分コード
	 * @return 私立大学の場合にtrue、それ以外はfalse
	 */
	public static boolean isShirituUniv(String c){
		
		if ("03".equals(c)) return true;
		else return false;
		
	}
	
	/**
	 * 国公立短大か？
	 * @param c 大学区分コード
	 * @return 国公立短大の場合にtrue、それ以外はfalse
	 */
	private static boolean isKoKKouritsuJnrUniv(String c){
		
		if ("04".equals(c)) return true;
		else return false;
		
	}
	
	/**
	 * 私立短大か？
	 * @param c 大学区分コード
	 * @return 私立短大の場合にtrue、それ以外はfalse
	 */
	private static boolean isShiritsuJnrUniv(String c){
		
		if ("05".equals(c)) return true;
		else return false;
		
	}
	
	/**
	 * 文科省外か？
	 * @param c 大学区分コード
	 * @return 文科省外の場合にtrue、それ以外はfalse
	 */
	private static boolean isOutOfMonka(String c){
		
		if ("06".equals(c)) return true;
		else return false;
		
	}
	
	/**
	 * 文科省外短か？
	 * @param c 大学区分コード
	 * @return 文科省外短の場合にtrue、それ以外はfalse
	 */
	private static boolean isOutOfMonkaJur(String c){
		
		if ("07".equals(c)) return true;
		else return false;
		
	}
	
	/**
	 * 専修・各種か？
	 * @param c 大学区分コード
	 * @return 専修・各種の場合にtrue、それ以外はfalse
	 */
	private static boolean isSpecial(String c){
		
		if ("08".equals(c) || "09".equals(c)) return true;
		else return false;
		
	}
	
}

/*
 * 作成日: 2004/07/21
 *
 * バランスチャート(合格者平均)の偏差値データ取得用Bean
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import jp.co.fj.keinavi.data.ExamData;//[3] add
import jp.co.fj.keinavi.data.individual.CourseData;
import jp.co.fj.keinavi.data.individual.IExamData;
import jp.co.fj.keinavi.data.individual.SubRecordIData;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNUtil;

import com.fjh.beans.DefaultBean;

/**
 * バランスチャート(合格者平均)<BR>
 * DBより偏差値を取得する。
 * @author kondo
 * 
 * history
 *   Symbol Date       Person     Note
 *   [1]    2005.01.21 kondo      change ２次 英語＋リスニング追加対応
 *   [2]    2005.01.21 kondo      change 配点200点の科目が複数ある場合最初の1つをとるように修正
 *   [3]    2005.03.02 kondo      change 科目変換に、模試コードを追加。
 *   [4]    2005.05.29 kondo      change 過去合格者平均の英語は科目コード1000に固定
 * 　[5]    2005.09.16 kondo      change 科目取得の際EXAMSUBJECTではなく、Viewを参照するように修正
 *
 * 2009.11.24   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応 
 * 2010.01.18   Shoji Hase Totec
 *              「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 *         　　 「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 */
public class I104Bean extends DefaultBean {
	
	private static final long serialVersionUID = 3212052505473195157L;
	
	//模試ＳＱＬ
	private String SQL_BASE = 
		"SELECT"+
		" /*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */ "+//12/5 sql hint
		" SUBRECORD_I.INDIVIDUALID"+
		" ,SUBRECORD_I.A_DEVIATION"+
		" ,SUBRECORD_I.ACADEMICLEVEL"+
		" ,SUBRECORD_I.SUBCD"+
		" ,EXAMINATION.EXAMNAME"+
		" ,EXAMINATION.EXAMTYPECD"+
		" ,SUBRECORD_I.EXAMCD"+
		" ,SUBRECORD_I.EXAMYEAR"+
		//" ,EXAMSUBJECT.DISPSEQUENCE"+//[5] delete
		//" ,EXAMSUBJECT.SUBALLOTPNT"+//[5] delete
		" ,V_I_CMEXAMSUBJECT.DISPSEQUENCE"+//[5] add
		" ,V_I_CMEXAMSUBJECT.SUBALLOTPNT"+//[5] add
		" ,SUBRECORD_I.SCOPE"+
		" FROM SUBRECORD_I"+
		" INNER JOIN EXAMINATION ON EXAMINATION.EXAMYEAR = SUBRECORD_I.EXAMYEAR"+
		" AND EXAMINATION.EXAMCD = SUBRECORD_I.EXAMCD"+
		//" INNER JOIN EXAMSUBJECT ON EXAMSUBJECT.EXAMYEAR = SUBRECORD_I.EXAMYEAR"+//[5] delete
		//" AND SUBRECORD_I.EXAMCD = EXAMSUBJECT.EXAMCD"+//[5] delete
		//" AND SUBRECORD_I.SUBCD = EXAMSUBJECT.SUBCD";//[5] delete
		" INNER JOIN V_I_CMEXAMSUBJECT ON V_I_CMEXAMSUBJECT.EXAMYEAR = SUBRECORD_I.EXAMYEAR"+//[5] add
		" AND SUBRECORD_I.EXAMCD = V_I_CMEXAMSUBJECT.EXAMCD"+//[5] add
		" AND SUBRECORD_I.SUBCD = V_I_CMEXAMSUBJECT.SUBCD";//[5] add
	
	//大学過去平均ＳＱＬ
	private String UNIV_SQL = 
	"(SELECT " + 
	" /*+ INDEX(CR PK_CANDIDATERATING) */ "+//12/5 sql hint
	"CR.CANDIDATERANK" + 		//志望順位
	",HS.UNINAME_ABBR UNIVNAME_ABBR" + 		//大学名短縮
	",HS.FACULTYNAME_ABBR" +	 //学部名短縮
	",HS.DEPTNAME_ABBR" + 		//学科名短縮
	",ST.CURSUBCD" + 				//科目コード
	",PS.SUCCESSAVGDEV" + 		//合格者平均偏差値
	" FROM EXAMINATION EX";
	
	
	//コンボボックスＳＱＬ
	private String COMBO_SQL_BASE = 
	"SELECT " + 
	" /*+ INDEX(CR PK_CANDIDATERATING) */ "+//12/5 sql hint
	"CR.CANDIDATERANK" + 	//志望順位
	",HS.UNINAME_ABBR UNIVNAME_ABBR" + //大学名短縮
	",HS.FACULTYNAME_ABBR" + //学部名短縮
	",HS.DEPTNAME_ABBR" + 	//学科名短縮
	" FROM EXAMINATION EX ";
	
	private String moshiSql;
	private String univSql;
	private String comboSql;
	private String countSql;

	private String targetExamYear;
	private String targetExamCode;
	private String[] targetCandidateRanks;//再表示の時に使う＝2回目以降のアクセス
	private String[] deviationRanges;
	
	private String targetEngCd;	//対象の英語科目
	private String targetMathCd;	//対象の数学科目
	private String targetJapCd;	//対象の国語科目
	private String targetSciCd;	//対象の理科科目
	private String targetSocCd;	//対象の社会科目
	private String ans1stSciCd;	//第1解答科目の理科科目
	private String ans1stSocCd;	//第1解答科目の社会科目

	private String personId;
	
	private String avgChoice;		// 教科複数受験フラグ（1:平均、2：良い方）
	
	private boolean isFirstAccess;
	
	private String examTypecd; //模試種類
	private boolean examflg = false;	//マーク模試だったらtrue
	private boolean dispflg = false;	//マーク模試だったらtrue
	private boolean engDispFlg = false;//対象模試の英語を受けていたらtrue//[4] add
	
	private Collection courseDatas;//教科データ
	
	private Collection comboxDatas;//コンボ内容（UniversityData）
	
	private Collection examinationDatas;	//前画面にて選択された、対象模試データ
	public int getExaminationDataSize(){
		int size = 0;
		if(getExaminationDatas() != null)
			size = getExaminationDatas().size();
		return size;
	}
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		try{
			conn.setAutoCommit(false);
			
			ExamData[] examDatas = new ExamData[1];//[3] add
			examDatas[0] = new ExamData();//[3] add
			examDatas[0].setExamYear(getTargetExamYear());//[3] add
			examDatas[0].setExamCD(getTargetExamCode());//[3] add
			
			//KNUtil.buildSubCDTransTable(conn, getTargetExamYear(), getTargetExamCode());//[3] delete
			KNUtil.buildSubCDTransTable(conn, examDatas);//[3] add
			
			//模試情報
			execDeviations();
			//過去模試情報
			setExaminationDatas(execUniversityDatas());
			//コンボ内容
			setComboxDatas(execComboBoxDatas());
			//教科名取得
			setCourseDatas(execCourseDatas(getTargetExamYear(), getTargetExamCode()));
		
			conn.commit();
		}catch(Exception e){
			conn.rollback();
			e.printStackTrace();
			throw new SQLException("i104BeanSQLに失敗しました。" + e);
		} finally {

		}
	}
	/**
	 * 初回アクセス時
	 * @param string1
	 * @param string2
	 * @param stringss
	 * @param b
	 * @param string3
	 */
	public void setSearchCondition(String string1, String string2, String devRanges, String string3){
		
		setTargetExamYear(string1);
		setTargetExamCode(string2);
		setDeviationRanges(IUtilBean.deconcatComma(devRanges));
		setPersonId(string3);
		//初回アクセス時は存在しない
		String[] ranks = new String[3];
		ranks[0] = "1";
		ranks[1] = "2";
		ranks[2] = "3";
		setTargetCandidateRanks(ranks);

		//模試ＳＱＬの作成（前画面にて選択）
		appendMoshiSql(SQL_BASE);
		appendMoshiSql(getYearAndPersonCondition(getPersonId(), getTargetExamYear(), getTargetExamCode()));
		
		
		//過年度合格者成績ＳＱＬの作成
		appendUnivSql(UNIV_SQL);
		appendUnivSql(getUnivCondition(getPersonId(), getTargetExamYear(), getTargetExamCode(), getTargetCandidateRanks()));
		
		appendCountSql(SQL_BASE);
		appendCountSql(get200count(getPersonId(), getTargetExamYear(), getTargetExamCode()));
		//志望大学順位を設定
		//志望大学並び順を設定
//		appendUnivSql(getCandidateRankCondition(getTargetCandidateRanks()));
//		appendUnivSql(getUnivOrderBy());
		
		//コンボボックスＳＱＬの作成
		//条件追加：個人ＩＤ、模試年度、大学コード、学部コード、学科コードが一致
		appendComboSql(COMBO_SQL_BASE);
		appendComboSql(getComboSqlCondition(getPersonId(), getTargetExamYear(), getTargetExamCode()));
	}
	/**
	 * 二回目以降のアクセス時
	 * @param string1
	 * @param string2
	 * @param stringss
	 * @param b
	 * @param string3
	 * @param string4 選択された模試コード３つ
	 */
	public void setSearchCondition(String string1, String string2, 
									String[] stringss, String string3, 
									String[] strings){
		
		setTargetExamYear(string1);
		setTargetExamCode(string2);
		setDeviationRanges(stringss);
		setPersonId(string3);							
		//二回目以降のアクセスにて存在する
		ArrayList temp = new ArrayList();
		for(int i=0; i<strings.length; i++){
			if(strings[i] != null && !strings[i].trim().equals("") && !strings[i].trim().equals("null")){
				temp.add(strings[i]);
			}
		}
		if(temp.size() > 0){
			setTargetCandidateRanks((String[]) temp.toArray(new String[0]));
		}
			
		
		//模試ＳＱＬの作成（前画面にて選択）
		appendMoshiSql(SQL_BASE);
		appendMoshiSql(getYearAndPersonCondition(getPersonId(), getTargetExamYear(), getTargetExamCode()));
		
		//過年度合格者成績ＳＱＬの作成
		appendUnivSql(UNIV_SQL);
		appendUnivSql(getUnivCondition(getPersonId(), getTargetExamYear(), getTargetExamCode(), getTargetCandidateRanks()));
		
		appendCountSql(SQL_BASE);
		appendCountSql(get200count(getPersonId(), getTargetExamYear(), getTargetExamCode()));
		//志望大学順位を設定
		//志望大学並び順を設定
//		appendUnivSql(getCandidateRankCondition(getTargetCandidateRanks()));
//		appendUnivSql(getUnivOrderBy());
		
		//コンボボックスＳＱＬの作成
		//条件追加：個人ＩＤ、模試年度、大学コード、学部コード、学科コードが一致
		appendComboSql(COMBO_SQL_BASE);
		appendComboSql(getComboSqlCondition(getPersonId(), getTargetExamYear(), getTargetExamCode()));
	}
	
	private void appendComboSql(String string){
		if(getComboSql() == null){
			setComboSql("");
		}
		comboSql += string;
	}
	
	//模試ＳＱＬの条件追加？
	private void appendMoshiSql(String string){
		if(getMoshiSql() == null){
			setMoshiSql("");
		}
		moshiSql += string;
	}

	/**
	 * 志望大学ＳＱＬに条件を追加
	 * @param string
	 */
	private void appendUnivSql(String string) {
		if(getUnivSql() == null) {
			setUnivSql("");
		}
		univSql += string;
	}
	/**
	 * メインデータを取得するSQL文にStringを付加する
	 * @param string
	 */
	private void appendCountSql(String string){
		if(getCountSql() == null)
			setCountSql("");
		countSql += string;
	}

	/**
	 * 志望大学順位を複数指定
	 * @param string
	 * @param strings
	 * @return
	 */
	private String getCandidateRankCondition(String[] ranks){
		if(ranks == null){
			return "";
		}else{
			String sqlOption = " AND CR.CANDIDATERANK IN (";
			for(int i = 0; i < ranks.length; i ++){
				if(i != 0)
					sqlOption += ",";
				sqlOption += ranks[i];
			}
			sqlOption += ") ";
			if(ranks.length == 0)
				return "";
			else
				return sqlOption;
		}
	}
	//コンボボックスＳＱＬの検索条件を設定
	private String getComboSqlCondition(String string1, String string2, String string3){
		String sqlOption = 	
							" INNER JOIN CANDIDATERATING CR" +
							" ON CR.INDIVIDUALID = ?"+
							" AND EX.EXAMYEAR = '"+string2+"'" + 
							" AND EX.EXAMCD = '" + string3 + "'"+
							" AND CR.EXAMYEAR = EX.EXAMYEAR" +
							" AND CR.EXAMCD = EX.EXAMCD" + 
							" LEFT JOIN UNIVMASTER_BASIC HS" +
							" ON HS.EVENTYEAR = EX.EXAMYEAR" +
							" AND HS.EXAMDIV = EX.EXAMDIV" +
							" AND HS.UNIVCD = CR.UNIVCD" +
							" AND HS.FACULTYCD = CR.FACULTYCD" + 
							" AND HS.DEPTCD = CR.DEPTCD" + 
							" ORDER BY CR.CANDIDATERANK";
		return sqlOption;
	}
	/**
	 * 模試年度とINDIVIDUALIDの指定
	 * @param string1
	 * @param string2
	 * @return
	 */
	private String getYearAndPersonCondition(String string1, String string2, String string3){
		String sqlOption = " WHERE SUBRECORD_I.INDIVIDUALID = ?"+
							" AND SUBRECORD_I.EXAMYEAR = '"+string2+"'" + 
							" AND EXAMINATION.EXAMCD = '" + string3 + "'"+
							//" ORDER BY EXAMSUBJECT.DISPSEQUENCE"; //[5] delete
							" ORDER BY V_I_CMEXAMSUBJECT.DISPSEQUENCE";//[5] add
							
		return sqlOption;
	}
	
	/**科目配点200の物をカウントする
	 */
	private String get200count(String string1, String string2, String string3){
		String sqlOption = " WHERE SUBRECORD_I.INDIVIDUALID = ?"+
							" AND SUBRECORD_I.EXAMYEAR = '"+string2+"'" + 
							" AND EXAMINATION.EXAMCD = '" + string3 + "'"+
							//" AND EXAMSUBJECT.SUBALLOTPNT = '200'"+//[1] delete
							//" AND (EXAMSUBJECT.SUBALLOTPNT = '200' OR EXAMSUBJECT.SUBCD = '1190') "+//[1] add//[5] delete
//" AND ((SUBSTR(EXAMSUBJECT.SUBCD, 0, 1) = '1') OR (SUBSTR(EXAMSUBJECT.SUBCD, 0, 1) = '2') OR (SUBSTR(EXAMSUBJECT.SUBCD, 0, 1) = '3'))";//[5] delete
							" AND (V_I_CMEXAMSUBJECT.SUBALLOTPNT = '200' OR V_I_CMEXAMSUBJECT.SUBCD = '1190') "+//[5] add
" AND ((SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '1') OR (SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '2') OR (SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '3'))";//[5] add
		return sqlOption;
	}

	/**
	 * 志望大学過去合格者平均をＤＢより取得
	 * 
	 * @param id		個人ＩＤ
	 * @param year		対象模試年度
	 * @param cd		対象模試コード
	 * @return String SQL
	 */
	private String getUnivCondition(String id, String year, String cd, String[] ranks) {
			
		String lastYear = Integer.toString(Integer.parseInt(year) -1);
		String sqlOption = 	" INNER JOIN CANDIDATERATING CR " +
							"ON CR.INDIVIDUALID = ? " +
							"AND EX.EXAMYEAR = '" + year + "' " +
							"AND EX.EXAMCD = '" + cd + "' " +
							"AND CR.EXAMYEAR = EX.EXAMYEAR " +
							"AND CR.EXAMCD = EX.EXAMCD " +
							getCandidateRankCondition(ranks) +
							"LEFT JOIN UNIVMASTER_BASIC HS " +
							"ON HS.EVENTYEAR = EX.EXAMYEAR " +
							"AND HS.EXAMDIV = EX.EXAMDIV " +
							"AND HS.UNIVCD = CR.UNIVCD " +
							"AND HS.FACULTYCD = CR.FACULTYCD " +
							"AND HS.DEPTCD = CR.DEPTCD " +
							"INNER JOIN " +
							"SUBCDTRANS ST " +
							//"ON ST.YEAR = '" + lastYear + "' " +//[3] delete
							"ON ST.EXAMYEAR = '" + lastYear + "' " +//[3] add
							"AND ST.EXAMCD = '" + cd + "' " +//[3] add
							"LEFT JOIN "+ 
							"SUBRECORD_I SI " + 
							"ON SI.INDIVIDUALID = CR.INDIVIDUALID " + 
							"AND SI.EXAMYEAR = EX.EXAMYEAR "+ 
							"AND SI.EXAMCD = EX.EXAMCD " +
							"AND SI.SUBCD = ST.SUBCD " + 
							"LEFT JOIN PREVSUCCESS PS  " +
							//"ON PS.EXAMYEAR = ST.YEAR " +//[3] delete
							"ON PS.EXAMYEAR = ST.EXAMYEAR " +//[3] add
							"AND PS.SUBCD = ST.SUBCD " +
							"AND PS.UNIVCD = CR.UNIVCD " +
							"AND PS.DEPTCD = CR.DEPTCD " +
							"AND PS.FACULTYCD = CR.FACULTYCD " +
							"AND PS.EXAMTYPECD = EX.EXAMTYPECD) " +
							
							"UNION ALL "+
							
							"(SELECT "+
								" /*+ INDEX(CR PK_CANDIDATERATING) */ "+//12/5 sql hint
								"CR.CANDIDATERANK,"+
								"HS.UNINAME_ABBR,"+
								"HS.FACULTYNAME_ABBR,"+
								"HS.DEPTNAME_ABBR,"+
								"PS.SUBCD,"+
								"PS.SUCCESSAVGDEV "+
							"FROM "+
								"EXAMINATION EX "+
								"INNER JOIN CANDIDATERATING CR "+
								"ON CR.INDIVIDUALID = ? " +
								"AND EX.EXAMYEAR = '" + year + "' " +
								"AND EX.EXAMCD = '" + cd + "' " +
								"AND CR.EXAMYEAR = EX.EXAMYEAR "+
								"AND CR.EXAMCD = EX.EXAMCD "+
								getCandidateRankCondition(ranks) +
								"LEFT JOIN UNIVMASTER_BASIC HS "+
								"ON HS.EVENTYEAR = EX.EXAMYEAR "+
								"AND HS.EXAMDIV = EX.EXAMDIV "+
								"AND HS.UNIVCD = CR.UNIVCD "+
								"AND HS.FACULTYCD = CR.FACULTYCD "+
								"AND HS.DEPTCD = CR.DEPTCD "+
								"LEFT JOIN PREVSUCCESS PS "+
								"ON PS.EXAMYEAR = '" + lastYear + "' " +
								"AND PS.UNIVCD = CR.UNIVCD "+
								"AND PS.DEPTCD = CR.DEPTCD "+
								"AND PS.FACULTYCD = CR.FACULTYCD "+
								"AND PS.EXAMTYPECD = EX.EXAMTYPECD " +
								//科目コード6000はいらない
								//"WHERE PS.SUBCD = '2000' OR PS.SUBCD = '4000' OR PS.SUBCD = '5000' OR PS.SUBCD = '6000') "+
								//"WHERE PS.SUBCD = '2000' OR PS.SUBCD = '4000' OR PS.SUBCD = '5000') "+//[4] delete
								"WHERE PS.SUBCD IN ('1000','2000','4000','5000')) "+//[4] add
							"ORDER BY CANDIDATERANK";
							
							return sqlOption;
	}

	/**
	 * 対象年の対象模試の教科コード、教科名をすべて取得
	 * @param String year
	 * @param String code
	 * @return Collection
	 */
	private Collection execCourseDatas(String year, String code) throws Exception {
		String nmGet = 
			"SELECT COURSE.COURSECD, COURSE.COURSENAME" +
			" FROM COURSE, EXAMSUBJECT" +
			" WHERE" +
			" COURSE.COURSECD = RPAD(SUBSTR(EXAMSUBJECT.SUBCD, 1, 1), 4, 0)" +
			" AND EXAMSUBJECT.EXAMCD ='" + code + "'" +
			" AND EXAMSUBJECT.EXAMYEAR = '" + year + "'" +
			" AND COURSE.YEAR = EXAMSUBJECT.EXAMYEAR" +
			" GROUP BY COURSE.COURSECD, COURSE.COURSENAME" +
			" ORDER BY COURSE.COURSECD";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
		pstmt = conn.prepareStatement(nmGet);
		rs = pstmt.executeQuery();
		
		//教科をリストにつめる。
		Collection courseDatas = new ArrayList();
		while(rs.next()) {
			CourseData courseData = new CourseData();
			courseData.setCourseName(nullToEmpty(rs.getString("COURSENAME")));
			courseData.setCourseCd(nullToEmpty(rs.getString("COURSECD")));
			if(Integer.parseInt(nullToEmpty(rs.getString("COURSECD")).substring(0, 1)) < 7){
				//総合ではない
				courseDatas.add(courseData);
			} else {
				//総合だったら追加しない
			}
		}	
		if(pstmt != null) pstmt.close();
		if(rs != null) rs.close();
		
		return courseDatas;
		
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}
	
	/**
	 * コンボボックス用、志望大学データをＤＢから取得します。
	 * 
	 * @return String
	 * @throws SQLException
	 */
	private Collection execComboBoxDatas() throws Exception{
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
		pstmt = conn.prepareStatement(getComboSql());
		pstmt.setString(1, personId);
		//ＳＱＬ実行
		rs = pstmt.executeQuery();
		
		Collection boxDatas = new ArrayList();
		String FULLSIZESPACE = "　";
		//取得データをAttayListにつめる
		while(rs.next()){
			//大学情報
			IExamData niversityData = new IExamData();
			//志望順位の設定
			niversityData.setExamCd(rs.getString("CANDIDATERANK"));
			//志望大学名の設定
			niversityData.setExamName(
			nullToEmpty(rs.getString("UNIVNAME_ABBR")) + FULLSIZESPACE +
			nullToEmpty(rs.getString("FACULTYNAME_ABBR")) + FULLSIZESPACE +
			nullToEmpty(rs.getString("DEPTNAME_ABBR"))
			);
			//名称の設定
			boxDatas.add(niversityData);
			//最大９個制限
			if(boxDatas.size() == 9) {
				break;
			}
		}
		if(pstmt != null) pstmt.close();
		if(rs != null) rs.close();

		return boxDatas;
		
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}
	
	/**
	 * 対象志望校（最大３校）の過年度合格者成績を、
	 * ＤＢから取得します。
	 * 
	 * @return String
	 * @throws SQLException
	 */
	private Collection execUniversityDatas() throws Exception {
		String FULLSIZESPACE = "　";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
		pstmt = conn.prepareStatement(getUnivSql());
		pstmt.setString(1, personId);
		pstmt.setString(2, personId);
		rs = pstmt.executeQuery();
		Map mappy = new LinkedHashMap();

		//模試データ追加
		if(getExaminationDatas().size() != 0)
			mappy.put(getTargetExamCode(), getExaminationDatas().iterator().next());
		
		while(rs.next()){
			IExamData examinationData = (IExamData) mappy.get(rs.getString("CANDIDATERANK"));
			if(examinationData == null){
				//マップの中にこの模試が存在しなければ新たに作成
				//同時に科目のコレクションの初期化			
				examinationData = new IExamData();
				if(getExaminationDatas() == null)
					setExaminationDatas(new ArrayList());
				getExaminationDatas().add(examinationData);
				mappy.put(rs.getString("CANDIDATERANK"), examinationData);	
			}
			
			examinationData.setExamCd(nullToEmpty(rs.getString("CANDIDATERANK")));
			examinationData.setExamName(nullToEmpty(rs.getString("UNIVNAME_ABBR")) + FULLSIZESPACE + nullToEmpty(rs.getString("FACULTYNAME_ABBR")) + FULLSIZESPACE + nullToEmpty(rs.getString("DEPTNAME_ABBR")));
			
			Collection subRecordDatas = examinationData.getSubRecordDatas();
			if(subRecordDatas == null){
				subRecordDatas = new ArrayList();
				examinationData.setSubRecordDatas(subRecordDatas);
			}
			
			SubRecordIData subRecordData = new SubRecordIData();
			subRecordData.setCDeviation(nullToEmptyF(rs.getString("SUCCESSAVGDEV")));
			subRecordData.setSubCd(rs.getString("CURSUBCD"));
			subRecordDatas.add(subRecordData);
		}
		if(pstmt != null) pstmt.close();
		if(rs != null) rs.close();
		
		return orderStraight(mappy.values());
		
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}
	
	/**
	 * 対象模試をＤＢから取得して、模試リストに追加します。
	 * 
	 * @throws Exception
	 */
	private void execDeviations() throws Exception{
		final String ELSUBCD = "1190"; //[1] add 英語＋リスニング
		
		//対象模試がマーク模試かどうか調べる。
		if(this.getExamTypecd().equals("01")){
			examflg = true;
		}
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		
		try{
			ArrayList examNameM200 = new ArrayList();
			ArrayList examNameJ200 = new ArrayList();
			ArrayList examNameE200 = new ArrayList();
			ArrayList examNameEL = new ArrayList(); //[1] add 英語＋リスニング
			
			pstmt1 = conn.prepareStatement(getCountSql());		
			pstmt1.setString(1, personId);
			rs1 = pstmt1.executeQuery();

			String subIndex = "";
			while(rs1.next()){
				//配点が200以上の数学・国語・英語を持つ模試リスト作成
				if(rs1.getString("SUBCD") != null){
					subIndex = rs1.getString("SUBCD").substring(0, 1);					
					if(subIndex.equals("2")){
						examNameM200.add(rs1.getString("EXAMNAME")); //数学
					}else{
						if(subIndex.equals("3")){
							examNameJ200.add(rs1.getString("EXAMNAME")); //国語
						}else{
							examNameE200.add(rs1.getString("EXAMNAME")); //英語
							//[1] add start
							if(rs1.getString("SUBCD").equals(ELSUBCD)){
								examNameEL.add(rs1.getString("EXAMNAME")); //英語＋リスニング
							}
							//[1] add end
						}
					}
				}
			}
			if(pstmt1 != null) pstmt1.close();
				if(rs1 != null) rs1.close();	
			

		//数学・国語・英語の表示順序1番目を取るカウント
		int mathCount = 0;
		int japanCount = 0;
		int engCount = 0;
		pstmt = conn.prepareStatement(getMoshiSql());
		pstmt.setString(1, personId);
		rs = pstmt.executeQuery();

		IExamData examinationData = null;	//対象模試
		Collection subRecordDatas = null;			//科目リスト
		while(rs.next()) {
			//対象模試がnullなら作る。
			if(examinationData == null) {examinationData  = new IExamData();}
			//科目リストがnullなら作る。
			if(subRecordDatas == null) {subRecordDatas = new ArrayList();}
			
			examinationData.setExamYear(nullToEmpty(rs.getString("EXAMYEAR")));		//模試年度
			examinationData.setExamCd(nullToEmpty(rs.getString("EXAMCD")));			//模試コード
			examinationData.setExamName(nullToEmpty(rs.getString("EXAMNAME")));		//模試名称
			examinationData.setExamTypeCd(nullToEmpty(rs.getString("EXAMTYPECD")));	//模試タイプ
			
			SubRecordIData subRecordData = new SubRecordIData();

			subIndex = "";
			if(rs.getString("SUBCD") != null){
				subIndex = rs.getString("SUBCD").substring(0, 1);
			}
			if(examNameE200.contains(nullToEmpty(rs.getString("EXAMNAME"))) && subIndex.equals("1") && engCount == 0){
				//[1] add start
				//模試が英語＋Ｌリストに存在する場合、英語＋Ｌに限定
				if(examNameEL.contains(rs.getString("EXAMNAME"))) {
					if(nullToEmpty(rs.getString("SUBCD")).equals(ELSUBCD)) {
						//英語＋Ｌ
						if(rs.getString("A_DEVIATION") != null){
							subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));
							choiceScholarLevel(subRecordData, avgChoice, 
									rs.getString("A_DEVIATION"), rs.getString("ACADEMICLEVEL"));
							subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
							subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
							subRecordDatas.add(subRecordData);
						}
						engCount++;
					}
				} else {
					if(nullToEmpty(rs.getString("SUBALLOTPNT")).equals("200")) {
						//英語得点200得点
						if(rs.getString("A_DEVIATION") != null){
							subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));
							choiceScholarLevel(subRecordData, avgChoice, 
									rs.getString("A_DEVIATION"), rs.getString("ACADEMICLEVEL"));
							subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
							subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
							subRecordDatas.add(subRecordData);
						}
						engCount++;
					}
				}
				
			}else if(!examNameE200.contains(rs.getString("EXAMNAME")) && subIndex.equals("1") && engCount == 0){
				//英語配点200ではない得点の表示順序1番目					
				subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));
				choiceScholarLevel(subRecordData, avgChoice, 
						rs.getString("A_DEVIATION"), rs.getString("ACADEMICLEVEL"));
				subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
				subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
				subRecordDatas.add(subRecordData);
				engCount ++;		
			}else if(examNameM200.contains(nullToEmpty(rs.getString("EXAMNAME"))) && nullToEmpty(rs.getString("SUBALLOTPNT")).equals("200") && subIndex.equals("2") && mathCount == 0){//[2] change
				//数学配点200の表示順序1番目
				subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));	//科目偏差値
				choiceScholarLevel(subRecordData, avgChoice, 
						rs.getString("A_DEVIATION"), rs.getString("ACADEMICLEVEL"));
				subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
				subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
				subRecordDatas.add(subRecordData);
				mathCount++;//[2] add
			}else if(!examNameM200.contains(nullToEmpty(rs.getString("EXAMNAME"))) && subIndex.equals("2") && mathCount == 0){
				//数学配点200ではない得点の表示順序1番目
				subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));	//科目偏差値
				choiceScholarLevel(subRecordData, avgChoice, 
						rs.getString("A_DEVIATION"), rs.getString("ACADEMICLEVEL"));
				subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
				subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
				subRecordDatas.add(subRecordData);
				mathCount ++;
			}else if(examNameJ200.contains(nullToEmpty(rs.getString("EXAMNAME"))) && nullToEmpty(rs.getString("SUBALLOTPNT")).equals("200") && subIndex.equals("3") && japanCount == 0){//[2] change
				//国語配点200得点
				subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));
				choiceScholarLevel(subRecordData, avgChoice, 
						rs.getString("A_DEVIATION"), rs.getString("ACADEMICLEVEL"));
				subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
				subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
				subRecordDatas.add(subRecordData);
				japanCount ++;//[2] add
			}else if(!examNameJ200.contains(rs.getString("EXAMNAME")) && subIndex.equals("3") && japanCount == 0){
				//国語配点200ではない得点の表示順序1番目					
				subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));
				choiceScholarLevel(subRecordData, avgChoice, 
						rs.getString("A_DEVIATION"), rs.getString("ACADEMICLEVEL"));
				subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
				subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
				subRecordDatas.add(subRecordData);
				japanCount ++;		
			}else if(subIndex.equals("4") || subIndex.equals("5") || subIndex.equals("6")){
				//理科・地歴公民
				subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));	//科目偏差値
				choiceScholarLevel(subRecordData, avgChoice, 
						rs.getString("A_DEVIATION"), rs.getString("ACADEMICLEVEL"));
				subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));				//科目コード
				subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
				subRecordDatas.add(subRecordData);
			}
		
	
		}
		if(pstmt != null) pstmt.close();
		if(rs != null) rs.close();
		
		if(examinationData != null && subRecordDatas != null) {
			//科目を設定
			examinationData.setSubRecordDatas(subRecordDatas);
			//メンバに良いほうの対象科目コードを設定
			
			//setTargetEngCd(examinationData.getMaxEngCode());//[4] delete
			//[4] add start
			final String ENG_UNIVDATA_SUBCD = "1000";
			if(!nullToEmpty(examinationData.getMaxEngCode()).equals("")) {
				setTargetEngCd(ENG_UNIVDATA_SUBCD);
				engDispFlg = true;
			}
			//[4] add end
			
			if(examflg && (nullToEmpty(examinationData.getMaxMathCode()).equals("2380") || nullToEmpty(examinationData.getMaxMathCode()).equals("2480") || nullToEmpty(examinationData.getMaxMathCode()).equals("2580"))){
				setTargetMathCd("2000");
				dispflg = true;
			}else{
				setTargetMathCd(examinationData.getMaxMathCode());
			}
			setTargetJapCd(examinationData.getMaxJapCode());
			setTargetSciCd(examinationData.getMaxSciCode());
			setTargetSocCd(examinationData.getMaxSocCode());
			setAns1stSciCd(examinationData.getAns1stSubjectCodeSci());
			setAns1stSocCd(examinationData.getAns1stSubjectCodeSoc());
		}
		
		//模試リストがnullだったらリストを作る。
		if(getExaminationDatas() == null) {
			setExaminationDatas(new ArrayList());
		}
		//リストに模試情報を追加
		if(examinationData != null) {
			getExaminationDatas().add(examinationData);
		}

		//
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
			if(pstmt1 != null) pstmt1.close();
			if(rs1 != null) rs1.close();
		}
	}
	
	private Collection orderStraight(Collection collection){

		int DEFAULT_MAX_SIZE = 4;
		int thisSize = 0;
		
		if(collection.size() < DEFAULT_MAX_SIZE) {
			thisSize = collection.size();
		} else{
			thisSize = DEFAULT_MAX_SIZE;
		}
		
		IExamData[] examinationDatas = new IExamData[thisSize];
		
		for (Iterator it=collection.iterator(); it.hasNext();) {
			IExamData examinationData = (IExamData)it.next();
			if(collection.size() > 0 && examinationData.getExamCd().equals(getTargetExamCode())){
				examinationDatas[0] = examinationData;
				setTargetSubCd(examinationData);//教科の最大偏差値の科目を設定
			}else if(collection.size() > 1 && examinationDatas[1] == null){
				examinationDatas[1] = examinationData;
				setTargetSubCd(examinationData);//教科の最大偏差値の科目を設定
			}else if(collection.size() > 2 && examinationDatas[2] == null){
				examinationDatas[2] = examinationData;
				setTargetSubCd(examinationData);//教科の最大偏差値の科目を設定
			}else if(collection.size() > 3 && examinationDatas[3] == null){
				examinationDatas[3] = examinationData;
				setTargetSubCd(examinationData);//教科の最大偏差値の科目を設定
			}else{
				//無視
			}
		}
			
		return Arrays.asList(examinationDatas);
	}
	
	/**
	 * 教科の最大偏差値の科目を設定
	 * @param exam 模試
	 */
	private void setTargetSubCd(IExamData exam) {
		
		//教科の最大偏差値の科目をそれぞれ設定
		//exam.setTargetEngSubCd(getTargetEngCd());//[4] delete
		final String ENG_UNIVDATA_SUBCD = "1000";//[4] add
		if(engDispFlg && !nullToEmpty(getTargetEngCd()).equals("")) {
			exam.setTargetEngSubCd(ENG_UNIVDATA_SUBCD);
		}
		//[4] add
		
		if(examflg && (nullToEmpty(getTargetMathCd()).equals("2380") || nullToEmpty(getTargetMathCd()).equals("2480") || nullToEmpty(getTargetMathCd()).equals("2580"))){
			exam.setTargetMathSubCd("2000");
			dispflg = true;
		}else{
			exam.setTargetMathSubCd(getTargetMathCd());
		}
	
		exam.setTargetJapSubCd(getTargetJapCd());
		exam.setTargetSciSubCd(getTargetSciCd());
		exam.setTargetSocSubCd(getTargetSocCd());
		exam.setTargetAns1stSciSubCd(getAns1stSciCd());
		exam.setTargetAns1stSocSubCd(getAns1stSocCd());
		
		exam.setTargetExamCd(getTargetExamCode());
	}
	
	/**
	 * StringをFloatのフォーマットで返します。
	 * @return
	 */
	public String nullToEmptyF(String str) {
		if(str == null) {
			return "";
		} else {
				return String.valueOf(new Float(str).floatValue());
		}
	}

	/**
	 * nullが来たら空白にする
	 */
	private String nullToEmpty(String str) {
		if(str == null) {
			return "";
		}
		if(str.equals("null")) {
			return "";
		}
		return str;
	}

	/**
	 * @return
	 */
	public String[] getDeviationRanges() {
		return deviationRanges;
	}

	/**
	 * @return
	 */
	public String getMoshiSql() {
		return moshiSql;
	}

	/**
	 * @return
	 */
	public String getPersonId() {
		return personId;
	}

	/**
	 * @return
	 */
	public String getSQL_BASE() {
		return SQL_BASE;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * 志望順位を返します
	 * @return
	 */
	public String[] getTargetCandidateRanks() {
		return targetCandidateRanks;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @param strings
	 */
	public void setDeviationRanges(String[] strings) {
		deviationRanges = strings;
	}

	/**
	 * @param string
	 */
	public void setMoshiSql(String string) {
		moshiSql = string;
	}

	/**
	 * @param string
	 */
	public void setPersonId(String string) {
		personId = string;
	}

	/**
	 * @param string
	 */
	public void setSQL_BASE(String string) {
		SQL_BASE = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param strings
	 */
	public void setTargetCandidateRanks(String[] strings) {
		targetCandidateRanks = strings;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * univSqlを返します。
	 * @return univSql
	 */
	public String getUnivSql() {
		return univSql;
	}

	/**
	 * univSqlを設定します。
	 * @param String
	 */
	public void setUnivSql(String univSql) {
		this.univSql = univSql;
	}

	/**
	 * @return
	 */
	public String getComboSql() {
		return comboSql;
	}

	/**
	 * @param string
	 */
	public void setComboSql(String string) {
		comboSql = string;
	}

	/**
	 * @return
	 */
	public boolean isFirstAccess() {
		return isFirstAccess;
	}

	/**
	 * @param b
	 */
	public void setFirstAccess(boolean b) {
		isFirstAccess = b;
	}

	/**
	 * @return
	 */
	public Collection getComboxDatas() {
		return comboxDatas;
	}

	/**
	 * @param collection
	 */
	public void setComboxDatas(Collection collection) {
		comboxDatas = collection;
	}

	/**
	 * 模試情報を取得します
	 * @return Collection
	 */
	public Collection getExaminationDatas() {
		return examinationDatas;
	}
	
	/**
	 * 模試情報を設定します
	 * @param Collection
	 */
	public void setExaminationDatas(Collection collection) {
		this.examinationDatas = collection;
	}

	/**
	 * @return
	 */
	public Collection getCourseDatas() {
		return courseDatas;
	}

	/**
	 * @param collection
	 */
	public void setCourseDatas(Collection collection) {
		courseDatas = collection;
	}

	/**
	 * @return
	 */
	public String getTargetEngCd() {
		return targetEngCd;
	}

	/**
	 * @return
	 */
	public String getTargetJapCd() {
		return targetJapCd;
	}

	/**
	 * @return
	 */
	public String getTargetMathCd() {
		return targetMathCd;
	}

	/**
	 * @return
	 */
	public String getTargetSciCd() {
		return targetSciCd;
	}

	/**
	 * @return
	 */
	public String getTargetSocCd() {
		return targetSocCd;
	}

	/**
	 * @return
	 */
	public String getAns1stSciCd() {
		return ans1stSciCd;
	}

	/**
	 * @return
	 */
	public String getAns1stSocCd() {
		return ans1stSocCd;
	}

	/**
	 * @param string
	 */
	public void setTargetEngCd(String string) {
		targetEngCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetJapCd(String string) {
		targetJapCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetMathCd(String string) {
		targetMathCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetSciCd(String string) {
		targetSciCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetSocCd(String string) {
		targetSocCd = string;
	}

	/**
	 * @param string
	 */
	public void setAns1stSciCd(String string) {
		ans1stSciCd = string;
	}

	/**
	 * @param string
	 */
	public void setAns1stSocCd(String string) {
		ans1stSocCd = string;
	}

	/**
	 * @return
	 */
	public String getCountSql() {
		return countSql;
	}

	/**
	 * @param string
	 */
	public void setCountSql(String string) {
		countSql = string;
	}

	/**
	 * @return
	 */
	public String getExamTypecd() {
		return examTypecd;
	}

	/**
	 * @param string
	 */
	public void setExamTypecd(String string) {
		examTypecd = string;
	}

	/**
	 * @return
	 */
	public boolean isExamflg() {
		return examflg;
	}

	/**
	 * @param b
	 */
	public void setExamflg(boolean b) {
		examflg = b;
	}

	/**
	 * @return
	 */
	public boolean isDispflg() {
		return dispflg;
	}

	/**
	 * @param b
	 */
	public void setDispflg(boolean b) {
		dispflg = b;
	}

	public void setAvgChoice(String s) {
		this.avgChoice = s;
	}
	
	/**
	 * 画面の選択状態（平均値／良い方）で、出力する学力レベルを選択する。
	 * @param hensa
	 * @param flg
	 * @param avg
	 * @param lvl
	 */
	private void choiceScholarLevel(SubRecordIData data,
			final String flg, final String avg, final String lvl) {
		
		if ("1".equals(flg)) {
			//平均値の場合、偏差値から学力レベルを出力
			data.setScholarlevel(IExamData.makeScholarLevel(avg));
		}
		else {
			//良い方の場合、SQLから取得した学力レベルを出力
			data.setScholarlevel(GeneralUtil.toBlank(lvl));
		}
		
	}

	/**
	 * 第1解答科目模試かどうか
	 * @return
	 */
	public boolean isAns1stExam() {
		return KNUtil.isAns1st(getTargetExamCode());
	}
}

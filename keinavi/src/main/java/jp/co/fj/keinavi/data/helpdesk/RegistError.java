/*
 * 作成日: 2004/09/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.helpdesk;

import java.io.Serializable;

/**
 * @author A.Ninomiya
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class RegistError implements Serializable  {

	String	line			=null;	// 行数
	String	schoolCode		=null;	// 学校コード
	String	userId			=null;	// ユーザID
	String	errorContents	=null;	// エラー内容


	//----------------------------------------------
	/**
	* 行数
	* @return
	*/
	public String getLine() {
		return line;
	}

	/**
	 * @param string
	 */
	public void setLine(String string) {
		line = string;
	}
	//----------------------------------------------
	/**
	* 学校コード
	* @return
	*/
	public String getSchoolCode() {
		return schoolCode;
	}

	/**
	 * @param string
	 */
	public void setSchoolCode(String string) {
		schoolCode = string;
	}
	//----------------------------------------------
	/**
	* ユーザID
	* @return
	*/
	public String getUserId() {
		return userId;
	}

	/**
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}
	//----------------------------------------------
	/**
	* エラー内容
	* @return
	*/
	public String getErrorContents() {
		return errorContents;
	}

	/**
	 * @param string
	 */
	public void setErrorContents(String string) {
		errorContents = string;
	}
	//----------------------------------------------

}

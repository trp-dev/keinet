/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.excel.data.school.S11Item;
import jp.co.fj.keinavi.excel.data.school.S11KataListBean;
import jp.co.fj.keinavi.excel.data.school.S11KmkListBean;
import jp.co.fj.keinavi.excel.school.S11;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 校内成績・成績概況
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * @author kawai
 */
public class S11SheetBean extends AbstractSheetBean {

	// データクラス
	private final S11Item data = new S11Item();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrKenmei("（" + super.getPrefName() + "）"); // 県名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntHyouFlg(super.getIntFlag(IProfileItem.CHART)); // 表フラグ
		data.setIntGraphFlg(super.getIntFlag(IProfileItem.GRAPH)); // グラフフラグ
		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ
			
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("s11").toString());
			ps.setString(1, exam.getExamYear()); // 模試年度
			ps.setString(2, exam.getExamCD()); // 模試コード
			ps.setString(3, profile.getBundleCD()); // 一括コード
			ps.setString(4, login.getPrefCD()); // 県コード

			rs = ps.executeQuery();
			while (rs.next()) {
				// 型
				if (rs.getInt(1) >= 7000) {
					S11KataListBean bean = new S11KataListBean();
					bean.setStrKmkCd(rs.getString(1));
					bean.setStrKmkmei(rs.getString(2));
					bean.setStrHaitenKmk(rs.getString(3));
					bean.setIntNinzuHome(rs.getInt(4));
					bean.setIntNinzuKen(rs.getInt(5));
					bean.setIntNinzuAll(rs.getInt(6));
					bean.setFloHeikinHome(rs.getFloat(7));
					bean.setFloHeikinKen(rs.getFloat(8));
					bean.setFloHeikinAll(rs.getFloat(9));
					bean.setFloHensaHome(rs.getFloat(10));
					bean.setFloHensaKen(rs.getFloat(11));
					bean.setFloHensaAll(rs.getFloat(12));
					data.getS11KataList().add(bean);
				// 科目
				} else {
					S11KmkListBean bean = new S11KmkListBean();
					bean.setStrKmkCd(rs.getString(1));
					bean.setStrKmkmei(rs.getString(2));
					bean.setStrHaitenKmk(rs.getString(3));
					bean.setIntNinzuHome(rs.getInt(4));
					bean.setIntNinzuKen(rs.getInt(5));
					bean.setIntNinzuAll(rs.getInt(6));
					bean.setFloHeikinHome(rs.getFloat(7));
					bean.setFloHeikinKen(rs.getFloat(8));
					bean.setFloHeikinAll(rs.getFloat(9));
					bean.setFloHensaHome(rs.getFloat(10));
					bean.setFloHensaKen(rs.getFloat(11));
					bean.setFloHensaAll(rs.getFloat(12));
					data.getS11KmkList().add(bean);
				}
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);	
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S11_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_SCHOOL_SCORE;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S11()
			.s11(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
	}

}

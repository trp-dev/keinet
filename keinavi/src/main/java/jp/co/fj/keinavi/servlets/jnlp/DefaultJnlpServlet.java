package jp.co.fj.keinavi.servlets.jnlp;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 * JNLPサーブレットの基本クラス
 *
 * 2008.02.08	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public abstract class DefaultJnlpServlet extends DefaultHttpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

	        forward(request, response, "/SheetConnection");
//		response.sendRedirect(response.encodeURL(
//				KNUtil.getContextPath(request) + "/JnlpServlet.jnlp" + createParameter(request)));
	}

	/**
	 * @param request HTTPリクエスト
	 * @return JNLPにアクセスするためのパラメータ
	 */
	protected abstract String createParameter(HttpServletRequest request);

	protected String getAllParameter(final HttpServletRequest request) {

		final StringBuffer buff = new StringBuffer();

		final Map map = request.getParameterMap();
		for (final Iterator ite = map.entrySet().iterator(); ite.hasNext();) {
			final Map.Entry entry = (Map.Entry) ite.next();
			final String[] value = (String[]) entry.getValue();

			if (buff.length() == 0) {
				buff.append("?");
			} else {
				buff.append("&");
			}

			buff.append(entry.getKey());
			buff.append("=");
			if (value.length > 0) {
				buff.append(value[0]);
			}
		}

		return buff.toString();
	}

}

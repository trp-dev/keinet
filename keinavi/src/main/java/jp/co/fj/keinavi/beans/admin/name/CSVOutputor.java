/*
 * 作成日: 2004/11/08
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.admin.name;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * CSVファイル（可変長）出力用クラス
 * 
 * @author kawai
 */
public class CSVOutputor {
	
	// ストリームオブジェクト
	private BufferedOutputStream out;
	// 行数
	private int count;
	// データ配列
	private List container = new LinkedList();
	// 数値フォーマット
	private DecimalFormat formator = new DecimalFormat("###.#");
	
	/**
	 * コンストラクタ
	 * @param filename
	 */
	public CSVOutputor(String filename) {
		try {
			out = new BufferedOutputStream(new FileOutputStream(new File(filename)));
		} catch (FileNotFoundException e) {
			throw new InternalError("出力ファイルエラーです。" + filename);
		}
	}

	/**
	 * 一行書き込む
	 * @param container
	 */
	public void write() {
		StringBuffer buff = new StringBuffer();

		boolean first = true;

		Iterator ite = this.container.iterator();
		while (ite.hasNext()) {
			String value = (String) ite.next();

			// １レコード目でなければカンマ追加
			if (first) first = false;
			else buff.append(","); 
			
			if (value != null) buff.append(value);
		}
		
		buff.append("\n");

		try {
			out.write(buff.toString().getBytes("MS932"));
			if (++count % 100 == 0) out.flush();
		} catch (Exception e) {
			throw new InternalError(e.getMessage());
		}
		
		this.container.clear();
	}

	/**
	 * ストリームを閉じる
	 */
	public void close() {
		 try {
			if (out != null) out.close();
		} catch (IOException e) {
			throw new InternalError(e.getMessage());
		}
	}

	/**
	 * データを追加する
	 * @param value
	 */
	public void add(String value) {
		this.container.add(value);
	}

	/**
	 * データを追加する
	 * @param value
	 */
	public void add(double value) {
		this.container.add(formator.format(value));
	}

}

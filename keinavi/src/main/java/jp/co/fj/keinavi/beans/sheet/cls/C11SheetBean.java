/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.cls;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.excel.cls.C11;
import jp.co.fj.keinavi.excel.data.cls.C11BnpListBean;
import jp.co.fj.keinavi.excel.data.cls.C11Item;
import jp.co.fj.keinavi.excel.data.cls.C11KmkDataListBean;
import jp.co.fj.keinavi.excel.data.cls.C11ListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * 2005.04.21	Yoshimoto KAWAI - Totec
 * 				新テスト対応
 * 
 * [3] 2005.08.25	Masami SHIMIZU - Totec
 * 				複合クラスの新テスト対応
 * 				（新テストの場合はクラスの偏差値データを無効値にする）
 * 
 * @author kawai
 *
 */
public class C11SheetBean extends AbstractSheetBean {

	// データクラス
	private final C11Item data = new C11Item();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		String[] code = super.getSubjectCDArray(); // 型・科目設定値
		List graph = super.getSubjectGraphList(); // 型・科目グラフ表示対象
		boolean isNewExam = KNUtil.isNewExam(exam); // 新テストかどうか [3]

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntHyouFlg(super.getIntFlag(IProfileItem.CHART)); // 表フラグ
		data.setIntGraphFlg(super.getIntFlag(IProfileItem.GRAPH_DIST_NO_AXIS)); // 度数分布グラフフラグ
		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ

		// 偏差値帯別人数積み上げグラフ
		switch (super.getIntFlag(IProfileItem.GRAPH_BUILDUP)) {
			case  1: data.setIntNinzuFlg(0); data.setIntPitchFlg(1); break; 
			case  2: data.setIntNinzuFlg(0); data.setIntPitchFlg(2); break; 
			case  3: data.setIntNinzuFlg(0); data.setIntPitchFlg(3); break; 
			case 11: data.setIntNinzuFlg(1); data.setIntPitchFlg(1); break; 
			case 12: data.setIntNinzuFlg(1); data.setIntPitchFlg(2); break;
			case 13: data.setIntNinzuFlg(1); data.setIntPitchFlg(3); break;
		}

		// ワークテーブルへデータを入れる
		super.insertIntoCountChargeClass();

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		try {
			// 担当クラス
			ps1 = conn.prepareStatement(QueryLoader.getInstance().load("c11_1").toString());

			// 型・科目別データリスト
			Query query = QueryLoader.getInstance().load("c11_2");
			query.setStringArray(1, code); // 型・科目コード

			ps2 = conn.prepareStatement(query.toString());
			ps2.setString(1, exam.getExamYear()); // 模試年度
			ps2.setString(2, exam.getExamCD()); // 模試コード
			ps2.setString(3, profile.getBundleCD()); // 一括コード
			ps2.setString(6, exam.getExamYear()); // 模試年度
			ps2.setString(7, exam.getExamCD()); // 模試コード
			ps2.setString(8, profile.getBundleCD()); // 一括コード
			ps2.setString(10, exam.getExamYear()); // 模試年度
			ps2.setString(11, exam.getExamCD()); // 模試コード
			ps2.setString(12, profile.getBundleCD()); // 一括コード
			ps2.setString(13, exam.getExamYear()); // 模試年度
			ps2.setString(14, exam.getExamCD()); // 模試コード

			// 偏差値分布データリスト
			//ps3 = conn.prepareStatement(QueryLoader.getInstance().load("c11_3").toString());
			ps3 = conn.prepareStatement(super.getQueryWithDeviationZone("c11_3").toString());
			ps3.setString(1, exam.getExamYear()); // 模試年度
			ps3.setString(2, exam.getExamCD()); // 模試コード
			ps3.setString(3, profile.getBundleCD()); // 一括コード
			ps3.setString(7, exam.getExamYear()); // 模試年度
			ps3.setString(8, exam.getExamCD()); // 模試コード
			ps3.setString(9, profile.getBundleCD()); // 一括コード
		
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				C11ListBean bean = new C11ListBean();
				bean.setStrGrade(rs1.getString(1));
				bean.setStrClass(rs1.getString(3));
				data.getC11List().add(bean);
				
				// 型・科目別データリスト
				List c1 = new LinkedList(); // 型の入れ物
				List c2 = new LinkedList(); // 科目の入れ物

				// 学年
				ps2.setInt(4, rs1.getInt(1));
				ps3.setInt(5, rs1.getInt(1));
				// クラス
				ps2.setString(5, rs1.getString(2));
				ps2.setString(9, rs1.getString(2));
				ps3.setString(6, rs1.getString(2));
				ps3.setString(11, rs1.getString(2));
				
				rs2 = ps2.executeQuery();
				while (rs2.next()) {
					C11KmkDataListBean k = new C11KmkDataListBean();
					k.setStrKmkCd(rs2.getString(1));
					k.setStrKmkmei(rs2.getString(2));
					k.setStrHaitenKmk(rs2.getString(3));
					k.setIntDispKmkFlg(graph.contains(rs2.getString(1)) ? 1 : 0);
					k.setIntNinzuCls(rs2.getInt(4));
					k.setFloHeikinHome(rs2.getFloat(5));
					// 新テストなら平均偏差値→無効値 [3]
					if (isNewExam) {
						k.setFloHensaCls(-999);
					}
					else {
						k.setFloHensaCls(rs2.getFloat(6));
					}
					k.setIntNinzuHome(rs2.getInt(7));
					k.setFloHeikinCls(rs2.getFloat(8));
					// 新テストなら平均偏差値→無効値 [3]
					if (isNewExam) {
						k.setFloHensaHome(-999);
					}
					else {
						k.setFloHensaHome(rs2.getFloat(9));
					}

					if (rs2.getInt(1) >= 7000) c1.add(k); // 型
					else c2.add(k); // 科目
	
					// 型・科目コード
					ps3.setString(4, rs2.getString(1));
					ps3.setString(10, rs2.getString(1));
					
					rs3 = ps3.executeQuery();
					while (rs3.next()) {
						C11BnpListBean b = new C11BnpListBean();
						b.setFloBnpMax(rs3.getFloat(1));
						b.setFloBnpMin(rs3.getFloat(2));
						b.setIntNinzu(rs3.getInt(3));
						k.getC11BnpList().add(b);
					}
					rs3.close();
				}
				rs2.close();				
				
				// 型・科目の順番に詰める
				bean.getC11KmkDataList().addAll(c1);
				bean.getC11KmkDataList().addAll(c2);
			}
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(rs3);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(ps3);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("C11_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.C_COND_DEV;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new C11()
			.c11(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
	}

}

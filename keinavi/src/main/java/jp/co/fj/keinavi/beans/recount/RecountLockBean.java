package jp.co.fj.keinavi.beans.recount;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.util.message.MessageLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 再集計処理に対するロックを取得するBean
 * 
 * 2006.02.15	[新規作成]
 * 
 * 2007.08.10	校内成績処理システムより移植
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public final class RecountLockBean {

	private RecountLockBean() {
	}

	/**
	 * @param con DBコネクション
	 * @param schoolCd 学校コード
	 * @return ロック成功かどうか
	 * @throws SQLException SQL例外
	 */
	public static boolean lock(final Connection con,
			final String schoolCd) throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// NOWAITオプションでロックする
			ps = con.prepareStatement(
					"SELECT re.schoolcd FROM recountqueue re "
					+ "WHERE re.schoolcd = ? "
					+ "FOR UPDATE NOWAIT");
			ps.setString(1, schoolCd);
			rs = ps.executeQuery();
			if (rs.next()) {
				rs.getString(1);
			}
		} catch (final SQLException e) {
			// 競合が発生
			if (e.getMessage().startsWith("ORA-00054")) {
				return false;
			} else {
				throw e;
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
		
		return true;
	}
	
	private static String getErrorMessage(final String id) {
		try {
			final String message = MessageLoader.getInstance().getMessage(id);
			if (message == null) {
				return "";
			} else {
				return message;
			}
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * @return エラーメッセージ
	 */
	public static String getErrorMessage() {
		return getErrorMessage("x016a");
	}
	
}

/*
 * 作成日: 2004/07/23
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */

package jp.co.fj.keinavi.data.individual;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class GradeComboData implements Serializable {

	private List gradeDatas;

	public List getGradeDatas() {
		return gradeDatas;
	}
	public void setGradeDatas(List list) {
		gradeDatas = list;
	}
	public void addGradeData(GradeData data){
		if(getGradeDatas() == null)
			setGradeDatas(new ArrayList());
		gradeDatas.add(data);
	}
	public GradeData get(String gradeString){
		GradeData data = null;
		if(getGradeDatas() != null){
			for (Iterator it=getGradeDatas().iterator(); it.hasNext();){
				GradeData gradeData = (GradeData)it.next();
				if(gradeData.getGrade().equals(gradeString))
					data = gradeData;
			}
		}
		return data;
	}
	public GradeData getNewGradeData(){
		return new GradeData();
	}
	public String[] getClassStrings(){
		String[] classStrings = new String[getGradeDatas().size()];
		int gradeCount = 0;
		for (Iterator it=getGradeDatas().iterator(); it.hasNext();){
			GradeData gradeData = (GradeData)it.next();
			classStrings[gradeCount] = "classString["+gradeCount+"] = new Array(";
			
			//----
			classStrings[gradeCount] += "new Array(\"all\", \"全担当クラス\"), "; 
			int classCount = 0;
			for (Iterator its=gradeData.getClassDatas().iterator(); its.hasNext();){
				GradeData.ClassData classData = (GradeData.ClassData)its.next();
				if(classCount != 0)
					classStrings[gradeCount] += ",";
				classStrings[gradeCount] += "new Array(\""+classData.getClasss()+"\", \""+classData.getClasss()+"\")";		
				classCount ++;
			}	
			classStrings[gradeCount] += ");";
			gradeCount ++;
		}
		return classStrings;
	}
	public String getGradeString(){
		String gradeString = "var gradeString = new Array(";
		if(getGradeDatas().size() < 1)
			return null;
		int count = 0;
		for (Iterator it=getGradeDatas().iterator(); it.hasNext();){
			GradeData gradeData = (GradeData)it.next();
			if(count != 0)
				gradeString += ",";
			gradeString += " new Array(\""+gradeData.getGrade()+"\", \""+gradeData.getGrade()+"\")";
			count ++;
		}
		gradeString += ");";
		return gradeString;
	}	
	public class GradeData {
		private String grade;
		private List classDatas;

		public List getClassDatas() {
			return classDatas;
		}
		public String getGrade() {
			return grade;
		}
		public void setClassDatas(List list) {
			classDatas = list;
		}
		public void setGrade(String string) {
			grade = string;
		}
		public void addClassData(String classStr){
			if(getClassDatas() == null)
				setClassDatas(new ArrayList());
			classDatas.add(new ClassData(classStr));
		}
		public class ClassData{
			private String classs;
			public ClassData(String string){
				classs = string;
			}
			public String getClasss(){
				return classs;
			}
			public void setClasss(String string){
				classs = string;
			}
		}

	}
}

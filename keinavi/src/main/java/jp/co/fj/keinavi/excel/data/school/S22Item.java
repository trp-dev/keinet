package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * 校内成績−過年度比較−偏差値分布 データクラス
 * 作成日: 2004/07/21
 * @author	A.Iwata
 */
public class S22Item {
	//学校名
	private String strGakkomei = "";
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//表フラグ
	private int intHyouFlg = 0;
	//構成比表示フラグ
	private int intKoseihiFlg = 0;
	//グラフフラグ
	private int intBnpGraphFlg = 0;
	//グラフ軸フラグ
	private int intAxisFlg = 0;
	//人数積み上げグラフフラグ
	private int intNinzuGraphFlg = 0;
	//人数積み上げグラフピッチフラグ
	private int intNinzuPitchFlg = 0;
	//構成比グラフフラグ
	private int intKoseiGraphFlg = 0;
	//構成比グラフピッチフラグ
	private int intKoseiPitchFlg = 0;

	// 2019/07/26 QQ)Tanioka 共通テスト対応 ADD START
	//共通テスト英語認定試験CEFR取得状況フラグ
	private int intCheckBoxFlg = 0;
	//自校受験者がいる試験を対象にするフラグ
	private int intTargetCheckBoxFlg = 0;
	// 2019/07/26 QQ)Tanioka 共通テスト対応 ADD END

	//セキュリティスタンプフラグ
	private int intSecuFlg = 0;
	//データリスト
	private ArrayList s22List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;
// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD START
	//認定試験別・合計CEFR取得状況データリスト
	private ArrayList s22PtCefrAcqStatusList = new ArrayList();
	//CEFR取得状況データリスト
	private ArrayList s22CefrAcqStatusList = new ArrayList();
// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD END


	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public int getIntHyouFlg() {
		return this.intHyouFlg;
	}
	public int getIntKoseihiFlg() {
		return this.intKoseihiFlg;
	}
	public int getIntBnpGraphFlg() {
		return this.intBnpGraphFlg;
	}
	public int getIntAxisFlg() {
		return this.intAxisFlg;
	}
	public int getIntNinzuGraphFlg() {
		return this.intNinzuGraphFlg;
	}
	public int getIntNinzuPitchFlg() {
		return this.intNinzuPitchFlg;
	}
	public int getIntKoseiGraphFlg() {
		return this.intKoseiGraphFlg;
	}
	public int getIntKoseiPitchFlg() {
		return this.intKoseiPitchFlg;
	}
	// 2019/07/26 QQ)Tanioka 共通テスト対応 ADD START
	public int getIntCheckBoxFlg() {
		return this.intCheckBoxFlg;
	}
	public int getIntTargetCheckBoxFlg() {
		return this.intTargetCheckBoxFlg;
	}
	// 2019/07/26 QQ)Tanioka 共通テスト対応 ADD END
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public ArrayList getS22List() {
		return this.s22List;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}
// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD START
	public ArrayList getS22PtCefrAcqStatusList() {
		return this.s22PtCefrAcqStatusList;
	}
	public ArrayList getS22CefrAcqStatusList() {
		return this.s22CefrAcqStatusList;
	}
// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD END

	/*---------------*/
	/* Set */
	/*---------------*/
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setIntHyouFlg(int intHyouFlg) {
		this.intHyouFlg = intHyouFlg;
	}
	public void setIntKoseihiFlg(int intKoseihiFlg) {
		this.intKoseihiFlg = intKoseihiFlg;
	}
	public void setIntBnpGraphFlg(int intBnpGraphFlg) {
		this.intBnpGraphFlg = intBnpGraphFlg;
	}
	public void setIntAxisFlg(int intAxisFlg) {
		this.intAxisFlg = intAxisFlg;
	}
	public void setIntNinzuGraphFlg(int intNinzuGraphFlg) {
		this.intNinzuGraphFlg = intNinzuGraphFlg;
	}
	public void setIntNinzuPitchFlg(int intNinzuPitchFlg) {
		this.intNinzuPitchFlg = intNinzuPitchFlg;
	}
	public void setIntKoseiGraphFlg(int intKoseiGraphFlg) {
		this.intKoseiGraphFlg = intKoseiGraphFlg;
	}
	public void setIntKoseiPitchFlg(int intKoseiPitchFlg) {
		this.intKoseiPitchFlg = intKoseiPitchFlg;
	}
	// 2019/07/26 QQ)Tanioka 共通テスト対応 ADD START
	public void setIntCheckBoxFlg(int intCheckBoxFlg) {
		this.intCheckBoxFlg = intCheckBoxFlg;
	}
	public void setIntTargetCheckBoxFlg(int intTargetCheckBoxFlg) {
		this.intTargetCheckBoxFlg = intTargetCheckBoxFlg;
	}
	// 2019/07/26 QQ)Tanioka 共通テスト対応 ADD END
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setS22List(ArrayList s22List) {
		this.s22List = s22List;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}
// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD START
	public void setS22PtCefrAcqStatusList(ArrayList s22PtCefrAcqStatusList) {
		this.s22PtCefrAcqStatusList = s22PtCefrAcqStatusList;
	}
	public void setS22CefrAcqStatusList(ArrayList s22CefrAcqStatusList) {
		this.s22CefrAcqStatusList = s22CefrAcqStatusList;
	}
// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD END

}

/**
 * 校内成績分析−クラス比較　設問別成績
 * 出力する帳票の判断
 * 作成日: 2004/08/09
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.school.S33Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

public class S33 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean s33( S33Item s33Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {

		KNLog log = KNLog.getInstance(null,null,null);

		try{
			int ret = 0;
			//S33Itemから各帳票を出力
			// 2019/09/04 QQ)Tanioka 帳票出力処理追加 UPD START
			//if (( s33Item.getIntHyouFlg()==0 ) && ( s33Item.getIntGraphFlg()==0 )){
			if (( s33Item.getIntHyouFlg()==0 ) && ( s33Item.getIntGraphFlg()==0 )&&
				( s33Item.getIntKokugoFlg()==0 ) && ( s33Item.getIntStatusFlg()==0 )){
			// 2019/09/04 QQ)Tanioka 帳票出力処理追加 UPD END
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
			    //throw new Exception("S33 ERROR : フラグ異常 ");
			    throw new IllegalStateException("S33 ERROR : フラグ異常");
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			}
			if ( s33Item.getIntHyouFlg()==1 ) {
				log.Ep("S33_01","S33_01帳票作成開始","");
				S33_01 exceledit = new S33_01();
				ret = exceledit.s33_01EditExcel( s33Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S33_01","帳票作成エラー","");
					return false;
				}
				log.Ep("S33_01","S33_01帳票作成終了","");
				sheetLog.add("S33_01");
			}
			if ( s33Item.getIntGraphFlg()==1 ) {
				log.Ep("S33_02","S33_02帳票作成開始","");
				S33_02 exceledit = new S33_02();
				ret = exceledit.s33_02EditExcel( s33Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S33_02","帳票作成エラー","");
					return false;
				}
				log.Ep("S33_02","S33_02帳票作成終了","");
				sheetLog.add("S33_02");
			}
			// 2019/09/05 QQ)Ooseto 共通テスト対応 ADD START
			if ( s33Item.getIntKokugoFlg()==1 ) {
			    log.Ep("S33_03","S33_03帳票作成開始","");
			    S33_03 exceledit = new S33_03();
			    ret = exceledit.s33_03EditExcel( s33Item, outfilelist, saveFlg, UserID );
			    if (ret!=0) {
			        log.Err("0"+ret+"S33_03","帳票作成エラー","");
			        return false;
			    }
			    log.Ep("S33_03","S33_03帳票作成終了","");
			    sheetLog.add("S33_03");
			}
			// 2019/09/05 QQ)Ooseto 共通テスト対応 ADD END
			// 2019/09/04 QQ)Tanioka 帳票出力処理追加 ADD START
			if ( s33Item.getIntStatusFlg()==1 ) {
				log.Ep("S33_04","S33_04帳票作成開始","");
				S33_04 exceledit = new S33_04();
				ret = exceledit.s33_04EditExcel( s33Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S33_04","帳票作成エラー","");
					return false;
				}
				log.Ep("S33_04","S33_04帳票作成終了","");
				sheetLog.add("S33_04");
			}
			// 2019/09/04 QQ)Tanioka 帳票出力処理追加 ADD END

		} catch(Exception e) {
			log.Err("99S33","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
package jp.co.fj.keinavi.servlets.maintenance;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.maintenance.DeclarationBean;
import jp.co.fj.keinavi.beans.maintenance.KNClassListBean;
import jp.co.fj.keinavi.beans.maintenance.SelectedStudentBean;
import jp.co.fj.keinavi.beans.maintenance.StudentListBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.maintenance.KNStudentExtData;
import jp.co.fj.keinavi.data.maintenance.TempExamineeData;
import jp.co.fj.keinavi.forms.maintenance.M101Form;
import jp.co.fj.keinavi.util.KNUtil;

import org.apache.commons.lang.StringUtils;

/**
 *
 * 生徒情報一覧画面サーブレット
 * 
 * 2005.11.24	[新規作成]
 * 2006.10.26	2006年度改修
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M101Servlet extends AbstractMServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// HTTPセッション
		final HttpSession session = request.getSession(false);
		// ログイン情報
		final LoginSession login = getLoginSession(request);
		// アクションフォーム
		final M101Form form = getActionForm(request);
		
		// JSPからの遷移
		if ("m101".equals(getBackward(request))) {
			//「一覧表示」を押しているなら生徒リスト初期化
			if ("3".equals(form.getActionMode())) {
				session.removeAttribute("StudentSearchBean");
			// ソート
			} else if ("5".equals(form.getActionMode())) {
				getStudentListBean(request).sort(
						getStudentListComparator(form.getSortKey()));
			//「＜」
			} else if ("6".equals(form.getActionMode())) {
				form.toLeftPage();
			//「＞」
			} else if ("7".equals(form.getActionMode())) {
				form.toRightPage();
			// 仮受験者クリア
			} else if ("8".equals(form.getActionMode())) {
				session.setAttribute("TempExamineeData",
						new TempExamineeData());
			}
		}
		
		// JSPへの遷移処理
		if ("m101".equals(getForward(request))) {
			// 個人成績が存在する模試リストのセットアップ
			if (getScoreExamListBean(request) == null) {
				session.setAttribute("ScoreExamListBean",
						createScoreExamListBean(request));
			}
			// 選択済み生徒情報の初期化
			if (getSelectedStudentBean(request) == null) {
				session.setAttribute("SelectedStudentBean",
						new SelectedStudentBean(login.getUserID()));
			}
			// クラスリストのセットアップ
			if (getClassListBean(request) == null) {
				session.setAttribute("ClassListBean",
						createClassListBean(request, false));
			}
			// フォームの初期化
			if (StringUtils.isEmpty(form.getTargetYear())) {
				form.init(getClassListBean(request).getFirstClassData());
				// セッションも初期化する
				final M101Form sform = (M101Form) session.getAttribute("M101Form");
				sform.setTargetYear(form.getTargetYear());
				sform.setTargetGrade(form.getTargetGrade());
				sform.setTargetClass(form.getTargetClass());
				sform.setSortKey(form.getSortKey());
			}
			// 生徒情報リストのセットアップ
			if (getStudentListBean(request) == null) {
				session.setAttribute("StudentSearchBean",
						createStudentListBean(request, form));
			}
			
			// 模試表示ページ設定
			getScoreExamListBean(request).setPageNo(form.getPageNo());
			// 作業中宣言のロード
			loadDeclaration(request);
			// 選択済み生徒を同期
			syncSelectedStudent(request);
			// 表示する生徒リストをセット
			request.setAttribute("StudentList", createDispStudentList(request));
			// 現年度をセット
			request.setAttribute("CurrentYear", KNUtil.getCurrentYear());
			// アクションフォームをセット
			request.setAttribute("form", form);
			
			forward2Jsp(request, response);
			
		// 不明なら転送
		} else {
			// 新規登録・編集・統合・非表示生徒確認なら作業中宣言を更新する
			if ("m102".equals(getForward(request))
					|| "m105".equals(getForward(request))
					|| "m107".equals(getForward(request))
					|| "m108".equals(getForward(request))) {
				updateDeclaration(request);
			}
			dispatch(request, response);
		}
	}

	// M101Formをセッションから取得する
	protected M101Form getM101Form(final HttpSession session) {
		if (session.getAttribute("M101Form") == null) {
			session.setAttribute("M101Form",  new M101Form());
		}
		return (M101Form) session.getAttribute("M101Form");
	}
	
	// アクションフォームを取得する
	private M101Form getActionForm(final HttpServletRequest request
			) throws ServletException {
		
		// アクションフォーム - request scope
		final M101Form rform = (M101Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.maintenance.M101Form");
		// アクションフォーム - session scope
		final HttpSession session = request.getSession(false);
		final M101Form sform;
		if (session.getAttribute("M101Form") == null) {
			sform = new M101Form();
			session.setAttribute("M101Form",  sform);
		} else {
			sform = (M101Form) session.getAttribute("M101Form");
		}

		// 年度・学年・クラス・絞り込み条件の保持は
		//「一覧表示」を押したタイミングで行う
		if ("m101".equals(getBackward(request))
				&& "3".equals(rform.getActionMode())) {
			sform.setTargetYear(rform.getTargetYear());
			sform.setTargetGrade(rform.getTargetGrade());
			sform.setTargetClass(rform.getTargetClass());
			sform.setConditionSelect(rform.getConditionSelect());
			sform.setConditionSubSelect(rform.getConditionSubSelect());
			sform.setConditionText(rform.getConditionText());
		// セッション値からの復元
		} else {
			rform.setTargetYear(sform.getTargetYear());
			rform.setTargetGrade(sform.getTargetGrade());
			rform.setTargetClass(sform.getTargetClass());
			rform.setConditionSelect(sform.getConditionSelect());
			rform.setConditionSubSelect(sform.getConditionSubSelect());
			rform.setConditionText(sform.getConditionText());
		}
		
		// 転送元がm101なら画面入力値をセッションに保持する
		if ("m101".equals(getBackward(request))) {
			sform.setDisplayStyle(rform.getDisplayStyle());
			sform.setSortKey(rform.getSortKey());
			// 主従生徒の保持
			setSelectedStudent(request, rform);
			// 仮受験者の保持
			getTempExamineeData(request).setTempExaminee(
					rform.getTempExaminee(),
					rform.getRegistedTempExaminee());
		// セッション値からの復元
		} else {
			rform.setDisplayStyle(sform.getDisplayStyle());
			rform.setSortKey(sform.getSortKey());
		}
		
		return rform;
	}

	// 選択済み生徒を保持する
	private void setSelectedStudent(final HttpServletRequest request,
			final M101Form form) throws ServletException {
		
		final SelectedStudentBean bean = getSelectedStudentBean(request);
		bean.setMainIndividualId(form.getTargetIndividualId());
		bean.setSubIndividualId(form.getTargetSubIndividualId());
	}
	
	// 表示する生徒リストを作る
	private List createDispStudentList(final HttpServletRequest request) {
		
		final List list = new ArrayList();
		final StudentListBean bean = getStudentListBean(request);
		final Map map = getSelectedStudentBean(request).getSelectedStudentMap();
		
		// 主生徒
		final KNStudentExtData mainStudent = getMainStudentData(request);
		if (mainStudent != null) {
			list.add(mainStudent);
		}
		
		// 従生徒
		final KNStudentExtData subStudent = getSubStudentData(request);
		if (subStudent != null) {
			list.add(subStudent);
		}
		
		for (final Iterator ite = bean.getDispStudentList(
				).iterator(); ite.hasNext();) {
			final KNStudentExtData data = (KNStudentExtData) ite.next();
			if (!map.containsKey(data.getIndividualId())) {
				data.setSelectedFlag(3);
				list.add(data);
			}
		}
		
		return list;
	}

	/**
	 * @param request HTTPリクエスト
	 * @param notDisplayMode 非表示モードか
	 * @return クラスリストBean
	 * @throws ServletException SQL例外
	 */
	protected KNClassListBean createClassListBean(
			final HttpServletRequest request,
			final boolean isNotDisplayMode) throws ServletException {
		
		final KNClassListBean bean = new KNClassListBean(
					getSchoolCd(request));
		if (isNotDisplayMode) {
			bean.setNotDisplayMode();
		}
		execute(request, bean);
		return bean;
	}

	// 生徒情報リストを作る
	private StudentListBean createStudentListBean(
			final HttpServletRequest request, final M101Form form)
			throws ServletException {

		final StudentListBean bean = new StudentListBean(
					getSchoolCd(request));
		
		// パラメータセット
		bean.setYear(form.getTargetYear());
		bean.setGrade(form.getTargetGrade());
		bean.setClassName(form.getTargetClass());
		bean.setCondition(form.getConditionSelect(),
				form.getConditionSubSelect(),
				form.getConditionText());
		
		// 実行
		execute(request, bean);
		
		// 表示生徒リスト初期化
		bean.initDispStudentList(getScoreExamListBean(request),
				getTempExamineeData(request),
				getStudentListComparator(form.getSortKey()));
		
		return bean;
	}

	/**
	 * 作業中宣言をロードする
	 * （注意）校内成績処理システムで呼ばれるメソッド
	 * 
	 * @param con DBコネクション
	 * @param session セッション
	 * @param schoolCd 学校コード
	 */
	public void loadDeclaration(final Connection con, final HttpSession session,
			final String schoolCd) throws Exception {
		final DeclarationBean bean = new DeclarationBean(schoolCd);
		bean.setConnection(null, con);
		bean.execute();
		session.setAttribute(DeclarationBean.SESSION_KEY, bean);
	}
	
	/**
	 * 作業中宣言を上書きする
	 * （注意）校内成績処理システムで呼ばれるメソッド
	 * 
	 * @param request HTTPリクエスト
	 * @throws ServletException SQLエラー
	 */
	public void updateDeclaration(
			final HttpServletRequest request) throws ServletException {
		super.updateDeclaration(request);
	}

}

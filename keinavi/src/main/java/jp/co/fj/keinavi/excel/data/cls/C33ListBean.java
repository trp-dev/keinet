package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * 偏差値分布（クラス比較）データクラス
 * 作成日: 2004/08/17
 * @author	Ito.Y
 */
public class C33ListBean {
	//型・科目名コード
	private String strKmkCd = "";
	//型・科目名
	private String strKmkmei = "";
	//型･科目用グラフ表示フラグ
	private int intDispKmkFlg = 0;
	//配点
	private String strHaitenKmk = "";
	//設問番号
	private String strSetsuNo = "";
	//設問内容
	private String strSetsuMei1 = "";
	//設問配点
	private String strSetsuHaiten = "";
	//クラスデータリスト
	private ArrayList c33ClassList = new ArrayList();
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}
	public int getIntDispKmkFlg() {
		return this.intDispKmkFlg;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public String getStrSetsuNo() {
		return this.strSetsuNo;
	}
	public String getStrSetsuMei1() {
		return this.strSetsuMei1;
	}
	public String getStrSetsuHaiten() {
		return this.strSetsuHaiten;
	}
	public ArrayList getC33ClassList() {
		return this.c33ClassList;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setIntDispKmkFlg(int intDispKmkFlg) {
		this.intDispKmkFlg = intDispKmkFlg;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setStrSetsuNo(String strSetsuNo) {
		this.strSetsuNo = strSetsuNo;
	}
	public void setStrSetsuMei1(String strSetsuMei1) {
		this.strSetsuMei1 = strSetsuMei1;
	}
	public void setStrSetsuHaiten(String strSetsuHaiten) {
		this.strSetsuHaiten = strSetsuHaiten;
	}
	public void setC33ClassList(ArrayList c33ClassList) {
		this.c33ClassList = c33ClassList;
	}
}

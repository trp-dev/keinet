package jp.co.fj.keinavi.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.auth.MoshiSysManager;
import jp.co.fj.keinavi.beans.NewestExamDivSearchBean;
import jp.co.fj.keinavi.beans.security.MenuSecurityBean;
import jp.co.fj.keinavi.beans.security.config.SecurityConfig;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.UnivMasterData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.interfaces.MessageInterface;
import jp.co.fj.keinavi.interfaces.PageInterface;
import jp.co.fj.keinavi.util.AssetLoginUtil;
import jp.co.fj.keinavi.util.BundleCdFactory;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.log.KNAccessLog;
import jp.co.fj.keinavi.util.log.KNLog;

import com.fjh.beans.DefaultBean;
import com.fjh.db.DBManager;
import com.fjh.forms.ActionForm;
import com.fjh.servlets.DefaultServlet;

public abstract class DefaultHttpServlet extends DefaultServlet implements PageInterface, MessageInterface {

    private static final long serialVersionUID = -8989000104796799596L;

    protected static final String CONTENT_TYPE = "text/html; charset=Shift_JIS";
    protected static final String CHAR_SET = "MS932";
    protected static final String GET_BYTES = "iso-8859-1";

    /**
     * 大学インスタンスリストを返します。
     *
     * @return 大学インスタンスリスト
     */
    protected ArrayList getUnivAll() {
        UnivMasterData data = getUnivMasterData();
        return data == null ? null : data.getUnivAll();
    }

    /**
     * 情報誌用科目データマップを返します。
     *
     * @return 情報誌用科目データマップ
     */
    protected final Map getUnivInfoMap() {
        UnivMasterData data = getUnivMasterData();
        return data == null ? null : data.getUnivInfoMap();
    }

    /**
     * 大学インスタンスマップを返します。
     *
     * @return 大学インスタンスマップ
     */
    protected final Map getUnivMap() {
        UnivMasterData data = getUnivMasterData();
        return data == null ? null : data.getUnivMap();
    }

    /**
     * メモリからロードした大学マスタ情報を返します。
     *
     * @return {@link UnivMasterData}
     */
    private UnivMasterData getUnivMasterData() {
        return (UnivMasterData) getServletContext().getAttribute(UnivMasterData.class.getName());
    }

    /**
     * {@inheritDoc}
     */
    public void init(final ServletConfig config) throws ServletException {
        super.init(config);
        DefaultServlet.CONTENT_TYPE = CONTENT_TYPE;
        DefaultServlet.CHAR_SET = CHAR_SET;
        DefaultServlet.GET_BYTES = GET_BYTES;
    }

    /**
     * {@inheritDoc}
     */
    public void destroy() {
        super.destroy();
        try {
            DBManager.destroy();
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 実行処理を行う
     *
     * @param request HTTPリクエスト
     * @param response HTTPレスポンス
     * @throws ServletException
     * @throws IOException
     */
    protected abstract void execute(
            final HttpServletRequest request,
            final HttpServletResponse response
            ) throws ServletException, IOException;

    /**
     * {@inheritDoc}
     */
    protected void doGet(final HttpServletRequest request,
            final HttpServletResponse response
            ) throws ServletException, IOException {

        try {
            // IE互換表示用ヘッダを出力する
            response.addHeader("X-UA-Compatible", "IE=EmulateIE7");
            // アクセスログ
            accessLog(request);
            // ログインチェック
            if (isLogin(request)) {
                // 権限チェック
                if (hasAuthority(request)) {
                    /* 資産認証クッキーを発行する */
                    publishAssetCookie(request, response);
                    execute(request, response);
                // 失敗なら例外を投げる
                } else {
                    throw createAuthCheckException(request);
                }
            // そうでなければタイムアウト例外
            } else {
                throw createTimeoutException();
            }

        // 例外発生
        } catch (final Throwable e) {
            LoginSession login = this.getLoginSession(request); // ログイン情報
            if (login == null) login = new LoginSession();

            // Kei-Navi例外
            if (e instanceof KNServletException) {
                KNServletException k = (KNServletException) e;

                KNLog.Err(
                    getRemoteAddr(request),
                    login.getUserID(),
                    login.getSectorSortingCD(),
                    login.getAccount(),
                    k.getErrorCode(),
                    k.getMessage(),
                    e);

                request.setAttribute("KNServletException", k);

            // それ以外のエラー
            } else {
                KNLog.Err(
                    getRemoteAddr(request),
                    login.getUserID(),
                    login.getSectorSortingCD(),
                    login.getAccount(),
                    "999999999999",
                    e.getMessage(),
                    e);
            }

            // エラー画面へ遷移する
            forward(request, response, JSP_ERROR);
        }
    }

    /**
     * アクセスログ（Lv1ログ）を出力する
     *
     * @param request HTTPリクエスト
     */
    protected void accessLog(final HttpServletRequest request) {

        // ログ出力済みならここまで
        if (request.getAttribute("AccessLogFlag") != null) {
            return;
        }

        KNAccessLog.lv1(getRemoteAddr(request),
                request.getRemoteHost(),
                getLoginSession(request),
                createScreenId(request), "1");

        // ログ出力済みフラグを立てる
        request.setAttribute("AccessLogFlag", Boolean.TRUE);
    }

    /**
     * アクションログ（Lv2ログ）を出力する
     *
     * @param request HTTPリクエスト
     * @param code 実行条件コード
     * @param info 補足情報
     */
    protected void actionLog(final HttpServletRequest request,
            final String code, final String info) {

        KNAccessLog.lv2(getRemoteAddr(request),
                request.getRemoteHost(),
                getLoginSession(request),
                createScreenId(request), code, info);
    }

    /**
     * アクションログ（Lv2ログ）を出力する
     *
     * @param request HTTPリクエスト
     * @param code 実行条件コード
     */
    protected void actionLog(final HttpServletRequest request,
            final String code) {
        actionLog(request, code, null);
    }

    // 画面IDを作る
    private String createScreenId(final HttpServletRequest request) {
        // 画面IDを作る
        // 「転送元:転送先」
        final String backward = getBackward(request);
        final String forward = getForward(request);
        final StringBuffer buff = new StringBuffer();
        if (backward != null) {
            buff.append(backward);
        }
        buff.append(":");
        if (forward != null) {
            buff.append(forward);
        }
        return buff.toString();
    }

    /**
     * {@inheritDoc}
     */
    protected void doPost(final HttpServletRequest request,
            final HttpServletResponse response
            ) throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * @param e 例外
     * @return ServletException
     */
    protected ServletException createServletException(final Throwable e) {
        final ServletException s = new ServletException(e.getMessage());
        s.initCause(e);
        return s;
    }

    /**
     * ログインセッションが存在するかどうか
     * @param request
     * @return
     */
    protected boolean isLogin(HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession(false);

        if (session == null) {
            return false;
        } else {
            return session.getAttribute(LoginSession.SESSION_KEY) != null;
        }
    }

    /**
     * 画面を表示する権限があるかどうか
     *
     * @param request Httpリクエスト
     * @return 権限があるかどうか
     * @throws ServletException アクションフォームの作成に失敗したら発生する
     */
    protected boolean hasAuthority(final HttpServletRequest request) throws ServletException {

        // ユーザの利用可能機能設定
        final MenuSecurityBean bean = getMenuSecurityBean(request);

        // 利用可能機能設定が作られていなければここまで
        if (bean == null) {
            return true;
        }

        // チェック済みフラグ
        if (request.getAttribute("hasAuthority") == null) {
            request.setAttribute("hasAuthority", new Object());

        // 既にチェック済みならここまで
        } else {
            return true;
        }

        // セキュリティ設定
        final SecurityConfig config = getSecurityConfig();

        // 転送先画面IDに対応する機能IDを取得
        final String fid = (String) config.getScreenSecurityMap().get(getForward(request));

        //System.out.println("forward="+getForward(request)+"/fid="+fid);

        // 機能IDが設定されていなければ権限ありとする
        if (fid == null) {
            return true;

        // そうでなければ機能権限を持っているか
        } else {
            return 	bean.isValid(fid);
        }
    }

    /**
     * 指定された画面IDのJSPへフォワードする
     *
     * @param request HTTPリクエスト
     * @param response HTTPレスポンス
     * @param id JSPのID(画面ID)
     * @throws IOException 転送失敗時に発生する？
     * @throws ServletException 転送失敗時に発生する？
     */
    protected void forward2Jsp(final HttpServletRequest request,
            final HttpServletResponse response, final String id)
            throws ServletException, IOException {

        final Map jsp = (Map) getServletContext().getAttribute("JspPathMap");

        // 設定がなければエラー
        if (!jsp.containsKey(id)) {
            throw new InternalError("JSPのパス設定がありません。" + id);
        }

        super.forward(request, response, (String) jsp.get(id));
    }

    /**
     * 転送先に指定されているJSPへフォワードする
     *
     * @param request HTTPリクエスト
     * @param response HTTPレスポンス
     * @throws IOException 転送失敗時に発生する？
     * @throws ServletException 転送失敗時に発生する？
     */
    protected void forward2Jsp(final HttpServletRequest request,
            final HttpServletResponse response)
            throws ServletException, IOException {

        this.forward2Jsp(request, response, getForward(request));
    }

    /**
     * 処理を転送する
     *
     * @param request HTTPリクエスト
     * @param response HTTPレスポンス
     * @throws IOException 転送失敗時に発生する？
     * @throws ServletException 転送失敗時に発生する？
     */
    protected void dispatch(final HttpServletRequest request,
            final HttpServletResponse response)
            throws ServletException, IOException {

        super.forward(request, response, "/DispatcherServlet");
    }

    /**
     * タイムアウト発生時の例外を生成する
     * @return
     */
    protected KNServletException createTimeoutException() {
        KNServletException e =
            new KNServletException("30分以上操作されなかったためタイムアウトしました。");
        e.setErrorCode("1");
        e.setClose(true);
        return e;
    }

    /**
     * 権限チェック失敗時の例外を生成する
     *
     * @return
     */
    protected KNServletException createAuthCheckException(final HttpServletRequest request) {

        KNServletException e =
            new KNServletException("利用権限がありません。画面ID=" + getForward(request));
        e.setErrorMessage("入力された学校IDはKei-Navi（有料）をご契約されていません。\\n「Kei-Naviダウンロードサービス」にアクセスしてください。");
        e.setErrorCode("1");
        return e;
    }

    /**
     * コネクションプールからコネクションを取得する
     * @param request HttpServletRequest
     * @return
     * @throws SQLException
     */
    protected Connection getConnectionPool(HttpServletRequest request) throws SQLException {
        Connection con = null; // コネクションオブジェクト
        try {
            con = DBManager.getConnectionPool(this.getDbKey(request));
        } catch (Exception e) {
            throw new SQLException(e.toString());
        }
        return con;
    }

    /**
     * コネクションを解放する
     * @param request HttpServletRequest
     * @param con コネクションオブジェクト
     */
    protected void releaseConnectionPool(HttpServletRequest request, Connection con) {
        if (con == null) return; // NULLなら中止
        try {
            DBManager.releaseConnectionPool(this.getDbKey(request), con);
        } catch (Exception e) {
            throw new InternalError(e.getMessage());
        }
    }

    /**
     * ロールバックを実行する
     * @param con
     */
    protected void rollback(Connection con) {
        if (con == null) return;
        try {
            con.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * DBのキーを取得する
     * @param request
     * @return
     */
    //private String getDbKey(HttpServletRequest request) {//T.Yamada changed
    protected String getDbKey(HttpServletRequest request) {
        LoginSession login = this.getLoginSession(request); // ログイン情報

        // デフォルトは通常DB
        if (login == null || login.getDbKey() == null) {
            return getNDBSID();
        }

        return login.getDbKey();
    }

    /**
     * 通常DBのキーを取得する
     * @return
     */
    public static String getNDBSID() {
        String value = null;
        try {
            value = KNCommonProperty.getNDBSID();
        } catch (Exception e) {
            throw new InternalError("システムプロパティの取得に失敗しました。: NDBSID");
        }
        return value;
    }

    /**
     * {@inheritDoc}
     */
    public ActionForm getActionForm(HttpServletRequest request, String className) throws ServletException {
        try {
            return super.getActionForm(request, className);
        } catch (final Exception e) {
            final ServletException s = new ServletException(e.getMessage());
            s.initCause(e);
            throw s;
        }
    }

    /**
     * エラーメッセージをセットする
     * @param request
     * @param message
     */
    protected void setErrorMessage(HttpServletRequest request, KNServletException e) {
        LoginSession login = this.getLoginSession(request); // ログイン情報
        if (login == null) login = new LoginSession();

        // ロギング
        KNLog.Err(
            getRemoteAddr(request),
            login.getUserID(),
            login.getSectorSortingCD(),
            login.getAccount(),
            e.getErrorCode(),
            e.getMessage(),
            e.getErrorMessage());

        request.setAttribute("ErrorMessage", e.getErrorMessage());
    }

    /**
     * エラーメッセージを取得する
     * @param request
     * @return
     */
    protected String getErrorMessage(HttpServletRequest request) {
        return (String) request.getAttribute("ErrorMessage");
    }

    /**
     * 転送先の画面IDを取得する
     * @return
     */
    protected String getForward(HttpServletRequest request) {
        return (String) request.getParameter("forward");
    }

    /**
     * 転送元の画面IDを取得する
     * @return
     */
    protected String getBackward(HttpServletRequest request) {
        return (String) request.getParameter("backward");
    }

    /**
     * セッションからプロファイルを取得する
     * @return
     */
    protected Profile getProfile(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) return null;
        return (Profile) session.getAttribute(Profile.SESSION_KEY);
    }

    /**
     * セッションからログインセッションを取得する
     * @return
     */
    protected LoginSession getLoginSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) return null;
        return (LoginSession) session.getAttribute(LoginSession.SESSION_KEY);
    }

    /**
     * セッションから模試セッションを取得する
     * @return
     */
    protected ExamSession getExamSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) return null;
        return (ExamSession) session.getAttribute(ExamSession.SESSION_KEY);
    }

    /**
     * セッションからユーザ利用機能設定を取得する
     *
     * @param request
     * @return
     */
    protected MenuSecurityBean getMenuSecurityBean(final HttpServletRequest request) {

        HttpSession session = request.getSession(false);
        if (session == null) return null;
        return (MenuSecurityBean) session.getAttribute(MenuSecurityBean.SESSION_KEY);
    }

    /**
     * セキュリティ設定を取得する
     *
     * @return
     */
    protected SecurityConfig getSecurityConfig() {

        return (SecurityConfig) getServletContext().getAttribute("SecurityConfig");
    }

    /**
     * 利用者の権限設定をセッションに設定する
     *
     * @param request
     */
    protected void setupMenuSecurity(final HttpServletRequest request) {

        // メニューセキュリティ
        request.getSession(false).setAttribute(
            MenuSecurityBean.SESSION_KEY,
            new MenuSecurityBean(getLoginSession(request), getSecurityConfig())
        );
    }

    /**
     * @return 最新の大学マスタ情報
     * @throws ServletException 情報が存在しない場合
     */
    protected NewestExamDivSearchBean getNewestExamDivSearchBean(
            ) throws ServletException {

        final NewestExamDivSearchBean bean = (NewestExamDivSearchBean
                ) getServletContext().getAttribute("NewestExamDivSearchBean");

        if (bean == null) {
            throw new ServletException("最新の大学マスタ情報がありません。");
        } else {
            return bean;
        }
    }

    /**
     * @param request HTTPリクエスト
     * @param examCd 模試コード
     * @return 集計テーブルの絞込みに使う一括コード
     */
    protected String getBundleCd(final LoginSession login,
            final Profile profile, final String examCd) {
        return BundleCdFactory.getBundleCd(login, profile, examCd);
    }

    /**
     * @param profile プロファイル
     * @param es 模試セッション
     * @return デフォルトの対象模試
     */
    protected ExamData getDefaultExamData(
            final Profile profile, final ExamSession es) {

        // 保存されている同系統の最新模試
        final ExamData exam = getSameTypeExam(profile, es.getExamList());

        // 見つからないなら最新の模試
        if (exam == null) {
            return KNUtil.getLatestExamData(es);
        } else {
            return exam;
        }
    }

    /**
     * @param profile プロファイル
     * @param examList 模試リスト
     * @return プロファイルに保存されている同系統の最新模試
     */
    protected ExamData getSameTypeExam(final Profile profile,
            final List examList) {

        // 模試コードが一致する模試を探す
        final ExamData base = findExamData(profile, examList);

        // 一致するものがなければここまで
        if (base == null) {
            return null;
        }

        // 新しい入れ物
        final List container = new ArrayList(examList);
        // データ開放日の降順でソート
        Collections.sort(container);

        // 模試種類が同じものを探す
        for (final Iterator ite = container.iterator(); ite.hasNext();) {
            final ExamData exam = (ExamData) ite.next();
            if (exam.getExamTypeCD().equals(base.getExamTypeCD())) {
                return exam;
            }
        }

        // 見つかりませんでした
        return null;
    }

    /**
     * @param profile プロファイル
     * @param examList 模試リスト
     * @return 同じ模試コードの模試データ
     */
    protected ExamData findExamData(final Profile profile,
            final List examList) {

        // 模試コード
        final String examCd = (String) profile.getItemMap(
                IProfileCategory.CM).get(IProfileItem.EXAM_CD);

        // 設定がなければここまで
        if (examCd == null) {
            return null;
        }

        // 模試コードが一致する模試を探す
        for (final Iterator ite = examList.iterator(); ite.hasNext();) {
            final ExamData exam = (ExamData) ite.next();
            if (examCd.equals(exam.getExamCD())) {
                return exam;
            }
        }

        // 見つかりませんでした
        return null;
    }

    /**
     * 現在の対象模試情報をプロファイルに保持する
     *
     * @param profile プロファイル
     * @param exam 対象模試
     */
    protected void saveExamType(final Profile profile, final ExamData exam) {
        profile.getItemMap(IProfileCategory.CM).put(
                IProfileItem.EXAM_YEAR, exam.getExamYear());
        profile.getItemMap(IProfileCategory.CM).put(
                IProfileItem.EXAM_CD, exam.getExamCD());
    }

    /**
     * （汎用メソッド）指定したBeanを実行する
     *
     * @param request HTTPリクエスト
     * @param bean 実行するBean
     * @throws ServletException 実行時例外
     */
    protected void execute(final HttpServletRequest request,
            final DefaultBean bean) throws ServletException {

        Connection con = null;
        try {
            con = getConnectionPool(request);
            bean.setConnection(null, con);
            bean.execute();
        } catch (final Exception e) {
            throw createServletException(e);
        } finally {
            releaseConnectionPool(request, con);
        }
    }

    /**
     * @param request HTTPリクエスト
     * @return ログイン中の学校コード
     */
    protected String getSchoolCd(final HttpServletRequest request) {
        return getLoginSession(request).getUserID();
    }

    /**
     * 模試受付システム用情報作成
     * @param request
     */
    protected void createMoshiSys(final HttpServletRequest request) throws ServletException{

        try {
            //模試受付システムURL
            request.setAttribute("moshiURL",KNCommonProperty.getMoshiURL());
            //POST情報作成
            MoshiSysManager ms = new MoshiSysManager();
            LoginSession login = getLoginSession(request);
            ms.create(login);

            //模試受付システム暗号キー
            request.setAttribute("sciv", ms.getSciv());
            //模試受付システム学校会員ID
            request.setAttribute("scid",ms.getScid());
            //模試受付システム学校名
            request.setAttribute("scnm",ms.getScnm());
        } catch (final Exception e) {
            throw createServletException(e);
        }

    }

    /**
     *「過年度の表示」の非表示フラグセット
     * HideUnivYear1 ... 1年前を非表示にする
     * HideUnivYear2 ... 2年前を非表示にする
     *               ...
     * HideUnivYear6 ... 6年前を非表示にする
     *
     * @param request
     * @param exam
     */
    protected void setHideUnivYear(final HttpServletRequest request,
            final ExamData exam) {

        // 2008年度以降の高2記述模試
        if (exam != null && "65".equals(exam.getExamCD())
                && "2008".compareTo(exam.getExamYear()) < 1) {
            int start = Integer.parseInt(exam.getExamYear()) - 2008 + 1;
            for (int i = start; i <= 6; i++) {
                request.setAttribute("HideUnivYear" + i, Boolean.TRUE);
            }
        }
    }

    /**
     * 資産認証クッキーを発行します。
     *
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @throws Exception 例外
     */
    private void publishAssetCookie(HttpServletRequest request, HttpServletResponse response) throws Exception{
        /* 未ログインかゲストログインなら発行しない */
        LoginSession login = getLoginSession(request);
        if (login != null && !login.isTrial()) {
            AssetLoginUtil.publishCookie(request, response);
        }
    }

    /**
     * クライアントIPアドレスを返します。
     *
     * @param request {@link HttpServletRequest}
     * @return クライアントIPアドレス
     * @see KNUtil#getRemoteAddr(HttpServletRequest)
     */
    protected String getRemoteAddr(HttpServletRequest request) {
        return KNUtil.getRemoteAddr(request);
    }

}

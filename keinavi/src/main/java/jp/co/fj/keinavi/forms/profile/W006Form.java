/*
 * 作成日: 2004/06/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.profile;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class W006Form extends W005Form {

	// プロファイル名称
	private String profileName = null;
	// コメント
	private String comment = null;
	// カレントフォルダ
	private String current = null;

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * @return
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @return
	 */
	public String getCurrent() {
		return current;
	}

	/**
	 * @return
	 */
	public String getProfileName() {
		return profileName;
	}

	/**
	 * @param string
	 */
	public void setComment(String string) {
		comment = string;
	}

	/**
	 * @param string
	 */
	public void setCurrent(String string) {
		current = string;
	}

	/**
	 * @param string
	 */
	public void setProfileName(String string) {
		profileName = string;
	}

}

/*
 * 作成日: 2004/07/16
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.com_set.factory;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.com_set.ComSubjectData;
import jp.co.fj.keinavi.data.com_set.SubjectData;
import jp.co.fj.keinavi.forms.AbstractActionFormFactory;
import jp.co.fj.keinavi.forms.com_set.AbstractSubjectForm;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public abstract class AbstractCMFormFactory extends AbstractActionFormFactory {

	/**
	 * アイテムマップを取得する
	 * @return
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap(IProfileCategory.CM);
	}
	
	/**
	 * 模試データを取得する
	 * @return
	 */
	protected ExamData getExamData(HttpServletRequest request) {
		CM001Form form = this.getCommonForm(request);
		
		return super.getExamSession(request)
			.getExamData(form.getTargetYear(), form.getTargetExam());
	}
	
	/**
	 * 共通アクションフォームを取得する
	 * @return
	 */
	protected CM001Form getCommonForm(HttpServletRequest request) {
		return (CM001Form) request.getSession(false).getAttribute(CM001Form.SESSION_KEY);
	}
	
	/**
	 * 型・科目のアクションフォームを初期化する
	 * @param form
	 * @param data
	 */
	protected void initSubjectForm(AbstractSubjectForm form, ComSubjectData data) {
		if (data == null) return;

		form.setAnalyze1(ProfileUtil.getSubjectAllValue(data, false));	
		form.setAnalyze2(ProfileUtil.getSubjectIndValue(data, false));
		form.setGraph1(ProfileUtil.getSubjectAllValue(data, true));
		form.setGraph2(ProfileUtil.getSubjectIndValue(data, true));	
	}

	/**
	 * 型・科目設定を反映する
	 * @param form
	 * @param data
	 */
	protected void reflectSubjectForm(AbstractSubjectForm form, ComSubjectData data) {	
	    // 設定値を初期化（全選択）
	    if (data.getASubjectData() == null) {
			data.setASubjectData(new ArrayList());
		} else {
		    data.getASubjectData().clear();
		}

	    // 設定値を初期化（個別選択）
		if (data.getISubjectData() == null) {
			data.setISubjectData(new ArrayList());
		} else {
		    data.getISubjectData().clear();
		}

		// 全選択
		if (form.getAnalyze1() != null) {
			
			// グラフ表示セット
			Set graph = CollectionUtil.array2Set(form.getGraph1());
			// 分析対象
			String[] analyze = form.getAnalyze1();			
			
			for (int i=0; i<analyze.length; i++) {
				SubjectData s = new SubjectData();

				s.setSubjectCD(analyze[i]);

				if (graph.contains(analyze[i])) s.setGraphDisp((short) 1);
				else s.setGraphDisp((short) 0);

				data.getASubjectData().add(s);
			}
		}

		// 個別選択
		if (form.getAnalyze2() != null) {
			
			// グラフ表示セット
			Set graph = CollectionUtil.array2Set(form.getGraph2());
			// 分析対象
			String[] analyze = form.getAnalyze2();			

			for (int i=0; i<analyze.length; i++) {
				SubjectData s = new SubjectData();

				s.setSubjectCD(analyze[i]);

				if (graph.contains(analyze[i])) s.setGraphDisp((short) 1);
				else s.setGraphDisp((short) 0);

				data.getISubjectData().add(s);
			}
		}
	}
}

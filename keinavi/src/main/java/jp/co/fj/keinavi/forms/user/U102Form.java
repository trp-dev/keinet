package jp.co.fj.keinavi.forms.user;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 *
 * 利用者パスワード変更画面アクションフォーム
 * 
 * 2005.10.13	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U102Form extends BaseForm {
	
	// 動作モード
	private String actionMode;
	// 旧パスワード
	private String prePassword;
	// 新パスワード
	private String newPassword;
	
	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	/**
	 * @return actionMode を戻します。
	 */
	public String getActionMode() {
		return actionMode;
	}

	/**
	 * @param actionMode 設定する actionMode。
	 */
	public void setActionMode(String actionMode) {
		this.actionMode = actionMode;
	}

	/**
	 * @return newPassword を戻します。
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword 設定する newPassword。
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * @return prePassword を戻します。
	 */
	public String getPrePassword() {
		return prePassword;
	}

	/**
	 * @param prePassword 設定する prePassword。
	 */
	public void setPrePassword(String prePassword) {
		this.prePassword = prePassword;
	}


}

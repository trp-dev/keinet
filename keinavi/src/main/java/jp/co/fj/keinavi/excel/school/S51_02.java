/**
 * 校内成績分析−過回比較　成績概況グラフ
 * 	Excelファイル編集
 * 作成日: 2004/08/18
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S51Item;
import jp.co.fj.keinavi.excel.data.school.S51KakaiListBean;
import jp.co.fj.keinavi.excel.data.school.S51ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S51_02 {

	private int noerror	= 0;		// 正常終了
	private int errfread	= 1;		// ファイルreadエラー
	private int errfwrite	= 2;		// ファイルwriteエラー
	private int errfdata	= 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	final private String	masterfile0	= "S51_02";		// ファイル名
	final private String	masterfile1	= "NS51_02";	// ファイル名
	private String	masterfile	= "";					// ファイル名
//	final private String	strArea		= "A1:AG58";		// 印刷範囲	
	final private int[]	tabCol		= {0,17};			// 表の基準点
	final private int[]	tabRow		= {46,49,52,55};	// 表の基準点

/*
 * 	Excel編集メイン
 * 		S51Item s51Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s51_02EditExcel(S51Item s51Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
 		
		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s51Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S51ListBean s51ListBean = new S51ListBean();

		try {

			int		intMaxSheetIndex	= 1;	// シートカウンター
			int		hyouCnt				= 0;	// 値範囲：０〜１　(表カウンター)
			int		kmkCnt				= 0;	// 値範囲：０〜３　(型・科目カウンター)
			int		mshCnt				= 0;	// 値範囲：０〜６　(模試カウンター)
			int		maxMsh				= 0;	// 過回模試数
			float		hensa				= 0;	// 今年度偏差値保持
			boolean	kataFlg				= true;// false:改行済(型・科目名変更済)　
			boolean	bolSheetCngFlg		= true;// 改シートフラグ
//add 2004/10/27 T.Sakai データ0件対応
			int		dispKmkFlgCnt		= 0;	// グラフ表示フラグカウンタ
//add end
			
			//マスタExcel読み込み
			workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
			if( workbook==null ){
				return errfread;
			}
			
			// データセット
			ArrayList	s51List	= s51Item.getS51List();
			Iterator	itr		= s51List.iterator();
			
			while( itr.hasNext() ) {
				s51ListBean = (S51ListBean)itr.next();
				
				if ( s51ListBean.getIntDispKmkFlg()==1 ) {
					//型から科目に変わる時のチェック
					if ( s51ListBean.getStrKmkCd().compareTo("7000") < 0 ) {
						//型・科目切替えの初回のみ処理する
						if (kataFlg){
							kmkCnt = 0;
							if(hyouCnt==1){
								hyouCnt	= 0;
								kataFlg	= false;
								bolSheetCngFlg	= true;
							} else if(hyouCnt==0){
								//2005.03.24 Add 型のデータが１件もないときに、空のグラフが表示されてしまう不具合対応
								// データ表示件数が０件でないときは、表カウントに加算する
								if (dispKmkFlgCnt > 0)
								{
									//この表カウントで、シート上の表示開始位置(左側なら0、右側なら1)が決まる
									hyouCnt	=1;
								}
								kataFlg	= false;
							}
						}
					}
					if ( kmkCnt==4 ) {
						kmkCnt = 0;
						hyouCnt++;
						if ( hyouCnt==2 ) {
							hyouCnt = 0;
							bolSheetCngFlg	= true;
						}
					}
					if ( bolSheetCngFlg ) {
						// データセットするシートの選択
						workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex));
						intMaxSheetIndex++;
						
						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);
	
						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, s51Item.getIntSecuFlg() ,30 ,32 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
						workCell.setCellValue(secuFlg);
	
						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
						workCell.setCellValue( "学校名　：" + cm.toString(s51Item.getStrGakkomei()) );
	
						// 対象模試セット
						String moshi =cm.setTaisyouMoshi( s51Item.getStrMshDate() );	// 模試月取得
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s51Item.getStrMshmei()) + moshi);
	
						//　改表フラグをFalseにする
						bolSheetCngFlg	=false;
					}
					
					// 型名セット
					workCell = cm.setCell( workSheet, workRow, workCell, tabRow[kmkCnt], tabCol[hyouCnt] );
					workCell.setCellValue( s51ListBean.getStrKmkmei() );

					// 配点セット
					if ( !cm.toString(s51ListBean.getStrHaitenKmk()).equals("") ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[kmkCnt]+2, tabCol[hyouCnt] );
						workCell.setCellValue("(" + s51ListBean.getStrHaitenKmk() + ")" );
					}
					
					// 基本ファイルを読込む
					S51KakaiListBean s51KakaiListBean = new S51KakaiListBean();
					
					// 過回データセット
					ArrayList s51KakaiList = s51ListBean.getS51KakaiList();
					Iterator itrKakai = s51KakaiList.iterator();
					
					maxMsh = 0;
					mshCnt = 0;
					
					//過回データ件数取得
					while ( itrKakai.hasNext() ){
						s51KakaiListBean = (S51KakaiListBean)itrKakai.next();
						maxMsh++;
					}
					itrKakai = s51KakaiList.iterator();
					
					while ( itrKakai.hasNext() ){
						s51KakaiListBean = (S51KakaiListBean)itrKakai.next();
						
						if(kmkCnt == 0){
							// 模試名セット
							String moshi =cm.setTaisyouMoshi( s51KakaiListBean.getStrMshDate() );	// 模試月取得
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[kmkCnt]-1, tabCol[hyouCnt]+(maxMsh-mshCnt)*2 );
							workCell.setCellValue( cm.toString(s51KakaiListBean.getStrMshmei()) + moshi );
						}
						
						if( mshCnt == 0 ){
							//最新模試の偏差値保持
							hensa = s51KakaiListBean.getFloHensa();
						}
						
						// 人数セット
						if ( s51KakaiListBean.getIntNinzu() != -999 ) {
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[kmkCnt], tabCol[hyouCnt]+1+(maxMsh-mshCnt)*2 );
							workCell.setCellValue( s51KakaiListBean.getIntNinzu() );
						}
	
						// 平均点セット
						if ( s51KakaiListBean.getFloHeikin() != -999.0 ) {
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[kmkCnt]+1, tabCol[hyouCnt]+1+(maxMsh-mshCnt)*2 );
							workCell.setCellValue( s51KakaiListBean.getFloHeikin() );
						}
	
						// 平均偏差値セット
						if ( s51KakaiListBean.getFloHensa() != -999.0 ) {
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[kmkCnt]+2, tabCol[hyouCnt]+1+(maxMsh-mshCnt)*2 );
							workCell.setCellValue( s51KakaiListBean.getFloHensa() );
						}
	
						// *セット
						if (s51KakaiListBean.getFloHensa() != -999.0) {
							if ( hensa < s51KakaiListBean.getFloHensa() ) {
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[kmkCnt]+2, tabCol[hyouCnt]+(maxMsh-mshCnt)*2 );
								workCell.setCellValue("*");
							}
						}
						mshCnt++;
					}
					kmkCnt++;
//add 2004/10/27 T.Sakai データ0件対応
					dispKmkFlgCnt++;
//add end
				}
			}
//add 2004/10/27 T.Sakai データ0件対応
			if ( dispKmkFlgCnt==0 ) {
				// データセットするシートの選択
				workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex));
				intMaxSheetIndex++;
						
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
	
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s51Item.getIntSecuFlg() ,30 ,32 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
				workCell.setCellValue(secuFlg);
	
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s51Item.getStrGakkomei()) );
	
				// 対象模試セット
				String moshi =cm.setTaisyouMoshi( s51Item.getStrMshDate() );	// 模試月取得
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s51Item.getStrMshmei()) + moshi);
			}
//add end
			// Excelファイル保存
			boolean bolRet = false;
			bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex-1);

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("S51_02","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

}
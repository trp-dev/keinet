package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import jp.co.fj.keinavi.data.individual.NestedPlanUnivData;
import jp.co.fj.keinavi.data.individual.PlanUnivData;
import jp.co.fj.keinavi.data.individual.ScheduleDetailData;
import jp.co.fj.keinavi.util.date.ExShortDatePlusUtil;
import jp.co.fj.keinavi.util.individual.InpleDateDivComparator;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultSearchBean;

/**
 *
 * 受験予定大学取得
 *
 * 2004.08.03	Tomohisa YAMADA - TOTEC
 * 				[新規作成]
 *
 * 2005.02.23	Tomohisa YAMADA - TOTEC
 * 				[1]受験した最新の模試が存在しない場合に、詳細画面に大学情報のみ表示する
 *
 * 2005.03.08	Tomohisa YAMADA - TOTEC
 * 				[2]最新模試取得の時に「担当クラスの年度よりも若い」制限を加える
 *
 * 2005.04.07 	Tomohisa YAMADA - TOTEC
 * 				[3]ScoreBeanの[35]に伴う修正
 *
 * 2005.08.02 	Tomohisa YAMADA - TOTEC
 * 				[4]マーク模試を受けていないときの、帳票エラー修正
 *
 * <2010年度改修>
 * 2010.01.20	Tomohisa YAMADA - Totec
 * 				入試日程変更対応
 *
 * 2016.01.15   Hiroyuki Nishiyama - QuiQsoft
 *              大規模改修：ソート順変更
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 *
 */
abstract public class PlanUnivSearchBean extends DefaultSearchBean {
	
	private static final long serialVersionUID = -3332266297523015456L;
	
	/**
	 * 個人ID
	 */
	protected String individualCd;

	/**
	 * 入試形態コード
	 */
	protected String entExamModeCd;

	/**
	 * 備考
	 */
	private String remarks;

	//初回フラグ
	//※新規・編集・複製（I304）の場合のみFALSEに成りうる
	//＝受験一覧表示時は必ずTRUEとなる
	protected boolean isFirst = true;

	// NestedPlanUnivList
	protected List univs;

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	abstract public void execute() throws SQLException, Exception;

	/**
	 * 受験予定大学一覧を取得
	 * @return
	 * @throws Exception
	 */
	public List getNestedPlanUnivList() throws Exception {
			return univs;
	}

	private static final SimpleDateFormat formatter = new SimpleDateFormat("M/d");

	/**
	 * 入試日程詳細情報を設定
	 * @param univData
	 * @throws Exception
	 */
	private void setupNonPlannedUniv(NestedPlanUnivData univData) throws Exception {

		//入試区分３・２・１の順にソート
		Collections.sort(univData.getExamInpleDateList());

		for (Iterator it = univData.getExamInpleDateList().iterator(); it.hasNext();) {

			final NestedPlanUnivData.ExamInpleDateData inple =
				(NestedPlanUnivData.ExamInpleDateData) it.next();

			//入試日程詳細データから一意の日付を配列で取り出す
			final Date[] uniqueDates = inple.getEntExamInpleUniqueDate();

			//個々の日付別にデータを作成する
			if (uniqueDates == null) {
				continue;
			}

			for (int i = 0;i < uniqueDates.length;	i++) {

				//入試日程詳細インスタンスを作成
				NestedPlanUnivData.InpleDateDivData divData = univData.new InpleDateDivData(
						univData.getYear(),
						univData.getExamDiv(),
						univData.getUnivCd(),
						univData.getFacultyCd(),
						univData.getDeptCd(),
						univData.getEntExamTypeCd(),
						univData.getUnivDivCd(),
						inple.getBranchName(),
						inple.getEntExamDiv_1_2order(),
						inple.getSchoolProventDiv(),
						inple.getEntExamDiv_1_2term());

				divData.setEntExamInpleUniqueDate(formatter.format(uniqueDates[i]));
				divData.setProvEntExamSite("");
				divData.setUnited(inple.isUnited());

				univData.getInpleDateDivDataList().add(divData);
			}
		}
	}

	/**
	 * 受験予定大学情報を設定
	 * @param univData
	 * @throws ParseException
	 */
	private void setupPlannedUniv(NestedPlanUnivData univData) throws ParseException {

		ExShortDatePlusUtil util = new ExShortDatePlusUtil();

		for (Iterator it = univData.getExamPlanDateList().iterator(); it.hasNext();) {

			final NestedPlanUnivData.ExamPlanDateData plan =
				(NestedPlanUnivData.ExamPlanDateData) it.next();

			// 受験予定大学インスタンスの作成
			for (int i = 0;i < plan.getExamPlanDates().length;i++) {

				NestedPlanUnivData.InpleDateDivData divData = univData.new InpleDateDivData(
						univData.getYear(),
						univData.getExamDiv(),
						univData.getUnivCd(),
						univData.getFacultyCd(),
						univData.getDeptCd(),
						univData.getEntExamTypeCd(),
						univData.getUnivDivCd(),
						plan.getBranchName(),
						plan.getEntExamDiv_1_2order(),
						plan.getSchoolProventDiv(),
						plan.getEntExamDiv_1_2term());

				divData.setEntExamInpleUniqueDate(util.getFormattedString(plan.getExamPlanDates()[i]));
				divData.setProvEntExamSite(plan.getProvEntExamSite()[i]);

				univData.getPlanDateDivDataList().add(divData);
			}
		}
	}

	/**
	 * 表示用にデータを変換
	 * @param ulist
	 * @return
	 * @throws Exception
	 */
	protected List setupList(List ulist) throws Exception {

		final Map mappy = new HashMap();

		Collections.sort(ulist);

		/**************************************************************
		 * PlanUnivDataのデータリストを
		 * NestedPlanUnivDataのリストに変換する
		 **************************************************************/
		for (final Iterator ite = ulist.iterator(); ite.hasNext();) {

			PlanUnivData data = (PlanUnivData) ite.next();

			NestedPlanUnivData univData =
				(NestedPlanUnivData) mappy.get(data.getDataKey());

			//未設定の場合は新規に作成してマップにつめる
			if (univData == null) {

				univData = new NestedPlanUnivData();

				univData.setUnivCd(data.getUnivCd());
				univData.setUnivNameAbbr(data.getUnivNameAbbr());
				univData.setFacultyCd(data.getFacultyCd());
				univData.setFacultyNameAbbr(data.getFacultyNameAbbr());
				univData.setDeptCd(data.getDeptCd());
				univData.setDeptNameAbbr(data.getDeptNameAbbr());
				univData.setDeptSortKey(data.getDeptSortKey());
				univData.setEntExamTypeCd(data.getEntExamTypeCd());
				univData.setEntExamOdeName(data.getEntExamOdeName());
				univData.setExamInpleDateList(new ArrayList());
				univData.setExamPlanDateList(new ArrayList());
				univData.setUnited(data.isUnited());
				univData.setYear(data.getYear());
				univData.setExamDiv(data.getExamDiv());
				univData.setRemarks(data.getRemarks());//備考
				univData.setUnivDivCd(data.getUnivDivCd());
				univData.setScheduleCd(data.getScheduleCd());
				univData.setPrefCdHq(data.getPrefCdHq());

				// 2016/01/15 QQ)Nishiyama 大規模改修 ADD START
				univData.setKanaNum            (data.getKanaNum()            );  // カナ付50音順番号
				univData.setUniNightDiv        (data.getUniNightDiv()        );  // 大学夜間部区分
				univData.setFacultyConCd       (data.getFacultyConCd()       );  // 学部内容コード
				univData.setDeptSerialNo       (data.getDeptSerialNo()       );  // 学科通番
				univData.setScheduleSys        (data.getScheduleSys()        );  // 日程方式番
				univData.setScheduleSysBranchCd(data.getScheduleSysBranchCd());  // 日程方式枝
				univData.setDeptNameKana       (data.getDeptNameKana()       );  // 学科カナ名
				// 2016/01/15 QQ)Nishiyama 大規模改修 ADD END

				mappy.put(data.getDataKey(), univData);
			}

			final ScheduleDetailData schedule = data.getSchedule();

			// データが受験予定大学の場合
			if (data.isRegistered()) {

				NestedPlanUnivData.ExamPlanDateData plandata =
					univData.new ExamPlanDateData(
							schedule.getEntExamDiv_1_2order(),
							schedule.getSchoolProventDiv(),
							schedule.getEntExamDiv_1_2term());

				plandata.setProvEntExamSite(data.getProvEntExamSite());
				plandata.setExamPlanDates(data.getExamPlanDates());


				//受験予定大学情報をリストに追加
				univData.getExamPlanDateList().add(plandata);

				//受験予定大学の場合のみガイドラインを設定
				univData.setGuideLine(data.getGuideLine());
			}
			// データ入試日程詳細の場合
			else {

				NestedPlanUnivData.ExamInpleDateData inpleData =
					univData.new ExamInpleDateData(
							schedule.getEntExamInpleDate1_1(),
							schedule.getEntExamInpleDate1_2(),
							schedule.getEntExamInpleDate1_3(),
							schedule.getEntExamInpleDate1_4(),
							schedule.getPleDateDiv1_1(),
							schedule.getPleDateDiv1_2(),
							schedule.getPleDateDiv1_3(),
							schedule.getEntExamInpleDate2_1(),
							schedule.getEntExamInpleDate2_2(),
							schedule.getEntExamInpleDate2_3(),
							schedule.getEntExamInpleDate2_4(),
							schedule.getPleDateDiv2_1(),
							schedule.getPleDateDiv2_2(),
							schedule.getPleDateDiv2_3(),
							schedule.getPleDateUniteDiv(),
							data.getBranchName(),
							schedule.getEntExamDiv_1_2order(),
							schedule.getSchoolProventDiv(),
							schedule.getEntExamDiv_1_2term());

				inpleData.setRemarks(data.getRemarks());

				//入試日程詳細データをリストに追加
				univData.getExamInpleDateList().add(inpleData);
			}
		}

		/**************************************************************
		 * 上記で変換されたNestedPlanUnivDataのリストを再度ループして
		 * 必要なデータを作成・設定し直す
		 **************************************************************/
		Collection list = (Collection) mappy.values();

		for (Iterator it3 = list.iterator(); it3.hasNext();) {

			NestedPlanUnivData univData = (NestedPlanUnivData) it3.next();

			//入試日程詳細
			if (univData.getExamInpleDateList() != null) {
				setupNonPlannedUniv(univData);
			}

			//受験予定大学
			if (univData.getExamPlanDateList() != null) {
				setupPlannedUniv(univData);
			}

			//inpleDataが既に予定大学に登録されているかのチェック
			//登録されていれば、日程詳細の方にチェックをつけて、
			//予定の方を非表示
			if (univData.getInpleDateDivDataList() != null) {
				squeezeData(univData);
			}

			univData.setDivDataMap(setupDivData(univData));
		}


		List nestedPlanUnivList = new ArrayList();

		nestedPlanUnivList.addAll(list);

		//次の順でソートする
		//1. 大学 2. 学部 3. 学科ソートキー 4. 入試形態
		// 2016/01/15 大規模改修によりソート順変更
		Collections.sort(nestedPlanUnivList);

		return nestedPlanUnivList;
	}

	/**
	 * 入試日程詳細情報と受験予定日の重複を解消する
	 * @param univData
	 */
	private void squeezeData(NestedPlanUnivData univData) {

		//入試日程詳細データ
		for (Iterator it = univData.getInpleDateDivDataList().iterator(); it.hasNext();) {

			NestedPlanUnivData.InpleDateDivData inple =
				(NestedPlanUnivData.InpleDateDivData) it.next();

			//受験予定大学データ
			if (univData.getPlanDateDivDataList() != null) {

				for (Iterator it2 =	univData.getPlanDateDivDataList().iterator(); it2.hasNext();) {

					NestedPlanUnivData.InpleDateDivData plan =
						(NestedPlanUnivData.InpleDateDivData) it2.next();

					//受験予定大学と入試日程詳細が同じ区分で同じ日の場合は
					//1. 入試日程詳細情報には登録済みフラグを立てる
					//2. 受験予定大学の受験地を入試日程詳細情報に設定する
					//DateDivKeyは以下の情報を順に結合したもの：
					//getUnivCd() getFacultyCd() getDeptCd() getEntExamTypeCd()
					//getEntExamDiv1() getEntExamDiv2() getEntExamDiv3()
					//getEntExamInpleUniqueDate() getYear() getExamDiv();
					if (!inple.getDateDivKey().equals(plan.getDateDivKey())) {
						continue;
					}

					inple.setRegistered(true);
					inple.setProvEntExamSite(plan.getProvEntExamSite());

					//受験予定大学でないことを設定
					inple.setInplePlan(true);

					//基準が入試日程詳細なので
					//受験予定大学の方を除去
					it2.remove();
				}
			}
		}
	}

	/**
	 * 入試区分単位にデータを整える
	 * @param univData
	 * @return
	 */
	private Map setupDivData(NestedPlanUnivData univData) {

		/** 入試区分１・２・３で一意に区別してマップに詰める(区分単位で表示が可能) */
		SortedMap map = new TreeMap();

		//入試日程詳細
		if (univData.getInpleDateDivDataList() != null) {
			for (Iterator it = univData.getInpleDateDivDataList().iterator(); it.hasNext();) {

				NestedPlanUnivData.InpleDateDivData data =
					(NestedPlanUnivData.InpleDateDivData) it.next();

				//div3,div2,div1の順番
				List divList = (List) map.get(data.getEntExamDivKey());

				//まだこの区分でリストが存在しなければ
				if (divList == null) {

					divList = new ArrayList();

					map.put(data.getEntExamDivKey(), divList);
				}

				data.setInple(true);
				data.setPlan(false);

				divList.add(data);
			}
		}

		//受験予定大学
		if (univData.getPlanDateDivDataList() != null) {
			for (Iterator it = univData.getPlanDateDivDataList().iterator(); it.hasNext();) {

				NestedPlanUnivData.InpleDateDivData data =
					(NestedPlanUnivData.InpleDateDivData) it.next();

				//無条件で受験予定大学に登録されているデータはチェックをつける(仕様変更) 10/12

				data.setRegistered(true);

				//div3,div2,div1の順番
				List divList = (List) map.get(data.getEntExamDivKey());

				if (divList == null) {

					divList = new ArrayList();

					map.put(data.getEntExamDivKey(), divList);
				}

				data.setPlan(true);
				data.setInple(false);

				divList.add(data);
			}
		}

		/** 日付順位ソートをかける */
		if (map != null) {
			Set s = map.entrySet();
			if (s != null) {
				Iterator itete = s.iterator();
				if (itete != null) {
					while (itete.hasNext()) {
						Map.Entry entry = (Map.Entry) itete.next();
						List value = (List) entry.getValue();
						Collections.sort(value,	new InpleDateDivComparator());
					}
				}
			}
		}

		return map;
	}

	/**
	 *
	 * @param str
	 * @return
	 */
	private String nullToEmpty(String str) {
		if (str == null) {
			return "";
		}
		if (str.equals("null")) {
			return "";
		}
		return str;
	}

	/**
	 * 対象学生の受験予定大学に入試日程詳細データを付加
	 * @param individual
	 * @param univ
	 * @param faculty
	 * @param dept
	 * @param entExamModeCd 入試形態
	 * @return
	 * @throws Exception
	 */
	protected List searchPlans(
			PreparedStatement ps1,
			String year,
			String div) throws Exception {

		List result = new ArrayList();

		//入試日程詳細リストを取得します。
		ResultSet rs1 = null;
		try {

			//入試形態なし、または入試形態が一般（01）
			if (entExamModeCd == null
					|| (nullToEmpty(entExamModeCd).equals("01"))) {

				rs1 = ps1.executeQuery();

				//データ詰め
				while (rs1.next()) {

					PlanUnivData data = new PlanUnivData(
							rs1.getString("UNIVCD"),
							rs1.getString("UNIVNAME_ABBR"),
							rs1.getString("FACULTYCD"),
							rs1.getString("FACULTYNAME_ABBR"),
							rs1.getString("DEPTCD"),
							rs1.getString("DEPTNAME_ABBR"),
							rs1.getString("SCHEDULECD"),
							rs1.getString("DEPTSORTKEY"),
							rs1.getString("ENTEXAMMODECD"),
							rs1.getString("ENTEXAMMODENAME"),
							rs1.getString("EXAMYEAR"),
							rs1.getString("EXAMDIV"),
							rs1.getString("UNIVDIVCD"),
							rs1.getString("PREFCD_HQ"),
							rs1.getString("ENTEXAMDIV_1_2TERM"),
							rs1.getString("ENTEXAMDIV_1_2ORDER"),
							rs1.getString("SCHOOLPROVENTDIV"),
							rs1.getString("ENTEXAMINPLEDATE1_1") == null ? null : ScheduleDetailData.formatter.parse(rs1.getString("ENTEXAMINPLEDATE1_1")),
							rs1.getString("ENTEXAMINPLEDATE1_2") == null ? null : ScheduleDetailData.formatter.parse(rs1.getString("ENTEXAMINPLEDATE1_2")),
							rs1.getString("ENTEXAMINPLEDATE1_3") == null ? null : ScheduleDetailData.formatter.parse(rs1.getString("ENTEXAMINPLEDATE1_3")),
							rs1.getString("ENTEXAMINPLEDATE1_4") == null ? null : ScheduleDetailData.formatter.parse(rs1.getString("ENTEXAMINPLEDATE1_4")),
							rs1.getString("ENTEXAMINPLEDATE2_1") == null ? null : ScheduleDetailData.formatter.parse(rs1.getString("ENTEXAMINPLEDATE2_1")),
							rs1.getString("ENTEXAMINPLEDATE2_2") == null ? null : ScheduleDetailData.formatter.parse(rs1.getString("ENTEXAMINPLEDATE2_2")),
							rs1.getString("ENTEXAMINPLEDATE2_3") == null ? null : ScheduleDetailData.formatter.parse(rs1.getString("ENTEXAMINPLEDATE2_3")),
							rs1.getString("ENTEXAMINPLEDATE2_4") == null ? null : ScheduleDetailData.formatter.parse(rs1.getString("ENTEXAMINPLEDATE2_4")),
							rs1.getString("PLEDATEDIV1_1"),
							rs1.getString("PLEDATEDIV1_2"),
							rs1.getString("PLEDATEDIV1_3"),
							rs1.getString("PLEDATEUNITEDIV"),
							rs1.getString("PLEDATEDIV2_1"),
							rs1.getString("PLEDATEDIV2_2"),
							rs1.getString("PLEDATEDIV2_3"),
							rs1.getString("SCHEDULEBRANCHNAME"));

					//高３大学マスタの最新のコードと模試区分でないときは、
					//統合分割された扱いにする。（グレイアウト表示フラグ）
					if (!(rs1.getString("EXAMYEAR").equals(year)
							&& rs1.getString("EXAMDIV").equals(div))) {
						data.setUnited(true);
					}else {
						data.setUnited(false);
					}

					// ガイドラインがなかったら空白なしの文字列
					if (rs1.getString("GUIDEREMARKS") != null) {
						data.setGuideLine(rs1.getString("GUIDEREMARKS"));
					}

					// 2016/01/15 QQ)Nishiyama 大規模改修 ADD START
					data.setKanaNum            (rs1.getString("KANA_NUM"));             // カナ付50音順番号
					data.setUniNightDiv        (rs1.getString("UNINIGHTDIV"));          // 大学夜間部区分
					data.setFacultyConCd       (rs1.getString("FACULTYCONCD"));         // 学部内容コード
					data.setScheduleCd         (rs1.getString("SCHEDULECD"));           // 大学グループ区分（日程コード）
					data.setDeptSerialNo       (rs1.getString("DEPTSERIAL_NO"));        // 学科通番
					data.setScheduleSys        (rs1.getString("SCHEDULESYS"));          // 日程方式番
					data.setScheduleSysBranchCd(rs1.getString("SCHEDULESYSBRANCHCD"));  // 日程方式枝
					data.setDeptNameKana       (rs1.getString("DEPTNAME_KANA"));        // 学科カナ名
					// 2016/01/15 QQ)Nishiyama 大規模改修 ADD END

					result.add(data);
				}
			}

			return result;

		} finally {
			DbUtils.closeQuietly(null, null, rs1);
		}
	}

	/**
	 * 受験予定大学一覧を取得
	 * @param individualCd
	 * @param univCd
	 * @param facultyCd
	 * @param deptCd
	 * @param entExamModeCd
	 * @return
	 * @throws Exception
	 */
	protected List searchPlannedUnivs(
			String year,
			String div,
			String individualCd,
			String univCd,
			String facultyCd,
			String deptCd,
			String entExamModeCd) throws Exception {

		List result = new ArrayList();

		PreparedStatement ps2 = null;
		ResultSet rs2 = null;

		try {

			Query query = QueryLoader.getInstance().load("i08");

			if (entExamModeCd != null) {
				query.append("AND t1.entexammodecd = ?");
			}

			if (univCd != null) {
				query.append("AND t1.univcd = ?");
			}

			if (facultyCd != null) {
				query.append("AND t1.facultycd = ?");
			}

			if (deptCd != null) {
				query.append("AND t1.deptcd = ?");
			}

			query.append("ORDER BY t1.univcd, t1.facultycd, t1.deptcd");

			ps2 = conn.prepareStatement(query.toString());
			ps2.setString(1, individualCd);

			int index = 2;

			if (entExamModeCd != null) {
				ps2.setString(index++, entExamModeCd);
			}

			if (univCd != null) {
				ps2.setString(index++, univCd);
			}

			if (facultyCd != null) {
				ps2.setString(index++, facultyCd);
			}

			if (deptCd != null) {
				ps2.setString(index++, deptCd);
			}

			rs2 = ps2.executeQuery();

			//データ詰め
			while (rs2.next()) {

				PlanUnivData data = new PlanUnivData();

				data.setRegistered(true);
				data.setYear(rs2.getString("YEAR"));
				data.setExamDiv(rs2.getString("EXAMDIV"));
				data.setUnivCd(rs2.getString("UNIVCD"));
				data.setUnivNameAbbr(rs2.getString("UNIVNAME_ABBR"));
				data.setFacultyCd(rs2.getString("FACULTYCD"));
				data.setFacultyNameAbbr(rs2.getString("FACULTYNAME_ABBR"));
				data.setDeptCd(rs2.getString("DEPTCD"));
				data.setDeptSortKey(rs2.getString("DEPTSORTKEY"));
				data.setDeptNameAbbr(rs2.getString("DEPTNAME_ABBR"));
				data.setEntExamTypeCd(rs2.getString("ENTEXAMMODECD"));
				data.setEntExamOdeName(rs2.getString("ENTEXAMMODENAME"));

				final ScheduleDetailData schedule =
					new ScheduleDetailData(
							rs2.getInt("ENTEXAMDIV1"),
							rs2.getInt("ENTEXAMDIV2"),
							rs2.getInt("ENTEXAMDIV3"));

				data.setSchedule(schedule);

				data.setRemarks(rs2.getString("REMARKS"));

				//下記項目すべての値が「1」のレコードの備考を
				//大学の備考として表示する
				if (schedule.getEntExamDiv_1_2order() == 1
					&& schedule.getSchoolProventDiv() == 1
					&& schedule.getEntExamDiv_1_2term() == 1) {
					setRemarks(data.getRemarks());
				}

				data.setUnivDivCd(rs2.getString("UNIVDIVCD"));
				data.setPrefCdHq(rs2.getString("PREFCD_HQ"));

				if (!(rs2.getString("YEAR").equals(year)
					&& rs2.getString("EXAMDIV").equals(div))) {
					data.setUnited(true);
				} else {
					data.setUnited(false);
				}

				int MAXDATAS = 10;

				List planDates = new ArrayList();
				List planSites = new ArrayList();

				for (int i = 0; i < MAXDATAS; i++) {
					String date = rs2.getString("EXAMPLANDATE" + (i + 1));
					String site = rs2.getString("PROVENTEXAMSITE" + (i + 1));
					if (date != null) {
						planDates.add(date);
						planSites.add(nullToEmpty(site));
					}
				}

				data.setExamPlanDates(
					(String[]) planDates.toArray(new String[0]));
				//ない場合は空欄で埋める
				data.setProvEntExamSite(
					(String[]) planSites.toArray(new String[0]));

				/** ガイドラインがなかったら new String("") <= 空白なしの文字列をいれて！*/
				String guideRemarks = rs2.getString("guideremarks");
				if (guideRemarks == null) {
					guideRemarks = "";
				}
				data.setGuideLine(guideRemarks);


				// 2016/01/15 QQ)Nishiyama 大規模改修 ADD START
				data.setKanaNum            (rs2.getString("KANA_NUM"));             // カナ付50音順番号
				data.setUniNightDiv        (rs2.getString("UNINIGHTDIV"));          // 大学夜間部区分
				data.setFacultyConCd       (rs2.getString("FACULTYCONCD"));         // 学部内容コード
				data.setScheduleCd         (rs2.getString("SCHEDULECD"));           // 大学グループ区分（日程コード）
				data.setDeptSerialNo       (rs2.getString("DEPTSERIAL_NO"));        // 学科通番
				data.setScheduleSys        (rs2.getString("SCHEDULESYS"));          // 日程方式番
				data.setScheduleSysBranchCd(rs2.getString("SCHEDULESYSBRANCHCD"));  // 日程方式枝
				data.setDeptNameKana       (rs2.getString("DEPTNAME_KANA"));        // 学科カナ名
				// 2016/01/15 QQ)Nishiyama 大規模改修 ADD END

				result.add(data);
			}

			return result;

		} finally {
			DbUtils.closeQuietly(null, ps2, rs2);
		}
	}

	/**
	 * @return
	 */
	public String getIndividualCd() {
		return individualCd;
	}

	/**
	 * @param string
	 */
	public void setIndividualCd(String string) {
		individualCd = string;
	}

	/**
	 * @return
	 */
	public String getEntExamModeCd() {
		return entExamModeCd;
	}

	/**
	 * @param string
	 */
	public void setEntExamModeCd(String string) {
		entExamModeCd = string;
	}

	/**
	 * @param b
	 */
	public void setFirst(boolean b) {
		isFirst = b;
	}

	/**
	 * @return
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param string
	 */
	public void setRemarks(String string) {
		remarks = string;
	}

}

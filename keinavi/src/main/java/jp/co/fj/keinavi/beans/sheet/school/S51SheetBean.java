/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.exam.ExamFilterManager;
import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.sheet.ExtS51ClassListBean;
import jp.co.fj.keinavi.data.sheet.ExtS51TakouListBean;
import jp.co.fj.keinavi.data.sheet.SheetSchoolData;
import jp.co.fj.keinavi.excel.data.school.S51ClassListBean;
import jp.co.fj.keinavi.excel.data.school.S51Item;
import jp.co.fj.keinavi.excel.data.school.S51KakaiListBean;
import jp.co.fj.keinavi.excel.data.school.S51ListBean;
import jp.co.fj.keinavi.excel.data.school.S51TakouListBean;
import jp.co.fj.keinavi.excel.data.school.S51YearListBean;
import jp.co.fj.keinavi.excel.school.S51;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * @author kawai
 *
 */
public class S51SheetBean extends AbstractSheetBean {

	// データクラス
	private final S51Item data = new S51Item();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		String[] code = super.getSubjectCDArray(); // 型・科目設定値
		List graph = super.getSubjectGraphList(); // 型・科目グラフ表示対象
		boolean isNewExam = KNUtil.isNewExam(exam); // 新テストかどうか

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntHyouFlg(super.getIntFlag(IProfileItem.CHART)); // 表フラグ
		data.setIntGraphFlg(super.getIntFlag(IProfileItem.GRAPH)); // グラフフラグ
		data.setIntClassFlg(super.getIntFlag(IProfileItem.PAST_COMP_CLASS)); // クラス比較フラグ
		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ

		// 過年度比較フラグ
		// 過年度比較出来る模試なら設定値
		if (ExamFilterManager.execute("prev", login, examSession, exam)) {
			data.setIntNendoFlg(super.getIntFlag(IProfileItem.PAST_COMP_PREV));
		// 出来ないなら0
		} else {
			data.setIntNendoFlg(0);
		}

		// 他校比較フラグ
		// センターリサーチは「0」固定
		if (KNUtil.isCenterResearch(exam)) {
			data.setIntTakouFlg(0);
		// そうでなければ設定値
		} else {
			data.setIntTakouFlg(super.getIntFlag(IProfileItem.PAST_COMP_OTHER));
		}
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// データリストの取得
			Query query = QueryLoader.getInstance().load("s22_1");
			query.setStringArray(1, code); // 型・科目コード

			ps = conn.prepareStatement(query.toString());
			ps.setString(1, exam.getExamYear()); // 対象年度
			ps.setString(2, exam.getExamCD()); // 対象模試

			List c1 = new LinkedList(); // 型の入れ物
			List c2 = new LinkedList(); // 科目の入れ物
				
			rs = ps.executeQuery();
			while (rs.next()) {
				// データBeanを作って入れる
				S51ListBean bean = new S51ListBean();
				bean.setStrKmkCd(rs.getString(1));
				bean.setStrKmkmei(rs.getString(2));
				bean.setIntDispKmkFlg(graph.contains(rs.getString(1)) ? 1 : 0);
				bean.setStrHaitenKmk(rs.getString(3));
				
				if (rs.getInt(1) >= 7000) c1.add(bean); // 型
				else c2.add(bean); // 科目
			}
			// 型・科目の順番で入れる
			data.getS51List().addAll(c1);
			data.getS51List().addAll(c2);
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
				
		
		// 出力可能な模試を設定
		insertIntoExistsExam();
		// 同系統で過去の模試の配列
		final ExamData[] past = getPastExamArray();
		// 模試コード変換テーブルをセットアップ
		insertIntoExamCdTrans(past);
		// 模試科目コード変換テーブルをセットアップ
		insertIntoSubCDTrans(past);

		// 過回比較
		if (data.getIntGraphFlg() == 1 || data.getIntHyouFlg() == 1) {
			this.executeCompPast(data, past, isNewExam);
		}

		// 過年度比較データリスト
		if (data.getIntNendoFlg() == 1) this.executeCompPrev(data, past, isNewExam);

		// クラス比較データリスト
		if (data.getIntClassFlg() == 1) this.executeCompClass(data, isNewExam);

		// 他校比較データリスト
		if (data.getIntTakouFlg() == 1) this.executeCompOther(data, past, isNewExam);
	}

	/**
	 * 過回比較用データリストを取得する
	 * @param data
	 * @throws Exception
	 */
	private void executeCompPast(S51Item data, ExamData[] past, boolean isNewExam) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("s51_1").toString());
			ps.setString(3, profile.getBundleCD()); // 一括コード

			// 科目分繰り返す
			Iterator ite = data.getS51List().iterator();
			while (ite.hasNext()) {
				S51ListBean bean = (S51ListBean) ite.next();
				
				ps.setString(4, bean.getStrKmkCd()); // 科目コード
				
				for (int i=0; i<past.length; i++) {
					S51KakaiListBean k = new S51KakaiListBean();
					k.setStrMshmei(past[i].getExamNameAbbr());
					k.setStrMshDate(past[i].getInpleDate());

					ps.setString(1, past[i].getExamYear()); // 模試年度
					ps.setString(2, past[i].getExamCD()); // 模試コード

					rs = ps.executeQuery();

					if (rs.next()) {
						k.setIntNinzu(rs.getInt(1));

						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							k.setFloHeikin(-999);
							k.setFloHensa(rs.getFloat(2));
						} else {
							k.setFloHeikin(rs.getFloat(2));
							k.setFloHensa(rs.getFloat(3));
						}

					} else {
						k.setIntNinzu(-999);
						k.setFloHeikin(-999);
						k.setFloHensa(-999);
					}

					rs.close();

					bean.getS51KakaiList().add(k);					
					
				}
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 過年度比較用データリストを取得する
	 * @param data
	 * @throws Exception
	 */
	private void executeCompPrev(S51Item data, ExamData[] past, boolean isNewExam) throws Exception {
		String[] years = super.getCompYearArray(); // 比較対象年度

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("s51_2").toString());
			ps.setString(2, profile.getBundleCD()); // 一括コード

			// 科目分繰り返す
			Iterator ite = data.getS51List().iterator();
			while (ite.hasNext()) {
				S51ListBean bean = (S51ListBean) ite.next();
				
				ps.setString(1, bean.getStrKmkCd()); // 科目コード
				
				// 比較対象年度分繰り返す
				for (int i=0; i<years.length; i++) {
					// 過回模試分繰り返す
					for (int j=0; j<past.length; j++) {
						// 模試短縮名と模試実施基準日をきちんと取らないとだめかも？？
						S51YearListBean y = new S51YearListBean();
						y.setStrNendo(years[i]);
						y.setStrMshmei(past[j].getExamNameAbbr());
						y.setStrMshDate(past[j].getInpleDate());
						bean.getS51YearList().add(y);

						ps.setString(4, past[j].getExamCD()); // 模試コード
						
						// 対象年度の調整
						// 対象学年の差分だけ年度を減らす
						ps.setString(
							3,
							String.valueOf(
								Integer.parseInt(years[i])
								- Integer.parseInt(exam.getTargetGrade())
								+ Integer.parseInt(past[j].getTargetGrade())
							)
						);
						
						rs = ps.executeQuery();

						if (rs.next()) {
							y.setIntNinzu(rs.getInt(1));

							// 新テストなら平均点→無効値／平均偏差値→平均点
							if (isNewExam) {
								y.setFloHeikin(-999);
								y.setFloHensa(rs.getFloat(2));
							} else {
								y.setFloHeikin(rs.getFloat(2));
								y.setFloHensa(rs.getFloat(3));
							}
							
						} else {
							y.setIntNinzu(-999);
							y.setFloHeikin(-999);
							y.setFloHensa(-999);
						}

						rs.close();						
					}
				}
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}

	}

	/**
	 * クラス比較用データリストを取得する
	 * @param data
	 * @throws Exception
	 */
	private void executeCompClass(S51Item data, boolean isNewExam) throws Exception {
		// ワークテーブルのセットアップ
		super.insertIntoCountCompClass();

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		try {
			// 同年度で過去の同種の模試
			Query query = QueryLoader.getInstance().load("s51_3");
			super.outDate2InDate(query); // データ開放日を書き換え
						
			ps1 = conn.prepareStatement(query.toString());
			ps1.setInt(1, login.isHelpDesk() ? 1 : 0); // ヘルプデスクかどうか
			ps1.setString(2, exam.getExamTypeCD()); // 模試種類コード
			ps1.setString(3, exam.getExamYear()); // 模試年度
			ps1.setString(4, exam.getTargetGrade()); // 対象学年
			ps1.setString(5, exam.getDataOpenDate()); // データ開放日

			// クラス比較用データリスト（自校）
			ps2 = conn.prepareStatement(QueryLoader.getInstance().load("s51_1").toString());
			ps2.setString(3, profile.getBundleCD()); // 一括コード

			// クラス比較用データリスト（クラス）
			ps3 = conn.prepareStatement(QueryLoader.getInstance().load("s51_4").toString());
			ps3.setString(3, profile.getBundleCD()); // 一括コード
			ps3.setString(10, profile.getBundleCD()); // 一括コード

			// 科目分繰り返す
			Iterator ite = data.getS51List().iterator();
			while (ite.hasNext()) {
				S51ListBean bean = (S51ListBean) ite.next();
				
				// 科目コード
				ps2.setString(4, bean.getStrKmkCd());
				ps3.setString(4, bean.getStrKmkCd());
				ps3.setString(7, bean.getStrKmkCd());
				ps3.setString(11, bean.getStrKmkCd());
				
				// 自校
				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					S51ClassListBean c = new S51ClassListBean();
					c.setStrMshmei(rs1.getString(3));
					c.setStrMshDate(rs1.getString(4));
					c.setIntDispClassFlg(1);

					ps2.setString(1, rs1.getString(1)); // 模試年度
					ps2.setString(2, rs1.getString(2)); // 模試コード

					rs2 = ps2.executeQuery();

					if (rs2.next()) {
						c.setIntNinzu(rs2.getInt(1));

						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							c.setFloHeikin(-999);
							c.setFloHensa(rs2.getFloat(2));
						} else {
							c.setFloHeikin(rs2.getFloat(2));
							c.setFloHensa(rs2.getFloat(3));
						}
						
					} else {
						c.setIntNinzu(-999);
						c.setFloHeikin(-999);
						c.setFloHensa(-999);
					}

					rs2.close();

					bean.getS51ClassList().add(c);
				}
				rs1.close();

				// クラス
				List container = new ArrayList(); // 入れ物

				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					// 模試年度
					ps3.setString(1, rs1.getString(1));
					ps3.setString(5, rs1.getString(1));
					ps3.setString(8, rs1.getString(1));
					// 模試コード
					ps3.setString(2, rs1.getString(2));
					ps3.setString(6, rs1.getString(2));
					ps3.setString(9, rs1.getString(2));
						
					rs3 = ps3.executeQuery();
					while (rs3.next()) {
						ExtS51ClassListBean c = new ExtS51ClassListBean();
						c.setStrGrade(rs3.getString(1));
						c.setStrClass(rs3.getString(3));
						c.setStrMshmei(rs1.getString(3));
						c.setStrMshDate(rs1.getString(4));
						c.setIntNinzu(rs3.getInt(4));
						c.setDataOpenDate(rs1.getString(5));
						c.setClassDispSeq(rs3.getInt(7));
						c.setIntDispClassFlg(rs3.getInt(8));

						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							c.setFloHeikin(-999);
							c.setFloHensa(rs3.getFloat(5));
						} else {
							c.setFloHeikin(rs3.getFloat(5));
							c.setFloHensa(rs3.getFloat(6));
						}

						container.add(c);
					}
					rs3.close();
				}
				rs1.close();

				// ソートして入れる
				Collections.sort(container);
				bean.getS51ClassList().addAll(container);
			}
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(rs3);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(ps3);
		}
	}

	/**
	 * 他校比較用データリストを取得する
	 * @param data
	 * @param past
	 * @throws Exception
	 */
	private void executeCompOther(S51Item data, ExamData[] past, boolean isNewExam) throws Exception {
		List compSchoolList = super.getCompSchoolList(); // 比較対象高校

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		try {
			// 他校比較用データリスト（自校）
			ps1 = conn.prepareStatement(QueryLoader.getInstance().load("s51_1").toString());
			ps1.setString(3, profile.getBundleCD()); // 一括コード

			// 他校比較用データリスト（全国）
			ps2 = conn.prepareStatement(QueryLoader.getInstance().load("s51_5_1").toString());

			// 他校比較用データリスト（県）
			ps3 = conn.prepareStatement(QueryLoader.getInstance().load("s51_5_2").toString());

			// 他校比較用データリスト（他校）
			ps4 = conn.prepareStatement(QueryLoader.getInstance().load("s51_5_3").toString());
			ps4.setString(6, exam.getExamYear()); // 模試年度
			ps4.setString(7, exam.getExamCD()); // 模試コード

			// 科目分繰り返す
			Iterator ite1 = data.getS51List().iterator();
			while (ite1.hasNext()) {
				S51ListBean bean = (S51ListBean) ite1.next();
				
				// 科目コード
				ps1.setString(4, bean.getStrKmkCd());
				ps2.setString(3, bean.getStrKmkCd());
				ps3.setString(3, bean.getStrKmkCd());
				ps4.setString(3, bean.getStrKmkCd());
				
				// 自校
				for (int i=0; i<past.length; i++) {
					S51TakouListBean t = new S51TakouListBean();
					t.setStrGakkomei(profile.getBundleName());
					t.setStrMshmei(past[i].getExamNameAbbr());
					t.setStrMshDate(past[i].getInpleDate());
					t.setIntDispGakkoFlg(1);

					ps1.setString(1, past[i].getExamYear()); // 模試年度
					ps1.setString(2, past[i].getExamCD()); // 模試コード

					rs1 = ps1.executeQuery();

					if (rs1.next()) {
						t.setIntNinzu(rs1.getInt(1));

						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							t.setFloHeikin(-999);
							t.setFloHensa(rs1.getFloat(2));
						} else {
							t.setFloHeikin(rs1.getFloat(2));
							t.setFloHensa(rs1.getFloat(3));
						}

					} else {
						t.setIntNinzu(-999);
						t.setFloHeikin(-999);
						t.setFloHensa(-999);
					}

					rs1.close();

					bean.getS51TakouList().add(t);
				}

				// 他校
				List container = new ArrayList(); // 入れ物

				for (int i=0; i<past.length; i++) {
					// 模試年度
					ps2.setString(1, past[i].getExamYear());
					ps3.setString(1, past[i].getExamYear());
					ps4.setString(1, past[i].getExamYear());
					// 模試コード
					ps2.setString(2, past[i].getExamCD());
					ps3.setString(2, past[i].getExamCD());
					ps4.setString(2, past[i].getExamCD());
	
					// 学校分繰り返す
					int count = 0;
					Iterator ite2 = compSchoolList.iterator();
					while (ite2.hasNext()) {
						SheetSchoolData school = (SheetSchoolData) ite2.next();

						switch (school.getSchoolTypeCode()) {
							// 全国
							case 1:
								rs2 = ps2.executeQuery();
								break;
						
							// 都道府県
							case 2:
								ps3.setString(4, school.getPrefCD()); // 県コード
								ps3.setString(5, school.getPrefCD()); // 県コード
								rs2 = ps3.executeQuery();
								break;

							// 高校
							case 3:
								ps4.setString(4, school.getSchoolCD()); // 一括コード
								ps4.setString(5, school.getSchoolCD()); // 一括コード
								rs2 = ps4.executeQuery();
								break;
						}

						if (rs2.next()) {
							ExtS51TakouListBean t = new ExtS51TakouListBean();
							t.setStrGakkomei(rs2.getString(1));
							t.setStrMshmei(past[i].getExamNameAbbr());
							t.setStrMshDate(past[i].getInpleDate());
							t.setIntNinzu(rs2.getInt(2));
							t.setExamYear(past[i].getExamYear());
							t.setSchoolDispSeq(count++);
							t.setIntDispGakkoFlg(school.getGraphFlag());
							t.setDataOpenDate(past[i].getDataOpenDate());

							// 新テストなら平均点→無効値／平均偏差値→平均点
							if (isNewExam) {
								t.setFloHeikin(-999);
								t.setFloHensa(rs2.getFloat(3));
							} else {
								t.setFloHeikin(rs2.getFloat(3));
								t.setFloHensa(rs2.getFloat(4));
							}

							container.add(t);
						}
						rs2.close();
					}
				}
				// ソートして詰める
				Collections.sort(container);
				bean.getS51TakouList().addAll(container);				
			}
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(ps3);
			DbUtils.closeQuietly(ps4);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S51_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_PAST_SCORE;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		
		// 出力対象があるなら作る
		if (data.getIntHyouFlg() + data.getIntGraphFlg()
				+ data.getIntNendoFlg() + data.getIntClassFlg()
				+data.getIntTakouFlg() > 0) {
			return new S51().s51(
					this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
		} else {
			return true;
		}
	}

}

package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * 成績概況（クラス比較）データリスト
 * 作成日: 2004/06/28
 * @author	T.Sakai
 */
public class S31ListBean {
	//型・科目名コード
	private String strKmkCd = "";
	//型・科目名
	private String strKmkmei = "";
	//配点
	private String strHaitenKmk = "";
	//型･科目用グラフ表示フラグ
	private int intDispKmkFlg = 0;
	//クラスデータリスト
	private ArrayList s31ClassList = new ArrayList();
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public int getIntDispKmkFlg() {
		return this.intDispKmkFlg;
	}
	public ArrayList getS31ClassList() {
		return this.s31ClassList;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setIntDispKmkFlg(int intDispKmkFlg) {
		this.intDispKmkFlg = intDispKmkFlg;
	}
	public void setS31ClassList(ArrayList s31ClassList) {
		this.s31ClassList = s31ClassList;
	}
}

package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.fj.keinavi.beans.NewestExamDivSearchBean;
import jp.co.fj.keinavi.data.individual.I301Data;
import jp.co.fj.keinavi.util.CollectionUtil;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 受験予定大学候補
 * 
 * 2004.08.10	Keisuke KONDO - TOTEC
 * 				新規作成
 * 
 * 2005.02.24 	Keisuke KONDO - TOTEC
 * 				[1]change 配点比率の無効値を"-1"から"0"に変更
 * 
 * 2005.03.08 	Keisuke KONDO - TOTEC
 * 				[2]change 判定対象模試を取得する時の条件に担当クラスの年度を追加
 * 
 * 2005.02.28   Keisuke KONDO - TOTEC
 * 				[3]change 模試で記入された大学は対象模試を使用するように修正
 * 
 * 2005.05.27	Keisuke KONDO - TOTEC
 * 				[4]change 模試で記入された大学のリストを
 * 				カウントするメソッドを追加
 * 
 * 2005.06.24	Keisuke KONDO - TOTEC
 * 				[5]change 模試で記入された大学の配点比を
 * 				高３模試用判定大学マスタより取得するよう修正
 * 
 * 2005.07.01 	Tomohisa YAMADA - TOTEC
 * 				[6]判定履歴における配点比の無効値を「0」から
 * 				「0または9999」にする。以前の0を保持しておく。
 * 
 * <2006年度対応>
 * 2006.05.13	Tomohisa YAMADA - TOTEC
 * 				[7]二次配点比（学科＋その他）
 *
 * <2010年度対応>
 * 2009.10.01   Shoji HASE - TOTEC       
 * 				[8]「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 *              「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 *              
 * 2009.10.15   Shoji HASE - TOTEC
 * 				[8]	UNIV8CDからDEPTCDの抽出を「7文字目から2文字」→「7文字目から4文字」に変更。
 * 
 * 2009.10.29   Shoji HASE - TOTEC
 * 				[8]「HS3_UNIV_EXAM：高3模試用判定大学マスタ」テーブルを
 *              「UNIVMASTER_CHOICESCHOOL：大学マスタ模試用志望校」テーブルに置き換え
 *				BRANCHCD='01'からUNICDBRANCHCD='001'に変更
 *
 * <2010年度改修>
 * 2010.01.20	Tomohisa YAMADA - TOTEC
 * 				入試日程変更
 *
 *
 * @author Keisuke KONDO - TOTEC
 * @version 1.0
 * 
 */
public class I301Bean extends DefaultBean {
	
	private static final long serialVersionUID = -7346110453866993344L;
	
	private static final String ENTEXAMTYPECD = "01";

	/** 出力パラメター */
	//模試で記入された大学用コレクション
	Collection i301DatasAtExam;
	//判定履歴用コレクション
	Collection i301DatasAtJudge;
	
	/** 入力パラメター */
	private String individualId;
	private String targetExamYear;//[3] add
	private String targetExamCd;//[3] add
	private String targetExamDiv;
	private String examName;
	private String examYear;
	private String examCd;
	private String examDiv;
	private String[] univValueAtExam;
	private String[] univValueAtJudge;
	private String errorMessage;
	private String classYear;//[2] add
	private NewestExamDivSearchBean newestExamDiv;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		// 対象模試の模試区分を設定
		setTargetExamDiv();
		
		// 判定対象模試を設定
		getExamination();
		
		boolean flag = true;
		if (getUnivValueAtExam() != null) {
			//模試で記入された大学を登録
			final String errorMessage = execRigUnivAtExam(getUnivValueAtExam());
			if (errorMessage == null) {
				setErrorMessage("受験予定大学を登録しました。");
			} else {
				setErrorMessage(errorMessage);
				conn.rollback();
				flag = false;
			}
		}
		
		final String[] univValueAtJudge = getUnivValueAtJudge();
		if (univValueAtJudge != null && flag) {
			//判定履歴の大学を登録
			final String errorMessage = execRigUnivAtJudge(univValueAtJudge);
			if (errorMessage == null) {
				setErrorMessage("受験予定大学を登録しました。");
			} else {
				setErrorMessage(errorMessage);
				conn.rollback();
			}
		}
		
		/**一覧用出力データ*/
		setI301DatasAtExam(execDatasAtExam());	//判定対象模試の志望校評価に入っている大学
		setI301DatasAtJudge(execDatasAtJudge());//判定履歴に入っている大学
	}
		
	//紐付けテーブルで最新に
	private String hookUpSql() {
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT NEWUNIV8CD ");
		strBuf.append("FROM UNIVCDHOOKUP ");
		strBuf.append("WHERE ");
		strBuf.append("OLDUNIV8CD = ? ");
		strBuf.append("AND YEAR = ? ");
		strBuf.append("AND OLDEXAMDIV = ? ");
		return strBuf.toString();
	}
	
	//高３大学マスタの最新に
	private String newUnivSql() {
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT EVENTYEAR EXAMYEAR, EXAMDIV ");
		strBuf.append("FROM UNIVMASTER_BASIC ");
		strBuf.append("WHERE ");
		strBuf.append("UNIVCD = ? ");
		strBuf.append("AND FACULTYCD = ? ");
		strBuf.append("AND DEPTCD = ? ");
		strBuf.append("AND EVENTYEAR = ? ");
		strBuf.append("AND EXAMDIV = ?");
		return strBuf.toString();
	}
	
	//大学名称を取得
	private String univNameSql() {
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT HS.UNINAME_ABBR UNIVNAME_ABBR, HS.FACULTYNAME_ABBR, HS.DEPTNAME_ABBR, P.PREFNAME ");
		strBuf.append("FROM UNIVMASTER_BASIC HS ");
		strBuf.append("LEFT JOIN PREFECTURE P ");
		strBuf.append("ON P.PREFCD = HS.PREFCD_EXAMHO ");
		strBuf.append("WHERE HS.EVENTYEAR = ? ");
		strBuf.append("AND HS.EXAMDIV = ? ");
		strBuf.append("AND HS.UNIVCD = ? ");
		strBuf.append("AND HS.FACULTYCD = ? ");
		strBuf.append("AND HS.DEPTCD = ? ");
		return strBuf.toString();
	}
	
	//受験予定大学に存在しているか
	private String examPlanSql() {
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT UNIVCD ");
		strBuf.append("FROM EXAMPLANUNIV ");
		strBuf.append("WHERE INDIVIDUALID = ? ");
		strBuf.append("AND UNIVCD = ? ");
		strBuf.append("AND FACULTYCD = ? ");
		strBuf.append("AND DEPTCD = ? ");
		return strBuf.toString();
	}
	
	// 模試で記入された大学を登録します。
	private String execRigUnivAtExam(final String[] univValues) throws Exception {
		
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps4 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs4 = null;
		try {
			// 40件制限チェック
			if (execCountExamPlanUnivALL(getIndividualId(), univValues) > 40) {
				return "受験予定の登録件数をオーバーしました。\\n登録できる大学は最大４０です。";
			}
			
			ps1 = conn.prepareStatement(hookUpSql());
			ps1.setString(2, getTargetExamYear());
			ps1.setString(3, getTargetExamDiv());
			ps2 = conn.prepareStatement(newUnivSql());
			ps2.setString(4, newestExamDiv.getExamYear());
			ps2.setString(5, newestExamDiv.getExamDiv());
			ps4 = conn.prepareStatement(examPlanSql());
			
			for (int i = 0; univValues.length > i; i++) {
				String[] univ		 = CollectionUtil.splitComma(univValues[i]);
				String univCd		 = univ[0];
				String facultyCd	 = univ[1];
				String deptCd		 = univ[2];
				String examYear 	 = getTargetExamYear();
				String examDiv		 = getTargetExamDiv();
				
				// 対象模試で紐付け
				ps1.setString(1, univCd + facultyCd + deptCd);
				rs1 = ps1.executeQuery();
				if (rs1.next()) {
					String newUniv8Cd = rs1.getString("NEWUNIV8CD");
					univCd		 = newUniv8Cd.substring(0,4); // 大学
					facultyCd	 = newUniv8Cd.substring(4,6); // 学部
					deptCd		 = newUniv8Cd.substring(6,10); // 学科
				}
				DbUtils.close(rs1);

				// 最新の高３大学マスタに存在するか
				ps2.setString(1, univCd);
				ps2.setString(2, facultyCd);
				ps2.setString(3, deptCd);
				rs2 = ps2.executeQuery();
				if (rs2.next()) {
					examYear = rs2.getString("EXAMYEAR");//模試年度
					examDiv = rs2.getString("EXAMDIV");//模試区分
				}
				DbUtils.close(rs2);
												
				// 受験予定大学に存在しているか
				ps4.setString(1, individualId);
				ps4.setString(2, univCd);
				ps4.setString(3, facultyCd);
				ps4.setString(4, deptCd);
				rs4 = ps4.executeQuery();
				
				// 存在しない
				if (!rs4.next()) {
					//新規レコード作成
					create(
						getIndividualId(), 
						univCd, 
						facultyCd, 
						deptCd, 
						examYear, 
						examDiv);
				}
				DbUtils.close(rs4);
			}
		} finally {
			DbUtils.closeQuietly(null, ps1, rs1);
			DbUtils.closeQuietly(null, ps2, rs2);
			DbUtils.closeQuietly(null, ps4, rs4);
		}
		
		return null;
	}
	
	/**
	 * 判定履歴にある大学を登録します。
	 * @param univValues
	 * @throws SQLException
	 * @throws RigUnivAtJudgeException
	 */
	private String execRigUnivAtJudge(final String[] univValues) throws Exception {
		
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps4 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs4 = null;
		
		try {
			// 40件制限チェック
			if (execCountExamPlanUnivALL(getIndividualId(), univValues) > 40) {
				return "受験予定の登録件数をオーバーしました。\\n登録できる大学は最大４０です。";
			}
			
			ps1 = conn.prepareStatement(hookUpSql());
			ps1.setString(2, getExamYear());
			ps1.setString(3, getExamDiv());
			ps2 = conn.prepareStatement(newUnivSql());
			ps2.setString(4, newestExamDiv.getExamYear());
			ps2.setString(5, newestExamDiv.getExamDiv());
			ps4 = conn.prepareStatement(examPlanSql());
			
			for (int i = 0; univValues.length > i; i++) {
				String[] univ		 = CollectionUtil.splitComma(univValues[i]);
				String univCd		 = univ[0];
				String facultyCd	 = univ[1];
				String deptCd		 = univ[2];
				String examYear 	 = getExamYear();
				String examDiv		 = getExamDiv();
				
				// 判定対象模試で紐付け
				ps1.setString(1, univCd + facultyCd + deptCd);
				rs1 = ps1.executeQuery();
				if (rs1.next()) {
					final String newUniv8Cd = rs1.getString("NEWUNIV8CD");
					univCd		 = newUniv8Cd.substring(0,4); // 大学
					facultyCd	 = newUniv8Cd.substring(4,6); // 学部
					deptCd		 = newUniv8Cd.substring(6,10); // 学科
				}
				DbUtils.close(rs1);
				
				// 最新の高３大学マスタに存在するか
				ps2.setString(1, univCd);
				ps2.setString(2, facultyCd);
				ps2.setString(3, deptCd);
				rs2 = ps2.executeQuery();
				if (rs2.next()) {
					examYear = rs2.getString("EXAMYEAR");//模試年度
					examDiv = rs2.getString("EXAMDIV");//模試区分
				}
				DbUtils.close(rs2);
				
				// 受験予定大学に存在しているか
				ps4.setString(1, individualId);
				ps4.setString(2, univCd);
				ps4.setString(3, facultyCd);
				ps4.setString(4, deptCd);
				rs4 = ps4.executeQuery();
				
				// 存在しない
				if (!rs4.next()) {
					//新規レコード作成
					create(
						getIndividualId(), 
						univCd, 
						facultyCd, 
						deptCd, 
						examYear, 
						examDiv);
				}
				DbUtils.close(rs4);
			}
		} finally {
			DbUtils.closeQuietly(null, ps1, rs1);
			DbUtils.closeQuietly(null, ps2, rs2);
			DbUtils.closeQuietly(null, ps4, rs4);
		}

		return null;
	}
	
	private void create(
			String individualid, 
			String univcd, 
			String facultycd, 
			String deptcd,
			String year,
			String div) throws Exception {
		PlanUnivInsertBean insert = 
			new PlanUnivInsertBean(
					individualid, 
					univcd, 
					facultycd, 
					deptcd, 
					ENTEXAMTYPECD, 
					1, 
					1, 
					1, 
					year, 
					div, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null);
		insert.setConnection(null, conn);
		insert.execute();
	}

	// 模試に記入されている大学の検索
	private Collection execDatasAtExam() throws Exception {
		
		Map map					 = new LinkedHashMap();
		PreparedStatement ps	 = null;
		ResultSet rs			 = null;
		try {
			ps = conn.prepareStatement(getExamUnivSql());
			ps.setString(1, individualId);
			ps.setString(2, individualId);
			rs = ps.executeQuery();
			while (rs.next()) {
				//map -> get
				I301Data data = (I301Data) map.get(rs.getString("CANDIDATERANK"));
				
				//new
				if (data == null) {
					data = new I301Data();
					data.setAllotPntRate1("-");
					data.setAllotPntRate2("-");
					data.setRegistered(false);
					data.setExist(false);
				}
				
				//set
				data.setUnivCd(rs.getString("UNIVCD"));//�@大学コード
				data.setFacultyCd(rs.getString("FACULTYCD"));//�A学部コード
				data.setDeptCd(rs.getString("DEPTCD"));//�B学科コード
				data.setPrefName(rs.getString("PREFNAME"));//�C所在地
				data.setMarkGRating(rs.getString("CENTERRATING"));//�Dセンター
				data.setDiscriptionGRating(rs.getString("SECONDRATING"));//�E２次
				data.setRatingPoint(rs.getString("RATINGPOINT"));//�F総合ポイント
				data.setTotalRating(rs.getString("TOTALRATING"));//�G総合評価
				data.setUnivName_Abbr(rs.getString("UNIVNAME_ABBR"));//�K大学名
				data.setFacultyName_Abbr(rs.getString("FACULTYNAME_ABBR"));//�L学部名
				data.setDeptName_Abbr(rs.getString("DEPTNAME_ABBR"));//�M学科名
				data.setCandidateRank(rs.getString("CANDIDATERANK"));//�P志望順位
				
				// 高３大学マスタに大学が存在するか
				if (rs.getString("UNIVNAME_ABBR") != null
						|| rs.getString("FACULTYNAME_ABBR") != null
						|| rs.getString("DEPTNAME_ABBR") != null) {
					data.setExist(true);//する
				} else {
					data.setExist(false);//しない
				}
                
                String allotPntRate = rs.getString("allotpntrate_all");
                
				
				if (allotPntRate != null) {
					if (allotPntRate.equals("0") || allotPntRate.equals("9999")) {
						allotPntRate = "-";
					}
				}
				
				//�O入試試験区分(1:センタ / 2:２次私大)
				String entExamDiv = rs.getString("ENTEXAMDIV");
				if(entExamDiv != null) {
					if(entExamDiv.equals("1")) {
						data.setAllotPntRate1(allotPntRate);
					} else {
						data.setAllotPntRate2(allotPntRate);
					}
				}
				
				map.put(data.getCandidateRank(), data);
			}
			DbUtils.close(rs);
			DbUtils.close(ps);
			
			return univCdHookUp(map.values());
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	// 判定対象模試情報の取得
	private void getExamination() throws Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT DH.YEAR, DH.EXAMCD, EX.EXAMDIV, EX.EXAMNAME "
					+ "FROM DETERMHISTORY DH INNER JOIN EXAMINATION EX "
					+ "ON EX.EXAMYEAR = DH.YEAR AND EX.EXAMCD = DH.EXAMCD AND DH.INDIVIDUALID = ? AND DH.YEAR <= ? "
					+ "GROUP BY DH.YEAR, DH.EXAMCD, EX.EXAMDIV, EX.EXAMNAME ");
			ps.setString(1, getIndividualId());
			ps.setString(2, getClassYear());
			rs = ps.executeQuery();
			while (rs.next()) {
				setExamYear(rs.getString("YEAR"));		//年度
				setExamCd(rs.getString("EXAMCD"));		//模試コード
				setExamDiv(rs.getString("EXAMDIV"));	//模試区分
				setExamName(rs.getString("EXAMNAME"));	//模試名
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	private void setTargetExamDiv() throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT examdiv FROM examination "
					+ "WHERE examyear = ? AND examcd = ?");
			ps.setString(1, getTargetExamYear());
			ps.setString(2, getTargetExamCd());
			rs = ps.executeQuery();
			if (rs.next()) {
				targetExamDiv = rs.getString(1);
			} else {
				throw new SQLException("模試区分の取得に失敗"
						+ "：ExamYear=" + getTargetExamYear()
						+ "：ExamCd=" + getTargetExamCd());
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	private String getTargetExamDiv() {
		return targetExamDiv;
	}

	// 紐付けテーブルで、最新にする。
	private Collection univCdHookUp(Collection datas) throws Exception {
		
		final List list = new ArrayList();
		
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps4 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs4 = null;
		try {
			//プリコンパイル
			ps1 = conn.prepareStatement(hookUpSql());
			ps1.setString(2, getTargetExamYear());
			ps1.setString(3, getTargetExamDiv());
			ps2 = conn.prepareStatement(newUnivSql());
			ps2.setString(4, newestExamDiv.getExamYear());
			ps2.setString(5, newestExamDiv.getExamDiv());
			ps4 = conn.prepareStatement(examPlanSql());
			
			// コレクションを受け取って、最新化しListにつめて返す。
			for (final Iterator it = datas.iterator(); it.hasNext();) {
				I301Data data = (I301Data)it.next();
				String univCd = data.getUnivCd();
				String facultyCd = data.getFacultyCd();
				String deptCd = data.getDeptCd();
				
				// 対象模試で紐付け
				ps1.setString(1, univCd + facultyCd + deptCd);
				rs1 = ps1.executeQuery();
				if (rs1.next()) {
					final String newUniv8Cd = rs1.getString("NEWUNIV8CD");
					univCd		 = newUniv8Cd.substring(0,4); // 大学
					facultyCd	 = newUniv8Cd.substring(4,6); // 学部
					deptCd		 = newUniv8Cd.substring(6,10); // 学科
				}
				DbUtils.close(rs1);
				
				// 最新の高３大学マスタに存在するか
				if (data.isExist()) {
					ps2.setString(1, univCd);
					ps2.setString(2, facultyCd);
					ps2.setString(3, deptCd);
					rs2 = ps2.executeQuery();
					data.setExist(rs2.next());
					DbUtils.close(rs2);
				}
				
				// 受験予定大学に存在しているか
				ps4.setString(1, individualId);
				ps4.setString(2, univCd);
				ps4.setString(3, facultyCd);
				ps4.setString(4, deptCd);
				rs4 = ps4.executeQuery();
				data.setRegistered(rs4.next());
				DbUtils.close(rs4);
				
				data.setUnivCd(univCd);
				data.setFacultyCd(facultyCd);
				data.setDeptCd(deptCd);
				
				//list -> add
				list.add(data);
			}
		} finally {
			DbUtils.closeQuietly(null, ps1, rs1);
			DbUtils.closeQuietly(null, ps2, rs2);
			DbUtils.closeQuietly(null, ps4, rs4);
		}
		
		return list;
	}
	
	// 模試に記入されている大学の検索ＳＱＬを返します。
	private String getExamUnivSql() {
		
		final StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT ");
		strBuf.append(" /*+ INDEX(CR PK_CANDIDATERATING) */ ");//12/5 sql hint
		strBuf.append("CR.UNIVCD, ");
		strBuf.append("CR.FACULTYCD, ");
		strBuf.append("CR.DEPTCD, ");
		strBuf.append("HS.UNINAME_ABBR UNIVNAME_ABBR, ");
		strBuf.append("HS.FACULTYNAME_ABBR, ");
		strBuf.append("HS.DEPTNAME_ABBR, ");
		strBuf.append("P.PREFNAME, ");
		strBuf.append("CR.INDIVIDUALID, ");
		strBuf.append("CR.CANDIDATERANK, ");
		strBuf.append("CR.CENTERRATING, ");
		strBuf.append("CR.SECONDRATING, ");
		strBuf.append("CR.RATINGPOINT, ");
		strBuf.append("CR.TOTALRATING, ");
        strBuf.append("NVL(CASE WHEN HP.ENTEXAMDIV = '1' AND HS.DOCKINGNEEDLDIV = '1' AND HS.UNIDIV IN ('01', '02') THEN NULL ELSE hp.TOTALALLOTPNT END, '9999') allotpntrate_all, ");
		strBuf.append("HP.ENTEXAMDIV, ");
		strBuf.append("EP.UNIVCD PLAN_UNIVCD ");
		strBuf.append("FROM ");
		strBuf.append("EXAMINATION EX ");
		strBuf.append("INNER JOIN ");
		strBuf.append("CANDIDATERATING CR ");
		strBuf.append("ON CR.INDIVIDUALID = ? ");
		strBuf.append("AND CR.EXAMYEAR = '").append(getTargetExamYear()).append("' ");
		strBuf.append("AND CR.EXAMCD = '").append(getTargetExamCd()).append("' ");
		strBuf.append("AND CR.EXAMYEAR = EX.EXAMYEAR ");
		strBuf.append("AND CR.EXAMCD = EX.EXAMCD ");
		strBuf.append("LEFT JOIN ");
		strBuf.append("UNIVMASTER_BASIC HS ");
		strBuf.append("ON HS.EVENTYEAR = EX.EXAMYEAR ");
		strBuf.append("AND HS.EXAMDIV = EX.EXAMDIV ");
		strBuf.append("AND HS.UNIVCD = CR.UNIVCD ");
		strBuf.append("AND HS.FACULTYCD = CR.FACULTYCD ");
		strBuf.append("AND HS.DEPTCD = CR.DEPTCD ");
		strBuf.append("LEFT JOIN ");
		strBuf.append("UNIVMASTER_CHOICESCHOOL HP ");
		strBuf.append("ON HP.EVENTYEAR = EX.EXAMYEAR ");
		strBuf.append("AND HP.EXAMDIV = EX.EXAMDIV ");
		strBuf.append("AND HP.UNIVCD = CR.UNIVCD ");
		strBuf.append("AND HP.FACULTYCD = CR.FACULTYCD ");
		strBuf.append("AND HP.DEPTCD = CR.DEPTCD ");
		strBuf.append("AND HP.ENTEXAMDIV IN ('1', '2') ");
		strBuf.append("AND HP.UNICDBRANCHCD = '001' ");
		strBuf.append("LEFT JOIN ");
		strBuf.append("PREFECTURE P ");
		strBuf.append("ON P.PREFCD = HS.PREFCD_EXAMHO ");
		strBuf.append("LEFT JOIN ");
		strBuf.append("( ");
		strBuf.append("SELECT ");
		strBuf.append("UNIVCD ");
		strBuf.append(",FACULTYCD ");
		strBuf.append(",DEPTCD ");
		strBuf.append("FROM ");
		strBuf.append("EXAMPLANUNIV ");
		strBuf.append("WHERE ");
		strBuf.append("INDIVIDUALID = ? ");
		strBuf.append("GROUP BY ");
		strBuf.append("UNIVCD ");
		strBuf.append(",FACULTYCD ");
		strBuf.append(",DEPTCD ");
		strBuf.append(") EP ");
		strBuf.append("ON EP.UNIVCD = CR.UNIVCD ");
		strBuf.append("AND EP.FACULTYCD = CR.FACULTYCD ");
		strBuf.append("AND EP.DEPTCD = CR.DEPTCD ");
		strBuf.append("ORDER BY CR.CANDIDATERANK ");
		
		return strBuf.toString();
	}
	
	// 判定履歴、大学コードより大学情報を取得
	private String getDetermHistorySql() {
		
		final StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT ");
		strBuf.append("DH.DISPRANK, ");
		strBuf.append("SUBSTR(NVL(UH.NEWUNIV8CD, (DH.UNIVCD || DH.FACULTYCD || DH.DEPTCD)), 0, 4) FINAL_UNIVCD, ");
		strBuf.append("SUBSTR(NVL(UH.NEWUNIV8CD, (DH.UNIVCD || DH.FACULTYCD || DH.DEPTCD)), 5, 2) FINAL_FACULTYCD, ");
		strBuf.append("SUBSTR(NVL(UH.NEWUNIV8CD, (DH.UNIVCD || DH.FACULTYCD || DH.DEPTCD)), 7, 4) FINAL_DEPTCD, ");
		strBuf.append("DH.UNIVCD OLD_UNIVCD, ");
		strBuf.append("DH.FACULTYCD OLD_FACULTYCD,");
		strBuf.append("DH.DEPTCD OLD_DEPTCD, ");
		strBuf.append("DH.INDIVIDUALID, ");
		strBuf.append("DH.CENTERRATING, ");
		strBuf.append("DH.SECONDRATING, ");
		strBuf.append("DH.RATINGPOINT, ");
		strBuf.append("DH.TOTALRATING, ");
		strBuf.append("CASE WHEN UB.DOCKINGNEEDLDIV = '1' AND UB.UNIDIV IN ('01', '02') THEN 9999 ELSE DH.CENTALLOTPNTRATE END AS CENTALLOTPNTRATE, ");
		strBuf.append("DH.SECALLOTPNTRATE ");
		strBuf.append("FROM ");
		strBuf.append("DETERMHISTORY DH ");
		strBuf.append("INNER JOIN ");
		strBuf.append("EXAMINATION ");
		strBuf.append("ON DH.YEAR = EXAMINATION.EXAMYEAR ");
		strBuf.append("AND DH.EXAMCD = EXAMINATION.EXAMCD ");
		strBuf.append("AND DH.INDIVIDUALID = ? ");
		strBuf.append("AND DH.YEAR <= '").append(getClassYear()).append("' ");//[2] add
		strBuf.append("LEFT JOIN ");
		strBuf.append("UNIVMASTER_BASIC UB ");
		strBuf.append("ON UB.EVENTYEAR = EXAMINATION.EXAMYEAR ");
		strBuf.append("AND UB.EXAMDIV = EXAMINATION.EXAMDIV ");
		strBuf.append("AND UB.UNIVCD = DH.UNIVCD ");
		strBuf.append("AND UB.FACULTYCD = DH.FACULTYCD ");
		strBuf.append("AND UB.DEPTCD = DH.DEPTCD ");
		strBuf.append("LEFT JOIN ");
		strBuf.append("UNIVCDHOOKUP UH ");
		strBuf.append("ON DH.YEAR = UH.YEAR ");
		strBuf.append("AND EXAMINATION.EXAMDIV = UH.OLDEXAMDIV ");
		strBuf.append("AND UH.OLDUNIV8CD = (DH.UNIVCD || DH.FACULTYCD || DH.DEPTCD) ");
		strBuf.append("ORDER BY DISPRANK ");
		return strBuf.toString();
	}
	
	// 判定履歴に登録されている大学を取得します。
	private Collection execDatasAtJudge() throws Exception {
		
		final Collection list = new ArrayList();
		
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		ResultSet rs4 = null;
		try {
			
			ps1 = conn.prepareStatement(getDetermHistorySql());
			ps2 = conn.prepareStatement(newUnivSql());
			ps2.setString(4, newestExamDiv.getExamYear());
			ps2.setString(5, newestExamDiv.getExamDiv());
			ps3 = conn.prepareStatement(univNameSql());
			ps4 = conn.prepareStatement(examPlanSql());
			
			ps1.setString(1, individualId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				final I301Data data = new I301Data();

				// 判定当時の大学コード
				String oldUnivCd	 = rs1.getString("OLD_UNIVCD");
				String oldFacultyCd	 = rs1.getString("OLD_FACULTYCD");
				String oldDeptCd	 = rs1.getString("OLD_DEPTCD");

				// 最新の大学コード
				String newUnivCd	 = rs1.getString("FINAL_UNIVCD");
				String newFacultyCd	 = rs1.getString("FINAL_FACULTYCD");
				String newDeptCd	 = rs1.getString("FINAL_DEPTCD");

				// 判定対象模試年度・区分
				String examYear 	 = getExamYear();
				String examDiv		 = getExamDiv();
				
				data.setCandidateRank(rs1.getString("DISPRANK"));//�@表示順
				data.setMarkGRating(rs1.getString("CENTERRATING"));//�Dセンター
				data.setDiscriptionGRating(rs1.getString("SECONDRATING"));//�E２次
				data.setTotalRating(rs1.getString("TOTALRATING"));//�G総合評価
				
				if (rs1.getString("CENTALLOTPNTRATE").equals("0")
						|| rs1.getString("CENTALLOTPNTRATE").equals("9999")) {
					data.setAllotPntRate1("-");
				} else {
					data.setAllotPntRate1(rs1.getString("CENTALLOTPNTRATE"));//�H配点比率センタ
				}
				
				if (rs1.getString("SECALLOTPNTRATE").equals("0")
						|| rs1.getString("SECALLOTPNTRATE").equals("9999")) {
					data.setAllotPntRate2("-");
				} else {
					data.setAllotPntRate2(rs1.getString("SECALLOTPNTRATE"));//�I配点比率２次
				}
				
				if(rs1.getString("RATINGPOINT") == null || rs1.getString("RATINGPOINT").equals("-1")) {
					data.setRatingPoint("");//�F総合ポイント
				} else {
					data.setRatingPoint(rs1.getString("RATINGPOINT"));//�F総合ポイント
				}
								
				// 最新の高３大学マスタに存在するか
				ps2.setString(1, newUnivCd);
				ps2.setString(2, newFacultyCd);
				ps2.setString(3, newDeptCd);
				rs2 = ps2.executeQuery();
				if (rs2.next()) {
					examYear = rs2.getString("EXAMYEAR");//模試年度
					examDiv = rs2.getString("EXAMDIV");//模試区分
					data.setUnivCd(newUnivCd);
					data.setFacultyCd(newFacultyCd);
					data.setDeptCd(newDeptCd);
					data.setExist(true);
				} else {
					data.setUnivCd(oldUnivCd);
					data.setFacultyCd(oldFacultyCd);
					data.setDeptCd(oldDeptCd);
					data.setExist(false);
				}
				DbUtils.close(rs2);
				
				// 高３大学マスタから大学名称を取得
				ps3.setString(1, examYear);
				ps3.setString(2, examDiv);
				ps3.setString(3, data.getUnivCd());
				ps3.setString(4, data.getFacultyCd());
				ps3.setString(5, data.getDeptCd());
				rs3 = ps3.executeQuery();
				if (rs3.next()) {
					data.setUnivName_Abbr(rs3.getString("UNIVNAME_ABBR"));
					data.setFacultyName_Abbr(rs3.getString("FACULTYNAME_ABBR"));
					data.setDeptName_Abbr(rs3.getString("DEPTNAME_ABBR"));
					data.setPrefName(rs3.getString("PREFNAME"));
				} else {
					data.setUnivName_Abbr(null);
					data.setFacultyName_Abbr(null);
					data.setDeptName_Abbr(null);
					data.setPrefName(null);
				}
				DbUtils.close(rs3);
				
				// 受験予定大学に存在しているか
				ps4.setString(1, individualId);
				ps4.setString(2, data.getUnivCd());
				ps4.setString(3, data.getFacultyCd());
				ps4.setString(4, data.getDeptCd());
				rs4 = ps4.executeQuery();
				data.setRegistered(rs4.next());
				DbUtils.close(rs4);
				
				list.add(data);	
			}
		} finally {
			DbUtils.closeQuietly(null, ps1, rs1);
			DbUtils.closeQuietly(null, ps2, rs2);
			DbUtils.closeQuietly(null, ps3, rs3);
			DbUtils.closeQuietly(null, ps4, rs4);
		}
		
		return list;
	}
	
	//この人の受験予定大学にあるすべての受験大学　＋　登録したい大学（複数）　の件数をチェックします
	private int execCountExamPlanUnivALL(final String individualId,
			final String[] univValues) throws Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			final Set set = new HashSet();
			for (int i = 0; i < univValues.length; i++) {
				final String[] univCds = CollectionUtil.splitComma(univValues[i]);
				set.add(univCds[0] + univCds[1] + univCds[2]);
			}
			
			ps = conn.prepareStatement("SELECT UNIVCD, FACULTYCD, DEPTCD FROM EXAMPLANUNIV "
					+ "WHERE INDIVIDUALID = ? "
					+ "GROUP BY UNIVCD, FACULTYCD, DEPTCD");
			ps.setString(1, individualId);
			rs = ps.executeQuery();
			while (rs.next()) {
				String univData[] = new String[3];
				univData[0] = rs.getString("UNIVCD");
				univData[1] = rs.getString("FACULTYCD");
				univData[2] = rs.getString("DEPTCD");
				set.add(univData[0] + univData[1] + univData[2]);
			}
			
			return set.size();
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	public int getI301DatasAtJudgeSize() {
		if(getI301DatasAtJudge() == null || getI301DatasAtJudge().size() <= 0) {
			return 0;
		}
		return getI301DatasAtJudge().size();
	}
	
	//[4] add start
	public int getI301DatasAtExamSize() {
		if(getI301DatasAtExam() == null || getI301DatasAtExam().size() <= 0) {
			return 0;
		}
		return getI301DatasAtExam().size();
	}
	//[4] add end
	
	/**
	 * @return
	 */
	public Collection getI301DatasAtExam() {return i301DatasAtExam;}

	/**
	 * @return
	 */
	public Collection getI301DatasAtJudge() {return i301DatasAtJudge;}

	/**
	 * @param collection
	 */
	public void setI301DatasAtExam(Collection collection) {i301DatasAtExam = collection;}

	/**
	 * @param collection
	 */
	public void setI301DatasAtJudge(Collection collection) {i301DatasAtJudge = collection;}

	/**
	 * @return
	 */
	public String[] getUnivValueAtExam() {
		return univValueAtExam;
	}

	/**
	 * @return
	 */
	public String[] getUnivValueAtJudge() {
		
		// 模試で記入された大学での登録あり⇒重複を省く
		if (getUnivValueAtExam() != null) {
			return CollectionUtil.remove(univValueAtJudge, getUnivValueAtExam());
		} else {
			return univValueAtJudge;
		}
	}

	/**
	 * @param strings
	 */
	public void setUnivValueAtExam(String[] strings) {
		univValueAtExam = strings;
	}

	/**
	 * @param strings
	 */
	public void setUnivValueAtJudge(String[] strings) {
		univValueAtJudge = strings;
	}

	/**
	 * @return
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @param string
	 */
	public void setExamCd(String string) {
		examCd = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

	/**
	 * @return
	 */
	public String getExamName() {
		return examName;
	}

	/**
	 * @param string
	 */
	public void setExamName(String string) {
		examName = string;
	}

	/**
	 * @return
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param string
	 */
	public void setErrorMessage(String string) {
		errorMessage = string;
	}

	/**
	 * @return
	 */
	public String getExamDiv() {
		return examDiv;
	}

	/**
	 * @param string
	 */
	public void setExamDiv(String string) {
		examDiv = string;
	}

	/**
	 * @return
	 */
	public String getClassYear() {
		return classYear;//[2] add
	}

	/**
	 * @param string
	 */
	public void setClassYear(String string) {
		classYear = string;//[2] add
	}
	
	//[3] add start
	/**
	 * 対象模試コードを取得します
	 */
	public String getTargetExamCd() {
		return targetExamCd;
	}
	/**
	 * 対象模試年度を取得します
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}
	/**
	 * 対象模試コードを設定します
	 */
	public void setTargetExamCd(String string) {
		targetExamCd = string;
	}
	/**
	 * 対象模試年度を設定します
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}
	//[3] add end

	/**
	 * @param pNewestExamDiv 設定する newestExamDiv
	 */
	public void setNewestExamDiv(NewestExamDivSearchBean pNewestExamDiv) {
		newestExamDiv = pNewestExamDiv;
	}

}

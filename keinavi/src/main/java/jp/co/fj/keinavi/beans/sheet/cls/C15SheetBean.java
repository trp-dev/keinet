package jp.co.fj.keinavi.beans.sheet.cls;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.excel.cls.C15;
import jp.co.fj.keinavi.excel.data.cls.C15Item;
import jp.co.fj.keinavi.excel.data.cls.C15ListBean;
import jp.co.fj.keinavi.excel.data.cls.C15PersonalDataListBean;
import jp.co.fj.keinavi.excel.data.cls.C15ShiboListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;
import jp.co.fj.keinavi.util.sql.SQLUtil;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * クラス成績分析 - クラス成績概況（志望大学別個人評価）
 *
 * 2004.08.04	[新規作成]
 *
 * 2005.02.28	Yoshimoto KAWAI - TOTEC
 *				担当クラスの年度対応
 *
 * 2005.04.06	Yoshimoto KAWAI - TOTEC
 * 				出力種別フラグ対応
 *
 * 2009.10.21	Shoji HASE -TOTEC
 *				UNIVDIVCD(１桁)→UNIDIV（２桁）変更にともない、条件分岐定数の変更
 *
 * <2010年度改修>
 * 2010.01.06   Tomohisa YAMADA - TOTEC
 *              センターリサーチ５段階評価対応
 *
 * <大規模改修>
 * 2016.01.21 	Hiroyuki Nishiyama - QuiQsoft
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class C15SheetBean extends AbstractSheetBean {

	// データクラス
	private final C15Item data = new C15Item();

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// 現在の年度
		final String current = ProfileUtil.getChargeClassYear(profile);
		// 高1・2大学マスタを利用するか
		final boolean isUniv12Exam = KNUtil.isUniv12Exam(exam);

		// 評価基準
		String[] crit = (String[])item.get(IProfileItem.UNIV_CRIT);

		if (crit != null) {

		    List container = new LinkedList();

			// センターリサーチなら評価はABCDEではなく◎○△▲
			if (KNUtil.isCenterResearch(exam)) {
			    for (int i=0; i<crit.length && crit[i].length() > 0; i++) {
			        switch (crit[i].charAt(0)) {
			        	case 'A': container.add("◎"); break;
			        	case 'B': container.add("○"); break;
			        	case 'C': container.add("△"); break;
			        	case 'D': container.add("▲"); break;
			        }
			    }
			}

			//センターリサーチは◎形式とＡ形式が混在するため
			//両形式を追加しておく
			container.addAll(Arrays.asList(crit));

		    crit = (String[]) container.toArray(new String[0]);
		}

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrYear(exam.getExamYear()); // 対象年度
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setStrMshCd(exam.getExamCD()); // 模試コード
		data.setIntDispFlg(getIntFlag(IProfileItem.UNIV_DIV)); // 表示フラグ
		data.setIntSecuFlg(getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(getOutputType()); // 出力種別フラグ

		// ワークテーブルへデータを入れる
		insertIntoCountChargeClass();
		insertIntoCountUniv();

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		PreparedStatement ps6= null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		ResultSet rs4 = null;
		ResultSet rs5 = null;
		ResultSet rs6 = null;
		try {
			// 担当クラス
			ps1 = conn.prepareStatement(QueryLoader.getInstance().getQuery("c11_1"));

			// 志望大学リスト
			final Query query1;
			if (!isUniv12Exam) {
				query1 = QueryLoader.getInstance().load("c15_1a");
			} else {
				query1 = QueryLoader.getInstance().load("c15_1b");
			}

			// 国私区分
			if (data.getIntDispFlg() == 2) { // 国立
				query1.append("AND (univdivcd = '01' OR univdivcd = '02') ");
			} else if (data.getIntDispFlg() == 3) { // 私立・その他
				query1.append("AND univdivcd <> '01' AND univdivcd <> '02' ");
			}

			// ソート
			// 2016/01/21 QQ)Nishiyama 大規模改修 UPD START
			/*
			switch (super.getIntFlag(IProfileItem.UNIV_ORDER)) {
				case  1: query1.append("ORDER BY univdiv, dispsequence, facultycd, schedulecd"); break;
				case  2: query1.append("ORDER BY univdiv, univcd, facultycd, schedulecd"); break;
				case  3: query1.append("ORDER BY univdiv, count DESC, dispsequence, facultycd, schedulecd"); break;
			}
			*/

			query1.append (" ORDER BY ");

			// ====================
			// *-*-*   大学   *-*-*
			// ====================
			query1.append ("    univdiv");				// 大学区分（大学コードの先頭１桁の変換結果）

			switch (super.getIntFlag(IProfileItem.UNIV_ORDER)) {
				// 選択順
				case  1:
					query1.append ("  , dispsequence");		// 表示順
					break;
				// 大学区分・名称順
				case  2:
					query1.append ("  , univname_kana");	// 大学名カナ
					query1.append ("  , univcd");			// 大学コード
					break;
				// 志望者数順
				case  3:
					query1.append ("  , count DESC");		// 志望者数（降順）
					query1.append ("  , univname_kana");	// 大学名カナ
					query1.append ("  , univcd");			// 大学コード
					break;
			}

			// ====================
			// *-*-*   学部   *-*-*
			// ====================
			query1.append ("  , uninightdiv");			// 大学夜間部区分
			query1.append ("  , facultyconcd");			// 学部内容コード

			query1.append ("  , schedulecd");			// 日程コード
			// 2016/01/21 QQ)Nishiyama 大規模改修 UPD END

			ps2 = conn.prepareStatement(query1.toString());
			ps2.setString(1, login.getUserID()); // 学校コード
			ps2.setString(2, current); // 現在の年度
			ps2.setString(5, exam.getExamYear()); // 模試年度
			ps2.setString(6, exam.getExamCD()); // 模試コード
			ps2.setString(7, exam.getExamDiv()); // 模試区分コード
			ps2.setString(8, login.getUserID()); // 学校コード
			ps2.setString(9, current); // 現在の年度
			ps2.setString(11, exam.getExamYear()); // 模試年度
			ps2.setString(12, exam.getExamCD()); // 模試コード
			ps2.setString(13, exam.getExamDiv()); // 模試区分コード

			// 分析対象の絞込み
			Query query2 = null;
			switch (getIntFlag(IProfileItem.UNIV_SELECTION)) {
				case 11: // 志望上位10校
					query2 = QueryLoader.getInstance().load("c15_2_1");	break;

				case 12: // 志望者数
					query2 = QueryLoader.getInstance().load("c15_2_2");

					// 志望者数の範囲
					switch (super.getIntFlag(IProfileItem.UNIV_CHOICE)) {
						case 1: query2.append("HAVING COUNT(individualid) >= 5"); break;
						case 2: query2.append("HAVING COUNT(individualid) >= 10"); break;
						case 3: query2.append("HAVING COUNT(individualid) >= 15"); break;
						case 4: query2.append("HAVING COUNT(individualid) >= 20"); break;
					}

					break;

				case 13: // 評価基準
					query2 = QueryLoader.getInstance().load("c15_2_3");

					if (crit != null && crit.length > 0) {
						query2.setStringArray(1, crit);
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//						//2019/09/06 QQ)Oosaki 共通テスト対応 UPD START
//						//共通テストリサーチ
//						if ("38".equals(exam.getExamCD())) {
//						    query2.append("WHERE centerrating_engpt IN(#) ");
//						    query2.append(" AND NOT");
//						    query2.append(" (");
//						    query2.append(" app_qual_judge = '×'");
//						    query2.append(" AND TRIM(app_qual_notice) IS NULL ");
//						    query2.append(" )");
//						// マーク系
//						//if ("01".equals(exam.getExamTypeCD())) {
//						//	query2.append("WHERE centerrating IN(#) ");
//						} else if ("01".equals(exam.getExamTypeCD())) {
//						      query2.append("WHERE centerrating IN(#) ");
//						//2019/09/06 QQ)Oosaki 共通テスト対応 UPD END
						if ("01".equals(exam.getExamTypeCD())) {
							query2.append("WHERE centerrating IN(#) ");
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END

						// 記述系
						} else {
							query2.append("WHERE secondrating IN(#) ");
						}
					}

					query2.append("GROUP BY	univcd");

					break;
			}

			if (query2 != null) {
				ps3 = conn.prepareStatement(query2.toString());
				ps3.setString(1, login.getUserID()); // 学校コード
				ps3.setString(2, current); // 現在の年度
				ps3.setString(5, exam.getExamYear()); // 模試年度
				ps3.setString(6, exam.getExamCD()); // 模試コード
				ps3.setString(7, login.getUserID()); // 学校コード
				ps3.setString(8, current); // 現在の年度
				ps3.setString(10, exam.getExamYear()); // 模試年度
				ps3.setString(11, exam.getExamCD()); // 模試コード
			}

			// 個人データリスト
			final Query query3;
			if (!isUniv12Exam) {
				query3 = QueryLoader.getInstance().load("c15_3a");
			} else {
				query3 = QueryLoader.getInstance().load("c15_3b");
			}

			// 絞り込み条件
			final StringBuffer cond = new StringBuffer();

			// 評価基準での絞り込み
			if (getIntFlag(IProfileItem.UNIV_SELECTION) == 13) {
				if (crit != null && crit.length > 0) {
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//				  //2019/09/06 QQ)Oosaki 共通テスト対応 UPD START
//				    //共通テストリサーチ
//				    if ("38".equals(exam.getExamCD())) {
//				        cond.append("AND cr.centerrating_engpt IN(");
//				        cond.append(SQLUtil.concatComma(crit));
//				        cond.append(") ");
//				        cond.append(" AND ( NOT");
//				        cond.append(" (");
//				        cond.append(" cr.app_qual_judge = '×'");
//				        cond.append(" AND TRIM(cr.app_qual_notice) IS NULL ");
//				        cond.append(" )");
//				        cond.append(" OR ");
//				        cond.append(" cr.app_qual_judge IS NULL)");
//					// マーク系
//					//if ("01".equals(exam.getExamTypeCD())) {
//					//	cond.append("AND cr.centerrating IN(");
//					//	cond.append(SQLUtil.concatComma(crit));
//					//	cond.append(") ");
//				    } else if ("01".equals(exam.getExamTypeCD())) {
//				        cond.append("AND cr.centerrating IN(");
//				        cond.append(SQLUtil.concatComma(crit));
//				        cond.append(") ");
//				//2019/09/06 QQ)Oosaki 共通テスト対応 UPD END
					// マーク系
					if ("01".equals(exam.getExamTypeCD())) {
						cond.append("AND cr.centerrating IN(");
						cond.append(SQLUtil.concatComma(crit));
						cond.append(") ");
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END
					// 記述系
					} else {
						cond.append("AND cr.secondrating IN(");
						cond.append(SQLUtil.concatComma(crit));
						cond.append(") ");
					}
				}
			}

			query3.replaceAll("#", cond.toString());

			// ソート
			switch (getIntFlag(IProfileItem.UNIV_CHOICE_ORDER)) {
				case  1: // クラス番号順
					query3.append("ORDER BY grade_seq, class, class_no, deptsortkey");
					break;
				case  2: // 評価順
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//				  //2019/09/06 QQ)Oosaki 共通テスト対応 UPD START
//                                  //共通テストリサーチ
//				    if ("38".equals(exam.getExamTypeCD())) {
//                                        query3.append("ORDER BY centerrating_engpt, second_seq, grade_seq, class, class_no, deptsortkey");
//					// マーク系
//					//if ("01".equals(exam.getExamTypeCD())) {
//					//	query3.append("ORDER BY center_seq, second_seq, grade_seq, class, class_no, deptsortkey");
//				    } else if ("01".equals(exam.getExamTypeCD())) {
//				        query3.append("ORDER BY center_seq, second_seq, grade_seq, class, class_no, deptsortkey");
//				  //2019/09/06 QQ)Oosaki 共通テスト対応 UPD END
					// マーク系
					if ("01".equals(exam.getExamTypeCD())) {
						query3.append("ORDER BY center_seq, second_seq, grade_seq, class, class_no, deptsortkey");
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END
					// 記述系
					} else {
						query3.append("ORDER BY second_seq, grade_seq, class, class_no, deptsortkey");
					}
					break;
			}

			// バインド変数のズレ補正
			final int offset;
			if (isUniv12Exam) {
				offset = 1;
			} else {
				offset = 0;
			}

			ps4 = conn.prepareStatement(query3.toString());
			ps4.setString(1, exam.getExamYear()); // 模試年度
			ps4.setString(2, exam.getExamCD()); // 模試コード
			ps4.setString(3, login.getUserID()); // 学校コード
			ps4.setString(4, current); // 現在の年度
			ps4.setString(7, exam.getExamDiv()); // 模試区分コード
			ps4.setString(11 - offset, exam.getExamYear()); // 模試年度
			ps4.setString(12 - offset, exam.getExamCD()); // 模試コード
			ps4.setString(13 - offset, login.getUserID()); // 学校コード
			ps4.setString(14 - offset, current); // 現在の年度
			ps4.setString(16 - offset, exam.getExamDiv()); // 模試区分コード

			// 高1・2模試の場合、出願予定は判断しない
			if (!isUniv12Exam) {
				// 出願予定（国公立）
				ps5 = conn.prepareStatement(QueryLoader.getInstance().getQuery("c15_4_1"));
				ps5.setString(1, exam.getExamYear()); // 模試年度
				ps5.setString(2, exam.getExamCD()); // 模試コード
				ps5.setString(3, exam.getExamDiv()); // 模試区分コード
				// 出願予定（私立・その他）
				ps6 = conn.prepareStatement(QueryLoader.getInstance().getQuery("c15_4_2"));
				ps6.setString(1, exam.getExamYear()); // 模試年度
				ps6.setString(2, exam.getExamCD()); // 模試コード
				ps6.setString(3, exam.getExamDiv()); // 模試区分コード
			}

			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				// データリスト
				C15ListBean bean = new C15ListBean();
				bean.setStrGrade(rs1.getString(1));
				bean.setStrClass(rs1.getString(3));
				data.getC15List().add(bean);

				// 分析対象の絞込み
				final Set univSet = new HashSet(); // フィルタ

				if (ps3 != null) {
					// 学年
					ps3.setString(3, rs1.getString(1));
					// クラス
					ps3.setString(4, rs1.getString(2));
					ps3.setString(9, rs1.getString(2));

					rs3 = ps3.executeQuery();
					while (rs3.next()) univSet.add(rs3.getString(1));
					rs3.close();
				}

				// 学年
				ps2.setString(3, rs1.getString(1));
				ps4.setString(5, rs1.getString(1));
				// クラス
				ps2.setString(4, rs1.getString(2));
				ps2.setString(10, rs1.getString(2));
				ps4.setString(6, rs1.getString(2));
				ps4.setString(15 - offset, rs1.getString(2));

				rs2 = ps2.executeQuery();
				while (rs2.next()) {
					// 絞込み対象かどうか
					if (query2 != null && !univSet.contains(rs2.getString(1))) {
						continue;
					}

					C15ShiboListBean s = new C15ShiboListBean();
					s.setStrDaigakuCd(rs2.getString(1));
					s.setStrDaigakuMei(rs2.getString(2));
					s.setStrGakubuCd(rs2.getString(3));
					s.setStrGakubuMei(rs2.getString(4));
					s.setStrNtiCd(rs2.getString(5));
					s.setStrNittei(rs2.getString(6));
					bean.getC15ShiboList().add(s);

					// 大学コード
					ps4.setString(8, s.getStrDaigakuCd());
					ps4.setString(17 - offset, s.getStrDaigakuCd());
					// 学部コード
					ps4.setString(9, s.getStrGakubuCd());
					ps4.setString(18 - offset, s.getStrGakubuCd());
					// 日程コード
					if (!isUniv12Exam) {
						ps4.setString(10, s.getStrNtiCd());
						ps4.setString(19, s.getStrNtiCd());
					}

					rs4 = ps4.executeQuery();
					while (rs4.next()) {
						C15PersonalDataListBean p = new C15PersonalDataListBean();
						p.setStrGrade(rs4.getString(1));
						p.setStrClass(rs4.getString(2));
						p.setStrClassNo(rs4.getString(3));
						p.setStrKanaName(rs4.getString(4));
						p.setStrSex(rs4.getString(5));
						p.setStrGakkaCd(rs4.getString(6));
						p.setStrGakkaMei(rs4.getString(7));
						p.setStrNittei(rs4.getString(8));
						p.setStrHyoukaMark(rs4.getString(9));
						p.setStrHyoukaKijyutsu(rs4.getString(10));
						p.setStrHyoukaAll(rs4.getString(11));
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 DEL START
//						//2019/09/06 QQ)Oosaki 共通テスト対応 ADD START
//						p.setStrHyoukaMarkEngPt(rs4.getString(18));
//						p.setStrAppQuelJudge(rs4.getString(19));
//						p.setStrAppQuelNotice(rs4.getString(20));
//						//2019/09/06 QQ)Oosaki 共通テスト対応 ADD END
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 DEL END

						s.getC15PersonalDataList().add(p);

						// 高1・2模試の場合、出願予定は判断しない
						if (!isUniv12Exam) {
							// 出願予定（国公立）
							// 前期・中期・後期でそれぞれ志望順位が最上位のもの
							if ("C".equals(s.getStrNtiCd())
								|| "D".equals(s.getStrNtiCd())
								|| "E".equals(s.getStrNtiCd())) {

								ps5.setString(4, rs4.getString(12));

								rs5 = ps5.executeQuery();
								while (rs5.next()) {
									if (rs5.getInt(1) == rs4.getInt(13)) {
										p.setIntYoteiFlg(1);
									}
								}
								rs5.close();

							// 出願予定（私立・その他）
							} else {
								ps6.setString(4, rs4.getString(12));

								rs6 = ps6.executeQuery();
								if (rs6.next()) {
									if (rs6.getInt(1) == rs4.getInt(13)) {
										p.setIntYoteiFlg(1);
									}
								}
								rs6.close();
							}
						}
					}
					rs4.close();

					// カウント
					s.setIntSoshibo(s.getC15PersonalDataList().size());

					Iterator ite = s.getC15PersonalDataList().iterator();
					while (ite.hasNext()) {
						C15PersonalDataListBean p = (C15PersonalDataListBean) ite.next();

						// 出願予定人数
						if (p.getIntYoteiFlg() == 1) {
							s.setIntYoteiNinzu(s.getIntYoteiNinzu() + 1);
						}

						// マーク系
						if ("01".equals(exam.getExamTypeCD())) {
							if ("A".equals(p.getStrHyoukaMark()) || "◎".equals(p.getStrHyoukaMark())) {
								s.setIntHyoukaA(s.getIntHyoukaA() + 1);
							} else if ("B".equals(p.getStrHyoukaMark()) || "○".equals(p.getStrHyoukaMark())) {
								s.setIntHyoukaB(s.getIntHyoukaB() + 1);
							} else if ("C".equals(p.getStrHyoukaMark()) || "△".equals(p.getStrHyoukaMark())) {
								s.setIntHyoukaC(s.getIntHyoukaC() + 1);
							} else if ("D".equals(p.getStrHyoukaMark()) || "▲".equals(p.getStrHyoukaMark())) {
								s.setIntHyoukaD(s.getIntHyoukaD() + 1);
							} else if ("E".equals(p.getStrHyoukaMark())) {
								s.setIntHyoukaE(s.getIntHyoukaE() + 1);
							// NULLなら記述系としてカウントする
							} else if (p.getStrHyoukaMark() == null){
								countSecondRating(p, s);
							}
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 DEL START
//							//2019/09/06 QQ)Oosaki 共通テスト対応 ADD START
//							//共通テスト評価人数(英語認定試験含む)をカウント
//							if ("A".equals(p.getStrHyoukaMarkEngPt())) {
//							    s.setIntRatingnumA(s.getIntRatingnumA() + 1);
//							} else if ("B".equals(p.getStrHyoukaMarkEngPt())) {
//							    s.setIntRatingnumB(s.getIntRatingnumB() + 1);
//							} else if ("C".equals(p.getStrHyoukaMarkEngPt())) {
//                                                            s.setIntRatingnumC(s.getIntRatingnumC() + 1);
//                                                        } else if ("D".equals(p.getStrHyoukaMarkEngPt())) {
//                                                            s.setIntRatingnumD(s.getIntRatingnumD() + 1);
//                                                        } else if ("E".equals(p.getStrHyoukaMarkEngPt())) {
//                                                            s.setIntRatingnumE(s.getIntRatingnumE() + 1);
//                                                        }
//							//2019/09/06 QQ)Oosaki 共通テスト対応 ADD END
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 DEL END
						// 記述系
						} else {
							countSecondRating(p, s);
						}
					}
				}
				rs2.close();
			}
		} finally {
			DbUtils.closeQuietly(null, ps1, rs1);
			DbUtils.closeQuietly(null, ps2, rs2);
			DbUtils.closeQuietly(null, ps3, rs3);
			DbUtils.closeQuietly(null, ps4, rs4);
			DbUtils.closeQuietly(null, ps5, rs5);
			DbUtils.closeQuietly(null, ps6, rs6);
		}
	}

	// 記述評価のカウントを行う
	private void countSecondRating(C15PersonalDataListBean p, C15ShiboListBean s) {
		if ("A".equals(p.getStrHyoukaKijyutsu()) || "◎".equals(p.getStrHyoukaKijyutsu())) {
			s.setIntHyoukaA(s.getIntHyoukaA() + 1);
		} else if ("B".equals(p.getStrHyoukaKijyutsu()) || "○".equals(p.getStrHyoukaKijyutsu())) {
			s.setIntHyoukaB(s.getIntHyoukaB() + 1);
		} else if ("C".equals(p.getStrHyoukaKijyutsu()) || "△".equals(p.getStrHyoukaKijyutsu())) {
			s.setIntHyoukaC(s.getIntHyoukaC() + 1);
		} else if ("D".equals(p.getStrHyoukaKijyutsu()) || "▲".equals(p.getStrHyoukaKijyutsu())) {
			s.setIntHyoukaD(s.getIntHyoukaD() + 1);
		} else if ("E".equals(p.getStrHyoukaKijyutsu())) {
			s.setIntHyoukaE(s.getIntHyoukaE() + 1);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("C15_01");
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.C_COND_UNIV;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new C15().c15(data, outfileList, getAction(), sessionKey, sheetLog);
	}

}

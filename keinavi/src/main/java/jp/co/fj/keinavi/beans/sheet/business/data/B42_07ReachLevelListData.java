package jp.co.fj.keinavi.beans.sheet.business.data;

import jp.co.fj.keinavi.beans.sheet.school.data.S42_15ReachLevelListData;

/**
 *
 * センター到達指標・到達レベルリスデータ
 * 
 * 2006.08.31	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class B42_07ReachLevelListData extends S42_15ReachLevelListData {
}

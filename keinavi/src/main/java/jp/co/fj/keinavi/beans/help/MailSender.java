/*
 * 作成日: 2004/09/15
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.help;

// メール関係
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.Date;
import java.util.Vector;
import javax.mail.*;
import javax.mail.Session;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.InternetAddress;
import javax.mail.Address;

import jp.co.fj.keinavi.util.KNCommonProperty;


/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class MailSender {
	
	/**
	*
	* ヘルプ評価メッセージメール送信
	* 
	* @param toTransmit     送信先アドレス指定
	*                          "Help"    :ヘルプ用アドレス
	*                          "Inquire" :お問い合わせ用アドレス 
	*
	* @param mailContents   ヘルプメッセージ内容
	*                          (0) ヘルプID
	* 	                       (1) 評価
	* 	                       (2) コメント
	* 	                       (3) お名前
	* 	                       (4) ユーザのＩＤ
	*                       お問い合わせメッセージ内容
	*                          (0) タイトル
	* 	                       (1) 本文
	* 	                       (2) 学校名
	* 	                       (3) お名前
	* 	                       (4) ご連絡先  電話番号
	* 	                       (5) ご連絡先  メールアドレス
	* 	                       (6) ユーザのＩＤ
	*
	* 	* @return void
	*/

	// ------------------------------------------------------------
	// 2004.11.18
	// Yoshimoto KAWAI - Totec
	// HTMLメール？になるので改行は改行コードとする
	//
	// -- 改行
	//private static final String pos = "<BR>";
	private static final String pos = "\r\n";
	// ------------------------------------------------------------
	private static final String FROM_ADDR = "navi@keinet.ne.jp";
	
	public static void sendMessage(String toTransmit, Vector mailContents) throws  MessagingException  {

		String smtp_host = "";
		String smtp_port = "";
		String toMailAdr = "";
		//String fromMailAdr =  "";

		// 本番用アドレス
		smtp_host = getSMTPServer();
		smtp_port = "25";
		if ( toTransmit.equals("Help") ) {
			toMailAdr = getToHelpMailAdr();
		} else {
			toMailAdr = getToInquireMailAdr();
		}
		//fromMailAdr =  "";

		// テスト用アドレス
		//smtp_host = "smtp.nifty.com";
		//smtp_port = "25";
		//toMailAdr = "atsuyuki.ninomiya@nifty.ne.jp";
		//fromMailAdr = "";

		// メッセージ内容編集
		String title   = null;
		String message = null;
		String name    = null;
		if ( toTransmit.equals("Help") ) {
			title   = "ヘルプについてのご意見（ヘルプID：" + (String) mailContents.get(0) + "）";
			message = "ヘルプについてのご意見（ヘルプID：" + (String) mailContents.get(0) + "）" + pos 
					+ "【評価】"              + pos + (String) mailContents.get(1) + pos
					+ "【コメント】"          + pos + (String) mailContents.get(2) + pos
					+ "【お名前】"            + pos + (String) mailContents.get(3) + pos
					+ "【ユーザＩＤ】"  + pos + (String) mailContents.get(4) + pos; 
			name    = (String) mailContents.get(3);
			//fromMailAdr = mailContents.get(4) + "@keinavi";
		} else {
			title   = (String) mailContents.get(0);
			message = "【タイトル】"          + pos + (String) mailContents.get(0) + pos 
					+ "【本文】"              + pos + (String) mailContents.get(1) + pos
					+ "【学校名】"            + pos + (String) mailContents.get(2) + pos
					+ "【お名前】"            + pos + (String) mailContents.get(3) + pos
					+ "【電話番号】"          + pos + (String) mailContents.get(4) + pos
			        + "【メールアドレス】"    + pos + (String) mailContents.get(5) + pos
					+ "【ユーザＩＤ】"  + pos + (String) mailContents.get(6) + pos; 
			name    = (String) mailContents.get(3);
			//fromMailAdr = mailContents.get(6) + "@keinavi";
		}

		Properties props = System.getProperties();
		// SMTPサーバーのアドレスを指定
		props.put("mail.smtp.host", smtp_host);
		props.put("mail.smtp.port", smtp_port);

		Session session = Session.getDefaultInstance(props, null);
		MimeMessage mimeMessage = new MimeMessage(session);
		// 送信元メールアドレスと送信者名を指定
		try {
			mimeMessage.setFrom(
				new InternetAddress(
					FROM_ADDR,
					name,
					"iso-2022-jp")
					);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		// 送信先メールアドレスを指定
		Address address[] = InternetAddress.parse(toMailAdr);
		mimeMessage.addRecipients(Message.RecipientType.TO, address);

		// メールのタイトルを指定
		mimeMessage.setSubject(title, "iso-2022-jp");
		// メールの内容を指定
		mimeMessage.setText(message, "iso-2022-jp");
		// メールの形式を指定
		//mimeMessage.setHeader("Content-Type", "text/html");
		mimeMessage.setHeader("Content-Type", "text/plain; charset=\"iso-2022-jp\"");
		// 送信日付を指定
		mimeMessage.setSentDate(new Date());
		// 送信します
		Transport.send(mimeMessage);
	}

	/**
	 * SMTPサーバーアドレス
	 * @return
	 */
	private static String getSMTPServer() {
		String smtpServer = null;
		smtpServer = "SMTP.keinet.ne.jp";
		try {
		    smtpServer = KNCommonProperty.getSMTPServer();
		} catch (Exception e) {
		}
		return smtpServer;
	}

	/**
	 * ヘルプ用送信先メールアドレス
	 * @return
	 */
	private static String getToHelpMailAdr() {
		String toMailAdr = null;
		try {
			toMailAdr = KNCommonProperty.getHelpCommentAddr();
		} catch (Exception e) {
		}
		return toMailAdr;
	}

	/**
	 * お問い合わせ用送信先メールアドレス
	 * @return
	 */
	private static String getToInquireMailAdr() {
		String toMailAdr = null;
		try {
			toMailAdr = KNCommonProperty.getInquireAddr();
		} catch (Exception e) {
		}
		return toMailAdr;
	}
	

}

package jp.co.fj.keinavi.beans.com_set;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.UnivDivBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.com_set.cm.UnivData;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 共通項目設定 - 志望大学Bean
 *
 * 2004.06.29	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class UnivBean extends AbstractComBean {

	private final List univList = new ArrayList(); // 大学オブジェクトのリスト

	private String[] univCodeArray; // 大学コードの配列
	private ExamData exam; // 対象模試
	private String bundleCD; //一括コード

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// 一般・全選択
			if (univCodeArray == null) {
				final List c1 = new LinkedList(); // 国公立の入れ物
				final List c2 = new LinkedList(); // 私立・その他の入れ物

				// SQL取得
				final Query query = QueryLoader.getInstance().load("cm08");
				KNUtil.rewriteUnivQuery(query, exam);

				ps = conn.prepareStatement(query.toString());
				ps.setString(1, exam.getExamDiv()); // 模試区分
				ps.setString(2, exam.getExamYear()); // 模試年度
				ps.setString(3, exam.getExamCD()); // 模試コード
				ps.setString(4, bundleCD); // 一括コード
				ps.setString(5, KNUtil.getRatingDiv(exam)); // 評価区分

				rs = ps.executeQuery();
				while (rs.next()) {
					UnivData data = new UnivData();
					data.setUnivCode(rs.getString(1));
					data.setUnivName(rs.getString(2));
					data.setUnivDiv(rs.getString(3));
					data.setLocation(rs.getString(4));
					data.setChoiceNum(rs.getInt(5));

					// 国公立
					if (UnivDivBean.isKokkouritsuUniv(data.getUnivDiv())) c1.add(data);
					// 私立・その他
					else c2.add(data);
				}

				// 共に10以上
				if (c1.size() >= 10 && c2.size() >= 10) {
					univList.addAll(c1.subList(0, 10));
					univList.addAll(c2.subList(0, 10));

				// 国公立が10以下
				} else if (c1.size() < 10 && c2.size() >= 10) {
					univList.addAll(c1);
					univList.addAll(c2.subList(0, 10));

				// 私立・その他が10以下
				} else if (c1.size() >= 10 && c2.size() < 10) {
					univList.addAll(c1.subList(0, 10));
					univList.addAll(c2);

				// 共に10以下
				} else {
					univList.addAll(c1);
					univList.addAll(c2);
				}

			// 個別選択
			} else if (univCodeArray.length > 0) {

				final List container = new ArrayList();

				// 営業
				if (bundleCD == null) {
					final Query query = QueryLoader.getInstance().load("cm05_1");
					query.setStringArray(1, univCodeArray); // 大学コードの配列
					KNUtil.rewriteUnivQuery(query, exam); // 書き換え

					ps = conn.prepareStatement(query.toString());
					ps.setString(1, exam.getExamYear()); // 模試年度
					ps.setString(2, exam.getExamDiv()); // 模試区分コード
					ps.setString(3, exam.getExamCD()); // 模試コード
					ps.setString(4, KNUtil.getRatingDiv(exam)); // 評価区分
				// 一般
				} else {
					final Query query = QueryLoader.getInstance().load("cm05_2");
					query.setStringArray(1, univCodeArray); // 大学コードの配列
					KNUtil.rewriteUnivQuery(query, exam); // 書き換え

					ps = conn.prepareStatement(query.toString());
					ps.setString(1, exam.getExamYear()); // 模試年度
					ps.setString(2, exam.getExamDiv()); // 模試区分コード
					ps.setString(3, exam.getExamCD()); // 模試コード
					ps.setString(4, bundleCD); // 一括コード
					ps.setString(5, KNUtil.getRatingDiv(exam)); // 評価区分
				}

				rs = ps.executeQuery();
				while (rs.next()) {
					UnivData data = new UnivData();
					data.setUnivCode(rs.getString(1));
					data.setUnivName(rs.getString(2));
					data.setUnivDiv(rs.getString(3));
					data.setLocation(rs.getString(4));
					data.setChoiceNum(rs.getInt(5));
					// 2016/01/18 QQ)Nishiyama 大規模改修 ADD START
					data.setUnivNameKana(rs.getString(6));
					// 2016/01/18 QQ)Nishiyama 大規模改修 ADD END
					container.add(data);
				}

				// 大学コードの配列の順序で格納する
				for (int i=0; i<univCodeArray.length; i++) {
					UnivData data = new UnivData(univCodeArray[i]);
					int index = container.indexOf(data);
					if (index >= 0) univList.add(container.get(index));
					else univList.add(data);
				}
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.com_set.AbstractComBean#getFullList()
	 */
	public List getFullList() {
		return univList;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.com_set.AbstractComBean#getFullListSize()
	 */
	public int getFullListSize() {
		return univList.size();
	}

	/**
	 * @see jp.co.fj.keinavi.beans.com_set.AbstractComBean#getPartList()
	 */
	public List getPartList() {
		return null;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.com_set.AbstractComBean#getPartListSize()
	 */
	public int getPartListSize() {
		return 0;
	}

	/**
	 * @param string
	 */
	public void setBundleCD(String string) {
		bundleCD = string;
	}

	/**
	 * @param strings
	 */
	public void setUnivCodeArray(String[] strings) {
		univCodeArray = strings;
	}

	/**
	 * @param pExam 設定する exam
	 */
	public void setExam(ExamData pExam) {
		exam = pExam;
	}

}

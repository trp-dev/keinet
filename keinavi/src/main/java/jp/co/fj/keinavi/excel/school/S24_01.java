package jp.co.fj.keinavi.excel.school;

//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S24HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.school.S24Item;
import jp.co.fj.keinavi.excel.data.school.S24ListBean;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * 作成日: 2004/07/13
 * @author C.Murata
 *
 * 校内成績分析−過年度比較−志望大学評価別人数　大学（日程なし）出力処理
 * 
 * <2010年度改修>
 * 2010.01.06   Tomohisa YAMADA - Totec
 *              センター・リサーチ５段階評価対応
 *              
 * 
 */
public class S24_01 {
	
	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow		= 80;			//MAX行数

	private boolean bolBookCngFlg = true;			//改ファイルフラグ	
	private boolean bolBookClsFlg = false;			//改ファイルフラグ	
	private boolean bolSheetCngFlg = true;			//改シートフラグ
	private int intDataStartRow = 7;					//データセット開始行
	
	final private String masterfile0 = "S24_01" ;
	final private String masterfile1 = "S24_01" ;
	private String masterfile = "" ;
	
	//2004.9.27 Add
	final private String strCenterResearch = "38";
	//2004.9.27 Add End

	/*
	 * 	Excel編集メイン
	 * 		S24Item s24Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int s24_01EditExcel(S24Item s24Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
			
			
		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFCell	workCell		= null;
		HSSFRow		workRow			= null;
		
		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intRow = 0;
		String strGenekiKbn = "";
		String strDaigakuCode = "";
		String strHyoukaKbn = "";
		
		int intLoopCnt = 0;
		int intNinzuListCnt = 0;
		int intSheetCngCount = 0;
		int intBookCngCount = 0;
		
		String strDispTarget = "";
		//2004.09.27Add
		String strKokushiKbn = "";
		String strStKokushiKbn = "";
		ArrayList HyoukaTitleList = new ArrayList();
		//2004.09.27AddEnd
		
		//テンプレートの決定
		if (s24Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S24ListBean s24ListBean = new S24ListBean();
		
		try{
			
			HyoukaTitleList.add(0, JudgementConstants.Judge.JUDGE_STR_A);
			HyoukaTitleList.add(1, JudgementConstants.Judge.JUDGE_STR_B);
			HyoukaTitleList.add(2, JudgementConstants.Judge.JUDGE_STR_C);
			HyoukaTitleList.add(3, JudgementConstants.Judge.JUDGE_STR_D);
			HyoukaTitleList.add(4, JudgementConstants.Judge.JUDGE_STR_E);

			//2004.9.27 Add End
			
			ArrayList s24List = s24Item.getS24List();
			ListIterator itr = s24List.listIterator();

			/**			
			//2004.9.28 Remove
			if( itr.hasNext() == false ){
				return errfdata;
			}
			//2004.9.28 Remove End
			 * 
			 */
			
			while( itr.hasNext() ){
				
				s24ListBean = (S24ListBean)itr.next();
				ArrayList s24HyoukaNinzuList = s24ListBean.getS24HyoukaNinzuList();
				
				if( !cm.toString(strGenekiKbn).equals(cm.toString(s24ListBean.getStrGenKouKbn())) ){
					strGenekiKbn = s24ListBean.getStrGenKouKbn();
					if( cm.toString(strGenekiKbn).equals("0") ){
						strDispTarget = "全体";
					}
					else if( cm.toString(strGenekiKbn).equals("1") ){
						strDispTarget = "現役生";
					}
					else if( cm.toString(strGenekiKbn).equals("2") ){
						strDispTarget = "高卒生";
					}

					strDaigakuCode = "";
					strHyoukaKbn = "";
					if( bolSheetCngFlg == false ){
						bolSheetCngFlg = true;
						intMaxSheetIndex++;
						if( intMaxSheetIndex >= intMaxSheetSr ){
							//改ファイル
							bolBookCngFlg = true; 
							bolBookClsFlg = true;
						}
					}
				}

				//2004.9.27 Add
				int intDaigakuCd = (int)Integer.parseInt(s24ListBean.getStrDaigakuCd());
				if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
					strKokushiKbn = "国公立大";
				}
				if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
					strKokushiKbn = "私立大";
				}
				if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
					strKokushiKbn = "短大";
				}
				if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
					strKokushiKbn = "その他";
				}
				
				if( !cm.toString(strKokushiKbn).equals(cm.toString(strStKokushiKbn)) ){
					bolSheetCngFlg = true;
					strStKokushiKbn = strKokushiKbn;
				}
				//2004.9.27 Add End
				
				if( bolSheetCngFlg == true ){
					
					//罫線出力（セルの上部に太線を出力）
//					cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
//														intRow, intRow, 0, 10, false, true, false, false);
					//2004.9.27 Add
					if( workSheet != null ){

						//罫線出力（セルの上部に太線を出力）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
															intRow, intRow, 0, 10, false, true, false, false);




						int intRemoveStartRow = ( intSheetCngCount * intNinzuListCnt ) + intDataStartRow;
						if( intMaxRow + intDataStartRow > intRemoveStartRow ){
							for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
								//行削除処理
								workSheet.removeRow(workSheet.getRow(index));
							}
							//罫線出力（セルの上部に太線を出力）
							cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
											intRemoveStartRow, intRemoveStartRow, 0, 10, false, true, false, false);
						}
					}
					//2004.9.27 Add End
					
					if( bolBookCngFlg == true ){
						if( bolBookClsFlg ){

							boolean bolRet = false;
							// Excelファイル保存
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);

							if( bolRet == false ){
								return errfwrite;
							}
							intBookCngCount++;
							bolBookClsFlg = false;
						}
						
						// マスタExcel読み込み
						workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
						//2004.9.28 Update 
//						if( workbook.equals(null) ){
						if( workbook == null ){
						//2004.9.28 Update End
							return errfread;
						}
					
						bolBookCngFlg = false;
						intMaxSheetIndex = 0;
					}
					
					//シート作成
					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);

					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);
				
					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, s24Item.getIntSecuFlg(), 8, 10 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 8 );
					workCell.setCellValue(secuFlg);
				
					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( "学校名　：" + cm.toString(s24Item.getStrGakkomei()) );
		
					// 模試月取得
					String moshi =cm.setTaisyouMoshi( s24Item.getStrMshDate() );
					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
					workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s24Item.getStrMshmei()) + moshi);
					
					//表示対象
					workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
					workCell.setCellValue( "表示対象：" + strDispTarget );
					
					//2004.9.27 Add 
					//評価別人数 見出し行
					workCell = cm.setCell( workSheet, workRow, workCell, 6, 6 );
					workCell.setCellValue( (String)HyoukaTitleList.get(0) );
								
					workCell = cm.setCell( workSheet, workRow, workCell, 6, 7 );
					workCell.setCellValue( (String)HyoukaTitleList.get(1) );
								
					workCell = cm.setCell( workSheet, workRow, workCell, 6, 8 );
					workCell.setCellValue( (String)HyoukaTitleList.get(2) );

					workCell = cm.setCell( workSheet, workRow, workCell, 6, 9 );
					workCell.setCellValue( (String)HyoukaTitleList.get(3) );

					workCell = cm.setCell( workSheet, workRow, workCell, 6, 10 );
					workCell.setCellValue( (String)HyoukaTitleList.get(4) );

					if( strStKokushiKbn.equals("国公立大") ){
						workCell = cm.setCell( workSheet, workRow, workCell, 6, 5 );
						workCell.setCellValue( "出願予定" );
					}
					else{
						workCell = cm.setCell( workSheet, workRow, workCell, 6, 5 );
						workCell.setCellValue( "第1志望" );
					}
					//2004.9.27 Add End
					
					intMaxSheetIndex++;		//シート数カウンタインクリメント
					bolSheetCngFlg = false;
					
					intRow = intDataStartRow;
					intLoopCnt = 0;
					
				}
				
				NumberFormat nf = NumberFormat.getInstance();
				intNinzuListCnt = (int)s24HyoukaNinzuList.size();			//評価別人数リスト データ数取得
				intSheetCngCount =nf.parse(nf.format(intMaxRow / intNinzuListCnt)).intValue(); 
				
				//評価区分セット
				if( !cm.toString(strHyoukaKbn).equals(cm.toString(s24ListBean.getStrHyouka())) ){
					String strHyoka = "";
					switch( Integer.parseInt(s24ListBean.getStrHyouka()) ){
						case 1:
							strHyoka = "センター";
							break;
						case 2:
							strHyoka = "2次・私大";
							break;
						case 3:
							strHyoka = "総合";
							break;
					}
					workCell = cm.setCell( workSheet, workRow, workCell, intRow, 1);
					workCell.setCellValue( strHyoka );
					strHyoukaKbn = s24ListBean.getStrHyouka();
					
					//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
					cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
														intRow, intRow, 1, 2, false, true, false, false);
					
				}

				//大学名セット
				if( !cm.toString(strDaigakuCode).equals(cm.toString(s24ListBean.getStrDaigakuCd()))){
					if( s24ListBean.getStrDaigakuMei()!=null ){
						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 0);
						workCell.setCellValue( s24ListBean.getStrDaigakuMei() );
					}
					strDaigakuCode = s24ListBean.getStrDaigakuCd();
					//2004.9.30 Add
					strHyoukaKbn = "";
					//2004.9.30 Add End
					
					//罫線出力(セルの上部に太線を引く）
					cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
														intRow, intRow, 0, 10, false, true, false, false );
					
				}
				
				//評価別人数データリストの内容をセット					
				boolean ret = setData( workSheet, s24HyoukaNinzuList, intRow );
				if( ret != true){
					return errfdata;
				}
				else{
					intRow += intNinzuListCnt;
					intLoopCnt++;
				}
				
				if( intLoopCnt >= intSheetCngCount ){
					//改シート
					//罫線出力（セルの上部に太線を出力）
					cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
														intRow, intRow, 0, 10, false, true, false, false);

					strDaigakuCode = "";															
					strHyoukaKbn = "";
					bolSheetCngFlg = true;
					if( intMaxSheetIndex >= intMaxSheetSr ){
						//改ファイル
						bolBookCngFlg = true; 
						bolBookClsFlg = true;
					}
				}
				
				
			}

			if( workbook != null ){
				/** 修正・改ファイルフラグがtrueでもfalseでも処理を実行する **/
				//罫線出力（セルの上部に太線を出力）
				cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
													intRow, intRow, 0, 10, false, true, false, false);
				//2004.9.27 Add
				if( workSheet != null ){
					int intRemoveStartRow = ( intSheetCngCount * intNinzuListCnt ) + intDataStartRow;
					if( intMaxRow + intDataStartRow > intRemoveStartRow ){
						for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
							//行削除処理
							workSheet.removeRow(workSheet.getRow(index));
						}
						//罫線出力（セルの上部に太線を出力）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
										intRemoveStartRow, intRemoveStartRow, 0, 10, false, true, false, false);
					}
				}
				//2004.9.27 Add End
			}

			//データ無し
//			if ( intBookCngCount == 0 ){
			if ( intMaxSheetIndex == 0 ){
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex = 1;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s24Item.getIntSecuFlg(), 8, 10 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 8 );
				workCell.setCellValue(secuFlg);
				
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s24Item.getStrGakkomei()) );
		
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s24Item.getStrMshDate() );
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s24Item.getStrMshmei()) + moshi);
					
				//表示対象
				workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
				workCell.setCellValue( "表示対象：" + strDispTarget );
			}

			// Excelファイル保存
			boolean bolRet = false;

			if(intBookCngCount > 0){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}
			if( bolRet == false ){
				return errfwrite;
					
			}
			
		}
		catch( Exception e ){
			return errfdata;
		}

		return noerror;
	}

			
		
	/*
	 * 	Excel編集
	 * 		HSSFWorkbook 	workbook: Excelワークブック
	 * 		ArrayList 		s24HyoukaNinzuList:評価別人数リスト
	 * 		int				intRow:開始行
	 * 		戻り値: なし
	 */
	private boolean setData(HSSFSheet workSheet, ArrayList s24HyoukaNinzuList, int intRow ){

		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		int		row = intRow;
		
		
		// 基本ファイルを読込む
		S24HyoukaNinzuListBean s24HyoukaNinzuListBean = new S24HyoukaNinzuListBean();

		try{
			
			Iterator itr = s24HyoukaNinzuList.iterator();
			
			while( itr.hasNext() ){
				
				//データリストより各項目を取得する
				s24HyoukaNinzuListBean = (S24HyoukaNinzuListBean)itr.next();

				//模試年度
				if( s24HyoukaNinzuListBean.getStrNendo()!=null ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 2);
					workCell.setCellValue( s24HyoukaNinzuListBean.getStrNendo() + "年度" );
				}
				
				//全国総志望者数
				if( s24HyoukaNinzuListBean.getIntSoshiboAll() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 3);
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntSoshiboAll() );
				}
				
				//志望者数（総志望）
				if( s24HyoukaNinzuListBean.getIntSoshiboHome() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 4);
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntSoshiboHome() );
				}
				
				//志望者数（第一志望）
				if( s24HyoukaNinzuListBean.getIntDai1shibo() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 5);
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntDai1shibo() );
				}
				
				//評価別人数
				if( s24HyoukaNinzuListBean.getIntHyoukaA() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 6 );
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntHyoukaA() );
				}

				if( s24HyoukaNinzuListBean.getIntHyoukaB() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 7 );
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntHyoukaB() );
				}

				if( s24HyoukaNinzuListBean.getIntHyoukaC() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 8 );
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntHyoukaC() );
				}

				if( s24HyoukaNinzuListBean.getIntHyoukaD() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 9 );
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntHyoukaD() );
				}

				if( s24HyoukaNinzuListBean.getIntHyoukaE() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 10 );
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntHyoukaE() );
				}
				
				row++;

			}
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
}

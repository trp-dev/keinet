package jp.co.fj.keinavi.beans.com_set;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.com_set.cm.CompYearData;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 比較対象年度Bean
 * 
 * 
 * 2004.06.29	[新規作成]
 * 
 * 2006.02.28	Yoshimoto KAWAI - TOTEC
 * 				校内成績処理システム対応
 * 				
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class CompYearBean extends DefaultBean {

	private List compYearList = new LinkedList(); // 年度オブジェクトのリスト
	private String targetYear; // 対象年度
	private String targetExam; // 対象模試
	private String bundleCd; // 一括コード
	private String schoolCd; // 学校コード

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// dummy...
		schoolCd.getClass();
		
		// 前年度〜6年前のオブジェクトを作る
		final int base = Integer.parseInt(targetYear) - 1;
		for (int i = 0; i < 6; i++) {
			// 比較対象年度オブジェクト
			compYearList.add(new CompYearData((base - i) + ""));
		}
		
		// 模試コード変換テーブルのセットアップ
		KNUtil.buildExamCdTransTable(conn,
				new ExamData[]{new ExamData(targetYear, targetExam)});

		// 受験人数の取得
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().load("cm10").toString());	
			ps.setString(1, bundleCd);
			ps.setString(3, targetExam);

			for (final Iterator ite = compYearList.iterator(); ite.hasNext();) {
				final CompYearData data = (CompYearData) ite.next();
				ps.setString(2, data.getYear());
				rs = ps.executeQuery();
				if (rs.next()) {
					data.setExaminees(rs.getInt(1));
				}
				rs.close();
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		} 
	}

	/**
	 * @return i
	 */
	public List getCompYearList() {
		return compYearList;
	}

	/**
	 * @param string s
	 */
	public void setTargetExam(final String string) {
		targetExam = string;
	}

	/**
	 * @param string s
	 */
	public void setTargetYear(final String string) {
		targetYear = string;
	}

	/**
	 * @param pBundleCd 設定する bundleCd。
	 */
	public void setBundleCd(final String pBundleCd) {
		bundleCd = pBundleCd;
	}

	/**
	 * @param pSchoolCd 設定する schoolCd。
	 */
	public void setSchoolCd(final String pSchoolCd) {
		schoolCd = pSchoolCd;
	}

}

package jp.co.fj.keinavi.servlets.sales.base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.sales.SD203AcceptBean;
import jp.co.fj.keinavi.beans.sales.SD203ListBean;
import jp.co.fj.keinavi.data.sales.SD203Data;
import jp.co.fj.keinavi.forms.sales.SD203Form;
import jp.co.fj.keinavi.interfaces.SpecialAppliStatus;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * 特例成績データ作成承認画面の抽象サーブレットです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public abstract class BaseAcceptServlet extends BaseSpecialAppliServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = 9039711324190473974L;

	/**
	 * JSP表示処理を行います。
	 */
	public void index(HttpServletRequest request, HttpServletResponse response,
			SD203Form form) throws Exception {

		request.setAttribute("results", findResults(request, form));

		forward2Jsp(request, response, "sd203");
	}

	/**
	 * 承認前のステータスを返します。
	 */
	abstract protected SpecialAppliStatus getUnAcceptedStatus();

	/**
	 * 承認後のステータスを返します。
	 */
	abstract protected SpecialAppliStatus getAcceptedStatus();

	/**
	 * 申請リストを取得します。
	 */
	private List findResults(HttpServletRequest request, SD203Form form)
			throws Exception {

		/* ソートキーの初期値をセットする */
		if (StringUtils.isEmpty(form.getSortKey())) {
			form.setSortKey("5");
		}

		return ((SD203ListBean) executeBean(request, new SD203ListBean(
				getUnAcceptedStatus(), form.getSortKey(),
				getSectorCdArray(request)))).getResults();
	}

	/**
	 * 承認処理を行います。
	 */
	public void accept(HttpServletRequest request,
			HttpServletResponse response, SD203Form form) throws Exception {

		if (form.getAppId() == null || form.getAppId().length == 0) {
			index(request, response, form);
			return;
		}

		/* フォームの申請IDとDBの申請IDの積集合を処理する */
		List formAppIdList = Arrays.asList(form.getAppId());

		List dbAppIdList = new ArrayList();
		for (Iterator ite = findResults(request, form).iterator(); ite
				.hasNext();) {
			SD203Data data = (SD203Data) ite.next();
			dbAppIdList.add(data.getAppId());
		}

		List appIdList = (List) CollectionUtils.intersection(formAppIdList,
				dbAppIdList);

		if (!appIdList.isEmpty()) {

			executeBean(request, new SD203AcceptBean(getAcceptedStatus(),
					appIdList));
		}

		index(request, response, form);
	}

}

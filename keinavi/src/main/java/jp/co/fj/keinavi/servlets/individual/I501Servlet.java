/*
 * 作成日: 2004/08/03
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.individual.InterviewBean;
import jp.co.fj.keinavi.beans.individual.InterviewSearchBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.forms.individual.I501Form;
import jp.co.fj.keinavi.servlets.KNServletException;

public class I501Servlet extends IndividualServlet{
	
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
			
		super.execute(request, response);
		
		//フォームデータを取得
		I501Form i501Form = null;
		try {
			i501Form = (I501Form)super.getActionForm(request, "jp.co.fj.keinavi.forms.individual.I501Form");
		} catch (Exception e) {
			KNServletException k = new KNServletException("0I501にてフォームデータの収得に失敗しました。");
			k.setErrorCode("00I501010101");
			throw k;
		}
		
		HttpSession session = request.getSession();
		
		//個人共通
		ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);
		//ログイン情報
		LoginSession login = getLoginSession(request);

		if ("i501".equals(getForward(request))) {//転送:JSP
			
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(false);
				
				/**　削除処理*/
				if(i501Form.getButton() != null && i501Form.getButton().equals("delete")){
					InterviewBean ib = new InterviewBean();
					ib.setIndividualId(i501Form.getIndividualId());
					ib.setKoIndividualId(i501Form.getKoIndividualId());
					ib.setFirstRegistrYdt(i501Form.getFirstRegistrYdt());
					ib.setDel(true);
					ib.setConnection("", con);
					ib.execute();
					con.commit();
				}
				/** メモ一覧取得 */
				InterviewSearchBean isb = new InterviewSearchBean(
						login.getUserID(), i501Form.getTargetPersonId());
				isb.setConnection("", con);
				isb.execute();
				// ページサイズは20で固定
				isb.setPageSize(20);
				if(!"i501".equals(getBackward(request)) || !iCommonMap.getTargetPersonId().equals(i501Form.getTargetPersonId())){
					// 初回ページ番号
					isb.setPageNo(Integer.parseInt("1"));
					i501Form.setPage(Integer.toString(isb.getPageNo()));
				}else{
					// 指定ページ番号
					try{
						isb.setPageNo(Integer.parseInt(i501Form.getPage()));
					}catch(Exception e){
						isb.setPageNo(Integer.parseInt(i501Form.getPage()) - 1);
					}
				}
				
				request.setAttribute("SearchBean", isb);
			} catch (Exception e) {
				e.printStackTrace();
				KNServletException k = new KNServletException("0I501にてエラーが発生しました。");
				k.setErrorCode("00I501010101");
				throw k;
			} finally {
				try {
					if (con != null) releaseConnectionPool(request, con);
				} catch (Exception e) {
					KNServletException k1 = new KNServletException("0I501にてコネクションの解放に失敗しました。");
					k1.setErrorCode("00I501010101");
					throw k1;
				}
			}			
			
			request.setAttribute("form", i501Form);
			
			iCommonMap.setTargetPersonId(i501Form.getTargetPersonId());
			super.forward(request, response, JSP_I501);
			
		}else{//転送:SERVLET
			
			iCommonMap.setTargetPersonId(i501Form.getTargetPersonId());
			super.forward(request, response, SERVLET_DISPATCHER);
			
		}
	
	}
}

/*
 * 作成日: 2004/09/17
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.individual.SubRecordData;

import com.fjh.beans.DefaultSearchBean;

/**
 * @author Totec) T.Yamada
 *
 * 2005.6.7 	T.Yamada 	[1]範囲区分の追加
 */
public class SubRecordSearchBean extends DefaultSearchBean{
	
	private static final long serialVersionUID = 9122941187892583739L;
	
	/*
	 * マークは換算得点
	 * 記述は全国偏差値
	 */
	private String query = "SELECT	 /*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */ SUBRECORD_I.INDIVIDUALID, SUBRECORD_I.EXAMYEAR " +
		",SUBRECORD_I.EXAMCD, SUBRECORD_I.SUBCD	,SUBRECORD_I.BUNDLECD " +
		",SUBRECORD_I.SCORE, SUBRECORD_I.CVSSCORE, SUBRECORD_I.S_RANK " +
		",SUBRECORD_I.A_RANK, SUBRECORD_I.S_DEVIATION, SUBRECORD_I.A_DEVIATION " +
		",SUBRECORD_I.SCOPE "+//[1] add
		" FROM SUBRECORD_I WHERE 1=1 ";//12/5 sql hint
		
	/** IN PARAM */
	private String individualId;
	private String examYear;
	private String examCd;

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		if(examYear != null && examCd != null){
			query += " AND SUBRECORD_I.EXAMYEAR = '"+examYear+"' ";
			query += " AND SUBRECORD_I.EXAMCD = '"+examCd+"' ";
		}else{
			return;//違法
		}
		if(individualId != null){
			query += " AND SUBRECORD_I.INDIVIDUALID = ? ";
		}

		PreparedStatement pstmt = null;
		ResultSet rs = null;		
		try{
			pstmt = conn.prepareStatement(query);
			if(individualId != null){
				pstmt.setString(1, individualId);
			}
			rs = pstmt.executeQuery();
			while(rs.next()){
				SubRecordData rd = new SubRecordData();
				rd.setPersonId(rs.getString("INDIVIDUALID"));
				rd.setExamYear(rs.getString("EXAMYEAR"));
				rd.setExamCd(rs.getString("EXAMCD"));
				rd.setSubCd(rs.getString("SUBCD"));
				rd.setBundleCd(rs.getString("BUNDLECD"));
				rd.setScore(rs.getString("SCORE"));
				rd.setCvsScore(rs.getString("CVSSCORE"));
				rd.setSRank(rs.getString("S_RANK"));
				rd.setCRank(rs.getString("A_RANK"));
				rd.setSDeviation(rs.getString("S_DEVIATION"));
				rd.setCDeviation(rs.getString("A_DEVIATION"));
				rd.setScope(rs.getString("SCOPE"));//[1] add
				recordSet.add(rd);
			}
		}catch(SQLException sqle){
			System.out.println("sqlexception : "+sqle);
			System.out.println(query);
			throw sqle;
		}catch(Exception e){
			System.out.println("exception : "+e);
			throw e;
		}finally{
			if(pstmt != null)pstmt.close();
			if(rs != null)rs.close();
		}
	}

	/**
	 * @return
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @param string
	 */
	public void setExamCd(String string) {
		examCd = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

}

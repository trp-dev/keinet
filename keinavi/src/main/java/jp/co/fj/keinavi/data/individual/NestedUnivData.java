/*
 * 作成日: 2004/08/19
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

import java.util.List;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class NestedUnivData {

	private String univCd;
	private String univNameAbbr;
	private String univNameKana;
		
	private List facultyList;
			
	public class FacultyData{
			
		private String facultyCd;
		private String facultyNameAbbr;
		private String facultyNameKana;
		
		private List deptList;
		
		public class DeptData{
			
			private String deptCd;
			private String deptNameAbbr;
			private String deptNameKana;
			
			private String superAbbrName;
			
			/**
			 * @return
			 */
			public String getDeptCd() {
				return deptCd;
			}

			/**
			 * @return
			 */
			public String getDeptNameAbbr() {
				return deptNameAbbr;
			}

			/**
			 * @return
			 */
			public String getDeptNameKana() {
				return deptNameKana;
			}

			/**
			 * @return
			 */
			public String getSuperAbbrName() {
				return superAbbrName;
			}

			/**
			 * @param string
			 */
			public void setDeptCd(String string) {
				deptCd = string;
			}

			/**
			 * @param string
			 */
			public void setDeptNameAbbr(String string) {
				deptNameAbbr = string;
			}

			/**
			 * @param string
			 */
			public void setDeptNameKana(String string) {
				deptNameKana = string;
			}

			/**
			 * @param string
			 */
			public void setSuperAbbrName(String string) {
				superAbbrName = string;
			}

		}
		/**
		 * @return
		 */
		public List getDeptList() {
			return deptList;
		}

		/**
		 * @return
		 */
		public String getFacultyCd() {
			return facultyCd;
		}

		/**
		 * @return
		 */
		public String getFacultyNameAbbr() {
			return facultyNameAbbr;
		}

		/**
		 * @return
		 */
		public String getFacultyNameKana() {
			return facultyNameKana;
		}

		/**
		 * @param list
		 */
		public void setDeptList(List list) {
			deptList = list;
		}

		/**
		 * @param string
		 */
		public void setFacultyCd(String string) {
			facultyCd = string;
		}

		/**
		 * @param string
		 */
		public void setFacultyNameAbbr(String string) {
			facultyNameAbbr = string;
		}

		/**
		 * @param string
		 */
		public void setFacultyNameKana(String string) {
			facultyNameKana = string;
		}

	}
	/**
	 * @return
	 */
	public List getFacultyList() {
		return facultyList;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @return
	 */
	public String getUnivNameAbbr() {
		return univNameAbbr;
	}

	/**
	 * @return
	 */
	public String getUnivNameKana() {
		return univNameKana;
	}

	/**
	 * @param list
	 */
	public void setFacultyList(List list) {
		facultyList = list;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

	/**
	 * @param string
	 */
	public void setUnivNameAbbr(String string) {
		univNameAbbr = string;
	}

	/**
	 * @param string
	 */
	public void setUnivNameKana(String string) {
		univNameKana = string;
	}
}

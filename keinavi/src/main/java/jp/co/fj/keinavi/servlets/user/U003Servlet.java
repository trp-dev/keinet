package jp.co.fj.keinavi.servlets.user;

import java.io.IOException;
import java.sql.Connection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.com_set.ChargeClassBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.user.LoginUserData;
import jp.co.fj.keinavi.data.user.UserClassData;
import jp.co.fj.keinavi.forms.user.U003Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.CollectionUtil;

/**
 *
 * 利用者管理−担当クラス設定画面サーブレット
 * 
 * 2005.10.13	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U003Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// HTTPセッション
		final HttpSession session = request.getSession(false);
		// ログインセッション
		final LoginSession login = getLoginSession(request);
		// 利用者データ
		final LoginUserData data = (LoginUserData) session.getAttribute("LoginUserData");
		// アクションフォーム
		final U003Form form = (U003Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.user.U003Form");
		
		// 転送元がJSP
		if ("u003".equals(getBackward(request))) {
		
			// まずは全部消す
			data.getChargeClassList().clear();
				
			// 個別設定値を保持する
			if ("2".equals(form.getMode())) {
				for (int i=0; i<form.getClasses().length; i++) {
					data.getChargeClassList().add(
							new UserClassData(form.getClasses()[i]));
				}
			}
			
			super.forward(request, response, JSP_CLOSE);
			
		// そうでなければJSPへ
		} else {
			
			// DBコネクション
			Connection con = null;
			try {
				con = super.getConnectionPool(request);

				// 担当クラスBean
				final ChargeClassBean bean =
						new ChargeClassBean(login.getUserID(), null, false);
				bean.setConnection(null, con);
				bean.execute();
				request.setAttribute("ClassBean", bean);

				final List c = new LinkedList(); // 入れ物
				final Iterator ite = data.getChargeClassList().iterator();
				while (ite.hasNext()) {
					c.add(((UserClassData) ite.next()).getKey());
				}
				form.setClasses((String[]) c.toArray(new String[0]));

			} catch (Exception e) {
				throw new ServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}

			request.setAttribute("form", form);
			request.setAttribute("CheckedMap", CollectionUtil.array2TrueMap(form.getClasses()));
			
			super.forward(request, response, JSP_U003);
		}
	}

}


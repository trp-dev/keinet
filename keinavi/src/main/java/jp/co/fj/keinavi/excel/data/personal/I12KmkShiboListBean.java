package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
//import java.util.ArrayList;
/**
 * �l���ѕ��́|�ʒk�p���[ �u�]��w�ʃ��X�g
 * �쐬��: 2004/07/20
 * @author	A.Iwata
 */
public class I12KmkShiboListBean {
	//��w��
	private String strDaigakuMei = "";
	//�w��
	private String strGakubuMei = "";
	//�w��
	private String strGakkaMei = "";
	//���_
	private int intTokuten = 0;
	//�΍��l
	private float floHensa = 0;
	
	/*----------*/
	/* Get      */
	/*----------*/

	public String getStrDaigakuMei() {
		return this.strDaigakuMei;
	}
	public String getStrGakubuMei() {
		return this.strGakubuMei;
	}
	public String getStrGakkaMei() {
		return this.strGakkaMei;
	}
	public int getIntTokuten() {
		return this.intTokuten;
	}
	public float getFloHensa() {
		return this.floHensa;
	}
		
	/*----------*/
	/* Set      */
	/*----------*/

	public void setStrDaigakuMei(String strDaigakuMei) {
		this.strDaigakuMei = strDaigakuMei;
	}
	public void setStrGakubuMei(String strGakubuMei) {
		this.strGakubuMei = strGakubuMei;
	}
	public void setStrGakkaMei(String strGakkaMei) {
		this.strGakkaMei = strGakkaMei;
	}
	public void setIntTokuten(int intTokuten) {
		this.intTokuten = intTokuten;
	}
	public void setFloHensa(float floHensa) {
		this.floHensa = floHensa;
	}
}

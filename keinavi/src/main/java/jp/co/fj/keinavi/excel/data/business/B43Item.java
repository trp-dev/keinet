package jp.co.fj.keinavi.excel.data.business;

import java.util.ArrayList;
/**
 * 校内成績分析−他校比較−偏差値分布データクラス
 * 作成日: 2004/07/19
 * @author	A.Iwata
 */
public class B43Item {
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//表フラグ
	private int intHyouFlg = 0;
	//*表示フラグ
	private int intBestFlg = 0;
	//セキュリティスタンプフラグ
	private int intSecuFlg = 0;
	//データリスト
	private ArrayList b43List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public int getIntHyouFlg() {
		return this.intHyouFlg;
	}
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public ArrayList getB43List() {
		return this.b43List;
	}
	public int getIntBestFlg() {
		return intBestFlg;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setIntHyouFlg(int intHyouFlg) {
		this.intHyouFlg = intHyouFlg;
	}
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setB43List(ArrayList b43List) {
		this.b43List = b43List;
	}
	public void setIntBestFlg(int i) {
		intBestFlg = i;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}

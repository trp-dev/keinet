package jp.co.fj.keinavi.servlets.user;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.beans.user.AdminUserUpdateBean;
import jp.co.fj.keinavi.beans.user.LoginUserInsertBean;
import jp.co.fj.keinavi.beans.user.LoginUserUpdateBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.user.LoginUserData;
import jp.co.fj.keinavi.forms.user.U004Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 * 利用者管理−登録完了画面サーブレット
 * 
 * 2005.10.13	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U004Servlet extends DefaultHttpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		// 転送先がJSP
		if ("u004".equals(getForward(request))) {
			
			// HTTPセッション
			final HttpSession session = request.getSession(false);
			// アクションフォーム
			final U004Form form = (U004Form) getActionForm(
					request, "jp.co.fj.keinavi.forms.user.U004Form");
			// ログインセッション
			final LoginSession login = getLoginSession(request);
			// 利用者データ
			final LoginUserData data = (LoginUserData) session.getAttribute("LoginUserData");
			
			try {
				// 更新処理（管理者）
				if (form.getOriginalLoginId() != null) {
					updateAdmin(request, login, data);
					//	アクセスログ
					actionLog(request, "702");
				// 更新処理（一般）
				} else if (form.getNewLoginId() == null) {
					update(request, login, data);
					//	アクセスログ
					actionLog(request, "702");
				// 登録処理
				} else {
					insert(request, login, data);
					//	アクセスログ
					actionLog(request, "701");
				}
			// 認識しているエラー
			} catch (final KNBeanException e) {
				request.setAttribute("ErrorMessage", e.getMessage());
				super.forward(request, response, JSP_U002);
				return;
			}
			
			super.forward(request, response, JSP_U004);
			
		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

	/**
	 * 登録処理
	 * 
	 * @param request
	 * @throws ServletException
	 */
	private void insert(final HttpServletRequest request, final LoginSession login,
			final LoginUserData data) throws ServletException, KNBeanException {
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);

			final LoginUserInsertBean bean = new LoginUserInsertBean(
					login.getUserID(), data);
			bean.setConnection(null, con);
			bean.execute();
			con.commit();
			
		// ID重複エラー
		} catch (final KNBeanException e) {
			rollback(con);
			throw e;
		// それ以外のエラー
		} catch (final Exception e) {
			rollback(con);
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);				
		}
	}
	
	/**
	 * 更新処理
	 * 
	 * @param request
	 * @throws ServletException
	 */
	private void update(final HttpServletRequest request,
			final LoginSession login,
			final LoginUserData data) throws ServletException {
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);
			
			final LoginUserUpdateBean bean = new LoginUserUpdateBean(
					login.getUserID(), data);
			bean.setConnection(null, con);
			bean.execute();
			con.commit();
		} catch (final Exception e) {
			rollback(con);
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);				
		}
	}

	/**
	 * 更新処理（管理者）
	 * 
	 * @param request
	 * @throws ServletException
	 */
	private void updateAdmin(final HttpServletRequest request,
			final LoginSession login,
			final LoginUserData data) throws ServletException, KNBeanException {
	
		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);
			
			final AdminUserUpdateBean bean = new AdminUserUpdateBean(
					login.getUserID(), data);
			bean.setConnection(null, con);
			bean.execute();
			con.commit();
			
			// 自分ならセッション情報書き換え
			if (!login.isHelpDesk() && login.getAccount().equals(data.getOriginalLoginId())) {
				login.setAccount(data.getLoginId());
			}
			
		// 認識しているエラー
		} catch (final KNBeanException e) {
			rollback(con);
			throw e;
		// それ以外のエラー
		} catch (final Exception e) {
			rollback(con);
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);				
		}
	}

}

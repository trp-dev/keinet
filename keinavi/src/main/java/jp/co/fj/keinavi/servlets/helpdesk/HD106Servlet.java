/*
 * 作成日: 2004/09/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.helpdesk;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.helpdesk.CertificatesBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.helpdesk.HD106Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.log.KNLog;

import com.fjh.db.DBManager;

/**
 * 2005.02.04 Yoshimoto KAWAI - Totec
 * スレッド化に伴ってHttpRequestオブジェクトは使えなくなったので
 * 処理結果はセッションへ入れるように変更
 *
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class HD106Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ

		// セッション
		HttpSession session = request.getSession();

		// ログイン情報
		LoginSession login = super.getLoginSession(request);

		// アクションフォーム
		HD106Form hd106Form = null;
		// requestから取得する
		try {
			hd106Form = (HD106Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.helpdesk.HD106Form");
		} catch (Exception e) {
			throw new ServletException(e);
		}

		if(("hd106").equals(getForward(request)) || getForward(request) == null) {

			try{
				//2004/12/24 スレッド処理追加
				CertThread ct = new CertThread(request, session, hd106Form, super.getConnectionPool(request), super.getDbKey(request));
				ct.start();

			}catch (SQLException e1) {

				throw new ServletException(e1.toString());

			}finally{}

			// アクションフォーム
			request.setAttribute("hd106Form", hd106Form);

			super.forward(request, response, JSP_HDOnCert);

			/* 2004/12/24 スレッド化の為コメントアウト
			Connection con = null; // コネクション
			try {

				con = super.getConnectionPool(request); 	// コネクション取得
				CertificatesBean bean = new CertificatesBean();
				bean.setConnection(null, con);
				bean.setHD106Form(hd106Form);
				bean.execute();

				request.setAttribute("CertificatesBean", bean);

			} catch (Exception e) {
				throw new ServletException(e.toString());
			} finally {
				super.releaseConnectionPool(request, con); // コネクション解放
			}


			// アクションフォーム
			request.setAttribute("hd106Form", hd106Form);

			super.forward(request, response, JSP_HD106);

			*/

		// 不明なら転送
		} else {
			String id = getForward(request);
			super.forward(request, response, SERVLET_DISPATCHER);
		}


	}
	/**
	 * 2004/12/24
	 * @author T.Yamada
	 * 証明書発行専用スレッド
	 *
	 */
	public class CertThread extends Thread{

		Connection con;
		HttpServletRequest request;
		HttpSession session;
		HD106Form form;
		String dBKey;

		public CertThread(HttpServletRequest aRequest, HttpSession aSession, HD106Form aForm, Connection aConn, String aDBKey){
			con = aConn;					//DBコネクション
			request = aRequest;				//リクエスト
			session = aSession;				//セッション
			form = aForm;					//フォーム
			dBKey = aDBKey;					//DBキー
		}
		public void run(){
			KNLog log = KNLog.getInstance(null, null, null);

			session.setAttribute(HDOnCertServlet.THREAD_KEY, HDOnCertServlet.STATUS_PROG);

			try{

				//sleep(10000);//テスト用に10秒眠らせる

				CertificatesBean bean = new CertificatesBean();
				bean.setConnection(null, con);
				bean.setHD106Form(form);
				bean.execute();

				// ------------------------------------------------------------
				// 2005.02.04
				// Yoshimoto KAWAI - Totec
				//request.setAttribute("CertificatesBean", bean);
				session.setAttribute("CertificatesBean", bean);
				// ------------------------------------------------------------

			}catch(Exception e){

				session.setAttribute(HDOnCertServlet.THREAD_KEY,HDOnCertServlet.STATUS_ERR_SQL);
				log.Err(null, null, e);

			}finally{

				try {
					DBManager.releaseConnectionPool(dBKey, con);// コネクション解放
				} catch (Exception e1) {
					session.setAttribute(HDOnCertServlet.THREAD_KEY,HDOnCertServlet.STATUS_ERR_SQL);
				}
			}

			session.setAttribute(HDOnCertServlet.THREAD_KEY, HDOnCertServlet.STATUS_DONE);

		}
	}

}

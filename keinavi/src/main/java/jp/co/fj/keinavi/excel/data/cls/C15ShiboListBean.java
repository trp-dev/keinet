package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−志望大学別個人評価データリスト
 * 作成日: 2004/08/30
 * @author	Ito.Y
 */
public class C15ShiboListBean {
	//大学コード
	private String strDaigakuCd = "";
	//大学名
	private String strDaigakuMei = "";
	//学部コード
	private String strGakubuCd = "";
	//学部名
	private String strGakubuMei = "";
	//日程コード
	private String strNtiCd = "";
	//日程
	private String strNittei = "";
	//総志望者数
	private int intSoshibo = 0;
	//出願予定人数
	private int intYoteiNinzu = 0;
	//評価別人数A
	private int intHyoukaA = 0;
	//評価別人数B
	private int intHyoukaB = 0;
	//評価別人数C
	private int intHyoukaC = 0;
	//評価別人数D
	private int intHyoukaD = 0;
	//評価別人数E
	private int intHyoukaE = 0;
	//志望者リスト
	private ArrayList c15PersonalDataList = new ArrayList();
	//2019/09/06 QQ)Oosaki 共通テスト対応 UPD START
	////2019/08/14 Hics)Ueta 共通テスト対応 ADD START
	////評価別人数A
	//private String intRatingnumA = "";
	////評価別人数B
	//private String intRatingnumB = "";
	////評価別人数C
	//private String intRatingnumC = "";
	////評価別人数D
	//private String intRatingnumD = "";
	////評価別人数E
	//private String intRatingnumE = "";
	////2019/08/14 Hics)Ueta 共通テスト対応 ADD END
	//評価別人数A
	private int intRatingnumA = 0;
	//評価別人数B
	private int intRatingnumB = 0;
	//評価別人数C
	private int intRatingnumC = 0;
	//評価別人数D
	private int intRatingnumD = 0;
	//評価別人数E
	private int intRatingnumE = 0;
	//2019/09/06 QQ)Oosaki 共通テスト対応 UPD END

	/*----------*/
	/* Get      */
	/*----------*/
	public ArrayList getC15PersonalDataList() {
		return c15PersonalDataList;
	}
	public int getIntHyoukaA() {
		return intHyoukaA;
	}
	public int getIntHyoukaB() {
		return intHyoukaB;
	}
	public int getIntHyoukaC() {
		return intHyoukaC;
	}
	public int getIntHyoukaD() {
		return intHyoukaD;
	}
	public int getIntHyoukaE() {
		return intHyoukaE;
	}
	public int getIntSoshibo() {
		return intSoshibo;
	}
	public int getIntYoteiNinzu() {
		return intYoteiNinzu;
	}
	public String getStrDaigakuCd() {
		return strDaigakuCd;
	}
	public String getStrDaigakuMei() {
		return strDaigakuMei;
	}
	public String getStrGakubuCd() {
		return strGakubuCd;
	}
	public String getStrGakubuMei() {
		return strGakubuMei;
	}
	public String getStrNittei() {
		return strNittei;
	}
	public String getStrNtiCd() {
		return strNtiCd;
	}
	//2019/09/06 QQ)Oosaki 共通テスト対応 UPD START
	////2019/08/14 Hics)Ueta 共通テスト対応 ADD START
	//public String getIntRatingnumA() {
	//	return intRatingnumA;
	//}
	//public String getIntRatingnumB() {
	//	return intRatingnumB;
        //}
	//public String getIntRatingnumC() {
	//	return intRatingnumC;
        //}
	//public String getIntRatingnumD() {
	//	return intRatingnumD;
        //}
	//public String getIntRatingnumE() {
	//	return intRatingnumE;
	//}
	////2019/08/14 Hics)Ueta 共通テスト対応 ADD END
	public int getIntRatingnumA() {
	        return intRatingnumA;
        }
        public int getIntRatingnumB() {
                return intRatingnumB;
        }
        public int getIntRatingnumC() {
                return intRatingnumC;
        }
        public int getIntRatingnumD() {
                return intRatingnumD;
        }
        public int getIntRatingnumE() {
                return intRatingnumE;
        }
	//2019/09/06 QQ)Oosaki 共通テスト対応 UPD END

	/*----------*/
	/* Set      */
	/*----------*/
	public void setC15PersonalDataList(ArrayList list) {
		c15PersonalDataList = list;
	}
	public void setIntHyoukaA(int i) {
		intHyoukaA = i;
	}
	public void setIntHyoukaB(int i) {
		intHyoukaB = i;
	}
	public void setIntHyoukaC(int i) {
		intHyoukaC = i;
	}
	public void setIntHyoukaD(int i) {
		intHyoukaD = i;
	}
	public void setIntHyoukaE(int i) {
		intHyoukaE = i;
	}
	public void setIntSoshibo(int i) {
		intSoshibo = i;
	}
	public void setIntYoteiNinzu(int i) {
		intYoteiNinzu = i;
	}
	public void setStrDaigakuCd(String string) {
		strDaigakuCd = string;
	}
	public void setStrDaigakuMei(String string) {
		strDaigakuMei = string;
	}
	public void setStrGakubuCd(String string) {
		strGakubuCd = string;
	}
	public void setStrGakubuMei(String string) {
		strGakubuMei = string;
	}
	public void setStrNittei(String string) {
		strNittei = string;
	}
	public void setStrNtiCd(String string) {
		strNtiCd = string;
	}
	//2019/09/06 QQ)Oosaki 共通テスト対応 UPD START
	////2019/08/14 Hics)Ueta 共通テスト対応 ADD START
	//public void setIntRatingnumA(String string) {
	//	intRatingnumA = string;
	//}
	//public void setIntRatingnumB(String string) {
	//	intRatingnumB = string;
	//}
	//public void setIntRatingnumC(String string) {
	//	intRatingnumC = string;
	//}
	//public void setIntRatingnumD(String string) {
	//	intRatingnumD = string;
	//}
	//public void setIntRatingnumE(String string) {
	//	intRatingnumE = string;
	//}
	////2019/08/14 Hics)Ueta 共通テスト対応 ADD END
	public void setIntRatingnumA(int i) {
                intRatingnumA = i;
        }
        public void setIntRatingnumB(int i) {
                intRatingnumB = i;
        }
        public void setIntRatingnumC(int i) {
                intRatingnumC = i;
        }
        public void setIntRatingnumD(int i) {
                intRatingnumD = i;
        }
        public void setIntRatingnumE(int i) {
                intRatingnumE = i;
        }
	//2019/09/06 QQ)Oosaki 共通テスト対応 UPD END
}
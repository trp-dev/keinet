package jp.co.fj.keinavi.beans.sheet.excel.user;

import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator;
import jp.co.fj.keinavi.beans.sheet.common.ISheetData;
import jp.co.fj.keinavi.beans.sheet.user.data.U01_01Data;
import jp.co.fj.keinavi.data.user.LoginUserData;
import jp.co.fj.keinavi.data.user.UserClassData;

import org.apache.poi.hssf.usermodel.HSSFCell;

/**
 *
 * 利用者登録通知作成クラス
 *
 * 2005.11.01	[新規作成]
 *
 * <大規模改修>
 * 2015.12.17 Kuniyasu Hisakawa - QuiQsoft
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class U01_01ExcelCreator extends BaseExcelCreator {

	// データクラス
	private final ISheetData sheetData;

	/**
	 * コンストラクタ
	 *
	 * @param pData
	 * @param pSequenceId
	 * @param pOutFileList
	 * @param pPrintFlag
	 * @throws Exception
	 */
	public U01_01ExcelCreator(final ISheetData pData, final String pSequenceId,
			final List pOutFileList, final int pPrintFlag) throws Exception {

		// 担当クラスのセルは255バイト以上になるのでセルコピーで対処する
		// 2016/01/08 QQ)Hisakawa 大規模改修 UPD START
		// super(pData, pSequenceId, pOutFileList, pPrintFlag, 1, "C19:G43");
		super(pData, pSequenceId, pOutFileList, pPrintFlag, 1, "C17:G41");
		// 2016/01/08 QQ)Hisakawa 大規模改修 UPD END

		this.sheetData = pData;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
	 * 			#initSheet(org.apache.poi.hssf.usermodel.HSSFSheet)
	 */
	protected void initSheet(ISheetData pData) {

		// 作成日
		setCreateDate();
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.BaseSheetCreator
	 * 			#createSheet(jp.co.fj.keinavi.beans.sheet.common.ISheetData)
	 */
	protected void createSheet(ISheetData pData) throws Exception {

		final U01_01Data data = (U01_01Data) pData;

		for (final Iterator ite = data.getU01_01ListData().iterator();
				ite.hasNext();) {

			// シート変更宣言
			setModifiedSheet();

			final LoginUserData user = (LoginUserData) ite.next();

			// 利用者ID
			getCell("C6").setCellValue(user.getLoginId());
			// 利用者名
			getCell("C8").setCellValue(user.getLoginName());
			// 初期パスワード
			getCell("C10").setCellValue(user.getDefaultPwd());
			// メンテナンス権限
			setMainteFlag(user.isMaintainer());
			// Kei-Navi権限
			setFunctionValue(getCell("F13"), user.getKnFunctionFlag());

			// 2015/12/17 QQ)Hisakawa 大規模改修 UPD START
			/*
			// 成績分析権限
			setFunctionValue(getCell("F14"), user.getScFunctionFlag());
			// 入試結果調査権限
			setFunctionValue(getCell("F15"), user.getEeFunctionFlag());
			// 答案閲覧権限
			setFunctionValue(getCell("F16"), user.getAbFunctionFlag());
			*/

			// 答案閲覧権限
			setFunctionValue(getCell("F14"), user.getAbFunctionFlag());

			// 2015/12/17 QQ)Hisakawa 大規模改修 UPD END

			// 担当クラス
			setChargeClassValue(user.getChargeClassList());

			// 改シート
			breakSheet();
		}
	}

	/**
	 * 担当クラスの値をセットする
	 *
	 * @param chargeClassList
	 */
	private void setChargeClassValue(final List chargeClassList) {

		// 2015/12/17 QQ)Hisakawa 大規模改修 UPD START

		// final HSSFCell cell = getCell("C19");
		final HSSFCell cell = getCell("C17");

		// 2015/12/17 QQ)Hisakawa 大規模改修 UPD END

		if (chargeClassList.size() == 0) {
			cell.setCellValue("制限なし");

		} else {

			final StringBuffer buff = new StringBuffer();

			String beforeYear = null;
			int beforeGrade = -1;

			for (final Iterator ite = chargeClassList.iterator();
					ite.hasNext();) {

				final UserClassData c = (UserClassData) ite.next();

				if (!c.getYear().equals(beforeYear)) {
					if (beforeYear != null) {
						buff.append("\r\n");
					}
					buff.append(c.getYear() + "年度");
				}

				if (!c.getYear().equals(beforeYear)
						|| c.getGrade() != beforeGrade) {
					buff.append("\r\n" + c.getGrade() + "年");
				} else {
					buff.append('・');
				}

				buff.append(c.getClassName() + "クラス");

				beforeYear = c.getYear();
				beforeGrade = c.getGrade();
			}

			cell.setCellValue(buff.toString());
		}
	}

	/**
	 * 機能権限の値をセットする
	 *
	 * @param cell
	 * @param flag
	 */
	private void setFunctionValue(HSSFCell cell, short flag) {

		if (flag == 1) {
			cell.setCellValue("可");
		} else if (flag == 2) {
			cell.setCellValue("集計データのみ可能");
		} else {
			cell.setCellValue("不可");
		}
	}

	/**
	 * メンテナンス権限をセットする
	 *
	 * @param isMaintainer
	 */
	private void setMainteFlag(boolean isMaintainer) {

		final HSSFCell cell = getCell("F12");

		if (isMaintainer) {
			cell.setCellValue("可");
		} else {
			cell.setCellValue("不可");
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
	 * 			#getSheetId()
	 */
	protected String getSheetId() {

		// 帳票IDを返す
		return "U01_01";
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
	 * 			#isGraphSheet()
	 */
	protected boolean isGraphSheet() {

		// グラフ帳票ではない
		return false;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
	 * 			#createSaveName(int)
	 */
	protected String createSaveName(final int workbookCount) {

		final U01_01Data data = (U01_01Data) sheetData;

		// 保存ワークブック名を作る
		if (data.getU01_01ListData().size() == 0
				|| workbookCount > data.getU01_01ListData().size()) {
			// シートがゼロなのはありえないが一応
			return super.createSaveName(workbookCount);
		} else {
			final LoginUserData user = (LoginUserData)
					data.getU01_01ListData().get(workbookCount - 1);

			// 2019/09/04 QQ)Tanioka 出力ファイル名変更 MOD START
			//return getExcelName() + "_" + user.getLoginId(
			//		) + "_" + getTimeStamp() + ".xls";
			return "利用者通知書_" + user.getLoginId() + "_" + getTimeStamp() + ".xls";
			// 2019/09/04 QQ)Tanioka 出力ファイル名変更 MOD END
		}
	}

}

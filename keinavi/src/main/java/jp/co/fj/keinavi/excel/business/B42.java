/**
 * 高校成績分析−高校間比較
 * 	Excelファイル編集
 * 作成日: 2004/08/19
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.business;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.business.B42Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

public class B42 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean b42(B42Item b42Item, ArrayList outfilelist, int intSaveFlg, String UserID, KNSheetLog sheetLog) {

		KNLog log = KNLog.getInstance(null,null,null);

		try{
			int ret = 0;
			//B42Itemから各帳票を出力
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD START
//			// 2019/09/05 QQ)Tanioka 共通テスト対応 UPD START
//			//if ( b42Item.getIntHyouFlg()==0 ) {
//			if ( b42Item.getIntHyouFlg()==0 && b42Item.getIntCheckBoxFlg()==0 ) {
//			// 2019/09/05 QQ)Tanioka 共通テスト対応 UPD END
			if ( b42Item.getIntHyouFlg()==0 ) {
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD END
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
			    //throw new Exception("B42 ERROR : フラグ異常");
			    throw new IllegalStateException("B42 ERROR : フラグ異常");
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			}
			if ( b42Item.getIntHyouFlg()==1 && b42Item.getIntKoseihiFlg()==2 && b42Item.getIntNendoFlg()==1 ) {
				B42_01 exceledit = new B42_01();
				ret = exceledit.b42_01EditExcel( b42Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"B42_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("B42_01");
			}
			if ( b42Item.getIntHyouFlg()==1 && b42Item.getIntKoseihiFlg()==2 && b42Item.getIntNendoFlg()==2 ) {
				B42_02 exceledit = new B42_02();
				ret = exceledit.b42_02EditExcel( b42Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"B42_02","帳票作成エラー","");
					return false;
				}
				sheetLog.add("B42_02");
			}
			if ( b42Item.getIntHyouFlg()==1 && b42Item.getIntKoseihiFlg()==2 && b42Item.getIntNendoFlg()==3 ) {
				B42_03 exceledit = new B42_03();
				ret = exceledit.b42_03EditExcel( b42Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"B42_03","帳票作成エラー","");
					return false;
				}
				sheetLog.add("B42_03");
			}
			if ( b42Item.getIntHyouFlg()==1 && b42Item.getIntKoseihiFlg()==1 && b42Item.getIntNendoFlg()==1 ) {
				B42_04 exceledit = new B42_04();
				ret = exceledit.b42_04EditExcel( b42Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"B42_04","帳票作成エラー","");
					return false;
				}
				sheetLog.add("B42_04");
			}
			if ( b42Item.getIntHyouFlg()==1 && b42Item.getIntKoseihiFlg()==1 && b42Item.getIntNendoFlg()==2 ) {
				B42_05 exceledit = new B42_05();
				ret = exceledit.b42_05EditExcel( b42Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"B42_05","帳票作成エラー","");
					return false;
				}
				sheetLog.add("B42_05");
			}
			if ( b42Item.getIntHyouFlg()==1 && b42Item.getIntKoseihiFlg()==1 && b42Item.getIntNendoFlg()==3 ) {
				B42_06 exceledit = new B42_06();
				ret = exceledit.b42_06EditExcel( b42Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"B42_06","帳票作成エラー","");
					return false;
				}
				sheetLog.add("B42_06");
			}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//			// 2019/09/05 QQ)Tanioka 共通テスト対応 ADD START
//			if ( b42Item.getIntCheckBoxFlg()==1 ) {
//
//				B42_07 exceledit = new B42_07();
//				ret = exceledit.b42_07EditExcel( b42Item, outfilelist, intSaveFlg, UserID );
//				if (ret!=0) {
//					log.Err("0"+ret+"B42_07","帳票作成エラー","");
//					return false;
//				}
//				sheetLog.add("B42_07");
//			}
//			// 2019/09/05 QQ)Tanioka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		} catch(Exception e) {
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
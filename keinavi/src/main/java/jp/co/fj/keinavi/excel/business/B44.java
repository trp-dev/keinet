/*
 * 校内成績分析−過年度比較　志望大学評価別人数
 * 作成日: 2004/09/
 * @author	A.Hasegawa
 */

package jp.co.fj.keinavi.excel.business;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.business.B44Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

/**
 * @author keinet
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class B44 {

	/*
	 * 出力帳票選択処理
	 * フラグにより帳票出力の判断をする。
	 * 		B44Item b44Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID:ユーザID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */

	 public boolean b44(B44Item b44Item, ArrayList outfilelist, int intSaveFlg, String UserID, KNSheetLog sheetLog) {

		KNLog log = KNLog.getInstance(null,null,null);

	 	try{

	 		int intRunFlg = b44Item.getIntDaiTotalFlg();

			int ret = 0;
			//B44Itemから各帳票を出力
			switch(intRunFlg) {
				case 1:	//大学（日程あり）
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL START
//					B44_02 exceledit02 = new B44_02();
//					ret = exceledit02.b44_02EditExcel(b44Item,outfilelist,intSaveFlg,UserID);
//					if (ret!=0) {
//						log.Err("0"+ret+"B44_02","帳票作成エラー","");
//						return false;
//					}
//					sheetLog.add("B44_02");
//					break;
//				case 2:	//大学（日程なし）
//					B44_01 exceledit01 = new B44_01();
//					ret = exceledit01.b44_01EditExcel(b44Item,outfilelist,intSaveFlg,UserID);
//					if (ret!=0) {
//						log.Err("0"+ret+"B44_01","帳票作成エラー","");
//						return false;
//					}
//					sheetLog.add("B44_01");
//					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL END
// 2019/10/02 QQ)Tanioka 廃止帳票 ADD START
				case 2:	//大学（日程なし）
				case 4:	//学部（日程なし）
// 2019/10/02 QQ)Tanioka 廃止帳票 ADD END
				case 3:	//学部（日程あり）
					B44_04 exceledit04 = new B44_04();
					ret = exceledit04.b44_04EditExcel(b44Item,outfilelist,intSaveFlg,UserID);
					if (ret!=0) {
						log.Err("0"+ret+"B44_04","帳票作成エラー","");
						return false;
					}
					sheetLog.add("B44_04");
					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL START
//				case 4:	//学部（日程なし）
//					B44_03 exceledit03 = new B44_03();
//					ret = exceledit03.b44_03EditExcel(b44Item,outfilelist,intSaveFlg,UserID);
//					if (ret!=0) {
//						log.Err("0"+ret+"B44_03","帳票作成エラー","");
//						return false;
//					}
//					sheetLog.add("B44_03");
//					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL END
				case 5:	//学科
					B44_05 exceledit05 = new B44_05();
					ret = exceledit05.b44_05EditExcel(b44Item,outfilelist,intSaveFlg,UserID);
					if (ret!=0) {
						log.Err("0"+ret+"B44_05","帳票作成エラー","");
						return false;
					}
					sheetLog.add("B44_05");
					break;
			}
	 	}
	 	catch(Exception e){
			log.Err("99B44","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
	 	}

	 	return true;

	 }

}

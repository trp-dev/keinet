package jp.co.fj.keinavi.util.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import jp.co.fj.keinavi.data.LoginSession;

/**
 *
 * K-Webへのリンクを持つ権限があるか判断するためのカスタムタグ
 * 
 * 2010.08.20    [新規作成]
 *
 * 2013.02.05	Tomohisa Yamada - TOTEC
 * 				Kei-Navi答案閲覧システムリンク改修
 * 
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class KWebConditionTag extends TagSupport {

    /**
     * @see javax.servlet.jsp.tagext.Tag#doStartTag()
     */
    public int doStartTag() throws JspException {        
    	
    	final LoginSession loginSession = 
    		(LoginSession) pageContext.getAttribute("LoginSession", PageContext.SESSION_SCOPE);
        
    	if (condition(loginSession)) {
    		return EVAL_BODY_INCLUDE;
    	}
    	
		return SKIP_BODY;
    }
    
    /**
     * 条件評価
     */
    protected boolean condition(
    		LoginSession loginSession) throws JspTagException { 
    	
    	//体験版の場合は必ず表示する。
    	if (loginSession.isTrial()) {
    		return true;
    	}
    	
    	//高校でない場合は非表示 ※営業からの代行は除く
    	if (!loginSession.isSchool()) {
    		return false;
    	}
    	
        //権限が存在する場合（=1）のみ許可
        return loginSession.getAbFunctionFlag() == 1;
    }

}

/*
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;//[1] add
import java.util.Collection;//[1] add

import jp.co.fj.keinavi.data.ExamData;//[1] add
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.IExamData;
import jp.co.fj.keinavi.data.sheet.ExtI11ListBean;
import jp.co.fj.keinavi.excel.data.personal.I12KmkShiboListBean;
import jp.co.fj.keinavi.excel.data.personal.I12KmkSuiiListBean;
import jp.co.fj.keinavi.excel.data.personal.I12MshHensaListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.sql.Query;

/**
 * 個人成績分析−成績分析面談 科目別成績推移グラフデータリスト印刷用Bean
 * 
 * @author
 *   Symbol Date       Person     Note
 *   [1]    2005.03.02 kondo      科目変換に、模試コードを追加。
 * 	 [2]    2005.04.14 kondo      対象模試が新テストだった場合、種別フラグに"1"を入れるように修正
 *   [3]    2005.09.14 kondo      科目取得の際、EXAMSUBJECTからとってきていたものを、Viewより取得するよう変更
 *   
 * 2009.11.30   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 * 2010.01.18   Shoji HASE Totec
 *              「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 * 　　　　　　 「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 * 
 */
public class I12_02SheetBean extends IndAbstractSheetBean {

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		Short studentDispGrade;//表示中の生徒
		if(!"i401".equals(backward)){
			/**科目別成績推移の場合プロファイル**/
			//教科分析シートフラグ
			data.setIntSuiiFlg(1);
			//教科分析シートセキュリティスタンプ
			data.setIntSuiiSecuFlg(((Short) profile.getItemMap(IProfileCategory.I_SCORE_COURSE_TRANS).get(IProfileItem.PRINT_STAMP)).shortValue());
			data.setIntSuiiShubetsuFlg(getNewTestFlg());//[2] add 新テストフラグ（科目別成績推移シート）
			//表示中の生徒
			studentDispGrade = (Short)profile.getItemMap(IProfileCategory.I_SCORE_COURSE_TRANS).get(IProfileItem.STUDENT_DISP_GRADE);
		}else{
			/**簡単印刷の場合プロファイル**/
			//教科分析シートフラグ
			String[] KyoSheetFlg = CollectionUtil.splitComma((String) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_FORMS));
			if(KyoSheetFlg.length != 0) {data.setIntSuiiFlg(Integer.parseInt(KyoSheetFlg[1]));}
			//セキュリティスタンプ
			data.setIntSuiiSecuFlg(((Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.PRINT_STAMP)).shortValue());
			data.setIntSuiiShubetsuFlg(getNewTestFlg());//[2] add 新テストフラグ（科目別成績推移シート）
			//表示中の生徒
			studentDispGrade = (Short)profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.STUDENT_DISP_GRADE);
		}
		
		//個人共通
		ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);
		
		//conn.setAutoCommit(false);//[1] delete
		//this.insertIntoSubCDTrans();//[1] delete
		
		
		/*下記条件の時に値を格納
		・面談シートフラグ=2or4
		・教科分析シートフラグ=1*/
		if (data.getIntSuiiFlg() == 1) {
			PreparedStatement	 ps2 = null;
			PreparedStatement	 ps3 = null;
			PreparedStatement	 ps4 = null;
			PreparedStatement	 ps5 = null;
			PreparedStatement	 ps9 = null; //特殊パターン対応
			ResultSet			 rs2 = null;
			ResultSet			 rs3 = null;
			ResultSet			 rs4 = null;
			
			
			//----------Data---------//
			String thisYear		 = super.getThisYear();							//現年度
			String examYear		 = iCommonMap.getTargetExamYear();	//模試年度
			String examCd		 = iCommonMap.getTargetExamCode();	//模試コード
			boolean gradeFlg		 = false;						//学年フラグ
			if(studentDispGrade.intValue() == 2) {gradeFlg = true;}
			//----------Data---------//
			
			//画面で選択された、順位
			if(!("i107".equals(backward))){
				targetCandidateRank1 = "1";
				targetCandidateRank2 = "2";
				targetCandidateRank3 = "3";
			}
			
			//[1] add start
			Collection examDatas = new ArrayList();
			Collection individualIds = new ArrayList();
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				for (java.util.Iterator it=data.getI11List().iterator(); it.hasNext();) {
					ExtI11ListBean printData = (ExtI11ListBean) it.next();
					individualIds.add(printData.getIndividualid());
				}
				
				Query qe = new Query(getExamDatasSQL(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(), gradeFlg));
				qe.setStringArray(1, (String[])individualIds.toArray(new String[individualIds.size()]));
				ps = conn.prepareStatement(qe.toString());
				rs = ps.executeQuery();
				while(rs.next()) {
					ExamData examData = new ExamData();
					examData.setExamYear(rs.getString("EXAMYEAR"));
					examData.setExamCD(rs.getString("EXAMCD"));
					examDatas.add(examData);
				}
			} catch(Exception e) {
				e.printStackTrace();
				throw e;
			} finally {
				if(ps != null) ps.close();
				if(rs != null) rs.close();
			}
			conn.setAutoCommit(false);
			this.insertIntoSubCDTrans((ExamData[])examDatas.toArray(new ExamData[examDatas.size()]));
			//[1] add end
			
			//１．全生徒
			for (java.util.Iterator it=data.getI11List().iterator(); it.hasNext();) {
				ExtI11ListBean printData = (ExtI11ListBean) it.next();
					
				try{
					String candidateRank1 = "1";
					String candidateRank2 = "2";
					String candidateRank3 = "3";
					//画面で選択された生徒なら、画面で選択された順位にする。
					if(printData.getIndividualid().equals(iCommonMap.getTargetPersonId())){
						candidateRank1 = targetCandidateRank1;
						candidateRank2 = targetCandidateRank2;
						candidateRank3 = targetCandidateRank3;
					}
					String[] candidateRanks = {candidateRank1,candidateRank2,candidateRank3};
					

					//プリコンパイル
					Query qe1 = new Query(candidateDataSql());
					qe1.setStringArray(1, candidateRanks);
					Query qe2 = new Query(univDataSql());
					qe2.setStringArray(1, candidateRanks);
					
					
					//特殊パターン
					Query qe3 = new Query(univDataSql2());	//特殊パターン対応
					qe3.setStringArray(1, candidateRanks);	//特殊パターン対応
					
					ps2 = conn.prepareStatement(subDataSql());			//ＳＱＬ２
					ps3 = conn.prepareStatement(deviationDataSql(studentDispGrade.toString()));	//ＳＱＬ３
					ps4 = conn.prepareStatement(qe1.toString());		//ＳＱＬ４
					ps5 = conn.prepareStatement(qe2.toString());		//ＳＱＬ５
					ps9 = conn.prepareStatement(qe3.toString());	//特殊パターンフラグ
					
					
					//科目別成績検索 
					ps2.setString(1, printData.getIndividualid());
					ps2.setString(2, examYear);
					ps2.setString(3, examCd);
					rs2 = ps2.executeQuery();
					
					while(rs2.next()) {
						//科目別成績new
						I12KmkSuiiListBean subRecordData = new I12KmkSuiiListBean();
						//科目別成績set
						subRecordData.setStrKmkmei(rs2.getString("SUBNAME"));
												
						//模試別偏差値検索
						ps3.setString(1, printData.getIndividualid());
						ps3.setString(2, examYear);
						ps3.setString(3, examCd);
						ps3.setString(4, examYear);
						ps3.setString(5, examCd);
						ps3.setString(6, rs2.getString("SUBCD"));
						ps3.setString(7, printData.getIndividualid());
						
//						ps3.setString(1, rs2.getString("SUBCD"));
//						ps3.setString(2, examYear);
//						ps3.setString(3, examCd);
//						ps3.setString(4, examYear);
//						ps3.setString(5, examCd);
//						ps3.setString(6, printData.getIndividualid());
						if(studentDispGrade.toString().equals("1")) {ps3.setString(8, thisYear);}	//現学年のみの場合
						rs3 = ps3.executeQuery();
						
						final int MAXEXAMCOUNT = 7;
						
						while(rs3.next()) {
							//模試別偏差値new
							I12MshHensaListBean examData = new I12MshHensaListBean();
							//模試別偏差値set
							examData.setStrMshmei(rs3.getString("EXAMNAME_ABBR"));
							examData.setStrMshDate(rs3.getString("INPLEDATE"));
							examData.setIntTokuten(rs3.getInt("SCORE"));
							examData.setFloHensa(rs3.getFloat("A_DEVIATION"));
							examData.setStrScholarLevel(rs3.getString("ACADEMICLEVEL"));
							
							//志望大学別検索
							if(examYear.equals(rs3.getString("EXAMYEAR")) && examCd.equals(rs3.getString("EXAMCD"))) {

								//特殊パターン対応（過去合格者平均のみ）
								//以下の科目コードの場合は変換を行う
								boolean exFlg = false;
								String targetSubCd = rs2.getString("SUBCD");
								if(rs3.getString("EXAMTYPECD").equals("01")) {
									if(targetSubCd.equals("2380")) {
										targetSubCd = "2000";
										exFlg = true;
									} else if(targetSubCd.equals("2480")){
										targetSubCd = "2000";
										exFlg = true;
									} else if(targetSubCd.equals("2580")){
										targetSubCd = "2000";
										exFlg = true;
									}
								}
								
								if(exFlg) {
									//flg = true
									ps9.setString(1, targetSubCd);
									ps9.setString(2, printData.getIndividualid());
									ps9.setString(3, examYear);
									ps9.setString(4, examCd);
									rs4 = ps9.executeQuery();
								} else {
									//flg = false
									//それ以外
									//ps5.setString(1, rs2.getString("SUBCD"));
									ps5.setString(1, targetSubCd);
									ps5.setString(2, printData.getIndividualid());
									ps5.setString(3, examYear);
									ps5.setString(4, examCd);
								//	ps5.setString(4, candidateRanks);//画面から来た、志望順位
									rs4 = ps5.executeQuery();
								}
							} else {
								//対象模試
								ps4.setString(1, printData.getIndividualid());
								ps4.setString(2, examYear);
								ps4.setString(3, examCd);
				
								rs4 = ps4.executeQuery();
							}

							while(rs4.next()) {
								//志望大学別new
								I12KmkShiboListBean candidateData = new I12KmkShiboListBean();
								//志望大学別set
								candidateData.setStrDaigakuMei(rs4.getString("UNIVNAME_ABBR"));
								candidateData.setStrGakubuMei(rs4.getString("FACULTYNAME_ABBR"));
								candidateData.setStrGakkaMei(rs4.getString("DEPTNAME_ABBR"));
								candidateData.setIntTokuten(-999);
								candidateData.setFloHensa(rs4.getFloat("SUCCESSAVGDEV"));

								//志望大学別List.add
								examData.getI12KmkShiboList().add(candidateData);
							}
							if(rs4 != null) {rs4.close();}
							//模試別偏差値List.add
							subRecordData.getI12MshHensaList().add(examData);
						}
						while(subRecordData.getI12MshHensaList().size() > MAXEXAMCOUNT) {
							subRecordData.getI12MshHensaList().remove(0);
						}
						//科目別成績List.add
						printData.getI12KmkSuiiList().add(subRecordData);
						if(rs3 != null) {rs3.close();}
					}
					if(ps2 != null) {ps2.close();}
					if(ps3 != null) {ps3.close();}
					if(ps4 != null) {ps4.close();}
					if(ps5 != null) {ps5.close();}
					if(ps9 != null) {ps9.close();}
					if(rs2 != null) {rs2.close();}
				}finally{
					if(ps2 != null) {ps2.close();}
					if(ps3 != null) {ps3.close();} 
					if(ps4 != null) {ps4.close();}
					if(ps5 != null) {ps5.close();}
					if(ps9 != null) {ps9.close();}
					if(rs2 != null) {rs2.close();}
					if(rs3 != null) {rs3.close();} 
					if(rs4 != null) {rs4.close();}
				}
			}
		}
		conn.commit();
	}
	
	//１．科目
	private String subDataSql() {
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT ");
		strBuf.append(" /*+ INDEX(SR PK_SUBRECORD_I) */ ");//12/5 sql hint
		strBuf.append("SR.SUBCD, ES.SUBNAME ");
		strBuf.append("FROM ");
		//strBuf.append("EXAMSUBJECT ES ");//[3] del
		strBuf.append("V_I_CMEXAMSUBJECT ES ");//[3] add
		strBuf.append("INNER JOIN ");
		strBuf.append("SUBRECORD_I SR ");
		strBuf.append("ON ES.SUBCD = SR.SUBCD ");
		strBuf.append("AND ES.EXAMYEAR = SR.EXAMYEAR ");
		strBuf.append("AND ES.EXAMCD = SR.EXAMCD ");
		strBuf.append("WHERE ");
		strBuf.append("SR.INDIVIDUALID = ? ");//学籍履歴情報（個人ＩＤ）
		strBuf.append("AND SR.EXAMYEAR = ? ");//セッション（年度）
		strBuf.append("AND SR.EXAMCD = ? ");//セッション（模試コード）
		strBuf.append("AND SR.SUBCD < '7000' ");
		strBuf.append("ORDER BY ");
		strBuf.append("ES.DISPSEQUENCE ");

		return strBuf.toString();
	}
	
	//２．模試別・偏差値データの取得
	private String deviationDataSql(String student) {
		String DATAOPENDATE = null;
		if(login.isHelpDesk()) {
			DATAOPENDATE = "IN_DATAOPENDATE";
		} else {
			DATAOPENDATE = "OUT_DATAOPENDATE";
		}
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT ");
		strBuf.append(" /*+ INDEX(SI PK_SUBRECORD_I) */ ");//12/5 sql hint
		strBuf.append("EX.EXAMYEAR, ");
		strBuf.append("EX.EXAMCD, ");
		strBuf.append("EX.EXAMNAME_ABBR, ");
		strBuf.append("EX.EXAMTYPECD, ");
		strBuf.append("EX.INPLEDATE, ");
		strBuf.append("NVL(SI.SCORE, -999) SCORE, ");
		strBuf.append("NVL(SI.A_DEVIATION, -999.0) A_DEVIATION,");
		strBuf.append("SI.ACADEMICLEVEL ");
		strBuf.append("FROM ");
		strBuf.append("EXAMINATION EX ");
		strBuf.append("INNER JOIN ");
		strBuf.append("(SELECT /*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */ EXAMYEAR, EXAMCD FROM SUBRECORD_I WHERE INDIVIDUALID = ? GROUP BY EXAMYEAR, EXAMCD) SB ");
		strBuf.append("ON EX.EXAMYEAR = SB.EXAMYEAR ");
		strBuf.append("AND EX.EXAMCD = SB.EXAMCD ");
		strBuf.append("AND EX.").append(DATAOPENDATE).append(" <= (SELECT ").append(DATAOPENDATE).append(" FROM EXAMINATION WHERE EXAMYEAR = ? AND EXAMCD = ?) ");
		strBuf.append("AND EX.EXAMTYPECD = (SELECT EXAMTYPECD FROM EXAMINATION WHERE EXAMYEAR = ? AND EXAMCD = ?) ");
		strBuf.append("LEFT JOIN ");
		strBuf.append("SUBCDTRANS ST ");
		//strBuf.append("ON ST.YEAR = EX.EXAMYEAR ");//[1] delete
		strBuf.append("ON ST.EXAMYEAR = EX.EXAMYEAR ");//[1] add
		strBuf.append("AND ST.CURSUBCD = ? ");
		strBuf.append("AND ST.EXAMCD = EX.EXAMCD ");//[1] add
		strBuf.append("LEFT JOIN ");
		strBuf.append("SUBRECORD_I SI ");
		strBuf.append("ON SI.EXAMYEAR = EX.EXAMYEAR ");
		strBuf.append("AND SI.EXAMCD = EX.EXAMCD ");
		strBuf.append("AND SI.INDIVIDUALID = ? ");
		strBuf.append("AND SI.SUBCD = ST.SUBCD ");
		if(student.equals("1")) {
			strBuf.append("WHERE EX.EXAMYEAR = ? ");	//セッション（現年度）
		}
		strBuf.append("ORDER BY ");
		strBuf.append("EX.").append(DATAOPENDATE).append(" ");
//		strBuf.append("SELECT ");
//		strBuf.append("EX.EXAMYEAR, ");
//		strBuf.append("EX.EXAMCD, ");
//		strBuf.append("EX.EXAMNAME_ABBR, ");
//		strBuf.append("EX.EXAMTYPECD, ");
//		strBuf.append("EX.INPLEDATE, ");
//		strBuf.append("NVL(SI.SCORE, -999) SCORE, ");
//		strBuf.append("NVL(SI.A_DEVIATION, -999.0) A_DEVIATION ");
//		strBuf.append("FROM ");
//		strBuf.append("EXAMINATION EX ");
//		strBuf.append("INNER JOIN ");
//		strBuf.append("SUBCDTRANS ST ");
//		strBuf.append("ON ST.YEAR = EX.EXAMYEAR ");
//		strBuf.append("AND ST.CURSUBCD = ? ");
//		strBuf.append("AND EX.").append(DATAOPENDATE).append(" <= (SELECT ").append(DATAOPENDATE).append(" FROM EXAMINATION WHERE EXAMYEAR = ? AND EXAMCD = ?) ");
//		strBuf.append("AND EX.EXAMTYPECD = (SELECT EXAMTYPECD FROM EXAMINATION WHERE EXAMYEAR = ? AND EXAMCD = ?) ");
//		strBuf.append("INNER JOIN ");
//		strBuf.append("SUBRECORD_I SI ");
//		strBuf.append("ON SI.INDIVIDUALID = ? ");
//		strBuf.append("AND SI.EXAMYEAR = EX.EXAMYEAR ");
//		strBuf.append("AND SI.EXAMCD = EX.EXAMCD ");
//		strBuf.append("AND SI.SUBCD = ST.SUBCD ");
//		if(student.equals("1")) {
//			strBuf.append("WHERE SI.EXAMYEAR = ? ");	//セッション（現年度）
//		}
//		strBuf.append("ORDER BY ");
//		strBuf.append("EX.").append(DATAOPENDATE).append(" ");
		return strBuf.toString();
	}
	
	//３．対象模試以外の志望大学リスト
	private String candidateDataSql() {
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT ");
		strBuf.append(" /*+ INDEX(CR PK_CANDIDATERATING) */ ");//12/5 sql hint
		strBuf.append("HS3.UNINAME_ABBR UNIVNAME_ABBR, HS3.FACULTYNAME_ABBR, HS3.DEPTNAME_ABBR, -999.0 SUCCESSAVGDEV ");
		strBuf.append("FROM ");
		strBuf.append("EXAMINATION EX ");
		strBuf.append("INNER JOIN ");
		strBuf.append("CANDIDATERATING CR ");
		strBuf.append("ON EX.EXAMYEAR = CR.EXAMYEAR ");
		strBuf.append("AND EX.EXAMCD = CR.EXAMCD ");
		strBuf.append("LEFT JOIN ");
		strBuf.append("UNIVMASTER_BASIC HS3 ");
		strBuf.append("ON HS3.EVENTYEAR = CR.EXAMYEAR ");
		strBuf.append("AND HS3.EXAMDIV = EX.EXAMDIV ");
		strBuf.append("AND HS3.UNIVCD = CR.UNIVCD ");
		strBuf.append("AND HS3.FACULTYCD = CR.FACULTYCD ");
		strBuf.append("AND HS3.DEPTCD = CR.DEPTCD ");
		strBuf.append("WHERE ");
		strBuf.append("CR.INDIVIDUALID = ? ");	//学籍基本情報（個人ＩＤ）
		strBuf.append("AND CR.EXAMYEAR = ? ");	//セッション（対象年度）
		strBuf.append("AND CR.EXAMCD = ? ");	//セッション（模試コード）
		strBuf.append("AND EX.EXAMTYPECD IN ('01','02','03')");	//マーク模試、記述模試、私大模試のみ
		strBuf.append("AND NOT EX.EXAMCD IN ('66','67')");		//（マーク高２、記述高２省く）
		strBuf.append("AND CR.CANDIDATERANK IN (#) ");	//画面入植（志望順位）
		strBuf.append("ORDER BY ");
		strBuf.append("CANDIDATERANK ASC ");
		return strBuf.toString();
	}
	
	//４．対象模試の志望大学リスト(特殊パターン対応)
	private String univDataSql2() {
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT ");
		strBuf.append(" /*+ INDEX(CR PK_CANDIDATERATING) */ ");//12/5 sql hint
		strBuf.append("HS3.UNINAME_ABBR UNIVNAME_ABBR, HS3.FACULTYNAME_ABBR, HS3.DEPTNAME_ABBR, NVL(PS.SUCCESSAVGDEV, -999.0) SUCCESSAVGDEV ");
		strBuf.append("FROM ");
		strBuf.append("EXAMINATION EX ");
		strBuf.append("INNER JOIN ");
		strBuf.append("CANDIDATERATING CR ");
		strBuf.append("ON EX.EXAMYEAR = CR.EXAMYEAR ");
		strBuf.append("AND EX.EXAMCD = CR.EXAMCD ");
//		strBuf.append("INNER JOIN SUBCDTRANS ST ");
//		strBuf.append("ON ST.YEAR = TO_NUMBER(EX.EXAMYEAR) - 1 ");
//		strBuf.append("AND ST.CURSUBCD = ?");
		strBuf.append("LEFT JOIN ");
		strBuf.append("UNIVMASTER_BASIC HS3 ");
		strBuf.append("ON HS3.EVENTYEAR = CR.EXAMYEAR ");
		strBuf.append("AND HS3.EXAMDIV = EX.EXAMDIV ");
		strBuf.append("AND HS3.UNIVCD = CR.UNIVCD ");
		strBuf.append("AND HS3.FACULTYCD = CR.FACULTYCD ");
		strBuf.append("AND HS3.DEPTCD = CR.DEPTCD ");
		strBuf.append("LEFT JOIN ");
		strBuf.append("PREVSUCCESS PS ");
		//strBuf.append("ON PS.EXAMYEAR = ST.YEAR ");
		strBuf.append("ON PS.EXAMYEAR = TO_NUMBER(EX.EXAMYEAR) - 1 ");
		strBuf.append("AND PS.UNIVCD = CR.UNIVCD ");
		strBuf.append("AND PS.FACULTYCD = CR.FACULTYCD ");
		strBuf.append("AND PS.DEPTCD = CR.DEPTCD ");
		strBuf.append("AND PS.EXAMTYPECD = EX.EXAMTYPECD ");
//		strBuf.append("AND PS.SUBCD = ST.SUBCD ");	//科目別成績（科目コード）
		strBuf.append("AND PS.SUBCD = ? ");
		strBuf.append("WHERE ");
		strBuf.append("CR.INDIVIDUALID = ? ");	//学歴基本情報（個人ＩＤ）
		strBuf.append("AND CR.EXAMYEAR = ? ");	//セッション（模試年度）
		strBuf.append("AND CR.EXAMCD = ? ");	//セッション（模試コード）
		strBuf.append("AND EX.EXAMTYPECD IN ('01','02','03')");	//マーク模試、記述模試、私大模試のみ
		strBuf.append("AND NOT EX.EXAMCD IN ('66','67')");		//（マーク高２、記述高２省く）
		strBuf.append("AND CR.CANDIDATERANK IN (#) ");	//画面（志望順位）
		strBuf.append("ORDER BY ");
		strBuf.append("CANDIDATERANK ASC ");
		
		return strBuf.toString();
		
	}
	
	//４．対象模試の志望大学リスト
	private String univDataSql() {
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT ");
		strBuf.append(" /*+ INDEX(CR PK_CANDIDATERATING) */ ");//12/5 sql hint
		strBuf.append("HS3.UNINAME_ABBR UNIVNAME_ABBR, HS3.FACULTYNAME_ABBR, HS3.DEPTNAME_ABBR, NVL(PS.SUCCESSAVGDEV, -999.0) SUCCESSAVGDEV ");
		strBuf.append("FROM ");
		strBuf.append("EXAMINATION EX ");
		strBuf.append("INNER JOIN ");
		strBuf.append("CANDIDATERATING CR ");
		strBuf.append("ON EX.EXAMYEAR = CR.EXAMYEAR ");
		strBuf.append("AND EX.EXAMCD = CR.EXAMCD ");
		strBuf.append("INNER JOIN SUBCDTRANS ST ");
		//strBuf.append("ON ST.YEAR = TO_NUMBER(EX.EXAMYEAR) - 1 ");//[3] delete
		strBuf.append("ON ST.EXAMYEAR = TO_NUMBER(EX.EXAMYEAR) - 1 ");
		strBuf.append("AND ST.CURSUBCD = ?");
		strBuf.append("AND ST.EXAMCD = EX.EXAMCD ");//[1] add
		strBuf.append("LEFT JOIN ");
		strBuf.append("UNIVMASTER_BASIC HS3 ");
		strBuf.append("ON HS3.EVENTYEAR = CR.EXAMYEAR ");
		strBuf.append("AND HS3.EXAMDIV = EX.EXAMDIV ");
		strBuf.append("AND HS3.UNIVCD = CR.UNIVCD ");
		strBuf.append("AND HS3.FACULTYCD = CR.FACULTYCD ");
		strBuf.append("AND HS3.DEPTCD = CR.DEPTCD ");
		strBuf.append("LEFT JOIN ");
		strBuf.append("PREVSUCCESS PS ");
		//strBuf.append("ON PS.EXAMYEAR = ST.YEAR ");//[1] delete
		strBuf.append("ON PS.EXAMYEAR = ST.EXAMYEAR ");//[1] add
		strBuf.append("AND PS.UNIVCD = CR.UNIVCD ");
		strBuf.append("AND PS.FACULTYCD = CR.FACULTYCD ");
		strBuf.append("AND PS.DEPTCD = CR.DEPTCD ");
		strBuf.append("AND PS.EXAMTYPECD = EX.EXAMTYPECD ");
		strBuf.append("AND PS.SUBCD = ST.SUBCD ");	//科目別成績（科目コード）
		strBuf.append("WHERE ");
		strBuf.append("CR.INDIVIDUALID = ? ");	//学歴基本情報（個人ＩＤ）
		strBuf.append("AND CR.EXAMYEAR = ? ");	//セッション（模試年度）
		strBuf.append("AND CR.EXAMCD = ? ");	//セッション（模試コード）
		strBuf.append("AND EX.EXAMTYPECD IN ('01','02','03')");	//マーク模試、記述模試、私大模試のみ
		strBuf.append("AND NOT EX.EXAMCD IN ('66','67')");		//（マーク高２、記述高２省く）
		strBuf.append("AND CR.CANDIDATERANK IN (#) ");	//画面（志望順位）
		strBuf.append("ORDER BY ");
		strBuf.append("CANDIDATERANK ASC ");
		
		return strBuf.toString();
		
	}
	
	//[1] add start
	/**
	 * 科目変換テーブル作成のための模試リストを取得するためのＳＱＬを作成する。
	 * @param year 対象模試年度
	 * @param cd 対象模試コード
	 * @param grade 年度フラグ
	 * @return
	 */
	private String getExamDatasSQL(String year, String cd, boolean grade) {
		StringBuffer strBuf = new StringBuffer();
		String DATAOPENDATE = null;
		if(login.isHelpDesk()) {
			DATAOPENDATE = "IN_DATAOPENDATE";
		} else {
			DATAOPENDATE = "OUT_DATAOPENDATE";
		}
		strBuf.append("SELECT ");
		strBuf.append("/*+ INDEX(SI PK_SUBRECORD_I) */ ");
		strBuf.append("EX.EXAMYEAR ");
		strBuf.append(",EX.EXAMCD ");
		strBuf.append("FROM ");
		strBuf.append("EXAMINATION EX ");
		strBuf.append("INNER JOIN ");
		strBuf.append("SUBRECORD_I SI ");
		strBuf.append("ON ");
		strBuf.append("EX.").append(DATAOPENDATE).append(" <= (SELECT ").append(DATAOPENDATE).append(" FROM EXAMINATION WHERE EXAMYEAR = '").append(year).append("' AND EXAMCD = '").append(cd).append("') ");
		strBuf.append("AND SI.INDIVIDUALID IN (#) ");
		strBuf.append("AND EX.EXAMTYPECD = (SELECT EXAMTYPECD FROM EXAMINATION WHERE EXAMYEAR = '").append(year).append("' AND EXAMCD = '").append(cd).append("') ");
		strBuf.append("AND EX.EXAMYEAR = SI.EXAMYEAR ");
		strBuf.append("AND EX.EXAMCD = SI.EXAMCD ");
		if(!grade) {
			strBuf.append("AND EX.EXAMYEAR = '").append(year).append("' ");
		}
		strBuf.append("GROUP BY EX.EXAMYEAR, EX.EXAMCD ");
		return strBuf.toString();
	}
	//[1] add end
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.I_SCORE_SHEET;
	}


}

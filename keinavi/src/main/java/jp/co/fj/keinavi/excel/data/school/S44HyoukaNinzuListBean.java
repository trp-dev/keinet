package jp.co.fj.keinavi.excel.data.school;

/**
 * �Z�����ѕ��́|���Z��r�|�u�]��w�]���ʐl�� �]���ʐl���f�[�^���X�g
 * �쐬��: 2004/07/20
 * @author A.Iwata
 *
 */
public class S44HyoukaNinzuListBean {
	//�͎��N�x
	private String strNendo = "";
	//�Z�����u�]�Ґ�
	private int intSoshibo = -999;
	//���u�]�Ґ�
	private int intDai1shibo = -999;
	//�]���ʐl�� A-E
	private int intHyoukaA = -999;
	private int intHyoukaB = -999;
	private int intHyoukaC = -999;
	private int intHyoukaD = -999;
	private int intHyoukaE = -999;

	// 2019/11/25 QQ)nagai �p��F�莎�������Ή� DEL START
	//// 2019/09/30 ���ʃe�X�g�Ή� ADD START
	////�]���ʐl�� A-E �܂�
	//private int intHyoukaA_Hukumu = -999;
	//private int intHyoukaB_Hukumu = -999;
	//private int intHyoukaC_Hukumu = -999;
	//private int intHyoukaD_Hukumu = -999;
	//private int intHyoukaE_Hukumu = -999;
	//// 2019/09/30 ���ʃe�X�g�Ή� ADD END
	// 2019/11/25 QQ)nagai �p��F�莎�������Ή� DEL END

	/*
	 * GET
	 */
	public String getStrNendo() {
		return this.strNendo;
	}
	public int getIntSoshibo() {
		return this.intSoshibo;
	}
	public int getIntDai1shibo() {
		return this.intDai1shibo;
	}
	public int getIntHyoukaA() {
		return this.intHyoukaA;
	}
	public int getIntHyoukaB() {
		return this.intHyoukaB;
	}
	public int getIntHyoukaC() {
		return this.intHyoukaC;
	}
	public int getIntHyoukaD() {
		return this.intHyoukaD;
	}
	public int getIntHyoukaE() {
		return this.intHyoukaE;
	}

	// 2019/11/25 QQ)nagai �p��F�莎�������Ή� DEL START
	//// 2019/09/30 ���ʃe�X�g�Ή� ADD START
	//public int getIntHyoukaA_Hukumu() {
	//	return this.intHyoukaA_Hukumu;
	//}
	//public int getIntHyoukaB_Hukumu() {
	//	return this.intHyoukaB_Hukumu;
	//}
	//public int getIntHyoukaC_Hukumu() {
	//	return this.intHyoukaC_Hukumu;
	//}
	//public int getIntHyoukaD_Hukumu() {
	//	return this.intHyoukaD_Hukumu;
	//}
	//public int getIntHyoukaE_Hukumu() {
	//	return this.intHyoukaE_Hukumu;
	//}
	//// 2019/09/30 ���ʃe�X�g�Ή� ADD END
	// 2019/11/25 QQ)nagai �p��F�莎�������Ή� DEL END

	/*
	 * SET
	 */

	public void setStrNendo(String strNendo) {
		this.strNendo = strNendo;
	}
	public void setIntSoshibo(int intSoshibo) {
		this.intSoshibo = intSoshibo;
	}
	public void setIntDai1shibo(int intDai1shibo) {
		this.intDai1shibo = intDai1shibo;
	}
	public void setIntHyoukaA(int intHyoukaA) {
		this.intHyoukaA = intHyoukaA;
	}
	public void setIntHyoukaB(int intHyoukaB) {
		this.intHyoukaB = intHyoukaB;
	}
	public void setIntHyoukaC(int intHyoukaC) {
		this.intHyoukaC = intHyoukaC;
	}
	public void setIntHyoukaD(int intHyoukaD) {
		this.intHyoukaD = intHyoukaD;
	}
	public void setIntHyoukaE(int intHyoukaE) {
		this.intHyoukaE = intHyoukaE;
	}

	// 2019/11/25 QQ)nagai �p��F�莎�������Ή� DEL START
	//// 2019/09/30 ���ʃe�X�g�Ή� ADD START
	//public void setIntHyoukaA_Hukumu(int intHyoukaA) {
	//	this.intHyoukaA_Hukumu = intHyoukaA;
	//}
	//public void setIntHyoukaB_Hukumu(int intHyoukaB) {
	//	this.intHyoukaB_Hukumu = intHyoukaB;
	//}
	//public void setIntHyoukaC_Hukumu(int intHyoukaC) {
	//	this.intHyoukaC_Hukumu = intHyoukaC;
	//}
	//public void setIntHyoukaD_Hukumu(int intHyoukaD) {
	//	this.intHyoukaD_Hukumu = intHyoukaD;
	//}
	//public void setIntHyoukaE_Hukumu(int intHyoukaE) {
	//	this.intHyoukaE_Hukumu = intHyoukaE;
	//}
	//// 2019/09/30 ���ʃe�X�g�Ή� ADD END
	// 2019/11/25 QQ)nagai �p��F�莎�������Ή� DEL END

}
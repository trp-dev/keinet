/**
 * 校内成績分析−過回比較　
 * 出力する帳票の判断
 * 作成日: 2004/08/16
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.school.S52Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

public class S52 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean s52( S52Item s52Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {

		KNLog log = KNLog.getInstance(null,null,null);

		try{
			int ret = 0;
			//S52Itemから各帳票を出力
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD START
//// 2019/09/13 DP)H.Nonaka 共通テスト対応 MOD START
////			if (( s52Item.getIntHyouFlg()==0 )&&( s52Item.getIntBnpGraphFlg()==0 )&&
////			( s52Item.getIntNinzuGraphFlg()==0 )&&( s52Item.getIntKoseiGraphFlg()==0 )) {
//			if (( s52Item.getIntHyouFlg()==0 )
//				&&( s52Item.getIntBnpGraphFlg()==0 )
//				&&( s52Item.getIntNinzuGraphFlg()==0 )
//				&&( s52Item.getIntKoseiGraphFlg()==0 )
//				&&( s52Item.getIntCheckBoxFlg()==0 )) {
//// 2019/09/13 DP)H.Nonaka 共通テスト対応 MOD END
			if (( s52Item.getIntHyouFlg()==0 )&&( s52Item.getIntBnpGraphFlg()==0 )&&
				( s52Item.getIntNinzuGraphFlg()==0 )&&( s52Item.getIntKoseiGraphFlg()==0 )) {
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD END
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
			    //throw new Exception("S52 ERROR : フラグ異常");
			    throw new IllegalStateException("S52 ERROR : フラグ異常");
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			}
			if (( s52Item.getIntHyouFlg()==1 )&&( s52Item.getIntKoseihiFlg()==2 )) {
				S52_01 exceledit = new S52_01();
				ret = exceledit.s52_01EditExcel( s52Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S52_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S52_01");
			}
			if (( s52Item.getIntHyouFlg()==1 )&&( s52Item.getIntKoseihiFlg()==1 )) {
				S52_02 exceledit = new S52_02();
				ret = exceledit.s52_02EditExcel( s52Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S52_02","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S52_02");
			}
			if (( s52Item.getIntBnpGraphFlg()==1 )&&( s52Item.getIntAxisFlg()==1 )) {
				S52_03 exceledit = new S52_03();
				ret = exceledit.s52_03EditExcel( s52Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S52_03","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S52_03");
			}
			if (( s52Item.getIntBnpGraphFlg()==1 )&&( s52Item.getIntAxisFlg()==2 )) {
				S52_04 exceledit = new S52_04();
				ret = exceledit.s52_04EditExcel( s52Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S52_04","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S52_04");
			}
			if (( s52Item.getIntNinzuGraphFlg()==1 )&&( s52Item.getIntNinzuPitchFlg()==1 )) {
				S52_05 exceledit = new S52_05();
				ret = exceledit.s52_05EditExcel( s52Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S52_05","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S52_05");
			}
			if (( s52Item.getIntNinzuGraphFlg()==1 )&&( s52Item.getIntNinzuPitchFlg()==2 )) {
				S52_06 exceledit = new S52_06();
				ret = exceledit.s52_06EditExcel( s52Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S52_06","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S52_06");
			}
			if (( s52Item.getIntNinzuGraphFlg()==1 )&&( s52Item.getIntNinzuPitchFlg()==3 )) {
				S52_07 exceledit = new S52_07();
				ret = exceledit.s52_07EditExcel( s52Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S52_07","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S52_07");
			}
			if (( s52Item.getIntKoseiGraphFlg()==1 )&&( s52Item.getIntKoseiPitchFlg()==1 )) {
				S52_08 exceledit = new S52_08();
				ret = exceledit.s52_08EditExcel( s52Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S52_08","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S52_08");
			}
			if (( s52Item.getIntKoseiGraphFlg()==1 )&&( s52Item.getIntKoseiPitchFlg()==2 )) {
				S52_09 exceledit = new S52_09();
				ret = exceledit.s52_09EditExcel( s52Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S52_09","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S52_09");
			}
			if (( s52Item.getIntKoseiGraphFlg()==1 )&&( s52Item.getIntKoseiPitchFlg()==3 )) {
				S52_10 exceledit = new S52_10();
				ret = exceledit.s52_10EditExcel( s52Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S52_10","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S52_10");
			}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/25 DP)H.Nonaka 共通テスト対応 ADD START
//			if (( s52Item.getIntCheckBoxFlg()==1 )) {
//				S52_11 exceledit = new S52_11();
//				ret = exceledit.s52_11EditExcel( s52Item, outfilelist, saveFlg, UserID );
//				if (ret!=0) {
//					log.Err("0"+ret+"S52_11","帳票作成エラー","");
//					return false;
//				}
//				sheetLog.add("S52_11");
//			}
//// 2019/09/25 DP)H.Nonaka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		} catch(Exception e) {
			log.Err("99S52","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
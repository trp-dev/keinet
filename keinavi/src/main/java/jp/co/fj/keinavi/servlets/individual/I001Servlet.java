package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.NewestExamDivSearchBean;
import jp.co.fj.keinavi.beans.individual.RefreshPlanUnivBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.forms.individual.I001Form;
import jp.co.fj.keinavi.util.KNCommonProperty;

/**
 * 
 * 個人成績分析・モード選択画面Servlet
 * 
 * 
 * 2005.12.20	Yoshimoto KAWAI - TOTEC
 * 				転送時に大学コード紐付け処理を追加 				
 * 
 * 
 * @author Administrator
 */
public class I001Servlet extends IndividualServlet {
	
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
			
		final HttpSession session = request.getSession(false); // セッション

		// JSP転送
		if ("i001".equals(getForward(request))) {
			// モードを分析モードで初期化する
			final ICommonMap map = (
					ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);
			if (map != null) map.setMode("0");

			super.forward(request, response, JSP_I001);
		
		// SERVLET転送
		} else {
			
			// Kei-Naviなら学校内全員の大学コード紐付け処理
			if ("i002".equals(getForward(request))
					&& !"kounai".equals(KNCommonProperty.getRealTimeSessionMode())) {
				univRefresh(request);
			}
			//モードをセットする
			final I001Form i001Form = (I001Form) getActionForm(request,
					"jp.co.fj.keinavi.forms.individual.I001Form");
			//個人専用オブジェクトをセッションに新規作成
			final ICommonMap iCommonMap = new ICommonMap();
			iCommonMap.setMode(i001Form.getMode());
			session.setAttribute(ICommonMap.SESSION_KEY, iCommonMap);
			
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

	private void univRefresh(
			final HttpServletRequest request) throws ServletException {
		
		final LoginSession login = getLoginSession(request);
		final NewestExamDivSearchBean newest = getNewestExamDivSearchBean();
		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);
			final RefreshPlanUnivBean bean = new RefreshPlanUnivBean(
					newest.getExamYear(), newest.getExamDiv(), login.getUserID());
			bean.setConnection(null, con);
			bean.execute();
			con.commit();
		} catch (final Exception e) {
			rollback(con);
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
	}
	
}

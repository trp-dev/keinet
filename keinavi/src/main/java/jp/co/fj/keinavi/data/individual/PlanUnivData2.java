package jp.co.fj.keinavi.data.individual;

import java.util.Map;
import java.util.TreeMap;

import jp.co.fj.kawaijuku.judgement.data.UnivSortKey;

/**
 * 
 * 受験予定大学一覧用データクラス
 * 
 * 
 * 2004.08.10	Tomohisa YAMADA - Totec
 * 				新規作成
 * 
 * <2010年度改修>
 * 2009.11.26	Tomohisa YAMADA - TOTEC
 * 				入試日程変更対応
 * 
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 * 
 * 
 */
public class PlanUnivData2 extends PlanUnivData implements UnivSortKey {
	
	private double cenScoreRate;		//得点率センターボーダー(難易度ランク)
	private String cenScoreRateStr;	//得点率センターボーダー文字列
	private double rankLLimits;		//偏差値難易度ランク(難易度ランク)
	private String rankLLimitsStr;		//偏差値難易度ランク文字列
	private String entExamModeCd;		//入試形態コード
	private String entExamModeName;	//入試形態名
	
	private String entExamDiv1;//入試区分1
	private String entExamDiv2;//入試区分2
	private String entExamDiv3;//入試区分3
	private String entExamDiv1Name;//入試区分名
	private String entExamDiv2Name;//入試区分名
	private String entExamDiv3Name;//入試区分名
	private String appliDeadline;		//出願締切日
	private String sucAnnDate;			//合格発表日
	private String proceDeadLine;		//入学手続締切日

	//受験日1〜10のスラッシュゼロ無しの文字列
	private String[] examPlanDateStr;
	//受験日1〜10
	private int[] examPlanDate;
	//受験地1〜10
	private String[] provEntExamSite;
	
	//1:出 2:受 3:合 4:締
	private String[] dateArray;
	
	/**
	 * 詳細情報(PlanUnivData2自身)のマップ
	 * (entExamModeCd+変更)entExamDiv1+entExamDiv2+entExamDiv3
	 * 
	 */
	private Map planMap = new TreeMap();
	
	//センター教科配点
	private String courseAllot1 = "";
	
	//二次教科配点
	private String courseAllot2 = "";
	
	//センター配点
	private String coursePntEn = "";		//英語(入試科目配点)
	private String coursePntMa = "";		//数学(入試科目配点)
	private String coursePntJap = "";		//国語(入試科目配点)
	private String coursePntSc = "";		//理科(入試科目配点)
	private String coursePntSo = "";		//地公(入試科目配点)
	private String coursePntEt = "";		//その他(入試科目配点)
	
	//二次配点
	private String coursePntEn2 = "";		//英語(入試科目配点):下段
	private String coursePntMa2 = "";		//数学(入試科目配点):下段
	private String coursePntJap2 = "";		//国語(入試科目配点):下段
	private String coursePntSc2 = "";		//理科(入試科目配点):下段
	private String coursePntSo2 = "";		//地公(入試科目配点):下段
	private String coursePntEt2 = "";		//その他(入試科目配点):下段
	
	//判定結果
	private String centerRating = "";		//センター(合格可能性判定)
	private String secondRating = "";		//二次(合格可能性判定)
	private String totalRating = "";		//総合(合格可能性判定)
	
	//ソート用キー
	private String natlPvtDiv;//国私区分
	private int flag_night; //夜間フラグ(大学夜間区分)
	private String facultyConCd; //学部内容コード
	private String deptSerialNo; //学科通番
	private String scheduleSys; //日程方式
	private String scheduleSysBranchCd; //日程方式枝番
	private String subNameKana; //学科カナ名
	private String kanaNum; //カナ付50音順番号

	/**
	 * 入試日程文字列を作成して返す
	 */
	public String getEntExamInpleDateStr() {
		
		//一般以外の場合は日程データを作成していないため
		//空文字を返す
		if (schedule == null) {
			return "";
		}
		
		return schedule.getInpleDateStr();
	}
	
	/**
	 * 入試形態まででユニークなキー
	 * 大学・学部・学科・入試形態
	 * @return
	 */
	public String getUniqueKey(){
		return univCd+deptCd+facultyCd;
	}
	
	/**
	 * 詳細情報ユニークキー
	 * entExamDiv3+entExamDiv2+entExamDiv1
	 * @return
	 */
	public String getEntExamDivKey(){
		return entExamDiv3+entExamDiv2+entExamDiv1;
	}
	
	/**
	 * @return
	 */
	public String getAppliDeadline() {
		return appliDeadline;
	}

	/**
	 * @return
	 */
	public String getCenterRating() {
		return centerRating;
	}

	/**
	 * @return
	 */
	public String getCoursePntEn() {
		return coursePntEn;
	}

	/**
	 * @return
	 */
	public String getCoursePntEn2() {
		return coursePntEn2;
	}

	/**
	 * @return
	 */
	public String getCoursePntEt() {
		return coursePntEt;
	}

	/**
	 * @return
	 */
	public String getCoursePntEt2() {
		return coursePntEt2;
	}

	/**
	 * @return
	 */
	public String getCoursePntJap() {
		return coursePntJap;
	}

	/**
	 * @return
	 */
	public String getCoursePntJap2() {
		return coursePntJap2;
	}

	/**
	 * @return
	 */
	public String getCoursePntMa() {
		return coursePntMa;
	}

	/**
	 * @return
	 */
	public String getCoursePntMa2() {
		return coursePntMa2;
	}

	/**
	 * @return
	 */
	public String getCoursePntSc() {
		return coursePntSc;
	}

	/**
	 * @return
	 */
	public String getCoursePntSc2() {
		return coursePntSc2;
	}

	/**
	 * @return
	 */
	public String getEntExamDiv1() {
		return entExamDiv1;
	}

	/**
	 * @return
	 */
	public String getEntExamDiv1Name() {
		return entExamDiv1Name;
	}

	/**
	 * @return
	 */
	public String getEntExamDiv2() {
		return entExamDiv2;
	}

	/**
	 * @return
	 */
	public String getEntExamDiv2Name() {
		return entExamDiv2Name;
	}

	/**
	 * @return
	 */
	public String getEntExamDiv3() {
		return entExamDiv3;
	}

	/**
	 * @return
	 */
	public String getEntExamDiv3Name() {
		return entExamDiv3Name;
	}

	/**
	 * @return
	 */
	public String getEntExamModeCd() {
		return entExamModeCd;
	}

	/**
	 * @return
	 */
	public String getEntExamModeName() {
		return entExamModeName;
	}

	/**
	 * @return
	 */
	public Map getPlanMap() {
		return planMap;
	}

	/**
	 * @return
	 */
	public String getProceDeadLine() {
		return proceDeadLine;
	}

	/**
	 * @return
	 */
	public String[] getProvEntExamSite() {
		return provEntExamSite;
	}

	/**
	 * @return
	 */
	public String getSecondRating() {
		return secondRating;
	}

	/**
	 * @return
	 */
	public String getSucAnnDate() {
		return sucAnnDate;
	}

	/**
	 * @return
	 */
	public String getTotalRating() {
		return totalRating;
	}

	/**
	 * @param string
	 */
	public void setAppliDeadline(String string) {
		appliDeadline = string;
	}

	/**
	 * @param string
	 */
	public void setCenterRating(String string) {
		centerRating = string;
	}

	/**
	 * @param string
	 */
	public void setCoursePntEn(String string) {
		coursePntEn = string;
	}

	/**
	 * @param string
	 */
	public void setCoursePntEn2(String string) {
		coursePntEn2 = string;
	}

	/**
	 * @param string
	 */
	public void setCoursePntEt(String string) {
		coursePntEt = string;
	}

	/**
	 * @param string
	 */
	public void setCoursePntEt2(String string) {
		coursePntEt2 = string;
	}

	/**
	 * @param string
	 */
	public void setCoursePntJap(String string) {
		coursePntJap = string;
	}

	/**
	 * @param string
	 */
	public void setCoursePntJap2(String string) {
		coursePntJap2 = string;
	}

	/**
	 * @param string
	 */
	public void setCoursePntMa(String string) {
		coursePntMa = string;
	}

	/**
	 * @param string
	 */
	public void setCoursePntMa2(String string) {
		coursePntMa2 = string;
	}

	/**
	 * @param string
	 */
	public void setCoursePntSc(String string) {
		coursePntSc = string;
	}

	/**
	 * @param string
	 */
	public void setCoursePntSc2(String string) {
		coursePntSc2 = string;
	}

	/**
	 * @param string
	 */
	public String getCoursePntSo() {
		return coursePntSo;
	}

	/**
	 * @param string
	 */
	public String getCoursePntSo2() {
		return coursePntSo2;
	}
	
	/**
	 * @param string
	 */
	public void setCoursePntSo(String string) {
		coursePntSo = string;
	}

	/**
	 * @param string
	 */
	public void setCoursePntSo2(String string) {
		coursePntSo2 = string;
	}
	
	/**
	 * @param string
	 */
	public void setEntExamDiv1(String string) {
		entExamDiv1 = string;
	}

	/**
	 * @param string
	 */
	public void setEntExamDiv1Name(String string) {
		entExamDiv1Name = string;
	}

	/**
	 * @param string
	 */
	public void setEntExamDiv2(String string) {
		entExamDiv2 = string;
	}

	/**
	 * @param string
	 */
	public void setEntExamDiv2Name(String string) {
		entExamDiv2Name = string;
	}

	/**
	 * @param string
	 */
	public void setEntExamDiv3(String string) {
		entExamDiv3 = string;
	}

	/**
	 * @param string
	 */
	public void setEntExamDiv3Name(String string) {
		entExamDiv3Name = string;
	}

	/**
	 * @param string
	 */
	public void setEntExamModeCd(String string) {
		entExamModeCd = string;
	}

	/**
	 * @param string
	 */
	public void setEntExamModeName(String string) {
		entExamModeName = string;
	}

	/**
	 * @param map
	 */
	public void setPlanMap(Map map) {
		planMap = map;
	}

	/**
	 * @param string
	 */
	public void setProceDeadLine(String string) {
		proceDeadLine = string;
	}

	/**
	 * @param strings
	 */
	public void setProvEntExamSite(String[] strings) {
		provEntExamSite = strings;
	}

	/**
	 * @param string
	 */
	public void setSecondRating(String string) {
		secondRating = string;
	}

	/**
	 * @param string
	 */
	public void setSucAnnDate(String string) {
		sucAnnDate = string;
	}

	/**
	 * @param string
	 */
	public void setTotalRating(String string) {
		totalRating = string;
	}

	/**
	 * @return
	 */
	public String[] getDateArray() {
		return dateArray;
	}

	/**
	 * @param strings
	 */
	public void setDateArray(String[] strings) {
		dateArray = strings;
	}

	/**
	 * @return
	 */
	public double getCenScoreRate() {
		return cenScoreRate;
	}

	/**
	 * @return
	 */
	public double getRankLLimits() {
		return rankLLimits;
	}

	/**
	 * @param d
	 */
	public void setCenScoreRate(double d) {
		cenScoreRate = d;
	}

	/**
	 * @param d
	 */
	public void setRankLLimits(double d) {
		rankLLimits = d;
	}

	/**
	 * @return
	 */
	public int[] getExamPlanDate() {
		return examPlanDate;
	}

	/**
	 * @return
	 */
	public String[] getExamPlanDateStr() {
		return examPlanDateStr;
	}

	/**
	 * @param is
	 */
	public void setExamPlanDate(int[] is) {
		examPlanDate = is;
	}

	/**
	 * @param strings
	 */
	public void setExamPlanDateStr(String[] strings) {
		examPlanDateStr = strings;
	}

	/**
	 * @return
	 */
	public String getCenScoreRateStr() {
		return cenScoreRateStr;
	}

	/**
	 * @return
	 */
	public String getRankLLimitsStr() {
		return rankLLimitsStr;
	}

	/**
	 * @param string
	 */
	public void setCenScoreRateStr(String string) {
		cenScoreRateStr = string;
	}

	/**
	 * @param string
	 */
	public void setRankLLimitsStr(String string) {
		rankLLimitsStr = string;
	}

	public String getCourseAllot1() {
		return courseAllot1;
	}

	public void setCourseAllot1(String string) {
		courseAllot1 = string;
	}

	public String getCourseAllot2() {
		return courseAllot2;
	}

	public void setCourseAllot2(String string) {
		courseAllot2 = string;
	}

	public String getNatlPvtDiv() {
		return natlPvtDiv;
	}

	public void setNatlPvtDiv(String natlPvtDiv) {
		this.natlPvtDiv = natlPvtDiv;
	}

	public int getFlag_night() {
		return flag_night;
	}

	public void setFlag_night(int flag_night) {
		this.flag_night = flag_night;
	}

	public String getFacultyConCd() {
		return facultyConCd;
	}

	public void setFacultyConCd(String facultyConCd) {
		this.facultyConCd = facultyConCd;
	}

	public String getDeptSerialNo() {
		return deptSerialNo;
	}

	public void setDeptSerialNo(String deptSerialNo) {
		this.deptSerialNo = deptSerialNo;
	}

	public String getScheduleSys() {
		return scheduleSys;
	}

	public void setScheduleSys(String scheduleSys) {
		this.scheduleSys = scheduleSys;
	}

	public String getScheduleSysBranchCd() {
		return scheduleSysBranchCd;
	}

	public void setScheduleSysBranchCd(String scheduleSysBranchCd) {
		this.scheduleSysBranchCd = scheduleSysBranchCd;
	}

	public String getSubNameKana() {
		return subNameKana;
	}

	public void setSubNameKana(String subNameKana) {
		this.subNameKana = subNameKana;
	}

	public String getKanaNum() {
		return kanaNum;
	}

	public void setKanaNum(String kanaNum) {
		this.kanaNum = kanaNum;
	}

	public String getUnivGDiv() {
		return getScheduleCd();
	}

}

package jp.co.fj.keinavi.beans.txt_out;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Connection;

import java.io.File;
import java.sql.ResultSet;

import jp.co.fj.keinavi.beans.security.IMenuDefine;
import jp.co.fj.keinavi.beans.security.MenuSecurityBean;
import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;

/**
 *
 * テキスト出力：集計データ：成績概況
 * 
 * 2004.10.12	[新規作成]
 * 
 *
 * @author Yukari OKUMURA - TOTEC
 * @version 1.0
 * 
 */
public class OutPutSougouSeiseki extends AbstractSheetBean{

	// 対象年度
	private String targetYear = null;
	// 対象模試
	private String targetExam = null;
	// 学校コード
	private String schoolCd = null;
	// ヘッダー情報
	private List header = null;
	
	// 他校コード
	private String OtherSchools = "";
	// 他校比較
	private int schoolDefFlg = 0;
	// クラス比較
	private int classDefFlg = 0;
	// 出力テキスト種別
	private int outType = 0;
	// 出力対象項目
	private Integer target[] = null;

	// 出力ファイル
	private File outPutFile = null;
	// コネクション
	private Connection con;
	
	public OutPutSougouSeiseki(){
		
	}
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return true;
	}

	public void execute()throws IOException, SQLException, Exception{
		
		OutPutParamSet set = new OutPutParamSet(login, exam, profile, "t002",sessionKey);
		
		final MenuSecurityBean menuSecurity = getMenuSecurityBean();
		schoolDefFlg = menuSecurity.isValid(IMenuDefine.MENU_TEXT_OTHER) ? set.getSchoolDefFlg() : 0;
		classDefFlg = menuSecurity.isValid(IMenuDefine.MENU_TEXT_CLASS) ? set.getClassDefFlg() : 0;

		targetYear = set.getTargetYear();
		targetExam = set.getTargetExam();
		schoolCd = profile.getBundleCD();
		header = set.getHeader();
		OtherSchools = set.getOtherSchools();
		outType =set.getOutType();
		target = set.getTarget();
		outPutFile =set.getOutPutFile();
		con = super.conn;
		
		OutputTextBean out = new OutputTextBean();
		out.setOutFile(outPutFile);	
		out.setOutputTextList(getSougouSeiseki());
		out.setHeadTextList(header);
		out.setOutTarget(target);
		out.setOutType(outType);
		out.execute();
		
		this.outfileList.add(outPutFile.getPath());
		sheetLog.add(outPutFile.getName());
	}
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.T_COUNTING_SCORE;
	}
	

	/**
	 * 	成績概況を取得
	 * 
	 * @return list 成績概況のレコードオブジェクト
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public LinkedList getSougouSeiseki()
		throws SQLException {
		
		// センターリサーチかどうか
		boolean cr = "38".equals(targetExam);
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		LinkedList list = new LinkedList();
		String query ="SELECT 0,s.EXAMYEAR,s.EXAMCD,em.EXAMNAME,1,'99',"
		// 全国の場合、都道府県名：'全国'  学校名：'(全国)'とする 
		// +" p.PREFNAME ,'99999' BUNDLECD,TO_CHAR('('|| p.PREFNAME ||')'),99 GRADE,'99' CLASS,"
		+" '全国','99999' BUNDLECD,TO_CHAR('('|| '全国' ||')'),99 GRADE,'99' CLASS,"
		+" es.SUBCD,es.SUBNAME,es.SUBALLOTPNT,s.NUMBERS,s.AVGPNT,"
		+" s.AVGSCORERATE,s.AVGDEVIATION"
		+" FROM SUBRECORD_A s, SUBRECORD_P sp,"
		+" EXAMINATION em, V_SM_CMEXAMSUBJECT es, PREFECTURE p"
		+" WHERE s.EXAMYEAR="+targetYear
		+" AND s.EXAMCD="+targetExam
		+" AND s.EXAMYEAR=em.EXAMYEAR" 
		+" AND s.EXAMCD=em.EXAMCD"
		+" AND s.EXAMYEAR=es.EXAMYEAR" 
		+" AND s.EXAMCD=es.EXAMCD"
		+" AND s.SUBCD=es.SUBCD"
		+" AND s.EXAMYEAR=sp.EXAMYEAR" 
		+" AND s.EXAMCD=sp.EXAMCD"
		+" AND s.SUBCD=sp.SUBCD"
		+" AND s.STUDENTDIV='0'"		// 現役高卒区分 0:計  1:現役  2:高卒
		+" AND sp.PREFCD = p.PREFCD"
		+" AND sp.PREFCD IN (SELECT FACULTYCD FROM SCHOOL sc WHERE BUNDLECD = '"+schoolCd+"')"
		+" UNION ALL"
		+" SELECT 0,s.EXAMYEAR,s.EXAMCD,em.EXAMNAME,2,s.PREFCD,"
		+" p.PREFNAME,(s.PREFCD || 999) BUNDLECD, TO_CHAR('('|| p.PREFNAME ||')'),99 GRADE,'99' CLASS,"
		+" es.SUBCD,es.SUBNAME,es.SUBALLOTPNT,s.NUMBERS,s.AVGPNT,"
		+" s.AVGSCORERATE,s.AVGDEVIATION"
		+" FROM SUBRECORD_P s,EXAMINATION em,"
		+" V_SM_CMEXAMSUBJECT es,PREFECTURE p"
		+" WHERE s.EXAMYEAR="+targetYear
		+" AND s.EXAMCD="+targetExam
		+" AND s.EXAMYEAR=em.EXAMYEAR" 
		+" AND s.EXAMCD=em.EXAMCD"
		+" AND s.EXAMYEAR=es.EXAMYEAR" 
		+" AND s.EXAMCD=es.EXAMCD"
		+" AND s.SUBCD=es.SUBCD"
		+" AND s.PREFCD = p.PREFCD"
		+" AND s.PREFCD IN (SELECT FACULTYCD FROM SCHOOL sc WHERE BUNDLECD = '"+schoolCd+"')"
		+" UNION ALL"
		+" SELECT 0,s.EXAMYEAR,s.EXAMCD,em.EXAMNAME,3,sc.FACULTYCD,"
		+" p.PREFNAME,s.BUNDLECD,sc.BUNDLENAME,99 GRADE,'99' CLASS,"
		+" es.SUBCD,es.SUBNAME,es.SUBALLOTPNT,s.NUMBERS,s.AVGPNT,"
		+" s.AVGSCORERATE,s.AVGDEVIATION"
		+" FROM SUBRECORD_S s,EXAMINATION em,"
		+" V_SM_CMEXAMSUBJECT es, SCHOOL sc,PREFECTURE p"
		+" WHERE s.EXAMYEAR="+targetYear
		+" AND s.EXAMCD="+targetExam
		+" AND s.BUNDLECD ='"+schoolCd+"'"
		+" AND s.EXAMYEAR=em.EXAMYEAR" 
		+" AND s.EXAMCD=em.EXAMCD"
		+" AND s.EXAMYEAR=es.EXAMYEAR "
		+" AND s.EXAMCD=es.EXAMCD"
		+" AND s.SUBCD=es.SUBCD"
		+" AND sc.BUNDLECD = s.BUNDLECD"
		+" AND sc.FACULTYCD= p.PREFCD";
		if(classDefFlg == 1){
			query+=" UNION ALL"
			+" SELECT 	0,s.EXAMYEAR,s.EXAMCD,em.EXAMNAME,4,sc.FACULTYCD,"
			+" p.PREFNAME,s.BUNDLECD,sc.BUNDLENAME,s.GRADE,s.CLASS,"
			+" es.SUBCD,es.SUBNAME,es.SUBALLOTPNT,s.NUMBERS,s.AVGPNT,"
			+" s.AVGSCORERATE,s.AVGDEVIATION"
			+" FROM SUBRECORD_C s,EXAMINATION em,"
			+" V_SM_CMEXAMSUBJECT es, SCHOOL sc,PREFECTURE p"
			+" WHERE s.EXAMYEAR="+targetYear
			+" AND s.EXAMCD="+targetExam
			+" AND s.BUNDLECD ='"+schoolCd+"'"
			+" AND s.EXAMYEAR=em.EXAMYEAR" 
			+" AND s.EXAMCD=em.EXAMCD"
			+" AND s.EXAMYEAR=es.EXAMYEAR "
			+" AND s.EXAMCD=es.EXAMCD"
			+" AND s.SUBCD=es.SUBCD"
			+" AND sc.BUNDLECD = s.BUNDLECD"
			+" AND sc.FACULTYCD= p.PREFCD";
		}
		if(schoolDefFlg==1){
			query +=" UNION ALL"
			+" SELECT 1,s.EXAMYEAR,s.EXAMCD,em.EXAMNAME,3,sc.FACULTYCD,"
			+" p.PREFNAME,s.BUNDLECD,sc.BUNDLENAME,99 GRADE,'99' CLASS,"
			+" es.SUBCD,es.SUBNAME,es.SUBALLOTPNT,s.NUMBERS,s.AVGPNT,"
			+" s.AVGSCORERATE,s.AVGDEVIATION"
			+" FROM SUBRECORD_S s,EXAMINATION em,"
			+" V_SM_CMEXAMSUBJECT es, SCHOOL sc,PREFECTURE p"
			+" WHERE s.EXAMYEAR="+targetYear
			+" AND s.EXAMCD="+targetExam
			+" AND s.BUNDLECD IN (" + OtherSchools+ ")"
			+" AND s.EXAMYEAR=em.EXAMYEAR" 
			+" AND s.EXAMCD=em.EXAMCD"
			+" AND s.EXAMYEAR=es.EXAMYEAR" 
			+" AND s.EXAMCD=es.EXAMCD"
			+" AND s.SUBCD=es.SUBCD"
			+" AND sc.BUNDLECD = s.BUNDLECD"
			+" AND sc.FACULTYCD= p.PREFCD";
		}
//		if(schoolDefFlg==1 && classDefFlg == 1){
//			query+=" UNION ALL"
//			+" SELECT 1,s.EXAMYEAR,s.EXAMCD,em.EXAMNAME,4,sc.FACULTYCD,"
//			+" p.PREFNAME,s.BUNDLECD,sc.BUNDLENAME,s.GRADE,s.CLASS,"
//			+" es.SUBCD,es.SUBNAME,es.SUBALLOTPNT,s.NUMBERS,s.AVGPNT,"
//			+" s.AVGSCORERATE,s.AVGDEVIATION"
//			+" FROM SUBRECORD_C s,EXAMINATION em,"
//			+" EXAMSUBJECT es, SCHOOL sc,PREFECTURE p"
//			+" WHERE s.EXAMYEAR="+targetYear
//			+" AND s.EXAMCD="+targetExam
//			+" AND s.BUNDLECD IN ("+ OtherSchools+")"
//			+" AND s.EXAMYEAR=em.EXAMYEAR "
//			+" AND s.EXAMCD=em.EXAMCD"
//			+" AND s.EXAMYEAR=es.EXAMYEAR" 
//			+" AND s.EXAMCD=es.EXAMCD"
//			+" AND s.SUBCD=es.SUBCD"
//			+" AND sc.BUNDLECD = s.BUNDLECD"
//			+" AND sc.FACULTYCD= p.PREFCD";
//		}
		
		query+=" ORDER BY 1, EXAMYEAR, EXAMCD, 5, BUNDLECD, GRADE, CLASS, SUBCD";
		
		try {
			
			stmt = con.prepareStatement(query);
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				LinkedHashMap data = new LinkedHashMap();
				
				// センターリサーチなら年度+1
				if (cr) {
					data.put(new Integer("1"), String.valueOf(rs.getInt(2) + 1));
				// それ以外
				} else {
					data.put(new Integer("1"), rs.getString(2));
				}

				data.put( new Integer("2"), rs.getString(3));
				data.put( new Integer("3"), rs.getString(4));
				data.put( new Integer("4"), rs.getString(5));
				data.put( new Integer("5"), rs.getString(6));
				data.put( new Integer("6"), rs.getString(7));
				data.put( new Integer("7"), rs.getString(8));
				data.put( new Integer("8"), rs.getString(9));
				data.put( new Integer("9"), rs.getString(10));
				data.put( new Integer("10"), rs.getString(11));
				data.put( new Integer("11"), rs.getString(12));
				data.put( new Integer("12"), rs.getString(13));
				data.put( new Integer("13"), rs.getString(14));
				data.put( new Integer("14"), rs.getString(15));
				if (rs.getString(16) == null) {
					data.put( new Integer("15"), rs.getString(16));								
				} else {
					data.put( new Integer("15"), String.valueOf(rs.getDouble(16)));								
				}
				if (rs.getString(17) == null) {
					data.put( new Integer("16"), rs.getString(17));
				} else {
					data.put( new Integer("16"), String.valueOf(rs.getDouble(17)));
				}
				if (rs.getString(18) == null) {
					data.put( new Integer("17"), rs.getString(18));				
				} else {
					data.put( new Integer("17"), String.valueOf(rs.getDouble(18)));				
				}
				list.add(data);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new RuntimeException("成績概況の抽出に失敗しました。");
			
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return list;
	}
	/**
	 * @param connection
	 */
	public void setCon(Connection connection) {
		con = connection;
	}

	/**
	 * @param string
	 */
	public void setOtherSchools(String string) {
		OtherSchools = string;
	}

	/**
	 * @param string
	 */
	public void setSchoolCd(String string) {
		schoolCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExam(String string) {
		targetExam = string;
	}

	/**
	 * @param string
	 */
	public void setTargetYear(String string) {
		targetYear = string;
	}

	/**
	 * @param list
	 */
	public void setHeader(LinkedList list) {
		header = list;
	}


	/**
	 * @return
	 */
	public File getOutPutFile() {
		return outPutFile;
	}

	/**
	 * @param i
	 */
	public void setOutType(int i) {
		outType = i;
	}

	/**
	 * @param i
	 */
	public void setSchoolDefFlg(int i) {
		schoolDefFlg = i;
	}

	/**
	 * @param i
	 */
	public void setClassDefFlg(int i) {
		classDefFlg = i;
	}

}
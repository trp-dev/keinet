package jp.co.fj.keinavi.beans.admin.application;
/*
 * $(#) $ld$
 *
 * All Rights Reserved, Copyright(c) 2004 FUJITSU KANSAI-CHUBU NETTECH LIMITED.
 *
 */

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * ツール管理下のディレクトリ位置の取得。
 *
 * @author $Author$
 * @author Yukari OKUMURA
 * @version $Revision: 1.00 $, $Date: 2004/03/31 17:30:00 $
 * @since 1.00
 *
 */
public class Application {

	// -- ツール配置ディレクトリを保存
	private static File keinaviDir;
	// -- resourcesディレクトリを保存
	private static File resourcesDir;
	// -- CSV出力ディレクトリ
	private static String csvPath;
	// -- SQL出力ディレクトリ
	private static String sqlPath;
	static{
		initialize();
	}


	/**
	 *
	 * 初期設定の読み込み。
	 *
	 * @return void
	 * @throws java.lang.RuntimeException keinavi.propertiesの読み込みに失敗した場合にスローされる。
	 * @exception java.lang.RuntimeException keinavi.propertiesの読み込みに失敗した場合にスローされる。
	 * @see java.lang.RuntimeException
	 */
	public static void initialize(){
		//keinavi.propertiesファイルを読み込む
		InputStream propin =
			Application.class.getResourceAsStream("/keinavi.properties");
		if (propin == null) {
			throw new RuntimeException("keinavi.propertiesファイルをWEB-INF/classesディレクトリに置いて下さい");
		}
		//keinaviPathエントリを読み込む
		Properties props = new Properties();
		try {
			props.load(propin);
		} catch (IOException ex) {
			throw new RuntimeException("keinavi.propertiesファイルの読みこみに失敗しました。");
		}
		String keinaviPath = props.getProperty("keinavi.path");
		if( keinaviPath == null ){
			throw new RuntimeException("keinavi.propertiesファイルにkeinavi.pathエントリがありません。");
		}
		//defPathエントリのフォルダが存在するかチェックする。
		File keinaviDir = new File(keinaviPath);
		if( !keinaviDir.exists()){
			System.out.println(keinaviDir.getPath());
			throw new RuntimeException("keinavi.propertiesファイルに記述されたディレクトリがありません。");
		}

		csvPath = props.getProperty("csv.path");

	        sqlPath = props.getProperty("sql.path");

		Application.keinaviDir = keinaviDir;
		setResourcesDir(keinaviDir);
	}


	/**
	 *
	 * resourcesディレクトリを記憶する。
	 *
	 * @param defPath ツールのフルパス
	 * @return void
	 * @throws java.lang.RuntimeException  binディレクトリの読み込み失敗した場合にスローされる。
	 * @exception java.lang.RuntimeException  binディレクトリの読み込み失敗した場合にスローされる。
	 * @see java.lang.RuntimeException
	 */
	private static void setResourcesDir(File defPath){
		//resourcesディレクトリを探す
		File resourcesDir = defPath;
		if( !resourcesDir.exists() ){
			throw new RuntimeException("resourcesディレクトリがありません。");
		}
		Application.resourcesDir = resourcesDir;
	}

	/**
	 * @return
	 */
	public static File getKeinaviDir() {
		return keinaviDir;
	}

	/**
	 * @return
	 */
	public static File getResourcesDir() {
		return resourcesDir;
	}

	/**
	 * @param file
	 */
	public static void setKeinaviDir(File file) {
		keinaviDir = file;
	}

	/**
	 * @return
	 */
	public static String getCsvPath() {
		return csvPath;
	}

        /**
         * @return
         */
        public static String getSqlPath() {
                return sqlPath;
        }

}

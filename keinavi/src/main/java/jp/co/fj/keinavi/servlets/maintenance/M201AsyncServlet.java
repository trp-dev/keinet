package jp.co.fj.keinavi.servlets.maintenance;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.maintenance.MAnswerSheetListBean;
import jp.co.fj.keinavi.beans.maintenance.MStudentListBean;
import jp.co.fj.keinavi.beans.maintenance.MTakenExamListBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.maintenance.M201Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 * 受験届修正画面非同期通信用サーブレット
 * 
 * 2006.10.05	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M201AsyncServlet extends DefaultHttpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// アクションフォーム
		final M201Form form = (M201Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.maintenance.M201Form");

		// 生徒情報のロード
		if ("1".equals(form.getActionMode())) {
			loadStundetList(request, response, form);
		}
		
		// 受験模試のロード
		if ("2".equals(form.getActionMode())) {
			loadTakenExam(request, response, form);
		}
		
		// 模試情報のロード
		if ("3".equals(form.getActionMode())) {
			loadAnswerSheetList(request, response, form);
		}
		
		// 生徒詳細のロード
		if ("4".equals(form.getActionMode())) {
			loadStundetDetail(request, response, form);
		}
	}

	// 生徒情報のロード
	private void loadStundetList(
			final HttpServletRequest request,
			final HttpServletResponse response,
			final M201Form form) throws ServletException, IOException {
		
		// ログイン情報
		final LoginSession login = getLoginSession(request);
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			MStudentListBean bean = new MStudentListBean(
					login.getUserID());
			bean.setConnection(null, con);
			bean.setTargetYear(form.getTargetYear1());
			bean.setTargetGrade(form.getTargetGrade1());
			bean.setTargetClass(form.getTargetClass1());
			bean.setInitialKana(form.getInitialKana1());
			bean.execute();
			request.setAttribute("StudentListBean", bean);
		} catch (final Exception e) {
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
		
		forward2Jsp(request, response, "m201_student");
	}
	
	// 受験模試のロード
	private void loadTakenExam(
			final HttpServletRequest request,
			final HttpServletResponse response,
			final M201Form form) throws ServletException, IOException {
		
		// ログイン情報
		final LoginSession login = getLoginSession(request);
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			MTakenExamListBean bean = new MTakenExamListBean(
					login.getUserID());
			bean.setConnection(null, con);
			bean.setTargetIndividualId(form.getTargetStudentId());
			bean.setHelpDesk(login.isHelpDesk());
			bean.execute();
			request.setAttribute("TakenExamListBean", bean);
		} catch (final Exception e) {
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}

		forward2Jsp(request, response, "m201_exam");
	}

	// 受験届のロード
	private void loadAnswerSheetList(
			final HttpServletRequest request,
			final HttpServletResponse response,
			final M201Form form) throws ServletException, IOException {
		
		// ログイン情報
		final LoginSession login = getLoginSession(request);
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			MAnswerSheetListBean bean = new MAnswerSheetListBean(
					login.getUserID());
			bean.setConnection(null, con);
			bean.setTargetYear(form.getTargetYear2());
			bean.setTargetGrade(form.getTargetGrade2());
			bean.setTargetClass(form.getTargetClass2());
			bean.setInitialKana(form.getInitialKana2());
			bean.setTargetExamCd(form.getTargetExam());
			bean.setHelpDesk(login.isHelpDesk());
			bean.execute();
			// セッションに保持する
			request.getSession(false).setAttribute(
					"AnswerSheetListBean", bean);
		} catch (final Exception e) {
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
		
		forward2Jsp(request, response, "m201_answer");
	}

	// 受験届詳細のロード
	private void loadStundetDetail(
			final HttpServletRequest request,
			final HttpServletResponse response,
			final M201Form form) throws ServletException, IOException {
		
		form.getClass();
		forward2Jsp(request, response, "m201_detail");
	}

}

package jp.co.fj.keinavi.beans.sheet.school.data;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * 高校リストデータ
 * 
 * 2006.08.31	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class S42_15BundleListData {

	// 一括コード
	private String bundleCd;
	// 一括名
	private String bundleName;
	// 模試年度
	private String examYear;
	// 模試年度における科目コード
	private String subCd;
	// 平均スコア
	private double avgPnt = -999;
	// 合計人数
	private int numbers = -999;
	// 親データクラス
	private S42_15SubjectListData subjectListData;
	// スコア帯データマップ
	private final Map scoreZoneListMap = new HashMap();
	
	// コンストラクタ
	protected S42_15BundleListData() {		
	}
	
	/**
	 * @param data 追加するスコア帯リストデータ
	 */
	public void addScoreZoneListData(final S42_15ScoreZoneListData data) {
		data.setBundleListData(this);
		scoreZoneListMap.put(data.getDevZoneCd(), data);
	}
	
	/**
	 * @return avgPnt を戻します。
	 */
	public double getAvgPnt() {
		return avgPnt;
	}
	/**
	 * @param pAvgPnt 設定する avgPnt。
	 */
	public void setAvgPnt(double pAvgPnt) {
		avgPnt = pAvgPnt;
	}
	/**
	 * @return bundleCd を戻します。
	 */
	public String getBundleCd() {
		return bundleCd;
	}
	/**
	 * @param pBundleCd 設定する bundleCd。
	 */
	public void setBundleCd(String pBundleCd) {
		bundleCd = pBundleCd;
	}
	/**
	 * @return bundleName を戻します。
	 */
	public String getBundleName() {
		return bundleName;
	}
	/**
	 * @param pBundleName 設定する bundleName。
	 */
	public void setBundleName(String pBundleName) {
		bundleName = pBundleName;
	}
	/**
	 * @return scoreZoneListMap を戻します。
	 */
	public Map getScoreZoneListMap() {
		return scoreZoneListMap;
	}
	/**
	 * @return examYear を戻します。
	 */
	public String getExamYear() {
		return examYear;
	}
	/**
	 * @param pExamYear 設定する examYear。
	 */
	public void setExamYear(String pExamYear) {
		examYear = pExamYear;
	}
	/**
	 * @return numbers を戻します。
	 */
	public int getNumbers() {
		return numbers;
	}
	/**
	 * @param pNumbers 設定する numbers。
	 */
	public void setNumbers(int pNumbers) {
		numbers = pNumbers;
	}
	/**
	 * @return subCd を戻します。
	 */
	public String getSubCd() {
		return subCd;
	}
	/**
	 * @param pSubCd 設定する subCd。
	 */
	public void setSubCd(String pSubCd) {
		subCd = pSubCd;
	}

	/**
	 * @return subjectListData を戻します。
	 */
	public S42_15SubjectListData getSubjectListData() {
		return subjectListData;
	}

	/**
	 * @param pSubjectListData 設定する subjectListData。
	 */
	public void setSubjectListData(S42_15SubjectListData pSubjectListData) {
		subjectListData = pSubjectListData;
	}
	
}

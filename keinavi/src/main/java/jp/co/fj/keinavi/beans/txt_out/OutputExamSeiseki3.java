package jp.co.fj.keinavi.beans.txt_out;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.util.KNUtil;

import org.apache.commons.lang.StringUtils;

/**
 *
 * 模試別成績データ（方式�B）
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)MORITA.Yuuichirou
 * @author QQ)Shimasaki.Toshiyuki       2015.03.30（理科基礎対応）
 *              個人科目別成績リスト（レイアウト�B）のヘッダ項目名を修正
 *
 */
public class OutputExamSeiseki3 extends OutputExamSeiseki {

    // serialVersionUID
    private static final long serialVersionUID = -8779746056513597767L;

    // 処理科目数
    private static final int SUBJECT_COUNT = 26;

    // 2015.03.30 ADD-S
    // 記述系利用設定情報
    private static final String SUBJECT_MASTER2 = "subjectmaster02.properties";
    // 2015.03.30 ADD-E

    // 実行関数
    public void execute() throws Exception {

        super.init();

        // 担当クラスを一時テーブルへ展開する
        super.setupCountChargeClass();

        OutPutParamSet set = new OutPutParamSet(3, login, exam, profile, "t104", sessionKey);

        List header = set.getHeader();
        File outPutFile = set.getOutPutFile();
        OutputTextBean out = new OutputTextBean();
        out.setOutFile(outPutFile);
        out.setOutputTextList(getAllData());
        //---------------------------------------------
        // [2004/11/09 nino] ヘッダ部の配点名を再設定
        // [2004/11/01 nino] ヘッダ部の科目名を再設定
        //---------------------------------------------
        header = setExamName(header, exam.getExamCD());
        //---------------------------------------------

        //10:個人情報数、6:ひとつの科目に対する項目数
        int startIndex = 10 + (SUBJECT_COUNT * 6);

        // ------------------------------------------------------------
        // 2005.02.16
        // Yoshimoto KAWAI - Totec
        // 余白部分の項目名書き換え
        // センターリサーチの場合は
        // 現文換算→現文得点
        // 古文換算→古文得点
        // 漢文換算→漢文得点
        //
        // マーク系
        if (mark) {
            ((String[]) header.get(startIndex++))[2] = "英語+L偏差値";
            ((String[]) header.get(startIndex++))[2] = this.cr ? "現文得点" : "現文換算点";
            ((String[]) header.get(startIndex++))[2] = this.cr ? "古文得点" : "古文換算点";
            ((String[]) header.get(startIndex++))[2] = this.cr ? "漢文得点" : "漢文換算点";
            ((String[]) header.get(startIndex++))[2] = this.ansno1 ? "第１解答理科" : "未使用A";
            ((String[]) header.get(startIndex++))[2] = this.ansno1 ? "第１解答地公" : "未使用B";
            ((String[]) header.get(startIndex++))[2] = this.ansno1 ? "未使用A" : "未使用C";
            ((String[]) header.get(startIndex++))[2] = this.ansno1 ? "未使用B" : "未使用D";
            ((String[]) header.get(startIndex++))[2] = this.ansno1 ? "未使用C" : "未使用E";
            // 記述系または高３プレステージ
        } else if (isWrtnOrPrivate) {
            //余白の位置から書き始める
            startIndex += 4;
            ((String[]) header.get(startIndex++))[2] = "英リス選択";
            // 2015.03.30 MOD-S
            //            ((String[]) header.get(startIndex++))[2] = "物理選択";
            //            ((String[]) header.get(startIndex++))[2] = "化学選択";
            //            ((String[]) header.get(startIndex++))[2] = "生物選択";
            //            ((String[]) header.get(startIndex++))[2] = "地学選択";
            // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
            String propertyfile = super.getPropertyFileName(exam);
            int index1 = propertyfile.indexOf("_");
            int index2 = propertyfile.indexOf(".");
            if (index1 > 0)
                propertyfile = propertyfile.substring(0, index1) + propertyfile.substring(index2);
            //if (super.getPropertyFileName(exam).equals(SUBJECT_MASTER2) ) {
            if (propertyfile.equals(SUBJECT_MASTER2)) {
                // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                ((String[]) header.get(startIndex++))[2] = "未使用F";
                ((String[]) header.get(startIndex++))[2] = "未使用G";
                ((String[]) header.get(startIndex++))[2] = "未使用H";
                ((String[]) header.get(startIndex++))[2] = "未使用I";
            } else {
                ((String[]) header.get(startIndex++))[2] = "物理選択";
                ((String[]) header.get(startIndex++))[2] = "化学選択";
                ((String[]) header.get(startIndex++))[2] = "生物選択";
                // 2020/04/14 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                if ("65".equals(exam.getExamCD()) && exam.getExamYear().compareTo(HEADER2019) < 0) {
                    ((String[]) header.get(startIndex++))[2] = "未使用I";
                } else {
                    ((String[]) header.get(startIndex++))[2] = "地学選択";
                }
                // 2020/04/14 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
            }
            // 2015.03.30 MOD-E
        }
        // ------------------------------------------------------------

        // ------------------------------------------------------------
        // 受験学力測定テスト対応
        if (KNUtil.isAbilityExam(exam)) {
            for (int i = 0; i < SUBJECT_COUNT; i++) {
                final String num = StringUtils.leftPad((i + 1) + "", 2, '0');
                ((String[]) header.get(13 + 6 * i))[2] = num + "スコア";
                ((String[]) header.get(14 + 6 * i))[2] = num + "エリア";
            }
        }
        // ------------------------------------------------------------

        // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
        //out.setHeadTextList(conv2Index(header));
        //out.setOutTarget(conv2Index(set.getTarget()));
        out.setHeadTextList(header);
        out.setOutTarget(set.getTarget());
        // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
        out.setOutType(set.getOutType());
        out.execute();

        this.outfileList.add(outPutFile.getPath());
        sheetLog.add(outPutFile.getName());
    }

    // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
    //    /**
    //     * ヘッダを項目番号からインデックス値に変換する。
    //     * @param headerList
    //     * @return
    //     */
    //    private List conv2Index(List headerList) {
    //
    //        for (Iterator ite = headerList.iterator(); ite.hasNext();) {
    //
    //            String[] values = (String[]) ite.next();
    //
    //          int value = Integer.parseInt(values[0]);
    //
    //          if (value >=9 && value<147) {
    //              values[0] = Integer.toString(value + 2);
    //          } else if (value >= 147 && value < 228) {
    //              values[0] = Integer.toString(value + 20);
    //          } else if (value >= 228 && value <246) {
    //              values[0] = Integer.toString(value - 79);
    //          } else if(value == 246 || value==247) {
    //              values[0] = Integer.toString(value -237);
    //          } else {
    //              values[0] = Integer.toString(value);
    //          }
    //        }
    //
    //        return headerList;
    //    }
    //
    //    /**
    //     * 選択項目を項目番号からインデックス値に変換する。
    //     * @param targets
    //     * @return
    //     */
    //    private Integer[] conv2Index(Integer[] targets) {
    //
    //        Integer[] converted = new Integer[targets.length];
    //
    //        for (int i = 0; i < targets.length; i ++) {
    //
    //
    //            int value = targets[i].intValue();
    //
    //            //147以降の場合は６つ後に変更
    //            if (value >=9 && value<147) {
    //                converted[i] = new Integer(value + 2);
    //            } else if (value >= 147 && value < 228) {
    //                converted[i] = new Integer(value + 20);
    //            } else if (value >= 228 && value <246) {
    //                converted[i] = new Integer(value - 79);
    //            } else if(value == 246 || value==247) {
    //                converted[i] = new Integer(value -237);
    //            } else {
    //                converted[i] = targets[i];
    //            }
    //
    ////            if (value >=9 && value<147) {
    ////                converted[i] = new Integer(value + 2);
    ////            } else if(value == 234 || value==235) {
    ////                converted[i] = new Integer(value -225);
    ////            }
    ////            else if (value >= 147 && value < 228) {
    ////                converted[i] = new Integer(value + 18);
    ////            } else if (value >= 228 && value <248) {
    ////                converted[i] = new Integer(value - 81);
    ////            } else {
    ////                converted[i] = targets[i];
    ////            }
    //        }
    //
    //
    //        return converted;
    //    }
    // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END

    /**
     * 結果を編集
     * @param seisekiMap
     * @param candMap
     * @return
     */
    protected LinkedList editData(LinkedHashMap seisekiMap, LinkedHashMap candMap) throws FileNotFoundException, IOException {

        // [2004/11/01 nino] ソート順を変更（学年、クラス、クラス番号）
        // 成績側の個人IDをソートする
        ArrayList mstList = new ArrayList();
        Set key = seisekiMap.keySet();
        Iterator it = key.iterator();
        while (it.hasNext()) {
            mstList.add((String) it.next());
        }
        // Collections.sort(mstList);  // ★[2004/11/01 nino] 個人IDでのソートをやめる★

        // 対象模試から表示順情報を取得する
        // [2004/11/10 nino] 模試科目1〜23で科目コード重複の対策でorderStr[][]を使用
        //                   orderStr[0] : ダミー
        //                   orderStr[1]〜orderStr[22] : 科目1〜科目22
        String[][] orderStr = new String[SUBJECT_COUNT + 1][];
        getShowOrder(exam, orderStr);

        // 各Mapから個人IDをkeyとする情報を取得
        Iterator mstit = mstList.iterator();
        LinkedList returnList = new LinkedList();
        //最初の行をセット
        //setFirstLise(returnList, targetExam);
        while (mstit.hasNext()) {
            String indkey = (String) mstit.next();
            LinkedList seisekiList = (LinkedList) seisekiMap.get(indkey);
            LinkedList candList = null;
            if (candMap.containsKey(indkey)) {
                candList = (LinkedList) candMap.get(indkey);
            } else {
                candList = new LinkedList();
            }

            // 個人データをセット
            setLineData(seisekiList, candList, orderStr, returnList);
        }

        return returnList;
    }

    /**
     * 模試ごとに参照するプロパティファイルを指定する
     * @param exam 対象模試
     * @return
     */
    protected String getPropertyFileName(ExamData exam) {
        return PREFIX_PROP + super.getPropertyFileName(exam);
    }

    /**
     * ヘッダ科目名を取得
     * @param targetExam
     * @return
     */
    protected String[] getHeadProperties(String targetExam) throws FileNotFoundException, IOException {

        String year = exam.getExamYear();
        String fileName = "head3.properties";
        int min = Integer.parseInt(HEADER2019);
        for (int i = Integer.parseInt(year); i > min; i--) {
            fileName = "head_".concat(String.valueOf(i)).concat(".properties");
            if (getClass().getResourceAsStream(fileName) != null) {
                break;
            }
        }
        InputStream in = OutputExamSeiseki3.class.getResourceAsStream(fileName);
        if (in == null) {
            throw new IllegalArgumentException("properties file not found.");
        }
        Properties prop = new Properties();
        try {
            // 設定を読み込む
            prop.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // ------------------------------------------------------------
        // 2005.02.16
        // Yoshimoto KAWAI - Totec
        // 項目名のグループ分け方法を変更
        //

        // 高１プレステージ
        if (KNUtil.isH1PStage(exam)) {
            return splitComma(prop.getProperty("HEADNAME13"));
        }

        // 高２プレステージ
        if (KNUtil.isH2PStage(exam)) {
            return splitComma(prop.getProperty("HEADNAME14"));
        }

        // 高３プレステージ
        if (KNUtil.isH3PStage(exam)) {
            return splitComma(prop.getProperty("HEADNAME15"));
        }

        // マーク高２
        if ("66".equals(exam.getExamCD())) {
            return splitComma(prop.getProperty("HEADNAME2"));

            // その他マーク系
        } else if ("01".equals(exam.getExamTypeCD())) {
            return splitComma(prop.getProperty("HEADNAME1"));

            // 高２記述
            // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
            //} else if ("65".equals(exam.getExamCD()) ) {
        } else if ("65".equals(exam.getExamCD()) && year.compareTo(HEADER2019) < 0) {
            return splitComma(prop.getProperty("HEADNAME12"));

            // その他記述系
            //} else if ("02".equals(exam.getExamTypeCD())){
        } else if ("02".equals(exam.getExamTypeCD()) || ("65".equals(exam.getExamCD()) && year.compareTo(HEADER2019) >= 0)) {
            // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
            return splitComma(prop.getProperty("HEADNAME3"));

            // 私大
        } else if ("03".equals(exam.getExamTypeCD())) {
            return splitComma(prop.getProperty("HEADNAME4"));

            // 高１記述
        } else if ("75".equals(exam.getExamCD())) {
            return splitComma(prop.getProperty("HEADNAME11"));

            // 高１
        } else if ("04".equals(exam.getExamTypeCD()) && "01".equals(exam.getTargetGrade())) {
            return splitComma(prop.getProperty("HEADNAME5"));

            // 高２
        } else if ("04".equals(exam.getExamTypeCD()) && "02".equals(exam.getTargetGrade())) {
            return splitComma(prop.getProperty("HEADNAME6"));

            // 模試種類コード04は1・2年生のみ
        } else if ("04".equals(exam.getExamTypeCD())) {
            throw new InternalError("マスタ不備：模試種類コード04は1・2年生のみ");

            // 受験学力測定テスト（高１用）
        } else if ("01".equals(exam.getTargetGrade()) && KNUtil.isAbilityExam(exam)) {
            return splitComma(prop.getProperty("HEADNAME8"));

            // 受験学力測定テスト（高２用）
        } else if ("02".equals(exam.getTargetGrade()) && KNUtil.isAbilityExam(exam)) {
            return splitComma(prop.getProperty("HEADNAME9"));

            // 受験学力測定テスト（高３用）
        } else if ("03".equals(exam.getTargetGrade()) && KNUtil.isAbilityExam(exam)) {
            return splitComma(prop.getProperty("HEADNAME10"));

            // オープン系
        } else {
            return splitComma(prop.getProperty("HEADNAME7"));

        }
        // ------------------------------------------------------------
    }

    /**
     * ヘッダ項目の配点（換算得点）、科目名をセット
     * @param headList
     * @param targetExam
     * @param sExamTypeMark
     */
    protected LinkedList setExamName(List headerList, String targetExam) throws FileNotFoundException, IOException {
        final int SEISEKISET = 6; // 成績データ集合
        final int SUBLENGTH = SUBJECT_COUNT; // 一行に表示する成績データの個数
        final int HEADLENGTH = 10; // 行ヘッダの長さ

        String[] headExam = getHeadProperties(targetExam);
        LinkedList headData = new LinkedList();

        // 科目名をセット
        String str = null;
        int iPos = 0;
        for (int listCnt = 0; listCnt < headerList.size(); listCnt++) {
            String[] header = (String[]) headerList.get(listCnt);

            if (HEADLENGTH <= listCnt && listCnt < HEADLENGTH + SEISEKISET * SUBLENGTH) {

                iPos = (listCnt - HEADLENGTH) % SEISEKISET;
                if (iPos == 1 && header.length > 2) {
                    // 科目名
                    str = header[2];
                    str = headExam[(int) ((listCnt - HEADLENGTH) / SEISEKISET)];
                    header[2] = str;
                }

                // [2004/11/09 nino] 配点（記述模試の場合）／換算得点（マーク模試の場合）
                //                   デフォルトは"配01"〜"配22"
                if (iPos == 2 && header.length > 2) {
                    str = header[2];
                    // ターゲット模試がマーク模試かをチェック
                    if (this.mark && !"05".equals(str.substring(0, 2)) && !"24".equals(str.substring(0, 2)) && !"25".equals(str.substring(0, 2)) && !"26".equals(str.substring(0, 2))) {
                        str = str.substring(0, 2) + "換算点";
                    }
                    header[2] = str;
                }
            }
            headData.add(header);
        }
        return headData;
    }

    /**
     * 行情報をセット
     * @param seisekiList 成績情報格納リスト（内部はString[10]）
     * @param candList    志望校情報格納リスト（内部はString[13]）
     * @param orderMap    表示順情報（key:科目コード、Object:String=表示位置１〜２２）
     * @param orderStr    表示順情報（[2004/11/10 nino] 模試コード重複対応用）
     * @param returnList
     */
    protected void setLineData(LinkedList seisekiList, LinkedList candList, String[][] orderStr, LinkedList returnList) {
        final int CANDIDATEPOS = 10 + (SUBJECT_COUNT * 6) + 10;
        final int SEISEKISET = 6;
        final int HEADLENGTH = 10; // 行ヘッダの長さ
        final int CANDSET = 8; // 志望校データ集合
        final String DEFAULT_VALUE = "9";
        final String DEFAULT_SORT_VALUE = "10000";
        // 配点／換算得点
        final int INDEX3;
        if (this.mark)
            INDEX3 = 12; // マーク系は換算得点
        else
            INDEX3 = 16; // それ以外は配点

        // マーク系の現古（科目７）の値
        String genko = null;
        // 採用した配点の値を覚えておく
        Map taken = new HashMap();

        // [2004/11/01 nino] 表示順序対応
        // 科目の位置で複数の科目が存在する場合は表示順序の若いものを表示する
        LinkedHashMap dispMap = new LinkedHashMap(); // 表示順序リスト

        LinkedHashMap lineMap = new LinkedHashMap();
        //行ヘッダを作成する
        String[] header = (String[]) seisekiList.get(0);
        lineMap.put(1, header[0]); // 通番（個人ID）
        lineMap.put(2, header[1]); // 年度
        lineMap.put(3, header[2]); // 模試コード
        lineMap.put(4, header[3]); // 学校コード
        lineMap.put(5, header[4]); // 学年
        lineMap.put(6, header[5]); // クラス
        lineMap.put(7, header[6]); // クラス番号
        lineMap.put(8, header[7]); // カナ氏名
        lineMap.put(9, header[8]); // 受験型
        lineMap.put(10, header[9]); // 文理コード

        // 記述系・私大の場合は不足情報のデフォルト値をセットしておく
        if (this.isWrtnOrPrivate) {

            int startIndex = 10 + (SUBJECT_COUNT * 6) + 5;

            lineMap.put(startIndex++, DEFAULT_VALUE);
            lineMap.put(startIndex++, DEFAULT_VALUE);
            lineMap.put(startIndex++, DEFAULT_VALUE);
            lineMap.put(startIndex++, DEFAULT_VALUE);
            lineMap.put(startIndex++, DEFAULT_VALUE);
        }

        Iterator seiIt = seisekiList.iterator();
        while (seiIt.hasNext()) {
            String[] seiseki = (String[]) seiIt.next();

            SeisekiData seisekiData = new SeisekiData(seiseki, this.open, this.cr);

            // [2004/10/29 nino] SUBSTR(C.SUBCD,1,3) → SUBSTR(C.SUBCD,1,4) に変更
            //                   ※テキスト出力は科目コード４桁に変更
            //                     科目コードのkeyは３桁のまま判定
            //String subcd = seiseki[8];					// 科目コードを取得
            //			String subcd;
            //			if (seiseki[8].length() >= 3) {
            //				subcd = seiseki[8].substring(0, 3);			// 科目コード先頭3桁を取得
            //			}else{
            //				subcd = seiseki[8];							// 科目コードを取得
            //			}
            // ------------------------------------------------------------
            // 2004.12.01
            // Yoshimoto KAWAI - Totec
            // オープン系は４桁のままにする
            String subcd;
            if (this.open) {
                subcd = seiseki[10]; // 科目コードを取得
            } else {
                subcd = seiseki[10].substring(0, 3); // 科目コード先頭3桁を取得
            }
            // ------------------------------------------------------------

            //------------------------------------------------------------------------------
            // [2004/11/10 nino] 模試コード重複対応
            //↓----------------------------------------------------------------------------
            //if (orderMap.containsKey(subcd)) {
            //	String pos = (String)orderMap.get(subcd);		// その科目の表示位置を取得
            //	// [nino 2004/10/25] 表示位置"9"からセットするため(-1)する
            //	// int iPos = new Integer(pos).intValue();		// 表示位置をkeyにMapに格納
            //	int iPos = new Integer(pos).intValue() - 1;		// 表示位置をkeyにMapに格納
            //------------------------------------------------------------------------------
            int subPos = 0; // 表示位置初期化（ダミー位置）
            while (subPos < orderStr.length) {
                subPos++;
                subPos = getSubjectPos(orderStr, subcd, subPos); // その科目の表示位置を取得
                if (subPos >= orderStr.length) {
                    break;
                }
                int iPos = subPos - 1; // 表示位置をkeyにMapに格納
                //↑----------------------------------------------------------------------------

                // ------------------------------------------------------------
                // 2005.02.15
                // Yoshimoto KAWAI - Totec
                // 特定の科目名をセットする
                setSubjectName(seiseki);
                // ------------------------------------------------------------

                //----------------------------------------------------------------------------------------
                // [2004/11/01 nino] 表示順序対応
                //↓--------------------------------------------------------------------------------------

                if (seiseki[15] == null) {
                    seiseki[15] = seiseki[10];
                }
                if (!dispMap.containsKey(iPos)) {
                    dispMap.put(iPos, DEFAULT_SORT_VALUE);
                }
                int dispSequence = Integer.valueOf(dispMap.get(iPos).toString()).intValue();
                if (dispSequence > Integer.valueOf(seiseki[15]).intValue()) {
                    // 表示順序格納
                    dispMap.put(iPos, seiseki[15]);
                    //↑--------------------------------------------------------------------------------------
                    int basePos = iPos * SEISEKISET + HEADLENGTH;
                    lineMap.put(basePos + 1, seiseki[10]);
                    lineMap.put(basePos + 2, seiseki[11]);
                    lineMap.put(basePos + 3, seiseki[INDEX3]);
                    lineMap.put(basePos + 4, seiseki[13]);
                    lineMap.put(basePos + 5, seiseki[14]);
                    lineMap.put(basePos + 6, seiseki[18]); // 学力レベル

                    // 配点で採用された科目は覚えておく
                    taken.put(basePos + 3, seiseki[16]);
                }
            }

            // マーク系のみ対応
            if (this.mark) {

                // リスニング偏差値
                if ("119".equals(subcd)) {
                    lineMap.put(167, seiseki[14]);
                }

                // 換算得点の処理
                String value;
                if (this.cr)
                    value = seiseki[13]; // センタリサーチは得点
                else
                    value = seiseki[12]; // それ以外は換算得点
                // 現文換算
                if ("312".equals(subcd) || "317".equals(subcd) || "357".equals(subcd)) {
                    lineMap.put(168, value);
                    genko = calculateCVSScore(genko, seiseki[12]);
                }
                // 古文換算
                if ("327".equals(subcd) || "367".equals(subcd) || "322".equals(subcd)) {
                    lineMap.put(169, value);
                    genko = calculateCVSScore(genko, seiseki[12]);
                }
                // 漢文換算
                if ("337".equals(subcd) || "377".equals(subcd) || "332".equals(subcd)) {
                    lineMap.put(170, value);
                }

                //第１解答科目対象模試
                if (this.ansno1) {
                    //第一解答科目フラグ
                    if (seisekiData.isAnswerNo1()) {
                        lineMap.put(seisekiData.getLineMapNo(), seisekiData.getAnswerNo1Flag());
                    }
                }

                // 記述系・私大のみの対応
            } else if (this.isWrtnOrPrivate) {
                // 英語不足
                if ("111".equals(subcd)) {
                    lineMap.put(171, seiseki[17]);

                    // 物理不足
                } else if ("428".equals(subcd) || "421".equals(subcd)) {
                    lineMap.put(172, seiseki[17]);

                    // 化学不足
                } else if ("438".equals(subcd) || "431".equals(subcd)) {
                    lineMap.put(173, seiseki[17]);

                    // 生物不足
                } else if ("448".equals(subcd) || "441".equals(subcd)) {
                    lineMap.put(174, seiseki[17]);

                    // 地学不足
                } else if ("458".equals(subcd)) {
                    lineMap.put(175, seiseki[17]);
                }

                // 2015.03.30 ADD-S
                // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                String propertyfile = super.getPropertyFileName(exam);
                int index1 = propertyfile.indexOf("_");
                int index2 = propertyfile.indexOf(".");
                if (index1 > 0)
                    propertyfile = propertyfile.substring(0, index1) + propertyfile.substring(index2);
                // 記述系の場合はデフォルト値：9 をセットする
                //if (super.getPropertyFileName(exam).equals(SUBJECT_MASTER2) ) {
                if (propertyfile.equals(SUBJECT_MASTER2)) {
                    // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                    lineMap.put(172, DEFAULT_VALUE);
                    lineMap.put(173, DEFAULT_VALUE);
                    lineMap.put(174, DEFAULT_VALUE);
                    lineMap.put(175, DEFAULT_VALUE);
                }
                // 2015.03.30 ADD-E

            }
        }

        // マーク系のみの対応
        if (this.mark) {
            // 現古の値を書き換える
            lineMap.put(49, genko);
            // 数学３は換算得点ではなく配点を入れる
            lineMap.put(37, taken.get(37));
            // 総合１は換算得点ではなく配点を入れる
            lineMap.put(151, taken.get(151));
            // 総合２は換算得点ではなく配点を入れる
            lineMap.put(157, taken.get(157));
            // 総合３は換算得点ではなく配点を入れる
            lineMap.put(163, taken.get(163));
        }

        Iterator candIt = candList.iterator();
        while (candIt.hasNext()) {
            String[] cand = (String[]) candIt.next();
            String candcd = cand[1]; // 志望順を取得
            int iCandPos = Integer.parseInt(candcd) - 1; // 表示位置をkeyにMapを格納
            int basePos = iCandPos * CANDSET + CANDIDATEPOS;
            lineMap.put(basePos, cand[11]); // 大学コード(5桁)
            lineMap.put(basePos + 1, cand[2]);
            lineMap.put(basePos + 2, cand[3]);
            lineMap.put(basePos + 3, cand[6]);
            lineMap.put(basePos + 4, cand[7]);
            lineMap.put(basePos + 5, cand[8]);
            lineMap.put(basePos + 6, cand[9]);
            lineMap.put(basePos + 7, cand[10]);
        }

        // 不足分のkeyをセット
        setLackKey(lineMap, 247);
        // Mapをソート

        // 行情報を追加
        returnList.add(lineMap);
    }

    /**
     * 現古の換算得点を計算して返す
     */
    private String calculateCVSScore(final String genko, final String value) {
        if (value == null)
            return genko;
        if (genko == null)
            return value;

        return String.valueOf(Integer.parseInt(value) + Integer.parseInt(genko));
    }

}

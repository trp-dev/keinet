/*
 * 作成日: 2004/06/30
 */
package jp.co.fj.keinavi.servlets.login;

import java.io.IOException;
import java.sql.Connection;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.login.StaffLoginBean;
import jp.co.fj.keinavi.beans.news.InformListBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.login.SectorData;
import jp.co.fj.keinavi.data.login.SectorSession;
import jp.co.fj.keinavi.forms.login.W001Form;
import jp.co.fj.keinavi.interfaces.ILogin;
import jp.co.fj.keinavi.interfaces.SpecialAppliMenu;
import jp.co.fj.keinavi.servlets.DefaultLoginServlet;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.KNCommonProperty;

/**
 * 職員用ログイン画面サーブレット<BR>
 * 
 * @author kawai
 */
public class WB001Servlet extends DefaultLoginServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = 2995966758737578736L;

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {

		// セッション
		HttpSession session = request.getSession();

		// アクションフォーム
		final W001Form form = (W001Form) getActionForm(request,
				"jp.co.fj.keinavi.forms.login.W001Form");

		Connection con = null; // コネクション
		// ---------------------------------------------------
		// お知らせリスト作成
		// 共通情報のみ
		// （ログインユーザID、ユーザモードをセットしない）
		// ---------------------------------------------------
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);

			InformListBean infbean = new InformListBean();
			infbean.setConnection(null, con);
			infbean.setUserID(""); // ユーザID
			infbean.setUserMode(0); // ユーザーモード
			infbean.setDisplayDiv("0"); // 表示区分 "0":お知らせ
			infbean.setTop(true);
			infbean.execute();
			request.setAttribute("InformListBean", infbean);
		} catch (final Exception e) {
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
		// ---------------------------------------------------

		// 転送元がログイン画面
		if ("wb001".equals(getBackward(request))) {
			con = null;
			try {
				con = getConnectionPool(request);

				// 営業部ログインBean
				StaffLoginBean bean = new StaffLoginBean();
				bean.setConnection(null, con);
				bean.setUserID(form.getAccount());
				bean.setUserPassword(form.getPassword());
				bean.execute();

				// 部門分類コードリストをセッションに保持
				session.setAttribute(SectorSession.SESSION_KEY,
						bean.getSectorSession());

				// 利用可能メニューIDをセッションに保持
				session.setAttribute(SpecialAppliMenu.SESSION_KEY,
						bean.getMenuIdSet());

				// ログインセッションを作る
				final LoginSession login = new LoginSession();
				login.setLoginID(form.getAccount());
				login.setUserMode(ILogin.SALES_NORMAL);
				login.setDbKey(KNCommonProperty.getNDBSID());
				session.setAttribute(LoginSession.SESSION_KEY, login);

				// 営業と校舎をカウントする
				int salesCount = 0;
				int cramCount = 0;
				Iterator ite = bean.getSectorSession().getSectorList()
						.iterator();
				while (ite.hasNext()) {
					SectorData data = (SectorData) ite.next();
					if ("1".equals(data.getHsBusiDivCD())) {
						salesCount++;
					} else if ("2".equals(data.getHsBusiDivCD())) {
						cramCount++;
					}
				}

				if (salesCount > 0 && cramCount > 0) {
					// 営業部かつ校舎
					actionLog(request, "101");
					forward(request, response, "/StaffSelect");
				} else if (salesCount > 0) {
					// 営業部
					actionLog(request, "101");
					forward(request, response, "/SalesSelect");
				} else if (cramCount > 0) {
					// 校舎
					// super.forward(request, response, "/CramSelect");
					forward(request, response, JSP_UC);
				} else {
					// 例外
					final KNServletException e = new KNServletException(
							form.getAccount() + " 部門分類コードの取得に失敗しました。");
					e.setErrorMessage("ＩＤまたはパスワードが違います。");
					e.setErrorCode("1");
					throw e;
				}

			} catch (final KNServletException e) {
				// ログイン失敗
				setErrorMessage(request, e);
				actionLog(request, "102", e.getMessage());
				forward(request, response, JSP_WB001);
			} catch (final Exception e) {
				// 想定外の例外
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

		} else {
			// そうでなければログイン画面を表示する
			session.invalidate();
			forward(request, response, JSP_WB001);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#getForward(javax.servlet.http.HttpServletRequest)
	 */
	protected String getForward(final HttpServletRequest request) {
		final String forward = super.getForward(request);
		if (forward == null) {
			return "wb001";
		} else {
			return forward;
		}
	}

}

package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−データリスト
 * 作成日: 2004/07/14
 * @author	H.Fujimoto
 */
public class C13ListBean {
	//学年
	private String strGrade = "";
	//クラス
	private String strClass = "";
	//総合成績データリスト
	private ArrayList c13AllDataList = new ArrayList();

	/*----------*/
	/* Get      */
	/*----------*/
	
	public ArrayList getC13AllDataList() {
		return this.c13AllDataList;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrGrade() {
		return this.strGrade;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	
	public void setC13AllDataList(ArrayList c13AllDataList) {
		this.c13AllDataList = c13AllDataList;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}

}
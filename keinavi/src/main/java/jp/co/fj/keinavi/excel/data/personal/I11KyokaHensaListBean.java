package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
//import java.util.ArrayList;
/**
 * �l���ѕ��́|���ѕ��͖ʒk ���ȕʕ΍��l���X�g
 * �쐬��: 2004/07/20
 * @author	A.Iwata
 * 
 * 2009.11.25   Fujito URAKAWA - Totec
 *              �u�w�̓��x���v�ǉ��Ή� 
 */
public class I11KyokaHensaListBean {
	//���Ȗ�
	private String strKyokamei = "";
	//�΍��l
	private float floHensa = 0;
	//�w�̓��x��
	private String strScholarLevel = "";
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKyokamei() {
		return this.strKyokamei;
	}
	public float getFloHensa() {
		return this.floHensa;
	}
	public String getStrScholarLevel() {
		return strScholarLevel;
	}
		
	/*----------*/
	/* Set      */
	/*----------*/
	
	public void setStrKyokamei(String strKyokamei) {
		this.strKyokamei = strKyokamei;
	}
	public void setFloHensa(float floHensa) {
		this.floHensa = floHensa;
	}
	public void setStrScholarLevel(String strScholarLevel) {
		this.strScholarLevel = strScholarLevel;
	}
}

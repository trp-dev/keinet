package jp.co.fj.keinavi.beans.sheet;

/**
 *
 * 設問別成績が存在しない場合の例外
 * 
 * 2006.07.19	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class NoQuestionException extends Exception {

}

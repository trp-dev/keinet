/*
 * 作成日: 2004/08/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * @author Administrator
 *
 * 2005.4.26 	K.Kondo 	[1]画面スクロール座標の追加
 */
public class I501Form extends ActionForm{

	private String targetPersonId;
	private String individualId;
	private String koIndividualId;
	private String button;
	private String page;
	private String firstRegistrYdt; 	//キー：初期登録日時
	private String scrollX;//[1] add サブミット前、表示座標Ｘ
	private String scrollY;//[1] add サブミット前、表示座標Ｙ
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.individual.ICommonForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ
		
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @return
	 */
	public String getButton() {
		return button;
	}

	/**
	 * @return
	 */
	public String getPage() {
		return page;
	}

	/**
	 * @param string
	 */
	public void setButton(String string) {
		button = string;
	}

	/**
	 * @param string
	 */
	public void setPage(String string) {
		page = string;
	}

	/**
	 * @return
	 */
	public String getFirstRegistrYdt() {
		return firstRegistrYdt;
	}

	/**
	 * @param string
	 */
	public void setFirstRegistrYdt(String string) {
		firstRegistrYdt = string;
	}
	
	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[1] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[1] add
	}

	/**
	 * @return individualId を戻します。
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @param pIndividualId 設定する individualId。
	 */
	public void setIndividualId(String pIndividualId) {
		individualId = pIndividualId;
	}

	/**
	 * @return koIndividualId を戻します。
	 */
	public String getKoIndividualId() {
		return koIndividualId;
	}

	/**
	 * @param pKoIndividualId 設定する koIndividualId。
	 */
	public void setKoIndividualId(String pKoIndividualId) {
		koIndividualId = pKoIndividualId;
	}


}

/*
 * 作成日: 2004/06/28
 *
 * バランスチャート表示用サーブレット
 *
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.help.OnepointBean;
import jp.co.fj.keinavi.beans.individual.I101Bean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I101Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * @author Totec) K.Kondo
 *
 * バランスチャートを表示する。
 * プロファイルからデータ取得するが、I103で再表示のときはフォームからデータ取得
 *
 * 2005.3.8 	Totec) K.Kondo      [1]change 現年度の年度条件を担当クラスの年度に変更
 * 2005.8.23 	Totec) T.Yamada 	[2]印刷での対象模試がセンターリサーチの時のチェックボックス無効時の対応
 *
 */
public class I101Servlet extends IndividualServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		final HttpServletRequest request,
		final HttpServletResponse response)
		throws ServletException, IOException{

		super.execute(request, response);

		//フォームデータの取得
		final I101Form i101Form = (I101Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.individual.I101Form");

		// ログイン情報
		final LoginSession login = getLoginSession(request);

		//プロファイル情報
		final Profile profile = getProfile(request);
		Short examType = (Short) profile.getItemMap(IProfileCategory.I_SCORE_SHEET).get(IProfileItem.EXAM_TYPE);
		Short studentDispGrade = (Short)profile.getItemMap(IProfileCategory.I_SCORE_SHEET).get(IProfileItem.STUDENT_DISP_GRADE);
		Short printStamp = (Short) profile.getItemMap(IProfileCategory.I_SCORE_SHEET).get(IProfileItem.PRINT_STAMP);
		Short printStudent = (Short) profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);

		//個人共通
		final ICommonMap iCommonMap = getICommonMap(request);

		// 転送元がJSPなら対象模試情報を初期化
		if ("i101".equals(getBackward(request))) {
			initTargetExam(request);
		}

		if ("i101".equals(getForward(request))) {
			// JSP転送
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);

				//ワンポイントアドバイス
				OnepointBean onebean = new OnepointBean();
				onebean.setConnection(null, con);
				onebean.setScreenID("i101");
				onebean.execute();
				request.setAttribute("OnepointBean", onebean);

				I101Bean i101Bean = new I101Bean();
				i101Bean.setClassYear(ProfileUtil.getChargeClassYear(profile));//[1] add

				if (!"i101".equals(getBackward(request))
						|| !iCommonMap.getTargetPersonId().equals(i101Form.getTargetPersonId())
						|| isChangedMode(request) || isChangedExam(request)) {

					// 生徒を切り替えたとき
					// モードを切り替えたとき
					// 対象模試を変えたとき
					if ("i101".equals(getBackward(request))) {

						//1.印刷対象生徒をセット
						String[] printTarget = {"0","0","0","0"};
						if(i101Form.getPrintTarget() != null){
							for(int i=0; i<i101Form.getPrintTarget().length; i++){
								printTarget[Integer.parseInt(i101Form.getPrintTarget()[i])] = "1";
							}
						}
						//[2] add start
						final String CENTER_EXAMCD = "38";
						if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
							printTarget[3] = "1";
						}
						//[2] add end
						i101Form.setPrintTarget(printTarget);
						//処理の実行
						i101Bean.setGrade(i101Form.getTargetGrade());
						i101Bean.setExamType(i101Form.getTargetExamType());
						i101Bean.setSearchCondition(
								i101Form.getTargetPersonId(),
								iCommonMap.getTargetExamYear(),
								iCommonMap.getTargetExamCode(),login.isHelpDesk());

					// 初回アクセス
					} else {
						/** プロファイルを使用 */
						//1.印刷対象生徒をセット
						String printTarget = (String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET);
						i101Form.setPrintTarget(CollectionUtil.splitComma(printTarget));

						/** Backwardがi102だったら2番.3番のプロファイルを使用しない       */
						if(!"i102".equals(getBackward(request))){
							//2.表示する学年をセット
							i101Form.setTargetGrade(studentDispGrade.toString());

							//3.模試の種類をセット
							i101Form.setTargetExamType(examType.toString());
						}

						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
						i101Form.setTargetGrade(iCommonMap.getTargetGrade());
						i101Form.setPrintStamp(iCommonMap.getPrintStamp());
						i101Form.setPrintStudent(iCommonMap.getPrintStudent());

						//4.セキュリティスタンプをセット
						//i101Form.setPrintStamp(printStamp.toString());

						//個人成績分析共通-印刷対象生保存をセット
						//i101Form.setPrintStudent(printStudent.toString());
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END

						//処理の実行
						i101Bean.setGrade(i101Form.getTargetGrade());
						i101Bean.setExamType(i101Form.getTargetExamType());
						i101Bean.setSearchCondition(
								iCommonMap.getTargetPersonId(),
								iCommonMap.getTargetExamYear(),
								iCommonMap.getTargetExamCode(),login.isHelpDesk());
					}

				// 再表示
				} else {
					//処理の実行
					i101Bean.setSearchCondition(i101Form.getTargetPersonId(), iCommonMap.getTargetExamYear(),
												iCommonMap.getTargetExamCode(), i101Form.getTargetGrade(),
												i101Form.getTargetExamType(),login.isHelpDesk());

					//1.印刷対象をフォームにセットしなおす
					String[] printTarget = {"0","0","0","0"};
					if(i101Form.getPrintTarget() != null){
						for(int i=0; i<i101Form.getPrintTarget().length; i++){
							printTarget[Integer.parseInt(i101Form.getPrintTarget()[i])] = "1";
						}
					}
					//[2] add start
					final String CENTER_EXAMCD = "38";
					if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
						printTarget[3] = "1";
					}
					//[2] add end
					i101Form.setPrintTarget(printTarget);

					/** フォームの値をプロファイルに保存　*/
					//2.表示する学年をセット
					profile.getItemMap(IProfileCategory.I_SCORE_SHEET).put(IProfileItem.STUDENT_DISP_GRADE, new Short(i101Form.getTargetGrade()));

					//3.模試の種類をセット
					profile.getItemMap(IProfileCategory.I_SCORE_SHEET).put(IProfileItem.EXAM_TYPE, new Short(i101Form.getTargetExamType()));
				}

				i101Bean.setConnection("", con);
				i101Bean.execute();
				request.setAttribute("i101Bean", i101Bean);

			} catch(final Exception e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request,con);
			}

			request.setAttribute("form", i101Form);
			iCommonMap.setTargetPersonId(i101Form.getTargetPersonId());
			super.forward(request, response, JSP_I101);

		// 不明ならServlet転送
		} else {

			if("i102".equals(getForward(request)) || "sheet".equals(getForward(request))){

				/** フォームの値をプロファイルに保存　*/
				//分析モードなら個人成績分析共通-印刷対象生保存をセット
				if(iCommonMap.isBunsekiMode()){
					profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(i101Form.getPrintStudent()));
				}

				//1.印刷対象生徒をセット
				String[] printTarget = {"0","0","0","0"};
				if(i101Form.getPrintTarget() != null){
					for(int i=0; i<i101Form.getPrintTarget().length; i++){
						printTarget[Integer.parseInt(i101Form.getPrintTarget()[i])] = "1";
					}
				}

				//[2] add start
				final String CENTER_EXAMCD = "38";
				if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
					printTarget[3] = "1";
				}
				//[2] add end

				profile.getItemMap(IProfileCategory.I_SCORE_COMMON).put(IProfileItem.PRINT_TARGET, CollectionUtil.deSplitComma(printTarget));

				//2.表示する学年をセット
				profile.getItemMap(IProfileCategory.I_SCORE_SHEET).put(IProfileItem.STUDENT_DISP_GRADE, new Short(i101Form.getTargetGrade()));

				//3.模試の種類をセット
				profile.getItemMap(IProfileCategory.I_SCORE_SHEET).put(IProfileItem.EXAM_TYPE, new Short(i101Form.getTargetExamType()));

				//4.セキュリティスタンプをセット
				profile.getItemMap(IProfileCategory.I_SCORE_SHEET).put(IProfileItem.PRINT_STAMP, new Short(i101Form.getPrintStamp()));

			} else {

				/** 102・印刷以外の場合プロファイルに保存　*/
				//分析モードなら個人成績分析共通-印刷対象生保存をセット
				if(iCommonMap.isBunsekiMode()){
					profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(i101Form.getPrintStudent()));
				}

				//1.印刷対象生徒をセット
				String[] printTarget = {"0","0","0","0"};
				if(i101Form.getPrintTarget() != null){
					for(int i=0; i<i101Form.getPrintTarget().length; i++){
						printTarget[Integer.parseInt(i101Form.getPrintTarget()[i])] = "1";
					}
				}
				//[2] add start
				final String CENTER_EXAMCD = "38";
				if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
					printTarget[3] = "1";
				}
				//[2] add end
				profile.getItemMap(IProfileCategory.I_SCORE_COMMON).put(IProfileItem.PRINT_TARGET, CollectionUtil.deSplitComma(printTarget));
			}

			iCommonMap.setTargetPersonId(i101Form.getTargetPersonId());
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD START
                        iCommonMap.setTargetGrade(i101Form.getTargetGrade());
                        iCommonMap.setPrintStamp(i101Form.getPrintStamp());
                        iCommonMap.setPrintStudent(i101Form.getPrintStudent());
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD END
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

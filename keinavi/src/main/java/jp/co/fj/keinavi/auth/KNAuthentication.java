/*
 * 作成日: 2004/08/18
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.auth;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import jp.co.kcn.sb.servlet.common.Encryption;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * 職員認証SOAP通信クラス （入塾用のクラスを継承して拡張）
 * 
 * 2010.04.06 Fujito URAKAWA - Totec 入塾用のクラスの継承を廃止 SORP認証からREST認証に変更
 * 
 * @author kawai
 */
public class KNAuthentication {

	/**
	 * staffAuthentication() メソッドのMap型返り値のキーを表す定数。認証結果。
	 */
	public static final String AUTH_RESULT = "authResult";

	/**
	 * staffAuthentication() メソッドのMap型返り値のキーを表す定数。セッションID。
	 */
	public static final String SESSION_ID = "sessionID";

	/**
	 * staffAuthentication() メソッドのMap型返り値のキーを表す定数。セッションID満了予定時刻。
	 */
	public static final String EXPIRATION = "expiration";

	/** REST接続先のベースURL */
	private final String restUrl;

	/**
	 * コンストラクタです
	 * 
	 * @param restUrl
	 *            REST接続先のベースURL
	 */
	public KNAuthentication(String restUrl) {
		this.restUrl = restUrl;
	}

	/**
	 * 
	 * @param sessionID
	 * @return
	 * @throws ServiceException
	 * @throws MalformedURLException
	 * @throws RemoteException
	 * @throws Exception
	 */
	public String[] getAccountingSectionList(String sessionID) throws Exception {

		HttpURLConnection urlcon = null;
		try {
			Map paramMap = new HashMap();
			// URLパラメータをセット
			paramMap.put("sessionID", sessionID);
			// URL文字列を作成する。
			String url = makeURLString("getAccountingSectionList", paramMap);
			// 上記パラメータをセットし、requestを投げる。
			urlcon = openConnection(url);
			// XmlDocumentを取得する。
			Document doc = getDocument(urlcon);
			// ルート要素を取得（タグ名：GetAccountingSectionList）
			Element root = doc.getDocumentElement();
			// SectionCodeの配列を返す
			return createSectionCodeArray(root
					.getElementsByTagName("accountingSectionCode"));
		} finally {
			if (urlcon != null) urlcon.disconnect();
		}
	}

	/**
	 * {@link NodeList}からSectionCodeの配列を生成します。
	 * 
	 * @param list
	 *            {@link NodeList}
	 * @return SectionCodeの配列
	 */
	private String[] createSectionCodeArray(NodeList list) {
		String[] array = new String[list.getLength()];
		for (int i = 0; i < list.getLength(); i++) {
			array[i] = list.item(i).getFirstChild().getNodeValue();
		}
		return array;
	}

	/**
	 * 職員認証 クライアント / Java版
	 * 
	 * @param ksncod
	 *            教職員コード(6桁)
	 * @param pwdkjn
	 *            パスワード(暗号化前のもの)
	 * @param gymcod
	 *            業務コード
	 * @return Map 職員認証サーバーからの取得したレスポンス KEY 名は、authResult , sessionID ,
	 *         expiration authResult 認証結果。"OK" 認証できた。"NG" 認証できなかった。 sessionID
	 *         セッションID 一意の文字列（32桁） expiration セッションIDが満了する時刻(YYYYMMDDHHMM)
	 * @throws ServiceException
	 * @throws MalformedURLException
	 * @throws RemoteException
	 * @throws Exception
	 */
	public Map staffAuthentication(String ksncod, String pwdkjn, String gymcod)
			throws Exception {

		Map res = new HashMap();
		HttpURLConnection urlcon = null;

		try {
			Map paramMap = new HashMap();
			// URLパラメータをセット
			paramMap.put("KSNCOD", ksncod);
			paramMap.put("PWDKJN", Encryption.encode(pwdkjn)); // 暗号化したパスワードを設定
			paramMap.put("GYMCOD", gymcod);
			// URL文字列を作成する。
			String url = makeURLString("StaffAuthentication", paramMap);
			// 上記パラメータをセットし、requestを投げる。
			urlcon = openConnection(url);
			// XmlDocumentを取得する。
			Document doc = getDocument(urlcon);
			// ルート要素を取得（タグ名：staffAuthentication）
			Element root = doc.getDocumentElement();
			// 戻り値用のMapデータを作成する。
			makeMapForStaffAuthentication(res, root);

			return res;
		} finally {
			urlcon.disconnect();
		}
	}

	/**
	 * REST認証実行用URL文字列を作成する。
	 * 
	 * @param s
	 * @param map
	 * @return
	 */
	private String makeURLString(final String s, final Map map) {

		return restUrl + s + "?" + makeURLParameter(map);
	}

	/**
	 * 戻り値用のMapを作成する（StaffAuthentication用）
	 * 
	 * @param res
	 * @param root
	 */
	private void makeMapForStaffAuthentication(Map res, Element root) {

		// authResult
		Element authResultEle = (Element) root.getElementsByTagName(AUTH_RESULT).item(0);
		// mapにセット
		res.put(AUTH_RESULT, authResultEle.getFirstChild().getNodeValue());
		// sessionID
		Element sessionIDEle = (Element) root.getElementsByTagName(SESSION_ID).item(0);
		if((sessionIDEle != null) && (sessionIDEle.getFirstChild() != null)){
			// mapにセット
			res.put(SESSION_ID, sessionIDEle.getFirstChild().getNodeValue());
		}
		// expiration
		Element expirationEle = (Element) root.getElementsByTagName(EXPIRATION).item(0);
		if((expirationEle != null) && (expirationEle.getFirstChild() != null)){
			// mapにセット
			res.put(EXPIRATION, expirationEle.getFirstChild().getNodeValue());
		}
	}

	/**
	 * 業務セキュリティ(画面の利用可能確認) クライアント / Java 版 PHP版より移植
	 * 
	 * @param ssnidt
	 *            セッションID(32桁)
	 * @param gymcod
	 *            業務コード
	 * @param mnucod
	 *            メニューコード
	 * @param gmnban
	 *            画面番号
	 * @return "OK" 認証できた / "NG" 認証できなかった。
	 * @throws ServiceException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws Exception
	 */
	public String validateCanUsingScreen(String ssnidt, String gymcod,
			String mnucod, String gmnban) throws Exception {

		HttpURLConnection urlcon = null;
		try {
			Map paramMap = new HashMap();
			// URLパラメータをセット
			paramMap.put("sessionID", ssnidt);
			paramMap.put("GYMCOD", gymcod);
			paramMap.put("MNUCOD", mnucod);
			paramMap.put("GMNBAN", gmnban);
			// URL文字列を作成する。
			String url = makeURLString("validateCanUsingScreen", paramMap);
			// 上記パラメータをセットし、requestを投げる。
			urlcon = openConnection(url);
			// XmlDocumentを取得する。
			Document doc = getDocument(urlcon);
			// ルート要素を取得（タグ名：staffAuthentication）
			Element root = doc.getDocumentElement();
			// 戻り用文字列をセット
			// authResult
			Element authResultEle = (Element) root.getElementsByTagName(
					AUTH_RESULT).item(0);
			// Stringにセット
			return authResultEle.getFirstChild().getNodeValue();
		} finally {
			if (urlcon != null) urlcon.disconnect();
		}
	}

	/**
	 * REST認証用の接続をOPENするメソッド
	 * 
	 * @param url
	 *            REST認証接続先（パラメタ文字列セット済）
	 * @throws IOException
	 */
	public HttpURLConnection openConnection(final String url)
			throws IOException {

		URL u = new URL(url);
		HttpURLConnection urlcon = (HttpURLConnection) u.openConnection();
		// GETでrequestを実施
		urlcon.setRequestMethod("GET");
		urlcon.setInstanceFollowRedirects(false);
		urlcon.setRequestProperty("Accept-Language", "ja;q=0.7,en;q=0.3");
		// 接続を開始
		urlcon.connect();

		return urlcon;
	}

	/**
	 * mapの(key,value)の組み合わせからGETパラメータ文字列を作成する。
	 * 
	 * @param map
	 *            パラメータマップ
	 */
	public String makeURLParameter(Map map) {

		StringBuffer sb = new StringBuffer();

		Set s = map.keySet();
		for (Iterator it = s.iterator(); it.hasNext();) {
			if (sb.length() > 0) sb.append("&");
			String key = (String) it.next();
			sb.append(key);
			sb.append("=");
			sb.append(map.get(key));
		}

		return sb.toString();
	}

	/**
	 * REST認証のresponseからXmlDocumentを取得します。
	 * 
	 * @param con
	 * @return
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 */
	public Document getDocument(HttpURLConnection urlcon)
			throws ParserConfigurationException, SAXException, IOException {

		// ドキュメントビルダーファクトリを生成
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		// ドキュメントビルダーを生成
		DocumentBuilder db = dbf.newDocumentBuilder();
		// Documentを取得
		Document doc = db.parse(urlcon.getInputStream());

		return doc;
	}

}

/*
 * 作成日: 2004/09/08
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.business;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.sheet.ExtB45KoukouListBean;
import jp.co.fj.keinavi.data.sheet.SheetSchoolData;
import jp.co.fj.keinavi.excel.business.B45;
import jp.co.fj.keinavi.excel.data.business.B45Item;
import jp.co.fj.keinavi.excel.data.business.B45ListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * 2005.04.27	Yoshimoto KAWAI - Totec
 * 				過回比較の対象期間を3年とした
 * 
 * @author kawai
 *
 */
public class B45SheetBean extends AbstractSheetBean {

	// データクラス
	private final B45Item	data = new B45Item();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		String[] code = super.getSubjectCDArray(); // 型・科目設定値
		List compSchoolList = super.getCompSchoolList(); // 比較対象高校
		boolean isNewExam = KNUtil.isNewExam(exam); // 新テストかどうか

		// データを詰める
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ

		// 過去の同系統の模試
		final List examList = new LinkedList();
		Query query2 = QueryLoader.getInstance().load("b45_1");
		super.outDate2InDate(query2); // データ開放日書き換え

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query2.toString());
			ps.setString(1, exam.getExamTypeCD()); // 模試種類コード
			ps.setString(2, exam.getExamYear()); // 模試年度
			ps.setInt(3, Integer.parseInt(exam.getTargetGrade())); // 対象学年
			ps.setString(4, String.valueOf(Integer.parseInt(exam.getExamYear()) - 1)); // 模試年度（前年度）
			ps.setInt(5, Integer.parseInt(exam.getTargetGrade()) - 1); // 対象学年（前年度）
			ps.setString(6, String.valueOf(Integer.parseInt(exam.getExamYear()) - 2)); // 模試年度（前々年度）
			ps.setInt(7, Integer.parseInt(exam.getTargetGrade()) - 2); // 対象学年（前々年度）
			ps.setString(8, exam.getDataOpenDate()); // データ開放日			
			
			rs = ps.executeQuery();
			while (rs.next()) {
				ExamData e = new ExamData();
				e.setExamYear(rs.getString(1));
				e.setExamCD(rs.getString(2));
				e.setExamNameAbbr(rs.getString(3));
				e.setInpleDate(rs.getString(4));
				e.setDataOpenDate(rs.getString(5));
				examList.add(e);
				// TOP7
				if (examList.size() == 7) {
					break;
				}
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}

		// ワークテーブル
		super.insertIntoSubCDTrans((ExamData[]) examList.toArray(new ExamData[0]));

		PreparedStatement ps1 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		ResultSet rs1 = null;
		ResultSet rs3 = null;
		try {
			List c1 = new LinkedList(); // 型の入れ物
			List c2 = new LinkedList(); // 科目の入れ物
						
			// データリストの取得
			Query query1 = QueryLoader.getInstance().load("s22_1");
			query1.setStringArray(1, code); // 型・科目コード

			ps1 = conn.prepareStatement(query1.toString());
			ps1.setString(1, exam.getExamYear()); // 対象年度
			ps1.setString(2, exam.getExamCD()); // 対象模試

			// 高校別データリスト（全国）
			ps3 = conn.prepareStatement(QueryLoader.getInstance().load("b45_2").toString());

			// 高校別データリスト（都道府県）
			ps4 = conn.prepareStatement(QueryLoader.getInstance().load("b45_3").toString());

			// 高校別データリスト（学校）
			Query query3 = QueryLoader.getInstance().load("b45_4");
			super.outDate2InDate(query3); // データ開放日書き換え
			ps5 = conn.prepareStatement(query3.toString());
						
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				B45ListBean	bean = new B45ListBean();
				bean.setStrKmkCd(rs1.getString(1));
				bean.setStrKmkmei(rs1.getString(2));
				bean.setIntHaitenKmk(rs1.getInt(3));
				
				if (rs1.getInt(1) >= 7000) c1.add(bean); // 型
				else c2.add(bean); // 科目

				Iterator it = examList.iterator();
				while (it.hasNext()) {
					ExamData e = (ExamData) it.next();
					
					// 模試年度
					ps3.setString(2, e.getExamYear());
					ps4.setString(1, e.getExamYear());
					ps5.setString(3, e.getExamYear());
					// 模試コード
					ps3.setString(3, e.getExamCD());
					ps4.setString(3, e.getExamCD());
					ps5.setString(4, e.getExamCD());
					// 型・科目コード
					ps3.setString(1, bean.getStrKmkCd());
					ps4.setString(2, bean.getStrKmkCd());
					ps5.setString(2, bean.getStrKmkCd());

					// 学校分繰り返す
					int count = 0;
					Iterator ite = compSchoolList.iterator();
					while (ite.hasNext()) {
						SheetSchoolData school = (SheetSchoolData) ite.next(); 

						ExtB45KoukouListBean k = new ExtB45KoukouListBean();
						k.setExamYear(e.getExamYear());
						k.setStrKakaiMshmei(e.getExamNameAbbr());
						k.setStrKakaiMshDate(e.getInpleDate());
						k.setDataOpenDate(e.getDataOpenDate());
						bean.getB45KoukouList().add(k);

						switch (school.getSchoolTypeCode()) {
							// 全国
							case 1:
								rs3 = ps3.executeQuery();	
								break;
						
							// 都道府県
							case 2:
								ps4.setString(4, school.getPrefCD()); // 県コード
								rs3 = ps4.executeQuery();					
								break;
						
							// 高校
							case 3:
								ps5.setString(1, school.getSchoolCD()); // 一括コード
								rs3 = ps5.executeQuery();
								break;
						}

						// 詰める					
						if (rs3.next()) {
							k.setSchoolDispSeq(count++);
							k.setStrGakkomei(rs3.getString(1));
							k.setIntNinzu(rs3.getInt(2));

							// 新テストなら平均点→無効値／平均偏差値→平均点
							if (isNewExam) {
								k.setFloHeikin(-999);
								k.setFloHensa(rs3.getFloat(3));
							} else {
								k.setFloHeikin(rs3.getFloat(3));
								k.setFloHensa(rs3.getFloat(4));
							}
						}
					
						rs3.close();
					}
				}
				// ソートしておく
				Collections.sort(bean.getB45KoukouList());
			}

			// 型・科目の順番に詰める
			data.getB45List().addAll(c1);
			data.getB45List().addAll(c2);
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs3);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps3);
			DbUtils.closeQuietly(ps4);
			DbUtils.closeQuietly(ps5);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("B45_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_PAST_SCORE;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new B45()
			.b45(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
	}

}

/*
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.ExamData;//[3] add
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.IExamData;
import jp.co.fj.keinavi.data.sheet.ExtI11ListBean;
import jp.co.fj.keinavi.excel.data.personal.I11GouChartListBean;
import jp.co.fj.keinavi.excel.data.personal.I11ShiboGouListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

/**
 * 個人成績分析−成績分析面談 バランスチャートデータリスト(合格者平均)印刷用Bean
 *
 * @author
 * history
 *   Symbol Date       Person     Note
 *   [1]    2005.01.21 kondo      change ２次 英語＋リスニング追加対応
 *   [2]    2005.01.21 kondo      change 配点200点の科目が複数ある場合最初の1つをとるように修正
 *   [3]    2005.03.02 kondo      change 科目変換に、模試コードを追加。
 *   [4]    2005.04.14 kondo      change 面談シートフラグが１だった場合もデータ取得を行うよう修正
 * 	 [5]    2005.04.14 kondo      対象模試が新テストだった場合、種別フラグに"1"を入れるように修正
 * 	 [6]    2005.05.29 kondo      過去合格者平均の英語は科目コード1000に固定
 * 2009.11.30   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 */

public class I12_01_2SheetBean extends IndAbstractSheetBean {

    private final Short menSheetFlg;

    /**
     * コンストラクタです。
     * @param menSheetFlg
     */
    public I12_01_2SheetBean(Short menSheetFlg) {
        super();
        this.menSheetFlg = menSheetFlg;
    }
    /**
     * コンストラクタです。
     */
    public I12_01_2SheetBean() {
        super();
        this.menSheetFlg = null;
    }

    /* (非 Javadoc)
     * @see com.fjh.beans.DefaultBean#execute()
     */

    public void execute() throws SQLException, Exception {

        final String ELSUBCD = "1190";//[1] add 英語＋リスニング

        //セッション情報
        ICommonMap commonMap = super.getICommonMap();

        //プロファイル関連
        int MultiExam; // １教科複数受験
        if(!"i003".equals(backward)){
            /**i104バランスチャート(合格者平均)の場合**/

            //セキュリティスタンプ
            Short printStamp = (Short) profile.getItemMap(IProfileCategory.I_SCORE_BC).get(IProfileItem.PRINT_STAMP);
            int SecuStanp = Integer.parseInt(printStamp.toString());

            //教科分析シートフラグ
            data.setIntKyoSheetFlg(1) ;															// 教科分析フラグ
            data.setIntKyoSheetSecuFlg(SecuStanp);												// 教科分析シートセキュリティスタンプ
            data.setIntKyoSheetShubetsuFlg(getNewTestFlg());//[5] add 新テストフラグ（教科シート）

            //１教科複数受験
            Short indMultiExam = (Short)profile.getItemMap(IProfileCategory.I_SCORE_BC_AVG).get(IProfileItem.IND_MULTI_EXAM);
            MultiExam = Integer.parseInt(indMultiExam.toString());
        }else{
            /**i003簡単印刷の場合**/
            //教科分析シートフラグ
            String []KyoSheetFlg = CollectionUtil.splitComma((String) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_FORMS));

            //セキュリティスタンプ
            Short printStamp = (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.PRINT_STAMP);
            int SecuStanp = Integer.parseInt(printStamp.toString());

            //[4] add start
            //面談シートフラグ
            Short MenSheet = menSheetFlg != null ? menSheetFlg : (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_SHEET);
            int MenSheetFlg = Integer.parseInt(MenSheet.toString());
            if(MenSheetFlg == 5) {
                data.setIntMenSheetFlg(MenSheetFlg) ;
                data.setIntMenSheetSecuFlg(SecuStanp);
                data.setIntMenSheetShubetsuFlg(getNewTestFlg());//[5] add 新テストフラグ（面談シート）
            }
            //面談シート１の制限
            if (MenSheetFlg == 5 && !isAvailableSheet1()) {
                data.setIntMenSheetFlg(0);
            }
            //[4] add end

            if(KyoSheetFlg[0].equals("1")) {
                data.setIntKyoSheetFlg(Integer.parseInt(KyoSheetFlg[0]));// 教科分析フラグ
                data.setIntKyoSheetSecuFlg(SecuStanp);// 教科分析シートセキュリティスタンプ
                data.setIntKyoSheetShubetsuFlg(getNewTestFlg());//[5] add 新テストフラグ（教科シート）
            }

            //１教科複数受験
            // 第1解答科目対応模試→ 第1解答科目
            // それ以外→平均
            if(KNUtil.isAns1st(commonMap.getTargetExamCode())){
                MultiExam = 3;
            }else{
                MultiExam = 1;
            }
        }

        /**マーク模試かどうか調べる**/
        boolean examflg = false;
        if(exam.getExamTypeCD().equals("01")){
            examflg = true;
        }

        /*下記条件の時に値を格納
        ・面談シートフラグ=2or4
        ・教科分析シートフラグ=1*/
        if (data.getIntKyoSheetFlg() == 1 || data.getIntMenSheetFlg() == 5) {//[4] add
        //if (data.getIntKyoSheetFlg() == 1) {//[4] delete

            /*******項番２SQL＿選択生徒情報取得***********************************************************/
            PreparedStatement pstmt1 = null;
            ResultSet rs1 = null;
            PreparedStatement pstmt2 = null;
            ResultSet rs2 = null;
            PreparedStatement pstmt3 = null;
            ResultSet rs3 = null;
            PreparedStatement pstmt4 = null;
            ResultSet rs4 = null;
            PreparedStatement pstmt5 = null;
            ResultSet rs5 = null;
            PreparedStatement pstmt6 = null;
            ResultSet rs6 = null;
            PreparedStatement pstmt7 = null;
            ResultSet rs7 = null;
            PreparedStatement pstmt8 = null;
            ResultSet rs8 = null;
            PreparedStatement pstmt9 = null;
            ResultSet rs9 = null;
            PreparedStatement pstmt10 = null;
            ResultSet rs10 = null;
            PreparedStatement pstmt11 = null;
            ResultSet rs11 = null;
            PreparedStatement pstmt12 = null;
            ResultSet rs12 = null;
            PreparedStatement pstmt13 = null;
            ResultSet rs13 = null;
            PreparedStatement pstmt14 = null;
            ResultSet rs14 = null;
            PreparedStatement pstmt15 = null;
            ResultSet rs15 = null;
            PreparedStatement pstmt16 = null;
            ResultSet rs16 = null;

            //科目年度またぎ
            conn.setAutoCommit(false);
            //this.insertIntoSubCDTrans();
            ExamData[] examDatas = new ExamData[1];//[3] add
            examDatas[0] = new ExamData();//[3] add
            examDatas[0].setExamYear(commonMap.getTargetExamYear());//[3] add
            examDatas[0].setExamCD(commonMap.getTargetExamCode());//[3] add
            this.insertIntoSubCDTrans(examDatas);//[3] add

            for (java.util.Iterator it=data.getI11List().iterator(); it.hasNext();) {
                ExtI11ListBean printData = (ExtI11ListBean) it.next();

                String rank1 ="1";
                String rank2 ="2";
                String rank3 ="3";
                //画面で選択された生徒なら、画面で選択された順位にする
                if(!"i104".equals(backward) || !commonMap.getTargetPersonId().equals(printData.getIndividualid())){
                    //104から来ていない場合
                    rank1 ="1";
                    rank2 ="2";
                    rank3 ="3";
                }else{
                    rank1 = targetCandidateRank1;
                    rank2 = targetCandidateRank2;
                    rank3 = targetCandidateRank3;
                }
                String[] CandidateRank = {rank1,rank2,rank3};

                try{

                    boolean eng_count = false;
                    boolean math_count = false;
                    boolean japan_count = false;

                    /**英語の偏差値取得(科目配点が200の物)***********************************************************************/
                    String engCourseCd = "";//[1] add

                    pstmt1 = conn.prepareStatement(QueryLoader.getInstance().load("i12_english_score1").toString());
                    pstmt1.setString(1, printData.getIndividualid());			//項番2で出たID
                    pstmt1.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                    pstmt1.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                    pstmt1.setString(4, commonMap.getTargetExamYear());			//画面模試年度
                    rs1 = pstmt1.executeQuery();

                    I11GouChartListBean bean = new I11GouChartListBean();
                    printData.getI11GouChartList().add(bean);

                    bean.setIntKyokaDispFlg(MultiExam);//1教科複数受験

                    while(rs1.next()){
                        //[1] change
                        //英語＋Ｌを優先する
                        //偏差値が-999だったら追加しない
                        if(rs1.getFloat(2) != -999.0){
                            //英語＋リスニングなら優先して、入れる
                            if(rs1.getString("SUBCD").equals(ELSUBCD)) {
                                bean.setStrKyokamei(rs1.getString(1));		//教科名
                                bean.setFloMshHensa(rs1.getFloat(2));		//偏差値
                                engCourseCd = rs1.getString(3);//[1]
                                choiceScholarLevel(bean, bean.getIntKyokaDispFlg(),
                                        rs1.getFloat(2), rs1.getString(4));
                                eng_count = true;		//確認フラグ
                            } else if(!eng_count) {
                                //それ以外は、最初の一つを入れる
                                bean.setStrKyokamei(rs1.getString(1));		//教科名
                                bean.setFloMshHensa(rs1.getFloat(2));		//偏差値
                                engCourseCd = rs1.getString(3);//[1]
                                choiceScholarLevel(bean, bean.getIntKyokaDispFlg(),
                                        rs1.getFloat(2), rs1.getString(4));
                                eng_count = true;		//確認フラグ
                            }
                        }
                        //engCourseCd = rs1.getString(3);//[1]
                    }
                    if(eng_count) {
                        //大学データ
                        //Query query2 = QueryLoader.getInstance().load("i12_univdata1");//[6] delete
                        //[6] add start
                        final String ENG_UNIVDATA_SUBCD = "1000";
                        String engUnivSub = GeneralUtil.toBlank(engCourseCd);
                        if(!engUnivSub.equals("")) {
                            engUnivSub = ENG_UNIVDATA_SUBCD;
                        }
                        Query query2 = QueryLoader.getInstance().load("i12_univdata2");
                        //[6] add end

                        query2.setStringArray(1,(String[])CandidateRank); 			// プロファイルIDリスト
                        pstmt2 = conn.prepareStatement(query2.toString());
                        pstmt2.setString(1, printData.getIndividualid());			//項番2で出たID
                        pstmt2.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                        pstmt2.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                        //pstmt2.setString(4, engCourseCd);						//項番10で出た教科コード//[6] delete
                        pstmt2.setString(4, engUnivSub);//[6] add 英語の科目コード（英語1000）
                        rs2 = pstmt2.executeQuery();

                        while(rs2.next()){
                            I11ShiboGouListBean UnivData = new	I11ShiboGouListBean();
                            UnivData.setStrDaigakuMei(rs2.getString(1));	//大学名
                            UnivData.setStrGakubuMei(rs2.getString(2));		//学部名
                            UnivData.setStrGakkaMei(rs2.getString(3));		//学科名
                            UnivData.setFloHensa(rs2.getFloat(4));			//合格者平均偏差値
                            bean.getI11ShiboGouList().add(UnivData);
                        }
                        if(pstmt2 != null) pstmt2.close();
                        if(rs2 != null) rs2.close();
                    }
                    if(pstmt1 != null) pstmt1.close();
                    if(rs1 != null) rs1.close();

                    /**英語の偏差値取得(↑の条件で一軒もヒットしなかったらこっち)**************************************/
                    if(eng_count == false){

                        pstmt15 = conn.prepareStatement(QueryLoader.getInstance().load("i12_english_score2").toString());
                        pstmt15.setString(1, printData.getIndividualid());			//項番2で出たID
                        pstmt15.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                        pstmt15.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                        pstmt15.setString(4, commonMap.getTargetExamYear());			//画面模試年度
                        rs15 = pstmt15.executeQuery();

                        while(rs15.next()){

                            bean.setStrKyokamei(rs15.getString(1));		//教科名
                            bean.setFloMshHensa(rs15.getFloat(2));		//偏差値
                            choiceScholarLevel(bean, MultiExam,
                                    rs15.getFloat(2), rs15.getString(4));
                            bean.setIntKyokaDispFlg(MultiExam);			////1教科複数受験

                            //Query query2 = QueryLoader.getInstance().load("i12_univdata1");//[6] delete
                            //[6] add start
                            final String ENG_UNIVDATA_SUBCD = "1000";
                            String engUnivSub = GeneralUtil.toBlank(rs15.getString(3));
                            if(!engUnivSub.equals("")) {
                                engUnivSub = ENG_UNIVDATA_SUBCD;
                            }
                            Query query2 = QueryLoader.getInstance().load("i12_univdata2");
                            //[6] add end

                            query2.setStringArray(1,(String[])CandidateRank); 			// プロファイルIDリスト
                            pstmt16 = conn.prepareStatement(query2.toString());
                            pstmt16.setString(1, printData.getIndividualid());			//項番2で出たID
                            pstmt16.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                            pstmt16.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                            //pstmt16.setString(4, rs15.getString(3));						//項番10で出た教科コード//[6] delete
                            pstmt16.setString(4, engUnivSub);//[6] add 英語の科目コード（英語1000）
                            rs16 = pstmt16.executeQuery();

                            while(rs16.next()){
                                I11ShiboGouListBean UnivData = new	I11ShiboGouListBean();
                                UnivData.setStrDaigakuMei(rs16.getString(1));	//大学名
                                UnivData.setStrGakubuMei(rs16.getString(2));		//学部名
                                UnivData.setStrGakkaMei(rs16.getString(3));		//学科名
                                UnivData.setFloHensa(rs16.getFloat(4));			//合格者平均偏差値
                                bean.getI11ShiboGouList().add(UnivData);
                            }
                            if(pstmt16 != null) pstmt16.close();
                            if(rs16 != null) rs16.close();
                            break;									//表示順序の一番目
                        }
                        if(pstmt16 != null) pstmt16.close();
                        if(rs16 != null) rs16.close();
                    }

                    /**数学の偏差値取得(科目配点が200の物)***********************************************************************/

                    pstmt3 = conn.prepareStatement(QueryLoader.getInstance().load("i12_math_score1").toString());
                    pstmt3.setString(1, printData.getIndividualid());			//項番2で出たID
                    pstmt3.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                    pstmt3.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                    pstmt3.setString(4, commonMap.getTargetExamYear());			//画面模試年度
                    rs3 = pstmt3.executeQuery();

                    bean = new I11GouChartListBean();
                    printData.getI11GouChartList().add(bean);

                    while(rs3.next()){
                        //数学の配点２００点結果
                        if(rs3.getFloat(2) != -999) {
                            if(math_count == false) {
                                //数学の結果が入っていない場合
                                I11ShiboGouListBean UnivData = new	I11ShiboGouListBean();
                                bean.setStrKyokamei(rs3.getString(1));		//教科名
                                bean.setFloMshHensa(rs3.getFloat(2));		//偏差値
                                choiceScholarLevel(bean, MultiExam,
                                        rs3.getFloat(2), rs3.getString(4));
                                bean.setIntKyokaDispFlg(MultiExam);			//1教科複数受験

                                String subCd = GeneralUtil.toBlank(rs3.getString(3));
                                String targetSubCd = "";

                                //大学データ
                                Query query4;
                                if(examflg && (subCd.equals("2380") || subCd.equals("2480") || subCd.equals("2580"))){
                                    //マーク模試、かつ科目コードが(2380 || 2480 || 2580)の場合、科目コード２０００固定（教科コード）
                                    query4 = QueryLoader.getInstance().load("i12_univdata2");
                                    targetSubCd = "2000";
                                }else{
                                    query4 = QueryLoader.getInstance().load("i12_univdata1");
                                    targetSubCd = subCd;
                                }

                                query4.setStringArray(1,(String[])CandidateRank); 			// プロファイルIDリスト
                                pstmt4 = conn.prepareStatement(query4.toString());
                                pstmt4.setString(1, printData.getIndividualid());			//項番2で出たID
                                pstmt4.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                                pstmt4.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                                pstmt4.setString(4, targetSubCd);
                                rs4 = pstmt4.executeQuery();

                                while(rs4.next()){
                                    UnivData = new	I11ShiboGouListBean();
                                    UnivData.setStrDaigakuMei(rs4.getString(1));	//大学名
                                    UnivData.setStrGakubuMei(rs4.getString(2));		//学部名
                                    UnivData.setStrGakkaMei(rs4.getString(3));		//学科名
                                    UnivData.setFloHensa(rs4.getFloat(4));			//合格者平均偏差値
                                    bean.getI11ShiboGouList().add(UnivData);
                                }
                                if(pstmt4 != null) pstmt4.close();
                                if(rs4 != null) rs4.close();
                            }
                            math_count = true;
                        }

                    }
                    if(pstmt3 != null) pstmt3.close();
                    if(rs3 != null) rs3.close();


                    /**数学の偏差値取得(↑の条件で一軒もヒットしなかったらこっち)********************************************/
                    if(math_count == false){

                        pstmt5 = conn.prepareStatement(QueryLoader.getInstance().load("i12_math_score2").toString());
                        pstmt5.setString(1, printData.getIndividualid());						//項番2で出たID
                        pstmt5.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                        pstmt5.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                        pstmt5.setString(4, commonMap.getTargetExamYear());			//画面模試年度
                        rs5 = pstmt5.executeQuery();

                        while(rs5.next()){

                            bean.setStrKyokamei(rs5.getString(1));		//教科名
                            bean.setFloMshHensa(rs5.getFloat(2));		//偏差値
                            choiceScholarLevel(bean, MultiExam,
                                    rs5.getFloat(2), rs5.getString(4));
                            bean.setIntKyokaDispFlg(MultiExam);			//1教科複数受験
                            //教科名・偏差値リスト

                            //[1]
                            String subCd = GeneralUtil.toBlank(rs5.getString(3));
                            String targetSubCd = "";
                            //大学データ
                            Query query6;
                            if(examflg && (subCd.equals("2380") || subCd.equals("2480") || subCd.equals("2580"))){
                                //マーク模試、かつ科目コードが(2380 || 2480 || 2580)の場合、科目コード２０００固定（教科コード）
                                query6 = QueryLoader.getInstance().load("i12_univdata2");
                                targetSubCd = "2000";
                            }else{
                                query6 = QueryLoader.getInstance().load("i12_univdata1");
                                targetSubCd = subCd;
                            }
                            query6.setStringArray(1,(String[])CandidateRank); 			// プロファイルIDリスト
                            pstmt6 = conn.prepareStatement(query6.toString());
                            pstmt6.setString(1, printData.getIndividualid());			//項番2で出たID
                            pstmt6.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                            pstmt6.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                            pstmt6.setString(4, targetSubCd);
                            rs6 = pstmt6.executeQuery();

//							//大学データ//[2] delete
//							Query query6 = QueryLoader.getInstance().load("i12_univdata1");
//							query6.setStringArray(1,(String[])CandidateRank); 			// プロファイルIDリスト
//							pstmt6 = conn.prepareStatement(query6.toString());
//							pstmt6.setString(1, printData.getIndividualid());			//項番2で出たID
//							pstmt6.setString(2, commonMap.getTargetExamYear());			//画面模試年度
//							pstmt6.setString(3, commonMap.getTargetExamCode());			//画面模試コード
//							if(examflg && (GeneralUtil.toBlank(rs5.getString(3)).equals("2380") || GeneralUtil.toBlank(rs5.getString(3)).equals("2480") || GeneralUtil.toBlank(rs5.getString(3)).equals("2580"))){
//								//マーク模試の場合、数学科目コード2000固定
//								pstmt6.setString(4, "2000");
//							}else{
//								pstmt6.setString(4, rs5.getString(3));						//項番10で出た教科コード
//							}
//							rs6 = pstmt6.executeQuery();
                            //[1]
                            while(rs6.next()){
                                I11ShiboGouListBean UnivData = new	I11ShiboGouListBean();
                                UnivData.setStrDaigakuMei(rs6.getString(1));	//大学名
                                UnivData.setStrGakubuMei(rs6.getString(2));		//学部名
                                UnivData.setStrGakkaMei(rs6.getString(3));		//学科名
                                UnivData.setFloHensa(rs6.getFloat(4));			//合格者平均偏差値
                                bean.getI11ShiboGouList().add(UnivData);
                            }
                            if(pstmt6 != null) pstmt6.close();
                            if(rs6 != null) rs6.close();
                            break;								//表示順序の一番目
                        }
                        if(pstmt5 != null) pstmt5.close();
                        if(rs5 != null) rs5.close();
                    }

                    /**国語の偏差値取得(科目配点が200の物)*******************************************************************/

                    pstmt13 = conn.prepareStatement(QueryLoader.getInstance().load("i12_japanLang_score1").toString());
                    pstmt13.setString(1, printData.getIndividualid());				//項番2で出たID
                    pstmt13.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                    pstmt13.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                    pstmt13.setString(4, commonMap.getTargetExamYear());			//画面模試年度
                    rs13 = pstmt13.executeQuery();

                    bean = new I11GouChartListBean();
                    printData.getI11GouChartList().add(bean);

                    while(rs13.next()){
                        if(japan_count == false) {
                            if(rs13.getFloat(2) != -999){
                                bean.setStrKyokamei(rs13.getString(1));		//教科名
                                bean.setFloMshHensa(rs13.getFloat(2));		//偏差値
                                choiceScholarLevel(bean, MultiExam,
                                        rs13.getFloat(2), rs13.getString(4));
                                bean.setIntKyokaDispFlg(MultiExam);			//1教科複数受験

                                //大学データ
                                Query query14 = QueryLoader.getInstance().load("i12_univdata1");
                                query14.setStringArray(1,(String[])CandidateRank); 				// プロファイルIDリスト
                                pstmt14 = conn.prepareStatement(query14.toString());
                                pstmt14.setString(1, printData.getIndividualid());				//項番2で出たID
                                pstmt14.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                                pstmt14.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                                pstmt14.setString(4, rs13.getString(3));						//項番10で出た教科コード
                                rs14 = pstmt14.executeQuery();

                                while(rs14.next()){
                                    I11ShiboGouListBean UnivData = new	I11ShiboGouListBean();
                                    UnivData.setStrDaigakuMei(rs14.getString(1));	//大学名
                                    UnivData.setStrGakubuMei(rs14.getString(2));	//学部名
                                    UnivData.setStrGakkaMei(rs14.getString(3));		//学科名
                                    UnivData.setFloHensa(rs14.getFloat(4));			//合格者平均偏差値
                                    bean.getI11ShiboGouList().add(UnivData);
                                }
                                if(pstmt14 != null) pstmt14.close();
                                if(rs14 != null) rs14.close();

                                japan_count = true;
                            }
                        }
                    }
                    if(pstmt13 != null) pstmt13.close();
                    if(rs13 != null) rs13.close();

                    /**国語の偏差値取得(↑の条件で一軒もヒットしなかったらこっち)****************************************/
                    if(japan_count == false){
                        pstmt7 = conn.prepareStatement(QueryLoader.getInstance().load("i12_japanLang_score2").toString());
                        pstmt7.setString(1, printData.getIndividualid());			//項番2で出たID
                        pstmt7.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                        pstmt7.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                        pstmt7.setString(4, commonMap.getTargetExamYear());			//画面模試年度
                        rs7 = pstmt7.executeQuery();

                        while(rs7.next()){

                            bean.setStrKyokamei(rs7.getString(1));		//教科名
                            bean.setFloMshHensa(rs7.getFloat(2));		//偏差値
                            choiceScholarLevel(bean, MultiExam,
                                    rs7.getFloat(2), rs7.getString(4));
                            bean.setIntKyokaDispFlg(MultiExam);			//1教科複数受験

                            //大学データ
                            Query query8 = QueryLoader.getInstance().load("i12_univdata1");
                            query8.setStringArray(1,(String[])CandidateRank); 			// プロファイルIDリスト
                            pstmt8 = conn.prepareStatement(query8.toString());
                            pstmt8.setString(1, printData.getIndividualid());			//項番2で出たID
                            pstmt8.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                            pstmt8.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                            pstmt8.setString(4, rs7.getString(3));						//項番10で出た教科コード
                            rs8 = pstmt8.executeQuery();

                            while(rs8.next()){
                                I11ShiboGouListBean UnivData = new	I11ShiboGouListBean();
                                UnivData.setStrDaigakuMei(rs8.getString(1));	//大学名
                                UnivData.setStrGakubuMei(rs8.getString(2));		//学部名
                                UnivData.setStrGakkaMei(rs8.getString(3));		//学科名
                                UnivData.setFloHensa(rs8.getFloat(4));			//合格者平均偏差値
                                bean.getI11ShiboGouList().add(UnivData);
                            }
                            if(pstmt8 != null) pstmt8.close();
                            if(rs8 != null) rs8.close();
                            break;									//表示順序の一番目
                        }
                        if(pstmt7 != null) pstmt7.close();
                        if(rs7 != null) rs7.close();
                    }

                    /**理科の偏差値取得*******************************************************************************/
                    //1教科複数受験　平均1or良い方2//プロファイルから1教科複数受験取得

                    if(MultiExam == 3){
                        if(KNUtil.isAns1st(commonMap.getTargetExamCode())){
                            //第1解答科目対応模試
                            pstmt9 = conn.prepareStatement(QueryLoader.getInstance().load("i12_science_ans1st_score").toString());
                            pstmt9.setString(1, printData.getIndividualid());			//項番2で出たID
                            pstmt9.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                            pstmt9.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                            pstmt9.setString(4, printData.getIndividualid());			//項番2で出たID
                            pstmt9.setString(5, commonMap.getTargetExamYear());			//画面模試年度
                            pstmt9.setString(6, commonMap.getTargetExamCode());			//画面模試コード
                            pstmt9.setString(7, commonMap.getTargetExamYear());			//画面模試年度
                        }else{
                            //平均
                            pstmt9 = conn.prepareStatement(QueryLoader.getInstance().load("i12_science_avg_score").toString());
                            pstmt9.setString(1, printData.getIndividualid());			//項番2で出たID
                            pstmt9.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                            pstmt9.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                            pstmt9.setString(4, commonMap.getTargetExamYear());			//画面模試年度
                        }
                    }else if(MultiExam == 1){
                        //平均ならこのSQL
                        pstmt9 = conn.prepareStatement(QueryLoader.getInstance().load("i12_science_avg_score").toString());
                        pstmt9.setString(1, printData.getIndividualid());						//項番2で出たID
                        pstmt9.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                        pstmt9.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                        pstmt9.setString(4, commonMap.getTargetExamYear());			//画面模試年度
                    }else{
                        //良い方ならこのSQL
                        pstmt9 = conn.prepareStatement(QueryLoader.getInstance().load("i12_science_max_score").toString());
                        pstmt9.setString(1, printData.getIndividualid());	//項番2で出たID
                        pstmt9.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                        pstmt9.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                        pstmt9.setString(4, commonMap.getTargetExamYear());			//画面模試年度
                    }

                    rs9 = pstmt9.executeQuery();
                    //理科で一番点数の良い科目コードをだす。
                    int count = 0;
                    String subname = "";
                    float subscor = -999;
                    String subcd = "";
                    String sublevel = "";
                    int ans1st=0;

                    bean = new I11GouChartListBean();
                    printData.getI11GouChartList().add(bean);
                    while(rs9.next()){

                        if(count == 0){
                            subname = rs9.getString(1);
                            subscor = rs9.getFloat(2);
                            subcd = rs9.getString(3);
                            sublevel = rs9.getString(4);
                            ans1st = rs9.getInt(5);
                        }else{
                            if(subscor < rs9.getFloat(2)){
                                subname = rs9.getString(1);
                                subscor = rs9.getFloat(2);
                                subcd = rs9.getString(3);
                                sublevel = rs9.getString(4);
                                ans1st = rs9.getInt(5);
                            }
                        }
                        count ++;
                    }
                    bean.setStrKyokamei(subname);//教科名
                    bean.setFloMshHensa(subscor);//偏差値
                    //学力レベル
                    if (MultiExam == 3){
                        if (ans1st == 1){
                            choiceScholarLevel(bean, 2, subscor, sublevel);
                        }else{
                            choiceScholarLevel(bean, 1, subscor, sublevel);
                        }
                    }else {
                        choiceScholarLevel(bean, MultiExam, subscor, sublevel);
                    }
                    bean.setIntKyokaDispFlg(MultiExam);	//1教科複数受験
                    if(pstmt9 != null) pstmt9.close();
                    if(rs9 != null) rs9.close();

                    if(MultiExam == 1 || (MultiExam == 3 && ans1st != 1)){
                        //大学データ
                        Query query10 = QueryLoader.getInstance().load("i12_univdata2");
                        query10.setStringArray(1,(String[])CandidateRank); 			    // プロファイルIDリスト
                        pstmt10 = conn.prepareStatement(query10.toString());
                        pstmt10.setString(1, printData.getIndividualid());				//項番2で出たID
                        pstmt10.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                        pstmt10.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                        pstmt10.setString(4, subcd);
                        rs10 = pstmt10.executeQuery();
                    }else{
                        //大学データ
                        Query query10 = QueryLoader.getInstance().load("i12_univdata1");
                        query10.setStringArray(1,(String[])CandidateRank); 				// プロファイルIDリスト
                        pstmt10 = conn.prepareStatement(query10.toString());
                        pstmt10.setString(1, printData.getIndividualid());				//項番2で出たID
                        pstmt10.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                        pstmt10.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                        pstmt10.setString(4, subcd);
                        rs10 = pstmt10.executeQuery();
                    }
                    while(rs10.next()){
                        I11ShiboGouListBean UnivData = new	I11ShiboGouListBean();
                        UnivData.setStrDaigakuMei(rs10.getString(1));	//大学名
                        UnivData.setStrGakubuMei(rs10.getString(2));	//学部名
                        UnivData.setStrGakkaMei(rs10.getString(3));		//学科名
                        UnivData.setFloHensa(rs10.getFloat(4));			//合格者平均偏差値
                        bean.getI11ShiboGouList().add(UnivData);
                    }
                    if(pstmt10 != null) pstmt10.close();
                    if(rs10 != null) rs10.close();

                    /**地歴の偏差値取得*******************************************************************************/

                    if(MultiExam == 3){
                        if(KNUtil.isAns1st(commonMap.getTargetExamCode())){
                            //第1解答科目対応模試
                            pstmt11 = conn.prepareStatement(QueryLoader.getInstance().load("i12_tireki_ans1st_score").toString());
                            pstmt11.setString(1, printData.getIndividualid());				//項番2で出たID
                            pstmt11.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                            pstmt11.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                            pstmt11.setString(4, printData.getIndividualid());				//項番2で出たID
                            pstmt11.setString(5, commonMap.getTargetExamYear());			//画面模試年度
                            pstmt11.setString(6, commonMap.getTargetExamCode());			//画面模試コード
                            pstmt11.setString(7, commonMap.getTargetExamYear());			//画面模試年度
                        }else{
                            //平均
                            pstmt11 = conn.prepareStatement(QueryLoader.getInstance().load("i12_tireki_avg_score").toString());
                            pstmt11.setString(1, printData.getIndividualid());						//項番2で出たID
                            pstmt11.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                            pstmt11.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                            pstmt11.setString(4, commonMap.getTargetExamYear());			//画面模試年度
                        }
                    }else if(MultiExam == 1){
                        //平均ならこのSQL
                        pstmt11 = conn.prepareStatement(QueryLoader.getInstance().load("i12_tireki_avg_score").toString());
                        pstmt11.setString(1, printData.getIndividualid());						//項番2で出たID
                        pstmt11.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                        pstmt11.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                        pstmt11.setString(4, commonMap.getTargetExamYear());			//画面模試年度
                    }else{
                        //良い方ならこのSQL
                        pstmt11 = conn.prepareStatement(QueryLoader.getInstance().load("i12_tireki_max_score").toString());
                        pstmt11.setString(1, printData.getIndividualid());	//項番2で出たID
                        pstmt11.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                        pstmt11.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                        pstmt11.setString(4, commonMap.getTargetExamYear());			//画面模試年度
                    }

                    rs11 = pstmt11.executeQuery();
                    //地歴で一番点数の良い科目コードをだす。
                    count = 0;
                    subname = "";
                    subscor = -999;
                    subcd = "";
                    sublevel = "";
                    ans1st=0;

                    bean = new I11GouChartListBean();
                    printData.getI11GouChartList().add(bean);
                    while(rs11.next()){

                        if(count == 0){
                            subname = rs11.getString(1);
                            subscor = rs11.getFloat(2);
                            subcd = rs11.getString(3);
                            sublevel = rs11.getString(4);
                            ans1st = rs11.getInt(5);
                        }else{
                            if(subscor < rs11.getFloat(2)){
                                subname = rs11.getString(1);
                                subscor = rs11.getFloat(2);
                                subcd = rs11.getString(3);
                                sublevel = rs11.getString(4);
                                ans1st = rs11.getInt(5);
                            }
                        }
                        count ++;
                    }
                    bean.setStrKyokamei(subname);//教科名
                    bean.setFloMshHensa(subscor);//偏差値
                    //学力レベル
                    if (MultiExam == 3){
                        if (ans1st == 1){
                            choiceScholarLevel(bean, 2, subscor, sublevel);
                        }else{
                            choiceScholarLevel(bean, 1, subscor, sublevel);
                        }
                    }else {
                        choiceScholarLevel(bean, MultiExam, subscor, sublevel);
                    }
                    bean.setIntKyokaDispFlg(MultiExam);	//1教科複数受験
                    if(pstmt11 != null) pstmt11.close();
                    if(rs11 != null) rs11.close();

                    if(MultiExam == 1|| (MultiExam == 3 && ans1st != 1)){
                        //大学データ
                        Query query12 = QueryLoader.getInstance().load("i12_univdata2");
                        query12.setStringArray(1,(String[])CandidateRank); 			    // プロファイルIDリスト
                        pstmt12 = conn.prepareStatement(query12.toString());
                        pstmt12.setString(1, printData.getIndividualid());				//項番2で出たID
                        pstmt12.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                        pstmt12.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                        pstmt12.setString(4, subcd);									//項番12で出た教科コード
                        rs12 = pstmt12.executeQuery();
                    }else{
                        Query query12 = QueryLoader.getInstance().load("i12_univdata1");
                        query12.setStringArray(1,(String[])CandidateRank); 			    // プロファイルIDリスト
                        pstmt12 = conn.prepareStatement(query12.toString());
                        pstmt12.setString(1, printData.getIndividualid());				//項番2で出たID
                        pstmt12.setString(2, commonMap.getTargetExamYear());			//画面模試年度
                        pstmt12.setString(3, commonMap.getTargetExamCode());			//画面模試コード
                        pstmt12.setString(4, subcd);									//項番12で出た教科コード
                        rs12 = pstmt12.executeQuery();
                    }
                    while(rs12.next()){
                        I11ShiboGouListBean UnivData = new	I11ShiboGouListBean();
                        UnivData.setStrDaigakuMei(rs12.getString(1));				//大学名
                        UnivData.setStrGakubuMei(rs12.getString(2));				//学部名
                        UnivData.setStrGakkaMei(rs12.getString(3));					//学科名
                        UnivData.setFloHensa(rs12.getFloat(4));						//合格者平均偏差値
                        bean.getI11ShiboGouList().add(UnivData);
                    }

                    if(pstmt12 != null) pstmt12.close();
                    if(rs12 != null) rs12.close();
            }catch(Exception e){
                e.printStackTrace();
            }finally{
                if(pstmt1 != null) pstmt1.close();
                if(rs1 != null) rs1.close();
                if(pstmt2 != null) pstmt2.close();
                if(rs2 != null) rs2.close();
                if(pstmt3 != null) pstmt3.close();
                if(rs3 != null) rs3.close();
                if(pstmt4 != null) pstmt4.close();
                if(rs4 != null) rs4.close();
                if(pstmt5 != null) pstmt5.close();
                if(rs5 != null) rs5.close();
                if(pstmt6 != null) pstmt6.close();
                if(rs6 != null) rs6.close();
                if(pstmt7 != null) pstmt7.close();
                if(rs7 != null) rs7.close();
                if(pstmt8 != null) pstmt8.close();
                if(rs8 != null) rs8.close();
                if(pstmt9 != null) pstmt9.close();
                if(rs9 != null) rs9.close();
                if(pstmt10 != null) pstmt10.close();
                if(rs10 != null) rs10.close();
                if(pstmt11 != null) pstmt11.close();
                if(rs11 != null) rs11.close();
                if(pstmt12 != null) pstmt12.close();
                if(rs12 != null) rs12.close();
                if(pstmt13 != null) pstmt13.close();
                if(rs13 != null) rs13.close();
                if(pstmt14 != null) pstmt14.close();
                if(rs14 != null) rs14.close();
                if(pstmt15 != null) pstmt15.close();
                if(rs15 != null) rs15.close();
                if(pstmt16 != null) pstmt16.close();
                if(rs16 != null) rs16.close();
            }

        }//個人情報
        conn.commit();

    }//flag条件


}

    /* (非 Javadoc)
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
     */
    protected String getCategory() {
        return IProfileCategory.I_SCORE_BC_AVG;
    }

    /**
     * 画面の選択状態（平均値／良い方）で、出力する学力レベルを選択する。
     * @param hensa
     * @param flg
     * @param avg
     * @param lvl
     */
    private void choiceScholarLevel(I11GouChartListBean bean,
            final int flg, final float avg, final String lvl) {

        // 	平均 or 第1解等科目
        //  第1解等科目で理科・地歴公民以外の場合は平均とする
        if (flg == 1 || flg == 3) {
            //平均値の場合、偏差値から学力レベルを出力
            bean.setStrScholarLevel(IExamData.makeScholarLevel(""+avg));
        }
        else {
            //良い方の場合、SQLから取得した学力レベルを出力
            bean.setStrScholarLevel(GeneralUtil.toBlank(lvl));
        }

    }


}

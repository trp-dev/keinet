/**
 * 校内成績分析−校内成績　記述系模試小設問別成績
 *      Excelファイル編集
 * 作成日: 2004/07/13
 * @author      M.Ooseto
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S13Item;
import jp.co.fj.keinavi.excel.data.school.S13RecordListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

public class S13_07 {

    // 帳票作成結果
    // 0:正常終了
    // 1:ファイル読込みエラー
    // 2:ファイル書込みエラー
    // 3:データセットエラー
    private int noerror = 0;
    private int errfread = 1;
    private int errfwrite = 2;
    private int errfdata = 3;

    // 共通関数用クラス インスタンス
    private CM cm = new CM();

    // 帳票テンプレートファイル
    private String masterfile = "S13_07";

    // 出力固定値
    private String BUNDLENAME = "学校名　：";
    private String EXAMNAME = "対象模試：";
    private String ERRORMESSAGE = "データセットエラー";
    private int INTSECUFLG = 0;

    // 固定値
    private int COLPLUS = 13;

    // 出力項目位置
    public enum CURSOL {
        //2019/10/07 QQ)Oosaki 出力位置変更 UPD START
        //// 教科名
        //SUBNAME(3,0),
        //// 明細
        //DATA_START(6,0),
        //DATA_END(60,11);
        // 教科名
        SUBNAME(5,0),
        // 明細
        DATA_START(8,0),
        DATA_END(62,11);
        //2019/10/07 QQ)Oosaki 出力位置変更 UPD END

        private final int col;
        private final int row;

        private CURSOL(int row, int col) {
            this.col = col;
            this.row = row;
        }
        public int getCol() {
            return this.col;
        }
        public int getRow() {
            return this.row;
        }

    }

    /**
     * Excel編集メイン
     * @param s13Item データクラス
     * @param outfilelist 出力Excelファイル名（フルパス）
     * @param intSaveFlg 1:保存 2:印刷 3:保存／印刷
     * @param UserID ユーザーID
     * @return 帳票作成結果
     */
    public int s13_07EditExcel(S13Item s13Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
        KNLog log = KNLog.getInstance(null,null,null);
        HSSFWorkbook workbook = null;
        HSSFSheet workSheet = null;
        HSSFRow workRow = null;
        HSSFCell workCell = null;

        // マスタExcel読み込み
        workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);

        // マスタExcel読込みエラー
        if( workbook==null ){
            return errfread;
        }

        workSheet = workbook.cloneSheet(0);

        // データ検索結果取得
        List <S13RecordListBean> result = s13Item.getS13RecordList();

        try {
            // 行
            int row = CURSOL.DATA_START.getRow();;
            // 右表の出力時の列
            int plus = 0;
            // シート数
            int sheetCount = 1;
            // 科目コード
            String subCd = "";
            // 設問番号
            String questionNo = "";
            // 小問番号
            String kwebShoquestionName = "";
            // 改ページフラグ
            boolean changePage = false;
            // 設問フラグ
            boolean changeQuestion = false;
            int index = 0;

            // ヘッダ値設定
            BUNDLENAME = BUNDLENAME + result.get(0).getBundleName();
            EXAMNAME = EXAMNAME + result.get(0).getExamName();
            INTSECUFLG = s13Item.getIntSecuFlg();

            // ヘッダ出力
            setHead(workSheet, workRow, workbook, workCell);

            while(index < result.size()) {
                S13RecordListBean data = result.get(index);

                // 小問番号チェック(1小問毎に複数レコード(知識技能、思考力・判断力、表現力)が取得されるため)
                if(!kwebShoquestionName.equals("") && kwebShoquestionName.equals(data.getKwebShoquestionName())) {
                    row--;
                }

                // 科目チェック、最大行数チェック(改表)
                if(!subCd.equals(data.getSubCd()) || row > CURSOL.DATA_END.getRow()) {

                    // 表チェック(改ページ)
                    if(changePage) {
                        if(row <= CURSOL.DATA_END.getRow()) {
                            // 前ページの最終行の罫線
                            setLine(workSheet, workbook, row, plus);
                        }
                        // 改ページ
                        changePage = false;
                        plus = 0;
                        workSheet = workbook.cloneSheet(0);
                        // ヘッダ出力
                        setHead(workSheet, workRow, workbook, workCell);
                        sheetCount++;
                    }
                    else {
                        if(!subCd.equals("")) {
                            if(row <= CURSOL.DATA_END.getRow()) {
                                // 前表(左側の表)の最終行の罫線
                                setLine(workSheet, workbook, row, plus);
                            }
                            // 改表(右側の表へ出力)
                            changePage = true;
                            plus = COLPLUS;
                        }
                    }

                    row = CURSOL.DATA_START.getRow();
                    subCd = data.getSubCd();
                    questionNo = "";

                    // 教科名
                    workCell = cm.setCell(workSheet, workRow, workCell, CURSOL.SUBNAME.getRow(), CURSOL.SUBNAME.getCol() + plus);
                    workCell.setCellValue(data.getSubName());
                }

                // 設問番号チェック(設問番号、設問内容、罫線出力)
                if(!questionNo.equals(data.getQuestionNo())) {
                    questionNo = data.getQuestionNo();
                    changeQuestion = true;
                }
                else {
                    changeQuestion = false;
                }

                // 記述系模試小設問別成績データ出力
                if(!subCd.equals("")) {
                    setVaule(workSheet, workRow, workbook, workCell, data, row, plus, changeQuestion);
                }

                // 小問番号保持
                kwebShoquestionName = data.getKwebShoquestionName();

                row++;
                index++;

            }

            // 最終行の罫線
            if(row <= CURSOL.DATA_END.getRow() && !subCd.equals("")) {
                setLine(workSheet, workbook, row, plus);
            }

            workbook.getSheetAt(1).setSelected(true);

            // Excel書込み
            boolean bolRet = false;
            bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, sheetCount);

            // Excel書込みエラー
            if( bolRet == false ){
                return errfwrite;
            }

        }catch(Exception e){
            log.Err(masterfile, ERRORMESSAGE, e.toString());
            return errfdata;
        }

        return noerror;

    }

    /**
     * Excelにヘッダ出力
     * @param workSheet Excelシート情報
     * @param workRow Excel行情報
     * @param workbook Excel情報
     * @param workCell Excelセル情報
     */
    public void setHead(HSSFSheet workSheet, HSSFRow workRow, HSSFWorkbook workbook, HSSFCell workCell) {

        // ヘッダ右側に帳票作成日時を表示する
        cm.setHeader(workbook, workSheet);

        // セキュリティスタンプセット
        String secuFlg = cm.setSecurity2(workbook, workSheet, INTSECUFLG , 0, 1, 22 ,24);
        workCell = cm.setCell(workSheet, workRow, workCell, 0, 22);
        workCell.setCellValue(secuFlg);

        //2019/10/07 QQ)Oosaki 出力位置変更 UPD START
        //// 学校名
        //workCell = cm.setCell(workSheet, workRow, workCell, 0, 0);
        //workCell.setCellValue(BUNDLENAME);
        //
        //// 対象模試
        //workCell = cm.setCell(workSheet, workRow, workCell, 1, 0);
        //workCell.setCellValue(EXAMNAME);
        // 学校名
        workCell = cm.setCell(workSheet, workRow, workCell, 2, 0);
        workCell.setCellValue(BUNDLENAME);

        // 対象模試
        workCell = cm.setCell(workSheet, workRow, workCell, 3, 0);
        workCell.setCellValue(EXAMNAME);
        //2019/10/07 QQ)Oosaki 出力位置変更 UPD END
    }

    /**
     * Excelにデータ出力
     * @param workSheet Excelシート情報
     * @param workRow Excel行情報
     * @param workbook Excel情報
     * @param workCell Excelセル情報
     * @param data 検索結果
     * @param row 行
     * @param plus 右表の出力時の列
     * @param changeQuestion 設問フラグ
     */
    public void setVaule(HSSFSheet workSheet, HSSFRow workRow, HSSFWorkbook workbook, HSSFCell workCell, S13RecordListBean data, int row, int plus, boolean changeQuestion) {

        int col = CURSOL.DATA_START.getCol() + plus;

        // 設問番号
        if(changeQuestion) {
            // 設問番号の開始行の罫線
            setLine(workSheet, workbook, row, plus);
            workCell = cm.setCell(workSheet, workRow, workCell, row, col);
            workCell.setCellValue(data.getQuestionNo());
        }
        col++;

        // 設問内容
        if(changeQuestion) {
            workCell = cm.setCell(workSheet, workRow, workCell, row, col);
            workCell.setCellValue(data.getQuestionName());
        }
        col++;

        // 小問番号
        workCell = cm.setCell(workSheet, workRow, workCell, row, col);
        workCell.setCellValue(data.getKwebShoquestionName());
        col++;

        // 配点
        workCell = cm.setCell(workSheet, workRow, workCell, row, col);
        workCell.setCellValue(data.getKwebShoquestionPnt());
        col++;

        // 校内平均−平均点
        workCell = cm.setCell(workSheet, workRow, workCell, row, col);
        workCell.setCellValue(data.getSsAvgPnt());
        col++;

        // 校内平均−得点率
        workCell = cm.setCell(workSheet, workRow, workCell, row, col);
        workCell.setCellValue(data.getSsAvgScoreRate());
        col++;

        // 全国平均−平均点
        workCell = cm.setCell(workSheet, workRow, workCell, row, col);
        workCell.setCellValue(data.getSaAvgPnt());
        col++;

        // 全国平均−得点率
        workCell = cm.setCell(workSheet, workRow, workCell, row, col);
        workCell.setCellValue(data.getSaAvgScoreRate());
        // 2020/02/07 QQ)Oosaki 罫線変更 UPD START
        //col+=2;
        col++;

        // 校内・全国得点率差
        workCell = cm.setCell(workSheet, workRow, workCell, row, col);
        workCell.setCellValue(data.getSsAvgScoreRate() - data.getSaAvgScoreRate());
        col++;
        // 2020/02/07 QQ)Oosaki 罫線変更 UPD END

        // 知識技能
        if(StringUtils.isNotBlank(data.getAcAdemicCd1())) {
            workCell = cm.setCell(workSheet, workRow, workCell, row, col);
            workCell.setCellValue(data.getAcAdemicCd1());
        }
        col++;

        // 思考力・判断力
        if(StringUtils.isNotBlank(data.getAcAdemicCd2())) {
            workCell = cm.setCell(workSheet, workRow, workCell, row, col);
            workCell.setCellValue(data.getAcAdemicCd2());
        }
        col++;

        // 表現力
        if(StringUtils.isNotBlank(data.getAcAdemicCd3())) {
            workCell = cm.setCell(workSheet, workRow, workCell, row, col);
            workCell.setCellValue(data.getAcAdemicCd3());
        }
    }

    /**
     * Excelに罫線出力
     * @param workSheet Excelシート情報
     * @param workbook Excel情報
     * @param row 行
     * @param plus 右表の出力時の列
     */
    public void setLine(HSSFSheet workSheet, HSSFWorkbook workbook, int row, int plus) {
        // 2020/02/07 QQ)Oosaki 罫線変更 UPD START
        //cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, row, row, CURSOL.DATA_START.getCol() + plus, CURSOL.DATA_START.getCol() + plus + 1, false, true, false, false);
        cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, row, row, CURSOL.DATA_START.getCol() + plus, CURSOL.DATA_END.getCol() + plus, false, true, false, false);
        // 2020/02/07 QQ)Oosaki 罫線変更 UPD END
    }

}

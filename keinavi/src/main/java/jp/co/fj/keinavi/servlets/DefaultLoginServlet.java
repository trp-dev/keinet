/*
 * 作成日: 2004/06/30
 */
package jp.co.fj.keinavi.servlets;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.login.SectorSession;

/**
 * ログイン画面サーブレット<BR>
 * ログインチェックは常にTRUEを返す
 * 
 * @author kawai
 */
abstract public class DefaultLoginServlet extends DefaultHttpServlet {

	// 証明書開始行
	private static final String CERT_BEGIN = "-----BEGIN CERTIFICATE-----";
	// 証明書終了行
	private static final String CERT_END = "-----END CERTIFICATE-----";
	
	/**
	 * 部門セッションを取得する
	 * @param request
	 * @return
	 */
	protected SectorSession getSectorSession(HttpServletRequest request) {
		return (SectorSession)request.getSession(false).getAttribute(SectorSession.SESSION_KEY);
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#isLogin(javax.servlet.http.HttpServletRequest)
	 */
	protected boolean isLogin(HttpServletRequest request) {
		return true;
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#hasAuthority(javax.servlet.http.HttpServletRequest)
	 */
	protected boolean hasAuthority(HttpServletRequest request) throws ServletException {
		
		return true;
	}

	/**
	 * クライアント証明書を取得する
	 * 
	 * @param request
	 * @return
	 * @throws CertificateException
	 */
	public X509Certificate[] getCertificate(final HttpServletRequest request) {
		// まずはApache経由で取得
		X509Certificate[] certs =
			(X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");

		if (certs != null) return certs;

		// なければHTTPヘッダから復元する（IPCOM対応）
		String cert = request.getHeader("X-Client-Cert");

		if (cert == null) return null; // ありませんでした

		try {
			CertificateFactory factory = CertificateFactory.getInstance("X.509");

			// 改行を入れてやらないとパースエラーになる
			if (cert.startsWith(CERT_BEGIN)) {
				cert = cert.substring(CERT_BEGIN.length());
			}
			if (cert.endsWith(CERT_END)) {
				cert = cert.substring(0, cert.length() - CERT_END.length());
			}
			StringBuffer buff = new StringBuffer();
			buff.append(CERT_BEGIN);
			buff.append("\n");
			buff.append(cert);
			buff.append("\n");
			buff.append(CERT_END);

			return
				new X509Certificate[]{
					(X509Certificate) factory.generateCertificate(
						new ByteArrayInputStream(buff.toString().getBytes())
					)
				};
		} catch (Exception e) {
			throw new RuntimeException("証明書の取得に失敗しました。", e);
		}
	}
}

/*
 * 作成日: 2004/08/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.util.ArrayList;
import java.util.Collection;

import com.fjh.beans.DefaultBean;

import jp.co.fj.keinavi.data.individual.I205Data;

/**
 * @author Administrator
 *
 * データ取得に必要なテーブル：CANDIDATERATING→EXAMINATION(模試区分)→HS3_UNIV_NAME
 * データ登録に必要なテーブル：CANDIDATERATING→EXAMINATION→SCHEDULEDETAIL→EXAMPLANUNIV
 * 
 */
public class I205Bean extends DefaultBean{
	
	/** 出力パラメター */
	//表示用受験予定大学用コレクション
	Collection i205Datas;

	/** 入力パラメター */
	//共通入力パラメター
	private String individualId;
	private String univValue;
	private String dispSubject;
	private String examPattern;
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.individual.ICommonBean#execute()
	 */
	public void execute() throws DatasException {
		//一覧用出力データ
		setI205Datas(execDatas(getIndividualId(), getUnivValue(), getDispSubject(), getExamPattern()));
	}
	/**
	 * 判定結果詳細情報の取得
	 * 
	 * @param individualId
	 * @param univValue
	 * @param dispSubject
	 * @param examPatter
	 * @return
	 * @throws DatasException
	 */
	private Collection execDatas(String individualId, String univValue, String dispSubject, String examPatter) throws DatasException{
		// TODO SELECTのSQLを完成させて一覧情報を以下のようにDataBeanに詰めてください
		String[][] temp = {
		//{"univCd","univName_Abbr","facultyCd","facultyName_Abbr","deptCd","deptName_Abbr","entExamTypeCd","entExamOdeName","entExamInpleDate,"entExamDiv1","entExamDiv2","entExamDiv2Name","entExamDiv3","provEntExamSite", "true"}
		 {"9999","静岡","99","人文","99","言語文化","1","一般","20050612","1","1","本学","1","東京","null"}
		};
		try{
			Collection collection = new ArrayList();
			for(int i=0; i<temp.length; i++){
				I205Data data = new I205Data();

				collection.add(data);
			}
			return collection;
		}catch(Exception e){
			throw new DatasException();
		}
	}
	private class DatasException extends Exception{
		static final String MESSAGE = "Datasでエラー";
		public DatasException(){
			super(MESSAGE);
		}
		public DatasException(Exception e){
			super(e);
		}
	}

	/**
	 * @return
	 */
	public Collection getI205Datas() {
		return i205Datas;
	}

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @return
	 */
	public String getUnivValue() {
		return univValue;
	}

	/**
	 * @param collection
	 */
	public void setI205Datas(Collection collection) {
		i205Datas = collection;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

	/**
	 * @param string
	 */
	public void setUnivValue(String string) {
		univValue = string;
	}

	/**
	 * @return
	 */
	public String getDispSubject() {
		return dispSubject;
	}

	/**
	 * @return
	 */
	public String getExamPattern() {
		return examPattern;
	}

	/**
	 * @param string
	 */
	public void setDispSubject(String string) {
		dispSubject = string;
	}

	/**
	 * @param string
	 */
	public void setExamPattern(String string) {
		examPattern = string;
	}

}

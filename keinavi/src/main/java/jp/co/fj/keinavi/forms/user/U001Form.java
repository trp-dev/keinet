package jp.co.fj.keinavi.forms.user;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 *
 * 利用者情報管理一覧画面アクションフォーム
 * 
 * 2005.10.12	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U001Form extends BaseForm {
	
	// 動作モード
	private String actionMode;
	// パスワード初期化・削除対象となる利用者ID
	private String targetLoginId;
	// 印刷対象の利用者ID
	private String[] loginId;
	
	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	/**
	 * @return actionMode を戻します。
	 */
	public String getActionMode() {
		return actionMode;
	}

	/**
	 * @param actionMode 設定する actionMode。
	 */
	public void setActionMode(String actionMode) {
		this.actionMode = actionMode;
	}

	/**
	 * @return loginId を戻します。
	 */
	public String[] getLoginId() {
		return loginId;
	}

	/**
	 * @param loginId 設定する loginId。
	 */
	public void setLoginId(String[] loginId) {
		this.loginId = loginId;
	}

	/**
	 * @return targetLoginId を戻します。
	 */
	public String getTargetLoginId() {
		return targetLoginId;
	}

	/**
	 * @param targetLoginId 設定する targetLoginId。
	 */
	public void setTargetLoginId(String targetLoginId) {
		this.targetLoginId = targetLoginId;
	}

}

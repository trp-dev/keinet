package jp.co.fj.keinavi.servlets.maintenance;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.admin.LastDateBean;
import jp.co.fj.keinavi.beans.maintenance.KNClassListBean;
import jp.co.fj.keinavi.beans.maintenance.StudentDeleteBean;
import jp.co.fj.keinavi.beans.maintenance.StudentListBean;
import jp.co.fj.keinavi.beans.maintenance.StudentReDisplayBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.maintenance.M101Form;
import jp.co.fj.keinavi.forms.maintenance.M108Form;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * 非表示生徒一覧画面サーブレット
 * 
 * 2006.11.01	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M108Servlet extends M101Servlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {
		
		// アクションフォーム
		final M108Form form = (M108Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.maintenance.M108Form");
		
		// JSPへの遷移
		if ("m108".equals(getForward(request))) {

			final KNClassListBean bean = createClassListBean(request, true);
			
			// 生徒一覧画面からの遷移ならフォーム初期化
			if ("m101".equals(getBackward(request))) {
				form.init(bean.getFirstClassData());
			}

			// クラスリストをセット
			request.setAttribute("ClassListBean", bean);
			// 生徒リストをセット
			request.setAttribute("StudentSearchBean",
					createStudentListBean(request, form));
			// 現年度をセット
			request.setAttribute("CurrentYear", KNUtil.getCurrentYear());
			// アクションフォームをセット
			request.setAttribute("form", form);
			
			forward2Jsp(request, response);
			
		// 転送
		} else {
			
			// 生徒再表示
			if ("1".equals(form.getActionMode())) {
				redispay(request, form);
				initStudentSession(request);
			// 完全に削除
			} else if ("2".equals(form.getActionMode())) {
				delete(request, form);
				initStudentSession(request);
				actionLog(request, "803");
			}
			
			dispatch(request, response);
		}
	}
	
	// 生徒情報リストを作る
	private StudentListBean createStudentListBean(
			final HttpServletRequest request, final M101Form form)
			throws ServletException {

		final StudentListBean bean = new StudentListBean(
				getSchoolCd(request));
		bean.setYear(form.getTargetYear());
		bean.setGrade(form.getTargetGrade());
		bean.setClassName(form.getTargetClass());
		bean.setNotDisplayMode();
		execute(request, bean);
		
		// 表示リスト初期化
		bean.initDispStudentList(
				getStudentListComparator(form.getSortKey()));
		
		return bean;
	}
	
	// 再表示処理
	private void redispay(final HttpServletRequest request,
			final M108Form form) throws ServletException {
		
		// ログイン情報
		final LoginSession login = getLoginSession(request);
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);
			
			// データ更新日
			LastDateBean.setLastDate1(con, login.getUserID());
			
			final StudentReDisplayBean bean = new StudentReDisplayBean(
					login.getUserID());
			bean.setConnection(null, con);
			bean.setIndividualId(form.getIndividualId());
			bean.execute();
			
			con.commit();
			
		} catch (final Exception e) {
			rollback(con);
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
	}

	// 削除処理
	private void delete(final HttpServletRequest request,
			final M108Form form) throws ServletException {
		
		// ログイン情報
		final LoginSession login = getLoginSession(request);
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);
			
			// データ更新日
			LastDateBean.setLastDate1(con, login.getUserID());
			
			final StudentDeleteBean bean = new StudentDeleteBean(
					login.getUserID());
			bean.setConnection(null, con);
			bean.setIndividualId(form.getIndividualId());
			bean.execute();
			
			con.commit();
			
		} catch (final Exception e) {
			rollback(con);
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
	}

}

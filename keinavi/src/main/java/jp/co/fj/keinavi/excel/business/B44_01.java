package jp.co.fj.keinavi.excel.business;

//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.io.FileInputStream;
//import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.business.B44GakkoListBean;
import jp.co.fj.keinavi.excel.data.business.B44HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.business.B44Item;
import jp.co.fj.keinavi.excel.data.business.B44ListBean;

import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
//import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 * 作成日: 2004/07/13
 * @author A.Hasegawa
 *
 * 校内成績分析−過年度比較−志望大学評価別人数　大学（日程なし）出力処理
 * 
 * <2010年度改修>
 * 2010.01.06   Tomohisa YAMADA - Totec
 *              センター・リサーチ５段階評価対応
 *               
 */
public class B44_01 {
	
	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private String		strArea			= "A1:AG58";	//印刷範囲	
	private String		mastersheet 	= "tmp";		//テンプレートシート名
	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow		= 54;			//MAX行数
	private int		intMaxClassSr   = 6;			//MAXクラス数

	private int intDataStartRow = 6;					//データセット開始行
	private int intDataStartCol = 3;					//データセット開始列
	
	final private String masterfile0 = "B44_01" ;
	final private String masterfile1 = "B44_01" ;
	private String masterfile = "" ;
	
	final private String strCenterResearch = "38";

	/*
	 * 	Excel編集メイン
	 * 		B44Item B44Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int b44_01EditExcel(B44Item b44Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		
		boolean bolBookCngFlg = true;			//改ファイルフラグ	
		boolean bolSheetCngFlg = true;			//改シートフラグ
			
		FileInputStream	fin = null;
		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFCell	workCell		= null;
		HSSFRow		workRow			= null;
		
		//テンプレートの決定
		if (b44Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intRow = 0;
		int intCol = 0;

		String strGenekiKbn = "";
		String strDaigakuCode = "";
		String strHyoukaKbn = "";
		String strNendo = "";
		
		//2004.09.27Add
		String strKokushiKbn = "";
		String strStKokushiKbn = "";
		int intChangeSheetRow = 0;
		//2004.09.27AddEnd

		int intLoopCnt = 0;
		int intNinzuListCnt = 0;
		int intSheetCngCount = 0;
		int intBookCngCount = -1;
		
		boolean bolDispHyoukaFlg = false;
		boolean bolDispDaigakuFlg = false;
		String strHyouka = "";
		
		String strDispTarget = "";
		
		ArrayList WorkBookList = new ArrayList();
		ArrayList WorkSheetList = new ArrayList();
		
		int intSheetCnt=0;
		
		int intBookIndex = -1;
		int intSheetIndex = -1;
		
		int intSheetMei = -1;
		
		int intSetRow = intDataStartRow;
		
		boolean bolClassDispFlg=true;
		
		KNLog log = KNLog.getInstance(null,null,null);
		ArrayList HyoukaTitleList = new ArrayList();
		
		log.Ep("B44_01","B44_01帳票作成開始","");

		// 基本ファイルを読込む
		B44ListBean b44ListBean = new B44ListBean();
		
		try{
			

			HyoukaTitleList.add(0,"A");
			HyoukaTitleList.add(1,"B");
			HyoukaTitleList.add(2,"C");
			
			ArrayList b44List = b44Item.getB44List();
			ListIterator itr = b44List.listIterator();
			
//			if( itr.hasNext() == false ){
//				return errfdata;
//			}
			
			while( itr.hasNext() ){
				
				b44ListBean = (B44ListBean)itr.next();
				
				if( !strGenekiKbn.equals(b44ListBean.getStrGenKouKbn()) ){
					strGenekiKbn = b44ListBean.getStrGenKouKbn();
					if( strGenekiKbn.equals("0") ){
						strDispTarget = "全体";
					}
					else if( strGenekiKbn.equals("1") ){
						strDispTarget = "現役生";
					}
					else if( strGenekiKbn.equals("2") ){
						strDispTarget = "高卒生";
					}

					if( bolSheetCngFlg == false ){
						bolSheetCngFlg = true;
					}
				}
				
				bolDispDaigakuFlg = false;
				if( !strDaigakuCode.equals(b44ListBean.getStrDaigakuCd())){
					bolDispDaigakuFlg = true;
					strHyoukaKbn = "";
					//大学名表示
					strDaigakuCode = b44ListBean.getStrDaigakuCd();
//					workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 0 );
//					workCell.setCellValue(b44ListBean.getStrDaigakuMei());
//					//罫線出力(セルの上部に太線を引く）
//					cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
//														intSetRow, intSetRow, 0, 32, false, true, false, false );
				}
				
				bolDispHyoukaFlg = false;
				if( !strHyoukaKbn.equals(b44ListBean.getStrHyoukaKbn()) ){
					bolDispHyoukaFlg = true;
					//評価区分表示
					strHyoukaKbn = b44ListBean.getStrHyoukaKbn();
					strHyouka = "";
					switch( Integer.parseInt(strHyoukaKbn) ){
						case 1:
							strHyouka = "センター";
							break;
						case 2:
							strHyouka = "2次・私大";
							break;
						case 3:
							strHyouka = "総合";
							break;
					}
//					workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
//					workCell.setCellValue( strHyoka );
				
//					//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
//					cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
//												intSetRow, intSetRow, 1, 1, false, true, false, false);
				
				}
				
				//2004.9.27 Add
				int intDaigakuCd = (int)Integer.parseInt(b44ListBean.getStrDaigakuCd());
				if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
					strKokushiKbn = "国公立大";
				}
				if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
					strKokushiKbn = "私立大";
				}
				if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
					strKokushiKbn = "短大";
				}
				if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
					strKokushiKbn = "その他";
				}
				
				if( (!cm.toString(strKokushiKbn).equals(cm.toString(strStKokushiKbn))) ){
					strStKokushiKbn = strKokushiKbn;
					bolSheetCngFlg = true;
				}
				//2004.9.27 Add End

				//クラス別評価人数リスト読み込み クラス名をExcelへセット
				ArrayList b44GakkoList = b44ListBean.getB44GakkoList();
				ListIterator itr2 = b44GakkoList.listIterator();
				
				B44GakkoListBean b44GakkoListBean = new B44GakkoListBean();
				B44GakkoListBean b44GakkoListBeanHomeData = new B44GakkoListBean();
				
				B44GakkoListBean HomeDataBean = new B44GakkoListBean();

				//クラス数取得
				int intClassSr = b44GakkoList.size();
				
				int intSheetSr = 0;
				if( intClassSr % intMaxClassSr > 0 ){
					intSheetSr = (int)Math.ceil(intClassSr / intMaxClassSr) + 1;
				}else{
					intSheetSr = (int)Math.ceil(intClassSr / intMaxClassSr);
				}
				
				int intLoopCnt2 = 0;
				int intNendoCount = 0;
				intChangeSheetRow = 0;
				
				boolean bolHomeDataDispFlg=false; 

				while( itr2.hasNext() ){
					
					String strClassmei = "";
					b44GakkoListBean = (B44GakkoListBean)itr2.next();

					ArrayList b44HyoukaNinzuList = b44GakkoListBean.getB44HyoukaNinzuList();
					B44HyoukaNinzuListBean b44HyoukaNinzuListBean = new B44HyoukaNinzuListBean();
					ListIterator itr3 = b44HyoukaNinzuList.listIterator();
					
					if( intLoopCnt2 == 0 ){
						bolHomeDataDispFlg = true;
						HomeDataBean = b44GakkoListBean;
//						b44GakkoListBean = (B44GakkoListBean)itr2.next();
						intNendoCount = b44HyoukaNinzuList.size();
						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
						intLoopCnt2++;
					}
					
					if( intLoopCnt2 % intMaxClassSr == 0 ){
						intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 5 );
					}else{
						intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 5 );
					}
					
//					intCol = intDataStartCol + ( ( intMaxClassSr - ( intLoopCnt2 % intMaxClassSr ) ) * 5 );
					
					
					int intHyoukaNinzuListIndex = 0;
					while( itr3.hasNext() ){
						
						b44HyoukaNinzuListBean = (B44HyoukaNinzuListBean)itr3.next();
						
						if( intSetRow  >= intChangeSheetRow ||((bolSheetCngFlg == true)&&(intMaxSheetIndex != 0))){
							bolSheetCngFlg = true;
							for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//								int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
								int intBIndex = (intSindex + 1) / intMaxSheetSr;
								if((intSindex + 1)%intMaxSheetSr!=0){
									intBIndex++;
								}
								workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
								workSheet = (HSSFSheet)WorkSheetList.get(intSindex);
								
								//罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intSetRow, intSetRow, 0, 32, false, true, false, false );
			
								if( workSheet != null ){
									int intRemoveStartRow = intChangeSheetRow;
									if( intMaxRow + intDataStartRow > intRemoveStartRow ){
										for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
											//行削除処理
											workSheet.removeRow(workSheet.getRow(index));
										}
										//罫線出力（セルの上部に太線を出力）
										cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
														intRemoveStartRow, intRemoveStartRow, 0, 32, false, true, false, false);
									}
								}
							}
						}
						
						if( bolSheetCngFlg == true ){
							
							bolDispHyoukaFlg = true;
							bolDispDaigakuFlg = true;
							
							intSetRow = intDataStartRow;
							bolClassDispFlg = true;
							bolHomeDataDispFlg = true;
							
							for( int intIndex = 0; intIndex < intSheetSr; intIndex ++ ){
								
								if( ( intMaxSheetIndex >= intMaxSheetSr ) || ( bolBookCngFlg == true ) ){
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook.equals(null) ){
										return errfread;
									}
									WorkBookList.add(workbook);
									intBookIndex++;
					
									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
									workbook = (HSSFWorkbook)WorkBookList.get(intBookIndex);
								}
								
								//シート作成
								// シートテンプレートのコピー
								intSheetMei = ( intBookIndex * intMaxSheetSr) + intMaxSheetIndex + 1;
								workSheet = workbook.cloneSheet(0);
								WorkSheetList.add(workSheet);
								intSheetIndex++;
								workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex);

								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);
				
								// セキュリティスタンプセット
								String secuFlg = cm.setSecurity( workbook, workSheet, b44Item.getIntSecuFlg(), 30, 32 );
								workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
								workCell.setCellValue(secuFlg);
				
//								// 学校名セット
//								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
//								workCell.setCellValue( "学校名　：" + b44Item.getStrGakkomei() );
		
								// 模試月取得
								String moshi =cm.setTaisyouMoshi( b44Item.getStrMshDate() );
								
								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
								workCell.setCellValue( "対象模試：" + b44Item.getStrMshmei() + moshi);
					
								//表示対象
								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
								workCell.setCellValue( "表示対象：" + strDispTarget );
								
								//評価別人数 見出し行
								for( int i = 1; i <= intMaxClassSr; i++ ){
									int intTitleSetCol = (i * 5) + ( intDataStartCol - 3 );
									
									//出願予定・第1志望
									if (strKokushiKbn == "国公立大") { 
										workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol-1 );
										workCell.setCellValue( "出願予定" );
									} else {
										workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol-1 );
										workCell.setCellValue( "第一志望" );
									}
								
									workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol );
									workCell.setCellValue( (String)HyoukaTitleList.get(0) );
									
									workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol+1 );
									workCell.setCellValue( (String)HyoukaTitleList.get(1) );
									
									workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol+2 );
									workCell.setCellValue( (String)HyoukaTitleList.get(2) );
								}
							
								intMaxSheetIndex++;		//シート数カウンタインクリメント
								bolSheetCngFlg = false;
						
								intLoopCnt = 0;
							}	//ForEnd
							
						}	//if( bolSheetCngFlg == true )
						
						intSheetCnt = (int)Math.ceil( (double)intLoopCnt2 / (double)intMaxClassSr );
						workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex -  (intSheetSr - intSheetCnt)  );

						int intSheetNo = intSheetIndex -  (intSheetSr - intSheetCnt);
						int intBIndex = (int)Math.ceil((double)(intSheetNo + 1) / (double)intMaxSheetSr );
						workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
										

						
						if( intCol == intDataStartCol ){
							
							//データセット
							//自校データ・大学名ほか表示
							//自校学校名
//							workCell = cm.setCell( workSheet, workRow, workCell, 3, 3 );
//							workCell.setCellValue(HomeDataBean.getStrGakkomei());
							//自校データ
//							ListIterator itrHome = HomeDataBean.getB44HyoukaNinzuList().listIterator();
//							int intLoopIndex = 0;
//							while( itrHome.hasNext() ){
//								B44HyoukaNinzuListBean HomeHyoukaNinzuListBean = (B44HyoukaNinzuListBean) itrHome.next();
//								boolean ret = setData( workSheet, HomeHyoukaNinzuListBean, intSetRow + intLoopIndex , 3 );
//								if( ret == false ){
//									return errfdata;
//								}
//								intLoopIndex++;
//							}
							
							if( bolDispHyoukaFlg == true ){
								//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
															intSetRow, intSetRow, 1, 1, false, true, false, false);
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
								workCell.setCellValue( strHyouka );
				
							}

							if( bolDispDaigakuFlg == true ){
								//罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intSetRow, intSetRow, 0, 32, false, true, false, false );
								//大学名表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 0 );
								workCell.setCellValue(b44ListBean.getStrDaigakuMei());
							}
							
						}
					

						bolHomeDataDispFlg = false;

						boolean ret = setData( workSheet, b44HyoukaNinzuListBean, intSetRow + intHyoukaNinzuListIndex, intCol);
						if( ret != true){
							return errfdata;
						}
						else{
							intHyoukaNinzuListIndex++;
							intLoopCnt++;
						}
						
						//
						
					}	//WhileEnd( itr3 )
					
					
					//クラス名
					if( bolClassDispFlg == true ){
//						String strGakunen = b44GakkoListBean.getStrGrade();
						String strGakkomei = b44GakkoListBean.getStrGakkomei();
						workCell = cm.setCell( workSheet, workRow, workCell, 3, intCol );
						workCell.setCellValue(strGakkomei);
					}
					intLoopCnt2++;
				}	//WhileEnd( itr2 )

				intSetRow += intNendoCount;
				bolClassDispFlg = false;

			}	//WhileEnd( itr )
			
			for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//				int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
				int intBIndex = (intSindex + 1) / intMaxSheetSr;
				if((intSindex + 1)%intMaxSheetSr!=0){
					intBIndex++;
				}
				workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
				workSheet = (HSSFSheet)WorkSheetList.get(intSindex);
								
				//罫線出力(セルの上部に太線を引く）
				cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
													intSetRow, intSetRow, 0, 32, false, true, false, false );
				if( workSheet != null ){
					int intRemoveStartRow = intChangeSheetRow;
					if( intMaxRow + intDataStartRow > intRemoveStartRow ){
						for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
							//行削除処理
							workSheet.removeRow(workSheet.getRow(index));
						}
						//罫線出力（セルの上部に太線を出力）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
										intRemoveStartRow, intRemoveStartRow, 0, 32, false, true, false, false);
					}
				}
			
			}

			//ファイル保存
			for( int intBookSr = 0; intBookSr < WorkBookList.size(); intBookSr++ ){
				workbook = (HSSFWorkbook)WorkBookList.get(intBookSr);
				
//				// 元のテンプレートの削除
//				workbook.removeSheetAt(workbook.getSheetIndex(mastersheet));

				boolean bolRet = false;
				// Excelファイル保存
				if( WorkBookList.size() > 1 ){
					if( WorkBookList.size() - 1 == intBookSr ){
						bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookSr+1, masterfile, intMaxSheetIndex);
					}else{
						bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookSr+1, masterfile, intMaxSheetIndex);
					}
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}
			
			//データ無し
			if ( WorkBookList.size() == 0 ){
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex = 1;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, b44Item.getIntSecuFlg(), 30, 32 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
				workCell.setCellValue(secuFlg);
				
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( b44Item.getStrMshDate() );
								
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "対象模試：" + b44Item.getStrMshmei() + moshi);

				//表示対象
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "表示対象：" );
								
				boolean bolRet = false;
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}
			
		}
		catch( Exception e ){
			log.Err("99B44_1","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("B44_01","B44_01帳票作成終了","");
		return noerror;
	}

			
		
	/*
	 * 	Excel編集
	 * 		HSSFWorkbook 	workbook: Excelワークブック
	 * 		ArrayList 		b44HyoukaNinzuList:評価別人数リスト
	 * 		int				intRow:開始行
	 * 		int				intCol:開始列
	 * 		戻り値: なし
	 */
	private boolean setData(HSSFSheet workSheet, B44HyoukaNinzuListBean b44HyoukaNinzuListBean, int intRow, int intCol ){

		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		int		maxSheetIndex	= 0;
		int		row = intRow;
		
		
		// 基本ファイルを読込む
		try{
			
			//模試年度
			workCell = cm.setCell( workSheet, workRow, workCell, row, 2);
			workCell.setCellValue( b44HyoukaNinzuListBean.getStrNendo() + "年度" );
			
			//全国総志望者数
			if( b44HyoukaNinzuListBean.getIntSouShibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol);
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntSouShibo() );
			}
			
			//志望者数（第一志望）
			if( b44HyoukaNinzuListBean.getIntDai1Shibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 1);
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntDai1Shibo() );
			}
			
			//評価別人数
			if( b44HyoukaNinzuListBean.getIntHyoukaA() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 2 );
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntHyoukaA() );
			}

			if( b44HyoukaNinzuListBean.getIntHyoukaB() != -999 ){			
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntHyoukaB() );
			}
			
			if( b44HyoukaNinzuListBean.getIntHyoukaC() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntHyoukaC() );
			}

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
}
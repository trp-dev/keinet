package jp.co.fj.keinavi.excel.data.school;
/**
 * 偏差値分布（クラス比較）データクラス
 * 作成日: 2004/06/28
 * @author	T.Sakai
 */
public class S31ClassListBean {
	//学校名
	private String strGakkomei = "";
	//学年
	private String strGrade = "";
	//クラス名
	private String strClass = "";
	//クラス用グラフ表示フラグ
	private int intDispClassFlg = 0;
	//人数
	private int intNinzu = 0;
	//平均点
	private float floHeikin = 0;
	//平均偏差値
	private float floHensa = 0;
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrGrade() {
		return this.strGrade;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public int getIntDispClassFlg() {
		return this.intDispClassFlg;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public float getFloHeikin() {
		return this.floHeikin;
	}
	public float getFloHensa() {
		return this.floHensa;
	}
	
	/*----------*/	
	/* Set      */	
	/*----------*/	
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setIntDispClassFlg(int intDispClassFlg) {
		this.intDispClassFlg = intDispClassFlg;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setFloHeikin(float floHeikin) {
		this.floHeikin = floHeikin;
	}
	public void setFloHensa(float floHensa) {
		this.floHensa = floHensa;
	}

}

package jp.co.fj.keinavi.servlets.individual;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.graph.BalanceChartApplet;
import jp.co.fj.keinavi.beans.graph.LineGraphApplet;
import jp.co.fj.keinavi.beans.graph.LineGraphApplet2;
import jp.co.fj.keinavi.beans.graph.LineGraphAppletQ;

/**
 * @author QQ) K.Hisakawa
 *
 * グラフ画像表示用サーブレット
 *
 *
 */
public class GraphServlet extends IndividualServlet {

    /**
     * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
     * javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    protected void execute(
        final HttpServletRequest request,
        final HttpServletResponse response)
        throws ServletException, IOException{

        super.execute(request, response);

        //BalanceChartApplet App = new BalanceChartApplet();

        HttpSession session = request.getSession(true);

        String graphType = (String) session.getAttribute("GraphType");
        session.removeAttribute("GraphType");

        //App = (BalanceChartApplet) session.getAttribute("GraphServlet");
        //session.removeAttribute("GraphServlet");

        //App.init();

        try {
            BufferedImage image = new BufferedImage(848, 320, BufferedImage.TYPE_INT_BGR);

            Graphics2D graphics = image.createGraphics();
            graphics.setBackground(Color.WHITE);
            graphics.clearRect(0, 0, image.getWidth(), image.getHeight());

            if(graphType == "BalanceChart") {
            	((BalanceChartApplet) session.getAttribute("GraphServlet")).paint(graphics);
            } else if(graphType == "LineGraph") {
            	((LineGraphApplet) session.getAttribute("GraphServlet")).paint(graphics);
            } else if(graphType == "LineGraph2") {
            	((LineGraphApplet2) session.getAttribute("GraphServlet")).paint(graphics);
            } else if(graphType == "LineGraphQ") {
            	((LineGraphAppletQ) session.getAttribute("GraphServlet")).paint(graphics);
            }

            session.removeAttribute("GraphServlet");

            graphics.drawImage(image, 0, 0, null);

            response.setContentType("image/png");
			OutputStream os = response.getOutputStream();
			ImageIO.write(image, "png", os);
			os.flush();

            image = null;
            graphics = null;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //App = null;
        }
    }
}

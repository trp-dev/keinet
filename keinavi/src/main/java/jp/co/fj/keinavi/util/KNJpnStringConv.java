package jp.co.fj.keinavi.util;
// JAVA PRESS からの流用です。
// hkana2Han(String string) を追加しました。
// Yoshimoto KAWAI - Totec
//package jp.co.gihyo.javapress.util;

// 2004.10.21
// Yoshimoto KAWAI - Totec
// さらに拗音・促音は直音にする
// 例）
// あんじょう → あんじよう →　ｱﾝｼﾖｳ
// ほっかいどう → ほつかいどう → ﾎﾂｶｲﾄｳ

/**
 * 日本語特有の文字変換を行うクラス
 *
 * @author Toshiyuki Moriya
 */
public class KNJpnStringConv {

    // 半角カタカナ<-->全角カタカナ変換テーブル
    private static final String kanaHanZenTbl[][] = {
        // 2文字構成の濁点付き半角カナ
        // 必ずテーブルに先頭に置いてサーチ順を優先すること
        { "ｶ", "ガ" }, { "ｷ", "ギ" }, { "ｸ", "グ" }, { "ｹ", "ゲ" }, { "ｺ", "ゴ" }, 
        { "ｻ", "ザ" }, { "ｼ", "ジ" }, { "ｽ", "ズ" }, { "ｾ", "ゼ" }, { "ｿ", "ゾ" },
        { "ﾀ", "ダ" }, { "ﾁ", "ヂ" }, { "ﾂ", "ヅ" }, { "ﾃ", "デ" }, { "ﾄ", "ド" },
        { "ﾊ", "バ" }, { "ﾋ", "ビ" }, { "ﾌ", "ブ" }, { "ﾍ", "ベ" }, { "ﾎ", "ボ" }, 
        { "ﾊ", "パ" }, { "ﾋ", "ピ" }, { "ﾌ", "プ" }, { "ﾍ", "ペ" }, { "ﾎ", "ポ" }, 
        { "ｳ", "ヴ" }, { "ﾞ", "゛" },
        // 1文字構成の半角カナ
        { "ｱ", "ア" }, { "ｲ", "イ" }, { "ｳ", "ウ" }, { "ｴ", "エ" }, { "ｵ", "オ" }, 
        { "ｶ", "カ" }, { "ｷ", "キ" }, { "ｸ", "ク" }, { "ｹ", "ケ" }, { "ｺ", "コ" }, 
        { "ｻ", "サ" }, { "ｼ", "シ" }, { "ｽ", "ス" }, { "ｾ", "セ" }, { "ｿ", "ソ" }, 
        { "ﾀ", "タ" }, { "ﾁ", "チ" }, { "ﾂ", "ツ" }, { "ﾃ", "テ" }, { "ﾄ", "ト" }, 
        { "ﾅ", "ナ" }, { "ﾆ", "ニ" }, { "ﾇ", "ヌ" }, { "ﾈ", "ネ" }, { "ﾉ", "ノ" }, 
        { "ﾊ", "ハ" }, { "ﾋ", "ヒ" }, { "ﾌ", "フ" }, { "ﾍ", "ヘ" }, { "ﾎ", "ホ" }, 
        { "ﾏ", "マ" }, { "ﾐ", "ミ" }, { "ﾑ", "ム" }, { "ﾒ", "メ" }, { "ﾓ", "モ" }, 
        { "ﾔ", "ヤ" }, { "ﾕ", "ユ" }, { "ﾖ", "ヨ" }, 
        { "ﾗ", "ラ" }, { "ﾘ", "リ" }, { "ﾙ", "ル" }, { "ﾚ", "レ" }, { "ﾛ", "ロ" }, 
        { "ﾜ", "ワ" }, { "ｦ", "ヲ" }, { "ﾝ", "ン" }, 
        { "ｱ", "ァ" }, { "ｲ", "ィ" }, { "ｳ", "ゥ" }, { "ｴ", "ェ" }, { "ｵ", "ォ" }, 
//		{ "ﾔ", "ャ" }, { "ﾕ", "ュ" }, { "ﾖ", "ョ" }, { "ﾂ", "ッ" }, 
		{ "ﾔ", "ャ" }, { "ﾕ", "ュ" }, { "ﾖ", "ョ" }, { "ﾂ", "ッ" }, 
        { "｡", "。" }, { "｢", "「" }, { "｣", "」" }, { "､", "、" }, { "･", "・" }, 
		{ "ｰ", "ー" }, { "ﾟ", "゜" }, { "ﾞ", "゛" }, { "" , ""   }
	};

    /**
     * 文字列に含まれる全角カナを半角カナに変換するメソッド
     * @param p 変換する全角カナ文字列
     * @return 変換後の半角カナ文字列
     */
    public static String kkanaZen2Han(String p) {
        String str = "";
        // パラメータの文字列を先頭から1文字づつ調べます
        for (int i = 0, j = 0; i < p.length(); i++) {
            // 文字列から１文字取り出します
            Character c = new Character(p.substring(i, i + 1).charAt(0));
            // Unicode全角カタカナのコード範囲か調べます
            if (c.compareTo(new Character((char)0x30a1)) >= 0
                && c.compareTo(new Character((char)0x30fc)) <= 0) {
                // 半角全角変換テーブルから全角カナにマッチするエントリを探し、
                // 対応する半角カナを取得して戻り文字列へセットします
                for (j = 0; j < kanaHanZenTbl.length; j++) {
                    if (p.substring(i).startsWith(kanaHanZenTbl[j][1])) {
                        str = str + kanaHanZenTbl[j][0];
                        break;
                    }
                }
                // 半角全角変換テーブルの全角カナにマッチするエントリがなければ
                if (j >= kanaHanZenTbl.length) {
                    str = str + p.substring(i, i + 1);
                }
            } else { // 全角カタカナ以外なら変換せずにそのまま戻り文字列へセットします
                str = str + p.substring(i, i + 1);
            }
        }
        // 変換後文字列を戻します
        return str;
    }

    /**
     * 文字列に含まれる全角かなを全角カナに変換するメソッド
     * @param p 変換する全角かな文字列
     * @return 変換後の全角カナ文字列
     */
    public static String hkana2Kkana(String p) {
        String str = "";
        // パラメータの文字列を先頭から1文字づつ調べます
        for (int i = 0, j = 0; i < p.length(); i++) {
            // 文字列から１文字取り出します
            Character c = new Character(p.substring(i, i + 1).charAt(0));
            // Unicode全角ひらがなのコード範囲(ぁ〜ん)であるか調べます
            if (c.compareTo(new Character((char)0x3041)) >= 0
                && c.compareTo(new Character((char)0x3093)) <= 0) {
                // 全角かな文字に0x0060を加算して全角カナ文字に変換します
                Character x =
                    new Character(
                        (char) (c.charValue()
                            + (new Character((char)0x0060)).charValue()));
                // 文字列としてセットします
                str = str + x.toString();
            } else { // 全角カタカナ以外なら変換せずにそのまま戻り文字列へセットします
                str = str + p.substring(i, i + 1);
            }
        }
        // 変換後文字列を戻します
        return str;
    }
    
    /**
     * 文字列に含まれる全角かなを半角カナに変換するメソッド
	 * @param string 変換する全角かな文字列
	 * @return 変換後の半角カナ文字列
	 */
	public static String hkana2Han(String string) {
    	return KNJpnStringConv.kkanaZen2Han(KNJpnStringConv.hkana2Kkana(string));
    }
    
}

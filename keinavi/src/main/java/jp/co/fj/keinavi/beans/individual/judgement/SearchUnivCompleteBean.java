package jp.co.fj.keinavi.beans.individual.judgement;

import java.util.ArrayList;
import java.util.List;

import com.fjh.beans.DefaultBean;

import jp.co.fj.keinavi.data.individual.UnivKeiNavi;


/**
 *
 * 大学コード・学部コード・学科コード完全一致検索
 * 
 * 
 * <2010年度改修>
 * 2009.12.01   Tomohisa YAMADA - Totec
 *              マスタデータの持ち方変更
 *              
 *
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 *
 */
public class SearchUnivCompleteBean extends DefaultBean implements java.io.Serializable {

	/**
	 * 検索対象[0]大学コード[1]学部コード[2]学科コード
	 */
	private String[][] searchCodeArray;
	/**
	 * 比較対象データ
	 */
	private List compairData = new ArrayList();
	/**
	 * 結果リスト
	 */
	private List resultArrayList = new ArrayList();

	/**
	 * 比較対象リストから指定の大学コード・学部コード・学科コードで
	 * 大学を見つけて結果リストに詰めて返す
	 */
	public void execute() {
		if(searchCodeArray != null){
			List iniArray = this.getCompairData(); //比較対象マスタデータ
			if (iniArray != null) {
				for (int i = 0; i < iniArray.size(); i++) {
					UnivKeiNavi univ = (UnivKeiNavi) iniArray.get(i);
					for (int j = 0; j < this.searchCodeArray.length; j++) {
						//選択された大学名と比較対照データの大学名を比較する
						if (univ.getUnivCd().equals(searchCodeArray[j][0])&& univ.getFacultyCd().equals(searchCodeArray[j][1])&& univ.getDeptCd().equals(searchCodeArray[j][2])) {
							//選択大学の場合は比較対照用データとしてセットする
							if(searchCodeArray[j].length == 4 && searchCodeArray[j][3] != null){
								univ.setAddedIndex(Integer.parseInt(searchCodeArray[j][3]));
							}
							resultArrayList.add(univ);
						}
					}
				}
			}
		}
	}

	/**
	 * @return
	 */
	public List getCompairData() {
		return compairData;
	}

	/**
	 * @return
	 */
	public List getResultArrayList() {
		return resultArrayList;
	}

	/**
	 * @return
	 */
	public String[][] getSearchCodeArray() {
		return searchCodeArray;
	}

	/**
	 * @param list
	 */
	public void setCompairData(List list) {
		compairData = list;
	}

	/**
	 * @param list
	 */
	public void setResultArrayList(List list) {
		resultArrayList = list;
	}

	/**
	 * @param strings
	 */
	public void setSearchCodeArray(String[][] strings) {
		searchCodeArray = strings;
	}


}
/*
 * 作成日: 2004/07/20
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.individual.ExamComboData;
import jp.co.fj.keinavi.data.individual.StudentData;
import jp.co.fj.keinavi.data.individual.SubRecordData;
import jp.co.fj.keinavi.data.individual.SubjectData;
import jp.co.fj.keinavi.util.date.ShortDateUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;


/**
 * 
 * 2005.02.25 Yoshimoto KAWAI - Totec
 *            担当クラスの年度対応
 * 
 * @author Administrator
 * 
 */
public class I002Bean extends DefaultBean{

	private boolean isNewMode;
	
	private String[][] classes;		//担当クラス
	private String schoolCd;			//学校コード
	private String bundleCd;			//一括コード
	private String examYear;			//対象模試年度
	private String examCode;			//対象模試コード
	private String examName;			//対象模試名
	private String examCodePrev;		//前回比較対象模試
	private String subCd;				//科目コード
	private String selectGrade;		//選択学年
	private String selectClass;		//選択クラス
	private String[] selectPerson;		//選択個人
	private boolean helpDesk;			//ヘルプデスクかどうか
	private String year;				//担当クラス年度
	
	/** フォーム出入力 */
	private String formGrade;
	private String formClass;
	private String formSubCd;
	private String formExamYear;
	private String formExamCd;
	private String formExamName;
	private String formPExamCd;

	/** データ出力 */
	//科目表示用リスト 9/24
	private List subjectList;
	//生徒科目別成績一覧
	private List recordList;
	//模試コンボ
	private ExamComboData examComboData = new ExamComboData();

	// 模試データリスト
	private List examDataList = new ArrayList();
	// 模試年度の配列
	private List examYearList = new ArrayList();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 * 初回アクセス時はフォーム値を渡す必要がある！
	 * 1.学校の担当の学年とクラスの生徒を全て取得
	 * 2.上記で得た生徒が一人でも受けた模試とその同系統(EXAMTYPECD)の過去模試の一覧を取得
	 * 3.模試コンボの作成
	 * 4.模試一覧の最新の模試とその同系統(EXAMTYPECD)の過去模試も最新を取得
	 * 5.最新模試(選択された模試)の科目一覧を取得
	 * 6.科目コンボの作成
	 * 7.科目コンボの一番上の科目で、今回対象模試と前回の対象模試
	 * のスコアと推移を取得、1.で得たリストに成績と推移を付属させる
	 * 8.学籍番号の順にソート
	 * 9.コンボ用に担当の生徒の全ての学年とクラスを取得
	 * 10.二回目以降の場合は学年クラスの指定があるので絞り込みをする
	 */
	public void execute() throws SQLException, Exception {
		
		// 担当クラスがなければここまで
		if (this.classes.length == 0) return;
		
		//生徒の一覧
		List individualList = new ArrayList();
		
		// 担当クラスの生徒を取得する
		String[] ids = null;
		{
			if ("".equals(this.selectGrade)) this.selectGrade = null;
			if ("".equals(this.selectClass)) this.selectClass = null;

			IndividualSearchBean bean = new IndividualSearchBean();
			bean.setConnection("", conn);
			bean.setClasses(getClasses());
			bean.setSchoolCd(getSchoolCd());
			bean.setSelectGrade(this.selectGrade);
			bean.setSelectClass(this.selectClass);
			bean.setYear(this.year);
			bean.execute();

			individualList = bean.getRecordSetAll();

			// 対象学年を確定する
			if (this.selectGrade == null) {
				
				// 選択中の生徒がいる学年を取得
				for (int i=0; selectPerson != null && i<selectPerson.length; i++) {
					int index = individualList.indexOf(new StudentData(selectPerson[i]));
					if (index >= 0) {
						this.selectGrade
							 = ((StudentData) individualList.get(index)).getStudentGrade();
						break;
					}
				}
			
				// 学年が見つからなければ {3,2,1,4,5} のいずれか
				if (this.selectGrade == null) {
					// 存在しない生徒なので選択中の生徒はクリアする
					this.selectPerson = null;
					String[] key = new String[] {"3", "2", "1", "4", "5", "9"};
				
					for (int i=0; i<key.length; i++) {
						Iterator ite = individualList.iterator();
						while (this.selectGrade == null && ite.hasNext()) {
							StudentData data = (StudentData) ite.next();
							if (data.getStudentGrade().equals(key[i])) {
								this.selectGrade = data.getStudentGrade();
							}
						}
					}
				}
			}

			// 対象学年以外は消す
			{
				List delete = new ArrayList();
				Iterator ite = individualList.iterator();
				while (ite.hasNext()) {
					StudentData data = (StudentData) ite.next();

					if (!data.getStudentGrade().equals(this.selectGrade)) {
						delete.add(data);
					}
				}
				individualList.removeAll(delete);
			}

			// 生徒のIDセット
			Set id = new HashSet();
			{
				Iterator ite = individualList.iterator();
				while(ite.hasNext()){
					StudentData data = (StudentData) ite.next();
					id.add(data.getStudentPersonId());
				}

				ids = (String[]) id.toArray(new String[0]);
			}

			// フォーム値の初期化
			this.formGrade = this.selectGrade;
			this.formClass = this.selectClass;
		}
		
		// 模試リストの生成
		{
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				// 担当クラスを一時表へ入れる
				ps = conn.prepareStatement("INSERT INTO countchargeclass VALUES(?, ?, 0)");

				for (int i=0; classes != null && i<classes.length; i++) {
					ps.setInt(1, Integer.parseInt(classes[i][0]));
					ps.setString(2, classes[i][1]);
					ps.executeUpdate();
				}
	
				ps.close();

				// 模試データを取得する
				Query query = QueryLoader.getInstance().load("i02");
				if (helpDesk) query.replaceAll("out_dataopendate", "in_dataopendate");
				
				// 今年度
				//String current =KNUtil.getCurrentYear();
				// 今日の日付
				String today = new ShortDateUtil().date2String();
				
				ps = conn.prepareStatement(query.toString());
				ps.setString(1, year); // 年度
				ps.setString(2, this.schoolCd); // 学校コード
				ps.setString(3, year); // 年度
				ps.setString(4, this.schoolCd); // 学校コード
				ps.setString(5, this.bundleCd); // 一括コード
				ps.setString(6, today); // 今日
				ps.setString(7, year); // 年度
				
				rs = ps.executeQuery();
				while (rs.next()) {
					ExamData data = new ExamData();
					data.setExamYear(rs.getString(1));
					data.setExamCD(rs.getString(2));
					data.setExamName(rs.getString(3));
					data.setExamTypeCD(rs.getString(4));
					data.setTargetGrade(rs.getString(5));
					data.setDataOpenDate(rs.getString(6));
					data.setDispSequence(rs.getInt(7));
					this.examDataList.add(data);
				}
				
				conn.commit();
				
			} catch (Exception e) {
				conn.rollback();
				throw e;
				
			} finally {
				DbUtils.closeQuietly(rs);
				DbUtils.closeQuietly(ps);
			}
			
			// 年度リスト
			{
				Iterator ite = this.examDataList.iterator();
				while (ite.hasNext()) {
					ExamData data = (ExamData) ite.next();	
					if (!this.examYearList.contains(data.getExamYear()))
						this.examYearList.add(data.getExamYear());
				}			
			}

			// 現在の模試がリストになければ初期化
			if (this.examDataList.indexOf(new ExamData(this.examYear, this.examCode)) < 0) {
				// 最新模試
				List container = new ArrayList();
				container.addAll(this.examDataList);
				Collections.sort(container);
			
				if (container.size() > 0) {
					ExamData data = (ExamData) container.get(0);

					formExamYear = data.getExamYear();
					formExamCd = data.getExamCD();
					formExamName = data.getExamName();
					
					// 前回模試は模試種類が同じ過去の模試で同年度・同学年
					Iterator ite = container.iterator();
					while (ite.hasNext()) {
						ExamData past = (ExamData) ite.next();
						
						if (data.getExamYear().equals(past.getExamYear())
							&& data.getExamTypeCD().equals(past.getExamTypeCD())
							&& data.getTargetGrade().equals(past.getTargetGrade())
							&& data.getDataOpenDate().compareTo(past.getDataOpenDate()) > 0) {
								
							formPExamCd = past.getExamCD();
							break;
						}
					}

				} else {
					formExamYear = "";
					formExamCd = "";
					formExamName = "";
					formPExamCd = "";
				}
			} else {
				formExamYear = this.examYear;
				formExamCd = this.examCode;
				formExamName = this.examName;
				formPExamCd = this.examCodePrev;				
			}		
		}

		// 模試コンボデータの初期化
		{
			Iterator ite = this.examDataList.iterator();
			while (ite.hasNext()) {
				ExamData data = (ExamData) ite.next();	

				ExamComboData.YearData yearData =
					(ExamComboData.YearData) examComboData.get(data.getExamYear());

				if (yearData == null) {	
					yearData = examComboData.getNewYearData();
					yearData.setYear(data.getExamYear());
					yearData.setExamDatas(new ArrayList());
					examComboData.addYearData(yearData);
				}	

				ExamComboData.YearData.ExamData examData =
					yearData.get(data.getExamCD());

				if (examData == null) {
					examData = yearData.getNewExamData();
					examData.setExamYear(data.getExamYear());
					examData.setExamCode(data.getExamCD());
					examData.setExamName(data.getExamName());
					examData.setExamNameAbbr(data.getExamNameAbbr());
					examData.setExamTypeCode(data.getExamTypeCD());
					examData.setDispSequence(String.valueOf(data.getDispSequence()));
					examData.setInpleDate(data.getInpleDate());
					examData.setTargetGrade(data.getTargetGrade());
					examData.setOutDataOpenDate(data.getDataOpenDate());
					examData.setPreExamDatas(new ArrayList());
					yearData.addExamData(examData);
				}
			}
		}
			
		// 科目リストを取得する
		{
			SubjectSearchBean bean = new SubjectSearchBean();
			bean.setConnection("", conn);
			bean.setExamYear(this.formExamYear);
			bean.setExamCd(this.formExamCd);
			bean.execute();
			this.subjectList = bean.getRecordSet();
		}
		
		//科目一覧が存在する場合
		if (subjectList.size() > 0){

			// 科目コードの初期値
			if (subCd == null || "".equals(subCd)
					|| subjectList.indexOf(new SubjectData(subCd)) < 0) {
				formSubCd = ((SubjectData) subjectList.get(0)).getSubjectCd(); 
			} else {
				formSubCd = subCd;
			}

			List record = execRecord(formExamYear, formExamCd, formSubCd, formPExamCd, ids);
			SubRecordData[] srds = (SubRecordData[]) record.toArray(new SubRecordData[record.size()]);
			//成績と生徒を合体させる
			ListIterator ite2 = individualList.listIterator();
			while(ite2.hasNext()){
				StudentData sd = (StudentData)ite2.next();
				boolean scoreExist = false;
				for(int i=0; i<srds.length; i++){
					//生徒が存在する
					if(sd.getStudentPersonId().equals(srds[i].getPersonId())){
						sd.setSubRecordData(srds[i]);
						scoreExist = true;
					}
				}
				//この模試のこの科目に対する成績がない。空を入れます。
				if(!scoreExist){
					SubRecordData srd = new SubRecordData();
					srd.setSubName("");
					srd.setSDeviation("");
					srd.setComparedCDeviation("-999");
					sd.setSubRecordData(srd);
				}
			}
//			//ソート
//			if(individualList != null){
//				Collections.sort(individualList, new StudentSorter().new SortByGradeClass());
//			}
		}
		
		recordList = individualList;
	}
	
	/**
	 * @param examYear
	 * @param examCd
	 * @param subCd
	 * @param preExamCd
	 * @param indOptionQuery
	 * @return
	 * @throws Exception
	 */
	private List execRecord(String examYear, String examCd, String subCd, String preExamCd, String[] ids) throws Exception{
		
		List recordSet = new ArrayList();

		if (ids == null || ids.length == 0) return recordSet;

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String tempIndId = "";
			
			Query query = QueryLoader.getInstance().load("i04");
			query.setStringArray(1, ids);
			
			// 総合の場合
			if ("7777".equals(subCd)) {
				query.replaceAll("R.SUBCD = ?", "R.SUBCD >= '7000' AND R.SUBCD < '8000'");
				ps = conn.prepareStatement(query.toString());
				ps.setString(1, preExamCd);
				ps.setString(2, examYear);
				ps.setString(3, examCd);
				
			// それ以外
			} else {
				ps = conn.prepareStatement(query.toString());
				ps.setString(1, preExamCd);
				ps.setString(2, examYear);
				ps.setString(3, examCd);
				ps.setString(4, subCd);
			}
			
			rs = ps.executeQuery();
			while(rs.next()){
				SubRecordData data = new SubRecordData();
				data.setPersonId(rs.getString(1));

				// 総合の場合複数件出てきてしまうので、二回目以降はリストに追加しない
				if (!data.getPersonId().equals(tempIndId)) {
					data.setExamYear(rs.getString(2));
					data.setExamCd(rs.getString(3));
					data.setSubCd(rs.getString(4));
					data.setSDeviation(nullToEmptyF(rs.getString(5))); // 今回偏差値
					data.setSDeviationPre(nullToEmptyF(rs.getString(6))); // 前回偏差値
					data.setSubName(rs.getString(7)); // 科目名
					data.setComparedCDeviation(rs.getString(8));
					// 対象模試を受験していない場合は以下の出力を出す。
					if (data.getSubName() == null) {
						data.setSubName("");
						data.setSDeviation("");
						data.setComparedCDeviation("");
					}
					recordSet.add(data);
				}
				tempIndId = data.getPersonId();
			}

			return recordSet;

		} catch (Exception e) {
			throw e;
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
	}
	
	/**
	 * StringをFloatのフォーマットで返します。
	 * @return
	 */
	public String nullToEmptyF(String str) {
		if(str == null) {
			return "";
		} else {
				return String.valueOf(new Float(str).floatValue());
		}
	}
	
	/**
	 * @return
	 */
	public String[][] getClasses() {
		return classes;
	}

	/**
	 * @return
	 */
	public String getExamCode() {
		return examCode;
	}

	/**
	 * @return
	 */
	public String getExamCodePrev() {
		return examCodePrev;
	}

	/**
	 * @return
	 */
	public ExamComboData getExamComboData() {
		return examComboData;
	}

	/**
	 * @return
	 */
	public String getExamName() {
		return examName;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @return
	 */
	public boolean isNewMode() {
		return isNewMode;
	}

	/**
	 * @return
	 */
	public List getRecordList() {
		return recordList;
	}

	/**
	 * @return
	 */
	public String getSchoolCd() {
		return schoolCd;
	}

	/**
	 * @return
	 */
	public String getSubCd() {
		return subCd;
	}

	/**
	 * @return
	 */
	public List getSubjectList() {
		return subjectList;
	}

	/**
	 * @param strings
	 */
	public void setClasses(String[][] strings) {
		classes = strings;
	}

	/**
	 * @param string
	 */
	public void setExamCode(String string) {
		examCode = string;
	}

	/**
	 * @param string
	 */
	public void setExamCodePrev(String string) {
		examCodePrev = string;
	}

	/**
	 * @param data
	 */
	public void setExamComboData(ExamComboData data) {
		examComboData = data;
	}

	/**
	 * @param string
	 */
	public void setExamName(String string) {
		examName = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @param b
	 */
	public void setNewMode(boolean b) {
		isNewMode = b;
	}

	/**
	 * @param list
	 */
	public void setRecordList(List list) {
		recordList = list;
	}

	/**
	 * @param string
	 */
	public void setSchoolCd(String string) {
		schoolCd = string;
	}

	/**
	 * @param string
	 */
	public void setSubCd(String string) {
		subCd = string;
	}

	/**
	 * @param list
	 */
	public void setSubjectList(List list) {
		subjectList = list;
	}

	/**
	 * @return
	 */
	public String getFormClass() {
		return formClass;
	}

	/**
	 * @return
	 */
	public String getFormGrade() {
		return formGrade;
	}

	/**
	 * @return
	 */
	public String getFormSubCd() {
		return formSubCd;
	}

	/**
	 * @param string
	 */
	public void setFormClass(String string) {
		formClass = string;
	}

	/**
	 * @param string
	 */
	public void setFormGrade(String string) {
		formGrade = string;
	}

	/**
	 * @param string
	 */
	public void setFormSubCd(String string) {
		formSubCd = string;
	}

	/**
	 * @return
	 */
	public String getFormExamCd() {
		return formExamCd;
	}

	/**
	 * @return
	 */
	public String getFormExamName() {
		return formExamName;
	}

	/**
	 * @return
	 */
	public String getFormExamYear() {
		return formExamYear;
	}

	/**
	 * @return
	 */
	public String getFormPExamCd() {
		return formPExamCd;
	}

	/**
	 * @param string
	 */
	public void setFormExamCd(String string) {
		formExamCd = string;
	}

	/**
	 * @param string
	 */
	public void setFormExamName(String string) {
		formExamName = string;
	}

	/**
	 * @param string
	 */
	public void setFormExamYear(String string) {
		formExamYear = string;
	}

	/**
	 * @param string
	 */
	public void setFormPExamCd(String string) {
		formPExamCd = string;
	}

	/**
	 * @return
	 */
	public String getSelectClass() {
		return selectClass;
	}

	/**
	 * @return
	 */
	public String getSelectGrade() {
		return selectGrade;
	}

	/**
	 * @param string
	 */
	public void setSelectClass(String string) {
		selectClass = string;
	}

	/**
	 * @param string
	 */
	public void setSelectGrade(String string) {
		selectGrade = string;
	}

	/**
	 * @return
	 */
	public String[] getSelectPerson() {
		return selectPerson;
	}

	/**
	 * @param strings
	 */
	public void setSelectPerson(String[] strings) {
		selectPerson = strings;
	}

	/**
	 * @return
	 */
	public List getExamDataList() {
		return examDataList;
	}

	/**
	 * @return
	 */
	public List getExamYearList() {
		return examYearList;
	}

	/**
	 * @param list
	 */
	public void setExamDataList(List list) {
		examDataList = list;
	}

	/**
	 * @param list
	 */
	public void setExamYearList(List list) {
		examYearList = list;
	}

	/**
	 * @return
	 */
	public String getBundleCd() {
		return bundleCd;
	}

	/**
	 * @param string
	 */
	public void setBundleCd(String string) {
		bundleCd = string;
	}

	/**
	 * @return
	 */
	public boolean isHelpDesk() {
		return helpDesk;
	}

	/**
	 * @param b
	 */
	public void setHelpDesk(boolean b) {
		helpDesk = b;
	}

	/**
	 * @return year を戻す。
	 */
	public String getYear() {
		return this.year;
	}
	/**
	 * @param year year を設定する。
	 */
	public void setYear(String year) {
		this.year = year;
	}
}

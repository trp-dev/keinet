/*
 * 作成日: 2004/09/28
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.helpdesk;

import java.text.SimpleDateFormat;
import java.text.ParseException;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class FormatChecker {


	// char
	public static String str2Char32(String str, int index) {
		String s = "";
		if (str != null) {

			int count = str.length();
			int n = index - count;
			for (int i = 0; i < n; i++) {
				s += " ";
			}
			return str + s;
		} else {
			return str;
		}
	}

	/**
	* 学校コードチェック
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isSchoolCode(String _str) {
		boolean bl = false;
		try {
			if (1 <= _str.length() && _str.length() <= 5)
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* ユーザIDチェック
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isUserId(String _str) {
		boolean bl = false;
		try {
			if (1 <= _str.length() && _str.length() <= 8)
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* 初期パスワードチェック
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isFirstPass(String _str) {
		boolean bl = false;
		try {
			if (1 <= _str.length() && _str.length() <= 8)
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* ユーザ区分チェック
	*   1:公立  2:私立  3:予備校  5:教育委員会  6:ヘルプデスク
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isUserKubun(String _str) {
		boolean bl = false;
		try {
			if (_str.equals("1") || _str.equals("2") || _str.equals("3") || _str.equals("5") || _str.equals("6") )
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* 契約区分チェック
	*   1:契約校  2:非契約校  3:リサーチ参加校  4:非受験校  5:ヘルプデスク
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isContractKubun(String _str) {
		boolean bl = false;
		try {
			if (_str.equals("1") || _str.equals("2") || _str.equals("3") || _str.equals("4") || _str.equals("5"))
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* 最大セッション数チェック
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isSessionNumber(String _str) {
		boolean bl = false;
		try {
			if (isNumber(_str) && (0 <= Integer.parseInt(_str) && Integer.parseInt(_str) <= 999) )
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* 私書箱機能提供チェック
	*
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isPostFunction(String _str) {
		boolean bl = false;
		try {
			if (_str.equals("0") || _str.equals("1"))
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}



	/**
	* 日付の文字列が日付として正しいか
	* @param sDate 対象の値
	* @return 判定
	*/
	public static boolean isDate(String sDate) {
		boolean judge = true;
		SimpleDateFormat sdf01 = new SimpleDateFormat("yyyyMMdd");
		//日付/時刻解析を厳密に行うかどうかを設定。
		//falseの場合は厳密な解析
		sdf01.setLenient(false);
		try {
			//日付/時刻文字列を解析
			sdf01.parse(sDate);
		} catch (ParseException px) {
			judge = false;
		} catch (Exception ex) {
			judge = false;
		}
		return judge;
	}
	
	/**
	* 数値の判定
	* @param _str 対象の値
	* @return 判定
	*/
	private static boolean isNumber(String _str) {
		boolean bl = false;
		try {
			Long nm = new Long(_str);
			if (nm != null)
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}


}

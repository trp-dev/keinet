/*
 * 作成日: 2004/06/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.profile;

import com.fjh.forms.ActionForm;


/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class W009Form extends ActionForm {

	// プロファイルフォルダID
	private String folderID = null;
	// 名称
	private String folderName = null;
	// コメント
	private String comment = null;
	// 集計区分コード
	private String countingDivCode = null; 

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * @return
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @return
	 */
	public String getFolderID() {
		return folderID;
	}

	/**
	 * @return
	 */
	public String getFolderName() {
		return folderName;
	}

	/**
	 * @param string
	 */
	public void setComment(String string) {
		comment = string;
	}

	/**
	 * @param string
	 */
	public void setFolderID(String string) {
		folderID = string;
	}

	/**
	 * @param string
	 */
	public void setFolderName(String string) {
		folderName = string;
	}

	/**
	 * @return
	 */
	public String getCountingDivCode() {
		return countingDivCode;
	}

	/**
	 * @param string
	 */
	public void setCountingDivCode(String string) {
		countingDivCode = string;
	}

}

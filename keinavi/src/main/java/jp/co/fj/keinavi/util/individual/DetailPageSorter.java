/*
 * 作成日: 2005/2/28
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.individual;

import java.util.Comparator;

import jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class DetailPageSorter {

	/*
	 * 
	 * @author Administrator
	 *
	 * 枝番順でソート
	 */
	public class SortByBranchCd implements Comparator{

		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {
			DetailPageBean j1 = (DetailPageBean) o1;
			DetailPageBean j2 = (DetailPageBean) o2;
			int branch1 = Integer.parseInt(j1.getSelectedBranch());
			int branch2 = Integer.parseInt(j2.getSelectedBranch());
			if(branch1 < branch2){
				return -1;	
			}else if(branch1 > branch2){
				return 1;
			}else{
				return 0;
			}
		}
		
	}	
}

/**
 * 高校成績分析−高校間比較（過年度：前年度）
 * 	Excelファイル編集
 * 作成日: 2004/08/04
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.business;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.data.sheet.ExtB41SeisekiListBean;
import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.business.B41Item;
import jp.co.fj.keinavi.excel.data.business.B41ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class B41_02 {

	private int noerror	= 0;		// 正常終了
	private int errfread	= 1;		// ファイルreadエラー
	private int errfwrite	= 2;		// ファイルwriteエラー
	private int errfdata	= 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	final private String	masterfile0	= "B41_02";		// ファイル名
	final private String	masterfile1	= "NB41_02";	// ファイル名
	private String	masterfile	= "";					// ファイル名
	final private int[]	tabColNow	= {1,9,17,25,33};	// 表の基準点(今年度)
	final private int[]	tabColPast	= {5,13,21,29,37};	// 表の基準点(前年度)


	/*
	 * 	Excel編集メイン
	 * 		B41Item b41Item: データクラス
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
 	public int b41_02EditExcel(B41Item b41Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (b41Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			// マスタExcel読み込み
			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFSheet		workSheet2	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;
		
			// 基本ファイルを読込む
			B41ListBean b41ListBean = new B41ListBean();
			
			// データセット
			ArrayList	b41List			= b41Item.getB41List();
			ArrayList	workbookList	= new ArrayList();
			ArrayList	workSheetList	= new ArrayList();
			Iterator	itr				= b41List.iterator();
			int		starCnt			= 1;	// *表示用カウンター
			float		shelHensa		= 0;	// *表示用偏差値退避変数
			boolean	bolhyouji		= true;// *表示用表示判定フラグ
			int		bestNo			= 0;	// 上位○件　○にあたる数字が入る(半角数字)
			String		joui			= "";	// 上位○件　○にあたる数字が入る(全角文字)
			int		row				= 0;	// 行
			int		kmkCnt			= 0;	// 科目カウンター
			int		maxSheetIndex	= 0;	// シートカウンター
			int		allSheetIndex	= 0;	// シートカウンター
			int		intMaxSheetSr	= 50;	// MAXシート数
			int		hyoujiNum		= 50;	// 最大表示件数
			int		fileIndex		= 1;	// ファイルカウンター
			int		dataSize		= 0;	// リストデータ件数
			int		shelSheetNum	= 0;	// シート番号保持
			int		sheetNumRow		= 0;	// シート枚数（Row方向）
			boolean	kataFlg			= true;// false:改行済(型・科目名変更済)　
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
			

			// b41Listのセット
			while( itr.hasNext() ) {

				b41ListBean = (B41ListBean)itr.next();

				// 基本ファイルを読み込む
				ExtB41SeisekiListBean	b41SeisekiListBean	= null;
				
				//データの保持
				ArrayList	b41SeisekiList	= b41ListBean.getB41SeisekiList();
				Iterator	itrB41Seiseki	= b41SeisekiList.iterator();

				//型から科目に変わる時のチェック
				if ( Integer.parseInt(b41ListBean.getStrKmkCd()) < 7000 ) {

					//型・科目切替えの初回のみ処理する
					if (kataFlg){
						if(kmkCnt!=0){
							kataFlg	= false;
							kmkCnt=0;
							bolSheetCngFlg	= true;
						}
						if(kmkCnt==0){
							kataFlg	= false;
						}
					}
				}
				// Best表示用
				if(b41Item.getIntBestFlg()==1){
					joui = "１０";
					bestNo=10;
				}
				if(b41Item.getIntBestFlg()==2){
					joui = "５";
					bestNo=5;
				}

				// 高校データセット
				ArrayList	bestListNow		= new ArrayList();
				ArrayList	bestListPast	= new ArrayList();
				dataSize = b41SeisekiList.size(); //データ件数
//				shelSheetNum = 0;	// シート番号保持

				// 下方向のシート数の計算
				sheetNumRow = dataSize/hyoujiNum;
				if(dataSize%hyoujiNum!=0){
					sheetNumRow++;
				}
				if(sheetNumRow>1){
					bolSheetCngFlg=true;
				}
				// 変数の初期化
				row	= 0;
				while(itrB41Seiseki.hasNext()){
					b41SeisekiListBean	= (ExtB41SeisekiListBean) itrB41Seiseki.next();

					//マスタExcel読み込み
					if(bolBookCngFlg){
						if((maxSheetIndex==intMaxSheetSr)||(maxSheetIndex==0)){
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook==null ){
								return errfread;
							}
							bolBookCngFlg = false;
							maxSheetIndex=0;
							workbookList.add(workbook);
						}
					}

					if ( bolSheetCngFlg ) {

						// シートの初めての型・科目なら新規シート・２科目め以降なら呼び出し
						if(kmkCnt==0){

							// シートテンプレートのコピー
							workSheet = workbook.cloneSheet(0);
							shelSheetNum = allSheetIndex;
							maxSheetIndex++;
							allSheetIndex++;

							// ヘッダ右側に帳票作成日時を表示する
							cm.setHeader(workbook, workSheet);

							// セキュリティスタンプセット
							String secuFlg = cm.setSecurity( workbook, workSheet, b41Item.getIntSecuFlg() ,38 ,40 );
							workCell = cm.setCell( workSheet, workRow, workCell, 0, 38 );
							workCell.setCellValue(secuFlg);

							// 対象模試セット
							String moshi =cm.setTaisyouMoshi( b41Item.getStrMshDate() );	// 模試月取得
							workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
							workCell.setCellValue( "対象模試　　：" + cm.toString(b41Item.getStrMshmei()) + moshi);

							// コメントセット
							if(b41Item.getIntBestFlg()!=3){
								workCell = cm.setCell( workSheet, workRow, workCell, 3, 40 );
								workCell.setCellValue( "※平均偏差値が高い上位" + joui + "校に\"*\"を表示しています。");
							}

							shelSheetNum = allSheetIndex;
						}else{
							// シートの呼び出し
//							workSheet = (HSSFSheet)workSheetList.get(allSheetIndex-sheetNumRow);
							if (intSaveFlg==1 || intSaveFlg==5) {
								workSheet = workbook.getSheetAt(maxSheetIndex-sheetNumRow+1);
							}else{
								workSheet = workbook.getSheetAt(maxSheetIndex-sheetNumRow+1+1);
							}
							shelSheetNum = allSheetIndex-sheetNumRow+1;
							sheetNumRow--;
						}
						//　改シートフラグをFalseにする
						bolSheetCngFlg	=false;
						row=0;
					}

					if(row==0){
						// 型名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 4, tabColNow[kmkCnt] );
						workCell.setCellValue( b41ListBean.getStrKmkmei() );

						// 配点セット
						if( !cm.toString(b41ListBean.getStrHaiten()).equals("")){
							workCell = cm.setCell( workSheet, workRow, workCell, 5, tabColNow[kmkCnt] );
							workCell.setCellValue( " (" + cm.toString(b41ListBean.getStrHaiten()) + ")" );
						}

						// 今年度セット
						workCell = cm.setCell( workSheet, workRow, workCell, 6, tabColNow[kmkCnt] );
						workCell.setCellValue( cm.toString(b41SeisekiListBean.getStrNendoNow()) + "年度" );

						// 前年度セット
						workCell = cm.setCell( workSheet, workRow, workCell, 6, tabColPast[kmkCnt] );
						workCell.setCellValue( cm.toString(b41SeisekiListBean.getStrNendoPast()) + "年度" );

					}

					// 高校名セット
					if(kmkCnt == 0){
						workCell = cm.setCell( workSheet, workRow, workCell, 8+row, 0 );
						workCell.setCellValue( b41SeisekiListBean.getStrGakkomei());
					}

					// 今年度受験人数セット
					if ( b41SeisekiListBean.getIntNinzuNow() != -999 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabColNow[kmkCnt]);
						workCell.setCellValue( b41SeisekiListBean.getIntNinzuNow() );
					}

					// 今年度平均点セット
					if ( b41SeisekiListBean.getFloHeikinNow() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabColNow[kmkCnt]+1);
						workCell.setCellValue( b41SeisekiListBean.getFloHeikinNow() );
					}

					// 今年度平均偏差値セット
					if ( b41SeisekiListBean.getFloStdHensaNow() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabColNow[kmkCnt]+3);
						workCell.setCellValue( b41SeisekiListBean.getFloStdHensaNow() );

						// *表示用リスト格納処理
						// 全国・都道府県は対象外
						if (!b41SeisekiListBean.getSchoolCD().endsWith("999")) {
							mkBestList(b41SeisekiListBean.getFloStdHensaNow(),shelSheetNum,row,bestListNow);
						}
					}

					// 前年度受験人数セット
//					if ( b41SeisekiListBean.getIntNinzuNow() != -999 ) {
					if ( b41SeisekiListBean.getIntNinzuPast() != -999 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabColPast[kmkCnt]);
						workCell.setCellValue( b41SeisekiListBean.getIntNinzuPast() );
					}

					// 前年度平均点セット
//					if ( b41SeisekiListBean.getFloHeikinNow() != -999.0 ) {
					if ( b41SeisekiListBean.getFloHeikinPast() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabColPast[kmkCnt]+1);
						workCell.setCellValue( b41SeisekiListBean.getFloHeikinPast() );
					}

					// 前年度平均偏差値セット
//					if ( b41SeisekiListBean.getFloStdHensaNow() != -999.0 ) {
					if ( b41SeisekiListBean.getFloStdHensaPast() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabColPast[kmkCnt]+3);
						workCell.setCellValue( b41SeisekiListBean.getFloStdHensaPast() );

						// *表示用リスト格納処理
						// 全国・都道府県は対象外
						if (!b41SeisekiListBean.getSchoolCD().endsWith("999")) {
							mkBestList(b41SeisekiListBean.getFloStdHensaPast(),shelSheetNum,row,bestListPast);
						}
					}

					row++;

					// リストにWorkSheetを格納
					if(row%hyoujiNum==0){
						if(itrB41Seiseki.hasNext()){
							if(kmkCnt==0){
								workSheetList.add(workSheet);
							}
							bolSheetCngFlg = true;
							if(workbook.getNumberOfSheets()==intMaxSheetSr){
								bolBookCngFlg = true;
							}
						}
					}
					if(itrB41Seiseki.hasNext()==false){
						if(kmkCnt==0){
							workSheetList.add(workSheet);
						}
					}
				}

				// *表示処理(今年度)
				if(bestListNow.size()!=0){
					Iterator itera = bestListNow.iterator();
					starCnt = 1;
					shelHensa = 0;
					bolhyouji = true;
					while(itera.hasNext()){
						B41BestListBean b41BestListBean =(B41BestListBean)itera.next();
//						workSheet = (HSSFSheet)workSheetList.get(b41BestListBean.getIntSheetNum());
//						workSheet2 = workbook.getSheetAt(b41BestListBean.getIntSheetNum());
						if (intSaveFlg==1 || intSaveFlg==5) {
							workSheet2 = workbook.getSheetAt(b41BestListBean.getIntSheetNum());
						}else{
							workSheet2 = workbook.getSheetAt(b41BestListBean.getIntSheetNum()+1);
						}

						if(starCnt==bestNo){
							shelHensa = b41BestListBean.getFloHensa();
						}
						if(starCnt<=bestNo){
							bolhyouji = true;
						}else{
							if(shelHensa==b41BestListBean.getFloHensa()){
								bolhyouji = true;
							}else{
								break;
							}
						}
						if(bolhyouji){
							workCell = cm.setCell( workSheet2, workRow, workCell, 8+b41BestListBean.getIntRow(), tabColNow[kmkCnt]+2 );
							workCell.setCellValue( "*" );
							
						}
						
						starCnt++;
					}
				}
				// *表示処理(前年度)
				if(bestListPast.size()!=0){
					Iterator itera = bestListPast.iterator();
					starCnt = 1;
					shelHensa = 0;
					bolhyouji = true;
					while(itera.hasNext()){
						B41BestListBean b41BestListBean =(B41BestListBean)itera.next();
//						workSheet = (HSSFSheet)workSheetList.get(b41BestListBean.getIntSheetNum());
//						workSheet2 = workbook.getSheetAt(b41BestListBean.getIntSheetNum());
						if (intSaveFlg==1 || intSaveFlg==5) {
							workSheet2 = workbook.getSheetAt(b41BestListBean.getIntSheetNum());
						}else{
							workSheet2 = workbook.getSheetAt(b41BestListBean.getIntSheetNum()+1);
						}

						if(starCnt==bestNo){
							shelHensa = b41BestListBean.getFloHensa();
						}
						if(starCnt<=bestNo){
							bolhyouji = true;
						}else{
							if(shelHensa==b41BestListBean.getFloHensa()){
								bolhyouji = true;
							}else{
								break;
							}
						}
						if(bolhyouji){
							workCell = cm.setCell( workSheet2, workRow, workCell, 8+b41BestListBean.getIntRow(), tabColPast[kmkCnt]+2 );
							workCell.setCellValue( "*" );
							
						}
						
						starCnt++;
					}
				}

				kmkCnt++;
				if(kmkCnt >= 5){
					kmkCnt=0;
					bolSheetCngFlg =true;
					if(workbook.getNumberOfSheets()==intMaxSheetSr){
						bolBookCngFlg = true;
					}
				}

				// ListにWorkBookが２つ以上格納されている時は出力処理を実行
				int listSize = workbookList.size();
				if(listSize>1){
					if(kmkCnt == 0){
						workbook = (HSSFWorkbook)workbookList.get(0);

						// Excelファイル保存
						boolean bolRet = false;
						bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, workbook.getNumberOfSheets());
						fileIndex++;
						if( bolRet == false ){
							return errfwrite;
						}

						// ファイル出力したデータは削除
						workbookList.remove(0);
						for(int a =0;a<intMaxSheetSr;a++){
							workSheetList.remove(0);
						}

						// WorkBook・変数を書き込んでる途中のものに戻す
						allSheetIndex = maxSheetIndex;
						workbook = (HSSFWorkbook)workbookList.get(0);
					}
				}
				
			}

			// Excelファイル保存
			boolean bolRet = false;
			if(fileIndex != 1){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, workbook.getNumberOfSheets());
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, workbook.getNumberOfSheets());
			}

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("B41_02","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

	/**
	 * 順位リスト作成メソッド
	 * 
	 * @param 偏差値
	 * @param シートNo
	 * @param 表示位置
	 * @param 格納リスト
	 */
	private void mkBestList(float hensa,int sheetNum,int row, ArrayList bestList ) throws Exception {
		try{
			B41BestListBean	b41Best	= new B41BestListBean();
			b41Best.setFloHensa(hensa);
			b41Best.setIntRow(row);
			b41Best.setIntSheetNum(sheetNum);
		
			if(bestList.size()==0){
				bestList.add(b41Best);
			}else{
				Iterator itera = bestList.iterator();
				int cnt=0;
				while(itera.hasNext()){
					B41BestListBean	b41BestListBean = (B41BestListBean)itera.next();
					if(b41BestListBean.getFloHensa()<=hensa){
						bestList.add(cnt,b41Best); 
						break;
					}
					cnt++;
					if(itera.hasNext()==false){
						bestList.add(b41Best);
						break;
					}
				}
			}
		}catch(Exception e){
			throw e;
		}
	}

}
/*
 * 作成日: 2004/10/05
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.admin.name;

import java.io.File;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Savepoint;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import jp.co.fj.keinavi.beans.ExamSearchBean;
import jp.co.fj.keinavi.beans.admin.FormatChecker;
import jp.co.fj.keinavi.beans.admin.IndivChangeInfoTableAccess;
import jp.co.fj.keinavi.beans.admin.application.Application;
import jp.co.fj.keinavi.beans.admin.application.DbAccessInf;
import jp.co.fj.keinavi.util.KNPropertyCtrl;
import jp.co.fj.keinavi.util.KNUtil;

import org.apache.commons.dbutils.DbUtils;

/**
 * 個表データ処理
 *
 * @author 奥村ゆかり
 *
 * 2004.11
 * Yoshimoto KAWAI - Totec
 * マルチスレッド処理はないので出来る限りインスタンスは使いまわす方針にした
 *
 * 2009.10.16   Totec) S.Hase
 *					大学コード１０桁からDEPTCDの抽出を「7文字目から2文字」→「7文字目から4文字」に変更。
 *					それにともない次項目の志望順位も「9文字目から1文字」→「11文字目から1文字」に変更。
 *
 * <新過程対応>
 * 2013.04.23   森田雄一郎 - Totec
 *              execute
 *
 * 2020.04.10 FJ)Tanaka
 * 	学校マスタ番号帯拡大対応
 * 		私塾（一括コード 71xxx 72xxx）ならプライバシー保護フラグは落とす
 *			↓
 * 		私塾（一括コード 71xxx 72xxx 75xxx 76xxx 77xxx 78xxx）ならプライバシー保護フラグは落とす
 *
 *
 *
 */
 
public class NameBringsBean {

	public static final Charset CHARSET = Charset.forName("MS932");
	// ステートメントの入れ物
	public static final Map STATEMENT = new HashMap();
	// エラー発生ROWID
	private static final Set ERROR_ROWID = new HashSet();
	// コミットインターバル
	private static final int INTERVAL_COMMIT = 1000;
	// セレクトインターバル（コミットインターバルの整数倍にしてください）
	private static final int INTERVAL_SELECT = INTERVAL_COMMIT * 10;
	// 最大レコード数（セレクトインターバルの整数倍にしてください）
	private static final int MAX_COUNT = INTERVAL_SELECT * 100;
	// 個表更新カウント
	private static int updTotalCount;
	// 個表新規カウント
	private static int insTotalCount;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// ロックファイル
		File file = null;
		// DBコネクション
		Connection con = null;

		SystemLogWriter.setLogWrite(true);

		SystemLogWriter.printLog(NameBringsBean.class, "■■■■ 名寄せ処理を開始します。 ■■■■", SystemLogWriter.INFO);

		System.out.println("名寄せ処理開始 " + getFormatedTimeString("yyyy/MM/dd HH:mm:ss:SS"));

		boolean success = false;
		try {
			Class.forName(DbAccessInf.getDriver());

			con = DriverManager.getConnection(DbAccessInf.getURL(), DbAccessInf.getLoginId(), DbAccessInf.getPassWord());

			con.setAutoCommit(false);

			// 学校用名寄せクラスにコネクションをセットしておく
			NameBringsForSchool.getInstance().setConnection(con);

			file = new File(Application.getKeinaviDir() + System.getProperty("file.separator") + "running.dat");

			//KNUtilにて必要
			KNPropertyCtrl.FilePath = Application.getKeinaviDir().getAbsolutePath();

			if (file.exists()) {
				// 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
				//throw new Exception("【処理エラー】処理ロックファイルが存在します。：" + file.getPath());
				throw new IllegalStateException("【処理エラー】処理ロックファイルが存在します。：" + file.getPath());
				// 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			} else {
				file.createNewFile();
				file.deleteOnExit();
			}

			System.out.println("■■■■ 名寄せ処理実行中につき、この処理の強制終了は禁止します。 ■■■■");

			// セレクトインターバル単位で繰り返す
			for (int i = 0; i < MAX_COUNT / INTERVAL_SELECT; i++) {
				SystemLogWriter.printLog(NameBringsBean.class, "セレクトループ(" + i + ")", SystemLogWriter.INFO);
				if (execute(con))
					break;
			}

			SystemLogWriter.printLog(NameBringsBean.class, "個表ワークデータ件数" + (insTotalCount + updTotalCount) + "件", SystemLogWriter.INFO);
			SystemLogWriter.printLog(NameBringsBean.class, "新規登録した個表データ件数" + insTotalCount + "件", SystemLogWriter.INFO);
			SystemLogWriter.printLog(NameBringsBean.class, "更新した個表データ件数" + updTotalCount + "件", SystemLogWriter.INFO);
			SystemLogWriter.printLog(NameBringsBean.class, "失敗件数" + ERROR_ROWID.size() + "件", SystemLogWriter.INFO);

			success = true;
		} catch (final Exception e) {
			e.printStackTrace();
			SystemLogWriter.printLog(NameBringsBean.class, e.getMessage(), SystemLogWriter.WARNING);
			System.out.println(e.getMessage());

		} finally {
			// 全てのステートメントをクローズ
			for (Iterator ite = STATEMENT.values().iterator(); ite.hasNext();) {
				PreparedStatement ps = (PreparedStatement) ite.next();
				DbUtils.closeQuietly(ps);
			}

			DbUtils.closeQuietly(con);

			SystemLogWriter.printLog(NameBringsBean.class, "■■■■ 名寄せ処理を終了します。 ■■■■", SystemLogWriter.INFO);

			System.out.println("名寄せ処理終了 " + getFormatedTimeString("yyyy/MM/dd HH:mm:ss:SS"));

			SystemLogWriter.setLogWrite(false);

			// ------------------------------------------------------------
			// 2004.11.15
			// Yoshimoto KAWAI - Totec
			// ログファイルを解放する
			SystemLogWriter.close();
			// ------------------------------------------------------------
		}

		if (success)
			System.exit(0);
		else
			System.exit(1);
	}

	/**
	*
	* タイムスタンプ
	*
	* @param index 書式
	* @return 現在の日付を設定された書式の文字型で返します。
	*/
	public static String getFormatedTimeString(String index) {
		java.util.Date currentTime = java.util.Calendar.getInstance().getTime();
		java.text.DateFormat form = new java.text.SimpleDateFormat(index);
		String timeStr = form.format(currentTime);
		return timeStr;
	}

	/**
	 * 名寄せ処理
	 * @param conn
	 * @return boolean 処理が完全終了したかどうか
	 * @throws Exception
	 */
	private static boolean execute(Connection conn) throws Exception {

		// コミットカウント
		int commitCount = 0;
		// 個表ワークデータの入れ物
		final HashMap workRecord = new HashMap();
		// 個表ワークデータ結果セット
		ResultSet rs = null;
		// セーブポイント
		Savepoint savePoint = null;

		try {
			// 個表ワークデータ
			rs = NameBringsTableAccess.SelWork(conn);

			while (rs.next()) {
				try {
					savePoint = conn.setSavepoint("sp");

					// エラー発生レコードなら次へ
					if (ERROR_ROWID.contains(rs.getString("ROWID"))) {
						continue;
					}

					// 個表ワークレコード
					workRecord.put("ROWID", rs.getString("ROWID"));
					workRecord.put("EXAMYEAR", rs.getString("EXAMYEAR"));
					workRecord.put("EXAMCD", rs.getString("EXAMCD"));
					workRecord.put("ANSWERSHEET_NO", rs.getString("ANSWERSHEET_NO"));
					workRecord.put("HOMESCHOOLCD", rs.getString("HOMESCHOOLCD"));
					workRecord.put("BUNDLECD", rs.getString("BUNDLECD"));
					workRecord.put("GRADE", rs.getString("GRADE"));
					workRecord.put("CLASS", rs.getString("CLASS"));
					workRecord.put("CLASS_NO", rs.getString("CLASS_NO"));
					workRecord.put("ATTENDGRADE", rs.getString("ATTENDGRADE"));
					workRecord.put("NAME_KANA", rs.getString("NAME_KANA"));
					workRecord.put("NAME_KANJI", rs.getString("NAME_KANJI"));
					workRecord.put("SEX", rs.getString("SEX"));
					workRecord.put("BIRTHDAY", NameBringsTableAccess.getBirthDay(rs.getString("BIRTHDAY")));
					workRecord.put("TEL_NO", rs.getString("TEL_NO"));
					workRecord.put("CUSTOMER_NO", rs.getString("CUSTOMER_NO"));
					workRecord.put("STUDENT_NO", rs.getString("STUDENT_NO"));
					workRecord.put("REGION", rs.getString("REGION"));
					workRecord.put("SCHOOLHOUSE", rs.getString("SCHOOLHOUSE"));
					workRecord.put("APPLIDATE", rs.getString("APPLIDATE"));
					workRecord.put("PRIVERCYFLG", rs.getString("PRIVERCYFLG"));
					workRecord.put("SUBRECORD1", rs.getString("SUBRECORD1"));
					workRecord.put("SUBRECORD2", rs.getString("SUBRECORD2"));
					workRecord.put("SUBRECORD3", rs.getString("SUBRECORD3"));
					workRecord.put("SUBRECORD4", rs.getString("SUBRECORD4"));
					workRecord.put("SUBRECORD5", rs.getString("SUBRECORD5"));
					workRecord.put("SUBRECORD6", rs.getString("SUBRECORD6"));
					workRecord.put("SUBRECORD7", rs.getString("SUBRECORD7"));
					workRecord.put("SUBRECORD8", rs.getString("SUBRECORD8"));
					workRecord.put("SUBRECORD9", rs.getString("SUBRECORD9"));
					workRecord.put("SUBRECORD10", rs.getString("SUBRECORD10"));
					workRecord.put("SUBRECORD11", rs.getString("SUBRECORD11"));
					workRecord.put("SUBRECORD12", rs.getString("SUBRECORD12"));
					workRecord.put("SUBRECORD13", rs.getString("SUBRECORD13"));
					workRecord.put("SUBRECORD14", rs.getString("SUBRECORD14"));
					workRecord.put("SUBRECORD15", rs.getString("SUBRECORD15"));
					workRecord.put("SUBRECORD16", rs.getString("SUBRECORD16"));
					workRecord.put("SUBRECORD17", rs.getString("SUBRECORD17"));
					workRecord.put("SUBRECORD18", rs.getString("SUBRECORD18"));
					workRecord.put("SUBRECORD19", rs.getString("SUBRECORD19"));
					workRecord.put("SUBRECORD20", rs.getString("SUBRECORD20"));
					workRecord.put("SUBRECORD21", rs.getString("SUBRECORD21"));
					workRecord.put("SUBRECORD22", rs.getString("SUBRECORD22"));
					workRecord.put("SUBRECORD23", rs.getString("SUBRECORD23"));
					workRecord.put("SUBRECORD24", rs.getString("SUBRECORD24"));
					workRecord.put("SUBRECORD25", rs.getString("SUBRECORD25"));
					workRecord.put("SUBRECORD26", rs.getString("SUBRECORD26"));
					workRecord.put("JAPDESC", rs.getString("JAPDESC"));
					workRecord.put("ENGPT", rs.getString("ENGPT"));
					workRecord.put("QUESTIONRECORD1", rs.getString("QUESTIONRECORD1"));
					workRecord.put("QUESTIONRECORD2", rs.getString("QUESTIONRECORD2"));
					workRecord.put("QUESTIONRECORD3", rs.getString("QUESTIONRECORD3"));
					workRecord.put("QUESTIONRECORD4", rs.getString("QUESTIONRECORD4"));
					workRecord.put("QUESTIONRECORD5", rs.getString("QUESTIONRECORD5"));
					workRecord.put("QUESTIONRECORD6", rs.getString("QUESTIONRECORD6"));
					workRecord.put("QUESTIONRECORD7", rs.getString("QUESTIONRECORD7"));
					workRecord.put("QUESTIONRECORD8", rs.getString("QUESTIONRECORD8"));
					workRecord.put("QUESTIONRECORD9", rs.getString("QUESTIONRECORD9"));
					workRecord.put("QUESTIONRECORD10", rs.getString("QUESTIONRECORD10"));
					workRecord.put("CANDIDATERATING1", rs.getString("CANDIDATERATING1"));
					workRecord.put("CANDIDATERATING2", rs.getString("CANDIDATERATING2"));
					workRecord.put("CANDIDATERATING3", rs.getString("CANDIDATERATING3"));
					workRecord.put("CANDIDATERATING4", rs.getString("CANDIDATERATING4"));
					workRecord.put("CANDIDATERATING5", rs.getString("CANDIDATERATING5"));
					workRecord.put("CANDIDATERATING6", rs.getString("CANDIDATERATING6"));
					workRecord.put("CANDIDATERATING7", rs.getString("CANDIDATERATING7"));
					workRecord.put("CANDIDATERATING8", rs.getString("CANDIDATERATING8"));
					workRecord.put("CANDIDATERATING9", rs.getString("CANDIDATERATING9"));
					workRecord.put("ACADEMIC1", rs.getString("ACADEMIC1"));
					workRecord.put("ACADEMIC2", rs.getString("ACADEMIC2"));
					workRecord.put("ACADEMIC3", rs.getString("ACADEMIC3"));
					workRecord.put("ACADEMIC4", rs.getString("ACADEMIC4"));
					workRecord.put("ACADEMIC5", rs.getString("ACADEMIC5"));
					workRecord.put("ACADEMIC6", rs.getString("ACADEMIC6"));

					//[2013/04/23 morita]受験型,及び文理コード追加 START
					workRecord.put("EXAMTYPE", rs.getString("EXAMTYPE"));
					workRecord.put("BUNRICODE", rs.getString("BUNRICODE"));
					//[2013/04/23 morita]受験型,及び文理コード追加 END

					// ------------------------------------------------------------
					// 2020.04.09 FJ)Tanaka 学校マスタ番号帯拡大対応 MOD STA
					// ------------------------------------------------------------
					// 私塾（一括コード 71xxx 72xxx 75xxx 76xxx 77xxx 78xxx ）なら
					// プライバシー保護フラグは落とす
					String bundleCd = (String) workRecord.get("BUNDLECD");
					if (bundleCd.startsWith("71") || bundleCd.startsWith("72") || bundleCd.startsWith("75") || bundleCd.startsWith("76") || bundleCd.startsWith("77") || bundleCd.startsWith("78")) {
						workRecord.put("PRIVERCYFLG", "0");
					}
					// ------------------------------------------------------------
					// 2020.04.09  FJ)Tanaka 学校マスタ番号帯拡大対応 MOD END
					// ------------------------------------------------------------

					// パラメタNULLを空文字に変換
					nullToBlank(workRecord);

					// 再集計対象チェック
					final String year = (String) workRecord.get("EXAMYEAR");
					final String examCd = (String) workRecord.get("EXAMCD");
					final String answersheet_no = (String) workRecord.get("ANSWERSHEET_NO");
					final String birthDay = (String) workRecord.get("BIRTHDAY");

					//模試情報の検索
					ExamSearchBean esb = new ExamSearchBean(year, examCd);
					esb.setConnection(null, conn);
					esb.execute();

					workRecord.put("YEAR02", year.substring(2, 4));

					// 日付の分割
					if (birthDay.length() == 8) {
						workRecord.put("BIRTHYEAR", birthDay.substring(0, 4));
						workRecord.put("BIRTHMONTH", birthDay.substring(4, 6));
						workRecord.put("BIRTHDATE", birthDay.substring(6, 8));
					} else {
						workRecord.put("BIRTHYEAR", "");
						workRecord.put("BIRTHMONTH", "");
						workRecord.put("BIRTHDATE", "");
					}

					// 個表テーブルの対象レコードを取得
					final HashMap record = NameBringsTableAccess.SelExamStudentInf(conn, year, examCd, answersheet_no);

					// 個表にレコードがあるのでUPDATE処理
					if (record != null) {
						// 個表ワークの学年・クラス
						final int tempGrade = Integer.parseInt((String) workRecord.get("GRADE"));
						final String tempClass = (String) workRecord.get("CLASS");

						// 個表の学年
						final String dualGrade = (String) record.get("IGRADE");

						// 個表の集計学年・集計クラス
						final int cntGrade = Integer.parseInt((String) record.get("CNTGRADE"));
						final String cntClass = (String) record.get("CNTCLASS");

						// 再集計対象登録
						// 学年またはクラスが異なる場合
						// 受験学力測定テストは再集計なし
						if ((cntGrade != tempGrade || !cntClass.equalsIgnoreCase(tempClass)) && !KNUtil.isAbilityExam(esb.getExam())) {

							// 申し込み区分を取得
							final String kubun = (String) record.get("APPLIDATE");

							// 科目別成績
							for (int j = 1; j < 27; j++) {
								String subRecord = (String) workRecord.get("SUBRECORD" + j);
								if (subRecord == null) {
									continue;
								}
								byte[] value = subRecord.getBytes(CHARSET);

								// 科目コード
								final String subCd = FormatChecker.byte2RecordValue(value, 0, 4);
								// NULLなら次へ
								if (subCd == null) {
									continue;
								}

								// 志望大学のみ再集計対象
								if (("2".equals(kubun) || "4".equals(kubun)) && "4".equals(dualGrade) || "5".equals(dualGrade)) {

								} else {
									// 科目再集計対象にINSERT
									NameBringsTableAccess.InsertSubCount(conn, year, examCd, bundleCd, cntGrade, cntClass, subCd);
									NameBringsTableAccess.InsertSubCount(conn, year, examCd, bundleCd, tempGrade, tempClass, subCd);
								}
							}

							// 志望大学別評価人数
							for (int j = 1; j < 10; j++) {
								byte[] value = ((String) workRecord.get("CANDIDATERATING" + j)).getBytes();

								// 大学コード
								final String univCd = FormatChecker.byte2RecordValue(value, 0, 4);
								// NULLなら次へ
								if (univCd == null) {
									continue;
								}

								// 志望順位
								final String candiRank = FormatChecker.byte2RecordValue(value, 10, 11, false);
								// 0なら次へ
								if ("0".equals(candiRank)) {
									continue;
								}

								// 学部コード
								final String facltyCd = FormatChecker.byte2RecordValue(value, 4, 6);
								// 学科コード
								final String deptCd = FormatChecker.byte2RecordValue(value, 6, 10);

								// 志望大学評価別人数再集計対象にINSERT
								NameBringsTableAccess.InsertRatingNumberRecount(conn, year, examCd, bundleCd, cntGrade, cntClass, univCd, facltyCd, deptCd);
								// 志望大学評価別人数再集計対象にINSERT
								NameBringsTableAccess.InsertRatingNumberRecount(conn, year, examCd, bundleCd, tempGrade, tempClass, univCd, facltyCd, deptCd);
							}
						}

						// 成績データを削除
						final String studentId = (String) record.get("S_INDIVIDUALID");
						NameBringsTableAccess.DeleteSubRecord_I(conn, studentId, year, examCd);
						NameBringsTableAccess.DeleteSubRecord_PP(conn, answersheet_no, year, examCd);
						NameBringsTableAccess.DeleteQRecord_I(conn, studentId, year, examCd);
						NameBringsTableAccess.DeleteQRecord_PP(conn, answersheet_no, year, examCd);
						NameBringsTableAccess.DeleteCandiDate(conn, studentId, year, examCd);
						NameBringsTableAccess.DeleteCandiDatePP(conn, answersheet_no, year, examCd);
						NameBringsTableAccess.deleteCefrRecord_I(conn, studentId, year, examCd);
						NameBringsTableAccess.deleteCefrRecord_PP(conn, answersheet_no, year, examCd);
						NameBringsTableAccess.deleteKokugoDesc_I(conn, studentId, year, examCd);
						NameBringsTableAccess.deleteKokugoDesc_PP(conn, answersheet_no, year, examCd);
						NameBringsTableAccess.deleteAcademic_I(conn, studentId, year, examCd);
						NameBringsTableAccess.deleteAcademic_PP(conn, answersheet_no, year, examCd);

						// 個表データ更新
						NameBringsTableAccess.updDualRec(conn, workRecord, (String) record.get("IROWID"));
						// 個表データ新規登録
					} else {
						NameBringsTableAccess.insDualRec(conn, workRecord);
					}

					// 学校向け処理開始
					schoolProccess(workRecord);

					// 連携テーブル更新
					IndivChangeInfoTableAccess.updatePrimary(conn, year, examCd, answersheet_no);

					// 校舎向け処理開始
					// 次期リリース

					// 個表ワークデータ削除
					NameBringsTableAccess.DelWork(conn, (String) workRecord.get("ROWID"));

					// 新規カウントアップ
					if (record == null)
						insTotalCount++;
					// 更新カウントアップ
					else
						updTotalCount++;

					// コミットカウントアップ
					commitCount++;

				} catch (final Exception e) {
					ERROR_ROWID.add(workRecord.get("ROWID"));

					conn.rollback(savePoint);
					if (e != null) {
						SystemLogWriter.printLog(NameBringsBean.class,String.format("%s (KEY:%s,%s,%s,%s)", e.getMessage().trim(), workRecord.get("EXAMYEAR"), workRecord.get("EXAMCD"), workRecord.get("ANSWERSHEET_NO"), workRecord.get("INDIVIDUALID")), SystemLogWriter.ERROR);
					}
				}

				// コミットポイント
				if (commitCount % INTERVAL_COMMIT == 0 && commitCount > 0) {
					conn.commit();
					SystemLogWriter.printLog(NameBringsBean.class, "コミットポイント" + commitCount + "件", SystemLogWriter.INFO);
					// セレクトインターバルに達したら抜ける
					if (commitCount == INTERVAL_SELECT)
						return false;
				}
			}

			conn.commit();

		} catch (final Exception e) {
			conn.rollback();
			throw e;
		} finally {
			// 個表結果セットクローズ
			DbUtils.closeQuietly(rs);
		}

		// ここまで来れば完全終了
		return true;
	}

	/**
	 * パラメータNULLを空文字に変換
	 *
	 * 2004.11.12
	 * Yoshimoto KAWAI -Totec
	 * クラス番号の初期値を99999から空に変更
	 *
	 * @param inputData
	 * @return
	 */
	private static void nullToBlank(final Map inputData) {
		if (inputData.get("YEAR02") == null)
			inputData.put("YEAR02", "");
		if (inputData.get("SCHOOLCD") == null)
			inputData.put("SCHOOLCD", "");
		if (inputData.get("HOMESCHOOLCD") == null)
			inputData.put("HOMESCHOOLCD", "");
		if (inputData.get("EXAMYEAR") == null)
			inputData.put("EXAMYEAR", "");
		if (inputData.get("GRADE") == null)
			inputData.put("GRADE", "");
		if (inputData.get("CLASS") == null)
			inputData.put("CLASS", "");
		if (inputData.get("CLASS_NO") == null)
			inputData.put("CLASS_NO", "");
		if (inputData.get("SEX") == null)
			inputData.put("SEX", "");
		if (inputData.get("NAME_KANA") == null)
			inputData.put("NAME_KANA", "");
		if (inputData.get("NAME_KANJI") == null)
			inputData.put("NAME_KANJI", "");
		if (inputData.get("BIRTHYEAR") == null)
			inputData.put("BIRTHYEAR", "");
		if (inputData.get("BIRTHMONTH") == null)
			inputData.put("BIRTHMONTH", "");
		if (inputData.get("BIRTHDATE") == null)
			inputData.put("BIRTHDATE", "");
		if (inputData.get("TEL_NO") == null)
			inputData.put("TEL_NO", "");
		if (inputData.get("ANSWERSHEETNO") == null)
			inputData.put("ANSWERSHEETNO", "");
	}

	/**
	 * 学校向け処理
	 *
	 * @param workRecord
	 * @throws Exception
	 */
	private static void schoolProccess(HashMap workRecord) throws Exception {

		// 在卒校コードのチェック
		if (workRecord.get("HOMESCHOOLCD") == null || "".equals((String) workRecord.get("HOMESCHOOLCD"))) {
			return;
		}

		// 学校用名寄せルート
		if ("0".equals(workRecord.get("PRIVERCYFLG"))) {
			NameBringsForSchool.getInstance().doCombi(workRecord);
			// プライバシー保護ルート
		} else {
			NameBringsForSchool.getInstance().InsWorkToMasterPP(workRecord);
		}
	}

}

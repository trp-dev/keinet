package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.beans.maintenance.module.StudentDataValidator;
import jp.co.fj.keinavi.data.maintenance.KNStudentData;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.message.MessageLoader;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 生徒情報更新Bean
 * 
 * 2005.10.25	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class KNStudentUpdateBean extends DefaultBean {

	// 生徒情報データ検査クラスのインスタンス
	private final StudentDataValidator validator =
			StudentDataValidator.getInstance();
	// 再集計対象登録Bean
	private final RecountRegisterBean recount = new RecountRegisterBean();
	
	private final String schoolCd;
	private final KNStudentData student;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 学校コード
	 * @param pStudent 正規化されている生徒情報データ
	 */
	public KNStudentUpdateBean(final String pSchoolCd,
			final KNStudentData pStudent) {
		
		if (pSchoolCd == null) {
			throw new IllegalArgumentException(
					"学校コードがNULLです。");
		}
		
		if (pStudent == null) {
			throw new IllegalArgumentException(
					"生徒情報データがNULLです。");
		}
		
		this.schoolCd = pSchoolCd;
		this.student = pStudent;
	}
	
	/**
	 * @see jp.co.fj.keispro.beans.common.csv.CSVLineRegisterBean#execute()
	 */
	public void execute() throws Exception {

		// データチェック
		validator.validate(student);
		
		// 学籍基本情報の更新
		updateBasicinfo();

		final String currentYear = KNUtil.getCurrentYear();
		final String lastYear = String.valueOf(
				Integer.parseInt(currentYear) - 1);
		final String beforeLastYear = String.valueOf(
				Integer.parseInt(currentYear) - 2);
		final StringBuffer info = new StringBuffer();
		
		// 学籍履歴情報（前々年度）の更新
		if (!updateHistoryinfo(
				student.getIndividualId(),
				beforeLastYear,
				student.getBeforeLastGrade(),
				student.getBeforeLastClassName(),
				student.getBeforeLastClassNo(),
				student.getOBeforeLastGrade(),
				student.getOBeforeLastClassName())) {
			
			info.append("\\n");
			info.append(beforeLastYear + "年度");
			info.append(student.getBeforeLastGrade() + "年");
			info.append(student.getBeforeLastClassName() + "クラス");
			info.append(student.getBeforeLastClassNo() + "番");
		}
		
		// 学籍履歴情報（前年度）の更新
		if (!updateHistoryinfo(
				student.getIndividualId(),
				lastYear,
				student.getLastGrade(),
				student.getLastClassName(),
				student.getLastClassNo(),
				student.getOLastGrade(),
				student.getOLastClassName())) {

			info.append("\\n");
			info.append(lastYear + "年度");
			info.append(student.getLastGrade() + "年");
			info.append(student.getLastClassName() + "クラス");
			info.append(student.getLastClassNo() + "番");
		}
		
		// 学籍履歴情報（今年度）の更新
		if (!updateHistoryinfo(
				student.getIndividualId(),
				currentYear,
				student.getCurrentGrade(),
				student.getCurrentClassName(),
				student.getCurrentClassNo(),
				student.getOCurrentGrade(),
				student.getOCurrentClassName())) {

			info.append("\\n");
			info.append(currentYear + "年度");
			info.append(student.getCurrentGrade() + "年");
			info.append(student.getCurrentClassName() + "クラス");
			info.append(student.getCurrentClassNo() + "番");
		}
		
		// 重複エラー
		if (info.length() > 0) {
			throw new KNBeanException(
					MessageLoader.getInstance().getMessage("x010a") + info.toString());
		}
	}

	private void updateBasicinfo() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(createBasicinfoQuery());
			ps.setString(1, student.getNameKana());
			ps.setString(2, student.getNameKanji());
			ps.setString(3, student.getSex());
			ps.setString(4,
					KNStudentInsertBean.BIRTHDAY_FORMAT.format(
							student.getBirthday()));
			ps.setString(5, student.getTelNo());
			ps.setString(6, student.getIndividualId());
			ps.setString(7, schoolCd);
			
			if (ps.executeUpdate() == 0) {
				throw new SQLException(
						"学籍基本情報の更新に失敗しました。");
			}
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	protected String createBasicinfoQuery() throws SQLException {
		
		return "UPDATE basicinfo " 
				+ "SET name_kana = ?, name_kanji = ?, "
				+ "sex = ?, birthday = ?, tel_no = ? "
				+ "WHERE individualid = ? AND schoolcd = ?";
	}
	
	private boolean updateHistoryinfo(final String individualId,
			final String year,	final int grade,
			final String className, final String classNo,
			final int oGrade, final String oClassName) throws Exception {

		// 学年が負である（＝履歴が存在しない）なら実行しない
		if (grade < 0) {
			return true;
		}
		
		// 再集計対象の登録
		registRecount(individualId, year, grade, className, oGrade, oClassName);
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(createHistoryinfoQuery());
			ps.setInt(1, grade);
			ps.setString(2, className);
			ps.setString(3, classNo);
			ps.setString(4, individualId);
			ps.setString(5, year);
			ps.setString(6, year);
			ps.setString(7, schoolCd);
			ps.setString(8, individualId);
			ps.setInt(9, grade);
			ps.setString(10, className);
			ps.setString(11, classNo);
			
			return ps.executeUpdate() == 1;
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	protected String createHistoryinfoQuery() throws SQLException {
		
		return QueryLoader.getInstance().load("m12").toString();
	}
	
	protected void registRecount(final String individualId, final String year,
			final int grade, final String className, final int oGrade,
			final String oClassName) throws Exception {
		
		recount.setConnection(null, conn);
		recount.setYear(year);
		recount.setIndividualId(individualId);
		recount.setAfterGrade(grade);
		recount.setAfterClassName(className);
		recount.setBeforeGrade(oGrade);
		recount.setBeforeClassName(oClassName);
		recount.execute();
	}

	/**
	 * @return schoolCd を戻します。
	 */
	protected String getSchoolCd() {
		return schoolCd;
	}

	/**
	 * @return student を戻します。
	 */
	protected KNStudentData getStudent() {
		return student;
	}

}

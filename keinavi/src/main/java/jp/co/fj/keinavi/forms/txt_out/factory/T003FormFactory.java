/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.txt_out.factory;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import jp.co.fj.keinavi.forms.txt_out.TMaxForm;

import com.fjh.forms.ActionForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class T003FormFactory extends AbstractTFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		// アクションフォーム
		TMaxForm form = super.createTMaxForm(request);
		// アイテムマップ
		Map item = getItemMap(request);
		
		CommonFormFactory commForm = new CommonFormFactory(form, item);
		commForm.setOutTarget();
		commForm.setTxtType();
		return commForm.getForm();
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
		TMaxForm form = (TMaxForm)request.getAttribute("form");
		// アイテムマップ
		Map item = getItemMap(request);

		CommonItemFactory commItem = new CommonItemFactory(form, item);
		commItem.setOutTarget();
		commItem.setTxtType();

	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap("050202");
	}

}
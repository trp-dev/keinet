package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
import java.util.ArrayList;
/**
 * 個人成績分析−受験用資料−受験大学スケジュール表 受験大学スケジュール用データリスト
 * 作成日: 2004/07/21
 * @author	A.Iwata
 */
public class I15ScheduleListBean {
	//模試別偏差値データリスト
	private ArrayList i15HensaList = new ArrayList();
	//受験大学別スケジュール
	private ArrayList i15CalendarList = new ArrayList();

	/*----------*/
	/* Get      */
	/*----------*/
	public ArrayList getI15HensaList() {
		return this.i15HensaList;
	}
	public ArrayList getI15CalendarList() {
		return this.i15CalendarList;
	}

		
	/*----------*/
	/* Set      */
	/*----------*/
	public void setI15HensaList(ArrayList i15HensaList) {
		this.i15HensaList = i15HensaList;
	}
	public void setI15CalendarList(ArrayList i15CalendarList) {
		this.i15CalendarList = i15CalendarList;
	}

}

/*
 * 作成日: 2004/07/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.login;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 部門セッション
 * @author kawai
 *
 */
public class SectorSession implements Serializable {

	// セッションキー
	public static final String SESSION_KEY = "SectorSession";

	// 部門データのリスト
	private List sectorList = new ArrayList();

	/**
	 * @return
	 */
	public List getSectorList() {
		return sectorList;
	}

}

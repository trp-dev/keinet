package jp.co.fj.keinavi.beans.individual.judgement;

import jp.co.fj.kawaijuku.judgement.beans.detail.UnivDetail;

/**
 * 
 * 公表科目表示用データクラス
 * 
 * <2010年度改修>
 * 2009.12.01   Tomohisa YAMADA - Totec
 *              新規作成
 *              
 *
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 *
 */
public class JudgeOpenBean {

	/**
	 * 判定詳細データ
	 */
	private UnivDetail detail;
	
    /**
     * センター英語配点（算出）
     */
    private String allot1Eng;
    
    /**
     * センター数学配点（算出）
     */
    private String allot1Mat;
    
    /**
     * センター国語配点（算出）
     */
    private String allot1Jap;
    
    /**
     * センター理科配点（算出）
     */
    private String allot1Sci;
    
    /**
     * センター社会配点（算出）
     */
    private String allot1Soc;
    
    /**
     * 二次英語配点（算出）
     */
    private String allot2Eng;
    
    /**
     * 二次数学配点（算出）
     */
    private String allot2Mat;
    
    /**
     * 二次国語配点（算出）
     */
    private String allot2Jap;
    
    /**
     * 二次理科配点（算出）
     */
    private String allot2Sci;
    
    /**
     * 二次社会配点（算出）
     */
    private String allot2Soc;
    
    /**
     * 二次その他配点（算出）
     */
    private String allot2Etc;
    
    /**
     * 対象大学の総枝番数（公表用）
     * ※帳票で使用されている
     */
    private int totalBranch = 0;
	
    /**
     * コンストラクタ
     * @param detail 判定詳細データ
     * @param totalBranchNum 対象大学の総枝番数
     */
	public JudgeOpenBean(UnivDetail detail, int totalBranchNum) {
		
		this.detail = detail;
		
		if (detail == null) {
			throw new InternalError("指定枝番の公表科目が存在しません。");
		}
		
		totalBranch = totalBranchNum;	
	}
	
	/**
	 * 実行部
	 * @throws Exception
	 */
	public void execute() throws Exception {
		
		//教科別配点を設定
		setupCourseAllot();
	}

	/**
	 * 教科別配点を算出・設定
	 * @throws Exception
	 */
	protected void setupCourseAllot() throws Exception {
		
        //配点情報の設定
		if (detail.getCenter().isImposedDisp()) {
	        String[] allots1 = detail.getCenter().getCourseAllotment();
	        
	        this.allot1Eng = allots1[0];
	        this.allot1Mat = allots1[1];
	        this.allot1Jap = allots1[2];
	        this.allot1Sci = allots1[3];
	        this.allot1Soc = allots1[4];
		}
        
        //配点情報の設定
		if (detail.getSecond().isImposedDisp()) {
	        String[] allots2 = detail.getSecond().getCourseAllotment();
	        
	        this.allot2Eng = allots2[0];
	        this.allot2Mat = allots2[1];
	        this.allot2Jap = allots2[2];
	        this.allot2Sci = allots2[3];
	        this.allot2Soc = allots2[4];
	        this.allot2Etc = allots2[5];
		}
	}
	
	/**
	 * センタ用科目文字列を取得
	 * @return
	 */
	public String getC_subString() {
		return detail.getCenter().getSubString();
	}
	
	/**
	 * 二次用科目文字列を取得
	 * @return
	 */
	public String getS_subString() {
		return detail.getSecond().getSubString();
	}
	
	/**
	 * センタ用ガイド文字列を取得
	 * @return
	 */
	public String getC_guideSubString() {
		
		//ガイド文字列があれば返す
		if (detail.getCenter().getGuideSubString() != null) {
			return detail.getCenter().getGuideSubString();
		}
		
		return getC_subString();
	}
	
	/**
	 * 二次用ガイド文字列を取得
	 * @return
	 */
	public String getS_guideSubString() {
		
		//ガイド文字列があれば返す
		if (detail.getSecond().getGuideSubString() != null) {
			return detail.getSecond().getGuideSubString();
		}
		
		return getS_subString();
	}
	
	/**
	 * センタ用満点値を取得
	 * @return
	 */
	public String getCFullPoint() {
        if(detail.getCenter().getFullPoint() != 9999){
            return String.valueOf(detail.getCenter().getFullPoint());
        }
        
        return " - ";    
	}
	
	/**
	 * 二次用満点値を取得
	 * @return
	 */
	public String getSFullPoint() {
        if(detail.getSecond().getFullPoint() != 9999){
            return String.valueOf(detail.getSecond().getFullPoint());
        }
        
        return " - ";
	}
	
	/**
	 * センタ用配点比
	 * @return
	 */
	public String getCAllotPntRateAll() {
        if (detail.getCenter().getAllotPntRateAll() != 9999 
        		&& detail.getCenter().getAllotPntRateAll() > 0) {
            return String.valueOf(detail.getCenter().getAllotPntRateAll());
        }
        
        return "-";
	}
	
	/**
	 * 二次用配点比
	 * @return
	 */
	public String getSAllotPntRateAll() {
        if (detail.getSecond().getAllotPntRateAll() != 9999 
        		&& detail.getSecond().getAllotPntRateAll() > 0) {
            return String.valueOf(detail.getSecond().getAllotPntRateAll());
        }
        
        return "-";
	}
	
	/**
	 * 枝番数フラグ
	 * ※帳票で使用している
	 * @return
	 */
	public int getBranchNumFlag() {
		
        if(totalBranch > 1){
            return 1;
        }

        return 0;
	}

	/**
	 * センタ用枝番名を取得
	 * @return
	 */
	public String getC_branchName() {
		return detail.getCenter().getBranchName();
	}
	
	/**
	 * 二次用枝番名を取得
	 * @return
	 */
	public String getS_branchName() {
		return detail.getSecond().getBranchName();
	}
	
	/**
	 * センタを課しているかどうか
	 * @return
	 */
	public boolean isCExist() {
		return detail.getCenter().isImposedDisp();
	}
	
	/**
	 * 二次を課しているかどうか
	 * @return
	 */
	public boolean isSExist() {
		return detail.getSecond().isImposedDisp();
	}
	
	/**
	 * 枝番を取得
	 * @return
	 */
	public String getBranchCode() {
		return this.detail.getBranchCode();
	}
	
	/**
	 * その他用文字列を取得
	 * @return
	 */
	public String getOtherAllotsStr() {
		return detail.getSecond().getOtherString();
	}

	/**
	 * センタ英語配点を返す
	 * @return
	 */
	public String getAllot1Eng() {
		return allot1Eng;
	}

	/**
	 * センタ数学配点を返す
	 * @return
	 */
	public String getAllot1Mat() {
		return allot1Mat;
	}

	/**
	 * センタ国語配点を返す
	 * @return
	 */
	public String getAllot1Jap() {
		return allot1Jap;
	}

	/**
	 * センタ理科配点を返す
	 * @return
	 */
	public String getAllot1Sci() {
		return allot1Sci;
	}
	
	/**
	 * センタ社会配点を返す
	 * @return
	 */
	public String getAllot1Soc() {
		return allot1Soc;
	}

	/**
	 * 二次英語配点を返す
	 * @return
	 */
	public String getAllot2Eng() {
		return allot2Eng;
	}

	/**
	 * 二次数学配点を返す
	 * @return
	 */
	public String getAllot2Mat() {
		return allot2Mat;
	}

	/**
	 * 二次国語配点を返す
	 * @return
	 */
	public String getAllot2Jap() {
		return allot2Jap;
	}

	/**
	 * 二次理科配点を返す
	 * @return
	 */
	public String getAllot2Sci() {
		return allot2Sci;
	}

	/**
	 * 二次社会配点を返す
	 * @return
	 */
	public String getAllot2Soc() {
		return allot2Soc;
	}
	
	/**
	 * 二次その他配点を返す
	 * @return
	 */
	public String getOtherAllot() {
		return allot2Etc;
	}

}

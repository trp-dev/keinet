package jp.co.fj.keinavi.beans.sheet.business;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.sheet.SheetSchoolData;
import jp.co.fj.keinavi.excel.business.B44;
import jp.co.fj.keinavi.excel.data.business.B44GakkoListBean;
import jp.co.fj.keinavi.excel.data.business.B44HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.business.B44Item;
import jp.co.fj.keinavi.excel.data.business.B44ListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 高校成績分析 - 高校間比較（志望大学評価別人数）
 *
 * 2004.09.08	[新規作成]
 *
 * 2005.04.06	Yoshimoto KAWAI - TOTEC
 * 				出力種別フラグ対応
 *
 * 2005.04.11	Yoshimoto KAWAI - TOTEC
 * 				日程・評価区分対応
 *
 * <大規模改修>
 * 2016.01.21 	Hiroyuki Nishiyama - QuiQsoft
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class B44SheetBean extends AbstractSheetBean {

	// データクラス
	private final B44Item data = new B44Item();

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// 比較対象高校
		final List compSchoolList = getCompSchoolList();
		// 大学集計区分
		final String univCountingDiv = getUnivCountngDiv();
		// 現役高卒区分
		final String[] studentDiv = getStudentDiv();
		// 評価区分
		final String[] ratingDiv = (String[]) item.get(IProfileItem.UNIV_RATING);

		// データを詰める
		data.setStrMshCd(exam.getExamCD()); // 模試コード
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntDaiTotalFlg(getDaiTotalFlg()); // 大学集計区分
		data.setIntSecuFlg(getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(getOutputType()); // 出力種別フラグ

		// 年度の配列（過年度の表示フラグにより異なる）
		final String[] years;
		switch (getPrevDisp()) {
			case 1: // 今年度のみ
				years = new String[] { exam.getExamYear() };
				break;
			case 2: // 前年度まで
				years = new String[] {
					exam.getExamYear(),
					String.valueOf(Integer.parseInt(exam.getExamYear()) - 1)
				};
				break;
			case 3: // 前々年度まで
				years = new String[] {
					exam.getExamYear(),
					String.valueOf(Integer.parseInt(exam.getExamYear()) - 1),
					String.valueOf(Integer.parseInt(exam.getExamYear()) - 2)
				};
				break;
			default:
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
			    //throw new Exception("過年度表示フラグが不正です。");
			    throw new IllegalStateException("過年度表示フラグが不正です。");
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
		}

		// ワークテーブルのセットアップ
		insertIntoExamCdTrans();
		insertIntoCountUniv();

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		try {
			// データリスト
			{
				// 比較対象高校に全国が含まれるかどうか
				boolean total = compSchoolList.contains(new SheetSchoolData("99999"));
				// 日程を評価するか
				boolean schedule = item.get(IProfileItem.UNIV_SCHEDULE) != null;

				// SQLを決定する
				final Query query;
				switch (getDaiTotalFlg()) {
					// 大学（日程あり）
					case 1:
						if (total) {
							query = QueryLoader.getInstance().load("b44_1a");

							// 日程の絞込み
							if (schedule) {
								query.replaceAll(
									"/* SCHEDULE CONDITION_A */",
									"AND ra.agendacd IN ('D', 'N', '1', ' ')"
								);
							}
						} else {
							query = QueryLoader.getInstance().load("b44_1b");

							// 日程の絞込み
							if (schedule) {
								query.replaceAll(
									"/* SCHEDULE CONDITION_P */",
									"AND rp.agendacd IN ('D', 'N', '1', ' ')"
								);
								query.replaceAll(
									"/* SCHEDULE CONDITION_S */",
									"AND rs.agendacd IN ('D', 'N', '1', ' ')"
								);
							}
						}
						break;

					// 大学（日程なし）
					case 2:
						query = total
							? QueryLoader.getInstance().load("b44_2a")
							: QueryLoader.getInstance().load("b44_2b");
						break;

					// 学部（日程あり）
					case 3:
						if (total) {
							query = QueryLoader.getInstance().load("b44_3a");

							// 日程の絞込み
							if (schedule) {
								query.replaceAll(
									"/* SCHEDULE CONDITION_A */",
									"AND ra.agendacd IN ('D', 'N', '1', ' ')"
								);
							}
						} else {
							query = QueryLoader.getInstance().load("b44_3b");

							// 日程の絞込み
							if (schedule) {
								query.replaceAll(
									"/* SCHEDULE CONDITION_P */",
									"AND rp.agendacd IN ('D', 'N', '1', ' ')"
								);
								query.replaceAll(
									"/* SCHEDULE CONDITION_S */",
									"AND rs.agendacd IN ('D', 'N', '1', ' ')"
								);
							}
						}
						break;

					// 学部（日程なし）
					case 4:
						query = total
							? QueryLoader.getInstance().load("b44_4a")
							: QueryLoader.getInstance().load("b44_4b");
						break;

					// 学科
					case 5:
						query = total
							? QueryLoader.getInstance().load("b44_5a")
							: QueryLoader.getInstance().load("b44_5b");

						// 日程の絞込み
						if (schedule && "2".equals(exam.getMasterDiv())) {
							query.replaceAll(
								"/* SCHEDULE CONDITION */",
								"AND u.unigdiv IN ('D', 'N', '1', ' ')"
							);
						}

						break;

					// エラー
					default:
						throw new IllegalArgumentException("大学集計区分の値が不正です。");
				}

				rewriteUnivQuery(query); // 模試によって参照マスタが変わるので書き換え

				// 2016/01/21 QQ)Nishiyama 大規模改修 ADD START
				// ORDER BY句の作成
				createOrderBy(query);
				// 2016/01/21 QQ)Nishiyama 大規模改修 ADD END

				// 比較対象高校に全国が含まれる場合
				if (total) {
					query.setStringArray(1, studentDiv); // 現役高卒区分
					query.setStringArray(2, ratingDiv); // 評価区分

					ps1 = conn.prepareStatement(query.toString());
					ps1.setString(1, exam.getExamYear()); // 模試年度
					ps1.setString(2, exam.getExamCD()); // 模試コード
					ps1.setString(3, univCountingDiv); // 大学集計区分
					ps1.setString(4, exam.getExamDiv()); // 模試区分

				// そうでない場合は県と高校の志望大学を抽出する
				} else {
					// 県コードの入れ物
					List p = new LinkedList();
					p.add("00");

					// 学校コードの入れ物
					List s = new LinkedList();
					s.add("00000");

					Iterator ite = compSchoolList.iterator();
					while (ite.hasNext()) {
						SheetSchoolData school = (SheetSchoolData) ite.next();

						switch (school.getSchoolTypeCode()) {
							// 全国
							case 1: break;
							// 県
							case 2: p.add(school.getPrefCD()); break;
							// 高校
							case 3: s.add(school.getSchoolCD()); break;
						}
					}

					query.setStringArray(1, studentDiv); // 現役高卒区分
					query.setStringArray(2, (String[]) p.toArray(new String[0])); // 県コード
					query.setStringArray(3, ratingDiv); // 評価区分
					query.setStringArray(4, studentDiv); // 現役高卒区分
					query.setStringArray(5, (String[]) s.toArray(new String[0])); // 学校コード
					query.setStringArray(6, ratingDiv); // 評価区分

					ps1 = conn.prepareStatement(query.toString());
					ps1.setString(1, exam.getExamYear()); // 模試年度
					ps1.setString(2, exam.getExamCD()); // 模試コード
					ps1.setString(3, univCountingDiv); // 大学集計区分
					ps1.setString(4, exam.getExamDiv()); // 模試区分
					ps1.setString(5, exam.getExamYear()); // 模試年度
					ps1.setString(6, exam.getExamCD()); // 模試コード
					ps1.setString(7, univCountingDiv); // 大学集計区分
					ps1.setString(8, exam.getExamDiv()); // 模試区分
				}
			}

			// 評価別人数データリスト（全国）
			{
				final Query query = QueryLoader.getInstance().load("b44_6");
				query.setStringArray(1, years); // 模試年度

				ps2 = conn.prepareStatement(query.toString());
				ps2.setString(2, univCountingDiv); // 大学集計区分
				ps2.setString(8, exam.getExamCD()); // 模試コード
			}

			// 評価別人数データリスト（県）
			{
				final Query query = QueryLoader.getInstance().load("b44_7");
				query.setStringArray(1, years); // 模試年度

				ps3 = conn.prepareStatement(query.toString());
				ps3.setString(3, univCountingDiv); // 大学集計区分
				ps3.setString(9, exam.getExamCD()); // 模試コード
			}

			// 評価別人数データリスト（高校）
			{
				final Query query = QueryLoader.getInstance().load("b44_8");
				outDate2InDate(query); // データ開放日書き換え
				query.setStringArray(1, years); // 模試年度

				ps4 = conn.prepareStatement(query.toString());
				ps4.setString(3, univCountingDiv); // 大学集計区分
				ps4.setString(9, exam.getExamCD()); // 模試コード
			}

			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				B44ListBean bean = new B44ListBean();
				bean.setStrGenKouKbn(rs1.getString(1));
				bean.setStrDaigakuCd(rs1.getString(2));
				bean.setStrDaigakuMei(rs1.getString(3));
				bean.setStrGakubuCd(rs1.getString(4));
				bean.setStrGakubuMei(rs1.getString(5));
				bean.setStrGakkaCd(rs1.getString(6));
				bean.setStrGakkaMei(rs1.getString(7));
				bean.setStrNtiCd(rs1.getString(8));
				bean.setStrNtiMei(rs1.getString(9));
				bean.setStrHyoukaKbn(rs1.getString(10));
				data.getB44List().add(bean);

				// 評価区分
				ps2.setString(1, bean.getStrHyoukaKbn());
				ps3.setString(2, bean.getStrHyoukaKbn());
				ps4.setString(2, bean.getStrHyoukaKbn());
				// 大学コード
				ps2.setString(3, bean.getStrDaigakuCd());
				ps3.setString(4, bean.getStrDaigakuCd());
				ps4.setString(4, bean.getStrDaigakuCd());
				// 学部コード
				ps2.setString(4, bean.getStrGakubuCd());
				ps3.setString(5, bean.getStrGakubuCd());
				ps4.setString(5, bean.getStrGakubuCd());
				// 学科コード
				ps2.setString(5, bean.getStrGakkaCd());
				ps3.setString(6, bean.getStrGakkaCd());
				ps4.setString(6, bean.getStrGakkaCd());
				// 日程コード
				ps2.setString(6, bean.getStrNtiCd());
				ps3.setString(7, bean.getStrNtiCd());
				ps4.setString(7, bean.getStrNtiCd());
				// 現役高卒区分
				ps2.setString(7, bean.getStrGenKouKbn());
				ps3.setString(8, bean.getStrGenKouKbn());
				ps4.setString(8, bean.getStrGenKouKbn());

				// 学校分繰り返す
				Iterator ite = compSchoolList.iterator();
				while (ite.hasNext()) {
					SheetSchoolData school = (SheetSchoolData) ite.next();

					B44GakkoListBean g = new B44GakkoListBean();
					bean.getB44GakkoList().add(g);

					switch (school.getSchoolTypeCode()) {
						// 全国
						case 1:
							rs2 = ps2.executeQuery();
							break;

						// 都道府県
						case 2:
							ps3.setString(1, school.getPrefCD()); // 県コード
							rs2 = ps3.executeQuery();
							break;

						// 高校
						case 3:
							ps4.setString(1, school.getSchoolCD()); // 一括コード
							rs2 = ps4.executeQuery();
							break;
					}

					String before = null; // ひとつ前の模試年度
					while (rs2.next()) {
						g.setStrGakkomei(rs2.getString(1));
						// ひとつ前と年度が異なれば入れる
						if (!rs2.getString(2).equals(before)) {
							B44HyoukaNinzuListBean h = new B44HyoukaNinzuListBean();
							h.setStrNendo(rs2.getString(2));
							h.setIntSouShibo(rs2.getInt(3));
							h.setIntDai1Shibo(rs2.getInt(4));
							h.setIntHyoukaA(rs2.getInt(5));
							h.setIntHyoukaB(rs2.getInt(6));
							h.setIntHyoukaC(rs2.getInt(7));
							h.setIntHyoukaD(rs2.getInt(8));
							h.setIntHyoukaE(rs2.getInt(9));

							// 2019/11/26 QQ)nagai 英語認定試験延期対応 DEL START
							//// 2019/09/30 QQ) 共通テスト対応 ADD START
							//h.setIntHyoukaA_Hukumu(rs2.getInt(10));
							//h.setIntHyoukaB_Hukumu(rs2.getInt(11));
							//h.setIntHyoukaC_Hukumu(rs2.getInt(12));
							//h.setIntHyoukaD_Hukumu(rs2.getInt(13));
							//h.setIntHyoukaE_Hukumu(rs2.getInt(14));
							//// 2019/09/30 QQ) 共通テスト対応 ADD END
							// 2019/11/26 QQ)nagai 英語認定試験延期対応 DEL END

							g.getB44HyoukaNinzuList().add(h);
						}
						before = rs2.getString(2);
					}
					rs2.close();
				}
			}
		} finally {
			DbUtils.closeQuietly(null, ps1, rs1);
			DbUtils.closeQuietly(null, ps2, rs2);
			DbUtils.closeQuietly(ps3);
			DbUtils.closeQuietly(ps4);
		}
	}

	// 過年度の表示フラグを取得する
	private int getPrevDisp() {

		int flag = getIntFlag(IProfileItem.PREV_DISP);

		// ２００８年度高２記述模試
		//「対象年度のみ」
		if ("2008".equals(exam.getExamYear()) && "65".equals(exam.getExamCD())) {
			return 1;
		}

		// ２００９年度高２記述模試
		//「対象年度の前々年度まで」となっていたら「対象年度の前年度まで」とする
		if ("2009".equals(exam.getExamYear()) && "65".equals(exam.getExamCD())) {
			if (flag == 3) {
				return 2;
			}
		}

		return flag;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("B44_01");
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_OTHER_UNIV;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new B44().b44(data, outfileList, getAction(), sessionKey, sheetLog);
	}

}

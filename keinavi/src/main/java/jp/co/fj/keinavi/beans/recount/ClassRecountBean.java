package jp.co.fj.keinavi.beans.recount;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import jp.co.fj.keinavi.beans.recount.data.RecountTask;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * クラス再集計Bean
 * 
 * 2006.02.13	[新規作成]
 * 
 * 2007.08.07	校内成績処理システムより移植
 * 
 * 2009.10.07	S.Hase		「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 * 　　　　                     「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 *				「HS12_UNIV：高１・２大学マスタ(判定＆志望校名称)」テーブルを
 * 　　　　                     「H12_UNIVMASTER：高１２用大学マスタ」テーブルに置き換え
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class ClassRecountBean implements IRecountBean {

	// ログインスタンス
	private static final Log LOG = LogFactory.getLog(ClassRecountBean.class);
	
	// DBコネクション
	private Connection con;
	
	// 再集計タスク
	private RecountTask task;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		if (LOG.isDebugEnabled()) {
			LOG.debug("タスク開始" + task);
		}
		
		// 受験者総数（クラス）削除
		delete("recount08");
		// 科目別成績（クラス）の削除
		delete("recount09");
		// 科目分布別成績（クラス）の削除
		delete("recount10");
		// 設問別成績（クラス）の削除
		delete("recount11");
		// 志望大学評価別人数（クラス）の削除
		delete("recount12");
		
		// 受験者総数（クラス）の再集計
		recountExamTakeNumC();

		// 科目別成績（クラス）の再集計
		// 科目分布別成績（クラス）の再集計
		// 新テスト
		if (task.isNewExam()) {
			recountNSubRecordC();
			recountSubDistRecordC("recount16n");
		// それ以外
		} else {
			recountSubRecordC(getMinStudentDiv());
			recountSubDistRecordC("recount16");
		}

		// 設問別成績（クラス）の再集計
		recountQRecordC();
		// 志望大学評価別人数（クラス）の再集計
		recountRatingnumberC();
		
		// 科目再集計対象の削除
		deleteRecountTable("recount01");
		// 志望大学評価別人数再集計対象の削除
		deleteRecountTable("recount02");
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("タスク終了" + task);
		}
	}

	// 集計レコードの削除
	private void delete(final String key) throws SQLException {
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("集計レコードの削除:" + key + task);
		}
		
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(
					QueryLoader.getInstance().getQuery(key));
			ps.setString(1, task.getExamYear());
			ps.setString(2, task.getExamCd());
			ps.setString(3, task.getBundleCd());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	// 再集計対象における最小の生徒区分を取得する
	private String getMinStudentDiv() throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement(
					QueryLoader.getInstance().getQuery("recount13"));
			ps.setString(1, task.getExamYear());
			ps.setString(2, task.getExamCd());
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString(1);
			} else {
				return null;
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	// 受験者総数（クラス）の再集計
	private void recountExamTakeNumC() throws SQLException {
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("受験者総数（クラス）の再集計" + task);
		}
		
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(
					QueryLoader.getInstance().getQuery("recount14"));
			ps.setString(1, task.getExamYear());
			ps.setString(2, task.getExamCd());
			ps.setString(3, task.getBundleCd());
			ps.setString(4, task.getExamYear());
			ps.setString(5, task.getExamCd());
			ps.setString(6, task.getBundleCd());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}


	// 科目別成績（クラス）の再集計
	private void recountSubRecordC(
			final String studentDiv) throws SQLException {
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("科目別成績（クラス）の再集計" + task);
		}
		
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(
					QueryLoader.getInstance().getQuery("recount15"));
			ps.setString(1, task.getExamYear());
			ps.setString(2, task.getExamCd());
			ps.setString(3, task.getBundleCd());
			ps.setString(4, task.getExamYear());
			ps.setString(5, task.getExamCd());
			ps.setString(6, task.getBundleCd());
			ps.setString(7, studentDiv);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	// 科目別成績（クラス）の再集計 ※新テスト
	private void recountNSubRecordC() throws SQLException {
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("科目別成績（クラス）の再集計（新テスト）" + task);
		}
		
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(
					QueryLoader.getInstance().getQuery("recount15n"));
			ps.setString(1, task.getExamYear());
			ps.setString(2, task.getExamCd());
			ps.setString(3, task.getBundleCd());
			ps.setString(4, task.getExamYear());
			ps.setString(5, task.getExamCd());
			ps.setString(6, task.getBundleCd());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	// 科目分布別成績（クラス）の再集計
	private void recountSubDistRecordC(final String key) throws SQLException {
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("科目分布別成績（クラス）の再集計:" + key + task);
		}
		
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(
					QueryLoader.getInstance().getQuery(key));
			ps.setString(1, task.getExamYear());
			ps.setString(2, task.getExamCd());
			ps.setString(3, task.getBundleCd());
			ps.setString(4, task.getExamYear());
			ps.setString(5, task.getExamCd());
			ps.setString(6, task.getBundleCd());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	// 設問別成績（クラス）の再集計
	private void recountQRecordC() throws SQLException {
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("設問別成績（クラス）の再集計" + task);
		}
		
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(
					QueryLoader.getInstance().getQuery("recount17"));
			ps.setString(1, task.getExamYear());
			ps.setString(2, task.getExamCd());
			ps.setString(3, task.getBundleCd());
			ps.setString(4, task.getExamYear());
			ps.setString(5, task.getExamCd());
			ps.setString(6, task.getBundleCd());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	// 志望大学評価別人数（クラス）の再集計
	private void recountRatingnumberC() throws SQLException {
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("志望大学評価別人数（クラス）の再集計" + task);
		}
		
		// ワークデータ登録
		registRatingnumberWork();
		
		// 大学集計区分（0=大学）
		recountRatingnumberC("recount19");
		// 大学集計区分（2=学部まで）
		recountRatingnumberC("recount21");
		// 大学集計区分（4=学科まで）
		recountRatingnumberC("recount23");
		
		// 大学マスタが高3なら日程ありレコード作成
		if (!task.isUniv12Exam()) {
			// 大学集計区分（1=大学+日程）
			recountRatingnumberC("recount20");
			// 大学集計区分（3=学部まで+日程）
			recountRatingnumberC("recount22");
		}
		
		// 現役高卒区分が0のレコード作成
		recountTotalRatingnumberC();
	}

	private void recountRatingnumberC(	final String key) throws SQLException {
		
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs1 = null;
		try {
			ps1 = con.prepareStatement(
					QueryLoader.getInstance().getQuery(key));
			
			ps2 = con.prepareStatement(
					QueryLoader.getInstance().getQuery("recount18"));

			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				
				// 模試年度
				ps2.setString(1, rs1.getString(1));
				// 模試コード
				ps2.setString(2, rs1.getString(2));
				// 一括コード
				ps2.setString(3, rs1.getString(3));
				// 学年
				ps2.setInt(4, rs1.getInt(4));
				// クラス
				ps2.setString(5, rs1.getString(5));
				// 大学集計区分
				ps2.setString(7, rs1.getString(6));
				// 大学コード
				ps2.setString(8, rs1.getString(7));
				// 学部コード
				ps2.setString(9, rs1.getString(8));
				// 学科コード
				ps2.setString(10, rs1.getString(9));
				// 日程コード
				ps2.setString(11, rs1.getString(10));
				// 現役高卒区分
				ps2.setString(12, rs1.getString(11));
				// 総志望者数
				setNumber(ps2, 13, rs1.getInt(12));
				// 第一志望者数
				setNumber(ps2, 14, rs1.getInt(13));

				// センター
				recountRatingnumberC(ps2, rs1, "1", 14);
				// 2次
				recountRatingnumberC(ps2, rs1, "2", 20);
				// 総合
				recountRatingnumberC(ps2, rs1, "3", 26);
			}
		} finally {
			DbUtils.closeQuietly(null, ps1, rs1);
			DbUtils.closeQuietly(ps2);
		}
	}

	private void setNumber(final PreparedStatement ps,
			final int index, final int number) throws SQLException {
		if (number > 0) {
			ps.setInt(index, number);
		} else {
			ps.setNull(index, Types.INTEGER);
		}
	}
	
	private void recountRatingnumberC(final PreparedStatement ps,
			final ResultSet rs, final String ratingDiv,
			final int index) throws SQLException {
		
		// 評価に値が入っているならレコードを作る
		if (rs.getInt(index) > 0) {
			// 評価区分
			ps.setString(6, ratingDiv);
			// 評価別人数A
			setNumber(ps, 15, rs.getInt(index + 1));
			// 評価別人数B
			setNumber(ps, 16, rs.getInt(index + 2));
			// 評価別人数C
			setNumber(ps, 17, rs.getInt(index + 3));
			// 評価別人数D
			setNumber(ps, 18, rs.getInt(index + 4));
			// 評価別人数E
			setNumber(ps, 19, rs.getInt(index + 5));
			// INSERT
			ps.executeUpdate();
		}
	}
	
	private void recountTotalRatingnumberC() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(
					QueryLoader.getInstance().getQuery("recount24"));
			ps.setString(1, task.getExamYear());
			ps.setString(2, task.getExamCd());
			ps.setString(3, task.getBundleCd());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	// 再集計対象レコード削除
	private void deleteRecountTable(final String key) throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(QueryLoader.getInstance().getQuery(key));
			ps.setString(1, task.getExamYear());
			ps.setString(2, task.getExamCd());
			ps.setString(3, task.getBundleCd());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	private void registRatingnumberWork() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(getRatingnumberWorkQuery());
			ps.setString(1, task.getExamDiv());
			ps.setString(2, task.getExamYear());
			ps.setString(3, task.getExamCd());
			ps.setString(4, task.getBundleCd());
			ps.setString(5, task.getExamDiv());
			ps.setString(6, task.getExamYear());
			ps.setString(7, task.getExamCd());
			ps.setString(8, task.getBundleCd());
			
			int count = ps.executeUpdate();
			
			if (LOG.isDebugEnabled()) {
				LOG.debug(
						"志望大学評価別人数（クラス）のワークデータ登録：" + count + "レコード");
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	private String getRatingnumberWorkQuery() throws SQLException {
		
		Query query = QueryLoader.getInstance().load("recount25");
		
		// 高1・2大学マスタ利用なら書き換える
		if (task.isUniv12Exam()) {
    		query.replaceAll("univmaster_basic", "h12_univmaster"); // マスタ
    		query.replaceAll("u.univcd", "u.univcd"); // 大学コード
    		query.replaceAll("u.unidiv", "u.unidiv"); // 大学区分コード
    		query.replaceAll("u.unigdiv", "'Z'"); // 日程コード
		}
		
		return query.toString();
	}
	
	/**
	 * @see jp.co.fj.keispro.beans.recount.IRecountBean
	 * 			#setConnection(java.sql.Connection)
	 */
	public void setConnection(final Connection pCon) {
		con = pCon;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.recount.IRecountBean
	 * 			#setRecountTask(jp.co.fj.keinavi.beans.recount.data.RecountTask)
	 */
	public void setRecountTask(final RecountTask pTask) {
		task = pTask;
	}

}

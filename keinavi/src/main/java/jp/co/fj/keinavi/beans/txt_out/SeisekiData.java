package jp.co.fj.keinavi.beans.txt_out;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * 取得成績データクラス
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)MORITA.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SeisekiData {

    /**
     * 出力フラグ用マップ
     */
    private static final Map ANSWER_NO1_FLAG_MAP = new HashMap();

    static {
        ANSWER_NO1_FLAG_MAP.put("4210", "0");//物理
        ANSWER_NO1_FLAG_MAP.put("4310", "1");//化学
        ANSWER_NO1_FLAG_MAP.put("4410", "2");//生物
        ANSWER_NO1_FLAG_MAP.put("4510", "3");//地学
        ANSWER_NO1_FLAG_MAP.put("4610", "4");//理総Ａ
        ANSWER_NO1_FLAG_MAP.put("4710", "5");//理総Ｂ
        ANSWER_NO1_FLAG_MAP.put("4215", "6");//物理�T
        ANSWER_NO1_FLAG_MAP.put("4315", "7");//化学�T
        ANSWER_NO1_FLAG_MAP.put("4415", "8");//生物�T
        ANSWER_NO1_FLAG_MAP.put("4515", "9");//地学�T
        ANSWER_NO1_FLAG_MAP.put("5370", "0");//世界史B
        ANSWER_NO1_FLAG_MAP.put("5270", "1");//日本史B
        ANSWER_NO1_FLAG_MAP.put("5470", "2");//地理B
        ANSWER_NO1_FLAG_MAP.put("5760", "3");//倫理政経
        ANSWER_NO1_FLAG_MAP.put("5560", "4");//政治経済
        ANSWER_NO1_FLAG_MAP.put("5160", "5");//現代社会
        ANSWER_NO1_FLAG_MAP.put("5660", "6");//倫理
        ANSWER_NO1_FLAG_MAP.put("5360", "7");//世界史A
        ANSWER_NO1_FLAG_MAP.put("5260", "8");//日本史A
        ANSWER_NO1_FLAG_MAP.put("5460", "9");//地理A
    }

    /** 成績データ配列 */
    private String[] seiseki;

    /** オープン系模試かどうか */
    private boolean open;

    /** センタリサーチかどうか */
    private boolean cr;

    /**
     * コンストラクタ
     * @param seiseki
     * @param open
     * @param cr
     */
    SeisekiData(String[] seiseki, boolean open, boolean cr) {
        this.seiseki = seiseki;
        this.open = open;
        this.cr = cr;
    }

    /**
     * 第一解答科目の出力用フラグを取得する。
     * @return
     */
    String getAnswerNo1Flag() {
        if (open) {
            throw new InternalError(
                "オープン系模試ではこのメソッドは使えません。");
        }
        return (String) ANSWER_NO1_FLAG_MAP.get(seiseki[10]);
    }

    /**
     * 科目コードを取得する。
     * @return
     */
    String getSubcd() {
        // 科目コードを取得
        if (open) {
            return seiseki[10];
        }
        // 科目コード先頭3桁を取得
        return seiseki[10].substring(0, 3);
    }

    /**
     * 得点を取得する。
     * ※センタリサーチ以外は換算得点
     * @return
     */
    String getScore() {
        //センタリサーチは得点
        if (cr) {
            return seiseki[13];
        }
        //それ以外は換算得点
        return seiseki[12];
    }

    /**
     * 範囲区分を取得する。
     * @return
     */
    String getScope() {
        return seiseki[17];
    }

    /**
     * 第一解答科目かどうかを判定する。
     * @return
     */
    boolean isAnswerNo1() {
        return "1".equals(getScope());
    }

    /**
     * 出力用の行マップのインデックス値を取得する。
     * ※項番148以降はプラス6（一科目分ずらす）する必要がある。
     * @return
     */
    Integer getLineMapNo() {

        //理科
        if (getSubcd().startsWith("4")) {
            // 2020/1/7 QQ)Ooseto 記述延期対応 UPD START
            // 2019/09/18 QQ)Ooseto 共通テスト対応 UPD START
            return new Integer(153 + 6+12);
            //return new Integer(177 + 6+12);
            // 2019/09/18 QQ)Ooseto 共通テスト対応 UPD END
            // 2020/1/7 QQ)Ooseto 記述延期対応 UPD END
        }

        //社会
        if (getSubcd().startsWith("5")) {
            // 2020/1/7 QQ)Ooseto 記述延期対応 UPD START
            // 2019/09/18 QQ)Ooseto 共通テスト対応 UPD START
            return new Integer(154 + 6+12);
            //return new Integer(178 + 6+12);
            // 2019/09/18 QQ)Ooseto 共通テスト対応 UPD END
            // 2020/1/7 QQ)Ooseto 記述延期対応 UPD END
        }

        return null;
    }

}

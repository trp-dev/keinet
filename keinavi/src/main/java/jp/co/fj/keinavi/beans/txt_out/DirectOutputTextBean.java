/*
 * 作成日: 2004/12/10
 */
package jp.co.fj.keinavi.beans.txt_out;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import jp.co.fj.keinavi.util.CollectionUtil;

/**
 * テキスト出力クラス
 * 
 * @author kawai
 */
public abstract class DirectOutputTextBean extends OutputTextBean {

    // 改行
    private static final String SEPARATOR = "\r\n";
    
    // 入れ物
    protected List container = new LinkedList();
    // Writer
	protected BufferedWriter writer;
	// 出力対象セット
	protected Set outSet;
	// カウンタ
	private int count;
	
    /**
     * コンストラクタ
     */
    protected DirectOutputTextBean() {
    }
    
    /**
     * 初期化処理
     * @throws IOException
     */
    public void init() throws IOException {
		// ディレクトリの作成
		new File(this.getOutFile().getParentFile().getPath()).mkdir();
		
		// Writerを設定する
		this.writer =
		    new BufferedWriter(
		            new OutputStreamWriter(
		                    new FileOutputStream(this.getOutFile().getPath(), false), "MS932"
                  	)
          	);

		// ヘッダー情報の抽出
		String header[] = super.getHeader(this.getHeadTextList());

		// ヘッダーの書き込み
		for(int i=0; i<header.length; i++){
			writer.write(header[i]+",");
		}

		// 改行しておく
		writer.write(SEPARATOR);

		// 出力対象
		this.outSet = CollectionUtil.array2Set(this.getOutTarget());
    }
    
    /**
     * レコードを追加する
     * @param container
     */
    public void add(final String value) {
        container.add(value);
    }
    
    public abstract void setupLine() throws IOException;
    
    /**
     * 行を書き出す
     * @throws IOException
     */
    protected void createLine() throws IOException {
    	this.setupLine();
        container.clear();
        writer.write(SEPARATOR);
        if (++count % 1000 == 0) writer.flush();
    }
    
    /**
     * Writerを閉じる
     */
    public void close() {
        try { if (writer != null) writer.close(); } catch (IOException e) {}
    }
    
    /**
     * 出力サブクラスのインスタンスを返す
     * @param type
     * @return
     */
    public static DirectOutputTextBean getInstance(final int type) {
        if (type == 1) return new CSVOutputBean();
        else return new FixedOutputBean();
    }
}

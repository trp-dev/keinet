package jp.co.fj.keinavi.forms.maintenance;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;

import jp.co.fj.keinavi.beans.maintenance.module.StudentDataNormalizer;
import jp.co.fj.keinavi.data.maintenance.KNStudentData;
import jp.co.fj.keinavi.forms.BaseForm;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * 生徒情報新規登録画面アクションフォーム
 * 
 * 2005.10.21	[新規作成]
 * 2006.10.27	2006年度改修
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M102Form extends BaseForm {

	// 動作モード
	private String actionMode;

	// 対象年度
	private String targetYear;

	// 対象学年
	private String targetGrade;

	// 対象クラス
	private String targetClass;

	// 学年
	private String grade;

	// クラス
	private String className;

	// クラス番号
	private String classNo;

	// 性別
	private String sex;

	// かな氏名
	private String nameKana;

	// 漢字氏名
	private String nameKanji;

	// 電話番号
	private String telNo;

	// 生年月日・年
	private String birthYear;

	// 生年月日・月
	private String birthMonth;

	// 生年月日・日
	private String birthDate;

	// 絞り込み選択
	private String conditionSelect;

	// 絞り込み条件
	private String conditionText;
	
	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}
	
	/**
	 * 画面の値から生徒情報データを作る
	 * 
	 * @return 生徒情報データ
	 */
	public KNStudentData toStudentData() {
		
		final StudentDataNormalizer normalizer = StudentDataNormalizer.getInstance();
		final KNStudentData data = createStudentData();
		
		data.setYear(getTargetYear());
		
		if (!StringUtils.isEmpty(getGrade())) {
			data.setGrade(Integer.parseInt(getGrade()));
			data.setClassName(normalizeClassName(getClassName()));
			data.setClassNo(normalizeClassNo(getClassNo()));
		}
		
		data.setSex(getSex());
		data.setNameHiragana(getNameKana());
		data.setNameKana(normalizer.normalizeNameKana(getNameKana()));
		data.setNameKanji(getNameKanji());

		// 従情報で上書きする場合、不正な情報が飛んでくる可能性があるので
		// 例外の発生は無視する
		try {
			data.setBirthday(new SimpleDateFormat("yyyy/MM/dd").parse(
					getBirthYear() + "/" + getBirthMonth() + "/" + getBirthDate()));
		} catch (final ParseException e) {
		}
		
		data.setTelNo(normalizer.normalizeTelNo(getTelNo()));
		
		return data;
	}
	
	/**
	 * 生徒情報データのインスタンスを作る
	 * [FactoryMethod]
	 * 
	 * @return 生徒情報データ
	 */
	protected KNStudentData createStudentData() {
		return new KNStudentData(Integer.parseInt(KNUtil.getCurrentYear())
				- Integer.parseInt(getTargetYear()));
	}
	
	/**
	 * @param className 正規化するクラス名
	 * @return 正規化されたクラス名
	 */
	protected String normalizeClassName(final String className) {
		
		return StudentDataNormalizer.getInstance(
				).normalizeClassName(className);
	}

	/**
	 * @param classNo 正規化するクラス番号
	 * @return 正規化されたクラス番号
	 */
	protected String normalizeClassNo(final String classNo) {
		
		return StudentDataNormalizer.getInstance(
				).normalizeClassNo(classNo);
	}
	
	/**
	 * @return actionMode を戻します。
	 */
	public String getActionMode() {
		return actionMode;
	}

	/**
	 * @param pActionMode
	 *            設定する actionMode。
	 */
	public void setActionMode(final String pActionMode) {
		this.actionMode = pActionMode;
	}

	/**
	 * @return className を戻します。
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param pClassName
	 *            設定する className。
	 */
	public void setClassName(final String pClassName) {
		this.className = pClassName;
	}

	/**
	 * @return classNo を戻します。
	 */
	public String getClassNo() {
		return classNo;
	}

	/**
	 * @param pClassNo
	 *            設定する classNo。
	 */
	public void setClassNo(final String pClassNo) {
		this.classNo = pClassNo;
	}

	/**
	 * @return grade を戻します。
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * @param pGrade
	 *            設定する grade。
	 */
	public void setGrade(final String pGrade) {
		this.grade = pGrade;
	}

	/**
	 * @return nameKana を戻します。
	 */
	public String getNameKana() {
		return nameKana;
	}

	/**
	 * @param pNameKana
	 *            設定する nameKana。
	 */
	public void setNameKana(final String pNameKana) {
		this.nameKana = pNameKana;
	}

	/**
	 * @return nameKanji を戻します。
	 */
	public String getNameKanji() {
		return nameKanji;
	}

	/**
	 * @param pNnameKanji
	 *            設定する nameKanji。
	 */
	public void setNameKanji(final String pNnameKanji) {
		this.nameKanji = pNnameKanji;
	}

	/**
	 * @return sex を戻します。
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @param pSex
	 *            設定する sex。
	 */
	public void setSex(final String pSex) {
		this.sex = pSex;
	}

	/**
	 * @return telNo を戻します。
	 */
	public String getTelNo() {
		return telNo;
	}

	/**
	 * @param pTelNo
	 *            設定する telNo。
	 */
	public void setTelNo(final String pTelNo) {
		this.telNo = pTelNo;
	}

	/**
	 * @return birthDate を戻します。
	 */
	public String getBirthDate() {
		return birthDate;
	}

	/**
	 * @param pBirthDate
	 *            設定する birthDate。
	 */
	public void setBirthDate(final String pBirthDate) {
		this.birthDate = pBirthDate;
	}

	/**
	 * @return birthMonth を戻します。
	 */
	public String getBirthMonth() {
		return birthMonth;
	}

	/**
	 * @param pBirthMonth
	 *            設定する birthMonth。
	 */
	public void setBirthMonth(final String pBirthMonth) {
		this.birthMonth = pBirthMonth;
	}

	/**
	 * @return birthYear を戻します。
	 */
	public String getBirthYear() {
		return birthYear;
	}

	/**
	 * @param pBirthYear
	 *            設定する birthYear。
	 */
	public void setBirthYear(final String pBirthYear) {
		this.birthYear = pBirthYear;
	}

	/**
	 * @return targetYear を戻します。
	 */
	public String getTargetYear() {
		return targetYear;
	}

	/**
	 * @param pTargetYear
	 *            設定する targetYear。
	 */
	public void setTargetYear(final String pTargetYear) {
		this.targetYear = pTargetYear;
	}

	/**
	 * @return conditionSelect を戻します。
	 */
	public String getConditionSelect() {
		return conditionSelect;
	}

	/**
	 * @param pConditionSelect
	 *            設定する conditionSelect。
	 */
	public void setConditionSelect(String pConditionSelect) {
		conditionSelect = pConditionSelect;
	}

	/**
	 * @return conditionText を戻します。
	 */
	public String getConditionText() {
		return conditionText;
	}

	/**
	 * @param pConditionText
	 *            設定する conditionText。
	 */
	public void setConditionText(String pConditionText) {
		conditionText = pConditionText;
	}

	/**
	 * @return targetClass
	 */
	public String getTargetClass() {
		return targetClass;
	}

	/**
	 * @param pTargetClass 設定する targetClass
	 */
	public void setTargetClass(String pTargetClass) {
		targetClass = pTargetClass;
	}

	/**
	 * @return targetGrade
	 */
	public String getTargetGrade() {
		return targetGrade;
	}

	/**
	 * @param pTargetGrade 設定する targetGrade
	 */
	public void setTargetGrade(String pTargetGrade) {
		targetGrade = pTargetGrade;
	}
	
}

/*
 * 作成日: 2004/12/22
 */
package jp.co.fj.keinavi.servlets.school;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.configuration.Configuration;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.school.S012Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.totec.config.ConfigResolver;

/**
 * サンプル画面サーブレット
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */
public class S012Servlet extends DefaultHttpServlet {

    // 設定ファイル
    private static final String PROPERTIES = "/WEB-INF/sample.txt";
    // 設定
	private static Properties CONFIG;
	// 画面名称マップ
	private static final Map SCREEN = new HashMap();
	
	static {
		final Configuration config = ConfigResolver.getInstance(
				).getConfiguration("sample.properties");
		for (final Iterator ite = config.getKeys(); ite.hasNext();) {
			final String key = (String) ite.next();
			SCREEN.put(key, config.getString(key));
		}
	}
	
    /* (非 Javadoc)
     * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    protected void execute(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        // 設定値のロード
        if (CONFIG == null) this.load();
    
        // ログイン情報
        LoginSession login = super.getLoginSession(request);
		// アクションフォームの取得 - request scope
		S012Form form = (S012Form) super.getActionForm(request,
				"jp.co.fj.keinavi.forms.school.S012Form");
        
        // カテゴリの指定がなければ画面IDから抜き出す
        if (form.getCategory() == null || "".equals(form.getCategory()))
            form.setCategory(form.getScreen().substring(0, 1));
        
        // 動作モードを判定する
        // 0 営業
        // 1 私塾
        // 2 教育委員会
        // 3 非契約校（営業代行）
        // 4 P保護学校
        // 5 高校
        int mode = 5;
        if (login.isSales()) mode = 0;
        else if (!login.isPact()) mode = 3;
        else if (login.isPrep()) mode = 1;
        else if (login.isBoard()) mode = 2;
        else if (login.isPrivacyProtection()) mode = 4;
        
        // 表示対象
        SortedSet targets = new TreeSet();
        {
            Iterator ite = CONFIG.keySet().iterator();
            while (ite.hasNext()) {
                String key = (String) ite.next();
                String value = (String) CONFIG.get(key);

                // 権限がなければ飛ばす
                if (value.length() <= mode || (int) value.charAt(mode) == 48) continue;
                
                if (key.startsWith("s")) {
                    request.setAttribute("DispSchool", new Boolean(true));
                } else if (key.startsWith("c")) {
                    request.setAttribute("DispClass", new Boolean(true));
                } else if (key.startsWith("i")) {
                    request.setAttribute("DispIndividual", new Boolean(true));
                } else if (key.startsWith("b")) {
                    request.setAttribute("DispBusiness", new Boolean(true));
                }
                
                // カテゴリに属していたら入れる
                if (key.startsWith(form.getCategory()))
                    targets.add(key.substring(0, key.indexOf('-')));
            }
        }
        
        // 画面IDがなければ決定する
        // → カテゴリ内の最初の画面ID
        if (form.getScreen() == null || "".equals(form.getScreen())) {
            Iterator ite = targets.iterator();
            while (ite.hasNext()) {
                String key = (String) ite.next();
                if (key.startsWith(form.getCategory())) {
                   form.setScreen(key);
                   break;
                }
            }
        }
        
        // 表示対象がなければ工事中
        if (targets.size() == 0 || !targets.contains(form.getScreen())) {
            super.forward(request, response, "/underconstruction.html");

        // あるならJSP
        } else {
            // 表示する画像リスト
            SortedSet container = new TreeSet(); 
            {
                Iterator ite = CONFIG.keySet().iterator();
                while (ite.hasNext()) {
                    String key = (String) ite.next();
                    if (key.startsWith(form.getScreen())) container.add(key);
                }
            }
            
            // 前へ・次へ
            String backward = (String) targets.last();
            String forward = (String) targets.first();
            Iterator ite = targets.iterator();
            while (ite.hasNext()) {
                String value = (String) ite.next();
                
                if (form.getScreen().equals(value)) {
                    if (ite.hasNext()) forward = (String) ite.next();
                    break;
                }
                
                backward = value;
            }

            request.setAttribute("Forward", forward);
            request.setAttribute("Backward", backward);
            request.setAttribute("form", form);
            request.setAttribute("ScreenName", SCREEN);
            request.setAttribute("Targets", targets);
            request.setAttribute("Images", container);
            super.forward(request, response, JSP_S012);
        }    
    }

    /**
     * 設定値を読み込む
     */
    synchronized private void load() {
        if (CONFIG != null) return;
        
		try {
	        InputStream in = new FileInputStream(
            	super.getServletContext().getRealPath(PROPERTIES));
		    Properties p = new Properties();
			p.load(in);
			
			CONFIG = p;
		} catch (IOException e) {
			throw new InternalError(e.getMessage());
		}
    }
    
}

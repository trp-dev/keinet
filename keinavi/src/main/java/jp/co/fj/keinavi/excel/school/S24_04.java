package jp.co.fj.keinavi.excel.school;

//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S24HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.school.S24Item;
import jp.co.fj.keinavi.excel.data.school.S24ListBean;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * 作成日: 2004/08/25
 * @author Ito.Y
 *
 * 校内成績分析−過年度比較−志望大学評価別人数　学部（日程あり）出力処理
 *
 * <2010年度改修>
 * 2010.01.06   Tomohisa YAMADA - Totec
 *              センター・リサーチ５段階評価対応
 *
 * 2010.07.12   Tomohisa YAMADA - Totec
 * 				私立大学の場合のヘッダ書き換えバグ修正
 *
 */
public class S24_04 {

	private int noerror = 0; // 正常終了
	private int errfread = 1; // ファイルreadエラー
	private int errfwrite = 2; // ファイルwriteエラー
	private int errfdata = 3; // データ設定エラー

	private CM cm = new CM(); //共通関数用クラス インスタンス

	private int intMaxSheetSr = 50; //MAXシート数
	private int intMaxRow = 80; //MAX行数

	private boolean bolBookCngFlg = true; //改ファイルフラグ
	private boolean bolBookClsFlg = false; //改ファイルフラグ
	private boolean bolSheetCngFlg = true; //改シートフラグ
	private int intDataStartRow = 7; //データセット開始行

	final private String masterfile0 = "S24_04";
	final private String masterfile1 = "S24_04";
	private String masterfile = "";

	// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 ADD START
	final private int MAXCOL = 11;
	// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 ADD END

	/*
	 * 	Excel編集メイン
	 * 		S24Item s24Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int s24_04EditExcel(S24Item s24Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		HSSFWorkbook workbook = null;
		HSSFSheet workSheet = null;
		HSSFCell workCell = null;
		HSSFRow workRow = null;

		int intMaxSheetIndex = 0; //シート作成数カウンタ

		int intRow = 0;
		String strGenekiKbn = "";
		String strGakubuCode = "";
		String strGakubuCodeHoji = "";
		String strGakubuuMei = "";
		String strDaigakuCode = "";
		String strDaigakuCodeHoji = "";
		String strDaigakuMei = "";
		String strNitteiCode = "";

		int intLoopCnt = 0;
		int intNinzuListCnt = 0;
		int intSheetCngCount = 0;
		int intBookCngCount = 0;

		String strDispTarget = "";

		//テンプレートの決定
		if (s24Item.getIntShubetsuFlg() == 1) {
			masterfile = masterfile1;
		} else {
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S24ListBean s24ListBean = new S24ListBean();

		// 学部合計リスト
		ArrayList addGkbCenterList = null;
		ArrayList addGkbSecondList = null;
		ArrayList addGkbTotalList = null;
		ArrayList tmpAddGkbList = null;

		// 大学合計リスト
		ArrayList addDgkCenterList = null;
		ArrayList addDgkSecondList = null;
		ArrayList addDgkTotalList = null;
		ArrayList tmpAddDgkList = null;

		// 日程合計リスト
		ArrayList addNtiChukiCenList = null;
		ArrayList addNtiChukiSecList = null;
		ArrayList addNtiChukiTotList = null;
		ArrayList addNtiZenkiCenList = null;
		ArrayList addNtiZenkiSecList = null;
		ArrayList addNtiZenkiTotList = null;
		ArrayList addNtiKokiCenList = null;
		ArrayList addNtiKokiSecList = null;
		ArrayList addNtiKokiTotList = null;
		ArrayList addNtiShinCenList = null;
		ArrayList addNtiShinSecList = null;
		ArrayList addNtiShinTotList = null;
		ArrayList addNtiCntrCenList = null;
		ArrayList addNtiCntrSecList = null;
		ArrayList addNtiCntrTotList = null;
		ArrayList tmpAddNtiList = null;

		// 日程名保持
		String zenkimei = "";
		String chukimei = "";
		String kokimei = "";
		String centermei = "";
		String sinsetumei = "";

		//2004.09.27Add
		String strKokushiKbn = "";
		String strStKokushiKbn = "";
		//2004.09.27AddEnd

		//2004.10.25Add
		int intDataCnt = 0;
		//2004.10.25AddEnd

		try {
			// Listの取得
			ArrayList s24List = s24Item.getS24List();
			ListIterator itr = s24List.listIterator();

			/** 2004.9.28 Remove
			if( itr.hasNext() == false ){
				return errfdata;
			}
			**/

			//2004.10.25Add
			if (itr.hasNext() == false) {
				intDataCnt = 0;
			} else {
				intDataCnt = 1;
			}

			//2004.10.25AddEnd

			/** ループ開始 **/
			// 24Listの展開
			while (itr.hasNext()) {

				s24ListBean = (S24ListBean) itr.next();
				ArrayList s24HyoukaNinzuList = s24ListBean.getS24HyoukaNinzuList();

				// 現役生・高卒生区分判定(区分が異なっていたら改シート)
				if ((!cm.toString(strGenekiKbn).equals(cm.toString(s24ListBean.getStrGenKouKbn()))) &&
						(addGkbCenterList == null) && (addGkbSecondList == null) && (addGkbTotalList == null) &&
						(addDgkCenterList == null) && (addDgkSecondList == null) && (addDgkTotalList == null)) {
					strGenekiKbn = s24ListBean.getStrGenKouKbn();
					if (cm.toString(strGenekiKbn).equals("0")) {
						strDispTarget = "全体";
					} else if (cm.toString(strGenekiKbn).equals("1")) {
						strDispTarget = "現役生";
					} else if (cm.toString(strGenekiKbn).equals("2")) {
						strDispTarget = "高卒生";
					}

					// 改シート・改ブック判定
					//					strDaigakuCode = "";
					strGakubuCode = "";
					strNitteiCode = "";
					if (bolSheetCngFlg == false) {
						//罫線出力（セルの上部に太線を出力）

					        // 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
						// 2019/09/30 QQ) 共通テスト対応 UPD START
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
						//    intRow, intRow, 0, 12, false, true, false, false);
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
						//		intRow, intRow, 0, 16, false, true, false, false);
                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                        intRow, intRow, 0, MAXCOL, false, true, false, false);
						// 2019/09/30 QQ) 共通テスト対応 UPD END
						// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

						bolSheetCngFlg = true;
						//intMaxSheetIndex++;
						if (intMaxSheetIndex >= intMaxSheetSr) {
							//改ファイル
							bolBookCngFlg = true;
							bolBookClsFlg = true;
						}
					}
				}

				//2004.9.27 Add
				int intDaigakuCd = (int) Integer.parseInt(s24ListBean.getStrDaigakuCd());
				if (intDaigakuCd >= 1001 && intDaigakuCd <= 1899) {
					strKokushiKbn = "国公立大";
				}
				if (intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999)) {
					strKokushiKbn = "私立大";
				}
				if (intDaigakuCd >= 3001 && intDaigakuCd <= 4999) {
					strKokushiKbn = "短大";
				}
				if (intDaigakuCd >= 5001 && intDaigakuCd <= 7999) {
					strKokushiKbn = "その他";
				}

				if ((!strKokushiKbn.equals(strStKokushiKbn)) &&
						(addGkbCenterList == null) && (addGkbSecondList == null) && (addGkbTotalList == null) &&
						(addDgkCenterList == null) && (addDgkSecondList == null) && (addDgkTotalList == null)) {
					strStKokushiKbn = strKokushiKbn;
					// 改シート・改ブック判定
					//					strDaigakuCode = "";
					strGakubuCode = "";
					strNitteiCode = "";
					if (bolSheetCngFlg == false) {
						//罫線出力（セルの上部に太線を出力）

					        // 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
						// 2019/09/30 QQ) 共通テスト対応 UPD START
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
						//        intRow, intRow, 0, 12, false, true, false, false);
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
						//		intRow, intRow, 0, 16, false, true, false, false);
                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                        intRow, intRow, 0, MAXCOL, false, true, false, false);
						// 2019/09/30 QQ) 共通テスト対応 UPD END
						// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

						bolSheetCngFlg = true;
						//intMaxSheetIndex++;
						if (intMaxSheetIndex >= intMaxSheetSr) {
							//改ファイル
							bolBookCngFlg = true;
							bolBookClsFlg = true;
						}
					}
				}
				//2004.9.27 Add End

				// 改シート・改ブック処理
				if (bolSheetCngFlg == true) {

					//2004.9.27 Add
					if (workSheet != null) {
						int intRemoveStartRow = (intSheetCngCount * intNinzuListCnt) + intDataStartRow;
						if (intMaxRow + intDataStartRow > intRemoveStartRow) {
							for (int index = intRemoveStartRow; index < intMaxRow + intDataStartRow; index++) {
								//行削除処理
								workSheet.removeRow(workSheet.getRow(index));
							}
							//罫線出力（セルの上部に太線を出力）

							// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
							// 2019/09/30 QQ) 共通テスト対応 UPD START
							//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
							//        intRemoveStartRow, intRemoveStartRow, 0, 12, false, true, false, false);
							//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
							//		intRemoveStartRow, intRemoveStartRow, 0, 16, false, true, false, false);
                                                        cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                                intRemoveStartRow, intRemoveStartRow, 0, MAXCOL, false, true, false, false);
							// 2019/09/30 QQ) 共通テスト対応 UPD END
							// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

						}
					}
					//2004.9.27 Add End

					if (bolBookCngFlg == true) {

						if (bolBookClsFlg == true) {
							//WorkBook保存
							boolean bolRet = false;
							// Excelファイル保存
							bolRet = clsBook(workbook, intMaxSheetIndex,
									intBookCngCount, intSaveFlg, outfilelist, UserID, false);
							if (bolRet == false) {
								return errfwrite;
							}
							intBookCngCount++;
							bolBookClsFlg = false;
						}

						// マスタExcel読み込み
						workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
						if (workbook == null) {
							return errfread;
						}

						bolBookCngFlg = false;
						intMaxSheetIndex = 0;
					}

					//シート作成
					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);

					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);

					// ヘッダー
					//2004.9.28 Update
					//					setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
					//										s24Item.getStrMshmei(),	s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget );
					setHeader(workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
							s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget,
							strStKokushiKbn);
					//2004.9.28 Update End

					intMaxSheetIndex++; //シート数カウンタインクリメント
					bolSheetCngFlg = false;

					intRow = intDataStartRow;
					intLoopCnt = 0;

				}

				NumberFormat nf = NumberFormat.getInstance();
				intNinzuListCnt = (int) s24HyoukaNinzuList.size(); //評価別人数リスト データ数取得
				intSheetCngCount = nf.parse(nf.format(intMaxRow / intNinzuListCnt)).intValue();

				/**  合計表示処理  **/
				// 小計セット → 大学が変わったときは処理する 2005.05.25
				if (s24ListBean.getStrGakubuCd() == null) {
					s24ListBean.setStrGakubuCd("");
				}

				//-----------------------------------------------------------------------------------------
				//大学計・学部計を出力
				//-----------------------------------------------------------------------------------------
				if (!cm.toString(strGakubuCodeHoji).equals(cm.toString(s24ListBean.getStrGakubuCd())) ||
						!cm.toString(strDaigakuCodeHoji).equals(cm.toString(s24ListBean.getStrDaigakuCd()))
						|| !cm.toString(strGenekiKbn).equals(cm.toString(s24ListBean.getStrGenKouKbn()))) {

					///////////////////////
					strNitteiCode = "";
					///////////////////////

					// データセット
					boolean firstFlg = true;
					for (int a = 0; a < 3; a++) {
						String strHyokaKbn = "";
						if (a == 0) {
							tmpAddGkbList = addGkbCenterList;
							tmpAddDgkList = addDgkCenterList;
							// 2019/09/30 QQ) 共通テスト対応 UPD START
							//strHyokaKbn = "センター";
							strHyokaKbn = "共テ";
							// 2019/09/30 QQ) 共通テスト対応 UPD END
						} else if (a == 1) {
							tmpAddGkbList = addGkbSecondList;
							tmpAddDgkList = addDgkSecondList;
							strHyokaKbn = "2次・私大";
						} else if (a == 2) {
							tmpAddGkbList = addGkbTotalList;
							tmpAddDgkList = addDgkTotalList;
							strHyokaKbn = "総合";
						} else {
							break;
						}

						if (tmpAddGkbList != null) {
							//評価区分セット
							workCell = cm.setCell(workSheet, workRow, workCell, intRow, 3);
							workCell.setCellValue(strHyokaKbn);
							//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
							cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
									intRow, intRow, 3, 4, false, true, false, false);

							//日程セット
							if ((firstFlg) || (intLoopCnt == 0)) {
								workCell = cm.setCell(workSheet, workRow, workCell, intRow, 2);
								//2004.9.28 Update
								//								workCell.setCellValue( "小計" );
								workCell.setCellValue("学部計");
								//2004.9.28 Update End
								//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
										intRow, intRow, 2, 4, false, true, false, false);

								firstFlg = false;
							}

							//学部名セット
							if ((intLoopCnt == 0)) {
								if (strGakubuuMei != null) {
									workCell = cm.setCell(workSheet, workRow, workCell, intRow, 1);
									workCell.setCellValue(strGakubuuMei);
								}
								//罫線出力(セルの上部に太線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
										intRow, intRow, 1, 4, false, true, false, false);

							}

							//大学名セット
							if ((intLoopCnt == 0)) {
								if (strDaigakuMei != null) {
									workCell = cm.setCell(workSheet, workRow, workCell, intRow, 0);
									workCell.setCellValue(strDaigakuMei);
								}
								//罫線出力(セルの上部に太線を引く）
								// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
								// 2019/09/30 QQ) 共通テスト対応 UPD START
								//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//        intRow, intRow, 0, 12, false, true, false, false );
								//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//		intRow, intRow, 0, 16, false, true, false, false);
                                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                                        intRow, intRow, 0, MAXCOL, false, true, false, false);
								// 2019/09/30 QQ) 共通テスト対応 UPD END
								// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

							}

							//評価別人数データリストの内容をセット
							boolean ret = setData(workSheet, tmpAddGkbList, tmpAddDgkList, null, intRow);
							if (ret != true) {
								return errfdata;
							} else {
								intRow += intNinzuListCnt;
								intLoopCnt++;
							}

							// set Total
							if (a == 0) {
								if (tmpAddDgkList == null) {
									addDgkCenterList = deepCopy(tmpAddGkbList);
								} else {
									addDgkCenterList = deepCopy(tmpAddDgkList);
								}
							} else if (a == 1) {
								if (tmpAddDgkList == null) {
									addDgkSecondList = deepCopy(tmpAddGkbList);
								} else {
									addDgkSecondList = deepCopy(tmpAddDgkList);
								}
							} else if (a == 2) {
								if (tmpAddDgkList == null) {
									addDgkTotalList = deepCopy(tmpAddGkbList);
								} else {
									addDgkTotalList = deepCopy(tmpAddDgkList);
								}
							}
						}

						// 改シート・改ブック処理
						if (intLoopCnt >= intSheetCngCount) {
							//改シート
							//罫線出力（セルの上部に太線を出力）

						        // 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
							// 2019/09/30 QQ) 共通テスト対応 UPD START
							//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
							//    intRow, intRow, 0, 12, false, true, false, false);
							//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
							//		intRow, intRow, 0, 16, false, true, false, false);
                                                        cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                                intRow, intRow, 0, MAXCOL, false, true, false, false);
							// 2019/09/30 QQ) 共通テスト対応 UPD END
							// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

							strGakubuCode = "";
							strNitteiCode = "";
							bolSheetCngFlg = true;
							if (intMaxSheetIndex >= intMaxSheetSr) {
								//改ファイル
								bolBookCngFlg = true;
								bolBookClsFlg = true;
							}

							if (bolSheetCngFlg == true) {

								//2004.9.27 Add
								if (workSheet != null) {
									int intRemoveStartRow = (intSheetCngCount * intNinzuListCnt) + intDataStartRow;
									if (intMaxRow + intDataStartRow > intRemoveStartRow) {
										for (int index = intRemoveStartRow; index < intMaxRow
												+ intDataStartRow; index++) {
											//行削除処理
											workSheet.removeRow(workSheet.getRow(index));
										}
										//罫線出力（セルの上部に太線を出力）

										// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
										// 2019/09/30 QQ) 共通テスト対応 UPD START
										//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
										//        intRemoveStartRow, intRemoveStartRow, 0, 12, false, true, false, false);
										//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM,
										//		HSSFColor.BLACK.index,
										//		intRemoveStartRow, intRemoveStartRow, 0, 16, false, true, false, false);
                                                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM,
                                                                                        HSSFColor.BLACK.index,
                                                                                        intRemoveStartRow, intRemoveStartRow, 0, MAXCOL, false, true, false, false);
										// 2019/09/30 QQ) 共通テスト対応 UPD END
										// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END
									}
								}
								//2004.9.27 Add End

								if (bolBookCngFlg == true) {

									if (bolBookClsFlg == true) {
										//WorkBook保存
										boolean bolRet = false;
										bolRet = clsBook(workbook, intMaxSheetIndex,
												intBookCngCount, intSaveFlg, outfilelist, UserID, false);
										if (bolRet == false) {
											return errfwrite;
										}
										intBookCngCount++;
										bolBookClsFlg = false;
									}

									// マスタExcel読み込み
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									//									if( workbook.equals(null) ){
									if (workbook == null) {
										return errfread;
									}

									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
								}

								//シート作成
								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);

								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);

								// ヘッダー
								//2004.9.28 Update
								//								setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
								//													s24Item.getStrMshmei(),	s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget );
								setHeader(workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(),
										s24Item.getStrGakkomei(),
										s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(),
										strDispTarget, strStKokushiKbn);
								//2004.9.28 Update End

								intMaxSheetIndex++; //シート数カウンタインクリメント
								bolSheetCngFlg = false;

								intRow = intDataStartRow;
								intLoopCnt = 0;

							}
						}
					}
					addGkbCenterList = null;
					addGkbSecondList = null;
					addGkbTotalList = null;
					tmpAddGkbList = null;

				}

				// 日程計・大学計セット
				////////////////
				//			  if( !cm.toString(strDaigakuCodeHoji).equals(cm.toString(s24ListBean.getStrDaigakuCd())) ){
				////////////////
				if ((!cm.toString(strDaigakuCodeHoji).equals(cm.toString(s24ListBean.getStrDaigakuCd())))
						|| (!cm.toString(strGenekiKbn).equals(cm.toString(s24ListBean.getStrGenKouKbn())))) {

					ArrayList tmpCenterList = null;
					ArrayList tmpSecondList = null;
					ArrayList tmpTotalList = null;

					for (int a = 0; a < 6; a++) {
						String komokumei = "";
						if (a == 0) {
							komokumei = centermei;

							tmpCenterList = addNtiCntrCenList;
							tmpSecondList = addNtiCntrSecList;
							tmpTotalList = addNtiCntrTotList;
						} else if (a == 1) {
							komokumei = chukimei;

							tmpCenterList = addNtiChukiCenList;
							tmpSecondList = addNtiChukiSecList;
							tmpTotalList = addNtiChukiTotList;
						} else if (a == 2) {
							komokumei = zenkimei;

							tmpCenterList = addNtiZenkiCenList;
							tmpSecondList = addNtiZenkiSecList;
							tmpTotalList = addNtiZenkiTotList;
						} else if (a == 3) {
							komokumei = kokimei;

							tmpCenterList = addNtiKokiCenList;
							tmpSecondList = addNtiKokiSecList;
							tmpTotalList = addNtiKokiTotList;
						} else if (a == 4) {
							komokumei = sinsetumei;

							tmpCenterList = addNtiShinCenList;
							tmpSecondList = addNtiShinSecList;
							tmpTotalList = addNtiShinTotList;
						} else if (a == 5) {
							komokumei = "大学";

							tmpCenterList = addDgkCenterList;
							tmpSecondList = addDgkSecondList;
							tmpTotalList = addDgkTotalList;
						} else {
							break;
						}

						// データセット
						boolean firstFlg = true;
						for (int b = 0; b < 3; b++) {
							String strHyokaKbn = "";
							if (b == 0) {
								tmpAddDgkList = tmpCenterList;
								// 2019/09/30 QQ) 共通テスト対応 UPD START
								//strHyokaKbn = "センター";
								strHyokaKbn = "共テ";
								// 2019/09/30 QQ) 共通テスト対応 UPD END
							} else if (b == 1) {
								tmpAddDgkList = tmpSecondList;
								strHyokaKbn = "2次・私大";
							} else if (b == 2) {
								tmpAddDgkList = tmpTotalList;
								strHyokaKbn = "総合";
							} else {
								break;
							}

							if (tmpAddDgkList != null) {
								//評価区分セット
								workCell = cm.setCell(workSheet, workRow, workCell, intRow, 3);
								workCell.setCellValue(strHyokaKbn);
								//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
										intRow, intRow, 3, 4, false, true, false, false);

								//学部名セット
								if ((firstFlg) || (intLoopCnt == 0)) {
									workCell = cm.setCell(workSheet, workRow, workCell, intRow, 1);
									workCell.setCellValue(cm.toString(komokumei) + "計");
									//罫線出力(セルの上部に太線を引く）
									cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
											intRow, intRow, 1, 4, false, true, false, false);

									firstFlg = false;
								}

								//大学名セット
								if ((intLoopCnt == 0)) {
									if (strDaigakuMei != null) {
										workCell = cm.setCell(workSheet, workRow, workCell, intRow, 0);
										workCell.setCellValue(strDaigakuMei);
									}
									//罫線出力(セルの上部に太線を引く）

									// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
									// 2019/09/30 QQ) 共通テスト対応 UPD START
									//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
									//        intRow, intRow, 0, 12, false, true, false, false );
									//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
									//		intRow, intRow, 0, 16, false, true, false, false);
                                                                        cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                                                intRow, intRow, 0, MAXCOL, false, true, false, false);
									// 2019/09/30 QQ) 共通テスト対応 UPD END
									// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

								}

								//評価別人数データリストの内容をセット
								boolean ret = setData(workSheet, tmpAddDgkList, null, null, intRow);
								if (ret != true) {
									return errfdata;
								} else {
									intRow += intNinzuListCnt;
									intLoopCnt++;
								}
							}

							// 改シート・改ブック処理
							if (intLoopCnt >= intSheetCngCount) {
								//改シート
								//罫線出力（セルの上部に太線を出力）
							        // 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
								// 2019/09/30 QQ) 共通テスト対応 UPD START
								//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
								//        intRow, intRow, 0, 12, false, true, false, false);
								//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//		intRow, intRow, 0, 16, false, true, false, false);
                                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                                        intRow, intRow, 0, MAXCOL, false, true, false, false);
								// 2019/09/30 QQ) 共通テスト対応 UPD END
								// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

								//								strDaigakuCode = "";
								strGakubuCode = "";
								strNitteiCode = "";
								bolSheetCngFlg = true;
								if (intMaxSheetIndex >= intMaxSheetSr) {
									//改ファイル
									bolBookCngFlg = true;
									bolBookClsFlg = true;
								}

								if (bolSheetCngFlg == true) {

									//2004.9.27 Add
									if (workSheet != null) {
										int intRemoveStartRow = (intSheetCngCount * intNinzuListCnt) + intDataStartRow;
										if (intMaxRow + intDataStartRow > intRemoveStartRow) {
											for (int index = intRemoveStartRow; index < intMaxRow
													+ intDataStartRow; index++) {
												//行削除処理
												workSheet.removeRow(workSheet.getRow(index));
											}
											//罫線出力（セルの上部に太線を出力）
											// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
											// 2019/09/30 QQ) 共通テスト対応 UPD START
											//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
											//        intRemoveStartRow, intRemoveStartRow, 0, 12, false, true, false, false);
											//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM,
											//		HSSFColor.BLACK.index,
											//		intRemoveStartRow, intRemoveStartRow, 0, 16, false, true, false,
											//		false);
                                                                                        cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM,
                                                                                                HSSFColor.BLACK.index,
                                                                                                intRemoveStartRow, intRemoveStartRow, 0, MAXCOL, false, true, false,
                                                                                                false);
											// 2019/09/30 QQ) 共通テスト対応 UPD END
											// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END
										}
									}
									//2004.9.27 Add End

									if (bolBookCngFlg == true) {

										if (bolBookClsFlg == true) {
											//WorkBook保存
											boolean bolRet = false;
											bolRet = clsBook(workbook, intMaxSheetIndex,
													intBookCngCount, intSaveFlg, outfilelist, UserID, false);
											if (bolRet == false) {
												return errfwrite;
											}
											intBookCngCount++;
											bolBookClsFlg = false;
										}

										// マスタExcel読み込み
										workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
										//										if( workbook.equals(null) ){
										if (workbook == null) {
											return errfread;
										}

										bolBookCngFlg = false;
										intMaxSheetIndex = 0;
									}

									//シート作成
									// シートテンプレートのコピー
									workSheet = workbook.cloneSheet(0);

									// ヘッダ右側に帳票作成日時を表示する
									cm.setHeader(workbook, workSheet);

									// ヘッダー
									//2004.9.28 Update
									//									setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
									//														s24Item.getStrMshmei(),	s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget );
									setHeader(workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(),
											s24Item.getStrGakkomei(),
											s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(),
											strDispTarget, strStKokushiKbn);
									//2004.9.28 Update End

									intMaxSheetIndex++; //シート数カウンタインクリメント
									bolSheetCngFlg = false;

									intRow = intDataStartRow;
									intLoopCnt = 0;

								}
							}
						}
					}

					addNtiChukiCenList = null;
					addNtiChukiSecList = null;
					addNtiChukiTotList = null;
					addNtiZenkiCenList = null;
					addNtiZenkiSecList = null;
					addNtiZenkiTotList = null;
					addNtiKokiCenList = null;
					addNtiKokiSecList = null;
					addNtiKokiTotList = null;
					addNtiShinCenList = null;
					addNtiShinSecList = null;
					addNtiShinTotList = null;
					addNtiCntrCenList = null;
					addNtiCntrSecList = null;
					addNtiCntrTotList = null;
					tmpAddNtiList = null;

					addDgkCenterList = null;
					addDgkSecondList = null;
					addDgkTotalList = null;
					tmpAddDgkList = null;

					//					if( (!cm.toString(strGenekiKbn).equals(cm.toString(s24ListBean.getStrGenKouKbn()))) && intLoopCnt!=0 ){
					if (!cm.toString(strGenekiKbn).equals(cm.toString(s24ListBean.getStrGenKouKbn()))) {

						strGenekiKbn = s24ListBean.getStrGenKouKbn();
						if (cm.toString(strGenekiKbn).equals("0")) {
							strDispTarget = "全体";
						} else if (cm.toString(strGenekiKbn).equals("1")) {
							strDispTarget = "現役生";
						} else if (cm.toString(strGenekiKbn).equals("2")) {
							strDispTarget = "高卒生";
						}

						if (intLoopCnt == 0) {
							// ヘッダー上書き
							setHeader(workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(),
									s24Item.getStrGakkomei(),
									s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(),
									strDispTarget, strStKokushiKbn);
						} else {
							// 改シート・改ブック判定
							//						strDaigakuCode = "";
							strGakubuCode = "";
							strNitteiCode = "";
							if (bolSheetCngFlg == false) {
								//罫線出力（セルの上部に太線を出力）

							        // 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
								// 2019/09/30 QQ) 共通テスト対応 UPD START
								//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
								//    intRow, intRow, 0, 12, false, true, false, false);
								//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//		intRow, intRow, 0, 16, false, true, false, false);
                                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                                        intRow, intRow, 0, MAXCOL, false, true, false, false);
								// 2019/09/30 QQ) 共通テスト対応 UPD END
								// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

								bolSheetCngFlg = true;
								//intMaxSheetIndex++;
								if (intMaxSheetIndex >= intMaxSheetSr) {
									//改ファイル
									bolBookCngFlg = true;
									bolBookClsFlg = true;
								}
								// 改シート・改ブック処理
								if (bolSheetCngFlg == true) {

									//2004.9.27 Add
									if (workSheet != null) {
										int intRemoveStartRow = (intSheetCngCount * intNinzuListCnt) + intDataStartRow;
										if (intMaxRow + intDataStartRow > intRemoveStartRow) {
											for (int index = intRemoveStartRow; index < intMaxRow
													+ intDataStartRow; index++) {
												//行削除処理
												workSheet.removeRow(workSheet.getRow(index));
											}
											//罫線出力（セルの上部に太線を出力）

											// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
											// 2019/09/30 QQ) 共通テスト対応 UPD START
											//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
											//        intRemoveStartRow, intRemoveStartRow, 0, 12, false, true, false, false);
											//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM,
											//		HSSFColor.BLACK.index,
											//		intRemoveStartRow, intRemoveStartRow, 0, 16, false, true, false,
											//		false);
                                                                                        cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM,
                                                                                                HSSFColor.BLACK.index,
                                                                                                intRemoveStartRow, intRemoveStartRow, 0, MAXCOL, false, true, false,
                                                                                                false);
											// 2019/09/30 QQ) 共通テスト対応 UPD END
											// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END
										}
									}
									//2004.9.27 Add End

									if (bolBookCngFlg == true) {

										if (bolBookClsFlg == true) {
											//WorkBook保存
											boolean bolRet = false;
											// Excelファイル保存
											bolRet = clsBook(workbook, intMaxSheetIndex,
													intBookCngCount, intSaveFlg, outfilelist, UserID, false);
											if (bolRet == false) {
												return errfwrite;
											}
											intBookCngCount++;
											bolBookClsFlg = false;
										}

										// マスタExcel読み込み
										workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
										//									if( workbook.equals(null) ){
										if (workbook == null) {
											return errfread;
										}

										bolBookCngFlg = false;
										intMaxSheetIndex = 0;
									}

									//シート作成
									// シートテンプレートのコピー
									workSheet = workbook.cloneSheet(0);

									// ヘッダ右側に帳票作成日時を表示する
									cm.setHeader(workbook, workSheet);

									// ヘッダー
									//2004.9.28 Update
									//								setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
									//													s24Item.getStrMshmei(),	s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget );
									setHeader(workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(),
											s24Item.getStrGakkomei(),
											s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(),
											strDispTarget, strStKokushiKbn);
									//2004.9.28 Update End

									intMaxSheetIndex++; //シート数カウンタインクリメント
									bolSheetCngFlg = false;

									intRow = intDataStartRow;
									intLoopCnt = 0;

								}
							}
						}
					}

					//2004.9.27 Add
					intDaigakuCd = (int) Integer.parseInt(s24ListBean.getStrDaigakuCd());
					if (intDaigakuCd >= 1001 && intDaigakuCd <= 1899) {
						strKokushiKbn = "国公立大";
					}
					if (intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999)) {
						strKokushiKbn = "私立大";
					}
					if (intDaigakuCd >= 3001 && intDaigakuCd <= 4999) {
						strKokushiKbn = "短大";
					}
					if (intDaigakuCd >= 5001 && intDaigakuCd <= 7999) {
						strKokushiKbn = "その他";
					}

					if ((!strKokushiKbn.equals(strStKokushiKbn))) {

						strStKokushiKbn = strKokushiKbn;

						if (intLoopCnt == 0) {
							// ヘッダー上書き
							setHeader(workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(),
									s24Item.getStrGakkomei(),
									s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(),
									strDispTarget, strStKokushiKbn);
						} else {

							// 改シート・改ブック判定
							strDaigakuCode = "";
							strGakubuCode = "";
							strNitteiCode = "";
							if (bolSheetCngFlg == false) {
								//罫線出力（セルの上部に太線を出力）

							        // 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
								// 2019/09/30 QQ) 共通テスト対応 UPD START
								//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
								//        intRow, intRow, 0, 12, false, true, false, false);
								//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//		intRow, intRow, 0, 16, false, true, false, false);
                                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                                        intRow, intRow, 0, MAXCOL, false, true, false, false);
								// 2019/09/30 QQ) 共通テスト対応 UPD END
								// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

								bolSheetCngFlg = true;
								//intMaxSheetIndex++;
								if (intMaxSheetIndex >= intMaxSheetSr) {
									//改ファイル
									bolBookCngFlg = true;
									bolBookClsFlg = true;
								}
								// 改シート・改ブック処理
								if (bolSheetCngFlg == true) {

									//2004.9.27 Add
									if (workSheet != null) {
										int intRemoveStartRow = (intSheetCngCount * intNinzuListCnt) + intDataStartRow;
										if (intMaxRow + intDataStartRow > intRemoveStartRow) {
											for (int index = intRemoveStartRow; index < intMaxRow
													+ intDataStartRow; index++) {
												//行削除処理
												workSheet.removeRow(workSheet.getRow(index));
											}
											//罫線出力（セルの上部に太線を出力）

											// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
											// 2019/09/30 QQ) 共通テスト対応 UPD START
											//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
											//        intRemoveStartRow, intRemoveStartRow, 0, 12, false, true, false, false);
											//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM,
											//		HSSFColor.BLACK.index,
											//		intRemoveStartRow, intRemoveStartRow, 0, 16, false, true, false,
											//		false);
                                                                                        cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM,
                                                                                                HSSFColor.BLACK.index,
                                                                                                intRemoveStartRow, intRemoveStartRow, 0, MAXCOL, false, true, false,
                                                                                                false);
											// 2019/09/30 QQ) 共通テスト対応 UPD END
											// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END
										}
									}
									//2004.9.27 Add End

									if (bolBookCngFlg == true) {

										if (bolBookClsFlg == true) {
											//WorkBook保存
											boolean bolRet = false;
											// Excelファイル保存
											bolRet = clsBook(workbook, intMaxSheetIndex,
													intBookCngCount, intSaveFlg, outfilelist, UserID, false);
											if (bolRet == false) {
												return errfwrite;
											}
											intBookCngCount++;
											bolBookClsFlg = false;
										}

										// マスタExcel読み込み
										workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
										//									if( workbook.equals(null) ){
										if (workbook == null) {
											return errfread;
										}

										bolBookCngFlg = false;
										intMaxSheetIndex = 0;
									}

									//シート作成
									// シートテンプレートのコピー
									workSheet = workbook.cloneSheet(0);

									// ヘッダ右側に帳票作成日時を表示する
									cm.setHeader(workbook, workSheet);

									// ヘッダー
									//2004.9.28 Update
									//								setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
									//													s24Item.getStrMshmei(),	s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget );
									setHeader(workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(),
											s24Item.getStrGakkomei(),
											s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(),
											strDispTarget, strStKokushiKbn);
									//2004.9.28 Update End

									intMaxSheetIndex++; //シート数カウンタインクリメント
									bolSheetCngFlg = false;

									intRow = intDataStartRow;
									intLoopCnt = 0;

								}
							}
						}
					}
					//2004.9.27 Add End

				}

				/**  データ表示処理  **/
				//評価区分セット
				//				if( !cm.toString(strHyoukaKbn).equals(cm.toString(s24ListBean.getStrHyouka())) ){
				String strHyoka = "";
				switch (Integer.parseInt(s24ListBean.getStrHyouka())) {
				case 1:
					// 2019/09/30 QQ) 共通テスト対応 UPD START
					//strHyoka = "センター";
					strHyoka = "共テ";
					// 2019/09/30 QQ) 共通テスト対応 UPD END
					tmpAddGkbList = addGkbCenterList;

					if (cm.toString(s24ListBean.getStrNtiCd()).equals("1")) {
						tmpAddNtiList = addNtiCntrCenList;
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("C")) {
						tmpAddNtiList = addNtiChukiCenList;
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("D")) {
						tmpAddNtiList = addNtiZenkiCenList;
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("E")) {
						tmpAddNtiList = addNtiKokiCenList;
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("N")) {
						tmpAddNtiList = addNtiShinCenList;
					}
					break;
				case 2:
					strHyoka = "2次・私大";
					tmpAddGkbList = addGkbSecondList;

					if (cm.toString(s24ListBean.getStrNtiCd()).equals("1")) {
						tmpAddNtiList = addNtiCntrSecList;
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("C")) {
						tmpAddNtiList = addNtiChukiSecList;
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("D")) {
						tmpAddNtiList = addNtiZenkiSecList;
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("E")) {
						tmpAddNtiList = addNtiKokiSecList;
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("N")) {
						tmpAddNtiList = addNtiShinSecList;
					}
					break;
				case 3:
					strHyoka = "総合";
					tmpAddGkbList = addGkbTotalList;

					if (cm.toString(s24ListBean.getStrNtiCd()).equals("1")) {
						tmpAddNtiList = addNtiCntrTotList;
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("C")) {
						tmpAddNtiList = addNtiChukiTotList;
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("D")) {
						tmpAddNtiList = addNtiZenkiTotList;
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("E")) {
						tmpAddNtiList = addNtiKokiTotList;
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("N")) {
						tmpAddNtiList = addNtiShinTotList;
					}
					break;
				}
				workCell = cm.setCell(workSheet, workRow, workCell, intRow, 3);
				workCell.setCellValue(strHyoka);

				//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
				cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
						intRow, intRow, 3, 4, false, true, false, false);

				//				}

				//日程セット
				if (!cm.toString(strNitteiCode).equals(cm.toString(s24ListBean.getStrNtiCd()))) {
					workCell = cm.setCell(workSheet, workRow, workCell, intRow, 2);
					workCell.setCellValue(s24ListBean.getStrNittei());
					strNitteiCode = s24ListBean.getStrNtiCd();

					//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
					cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
							intRow, intRow, 2, 4, false, true, false, false);

					if (cm.toString(s24ListBean.getStrNtiCd()).equals("1")) {
						centermei = s24ListBean.getStrNittei();
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("C")) {
						chukimei = s24ListBean.getStrNittei();
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("D")) {
						zenkimei = s24ListBean.getStrNittei();
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("E")) {
						kokimei = s24ListBean.getStrNittei();
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("N")) {
						sinsetumei = s24ListBean.getStrNittei();
					}

				}

				//学部名セット → 大学が変わったときは処理する 2005.05.25
				//				if( !cm.toString(strGakubuCode).equals(cm.toString(s24ListBean.getStrGakubuCd())) ){
				if (!cm.toString(strGakubuCode).equals(cm.toString(s24ListBean.getStrGakubuCd())) ||
						!cm.toString(strDaigakuCodeHoji).equals(cm.toString(s24ListBean.getStrDaigakuCd()))) {
					if (s24ListBean.getStrGakubuMei() != null) {
						workCell = cm.setCell(workSheet, workRow, workCell, intRow, 1);
						workCell.setCellValue(s24ListBean.getStrGakubuMei());
					}
					strGakubuCode = s24ListBean.getStrGakubuCd();
					strGakubuCodeHoji = s24ListBean.getStrGakubuCd();
					strGakubuuMei = s24ListBean.getStrGakubuMei();
					//罫線出力(セルの上部に太線を引く）
					cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
							intRow, intRow, 1, 4, false, true, false, false);

				}

				//大学名セット
				if ((!cm.toString(strDaigakuCode).equals(cm.toString(s24ListBean.getStrDaigakuCd())))
						|| intLoopCnt == 0) {
					if (s24ListBean.getStrDaigakuMei() != null) {
						workCell = cm.setCell(workSheet, workRow, workCell, intRow, 0);
						workCell.setCellValue(s24ListBean.getStrDaigakuMei());
					}
					strDaigakuCode = s24ListBean.getStrDaigakuCd();
					strDaigakuCodeHoji = s24ListBean.getStrDaigakuCd();
					strDaigakuMei = s24ListBean.getStrDaigakuMei();
					//罫線出力(セルの上部に太線を引く）

					// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
					// 2019/09/30 QQ) 共通テスト対応 UPD START
					//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
					//        intRow, intRow, 0, 12, false, true, false, false );
					//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
					//		intRow, intRow, 0, 16, false, true, false, false);
                                        cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                intRow, intRow, 0, MAXCOL, false, true, false, false);
					// 2019/09/30 QQ) 共通テスト対応 UPD END
					// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

				}

				//評価別人数データリストの内容をセット
				boolean ret = setData(workSheet, s24HyoukaNinzuList, tmpAddGkbList, tmpAddNtiList, intRow);
				if (ret != true) {
					return errfdata;
				} else {
					intRow += intNinzuListCnt;
					intLoopCnt++;
				}

				// set Total
				switch (Integer.parseInt(s24ListBean.getStrHyouka())) {
				case 1:
					if (tmpAddGkbList == null) {
						addGkbCenterList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
					} else {
						addGkbCenterList = deepCopy(tmpAddGkbList);
					}

					if (cm.toString(s24ListBean.getStrNtiCd()).equals("1")) {
						if (tmpAddNtiList == null) {
							addNtiCntrCenList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiCntrCenList = deepCopy(tmpAddNtiList);
						}
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("C")) {
						if (tmpAddNtiList == null) {
							addNtiChukiCenList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiChukiCenList = deepCopy(tmpAddNtiList);
						}
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("D")) {
						if (tmpAddNtiList == null) {
							addNtiZenkiCenList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiZenkiCenList = deepCopy(tmpAddNtiList);
						}
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("E")) {
						if (tmpAddNtiList == null) {
							addNtiKokiCenList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiKokiCenList = deepCopy(tmpAddNtiList);
						}
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("N")) {
						if (tmpAddNtiList == null) {
							addNtiShinCenList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiShinCenList = deepCopy(tmpAddNtiList);
						}
					}
					break;
				case 2:
					if (tmpAddGkbList == null) {
						addGkbSecondList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
					} else {
						addGkbSecondList = deepCopy(tmpAddGkbList);
					}

					if (cm.toString(s24ListBean.getStrNtiCd()).equals("1")) {
						if (tmpAddNtiList == null) {
							addNtiCntrSecList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiCntrSecList = deepCopy(tmpAddNtiList);
						}
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("C")) {
						if (tmpAddNtiList == null) {
							addNtiChukiSecList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiChukiSecList = deepCopy(tmpAddNtiList);
						}
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("D")) {
						if (tmpAddNtiList == null) {
							addNtiZenkiSecList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiZenkiSecList = deepCopy(tmpAddNtiList);
						}
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("E")) {
						if (tmpAddNtiList == null) {
							addNtiKokiSecList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiKokiSecList = deepCopy(tmpAddNtiList);
						}
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("N")) {
						if (tmpAddNtiList == null) {
							addNtiShinSecList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiShinSecList = deepCopy(tmpAddNtiList);
						}
					}
					break;
				case 3:
					if (tmpAddGkbList == null) {
						addGkbTotalList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
					} else {
						addGkbTotalList = deepCopy(tmpAddGkbList);
					}

					if (cm.toString(s24ListBean.getStrNtiCd()).equals("1")) {
						if (tmpAddNtiList == null) {
							addNtiCntrTotList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiCntrTotList = deepCopy(tmpAddNtiList);
						}
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("C")) {
						if (tmpAddNtiList == null) {
							addNtiChukiTotList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiChukiTotList = deepCopy(tmpAddNtiList);
						}
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("D")) {
						if (tmpAddNtiList == null) {
							addNtiZenkiTotList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiZenkiTotList = deepCopy(tmpAddNtiList);
						}
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("E")) {
						if (tmpAddNtiList == null) {
							addNtiKokiTotList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiKokiTotList = deepCopy(tmpAddNtiList);
						}
					} else if (cm.toString(s24ListBean.getStrNtiCd()).equals("N")) {
						if (tmpAddNtiList == null) {
							addNtiShinTotList = deepCopy(s24ListBean.getS24HyoukaNinzuList());
						} else {
							addNtiShinTotList = deepCopy(tmpAddNtiList);
						}
					}
					break;
				}
				tmpAddGkbList = null;
				tmpAddNtiList = null;

				if (intLoopCnt >= intSheetCngCount) {
					//改シート
					//罫線出力（セルの上部に太線を出力）

				        // 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
					// 2019/09/30 QQ) 共通テスト対応 UPD START
					//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
					//        intRow, intRow, 0, 12, false, true, false, false);
					//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
					//		intRow, intRow, 0, 16, false, true, false, false);
                                        cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                intRow, intRow, 0, MAXCOL, false, true, false, false);
					// 2019/09/30 QQ) 共通テスト対応 UPD END
					// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

					//strDaigakuCode = "";
					strGakubuCode = "";
					strNitteiCode = "";
					bolSheetCngFlg = true;
					if (intMaxSheetIndex >= intMaxSheetSr) {
						//改ファイル
						bolBookCngFlg = true;
						bolBookClsFlg = true;
					}
				}

			}

			// 最後に各合計の書き込み
			// データセット
			boolean firstFlg = true;
			for (int a = 0; a < 3; a++) {
				// 改ファイル処理
				if (bolSheetCngFlg == true) {
					if (intMaxSheetIndex >= intMaxSheetSr) {
						//改ファイル
						bolBookCngFlg = true;
						bolBookClsFlg = true;
					}

					if (bolSheetCngFlg == true) {

						//2004.9.27 Add
						if (workSheet != null) {
							int intRemoveStartRow = (intSheetCngCount * intNinzuListCnt) + intDataStartRow;
							if (intMaxRow + intDataStartRow > intRemoveStartRow) {
								for (int index = intRemoveStartRow; index < intMaxRow + intDataStartRow; index++) {
									//行削除処理
									workSheet.removeRow(workSheet.getRow(index));
								}
								//罫線出力（セルの上部に太線を出力）

								// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
								// 2019/09/30 QQ) 共通テスト対応 UPD START
								//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
								//        intRemoveStartRow, intRemoveStartRow, 0, 12, false, true, false, false);
								//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//		intRemoveStartRow, intRemoveStartRow, 0, 16, false, true, false, false);
                                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                                        intRemoveStartRow, intRemoveStartRow, 0, MAXCOL, false, true, false, false);
								// 2019/09/30 QQ) 共通テスト対応 UPD END
								// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END
							}
						}
						//2004.9.27 Add End

						if (bolBookCngFlg == true) {

							if (bolBookClsFlg == true) {
								//WorkBook保存
								boolean bolRet = false;
								bolRet = clsBook(workbook, intMaxSheetIndex,
										intBookCngCount, intSaveFlg, outfilelist, UserID, false);
								if (bolRet == false) {
									return errfwrite;
								}
								intBookCngCount++;
								bolBookClsFlg = false;
							}

							// マスタExcel読み込み
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							//							if( workbook.equals(null) ){
							if (workbook == null) {
								return errfread;
							}

							bolBookCngFlg = false;
							intMaxSheetIndex = 0;
						}

						//シート作成
						// シートテンプレートのコピー
						workSheet = workbook.cloneSheet(0);

						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);

						// ヘッダー
						//2004.9.28 Update
						//						setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
						//											s24Item.getStrMshmei(),	s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget );
						setHeader(workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(),
								s24Item.getStrGakkomei(),
								s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget,
								strStKokushiKbn);
						//2004.9.28 Update End

						intMaxSheetIndex++; //シート数カウンタインクリメント
						bolSheetCngFlg = false;

						intRow = intDataStartRow;
						intLoopCnt = 0;

					}
				}

				String strHyokaKbn = "";
				if (a == 0) {
					tmpAddGkbList = addGkbCenterList;
					tmpAddDgkList = addDgkCenterList;
					// 2019/09/30 QQ) 共通テスト対応 UPD START
					//strHyokaKbn = "センター";
					strHyokaKbn = "共テ";
					// 2019/09/30 QQ) 共通テスト対応 UPD END
				} else if (a == 1) {
					tmpAddGkbList = addGkbSecondList;
					tmpAddDgkList = addDgkSecondList;
					strHyokaKbn = "2次・私大";
				} else if (a == 2) {
					tmpAddGkbList = addGkbTotalList;
					tmpAddDgkList = addDgkTotalList;
					strHyokaKbn = "総合";
				} else {
					break;
				}

				if (tmpAddGkbList != null) {
					//評価区分セット
					workCell = cm.setCell(workSheet, workRow, workCell, intRow, 3);
					workCell.setCellValue(strHyokaKbn);
					//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
					cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
							intRow, intRow, 3, 4, false, true, false, false);

					//日程セット
					if ((firstFlg) || (intLoopCnt == 0)) {
						workCell = cm.setCell(workSheet, workRow, workCell, intRow, 2);
						//2004.9.28 Update
						//						workCell.setCellValue( "小計" );
						workCell.setCellValue("学部計");
						//2004.9.28 Update End
						//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
								intRow, intRow, 2, 4, false, true, false, false);

						firstFlg = false;
					}

					//学部名セット
					if ((intLoopCnt == 0)) {
						if (strGakubuuMei != null) {
							workCell = cm.setCell(workSheet, workRow, workCell, intRow, 1);
							workCell.setCellValue(strGakubuuMei);
						}
						//罫線出力(セルの上部に太線を引く）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
								intRow, intRow, 1, 4, false, true, false, false);

					}

					//大学名セット
					if ((intLoopCnt == 0)) {
						if (strDaigakuMei != null) {
							workCell = cm.setCell(workSheet, workRow, workCell, intRow, 0);
							workCell.setCellValue(strDaigakuMei);
						}
						//罫線出力(セルの上部に太線を引く）

						// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
						// 2019/09/30 QQ) 共通テスト対応 UPD START
						//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
						//        intRow, intRow, 0, 12, false, true, false, false );
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
						//		intRow, intRow, 0, 16, false, true, false, false);
                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                        intRow, intRow, 0, MAXCOL, false, true, false, false);
						// 2019/09/30 QQ) 共通テスト対応 UPD END
						// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

					}

					//評価別人数データリストの内容をセット
					boolean ret = setData(workSheet, tmpAddGkbList, tmpAddDgkList, null, intRow);
					if (ret != true) {
						return errfdata;
					} else {
						intRow += intNinzuListCnt;
						intLoopCnt++;
					}

					// set Total
					if (a == 0) {
						if (tmpAddDgkList == null) {
							addDgkCenterList = deepCopy(tmpAddGkbList);
						} else {
							addDgkCenterList = deepCopy(tmpAddDgkList);
						}
					} else if (a == 1) {
						if (tmpAddDgkList == null) {
							addDgkSecondList = deepCopy(tmpAddGkbList);
						} else {
							addDgkSecondList = deepCopy(tmpAddDgkList);
						}
					} else if (a == 2) {
						if (tmpAddDgkList == null) {
							addDgkTotalList = deepCopy(tmpAddGkbList);
						} else {
							addDgkTotalList = deepCopy(tmpAddDgkList);
						}
					}
				}

				// 改シート・改ブック処理
				if ((intLoopCnt >= intSheetCngCount) &&
						(((a == 0) && (addGkbSecondList != null || addGkbTotalList != null))
								|| (a == 1 && addGkbTotalList != null) ||
								((addDgkCenterList != null) || (addDgkSecondList != null)
										|| (addDgkTotalList != null)))) {
					//改シート
					//罫線出力（セルの上部に太線を出力）

				        // 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
					// 2019/09/30 QQ) 共通テスト対応 UPD START
					//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
					//        intRow, intRow, 0, 12, false, true, false, false);
					//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
					//		intRow, intRow, 0, 16, false, true, false, false);
                                        cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                intRow, intRow, 0, MAXCOL, false, true, false, false);
					// 2019/09/30 QQ) 共通テスト対応 UPD END
					// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

					//strDaigakuCode = "";
					strGakubuCode = "";
					strNitteiCode = "";
					bolSheetCngFlg = true;
				}
			}
			addGkbCenterList = null;
			addGkbSecondList = null;
			addGkbTotalList = null;
			tmpAddGkbList = null;

			// 大学計セット

			ArrayList tmpCenterList = null;
			ArrayList tmpSecondList = null;
			ArrayList tmpTotalList = null;

			for (int a = 0; a < 6; a++) {
				String komokumei = "";
				if (a == 0) {
					komokumei = centermei;

					tmpCenterList = addNtiCntrCenList;
					tmpSecondList = addNtiCntrSecList;
					tmpTotalList = addNtiCntrTotList;
				} else if (a == 1) {
					komokumei = chukimei;

					tmpCenterList = addNtiChukiCenList;
					tmpSecondList = addNtiChukiSecList;
					tmpTotalList = addNtiChukiTotList;
				} else if (a == 2) {
					komokumei = zenkimei;

					tmpCenterList = addNtiZenkiCenList;
					tmpSecondList = addNtiZenkiSecList;
					tmpTotalList = addNtiZenkiTotList;
				} else if (a == 3) {
					komokumei = kokimei;

					tmpCenterList = addNtiKokiCenList;
					tmpSecondList = addNtiKokiSecList;
					tmpTotalList = addNtiKokiTotList;
				} else if (a == 4) {
					komokumei = sinsetumei;

					tmpCenterList = addNtiShinCenList;
					tmpSecondList = addNtiShinSecList;
					tmpTotalList = addNtiShinTotList;
				} else if (a == 5) {
					komokumei = "大学";

					tmpCenterList = addDgkCenterList;
					tmpSecondList = addDgkSecondList;
					tmpTotalList = addDgkTotalList;
				} else {
					break;
				}

				// データセット
				firstFlg = true;
				for (int b = 0; b < 3; b++) {
					String strHyokaKbn = "";
					if (b == 0) {
						tmpAddDgkList = tmpCenterList;
						// 2019/09/30 QQ) 共通テスト対応 UPD START
						//strHyokaKbn = "センター";
						strHyokaKbn = "共テ";
						// 2019/09/30 QQ) 共通テスト対応 UPD END
					} else if (b == 1) {
						tmpAddDgkList = tmpSecondList;
						strHyokaKbn = "2次・私大";
					} else if (b == 2) {
						tmpAddDgkList = tmpTotalList;
						strHyokaKbn = "総合";
					} else {
						break;
					}

					if (tmpAddDgkList != null && bolSheetCngFlg == true) {
						if (intMaxSheetIndex >= intMaxSheetSr) {
							//改ファイル
							bolBookCngFlg = true;
							bolBookClsFlg = true;
						}

						if (bolSheetCngFlg == true) {

							//2004.9.27 Add
							if (workSheet != null) {
								int intRemoveStartRow = (intSheetCngCount * intNinzuListCnt) + intDataStartRow;
								if (intMaxRow + intDataStartRow > intRemoveStartRow) {
									for (int index = intRemoveStartRow; index < intMaxRow + intDataStartRow; index++) {
										//行削除処理
										workSheet.removeRow(workSheet.getRow(index));
									}
									//罫線出力（セルの上部に太線を出力）

									// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
									// 2019/09/30 QQ) 共通テスト対応 UPD START
									//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
									//        intRemoveStartRow, intRemoveStartRow, 0, 12, false, true, false, false);
									//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
									//		intRemoveStartRow, intRemoveStartRow, 0, 16, false, true, false, false);
                                                                        cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                                                intRemoveStartRow, intRemoveStartRow, 0, MAXCOL, false, true, false, false);
									// 2019/09/30 QQ) 共通テスト対応 UPD END
									// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END
								}
							}
							//2004.9.27 Add End

							if (bolBookCngFlg == true) {

								if (bolBookClsFlg == true) {
									//WorkBook保存
									boolean bolRet = false;
									bolRet = clsBook(workbook, intMaxSheetIndex,
											intBookCngCount, intSaveFlg, outfilelist, UserID, false);
									if (bolRet == false) {
										return errfwrite;
									}
									intBookCngCount++;
									bolBookClsFlg = false;
								}

								// マスタExcel読み込み
								workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
								//								if( workbook.equals(null) ){
								if (workbook == null) {
									return errfread;
								}

								bolBookCngFlg = false;
								intMaxSheetIndex = 0;
							}

							//シート作成
							// シートテンプレートのコピー
							workSheet = workbook.cloneSheet(0);

							// ヘッダ右側に帳票作成日時を表示する
							cm.setHeader(workbook, workSheet);

							// ヘッダー
							//2004.9.28 Update
							//							setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
							//												s24Item.getStrMshmei(),	s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget );
							setHeader(workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(),
									s24Item.getStrGakkomei(),
									s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(),
									strDispTarget, strStKokushiKbn);
							//2004.9.28 Update End

							intMaxSheetIndex++; //シート数カウンタインクリメント
							bolSheetCngFlg = false;

							intRow = intDataStartRow;
							intLoopCnt = 0;

						}
					}

					if (tmpAddDgkList != null) {
						//評価区分セット
						workCell = cm.setCell(workSheet, workRow, workCell, intRow, 3);
						workCell.setCellValue(strHyokaKbn);
						//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
								intRow, intRow, 3, 4, false, true, false, false);

						//学部名セット
						if ((firstFlg) || (intLoopCnt == 0)) {
							workCell = cm.setCell(workSheet, workRow, workCell, intRow, 1);
							workCell.setCellValue(cm.toString(komokumei) + "計");
							//罫線出力(セルの上部に太線を引く）
							cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
									intRow, intRow, 1, 4, false, true, false, false);

							firstFlg = false;
						}

						//大学名セット
						if ((intLoopCnt == 0)) {
							if (strDaigakuMei != null) {
								workCell = cm.setCell(workSheet, workRow, workCell, intRow, 0);
								workCell.setCellValue(strDaigakuMei);
							}
							//罫線出力(セルの上部に太線を引く）

							// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
							// 2019/09/30 QQ) 共通テスト対応 UPD START
							//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
							//        intRow, intRow, 0, 12, false, true, false, false );
							//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
							//		intRow, intRow, 0, 16, false, true, false, false);
                                                        cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                                intRow, intRow, 0, MAXCOL, false, true, false, false);
							// 2019/09/30 QQ) 共通テスト対応 UPD END
							// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

						}

						//評価別人数データリストの内容をセット
						boolean ret = setData(workSheet, tmpAddDgkList, null, null, intRow);
						if (ret != true) {
							return errfdata;
						} else {
							intRow += intNinzuListCnt;
							intLoopCnt++;
						}
					}

					// 改シート・改ブック処理
					if (intLoopCnt >= intSheetCngCount) {
						//改シート
						//罫線出力（セルの上部に太線を出力）

					        // 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
						// 2019/09/30 QQ) 共通テスト対応 UPD START
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
						//        intRow, intRow, 0, 12, false, true, false, false);
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
						//		intRow, intRow, 0, 16, false, true, false, false);
                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                        intRow, intRow, 0, MAXCOL, false, true, false, false);
						// 2019/09/30 QQ) 共通テスト対応 UPD END
						// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

						//strDaigakuCode = "";
						strGakubuCode = "";
						strNitteiCode = "";
						bolSheetCngFlg = true;
					}
				}
			}

			//2004.9.28 Update
			//			if(bolBookCngFlg == false){
			//
			//				//罫線出力（セルの上部に太線を出力）
			//				cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
			//													intRow, intRow, 0, 12, false, true, false, false);
			//				// Excelファイル保存
			//				boolean bolRet = false;
			//				bolRet = clsBook( workbook, workSheet, workCell, workRow, intMaxSheetIndex,
			//									intBookCngCount, intSaveFlg, outfilelist, UserID, true );
			//				if( bolRet == false ){
			//					return errfwrite;
			//
			//				}
			//			}
			if (workbook != null) {

				if (workSheet != null) {
					int intRemoveStartRow = (intSheetCngCount * intNinzuListCnt) + intDataStartRow;
					if (intMaxRow + intDataStartRow > intRemoveStartRow) {
						for (int index = intRemoveStartRow; index < intMaxRow + intDataStartRow; index++) {
							//行削除処理
							workSheet.removeRow(workSheet.getRow(index));
						}
						//罫線出力（セルの上部に太線を出力）

						// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
						// 2019/09/30 QQ) 共通テスト対応 UPD START
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
						//        intRemoveStartRow, intRemoveStartRow, 0, 12, false, true, false, false);
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
						//		intRemoveStartRow, intRemoveStartRow, 0, 16, false, true, false, false);
                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                        intRemoveStartRow, intRemoveStartRow, 0, MAXCOL, false, true, false, false);
						// 2019/09/30 QQ) 共通テスト対応 UPD END
						// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END
					}
				}
				//罫線出力（セルの上部に太線を出力）

				// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
				// 2019/09/30 QQ) 共通テスト対応 UPD START
				//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
				//        intRow, intRow, 0, 12, false, true, false, false);
				//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
				//		intRow, intRow, 0, 16, false, true, false, false);
                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                        intRow, intRow, 0, MAXCOL, false, true, false, false);
				// 2019/09/30 QQ) 共通テスト対応 UPD END
				// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END
			}
			// Excelファイル保存
			//2004.10.25Add
			if (intDataCnt == 0) {
				intMaxSheetIndex = 1;
			}
			//2004.10.25AddEnd

			boolean bolRet = false;
			bolRet = clsBook(workbook, intMaxSheetIndex,
					intBookCngCount, intSaveFlg, outfilelist, UserID, true);
			if (bolRet == false) {
				return errfwrite;

			}
			//2004.9.28 Update End

		} catch (Exception e) {
			return errfdata;
		}

		return noerror;
	}

	/**
	 * Excelファイルを閉じる。
	 * @param workbook
	 * @param workSheet
	 * @param workCell
	 * @param workRow
	 * @param intMaxSheetIndex
	 * @param intBookCngCount
	 * @param intSaveFlg
	 * @param outfilelist
	 * @param UserID
	 * @param lastFlg
	 * @return
	 * @throws Exception
	 */
	private boolean clsBook(HSSFWorkbook workbook, int intMaxSheetIndex,
			int intBookCngCount, int intSaveFlg, ArrayList outfilelist,
			String UserID, boolean lastFlg) throws Exception {
		try {
			//WorkBook保存

			boolean bolRet = false;
			// Excelファイル保存
			if (lastFlg == false) {
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount + 1, masterfile,
						intMaxSheetIndex);
			} else {
				if (intBookCngCount > 0) {
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount + 1, masterfile,
							intMaxSheetIndex);
				} else {
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}
			}

			return bolRet;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Excelのタイトルを書き込む。
	 * @param workbook
	 * @param workSheet
	 * @param workCell
	 * @param workRow
	 * @param secuFlg
	 * @param gakkomei
	 * @param mshmei
	 * @param mshDate
	 * @param mshCd
	 * @param dispTarget
	 * @throws Exception
	 */
	//2004.9.28 Update
	//	private void setHeader( HSSFWorkbook workbook, HSSFSheet workSheet, HSSFCell workCell, HSSFRow workRow,
	//							int secuFlg, String gakkomei, String mshmei, String mshDate, String mshCd, String dispTarget )
	//							throws Exception{
	private void setHeader(HSSFWorkbook workbook, HSSFSheet workSheet, HSSFCell workCell, HSSFRow workRow,
			int secuFlg, String gakkomei, String mshmei, String mshDate, String mshCd, String dispTarget,
			String KokushiKbn)
			throws Exception {
		//2004.9.28 Update End
		try {

			// セキュリティスタンプセット

	                // 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
			//2019/10/03 QQ)Oosaki 共通テスト対応 UPD START
			//// 2019/09/30 QQ) 共通テスト対応 UPD START
			////String secuMes = cm.setSecurity( workbook, workSheet, secuFlg, 10, 12 );
			////workCell = cm.setCell( workSheet, workRow, workCell, 0, 10 );
			//String secuMes = cm.setSecurity(workbook, workSheet, secuFlg, 11, 16);
			//workCell = cm.setCell(workSheet, workRow, workCell, 0, 11);
			//// 2019/09/30 QQ) 共通テスト対応 UPD END
			//String secuMes = cm.setSecurity(workbook, workSheet, secuFlg, 14, 16);
			//workCell = cm.setCell(workSheet, workRow, workCell, 0, 14);
			String secuMes = cm.setSecurity(workbook, workSheet, secuFlg, 9, 11);
                        workCell = cm.setCell(workSheet, workRow, workCell, 0, 9);
			//2019/10/03 QQ)Oosaki 共通テスト対応 UPD END
			// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END
			workCell.setCellValue(secuMes);

			// 学校名セット
			workCell = cm.setCell(workSheet, workRow, workCell, 2, 0);
			workCell.setCellValue("学校名　：" + cm.toString(gakkomei));

			// 模試月取得
			String moshi = cm.setTaisyouMoshi(mshDate);
			// 対象模試セット
			workCell = cm.setCell(workSheet, workRow, workCell, 3, 0);
			workCell.setCellValue(cm.getTargetExamLabel() + "：" + cm.toString(mshmei) + moshi);

			//表示対象
			workCell = cm.setCell(workSheet, workRow, workCell, 4, 0);
			workCell.setCellValue("表示対象：" + dispTarget);

			//表示対象
			// 2019/09/30 QQ) 共通テスト対応 UPD START

			//workCell = cm.setCell( workSheet, workRow, workCell, 6, 8 );
			//workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_A);
			//workCell = cm.setCell( workSheet, workRow, workCell, 6, 9 );
			//workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_B);
			//workCell = cm.setCell( workSheet, workRow, workCell, 6, 10 );
			//workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_C);
			//workCell = cm.setCell( workSheet, workRow, workCell, 6, 11 );
			//workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_D);
			//workCell = cm.setCell( workSheet, workRow, workCell, 6, 12 );
			//workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_E);

			workCell = cm.setCell(workSheet, workRow, workCell, 6, 7);
			workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_A);
			// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
			//workCell = cm.setCell(workSheet, workRow, workCell, 6, 9);
			workCell = cm.setCell(workSheet, workRow, workCell, 6, 8);
			workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_B);
			//workCell = cm.setCell(workSheet, workRow, workCell, 6, 11);
			workCell = cm.setCell(workSheet, workRow, workCell, 6, 9);
			workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_C);
			//workCell = cm.setCell(workSheet, workRow, workCell, 6, 13);
			workCell = cm.setCell(workSheet, workRow, workCell, 6, 10);
			workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_D);
			//workCell = cm.setCell(workSheet, workRow, workCell, 6, 15);
			workCell = cm.setCell(workSheet, workRow, workCell, 6, 11);
			workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_E);
			// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

			// 2019/09/30 QQ) 共通テスト対応 UPD END

			//2019/10/03 QQ)Oosaki 共通テスト対応 DEL START
			//// 2019/09/30 QQ) 共通テスト対応 UPD START
			//
			////2004.9.28 Add
			////国私区分
			////if( KokushiKbn.equals("国公立大")){
			////	workCell = cm.setCell( workSheet, workRow, workCell, 6, 7 );
			////	workCell.setCellValue( "出願予定" );
			////}
			////else{
			////	workCell = cm.setCell( workSheet, workRow, workCell, 6, 7 );
			////	workCell.setCellValue( "第1志望" );
			////}
			////2004.9.28 Add End
			//workCell = cm.setCell(workSheet, workRow, workCell, 6, 6);
			//workCell.setCellValue("第1志望");
			//
			//// 2019/09/30 QQ) 共通テスト対応 UPD END
			//2019/10/03 QQ)Oosaki 共通テスト対応 DEL END

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * ListのDeepCopyを行う。
	 * @param inList
	 * @return
	 */
	private ArrayList deepCopy(ArrayList inList) throws Exception {
		try {
			ArrayList outList = new ArrayList(5);

			if ((inList == null) || (inList.size() == 0)) {
				return null;
			} else {
				for (int a = 0; a < inList.size(); a++) {
					S24HyoukaNinzuListBean inS24HyoukaNinzuListBean = new S24HyoukaNinzuListBean();
					S24HyoukaNinzuListBean outS24HyoukaNinzuListBean = new S24HyoukaNinzuListBean();
					inS24HyoukaNinzuListBean = (S24HyoukaNinzuListBean) inList.get(a);

					// データの移し変え
					outS24HyoukaNinzuListBean.setFloHensaAll(inS24HyoukaNinzuListBean.getFloHensaAll());
					outS24HyoukaNinzuListBean.setFloHensaHome(inS24HyoukaNinzuListBean.getFloHensaHome());
					outS24HyoukaNinzuListBean.setFloTokuritsuAll(inS24HyoukaNinzuListBean.getFloTokuritsuAll());
					outS24HyoukaNinzuListBean.setFloTokuritsuHome(inS24HyoukaNinzuListBean.getFloTokuritsuHome());
					outS24HyoukaNinzuListBean.setIntDai1shibo(inS24HyoukaNinzuListBean.getIntDai1shibo());
					outS24HyoukaNinzuListBean.setIntHyoukaA(inS24HyoukaNinzuListBean.getIntHyoukaA());
					outS24HyoukaNinzuListBean.setIntHyoukaB(inS24HyoukaNinzuListBean.getIntHyoukaB());
					outS24HyoukaNinzuListBean.setIntHyoukaC(inS24HyoukaNinzuListBean.getIntHyoukaC());
					outS24HyoukaNinzuListBean.setIntHyoukaD(inS24HyoukaNinzuListBean.getIntHyoukaD());
					outS24HyoukaNinzuListBean.setIntHyoukaE(inS24HyoukaNinzuListBean.getIntHyoukaE());

					// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 DEL START
					// 2019/09/30 QQ) 共通テスト対応 ADD START
//					outS24HyoukaNinzuListBean.setIntHyoukaA_Hukumu(inS24HyoukaNinzuListBean.getIntHyoukaA_Hukumu());
//					outS24HyoukaNinzuListBean.setIntHyoukaB_Hukumu(inS24HyoukaNinzuListBean.getIntHyoukaB_Hukumu());
//					outS24HyoukaNinzuListBean.setIntHyoukaC_Hukumu(inS24HyoukaNinzuListBean.getIntHyoukaC_Hukumu());
//					outS24HyoukaNinzuListBean.setIntHyoukaD_Hukumu(inS24HyoukaNinzuListBean.getIntHyoukaD_Hukumu());
//					outS24HyoukaNinzuListBean.setIntHyoukaE_Hukumu(inS24HyoukaNinzuListBean.getIntHyoukaE_Hukumu());
					// 2019/09/30 QQ) 共通テスト対応 ADD END
					// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 DEL END

					outS24HyoukaNinzuListBean.setIntSoshiboAll(inS24HyoukaNinzuListBean.getIntSoshiboAll());
					outS24HyoukaNinzuListBean.setIntSoshiboHome(inS24HyoukaNinzuListBean.getIntSoshiboHome());
					outS24HyoukaNinzuListBean.setStrNendo(inS24HyoukaNinzuListBean.getStrNendo());

					outList.add(outS24HyoukaNinzuListBean);
				}

				return outList;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/*
	 * 	Excel編集
	 * 		HSSFWorkbook 	workbook: Excelワークブック
	 * 		ArrayList 		s24HyoukaNinzuList:評価別人数リスト
	 * 		int				intRow:開始行
	 * 		戻り値: なし
	 */
	private boolean setData(HSSFSheet workSheet, ArrayList s24HyoukaNinzuList, ArrayList addList, ArrayList addNtiList,
			int intRow) {

		HSSFRow workRow = null;
		HSSFCell workCell = null;
		int rownum = 0;
		int row = intRow;
		boolean nullFlg = true;
		boolean nullNtiFlg = true;

		// 基本ファイルを読込む
		S24HyoukaNinzuListBean s24HyoukaNinzuListBean = new S24HyoukaNinzuListBean();
		S24HyoukaNinzuListBean s24AddNinzuListBean = new S24HyoukaNinzuListBean();
		S24HyoukaNinzuListBean s24AddNtiNinzuListBean = new S24HyoukaNinzuListBean();

		try {
			if (addList != null) {
				nullFlg = false;
			}
			if (addNtiList != null) {
				nullNtiFlg = false;
			}

			Iterator itr = s24HyoukaNinzuList.iterator();

			while (itr.hasNext()) {

				//データリストより各項目を取得する
				s24HyoukaNinzuListBean = (S24HyoukaNinzuListBean) itr.next();
				if (nullFlg == false) {
					s24AddNinzuListBean = (S24HyoukaNinzuListBean) addList.get(rownum);
				}
				if (nullNtiFlg == false) {
					s24AddNtiNinzuListBean = (S24HyoukaNinzuListBean) addNtiList.get(rownum);
				}

				//模試年度
				if (s24HyoukaNinzuListBean.getStrNendo() != null) {
					workCell = cm.setCell(workSheet, workRow, workCell, row, 4);
					workCell.setCellValue(s24HyoukaNinzuListBean.getStrNendo() + "年度");
				}

				//全国総志望者数
				// 2019/09/30 QQ) 共通テスト対応 DEL START
				//if( s24HyoukaNinzuListBean.getIntSoshiboAll() != -999 ){
				//	workCell = cm.setCell( workSheet, workRow, workCell, row, 5);
				//	workCell.setCellValue( s24HyoukaNinzuListBean.getIntSoshiboAll() );

				//	if(nullFlg==false){
				//		if(s24AddNinzuListBean.getIntSoshiboAll()!=-999.0){
				//			s24AddNinzuListBean.setIntSoshiboAll(s24AddNinzuListBean.getIntSoshiboAll() + s24HyoukaNinzuListBean.getIntSoshiboAll());
				//		}else{
				//			s24AddNinzuListBean.setIntSoshiboAll(s24HyoukaNinzuListBean.getIntSoshiboAll());
				//		}
				//	}
				//	if(nullNtiFlg==false){
				//		if(s24AddNtiNinzuListBean.getIntSoshiboAll()!=-999.0){
				//			s24AddNtiNinzuListBean.setIntSoshiboAll(s24AddNtiNinzuListBean.getIntSoshiboAll() + s24HyoukaNinzuListBean.getIntSoshiboAll());
				//		}else{
				//			s24AddNtiNinzuListBean.setIntSoshiboAll(s24HyoukaNinzuListBean.getIntSoshiboAll());
				//		}
				//	}
				//}
				// 2019/09/30 QQ) 共通テスト対応 DEL END

				//志望者数（総志望）
				if (s24HyoukaNinzuListBean.getIntSoshiboHome() != -999) {
					// 2019/09/30 共通テスト対応 UPD START
					//workCell = cm.setCell( workSheet, workRow, workCell, row, 6);
					workCell = cm.setCell(workSheet, workRow, workCell, row, 5);
					// 2019/09/30 共通テスト対応 UPD END
					workCell.setCellValue(s24HyoukaNinzuListBean.getIntSoshiboHome());

					if (nullFlg == false) {
						if (s24AddNinzuListBean.getIntSoshiboHome() != -999.0) {
							s24AddNinzuListBean.setIntSoshiboHome(s24AddNinzuListBean.getIntSoshiboHome()
									+ s24HyoukaNinzuListBean.getIntSoshiboHome());
						} else {
							s24AddNinzuListBean.setIntSoshiboHome(s24HyoukaNinzuListBean.getIntSoshiboHome());
						}
					}
					if (nullNtiFlg == false) {
						if (s24AddNtiNinzuListBean.getIntSoshiboHome() != -999.0) {
							s24AddNtiNinzuListBean.setIntSoshiboHome(s24AddNtiNinzuListBean.getIntSoshiboHome()
									+ s24HyoukaNinzuListBean.getIntSoshiboHome());
						} else {
							s24AddNtiNinzuListBean.setIntSoshiboHome(s24HyoukaNinzuListBean.getIntSoshiboHome());
						}
					}
				}

				//志望者数（第一志望）
				if (s24HyoukaNinzuListBean.getIntDai1shibo() != -999) {
					// 2019/09/30 QQ) 共通テスト対応 UPD START
					//workCell = cm.setCell( workSheet, workRow, workCell, row, 7);
					workCell = cm.setCell(workSheet, workRow, workCell, row, 6);
					// 2019/09/30 QQ) 共通テスト対応 UPD END
					workCell.setCellValue(s24HyoukaNinzuListBean.getIntDai1shibo());

					if (nullFlg == false) {
						if (s24AddNinzuListBean.getIntDai1shibo() != -999.0) {
							s24AddNinzuListBean.setIntDai1shibo(
									s24AddNinzuListBean.getIntDai1shibo() + s24HyoukaNinzuListBean.getIntDai1shibo());
						} else {
							s24AddNinzuListBean.setIntDai1shibo(s24HyoukaNinzuListBean.getIntDai1shibo());
						}
					}
					if (nullNtiFlg == false) {
						if (s24AddNtiNinzuListBean.getIntDai1shibo() != -999.0) {
							s24AddNtiNinzuListBean.setIntDai1shibo(s24AddNtiNinzuListBean.getIntDai1shibo()
									+ s24HyoukaNinzuListBean.getIntDai1shibo());
						} else {
							s24AddNtiNinzuListBean.setIntDai1shibo(s24HyoukaNinzuListBean.getIntDai1shibo());
						}
					}
				}

				//評価別人数
				if (s24HyoukaNinzuListBean.getIntHyoukaA() != -999) {
					// 2019/09/30 QQ) 共通テスト対応 UPD START
					//workCell = cm.setCell( workSheet, workRow, workCell, row, 8 );
					workCell = cm.setCell(workSheet, workRow, workCell, row, 7);
					// 2019/09/30 QQ) 共通テスト対応 UPD END
					workCell.setCellValue(s24HyoukaNinzuListBean.getIntHyoukaA());

					if (nullFlg == false) {
						if (s24AddNinzuListBean.getIntHyoukaA() != -999.0) {
							s24AddNinzuListBean.setIntHyoukaA(
									s24AddNinzuListBean.getIntHyoukaA() + s24HyoukaNinzuListBean.getIntHyoukaA());
						} else {
							s24AddNinzuListBean.setIntHyoukaA(s24HyoukaNinzuListBean.getIntHyoukaA());
						}
					}
					if (nullNtiFlg == false) {
						if (s24AddNtiNinzuListBean.getIntHyoukaA() != -999.0) {
							s24AddNtiNinzuListBean.setIntHyoukaA(
									s24AddNtiNinzuListBean.getIntHyoukaA() + s24HyoukaNinzuListBean.getIntHyoukaA());
						} else {
							s24AddNtiNinzuListBean.setIntHyoukaA(s24HyoukaNinzuListBean.getIntHyoukaA());
						}
					}
				}

				// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 DEL START
				// 2019/09/30 QQ) 共通テスト対応 ADD START
				// A 含む
//				if (s24HyoukaNinzuListBean.getIntHyoukaA_Hukumu() != -999) {
//					workCell = cm.setCell(workSheet, workRow, workCell, row, 8);
//					workCell.setCellValue(s24HyoukaNinzuListBean.getIntHyoukaA_Hukumu());
//
//					if (nullFlg == false) {
//						if (s24AddNinzuListBean.getIntHyoukaA_Hukumu() != -999.0) {
//							s24AddNinzuListBean.setIntHyoukaA_Hukumu(s24AddNinzuListBean.getIntHyoukaA_Hukumu()
//									+ s24HyoukaNinzuListBean.getIntHyoukaA_Hukumu());
//						} else {
//							s24AddNinzuListBean.setIntHyoukaA_Hukumu(s24HyoukaNinzuListBean.getIntHyoukaA_Hukumu());
//						}
//					}
//					if (nullNtiFlg == false) {
//						if (s24AddNtiNinzuListBean.getIntHyoukaA_Hukumu() != -999.0) {
//							s24AddNtiNinzuListBean.setIntHyoukaA_Hukumu(s24AddNtiNinzuListBean.getIntHyoukaA_Hukumu()
//									+ s24HyoukaNinzuListBean.getIntHyoukaA_Hukumu());
//						} else {
//							s24AddNtiNinzuListBean.setIntHyoukaA_Hukumu(s24HyoukaNinzuListBean.getIntHyoukaA_Hukumu());
//						}
//					}
//				}
				// 2019/09/30 QQ) 共通テスト対応 ADD END
				// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 DEL END

				if (s24HyoukaNinzuListBean.getIntHyoukaB() != -999) {
				        // 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
					//workCell = cm.setCell(workSheet, workRow, workCell, row, 9);
					workCell = cm.setCell(workSheet, workRow, workCell, row, 8);
					// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END
					workCell.setCellValue(s24HyoukaNinzuListBean.getIntHyoukaB());

					if (nullFlg == false) {
						if (s24AddNinzuListBean.getIntHyoukaB() != -999.0) {
							s24AddNinzuListBean.setIntHyoukaB(
									s24AddNinzuListBean.getIntHyoukaB() + s24HyoukaNinzuListBean.getIntHyoukaB());
						} else {
							s24AddNinzuListBean.setIntHyoukaB(s24HyoukaNinzuListBean.getIntHyoukaB());
						}
					}
					if (nullNtiFlg == false) {
						if (s24AddNtiNinzuListBean.getIntHyoukaB() != -999.0) {
							s24AddNtiNinzuListBean.setIntHyoukaB(
									s24AddNtiNinzuListBean.getIntHyoukaB() + s24HyoukaNinzuListBean.getIntHyoukaB());
						} else {
							s24AddNtiNinzuListBean.setIntHyoukaB(s24HyoukaNinzuListBean.getIntHyoukaB());
						}
					}
				}

				// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 DEL START
				// 2019/09/30 QQ) 共通テスト対応 ADD START
				// B 含む
//				if (s24HyoukaNinzuListBean.getIntHyoukaB_Hukumu() != -999) {
//					workCell = cm.setCell(workSheet, workRow, workCell, row, 10);
//					workCell.setCellValue(s24HyoukaNinzuListBean.getIntHyoukaB_Hukumu());
//
//					if (nullFlg == false) {
//						if (s24AddNinzuListBean.getIntHyoukaB_Hukumu() != -999.0) {
//							s24AddNinzuListBean.setIntHyoukaB_Hukumu(s24AddNinzuListBean.getIntHyoukaB_Hukumu()
//									+ s24HyoukaNinzuListBean.getIntHyoukaB_Hukumu());
//						} else {
//							s24AddNinzuListBean.setIntHyoukaB_Hukumu(s24HyoukaNinzuListBean.getIntHyoukaB_Hukumu());
//						}
//					}
//					if (nullNtiFlg == false) {
//						if (s24AddNtiNinzuListBean.getIntHyoukaB_Hukumu() != -999.0) {
//							s24AddNtiNinzuListBean.setIntHyoukaB_Hukumu(s24AddNtiNinzuListBean.getIntHyoukaB_Hukumu()
//									+ s24HyoukaNinzuListBean.getIntHyoukaB_Hukumu());
//						} else {
//							s24AddNtiNinzuListBean.setIntHyoukaB_Hukumu(s24HyoukaNinzuListBean.getIntHyoukaB_Hukumu());
//						}
//					}
//				}
				// 2019/09/30 QQ) 共通テスト対応 ADD END
				// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 DEL END

				if (s24HyoukaNinzuListBean.getIntHyoukaC() != -999) {
				        // 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
					// 2019/09/30 QQ) 共通テスト対応 UPD START
					//workCell = cm.setCell( workSheet, workRow, workCell, row, 10 );
					//workCell = cm.setCell(workSheet, workRow, workCell, row, 11);
					// 2019/09/30 QQ) 共通テスト対応 UPD END
					workCell = cm.setCell(workSheet, workRow, workCell, row, 9);
					// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

					workCell.setCellValue(s24HyoukaNinzuListBean.getIntHyoukaC());

					if (nullFlg == false) {
						if (s24AddNinzuListBean.getIntHyoukaC() != -999.0) {
							s24AddNinzuListBean.setIntHyoukaC(
									s24AddNinzuListBean.getIntHyoukaC() + s24HyoukaNinzuListBean.getIntHyoukaC());
						} else {
							s24AddNinzuListBean.setIntHyoukaC(s24HyoukaNinzuListBean.getIntHyoukaC());
						}
					}
					if (nullNtiFlg == false) {
						if (s24AddNtiNinzuListBean.getIntHyoukaC() != -999.0) {
							s24AddNtiNinzuListBean.setIntHyoukaC(
									s24AddNtiNinzuListBean.getIntHyoukaC() + s24HyoukaNinzuListBean.getIntHyoukaC());
						} else {
							s24AddNtiNinzuListBean.setIntHyoukaC(s24HyoukaNinzuListBean.getIntHyoukaC());
						}
					}
				}

				// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 DEL START
				// 2019/09/30 QQ) 共通テスト対応 ADD START
				// C 含む
//				if (s24HyoukaNinzuListBean.getIntHyoukaC_Hukumu() != -999) {
//					workCell = cm.setCell(workSheet, workRow, workCell, row, 12);
//					workCell.setCellValue(s24HyoukaNinzuListBean.getIntHyoukaC_Hukumu());
//
//					if (nullFlg == false) {
//						if (s24AddNinzuListBean.getIntHyoukaC_Hukumu() != -999.0) {
//							s24AddNinzuListBean.setIntHyoukaC_Hukumu(s24AddNinzuListBean.getIntHyoukaC_Hukumu()
//									+ s24HyoukaNinzuListBean.getIntHyoukaC_Hukumu());
//						} else {
//							s24AddNinzuListBean.setIntHyoukaC_Hukumu(s24HyoukaNinzuListBean.getIntHyoukaC_Hukumu());
//						}
//					}
//					if (nullNtiFlg == false) {
//						if (s24AddNtiNinzuListBean.getIntHyoukaC_Hukumu() != -999.0) {
//							s24AddNtiNinzuListBean.setIntHyoukaC_Hukumu(s24AddNtiNinzuListBean.getIntHyoukaC_Hukumu()
//									+ s24HyoukaNinzuListBean.getIntHyoukaC_Hukumu());
//						} else {
//							s24AddNtiNinzuListBean.setIntHyoukaC_Hukumu(s24HyoukaNinzuListBean.getIntHyoukaC_Hukumu());
//						}
//					}
//				}
				// 2019/09/30 QQ) 共通テスト対応 ADD END
				// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 DEL END

				if (s24HyoukaNinzuListBean.getIntHyoukaD() != -999) {
				        // 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
					// 2019/09/30 QQ) 共通テスト対応 UPD START
					//workCell = cm.setCell( workSheet, workRow, workCell, row, 11 );
					//workCell = cm.setCell(workSheet, workRow, workCell, row, 13);
					// 2019/09/30 QQ) 共通テスト対応 UPD END
					workCell = cm.setCell(workSheet, workRow, workCell, row, 10);
					// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

					workCell.setCellValue(s24HyoukaNinzuListBean.getIntHyoukaD());

					if (nullFlg == false) {
						if (s24AddNinzuListBean.getIntHyoukaD() != -999.0) {
							s24AddNinzuListBean.setIntHyoukaD(
									s24AddNinzuListBean.getIntHyoukaD() + s24HyoukaNinzuListBean.getIntHyoukaD());
						} else {
							s24AddNinzuListBean.setIntHyoukaD(s24HyoukaNinzuListBean.getIntHyoukaD());
						}
					}
					if (nullNtiFlg == false) {
						if (s24AddNtiNinzuListBean.getIntHyoukaD() != -999.0) {
							s24AddNtiNinzuListBean.setIntHyoukaD(
									s24AddNtiNinzuListBean.getIntHyoukaD() + s24HyoukaNinzuListBean.getIntHyoukaD());
						} else {
							s24AddNtiNinzuListBean.setIntHyoukaD(s24HyoukaNinzuListBean.getIntHyoukaD());
						}
					}
				}

				// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 DEL START
				// 2019/09/30 QQ) 共通テスト対応 ADD START
				// D 含む
//				if (s24HyoukaNinzuListBean.getIntHyoukaD_Hukumu() != -999) {
//					workCell = cm.setCell(workSheet, workRow, workCell, row, 14);
//					workCell.setCellValue(s24HyoukaNinzuListBean.getIntHyoukaD_Hukumu());
//
//					if (nullFlg == false) {
//						if (s24AddNinzuListBean.getIntHyoukaD_Hukumu() != -999.0) {
//							s24AddNinzuListBean.setIntHyoukaD_Hukumu(s24AddNinzuListBean.getIntHyoukaD_Hukumu()
//									+ s24HyoukaNinzuListBean.getIntHyoukaD_Hukumu());
//						} else {
//							s24AddNinzuListBean.setIntHyoukaD_Hukumu(s24HyoukaNinzuListBean.getIntHyoukaD_Hukumu());
//						}
//					}
//					if (nullNtiFlg == false) {
//						if (s24AddNtiNinzuListBean.getIntHyoukaD_Hukumu() != -999.0) {
//							s24AddNtiNinzuListBean.setIntHyoukaD_Hukumu(s24AddNtiNinzuListBean.getIntHyoukaD_Hukumu()
//									+ s24HyoukaNinzuListBean.getIntHyoukaD_Hukumu());
//						} else {
//							s24AddNtiNinzuListBean.setIntHyoukaD_Hukumu(s24HyoukaNinzuListBean.getIntHyoukaD_Hukumu());
//						}
//					}
//				}
				// 2019/09/30 QQ) 共通テスト対応 ADD END
				// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 DEL END

				if (s24HyoukaNinzuListBean.getIntHyoukaE() != -999) {
				        // 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START
					// 2019/09/30 QQ) 共通テスト対応 UPD START
					//workCell = cm.setCell( workSheet, workRow, workCell, row, 12 );
					//workCell = cm.setCell(workSheet, workRow, workCell, row, 15);
					// 2019/09/30 QQ) 共通テスト対応 UPD END
					workCell = cm.setCell(workSheet, workRow, workCell, row, 11);
					// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END

					workCell.setCellValue(s24HyoukaNinzuListBean.getIntHyoukaE());

					if (nullFlg == false) {
						if (s24AddNinzuListBean.getIntHyoukaE() != -999.0) {
							s24AddNinzuListBean.setIntHyoukaE(
									s24AddNinzuListBean.getIntHyoukaE() + s24HyoukaNinzuListBean.getIntHyoukaE());
						} else {
							s24AddNinzuListBean.setIntHyoukaE(s24HyoukaNinzuListBean.getIntHyoukaE());
						}
					}
					if (nullNtiFlg == false) {
						if (s24AddNtiNinzuListBean.getIntHyoukaE() != -999.0) {
							s24AddNtiNinzuListBean.setIntHyoukaE(
									s24AddNtiNinzuListBean.getIntHyoukaE() + s24HyoukaNinzuListBean.getIntHyoukaE());
						} else {
							s24AddNtiNinzuListBean.setIntHyoukaE(s24HyoukaNinzuListBean.getIntHyoukaE());
						}
					}
				}

				// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 DEL START
				// 2019/09/30 QQ) 共通テスト対応 ADD START
				// E 含む
//				if (s24HyoukaNinzuListBean.getIntHyoukaE_Hukumu() != -999) {
//					workCell = cm.setCell(workSheet, workRow, workCell, row, 16);
//					workCell.setCellValue(s24HyoukaNinzuListBean.getIntHyoukaE_Hukumu());
//
//					if (nullFlg == false) {
//						if (s24AddNinzuListBean.getIntHyoukaE_Hukumu() != -999.0) {
//							s24AddNinzuListBean.setIntHyoukaE_Hukumu(s24AddNinzuListBean.getIntHyoukaE_Hukumu()
//									+ s24HyoukaNinzuListBean.getIntHyoukaE_Hukumu());
//						} else {
//							s24AddNinzuListBean.setIntHyoukaE_Hukumu(s24HyoukaNinzuListBean.getIntHyoukaE_Hukumu());
//						}
//					}
//					if (nullNtiFlg == false) {
//						if (s24AddNtiNinzuListBean.getIntHyoukaE_Hukumu() != -999.0) {
//							s24AddNtiNinzuListBean.setIntHyoukaE_Hukumu(s24AddNtiNinzuListBean.getIntHyoukaE_Hukumu()
//									+ s24HyoukaNinzuListBean.getIntHyoukaE_Hukumu());
//						} else {
//							s24AddNtiNinzuListBean.setIntHyoukaE_Hukumu(s24HyoukaNinzuListBean.getIntHyoukaE_Hukumu());
//						}
//					}
//				}
				// 2019/09/30 QQ) 共通テスト対応 ADD END
				// 2019/11/18 QQ)Ooseto 英語認定試験延期対応 DEL END

				if (nullFlg == false) {
					addList.set(rownum, s24AddNinzuListBean);
				}

				if (nullNtiFlg == false) {
					addNtiList.set(rownum, s24AddNtiNinzuListBean);
				}

				row++;
				rownum++;

			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

}

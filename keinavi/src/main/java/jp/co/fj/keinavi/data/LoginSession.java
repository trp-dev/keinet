/*
 * 作成日: 2004/06/24
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import jp.co.fj.keinavi.beans.login.LoginTransactionBean;
import jp.co.fj.keinavi.interfaces.ILogin;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.totec.config.ConfigResolver;

import org.apache.commons.configuration.Configuration;

import com.fjh.db.DBManager;

/**
 *
 * ログイン情報データクラス
 *
 * 2005.03.23	Yoshimoto KAWAI - Totec
 * 				利用者ＩＤを追加
 *
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 *
 * @author kawai
 */
public class LoginSession implements Serializable, HttpSessionBindingListener {

    public static final String SESSION_KEY = "LoginSession"; // セッションキー
    private static final String PROP_FILE = "develop.properties";
	private String loginID; // ログインユーザID（学校の認証ＩＤ）≠学校ID
	private short pactDiv; // 契約区分
	private short userDiv; // ユーザ区分
	private String userID; // 利用者ID→実際は学校IDが入る・・・名前が混乱している・・・
	private int userMode; // 利用者モード
	private short featprovMode; // 私書箱機能モード
	private short trialMode; // 体験版モード
	private String sectorCd; // 部門コード
	private String sectorSortingCD; // 部門分類コード
	private String userName; // 利用者名称
	private String prefCD; // 県コード

	private boolean cert; // 証明書があるかどうか

	private boolean simple; // シンプルモードかどうか
	private String dbKey; // DBKEY
	private boolean privacyProtection; // プライバシー保護かどうか
	private String account; // 利用者ＩＤ
	private String accountName; // 利用者名
	private boolean isManager = true; // 管理者かどうか
	private boolean isMaintainer = true; // メンテナンス権限があるかどうか
	private short knFunctionFlag = 1; // Kei-Navi機能権限
	private short scFunctionFlag = 1; // 校内成績処理機能権限
	private short eeFunctionFlag = 1; // 入試結果機能権限
	private short abFunctionFlag = 1; // 答案閲覧機能権限
	private String bundleCd;	// 一括コード
	private String bundleName;	// 一括名


    /**
     * コンストラクタです。
     */
    public LoginSession() {
        super();
        try {
            // 開発環境用の設定ファイル読込
            final Configuration config = ConfigResolver.getInstance().getConfiguration(PROP_FILE);
            if (config != null) {
                // 証明書認証を取得
                this.cert = config.getBoolean("certificate");
            }
        } catch (Exception ignore) {
            // エラーは無視
        }
    }

	/**
	 * 営業権限があるかどうか
	 * @return
	 */
	public boolean isSales() {
		return userMode == ILogin.SALES_NORMAL
			|| userMode	== ILogin.HELP_SALES;
	}

	/**
	 * 集計区分を評価するかどうか
	 * @return
	 */
	public boolean isCountingDiv() {
		return userMode == ILogin.SCHOOL_NORMAL
			|| userMode == ILogin.SALES_SCHOOL
			|| userMode == ILogin.HELP_SCHOOL;
	}

	/**
	 * 高校かどうか
	 * @return
	 */
	public boolean isSchool() {
		return userMode == ILogin.SCHOOL_NORMAL
			|| userMode == ILogin.HELP_SCHOOL;
	}

	/**
	 * 営業または校舎かどうか
	 * @return
	 */
	public boolean isBusiness() {
		return userMode == ILogin.SALES_NORMAL
			|| userMode == ILogin.CRAM_NORMAL
			|| userMode == ILogin.HELP_SALES
			|| userMode == ILogin.HELP_CRAM;
	}

	/**
	 * 営業→高校かどうか
	 * @return
	 */
	public boolean isDeputy() {
		return userMode == ILogin.SALES_SCHOOL;
	}

	/**
	 * ヘルプデスクかどうか
	 * @return
	 */
	public boolean isHelpDesk() {
		return pactDiv == 5
			|| userMode == ILogin.HELP_SCHOOL
			|| userMode == ILogin.HELP_SALES
			|| userMode == ILogin.HELP_CRAM;
	}

	/**
	 * 私塾かどうか
	 * @return
	 */
	public boolean isPrep() {
		return userDiv == ILogin.USER_PREP;
	}

	/**
	 * 教育委員会かどうか
	 * @return
	 */
	public boolean isBoard() {
		return userDiv == ILogin.USER_BOARD;
	}

	/**
	 * 契約校かどうか
	 * @return
	 */
	public boolean isPact() {
		return this.pactDiv == ILogin.PACT_NORMAL;
	}

	/**
	 * 体験版モードかどうか
	 * @return
	 */
	public boolean isTrial() {
		return this.trialMode == 1;
	}

	/* (非 Javadoc)
	 * @see javax.servlet.http.HttpSessionBindingListener#valueBound(javax.servlet.http.HttpSessionBindingEvent)
	 */
	public void valueBound(HttpSessionBindingEvent e) {
		// やることは無いはず
	}

	/* (非 Javadoc)
	 * @see javax.servlet.http.HttpSessionBindingListener#valueUnbound(javax.servlet.http.HttpSessionBindingEvent)
	 */
	public void valueUnbound(HttpSessionBindingEvent event) {

		// 証明書がなければ中止
		if (!cert) return;

		// 接続数をカウントダウンする
		Connection con = null;
		try {
			con = DBManager.getConnectionPool(KNCommonProperty.getNDBSID());
			con.setAutoCommit(false);
			LoginTransactionBean bean = new LoginTransactionBean();
			bean.setConnection(KNCommonProperty.getNDBSID(), con);
			bean.setUserID(loginID);
			bean.setCountUp(false);
			bean.execute();
			con.commit();
		} catch (final Exception e) {
			e.printStackTrace();
			try { if (con != null) con.rollback(); }
			catch (final SQLException ex) {}
		} finally {
			try { DBManager.releaseConnectionPool(KNCommonProperty.getNDBSID(), con); }
			catch (final Exception ex) {}
		}
	}

	/**
	 * @return
	 */
	public String getLoginID() {
		return loginID;
	}

	/**
	 * @return
	 */
	public short getPactDiv() {
		return pactDiv;
	}

	/**
	 * @return
	 */
	public String getSectorSortingCD() {
		return sectorSortingCD;
	}

	/**
	 * @return
	 */
	public short getTrialMode() {
		return trialMode;
	}

	/**
	 * @return
	 */
	public short getUserDiv() {
		return userDiv;
	}

	/**
	 * @return
	 */
	public String getUserID() {
		return userID;
	}

	/**
	 * @return
	 */
	public int getUserMode() {
		return userMode;
	}

	/**
	 * @return
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param string
	 */
	public void setLoginID(String string) {
		loginID = string;
	}

	/**
	 * @param s
	 */
	public void setPactDiv(short s) {
		pactDiv = s;
	}

	/**
	 * @param string
	 */
	public void setSectorSortingCD(String string) {
		sectorSortingCD = string;
	}

	/**
	 * @param s
	 */
	public void setTrialMode(short s) {
		trialMode = s;
	}

	/**
	 * @param s
	 */
	public void setUserDiv(short s) {
		userDiv = s;
	}

	/**
	 * @param string
	 */
	public void setUserID(String string) {
		userID = string;
	}

	/**
	 * @param i
	 */
	public void setUserMode(int i) {
		userMode = i;
	}

	/**
	 * @param string
	 */
	public void setUserName(String string) {
		userName = string;
	}

	/**
	 * @return
	 */
	public short getFeatprovMode() {
		return featprovMode;
	}

	/**
	 * @param s
	 */
	public void setFeatprovMode(short s) {
		featprovMode = s;
	}

	/**
	 * @return
	 */
	public boolean isCert() {
		return cert;
	}

	/**
	 * @param b
	 */
	public void setCert(boolean b) {
		cert = b;
	}

	/**
	 * @return
	 */
	public String getDbKey() {
		return dbKey;
	}

	/**
	 * @param string
	 */
	public void setDbKey(String string) {
		dbKey = string;
	}

	/**
	 * @return
	 */
	public boolean isSimple() {
		return simple;
	}

	/**
	 * @param b
	 */
	public void setSimple(boolean b) {
		simple = b;
	}

	/**
	 * @return
	 */
	public String getPrefCD() {
		return prefCD;
	}

	/**
	 * @param string
	 */
	public void setPrefCD(String string) {
		prefCD = string;
	}

    /**
     * @return privacyProtection を戻します。
     */
    public boolean isPrivacyProtection() {
        return privacyProtection;
    }

    /**
     * @param privacyProtection privacyProtection を設定。
     */
    public void setPrivacyProtection() {
        if (this.userID == null || "".equals(this.userID))
            throw new InternalError("プライバシー保護状態の設定に失敗しました。学校コードを設定してください。");
        this.privacyProtection = KNUtil.PRIVACY_PROTECTION_SCHOOL.contains(this.userID);
    }

	public String getAccount() {
		return this.account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * @return accountName を戻します。
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * @param accountName 設定する accountName。
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * @return eeFunctionFlag を戻します。
	 */
	public short getEeFunctionFlag() {
		return eeFunctionFlag;
	}

	/**
	 * @param eeFunctionFlag 設定する eeFunctionFlag。
	 */
	public void setEeFunctionFlag(short eeFunctionFlag) {
		this.eeFunctionFlag = eeFunctionFlag;
	}

	/**
	 * @return isMaintainer を戻します。
	 */
	public boolean isMaintainer() {
		return isMaintainer;
	}

	/**
	 * @param isMaintainer 設定する isMaintainer。
	 */
	public void setMaintainer(boolean isMaintainer) {
		this.isMaintainer = isMaintainer;
	}

	/**
	 * @return isManager を戻します。
	 */
	public boolean isManager() {
		return isManager;
	}

	/**
	 * @param isManager 設定する isManager。
	 */
	public void setManager(boolean isManager) {
		this.isManager = isManager;
	}

	/**
	 * @return knFunctionFlag を戻します。
	 */
	public short getKnFunctionFlag() {
		return knFunctionFlag;
	}

	/**
	 * @param knFunctionFlag 設定する knFunctionFlag。
	 */
	public void setKnFunctionFlag(short knFunctionFlag) {
		this.knFunctionFlag = knFunctionFlag;
	}

	/**
	 * @return scFunctionFlag を戻します。
	 */
	public short getScFunctionFlag() {
		return scFunctionFlag;
	}

	/**
	 * @param scFunctionFlag 設定する scFunctionFlag。
	 */
	public void setScFunctionFlag(short scFunctionFlag) {
		this.scFunctionFlag = scFunctionFlag;
	}

	/**
	 * @return abcFunctionFlag を戻します。
	 */
	public short getAbFunctionFlag() {
		return abFunctionFlag;
	}

	/**
	 * @param abFunctionFlag 設定する abFunctionFlag。
	 */
	public void setAbFunctionFlag(short abFunctionFlag) {
		this.abFunctionFlag = abFunctionFlag;
	}

	/**
	 * @return sectorCd を戻します。
	 */
	public String getSectorCd() {
		return sectorCd;
	}

	/**
	 * @param pSectorCd 設定する sectorCd。
	 */
	public void setSectorCd(String pSectorCd) {
		sectorCd = pSectorCd;
	}

	/**
	 * @param bundleCd を戻します。
	 */
	public String getBundleCd() {
		return bundleCd;
	}

	/**
	 * @param bundleCd 設定する bundleCd。
	 */
	public void setBundleCd(String bundleCd) {
		this.bundleCd = bundleCd;
	}

	/**
	 * @param bundleName を戻します。
	 */
	public String getBundleName() {
		return bundleName;
	}

	/**
	 * @param bundleName 設定する bundleName。
	 */
	public void setBundleName(String bundleName) {
		this.bundleName = bundleName;
	}
}

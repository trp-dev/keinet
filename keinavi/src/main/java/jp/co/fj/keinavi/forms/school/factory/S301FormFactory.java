/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.school.factory;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.school.S001Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

import com.fjh.forms.ActionForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class S301FormFactory extends AbstractSFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		return null;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
		S001Form form = (S001Form) request.getAttribute("form");
		//一括出力対象
		Set outItem = CollectionUtil.array2Set(form.getOutItem());
		//プロファイル
		Profile profile = (Profile) super.getProfile(request);
		
		// 全国総合成績
		super.setTotalOutItem(profile, outItem);

		// 成績概況
		ProfileUtil.setOutItem(
			profile,
			IProfileCategory.S_OTHER_SCORE,
			outItem.contains("OtherScore")
		);

		// 偏差値分布
		ProfileUtil.setOutItem(
			profile,
			IProfileCategory.S_OTHER_DEV,
			outItem.contains("OtherDev")
		);

		// 設問別成績
		ProfileUtil.setOutItem(
			profile,
			IProfileCategory.S_OTHER_QUE,
			outItem.contains("OtherQue")
		);

		// 志望大学評価別人数 
		ProfileUtil.setOutItem(
			profile,
			IProfileCategory.S_OTHER_UNIV,
			outItem.contains("OtherUniv")
		);
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return null;
	}

}

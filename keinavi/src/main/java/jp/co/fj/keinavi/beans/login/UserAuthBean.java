package jp.co.fj.keinavi.beans.login;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.message.MessageLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 利用者認証Bean
 * 
 * 
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				新規作成
 * 
 * 
 * @author kawai
 * 
 */
public class UserAuthBean extends DefaultBean {
	
	private final LoginSession loginSession; // ログインセッション
	private final String password; // パスワード
	
	/**
	 * コンストラクタ
	 * 
	 * @param session
	 */
	public UserAuthBean(final LoginSession loginSession,
			final String password) {
		this.loginSession = loginSession;
		this.password = password;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					"SELECT m.loginname, m.manage_flg, m.mainte_flg, "
					+ "m.func1_flg, m.func2_flg, m.func3_flg, m.func4_flg "
					+ "FROM loginid_manage m "
					+ "WHERE m.schoolcd = ? AND m.loginid = ? AND m.loginpwd = ?");
			
			// 学校コード
			ps.setString(1, loginSession.getUserID());
			// 利用者ID
			ps.setString(2, loginSession.getAccount());
			// パスワード
			ps.setString(3, password);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				loginSession.setAccountName(rs.getString(1));
				loginSession.setManager("1".equals(rs.getString(2)));
				loginSession.setMaintainer("1".equals(rs.getString(3)));
				loginSession.setKnFunctionFlag(rs.getShort(4));
				loginSession.setScFunctionFlag(rs.getShort(5));
				loginSession.setEeFunctionFlag(rs.getShort(6));
				loginSession.setAbFunctionFlag(rs.getShort(7));
				
			} else {
				final KNServletException e = new KNServletException(
						loginSession.getLoginID() + " "
						+ loginSession.getAccount() + " 利用者認証に失敗しました。");
				e.setErrorCode("1");
				e.setErrorMessage(MessageLoader.getInstance().getMessage("w028a"));
				throw e;
			}
			
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

}

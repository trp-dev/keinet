package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * 校内成績分析−過年度比較−偏差値分布 データリスト
 * 作成日: 2004/07/21
 * @author	A.Iwata
 */
public class S22ListBean {
	
	//型・科目コード
	private String strKmkCd = "";
	//型･科目名
	private String strKmkmei = "";
	//型・科目用グラフ表示フラグ
	private int intDispKmkFlg = 0;
	//配点
	private String strHaitenKmk = "";
	//偏差値分布データリスト
	private ArrayList s22BnpList = new ArrayList();
	//集計データリスト
	private ArrayList s22TotalList = new ArrayList();

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return strKmkCd;
	}
	public String getStrKmkmei() {
		return strKmkmei;
	}
	public int getIntDispKmkFlg() {
		return intDispKmkFlg;
	}
	public String getStrHaitenKmk() {
		return strHaitenKmk;
	}
	public ArrayList getS22BnpList() {
		return this.s22BnpList;
	}
	public ArrayList getS22TotalList() {
		return this.s22TotalList;
	}
	

	/*----------*/
	/* Set      */
	/*----------*/

	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setIntDispKmkFlg(int intDispKmkFlg) {
		this.intDispKmkFlg = intDispKmkFlg;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setS22BnpList(ArrayList s22BnpList) {
		this.s22BnpList = s22BnpList;
	}
	public void setS22TotalList(ArrayList s22TotalList) {
		this.s22TotalList = s22TotalList;
	}


}

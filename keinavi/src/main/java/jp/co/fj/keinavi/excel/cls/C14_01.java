/**
 * 校内成績分析−クラス比較　成績概況（設問別個人成績）
 * 	Excelファイル編集
 * 作成日: 2004/09/02
 * @author	H.Fujimoto
 * 
 * 2009.10.09   Fujito URAKAWA - Totec
 *              「全体成績」追加対応 
 */

package jp.co.fj.keinavi.excel.cls;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C14Item;
import jp.co.fj.keinavi.excel.data.cls.C14KmkListBean;
import jp.co.fj.keinavi.excel.data.cls.C14ListBean;
import jp.co.fj.keinavi.excel.data.cls.C14PersonalDataListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class C14_01 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス

	final private String masterfile0	= "C14_01";
	final private String masterfile1	= "C14_01";
	private String masterfile	= "";

	private boolean bolSheetCngFlg = true;			//改シートフラグ

/*
 * 	Excel編集メイン
 * 		S13Item c14Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int c14_01EditExcel(C14Item c14Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		ArrayList WorkBooklistS;
		ArrayList WorkSheetlistS;

		HSSFWorkbook	workbookNow 	= null;
		HSSFSheet		workSheetNow	= null;
		HSSFWorkbook	workbookKari 	= null;

		KNLog log = KNLog.getInstance(null,null,null);

		//テンプレートの決定
		if (c14Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			// POI初期化
			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			// 基本ファイルを読込む
			C14ListBean c14ListBean = new C14ListBean();

			// データセット
			ArrayList	c14List	= c14Item.getC14List();
			Iterator	itr		= c14List.iterator();
			
			int		row			= 0;	//行
			int		col			= 0;	//列
			int		ninzuCnt	= 0;	//人数カウンタ
			int		setsumonCnt = 0;	//設問カウンタ
			int		TotalsetsuCnt = 0;	//全設問カウンタ
			String		kmk			= "";	//型・科目チェック用
			int		intMaxSheetIndex	= 0;
			int		intBookCngCount		= 0;	// ブックカウンター
			boolean	bolCloseBookFlg		= false;// true:改表実行　（ブック閉フラグ）
			boolean	bolLoadBookFlg		= true;// true:改表実行　（ブック開フラグ）
			boolean	bolClosePBookFlg	= false;// true:人数で50超え時の改表実行　（ブック開フラグ）
			
			int		 dataSize		= 0;	// リストデータ件数
			int		 houjiNum		= 45;	// エクセル表示可能件数
			int		sheetNumRow		= 0;	// シート枚数（Row方向）
			int         ninzuSheetCnt = 0;		//人数による改シートの回数
			int         SetsuSheetCnt = 0;		//設問による改シートの回数

			WorkBooklistS = new ArrayList();
			WorkSheetlistS = new ArrayList();
			
			log.Ep("C14_01","基本データデータセット開始","");
			
			while( itr.hasNext() ) {
				c14ListBean = (C14ListBean)itr.next();

				// 基本ファイルを読込む
				C14KmkListBean c14KmkListBean = new C14KmkListBean();
				
				// 科目データセット
				ArrayList c14KmkList = c14ListBean.getC14KmkList();
				Iterator itrKmk = c14KmkList.iterator();

				boolean kmkFlg = true;	//型→科目変更フラグ
				
				log.Ep("C14_01","科目データデータセット開始","");

				while ( itrKmk.hasNext() ){
					// 基本ファイルを読込む(科目)
					C14PersonalDataListBean c14PersonalDataListBean = new C14PersonalDataListBean();

					c14KmkListBean = (C14KmkListBean)itrKmk.next();
					
					//人数データセット
					ArrayList c14PersonalData = c14KmkListBean.getC14PersonalDataList();
					Iterator itrPersonal = c14PersonalData.iterator();

					dataSize = c14PersonalData.size(); //データ件数
					//下方向のシート数の計算
					sheetNumRow = dataSize/(houjiNum+1);

					//科目が変わる時のチェック
					if ( !cm.toString(kmk).equals(cm.toString(c14KmkListBean.getStrKmkCd())) ) {
						bolSheetCngFlg = true;
						setsumonCnt = 0;

						TotalsetsuCnt = 0;
						SetsuSheetCnt = 0;
						kmkFlg = true;
					}

					//改シート条件（クラスが変わるor科目が変わるor設問が10以上）
					if ( bolSheetCngFlg == true ) {
						
						if (TotalsetsuCnt != 0){
							SetsuSheetCnt++;
						}

						//人数で５０シート超えてたとき、このタイミングでExcelファイル保存
						if(bolClosePBookFlg == true){
							workbookNow = workbook;
							workSheetNow = workSheet;
							workbook = workbookKari;

							// 保存処理
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, 50);
							if( bolRet == false ){
								return errfwrite;
							}

							intBookCngCount++;
							workbook = workbookNow;
							workSheet = workSheetNow;

							bolClosePBookFlg = false;
							workbookKari = null;

							workbookNow = null;
							workSheetNow = null;
						}
						
						// Excelファイル保存
						if(intMaxSheetIndex >= 50){
							
							bolCloseBookFlg = true;
						}
						if( bolCloseBookFlg == true){

							// 保存処理
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
							if( bolRet == false ){
								return errfwrite;
							}

							intBookCngCount++;
							bolCloseBookFlg	= false;
							bolLoadBookFlg	= true;
							TotalsetsuCnt = 0;
							SetsuSheetCnt = 0;
						}

						//マスタExcel読み込み
						if( bolLoadBookFlg == true ){
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook == null ){
								return errfread;
							}

							intMaxSheetIndex = 0;
							bolLoadBookFlg	= false;
						}

						if (kmkFlg == true){
							//リストクリア
							WorkBooklistS.clear();
							WorkSheetlistS.clear();

						}

						// シートテンプレートのコピー
						workSheet = workbook.cloneSheet(0);
						intMaxSheetIndex++;
						
						WorkBooklistS.add(SetsuSheetCnt,workbook);
						WorkSheetlistS.add(SetsuSheetCnt,workSheet);
						
						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);
					
						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, c14Item.getIntSecuFlg() ,35 ,37 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 35 );
						workCell.setCellValue(secuFlg);
					
						// ラベルセット
						if (c14Item.getIntShubetsuFlg() == 1){
						} else{
							workCell = cm.setCell( workSheet, workRow, workCell, 10, 3 );
							workCell.setCellValue( "全国平均点" );
							workCell = cm.setCell( workSheet, workRow, workCell, 11, 3 );
							workCell.setCellValue( "都道府県内平均点" );
						}

						// 学校名セット → 2005/03/29 学校名の出力追加
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( "学校名　：" + cm.toString(c14Item.getStrGakkomei()) + "　学年：" + cm.toString(c14ListBean.getStrGrade()) + "　クラス名：" + cm.toString(c14ListBean.getStrClass()));

						// 模試月取得
						String moshi =cm.setTaisyouMoshi( c14Item.getStrMshDate() );
						// 対象模試セット
						workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
						workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(c14Item.getStrMshmei()) + moshi);

						// 科目名+配点セット
						String haiten = "";
						if ( !cm.toString(c14KmkListBean.getStrHaitenKmk()).equals("") ) {
							haiten = "（" + c14KmkListBean.getStrHaitenKmk() + "）";
						}
						workCell = cm.setCell( workSheet, workRow, workCell, 5, 0 );
						workCell.setCellValue( "型・科目：" + cm.toString(c14KmkListBean.getStrKmkmei()) + haiten );

						// 受験人数セット
						workCell = cm.setCell( workSheet, workRow, workCell, 5, 4 );
						if ( c14KmkListBean.getIntNinzu() != -999 ) {
							workCell.setCellValue( "受験者数：" + c14KmkListBean.getIntNinzu() + "人" );
						}

						col = 5;
						bolSheetCngFlg = false;
						kmkFlg = false;
					}
					row = 15;
//Add T.Sakai 2004/10/6
					//先頭の個人情報の時設問情報を出力
					if (ninzuCnt == 0 ){		

						//設問毎にブック情報を取得
						if (setsumonCnt < 11) {
							workbook = (HSSFWorkbook)WorkBooklistS.get(ninzuSheetCnt);
							workSheet = (HSSFSheet)WorkSheetlistS.get(ninzuSheetCnt);
						}else if(setsumonCnt == 11){
							workbook = (HSSFWorkbook)WorkBooklistS.get(SetsuSheetCnt);
							workSheet = (HSSFSheet)WorkSheetlistS.get(SetsuSheetCnt);
						}else{
							workbook = (HSSFWorkbook)WorkBooklistS.get(SetsuSheetCnt-sheetNumRow+ninzuSheetCnt);
							workSheet = (HSSFSheet)WorkSheetlistS.get(SetsuSheetCnt-sheetNumRow+ninzuSheetCnt);
						}

						// 設問番号セット
						workCell = cm.setCell( workSheet, workRow, workCell, 6, col );
						workCell.setCellValue( c14KmkListBean.getStrSetsuNo() );
						// 設問内容セット
						workCell = cm.setCell( workSheet, workRow, workCell, 7, col );
						workCell.setCellValue( c14KmkListBean.getStrSetsuMei1() );
						// 設問配点セット
						workCell = cm.setCell( workSheet, workRow, workCell, 8, col );
						if ( !cm.toString(c14KmkListBean.getStrSetsuHaiten()).equals("") ) {
							workCell.setCellValue( "（" + c14KmkListBean.getStrSetsuHaiten() + "）" );
						}
						// 全国平均点セット
						workCell = cm.setCell( workSheet, workRow, workCell, 10, col+1 );
						if ( c14KmkListBean.getFloHeikinAll()!=-999.0 ) {
							workCell.setCellValue( c14KmkListBean.getFloHeikinAll() );
						}
						// 県内平均点セット
						workCell = cm.setCell( workSheet, workRow, workCell, 11, col+1 );
						if ( c14KmkListBean.getFloHeikinKen()!=-999.0 ) {
							workCell.setCellValue( c14KmkListBean.getFloHeikinKen() );
						}
						// 校内平均点セット
						workCell = cm.setCell( workSheet, workRow, workCell, 12, col+1 );
						if ( c14KmkListBean.getFloHeikinHome()!=-999.0 ) {
							workCell.setCellValue( c14KmkListBean.getFloHeikinHome() );
						}
						// クラス平均点セット
						workCell = cm.setCell( workSheet, workRow, workCell, 13, col+1 );
						if ( c14KmkListBean.getFloHeikinCls()!=-999.0 ) {
							workCell.setCellValue( c14KmkListBean.getFloHeikinCls() );
						}
							
						bolSheetCngFlg = false;
					}
//Add End T.Sakai 2004/10/6
					log.Ep("C14_01","個人データデータセット開始","");

					while ( itrPersonal.hasNext() ){
						//基本ファイルを読込む(個人)
						c14PersonalDataListBean = (C14PersonalDataListBean)itrPersonal.next();
						
						if (ninzuCnt == 45) {
							bolSheetCngFlg = true;
							ninzuCnt = 0;
							ninzuSheetCnt++;
							row = 15;
						}
						
						//改シート条件（人数が４５人を超えるたび）
						if (setsumonCnt==0 || setsumonCnt==11) {
							if (bolSheetCngFlg == true) {
								SetsuSheetCnt++;
								// Excelファイル保存
								if((intMaxSheetIndex >= 50)){
									bolClosePBookFlg = true;
									workbookKari = workbook;

									//マスタExcel読み込み
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook==null ){
										return errfread;
									}
									intMaxSheetIndex = 0;
								}

								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);

								intMaxSheetIndex++;
	
								WorkBooklistS.add(SetsuSheetCnt,workbook);
								WorkSheetlistS.add(SetsuSheetCnt,workSheet);
	
								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);
						
								// セキュリティスタンプセット
								String secuFlg = cm.setSecurity( workbook, workSheet, c14Item.getIntSecuFlg() ,35 ,37 );
								workCell = cm.setCell( workSheet, workRow, workCell, 0, 35 );
								workCell.setCellValue(secuFlg);
						
								// ラベルセット
								if (c14Item.getIntShubetsuFlg() == 1){
								} else{
									workCell = cm.setCell( workSheet, workRow, workCell, 10, 3 );
									workCell.setCellValue( "全国平均点" );
									workCell = cm.setCell( workSheet, workRow, workCell, 11, 3 );
									workCell.setCellValue( "都道府県内平均点" );
								}

								// 学校名セット → 2005/03/29 学校名の出力追加
								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
								workCell.setCellValue( "学校名　：" + cm.toString(c14Item.getStrGakkomei()) + "　学年：" + cm.toString(c14ListBean.getStrGrade()) + "　クラス名：" + cm.toString(c14ListBean.getStrClass()));
	
								// 模試月取得
								String moshi =cm.setTaisyouMoshi( c14Item.getStrMshDate() );
								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
								workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(c14Item.getStrMshmei()) + moshi);
	
								// 科目名+配点セット
								String haiten = "";
								if ( !cm.toString(c14KmkListBean.getStrHaitenKmk()).equals("") ) {
									haiten = "（" + c14KmkListBean.getStrHaitenKmk() + "）";
								}
								workCell = cm.setCell( workSheet, workRow, workCell, 5, 0 );
								workCell.setCellValue( "型・科目：" + cm.toString(c14KmkListBean.getStrKmkmei()) + haiten );
	
								// 受験人数セット
								workCell = cm.setCell( workSheet, workRow, workCell, 5, 4 );
								if ( c14KmkListBean.getIntNinzu() != -999 ) {
									workCell.setCellValue( "受験者数：" + c14KmkListBean.getIntNinzu() + "人");
								}

								row = 15;
								bolSheetCngFlg = false;
							}
						}

						//先頭の個人情報の時設問情報を出力
						if (ninzuCnt == 0 ){		

							//設問毎にブック情報を取得
							if (setsumonCnt < 11) {
								workbook = (HSSFWorkbook)WorkBooklistS.get(ninzuSheetCnt);
								workSheet = (HSSFSheet)WorkSheetlistS.get(ninzuSheetCnt);
							}else if(setsumonCnt == 11){
								workbook = (HSSFWorkbook)WorkBooklistS.get(SetsuSheetCnt);
								workSheet = (HSSFSheet)WorkSheetlistS.get(SetsuSheetCnt);
							}else{
								workbook = (HSSFWorkbook)WorkBooklistS.get(SetsuSheetCnt-sheetNumRow+ninzuSheetCnt);
								workSheet = (HSSFSheet)WorkSheetlistS.get(SetsuSheetCnt-sheetNumRow+ninzuSheetCnt);
							}

							// 設問番号セット
							workCell = cm.setCell( workSheet, workRow, workCell, 6, col );
							workCell.setCellValue( c14KmkListBean.getStrSetsuNo() );
							// 設問内容セット
							workCell = cm.setCell( workSheet, workRow, workCell, 7, col );
							workCell.setCellValue( c14KmkListBean.getStrSetsuMei1() );
							// 設問配点セット
							workCell = cm.setCell( workSheet, workRow, workCell, 8, col );
							if ( !cm.toString(c14KmkListBean.getStrSetsuHaiten()).equals("") ) {
								workCell.setCellValue( "（" + c14KmkListBean.getStrSetsuHaiten() + "）" );
							}
							// 全国平均点セット
							workCell = cm.setCell( workSheet, workRow, workCell, 10, col+1 );
							if ( c14KmkListBean.getFloHeikinAll()!=-999.0 ) {
								workCell.setCellValue( c14KmkListBean.getFloHeikinAll() );
							}
							// 県内平均点セット
							workCell = cm.setCell( workSheet, workRow, workCell, 11, col+1 );
							if ( c14KmkListBean.getFloHeikinKen()!=-999.0 ) {
								workCell.setCellValue( c14KmkListBean.getFloHeikinKen() );
							}
							// 校内平均点セット
							workCell = cm.setCell( workSheet, workRow, workCell, 12, col+1 );
							if ( c14KmkListBean.getFloHeikinHome()!=-999.0 ) {
								workCell.setCellValue( c14KmkListBean.getFloHeikinHome() );
							}
							// クラス平均点セット
							workCell = cm.setCell( workSheet, workRow, workCell, 13, col+1 );
							if ( c14KmkListBean.getFloHeikinCls()!=-999.0 ) {
								workCell.setCellValue( c14KmkListBean.getFloHeikinCls() );
							}
							
							bolSheetCngFlg = false;
						}

						//設問１or設問１１の時学年・クラス・番号・氏名出力
						if (setsumonCnt==0 || setsumonCnt==11) {
							//学年セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, 0 );
							workCell.setCellValue( c14PersonalDataListBean.getStrGrade() );
							// クラスセット
							workCell = cm.setCell( workSheet, workRow, workCell, row, 1 );
							workCell.setCellValue( c14PersonalDataListBean.getStrClass() );
							// クラス番号セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, 2 );
							workCell.setCellValue( c14PersonalDataListBean.getStrClassNo() );
							// 氏名セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, 3 );
							workCell.setCellValue( c14PersonalDataListBean.getStrKanaName() );
							// 性別セット
							int sexKbn = Integer.parseInt( c14PersonalDataListBean.getStrSex() );
							String strSex = "";
							if( sexKbn == 1 ){
								strSex = "男";
							}else if( sexKbn == 2 ){
								strSex = "女";
							}else if( sexKbn == 9 ){
								strSex = "不";
							}
							workCell = cm.setCell( workSheet, workRow, workCell, row, 4 );
							workCell.setCellValue( strSex );
						} 

						//*セット
						if ( c14PersonalDataListBean.getIntTokuten() != -999 ) {
							if (c14KmkListBean.getFloHeikinHome() > c14PersonalDataListBean.getIntTokuten()) {
								workCell = cm.setCell( workSheet, workRow, workCell, row, col );
								workCell.setCellValue( "*" );
							}
						}
						//得点セット
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						if ( c14PersonalDataListBean.getIntTokuten() != -999 ) {
							workCell.setCellValue( c14PersonalDataListBean.getIntTokuten() );
						}
						//得点率セット
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
						if ( c14PersonalDataListBean.getFloTokuritsu() !=-999.0 ) {
							workCell.setCellValue( c14PersonalDataListBean.getFloTokuritsu() );
						}
						
						ninzuCnt++;
						row++;
					}
					
					log.Ep("C14_01","個人データデータセット終了","");

					TotalsetsuCnt++;
					setsumonCnt++;
					col++;
					col++;
					col++;
					ninzuCnt = 0;
					ninzuSheetCnt = 0;
					kmk = c14KmkListBean.getStrKmkCd();
					
					if (setsumonCnt == 11) {
						bolSheetCngFlg = true;
					}
				}
				
				log.Ep("C14_01","科目データデータセット終了","");

				setsumonCnt = 0;
				bolSheetCngFlg = true;
				
				TotalsetsuCnt = 0;
				SetsuSheetCnt = 0;

				//リストクリア
				WorkBooklistS.clear();
				WorkSheetlistS.clear();


			}

			// Excelファイル保存
			boolean bolRet = false;
			if(intBookCngCount==0){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, intMaxSheetIndex);
			}else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("C14_01","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("C14_01","基本データデータセット終了","");
		return noerror;
	}

}
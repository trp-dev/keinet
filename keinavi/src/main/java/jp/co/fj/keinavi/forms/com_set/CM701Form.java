package jp.co.fj.keinavi.forms.com_set;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * 
 * 担当クラスアクションフォーム
 * 
 * 
 * 2005.02.24	Yoshimoto KAWAI - Totec
 * 				年度対応
 * 
 * 2005.10.12	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 * 
 * @author kawai
 *
 */
public class CM701Form extends BaseForm {
	
	// 学年
	private String grade = null;
	// 担当クラス
	private String[] classes = null;
	// 登録するかどうか
	private String operation = null;
	// 年度
	private String year = null;
	
	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * @return classes を戻します。
	 */
	public String[] getClasses() {
		return classes;
	}

	/**
	 * @param classes 設定する classes。
	 */
	public void setClasses(String[] classes) {
		this.classes = classes;
	}

	/**
	 * @return
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * @param string
	 */
	public void setGrade(String string) {
		grade = string;
	}

	/**
	 * @return
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * @param string
	 */
	public void setOperation(String string) {
		operation = string;
	}

	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
}

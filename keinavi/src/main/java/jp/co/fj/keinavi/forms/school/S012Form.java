/*
 * 作成日: 2004/12/22
 */
package jp.co.fj.keinavi.forms.school;

import com.fjh.forms.ActionForm;

/**
 * サンプル画面アクションフォーム
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */
public class S012Form extends ActionForm {

    private String screen; // 画面ID
    private String category; // カテゴリ
    
    /* (非 Javadoc)
     * @see com.fjh.forms.ActionForm#validate()
     */
    public void validate() {
    }

    /**
     * @return category を戻す。
     */
    public String getCategory() {
        return category;
    }
    /**
     * @param category category を設定する。
     */
    public void setCategory(String category) {
        this.category = category;
    }
    /**
     * @return screen を戻す。
     */
    public String getScreen() {
        return screen;
    }
    /**
     * @param screen screen を設定する。
     */
    public void setScreen(String screen) {
        this.screen = screen;
    }
}

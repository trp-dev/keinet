/**
 * 個人成績分析−面談シート　成績グラフ
 * @author H.Fujimoto
 */
 
package jp.co.fj.keinavi.excel.personal;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.personal.*;
import jp.co.fj.keinavi.util.log.*;

public class I12 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean i12( I11Item i11Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog) {

		KNLog log = KNLog.getInstance(null,null,null);

		try{
			
			//I11Itemから各帳票を出力
			int ret = 0;
			if (  i11Item.getIntKyoSheetFlg()!=1
			  &&  i11Item.getIntSuiiFlg()!=1  
			  &&  i11Item.getIntSetsumonFlg()!=1 ){
				throw new Exception("I12_01 ERROR : フラグ異常");
			}
			
			if( i11Item.getIntKyoSheetFlg()==1 ){
				log.Ep("I12_01","I12_01帳票作成開始","");
				I12_01 exceledit1 = new I12_01();
				ret = exceledit1.i12_01EditExcel( i11Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"I12_01","帳票作成エラー","");
					return false;
				}
				log.Ep("I12_01","I12_01帳票作成終了","");
				sheetLog.add("I12_01");
			}

			if( i11Item.getIntSuiiFlg()==1 ){
				log.Ep("I12_02","I12_02帳票作成開始","");
				I12_02 exceledit2 = new I12_02();
				ret = exceledit2.i12_02EditExcel( i11Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"I12_02","帳票作成エラー","");
					return false;
				}
				log.Ep("I12_02","I12_02帳票作成終了","");
				sheetLog.add("I12_02");
			}

			if( i11Item.getIntSetsumonFlg()==1 ){
				log.Ep("I12_03","I12_03帳票作成開始","");
				I12_03 exceledit3 = new I12_03();
				ret = exceledit3.i12_03EditExcel( i11Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"I13_03","帳票作成エラー","");
					return false;
				}
				log.Ep("I12_03","I12_03帳票作成終了","");
				sheetLog.add("I12_03");
			}


		} catch(Exception e) {
			e.printStackTrace();
			log.Err("99I12","帳票作成エラー",e.toString());
			return false;
		}
		return true;
	}

}
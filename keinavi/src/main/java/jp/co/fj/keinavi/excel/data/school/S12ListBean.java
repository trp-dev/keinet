package jp.co.fj.keinavi.excel.data.school;

//import org.apache.struts.action.ActionForm;
import java.util.ArrayList;
/**
 * 偏差値分布（校内成績）クラスデータリスト
 * 作成日: 2004/07/06
 * @author	T.Kuzuno
 */
public class S12ListBean {
	//型・科目名コード
	private String strKmkCd = "";
	//型・科目名
	private String strKmkmei = "";
	//型･科目用グラフ表示フラグ
	private int intDispKmkFlg = 0;
	//配点
	private String strHaitenKmk = "";
	//合計人数
	private int intNinzu = 0;
	//平均点
	private float floHeikin = 0;
	//平均偏差値
	private float floHensa = 0;
	//偏差値分布データリスト
	private ArrayList s12BnpList = new ArrayList();

	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}
	public int getIntDispKmkFlg() {
		return this.intDispKmkFlg;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public float getFloHeikin() {
		return this.floHeikin;
	}
	public float getFloHensa() {
		return this.floHensa;
	}
	public ArrayList getS12BnpList() {
		return this.s12BnpList;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setIntDispKmkFlg(int intDispKmkFlg) {
		this.intDispKmkFlg = intDispKmkFlg;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setFloHeikin(float floHeikin) {
		this.floHeikin = floHeikin;
	}
	public void setFloHensa(float floHensa) {
		this.floHensa = floHensa;
	}
	public void setS12BnpList(ArrayList s12BnpList) {
		this.s12BnpList = s12BnpList;
	}
}

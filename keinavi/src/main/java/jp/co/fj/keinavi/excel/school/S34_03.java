package jp.co.fj.keinavi.excel.school;

//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.ListIterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S34ClassListBean;
import jp.co.fj.keinavi.excel.data.school.S34HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.school.S34Item;
import jp.co.fj.keinavi.excel.data.school.S34ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * 作成日: 2004/09/09
 * @author Ito.Y
 *
 * 校内成績分析−過年度比較−志望大学評価別人数　大学（学部あり）出力処理
 * 
 * <2010年度改修>
 * 2010.01.06   Tomohisa YAMADA - Totec
 *              センター・リサーチ５段階評価対応
 * 
 */
public class S34_03 {
	
	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow		= 50;			//MAX行数
	private int		intMaxClassSr   = 5;			//MAXクラス数

	private int intDataStartRow = 8;					//データセット開始行
	private int intDataStartCol = 9;					//データセット開始列
	
	final private String masterfile0 = "S34_03" ;
	final private String masterfile1 = "S34_03" ;
	private String masterfile = "" ;
	
	final private String strCenterResearch = "38";

	/*
	 * 	Excel編集メイン
	 * 		S34Item S34Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int s34_03EditExcel(S34Item s34Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("S34_03","S34_03帳票作成開始","");

		//テンプレートの決定
		if (s34Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		/** セット処理 **/
		boolean bolBookCngFlg = true;			//改ファイルフラグ	
		boolean bolSheetCngFlg = true;			//改シートフラグ
			
		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFCell	workCell		= null;
		HSSFRow		workRow			= null;
		HSSFWorkbook	tmpworkbook = null;
		
		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intCol = 0;

		String strGenekiKbn = "";
		String strDaigakuCode = "";
		String strGkbCode = "";
		String strHyoukaKbn = "";
		
		int intLoopCnt = 0;
		int intBookCngCount = 1;

		//2004.09.27Add
		String strKokushiKbn = "";
		String strStKokushiKbn = "";
		int intChangeSheetRow = 0;
		//2004.09.27AddEnd
		
		boolean bolDispHyoukaFlg = false;
		boolean bolDispGkbFlg = false;
		boolean bolDispDaigakuFlg = false;
		String strHyouka = "";
		
		String strDispTarget = "";
		
		ArrayList WorkBookList = new ArrayList();
		ArrayList WorkSheetList = new ArrayList();
		
		int intSheetCnt=0;
		
		int intBookIndex = -1;
		int intSheetIndex = -1;
		
		int intSetRow = intDataStartRow;
		
		boolean bolClassDispFlg=true;
		
		ArrayList HyoukaTitleList = new ArrayList();
		
		// 基本ファイルを読込む
		S34ListBean s34ListBean = new S34ListBean();
		
		try{
			/** S34Item編集 **/
			s34Item  = editS34( s34Item );
			
			HyoukaTitleList.add(0,"A");
			HyoukaTitleList.add(1,"B");
			HyoukaTitleList.add(2,"C");

			// 基本ファイルを読込む
			ArrayList s34List = s34Item.getS34List();
			ListIterator itr = s34List.listIterator();
			
//			if( itr.hasNext() == false ){
//				return errfdata;
//			}

			/** 大学・学部・評価 **/
			// S34ListBean 
			while( itr.hasNext() ){
				s34ListBean = (S34ListBean)itr.next();
				
				// 大学名
				bolDispDaigakuFlg = false;
				if( !cm.toString(strDaigakuCode).equals(cm.toString(s34ListBean.getStrDaigakuCd()))){
					bolDispDaigakuFlg = true;
					//大学名表示
					strDaigakuCode = s34ListBean.getStrDaigakuCd();
				}
				
				// 学部
				bolDispGkbFlg = false;
				if( !cm.toString(strGkbCode).equals(cm.toString(s34ListBean.getStrGakubuCd())) ){
					bolDispGkbFlg = true;
					strGkbCode = s34ListBean.getStrGakubuCd();
				}

				// 評価区分
				bolDispHyoukaFlg = false;
				if( !cm.toString(strHyoukaKbn).equals(cm.toString(s34ListBean.getStrHyouka())) ){
					bolDispHyoukaFlg = true;
					//評価区分表示
					strHyoukaKbn = s34ListBean.getStrHyouka();
					strHyouka = "";
					switch( Integer.parseInt(strHyoukaKbn) ){
						case 1:
							strHyouka = "センター";
							break;
						case 2:
							strHyouka = "2次・私大";
							break;
						case 3:
							strHyouka = "総合";
							break;
					}
				}

				// 表示対象
				if( !cm.toString(strGenekiKbn).equals(cm.toString(s34ListBean.getStrGenKouKbn())) ){
					strGenekiKbn = s34ListBean.getStrGenKouKbn();
					if( cm.toString(strGenekiKbn).equals("0") ){
						strDispTarget = "全体";
					}
					else if( cm.toString(strGenekiKbn).equals("1") ){
						strDispTarget = "現役生";
					}
					else if( cm.toString(strGenekiKbn).equals("2") ){
						strDispTarget = "高卒生";
					}

					if( bolSheetCngFlg == false ){
						bolSheetCngFlg = true;
					}
				}

				//2004.9.27 Add
				int intDaigakuCd = (int)Integer.parseInt(s34ListBean.getStrDaigakuCd());
				if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
					strKokushiKbn = "国公立大";
				}
				if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
					strKokushiKbn = "私立大";
				}
				if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
					strKokushiKbn = "短大";
				}
				if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
					strKokushiKbn = "その他";
				}
				
				if( !strKokushiKbn.equals(strStKokushiKbn) ){
					bolSheetCngFlg = true;
					strStKokushiKbn = strKokushiKbn;
				}
				//2004.9.27 Add End
				
				//クラス別評価人数リスト読み込み クラス名をExcelへセット
				ArrayList s34ClassList = s34ListBean.getS34ClassList();
				ListIterator itr2 = s34ClassList.listIterator();

				// Listの取得
				S34ClassListBean s34ClassListBean = new S34ClassListBean();
				S34ClassListBean HomeDataBean = new S34ClassListBean();

				//クラス数取得
				int intClassSr = s34ClassList.size();

//				int intSheetSr = (int)Math.ceil(intClassSr / intMaxClassSr);
//				int intSheetSr = intClassSr / intMaxClassSr;
//				if(intClassSr%intMaxClassSr!=0){
//					intSheetSr++;
//				}
				int intSheetSr = (intClassSr - 1) / intMaxClassSr;
				if((intClassSr - 1)%intMaxClassSr!=0){
					intSheetSr++;
				}
				
				int intLoopCnt2 = 0;
				int intNendoCount = 0;
				intChangeSheetRow = 0;
				
				/** 学校名・学年・クラス **/
				// S34ClassListBean
				while( itr2.hasNext() ){
					s34ClassListBean = (S34ClassListBean)itr2.next();

					// Listの取得
					ArrayList s34HyoukaNinzuList = s34ClassListBean.getS34HyoukaNinzuList();
					S34HyoukaNinzuListBean s34HyoukaNinzuListBean = new S34HyoukaNinzuListBean();
					
					if( intLoopCnt2 == 0 ){
						HomeDataBean = s34ClassListBean;
						s34ClassListBean = (S34ClassListBean)itr2.next();
						s34HyoukaNinzuList = s34ClassListBean.getS34HyoukaNinzuList();
						intNendoCount = s34HyoukaNinzuList.size();
						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
						intLoopCnt2++;
					}
					ListIterator itr3 = s34HyoukaNinzuList.listIterator();
//					ArrayList s34HyoukaNinzuList = s34ClassListBean.getS34HyoukaNinzuList();
//					S34HyoukaNinzuListBean s34HyoukaNinzuListBean = new S34HyoukaNinzuListBean();
//					ListIterator itr3 = s34HyoukaNinzuList.listIterator();
//
//					// 自校情報表示判定
//					if( intLoopCnt2 == 0 ){
//						bolHomeDataDispFlg = true;
//						HomeDataBean = s34ClassListBean;
//						s34ClassListBean = (S34ClassListBean)itr2.next();
//						intNendoCount = s34HyoukaNinzuList.size();
//						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
//						intLoopCnt2++;
//					}

					// 始点情報（Col）
					if( intLoopCnt2 % intMaxClassSr == 0 ){
						intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 5 );
					}else{
						intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 5 );
					}
					
//					intCol = intDataStartCol + ( ( intMaxClassSr - ( intLoopCnt2 % intMaxClassSr ) ) * 5 );

					/** 年度・志望者数 **/
					// S34HyoukaNinzuListBean
					int intHyoukaNinzuListIndex = 0;
					while( itr3.hasNext() ){
						s34HyoukaNinzuListBean = (S34HyoukaNinzuListBean)itr3.next();

						// 規定行数を表示したら線を引く
						if(( intSetRow  >= intChangeSheetRow )||((bolSheetCngFlg == true)&&(intMaxSheetIndex != 0))){
							bolSheetCngFlg = true;
							for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//								int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
								int intBIndex = (intSindex + 1) / intMaxSheetSr;
								if((intSindex + 1)%intMaxSheetSr!=0){
									intBIndex++;
								}
								workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
								workSheet = (HSSFSheet)WorkSheetList.get(intSindex);
								
								// 罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intSetRow, intSetRow, 0, 33, false, true, false, false );
								if( workSheet != null ){
									int intRemoveStartRow = intChangeSheetRow;
									if( intMaxRow + intDataStartRow > intRemoveStartRow ){
										for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
											//行削除処理
											workSheet.removeRow(workSheet.getRow(index));
										}
										//罫線出力（セルの上部に太線を出力）
										cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
														intRemoveStartRow, intRemoveStartRow, 0, 33, false, true, false, false);
									}
								}
			
							}

							// 保存処理
							int intSheetNum = 0;
							switch( intSaveFlg ){
								case 1:
								case 5:
									intSheetNum = ((HSSFWorkbook)WorkBookList.get(0)).getNumberOfSheets()-1;
									break;
								case 2:
								case 3:
								case 4:
									intSheetNum = ((HSSFWorkbook)WorkBookList.get(0)).getNumberOfSheets()-2;
									break;
							}

							if( intSheetNum >= intMaxSheetSr ){
								tmpworkbook = (HSSFWorkbook)WorkBookList.get(0);
								
								boolean bolRet = false;
								// Excelファイル保存
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, tmpworkbook, UserID, intBookCngCount, masterfile, intSheetNum);

								if( bolRet == false ){
									return errfwrite;
								}
								intBookCngCount++;
								
								// 削除
								WorkBookList.remove(0);
								for(int a=0; a<intMaxSheetSr;a++){
									WorkSheetList.remove(0);
								}
								tmpworkbook=null;
								intBookIndex--;
								intSheetIndex -= intMaxSheetSr;
							}
						}

						// 改シート判定
						if( bolSheetCngFlg == true ){
							
							bolDispHyoukaFlg = true;
							bolDispDaigakuFlg = true;
														
							intSetRow = intDataStartRow;
							bolClassDispFlg = true;
							
							for( int intIndex = 0; intIndex < intSheetSr; intIndex ++ ){
								
								if( ( intMaxSheetIndex >= intMaxSheetSr ) || ( bolBookCngFlg == true ) ){
									// マスタExcel読み込み
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook==null ){
										return errfread;
									}
									WorkBookList.add(workbook);
									intBookIndex++;
					
									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
									workbook = (HSSFWorkbook)WorkBookList.get(intBookIndex);
								}
								
								//シート作成
								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);
								WorkSheetList.add(workSheet);
								intSheetIndex++;
								workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex);

								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);
				
								// セキュリティスタンプセット
								String secuFlg = cm.setSecurity( workbook, workSheet, s34Item.getIntSecuFlg(), 31, 33 );
								workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
								workCell.setCellValue(secuFlg);
				
								// 学校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
								workCell.setCellValue( "学校名　：" + cm.toString(s34Item.getStrGakkomei()) );
		
								// 模試月取得
								String moshi =cm.setTaisyouMoshi( s34Item.getStrMshDate() );
								
								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
								workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s34Item.getStrMshmei()) + moshi);
					
								//表示対象
								workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
								workCell.setCellValue( "表示対象：" + strDispTarget );
								
								//評価別人数 見出し行
								for( int i = 0; i <= intMaxClassSr; i++ ){
									int intTitleSetCol = (i * 5) + ( intDataStartCol - 3 );
									
									//出願予定・第1志望
									if (strKokushiKbn == "国公立大") { 
										workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
										workCell.setCellValue( "出願予定" );
									} else {
										workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
										workCell.setCellValue( "第１志望" );
									}
								
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol );
									workCell.setCellValue( (String)HyoukaTitleList.get(0) );
									
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+1 );
									workCell.setCellValue( (String)HyoukaTitleList.get(1) );
									
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+2 );
									workCell.setCellValue( (String)HyoukaTitleList.get(2) );
								}
							
								intMaxSheetIndex++;		//シート数カウンタインクリメント
								bolSheetCngFlg = false;
						
								intLoopCnt = 0;
							}	//ForEnd
							
						}	//if( bolSheetCngFlg == true )

//						intSheetCnt = (int)Math.ceil( (double)intLoopCnt2 / (double)intMaxClassSr );
						intSheetCnt = intLoopCnt2 / intMaxClassSr;
						if(intLoopCnt2%intMaxClassSr!=0){
							intSheetCnt++;
						}
						workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex -  (intSheetSr - intSheetCnt)  );

						int intSheetNo = intSheetIndex -  (intSheetSr - intSheetCnt);
//						int intBIndex = (int)Math.ceil((double)(intSheetNo + 1) / (double)intMaxSheetSr );
						int intBIndex = (intSheetNo + 1) / intMaxSheetSr;
						if((intSheetNo + 1)%intMaxSheetSr!=0){
							intBIndex++;
						}
						workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
										

						
						if( intCol == intDataStartCol ){
							
							//データセット
							//自校データ・大学名ほか表示
							//自校学校名
							if( HomeDataBean.getStrGakkomei()!=null ){
								workCell = cm.setCell( workSheet, workRow, workCell, 5, 4 );
								workCell.setCellValue(HomeDataBean.getStrGakkomei());
							}
							//自校データ
							ListIterator itrHome = HomeDataBean.getS34HyoukaNinzuList().listIterator();
							int intLoopIndex = 0;
							while( itrHome.hasNext() ){
								S34HyoukaNinzuListBean HomeHyoukaNinzuListBean = (S34HyoukaNinzuListBean) itrHome.next();
								boolean ret = setData( workSheet, HomeHyoukaNinzuListBean, intSetRow + intLoopIndex , 4 );
								if( ret == false ){
									return errfdata;
								}
								intLoopIndex++;
							}
							
							if( bolDispHyoukaFlg == true ){
								//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
															intSetRow, intSetRow, 2, 2, false, true, false, false);
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( strHyouka );
				
							}
							
							if( bolDispGkbFlg == true ){
								//罫線出力(セルの上部（学部の列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
															intSetRow, intSetRow, 1, 2, false, true, false, false);
								//学部表示
								if( s34ListBean.getStrGakubuMei()!=null ){
									workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
									workCell.setCellValue( s34ListBean.getStrGakubuMei() );
								}
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( strHyouka );
				
							}

							if( bolDispDaigakuFlg == true ){
								//罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intSetRow, intSetRow, 0, 33, false, true, false, false );
								//大学名表示
								if(s34ListBean.getStrDaigakuMei()!=null){
									workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 0 );
									workCell.setCellValue(s34ListBean.getStrDaigakuMei());
								}
								//学部表示
								if( s34ListBean.getStrGakubuMei()!=null ){
									workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
									workCell.setCellValue( s34ListBean.getStrGakubuMei() );
								}
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( strHyouka );
							}
							
						}
					
						boolean ret = setData( workSheet, s34HyoukaNinzuListBean, intSetRow + intHyoukaNinzuListIndex, intCol);
						if( ret != true){
							return errfdata;
						}
						else{
							intHyoukaNinzuListIndex++;
							intLoopCnt++;
						}
						
						//
						
					}	//WhileEnd( itr3 )
					
					
					//クラス名
					if( bolClassDispFlg == true ){
						String strGakunen = s34ClassListBean.getStrGrade();
						String strClass = s34ClassListBean.getStrClass();
						workCell = cm.setCell( workSheet, workRow, workCell, 5, intCol );
						workCell.setCellValue(cm.toString(strGakunen) + "年 " + cm.toString(strClass) + "クラス");
					}
					intLoopCnt2++;
				}	//WhileEnd( itr2 )

				intSetRow += intNendoCount;
				bolClassDispFlg = false;

				log.It("S34_03","大学名：",s34ListBean.getStrDaigakuMei());

			}	//WhileEnd( itr )
			
			for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//				int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr ); /** 20040930 **/
				int intBIndex = (intSindex + 1) / intMaxSheetSr;
				if((intSindex + 1)%intMaxSheetSr!=0){
					intBIndex++;
				}
				workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
				workSheet = (HSSFSheet)WorkSheetList.get(intSindex);
								
				//罫線出力(セルの上部に太線を引く）
				cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
													intSetRow, intSetRow, 0, 33, false, true, false, false );
				if( workSheet != null ){
					int intRemoveStartRow = intChangeSheetRow;
					if( intMaxRow + intDataStartRow > intRemoveStartRow ){
						for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
							//行削除処理
							workSheet.removeRow(workSheet.getRow(index));
						}
						//罫線出力（セルの上部に太線を出力）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
										intRemoveStartRow, intRemoveStartRow, 0, 33, false, true, false, false);
					}
				}
			
			}

			//ファイル保存
			for( int intBookSr = 0; intBookSr < WorkBookList.size(); intBookSr++ ){
				workbook = (HSSFWorkbook)WorkBookList.get(intBookSr);

				boolean bolRet = false;
				// Excelファイル保存
				if( (intBookCngCount != 1)||((intBookCngCount == 1)&&(WorkBookList.size()>1)) ){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, intMaxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetSr);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}
			
			//データ無し
			if ( WorkBookList.size() == 0 ){
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex = 1;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s34Item.getIntSecuFlg(), 31, 33 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
				workCell.setCellValue(secuFlg);
				
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s34Item.getStrGakkomei()) );
		
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s34Item.getStrMshDate() );
								
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + s34Item.getStrMshmei() + moshi);

				//表示対象
				workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
				workCell.setCellValue( "表示対象：" );
			}
			
		}
		catch( Exception e ){
			log.Err("S34_03","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S34_03","S34_03帳票作成終了","");
		return noerror;
	}

	/**
	 * S34Item加算・編集
	 * @param s34Item
	 * @return
	 */
	private S34Item editS34( S34Item s34Item ) throws Exception{
		try{
			S34Item		s34ItemEdt	= new S34Item();
			String		daigakuCd	= "";
			String 		genekiKbn 	= "";
			boolean	centerFlg	= false;
			boolean	secondFlg	= false;
			boolean	totalFlg	= false;

			ArrayList s34ClassListAdd		= new ArrayList();
			ArrayList s34ClassListAddCnt	= new ArrayList();
			ArrayList s34ClassListAddSnd	= new ArrayList();
			ArrayList s34ClassListAddTtl	= new ArrayList();
			ArrayList s34NinzuListAdd		= new ArrayList();

			s34ItemEdt.setStrGakkomei( s34Item.getStrGakkomei() );
			s34ItemEdt.setStrMshmei( s34Item.getStrMshmei() );
			s34ItemEdt.setStrMshDate( s34Item.getStrMshDate() );
			s34ItemEdt.setStrMshCd( s34Item.getStrMshCd() );
			s34ItemEdt.setIntSecuFlg( s34Item.getIntSecuFlg() );
			s34ItemEdt.setIntDaiTotalFlg( s34Item.getIntDaiTotalFlg() );

			S34ListBean s34ListBeanAddCnt = new S34ListBean();
			S34ListBean s34ListBeanAddSnd = new S34ListBean();
			S34ListBean s34ListBeanAddTtl = new S34ListBean();

			/** 大学・学部・学科・日程・評価 **/
			ArrayList s34List		= s34Item.getS34List();
			ArrayList s34ListEdt	= new ArrayList();
			ListIterator itr = s34List.listIterator();
			while( itr.hasNext() ){
				S34ListBean s34ListBean		= (S34ListBean) itr.next();
				S34ListBean s34ListBeanEdt	= new S34ListBean();

				// 大学コードが変わる→大学計の保存
//////////////////
//				if( !cm.toString(daigakuCd).equals( cm.toString(s34ListBean.getStrDaigakuCd()) ) ){
//////////////////
				if( (!cm.toString(daigakuCd).equals( cm.toString(s34ListBean.getStrDaigakuCd())))
					||( !cm.toString(genekiKbn).equals(cm.toString(s34ListBean.getStrGenKouKbn()))) ){
					genekiKbn = s34ListBean.getStrGenKouKbn();

					daigakuCd = s34ListBean.getStrDaigakuCd();

					if(centerFlg == true){
						s34ListBeanAddCnt.setS34ClassList( s34ClassListAddCnt );
						s34ListEdt.add( s34ListBeanAddCnt );
						s34ClassListAddCnt = new ArrayList();
						centerFlg = false;
					}
					if(secondFlg == true){
						s34ListBeanAddSnd.setS34ClassList( s34ClassListAddSnd );
						s34ListEdt.add( s34ListBeanAddSnd );
						s34ClassListAddSnd = new ArrayList();
						secondFlg = false;
					}
					if(totalFlg == true){
						s34ListBeanAddTtl.setS34ClassList( s34ClassListAddTtl );
						s34ListEdt.add( s34ListBeanAddTtl );
						s34ClassListAddTtl = new ArrayList();
						totalFlg = false;
					}
					s34ListBeanAddCnt = new S34ListBean();
					s34ListBeanAddSnd = new S34ListBean();
					s34ListBeanAddTtl = new S34ListBean();
					s34NinzuListAdd = new ArrayList();
				}

				// 基本部分の移し変え
				s34ListBeanEdt.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
				s34ListBeanEdt.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
				s34ListBeanEdt.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
				s34ListBeanEdt.setStrGakubuCd( s34ListBean.getStrGakubuCd() );
				s34ListBeanEdt.setStrGakubuMei( s34ListBean.getStrGakubuMei() );
				s34ListBeanEdt.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
				s34ListBeanEdt.setStrGakkaMei( s34ListBean.getStrGakkaMei() );
				s34ListBeanEdt.setStrNtiCd( s34ListBean.getStrNtiCd() );
				s34ListBeanEdt.setStrNittei( s34ListBean.getStrNittei() );
				s34ListBeanEdt.setStrHyouka( s34ListBean.getStrHyouka() );

				// 小計値の保存部
				switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
					case 1:
						s34ListBeanAddCnt.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanAddCnt.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanAddCnt.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanAddCnt.setStrGakubuCd( "GKB-DAI" );
						s34ListBeanAddCnt.setStrGakubuMei( "大学計" );
						s34ListBeanAddCnt.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanAddCnt.setStrGakkaMei( s34ListBean.getStrGakkaMei() );
						s34ListBeanAddCnt.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanAddCnt.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanAddCnt.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListAdd = deepCopyCl( s34ClassListAddCnt );
						centerFlg = true;
						break;
					case 2:
						s34ListBeanAddSnd.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanAddSnd.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanAddSnd.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanAddSnd.setStrGakubuCd( "GKB-DAI" );
						s34ListBeanAddSnd.setStrGakubuMei( "大学計" );
						s34ListBeanAddSnd.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanAddSnd.setStrGakkaMei( s34ListBean.getStrGakkaMei() );
						s34ListBeanAddSnd.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanAddSnd.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanAddSnd.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListAdd = deepCopyCl( s34ClassListAddSnd );
						secondFlg = true;
						break;
					case 3:
						s34ListBeanAddTtl.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanAddTtl.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanAddTtl.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanAddTtl.setStrGakubuCd( "GKB-DAI" );
						s34ListBeanAddTtl.setStrGakubuMei( "大学計" );
						s34ListBeanAddTtl.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanAddTtl.setStrGakkaMei( s34ListBean.getStrGakkaMei() );
						s34ListBeanAddTtl.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanAddTtl.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanAddTtl.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListAdd = deepCopyCl( s34ClassListAddTtl );
						totalFlg = true;
						break;
				}

				// 
				ArrayList s34ClassList		= s34ListBean.getS34ClassList();
				ArrayList s34ClassListEdt	= new ArrayList();
				ListIterator itrClass = s34ClassList.listIterator();
				int b = 0;
				boolean firstFlg3 = true;
				if(s34ClassListAdd.size() == 0){
					firstFlg3 = false;
				}
				/** 学校・学年・クラス **/
				while( itrClass.hasNext() ){
					S34ClassListBean s34ClassListBean = (S34ClassListBean)itrClass.next();
					S34ClassListBean s34ClassListBeanEdt = new S34ClassListBean();
					S34ClassListBean s34ClassListBeanAdd = null;

					// 移し変え
					s34ClassListBeanEdt.setStrGakkomei( s34ClassListBean.getStrGakkomei() );
					s34ClassListBeanEdt.setStrGrade( s34ClassListBean.getStrGrade() );
					s34ClassListBeanEdt.setStrClass( s34ClassListBean.getStrClass() );

					// 小計用データ呼び出し
					if(firstFlg3 == true){
						s34ClassListBeanAdd = (S34ClassListBean) s34ClassListAdd.get(b);
						s34NinzuListAdd = deepCopyNz( s34ClassListBeanAdd.getS34HyoukaNinzuList() );
					}else{
						s34ClassListBeanAdd = new S34ClassListBean();

						s34ClassListBeanAdd.setStrGakkomei( s34ClassListBean.getStrGakkomei() );
						s34ClassListBeanAdd.setStrGrade( s34ClassListBean.getStrGrade() );
						s34ClassListBeanAdd.setStrClass( s34ClassListBean.getStrClass() );
					}

					/** 年度・人数 **/
					ArrayList s34NinzuList = s34ClassListBean.getS34HyoukaNinzuList();
					ArrayList s34NinzuListEdt = new ArrayList();
					ListIterator itrNinzu = s34NinzuList.listIterator();
					int a = 0;
					boolean firstFlg2 = true;
					if(s34NinzuListAdd.size() == 0){
						firstFlg2 = false;
					}
					while( itrNinzu.hasNext() ){
						S34HyoukaNinzuListBean s34NinzuBean = (S34HyoukaNinzuListBean)itrNinzu.next();
						S34HyoukaNinzuListBean s34NinzuBeanEdt = new S34HyoukaNinzuListBean();
						S34HyoukaNinzuListBean s34NinzuBeanAdd = null;

						// 移し変え
						s34NinzuBeanEdt.setStrNendo( s34NinzuBean.getStrNendo() );
						s34NinzuBeanEdt.setIntSoushibo( s34NinzuBean.getIntSoushibo() );
						s34NinzuBeanEdt.setIntDai1shibo( s34NinzuBean.getIntDai1shibo() );
						s34NinzuBeanEdt.setIntHyoukaA( s34NinzuBean.getIntHyoukaA() );
						s34NinzuBeanEdt.setIntHyoukaB( s34NinzuBean.getIntHyoukaB() );
						s34NinzuBeanEdt.setIntHyoukaC( s34NinzuBean.getIntHyoukaC() );

						s34NinzuListEdt.add( s34NinzuBeanEdt );

						// 加算処理
						if(firstFlg2 == true){
							s34NinzuBeanAdd = (S34HyoukaNinzuListBean) s34NinzuListAdd.get(a);
						}else{
							s34NinzuBeanAdd = new S34HyoukaNinzuListBean();

							s34NinzuBeanAdd.setStrNendo( s34NinzuBean.getStrNendo() );
//							s34NinzuBeanAdd.setIntSoushibo( 0 );
//							s34NinzuBeanAdd.setIntDai1shibo( 0 );
//							s34NinzuBeanAdd.setIntHyoukaA( 0 );
//							s34NinzuBeanAdd.setIntHyoukaB( 0 );
//							s34NinzuBeanAdd.setIntHyoukaC( 0 );
							s34NinzuBeanAdd.setIntSoushibo( -999 );
							s34NinzuBeanAdd.setIntDai1shibo( -999 );
							s34NinzuBeanAdd.setIntHyoukaA( -999 );
							s34NinzuBeanAdd.setIntHyoukaB( -999 );
							s34NinzuBeanAdd.setIntHyoukaC( -999 );
						}

						if(s34NinzuBean.getIntSoushibo()!=-999){
//							s34NinzuBeanAdd.setIntSoushibo( s34NinzuBeanAdd.getIntSoushibo() + s34NinzuBean.getIntSoushibo() );
							if(s34NinzuBeanAdd.getIntSoushibo()!=-999){
								s34NinzuBeanAdd.setIntSoushibo(s34NinzuBeanAdd.getIntSoushibo() + s34NinzuBean.getIntSoushibo());
							}else{
								s34NinzuBeanAdd.setIntSoushibo(s34NinzuBean.getIntSoushibo());
							}
						}
						if(s34NinzuBean.getIntDai1shibo()!=-999){
//							s34NinzuBeanAdd.setIntDai1shibo( s34NinzuBeanAdd.getIntDai1shibo() + s34NinzuBean.getIntDai1shibo() );
							if(s34NinzuBeanAdd.getIntDai1shibo()!=-999){
								s34NinzuBeanAdd.setIntDai1shibo(s34NinzuBeanAdd.getIntDai1shibo() + s34NinzuBean.getIntDai1shibo());
							}else{
								s34NinzuBeanAdd.setIntDai1shibo(s34NinzuBean.getIntDai1shibo());
							}
						}
						if(s34NinzuBean.getIntHyoukaA()!=-999){
//							s34NinzuBeanAdd.setIntHyoukaA( s34NinzuBeanAdd.getIntHyoukaA() + s34NinzuBean.getIntHyoukaA() );
							if(s34NinzuBeanAdd.getIntHyoukaA()!=-999){
								s34NinzuBeanAdd.setIntHyoukaA(s34NinzuBeanAdd.getIntHyoukaA() + s34NinzuBean.getIntHyoukaA());
							}else{
								s34NinzuBeanAdd.setIntHyoukaA(s34NinzuBean.getIntHyoukaA());
							}
						}
						if(s34NinzuBean.getIntHyoukaB()!=-999){
//							s34NinzuBeanAdd.setIntHyoukaB( s34NinzuBeanAdd.getIntHyoukaB() + s34NinzuBean.getIntHyoukaB() );
							if(s34NinzuBeanAdd.getIntHyoukaB()!=-999){
								s34NinzuBeanAdd.setIntHyoukaB(s34NinzuBeanAdd.getIntHyoukaB() + s34NinzuBean.getIntHyoukaB());
							}else{
								s34NinzuBeanAdd.setIntHyoukaB(s34NinzuBean.getIntHyoukaB());
							}
						}
						if(s34NinzuBean.getIntHyoukaC()!=-999){
//							s34NinzuBeanAdd.setIntHyoukaC( s34NinzuBeanAdd.getIntHyoukaC() + s34NinzuBean.getIntHyoukaC() );
							if(s34NinzuBeanAdd.getIntHyoukaC()!=-999){
								s34NinzuBeanAdd.setIntHyoukaC(s34NinzuBeanAdd.getIntHyoukaC() + s34NinzuBean.getIntHyoukaC());
							}else{
								s34NinzuBeanAdd.setIntHyoukaC(s34NinzuBean.getIntHyoukaC());
							}
						}

						if(firstFlg2 == true){
							s34NinzuListAdd.set(a,s34NinzuBeanAdd);
						}else{
							s34NinzuListAdd.add(s34NinzuBeanAdd);
						}
						a++;
					}

					// 格納
					s34ClassListBeanEdt.setS34HyoukaNinzuList( s34NinzuListEdt );
					s34ClassListEdt.add( s34ClassListBeanEdt );

					// 計算結果格納
					s34ClassListBeanAdd.setS34HyoukaNinzuList( deepCopyNz(s34NinzuListAdd) );
					s34NinzuListAdd = new ArrayList();
					if(firstFlg3 == true){
						s34ClassListAdd.set(b,s34ClassListBeanAdd);
					}else{
						s34ClassListAdd.add(s34ClassListBeanAdd);
					}
					b++;
				}
				// 格納
				s34ListBeanEdt.setS34ClassList( s34ClassListEdt );
				s34ListEdt.add( s34ListBeanEdt );
				switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
					case 1:
						s34ClassListAddCnt = deepCopyCl( s34ClassListAdd );
						break;
					case 2:
						s34ClassListAddSnd = deepCopyCl( s34ClassListAdd );
						break;
					case 3:
						s34ClassListAddTtl = deepCopyCl( s34ClassListAdd );
						break;
				}
				s34ClassListAdd = new ArrayList();
			}
			// 小計の保存
			if(centerFlg == true){
				s34ListBeanAddCnt.setS34ClassList( s34ClassListAddCnt );
				s34ListEdt.add( s34ListBeanAddCnt );
				s34ClassListAddCnt = new ArrayList();
				centerFlg = false;
			}
			if(secondFlg == true){
				s34ListBeanAddSnd.setS34ClassList( s34ClassListAddSnd );
				s34ListEdt.add( s34ListBeanAddSnd );
				s34ClassListAddSnd = new ArrayList();
				secondFlg = false;
			}
			if(totalFlg == true){
				s34ListBeanAddTtl.setS34ClassList( s34ClassListAddTtl );
				s34ListEdt.add( s34ListBeanAddTtl );
				s34ClassListAddTtl = new ArrayList();
				totalFlg = false;
			}
			// 格納
			s34ItemEdt.setS34List( s34ListEdt );

			return s34ItemEdt;
		}catch(Exception e){
			throw e;
		}
	}

	/**
	 * 
	 * @param s34NinzuList
	 * @return
	 */
	private ArrayList deepCopyNz(ArrayList s34NinzuList){
		ListIterator itrNinzu = s34NinzuList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrNinzu.hasNext()){
			S34HyoukaNinzuListBean s34NinzuBean = (S34HyoukaNinzuListBean)itrNinzu.next();
			S34HyoukaNinzuListBean s34NinzuBeanCopy = new S34HyoukaNinzuListBean();

			s34NinzuBeanCopy.setStrNendo( s34NinzuBean.getStrNendo() );
			s34NinzuBeanCopy.setIntSoushibo( s34NinzuBean.getIntSoushibo() );
			s34NinzuBeanCopy.setIntDai1shibo( s34NinzuBean.getIntDai1shibo() );
			s34NinzuBeanCopy.setIntHyoukaA( s34NinzuBean.getIntHyoukaA() );
			s34NinzuBeanCopy.setIntHyoukaB( s34NinzuBean.getIntHyoukaB() );
			s34NinzuBeanCopy.setIntHyoukaC( s34NinzuBean.getIntHyoukaC() );

			copyList.add( s34NinzuBeanCopy );
		}
		
		return copyList;
	}

	/**
	 * 
	 * @param s34ClassList
	 * @return
	 */
	private ArrayList deepCopyCl(ArrayList s34ClassList){
		ListIterator itrClass = s34ClassList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrClass.hasNext()){
			S34ClassListBean s34ClassBean = (S34ClassListBean)itrClass.next();
			S34ClassListBean s34ClassBeanCopy = new S34ClassListBean();

			s34ClassBeanCopy.setStrClass( s34ClassBean.getStrClass() );
			s34ClassBeanCopy.setStrGakkomei( s34ClassBean.getStrGakkomei() );
			s34ClassBeanCopy.setStrGrade( s34ClassBean.getStrGrade() );
			s34ClassBeanCopy.setS34HyoukaNinzuList( deepCopyNz(s34ClassBean.getS34HyoukaNinzuList()) );

			copyList.add( s34ClassBeanCopy );
		}
		
		return copyList;
	}

	/*
	 * 	Excel編集
	 * 		HSSFWorkbook 	workbook: Excelワークブック
	 * 		ArrayList 		s34HyoukaNinzuList:評価別人数リスト
	 * 		int				intRow:開始行
	 * 		int				intCol:開始列
	 * 		戻り値: なし
	 */
	private boolean setData(HSSFSheet workSheet, S34HyoukaNinzuListBean s34HyoukaNinzuListBean, int intRow, int intCol ){

		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		int		row = intRow;
		
		
		// 基本ファイルを読込む
		try{
			
			//模試年度
			if( s34HyoukaNinzuListBean.getStrNendo()!=null ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, 3);
				workCell.setCellValue( s34HyoukaNinzuListBean.getStrNendo() + "年度" );
			}
			
			//全国総志望者数
			if( s34HyoukaNinzuListBean.getIntSoushibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol);
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntSoushibo() );
			}
			
			//志望者数（第一志望）
			if( s34HyoukaNinzuListBean.getIntDai1shibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 1);
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntDai1shibo() );
			}
			
			//評価別人数
			if( s34HyoukaNinzuListBean.getIntHyoukaA() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 2 );
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntHyoukaA() );
			}

			if( s34HyoukaNinzuListBean.getIntHyoukaB() != -999 ){			
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntHyoukaB() );
			}
			
			if( s34HyoukaNinzuListBean.getIntHyoukaC() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntHyoukaC() );
			}

		}
		catch(Exception e){
			return false;
		}

		return true;
	}
	
}
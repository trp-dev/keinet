/**
 * 校内成績分析−クラス比較　設問別成績（クラス比較）
 *      小設問別正答状況 データクラス
 * 作成日: 2019/09/11
 * @author      M.Ooseto
 */

package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;

public class S33StatusBean {
    // 学校名
    private String bundleName = "";
    // 対象模試名
    private String examName = "";
    // 対象学年
    private int tergetGrade = 0;
    // 校内全体 データリスト
    private ArrayList S33StatusList = new ArrayList();
    // クラス データリスト
    private ArrayList S33StatusClassList = new ArrayList();

    /*-----*/
    /* Get */
    /*-----*/
    public String getBundleName() {
        return this.bundleName;
    }
    public String getExamName() {
        return this.examName;
    }
    public int getTergetGrade() {
        return this.tergetGrade;
    }
    public ArrayList getS33StatusList() {
        return this.S33StatusList;
    }
    public ArrayList getS33StatusClassList() {
        return this.S33StatusClassList;
    }

    /*-----*/
    /* Set */
    /*-----*/
    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }
    public void setExamName(String examName) {
        this.examName = examName;
    }
    public void setTergetGrade(int tergetGrade) {
        this.tergetGrade = tergetGrade;
    }
    public void setS33StatusList(ArrayList S33StatusList) {
        this.S33StatusList = S33StatusList;
    }
    public void setS33StatusClassList(ArrayList S33StatusClassList) {
        this.S33StatusClassList = S33StatusClassList;
    }
}

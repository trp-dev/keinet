package jp.co.fj.keinavi.beans.maintenance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.beans.admin.name.NameBringsNear;
import jp.co.fj.keinavi.beans.maintenance.module.StudentDataValidator;
import jp.co.fj.keinavi.data.maintenance.KNStudentData;
import jp.co.fj.keinavi.util.message.MessageLoader;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 生徒情報登録Bean
 * 
 * 2006.11.02 [新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class StudentInsertBean extends DefaultBean {

	// 生年月日のフォーマッタ
	private final DateFormat birthDayFormat = new SimpleDateFormat("yyyyMMdd");
	// 名寄せモジュール
	private final NameBringsNear name = new NameBringsNear();
	// ログ出力モジュール
	private final MainteLogBean log;

	// 学校コード
	private final String schoolCd;
	// 生徒データ
	private KNStudentData student;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 学校コード
	 */
	public StudentInsertBean(final String pSchoolCd) {
		schoolCd = pSchoolCd;
		log = new MainteLogBean(pSchoolCd);
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		// データチェック
		validate();
		
		// 名寄せ実行
		// 1 ... 編集
		// 2 ... 進級
		// 3 ... 新規
		identify();
		
		// 進級または編集なら個人IDは決まってる
		if (name.getCombiJudge() < 3) {
			student.setIndividualId((String) name.getResult().get(0));
		// 新規なら個人IDを割り振る
		} else {
			student.setIndividualId(createIndividualId(student.getYear()));
		}
	
		// 重複チェック
		if (isDuplication(student.getIndividualId(), 
				student.getYear(), student.getGrade(),
				student.getClassName(), student.getClassNo())) {
			throw new KNBeanException(
					MessageLoader.getInstance().getMessage("m011a"));
		}
		
		// 編集
		if (name.getCombiJudge() == 1) {
			// 学籍基本情報
			if (!updateBasicinfo()) {
				throw new Exception("学籍基本情報の更新に失敗しました。");
			}
			// 変更前履歴情報取得
			getOldHistoryinfo();
			// 学籍履歴情報
			updateHistoryinfo(student.getIndividualId(),
					student.getYear(), student.getGrade(),
					student.getClassName(),
					student.getClassNo(), student.getOCurrentGrade(),
					student.getOCurrentClassName());
			// ログ出力
			updateLog(student.getIndividualId(),
					student.getYear(), student.getGrade(),
					student.getClassName(), student.getClassNo(),
					student.getNameKana());
			// 集計学年・クラスの変更
			updateCntGrade();
		// 進級処理
		} else if (name.getCombiJudge() == 2) {
			// 学籍履歴情報のみ登録
			insertHistoryinfo();
			// ログ出力
			updateLog(student.getIndividualId(),
					student.getYear(), student.getGrade(),
					student.getClassName(), student.getClassNo(),
					student.getNameKana());
		// 新規登録
		} else {
			// 学籍基本情報
			insertBasicinfo();
			// 学籍履歴情報
			insertHistoryinfo();
			// ログ出力
			insertLog(student.getIndividualId(),
					student.getYear(), student.getGrade(),
					student.getClassName(), student.getClassNo(),
					student.getNameKana());
		}
		
		// 識別マーク
		updateCMark();
	}

	/**
	 * 生徒情報の検査
	 * 
	 * @throws Exception 不正データ例外
	 */
	void validate() throws Exception {
		StudentDataValidator.getInstance().validate(student);		
	}

	/**
	 * 名寄せする
	 * 
	 * @return モジュールインスタンス
	 * @throws SQLException
	 */
	NameBringsNear identify() throws SQLException {
		name.setInputData(createIdentificationData());
		name.execute();
		return name;
	}

	// 名寄せ処理用のデータを作る
	private HashMap createIdentificationData() {
		
		// 生年月日
		final String birthday = birthDayFormat.format(student.getBirthday());
		
		final HashMap inputData = new HashMap();
		inputData.put("YEAR02", student.getYear().substring(2, 4));
		inputData.put("EXAMYEAR", student.getYear());
		inputData.put("GRADE", String.valueOf(student.getGrade()));
		inputData.put("CLASS", student.getClassName());
		inputData.put("CLASS_NO", student.getClassNo());
		inputData.put("SEX", student.getSex());
		inputData.put("NAME_KANA", student.getNameKana());
		inputData.put("NAME_KANJI", student.getNameKanji());
		inputData.put("BIRTHYEAR", birthday.substring(0, 4));
		inputData.put("BIRTHMONTH", birthday.substring(4, 6));
		inputData.put("BIRTHDATE", birthday.substring(6, 8));
		inputData.put("TEL_NO", student.getTelNo());
		inputData.put("SCHOOLCD", schoolCd);
		inputData.put("HOMESCHOOLCD", schoolCd);
		inputData.put("CUSTOMER_NO", "99999999999");
		return inputData;
	}

	// 変更前学年・クラスを取得する
	private void getOldHistoryinfo() throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					"SELECT grade, class FROM historyinfo "
					+ "WHERE individualid = ? AND year = ?");
			ps.setString(1, student.getIndividualId());
			ps.setString(2, student.getYear());
			rs = ps.executeQuery();
			if (rs.next()) {
				student.setOCurrentGrade(rs.getInt(1));
				student.setOCurrentClassName(rs.getString(2));
			} else {
				throw new SQLException(
						"変更前学年クラスの取得に失敗しました。");
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * 学籍基本情報を登録する
	 * 
	 * @throws SQLException
	 */
	void insertBasicinfo() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("m28_1"));
			ps.setString(1, student.getIndividualId());
			ps.setString(2, schoolCd);
			ps.setString(3, schoolCd);
			ps.setString(4, student.getNameKana());
			ps.setString(5, student.getNameKanji());
			ps.setString(6, student.getSex());
			ps.setString(7,
					birthDayFormat.format(student.getBirthday()));
			ps.setString(8, student.getTelNo());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 学籍履歴情報を登録する
	 * 
	 * @throws SQLException
	 */
	void insertHistoryinfo() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("m28_2"));
			ps.setString(1, student.getIndividualId());
			ps.setString(2, student.getYear());
			ps.setInt(3, student.getGrade());
			ps.setString(4, student.getClassName());
			ps.setString(5, student.getClassNo());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 識別マーク更新
	 * 
	 * @throws SQLException
	 */
	void updateCMark() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					"UPDATE basicinfo SET cmark = '1' "
					+ "WHERE individualid = ?");
			ps.setString(1, student.getIndividualId());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 学籍基本情報を更新する
	 * 
	 * @throws SQLException
	 */
	boolean updateBasicinfo() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("m28_3"));
			ps.setString(1, student.getNameKana());
			ps.setString(2, student.getNameKanji());
			ps.setString(3, student.getSex());
			ps.setString(4,
					birthDayFormat.format(student.getBirthday()));
			ps.setString(5, student.getTelNo());
			ps.setString(6, student.getIndividualId());
			ps.setString(7, schoolCd);
			return ps.executeUpdate() == 1;
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 学籍履歴情報を更新する
	 * 
	 * @param individualId 個人ID
	 * @param year 年度
	 * @param grade 学年
	 * @param className 変更後クラス
	 * @param classNo 変更後クラス番号
	 * @param oGrade 変更前学年
	 * @param oClassName 変更前クラス
	 * @throws Exception SQL例外
	 */
	boolean updateHistoryinfo(final String individualId,
			final String year,	final int grade,
			final String className, final String classNo,
			final int oGrade, final String oClassName) throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("m28_4"));
			ps.setInt(1, grade);
			ps.setString(2, className);
			ps.setString(3, classNo);
			ps.setString(4, individualId);
			ps.setString(5, year);
			if (ps.executeUpdate() == 0) {
				return false;
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}
		
		// 再集計対象を登録する
		registRecount(individualId, year, grade,
				className, oGrade, oClassName);
		
		return true;
	}

	// 再集計対象を登録する
	private void registRecount(final String individualId, final String year,
			final int grade, final String className, final int oGrade,
			final String oClassName) throws SQLException {
		
		try {
			final RecountRegisterBean bean = new RecountRegisterBean();
			bean.setConnection(null, conn);
			bean.setYear(year);
			bean.setIndividualId(individualId);
			bean.setAfterGrade(grade);
			bean.setAfterClassName(className);
			bean.setBeforeGrade(oGrade);
			bean.setBeforeClassName(oClassName);
			bean.execute();
		} catch (final Exception e) {
			final SQLException s = new SQLException(e.getMessage());
			s.initCause(e);
			throw s;
		}
	}
	
	/**
	 * @param year 対象年度
	 * @return 個人ID
	 * @throws SQLException
	 */
	String createIndividualId(
			final String year) throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					"SELECT TO_CHAR(s_idcount.NEXTVAL, "
					+ "'FM00000000') FROM DUAL");
			rs = ps.executeQuery();
			if (rs.next()) {
				return 	year.substring(2, 4) + rs.getString(1);
			} else {
				throw new SQLException("個人IDの取得に失敗しました。");
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	/**
	 * 履歴情報重複チェック（統合情報あり）
	 * 
	 * @param individualId 個人ID
	 * @param year 年度
	 * @param grade 学年
	 * @param className クラス
	 * @param classNo クラス番号
	 * @param subIndividualId 従生徒個人ID
	 * @return 自分以外の同じ年度・学年・クラス・クラス番号の生徒が存在するかどうか
	 * @throws SQLException
	 */
	boolean isDuplication(final String individualId, final String year,
			final int grade, final String className, final String classNo,
			final String subIndividualId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final Query query = QueryLoader.getInstance().load("m16");
			
			// 従生徒あり
			if (StringUtils.isNotEmpty(subIndividualId)) {
				query.append("AND bi.individualid <> ?");
			}
			
			ps = conn.prepareStatement(query.toString());
			ps.setString(1, year);
			ps.setInt(2, grade);
			ps.setString(3, className);
			ps.setString(4, classNo);
			ps.setString(5, schoolCd);

			// 個人ID
			// NULL値の演算はFALSEとなるためダミー文字列
			if (StringUtils.isEmpty(individualId)) {
				ps.setString(6, "NULL");
			} else {
				ps.setString(6, individualId);
			}
			
			// 従生徒個人ID
			if (StringUtils.isNotEmpty(subIndividualId)) {
				ps.setString(7, subIndividualId);
			}
			
			return (rs = ps.executeQuery()).next();
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * 履歴情報重複チェック（統合情報なし）
	 * 
	 * @param individualId 個人ID
	 * @param year 年度
	 * @param grade 学年
	 * @param className クラス
	 * @param classNo クラス番号
	 * @param subIndividualId 従生徒個人ID
	 * @return 自分以外の同じ年度・学年・クラス・クラス番号の生徒が存在するかどうか
	 * @throws SQLException
	 */
	boolean isDuplication(final String individualId, final String year,
			final int grade, final String className, final String classNo) throws SQLException {
		return isDuplication(individualId, year, grade, className, classNo, null);
	}
	
	// 集計学年・クラスの更新
	void updateCntGrade() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("m32"));
			ps.setString(1, student.getIndividualId());
			ps.setString(2, student.getIndividualId());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	// 新規登録ログを出力
	void insertLog(final String individualId, final String year,
			final int grade, final String className, final String classNo,
			final String nameKana) throws SQLException {
		log.insertLog(individualId, year, grade, className, classNo, nameKana);
	}
	
	// 更新ログを出力
	void updateLog(final String individualId, final String year,
			final int grade, final String className, final String classNo,
			final String nameKana) throws SQLException {
		log.updateLog(individualId, year, grade, className, classNo, nameKana);
	}
	
	/**
	 * @see com.fjh.beans.DefaultBean#setConnection(
	 * 			java.lang.String, java.sql.Connection)
	 */
	public void setConnection(final String pName,
			final Connection pConn) {
		super.setConnection(pName, pConn);
		name.setConnection(pName, pConn);
		log.setConnection(pName, pConn);
	}
	
	/**
	 * @param pStudent 設定する student。
	 */
	public void setStudent(final KNStudentData pStudent) {
		this.student = pStudent;
	}

	/**
	 * @return schoolCd を戻します。
	 */
	public String getSchoolCd() {
		return schoolCd;
	}

	/**
	 * @return student を戻します。
	 */
	public KNStudentData getStudent() {
		return student;
	}

}

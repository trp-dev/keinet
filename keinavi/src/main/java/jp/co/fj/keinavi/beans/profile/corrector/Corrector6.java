/*
 * 作成日: 2009/10/20
 */
package jp.co.fj.keinavi.beans.profile.corrector;

import java.util.Map;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.DefaultProfile;
import jp.co.fj.keinavi.data.profile.Profile;

/**
 * バージョン５→６
 * マーク模試高校別設問成績層正答率対応
 *
 * 大学コード10／5桁化対応
 *
 * @author Fujito URAKAWA - Totec
 * @version 1.0
 */
public class Corrector6 implements ICorrector {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.profile.corrector.ICorrector#execute(jp.co.fj.keinavi.data.profile.Profile)
	 */
	public void execute(final Profile profile, final LoginSession login) {

		Map item = null;

		// マーク模試高校別設問成績層正答率
		profile.getItemMap(S_SCHOOL_QUE).put(IND_MARK_EXAM_AREA, new Short("0"));

		// テキスト出力（集計データ−志望大学評価別人数）を初期化する
		profile.getItemMap(T_COUNTING_UNIV).put(TEXT_OUT_ITEM, DefaultProfile.getOutTargetValue(28));

		// テキスト出力（個人データ−模試別成績データ）を初期化する
		profile.getItemMap(T_IND_EXAM2).put(TEXT_OUT_ITEM, DefaultProfile.getOutTargetValue(247));

	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.profile.corrector.ICorrector#getVersion()
	 */
	public short getVersion() {
		return 6;
	}

}

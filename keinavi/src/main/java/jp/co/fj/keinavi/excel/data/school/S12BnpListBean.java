package jp.co.fj.keinavi.excel.data.school;

/**
 * �΍��l���z�i�Z�����сj�f�[�^���X�g
 * �쐬��: 2004/07/06
 * @author	T.Kuzuno
 */
public class S12BnpListBean {
	//�΍��l�я���l
	private float floBnpMax = 0;
	//�΍��l�щ����l
	private float floBnpMin = 0;
	//�l��
	private int intNinzu = 0;
	//�\����
	private float floKoseihi = 0;
	
	/*----------*/
	/* Get      */
	/*----------*/
	public float getFloBnpMax() {
		return this.floBnpMax;
	}
	public float getFloBnpMin() {
		return this.floBnpMin;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public float getFloKoseihi() {
		return this.floKoseihi;
	}

	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setFloBnpMax(float floBnpMax) {
		this.floBnpMax = floBnpMax;
	}
	public void setFloBnpMin(float floBnpMin) {
		this.floBnpMin = floBnpMin;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setFloKoseihi(float floKoseihi) {
		this.floKoseihi = floKoseihi;
	}
}

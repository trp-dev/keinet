/**
 * 校内成績分析−他校比較　偏差値分布
 * 出力する帳票の判断
 * 作成日: 2004/08/11
 * @author	Ito.Y
 */
 
package jp.co.fj.keinavi.excel.school;
import jp.co.fj.keinavi.util.log.*;

import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;

public class S42 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean s42( S42Item s42Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {
		KNLog log = KNLog.getInstance(null,null,null);
		
		try{
			int ret = 0;
			//S42Itemから各帳票を出力
			if (( s42Item.getIntHyouFlg()==0 )&&( s42Item.getIntBnpGraphFlg()==0 )&&
				( s42Item.getIntNinzuGraphFlg()==0 )&&( s42Item.getIntKoseiGraphFlg()==0 )) {
				throw new Exception("S42 ERROR : フラグ異常");
			}
			if (( s42Item.getIntHyouFlg()==1 )&&(s42Item.getIntKoseihiFlg()==2)&&(s42Item.getIntNendoFlg()==1)) {
				S42_01 exceledit = new S42_01();
				ret = exceledit.s42_01EditExcel( s42Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S42_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S42_01");
			}
			if (( s42Item.getIntHyouFlg()==1 )&&(s42Item.getIntKoseihiFlg()==2)&&(s42Item.getIntNendoFlg()==2)) {
				S42_02 exceledit = new S42_02();
				ret = exceledit.s42_02EditExcel( s42Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S42_02","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S42_02");
			}
			if (( s42Item.getIntHyouFlg()==1 )&&(s42Item.getIntKoseihiFlg()==2)&&(s42Item.getIntNendoFlg()==3)) {
				S42_03 exceledit = new S42_03();
				ret = exceledit.s42_03EditExcel( s42Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S42_03","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S42_03");
			}
			if (( s42Item.getIntHyouFlg()==1 )&&(s42Item.getIntKoseihiFlg()==1)&&(s42Item.getIntNendoFlg()==1)) {
				S42_04 exceledit = new S42_04();
				ret = exceledit.s42_04EditExcel( s42Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S42_04","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S42_04");
			}
			if (( s42Item.getIntHyouFlg()==1 )&&(s42Item.getIntKoseihiFlg()==1)&&(s42Item.getIntNendoFlg()==2)) {
				S42_05 exceledit = new S42_05();
				ret = exceledit.s42_05EditExcel( s42Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S42_05","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S42_05");
			}
			if (( s42Item.getIntHyouFlg()==1 )&&(s42Item.getIntKoseihiFlg()==1)&&(s42Item.getIntNendoFlg()==3)) {
				S42_06 exceledit = new S42_06();
				ret = exceledit.s42_06EditExcel( s42Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S42_06","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S42_06");
			}
			if (( s42Item.getIntBnpGraphFlg()==1 )&&(s42Item.getIntAxisFlg()==1)) {
				S42_07 exceledit = new S42_07();
				ret = exceledit.s42_07EditExcel( s42Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S42_07","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S42_07");
			}
			if (( s42Item.getIntBnpGraphFlg()==1 )&&(s42Item.getIntAxisFlg()==2)) {
				S42_08 exceledit = new S42_08();
				ret = exceledit.s42_08EditExcel( s42Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S42_08","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S42_08");
			}
			if (( s42Item.getIntNinzuGraphFlg()==1 )&&(s42Item.getIntNinzuPitchFlg()==1)) {
				S42_09 exceledit = new S42_09();
				ret = exceledit.s42_09EditExcel( s42Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S42_09","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S42_09");
			}
			if (( s42Item.getIntNinzuGraphFlg()==1 )&&(s42Item.getIntNinzuPitchFlg()==2)) {
				S42_10 exceledit = new S42_10();
				ret = exceledit.s42_10EditExcel( s42Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S42_10","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S42_10");
			}
			if (( s42Item.getIntNinzuGraphFlg()==1 )&&(s42Item.getIntNinzuPitchFlg()==3)) {
				S42_11 exceledit = new S42_11();
				ret = exceledit.s42_11EditExcel( s42Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S42_11","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S42_11");
			}
			if (( s42Item.getIntKoseiGraphFlg()==1 )&&(s42Item.getIntKoseiPitchFlg()==1)) {
				S42_12 exceledit = new S42_12();
				ret = exceledit.s42_12EditExcel( s42Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S42_12","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S42_12");
			}
			if (( s42Item.getIntKoseiGraphFlg()==1 )&&(s42Item.getIntKoseiPitchFlg()==2)) {
				S42_13 exceledit = new S42_13();
				ret = exceledit.s42_13EditExcel( s42Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S42_13","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S42_13");
			}
			if (( s42Item.getIntKoseiGraphFlg()==1 )&&(s42Item.getIntKoseiPitchFlg()==3)) {
				S42_14 exceledit = new S42_14();
				ret = exceledit.s42_14EditExcel( s42Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S42_14","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S42_14");
			}
			
		} catch(Exception e) {
			log.Err("99S42","帳票作成エラー",e.toString());
			return false;
		}
		return true;
	}

}
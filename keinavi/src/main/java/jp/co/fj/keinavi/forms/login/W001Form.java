/*
 * 作成日: 2004/06/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.login;

import com.fjh.forms.ActionForm;



/**
 * 2005.03.18	Yoshimoto KAWAI - Totec
 * 				利用者ＩＤを追加
 * 
 * @author kawai
 *
 */
public class W001Form extends ActionForm {

	private String account; // 学校ＩＤ
	private String password; // パスワード
	private String simple; // シンプルモードかどうか
	private String user; // 利用者ＩＤ

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * @return
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param string
	 */
	public void setAccount(String string) {
		account = string;
	}

	/**
	 * @param string
	 */
	public void setPassword(String string) {
		password = string;
	}

	/**
	 * @return
	 */
	public String getSimple() {
		return simple;
	}

	/**
	 * @param string
	 */
	public void setSimple(String string) {
		simple = string;
	}

	public String getUser() {
		return this.user;
	}
	public void setUser(String user) {
		this.user = user;
	}
}

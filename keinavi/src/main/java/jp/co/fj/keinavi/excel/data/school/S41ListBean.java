package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * �Z�����ѕ��́|���Z��r�|���ъT���f�[�^���X�g
 * �쐬��: 2004/07/16
 * @author	A.Iwata
 */
public class S41ListBean {
	//�^�E�Ȗږ��R�[�h
	private String strKmkCd = "";
	//�^�E�Ȗږ�
	private String strKmkmei = "";
	//�^��Ȗڗp�O���t�\���t���O
	private int intDispKmkFlg = 0;
	//�z�_
	private String strHaitenKmk = "";
	//�w�Z�f�[�^���X�g
	private ArrayList s41GakkoList = new ArrayList();
	
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}
	public int getIntDispKmkFlg() {
		return this.intDispKmkFlg;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public ArrayList getS41GakkoList() {
		return this.s41GakkoList;
	}


	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setIntDispKmkFlg(int intDispKmkFlg) {
		this.intDispKmkFlg = intDispKmkFlg;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setS41GakkoList(ArrayList s41GakkoList) {
		this.s41GakkoList = s41GakkoList;
	}
}

package jp.co.fj.keinavi.forms.sales;

import java.io.IOException;

import jp.co.fj.keinavi.forms.BaseForm;
import jp.co.fj.keinavi.util.DataValidator;
import jp.co.fj.keinavi.util.message.MessageLoader;

/**
 *
 * ファイルダウンロード画面フォーム
 * 
 * <2010年度マーク高２模試対応>
 * 2011.01.24	Tomohisa YAMADA
 * 				新規作成
 * 
 */
public class SD101Form extends BaseForm {
	
	//前画面メニュー
	private String menu;
	
	//0：通常遷移、1:ダウンロード、2:ソート
	private String actionMode = "0";
	
	//ソートインデックス
	private String sortIndex = "3";
	
	//営業部コード
	private String salesCode;
	
	//ダウンロード対象アップロードファイルID配列
	private String[] uploadFileID;
	
	//ダウンロード圧縮ファイル名
	private String zipFileName;
	
	//すべて選択
	private String[] checkall;
	
	/**
	 * 入力チェック
	 */
	public void validate() {
		//ファイルダウンロード時のみチェック
		if ("1".equals(actionMode)) {
			if (uploadFileID.length == 0) {
				setErrorMessage("ダウンロードファイルを選択してください。");
			}
			else if (zipFileName.length() == 0) {
				setErrorMessage("ダウンロードファイル名を入力してください。");
			}
			else if (DataValidator.getInstance().hasJapanese(zipFileName)) {
				try {
					setErrorMessage(MessageLoader.getInstance().getMessage("f003a"));
				} catch (IOException e) {
					e.printStackTrace();
					throw new InternalError(e.getMessage());
				}
			}
		}
	}

	public String getSalesCode() {
		return salesCode;
	}

	public void setSalesCode(String salesCode) {
		this.salesCode = salesCode;
	}

	public String[] getUploadFileID() {
		return uploadFileID;
	}

	public void setUploadFileID(String[] uploadFileID) {
		this.uploadFileID = uploadFileID;
	}

	public String getZipFileName() {
		return zipFileName;
	}

	public void setZipFileName(String zipFileName) {
		this.zipFileName = zipFileName;
	}

	public String getSortIndex() {
		return sortIndex;
	}

	public void setSortIndex(String sortIndex) {
		this.sortIndex = sortIndex;
	}

	public String getActionMode() {
		return actionMode;
	}

	public void setActionMode(String actionMode) {
		this.actionMode = actionMode;
	}

	public String[] getCheckall() {
		return checkall;
	}

	public void setCheckall(String[] checkall) {
		this.checkall = checkall;
	}

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

}

package jp.co.fj.keinavi.auth;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.util.KNCommonProperty;

/**
 *
 * K-WEBシステム連携用マネージャ
 * 
 * 2010.08.20	[新規作成]
 * 
 * 2013.02.12	Tomohisa Yamada - TOTEC
 * 				Kei-Navi答案閲覧システムリンク改修
 *
 * @author Tomohisa Yamada - TOTEC
 * @version 1.0
 * 
 */
public class KWebSysManager {
	
	//日付フォーマット
	private final SimpleDateFormat format = 
		new SimpleDateFormat("yyyyMMddHHmmss");
	
	//既存機能の再利用
	private final MoshiSysManager msm = new MoshiSysManager();
	
	//プロパティファイルからK-WebへのURLを取得
	private String url = KNCommonProperty.getKWebURL();
	
	//初期化ベクトル値
	//※暗号化しない
	private String sciv = null;
	//学校会員ID
	private String scid = null;
	//学校名
	private String scnm = null;
	//学校コード
	private String sccd = null;
	//ボタンを押した日時
	private String sstart = null;
	//ユーザエージェント
	private String usragt = null;
	
	/**
	 * コンストラクタ
	 * @throws Exception 
	 * @throws Exception 
	 */	
	public KWebSysManager(HttpServletRequest request,
			LoginSession loginSession, Profile profile) throws Exception {
		//作成
		msm.create(loginSession);
			
		//既存機能の再利用
		sciv = msm.getSciv();
		scid = msm.getScid();

		//追加項目
		scnm = msm.toValue(loginSession.getBundleName());
		sccd = msm.toValue(loginSession.getBundleCd());
		sstart = msm.toValue(format.format(new Date()));
		usragt = msm.toValue(request.getHeader("User-Agent"));
		
		if (loginSession == null || loginSession.isTrial()) {
			url = KNCommonProperty.getKWebExpURL();
		}
	}

	//変換値をマップ型に変換して返す
	public Map toMap() {
		final Map params = new HashMap();
		params.put("url", url);
		params.put("sciv", sciv);
		params.put("scid", scid);
		params.put("scnm", scnm);
		params.put("sccd", sccd);
		params.put("sstart", sstart);
		params.put("usragt", usragt);
		return params;
	}

	public String getSciv() {
		return sciv;
	}

	public String getScid() {
		return scid;
	}

	public String getScnm() {
		return scnm;
	}

	public String getSccd() {
		return sccd;
	}

	public String getSstart() {
		return sstart;
	}

	public String getUsragt() {
		return usragt;
	}

	/**
	 * リンク先URL
	 * @return
	 */
	public String getUrl() {
		return url;
	}
}

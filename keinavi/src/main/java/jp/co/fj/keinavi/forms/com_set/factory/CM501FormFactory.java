/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.com_set.factory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.com_set.ComClassData;
import jp.co.fj.keinavi.data.com_set.CompClassData;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM501Form;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

import com.fjh.forms.ActionForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM501FormFactory extends AbstractCMFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		CM501Form form = new CM501Form();

		// アイテムマップ
		Map item = super.getItemMap(request);
		// 対象模試データ
		ExamData exam = super.getExamData(request);

		// 選択方式
		form.setSelection(item.get(IProfileItem.CLASS_SELECTION).toString());

		// プロファイル設定値
		List container = (List) item.get(IProfileItem.CLASS_COMP_CLASS);

		int index = container
			.indexOf(new CompClassData(exam.getExamYear(), exam.getExamCD()));

		if (index >= 0) {
			CompClassData data = (CompClassData) container.get(index);
			form.setCompare1(ProfileUtil.getCompClassAllValue(data, false));
			form.setCompare2(ProfileUtil.getCompClassIndValue(data, false));
			form.setGraph1(ProfileUtil.getCompClassAllValue(data, true));
			form.setGraph2(ProfileUtil.getCompClassIndValue(data, true));
		}

		return form;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アイテムマップ
		Map item = super.getItemMap(request);
		// 共通アクションフォーム
		CM001Form f001 = super.getCommonForm(request);
		// 個別アクションフォーム
		CM501Form f501 = (CM501Form)f001.getActionForm("CM501Form");

		// アクションフォームがNULLなら画面が開かれていないので中止
		if (f501 == null) return;

		// 選択方式
		item.put(IProfileItem.CLASS_SELECTION, Short.valueOf(f501.getSelection()));

		// 比較対象クラスリスト
		CompClassData data = new CompClassData(f001.getTargetYear(), f001.getTargetExam());
		{
			List container = (List)item.get(IProfileItem.CLASS_COMP_CLASS);

			int index = container.indexOf(data);
			if (index >= 0) data = (CompClassData) container.get(index);
			else container.add(data);
		}

		// 設定値を初期化（全選択）
		if (data.getAClassData() == null) {
			data.setAClassData(new ArrayList());
		} else {
		    data.getAClassData().clear();
		}

		// 設定値を初期化（個別選択）
		if (data.getIClassData() == null) {
			data.setIClassData(new ArrayList());
		} else {
		    data.getIClassData().clear();
		}

		// 全選択
		if (f501.getCompare1() != null) {

			// グラフ表示セット
			Set graph = CollectionUtil.array2Set(f501.getGraph1());
			// 比較対象
			String[] compare = f501.getCompare1();

			for (int i=0; i<compare.length; i++) {
				ComClassData c = new ComClassData(compare[i]);

				if (graph.contains(compare[i])) c.setGraphDisp((short) 1);
				else c.setGraphDisp((short) 0);

				data.getAClassData().add(c);
			}
		}
		
		// 個別選択
		if (f501.getCompare2() != null) {

		    // グラフ表示セット
			Set graph = CollectionUtil.array2Set(f501.getGraph2());
			// 比較対象
			String[] compare = f501.getCompare2();

			for (int i=0; i<compare.length; i++) {
				ComClassData c = new ComClassData(compare[i]);

				if (graph.contains(compare[i])) c.setGraphDisp((short) 1);
				else c.setGraphDisp((short) 0);

				data.getIClassData().add(c);
			}
		}
	}
}

package jp.co.fj.keinavi.data.user;

import org.apache.commons.lang.StringUtils;

/**
 *
 * 利用者の担当クラスデータ
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class UserClassData {

	// 年度
	private String year;
	// 学年
	private int grade;
	// クラス
	private String className;
	
	/**
	 * コンストラクタ
	 */
	public UserClassData() {
	}

	/**
	 * コンストラクタ
	 */
	public UserClassData(final String key) {
		
		final String[] keys = StringUtils.split(key, ',');
		this.year = keys[0];
		this.grade = Integer.parseInt(keys[1]);
		this.className = keys[2];
	}

	/**
	 * 担当クラスを一意に識別するキーを取得する
	 * 
	 * @return
	 */
	public String getKey() {
		
		return year + "," + grade + "," + className;
	}
	
	/**
	 * @return className を戻します。
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @return grade を戻します。
	 */
	public int getGrade() {
		return grade;
	}

	/**
	 * @return year を戻します。
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param className 設定する className。
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @param grade 設定する grade。
	 */
	public void setGrade(int grade) {
		this.grade = grade;
	}

	/**
	 * @param year 設定する year。
	 */
	public void setYear(String year) {
		this.year = year;
	}

}

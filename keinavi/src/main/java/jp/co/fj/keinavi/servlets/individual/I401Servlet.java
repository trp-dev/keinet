package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.keinavi.beans.SubRecordASearchBean;
import jp.co.fj.keinavi.beans.individual.DockedSubRecBean;
import jp.co.fj.keinavi.beans.individual.ExamSearchBean;
import jp.co.fj.keinavi.beans.individual.JudgeByAutoBean;
import jp.co.fj.keinavi.beans.individual.JudgementStoreBean;
import jp.co.fj.keinavi.beans.individual.WishUnivSearchBean;
import jp.co.fj.keinavi.beans.individual.judgement.JudgementListBean;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.StudentData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I401Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.judgement.beans.ScoreKeiNavi;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.individual.IPropsLoader;

/**
 * 
 * 2004.06.21   Tomohisa YAMADA - Totec
 *              新規作成
 *
 * 2005.01.31 	Tomohisa YAMADA - Totec
 * 				高２系模試も判定できるようにする
 * 
 * 2005.04.08  	Tomohisa YAMADA - Totec
 * 				ScoreBeanの[35]に伴う変更
 * 
 * 2005.04.12	Keisuke KONDO - Totec
 * 				面談シート１を選択していて、かつボタン無効の場合の対応
 * 
 * 2005.04.20	Keisuke KONDO - Totec
 * 				対象模試がセンターリサーチの時のチェックボックス無効時の対応
 * 
 * 2005.04.21	Keisuke KONDO - Totec
 * 				対象模試がマーク記述以外の時の志望大学判定チェックボックス無効時の対応
 * 
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 *              マスタデータの持ち方変更
 *              
 * 2010.04.08	Tomohisa YAMADA - Totec
 * 				数学�@の偏差値を動的に算出する
 *               
 *              
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 *
 */
public class I401Servlet extends IndividualServlet {
	
	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		final HttpServletRequest request,
		final HttpServletResponse response)
		throws ServletException, IOException{
			
		super.execute(request, response);	
		
		// フォームデータの取得
		final I401Form form = (I401Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.individual.I401Form");
		
		// 大学データが未ロードならエラーページに遷移する
		if (getUnivAll() == null || getUnivAll().size() == 0) {
			request.setAttribute("form", form);
			forward(request, response, JSP_ONLOAD_UNIV);
			return;
		}

		// HTTPセッション
		final HttpSession session = request.getSession(false);
		//個人共通
		final ICommonMap iCommonMap = getICommonMap(request);
		//模試データ
		final ExamSession examSession = getExamSession(request);
		//プロファイル情報
		final Profile profile = getProfile(request);
		
		final Short printStudent = (Short) profile.getItemMap(
				IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);
		final Short InterviewSheet = (Short) profile.getItemMap(
				IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_SHEET);
		final String InterviewForms = (String) profile.getItemMap(
				IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_FORMS);
		final Short Report = (Short) profile.getItemMap(
				IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_REPORT);
		final Short ExamDoc = (Short) profile.getItemMap(
				IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_EXAM_DOC);
		final Short printStamp = (Short) profile.getItemMap(
				IProfileCategory.I_EASY_PRINT).get(IProfileItem.PRINT_STAMP);
		final Short targetGrade = (Short) profile.getItemMap(
				IProfileCategory.I_EASY_PRINT).get(IProfileItem.STUDENT_DISP_GRADE);

		// 転送元がJSPなら対象模試情報を初期化
		if ("i401".equals(getBackward(request))) {
			initTargetExam(request);
		}
		
		//JSP転送
		if ("i401".equals(getForward(request))) {
			
			if(!"i401".equals(getBackward(request))
				|| !iCommonMap.getTargetPersonId().equals(form.getTargetPersonId())
				|| isChangedMode(request) || isChangedExam(request)){

				// 対象生徒が変わった時
				if ("i401".equals(getBackward(request))) {
	                //2.面談用帳票をセット
					String[] interviewForm = {"0","0","0"};
					if(form.getInterviewForms() != null){
						for(int i=0; i < form.getInterviewForms().length; i++){
							interviewForm[Integer.parseInt(form.getInterviewForms()[i])] = "1";
						}				
					}
					form.setInterviewForms(interviewForm);
					
				//初回アクセス
				} else {
					if(iCommonMap.isBunsekiMode()){
						//個人成績分析共通-印刷対象生保存をセット
						form.setPrintStudent(printStudent.toString());
					}
					// フォームの値をプロファイルに保存
					//1.成績分析面談シートをセット
					form.setInterviewSheet(InterviewSheet.toString());
					//2.面談用帳票をセット
					form.setInterviewForms(CollectionUtil.splitComma(InterviewForms));
					//3.個人成績表をセット
					form.setReport(Report.toString());	
					//4.受験用資料をセット
					form.setExamDoc(ExamDoc.toString());	
					//5.セキュリティスタンプをセット
					form.setPrintStamp(printStamp.toString());
					//6.表示する学年
					form.setTargetGrade(targetGrade == null ? "2" : targetGrade.toString());
				}	
			} else {
				// フォームの値をプロファイルに保存
				//1.成績分析面談シートを保存
				profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
						IProfileItem.IND_INTERVIEW_SHEET,
						new Short(form.getInterviewSheet()));
	

				//2.面談用帳票を保存
				String[] InterviewForm = {"0","0","0"};
				if(form.getInterviewForms() != null){
					for(int i=0; i < form.getInterviewForms().length; i++){
						InterviewForm[Integer.parseInt(form.getInterviewForms()[i])] = "1";
					}
				}
				profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
						IProfileItem.IND_INTERVIEW_FORMS,
						CollectionUtil.deSplitComma(InterviewForm));
				form.setInterviewForms(InterviewForm);

				//3.個人成績表を保存
				if(form.getReport() != null){
					profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
							IProfileItem.IND_REPORT, new Short(form.getReport()));
				}else{
					profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
							IProfileItem.IND_REPORT, new Short("0"));
				}

				//4.受験用資料を保存
				if(form.getExamDoc() != null){
					profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
							IProfileItem.IND_EXAM_DOC, new Short(form.getExamDoc()));
				}else{
					profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
							IProfileItem.IND_EXAM_DOC, new Short("0"));
				}

				//5.セキュリティスタンプを保存
				profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
						IProfileItem.PRINT_STAMP, new Short(form.getPrintStamp()));

				//6.出力する学年を保存
				profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
						IProfileItem.STUDENT_DISP_GRADE, Short.valueOf(form.getTargetGrade()));
			}
			
			request.setAttribute(I401Form.SESSION_KEY, form);
			iCommonMap.setTargetPersonId(form.getTargetPersonId());
			forward(request, response, JSP_I401);

		//SERVLET転送
		} else {
			// 対象生徒を保持する
			iCommonMap.setTargetPersonId(form.getTargetPersonId());

			//帳票出力
			if ("sheet".equals(getForward(request))) {
				
				//分析モード時のみ印刷対象生保存を保存
				if (iCommonMap.isBunsekiMode()) {
					profile.getItemMap(IProfileCategory.I_COMMON).put(
							IProfileItem.PRINT_STUDENT, new Short(form.getPrintStudent()));
				}

				//フォームの値をプロファイルに保存
				//1.成績分析面談シートを保存
				if (form.getInterviewSheet() != null) {
					profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
							IProfileItem.IND_INTERVIEW_SHEET,
							new Short(form.getInterviewSheet()));
				}
				
				//2.面談用帳票を保存
				String[] InterviewForm = {"0","0","0"};
				if (form.getInterviewForms() != null) {
					for (int i = 0; i < form.getInterviewForms().length; i++) {
						InterviewForm[Integer.parseInt(form.getInterviewForms()[i])] = "1";
					}
				}
				
				final String CENTER_EXAMCD = "38";
				final String KYOSHEET_PRINT = "1";
				if (CollectionUtil.splitComma(InterviewForms)[2].equals(KYOSHEET_PRINT)
						&& iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
					InterviewForm[2] = KYOSHEET_PRINT;
				}
				profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
						IProfileItem.IND_INTERVIEW_FORMS,
						CollectionUtil.deSplitComma(InterviewForm));
				
				//3.個人成績表を保存
				if (form.getReport() != null) {
					profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
							IProfileItem.IND_REPORT, new Short(form.getReport()));
				} else {
					profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
							IProfileItem.IND_REPORT, new Short("0"));
				}

				//4.受験用資料を保存
				if (form.getExamDoc() != null) {
					profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
							IProfileItem.IND_EXAM_DOC, new Short(form.getExamDoc()));
				} else {
					if (ExamDoc.toString().equals("1")
							&& !(iCommonMap.getTargetExamTypeCode().equals("01")
									|| iCommonMap.getTargetExamTypeCode().equals("02")))  {
						// 対象模試が、マーク記述以外　かつ
						// 前回のプロファイル保存で、「印刷」が選択されている場合
						profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
								IProfileItem.IND_EXAM_DOC, new Short("1"));
					} else {
						profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
								IProfileItem.IND_EXAM_DOC, new Short("0"));
					}
				}
				
				//5.セキュリティスタンプを保存
				profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
						IProfileItem.PRINT_STAMP, new Short(form.getPrintStamp()));
				//6.出力する学年を保存
				profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(
						IProfileItem.STUDENT_DISP_GRADE, Short.valueOf(form.getTargetGrade()));
				
				// 志望大学判定一覧印刷にチェックがあればオマカセ判定を行う
				Short Judgeflag = ((Short) profile.getItemMap(
						IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_EXAM_DOC));
				
				// 簡単印刷オマカセ判定実行
				if (Judgeflag.shortValue() != 0
						&& (iCommonMap.getTargetExamTypeCode().equals("01")
								|| iCommonMap.getTargetExamTypeCode().equals("02"))) {
					
					// 判定処理スレッド起動
					new Thread(new JudgeThread(getDbKey(request), session,
							iCommonMap, form.getPrintStudent(),
							examSession, getUnivAll())).start();
				}
			}
			
			dispatch(request, response);
		}
	}
	
	// 判定処理スレッド
	private class JudgeThread extends BaseJudgeThread {
		
		private final ICommonMap iCommonMap;
		private final String judgeStudent;
		private final ExamSession examSession;
		private final List univAll;
		
		// コンストラクタ
		private JudgeThread(final String dbKey, final HttpSession session,
				final ICommonMap pICommonMap, final String pJudgeStudent,
				final ExamSession pExamSession, final List pUnivAll){
			super(session, dbKey);
			iCommonMap = pICommonMap;
			judgeStudent = pJudgeStudent;
			examSession = pExamSession;
			univAll = pUnivAll;
		}
		
		/**
		 * @see jp.co.fj.keinavi.servlets.individual.BaseJudgeThread#execute(
		 * 			java.sql.Connection)
		 */
		protected Map execute(final Connection con) throws Exception {

			// 判定結果マップ
			final Map judgedMap = new HashMap();
			
			try {
				/** ステップ１：成績の取得 */
				List scoreBeanList = new ArrayList();
				//選択中の生徒全て
				List selectedIndividuals = getSelectedIndividuals(iCommonMap, judgeStudent);

				//対象模試情報の取得
				ExamSearchBean esb = new ExamSearchBean();
				esb.setExamYear(iCommonMap.getTargetExamYear());
				esb.setExamCd(iCommonMap.getTargetExamCode());
				esb.setConnection("", con);
				esb.execute();
				//絶対に取れるはずだから、最初のを取得
				ExaminationData targetExam = (ExaminationData) esb.getRecordSet().get(0);
						
				//成績取得
				if(selectedIndividuals != null && selectedIndividuals.size() > 0){
					Iterator ite = selectedIndividuals.iterator();
					while(ite.hasNext()){
						StudentData sd = (StudentData) ite.next();
							
						//ドッキング先模試情報の取得
						DockedSubRecBean dsrb = new DockedSubRecBean();
						dsrb.setIndividualId(sd.getStudentPersonId());
						dsrb.setExamYear(iCommonMap.getTargetExamYear());
						dsrb.setExamCd(iCommonMap.getTargetExamCode());
						dsrb.setExamSession(examSession);
						dsrb.setConnection("", con);
						dsrb.execute();
							
						//ScoreBeanに取得してきた成績をセット
						Score sb = new ScoreKeiNavi(
								SubRecordASearchBean.searchNullSet(
										dsrb.getDockedBean().getExaminationDatas()[0], 
										con));
						/**
						 * 11/5 性別・対象模試情報追加
						 */
						if(sd.getStudentSex().equals(IPropsLoader.getInstance().getMessage("CODE_GENDER_MALE"))){
							sb.setMale(true);	
						}else if(sd.getStudentSex().equals(IPropsLoader.getInstance().getMessage("CODE_GENDER_FEMALE"))){
							sb.setMale(false);	
						}
						//選択対象模試情報を付加する（A,B,C判定の為）
						sb.setTargetExam(targetExam);
								
						sb.setIndividualId(sd.getStudentPersonId());
						sb.setMark(dsrb.getMark());
						sb.setWrtn(dsrb.getWrtn());
						sb.setCExam(dsrb.getDockedBean().getExaminationDatas()[0]);//[0]はマーク模試
						sb.setSExam(dsrb.getDockedBean().getExaminationDatas()[1]);//[1]は記述模試
						sb.execute();
							
						//スコアリストに追加
						scoreBeanList.add(sb);
					}
				}
				//判定
				List judgedList = new ArrayList();
				Iterator ite = scoreBeanList.iterator();
				while(ite.hasNext()){
					Score sb = (Score) ite.next();
							
					JudgementListBean listBean = new JudgementListBean();
					//マーク模試も記述模試も受けていないので判定を行わない
					if(sb.getMark() == null && sb.getWrtn() == null){
						listBean.setJudged(true);
						listBean.setExamTaken(false);
					}
					//マーク模試または記述模試どちらか一方でも受けているので判定する
					else{								
						/** この生徒の志望の大学の分野と地区を取得 */
						WishUnivSearchBean wusb = new WishUnivSearchBean();
						wusb.setIndividualId(sb.getIndividualId());
						wusb.setExamYear(iCommonMap.getTargetExamYear());
						wusb.setExamCd(iCommonMap.getTargetExamCode());
						wusb.setConnection("", con);
						wusb.execute();
						
						/** おまかせ判定実行 */
						JudgeByAutoBean jbab = new JudgeByAutoBean();
						jbab.setUnivAllList(univAll);
						jbab.setWishUnivList(wusb.getRecordSet());
						jbab.setScoreBean(sb);
						jbab.setPrecedeCenterPre();
						jbab.execute();
								
						judgedList = jbab.getRecordSet();
						listBean.createListBean(judgedList, sb);
								
						//判定結果があるときのみ（0件以上）保存する
						if(judgedList != null && judgedList.size() > 0){
							// 結果の保存
							JudgementStoreBean jsb = new JudgementStoreBean();
							jsb.setIndividualId(sb.getIndividualId());
							jsb.setExamYear(iCommonMap.getTargetExamYear());
							jsb.setExamCd(iCommonMap.getTargetExamCode());
							jsb.setResultList(listBean.getRecordSet());
							jsb.setConnection("", con);
							jsb.execute();
						}
					}
					judgedMap.put(sb.getIndividualId(), listBean);
				}

				con.commit();

			} catch (final Exception e) {
				DbUtils.rollback(con);
				throw e;
			}
			
			return judgedMap;
		}
	}	
}

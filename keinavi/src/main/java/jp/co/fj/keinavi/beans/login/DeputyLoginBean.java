package jp.co.fj.keinavi.beans.login;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.interfaces.ILogin;
import jp.co.fj.keinavi.servlets.KNServletException;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 代行ログインBean
 * 
 * 2004/07/02	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class DeputyLoginBean extends DefaultBean {
	
	private LoginSession loginSession; // ログインセッション

	private String schoolCD; // 学校コード
	private String sectorCd; // 部門コード
	private String sectorSortingCD; // 部門分類コード
	private LoginSession container = new LoginSession(); // 値を内部で保持するためのインスタンス

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// マスタ取得
		getPactSchool();
		getSchool();
		
		// 情報コピー
		loginSession.setPactDiv(container.getPactDiv());
		loginSession.setUserDiv(container.getUserDiv());
		loginSession.setLoginID(container.getLoginID());
		loginSession.setUserID(container.getUserID());
		loginSession.setSectorCd(container.getSectorCd());
		loginSession.setSectorSortingCD(container.getSectorSortingCD());
		loginSession.setUserMode(container.getUserMode());
		loginSession.setUserName(container.getUserName());
		loginSession.setPrefCD(container.getPrefCD());
		loginSession.setAccountName(container.getAccountName());
		loginSession.setPrivacyProtection();
	}

	// 契約校マスタ取得
	private void getPactSchool() throws Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					"SELECT	pactdiv, cert_userdiv, userid "
					+ "FROM	pactschool WHERE schoolcd = ? "
					+ "ORDER BY renewaldate DESC");
			ps.setString(1, schoolCD);

			rs = ps.executeQuery();
			if (rs.next()) {
				container.setPactDiv(rs.getShort(1));
				container.setUserDiv(rs.getShort(2));
				container.setLoginID(rs.getString(3));
			} else {
				final KNServletException e =
					new KNServletException("契約校マスタの取得に失敗しました。");
				e.setErrorCode("1");
				e.setErrorMessage("正しい学校コードを入力してください。");
				throw e;
			} 
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	// 学校マスタ取得
	private void getSchool() throws Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					"SELECT bundlename, facultycd FROM school WHERE bundlecd = ?");
			ps.setString(1, schoolCD);

			rs = ps.executeQuery();
			if (rs.next()) {
				container.setUserID(schoolCD);
				container.setSectorCd(sectorCd);
				container.setSectorSortingCD(sectorSortingCD);
				container.setUserMode(ILogin.SALES_SCHOOL);
				container.setUserName(rs.getString(1));
				container.setPrefCD(rs.getString(2));
				container.setPrivacyProtection();
				container.setAccountName("河合塾");
			} else {
				final KNServletException e =
					new KNServletException("学校マスタの取得に失敗しました。");
				e.setErrorCode("1");
				e.setErrorMessage("正しい学校コードを入力してください。");
				throw e;
			} 
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	/**
	 * @param pSectorCd 設定する sectorCd。
	 */
	public void setSectorCd(String pSectorCd) {
		sectorCd = pSectorCd;
	}

	/**
	 * @param string
	 */
	public void setSchoolCD(String string) {
		schoolCD = string;
	}

	/**
	 * @param string
	 */
	public void setSectorSortingCD(String string) {
		sectorSortingCD = string;
	}

	/**
	 * @param session
	 */
	public void setLoginSession(LoginSession session) {
		loginSession = session;
	}

}

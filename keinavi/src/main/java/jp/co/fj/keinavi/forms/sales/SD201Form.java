package jp.co.fj.keinavi.forms.sales;

import jp.co.fj.keinavi.forms.MethodInvokerForm;
import jp.co.fj.keinavi.util.DataValidator;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * 特例成績データ作成申請画面のアクションフォームです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD201Form extends MethodInvokerForm {

	/** serialVersionUID */
	private static final long serialVersionUID = 1993571396215733704L;

	/** 模試年度 */
	private String examYear;

	/** 模試コード */
	private String examCd;

	/** 学校コード */
	private String schoolCd;

	/** 担当先生名 */
	private String teacherName;

	/** 申請理由 */
	private String comments;

	/**
	 * 申請時の入力チェックを行います。
	 */
	public void validateApply() {

		if (StringUtils.isEmpty(teacherName)) {
			setErrorMessage("common", "担当先生名を入力してください。");
		} else if (!DataValidator.getInstance().isValidByteLength(teacherName,
				0, 20)) {
			setErrorMessage("common", "担当先生名は全角10文字までです。");
		}

		if (!DataValidator.getInstance().isValidByteLength(comments, 0, 80)) {
			setErrorMessage("common", "申請理由は全角40文字までです。");
		}
	}

	/**
	 * 模試年度を返します。
	 * 
	 * @return 模試年度
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * 模試年度をセットします。
	 * 
	 * @param examYear
	 *            模試年度
	 */
	public void setExamYear(String examYear) {
		this.examYear = examYear;
	}

	/**
	 * 模試コードを返します。
	 * 
	 * @return 模試コード
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * 模試コードをセットします。
	 * 
	 * @param examCd
	 *            模試コード
	 */
	public void setExamCd(String examCd) {
		this.examCd = examCd;
	}

	/**
	 * 学校コードを返します。
	 * 
	 * @return 学校コード
	 */
	public String getSchoolCd() {
		return schoolCd;
	}

	/**
	 * 学校コードをセットします。
	 * 
	 * @param schoolCd
	 *            学校コード
	 */
	public void setSchoolCd(String schoolCd) {
		this.schoolCd = schoolCd;
	}

	/**
	 * 担当先生名を返します。
	 * 
	 * @return 担当先生名
	 */
	public String getTeacherName() {
		return teacherName;
	}

	/**
	 * 担当先生名をセットします。
	 * 
	 * @param teacherName
	 *            担当先生名
	 */
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	/**
	 * 申請理由を返します。
	 * 
	 * @return 申請理由
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * 申請理由をセットします。
	 * 
	 * @param comments
	 *            申請理由
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

}

/**
 * クラス成績分析−クラス成績概況−個人成績（志望大学評価付）
 * 	Excelファイル編集
 * 作成日: 2004/09/02
 * @author	Ito.Y
 *
 * 2009.11.24   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 */

package jp.co.fj.keinavi.excel.cls;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C12Item;
import jp.co.fj.keinavi.excel.data.cls.C12KmkSeisekiListBean;
import jp.co.fj.keinavi.excel.data.cls.C12ListBean;
import jp.co.fj.keinavi.excel.data.cls.C12SeisekiListBean;
import jp.co.fj.keinavi.excel.data.cls.C12ShiboHyoukaListBean;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class C12_02 {

    private int noerror	= 0;		// 正常終了
    private int errfread	= 1;		// ファイルreadエラー
    private int errfwrite	= 2;		// ファイルwriteエラー
    private int errfdata	= 3;		// データ設定エラー

    private CM cm = new CM();		//共通関数用クラス インスタンス

    final private String	masterfile0	= "C12_02";	// ファイル名
    final private String	masterfile1	= "C12_02";	// ファイル名
    private String	masterfile	= "";				// ファイル名
    final private int[]	tabRow	= {4,16,28,40,52};	// 表の基準点(今年度)

    /*
     * 	Excel編集メイン
     * 		C12Item c12Item: データクラス
     * 		String outfile: 出力Excelファイル名（フルパス）
     * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
     * 		String	UserID：ユーザーID
     * 		戻り値: 0:正常終了、0以外:異常終了
     */
     public int c12_02EditExcel(C12Item c12Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
        KNLog log = KNLog.getInstance(null,null,null);
        log.Ep("C12_02","C12_02帳票作成開始","");

        //テンプレートの決定
        if (c12Item.getIntShubetsuFlg() == 1){
            masterfile = masterfile1;
        } else{
            masterfile = masterfile0;
        }

        try {
            HSSFWorkbook	workbook	= null;
            HSSFSheet		workSheet	= null;
            HSSFRow			workRow		= null;
            HSSFCell		workCell	= null;

            int		row				= 0;	// 行
            int		col				= 0;	// 列
            int		maxSheetIndex	= 0;	// シートカウンター
            int		maxSheetNum		= 50;	// 最大シート数
            int		fileIndex		= 1;	// ファイルカウンター
            int		kyoukaCnt		= 0;	// 教科カウンター
            boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
            boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
            float		hensaAll		= 0;	// *セット用
            boolean	firstKataFlg	= false;// 2005.01.15 最初に取得した型用

            // 基本ファイルを読込む
            C12ListBean c12ListBean = new C12ListBean();

            // データセット
            ArrayList	c12List			= c12Item.getC12List();
            Iterator	itr				= c12List.iterator();

            /** 学年・クラス情報 **/
            // c12Listのセット
            while( itr.hasNext() ) {
                c12ListBean = (C12ListBean)itr.next();
                row=0;
                col=0;
                bolSheetCngFlg = true;
                if(maxSheetIndex>=maxSheetNum){
                    bolBookCngFlg = true;
                }

                // 基本ファイルを読み込む
                C12SeisekiListBean	c12SeisekiListBean	= new C12SeisekiListBean();

                //データの保持
                ArrayList	c12SeisekiList	= c12ListBean.getC12SeisekiList();
                Iterator	itrC12Seiseki	= c12SeisekiList.iterator();

                /** 個人Data **/
                while(itrC12Seiseki.hasNext()){
                    c12SeisekiListBean = (C12SeisekiListBean) itrC12Seiseki.next();

                    //マスタExcel読み込み
                    if(bolBookCngFlg){
                        if(maxSheetIndex>=maxSheetNum){

                            // Excelファイル保存
                            boolean bolRet = false;
                            bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, maxSheetIndex);
                            fileIndex++;

                            if( bolRet == false ){
                                return errfwrite;
                            }
                        }

                        if((maxSheetIndex>=maxSheetNum)||(maxSheetIndex==0)){
                            workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
                            if( workbook==null ){
                                return errfread;
                            }
                            bolBookCngFlg = false;
                            maxSheetIndex=0;
                        }
                    }

                    if ( bolSheetCngFlg ) {

                        // シートテンプレートのコピー
                        workSheet = workbook.cloneSheet(0);
                        maxSheetIndex++;

                        // ヘッダ右側に帳票作成日時を表示する
                        cm.setHeader(workbook, workSheet);

                        // セキュリティスタンプセット
                        //2005.01.12 Update 表示列を＋２する
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                        // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD START
                        //2019/09/30 QQ)Oosaki 共通テスト対応 UPD START
                        //// 2019/09/24 QQ)Tanouchi 共通テスト対応 UPD START
                        ////String secuFlg = cm.setSecurity( workbook, workSheet, c12Item.getIntSecuFlg() ,53 ,55 );
                        ////workCell = cm.setCell( workSheet, workRow, workCell, 0, 53 );
                        //String secuFlg = cm.setSecurity( workbook, workSheet, c12Item.getIntSecuFlg() ,56 ,58 );
                        //workCell = cm.setCell( workSheet, workRow, workCell, 0, 56 );
                        //String secuFlg = cm.setSecurity( workbook, workSheet, c12Item.getIntSecuFlg() ,54 ,56 );
                        //workCell = cm.setCell( workSheet, workRow, workCell, 0, 54 );
                        //String secuFlg = cm.setSecurity( workbook, workSheet, c12Item.getIntSecuFlg() ,52 ,54 );
                        //workCell = cm.setCell( workSheet, workRow, workCell, 0, 52 );
                        String secuFlg = cm.setSecurity( workbook, workSheet, c12Item.getIntSecuFlg() ,49 ,51 );
                        workCell = cm.setCell( workSheet, workRow, workCell, 0, 49 );
                        // 2019/09/24 QQ)Tanouchi 共通テスト対応 UPD END
                        //2019/09/30 QQ)Oosaki 共通テスト対応 UPD END
                        // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD END
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                        workCell.setCellValue(secuFlg);

                        // 学校名セット
                        String strGakkoMei="学校名　：" + cm.toString(c12Item.getStrGakkomei())
                                            + "　学年：" + cm.toString(c12ListBean.getStrGrade()) + "　クラス名：" + cm.toString(c12ListBean.getStrClass());
                        workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
                        workCell.setCellValue( strGakkoMei );

                        // 対象模試セット
                        String moshi =cm.setTaisyouMoshi( c12Item.getStrMshDate() );	// 模試月取得
                        workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
                        workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(c12Item.getStrMshmei()) + moshi);

                        //　改シートフラグをFalseにする
                        bolSheetCngFlg	=false;
                        row=0;
                    }

                    // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
                    //2019/09/26 QQ)Oosaki 共通テスト対応 ADD START
                    //セルスタイル作成
                    //HSSFCellStyle CellStyle = this.createStyle(workbook);
                    //2019/09/26 QQ)Oosaki 共通テスト対応 ADD END
                    // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START

                    // 基本ファイルを読み込む
                    C12KmkSeisekiListBean	c12KmkSeisekiListBean	= new C12KmkSeisekiListBean();

                    //データの保持
                    ArrayList	c12KmkSeisekiList	= c12SeisekiListBean.getC12KmkSeisekiList();
                    Iterator	itrC12KmkSeiseki	= c12KmkSeisekiList.iterator();

                    /** 個人詳細Data **/
                    String strKykHoji = "";
                    String kyouka = "";	//型の7000番台と8000番台の判断用
                    int tabCol = 0;
                    col = 0;
                    kyoukaCnt = 0;
                    hensaAll = 0;
                    firstKataFlg = false; //*判断の型は最初に取得した型

                    //第一解答科目の成績
                    C12KmkSeisekiListBean rikaSeiseki = null;
                    C12KmkSeisekiListBean chikouSeiseki = null;

                    while(itrC12KmkSeiseki.hasNext()){
                        c12KmkSeisekiListBean = (C12KmkSeisekiListBean) itrC12KmkSeiseki.next();

                        if(col==0){
                            // 学年セット
                            workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row], 0 );
                            workCell.setCellValue( "学年：" + cm.toString(c12SeisekiListBean.getStrGrade()) );

                            // クラスセット
                            workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row], 2 );
                            workCell.setCellValue( "クラス：" + cm.toString(c12SeisekiListBean.getStrClass()) );

                            // クラス番号セット
                            workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row], 6 );
                            workCell.setCellValue( "クラス番号：" + cm.toString(c12SeisekiListBean.getStrClassNo()) );

                            // 氏名セット
                            workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row], 12 );
                            workCell.setCellValue( "氏名：" + cm.toString(c12SeisekiListBean.getStrKanaName()) );

                            // 性別セット
                            int sexKbn = Integer.parseInt( c12SeisekiListBean.getStrSex() );
                            String strSex = "";
                            if( sexKbn == 1 ){
                                strSex = "性別：男";
                            }else if( sexKbn == 2 ){
                                strSex = "性別：女";
                            }else{		//}else if( sexKbn == 9 ){
                                strSex = "性別：不明";
                            }
                            workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row], 24 );
                            workCell.setCellValue( strSex );
                        }
                        // 教科コードによって判定
                        String strKyouka = c12KmkSeisekiListBean.getStrKyokaCd()+" ";
                        strKyouka = strKyouka.substring(0,1);
                        if( !cm.toString(strKyouka).equals(cm.toString(strKykHoji)) ){
                            switch( Integer.parseInt( strKyouka ) ){
                                case 1:	//英語
                                    tabCol = 6;
                                    kyoukaCnt = 3;
                                    break;
                                case 2:	//数学
                                    tabCol = 15;
                                    kyoukaCnt = 3;
                                    break;
                                case 3:	//国語
                                    tabCol = 24;
                                    // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                    // 2019/09/24 QQ)Tanouchi 共通テスト対応 UPD START
                                    kyoukaCnt = 1;
                                    //kyoukaCnt = 2;
                                    // 2019/09/24 QQ)Tanouchi 共通テスト対応 UPD START
                                    // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                    break;
                                case 4:	//理科
                                    // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                    //2019/09/26 QQ)Oosaki 共通テスト対応 UPD START
                                    tabCol = 27;
                                    //tabCol = 30;
                                    //2019/09/26 QQ)Oosaki 共通テスト対応 UPD END
                                    // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                    kyoukaCnt = 3;
                                    break;
                                case 5:	//地歴公民
                                    // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                    //2019/09/26 QQ)Oosaki 共通テスト対応 UPD START
                                    tabCol = 36;
                                    //tabCol = 39;
                                    //2019/09/26 QQ)Oosaki 共通テスト対応 UPD END
                                    // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                    kyoukaCnt = 2;
                                    break;
                                case 7:	//総合
                                    tabCol = 2;
                                    kyoukaCnt = 1;
                                    break;
                                case 8:
                                    if( strKykHoji.equals("7") == false ){
                                        tabCol = 2;
                                        kyoukaCnt = 1;
                                    }
                                    else{
                                        tabCol = 4;
                                        kyoukaCnt = 1;
                                    }
                                    break;
                                default:
                                    kyoukaCnt = 0;
                            }
                            col = 0;
                            strKykHoji = strKyouka;
                        }
                        if(cm.toString(strKyouka).equals("7") || cm.toString(strKyouka).equals("8")){		 // 型の場合
                            if (kyoukaCnt>0 && !cm.toString(strKyouka).equals(kyouka)) {
                                // 科目名セット
                                workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+1, tabCol+2*col );
                                workCell.setCellValue( c12KmkSeisekiListBean.getStrKmkmei());

                                // 科目配点セット
                                if(  c12KmkSeisekiListBean.getIntHaiten() != -999 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3, tabCol+2*col );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getIntHaiten());
                                }

                                // 得点セット
                                if(  c12KmkSeisekiListBean.getIntTokuten() != -999 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+4, tabCol+2*col );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getIntTokuten());
                                }

                                // 校内偏差セット
                                if(  c12KmkSeisekiListBean.getFloHensaHome() != -999.0 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+5, tabCol+2*col );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getFloHensaHome());
                                }

                                // 校内順位セット
                                if(  c12KmkSeisekiListBean.getIntJyuniHome() != -999 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+6, tabCol+2*col );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getIntJyuniHome());
                                }

                                // 校内人数セット
                                if(  c12KmkSeisekiListBean.getIntNinzuHome() != -999 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+7, tabCol+2*col );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getIntNinzuHome());
                                }

                                // 全国偏差セット
                                if(  c12KmkSeisekiListBean.getFloHensaAll() != -999.0 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+8, tabCol+2*col );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getFloHensaAll());
                                }

                                // 学力レベルセット
                                if (  !"".equals(c12KmkSeisekiListBean.getStrScholarLevel())) {
                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+8, tabCol+2*col+1 );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getStrScholarLevel());
                                }

                                // 全国順位セット
                                if(  c12KmkSeisekiListBean.getIntJyuniAll() != -999 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+9, tabCol+2*col );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getIntJyuniAll());
                                }

                                // 全国人数セット
                                if(  c12KmkSeisekiListBean.getIntNinzuAll() != -999 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+10, tabCol+2*col );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getIntNinzuAll());
                                }

                                // 換算得点セット
                                if(  c12KmkSeisekiListBean.getIntKansanTokuten() != -999 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+11, tabCol+2*col );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getIntKansanTokuten());
                                }

                                // *取得 2005.01.15 *判断の型は最初に取得した型
                                if(firstKataFlg == false){
                                    if(  c12KmkSeisekiListBean.getFloHensaAll() != -999.0 ){
                                        hensaAll = c12KmkSeisekiListBean.getFloHensaAll();
                                        firstKataFlg = true;
                                    }
                                }

                                col++;
                                kyoukaCnt--;
                            }
                            kyouka = strKyouka;
                        }else{		// 科目の場合
                            if (kyoukaCnt>0) {
                                //理科の先頭科目が基礎科目なら、先頭は空欄にする
                                if ("4".equals(strKyouka) && col == 0 && "1".equals(c12KmkSeisekiListBean.getBasicFlg())) {
                                    col++;
                                }

                                // 科目名セット
                                if (cm.toString(strKyouka).equals("4") || cm.toString(strKyouka).equals("5")){
                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+2, tabCol+3*col );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getStrKmkmei());
                                }else{
                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+1, tabCol+3*col );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getStrKmkmei());
                                }

                                // 科目配点セット
                                if(  c12KmkSeisekiListBean.getIntHaiten() != -999 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3, tabCol+3*col+1 );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getIntHaiten());
                                }

                                // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
                                //2019/09/26 QQ)Oosaki 共通テスト対応 ADD START
//                                if( ("3915").equals(c12KmkSeisekiListBean.getStrKmkCd()) ) {
//                                	workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+4, tabCol+3*col+1 );
//                                	workCell.setCellStyle(CellStyle);
//                                	workCell.setCellValue( c12KmkSeisekiListBean.getStrKokugoTotalHyouka());
//                                } else {
                                //2019/09/26 QQ)Oosaki 共通テスト対応 ADD END
                                // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END

	                                // 得点セット
	                                if(  c12KmkSeisekiListBean.getIntTokuten() != -999 ){
	                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+4, tabCol+3*col+1 );
	                                    workCell.setCellValue( c12KmkSeisekiListBean.getIntTokuten());
	                                }

	                                // 校内偏差セット
	                                if(  c12KmkSeisekiListBean.getFloHensaHome() != -999.0 ){
	                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+5, tabCol+3*col+1 );
	                                    workCell.setCellValue( c12KmkSeisekiListBean.getFloHensaHome());
	                                }

	                                // 校内順位セット
	                                if(  c12KmkSeisekiListBean.getIntJyuniHome() != -999 ){
	                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+6, tabCol+3*col+1 );
	                                    workCell.setCellValue( c12KmkSeisekiListBean.getIntJyuniHome());
	                                }

	                                // 校内人数セット
	                                if(  c12KmkSeisekiListBean.getIntNinzuHome() != -999 ){
	                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+7, tabCol+3*col+1 );
	                                    workCell.setCellValue( c12KmkSeisekiListBean.getIntNinzuHome());
	                                }

	                                // 全国偏差セット
	                                if(  c12KmkSeisekiListBean.getFloHensaAll() != -999.0 ){
	                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+8, tabCol+3*col+1 );
	                                    workCell.setCellValue( c12KmkSeisekiListBean.getFloHensaAll());
	                                }

	                                // 学力レベルセット
	                                if (  !"".equals(c12KmkSeisekiListBean.getStrScholarLevel())) {
	                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+8, tabCol+3*col+2 );
	                                    workCell.setCellValue( c12KmkSeisekiListBean.getStrScholarLevel());
	                                }

	                                // 全国順位セット
	                                if(  c12KmkSeisekiListBean.getIntJyuniAll() != -999 ){
	                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+9, tabCol+3*col+1 );
	                                    workCell.setCellValue( c12KmkSeisekiListBean.getIntJyuniAll());
	                                }

	                                // 全国人数セット
	                                if(  c12KmkSeisekiListBean.getIntNinzuAll() != -999 ){
	                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+10, tabCol+3*col+1 );
	                                    workCell.setCellValue( c12KmkSeisekiListBean.getIntNinzuAll());
	                                }

	                                // 換算得点セット
	                                if(  c12KmkSeisekiListBean.getIntKansanTokuten() != -999 ){
	                                    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+11, tabCol+3*col+1 );
	                                    workCell.setCellValue( c12KmkSeisekiListBean.getIntKansanTokuten());
	                                }

	                                // #セット
	                                if(  c12KmkSeisekiListBean.getFloHensaAll() != -999.0 ){
	                                    if ( hensaAll >= c12KmkSeisekiListBean.getFloHensaAll()+2.5f) {
	                                        workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+8, tabCol+3*col );
	                                        workCell.setCellValue( "#");
	                                    }
	                                }

	                                // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 DEL START
	                                //2019/09/26 QQ)Oosaki 共通テスト対応 ADD START
	                                //CEFR出力
//	                                if( ("01").equals(c12KmkSeisekiListBean.getStrMosiType()) ) {
//	                                	workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+1, 12 );
//                                        workCell.setCellValue( "CEFR" );
//                                        workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+4, 13 );
//                                        workCell.setCellStyle(CellStyle);
//                                        workCell.setCellValue( c12KmkSeisekiListBean.getStrCefrScore() );
//	                                }
	                                //2019/09/26 QQ)Oosaki 共通テスト対応 ADD END
	                                // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 DEL END

	                                // *セット
	                                // マーク模試の場合のみ、換算得点が第１解答科目＜第２解答科目の場合に*表示
	                                if (KNUtil.isAns1st(c12Item.getStrMshCd())){
	                                    if (cm.toString(strKyouka).equals("4") && c12KmkSeisekiListBean.getIntScope() == 0 && !"1".equals(c12KmkSeisekiListBean.getBasicFlg())){
	                                        if (rikaSeiseki != null){
	                                            if (rikaSeiseki.getIntKansanTokuten() < c12KmkSeisekiListBean.getIntKansanTokuten()){
	                                                workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+11, tabCol+3*col );
	                                                workCell.setCellValue( "*");
	                                            }
	                                        }
	                                    }else if (cm.toString(strKyouka).equals("5") && c12KmkSeisekiListBean.getIntScope() == 0){
	                                        if (chikouSeiseki != null){
	                                            if (chikouSeiseki.getIntKansanTokuten() < c12KmkSeisekiListBean.getIntKansanTokuten()){
	                                                workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+11, tabCol+3*col );
	                                                workCell.setCellValue( "*");
	                                            }
	                                        }
	                                    }
	                                }
	                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
//                                }
                                // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END

                                // 第１解答科目保存（マーク模試のみ）
                                if (KNUtil.isAns1st(c12Item.getStrMshCd())){
                                    if (cm.toString(strKyouka).equals("4") && c12KmkSeisekiListBean.getIntScope() == 1){
                                        rikaSeiseki = c12KmkSeisekiListBean;
                                    }else if (cm.toString(strKyouka).equals("5") && c12KmkSeisekiListBean.getIntScope() == 1){
                                        chikouSeiseki = c12KmkSeisekiListBean;
                                    }
                                }

                                col++;
                            }
                            kyoukaCnt--;
                        }
                    }

                    // 基本ファイルを読み込む
                    C12ShiboHyoukaListBean	c12ShiboHyoukaListBean	= new C12ShiboHyoukaListBean();

                    //データの保持
                    ArrayList	c12ShiboHyoukaList	= c12SeisekiListBean.getC12ShiboHyoukaList();
                    Iterator	itrC12ShiboHyouka	= c12ShiboHyoukaList.iterator();

                    /** 志望大学情報 **/
                    //2019/09/26 QQ)Oosaki 共通テスト対応 DEL START
                    //// 人中セット 2004.11.29 add
                    //// 2005.01.12 Update Colの値を変数化、およびColの値に2を加算(33→35)
                    //int colPos = 49;
                    //// "人中"をループ処理で表示
                    //for(int loopCnt=7;loopCnt < 64;loopCnt++)
                    //{
                    //    workCell = cm.setCell( workSheet, workRow, workCell, loopCnt, colPos );
                    //    workCell.setCellValue( "人中" );
                    //}
                    //2019/09/26 QQ)Oosaki 共通テスト対応 DEL END

                    int shbRow = 0;
                    while(itrC12ShiboHyouka.hasNext()){
                        c12ShiboHyoukaListBean = (C12ShiboHyoukaListBean) itrC12ShiboHyouka.next();

                    // 2005.01.12 Update Colの値にそれぞれ+2

                        // 志望大学名セット
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                        //2019/09/26 QQ)Oosaki 共通テスト対応 UPD START
                        workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 43 );
                        //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 46 );
                        //2019/09/26 QQ)Oosaki 共通テスト対応 UPD END
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                        workCell.setCellValue( c12ShiboHyoukaListBean.getStrDaiMei());

                        // 志望学部名セット
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                        //2019/09/26 QQ)Oosaki 共通テスト対応 UPD START
                        workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 44 );
                        //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 47 );
                        //2019/09/26 QQ)Oosaki 共通テスト対応 UPD END
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                        workCell.setCellValue( c12ShiboHyoukaListBean.getStrGkbMei());

                        // 志望学科名セット
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                        //2019/09/26 QQ)Oosaki 共通テスト対応 UPD START
                        workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow,45 );
                        //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 48 );
                        //2019/09/26 QQ)Oosaki 共通テスト対応 UPD END
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                        workCell.setCellValue( c12ShiboHyoukaListBean.getStrGkkMei());

                        //2019/09/26 QQ)Oosaki 共通テスト対応 DEL START
                        //// 志望者順位セット
                        //if(  c12ShiboHyoukaListBean.getIntShibokounaiJyuni() != -999 ){
                        //    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 46 );
                        //    workCell.setCellValue( c12ShiboHyoukaListBean.getIntShibokounaiJyuni());
                        //}
                        //
                        //// 総志望者数セット
                        //if( c12ShiboHyoukaListBean.getIntShibosyaAll() != -999 ){
                        //    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 48 );
                        //    workCell.setCellValue( c12ShiboHyoukaListBean.getIntShibosyaAll() );
                        //}
                        //2019/09/26 QQ)Oosaki 共通テスト対応 DEL END

                        //2019/09/26 QQ)Oosaki 共通テスト対応 ADD START
                        //評価得点(英語認定試験の結果を含まない)セット
                        if( c12ShiboHyoukaListBean.getIntHyoukaTokuten() != -999 ) {
                                // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                        	//workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 49 );
                        	workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 46 );
                        	// 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                        	// 2020/02/27 QQ)Ooseto 共通テスト対応 UPD START
                        	//workCell.setCellValue( c12ShiboHyoukaListBean.getIntHyoukaTokuten() + "/" + c12ShiboHyoukaListBean.getIntHyoukaFullTokuten());
                        	if(c12ShiboHyoukaListBean.getIntHyoukaFullTokuten() != -999) {
                        	    workCell.setCellValue( c12ShiboHyoukaListBean.getIntHyoukaTokuten() + "/" + c12ShiboHyoukaListBean.getIntHyoukaFullTokuten());
                        	}
                        	else {
                        	    workCell.setCellValue( c12ShiboHyoukaListBean.getIntHyoukaTokuten() + "/");
                        	}
                        	// 2020/02/27 QQ)Ooseto 共通テスト対応 UPD END
                        }

                        //評価(英語認定試験の結果を含まない)セット
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                        //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 50 );
                        workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 47 );
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                        workCell.setCellValue( c12ShiboHyoukaListBean.getStrHyoukaMark());

                        // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 DEL START
                        //評価得点(英語認定試験の結果を含む)セット
//                        if( c12ShiboHyoukaListBean.getIntHyoukaEngTokuten() != -999 ) {
//                        	workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 51 );
//                        	workCell.setCellValue( c12ShiboHyoukaListBean.getIntHyoukaEngTokuten() + "/" + c12ShiboHyoukaListBean.getIntHyoukaEngFullTokuten());
//                        }
//
//                        //評価(英語認定試験の結果を含まない)セット
//                        if( cm.toString(c12ShiboHyoukaListBean.getStrCenterratingEngpt()).equals("") ) {
//                        } else {
//                        	String hyoka_eng = "";
//
//                        	if( cm.toString(c12ShiboHyoukaListBean.getStrCenterratingEngpt()).equals("") == false ) {
//                        		hyoka_eng = cm.padRight(c12ShiboHyoukaListBean.getStrCenterratingEngpt(), "UTF-8",2);
//                        	} else {
//                        		hyoka_eng = "  ";
//                        	}
//
//                        	if( cm.toString(c12ShiboHyoukaListBean.getStrHyoukaJodge()).equals("") == false ) {
//                        		hyoka_eng += " " + c12ShiboHyoukaListBean.getStrHyoukaJodge();
//                        	} else {
//                        		hyoka_eng += "   ";
//                        	}
//
//                        	if( cm.toString(c12ShiboHyoukaListBean.getStrHyoukaNotice()).equals("") == false ) {
//                        		hyoka_eng += cm.padRight(c12ShiboHyoukaListBean.getStrHyoukaNotice(), "UTF-8",2);
//                        	} else {
//                        		hyoka_eng += "  ";
//                        	}
//
//	                        workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 52 );
//	                        workCell.setCellValue( hyoka_eng );
//                        }
                        // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 DEL END
                        //2019/09/26 QQ)Oosaki 共通テスト対応 ADD END

                        //2019/09/30 QQ)Oosaki 共通テスト対応 DEL START
                        //// 評価得点（センター）セット
                        //if(  c12ShiboHyoukaListBean.getIntHyoukaTokuten() != -999 ){
                        //	//2019/09/26 QQ)Oosaki 共通テスト対応 UPD START
                        //    //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 50 );
                        //    workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 53 );
                        //    //2019/09/26 QQ)Oosaki 共通テスト対応 UPD END
                        //    workCell.setCellValue( c12ShiboHyoukaListBean.getIntHyoukaTokuten());
                        //}
                        //
                        //// 評価（センター）セット
                        ////2019/09/26 QQ)Oosaki 共通テスト対応 UPD START
                        ////workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 51 );
                        //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 54 );
                        ////2019/09/26 QQ)Oosaki 共通テスト対応 UPD END
                        //workCell.setCellValue( c12ShiboHyoukaListBean.getStrHyoukaMark());
                        //2019/09/30 QQ)Oosaki 共通テスト対応 DEL END

                        // 評価偏差（個別試験）セット
                        if(  c12ShiboHyoukaListBean.getFloKijyutsuHensa() != -999.0 ){
                            // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                            // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD START
                            //2019/09/30 QQ)Oosaki 共通テスト対応 UPD START
                            //	//2019/09/26 QQ)Oosaki 共通テスト対応 UPD START
                            ////workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 52 );
                            //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 55 );
                            ////2019/09/26 QQ)Oosaki 共通テスト対応 UPD END
                            //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 53 );
                            //2019/09/30 QQ)Oosaki 共通テスト対応 UPD END
                            //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 51 );
                            workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 48 );
                            // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD END
                            // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                            workCell.setCellValue( c12ShiboHyoukaListBean.getFloKijyutsuHensa());
                        }

                        // 評価（個別試験）セット
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                        // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD START
                        //2019/09/30 QQ)Oosaki 共通テスト対応 UPD START
                        ////2019/09/26 QQ)Oosaki 共通テスト対応 UPD START
                        ////workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 53 );
                        //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 56 );
                        ////2019/09/26 QQ)Oosaki 共通テスト対応 UPD END
                        //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 54 );
                        //2019/09/30 QQ)Oosaki 共通テスト対応 UPD END
                        //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 52 );
                        workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 49 );
                        // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD END
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                        workCell.setCellValue( c12ShiboHyoukaListBean.getStrHyoukaKijyutsu());

                        // 総合ポイントセット
                        if(  c12ShiboHyoukaListBean.getIntPointAll() != -999 ){
                            // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                            // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD START
                            //2019/09/30 QQ)Oosaki 共通テスト対応 UPD START
                            //	//2019/09/26 QQ)Oosaki 共通テスト対応 UPD START
                            ////workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 54 );
                            //	workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 57 );
                            ////2019/09/26 QQ)Oosaki 共通テスト対応 UPD END
                            //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 55 );
                            //2019/09/30 QQ)Oosaki 共通テスト対応 UPD END
                            //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 53 );
                            workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 50 );
                            // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD END
                            // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                            workCell.setCellValue( c12ShiboHyoukaListBean.getIntPointAll());
                        }

                        // 評価（総合）セット
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                        // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD START
                        //2019/09/30 QQ)Oosaki 共通テスト対応 UPD START
                        ////2019/09/26 QQ)Oosaki 共通テスト対応 UPD START
                        ////workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 55 );
                        //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 58 );
                        ////2019/09/26 QQ)Oosaki 共通テスト対応 UPD END
                        //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 56 );
                        //2019/09/30 QQ)Oosaki 共通テスト対応 UPD END
                        //workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 54 );
                        workCell = cm.setCell( workSheet, workRow, workCell, tabRow[row]+3+shbRow, 51 );
                        // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD END
                        // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                        workCell.setCellValue( c12ShiboHyoukaListBean.getStrHyoukaAll());
                        shbRow++;
                    }

                    row++;
                    if(row>=5){
                        bolSheetCngFlg=true;
                        if(maxSheetIndex>=maxSheetNum){
                            bolBookCngFlg=true;
                        }
                    }
                }
            }

            // Excelファイル保存
            boolean bolRet = false;
            if(fileIndex != 1){
                bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, maxSheetIndex);
            }
            else{
                bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
            }

            if( bolRet == false ){
                return errfwrite;
            }

        } catch(Exception e) {
            log.Err("C12_02","データセットエラー",e.toString());
            return errfdata;
        }

        log.Ep("C12_02","C12_02帳票作成終了","");
        return noerror;
    }

     // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
     // 2019/09/26 QQ)Oosaki 共通テスト対応 ADD START
//     /**
//      * スタイルを作成する
//      * @param  workbook - ワークブック
//      * @return style    - スタイル
//      */
//     private HSSFCellStyle createStyle(HSSFWorkbook workbook) {
//         // スタイルを新規作成
//         HSSFCellStyle style = workbook.createCellStyle();
//
//         // フォントを定義する
//         HSSFFont font = workbook.createFont();
//         // フォント名
//         font.setFontName("ＭＳ ゴシック");
//         // フォントサイズ
//         font.setFontHeightInPoints( (short)10 );
//         // 設定
//         style.setFont(font);
//         // 中央揃え
//         style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
//
//         return style;
//     }
     // 2019/09/26 QQ)Oosaki 共通テスト対応 ADD END
     // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END
}
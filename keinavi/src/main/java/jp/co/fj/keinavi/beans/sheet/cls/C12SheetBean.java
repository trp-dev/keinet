package jp.co.fj.keinavi.beans.sheet.cls;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.excel.cls.C12;
import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C12Item;
import jp.co.fj.keinavi.excel.data.cls.C12JyuniListBean;
import jp.co.fj.keinavi.excel.data.cls.C12JyuniPersonalListBean;
import jp.co.fj.keinavi.excel.data.cls.C12KmkListBean;
import jp.co.fj.keinavi.excel.data.cls.C12KmkSeisekiListBean;
import jp.co.fj.keinavi.excel.data.cls.C12ListBean;
import jp.co.fj.keinavi.excel.data.cls.C12SeisekiListBean;
import jp.co.fj.keinavi.excel.data.cls.C12ShiboHyoukaListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 2005.02.28	Yoshimoto KAWAI - Totec
 *           	担当クラスの年度対応
 *
 * 2005.03.30	Yoshimoto KAWAI - Totec
 * 				新テスト対応
 *
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 *
 * 2005.08.02	Masami Shimizu - Totec
 * 				私大模試の数学科目について全国人数を非表示にする対応
 *
 * 2005.12.08	Yoshimoto KAWAI - TOTEC
 * 				医進模試の特殊処理の対象を東北大OP以外のオープン模試に拡大
 *
 * 2007.08.24	Yuji UDONO - KCN
 * 				東北大OPについて、2007年度以降から全国順位の表示のみ再開
 *
 * 2009.11.24   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 *
 * @author kawai
 *
 */
public class C12SheetBean extends AbstractSheetBean {

	private static final long serialVersionUID = 7641789123941150588L;

	// 個人成績帳票と順位表で校内順位を無効値とする模試
	// 医進と東北大OP以外のオープン模試
	private static final Set SRANK_DISABLED_EXAM = new HashSet();

	static {
		// 医進
		SRANK_DISABLED_EXAM.add("32");
		// 東大
		SRANK_DISABLED_EXAM.add("12");
		SRANK_DISABLED_EXAM.add("13");
		// 京大
		SRANK_DISABLED_EXAM.add("15");
		SRANK_DISABLED_EXAM.add("16");
		// 名大
		SRANK_DISABLED_EXAM.add("18");
		SRANK_DISABLED_EXAM.add("19");
		// 東工大
		SRANK_DISABLED_EXAM.add("21");
		// 一橋大
		SRANK_DISABLED_EXAM.add("22");
		// 阪大
		SRANK_DISABLED_EXAM.add("24");
		// 神大
		SRANK_DISABLED_EXAM.add("25");
		// 広大
		SRANK_DISABLED_EXAM.add("26");
		// 九大
		SRANK_DISABLED_EXAM.add("27");
		// 北大
		SRANK_DISABLED_EXAM.add("41");
		// 早慶大
		SRANK_DISABLED_EXAM.add("31");
	}

	// データクラス
	private final C12Item data = new C12Item();

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	@SuppressWarnings("resource")
	public void execute() throws SQLException, Exception {

		// 対象クラスの年度
		final String current = ProfileUtil.getChargeClassYear(profile);
		// 東北大OPは順位に関するデータは入れない
		final boolean rank = !("42".equals(exam.getExamCD()) || "43".equals(exam.getExamCD()));
		// センターリサーチかどうか
		final boolean cr = "38".equals(exam.getExamCD());
		// 医進模試かどうか
		// 東北大OP以外のオープン模試も対象となる
		final boolean medical = SRANK_DISABLED_EXAM.contains(exam.getExamCD());

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshCd(exam.getExamCD()); // 模試コード
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntHyouFlg(super.getIntFlag(IProfileItem.CHART)); // 表フラグ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ
		// 2019/11/25 QQ)Ooseto 英語認定試験延期対応 DEL START
		// 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD START
		//String cefr = "";
		// 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD END
		// 2019/11/25 QQ)Ooseto 英語認定試験延期対応 DEL END

		// 表
		switch (super.getIntFlag(IProfileItem.IND_SCORE_RANK)) {
			case  1: data.setIntJyuniFlg(0); data.setIntRankFlg(1); break;
			case  2: data.setIntJyuniFlg(0); data.setIntRankFlg(2); break;
			case 11: data.setIntJyuniFlg(1); data.setIntRankFlg(1); break;
			case 12: data.setIntJyuniFlg(1); data.setIntRankFlg(2); break;
		}


		// 志望大学表示フラグ
		// 新テストは「なし」固定
		if (KNUtil.isNewExam(exam)) {
			data.setIntShiboFlg(2);
		} else {
			data.setIntShiboFlg(super.getIntFlag(IProfileItem.UNIV_DISP));
		}

		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ

		// ワークテーブルへデータを入れる
		super.insertIntoCountChargeClass();

		// 表フラグが1ならデータリストを作る
		if (data.getIntHyouFlg() == 1) {
			PreparedStatement ps1 = null;
			PreparedStatement ps2 = null;
			PreparedStatement ps3 = null;
			PreparedStatement ps4 = null;
			ResultSet rs1 = null;
			ResultSet rs2 = null;
			ResultSet rs3 = null;
			ResultSet rs4 = null;
			// 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD START
			String KokugoKijutuScore = "";
			// 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD END

			try {
				// 担当クラス
				ps1 = conn.prepareStatement(QueryLoader.getInstance().load("c11_1").toString());

				// データリスト
				ps2 = conn.prepareStatement(QueryLoader.getInstance().load("c12_1").toString());
				ps2.setString(1, login.getUserID()); // 学校コード
				ps2.setString(2, current); // 現在の年度
				ps2.setString(5, exam.getExamYear()); // 模試年度
				ps2.setString(6, exam.getExamCD()); // 模試コード
				ps2.setString(7, login.getUserID()); // 学校コード
				ps2.setString(8, current); // 現在の年度
				ps2.setString(10, exam.getExamYear()); // 模試年度
				ps2.setString(11, exam.getExamCD()); // 模試コード

				// 科目別成績データリスト
				Query query1 = QueryLoader.getInstance().load("c12_2");

				if (data.getIntShiboFlg() == 1) query1.append("AND es.subcd < 9000 ");

				query1.append("ORDER BY c.coursecd,");
				query1.append(" basicflg,");
				query1.append(" case when ri.examcd in ('01','02','03','04','38') then");
				query1.append("     case when ri.scope=9 or ri.scope is null then es.dispsequence");
				query1.append("     else case substr(ri.subcd,1,1) when '4' then 4000");
				query1.append("                                    when '5' then 5000");
				query1.append("          else es.dispsequence");
				query1.append("          end");
				query1.append("     end");
				query1.append(" else es.dispsequence");
				query1.append(" end ASC,");
				query1.append(" ri.scope desc");

                ps3 = conn.prepareStatement(query1.toString());
                ps3.setString(2, profile.getBundleCD()); // 一括コード
                ps3.setString(3, profile.getBundleCD()); // 一括コード
                ps3.setString(4, exam.getExamYear()); // 模試年度
                ps3.setString(5, exam.getExamCD()); // 模試コード

                // 志望校評価データリスト
                Query query2 = QueryLoader.getInstance().load("c12_3");
                super.rewriteUnivQuery(query2);

                ps4 = conn.prepareStatement(query2.toString());
                ps4.setString(2, exam.getExamYear()); // 模試年度
                ps4.setString(3, exam.getExamCD()); // 模試コード
                ps4.setString(4, exam.getExamDiv()); // 模試区分コード

                rs1 = ps1.executeQuery();
                while (rs1.next()) {
                    C12ListBean bean = new C12ListBean();
                    bean.setStrGrade(rs1.getString(1));
                    bean.setStrClass(rs1.getString(3));
                    data.getC12List().add(bean);

                    // 学年
                    ps2.setInt(3, rs1.getInt(1));
                    // クラス
                    ps2.setString(4, rs1.getString(2));
                    ps2.setString(9, rs1.getString(2));

                    rs2 = ps2.executeQuery();
                    while (rs2.next()) {
                        C12SeisekiListBean s = new C12SeisekiListBean();
                        s.setStrGrade(rs2.getString(2));
                        s.setStrClass(rs2.getString(3));
                        s.setStrClassNo(rs2.getString(4));
                        s.setStrKanaName(rs2.getString(5));
                        s.setStrSex(rs2.getString(6));
                        bean.getC12SeisekiList().add(s);

                        // 個人ID
                        ps3.setString(1, rs2.getString(1));
                        ps4.setString(1, rs2.getString(1));

                        // 科目別成績データリスト
                        List c1 = new LinkedList(); // 型の入れ物
                        List c2 = new LinkedList(); // 科目の入れ物

                        rs3 = ps3.executeQuery();
                        while (rs3.next()) {
                            C12KmkSeisekiListBean k = new C12KmkSeisekiListBean();
                            k.setStrKyokaCd(rs3.getString(1));
                            k.setStrKyokamei(rs3.getString(2));
                            k.setStrKmkCd(rs3.getString(3));
                            k.setStrKmkmei(rs3.getString(4));
                            k.setIntHaiten(rs3.getInt(5));
                            // 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD START
                            if(!("3915").equals(rs3.getString(3))) {
                            // 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD END
                            	k.setIntTokuten(rs3.getInt(6));
                            // 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD START
                            } else {
                            	if(rs3.getString(20).equals("01")) {
                                	KokugoKijutuScore = rs3.getString(21)+rs3.getString(22)+rs3.getString(23)+rs3.getString(24);
                                	k.setStrKokugoTotalHyouka(KokugoKijutuScore);
                                	}
                            }
                            // 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD END
                            k.setFloHensaHome(rs3.getFloat(7));
                            k.setFloHensaAll(rs3.getFloat(8));
                            k.setIntKansanTokuten(rs3.getInt(11));
                            k.setStrScholarLevel(null2Empty(rs3.getString(14)));
                            k.setIntScope(rs3.getInt(15));
                            k.setBasicFlg(rs3.getString(16));
                            // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 DEL START
							// 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD START
//                            cefr = cm.cefr(rs3.getString(18), rs3.getString(19), rs3.getString(17), conn);
//                            k.setStrCefrScore(cefr);
//                            k.setStrMosiType(rs3.getString(20));
							// 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD END
                            // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 DEL END

                            // 東北大OP以外
                            if (rank) {
                                // センターリサーチまたは医進模試
                                // 校内データは不要
                                if (cr || medical) {
                                    k.setIntNinzuHome(-999);
                                    k.setIntNinzuAll(rs3.getInt(10));
                                    k.setIntJyuniHome(-999);
                                    k.setIntJyuniAll(rs3.getInt(13));
                                // それ以外
                                } else {
                                    k.setIntNinzuHome(rs3.getInt(9));
                                    k.setIntNinzuAll(rs3.getInt(10));
                                    k.setIntJyuniHome(rs3.getInt(12));
                                    k.setIntJyuniAll(rs3.getInt(13));
                                }
                            // 東北大OP
                            } else {
                                // 2007年度以降：全国順位データのみ出力
                                if(Integer.parseInt(exam.getExamYear()) > 2006){
                                    k.setIntNinzuHome(-999);
                                    k.setIntNinzuAll(rs3.getInt(10));
                                    k.setIntJyuniHome(-999);
                                    k.setIntJyuniAll(rs3.getInt(13));
                                }
                                // 2006年度以前：順位データは不要
                                else{
                                    k.setIntNinzuHome(-999);
                                    k.setIntNinzuAll(-999);
                                    k.setIntJyuniHome(-999);
                                    k.setIntJyuniAll(-999);
                                }
                            }

                            if (rs3.getInt(3) >= 7000) c1.add(k); // 型
                            else c2.add(k); // 科目
                        }
                        rs3.close();

                        // 型・科目の順番に詰める
                        s.getC12KmkSeisekiList().addAll(c1);
                        s.getC12KmkSeisekiList().addAll(c2);

                        rs4 = ps4.executeQuery();
                        while (rs4.next()) {
                            C12ShiboHyoukaListBean h = new C12ShiboHyoukaListBean();
                            h.setStrDaiMei(rs4.getString(1));
                            h.setStrGkbMei(rs4.getString(2));
                            h.setStrGkkMei(rs4.getString(3));
                            h.setIntShibosyaAll(rs4.getInt(4));
                            h.setIntShibokounaiJyuni(rs4.getInt(5));
                            h.setIntHyoukaTokuten(rs4.getInt(6));
                            h.setStrHyoukaMark(rs4.getString(7));
                            h.setFloKijyutsuHensa(rs4.getFloat(8));
                            h.setStrHyoukaKijyutsu(rs4.getString(9));
                            h.setIntPointAll(rs4.getInt(10));
                            h.setStrHyoukaAll(rs4.getString(11));
                            // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD START
                            //2019/09/26 QQ)Oosaki 共通テスト対応 ADD START
                            //h.setStrCenterratingEngpt(rs4.getString(12));
                            //2019/09/26 QQ)Oosaki 共通テスト対応 ADD END
                            // 2019/09/19 QQ)Tanouchi 共通テスト対応 ADD START
                            //h.setStrHyoukaJodge(rs4.getString(13));
                            //h.setStrHyoukaNotice(rs4.getString(14));
                            // 2019/09/19 QQ)Tanouchi 共通テスト対応 ADD END
                            // 2019/09/24 QQ)Tanouchi 共通テスト対応 ADD START
                            //h.setIntHyoukaEngTokuten(rs4.getInt(15));
                            //h.setIntHyoukaFullTokuten(rs4.getInt(16));
                            h.setIntHyoukaFullTokuten(rs4.getInt(12));
                            //h.setIntHyoukaEngFullTokuten(rs4.getInt(17));
                            // 2019/09/24 QQ)Tanouchi 共通テスト対応 ADD END
                            // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD END
                            s.getC12ShiboHyoukaList().add(h);
                        }
                        rs4.close();
                    }
                    rs2.close();
                }
            } finally {
                DbUtils.closeQuietly(rs1);
                DbUtils.closeQuietly(rs2);
                DbUtils.closeQuietly(rs3);
                DbUtils.closeQuietly(rs4);
                DbUtils.closeQuietly(ps1);
                DbUtils.closeQuietly(ps2);
                DbUtils.closeQuietly(ps3);
                DbUtils.closeQuietly(ps4);
            }
        }

        // 型・科目別成績順位フラグが1なら順位表データリストを作る
        if (data.getIntJyuniFlg() == 1) {
            String[] Class = new String[50];
            PreparedStatement ps1 = null;
            PreparedStatement ps2a = null;
            PreparedStatement ps2b = null;
            PreparedStatement ps2c = null;
            PreparedStatement ps3a = null;
            PreparedStatement ps3b = null;
            ResultSet rs1 = null;
            ResultSet rs2 = null;
            ResultSet rs3 = null;
            String Classcheck = "";
            String Gradecheck = "";
            String gradecheck = "";
            int g = 0;
            try {
                // 担当クラス
                ps1 = conn.prepareStatement(QueryLoader.getInstance().load("c11_1").toString());

                // 型・科目リスト（通常クラス）
                ps2a = conn.prepareStatement(QueryLoader.getInstance().load("c12_4a").toString());
                ps2a.setString(1, profile.getBundleCD()); // 一括コード
                ps2a.setString(4, exam.getExamYear()); // 模試年度
                ps2a.setString(5, exam.getExamCD()); // 模試コード

                // 型・科目リスト（複合クラス）
                ps2b = conn.prepareStatement(QueryLoader.getInstance().load("c12_4b").toString());
                ps2b.setString(2, profile.getBundleCD()); // 一括コード
                ps2b.setString(3, exam.getExamYear()); // 模試年度
                ps2b.setString(4, exam.getExamCD()); // 模試コード


                // 順位表個人データリスト(クラス別)
                ps3a = conn.prepareStatement(QueryLoader.getInstance().load("c12_5a").toString());
                ps3a.setString(1, login.getUserID()); // 学校コード
                ps3a.setString(2, current); // 現在の年度
                ps3a.setString(5, exam.getExamYear()); // 模試年度
                ps3a.setString(6, exam.getExamCD()); // 模試コード
                ps3a.setString(8, login.getUserID()); // 学校コード
                ps3a.setString(9, current); // 現在の年度
                ps3a.setString(11, exam.getExamYear()); // 模試年度
                ps3a.setString(12, exam.getExamCD()); // 模試コード

                rs1 = ps1.executeQuery();
                while(rs1.next()) {
                    String Grade="";
                    int grade =rs1.getInt(1);

                    Grade = String.format("%02d",grade);

                    String classCd = rs1.getString(2);
// 2019/09/26 QQ)Tanioka 順位表障害対応 ADD START
                    String className = rs1.getString(3);
// 2019/09/26 QQ)Tanioka 順位表障害対応 ADD END
                    gradecheck = exam.getTargetGrade();

                    if(data.getIntRankFlg()==2) {
                    //1学年のクラス順位表の作成
                    if(Grade.equals(gradecheck)) {
                        Class[g] =  classCd;
                        //2019/09/06 QQ)Tanouchi 共通テスト対応 UPD START
                        //Classcheck= Classcheck+classCd +" ";
// 2019/09/26 QQ)Tanioka 順位表障害対応 UPD START
//                        Classcheck= Classcheck+classCd +",";
                        Classcheck= Classcheck+className +",";
// 2019/09/26 QQ)Tanioka 順位表障害対応 UPD END
                        //2019/09/06 QQ)Tanouchi 共通テスト対応 UPD END
                        Gradecheck =String.format("%1d",grade);
                        // クラス
                        g++;
                        continue;
                         }
                    }

                } DbUtils.closeQuietly(rs1);
                //2019/09/06 QQ)Tanouchi 共通テスト対応 UPD START
                if(Classcheck.endsWith(",")) {
            		Classcheck = Classcheck.substring(0,Classcheck.length()-1);
            	}
                //2019/09/06 QQ)Tanouchi 共通テスト対応 UPD END

                // 型・科目リスト（選択クラス合計）
                Query query2 =QueryLoader.getInstance().load("c12_4c");
                query2.setStringArray(1, Class);
                ps2c = conn.prepareStatement(query2.toString());
                ps2c.setString(1, profile.getBundleCD()); // 一括コード
                ps2c.setString(2, exam.getTargetGrade());// 学年
                ps2c.setString(3, exam.getExamYear()); // 模試年度
                ps2c.setString(4, exam.getExamCD()); // 模試コード


                // 順位表個人データリスト(選択クラス合計)
                Query query =QueryLoader.getInstance().load("c12_5b");
                query.setStringArray(1, Class);
                query.setStringArray(2, Class);
                ps3b = conn.prepareStatement(query.toString());
                ps3b.setString(1, login.getUserID()); // 学校コード
                ps3b.setString(2, current); // 現在の年度
                ps3b.setString(3, exam.getTargetGrade());// 学年
                ps3b.setString(4, exam.getExamYear()); // 模試年度
                ps3b.setString(5, exam.getExamCD()); // 模試コード
                ps3b.setString(7, login.getUserID()); // 学校コード
                ps3b.setString(8, current); // 現在の年度
                ps3b.setString(9, exam.getExamYear()); // 模試年度
                ps3b.setString(10, exam.getExamCD()); // 模試コード



                rs1 = ps1.executeQuery();
                while (rs1.next()) {
                    C12JyuniListBean j = new C12JyuniListBean();
                    if(data.getIntRankFlg()==2) {
                        j.setStrGrade(Gradecheck);
                        j.setStrClass(Classcheck);
                    }else {
                        j.setStrGrade(rs1.getString(1));
                        j.setStrClass(rs1.getString(3));
                    }
                    data.getC12JyuniList().add(j);

                    int grade = rs1.getInt(1);
                    String classCd = rs1.getString(2);

                    if(data.getIntRankFlg()==1) {
                     // 学年
                        ps3a.setInt(3, grade);
                        // クラス
                        ps3a.setString(4, classCd);
                        ps3a.setString(10, classCd);

                    }
                    // 選択クラス合計
                    if(data.getIntRankFlg()==2) {
                        rs2 = ps2c.executeQuery();
                    // 複合クラス
                        }else if (classCd.length() == 7) {
                        ps2b.setString(1, classCd);
                        rs2 = ps2b.executeQuery();
                    // 通常クラス
                    } else {
                        ps2a.setInt(2, grade);
                        ps2a.setString(3, classCd);
                        rs2 = ps2a.executeQuery();
                    }

                    // 型・科目別データリスト
                    List c1 = new LinkedList(); // 型の入れ物
                    List c2 = new LinkedList(); // 科目の入れ物

                    while (rs2.next()) {
                        C12KmkListBean k = new C12KmkListBean();
                        k.setStrKmkCd(rs2.getString(1));
                        k.setStrKmkmei(rs2.getString(2));
                        k.setStrHaitenkmk(rs2.getString(3));
                        k.setIntNinzu(rs2.getInt(4));

                        if (rs2.getInt(1) >= 7000) c1.add(k); // 型
                        else c2.add(k); // 科目

                        if(data.getIntRankFlg()==1) {
                            // 型・科目コード
                            ps3a.setString(7, rs2.getString(1));
                            ps3a.setString(13, rs2.getString(1));
                            rs3 = ps3a.executeQuery();
                           }else {
                            // 型・科目コード
                            ps3b.setString(6, rs2.getString(1));
                            ps3b.setString(11, rs2.getString(1));
                            rs3 = ps3b.executeQuery();
                           }


                        while (rs3.next()) {
                            C12JyuniPersonalListBean p = new C12JyuniPersonalListBean();

                            // 医進模試は順位不要
                            if (medical) {
                                p.setIntJyuni(-999);
                            // それ以外
                            } else {
                                p.setIntJyuni(rs3.getInt(1));
                            }

                            p.setStrGrade(rs3.getString(2));
                            p.setStrClass(rs3.getString(3));
                            p.setStrClassNo(rs3.getString(4));
                            p.setStrKanaName(rs3.getString(5));
                            p.setStrSex(rs3.getString(6));
                            p.setIntTokuten(rs3.getInt(7));
                            p.setFloHensaHome(rs3.getFloat(8));
                            p.setFloHensaAll(rs3.getFloat(9));
                            p.setStrScholarLevel(null2Empty(rs3.getString(10)));
                            k.getC12JyuniPersonalList().add(p);
                        }
                        rs3.close();
                    }
                    rs2.close();

                    // 型・科目の順番に詰める
                    j.getC12KmkList().addAll(c1);
                    j.getC12KmkList().addAll(c2);
                    if(data.getIntRankFlg()==2){
                        break;
                    }
                }
            } finally {
                DbUtils.closeQuietly(rs1);
                DbUtils.closeQuietly(rs2);
                DbUtils.closeQuietly(rs3);
                DbUtils.closeQuietly(ps1);
                DbUtils.closeQuietly(ps2a);
                DbUtils.closeQuietly(ps2b);
                DbUtils.closeQuietly(ps2c);
                DbUtils.closeQuietly(ps3a);
                DbUtils.closeQuietly(ps3b);
            }
        }
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
     */
    protected double calcNumberOfPrint() {
        return getNumberOfPrint("C12_01");
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
     */
    protected String getCategory() {
        return IProfileCategory.C_COND_IND;
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
     */
    protected boolean createSheet() {
        return new C12()
            .c12(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
    }

    /**
     * nullを空白にする
     * @return
     */
    private String null2Empty(String input){
        String rt = "";
        if(input != null) rt = input;
        return rt;
    }
    // 2019/09/19 QQ)Tanouchi 共通テスト対応 ADD START
    private CM cm = new CM();
	// 2019/09/19 QQ)Tanouchi 共通テスト対応 ADD END

}

package jp.co.fj.keinavi.beans.user;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.data.user.LoginUserData;
import jp.co.fj.keinavi.util.KNValidator;
import jp.co.fj.keinavi.util.PasswordGenerator;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 利用者登録Bean
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class LoginUserInsertBean extends DefaultBean {

	// 学校コード
	private final String schoolCd;
	// 利用者データ
	private final LoginUserData loginUserData;

	// パスワード生成クラス
	private static final PasswordGenerator PWDGENERATOR = new PasswordGenerator(
			"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
	
	/**
	 * コンストラクタ
	 */
	public LoginUserInsertBean(final String schoolCd, final LoginUserData loginUserData) {
		
		if (schoolCd == null) {
			throw new IllegalArgumentException("学校コードがNULLです。");
		}
		
		if (loginUserData == null) {
			throw new IllegalArgumentException("利用者データがNULLです。");
		}
		
		if (!KNValidator.isLoginId(loginUserData.getLoginId())) {
			throw new IllegalArgumentException(
					"不正な利用者IDです。" + loginUserData.getLoginId());
		}

		if (!KNValidator.isKnFunctionFlag(loginUserData.getKnFunctionFlag())) {
			throw new IllegalArgumentException(
					"不正なKei-Navi機能権限フラグです。" + loginUserData.getKnFunctionFlag());
		}
		
		if (!KNValidator.isScFunctionFlag(loginUserData.getScFunctionFlag())) {
			throw new IllegalArgumentException(
					"不正な校内成績処理機能権限フラグです。"
					+ loginUserData.getScFunctionFlag());
		}
		
		if (!KNValidator.isEeFunctionFlag(loginUserData.getEeFunctionFlag())) {
			throw new IllegalArgumentException(
					"不正な入試結果調査機能権限フラグです。"
					+ loginUserData.getEeFunctionFlag());
		}
		
		if (!KNValidator.isAbFunctionFlag(loginUserData.getAbFunctionFlag())) {
			throw new IllegalArgumentException(
					"不正な答案閲覧機能権限フラグです。"
					+ loginUserData.getAbFunctionFlag());
		}
		
		this.schoolCd = schoolCd;
		this.loginUserData = loginUserData;
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		//「admin」は不可
		if ("admin".equals(loginUserData.getLoginId())) {
			throw new KNBeanException();
		}
		
		// INSERT
		registUser();
		// 担当クラスを更新
		registClass();
	}
	
	/**
	 * 利用者情報を登録する
	 * 
	 * @throws Exception
	 */
	protected void registUser() throws Exception {
		
		// 初期パスワードをセット
		loginUserData.setDefaultPwd(PWDGENERATOR.generate(8));
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("u05").toString());
			
			// 学校コード
			ps.setString(1, schoolCd);
			// 利用者ID
			ps.setString(2, loginUserData.getLoginId());
			// 利用者名
			ps.setString(3, loginUserData.getLoginName());
			// ログインパスワード
			ps.setString(4, loginUserData.getDefaultPwd());
			// デフォルトパスワード
			ps.setString(5, loginUserData.getDefaultPwd());
			
			// メンテ可
			if (loginUserData.isMaintainer()) {
				ps.setString(6, "1");
				ps.setString(7, "1");
				ps.setString(8, "1");
				ps.setString(9, "1");
				ps.setString(10, "1");
				
			// メンテ不可
			} else {
				ps.setString(6, "2");
				ps.setString(7, String.valueOf(loginUserData.getKnFunctionFlag()));
				ps.setString(8, String.valueOf(loginUserData.getScFunctionFlag()));
				ps.setString(9, String.valueOf(loginUserData.getEeFunctionFlag()));
				ps.setString(10, String.valueOf(loginUserData.getAbFunctionFlag()));
			}
			
			// 学校コード
			ps.setString(11, schoolCd);
			// 利用者ID
			ps.setString(12, loginUserData.getLoginId());
			// 学校コード
			ps.setString(13, schoolCd);
			// 利用者ID
			ps.setString(14, loginUserData.getLoginId());
			
			if (ps.executeUpdate() == 0) {
				throw new KNBeanException("利用者IDが重複しています。", "1");
			}
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	/**
	 * 利用者の担当クラスを更新する
	 * 
	 * @throws Exception 
	 */
	protected void registClass() throws Exception {
		
		final LoginUserClassRegisterBean bean = new LoginUserClassRegisterBean(
				schoolCd, loginUserData);
		bean.setConnection(null, conn);
		bean.execute();
	}
	
}

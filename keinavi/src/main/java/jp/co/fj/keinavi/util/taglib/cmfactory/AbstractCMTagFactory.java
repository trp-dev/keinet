/*
 * 作成日: 2004/07/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.taglib.cmfactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;

import org.apache.commons.configuration.Configuration;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.BaseForm;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.util.message.MessageLoader;
import jp.co.totec.config.ConfigResolver;

/**
 * 
 * 2005.05.29	Yoshimoto KAWAI - Totec
 * 				対象模試データを取得するメソッドを追加
 * 
 * 
 * @author kawai
 *
 */
public abstract class AbstractCMTagFactory {

	// Factoryを入れておくマップ
	private static Map factoryMap = new HashMap();

	static {
		final Configuration config = ConfigResolver.getInstance(
				).getConfiguration("cmtag.properties");
		for (final Iterator ite = config.getKeys(); ite.hasNext();) {
			final String key = (String) ite.next();
			final String className = config.getString(key);
			try {
				factoryMap.put(key, Class.forName(className).newInstance());
			} catch (final Exception e) {
				throw new IllegalArgumentException(
						"共通項目設定Taglibファクトリのインスタンス生成に失敗しました。" + className);
			}
		}
	}
	
	/**
	 * プロファイルの設定状況を返す
	 * @param request
	 * @return
	 */
	abstract public String createCMStatus(ServletRequest request, HttpSession session) throws JspException;
	
	/**
	 * プロファイル設定画面におけるプロファイルの設定状況を返す
	 * @param request
	 * @return
	 */
	abstract public String createTempCMStatus(ServletRequest request, HttpSession session) throws JspException;
	
	/**
	 * 指定されたFactoryクラスを返す
	 * @param key
	 * @return
	 * @throws JspException
	 */
	public static AbstractCMTagFactory createAbstractCMTagFactory(String key) throws JspException {
		if (factoryMap.containsKey(key)) {
			return (AbstractCMTagFactory)factoryMap.get(key);
		} else {
			throw new JspException("不正なアイテムの指定です。");
		}
	}

	/**
	 * リクエストからBaseFormを取得する
	 * @param request
	 * @return
	 * @throws JspException
	 */
	protected BaseForm getBaseForm(ServletRequest request) throws JspException {
		// Baseフォームを取得する
		BaseForm form = (BaseForm)request.getAttribute("form");
		if (form == null) {
			new JspException("Baseフォームの取得に失敗しました。");
		}
		return form;
	}
	
	/**
	 * 共通項目設定のアイテムマップを取得する
	 * @param request
	 * @return
	 * @throws JspException
	 */
	protected Map getItemMap(HttpSession session) throws JspException {
		return getProfile(session).getItemMap("010000");
	}

	/**
	 * プロファイルを取得する
	 * @param request
	 * @return
	 * @throws JspException
	 */
	protected Profile getProfile(HttpSession session) throws JspException {
		Profile profile = (Profile) session.getAttribute(Profile.SESSION_KEY);
		if (profile == null) {
			new JspException("プロファイルの取得に失敗しました。");
		}
		return profile;
	}
	
	/**
	 * 模試セッションを取得する
	 * @param request
	 * @return
	 * @throws JspException
	 */
	protected ExamSession getExamSession(HttpSession session) throws JspException {
		ExamSession exam = (ExamSession) session.getAttribute(ExamSession.SESSION_KEY);
		if (exam == null) {
			new JspException("プロファイルの取得に失敗しました。");
		}
		return exam;
	}
	
	/**
	 * 共通アクションフォームを取得する
	 * @param session
	 * @return
	 * @throws JspException
	 */
	protected CM001Form getCommonForm(HttpSession session) throws JspException {
		CM001Form form = (CM001Form) session.getAttribute(CM001Form.SESSION_KEY);
		if (form == null) {
			new JspException("共通アクションフォームの取得に失敗しました。");
		}
		return form;
	}
	
	/**
	 * 対象模試データを取得する
	 * 
	 * @param request
	 * @param session
	 * @return
	 * @throws JspException
	 */
	protected ExamData getExamData(final ServletRequest request, final HttpSession session) throws JspException {
		BaseForm form = getBaseForm(request);
		ExamSession es = getExamSession(session);
		return es.getExamData(form.getTargetYear(), form.getTargetExam());
	}

	/**
	 * @param  id
	 * @return外部定義メッセージ
	 * @throws JspException
	 */
	protected String getMessage(final String id) throws JspException {
		try {
			return MessageLoader.getInstance().getMessage(id);
		} catch (final IOException e) {
			final JspException j = new JspException(e.getMessage());
			j.initCause(e);
			throw j;
		}
	}
}

/*
 * 作成日: 2004/09/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.helpdesk;

import com.fjh.forms.ActionForm;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class HD101Form extends ActionForm {

	private String[] pactDiv       = null;    // 契約区分
	private String[] userDiv       = null;    // ユーザ区分
	private String   schoolCode    = "";     	// 学校コード
	private String   judge         = "0";     	// 発行済証明書条件 0:すべて 1:未発行 2:有効 3:期限切れ 4:期限指定
	private String   certEffeYear  = null;     // 有効　　　年
	private String   certEffeMonth = null;     // 　　　　　月
	private String   certEffeDay   = null;     // 　　　　　日
	private String   certOverYear  = null;     // 期限切れ　年
	private String   certOverMonth = null;     // 　　　　　月
	private String   certOverDay   = null;     // 　　　　　日
	private String   certSpecYear  = null;     // 期限指定　年
	private String   certSpecMonth = null;     // 　　　　　月
	private String   certSpecDay   = null;	// 　　　　　日
	private String[] certificates  = null;    // 証明書発行対象
	private String   sortMode      = null;    // ソートモード
	// 2015/10/16 QQ)Nishiyama デジタル証明書対応 ADD START
	private String   delUserProc   = "0";       // ユーザ削除データ取得の結果　0:データあり, 1:データなし
	private String   selectUser    = "";        // 対象ユーザ
	private String   fileName      = "";        // ファイル名
	private byte[]   fileData;                 // ファイルの内容
	// ラジオボタンの選択値
	private String   content      = "0";     // 処理の内容
	private String   tsvUser      = "0";     // 画面 or ファイル
	// 2015/10/16 QQ)Nishiyama デジタル証明書対応 ADD END

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		//

	}


	/**
	 * @return
	 */
	public String[] getPactDiv() {
		return pactDiv;
	}

	/**
	 * @param string
	 */
	public void setPactDiv(String[] string) {
		pactDiv = string;
	}

	/**
	 * @return
	 */
	public String[] getUserDiv() {
		return userDiv;
	}

	/**
	 * @param string
	 */
	public void setUserDiv(String[] string) {
		userDiv = string;
	}

	/**
	 * @return
	 */
	public String getSchoolCode() {
		return schoolCode;
	}

	/**
	 * @param string
	 */
	public void setSchoolCode(String string) {
		schoolCode = string;
	}

	/**
	 * @return
	 */
	public String getJudge() {
		return judge;
	}

	/**
	 * @param string
	 */
	public void setJudge(String string) {
		judge = string;
	}

	/**
	 * @return
	 */
	public String getCertEffeYear() {
		return certEffeYear;
	}

	/**
	 * @param string
	 */
	public void setCertEffeYear(String string) {
		certEffeYear = string;
	}
	/**
	 * @return
	 */
	public String getCertEffeMonth() {
		return certEffeMonth;
	}

	/**
	 * @param string
	 */
	public void setCertEffeMonth(String string) {
		certEffeMonth = string;
	}
	/**
	 * @return
	 */
	public String getCertEffeDay() {
		return certEffeDay;
	}

	/**
	 * @param string
	 */
	public void setCertEffeDay(String string) {
		certEffeDay = string;
	}

	/**
	 * @return
	 */
	public String getCertOverYear() {
		return certOverYear;
	}

	/**
	 * @param string
	 */
	public void setCertOverYear(String string) {
		certOverYear = string;
	}
	/**
	 * @return
	 */
	public String getCertOverMonth() {
		return certOverMonth;
	}

	/**
	 * @param string
	 */
	public void setCertOverMonth(String string) {
		certOverMonth = string;
	}
	/**
	 * @return
	 */
	public String getCertOverDay() {
		return certOverDay;
	}

	/**
	 * @param string
	 */
	public void setCertOverDay(String string) {
		certOverDay = string;
	}
	// ---------------------------------------------
	/**
	 * @return
	 */
	public String getCertSpecYear() {
		return certSpecYear;
	}

	/**
	 * @param string
	 */
	public void setCertSpecYear(String string) {
		certSpecYear = string;
	}
	/**
	 * @return
	 */
	public String getCertSpecMonth() {
		return certSpecMonth;
	}

	/**
	 * @param string
	 */
	public void setCertSpecMonth(String string) {
		certSpecMonth = string;
	}
	/**
	 * @return
	 */
	public String getCertSpecDay() {
		return certSpecDay;
	}

	/**
	 * @param string
	 */
	public void setCertSpecDay(String string) {
		certSpecDay = string;
	}

	/**certificates
	 * @return
	 */
	public String[] getCertificates() {
		return certificates;
	}

	/**
	 * @param string
	 */
	public void setCertificates(String[] string) {
		certificates = string;
	}

	/**
	 * @return
	 */
	public String getSortMode() {
		return sortMode;
	}

	/**
	 * @param string
	 */
	public void setSortMode(String string) {
		sortMode = string;
	}

	// 2015/10/16 QQ)Nishiyama デジタル証明書対応 ADD START
	/**
	 * @return
	 */
	public String getDelUserProc() {
		return delUserProc;
	}

	/**
	 * @param string
	 */
	public void setDelUserProc(String string) {
		delUserProc = string;
	}

	/**
	 * @return
	 */
	public String getSelectUser() {
		return selectUser;
	}

	/**
	 * @param string
	 */
	public void setSelectUser(String string) {
		selectUser = string;
	}

	/**
	 * @return
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param string
	 */
	public void setFileName(String string) {
		fileName = string;
	}

	/**
	 * @return
	 */
	public byte[] getFileData() {
		return fileData;
	}

	/**
	 * @param bs
	 */
	public void setFileData(byte[] bs) {
		fileData = bs;
	}

	/**
	 * @return
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param string
	 */
	public void setContent(String string) {
		content = string;
	}

	/**
	 * @return
	 */
	public String getTsvUser() {
		return tsvUser;
	}

	/**
	 * @param string
	 */
	public void setTsvUser(String string) {
		tsvUser = string;
	}
	// 2015/10/16 QQ)Nishiyama デジタル証明書対応 ADD END

}

/**
 * 校内成績分析−過年度比較　成績概況
 * 	Excelファイル編集
 * 作成日: 2004/07/26
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S21Item;
import jp.co.fj.keinavi.excel.data.school.S21ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S21_01 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	//共通関数用クラス インスタンス

	final private String	masterfile0	= "S21_01";		// ファイル名
	final private String	masterfile1	= "NS21_01";	// ファイル名
	private String	masterfile	= "";					// ファイル名
	final private int[]	tabRow		= {4,13,22,31,40,49};				// 表の基準点
	final private int[]	tabCol		= {1,5,9,13,17,21,25,29,33,37};		// 表の基準点


	/*
	 * 	Excel編集メイン
	 * 		S21Item s21Item: データクラス
	 * 		String masterfile: マスタExcelファイル名（フルパス）
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
		public int s21_01EditExcel(S21Item s21Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
			KNLog log = KNLog.getInstance(null,null,null);
			log.Ep("S21_01","S21_01帳票作成開始","");
			
			HSSFWorkbook		workbook		= null;
			HSSFSheet			workSheet		= null;
			HSSFRow				workRow			= null;
			HSSFCell			workCell		= null;
			
			int		maxSheetIndex	= 0;		// シートカウンター
			float		hensa			= 0;		// 今年度の偏差値保持
			int		kmkCd			= 0;		// 今年度の科目コード保持
			int		hyouCnt			= 0;		// 値範囲：０〜５(表カウンター)
			int		kmkCnt			= 0;		// 値範囲：０〜９(型・科目カウンター)
			boolean	kataFlg			= true;	// false:改行済(型・科目名変更済)　
			boolean	bolSheetCngFlg	= true;	//改シートフラグ
			
			//テンプレートの決定
			if (s21Item.getIntShubetsuFlg() == 1){
				masterfile = masterfile1;
			} else{
				masterfile = masterfile0;
			}

			//マスタExcel読み込み
			workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
			if( workbook==null ){
				return errfread;
			}
			
			// 基本ファイルを読込む
			S21ListBean s21ListBean = new S21ListBean();
			
			try {
				// 型データセット
				ArrayList	s21List		= s21Item.getS21List();
				Iterator	itr21List	= s21List.iterator();
				int row = 0;
				
				while( itr21List.hasNext() ) {
					s21ListBean = (S21ListBean)itr21List.next();
					
					//項目変更
					if(( row > 0 )&&( row < 5 )){
						if(kmkCd != Integer.parseInt(s21ListBean.getStrKmkCd())){
							//下の改項目・改表処理を通るようにする
							row = 5;
						}
					}
					if( row >= 5 ){
						row = 0;
						kmkCnt++;
						//改表
						if( kmkCnt >= 10 ){
							kmkCnt = 0;
							hyouCnt++;
						}
					}
					
					//型から科目に変わる時のチェック
					if ( s21ListBean.getStrKmkCd().compareTo("7000") < 0 ) {
						if( kataFlg ){
							if( kmkCnt > 0 ){
								kmkCnt = 0;
								hyouCnt++;
							}
						}
						kataFlg	= false;
					}
					
					if(hyouCnt >= 6){
						bolSheetCngFlg = true ;
					}
					
					// シートテンプレートのコピー
					if( bolSheetCngFlg == true ){
						workSheet = workbook.cloneSheet(0);
						maxSheetIndex++;
						
						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);
						
						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, s21Item.getIntSecuFlg() ,37 ,40 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 37 );
						workCell.setCellValue(secuFlg);
						
						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( "学校名　：" + cm.toString(s21Item.getStrGakkomei()) );
						
						// 対象模試セット
						String moshi =cm.setTaisyouMoshi( s21Item.getStrMshDate() );	// 模試月取得
						workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
						workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s21Item.getStrMshmei()) + moshi);
						
						row = 0;
						kmkCnt = 0;
						hyouCnt = 0;
						bolSheetCngFlg = false ;
					}
					
					if( row == 0 ){
						// 型名セット
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt], tabCol[kmkCnt] );
						workCell.setCellValue( s21ListBean.getStrKmkmei() );

						// 配点セット
						if ( !cm.toString(s21ListBean.getStrHaitenKmk()).equals("") ) {
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+1, tabCol[kmkCnt] );
							workCell.setCellValue("(" + s21ListBean.getStrHaitenKmk() + ")" );
						}

						//今年度の偏差値保持
						hensa	= s21ListBean.getFloHensa();
						kmkCd	= Integer.parseInt(s21ListBean.getStrKmkCd());
					}
					
					if(kmkCnt == 0){
						// 年度セット
						if ( !cm.toString(s21ListBean.getStrNendo()).equals("") ) {
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, 0 );
							workCell.setCellValue( s21ListBean.getStrNendo() +"年度" );
						}
					}
					
					// 人数セット
					if ( s21ListBean.getIntNinzu() != -999 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[kmkCnt] );
						workCell.setCellValue( s21ListBean.getIntNinzu() );
					}
					
					// 平均点セット
					if ( s21ListBean.getFloHeikin() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[kmkCnt]+1 );
						workCell.setCellValue( s21ListBean.getFloHeikin() );
					}
					
					// 平均偏差値セット
					if ( s21ListBean.getFloHensa() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[kmkCnt]+3 );
						workCell.setCellValue( s21ListBean.getFloHensa() );
					}
					
					// *セット
					if ( hensa != -999.0 ) {
						if ( hensa < s21ListBean.getFloHensa() ) {
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[kmkCnt]+2 );
							workCell.setCellValue("*");
						}
					}
					row++;
				}
//add 2004/10/25 T.Sakai データ0件対応
				if ( s21List.size()==0 ) {
					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);
					maxSheetIndex++;
					
					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);
					
					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, s21Item.getIntSecuFlg() ,37 ,40 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 37 );
					workCell.setCellValue(secuFlg);
					
					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( "学校名　：" + cm.toString(s21Item.getStrGakkomei()) );
					
					// 対象模試セット
					String moshi =cm.setTaisyouMoshi( s21Item.getStrMshDate() );	// 模試月取得
					workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
					workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s21Item.getStrMshmei()) + moshi);
				}
//add end
				// Excelファイル保存
				boolean bolRet = false;
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);

				if( bolRet == false ){
					return errfwrite;					
				}
			
			} catch(Exception e) {
				log.Err("S21_01","データセットエラー",e.toString());
				return errfdata;
			}

			log.Ep("S21_01","S21_01帳票作成終了","");
			return noerror;
		}


}

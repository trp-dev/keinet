/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.configuration.Configuration;

import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.totec.config.ConfigResolver;

import com.fjh.forms.ActionForm;

/**
 *
 * アクションフォームファクトリ
 * 
 * 2004.07.06	[新規作成]
 * 
 * 2006.01.24	Yoshimoto KAWAI - TOTEC
 * 				設定を外部ファイル化 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public abstract class AbstractActionFormFactory {

	// InitServletで初期化する
	private static Map factoryMap = new HashMap();
	
	static {
		final Configuration config = ConfigResolver.getInstance(
				).getConfiguration("form.properties");
		for (final Iterator ite = config.getKeys(); ite.hasNext();) {
			final String key = (String) ite.next();
			final String className = config.getString(key);
			try {
				factoryMap.put(key, Class.forName(className).newInstance());
			} catch (final Exception e) {
				throw new IllegalArgumentException(
						"アクションフォームファクトリのインスタンス生成に失敗しました。" + className);
			}
		}
	}
	
	/**
	 * アクションフォームを生成する
	 * @param request
	 * @return
	 */
	abstract public ActionForm createActionForm(HttpServletRequest request); 

	/**
	 * アクションフォームの値をプロファイルへ戻す
	 * @param request
	 */
	abstract public void restore(HttpServletRequest request);


	/**
	 * アイテムマップを取得する
	 * （アイテムマップはフォーム毎に異なるので
	 *  それぞれのConcreteFactoryで実装すること）
	 * @param request
	 * @return
	 */
	abstract public Map getItemMap(HttpServletRequest request);

	/**
	 * ActionFormのFactoryを返す
	 * @param key
	 * @return
	 */
	public static AbstractActionFormFactory getFactory(String key) {
		return (AbstractActionFormFactory)factoryMap.get(key);
	}

	/**
	 * リクエストパラメータから対象年度を取得する
	 * @param request
	 * @return
	 */
	protected String getTargetYear(HttpServletRequest request) {
		return request.getParameter("targetYear");
	}

	/**
	 * リクエストパラメータから対象模試を取得する
	 * @param request
	 * @return
	 */
	protected String getTargetExam(HttpServletRequest request) {
		return request.getParameter("targetExam");
	}
	
	/**
	 * セッションからプロファイルを取得する
	 * @param request
	 * @return
	 */
	protected Profile getProfile(HttpServletRequest request) {
		return (Profile) request.getSession(false).getAttribute(Profile.SESSION_KEY);
	}

	/**
	 * セッションから模試セッションを取得する
	 * @param request
	 * @return
	 */
	protected ExamSession getExamSession(HttpServletRequest request) {
		return (ExamSession) request.getSession().getAttribute(ExamSession.SESSION_KEY);
	}

	/**
	 * セッションからログイン情報を取得する
	 * @param request
	 * @return
	 */
	protected LoginSession getLoginSession(HttpServletRequest request) {
		return (LoginSession) request.getSession(false).getAttribute(LoginSession.SESSION_KEY);
	}

}

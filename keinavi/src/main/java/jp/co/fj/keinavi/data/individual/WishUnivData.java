/*
 * 作成日: 2004/09/23
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

import java.util.List;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class WishUnivData implements Comparable{
	
	private String individualId;
	private String examYear;
	private String examCd;
	private String candidateRank;
	private String examDiv;
	private String univCd;
	private String facultyCd;
	private String deptCd;
	private String prefCd;
	private String districtCd;//地区コード
	/**
	 * 分野は複数存在し得る
	 */
	private List univStemmaList;//分野コード(複数)
	private String univDivCd;//大学区分コード
	
	/* (非 Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o2) {
		WishUnivData d2 = (WishUnivData) o2;
		if (Integer.parseInt(this.candidateRank) < Integer.parseInt(d2.getCandidateRank())) {
			return -1;
		} else if (Integer.parseInt(this.candidateRank) > Integer.parseInt(d2.getCandidateRank())) {
			return 1;
		} else {
			return 0;
		}
	}
	
	/**
	 * @return
	 */
	public String getCandidateRank() {
		return candidateRank;
	}

	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getDistrictCd() {
		return districtCd;
	}

	/**
	 * @return
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * @return
	 */
	public String getExamDiv() {
		return examDiv;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @return
	 */
	public String getPrefCd() {
		return prefCd;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @param string
	 */
	public void setCandidateRank(String string) {
		candidateRank = string;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setDistrictCd(String string) {
		districtCd = string;
	}

	/**
	 * @param string
	 */
	public void setExamCd(String string) {
		examCd = string;
	}

	/**
	 * @param string
	 */
	public void setExamDiv(String string) {
		examDiv = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

	/**
	 * @param string
	 */
	public void setPrefCd(String string) {
		prefCd = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

	/**
	 * @return
	 */
	public String getUnivDivCd() {
		return univDivCd;
	}

	/**
	 * @param string
	 */
	public void setUnivDivCd(String string) {
		univDivCd = string;
	}

	/**
	 * @return
	 */
	public List getUnivStemmaList() {
		return univStemmaList;
	}

	/**
	 * @param list
	 */
	public void setUnivStemmaList(List list) {
		univStemmaList = list;
	}

}

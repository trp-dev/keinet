/*
 * 作成日: 2004/07/09
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.com_set;

import java.io.Serializable;
import java.util.List;

/**
 * 型・科目プロファイルデータクラス
 * 
 * @author kawai
 */
public class ComSubjectData implements Serializable {

	private String examYear; // 模試年度
	private String examCD; // 模試コード
	private List aSubjectData;// 全選択用のリスト
	private List iSubjectData; // 個別選択用のリスト

	/**
	 * コンストラクタ
	 * @param examYear
	 * @param examCD
	 */
	public ComSubjectData() {
	}

	/**
	 * コンストラクタ
	 * @param examYear
	 * @param examCD
	 */
	public ComSubjectData(String examYear, String examCD) {
		this.examYear = examYear;
		this.examCD = examCD;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		ComSubjectData data = (ComSubjectData) obj;

		return this.getExamYear().equals(data.getExamYear())
			&& this.getExamCD().equals(data.getExamCD());
	}

	/**
	 * @return
	 */
	public List getASubjectData() {
		return aSubjectData;
	}

	/**
	 * @return
	 */
	public String getExamCD() {
		return examCD;
	}

	/**
	 * @return
	 */
	public List getISubjectData() {
		return iSubjectData;
	}

	/**
	 * @param list
	 */
	public void setASubjectData(List list) {
		aSubjectData = list;
	}

	/**
	 * @param string
	 */
	public void setExamCD(String string) {
		examCD = string;
	}

	/**
	 * @param list
	 */
	public void setISubjectData(List list) {
		iSubjectData = list;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

}

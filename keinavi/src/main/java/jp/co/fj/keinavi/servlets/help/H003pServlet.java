/*
 * 作成日: 2004/10/08
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.help;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.help.HelpListBean;
import jp.co.fj.keinavi.forms.help.H003pForm;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class H003pServlet extends DefaultHttpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// アクションフォーム
		final H003pForm form = (H003pForm) getActionForm(request,
				"jp.co.fj.keinavi.forms.help.H003pForm");

		final HelpListBean bean = new HelpListBean();
		bean.setFormId(getForward(request));
		bean.setHelpId(form.getHelpId());
		execute(request, bean);
		request.setAttribute("HelpListBean", bean);
		
		//	アクセスログ
		actionLog(request, "2002", form.getHelpId());

		// アクションフォーム
		request.setAttribute("form", form);
		forward(request, response, JSP_H003P);
	}

}

package jp.co.fj.keinavi.util;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Hex;

/**
 *
 * Rangeヘッダによる部分リクエストに対するレスポンス出力クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class RangeResponse {

    /** Rangeヘッダ */
    public static final String RANGE = "Range";

    /** HttpServletResponse */
    private final HttpServletResponse response;

    /** 出力ファイル */
    private final File file;

    /** Rangeリスト */
    private final List<Range> rangeList;

    /** Content-Type */
    private final String contentType;

    /**
     * コンストラクタです。
     *
     * @param response {@link HttpServletResponse}
     * @param range Rangeヘッダ
     * @param file 出力ファイル
     * @param contentType MIMEタイプ
     * @throws IOException IO例外
     */
    public RangeResponse(HttpServletResponse response, String range, File file, String contentType) throws IOException {
        this.response = response;
        this.file = file;
        this.contentType = contentType;
        this.rangeList = parseRange(range);
    }

    /**
     * レスポンスを出力します。
     *
     * @throws IOException IO例外
     */
    public void output() throws IOException {

        if (rangeList == null) {
            /* Rangeヘッダの解析に失敗していたら処理しない */
            return;
        }

        response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
        response.setHeader("Accept-Ranges", "bytes");

        RandomAccessFile in = null;
        try {
            in = new RandomAccessFile(file, "r");
            if (rangeList.size() == 1) {
                outputSingleRange(in, rangeList.get(0));
            } else {
                outputMultiRange(in, rangeList);
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    /**
     * Rangeヘッダを解析します。
     *
     * @param range Rangeヘッダ
     * @return 範囲リスト
     * @throws IOException IO例外
     */
    private List<Range> parseRange(String range) throws IOException {

        long length = file.length();

        if (!range.matches("^bytes=\\d*-\\d*(\\s*,\\s*\\d*-\\d*)*$")) {
            /* Rangeヘッダが不正な場合 */
            response.setHeader("Content-Range", "bytes */" + length);
            response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
            return null;
        }

        List<Range> list = new ArrayList<Range>();

        for (String part : range.substring(6).split("\\s*,\\s*")) {
            long start = sublong(part, 0, part.indexOf("-"));
            long end = sublong(part, part.indexOf("-") + 1, part.length());

            if (start == -1) {
                start = length - end;
                end = length - 1;
            } else if (end == -1 || end > length - 1) {
                end = length - 1;
            }

            if (start > end) {
                /* Rangeヘッダが不正な場合 */
                response.setHeader("Content-Range", "bytes */" + length);
                response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
                return null;
            }

            list.add(new Range(start, end, length));
        }

        return list;
    }

    /**
     * 単一の範囲レスポンスを出力します。
     *
     * @param in 入力ファイル
     * @param r {@link Range}
     * @throws IOException IO例外
     */
    private void outputSingleRange(RandomAccessFile in, Range r) throws IOException {

        response.setContentType(contentType);
        response.setHeader("Content-Range", "bytes " + r.start + "-" + r.end + "/" + r.total);
        response.setHeader("Content-Length", String.valueOf(r.length));

        copy(in, response.getOutputStream(), r.start, r.length);
    }

    /**
     * 複数の範囲レスポンスを出力します。
     *
     * @param in 入力ファイル
     * @param rangeList {@link Range}のリスト
     * @throws IOException IO例外
     */
    private void outputMultiRange(RandomAccessFile in, List<Range> rangeList) throws IOException {

        String boundary = createBoundary();

        response.setContentType("multipart/byteranges; boundary=" + boundary);

        ServletOutputStream out = response.getOutputStream();

        for (Range r : rangeList) {
            out.println();
            out.println("--" + boundary);
            out.println("Content-Type: " + contentType);
            out.println("Content-Range: bytes " + r.start + "-" + r.end + "/" + r.total);
            out.println();
            copy(in, out, r.start, r.length);
        }

        out.println();
        out.println("--" + boundary + "--");
    }

    /**
     * マルチパート応答のバウンダリを生成します。
     *
     * @return バウンダリ
     */
    private String createBoundary() {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        return new String(Hex.encodeHex(digest.digest(Long.toString(System.currentTimeMillis()).getBytes())));
    }

    /**
     * 入力ファイルの指定した範囲を読み出して、出力ストリームにコピーします。
     *
     * @param in 入力ファイル
     * @param out 出力ストリーム
     * @param start 範囲開始位置
     * @param length 範囲サイズ
     * @throws IOException IO例外
     */
    private void copy(RandomAccessFile in, OutputStream out, long start, long length) throws IOException {

        byte[] buffer = new byte[1024 * 32];
        int read;
        if (in.length() == length) {
            while ((read = in.read(buffer)) > 0) {
                out.write(buffer, 0, read);
            }
        } else {
            in.seek(start);
            long toRead = length;
            while ((read = in.read(buffer)) > 0) {
                toRead -= read;
                if (toRead > 0) {
                    out.write(buffer, 0, read);
                } else {
                    out.write(buffer, 0, (int) toRead + read);
                    break;
                }
            }
        }
    }

    /**
     * 文字列の指定部分をlongに変換します。
     *
     * @param value 文字列
     * @param beginIndex 開始位置
     * @param endIndex 終了位置
     * @return long値
     */
    private long sublong(String value, int beginIndex, int endIndex) {
        String substring = value.substring(beginIndex, endIndex);
        return (substring.length() > 0) ? Long.parseLong(substring) : -1;
    }

    /**
     * 部分リクエストの範囲情報を保持するクラス
     */
    private class Range {

        /** 範囲開始位置 */
        private final long start;

        /** 範囲終了位置 */
        private final long end;

        /** 範囲サイズ */
        private final long length;

        /** ファイルサイズ */
        private final long total;

        /**
         * コンストラクタです。
         *
         * @param start 範囲開始位置
         * @param end 範囲終了位置
         * @param total ファイルサイズ
         */
        public Range(long start, long end, long total) {
            this.start = start;
            this.end = end;
            this.length = end - start + 1;
            this.total = total;
        }

    }

}

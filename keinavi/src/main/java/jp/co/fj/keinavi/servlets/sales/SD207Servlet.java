package jp.co.fj.keinavi.servlets.sales;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.sales.SD207CancelBean;
import jp.co.fj.keinavi.beans.sales.SD207DownloadBean;
import jp.co.fj.keinavi.beans.sales.SD207ListBean;
import jp.co.fj.keinavi.data.sales.SD207Data;
import jp.co.fj.keinavi.forms.sales.SD207Form;
import jp.co.fj.keinavi.interfaces.SpecialAppliMenu;
import jp.co.fj.keinavi.interfaces.SpecialAppliStatus;
import jp.co.fj.keinavi.servlets.sales.base.BaseSpecialAppliServlet;
import jp.co.fj.keinavi.util.KNUtil;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * 特例成績データダウンロード画面のサーブレットです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD207Servlet extends BaseSpecialAppliServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = 2408864290095924577L;

	/**
	 * @see jp.co.fj.keinavi.servlets.sales.base.BaseSpecialAppliServlet#getMenu()
	 */
	protected SpecialAppliMenu getMenu() {
		return SpecialAppliMenu.DOWNLOAD;
	}

	/**
	 * JSP表示処理を行います。
	 */
	public void index(HttpServletRequest request, HttpServletResponse response,
			SD207Form form) throws Exception {

		/* 対象年度の初期値をセットする */
		if (StringUtils.isEmpty(form.getExamYear())) {
			form.setExamYear(KNUtil.getCurrentYear());
		}

		/* 絞込み条件の初期値をセットする */
		if (StringUtils.isEmpty(form.getCondition())) {
			form.setCondition("1");
		}

		/* ソートキーの初期値をセットする */
		if (StringUtils.isEmpty(form.getSortKey())) {
			form.setSortKey("4");
		}

		/* 現年度をリクエストスコープにセットする */
		request.setAttribute("currentYear", KNUtil.getCurrentYear());

		/* 申請リストをリクエストスコープにセットする */
		request.setAttribute(
				"results",
				findResults(request, form.getExamYear(), form.getCondition(),
						form.getSortKey()));

		forward2Jsp(request, response);
	}

	/**
	 * 申請リストを取得します。
	 */
	private List findResults(HttpServletRequest request, String exaYear,
			String condition, String sortKey) throws Exception {

		SD207ListBean bean = new SD207ListBean(exaYear, condition, sortKey,
				getSectorCdArray(request));
		executeBean(request, bean);

		return bean.getResults();
	}

	/**
	 * 取消処理を行います。
	 */
	public void cancel(HttpServletRequest request,
			HttpServletResponse response, SD207Form form) throws Exception {

		SD207Data data = findData(request, form, form.getCancelAppId());

		/* 承認待ちであるかチェックする */
		if (SpecialAppliStatus.ACCEPTED.code.equals(data.getStatusCd())) {
			throw new RuntimeException("指定された申請データは取消できません。" + data.getAppId());
		}

		executeBean(request, new SD207CancelBean(data.getAppId()));

		index(request, response, form);
	}

	/**
	 * DBから指定した申請IDのデータを取得します。
	 */
	private SD207Data findData(HttpServletRequest request, SD207Form form,
			String appId) throws Exception {

		/* 全ての申請データを取得する */
		List results = findResults(request, form.getExamYear(), "2",
				form.getSortKey());

		for (Iterator ite = results.iterator(); ite.hasNext();) {
			SD207Data data = (SD207Data) ite.next();
			if (data.getAppId().equals(appId)) {
				return data;
			}
		}

		throw new RuntimeException("指定された申請データは利用できません。" + appId);
	}

	/**
	 * ダウンロード処理を行います。
	 */
	public void download(HttpServletRequest request,
			HttpServletResponse response, SD207Form form) throws Exception {

		SD207Data data = findData(request, form, form.getAppId());

		/* 承認済かつ開放日を過ぎているかチェックする */
		if (!SpecialAppliStatus.ACCEPTED.code.equals(data.getStatusCd())
				|| data.getOpenDate() != null) {
			throw new RuntimeException("指定された申請データはダウンロードできません。"
					+ data.getAppId());
		}

		SD207DownloadBean bean = new SD207DownloadBean(data, response);
		executeBean(request, bean);

		if (bean.getErrorCount() > 0) {
			form.setErrorMessage(bean.getErrorMessage());
			index(request, response, form);
		}
	}

}

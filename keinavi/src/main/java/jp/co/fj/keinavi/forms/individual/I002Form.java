/*
 * 作成日: 2004/07/12
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * @author Administrator
 *
 * 2005.4.26 	K.Kondo 	[1]画面スクロール座標の追加
 */
public class I002Form extends ActionForm{

	private String targetGrade;
	private String targetClass;
	private String targetSubject;
	
	private String[] personId;
	
	private String targetExamYear;
	private String targetExamCode;
	private String targetExamName;
	private String targetExamNamePrev;
	private String targetExamCodePrev;
	private String scrollX;//[1] add サブミット前、表示座標Ｘ
	private String scrollY;//[1] add サブミット前、表示座標Ｙ
	
	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ
		
	}

	/**
	 * @return
	 */
	public String getTargetClass() {
		return targetClass;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * @return
	 */
	public String getTargetExamCodePrev() {
		return targetExamCodePrev;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @return
	 */
	public String getTargetGrade() {
		return targetGrade;
	}

	/**
	 * @return
	 */
	public String getTargetSubject() {
		return targetSubject;
	}

	/**
	 * @param string
	 */
	public void setTargetClass(String string) {
		targetClass = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCodePrev(String string) {
		targetExamCodePrev = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * @param string
	 */
	public void setTargetGrade(String string) {
		targetGrade = string;
	}

	/**
	 * @param string
	 */
	public void setTargetSubject(String string) {
		targetSubject = string;
	}

	/**
	 * @return
	 */
	public String[] getPersonId() {
		return personId;
	}

	/**
	 * @return
	 */
	public String getTargetExamName() {
		return targetExamName;
	}

	/**
	 * @param strings
	 */
	public void setPersonId(String[] strings) {
		personId = strings;
	}

	/**
	 * @param string
	 */
	public void setTargetExamName(String string) {
		targetExamName = string;
	}

	/**
	 * @return
	 */
	public String getTargetExamNamePrev() {
		return targetExamNamePrev;
	}

	/**
	 * @param string
	 */
	public void setTargetExamNamePrev(String string) {
		targetExamNamePrev = string;
	}
	
	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[1] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[1] add
	}

}

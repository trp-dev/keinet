package jp.co.fj.keinavi.excel.data.cls;

/**
 * クラス成績概況−順位表個人データリスト
 * 作成日: 2004/08/06
 * @author	A.Iwata
 * 
 * 2009.11.24   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応 
 */
public class C12JyuniPersonalListBean {
	//校内順位
	private int intJyuni = 0;
	//学年
	private String strGrade = "";
	//クラス
	private String strClass = "";
	//クラス番号
	private String strClassNo = "";
	//氏名
	private String strKanaName = "";
	//性別
	private String strSex = "";
	//得点
	private int intTokuten = 0;
	//校内偏差値
	private float floHensaHome = 0;
	//全国偏差値
	private float floHensaAll = 0;
	//学力レベル
	private String strScholarLevel = "";

	/*----------*/
	/* Get      */
	/*----------*/
	
	public float getFloHensaAll() {
		return this.floHensaAll;
	}
	public float getFloHensaHome() {
		return this.floHensaHome;
	}
	public int getIntJyuni() {
		return this.intJyuni;
	}
	public int getIntTokuten() {
		return this.intTokuten;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrClassNo() {
		return this.strClassNo;
	}
	public String getStrGrade() {
		return this.strGrade;
	}
	public String getStrKanaName() {
		return this.strKanaName;
	}
	public String getStrSex() {
		return this.strSex;
	}
	public String getStrScholarLevel() {
		return strScholarLevel;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	
	public void setFloHensaAll(float floHensaAll) {
		this.floHensaAll = floHensaAll;
	}
	public void setFloHensaHome(float floHensaHome) {
		this.floHensaHome = floHensaHome;
	}
	public void setIntJyuni(int intJyuni) {
		this.intJyuni = intJyuni;
	}
	public void setIntTokuten(int intTokuten) {
		this.intTokuten = intTokuten;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrClassNo(String strClassNo) {
		this.strClassNo = strClassNo;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}
	public void setStrKanaName(String strKanaName) {
		this.strKanaName = strKanaName;
	}
	public void setStrSex(String strSex) {
		this.strSex = strSex;
	}
	public void setStrScholarLevel(String string) {
		this.strScholarLevel = string;
	}

}
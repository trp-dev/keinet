package jp.co.fj.keinavi.beans.sheet.business;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.sheet.ExtB41JuniListBean;
import jp.co.fj.keinavi.data.sheet.ExtB41SeisekiListBean;
import jp.co.fj.keinavi.data.sheet.SheetSchoolData;
import jp.co.fj.keinavi.excel.business.B41;
import jp.co.fj.keinavi.excel.data.business.B41Item;
import jp.co.fj.keinavi.excel.data.business.B41ListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 高校成績分析 - 高校間比較 - 成績概況
 * 
 * 
 * 2004.09.08	[新規作成]
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * 2005.04.20	Yoshimoto KAWAI - Totec
 * 				新テスト対応
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class B41SheetBean extends AbstractSheetBean {

	// データクラス
	private final B41Item data = new B41Item();

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		final String[] code = getSubjectCDArray(); // 型・科目設定値
		final List compSchoolList = getCompSchoolList(); // 比較対象高校
		final String studentDiv = getMinStudentDiv();
		
		final String lastYear
			= String.valueOf(Integer.parseInt(exam.getExamYear()) - 1); // 前年度

		// 新テストかどうか
		final boolean isNewExam = KNUtil.isNewExam(exam);
		
		// データを詰める
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntHyouFlg(getIntFlag(IProfileItem.CHART)); // 表フラグ
		data.setIntRankFlg(getIntFlag(IProfileItem.IND_SCORE_RANK)); // 型･科目別成績順位フラグ
		data.setIntPastYearFlg(getIntFlag(IProfileItem.PREV_DISP)); // 過年度表示比較フラグ
		data.setIntHeikinAllFlg(getIntFlag(IProfileItem.IND_TOTAL_AVG)); // 全体平均表示フラグ
		data.setIntHeikinGenFlg(getIntFlag(IProfileItem.IND_ACTIVE_AVG)); // 現役平均表示フラグ
		data.setIntHeikinSotuFlg(getIntFlag(IProfileItem.IND_GRAD_AVG)); // 高卒平均表示フラグ
		data.setIntBestFlg(getIntFlag(IProfileItem.IND_UPPER_MARK)); // 上位校の＊表示フラグ
		data.setIntSecuFlg(getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(getOutputType()); // 出力種別フラグ

		// 表示高校数
		switch (super.getIntFlag(IProfileItem.IND_SCHOOL_NUM)) {
			case 1: data.setIntGakkouNum(50); break;
			case 2: data.setIntGakkouNum(100); break;
			case 3: data.setIntGakkouNum(150); break;
			case 4: data.setIntGakkouNum(200); break;
			case 5: data.setIntGakkouNum(250); break;
			case 6: data.setIntGakkouNum(300); break;
			default: throw new IllegalArgumentException("表示高校数の設定値が不正です。");
		}

		// ワークテーブルのセットアップ
		insertIntoSubCDTrans(new ExamData[]{exam});

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		try {
			List c1 = new LinkedList(); // 型の入れ物
			List c2 = new LinkedList(); // 科目の入れ物
						
			// データリストの取得
			{
				Query query = QueryLoader.getInstance().load("s22_1");
				query.setStringArray(1, code); // 型・科目コード

				ps1 = conn.prepareStatement(query.toString());
				ps1.setString(1, exam.getExamYear()); // 模試年度
				ps1.setString(2, exam.getExamCD()); // 模試コード
			}

			// 成績表データリスト（全国・高卒・現役）
			ps2 = conn.prepareStatement(QueryLoader.getInstance().load("b41_1").toString());
			ps2.setString(3, lastYear); // 模試年度（前年度）
			ps2.setString(4, exam.getExamCD()); // 模試コード（今年度）

			// 成績表・順位表データリスト（全国）
			ps3 = conn.prepareStatement(QueryLoader.getInstance().load("b41_2_1").toString());
			ps3.setString(2, studentDiv); // 現役高卒区分
			ps3.setString(3, lastYear); // 模試年度（前年度）
			ps3.setString(4, exam.getExamCD()); // 模試コード（今年度）
			ps3.setString(6, studentDiv); // 現役高卒区分

			// 成績表・順位表データリスト（県）
			ps4 = conn.prepareStatement(QueryLoader.getInstance().load("b41_2_2").toString());
			ps4.setString(3, lastYear); // 模試年度（前年度）
			ps4.setString(4, exam.getExamCD()); // 模試コード（今年度）

			// 成績表・順位表データリスト（高校）
			{
				Query query = QueryLoader.getInstance().load("b41_2_3");
				super.outDate2InDate(query); // データ開放日書き換え			
			
				ps5 = conn.prepareStatement(query.toString());
				ps5.setString(3, lastYear); // 模試年度（前年度）
				ps5.setString(4, exam.getExamCD()); // 模試コード（今年度）
			}

			rs1 = ps1.executeQuery();
			
			while (rs1.next()) {
				B41ListBean bean = new B41ListBean();
				bean.setStrKmkCd(rs1.getString(1));
				bean.setStrKmkmei(rs1.getString(2));
				bean.setStrHaiten(rs1.getString(3));
				
				if (rs1.getInt(1) >= 7000) c1.add(bean); // 型
				else c2.add(bean); // 科目

				// 型・科目コード
				ps2.setString(1, bean.getStrKmkCd());
				ps2.setString(5, bean.getStrKmkCd());
				ps3.setString(1, bean.getStrKmkCd());
				ps3.setString(5, bean.getStrKmkCd());
				ps4.setString(2, bean.getStrKmkCd());
				ps4.setString(5, bean.getStrKmkCd());
				ps5.setString(2, bean.getStrKmkCd());
				ps5.setString(5, bean.getStrKmkCd());

				//「全体平均表示」が”あり”の場合
				if (data.getIntHeikinAllFlg() == 1) {
					// 現役高卒区分
					ps2.setString(2, "0");
					ps2.setString(6, "0");
					
					rs2 = ps2.executeQuery();
					
					if (rs2.next()) {
						ExtB41SeisekiListBean s = new ExtB41SeisekiListBean();
						s.setStrNendoNow(exam.getExamYear());
						s.setStrGakkomei("全国");
						s.setIntNinzuNow(rs2.getInt(1));
						s.setStrNendoPast(lastYear);
						s.setSchoolCD("99999");
						
						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							s.setFloHeikinNow(-999);
							s.setFloStdHensaNow(rs2.getFloat(2));
						} else {
							s.setFloHeikinNow(rs2.getFloat(2));
							s.setFloStdHensaNow(rs2.getFloat(3));
						}

						bean.getB41SeisekiList().add(s);
						
						ExtB41JuniListBean j = new ExtB41JuniListBean(isNewExam);
						j.setStrNendoNow(exam.getExamYear());
						j.setStrGakkomei("全国");
						j.setIntNinzuNow(rs2.getInt(1));
						j.setStrNendoPast(lastYear);
						j.setSchoolCD("99999");

						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							j.setFloHeikinNow(-999);
							j.setFloStdHensaNow(rs2.getFloat(2));
						} else {
							j.setFloHeikinNow(rs2.getFloat(2));
							j.setFloStdHensaNow(rs2.getFloat(3));
						}

						bean.getB41JuniList().add(j);
						
						// 「前年度まで」なら前年度の値を入れる						
						if (data.getIntPastYearFlg() == 2){
							s.setIntNinzuPast(rs2.getInt(4));
							j.setIntNinzuPast(rs2.getInt(4));

							// 新テストなら平均点→無効値／平均偏差値→平均点
							if (isNewExam) {
								s.setFloHeikinPast(-999);
								s.setFloStdHensaPast(rs2.getFloat(5));
								j.setFloHeikinPast(-999);
								j.setFloStdHensaPast(rs2.getFloat(5));
							} else {
								s.setFloHeikinPast(rs2.getFloat(5));
								s.setFloStdHensaPast(rs2.getFloat(6));
								j.setFloHeikinPast(rs2.getFloat(5));
								j.setFloStdHensaPast(rs2.getFloat(6));
							}

						} else {
							s.setIntNinzuPast(-999);
							s.setFloHeikinPast(-999);
							s.setFloStdHensaPast(-999);
							j.setIntNinzuPast(-999);
							j.setFloHeikinPast(-999);
							j.setFloStdHensaPast(-999);
						}
					}
					
					rs2.close();				
				}

				//「現役平均表示」が”あり”の場合
				if (data.getIntHeikinGenFlg() == 1) {
					// 現役高卒区分
					ps2.setString(2, "1");
					ps2.setString(6, "1");
					
					rs2 = ps2.executeQuery();
					
					if (rs2.next()) {
						ExtB41SeisekiListBean s = new ExtB41SeisekiListBean();
						s.setStrNendoNow(exam.getExamYear());
						s.setStrGakkomei("現役生平均");
						s.setIntNinzuNow(rs2.getInt(1));
						s.setStrNendoPast(lastYear);
						s.setSchoolCD("98999");
						
						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							s.setFloHeikinNow(-999);
							s.setFloStdHensaNow(rs2.getFloat(2));
						} else {
							s.setFloHeikinNow(rs2.getFloat(2));
							s.setFloStdHensaNow(rs2.getFloat(3));
						}

						bean.getB41SeisekiList().add(s);
						
						ExtB41JuniListBean j = new ExtB41JuniListBean(isNewExam);
						j.setStrNendoNow(exam.getExamYear());
						j.setStrGakkomei("現役生平均");
						j.setIntNinzuNow(rs2.getInt(1));
						j.setStrNendoPast(lastYear);
						j.setSchoolCD("99999");

						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							j.setFloHeikinNow(-999);
							j.setFloStdHensaNow(rs2.getFloat(2));
						} else {
							j.setFloHeikinNow(rs2.getFloat(2));
							j.setFloStdHensaNow(rs2.getFloat(3));
						}

						bean.getB41JuniList().add(j);
						
						// 「前年度まで」なら前年度の値を入れる						
						if (data.getIntPastYearFlg() == 2){
							s.setIntNinzuPast(rs2.getInt(4));
							j.setIntNinzuPast(rs2.getInt(4));

							// 新テストなら平均点→無効値／平均偏差値→平均点
							if (isNewExam) {
								s.setFloHeikinPast(-999);
								s.setFloStdHensaPast(rs2.getFloat(5));
								j.setFloHeikinPast(-999);
								j.setFloStdHensaPast(rs2.getFloat(5));
							} else {
								s.setFloHeikinPast(rs2.getFloat(5));
								s.setFloStdHensaPast(rs2.getFloat(6));
								j.setFloHeikinPast(rs2.getFloat(5));
								j.setFloStdHensaPast(rs2.getFloat(6));
							}

						} else {
							s.setIntNinzuPast(-999);
							s.setFloHeikinPast(-999);
							s.setFloStdHensaPast(-999);
							j.setIntNinzuPast(-999);
							j.setFloHeikinPast(-999);
							j.setFloStdHensaPast(-999);
						}
					}
					
					rs2.close();				
				}

				//「高卒平均表示」が”あり”の場合
				if (data.getIntHeikinSotuFlg() == 1) {
					// 現役高卒区分
					ps2.setString(2, "2");
					ps2.setString(6, "2");
					
					rs2 = ps2.executeQuery();
					
					if (rs2.next()) {
						ExtB41SeisekiListBean s = new ExtB41SeisekiListBean();
						s.setStrNendoNow(exam.getExamYear());
						s.setStrGakkomei("高卒生平均");
						s.setIntNinzuNow(rs2.getInt(1));
						s.setStrNendoPast(lastYear);
						s.setSchoolCD("97999");

						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							s.setFloHeikinNow(-999);
							s.setFloStdHensaNow(rs2.getFloat(2));
						} else {
							s.setFloHeikinNow(rs2.getFloat(2));
							s.setFloStdHensaNow(rs2.getFloat(3));
						}

						bean.getB41SeisekiList().add(s);
						
						ExtB41JuniListBean j = new ExtB41JuniListBean(isNewExam);
						j.setStrNendoNow(exam.getExamYear());
						j.setStrGakkomei("高卒生平均");
						j.setIntNinzuNow(rs2.getInt(1));
						j.setStrNendoPast(lastYear);
						j.setSchoolCD("99999");

						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							j.setFloHeikinNow(-999);
							j.setFloStdHensaNow(rs2.getFloat(2));
						} else {
							j.setFloHeikinNow(rs2.getFloat(2));
							j.setFloStdHensaNow(rs2.getFloat(3));
						}

						bean.getB41JuniList().add(j);
						
						// 「前年度まで」なら前年度の値を入れる						
						if (data.getIntPastYearFlg() == 2){
							s.setIntNinzuPast(rs2.getInt(4));
							j.setIntNinzuPast(rs2.getInt(4));

							// 新テストなら平均点→無効値／平均偏差値→平均点
							if (isNewExam) {
								s.setFloHeikinPast(-999);
								s.setFloStdHensaPast(rs2.getFloat(5));
								j.setFloHeikinPast(-999);
								j.setFloStdHensaPast(rs2.getFloat(5));
							} else {
								s.setFloHeikinPast(rs2.getFloat(5));
								s.setFloStdHensaPast(rs2.getFloat(6));
								j.setFloHeikinPast(rs2.getFloat(5));
								j.setFloStdHensaPast(rs2.getFloat(6));
							}
							
						} else {
							s.setIntNinzuPast(-999);
							s.setFloHeikinPast(-999);
							s.setFloStdHensaPast(-999);
							j.setIntNinzuPast(-999);
							j.setFloHeikinPast(-999);
							j.setFloStdHensaPast(-999);
						}
					}
					
					rs2.close();
				}

				// 学校分繰り返す
				Iterator ite = compSchoolList.iterator();
				while (ite.hasNext()) {
					SheetSchoolData school = (SheetSchoolData) ite.next();

					switch (school.getSchoolTypeCode()) {
						// 全国
						case 1:
							rs3 = ps3.executeQuery();
							break;
						// 県
						case 2:
							ps4.setString(1, school.getPrefCD());
							rs3 = ps4.executeQuery();
							break;
						// 高校
						case 3:
							ps5.setString(1, school.getSchoolCD());
							rs3 = ps5.executeQuery();
							break;
					}

					while (rs3.next()) {
						ExtB41SeisekiListBean s = new ExtB41SeisekiListBean();
						s.setStrNendoNow(exam.getExamYear());
						s.setStrGakkomei(rs3.getString(1));
						s.setIntNinzuNow(rs3.getInt(2));
						s.setStrNendoPast(lastYear);
						s.setSchoolCD(school.getSchoolCD());

						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							s.setFloHeikinNow(-999);
							s.setFloStdHensaNow(rs3.getFloat(3));
						} else {
							s.setFloHeikinNow(rs3.getFloat(3));
							s.setFloStdHensaNow(rs3.getFloat(4));
						}

						bean.getB41SeisekiList().add(s);
	
						ExtB41JuniListBean j = new ExtB41JuniListBean(isNewExam);
						j.setStrNendoNow(exam.getExamYear());
						j.setStrGakkomei(rs3.getString(1));
						j.setIntNinzuNow(rs3.getInt(2));
						j.setStrNendoPast(lastYear);
						j.setSchoolCD(school.getSchoolCD());

						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							j.setFloHeikinNow(-999);
							j.setFloStdHensaNow(rs3.getFloat(3));
						} else {
							j.setFloHeikinNow(rs3.getFloat(3));
							j.setFloStdHensaNow(rs3.getFloat(4));
						}

						bean.getB41JuniList().add(j);					

						// 「前年度まで」なら前年度の値を入れる						
						if (data.getIntPastYearFlg() == 2){
							s.setIntNinzuPast(rs3.getInt(5));
							j.setIntNinzuPast(rs3.getInt(5));

							// 新テストなら平均点→無効値／平均偏差値→平均点
							if (isNewExam) {
								s.setFloHeikinPast(-999);
								s.setFloStdHensaPast(rs3.getFloat(6));
								j.setFloHeikinPast(-999);
								j.setFloStdHensaPast(rs3.getFloat(6));
							} else {
								s.setFloHeikinPast(rs3.getFloat(6));
								s.setFloStdHensaPast(rs3.getFloat(7));
								j.setFloHeikinPast(rs3.getFloat(6));
								j.setFloStdHensaPast(rs3.getFloat(7));
							}

						} else {
							s.setIntNinzuPast(-999);
							s.setFloHeikinPast(-999);
							s.setFloStdHensaPast(-999);
							j.setIntNinzuPast(-999);
							j.setFloHeikinPast(-999);
							j.setFloStdHensaPast(-999);
						}
					}
					rs3.close();
				}

				// 順位表はソートする
				Collections.sort(bean.getB41JuniList());
				// 表示高校数の絞り込み
				if (bean.getB41JuniList().size() > data.getIntGakkouNum()) {
					final List subList = bean.getB41JuniList().subList(
							0, data.getIntGakkouNum());
					bean.setB41JuniList(new ArrayList(subList));
				}
			}

			// 型・科目の順番に詰める
			data.getB41List().addAll(c1);
			data.getB41List().addAll(c2);
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(rs3);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(ps3);
			DbUtils.closeQuietly(ps4);
			DbUtils.closeQuietly(ps5);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("B41_01");
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_OTHER_SCORE;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new B41().b41(data, outfileList, getAction(), sessionKey, sheetLog);
	}

}

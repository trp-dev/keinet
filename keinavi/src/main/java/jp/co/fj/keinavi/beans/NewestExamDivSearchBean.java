package jp.co.fj.keinavi.beans;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 大学マスタから最新の年度と区分を取得するBean
 * 
 * 2005/10/28	[新規作成]
 * 
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class NewestExamDivSearchBean extends DefaultBean {

    /**
     * 最新の模試年度
     */
    private String examYear;
    
    /**
     * 最新の模試年度の最新の模試区分
     */
    private String examDiv;
    
    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(
            		QueryLoader.getInstance().load("w10").toString());
            rs = ps.executeQuery();
            if (rs.next()) {
                this.examYear = rs.getString(1);
                this.examDiv = rs.getString(2);
            } else {
                throw new Exception(
                		"最新の模試年度と区分が取得できませんでした。");
            }

        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }
    
    /**
     * @return examDiv を戻します。
     */
    public String getExamDiv() {
        return examDiv;
    }
    /**
     * @return examYear を戻します。
     */
    public String getExamYear() {
        return examYear;
    }
    
}

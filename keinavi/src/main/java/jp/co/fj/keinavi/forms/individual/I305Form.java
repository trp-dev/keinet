/*
 * 作成日: 2004/08/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class I305Form extends IOnJudgeForm{

	private String targetPersonId;
	
	//個人成績分析共通-印刷対象生徒（判定対象生徒)
	private String printStudent;
	
	//セキュリティスタンプ
	private String printStamp;
	
	//出力フォーマット
	private String textFormat[];
	
	//印刷ボタン押下時パラメータ
	private String printStatus;
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.individual.ICommonForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ
		
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @return
	 */
	public String getPrintStamp() {
		return printStamp;
	}

	/**
	 * @param string
	 */
	public void setPrintStamp(String string) {
		printStamp = string;
	}

	/**
	 * @return
	 */
	public String getPrintStudent() {
		return printStudent;
	}

	/**
	 * @param string
	 */
	public void setPrintStudent(String string) {
		printStudent = string;
	}

	/**
	 * @return
	 */
	public String[] getTextFormat() {
		return textFormat;
	}

	/**
	 * @param strings
	 */
	public void setTextFormat(String[] strings) {
		textFormat = strings;
	}

	/**
	 * @return
	 */
	public String getPrintStatus() {
		return printStatus;
	}

	/**
	 * @param string
	 */
	public void setPrintStatus(String string) {
		printStatus = string;
	}

}

package jp.co.fj.keinavi.auth;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.util.AesEncryptor;

import org.apache.commons.codec.net.URLCodec;

/**
 *
 * Kei-Net連携用Cookieマネージャ
 * 
 * 2007.07.31	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class KnetCookieManager {

	// KeiNaviFlag（定数）
	private static final String KEINAVI_FLAG = "KeiNavi";
	
	// 暗号化処理クラス
	protected final AesEncryptor ec;
	// URLCodec
	private final URLCodec uc = new URLCodec();
	
	/**
	 * コンストラクタ
	 * 
	 * ※デフォルトの秘密鍵を持つ
	 */
	public KnetCookieManager() {
		ec = new AesEncryptor();
	}
	
	/**
	 * コンストラクタ
	 * 
	 * @param key 指定する秘密鍵
	 */
	public KnetCookieManager(final String key) {
		ec = new AesEncryptor(key);
	}
	
	public void create(final HttpServletResponse response,
			final LoginSession login) throws Exception {
		
		// IV
		response.addCookie(createCookie("iv-navi", encode(ec.getIv())));
		// 学校会員ID
		response.addCookie(createCookie("userId", toValue(login.getLoginID())));
		// 学校名
		response.addCookie(createCookie("userName", toValue(login.getUserName())));
		// KeiNaviFlag
		response.addCookie(createCookie("KeiNaviFlag", toValue(KEINAVI_FLAG)));
		// KeiNaviType
		response.addCookie(createCookie("KeiNaviType",
				login.isPact() ? toValue("1") :  toValue("2")));
	}

	// Cookieオブジェクトを作る
	private Cookie createCookie(final String key, final String value) {
		
		final Cookie cookie = new Cookie(key, value);
		cookie.setDomain(".keinet.ne.jp");
		cookie.setPath("/");
		return cookie;
	}
	
	// Cookieの値にする
	protected String toValue(final String value) {
		return encode(ec.encrypt(value));
	}
	
	// URLエンコード処理
	protected String encode(final byte[] value) {
		return new String(uc.encode(value));
	}

}

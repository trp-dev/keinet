package jp.co.fj.keinavi.beans.sheet.sales;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import jp.co.fj.keinavi.beans.sheet.common.AbstractExtSheetBean;
import jp.co.fj.keinavi.beans.sheet.excel.sales.SD01_01ExcelCreator;
import jp.co.fj.keinavi.beans.sheet.sales.data.SD01_01Data;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * パスワード通知書のSheetBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SD206SheetBean extends AbstractExtSheetBean {

	/** serialVersionUID */
	private static final long serialVersionUID = -3968198899600489196L;

	/** 帳票データ */
	private final SD01_01Data data = new SD01_01Data();

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		/* セッションから申請IDを取得する */
		String appId = (String) session.getAttribute("SD206Form.appId");
		session.removeAttribute("SD206Form.appId");

		setUpData(appId);
	}

	/**
	 * データクラスをセットアップします。
	 */
	private void setUpData(String appId) throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance()
					.load("sd206_sheet").toString());
			ps.setString(1, appId);
			rs = ps.executeQuery();
			if (rs.next()) {
				data.setPostCode(rs.getString(1));
				data.setAddress(rs.getString(2));
				data.setBundleCd(rs.getString(3));
				data.setBundleName(rs.getString(4));
				data.setTeacherName(rs.getString(5));
				data.setExamName(rs.getString(6));
				data.setPassword(rs.getString(7));
				// 2019/09/09 QQ)Tanioka 帳票出力名修正 ADD START
				data.setExamCd(rs.getString(8));
				// 2019/09/09 QQ)Tanioka 帳票出力名修正 ADD END
			} else {
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
			    //throw new Exception("申請データが見つかりませんでした。" + appId);
			    throw new IllegalStateException("申請データが見つかりませんでした。" + appId);
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.AbstractExtSheetBean#create()
	 */
	protected void create() throws Exception {

		new SD01_01ExcelCreator(data, sessionKey, outfileList, getAction())
				.execute();
		sheetLog.add("SD01_01");
	}

}

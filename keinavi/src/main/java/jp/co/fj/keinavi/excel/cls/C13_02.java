/**
 * 作成日: 2004/09/
 * @author A.Hasegawa
 * 
 * クラス成績分析−クラス成績概況（個人成績推移）−型・科目別比較（2回比較） 出力処理
 * 
 * 2009.11.24   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応 
 */
package jp.co.fj.keinavi.excel.cls;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C13Item;
import jp.co.fj.keinavi.excel.data.cls.C13KmkDataListBean;
import jp.co.fj.keinavi.excel.data.cls.C13KmkListBean;
import jp.co.fj.keinavi.excel.data.cls.C13KmkSeisekiListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class C13_02 {
	
	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow		= 80;			//MAX行数

	private boolean bolBookClsFlg = false;
	private boolean bolBookCngFlg = true;			//改ファイルフラグ	
	private boolean bolSheetCngFlg = true;			//改シートフラグ
	private int intDataStartRow = 12;					//データセット開始行
	
	final private String masterfile0 = "C13_02" ;
	final private String masterfile1 = "C13_02" ;
	private String masterfile = "" ;

	/*
	 * 	Excel編集メイン
	 * 		C13Item c13Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int c13_02EditExcel(C13Item c13Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
			
			
		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFCell	workCell		= null;
		HSSFRow		workRow			= null;
		
		//テンプレートの決定
		if (c13Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intRow = 0;
		int intRowBase = 0;
		
		int intCount = 0;
		
		String strKmkCd = "";
		
		float floHensaAllNow = 0;		//今回模試の偏差値
		float floHensaAllLast = 0;		//前回模試の偏差値
		float floHensaDiff = 0;		//前回差
		String strKeiko = "";			//傾向
		float floPitch_sogo= 0 ;		//変動幅
		
		int intBookCngCount = 0;
		
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("C13_02","C13_02帳票作成開始","");

		// 基本ファイルを読込む
		C13KmkDataListBean c13KmkDataListBean = new C13KmkDataListBean();
		
		try{
			ArrayList c13KmkDataList = c13Item.getC13KmkDataList();
			ListIterator itr = c13KmkDataList.listIterator();
			
			while( itr.hasNext() ){
				
				c13KmkDataListBean = (C13KmkDataListBean)itr.next();
				ArrayList c13KmkList = c13KmkDataListBean.getC13KmkList();
				
				C13KmkListBean c13KmkListBean = new C13KmkListBean();
				Iterator itrKmk = c13KmkList.iterator();
				
				while(itrKmk.hasNext()){
					c13KmkListBean = (C13KmkListBean) itrKmk.next();

					if (strKmkCd != c13KmkListBean.getStrKmkCd() ){
						strKmkCd = c13KmkListBean.getStrKmkCd();
						bolSheetCngFlg = true;
						intCount = 0;
						if (intMaxSheetIndex >= intMaxSheetSr){
							bolBookCngFlg = true;
							bolBookClsFlg = true;
						}
					}

					C13KmkSeisekiListBean c13KmkSeisekiListBean = new C13KmkSeisekiListBean();
					Iterator itrSeiseki = c13KmkListBean.getC13KmkSeisekiList().iterator();
					
					if( bolSheetCngFlg == true ){
					
						if( bolBookCngFlg == true ){
						
							if(bolBookClsFlg == true ){
								boolean bolRet = false;
								// Excelファイル保存
								if(itr.hasNext() == true){
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
								}
								else{
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
								}

								if( bolRet == false ){
									return errfwrite;
								}
								intBookCngCount++;
								bolBookClsFlg = false;
							}
						
							// マスタExcel読み込み
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook.equals(null) ){
								return errfread;
							}
								
							bolBookCngFlg = false;
							intMaxSheetIndex = 0;
						}
					
						//シート作成
						// シートテンプレートのコピー
						workSheet = workbook.cloneSheet(0);

						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);
				
						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, c13Item.getIntSecuFlg(), 12, 15 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 12 );
						workCell.setCellValue(secuFlg);
				
						// ラベルセット
						if (c13Item.getIntShubetsuFlg() == 1){
						} else{
							workCell = cm.setCell( workSheet, workRow, workCell, 10, 8 );
							workCell.setCellValue( "全国\n偏差値" );
							workCell = cm.setCell( workSheet, workRow, workCell, 10, 14 );
							workCell.setCellValue( "全国\n偏差値" );
						}

						// 学校名セット → 2005/03/29 「学校名：」重複削除
						String strGakkoMei = "学校名　：" + cm.toString(c13Item.getStrGakkomei()) + "　学年：" + cm.toString(c13KmkDataListBean.getStrGrade()) + "　クラス名：" + cm.toString(c13KmkDataListBean.getStrClass());
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( strGakkoMei );
							
						// 今回模試セット
						if(!cm.toString(c13Item.getStrMshmeiNow()).equals("")){
							// 模試月取得(対象模試)
							String moshiNow =cm.setTaisyouMoshi( c13Item.getStrMshDateNow() );
							// 対象模試セット
							workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
							workCell.setCellValue( cm.getThisExamLabel() + "：" + cm.toString(c13Item.getStrMshmeiNow()) + moshiNow);
							// 表ヘッダ対象模試セット
							workCell = cm.setCell( workSheet, workRow, workCell, 8, 5 );
							workCell.setCellValue( c13Item.getStrMshmeiShortNow() + moshiNow);
						}else{
							// 対象模試セット
							workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
							workCell.setCellValue( cm.getThisExamLabel() + "：" );
						}
							
						// 前回模試セット
						if(!cm.toString(c13Item.getStrMshmeiLast1()).equals("")){
							// 模試月取得(過回模試１)
							String moshiLast1 =cm.setTaisyouMoshi( c13Item.getStrMshDateLast1() );
							// 対象模試セット
							workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
							workCell.setCellValue( cm.getLastExamLabel() + "：" + cm.toString(c13Item.getStrMshmeiLast1()) + moshiLast1);
							// 表ヘッダ対象模試セット
							workCell = cm.setCell( workSheet, workRow, workCell, 8, 11 );
							workCell.setCellValue( c13Item.getStrMshmeiShortLast1() + moshiLast1);
						}else{
							// 対象模試セット
							workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
							workCell.setCellValue( cm.getLastExamLabel() + "：" );
						}
					
						//表示条件
						workCell = cm.setCell( workSheet, workRow, workCell, 6, 0 );
						if(!cm.toString(c13Item.getStrKeyHensa()).equals("")){
							workCell.setCellValue( "表示条件：偏差値" + cm.toString(c13Item.getStrKeyHensa()) + "以上");
						}else{
							workCell.setCellValue( "表示条件：" );
						}
					
						//変動幅
						workCell = cm.setCell( workSheet, workRow, workCell, 7, 0 );
						if(!cm.toString(c13Item.getStrKeyPitch()).equals("")){
							if (c13Item.getIntShubetsuFlg() == 1){
								workCell.setCellValue( "変動幅　：" + cm.toString(c13Item.getStrKeyPitch()) +"点以上");
							} else{
								workCell.setCellValue( "変動幅　：" + cm.toString(c13Item.getStrKeyPitch()) +"ポイント以上");
							}
							floPitch_sogo = Float.parseFloat(c13Item.getStrKeyPitch());
						}else{
							workCell.setCellValue( "変動幅　：" );
							floPitch_sogo= 0 ;
						}
					
						//型・科目名
						if ( !cm.toString(c13KmkListBean.getStrKmkmei()).equals("") ) {
							workCell = cm.setCell( workSheet, workRow, workCell, 5, 0 );
							workCell.setCellValue( "型・科目名：" + cm.toString(c13KmkListBean.getStrKmkmei()) );
						}
					
						intMaxSheetIndex++;		//シート数カウンタインクリメント
						bolSheetCngFlg = false;
					
						intRowBase = intDataStartRow;
						intCount = 0;
					}
					
					while( itrSeiseki.hasNext() ){

						if( bolSheetCngFlg == true ){
					
							if( bolBookCngFlg == true ){
						
								if(bolBookClsFlg == true ){
									boolean bolRet = false;
									// Excelファイル保存
									if(itr.hasNext() == true){
										bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
									}
									else{
										bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
									}

									if( bolRet == false ){
										return errfwrite;
									}
									intBookCngCount++;
									bolBookClsFlg = false;
								}
						
								// マスタExcel読み込み
								workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
								if( workbook.equals(null) ){
									return errfread;
								}
								
								bolBookCngFlg = false;
								intMaxSheetIndex = 0;
							}
					
							//シート作成
							// シートテンプレートのコピー
							workSheet = workbook.cloneSheet(0);

							// ヘッダ右側に帳票作成日時を表示する
							cm.setHeader(workbook, workSheet);
				
							// セキュリティスタンプセット
							String secuFlg = cm.setSecurity( workbook, workSheet, c13Item.getIntSecuFlg(), 12, 15 );
							workCell = cm.setCell( workSheet, workRow, workCell, 0, 12 );
							workCell.setCellValue(secuFlg);
				
							// ラベルセット
							if (c13Item.getIntShubetsuFlg() == 1){
							} else{
								workCell = cm.setCell( workSheet, workRow, workCell, 10, 8 );
								workCell.setCellValue( "全国\n偏差値" );
								workCell = cm.setCell( workSheet, workRow, workCell, 10, 14 );
								workCell.setCellValue( "全国\n偏差値" );
							}

							// 学校名セット → 2005/03/29 「学校名：」重複削除
							String strGakkoMei = "学校名　：" + cm.toString(c13Item.getStrGakkomei()) + "　学年：" + cm.toString(c13KmkDataListBean.getStrGrade()) + "　クラス名：" + cm.toString(c13KmkDataListBean.getStrClass());
							workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
							workCell.setCellValue( strGakkoMei );
							
							// 今回模試セット
							if(!cm.toString(c13Item.getStrMshmeiNow()).equals("")){
								// 模試月取得(対象模試)
								String moshiNow =cm.setTaisyouMoshi( c13Item.getStrMshDateNow() );
								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
								workCell.setCellValue( cm.getThisExamLabel() + "：" + cm.toString(c13Item.getStrMshmeiNow()) + moshiNow);
								// 表ヘッダ対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 8, 5 );
								workCell.setCellValue( c13Item.getStrMshmeiShortNow() + moshiNow);
							}else{
								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
								workCell.setCellValue( cm.getThisExamLabel() + "：" );
							}
							
							// 前回模試セット
							if(!cm.toString(c13Item.getStrMshmeiLast1()).equals("")){
								// 模試月取得(過回模試１)
								String moshiLast1 =cm.setTaisyouMoshi( c13Item.getStrMshDateLast1() );
								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
								workCell.setCellValue( cm.getLastExamLabel() + "：" + cm.toString(c13Item.getStrMshmeiLast1()) + moshiLast1);
								// 表ヘッダ対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 8, 11 );
								workCell.setCellValue( c13Item.getStrMshmeiShortLast1() + moshiLast1);
							}else{
								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
								workCell.setCellValue( cm.getLastExamLabel() + "：" );
							}
					
							//表示条件
							workCell = cm.setCell( workSheet, workRow, workCell, 6, 0 );
							if(!cm.toString(c13Item.getStrKeyHensa()).equals("")){
								workCell.setCellValue( "表示条件：偏差値" + cm.toString(c13Item.getStrKeyHensa()) + "以上");
							}else{
								workCell.setCellValue( "表示条件：" );
							}
					
							//変動幅
							workCell = cm.setCell( workSheet, workRow, workCell, 7, 0 );
							if(!cm.toString(c13Item.getStrKeyPitch()).equals("")){
								if (c13Item.getIntShubetsuFlg() == 1){
									workCell.setCellValue( "変動幅　：" + cm.toString(c13Item.getStrKeyPitch()) +"点以上");
								} else{
									workCell.setCellValue( "変動幅　：" + cm.toString(c13Item.getStrKeyPitch()) +"ポイント以上");
								}
							}else{
								workCell.setCellValue( "変動幅　：" );
							}
					
							//型・科目名
							if ( !cm.toString(c13KmkListBean.getStrKmkmei()).equals("") ) {
								workCell = cm.setCell( workSheet, workRow, workCell, 5, 0 );
								workCell.setCellValue( "型・科目名：" + cm.toString(c13KmkListBean.getStrKmkmei()) );
							}
					
							intMaxSheetIndex++;		//シート数カウンタインクリメント
							bolSheetCngFlg = false;
					
							intRowBase = intDataStartRow;
							intCount = 0;
						}
				

						//データリストより各項目を取得する
						c13KmkSeisekiListBean = (C13KmkSeisekiListBean)itrSeiseki.next();

						intRow = intRowBase + intCount;
						
						floHensaAllNow = 0;
						floHensaAllLast = 0;
						if (c13KmkSeisekiListBean.getFloHensaAllNow()!=-999.0) {
							floHensaAllNow = c13KmkSeisekiListBean.getFloHensaAllNow();
						}
						if (c13KmkSeisekiListBean.getFloHensaAllLast1()!=-999.0) {
							floHensaAllLast = c13KmkSeisekiListBean.getFloHensaAllLast1();
						}

						//学年
						if (!cm.toString(c13KmkSeisekiListBean.getStrGrade()).equals("")) {
							workCell = cm.setCell( workSheet, workRow, workCell, intRow, 0);
							workCell.setCellValue( c13KmkSeisekiListBean.getStrGrade() );
						}
				
						//クラス
						if (!cm.toString(c13KmkSeisekiListBean.getStrClass()).equals("")) {
							workCell = cm.setCell( workSheet, workRow, workCell, intRow, 1);
							workCell.setCellValue( c13KmkSeisekiListBean.getStrClass() );
						}
				
						//クラス番号
						if (!cm.toString(c13KmkSeisekiListBean.getStrClassNo()).equals("")) {
							workCell = cm.setCell( workSheet, workRow, workCell, intRow, 2);
							workCell.setCellValue( c13KmkSeisekiListBean.getStrClassNo() );
						}
				
						//氏名
						if (!cm.toString(c13KmkSeisekiListBean.getStrKanaName()).equals("")) {
							workCell = cm.setCell( workSheet, workRow, workCell, intRow, 3);
							workCell.setCellValue( c13KmkSeisekiListBean.getStrKanaName() );
						}
				
						//性別
						if (!cm.toString(c13KmkSeisekiListBean.getStrSex()).equals("")) {
							int sexKbn = Integer.parseInt( c13KmkSeisekiListBean.getStrSex() );
							String strSex = "";
							if( sexKbn == 1 ){
								strSex = "男";
							}else if( sexKbn == 2 ){
								strSex = "女";
							}else if( sexKbn == 9 ){
								strSex = "不";
							}
							workCell = cm.setCell( workSheet, workRow, workCell, intRow, 4 );
							workCell.setCellValue( strSex );
						}

						//今回模試
						if(!cm.toString(c13Item.getStrMshmeiNow()).equals("")){
							//型・科目名
							if (!cm.toString(c13KmkListBean.getStrKmkmei()).equals("")) {
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 5);
								workCell.setCellValue( c13KmkListBean.getStrKmkmei() );
							}
							
							//配点
							if (!cm.toString(c13KmkSeisekiListBean.getStrHaitenNow()).equals("")) {
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 6);
								workCell.setCellValue( Integer.parseInt(c13KmkSeisekiListBean.getStrHaitenNow()) );
							}
							
							//得点
							if (c13KmkSeisekiListBean.getIntTokutenNow()!=-999) {
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 7);
								workCell.setCellValue( c13KmkSeisekiListBean.getIntTokutenNow() );
							}
	
							//全国偏差値
							if (c13KmkSeisekiListBean.getFloHensaAllNow()!=-999.0) {
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 8);
								workCell.setCellValue( c13KmkSeisekiListBean.getFloHensaAllNow() );
							}
							
							//学力レベル
							if (!"".equals(c13KmkSeisekiListBean.getStrScholarLevelNow())) {
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 9);
								workCell.setCellValue( c13KmkSeisekiListBean.getStrScholarLevelNow() );
							}
	
							//傾向 2004.11.12 変動幅でﾁｪｯｸする
							floHensaDiff = 0;
							strKeiko = "";

							if (floHensaAllNow!=0) {
								if(floHensaAllLast!=0){
									floHensaDiff = floHensaAllNow - floHensaAllLast;
									if(floHensaDiff >= floPitch_sogo) {
										strKeiko = "△";
									}else if(floHensaDiff <= floPitch_sogo * -1) {
										strKeiko = "▼";
									}
								}else{
										strKeiko = "−";
								}
							}

//							if (floHensaAllNow!=0) {
//								if(floHensaAllLast!=0){
//									floHensaDiff = floHensaAllNow - floHensaAllLast;
//									if(floHensaDiff > 0) {
//										strKeiko = "△";
//									}else if(floHensaDiff < 0) {
//										strKeiko = "▼";
//									}
//								}else{
//										strKeiko = "−";
//								}
//							}

							workCell = cm.setCell( workSheet, workRow, workCell, intRow, 10);
							workCell.setCellValue( strKeiko );
						}
						//前回模試
						if(!cm.toString(c13Item.getStrMshmeiLast1()).equals("")){
							//型・科目名
							if (!cm.toString(c13KmkListBean.getStrKmkmei()).equals("")) {
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 11);
								workCell.setCellValue( c13KmkListBean.getStrKmkmei() );
							}
							
							//配点
							if (!cm.toString(c13KmkSeisekiListBean.getStrHaitenLast1()).equals("")) {
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 12);
								workCell.setCellValue( Integer.parseInt(c13KmkSeisekiListBean.getStrHaitenLast1()) );
							}
							
							//得点
							if (c13KmkSeisekiListBean.getIntTokutenLast1()!=-999) {
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 13);
								workCell.setCellValue( c13KmkSeisekiListBean.getIntTokutenLast1() );
							}
	
							//全国偏差値
							if (c13KmkSeisekiListBean.getFloHensaAllLast1()!=-999.0) {
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 14);
								workCell.setCellValue( c13KmkSeisekiListBean.getFloHensaAllLast1() );
							}
							
							//学力レベル
							if (!"".equals(c13KmkSeisekiListBean.getStrScholarLevelNow())) {
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 15);
								workCell.setCellValue( c13KmkSeisekiListBean.getStrScholarLevelLast1() );
							}
							
						}
						intCount++ ;
						
						if (intCount >= intMaxRow){
							bolSheetCngFlg = true;
							intCount = 0;
							if (intMaxSheetIndex >= intMaxSheetSr){
								bolBookCngFlg = true;
								bolBookClsFlg = true;
							}
						}

					}
				}
				
				
				bolSheetCngFlg = true;
				intCount = 0;
				if (intMaxSheetIndex >= intMaxSheetSr){
					bolBookCngFlg = true;
					bolBookClsFlg = true;
				}
				
				
			}
			// Excelファイル保存
			boolean bolRet = false;

			if(intBookCngCount > 0 || itr.hasNext() == true){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}
				
			if( bolRet == false ){
				return errfwrite;
			}
			
		}
		catch( Exception e ){
			log.Err("99C13_02","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("C13_02","C13_02帳票作成終了","");

		return noerror;
	}

			
	
}

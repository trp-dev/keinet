/*
 * 作成日: 2004/08/03
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.individual.InterviewBean;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.forms.individual.I502Form;
import jp.co.fj.keinavi.servlets.KNServletException;

public class I502Servlet extends IndividualServlet{
	
	/**
	 * 挿入・更新を行う最大回数
	 */
	private final int MAX_FAIL_COUNT = 3;
	
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException{
			
		//フォームデータを取得
		I502Form i502Form = null;
		try {
			i502Form = (I502Form)super.getActionForm(request, "jp.co.fj.keinavi.forms.individual.I502Form");
		} catch (Exception e) {
			KNServletException k = new KNServletException("0I502にてフォームデータの収得に失敗しました。"  + e);
			k.setErrorCode("00I502010101");
			throw k;
		}
		
		HttpSession session = request.getSession();
		
		//個人共通
		ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);

		if ("i502".equals(getForward(request))) {//転送:JSP
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(false);
						
				InterviewBean ib = new InterviewBean();
				ib.setIndividualId(i502Form.getIndividualId());
				ib.setKoIndividualId(i502Form.getKoIndividualId());

				if(i502Form.getButton() != null && i502Form.getButton().equals("update")){
					/** INSERT/UPDATE処理 */
					int failCount = 0;
					while(!insertUpDate(con, ib, i502Form)){
						if(failCount == MAX_FAIL_COUNT){
							throw new Exception();
						}
						Thread.sleep(1000);
						failCount ++;
					}				
				}else if(i502Form.getButton() != null && i502Form.getButton().equals("edit")){
					/** 編集選択 - このメモ情報を再取得するだけで、UPDATEはまだ行わない */
					ib.setEdit(true);
					ib.setFirstRegistrYdt(i502Form.getFirstRegistrYdt());
					ib.setConnection("", con);
					ib.execute();
					
					// 対象データの取得に失敗
					if (ib.getI502Data() == null) {
						final KNServletException e = new KNServletException(
								"編集対象のメモは既に削除されています。");
						e.setErrorCode("1");
						e.setClose(true);
						throw e;
					}

					//フォーム再作成(形が違うから)
					i502Form.setYear(ib.getYear());
					i502Form.setMonth(ib.getMonth());
					i502Form.setDate(ib.getDate());
				}else{
					/** 新規作成 - i502.jspを再利用するために、空のi502Dataを作成する　*/
					ib.setNew(true);
					ib.setConnection("", con);
					ib.execute();
					//新規用フォーム作成
					i502Form.setYear(ib.getYear());
					i502Form.setMonth(ib.getMonth());
					i502Form.setDate(ib.getDate());
				}
				con.commit();
				
				request.setAttribute("i502Bean", ib);
			} catch (final KNServletException e) {
				throw e;
			}catch(Exception e){
				e.printStackTrace();
				KNServletException k = new KNServletException("0I502にてエラーが発生しました。" + e);
				k.setErrorCode("00I502010101");
				try {
					con.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
					KNServletException k1 = new KNServletException("0I502にてコネクションローバックに失敗しました。"  + e);
					k1.setErrorCode("00I502010101");
					throw k1;
				}
				throw k;
			} finally {
				try {if (con != null) releaseConnectionPool(request, con);
				} catch (Exception e) {
					KNServletException k2 = new KNServletException("0I502にてコネクションの解放に失敗しました。" + e);
					k2.setErrorCode("00I502010101");
					throw k2;
				}
			}
				
			request.setAttribute("form", i502Form);
			
			iCommonMap.setTargetPersonId(i502Form.getTargetPersonId());
			
			if(i502Form.getButton() != null && i502Form.getButton().equals("update")){
				//処理が終わったらウィンドウを閉じる
				super.forward(request, response, JSP_CLOSE);
			}else{
				super.forward(request, response, JSP_I502);
			}
			
		}else{//転送:SERVLET
			
			iCommonMap.setTargetPersonId(i502Form.getTargetPersonId());
			super.forward(request, response, SERVLET_DISPATCHER);
			
		}
	
	}
	private boolean insertUpDate(Connection con, InterviewBean ib, I502Form i502Form){
		boolean isSuccess = true;
		try{
			if(i502Form.getFirstRegistrYdt().trim().equals("")){
				/** 新規挿入 - 実際にDB上に挿入を行う */
				ib.setIns(true);
			}else{
				/** 編集処理　- 実際にDB上に上書きを行う */
				ib.setUpdate(true);
				ib.setFirstRegistrYdt(i502Form.getFirstRegistrYdt());
			}
			ib.setYear(i502Form.getYear());
			ib.setMonth(i502Form.getMonth());
			ib.setDate(i502Form.getDate());
			ib.setTitle(i502Form.getTitle());
			ib.setText(i502Form.getText());
			ib.setConnection("", con);
			ib.execute();
		}catch(Exception e){
			isSuccess = false;
		}
		return isSuccess;
	}
}

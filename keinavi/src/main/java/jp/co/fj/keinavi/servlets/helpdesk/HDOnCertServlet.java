/*
 * 作成日: 2004/09/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.helpdesk;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.forms.helpdesk.HDOnCertForm;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.servlets.KNServletException;

/**
 * @author nino
 *8
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class HDOnCertServlet extends DefaultHttpServlet {
	
	public static final String THREAD_KEY = "threadStatus";
	public static final String STATUS_PROG = "0001";
	public static final String STATUS_DONE = "0002";
	public static final String STATUS_ERR_SQL = "1001";
	
	private final String ERROR_MSG_FORM = "フォームの取得に失敗しました";
	private final String ERROR_MSG_ABNORM = "Thread異常処理発生(status == null)";
	private final String ERROR_MSG_SQL = "Thread異常処理発生(SQL ERROR)";
	private final String ERROR_MSG_MAP = "結果が存在しません";
	private final String ERROR_CODE_FORM = "XXXXX";
	private final String ERROR_CODE_ABNORM = "YYYYY";
	private final String ERROR_CODE_SQL = "ZZZZZ";
	private final String ERROR_CODE_MAP = "AAAAA";
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		String status = (String) request.getSession(false).getAttribute(THREAD_KEY);
		
		HDOnCertForm form = null;
		try{
			form = (HDOnCertForm)super.getActionForm(request, "jp.co.fj.keinavi.forms.helpdesk.HDOnCertForm");
		}catch (Exception e){
			KNServletException k = new KNServletException(ERROR_MSG_FORM + e);
			k.setErrorCode(ERROR_CODE_FORM);
			throw k;
		}
		//再表示時にフォームの値を引き継ぐ必要がある
		request.setAttribute("hd106Form", form);
		
		//ありえない異常終了
		if(status == null){
			KNServletException k = new KNServletException(ERROR_MSG_ABNORM);
			k.setErrorCode(ERROR_CODE_ABNORM);
			throw k;
		}
		//SQLエラー
		else if(status.equals(STATUS_ERR_SQL)){
			KNServletException k = new KNServletException(ERROR_MSG_SQL);
			k.setErrorCode(ERROR_CODE_SQL);
			throw k;
		}
		//処理中
		else if(status.equals(STATUS_PROG)){
			//処理中表示
			super.forward(request, response, JSP_HDOnCert);
		}
		//処理が終わった
		else if(status.equals(STATUS_DONE)){

			super.forward(request, response, JSP_HD106);

		}
	}

}

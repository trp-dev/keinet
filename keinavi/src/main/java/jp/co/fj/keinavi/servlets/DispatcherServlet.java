package jp.co.fj.keinavi.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * 指定された画面IDへの転送を行うServlet
 *
 *
 * 2005.8.11 	Totec) T.Yamada 	[1]F002_5用追加
 *
 * 2005.10.12	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 * @author kawai
 *
 */
public class DispatcherServlet extends DefaultHttpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response
			) throws ServletException, IOException {

		// 転送先の画面ID
		final String id = getForward(request);
		// 転送先パス
		final String path;
		if (id == null) {
			path = null;
		} else {
			path = (String) ((Map) getServletContext().getAttribute("ForwardMap")).get(id);
		}
		if (path == null) {
			throw new ServletException("不正な転送先が指定されました。画面ID: " + id);
		} else {
			forward(request, response, path);
		}
	}

}

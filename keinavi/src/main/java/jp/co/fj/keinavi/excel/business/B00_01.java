/**
 * 校内成績分析−全国総合成績概況(高３模試)
 * 	Excelファイル編集
 * 作成日: 2004/07/30
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.business;

import java.io.*;
import java.util.*;
import java.text.*;

import jp.co.fj.keinavi.excel.data.business.*;
import jp.co.fj.keinavi.excel.cm.*;
import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.*;

public class B00_01 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private String		strArea			= "A1:N79";	//印刷範囲	
	private String		mastersheet 	= "tmp";	//テンプレートシート名
	
	final private String masterfile0 = "B00_01";
	final private String masterfile1 = "B00_01";
	private String masterfile = "";

/*
 * 	Excel編集メイン
 * 		B00Item b00Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int b00_01EditExcel(B00Item b00Item, ArrayList outfilelist, int intSaveFlg, String UserID ) {
		KNLog log = KNLog.getInstance(null,null,null);
		
		FileInputStream	fin 		= null;
		HSSFWorkbook	workbook 	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		int	intMaxSheetIndex	= 0;
		int	intMaxSheetSr	= 50;			//MAXシート数
		int			kmkMaxSheetCnt		= 1;	//科目用シートカウンタ
		int			kataCnt				= 0;	//型カウンタ
		int			kmkCnt				= 0;	//科目カウンタ
		NumberFormat nf = NumberFormat.getInstance();
		
		//テンプレートの決定
		if (b00Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		//マスタExcel読み込み
		workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
		if( workbook==null ){
			return errfread;
		}

		// 基本ファイルを読込む
		B00KataListBean b00KataListBean = new B00KataListBean();
		B00KmkListBean b00KmkListBean = new B00KmkListBean();

		try {
			// 型データセット
			ArrayList b00KataList = b00Item.getB00KataList();
			Iterator itrKata = b00KataList.iterator();
			int row = 10;

			log.Ep("B00_01","型データセット開始","");
			
			while( itrKata.hasNext() ) {
				b00KataListBean = (B00KataListBean)itrKata.next();

				if (kataCnt == 0) {
					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);
					intMaxSheetIndex++;
					row = 10;
					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);

					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( cm.toString(b00Item.getStrMshmei()) + "　成績表");
			
					// 受験者数セット
					workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
					workCell.setCellValue( "受験者総数：" + nf.format(b00Item.getIntAllNinzu()) + "名　（現役：" + nf.format(b00Item.getIntGenNinzu()) + "名／高卒：" + nf.format(b00Item.getIntSotuNinzu()) + "名）");
				}
				// 型名セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 0 );
				workCell.setCellValue( b00KataListBean.getStrKmkmei() );
				// 配点セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 1 );
				if ( !cm.toString(b00KataListBean.getStrHaitenKmk()).equals("") ) {
					workCell.setCellValue( Integer.parseInt( b00KataListBean.getStrHaitenKmk() ) );
				}
				// 平均点・全体セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 2 );
				if ( b00KataListBean.getFloHeikinAll() != -999 ) {
					workCell.setCellValue( b00KataListBean.getFloHeikinAll() );
				}
				// 平均点・現役セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 3 );
				if ( b00KataListBean.getFloHeikinGen() != -999.0 ) {
					workCell.setCellValue( b00KataListBean.getFloHeikinGen() );
				}
				// 平均点・高卒セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 4 );
				if ( b00KataListBean.getFloHeikinSotu() != -999.0 ) {
					workCell.setCellValue( b00KataListBean.getFloHeikinSotu() );
				}
				// 標準偏差セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 5 );
				if ( b00KataListBean.getFloStdHensa() != -999 ) {
					workCell.setCellValue( b00KataListBean.getFloStdHensa() );
				}
				// 平均偏差値・全国セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 6 );
				if ( b00KataListBean.getFloStdHensaAll() != -999.0 ) {
					workCell.setCellValue( b00KataListBean.getFloStdHensaAll() );
				}
				// 平均偏差値・現役セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 7 );
				if ( b00KataListBean.getFloStdHensaGen() != -999.0 ) {
					workCell.setCellValue( b00KataListBean.getFloStdHensaGen() );
				}
				// 平均偏差値・高卒セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 8 );
				if ( b00KataListBean.getFloStdHensaSotu() != -999.0 ) {
					workCell.setCellValue( b00KataListBean.getFloStdHensaSotu() );
				}
				// 最高点
				workCell = cm.setCell( workSheet, workRow, workCell, row, 9 );
				if ( b00KataListBean.getIntMaxTen() != -999 ) {
					workCell.setCellValue( b00KataListBean.getIntMaxTen() );
				}
				// 最低点
				workCell = cm.setCell( workSheet, workRow, workCell, row, 10 );
				if ( b00KataListBean.getIntMinTen() != -999.0 ) {
					workCell.setCellValue( b00KataListBean.getIntMinTen() );
				}
				// 人数・全国セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 11 );
				if ( b00KataListBean.getIntNinzuAll() != -999.0 ) {
					workCell.setCellValue( b00KataListBean.getIntNinzuAll() );
				}
				// 人数・現役セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 12 );
				if ( b00KataListBean.getIntNinzuGen() != -999.0 ) {
					workCell.setCellValue( b00KataListBean.getIntNinzuGen() );
				}
				// 人数・高卒セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 13 );
				if ( b00KataListBean.getIntNinzuSotu() != -999.0 ) {
					workCell.setCellValue( b00KataListBean.getIntNinzuSotu() );
				}
				
				row++;
				kataCnt++;
				log.It("B00_01","型カウント",String.valueOf(kataCnt));
				if (kataCnt >= 20) {
					kataCnt = 0;
				}

			}

			// 科目データセット
			ArrayList b00KmkList = b00Item.getB00KmkList();
			Iterator itrKmk = b00KmkList.iterator();
			row = 39;

			log.Ep("B00_01","科目データセット開始","");
			
			if ( intMaxSheetIndex!=0 ) {
				if (intSaveFlg==1 || intSaveFlg==5) {
					workSheet = workbook.getSheetAt(kmkMaxSheetCnt);
				} else {
					workSheet = workbook.getSheetAt(kmkMaxSheetCnt+1);
				}
			}
			
			while( itrKmk.hasNext() ) {
				b00KmkListBean = (B00KmkListBean)itrKmk.next();
				if (kmkCnt == 0) {
					if (kmkMaxSheetCnt>intMaxSheetIndex) {
						// シートテンプレートのコピー
						workSheet = workbook.cloneSheet(0);
						intMaxSheetIndex++;
						kmkMaxSheetCnt++;
					} else {
						if (intSaveFlg==1 || intSaveFlg==5) {
							workSheet = workbook.getSheetAt(kmkMaxSheetCnt);
						} else {
							workSheet = workbook.getSheetAt(kmkMaxSheetCnt+1);
						}
						kmkMaxSheetCnt++;
					}
					row = 39;
					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);

					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( cm.toString(b00Item.getStrMshmei()) + "　成績表");
			
					// 受験者数セット
					workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
					workCell.setCellValue( "受験者総数：" + nf.format(b00Item.getIntAllNinzu()) + "名　（現役：" + nf.format(b00Item.getIntGenNinzu()) + "名／高卒：" + nf.format(b00Item.getIntSotuNinzu()) + "名）");
				}
				
				// 科目名セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 0 );
				workCell.setCellValue( b00KmkListBean.getStrKmkmei() );
				// 配点セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 1 );
				if ( !cm.toString(b00KmkListBean.getStrHaitenKmk()).equals("") ) {
					workCell.setCellValue( Integer.parseInt( b00KmkListBean.getStrHaitenKmk() ) );
				}
				// 平均点・全体セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 2 );
				if ( b00KmkListBean.getFloHeikinAll() != -999 ) {
					workCell.setCellValue( b00KmkListBean.getFloHeikinAll() );
				}
				// 平均点・現役セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 3 );
				if ( b00KmkListBean.getFloHeikinGen() != -999.0 ) {
					workCell.setCellValue( b00KmkListBean.getFloHeikinGen() );
				}
				// 平均点・高卒セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 4 );
				if ( b00KmkListBean.getFloHeikinSotu() != -999.0 ) {
					workCell.setCellValue( b00KmkListBean.getFloHeikinSotu() );
				}
				// 標準偏差セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 5 );
				if ( b00KmkListBean.getFloStdHensa() != -999 ) {
					workCell.setCellValue( b00KmkListBean.getFloStdHensa() );
				}
				// 平均偏差値・全国セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 6 );
				if ( b00KmkListBean.getFloStdHensaAll() != -999.0 ) {
					workCell.setCellValue( b00KmkListBean.getFloStdHensaAll() );
				}
				// 平均偏差値・現役セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 7 );
				if ( b00KmkListBean.getFloStdHensaGen() != -999.0 ) {
					workCell.setCellValue( b00KmkListBean.getFloStdHensaGen() );
				}
				// 平均偏差値・高卒セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 8 );
				if ( b00KmkListBean.getFloStdHensaSotu() != -999.0 ) {
					workCell.setCellValue( b00KmkListBean.getFloStdHensaSotu() );
				}
				// 最高点
				workCell = cm.setCell( workSheet, workRow, workCell, row, 9 );
				if ( b00KmkListBean.getIntMaxTen() != -999 ) {
					workCell.setCellValue( b00KmkListBean.getIntMaxTen() );
				}
				// 最低点
				workCell = cm.setCell( workSheet, workRow, workCell, row, 10 );
				if ( b00KmkListBean.getIntMinTen() != -999.0 ) {
					workCell.setCellValue( b00KmkListBean.getIntMinTen() );
				}
				// 人数・全国セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 11 );
				if ( b00KmkListBean.getFloHeikinAll() != -999.0 ) {
					workCell.setCellValue( b00KmkListBean.getIntNinzuAll() );
				}
				// 人数・現役セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 12 );
				if ( b00KmkListBean.getFloHeikinGen() != -999.0 ) {
					workCell.setCellValue( b00KmkListBean.getIntNinzuGen() );
				}
				// 人数・高卒セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 13 );
				if ( b00KmkListBean.getFloHeikinSotu() != -999.0 ) {
					workCell.setCellValue( b00KmkListBean.getIntNinzuSotu() );
				}
				row++;
				kmkCnt++;
				log.It("B00_01","科目カウント",String.valueOf(kmkCnt));
				if (kmkCnt >= 40) {
					kmkCnt = 0;
				}

			}
			log.Ep("B00_01","科目データセット終了","");

			// Excelファイル保存
			boolean bolRet = false;
			bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch( Exception e ) {
			log.Err("B00_01","データセットエラー",e.toString());
			return errfdata;
		}
		
		return noerror;
	}

}

/*
 * 作成日: 2004/08/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * @author Administrator
 *
 * 2005.4.26 	K.Kondo 	[1]画面スクロール座標の追加
 */
public class I303Form extends ActionForm{

	// 選択中の生徒
	private String targetPersonId;
	// ボタン
	private String button = null;
	// 検索モード（大学名／大学コード）
	private String searchMode = null;
	// 大学名／大学コードの文字列
	private String searchStr = null;
	// Char検索時のchar value
	private String charValue = null;
	
	private String scrollX;//[1] add サブミット前、表示座標Ｘ
	private String scrollY;//[1] add サブミット前、表示座標Ｙ
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.individual.ICommonForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ
		
	}

	/**
	 * @return
	 */
	public String getButton() {
		return button;
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @param string
	 */
	public void setButton(String string) {
		button = string;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @return
	 */
	public String getSearchMode() {
		return searchMode;
	}

	/**
	 * @return
	 */
	public String getSearchStr() {
		return searchStr;
	}

	/**
	 * @param string
	 */
	public void setSearchMode(String string) {
		searchMode = string;
	}

	/**
	 * @param string
	 */
	public void setSearchStr(String string) {
		searchStr = string;
	}

	/**
	 * @return
	 */
	public String getCharValue() {
		return charValue;
	}

	/**
	 * @param string
	 */
	public void setCharValue(String string) {
		charValue = string;
	}
	
	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[1] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[1] add
	}
}

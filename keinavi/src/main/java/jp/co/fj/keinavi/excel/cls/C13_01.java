/**
 * 作成日: 2004/09/
 * @author A.Hasegawa
 *
 * 2009.11.24   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 *
 * クラス成績分析−クラス成績概況（個人成績推移） 出力処理
 *
 */
package jp.co.fj.keinavi.excel.cls;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C13AllDataListBean;
import jp.co.fj.keinavi.excel.data.cls.C13Item;
import jp.co.fj.keinavi.excel.data.cls.C13KyokaDataListBean;
import jp.co.fj.keinavi.excel.data.cls.C13ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class C13_01 {

    private int noerror = 0;		// 正常終了
    private int errfread = 1;		// ファイルreadエラー
    private int errfwrite = 2;	// ファイルwriteエラー
    private int errfdata = 3;		// データ設定エラー

    private CM cm = new CM();		//共通関数用クラス インスタンス

    private int		intMaxSheetSr	= 50;			//MAXシート数

    private boolean bolBookClsFlg = false;
    private boolean bolBookCngFlg = true;			//改ファイルフラグ
    private boolean bolSheetCngFlg = true;			//改シートフラグ
    private int intDataStartRow = 10;					//データセット開始行

    final private String masterfile0 = "C13_01" ;
    final private String masterfile1 = "NC13_01" ;
    private String masterfile = "" ;

    /*
     * 	Excel編集メイン
     * 		C13Item c13Item: データクラス
     * 		String 	masterfile: マスタExcelファイル名（フルパス）
     * 		String 	outfile: 出力Excelファイル名（フルパス）
     * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
     * 		String	UserID：ユーザーID
     * 		戻り値: 0:正常終了、0以外:異常終了
     */
    public int c13_01EditExcel(C13Item c13Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

        HSSFWorkbook	workbook = null;
        HSSFSheet	workSheet		= null;
        HSSFCell	workCell		= null;
        HSSFRow		workRow			= null;

        //テンプレートの決定
        if (c13Item.getIntShubetsuFlg() == 1){
            masterfile = masterfile1;
        } else{
            masterfile = masterfile0;
        }

        int intMaxSheetIndex = 0;					//シート作成数カウンタ

        int intRow = 0;
        int intRowBase = 0;
        int intRowSg = 0;		//総合
        int intRowEg = 0;		//英語
        int intRowMm = 0;		//数学
        int intRowJp = 0;		//国語
        int intRowSc = 0;		//理科
        int intRowSs = 0;		//地歴公民

        float floHensaAllNow = 0;
        float floHensaAllLast = 0;
        float floPitch = 0;
        float fltMstPitch = 0;
        float floPitch_sogo= 0 ;		//変動幅

        String strPitch = "";
        String strFlont = "※総合成績の偏差値より";
        String strRea = "ポイント以上低い科目の偏差値に" + "*" + "を表示しています。";

        int intCount = 0;

        int intBookCngCount = 0;

        KNLog log = KNLog.getInstance(null,null,null);

        log.Ep("C13_01","C13_01帳票作成開始","");

        // 基本ファイルを読込む
        C13ListBean c13ListBean = new C13ListBean();

        try{
            ArrayList c13List = c13Item.getC13List();
            ListIterator itr = c13List.listIterator();

            while( itr.hasNext() ){

                c13ListBean = (C13ListBean)itr.next();
                ArrayList c13AllDataList = c13ListBean.getC13AllDataList();

                C13AllDataListBean c13AllDataListBean = new C13AllDataListBean();
                Iterator itrAll = c13AllDataList.iterator();

                while(itrAll.hasNext()){


                    if( bolSheetCngFlg == true ){

                        if( bolBookCngFlg == true ){

                            if(bolBookClsFlg == true){
                                //WorkBook保存
                                boolean bolRet = false;
                                // Excelファイル保存
                                if(itr.hasNext() == true){
                                    bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
                                }
                                else{
                                    bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
                                }

                                if( bolRet == false ){
                                    return errfwrite;
                                }
                                intBookCngCount++;
                                bolBookClsFlg = false;
                            }

                            // マスタExcel読み込み
                            workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
                            if( workbook==null ){
                                return errfread;
                            }

                            bolBookCngFlg = false;
                            intMaxSheetIndex = 0;
                        }

                        //シート作成
                        // シートテンプレートのコピー
                        workSheet = workbook.cloneSheet(0);

                        // ヘッダ右側に帳票作成日時を表示する
                        cm.setHeader(workbook, workSheet);

                        // セキュリティスタンプセット
                        String secuFlg = cm.setSecurity( workbook, workSheet, c13Item.getIntSecuFlg(), 31, 34 );
                        workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
                        workCell.setCellValue(secuFlg);

                        // 学校名セット
                        String strGakkoMei = "学校名　：" + cm.toString(c13Item.getStrGakkomei()) + "　学年：" + cm.toString(c13ListBean.getStrGrade()) + "　クラス名：" + cm.toString(c13ListBean.getStrClass());
                        workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
                        workCell.setCellValue( strGakkoMei );

                        // 今回模試セット
                        if(!cm.toString(c13Item.getStrMshmeiNow()).equals("")){
                            // 模試月取得(対象模試)
                            String moshiNow =cm.setTaisyouMoshi( c13Item.getStrMshDateNow() );
                            // 対象模試セット
                            workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
                            workCell.setCellValue( cm.getThisExamLabel() + "：" + cm.toString(c13Item.getStrMshmeiNow()) + moshiNow);
                        }else{
                            // 対象模試セット
                            workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
                            workCell.setCellValue( cm.getThisExamLabel() + "：" );
                        }

                        // 前回模試セット
                        if(!cm.toString(c13Item.getStrMshmeiLast1()).equals("")){
                            // 模試月取得(過回模試１)
                            String moshiLast1 =cm.setTaisyouMoshi( c13Item.getStrMshDateLast1() );
                            // 対象模試セット
                            workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
                            workCell.setCellValue( cm.getLastExamLabel() + "：" + cm.toString(c13Item.getStrMshmeiLast1()) + moshiLast1);
                        }else{
                            workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
                            workCell.setCellValue( cm.getLastExamLabel() + "：" );
                        }

                        //表示条件
                        workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
                        if(!cm.toString(c13Item.getStrKeyHensa()).equals("")){
                            workCell.setCellValue( "表示条件：偏差値" + cm.toString(c13Item.getStrKeyHensa()) + "以上");
                        }else{
                            workCell.setCellValue( "表示条件：" );
                        }

                        //変動幅
                        workCell = cm.setCell( workSheet, workRow, workCell, 5, 0 );
                        if(!cm.toString(c13Item.getStrKeyPitch()).equals("")){
                            if (c13Item.getIntShubetsuFlg() == 1){
                                workCell.setCellValue( "変動幅　：" + cm.toString(c13Item.getStrKeyPitch()) +"点以上");
                            } else{
                                workCell.setCellValue( "変動幅　：" + cm.toString(c13Item.getStrKeyPitch()) +"ポイント以上");
                            }
                            fltMstPitch = Float.parseFloat(c13Item.getStrKeyPitch());
                            floPitch_sogo = Float.parseFloat(c13Item.getStrKeyPitch());
                        }else{
                            workCell.setCellValue( "変動幅　：" );
                            fltMstPitch = 0;
                            floPitch_sogo= 0 ;
                        }

                        //注釈コメント
                        if (c13Item.getIntShubetsuFlg() == 1){
                        } else{
                            workCell = cm.setCell( workSheet, workRow, workCell, 5, 34 );
                            workCell.setCellValue( strFlont + cm.toString(c13Item.getStrKeyPitch()) + strRea );
                        }

                        intMaxSheetIndex++;		//シート数カウンタインクリメント
                        bolSheetCngFlg = false;

                        intRowBase = intDataStartRow;
                        intCount = 0;
                    }

                    c13AllDataListBean = (C13AllDataListBean) itrAll.next();

                    intRow = intRowBase + (intCount * 3) - 1;

                    //学年
                    if(!cm.toString(c13AllDataListBean.getStrGrade()).equals("")){
                        workCell = cm.setCell( workSheet, workRow, workCell, intRow, 0);
                        workCell.setCellValue( c13AllDataListBean.getStrGrade() );
                    }

                    //クラス
                    if(!cm.toString(c13AllDataListBean.getStrClass()).equals("")){
                        workCell = cm.setCell( workSheet, workRow, workCell, intRow, 1);
                        workCell.setCellValue( c13AllDataListBean.getStrClass() );
                    }

                    //クラス番号
                    if(!cm.toString(c13AllDataListBean.getStrClassNo()).equals("")){
                        workCell = cm.setCell( workSheet, workRow, workCell, intRow, 2);
                        workCell.setCellValue( c13AllDataListBean.getStrClassNo() );
                    }

                    //氏名
                    if(!cm.toString(c13AllDataListBean.getStrKanaName()).equals("")){
                        workCell = cm.setCell( workSheet, workRow, workCell, intRow, 3);
                        workCell.setCellValue( c13AllDataListBean.getStrKanaName() );
                    }

                    //性別
                    int sexKbn = Integer.parseInt( cm.toString(c13AllDataListBean.getStrSex()) );
                    String strSex = "";
                    if( sexKbn == 1 ){
                        strSex = "男";
                    }else if( sexKbn == 2 ){
                        strSex = "女";
                    }else if( sexKbn == 9 ){
                        strSex = "不";
                    }
                    workCell = cm.setCell( workSheet, workRow, workCell, intRow, 4 );
                    workCell.setCellValue( strSex );


                    C13KyokaDataListBean c13KyokaDataListBean = new C13KyokaDataListBean();
                    Iterator itrKyoka = c13AllDataListBean.getC13KyokaDataList().iterator();

                    intRowSg = 0;
                    intRowEg = 0;
                    intRowMm = 0;
                    intRowJp = 0;
                    intRowSc = 0;
                    intRowSs = 0;

                    floPitch = 0;
                    floHensaAllNow = 0;
                    String kyouka = "";
                    while( itrKyoka.hasNext() ){

                        //データリストより各項目を取得する
                        c13KyokaDataListBean = (C13KyokaDataListBean)itrKyoka.next();

                        // 教科コードによって判定
                        String strKyouka = cm.toString(c13KyokaDataListBean.getStrKyokaCd());
                        strKyouka = strKyouka.substring(0,1);
                        switch( Integer.parseInt(strKyouka) ){
                            //総合
                            case 7:
                            case 8:
                                if (intRowSg < 3 && !kyouka.equals(strKyouka)){
                                    if (intRowSg==0){
                                        if(c13KyokaDataListBean.getFloHensaAllNow()!=-999.0){
                                            floHensaAllNow = c13KyokaDataListBean.getFloHensaAllNow();
                                        }else{
                                            floHensaAllNow = 0;
                                        }
                                        if(c13KyokaDataListBean.getFloHensaAllLast()!=-999.0){
                                            floHensaAllLast = c13KyokaDataListBean.getFloHensaAllLast();
                                        }else{
                                            floHensaAllLast = 0;
                                        }
                                    }
                                    //型
                                    if(!cm.toString(c13KyokaDataListBean.getStrKmkmei()).equals("")){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSg, 5);
                                        workCell.setCellValue( c13KyokaDataListBean.getStrKmkmei() );
                                    }

                                    //偏差値
                                    if(c13KyokaDataListBean.getFloHensaAllNow()!=-999.0){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSg, 6);
                                        workCell.setCellValue( c13KyokaDataListBean.getFloHensaAllNow() );
                                    }

                                    //学力レベル
                                    if(!"".equals(c13KyokaDataListBean.getStrScholarLevel())){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSg, 7);
                                        workCell.setCellValue( c13KyokaDataListBean.getStrScholarLevel() );
                                    }

                                    //前回差
                                    if(c13KyokaDataListBean.getFloDiffer()!=-999.0){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSg, 8);
                                        workCell.setCellValue( c13KyokaDataListBean.getFloDiffer() );
                                    }

                                    //傾向 2004.11.14 変動幅でﾁｪｯｸする
                                    String strKeiko = "";
                                    if(c13KyokaDataListBean.getFloDiffer()!=-999.0){
                                        if(c13KyokaDataListBean.getFloDiffer() >= floPitch_sogo) {
                                            strKeiko = "△";
                                        }else if(c13KyokaDataListBean.getFloDiffer() <= floPitch_sogo * -1) {
                                            strKeiko = "▼";
                                        }
                                    }else if(floHensaAllLast == 0) {
                                            strKeiko = "−";
                                    }
                                    workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSg, 9);
                                    workCell.setCellValue( strKeiko );
                                }
                                if ( !kyouka.equals(strKyouka) ){
                                    intRowSg++;
                                }
                                break;

                            //英語
                            case 1:
                                //科目
                                if (intRowEg < 3){
                                    workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowEg, 10);
                                    workCell.setCellValue( c13KyokaDataListBean.getStrKmkmei() );

                                    //変動幅の確認
                                    if(c13KyokaDataListBean.getFloHensaAllNow()!=-999.0){
                                        floPitch = floHensaAllNow - c13KyokaDataListBean.getFloHensaAllNow();
                                    }else{
                                        floPitch = floHensaAllNow;
                                    }

                                    // 得点用のときは処理しない
                                    if (c13Item.getIntShubetsuFlg() == 1){
                                    } else{
                                        strPitch = "";
                                        if(floPitch >= fltMstPitch) {
                                            strPitch = "*";
                                            workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowEg, 11);
                                            workCell.setCellValue( strPitch );
                                        }
                                    }

                                    //偏差値
                                    if(c13KyokaDataListBean.getFloHensaAllNow()!=-999.0){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowEg, 12);
                                        workCell.setCellValue( c13KyokaDataListBean.getFloHensaAllNow() );
                                    }

                                    //学力レベル
                                    if(!"".equals(c13KyokaDataListBean.getStrScholarLevel())){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowEg, 13);
                                        workCell.setCellValue( c13KyokaDataListBean.getStrScholarLevel() );
                                    }

                                    //前回差
                                    if(c13KyokaDataListBean.getFloDiffer()!=-999.0){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowEg, 14);
                                        workCell.setCellValue( c13KyokaDataListBean.getFloDiffer() );
                                    }
                                }
                                intRowEg++;
                                break;

                            //数学
                            case 2:
                                //科目
                                if (intRowMm < 3){
                                    workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowMm, 15);
                                    workCell.setCellValue( c13KyokaDataListBean.getStrKmkmei() );

                                    //変動幅の確認
                                    if(c13KyokaDataListBean.getFloHensaAllNow()!=-999.0){
                                        floPitch = floHensaAllNow - c13KyokaDataListBean.getFloHensaAllNow();
                                    }else{
                                        floPitch = floHensaAllNow;
                                    }

                                    // 得点用のときは処理しない
                                    if (c13Item.getIntShubetsuFlg() == 1){
                                    } else{
                                        strPitch = "";
                                        if(floPitch >= fltMstPitch) {
                                            strPitch = "*";
                                            workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowMm, 16);
                                            workCell.setCellValue( strPitch );
                                        }
                                    }

                                    //偏差値
                                    if(c13KyokaDataListBean.getFloHensaAllNow()!=-999.0){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowMm, 17);
                                        workCell.setCellValue( c13KyokaDataListBean.getFloHensaAllNow() );
                                    }

                                    //学力レベル
                                    if(!"".equals(c13KyokaDataListBean.getStrScholarLevel())){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowMm, 18);
                                        workCell.setCellValue( c13KyokaDataListBean.getStrScholarLevel() );
                                    }

                                    //前回差
                                    if(c13KyokaDataListBean.getFloDiffer()!=-999.0){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowMm, 19);
                                        workCell.setCellValue( c13KyokaDataListBean.getFloDiffer() );
                                    }
                                }
                                intRowMm++;
                                break;

                            //国語
                            case 3:
                                //科目
                                if (intRowJp < 3){
                                    workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowJp, 20);
                                    workCell.setCellValue( c13KyokaDataListBean.getStrKmkmei() );

                                    //変動幅の確認
                                    if(c13KyokaDataListBean.getFloHensaAllNow()!=-999.0){
                                        floPitch = floHensaAllNow - c13KyokaDataListBean.getFloHensaAllNow();
                                    }else{
                                        floPitch = floHensaAllNow;
                                    }

                                    // 得点用のときは処理しない
                                    if (c13Item.getIntShubetsuFlg() == 1){
                                    } else{
                                        strPitch = "";
                                        if(floPitch >= fltMstPitch) {
                                            strPitch = "*";
                                            workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowJp, 21);
                                            workCell.setCellValue( strPitch );
                                        }
                                    }

                                    //偏差値
                                    if(c13KyokaDataListBean.getFloHensaAllNow()!=-999.0){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowJp, 22);
                                        workCell.setCellValue( c13KyokaDataListBean.getFloHensaAllNow() );
                                    }

                                    //学力レベル
                                    if(!"".equals(c13KyokaDataListBean.getStrScholarLevel())){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowJp, 23);
                                        workCell.setCellValue( c13KyokaDataListBean.getStrScholarLevel() );
                                    }

                                    //前回差
                                    if(c13KyokaDataListBean.getFloDiffer()!=-999.0){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowJp, 24);
                                        workCell.setCellValue( c13KyokaDataListBean.getFloDiffer() );
                                    }
                                }
                                intRowJp++;
                                break;

                            //理科
                            case 4:
                                //科目
                                if (intRowSc < 3){
                                    //先頭科目が基礎科目なら、先頭は空欄にする
                                    if (intRowSc == 0 && "1".equals(c13KyokaDataListBean.getBasicFlg())) {
                                        intRowSc++;
                                    }

                                    workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSc, 25);
                                    workCell.setCellValue( c13KyokaDataListBean.getStrKmkmei() );

                                    //変動幅の確認
                                    if(c13KyokaDataListBean.getFloHensaAllNow()!=-999.0){
                                        floPitch = floHensaAllNow - c13KyokaDataListBean.getFloHensaAllNow();
                                    }else{
                                        floPitch = floHensaAllNow;
                                    }

                                    // 得点用のときは処理しない
                                    if (c13Item.getIntShubetsuFlg() == 1){
                                    } else{
                                        strPitch = "";
                                        if(floPitch >= fltMstPitch) {
                                            strPitch = "*";
                                            workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSc, 26);
                                            workCell.setCellValue( strPitch );
                                        }
                                    }

                                    //偏差値
                                    if(c13KyokaDataListBean.getFloHensaAllNow()!=-999.0){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSc, 27);
                                        workCell.setCellValue( c13KyokaDataListBean.getFloHensaAllNow() );
                                    }

                                    //学力レベル
                                    if(!"".equals(c13KyokaDataListBean.getStrScholarLevel())){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSc, 28);
                                        workCell.setCellValue( c13KyokaDataListBean.getStrScholarLevel() );
                                    }

                                    //前回差
                                    if(c13KyokaDataListBean.getFloDiffer()!=-999.0){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSc, 29);
                                        workCell.setCellValue( c13KyokaDataListBean.getFloDiffer() );
                                    }
                                }
                                intRowSc++;
                                break;

                            //地歴公民
                            case 5:
                                //科目
                                if (intRowSs < 3){
                                    workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSs, 30);
                                    workCell.setCellValue( c13KyokaDataListBean.getStrKmkmei() );

                                    //変動幅の確認
                                    if(c13KyokaDataListBean.getFloHensaAllNow()!=-999.0){
                                        floPitch = floHensaAllNow - c13KyokaDataListBean.getFloHensaAllNow();
                                    }else{
                                        floPitch = floHensaAllNow;
                                    }

                                    // 得点用のときは処理しない
                                    if (c13Item.getIntShubetsuFlg() == 1){
                                    } else{
                                        strPitch = "";
                                        if(floPitch >= fltMstPitch) {
                                            strPitch = "*";
                                            workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSs, 31);
                                            workCell.setCellValue( strPitch );
                                        }
                                    }

                                    //偏差値
                                    if(c13KyokaDataListBean.getFloHensaAllNow()!=-999.0){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSs, 32);
                                        workCell.setCellValue( c13KyokaDataListBean.getFloHensaAllNow() );
                                    }

                                    //学力レベル
                                    if(!"".equals(c13KyokaDataListBean.getStrScholarLevel())){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSs, 33);
                                        workCell.setCellValue( c13KyokaDataListBean.getStrScholarLevel() );
                                    }

                                    //前回差
                                    if(c13KyokaDataListBean.getFloDiffer()!=-999.0){
                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow + intRowSs, 34);
                                        workCell.setCellValue( c13KyokaDataListBean.getFloDiffer() );
                                    }
                                }
                                intRowSs++;
                                break;

                        }
                        if (!kyouka.equals(strKyouka)) {

                        }
                        kyouka = strKyouka;
                    }
                    intCount++ ;
                    if (intCount >= 20){
                        bolSheetCngFlg = true;
                        intCount = 0;
                        if (intMaxSheetIndex >= intMaxSheetSr){
                            bolBookCngFlg = true;
                            bolBookClsFlg = true;
                        }
                    }

                }


                bolSheetCngFlg = true;
                intCount = 0;
                if (intMaxSheetIndex >= intMaxSheetSr){
                    bolBookCngFlg = true;
                    bolBookClsFlg = true;
                }

            }

            // Excelファイル保存
            boolean bolRet = false;

            if(intBookCngCount > 0 || itr.hasNext() == true){
                bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
            }
            else{
                bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
            }

            if( bolRet == false ){
                return errfwrite;

            }

        }
        catch( Exception e ){
            log.Err("99C13_01","データセットエラー",e.toString());
            return errfdata;
        }

        log.Ep("C13_01","C13_01帳票作成開始","");

        return noerror;
    }



}

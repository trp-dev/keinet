/*
 * 作成日: 2004/07/13
 *
 * 校内成績分析ー過年度比較−志望大学評価別人数
 * 作成日: 2004/07/13
 * @author	C.Murata
 */
package jp.co.fj.keinavi.excel.data.school;

import java.util.*;

public class S24Item {
	
	//学校名
	private String strGakkomei = "";
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//模試コード
	private String strMshCd = "";
	//大学集計区分
	private int intDaiTotalFlg = 0;
	//セキュリティスタンプ
	private int intSecuFlg = 0;
	//データリスト
	private ArrayList s24List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;

	
	/*
	 * GET
	 */
	public int getIntDaiTotalFlg() {
		return intDaiTotalFlg;
	}
	public int getIntSecuFlg() {
		return intSecuFlg;
	}
	public ArrayList getS24List() {
		return s24List;
	}
	public String getStrGakkomei() {
		return strGakkomei;
	}
	public String getStrMshDate() {
		return strMshDate;
	}
	public String getStrMshmei() {
		return strMshmei;
	}
	public String getStrMshCd() {
		return strMshCd;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/*
	 * SET
	 */
	public void setIntDaiTotalFlg(int i) {
		intDaiTotalFlg = i;
	}
	public void setIntSecuFlg(int i) {
		intSecuFlg = i;
	}
	public void setS24List(ArrayList list) {
		s24List = list;
	}
	public void setStrGakkomei(String string) {
		strGakkomei = string;
	}
	public void setStrMshDate(String string) {
		strMshDate = string;
	}
	public void setStrMshmei(String string) {
		strMshmei = string;
	}
	public void setStrMshCd(String string) {
		strMshCd = string;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}

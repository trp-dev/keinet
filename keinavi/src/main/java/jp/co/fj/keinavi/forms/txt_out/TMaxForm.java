/*
 * 作成日: 2004/10/12
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.txt_out;

/**
 * @author 奥村ゆかり
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
import jp.co.fj.keinavi.forms.BaseForm;

public class TMaxForm extends BaseForm {
	
	// 出力対象
	private String[] outTarget = null;
	// 保存処理
	private String save = null;
	// ファイル形式
	private String txtType = null;
	// 出力
	private String out = null;
	
	private String changeExam; // 模試に対する変更を行ったかどうか

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * @return
	 */
	public String[] getOutTarget() {
		return outTarget;
	}

	/**
	 * @return
	 */
	public String getSave() {
		return save;
	}

	/**
	 * @param strings
	 */
	public void setOutTarget(String[] strings) {
		outTarget = strings;
	}

	/**
	 * @param string
	 */
	public void setSave(String string) {
		save = string;
	}

	/**
	 * @return
	 */
	public String getChangeExam() {
		return changeExam;
	}

	/**
	 * @param string
	 */
	public void setChangeExam(String string) {
		changeExam = string;
	}

	/**
	 * @return
	 */
	public String getTxtType() {
		return txtType;
	}

	/**
	 * @param string
	 */
	public void setTxtType(String string) {
		txtType = string;
	}

	/**
	 * @return
	 */
	public String getOut() {
		return out;
	}

	/**
	 * @param string
	 */
	public void setOut(String string) {
		out = string;
	}

}

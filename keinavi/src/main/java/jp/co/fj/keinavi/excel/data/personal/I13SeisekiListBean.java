package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
import java.util.ArrayList;
/**
 * �l���ѕ��́|�l���ѕ\�|�A���l���ѕ\ �A���l���ѕ\�f�[�^���X�g
 * �쐬��: 2004/07/20
 * @author	A.Iwata
 * 
 * 2006/09/04 Totec)T.Yamada �󌱊w�͑���Ή�
 * 
 */
public class I13SeisekiListBean {
	//�͎��R�[�h
	private String strMshCd = "";
	//�͎���
	private String strMshmei = "";
	//�͎����{���
	private String strMshDate = "";
    
    /**
     * �͎���ރR�[�h
     */
    private String strMshTypeCd;
    
	//���ȕʐ��у��X�g
	private ArrayList i13KyokaSeisekiList = new ArrayList();
	//�u�]��w�ʕ]���f�[�^���X�g
	private ArrayList i13HyoukaList = new ArrayList();
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrMshCd() {
		return this.strMshCd;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public ArrayList getI13KyokaSeisekiList() {
		return this.i13KyokaSeisekiList;
	}
	public ArrayList getI13HyoukaList() {
		return this.i13HyoukaList;
	}
    
    /**
     * @return �͎���ރR�[�h
     */
    public String getStrMshTypeCd() {
        return strMshTypeCd;
    }
		
	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrMshCd(String strMshCd) {
		this.strMshCd = strMshCd;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setI13KyokaSeisekiList(ArrayList i13KyokaSeisekiList) {
		this.i13KyokaSeisekiList = i13KyokaSeisekiList;
	}
	public void setI13HyoukaList(ArrayList i13HyoukaList) {
		this.i13HyoukaList = i13HyoukaList;
	}

    /**
     * @param pStrMshTypeCd �͎���ރR�[�h
     */
    public void setStrMshTypeCd(String pStrMshTypeCd) {
        strMshTypeCd = pStrMshTypeCd;
    }
    
    
}

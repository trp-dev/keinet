package jp.co.fj.keinavi.filter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import jp.co.fj.keinavi.util.RangeResponse;
import jp.co.totec.io.IOUtil;

/**
 *
 * 資産コンテンツフィルタです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class AssetContentsFilter implements Filter {

    /** FilterConfig */
    private FilterConfig filterConfig;

    /** 資産ルートパス */
    private File rootPath;

    /** 公開資産パス */
    private File publicPath;

    /** 非公開資産パス */
    private File privatePath;

    /**
     * {@inheritDoc}
     */
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        try {
            PropertiesConfiguration config = new PropertiesConfiguration(
                    AssetContentsFilter.class.getResource("/jp/co/fj/keinavi/resources/common.properties"));
            rootPath = new File(config.getString("AssetRootPath", "/keinavi/help_user"));
            if (!rootPath.exists()) {
                System.out.println("警告：資産ルートパス（AssetRootPath）に設定されているディレクトリが存在しません。" + rootPath);
            }
            publicPath = new File(rootPath, "public");
            privatePath = new File(rootPath, "private");
        } catch (ConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        /* 資産ファイルオブジェクトを生成する */
        File file = new File(rootPath, request.getServletPath());

        /* ディレクトリへのアクセスの場合は、index.htmlへのアクセスと見なす */
        if (file.isDirectory()) {
            file = new File(file, "index.html");
        }

        /* 資産ファイルの妥当性をチェックする */
        if (!file.isFile() || (!file.getCanonicalPath().startsWith(publicPath.getCanonicalPath())
                && !file.getCanonicalPath().startsWith(privatePath.getCanonicalPath()))) {
            /* 資産が見つからない場合は404エラーとする */
            /* ディレクトリトラバーサル対策も同時に実施する */
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            request.getRequestDispatcher("/404.html").forward(request, response);
            return;
        }

        /* クライアントに資産ファイルを送信する */
        String range = request.getHeader(RangeResponse.RANGE);
        if (range == null) {
            /* Rangeヘッダなし */
            sendAssetFile(request, response, file);
        } else {
            /* Rangeヘッダあり（部分リクエスト） */
            new RangeResponse(response, range, file, getContentType(file)).output();
        }
    }

    /**
     * クライアントに資産ファイルを送信します。
     *
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @param file 資産ファイル
     * @throws IOException IO例外
     */
    private void sendAssetFile(HttpServletRequest request, HttpServletResponse response, File file) throws IOException {

        String eTag = "\"" + calcETag(file) + "\"";
        if (eTag.equals(request.getHeader("If-None-Match"))) {
            /* ETagが一致するので、304（Not Modified）を返す */
            response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
            return;
        }

        response.setContentLength((int) file.length());
        response.setContentType(getContentType(file));
        response.setHeader("Accept-Ranges", "bytes");
        response.setHeader("ETag", eTag);
        response.setDateHeader("Expires", System.currentTimeMillis() + 24 * 60 * 60 * 1000);

        InputStream in = null;
        byte[] buff = new byte[16 * 1024];
        try {
            in = new FileInputStream(file);
            OutputStream out = response.getOutputStream();
            int len = -1;
            while ((len = in.read(buff, 0, buff.length)) > -1) {
                out.write(buff, 0, len);
            }
            out.flush();
        } finally {
            IOUtil.closeQuietly(in);
        }
    }

    /**
     * 資産ファイルのETagを計算します。
     *
     * @param file 資産ファイル
     * @return ETag
     */
    private String calcETag(File file) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        return new String(Hex.encodeHex(digest.digest((file.length() + "/" + file.lastModified()).getBytes())));
    }

    /**
     * 資産ファイルのContent-Typeを返します。
     *
     * @param file 資産ファイル
     * @return Content-Type
     */
    private String getContentType(File file) {
        return filterConfig.getServletContext().getMimeType(file.getName());
    }

    /**
     * {@inheritDoc}
     */
    public void destroy() {
    }

}

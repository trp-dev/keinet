package jp.co.fj.keinavi.forms.maintenance;


/**
 *
 * 統合画面アクションフォーム
 * 
 * 2006.10.27	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M107Form extends M105Form {

	// 編集画面から来たかどうかのフラグ
	private String editFlag;

	/**
	 * @return editFlag を戻します。
	 */
	public String getEditFlag() {
		return editFlag;
	}

	/**
	 * @param pEditFlag 設定する editFlag。
	 */
	public void setEditFlag(String pEditFlag) {
		editFlag = pEditFlag;
	}
	
}

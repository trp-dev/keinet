/*
 * 作成日: 2004/10/08
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.help;

import java.sql.SQLException;
import java.sql.Connection;

import com.fjh.beans.DefaultBean;
import jp.co.fj.keinavi.util.date.DateUtil;
import jp.co.fj.keinavi.util.date.FullDateUtil;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class H101Bean extends DefaultBean {

	private Connection conn = null;

	/* 
	 * 
	 * (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// TODO 自動生成されたメソッド・スタブ

		// 日付用クラス
		DateUtil date = new FullDateUtil();

	}


	/**
	 * @return
	 */
	public Connection getConn() {
		return conn;
	}

	/**
	 * @param connection
	 */
	public void setConn(Connection connection) {
		conn = connection;
	}

}

package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * 校内成績分析−過回比較−成績概況 データクラス
 * 作成日: 2004/07/21
 * @author	A.Iwata
 */
public class S51Item {
	//学校名
	private String strGakkomei = "";
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//セキュリティスタンプフラグ
	private int intSecuFlg = 0;
	//表フラグ
	private int intHyouFlg = 0;
	//グラフフラグ
	private int intGraphFlg = 0;
	//過年度比較フラグ
	private int intNendoFlg = 0;
	//クラス比較フラグ
	private int intClassFlg = 0;
	//他校比較フラグ
	private int intTakouFlg = 0;
	//データリスト
	private ArrayList s51List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public int getIntHyouFlg() {
		return this.intHyouFlg;
	}
	public int getIntGraphFlg() {
		return this.intGraphFlg;
	}
	public int getIntNendoFlg() {
		return this.intNendoFlg;
	}
	public int getIntClassFlg() {
		return this.intClassFlg;
	}
	public int getIntTakouFlg() {
		return this.intTakouFlg;
	}
	public ArrayList getS51List() {
		return this.s51List;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setIntHyouFlg(int intHyouFlg) {
		this.intHyouFlg = intHyouFlg;
	}
	public void setIntGraphFlg(int intGraphFlg) {
		this.intGraphFlg = intGraphFlg;
	}
	public void setIntNendoFlg(int intNendoFlg) {
		this.intNendoFlg = intNendoFlg;
	}
	public void setIntClassFlg(int intClassFlg) {
		this.intClassFlg = intClassFlg;
	}
	public void setIntTakouFlg(int intTakouFlg) {
		this.intTakouFlg = intTakouFlg;
	}
	public void setS51List(ArrayList s51List) {
		this.s51List = s51List;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}

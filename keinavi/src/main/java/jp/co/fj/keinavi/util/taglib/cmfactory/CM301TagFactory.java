/*
 * 作成日: 2004/07/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.taglib.cmfactory;

import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;

import jp.co.fj.keinavi.forms.com_set.CM301Form;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * 
 * 2005.05.29	Yoshimoto KAWAI - Totec
 * 				有効な比較対象年度のみを評価するように変更
 * 
 * @author kawai
 *
 */
public class CM301TagFactory extends AbstractCMTagFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.taglib.cmfactory.AbstractCMTagFactory#createCMStatus(javax.servlet.ServletRequest)
	 */
	public String createCMStatus(ServletRequest request, HttpSession session) throws JspException {
		String result = null;
		Map item = getItemMap(session);
		//String[] year = (String[])item.get("1101");
		String[] year = ProfileUtil.compYearFilter((String[]) item.get("1101"),
				super.getExamData(request, session));
		
		if (year.length == 0) result = "設定なし";
		else result = year.length + "年度分";

		return result;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.taglib.cmfactory.AbstractCMTagFactory#createTempCMStatus(javax.servlet.ServletRequest, javax.servlet.http.HttpSession)
	 */
	public String createTempCMStatus(ServletRequest request, HttpSession session) throws JspException {
		String result = null;
		CM301Form form = (CM301Form) super.getCommonForm(session).getActionForm("CM301Form");
		String[] year = ProfileUtil.compYearFilter(form.getCompYear(),
				super.getExamData(request, session));
		
		if (year.length == 0) result = "設定なし";
		else result = year.length + "年度分";
		
		return result;
	}

}

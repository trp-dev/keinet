/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.cls.factory;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.forms.cls.CMaxForm;

import com.fjh.forms.ActionForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class C006FormFactory extends AbstractCFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		// アクションフォーム
		CMaxForm form = super.createCMaxForm(request);
		// アイテムマップ
		Map map = getItemMap(request);
		
		//  国私区分
		getPrintUnivDiv( map, form );
		//  志望者の表示順序
		getCandidateOrder( map, form );
		//  大学の表示順序
		getUnivOrder( map, form );
		//  印刷対象クラス
		getPrintClass( map, form );
		//  志望大学
		getChoiceUnivMode( map, form);
		//  志望大学（志望者数）
		getCandRange( map, form);
		//  志望大学（評価基準）
		getEvaluation( map, form);
		//  セキュリティスタンプ
		getStamp( map, form );

		return form;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
		CMaxForm form = (CMaxForm)request.getAttribute("form");
		// アイテムマップ
		Map map = getItemMap(request);
		
		//  国私区分
		setPrintUnivDiv( map, form );
		//  志望者の表示順序
		setCandidateOrder( map, form );
		//  大学の表示順序
		setUnivOrder( map, form );
		//  印刷対象クラス
		setPrintClass( map, form );
		//  志望大学
		setChoiceUnivMode( map, form);
		//  志望大学（志望者数）
		setCandRange( map, form);
		//  志望大学（評価基準）
		setEvaluation( map, form);
		//  セキュリティスタンプ
		setStamp( map, form );
		
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap("030105");
	}

}

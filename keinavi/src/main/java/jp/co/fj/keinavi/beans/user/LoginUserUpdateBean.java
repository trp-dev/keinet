package jp.co.fj.keinavi.beans.user;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.user.LoginUserData;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 利用者更新Bean
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class LoginUserUpdateBean extends LoginUserInsertBean {

	// 学校コード
	private final String schoolCd;
	// 利用者データ
	private final LoginUserData loginUserData;
	
	/**
	 * コンストラクタ
	 */
	public LoginUserUpdateBean(final String schoolCd, final LoginUserData loginUserData) {
		
		super(schoolCd, loginUserData);
		
		this.schoolCd = schoolCd;
		this.loginUserData = loginUserData;
	}
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.user.LoginUserInsertBean#registUser()
	 */
	public void registUser() throws SQLException, Exception {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("u06").toString());
			
			// 利用者名
			ps.setString(1, loginUserData.getLoginName());
			
			// メンテ可
			if (loginUserData.isMaintainer()) {
				ps.setString(2, "1");
				ps.setString(3, "1");
				ps.setString(4, "1");
				ps.setString(5, "1");
				ps.setString(6, "1");
				
			// メンテ不可
			} else {
				ps.setString(2, "2");
				ps.setString(3, String.valueOf(loginUserData.getKnFunctionFlag()));
				ps.setString(4, String.valueOf(loginUserData.getScFunctionFlag()));
				ps.setString(5, String.valueOf(loginUserData.getEeFunctionFlag()));
				ps.setString(6, String.valueOf(loginUserData.getAbFunctionFlag()));
			}
			
			// 学校コード
			ps.setString(7, schoolCd);
			// 利用者ID
			ps.setString(8, loginUserData.getLoginId());
			
			if (ps.executeUpdate() == 0) {
				throw new SQLException("管理者のレコードは更新できません。");
			}
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
}

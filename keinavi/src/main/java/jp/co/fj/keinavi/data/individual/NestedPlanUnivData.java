package jp.co.fj.keinavi.data.individual;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.date.ExShortDatePlusUtil;
import jp.co.fj.keinavi.util.db.RecordProcessor;

import org.apache.commons.lang.StringUtils;

/**
 *
 * 受験予定大学一覧情報用データクラス
 *
 * 2004.08.20	Keisuke KONDO - Totec
 * 				新規作成
 *
 * <2010年度改修>
 * 2009.10.02	Tomohisa YAMADA - Totec
 * 				入試日程変更対応
 *
 * 2009.12.03	Shoji HASE - Totec
 *				UNIVDIVCD(１桁)→UNIDIV（２桁）変更にともない、条件分岐定数の変更
 *
 * 2016.01.15   Hiroyuki Nishiyama - QuiQsoft
 *              大規模改修：ソート順変更
 *
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 */
public class NestedPlanUnivData implements Comparable{

	//共：大学コード
	private String univCd;
	//共：大学名
	private String univNameAbbr;
	//共：学部コード
	private String facultyCd;
	//共：学部
	private String facultyNameAbbr;
	//共：学科コード
	private String deptCd;
	//共：学科
	private String deptNameAbbr;
	//日程コード(１はセンター)
	private String scheduleCd;
	//予：入試形態コード 01:一般 02:推薦 03:AO 04:特奨・給費生 05:その他
	private String entExamTypeCd;
	//予：入試形態名
	private String entExamOdeName;
	//共：年度
	private String year;
	//共：模試区分
	private String examDiv;
	//学科ソートキー
	private String deptSortKey;

	//ガイドライン
	private String guideLine;
	//区分　１：１：１の備考
	private String remarks;
	//大学区分コード
	private String univDivCd;
	//本部県コード
	private String prefCdHq;
	//「統合・分割」:true;
	private boolean isUnited;

	/* 2016/01/15 QQ)Nishiyama 大規模改修 ADD START */
	private String kanaNum;                // カナ付50音順番号
	private String uniNightDiv;            // 大学夜間部区分
	private String facultyConCd;           // 学部内容コード
	private String deptSerialNo;           // 学科通番
	private String scheduleSys;            // 日程方式
	private String scheduleSysBranchCd;    // 日程方式枝番
	private String deptNameKana;           // 学科カナ名
	/* 2016/01/15 QQ)Nishiyama 大規模改修 ADD END */

	public String getEntExamInpleDateStrFull() {
		return ScheduleDetailData.toFullString(examInpleDateList, univDivCd);
	}

	/* (非 Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 * 大学・学部・学科ソートキー・入試形態の順
	 */
	public int compareTo(Object o) {
		NestedPlanUnivData d2 = (NestedPlanUnivData) o;

		// 2016/01/15 QQ)Nishiyama 大規模改修 UPD START
		/*
		if(Integer.parseInt(this.univCd) < Integer.parseInt(d2.getUnivCd())){
			return -1;
		}else if(Integer.parseInt(this.univCd) > Integer.parseInt(d2.getUnivCd())){
			return 1;
		}else{
			if(Integer.parseInt(this.facultyCd) < Integer.parseInt(d2.getFacultyCd())){
				return -1;
			}else if(Integer.parseInt(this.facultyCd) > Integer.parseInt(d2.getFacultyCd())){
				return 1;
			}else{
				if(this.deptSortKey.compareTo(d2.getDeptSortKey()) < 0){
					return -1;
				}else if(this.deptSortKey.compareTo(d2.getDeptSortKey()) > 0){
					return 1;
				}else{
					if(Integer.parseInt(this.entExamTypeCd) < Integer.parseInt(d2.getEntExamTypeCd())){
						return -1;
					}else if(Integer.parseInt(this.entExamTypeCd) > Integer.parseInt(d2.getEntExamTypeCd())){
						return 1;
					}else{
						return 0;
					}
				}
			}
		}
		*/
		int retVal = 0;

		// 大学のソート
		retVal = this.sortUniv(d2);
		if (retVal != 0) {
			return retVal;
		}

		// 学部のソート
		retVal = this.sortFaculty(d2);
		if (retVal != 0) {
			return retVal;
		}

		// 学科のソート
		retVal = this.sortDept(d2);
		if (retVal != 0) {
			return retVal;
		}

		// 入試形態
		String entExamTypeCd1 = this.entExamTypeCd;
		String entExamTypeCd2 = d2.getEntExamTypeCd();
		retVal = this.strCompare(entExamTypeCd1, entExamTypeCd2);

		return retVal;
		// 2016/01/15 QQ)Nishiyama 大規模改修 UPD END
	}

	// 2016/01/15 QQ)Nishiyama 大規模改修 ADD START
	private int sortUniv (NestedPlanUnivData d2) {

		int comVal = 0;

		// 1.大学区分
		String uniDiv1 = this.univDivCd;
		String uniDiv2 = d2.getUnivDivCd();

		// 01：国立 を 02：公立 に見立てる
		if ("01".equals(uniDiv1)) uniDiv1 = "02";
		if ("01".equals(uniDiv2)) uniDiv2 = "02";

		comVal = this.strCompare(uniDiv1, uniDiv2);
		if (comVal != 0) {
			return comVal;
		}

		// 2.カナ付50音順番号
		comVal = this.strCompare(this.kanaNum, d2.getKanaNum());
		if (comVal != 0) {
			return comVal;
		}

		// 3.大学コード
		comVal = this.strCompare(this.univCd, d2.getUnivCd());
		if (comVal != 0) {
			return comVal;
		}

		return 0;
	}

	//
	// 学部のソート
	//
	private int sortFaculty (NestedPlanUnivData d2) {

		int comVal = 0;

		// 1.大学夜間部区分
		comVal = this.strCompare(this.uniNightDiv, d2.getUniNightDiv());
		if (comVal != 0) {
			return comVal;
		}

		// 2.学部内容コード
		comVal = this.strCompare(this.facultyConCd, d2.getFacultyConCd());
		if (comVal != 0) {
			return comVal;
		}

		return 0;
	}

	//
	// 学科のソート
	//
	private int sortDept (NestedPlanUnivData d2) {

		int comVal = 0;

		// 1.大学グループ区分（日程コード）
		comVal = this.strCompare(this.scheduleCd, d2.getScheduleCd());
		if (comVal != 0) {
			return comVal;
		}

		// 2.学科通番
		comVal = this.strCompare(this.deptSerialNo, d2.getDeptSerialNo());
		if (comVal != 0) {
			return comVal;
		}

		// 3.日程方式
		comVal = this.strCompare(this.scheduleSys, d2.getScheduleSys());
		if (comVal != 0) {
			return comVal;
		}

		// 4.日程方式枝番
		comVal = this.strCompare(this.scheduleSysBranchCd, d2.getScheduleSysBranchCd());
		if (comVal != 0) {
			return comVal;
		}

		// 5.学科カナ名
		comVal = this.strCompare(this.deptNameKana, d2.getDeptNameKana());
		if (comVal != 0) {
			return comVal;
		}

		// 6.学科コード
		comVal = this.strCompare(this.deptCd, d2.getDeptCd());
		if (comVal != 0) {
			return comVal;
		}

		return 0;
	}

	//
	// 文字列比較（Nullは空白に変換して比較）
	//
	private int strCompare (String str1, String str2) {

		if (StringUtils.isEmpty(str1)) str1 = "";
		if (StringUtils.isEmpty(str2)) str2 = "";

		return str1.compareTo(str2);
	}

	// 2016/01/15 QQ)Nishiyama 大規模改修 ADD END

	//inpledDataDivDataを区分でまとめたマップ
	private Map divDataMap;
	public int getDivDataMapSize(){
		int size = 0;
		if(getDivDataMap() != null)
			size = getDivDataMap().size();
		return size;
	}

	//入試日詳細より
	private List examInpleDateList;
	public int getExamInpleDateListSize(){
		int size = 0;
		if(getExamInpleDateList() != null)
			size = getExamInpleDateList().size();
		return size;
	}
	//入試予定より
	private List examPlanDateList;
	public int getExamPlanDateListSize(){
		int size = 0;
		if(getExamPlanDateList() != null)
			size = getExamPlanDateList().size();
		return size;
	}
	//JSP側で表示がしやすいので
	private List inpleDateDivDataList = new ArrayList();
	public int getInpleDateDivDataSize(){
		int size = 0;
		if(getInpleDateDivDataList() != null)
			size = getInpleDateDivDataList().size();
		return size;
	}
	//同上:こちらは受験予定大学用（InpleDateDivDataを使用）
	private List planDateDivDataList = new ArrayList();
	public int getPlanDateDivDataSize(){
		int size = 0;
		if(getPlanDateDivDataList() != null)
			size = getPlanDateDivDataList().size();
		return size;
	}

	/**
	 * 入試形態のユニークなキー(カンマ区切り)
	 * 大学・学部・学科・入試形態
	 * @return
	 */
	public String getPlanUnivDataKey(){
		return getUnivCd()+","+getFacultyCd()+","+getDeptCd()+","+getEntExamTypeCd();
	}

	/**
	 * 大学のユニークなキー
	 * 大学・学部・学科
	 * @return
	 */
	public String getUniqueKey(){
		return univCd+facultyCd+deptCd;
	}

	/**
	 * @return 私大日程URL
	 */
	public String getShidaiNitteiUrl() {

		// 私大でなければここまで
		if (!"03".equals(getUnivDivCd())) {
			return "";
		}

		final StringBuffer buff = new StringBuffer(
				KNCommonProperty.getShidaiNitteiUrl());

		// URL設定がなければここまで
		if (buff.length() == 0) {
			return "";
		}

		// 大学コード.html
		buff.append(getUnivCd());
		//buff.append(".html");

		return buff.toString();
	}

	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getDeptNameAbbr() {
		return deptNameAbbr;
	}

	/**
	 * @return
	 */
	public String getEntExamOdeName() {
		return entExamOdeName;
	}

	/**
	 * @return
	 */
	public String getEntExamTypeCd() {
		return entExamTypeCd;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getFacultyNameAbbr() {
		return facultyNameAbbr;
	}

	/**
	 * @return
	 */
	public boolean isUnited() {
		return isUnited;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @return
	 */
	public String getUnivNameAbbr() {
		return univNameAbbr;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setDeptNameAbbr(String string) {
		deptNameAbbr = string;
	}

	/**
	 * @param string
	 */
	public void setEntExamOdeName(String string) {
		entExamOdeName = string;
	}

	/**
	 * @param string
	 */
	public void setEntExamTypeCd(String string) {
		entExamTypeCd = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyNameAbbr(String string) {
		facultyNameAbbr = string;
	}

	/**
	 * @param b
	 */
	public void setUnited(boolean b) {
		isUnited = b;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

	/**
	 * @param string
	 */
	public void setUnivNameAbbr(String string) {
		univNameAbbr = string;
	}

	/**
	 * @return
	 */
	public List getExamInpleDateList() {
		return examInpleDateList;
	}

	/**
	 * @param collection
	 */
	public void setExamInpleDateList(List collection) {
		examInpleDateList = collection;
	}

	/**
	 * @return
	 */
	public List getExamPlanDateList() {
		return examPlanDateList;
	}

	/**
	 * @param collection
	 */
	public void setExamPlanDateList(List collection) {
		examPlanDateList = collection;
	}

	/**
	 * @return
	 */
	public List getInpleDateDivDataList() {
		return inpleDateDivDataList;
	}

	/**
	 * @param list
	 */
	public void setInpleDateDivDataList(List list) {
		inpleDateDivDataList = list;
	}

	/**
	 * @return
	 */
	public List getPlanDateDivDataList() {
		return planDateDivDataList;
	}

	/**
	 * @param list
	 */
	public void setPlanDateDivDataList(List list) {
		planDateDivDataList = list;
	}

	/**
	 * @return
	 */
	public String getGuideLine() {
		return guideLine;
	}

	/**
	 * @param string
	 */
	public void setGuideLine(String string) {
		guideLine = string;
	}

	/**
	 * @return
	 */
	public String getExamDiv() {
		return examDiv;
	}

	/**
	 * @return
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param string
	 */
	public void setExamDiv(String string) {
		examDiv = string;
	}

	/**
	 * @param string
	 */
	public void setYear(String string) {
		year = string;
	}

	/**
	 * @return
	 */
	public Map getDivDataMap() {
		return divDataMap;
	}

	/**
	 * @param map
	 */
	public void setDivDataMap(Map map) {
		divDataMap = map;
	}

	/**
	 * @return
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param string
	 */
	public void setRemarks(String string) {
		remarks = string;
	}


	/**
	 * @return
	 */
	public String getDeptSortKey() {
		return deptSortKey;
	}

	/**
	 * @param string
	 */
	public void setDeptSortKey(String string) {
		deptSortKey = string;
	}

	/**
	 * @return prefCdHq を戻します。
	 */
	public String getPrefCdHq() {
		return prefCdHq;
	}

	/**
	 * @param pPrefCdHq 設定する prefCdHq。
	 */
	public void setPrefCdHq(String pPrefCdHq) {
		prefCdHq = pPrefCdHq;
	}

	/**
	 * @return univDivCd を戻します。
	 */
	public String getUnivDivCd() {
		return univDivCd;
	}

	/**
	 * @param pUnivDivCd 設定する univDivCd。
	 */
	public void setUnivDivCd(String pUnivDivCd) {
		univDivCd = pUnivDivCd;
	}

	public String getScheduleCd() {
		return scheduleCd;
	}

	public void setScheduleCd(String scheduleCd) {
		this.scheduleCd = scheduleCd;
	}

	// 2016/01/15 QQ)Nishiyama 大規模改修 ADD START
	/**
	 * カナ付50音順番号を取得する
	 *
	 * @return カナ付50音順番号
	 * */
	 public String getKanaNum() {
	    return kanaNum;
	 }	/**

	 * カナ付50音順番号を設定する
	 *
	 * @param kanaNum カナ付50音順番号
	 * */
	 public void setKanaNum(String kanaNum) {
	    this.kanaNum = kanaNum;
	 }

	/**
	 * 大学夜間部区分を取得する
	 *
	 * @return 大学夜間部区分
	 * */
	 public String getUniNightDiv() {
	    return uniNightDiv;
	 }

	/**
	 * 大学夜間部区分を設定する
	 *
	 * @param uniNightDiv 大学夜間部区分
	 * */
	 public void setUniNightDiv(String uniNightDiv) {
	    this.uniNightDiv = uniNightDiv;
	 }

	/**
	 * 学部内容コードを取得する
	 *
	 * @return 学部内容コード
	 * */
	 public String getFacultyConCd() {
	    return facultyConCd;
	 }

	/**
	 * 学部内容コードを設定する
	 *
	 * @param facultyConCd 学部内容コード
	 * */
	 public void setFacultyConCd(String facultyConCd) {
	    this.facultyConCd = facultyConCd;
	 }

	/**
	 * 学科通番を取得する
	 *
	 * @return 学科通番
	 * */
	 public String getDeptSerialNo() {
	    return deptSerialNo;
	 }

	/**
	 * 学科通番を設定する
	 *
	 * @param deptSerialNo 学科通番
	 * */
	 public void setDeptSerialNo(String deptSerialNo) {
	    this.deptSerialNo = deptSerialNo;
	 }

	/**
	 * 日程方式を取得する
	 *
	 * @return 日程方式
	 * */
	 public String getScheduleSys() {
	    return scheduleSys;
	 }

	/**
	 * 日程方式を設定する
	 *
	 * @param scheduleSys 日程方式
	 * */
	 public void setScheduleSys(String scheduleSys) {
	    this.scheduleSys = scheduleSys;
	 }

	/**
	 * 日程方式枝番を取得する
	 *
	 * @return 日程方式枝番
	 * */
	 public String getScheduleSysBranchCd() {
	    return scheduleSysBranchCd;
	 }

	/**
	 * 日程方式枝番を設定する
	 *
	 * @param scheduleSysBranchCd 日程方式枝番
	 * */
	 public void setScheduleSysBranchCd(String scheduleSysBranchCd) {
	    this.scheduleSysBranchCd = scheduleSysBranchCd;
	 }

	/**
	 * 学科カナ名を取得する
	 *
	 * @return 学科カナ名
	 */
	 public String getDeptNameKana() {
	    return deptNameKana;
	 }

	/**
	 * 学科カナ名を設定する
	 *
	 * @param deptNameKana 学科カナ名
	 * */
	 public void setDeptNameKana(String deptNameKana) {
	    this.deptNameKana = deptNameKana;
	 }
	// 2016/01/15 QQ)Nishiyama 大規模改修 ADD END

	public class ExamInpleDateData extends ScheduleDetailData implements Comparable{

		private boolean isRegistered;

		//予：備考
		private String remarks;

		private boolean isUnited;

		public ExamInpleDateData(
	            Date entExamInpleDate11,
	            Date entExamInpleDate12,
	            Date entExamInpleDate13,
	            Date entExamInpleDate14,
	            String inpleDate11,
	            String inpleDate12,
	            String inpleDate13,
	            Date entExamInpleDate21,
	            Date entExamInpleDate22,
	            Date entExamInpleDate23,
	            Date entExamInpleDate24,
	            String inpleDate21,
	            String inpleDate22,
	            String inpleDate23,
	            String union,
	            String branchName,
				int div1,
				int div2,
				int div3) throws ParseException {
			super(
					entExamInpleDate11,
		            entExamInpleDate12,
		            entExamInpleDate13,
		            entExamInpleDate14,
		            inpleDate11,
		            inpleDate12,
		            inpleDate13,
		            entExamInpleDate21,
		            entExamInpleDate22,
		            entExamInpleDate23,
		            entExamInpleDate24,
		            inpleDate21,
		            inpleDate22,
		            inpleDate23,
		            union,
		            branchName,
					div1,
					div2,
					div3);
		}

		/**
		 * 日程情報からユニークな日付の文字列配列を算出して取得
		 * @return
		 * @throws ParseException
		 */
		public Date[] getEntExamInpleUniqueDate() throws Exception {
			return toUniqueDates();
		}

		/**
		 * ソート用比較
		 * 入試区分３・２・１の順
		 */
		public int compareTo(Object o) {

			ExamInpleDateData d2 = (ExamInpleDateData) o;

			if(getEntExamDiv_1_2term() < d2.getEntExamDiv_1_2term()){
				return -1;
			}else if(getEntExamDiv_1_2term() > d2.getEntExamDiv_1_2term()){
				return 1;
			}else{
				if(getSchoolProventDiv() < d2.getSchoolProventDiv()){
					return -1;
				}else if(getSchoolProventDiv() > d2.getSchoolProventDiv()){
					return 1;
				}else{
					if(getEntExamDiv_1_2order() < d2.getEntExamDiv_1_2order()){
						return -1;
					}else if(getEntExamDiv_1_2order() > d2.getEntExamDiv_1_2order()){
						return 1;
					}else{
						return 0;
					}
				}
			}
		}

		/**
		 * ExamInpleDateDataのデータを一意にするキー
		 * string[0] = 大学コード
		 * string[1] = 学部コード
		 * string[2] = 学科コード
		 * @return
		 */
		public String getUnivValue(){
			StringBuffer buf = new StringBuffer();
			buf.append(getPlanUnivDataKey());
			buf.append(",");
			buf.append(getEntExamDiv_1_2order());
			buf.append(",");
			buf.append(getSchoolProventDiv());
			buf.append(",");
			buf.append(getEntExamDiv_1_2term());
			return buf.toString();
		}

		/**
		 * @return
		 */
		public String getEntExamInpleDateStr() {
			return toAppendedDiv(univDivCd);
		}

		/**
		 * @return
		 */
		public String getRemarks() {
			return remarks;
		}

		/**
		 * @param string
		 */
		public void setRemarks(String string) {
			remarks = string;
		}

		/**
		 * @return
		 */
		public boolean isRegistered() {
			return isRegistered;
		}

		/**
		 * @param b
		 */
		public void setRegistered(boolean b) {
			isRegistered = b;
		}

		/**
		 * @return
		 */
		public boolean isUnited() {
			return isUnited;
		}

		/**
		 * @param b
		 */
		public void setUnited(boolean b) {
			isUnited = b;
		}
	}

	public class ExamPlanDateData extends ScheduleDetailData implements Comparable {

		//予：地方試験地1〜10
		private String[] provEntExamSite;
		//予：受験予定日1〜10
		private String[] examPlanDates;

		public ExamPlanDateData(int div1, int div2, int div3) {
			super(div1, div2, div3);
		}

		public String getEntExamDivKey(){
			return Integer.toString(entExamDiv_1_2term)
			+Integer.toString(schoolProventDiv)
			+Integer.toString(entExamDiv_1_2order);
		}

		/**
		 * @return 入試区分１の名称
		 */
		public String getEntExamDiv1Name() {
			return ScheduleDetailData.orderToName(entExamDiv_1_2order);
		}

		/**
		 * @return 入試区分２の名称
		 */
		public String getEntExamDiv2Name() {
			return ScheduleDetailData.provToName(univDivCd, schoolProventDiv);
		}

		/**
		 * @return 入試区分２の名称（編集画面用）
		 */
		public String getDetailEntExamDiv2Name() {
			return ScheduleDetailData.provToShortName(univDivCd, schoolProventDiv);
		}


		/* (非 Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 * 入試区分３・２・１の順にソート
		 */
		public int compareTo(Object o) {
			NestedPlanUnivData.ExamPlanDateData d2 = (NestedPlanUnivData.ExamPlanDateData) o;
			if(this.entExamDiv_1_2term < d2.getEntExamDiv_1_2term()){
				return -1;
			}else if(this.entExamDiv_1_2term > d2.getEntExamDiv_1_2term()){
				return 1;
			}else{
				if(this.schoolProventDiv < d2.getSchoolProventDiv()){
					return -1;
				}else if(this.schoolProventDiv > d2.getSchoolProventDiv()){
					return 1;
				}else{
					if(this.entExamDiv_1_2order < d2.getEntExamDiv_1_2order()){
						return -1;
					}else if(this.entExamDiv_1_2order > d2.getEntExamDiv_1_2order()){
						return 1;
					}else{
						return 0;
					}
				}
			}
		}

		/**
		 * @return
		 */
		public String[] getProvEntExamSite() {
			return provEntExamSite;
		}

		/**
		 * @param strings
		 */
		public void setProvEntExamSite(String[] strings) {
			provEntExamSite = strings;
		}

		/**
		 * @return
		 */
		public String[] getExamPlanDates() {
			return examPlanDates;
		}

		/**
		 * @param strings
		 */
		public void setExamPlanDates(String[] strings) {
			examPlanDates = strings;
		}
	}

	public class InpleDateDivData {

		//入試１期２期区分（ENTEXAMDIV3）
		protected int entExamDiv_1_2term;
		//入試１次２次区分（ENTEXAMDIV1）
		protected int entExamDiv_1_2order;
		//入試本学地方区分（ENTEXAMDIV2）
		protected int schoolProventDiv;

		private String univDivCd;

		//他：相関を使って実地日を分割した文字列
		private String entExamInpleUniqueDate;
		//予：地方試験地
		private String provEntExamSite;

		//受験予定日のデータかどうか
		private boolean isRegistered;
		private boolean isUnited;
		//入試日程と受験予定日のデータかどうか
		private boolean isInplePlan;
		//入試日程のデータかどうか
		private boolean isInple;
		//受験予定のデータかどうか
		private boolean isPlan;

		private String year;
		private String examDiv;
		private String univ;
		private String faculty;
		private String dept;
		private String entExamTypeCode;

		private String branchName;

		public InpleDateDivData(
				String year,
				String examDiv,
				String univcd,
				String facultycd,
				String deptcd,
				String entExamTypeCode,
				String univDivCode,
				String branchName,
				int div1,
				int div2,
				int div3) {

			this.year = year;
			this.examDiv = examDiv;
			this.univ = univcd;
			this.faculty = facultycd;
			this.dept = deptcd;
			this.entExamTypeCode = entExamTypeCode;
			this.univDivCd = univDivCode;

			this.branchName = branchName;

			entExamDiv_1_2term = div3;
			entExamDiv_1_2order = div1;
			schoolProventDiv = div2;
		}

		public String getBranchName() {
			return branchName;
		}

		public String getEntExamDivKey(){

			return Integer.toString(entExamDiv_1_2term)
			+Integer.toString(schoolProventDiv)
			+Integer.toString(entExamDiv_1_2order);
		}

		/**
		 * @return 入試区分１の名称
		 */
		public String getEntExamDiv_1_2orderName() {
			return ScheduleDetailData.orderToName(entExamDiv_1_2order);
		}

		/**
		 * @return 入試区分２の名称
		 */
		public String getSchoolProventDivName() {
			return ScheduleDetailData.provToName(univDivCd, schoolProventDiv);
		}

		/**
		 * @return 入試区分２の名称（編集画面用）
		 */
		public String getDetailSchoolProventDivName() {
			return ScheduleDetailData.provToShortName(univDivCd, schoolProventDiv);
		}

		/**
		 * このInpleDateDivDataを一意にするキー
		 * @return
		 */
		public String getDateDivKey(){

			StringBuffer buf = new StringBuffer();

			buf.append(univ);
			buf.append(",");
			buf.append(faculty);
			buf.append(",");
			buf.append(dept);
			buf.append(",");
			buf.append(entExamTypeCode);
			buf.append(",");
			buf.append(entExamDiv_1_2order);
			buf.append(",");
			buf.append(schoolProventDiv);
			buf.append(",");
			buf.append(entExamDiv_1_2term);
			buf.append(",");
			buf.append(getEntExamInpleUniqueDate());
			buf.append(",");
			buf.append(year);
			buf.append(",");
			buf.append(examDiv);

			return buf.toString();
		}

		/**
		 * @return
		 */
		public String getEntExamInpleUniqueDate() {
			return entExamInpleUniqueDate;
		}

		/**
		 * @param string
		 */
		public void setEntExamInpleUniqueDate(String string) {
			entExamInpleUniqueDate = string;
		}

		/**
		 * @return
		 */
		public String getProvEntExamSite() {
			return provEntExamSite;
		}

		/**
		 * @param string
		 */
		public void setProvEntExamSite(String string) {
			provEntExamSite = string;
		}

		/**
		 * @return
		 */
		public String getEntExamInpleDate() {
			return RecordProcessor.trimZero(RecordProcessor.getDateDigit(ExShortDatePlusUtil.getUnformattedString(entExamInpleUniqueDate)));
		}

		/**
		 * @return
		 */
		public String getEntExamInpleMonth() {
			return RecordProcessor.trimZero(ScheduleDetailData.getMonthDigit(ExShortDatePlusUtil.getUnformattedString(entExamInpleUniqueDate)));
		}

		/**
		 * @return
		 */
		public boolean isRegistered() {
			return isRegistered;
		}

		/**
		 * @param b
		 */
		public void setRegistered(boolean b) {
			isRegistered = b;
		}

		/**
		 * @return
		 */
		public boolean isUnited() {
			return isUnited;
		}

		/**
		 * @param b
		 */
		public void setUnited(boolean b) {
			isUnited = b;
		}

		/**
		 * @return
		 */
		public boolean isInple() {
			return isInple;
		}

		/**
		 * @return
		 */
		public boolean isInplePlan() {
			return isInplePlan;
		}

		/**
		 * @return
		 */
		public boolean isPlan() {
			return isPlan;
		}

		/**
		 * @param b
		 */
		public void setInple(boolean b) {
			isInple = b;
		}

		/**
		 * @param b
		 */
		public void setInplePlan(boolean b) {
			isInplePlan = b;
		}

		/**
		 * @param b
		 */
		public void setPlan(boolean b) {
			isPlan = b;
		}

		public int getEntExamDiv_1_2term() {
			return entExamDiv_1_2term;
		}

		public int getEntExamDiv_1_2order() {
			return entExamDiv_1_2order;
		}

		public int getSchoolProventDiv() {
			return schoolProventDiv;
		}


	}

}
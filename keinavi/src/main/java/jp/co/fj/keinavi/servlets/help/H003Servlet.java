/*
 * 作成日: 2004/09/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.help;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.beans.help.HelpListBean;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.forms.help.H003Form;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class H003Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(

		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ

		// セッション
		HttpSession session = request.getSession();

		// ログイン情報
		LoginSession login = super.getLoginSession(request);

		// アクションフォーム
		H003Form form = null;
		// requestから取得する
		try {
			form = (H003Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.help.H003Form");
		} catch (Exception e) {
			throw new ServletException(e);
		}

	String formId = getForward(request);
		if(("h003").equals(getForward(request))) {
			Connection con = null; // コネクション
			try {
				con = super.getConnectionPool(request); 	// コネクション取得
				HelpListBean bean = new HelpListBean();
				bean.setFormId(getForward(request));		// ヘルプ詳細
				bean.setHelpId(form.getHelpId());
				bean.setCategory(form.getCategory());
				bean.setFreeWord(form.getFreeWord());
				bean.setUseful(form.getUseful());
				bean.setComment(form.getComment());
				bean.setName(form.getName());
				bean.setDetailBack(form.getDetailBack());
				bean.setPage(form.getPage());
				bean.setConnection(null, con);
				bean.execute();
	
				request.setAttribute("HelpListBean", bean);
				//	アクセスログ
				actionLog(request, "2002", form.getHelpId());
			} catch (Exception e) {
				throw new ServletException(e.toString());
			} finally {
				super.releaseConnectionPool(request, con); // コネクション解放
			}
	
			// アクションフォーム
			request.setAttribute("form", form);
			super.forward(request, response, JSP_H003);

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

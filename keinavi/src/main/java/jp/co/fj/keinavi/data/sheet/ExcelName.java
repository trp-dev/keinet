package jp.co.fj.keinavi.data.sheet;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.totec.config.ConfigResolver;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 *
 * エクセルのファイル名を管理するクラス
 *
 * 2006.07.24	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class ExcelName {

	// singleton
	private static final ExcelName instance = new ExcelName();
	// 帳票ID→ファイル名変換マップ
	private static final Map map = new HashMap();

	static {

// MOD START 2019/07/26 QQ)K.Hisakawa 共通テスト対応
//		final Configuration config = ConfigResolver.getInstance(
//				).getConfiguration("excelname.properties");

	        PropertiesConfiguration  config = new PropertiesConfiguration();

                try {
                    config.setEncoding("UTF-8");
                    config.setURL(ConfigResolver.getInstance().findResource("excelname.properties"));
                    config.load();

                } catch (ConfigurationException e) {
                    // 2019/10/16 QQ)Ooseto 共通テスト対応 ADD START
                    KNLog log = KNLog.getInstance(null,null,null);
                    log.Err("","エクセルファイル名取得エラー",e.toString());
                    // 2019/10/16 QQ)Ooseto 共通テスト対応 ADD END
                }
 // MOD END   2019/07/26 QQ)K.Hisakawa 共通テスト対応

		for (final Iterator ite = config.getKeys(); ite.hasNext();) {
			final String key = (String) ite.next();
			map.put(key, config.getString(key));
		}

	}

	private ExcelName() {
	}

	/**
	 * @return instance を戻します。
	 */
	public static ExcelName getInstance() {
		return instance;
	}

	/**
	 * @param id 帳票ID
	 * @return エクセルファイル名
	 */
	public String getName(final String id) {

		if (map.containsKey(id)) {
			return (String) map.get(id);
		} else {
			return "unknown";
		}
	}

}

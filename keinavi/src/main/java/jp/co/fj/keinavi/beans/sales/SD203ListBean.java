package jp.co.fj.keinavi.beans.sales;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.data.sales.SD203Data;
import jp.co.fj.keinavi.interfaces.SpecialAppliStatus;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 特例成績データ作成承認画面のリストBeanです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD203ListBean extends DefaultBean {

	/** serialVersionUID */
	private static final long serialVersionUID = -7448626524746030074L;

	/** 申請状態 */
	private final SpecialAppliStatus status;

	/** ソートキー */
	private final String sortKey;

	/** 所属する部門コードの配列 */
	private final String[] sectorCdArray;

	/** データリスト */
	private final List results = new ArrayList();

	/**
	 * コンストラクタです。
	 */
	public SD203ListBean(SpecialAppliStatus status, String sortKey,
			String[] sectorCdArray) {
		this.status = status;
		this.sortKey = sortKey;
		this.sectorCdArray = sectorCdArray;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(createQuery());
			ps.setString(1, status.code);

			rs = ps.executeQuery();

			SD203Data data = null;
			while (rs.next()) {

				if (data == null || !data.getAppId().equals(rs.getString(1))) {
					data = new SD203Data();
					data.setAppId(rs.getString(1));
					data.setExamYear(rs.getString(2));
					data.setExamName(rs.getString(3));
					data.setSchoolCd(rs.getString(4));
					data.setSchoolName(rs.getString(5));
					data.setPactDivName(rs.getString(6));
					data.setTeacherName(rs.getString(7));
					data.setAppComment(rs.getString(8));
					data.setAppDate(rs.getTimestamp(9));
					results.add(data);
				}

				data.getSectorNameList().add(rs.getString(10));
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * SQLを生成します。
	 */
	private String createQuery() throws SQLException {
		Query query = QueryLoader.getInstance().load("sd203_list");
		query.setStringArray(1, sectorCdArray);
		query.append(createOrderBy());
		return query.toString();
	}

	/**
	 * ORDER BY句を生成します。
	 */
	private String createOrderBy() {

		if ("1".equals(sortKey)) {
			/* 対象年度 */
			return "EXAMYEAR, APP_DATE, BUNDLECD, APPLICATION_ID, SECTORNAME";
		} else if ("2".equals(sortKey)) {
			/* 対象模試 */
			return "DISPSEQUENCE, EXAMCD, APP_DATE, BUNDLECD, APPLICATION_ID, SECTORNAME";
		} else if ("3".equals(sortKey)) {
			/* 申請営業部 */
			return "SECTORSORTKEY, APP_DATE, BUNDLECD, APPLICATION_ID, SECTORNAME";
		} else if ("4".equals(sortKey)) {
			/* 学校コード */
			return "BUNDLECD, APP_DATE, APPLICATION_ID, SECTORNAME";
		} else if ("5".equals(sortKey)) {
			/* 申請日時 */
			return "APP_DATE, BUNDLECD, APPLICATION_ID, SECTORNAME";
		} else {
			/* 不正なソートキーなら例外とする */
			throw new RuntimeException("ソートキーが不正です。" + sortKey);
		}
	}

	/**
	 * データリストを返します。
	 * 
	 * @return データリスト
	 */
	public List getResults() {
		return results;
	}

}

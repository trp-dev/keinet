/*
 * 作成日: 2004/09/24
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.sheet;

import jp.co.fj.keinavi.excel.data.school.S51TakouListBean;

/**
 * 校内成績分析・過回比較・成績概況
 * 他校データリスト
 * 
 * @author kawai
 */
public class ExtS51TakouListBean extends S51TakouListBean implements Comparable {

	private String dataOpenDate; // データ開放日
	private int schoolDispSeq; // 学校の表示順序
	private String examYear; // 模試年度

	/* (非 Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		// 学校の表示順の昇順・模試年度の降順・データ開放日の降順でソートする
		ExtS51TakouListBean obj = (ExtS51TakouListBean) o;

		if (this.getSchoolDispSeq() > obj.getSchoolDispSeq()) {
			return 1;

		} else if (this.getSchoolDispSeq() < obj.getSchoolDispSeq()) {
			return -1;

		} else {
			int result = obj.getExamYear().compareTo(this.getExamYear());
			
			if (result == 0) {
				return obj.getDataOpenDate().compareTo(this.getDataOpenDate());
				
			} else {
				return result;
			}
		}
	}

	/**
	 * @return
	 */
	public int getSchoolDispSeq() {
		return schoolDispSeq;
	}

	/**
	 * @param i
	 */
	public void setSchoolDispSeq(int i) {
		schoolDispSeq = i;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @return
	 */
	public String getDataOpenDate() {
		return dataOpenDate;
	}

	/**
	 * @param string
	 */
	public void setDataOpenDate(String string) {
		dataOpenDate = string;
	}

}

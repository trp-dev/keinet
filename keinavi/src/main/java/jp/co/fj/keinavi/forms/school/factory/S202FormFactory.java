/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.school.factory;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.forms.school.SMaxForm;
import com.fjh.forms.ActionForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class S202FormFactory extends AbstractSFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 * 成績概況
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		// アクションフォーム
		SMaxForm form = super.createSMaxForm(request);
		// アイテムマップ
		Map item = getItemMap(request);
		
		CommonFormFactory commForm = new CommonFormFactory(form, item);
		commForm.setForm0103();
		commForm.setForm0203();
		commForm.setForm0601();
		commForm.setForm0701();
		commForm.setForm0502();
		commForm.setForm1305();
		commForm.setFormat();
		
		return commForm.getForm();
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
//		SMaxForm form = (SMaxForm)request.getSession().getAttribute(SMaxForm.SESSION_KEY);
		SMaxForm form = (SMaxForm)request.getAttribute("form");
		// アイテムマップ
		Map item = getItemMap(request);
		
		CommonItemFactory commItem = new CommonItemFactory(form, item);
		commItem.setItem0103();
		commItem.setItem0203();
		commItem.setItem0601();
		commItem.setItem0701();
		commItem.setItem0502();
		commItem.setItem1305();
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap("020401");
	}

}

package jp.co.fj.keinavi.beans.graph;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * Zà¬ÑªÍ - ßñär - ¬ÑTµ
 * 
 * 2006.07.27	[VKì¬]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class S402GraphTargetBean extends S004GraphTargetBean {

	/**
	 * RXgN^
	 * 
	 * @param id vt@CJeSID
	 */
	public S402GraphTargetBean(final String id) {
		super(id);
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.GraphCheckerBeanAdapter#hasClassGraphSheet(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	public boolean hasClassGraphSheet(HttpServletRequest request) {

		// ßñENXär
		if (hasSubjectGraphSheet(request)
				&&isValidValue(request, PAST_COMP_CLASS)) {
			return true;
		}

		return false;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.GraphCheckerBeanAdapter#hasSchoolGraphSheet(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	public boolean hasSchoolGraphSheet(HttpServletRequest request) {
		
		// Z^[T[`ÍÎÛO
		if (KNUtil.isCenterResearch(getExamData(request))) {
			return false;
		}
		
		// ßñE¼Zär
		if (hasSubjectGraphSheet(request)
				&& isValidValue(request, PAST_COMP_OTHER)) {
			return true;
		}

		return false;
	}
	
}

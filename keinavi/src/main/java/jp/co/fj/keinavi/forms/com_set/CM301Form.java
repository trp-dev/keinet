package jp.co.fj.keinavi.forms.com_set;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM301Form extends BaseForm {

	// 比較対象年度
	private String[] compYear = null;
	// 前年度
	private String last1 = null;
	// 前々年度
	private String last2 = null;

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {

	}

	/**
	 * @return
	 */
	public String[] getCompYear() {
		return compYear;
	}

	/**
	 * @param strings
	 */
	public void setCompYear(String[] strings) {
		compYear = strings;
	}

	/**
	 * @return
	 */
	public String getLast1() {
		return last1;
	}

	/**
	 * @return
	 */
	public String getLast2() {
		return last2;
	}

	/**
	 * @param string
	 */
	public void setLast1(String string) {
		last1 = string;
	}

	/**
	 * @param string
	 */
	public void setLast2(String string) {
		last2 = string;
	}

}

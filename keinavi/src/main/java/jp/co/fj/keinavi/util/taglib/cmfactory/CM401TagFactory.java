/*
 * 作成日: 2004/07/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.taglib.cmfactory;

import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;

import jp.co.fj.keinavi.forms.com_set.CM401Form;


/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM401TagFactory extends AbstractCMTagFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.taglib.cmfactory.AbstractCMTagFactory#createCMStatus(javax.servlet.ServletRequest)
	 */
	public String createCMStatus(ServletRequest request, HttpSession session) throws JspException {
		String result = null;
		Map item = getItemMap(session);
		// 全選択
		if ("1".equals(item.get("0307").toString())) {
			result = getMessage("cm002d");
		// 個別選択
		} else {
			String[] univ = (String[])item.get("0301");
			if (univ == null || univ.length == 0) {
				result = "設定なし";
			} else {
				result = univ.length + "大学";
			}
		}
		return result;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.taglib.cmfactory.AbstractCMTagFactory#createTempCMStatus(javax.servlet.ServletRequest, javax.servlet.http.HttpSession)
	 */
	public String createTempCMStatus(ServletRequest request, HttpSession session) throws JspException {
		String result = null;
		CM401Form form = (CM401Form) super.getCommonForm(session).getActionForm("CM401Form");
		
		// 全選択
		if ("1".equals(form.getSelection())) {
			result = getMessage("cm002d");			
		// 個別選択
		} else {
			if (form.getUniv() == null || form.getUniv().length == 0) {
				result = "設定なし";
			} else {
				result = form.getUniv().length + "大学";
			}
		}
		
		return result;
	}
}

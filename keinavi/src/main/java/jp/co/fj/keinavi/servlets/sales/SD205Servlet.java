package jp.co.fj.keinavi.servlets.sales;

import jp.co.fj.keinavi.interfaces.SpecialAppliMenu;
import jp.co.fj.keinavi.interfaces.SpecialAppliStatus;
import jp.co.fj.keinavi.servlets.sales.base.BaseAcceptServlet;

/**
 * 
 * 特例成績データ作成承認（模試運用管理部）画面のサーブレットです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD205Servlet extends BaseAcceptServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = -2795657140795991443L;

	/**
	 * @see jp.co.fj.keinavi.servlets.sales.base.BaseSpecialAppliServlet#getMenu()
	 */
	protected SpecialAppliMenu getMenu() {
		return SpecialAppliMenu.ACCEPT_KANRI;
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.sales.base.BaseAcceptServlet#getUnAcceptedStatus()
	 */
	protected SpecialAppliStatus getUnAcceptedStatus() {
		return SpecialAppliStatus.PENDING_KANRI;
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.sales.base.BaseAcceptServlet#getAcceptedStatus()
	 */
	protected SpecialAppliStatus getAcceptedStatus() {
		return SpecialAppliStatus.ACCEPTED;
	}

}

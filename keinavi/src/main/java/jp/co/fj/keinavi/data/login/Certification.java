package jp.co.fj.keinavi.data.login;

import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;

import com.fjh.certificates.CertificateDName;

/**
 * 証明書データ
 * 
 * @author kawai
 */
public class Certification implements java.io.Serializable {

	private String certCN; // 証明書CN
	private String certOU; // 証明書OU
	private String certO; // 証明書O
	private String certL; // 証明書L
	private String certST; // 証明書ST
	private String certC; // 証明書C
	private String validDate; // 有効期限

	
	/**
	 * コンストラクタ
	 */
	public Certification() {
	}

	/**
	 * コンストラクタ
	 */
	public Certification(X509Certificate cert) {
		CertificateDName dname = new CertificateDName(cert.getSubjectDN().getName());
		
		this.setCertCN(dname.getValue("CN"));
		this.setCertOU(dname.getValue("OU"));
		this.setCertO(dname.getValue("O"));
		this.setCertL(dname.getValue("L"));
		this.setCertST(dname.getValue("ST"));
		this.setCertC(dname.getValue("C"));
	
		SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
	
		this.setValidDate(f.format(cert.getNotAfter()));
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		Certification cert = (Certification) obj;

		return this.getCertCN().equals(cert.getCertCN())
			&& this.getCertOU().equals(cert.getCertOU())
			&& this.getCertO().equals(cert.getCertO())
			&& this.getCertL().equals(cert.getCertL())
			&& this.getCertST().equals(cert.getCertST())
			&& this.getCertC().equals(cert.getCertC())
			&& this.getValidDate().equals(cert.getValidDate());

	}

	/* (非 Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "CN:" + this.getCertCN()
			+ "OU:" + this.getCertOU()
			+ "O:" + this.getCertO()
			+ "L:" + this.getCertL()
			+ "ST:" + this.getCertST()
			+ "C:" + this.getCertC()
			+ "ValidDate:" + this.getValidDate();
	}


	/**
	 * @return
	 */
	public String getCertC() {
		return certC;
	}

	/**
	 * @return
	 */
	public String getCertCN() {
		return certCN;
	}

	/**
	 * @return
	 */
	public String getCertL() {
		return certL;
	}

	/**
	 * @return
	 */
	public String getCertO() {
		return certO;
	}

	/**
	 * @return
	 */
	public String getCertOU() {
		return certOU;
	}

	/**
	 * @return
	 */
	public String getCertST() {
		return certST;
	}

	/**
	 * @param string
	 */
	public void setCertC(String string) {
		certC = string == null ? "" : string;
	}

	/**
	 * @param string
	 */
	public void setCertCN(String string) {
		certCN = string == null ? "" : string;
	}

	/**
	 * @param string
	 */
	public void setCertL(String string) {
		certL = string == null ? "" : string;
	}

	/**
	 * @param string
	 */
	public void setCertO(String string) {
		certO = string == null ? "" : string;
	}

	/**
	 * @param string
	 */
	public void setCertOU(String string) {
		certOU = string == null ? "" : string;
	}

	/**
	 * @param string
	 */
	public void setCertST(String string) {
		certST = string == null ? "" : string;
	}

	/**
	 * @return
	 */
	public String getValidDate() {
		return validDate;
	}

	/**
	 * @param string
	 */
	public void setValidDate(String string) {
		validDate = string;
	}

}
package jp.co.fj.keinavi.data.admin;

/**
 *
 * 受験届修正・コンボデータ
 * 
 * 2004.11.01	[新規作成]
 * 2006.10.06	リファクタリング
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class MExamData {

	// 模試年度
	private String examYear;
	// 模試コード
	private String examCd;
	// 模試名
	private String examName;
	// 学年
	private int grade;
	// クラス
	private String className;
	// 模試実施基準日
	private String inpleDate;
	// 表示順
	private int dispSequence;
	
	/**
	 * @return dispSequence
	 */
	public int getDispSequence() {
		return dispSequence;
	}
	/**
	 * @param pDispSequence 設定する dispSequence
	 */
	public void setDispSequence(int pDispSequence) {
		dispSequence = pDispSequence;
	}
	/**
	 * @return inpleDate
	 */
	public String getInpleDate() {
		return inpleDate;
	}
	/**
	 * @param pInpleDate 設定する inpleDate
	 */
	public void setInpleDate(String pInpleDate) {
		inpleDate = pInpleDate;
	}
	/**
	 * @return className を戻します。
	 */
	public String getClassName() {
		return className;
	}
	/**
	 * @param pClassName 設定する className。
	 */
	public void setClassName(String pClassName) {
		className = pClassName;
	}
	/**
	 * @return examCd を戻します。
	 */
	public String getExamCd() {
		return examCd;
	}
	/**
	 * @param pExamCd 設定する examCd。
	 */
	public void setExamCd(String pExamCd) {
		examCd = pExamCd;
	}
	/**
	 * @return examName を戻します。
	 */
	public String getExamName() {
		return examName;
	}
	/**
	 * @param pExamName 設定する examName。
	 */
	public void setExamName(String pExamName) {
		examName = pExamName;
	}
	/**
	 * @return examYear を戻します。
	 */
	public String getExamYear() {
		return examYear;
	}
	/**
	 * @param pExamYear 設定する examYear。
	 */
	public void setExamYear(String pExamYear) {
		examYear = pExamYear;
	}
	/**
	 * @return grade を戻します。
	 */
	public int getGrade() {
		return grade;
	}
	/**
	 * @param pGrade 設定する grade。
	 */
	public void setGrade(int pGrade) {
		grade = pGrade;
	}

}

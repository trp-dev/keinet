package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.keinavi.beans.SubRecordASearchBean;
import jp.co.fj.keinavi.beans.individual.DockedSubRecBean;
import jp.co.fj.keinavi.beans.individual.ExamSearchBean;
import jp.co.fj.keinavi.beans.individual.JudgeByAutoBean;
import jp.co.fj.keinavi.beans.individual.JudgeByCondBean;
import jp.co.fj.keinavi.beans.individual.JudgeByUnivBean;
import jp.co.fj.keinavi.beans.individual.JudgementStoreBean;
import jp.co.fj.keinavi.beans.individual.ProvisoSearchBean;
import jp.co.fj.keinavi.beans.individual.ProvisoStoreBean;
import jp.co.fj.keinavi.beans.individual.WishUnivSearchBean;
import jp.co.fj.keinavi.beans.individual.judgement.JudgementListBean;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.ProvisoData;
import jp.co.fj.keinavi.data.individual.StudentData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I204Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.judgement.beans.ScoreKeiNavi;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.db.RecordProcessor;
import jp.co.fj.keinavi.util.individual.IPropsLoader;
import jp.co.fj.keinavi.util.individual.JudgementSorterV2;

/**
 * 
 * 
 * 判定結果一覧表示
 * 
 * 
 * 2004.09.01	Tomohisa YAMADA - Totec 
 * 				新規作成
 * 
 * 2005.01.10	Tomohisa YAMADA - Totec 
 * 				[1.5]センターリサーチ対応
 * 
 * 2005.01.31	Tomohisa YAMADA - Totec 
 * 				[2]高２系模試も判定できるようにする
 * 
 * 2005.03.08		Tomohisa YAMADA - Totec 
 * 				[3]大学指定判定結果最大表示件数の変更２０→４０
 * 
 * 2005.04.08		Tomohisa YAMADA - Totec 
 * 				[4]ScoreBeanの[35]に伴う修正
 * 
 * <2010年度改修>
 * 2010.01.16	Tomohisa YAMADA - Totec
 * 				センターリサーチ5段階対応
 * 
 * 2010.04.08	Tomohisa YAMADA - Totec
 * 				数学�@の偏差値を動的に算出する
 * 
 *
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 * 
 * 
 */
public class I204Servlet extends IndividualServlet {
	
	/**
	 * @see jp.co.fj.keinavi.servlets.individual.IndividualServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			 javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(final HttpServletRequest request,
		final HttpServletResponse response)
		throws ServletException, IOException {

		super.execute(request, response);
		
		// フォームのデータを取得 ::::::::
		final I204Form form = (I204Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.individual.I204Form");
		
		// HTTPセッション
		final HttpSession session = request.getSession(false);
		// プロファイル情報
		final Profile profile = getProfile(request);
		//個人共通
		final ICommonMap iCommonMap = getICommonMap(request);
		//模試データ
		final ExamSession examSession = getExamSession(request);
		
		//印刷対象生徒（判定対象生徒）※結果一覧のページだけ固有のPRINT_STUDENTを使う
		final Short printStudent = (Short) profile.getItemMap(
				IProfileCategory.I_UNIV_RESULTS).get(IProfileItem.PRINT_STUDENT);
		final Short judgeStudent = (Short) profile.getItemMap(
				IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);
		//セキュリティスタンプ
		final Short printStamp = (Short) profile.getItemMap(
				IProfileCategory.I_UNIV_RESULTS).get(IProfileItem.PRINT_STAMP);

		// JSP転送
		if ("i204".equals(getForward(request))) {
			
			// 生徒を切り替えた
			if((!"i202".equals(getBackward(request)) && !"i203".equals(getBackward(request))
					&& !"i201".equals(getBackward(request)))
					|| !iCommonMap.getTargetPersonId().equals(form.getTargetPersonId())
					|| isChangedMode(request)){
				
				final Map judgedMap = BaseJudgeThread.getScreenJudgedMap(session);
				
				// 表示中の生徒の結果一覧を取得してセッションに入れなおす
				if (judgedMap.get(form.getTargetPersonId()) != null) {
					session.setAttribute(JudgementListBean.SESSION_KEY,
							judgedMap.get(form.getTargetPersonId()));
				// 判定を行っていない場合はisJudge=false(デフォルト)のJudgementListBeanを代入
				}else{
					JudgementListBean bean = new JudgementListBean();
					bean.setJudged(false);
					session.setAttribute(JudgementListBean.SESSION_KEY, bean);
				}
				
				request.setAttribute("form", form);
				iCommonMap.setTargetPersonId(form.getTargetPersonId());
				
				forward(request, response, JSP_I204);
				
			// 判定処理
			} else {
				// 対象生徒を保持する
				iCommonMap.setTargetPersonId(form.getTargetPersonId());
				
				// 判定処理スレッド起動
				new Thread(new JudgeThread(getDbKey(request), session, iCommonMap,
						form, judgeStudent.toString(), examSession, getUnivAll())).start();
				
				// 印刷対象生徒
				if (iCommonMap.isBunsekiMode()) {
					form.setJudgeStudent(printStudent.toString());
				}
				
				form.setPrintStamp(printStamp.toString());
				form.setTrueForward(getForward(request));
				form.setTrueBackward(getBackward(request));
				request.setAttribute("form", form);
				
				forward(request, response, JSP_IOnJudge);
			}			
			
		//Servlet転送
		} else {

			//フォームの値をプロファイルに保存
			if ("sheet".equals(getForward(request))) {
				
				if(iCommonMap.isBunsekiMode()){
					//個人成績分析共通-印刷対象生保存をセット
					profile.getItemMap(IProfileCategory.I_UNIV_RESULTS).put(
							IProfileItem.PRINT_STUDENT, new Short(form.getJudgeStudent()));
				}
				
				//4.セキュリティスタンプを保存
				profile.getItemMap(IProfileCategory.I_UNIV_RESULTS).put(
						IProfileItem.PRINT_STAMP, new Short(form.getPrintStamp()));
			}
			
			iCommonMap.setTargetPersonId(form.getTargetPersonId());
			dispatch(request, response);
		}
	}
	
	// 判定処理スレッド
	private class JudgeThread extends BaseJudgeThread {
		
		private final HttpSession session;
		private final ICommonMap iCommonMap;
		private final I204Form form;
		private final ExamSession examSession;
		private final String judgeStudent;
		private final List univAll;
		
		// コンストラクタ
		private JudgeThread(final String dbKey, final HttpSession pSession,
				final ICommonMap pICommonMap, final I204Form pForm,
				final String pJudgeStudent, final ExamSession pExamSession,
				final List pUnivAll){
			super(pSession, dbKey, false);
			session = pSession;
			iCommonMap = pICommonMap;
			form = pForm;
			judgeStudent = pJudgeStudent;
			examSession = pExamSession;
			univAll = pUnivAll;
		}
		
		/**
		 * @see jp.co.fj.keinavi.servlets.individual.BaseJudgeThread#execute(
		 * 			java.sql.Connection)
		 */
		protected Map execute(final Connection con) throws Exception {

			// 判定結果マップ
			final Map judgedMap = new HashMap();

			try {
				/** ステップ１：成績の取得 */
				final List scoreBeanList = new ArrayList();

				//対象模試情報の取得
				ExamSearchBean esb = new ExamSearchBean();
				esb.setExamYear(iCommonMap.getTargetExamYear());
				esb.setExamCd(iCommonMap.getTargetExamCode());
				esb.setConnection("", con);
				esb.execute();
				//絶対に取れるはずだから、最初のを取得
				ExaminationData targetExam = (ExaminationData) esb.getRecordSet().get(0);
					
				//成績取得
				final List selectedIndividuals = getSelectedIndividuals(
						iCommonMap, judgeStudent);
				if (selectedIndividuals != null && selectedIndividuals.size() > 0) {
					Iterator ite = selectedIndividuals.iterator();
					while(ite.hasNext()){
						StudentData sd = (StudentData) ite.next();
							
						//ドッキング先模試情報の取得
						DockedSubRecBean dsrb = new DockedSubRecBean();
						dsrb.setIndividualId(sd.getStudentPersonId());
						dsrb.setExamYear(iCommonMap.getTargetExamYear());
						dsrb.setExamCd(iCommonMap.getTargetExamCode());
						dsrb.setExamSession(examSession);
						dsrb.setConnection("", con);
						dsrb.execute();
							
						//ScoreBeanに取得してきた成績をセット
						Score sb = new ScoreKeiNavi(
								SubRecordASearchBean.searchNullSet(
										dsrb.getDockedBean().getExaminationDatas()[0], 
										con));
						
						//このスコアが男性のものかどうかの情報を付加する（女子大条件）
						if(sd.getStudentSex().equals(IPropsLoader.getInstance().getMessage("CODE_GENDER_MALE"))){
							sb.setMale(true);	
						}else if(sd.getStudentSex().equals(IPropsLoader.getInstance().getMessage("CODE_GENDER_FEMALE"))){
							sb.setMale(false);	
						}
						//選択対象模試情報を付加する（A,B,C判定の為）
						sb.setTargetExam(targetExam);

						sb.setIndividualId(sd.getStudentPersonId());
						sb.setMark(dsrb.getMark());
						sb.setWrtn(dsrb.getWrtn());
						if(dsrb.getDockedBean().getExaminationDatas()[0] != null){
							sb.setCExam(dsrb.getDockedBean().getExaminationDatas()[0]);//[0]はマーク模試
						}
						if(dsrb.getDockedBean().getExaminationDatas()[1] != null){
							sb.setSExam(dsrb.getDockedBean().getExaminationDatas()[1]);//[1]は記述模試
						}
						sb.execute();
						scoreBeanList.add(sb);
					}
				}
					
				/** ステップ２：判定 */
				/** 1. おまかせ判定 */
				if (form.getButton().equals("byAuto")) {
					List judgedList = new ArrayList();
					Iterator ite = scoreBeanList.iterator();
					while(ite.hasNext()){
						Score sb = (Score) ite.next();
							
						JudgementListBean listBean = new JudgementListBean();
						//マーク模試も記述模試も受けていないので判定を行わない
						if(sb.getMark() == null && sb.getWrtn() == null){
							listBean.setJudged(false);
							listBean.setExamTaken(false);
						}
						//マーク模試または記述模試どちらか一方でも受けているので判定する
						else{
							/** この生徒の志望の大学の分野と地区を取得 */
							WishUnivSearchBean wusb = new WishUnivSearchBean();
							wusb.setIndividualId(sb.getIndividualId());
							wusb.setExamYear(iCommonMap.getTargetExamYear());
							wusb.setExamCd(iCommonMap.getTargetExamCode());
							wusb.setConnection("", con);
							wusb.execute();
					
							/** おまかせ判定実行 */
							JudgeByAutoBean jbab = new JudgeByAutoBean();
							jbab.setUnivAllList(univAll);
							jbab.setWishUnivList(wusb.getRecordSet());
							jbab.setScoreBean(sb);
							jbab.setPrecedeCenterPre();
							jbab.execute();
		
							judgedList = jbab.getRecordSet();
							listBean.createListBean(judgedList, sb);
								
							//判定結果があるときのみ（0件以上）保存する
							if(judgedList != null && judgedList.size() > 0){
								// 結果の保存
								JudgementStoreBean jsb = new JudgementStoreBean();
								jsb.setIndividualId(sb.getIndividualId());
								jsb.setExamYear(iCommonMap.getTargetExamYear());
								jsb.setExamCd(iCommonMap.getTargetExamCode());
								jsb.setResultList(listBean.getRecordSet());
								jsb.setConnection("", con);
								jsb.execute();
							}
						}
						judgedMap.put(sb.getIndividualId(), listBean);
					}
				}
				/** 2. こだわり条件判定 */
				else if(form.getButton().equals("byCond")){
					/** この生徒の条件の保存 */
					ProvisoStoreBean psb = new ProvisoStoreBean();
					psb.setIndividualId(form.getTargetPersonId());//個人ID
					psb.setDistrictProviso(form.getPref());//地区条件
					psb.setSearchStemmaDiv(form.getSearchStemmaDiv());//検索系統区分
					psb.setStemma2Code(form.getStemma2Code());//中系統コード
					psb.setStemma11Code(form.getStemma11Code());//小系統文系コード
					psb.setStemma10Code(form.getStemma10Code());//小系統理系コード
					//分野コードは中系統・文系・理系で値が重なることがあるので一意にする
					Set set = CollectionUtil.array2Set(form.getStemmaCode());
					String[] stemmaCodes = (String[]) set.toArray(new String[set.size()]);
					psb.setStemmaCode(stemmaCodes);//分野コード
					psb.setDescImposeDiv(form.getDescImposeDiv());// 1:（センター利用大）2次・独自試験を課さない 
					psb.setSubImposeDiv(form.getSubImposeDiv());//試験科目課し区分
					psb.setImposeSub(form.getImposeSub());//課し試験科目
					psb.setSchoolDivProviso(form.getSchoolDivProviso());//学校区分条件
					psb.setStartMonth(form.getStartMonth());
					psb.setStartDate(form.getStartDate());
					psb.setEndMonth(form.getEndMonth());
					psb.setEndDate(form.getEndDate());
					psb.setRaingDiv(form.getRaingDiv());//評価試験区分
					psb.setRaingProviso(form.getRaingProviso());//評価範囲条件
					psb.setOutOfTergetProviso(form.getOutOfTergetProviso());//対象外条件
					psb.setDateSearchDiv(form.getDateSearchDiv());//入試日区分
					psb.setConnection("", con);
					psb.execute();
					/** 判定 */
					Iterator ite = scoreBeanList.iterator();
					while(ite.hasNext()){
						List judgedList = new ArrayList();
						Score sb = (Score) ite.next();

						//選択中の生徒の条件を取得（現在の生徒も含む‐既に保存されている）
						ProvisoSearchBean prb = new ProvisoSearchBean();
						prb.setIndividualId(sb.getIndividualId());
						prb.setConnection("", con);
						prb.execute();
						List savedIndividual = prb.getRecordSet();
							
						JudgementListBean listBean = new JudgementListBean();
						//この生徒は条件保存されていないのでフラグでどーするか処理を分ける
						if(savedIndividual == null || savedIndividual.size() < 1){
							//この生徒は判定しない
							if(form.getIndStudentUsage().equals("1")){
								sb = null;//ScoreBeanをnullにする
							}
							//この生徒はおまかせ判定
							else{
								//マーク模試も記述模試も受けていないので判定を行わない
								if(sb.getMark() == null && sb.getWrtn() == null){
									listBean.setJudged(false);
									listBean.setExamTaken(false);
								}
								//どちらを受けているので判定する
								else{
									//この生徒の志望の大学の分野と地区を取得
									WishUnivSearchBean wusb = new WishUnivSearchBean();
									wusb.setIndividualId(sb.getIndividualId());
									wusb.setExamYear(iCommonMap.getTargetExamYear());
									wusb.setExamCd(iCommonMap.getTargetExamCode());
									wusb.setConnection("", con);
									wusb.execute();
				
									// おまかせ判定実行
									JudgeByAutoBean jbab = new JudgeByAutoBean();
									jbab.setUnivAllList(univAll);
									jbab.setWishUnivList(wusb.getRecordSet());
									jbab.setScoreBean(sb);
									jbab.execute();
	
									judgedList = jbab.getRecordSet();
								}
									
							}
						}
						//この生徒は条件保存されているのでそのままその条件で判定
						else{
							//マーク模試も記述模試も受けていないので判定を行わない
							if(sb.getMark() == null && sb.getWrtn() == null){
								listBean.setJudged(false);
								listBean.setExamTaken(false);
							}
							//どちらを受けているので判定する
							else{
								judgedList = judgeByCond(sb, (ProvisoData)savedIndividual.get(0));
							}
						}
						//判定結果一覧の作成と結果の保存
						if(listBean.isJudged() && sb != null){
							listBean.createListBean(judgedList, sb);
						}
								
						//判定結果があるときのみ（0件以上）保存する
						if(judgedList != null && judgedList.size() > 0){
							// 結果の保存
							JudgementStoreBean jsb = new JudgementStoreBean();
							jsb.setIndividualId(sb.getIndividualId());
							jsb.setExamYear(iCommonMap.getTargetExamYear());
							jsb.setExamCd(iCommonMap.getTargetExamCode());
							jsb.setResultList(listBean.getRecordSet());
							jsb.setConnection("", con);
							jsb.execute();
						}
						if(sb != null){
							judgedMap.put(sb.getIndividualId(), listBean);	
						}
					}
				}
				/** 3. 大学指定判定 */
				else{
					List judgedList = new ArrayList();
					Iterator ite = scoreBeanList.iterator();
					while(ite.hasNext()){
						Score sb = (Score) ite.next();
							
						JudgementListBean listBean = new JudgementListBean();
						//マーク模試も記述模試も受けていないので判定を行わない
						if(sb.getMark() == null && sb.getWrtn() == null){
							listBean.setJudged(false);
							listBean.setExamTaken(false);
						}
						//マーク模試または記述模試どちらか一方でも受けているので判定する
						else{
							/** 指定の大学情報を取得 */
							String[][] univArray = new String[form.getUnivValue().length][4];
							for(int i=0; i<form.getUnivValue().length; i++){
								StringTokenizer st = new StringTokenizer(form.getUnivValue()[i], ",");
								String[] univValue = new String[4];
								univValue[0] = st.nextToken();//univCd
								univValue[1] = st.nextToken();//facultyCd
								univValue[2] = st.nextToken();//deptCd
								univValue[3] = Integer.toString(i);//追加された順番を覚えておく
								univArray[i] = univValue;
								//System.out.println("univValueArray "+univValue[0]+univValue[1]+univValue[2]);
							}
					
							/** 大学指定判定実行 */
							JudgeByUnivBean jbub = new JudgeByUnivBean();
							jbub.setUnivAllList(univAll);
							jbub.setUnivArray(univArray);
							jbub.setPrecedeCenterPre();
							jbub.setScoreBean(sb);
							jbub.execute();	

							judgedList = jbub.getRecordSet();
							if(sb.isTargetMarkExam()){//[4] add
								//選択対象模試がマークならばセンター評価順にソート
								Collections.sort(judgedList, new JudgementSorterV2().new SortByCenterScoreRank());
							}else{
								//選択対象模試が記述模試ならば二次評価順にソート
								Collections.sort(judgedList, new JudgementSorterV2().new SortBySecondScoreRank());
							}
			
							if(judgedList.size() > Integer.parseInt(IPropsLoader.getInstance().getMessage("DISP_NUM_BY_UNIV"))){
								judgedList = judgedList.subList(0, Integer.parseInt(IPropsLoader.getInstance().getMessage("DISP_NUM_BY_UNIV")));
							}
							
							if(listBean.isJudged()){
								listBean.createListBean(judgedList, sb);
							}
								
							//判定結果があるときのみ（0件以上）保存する
							if (judgedList != null && judgedList.size() > 0) {
								// 結果の保存
								JudgementStoreBean jsb = new JudgementStoreBean();
								jsb.setIndividualId(sb.getIndividualId());
								jsb.setExamYear(iCommonMap.getTargetExamYear());
								jsb.setExamCd(iCommonMap.getTargetExamCode());
								jsb.setResultList(listBean.getRecordSet());
								jsb.setConnection("", con);
								jsb.execute();								
							}
						}
						judgedMap.put(sb.getIndividualId(), listBean);
					}
				}
					
				con.commit();
				
			} catch (final Exception e) {
				DbUtils.rollback(con);
				throw e;
			}
			
			//「表示中の生徒一人」の結果一覧を作成してセッションに追加
			if (judgedMap.get(form.getTargetPersonId()) != null) {
				session.setAttribute(JudgementListBean.SESSION_KEY,
						judgedMap.get(form.getTargetPersonId()));
			// 判定を行っていない場合はisJudge=false(デフォルト)のJudgementListBeanを代入
			} else {
				JudgementListBean bean = new JudgementListBean();
				bean.setJudged(false);
				session.setAttribute(JudgementListBean.SESSION_KEY, bean);
			}
			
			// 「おまかせ・大学指定・こだわり」のアクションを表示用にセッションに追加
			session.setAttribute("judgedBy", form.getButton());
			
			return judgedMap;
		}
		
		// こだわり条件判定
		// これだけは他で使いまわすこともないのでロジックをここに置く。
		private List judgeByCond(Score sb, ProvisoData form) throws Exception {//[1.5] add
				
			// 条件指定判定実行
			JudgeByCondBean jbcb = new JudgeByCondBean();
			jbcb.setUnivAllList(univAll);
			jbcb.setDistrictProviso(CollectionUtil.deconcatComma(form.getDistrictProviso()));//地区条件
			jbcb.setSearchStemmaDiv(form.getSearchStemmaDiv());//検索系統区分
			jbcb.setSearchStemmaDiv(form.getSearchStemmaDiv());
			jbcb.setStemmaProviso(CollectionUtil.deconcatComma(form.getStemmaProviso()));//中系統コード || 分野コード
			jbcb.setDescImposeDiv(form.getDescImposeDiv());// 1:（センター利用大）2次・独自試験を課さない 
			jbcb.setSubImposeDiv(form.getSubImposeDiv());//試験科目課し区分
			jbcb.setImposeSub(CollectionUtil.deconcatComma(form.getImposeSub()));//課し試験科目
			String startMonth = RecordProcessor.getMonthDigit(form.getStartDateProviso());
			String startDate = RecordProcessor.getDateDigit(form.getStartDateProviso());
			String endMonth = RecordProcessor.getMonthDigit(form.getEndDateProviso());
			String endDate = RecordProcessor.getDateDigit(form.getEndDateProviso());
			jbcb.setStartMonth(startMonth);
			jbcb.setStartDate(startDate);
			jbcb.setEndMonth(endMonth);
			jbcb.setEndDate(endDate);
			jbcb.setRaingDiv(form.getRaingDiv());//評価試験区分
			//判定マーク模試がセンターリサーチ
			if(sb.isMarkCenterResearch()){//[4] add
				//私大一般等を除外する
				jbcb.setSchoolDivProviso(CollectionUtil.remove(CollectionUtil.deconcatComma(form.getSchoolDivProviso()), new String[]{"4","6","8","10","11"}));
			}
			//判定マーク模試がセンターリサーチ以外
			else{
				jbcb.setRaingProviso(CollectionUtil.deconcatComma(form.getRaingProviso()));
				jbcb.setSchoolDivProviso(CollectionUtil.deconcatComma(form.getSchoolDivProviso()));
			}
			//[1.5] add end
			jbcb.setOutOfTergetProviso(CollectionUtil.deconcatComma(form.getOutOfTergetProviso()));//対象外条件
			jbcb.setDateSearchDiv(form.getDateSearchDiv());
				
			jbcb.setScoreBean(sb);
			//[4] del end
			jbcb.setPrecedeCenterPre();
			jbcb.execute();
						
			return jbcb.getRecordSet();
		}
		
	}
	
}

package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.forms.individual.IOnJudgeForm;

/**
 *
 * 判定処理中画面のサーブレット
 * Vista対応で画面用の判定にしか使わなくなった
 * 
 * 2004.11.20	[新規作成]
 * 2008.02.13	Vista対応
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class IOnJudgeServlet extends IndividualServlet {
	
	/**
	 * @see jp.co.fj.keinavi.servlets.individual.IndividualServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			 javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(final HttpServletRequest request,
		final HttpServletResponse response)
		throws ServletException, IOException {
			
		// アクションフォーム
		final IOnJudgeForm form = (IOnJudgeForm) getActionForm(
				request, "jp.co.fj.keinavi.forms.individual.IOnJudgeForm");
		
		// HTTPセッション
		final HttpSession session = request.getSession(false);
		
		// 判定処理スレッドステータス
		final String status = BaseJudgeThread.getScreenStatus(session);

		// 処理中
		if (status == BaseJudgeThread.STATUS_PROG) {
			request.setAttribute("form", form);
			forward(request, response, JSP_IOnJudge);
		// エラー
		} else if (status == BaseJudgeThread.STATUS_ERR) {
			throw new ServletException(
					"判定処理スレッドでエラーが発生しました。");
		// 処理が終わった
		} else if (status == BaseJudgeThread.STATUS_DONE) {
			request.setAttribute("form", form);
			forward(request, response, JSP_I204);
		// 不正なステータス
		} else {
			throw new ServletException(
					"判定処理スレッドの状態が不正です。" + status);
		}
	}
	
}

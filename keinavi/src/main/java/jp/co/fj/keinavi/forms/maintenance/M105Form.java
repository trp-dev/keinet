package jp.co.fj.keinavi.forms.maintenance;

import java.text.Format;
import java.text.SimpleDateFormat;

import jp.co.fj.keinavi.data.maintenance.KNStudentData;

import org.apache.commons.lang.StringUtils;

/**
 *
 * 生徒情報編集画面アクションフォーム
 * 
 * 2005.11.24	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M105Form extends M102Form {

	// 対象個人ID
	private String targetIndividualId;
	// 前年度学年
	private String lastGrade;
	// 前年度クラス
	private String lastClassName;
	// 前年度クラス番号
	private String lastClassNo;
	// 前々年度学年
	private String beforeLastGrade;
	// 前々年度クラス
	private String beforeLastClassName;
	// 前々年度クラス番号
	private String beforeLastClassNo;
	
	// 変更前の今年度学年
	private String oGrade;
	// 変更前の今年度クラス
	private String oClass;
	// 変更前の前年度学年
	private String oLastGrade;
	// 変更前の前年度クラス
	private String oLastClassName;
	// 変更前の前々年度学年
	private String oBeforeLastGrade;
	// 変更前の前々年度クラス
	private String oBeforeLastClassName;
	
	// 強制更新フラグ
	private String forcingFlag;
	// 上書き情報（かな氏名）
	private String overwriteNameKana;
	// 上書き情報（漢字氏名）
	private String overwriteNameKanji;
	// 上書き情報（生年月日）
	private String overwriteBirthday;
	// 上書き情報（電話番号）
	private String overwriteTelNo;
	
	/**
	 * @return 登録（主情報のみ変更）モードかどうか
	 */
	public boolean isRegistMode() {
		return "m105".equals(
				getBackward()) && "1".equals(getActionMode());
	}
	
	/**
	 * @return 主従切替モードかどうか
	 */
	public boolean isExchangeMode() {
		return "m105".equals(
				getBackward()) && "2".equals(getActionMode());
	}
	
	/**
	 * @return 従情報削除モードかどうか
	 */
	public boolean isRemoveMode() {
		return "m105".equals(
				getBackward()) && "3".equals(getActionMode());
	}
	
	/**
	 * 初期化処理
	 * 
	 * @param data 生徒情報データ
	 */
	public void init(final KNStudentData data) {
		
		setTargetIndividualId(data.getIndividualId());
		setGrade(String.valueOf(data.getCurrentGrade()));
		setClassName(data.getCurrentClassName());
		setClassNo(data.getCurrentClassNo());
		setLastGrade(String.valueOf(data.getLastGrade()));
		setLastClassName(data.getLastClassName());
		setLastClassNo(data.getLastClassNo());
		setBeforeLastGrade(String.valueOf(data.getBeforeLastGrade()));
		setBeforeLastClassName(data.getBeforeLastClassName());
		setBeforeLastClassNo(data.getBeforeLastClassNo());
		setSex(data.getSex());
		setNameKana(data.getNameHiragana());
		setNameKanji(data.getNameKanji());
		setOGrade(String.valueOf(data.getOCurrentGrade()));
		setOClass(data.getOCurrentClassName());
		setOLastGrade(String.valueOf(data.getOLastGrade()));
		setOLastClassName(data.getOLastClassName());
		setOBeforeLastGrade(String.valueOf(data.getOBeforeLastGrade()));
		setOBeforeLastClassName(data.getOBeforeLastClassName());
		
		if (data.getBirthday() == null) {
			setBirthYear("");
			setBirthMonth("");
			setBirthDate("");
		} else {
			final Format format = new SimpleDateFormat("yyyy/MM/dd");
			final String[] value = StringUtils.split(
					format.format(data.getBirthday()), '/');
			setBirthYear(value[0]);
			setBirthMonth(value[1]);
			setBirthDate(value[2]);
		}
		
		setTelNo(data.getTelNo());
	}
	
	/**
	 * @see jp.co.fj.keinavi.forms.maintenance.M102Form#createStudentData()
	 */
	protected KNStudentData createStudentData() {
		return new KNStudentData(0);
	}

	/**
	 * @see jp.co.fj.keinavi.forms.maintenance.M102Form#toStudentData()
	 */
	public KNStudentData toStudentData() {
		
		final KNStudentData data = super.toStudentData();
		
		data.setIndividualId(getTargetIndividualId());
		
		if (!StringUtils.isEmpty(getLastGrade())) {
			data.setLastGrade(
					Integer.parseInt(getLastGrade()));
			data.setLastClassName(
					normalizeClassName(getLastClassName()));
			data.setLastClassNo(
					normalizeClassNo(getLastClassNo()));
		}
		
		if (!StringUtils.isEmpty(getBeforeLastGrade())) {
			data.setBeforeLastGrade(
					Integer.parseInt(getBeforeLastGrade()));
			data.setBeforeLastClassName(
					normalizeClassName(getBeforeLastClassName()));
			data.setBeforeLastClassNo(
					normalizeClassNo(getBeforeLastClassNo()));
		}
		
		// 今年度の変更前
		if (getOClass().length() > 0) {
			data.setOCurrentGrade(
					Integer.parseInt(getOGrade()));
			data.setOCurrentClassName(getOClass());
		}
		
		// 前年度の変更前
		if (getOLastClassName().length() > 0) {
			data.setOLastGrade(
					Integer.parseInt(getOLastGrade()));
			data.setOLastClassName(getOLastClassName());
		}
		
		// 前々年度の変更前
		if (getOBeforeLastClassName().length() > 0) {
			data.setOBeforeLastGrade(
					Integer.parseInt(getOBeforeLastGrade()));
			data.setOBeforeLastClassName(getOBeforeLastClassName());
		}
		
		return data;
	}

	/**
	 * @return targetIndividualId を戻します。
	 */
	public String getTargetIndividualId() {
		return targetIndividualId;
	}
	/**
	 * @param pTargetIndividualId 設定する targetIndividualId。
	 */
	public void setTargetIndividualId(final String pTargetIndividualId) {
		this.targetIndividualId = pTargetIndividualId;
	}
	/**
	 * @return beforeLastClassName を戻します。
	 */
	public String getBeforeLastClassName() {
		return beforeLastClassName;
	}
	/**
	 * @param pBeforeLastClassName 設定する beforeLastClassName。
	 */
	public void setBeforeLastClassName(final String pBeforeLastClassName) {
		this.beforeLastClassName = pBeforeLastClassName;
	}
	/**
	 * @return beforeLastClassNo を戻します。
	 */
	public String getBeforeLastClassNo() {
		return beforeLastClassNo;
	}
	/**
	 * @param pBeforeLastClassNo 設定する beforeLastClassNo。
	 */
	public void setBeforeLastClassNo(final String pBeforeLastClassNo) {
		this.beforeLastClassNo = pBeforeLastClassNo;
	}
	/**
	 * @return beforeLastGrade を戻します。
	 */
	public String getBeforeLastGrade() {
		return beforeLastGrade;
	}
	/**
	 * @param pBeforeLastGrade 設定する beforeLastGrade。
	 */
	public void setBeforeLastGrade(final String pBeforeLastGrade) {
		this.beforeLastGrade = pBeforeLastGrade;
	}
	/**
	 * @return lastClassName を戻します。
	 */
	public String getLastClassName() {
		return lastClassName;
	}
	/**
	 * @param pLastClassName 設定する lastClassName。
	 */
	public void setLastClassName(final String pLastClassName) {
		this.lastClassName = pLastClassName;
	}
	/**
	 * @return lastClassNo を戻します。
	 */
	public String getLastClassNo() {
		return lastClassNo;
	}
	/**
	 * @param pLastClassNo 設定する lastClassNo。
	 */
	public void setLastClassNo(final String pLastClassNo) {
		this.lastClassNo = pLastClassNo;
	}
	/**
	 * @return lastGrade を戻します。
	 */
	public String getLastGrade() {
		return lastGrade;
	}
	/**
	 * @param pLastGrade 設定する lastGrade。
	 */
	public void setLastGrade(final String pLastGrade) {
		this.lastGrade = pLastGrade;
	}
	/**
	 * @return oBeforeLastClassName を戻します。
	 */
	public String getOBeforeLastClassName() {
		return oBeforeLastClassName;
	}
	/**
	 * @param pBeforeLastClassName 設定する oBeforeLastClassName。
	 */
	public void setOBeforeLastClassName(final String pBeforeLastClassName) {
		oBeforeLastClassName = pBeforeLastClassName;
	}
	/**
	 * @return oBeforeLastGrade を戻します。
	 */
	public String getOBeforeLastGrade() {
		return oBeforeLastGrade;
	}
	/**
	 * @param pBeforeLastGrade 設定する oBeforeLastGrade。
	 */
	public void setOBeforeLastGrade(final String pBeforeLastGrade) {
		oBeforeLastGrade = pBeforeLastGrade;
	}
	/**
	 * @return oClass を戻します。
	 */
	public String getOClass() {
		return oClass;
	}
	/**
	 * @param pClass 設定する oClass。
	 */
	public void setOClass(final String pClass) {
		oClass = pClass;
	}
	/**
	 * @return oGrade を戻します。
	 */
	public String getOGrade() {
		return oGrade;
	}
	/**
	 * @param pGrade 設定する oGrade。
	 */
	public void setOGrade(final String pGrade) {
		oGrade = pGrade;
	}
	/**
	 * @return oLastClassName を戻します。
	 */
	public String getOLastClassName() {
		return oLastClassName;
	}
	/**
	 * @param pLastClassName 設定する oLastClassName。
	 */
	public void setOLastClassName(final String pLastClassName) {
		oLastClassName = pLastClassName;
	}
	/**
	 * @return oLastGrade を戻します。
	 */
	public String getOLastGrade() {
		return oLastGrade;
	}
	/**
	 * @param pLastGrade 設定する oLastGrade。
	 */
	public void setOLastGrade(final String pLastGrade) {
		oLastGrade = pLastGrade;
	}

	/**
	 * @return forcingFlag を戻します。
	 */
	public String getForcingFlag() {
		return forcingFlag;
	}

	/**
	 * @param pForcingFlag 設定する forcingFlag。
	 */
	public void setForcingFlag(String pForcingFlag) {
		forcingFlag = pForcingFlag;
	}

	/**
	 * @return overwriteBirthday を戻します。
	 */
	public String getOverwriteBirthday() {
		return overwriteBirthday;
	}

	/**
	 * @param pOverwriteBirthday 設定する overwriteBirthday。
	 */
	public void setOverwriteBirthday(String pOverwriteBirthday) {
		overwriteBirthday = pOverwriteBirthday;
	}

	/**
	 * @return overwriteNameKana を戻します。
	 */
	public String getOverwriteNameKana() {
		return overwriteNameKana;
	}

	/**
	 * @param pOverwriteNameKana 設定する overwriteNameKana。
	 */
	public void setOverwriteNameKana(String pOverwriteNameKana) {
		overwriteNameKana = pOverwriteNameKana;
	}

	/**
	 * @return overwriteNameKanji を戻します。
	 */
	public String getOverwriteNameKanji() {
		return overwriteNameKanji;
	}

	/**
	 * @param pOverwriteNameKanji 設定する overwriteNameKanji。
	 */
	public void setOverwriteNameKanji(String pOverwriteNameKanji) {
		overwriteNameKanji = pOverwriteNameKanji;
	}

	/**
	 * @return overwriteTelNo を戻します。
	 */
	public String getOverwriteTelNo() {
		return overwriteTelNo;
	}

	/**
	 * @param pOverwriteTelNo 設定する overwriteTelNo。
	 */
	public void setOverwriteTelNo(String pOverwriteTelNo) {
		overwriteTelNo = pOverwriteTelNo;
	}
	
}

/*
 * 作成日: 2004/11/30
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.db.DBManager;


/**
 * クラス再集計処理スレッド
 * 
 * 2005.02.24 Yoshimoto KAWAI - Totec
 *            再集計依頼だけをDBへ保存するように変更
 * 
 * @author Yoshimoto KAWAI - Totec
 */
class Recount implements Runnable {

	String dbKey;
	String schoolCd;
	KNLog log;

	/* (非 Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		Connection con = null;
		try {
			log.Lv1("M001",0,"再計算処理開始");

			con = DBManager.getConnectionPool(this.dbKey);
			con.setAutoCommit(false);

			// 処理をロックする
			this.lock(con);

//			// 一括コードのリストを取得する
//			CountingDivBean bean = new CountingDivBean();
//			bean.setConnection(null, con);
//			bean.setSchoolCD(this.schoolCd);
//			bean.execute();
//				
//			ReCountBean recount = new ReCountBean(log);
//			recount.setBundleCd(bean.getCountingDivCodeArray());
//			recount.ReCountClass(con);

		} catch (Exception e) {
			try { con.rollback(); } catch (SQLException s) {}
			log.Lv1("M001", 0, "再集計処理異常終了");
			log.Lv1("M001", 0, e.getMessage());

		} finally {
			//this.unLock(con);
			try { DBManager.releaseConnectionPool(this.dbKey, con); } catch (Exception e) {}
		}

		log.Lv1("M001", 0, "再集計処理正常終了");
	}

	/**
	 * 処理をロックする
	 * @param con
	 * @throws SQLException
	 */
	private void lock(Connection con) throws SQLException {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("INSERT INTO recountqueue VALUES(?, SYSDATE)");
			ps.setString(1, this.schoolCd);
			if (ps.executeUpdate() == 0) throw new SQLException("再集計処理中です。");
			con.commit();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

//	/**
//	 * 処理のロックを外す
//	 * @param con
//	 * @throws SQLException
//	 */
//	private void unLock(Connection con) {
//		PreparedStatement ps = null;
//		try {
//			ps = con.prepareStatement("DELETE FROM recountqueue WHERE schoolcd = ?");
//			ps.setString(1, this.schoolCd);
//			ps.executeUpdate();
//			con.commit();
//		} catch (SQLException e) {
//			try { con.rollback(); } catch (SQLException s) {}
//			log.Lv1("M001", 0, e.getMessage());
//		} finally {
//			DbUtils.closeQuietly(ps);
//		}
//	}

	/**
	 * 処理中かどうか
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static boolean isProcess(Connection con, String schoolCd) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement("SELECT ROWID FROM recountqueue WHERE schoolcd = ?");
			ps.setString(1, schoolCd);
			rs = ps.executeQuery();
			return rs.next();
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * @param string
	 */
	public void setDbKey(String string) {
		dbKey = string;
	}

	/**
	 * @param log
	 */
	public void setLog(KNLog log) {
		this.log = log;
	}

	/**
	 * @param string
	 */
	public void setSchoolCd(String string) {
		schoolCd = string;
	}

}
package jp.co.fj.keinavi.beans.profile;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.date.DateUtil;
import jp.co.fj.keinavi.util.date.ShortDateUtil;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応 
 * 
 * @author kawai
 *
 */
public class ProfileFolderUpdateBean extends DefaultBean {

	private LoginSession loginSession; // ログイン情報	
	private String folderID; // プロファイルフォルダID
	private String folderName;	// プロファイルフォルダ名称
	private String comment; // コメント
	private String bundleCD; // 一括コード
	private boolean isStandard = false; // ひな型作成モードか

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// 日付用のクラス
		DateUtil date = new ShortDateUtil();

		// フォルダIDが空なら新規
		if ("".equals(folderID)) {
			
			// フォルダIDを採番
			createFolderId();
			
			// INSERT
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				ps = conn.prepareStatement(
						"INSERT INTO profilefolder VALUES(?,?,?,?,?,?,?,?)");
				ps.setString(1, folderID);
				ps.setString(2, folderName);
				ps.setString(3, date.date2String());
				ps.setString(4, date.date2String());
				ps.setString(5, comment);

				// 高校
				if (loginSession.isSchool()) {
					ps.setString(6, loginSession.getAccount());
					ps.setString(7, bundleCD);
					ps.setString(8, null);

				// 営業部または校舎
				} else if (loginSession.isBusiness()) {
					ps.setString(6, null);
					ps.setString(7, null);
					ps.setString(8, loginSession.getSectorSortingCD());

				// 営業 → 高校
				} else if (loginSession.isDeputy()) {
					ps.setString(6, null);
					ps.setString(7, bundleCD);
					ps.setString(8, loginSession.getSectorSortingCD());
				}

				ps.executeUpdate();
				
			} finally {
				DbUtils.closeQuietly(ps);
			}

			// 営業部の高校代行の場合、
			// ひな型プロファイルは無制限
			if (loginSession.isDeputy() && isStandard) {
				return;
			}
			
			// プロファイルフォルダ数の制限値チェック（楽観的なロック）
			try {
				StringBuffer query = new StringBuffer();
				query.append("SELECT count(profilefid) FROM profilefolder ");
			
				// 高校		
				if (loginSession.isSchool()) {
					query.append("WHERE bundlecd = ? AND sectorsortingcd IS NULL");
				
					ps = conn.prepareStatement(query.toString());
					ps.setString(1, bundleCD); // 一括コード

				// 営業部または校舎
				} else if(loginSession.isBusiness()) {
					query.append("WHERE sectorsortingcd = ? AND userid IS NULL ");
					query.append("AND bundlecd IS NULL");

					ps = conn.prepareStatement(query.toString());
					ps.setString(1, loginSession.getSectorSortingCD()); // 部門分類コード

				// 営業部 → 高校
				} else if(loginSession.isDeputy()) {
					query.append("WHERE bundlecd = ? AND sectorsortingcd = ?");
				
					ps = conn.prepareStatement(query.toString());
					ps.setString(1, bundleCD); // 一括コード
					ps.setString(2, loginSession.getSectorSortingCD()); // 部門分類コード
				}
			
				rs = ps.executeQuery();
				
				// 20個になると別名保存できないので
				// ひな型なら制限値を減らす
				final int limit = isStandard ? 19 : 20;
				
				// 制限値を超えていたらエラー
				if (rs.next() && rs.getInt(1) > limit) {
					KNServletException e =
						new KNServletException("フォルダの制限数を超えています。");
					e.setErrorCode("10W002999999");
					throw e;
				}
			} finally {
				DbUtils.closeQuietly(rs);	
				DbUtils.closeQuietly(ps);	
			}
			
		// そうでなければUPDATE	
		} else {
			PreparedStatement ps = null;
			try {
				StringBuffer query = new StringBuffer();
				query.append("UPDATE profilefolder ");
				query.append("SET profilefname = ?, com = ?, renewaldate = ? ");
				query.append("WHERE profilefid = ? ");

				// 高校
				if (loginSession.isSchool()) {
					query.append("AND bundlecd = ? AND sectorsortingcd IS NULL");
				
					ps = conn.prepareStatement(query.toString());
					ps.setString(5, bundleCD); // 一括コード

				// 営業部または校舎
				} else if (loginSession.isBusiness()) {
					query.append("AND userid IS NULL AND bundlecd IS NULL AND sectorsortingcd = ?");

					ps = conn.prepareStatement(query.toString());
					ps.setString(5, loginSession.getSectorSortingCD()); // 部門分類コード

				// 営業 → 高校
				} else if (loginSession.isDeputy()) {
					query.append("AND userid IS NULL AND bundlecd = ? AND sectorsortingcd = ?");

					ps = conn.prepareStatement(query.toString());
					ps.setString(5, bundleCD); // 一括コード
					ps.setString(6, loginSession.getSectorSortingCD()); // 部門分類コード
				}
			
				ps.setString(1, folderName); // フォルダ名
				ps.setString(2, comment); // コメント
				ps.setString(3, date.date2String()); // 更新日
				ps.setString(4, folderID); // フォルダID
			
				ps.execute();
			} finally {
				DbUtils.closeQuietly(ps);
			}
		}
	}

	// フォルダIDを採番する
	private void createFolderId() throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					"SELECT TO_CHAR(SYSDATE, 'YY') "
					+ "|| TO_CHAR(profilefidcount.nextval, 'FM00000') "
					+ "FROM DUAL");
			rs = ps.executeQuery();
			if (rs.next()) {
				folderID = rs.getString(1);
			} else {
				throw new InternalError("フォルダIDの生成に失敗しました。");
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * フォルダを公開状態にする
	 * 
	 * @throws SQLException 
	 */
	public void publish() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					"UPDATE profilefolder "
					+ "SET sectorsortingcd = NULL "
					+ "WHERE profilefid = ?");
			ps.setString(1, folderID);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	// --------------------------------------------------------------------------------

	/**
	 * @param string
	 */
	public void setBundleCD(String string) {
		bundleCD = string;
	}

	/**
	 * @param string
	 */
	public void setComment(String string) {
		comment = string;
	}

	/**
	 * @param string
	 */
	public void setFolderID(String string) {
		folderID = string;
	}

	/**
	 * @param string
	 */
	public void setFolderName(String string) {
		folderName = string;
	}

	/**
	 * @param session
	 */
	public void setLoginSession(LoginSession session) {
		loginSession = session;
	}

	/**
	 * @return folderID を戻します。
	 */
	public String getFolderID() {
		return folderID;
	}

	/**
	 * @param pIsStandard 設定する isStandard。
	 */
	public void setStandard() {
		isStandard = true;
	}

}

/*
 * 作成日: 2004/09/24
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.individual.SubjectData;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultSearchBean;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class SubjectSearchBean extends DefaultSearchBean{

	private String examYear;
	private String examCd;

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("i03").toString());
			ps.setString(1, this.examYear);
			ps.setString(2, this.examCd);
			
			rs = ps.executeQuery();
			while(rs.next()){
				SubjectData data = new SubjectData();
				data.setSubjectCd(rs.getString(1));
				data.setSubjectName(rs.getString(2));
				data.setDispSequence(rs.getString(3));
				data.setSubAddrName(rs.getString(4));
				super.recordSet.add(data);
			}

			// レコードがあれば総合も入れておく
			if (super.recordSet.size() > 0) {
				SubjectData data = new SubjectData();
				data.setSubjectCd("7777");
				data.setSubjectName("総合");
				data.setDispSequence("0000");
				data.setSubAddrName("総合");
				super.recordSet.add(0, data);
			}

		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);	
		}
	}

	/**
	 * @param string
	 */
	public void setExamCd(String string) {
		examCd = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

}

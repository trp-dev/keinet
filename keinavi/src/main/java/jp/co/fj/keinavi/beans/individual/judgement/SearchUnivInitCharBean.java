package jp.co.fj.keinavi.beans.individual.judgement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fjh.beans.DefaultBean;

import jp.co.fj.keinavi.data.individual.UnivKeiNavi;


/**
 *
 * 大学頭文字かな検索
 * 
 * 200x.xx.xx   Tomohisa YAMADA - Totec
 *              新規作成
 *              
 * <2010年度改修>
 * 2009.12.01   Tomohisa YAMADA - Totec
 *              マスタデータの持ち方変更
 *              
 *
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 *
 */
public class SearchUnivInitCharBean extends DefaultBean implements java.io.Serializable{

	private String initCharHalf = "";//検索対象大学名半角頭文字
	private String initCharFull = "";//検索対象大学名全角頭文字
	private ArrayList compairData = new ArrayList();//比較対象データ
	private List recordSet = new ArrayList();//結果データ

	/**
	 * 頭文字指定で大学を検索する
	 */
	public void execute(){
		List iniArray = compairData;
		if(iniArray != null){
			Iterator ite = iniArray.iterator();
			while(ite.hasNext()){
				UnivKeiNavi univ = (UnivKeiNavi)ite.next();
				if(univ.getUnivNameKana().substring(0, 1).equals(initCharHalf) || univ.getUnivNameKana().substring(0, 1).equals(initCharFull)){
					recordSet.add(univ);
				}
			}
		}
	}


	/**
	 * @return
	 */
	public ArrayList getCompairData() {
		return compairData;
	}

	/**
	 * @return
	 */
	public String getInitCharFull() {
		return initCharFull;
	}

	/**
	 * @return
	 */
	public String getInitCharHalf() {
		return initCharHalf;
	}

	/**
	 * @return
	 */
	public List getRecordSet() {
		return recordSet;
	}

	/**
	 * @param list
	 */
	public void setCompairData(ArrayList list) {
		compairData = list;
	}

	/**
	 * @param string
	 */
	public void setInitCharFull(String string) {
		initCharFull = string;
	}

	/**
	 * @param string
	 */
	public void setInitCharHalf(String string) {
		initCharHalf = string;
	}

	/**
	 * @param list
	 */
	public void setRecordSet(List list) {
		recordSet = list;
	}

}
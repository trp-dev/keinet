/**
 * 校内成績分析−クラス比較　設問別成績（クラス比較）
 *      国語評価別人数 データクラス
 * 作成日: 2019/09/05
 * @author      M.Ooseto
 */

package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;

public class S13SugakuKokugoQuiListBean {

	//小設問別正答状況（数学）データリスト
	private ArrayList s13SugakuQueList = new ArrayList();
	//小設問別正答状況（国語）データリスト
	private ArrayList s13KokugoQueList = new ArrayList();

	/*----------*/
	/* Get      */
	/*----------*/
	public ArrayList getS13SugakuQueList() {
		return s13SugakuQueList;
	}
	public ArrayList getS13KokugoQueList() {
		return s13KokugoQueList;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setS13SugakuQueList(ArrayList s13SugakuQueList) {
		this.s13SugakuQueList = s13SugakuQueList;
	}
	public void setS13KokugoQueList(ArrayList s13KokugoQueList) {
		this.s13KokugoQueList = s13KokugoQueList;
	}

}

package jp.co.fj.keinavi.beans.maintenance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import jp.co.fj.keinavi.beans.admin.name.NameBringsNear;
import jp.co.fj.keinavi.beans.maintenance.module.StudentDataNormalizer;
import jp.co.fj.keinavi.beans.maintenance.module.StudentDataValidator;
import jp.co.fj.keinavi.data.maintenance.KNHistoryData;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.csv.CSVLineRegisterBean;
import jp.co.fj.keinavi.util.csv.data.CSVLineRecord;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 生徒情報一括登録Bean
 * 
 * 2005.12.06	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class KNStudentCSVLineRegisterBean extends CSVLineRegisterBean {

	private final String schoolCd;
	private final int currentYear = Integer.parseInt(KNUtil.getCurrentYear());
	private final KNStudentInsertBean bean;
	private final Set historySet = new HashSet(); 
	
	// 名寄せモジュール
	private final NameBringsNear name = new NameBringsNear();
	// 再集計対象登録Bean
	private final RecountRegisterBean recount = new RecountRegisterBean();
	// データ正規化モジュール
	private final StudentDataNormalizer normalizer =
			StudentDataNormalizer.getInstance();
	// データチェックモジュール
	private final StudentDataValidator validator =
			StudentDataValidator.getInstance();
	
	/**
	 * @param pSchoolCd 学校コード
	 * @throws SQLException 
	 */
	public KNStudentCSVLineRegisterBean(final String pSchoolCd) {
		
		if (pSchoolCd == null) {
			throw new IllegalArgumentException(
					"学校コードがNULLです。");
		}
		
		this.schoolCd = pSchoolCd;
		this.bean = createKNStudentInsertBean(pSchoolCd);
	}
	
	/**
	 * [FactoryMethod]
	 * 
	 * @return KNStudentInsertBeanのインスタンス
	 */
	protected KNStudentInsertBean createKNStudentInsertBean(
			final String pSchool) {
		
		return new KNStudentInsertBean(pSchool);
	}
	
	/**
	 * @see com.fjh.beans.DefaultBean
	 * 		#setConnection(java.lang.String, java.sql.Connection)
	 */
	public void setConnection(final String DBName, final Connection conn) {
		super.setConnection(DBName, conn);
		bean.setConnection(DBName, conn);
		name.setConnection(DBName, conn);
	}

	/**
	 * @see jp.co.fj.keinavi.util.csv.CSVLineRegisterBean
	 * 		#normalizeRecord(java.lang.String[])
	 */
	public String[] normalizeRecord(final String[] record) {
		
		// 個人ID
		record[getIndividualIdPosition()] = normalizer.normalizeIndividualId(record[getIndividualIdPosition()]);
		// クラス
		record[2] = normalizer.normalizeClassName(record[2]);
		// クラス番号
		record[3] = normalizer.normalizeClassNo(record[3]);
		// カナ氏名
		record[4] = normalizer.normalizeNameKana(record[4]);
		// 生年月日
		record[7] = normalizer.normalizeBirthday(record[7]);
		// 電話番号
		record[8] = normalizer.normalizeTelNo(record[8]);
		// 結合状態フラグ
		record[record.length -1] = "";
		
		return record;
	}

	protected int getIndividualIdPosition() {
		return 9;
	}
	
	/**
	 * @throws SQLException 
	 * @see jp.co.fj.keinavi.util.csv.CSVLineRegisterBean
	 * 		#validateAllRecord(java.util.List)
	 */
	public void validateAllRecord(
			final List lineRecordList) throws SQLException {
		
		// 個人IDの処理
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					"SELECT 1 FROM "
					+ getBasicinfoTableName()
					+ " bi WHERE bi.individualid = ? AND bi.schoolcd = ?");
			ps.setString(2, schoolCd);
			
			for (final Iterator ite = lineRecordList.iterator(); ite.hasNext();) {
				
				final CSVLineRecord record = (CSVLineRecord) ite.next();
				
				// エラーがあるなら飛ばす
				if (record.hasError()) {
					continue;
				}
				
				final String[] data = record.getRecord();
				final String individualId = data[getIndividualIdPosition()];
				
				// 空文字→新規レコードなら進級判定
				if (individualId.length() == 0) {
					name.setInputData(createInputData(data));
					name.execute();
					
					if (name.getCombiJudge() == 2) {
						data[getIndividualIdPosition()] = (String) name.getResult().get(0);
						data[data.length -1] = "2";
					} else if (name.getCombiJudge() == 1) {
						record.addError("同一人物と思われる生徒が既に登録されています。");
					}
					
				// 更新レコードなら個人IDのチェック
				} else {
					ps.setString(1, individualId);
					rs = ps.executeQuery();
					if (!rs.next()) {
						record.addError("個人IDが不正です。");
					}
					rs.close();
				}
			}
			
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.util.csv.CSVLineRegisterBean
	 * 		#validateRecord(jp.co.fj.keinavi.util.csv.data.CSVLineRecord)
	 */
	public void validateRecord(final CSVLineRecord record) throws SQLException {
		
		// 行データ
		final String[] data = record.getRecord();
		
		// 年度
		if (!validator.isValidYear(data[0], currentYear)) {
			record.addError("年度が不正です。");
		}
		
		// 学年
		if (!validator.isValidGrade(data[1])) {
			record.addError("学年が不正です。");
		}
		
		// クラス
		if (!validator.isValidClassName(data[2])) {
			record.addError("クラスが不正です。");
		}
		
		// クラス番号
		if (!validator.isValidClassNo(data[3])) {
			record.addError("クラス番号が不正です。");
		}
		
		// ここまでのデータが正しいなら重複チェック
		if (!record.hasError()) {
			
			// 履歴データを作る
			final KNHistoryData history = new KNHistoryData(
					data[0], data[1], data[2], data[3]);
			
			// ファイル内での整合性チェック
			if (historySet.contains(history)) {
				record.addError(
					"同じ年度・学年・クラス・クラス番号の生徒は登録できません。");
				
			// データベースとの整合性チェック
			} else if (bean.isDuplication(data[getIndividualIdPosition()], data[0],
					Integer.parseInt(data[1]), data[2], data[3])) {
				
				record.addError(
					"同じ年度・学年・クラス・クラス番号の生徒が既に登録されています。");
			}
			
			// 履歴データは保持しておく
			historySet.add(history);
		}
		
		// かな氏名
		if (!validator.isValidKanaName(data[4])) {
			record.addError("かな氏名が不正です。");
		}
		
		// 漢字氏名
		if (!validator.isValidKanjiName(data[5])) {
			record.addError("漢字氏名が不正です。");
		}
		
		// 性別
		if (!validator.isValidSex(data[6])) {
			record.addError("性別が不正です。");
		}
		
		// 生年月日
		if (!validator.isValidBirthday(data[7])) {
			record.addError("生年月日が不正です。");
		}

		// 電話番号
		if (!validator.isValidTelNo(data[8])) {
			record.addError("電話番号が不正です。");
		}

		// 個人ID
		if (!validator.isValidIndividualId(data[getIndividualIdPosition()])) {
			record.addError("個人IDが不正です。");
		}
	}

	private HashMap createInputData(final String[] data) {
		final HashMap inputData = new HashMap();
		inputData.put("YEAR02", data[0].substring(2, 4));
		inputData.put("EXAMYEAR", data[0]);
		inputData.put("GRADE", data[1]);
		inputData.put("CLASS", data[2]);
		inputData.put("CLASS_NO", data[3]);
		inputData.put("SEX", data[6]);
		inputData.put("NAME_KANA", data[4]);
		inputData.put("NAME_KANJI", data[5]);
		inputData.put("BIRTHYEAR", data[7].substring(0, 4));
		inputData.put("BIRTHMONTH", data[7].substring(4, 6));
		inputData.put("BIRTHDATE", data[7].substring(6, 8));
		inputData.put("TEL_NO", data[8]);
		inputData.put("SCHOOLCD", schoolCd);
		inputData.put("HOMESCHOOLCD", schoolCd);
		return inputData;
	}

	protected String getBasicinfoTableName() {
		return "basicinfo";
	}

	protected String getHistoryinfoTableName() {
		return "historyinfo";
	}

	/**
	 * @see jp.co.fj.keinavi.util.csv.CSVLineRegisterBean
	 * 		#judgeRecordType(jp.co.fj.keinavi.util.csv.data.CSVLineRecord)
	 */
	public void judgeRecordType(final CSVLineRecord record) throws SQLException {
		
		// 行データ
		final String[] data = record.getRecord();

		// 個人IDの有無で新規・更新は分かれる
		// 新規
		if (data[getIndividualIdPosition()].length() == 0) {
			record.setInsertRecord();
			
		// 更新
		} else {
			record.setUpdateRecord();
			// 更新前レコードの取得
			record.setOriginal(getOriginalRecord(data[getIndividualIdPosition()], data[0]));
		}
	}

	protected String[] getOriginalRecord(
			final String individualId, final String year) throws SQLException {

		final String[] value = new String[10];
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(createOriginalQuery());
			ps.setString(1, year);
			ps.setString(2, individualId);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				value[1] = rs.getString(1);
				value[2] = rs.getString(2);
				value[3] = rs.getString(3);
				value[4] = rs.getString(4);
			}
			
			return value;
			
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	protected String createOriginalQuery() throws SQLException {
		
		return QueryLoader.getInstance().load("m17").toString();
	}

	/**
	 * @see jp.co.fj.keinavi.util.csv.CSVLineRegisterBean
	 * 		#registRecord(jp.co.fj.keinavi.util.csv.data.CSVLineRecord)
	 */
	public void registRecord(final CSVLineRecord record) throws SQLException {
		
		// 行データ
		final String[] data = record.getRecord();
		
		// 確認画面から来たならデータベースとの整合性チェック
		if (!isDirectCommit() &&
				bean.isDuplication(data[getIndividualIdPosition()],
						data[0], Integer.parseInt(data[1]), data[2], data[3])) {
			record.addError(
				"同じ年度・学年・クラス・クラス番号の生徒が既に登録されています。");
		}
	
		if (data[getIndividualIdPosition()].length() == 0) {
			insert(record);
		} else {
			update(record);
		}
	}

	private void insert(final CSVLineRecord record) throws SQLException {
		
		final String[] data = record.getRecord();
		
		// 個人IDの生成
		data[getIndividualIdPosition()] = bean.createIndividualId(data[0]);
		// 学籍基本情報の登録
		bean.insertBasicinfo(
				data[getIndividualIdPosition()], data[4], data[5], data[6], data[7], data[8]);
		// 学籍履歴情報の登録
		insertHistoryinfo(data);
	}

	private void insertHistoryinfo(final String[] data) throws SQLException {
		
		bean.insertHistoryinfo(
				data[getIndividualIdPosition()], data[0], Integer.parseInt(data[1]), data[2], data[3]);
	}
	
	private void update(final CSVLineRecord record) throws SQLException {
		
		final String[] data = record.getRecord();
		
		// 名寄せ進級は履歴のみ作る
		if ("2".equals(data[data.length -1])) {
			insertHistoryinfo(data);

		// それ以外は学籍基本情報の更新に成功なら
		} else {
			// 学籍基本情報の更新に成功後、
			// 学籍履歴情報の更新に失敗したら登録する（進級処理）
			if (updateBasicinfo(data) && !updateHistoryinfo(record)) {
				insertHistoryinfo(data);
			}
		}
	}

	private boolean updateBasicinfo(String[] data) throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					"UPDATE " + getBasicinfoTableName() + " "
					+ "SET name_kana = ?, name_kanji = ?, "
					+ "sex = ?, birthday = ?, tel_no = ? "
					+ "WHERE individualid = ?");
			ps.setString(1, data[4]);
			ps.setString(2, data[5]);
			ps.setString(3, data[6]);
			ps.setString(4, data[7]);
			ps.setString(5, data[8]);
			ps.setString(6, data[getIndividualIdPosition()]);
			return ps.executeUpdate() == 1;
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	private boolean updateHistoryinfo(final CSVLineRecord record) throws SQLException {

		final String[] data = record.getRecord();
		final String[] original = record.getOriginal();
		final int grade = Integer.parseInt(data[1]);
		
		final boolean result;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					"UPDATE " + getHistoryinfoTableName() + " "
					+ "SET grade = ?, class = ?, class_no = ? "
					+ "WHERE individualid = ? AND year = ?");
			ps.setInt(1, grade);
			ps.setString(2, data[2]);
			ps.setString(3, data[3]);
			ps.setString(4, data[getIndividualIdPosition()]);
			ps.setString(5, data[0]);
			result = ps.executeUpdate() == 1;
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
		
		// 履歴の更新に成功なら再集計対象となる
		if (result) {
			registRecount(data[getIndividualIdPosition()], data[0],
					grade, data[2], Integer.parseInt(original[1]), original[2]);
		}
		
		return result;
	}

	/**
	 * 再集計対象を登録する
	 * 
	 * @param individualId
	 * @param year
	 * @param grade
	 * @param className
	 * @throws SQLException
	 */
	protected void registRecount(final String individualId,
			final String year, final int grade, final String className,
			final int oGrade, final String oClassName) throws SQLException {
		
		try {
			recount.setConnection(null, conn);
			recount.setYear(year);
			recount.setIndividualId(individualId);
			recount.setAfterGrade(grade);
			recount.setAfterClassName(className);
			recount.setBeforeGrade(oGrade);
			recount.setBeforeClassName(oClassName);
			recount.execute();
		} catch (final Exception e) {
			throw new SQLException(
					e.getClass().getName() + ":" + e.getMessage());
		}
	}
	
}

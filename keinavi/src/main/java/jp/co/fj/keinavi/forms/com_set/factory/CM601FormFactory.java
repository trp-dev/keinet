/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.com_set.factory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.data.com_set.CompSchoolData;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM601Form;
import jp.co.fj.keinavi.util.CollectionUtil;

import com.fjh.forms.ActionForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM601FormFactory extends AbstractCMFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		CM601Form form = new CM601Form();
		// アイテムマップ
		Map item = getItemMap(request);
		// 比較・グラフ表示対象
		List school = new LinkedList();
		List graph = new LinkedList();
		List list = (List)item.get("0401");
		if (list != null) {
			Iterator ite = list.iterator();
			while (ite.hasNext()) {
				CompSchoolData csd = (CompSchoolData)ite.next();
				school.add(csd.getSchoolCD());
				// グラフ表示対象なら入れる
				if (csd.getGraphDisp() == 1)
					graph.add(csd.getSchoolCD());
			}
			form.setSchool((String[])school.toArray(new String[0]));
			form.setGraph((String[])graph.toArray(new String[0]));
		}
		return form;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// セッション
		HttpSession session = request.getSession();
		// アイテムマップ
		Map item = getItemMap(request);
		// 共通アクションフォーム
		CM001Form f001 = (CM001Form)session.getAttribute(CM001Form.SESSION_KEY);
		// 個別アクションフォーム
		CM601Form f601 = (CM601Form)f001.getActionForm("CM601Form");

		// 設定値リスト
		List container = (List)item.get("0401");
		if (container == null) {
			container = new ArrayList();
			item.put("0401", container);
		}
		
		// 高校CD
		String[] school = f601.getSchool();

		// いったんクリア
		container.clear();

		if (school != null) {
			// グラフ表示
			Set graph = CollectionUtil.array2Set(f601.getGraph());
			for (int i=0; i<school.length; i++) {
				// オブジェクトを作る
				CompSchoolData data = new CompSchoolData();
				data.setSchoolCD(school[i]);
				if (graph.contains(school[i])) data.setGraphDisp((short)1);
				container.add(data);
			}
		}
	}

}

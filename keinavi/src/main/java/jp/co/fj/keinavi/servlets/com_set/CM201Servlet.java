package jp.co.fj.keinavi.servlets.com_set;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.com_set.SubjectBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.com_set.ComSubjectData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.AbstractActionFormFactory;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM201Form;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM201Servlet extends AbstractCMServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 共通処理
		super.execute(request, response);

		// 共通アクションフォーム - session scope
		CM001Form scform = (CM001Form)super.getCommonForm(request);
		// 個別アクションフォーム - session scope
		CM201Form siform = (CM201Form)scform.getActionForm("CM201Form");

		// ログイン情報
		LoginSession login = super.getLoginSession(request);
		// プロファイル
		Profile profile = super.getProfile(request);
		// 模試セッション
		ExamSession examSession = super.getExamSession(request);
		// 対象模試データ
		ExamData exam = examSession.getExamData(scform.getTargetYear(), scform.getTargetExam());

		// JSP
		if ("cm201".equals(getForward(request))) {
			// 個別アクションフォームの初期化
			if (siform == null) {
				siform = (CM201Form) AbstractActionFormFactory
										.getFactory("cm201").createActionForm(request);
				
				scform.setActionForm("CM201Form", siform);
			}

			Connection con = null; // DBコネクション
			
			try {
				con = super.getConnectionPool(request);

				// 型・科目Bean
				SubjectBean bean = new SubjectBean(login);
				bean.setConnection(null, con);
				bean.setMode("2");
				bean.setTargetYear(scform.getTargetYear());
				bean.setTargetExam(scform.getTargetExam());
				bean.setBundleCD(getBundleCd(login, profile, scform.getTargetExam()));
				bean.execute();
				request.setAttribute("SubjectBean", bean);

				// 個別選択がNULLなら初期値を設定する
				if (siform.getAnalyze2() == null) {
					ComSubjectData data = ProfileUtil.getCourseValue(
						profile,
						examSession,
						exam,
						bean,
						false);
					
					siform.setAnalyze2(ProfileUtil.getSubjectIndValue(data, false));
				}

			} catch (Exception e) {
				throw createServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}

			super.forward(request, response, JSP_CM201);

		// 不明なら転送
		} else {
			// アクションフォームを取得
			CM001Form rcform = (CM001Form) super.getActionForm(request,
				"jp.co.fj.keinavi.forms.com_set.CM001Form");
			CM201Form riform = (CM201Form) super.getActionForm(request,
				"jp.co.fj.keinavi.forms.com_set.CM201Form");

			// フォームの値をセッションに保持する
			siform.setSelection(riform.getSelection());
			siform.setAnalyze1(riform.getAnalyze1());
			siform.setGraph1(riform.getGraph1());

			// 個別選択の分析対象はNULLを入れない
			if (riform.getAnalyze2() == null) siform.setAnalyze2(new String[0]);
			else siform.setAnalyze2(riform.getAnalyze2());

			siform.setGraph2(riform.getGraph2());

			// 変更フラグ
			scform.setChanged(rcform.getChanged());

			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

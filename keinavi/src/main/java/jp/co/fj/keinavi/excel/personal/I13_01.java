/**
 * 個人成績分析−面談シート　連続個人成績表（単年連個）
 * 	Excelファイル編集
 * 作成日: 2004/09/15
 * @author	Ito.Y
 *
 * 2006/09/05   Totec)T.Yamada      [1]受験学力測定テスト対応
 *
 * 2009.11.30   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 *
 */

package jp.co.fj.keinavi.excel.personal;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.personal.I11Item;
import jp.co.fj.keinavi.excel.data.personal.I11ListBean;
import jp.co.fj.keinavi.excel.data.personal.I13HyoukaListBean;
import jp.co.fj.keinavi.excel.data.personal.I13KyokaSeisekiListBean;
import jp.co.fj.keinavi.excel.data.personal.I13SeisekiListBean;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class I13_01 {

    private int	noerror		= 0;	// 正常終了
    private int	errfread	= 1;	// ファイルreadエラー
    private int	errfwrite	= 2;	// ファイルwriteエラー
    private int	errfdata	= 3;	// データ設定エラー

    private CM		cm			= new CM();	// 共通関数用クラス インスタンス


    //2005.04.26 Add 新テスト対応
    //final private String	masterfile		= "I13_01";	// ファイル名
    final private String 	masterfile0 	= "I13_01";	// ファイル名１(新テスト対応用)
    final private String 	masterfile1 	= "I13_01";	// ファイル名２(新テスト対応用)
    private String masterfile = "";

    final private String[] cntMshCd 		= {"38"};		// センター模試コード
    final private String[] cntPreMshCd 	= {"04"};		// センタープレ模試コード
    final private String[] zntMrkMshCd 	= {"01","02","03"};		// 全統マーク模試コード
    final private String[] zntKjtMshCd 	= {"05","06","07"};		// 全統記述模試コード
    final private String[] zntShdMshCd 	= {"09","10"};	// 全統私大模試コード
    final private String[] zntNewMshCd 	= {"28","68","78"};	// 新テストコード

/*
 * 	Excel編集メイン
 * 		I11Item i11Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
     public int i13_01EditExcel(I11Item i11Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
        KNLog log = KNLog.getInstance(null,null,null);

        //2005.04.26 Add 新テスト対応
        //テンプレートの決定
        if (i11Item.getIntSeisekiShubetsuFlg() == 1){
            masterfile = masterfile1;
        } else{
            masterfile = masterfile0;
        }

        try {

            // データセット
            ArrayList	i11List			= i11Item.getI11List();
            Iterator	itr				= i11List.iterator();

            int		maxSheetIndex	= 0;	// シートカウンター

            // マスタExcel読み込み
            HSSFWorkbook	workbook	= null;
            HSSFSheet		workSheet	= null;
            HSSFRow			workRow		= null;
            HSSFCell		workCell	= null;

            // 基本ファイルを読込む
            I11ListBean i11ListBean = new I11ListBean();

            /** データリスト **/
            while( itr.hasNext() ) {
                i11ListBean = (I11ListBean) itr.next();

                // マスタExcel読み込み
                workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
                if( workbook==null ){
                    return errfread;
                }

                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
                // 2019/09/18 QQ)Oosaki 共通テスト対応 ADD START
                // セルスタイルを作成
//                HSSFCellStyle CellStyle = this.createStyle(workbook);
                // 2019/09/18 QQ)Oosaki 共通テスト対応 ADD END
                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START

                // シートテンプレートのコピー
                workSheet = workbook.cloneSheet(0);
                maxSheetIndex++;

                // ヘッダ右側に帳票作成日時を表示する
                cm.setHeader(workbook, workSheet);

                // セキュリティスタンプセット
                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntSeisekiSecuFlg() ,64 ,65 );
                //String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntSeisekiSecuFlg() ,68 ,69 );
                //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                //Update end
                //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                workCell = cm.setCell( workSheet, workRow, workCell, 0, 64 );
                //workCell = cm.setCell( workSheet, workRow, workCell, 0, 68 );
                //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                workCell.setCellValue(secuFlg);

                // 学校名セット
                workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
                workCell.setCellValue( "学校名　：" + cm.toString(i11ListBean.getStrGakkomei()) + "　学年：" + cm.toString(i11ListBean.getStrGrade())
                + "　クラス名：" + cm.toString(i11ListBean.getStrClass()) + "　クラス番号：" + cm.toString(i11ListBean.getStrClassNum()) );

                // 氏名・性別セット
                String strSex = "";
                if( cm.toString(i11ListBean.getStrSex()).equals("1") ){
                    strSex = "男";
                }else if( cm.toString(i11ListBean.getStrSex()).equals("2") ){
                    strSex = "女";
                }else{
                    strSex = "不明";
                }
                workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );

                if( !cm.toString(i11ListBean.getStrShimei()).equals("")){
                    workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrShimei()) + "（" + cm.toString(i11ListBean.getStrKana()) + "）　性別：" + strSex );
                }else{
                    workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrKana()) + "　性別：" + strSex );
                }


                /** 連続個人成績表リスト **/
                // データセット
                ArrayList i13SeisekiList = i11ListBean.getI13SeisekiList();
                Iterator itrI13Seiseki = i13SeisekiList.iterator();

                int mshRow = 0;
                int stMshRow = 0;
                int stMshCol = 0;
                int mrkMshRow = 0;
                int kjtMshRow = 0;
                int shdMshRow = 0;
                int opnMshRow = 0;
                while(itrI13Seiseki.hasNext()){
                    I13SeisekiListBean i13seisekiListBean = (I13SeisekiListBean)itrI13Seiseki.next();

                    // 模試グループ判定。
                    int mshHnteiFlg = 6; // 1:センターリサーチ 2:センタープレ 3:全統マーク模試 4:全統記述模試 5:全統私大模試 6:大学入試オープン 9:読み飛ばし用
                    for(int a=0;a<cntMshCd.length;a++){
                        if(cm.toString(i13seisekiListBean.getStrMshCd()).equals(cntMshCd[a])){
                            mshHnteiFlg = 1;
                            break;
                        }
                    }
                    for(int a=0;a<cntPreMshCd.length;a++){
                        if(cm.toString(i13seisekiListBean.getStrMshCd()).equals(cntPreMshCd[a])){
                            mshHnteiFlg = 2;
                            break;
                        }
                    }
                    for(int a=0;a<zntMrkMshCd.length;a++){
                        if(cm.toString(i13seisekiListBean.getStrMshCd()).equals(zntMrkMshCd[a])){
                            mshHnteiFlg = 3;
                            break;
                        }
                    }
                    for(int a=0;a<zntKjtMshCd.length;a++){
                        if(cm.toString(i13seisekiListBean.getStrMshCd()).equals(zntKjtMshCd[a])){
                            mshHnteiFlg = 4;
                            break;
                        }
                    }
                    for(int a=0;a<zntShdMshCd.length;a++){
                        if(cm.toString(i13seisekiListBean.getStrMshCd()).equals(zntShdMshCd[a])){
                            mshHnteiFlg = 5;
                            break;
                        }
                    }
                    // 2005.04.26 Add 新テスト用判定処理　追加
                    for(int a=0;a<zntNewMshCd.length;a++){
                        if(cm.toString(i13seisekiListBean.getStrMshCd()).equals(zntNewMshCd[a])){
                            //新テストは読み飛ばす
                            mshHnteiFlg = 9;
                            break;
                        }
                    }

                    //受験学力測定は読み飛ばす
                    if (KNUtil.TYPECD_ABILITY.equals(i13seisekiListBean.getStrMshTypeCd())) {
                        mshHnteiFlg = 9;
                    }

                    //2005.04.26 Update 新テスト対応
                    //					大学入試オープンの判定を別で用意し、elseまで来たデータはすべて読み飛ばす処理に変更
                    if(mshHnteiFlg == 1){				// センター
                    }else if(mshHnteiFlg == 2){		// センタープレ
                        stMshRow = 8;
                        stMshCol = 0;
                        mshRow = 0;
                    }else if(mshHnteiFlg == 3){		// 全統マーク
                        stMshRow = 12;
                        stMshCol = 1;
                        mshRow = mrkMshRow;
                        if(mshRow >= 3){
                            mshHnteiFlg = 9;
                        }
                    }else if(mshHnteiFlg == 4){		// 全統記述
                        stMshRow = 24;
                        stMshCol = 1;
                        mshRow = kjtMshRow;
                        if(mshRow >= 3){
                            mshHnteiFlg = 9;
                        }
                    }else if(mshHnteiFlg == 5){		// 全統私大
                        mshHnteiFlg = 5;
                        stMshRow = 36;
                        stMshCol = 1;
                        mshRow = shdMshRow;
                        if(mshRow >= 2){
                            mshHnteiFlg = 9;
                        }
                    }else if(mshHnteiFlg == 6){								// 大学入試オープン
                        mshHnteiFlg = 6;
                        stMshRow = 46;
                        stMshCol = 1;
                        mshRow = opnMshRow;
                        if(mshRow >= 6){
                            mshHnteiFlg = 9;
                        }
                    }else{
                        // ここに来たデータは全て読み飛ばす
                        mshHnteiFlg = 9;
                    }


                    if(mshHnteiFlg == 1){	// センターリサーチ用
                        // 対象模試セット
                        String moshi = cm.setTaisyouMoshi( i13seisekiListBean.getStrMshDate() );	// 模試月取得
                        workCell = cm.setCell( workSheet, workRow, workCell, 5, 0 );
                        workCell.setCellValue( cm.toString(i13seisekiListBean.getStrMshmei()) + moshi );

                        /** 教科別成績リスト **/
                        // データセット
                        ArrayList i13KyokaSeisekiList = i13seisekiListBean.getI13KyokaSeisekiList();
                        Iterator itrI13KyokaSeiseki = i13KyokaSeisekiList.iterator();

                        String strKykHoji = "";
                        int kyokaTabCol = 0;
                        int kyokaCol = 0;
                        int kyoukaCnt = 0;
                        while(itrI13KyokaSeiseki.hasNext()){
                            I13KyokaSeisekiListBean i13KyokaSeisekiListBean = (I13KyokaSeisekiListBean)itrI13KyokaSeiseki.next();

                            // 教科コードによって判定 → 2005.01.06 小論文は出力しない
                            String strKyouka = i13KyokaSeisekiListBean.getStrKyokaCd()+" ";
                            strKyouka = strKyouka.substring(0,1);
                            if( strKyouka.equals("6") == false ){
                                if( !strKyouka.equals(strKykHoji) ){
                                    switch( Integer.parseInt( strKyouka ) ){
                                        case 1:	//英語
                                            kyokaTabCol = 4;
                                            kyoukaCnt = 3;
                                            break;
                                        case 2:	//数学
                                            kyokaTabCol = 16;
                                            kyoukaCnt = 3;
                                            break;
                                        case 3:	//国語
                                            kyokaTabCol = 28;
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                            // 2019/08/02 Hics)Ueta 共通テスト対応 UPD START
                                            kyoukaCnt = 1;
                                            //kyoukaCnt = 2;
                                            // 2019/08/02 Hics)Ueta 共通テスト対応
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                            break;
                                        case 4: //理科
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                            // 2019/08/02 Hics)Ueta 共通テスト対応 UPD START
                                            kyokaTabCol = 32;
                                            //kyokaTabCol = 36;
                                            // 2019/08/02 Hics)Ueta 共通テスト対応 UPD END
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                            kyoukaCnt = 3;
                                            break;
                                        case 5:     //地歴公民
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                            // 2019/08/02 Hics)Ueta 共通テスト対応 UPD START
                                            kyokaTabCol = 44;
                                            //kyokaTabCol = 48;
                                            // 2019/08/02 Hics)Ueta 共通テスト対応 UPD END
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                            kyoukaCnt = 2;
                                            break;
                                        case 7:     //総合
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                            // 2019/08/02 Hics)Ueta 共通テスト対応 UPD START
                                            kyokaTabCol = 52;
                                            //kyokaTabCol = 56;
                                            // 2019/08/02 Hics)Ueta 共通テスト対応 UPD START
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                            kyoukaCnt = 1;
                                            break;
                                    }
                                    kyokaCol = 0;
                                    strKykHoji = strKyouka;
                                }
                                if (kyoukaCnt>0) {
                                    //理科の先頭科目が基礎科目なら、先頭は空欄にする
                                    if ("4".equals(strKyouka) && kyokaCol == 0 && "1".equals(i13KyokaSeisekiListBean.getBasicFlg())) {
                                        kyokaCol++;
                                    }

                                    // 科目名セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 5, kyokaTabCol+4*kyokaCol );
                                    workCell.setCellValue( i13KyokaSeisekiListBean.getStrKmkmei() );

                                    if (Integer.parseInt( strKyouka ) == 7){
                                        // 得点セット
                                        if(i13KyokaSeisekiListBean.getIntTokuten() != -999){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 7, kyokaTabCol+3*kyokaCol );
                                            workCell.setCellValue( i13KyokaSeisekiListBean.getIntTokuten() );
                                        }
                                        // 偏差値セット
                                        if(i13KyokaSeisekiListBean.getFloHensa() != -999.0){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 7, kyokaTabCol+3*kyokaCol+1 );
                                            workCell.setCellValue( i13KyokaSeisekiListBean.getFloHensa() );
                                        }
                                        // 学力レベル
                                        if(!"".equals(i13KyokaSeisekiListBean.getStrScholarLevel())){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 7, kyokaTabCol+3*kyokaCol+2 );
                                            workCell.setCellValue( i13KyokaSeisekiListBean.getStrScholarLevel() );
                                        }
                                    } else {
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
                                        //2019/09/20 QQ)Oosaki 共通テスト対応 ADD START
//                                        if(("3915").equals(i13KyokaSeisekiListBean.getSubCd())) {
//                                            workCell = cm.setCell( workSheet, workRow, workCell, 7, kyokaTabCol+4*kyokaCol+1 );
//                                            // 出力する際セルの中央に寄せる
//                                            workCell.setCellStyle(CellStyle);
//                                            workCell.setCellValue( i13KyokaSeisekiListBean.getKokugoTokuten() );
//                                        } else {
                                        //2019/09/20 QQ)Oosaki 共通テスト対応 ADD END
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END
                                            // 得点セット
                                            if(i13KyokaSeisekiListBean.getIntTokuten() != -999){
                                                workCell = cm.setCell( workSheet, workRow, workCell, 7, kyokaTabCol+4*kyokaCol+1 );
                                                workCell.setCellValue( i13KyokaSeisekiListBean.getIntTokuten() );
                                            }
                                            // 偏差値セット
                                            if(i13KyokaSeisekiListBean.getFloHensa() != -999.0){
                                                workCell = cm.setCell( workSheet, workRow, workCell, 7, kyokaTabCol+4*kyokaCol+2 );
                                                workCell.setCellValue( i13KyokaSeisekiListBean.getFloHensa() );
                                            }
                                            // 学力レベル
                                            if(!"".equals(i13KyokaSeisekiListBean.getStrScholarLevel())){
                                                workCell = cm.setCell( workSheet, workRow, workCell, 7, kyokaTabCol+4*kyokaCol+3 );
                                                workCell.setCellValue( i13KyokaSeisekiListBean.getStrScholarLevel() );
                                            }

                                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                                            // 2019/09/20 QQ)Oosaki 共通テスト対応 ADD START
                                            //CEFR出力
//                                            if(("01").equals(i13KyokaSeisekiListBean.getExcdtype())) {
//                                                    workCell = cm.setCell( workSheet, workRow, workCell, 5, 12 );
//                                                workCell.setCellValue( "CEFR" );
//                                                workCell = cm.setCell( workSheet, workRow, workCell, 7, 13 );
//                                                workCell.setCellStyle(CellStyle);
//                                                workCell.setCellValue( i13KyokaSeisekiListBean.getCefrScore() );
//                                            }
                                            // 2019/09/20 QQ)Oosaki 共通テスト対応 ADD END
                                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
//                                        }
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END
                                    }

                                }
                                kyoukaCnt--;
                                kyokaCol++;
                            }
                        }
                        //2005.01.13 センターリサーチのデータにも志望大学別評価データリストを表示
                        /** 志望大学別評価データリスト **/
                        // データセット
                        ArrayList i13HyoukaList = i13seisekiListBean.getI13HyoukaList();
                        Iterator itrI13Hyouka = i13HyoukaList.iterator();

                        int shiboRow = 0;
                        int shiboCol = 0;
                        while(itrI13Hyouka.hasNext()){
                            I13HyoukaListBean i13HyoukaListBean = (I13HyoukaListBean)itrI13Hyouka.next();

                            // 大学名セット
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                            // 2019/08/05 Hics)Ueta 共通テスト対応 UPD START
                            workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow+4, 57+5*shiboCol );
                            //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow+4, 61+5*shiboCol );
                            // 2019/08/05 Hics)Ueta 共通テスト対応 UPD END
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                            workCell.setCellValue( i13HyoukaListBean.getStrDaigakuMei() );

                            // 学部名セット
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                            // 2019/08/05 Hics)Ueta 共通テスト対応 UPD START
                            workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow+4, 58+5*shiboCol );
                            //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow+4, 62+5*shiboCol );
                            // 2019/08/05 Hics)Ueta 共通テスト対応 UPD END
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                            workCell.setCellValue( i13HyoukaListBean.getStrGakubuMei() );

                            // 学科名セット
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                            // 2019/08/05 Hics)Ueta 共通テスト対応 UPD START
                            workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow+4, 59+5*shiboCol );
                            //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow+4, 63+5*shiboCol );
                            // 2019/08/05 Hics)Ueta 共通テスト対応 UPD END
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                            workCell.setCellValue( i13HyoukaListBean.getStrGakkaMei() );

                            // 評価セット
                            if( cm.toString(i13HyoukaListBean.getStrHyoukaMark()).equals("") &&
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                                // 2019/08/02 Hics)Ueta 共通テスト対応 ADD START
                                //cm.toString(i13HyoukaListBean.getStrHyoukaMark_engpt()).equals("") &&
                                // 2019/08/02 Hics)Ueta 共通テスト対応 ADD END
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
                                cm.toString(i13HyoukaListBean.getStrHyoukaKijyutsu()).equals("") &&
                                cm.toString(i13HyoukaListBean.getStrHyoukaAll()).equals("") ){
                            }else{

                                String hyoka = "";
                                if( cm.toString(i13HyoukaListBean.getStrHyoukaMark()).equals("") == false ){
                                    // 2019/08/28 Hics)Ueta 共通テスト対応 UPD START
                                    //hyoka = i13HyoukaListBean.getStrHyoukaMark() + ":" ;
                                hyoka = cm.padRight(i13HyoukaListBean.getStrHyoukaMark(),"UTF-8",2) + ":";
                                // 2019/08/28 Hics)Ueta 共通テスト対応 UPD END
                            }else{
                                    // 2019/08/05 Hics)Ueta 共通テスト対応 UPD START
                                //hyoka = "　:" ;
                                hyoka = " " + " :" ;
                                // 2019/08/05 Hics)Ueta 共通テスト対応 UPD END
                            }

                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                            // 2019/08/05 Hics)Ueta 共通テスト対応 ADD START
//                            if( cm.toString(i13HyoukaListBean.getStrHyoukaMark_engpt()).equals("")  == false ){
//                                //2019/09/20 QQ)Oosaki 共通テスト対応 UPD START
//                                //hyoka += cm.padRight(i13HyoukaListBean.getStrHyoukaMark_engpt(),"UTF-8",2) + ":";
//                                hyoka += cm.padRight(i13HyoukaListBean.getStrHyoukaMark_engpt(),"UTF-8",2);
//                                //2019/09/20 QQ)Oosaki 共通テスト対応 UPD END
//                            }else{
//                                //2019/09/20 QQ)Oosaki 共通テスト対応 UPD START
//                                ////hyoka += "　:" ;
//                                //hyoka += " " + " " + ":" ;
//                                hyoka += "  ";
//                                //2019/09/20 QQ)Oosaki 共通テスト対応 UPD END
//                            }
//                            //2019/09/20 QQ)Oosaki 共通テスト対応 ADD START
//                            if( cm.toString(i13HyoukaListBean.getHyoukaJodge()).equals("")  == false ){
//                                hyoka += " " + i13HyoukaListBean.getHyoukaJodge();
//                            }else{
//                                hyoka += "   ";
//                            }
//                            if( cm.toString(i13HyoukaListBean.getHyoukaNotice()).equals("")  == false ){
//                                hyoka += cm.padRight(i13HyoukaListBean.getHyoukaNotice(),"UTF-8",2) + ":";
//                            }else{
//                                hyoka += "  :";
//                            }
                            //2019/09/20 QQ)Oosaki 共通テスト対応 ADD END
                            // 2019/08/05 Hics)Ueta 共通テスト対応 ADD END
                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END

                            if( cm.toString(i13HyoukaListBean.getStrHyoukaKijyutsu()).equals("") == false ){
                                // 2019/08/28 Hics)Ueta 共通テスト対応 UPD START
                                //hyoka += i13HyoukaListBean.getStrHyoukaKijyutsu() + ":" ;
                                hyoka += cm.padRight(i13HyoukaListBean.getStrHyoukaKijyutsu(),"UTF-8",2) + ":";
                                // 2019/08/28 Hics)Ueta 共通テスト対応 UPD END
                            }else{
                                    // 2019/08/05 Hics)Ueta 共通テスト対応 UPD START
                                //hyoka += "　:" ;
                                hyoka += " " + " " + ":" ;
                                // 2019/08/05 Hics)Ueta 共通テスト対応 UPD END
                            }
                            if( cm.toString(i13HyoukaListBean.getStrHyoukaAll()).equals("") == false ){
                                    // 2019/08/28 Hics)Ueta 共通テスト対応 UPD START
                                //hyoka += i13HyoukaListBean.getStrHyoukaAll() ;
                                hyoka += cm.padRight(i13HyoukaListBean.getStrHyoukaAll(), "UTF-8",2);
                                // 2019/08/28 Hics)Ueta 共通テスト対応 UPD END
                            }else{
                                    // 2019/08/05 Hics)Ueta 共通テスト対応 UPD START
                                //hyoka += " " ;
                                hyoka += " " + " " ;
                                // 2019/08/05 Hics)Ueta 共通テスト対応 UPD END
                            }
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                            // 2019/08/05 Hics)Ueta 共通テスト対応 UPD START
                            workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow+4, 60+5*shiboCol );
                            //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow+4, 64+5*shiboCol );
                            // 2019/08/05 Hics)Ueta 共通テスト対応 UPD END
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                            workCell.setCellValue( hyoka );
                            }

                            shiboRow++;
                            if(shiboRow >= 4){
                                shiboRow = 0;
                                shiboCol++;
                                if(shiboCol >= 2){
                                    break;
                                }
                            }
                        }

                    }else if((mshHnteiFlg >= 2)&&(mshHnteiFlg <= 6)){	// センタープレ・全統模試・大学オープン
                        // 対象模試セット
                        String moshi = cm.setTaisyouMoshi( i13seisekiListBean.getStrMshDate() );	// 模試月取得
                        workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+4*mshRow, stMshCol );
                        workCell.setCellValue( i13seisekiListBean.getStrMshmei() + moshi );

                        /** 教科別成績リスト **/
                        // データセット
                        ArrayList i13KyokaSeisekiList = i13seisekiListBean.getI13KyokaSeisekiList();
                        Iterator itrI13KyokaSeiseki = i13KyokaSeisekiList.iterator();

                        String strKykHoji = "";
                        int kyokaTabCol = 0;
                        int kyokaCol = 0;
                        int kyoukaCnt = 0;

                        //第一解答科目の成績
                        I13KyokaSeisekiListBean rikaSeiseki = null;
                        I13KyokaSeisekiListBean chikouSeiseki = null;

                        while(itrI13KyokaSeiseki.hasNext()){
                            I13KyokaSeisekiListBean i13KyokaSeisekiListBean = (I13KyokaSeisekiListBean)itrI13KyokaSeiseki.next();

                            // 教科コードによって判定 → 2005.01.06 小論文を出力する
                            String strKyouka = i13KyokaSeisekiListBean.getStrKyokaCd()+" ";
                            strKyouka = strKyouka.substring(0,1);
                            if( !strKyouka.equals(strKykHoji) ){
                                switch( Integer.parseInt( strKyouka ) ){
                                    case 1:     //英語
                                        kyokaTabCol = 4;
                                        // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD START
                                        //kyoukaCnt = 2;
                                        kyoukaCnt = 3;
                                        // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD END
                                        break;
                                    case 2:     //数学
                                        kyokaTabCol = 16;
                                        kyoukaCnt = 3;
                                        break;
                                    case 3:     //国語
                                        kyokaTabCol = 28;
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                        // 2019/08/02 Hics)Ueta 共通テスト対応 UPD START
                                        kyoukaCnt = 1;
                                        //kyoukaCnt = 2;
                                        // 2019/08/02 Hics)Ueta 共通テスト対応 UPD END
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                        break;
                                    case 4:     //理科
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                        // 2019/08/02 Hics)Ueta 共通テスト対応 UPD START
                                        kyokaTabCol = 32;
                                        //kyokaTabCol = 36;
                                        // 2019/08/02 Hics)Ueta 共通テスト対応 UPD END
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                        kyoukaCnt = 3;
                                        break;
                                    case 5:     //地歴公民
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                        // 2019/08/02 Hics)Ueta 共通テスト対応 UPD START
                                        kyokaTabCol = 44;
                                        //kyokaTabCol = 48;
                                        // 2019/08/02 Hics)Ueta 共通テスト対応 UPD END
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                        kyoukaCnt = 2;
                                        break;
                                    case 6:
                                        if( strKykHoji.equals("5") == true ){
                                        }else{
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                            // 2019/08/02 Hics)Ueta 共通テスト対応 UPD START
                                            kyokaTabCol = 44;
                                            //kyokaTabCol = 48;
                                            // 2019/08/02 Hics)Ueta 共通テスト対応 UPD END
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                            kyoukaCnt = 2;
                                            kyokaCol = 0;
                                            strKykHoji = strKyouka;
                                        }
                                        break;
                                    case 7:     //総合
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                        // 2019/08/02 Hics)Ueta 共通テスト対応 UPD START
                                        kyokaTabCol = 52;
                                        //kyokaTabCol = 56;
                                        // 2019/08/02 Hics)Ueta 共通テスト対応 UPD END
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                        kyoukaCnt = 1;
                                        break;
                                }
                                if( strKyouka.equals("6") == false ){
                                    kyokaCol = 0;
                                    strKykHoji = strKyouka;
                                }
                            }
                            if (kyoukaCnt>0) {
                                //理科の先頭科目が基礎科目なら、先頭は空欄にする
                                if ("4".equals(strKyouka) && kyokaCol == 0 && "1".equals(i13KyokaSeisekiListBean.getBasicFlg())) {
                                    kyokaCol++;
                                }

                                // 科目名セット
                                workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+4*mshRow, kyokaTabCol+4*kyokaCol );
                                workCell.setCellValue( i13KyokaSeisekiListBean.getStrKmkmei() );

                                if (Integer.parseInt( strKyouka ) == 7) {
                                    // 得点セット
                                    if(i13KyokaSeisekiListBean.getIntTokuten() != -999){
                                        workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+2+4*mshRow, kyokaTabCol+3*kyokaCol );
                                        workCell.setCellValue( i13KyokaSeisekiListBean.getIntTokuten() );
                                    }

                                    // 配点セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+2+4*mshRow, kyokaTabCol+3*kyokaCol+1 );
                                    workCell.setCellValue( i13KyokaSeisekiListBean.getStrHaitenKmk() );

                                    // 換算得点セット
                                    if(i13KyokaSeisekiListBean.getIntKansanTokuten() != -999){
                                        workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+3+4*mshRow, kyokaTabCol+3*kyokaCol );
                                        workCell.setCellValue( i13KyokaSeisekiListBean.getIntKansanTokuten() );
                                    }

                                    // 偏差値セット
                                    if(i13KyokaSeisekiListBean.getFloHensa() != -999.0){
                                        workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+3+4*mshRow, kyokaTabCol+3*kyokaCol+1 );
                                        workCell.setCellValue( i13KyokaSeisekiListBean.getFloHensa() );
                                    }

                                    // 学力レベルセット
                                    if(!"".equals(i13KyokaSeisekiListBean.getStrScholarLevel())){
                                        workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+3+4*mshRow, kyokaTabCol+3*kyokaCol+2 );
                                        workCell.setCellValue( i13KyokaSeisekiListBean.getStrScholarLevel() );
                                    }
                                } else {
                                    // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
                                  //2019/09/20 QQ)Oosaki 共通テスト対応 ADD START
//                                    if(("3915").equals(i13KyokaSeisekiListBean.getSubCd())) {
//                                        workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+2+4*mshRow, kyokaTabCol+4*kyokaCol+1 );
//                                        // 出力する際セルの中央に寄せる
//                                        workCell.setCellStyle(CellStyle);
//                                        workCell.setCellValue( i13KyokaSeisekiListBean.getKokugoTokuten() );
//                                    } else {
                                    //2019/09/20 QQ)Oosaki 共通テスト対応 ADD END
                                    // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
                                        // 得点セット
                                        if(i13KyokaSeisekiListBean.getIntTokuten() != -999){
                                            workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+2+4*mshRow, kyokaTabCol+4*kyokaCol+1 );
                                            workCell.setCellValue( i13KyokaSeisekiListBean.getIntTokuten() );
                                        }

                                        // 配点セット
                                        workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+2+4*mshRow, kyokaTabCol+4*kyokaCol+2 );
                                        workCell.setCellValue( i13KyokaSeisekiListBean.getStrHaitenKmk() );

                                        // 換算得点セット
                                        if(i13KyokaSeisekiListBean.getIntKansanTokuten() != -999){
                                            // 換算得点に*を表示
                                            // マーク模試の場合のみ、換算得点が第１解答科目＜第２解答科目の場合に*表示
                                            if (KNUtil.isAns1st(i13seisekiListBean.getStrMshCd())){
                                                if (cm.toString(strKyouka).equals("4") && i13KyokaSeisekiListBean.getIntScope() == 0 && !"1".equals(i13KyokaSeisekiListBean.getBasicFlg())){
                                                    if (rikaSeiseki != null){
                                                        if (rikaSeiseki.getIntKansanTokuten() < i13KyokaSeisekiListBean.getIntKansanTokuten()){
                                                            workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+3+4*mshRow, kyokaTabCol+4*kyokaCol );
                                                            workCell.setCellValue( "*" );
                                                        }
                                                    }
                                                }else if (cm.toString(strKyouka).equals("5") && i13KyokaSeisekiListBean.getIntScope() == 0){
                                                    if (chikouSeiseki != null){
                                                        if (chikouSeiseki.getIntKansanTokuten() < i13KyokaSeisekiListBean.getIntKansanTokuten()){
                                                            workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+3+4*mshRow, kyokaTabCol+4*kyokaCol );
                                                            workCell.setCellValue( "*" );
                                                        }
                                                    }
                                                }
                                            }
                                            workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+3+4*mshRow, kyokaTabCol+4*kyokaCol+1 );
                                            workCell.setCellValue( i13KyokaSeisekiListBean.getIntKansanTokuten() );
                                        }

                                        // 偏差値セット
                                        if(i13KyokaSeisekiListBean.getFloHensa() != -999.0){
                                            workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+3+4*mshRow, kyokaTabCol+4*kyokaCol+2 );
                                            workCell.setCellValue( i13KyokaSeisekiListBean.getFloHensa() );
                                        }

                                        // 学力レベルセット
                                        if(!"".equals(i13KyokaSeisekiListBean.getStrScholarLevel())){
                                            workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+3+4*mshRow, kyokaTabCol+4*kyokaCol+3 );
                                            workCell.setCellValue( i13KyokaSeisekiListBean.getStrScholarLevel() );
                                        }

                                        // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                                        // 2019/09/20 QQ)Oosaki 共通テスト対応 ADD START
                                        //CEFR出力
//                                        if(("01").equals(i13KyokaSeisekiListBean.getExcdtype())) {
//                                            workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+4*mshRow, 12 );
//                                            workCell.setCellValue( "CEFR" );
//                                            workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+2+4*mshRow, 13 );
//                                            workCell.setCellStyle(CellStyle);
//                                            workCell.setCellValue( i13KyokaSeisekiListBean.getCefrScore() );
//                                        }
                                        // 2019/09/20 QQ)Oosaki 共通テスト対応 ADD END
                                        // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END

                                        // 第１解答科目保存（マーク模試のみ）
                                        if (KNUtil.isAns1st(i13seisekiListBean.getStrMshCd())){
                                            if (cm.toString(strKyouka).equals("4") && i13KyokaSeisekiListBean.getIntScope() == 1){
                                                rikaSeiseki = i13KyokaSeisekiListBean;
                                            }else if (cm.toString(strKyouka).equals("5") && i13KyokaSeisekiListBean.getIntScope() == 1){
                                                chikouSeiseki = i13KyokaSeisekiListBean;
                                            }
                                        }
                                    // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
//                                    }
                                    // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END
                                }
                            }
                            kyoukaCnt--;
                            kyokaCol++;
                        }

                        if((mshHnteiFlg >= 2)&&(mshHnteiFlg <= 5)){
                            /** 志望大学別評価データリスト **/
                            // データセット
                            ArrayList i13HyoukaList = i13seisekiListBean.getI13HyoukaList();
                            Iterator itrI13Hyouka = i13HyoukaList.iterator();

                            int shiboRow = 0;
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                            // 2019/08/02 Hics)Ueta 共通テスト対応 UPD START
                            int shiboCol = 0;
                            //int shiboCol = 1;
                            // 2019/08/02 Hics)Ueta 共通テスト対応 UPD END
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                            while(itrI13Hyouka.hasNext()){
                                I13HyoukaListBean i13HyoukaListBean = (I13HyoukaListBean)itrI13Hyouka.next();

                                // 大学名セット
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                // 2019/08/02 Hics)Ueta 共通テスト対応 UPD START
                                workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow, 57+5*shiboCol );
                                //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow, 56+5*shiboCol );
                                // 2019/08/02 Hics)Ueta 共通テスト対応 UPD END
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                workCell.setCellValue( i13HyoukaListBean.getStrDaigakuMei() );

                                // 学部名セット
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                // 2019/08/02 Hics)Ueta 共通テスト対応 UPD START
                                workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow, 58+5*shiboCol );
                                //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow, 57+5*shiboCol );
                                // 2019/08/02 Hics)Ueta 共通テスト対応 UPD END
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                workCell.setCellValue( i13HyoukaListBean.getStrGakubuMei() );

                                // 学科名セット
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                // 2019/08/02 Hics)Ueta 共通テスト対応 UPD START
                                workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow, 59+5*shiboCol );
                                //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow, 58+5*shiboCol );
                                // 2019/08/02 Hics)Ueta 共通テスト対応 UPD END
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                workCell.setCellValue( i13HyoukaListBean.getStrGakkaMei() );

                                // 評価セット
                                if( cm.toString(i13HyoukaListBean.getStrHyoukaMark()).equals("") &&
                                    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                                    // 2019/08/02 Hics)Ueta 共通テスト対応 ADD START
                                    //cm.toString(i13HyoukaListBean.getStrHyoukaMark_engpt()).equals("") &&
                                    // 2019/08/02 Hics)Ueta 共通テスト対応 ADD END
                                    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
                                    cm.toString(i13HyoukaListBean.getStrHyoukaKijyutsu()).equals("") &&
                                    cm.toString(i13HyoukaListBean.getStrHyoukaAll()).equals("") ){
                                }else{

                                    String hyoka = "";
                                    // 2019/08/05 Hics)Ueta 共通テスト対応 UPD START
                                    if( cm.toString(i13HyoukaListBean.getStrHyoukaMark()).equals("") == false ){
                                        //2019/09/20 QQ)Oosaki 共通テスト対応 UPD START
                                        //// 2019/08/05 Hics)Ueta 共通テスト対応 ADD START
                                        //if( cm.toString(i13HyoukaListBean.getStrHyoukaMark()).length() == 1) {
                                        //    hyoka = i13HyoukaListBean.getStrHyoukaMark() + " " + ":" ;
                                        //}else if( cm.toString(i13HyoukaListBean.getStrHyoukaMark()).length() == 2){
                                        //    hyoka = i13HyoukaListBean.getStrHyoukaMark() + ":" ;
                                        //}
                                        //// 2019/08/05 Hics)Ueta 共通テスト対応 ADD END
                                        hyoka = cm.padRight(i13HyoukaListBean.getStrHyoukaMark(),"UTF-8",2) + ":";
                                        //2019/09/20 QQ)Oosaki 共通テスト対応 UPD END
                                    }else{
                                        // 2019/08/05 Hics)Ueta 共通テスト対応 UPD START
                                        //hyoka = "　:" ;
                                        hyoka = " " + " " + ":" ;
                                        // 2019/08/05 Hics)Ueta 共通テスト対応 UPD END
                                    }

                                    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                                    // 2019/08/05 Hics)Ueta 共通テスト対応 ADD START
//                                    if( cm.toString(i13HyoukaListBean.getStrHyoukaMark_engpt()).equals("")  == false ){
//                                        //2019/09/20 QQ)Oosaki 共通テスト対応 UPD START
//                                        //if( cm.toString(i13HyoukaListBean.getStrHyoukaMark_engpt()).length() == 1) {
//                                        //    hyoka += i13HyoukaListBean.getStrHyoukaMark_engpt() + " " + ":" ;
//                                        //}else if( cm.toString(i13HyoukaListBean.getStrHyoukaMark_engpt()).length() == 2){
//                                        //    hyoka += i13HyoukaListBean.getStrHyoukaMark_engpt() + ":" ;
//                                        //}
//                                        hyoka += cm.padRight(i13HyoukaListBean.getStrHyoukaMark_engpt(),"UTF-8",2);
//                                        //2019/09/20 QQ)Oosaki 共通テスト対応 UPD END
//                                    }else{
//                                        // 2019/08/05 Hics)Ueta 共通テスト対応 ADD END
//                                        //2019/09/20 QQ)Oosaki 共通テスト対応 UPD START
//                                        //// 2019/08/05 Hics)Ueta 共通テスト対応 UPD START
//                                        ////hyoka += "　:" ;
//                                        //hyoka += "A" + "G" + " " + "" + "△" + "！" + ":" ;
//                                        //// 2019/08/05 Hics)Ueta 共通テスト対応 UPD END
//                                        hyoka += "  ";
//                                        //2019/09/20 QQ)Oosaki 共通テスト対応 UPD END
//                                    }
//                                    //2019/09/20 QQ)Oosaki 共通テスト対応 ADD START
//                                    if( cm.toString(i13HyoukaListBean.getHyoukaJodge()).equals("")  == false ){
//                                        hyoka += " " + i13HyoukaListBean.getHyoukaJodge();
//                                    }else{
//                                        hyoka += "   ";
//                                    }
//                                    if( cm.toString(i13HyoukaListBean.getHyoukaNotice()).equals("")  == false ){
//                                        hyoka += cm.padRight(i13HyoukaListBean.getHyoukaNotice(),"UTF-8",2) + ":";
//                                    }else{
//                                        hyoka += "  :";
//                                    }
                                    //2019/09/20 QQ)Oosaki 共通テスト対応 ADD END
                                    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END

                                    if( cm.toString(i13HyoukaListBean.getStrHyoukaKijyutsu()).equals("") == false ){
                                        // 2019/08/05 Hics)Ueta 共通テスト対応 ADD START
                                        if( cm.toString(i13HyoukaListBean.getStrHyoukaKijyutsu()).length() == 1) {
                                            hyoka += i13HyoukaListBean.getStrHyoukaKijyutsu() + " " + ":" ;
                                        }else if( cm.toString(i13HyoukaListBean.getStrHyoukaKijyutsu()).length() == 2){
                                        // 2019/08/05 Hics)Ueta 共通テスト対応 ADD END
                                            hyoka += i13HyoukaListBean.getStrHyoukaKijyutsu() + ":" ;
                                        }
                                    }else{
                                        // 2019/08/05 Hics)Ueta 共通テスト対応 UPD START
                                        //hyoka += "　:" ;
                                        hyoka += " " + " " + ":" ;
                                        // 2019/08/05 Hics)Ueta 共通テスト対応 UPD END
                                    }
                                    if( cm.toString(i13HyoukaListBean.getStrHyoukaAll()).equals("") == false ){
                                        // 2019/08/05 Hics)Ueta 共通テスト対応 ADD START
                                        if( cm.toString(i13HyoukaListBean.getStrHyoukaAll()).length() == 1) {
                                            hyoka += i13HyoukaListBean.getStrHyoukaAll() + " " ;
                                        }else if( cm.toString(i13HyoukaListBean.getStrHyoukaAll()).length() == 2){
                                        // 2019/08/05 Hics)Ueta 共通テスト対応 ADD END
                                            hyoka += i13HyoukaListBean.getStrHyoukaAll() ;
                                        }
                                    }else{
                                        // 2019/08/05 Hics)Ueta 共通テスト対応 UPD START
                                       // hyoka += " " ;
                                       hyoka += " " + " ";
                                       // 2019/08/05 Hics)Ueta 共通テスト対応 UPD END
                                    }
                                    // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                    // 2019/08/05 Hics)Ueta 共通テスト対応 UPD START
                                    workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow, 60+5*shiboCol );
                                    //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+shiboRow+4*mshRow, 59+5*shiboCol );
                                    // 2019/08/05 Hics)Ueta 共通テスト対応 UPD END
                                    // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                    workCell.setCellValue( hyoka );
                                }

                                shiboRow++;
                                if(shiboRow >= 4){
                                    shiboRow = 0;
                                    shiboCol++;
                                    // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD START
                                    if(shiboCol >= 2){
                                        break;
                                    }
//                                    if(shiboCol >= 3){
//                                        break;
//                                    }
                                    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD END
                                    // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                }
                            }
                        }else if(mshHnteiFlg == 6){ // 大学入試オープン用
                            /** 志望大学別評価データリスト **/
                            // データセット
                            ArrayList i13HyoukaList = i13seisekiListBean.getI13HyoukaList();
                            Iterator itrI13Hyouka = i13HyoukaList.iterator();

                            int shiboCol = 0;
                            while(itrI13Hyouka.hasNext()){
                                I13HyoukaListBean i13HyoukaListBean = (I13HyoukaListBean)itrI13Hyouka.next();

                                //2005.01.17 Update Colの値に+2
                                // 大学名セット
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD START
                                workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+4*mshRow, 56+5*shiboCol );
                                //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+4*mshRow, 60+5*shiboCol );
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD END
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                workCell.setCellValue( i13HyoukaListBean.getStrDaigakuMei() );

                                // 学部名セット
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD START
                                workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+4*mshRow, 58+5*shiboCol );
                                //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+4*mshRow, 62+5*shiboCol );
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD END
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                workCell.setCellValue( i13HyoukaListBean.getStrGakubuMei() );

                                // 学科名セット
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD START
                                workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+1+4*mshRow, 56+5*shiboCol );
                                //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+1+4*mshRow, 60+5*shiboCol );
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD END
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD WNS
                                workCell.setCellValue( i13HyoukaListBean.getStrGakkaMei() );

                                // 定員セット
                                if ("8".equals(i13HyoukaListBean.getStrTeiinRel()) || i13HyoukaListBean.getIntTeiin() == 0 || i13HyoukaListBean.getIntTeiin() == -999) {
                                    // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD START
                                    workCell = cm.setCell( workSheet, workRow, workCell, stMshRow + 3 + 4 * mshRow, 58 + 5 * shiboCol - 2);
                                    //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow + 3 + 4 * mshRow, 62 + 5 * shiboCol - 2);
                                    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD END
                                    // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                    workCell.setCellValue( "" );
                                } else {
                                    // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD START
                                    workCell = cm.setCell( workSheet, workRow, workCell, stMshRow + 3 + 4 * mshRow, 58 + 5 * shiboCol );
                                    //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow + 3 + 4 * mshRow, 62 + 5 * shiboCol );
                                    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD END
                                    // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                    workCell.setCellValue( ("9".equals(i13HyoukaListBean.getStrTeiinRel()) ? "推定" : "") + i13HyoukaListBean.getIntTeiin());
                                }

                                // 評価セット
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD START
                                workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+4*mshRow, 60+5*shiboCol );
                                //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+4*mshRow, 64+5*shiboCol );
                                workCell.setCellValue( i13HyoukaListBean.getStrHyoukaMark() );

                                workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+1+4*mshRow, 60+5*shiboCol );
                                //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+1+4*mshRow, 64+5*shiboCol );
                                workCell.setCellValue( i13HyoukaListBean.getStrHyoukaKijyutsu() );

                                workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+2+4*mshRow, 60+5*shiboCol );
                                //workCell = cm.setCell( workSheet, workRow, workCell, stMshRow+2+4*mshRow, 64+5*shiboCol );
                                workCell.setCellValue( i13HyoukaListBean.getStrHyoukaAll() );
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD END
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END

                                shiboCol++;
                                if(shiboCol >= 2){
                                    break;
                                }
                            }
                        }
                        mshRow++;
                        if(mshHnteiFlg == 6){		// 大学入試オープン
                            opnMshRow = mshRow;
                        }else{
                            for(int a=0;a<zntMrkMshCd.length;a++){
                                if(cm.toString(i13seisekiListBean.getStrMshCd()).equals(zntMrkMshCd[a])){// 全統マーク模試
                                    mrkMshRow = mshRow;
                                    break;
                                }
                            }
                            for(int a=0;a<zntKjtMshCd.length;a++){
                                if(cm.toString(i13seisekiListBean.getStrMshCd()).equals(zntKjtMshCd[a])){// 全統記述模試
                                    kjtMshRow = mshRow;
                                    break;
                                }
                            }
                            for(int a=0;a<zntShdMshCd.length;a++){
                                if(cm.toString(i13seisekiListBean.getStrMshCd()).equals(zntShdMshCd[a])){// 全統私大模試
                                    shdMshRow = mshRow;
                                    break;
                                }
                            }
                        }
                    }
                }
                // Excelファイル保存
                boolean bolRet = false;
                String strFileName = masterfile + "_" + cm.toString(i11ListBean.getStrGrade()) + cm.toString(i11ListBean.getStrClass()) + cm.toString(i11ListBean.getStrClassNum());
                    bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, strFileName, maxSheetIndex);

                if( bolRet == false ){
                    return errfwrite;
                }

            }	// while( itr.hasNext() )

        } catch(Exception e) {
            log.Err("I13_01","データセットエラー",e);
            return errfdata;
        }

        return noerror;
    }

     // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
     // 2019/09/20 QQ)Oosaki 共通テスト対応 ADD START
//     /**
//      * スタイルを作成する
//      * @param  workbook - ワークブック
//      * @return style    - スタイル
//      */
//     private HSSFCellStyle createStyle(HSSFWorkbook workbook) {
//         // スタイルを新規作成
//         HSSFCellStyle style = workbook.createCellStyle();
//
//         // フォントを定義する
//         HSSFFont font = workbook.createFont();
//         // フォント名
//         font.setFontName("ＭＳ ゴシック");
//         // フォントサイズ
//         font.setFontHeightInPoints( (short)10 );
//         // 設定
//         style.setFont(font);
//         // 中央揃え
//         style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
//
//         return style;
//     }
     // 2019/09/20 QQ)Oosaki 共通テスト対応 ADD END
     // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END

}
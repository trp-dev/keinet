package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import jp.co.fj.keinavi.data.individual.PlanUnivData2;
import jp.co.fj.keinavi.data.individual.ScheduleDetailData;
import jp.co.fj.keinavi.util.db.RecordProcessor;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import com.fjh.beans.DefaultSearchBean;

/**
 *
 * 受験予定大学取得
 * 
 * 2004.10.12	Tomohisa YAMADA - TOTEC
 * 				[新規作成]
 * 
 * <2010年度改修>
 * 2009.11.26	Tomohisa YAMADA - TOTEC
 * 				入試日程変更対応
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class PlanUnivSearchBean2 extends DefaultSearchBean{	
	
	private static final SimpleDateFormat formatter = new SimpleDateFormat("M/d");
	
	/**
	 * 個人ID
	 */
	private String individualId;

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		if (individualId == null) {
			throw new InternalError("個人IDが未設定です。");
		}
		
		Query query = QueryLoader.getInstance().load("i09");

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{

			pstmt = conn.prepareStatement(query.toString());
			pstmt.setString(1, individualId);
			
			rs = pstmt.executeQuery();
			
			Map tempMap = new TreeMap();
			
			while(rs.next()){
				PlanUnivData2 temp = new PlanUnivData2();
				//UNIQUE KEY
				temp.setUnivCd(rs.getString("UNIVCD"));//大学コード
				temp.setDeptCd(rs.getString("DEPTCD"));//学部コード
				temp.setFacultyCd(rs.getString("FACULTYCD"));//学科コードs
				temp.setUnivNameAbbr(rs.getString("UNIVNAME_ABBR"));//大学名
				temp.setFacultyNameAbbr(rs.getString("FACULTYNAME_ABBR"));//学部名
				temp.setDeptNameAbbr(rs.getString("DEPTNAME_ABBR"));//学科名
				temp.setDeptSortKey(rs.getString("DEPTSORTKEY"));//学科ソートキー
				temp.setScheduleCd(rs.getString("SCHEDULECD"));//日程コード（１はセンター）
				temp.setCenScoreRate(rs.getDouble("CEN_SCORERATE"));//得点率センターボーダー(難易度ランク)
				temp.setRankLLimits(rs.getDouble("RANKLLIMITS"));//偏差値難易度ランク(難易度ランク)
				temp.setEntExamModeCd(rs.getString("ENTEXAMMODECD"));//入試形態コード
				temp.setEntExamModeName(rs.getString("ENTEXAMMODENAME"));//入試形態名
				temp.setNatlPvtDiv(rs.getString("NATLPVTDIV"));//国私区分
				temp.setFlag_night(rs.getInt("UNINIGHTDIV"));//夜間フラグ(大学夜間区分)
				temp.setFacultyConCd(rs.getString("FACULTYCONCD"));//学部内容コード
				temp.setDeptSerialNo(rs.getString("DEPTSERIAL_NO"));//学科通番
				temp.setScheduleSys(rs.getString("SCHEDULESYS"));//日程方式
				temp.setScheduleSysBranchCd(rs.getString("SCHEDULESYSBRANCHCD"));//日程方式枝番
				temp.setSubNameKana(rs.getString("DEPTNAME_KANA"));//学科カナ名
				temp.setKanaNum(rs.getString("KANA_NUM"));//カナ付50音順番号
				
				PlanUnivData2 data = 
					(PlanUnivData2) tempMap.get(temp.getUniqueKey()+temp.getEntExamModeCd());

				if(data == null){
					data = temp;
					tempMap.put(data.getUniqueKey()+temp.getEntExamModeCd(), data);
				}
				
				data = (PlanUnivData2) tempMap.get(temp.getUniqueKey()+temp.getEntExamModeCd());
				
				PlanUnivData2 pud = new PlanUnivData2();
				
				pud.setEntExamDiv1(rs.getString("ENTEXAMDIV1"));
				pud.setEntExamDiv2(rs.getString("ENTEXAMDIV2"));
				pud.setEntExamDiv3(rs.getString("ENTEXAMDIV3"));
				
				//入試日程が一般の時のみ試験日、出願締切日、合格発表日、手続日を表示する
				// 11/21
				if(temp.getEntExamModeCd().equals("01")){
					
					//出願締切日(m/d)
					if (rs.getString("ENTEXAMAPPLIDEADLINE") != null) {
						pud.setAppliDeadline(formatter.format(ScheduleDetailData.formatter.parse(rs.getString("ENTEXAMAPPLIDEADLINE"))));
					}
					
					//合格発表日(m/d)
					if (rs.getString("SUCANNDATE") != null) {
						pud.setSucAnnDate(formatter.format(ScheduleDetailData.formatter.parse(rs.getString("SUCANNDATE"))));
					}
					
					//入学手続締切日(m/d)
					if (rs.getString("PROCEDEADLINE") != null) {
						pud.setProceDeadLine(formatter.format(ScheduleDetailData.formatter.parse(rs.getString("PROCEDEADLINE"))));
					}
					
					final ScheduleDetailData schedule = 
						new ScheduleDetailData(
							rs.getString("ENTEXAMINPLEDATE1_1") == null ? null : ScheduleDetailData.formatter.parse(rs.getString("ENTEXAMINPLEDATE1_1")), 
							rs.getString("ENTEXAMINPLEDATE1_2") == null ? null : ScheduleDetailData.formatter.parse(rs.getString("ENTEXAMINPLEDATE1_2")), 
							rs.getString("ENTEXAMINPLEDATE1_3") == null ? null : ScheduleDetailData.formatter.parse(rs.getString("ENTEXAMINPLEDATE1_3")), 
							rs.getString("ENTEXAMINPLEDATE1_4") == null ? null : ScheduleDetailData.formatter.parse(rs.getString("ENTEXAMINPLEDATE1_4")), 
							rs.getString("PLEDATEDIV1_1"), 
							rs.getString("PLEDATEDIV1_2"), 
							rs.getString("PLEDATEDIV1_3"), 
				            rs.getString("ENTEXAMINPLEDATE2_1") == null ? null : ScheduleDetailData.formatter.parse(rs.getString("ENTEXAMINPLEDATE2_1")), 
				            rs.getString("ENTEXAMINPLEDATE2_2") == null ? null : ScheduleDetailData.formatter.parse(rs.getString("ENTEXAMINPLEDATE2_2")), 
				            rs.getString("ENTEXAMINPLEDATE2_3") == null ? null : ScheduleDetailData.formatter.parse(rs.getString("ENTEXAMINPLEDATE2_3")), 
				            rs.getString("ENTEXAMINPLEDATE2_4") == null ? null : ScheduleDetailData.formatter.parse(rs.getString("ENTEXAMINPLEDATE2_4")), 
				            rs.getString("PLEDATEDIV2_1"), 
				            rs.getString("PLEDATEDIV2_2"), 
				            rs.getString("PLEDATEDIV2_3"), 
				            rs.getString("PLEDATEUNITEDIV"),
				            null, //String branchName,
				            Integer.parseInt(rs.getString("ENTEXAMDIV1")),
				            Integer.parseInt(rs.getString("ENTEXAMDIV2")),
				            Integer.parseInt(rs.getString("ENTEXAMDIV3")));
					
					pud.setSchedule(schedule);

				}
				
				// 受験日
				//とりあえず文字列で取得する						
				String[] examDatesStr = new String[]{	
												rs.getString("EXAMPLANDATE1"),rs.getString("EXAMPLANDATE2"),
												rs.getString("EXAMPLANDATE3"),rs.getString("EXAMPLANDATE4"),
												rs.getString("EXAMPLANDATE5"),rs.getString("EXAMPLANDATE6"),
												rs.getString("EXAMPLANDATE7"),rs.getString("EXAMPLANDATE8"),
												rs.getString("EXAMPLANDATE9"),rs.getString("EXAMPLANDATE10")};
				//nullの場合は加えない			
				List examDateList = new ArrayList();
				for(int i=0; i<examDatesStr.length; i++){
					if(examDatesStr[i] != null && examDatesStr[i].length() > 0 && !examDatesStr[i].trim().equals("0")){
						examDateList.add(examDatesStr[i]);
					}
				}
												
				//mmdd型のintとしてセットする
				if(examDateList != null && examDateList.size() > 0){
					int[] examDate = new int[examDateList.size()];
					String[] examDateStr = new String[examDateList.size()];
					for(int i=0; i<examDateList.size(); i++){
						examDate[i] = Integer.parseInt((String) examDateList.get(i));
						examDateStr[i] = RecordProcessor.dateWSlash((String) examDateList.get(i), true);
					}
					/** 受験日 mmddの型 */
					pud.setExamPlanDate(examDate);
					/** 受験日 m/dの型 */
					pud.setExamPlanDateStr(examDateStr);
				}
				
				/** 受験地 */
				List examSites = new ArrayList();
				examSites.add(rs.getString("PROVENTEXAMSITE1"));
				examSites.add(rs.getString("PROVENTEXAMSITE2"));
				examSites.add(rs.getString("PROVENTEXAMSITE3"));
				examSites.add(rs.getString("PROVENTEXAMSITE4"));
				examSites.add(rs.getString("PROVENTEXAMSITE5"));
				examSites.add(rs.getString("PROVENTEXAMSITE6"));
				examSites.add(rs.getString("PROVENTEXAMSITE7"));
				examSites.add(rs.getString("PROVENTEXAMSITE8"));
				examSites.add(rs.getString("PROVENTEXAMSITE9"));
				examSites.add(rs.getString("PROVENTEXAMSITE10"));
				pud.setProvEntExamSite((String[]) examSites.toArray(new String[10]));
				
				//★受験日が存在する区分のみ表示する
				if(examDateList.size() > 0 || "1".equals(temp.getScheduleCd())){
					data.getPlanMap().put(pud.getEntExamModeCd()+pud.getEntExamDivKey(), pud);
				}
				//recordSet.add(data);
			}
			if(tempMap != null && tempMap.size() > 0){
				Set set = tempMap.entrySet();
				Iterator ite = set.iterator();
				while(ite.hasNext()){
					Map.Entry me = (Map.Entry) ite.next();
					PlanUnivData2 pud2 = (PlanUnivData2) me.getValue();
					recordSet.add(pud2);
				}
			}
			
		}catch(SQLException sqle){
			sqle.printStackTrace();
			System.out.println(query);
			throw sqle;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
		
	}

	/**
	 * 個人IDを設定する
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

}

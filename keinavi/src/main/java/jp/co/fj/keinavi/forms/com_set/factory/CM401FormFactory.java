/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.com_set.factory;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM401Form;
import jp.co.fj.keinavi.interfaces.IProfileItem;

import com.fjh.forms.ActionForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM401FormFactory extends AbstractCMFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		CM401Form form = new CM401Form();

		// アイテムマップ
		Map item = getItemMap(request);

		// 選択方式
		form.setSelection(
			item.get(IProfileItem.UNIV_SELECTION_FORMULA).toString()
		);

		// 大学コード
		form.setUniv((String[]) item.get(IProfileItem.UNIV));

		return form;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アイテムマップ
		Map item = getItemMap(request);
		// 共通アクションフォーム
		CM001Form f001 = super.getCommonForm(request);
		// 個別アクションフォーム
		CM401Form f401 = (CM401Form)f001.getActionForm("CM401Form");

		// 選択方式
		item.put(
			IProfileItem.UNIV_SELECTION_FORMULA,
			Short.valueOf(f401.getSelection())
		);

		// 志望大学（個別選択）
		if ("2".equals(f401.getSelection())) {
			item.put(IProfileItem.UNIV, f401.getUniv());
		}
	}

}

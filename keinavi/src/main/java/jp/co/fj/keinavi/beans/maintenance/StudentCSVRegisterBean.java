package jp.co.fj.keinavi.beans.maintenance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import jp.co.fj.keinavi.beans.admin.IndivChangeInfoTableAccess;
import jp.co.fj.keinavi.beans.admin.name.NameBringsNear;
import jp.co.fj.keinavi.beans.maintenance.module.StudentDataNormalizer;
import jp.co.fj.keinavi.beans.maintenance.module.StudentDataValidator;
import jp.co.fj.keinavi.data.maintenance.KNHistoryData;
import jp.co.fj.keinavi.data.maintenance.KNStudentData;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.csv.CSVLineRegisterBean;
import jp.co.fj.keinavi.util.csv.data.CSVLineRecord;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

/**
 *
 * 生徒情報一括登録Bean
 * 
 * 2006.11.02	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class StudentCSVRegisterBean extends CSVLineRegisterBean {

	// 現年度
	private final int currentYear = Integer.parseInt(KNUtil.getCurrentYear());
	// 生徒登録Bean
	private final StudentInsertBean bean;
	// 履歴重複チェック用
	private final Set historySet = new HashSet(); 
	
	// wrapperクラスインスタンス
	private final StudentData data = new StudentData();
	// データ正規化モジュール
	private final StudentDataNormalizer normalizer = StudentDataNormalizer.getInstance();
	// データチェックモジュール
	private final StudentDataValidator validator = StudentDataValidator.getInstance();
	
	/**
	 * @param pSchoolCd 学校コード
	 */
	public StudentCSVRegisterBean(final String pSchoolCd) {
		bean = new StudentInsertBean(pSchoolCd);
	}
	
	/**
	 * @see com.fjh.beans.DefaultBean
	 * 		#setConnection(java.lang.String, java.sql.Connection)
	 */
	public void setConnection(final String DBName, final Connection conn) {
		super.setConnection(DBName, conn);
		bean.setConnection(DBName, conn);
	}

	/**
	 * @see jp.co.fj.keinavi.util.csv.CSVLineRegisterBean
	 * 		#normalizeRecord(java.lang.String[])
	 */
	public String[] normalizeRecord(final String[] record) {
		
		data.init(record);
		
		// 個人ID
		data.setIndividualId(
				normalizer.normalizeIndividualId(data.getIndividualId()));
		// クラス
		data.setClassName(
				normalizer.normalizeClassName(data.getClassName()));
		// クラス番号
		data.setClassNo(
				normalizer.normalizeClassNo(data.getClassNo()));
		// カナ氏名
		data.setNameKana(
				normalizer.normalizeNameKana(data.getNameKana()));
		// 生年月日
		data.setBirthday(
				normalizer.normalizeBirthday(data.getBirthdayString()));
		// 電話番号
		data.setTelNo(
				normalizer.normalizeTelNo(data.getTelNo()));
		// 結合状態フラグ
		data.setIdentFlag("");
		
		return record;
	}
	
	/**
	 * @throws SQLException 
	 * @see jp.co.fj.keinavi.util.csv.CSVLineRegisterBean
	 * 		#validateAllRecord(java.util.List)
	 */
	public void validateAllRecord(
			final List lineRecordList) throws SQLException {
		
		// 個人IDの処理
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					"SELECT 1 FROM basicinfo bi "
					+ "WHERE bi.individualid = ? AND bi.schoolcd = ?");
			ps.setString(2, bean.getSchoolCd());
			
			for (final Iterator ite = lineRecordList.iterator(); ite.hasNext();) {
				
				final CSVLineRecord record = (CSVLineRecord) ite.next();
				
				// エラーがあるなら飛ばす
				if (record.hasError()) {
					continue;
				}
				
				data.init(record.getRecord());
				bean.setStudent(data);
				
				// 個人IDがなければ名寄せ
				if (StringUtils.isEmpty(data.getIndividualId())) {
					final NameBringsNear name = bean.identify();
					// 結合状態保持
					data.setIdentFlag(name.getCombiJudge() + "");
					// 進級または編集なら個人IDは決まってる
					if (name.getCombiJudge() < 3) {
						data.setIndividualId((String) name.getResult().get(0));
					}
				// 更新レコードなら個人IDのチェック
				} else {
					ps.setString(1, data.getIndividualId());
					rs = ps.executeQuery();
					if (!rs.next()) {
						record.addError("個人IDが不正です。");
					}
					rs.close();
				}
				
				// 履歴情報重複チェック
				// ※CSVに個人IDがある場合はチェックしない
				if ((StringUtils.isNotEmpty(data.getIdentFlag())
						|| StringUtils.isEmpty(data.getIndividualId()))
						&& bean.isDuplication(data.getIndividualId(),
						data.getYear(), data.getGrade(),
						data.getClassName(), data.getClassNo())) {
					record.addError(
							"同じ年度・学年・クラス・クラス番号の生徒が既に登録されています。");
				}
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.util.csv.CSVLineRegisterBean
	 * 		#validateRecord(jp.co.fj.keinavi.util.csv.data.CSVLineRecord)
	 */
	public void validateRecord(final CSVLineRecord record) throws SQLException {
		
		data.init(record.getRecord());
		
		// 年度
		if (!validator.isValidYear(data.getYear(), currentYear)) {
			record.addError("年度が不正です。");
		}
		
		// 学年
		if (!validator.isValidGrade(data.getGradeString())) {
			record.addError("学年が不正です。");
		}
		
		// クラス
		if (!validator.isValidClassName(data.getClassName())) {
			record.addError("クラスが不正です。");
		}
		
		// クラス番号
		if (!validator.isValidClassNo(data.getClassNo())) {
			record.addError("クラス番号が不正です。");
		}
		
		// ここまでのデータが正しいなら重複チェック
		if (!record.hasError()) {
			// 履歴データを作る
			final KNHistoryData history = new KNHistoryData(
					data.getYear(), data.getGradeString(),
					data.getClassName(), data.getClassNo());
			// ファイル内での整合性チェック
			if (!historySet.add(history)) {
				record.addError(
					"同じ年度・学年・クラス・クラス番号の生徒は登録できません。");
			}
		}
		
		// かな氏名
		if (!validator.isValidKanaName(data.getNameKana())) {
			record.addError("かな氏名が不正です。");
		}
		
		// 漢字氏名
		if (!validator.isValidKanjiName(data.getNameKanji())) {
			record.addError("漢字氏名が不正です。");
		}
		
		// 性別
		if (!validator.isValidSex(data.getSex())) {
			record.addError("性別が不正です。");
		}
		
		// 生年月日
		if (!validator.isValidBirthday(data.getBirthdayString())) {
			record.addError("生年月日が不正です。");
		}

		// 電話番号
		if (!validator.isValidTelNo(data.getTelNo())) {
			record.addError("電話番号が不正です。");
		}

		// 個人ID
		if (!validator.isValidIndividualId(data.getIndividualId())) {
			record.addError("個人IDが不正です。");
		}
	}

	/**
	 * @see jp.co.fj.keinavi.util.csv.CSVLineRegisterBean
	 * 		#judgeRecordType(jp.co.fj.keinavi.util.csv.data.CSVLineRecord)
	 */
	public void judgeRecordType(final CSVLineRecord record) throws SQLException {
		
		data.init(record.getRecord());
		
		// 個人IDの有無で新規・更新は分かれる
		// 新規
		if (StringUtils.isEmpty(data.getIndividualId())) {
			record.setInsertRecord();
		// 更新
		} else {
			record.setUpdateRecord();
			// 更新前レコードの取得
			record.setOriginal(getOriginalRecord(
					data.getIndividualId(), data.getYear()));
		}
	}

	// 更新前レコードを取得する
	private String[] getOriginalRecord(
			final String individualId, final String year) throws SQLException {

		final String[] value = new String[11];
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("m17"));
			ps.setString(1, year);
			ps.setString(2, individualId);
			rs = ps.executeQuery();
			if (rs.next()) {
				value[1] = rs.getString(1);
				value[2] = rs.getString(2);
				value[3] = rs.getString(3);
				value[4] = rs.getString(4);
			}
			return value;
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.util.csv.CSVLineRegisterBean
	 * 		#registRecord(jp.co.fj.keinavi.util.csv.data.CSVLineRecord)
	 */
	public void registRecord(final CSVLineRecord record) throws SQLException {
		
		data.init(record.getRecord(), record.getOriginal());
		bean.setStudent(data);
		
		// 確認画面から来たならデータベースとの整合性チェック
		// ※CSVに個人IDがある場合はチェックしない
		if ((StringUtils.isNotEmpty(data.getIdentFlag())
				|| StringUtils.isEmpty(data.getIndividualId()))
				&& bean.isDuplication(data.getIndividualId(),
				data.getYear(), data.getGrade(),
				data.getClassName(), data.getClassNo())) {
			record.addError(
					"同じ年度・学年・クラス・クラス番号の生徒が既に登録されています。");
		}
	
		if (record.isInsertRecord()) {
			insert();
		} else {
			update();
			//更新：変更成績情報テーブル
			IndivChangeInfoTableAccess.updateIndividual(conn, data.getIndividualId());
		}
		
		// 識別マーク更新
		bean.updateCMark();
	}

	// 新規ルート
	private void insert() throws SQLException {
		// 個人IDの生成
		data.setIndividualId(bean.createIndividualId(data.getYear()));
		// 学籍基本情報の登録
		bean.insertBasicinfo();
		// 学籍履歴情報の登録
		bean.insertHistoryinfo();
		// 新規登録ログ
		bean.insertLog(data.getIndividualId(), data.getYear(),
				data.getGrade(), data.getClassName(),
				data.getClassNo(), data.getNameKana());
	}

	// 更新ルート
	private void update() throws SQLException {
		
		// 名寄せ進級は履歴のみ作る
		if ("2".equals(data.getIdentFlag())) {
			bean.insertHistoryinfo();
		// それ以外
		} else {
			// 学籍基本情報の更新に成功後、
			// 学籍履歴情報の更新に失敗したら登録する（進級処理）
			if (bean.updateBasicinfo() && !bean.updateHistoryinfo(
					data.getIndividualId(), data.getYear(),
					data.getGrade(), data.getClassName(),
					data.getClassNo(), data.getOCurrentGrade(),
					data.getOCurrentClassName())) {
				bean.insertHistoryinfo();
			// 履歴情報を更新する場合は集計学年・クラスを更新
			} else {
				bean.updateCntGrade();
			}
		}
		
		// 更新ログ
		bean.updateLog(data.getIndividualId(), data.getYear(),
				data.getGrade(), data.getClassName(),
				data.getClassNo(), data.getNameKana());
	}
	
	// wrapperクラス
	private class StudentData extends KNStudentData {
		
		// 生年月日のフォーマッタ
		private final DateFormat birthDayFormat = new SimpleDateFormat("yyyyMMdd");
		
		public StudentData() {
			super(0);
		}
		
		private String[] data;
		
		/**
		 * @param pData 設定する data。
		 */
		public void init(final String[] pData) {
			data = pData;
		}

		/**
		 * @param pData 設定する data。
		 * @param original 変更前レコード
		 */
		public void init(final String[] pData,
				final String[] original) {
			init(pData);
			if (original != null && !StringUtils.isEmpty(original[1])) {
				setOCurrentGrade(Integer.parseInt(original[1]));
				setOCurrentClassName(original[2]);
			}
		}
		
		/**
		 * @return year を戻します。
		 */
		public String getYear() {
			return data[0];
		}
		/**
		 * @param pYear 設定する year。
		 */
		public void setYear(String pYear) {
			data[0] = pYear;
		}
		/**
		 * @return grade を戻します。
		 */
		public int getGrade() {
			return Integer.parseInt(data[1]);
		}
		/**
		 * @return grade を戻します。
		 */
		public String getGradeString() {
			return data[1];
		}
		/**
		 * @param pGrade 設定する grade。
		 */
		public void setGrade(String pGrade) {
			data[1] = pGrade;
		}
		/**
		 * @return className を戻します。
		 */
		public String getClassName() {
			return data[2];
		}
		/**
		 * @param pClassName 設定する className。
		 */
		public void setClassName(String pClassName) {
			data[2] = pClassName;
		}
		/**
		 * @return classNo を戻します。
		 */
		public String getClassNo() {
			return data[3];
		}
		/**
		 * @param pClassNo 設定する classNo。
		 */
		public void setClassNo(String pClassNo) {
			data[3] = pClassNo;
		}
		/**
		 * @return nameKana を戻します。
		 */
		public String getNameKana() {
			return data[4];
		}
		/**
		 * @param pNameKana 設定する nameKana。
		 */
		public void setNameKana(String pNameKana) {
			data[4] = pNameKana;
		}
		/**
		 * @return nameKanji を戻します。
		 */
		public String getNameKanji() {
			return data[5];
		}
		/**
		 * @param pNameKanji 設定する nameKanji。
		 */
		public void setNameKanji(String pNameKanji) {
			data[5] = pNameKanji;
		}
		/**
		 * @return sex を戻します。
		 */
		public String getSex() {
			return data[6];
		}
		/**
		 * @param pSex 設定する sex。
		 */
		public void setSex(String pSex) {
			data[6] = pSex;
		}
		/**
		 * @return birthday を戻します。
		 */
		public Date getBirthday() {
			try {
				return birthDayFormat.parse(data[7]);
			} catch (final ParseException p) {
				final InternalError e = new InternalError(p.getMessage());
				e.initCause(p);
				throw e;
			}
		}
		/**
		 * @return birthday を戻します。
		 */
		public String getBirthdayString() {
			return data[7];
		}
		/**
		 * @param pBirthday 設定する birthday。
		 */
		public void setBirthday(String pBirthday) {
			data[7] = pBirthday;
		}
		/**
		 * @return telNo を戻します。
		 */
		public String getTelNo() {
			return data[8];
		}
		/**
		 * @param pTelNo 設定する telNo。
		 */
		public void setTelNo(String pTelNo) {
			data[8] = pTelNo;
		}
		/**
		 * @return individualId を戻します。
		 */
		public String getIndividualId() {
			return data[9];
		}
		/**
		 * @param pIndividualId 設定する individualId。
		 */
		public void setIndividualId(String pIndividualId) {
			data[9] = pIndividualId;
		}
		/**
		 * @return identFlag を戻します。
		 */
		public String getIdentFlag() {
			return data[10];
		}
		/**
		 * @param pIdentFlag 設定する identFlag。
		 */
		public void setIdentFlag(String pIdentFlag) {
			data[10] = pIdentFlag;
		}
		
	}
	
}

/*
 * 作成日: 2004/11/05
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

/**
 * @author Administrator
 *
 * UNIVSTEMMAとSTEMMAをあわせたようなもの
 */
public class UnivStemmaData {
	
	private String year;
	private String univCd;
	private String facultyCd;
	private String deptCd;
	private String stemmaDiv;//あれば
	
	private String humaFlg;
	private String sciFlg;
	
	/**
	 * 分野コード・名
	 */
	private String regionCd;
	private String regionName;
	/**
	 * 小系統コード・名
	 */
	private String sStemmaCd;
	private String sStemmaName;
	/**
	 * 中系統コード・名
	 */
	private String mStemmaCd;
	private String mStemmaName;
	
	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getHumaFlg() {
		return humaFlg;
	}

	/**
	 * @return
	 */
	public String getMStemmaCd() {
		return mStemmaCd;
	}

	/**
	 * @return
	 */
	public String getMStemmaName() {
		return mStemmaName;
	}

	/**
	 * @return
	 */
	public String getRegionCd() {
		return regionCd;
	}

	/**
	 * @return
	 */
	public String getRegionName() {
		return regionName;
	}

	/**
	 * @return
	 */
	public String getSciFlg() {
		return sciFlg;
	}

	/**
	 * @return
	 */
	public String getSStemmaCd() {
		return sStemmaCd;
	}

	/**
	 * @return
	 */
	public String getSStemmaName() {
		return sStemmaName;
	}

	/**
	 * @return
	 */
	public String getStemmaDiv() {
		return stemmaDiv;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @return
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setHumaFlg(String string) {
		humaFlg = string;
	}

	/**
	 * @param string
	 */
	public void setMStemmaCd(String string) {
		mStemmaCd = string;
	}

	/**
	 * @param string
	 */
	public void setMStemmaName(String string) {
		mStemmaName = string;
	}

	/**
	 * @param string
	 */
	public void setRegionCd(String string) {
		regionCd = string;
	}

	/**
	 * @param string
	 */
	public void setRegionName(String string) {
		regionName = string;
	}

	/**
	 * @param string
	 */
	public void setSciFlg(String string) {
		sciFlg = string;
	}

	/**
	 * @param string
	 */
	public void setSStemmaCd(String string) {
		sStemmaCd = string;
	}

	/**
	 * @param string
	 */
	public void setSStemmaName(String string) {
		sStemmaName = string;
	}

	/**
	 * @param string
	 */
	public void setStemmaDiv(String string) {
		stemmaDiv = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

	/**
	 * @param string
	 */
	public void setYear(String string) {
		year = string;
	}

}

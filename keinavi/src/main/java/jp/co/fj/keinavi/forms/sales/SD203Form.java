package jp.co.fj.keinavi.forms.sales;

import jp.co.fj.keinavi.forms.MethodInvokerForm;

/**
 * 
 * 特例成績データ作成承認（営業部）画面のアクションフォームです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD203Form extends MethodInvokerForm {

	/** serialVersionUID */
	private static final long serialVersionUID = 8527889753889489310L;

	/** ソートキー */
	private String sortKey;

	/** 申請ID */
	private String[] appId;

	/**
	 * ソートキーを返します。
	 * 
	 * @return ソートキー
	 */
	public String getSortKey() {
		return sortKey;
	}

	/**
	 * ソートキーをセットします。
	 * 
	 * @param soryKey
	 *            ソートキー
	 */
	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}

	/**
	 * 申請IDを返します。
	 * 
	 * @return 申請ID
	 */
	public String[] getAppId() {
		return appId;
	}

	/**
	 * 申請IDをセットします。
	 * 
	 * @param appId
	 *            申請ID
	 */
	public void setAppId(String[] appId) {
		this.appId = appId;
	}

}

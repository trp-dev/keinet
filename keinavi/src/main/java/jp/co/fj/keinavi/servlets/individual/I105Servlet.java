package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.graph.BalanceChartApplet;
import jp.co.fj.keinavi.beans.individual.I105Bean;
import jp.co.fj.keinavi.beans.individual.PreExamSearchBean;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.individual.CourseData;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I105Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.individual.ExamSorter;

/**
 *
 * バランスチャート(合格者平均/過去模試グラフ)
 * ※プロファイルからデータ取得するが、I105で再表示のときはフォームからデータ取得
 *
 * 2004.7.23   [新規作成]
 * 2006.5.22   Tomohisa YAMADA - TOTEC [1]初期表示で対象試験がない場合の「再表示」不具合修正
 * 2016.1.14   QQ) K.Hisakawa		   サーブレット化対応
 *
 *
 * @author Keisuke KONDO - TOTEC
 * @version 1.0
 *
 */
public class I105Servlet extends IndividualServlet {

	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		//フォームデータを取得 ::::::::
		I105Form i105Form = null;
		try {
			i105Form = (I105Form)super.getActionForm(request, "jp.co.fj.keinavi.forms.individual.I105Form");
		} catch (Exception e) {
			KNServletException k = new KNServletException("0I105にてフォームデータの取得に失敗しました。" + e);
			k.setErrorCode("00I105010101");
			throw k;
		}

		HttpSession session = request.getSession();

		// ログイン情報
		LoginSession login =
			(LoginSession) session.getAttribute(LoginSession.SESSION_KEY);
		//プロファイル情報
		Profile profile = (Profile) session.getAttribute(Profile.SESSION_KEY);
		//個人共通
		ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);
		//模試データ
		ExamSession examSession = (ExamSession) session.getAttribute(ExamSession.SESSION_KEY);
		// 教科複数受験フラグ（1:平均、2：良い方）
		Short indMultiExam = (Short)profile.getItemMap(IProfileCategory.I_SCORE_BC_AVG).get(IProfileItem.IND_MULTI_EXAM);

		// 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
        Collection cCourseDatas = null;
        Collection cExaminationDatas = null;
        // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

		if ("i105".equals(getForward(request))) {
			// JSP転送
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);

				I105Bean i105Bean = new I105Bean();
				//ログイン情報フラグ
				i105Bean.setHelpflg(login.isHelpDesk());

				if(!"i105".equals(getBackward(request)) || !iCommonMap.getTargetPersonId().equals(i105Form.getTargetPersonId())){
				//初回アクセス

				//過去模試取得Bean
				PreExamSearchBean pesb = new PreExamSearchBean();
				pesb.setExamYear(iCommonMap.getTargetExamYear());
				pesb.setSameYear(true);
				pesb.setExamCd(iCommonMap.getTargetExamCode());
				pesb.setHelpflg(login.isHelpDesk());
				pesb.setPersonId(iCommonMap.getTargetPersonId());
				pesb.setConnection("", con);
				/*
				 * 12/15 add
				 */
				pesb.setExamTypeCds(new String[]{"01","02","03"});//マーク・記述・私大に限定
				pesb.setExcludeExamCds(new String[]{"66","67"});//高２マークと記述を省く

				pesb.execute();

				List preExams = pesb.getRecordSet();
				ExaminationData rightPre = null;

				//0無効値
				String preExamYear = "0";
				String preExamCd = "0";
				if(preExams != null && preExams.size() > 0){
					if(login.isHelpDesk()){
						Collections.sort(preExams, new ExamSorter().new SortByInDataOpenDate());
					}else{
						Collections.sort(preExams, new ExamSorter().new SortByOutDataOpenDate());
					}
					rightPre = (ExaminationData)preExams.get(0);
					preExamYear = rightPre.getExamYear();
					preExamCd   = rightPre.getExamCd();
					//模試種類セット
					i105Bean.setExamTypecd(rightPre.getExamTypeCd());
				}else{
					i105Bean.setExamTypecd("");
				}


					i105Bean.setFirstAccess(true);

					i105Bean.setSearchCondition(preExamYear,
												preExamCd,
												iCommonMap.getTargetExamCode(),
												i105Form.getTargetPersonId(),
												new String[]{"1","2","3"});

					// 教科複数受験フラグをbeanにセット
					i105Bean.setAvgChoice(i105Form.getAvgChoice());

					//検索実行
					i105Bean.setConnection("", con);
					i105Bean.execute();

					//模試タイプ
					i105Form.setMarkexamflg(i105Bean.isExamflg());
					//模試タイプ
					i105Form.setDispflg(i105Bean.isDispflg());
					//初回用フォーム作成
					if(!preExamYear.equals("0")){
						i105Form.setTargetExamYear(i105Bean.getTargetExamYear());
					}else{
						i105Form.setTargetExamYear("");
					}
					i105Form.setTargetExamCode(i105Bean.getTargetExamCode());
				}
				//■再表示
				else{

                    String selectedCode = i105Form.getTargetExamCode();

                    //画面上に試験がなにもない場合
                    if (selectedCode == null) {
                        selectedCode = "0";
                    }

					i105Bean.setFirstAccess(false);
                    i105Bean.setSearchCondition(
                            iCommonMap.getTargetExamYear(),
                            selectedCode,
                            iCommonMap.getTargetExamCode(),
							i105Form.getTargetPersonId(),
							new String[]{"1","2","3"});

					//教科複数受験フラグをbeanにセット
					i105Bean.setAvgChoice(i105Form.getAvgChoice());

					//検索実行
					i105Bean.setConnection("", con);
					i105Bean.execute();

					//模試タイプ
					i105Form.setMarkexamflg(i105Bean.isExamflg());
					i105Form.setDispflg(i105Bean.isDispflg());

					if(!i105Bean.getTargetExamYear().equals("0") && i105Bean.getTargetExamYear() != null){
						i105Form.setTargetExamYear(i105Bean.getTargetExamYear());
					}else{
						i105Form.setTargetExamYear("");
					}
					i105Form.setTargetExamCode(i105Bean.getTargetExamCode());
				}

				request.setAttribute("i105Bean", i105Bean);

                // 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
				cCourseDatas = i105Bean.getCourseDatas();
                cExaminationDatas = i105Bean.getExaminationDatas();
                // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

			}catch(Exception e){
				e.printStackTrace();
				KNServletException k = new KNServletException("0I105にてエラーが発生しました。" + e);
				k.setErrorCode("00I105010101");
				throw k;

			} finally {
				try {if (con != null) releaseConnectionPool(request, con);
				} catch (Exception e) {
					KNServletException k = new KNServletException("0I105にてコネクションの解放に失敗しました。" + e);
					k.setErrorCode("00I105010101");
					throw k;
				}
			}

			request.setAttribute("form", i105Form);

			iCommonMap.setTargetPersonId(i105Form.getTargetPersonId());

			// 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
			int subCount = 0;
            String strSubjectDAT = "";

            for (java.util.Iterator it=cCourseDatas.iterator(); it.hasNext();) {
				if(subCount != 0) {
					strSubjectDAT = strSubjectDAT + ",";
					strSubjectDAT = strSubjectDAT + String.valueOf(((CourseData)it.next()).getCourseName());
				} else {
					strSubjectDAT = String.valueOf(((CourseData)it.next()).getCourseName());
				}
				subCount ++;
			}

            int iItemNUM = cExaminationDatas.size();

			String moshiString = "";
			String[] dataStrings = new String[cExaminationDatas.size()];
			int moshiCount = 0;
			String maxEngSubCd = "";
			String maxMathSubCd = "";
			String maxJapSubCd = "";
			String maxSciSubCd = "";
			String maxSocSubCd = "";

			for (java.util.Iterator it=cExaminationDatas.iterator(); it.hasNext();) {
			    ExaminationData data = (ExaminationData)it.next();

			    String lowRange = i105Form.getLowerRange();

			    String upRange = "100.0";

			    if(moshiCount != 0) {
			        //大学の場合
			        moshiString += ",";
			        dataStrings[moshiCount] += ",";

			        if(i105Form.getAvgChoice().equals("1")) {
			            if(cCourseDatas.size() > 3) {
			                if(i105Form.isMarkexamflg() && i105Form.isDispflg()){
			                    //平均を表示だけど、平均値が存在するのは理科・地歴のみ、他は全部MAX
			                    dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
			                }else{
			                    //平均を表示だけど、平均値が存在するのは理科・地歴のみ、他は全部MAX
			                    dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
			                }
			            } else {
			                if(i105Form.isMarkexamflg() && i105Form.isDispflg()){
			                    dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
			                }else{
			                    dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
			                }
			            }
			        } else if(i105Form.getAvgChoice().equals("3")) {
			            //第1解答科目
			            if(cCourseDatas.size() > 3) {
			                    if(i105Form.isMarkexamflg() && i105Form.isDispflg()){
			                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
			                        if (!maxSciSubCd.equals("-999")) {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+",";
			                        } else {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+",";
			                        }
			                        if (!maxSocSubCd.equals("-999")) {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
			                        } else {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
			                        }
			                    }else{
			                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
			                        if (!maxSciSubCd.equals("-999")) {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+",";
			                        } else {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+",";
			                        }
			                        if (!maxSocSubCd.equals("-999")) {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
			                        } else {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
			                        }
			                    }
			            } else {
			                    if(i105Form.isMarkexamflg() && i105Form.isDispflg()){
			                        dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
			                    }else{
			                        dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
			                    }
			            }
			        } else {
			            if(cCourseDatas.size() > 3) {
			                if(i105Form.isMarkexamflg() && i105Form.isDispflg()){
			                    dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
			                }else{
			                    dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
			                }
			            } else {
			                if(i105Form.isMarkexamflg() && i105Form.isDispflg()){
			                    dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
			                }else{
			                    dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
			                }

			            }
			        }
			    } else {
			        //模試の場合
			        if(i105Form.getAvgChoice().equals("1")) {
			            if(cCourseDatas.size() > 3) {
			                //平均を表示だけど、平均値が存在するのは理科・地歴のみ、他は全部MAX
			                if(i105Form.isMarkexamflg() && i105Form.isDispflg()){
			                    dataStrings[moshiCount] =
			                                            GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                            GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                            GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
			                                            GeneralUtil.toRangeLimit(data.getAvgSci(), lowRange, upRange)+","+
			                                            GeneralUtil.toRangeLimit(data.getAvgSoc(), lowRange, upRange);

			                                            maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                            maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                            maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                }else{
			                    dataStrings[moshiCount] =
			                                            GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                            GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                            GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
			                                            GeneralUtil.toRangeLimit(data.getAvgSci(), lowRange, upRange)+","+
			                                            GeneralUtil.toRangeLimit(data.getAvgSoc(), lowRange, upRange);

			                                            maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                            maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                            maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                }
			            } else {
			                dataStrings[moshiCount] =
			                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

			                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			            }
			        } else if(i105Form.getAvgChoice().equals("3")) {
			            // 第1解答科目表示
			            if(cCourseDatas.size() > 3) {
			                    if(i105Form.isMarkexamflg() && i105Form.isDispflg()){
			                        dataStrings[moshiCount] =
			                                                GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAns1stSci(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAns1stSoc(), lowRange, upRange);

			                                                maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                                maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                                maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                                                maxSciSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSci());
			                                                maxSocSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSoc());
			                    }else{
			                        dataStrings[moshiCount] =
			                                                GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAns1stSci(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAns1stSoc(), lowRange, upRange);

			                                                maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                                maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                                maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                                                maxSciSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSci());
			                                                maxSocSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSoc());
			                    }
			            } else {

			                if(i105Form.isMarkexamflg() && i105Form.isDispflg()){
			                                dataStrings[moshiCount] =
			                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

			                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                                }else{
			                                dataStrings[moshiCount] =
			                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

			                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                                }
			            }
			        } else {
			            if(cCourseDatas.size() > 3) {
			                dataStrings[moshiCount] =
			                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxSci(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxSoc(), lowRange, upRange);

			                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                                        maxSciSubCd = GeneralUtil.to999(data.getMaxSciCode());
			                                        maxSocSubCd = GeneralUtil.to999(data.getMaxSocCode());
			            } else {
			                dataStrings[moshiCount] =
			                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

			                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			            }
			        }
			    }
			    moshiString += data.getExamName();
			    moshiCount ++;
			}

            BalanceChartApplet App = new BalanceChartApplet();
            App.setParameter("dataSelect", "off");
            App.setParameter("dispSelect", "on");
            App.setParameter("dispZoneL", String.valueOf((int)Double.parseDouble(i105Form.getLowerRange())));
            App.setParameter("dispZoneH", String.valueOf((int)Double.parseDouble(i105Form.getUpperRange())));
            App.setParameter("subjectNUM", String.valueOf(cCourseDatas.size()));
            App.setParameter("subjectDAT", strSubjectDAT);
            App.setParameter("itemTITLE", "□対象生徒偏差値,■合格者平均偏差値");
            App.setParameter("itemNUM", String.valueOf(iItemNUM));
            App.setParameter("itemDAT", moshiString);
            for(int i=0; i<dataStrings.length; i++){
                App.setParameter("DAT" + String.valueOf(i+1), dataStrings[i]);
            }
            App.setParameter("colorDAT", "1,0,2,15,13,14");

            App.init();

            session.setAttribute("GraphServlet", App);
            session.setAttribute("GraphType", "BalanceChart");

            App = null;
            dataStrings = null;
            // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

			super.forward(request, response, JSP_I105);

		} else {
		// 不明ならServlet転送

			iCommonMap.setTargetPersonId(i105Form.getTargetPersonId());
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

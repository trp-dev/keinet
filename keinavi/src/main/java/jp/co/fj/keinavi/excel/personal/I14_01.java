package jp.co.fj.keinavi.excel.personal;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.personal.I11Item;
import jp.co.fj.keinavi.excel.data.personal.I11ListBean;
import jp.co.fj.keinavi.excel.data.personal.I14HanteiListBean;
import jp.co.fj.keinavi.excel.data.personal.I14HyoukaListBean;
import jp.co.fj.keinavi.excel.data.personal.I14ShibouHanteiListBean;
import jp.co.fj.keinavi.excel.data.personal.I14TokutenListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * 個人成績分析　志望大学判定一覧 Excelファイル編集
 *
 *
 * 2004.09.16   Y ITO - THS
 * 				新規作成
 *
 * <2010年度改修>
 * 2009.11.30   Tomohisa YAMADA - Totec
 *              ・5教科判定対応
 *
 * 2009.11.30   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 *
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 *
 */
public class I14_01 {

    private int	noerror		= 0;	// 正常終了
    private int	errfread	= 1;	// ファイルreadエラー
    private int	errfwrite	= 2;	// ファイルwriteエラー
    private int	errfdata	= 3;	// データ設定エラー

    private CM		cm			= new CM();	// 共通関数用クラス インスタンス

    //2005.04.26 Add 新テスト対応
    //final private String	masterfile		= "I14_01";	// ファイル名
    final private String 	masterfile0 	= "I14_01";	// ファイル名１(新テスト対応用)
    final private String 	masterfile1 	= "I14_01";	// ファイル名２(新テスト対応用)
    private String masterfile = "";


/*
 * 	Excel編集メイン
 * 		I11Item i11Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
     public int i14_01EditExcel(I11Item i11Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
        KNLog log = KNLog.getInstance(null,null,null);

        //2005.04.26 Add 新テスト対応
        //テンプレートの決定
        if (i11Item.getIntHanteiShubetsuFlg() == 1){
            masterfile = masterfile1;
        } else{
            masterfile = masterfile0;
        }

        try {

            // データセット
            ArrayList	i11List			= i11Item.getI11List();
            Iterator	itr				= i11List.iterator();

            int		maxSheetIndex	= 0;	// シートカウンター

            // マスタExcel読み込み
            HSSFWorkbook	workbook	= null;
            HSSFSheet		workSheet	= null;
            HSSFRow			workRow		= null;
            HSSFCell		workCell	= null;

            // 基本ファイルを読込む
            I11ListBean i11ListBean = new I11ListBean();

            /** データリスト **/
            while( itr.hasNext() ) {
                i11ListBean = (I11ListBean) itr.next();

                // マスタExcel読み込み
                workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
                if( workbook==null ){
                    return errfread;
                }

                // シートテンプレートのコピー
                workSheet = workbook.cloneSheet(0);

                // ヘッダ右側に帳票作成日時を表示する
                cm.setHeader(workbook, workSheet);

                // セキュリティスタンプセット
                //2004.10.15 Update
                //String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntMenSheetSecuFlg() ,133 ,141 );
                String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntHanteiSecuFlg() ,133 ,141 );
                //Update End
                workCell = cm.setCell( workSheet, workRow, workCell, 0, 133 );
                workCell.setCellValue(secuFlg);

                // 学校名セット
                workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
                workCell.setCellValue( "学校名　：" + cm.toString(i11ListBean.getStrGakkomei()) + "　学年：" + cm.toString(i11ListBean.getStrGrade() )
                + "　クラス名：" + cm.toString(i11ListBean.getStrClass()) + "　クラス番号：" + cm.toString(i11ListBean.getStrClassNum()) );

                // 氏名・性別セット
                String strSex = "";
                if( cm.toString(i11ListBean.getStrSex()).equals("1") ){
                    strSex = "男";
                }else if( cm.toString(i11ListBean.getStrSex()).equals("2") ){
                    strSex = "女";
                }else{
                    strSex = "不明";
                }
                workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );

                if( !cm.toString(i11ListBean.getStrShimei()).equals("")){
                    workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrShimei()) + "（" + cm.toString(i11ListBean.getStrKana()) + "）　性別：" + strSex );
                }else{
                    workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrKana()) + "　性別：" + strSex );
                }


                // 模試名セット
                String msh = cm.setTaisyouMoshi( i11ListBean.getStrBaseMshDate() );
                workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
                workCell.setCellValue( "対象模試：" + cm.toString(i11ListBean.getStrBaseMshmei()) + msh );

                // 順位セット
                for(int jun=0;jun<20;jun++ ){
                    workCell = cm.setCell( workSheet, workRow, workCell, jun*2+30, 0 );
                    workCell.setCellValue(Integer.toString(jun+1));
                }

                /** 志望校判定データリスト **/
                // データセット
                ArrayList i14HanteiList = i11ListBean.getI14HanteiList();
                Iterator itrI14Hantei = i14HanteiList.iterator();

                while(itrI14Hantei.hasNext()){
                    I14HanteiListBean i14HanteiListBean = (I14HanteiListBean) itrI14Hantei.next();

                    /** 志望校評価リスト **/
                    // データセット
                    ArrayList i14HyoukaList = i14HanteiListBean.getI14HyoukaList();
                    Iterator itrI14Hyouka = i14HyoukaList.iterator();

                    int hyokaRow = 0;
                    while(itrI14Hyouka.hasNext()){
                        I14HyoukaListBean i14HyoukaListBean = (I14HyoukaListBean) itrI14Hyouka.next();

                        //2005.04.22 Update　２次開発ペンディング項目一覧No.46対応
                        //					都道府県の表示を２段に分けて、上段に本拠地の所在地(県名)、
                        //					下段に、キャンパスの所在地(県名)を表示させるように変更。
                        //					本拠地とキャンパスが同じ場合は、キャンパスは表示しない。
                        // 都道府県セット(本拠地)
                        String strHonkyoti = i14HyoukaListBean.getStrHonbuShozaichi();
                        workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow, 1 );
                        workCell.setCellValue( strHonkyoti );

                        // 都道府県セット(キャンパス所在地)
                        try
                        {
                            String strCanpas = i14HyoukaListBean.getStrKoshaShozaichi();
                            // 本拠地の県名と、キャンパスの県名を比較
                            if(strCanpas.equals(strHonkyoti) != true )
                            {
                                // 一致しない場合は出力する
                                workCell = cm.setCell( workSheet, workRow, workCell, 8+2*hyokaRow, 1 );
                                workCell.setCellValue( strCanpas );
                            }
                        }
                        catch(NullPointerException e)
                        {
                            //キャンパス所在地がNullだったら何もしない
                        }

                        // 大学区分セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow, 2 );
                        workCell.setCellValue( i14HyoukaListBean.getStrDaigakuKbn() );

                        // 志望大学セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow, 3 );
                        workCell.setCellValue( i14HyoukaListBean.getStrDaigakuMei() );

                        // 志望学部セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 8+2*hyokaRow, 4 );
                        workCell.setCellValue( i14HyoukaListBean.getStrGakubuMei() );

                        // 志望学科セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 8+2*hyokaRow, 5 );
                        workCell.setCellValue( i14HyoukaListBean.getStrGakkaMei() );

                        // 評価得点セット
                        if( i14HyoukaListBean.getIntHyoukaTokuten() != -999 ){
                            workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow, 7 );
                            workCell.setCellValue( i14HyoukaListBean.getIntHyoukaTokuten() );
                        }

                        // 評価（センター）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow, 8 );
                        workCell.setCellValue( i14HyoukaListBean.getStrHyoukaCenter() );

                        // 評価偏差値セット
                        if( i14HyoukaListBean.getFloHyoukaHensa() != -999.0 ){
                            workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow, 9 );
                            workCell.setCellValue( i14HyoukaListBean.getFloHyoukaHensa() );
                        }

                        // 評価（２次）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow, 10 );
                        workCell.setCellValue( i14HyoukaListBean.getStrHyoukaKijyutsu() );

                        // 評価ポイントセット
                        if( i14HyoukaListBean.getIntHyoukaPoint() != -999 ){
                            workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow, 11 );
                            workCell.setCellValue( i14HyoukaListBean.getIntHyoukaPoint());
                        }

                        // 評価（総合）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow, 12 );
                        workCell.setCellValue( i14HyoukaListBean.getStrHyoukaAll() );

                        hyokaRow++;
                        if( hyokaRow >= 9 ){
                            break;
                        }
                    }

                    /** 得点リスト **/
                    // データセット
                    ArrayList i14TokutenList = i14HanteiListBean.getI14TokutenList();
                    Iterator itrI14Tokuten = i14TokutenList.iterator();

                    String strTktHoji = "";
                    int tokutenTabCol = 0;
                    int tokutenRow = 0;
                    int maxRowNum = 0;
                    while(itrI14Tokuten.hasNext()){
                        I14TokutenListBean i14TokutenListBean = (I14TokutenListBean) itrI14Tokuten.next();

                        // 教科コードによって判定
                        String strKyouka = i14TokutenListBean.getStrKyokaCd()+" ";
                        strKyouka = strKyouka.substring(0,1);
                        if( !strKyouka.equals(strTktHoji) ){
                            switch( Integer.parseInt( strKyouka ) ){
                                case 1:
                                    tokutenRow = 0;
                                    tokutenTabCol = 37;
                                    //2005.01.25 Update maxRowNum=2 → 3
                                    maxRowNum = 3;
                                    break;
                                case 2:
                                    tokutenRow = 0;
                                    tokutenTabCol = 58;
                                    maxRowNum = 3;
                                    break;
                                case 3:
                                    tokutenRow = 0;
                                    tokutenTabCol = 79;
                                    maxRowNum = 1;
                                    break;
                                case 4:
                                    tokutenRow = 0;
                                    tokutenTabCol = 100;
                                    maxRowNum = 3;
                                    break;
                                case 5:
                                    tokutenRow = 0;
                                    tokutenTabCol = 121;
                                    maxRowNum = 2;
                                    break;
                                case 7:
                                    tokutenRow = 0;
                                    tokutenTabCol = 16;
                                    maxRowNum = 1;
                                    break;
                                case 8:	//2004.10.25 Add
                                    if( strTktHoji.equals("7") == true ){
                                        tokutenRow = 1;
                                    }
                                    else{
                                        tokutenRow = 0;
                                    }
                                    tokutenTabCol = 16;
                                    maxRowNum = 1;
                                    break;
                                default:
                                    maxRowNum = 0;

                            }
                            strTktHoji = strKyouka;
                        }

                        //2004.10.28 Update
//						if(tokutenRow < maxRowNum){
                        if(maxRowNum > 0 ){
                        //Update End
                            //理科の先頭科目が基礎科目なら、先頭は空欄にする
                            if ("4".equals(strKyouka) && tokutenRow == 0 && "1".equals(i14TokutenListBean.getBasicFlg())) {
                                tokutenRow++;
                            }

                            // 型・科目セット
                            workCell = cm.setCell( workSheet, workRow, workCell, 7+2*tokutenRow, tokutenTabCol );
                            workCell.setCellValue( i14TokutenListBean.getStrKmkmei() );

                            // 得点セット
                            if( i14TokutenListBean.getIntTokuten() != -999 ){
                                workCell = cm.setCell( workSheet, workRow, workCell, 7+2*tokutenRow, tokutenTabCol+8 );
                                workCell.setCellValue( i14TokutenListBean.getIntTokuten());
                            }

                            // 偏差値セット
                            if( i14TokutenListBean.getFloHensa() != -999.0 ){
                                workCell = cm.setCell( workSheet, workRow, workCell, 7+2*tokutenRow, tokutenTabCol+15 );
                                workCell.setCellValue( i14TokutenListBean.getFloHensa() );
                            }

                            // 学力レベルセット
                            if( !"".equals(i14TokutenListBean.getStrScholarLevel()) ){
                                workCell = cm.setCell( workSheet, workRow, workCell, 7+2*tokutenRow, tokutenTabCol+19 );
                                workCell.setCellValue( i14TokutenListBean.getStrScholarLevel() );
                            }

                            tokutenRow++;
                            maxRowNum--;	//2004.10.28 Add
                        }
                    }


                    // 判定条件

                    // 対象地区セット
                    workCell = cm.setCell( workSheet, workRow, workCell, 15, 23 );
                    workCell.setCellValue( i14HanteiListBean.getStrArea() );

                    // 大学名称セット
                    workCell = cm.setCell( workSheet, workRow, workCell, 16, 23 );
                    workCell.setCellValue( i14HanteiListBean.getStrDaigaku() );

                    // 学部系統セット
                    workCell = cm.setCell( workSheet, workRow, workCell, 17, 23 );
                    workCell.setCellValue( i14HanteiListBean.getStrKeitou() );

                    // 二次科目セット
                    String strKasu = "";
                    if( i14HanteiListBean.getIntSecondFlg() == 0 ){
                        if( i14HanteiListBean.getIntAssignFlg() == 1 ){
                            strKasu = cm.toString(i14HanteiListBean.getStrNijiKmk()) + "を課す";
                        }else if( i14HanteiListBean.getIntAssignFlg() == 2 ){
                            strKasu = cm.toString(i14HanteiListBean.getStrNijiKmk()) + "を課さない";
                        }
                    }else if( i14HanteiListBean.getIntSecondFlg() == 1 ){
                        strKasu = "２次・独自試験を課さない";
                    }
                    workCell = cm.setCell( workSheet, workRow, workCell, 18, 23 );
                    workCell.setCellValue( strKasu );

                    // 学校区分セット
                    workCell = cm.setCell( workSheet, workRow, workCell, 19, 23 );
                    workCell.setCellValue( i14HanteiListBean.getStrGakkoKbn() );

                    // 入試日セット
                    workCell = cm.setCell( workSheet, workRow, workCell, 20, 23 );
                    workCell.setCellValue( cm.toString(i14HanteiListBean.getStrNyushibiStart()) + "〜" + cm.toString(i14HanteiListBean.getStrNyushibiEnd()) );

                    // 評価範囲セット
                    String strHouka = "";
                    if( i14HanteiListBean.getIntHyoukaHanniFlg() == 1 ){
                        strHouka = "センターまたは２次・私大";
                    }else if( i14HanteiListBean.getIntHyoukaHanniFlg() == 2 ){
                        strHouka = "総合（ドッキング）";
                    }
                    workCell = cm.setCell( workSheet, workRow, workCell, 21, 23 );
                    workCell.setCellValue( strHouka +  "　" + cm.toString(i14HanteiListBean.getStrHyoukaHanni()) );

                    // 対象外セット
                    workCell = cm.setCell( workSheet, workRow, workCell, 22, 23 );
                    workCell.setCellValue( i14HanteiListBean.getStrTaisyougai() );


                    /** 志望校判定用データリスト **/
                    // データセット
                    ArrayList i14ShibouHanteiList = i14HanteiListBean.getI14ShibouHanteiList();
                    Iterator itrI14Shibo = i14ShibouHanteiList.iterator();

                    int shbRow = 0;	//表示志望校の表示行
                    int shbRecCnt = 0;	//表示志望校のカウント
                    while(itrI14Shibo.hasNext()){

                        //2005.03.16 Update ２次開発ペンディング項目一覧No.49対応
                        //					帳票のヘッダ情報および志望校判定用データ一覧以外のデータ表示処理を
                        //					志望校判定用データの表示処理内でも行うようにする
                        //					志望校データ表示の２１回目のみ処理が走るように変更
                        if( shbRecCnt == 20){

                            shbRow = 0;	//表示行を初期化する

                            workSheet = workbook.cloneSheet(0);
                            maxSheetIndex++;

                            // ヘッダ右側に帳票作成日時を表示する
                            cm.setHeader(workbook, workSheet);

                            // セキュリティスタンプセット
                            //2004.10.15 Update
                            //String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntMenSheetSecuFlg() ,133 ,141 );
                            String secuFlg2 = cm.setSecurity( workbook, workSheet, i11Item.getIntHanteiSecuFlg() ,133 ,141 );
                            //Update End
                            workCell = cm.setCell( workSheet, workRow, workCell, 0, 133 );
                            workCell.setCellValue(secuFlg2);

                            // 学校名セット
                            workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
                            workCell.setCellValue( "学校名　：" + cm.toString(i11ListBean.getStrGakkomei()) + "　学年：" + cm.toString(i11ListBean.getStrGrade() )
                            + "　クラス名：" + cm.toString(i11ListBean.getStrClass()) + "　クラス番号：" + cm.toString(i11ListBean.getStrClassNum()) );

                            // 氏名・性別セット
                            String strSex2 = "";
                            if( cm.toString(i11ListBean.getStrSex()).equals("1") ){
                                strSex2 = "男";
                            }else if( cm.toString(i11ListBean.getStrSex()).equals("2") ){
                                strSex2 = "女";
                            }else{
                                strSex2 = "不明";
                            }
                            workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );

                            if( !cm.toString(i11ListBean.getStrShimei()).equals("")){
                                workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrShimei()) + "（" + cm.toString(i11ListBean.getStrKana()) + "）　性別：" + strSex2 );
                            }else{
                                workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrKana()) + "　性別：" + strSex2 );
                            }


                            // 模試名セット
                            String msh2 = cm.setTaisyouMoshi( i11ListBean.getStrBaseMshDate() );
                            workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
                            workCell.setCellValue( "対象模試：" + cm.toString(i11ListBean.getStrBaseMshmei()) + msh2 );

                            // 順位セット
                            for(int jun=0;jun<20;jun++ ){
                                workCell = cm.setCell( workSheet, workRow, workCell, jun*2+30, 0 );
                                workCell.setCellValue(Integer.toString(jun+21));
                            }

                            /** 志望校判定データリスト **/
                            // データセット
                            ArrayList i14HanteiList2 = i11ListBean.getI14HanteiList();
                            Iterator itrI14Hantei2 = i14HanteiList2.iterator();

                            while(itrI14Hantei2.hasNext()){
                                I14HanteiListBean i14HanteiListBean2 = (I14HanteiListBean) itrI14Hantei2.next();

                                /** 志望校評価リスト **/
                                // データセット
                                ArrayList i14HyoukaList2 = i14HanteiListBean2.getI14HyoukaList();
                                Iterator itrI14Hyouka2 = i14HyoukaList2.iterator();

                                int hyokaRow2 = 0;
                                while(itrI14Hyouka2.hasNext()){
                                    I14HyoukaListBean i14HyoukaListBean2 = (I14HyoukaListBean) itrI14Hyouka2.next();

                                    //2005.04.22 Update　２次開発ペンディング項目一覧No.46対応
                                    //					都道府県の表示を２段に分けて、上段に本拠地の所在地(県名)、
                                    //					下段に、キャンパスの所在地(県名)を表示させるように変更。
                                    //					本拠地とキャンパスが同じ場合は、キャンパスは表示しない。
                                    // 都道府県セット(本拠地)
                                    String strHonkyoti = i14HyoukaListBean2.getStrHonbuShozaichi();
                                    workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow2, 1 );
                                    workCell.setCellValue( strHonkyoti );

                                    // 都道府県セット(キャンパス所在地)
                                    try
                                    {
                                        String strCanpas = i14HyoukaListBean2.getStrKoshaShozaichi();
                                        // 本拠地の県名と、キャンパスの県名を比較
                                        if(strCanpas.equals(strHonkyoti) != true )
                                        {
                                            // 一致しない場合は出力する
                                            workCell = cm.setCell( workSheet, workRow, workCell, 8+2*hyokaRow2, 1 );
                                            workCell.setCellValue( strCanpas );
                                        }
                                    }
                                    catch(NullPointerException e)
                                    {
                                        //キャンパス所在地がNullだったら何もしない
                                    }

                                    // 大学区分セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow2, 2 );
                                    workCell.setCellValue( i14HyoukaListBean2.getStrDaigakuKbn() );

                                    // 志望大学セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow2, 3 );
                                    workCell.setCellValue( i14HyoukaListBean2.getStrDaigakuMei() );

                                    // 志望学部セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 8+2*hyokaRow2, 4 );
                                    workCell.setCellValue( i14HyoukaListBean2.getStrGakubuMei() );

                                    // 志望学科セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 8+2*hyokaRow2, 5 );
                                    workCell.setCellValue( i14HyoukaListBean2.getStrGakkaMei() );

                                    // 評価得点セット
                                    if( i14HyoukaListBean2.getIntHyoukaTokuten() != -999 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow2, 7 );
                                        workCell.setCellValue( i14HyoukaListBean2.getIntHyoukaTokuten() );
                                    }

                                    // 評価（センター）セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow2, 8 );
                                    workCell.setCellValue( i14HyoukaListBean2.getStrHyoukaCenter() );

                                    // 評価偏差値セット
                                    if( i14HyoukaListBean2.getFloHyoukaHensa() != -999.0 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow2, 9 );
                                        workCell.setCellValue( i14HyoukaListBean2.getFloHyoukaHensa() );
                                    }

                                    // 評価（２次）セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow2, 10 );
                                    workCell.setCellValue( i14HyoukaListBean2.getStrHyoukaKijyutsu() );

                                    // 評価ポイントセット
                                    if( i14HyoukaListBean2.getIntHyoukaPoint() != -999 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow2, 11 );
                                        workCell.setCellValue( i14HyoukaListBean2.getIntHyoukaPoint());
                                    }

                                    // 評価（総合）セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 7+2*hyokaRow2, 12 );
                                    workCell.setCellValue( i14HyoukaListBean2.getStrHyoukaAll() );

                                    hyokaRow2++;
                                    if( hyokaRow2 >= 9 ){
                                        break;
                                    }
                                }

                                /** 得点リスト **/
                                // データセット
                                ArrayList i14TokutenList2 = i14HanteiListBean2.getI14TokutenList();
                                Iterator itrI14Tokuten2 = i14TokutenList2.iterator();

                                String strTktHoji2 = "";
                                int tokutenTabCol2 = 0;
                                int tokutenRow2 = 0;
                                int maxRowNum2 = 0;
                                while(itrI14Tokuten2.hasNext()){
                                    I14TokutenListBean i14TokutenListBean2 = (I14TokutenListBean) itrI14Tokuten2.next();

                                    // 教科コードによって判定
                                    String strKyouka = i14TokutenListBean2.getStrKyokaCd()+" ";
                                    strKyouka = strKyouka.substring(0,1);
                                    if( !strKyouka.equals(strTktHoji2) ){
                                        switch( Integer.parseInt( strKyouka ) ){
                                            case 1:
                                                tokutenRow2 = 0;
                                                tokutenTabCol2 = 37;
                                                //2005.01.25 Update maxRowNum=2 → 3
                                                maxRowNum2 = 3;
                                                break;
                                            case 2:
                                                tokutenRow2 = 0;
                                                tokutenTabCol2 = 58;
                                                maxRowNum2 = 3;
                                                break;
                                            case 3:
                                                tokutenRow2 = 0;
                                                tokutenTabCol2 = 79;
                                                maxRowNum2 = 1;
                                                break;
                                            case 4:
                                                tokutenRow2 = 0;
                                                tokutenTabCol2 = 100;
                                                maxRowNum2 = 3;
                                                break;
                                            case 5:
                                                tokutenRow2 = 0;
                                                tokutenTabCol2 = 121;
                                                maxRowNum2 = 2;
                                                break;
                                            case 7:
                                                tokutenRow2 = 0;
                                                tokutenTabCol2 = 16;
                                                maxRowNum2 = 1;
                                                break;
                                            case 8:	//2004.10.25 Add
                                                if( strTktHoji2.equals("7") == true ){
                                                    tokutenRow2 = 1;
                                                }
                                                else{
                                                    tokutenRow2 = 0;
                                                }
                                                tokutenTabCol2 = 16;
                                                maxRowNum2 = 1;
                                                break;
                                            default:
                                                maxRowNum2= 0;

                                        }
                                        strTktHoji2 = strKyouka;
                                    }

                                    //2004.10.28 Update
                                    if(maxRowNum2 > 0 ){
                                    //Update End
                                        //理科の先頭科目が基礎科目なら、先頭は空欄にする
                                        if ("4".equals(strKyouka) && tokutenRow2 == 0 && "1".equals(i14TokutenListBean2.getBasicFlg())) {
                                            tokutenRow2++;
                                        }

                                        // 型・科目セット
                                        workCell = cm.setCell( workSheet, workRow, workCell, 7+2*tokutenRow2, tokutenTabCol2 );
                                        workCell.setCellValue( i14TokutenListBean2.getStrKmkmei() );

                                        // 得点セット
                                        if( i14TokutenListBean2.getIntTokuten() != -999 ){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 7+2*tokutenRow2, tokutenTabCol2+8 );
                                            workCell.setCellValue( i14TokutenListBean2.getIntTokuten());
                                        }

                                        // 偏差値セット
                                        if( i14TokutenListBean2.getFloHensa() != -999.0 ){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 7+2*tokutenRow2, tokutenTabCol2+15 );
                                            workCell.setCellValue( i14TokutenListBean2.getFloHensa() );
                                        }

                                        // 学力レベルセット
                                        if( !"".equals(i14TokutenListBean2.getStrScholarLevel()) ){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 7+2*tokutenRow2, tokutenTabCol2+19 );
                                            workCell.setCellValue( i14TokutenListBean2.getStrScholarLevel() );
                                        }

                                        tokutenRow2++;
                                        maxRowNum2--;	//2004.10.28 Add
                                    }
                                }


                                // 判定条件

                                // 対象地区セット
                                workCell = cm.setCell( workSheet, workRow, workCell, 15, 23 );
                                workCell.setCellValue( i14HanteiListBean2.getStrArea() );

                                // 大学名称セット
                                workCell = cm.setCell( workSheet, workRow, workCell, 16, 23 );
                                workCell.setCellValue( i14HanteiListBean2.getStrDaigaku() );

                                // 学部系統セット
                                workCell = cm.setCell( workSheet, workRow, workCell, 17, 23 );
                                workCell.setCellValue( i14HanteiListBean2.getStrKeitou() );

                                // 二次科目セット
                                String strKasu2 = "";
                                if( i14HanteiListBean2.getIntSecondFlg() == 0 ){
                                    if( i14HanteiListBean2.getIntAssignFlg() == 1 ){
                                        strKasu2 = cm.toString(i14HanteiListBean2.getStrNijiKmk()) + "を課す";
                                    }else if( i14HanteiListBean2.getIntAssignFlg() == 2 ){
                                        strKasu2 = cm.toString(i14HanteiListBean2.getStrNijiKmk()) + "を課さない";
                                    }
                                }else if( i14HanteiListBean2.getIntSecondFlg() == 1 ){
                                    strKasu2 = "２次・独自試験を課さない";
                                }
                                workCell = cm.setCell( workSheet, workRow, workCell, 18, 23 );
                                workCell.setCellValue( strKasu2 );

                                // 学校区分セット
                                workCell = cm.setCell( workSheet, workRow, workCell, 19, 23 );
                                workCell.setCellValue( i14HanteiListBean2.getStrGakkoKbn() );

                                // 入試日セット
                                workCell = cm.setCell( workSheet, workRow, workCell, 20, 23 );
                                workCell.setCellValue( cm.toString(i14HanteiListBean2.getStrNyushibiStart()) + "〜" + cm.toString(i14HanteiListBean2.getStrNyushibiEnd()) );

                                // 評価範囲セット
                                String strHouka2 = "";
                                if( i14HanteiListBean2.getIntHyoukaHanniFlg() == 1 ){
                                    strHouka2 = "センターまたは２次・私大";
                                }else if( i14HanteiListBean2.getIntHyoukaHanniFlg() == 2 ){
                                    strHouka2 = "総合（ドッキング）";
                                }
                                workCell = cm.setCell( workSheet, workRow, workCell, 21, 23 );
                                workCell.setCellValue( strHouka2 +  "　" + cm.toString(i14HanteiListBean2.getStrHyoukaHanni()) );

                                // 対象外セット
                                workCell = cm.setCell( workSheet, workRow, workCell, 22, 23 );
                                workCell.setCellValue( i14HanteiListBean2.getStrTaisyougai() );
                            }
                        }



                        I14ShibouHanteiListBean i14ShibouHanteiListBean = (I14ShibouHanteiListBean) itrI14Shibo.next();

//						// 都道府県セット
//						workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 1 );
//						workCell.setCellValue( i14ShibouHanteiListBean.getStrKenmei() );

                        //2005.04.22 Update　２次開発ペンディング項目一覧No.46対応
                        //					都道府県の表示を２段に分けて、上段に本拠地の所在地(県名)、
                        //					下段に、キャンパスの所在地(県名)を表示させるように変更。
                        //					本拠地とキャンパスが同じ場合は、キャンパスは表示しない。
                        // 都道府県セット(本拠地)
                        String strHonkyoti = i14ShibouHanteiListBean.getStrHonbuShozaichi();
                        workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 1 );
                        workCell.setCellValue( strHonkyoti );

                        // 都道府県セット(キャンパス所在地)
                        try
                        {
                            String strCanpas = i14ShibouHanteiListBean.getStrKoshaShozaichi();
                            // 本拠地の県名と、キャンパスの県名を比較
                            if(strCanpas.equals(strHonkyoti) != true )
                            {
                                // 一致しない場合は出力する
                                workCell = cm.setCell( workSheet, workRow, workCell, 31+2*shbRow, 1 );
                                workCell.setCellValue( strCanpas );
                            }
                        }
                        catch(NullPointerException e)
                        {
                            //キャンパス所在地がNullだったら何もしない
                        }

                        // 大学区分セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 2 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrDaigakuKbn() );

                        // 大学名セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 3 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrDaigakuMei() );

                        // 学部名セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 31+2*shbRow, 4 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrGakubuMei() );

                        // 学科名セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 31+2*shbRow, 5 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrGakkaMei() );

                        // [複]セット
                        if( i14ShibouHanteiListBean.getIntPluralFlg() == 1 ){
                            workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 6 );
                            workCell.setCellValue( "[複]" );
                        }

                        // 募集人数セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 31 + 2 * shbRow, 6 );
                        if (!"8".equals(i14ShibouHanteiListBean.getStrExamCapaRel()) && !"0".equals(i14ShibouHanteiListBean.getStrBoshuNinzu())
                                && !"".equals(cm.toString(i14ShibouHanteiListBean.getStrBoshuNinzu()))) {
                            workCell.setCellValue("(" + cm.toString(i14ShibouHanteiListBean.getStrBoshuNinzu())
                                    + ("9".equals(i14ShibouHanteiListBean.getStrExamCapaRel()) ? "推" : "") + ")");
                        }

                        // 評価得点セット
                        if( i14ShibouHanteiListBean.getIntHyoukaTokuten() != -999 ){
                            workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 7 );
                            workCell.setCellValue( i14ShibouHanteiListBean.getIntHyoukaTokuten() );
                        }

                        // 評価（センター）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 8 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrHyoukaMark() );

                        // 評価偏差値セット
                        if( i14ShibouHanteiListBean.getFloKijyutsuHensa() != -999.0){
                            workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 9 );
                            workCell.setCellValue( i14ShibouHanteiListBean.getFloKijyutsuHensa() );
                        }

                        // 評価（２次）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 10 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrHyoukaKijyutsu() );

                        // 評価ポイントセット
                        if( i14ShibouHanteiListBean.getIntPointAll() != -999 ){
                            workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 11 );
                            workCell.setCellValue( i14ShibouHanteiListBean.getIntPointAll() );
                        }

                        // 評価（総合）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 12 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrHyoukaAll() );

                        // ボーダー得点セット 2004.11.19 無効値を"--"にする → "  --" 2004.11.29
                        if( i14ShibouHanteiListBean.getIntBorderTen() != -999){
                            workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 13 );
                            workCell.setCellValue( i14ShibouHanteiListBean.getIntBorderTen() );
                        }else{
                            workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 13 );
                            workCell.setCellValue( "  --" );
                        }

                        // ボーダー得点率セット 2004.11.19 無効値を"--"にする
                        if( i14ShibouHanteiListBean.getFloBorderTokuritsu() != -999.0){
                            workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 14 );
                            workCell.setCellValue( i14ShibouHanteiListBean.getFloBorderTokuritsu() );
                        }else{
                            workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 14 );
                            workCell.setCellValue( "--" );
                        }

                        // ２次ランクセット 2004.11.19 無効値を"--"にする
                        if( i14ShibouHanteiListBean.getFloRankHensa() == 0){
                            workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 15 );
                            workCell.setCellValue( JudgementConstants.Univ.RANK_STR_NONE );
                        }else if( i14ShibouHanteiListBean.getFloRankHensa() != -999.0){
                            workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 15 );
                            workCell.setCellValue( i14ShibouHanteiListBean.getFloRankHensa() );
                        }else{
                            workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 15 );
                            workCell.setCellValue( "--" );
                        }

                        // 試験科目(センター)セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 16 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrShikenKmkCenter() );

                        // 試験科目（２次）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 31+2*shbRow, 16 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrShikenKmk2() );

                        // 配点総合（センター）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 71 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrSoutenCenter() );

                        // 配点総合（２次）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 31+2*shbRow, 71 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrSouten2() );

                        // 教科配点（センター）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 76 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrCourseAllotCenter() );

                        // 教科配点（２次）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 31+2*shbRow, 76 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrCourseAllot2() );

                        // 入試日程（出願締め切り）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 117 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrSyutsuganbi() );

                        // 入試日程（試験日）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 122 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrShikenbi() );

                        // 入試日程（合格者発表日）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 132 );
                        workCell.setCellValue( i14ShibouHanteiListBean.getStrGoukakubi() );

                        // 入試日程（手続き締切日）セット
                        workCell = cm.setCell( workSheet, workRow, workCell, 30+2*shbRow, 137 );
                        if(i14ShibouHanteiListBean.getStrTetsudukibi().equals(",")){
                            workCell.setCellValue("");
                        }
                        else{
                            workCell.setCellValue( i14ShibouHanteiListBean.getStrTetsudukibi() );
                        }
                        //Update End

                        shbRow++;
                        //2005.03.16 Add 行数カウント以外に、表示レコード数のカウントを追加
                        shbRecCnt++;
                        //2005.03.16 Update 判定校数の上限を20校から40校に変更
                        if( shbRecCnt >= 40 ){
                            break;
                        }
                    }// while(trI14Shibo.hasNext())

                }// while(itrI14Hantei.hasNext())

                // Excelファイル保存
                boolean bolRet = false;
                String strFileName = masterfile + "_" + cm.toString(i11ListBean.getStrGrade()) + cm.toString(i11ListBean.getStrClass()) + cm.toString(i11ListBean.getStrClassNum());
//				if( (itr.hasNext()==false)&&(fileIndex == 1) ){
//					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex+1);
                    bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, strFileName, maxSheetIndex+1);
//				}else{
////					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, maxSheetIndex+1);
//					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex,
//						masterfile + i11ListBean.getStrGrade() + i11ListBean.getStrClass() + i11ListBean.getStrClassNum(), maxSheetIndex+1);
//				}

                if( bolRet == false ){
                    return errfwrite;
                }


            }	// while( itr.hasNext() )

        }
         catch(Exception e) {
            log.Err("I14_01","データセットエラー",e);
            return errfdata;
        }

        return noerror;
    }

}
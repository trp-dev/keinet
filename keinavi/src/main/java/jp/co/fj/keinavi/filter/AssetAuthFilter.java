package jp.co.fj.keinavi.filter;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.util.AssetLoginUtil;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * 資産認証フィルタです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class AssetAuthFilter implements Filter {

    /**
     * {@inheritDoc}
     */
    public void init(FilterConfig arg0) throws ServletException {
    }

    /**
     * {@inheritDoc}
     */
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        if (KNUtil.getRemoteAddr(request).startsWith("10.")) {
            /* イントラアクセスの場合→資産コンテンツフィルタの処理へ進む */
            chain.doFilter(req, res);
        } else if (AssetLoginUtil.checkCookie(request)) {
            /* 資産認証成功→資産認証クッキーを再発行後、資産コンテンツフィルタの処理へ進む */
            AssetLoginUtil.publishCookie(request, response);
            chain.doFilter(req, res);
        } else {
            /* 資産認証失敗→ログイン画面へリダイレクトする */
            response.sendRedirect(KNUtil.getContextPath(request) + "/AssetLogin?next="
                    + URLEncoder.encode(request.getServletPath(), "Windows-31J"));
        }
    }

    /**
     * {@inheritDoc}
     */
    public void destroy() {
    }

}

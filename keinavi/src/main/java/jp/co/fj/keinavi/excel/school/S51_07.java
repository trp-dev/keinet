/**
 * 校内成績分析−過回・他校比較　成績概況
 * 	Excelファイル編集
 * 作成日: 2004/09/07
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.io.*;
import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.excel.cm.*;
import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.*;

public class S51_07 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー
	
	private CM		cm			= new CM();	//共通関数用クラス インスタンス
	
	final private String	masterfile0	= "S51_07";		// ファイル名
	final private String	masterfile1	= "NS51_07";	// ファイル名
	private String	masterfile	= "";					// ファイル名
	final private String	strArea		= "A1:AC39";	// 印刷範囲
	final private int[]	tabRow		= {5,17,29};	// 表の基準点
	final private int[]	tabCol		= {1,5,9,13,17,21,25};		// 表の基準点
	private boolean bolBookCngFlg = true;			//改ファイルフラグ
	private boolean bolSheetCngFlg = true;			//改シートフラグ
	final private int intMaxSheetSr = 50;	//MAXシート数の値を入れる

/*
 * 	Excel編集メイン
 * 		S51Item s51Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
	public int s51_07EditExcel(S51Item s51Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		
		FileInputStream		fin				= null;
		HSSFWorkbook		workbook		= null;
		HSSFSheet			workSheet		= null;
		HSSFRow				workRow			= null;
		HSSFCell			workCell		= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s51Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S51ListBean s51ListBean = new S51ListBean();
		
		try {
			int		row				= 0;		// 模試数カウンタ
			int		maxSheetIndex	= 0;		// シートカウンター
			int		fileIndex		= 0;		// ファイルカウンター
			float		hensa			= 0;		// 最新模試の偏差値保持
			String		kmkCd			= "";		// 今年度の科目コード保持
			int		hyouCnt			= 0;		// 値範囲：０〜２(表カウンター)
			int		takouCnt		= 0;		// 値範囲：０〜６(他校カウンター)
			String		kmk				= "";		// 型・科目チェック用
			boolean	setFlg			= true;	// *セット判断フラグ
			ArrayList	homeList		= new ArrayList();	//自校情報格納リスト
			boolean	jikouFlg		= true;	// 自校フラグ
			
			// 型データセット
			ArrayList	s51List		= s51Item.getS51List();
			Iterator	itr	= s51List.iterator();
			
			while( itr.hasNext() ) {
				s51ListBean = (S51ListBean)itr.next();
				
				// 基本ファイルを読込む
				S51TakouListBean s51TakouListBean = new S51TakouListBean();
				
				// 他校データセット
				ArrayList s51TakouList = s51ListBean.getS51TakouList();
				Iterator itrTakou = s51TakouList.iterator();
				
				hensa = 0;
				takouCnt = 0;
				jikouFlg = true;
				String takou = "";
				homeList = new ArrayList();

				while ( itrTakou.hasNext() ){
					s51TakouListBean = (S51TakouListBean)itrTakou.next();
					
					if (takouCnt>=7 && hyouCnt>=2 && !cm.toString(takou).equals(cm.toString(s51TakouListBean.getStrGakkomei()))) {
						// 同型・科目で他校のデータが50シート目を超えたときファイル保存
						if(!cm.toString(takou).equals(cm.toString(s51TakouListBean.getStrGakkomei())) && maxSheetIndex==intMaxSheetSr){

							// Excelファイル保存
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex+1, masterfile, maxSheetIndex);
							fileIndex++;
							if( bolRet == false ){
								return errfwrite;
							}
						}
					}
					//改表、改シートチェック
					if (takouCnt>=7 && !cm.toString(takou).equals(cm.toString(s51TakouListBean.getStrGakkomei())) ) {
						hyouCnt++;
						takouCnt = 0;
						jikouFlg = false;
					}
					if ( hyouCnt>=3 ) {
						bolSheetCngFlg = true;
						hyouCnt = 0;
					}
					
					if(takouCnt==0 && hyouCnt==0 && maxSheetIndex==intMaxSheetSr){
						bolBookCngFlg = true;
					}
					//マスタExcel読み込み
					if(bolBookCngFlg){
						if((maxSheetIndex==intMaxSheetSr)||(maxSheetIndex==0)){
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook==null ){
								return errfread;
							}
							bolBookCngFlg = false;
							maxSheetIndex=0;
						}
					}
					if(bolSheetCngFlg){
						// シートテンプレートのコピー
						workSheet = workbook.cloneSheet(0);
						maxSheetIndex++;
		
						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);
		
						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, s51Item.getIntSecuFlg() ,26 ,28 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 26 );
						workCell.setCellValue(secuFlg);
		
						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( "学校名　：" + cm.toString(s51Item.getStrGakkomei()) );
			
						// 対象模試セット
						String moshi =cm.setTaisyouMoshi( s51Item.getStrMshDate() );	// 模試月取得
						workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
						workCell.setCellValue( "対象模試：" + cm.toString(s51Item.getStrMshmei()) + moshi);
						bolSheetCngFlg = false;
					}
				
					// 型・科目名+配点セット
					if ( takouCnt==0 ) {
						if ( !cm.toString(s51ListBean.getStrKmkCd()).equals(cm.toString(kmkCd)) || (takouCnt==0 && hyouCnt==0) ) {
							kmkCd = s51ListBean.getStrKmkCd();
							String haiten = "";
							if ( !cm.toString(s51ListBean.getStrHaitenKmk()).equals("") ) {
								haiten="(" + s51ListBean.getStrHaitenKmk() + ")";
							}
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]-1, 0 );
							workCell.setCellValue( "型・科目：" + cm.toString(s51ListBean.getStrKmkmei()) + haiten );
						}
					}
					
					//自校情報格納
					if ( cm.toString(s51TakouListBean.getStrGakkomei()).equals(cm.toString(s51Item.getStrGakkomei())) ) {
						homeList.add(s51TakouListBean);
					}
					
					if ( hyouCnt==0 && takouCnt==0 ) {
						//高校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt], tabCol[takouCnt] );
						workCell.setCellValue( s51Item.getStrGakkomei() );
						//同型・科目で改シートが発生したとき、最初の他校データには自校情報をセット
						if ( !cm.toString(s51TakouListBean.getStrGakkomei()).equals(cm.toString(s51Item.getStrGakkomei())) ) {
							// 自校データセット
							S51TakouListBean homeListBean = new S51TakouListBean();
							Iterator itrHome = homeList.iterator();
							int homeRow = 0;
							setFlg = true;
							while ( itrHome.hasNext() ){
								homeListBean = (S51TakouListBean)itrHome.next();
								// 模試名セット
								String moshi =cm.setTaisyouMoshi( homeListBean.getStrMshDate() );	// 模試月取得
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+homeRow, 0 );
								workCell.setCellValue( cm.toString(homeListBean.getStrMshmei()) + moshi );
								// 人数セット
								if ( homeListBean.getIntNinzu() != -999 ) {
									workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+homeRow, tabCol[takouCnt] );
									workCell.setCellValue( homeListBean.getIntNinzu() );
								}

								// 平均点セット
								if ( homeListBean.getFloHeikin() != -999.0 ) {
									workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+homeRow, tabCol[takouCnt]+1 );
									workCell.setCellValue( homeListBean.getFloHeikin() );
								}

								// 平均偏差値セット
								if ( homeListBean.getFloHensa() != -999.0 ) {
									workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+homeRow, tabCol[takouCnt]+3 );
									workCell.setCellValue( homeListBean.getFloHensa() );
								}
								//最新模試の偏差値保持
								if ( homeListBean.getFloHensa()!=-999.0 && setFlg ) {
									hensa = homeListBean.getFloHensa();	
									setFlg = false;
								}
								// *セット
								if (homeListBean.getFloHensa() != -999.0) {
									if ( hensa < homeListBean.getFloHensa() ) {
										workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+homeRow, tabCol[takouCnt]+2 );
										workCell.setCellValue("*");
									}
								}
								homeRow++;
							}
							takouCnt++;
							// 自校情報セット後には他校名セット
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt], tabCol[takouCnt] );
							workCell.setCellValue( s51TakouListBean.getStrGakkomei() );
						}
						takouCnt++;
						row = 0;
						setFlg = true;
					} else {
						if ( !cm.toString(takou).equals(cm.toString(s51TakouListBean.getStrGakkomei())) ) {
							// 他校名セット
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt], tabCol[takouCnt] );
							workCell.setCellValue( s51TakouListBean.getStrGakkomei() );
							takouCnt++;
							row = 0;
							setFlg = true;
						}
					}
					takou = s51TakouListBean.getStrGakkomei();
					
					if ( takouCnt==1 ) {
						// 模試名セット
						String moshi =cm.setTaisyouMoshi( s51TakouListBean.getStrMshDate() );	// 模試月取得
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, 0 );
						workCell.setCellValue( cm.toString(s51TakouListBean.getStrMshmei()) + moshi );
					}
					//最新模試の偏差値保持
					if ( s51TakouListBean.getFloHensa()!=-999.0 && setFlg ) {
						hensa = s51TakouListBean.getFloHensa();	
						setFlg = false;
					}
					
					// *セット → 2005/03/16 自校のみ*をつける
					if ( takouCnt == 1 && jikouFlg == true ) {
						if (s51TakouListBean.getFloHensa() != -999.0) {
							if ( hensa < s51TakouListBean.getFloHensa() ) {
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[takouCnt-1]+2 );
								workCell.setCellValue("*");
							}
						}
					}
					
					// 人数セット
					if ( s51TakouListBean.getIntNinzu() != -999 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[takouCnt-1] );
						workCell.setCellValue( s51TakouListBean.getIntNinzu() );
					}

					// 平均点セット
					if ( s51TakouListBean.getFloHeikin() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[takouCnt-1]+1 );
						workCell.setCellValue( s51TakouListBean.getFloHeikin() );
					}

					// 平均偏差値セット
					if ( s51TakouListBean.getFloHensa() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[takouCnt-1]+3 );
						workCell.setCellValue( s51TakouListBean.getFloHensa() );
					}
					row++;
					
				}
				hyouCnt++;
				
				// 50シート以上データが存在するときファイル保存
				if(hyouCnt >= 3 && itr.hasNext() && maxSheetIndex==intMaxSheetSr){

					// Excelファイル保存
					boolean bolRet = false;
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex+1, masterfile, maxSheetIndex);
					fileIndex++;
					if( bolRet == false ){
						return errfwrite;
					}
				}

			}
	
			// Excelファイル保存
			boolean bolRet = false;
			if(fileIndex != 0){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex+1, masterfile, maxSheetIndex);
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;					
			}
		
		} catch(Exception e) {
			log.Err("S51_07","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

}

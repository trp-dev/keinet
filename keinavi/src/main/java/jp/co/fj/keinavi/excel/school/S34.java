/*
 * 校内成績分析−クラス比較　志望大学評価別人数
 * 作成日: 2004/07/13
 * @author	C.Murata
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.school.S34Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

/**
 * @author keinet
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class S34 {

	/*
	 * 出力帳票選択処理
	 * フラグにより帳票出力の判断をする。
	 * 		S34Item s34Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID:ユーザID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */

	 public boolean s34(S34Item s34Item, ArrayList outfilelist, int intSaveFlg, String UserID, KNSheetLog sheetLog) {

		KNLog log = KNLog.getInstance(null,null,null);

	 	try{

	 		int intRunFlg = s34Item.getIntDaiTotalFlg();
	 		int ret = 0;
			//S34Itemから各帳票を出力
			switch(intRunFlg) {
				case 1:	//大学（日程なし）
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL START
//					log.Ep("S34_02","S34_02帳票作成開始","");
//					S34_02 exceledit02 = new S34_02();
//					ret = exceledit02.s34_02EditExcel(s34Item,outfilelist,intSaveFlg,UserID);
//					if( ret != 0 ){
//						log.Err("0"+ret+"S34_02","帳票作成エラー","");
//						return false;
//					}
//					log.Ep("S34_02","S34_02帳票作成終了","");
//					sheetLog.add("S34_02");
//					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL END
				case 2:	//大学（日程あり）
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL START
//					log.Ep("S34_01","S34_01帳票作成開始","");
//					S34_01 exceledit01 = new S34_01();
//					ret = exceledit01.s34_01EditExcel(s34Item,outfilelist,intSaveFlg,UserID);
//					if( ret != 0 ){
//						log.Err("0"+ret+"S34_01","帳票作成エラー","");
//						return false;
//					}
//					log.Ep("S34_01","S34_01帳票作成終了","");
//					sheetLog.add("S34_01");
//					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL END
				case 3:	//学部（日程あり）
// 2019/10/02 QQ)Tanioka 廃止帳票 ADD START
				case 4:
// 2019/10/02 QQ)Tanioka 廃止帳票 ADD END
					log.Ep("S34_04","S34_04帳票作成開始","");
					S34_04 exceledit04 = new S34_04();
					ret = exceledit04.s34_04EditExcel(s34Item,outfilelist,intSaveFlg,UserID);
					if( ret != 0 ){
						log.Err("0"+ret+"S34_04","帳票作成エラー","");
						return false;
					}
					log.Ep("S34_04","S34_04帳票作成終了","");
					sheetLog.add("S34_04");
					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL START
//				case 4:	//学部（日程なし）
//					log.Ep("S34_03","S34_03帳票作成開始","");
//					S34_03 exceledit03 = new S34_03();
//					ret = exceledit03.s34_03EditExcel(s34Item,outfilelist,intSaveFlg,UserID);
//					if( ret != 0 ){
//						log.Err("0"+ret+"S34_03","帳票作成エラー","");
//						return false;
//					}
//					log.Ep("S34_03","S34_03帳票作成終了","");
//					sheetLog.add("S34_03");
//					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL END
				case 5:	//学科
					log.Ep("S34_05","S34_05帳票作成開始","");
					S34_05 exceledit05 = new S34_05();
					ret = exceledit05.s34_05EditExcel(s34Item,outfilelist,intSaveFlg,UserID);
					if( ret != 0 ){
						log.Err("0"+ret+"S34_05","帳票作成エラー","");
						return false;
					}
					log.Ep("S34_05","S34_05帳票作成終了","");
					sheetLog.add("S34_05");
					break;

				default:
				    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
				    //throw new Exception("S34 ERROR : フラグ異常 ");
				    throw new IllegalStateException("S34 ERROR : フラグ異常");
				    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			}
	 	}
	 	catch(Exception e){
			log.Err("99S34","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
	 	}

	 	return true;

	 }

}

package jp.co.fj.keinavi.util.csv.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 *
 * CSV行レコードオブジェクト
 * 
 * 2005.10.28	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class CSVLineRecord {

	// 行データ
	private final String[] record;
	// 行番号
	private final int lineNo;
	
	// 変更前データ
	private String[] original = null;
	// レコードタイプ
	// 1 ... 新規
	// 2 ... 更新
	// 3 ... 削除
	// 4 ... 進級？？
	private int recordType = 0;
	// エラーレコードのリスト
	private final List errorDataList = new ArrayList();

	/**
	 * コンストラクタ
	 * 
	 * @param pRecord 行レコード
	 * @param pLineNo 行番号
	 */
	public CSVLineRecord(final String[] pRecord, final int pLineNo) {
		
		this.record = pRecord;
		this.lineNo = pLineNo;
	}

	/**
	 * エラー情報をクリアする
	 */
	public void clearError() {
		errorDataList.clear();
	}
	
	/**
	 * エラーデータを追加する
	 * 
	 * @param pErrorItemName エラー発生項目名（列名）
	 * @param pErrorContent エラー内容
	 */
	public void addError(final String pErrorItemName,
			final String pErrorContent) {
		
		errorDataList.add(
				new CSVRegisterError(lineNo, pErrorItemName, pErrorContent));
	}
	
	/**
	 * エラーデータを追加する
	 * 
	 * @param pErrorContent エラー内容
	 */
	public void addError(final String pErrorContent) {
		
		errorDataList.add(
				new CSVRegisterError(lineNo, null, pErrorContent));
	}
	
	/**
	 * @return エラーが発生しているかどうか
	 */
	public boolean hasError() {
		return errorDataList.size() > 0;
	}
	
	/**
	 * @return 新規レコードであるかどうか
	 */
	public boolean isInsertRecord() {
		return recordType == 1;
	}
	
	/**
	 * @return 更新レコードであるかどうか
	 */
	public boolean isUpdateRecord() {
		return recordType == 2;
	}
	
	/**
	 * @return 削除レコードであるかどうか
	 */
	public boolean isDeleteRecord() {
		return recordType == 3;
	}
	
	/**
	 * このレコードの種別を「新規」にする
	 */
	public void setInsertRecord() {
		recordType = 1;
	}
	
	/**
	 * このレコードの種別を「更新」にする
	 */
	public void setUpdateRecord() {
		recordType = 2;
	}
	
	/**
	 * このレコードの種別を「削除」にする
	 */
	public void setDeleteRecord() {
		recordType = 3;
	}
	
	/**
	 * @return errorDataList を戻します。
	 */
	public List getErrorDataList() {
		return errorDataList;
	}

	/**
	 * @return record を戻します。
	 */
	public String[] getRecord() {
		return record;
	}

	/**
	 * @return original を戻します。
	 */
	public String[] getOriginal() {
		return original;
	}

	/**
	 * @param pOriginal 設定する original。
	 */
	public void setOriginal(final String[] pOriginal) {
		this.original = pOriginal;
	}

	/**
	 * @return lineNo を戻します。
	 */
	public int getLineNo() {
		return lineNo;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return
				new ToStringBuilder(this)
						.append("Record", record)
						.append("LineNo", lineNo)
						.append("RecordType", recordType)
						.toString();
	}
	
}

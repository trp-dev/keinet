package jp.co.fj.keinavi.servlets.sales;

import jp.co.fj.keinavi.interfaces.SpecialAppliMenu;
import jp.co.fj.keinavi.interfaces.SpecialAppliStatus;
import jp.co.fj.keinavi.servlets.sales.base.BaseAcceptServlet;

/**
 * 
 * 特例成績データ作成承認（高校事業企画部）画面のサーブレットです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD204Servlet extends BaseAcceptServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = -3718735100862292537L;

	/**
	 * @see jp.co.fj.keinavi.servlets.sales.base.BaseSpecialAppliServlet#getMenu()
	 */
	protected SpecialAppliMenu getMenu() {
		return SpecialAppliMenu.ACCEPT_KIKAKU;
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.sales.base.BaseAcceptServlet#getUnAcceptedStatus()
	 */
	protected SpecialAppliStatus getUnAcceptedStatus() {
		return SpecialAppliStatus.PENDING_KIKAKU;
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.sales.base.BaseAcceptServlet#getAcceptedStatus()
	 */
	protected SpecialAppliStatus getAcceptedStatus() {
		return SpecialAppliStatus.PENDING_KANRI;
	}

}

package jp.co.fj.keinavi.beans.graph;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.com_set.ComClassData;
import jp.co.fj.keinavi.data.com_set.ComSubjectData;
import jp.co.fj.keinavi.data.com_set.CompClassData;
import jp.co.fj.keinavi.data.com_set.CompSchoolData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.BaseForm;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 *
 * グラフ出力対象の設定を調べるBean
 * 
 * 2006.07.27	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public abstract class AbstractGraphCheckerBean
		implements IProfileCategory, IProfileItem {

	// 共通項目設定の有効値
	private final Short validValue = Short.valueOf("1");
	// 出力対象となる下限値
	private final Short validLowValue = Short.valueOf("10");
	
	// プロファイルカテゴリID
	private final String categoryId;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pItem
	 */
	public AbstractGraphCheckerBean(final String pCategoryId) {
		this.categoryId = pCategoryId;
	}
	
	/**
	 * @param request HTTPリクエスト
	 * @param isBundle 一括出力であるか
	 * @return 型のグラフ設定チェック
	 */
	public abstract int checkTypeConfig(
			HttpServletRequest request, boolean isBundle);
	
	/**
	 * @param request HTTPリクエスト
	 * @param isBundle 一括出力であるか
	 * @return 科目のグラフ設定チェック
	 */
	public abstract int checkCourseConfig(
			HttpServletRequest request, boolean isBundle);
	
	/**
	 * @param request HTTPリクエスト
	 * @param isBundle 一括出力であるか
	 * @return 比較対象高校のグラフ設定チェック
	 */
	public abstract int checkClassConfig(
			HttpServletRequest request, boolean isBundle);
	
	/**
	 * @param request HTTPリクエスト
	 * @param isBundle 一括出力であるか
	 * @return 比較対象クラスのグラフ設定チェック
	 */
	public abstract int checkSchoolConfig(
			HttpServletRequest request, boolean isBundle);

	/**
	 * @param request HTTPリクエスト
	 * @return 型・科目を使うグラフ出力対象帳票を出力するか
	 */
	public abstract boolean hasSubjectGraphSheet(HttpServletRequest request);

	/**
	 * @param request HTTPリクエスト
	 * @return 比較対象クラスを使うグラフ出力対象帳票を出力するか
	 */
	public abstract boolean hasClassGraphSheet(HttpServletRequest request);
	
	/**
	 * @param request HTTPリクエスト
	 * @return 比較対象高校を使うグラフ出力対象帳票を出力するか
	 */
	public abstract boolean hasSchoolGraphSheet(HttpServletRequest request);
	
	/**
	 * @param request HTTPリクエスト
	 * @return プロファイル
	 */
	protected final Profile getProfile(HttpServletRequest request) {
		return (Profile) request.getSession(false).getAttribute(Profile.SESSION_KEY);
	}
	
	/**
	 * @param request HTTPリクエスト
	 * @return プロファイルアイテムマップ
	 **/
	protected final Map getItemMap(HttpServletRequest request) {
		return getProfile(request).getItemMap(categoryId);
	}

	/**
	 * @param request HTTPリクエスト
	 * @param id アイテムID
	 * @return 有効範囲にある値かどうか
	 */
	protected boolean isValidRange(final HttpServletRequest request, final String id) {

		final Map item = getItemMap(request);
		
		if (item == null) {
			return false;
		}
		
		if (item.containsKey(id)) {
			return validLowValue.compareTo((Short) item.get(id)) < 0;
		} else {
			return false;
		}
	}

	/**
	 * @param request HTTPリクエスト
	 * @param id アイテムID
	 * @return 有効な値であるか
	 */
	protected boolean isValidValue(final HttpServletRequest request, final String id) {

		final Map item = getItemMap(request);
		
		if (item == null) {
			return false;
		}
		
		if (item.containsKey(id)) {
			return validValue.equals(item.get(id));
		} else {
			return false;
		}
	}

	/**
	 * @param request HTTPリクエスト
	 * @return 対象模試
	 */
	protected ExamData getExamData(final HttpServletRequest request) {
		
		// 模試セッション
		final ExamSession es = (ExamSession) request.getSession(
				false).getAttribute(ExamSession.SESSION_KEY);
		// アクションフォーム
		final BaseForm form = (BaseForm) request.getAttribute("form");
		
		return es.getExamData(form.getTargetYear(), form.getTargetExam());
	}

	/**
	 * @param request HTTPリクエスト
	 * @return 一括出力対象であるか
	 */
	public boolean isBundleTarget(final HttpServletRequest request) {

		final Map item = getItemMap(request);
		
		if (item == null) {
			return false;
		}
		
		return validValue.equals(item.get(PRINT_OUT_TARGET));
	}
	
	/**
	 * @param request HTTPリクエスト
	 * @param mode 型 or 科目
	 * @return 共通項目設定（型・科目）のグラフ設定チェック結果
	 */
	protected int checkCommonSubject(final HttpServletRequest request, final int mode) {
		
		// 対象模試
		final ExamData exam = getExamData(request);
		
		// 対象模試がなければここまで
		if (exam == null) {
			return 0;
		}

		// プロファイル
		final Profile profile = getProfile(request);
		// 共通項目設定
		final Map cmItem = profile.getItemMap(CM);
		
		// グラフ出力対象を取得
		final String[] target;
		
		// 型
		if (mode == 1) {

			final ComSubjectData data = ProfileUtil.getComTypeData(profile, exam);
			
			// 設定がなければここまで
			if (data == null) {
				return 1;
			}
			
			// 全選択
			if (validValue.equals(cmItem.get(TYPE_SELECTION))) {
				target = ProfileUtil.getSubjectAllValue(data, true);
			// 個別選択
			} else {
				target = ProfileUtil.getSubjectIndValue(data, true);
			}

		// 科目
		} else {

			final ComSubjectData data = ProfileUtil.getComCourseData(profile, exam);

			// 設定がなければここまで
			if (data == null) {
				return 1;
			}
			
			// 全選択
			if (validValue.equals(cmItem.get(COURSE_SELECTION))) {
				target = ProfileUtil.getSubjectAllValue(data, true);
			// 個別選択
			} else {
				target = ProfileUtil.getSubjectIndValue(data, true);
			}
		}
		
		return target.length > 0 ? 0 : 1;
	}
	
	/**
	 * @param request HTTPリクエスト
	 * @param mode 型 or 科目
	 * @return 個別設定（型・科目）のグラフ設定チェック結果
	 */
	protected int checkIndSubject(final HttpServletRequest request, final int mode) {
		
		final Map item = getItemMap(request);
		
		// グラフ出力対象を取得
		final ComSubjectData data;

		// 型
		if (mode == 1) {
			data = (ComSubjectData) item.get(TYPE_IND);
		// 科目
		} else {
			data = (ComSubjectData) item.get(COURSE_IND);
		}

		// 個別設定がなければここまで
		if (data == null) {
			return 2;
		}
		
		return ProfileUtil.getSubjectIndValue(data, true).length > 0 ? 0 : 2;
	}
	
	/**
	 * @param request HTTPリクエスト
	 * @return 共通項目設定（比較対象クラス）のグラフ設定チェック結果
	 */
	protected int checkCommonClass(final HttpServletRequest request) {
		
		// 対象模試
		final ExamData exam = getExamData(request);
		
		// 対象模試がなければここまで
		if (exam == null) {
			return 0;
		}

		// プロファイル
		final Profile profile = getProfile(request);
		// 共通項目設定
		final Map cmItem = profile.getItemMap(CM);
		// 設定リスト
		final List c = (List) cmItem.get(CLASS_COMP_CLASS);
		
		// 設定がなければここまで
		if (c == null) {
			return 0;
		}
		
		// 対象模試の設定を探す
		final int index = c.indexOf(
				new CompClassData(exam.getExamYear(), exam.getExamCD()));

		// 設定あり
		final List list;
		if (index >= 0) {
			final CompClassData data = (CompClassData) c.get(index);
		    // 全選択
			if (validValue.equals(cmItem.get(CLASS_SELECTION))) {
				list = data.getAClassData();
			// 個別選択
			} else {
				list = data.getIClassData();
			}
		// 設定なし
		} else {
			return 1;
		}

		// グラフ表示対象があるか調べる
		for (final Iterator ite = list.iterator(); ite.hasNext();) {
			final ComClassData data = (ComClassData) ite.next();
			if (data.getGraphDisp() == 1) {
				return 0;
			}
		}

		return 1;
	}

	/**
	 * @param request HTTPリクエスト
	 * @return 共通項目設定（比較対象高校）のグラフ設定チェック結果
	 */
	protected int checkCommonSchool(final HttpServletRequest request) {
		
		// プロファイル
		final Profile profile = getProfile(request);
		// 共通項目設定
		final Map cmItem = profile.getItemMap(CM);
		
		final List list = (List) cmItem.get(SCHOOL_COMP_SCHOOL);

		// 設定がなければここまで
		if (list == null) {
			return 0;
		}
		
		// グラフ表示対象があるか調べる
		for (final Iterator ite = list.iterator(); ite.hasNext();) {
			final CompSchoolData data = (CompSchoolData) ite.next();
			if (data.getGraphDisp() == 1) {
				return 0;
			}
		}

		return 1;
	}

}

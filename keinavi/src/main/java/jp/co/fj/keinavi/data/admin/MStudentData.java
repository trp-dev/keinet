/*
 * 作成日: 2004/10/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.admin;

import java.io.Serializable;

/**
 * 
 * 
 * @author kawai
 */
public class MStudentData implements Serializable {

	String individualid; // 個人ID
	String year1; // 前々年度
	String grade1; //（前々年度）学年
	String className1; //（前々年度）クラス
	String classNo1; //（前々年度）クラス番号
	String year2; // 前年度
	String grade2; //（前年度）学年
	String className2; //（前年度）クラス
	String classNo2; //（前年度）クラス番号
	String year3; // 今年度
	String grade3; //（今年度）学年
	String className3; //（今年度）クラス
	String classNo3; //（今年度）クラス番号
	String sex; // 性別
	String nameKana; // かな氏名
	String nameKanji; // 漢字氏名
	String birthday; // 誕生日
	String telNo; // 電話番号

	/**
	 * @return
	 */
	public String getBirthday() {
		return birthday;
	}

	/**
	 * @return
	 */
	public String getClassName1() {
		return className1;
	}

	/**
	 * @return
	 */
	public String getClassName2() {
		return className2;
	}

	/**
	 * @return
	 */
	public String getClassName3() {
		return className3;
	}

	/**
	 * @return
	 */
	public String getClassNo1() {
		return classNo1;
	}

	/**
	 * @return
	 */
	public String getClassNo2() {
		return classNo2;
	}

	/**
	 * @return
	 */
	public String getGrade1() {
		return grade1;
	}

	/**
	 * @return
	 */
	public String getGrade2() {
		return grade2;
	}

	/**
	 * @return
	 */
	public String getGrade3() {
		return grade3;
	}

	/**
	 * @return
	 */
	public String getIndividualid() {
		return individualid;
	}

	/**
	 * @return
	 */
	public String getNameKana() {
		return nameKana;
	}

	/**
	 * @return
	 */
	public String getNameKanji() {
		return nameKanji;
	}

	/**
	 * @return
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @return
	 */
	public String getTelNo() {
		return telNo;
	}

	/**
	 * @param string
	 */
	public void setBirthday(String string) {
		birthday = string;
	}

	/**
	 * @param string
	 */
	public void setClassName1(String string) {
		className1 = string;
	}

	/**
	 * @param string
	 */
	public void setClassName2(String string) {
		className2 = string;
	}

	/**
	 * @param string
	 */
	public void setClassName3(String string) {
		className3 = string;
	}

	/**
	 * @param string
	 */
	public void setClassNo1(String string) {
		classNo1 = string;
	}

	/**
	 * @param string
	 */
	public void setClassNo2(String string) {
		classNo2 = string;
	}

	/**
	 * @param string
	 */
	public void setGrade1(String string) {
		grade1 = string;
	}

	/**
	 * @param string
	 */
	public void setGrade2(String string) {
		grade2 = string;
	}

	/**
	 * @param string
	 */
	public void setGrade3(String string) {
		grade3 = string;
	}

	/**
	 * @param string
	 */
	public void setIndividualid(String string) {
		individualid = string;
	}

	/**
	 * @param string
	 */
	public void setNameKana(String string) {
		nameKana = string;
	}

	/**
	 * @param string
	 */
	public void setNameKanji(String string) {
		nameKanji = string;
	}

	/**
	 * @param string
	 */
	public void setSex(String string) {
		sex = string;
	}

	/**
	 * @param string
	 */
	public void setTelNo(String string) {
		telNo = string;
	}

	/**
	 * @return
	 */
	public String getClassNo3() {
		return classNo3;
	}

	/**
	 * @param string
	 */
	public void setClassNo3(String string) {
		classNo3 = string;
	}

	/**
	 * @return
	 */
	public String getYear1() {
		return year1;
	}

	/**
	 * @return
	 */
	public String getYear2() {
		return year2;
	}

	/**
	 * @return
	 */
	public String getYear3() {
		return year3;
	}

	/**
	 * @param string
	 */
	public void setYear1(String string) {
		year1 = string;
	}

	/**
	 * @param string
	 */
	public void setYear2(String string) {
		year2 = string;
	}

	/**
	 * @param string
	 */
	public void setYear3(String string) {
		year3 = string;
	}

}

package jp.co.fj.keinavi.beans.sheet.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import jp.co.fj.kawaijuku.judgement.data.UnivInfoBean;
import jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean;
import jp.co.fj.keinavi.beans.individual.judgement.JudgementListBean;
import jp.co.fj.keinavi.data.sheet.ExtI11ListBean;
import jp.co.fj.keinavi.excel.data.personal.I15CalendarListBean;
import jp.co.fj.keinavi.excel.data.personal.I15HanteiRirekiListBean;
import jp.co.fj.keinavi.excel.data.personal.I15HensaListBean;
import jp.co.fj.keinavi.excel.data.personal.I15HyoukaListBean;
import jp.co.fj.keinavi.excel.data.personal.I15KmkHensaListBean;
import jp.co.fj.keinavi.excel.data.personal.I15ScheduleListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.servlets.individual.BaseJudgeThread;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.QueryLoader;

/**
 * 個人成績分析−成績分析面談 受験予定大学スケジュールデータリスト印刷用Bean
 *
 *
 * 2005.02.23   Tomohisa YAMADA - Totec
 * 				[1]判定結果をDetailPageBeanで
 * 				一本化する。
 *
 * 2005.04.14   Keisuke KONDO - Totec
 * 				[2]対象模試が新テストだった場合、
 * 				種別フラグに"1"を入れるように修正
 *
 * 2005.09.16   Keisuke KONDO - Totec
 *  			[3]科目取得の際EXAMSUBJECTではなく、
 *  			Viewを参照するように修正
 *
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 *              ・センター・リサーチ５段階評価対応
 *              ・判定処理の隠蔽
 *
 * 2009.11.30   Tomohisa YAMADA - Totec
 *              ・5教科判定対応
 *
 * 2009.12.01   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 *
 * 2010.01.18	Shoji HASE - Totec
 *              ・「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 *				  「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 * 	　　　　　　・「HS3_UNIV_PUB：高3公表用判定大学マスタ」テーブルを
 *				  「UNIVMASTER_CHOICESCHOOL：大学マスタ模試用志望校」テーブルに置き換え
 *
 * <2010年度＃マーク対応（判定軽量化）>
 * 2010.10.25	Tomohisa YAMADA - Totec
 * 				公表用データを随時取得
 *
 *
 */
public class I15SheetBean extends IndAbstractSheetBean {

    /** serialVersionUID */
    private static final long serialVersionUID = 7655728630770325277L;

    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws SQLException, Exception {

        /**プロファイル関連**/
        //受験大学スケジュール出力フォーマット
        data.setIntScheduleFormatFlg(((Short) profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).get(IProfileItem.TEXT_OUT_FORMAT)).shortValue());
        //受験大学スケジュールセキュリティスタンプ
        data.setIntScheduleSecuFlg(((Short) profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).get(IProfileItem.PRINT_STAMP)).shortValue());

        data.setIntScheduleFlg(1);
        data.setIntScheduleShubetsuFlg(getNewTestFlg());//[2] add 新テストフラグ（受験予定スケジュールシート）

        //下記条件の時に値を格納
        //・受験予定大学スケジュールフラグ == 1

        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;
        PreparedStatement ps4 = null;
        PreparedStatement ps5 = null;
        PreparedStatement ps6 = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        ResultSet rs4 = null;
        ResultSet rs5 = null;
        ResultSet rs6 = null;

        String cExamNameAbbr = "";
        String sExamNameAbbr = "";

        //１．全生徒
        for (java.util.Iterator it=data.getI11List().iterator(); it.hasNext();) {
            try {
                ps1 = null;
                ps2 = null;
                ps3 = null;
                ps4 = null;
                ps5 = null;
                ps6 = null;
                final int MAXEXAMCOUNT = 7;
                int examCount = 0;
                List examList = new ArrayList();

                //高３大学マスタの最新（年度・区分）
                ps5 = conn.prepareStatement(latestUnivData());
                rs5 = ps5.executeQuery();
                String univYear = null;
                String univDiv = null;
                if(rs5.next()) {
                    univYear = rs5.getString("EXAMYEAR");
                    univDiv = rs5.getString("EXAMDIV");
                }
                if(ps5 != null) {ps5.close();}
                if(rs5 != null) {rs5.close();}

                ExtI11ListBean printData = (ExtI11ListBean) it.next();

                //ヌルは入らない、が、判定データが渡ってくるまでのヌル回避仮対応
                boolean testFlg = true;
                Map map = new HashMap();

                /** 判定結果取得 */
                final Map judgedMap = BaseJudgeThread.getSheetJudgedMap(session);

                if (judgedMap != null) {
                    //必要データ
                    //合格可能性判定、センター	（１：Ａ　２：Ｂ　３：Ｃ　４：Ｄ　５：Ｅ）
                    //合格可能性判定、２次		（１：Ａ　２：Ｂ　３：Ｃ　４：Ｄ　５：Ｅ）
                    //配点（英、数、国、理、地、公、その他）
                    //セッションに格納してある特定の生徒の結果一覧を取得
                    JudgementListBean jlb = (JudgementListBean) judgedMap.get(printData.getIndividualid());
                    //個人単位に模試名があるのでここでセット
                    cExamNameAbbr = jlb.getCExamNameAbbr();
                    sExamNameAbbr = jlb.getSExamNameAbbr();

                    for(int i=0;i<(jlb.getRecordSet()).size();i++){

                        DetailPageBean makeBean = (DetailPageBean)((jlb.getRecordSet()).get(i));//[1] add

                        String[] judgeDatas = new String[13];

                        //情報誌用科目データ作成
                        //強制的に最初の枝番で配点を出す（仕様）
                        SortedMap infoMap = (SortedMap) univInfoMap.get(makeBean.getUniqueKey());

                        //情報誌用科目データが存在する場合のみ実行
                        if(infoMap != null){

                            //final int CER = 0;	//センター評価
                            //final int SER = 1;	//２次評価
                            //final int EN1 = 2;	//模試名センター
                            //final int EN2 = 3;	//模試名２次
                            //final int CDT = 4;	//センター試験実施予定日
                            //final int SDT = 5;	//２次試験実施予定日
                            //final int ENG = 6;	//英語
                            //final int MAT = 7;	//数学
                            //final int JAP = 8;	//国語
                            //final int SCI = 9;	//理科
                            //final int GEO = 10;	//地理
                            //final int SIV = 11;	//公民
                            //final int ETC = 12;	//その他

                            judgeDatas[2]  = jlb.getCExamNameAbbr();//センター模試名
                            judgeDatas[3]  = jlb.getSExamNameAbbr();//セカンド模試名

                            //センター試験
                            if(judgeDatas[2] == null || judgeDatas[2].trim().equals("")) {
                                judgeDatas[0]  = null;
                            } else {
                                judgeDatas[0]  = makeBean.getCLetterJudgement();//評価
                            }
                            //２次試験　評価
                            if(judgeDatas[3] == null || judgeDatas[3].trim().equals("")) {
                                judgeDatas[1]  = null;
                            } else {
                                judgeDatas[1]  = makeBean.getSLetterJudgement();//評価
                            }

                            judgeDatas[4]  = makeBean.getC_examInpleDate();//センター試験実施予定日
                            judgeDatas[5]  = makeBean.getS_examInpleDate();//２次試験実施予定日

                            //２次教科配点
                            UnivInfoBean infoBean = (UnivInfoBean) infoMap.values().iterator().next();
                            judgeDatas[6] = KNUtil.splitInfoAllot(infoBean.getAllot2())[1];
                        } else {

                            judgeDatas[2]  = jlb.getCExamNameAbbr();//センター模試名
                            judgeDatas[3]  = jlb.getSExamNameAbbr();//セカンド模試名
                            //センター試験
                            if(judgeDatas[2] == null || judgeDatas[2].trim().equals("")) {
                                judgeDatas[0]  = null;
                            } else {
                                judgeDatas[0]  = makeBean.getCLetterJudgement();//評価
                            }
                            //２次試験　評価
                            if(judgeDatas[3] != null || !(judgeDatas[3].trim().equals(""))) {
                                judgeDatas[1]  = null;
                            } else {
                                judgeDatas[1]  = makeBean.getSLetterJudgement();//評価
                            }
                            judgeDatas[4]  = makeBean.getC_examInpleDate();	//センター試験実施予定日
                            judgeDatas[5]  = makeBean.getS_examInpleDate();	//２次試験実施予定日
                        }

                        //マップにいれる
                        map.put(makeBean.getUnivCd() + makeBean.getFacultyCd() + makeBean.getDeptCd(), judgeDatas);
                    }
                } else {
                    //仮フラグ
                    testFlg = false;
                }
                /** 判定結果取得 */
                ///スケジュールcreate
                I15ScheduleListBean schedule = new I15ScheduleListBean();
                //スケジュールset
                ps1 = conn.prepareStatement(examDataSql());
                ps1.setString(1, printData.getIndividualid());
                ps1.setString(2, getThisYear());
                rs1 = ps1.executeQuery();
                ps2 = conn.prepareStatement(deviationDataSql());
                while(rs1.next()) {/**Ａコース*/
                    if(examCount >= MAXEXAMCOUNT) {
                        break;
                    }
                    //create
                    I15HensaListBean examData = new I15HensaListBean();
                    //set
                    examData.setStrMshmei(rs1.getString("EXAMNAME_ABBR"));	//模試名
                    examData.setStrMshDate(rs1.getString("INPLEDATE"));	//模試実施基準日

                    //模試情報
                    String examination = rs1.getString("EXAMYEAR") + "," + rs1.getString("EXAMCD") +
                                            "," + rs1.getString("EXAMNAME_ABBR") + "," + rs1.getString("INPLEDATE");
                    examList.add(examination);

                    ps2.setString(1, printData.getIndividualid());
                    ps2.setString(2, rs1.getString("EXAMYEAR"));
                    ps2.setString(3, rs1.getString("EXAMCD"));
                    ps2.setString(4, printData.getIndividualid());
                    ps2.setString(5, rs1.getString("EXAMYEAR"));
                    ps2.setString(6, rs1.getString("EXAMCD"));
                    rs2 = ps2.executeQuery();
                    while(rs2.next()) {
                        //create
                        I15KmkHensaListBean subData = new I15KmkHensaListBean();
                        //set
                        subData.setStrKyokaCd(rs2.getString("COURSECD"));           //教科コード
                        subData.setStrKyokamei(rs2.getString("COURSENAME"));        //教科名
                        subData.setStrKmkmei(rs2.getString("SUBADDRNAME"));         //型・科目名
                        subData.setFloHensa(rs2.getFloat("A_DEVIATION"));           //偏差値
                        subData.setStrScholarLevel(rs2.getString("ACADEMICLEVEL")); //学力レベル
                        subData.setBasicFlg(rs2.getString("BASICFLG"));             //基礎科目フラグ
                        //add
                        examData.getI15KmkHensaList().add(subData);
                    }
                    if(rs2 != null) {rs2.close();}
                    //add
                    schedule.getI15HensaList().add(examData);
                    examCount++;
                }
                if(rs1 != null) {rs1.close();}
                if(ps1 != null) {ps1.close();}
                if(ps2 != null) {ps2.close();}
                ps3 = conn.prepareStatement(QueryLoader.getInstance().load("i15_01").toString());
                ps3.setString(1, printData.getIndividualid());
                rs3 = ps3.executeQuery();
                ps4 = conn.prepareStatement(patternSql());

                while(rs3.next()) {/**Ｂコース*/

                    String[] judgeDatas = new String[13];

                    //仮対応
                    if(testFlg) {
                        judgeDatas = (String[])map.get(rs3.getString("UNIVCD") + rs3.getString("FACULTYCD") + rs3.getString("DEPTCD"));
                        if(judgeDatas == null) {
                            int count= 0;
                            judgeDatas = new String[13];
                            judgeDatas[count++] = null;//0
                            judgeDatas[count++] = null;//1
                            judgeDatas[count++] = cExamNameAbbr;//2
                            judgeDatas[count++] = sExamNameAbbr;//3
                            judgeDatas[count++] = null;//4
                            judgeDatas[count++] = null;//5
                            judgeDatas[count++] = null;//6
                            judgeDatas[count++] = null;//7
                            judgeDatas[count++] = null;//8
                            judgeDatas[count++] = null;//9
                            judgeDatas[count++] = null;//10
                            judgeDatas[count++] = null;//11
                            judgeDatas[count++] = null;//12
                        }
                    } else {
                        int count= 0;
                        judgeDatas[count++] = null;//0
                        judgeDatas[count++] = null;//1
                        judgeDatas[count++] = cExamNameAbbr;//2
                        judgeDatas[count++] = sExamNameAbbr;//3
                        judgeDatas[count++] = null;//4
                        judgeDatas[count++] = null;//5
                        judgeDatas[count++] = null;//6
                        judgeDatas[count++] = null;//7
                        judgeDatas[count++] = null;//8
                        judgeDatas[count++] = null;//9
                        judgeDatas[count++] = null;//10
                        judgeDatas[count++] = null;//11
                        judgeDatas[count++] = null;//12
                    }
                    //仮対応
                    //create
                    I15CalendarListBean calenderData = new I15CalendarListBean();
                    //set
                    calenderData.setStrDaigakuMei(rs3.getString("UNIVNAME_ABBR"));		//大学名
                    calenderData.setStrGakubuMei(rs3.getString("FACULTYNAME_ABBR"));	//学部
                    calenderData.setStrGakkaMei(rs3.getString("DEPTNAME_ABBR"));		//学科
                    //複数パターンフラグ

                    ps4.setString(1, univYear);
                    ps4.setString(2, univDiv);
                    ps4.setString(3, rs3.getString("UNIVCD"));
                    ps4.setString(4, rs3.getString("FACULTYCD"));
                    ps4.setString(5, rs3.getString("DEPTCD"));
                    rs4 = ps4.executeQuery();
                    if(rs4.next()) {
                        if(rs4.getInt("BRANCHCOUNT") > 1) {
                            calenderData.setIntPluralFlg(1);
                        } else {
                            calenderData.setIntPluralFlg(0);
                        }
                    } else {
                        calenderData.setIntPluralFlg(0);
                    }
                    if(rs4 != null) {rs4.close();}

                    //複数パターンフラグ
                    calenderData.setStrDaigakuKbn(rs3.getString("UNIVDIVCD"));			//大学区分
                    calenderData.setStrNittei(rs3.getString("SCHEDULECD"));				//日程コード
                    calenderData.setStrShikenti1(rs3.getString("PROVENTEXAMSITE1"));	//地方試験地1
                    calenderData.setStrShikenti2(rs3.getString("PROVENTEXAMSITE2"));	//地方試験地2
                    calenderData.setStrShikenti3(rs3.getString("PROVENTEXAMSITE3"));	//地方試験地3
                    calenderData.setStrShikenti4(rs3.getString("PROVENTEXAMSITE4"));	//地方試験地4
                    calenderData.setStrShikenti5(rs3.getString("PROVENTEXAMSITE5"));	//地方試験地5
                    calenderData.setStrShikenti6(rs3.getString("PROVENTEXAMSITE6"));	//地方試験地6
                    calenderData.setStrShikenti7(rs3.getString("PROVENTEXAMSITE7"));	//地方試験地7
                    calenderData.setStrShikenti8(rs3.getString("PROVENTEXAMSITE8"));	//地方試験地8
                    calenderData.setStrShikenti9(rs3.getString("PROVENTEXAMSITE9"));	//地方試験地9
                    calenderData.setStrShikenti10(rs3.getString("PROVENTEXAMSITE10"));	//地方試験地10
                    calenderData.setStrJukenbi1(rs3.getString("EXAMPLANDATE1"));		//受験日1
                    calenderData.setStrJukenbi2(rs3.getString("EXAMPLANDATE2"));		//受験日2
                    calenderData.setStrJukenbi3(rs3.getString("EXAMPLANDATE3"));		//受験日3
                    calenderData.setStrJukenbi4(rs3.getString("EXAMPLANDATE4"));		//受験日4
                    calenderData.setStrJukenbi5(rs3.getString("EXAMPLANDATE5"));		//受験日5
                    calenderData.setStrJukenbi6(rs3.getString("EXAMPLANDATE6"));		//受験日6
                    calenderData.setStrJukenbi7(rs3.getString("EXAMPLANDATE7"));		//受験日7
                    calenderData.setStrJukenbi8(rs3.getString("EXAMPLANDATE8"));		//受験日8
                    calenderData.setStrJukenbi9(rs3.getString("EXAMPLANDATE9"));		//受験日9
                    calenderData.setStrJukenbi10(rs3.getString("EXAMPLANDATE10"));		//受験日10
                    calenderData.setStrSyutsuganbi(rs3.getString("APPLIDEADLINE"));		//出願締切日
                    calenderData.setStrGoukakubi(rs3.getString("SUCANNDATE"));			//合格発表日
                    calenderData.setStrTetsudukibi(rs3.getString("PROCEDEADLINE"));		//入学手続締切日
                    calenderData.setFloBorderTokuritsu(rs3.getFloat("CEN_SCORERATE"));	//ボーダー得点率
                    calenderData.setFloRankHensa(rs3.getFloat("RANKLLIMITS"));			//難易ランク偏差値
                    calenderData.setStrRankName(rs3.getString("RANKNAME"));				//難易ランク名

                    //仮対応中
                    calenderData.setStrCourseAllot(judgeDatas[6]);		//２次教科配点
                    calenderData.setStrBiko(rs3.getString("REMARKS"));	//備考

                    String SCHEDULECD = rs3.getString("SCHEDULECD");

                    //センターの評価
                    if(judgeDatas[2] == null || judgeDatas[2].trim().equals("")) {
                        I15HyoukaListBean possibilityData = new I15HyoukaListBean();
                        possibilityData.setStrHyoukaCenter("1");	//評価区分
                        possibilityData.setStrMshmei(judgeDatas[2]);		//模試名
                        possibilityData.setStrMshDate(null);		//模試実施基準日
                        possibilityData.setStrHyouka(null);		//評価
                        calenderData.getI15HyoukaList().add(possibilityData);
                    } else {
                        I15HyoukaListBean possibilityData = new I15HyoukaListBean();
                        //set
                        possibilityData.setStrHyoukaCenter("1");	//評価区分
                        possibilityData.setStrMshmei(judgeDatas[2]);		//模試名
                        possibilityData.setStrMshDate(judgeDatas[4]);		//模試実施基準日
                        possibilityData.setStrHyouka(judgeDatas[0]);		//評価
                        //add
                        calenderData.getI15HyoukaList().add(possibilityData);
                    }

                    //２次の評価
                    if(judgeDatas[3] == null || judgeDatas[3].trim().equals("")) {
                        I15HyoukaListBean possibilityData = new I15HyoukaListBean();
                        possibilityData.setStrHyoukaCenter("2");	//評価区分
                        possibilityData.setStrMshmei(judgeDatas[3]);		//模試名
                        possibilityData.setStrMshDate(null);		//模試実施基準日
                        possibilityData.setStrHyouka(null);		//評価
                        calenderData.getI15HyoukaList().add(possibilityData);
                    } else {
                        I15HyoukaListBean possibilityData = new I15HyoukaListBean();
                        //set
                        possibilityData.setStrHyoukaCenter("2");	//評価区分
                        possibilityData.setStrMshmei(judgeDatas[3]);		//模試名
                        possibilityData.setStrMshDate(judgeDatas[5]);		//模試実施基準日
                        possibilityData.setStrHyouka(judgeDatas[1]);		//評価
                        //add
                        calenderData.getI15HyoukaList().add(possibilityData);
                    }

                    //その人が受けた模試
                    ps6 = conn.prepareStatement(candidateSql());
                    for (java.util.Iterator itr = examList.iterator(); itr.hasNext();) {
                        //[0]:模試年度、[1]:模試コード
                        String[] exam = new String[4];
                        exam = CollectionUtil.deconcatComma((String)itr.next());

                        ps6.setString(1, printData.getIndividualid());
                        ps6.setString(2, exam[0]);	//模試年度
                        ps6.setString(3, exam[1]);	//模試コード
                        ps6.setString(4, rs3.getString("UNIVCD"));
                        ps6.setString(5, rs3.getString("FACULTYCD"));
                        ps6.setString(6, rs3.getString("DEPTCD"));
                        rs6 = ps6.executeQuery();

                        //create
                        I15HanteiRirekiListBean historyData = new I15HanteiRirekiListBean();
                        //set
                        if(rs6.next()) {
                            historyData.setStrMshmei(rs6.getString("EXAMNAME_ABBR"));	//模試名
                            historyData.setStrMshDate(rs6.getString("INPLEDATE"));		//模試実施基準日
                            //マーク模試で日程コードが以下のときは、センター評価をセット
                            if(GeneralUtil.toBlank(rs6.getString("EXAMTYPECD")).equals("01") && (!GeneralUtil.toBlank(SCHEDULECD).equals("C") && !GeneralUtil.toBlank(SCHEDULECD).equals("D")
                            && !GeneralUtil.toBlank(SCHEDULECD).equals("E") && !GeneralUtil.toBlank(SCHEDULECD).equals("1"))){
                                historyData.setStrHyouka(rs6.getString("SECONDRATING"));
                            }else{
                                if(GeneralUtil.toBlank(rs6.getString("EXAMTYPECD")).equals("01") && (GeneralUtil.toBlank(SCHEDULECD).equals("C") || GeneralUtil.toBlank(SCHEDULECD).equals("D")
                                || GeneralUtil.toBlank(SCHEDULECD).equals("E") || GeneralUtil.toBlank(SCHEDULECD).equals("1"))){
                                    historyData.setStrHyouka(rs6.getString("CENTERRATING"));
                                }else{
                                    historyData.setStrHyouka(rs6.getString("SECONDRATING"));
                                }
                            }
                        } else {
                            //結果無い
                            historyData.setStrMshmei(exam[2]);	//模試名
                            historyData.setStrMshDate(exam[3]);	//模試実施基準日
                            historyData.setStrHyouka(null);	//評価
                        }
                        if(rs6 != null) {rs6.close();}

                        //add
                        calenderData.getI15HanteiRirekiList().add(historyData);
                    }
                    if(ps6 != null) {ps6.close();}
                    //add
                    schedule.getI15CalendarList().add(calenderData);
                }
                if(ps3 != null) {ps3.close();}
                if(rs3 != null) {rs3.close();}
                if(ps4 != null) {ps4.close();}

                //スケジュールadd
                printData.getI15ScheduleList().add(schedule);
            } finally {
                if (ps1 != null) {ps1.close();}
                if (ps2 != null) {ps2.close();}
                if (ps3 != null) {ps3.close();}
                if (ps4 != null) {ps4.close();}
                if (ps5 != null) {ps5.close();}
                if (ps6 != null) {ps6.close();}
                if (rs1 != null) {rs1.close();}
                if (rs2 != null) {rs2.close();}
                if (rs3 != null) {rs3.close();}
                if (rs4 != null) {rs4.close();}
                if (rs5 != null) {rs5.close();}
                if (rs6 != null) {rs6.close();}
            }
        }
    }

    //最新の大学マスタ（19-1-1）
    private String latestUnivData() {
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("SELECT ");
        strBuf.append("EVENTYEAR EXAMYEAR, ");
        strBuf.append("MAX(EXAMDIV) EXAMDIV ");
        strBuf.append("FROM ");
        strBuf.append("UNIVMASTER_BASIC ");
        strBuf.append("WHERE ");
        strBuf.append("EVENTYEAR = (SELECT MAX(EVENTYEAR) FROM UNIVMASTER_BASIC) ");
        strBuf.append("GROUP BY ");
        strBuf.append("EVENTYEAR ");
        return strBuf.toString();
    }

    //模試
    private String examDataSql() {

        // ヘルプデスクは内部データ開放日
        final String DATAOPENDATE;
        if(login.isHelpDesk()){
            DATAOPENDATE = "IN_DATAOPENDATE";
        } else {
            DATAOPENDATE = "OUT_DATAOPENDATE";
        }

        final StringBuffer strBuf = new StringBuffer();
        strBuf.append("SELECT ");
        strBuf.append(" /*+ INDEX(SI PK_SUBRECORD_I) */ ");//12/5 sql hint
        strBuf.append("SI.EXAMYEAR EXAMYEAR, ");
        strBuf.append("SI.EXAMCD EXAMCD, ");
        strBuf.append("EX.EXAMNAME_ABBR EXAMNAME_ABBR, ");
        strBuf.append("EX.INPLEDATE INPLEDATE ");
        strBuf.append("FROM ");

        strBuf.append("EXAMINATION EX ");

        strBuf.append("INNER JOIN ");
        strBuf.append("SUBRECORD_I SI ");
        strBuf.append("ON EX.EXAMYEAR = SI.EXAMYEAR ");
        strBuf.append("AND EX.EXAMCD = SI.EXAMCD ");
        strBuf.append("AND SI.SUBCD = SI.SUBCD ");
        strBuf.append("AND EX.EXAMTYPECD IN ('01','02','03') ");
        strBuf.append("AND EX.EXAMCD <> '38' ");
        strBuf.append("AND EX.").append(DATAOPENDATE).append(" ");
        strBuf.append("<= TO_CHAR(SYSDATE, 'YYYYMMDD') || '0000' ");

        strBuf.append("WHERE ");
        strBuf.append("SI.INDIVIDUALID = ? ");
        strBuf.append("AND SI.EXAMYEAR = ? ");
        strBuf.append("GROUP BY ");
        strBuf.append("SI.EXAMYEAR, ");
        strBuf.append("SI.EXAMCD, ");
        strBuf.append("EX.EXAMNAME_ABBR, ");
        strBuf.append("EX.INPLEDATE ");
        strBuf.append("ORDER BY ");
        strBuf.append("INPLEDATE DESC, EXAMCD DESC");

        return strBuf.toString();
    }

    //偏差値
    private String deviationDataSql() {
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("SELECT ");
        strBuf.append(" /*+ INDEX(SI PK_SUBRECORD_I) */ ");//12/5 sql hint
        strBuf.append("C.COURSECD, ");
        strBuf.append("C.COURSENAME, ");
        strBuf.append("ES.SUBADDRNAME, ");
        strBuf.append("ES.DISPSEQUENCE, ");
        strBuf.append("SI.SCORE, ");
        strBuf.append("SI.A_DEVIATION, ");
        strBuf.append("SI.ACADEMICLEVEL, ");
        strBuf.append("1 SORT_NO, ");
        strBuf.append("ES.DISPSEQUENCE DISP, ");
        strBuf.append("SI.SCOPE, ");
        strBuf.append("NULL BASICFLG ");

        strBuf.append("FROM ");
        //strBuf.append("EXAMSUBJECT ES ");//[3] delete
        strBuf.append("V_I_CMEXAMSUBJECT ES ");//[3] add

        strBuf.append("LEFT JOIN ");
        strBuf.append("SUBRECORD_I SI ");
        strBuf.append("ON ES.SUBCD = SI.SUBCD ");
        strBuf.append("AND ES.EXAMYEAR = SI.EXAMYEAR ");
        strBuf.append("AND ES.EXAMCD = SI.EXAMCD ");

        strBuf.append("INNER JOIN ");
        strBuf.append("COURSE C ");
        strBuf.append("ON C.YEAR = SI.EXAMYEAR ");
        strBuf.append("AND C.COURSECD = (SUBSTR(SI.SUBCD, 0, 1) || '000') ");

        strBuf.append("INNER JOIN ");
        strBuf.append("EXAMINATION EX ");
        strBuf.append("ON EX.EXAMYEAR = SI.EXAMYEAR ");
        strBuf.append("AND EX.EXAMCD = SI.EXAMCD ");

        strBuf.append("WHERE ");
        strBuf.append("SI.INDIVIDUALID = ? ");
        strBuf.append("AND SI.EXAMYEAR = ? ");
        strBuf.append("AND SI.EXAMCD = ? ");
        strBuf.append("and si.subcd >= '7000' ");
        strBuf.append("and si.subcd < '9000' ");

        strBuf.append("UNION ALL ");

        strBuf.append("SELECT ");
        strBuf.append(" /*+ INDEX(SI PK_SUBRECORD_I) */ ");//12/5 sql hint
        strBuf.append("C.COURSECD, ");
        strBuf.append("C.COURSENAME, ");
        strBuf.append("ES.SUBADDRNAME, ");
        strBuf.append("ES.DISPSEQUENCE, ");
        strBuf.append("SI.SCORE, ");
        strBuf.append("SI.A_DEVIATION, ");
        strBuf.append("SI.ACADEMICLEVEL, ");
        strBuf.append("2 SORT_NO, ");
        strBuf.append("CASE WHEN SI.EXAMCD IN ('01','02','03','04','38') THEN ");
        strBuf.append("    CASE WHEN SI.SCOPE=9 OR SI.SCOPE IS NULL THEN ES.DISPSEQUENCE ");
        strBuf.append("    ELSE CASE SUBSTR(SI.SUBCD,1,1) WHEN '4' THEN 4000 ");
        strBuf.append("                                   WHEN '5' THEN 5000 ");
        strBuf.append("         ELSE ES.DISPSEQUENCE ");
        strBuf.append("         END ");
        strBuf.append("    END ");
        strBuf.append("ELSE ES.DISPSEQUENCE ");
        strBuf.append("END DISP, ");
        strBuf.append("SI.SCOPE, ");
        strBuf.append("GETCHARFLAGVALUE(ES.NOTDISPLAYFLG, 5) BASICFLG ");

        strBuf.append("FROM ");
        strBuf.append("EXAMSUBJECT ES ");

        strBuf.append("LEFT JOIN ");
        strBuf.append("SUBRECORD_I SI ");
        strBuf.append("ON ES.SUBCD = SI.SUBCD ");
        strBuf.append("AND ES.EXAMYEAR = SI.EXAMYEAR ");
        strBuf.append("AND ES.EXAMCD = SI.EXAMCD ");

        strBuf.append("INNER JOIN ");
        strBuf.append("COURSE C ");
        strBuf.append("ON C.YEAR = SI.EXAMYEAR ");
        strBuf.append("AND C.COURSECD = (SUBSTR(SI.SUBCD, 0, 1) || '000') ");
        strBuf.append("AND C.COURSECD <= '5000' ");

        strBuf.append("INNER JOIN EXAMINATION EX ");
        strBuf.append("ON EX.EXAMYEAR = SI.EXAMYEAR ");
        strBuf.append("AND EX.EXAMCD = SI.EXAMCD ");

        strBuf.append("WHERE ");
        strBuf.append("SI.INDIVIDUALID = ? ");
        strBuf.append("AND SI.EXAMYEAR = ? ");
        strBuf.append("AND SI.EXAMCD = ? ");
        strBuf.append("AND SI.SUBCD < '6000' ");
        strBuf.append("AND GETCHARFLAGVALUE(ES.NOTDISPLAYFLG, 3) = '0' ");

        strBuf.append("ORDER BY ");
        strBuf.append("SORT_NO ASC, ");
        strBuf.append("COURSECD ASC, ");
        strBuf.append("BASICFLG ASC, ");
        strBuf.append("DISP ASC, ");
        strBuf.append("SCOPE DESC ");
        return strBuf.toString();
    }

    //複数パターン
    private String patternSql() {
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("SELECT ");
        strBuf.append("HP.UNIVCD, ");
        strBuf.append("HP.FACULTYCD, ");
        strBuf.append("HP.DEPTCD, ");
        strBuf.append("COUNT(HP.UNICDBRANCHCD) BRANCHCOUNT ");
        strBuf.append("FROM ");
        strBuf.append("UNIVMASTER_CHOICESCHOOL HP ");
        strBuf.append("WHERE ");
        strBuf.append("HP.EVENTYEAR = ? ");
        strBuf.append("AND HP.EXAMDIV = ? ");
        strBuf.append("AND HP.ENTEXAMDIV = '2' ");
        strBuf.append("AND HP.UNIVCD = ? ");
        strBuf.append("AND HP.FACULTYCD = ? ");
        strBuf.append("AND HP.DEPTCD = ? ");
        strBuf.append("GROUP BY ");
        strBuf.append("HP.UNIVCD, ");
        strBuf.append("HP.FACULTYCD, ");
        strBuf.append("HP.DEPTCD ");
        return strBuf.toString();
    }

    //
    private String candidateSql() {
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("SELECT ");
        strBuf.append(" /*+ INDEX(CR PK_CANDIDATERATING) */ ");//12/5 sql hint
        strBuf.append("EX.EXAMNAME_ABBR, ");
        strBuf.append("EX.INPLEDATE, ");
        strBuf.append("EX.EXAMTYPECD, ");
        strBuf.append("CR.CENTERRATING, ");
        strBuf.append("CR.SECONDRATING ");
        strBuf.append("FROM ");
        strBuf.append("EXAMINATION EX ");
        strBuf.append("INNER JOIN ");
        strBuf.append("CANDIDATERATING CR ");
        strBuf.append("ON EX.EXAMYEAR = CR.EXAMYEAR ");
        strBuf.append("AND EX.EXAMCD = CR.EXAMCD ");
        strBuf.append("WHERE ");
        strBuf.append("CR.INDIVIDUALID = ? ");
        strBuf.append("AND CR.EXAMYEAR = ? ");
        strBuf.append("AND CR.EXAMCD = ? ");
        strBuf.append("AND CR.UNIVCD = ? ");
        strBuf.append("AND CR.FACULTYCD = ? ");
        strBuf.append("AND CR.DEPTCD = ? ");
        return strBuf.toString();
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
     */
    protected String getCategory() {
        return IProfileCategory.I_SCORE_QUE;
    }

}

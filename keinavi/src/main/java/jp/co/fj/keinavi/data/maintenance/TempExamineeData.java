package jp.co.fj.keinavi.data.maintenance;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

/**
 *
 * 仮受験者データ
 * 
 * 2006.10.03	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class TempExamineeData implements Serializable {

	// key ... 模試年度＋模試コード
	// value ... 仮受験者個人IDマップ
	private final Map tempExamineeMap = new HashMap();

	/**
	 * @return tempExaminee を戻します。
	 */
	public Map getTempExamineeMap() {
		return tempExamineeMap;
	}

	/**
	 * @param examId 対象模試
	 * @param individualId 個人ID
	 * @return 対象模試の仮受験者であるか
	 */
	public boolean isTempExaminee(final String examId,
			final String individualId) {
		final Map map = (Map) tempExamineeMap.get(examId);
		if (map == null) {
			return false;
		} else {
			return map.containsKey(individualId);
		}
	}
	
	/**
	 * @param p1 仮受験者
	 * @param p2 （画面内の）登録済み仮受験者
	 */
	public void setTempExaminee(final String[] p1,
			final String[] p2) {
		
		// 両方空なら処理しない
		if (p1.length == 0 && p2.length == 0) {
			return;
		}
		
		final List tempExaminee = Arrays.asList(p1);
		final List registedTempExaminee = Arrays.asList(p2);

		// 登録
		for (Iterator ite = tempExaminee.iterator(); ite.hasNext();) {
			final String value = (String) ite.next();
			// 模試年度＋模試コード
			final String examId = value.substring(0, 6);
			// 個人ID
			final String individualId = value.substring(7);

			Map map = (Map) tempExamineeMap.get(examId);
			if (map == null) {
				map = new HashMap();
				tempExamineeMap.put(examId, map);
			}
			map.put(individualId, Boolean.TRUE);
		}
		
		// 削除
		final List deleteList = (List) CollectionUtils.subtract(
				registedTempExaminee, tempExaminee);
		for (Iterator ite = deleteList.iterator(); ite.hasNext();) {
			final String value = (String) ite.next();
			// 模試年度＋模試コード
			final String examId = value.substring(0, 6);
			// 個人ID
			final String individualId = value.substring(7);
			
			final Map map = (Map) tempExamineeMap.get(examId);
			if (map == null) {
				continue;
			}
			map.remove(individualId);
		}
	}
	
}

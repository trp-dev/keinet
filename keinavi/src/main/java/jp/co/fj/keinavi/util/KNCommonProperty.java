package jp.co.fj.keinavi.util;


/**
 * 共通プロパティファイルから値を取得する。
 *
 * 2005.06.13	Yoshimoto KAWAI - TOTEC
 * 				インターネット模試判定など、
 * 				他のプロジェクトでも拡張可能なように
 * 				初期化メソッドなどの可視性をゆるくした
 *
 * 2010.04.06	Fujito URAKAWA
 * 				REST認証対応
 * 				職員認証用の設定を追加
 *
 * <2010年度マーク高２模試対応>
 * 2011.01.24	Tomohisa YAMADA
 * 				アップロード機能
 *
 * 2013.02.05	Tomohisa Yamada - TOTEC
 * 				Kei-Navi答案閲覧システムリンク改修
 *
 * @Date	2005/06/17
 * @author	TOTEC)Nishiyama
 */
public class KNCommonProperty {
	/**
	 * KNPropertyクラスのインスタンス
	 */
	private static KNProperty ComProp;

	/**
	 * 文字列定義
	 */

	private static final String KNRootPath 				= "KNRootPath";

	private static final String NDBDriver					= "NDBDriver";
	private static final String NDBSID 					= "NDBSID";
	private static final String NDBURL 					= "NDBURL";
	private static final String NDBConnUserID 			= "NDBConnUserID";
	private static final String NDBConnUserPass 	    	= "NDBConnUserPass";
	private static final String NDBPoolCountLimit   		= "NDBPoolCountLimit";

	private static final String FDBDriver					= "FDBDriver";
	private static final String FDBSID 					= "FDBSID";
	private static final String FDBURL 					= "FDBURL";
	private static final String FDBConnUserID 			= "FDBConnUserID";
	private static final String FDBConnUserPass 	    	= "FDBConnUserPass";
	private static final String FDBPoolCountLimit   		= "FDBPoolCountLimit";

	private static final String KNLogRootPath 			= "KNLogRootPath";
	private static final String AccLogFileSizeLimit 		= "AccLogFileSizeLimit";
	private static final String AccLogCountLimit 			= "AccLogCountLimit";
	private static final String ErrLogFileSizeLimit 		= "ErrLogFileSizeLimit";
	private static final String ErrLogCountLimit 			= "ErrLogCountLimit";
	private static final String DebugLogFileSizeLimit 	= "DebugLogFileSizeLimit";
	private static final String DebugLogCountLimit 		= "DebugLogCountLimit";
	private static final String DebugLogLevel 			= "DebugLogLevel";

	private static final String KNDataPath   				= "KNDataPath";
	private static final String KNTempPath   				= "KNTempPath";
	private static final String NewHelpLimit   			= "NewHelpLimit";
	private static final String HelpCharLimit   			= "HelpCharLimit";
	private static final String HelpCommentAddr   		= "HelpCommentAddr";
	private static final String InquireAddr   			= "InquireAddr";
	private static final String InfoDisplayCountLimit   	= "InfoDisplayCountLimit";
	private static final String NoteInfoDisplayCountLimit	= "NoteInfoDisplayCountLimit";
	private static final String TopInfoDisplayCountLimit	= "TopInfoDisplayCountLimit";
	private static final String OnePointCountLimt   		= "OnePointCountLimt";
	private static final String AdminPassWord   		    = "AdminPassWord";
	private static final String RestUrl      		    	= "RestUrl";
	private static final String SMTPServer				= "SMTPServer";
	private static final String DLSDataPath				= "DLSDataPath";
	private static final String DLSPOBDataPath			= "DLSPOBDataPath";
	private static final String TrialUserID				= "TrialUserID";
	private static final String CertsServer				= "CertsServer";
	private static final String CertsCsvPath				= "CertsCsvPath";
	private static final String CertsP12Path				= "CertsP12Path";
	private static final String CertsFilePath				= "CertsFilePath";
	private static final String ConnectionWaitTime		= "ConnectionWaitTime";
	private static final String PrivacyProtectionSchool	= "PrivacyProtectionSchool";
	private static final String ExamScoreDLPath			= "ExamScoreDLPath";
	private static final String CRExamScoreDLPath			= "CRExamScoreDLPath";
	private static final String EnableExamResultsResearch	= "EnableExamResultsResearch";
	private static final String RealTimeSessionMode		= "RealTimeSessionMode";
	private static final String ShidaiNitteiUrl			= "ShidaiNitteiUrl";
	private static final String ServiceName				= "ServiceName";
	private static final String RecountThreadSize			= "RecountThreadSize";
	private static final String MoshiURL					= "MoshiURL";
	private static final String KWEB_URL					= "K-WebURL";
	private static final String KWEBEXP_URL					= "K-Web-ExpURL";
	// 2019/09/12 QQ)Ooseto 共通テスト対応 ADD START
	private static final String AbilityDLPath                    = "AbilityDLPath";
	private static final String CRAbilityDLPath                  = "CRAbilityDLPath";
	// 2019/09/12 QQ)Ooseto 共通テスト対応 ADD END


	/**
	 * KNPropertyクラスを初期化する。
	 *
	 * @throws Exception
	 */
	protected static void init() throws Exception {
	    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
//		if(ComProp == null) {
	    ComProp = KNProperty.getInstance(KNPropertyCtrl.FilePath + "/common.properties");
//		}
	    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
	}

	/**
	 * ダウンロードサービス用データファイルディレクトリ
	 * @return
	 * @throws Exception
	 */
	public static String getDLSDataPath() throws Exception {
		init();
		return ComProp.getStrProperty(DLSDataPath);
	}

	/**
	 * ダウンロードサービス用高校別（旧私書箱）データファイルディレクトリ
	 * @return
	 * @throws Exception
	 */
	public static String getDLSPOBDataPath() throws Exception {
		init();
		return ComProp.getStrProperty(DLSPOBDataPath);
	}

	/**
	 * Kei-Navi体験版用のユーザID
	 * @return
	 * @throws Exception
	 */
	public static String getTrialUserID() throws Exception {
		init();
		return ComProp.getStrProperty(TrialUserID);
	}

	/**
	 * SMTPServer名を取得する
	 * @return SMTPサーバー名
	 * @throws Exception
	 */
	public static String getSMTPServer() throws Exception{
		init();
		return ComProp.getStrProperty(SMTPServer);
	}

	/**
	 * システムホームディレクトリを取得する。
	 *
	 * @return システムホームディレクトリ文字列
	 * @throws Exception
	 */
	public static String getKNRootPath() throws Exception {
		init();
		return ComProp.getStrProperty(KNCommonProperty.KNRootPath);
	}

	/**
	 * 通常メニュー用のDBドライバ名を取得する。
	 * @return DBドライバ名
	 * @throws Exception
	 */
	public static String getNDBDriver() throws Exception {
		init();
		return ComProp.getStrProperty(NDBDriver);
	}

	/**
	 * 通常メニュー用のデータベースSIDを取得する。
	 * @return データベースSID
	 * @throws Exception
	 */
	public static String getNDBSID() throws Exception {
		init();
		return ComProp.getStrProperty(NDBSID);
	}

	/**
	 * 通常メニュー用のDB接続URLを取得する。
	 * @return DB接続URL
	 * @throws Exception
	 */
	public static String getNDBURL() throws Exception {
		init();
		return ComProp.getStrProperty(NDBURL);
	}

	/**
	 * 通常メニュー用のDB接続ユーザIDを取得する。
	 * @return DB接続ユーザID
	 * @throws Exception
	 */
	public static String getNDBConnUserID() throws Exception {
		init();
		return ComProp.getStrProperty(NDBConnUserID);
	}

	/**
	 * 通常メニュー用のDB接続パスワードを取得する。
	 * @return DB接続パスワード
	 * @throws Exception
	 */
	public static String getNDBConnUserPass() throws Exception {
		init();
		return ComProp.getStrProperty(NDBConnUserPass);
	}

	/**
	 * 通常メニュー用のDBセッションプール数を取得する。
	 * @return DBセッションプール数
	 * @throws Exception
	 */
	public static int getNDBPoolCountLimit() throws Exception {
		init();
		return ComProp.getIntProperty(NDBPoolCountLimit);
	}

	/**
	 * 無料メニュー用のDBドライバ名を取得する。
	 * @return DBドライバ名
	 * @throws Exception
	 */
	public static String getFDBDriver() throws Exception {
		init();
		return ComProp.getStrProperty(FDBDriver);
	}

	/**
	 * 無料メニュー用のデータベースSIDを取得する。
	 * @return データベースSID
	 * @throws Exception
	 */
	public static String getFDBSID() throws Exception {
		init();
		return ComProp.getStrProperty(FDBSID);
	}

	/**
	 * 無料メニュー用のDB接続URLを取得する。
	 * @return DB接続URL
	 * @throws Exception
	 */
	public static String getFDBURL() throws Exception {
		init();
		return ComProp.getStrProperty(FDBURL);
	}

	/**
	 * 無料メニュー用のDB接続ユーザIDを取得する。
	 * @return DB接続ユーザID
	 * @throws Exception
	 */
	public static String getFDBConnUserID() throws Exception {
		init();
		return ComProp.getStrProperty(FDBConnUserID);
	}

	/**
	 * 無料メニュー用のDB接続パスワードを取得する。
	 * @return DB接続パスワード
	 * @throws Exception
	 */
	public static String getFDBConnUserPass() throws Exception {
		init();
		return ComProp.getStrProperty(FDBConnUserPass);
	}

	/**
	 * 無料メニュー用のDBセッションプール数を取得する。
	 * @return DBセッションプール数
	 * @throws Exception
	 */
	public static int getFDBPoolCountLimit() throws Exception {
		init();
		return ComProp.getIntProperty(FDBPoolCountLimit);
	}

	/**
	 * KNLogのルートパスを取得する。
	 *
	 * @return KNLogのルートパス文字列
	 * @throws Exception
	 */
	public static String getKNLogRootPath() throws Exception {
		init();
		return ComProp.getStrProperty(KNCommonProperty.KNLogRootPath);
	}

	/**
	 * アクセスログのファイルサイズの限界値を取得する。
	 *
	 * @return アクセスログのファイルサイズ限界値
	 * @throws Exception
	 */
	public static int getAccLogFileSizeLimit() throws Exception {
		init();
		return ComProp.getIntProperty(KNCommonProperty.AccLogFileSizeLimit);
	}

	/**
	 * アクセスログのログファイル数の限界値を取得する。
	 *
	 * @return アクセスログのログファイル数の限界値
	 * @throws Exception
	 */
	public static int getAccLogCountLimit() throws Exception {
		init();
		return ComProp.getIntProperty(KNCommonProperty.AccLogCountLimit);
	}

	/**
	 * エラーログのファイルサイズの限界値を取得する。
	 *
	 * @return エラーログのファイルサイズの限界値
	 * @throws Exception
	 */
	public static int getErrLogFileSizeLimit() throws Exception {
		init();
		return ComProp.getIntProperty(KNCommonProperty.ErrLogFileSizeLimit);
	}

	/**
	 * エラーログのログファイル数の限界値を取得する。
	 *
	 * @return エラーログのログファイル数の限界値
	 * @throws Exception
	 */
	public static int getErrLogCountLimit() throws Exception {
		init();
		return ComProp.getIntProperty(KNCommonProperty.ErrLogCountLimit);
	}

	/**
	 * デバッグログのファイルサイズの限界値を取得する。
	 *
	 * @return デバッグログのファイルサイズの限界値
	 * @throws Exception
	 */
	public static int getDebugLogFileSizeLimit() throws Exception {
		init();
		return ComProp.getIntProperty(KNCommonProperty.DebugLogFileSizeLimit);
	}

	/**
	 * デバッグログのログファイル数の限界値を取得する。
	 *
	 * @return デバッグログのファイル数の限界値
	 * @throws Exception
	 */
	public static int getDebugLogCountLimit() throws Exception {
		init();
		return ComProp.getIntProperty(KNCommonProperty.DebugLogCountLimit);
	}

	/**
	 * デバッグログの出力ログレベルを取得する。
	 *
	 * @return デバッグログの出力レベル
	 * @throws Exception
	 */
	public static int getDebugLogLevel() throws Exception {
		init();
		return ComProp.getIntProperty(KNCommonProperty.DebugLogLevel);
	}

	/**
	 * データファイルディレクトリを取得する。
	 *
	 * @return データファイルディレクトリ
	 * @throws Exception
	 */
	public static String getKNDataPath() throws Exception {
		init();
		return ComProp.getStrProperty(KNCommonProperty.KNDataPath);
	}

	/**
	 * 作業ディレクトリを取得する。
	 *
	 * @return 作業ディレクトリ
	 * @throws Exception
	 */
	public static String getKNTempPath() throws Exception {
		init();
		return ComProp.getStrProperty(KNCommonProperty.KNTempPath);
	}

	/**
	 * ヘルプ 新着マーク表示日数を取得する。
	 *
	 * @return ヘルプ 新着マーク表示日数
	 * @throws Exception
	 */
	public static int getNewHelpLimit() throws Exception {
		init();
		return ComProp.getIntProperty(KNCommonProperty.NewHelpLimit);
	}

	/**
	 * ヘルプ 検索結果文字数を取得する。
	 *
	 * @return ヘルプ 検索結果文字数
	 * @throws Exception
	 */
	public static int getHelpCharLimit() throws Exception {
		init();
		return ComProp.getIntProperty(KNCommonProperty.HelpCharLimit);
	}

	/**
	 * ヘルプコメント 送信先メールアドレスを取得する。
	 *
	 * @return ヘルプコメント 送信先メールアドレス
	 * @throws Exception
	 */
	public static String getHelpCommentAddr() throws Exception {
		init();
		return ComProp.getStrProperty(KNCommonProperty.HelpCommentAddr);
	}

	/**
	 * お問い合わせ 送信先メールアドレスを取得する。
	 *
	 * @return お問い合わせ 送信先メールアドレス
	 * @throws Exception
	 */
	public static String getInquireAddr() throws Exception {
		init();
		return ComProp.getStrProperty(KNCommonProperty.InquireAddr);
	}


	/**
	 * お知らせ表示件数を取得する。
	 *
	 * @return お知らせ表示件数
	 * @throws Exception
	 */
	public static int getInfoDisplayCountLimit() throws Exception {
		init();
		return ComProp.getIntProperty(KNCommonProperty.InfoDisplayCountLimit);
	}

	/**
	 * Kei-Net注目情報表示件数を取得する。
	 *
	 * @return Kei-Net注目情報表示件数
	 * @throws Exception
	 */
	public static int getNoteInfoDisplayCountLimit() throws Exception {
		init();
		return ComProp.getIntProperty(KNCommonProperty.NoteInfoDisplayCountLimit);
	}

	/**
	 * トップ画面（ログイン画面）お知らせ情報表示件数を取得する。
	 *
	 * @return トップ画面（ログイン画面）お知らせ情報表示件数
	 * @throws Exception
	 */
	public static int getTopInfoDisplayCountLimit() throws Exception {
		init();
		return ComProp.getIntProperty(KNCommonProperty.TopInfoDisplayCountLimit);
	}

	/**
	 * ワンポイントアドバイス 表示件数を取得する。
	 *
	 * @return ワンポイントアドバイス 表示件数
	 * @throws Exception
	 */
	public static int getOnePointCountLimt() throws Exception {
		init();
		return ComProp.getIntProperty(KNCommonProperty.OnePointCountLimt);
	}

	/**
	 * メンテナンス用パスワードを取得する。
	 *
	 * @return メンテナンス用パスワード
	 * @throws Exception
	 */
	public static String getAdminPassWord() throws Exception {
		init();
		return ComProp.getStrProperty(KNCommonProperty.AdminPassWord);
	}

	/**
	 * REST認証URLを取得する。
	 *
	 * @return SOAP認証URL
	 * @throws Exception
	 */
	public static String getRestUrl() throws Exception {
		init();
		return ComProp.getStrProperty(KNCommonProperty.RestUrl);
	}

	/**
	 * AICAによる証明書発行を行うサーバ
	 *
	 * @return 証明書発行を行うサーバ
	 * @throws Exception
	 */
	public static String getCertsServer() throws Exception {
		init();
		return ComProp.getStrProperty(KNCommonProperty.CertsServer);
	}

	/**
	 * AICAによる証明書発行に使用するＣＳＶファイルの作成ディレクトリ
	 *
	 * @return ＣＳＶファイルの作成ディレクトリ
	 * @throws Exception
	 */
	public static String getCertsCsvPath() throws Exception {
		init();
		return ComProp.getStrProperty(KNCommonProperty.CertsCsvPath);
	}

	/**
	 * AICAにより作成される証明書ファイルの保存ディレクトリ
	 *
	 * @return 証明書ファイルの保存ディレクトリ
	 * @throws Exception
	 */
	public static String getCertsP12Path() throws Exception {
		init();
		return ComProp.getStrProperty(KNCommonProperty.CertsP12Path);
	}

	/**
	 * 作成された証明書ファイルの格納ディレクトリ（FTPによるダウンロード用）
	 *
	 * @return 証明書ファイルの格納ディレクトリ
	 * @throws Exception
	 */
	public static String getCertsFilePath() throws Exception {
		init();
		return ComProp.getStrProperty(KNCommonProperty.CertsFilePath);
	}

	/**
	 * @return コネクション取得ウェイト秒
	 * @throws Exception
	 */
	public static String getConnectionWaitTime() throws Exception {
		init();
		return ComProp.getStrProperty(KNCommonProperty.ConnectionWaitTime);
	}

	/**
	 * 設定値を取得する
	 * @param key
	 * @return
	 */
	protected static String getValue(String key) {
		try {
			init();
            String value = ComProp.getStrProperty(key);
            if (value == null) return "";
            else return value;
        } catch (Exception e) {
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
            //throw new InternalError(e.getMessage());
            throw new InternalError(e);
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
        }
	}

	/**
	 * @param id 設定値ID
	 * @return 指定したIDの設定値
	 */
	public static String getStringValue(final String id) {
		return getValue(id);
	}

	/**
	 * @param id 設定値ID
	 * @param defaultValue デフォルト値
	 * @return 指定したIDの設定値
	 */
	public static int getIntValue(final String id,
			final int defaultValue) {
		try {
			return Integer.parseInt(getValue(id));
		} catch (final NumberFormatException e) {
			return defaultValue;
		}
	}

	/**
	 * @return プライバシー保護の学校コード（カンマ区切り）
	 * @throws Exception
	 */
	public static String getPrivacyProtectionSchool() {
	    return getValue(KNCommonProperty.PrivacyProtectionSchool);
	}

	/**
	 * @return 模試成績データ格納パス
	 * @throws Exception
	 */
	public static String getExamScoreDLPath() {
	    return getValue(KNCommonProperty.ExamScoreDLPath);
	}

	/**
	 * @return 模試成績データ格納パス（センターリサーチ）
	 * @throws Exception
	 */
	public static String getCRExamScoreDLPath() {
	    return getValue(KNCommonProperty.CRExamScoreDLPath);
	}

	/**
	 * @return 入試結果調査の有効フラグ
	 * @throws Exception
	 */
	public static String getEnableExamResultsResearch() {
	    return getValue(KNCommonProperty.EnableExamResultsResearch);
	}

	/**
	 * @return 接続制限数の動作モード
	 * @throws Exception
	 */
	public static String getRealTimeSessionMode() {
	    return getValue(KNCommonProperty.RealTimeSessionMode);
	}

	/**
	 * @return 私大入試日程URL
	 */
	public static String getShidaiNitteiUrl() {
	    return getValue(KNCommonProperty.ShidaiNitteiUrl);
	}

	/**
	 * @return サービス名
	 */
	public static String getServiceName() {
	    return getValue(KNCommonProperty.ServiceName);
	}

	/**
	 * @return 再集計処理スレッド数名
	 */
	public static int getRecountThreadSize() {
	    return getIntValue(KNCommonProperty.RecountThreadSize, 3);
	}

	/**
	 * @return 模試受付システムURL
	 */
	public static String getMoshiURL() {
		return getValue(KNCommonProperty.MoshiURL);
	}

	/**
	 * @return K-Web用URL
	 */
	public static String getKWebURL() {
		return getValue(KNCommonProperty.KWEB_URL);
	}

	/**
	 * @return K-Web（体験版）用URL
	 */
	public static String getKWebExpURL() {
		return getValue(KNCommonProperty.KWEBEXP_URL);
	}

	/**
	 * @return SERVER_NAMER_FORMATTER
	 */
	public static String getUploadFileServerNamerFormatter() {
		return getValue("UPLOADFILE_SERVER_NAMER_FORMATTER");
	}

	/**
	 * @return JUKU_FILE_TYPE_ID
	 */
	public static String getUploadFileJukuFileTypeID() {
		return getValue("UPLOADFILE_JUKU_FILE_TYPE_ID");
	}

	/**
	 * @return JUKU_FILE_EXTENSION
	 */
	public static String getUploadFileJukuFileExtension() {
		return getValue("UPLOADFILE_JUKU_FILE_EXTENSION");
	}

	/**
	 * @return TEMPORARY_DIRECTORY_PATH
	 */
	public static String getUploadFileTemporaryDirectoryPath() {
		return getValue("UPLOADFILE_TEMPORARY_DIRECTORY_PATH");
	}

	/**
	 * @return UPLOADFILE_DOWNLOADBLE_DIRECTORY_PATH=
	 */
	public static String getUploadFileDownloadableDirectoryPath() {
		return getValue("UPLOADFILE_DOWNLOADBLE_DIRECTORY_PATH");
	}

	/**
	 * @return SERVER_DIRECTORY_PATH
	 */
	public static String getUploadFileServerDirectoryPath() {
		return getValue("UPLOADFILE_SERVER_DIRECTORY_PATH");
	}

	/**
	 * @return MONTHS_AFTER_DOWNLOAD
	 */
	public static int getUploadFileMonthsAfterDownload() {
		return getIntValue("UPLOADFILE_MONTHS_AFTER_DOWNLOAD", 12);
	}

	/**
	 * @return MONTHS_AFTER_HISTORY
	 */
	public static int getUploadFileMonthsAfterHistory() {
		return getIntValue("UPLOADFILE_MONTHS_AFTER_HISTORY", 3);
	}

	/**
	 * @return DAYS_AFTER_VIRUS
	 */
	public static int getUploadFileDaysAfterVirus() {
		return getIntValue("UPLOADFILE_DAYS_AFTER_VIRUS", 7);
	}

	// 2019/09/12 QQ)Ooseto 共通テスト対応 ADD START
	/**
	 * @return 学力要素別データ格納パス
         * @throws Exception
	 */
	public static String getAbilityDLPath() {
	    return getValue(KNCommonProperty.AbilityDLPath);
	}

	/**
	 * @return 学力要素別データ格納パス（センターリサーチ）
         * @throws Exception
	 */
	public static String getCRAbilityDLPath() {
	    return getValue(KNCommonProperty.CRAbilityDLPath);
	}
	// 2019/09/12 QQ)Ooseto 共通テスト対応 ADD END

}

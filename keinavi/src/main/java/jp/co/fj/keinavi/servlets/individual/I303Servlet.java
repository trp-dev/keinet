/*
 * 作成日: 2004/08/03
 *
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.individual.UnivSearchBean;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.NestedUnivData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I303Form;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNJpnStringConv;

import jp.co.fj.keinavi.servlets.KNServletException;

/**
 * @author Totec) T.Yamada
 * 
 * 2005.6.23 	T.Yamada 	[1]I203Servletの[1]と同じ処理を加える
 * 
 */
public class I303Servlet extends IndividualServlet{
	
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
			
		//フォームデータを取得
		I303Form i303Form = null;
		try {
			i303Form = (I303Form)super.getActionForm(request, "jp.co.fj.keinavi.forms.individual.I303Form");
		} catch (Exception e) {
			KNServletException k = new KNServletException("0I303にてフォームデータの収得に失敗しました。");
			k.setErrorCode("00I303010101");
			throw k;
		}
		
		HttpSession session = request.getSession();
		
		//プロファイル情報
		Profile profile = (Profile) session.getAttribute(Profile.SESSION_KEY);
		//個人共通
		ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);
		//模試データ
		ExamSession examSession = (ExamSession) session.getAttribute(ExamSession.SESSION_KEY);	

		if ("i303".equals(getForward(request))) {//転送:JSP
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);
				
				/** 大学情報の取得 */
				UnivSearchBean univSearchBean = new UnivSearchBean();

				if(!"i303".equals(getBackward(request))){
					//初回アクセス
					//初回用フォーム作成
					i303Form.setSearchMode("name");
					i303Form.setSearchStr("");
					
				}else{
					//二回目以降
					if(i303Form.getButton().trim().equals("search")){
						//検索ボタンが押された
						if(i303Form.getSearchMode().trim().equals("name")){
							//名前検索
							//univSearchBean.setHiraganaFull(KNJpnStringConv.hkana2Han(i303Form.getSearchStr().trim()));//[1] del
							univSearchBean.setHiraganaFull(KNJpnStringConv.hkana2Han(GeneralUtil.truncateString(i303Form.getSearchStr().trim(), 12)));//[1] del
							univSearchBean.setConnection("", con);
							univSearchBean.execute();
						}else{
							//コード検索
							univSearchBean.setUnivCd(i303Form.getSearchStr().trim());
							univSearchBean.setConnection("", con);
							univSearchBean.execute();
						}
					}else{
						//頭文字が押された
						univSearchBean.setInitialCharFull(KNJpnStringConv.hkana2Han(i303Form.getCharValue().trim()));
						univSearchBean.setConnection("", con);
						univSearchBean.execute();
					}
				}
				//大学・学部・学科ソートキー順にソート
				if(univSearchBean.getRecordSet() != null && univSearchBean.getRecordSet().size() > 0){
					
				}
				request.setAttribute("univSearchBean", univSearchBean);
			
			} catch (Exception e) {
				e.printStackTrace();
				KNServletException k = new KNServletException("0I303にて検索時エラーが発生しました。");
				k.setErrorCode("00I303020002");
				try {
					con.rollback();
				} catch (SQLException e1) {
					KNServletException k1 = new KNServletException("0I303にてコネクションローバックに失敗しました。");
					k1.setErrorCode("00I104010101");
					throw k1;
				}
				throw k;
			} finally {
				try {if (con != null) releaseConnectionPool(request, con);
				} catch (Exception e) {
					KNServletException k2 = new KNServletException("0I303にてコネクションの解放に失敗しました。");
					k2.setErrorCode("00I303010101");
					throw k2;
				}
			}

			request.setAttribute("form", i303Form);
		
			iCommonMap.setTargetPersonId(i303Form.getTargetPersonId());
			super.forward(request, response, JSP_I303);
			
		}else{//転送:SERVLET
			
			iCommonMap.setTargetPersonId(i303Form.getTargetPersonId());
			super.forward(request, response, SERVLET_DISPATCHER);
			
		}
	
	}
	private void testOutput(UnivSearchBean bean) throws Exception{
		
		List list = bean.getNestedUnivList();
		if(list != null){
			System.out.println("univListSize : "+list.size());
			for (java.util.Iterator it=list.iterator(); it.hasNext();) {
				NestedUnivData univData = (NestedUnivData)it.next();
				System.out.println(univData.getUnivCd());
				System.out.println(univData.getUnivNameAbbr());
				for (java.util.Iterator it2=univData.getFacultyList().iterator(); it2.hasNext();) {
					NestedUnivData.FacultyData facultyData = (NestedUnivData.FacultyData)it2.next();
					System.out.println(facultyData.getFacultyCd());
					System.out.println(facultyData.getFacultyNameAbbr());
					for (java.util.Iterator it3=facultyData.getDeptList().iterator(); it3.hasNext();) {
						NestedUnivData.FacultyData.DeptData deptData = (NestedUnivData.FacultyData.DeptData)it3.next();
						System.out.println(deptData.getDeptCd());
						System.out.println(deptData.getDeptNameAbbr());
						System.out.println(deptData.getSuperAbbrName());
					}
				}
			}
		}else{
			System.out.println("NestedUnivList is null");
		}
	}
}

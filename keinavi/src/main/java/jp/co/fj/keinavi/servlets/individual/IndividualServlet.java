package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.ExamComboData.YearData;
import jp.co.fj.keinavi.data.individual.ExamComboData.YearData.ExamData;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 * 個人成績分析基本サーブレット
 * 
 * 2004.07.27	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class IndividualServlet extends DefaultHttpServlet {
	
	// モード切替フラグキー
	private static final String KEY_MODE_CHANGED = "_KEY_MODE_CHANGED";
	// 対象模試変更フラグキー
	private static final String KEY_EXAM_CHANGED = "_KEY_EXAM_CHANGED";
	
	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
			final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException, IOException {
	
		final HttpSession session = request.getSession(false);
		final ICommonMap iCommonMap = (ICommonMap) session.getAttribute(
				ICommonMap.SESSION_KEY);
			
		// モード切替
		if ("1".equals(request.getParameter("changeMode"))) {
			iCommonMap.setMode(iCommonMap.isBunsekiMode() ? "1" : "0");
			iCommonMap.setTargetPersonId(request.getParameter("targetPersonId"));
			request.setAttribute(KEY_MODE_CHANGED, Boolean.TRUE);
		}
	}
	
	/**
	 * 対象模試情報の初期化を行う
	 * 
	 * @param request HTTPリクエスト
	 */
	protected void initTargetExam(final HttpServletRequest request) {

		final HttpSession session = request.getSession(false);
		final ICommonMap iCommonMap = (ICommonMap) session.getAttribute(
				ICommonMap.SESSION_KEY);
		final String targetExamYear = request.getParameter("targetExamYear");
		final String targetExamCode = request.getParameter("targetExamCode");
		
		// フォーム値とセッション値の比較
		if (!iCommonMap.getTargetExamYear().equals(targetExamYear)
			|| !iCommonMap.getTargetExamCode().equals(targetExamCode)) {
	
			// 対象模試を変更した
			request.setAttribute(KEY_EXAM_CHANGED, Boolean.TRUE);

			// 年度切替によってセッションに存在しない模試が
			// 選ばれることもある。その場合はその年度の最新模試
			if (iCommonMap.getExamComboData().getExamData(
					targetExamYear, targetExamCode) == null) {

				final YearData year = iCommonMap.getExamComboData(
						).get(targetExamYear);
				if (year != null) {
					ExamData data = (ExamData) year.getExamDatas().get(0);
					iCommonMap.setTargetExam(targetExamYear, data.getExamCode());
				}
				
			// セッションに存在する
			} else {
				iCommonMap.setTargetExam(targetExamYear, targetExamCode);
			}
		}
	}

	/**
	 * @param request HTTPリクエスト
	 * @return iCommonMap
	 */
	protected ICommonMap getICommonMap(final HttpServletRequest request) {
		return (ICommonMap) request.getSession(
				false).getAttribute(ICommonMap.SESSION_KEY);
	}
	
	/**
	 * @param request HTTPリクエスト
	 * @return isChangedExam を戻します。
	 */
	public boolean isChangedExam(final HttpServletRequest request) {
		return request.getAttribute(KEY_EXAM_CHANGED) != null;
	}

	/**
	 * @param request HTTPリクエスト
	 * @return isChangedMode を戻します。
	 */
	public boolean isChangedMode(final HttpServletRequest request) {
		return request.getAttribute(KEY_MODE_CHANGED) != null;
	}
	
}

package jp.co.fj.keinavi.beans.user;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 利用者削除Bean
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class LoginUserDeleteBean extends DefaultBean {

	// 学校コード
	private final String schoolCd;
	// 利用者ID
	private final String loginId;
	
	/**
	 * コンストラクタ
	 */
	public LoginUserDeleteBean(final String schoolCd, final String loginId) {
		
		if (schoolCd == null) {
			throw new IllegalArgumentException("学校コードがNULLです。");
		}
		
		if (loginId == null) {
			throw new IllegalArgumentException("利用者IDがNULLです。");
		}
		
		this.schoolCd = schoolCd;
		this.loginId = loginId;
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		deleteLoginIdManage();
		deleteLoginCharge();
	}

	private void deleteLoginIdManage() throws SQLException {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					"DELETE FROM loginid_manage m "
					+ "WHERE m.schoolcd = ? AND m.loginid = ? AND m.manage_flg <> '1'");
			
			// 学校コード
			ps.setString(1, schoolCd);
			// 利用者ID
			ps.setString(2, loginId);
			
			if (ps.executeUpdate() == 0) {
				throw new SQLException(
						"管理者は削除できません。学校ID=" + schoolCd + "/利用者ID=" + loginId);
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	private void deleteLoginCharge() throws SQLException {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					"DELETE FROM login_charge c "
					+ "WHERE c.schoolcd = ? AND c.loginid = ?");
			
			// 学校コード
			ps.setString(1, schoolCd);
			// 利用者ID
			ps.setString(2, loginId);
			
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

}

package jp.co.fj.keinavi.servlets.com_set;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.com_set.CompYearBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM301Form;
import jp.co.fj.keinavi.util.KNUtil;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM301Servlet extends AbstractCMServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 共通処理
		super.execute(request, response);

		// ログイン情報
		LoginSession login = getLoginSession(request);
		// プロファイル
		Profile profile = super.getProfile(request);
		// 共通アクションフォーム - session scope
		CM001Form scform = super.getCommonForm(request);
		// 個別アクションフォーム - session scope
		CM301Form siform = (CM301Form)scform.getActionForm("CM301Form");

		// JSP
		if ("cm301".equals(getForward(request))) {
			
			Connection con = null; // DBコネクション
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(false);

				// 対象年度Bean
				CompYearBean bean = new CompYearBean();
				bean.setConnection(null, con);
				bean.setTargetYear(scform.getTargetYear());
				bean.setTargetExam(scform.getTargetExam());
				bean.setBundleCd(getBundleCd(login, profile, scform.getTargetExam()));
				bean.setSchoolCd(login.getUserID());
				bean.execute();
				request.setAttribute("CompYearBean", bean);

				// 一時表クリア
				con.commit();
				
			} catch (final Exception e) {
				rollback(con);
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			// 現年度
			request.setAttribute("CurrentYear", KNUtil.getCurrentYear());
			
			super.forward(request, response, JSP_CM301);

		// 不明なら転送
		} else {
			// アクションフォームを取得
			CM001Form rcform = (CM001Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.com_set.CM001Form");
			CM301Form riform = (CM301Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.com_set.CM301Form");

			// なければ空の配列を入れておく
			if (riform.getCompYear() == null) siform.setCompYear(new String[0]);
			// ある場合 
			else siform.setCompYear(riform.getCompYear());

			// 変更フラグ
			scform.setChanged(rcform.getChanged());

			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

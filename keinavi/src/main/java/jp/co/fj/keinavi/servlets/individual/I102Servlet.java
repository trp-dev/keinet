/*
 * 作成日: 2004/06/28
 *
 * バランスチャート表示用サーブレット
 *
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.help.OnepointBean;
import jp.co.fj.keinavi.beans.individual.ExamSearchBean;
import jp.co.fj.keinavi.beans.individual.I102Bean;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I102Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * @author Totec) K.Kondo
 *
 * バランスチャートを表示する。
 * プロファイルからデータ取得するが、I103で再表示のときはフォームからデータ取得
 *
 * 2005.3.8 	Totec) K.Kondo      [1]change 現年度の年度条件を担当クラスの年度に変更
 * 2005.8.23 	Totec) T.Yamada 	[2]印刷での対象模試がセンターリサーチの時のチェックボックス無効時の対応
 *
 */
public class I102Servlet extends IndividualServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		final HttpServletRequest request,
		final HttpServletResponse response)
		throws ServletException, IOException{

		super.execute(request, response);

		//フォームデータの取得
		final I102Form i102Form = (I102Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.individual.I102Form");

		// ログイン情報
		final LoginSession login = getLoginSession(request);

		//プロファイル情報
		final Profile profile = getProfile(request);
		Short studentDispGrade = (Short)profile.getItemMap(IProfileCategory.I_SCORE_SHEET).get(IProfileItem.STUDENT_DISP_GRADE);
		Short examType = (Short)profile.getItemMap(IProfileCategory.I_SCORE_SHEET).get(IProfileItem.EXAM_TYPE);
		Short printStamp = (Short) profile.getItemMap(IProfileCategory.I_SCORE_SHEET).get(IProfileItem.PRINT_STAMP);
		Short printStudent = (Short) profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);

		//個人共通
		final ICommonMap iCommonMap = getICommonMap(request);
		//模試データ
		final ExamSession examSession = getExamSession(request);

		// 転送元がJSPなら対象模試情報を初期化
		if ("i102".equals(getBackward(request))) {
			initTargetExam(request);
		}

		// 転送：JSP
		if ("i102".equals(getForward(request))) {
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(true);

				//ワンポイントアドバイス
				OnepointBean onebean = new OnepointBean();
				onebean.setConnection(null, con);
				onebean.setScreenID("i102");
				onebean.execute();
				request.setAttribute("OnepointBean", onebean);

				//対象模試情報取得（大学マスタ区分の取得）
				ExamSearchBean esb = new ExamSearchBean();
				esb.setExamSession(examSession);
				esb.setExamYear(iCommonMap.getTargetExamYear());
				esb.setExamCd(iCommonMap.getTargetExamCode());
				esb.setConnection("", con);
				esb.execute();
				ExaminationData ed = (ExaminationData) esb.getRecordSet().get(0);

				//一覧取得
				I102Bean i102Bean = new I102Bean();
				i102Bean.setExamSession(examSession);
				i102Bean.setMasterDiv(ed.getMasterDiv());
				i102Bean.setClassYear(ProfileUtil.getChargeClassYear(profile));//[1] add

				if(!"i102".equals(getBackward(request))
						|| !iCommonMap.getTargetPersonId().equals(i102Form.getTargetPersonId())
						|| isChangedMode(request) || isChangedExam(request)){

					// 生徒を切り替えたとき
					// モードを切り替えたとき
					// 対象模試を変えたとき
					if ("i102".equals(getBackward(request))) {
						i102Bean.setStudentDispGrade(i102Form.getTargetGrade());
						i102Bean.setExamType(i102Form.getTargetExamType());
						i102Bean.setSearchCondition(i102Form.getTargetPersonId(), iCommonMap.getTargetExamYear(),
													iCommonMap.getTargetExamCode(),login.isHelpDesk());

						//1.印刷対象生徒をフォームにセット
						String[] printTarget = {"0","0","0","0"};
						if(i102Form.getPrintTarget() != null){
							for(int i=0; i<i102Form.getPrintTarget().length; i++){
								printTarget[Integer.parseInt(i102Form.getPrintTarget()[i])] = "1";
							}
						}
						//[2] add start
						final String CENTER_EXAMCD = "38";
						if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
							printTarget[3] = "1";
						}
						//[2] add end
						i102Form.setPrintTarget(printTarget);
						//フォーム作成
						i102Form.setTargetGrade(i102Bean.getStudentDispGrade());
						i102Form.setTargetExamType(i102Bean.getExamType());

					// 初回アクセス
					} else {
						i102Bean.setStudentDispGrade(studentDispGrade.toString());
						i102Bean.setExamType(examType.toString());
						i102Bean.setSearchCondition(i102Form.getTargetPersonId(), iCommonMap.getTargetExamYear(),
													iCommonMap.getTargetExamCode(),login.isHelpDesk());

						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
						//個人成績分析共通-印刷対象生保存をセット
						//i102Form.setPrintStudent(printStudent.toString());
						i102Form.setPrintStudent(iCommonMap.getPrintStudent());
						//1.印刷対象生徒をセット
						String printTarget = (String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET);
						i102Form.setPrintTarget(CollectionUtil.splitComma(printTarget));
						//4.セキュリティスタンプをセット
						//i102Form.setPrintStamp(printStamp.toString());
						i102Form.setPrintStamp(iCommonMap.getPrintStamp());
						//初回用フォーム作成
						//i102Form.setTargetGrade(i102Bean.getStudentDispGrade());
						i102Form.setTargetGrade(iCommonMap.getTargetGrade());
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
						i102Form.setTargetExamType(i102Bean.getExamType());
					}

				// 再表示
				} else {
					i102Bean.setSearchCondition(i102Form.getTargetPersonId(), iCommonMap.getTargetExamYear(),
												iCommonMap.getTargetExamCode(), i102Form.getTargetGrade(),
												i102Form.getTargetExamType(),login.isHelpDesk());

					//1.印刷対象グラフをフォームにセットしなおす
					String[] printTarget = {"0","0","0","0"};
					if(i102Form.getPrintTarget() != null){
						for(int i=0; i<i102Form.getPrintTarget().length; i++){
							printTarget[Integer.parseInt(i102Form.getPrintTarget()[i])] = "1";
						}
					}
					//[2] add start
					final String CENTER_EXAMCD = "38";
					if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
						printTarget[3] = "1";
					}
					//[2] add end
					i102Form.setPrintTarget(printTarget);

					/** フォームの値をプロファイルに保存　*/
					//2.表示する学年をセット
					profile.getItemMap(IProfileCategory.I_SCORE_SHEET).put(IProfileItem.STUDENT_DISP_GRADE, new Short(i102Form.getTargetGrade()));

					//3.模試の種類をセット
					profile.getItemMap(IProfileCategory.I_SCORE_SHEET).put(IProfileItem.EXAM_TYPE, new Short(i102Form.getTargetExamType()));
				}
				i102Bean.setConnection("", con);
				i102Bean.execute();

				request.setAttribute("i102Bean", i102Bean);

			} catch(final Exception e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			request.setAttribute("form", i102Form);

			iCommonMap.setTargetPersonId(i102Form.getTargetPersonId());
			super.forward(request, response, JSP_I102);

		//転送：Servlet
		} else {

			//帳票 forwordがsheetならプロファイルを保存する
			if ("sheet".equals(getForward(request))) {
				/** フォームの値をプロファイルに保存　*/
				//分析モードなら個人成績分析共通-印刷対象生保存をセット
				if(iCommonMap.isBunsekiMode()){
					profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(i102Form.getPrintStudent()));
				}

		        //1.印刷対象生徒をセット
		        String[] printTarget = {"0","0","0","0"};
		        if(i102Form.getPrintTarget() != null){
		           for(int i=0; i<i102Form.getPrintTarget().length; i++){
			           printTarget[Integer.parseInt(i102Form.getPrintTarget()[i])] = "1";
		           }
		        }
				//[2] add start
				final String CENTER_EXAMCD = "38";
				if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
					printTarget[3] = "1";
				}
				//[2] add end
			    profile.getItemMap(IProfileCategory.I_SCORE_COMMON).put(IProfileItem.PRINT_TARGET, CollectionUtil.deSplitComma(printTarget));
				//2.表示する学年をセット
				profile.getItemMap(IProfileCategory.I_SCORE_SHEET).put(IProfileItem.STUDENT_DISP_GRADE, new Short(i102Form.getTargetGrade()));
				//3.模試の種類をセット
				profile.getItemMap(IProfileCategory.I_SCORE_SHEET).put(IProfileItem.EXAM_TYPE, new Short(i102Form.getTargetExamType()));
				//4.セキュリティスタンプをセット
				profile.getItemMap(IProfileCategory.I_SCORE_SHEET).put(IProfileItem.PRINT_STAMP, new Short(i102Form.getPrintStamp()));

			//単なるページ遷移
			} else {
				//分析モードなら個人成績分析共通-印刷対象生保存をセット
				if(iCommonMap.isBunsekiMode()){
					profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(i102Form.getPrintStudent()));
				}
				//1.印刷対象生徒をセット
				String[] printTarget = {"0","0","0","0"};
				if(i102Form.getPrintTarget() != null){
				   for(int i=0; i<i102Form.getPrintTarget().length; i++){
					   printTarget[Integer.parseInt(i102Form.getPrintTarget()[i])] = "1";
				   }
				}
				//[2] add start
				final String CENTER_EXAMCD = "38";
				if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
					printTarget[3] = "1";
				}
				//[2] add end
				profile.getItemMap(IProfileCategory.I_SCORE_COMMON).put(IProfileItem.PRINT_TARGET, CollectionUtil.deSplitComma(printTarget));
				if ("i101".equals(getForward(request))) {
					//4.セキュリティスタンプをセット
					profile.getItemMap(IProfileCategory.I_SCORE_SHEET).put(IProfileItem.PRINT_STAMP, new Short(i102Form.getPrintStamp()));
				}
			}

			iCommonMap.setTargetPersonId(i102Form.getTargetPersonId());
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD START
                        iCommonMap.setTargetGrade(i102Form.getTargetGrade());
                        iCommonMap.setPrintStamp(i102Form.getPrintStamp());
                        iCommonMap.setPrintStudent(i102Form.getPrintStudent());
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD END
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}
}

package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.data.individual.StudentData;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 生徒が受験した模試情報を取得するためのBean
 * 
 * 2006.12.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class TakenExamBean extends DefaultBean {

	private List studentList;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					"SELECT ri.examyear || ri.examcd "
					+ "FROM individualrecord ri "
					+ "WHERE ri.s_individualid = ?");
			for (Iterator ite = studentList.iterator(); ite.hasNext();) {
				final StudentData data = (StudentData) ite.next();
				ps.setString(1, data.getStudentPersonId());
				rs = ps.executeQuery();
				while (rs.next()) {
					data.addTakenExam(rs.getString(1));
				}
				rs.close();
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * @param pStudentList 設定する studentList
	 */
	public void setStudentList(List pStudentList) {
		studentList = pStudentList;
	}
	
}

/**
 * 高校成績分析−過回比較　成績概況（営業用）
 * 	Excelファイル編集
 * 作成日: 2004/09/
 * @author	A.Hasegawa
 */

package jp.co.fj.keinavi.excel.business;

import java.io.*;
import java.util.*;

import jp.co.fj.keinavi.excel.data.business.*;
import jp.co.fj.keinavi.excel.cm.*;

import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.*;
//import org.apache.poi.poifs.filesystem.*;

public class B45_01 {

private int	noerror		= 0;	// 正常終了
private int	errfread	= 1;	// ファイルreadエラー
private int	errfwrite	= 2;	// ファイルwriteエラー
private int	errfdata	= 3;	// データ設定エラー

private CM		cm			= new CM();	//共通関数用クラス インスタンス

final private String	masterfile0	= "B45_01";		// ファイル名
final private String	masterfile1	= "NB45_01";	// ファイル名
private String	masterfile	= "";					// ファイル名
final private String	strArea		= "A1:AC39";	// 印刷範囲	
final private String	mastersheet = "tmp";		// テンプレートシート名
final private int[]	tabRow		= {5,17,29,41};	// 表の基準点
final private int[]	tabCol		= {1,5,9,13,17,21,25,29};		// 表の基準点
private boolean bolBookCngFlg = true;			//改ファイルフラグ
private boolean bolSheetCngFlg = true;			//改シートフラグ

/*
 * 	Excel編集メイン
 * 		B45Item b45Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
	public int b45_01EditExcel(B45Item b45Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		FileInputStream		fin				= null;
		HSSFWorkbook		workbook		= null;
		HSSFSheet			workSheet		= null;
		HSSFRow				workRow			= null;
		HSSFCell			workCell		= null;


		//テンプレートの決定
		if (b45Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// *作成用
		int		maxSheetIndex	= 0;		// シートカウンター
		int		fileIndex		= 0;		// ファイルカウンター
		float		hensa			= 0;		// 最新模試の偏差値保持
		String		kmkCd			= "";		// 今年度の科目コード保持
		int		hyouCnt			= 0;		// 値範囲：０〜２(表カウンター)
		int		takouCnt		= 0;		// 値範囲：０〜６(他校カウンター)
		String		kmk				= "";		// 型・科目チェック用
		boolean	setFlg			= true;	// *セット判断フラグ
		ArrayList	homeList		= new ArrayList();	//自校情報格納リスト
		int		row				= 0;		// 模試数カウンタ

		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("B44_05","B44_05帳票作成開始","");
	
		// 基本ファイルを読込む
		B45ListBean b45ListBean = new B45ListBean();
	
		try {
			
			// 型データセット
			ArrayList	b45List		= b45Item.getB45List();
			Iterator	itr	= b45List.iterator();
			
			while( itr.hasNext() ) {
				b45ListBean = (B45ListBean)itr.next();
				
				// 基本ファイルを読込む
				B45KoukouListBean b45KoukouListBean = new B45KoukouListBean();
				
				// 他校データセット
				ArrayList b45KoukouList = b45ListBean.getB45KoukouList();
				Iterator itrKoukou = b45KoukouList.iterator();
				
				hensa = 0;
				takouCnt = 0;
				String takou = "";
				homeList = new ArrayList();

				while ( itrKoukou.hasNext() ){
					b45KoukouListBean = (B45KoukouListBean)itrKoukou.next();
					
					if (takouCnt>=8 && hyouCnt>=3 && !cm.toString(takou).equals(cm.toString(b45KoukouListBean.getStrGakkomei()))) {
						// 同型・科目で他校のデータが50シート目を超えたときファイル保存
						if(!cm.toString(takou).equals(cm.toString(b45KoukouListBean.getStrGakkomei())) && maxSheetIndex==50){
//							// 元のテンプレートの削除
//							workbook.removeSheetAt(workbook.getSheetIndex(mastersheet));

//							for(int i=0; i<workbook.getNumberOfSheets(); i++){
//								workSheet = workbook.getSheetAt(i);
								// 印刷範囲を1ページに設定
//								HSSFPrintSetup ps = workSheet.getPrintSetup();
//								workSheet.setAutobreaks(true);
//								ps.setFitHeight((short)1);
//								ps.setFitWidth((short)1);
								// 印刷範囲の設定
//								workbook.setPrintArea(i, strArea);
//							}

//							// シートの選択
//							workbook.getSheet("1").setSelected(true);

							// Excelファイル保存
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex+1, masterfile, maxSheetIndex);
							fileIndex++;
							if( bolRet == false ){
								return errfwrite;
							}
						}
					}
					//改表、改シートチェック
					if (takouCnt>=8 && !cm.toString(takou).equals(cm.toString(b45KoukouListBean.getStrGakkomei())) ) {
						hyouCnt++;
						takouCnt = 0;
					}
					if ( hyouCnt>=4 ) {
						bolSheetCngFlg = true;
						hyouCnt = 0;
					}
					
					if(takouCnt==0 && hyouCnt==0 && maxSheetIndex==50){
						bolBookCngFlg = true;
					}
					//マスタExcel読み込み
					if(bolBookCngFlg){
						if((maxSheetIndex==50)||(maxSheetIndex==0)){
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook.equals(null) ){
								return errfread;
							}
							bolBookCngFlg = false;
							maxSheetIndex=0;
						}
					}
					if(bolSheetCngFlg){
						// シートテンプレートのコピー
						workSheet = workbook.cloneSheet(0);
						maxSheetIndex++;
		
						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);
		
						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, b45Item.getIntSecuFlg() ,30 ,32 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
						workCell.setCellValue(secuFlg);
		
//						// 学校名セット
//						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
//						workCell.setCellValue( "学校名　：" + b45Item.getStrGakkomei() );
			
						// 対象模試セット
						String moshi =cm.setTaisyouMoshi( b45Item.getStrMshDate() );	// 模試月取得
						workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
						workCell.setCellValue( "対象模試：" + cm.toString(b45Item.getStrMshmei()) + moshi);
						bolSheetCngFlg = false;
					}
				
					// 型・科目名+配点セット
					if ( takouCnt==0 ) {
//						if ( !b45ListBean.getStrKmkCd().equals(kmkCd) || (takouCnt==0 && hyouCnt==0) ) {
							kmkCd = b45ListBean.getStrKmkCd();
							String haiten = "";
							if ( b45ListBean.getIntHaitenKmk() != 0 ) {
								haiten="(" + b45ListBean.getIntHaitenKmk() + ")";
							}
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]-1, 0 );
							workCell.setCellValue( "型・科目：" + cm.toString(b45ListBean.getStrKmkmei()) + haiten );
//						}
					}
					
					if ( !cm.toString(takou).equals(cm.toString(b45KoukouListBean.getStrGakkomei())) ) {
						// 他校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt], tabCol[takouCnt] );
						workCell.setCellValue( b45KoukouListBean.getStrGakkomei() );
						takouCnt++;
						row = 0;
						setFlg = true;
					}

					takou = b45KoukouListBean.getStrGakkomei();
					
					if ( takouCnt==1 ) {
						// 模試名セット
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, 0 );
						workCell.setCellValue( b45KoukouListBean.getStrKakaiMshmei() );
					}
					//最新模試の偏差値保持
					if ( b45KoukouListBean.getFloHensa()!=-999.0 && setFlg ) {
						hensa = b45KoukouListBean.getFloHensa();	
						setFlg = false;
					}
					
//					// *セット
//					if (b45KoukouListBean.getFloHensa() != -999.0) {
//						if ( hensa < b45KoukouListBean.getFloHensa() ) {
//							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[takouCnt-1]+2 );
//							workCell.setCellValue("*");
//						}
//					}
					
					// 人数セット
					if ( b45KoukouListBean.getIntNinzu() != -999 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[takouCnt-1] );
						workCell.setCellValue( b45KoukouListBean.getIntNinzu() );
					}

					// 平均点セット
					if ( b45KoukouListBean.getFloHeikin() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[takouCnt-1]+1 );
						workCell.setCellValue( b45KoukouListBean.getFloHeikin() );
					}

					// 平均偏差値セット
					if ( b45KoukouListBean.getFloHensa() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[takouCnt-1]+3 );
						workCell.setCellValue( b45KoukouListBean.getFloHensa() );
					}
					row++;
					
				}
				hyouCnt++;
				
				// 50シート以上データが存在するときファイル保存
				if(hyouCnt >= 4 && itr.hasNext() && maxSheetIndex==50){
//					// 元のテンプレートの削除
//					workbook.removeSheetAt(workbook.getSheetIndex(mastersheet));

//					for(int i=0; i<workbook.getNumberOfSheets(); i++){
//						workSheet = workbook.getSheetAt(i);
						// 印刷範囲を1ページに設定
//						HSSFPrintSetup ps = workSheet.getPrintSetup();
//						workSheet.setAutobreaks(true);
//						ps.setFitHeight((short)1);
//						ps.setFitWidth((short)1);
						// 印刷範囲の設定
//						workbook.setPrintArea(i, strArea);
//					}

//					// シートの選択
//					workbook.getSheet("1").setSelected(true);

					// Excelファイル保存
					boolean bolRet = false;
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex+1, masterfile, maxSheetIndex);
					fileIndex++;
					if( bolRet == false ){
						return errfwrite;
					}
				}

			}
		
			// Excelファイル保存
			boolean bolRet = false;
			if(fileIndex != 0){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex+1, masterfile, maxSheetIndex);
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;					
			}
		
		} catch(Exception e) {
			log.Err("99B44_5","データセットエラー",e.toString());
			e.printStackTrace();
			return errfdata;
		}

		log.Ep("B44_05","B44_05帳票作成終了","");
		return noerror;
	}

}

package jp.co.fj.keinavi.beans.sales;

import java.sql.PreparedStatement;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.interfaces.SpecialAppliStatus;
import jp.co.fj.keinavi.util.PasswordGenerator;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 特例成績データ作成承認画面の承認処理Beanです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD203AcceptBean extends DefaultBean {

	/** serialVersionUID */
	private static final long serialVersionUID = -8952551215444544639L;

	/** パスワード生成クラス（前半2文字用） */
	private static final PasswordGenerator PWD_PREFIX = new PasswordGenerator(
			"ABCDEFGHJKLMNPQRSTUVWXYZ");

	/** パスワード生成クラス（後半8文字用） */
	private static final PasswordGenerator PWD_NUMBER = new PasswordGenerator(
			"0123456789");

	/** 承認後の申請状態 */
	private final SpecialAppliStatus status;

	/** 承認する申請ID */
	private final List appIdList;

	/**
	 * コンストラクタです。
	 */
	public SD203AcceptBean(SpecialAppliStatus status, List appIdList) {
		this.status = status;
		this.appIdList = appIdList;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance()
					.load("sd203_accept").toString());
			ps.setString(1, status.code);
			ps.setString(4, status.code);
			for (Iterator ite = appIdList.iterator(); ite.hasNext();) {
				ps.setString(2, createPassword());
				ps.setString(3, (String) ite.next());
				ps.executeUpdate();
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * パスワードを生成します。
	 */
	private String createPassword() {

		if (!SpecialAppliStatus.ACCEPTED.equals(status)) {
			/* 承認済とならないならパスワードは生成しない */
			return null;
		}

		return PWD_PREFIX.generate(2) + PWD_NUMBER.generate(8);
	}

}

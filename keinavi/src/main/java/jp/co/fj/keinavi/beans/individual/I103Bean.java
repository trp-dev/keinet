package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

//import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.IExamData;
import jp.co.fj.keinavi.data.individual.SubRecordIData;
import jp.co.fj.keinavi.util.KNUtil;

import com.fjh.beans.DefaultBean;

/**
 *
 * バランスチャートの偏差値データ取得用Bean
 * 
 * 2004/06/29    [新規作成]
 *
 * @author Keisuke KONDO - TOTEC
 * @version 1.0
 * 
 *   [1]    2005.01.21 kondo      change ２次 英語＋リスニング追加対応
 *   [2]    2005.01.21 kondo      change 配点200点の科目が複数の場合最初の1つをとるように修正
 *   [3]    2005.05.06 kondo      change 対象生徒が対象模試以前の模試を受けているか否かを返すメソッドを追加
 *   [4]    2005.09.16 kondo      change 科目取得の際EXAMSUBJECTではなく、Viewを参照するように修正
 *   [5]    2005.10.17 kondo      change 対象生徒が対象模試を受けていない場合でも、必ず対象模試の空データを取得するよう修正
 *   [6]    2005.10.25 kondo             外部データ、内部データを切り替えるように修正
 * 2006.09.04     Totec)T.Yamada      [7]受験学力測定対応
 * 
 * 2009.11.24   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応 
 * 
 */
public class I103Bean extends DefaultBean {

    private static final long serialVersionUID = -3241913773562555296L;
    
    private String targetExamYear;
    
    private String targetExamCode;
    
    private String[] targetExamCodes = null;
    
    private String[] deviationRanges;
    
    private String personId;
    
    private boolean helpflg;
    
	private String SQL_BASE = 
		"SELECT"+
		" /*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */"+//12/5 SQL HINT
		" SUBRECORD_I.INDIVIDUALID"+
		" ,SUBRECORD_I.A_DEVIATION"+
		" ,SUBRECORD_I.ACADEMICLEVEL"+
		" ,SUBRECORD_I.SUBCD"+
		" ,EXAMINATION.EXAMNAME"+
		" ,EXAMINATION.DISPSEQUENCE"+
		" ,EXAMINATION.EXAMTYPECD"+
		" ,SUBRECORD_I.EXAMCD"+
		" ,SUBRECORD_I.EXAMYEAR"+
		" ,V_I_CMEXAMSUBJECT.SUBALLOTPNT"+//[4] add
		" FROM SUBRECORD_I"+
		" INNER JOIN EXAMINATION ON EXAMINATION.EXAMYEAR = SUBRECORD_I.EXAMYEAR"+
		" AND EXAMINATION.EXAMCD = SUBRECORD_I.EXAMCD"+
		" INNER JOIN V_I_CMEXAMSUBJECT ON V_I_CMEXAMSUBJECT.EXAMYEAR = SUBRECORD_I.EXAMYEAR"+//[4] add
		" AND SUBRECORD_I.EXAMCD = V_I_CMEXAMSUBJECT.EXAMCD"+//[4] add
		" AND SUBRECORD_I.SUBCD = V_I_CMEXAMSUBJECT.SUBCD";//[4] add
	
  
	private String moshiSql;
	private String comboSql;
	private String subNumSql;
	
    //科目配点が200点のものを探すクエリ
	private String countSql;

	private Collection subNames;
	
	private Collection examinationDatas;
	
	private boolean didTakeExam;//[3] 模試を受けているか
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
        
        //科目配点が200点のものを探すクエリ
        countSql = createCountQuery();

        //偏差値データの取得
		setExaminationDatas(execDeviations());

        //教科名の取得
		setSubNames(execSubNames(targetExamYear, targetExamCode));
		
        //対象生徒が対象年度の対象模試を含む対象模試以前の模試を受けているかを返す
		setDidTakeExam(didTakeExam(personId, targetExamYear, targetExamCode));
	}
    
    private String createCountQuery() {
        
        StringBuffer buf = new StringBuffer();
        
        buf.append(SQL_BASE);
        buf.append(get200Count(personId, targetExamYear,targetExamYear, targetExamCode ,isHelpflg()));
        
        //模試選択がおこなわれている（=初回移行)なら絞り込み条件を付加
        buf.append(getExamCdCondition(targetExamCode, targetExamCodes));
        
        buf.append(getMoshiOrderBy());
        
        return buf.toString();
    }

	/**
	 * 模試コードを複数指定
	 * @param string
	 * @param strings
	 * @return
	 */
	private String getExamCdCondition(String string, String[] strings){
		String sqlOption = " AND SUBRECORD_I.EXAMCD IN (";
		sqlOption += " '"+string+"'";
		for(int i = 0; i < targetExamCodes.length; i ++){
			sqlOption += ",'" + targetExamCodes[i] + "'";
		}
		sqlOption += ")";
		return sqlOption;
	}
	/**
	 * 模試コードを複数指定
	 * @param string
	 * @param strings
	 * @return
	 */
	private String getExamCdCondition2(String string, String[] strings){
		String sqlOption = "WHERE SI.EXAMCD IN (";
		sqlOption += " '"+string+"'";
		for(int i = 0; i < targetExamCodes.length; i ++){
			sqlOption += ",'" + targetExamCodes[i] + "'";
		}
		sqlOption += ")";
		return sqlOption;
	}
	
	/**
	 * 模試データ取得用ORDER BY
	 * @return
	 */
	private String getMoshiOrderBy(){
		
		String sqlOption = "";
		if(helpflg == true){
			sqlOption =
			" ORDER BY EXAMINATION.IN_DATAOPENDATE DESC, EXAMINATION.EXAMCD DESC";
			
		}else{
			sqlOption =
			" ORDER BY EXAMINATION.OUT_DATAOPENDATE DESC, EXAMINATION.EXAMCD DESC";
		}

		return sqlOption;
	}	
	
	/**
	 * 科目の配点が200の物を探す
	 * @return
	 */	
	private String get200Count(String id, String string2, String year, String cd ,boolean isHelpflg){
	
		String sqlOption ="";
        
		if(isHelpflg){
            
			sqlOption =
			" WHERE SUBRECORD_I.INDIVIDUALID = ?"+ 
			" AND SUBRECORD_I.EXAMYEAR = '"+string2+"'"+
			" AND (V_I_CMEXAMSUBJECT.SUBALLOTPNT = '200' OR V_I_CMEXAMSUBJECT.SUBCD = '1190') "+//[4] add
			" AND EXAMINATION.IN_DATAOPENDATE <= ("+
						   " SELECT EXAMINATION.IN_DATAOPENDATE FROM EXAMINATION"+
						   " WHERE EXAMINATION.EXAMYEAR = '"+year+"'"+
						   " AND EXAMINATION.EXAMCD = '"+cd+"'"+
						   " )"+
			" AND ((SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '1') OR (SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '2') OR (SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '3'))";//[4] add
		} else {
			sqlOption =
			" WHERE SUBRECORD_I.INDIVIDUALID = ?"+ 
			" AND SUBRECORD_I.EXAMYEAR = '"+string2+"'"+
			" AND (V_I_CMEXAMSUBJECT.SUBALLOTPNT = '200' OR V_I_CMEXAMSUBJECT.SUBCD = '1190') "+//[4] add
			" AND EXAMINATION.OUT_DATAOPENDATE <= ("+
						   " SELECT EXAMINATION.OUT_DATAOPENDATE FROM EXAMINATION"+
						   " WHERE EXAMINATION.EXAMYEAR = '"+year+"'"+
						   " AND EXAMINATION.EXAMCD = '"+cd+"'"+
						   " )"+
			" AND ((SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '1') OR (SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '2') OR (SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '3'))";//[4] add
		}
		
		return sqlOption;
	}
	
	private String getMoshiSql2() {
        
		String DATAOPENDATE = null;
        
		if(isHelpflg()) {
			DATAOPENDATE = "IN_DATAOPENDATE";
		} else {
			DATAOPENDATE = "OUT_DATAOPENDATE";
		}
        
		StringBuffer strBuf = new StringBuffer();
        
//[5] add start
		strBuf.append("SELECT ");
		strBuf.append("	null individualid, ");
		strBuf.append("	null a_deviation, ");
		strBuf.append(" null academiclevel, ");
		strBuf.append("	null subcd, ");
		strBuf.append("	ex.examname, ");
		strBuf.append("	null dispsequence, ");
		strBuf.append("	ex.examtypecd , ");
		strBuf.append("	ex.examcd , ");
		strBuf.append("	ex.examyear , ");
		strBuf.append("	null suballotpnt, ");
		//strBuf.append("	ex.in_dataopendate ");//[6] delete
		strBuf.append("	ex.").append(DATAOPENDATE).append(", ");//[6] add
		strBuf.append(" null scope ");
		strBuf.append("FROM ");
		strBuf.append("	examination ex ");
		strBuf.append("WHERE ");
		strBuf.append("	NOT EXISTS ");
		strBuf.append("	( ");
		strBuf.append("		SELECT ");
		strBuf.append("			/*+ INDEX(SI PK_SUBRECORD_I) */ ");
		strBuf.append("			null ");
		strBuf.append("		FROM ");
		strBuf.append("			subrecord_i si ");
		strBuf.append("		WHERE ");
		strBuf.append("			si.individualid = ? ");
		strBuf.append("		AND ");
		strBuf.append("			si.examyear = ex.examyear ");
		strBuf.append("		AND ");
		strBuf.append("			si.examcd = ex.examcd ");
		strBuf.append("	) ");
		strBuf.append("AND ");
		strBuf.append("	ex.examcd = '").append(targetExamCode).append("' ");
		strBuf.append("AND ");
		strBuf.append("	ex.examyear = '").append(targetExamYear).append("' ");
//[5] add end

		strBuf.append("UNION ALL ");
		strBuf.append("SELECT ");
		strBuf.append(" /*+ INDEX(SI PK_SUBRECORD_I) */ ");//12/5 sql hint
		strBuf.append("SI.INDIVIDUALID , ");
		strBuf.append("SI.A_DEVIATION , ");
		strBuf.append("SI.ACADEMICLEVEL , ");
		strBuf.append("SI.SUBCD , ");
		strBuf.append("EX.EXAMNAME , ");
		strBuf.append("ES.DISPSEQUENCE , ");
		strBuf.append("EX.EXAMTYPECD , ");
		strBuf.append("SI.EXAMCD , ");
		strBuf.append("SI.EXAMYEAR , ");
		strBuf.append("ES.SUBALLOTPNT, ");
		strBuf.append("EX.").append(DATAOPENDATE).append(", ");
		strBuf.append("SI.SCOPE ");
		strBuf.append("FROM ");
		strBuf.append("SUBRECORD_I SI ");
		strBuf.append("INNER JOIN ");
		strBuf.append("EXAMINATION EX ");
		strBuf.append("ON EX.EXAMYEAR = SI.EXAMYEAR ");
		strBuf.append("AND EX.EXAMCD = SI.EXAMCD ");
		strBuf.append("AND SI.INDIVIDUALID = ? ");
		strBuf.append("AND SI.EXAMYEAR = '").append(targetExamYear).append("' ");
		strBuf.append("AND EX.").append(DATAOPENDATE).append(" <= ( ");
		strBuf.append("SELECT ");
		strBuf.append(DATAOPENDATE).append(" ");
		strBuf.append("FROM ");
		strBuf.append("EXAMINATION ");
		strBuf.append("WHERE ");
		strBuf.append("EXAMYEAR = '").append(targetExamYear).append("' ");
		strBuf.append("AND EXAMCD = '").append(targetExamCode).append("' ");
		strBuf.append(") ");
		strBuf.append("INNER JOIN ");
		//strBuf.append("EXAMSUBJECT ES ");//[4] delete
		strBuf.append("V_I_CMEXAMSUBJECT ES ");//[4] add
		strBuf.append("ON ES.EXAMYEAR = SI.EXAMYEAR ");
		strBuf.append("AND SI.EXAMCD = ES.EXAMCD ");
		strBuf.append("AND SI.SUBCD = ES.SUBCD ");
		strBuf.append(getExamCdCondition2(targetExamCode, targetExamCodes));
		strBuf.append("ORDER BY ");
		strBuf.append(DATAOPENDATE).append(" DESC ");
		strBuf.append(",EXAMCD DESC ");
		strBuf.append(",DISPSEQUENCE ASC");
        
		return strBuf.toString();
	}
	//[3] add start
    
	/**
	 * 対象生徒が、対象年度の対象模試を含む対象模試以前の模試を受けているかを返す
	 * @return booolean 模試受験有無
	 */
	private boolean didTakeExam(String individualId, String examYear, String examCd) throws Exception {
		String DATAOPENDATE = null;
		if(isHelpflg()) {
			DATAOPENDATE = "IN_DATAOPENDATE";
		} else {
			DATAOPENDATE = "OUT_DATAOPENDATE";
		}
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT COUNT(*) COUNT FROM EXAMINATION EX ");
		strBuf.append("WHERE EXISTS (SELECT /*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */ * FROM SUBRECORD_I WHERE INDIVIDUALID = ? AND EXAMCD = EX.EXAMCD AND EX.EXAMYEAR = EXAMYEAR)");
		strBuf.append("AND EXAMYEAR = '").append(examYear).append("'");
		strBuf.append("AND EX.").append(DATAOPENDATE).append(" <= (SELECT ").append(DATAOPENDATE).append(" FROM EXAMINATION WHERE EXAMYEAR = '").append(examYear).append("' AND EXAMCD = '").append(examCd).append("')");
		
		String count = "0";
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = conn.prepareStatement(strBuf.toString());
			ps.setString(1, individualId);
			rs = ps.executeQuery();
			while(rs.next()) {
				count = rs.getString("COUNT");
			}
			if(ps != null) ps.close();
			if(rs != null) rs.close();
			if(count.equals("0")) {
				return false;
			} else {
				return true;
			}
		}catch(Exception e) {
			throw e;
		} finally {
			if(ps != null) ps.close();
			if(rs != null) rs.close();
		}
	}
	//[3] add end
	/**
	 * １．この年のこの模試の教科コードをすべて取得
	 * ２．取得した教科コードから教科名を取得
	 * @param string
	 * @param string2
	 * @return
	 */
	private Collection execSubNames(String year, String code) throws Exception {
		//その模試にある全ての教科コードを取得（総合含む）

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PreparedStatement pstmt2 = null;
		ResultSet rs2 = null;
		try{
			String kmkGet = ""+
			" SELECT"+
			
			//1215 kondo 追加 start
			" EXAMINATION.TERGETGRADE,"+
			//1215 kondo 追加 end
			
			" EXAMSUBJECT.SUBCD"+
			" FROM EXAMINATION"+
			" INNER JOIN EXAMSUBJECT ON EXAMINATION.EXAMYEAR = EXAMSUBJECT.EXAMYEAR"+
			" AND EXAMINATION.EXAMCD = EXAMSUBJECT.EXAMCD"+
			" WHERE EXAMINATION.EXAMYEAR = '"+year+"'"+
			" AND EXAMINATION.EXAMCD = '"+code+"'"+
			" ORDER BY EXAMSUBJECT.SUBCD";
			
			pstmt = conn.prepareStatement(kmkGet);
			rs = pstmt.executeQuery();
			Collection subs = new ArrayList();
			
			while(rs.next()){
				
				//1215 kondo 追加 start
				if(rs.getString("TERGETGRADE").equals("03")) {
					//対象学年が３年の場合、５教科を設定
					subs.add("1");
					subs.add("2");
					subs.add("3");
					subs.add("4");
					subs.add("5");
					break;
				}
				//1215 kondo 追加 end
				
				String subIndex = rs.getString("SUBCD").substring(0, 1);
				//総合はカウントせずにsubsを一意の教科にする
				//1215 kondo 修正(６０００番台は含まない)
				if(Integer.parseInt(rs.getString("SUBCD").substring(0, 1)) < 6 && !subs.contains(subIndex)){
					subs.add(subIndex);
				}
			}
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
			//その模試の教科数
			//int subNum = subs.size();
			
			//上記で取得した教科の教科名の取得
			String nmGet = ""+
			" SELECT COURSENAME, YEAR, COURSECD"+ 
			" FROM COURSE"+
			" WHERE YEAR = '"+year+"' AND COURSECD IN (";
			int count = 0;
			for (Iterator it=subs.iterator(); it.hasNext();) {
				if(count != 0)
					nmGet += ",";
				nmGet += "'"+((String)it.next()).concat("000")+"'";
				count ++;
			}
			nmGet += ") ORDER BY COURSECD";
			
			pstmt2 = conn.prepareStatement(nmGet);
			rs2 = pstmt2.executeQuery();
			Collection subNames = new ArrayList();
			while(rs2.next()){
				String courseName = rs2.getString("COURSENAME");
				if(!subNames.contains(courseName))
					subNames.add(courseName);
			}
			if(pstmt2 != null) pstmt2.close();
			if(rs2 != null) rs2.close();
			
			return subNames;
			
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
			if(pstmt2 != null) pstmt2.close();
			if(rs2 != null) rs2.close();
		}
	}
	
	/**
	 * 偏差値データの取得
	 * @return
	 * @throws Exception
	 */
	private List execDeviations() throws Exception{
		
		final String ELSUBCD = "1190"; //[1] add 英語＋リスニング
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PreparedStatement pstmt2 = null;
		ResultSet rs2 = null;

		try{
			pstmt2 = conn.prepareStatement(getCountSql());		
			pstmt2.setString(1, personId);
			rs2 = pstmt2.executeQuery();	
			ArrayList examNameM200 = new ArrayList();
			ArrayList examNameJ200 = new ArrayList();
			ArrayList examNameE200 = new ArrayList();
			ArrayList examNameEL = new ArrayList(); //[1] add 英語＋リスニング
			String subIndex = "";
			while(rs2.next()){
				//配点が200以上の数学・国語を持つ模試リスト作成
				if(rs2.getString("SUBCD") != null){
					subIndex = rs2.getString("SUBCD").substring(0, 1);
					if(subIndex.equals("2")){
						examNameM200.add(rs2.getString("EXAMNAME")); //数学
					}else{
						if(subIndex.equals("3")){
							examNameJ200.add(rs2.getString("EXAMNAME")); //国語
						}else{
							examNameE200.add(rs2.getString("EXAMNAME")); //英語
							//[1] add start
							if(rs2.getString("SUBCD").equals(ELSUBCD)){
								examNameEL.add(rs2.getString("EXAMNAME")); //英語＋リスニング
							}
							//[1] add end
						}                  
					}
				}
				
			}
			if(pstmt2 != null) pstmt2.close();
			if(rs2 != null) rs2.close();
			
			pstmt = conn.prepareStatement(getMoshiSql2());
			pstmt.setString(1, personId);
			pstmt.setString(2, personId);
			//pstmt = conn.prepareStatement(getMoshiSql());	
			rs = pstmt.executeQuery();
			Map mappy = new LinkedHashMap();
			//数学・国語の表示順序1番目を取るカウント
			int mathCount = 0;
			int japanCount = 0;
			int engCount = 0;
			
			while(rs.next()){			
				IExamData examinationData = (IExamData) mappy.get(rs.getString("EXAMCD"));
				
				if(examinationData == null){
					//マップの中にこの模試が存在しなければ新たに作成
					//同時に科目のコレクションの初期化
					examinationData = new IExamData();
					if(getExaminationDatas() == null)
						setExaminationDatas(new ArrayList());
					getExaminationDatas().add(examinationData);
					mappy.put(rs.getString("EXAMCD"), examinationData);
					//数学・国語・英語カウント模試ごとに初期化
					mathCount = 0;
					japanCount = 0;
					engCount = 0;
				}
				
				examinationData.setExamYear(rs.getString("EXAMYEAR"));
				examinationData.setExamCd(rs.getString("EXAMCD"));
				examinationData.setExamName(rs.getString("EXAMNAME"));
				examinationData.setExamTypeCd(rs.getString("EXAMTYPECD"));
				
				Collection subRecordDatas = examinationData.getSubRecordDatas();
				if(subRecordDatas == null){
					subRecordDatas = new ArrayList();
					examinationData.setSubRecordDatas(subRecordDatas);
				}
				
				SubRecordIData subRecordData = new SubRecordIData();
				
				String subIndex2 = "";
				if(rs.getString("SUBCD") != null){
					subIndex2 = rs.getString("SUBCD").substring(0, 1);
				}
								
				if(examNameE200.contains(rs.getString("EXAMNAME")) && subIndex2.equals("1") && engCount == 0){
					//[1] start
					//模試が英語＋Ｌリストに存在する場合、英語＋Ｌに限定
					if(examNameEL.contains(rs.getString("EXAMNAME"))) {
						if(nullToEmpty(rs.getString("SUBCD")).equals(ELSUBCD)) {
							//英語＋Ｌ
							if(rs.getString("A_DEVIATION") != null){
								subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));
								subRecordData.setScholarlevel(nullToEmpty(rs.getString("ACADEMICLEVEL")));
								subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
								subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
								subRecordDatas.add(subRecordData);
							}
							engCount++;
						}
					} else {
						if(nullToEmpty(rs.getString("SUBALLOTPNT")).equals("200")) {
							//英語得点200得点
							if(rs.getString("A_DEVIATION") != null){
								subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));
								subRecordData.setScholarlevel(nullToEmpty(rs.getString("ACADEMICLEVEL")));
								subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
								subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
								subRecordDatas.add(subRecordData);
							}
							engCount++;
						}
					}
					//[1] end
				}else if(!examNameE200.contains(rs.getString("EXAMNAME")) && subIndex2.equals("1") && engCount == 0){
					//英語得点200得点ではない得点の表示順序1番目
					if(rs.getString("A_DEVIATION") != null){
						subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));
						subRecordData.setScholarlevel(nullToEmpty(rs.getString("ACADEMICLEVEL")));
						subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
						subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
						subRecordDatas.add(subRecordData);
					}
					engCount ++;
				}else if(examNameM200.contains(rs.getString("EXAMNAME")) && nullToEmpty(rs.getString("SUBALLOTPNT")).equals("200") && subIndex2.equals("2") && mathCount == 0){//[2]
					//数学配点200得点
					if(rs.getString("A_DEVIATION") != null){
						subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));
						subRecordData.setScholarlevel(nullToEmpty(rs.getString("ACADEMICLEVEL")));
						subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
						subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
						subRecordDatas.add(subRecordData);
					}
					mathCount ++;//[2] add
				}else if(!examNameM200.contains(rs.getString("EXAMNAME")) && subIndex2.equals("2") && mathCount == 0){
					//数学配点200ではない得点の表示順序1番目
					if(rs.getString("A_DEVIATION") != null){
						subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));
						subRecordData.setScholarlevel(nullToEmpty(rs.getString("ACADEMICLEVEL")));
						subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
						subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
						subRecordDatas.add(subRecordData);
					}
					mathCount ++;
				}else if(examNameJ200.contains(rs.getString("EXAMNAME")) && nullToEmpty(rs.getString("SUBALLOTPNT")).equals("200") && subIndex2.equals("3") && japanCount == 0){//[2]
					//国語配点200得点
					if(rs.getString("A_DEVIATION") != null){
						subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));
						subRecordData.setScholarlevel(nullToEmpty(rs.getString("ACADEMICLEVEL")));
						subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
						subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
						subRecordDatas.add(subRecordData);
					}
					japanCount ++;//[2] add
				}else if(!examNameJ200.contains(rs.getString("EXAMNAME")) && subIndex2.equals("3") && japanCount == 0){
					//国語配点200ではない得点の表示順序1番目
					if(rs.getString("A_DEVIATION") != null){		
						subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));
						subRecordData.setScholarlevel(nullToEmpty(rs.getString("ACADEMICLEVEL")));
						subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
						subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
						subRecordDatas.add(subRecordData);
					}
					japanCount ++;
				}else if(subIndex2.equals("4") || subIndex2.equals("5") || subIndex2.equals("6")){
					//理科・地歴得点
					if(rs.getString("A_DEVIATION") != null){
						subRecordData.setCDeviation(nullToEmptyF(rs.getString("A_DEVIATION")));
						subRecordData.setScholarlevel(nullToEmpty(rs.getString("ACADEMICLEVEL")));
						subRecordData.setSubCd(nullToEmpty(rs.getString("SUBCD")));
						subRecordData.setScope(nullToEmpty(rs.getString("SCOPE")));
						subRecordDatas.add(subRecordData);
					}
				}
			}

			return orderStraight(mappy);
			
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
			if(pstmt2 != null) pstmt2.close();
			if(rs2 != null) rs2.close();
		}
	}
    
	/**
     * 取得した結果を比較模試の順番にならびかえる 
     * @param container
     * @return
	 */
	private List orderStraight(final Map container){
        
        final IExamData[] exams = new IExamData[4];
        
        exams[0] = (IExamData) container.get(targetExamCode);
        exams[1] = (IExamData) container.get(targetExamCodes[0]);
        exams[2] = (IExamData) container.get(targetExamCodes[1]);
        exams[3] = (IExamData) container.get(targetExamCodes[2]);
        
	    final List list = new ArrayList();
        
	    for (int i = 0; i < exams.length; i++) {
		
            if (exams[i] != null) {
                list.add(exams[i]);
            }
		}
        
		return list;
	}
	
	/**
	 * nullが来たら空白にする
	 */
	private String nullToEmpty(String str) {
		if(str == null) {
			return "";
		}
		if(str.equals("null")) {
			return "";
		}
		return str;
	}
	/**
	 * StringをFloatのフォーマットで返します。
	 * @return
	 */
	public String nullToEmptyF(String str) {
		if(str == null) {
			return "";
		} else {
				return String.valueOf(new Float(str).floatValue());
		}
	}
	
	/**
	 * @return
	 */
	public String[] getDeviationRanges() {
		return deviationRanges;
	}

	/**
	 * @return
	 */
	public String getMoshiSql() {
		return moshiSql;
	}

	/**
	 * @return
	 */
	public String getPersonId() {
		return personId;
	}

	/**
	 * @return
	 */
	public String getSQL_BASE() {
		return SQL_BASE;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * @return
	 */
	public String[] getTargetExamCodes() {
		return targetExamCodes;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @param strings
	 */
	public void setDeviationRanges(String[] strings) {
		deviationRanges = strings;
	}

	/**
	 * @param string
	 */
	public void setMoshiSql(String string) {
		moshiSql = string;
	}

	/**
	 * @param string
	 */
	public void setPersonId(String string) {
		personId = string;
	}

	/**
	 * @param string
	 */
	public void setSQL_BASE(String string) {
		SQL_BASE = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param strings
	 */
	public void setTargetExamCodes(String[] strings) {
		targetExamCodes = strings;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * @return
	 */
	public String getComboSql() {
		return comboSql;
	}

	/**
	 * @param string
	 */
	public void setComboSql(String string) {
		comboSql = string;
	}

	/**
	 * @return
	 */
	public Collection getExaminationDatas() {
		return examinationDatas;
	}

	/**
	 * @param collection
	 */
	public void setExaminationDatas(Collection collection) {
		examinationDatas = collection;
	}

	/**
	 * @return
	 */
	public String getSubNumSql() {
		return subNumSql;
	}

	/**
	 * @param string
	 */
	public void setSubNumSql(String string) {
		subNumSql = string;
	}

	/**
	 * @return
	 */
	public Collection getSubNames() {
		return subNames;
	}

	/**
	 * @param collection
	 */
	public void setSubNames(Collection collection) {
		subNames = collection;
	}

	/**
	 * @return
	 */
	public String getCountSql() {
		return countSql;
	}

	/**
	 * @param string
	 */
	public void setCountSql(String string) {
		countSql = string;
	}

	/**
	 * @return
	 */
	public boolean isHelpflg() {
		return helpflg;
	}

	/**
	 * @param b
	 */
	public void setHelpflg(boolean b) {
		helpflg = b;
	}

	/**
	 * @return
	 */
	public boolean isDidTakeExam() {
		return didTakeExam;//[3] add
	}

	/**
	 * @param b
	 */
	public void setDidTakeExam(boolean b) {
		didTakeExam = b;//[3] add
	}

	/**
	 * 第1解答科目模試かどうか（対象模試）
	 * @return
	 */
	public boolean isAns1stExam() {
		return KNUtil.isAns1st(targetExamCode);
	}

	/**
	 * 第1解答科目模試かどうか（比較対象模試）
	 * @return
	 */
	public boolean isAns1stExamCodes() {
		for(int i = 0; i < targetExamCodes.length; i ++){
			if (KNUtil.isAns1st(targetExamCodes[i])){
				return true;
			}
		}
		return false;
	}

	/**
	 * 第1解答科目模試を返す
	 * @return
	 */
	public String[] getAns1st() {
		return KNUtil.getAns1st();
	}
}

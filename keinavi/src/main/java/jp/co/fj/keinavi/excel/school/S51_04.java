/**
 * 校内成績分析−過回・過年度比較　成績概況グラフ
 * 	Excelファイル編集
 * 作成日: 2004/08/20
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S51Item;
import jp.co.fj.keinavi.excel.data.school.S51ListBean;
import jp.co.fj.keinavi.excel.data.school.S51YearListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S51_04 {

	private int noerror	= 0;		// 正常終了
	private int errfread	= 1;		// ファイルreadエラー
	private int errfwrite	= 2;		// ファイルwriteエラー
	private int errfdata	= 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	final private String	masterfile0	= "S51_04";		// ファイル名
	final private String	masterfile1	= "NS51_04";	// ファイル名
	private String	masterfile	= "";					// ファイル名
//	final private String	strArea		= "A1:AG59";	// 印刷範囲	
	final private int[]	tabCol		= {0,17};	// 表の基準点
	final private int[]	tabRow		= {44,47,50,53,56};	// 表の基準点
	final private int intMaxSheetSr = 20;	//MAXシート数の値を入れる

/*
 * 	Excel編集メイン
 * 		S51Item s51Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s51_04EditExcel(S51Item s51Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
 		
		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s51Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S51ListBean s51ListBean = new S51ListBean();
		
		try {
			int		intMaxSheetIndex	= 0;	// シートカウンター
			int		intBookCngCount		= 0;	// ブックカウンター
			int		hyouCnt				= 0;	// 値範囲：０〜１　(表カウンター)
			int		yearCnt				= 0;	// 値範囲：０〜４　(年度カウンター)
			int		mshCnt				= 0;	// 値範囲：０〜６　(模試カウンター)
			int		maxMsh				= 0;	// 過回模試数
			float		hensa				= 0;	// 今年度偏差値保持
			boolean	setFlg				= true;// *セット判断フラグ
			boolean	bolSheetCngFlg		= true;// 改シートフラグ
			boolean	bolBookCngFlg		= true;// 改ファイルフラグ
//add 2004/10/27 T.Sakai データ0件対応
			int		dispKmkFlgCnt		= 0;	// グラフ表示フラグカウンタ
//add end
			
			//マスタExcel読み込み
			workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
			if( workbook==null ){
				return errfread;
			}
			
			// データセット
			ArrayList	s51List	= s51Item.getS51List();
			Iterator	itr		= s51List.iterator();
			
			while( itr.hasNext() ) {
				s51ListBean = (S51ListBean)itr.next();
				
				if ( s51ListBean.getIntDispKmkFlg()==1 ) {
					yearCnt = 0;
					mshCnt = 0;
					maxMsh = 0;
					String year = "";
					
					if ( hyouCnt==2 ) {
						hyouCnt = 0;
						bolSheetCngFlg = true;
						if ( bolSheetCngFlg && intMaxSheetIndex>=intMaxSheetSr ) {
							bolBookCngFlg = true;
						}
					}
					
					if ( bolSheetCngFlg ) {
						if(bolBookCngFlg && intMaxSheetIndex>=intMaxSheetSr ){
							// Excelファイル保存
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, intMaxSheetIndex);

							if( bolRet == false ){
								return errfwrite;					
							}

							//マスタExcel読み込み
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook==null ){
								return errfread;
							}
							intBookCngCount++;
							intMaxSheetIndex = 0;
						
						}
						// データセットするシートの選択
						workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex+1));
						intMaxSheetIndex++;
						
						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);
	
						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, s51Item.getIntSecuFlg() ,30 ,32 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
						workCell.setCellValue(secuFlg);
	
						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
						workCell.setCellValue( "学校名　：" + cm.toString(s51Item.getStrGakkomei()) );
	
						// 対象模試セット
						String moshi =cm.setTaisyouMoshi( s51Item.getStrMshDate() );	// 模試月取得
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s51Item.getStrMshmei()) + moshi);
	
						//　改表フラグをFalseにする
						bolSheetCngFlg	=false;
						bolBookCngFlg	=false;
					}
					
					// 型・科目名+配点セット
					String haiten = "";
					if ( !cm.toString(s51ListBean.getStrHaitenKmk()).equals("") ) {
						haiten="(" + s51ListBean.getStrHaitenKmk() + ")";
					}
					workCell = cm.setCell( workSheet, workRow, workCell, 4, tabCol[hyouCnt] );
					workCell.setCellValue( "型・科目：" + cm.toString(s51ListBean.getStrKmkmei()) + haiten );
					
					// 基本ファイルを読込む
					S51YearListBean s51YearListBean = new S51YearListBean();
					
					// 過年度データセット
					ArrayList s51YearList = s51ListBean.getS51YearList();
					Iterator itrYear = s51YearList.iterator();
					
					//過回模試数取得
					while ( itrYear.hasNext() ){
						s51YearListBean = (S51YearListBean)itrYear.next();
						if ( year.equals(s51YearListBean.getStrNendo()) || year=="" ) {
							year = s51YearListBean.getStrNendo();
							maxMsh++;
						}
					}
					itrYear = s51YearList.iterator();
					
					year = "";
					setFlg = true;
					
					while ( itrYear.hasNext() ){
						s51YearListBean = (S51YearListBean)itrYear.next();
						
						if ( !year.equals(s51YearListBean.getStrNendo()) ) {
							if ( !year.equals("") ) {
								yearCnt++;
							}
							// 年度セット
							if ( !cm.toString(s51YearListBean.getStrNendo()).equals("") ) {
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[yearCnt], tabCol[hyouCnt] );
								workCell.setCellValue( s51YearListBean.getStrNendo() + "年度" );
							}
							year = s51YearListBean.getStrNendo();
							mshCnt = 0;
						}
						if(yearCnt == 0){
							// 模試名セット
							String moshi =cm.setTaisyouMoshi( s51YearListBean.getStrMshDate() );	// 模試月取得
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[yearCnt]-1, tabCol[hyouCnt]+(maxMsh-mshCnt)*2 );
							workCell.setCellValue( cm.toString(s51YearListBean.getStrMshmei()) + moshi );
							//最新模試の偏差値保持
							if ( s51YearListBean.getFloHensa()!=-999.0 && setFlg ) {
								hensa = s51YearListBean.getFloHensa();	
								setFlg = false;
							}
							// *セット
							if (s51YearListBean.getFloHensa() != -999.0) {
								if ( hensa < s51YearListBean.getFloHensa() ) {
									workCell = cm.setCell( workSheet, workRow, workCell, tabRow[yearCnt]+2, tabCol[hyouCnt]+(maxMsh-mshCnt)*2 );
									workCell.setCellValue("*");
								}
							}
						}
						
						// 人数セット
						if ( s51YearListBean.getIntNinzu() != -999 ) {
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[yearCnt], tabCol[hyouCnt]+1+(maxMsh-mshCnt)*2 );
							workCell.setCellValue( s51YearListBean.getIntNinzu() );
						}
	
						// 平均点セット
						if ( s51YearListBean.getFloHeikin() != -999.0 ) {
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[yearCnt]+1, tabCol[hyouCnt]+1+(maxMsh-mshCnt)*2 );
							workCell.setCellValue( s51YearListBean.getFloHeikin() );
						}
	
						// 平均偏差値セット
						if ( s51YearListBean.getFloHensa() != -999.0 ) {
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[yearCnt]+2, tabCol[hyouCnt]+1+(maxMsh-mshCnt)*2 );
							workCell.setCellValue( s51YearListBean.getFloHensa() );
						}
	

						mshCnt++;
					}
					hyouCnt++;
//add 2004/10/27 T.Sakai データ0件対応
					dispKmkFlgCnt++;
//add end
				}
			}
//add 2004/10/27 T.Sakai データ0件対応
			if ( dispKmkFlgCnt==0 ) {
				// データセットするシートの選択
				workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex+1));
				intMaxSheetIndex++;
						
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
	
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s51Item.getIntSecuFlg() ,30 ,32 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
				workCell.setCellValue(secuFlg);
	
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s51Item.getStrGakkomei()) );
	
				// 対象模試セット
				String moshi =cm.setTaisyouMoshi( s51Item.getStrMshDate() );	// 模試月取得
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s51Item.getStrMshmei()) + moshi);
			}
//add end
			boolean bolRet = false;
			// Excelファイル保存
			if(intBookCngCount == 1){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, intMaxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("S51_04","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

}
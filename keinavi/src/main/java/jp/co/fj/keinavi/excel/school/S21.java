/**
 * 校内成績分析−クラス比較　成績概況（クラス比較）
 * @author Ito.Y
 */
 
package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.util.log.*;

public class S21 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean s21( S21Item s21Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog) {
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		try{
			int ret = 0;
			//S21Itemから各帳票を出力
			if (( s21Item.getIntHyouFlg()==0 ) && ( s21Item.getIntGraphFlg()==0 )){
				throw new Exception("S21 ERROR : フラグ異常 ");
			}
			if ( s21Item.getIntHyouFlg()==1 ) {
				S21_01 exceledit = new S21_01();
				ret = exceledit.s21_01EditExcel( s21Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S21_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S21_01");
			}
			if ( s21Item.getIntGraphFlg()==1 ) {
				S21_02 exceledit = new S21_02();
				ret = exceledit.s21_02EditExcel( s21Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S21_02","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S21_02");
			}
			
		} catch(Exception e) {
			log.Err("99S21","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
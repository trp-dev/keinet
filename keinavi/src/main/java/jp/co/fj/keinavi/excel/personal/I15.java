/**
 * 個人成績分析−受験大学スケジュール表
 * @author H.Fujimoto
 */
 
package jp.co.fj.keinavi.excel.personal;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.personal.*;
import jp.co.fj.keinavi.util.log.*;

public class I15 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean i15( I11Item i11Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog) {

		KNLog log = KNLog.getInstance(null,null,null);

		try{
			//I11Itemから各帳票を出力
			int ret = 0;
			if (  i11Item.getIntScheduleFormatFlg()!=1 && i11Item.getIntScheduleFormatFlg()!=2
			   && i11Item.getIntScheduleFormatFlg()!=3 ){
				throw new Exception("I15 ERROR : フラグ異常");
			}
			
			log.Ep("I15","I15帳票作成開始","");
			I15_01 exceledit1 = new I15_01();
			ret = exceledit1.i15_01EditExcel( i11Item, outfilelist, saveFlg, UserID );
			if (ret!=0) {
				log.Err("0"+ret+"I15","帳票作成エラー","");
				return false;
			}
			log.Ep("I15","I15帳票作成終了","");
			sheetLog.add("I15_01");
			
		} catch(Exception e) {
			e.printStackTrace();
			log.Err("99I15","帳票作成エラー",e.toString());
			return false;
		}
		return true;
	}

}
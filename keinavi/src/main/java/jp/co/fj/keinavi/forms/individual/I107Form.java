package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * �o�����X�`���[�g(���i�ҕ���)�p�t�H�[��
 * @author kondo
 *
 * 2005.4.26 	K.Kondo 	[1]��ʃX�N���[�����W�̒ǉ�
 */
public class I107Form extends ActionForm {

	private String mode;				//���[�h�i���́A�ʒk�j

	private String targetPersonId;		//�l�h�c
	private String targetExamCode;		//�͎��R�[�h
	private String targetExamYear;		//�͎��N�x

	private String targetGrade;			//�\������w�N
	
	private String targetCandidateRank1;	//��w�u�]���ʃR�[�h�P
	private String targetCandidateRank2;	//��w�u�]���ʃR�[�h�Q
	private String targetCandidateRank3;	//��w�u�]���ʃR�[�h�R
	private String lowerRange;				//�΍��l�͈́i�����j
	private String upperRange;				//�΍��l�͈́i����j
	private String avgChoice;				//�΍��l�v�Z���@�i���ρA�ǂ����j
	private String targetSubCode;			//�ȖڃR�[�h
	private String targetSubName;			//�Ȗږ�
	
	//�l���ѕ��͋���-����Ώې��k�i����Ώې��k)
	private String printStudent;
	
	//���ѕ��͋��ʁF����Ώ�
	private String[] printTarget;
	
	//�Z�L�����e�B�X�^���v
	private String printStamp;
	
	//����{�^���������p�����[�^
	private String printStatus;
	
	private String scrollX;//[1] add �T�u�~�b�g�O�A�\�����W�w
	private String scrollY;//[1] add �T�u�~�b�g�O�A�\�����W�x

	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}
	
	/**
	 * @return
	 */
	public String getAvgChoice() {
		return avgChoice;
	}

	/**
	 * @return
	 */
	public String getLowerRange() {
		return lowerRange;
	}

	/**
	 * @return
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @return
	 */
	public String getTargetCandidateRank1() {
		return targetCandidateRank1;
	}

	/**
	 * @return
	 */
	public String getTargetCandidateRank2() {
		return targetCandidateRank2;
	}

	/**
	 * @return
	 */
	public String getTargetCandidateRank3() {
		return targetCandidateRank3;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @return
	 */
	public String getTargetSubCode() {
		return targetSubCode;
	}

	/**
	 * @return
	 */
	public String getUpperRange() {
		return upperRange;
	}

	/**
	 * @param string
	 */
	public void setAvgChoice(String string) {
		avgChoice = string;
	}

	/**
	 * @param string
	 */
	public void setLowerRange(String string) {
		lowerRange = string;
	}

	/**
	 * @param string
	 */
	public void setMode(String string) {
		mode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetCandidateRank1(String string) {
		targetCandidateRank1 = string;
	}

	/**
	 * @param string
	 */
	public void setTargetCandidateRank2(String string) {
		targetCandidateRank2 = string;
	}

	/**
	 * @param string
	 */
	public void setTargetCandidateRank3(String string) {
		targetCandidateRank3 = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @param string
	 */
	public void setTargetSubCode(String string) {
		targetSubCode = string;
	}

	/**
	 * @param string
	 */
	public void setUpperRange(String string) {
		upperRange = string;
	}
	/**
	 * @return
	 */
	public String getTargetSubName() {
		return targetSubName;
	}

	/**
	 * @param string
	 */
	public void setTargetSubName(String string) {
		targetSubName = string;
	}

	/**
	 * @return
	 */
	public String getTargetGrade() {
		return targetGrade;
	}

	/**
	 * @param string
	 */
	public void setTargetGrade(String string) {
		targetGrade = string;
	}

	/**
	 * @return
	 */
	public String[] getPrintTarget() {
		return printTarget;
	}

	/**
	 * @param strings
	 */
	public void setPrintTarget(String[] strings) {
		printTarget = strings;
	}

	/**
	 * @return
	 */
	public String getPrintStamp() {
		return printStamp;
	}

	/**
	 * @param string
	 */
	public void setPrintStamp(String string) {
		printStamp = string;
	}

	/**
	 * @return
	 */
	public String getPrintStudent() {
		return printStudent;
	}

	/**
	 * @param string
	 */
	public void setPrintStudent(String string) {
		printStudent = string;
	}

	/**
	 * @return
	 */
	public String getPrintStatus() {
		return printStatus;
	}

	/**
	 * @param string
	 */
	public void setPrintStatus(String string) {
		printStatus = string;
	}
	
	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[1] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[1] add
	}
	
}

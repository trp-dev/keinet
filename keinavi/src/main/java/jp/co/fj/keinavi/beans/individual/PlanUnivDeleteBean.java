package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.util.sql.Query;

import com.fjh.beans.DefaultBean;

/**
 *
 * 受験予定大学削除
 * 
 * <2010年度改修>
 * 2010.01.19	Tomohisa YAMADA - TOTEC
 * 				新規作成
 *
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class PlanUnivDeleteBean extends DefaultBean {
	
	/**
	 * 個人ID
	 */
	private String individualId;
	
	/**
	 * 大学コード
	 */
	private String univcd;
	
	/**
	 * 学部コード
	 */
	private String facultycd;
	
	/**
	 * 学科コード
	 */
	private String deptcd;
	
	/**
	 * 入試形態コード
	 */
	private String mode;
	
	/**
	 * コンストラクタ
	 * @param string
	 */
	public PlanUnivDeleteBean(String individualid) {
		this.individualId = individualid;
	}
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		PreparedStatement ps = null;
		
		try{
			Query query = new Query("DELETE FROM examplanuniv WHERE individualid = ? ");
			
			if (univcd != null) {
				query.append("AND ");
				query.append("univcd = '"+ univcd +"' ");
			}
			
			if (facultycd != null) {
				query.append("AND ");
				query.append("facultycd = '"+ facultycd +"' ");
			}
			
			if (deptcd != null) {
				query.append("AND ");
				query.append("deptcd = '"+ deptcd +"' ");
			}
			
			if (mode != null) {
				query.append("AND ");
				query.append("entexammodecd = '"+ mode +"' ");
			}
			
			ps = conn.prepareStatement(query.toString());
			ps.setString(1, individualId);
			
			ps.executeUpdate();
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 大学コードを設定する
	 * @param univcd
	 */
	public void setUnivcd(String univcd) {
		this.univcd = univcd;
	}

	/**
	 * 学部コードを設定する
	 * @param facultycd
	 */
	public void setFacultycd(String facultycd) {
		this.facultycd = facultycd;
	}

	/**
	 * 学科コードを設定する
	 * @param deptcd
	 */
	public void setDeptcd(String deptcd) {
		this.deptcd = deptcd;
	}

	/**
	 * 入試形態コードを設定する
	 * @param mode
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}	

}

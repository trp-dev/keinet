package jp.co.fj.keinavi.servlets.profile;

import java.io.IOException;
import java.io.InvalidClassException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.ExamBean;
import jp.co.fj.keinavi.beans.com_set.CompClassBean;
import jp.co.fj.keinavi.beans.com_set.SubjectBean;
import jp.co.fj.keinavi.beans.help.OnepointNewBean;
import jp.co.fj.keinavi.beans.news.InformListBean;
import jp.co.fj.keinavi.beans.profile.CountingDivBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.profile.W008Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 *
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 * @author kawai
 *
 */
public class W008Servlet extends DefaultHttpServlet {

	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// ログイン情報
		LoginSession login = super.getLoginSession(request);
		// プロファイル
		Profile profile = super.getProfile(request);

		// アクションフォーム
		W008Form form = (W008Form)super.getActionForm(request,
			"jp.co.fj.keinavi.forms.profile.W008Form");

		// 専用トップページからの遷移なら対象年度と対象模試を更新
		if ("w008".equals(super.getBackward(request))) {
			profile.setTargetYear(form.getTargetYear());
			profile.setTargetExam(form.getTargetExam());
		}

		if ("w008".equals(getForward(request))) {

			// 2016/01/28 QQ)Nishiyama 大規模改修 ADD START
			//模試受付システム用情報作成
			createMoshiSys(request);
			// 2016/01/28 QQ)Nishiyama 大規模改修 ADD END

			// DBコネクション
			Connection con = null;
			try {
				con = super.getConnectionPool(request);
				con.setAutoCommit(false);

				// 転送元がf004なら模試セッションを作る
				if ("f004".equals(getBackward(request))) {
					ExamBean bean = new ExamBean();
					bean.setConnection(null, con);
					bean.setLoginSession(login);
					bean.setBundleCD(profile.getBundleCD());
					bean.execute();

					request
						.getSession(false)
						.setAttribute(ExamSession.SESSION_KEY, bean.getExamSession());
				}

				// 転送元がプロファイル選択画面以外なら対象模試を解決
				if (!"w002".equals(getBackward(request))) {
					// 模試セッション
					ExamSession examSession = getExamSession(request);
					// トップページの対象模試設定
					// デフォルトは最新模試
					if (profile.getTargetYear() == null && form.getTargetYear() == null) {
						ExamData data = KNUtil.getLatestExamData(examSession);
						profile.setTargetYear(data.getExamYear());
						profile.setTargetExam(data.getExamCD());
					// 年度を変更したときに対象模試データがなければリストの一番上
					} else if (form.getTargetYear() != null
							&& examSession.getExamData(form.getTargetYear(),
									form.getTargetExam()) == null) {
						ExamData exam = (ExamData) examSession.getExamList(
								form.getTargetYear()).get(0);
						profile.setTargetExam(exam.getExamCD());
					}
				}

				// 対象年度と対象模試の初期値をセット
				form.setTargetYear(profile.getTargetYear());
				form.setTargetExam(profile.getTargetExam());

				// 集計区分Bean
				if (login.isCountingDiv()) {
					CountingDivBean bean = new CountingDivBean();
					bean.setSchoolCD(login.getUserID());
					bean.setConnection(null, con);
					bean.execute();
					request.setAttribute("CountingDivBean", bean);
				}

				// 型Bean
				// Taglibでの評価用
				{
					SubjectBean bean = new SubjectBean(login);
					bean.setConnection(null, con);
					bean.setMode("1");
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.execute();
					request.setAttribute("TypeBean", bean);
				}

				// 科目Bean
				// Taglibでの評価用
				{
					SubjectBean bean = new SubjectBean(login);
					bean.setConnection(null, con);
					bean.setMode("2");
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.execute();
					request.setAttribute("CourseBean", bean);
				}

				// 比較対象クラスBean
				// Taglibでの評価用
				{
					CompClassBean bean = new CompClassBean();
					bean.setConnection(null, con);
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.setSchoolCD(login.getUserID());
					bean.execute();
					request.setAttribute("CompClassBean", bean);
				}

				//---------------------------------------------------
				// お知らせリスト作成
				//---------------------------------------------------
				InformListBean infbean = new InformListBean();
				infbean.setConnection(null, con);
				infbean.setUserID(login.getUserID()); 		// ユーザID
				infbean.setUserMode(login.getUserMode()); 	// ユーザーモード
				infbean.setDisplayDiv("0"); 				// 表示区分 "0":お知らせ
				infbean.execute();
				request.setAttribute("InformListBean", infbean);
				//---------------------------------------------------

				//---------------------------------------------------
				// Kei-Net注目情報リスト作成
				//---------------------------------------------------
				InformListBean keibean = new InformListBean();
				keibean.setConnection(null, con);
				keibean.setUserID(login.getUserID()); 		// ログインユーザID
				keibean.setUserMode(login.getUserMode()); 	// ユーザーモード
				keibean.setDisplayDiv("1"); 				// 表示区分 "1":Kei-Net注目情報
				keibean.execute();
				request.setAttribute("KeinetListBean", keibean);
				//---------------------------------------------------

				//---------------------------------------------------
				// 新着ワンポイントアドバイス
				//---------------------------------------------------
				OnepointNewBean onebean = new OnepointNewBean();
				onebean.setConnection(null, con);
				onebean.execute();
				request.setAttribute("OnepointBean", onebean);
				//---------------------------------------------------

				con.commit();

				request.setAttribute("form", form);
				super.forward(request, response, JSP_W008);

			} catch (InvalidClassException e) {
				super.rollback(con);

				KNServletException ke = new KNServletException(e.getMessage());
				ke.setErrorCode("10W002069999");
				ke.setErrorMessage("プロファイルのバージョンエラーが発生しました。プロファイルを作成し直してください。");
				throw ke;

			// 既知のエラーなら前画面へ
			} catch (KNServletException e) {
				super.rollback(con);
				super.setErrorMessage(request, e);
				super.forward(request, response, "/ProfileSelect");

			} catch (Exception e) {
				super.rollback(con);
				throw createServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}


		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

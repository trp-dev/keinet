package jp.co.fj.keinavi.servlets.helpdesk;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.helpdesk.CertificatesTargetCsvRegistBean;
import jp.co.fj.keinavi.beans.helpdesk.HelpDeskRegistBean;
import jp.co.fj.keinavi.forms.helpdesk.HD101Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 * ファイル登録画面サーブレット
 *
 * 2015.10.21	[新規作成]
 *
 *
 * @author Hiroyuki Nishiyama - QuiQsoft
 * @version 1.0
 *
 */
public class HD107Servlet extends DefaultHttpServlet {

	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// セッション
		HttpSession session = request.getSession();
		HD101Form hd101Form = null;
		HD101Form hd101FormTemp = null;

		CertificatesTargetCsvRegistBean csvRegistBeen = null;
		Connection con = null; // コネクション

		// requestから取得する
		try {
			// ※子画面を表示する際に小細工をしているので
			// HD107FormではGET出来ない
			hd101FormTemp = (HD101Form)super.getActionForm(request,
					"jp.co.fj.keinavi.forms.helpdesk.HD101Form");
		} catch (Exception e) {
			throw new ServletException(e);
		}

		// 「登録して閉じる」
		if(super.getBackward(request) == null) {

			//*****************************************
			// ファイルの内容をテンポラリに登録する
			//*****************************************
			csvRegistBeen = new CertificatesTargetCsvRegistBean();

			try {
				con = super.getConnectionPool(request); 	// コネクション取得

				csvRegistBeen.setConnection(null, con);
				csvRegistBeen.setHD101Form(hd101FormTemp);
				csvRegistBeen.execute();

			} catch (Exception e) {
				throw new ServletException(e.toString());
			} finally {
				super.releaseConnectionPool(request, con); // コネクション解放
			}

			hd101Form = (HD101Form) session.getAttribute("hd101Form");

			// 処理結果を取得
			HelpDeskRegistBean hdrBean = csvRegistBeen.getHdrBean();
			hdrBean.setErrPattern("0");
			session.setAttribute("HelpDeskRegistBean", hdrBean);

			// 正常
			if (hdrBean.getErrorList().size() == 0) {
				// ファイル名を元画面に渡す
				hd101Form.setFileName(hd101FormTemp.getFileName());
			} else {
				hd101Form.setFileName("");
			}
			session.setAttribute("hd101Form", hd101Form);

			// 画面を閉じる
			super.forward(request, response, JSP_CLOSE);

		} else {

			// フォームをセッションに保持
			session.setAttribute("hd101Form", hd101FormTemp);
			// 子画面を表示
			super.forward(request, response, JSP_HD107);

		}

	}

}

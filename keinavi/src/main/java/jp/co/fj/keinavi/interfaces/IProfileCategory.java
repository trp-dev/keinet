/*
 * 作成日: 2004/07/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.interfaces;

/**
 * プロファイルのカテゴリIDを定義するインターフェース
 *
 * 2005.02.09 Yoshimoto KAWAI - Totec
 *            テキスト出力（模試別成績データ・新形式）を追加
 *
 * @author kawai
 */
public interface IProfileCategory {

	// --------------------------------------------------------------------------------
	/** 共通項目設定 */
	String CM = "010000";
	// --------------------------------------------------------------------------------
	/** 校内成績分析・全国総合成績 */
	String SCORE_TOTAL = "020100";
	// --------------------------------------------------------------------------------
	/** 校内成績分析・校内成績・成績概況 */
	String S_SCHOOL_SCORE = "020201";
	/** 校内成績分析・校内成績・偏差値分布 */
	String S_SCHOOL_DEV = "020202";
	/** 校内成績分析・校内成績・設問別成績 */
	String S_SCHOOL_QUE = "020203";
	// --------------------------------------------------------------------------------
	/** 校内成績分析・過年度比較・成績概況 */
	String S_PREV_SCORE = "020301";
	/** 校内成績分析・過年度比較・偏差値分布 */
	String S_PREV_DEV = "020302";
	/** 校内成績分析・過年度比較・志望大学評価別人数 */
	String S_PREV_UNIV = "020303";
	// --------------------------------------------------------------------------------
	/** 校内成績分析・クラス比較・成績概況 */
	String S_CLASS_SCORE = "020401";
	/** 校内成績分析・クラス比較・偏差値分布 */
	String S_CLASS_DEV = "020402";
	/** 校内成績分析・クラス比較・設問別成績 */
	String S_CLASS_QUE = "020403";
	/** 校内成績分析・クラス比較・志望大学評価別人数 */
	String S_CLASS_UNIV = "020404";
	// --------------------------------------------------------------------------------
	/** 校内成績分析・他校比較・成績概況 */
	String S_OTHER_SCORE = "020501";
	/** 校内成績分析・他校比較・偏差値分布 */
	String S_OTHER_DEV = "020502";
	/** 校内成績分析・他校比較・設問別成績 */
	String S_OTHER_QUE = "020503";
	/** 校内成績分析・他校比較・志望大学評価別人数 */
	String S_OTHER_UNIV = "020504";
	// --------------------------------------------------------------------------------
	/** 校内成績分析・過回比較・成績概況 */
	String S_PAST_SCORE = "020601";
	/** 校内成績分析・過回比較・偏差値分布 */
	String S_PAST_DEV = "020602";
	// --------------------------------------------------------------------------------
	/** クラス成績分析・クラス成績概況・偏差値分布 */
	String C_COND_DEV = "030101";
	/** クラス成績分析・クラス成績概況・個人成績 */
	String C_COND_IND = "030102";
	/** クラス成績分析・クラス成績概況・個人成績推移 */
	String C_COND_TRANS = "030103";
	/** クラス成績分析・クラス成績概況・設問別個人成績 */
	String C_COND_QUE = "030104";
	/** クラス成績分析・クラス成績概況・志望大学別個人評価 */
	String C_COND_UNIV = "030105";
	// --------------------------------------------------------------------------------
	/** クラス成績分析・クラス比較・成績概況 */
	String C_COMP_SCORE = "030201";
	/** クラス成績分析・クラス比較・偏差値分布 */
	String C_COMP_DEV = "030202";
	/** クラス成績分析・クラス比較・設問別成績 */
	String C_COMP_QUE = "030203";
	/** クラス成績分析・クラス比較・志望大学評価別人数	 */
	String C_COMP_UNIV = "030204";
	// --------------------------------------------------------------------------------
	/** 個人成績分析・個人成績分析共通 */
	String I_COMMON = "040100";
	/** 個人成績分析・成績分析・成績分析共通 */
	String I_SCORE_COMMON = "040201";
	/** 個人成績分析・成績分析・個人成績表 */
	String I_SCORE_SHEET = "040202";
	/** 個人成績分析・成績分析・バランスチャート */
	String I_SCORE_BC = "040203";
	/** 個人成績分析・成績分析・バランスチャート（合格者平均） */
	String I_SCORE_BC_AVG = "040204";
	/** 個人成績分析・成績分析・成績推移グラフ */
	String I_SCORE_TRANS = "040205";
	/** 個人成績分析・成績分析・科目別成績推移グラフ */
	String I_SCORE_COURSE_TRANS = "040206";
	/** 個人成績分析・成績分析・設問別成績 */
	String I_SCORE_QUE = "040207";
	// --------------------------------------------------------------------------------
	/** 個人成績分析・志望大学判定・こだわり判定 */
	String I_UNIV_JUDGE = "040301";
	/** 個人成績分析・志望大学判定・判定結果一覧 */
	String I_UNIV_RESULTS = "040302";
	// --------------------------------------------------------------------------------
	/** 個人成績分析・個人手帳 */
	String I_PERSONAL_NOTE = "040400";
	// --------------------------------------------------------------------------------
	/** 個人成績分析・かんたん印刷 */
	String I_EASY_PRINT = "040500";
	// --------------------------------------------------------------------------------
	/** テキスト出力・テキスト出力共通 */
	String T_COMMON = "050100";
	// --------------------------------------------------------------------------------
	/** テキスト出力・集計データ・成績概況 */
	String T_COUNTING_SCORE = "050201";
	/** テキスト出力・集計データ・偏差値分布 */
	String T_COUNTING_DEV = "050202";
	/** テキスト出力・集計データ・設問別成績 */
	String T_COUNTING_QUE = "050203";
	/** テキスト出力・集計データ・志望大学評価別人数 */
	String T_COUNTING_UNIV = "050204";
	// --------------------------------------------------------------------------------
	/** テキスト出力・個人データ・模試別成績データ（旧形式） */
	String T_IND_EXAM = "050301";
	/** テキスト出力・個人データ・設問別成績 */
	String T_IND_QUE = "050302";
	/** テキスト出力・個人データ・模試別成績データ（新形式） */
	String T_IND_EXAM2 = "050303";
	// 2019/09/11 QQ)Tanioka 共通テスト対応 ADD START
	/** テキスト出力・個人データ・（記述系模試）学力要素別成績データ */
	String T_IND_DESCEXAM = "050304";
	// 2019/09/11 QQ)Tanioka 共通テスト対応 ADD END
	// --------------------------------------------------------------------------------

}

/**
 * 校内成績分析−他校比較　偏差値分布-度数分布グラフ（人数）
 * 	Excelファイル編集
 * 作成日: 2004/08/11
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.school;

import java.io.*;
import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.excel.cm.*;
import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.*;

public class S42_07 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	final private int	intMaxSheetSr	= 40;			// 最大シート数	
	final private String	strArea			= "A1:AQ61";	// 印刷範囲	
	final private String	masterfile0		= "S42_07";		// ファイル名
	final private String	masterfile1		= "NS42_07";	// ファイル名
	private String	masterfile		= "";					// ファイル名
	/*
	 * 	Excel編集メイン
	 * 		S42Item s42Item: データクラス
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
 	public int s42_07EditExcel(S42Item s42Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("S42_07","S42_07帳票作成開始","");
		
		//テンプレートの決定
		if (s42Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			
			// データセット
			ArrayList	s42List			= s42Item.getS42List();
			Iterator	itr				= s42List.iterator();
			int		col				= 0;	// 列
			int 		setCol			= -1;	// *セット用
			int 		ninzu			= 0;	// *作成用
			int		gakkoCnt		= 0;	// 学校カウンター
			int		maxSheetIndex	= 0;	// シートカウンター
			int		allGakkoCnt		= 0;	// 全学校カウンター
			int		maxGakkoCnt		= 0;	// 学校カウンター
			int		intBookCngCount	= 0;	// ブックカウンター
			int		kmkCd			= 0;	// 科目コード保持
			boolean	kataFlg			= true;// false:改行済(型・科目名変更済)　
			boolean	bolGakkouFlg	= true;// true:学校情報表示　（学校情報表示フラグ）
			boolean	bolFirstDataFlg	= true;// true:List.Next()を実行
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
//add 2004/10/27 T.Sakai データ0件対応
			int		dispGakkoFlgCnt		= 0;		// グラフ表示フラグカウンタ
//add end

			FileInputStream	fin			= null;
			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			// 基本ファイルを読込む
			S42ListBean s42ListBean = new S42ListBean();

			while( itr.hasNext() ) {
				s42ListBean = (S42ListBean)itr.next();
				// グラフ表示フラグ判定
				if ( s42ListBean.getIntDispKmkFlg()==1 ) {
	
					// 基本ファイルを読み込む
					S42GakkoListBean	s42GakkoListBean	= new S42GakkoListBean();
	
					//データの保持
					ArrayList	s42GakkoList		= s42ListBean.getS42GakkoList();
					Iterator	itrS42Gakko		= s42GakkoList.iterator();

					// 表示学校数取得
					maxGakkoCnt=0;
					while(itrS42Gakko.hasNext()){
						s42GakkoListBean	= (S42GakkoListBean) itrS42Gakko.next();
						if(s42GakkoListBean.getIntDispGakkoFlg()==1){
							maxGakkoCnt++;
						}
					}
					itrS42Gakko		= s42GakkoList.iterator();
	
					// 型・科目の最初のデータ(=自校データ)
					bolFirstDataFlg=true;

					//　学校データセット
					gakkoCnt = 0;
					allGakkoCnt = 0;
					while(itrS42Gakko.hasNext()){
	
						// WorkBook作成
						if( bolBookCngFlg == true ){
							// マスタExcel読み込み
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook==null ){
								return errfread;
							}
							bolSheetCngFlg	= true;
							bolBookCngFlg	= false;
							maxSheetIndex	= 0;
						}
	
						// シート作成(初期値格納)
						if(bolSheetCngFlg){
							// データセットするシートの選択
							workSheet = workbook.getSheet(Integer.toString(maxSheetIndex+1));
							maxSheetIndex++;
	
							// ヘッダ右側に帳票作成日時を表示する
							cm.setHeader(workbook, workSheet);
	
							// セキュリティスタンプセット
							String secuFlg = cm.setSecurity( workbook, workSheet, s42Item.getIntSecuFlg() ,41 ,42 );
							workCell = cm.setCell( workSheet, workRow, workCell, 0, 41 );
							workCell.setCellValue(secuFlg);
	
							// 学校名セット
							workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
							workCell.setCellValue( "学校名　　　：" + cm.toString(s42Item.getStrGakkomei()) );
	
							// 対象模試セット
							String moshi =cm.setTaisyouMoshi( s42Item.getStrMshDate() );	// 模試月取得
							workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
							workCell.setCellValue( "対象模試　　：" + cm.toString(s42Item.getStrMshmei()) + moshi);
	
							// 型名・配点セット
							String haiten = "";
							if ( !cm.toString(s42ListBean.getStrHaitenKmk()).equals("") ) {
								haiten = "（" + s42ListBean.getStrHaitenKmk() + "）";
							}
							workCell = cm.setCell( workSheet, workRow, workCell, 5, 0 );
							workCell.setCellValue( "型・科目：" + cm.toString(s42ListBean.getStrKmkmei()) + haiten );
							kmkCd=Integer.parseInt(s42ListBean.getStrKmkCd());
							
//add 2004/10/27 T.Sakai スペース対応
  							if (s42Item.getIntShubetsuFlg() == 1){
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
								workCell.setCellValue( "〜 19" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
								workCell.setCellValue( " 20〜\n 29" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
								workCell.setCellValue( " 30〜\n 39" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
								workCell.setCellValue( " 40〜\n 49" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
								workCell.setCellValue( " 50〜\n 59" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
								workCell.setCellValue( " 60〜\n 69" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
								workCell.setCellValue( " 70〜\n 79" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
								workCell.setCellValue( " 80〜\n 89" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
								workCell.setCellValue( " 90〜\n 99" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
								workCell.setCellValue( "100〜\n109" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
								workCell.setCellValue( "110〜\n119" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
								workCell.setCellValue( "120〜\n129" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
								workCell.setCellValue( "130〜\n139" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
								workCell.setCellValue( "140〜\n149" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
								workCell.setCellValue( "150〜\n159" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
								workCell.setCellValue( "160〜\n169" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
								workCell.setCellValue( "170〜\n179" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
								workCell.setCellValue( "180〜\n189" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
								workCell.setCellValue( "190〜" );
  							} else{
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
								workCell.setCellValue( "〜32.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
								workCell.setCellValue( "32.5〜\n34.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
								workCell.setCellValue( "35.0〜\n37.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
								workCell.setCellValue( "37.5〜\n39.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
								workCell.setCellValue( "40.0〜\n42.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
								workCell.setCellValue( "42.5〜\n44.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
								workCell.setCellValue( "45.0〜\n47.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
								workCell.setCellValue( "47.5〜\n49.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
								workCell.setCellValue( "50.0〜\n52.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
								workCell.setCellValue( "52.5〜\n54.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
								workCell.setCellValue( "55.0〜\n57.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
								workCell.setCellValue( "57.5〜\n59.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
								workCell.setCellValue( "60.0〜\n62.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
								workCell.setCellValue( "62.5〜\n64.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
								workCell.setCellValue( "65.0〜\n67.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
								workCell.setCellValue( "67.5〜\n69.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
								workCell.setCellValue( "70.0〜\n72.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
								workCell.setCellValue( "72.5〜\n74.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
								workCell.setCellValue( "75.0〜" );
  							}
//add end
							
							bolSheetCngFlg=false;
							bolGakkouFlg=true;
						}
	
						//　学校フラグ判定(trueの時は学校情報表示：falseの時は学校情報表示)
						if(bolGakkouFlg){
							s42GakkoListBean	= (S42GakkoListBean) s42GakkoList.get(0);
							bolGakkouFlg=false;

							// 学校情報読み飛ばし
							if(bolFirstDataFlg){
								s42GakkoListBean	= (S42GakkoListBean) itrS42Gakko.next();
								bolFirstDataFlg=false;
							}
						}else{
							// 学校情報取得
							s42GakkoListBean	= (S42GakkoListBean) itrS42Gakko.next();
						}

						// データ表示フラグ判定
						if ( s42GakkoListBean.getIntDispGakkoFlg()==1 ) {
//add 2004/10/27 T.Sakai データ0件対応
							dispGakkoFlgCnt++;
//add end
							S42BnpListBean s42BnpListBean = new S42BnpListBean();
							ArrayList	s42BnpList	= s42GakkoListBean.getS42BnpList();
							Iterator	itrS42Bnp	= s42BnpList.iterator();
		
							// 偏差値データセット
							col=0;
							while(itrS42Bnp.hasNext()){
								s42BnpListBean = (S42BnpListBean) itrS42Bnp.next();
		
								if(col==0){
									// 高校名セット
									workCell = cm.setCell( workSheet, workRow, workCell, 51+gakkoCnt, 1 );
									workCell.setCellValue( s42GakkoListBean.getStrGakkomei() );
									workCell = cm.setCell( workSheet, workRow, workCell, 56+gakkoCnt, 1 );
									workCell.setCellValue( s42GakkoListBean.getStrGakkomei() );
		
									// 合計人数セット
									if(s42GakkoListBean.getIntNinzu()!=-999){
										workCell = cm.setCell( workSheet, workRow, workCell, 51+gakkoCnt, 40 );
										workCell.setCellValue( s42GakkoListBean.getIntNinzu() );
									}
		
									// 平均点セット
									if(s42GakkoListBean.getFloHeikin()!=-999.0){
										workCell = cm.setCell( workSheet, workRow, workCell, 51+gakkoCnt, 41 );
										workCell.setCellValue( s42GakkoListBean.getFloHeikin() );
									}
		
									// 平均偏差値セット
									if(s42GakkoListBean.getFloHensa()!=-999.0){
										workCell = cm.setCell( workSheet, workRow, workCell, 51+gakkoCnt, 42 );
										workCell.setCellValue( s42GakkoListBean.getFloHensa() );
									}
								}
		
								// 人数セット
								if(s42BnpListBean.getIntNinzu()!=-999){
									//2004.10.19 update
//									workCell = cm.setCell( workSheet, workRow, workCell, 51+gakkoCnt, 3+2*col );
									workCell = cm.setCell( workSheet, workRow, workCell, 51+gakkoCnt, 39-2*col );
									//update end
									workCell.setCellValue( s42BnpListBean.getIntNinzu() );
		
									// *セット準備
									if( s42BnpListBean.getIntNinzu() > ninzu ){
										ninzu = s42BnpListBean.getIntNinzu();
										setCol = col;
									}
								}
		
								// 構成比セット
								if(s42BnpListBean.getFloKoseihi()!=-999.0){
									//2004.10.19 update
//									workCell = cm.setCell( workSheet, workRow, workCell, 56+gakkoCnt, 3+2*col );
									workCell = cm.setCell( workSheet, workRow, workCell, 56+gakkoCnt, 39-2*col );
									workCell.setCellValue( s42BnpListBean.getFloKoseihi() );
									//update end
								}
								
								col++;
		
								// *セット
								if(col >=19){
									if(setCol!=-1){
										//2004.10.19 update
//										workCell = cm.setCell( workSheet, workRow, workCell,  51+gakkoCnt, 2+2*setCol );
										workCell = cm.setCell( workSheet, workRow, workCell,  51+gakkoCnt, 38-2*setCol );
										//update end
										workCell.setCellValue("*");
									}
									ninzu=0;
									setCol=-1;
									break;
								}
		
							}

							gakkoCnt++;
							allGakkoCnt++;

							// 以降に学校情報がなければループ脱出
							if(maxGakkoCnt==allGakkoCnt){
								break;
							}
	
							// 学校情報が５件になったら
							if(gakkoCnt>=5){
								gakkoCnt=0;
								allGakkoCnt--;
								bolSheetCngFlg=true;
							}

							// ファイルを閉じる処理
							if(bolSheetCngFlg){
								if((maxSheetIndex >= intMaxSheetSr)&&(itrS42Gakko.hasNext())){
									bolBookCngFlg = true;
								}
		
						
								if( bolBookCngFlg == true){
									boolean bolRet = false;
									// Excelファイル保存
									if(intBookCngCount==0){
										if(itr.hasNext() == true){
											bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
										}
										else{
											bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
										}
									}else{
										bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
									}
		
									if( bolRet == false ){
										return errfwrite;
									}
									intBookCngCount++;
								}
							}
						}

						// 学校が非表示指定(表示フラグ≠１)だった場合（エラー）
						bolGakkouFlg=false;
					}


					// ファイルを閉じる処理
					if( itr.hasNext() ){
						gakkoCnt=0;
						bolSheetCngFlg=true;
	
						if(maxSheetIndex >= intMaxSheetSr){
							bolBookCngFlg = true;
						}
	
					
						if( bolBookCngFlg == true){
	
							boolean bolRet = false;
							// Excelファイル保存
							if(intBookCngCount==0){
								if(itr.hasNext() == true){
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
								}
								else{
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
								}
							}else{
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
							}
	
							if( bolRet == false ){
								return errfwrite;
							}
							intBookCngCount++;
						}
					}
				}

			}
//add 2004/10/27 T.Sakai データ0件対応
			if ( dispGakkoFlgCnt==0 ) {
				// マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				bolBookCngFlg	= false;
				maxSheetIndex	= 0;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet( Integer.toString(maxSheetIndex+1) );
				maxSheetIndex++;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);

				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s42Item.getIntSecuFlg() ,41 ,42 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 41 );
				workCell.setCellValue(secuFlg);
	
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　　　：" + cm.toString(s42Item.getStrGakkomei()) );
	
				// 対象模試セット
				String moshi =cm.setTaisyouMoshi( s42Item.getStrMshDate() );	// 模試月取得
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "対象模試　　：" + cm.toString(s42Item.getStrMshmei()) + moshi);
				
//add 2004/10/27 T.Sakai スペース対応
 	 			if (s42Item.getIntShubetsuFlg() == 1){
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
					workCell.setCellValue( "〜 19" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
					workCell.setCellValue( " 20〜\n 29" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
					workCell.setCellValue( " 30〜\n 39" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
					workCell.setCellValue( " 40〜\n 49" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
					workCell.setCellValue( " 50〜\n 59" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
					workCell.setCellValue( " 60〜\n 69" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
					workCell.setCellValue( " 70〜\n 79" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
					workCell.setCellValue( " 80〜\n 89" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
					workCell.setCellValue( " 90〜\n 99" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
					workCell.setCellValue( "100〜\n109" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
					workCell.setCellValue( "110〜\n119" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
					workCell.setCellValue( "120〜\n129" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
					workCell.setCellValue( "130〜\n139" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
					workCell.setCellValue( "140〜\n149" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
					workCell.setCellValue( "150〜\n159" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
					workCell.setCellValue( "160〜\n169" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
					workCell.setCellValue( "170〜\n179" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
					workCell.setCellValue( "180〜\n189" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
					workCell.setCellValue( "190〜" );
 	 			} else{
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
					workCell.setCellValue( "〜32.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
					workCell.setCellValue( "32.5〜\n34.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
					workCell.setCellValue( "35.0〜\n37.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
					workCell.setCellValue( "37.5〜\n39.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
					workCell.setCellValue( "40.0〜\n42.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
					workCell.setCellValue( "42.5〜\n44.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
					workCell.setCellValue( "45.0〜\n47.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
					workCell.setCellValue( "47.5〜\n49.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
					workCell.setCellValue( "50.0〜\n52.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
					workCell.setCellValue( "52.5〜\n54.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
					workCell.setCellValue( "55.0〜\n57.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
					workCell.setCellValue( "57.5〜\n59.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
					workCell.setCellValue( "60.0〜\n62.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
					workCell.setCellValue( "62.5〜\n64.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
					workCell.setCellValue( "65.0〜\n67.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
					workCell.setCellValue( "67.5〜\n69.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
					workCell.setCellValue( "70.0〜\n72.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
					workCell.setCellValue( "72.5〜\n74.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
					workCell.setCellValue( "75.0〜" );
 	 			}
//add end
			}
//add end
			// ファイルを閉じる処理
			if( bolBookCngFlg == false){

				boolean bolRet = false;
				// Excelファイル保存
				if(intBookCngCount!=0){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}
			
		} catch(Exception e) {
			log.Err("S42_07","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S42_07","S42_07帳票作成終了","");
		return noerror;
	}

}
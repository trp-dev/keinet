package jp.co.fj.keinavi.util;

/**
 *
 * 入力値の検査メソッドを定義するクラス
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class KNValidator {

	protected KNValidator() {
	}

	/**
	 * 利用者IDとして有効であるかどうか
	 * 
	 * @param loginId
	 * @return
	 */
	public static boolean isLoginId(final String loginId) {
		
		if (loginId == null) return false;
		
		return loginId.matches("^[0-9a-zA-Z]{1,8}$");
	}
	
	/**
	 * 利用者パスワードとして有効であるかどうか
	 * 
	 * @param loginId
	 * @return
	 */
	public static boolean isLoginPassword(final String password) {
		
		if (password == null) return false;
		
		return password.matches("^[0-9a-zA-Z]{4,8}$");
	}
	
	/**
	 * Kei-Navi利用権限フラグとして有効であるかどうか
	 * 
	 * @param knFlag
	 * @return
	 */
	public static boolean isKnFunctionFlag(final short knFlag) {
		
		return knFlag > 0 && knFlag <= 3;
	}
	
	/**
	 * 校内成績処理利用権限フラグとして有効であるかどうか
	 * 
	 * @param scFlag
	 * @return
	 */
	public static boolean isScFunctionFlag(final short scFlag) {
		
		return scFlag > 0 && scFlag <= 3;
	}
	
	/**
	 * 入試結果調査利用権限フラグとして有効であるかどうか
	 * 
	 * @param eeFlag
	 * @return
	 */
	public static boolean isEeFunctionFlag(final short eeFlag) {
		
		return eeFlag == 1 || eeFlag == 3;
	}
	
	/**
	 * 答案閲覧利用権限フラグとして有効であるかどうか
	 * 
	 * @param abFlag
	 * @return
	 */
	public static boolean isAbFunctionFlag(final short abFlag) {
		
		return abFlag == 1 || abFlag == 3;
	}
	
	
	
}

/*
 * 作成日: 2004/09/08
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.business;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.beans.sheet.NoQuestionException;
import jp.co.fj.keinavi.data.sheet.SheetSchoolData;
import jp.co.fj.keinavi.excel.business.B43;
import jp.co.fj.keinavi.excel.data.business.B43GakkoListBean;
import jp.co.fj.keinavi.excel.data.business.B43Item;
import jp.co.fj.keinavi.excel.data.business.B43ListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

/**
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * 2009.10.13   Fujito URAKAWA - Totec
 *              「全体成績」追加対応 
 * 
 * @author kawai
 *
 */
public class B43SheetBean extends AbstractSheetBean {

	// データクラス
	private final B43Item	data = new B43Item();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		String[] code = super.getSubjectCDArray(); // 型・科目設定値
		List compSchoolList = super.getCompSchoolList(); // 比較対象高校
		
		String tmp_kmkcd = "";	// 科目コードチェック用変数

		// 設問別成績存在チェック
		if (hasAllNoQuestionSubject(code)) {
			throw new NoQuestionException();
		}
		
		// データを詰める
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntHyouFlg(1); // 表フラグ
		data.setIntBestFlg(super.getIntFlag(IProfileItem.IND_UPPER_MARK)); // 上位校の＊表示フラグ
		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		PreparedStatement ps6 = null;
		PreparedStatement ps7 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		try {
			// データリスト
			{
				Query query = QueryLoader.getInstance().load("s33_1");
				query.setStringArray(1, code); // 型・科目コード

				ps1 = conn.prepareStatement(query.toString());
				ps1.setString(1, exam.getExamYear()); // 模試年度
				ps1.setString(2, exam.getExamCD()); // 模試コード

			}
			// 設問別人数リスト（全国）
			ps2 = conn.prepareStatement(QueryLoader.getInstance().load("b43_1").toString());
			ps2.setString(3, exam.getExamYear()); // 模試年度
			ps2.setString(4, exam.getExamCD()); // 模試コード

			// 設問別人数リスト（都道府県）
			ps3 = conn.prepareStatement(QueryLoader.getInstance().load("b43_2").toString());
			ps3.setString(1, exam.getExamYear()); // 模試年度
			ps3.setString(2, exam.getExamCD()); // 模試コード

			// 設問別人数リスト（学校）
			{
				Query query = QueryLoader.getInstance().load("b43_3");
				super.outDate2InDate(query); // データ開放日書き換え

				ps4 = conn.prepareStatement(query.toString());
				ps4.setString(1, exam.getDataOpenDate()); // データ開放日
				ps4.setString(2, exam.getDataOpenDate()); // データ開放日
				ps4.setString(3, exam.getExamYear()); // 模試年度
				ps4.setString(4, exam.getExamCD()); // 模試コード
			}
			
			// 設問別科目人数リスト（全国）
			{
				ps5 = conn.prepareStatement(QueryLoader.getInstance().load("s43_4").toString());
				ps5.setString(1, exam.getExamYear());
				ps5.setString(2, exam.getExamCD());
				ps5.setString(4, exam.getExamYear());
				ps5.setString(5, exam.getExamCD());
			}
			// 設問別科目人数リスト（都道府県）
			{
				ps6 = conn.prepareStatement(QueryLoader.getInstance().load("s43_5").toString());
				ps6.setString(1, exam.getExamYear());
				ps6.setString(2, exam.getExamCD());
			}
			// 設問別科目人数リスト（学校）
			{
				Query query = QueryLoader.getInstance().load("s43_7");
				super.outDate2InDate(query);
				
				ps7 = conn.prepareStatement(query.toString());
				ps7.setString(1, exam.getExamYear());
				ps7.setString(2, exam.getExamCD());
				ps7.setString(5, exam.getExamYear());
				ps7.setString(6, exam.getExamCD());

			}
			
			rs1 = ps1.executeQuery();
			
			while (rs1.next()) {
				
				// 科目コードが異なる場合のみ、全体成績データをセットする
				if (!tmp_kmkcd.equals(rs1.getString(1))) {
					
					putTotalSubjectData(rs1, ps5, ps6, ps7, compSchoolList);
					
				}
				
				B43ListBean bean = new B43ListBean();
				bean.setStrKmkCd(rs1.getString(1));
				bean.setStrKmkmei(rs1.getString(2));
				bean.setStrHaitenKmk(rs1.getString(3));
				bean.setStrSetsumonNo(rs1.getString(4));
				bean.setStrSetsuMei1(rs1.getString(5));
				bean.setStrSetsuHaiten(rs1.getString(6));
				data.getB43List().add(bean);				

				// 科目コード
				ps2.setString(1, bean.getStrKmkCd());
				ps3.setString(3, bean.getStrKmkCd());
				ps4.setString(5, bean.getStrKmkCd());
				// 設問番号
				ps2.setString(2, rs1.getString(7));
				ps3.setString(4, rs1.getString(7));
				ps4.setString(6, rs1.getString(7));

				// 学校分繰り返す
				Iterator ite = compSchoolList.iterator();
				while (ite.hasNext()) {
					SheetSchoolData school = (SheetSchoolData) ite.next();
	
					switch (school.getSchoolTypeCode()) {
						// 全国
						case 1:
							rs2 = ps2.executeQuery();	
							break;
						
						// 都道府県
						case 2:
							ps3.setString(5, school.getPrefCD()); // 県コード
							rs2 = ps3.executeQuery();					
							break;
						
						// 高校
						case 3:
							ps4.setString(7, school.getSchoolCD()); // 一括コード
							rs2 = ps4.executeQuery();
							break;
					}

					// 詰める					
					if (rs2.next()) {
						B43GakkoListBean g = new B43GakkoListBean();
						g.setStrGakkomei(rs2.getString(1));
						g.setIntNinzu(rs2.getInt(2));
						g.setFloTokuritsu(rs2.getFloat(3));
						bean.getB43GakkoList().add(g);								
					}
					
					rs2.close();
				}
				
				// 処理中の科目コードをセット
				tmp_kmkcd = rs1.getString(1);
				
			}
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(ps3);
			DbUtils.closeQuietly(ps4);
			DbUtils.closeQuietly(ps5);
			DbUtils.closeQuietly(ps6);
			DbUtils.closeQuietly(ps7);
		}
	}

	/**
	 * 設問別成績の先頭に「全体成績」データをセットする
	 * @param rs1
	 * @param ps5
	 * @param ps6
	 * @param ps7
	 * @param compSchoolList
	 * @throws SQLException 
	 */
	private void putTotalSubjectData(ResultSet rs1, PreparedStatement ps5,
			PreparedStatement ps6, PreparedStatement ps7, List compSchoolList) throws SQLException {
		
		B43ListBean bean = new B43ListBean();
		bean.setStrKmkCd(rs1.getString(1));
		bean.setStrKmkmei(rs1.getString(2));
		bean.setStrHaitenKmk(rs1.getString(3));
		bean.setStrSetsumonNo("");						// 設問Noに「""」をセット
		bean.setStrSetsuMei1("全体成績");					// 設問名に「"全体成績"」をセット
		bean.setStrSetsuHaiten(rs1.getString(3));		// 配点に科目配点をセット
		data.getB43List().add(bean);
		
		ResultSet rs2 = null; // 他校
		
		try {
			// 科目コード
			ps5.setString(3, rs1.getString(1));	// 全国
			ps6.setString(3, rs1.getString(1));	// 県
			ps7.setString(3, rs1.getString(1));	// 他校

			// 他校の科目別成績をセット
			{
				Iterator ite = compSchoolList.iterator();
				while (ite.hasNext()) {
					SheetSchoolData school = (SheetSchoolData) ite.next();
		
					switch (school.getSchoolTypeCode()) {
						// 全国
						case 1:
							rs2 = ps5.executeQuery();
							break;
							
						// 県
						case 2:
							ps6.setString(4, school.getPrefCD()); // 県コード
							rs2 = ps6.executeQuery();
							break;
						
						// 高校
						case 3:
							ps7.setString(4, school.getSchoolCD()); // 一括コード
							rs2 = ps7.executeQuery();
							break;
					}
	
					if (rs2.next()) {
						B43GakkoListBean g = new B43GakkoListBean();
						g.setStrGakkomei(rs2.getString(1));
						g.setIntNinzu(rs2.getInt(2));
						g.setFloTokuritsu(rs2.getFloat(3));
						bean.getB43GakkoList().add(g);
					}
					rs2.close();
				}
			}
		}
		finally {
			DbUtils.closeQuietly(rs2);
		}
		
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("B43_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_OTHER_QUE;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new B43()
			.b43(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
	}

}

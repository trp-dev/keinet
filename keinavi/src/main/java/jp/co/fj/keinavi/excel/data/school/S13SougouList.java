package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;

/**
 * �Z�����сi�ݖ�ʐ��сj ���ʃe�X�g����L�q���]���ʐl�� �����]���^�Z���E�S�� �f�[�^���X�g
 * �쐬��: 2019/09/11
 * @author	QQ)Tanouchi
 */
public class S13SougouList {

	//�����]���^�Z���E�S��
	//�Z��-A�]�� �l���Z�b�g
	private int S_AHyoukaNin = 0;
	//�Z��-A�]�� �������Z�b�g
	private float S_AHyoukaWari = 0;
	//�Z��-B�]�� �l���Z�b�g
	private int S_BHyoukaNin = 0;
	//�Z��-B�]�� �������Z�b�g
	private float S_BHyoukaWari = 0;
	//�Z��-C�]�� �l���Z�b�g
	private int S_CHyoukaNin = 0;
	//�Z��-C�]�� �������Z�b�g
	private float S_CHyoukaWari = 0;
	//�Z��-D�]�� �l���Z�b�g
	private int S_DHyoukaNin = 0;
	//�Z��-D�]�� �������Z�b�g
	private float S_DHyoukaWari = 0;
	//�Z��-E�]�� �l���Z�b�g
	private int S_EHyoukaNin = 0;
	//�Z��-E�]�� �������Z�b�g
	private float S_EHyoukaWari = 0;
	//�S��-A�]�� �l���Z�b�g
	private int A_AHyoukaNin = 0;
	//�S��-A�]�� �������Z�b�g
	private float A_AHyoukaWari = 0;
	//�S��-B�]�� �l���Z�b�g
	private int A_BHyoukaNin = 0;
	//�S��-B�]�� �������Z�b�g
	private float A_BHyoukaWari = 0;
	//�S��-C�]�� �l���Z�b�g
	private int A_CHyoukaNin = 0;
	//�S��-C�]�� �������Z�b�g
	private float A_CHyoukaWari = 0;
	//�S��-D�]�� �l���Z�b�g
	private int A_DHyoukaNin = 0;
	//�S��-D�]�� �������Z�b�g
	private float A_DHyoukaWari = 0;
	//�S��-E�]�� �l���Z�b�g
	private int A_EHyoukaNin = 0;
	//�S��-E�]�� �������Z�b�g
	private float A_EHyoukaWari = 0;

	//���ʕ\�f�[�^���X�g
	private ArrayList S13SougouList = new ArrayList();

	/*----------*/
	/* Set      */
	/*----------*/

	public int getS_AHyoukaNin() {
		return S_AHyoukaNin;
	}
	public float getS_AHyoukaWari() {
		return S_AHyoukaWari;
	}
	public int getS_BHyoukaNin() {
		return S_BHyoukaNin;
	}
	public float getS_BHyoukaWari() {
		return S_BHyoukaWari;
	}
	public int getS_CHyoukaNin() {
		return S_CHyoukaNin;
	}
	public float getS_CHyoukaWari() {
		return S_CHyoukaWari;
	}
	public int getS_DHyoukaNin() {
		return S_DHyoukaNin;
	}
	public float getS_DHyoukaWari() {
		return S_DHyoukaWari;
	}
	public int getS_EHyoukaNin() {
		return S_EHyoukaNin;
	}
	public float getS_EHyoukaWari() {
		return S_EHyoukaWari;
	}

	public int getA_AHyoukaNin() {
		return A_AHyoukaNin;
	}
	public float getA_AHyoukaWari() {
		return A_AHyoukaWari;
	}
	public int getA_BHyoukaNin() {
		return A_BHyoukaNin;
	}
	public float getA_BHyoukaWari() {
		return A_BHyoukaWari;
	}
	public int getA_CHyoukaNin() {
		return A_CHyoukaNin;
	}
	public float getA_CHyoukaWari() {
		return A_CHyoukaWari;
	}
	public int getA_DHyoukaNin() {
		return A_DHyoukaNin;
	}
	public float getA_DHyoukaWari() {
		return A_DHyoukaWari;
	}
	public int getA_EHyoukaNin() {
		return A_EHyoukaNin;
	}
	public float getA_EHyoukaWari() {
		return A_EHyoukaWari;
	}

	public ArrayList getS13SougouList() {
		return this.S13SougouList;
	}

	/*----------*/
	/* Get      */
	/*----------*/

	public void setS_AHyoukaNin(int s_AHyoukaNin) {
		this.S_AHyoukaNin = s_AHyoukaNin;
	}
	public void setS_AHyoukaWari(float s_AHyoukaWari) {
		this.S_AHyoukaWari = s_AHyoukaWari;
	}
	public void setS_BHyoukaNin(int s_BHyoukaNin) {
		this.S_BHyoukaNin = s_BHyoukaNin;
	}
	public void setS_BHyoukaWari(float s_BHyoukaWari) {
		this.S_BHyoukaWari = s_BHyoukaWari;
	}
	public void setS_CHyoukaNin(int s_CHyoukaNin) {
		this.S_CHyoukaNin = s_CHyoukaNin;
	}
	public void setS_CHyoukaWari(float s_CHyoukaWari) {
		this.S_CHyoukaWari = s_CHyoukaWari;
	}
	public void setS_DHyoukaNin(int s_DHyoukaNin) {
		this.S_DHyoukaNin = s_DHyoukaNin;
	}
	public void setS_DHyoukaWari(float s_DHyoukaWari) {
		this.S_DHyoukaWari = s_DHyoukaWari;
	}
	public void setS_EHyoukaNin(int s_EHyoukaNin) {
		this.S_EHyoukaNin = s_EHyoukaNin;
	}
	public void setS_EHyoukaWari(float s_EHyoukaWari) {
		this.S_EHyoukaWari = s_EHyoukaWari;
	}

	public void setA_AHyoukaNin(int a_AHyoukaNin) {
		this.A_AHyoukaNin = a_AHyoukaNin;
	}
	public void setA_AHyoukaWari(float a_AHyoukaWari) {
		this.A_AHyoukaWari = a_AHyoukaWari;
	}
	public void setA_BHyoukaNin(int a_BHyoukaNin) {
		this.A_BHyoukaNin = a_BHyoukaNin;
	}
	public void setA_BHyoukaWari(float a_BHyoukaWari) {
		this.A_BHyoukaWari = a_BHyoukaWari;
	}
	public void setA_CHyoukaNin(int a_CHyoukaNin) {
		this.A_CHyoukaNin = a_CHyoukaNin;
	}
	public void setA_CHyoukaWari(float a_CHyoukaWari) {
		this.A_CHyoukaWari = a_CHyoukaWari;
	}
	public void setA_DHyoukaNin(int a_DHyoukaNin) {
		this.A_DHyoukaNin = a_DHyoukaNin;
	}
	public void setA_DHyoukaWari(float a_DHyoukaWari) {
		this.A_DHyoukaWari = a_DHyoukaWari;
	}
	public void setA_EHyoukaNin(int a_EHyoukaNin) {
		this.A_EHyoukaNin = a_EHyoukaNin;
	}
	public void setA_EHyoukaWari(float a_EHyoukaWari) {
		this.A_EHyoukaWari = a_EHyoukaWari;
	}

	public void setS13SougouList(ArrayList s13SougouList) {
		this.S13SougouList = s13SougouList;
	}
}
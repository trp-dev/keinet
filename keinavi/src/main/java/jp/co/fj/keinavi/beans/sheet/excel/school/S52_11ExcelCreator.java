package jp.co.fj.keinavi.beans.sheet.excel.school;

import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.common.ISheetData;
import jp.co.fj.keinavi.beans.sheet.school.data.S52_11Data;
import jp.co.fj.keinavi.beans.sheet.school.data.S52_11ExamListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S52_11SubjectListData;
import jp.co.fj.keinavi.excel.cm.CM;

/**
 *
 * 校内成績分析 - 過回比較 - 偏差値分布
 * 受験学力測定テスト専用
 *
 * 2007.07.30	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class S52_11ExcelCreator extends S42_15ExcelCreator {

    // 行開始インデックス
    private final int startRowIndex = 7;
    // 共通処理クラス
    private CM cm = new CM();

    /**
     * コンストラクタ
     *
     * @param pData
     * @param pSequenceId
     * @param pOutFileList
     * @param pPrintFlag
     * @throws Exception
     */
    public S52_11ExcelCreator(final ISheetData pData, final String pSequenceId,
            final List pOutFileList, final int pPrintFlag) throws Exception {

        super(pData, pSequenceId, pOutFileList, pPrintFlag, 7, -1);
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
     * 			#initSheet(org.apache.poi.hssf.usermodel.HSSFSheet)
     */
    protected void initSheet(final ISheetData pData) throws Exception {

        // データクラス
        final S52_11Data data = (S52_11Data) pData;

        // 作成日
        setCreateDate();
        // セキュリティスタンプ
        setSecurityStamp(data.getSecurityStamp(), 49, 52);
        // 学校名
        setCellValue("A2", "学校名　　　：" + data.getBundleName());
        // 対象模試
        setCellValue("A3", createTargetExamLabel(
                data.getExamName(), data.getInpleDate()));
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.BaseSheetCreator
     * 			#createSheet(jp.co.fj.keinavi.beans.sheet.common.ISheetData)
     */
    protected void createSheet(final ISheetData pData) throws Exception {

        // データクラス
        final S52_11Data data = (S52_11Data) pData;

        // 表示位置
        int position = 0;

        // 科目でループする
        int count = 0;
        for (final Iterator ite = data.getSubjectListData(
                ).iterator(); ite.hasNext();) {

            createSheet((S52_11SubjectListData) ite.next(), position);
            position = ++count % 3;

            if (ite.hasNext() && position == 0) {
                breakSheet();
            }
        }
    }

    protected void createSheet(final S52_11SubjectListData subData,
            final int position) throws Exception {

        // 科目
        if (position == 0) {
            setCellValue("A5", "科目：" + subData.getSubName());
        } else if (position == 1) {
            setCellValue("S5", "科目：" + subData.getSubName());
        } else {
            setCellValue("AK5", "科目：" + subData.getSubName());
        }

        // センター到達指標
        setCenterReachArea(subData.getReachLevelListData(), 18 * position);

        // シート内の位置
        int index = 0;

        for (final Iterator ite = subData.getExamListData().iterator(); ite.hasNext();) {

            final S52_11ExamListData yearData = (
                    S52_11ExamListData) ite.next();

            // シート変更宣言
            setModifiedSheet();
            // データセット
            createSheet(yearData, index, position);

            index++;
        }
    }

    private void createSheet(final S52_11ExamListData examData,
            final int index, final int position) {

        // 列位置
        final int cIndex = (2 + 18 * position) + 3 * index;

        // 模試名
        setCellValue(startRowIndex - 2, cIndex, getExamName(examData));
        // 合計人数
        setCellValue(startRowIndex + 61, cIndex, examData.getNumbers());
        // 平均スコア
        setCellValue(startRowIndex + 62, cIndex, examData.getAvgPnt());

        // スコア帯データをセット
        if (!examData.getScoreZoneListMap().isEmpty()) {
            createSheet(examData.getScoreZoneListMap(), cIndex);
        }
    }

    private String getExamName(S52_11ExamListData data) {
        return data.getExamName() + cm.setTaisyouMoshi(data.getInpleDate());
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
     * 			#getSheetId()
     */
    protected String getSheetId() {
        return "S52_11";
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
     * 			#isGraphSheet()
     */
    protected boolean isGraphSheet() {
        return false;
    }

}

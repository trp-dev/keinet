package jp.co.fj.keinavi.util.taglib;

import java.io.IOException;
import java.io.LineNumberReader;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 * @author kawai
 *
 * 改行コードを<BR>に変換する
 * 
 */
public class PreTag extends BodyTagSupport {
	
	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	public int doStartTag() throws JspException {
		return (EVAL_BODY_BUFFERED);
	}
	
	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.IterationTag#doAfterBody()
	 */
	public int doAfterBody() throws JspException {
		return SKIP_BODY;
	}
	
	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {
		JspWriter writer = pageContext.getOut();
		if (bodyContent != null) {
			try {
				StringBuffer buff = new StringBuffer();
				LineNumberReader reader = new LineNumberReader(bodyContent.getReader());
				String line = null;
				while ((line = reader.readLine()) != null) {
					buff.append(line);
					buff.append("<br>");
				}
				writer.print(buff.toString());
			} catch (IOException e) {
				throw new JspException(e);
			}
		}
		return EVAL_PAGE;
	}

	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.BodyTag#doInitBody()
	 */
	public void doInitBody() throws JspException {
		super.doInitBody();
	}

}

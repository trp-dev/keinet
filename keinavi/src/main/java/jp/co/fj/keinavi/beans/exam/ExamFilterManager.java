package jp.co.fj.keinavi.beans.exam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.keinavi.beans.exam.filter.IExamFilterBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.totec.config.ConfigResolver;

import org.apache.commons.configuration.Configuration;

/**
 *
 * 模試フィルタリングマネージャ
 *
 * 2006.08.23	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class ExamFilterManager {

	// Filterを入れておくマップ
	private static final Map FILTER = new HashMap();
	// 設定値を入れておくマップ
	private static final Map CONFIG = new HashMap();

	static {
		final Configuration config = ConfigResolver.getInstance(
				).getConfiguration("examfilter.properties");
		for (final Iterator ite = config.getKeys(); ite.hasNext();) {
			final String key = (String) ite.next();
			final String value = config.getString(key);
			// 値がなければ飛ばす
			if (value == null) {
				continue;
			}
			// ドットを含むならフィルタのFQDN
			if (value.indexOf('.') >= 0) {
				initFilter(key, value);
			// そうでなければフィルタ設定
			} else {
				CONFIG.put(key, config.getStringArray(key));
			}
		}
	}

	private static void initFilter(final String key, final String className) {
		try {
			FILTER.put(key, Class.forName(className).newInstance());
		} catch (final Exception e) {
		    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
		    //throw new InternalError("インスタンス生成に失敗。" + className);
		    throw new InternalError(e);
		    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
		}
	}

	/**
	 * @param id 評価する画面ID
	 * @param login ログイン情報
	 * @param examSession 模試セッション
	 * @param exam 模試データ
	 * @return 判定結果
	 */
	public static boolean execute(final String id ,final LoginSession login,
			final ExamSession examSession, final ExamData exam) {

	 // 2019/10/10 QQ)Tanioka t106で対象模試にプライムステージを含めるよう修正 ADD START
	                 //フィルタフラグ
	                 Boolean filterFlg = false;
	                 //複数フィルタ使用フラグ
	                 Boolean filterAllUseFlg = false;
	 // 2019/10/10 QQ)Tanioka t106で対象模試にプライムステージを含めるよう修正 ADD END

// 2019/09/11 QQ)Tanioka 共通テスト対応 UPD START
//		// 模試データがなければtrue固定
//		if (exam == null) {
//			return true;
//		}
		// 模試データがなければtrue固定 (評価する画面IDが't106'以外)
		// ※null判定を消すだけでは全画面に影響を及ぼす可能性があるため、't106'のみ個別対応
		if ( exam == null ) {
			if( !"t106".equals(id) ) {
				return true;
			}
		}
// 2019/09/11 QQ)Tanioka 共通テスト対応 UPD END

		final String[] target;
		// 画面ごとの設定がある場合
		if (CONFIG.containsKey(id)) {
			target = (String[]) CONFIG.get(id);
		// フィルタ設定がある場合
		} else if (FILTER.containsKey(id)) {
			target = new String[]{id};
		// どちらもなければtrueフィルタ未定義
		} else {
			return true;
		}

		// 評価する
		for (int i = 0; i < target.length; i++) {

			final IExamFilterBean filter = (IExamFilterBean) FILTER.get(target[i]);

			// 設定ミス
			if (filter == null) {
				throw new IllegalArgumentException(
						"模試フィルタIDが不正です。" + target[i]);
			}

			// 2019/10/10 QQ)Tanioka t106で対象模試にプライムステージを含めるよう修正 ADD START
			                        // 一部の画面は個別対応
			                        if("t106".equals(id)) {
			                                // 複数フィルタに対応。フィルタが一つでもtrueとなればtrueで返す。
			                                filterAllUseFlg = true;

			                                if (!filter.execute(login, examSession, exam)) {
			                                        filterFlg = true;
			                                }


			                        } else {
			// 2019/10/10 QQ)Tanioka t106で対象模試にプライムステージを含めるよう修正 ADD END
			                                // 失敗ならここまで
			                                if (!filter.execute(login, examSession, exam)) {
			                                        return false;
			                                }

			// 2019/10/10 QQ)Tanioka t106で対象模試にプライムステージを含めるよう修正 ADD START
			                        }
			// 2019/10/10 QQ)Tanioka t106で対象模試にプライムステージを含めるよう修正 ADD END

			                }

			// 2019/10/10 QQ)Tanioka t106で対象模試にプライムステージを含めるよう修正 ADD START
			                //複数フィルタ使用時
			                if(filterAllUseFlg) {
			                        if(filterFlg) {
			                                return true;
			                        } else {
			                                return false;
			                        }
			                }
			// 2019/10/10 QQ)Tanioka t106で対象模試にプライムステージを含めるよう修正 ADD END

		// ここまで来ればOK
		return true;
	}

	/**
	 * 模試セッションを再構築する
	 * @param examSession
	 * @return
	 */
	public static ExamSession rebuild(final String id,
			final LoginSession login, final ExamSession examSession) {

		// 新しい入れ物
		final ExamSession s = new ExamSession();
		s.setPublicExamSet(examSession.getPublicExamSet());

		// 模試年度配列
		final String[] years = examSession.getYears();
		// データがなければここまで
		if (years == null) {
			return s;
		}

		// 全ての模試について、判定が通る物だけを格納する
		for (int i=0; i<years.length; i++) {
			for (final Iterator ite = examSession.getExamList(
					years[i]).iterator(); ite.hasNext();) {
				final ExamData exam = (ExamData) ite.next();
				if (execute(id, login, examSession, exam)) {
					final List list;
					if (s.getExamMap().containsKey(years[i])) {
						list = (List) s.getExamList(years[i]);
					} else {
						list = new ArrayList();
						s.getExamMap().put(years[i], list);
					}
					list.add(exam);
				}
			}
		}

		// 年度の配列を構築する
		final List list = new ArrayList(s.getExamMap().keySet());
		Collections.sort(list);
		Collections.reverse(list);
		s.setYears((String[]) list.toArray(new String[0]));

		return s;
	}

}

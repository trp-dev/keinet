/**
 * �Z�����ѕ��́|�N���X��r�@�ݖ�ʐ��сi�N���X��r�j
 *      ����]���ʐl�� �f�[�^�N���X
 * �쐬��: 2019/09/05
 * @author      M.Ooseto
 */

package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;

public class S33KokugoListBean {
    // �w�Z��
    private String bundleName = "";
    // �Ώ͎ۖ���
    private String examName = "";
    // �Ώۊw�N
    private int tergetGrade = 0;
    // �Z���S�́|�󌱎Ґ�
    private int numbers = 0;
    // �Z���S�́|A�]���@����
    private float evalACompratio = 0;
    // �Z���S�́|B�]���@����
    private float evalBCompratio = 0;
    // �Z���S�́|C�]���@����
    private float evalCCompratio = 0;
    // �Z���S�́|D�]���@����
    private float evalDCompratio = 0;
    // �Z���S�́|E�]���@����
    private float evalECompratio = 0;
    // �Z���S�́|A�]���@�l��
    private int evalANumbers = 0;
    // �Z���S�́|B�]���@�l��
    private int evalBNumbers = 0;
    // �Z���S�́|C�]���@�l��
    private int evalCNumbers = 0;
    // �Z���S�́|D�]���@�l��
    private int evalDNumbers = 0;
    // �Z���S�́|E�]���@�l��
    private int evalENumbers = 0;
    // �����]��-�N���X �f�[�^���X�g
    private ArrayList S33KokugoClassList = new ArrayList();
    // ��ʕ]��-�Z���S�� �f�[�^���X�g
    private ArrayList S33KokugoQuestionList = new ArrayList();
    // ��ʕ]��-�N���X �f�[�^���X�g
    private ArrayList S33KokugoQuestionClassList = new ArrayList();

    /*-----*/
    /* Get */
    /*-----*/
    public String getBundleName() {
        return this.bundleName;
    }
    public String getExamName() {
        return this.examName;
    }
    public int getTergetGrade() {
        return this.tergetGrade;
    }
    public int getNumbers() {
        return this.numbers;
    }
    public float getEvalACompratio() {
        return this.evalACompratio;
    }
    public float getEvalBCompratio() {
        return this.evalBCompratio;
    }
    public float getEvalCCompratio() {
        return this.evalCCompratio;
    }
    public float getEvalDCompratio() {
        return this.evalDCompratio;
    }
    public float getEvalECompratio() {
        return this.evalECompratio;
    }
    public int getEvalANumbers() {
        return this.evalANumbers;
    }
    public int getEvalBNumbers() {
        return this.evalBNumbers;
    }
    public int getEvalCNumbers() {
        return this.evalCNumbers;
    }
    public int getEvalDNumbers() {
        return this.evalDNumbers;
    }
    public int getEvalENumbers() {
        return this.evalENumbers;
    }
    public ArrayList getS33KokugoClassList() {
        return this.S33KokugoClassList;
    }
    public ArrayList getS33KokugoQuestionList() {
        return this.S33KokugoQuestionList;
    }
    public ArrayList getS33KokugoQuestionClassList() {
        return this.S33KokugoQuestionClassList;
    }

    /*-----*/
    /* Set */
    /*-----*/
    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }
    public void setExamName(String examName) {
        this.examName = examName;
    }
    public void setTergetGrade(int tergetGrade) {
        this.tergetGrade = tergetGrade;
    }
    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }
    public void setEvalACompratio(float evalACompratio) {
        this.evalACompratio = evalACompratio;
    }
    public void setEvalBCompratio(float evalBCompratio) {
        this.evalBCompratio = evalBCompratio;
    }
    public void setEvalCCompratio(float evalCCompratio) {
        this.evalCCompratio = evalCCompratio;
    }
    public void setEvalDCompratio(float evalDCompratio) {
        this.evalDCompratio = evalDCompratio;
    }
    public void setEvalECompratio(float evalECompratio) {
        this.evalECompratio = evalECompratio;
    }
    public void setEvalANumbers(int evalANumbers) {
        this.evalANumbers = evalANumbers;
    }
    public void setEvalBNumbers(int evalBNumbers) {
        this.evalBNumbers = evalBNumbers;
    }
    public void setEvalCNumbers(int evalCNumbers) {
        this.evalCNumbers = evalCNumbers;
    }
    public void setEvalDNumbers(int evalDNumbers) {
        this.evalDNumbers = evalDNumbers;
    }
    public void setEvalENumbers(int evalENumbers) {
        this.evalENumbers = evalENumbers;
    }
    public void setS33KokugoClassList(ArrayList S33KokugoClassList) {
        this.S33KokugoClassList = S33KokugoClassList;
    }
    public void setS33KokugoQuestionList(ArrayList S33KokugoQuestionList) {
        this.S33KokugoQuestionList = S33KokugoQuestionList;
    }
    public void setS33KokugoQuestionClassList(ArrayList S33KokugoQuestionClassList) {
        this.S33KokugoQuestionClassList = S33KokugoQuestionClassList;
    }

}

package jp.co.fj.keinavi.data.individual;

/**
*
* 個人成績分析用成績データ
*
* 2006/08/31    [新規作成]
*
* @author Tomohisa YAMADA - TOTEC
* @version 1.0
*
* 2009.11.20   Fujito URAKAWA - Totec
*              「学力レベル」追加対応
*
*/
public class SubRecordIData extends SubRecordData {

    /**
     * 到達レベル／センター到達エリア
     */
    private String level;

    /**
     * 学力レベル
     */
    private String scholarlevel;

    /**
     * @return
     */
    public String getLevel() {
        return level;
    }

    /**
     * @param pLevel
     */
    public void setLevel(String pLevel) {
        level = pLevel;
    }


    public String getScholarlevel() {
        return scholarlevel;
    }

    public void setScholarlevel(String string) {
        this.scholarlevel = string;
    }

    public static SubRecordIData getSubRecordIDataEmptyData() {

        SubRecordIData empty = new SubRecordIData();
        empty.setSubName("");
        empty.setScore("");
        empty.setSubAllotPnt("");
        empty.setCvsScore("");
        empty.setCDeviation("");
        empty.setScope("");//[1] add
        return empty;

    }

}

package jp.co.fj.keinavi.beans.recount.data;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 *
 * 再集計依頼データ
 * 
 * 2006.02.13	[新規作成]
 * 
 * 2007.08.07	校内成績処理システムより移植
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class RecountRequest {

	// 学校コード
	private final String schoolCd;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 学校コード
	 */
	public RecountRequest(final String pSchoolCd) {
		schoolCd = pSchoolCd;
	}

	/**
	 * @return schoolCd を戻します。
	 */
	public String getSchoolCd() {
		return schoolCd;
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return new ToStringBuilder(this)
				.append("SchoolCd", schoolCd)
				.toString();
	}

}

/*
 * 作成日: 2004/09/17
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.news;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.beans.news.InformSearchBean;
import jp.co.fj.keinavi.forms.news.N001Form;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class N001Servlet extends DefaultHttpServlet {
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(

		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ


		// セッション
		HttpSession session = request.getSession();

		// ログイン情報
		LoginSession login = super.getLoginSession(request);

		// アクションフォーム
		N001Form form = null;
		// requestから取得する
		try {
			form = (N001Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.news.N001Form");
		} catch (Exception e) {
			throw new ServletException(e);
		}
		
		if ( "n001".equals(getForward(request)) || getForward(request) == null ) {

			// ページ初期化
			if ( form.getPage() == null ) {
				form.setPage("1");
			}
			if ( form.getDisplayDiv() == null ) {
				form.setDisplayDiv("0"); 
			}
		

			// DBコネクション
			Connection con = null;
			try {
				con = super.getConnectionPool(request);
				// お知らせ検索
				InformSearchBean bean = new InformSearchBean();
				bean.setConnection(null, con);

				if (login != null) {
					bean.setUserID(login.getUserID());		// ログインユーザID
					bean.setUserMode(login.getUserMode());	// ユーザーモード
				}
				bean.setDisplayDiv(form.getDisplayDiv());	// 表示区分 "0":お知らせ "1":Kei-Net情報
				bean.execute();
				// ページサイズは設定ファイルの値
				// （これはお知らせトップページでは使用しない）
				//bean.setPageSize(bean.getInfoDisplayCountLimit("0")); 
				bean.setPageSize(10);								// 10行固定 
				bean.setPageNo(Integer.parseInt(form.getPage())); 	// ページ番号
				request.setAttribute("SearchBean", bean);

			} catch (Exception e) {
				throw new ServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}
			
			// アクションフォーム
			request.setAttribute("form", form);
	
			super.forward(request, response, JSP_N001);

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
		
	}

}

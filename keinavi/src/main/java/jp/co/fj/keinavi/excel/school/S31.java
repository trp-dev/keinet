/**
 * 校内成績分析−クラス比較　成績概況（クラス比較）
 * 出力する帳票の判断
 * 作成日: 2004/07/26
 * @author	T.Sakai
 */
 
package jp.co.fj.keinavi.excel.school;

import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.util.log.*;

public class S31 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean s31( S31Item s31Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		try{
			int ret = 0;
			//S31Itemから各帳票を出力
			if (( s31Item.getIntHyouFlg()==0 ) && ( s31Item.getIntGraphFlg()==0 )){
				throw new Exception("S31 ERROR : フラグ異常 ");
			}
			if ( s31Item.getIntHyouFlg()==1 ) {
				log.Ep("S31_01","S31_01帳票作成開始","");
				S31_01 exceledit = new S31_01();
				ret = exceledit.s31_01EditExcel( s31Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S31_01","帳票作成エラー","");
					return false;
				}
				log.Ep("S31_01","S31_01帳票作成終了","");
				sheetLog.add("S31_01");
			}
			if ( s31Item.getIntGraphFlg()==1 ) {
				log.Ep("S31_02","S31_02帳票作成開始","");
				S31_02 exceledit = new S31_02();
				ret = exceledit.s31_02EditExcel( s31Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S31_02","帳票作成エラー","");
					return false;
				}
				log.Ep("S31_02","S31_02帳票作成終了","");
				sheetLog.add("S31_02");
			}
			
		} catch(Exception e) {
			log.Err("99S31","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
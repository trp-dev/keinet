/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.excel.data.school.S32BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S32ClassListBean;
import jp.co.fj.keinavi.excel.data.school.S32Item;
import jp.co.fj.keinavi.excel.data.school.S32ListBean;
import jp.co.fj.keinavi.excel.school.S32;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 *
 * 2005.04.21	Yoshimoto KAWAI - Totec
 * 				新テスト対応
 *
 * [3] 2005.08.25	Masami SHIMIZU - Totec
 * 				複合クラスの新テスト対応
 * 				（新テストの場合はクラスの偏差値データを無効値にする）
 *
 * @author kawai
 *
 */
public class S32SheetBean extends AbstractSheetBean {

	// データクラス
	private final S32Item data = new S32Item();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		String[] code = super.getSubjectCDArray(); // 型・科目設定値
		List graph = super.getSubjectGraphList(); // 型・科目グラフ表示対象
		boolean isNewExam = KNUtil.isNewExam(exam); // 新テストかどうか [3]

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ

		// 表
		switch (super.getIntFlag(IProfileItem.CHART_COMP_RATIO)) {
			case  1: data.setIntHyouFlg(0); data.setIntKoseihiFlg(1); break;
			case  2: data.setIntHyouFlg(0); data.setIntKoseihiFlg(2); break;
			case 11: data.setIntHyouFlg(1); data.setIntKoseihiFlg(1); break;
			case 12: data.setIntHyouFlg(1); data.setIntKoseihiFlg(2); break;
		}

		// 偏差値帯別度数分布グラフ
		switch (super.getIntFlag(IProfileItem.GRAPH_DIST)) {
			case  1: data.setIntBnpGraphFlg(0); data.setIntAxisFlg(1); break;
			case  2: data.setIntBnpGraphFlg(0); data.setIntAxisFlg(2); break;
			case 11: data.setIntBnpGraphFlg(1); data.setIntAxisFlg(1); break;
			case 12: data.setIntBnpGraphFlg(1); data.setIntAxisFlg(2); break;
		}

		// 偏差値帯別人数積み上げグラフ
		switch (super.getIntFlag(IProfileItem.GRAPH_BUILDUP)) {
			case  1: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(1); break;
			case  2: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(2); break;
			case  3: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(3); break;
			case 11: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(1); break;
			case 12: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(2); break;
			case 13: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(3); break;
		}

		// 偏差値帯別構成比グラフ
		switch (super.getIntFlag(IProfileItem.GRAPH_COMP_RATIO)) {
			case  1: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(1); break;
			case  2: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(2); break;
			case  3: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(3); break;
			case 11: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(1); break;
			case 12: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(2); break;
			case 13: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(3); break;
		}

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//		// 2019/07/26 QQ)Tanioka 共通テスト対応 ADD START
//		// 共通テスト英語認定試験CEFR取得状況
//		switch (super.getIntFlag(IProfileItem.OPTION_CHECK_BOX)) {
//			case  1: data.setIntCheckBoxFlg(0); data.setIntTargetCheckBoxFlg(1); break;
//			case  2: data.setIntCheckBoxFlg(0); data.setIntTargetCheckBoxFlg(2); break;
//			case 11: data.setIntCheckBoxFlg(1); data.setIntTargetCheckBoxFlg(1); break;
//			case 12: data.setIntCheckBoxFlg(1); data.setIntTargetCheckBoxFlg(2); break;
//		}
//		// 2019/07/26 QQ)Tanioka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		// ワークテーブルのセットアップ
		super.insertIntoCountCompClass();

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/13 DP)Nonaka 共通テスト対応 ADD START
//// 共通テスト英語認定試験CEFR取得状況
//		PreparedStatement ps6 = null;
//		PreparedStatement ps7 = null;
//		PreparedStatement ps8 = null;
//		PreparedStatement ps9 = null;
//// 2019/09/13 DP)Nonaka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		ResultSet rs4 = null;
		ResultSet rs5 = null;
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/13 DP)Nonaka 共通テスト対応 ADD START
//// 共通テスト英語認定試験CEFR取得状況
//		ResultSet rs6 = null;
//		ResultSet rs7 = null;
//		ResultSet rs8 = null;
//		ResultSet rs9 = null;
//// 2019/09/13 DP)Nonaka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
		try {
			List c1 = new LinkedList(); // 型の入れ物
			List c2 = new LinkedList(); // 科目の入れ物

			// データリストの取得
			Query query1 = QueryLoader.getInstance().load("s22_1");
			query1.setStringArray(1, code); // 型・科目コード

			ps1 = conn.prepareStatement(query1.toString());
			ps1.setString(1, exam.getExamYear()); // 対象年度
			ps1.setString(2, exam.getExamCD()); // 対象模試

			// クラスデータリスト（自校）
			ps2 = conn.prepareStatement(QueryLoader.getInstance().load("s31_1").toString());
			ps2.setString(1, exam.getExamYear()); // 模試年度
			ps2.setString(2, exam.getExamCD()); // 模試コード
			ps2.setString(3, profile.getBundleCD()); // 一括コード

			// 偏差値分布データリスト（自校）
			ps3 = conn.prepareStatement(QueryLoader.getInstance().load("s12_2").toString());
			ps3.setString(1, exam.getExamYear()); // 模試年度
			ps3.setString(2, exam.getExamCD()); // 模試コード
			ps3.setString(3, profile.getBundleCD()); // 一括コード

			// クラスデータリスト（クラス）
			ps4 = conn.prepareStatement(QueryLoader.getInstance().load("s31_2").toString());
			ps4.setString(1, exam.getExamYear()); // 模試年度
			ps4.setString(2, exam.getExamCD()); // 模試コード
			ps4.setString(3, profile.getBundleCD()); // 一括コード
			ps4.setString(5, exam.getExamYear()); // 模試年度
			ps4.setString(6, exam.getExamCD()); // 模試コード
			ps4.setString(7, exam.getExamYear()); // 模試年度
			ps4.setString(8, exam.getExamCD()); // 模試コード
			ps4.setString(9, profile.getBundleCD()); // 一括コード
			ps4.setString(11, exam.getExamYear()); // 模試年度
			ps4.setString(12, exam.getExamCD()); // 模試コード

			// 偏差値分布データリスト（クラス）
			//ps5 = conn.prepareStatement(QueryLoader.getInstance().load("s32").toString());
			ps5 = conn.prepareStatement(super.getQueryWithDeviationZone("s32").toString());
			ps5.setString(1, exam.getExamYear()); // 模試年度
			ps5.setString(2, exam.getExamCD()); // 模試コード
			ps5.setString(3, profile.getBundleCD()); // 一括コード
			ps5.setString(7, exam.getExamYear()); // 模試年度
			ps5.setString(8, exam.getExamCD()); // 模試コード
			ps5.setString(9, profile.getBundleCD()); // 一括コード
			ps5.setString(13, exam.getExamYear()); // 模試年度
			ps5.setString(14, exam.getExamCD()); // 模試コード
			ps5.setString(15, profile.getBundleCD()); // 一括コード

			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				S32ListBean bean = new S32ListBean();
				bean.setStrKmkCd(rs1.getString(1));
				bean.setStrKmkmei(rs1.getString(2));
				bean.setStrHaitenKmk(rs1.getString(3));
				bean.setIntDispKmkFlg(graph.contains(rs1.getString(1)) ? 1 : 0);

				if (rs1.getInt(1) >= 7000) c1.add(bean); // 型
				else c2.add(bean); // 科目

				// 型・科目コード
				ps2.setString(4, bean.getStrKmkCd());
				ps3.setString(4, bean.getStrKmkCd());
				ps4.setString(4, bean.getStrKmkCd());
				ps4.setString(10, bean.getStrKmkCd());
				ps4.setString(13, bean.getStrKmkCd());
				ps5.setString(4, bean.getStrKmkCd());
				ps5.setString(10, bean.getStrKmkCd());
				ps5.setString(16, bean.getStrKmkCd());

				// 自校
				{
					S32ClassListBean c = new S32ClassListBean();
					c.setStrGakkomei(profile.getBundleName());
					c.setIntDispClassFlg(1);

					rs2 = ps2.executeQuery();

					if (rs2.next()) {
						c.setIntNinzu(rs2.getInt(1));
						c.setFloHeikin(rs2.getFloat(2));
						// 新テストなら平均偏差値→無効値 [3]
						if (isNewExam) {
							c.setFloHensa(-999);
						}
						else {
							c.setFloHensa(rs2.getFloat(3));
						}
					} else {
						c.setIntNinzu(-999);
						c.setFloHeikin(-999);
						c.setFloHensa(-999);
					}

					rs2.close();

					rs3 = ps3.executeQuery();
					while (rs3.next()) {
						S32BnpListBean b = new S32BnpListBean();
						b.setFloBnpMax(rs3.getFloat(1));
						b.setFloBnpMin(rs3.getFloat(2));
						b.setIntNinzu(rs3.getInt(3));
						b.setFloKoseihi(rs3.getFloat(4));
						c.getS32BnpList().add(b);
					}
					rs3.close();

					bean.getS32ClassList().add(c);
				}

				// クラス
				{
					rs4 = ps4.executeQuery();
					while (rs4.next()) {
						S32ClassListBean c = new S32ClassListBean();
						c.setStrGrade(rs4.getString(1));
						c.setStrClass(rs4.getString(2));
						c.setIntNinzu(rs4.getInt(4));
						c.setFloHeikin(rs4.getFloat(5));
						// 新テストなら平均偏差値→無効値 [3]
						if (isNewExam) {
							c.setFloHensa(-999);
						}
						else {
							c.setFloHensa(rs4.getFloat(6));
						}
						c.setIntDispClassFlg(rs4.getInt(7));

						// 学年
						ps5.setInt(5, rs4.getInt(1));
						ps5.setInt(11, rs4.getInt(1));
						ps5.setInt(17, rs4.getInt(1));
						// クラス
						ps5.setString(6, rs4.getString(3));
						ps5.setString(12, rs4.getString(3));
						ps5.setString(18, rs4.getString(3));

						rs5 = ps5.executeQuery();
						while (rs5.next()) {
							S32BnpListBean b = new S32BnpListBean();
							b.setFloBnpMax(rs5.getFloat(1));
							b.setFloBnpMin(rs5.getFloat(2));
							b.setIntNinzu(rs5.getInt(3));
							b.setFloKoseihi(rs5.getFloat(4));
							c.getS32BnpList().add(b);
						}
						rs5.close();

						bean.getS32ClassList().add(c);
					}
					rs4.close();
				}
			}

			// 型・科目の順番に詰める
			data.getS32List().addAll(c1);
			data.getS32List().addAll(c2);
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/13 DP)Nonaka 共通テスト対応 ADD START
//// 共通テスト英語認定試験CEFR取得状況
//
//
//			Query query6 = QueryLoader.getInstance().load("s22_7");
//			ps6 = conn.prepareStatement(query6.toString());
//			ps6.setString(1, exam.getExamYear()); // 年度
//			ps6.setString(2, exam.getExamDiv()); // 模試区分
//			String allFlg = "0";
//
//			//各試験
//			rs6 = ps6.executeQuery();
//			while (rs6.next()) {
//				String engptCd = rs6.getString(1);
//				String engptLevelCd = rs6.getString(5);
//				String cefrLevelCd = rs6.getString(8);
//
//				Query query8 = QueryLoader.getInstance().load("s32_1");
//				ps8 = conn.prepareStatement(query8.toString());
//
//				ps8.setString(1, profile.getBundleCD());
//				ps8.setString(2, engptCd);
//				ps8.setString(3, engptLevelCd);
//
//				ps8.setString(4, profile.getBundleCD());
//				ps8.setString(5, engptCd);
//				ps8.setString(6, engptLevelCd);
//				ps8.setString(7, cefrLevelCd);
//				ps8.setString(8, exam.getExamYear());
//				ps8.setString(9, exam.getExamCD());
//
//				ps8.setString(10, exam.getExamYear());
//				ps8.setString(11, exam.getExamCD());
//				ps8.setString(12, profile.getBundleCD());
//				ps8.setString(13, engptCd);
//				ps8.setString(14, engptLevelCd);
//				ps8.setString(15, cefrLevelCd);
//				ps8.setString(16, profile.getBundleCD());
//
//				ps8.setString(17, exam.getExamYear());
//				ps8.setString(18, exam.getExamCD());
//				ps8.setString(19, profile.getBundleCD());
//				ps8.setString(20, profile.getBundleCD());
//				ps8.setString(21, engptCd);
//				ps8.setString(22, engptLevelCd);
//				ps8.setString(23, cefrLevelCd);
//
//				rs7 = ps8.executeQuery();
//				while (rs7.next()) {
//
//					S32PtCefrAcqStatusListBean pcaslb = new S32PtCefrAcqStatusListBean();
//					pcaslb.setEngptcd(rs6.getString(1));
//					pcaslb.setEngptname_abbr(rs6.getString(2));
//					pcaslb.setLevelflg(rs6.getString(3));
//					pcaslb.setEi_sort(rs6.getInt(4));
//					pcaslb.setEngptlevelcd(rs6.getString(5));
//					pcaslb.setEngptlevelname_abbr(rs6.getString(6));
//					pcaslb.setEdi_sort(rs6.getInt(7));
//					pcaslb.setCefrlevelcd(rs6.getString(8));
//					pcaslb.setCerflevelname_abbr(rs6.getString(9));
//					pcaslb.setCi_sort(rs6.getInt(10));
//					pcaslb.setClasses(rs7.getString(1));
//					pcaslb.setDispsequence(rs7.getInt(2));
//					pcaslb.setNumbers(rs7.getInt(3));
//					pcaslb.setCompratio(rs7.getFloat(4));
//					pcaslb.setFlg(rs7.getString(5));
//					data.getS32PtCefrAcqStatusList().add(pcaslb);
//
//					//校内全体のフラグを他のクラスに適用する
//						String clsName = rs7.getString(1);
//					if ("校内全体".equals(clsName)) {
//						allFlg = rs7.getString(5);
//					}
//					else {
//						pcaslb.setFlg(allFlg);
//					}
//
//				}
//				rs7.close();
//				ps8.close();
//			}
//			rs6.close();
//			ps6.close();
//
//			//全試験
//			Query query7 = QueryLoader.getInstance().load("s22_8");
//			ps7 = conn.prepareStatement(query7.toString());
//			ps7.setString(1, exam.getExamYear()); // 年
//			ps7.setString(2, exam.getExamDiv()); // 模試区分
//
//			rs8 = ps7.executeQuery();
//			while (rs8.next()) {
//				String cefrLevelCd = rs8.getString(1);
//
//				Query query9 = QueryLoader.getInstance().load("s32_2");
//				ps9 = conn.prepareStatement(query9.toString());
//
//				ps9.setString(1, profile.getBundleCD());
//				ps9.setString(2, cefrLevelCd);
//				ps9.setString(3, exam.getExamYear());
//				ps9.setString(4, exam.getExamCD());
//
//				ps9.setString(5, exam.getExamYear());
//				ps9.setString(6, exam.getExamCD());
//				ps9.setString(7, profile.getBundleCD());
//				ps9.setString(8, cefrLevelCd);
//				ps9.setString(9, profile.getBundleCD());
//
//				ps9.setString(10, exam.getExamYear());
//				ps9.setString(11, exam.getExamCD());
//				ps9.setString(12, profile.getBundleCD());
//				ps9.setString(13, profile.getBundleCD());
//				ps9.setString(14, cefrLevelCd);
//
//				rs9 = ps9.executeQuery();
//				while (rs9.next()) {
//
//					S32CefrAcqStatusListBean caslb = new S32CefrAcqStatusListBean();
//					caslb.setEngptcd(" ");
//					caslb.setEngptname_abbr(" ");
//					caslb.setLevelflg(" ");
//					caslb.setEi_sort(0);
//					caslb.setEngptlevelcd(" ");
//					caslb.setEngptlevelname_abbr(" ");
//					caslb.setEdi_sort(0);
//					caslb.setCefrlevelcd(rs8.getString(1));
//					caslb.setCerflevelname_abbr(rs8.getString(2));
//					caslb.setCi_sort(rs8.getInt(3));
//					caslb.setClasses(rs9.getString(1));
//					caslb.setDispsequence(rs9.getInt(2));
//					caslb.setNumbers(rs9.getInt(3));
//					caslb.setCompratio(rs9.getFloat(4));
//					caslb.setFlg(rs9.getString(5));
//					data.getS32CefrAcqStatusList().add(caslb);
//
//				}
//				rs9.close();
//				ps9.close();
//			}
//			rs8.close();
//			ps7.close();
//
//// 2019/09/13 DP)Nonaka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(rs3);
			DbUtils.closeQuietly(rs4);
			DbUtils.closeQuietly(rs5);
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/13 DP)Nonaka 共通テスト対応 ADD START
//// 共通テスト英語認定試験CEFR取得状況
//			DbUtils.closeQuietly(rs6);
//			DbUtils.closeQuietly(rs7);
//			DbUtils.closeQuietly(rs8);
//			DbUtils.closeQuietly(rs9);
//// 2019/09/13 DP)Nonaka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(ps3);
			DbUtils.closeQuietly(ps4);
			DbUtils.closeQuietly(ps5);
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/13 DP)Nonaka 共通テスト対応 ADD START
//// 共通テスト英語認定試験CEFR取得状況
//			DbUtils.closeQuietly(ps6);
//			DbUtils.closeQuietly(ps7);
//			DbUtils.closeQuietly(ps8);
//			DbUtils.closeQuietly(ps9);
//// 2019/09/13 DP)Nonaka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S32_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_CLASS_DEV;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S32().s32(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
	}

}

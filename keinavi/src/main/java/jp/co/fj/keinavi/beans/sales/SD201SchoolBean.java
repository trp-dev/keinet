package jp.co.fj.keinavi.beans.sales;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 画面に表示する学校名を生成するBeanです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD201SchoolBean extends DefaultBean {

	/** serialVersionUID */
	private static final long serialVersionUID = 2835771966019664250L;

	/** 学校コード */
	private final String schoolCd;

	/** 学校名 */
	private String schoolName;

	/**
	 * コンストラクタです。
	 * 
	 * @param schoolCd
	 *            学校コード
	 */
	public SD201SchoolBean(String schoolCd) {
		this.schoolCd = schoolCd;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance()
					.load("sd201_school_name").toString());
			ps.setString(1, schoolCd);
			rs = ps.executeQuery();
			if (rs.next()) {
				schoolName = rs.getString(1);
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * 学校名を返します。
	 * 
	 * @return 学校名
	 */
	public String getSchoolName() {
		return schoolName;
	}

}

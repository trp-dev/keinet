package jp.co.fj.keinavi.excel.data.school;

//import org.apache.struts.action.ActionForm;
import java.util.ArrayList;
/**
 * 校内成績分析−他校比較−設問別成績 データリスト
 * 作成日: 2004/07/20
 * @author	A.Iwata
 */
public class S43ListBean {
	//型・科目名コード
	private String strKmkCd = "";
	//型・科目名
	private String strKmkmei = "";
	//型･科目用グラフ表示フラグ
	private int intDispKmkFlg = 0;
	//配点
	private String strHaitenKmk = "";
	//設問番号
	private String strSetsuNo = "";
	//設問内容1
	private String strSetsuMei1 = "";
	//設問配点
	private String strSetsuHaiten = "";
	//設問別人数リスト
	private ArrayList s43GakkoList = new ArrayList();

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}
	public int getIntDispKmkFlg() {
		return this.intDispKmkFlg;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public String getStrSetsuNo() {
		return this.strSetsuNo;
	}
	public String getStrSetsuMei1() {
		return this.strSetsuMei1;
	}
	public String getStrSetsuHaiten() {
		return this.strSetsuHaiten;
	}
	public ArrayList getS43GakkoList() {
		return this.s43GakkoList;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setIntDispKmkFlg(int intDispKmkFlg) {
		this.intDispKmkFlg = intDispKmkFlg;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setStrSetsuNo(String strSetsuNo) {
		this.strSetsuNo = strSetsuNo;
	}
	public void setStrSetsuMei1(String strSetsuMei1) {
		this.strSetsuMei1 = strSetsuMei1;
	}
	public void setStrSetsuHaiten(String strSetsuHaiten) {
		this.strSetsuHaiten = strSetsuHaiten;
	}
	public void setS43GakkoList(ArrayList s43GakkoList) {
		this.s43GakkoList = s43GakkoList;
	}
}

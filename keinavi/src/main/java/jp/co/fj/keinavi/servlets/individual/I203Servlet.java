package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.individual.judgement.SearchUnivCodeFrontMatchBean;
import jp.co.fj.keinavi.beans.individual.judgement.SearchUnivFrontMatchBean;
import jp.co.fj.keinavi.beans.individual.judgement.SearchUnivInitCharBean;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.NestedUnivData;
import jp.co.fj.keinavi.data.individual.UnivKeiNavi;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I203Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNJpnStringConv;
import jp.co.fj.keinavi.util.individual.UnivSorter;

/**
 * 判定用大学検索画面
 * 
 * 2004.08.03   Tomohisa YAMADA - TOTEC
 *              新規作成
 * 
 * 2005.06.23 	Tomohisa YAMADA - TOTEC
 * 				検索文字列を12文字で切る処理追加
 * 
 * <2010年度改修>
 * 2010.01.06   Tomohisa YAMADA - TOTEC
 *              センターリサーチ５段階評価対応
 *              マスタデータの持ち方変更
 * 
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class I203Servlet extends IndividualServlet{
	
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
			
		//初期設定を行ってくれます。	
		super.execute(request, response);
			
		//フォームデータを取得
		I203Form form = null;
		try {
			form = (I203Form)super.getActionForm(request, "jp.co.fj.keinavi.forms.individual.I203Form");
		} catch (Exception e) {
			KNServletException k = new KNServletException("0I203Servlet.javaにてフォームデータの収得に失敗しました。");
			k.setErrorCode("00I203010101");
			throw k;
		}
		
		HttpSession session = request.getSession();
		
		//プロファイル情報
		Profile profile = (Profile) session.getAttribute(Profile.SESSION_KEY);
		
		//個人共通
		ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);
		
		//プロファイル判定対象生徒
		Short JudgeStudent = (Short)profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);
		
		if ("i203".equals(getForward(request))) {//転送:JSP

			if(!"i203".equals(getBackward(request))){
				//初回アクセス
				//初回用フォーム作成
				form.setSearchMode("name");
				form.setSearchStr("");
				//保留
				if(iCommonMap.isBunsekiMode()){
					form.setJudgeStudent(JudgeStudent.toString());
				}
			} else {
				List searchList = null;
				if(form.getButton().trim().equals("search")){
					//検索ボタンが押された
					if(form.getSearchMode().trim().equals("name")){
						//大学名前方一致
						//大学名(前方一致)から学部一覧を取得するBeanを生成する
						SearchUnivFrontMatchBean sufm = new SearchUnivFrontMatchBean();
						//sufm.setSearchUnivName(KNJpnStringConv.hkana2Han(form.getSearchStr()));//[1] del
						sufm.setSearchUnivName(KNJpnStringConv.hkana2Han(GeneralUtil.truncateString(form.getSearchStr(), 12)));//[1] add
						sufm.setCompairData(super.getUnivAll());
						sufm.execute();
	
						searchList = sufm.getResultArrayList();
					}else{
						//大学コード前方一致
						SearchUnivCodeFrontMatchBean sucfm = new SearchUnivCodeFrontMatchBean();
						sucfm.setSearchUnivCode(form.getSearchStr());
						sucfm.setCompairData(super.getUnivAll());
						sucfm.execute();
						
						searchList = sucfm.getResultArrayList();
					}
					
				} else {
					//頭文字が押された
					SearchUnivInitCharBean suic = new SearchUnivInitCharBean();
					suic.setInitCharHalf(KNJpnStringConv.hkana2Han(form.getCharValue().trim()));
					suic.setInitCharFull(KNJpnStringConv.hkana2Kkana(form.getCharValue().trim()));
					suic.setCompairData(super.getUnivAll());
					suic.execute();
					
					searchList = suic.getRecordSet();
				}
				//大学コード・学部コード・学科ソートキーでソート
				if(searchList != null && searchList.size() > 0){
					Collections.sort(searchList, new UnivSorter().new SortByUFDeptSortKey());
				}
				request.setAttribute("nestedUnivList", getNestedUnivList(searchList));
				request.setAttribute("nestedUnivListSize", Integer.toString(getNestedUnivList(searchList).size()));
			}
			
			//判定結果一覧から戻った場合はリストを表示しない。
			if("i204".equals(getBackward(request))){
				request.setAttribute("nestedUnivList", "");
				request.setAttribute("nestedUnivListSize", "0");
			}
			
			request.setAttribute("form", form);
			iCommonMap.setTargetPersonId(form.getTargetPersonId());
			super.forward(request, response, JSP_I203);
			
		//転送:SERVLET
		} else {
			
			//判定対象生徒保存
			if("i204".equals(getForward(request)) && iCommonMap.isBunsekiMode()){
				profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(form.getJudgeStudent()));
			}
			
			iCommonMap.setTargetPersonId(form.getTargetPersonId());
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}
	
	public List getNestedUnivList(List searchList){
		List list = new ArrayList();
		try{
			String[] tempData = new String[3];//for comparing
			List rList = searchList;
			Iterator ite = rList.iterator();
			while(ite.hasNext()){
				UnivKeiNavi data = (UnivKeiNavi)ite.next();
				
				//初めてのUNIV
				NestedUnivData univData;
				if(tempData[0] == null || !data.getUnivCd().equals(tempData[0]) || list.size() == 0){
					univData = new NestedUnivData();
					univData.setUnivCd(data.getUnivCd());
					univData.setUnivNameAbbr(data.getUnivName());
					univData.setFacultyList(new ArrayList());
					list.add(univData);
				}
				univData = (NestedUnivData) list.get(list.size() -1 );
				
				//初めてのFACULTY
				NestedUnivData.FacultyData facultyData;
				if(tempData[1] == null || !data.getFacultyCd().equals(tempData[1]) || univData.getFacultyList().size() == 0){
					facultyData = univData.new FacultyData();
					facultyData.setFacultyCd(data.getFacultyCd());
					facultyData.setFacultyNameAbbr(data.getDeptName());
					facultyData.setDeptList(new ArrayList());
					univData.getFacultyList().add(facultyData);
				}
				facultyData = (NestedUnivData.FacultyData) univData.getFacultyList().get(univData.getFacultyList().size() - 1);
				
				//初めてのDEPT
				NestedUnivData.FacultyData.DeptData deptData;
				if(tempData[2] == null || !data.getDeptCd().equals(tempData[2]) || facultyData.getDeptList().size() == 0){
					deptData = facultyData.new DeptData();
					deptData.setDeptCd(data.getDeptCd());
					deptData.setDeptNameAbbr(data.getSubName());
					deptData.setSuperAbbrName(data.getSubName());
					facultyData.getDeptList().add(deptData);
				}
				
				tempData[0] = data.getUnivCd();
				tempData[1] = data.getFacultyCd();
				tempData[2] = data.getDeptCd();
			}
			
		}catch(Exception e){
			e.printStackTrace();
			KNServletException k = new KNServletException("0I203Servlet.javaにて*********");
			k.setErrorCode("00I203010101");
		}
		return list;
	}

}

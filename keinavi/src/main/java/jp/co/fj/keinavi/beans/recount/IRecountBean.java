package jp.co.fj.keinavi.beans.recount;

import java.sql.Connection;

import jp.co.fj.keinavi.beans.recount.data.RecountTask;

/**
 *
 * 再集計Beanインターフェース
 * 
 * 2006.02.13	[新規作成]
 * 
 * 2007.08.07	校内成績処理システムより移植
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public interface IRecountBean {

	/**
	 * @throws Exception SQL例外
	 */
	void execute() throws Exception;
	
	/**
	 * @param pCon 設定する con。
	 */
	void setConnection(Connection pCon);
	
	/**
	 * @param pBundleCd 設定する bundleCd。
	 */
	void setRecountTask(RecountTask task);
	
}

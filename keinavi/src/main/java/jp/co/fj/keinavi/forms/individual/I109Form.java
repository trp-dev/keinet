package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * バランスチャート用フォーム
 * @author ishiguro
 *
 * 2005.4.26 	K.Kondo 	[1]画面スクロール座標の追加
 */
public class I109Form extends ActionForm {

	private String mode;

	private String targetPersonId;		//個人ＩＤ
	private String targetExamCode;		//模試コード
	private String targetExamYear;		//模試年度
	private String targetSubName;	//科目名
	private String targetSubCode;	//科目コード
	private String dispSelect;
	
	private String scrollX;//[1] add サブミット前、表示座標Ｘ
	private String scrollY;//[1] add サブミット前、表示座標Ｙ
	
	private String targetScholarLevel;	// 対象生徒・対象科目学力レベル
	private String dispScholarLevel;	// 画面表示学力レベル
	
	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ
		
	}

	/**
	 * @return
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @return
	 */
	public String getTargetSubCode() {
		return targetSubCode;
	}

	/**
	 * @return
	 */
	public String getTargetSubName() {
		return targetSubName;
	}
	
	public String getTargetScholarLevel() {
		return targetScholarLevel;
	}

	public String getDispScholarLevel() {
		return dispScholarLevel;
	}

	/**
	 * @param string
	 */
	public void setMode(String string) {
		mode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @param string
	 */
	public void setTargetSubCode(String string) {
		targetSubCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetSubName(String string) {
		targetSubName = string;
	}

	/**
	 * @return
	 */
	public String getDispSelect() {
		return dispSelect;
	}

	/**
	 * @param string
	 */
	public void setDispSelect(String string) {
		dispSelect = string;
	}
	
	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[1] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[1] add
	}
	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[1] add
	}

	public void setTargetScholarLevel(String string) {
		this.targetScholarLevel = string;
	}

	public void setDispScholarLevel(String string) {
		this.dispScholarLevel = string;
	}
	
}

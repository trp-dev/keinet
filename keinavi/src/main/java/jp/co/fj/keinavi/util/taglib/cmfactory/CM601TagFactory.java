/*
 * 作成日: 2004/07/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.taglib.cmfactory;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;

import jp.co.fj.keinavi.forms.com_set.CM601Form;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM601TagFactory extends AbstractCMTagFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.taglib.cmfactory.AbstractCMTagFactory#createCMStatus(javax.servlet.ServletRequest)
	 */
	public String createCMStatus(ServletRequest request, HttpSession session) throws JspException {
		String result = null;
		Map item = getItemMap(session);
		List list = (List)item.get("0401");
		if (list == null || list.size() == 0) {
			result = "設定なし";
		} else {
			result = list.size() + "校";
		}
		return result;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.taglib.cmfactory.AbstractCMTagFactory#createTempCMStatus(javax.servlet.ServletRequest, javax.servlet.http.HttpSession)
	 */
	public String createTempCMStatus(ServletRequest request, HttpSession session) throws JspException {
		String result = null;
		CM601Form form = (CM601Form) super.getCommonForm(session).getActionForm("CM601Form");
		
		if (form.getSchool() == null || form.getSchool().length == 0) {
			result = "設定なし";
		} else {
			result = form.getSchool().length + "校";
		}
		
		return result;
	}

}

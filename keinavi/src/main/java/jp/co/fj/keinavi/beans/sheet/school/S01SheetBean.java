/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.excel.data.school.S01Item;
import jp.co.fj.keinavi.excel.data.school.S01KataListBean;
import jp.co.fj.keinavi.excel.data.school.S01KmkListBean;
import jp.co.fj.keinavi.excel.school.S01;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * @author kawai
 *
 */
public class S01SheetBean extends AbstractSheetBean {

	//	データクラス
	private final S01Item data = new S01Item();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		// データを詰める
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ
		
		// 受験人数・模試フラグ
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			ps1 = conn.prepareStatement(QueryLoader.getInstance().load("s01_1").toString());
			ps1.setString(1, exam.getExamYear()); // 対象年度
			ps1.setString(2, exam.getExamCD()); // 対象模試
			ps1.setString(3, exam.getExamYear()); // 対象年度
			ps1.setString(4, exam.getExamCD()); // 対象模試
			ps1.setString(5, exam.getExamYear()); // 対象年度
			ps1.setString(6, exam.getExamCD()); // 対象模試

			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				data.setIntAllNinzu(rs1.getInt(1));
				data.setIntGenNinzu(rs1.getInt(2));
				data.setIntSotuNinzu(rs1.getInt(3));
				data.setIntMshFlg("03".equals(exam.getTargetGrade())  ? 1 : 2);
			} else {
				throw new Exception("受験人数（全国）の取得に失敗しました。");
			}
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(ps1);
		}
		
		// 型・科目別成績リスト
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		try {
			ps2 = conn.prepareStatement(QueryLoader.getInstance().load("s01_2").toString());
			ps2.setString(1, exam.getExamYear()); // 対象年度
			ps2.setString(2, exam.getExamCD()); // 対象模試
			ps2.setString(3, exam.getExamYear()); // 対象年度
			ps2.setString(4, exam.getExamCD()); // 対象模試
			ps2.setString(5, exam.getExamYear()); // 対象年度
			ps2.setString(6, exam.getExamCD()); // 対象模試
			ps2.setString(7, exam.getExamYear()); // 対象年度
			ps2.setString(8, exam.getExamCD()); // 対象模試

			rs2 = ps2.executeQuery();
			while (rs2.next()) {
				// 科目
				if (rs2.getInt(1) < 7000) {
					S01KmkListBean k = new S01KmkListBean(); 
					k.setStrKmkCd(rs2.getString(1));
					k.setStrKmkmei(rs2.getString(2));
					k.setStrHaitenKmk(rs2.getString(3));
					k.setFloHeikinAll(rs2.getFloat(4));
					k.setFloHeikinGen(rs2.getFloat(5));
					k.setFloHeikinSotu(rs2.getFloat(6));
					k.setFloStdHensa(rs2.getFloat(7));
					k.setFloHensaAll(rs2.getFloat(8));
					k.setFloHensaGen(rs2.getFloat(9));
					k.setFloHensaSotu(rs2.getFloat(10));
					k.setIntMaxTen(rs2.getInt(11));
					k.setIntMinTen(rs2.getInt(12));
					k.setIntNinzuAll(rs2.getInt(13));
					k.setIntNinzuGen(rs2.getInt(14));
					k.setIntNinzuSotu(rs2.getInt(15));
					data.getS01KmkList().add(k);
				// 型					
				} else {
					S01KataListBean k = new S01KataListBean(); 
					k.setStrKmkCd(rs2.getString(1));
					k.setStrKmkmei(rs2.getString(2));
					k.setStrHaitenKmk(rs2.getString(3));
					k.setFloHeikinAll(rs2.getFloat(4));
					k.setFloHeikinGen(rs2.getFloat(5));
					k.setFloHeikinSotu(rs2.getFloat(6));
					k.setFloStdHensa(rs2.getFloat(7));
					k.setFloHensaAll(rs2.getFloat(8));
					k.setFloHensaGen(rs2.getFloat(9));
					k.setFloHensaSotu(rs2.getFloat(10));
					k.setIntMaxTen(rs2.getInt(11));
					k.setIntMinTen(rs2.getInt(12));
					k.setIntNinzuAll(rs2.getInt(13));
					k.setIntNinzuGen(rs2.getInt(14));
					k.setIntNinzuSotu(rs2.getInt(15));
					data.getS01KataList().add(k);
				}
			}
		} finally {
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(ps2);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S01_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.SCORE_TOTAL;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S01()
			.s01(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
	}

}

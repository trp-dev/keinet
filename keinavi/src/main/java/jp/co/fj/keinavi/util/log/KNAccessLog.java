package jp.co.fj.keinavi.util.log;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.util.KNCommonProperty;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * Kei-Naviアクセスログクラス
 *
 * 2006.09.27	[新規作成]
 * 2014.10.xx   FWEST)森 バナーログ追加対応
 * 2014.12.01   FWEST)森 バナーログファイル名変更
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.2
 *
 */
public class KNAccessLog {

    // サービス名
    private static final String SERVICE = KNCommonProperty.getServiceName();
    // Logger
    private static final Log LOG = LogFactory.getLog("accesslog");
    //2014/10 バナー広告追加対応
    private static final Log LOG2 = LogFactory.getLog("bannerlog1");
    //upd 20141201 FWEST森 バナーログファイル名変更 start
    //private static final Log LOG3 = LogFactory.getLog("bannerlog2");
    //private static final Log LOG4 = LogFactory.getLog("bannerlog3");
    //upd 20141201 FWEST森 バナーログファイル名変更 end
    private static  Log BANNERLOG;
    // コンストラクタ
    private KNAccessLog() {
    }

    // ログを出力する
    private static void output(final String remoteAddr,
            final String remoteHost, final LoginSession login,
            final String screenId, final String code,
            final String info) {

        // Infoログが無効ならここまで
        if (!LOG.isInfoEnabled()) {
            return;
        }

        final StringBuffer buff = new StringBuffer();

        // IPアドレス
        buff.append(filter(remoteAddr));
        buff.append(",");

        // ドメイン
        buff.append(filter(remoteHost));
        buff.append(",");

        // サービス種類
        buff.append(filter(getServiceType(login)));
        buff.append(",");

        // 学校ID
        buff.append(filter(login == null ? null : login.getUserID()));
        buff.append(",");

        // 学校名
        buff.append(filter(login == null ? null : login.getUserName()));
        buff.append(",");

        // 利用者ID
        buff.append(filter(login == null ? null : login.getAccount()));
        buff.append(",");

        // 部門コード
        if (login != null && login.getSectorCd() != null) {
            buff.append("kawai");
        }
        buff.append(filter(login == null ? null : login.getSectorCd()));
        buff.append(",");

        // ユーザ種別
        buff.append(filter(getUserType(login)));
        buff.append(",");

        // 画面ID
        buff.append(filter(screenId));
        buff.append(",");

        // 実行条件コード
        buff.append(filter(code));
        buff.append(",");

        // 補足情報
        buff.append(StringUtils.isEmpty(info) ? "-" : "\"" + info + "\"");

        LOG.info(buff);
    }

    //2014/10 バナー広告追加対応 start
    /**
    *
    * @param pattern     動作種別
    * @param remoteAddr  接続元アドレス
    * @param bannerCode1 バナー管理番号１
    * @param bannerCode2 バナー管理番号２
    * @param bannerName  バナー名
    * @param bannerUrl   接続URL
    */
   private static void outputBanner(String pattern, String remoteAddr,String serverName, String bannerCode1, String bannerCode2,
           String bannerName, String bannerUrl) {
       // TODO 自動生成されたメソッド・スタブ
       //サーバー名によって、LOGを選択
       //upd 20141201 FWEST森 バナーログファイル名変更 start
       //if("rksm07".equals(serverName))
       //{
           BANNERLOG=LOG2;
/*       }
       else if("rksm08".equals(serverName))
       {
           BANNERLOG=LOG3;
       }
       else if("rksm09".equals(serverName))
       {
           BANNERLOG=LOG4;
       }
       //本稼働削除 start
       //else
       //{
       //    BANNERLOG=LOG2;
       //}
       //本稼働削除 end
*/
       //upd 20141201 FWEST森 バナーログファイル名変更 end
       // Infoログが無効ならここまで
       if (!BANNERLOG.isInfoEnabled()) {
           return;
       }
       final StringBuffer buff = new StringBuffer();

       //動作種別
       buff.append(filter(pattern));
       buff.append(",");

       // 接続元アドレス
       buff.append(filter(remoteAddr));
       buff.append(",");

       // バナー管理番号１
       buff.append(filter(bannerCode1));
       buff.append(",");

       // バナー管理番号２
       buff.append(filter(bannerCode2));
       buff.append(",");

       // バナー名
       buff.append(filter(bannerName));
       buff.append(",");

       // 接続URL
       buff.append(filter(bannerUrl));
       //buff.append(",");

       BANNERLOG.info(buff);
   }
    //2014/10 バナー広告追加対応 end

    // nullはハイフンに変換する
    private static String filter(final String value) {
        return StringUtils.isEmpty(value) ? "-" : value;
    }

    // サービス種類を取得する
    private static String getServiceType(final LoginSession login) {

        // Kei-Navi
        if ("keinavi".equals(SERVICE)) {
            // ログイン前
            if (login == null) {
                return null;
            // 体験版
            } else if (login.isTrial()) {
                return "05";
            // ヘルプデスク
            } else if (login.isHelpDesk()) {
                return "04";
            // 営業部
            } else if (login.isSales() || login.isDeputy()) {
                return "03";
            // 非契約校
            } else if (login.isSimple()) {
                return "02";
            // 契約校
            } else {
                return "01";
            }
        // 校内成績処理システム
        } else if ("kounai".equals(SERVICE)) {
            // ログイン前
            if (login == null) {
                return null;
            // 体験版
            } else if (login.isTrial()) {
                return "15";
            // ヘルプデスク
            } else if (login.isHelpDesk()) {
                return "14";
            // 契約校
            } else {
                return "11";
            }
        // インターネット模試判定
        } else if ("inet".equals(SERVICE)) {
            return "21";
        } else {
            throw new IllegalArgumentException("サービス名が不正です。");
        }
    }

    // ユーザ種別を取得する
    private static String getUserType(final LoginSession login) {

        if (login == null) {
            return null;
        }

        // 塾内
        if (login.isHelpDesk() || login.isSales() || login.isDeputy()) {
            return "04";
        // 私塾
        } else if (login.isPrep()) {
            return "03";
        // 私立
        } else if (login.getUserDiv() == 2) {
            return "02";
        // 公立
        } else {
            return "01";
        }
    }

    /**
     * Lv1ログを出力する
     *
     * @param remoteAddr IPアドレス
     * @param remoteHost ドメイン
     * @param login ログイン情報
     * @param screenId 画面ID
     * @param code 実行条件コード
     * @param info 補足情報
     */
    public static void lv1(final String remoteAddr, final String remoteHost,
            final LoginSession login, final String screenId,
            final String code, final String info) {
        output(remoteAddr, remoteHost, login, screenId, code, info);
    }

    /**
     * Lv1ログを出力する
     *
     * @param remoteAddr IPアドレス
     * @param remoteHost ドメイン
     * @param login ログイン情報
     * @param screenId 画面ID
     * @param code 実行条件コード
     */
    public static void lv1(final String remoteAddr, final String remoteHost,
            final LoginSession login, final String screenId,
            final String code) {
        output(remoteAddr, remoteHost, login, screenId, code, null);
    }

    /**
     * Lv2ログを出力する
     *
     * @param remoteAddr IPアドレス
     * @param remoteHost ドメイン
     * @param login ログイン情報
     * @param screenId 画面ID
     * @param code 実行条件コード
     * @param info 補足情報
     */
    public static void lv2(final String remoteAddr, final String remoteHost,
            final LoginSession login, final String screenId,
            final String code, final String info) {
        output(remoteAddr, remoteHost, login, screenId, code, info);
    }

    /**
     * Lv2ログを出力する
     *
     * @param remoteAddr IPアドレス
     * @param remoteHost ドメイン
     * @param login ログイン情報
     * @param screenId 画面ID
     * @param code 実行条件コード
     */
    public static void lv2(final String remoteAddr, final String remoteHost,
            final LoginSession login, final String screenId,
            final String code) {
        output(remoteAddr, remoteHost, login, screenId, code, null);
    }

    //2014/10 バナー広告追加対応 start
    /**
     *
     * @param pattern     動作種別
     * @param remoteAddr  接続元アドレス
     * @param bannerCode1 バナー管理番号１
     * @param bannerCode2 バナー管理番号２
     * @param bannerName  バナー名
     * @param bannerUrl   接続URL
     *
     * @author FWEST)LIU
     *
     */
    public static void lv1s(String pattern, String remoteAddr,String serverName, String bannerCode1, String bannerCode2, String bannerName,
            String bannerUrl)
    {
        // TODO 自動生成されたメソッド・スタブ
        outputBanner(pattern,remoteAddr,serverName,bannerCode1,bannerCode2,bannerName,bannerUrl);

    }
    //2014/10 バナー広告追加対応 end
}

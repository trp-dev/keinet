/*
 * 作成日: 2004/08/31
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.admin;

import jp.co.fj.keinavi.forms.BaseForm;
/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class M301Form extends BaseForm {

	private String actionMode = null;
	private String targetYear = null;
	private String targetGrade = null;
	private String targetClassgCd = null;

	/**
	 * @return
	 */
	public String getActionMode() {
		return actionMode;
	}

	/**
	 * @param string
	 */
	public void setActionMode(String string) {
		actionMode = string;
	}

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 


}

	/**
	 * @return
	 */
	public String getTargetGrade() {
		return targetGrade;
	}

	/**
	 * @return
	 */
	public String getTargetYear() {
		return targetYear;
	}


	/**
	 * @param string
	 */
	public void setTargetGrade(String string) {
		targetGrade = string;
	}

	/**
	 * @param string
	 */
	public void setTargetYear(String string) {
		targetYear = string;
	}

	/**
	 * @return
	 */
	public String getTargetClassgCd() {
		return targetClassgCd;
	}

	/**
	 * @param string
	 */
	public void setTargetClassgCd(String string) {
		targetClassgCd = string;
	}

}
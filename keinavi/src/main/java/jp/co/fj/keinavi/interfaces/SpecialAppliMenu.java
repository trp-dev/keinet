package jp.co.fj.keinavi.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 
 * 特例成績データ作成承認システムのメニューを表す定数クラスです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SpecialAppliMenu implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = -6622306491931600223L;

	/** セッションキー */
	public static final String SESSION_KEY = "SpecialApplicationMenu";

	/** 特例成績データ作成申請 */
	public static final SpecialAppliMenu APPLY = new SpecialAppliMenu(
			"MENU_APPLY", "94", "31", "0010");

	/** 特例成績データ作成承認（営業部） */
	public static final SpecialAppliMenu ACCEPT_EIGYO = new SpecialAppliMenu(
			"MENU_ACCEPT_EIGYO", "94", "31", "0020");

	/** 特例成績データ作成承認（高校事業企画部） */
	public static final SpecialAppliMenu ACCEPT_KIKAKU = new SpecialAppliMenu(
			"MENU_ACCEPT_KIKAKU", "94", "31", "0030");

	/** 特例成績データ作成承認（模試運用管理部）、 特例成績データ申請一覧確認 */
	public static final SpecialAppliMenu ACCEPT_KANRI = new SpecialAppliMenu(
			"MENU_ACCEPT_KANRI", "94", "31", "0040");

	/** 特例成績データダウンロード */
	public static final SpecialAppliMenu DOWNLOAD = new SpecialAppliMenu(
			"MENU_DOWNLOAD", "94", "31", "0050");

	/** メニューリスト */
	public static final List LIST;
	static {
		List list = new ArrayList(5);
		list.add(APPLY);
		list.add(ACCEPT_EIGYO);
		list.add(ACCEPT_KIKAKU);
		list.add(ACCEPT_KANRI);
		list.add(DOWNLOAD);
		LIST = Collections.unmodifiableList(list);
	}

	/** ID */
	public final String id;

	/** 業務ID */
	public final String gymcod;

	/** メニューコード */
	public final String mnucod;

	/** 画面コード */
	public final String gmnban;

	/**
	 * コンストラクタです。
	 */
	private SpecialAppliMenu(String id, String gymcod, String mnucod,
			String gmnban) {
		this.id = id;
		this.gymcod = gymcod;
		this.mnucod = mnucod;
		this.gmnban = gmnban;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SpecialAppliMenu)) {
			return false;
		}
		SpecialAppliMenu other = (SpecialAppliMenu) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

}

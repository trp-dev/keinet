/*
 * 作成日: 2004/06/30
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.individual.IUtilBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.com_set.ChargeClassData;
import jp.co.fj.keinavi.data.profile.DefaultProfile;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.interfaces.ILogin;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.servlets.KNServletException;





/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ILoginServlet extends HttpServlet {
	
	//利用可能な模試
	static String[][] examSession2004= {
										{"2004", "01","第一回マーク模試","200040512"},
										{"2004", "02","第二回マーク模試","200040512"},
										{"2004", "03","第三回マーク模試","200040512"},
										{"2004", "04","第四回マーク模試","200040512"},
										{"2004", "05","第一回マーク模試","200040512"},
										{"2004", "07","第二回マーク模試","200040512"},
										{"2004", "66","プレ何とか模試","200040512"}};
	static String[][] examSession2003 = {
										{"2003", "01","第一回マーク模試","200040512"},
										{"2003", "02","第二回マーク模試","200040512"},
										{"2003", "07","第一回なんとか模試","200040512"},
										{"2003", "06","第一回なんたら模試","200040512"}};
	//担当クラス			
	static String[][] chargeClassData = {{"3","A"},{"3","B"},{"3","C"},{"2","A"},{"2","B"},
											{"2","C"},{"1","A"},{"1","B"},{"1","C"},{"5","A"}};
	
	//選択中の生徒
	static String[] studentSelected ={"0400001001", "0400001002" ,"0400001003", "0400001004"};
	
	public void doPost (HttpServletRequest request,
	HttpServletResponse response) throws ServletException, IOException{
		doGet(request, response);
	}
	public void doGet (HttpServletRequest request,
	HttpServletResponse response) throws ServletException, IOException{
		
		/** Loginの作成 */
		LoginSession login = new LoginSession();
		login.setUserMode(ILogin.SCHOOL_NORMAL);
		HttpSession session = request.getSession();
		session.setAttribute(LoginSession.SESSION_KEY, login);
		
		/** Sampleデータの作成**/	
		setSampleData(request);
		
		RequestDispatcher dispatch = request.getRequestDispatcher("/I001Servlet");
		dispatch.forward(request, response);
	}
	private void setSampleData(HttpServletRequest request) throws KNServletException{
		try{
			//個人成績分析機能において共通の処理を行う
			//プロファイルデータのセットとゲットなど
			HttpSession session = request.getSession();
			Profile profile = (Profile) session.getAttribute(Profile.SESSION_KEY);
			if(profile == null){
				profile = new Profile();
				/** デフォルトプロファイルの値をコピー */
				profile.setCategoryMap(new DefaultProfile().getCategoryMap());
				session.setAttribute(Profile.SESSION_KEY, profile);
			}
			setRealSampleData(profile, request);
		}catch(Exception e){
			KNServletException k = new KNServletException("0I001にてプロファイルデータのセットに失敗しました。");
			k.setErrorCode("00I001060203");
			throw k;
		}
	}
	private void setRealSampleData(Profile profile, HttpServletRequest request) throws KNServletException{
		
		try{
			//bundleCdをセット
			profile.setBundleCD("00001");
			
			//chargeClassDatasの作成
			ArrayList chargeClassDatas = new ArrayList();
			chargeClassDatas.add(new ChargeClassData((short)3, "A"));
			chargeClassDatas.add(new ChargeClassData((short)3, "B"));
			chargeClassDatas.add(new ChargeClassData((short)3, "C"));
			chargeClassDatas.add(new ChargeClassData((short)3, "D"));
			chargeClassDatas.add(new ChargeClassData((short)3, "E"));
			chargeClassDatas.add(new ChargeClassData((short)3, "F"));
			chargeClassDatas.add(new ChargeClassData((short)2, "A"));
			chargeClassDatas.add(new ChargeClassData((short)2, "B"));
			chargeClassDatas.add(new ChargeClassData((short)2, "C"));
			chargeClassDatas.add(new ChargeClassData((short)2, "D"));
			chargeClassDatas.add(new ChargeClassData((short)2, "E"));
			chargeClassDatas.add(new ChargeClassData((short)1, "A"));
			chargeClassDatas.add(new ChargeClassData((short)1, "B"));
			chargeClassDatas.add(new ChargeClassData((short)1, "C"));
			chargeClassDatas.add(new ChargeClassData((short)1, "D"));
			chargeClassDatas.add(new ChargeClassData((short)1, "E"));
			profile.getItemMap(IProfileCategory.CM).put(IProfileItem.CLASS, chargeClassDatas);
			
			
			//examSessionの作成
			ExamSession eSession = new ExamSession();
			eSession.setExamMap(new TreeMap());
			eSession.setYears(new String[]{"2003", "2004"});
			List list04 = new ArrayList();
			for(int i=0; i<examSession2004.length; i++){
				ExamData ed = new ExamData();
				ed.setExamYear(examSession2004[i][0]);
				ed.setExamCD(examSession2004[i][1]);
				ed.setExamName(examSession2004[i][2]);
				ed.setOutDataOpenDate(examSession2004[i][3]);
				list04.add(ed);
			}
			eSession.getExamMap().put("2004", list04);
			List list03 = new ArrayList();
			for(int i=0; i<examSession2003.length; i++){
				ExamData ed = new ExamData();
				ed.setExamYear(examSession2003[i][0]);
				ed.setExamCD(examSession2003[i][1]);
				ed.setExamName(examSession2003[i][2]);
				ed.setOutDataOpenDate(examSession2003[i][3]);
				list03.add(ed);
			}
			eSession.getExamMap().put("2003", list03);
			request.getSession().setAttribute(ExamSession.SESSION_KEY, eSession);
			
			
			//studentSelectedの作成
			String studentString = IUtilBean.concatComma(studentSelected);
			profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.STUDENT_SELECTED, studentString);		
			
			request.getSession().setAttribute(Profile.SESSION_KEY, profile);
			
		}catch(Exception e){
			KNServletException k = new KNServletException("0I001にてエラーが発生しました。");
			k.setErrorCode("00I001010101");
			throw k;
		}
	}
}

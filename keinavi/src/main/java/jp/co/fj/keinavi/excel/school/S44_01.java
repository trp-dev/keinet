package jp.co.fj.keinavi.excel.school;

//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.io.FileInputStream;
//import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S44GakkoListBean;
import jp.co.fj.keinavi.excel.data.school.S44HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.school.S44Item;
import jp.co.fj.keinavi.excel.data.school.S44ListBean;

import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
//import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 * 作成日: 2004/07/13
 * @author C.Murata
 *
 * 校内成績分析−過年度比較−志望大学評価別人数　大学（日程なし）出力処理
 * 
 */
public class S44_01 {
	
	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private String		strArea			= "A1:AG58";	//印刷範囲	
	private String		mastersheet 	= "tmp";		//テンプレートシート名
	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow		= 50;			//MAX行数
	private int		intMaxClassSr   = 5;			//MAXクラス数

	private int intDataStartRow = 8;					//データセット開始行
	private int intDataStartCol = 8;					//データセット開始列
	
	final private String masterfile0 = "S44_01" ;
	final private String masterfile1 = "S44_01" ;
	private String masterfile = "" ;
	
	final private String strCenterResearch = "38";

	/*
	 * 	Excel編集メイン
	 * 		S44Item S44Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int s44_01EditExcel(S44Item s44Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		
		boolean bolBookCngFlg = true;			//改ファイルフラグ	
		boolean bolSheetCngFlg = true;			//改シートフラグ
			
		FileInputStream	fin = null;
		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFCell	workCell		= null;
		HSSFRow		workRow			= null;
		
		//テンプレートの決定
		if (s44Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intRow = 0;
		int intCol = 0;

		String strGenekiKbn = "";
		String strDaigakuCode = "";
		String strHyoukaKbn = "";
		String strNendo = "";
		
		//2004.09.27Add
		String strKokushiKbn = "";
		String strStKokushiKbn = "";
		//2004.09.27AddEnd

		int intLoopCnt = 0;
		int intNinzuListCnt = 0;
		int intSheetCngCount = 0;
		int intBookCngCount = -1;
		int intChangeSheetRow = 0;
		
		boolean bolDispHyoukaFlg = false;
		boolean bolDispDaigakuFlg = false;
		String strHyouka = "";
		
		String strDispTarget = "";
		
		ArrayList WorkBookList = new ArrayList();
		ArrayList WorkSheetList = new ArrayList();
		
		int intSheetCnt=0;
		
		int intBookIndex = -1;
		int intSheetIndex = -1;
		
		int intSheetMei = -1;
		
		int intSetRow = intDataStartRow;
		
		boolean bolClassDispFlg=true;
		
		KNLog log = KNLog.getInstance(null,null,null);
		ArrayList HyoukaTitleList = new ArrayList();
		
		log.Ep("S44_01","S44_01帳票作成開始","");

		// 基本ファイルを読込む
		S44ListBean s44ListBean = new S44ListBean();
		
		try{
			
			if(cm.toString(s44Item.getStrMshCd()).equals(cm.toString(strCenterResearch))){
				HyoukaTitleList.add(0,"◎");
				HyoukaTitleList.add(1,"○");
				HyoukaTitleList.add(2,"△");
			}else{
				HyoukaTitleList.add(0,"A");
				HyoukaTitleList.add(1,"B");
				HyoukaTitleList.add(2,"C");
			}
			
			ArrayList s44List = s44Item.getS44List();
			ListIterator itr = s44List.listIterator();
			
//			if( itr.hasNext() == false ){
//				log.Err("01S44_01","帳票作成エラー","");
//				return errfdata;
//			}
			
			while( itr.hasNext() ){
				
				s44ListBean = (S44ListBean)itr.next();
				
				if( !cm.toString(strGenekiKbn).equals(cm.toString(s44ListBean.getStrGenKouKbn())) ){
					strGenekiKbn = s44ListBean.getStrGenKouKbn();
					if( cm.toString(strGenekiKbn).equals("0") ){
						strDispTarget = "全体";
					}
					else if( cm.toString(strGenekiKbn).equals("1") ){
						strDispTarget = "現役生";
					}
					else if( cm.toString(strGenekiKbn).equals("2") ){
						strDispTarget = "高卒生";
					}

					if( bolSheetCngFlg == false ){
						bolSheetCngFlg = true;
					}
				}
				
				bolDispHyoukaFlg = false;
				if( !cm.toString(strHyoukaKbn).equals(cm.toString(s44ListBean.getStrHyouka())) ){
					bolDispHyoukaFlg = true;
					//評価区分表示
					strHyoukaKbn = s44ListBean.getStrHyouka();
					strHyouka = "";
					switch( Integer.parseInt(strHyoukaKbn) ){
						case 1:
							strHyouka = "センター";
							break;
						case 2:
							strHyouka = "2次・私大";
							break;
						case 3:
							strHyouka = "総合";
							break;
					}
//					workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
//					workCell.setCellValue( strHyoka );
				
//					//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
//					cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
//												intSetRow, intSetRow, 1, 1, false, true, false, false);
				
				}
				
				//2004.9.27 Add
				int intDaigakuCd = (int)Integer.parseInt(s44ListBean.getStrDaigakuCd());
				if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
					strKokushiKbn = "国公立大";
				}
				if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
					strKokushiKbn = "私立大";
				}
				if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
					strKokushiKbn = "短大";
				}
				if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
					strKokushiKbn = "その他";
				}
				
				if( (!cm.toString(strKokushiKbn).equals(cm.toString(strStKokushiKbn))) ){
					strStKokushiKbn = strKokushiKbn;
					bolSheetCngFlg = true;
				}
				//2004.9.27 Add End

				bolDispDaigakuFlg = false;
				if( !cm.toString(strDaigakuCode).equals(cm.toString(s44ListBean.getStrDaigakuCd()))){
					bolDispDaigakuFlg = true;
					bolDispHyoukaFlg = true;
					//大学名表示
					strDaigakuCode = s44ListBean.getStrDaigakuCd();
//					workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 0 );
//					workCell.setCellValue(s44ListBean.getStrDaigakuMei());
//					//罫線出力(セルの上部に太線を引く）
//					cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
//														intSetRow, intSetRow, 0, 32, false, true, false, false );
				}
				
				//クラス別評価人数リスト読み込み クラス名をExcelへセット
				ArrayList s44GakkoList = s44ListBean.getS44GakkoList();
				ListIterator itr2 = s44GakkoList.listIterator();
				
				S44GakkoListBean s44GakkoListBean = new S44GakkoListBean();
				S44GakkoListBean s44GakkoListBeanHomeData = new S44GakkoListBean();
				
				S44GakkoListBean HomeDataBean = new S44GakkoListBean();

				//クラス数取得
				int intClassSr = s44GakkoList.size();
				
				int intSheetSr = 0;
				if( (intClassSr - 1) % intMaxClassSr > 0 ){
					intSheetSr = (int)Math.ceil((intClassSr - 1) / intMaxClassSr) + 1;
				}else{
					intSheetSr = (int)Math.ceil((intClassSr - 1) / intMaxClassSr);
				}
				
				int intLoopCnt2 = 0;
				int intNendoCount = 0;
				intChangeSheetRow = 0;
				
				boolean bolHomeDataDispFlg=false; 

				while( itr2.hasNext() ){
					
					String strClassmei = "";
					s44GakkoListBean = (S44GakkoListBean)itr2.next();

					ArrayList s44HyoukaNinzuList = s44GakkoListBean.getS44HyoukaNinzuList();
					S44HyoukaNinzuListBean s44HyoukaNinzuListBean = new S44HyoukaNinzuListBean();
					ListIterator itr3 = s44HyoukaNinzuList.listIterator();
					
					if( intLoopCnt2 == 0 ){
						bolHomeDataDispFlg = true;
						HomeDataBean = s44GakkoListBean;
						s44GakkoListBean = (S44GakkoListBean)itr2.next();
						intNendoCount = s44HyoukaNinzuList.size();
						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
						intLoopCnt2++;

						s44HyoukaNinzuList = s44GakkoListBean.getS44HyoukaNinzuList();
						s44HyoukaNinzuListBean = new S44HyoukaNinzuListBean();
						itr3 = s44HyoukaNinzuList.listIterator();
					}

					if( intLoopCnt2 % intMaxClassSr == 0 ){
						intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 5 );
					}else{
						intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 5 );
					}
					
//					intCol = intDataStartCol + ( ( intMaxClassSr - ( intLoopCnt2 % intMaxClassSr ) ) * 5 );
					
					
					int intHyoukaNinzuListIndex = 0;
					while( itr3.hasNext() ){
						
						s44HyoukaNinzuListBean = (S44HyoukaNinzuListBean)itr3.next();
						
						if( intSetRow  >= intChangeSheetRow ||((bolSheetCngFlg == true)&&(intMaxSheetIndex != 0))){
							bolSheetCngFlg = true;
							for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
								int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
								workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
								workSheet = (HSSFSheet)WorkSheetList.get(intSindex);
								
								//罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intSetRow, intSetRow, 0, 32, false, true, false, false );
								if( workSheet != null ){
									int intRemoveStartRow = intChangeSheetRow;
									if( intMaxRow + intDataStartRow > intRemoveStartRow ){
										for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
											//行削除処理
											workSheet.removeRow(workSheet.getRow(index));
										}
										//罫線出力（セルの上部に太線を出力）
										cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
														intRemoveStartRow, intRemoveStartRow, 0, 32, false, true, false, false);
									}
								}
			
							}
						}
						
						if( bolSheetCngFlg == true ){
							
							bolDispHyoukaFlg = true;
							bolDispDaigakuFlg = true;
							
							intSetRow = intDataStartRow;
							bolClassDispFlg = true;
							bolHomeDataDispFlg = true;
							
							for( int intIndex = 0; intIndex < intSheetSr; intIndex ++ ){
								
								if( ( intMaxSheetIndex >= intMaxSheetSr ) || ( bolBookCngFlg == true ) ){
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook == null ){
										log.Err("02S44_01","帳票作成エラー","");
										return errfread;
									}
									WorkBookList.add(workbook);
									intBookIndex++;

									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
									workbook = (HSSFWorkbook)WorkBookList.get(intBookIndex);
								}
								
								//シート作成
								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);
								WorkSheetList.add(workSheet);
								intSheetIndex++;
								workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex);

								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);
				
								// セキュリティスタンプセット
								String secuFlg = cm.setSecurity( workbook, workSheet, s44Item.getIntSecuFlg(), 30, 32 );
								workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
								workCell.setCellValue(secuFlg);
				
								// 学校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
								workCell.setCellValue( "学校名　：" + s44Item.getStrGakkomei() );
		
								// 模試月取得
								String moshi =cm.setTaisyouMoshi( s44Item.getStrMshDate() );
								
								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
								workCell.setCellValue( "対象模試：" + s44Item.getStrMshmei() + moshi);
					
								//表示対象
								workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
								workCell.setCellValue( "表示対象：" + strDispTarget );
								
								//評価別人数 見出し行
								for( int i = 0; i <= intMaxClassSr; i++ ){
//								for( int i = 0; i <= intClassSr-1; i++ ){
									int intTitleSetCol = (i * 5) + ( intDataStartCol - 3 );
									
									//出願予定・第1志望
									if (strKokushiKbn == "国公立大") { 
										workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
										workCell.setCellValue( "出願予定" );
									} else {
										workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
										workCell.setCellValue( "第１志望" );
									}
								
									//評価別人数 見出し行
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol );
									workCell.setCellValue( (String)HyoukaTitleList.get(0) );
									
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+1 );
									workCell.setCellValue( (String)HyoukaTitleList.get(1) );
									
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+2 );
									workCell.setCellValue( (String)HyoukaTitleList.get(2) );
								}
							
								intMaxSheetIndex++;		//シート数カウンタインクリメント
								bolSheetCngFlg = false;
						
								intLoopCnt = 0;
							}	//ForEnd
							
						}	//if( bolSheetCngFlg == true )
						
						intSheetCnt = (int)Math.ceil( (double)intLoopCnt2 / (double)intMaxClassSr );
						workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex -  (intSheetSr - intSheetCnt)  );

						int intSheetNo = intSheetIndex -  (intSheetSr - intSheetCnt);
						int intBIndex = (int)Math.ceil((double)(intSheetNo + 1) / (double)intMaxSheetSr );
						workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
										

						
						if( intCol == intDataStartCol ){
							
							//データセット
							//自校データ・大学名ほか表示
							//自校学校名
							workCell = cm.setCell( workSheet, workRow, workCell, 5, 3 );
							workCell.setCellValue(HomeDataBean.getStrGakkomei());
							//自校データ
							ListIterator itrHome = HomeDataBean.getS44HyoukaNinzuList().listIterator();
							int intLoopIndex = 0;
							while( itrHome.hasNext() ){
								S44HyoukaNinzuListBean HomeHyoukaNinzuListBean = (S44HyoukaNinzuListBean) itrHome.next();
								boolean ret = setData( workSheet, HomeHyoukaNinzuListBean, intSetRow + intLoopIndex , 3 );
								if( ret == false ){
									log.Err("0"+ret+"S44_01","帳票作成エラー","");
									return errfdata;
								}
								intLoopIndex++;
							}
							
							if( bolDispHyoukaFlg == true ){
								//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
															intSetRow, intSetRow, 1, 1, false, true, false, false);
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
								workCell.setCellValue( strHyouka );
				
							}

							if( bolDispDaigakuFlg == true ){
								//罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intSetRow, intSetRow, 0, 32, false, true, false, false );
								//大学名表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 0 );
								workCell.setCellValue(s44ListBean.getStrDaigakuMei());
							}
							
						}
					

						bolHomeDataDispFlg = false;

						boolean ret = setData( workSheet, s44HyoukaNinzuListBean, intSetRow + intHyoukaNinzuListIndex, intCol);
						if( ret != true){
							log.Err("0"+ret+"S44_01","帳票作成エラー","");
							return errfdata;
						}
						else{
							intHyoukaNinzuListIndex++;
							intLoopCnt++;
						}
						
						//
						
					}	//WhileEnd( itr3 )
					
					
					//クラス名
					if( bolClassDispFlg == true ){
//						String strGakunen = s44GakkoListBean.getStrGrade();
						String strGakkomei = s44GakkoListBean.getStrGakkomei();
						workCell = cm.setCell( workSheet, workRow, workCell, 5, intCol );
						workCell.setCellValue(strGakkomei);
					}
					intLoopCnt2++;
				}	//WhileEnd( itr2 )

				intSetRow += intNendoCount;
				bolClassDispFlg = false;

			}	//WhileEnd( itr )
			
			for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
				int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
				workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
				workSheet = (HSSFSheet)WorkSheetList.get(intSindex);
								
				//罫線出力(セルの上部に太線を引く）
				cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
													intSetRow, intSetRow, 0, 32, false, true, false, false );
			
				if( workSheet != null ){
					int intRemoveStartRow = intChangeSheetRow;
					if( intMaxRow + intDataStartRow > intRemoveStartRow ){
						for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
							//行削除処理
							workSheet.removeRow(workSheet.getRow(index));
						}
						//罫線出力（セルの上部に太線を出力）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
										intRemoveStartRow, intRemoveStartRow, 0, 32, false, true, false, false);
					}
				}

			}

			//ファイル保存
			for( int intBookSr = 0; intBookSr < WorkBookList.size(); intBookSr++ ){
				workbook = (HSSFWorkbook)WorkBookList.get(intBookSr);
				
//				// 元のテンプレートの削除
//				workbook.removeSheetAt(workbook.getSheetIndex(mastersheet));

				boolean bolRet = false;
				// Excelファイル保存
				if( WorkBookList.size() > 1 ){
					if( WorkBookList.size() - 1 == intBookSr ){
						bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookSr+1, masterfile, intMaxSheetIndex);
					}else{
						bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookSr+1, masterfile, intMaxSheetSr);
					}
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}

//				if(intBookCngCount > 0){
//					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
//				}else{
//					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
//				}

				if( bolRet == false ){
					log.Err("0"+bolRet+"S44_01","帳票作成エラー","");
					return errfwrite;
				}
				intBookCngCount++;
			}
			
			//データ無し
			if ( WorkBookList.size() == 0 ){
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex = 1;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s44Item.getIntSecuFlg(), 30, 32 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 30 );
				workCell.setCellValue(secuFlg);
				
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s44Item.getStrGakkomei()) );
		
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s44Item.getStrMshDate() );
								
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
				workCell.setCellValue( "対象模試：" + s44Item.getStrMshmei() + moshi);

				//表示対象
				workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
				workCell.setCellValue( "表示対象：" );
								
				boolean bolRet = false;
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}
			
		}
		catch( Exception e ){
			log.Err("99S44","帳票作成エラー",e.toString());
			return errfdata;
		}

		log.Ep("S44_01","S44_01帳票作成終了","");

		return noerror;
	}

			
		
	/*
	 * 	Excel編集
	 * 		HSSFWorkbook 	workbook: Excelワークブック
	 * 		ArrayList 		s44HyoukaNinzuList:評価別人数リスト
	 * 		int				intRow:開始行
	 * 		int				intCol:開始列
	 * 		戻り値: なし
	 */
	private boolean setData(HSSFSheet workSheet, S44HyoukaNinzuListBean s44HyoukaNinzuListBean, int intRow, int intCol ){

		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		int		maxSheetIndex	= 0;
		int		row = intRow;
		
		
		// 基本ファイルを読込む
		try{
			
			//模試年度
			workCell = cm.setCell( workSheet, workRow, workCell, row, 2);
			workCell.setCellValue( s44HyoukaNinzuListBean.getStrNendo() + "年度" );
			
			//全国総志望者数
			if( s44HyoukaNinzuListBean.getIntSoshibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol);
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntSoshibo() );
			}
			
			//志望者数（第一志望）
			if( s44HyoukaNinzuListBean.getIntDai1shibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 1);
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntDai1shibo() );
			}
			
			//評価別人数
			if( s44HyoukaNinzuListBean.getIntHyoukaA() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 2 );
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntHyoukaA() );
			}

			if( s44HyoukaNinzuListBean.getIntHyoukaB() != -999 ){			
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntHyoukaB() );
			}
			
			if( s44HyoukaNinzuListBean.getIntHyoukaC() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntHyoukaC() );
			}

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
}
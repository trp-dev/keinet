/*
 * 作成日: 2004/06/30
 */
package jp.co.fj.keinavi.servlets.login;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.login.SectorData;
import jp.co.fj.keinavi.data.login.SectorSession;
import jp.co.fj.keinavi.forms.login.WB002Form;
import jp.co.fj.keinavi.interfaces.ILogin;
import jp.co.fj.keinavi.servlets.DefaultLoginServlet;

/**
 * 営業・校舎選択画面サーブレット<BR>
 * 
 * @author kawai
 */
public class WB002Servlet extends DefaultLoginServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {
		
		// ログイン情報
		final LoginSession login = getLoginSession(request);
		// アクションフォーム
		final WB002Form form = (WB002Form) getActionForm(request,
			"jp.co.fj.keinavi.forms.login.WB002Form");

		// ログアウト
		if ("logout".equals(getForward(request))) {
			dispatch(request, response);
		// 転送元がログイン画面
		} else if ("wb001".equals(getBackward(request))) {
			forward(request, response, JSP_WB002);
		//「決定」ボタンを押している場合
		} else {
			// 営業部
			if ("1".equals(form.getMenu())) {
				forward(request, response, "/SalesSelect");
			// 校舎
			} else {
				// 校舎の部門データをカウントし、部門分類コードも取得しておく
				int count = 0;
				String sectorSortingCD = null;
				SectorSession sector = super.getSectorSession(request);
				Iterator ite = sector.getSectorList().iterator();
				while (ite.hasNext()) {
					SectorData data = (SectorData)ite.next();
					if ("2".equals(data.getHsBusiDivCD())) {
						count++;
						sectorSortingCD = data.getSectorSortingCD();
					} 
				}
				// 1つだけなら校舎用メニューへ飛ばす
				if (count == 1) {
					login.setLoginID(form.getAccount());
					login.setUserID(sectorSortingCD);
					login.setSectorSortingCD(sectorSortingCD);
					login.setUserMode(ILogin.CRAM_NORMAL);
//					super.forward(request, response, SERVLET_DISPATCHER);
				// そうでなければ選択画面へ
				} else {
//					super.forward(request, response, "/CramSelect");
				}
				// 工事中
				super.forward(request, response, JSP_UC);
			}
		}
	}

}

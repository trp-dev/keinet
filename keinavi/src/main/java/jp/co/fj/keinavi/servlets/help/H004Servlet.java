/*
 * 作成日: 2004/09/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.help;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.beans.help.HelpSearchBean;
import jp.co.fj.keinavi.beans.help.HelpListBean;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.forms.help.H004Form;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class H004Servlet extends DefaultHttpServlet {
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(

		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ

		// セッション
		HttpSession session = request.getSession();

		// ログイン情報
		LoginSession login = super.getLoginSession(request);

		// アクションフォーム
		H004Form form = null;
		// requestから取得する
		try {
			form = (H004Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.help.H004Form");
		} catch (Exception e) {
			throw new ServletException(e);
		}
		
		if ( "h004".equals(getForward(request)) ) {
			// DBコネクション
			Connection con = null;
			try {
				con = super.getConnectionPool(request);
				// ヘルプ検索Bean
				HelpSearchBean bean = new HelpSearchBean();
				bean.setConnection(null, con);
				bean.setCategory(form.getCategory());
				bean.setFreeWord(form.getFreeWord());
				bean.execute();
				bean.setPageSize(20); // ページサイズは20で固定
				bean.setPageNo(Integer.parseInt(form.getPage())); // ページ番号
				request.setAttribute("SearchBean", bean);

				// パンくず表示のためのダミー	
				HelpListBean hlb = new HelpListBean();
				hlb.setFormId("");
				hlb.setCategory(form.getCategory());
				hlb.setFreeWord(form.getFreeWord());
				hlb.setDetailBack("");
				hlb.setConnection(null, con);
				hlb.execute();
				request.setAttribute("HelpListBean", hlb);

			} catch (Exception e) {
				throw new ServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}
			
			super.forward(request, response, JSP_H004);

		
		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
		
	}

}

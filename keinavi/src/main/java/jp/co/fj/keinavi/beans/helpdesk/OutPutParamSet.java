/*
 * 作成日: 2015/10/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.helpdesk;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.util.KNCommonProperty;

/**
 * ユーザ管理（証明書TSV出力パラメータ設定）
 *
 * @author Hiroyuki Nishiyama - QuiQsoft
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class OutPutParamSet {

	// 画面ID
	private String id = "";
	// セッションID
	private String sessionId = "";
	// 出力ファイルの種類 --> 0:証明書, 1:ユーザ登録, 2:ユーザ削除
	private String fileKind = "";
	// 日付
	private Date date = new Date();

	// ファイル分割単位
	private int divNum = 0;

	// 出力テキスト種別
	private int outType = 1;

	// 出力対象項目
	private Integer target[] = null;

	// 出力ファイル
	private File outPutFile = null;
	// 出力ファイル名
	private String fileName = null;

	// 出力日時フォーマッタ
	private static final SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSS");

	// 連番フォーマッタ
	private DecimalFormat seqFormat = null;

	/**
	 * コンストラクタ
	 * @param login
	 * @param sessionID
	 * @param id
	 * @param content
	 * @param fileKind
	 * @throws Exception
	 */
	public OutPutParamSet(LoginSession login, String sessionID, String id, String fileKind) throws Exception {

		this.id = id;
		this.sessionId = sessionID;
		this.fileKind = fileKind;

		// 出力項目の設定
		this.target = getOutTarget();
	}


	/**
	 * ファイルオブジェクト作成する
	 * @param seq
	 */
	protected void createOutPutFile(int seq) throws Exception {

		// 出力ファイルの設定
		this.fileName = this.getOutputFileName(seq);
		this.outPutFile =
			    new File(
			        KNCommonProperty.getKNTempPath()
			        + File.separator + sessionId
			        + File.separator + fileName
		        );
	}

	/**
	 * 出力ファイル名を取得する
	 * @param seq
	 * @return ファイル名
	 */
	private String getOutputFileName(int seq) {

		StringBuffer buff = new StringBuffer();
		StringBuffer seqBuff = new StringBuffer();
		String timeStamp = null;

	    if ("0".equals(fileKind))  buff.append("cert_issue_");
	    else if ("1".equals(fileKind))  buff.append("user_regist_");
	    else if ("2".equals(fileKind))  buff.append("user_delete_");
	    else throw new IllegalArgumentException("不正な出力パターンです。" + fileKind);

	    // 連番
	    for (int i = 1; i <= Integer.toString(divNum).length(); i++) {
	    	seqBuff.append("0");
	    }
	    seqFormat = new DecimalFormat(seqBuff.toString());
	    buff.append(seqFormat.format(seq));

	    // タイムスタンプ
	    timeStamp = format.format(date);
	    // 「yyyyMMddHHmmssSS」をオーバーする場合は先頭から16桁
	    if (timeStamp.length() > 16) {
	    	timeStamp = timeStamp.substring(0, 16);
	    }
	    buff.append("_" + timeStamp);
	    //buff.append(".csv");
	    buff.append(".tsv");

	    return buff.toString();
	}

	/**
	 * 出力項目の取得する
	 * @return 項目キーの配列
	 */
	private Integer[] getOutTarget() {

		List list = new ArrayList();
		int colNum = 0;

	    if ("0".equals(fileKind))       colNum = 13;
	    else if ("1".equals(fileKind))  colNum = 22;
	    else if ("2".equals(fileKind))  colNum = 1;
	    else throw new IllegalArgumentException("不正な出力パターンです。" + fileKind);

		for ( int i = 1; i <= colNum; i++ ) {
			list.add( new Integer(i) );
		}

		return (Integer[]) list.toArray(new Integer[0]);
	}

	/**
	 * @return
	 */
	public String getFileKind() {
		return fileKind;
	}

	/**
	 * @return
	 */
	public void setFileKind(String str) {
		fileKind = str;
	}

	/**
	 * @return
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @return
	 */
	public void setDate(Date dt) {
		date = dt;
	}


	/**
	 * @return
	 */
	public int getDivNum() {
		return divNum;
	}

	/**
	 * @return
	 */
	public void setDivNum(int num) {
		divNum = num;
	}

	/**
	 * @return
	 */
	public File getOutPutFile() {
		return outPutFile;
	}

	/**
	 * @return
	 */
	public int getOutType() {
		return outType;
	}

	/**
	 * @return
	 */
	public Integer[] getTarget() {
		return target;
	}

	/**
	 * @return
	 */
	public String getFileName() {
		return fileName;
	}

}
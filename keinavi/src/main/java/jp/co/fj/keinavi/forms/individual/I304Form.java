/*
 * 作成日: 2004/08/10
 *
 */
package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * @author Totec) T.Yamada
 * 
 * 2005.3.10 	T.Yamada 	[1]javascriptに渡す用の年度プロパティ追加
 * 2005.4.26	K.Kondo		[2]画面スクロール座標の追加
 */
public class I304Form extends ActionForm{

	// 選択中の生徒//
	private String targetPersonId;
	// 大学コード//
	private String univCd;
	// 学部コード//
	private String facultyCd;
	// 学科コード//
	private String deptCd;	
	// 編集用のキーコード//
	private String univValue4Detail;
	//新規・複製・編集を判断する//
	private String button;
	//入試形態を変更したかどうか
	private String refresh;
	//入試形態コード//
	private String entExamModeCd;
	//月//
	private String[] month;
	//日//
	private String[] date;
	//試験地//
	private String[] provEntExamSite;
	//入試区分123//
	private String[] entExamDiv1;
	private String[] entExamDiv2;
	private String[] entExamDiv3;
	//ガイドライン//
	private String remarks;
	//以前のアクション//
	private String preButton;
	
	/**
	 * [1] add
	 * 年度
	 */
	private String year;
	
	private String scrollX;//[2] add サブミット前、表示座標Ｘ
	private String scrollY;//[2] add サブミット前、表示座標Ｙ
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.individual.ICommonForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ
		
	}

	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

	/**
	 * @return
	 */
	public String getUnivValue4Detail() {
		return univValue4Detail;
	}

	/**
	 * @param string
	 */
	public void setUnivValue4Detail(String string) {
		univValue4Detail = string;
	}

	/**
	 * @return
	 */
	public String getButton() {
		return button;
	}

	/**
	 * @param string
	 */
	public void setButton(String string) {
		button = string;
	}

	/**
	 * @return
	 */
	public String getRefresh() {
		return refresh;
	}

	/**
	 * @param string
	 */
	public void setRefresh(String string) {
		refresh = string;
	}

	/**
	 * @return
	 */
	public String getEntExamModeCd() {
		return entExamModeCd;
	}

	/**
	 * @param string
	 */
	public void setEntExamModeCd(String string) {
		entExamModeCd = string;
	}

	/**
	 * @return
	 */
	public String getPreButton() {
		return preButton;
	}

	/**
	 * @param string
	 */
	public void setPreButton(String string) {
		preButton = string;
	}


	/**
	 * @return
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param string
	 */
	public void setRemarks(String string) {
		remarks = string;
	}

	/**
	 * @return
	 */
	public String[] getDate() {
		return date;
	}

	/**
	 * @return
	 */
	public String[] getMonth() {
		return month;
	}

	/**
	 * @return
	 */
	public String[] getProvEntExamSite() {
		return provEntExamSite;
	}

	/**
	 * @param strings
	 */
	public void setDate(String[] strings) {
		date = strings;
	}

	/**
	 * @param strings
	 */
	public void setMonth(String[] strings) {
		month = strings;
	}

	/**
	 * @param strings
	 */
	public void setProvEntExamSite(String[] strings) {
		provEntExamSite = strings;
	}
	/**
	 * @return
	 */
	public String[] getEntExamDiv1() {
		return entExamDiv1;
	}

	/**
	 * @return
	 */
	public String[] getEntExamDiv2() {
		return entExamDiv2;
	}

	/**
	 * @return
	 */
	public String[] getEntExamDiv3() {
		return entExamDiv3;
	}

	/**
	 * @param strings
	 */
	public void setEntExamDiv1(String[] strings) {
		entExamDiv1 = strings;
	}

	/**
	 * @param strings
	 */
	public void setEntExamDiv2(String[] strings) {
		entExamDiv2 = strings;
	}

	/**
	 * @param strings
	 */
	public void setEntExamDiv3(String[] strings) {
		entExamDiv3 = strings;
	}

	/**
	 * @return
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param string
	 */
	public void setYear(String string) {
		year = string;
	}

	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[2] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[2] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[2] add
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[2] add
	}
}

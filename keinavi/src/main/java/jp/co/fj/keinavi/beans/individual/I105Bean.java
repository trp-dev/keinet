/*
 * 作成日: 2004/07/23
 *
 * バランスチャート(合格者平均/過回模試グラフ)の偏差値データ取得用Bean
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import jp.co.fj.keinavi.data.ExamData;//[3]
import jp.co.fj.keinavi.data.individual.CourseData;
import jp.co.fj.keinavi.data.individual.IExamData;
import jp.co.fj.keinavi.data.individual.SubRecordIData;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNUtil;

import com.fjh.beans.DefaultBean;

/**
 * バランスチャート(合格者平均)<BR>
 * DBより偏差値を取得する。
 * @author kondo
 * 
 * history
 *   Symbol Date       Person     Note
 *   [1]    2005.01.21 kondo      change ２次 英語＋リスニング追加対応
 *   [2]    2005.01.21 kondo      change 配点200点の科目が複数ある場合最初の1つをとるように修正
 *   [3]    2005.03.02 kondo      change 科目変換に、模試コードを追加。
 *   [4]    2005.05.29 kondo      change 過去合格者平均の英語は科目コード1000に固定
 *   [5]    2005.09.16 kondo      change 科目取得の際EXAMSUBJECTではなく、Viewを参照するように修正
 * 
 * 2009.11.24   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応 
 * 2010.01.18   Shoji HASE Totec
 *              「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 * 　　　     　「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 */
public class I105Bean extends DefaultBean {
	
	private static final long serialVersionUID = -4993995575041338930L;
	
	public static String DEFAULT_RANK1 = "1";
	public static String DEFAULT_RANK2 = "2";
	public static String DEFAULT_RANK3 = "3";
	public static String[] DEFAULT_RANKS = {DEFAULT_RANK1,DEFAULT_RANK2,DEFAULT_RANK3};
		
	//模試ＳＱＬ
	private String SQL_BASE = 
		"SELECT"+
		" /*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */ "+//12/5 sql hint
		" SUBRECORD_I.INDIVIDUALID"+
		" ,SUBRECORD_I.A_DEVIATION"+
		" ,SUBRECORD_I.ACADEMICLEVEL"+
		" ,SUBRECORD_I.SUBCD"+
		" ,EXAMINATION.EXAMNAME"+
		" ,EXAMINATION.EXAMTYPECD"+
		" ,SUBRECORD_I.EXAMCD"+
		" ,SUBRECORD_I.EXAMYEAR"+
		//" ,EXAMSUBJECT.DISPSEQUENCE"+//[5] delete
		//" ,EXAMSUBJECT.SUBALLOTPNT"+//[5] delete
		" ,V_I_CMEXAMSUBJECT.DISPSEQUENCE"+//[5] add
		" ,V_I_CMEXAMSUBJECT.SUBALLOTPNT"+//[5] add
		" ,SUBRECORD_I.SCOPE"+
		" FROM SUBRECORD_I"+
		" INNER JOIN EXAMINATION ON EXAMINATION.EXAMYEAR = SUBRECORD_I.EXAMYEAR"+
		" AND EXAMINATION.EXAMCD = SUBRECORD_I.EXAMCD"+
		//" INNER JOIN EXAMSUBJECT ON EXAMSUBJECT.EXAMYEAR = SUBRECORD_I.EXAMYEAR"+//[5] delete
		//" AND SUBRECORD_I.EXAMCD = EXAMSUBJECT.EXAMCD"+//[5] delete
		//" AND SUBRECORD_I.SUBCD = EXAMSUBJECT.SUBCD";//[5] delete
		" INNER JOIN V_I_CMEXAMSUBJECT ON V_I_CMEXAMSUBJECT.EXAMYEAR = SUBRECORD_I.EXAMYEAR"+//[5] add
		" AND SUBRECORD_I.EXAMCD = V_I_CMEXAMSUBJECT.EXAMCD"+//[5] add
		" AND SUBRECORD_I.SUBCD = V_I_CMEXAMSUBJECT.SUBCD";//[5] add
	
	//大学過去平均ＳＱＬ
	private String UNIV_SQL = 
	"(SELECT " + 
	" /*+ INDEX(CR PK_CANDIDATERATING) */ "+//12/5 sql hint
	"CR.CANDIDATERANK" + 		//志望順位
	",HS.UNINAME_ABBR UNIVNAME_ABBR" + 		//大学名短縮
	",HS.FACULTYNAME_ABBR" +	 //学部名短縮
	",HS.DEPTNAME_ABBR" + 		//学科名短縮
	",ST.CURSUBCD" + 				//科目コード
	",PS.SUCCESSAVGDEV" + 		//合格者平均偏差値
	" FROM EXAMINATION EX";
	
	private String COMBO_SQL_BASE = 
	"SELECT "+
	" /*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */ "+//12/5 sql hint
	" SUBRECORD_I.INDIVIDUALID"+			//パーソンＩＤ
	" ,SUBRECORD_I.EXAMYEAR"+		//模試年月
	" ,SUBRECORD_I.EXAMCD"+			//模試コード
	" ,EXAMINATION.EXAMNAME"+		//模試名
	" ,EXAMINATION.EXAMTYPECD"+		//模試名
	" ,EXAMINATION.DISPSEQUENCE"+	//表示順
	" FROM SUBRECORD_I"+	
	" INNER JOIN EXAMINATION ON SUBRECORD_I.EXAMYEAR = EXAMINATION.EXAMYEAR"+
	" AND SUBRECORD_I.EXAMCD = EXAMINATION.EXAMCD";
	
	private String moshiSql;
	private String univSql;
	private String comboSql;
	private String countSql;

	private String targetExamYear;
	private String targetExamCode;
	
	private String firstExamCode;
	
	private String[] targetCandidateRanks;//再表示の時に使う＝2回目以降のアクセス
	private String targetExamTypeCode;
	
	private String[] deviationRanges;
	
	private String targetEngCd;	//対象の英語科目
	private String targetMathCd;	//対象の数学科目
	private String targetJapCd;	//対象の国語科目
	private String targetSciCd;	//対象の理科科目
	private String targetSocCd;	//対象の社会科目
	private String ans1stSciCd;	//第1解答科目の理科科目
	private String ans1stSocCd;	//第1解答科目の社会科目

	private boolean helpflg;		//ログイン情報
	private String personId;
	
	private String avgChoice;		// 教科複数受験フラグ（1:平均、2：良い方）
	
	private boolean isFirstAccess;
	private boolean examflg = false;
	private boolean dispflg = false;
	private boolean engDispFlg = false;//対象模試の英語を受けていたらtrue//[4] add
	private Collection courseDatas;//教科データ
	private Collection comboxDatas;//コンボ内容（UniversityData）
	private Collection examinationDatas;	//前画面にて選択された、対象模試データ
	
	private String examTypecd; //模試種類
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		try{
			conn.setAutoCommit(false);
			ExamData[] examDatas = new ExamData[1];//[3] add
			examDatas[0] = new ExamData();//[3] add
			examDatas[0].setExamYear(getTargetExamYear());//[3] add
			examDatas[0].setExamCD(getTargetExamCode());//[3] add
			//KNUtil.buildSubCDTransTable(conn, getTargetExamYear(), getTargetExamCode());//[3] delete
			KNUtil.buildSubCDTransTable(conn, examDatas);//[3] add
			//模試情報
			execDeviations();
			//過去模試情報
			setExaminationDatas(execUniversityDatas());
			//コンボ内容
			setComboxDatas(execComboBoxDatas());
			//教科名取得
			setCourseDatas(execCourseDatas(getTargetExamYear(), getTargetExamCode()));
			
			conn.commit();
		}catch(Exception e){
			conn.rollback();
			e.printStackTrace();
			throw new SQLException("i105BeanSQLに失敗しました。");
		} finally {
	
		}

	}
	
	/**
	 * 検索条件を作ります
	 * @param String string1
	 * @param String string2
	 * @param String string3
	 * @param String[] string2
	 */
	public void setSearchCondition(	String examYear, 
										String examCode, 
										String firstCode,
										String personId,
										String[] stringss2	){
		
		setTargetExamYear(examYear);			//模試年度
		setTargetExamCode(examCode);			//模試コード
		setFirstExamCode(firstCode);			//前画面にて選択された、模試コード
		setPersonId(personId);					//個人ID
		setTargetCandidateRanks(stringss2);		//志望順位
		
		//:::: SQL文の作成 ::::::::::
		
		//模試ＳＱＬの作成
		appendMoshiSql(SQL_BASE);
		appendMoshiSql(getYearAndPersonCondition(getPersonId(), getTargetExamYear(), getTargetExamCode()));	//PersonId、模試年度
		appendMoshiSql(getExamCodeCondition(getTargetExamCode()));	//模試ＩＤ
		appendMoshiSql(getMoshiOrderBy());
		
		//過年度合格者成績ＳＱＬの作成
		appendUnivSql(UNIV_SQL);					//基本情報を設定
		appendUnivSql(getUnivCondition(getPersonId(), getTargetExamYear(), getTargetExamCode(), getTargetCandidateRanks()));
		//appendUnivSql(getCandidateRankCondition());	//志望順位を設定
		
		//コンボＳＱＬ作成
		appendComboSql(COMBO_SQL_BASE);		//基本情報を設定
		appendComboSql(getComboSqlCondition(getPersonId(), Integer.parseInt(getTargetExamYear()), getFirstExamCode()));
		
		appendCountSql(SQL_BASE);
		appendCountSql(get200count(getPersonId(), getTargetExamYear(), getTargetExamCode()));
		appendCountSql(getMoshiOrderBy());
		
		//appendComboSql(getYearAndPersonCondition(getPersonId(), getTargetExamYear()));	//PersonId、模試年度
		/** 2014修正 */
		//getSequenceCondition(getTargetExamYear(), getTargetExamCode(), getPersonId());	//対象模試より過去模試
		//appendComboSql(getMoshiOrderBy());	//ソート条件
	}
	
	private void appendComboSql(String string){
		if(getComboSql() == null){
			setComboSql("");
		}
		comboSql += string;
	}
	
	private void appendMoshiSql(String string){
		if(getMoshiSql() == null){
			setMoshiSql("");
		}
		moshiSql += string;
	}
	/**
	 * メインデータを取得するSQL文にStringを付加する
	 * @param string
	 */
	private void appendCountSql(String string){
		if(getCountSql() == null)
			setCountSql("");
		countSql += string;
	}
	/**
	 * 志望大学ＳＱＬに条件を追加
	 * @param string
	 */
	private void appendUnivSql(String string) {
		if(getUnivSql() == null) {
			setUnivSql("");
		}
		univSql += string;
	}
	
	/**科目配点200の物をカウントする
	 */
	private String get200count(String string1, String string2, String string3){
		String sqlOption = " WHERE SUBRECORD_I.INDIVIDUALID = ?"+
							" AND SUBRECORD_I.EXAMYEAR = '"+string2+"'" + 
							" AND EXAMINATION.EXAMCD = '" + string3 + "'"+
							//" AND EXAMSUBJECT.SUBALLOTPNT = '200'"+//[1] delete
							//" AND (EXAMSUBJECT.SUBALLOTPNT = '200' OR EXAMSUBJECT.SUBCD = '1190') "+//[1] add//[5] delete
							//" AND ((SUBSTR(EXAMSUBJECT.SUBCD, 0, 1) = '1') OR (SUBSTR(EXAMSUBJECT.SUBCD, 0, 1) = '2') OR (SUBSTR(EXAMSUBJECT.SUBCD, 0, 1) = '3'))";//[5] delete
							" AND (V_I_CMEXAMSUBJECT.SUBALLOTPNT = '200' OR V_I_CMEXAMSUBJECT.SUBCD = '1190') "+//[5] add
							" AND ((SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '1') OR (SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '2') OR (SUBSTR(V_I_CMEXAMSUBJECT.SUBCD, 0, 1) = '3'))";//[5] add
		return sqlOption;
	}
	/**
	 * 模試データ取得用ORDER BY
	 * @return
	 */
	private String getMoshiOrderBy(){
		String sqlOption = ""+
		//" ORDER BY EXAMSUBJECT.DISPSEQUENCE";//[5] delete
		" ORDER BY V_I_CMEXAMSUBJECT.DISPSEQUENCE";//[5] add
		return sqlOption;
	}
	
	/**
	 * 志望大学順位を複数指定
	 * @param string
	 * @param strings
	 * @return
	 */
	private String getCandidateRankCondition(String[] ranks){
		if(ranks == null){
			return "";
		}else{
			String sqlOption = " AND CR.CANDIDATERANK IN (";
			for(int i = 0; i < ranks.length; i ++){
				if(i != 0)
					sqlOption += ",";
				sqlOption += ranks[i];
			}
			sqlOption += ") ";
			if(ranks.length == 0)
				return "";
			else
				return sqlOption;
		}
	}
	
	/**
	 * 模試コンボ内容取得ＳＱＬの作成
	 * 
	 * @param id	個人ＩＤ
	 * @param year	対象模試年度
	 * @param cd	対象模試コード
	 * @return	模試コンボＳＱＬ
	 */
	private String getComboSqlCondition(String id, int year, String cd){
		
		String sqlOption ="";
		if(helpflg  == true){
			//ヘルプデスク
			sqlOption = 	" WHERE " +
							"SUBRECORD_I.INDIVIDUALID = ? " +
							"AND SUBRECORD_I.EXAMYEAR = '" + year + "' " +
							"AND EXAMINATION.IN_DATAOPENDATE < (" +
							"SELECT IN_DATAOPENDATE " +
							"FROM EXAMINATION WHERE EXAMYEAR = '" + year + "' " +
							"AND EXAMINATION.EXAMCD = '" + cd + "') " +
							
			" AND EXAMINATION.EXAMTYPECD IN ('01','02','03') " +
			" AND EXAMINATION.EXAMCD NOT IN('66','67')"+
			
							"GROUP BY SUBRECORD_I.INDIVIDUALID, SUBRECORD_I.EXAMYEAR, " +
							"SUBRECORD_I.EXAMCD, EXAMINATION.EXAMNAME ,EXAMINATION.DISPSEQUENCE ,EXAMINATION.IN_DATAOPENDATE ,EXAMINATION.EXAMTYPECD" +
							" ORDER BY EXAMINATION.IN_DATAOPENDATE DESC";
		}else{
			//
			sqlOption = 	" WHERE " +
							"SUBRECORD_I.INDIVIDUALID = ? " +
							"AND SUBRECORD_I.EXAMYEAR = '" + year + "' " +
							"AND EXAMINATION.OUT_DATAOPENDATE < (" +
								"SELECT OUT_DATAOPENDATE " +
								"FROM EXAMINATION WHERE EXAMYEAR = '" + year + "' " +
							"AND EXAMINATION.EXAMCD = '" + cd + "') " +
							
			" AND EXAMINATION.EXAMTYPECD IN ('01','02','03') " +
			" AND EXAMINATION.EXAMCD NOT IN('66','67')"+
			
							"GROUP BY SUBRECORD_I.INDIVIDUALID, SUBRECORD_I.EXAMYEAR, " +
								"SUBRECORD_I.EXAMCD, EXAMINATION.EXAMNAME ,EXAMINATION.DISPSEQUENCE ,EXAMINATION.OUT_DATAOPENDATE ,EXAMINATION.EXAMTYPECD" +
							" ORDER BY EXAMINATION.OUT_DATAOPENDATE DESC";
		}
		return sqlOption;
		
	}
	
	/**
	 * 模試年度とINDIVIDUALIDの指定
	 * @param string1
	 * @param string2
	 * @return
	 */
	private String getYearAndPersonCondition(String string1, String string2, String string3){
		String sqlOption = " WHERE SUBRECORD_I.INDIVIDUALID = ?"+
							" AND SUBRECORD_I.EXAMYEAR = '"+string2+"'" +
							" AND EXAMINATION.EXAMCD = '" + string3 + "'"; 
		return sqlOption;
	}
	
	/**
	 * 模試コードの指定
	 * @param string
	 * @return
	 */
	private String getExamCodeCondition(String string) {
		String sqlOption = " AND SUBRECORD_I.EXAMCD = '" + string + "'";
		return sqlOption;
	}
	
	/**
	 * 志望大学過去合格者平均をＤＢより取得
	 * 
	 * @param id		個人ＩＤ
	 * @param year		対象模試年度
	 * @param cd		対象模試コード
	 * @return String SQL
	 */
	private String getUnivCondition(String id, String year, String cd, String[] ranks) {
		
		String lastYear = "";
		if(!year.equals("")){
			lastYear = Integer.toString(Integer.parseInt(year) -1);
		}
		
		String sqlOption = 	" INNER JOIN CANDIDATERATING CR " +
							"ON CR.INDIVIDUALID = ? " +
							"AND EX.EXAMYEAR = '" + year + "' " +
							"AND EX.EXAMCD = '" + cd + "' " +
							"AND CR.EXAMYEAR = EX.EXAMYEAR " +
							"AND CR.EXAMCD = EX.EXAMCD " +
							getCandidateRankCondition(ranks) +
							"LEFT JOIN UNIVMASTER_BASIC HS " +
							"ON HS.EVENTYEAR = EX.EXAMYEAR " +
							"AND HS.EXAMDIV = EX.EXAMDIV " +
							"AND HS.UNIVCD = CR.UNIVCD " +
							"AND HS.FACULTYCD = CR.FACULTYCD " +
							"AND HS.DEPTCD = CR.DEPTCD " +
							"INNER JOIN " +
							"SUBCDTRANS ST " +
							//"ON ST.YEAR = '" + lastYear + "' " +//[3] delete
							"ON ST.EXAMYEAR = '" + lastYear + "' " +//[3] add
							"AND ST.EXAMCD = '" + cd + "' " +//[3] add
							"LEFT JOIN "+ 
							"SUBRECORD_I SI " + 
							"ON SI.INDIVIDUALID = CR.INDIVIDUALID " + 
							"AND SI.EXAMYEAR = EX.EXAMYEAR "+ 
							"AND SI.EXAMCD = EX.EXAMCD " +
							"AND SI.SUBCD = ST.SUBCD " + 
							"LEFT JOIN PREVSUCCESS PS  " +
							//"ON PS.EXAMYEAR = ST.YEAR " +//[3] delete
							"ON PS.EXAMYEAR = ST.EXAMYEAR " +//[3] add
							"AND PS.SUBCD = ST.SUBCD " +
							"AND PS.UNIVCD = CR.UNIVCD " +
							"AND PS.DEPTCD = CR.DEPTCD " +
							"AND PS.FACULTYCD = CR.FACULTYCD " +
							"AND PS.EXAMTYPECD = EX.EXAMTYPECD) " +
							
							"UNION ALL "+
							
							"(SELECT "+
								" /*+ INDEX(CR PK_CANDIDATERATING) */ "+//12/5 sql hint
								"CR.CANDIDATERANK,"+
								"HS.UNINAME_ABBR,"+
								"HS.FACULTYNAME_ABBR,"+
								"HS.DEPTNAME_ABBR,"+
								"PS.SUBCD,"+
								"PS.SUCCESSAVGDEV "+
							"FROM "+
								"EXAMINATION EX "+
								"INNER JOIN CANDIDATERATING CR "+
								"ON CR.INDIVIDUALID = ? " +
								"AND EX.EXAMYEAR = '" + year + "' " +
								"AND EX.EXAMCD = '" + cd + "' " +
								"AND CR.EXAMYEAR = EX.EXAMYEAR "+
								"AND CR.EXAMCD = EX.EXAMCD "+
								getCandidateRankCondition(ranks) +
								"LEFT JOIN UNIVMASTER_BASIC HS "+
								"ON HS.EVENTYEAR = EX.EXAMYEAR "+
								"AND HS.EXAMDIV = EX.EXAMDIV "+
								"AND HS.UNIVCD = CR.UNIVCD "+
								"AND HS.FACULTYCD = CR.FACULTYCD "+
								"AND HS.DEPTCD = CR.DEPTCD "+
								"LEFT JOIN PREVSUCCESS PS "+
								"ON PS.EXAMYEAR = '" + lastYear + "' " +
								"AND PS.UNIVCD = CR.UNIVCD "+
								"AND PS.DEPTCD = CR.DEPTCD "+
								"AND PS.FACULTYCD = CR.FACULTYCD "+
								"AND PS.EXAMTYPECD = EX.EXAMTYPECD " +
								//"WHERE PS.SUBCD = '2000' OR PS.SUBCD = '4000' OR PS.SUBCD = '5000' OR PS.SUBCD = '6000') "+//[4] delete
								"WHERE PS.SUBCD IN ('1000','2000','4000','5000')) "+//[4] add
							"ORDER BY CANDIDATERANK";
							
							return sqlOption;
	}
	
	/**
	 * 対象年の対象模試の教科コード、教科名をすべて取得
	 * @param String year
	 * @param String code
	 * @return Collection
	 */
	private Collection execCourseDatas(String year, String code) throws Exception {
		String nmGet = 
			"SELECT COURSE.COURSECD, COURSE.COURSENAME" +
			" FROM COURSE, EXAMSUBJECT" +
			" WHERE" +
			" COURSE.COURSECD = RPAD(SUBSTR(EXAMSUBJECT.SUBCD, 1, 1), 4, 0)" +
			" AND EXAMSUBJECT.EXAMCD ='" + code + "'" +
			" AND EXAMSUBJECT.EXAMYEAR = '" + year + "'" +
			" AND COURSE.YEAR = EXAMSUBJECT.EXAMYEAR" +
			" GROUP BY COURSE.COURSECD, COURSE.COURSENAME" +
			" ORDER BY COURSE.COURSECD";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{	
			pstmt = conn.prepareStatement(nmGet);
			rs = pstmt.executeQuery();
			
			//教科をリストにつめる。
			Collection courseDatas = new ArrayList();
			while(rs.next()) {
				CourseData courseData = new CourseData();
				courseData.setCourseName(rs.getString("COURSENAME"));
				courseData.setCourseCd(rs.getString("COURSECD"));
				
				if(Integer.parseInt(rs.getString("COURSECD").substring(0, 1)) < 7){
					//総合ではない
					courseDatas.add(courseData);
				} else {
					//総合だったら追加しない
				}
			}
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
			
			return courseDatas;
			
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
		
	}
	
	//コンボボックスの内容（大学名）をＤＢから取得する。
	private Collection execComboBoxDatas() throws Exception{
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = conn.prepareStatement(getComboSql());
			pstmt.setString(1, personId);
			//ＳＱＬ実行
			rs = pstmt.executeQuery();
			
			Collection boxDatas = new ArrayList();
			
			//取得データをAttayListにつめる
			while(rs.next()){
				//大学情報
				IExamData examinationData = new IExamData();
				//志望順位の設定
				examinationData.setExamYear(rs.getString("EXAMYEAR"));
				examinationData.setExamCd(rs.getString("EXAMCD"));
				//志望大学名の設定
				examinationData.setExamName(rs.getString("EXAMNAME"));
				
				//名称の設定
				boxDatas.add(examinationData);
			}
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
			
			return boxDatas;
			
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}
	
	//志望大学過去偏差値を取得
	private Collection execUniversityDatas() throws Exception {
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String FULLSIZESPACE = "　";
		try{
			pstmt = conn.prepareStatement(getUnivSql());
			pstmt.setString(1, personId);
			pstmt.setString(2, personId);
			rs = pstmt.executeQuery();
			Map mappy = new LinkedHashMap();
			
			//模試データ追加
			if(getExaminationDatas().size() == 0) {
				//模試データが取得できていないならば、志望大学過去偏差値は取得できないため、そのまま返す。
				return getExaminationDatas();
			}
			mappy.put(getTargetExamCode(), getExaminationDatas().iterator().next());
			
			while(rs.next()){
				IExamData examinationData = (IExamData) mappy.get(rs.getString("CANDIDATERANK"));
				if(examinationData == null){
					//マップの中にこの模試が存在しなければ新たに作成
					//同時に科目のコレクションの初期化
					examinationData = new IExamData();
					if(getExaminationDatas() == null)
						setExaminationDatas(new ArrayList());
					getExaminationDatas().add(examinationData);
					mappy.put(rs.getString("CANDIDATERANK"), examinationData);	
				}
				examinationData.setExamCd(nullToEmpty(rs.getString("CANDIDATERANK")));
				examinationData.setExamName(nullToEmpty(rs.getString("UNIVNAME_ABBR")) + FULLSIZESPACE + nullToEmpty(rs.getString("FACULTYNAME_ABBR")) + FULLSIZESPACE + nullToEmpty(rs.getString("DEPTNAME_ABBR")));
				
				Collection subRecordDatas = examinationData.getSubRecordDatas();
				if(subRecordDatas == null){
					subRecordDatas = new ArrayList();
					examinationData.setSubRecordDatas(subRecordDatas);
				}
						
				SubRecordIData subRecordData = new SubRecordIData();
				subRecordData.setCDeviation(nullToEmptyF(rs.getString("SUCCESSAVGDEV")));
				subRecordData.setSubCd(rs.getString("CURSUBCD"));
				
				
				subRecordDatas.add(subRecordData);
			}
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		
			return orderStraight(mappy.values());
			
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}
	
	/**
	 * 対象模試をＤＢから取得して、模試リストに追加します。
	 * 
	 * @throws Exception
	 */
	private void execDeviations() throws Exception{
		final String ELSUBCD = "1190"; //[1] add 英語＋リスニング
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PreparedStatement pstmt2 = null;
		ResultSet rs2 = null;
		try{
			

			//配点が200点の数学を探す
			pstmt = conn.prepareStatement(getCountSql());		
			pstmt.setString(1, personId);
			rs = pstmt.executeQuery();
			ArrayList examNameM200 = new ArrayList();
			ArrayList examNameJ200 = new ArrayList();
			ArrayList examNameE200 = new ArrayList();
			ArrayList examNameEL = new ArrayList(); //[1] add 英語＋リスニング
			String subIndex = "";
			while(rs.next()){
				//配点が200以上の数学・国語を持つ模試リスト作成
				if(rs.getString("SUBCD") != null){
					subIndex = rs.getString("SUBCD").substring(0, 1);
					
					if(subIndex.equals("2")){
						examNameM200.add(rs.getString("EXAMNAME")); //数学
					}else{
						if(subIndex.equals("3")){
							examNameJ200.add(rs.getString("EXAMNAME")); //国語
						}else{
							examNameE200.add(rs.getString("EXAMNAME")); //英語
							//[1] add start
							if(rs.getString("SUBCD").equals(ELSUBCD)){
								examNameEL.add(rs.getString("EXAMNAME")); //英語＋リスニング
							}
							//[1] add end
						}
					}
				}
			}
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();	
		
		//検索実行			
		pstmt2 = conn.prepareStatement(getMoshiSql());
		pstmt2.setString(1, personId);
		rs2 = pstmt2.executeQuery();

		//数学・国語の表示順序1番目を取るカウント
		int mathCount = 0;
		int japanCount = 0;
		int engCount = 0;
		
		IExamData examinationData = null;	//対象模試
		Collection subRecordDatas = null;			//科目リスト
		while(rs2.next()) {
			//対象模試がnullなら作る。
			if(examinationData == null) {examinationData  = new IExamData();}
			//科目リストがnullなら作る。
			if(subRecordDatas == null) {subRecordDatas = new ArrayList();}
			
			examinationData.setExamYear(nullToEmpty(rs2.getString("EXAMYEAR")));		//模試年度
			examinationData.setExamCd(nullToEmpty(rs2.getString("EXAMCD")));			//模試コード
			examinationData.setExamName(nullToEmpty(rs2.getString("EXAMNAME")));		//模試名称
			examinationData.setExamTypeCd(nullToEmpty(rs2.getString("EXAMTYPECD")));	//模試タイプ
			
			//対象模試がマーク模試かどうか調べる。
			
			if(rs2.getString("EXAMTYPECD") != null && rs2.getString("EXAMTYPECD").equals("01")){
				examflg = true;
			}
			
			SubRecordIData subRecordData = new SubRecordIData();

			subIndex = "";
			if(rs2.getString("SUBCD") != null){
				subIndex = rs2.getString("SUBCD").substring(0, 1);
			}
			if(examNameE200.contains(rs2.getString("EXAMNAME")) && subIndex.equals("1") && engCount == 0){
				//[1] add start
				//模試が英語＋Ｌリストに存在する場合、英語＋Ｌに限定
				if(examNameEL.contains(rs2.getString("EXAMNAME"))) {
					if(nullToEmpty(rs2.getString("SUBCD")).equals(ELSUBCD)) {
						//英語＋Ｌ
						if(rs2.getString("A_DEVIATION") != null){
							subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));
							choiceScholarLevel(subRecordData, avgChoice, 
									rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
							subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));
							subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
							subRecordDatas.add(subRecordData);
						}
						engCount++;
					}
				} else {
					if(nullToEmpty(rs2.getString("SUBALLOTPNT")).equals("200")) {
						//英語得点200得点
						if(rs2.getString("A_DEVIATION") != null){
							subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));
							choiceScholarLevel(subRecordData, avgChoice, 
									rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
							subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));
							subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
							subRecordDatas.add(subRecordData);
						}
						engCount++;
					}
				}
			}else if(!examNameE200.contains(rs2.getString("EXAMNAME")) && subIndex.equals("1") && engCount == 0){
				subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));	//科目偏差値
				choiceScholarLevel(subRecordData, avgChoice, 
						rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
				subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));				//科目コード
				subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
				subRecordDatas.add(subRecordData);
				engCount ++;
			}else if(examNameM200.contains(rs2.getString("EXAMNAME")) && nullToEmpty(rs2.getString("SUBALLOTPNT")).equals("200") && subIndex.equals("2") && mathCount == 0){//[2] change
				subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));	//科目偏差値
				choiceScholarLevel(subRecordData, avgChoice, 
						rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
				subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));				//科目コード
				subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
				subRecordDatas.add(subRecordData);
				mathCount ++;//[2]
			}else if(!examNameM200.contains(rs2.getString("EXAMNAME")) && subIndex.equals("2") && mathCount == 0){
				subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));	//科目偏差値
				choiceScholarLevel(subRecordData, avgChoice, 
						rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
				subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));				//科目コード
				subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
				subRecordDatas.add(subRecordData);
				mathCount ++;
			}else if(examNameJ200.contains(rs2.getString("EXAMNAME")) && nullToEmpty(rs2.getString("SUBALLOTPNT")).equals("200") && subIndex.equals("3") && japanCount == 0){//[2] change
				subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));
				choiceScholarLevel(subRecordData, avgChoice, 
						rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
				subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));
				subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
				subRecordDatas.add(subRecordData);
				japanCount ++;//[2] change
			}else if(!examNameJ200.contains(rs2.getString("EXAMNAME")) && subIndex.equals("3") && japanCount == 0){
				subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));
				choiceScholarLevel(subRecordData, avgChoice, 
						rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
				subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));
				subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
				subRecordDatas.add(subRecordData);
				japanCount ++;	
			}else if(subIndex.equals("4") || subIndex.equals("5") || subIndex.equals("6")){
				subRecordData.setCDeviation(nullToEmptyF(rs2.getString("A_DEVIATION")));	//科目偏差値
				choiceScholarLevel(subRecordData, avgChoice, 
						rs2.getString("A_DEVIATION"), rs2.getString("ACADEMICLEVEL"));
				subRecordData.setSubCd(nullToEmpty(rs2.getString("SUBCD")));				//科目コード
				subRecordData.setScope(nullToEmpty(rs2.getString("SCOPE")));
				subRecordDatas.add(subRecordData);
			}
		}
		if(pstmt2 != null) pstmt2.close();
		if(rs2 != null) rs2.close();
			
			//対象模試があるなら、科目リストを設定
			if(examinationData != null && subRecordDatas != null) {
				//科目を設定
				examinationData.setSubRecordDatas(subRecordDatas);
				//メンバに良いほうの対象科目コードを設定

				//setTargetEngCd(examinationData.getMaxEngCode());//[4] delete
				//[4] add start
				final String ENG_UNIVDATA_SUBCD = "1000";
				if(!nullToEmpty(examinationData.getMaxEngCode()).equals("")) {
					setTargetEngCd(ENG_UNIVDATA_SUBCD);
					engDispFlg = true;
				}
				//[4] add end
				if(examflg && (nullToEmpty(examinationData.getMaxMathCode()).equals("2380") || nullToEmpty(examinationData.getMaxMathCode()).equals("2480") || nullToEmpty(examinationData.getMaxMathCode()).equals("2580"))){
					setTargetMathCd("2000");
					dispflg = true;
				}else{
					setTargetMathCd(examinationData.getMaxMathCode());
				}
				setTargetJapCd(examinationData.getMaxJapCode());
				setTargetSciCd(examinationData.getMaxSciCode());
				setTargetSocCd(examinationData.getMaxSocCode());
				setAns1stSciCd(examinationData.getAns1stSubjectCodeSci());
				setAns1stSocCd(examinationData.getAns1stSubjectCodeSoc());
			}
			
			//模試リストがnullだったらリストを作る。
			if(getExaminationDatas() == null) {
				setExaminationDatas(new ArrayList());
			}
			//リストに模試情報を追加
			if(examinationData != null) {
				getExaminationDatas().add(examinationData);
			}
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
			if(pstmt2 != null) pstmt2.close();
			if(rs2 != null) rs2.close();
		}
	}
	
	private Collection orderStraight(Collection collection){
		int DEFAULT_MAX_SIZE = 4;
		int thisSize = 0;
		
		if(collection.size() < DEFAULT_MAX_SIZE) {
			thisSize = collection.size();
		} else{
			thisSize = DEFAULT_MAX_SIZE;
		}
		
		IExamData[] examinationDatas = new IExamData[thisSize];
		for (Iterator it=collection.iterator(); it.hasNext();) {
			IExamData examinationData = (IExamData)it.next();
			if(collection.size() > 0 && examinationData.getExamCd().equals(getTargetExamCode())){
				examinationDatas[0] = examinationData;
				setTargetSubCd(examinationData);//教科の最大偏差値の科目を設定
//			}else if(collection.size() > 1 && examinationData.getExamCd().equals(getTargetCandidateRanks()[0])){
			}else if(collection.size() > 1 && examinationDatas[1] == null){
				examinationDatas[1] = examinationData;
				setTargetSubCd(examinationData);//教科の最大偏差値の科目を設定
//			}else if(collection.size() > 2 && examinationData.getExamCd().equals(getTargetCandidateRanks()[1])){
			}else if(collection.size() > 2 && examinationDatas[2] == null){
				examinationDatas[2] = examinationData;
				setTargetSubCd(examinationData);//教科の最大偏差値の科目を設定
//			}else if(collection.size() > 3 && examinationData.getExamCd().equals(getTargetCandidateRanks()[2])){
			}else if(collection.size() > 3 && examinationDatas[3] == null){
				examinationDatas[3] = examinationData;
				setTargetSubCd(examinationData);//教科の最大偏差値の科目を設定
			}else{
				//無視
			}
		}
		return Arrays.asList(examinationDatas);
	}
	
	/**
	 * 教科の最大偏差値の科目を設定
	 * @param exam 模試
	 */
	private void setTargetSubCd(IExamData exam) {
		if(exam != null) {
			//教科の最大偏差値の科目をそれぞれ設定
			//exam.setTargetEngSubCd(getTargetEngCd());//[4] delete
			//[4] add start
			final String ENG_UNIVDATA_SUBCD = "1000";
			if(engDispFlg && !nullToEmpty(getTargetEngCd()).equals("")) {
				exam.setTargetEngSubCd(ENG_UNIVDATA_SUBCD);
			}
			//[4] add end
			
			if(examflg && (nullToEmpty(getTargetMathCd()).equals("2380") || nullToEmpty(getTargetMathCd()).equals("2480") || nullToEmpty(getTargetMathCd()).equals("2580"))){
				exam.setTargetMathSubCd("2000");
				dispflg = true;
			}else{
				exam.setTargetMathSubCd(getTargetMathCd());
			}
			
			exam.setTargetJapSubCd(getTargetJapCd());
			exam.setTargetSciSubCd(getTargetSciCd());
			exam.setTargetSocSubCd(getTargetSocCd());
			exam.setTargetAns1stSciSubCd(getAns1stSciCd());
			exam.setTargetAns1stSocSubCd(getAns1stSocCd());
			
			exam.setTargetExamCd(getTargetExamCode());
		}
	}
	
	/**
	 * StringをFloatのフォーマットで返します。
	 * @return
	 */
	public String nullToEmptyF(String str) {
		if(str == null) {
			return "";
		} else{
				return String.valueOf(new Float(str).floatValue());
		}
	}
	/**
	 * nullが来たら空白にする
	 */
	private String nullToEmpty(String str) {
		if(str == null) {
			return "";
		}
		if(str.equals("null")) {
			return "";
		}
		return str;
	}
	/**
	 * @return
	 */
	public String[] getDeviationRanges() {
		return deviationRanges;
	}

	/**
	 * @return
	 */
	public String getMoshiSql() {
		return moshiSql;
	}

	/**
	 * @return
	 */
	public String getPersonId() {
		return personId;
	}

	/**
	 * @return
	 */
	public String getSQL_BASE() {
		return SQL_BASE;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * 志望順位を返します
	 * @return
	 */
	public String[] getTargetCandidateRanks() {
		return targetCandidateRanks;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @param strings
	 */
	public void setDeviationRanges(String[] strings) {
		deviationRanges = strings;
	}

	/**
	 * @param string
	 */
	public void setMoshiSql(String string) {
		moshiSql = string;
	}

	/**
	 * @param string
	 */
	public void setPersonId(String string) {
		personId = string;
	}

	/**
	 * @param string
	 */
	public void setSQL_BASE(String string) {
		SQL_BASE = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param strings
	 */
	public void setTargetCandidateRanks(String[] strings) {
		targetCandidateRanks = strings;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * univSqlを返します。
	 * @return univSql
	 */
	public String getUnivSql() {
		return univSql;
	}

	/**
	 * univSqlを設定します。
	 * @param String
	 */
	public void setUnivSql(String univSql) {
		this.univSql = univSql;
	}

	/**
	 * @return
	 */
	public String getComboSql() {
		return comboSql;
	}

	/**
	 * @param string
	 */
	public void setComboSql(String string) {
		comboSql = string;
	}

	/**
	 * @return
	 */
	public boolean isFirstAccess() {
		return isFirstAccess;
	}

	/**
	 * @param b
	 */
	public void setFirstAccess(boolean b) {
		isFirstAccess = b;
	}

	/**
	 * @return
	 */
	public Collection getComboxDatas() {
		return comboxDatas;
	}

	/**
	 * @param collection
	 */
	public void setComboxDatas(Collection collection) {
		comboxDatas = collection;
	}

	/**
	 * 模試情報を取得します
	 * @return Collection
	 */
	public Collection getExaminationDatas() {
		return examinationDatas;
	}
	
	/**
	 * 模試情報を設定します
	 * @param Collection
	 */
	public void setExaminationDatas(Collection collection) {
		this.examinationDatas = collection;
	}

	/**
	 * 模試種類コードを返します
	 * @return String
	 */
	public String getTargetExamTypeCode() {
		return targetExamTypeCode;
	}
	
	/**
	 * 模試種類コードを設定します
	 * @param String string
	 */
	public void setTargetExamTypeCode(String string) {
		targetExamTypeCode = string;
	}
	/**
	 * @return
	 */
	public Collection getCourseDatas() {
		return courseDatas;
	}

	/**
	 * @param collection
	 */
	public void setCourseDatas(Collection collection) {
		courseDatas = collection;
	}

	/**
	 * @return
	 */
	public String getFirstExamCode() {
		return firstExamCode;
	}

	/**
	 * @param string
	 */
	public void setFirstExamCode(String string) {
		firstExamCode = string;
	}

	/**
	 * @return
	 */
	public String getTargetEngCd() {
		return targetEngCd;
	}

	/**
	 * @return
	 */
	public String getTargetJapCd() {
		return targetJapCd;
	}

	/**
	 * @return
	 */
	public String getTargetMathCd() {
		return targetMathCd;
	}

	/**
	 * @return
	 */
	public String getTargetSciCd() {
		return targetSciCd;
	}

	/**
	 * @return
	 */
	public String getTargetSocCd() {
		return targetSocCd;
	}

	/**
	 * @return
	 */
	public String getAns1stSciCd() {
		return ans1stSciCd;
	}

	/**
	 * @return
	 */
	public String getAns1stSocCd() {
		return ans1stSocCd;
	}

	/**
	 * @param string
	 */
	public void setTargetEngCd(String string) {
		targetEngCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetJapCd(String string) {
		targetJapCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetMathCd(String string) {
		targetMathCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetSciCd(String string) {
		targetSciCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetSocCd(String string) {
		targetSocCd = string;
	}

	/**
	 * @param string
	 */
	public void setAns1stSciCd(String string) {
		ans1stSciCd = string;
	}

	/**
	 * @param string
	 */
	public void setAns1stSocCd(String string) {
		ans1stSocCd = string;
	}

	/**
	 * @return
	 */
	public String getCountSql() {
		return countSql;
	}

	/**
	 * @param string
	 */
	public void setCountSql(String string) {
		countSql = string;
	}

	/**
	 * @return
	 */
	public boolean isHelpflg() {
		return helpflg;
	}

	/**
	 * @param b
	 */
	public void setHelpflg(boolean b) {
		helpflg = b;
	}

	/**
	 * @return
	 */
	public String getExamTypecd() {
		return examTypecd;
	}

	/**
	 * @param string
	 */
	public void setExamTypecd(String string) {
		examTypecd = string;
	}

	/**
	 * @return
	 */
	public boolean isExamflg() {
		return examflg;
	}

	/**
	 * @param b
	 */
	public void setExamflg(boolean b) {
		examflg = b;
	}

	/**
	 * @return
	 */
	public boolean isDispflg() {
		return dispflg;
	}

	/**
	 * @param b
	 */
	public void setDispflg(boolean b) {
		dispflg = b;
	}

	public void setAvgChoice(String avgChoice) {
		this.avgChoice = avgChoice;
	}
	
	/**
	 * 画面の選択状態（平均値／良い方）で、出力する学力レベルを選択する。
	 * @param hensa
	 * @param flg
	 * @param avg
	 * @param lvl
	 */
	private void choiceScholarLevel(SubRecordIData data,
			final String flg, final String avg, final String lvl) {
		
		if ("1".equals(flg)) {
			//平均値の場合、偏差値から学力レベルを出力
			data.setScholarlevel(IExamData.makeScholarLevel(avg));
		}
		else {
			//良い方の場合、SQLから取得した学力レベルを出力
			data.setScholarlevel(GeneralUtil.toBlank(lvl));
		}
		
	}

}

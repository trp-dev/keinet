package jp.co.fj.keinavi.util.log;

import java.io.*;
import java.util.*;
import java.util.logging.*;

/**
 * ログをファイルに出力するクラス
 * 
 * 2005.02.23 Yoshimoto KAWAI - Totec
 *            利用者ＩＤの出力に対応
 *
 * @Date		2004/06/09
 * @author	TOTEC)Nishiyama
 */
public class Log {

	/**
	 * エラー定義
	 */
	protected static Hashtable ErrHash = new Hashtable();
	protected static long LoadErrTime = 0;
	
	/**
	 * ログの出力クラスの深さ
	 */
	protected static int Depth = 3;
		
	/**
	 *
	 * intから16進8桁に変換
	 *
	 * @param Value 対象値
	 * @return 変換値
	 */
	protected static String IntToHex8(int Value) {
		try {
			if (Value == 0) {
				return null;
			}
			String Hex8Str0 = "00000000" + Integer.toHexString(Value).toUpperCase();
			int len = Hex8Str0.length();
			return Hex8Str0.substring(len - 8, len);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * システム名を取得
	 *
	 * @param ErrCD エラーコード
	 * @return システム名
	 */
	protected static String getSystemName(int ErrCD) {
		String SysNM = "";
		switch (ErrCD / 0x10000) {
		    case 1:
				SysNM = "LOG";
				break;
		    default:
				SysNM = "-";
				break;
		}
		return SysNM;
	}

	/**
	 * スタックトレースを取得
	 * 
	 * @param T 例外
	 * @return スタックトレース
	 */
	protected static String getStack(Throwable T) throws Exception {
		if (T == null) {
			return "";
		}

		StringWriter wr = new StringWriter(1024);
		T.printStackTrace(new PrintWriter(wr));
		return wr.toString();
	}

	/**
	 * コールスタックを取得
	 * 
	 * @param	depth	取得するStackTraceElementの階層(このメソッドからの距離(0:このメソッド)）
	 * @return コールスタック
	 */
	protected static StackTraceElement getCallStack(int depth) throws Exception {
		Throwable t = new Throwable();
		StackTraceElement[] el = t.getStackTrace();
		for(int i = 0; i < el.length - depth; i ++) {
			if (el[i].getClassName() != "jp.co.fj.keinavi.util.log.Log" ||
			    el[i].getMethodName() != "getCallStack") {
				continue;
			}
			
			return el[i+depth];
		}
		return null;
	}

	/**
	 * コールスタックをログレコードに設定する
	 * 
	 * @param	rec	ログレコード
	 * @param	depth	取得するStackTraceElementの階層(このメソッドからの距離(0:このメソッド)）
	 */
	protected static void setCallStack2Record(LogRecord rec, int depth) {
		try {
			StackTraceElement el = getCallStack(depth + 1);
			if (el == null) {
				return;
			}

			String className = el.getClassName();
			rec.setSourceClassName(className.substring(className.lastIndexOf('.') + 1));
			rec.setSourceMethodName(el.getMethodName());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 値を変換する
	 * 無効な値ならハイフンで置き換える
	 */
	protected static String convert(String value) {
		return value == null || "".equals(value) ? "-" : value;
	}
}
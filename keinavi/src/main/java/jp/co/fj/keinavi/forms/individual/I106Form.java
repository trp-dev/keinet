package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * 成績分析／成績推移グラフ用フォーム
 * @author kondo
 *
 * 2005.4.26 	K.Kondo 	[1]画面スクロール座標の追加
 */
public class I106Form extends ActionForm {

	private String mode;				//モード（分析、面談）

	private String targetPersonId;		//個人ＩＤ
	private String targetExamCode;		//模試コード
	private String targetExamYear;		//模試年度

	private String lowerRange;				//偏差値範囲（下限）
	private String upperRange;				//偏差値範囲（上限）
	private String avgChoice;				//偏差値計算方法（平均、良い方）

	private String targetGrade;			//学年の検索方法
	private String targetExamType;			//模試種類の検索方法
	
	//個人成績分析共通-印刷対象生徒（判定対象生徒)
	private String printStudent;
	
	//成績分析共通：印刷対象
	private String[] printTarget;
	
	//セキュリティスタンプ
	private String printStamp;
	
	//印刷ボタン押下時パラメータ
	private String printStatus;
	
	private String scrollX;//[1] add サブミット前、表示座標Ｘ
	private String scrollY;//[1] add サブミット前、表示座標Ｙ
	
	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}	
	
	/**
	 * @return
	 */
	public String getAvgChoice() {
		return avgChoice;
	}

	/**
	 * @return
	 */
	public String getLowerRange() {
		return lowerRange;
	}

	/**
	 * @return
	 */
	public String getUpperRange() {
		return upperRange;
	}

	/**
	 * @param string
	 */
	public void setAvgChoice(String string) {
		avgChoice = string;
	}

	/**
	 * @param string
	 */
	public void setLowerRange(String string) {
		lowerRange = string;
	}

	/**
	 * @param string
	 */
	public void setUpperRange(String string) {
		upperRange = string;
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * @return
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param string
	 */
	public void setMode(String string) {
		mode = string;
	}

	/**
	 * 学年の検索方法を返します
	 * @return String
	 */
	public String getTargetGrade() {
		return targetGrade;
	}

	/**
	 * 学年の検索方法を設定します
	 * @param String string
	 */
	public void setTargetGrade(String string) {
		targetGrade = string;
	}

	/**
	 * 模試種類の検索方法を返します
	 * @return String
	 */
	public String getTargetExamType() {
		return targetExamType;
	}

	/**
	 * 模試種類の検索方法を設定します
	 * @param String string
	 */
	public void setTargetExamType(String string) {
		targetExamType = string;
	}

	/**
	 * @return
	 */
	public String[] getPrintTarget() {
		return printTarget;
	}

	/**
	 * @param strings
	 */
	public void setPrintTarget(String[] strings) {
		printTarget = strings;
	}

	/**
	 * @return
	 */
	public String getPrintStamp() {
		return printStamp;
	}

	/**
	 * @param string
	 */
	public void setPrintStamp(String string) {
		printStamp = string;
	}

	/**
	 * @return
	 */
	public String getPrintStudent() {
		return printStudent;
	}

	/**
	 * @param string
	 */
	public void setPrintStudent(String string) {
		printStudent = string;
	}

	/**
	 * @return
	 */
	public String getPrintStatus() {
		return printStatus;
	}

	/**
	 * @param string
	 */
	public void setPrintStatus(String string) {
		printStatus = string;
	}
	
	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[1] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[1] add
	}
	
}

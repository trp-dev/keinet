/*
 * 作成日: 2004/10/07
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.date.ShortDatePlusUtil;

import com.fjh.beans.DefaultBean;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ProvisoStoreBean extends DefaultBean{

	private String individualId;//個人ID
	private String[] districtProviso;//地区条件(県コードprefectureをセット)
	private String searchStemmaDiv;//系統選択　中・文・理
	private String[] stemma2Code;//中系統コード
	private String[] stemma11Code;//小系統文系コード
	private String[] stemma10Code;//小系統理系コード
	private String[] stemmaCode;//分野コード
	private String descImposeDiv;//2次試験課し区分 1:課さない
	private String subImposeDiv;//試験科目課し区分 0:課さない、1:課す
	private String[] imposeSub;//課し試験科目
	private String[] schoolDivProviso;//学校区分条件
	//private String startDateProviso;//入試開始日条件
	private String startMonth;
	private String startDate;
	//private String endDateProviso;//入試終了日条件
	private String endMonth;
	private String endDate;
	private String raingDiv;//評価試験区分 1：センターまたは2次・私大、2：総合（ドッキング）
	private String[] raingProviso;//評価範囲条件
	private String[] outOfTergetProviso;//対象外条件
	private String dateSearchDiv;//入試日区分

	private String delQuery = ""+
	"DELETE FROM HANGUPPROVISO WHERE INDIVIDUALID = ?";
	
	private String insQuery = ""+
	"INSERT INTO HANGUPPROVISO (" +
	" INDIVIDUALID"+",DISTRICTPROVISO"+",SEARCHSTEMMADIV"+",STEMMAPROVISO"+
	",DESCIMPOSEDIV"+
	",SUBIMPOSEDIV"+",IMPOSESUB"+",SCHOOLDIVPROVISO"+",STARTDATEPROVISO"+",ENDDATEPROVISO"+
	",RAINGDIV"+",RAINGPROVISO"+",OUTOFTERGETPROVISO, DATESEARCHDIV"+
	") VALUES (" +
	"" +
	"?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		/** 削除処理 */
		PreparedStatement pstmt = null;
		try{
			pstmt = conn.prepareStatement(delQuery);
			pstmt.setString(1, individualId);
			pstmt.executeUpdate();
		}catch(SQLException sqle){
			System.out.println(delQuery);
			throw sqle;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();	
		}
		/** 挿入処理 */
		try{
			pstmt = conn.prepareStatement(insQuery);
			pstmt.setString(1, individualId.trim());
			if(districtProviso == null)
				pstmt.setNull(2, Types.VARCHAR);
			else
				pstmt.setString(2, CollectionUtil.deSplitComma(districtProviso));
			pstmt.setString(3, searchStemmaDiv);
			//中系統だったら、中系統コードを入れる
			if(searchStemmaDiv.equals("1")){
				if(stemma2Code == null)
					pstmt.setNull(4, Types.VARCHAR);
				else
					pstmt.setString(4, CollectionUtil.deSplitComma(stemma2Code));
			}
			//小系統(文系)または小系統(理系)だったら、分野コードを入れる
			else{// if(searchStemmaDiv.equals("2") || searchStemmaDiv.equals("3")){
				if(stemmaCode == null)
					pstmt.setNull(4, Types.VARCHAR);
				else
					pstmt.setString(4, CollectionUtil.deSplitComma(stemmaCode));
			}
		
			pstmt.setString(5, descImposeDiv);
			pstmt.setString(6, subImposeDiv);
			if(imposeSub == null)
				pstmt.setNull(7, Types.VARCHAR);
			else
				pstmt.setString(7, CollectionUtil.deSplitComma(imposeSub));
			if(schoolDivProviso == null)
				pstmt.setNull(8, Types.VARCHAR);
			else
				pstmt.setString(8, CollectionUtil.deSplitComma(schoolDivProviso));
			pstmt.setString(9, ShortDatePlusUtil.getUnformattedString(startMonth+"/"+startDate));
			pstmt.setString(10, ShortDatePlusUtil.getUnformattedString(endMonth+"/"+endDate));
			pstmt.setString(11, raingDiv);
			if(raingProviso == null)
				pstmt.setNull(12, Types.VARCHAR);
			else
				pstmt.setString(12, CollectionUtil.deSplitComma(raingProviso));
			if(outOfTergetProviso == null)
				pstmt.setNull(13, Types.VARCHAR);
			else
				pstmt.setString(13, CollectionUtil.deSplitComma(outOfTergetProviso));
			if(dateSearchDiv == null){
				pstmt.setNull(14, Types.VARCHAR);
			}else{
				pstmt.setString(14, dateSearchDiv);	
			}
			pstmt.executeUpdate();
		}catch(SQLException sqle){
			System.out.println(insQuery);
			sqle.printStackTrace();
			throw sqle;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();	
		}
	}
	
	/**
	 * @return
	 */
	public String getDescImposeDiv() {
		return descImposeDiv;
	}

	/**
	 * @return
	 */
	public String[] getDistrictProviso() {
		return districtProviso;
	}

	/**
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @return
	 */
	public String getEndMonth() {
		return endMonth;
	}

	/**
	 * @return
	 */
	public String[] getImposeSub() {
		return imposeSub;
	}

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @return
	 */
	public String[] getOutOfTergetProviso() {
		return outOfTergetProviso;
	}

	/**
	 * @return
	 */
	public String getRaingDiv() {
		return raingDiv;
	}

	/**
	 * @return
	 */
	public String[] getRaingProviso() {
		return raingProviso;
	}

	/**
	 * @return
	 */
	public String[] getSchoolDivProviso() {
		return schoolDivProviso;
	}

	/**
	 * @return
	 */
	public String getSearchStemmaDiv() {
		return searchStemmaDiv;
	}

	/**
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @return
	 */
	public String getStartMonth() {
		return startMonth;
	}

	/**
	 * @return
	 */
	public String[] getStemma10Code() {
		return stemma10Code;
	}

	/**
	 * @return
	 */
	public String[] getStemma11Code() {
		return stemma11Code;
	}

	/**
	 * @return
	 */
	public String[] getStemma2Code() {
		return stemma2Code;
	}

	/**
	 * @return
	 */
	public String[] getStemmaCode() {
		return stemmaCode;
	}

	/**
	 * @return
	 */
	public String getSubImposeDiv() {
		return subImposeDiv;
	}

	/**
	 * @param string
	 */
	public void setDescImposeDiv(String string) {
		descImposeDiv = string;
	}

	/**
	 * @param strings
	 */
	public void setDistrictProviso(String[] strings) {
		districtProviso = strings;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string) {
		endDate = string;
	}

	/**
	 * @param string
	 */
	public void setEndMonth(String string) {
		endMonth = string;
	}

	/**
	 * @param strings
	 */
	public void setImposeSub(String[] strings) {
		imposeSub = strings;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

	/**
	 * @param strings
	 */
	public void setOutOfTergetProviso(String[] strings) {
		outOfTergetProviso = strings;
	}

	/**
	 * @param string
	 */
	public void setRaingDiv(String string) {
		raingDiv = string;
	}

	/**
	 * @param strings
	 */
	public void setRaingProviso(String[] strings) {
		raingProviso = strings;
	}

	/**
	 * @param strings
	 */
	public void setSchoolDivProviso(String[] strings) {
		schoolDivProviso = strings;
	}

	/**
	 * @param string
	 */
	public void setSearchStemmaDiv(String string) {
		searchStemmaDiv = string;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string) {
		startDate = string;
	}

	/**
	 * @param string
	 */
	public void setStartMonth(String string) {
		startMonth = string;
	}

	/**
	 * @param strings
	 */
	public void setStemma10Code(String[] strings) {
		stemma10Code = strings;
	}

	/**
	 * @param strings
	 */
	public void setStemma11Code(String[] strings) {
		stemma11Code = strings;
	}

	/**
	 * @param strings
	 */
	public void setStemma2Code(String[] strings) {
		stemma2Code = strings;
	}

	/**
	 * @param strings
	 */
	public void setStemmaCode(String[] strings) {
		stemmaCode = strings;
	}

	/**
	 * @param string
	 */
	public void setSubImposeDiv(String string) {
		subImposeDiv = string;
	}

	/**
	 * @return
	 */
	public String getDateSearchDiv() {
		return dateSearchDiv;
	}

	/**
	 * @param string
	 */
	public void setDateSearchDiv(String string) {
		dateSearchDiv = string;
	}

}

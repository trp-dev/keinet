/**
 * 校内成績分析−クラス比較　設問別成績（クラス比較）
 *      国語評価別人数 問別評価-校内全体 データクラス
 * 作成日: 2019/09/05
 * @author      M.Ooseto
 */

package jp.co.fj.keinavi.excel.data.school;

public class S33KokugoQuestionListBean {
    // 設問番号
    private String questionNo = "";

    // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD START
    // 校内全体−a評価　割合
    private float evalACompratio = 0;
    // 校内全体−a*評価　割合
    private float evalAAstCompratio = 0;
    // 校内全体−b評価　割合
    private float evalBCompratio = 0;
    // 校内全体−b*評価　割合
    private float evalBAstCompratio = 0;
    // 校内全体−c評価　割合
    private float evalCCompratio = 0;
    // 校内全体−d評価　割合
    //private float evalDCompratio = 0;
    // 校内全体−a評価　人数
    private int evalANumbers = 0;
    // 校内全体−a*評価　人数
    private int evalAAstNumbers = 0;
    // 校内全体−b評価　人数
    private int evalBNumbers = 0;
    // 校内全体−b*評価　人数
    private int evalBAstNumbers = 0;
    // 校内全体−c評価　人数
    private int evalCNumbers = 0;
    // 校内全体−d評価　人数
    //private int evalDNumbers = 0;
    // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD END

    /*-----*/
    /* Get */
    /*-----*/
    public String getQuestionNo() {
        return this.questionNo;
    }
    // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD START
    public float getEvalACompratio() {
        return this.evalACompratio;
    }
    public float getEvalAAstCompratio() {
        return this.evalAAstCompratio;
    }
    public float getEvalBCompratio() {
        return this.evalBCompratio;
    }
    public float getEvalBAstCompratio() {
        return this.evalBAstCompratio;
    }
    public float getEvalCCompratio() {
        return this.evalCCompratio;
    }
//    public float getEvalDCompratio() {
//        return this.evalDCompratio;
//    }
    public int getEvalANumbers() {
        return this.evalANumbers;
    }
    public int getEvalAAstNumbers() {
        return this.evalAAstNumbers;
    }
    public int getEvalBNumbers() {
        return this.evalBNumbers;
    }
    public int getEvalBAstNumbers() {
        return this.evalBAstNumbers;
    }
    public int getEvalCNumbers() {
        return this.evalCNumbers;
    }
//    public int getEvalDNumbers() {
//        return this.evalDNumbers;
//    }
    // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD END

    /*-----*/
    /* Set */
    /*-----*/
    public void setQuestionNo(String questionNo) {
        this.questionNo = questionNo;
    }
    // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD START
    public void setEvalACompratio(float evalACompratio) {
        this.evalACompratio = evalACompratio;
    }
    public void setEvalAAstCompratio(float evalAAstCompratio) {
        this.evalAAstCompratio = evalAAstCompratio;
    }
    public void setEvalBCompratio(float evalBCompratio) {
        this.evalBCompratio = evalBCompratio;
    }
    public void setEvalBAstCompratio(float evalBAstCompratio) {
        this.evalBAstCompratio = evalBAstCompratio;
    }
    public void setEvalCCompratio(float evalCCompratio) {
        this.evalCCompratio = evalCCompratio;
    }
//    public void setEvalDCompratio(float evalDCompratio) {
//        this.evalDCompratio = evalDCompratio;
//    }
    public void setEvalANumbers(int evalANumbers) {
        this.evalANumbers = evalANumbers;
    }
    public void setEvalAAstNumbers(int evalAAstNumbers) {
        this.evalAAstNumbers = evalAAstNumbers;
    }
    public void setEvalBNumbers(int evalBNumbers) {
        this.evalBNumbers = evalBNumbers;
    }
    public void setEvalBAstNumbers(int evalBAstNumbers) {
        this.evalBAstNumbers = evalBAstNumbers;
    }
    public void setEvalCNumbers(int evalCNumbers) {
        this.evalCNumbers = evalCNumbers;
    }
//    public void setEvalDNumbers(int evalDNumbers) {
//        this.evalDNumbers = evalDNumbers;
//    }
    // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD END

}

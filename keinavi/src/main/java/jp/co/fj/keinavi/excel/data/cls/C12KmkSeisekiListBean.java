package jp.co.fj.keinavi.excel.data.cls;

/**
 * NX¬ÑTµ|ÈÚÊ¬Ñf[^Xg
 * ì¬ú: 2004/07/14
 * @author	H.Fujimoto
 *
 * 2009.11.24   Fujito URAKAWA - Totec
 *              uwÍxvÇÁÎ
 */
public class C12KmkSeisekiListBean {

    //³ÈR[h
    private String strKyokaCd = "";
    //³È¼
    private String strKyokamei = "";
    //^EÈÚR[h
    private String strKmkCd = "";
    //^EÈÚ¼
    private String strKmkmei = "";
    //z_
    private int intHaiten = 0;
    //¾_
    private int intTokuten = 0;
    //ZàÎ·l
    private float floHensaHome = 0;
    //SÎ·l
    private float floHensaAll = 0;
    //Zàl
    private int intNinzuHome = 0;
    //Sl
    private int intNinzuAll = 0;
    //·Z¾_
    private int intKansanTokuten = 0;
    //ZàÊ
    private int intJyuniHome = 0;
    //SÊ
    private int intJyuniAll = 0;
    //wÍx
    private String strScholarLevel = "";
    //ÍÍæª
    private int intScope=0;
    //îbÈÚtO
    private String basicFlg;
    // 2019/09/19 QQ)Tanouchi ¤ÊeXgÎ ADD START
    // 2019/11/25 QQ)Ooseto pêFè±úÎ DEL START
    // CEFRx»è
    //private String strCefrScore;
    // ÍíÊR[h
    //private String strMosiType;
    // 2019/11/25 QQ)Ooseto pêFè±úÎ DEL END
    // ê]¿A
    private String strKokugoAHyouka;
    // ê]¿B
    private String strKokugoBHyouka;
    // ê]¿C
    private String strKokugoCHyouka;
    // ê]¿
    private String strKokugoTotalHyouka;
    // 2019/09/19 QQ)Tanouchi ¤ÊeXgÎ ADD END

    /*----------*/
    /* Get      */
    /*----------*/

    public float getFloHensaAll() {
        return this.floHensaAll;
    }
    public float getFloHensaHome() {
        return this.floHensaHome;
    }
    public int getIntJyuniAll() {
        return this.intJyuniAll;
    }
    public int getIntJyuniHome() {
        return this.intJyuniHome;
    }
    public int getIntNinzuAll() {
        return this.intNinzuAll;
    }
    public int getIntNinzuHome() {
        return this.intNinzuHome;
    }
    public int getIntKansanTokuten() {
        return this.intKansanTokuten;
    }
    public int getIntHaiten() {
        return this.intHaiten;
    }
    public int getIntTokuten() {
        return this.intTokuten;
    }
    public String getStrKmkCd() {
        return this.strKmkCd;
    }
    public String getStrKmkmei() {
        return this.strKmkmei;
    }
    public String getStrKyokaCd() {
        return this.strKyokaCd;
    }
    public String getStrKyokamei() {
        return this.strKyokamei;
    }
    public String getStrScholarLevel() {
        return strScholarLevel;
    }
    public int getIntScope() {
        return intScope;
    }
    public String getBasicFlg() {
        return basicFlg;
    }
    // 2019/09/19 QQ)Tanouchi ¤ÊeXgÎ ADD START
    // 2019/11/25 QQ)Ooseto pêFè±úÎ DEL START
//	public String getStrCefrScore() {
//		return strCefrScore;
//	}
//	public String getStrMosiType() {
//		return strMosiType;
//	}
    // 2019/11/25 QQ)Ooseto pêFè±úÎ DEL END
	public String getStrKokugoAHyouka() {
		return strKokugoAHyouka;
	}
	public String getStrKokugoBHyouka() {
		return strKokugoBHyouka;
	}
	public String getStrKokugoCHyouka() {
		return strKokugoCHyouka;
	}
	public String getStrKokugoTotalHyouka() {
		return strKokugoTotalHyouka;
	}
    // 2019/09/19 QQ)Tanouchi ¤ÊeXgÎ ADD END
    /*----------*/
    /* Set      */
    /*----------*/

    public void setFloHensaAll(float floHensaAll) {
        this.floHensaAll = floHensaAll;
    }
    public void setFloHensaHome(float floHensaHome) {
        this.floHensaHome = floHensaHome;
    }
    public void setIntJyuniAll(int intJyuniAll) {
        this.intJyuniAll = intJyuniAll;
    }
    public void setIntJyuniHome(int intJyuniHome) {
        this.intJyuniHome = intJyuniHome;
    }
    public void setIntNinzuAll(int intNinzuAll) {
        this.intNinzuAll = intNinzuAll;
    }
    public void setIntNinzuHome(int intNinzuHome) {
        this.intNinzuHome = intNinzuHome;
    }
    public void setIntKansanTokuten(int intKansanTokuten) {
        this.intKansanTokuten = intKansanTokuten;
    }
    public void setIntHaiten(int intHaiten) {
        this.intHaiten = intHaiten;
    }
    public void setIntTokuten(int intTokuten) {
        this.intTokuten = intTokuten;
    }
    public void setStrKmkCd(String strKmkCd) {
        this.strKmkCd = strKmkCd;
    }
    public void setStrKmkmei(String strKmkmei) {
        this.strKmkmei = strKmkmei;
    }
    public void setStrKyokaCd(String strKyokaCd) {
        this.strKyokaCd = strKyokaCd;
    }
    public void setStrKyokamei(String strKyokamei) {
        this.strKyokamei = strKyokamei;
    }
    public void setStrScholarLevel(String string) {
        this.strScholarLevel = string;
    }
    public void setIntScope(int intScope) {
        this.intScope = intScope;
    }
    public void setBasicFlg(String basicFlg) {
        this.basicFlg = basicFlg;
    }
	// 2019/09/19 QQ)Tanouchi ¤ÊeXgÎ ADD START
    // 2019/11/25 QQ)Ooseto pêFè±úÎ DEL START
//	public void setStrCefrScore(String strCefrScore) {
//		this.strCefrScore = strCefrScore;
//	}
//	public void setStrMosiType(String strMosiType) {
//		this.strMosiType = strMosiType;
//	}
    // 2019/11/25 QQ)Ooseto pêFè±úÎ DEL END
	public void setStrKokugoAHyouka(String strKokugoAHyouka) {
		this.strKokugoAHyouka = strKokugoAHyouka;
	}
	public void setStrKokugoBHyouka(String strKokugoBHyouka) {
		this.strKokugoBHyouka = strKokugoBHyouka;
	}
	public void setStrKokugoCHyouka(String strKokugoCHyouka) {
		this.strKokugoCHyouka = strKokugoCHyouka;
	}
	public void setStrKokugoTotalHyouka(String strKokugoTotalHyouka) {
		this.strKokugoTotalHyouka = strKokugoTotalHyouka;
	}
    // 2019/09/19 QQ)Tanouchi ¤ÊeXgÎ ADD END
}
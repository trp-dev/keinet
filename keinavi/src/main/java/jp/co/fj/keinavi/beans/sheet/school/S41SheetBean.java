/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.sheet.SheetSchoolData;
import jp.co.fj.keinavi.excel.data.school.S41GakkoListBean;
import jp.co.fj.keinavi.excel.data.school.S41Item;
import jp.co.fj.keinavi.excel.data.school.S41ListBean;
import jp.co.fj.keinavi.excel.school.S41;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * @author kawai
 *
 */
public class S41SheetBean extends AbstractSheetBean {

	// データクラス
	private final S41Item data = new S41Item();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		String[] code = super.getSubjectCDArray(); // 型・科目設定値
		List graph = super.getSubjectGraphList(); // 型・科目グラフ表示対象
		List compSchoolList = super.getCompSchoolList(); // 比較対象高校
		boolean isNewExam = KNUtil.isNewExam(exam); // 新テストかどうか
		
		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntHyouFlg(super.getIntFlag(IProfileItem.CHART)); // 表フラグ
		data.setIntGraphFlg(super.getIntFlag(IProfileItem.GRAPH)); // グラフフラグ
		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		try {
			// データリストの取得
			Query query1 = QueryLoader.getInstance().load("s22_1");
			query1.setStringArray(1, code); // 型・科目コード

			ps1 = conn.prepareStatement(query1.toString());
			ps1.setString(1, exam.getExamYear()); // 対象年度
			ps1.setString(2, exam.getExamCD()); // 対象模試

			// 学校データリスト（自校）
			ps2 = conn.prepareStatement(QueryLoader.getInstance().load("s31_1").toString());
			ps2.setString(1, exam.getExamYear()); // 模試年度
			ps2.setString(2, exam.getExamCD()); // 模試コード
			ps2.setString(3, profile.getBundleCD()); // 一括コード

			// 学校データリスト（全国）
			ps3 = conn.prepareStatement(QueryLoader.getInstance().load("s41_1").toString());
			ps3.setString(1, exam.getExamYear()); // 模試年度
			ps3.setString(2, exam.getExamCD()); // 模試コード
			ps3.setString(3, exam.getExamYear()); // 模試年度
			ps3.setString(4, exam.getExamCD()); // 模試コード

			// 学校データリスト（県）
			ps4 = conn.prepareStatement(QueryLoader.getInstance().load("s41_2").toString());
			ps4.setString(1, exam.getExamYear()); // 模試年度
			ps4.setString(2, exam.getExamCD()); // 模試コード

			// 学校データリスト（他校）
			Query query2 = QueryLoader.getInstance().load("s41_3");
			super.outDate2InDate(query2); // データ開放日
			
			ps5 = conn.prepareStatement(query2.toString());
			ps5.setString(1, exam.getExamYear()); // 模試年度
			ps5.setString(2, exam.getExamCD()); // 模試コード
			ps5.setString(5, exam.getExamYear()); // 模試年度
			ps5.setString(6, exam.getExamCD()); // 模試コード

			List c1 = new LinkedList(); // 型の入れ物
			List c2 = new LinkedList(); // 科目の入れ物
				
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				S41ListBean bean = new S41ListBean();
				bean.setStrKmkCd(rs1.getString(1));
				bean.setStrKmkmei(rs1.getString(2));
				bean.setIntDispKmkFlg(graph.contains(rs1.getString(1)) ? 1 : 0);
				bean.setStrHaitenKmk(rs1.getString(3));
				
				if (rs1.getInt(1) >= 7000) c1.add(bean); // 型
				else c2.add(bean); // 科目

				// 型・科目コード
				ps2.setString(4, bean.getStrKmkCd()); 			
				ps3.setString(5, bean.getStrKmkCd());	
				ps4.setString(3, bean.getStrKmkCd());
				ps5.setString(3, bean.getStrKmkCd());
				
				// 自校
				{
					S41GakkoListBean g = new S41GakkoListBean();
					g.setStrGakkomei(profile.getBundleName());
					g.setIntDispGakkoFlg(1);

					rs2 = ps2.executeQuery();

					if (rs2.next()) {
						g.setIntNinzu(rs2.getInt(1));

						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							g.setFloHeikin(-999);
							g.setFloHensa(rs2.getFloat(2));
						} else {
							g.setFloHeikin(rs2.getFloat(2));
							g.setFloHensa(rs2.getFloat(3));
						}

					} else {
						g.setIntNinzu(-999);
						g.setFloHeikin(-999);
						g.setFloHensa(-999);
					}

					rs2.close();

					bean.getS41GakkoList().add(g);
				}
				
				// 比較対象高校分繰り返す
				Iterator ite = compSchoolList.iterator();
				while (ite.hasNext()) {
					SheetSchoolData school = (SheetSchoolData) ite.next();

					switch (school.getSchoolTypeCode()) {
						// 全国
						case 1:
							rs3 = ps3.executeQuery();
							break;
							
						// 県
						case 2:
							ps4.setString(4, school.getPrefCD()); // 県コード
							rs3 = ps4.executeQuery();
							break;
						
						// 高校
						case 3:
							ps5.setString(4, school.getSchoolCD()); // 一括コード
							rs3 = ps5.executeQuery();
							break;
					}

					if (rs3.next()) {
						S41GakkoListBean g = new S41GakkoListBean();
						g.setStrGakkomei(rs3.getString(1));
						g.setIntDispGakkoFlg(school.getGraphFlag());
						g.setIntNinzu(rs3.getInt(2));

						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							g.setFloHeikin(-999);
							g.setFloHensa(rs3.getFloat(3));
						} else {
							g.setFloHeikin(rs3.getFloat(3));
							g.setFloHensa(rs3.getFloat(4));
						}

						bean.getS41GakkoList().add(g);
					}
					
					rs3.close();
				}
			}

			// 型・科目の順番で入れる
			data.getS41List().addAll(c1);
			data.getS41List().addAll(c2);

		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(rs3);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(ps3);
			DbUtils.closeQuietly(ps4);
			DbUtils.closeQuietly(ps5);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S41_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_OTHER_SCORE;
	}
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S41()
			.s41(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
	}

}

package jp.co.fj.keinavi.servlets.shared_lib;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.servlets.DefaultLoginServlet;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class PrintDialogServlet extends DefaultLoginServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {
		forward(request, response, JSP_PRINT_DIALOG);
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#accessLog(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	protected void accessLog(final HttpServletRequest request) {
		// アクセスログは出力しない
	}

}

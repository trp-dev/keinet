/*
 * 作成日: 2015/10/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.helpdesk;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import jp.co.fj.keinavi.forms.helpdesk.HD101Form;

import com.fjh.beans.DefaultBean;

/**
 * ユーザ管理（証明書TSV出力前のチェック処理）
 *
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CertificatesTsvCheckBean extends DefaultBean {

	HD101Form 	hd101Form = null;	// HD101Form 情報

	private int  successUser	=0;			// 成功ユーザ
	private int  failureUser	=0;			// 失敗ユーザ

	private boolean isDelUserExist = true;
	// エラー画面に渡す情報を作成
	private HelpDeskRegistBean hdrBean = new HelpDeskRegistBean();


	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// TODO 自動生成されたメソッド・スタブ

		String[] userId   = null;		// 選択されたユーザID
		String[] selUser   = null;		// 選択されたユーザID（画面指定のユーザ）
		boolean judge	  = true;
		int selUserCnt = 0;

		int errLineNo = 0;

		// コネクション設定
		this.hdrBean.setConnection(null, conn);

		// ユーザ削除選択
		if ("2".equals(this.hd101Form.getContent())) {

			// 削除対象ユーザの件数を取得
			if(SelectDeleteUser() == 0) {
				this.isDelUserExist = false;
			}

			return;
		}

		// 画面指定
		if ("0".equals(this.hd101Form.getTsvUser())) {
			selUser = this.hd101Form.getSelectUser().split(",");
			userId = new String[selUser.length / 3];

			// ユーザIDを取得する
			// 明細の３カラム目のみセット
			for (int i = 2; i < selUser.length; i += 3 ) {
				userId[selUserCnt] = selUser[i];
				selUserCnt++;
			}

		// ファイル指定
		} else {
			// CSVからユーザを取得
			userId = this.selectTemp();
		}

		for ( int userPos = 0; userPos < userId.length; userPos++ ) {

			// 判定
			judge = true;
			errLineNo = userPos + 1;

			try {
				// 契約校マスタの存在チェック
				String selUserId = SelectPactSchool(userId[userPos]);
				if ( selUserId == null ) {
					hdrBean.setErrorList(errLineNo, "", userId[userPos], "契約校マスタに存在しません。");
					judge = false;
				}

				//---- 判定 --------------------------------------
				if (judge) {
					successUser++;		// 成功ユーザ
				} else {
					failureUser++;		// 失敗ユーザ
				}

			} catch (SQLException ex) {
				throw ex;
			} catch (Exception ex) {
				throw ex;
			} finally{

			}

		}

	}

	/**
	 *
	 * 契約校マスタから学校コードを取得する
	 *
	 * @param userId  ユーザID
	 * @return count
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private String SelectPactSchool(String UserId)
		throws SQLException {

		String USERID = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();

		try {
			sb.append(" SELECT  NVL(USERID, ' ')");
			sb.append("   FROM  PACTSCHOOL");
			sb.append("  WHERE  USERID = ?");

			stmt = conn.prepareStatement(sb.toString());
			stmt.setString(1, UserId);

			rs = stmt.executeQuery();
			while (rs.next()) {
				USERID = (rs.getString(1));
			}
		} catch (SQLException ex) {
			throw new SQLException("契約校マスタの取得に失敗しました。");
		} catch (Exception ex) {
			throw new SQLException("契約校マスタの取得に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return USERID;
	}

	/**
	 *
	 * CSVファイルのアップロード結果から処理対象のユーザIDを取得する
	 *
	 * @return String[]
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private String[] selectTemp() throws SQLException {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		ArrayList userIdList = new ArrayList();

		try {
			sb.append(" SELECT  ");
			sb.append("        LINENO");
			sb.append("      , USERID");
			sb.append("   FROM CERT_TARGET");
			sb.append(" ORDER BY");
			sb.append("        LINENO");

			stmt = conn.prepareStatement(sb.toString());

			rs = stmt.executeQuery();
			while (rs.next()) {
				userIdList.add(rs.getString(2));
			}
		} catch (SQLException ex) {
			throw new SQLException("CSVアップロード結果の取得に失敗しました。");
		} catch (Exception ex) {
			throw new SQLException("CSVアップロード結果の取得に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}

		return (String[])userIdList.toArray(new String[0]);
	}

	/**
	 *
	 * 契約校マスタから削除対象ユーザを取得する
	 *
	 * @return count
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private int SelectDeleteUser()
		throws SQLException {

		int userCnt = 0;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		String fromDate = "";
		String toDate = "";

		try {

			// 日付取得用
			Date dt = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);

			// 去年の５月１日
			fromDate = String.valueOf(cal.get(Calendar.YEAR)  - 1) + "0501";
			// 今年の４月末
			toDate = String.valueOf(cal.get(Calendar.YEAR)) + "0430";

			// SELECT文作成
			sb.append(" SELECT COUNT(T1.USERID)");
			sb.append("   FROM PACTSCHOOL T1");
			sb.append("  WHERE 1=1");
			sb.append("    AND (");
									// 期限切れ
			sb.append("          (");
			sb.append("           T1.ISSUED_BEFOREDATE BETWEEN ? AND ?");
			sb.append("          )");
			sb.append("        OR EXISTS");
									// 私書箱機能提供フラグが 1 ⇒ 0
			sb.append("          (SELECT 1");
			sb.append("             FROM PACTSCHOOLSAVE T2");
			sb.append("            WHERE 1=1");
			sb.append("              AND T1.FEATPROVFLG = '0'");
			sb.append("              AND T2.FEATPROVFLG = '1'");
			sb.append("              AND T1.USERID      = T2.USERID");
			sb.append("          )");
			sb.append("        )");
			sb.append(" ORDER BY");
			sb.append("        T1.USERID");

			stmt = conn.prepareStatement(sb.toString());
			stmt.setString(1, fromDate);
			stmt.setString(2, toDate);

			rs = stmt.executeQuery();
			while (rs.next()) {
				userCnt = Integer.parseInt(rs.getString(1));
			}

		} catch (SQLException ex) {
			throw new SQLException("契約校マスタの取得に失敗しました。");
		} catch (Exception ex) {
			throw new SQLException("契約校マスタの取得に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return userCnt;
	}

	/**
	 * @return HD101Form
	 */
	public HD101Form getHD101Form() {
		return hd101Form;
	}
	/**
	 * @param string
	 */
	public void setHD101Form(HD101Form form) {
		hd101Form = form;
	}

	/**
	 * @return
	 */
	public int getSuccessUser() {
		return successUser;
	}

	/**
	 * @param i
	 */
	public void setSuccessUser(int i) {
		successUser = i;
	}

	/**
	 * @return
	 */
	public int getFailureUser() {
		return failureUser;
	}

	/**
	 * @param i
	 */
	public void setFailureUser(int i) {
		failureUser = i;
	}

	/**
	 * @return
	 */
	public boolean getIsDelUserExist() {
		return isDelUserExist;
	}

	/**
	 * @param bean
	 */
	public void setIsDelUserExist(boolean flg) {
		isDelUserExist = flg;
	}

	/**
	 * @return
	 */
	public HelpDeskRegistBean getHdrBean() {
		return hdrBean;
	}

	/**
	 * @param bean
	 */
	public void setHdrBean(HelpDeskRegistBean bean) {
		hdrBean = bean;
	}
}

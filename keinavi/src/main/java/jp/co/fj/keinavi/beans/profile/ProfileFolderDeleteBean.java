/*
 * 作成日: 2004/07/02
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.profile;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.LoginSession;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ProfileFolderDeleteBean extends DefaultBean {
	
	private String folderID; // プロファイルフォルダID
	private String bundleCD; // 一括コード
	private LoginSession loginSession; // ログイン情報

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		try {
			StringBuffer query = new StringBuffer();
			query.append("DELETE FROM profilefolder f WHERE f.profilefid = ? ");
			query.append("AND NOT EXISTS (");
			query.append("SELECT * FROM profile p ");
			query.append("WHERE p.profilefid = f.profilefid AND p.updngflg = 1");
			query.append(") ");
		
			// 高校
			if (loginSession.isSchool()) {
				query.append("AND bundlecd = ? AND sectorsortingcd IS NULL");

				ps1 = conn.prepareStatement(query.toString());
				ps1.setString(2, bundleCD); // 一括コード

			// 営業部または校舎
			} else if (loginSession.isBusiness()) {
				query.append("AND userid IS NULL AND bundlecd IS NULL AND sectorsortingcd = ?");

				ps1 = conn.prepareStatement(query.toString());
				ps1.setString(2, loginSession.getSectorSortingCD()); // 部門分類コード

			// 営業 → 高校
			} else if (loginSession.isDeputy()) {
				query.append("AND userid IS NULL AND bundlecd = ? AND sectorsortingcd = ?");
				ps1 = conn.prepareStatement(query.toString());

				ps1.setString(2, bundleCD); // 一括コード
				ps1.setString(3, loginSession.getSectorSortingCD()); // 部門分類コード
			}

			ps1.setString(1, folderID); // プロファイルフォルダID
			
			// 削除成功なら中身も消す
			if (ps1.executeUpdate() == 1) {
				// BLOB
				ps2 = conn.prepareStatement(
					"DELETE FROM profileparam "
					+ "WHERE profileid IN "
					+ "(SELECT profileid FROM profile WHERE profilefid = ?)");
				ps2.setString(1, folderID);
				ps2.execute();
				ps2.close();
				
				// プロファイル
				ps3 = conn.prepareStatement("DELETE FROM profile WHERE profilefid = ?");
				ps3.setString(1, folderID);
				ps3.execute();
			}
		} finally {
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);			
			DbUtils.closeQuietly(ps3);			
		}
	}

	// --------------------------------------------------------------------------------

	/**
	 * @return
	 */
	public String getBundleCD() {
		return bundleCD;
	}

	/**
	 * @return
	 */
	public String getFolderID() {
		return folderID;
	}

	/**
	 * @return
	 */
	public LoginSession getLoginSession() {
		return loginSession;
	}

	/**
	 * @param string
	 */
	public void setBundleCD(String string) {
		bundleCD = string;
	}

	/**
	 * @param string
	 */
	public void setFolderID(String string) {
		folderID = string;
	}

	/**
	 * @param session
	 */
	public void setLoginSession(LoginSession session) {
		loginSession = session;
	}

}

/*
 * 作成日: 2004/07/02
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.profile;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.data.profile.CountingDivData;
import jp.co.fj.keinavi.util.date.DateUtil;
import jp.co.fj.keinavi.util.date.ShortDateUtil;

import com.fjh.beans.DefaultBean;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CountingDivBean extends DefaultBean {

	private String schoolCD; // 学校コード
	private List countingDivList = new LinkedList(); // 集計区分オブジェクトリスト

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		if (schoolCD == null) return;

		// 日付用クラス
		DateUtil date = new ShortDateUtil();

		StringBuffer query = new StringBuffer();
		query.append("SELECT * FROM school ");
		query.append("WHERE (bundlecd = ? OR parentschoolcd = ?) ");		
		query.append("AND (newgetdate IS NULL OR newgetdate <= ?) ");
		query.append("AND (stopdate IS NULL OR stopdate >= ?) ");
		query.append("ORDER BY 1");

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query.toString());
			ps.setString(1, schoolCD);
			ps.setString(2, schoolCD);
			ps.setString(3, date.date2String());
			ps.setString(4, date.date2String());
			rs = ps.executeQuery();
			while (rs.next()) {
				CountingDivData data = new CountingDivData();
				data.setCountingDivCode(rs.getString(1));
				data.setCountingDivName(rs.getString(2));
				countingDivList.add(data);
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 集計区分リストのサイズを返す
	 * @return
	 */
	public int getCountingDivListSize() {
		return countingDivList.size();
	}

	/**
	 * 集計区分コード（一括コード）の配列を返す
	 * @return
	 */
	public String[] getCountingDivCodeArray() {
		List container = new LinkedList();
		
		Iterator ite = this.countingDivList.iterator();
		while (ite.hasNext()) {
			CountingDivData data = (CountingDivData) ite.next();
			container.add(data.getCountingDivCode());
		}

		return (String[]) container.toArray(new String[0]);
	}

	/**
	 * @return
	 */
	public List getCountingDivList() {
		return countingDivList;
	}

	/**
	 * @param string
	 */
	public void setSchoolCD(String string) {
		schoolCD = string;
	}

}

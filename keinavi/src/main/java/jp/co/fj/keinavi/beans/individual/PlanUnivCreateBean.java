package jp.co.fj.keinavi.beans.individual;

import java.sql.SQLException;
import java.util.Collection;

import jp.co.fj.keinavi.util.CollectionUtil;

/**
 *
 * 受験予定大学作成
 * 
 * <2010年度改修>
 * 2010.01.19	Tomohisa YAMADA - TOTEC
 * 				新規作成
 *
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class PlanUnivCreateBean extends PlanUnivBean {
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		create(individualId, univValues, univValue);
	}
	
	/**
	 * 作成
	 * ※PlanUnivBeanから移動
	 * @param individualId
	 * @param univValues
	 * @param univValue
	 * @throws Exception
	 */
	private void create(
			String individualId, 
			String[] univValues, 
			String univValue) throws Exception{

		if(univValues == null) {
			return;
		}
		
		String univCd = "";
		String facultyCd = "";
		String deptCd = "";
		String entExamTypeCd = "";
		
		//univValuesはそのままでは使えない、分解して下準備をする。
		Collection planDatas = createInsertUnivDatas(univValues, univValue);
		if(planDatas == null) {
			return;
		}
		
		//新規作成時は、univValueのデータからとればよい。
		String[] univData = CollectionUtil.splitComma(univValue);
		univCd			 = univData[0];	//大学
		facultyCd		 = univData[1];	//学部
		deptCd			 = univData[2];	//学科
		entExamTypeCd	 = univData[3];	//入試形態
					
		if(execChackPlanUniv(getIndividualId(), univCd, facultyCd, deptCd, entExamTypeCd)) {
			//登録可能
			//１，１，１のデータが無いようであれば、作成し追加
			planDatas = setNecessaryData(planDatas);
			if(execCountExamPlanUniv(individualId, univCd, facultyCd, deptCd) < 40) {
				execInsertPlanUniv(individualId, planDatas);//登録処理
			} else {
				setErrorMessage("受験予定の登録件数をオーバーしました\\n登録できる大学は最大４０です");
			}
		} else {
			setErrorMessage("受験予定が既に登録されています\\nこの受験予定は登録できません");
		}
	}

}

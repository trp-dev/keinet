/*
 * 作成日: 2004/08/20
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.data.individual.EntExamModeData;

import com.fjh.beans.DefaultBean;

/**
 * @author T.Yamada
 * 入試形態マスタの一覧を取得
 */
public class EntExamModeBean extends DefaultBean{

	/** 出力パラメター */
	List entExamModeList;

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		StringBuffer query = new StringBuffer("");
		query.append("SELECT ENTEXAMMODECD, ENTEXAMMODENAME");
		query.append(" FROM ENTEXAMMODE WHERE 1 = 1");
		//System.out.println(query.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement(query.toString());	
			rs = ps.executeQuery();
			entExamModeList = new ArrayList();
			while (rs.next()) {
				EntExamModeData data = new EntExamModeData();
				data.setEntExamModeCd(rs.getString("ENTEXAMMODECD"));
				data.setEntExamModeName(rs.getString("ENTEXAMMODENAME"));
				entExamModeList.add(data);
			}
		}catch(Exception e){
			throw e;
		}finally{
			if(ps != null) ps.close();
			if(rs != null) rs.close();
		}
		
	}

	/**
	 * @return
	 */
	public List getEntExamModeList() {
		return entExamModeList;
	}

	/**
	 * @param list
	 */
	public void setEntExamModeList(List list) {
		entExamModeList = list;
	}

}

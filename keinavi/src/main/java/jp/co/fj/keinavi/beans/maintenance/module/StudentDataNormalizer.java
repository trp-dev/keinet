package jp.co.fj.keinavi.beans.maintenance.module;

import jp.co.fj.keinavi.util.JpnStringConv;

import org.apache.commons.lang.StringUtils;

/**
 *
 * 生徒情報データの正規化を行うクラス
 * 
 * 2005.10.25	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public final class StudentDataNormalizer {
	
	// Singleton
	private static final StudentDataNormalizer INSTANCE =
			new StudentDataNormalizer();

	// コンストラクタ
	private StudentDataNormalizer() {
		
	}
	
	/**
	 * StudentDataNormalizerのインスタンスを取得する
	 * 
	 * @return StudentDataNormalizerのインスタンス
	 */
	public static StudentDataNormalizer getInstance() {
		return INSTANCE;
	}
	
	/**
	 * 個人IDを正規化する
	 * 
	 * @param individualId 個人ID
	 * @return 10桁に合わせた個人ID
	 */
	public String normalizeIndividualId(final String individualId) {

		return individualId == null || individualId.length() == 0
				? "" : StringUtils.leftPad(individualId, 10, '0');
	}
	/**
	 * クラス名を正規化する
	 * 
	 * @param className クラス名
	 * @return 大文字に変換後2桁に合わせたクラス名
	 */
	public String normalizeClassName(final String className) {

		// 大文字にして0補完して返す
		return className == null || className.length() == 0
				? "" : StringUtils.leftPad(className.toUpperCase(), 2, '0');
	}
	
	/**
	 * クラス番号を正規化する
	 * 
	 * @param classNo クラス番号
	 * @return 5桁に合わせたクラス番号
	 */
	public String normalizeClassNo(final String classNo) {

		// 0補完して返す
		return classNo == null || classNo.length() == 0
				? "" : StringUtils.leftPad(classNo, 5, '0');
	}
	
	/**
	 * カナ氏名を正規化する
	 * 
	 * @param nameHiragana かな氏名（ひらがな、全角カタカナ、半角カタカナ）
	 * @return 半角カタカナに変換したかな氏名
	 */
	public String normalizeNameKana(final String nameHiragana) {

		// ひらがな→半角カタカナ
		final String kana = JpnStringConv.hkana2Han(nameHiragana);
		// 全角カタカナ→半角カタカナ
		return JpnStringConv.kkanaZen2Han(kana == null ? "" : kana);
	}
	
	/**
	 * 生年月日を正規化する
	 * 
	 * @param birthday 生年月日
	 * @return スラッシュを除いて８桁にした生年月日
	 */
	public String normalizeBirthday(final String birthday) {

		if (birthday.indexOf('/') >= 0) {
			
			final String[] value = StringUtils.split(birthday, "/", 3);
			final StringBuffer buff = new StringBuffer();
			
			for (int i = 0; i < value.length; i++) {
				
				if (value[i].length() == 1) {
					buff.append('0');
				}
				buff.append(value[i]);
			}
			
			return buff.toString();
			
		} else {
			return birthday;
		}
	}
	
	/**
	 * 電話番号を正規化する
	 * 
	 * @param telNo 電話番号
	 * @return 「(」「)」「-」を除いた電話番号
	 */
	public String normalizeTelNo(final String telNo) {
		
		if (telNo == null || telNo.length() == 0) {
			return "";
		}
		
		final String value = telNo.replaceAll(
				"\\(", "").replaceAll("\\)", "").replaceAll("-", "");
		
		if (value.length() == 0 || value.startsWith("0") || value.length() == 11) {
			return value;
		} else {
			return "0" + value;
		}
	}	

}

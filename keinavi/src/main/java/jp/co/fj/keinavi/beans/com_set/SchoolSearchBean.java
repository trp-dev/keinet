/*
 * 作成日: 2004/07/01
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.com_set;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Set;

import jp.co.fj.keinavi.data.com_set.cm.SchoolData;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.JpnStringConv;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;
import jp.co.fj.keinavi.util.sql.SQLUtil;

import org.apache.commons.dbutils.DbUtils;

/**
 * 2005.02.15 Yoshimoto KAWAI - Totec
 *            自校を検索対象から外すように変更
 *
 * @author kawai
 *
 */
public class SchoolSearchBean extends AbstractDefaultSearchBean {

	private String button; // ボタン
	private String[] pref; // 県コードの配列
	private String[] schoolDiv; // 公私区分コードの配列
	private String rank[]; // ランクの配列
	private String examinees; // 受験人数
	private String view; // 表示対象（すべて／高校のみ／都道府県平均のみ）
	private String searchMode; // 検索モード（高校名／高校コード）
	private String searchStr; // 高校名／高校コードの文字列
	private String targetYear; // 対象年度
	private String targetExam; // 対象模試
	private String[] registedSchool; // 登録済みの学校コード
	private boolean sales; // 営業かどうか
	private String bundleCd; // 一括コード

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// 登録済みの学校コード
		Set registed = CollectionUtil.array2Set(registedSchool);

		// SQL
		Query query = QueryLoader.getInstance().load("cm02");
		// 絞込み条件
		StringBuffer cond = new StringBuffer();

		// 営業でなければ 01000 〜 47999 の範囲
		if (!sales) {
			cond.append("AND s.bundlecd <= '47999' ");
			cond.append("AND s.bundlecd >= '01000' ");
			cond.append("AND SUBSTR(s.bundlecd, 3, 3) <= '999' ");
		}

		// 「上記の条件で検索」ボタン
		if ("上記の条件で検索".equals(button)) {
			// 県コード（全結果に対して絞込み）
			if (pref != null) {
				query.append("AND facultycd IN('00'," + SQLUtil.concatComma(pref) +  ") ");
			}

			// ランク（高校に対して絞込み）
			if (rank != null) {
			    // 2020/03/24 QQ)Ooseto 障害対応 UPD START
			    //cond.append("AND s.rank IN(" + SQLUtil.concatComma(rank) + ") ");
			    cond.append("AND ( s.rank IN(" + SQLUtil.concatComma(rank) + ") ");
// 2019/11/1 QQ)nagai ランクがnullの場合を考慮 ADD START
			    if (Arrays.asList(rank).contains("なし")) {
			        cond.append("OR s.rank is null ");
			    }
// 2019/11/1 QQ)nagai ランクがnullの場合を考慮 ADD END
                            cond.append(" ) ");
                            // 2020/03/24 QQ)Ooseto 障害対応 UPD END
			}
// 2019/11/1 QQ)nagai ランクがnullの場合の考慮ができていないため削除 DEL START
			///// 2019/09/26 QQ)Tanioka ランク「なし」の検索障害対応 ADD START
			//if (Arrays.asList(rank).contains("なし")) {
			//	cond.append("OR s.rank is null ");
			//}
			///// 2019/09/26 QQ)Tanioka ランク「なし」の検索障害対応 ADD END
// 2019/11/1 QQ)nagai ランクがnullの場合の考慮ができていないため削除 DEL END

			// 公私区分（高校に対して絞込み）
			if (schoolDiv != null) {
				cond.append("AND s.nppdiv IN(" + SQLUtil.concatComma(schoolDiv) + ") ");
			}

			// 受験人数（全結果に対して絞込み）
			if (!"all".equals(examinees)) {
				query.append("AND numbers >= " + Integer.parseInt(examinees) + " ");
			}

			// 高校のみ表示
			if ("school".equals(view)) {
				query.append("AND SUBSTR(bundlecd, 3, 3) <> '999' ");
			}

			// 都道府県平均のみ表示
			if ("average".equals(view)) {
				query.append("AND SUBSTR(bundlecd, 3, 3) = '999' ");
			}

		// 「検索」ボタン
		} else if ("検索".equals(button)) {
			// 高校名
			if ("name".equals(searchMode)) {
				query.append("AND TO_SEARCHSTR(bundlename_kana) ");
				query.append("LIKE CONCAT(TO_SEARCHSTR(");
				query.append(
					SQLUtil.quote(
						JpnStringConv.hkana2Han(GeneralUtil.truncateString(searchStr, 20))
					)
				);
				query.append("), '%') ");
			// 高校コード
			} else if ("code".equals(searchMode)) {
				query.append("AND bundlecd = ");
				query.append(SQLUtil.quote(searchStr));
				query.append(" ");
			}

		// 高校名の頭文字
		} else {
			query.append("AND bundlename_kana LIKE CONCAT(");
			query.append(SQLUtil.quote(JpnStringConv.hkana2Han(button)));
			query.append(", '%') ");
		}

		// 自校は除く
		if (this.bundleCd != null) {
			query.append("AND bundlecd <> ");
			query.append(SQLUtil.quote(this.bundleCd));
			query.append(" ");
		}

		query.replaceAll("#", cond.toString());
		query.append("ORDER BY dispsequence, bundlecd");

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query.toString());
			ps.setString(1, targetYear); // 模試年度
			ps.setString(2, targetExam); // 模試コード
			ps.setString(3, targetYear); // 模試年度
			ps.setString(4, targetExam); // 模試コード
			ps.setString(5, targetYear); // 模試年度
			ps.setString(6, targetExam); // 模試コード
			ps.setString(7, targetYear); // 模試年度
			ps.setString(8, targetExam); // 模試コード

			rs = ps.executeQuery();
			while (rs.next()) {
				// 表示対象なら入れる
				if(super.isOutputItem()) {
					SchoolData data = new SchoolData();
					data.setSchoolCode(rs.getString(1));
					data.setSchoolName(rs.getString(2));
					data.setRank(rs.getString(4));
					data.setLocation(rs.getString(5));
					data.setExaminees(rs.getInt(6));
					data.setRegisted(registed.contains(data.getSchoolCode()));
					recordSet.add(data);

				// そうでなければダミー
				} else {
					recordSet.add(null);
				}
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}

		// ページ番号計算
		super.calculate();
	}

	/**
	 * @param string
	 */
	public void setExaminees(String string) {
		examinees = string;
	}

	/**
	 * @param strings
	 */
	public void setPref(String[] strings) {
		pref = strings;
	}

	/**
	 * @param strings
	 */
	public void setRank(String[] strings) {
		rank = strings;
	}

	/**
	 * @param string
	 */
	public void setSearchMode(String string) {
		searchMode = string;
	}

	/**
	 * @param string
	 */
	public void setSearchStr(String string) {
		searchStr = string;
	}

	/**
	 * @param string
	 */
	public void setView(String string) {
		view = string;
	}

	/**
	 * @return
	 */
	public String getButton() {
		return button;
	}

	/**
	 * @param string
	 */
	public void setButton(String string) {
		button = string;
	}

	/**
	 * @return
	 */
	public String[] getSchoolDiv() {
		return schoolDiv;
	}

	/**
	 * @param strings
	 */
	public void setSchoolDiv(String[] strings) {
		schoolDiv = strings;
	}

	/**
	 * @return
	 */
	public String getTargetExam() {
		return targetExam;
	}

	/**
	 * @return
	 */
	public String getTargetYear() {
		return targetYear;
	}

	/**
	 * @param string
	 */
	public void setTargetExam(String string) {
		targetExam = string;
	}

	/**
	 * @param string
	 */
	public void setTargetYear(String string) {
		targetYear = string;
	}

	/**
	 * @return
	 */
	public String[] getRegistedSchool() {
		return registedSchool;
	}

	/**
	 * @param strings
	 */
	public void setRegistedSchool(String[] strings) {
		registedSchool = strings;
	}

	/**
	 * @param b
	 */
	public void setSales(boolean b) {
		sales = b;
	}

	public void setBundleCd(String bundleCd) {
		this.bundleCd = bundleCd;
	}
}

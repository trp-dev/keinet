/*
 * 作成日: 2004/12/10
 */
package jp.co.fj.keinavi.beans.txt_out;

import java.io.IOException;
import java.util.Iterator;

/**
 * 
 * @author kawai
 */
public class CSVOutputBean extends DirectOutputTextBean {

    /* (非 Javadoc)
     * @see jp.co.fj.keinavi.beans.txt_out.DirectOutputTextBean#setupLine()
     */
    public void setupLine() throws IOException {
		int index = 0;
		boolean first = true;
		for (Iterator ite = this.container.iterator(); ite.hasNext(); ) {
		    String value = (String) ite.next();
		    if (value == null) value = "";
		    
			// 出力対象にセットされた項目ならば出力
			if (outSet.contains(new Integer(++index))) {
			    if (first) {
			        first = false;
			    } else {
			        writer.write(",");
			    }
		        writer.write(value);
			}
		}
    }

}

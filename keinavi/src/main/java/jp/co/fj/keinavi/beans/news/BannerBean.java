package jp.co.fj.keinavi.beans.news;


/**
 * @author FWEST
 * 2014/10 バナー広告追加対応
 */
public class BannerBean {

    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    /**バナー管理番号1*/
    private String bannerCode1;
    /**バナー管理番号2*/
    private String bannerCode2;
    /**バナー名*/
    private String bannerName;
    /**バナー画像ファイル格納先*/
    private String imgSrc;
    /**接続Url*/
    private String url;

    /**
     * bannerCode1を返します。
     *
     * @return bannerCode1
     */
    public String getBannerCode1() {
        return bannerCode1;
    }

    /**
     * bannerCode1をセットします。
     *
     * @param bannerCode1 bannerCode1
     */
    public void setBannerCode1(String bannerCode1) {
        this.bannerCode1 = bannerCode1;
    }

    /**
     * bannerCode2を返します。
     *
     * @return bannerCode2
     */
    public String getBannerCode2() {
        return bannerCode2;
    }

    /**
     * bannerCode2をセットします。
     *
     * @param bannerCode2 bannerCode2
     */
    public void setBannerCode2(String bannerCode2) {
        this.bannerCode2 = bannerCode2;
    }

    /**
     * bannerNameを返します。
     *
     * @return bannerName
     */
    public String getBannerName() {
        return bannerName;
    }

    /**
     * bannerNameをセットします。
     *
     * @param bannerName bannerName
     */
    public void setBannerName(String bannerName) {
        this.bannerName = bannerName;
    }

    /**
     * imgSrcを返します。
     *
     * @return imgSrc
     */
    public String getImgSrc() {
        return imgSrc;
    }

    /**
     * imgSrcをセットします。
     *
     * @param imgSrc imgSrc
     */
    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    /**
     * urlを返します。
     *
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * urlをセットします。
     *
     * @param url url
     */
    public void setUrl(String url) {
        this.url = url;
    }

}

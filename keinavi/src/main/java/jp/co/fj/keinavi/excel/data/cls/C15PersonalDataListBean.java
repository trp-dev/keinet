package jp.co.fj.keinavi.excel.data.cls;

/**
 * クラス成績概況−志望者データリスト
 * 作成日: 2004/08/30
 * @author	Ito.Y
 */
public class C15PersonalDataListBean {
	//学年
	private String strGrade = "";
	//クラス
	private String strClass = "";
	//クラス番号
	private String strClassNo = "";
	//氏名
	private String strKanaName = "";
	//性別
	private String strSex = "";
	//志望学科コード
	private String strGakkaCd = "";
	//志望学科名
	private String strGakkaMei = "";
	//日程
	private String strNittei = "";
	//センター評価
	private String strHyoukaMark = "";
	//２次評価
	private String strHyoukaKijyutsu = "";
	//総合評価
	private String strHyoukaAll = "";
	//出願予定
	private int intYoteiFlg = 0;

	//2019/09/06 QQ)Oosaki 共通テスト対応 UPD START
	////2019/08/14 Hics)Ueta 共通テスト対応 ADD START
	////評価別人数A
	//private String intRatingnumA= "";
	////評価別人数B
	//private String intRatingnumB= "";
	////評価別人数C
	//private String intRatingnumC= "";
	////評価別人数D
	//private String intRatingnumD= "";
	////評価別人数E
	//private String intRatingnumE= "";
	////2019/08/14 Hics)Ueta 共通テスト対応 ADD END
	//共通テスト評価(英語認定試験含む)
	private String strHyoukaMarkEngPt = "";
	//英語資格要件-出願資格判定
	private String strAppQuelJudge = "";
	//英語資格要件-出願基準非対応告知
	private String strAppQuelNotice = "";
	//2019/09/06 QQ)Oosaki 共通テスト対応 UPD END

	/*----------*/
	/* Get      */
	/*----------*/

	public int getIntYoteiFlg() {
		return this.intYoteiFlg;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrClassNo() {
		return this.strClassNo;
	}
	public String getStrHyoukaKijyutsu() {
		return this.strHyoukaKijyutsu;
	}
	public String getStrHyoukaMark() {
		return this.strHyoukaMark;
	}
	public String getStrKanaName() {
		return this.strKanaName;
	}
	public String getStrSex() {
		return this.strSex;
	}
	public String getStrGakkaCd() {
		return strGakkaCd;
	}
	public String getStrGakkaMei() {
		return strGakkaMei;
	}
	public String getStrGrade() {
		return strGrade;
	}
	public String getStrHyoukaAll() {
		return strHyoukaAll;
	}
	public String getStrNittei() {
		return strNittei;
	}
	//2019/09/06 QQ)Oosaki 共通テスト対応 UPD START
	////2019/08/14 Hics)Ueta 共通テスト対応 ADD START
	//public String getIntRatingnumA() {
	//	return intRatingnumA;
	//}
	//public String getIntRatingnumB() {
	//	return intRatingnumB;
	//}
	//public String getIntRatingnumC() {
	//	return intRatingnumC;
	//}
	//public String getIntRatingnumD() {
	//	return intRatingnumD;
	//}
	//public String getIntRatingnumE() {
	//	return intRatingnumE;
	//}
	////2019/08/14 Hics)Ueta 共通テスト対応 ADD END
	public String getStrHyoukaMarkEngPt() {
	        return strHyoukaMarkEngPt;
	}
	public String getStrAppQuelJudge() {
	        return strAppQuelJudge;
	}
	public String getStrAppQuelNotice() {
	        return strAppQuelNotice;
	}
	//2019/09/06 QQ)Oosaki 共通テスト対応 UPD END

	/*----------*/
	/* Set      */
	/*----------*/

	public void setIntYoteiFlg(int intYoteiFlg) {
		this.intYoteiFlg = intYoteiFlg;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrClassNo(String strClassNo) {
		this.strClassNo = strClassNo;
	}
	public void setStrHyoukaKijyutsu(String strHyoukaKijyutsu) {
		this.strHyoukaKijyutsu = strHyoukaKijyutsu;
	}
	public void setStrHyoukaMark(String strHyoukaMark) {
		this.strHyoukaMark = strHyoukaMark;
	}
	public void setStrKanaName(String strKanaName) {
		this.strKanaName = strKanaName;
	}
	public void setStrSex(String strSex) {
		this.strSex = strSex;
	}
	public void setStrGakkaCd(String string) {
		strGakkaCd = string;
	}
	public void setStrGakkaMei(String string) {
		strGakkaMei = string;
	}
	public void setStrGrade(String string) {
		strGrade = string;
	}
	public void setStrHyoukaAll(String string) {
		strHyoukaAll = string;
	}
	public void setStrNittei(String string) {
		strNittei = string;
	}
	//2019/09/06 QQ)Oosaki 共通テスト対応 UPD START
	////2019/08/14 Hics)Ueta 共通テスト対応 ADD START
	//public void setIntRatingnumA(String string) {
	//	intRatingnumA = string;
	//}
	//public void setIntRatingnumB(String string) {
	//	intRatingnumB = string;
	//}
	//public void setIntRatingnumC(String string) {
	//	intRatingnumC = string;
	//}
	//public void setIntRatingnumD(String string) {
	//	intRatingnumD = string;
	//}
	//public void setIntRatingnumE(String string) {
	//	intRatingnumE = string;
	//}
	////2019/08/14 Hics)Ueta 共通テスト対応 ADD END
	public void setStrHyoukaMarkEngPt(String string) {
	        strHyoukaMarkEngPt = string;
	}
	public void setStrAppQuelJudge(String string) {
	        strAppQuelJudge = string;
	}
	public void setStrAppQuelNotice(String string) {
	        strAppQuelNotice = string;
	}
	//2019/09/06 QQ)Oosaki 共通テスト対応 UPD END

}
package jp.co.fj.keinavi.servlets.com_set;

import java.io.IOException;
import java.sql.Connection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.com_set.ChargeClassBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.com_set.ChargeClassData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.com_set.CM701Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * 担当クラスServlet
 * 
 * 
 * 2005.02.24	Yoshimoto KAWAI - Totec
 * 				年度対応
 * 
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 * 
 * @author kawai
 * 
 */
public class CM701Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// プロファイル
		Profile profile = super.getProfile(request);
		// ログイン情報
		LoginSession login = super.getLoginSession(request);

		// 担当クラスリスト
		List container = (List) profile
			.getItemMap(IProfileCategory.CM)
			.get(IProfileItem.CLASS);

		// アクションフォームを取得
		CM701Form form = (CM701Form) super.getActionForm(request,
			"jp.co.fj.keinavi.forms.com_set.CM701Form");

		// 年度の初期値
		if (form.getYear() == null) {
			form.setYear(ProfileUtil.getChargeClassYear(profile));
		}
		
		// 「登録して閉じるボタン」
		if ("1".equals(form.getOperation())) {
			// まずクリア
			container.clear();
						
			// 詰める
			if (form.getClasses() != null) {
				final String[] classes = form.getClasses();
				for (int i=0; i<classes.length; i++) {
					final String[] values = StringUtils.split(classes[i], ',');
					container.add(new ChargeClassData(values[1], values[2]));
				}
			}

			// 年度
			profile.getItemMap(IProfileCategory.CM).put(
					IProfileItem.CLASS_YEAR, form.getYear());

			// プロファイル変更フラグを立てる
			profile.setChanged(true);

			super.forward(request, response, JSP_CLOSE);

		// デフォルト
		} else {
			
			// DBコネクション
			Connection con = null;
			try {
				con = super.getConnectionPool(request);

				// 担当クラスBean
				final ChargeClassBean bean =
						new ChargeClassBean(login.getUserID(), login.getAccount());
				bean.setConnection(null, con);
				bean.execute();
				request.setAttribute("ClassBean", bean);

				// 転送元がcm701なら年度を変えているので設定なし
				if ("cm701".equals(super.getBackward(request))) {
					throw new InternalError("意図しない画面遷移です。");
					
				// そうでなければ初期表示なので設定値を取得する
				} else {
					final List c = new LinkedList(); // 入れ物
					final Iterator ite = container.iterator();
					while (ite.hasNext()) {
						final ChargeClassData data = (ChargeClassData) ite.next();		
						c.add(form.getYear() + "," + data.getGrade() + "," + data.getClassName());
					}
					form.setClasses((String[]) c.toArray(new String[0]));
				}

			} catch (Exception e) {
				throw new ServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}

			request.setAttribute("form", form);
			request.setAttribute("CheckedMap", CollectionUtil.array2TrueMap(form.getClasses()));
			
			super.forward(request, response, JSP_CM701);
		}

	}

}

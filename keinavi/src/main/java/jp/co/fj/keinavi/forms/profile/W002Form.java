/*
 * 作成日: 2004/06/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.profile;

import jp.co.fj.keinavi.forms.BaseForm;


/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class W002Form extends BaseForm {

	private String countingDivCode; // 集計区分
	private String profileID; // プロファイルID
	private String current; // カレントフォルダID
	private String password; // メンテナンス用パスワード
	private String changeCountingDivFlag; // 集計区分変更フラグ
	
	// メニュータブID
	// 1 ... プロファイル選択
	// 2 ... プロファイル新規作成
	// 3 ... 成績分析開始
	private String tabId;
	
	// アクション
	//   1 ... フォルダ削除 
	//   2 ... プロファイル削除
	private String operation;

	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	/**
	 * @return
	 */
	public String getCountingDivCode() {
		return countingDivCode;
	}

	/**
	 * @param string
	 */
	public void setCountingDivCode(String string) {
		countingDivCode = string;
	}

	/**
	 * @return
	 */
	public String getCurrent() {
		return current;
	}

	/**
	 * @param string
	 */
	public void setCurrent(String string) {
		current = string;
	}

	/**
	 * @return
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * @param string
	 */
	public void setOperation(String string) {
		operation = string;
	}

	/**
	 * @return
	 */
	public String getProfileID() {
		return profileID;
	}

	/**
	 * @param string
	 */
	public void setProfileID(String string) {
		profileID = string;
	}

	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param string
	 */
	public void setPassword(String string) {
		password = string;
	}

	/**
	 * @return tabId を戻します。
	 */
	public String getTabId() {
		return tabId;
	}

	/**
	 * @param pTabId 設定する tabId。
	 */
	public void setTabId(String pTabId) {
		tabId = pTabId;
	}

	/**
	 * @return changeCountingDivFlag を戻します。
	 */
	public String getChangeCountingDivFlag() {
		return changeCountingDivFlag;
	}

	/**
	 * @param pChangeCountingDivFlag 設定する changeCountingDivFlag。
	 */
	public void setChangeCountingDivFlag(String pChangeCountingDivFlag) {
		changeCountingDivFlag = pChangeCountingDivFlag;
	}

}

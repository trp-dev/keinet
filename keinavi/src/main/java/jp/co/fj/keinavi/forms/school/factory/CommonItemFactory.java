/*
 * 作成日: 2004/07/28
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.school.factory;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import jp.co.fj.keinavi.data.com_set.ComSubjectData;
import jp.co.fj.keinavi.data.com_set.SubjectData;
import jp.co.fj.keinavi.forms.school.SMaxForm;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
/**
 *
 * 2005.04.05	Yoshimoto KAWAI - Totec
 * 				s205とs305の「過年度の表示」対応
 *
 * 2005.04.11	Yoshimoto KAWAI - Totec
 * 				日程・評価区分対応
 *
 * 2009.10.20 Fujito URAKAWA - Totec
 *            新校内資料「科目・設問別正答率（レベル別）」対応
 *
 * @author 奥村ゆかり
 *
 */
public class CommonItemFactory {

	SMaxForm form; // アクションフォーム
	Map item; // アイテムマップ
	Set formatItem; // フォーマット
	Set optionItem; // オプション

	/**
	 * @param form
	 * @param item
	 */
	public CommonItemFactory(SMaxForm form, Map item) {
		this.form = form;
		this.item = item;

		this.formatItem = CollectionUtil.array2Set(form.getFormatItem());
		this.optionItem = CollectionUtil.array2Set(form.getOptionItem());
	}

	/**
	 *
	 * 型・共通項目/個別設定項目をフォームにセットする
	 *
	 * @return void
	 *
	 */
	public void setItem0103() {
		// 型・共通項目設定利用
		item.put(IProfileItem.TYPE_USAGE, Short.valueOf(form.getCommonType()));

		// 個別選択
		if ("0".equals(form.getCommonType())) {
			ComSubjectData data = (ComSubjectData) item.get(IProfileItem.TYPE_IND);

			// なければ作る
			if (data == null) {
				data = new ComSubjectData();
				data.setISubjectData(new ArrayList());
				item.put(IProfileItem.TYPE_IND, data);

			// あるならクリア
			} else {
				data.getISubjectData().clear();
			}

			if (form.getAnalyzeType() != null) {
				// グラフ表示対象
				Set graph = CollectionUtil.array2Set(form.getGraphType());

				for (int i = 0; i < form.getAnalyzeType().length; i++) {
					SubjectData s = new SubjectData();
					s.setSubjectCD(form.getAnalyzeType()[i]);

					if (graph.contains(form.getAnalyzeType()[i])) {
						s.setGraphDisp((short) 1);
					} else {
						s.setGraphDisp((short) 0);
					}

					data.getISubjectData().add(s);
				}
			}
		}
	}

	/**
	 *
	 * 科目・共通項目/個別設定項目をフォームにセットする
	 *
	 * @return void
	 *
	 */
	public void setItem0203() {
		// 型・共通項目設定利用
		item.put(IProfileItem.COURSE_USAGE, Short.valueOf(form.getCommonCourse()));

		// 個別選択
		if ("0".equals(form.getCommonCourse())) {
			ComSubjectData data = (ComSubjectData) item.get(IProfileItem.COURSE_IND);

			// なければ作る
			if (data == null) {
				data = new ComSubjectData();
				data.setISubjectData(new ArrayList());
				item.put(IProfileItem.COURSE_IND, data);

			// あるならクリア
			} else {
				data.getISubjectData().clear();
			}

			if (form.getAnalyzeCourse() != null) {
				// グラフ表示対象
				Set graph = CollectionUtil.array2Set(form.getGraphCourse());

				for (int i = 0; i < form.getAnalyzeCourse().length; i++) {
					SubjectData s = new SubjectData();
					s.setSubjectCD(form.getAnalyzeCourse()[i]);

					if (graph.contains(form.getAnalyzeCourse()[i])) {
						s.setGraphDisp((short) 1);
					} else {
						s.setGraphDisp((short) 0);
					}

					data.getISubjectData().add(s);
				}
			}
		}
	}

	/**
	 * 表（構成比選択あり）の値をプロファイルにセットする
	 */
	public void setItem0602() {
		Short value = null;

		// 表あり
		if (formatItem.contains("Chart")) {
			if ("11".equals(form.getCompRatio())) value = new Short("11");
			else if ("12".equals(form.getCompRatio())) value = new Short("12");
			else throw new IllegalArgumentException("構成比表示の値が不正です。");

		// 表なし
		} else {
			if ("11".equals(form.getCompRatio())) value = new Short("1");
			else if ("12".equals(form.getCompRatio())) value = new Short("2");
			else throw new IllegalArgumentException("構成比表示の値が不正です。");
		}

		item.put(IProfileItem.CHART_COMP_RATIO, value);
	}

	/**
	 * 表（構成比選択あり）の値をプロファイルにセットする
	 * ※高校間比較・偏差値分布
	 */
	public void setB003Item0602() {
		Short value = null;

		if ("11".equals(form.getCompRatio())) value = new Short("11");
		else if ("12".equals(form.getCompRatio())) value = new Short("12");
		else throw new IllegalArgumentException("構成比表示の値が不正です。");

		item.put(IProfileItem.CHART_COMP_RATIO, value);
	}

	/**
	 * 偏差値帯別度数分布グラフの値をプロファイルにセットする
	 */
	public void setItem0703() {
		Short value = null;

		// あり
		if (formatItem.contains("GraphDist")) {
			if ("11".equals(form.getAxis())) value = new Short("11");
			else if ("22".equals(form.getAxis())) value = new Short("12");
			else throw new IllegalArgumentException("偏差値帯別度数分布グラフの値が不正です。");

		// なし
		} else {
			if ("11".equals(form.getAxis())) value = new Short("1");
			else if ("22".equals(form.getAxis())) value = new Short("2");
			else throw new IllegalArgumentException("偏差値帯別度数分布グラフの値が不正です。");
		}

		item.put(IProfileItem.GRAPH_DIST, value);
	}

	/**
	 * 偏差値帯別人数積み上げグラフの値をプロファイルにセットする
	 */
	public void setItem0705() {
		Short value = null;

		// あり
		if (optionItem.contains("GraphBuildup")) {
			if ("11".equals(form.getPitchBuildup())) value = new Short("11");
			else if ("12".equals(form.getPitchBuildup())) value = new Short("12");
			else if ("13".equals(form.getPitchBuildup())) value = new Short("13");
			else throw new IllegalArgumentException("偏差値帯別人数積み上げグラフの値が不正です。");

		// なし
		} else {
			if ("11".equals(form.getPitchBuildup())) value = new Short("1");
			else if ("12".equals(form.getPitchBuildup())) value = new Short("2");
			else if ("13".equals(form.getPitchBuildup())) value = new Short("3");
			else throw new IllegalArgumentException("偏差値帯別人数積み上げグラフの値が不正です。");
		}

		item.put(IProfileItem.GRAPH_BUILDUP, value);
	}

	/**
	 * 偏差値帯別構成比グラフの値をプロファイルへセットする
	 */
	public void setItem0702() {
		Short value = null;

		// あり
		if (optionItem.contains("GraphCompRatio")) {
			if ("11".equals(form.getPitchCompRatio())) value = new Short("11");
			else if ("12".equals(form.getPitchCompRatio())) value = new Short("12");
			else if ("13".equals(form.getPitchCompRatio())) value = new Short("13");
			else throw new IllegalArgumentException("偏差値帯別人数積み上げグラフの値が不正です。");

		// なし
		} else {
			if ("11".equals(form.getPitchCompRatio())) value = new Short("1");
			else if ("12".equals(form.getPitchCompRatio())) value = new Short("2");
			else if ("13".equals(form.getPitchCompRatio())) value = new Short("3");
			else throw new IllegalArgumentException("偏差値帯別人数積み上げグラフの値が不正です。");
		}

		item.put(IProfileItem.GRAPH_COMP_RATIO, value);
	}

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//	// 2019/07/17 QQ)Tanioka 共通テスト対応 ADD START
//	/**
//	 * 共通テスト英語認定試験CEFR取得状況の値をプロファイルへセットする
//	 */
//	public void setItem1602() {
//		Short value = null;
//
//		// あり
//		if (optionItem.contains("OptionCheckBox")) {
//			if ("11".equals(form.getTargetCheckBox())) value = new Short("11");
//			else if (null == form.getTargetCheckBox() || "12".equals(form.getTargetCheckBox())) value = new Short("12");
//			else throw new IllegalArgumentException("共通テスト英語認定試験CEFR取得状況の値が不正です。");
//		// なし
//		} else {
//			if ("11".equals(form.getTargetCheckBox())) value = new Short("1");
//			else if (null == form.getTargetCheckBox() || "12".equals(form.getTargetCheckBox())) value = new Short("2");
//			else throw new IllegalArgumentException("共通テスト英語認定試験CEFR取得状況の値が不正です。");
//		}
//
//		item.put(IProfileItem.OPTION_CHECK_BOX, value);
//	}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

	/**
	 * 国語 評価別人数の値をプロファイルへセットする
	 */
	public void setItem1701() {
		item.put(IProfileItem.OPTION_CHECK_KOKUGO,
				optionItem.contains("targetCheckKokugo") ? new Short("1") : new Short("0"));
	}

	/**
	 * 小設問別正答状況の値をプロファイルへセットする
	 */
	public void setItem1702() {
		item.put(IProfileItem.OPTION_CHECK_STATUS,
				optionItem.contains("targetCheckStatus") ? new Short("1") : new Short("0"));
	}

	/**
	 * 小設問別成績の値をプロファイルへセットする
	 */
	public void setItem1703() {
		item.put(IProfileItem.OPTION_CHECK_RECORD,
				optionItem.contains("targetCheckRecord") ? new Short("1") : new Short("0"));
	}
	// 2019/07/17 QQ)Tanioka 共通テスト対応 ADD END

	// 表
	public void setItem0601() {
		item.put(
			"0601",
			formatItem.contains("Chart") ? new Short("1") : new Short("0"));
	}

	// グラフ
	public void setItem0701() {
		item.put(
			"0701",
			formatItem.contains("Graph") ? new Short("1") : new Short("0"));
	}

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//	// 2019/07/17 QQ)Tanioka 共通テスト対応 ADD START
//	// 共通テスト英語認定試験CEFR取得状況
//	public void setItem1601() {
//		item.put(
//			IProfileItem.OPTION_CEFL,
//			formatItem.contains("Option") ? new Short("1") : new Short("0"));
//	}
//	// 2019/07/17 QQ)Tanioka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

	// マーク模擬試験正答状況
	public void setItem1504() {
		item.put(
			"1504",
			optionItem.contains("MarkExam") ? new Short("1") : new Short("0"));
	}

	// マーク模試高校別設問成績層正答率
	public void setItem1516() {
		item.put(
				"1516",
				optionItem.contains("MarkExamArea") ? new Short("1") : new Short("0"));
	}

	// 大学の表示順
	public void setItem0310() {
		item.put("0310", new Short(form.getDispUnivOrder()));
	}

	// 大学集計区分
	public void setItem0309() {
		item.put("0309", new Short(form.getDispUnivCount()));
	}

	// 表示対象（志望大学）
	public void setItem0311() {
		item.put("0311", form.getDispUnivStudent());
	}

	// 日程
	public void setItem0312() {
		if (form.getDispUnivSchedule() == null) {
			item.put("0312", null);
		} else {
			item.put("0312", new Short(form.getDispUnivSchedule()));
		}
	}

	// 評価区分
	public void setItem0313() {
		item.put("0313", form.getDispUnivRating());
	}

	/**
	 * クラスの表示順序
	 */
	public void setItem0502() {
		item.put("0502", new Short(form.getDispClassOrder()));
	}

	/**
	 * クラスの表示順序
	 * 校内成績分析・過回比較（成績概況）
	 * 過回・クラス比較オプションが未チェックの場合は処理しない
	 */
	public void setS402Item0502() {
		if (optionItem.contains("CompClass"))
			item.put("0502", new Short(form.getDispClassOrder()));
	}

	/**
	 *
	 *  セキュティスタンプをプロファイルにセットする
	 *
	 * @return void
	 *
	 */
	public void setItem1305() {
		item.put("1305", new Short(form.getStamp()));
	}


	/**
	 *
	 *  型・科目別成績順位をプロファイルにセットする
	 *
	 * @return void
	 *
	 */
	public void setItem1501() {
		item.put(
			"1501",
			optionItem.contains("RankOrder") ? new Short("1") : new Short("0"));

	}


	/**
	 * 高校の表示順序をプロファイルにセットする
	 */
	public void setItem0402() {
		item.put("0402", new Short(form.getDispSchoolOrder()));
	}

	/**
	 * 高校の表示順序をプロファイルにセットする
	 * 校内成績分析・過回比較（成績概況）
	 * 過回・他校比較オプションが未チェックの場合は処理しない
	 */
	public void setS402Item0402() {
		if (optionItem.contains("CompOther"))
			item.put("0402", new Short(form.getDispSchoolOrder()));
	}

	/**
	 * 過年度の表示をプロファイルにセットする
	 */
	public void setItem1102() {
		item.put("1102", new Short(form.getDispPast()));
	}

	/**
	 * 過年度の表示をプロファイルにセットする
	 */
	public void setItem1102_2() {
		//if (form.getDispUnivYear() == null) {
		//	item.put("1102", new String[]{"1", "2"});
		//} else {
			item.put("1102", form.getDispUnivYear());
		//}
	}

	/**
	 * 校内成績分析・他校比較（偏差値分布）
	 * 過年度の表示をプロファイルにセットする
	 * 表が未チェックの場合は処理しない
	 */
	public void setS303Item1102() {
		if (formatItem.contains("Chart")) item.put("1102", new Short(form.getDispPast()));
	}

	/**
	 *
	 *  全体平均表示をプロファイルにセットする
	 *
	 * @return void
	 *
	 */
	public void setItem1511() {
		item.put("1511", new Short(form.getDispTotalAvg()));
	}


	/**
	 *
	 *  現役全体平均表示をプロファイルにセットする
	 *
	 * @return void
	 *
	 */
	public void setItem1512() {
		item.put("1512", new Short(form.getDispActiveAvg()));
	}


	/**
	 *
	 *  高卒全体平均表示をプロファイルにセットする
	 *
	 * @return void
	 *
	 */
	public void setItem1513() {
		item.put("1513", new Short(form.getDispGradAvg()));
	}


	/**
	 *
	 *  表示高校数をプロファイルにセットする
	 *
	 * @return void
	 *
	 */
	public void setItem1514() {
		item.put("1514", new Short(form.getDispSchoolNum()));
	}


	/**
	 *
	 *  上位校の*印表示をプロファイルにセットする
	 *
	 * @return void
	 *
	 */
	public void setItem1515() {
		item.put("1515", new Short(form.getDispUpperMark()));
	}


	/**
	 * 過回・過年度比較をセットする
	 */
	public void setItem1201() {
		item.put(
			IProfileItem.PAST_COMP_PREV,
			optionItem.contains("CompPrev") ? new Short("1") : new Short("0")
		);
	}

	/**
	 * 過回・クラス比較をセットする
	 */
	public void setItem1202() {
		item.put(
			IProfileItem.PAST_COMP_CLASS,
			optionItem.contains("CompClass") ? new Short("1") : new Short("0")
		);
	}

	/**
	 * 過回・他校比較をセットする
	 */
	public void setItem1203() {
		item.put(
			IProfileItem.PAST_COMP_OTHER,
			optionItem.contains("CompOther") ? new Short("1") : new Short("0")
		);
	}


}

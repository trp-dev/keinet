package jp.co.fj.keinavi.beans.individual;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.kawaijuku.judgement.beans.judge.Judgement;
import jp.co.fj.kawaijuku.judgement.beans.judge.JudgementExecutor;
import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.kawaijuku.judgement.data.Univ;
import jp.co.fj.keinavi.data.individual.UnivKeiNavi;

import com.fjh.beans.DefaultBean;

/**
 *
 * 判定処理実行マネージャ
 *
 * 2007.09.14	Yoshimoto KAWAI - TOTEC
 * 				新規作成
 *
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 *              センターリサーチ５段階評価対応
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 *
 */
public class JudgementManager extends DefaultBean {

    // 判定処理Bean
    private final JudgementExecutor judge = new JudgementExecutor();

    // 判定用成績データ
    private final Score score;

    // 「一般私大」「短大」判定用成績データ
    private final Score scoreForPrivate;

    // 判定結果リスト（複数判定の場合）
    private final List results = new ArrayList();

    // 判定結果（単体判定の場合）
    private Judgement result = null;

    // 判定対象大学リスト
    private List univList = null;

    // 判定対象大学
    private UnivKeiNavi univ = null;

    // 枝番リスト
    private ArrayList branch = null;

    /**
     * コンストラクタ
     *
     * @param pScoreBean 成績データ
     * @param pSwitchScore 「一般私大」「短大」判定においてセンタープレを優先利用するか
     * @throws Exception 成績初期化時の例外
     */
    public JudgementManager(final Score scoreBean, final boolean precedeCenterPre) throws Exception {

        score = scoreBean;

        // 対象模試がセンタープレで記述系の成績が存在する場合に限り、
        // 優先フラグが有効なら「一般私大」「短大」判定用成績を作る
        // 優先フラグを外部から与えるのは、センタープレが対象であっても
        // 2次判定に記述系を使わなければならない場合があるため
        // ⇒たとえば、個人手帳のスケジュール表
        if (precedeCenterPre && "04".equals(scoreBean.getTargetExam().getExamCd()) && scoreBean.getWrtn() != null) {
            scoreForPrivate = new Score();
            scoreForPrivate.setMale(score.isMale());
            scoreForPrivate.setTargetExam(score.getTargetExam());
            scoreForPrivate.setIndividualId(score.getIndividualId());
            scoreForPrivate.setMark(score.getMark());
            scoreForPrivate.setCExam(score.getCExam());
            scoreForPrivate.execute();
        } else {
            scoreForPrivate = null;
        }
    }

    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws Exception {
        // 単体実行
        if (univ != null) {
            //「一般私大」「短大」
            if (scoreForPrivate != null && isPrivateUniv(univ)) {
                judgeSingle(scoreForPrivate);
                // それ以外
            } else {
                judgeSingle(score);
            }

            // 複数実行
        } else {
            //「一般私大」「短大」だけで判定する場合
            if (scoreForPrivate != null) {
                // 一般私大、短大のリスト
                final List listPrivate = new ArrayList();
                // それ以外の大学リスト
                final List listOther = new ArrayList();

                for (final Iterator ite = univList.iterator(); ite.hasNext();) {
                    final UnivKeiNavi univ = (UnivKeiNavi) ite.next();
                    if (isPrivateUniv(univ)) {
                        listPrivate.add(univ);
                    } else {
                        listOther.add(univ);
                    }
                }

                // 一般私大、短大以外を判定
                judgeMulti(score, listOther);
                // 一般私大、短大
                judgeMulti(scoreForPrivate, listPrivate);

                // そのまま判定
            } else {
                judgeMulti(score, univList);
            }
        }

    }

    //「一般私大」「短大」判定
    private boolean isPrivateUniv(final UnivKeiNavi u) {
        return " ".equals(u.getSchedule());
    }

    // 判定ロジック呼び出し（単体実行）
    private void judgeSingle(final Score score) throws Exception {

        judge.setScore(score);
        judge.setUniv((Univ) univ);
        judge.setSBranch(branch);
        judge.execute();
        result = judge.getJudgement();
    }

    // 判定ロジック呼び出し（複数実行）
    private void judgeMulti(final Score score, final List list) throws Exception {

        if (!list.isEmpty()) {
            judge.setScore(score);
            judge.setUnivList(list);
            judge.execute();
            results.addAll(judge.getJudgements());
        }
    }

    /**
     * @return results
     */
    public List getResults() {
        return results;
    }

    /**
     * @param pBranch 設定する branch
     */
    public void setBranch(ArrayList pBranch) {
        branch = pBranch;
    }

    /**
     * @param pUniv 設定する univ
     */
    public void setUniv(UnivKeiNavi pUniv) {
        univ = pUniv;
    }

    /**
     * @param pUnivList 設定する univList
     */
    public void setUnivList(List pUnivList) {
        univList = pUnivList;
    }

    /**
     * @return result
     */
    public Judgement getResult() {
        return result;
    }

}

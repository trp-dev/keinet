package jp.co.fj.keinavi.beans.sheet.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.IExamData;
import jp.co.fj.keinavi.data.sheet.ExtI11ListBean;
import jp.co.fj.keinavi.excel.data.personal.I11KyokaHensaListBean;
import jp.co.fj.keinavi.excel.data.personal.I11KyokaSuiiListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.individual.ExamSorter;
import jp.co.fj.keinavi.util.sql.QueryLoader;

/**
 *
 * 個人成績分析−成績分析面談 教科別成績推移グラフ印刷用Bean
 *
 * 200X.XX.XX [新規作成]
 *
 *
 * @author Keisuke KONDO - TOTEC
 * @version 1.0
 *
 * [1]    2005.02.02 kondo      change 外部データ開放日が同一の場合、模試コード順にソートするよう修正
 * [2]    2005.01.21 kondo      change ２次 英語＋リスニング追加対応
 * [3]    2005.01.21 kondo      change 配点200点の科目が複数ある場合最初の1つをとるように修正
 * [4]    2005.04.14 kondo             面談シート４の時、面談シートフラグを設定するように修正
 * [5]    2005.04.14 kondo      対象模試が新テストだった場合、種別フラグに"1"を入れるように修正
 * [6]    2006.09.06 Totec)T.Yamada     受験学力測定対応 - 除外する
 *
 * 2009.11.25   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 *
 */
public class I12_01_3SheetBean extends IndAbstractSheetBean {

    private final Short menSheetFlg;

    /**
     * コンストラクタです。
     * @param menSheetFlg
     */
    public I12_01_3SheetBean(Short menSheetFlg) {
        super();
        this.menSheetFlg = menSheetFlg;
    }
    /**
     * コンストラクタです。
     */
    public I12_01_3SheetBean() {
        super();
        this.menSheetFlg = null;
    }

    /* (非 Javadoc)
     * @see com.fjh.beans.DefaultBean#execute()
     */

    public void execute() throws SQLException, Exception {

        final String ELSUBCD = "1190";//[1] add

        //セッション情報
        ICommonMap commonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);
        // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD START
        // 面談シートフラグ
        String flag = "0";
        // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD END

        //プロファイル関連
        int ExamType; //模試の種類
        int MultiExam; //１教科複数受験
        int savaMultiExam;
        int DispGrade; //表示する学年
        if(!"i003".equals(backward)){
            /**i103バランスチャートの場合**/

            //セキュリティスタンプ
            Short printStamp = (Short) profile.getItemMap(IProfileCategory.I_SCORE_BC).get(IProfileItem.PRINT_STAMP);
            int SecuStanp = Integer.parseInt(printStamp.toString());

            data.setIntKyoSheetFlg(1) ;															// 教科分析フラグ
            data.setIntKyoSheetSecuFlg(SecuStanp);												// 教科シートセキュリティスタンプ
            data.setIntKyoSheetShubetsuFlg(getNewTestFlg());//[5] add 新テストフラグ（教科シート）

            //模試の種類
            Short examType = (Short)profile.getItemMap(IProfileCategory.I_SCORE_TRANS).get(IProfileItem.EXAM_TYPE);
            ExamType = (Integer.parseInt(examType.toString()));

            //1教科複数受験
            Short indMultiExam = (Short)profile.getItemMap(IProfileCategory.I_SCORE_TRANS).get(IProfileItem.IND_MULTI_EXAM);
            savaMultiExam = Integer.parseInt(indMultiExam.toString());

            //表示する学年
            Short StudentDispGrade = (Short) profile.getItemMap(IProfileCategory.I_SCORE_TRANS).get(IProfileItem.STUDENT_DISP_GRADE);
            DispGrade = Integer.parseInt(StudentDispGrade.toString());

            // 2020/02/14 QQ)Ooseto 共通テスト対応 ADD START
            flag = "1";
            // 2020/02/14 QQ)Ooseto 共通テスト対応 ADD END
        }else{
            /**i003簡単印刷の場合**/
            //教科分析シートフラグ
            String []KyoSheetFlg = CollectionUtil.splitComma((String) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_FORMS));

            //面談シートフラグ
            Short MenSheet = menSheetFlg != null ? menSheetFlg : (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_SHEET);
            int MenSheetFlg = Integer.parseInt(MenSheet.toString());
            // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD START
            flag = MenSheet.toString();
            // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD END

            //セキュリティスタンプ
            Short printStamp = (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.PRINT_STAMP);
            int SecuStanp = Integer.parseInt(printStamp.toString());

            //教科分析シートが選択されたか、面談シートが選択されたのか
            if(Integer.parseInt(KyoSheetFlg[0]) == 1){
                data.setIntKyoSheetFlg(Integer.parseInt(KyoSheetFlg[0])) ;							// 教科分析フラグ
                data.setIntKyoSheetSecuFlg(SecuStanp);												// 教科分析シートセキュリティスタンプ
                data.setIntKyoSheetShubetsuFlg(getNewTestFlg());//[5] add 新テストフラグ（教科シート）
            }
            //if(MenSheetFlg == 2 || MenSheetFlg == 3){//[4] delete
            if(MenSheetFlg == 2 || MenSheetFlg == 3 || MenSheetFlg == 5){//[4] add
                data.setIntMenSheetFlg(MenSheetFlg);												// 面談シートフラグ
                data.setIntMenSheetSecuFlg(SecuStanp);												// 面談シートフラグセキュリティスタンプ
                data.setIntMenSheetShubetsuFlg(getNewTestFlg());//[5] add 新テストフラグ（面談シート）
            }
            //教科分析シートは受験学力測定テストNG
            if (Integer.parseInt(KyoSheetFlg[0]) == 1
                    && KNUtil.TYPECD_ABILITY.equals(commonMap.getTargetExamTypeCode())) {
                data.setIntKyoSheetFlg(0);
            }
            //面談シート１の制限
            if (MenSheetFlg == 5 && !isAvailableSheet1()) {
                data.setIntMenSheetFlg(0);
            }

            //模試の種類→「すべて」で固定
            ExamType = 1;

            //１教科複数受験→第1解答科目で固定
            savaMultiExam = 3;

            //表示する学年
            Short StudentDispGrade = (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.STUDENT_DISP_GRADE);
            DispGrade = Integer.parseInt(StudentDispGrade.toString());
        }

        //取れた模試を数える
        int examCount = 1;

        /*下記条件の時に値を格納
        ・面談シートフラグ=2or4
        ・教科分析シートフラグ=1*/

        if (data.getIntMenSheetFlg() == 2 || data.getIntMenSheetFlg() == 3 || data.getIntMenSheetFlg() == 5 || data.getIntKyoSheetFlg() == 1) {

            /*******項番２SQL＿選択生徒情報取得***********************************************************/

            for (java.util.Iterator it=data.getI11List().iterator(); it.hasNext();) {
                ExtI11ListBean printData = (ExtI11ListBean) it.next();

                // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD START
                // dataが処理中の帳票のデータでない場合(面談シートフラグが異なる場合)、処理しない
                if(!printData.getMenSheetFlg().equals(flag.toString())) continue;
                // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD END

                PreparedStatement pstmt = null;
                ResultSet rs = null;
                PreparedStatement pstmt1 = null;
                ResultSet rs1 = null;
                PreparedStatement pstmt2 = null;
                ResultSet rs2 = null;
                PreparedStatement pstmt3 = null;
                ResultSet rs3 = null;
                PreparedStatement pstmt4 = null;
                ResultSet rs4 = null;
                PreparedStatement pstmt5 = null;
                ResultSet rs5 = null;
                PreparedStatement pstmt6 = null;
                ResultSet rs6 = null;
                PreparedStatement pstmt7 = null;
                ResultSet rs7 = null;
                PreparedStatement pstmt8 = null;
                ResultSet rs8 = null;
                PreparedStatement pstmt9 = null;
                ResultSet rs9 = null;

                try{
                    //項番８SQL＿模試名称の取得
                    //画面からきたパラメータによってSQL条件を変える。
                    String sqlOption = "";

                    //ヘルプデスクから来た場合
                    if(login.isHelpDesk()){

                        sqlOption +=
                        "SELECT"+
                        " /*+ INDEX(SUB PK_SUBRECORD_I) */ "+//12/5 sql hint
                            " NVL(SUB.EXAMYEAR, NULL)"+
                            ",NVL(SUB.EXAMCD,NULL)"+
                            ",NVL(EX.EXAMNAME_ABBR,NULL)"+
                            ",NVL(EX.INPLEDATE,NULL)"+
                            ",NVL(EX.IN_DATAOPENDATE,NULL)"+
                            ", NVL(ex.examtypecd, null) examtypecd " +
                        " FROM"+
                            " SUBRECORD_I SUB"+
                            " INNER JOIN"+
                            " EXAMINATION EX"+
                            " ON SUB.EXAMYEAR = EX.EXAMYEAR"+
                            " AND SUB.EXAMCD = EX.EXAMCD"+
                            " AND EX.IN_DATAOPENDATE <= (SELECT"+
                                                            " EXAMINATION.IN_DATAOPENDATE"+
                                                            " FROM"+
                                                            " EXAMINATION"+
                                                            " WHERE EXAMYEAR = '" + commonMap.getTargetExamYear() + "'"+
                                                            " AND EXAMCD = '" + commonMap.getTargetExamCode() + "')";

                    }else{

                        sqlOption +=
                        "SELECT"+
                            " /*+ INDEX(SUB PK_SUBRECORD_I) */ "+//12/5 sql hint
                            " NVL(SUB.EXAMYEAR, NULL)"+
                            ",NVL(SUB.EXAMCD,NULL)"+
                            ",NVL(EX.EXAMNAME_ABBR,NULL)"+
                            ",NVL(EX.INPLEDATE,NULL)"+
                            ",NVL(EX.OUT_DATAOPENDATE,NULL)"+
                            ", NVL(ex.examtypecd, null) examtypecd " +
                        " FROM"+
                            " SUBRECORD_I SUB"+
                            " INNER JOIN"+
                            " EXAMINATION EX"+
                            " ON SUB.EXAMYEAR = EX.EXAMYEAR"+
                            " AND SUB.EXAMCD = EX.EXAMCD"+
                            " AND EX.OUT_DATAOPENDATE <= (SELECT"+
                                                            " EXAMINATION.OUT_DATAOPENDATE"+
                                                            " FROM"+
                                                            " EXAMINATION"+
                                                            " WHERE EXAMYEAR = '" + commonMap.getTargetExamYear() + "'"+
                                                            " AND EXAMCD = '" + commonMap.getTargetExamCode() + "')";
                        }

                        sqlOption += " AND SUB.INDIVIDUALID = '" + printData.getIndividualid() + "'";

                    //表示する学年が現学年のみ
                    if(DispGrade == 1){
                        sqlOption += " AND SUB.EXAMYEAR = '" + super.getThisYear() + "'";
                    }
                    //模試種類が同系統の場合
                    if(ExamType == 2){
                        sqlOption +=
                            " AND EX.EXAMTYPECD = (SELECT" +
                                                            " EXAMINATION.EXAMTYPECD"+
                                                            " FROM"+
                                                            " EXAMINATION"+
                                                            " WHERE EXAMYEAR = '" + commonMap.getTargetExamYear() + "'"+
                                                            " AND EXAMCD = '" + commonMap.getTargetExamCode() + "')";
                    }

                    if(login.isHelpDesk()){
                        sqlOption +=
                            " GROUP BY SUB.EXAMYEAR, SUB.EXAMCD, EX.EXAMNAME_ABBR, EX.INPLEDATE ,EX.IN_DATAOPENDATE"+
                            " ,ex.examtypecd" +
                            //" ORDER BY EX.IN_DATAOPENDATE DESC";//[1] delete
                            " ORDER BY EX.IN_DATAOPENDATE DESC, SUB.EXAMCD DESC";//[1] add
                    }else{
                        sqlOption +=
                            " GROUP BY SUB.EXAMYEAR, SUB.EXAMCD, EX.EXAMNAME_ABBR, EX.INPLEDATE ,EX.OUT_DATAOPENDATE"+
                            " ,ex.examtypecd" +
                            //" ORDER BY EX.OUT_DATAOPENDATE DESC";//[1] delete
                            " ORDER BY EX.OUT_DATAOPENDATE DESC, SUB.EXAMCD DESC";//[1] add
                    }

                    //模試数を初期化
                    examCount = 1;

                    pstmt = conn.prepareStatement(sqlOption.toString());
                    rs = pstmt.executeQuery();

                    ExaminationData examData = null;
                    ArrayList examStatus = new ArrayList();

                    boolean ans1st = false;

                    while(rs.next()){

                        //受験学力テスト
                        if (KNUtil.TYPECD_ABILITY.equals(rs.getString("examtypecd"))) {
                            continue;
                        }

                        ExaminationData exdata = new ExaminationData();
                        exdata.setExamYear(rs.getString(1));
                        exdata.setExamCd(rs.getString(2));
                        exdata.setExamAddrName(rs.getString(3));
                        exdata.setInpleDate(rs.getString(4));
                        if(login.isHelpDesk()){
                            exdata.setInDataOpenDate(rs.getString(5));
                        }else{
                            exdata.setOutDataOpenData(rs.getString(5));
                        }
                        examStatus.add(exdata);

                        // 対象模試＋比較対象模試に第1解答科目対応模試あり
                        if (KNUtil.isAns1st(exdata.getExamCd())) {
                            ans1st = true;
                        }

                        if(examCount == 7){
                            //過去模試は７つまで
                            break;
                        }
                        examCount ++;
                    }
                    if(pstmt != null) pstmt.close();
                    if(rs != null) rs.close();

                    //１教科複数受験
                    // 第1解答科目を表示で、対象模試と比較対象模試が第1解答科目対応模試以外→ 平均を表示
                    if(!ans1st && savaMultiExam == 3){
                        MultiExam = 1;
                    }else {
                        MultiExam = savaMultiExam;
                    }

                    if(examStatus != null && examStatus.size() > 0){

                        if(login.isHelpDesk()){
                            //Collections.sort(examStatus, new ExamSorter().new SortByInDataOpenDate2());//[1] delete
                            Collections.sort(examStatus, new ExamSorter().new SortByInDataOpenDate3());//[1] add
                            Collections.reverse(examStatus);//[1] add
                        }else{
                            //Collections.sort(examStatus, new ExamSorter().new SortByOutDataOpenDate2());//[1] delete
                            Collections.sort(examStatus, new ExamSorter().new SortByOutDataOpenDate3());//[1] add
                            Collections.reverse(examStatus);//[1] add
                        }

                        for(int i = 0; i < examStatus.size(); i++){
                            examData = (ExaminationData)examStatus.get(i);

                        I11KyokaSuiiListBean bean = new I11KyokaSuiiListBean();
                        printData.getI11KyokaSuiiList().add(bean);

                        bean.setStrMshmei(examData.getExamAddrName());	//模試短縮名
                        bean.setStrMshDate(examData.getInpleDate());	//模試実施基準日
                        bean.setIntKyokaDispFlg(MultiExam);

                        /**総合の偏差値取得***********************************************************************/
                        pstmt2 = conn.prepareStatement(QueryLoader.getInstance().load("i12_sogo_score").toString());
                        pstmt2.setString(1, printData.getIndividualid());			//項番2で出たID
                        pstmt2.setString(2, examData.getExamYear());			    //項番6で出た年度
                        pstmt2.setString(3, examData.getExamCd());					//項番6で出た模試コード
                        pstmt2.setString(4, examData.getExamYear());			    //項番6で出た年度
                        rs2 = pstmt2.executeQuery();

                        ArrayList KyokaData = new ArrayList();
                        I11KyokaHensaListBean Hensa = new I11KyokaHensaListBean();	//教科名・偏差値リスト
                        while(rs2.next()){
                            Hensa.setStrKyokamei(rs2.getString(1));	//教科名
                            Hensa.setFloHensa(rs2.getFloat(2));		//偏差値
                            choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                    rs2.getFloat(2), rs2.getString("ACADEMICLEVEL"));
                            break;//総合は何件かあるが、表示順で並べた先頭の物を表示する。
                        }
                        KyokaData.add(Hensa);
                        if(pstmt2 != null) pstmt2.close();
                        if(rs2 != null) rs2.close();

                        /**英語の偏差値取得(科目配点が200の物)************************************************************/
                        boolean eng_count = false;
                        pstmt3 = conn.prepareStatement(QueryLoader.getInstance().load("i12_english_score1").toString());
                        pstmt3.setString(1, printData.getIndividualid());			//項番2で出たID
                        pstmt3.setString(2, examData.getExamYear());			    //項番6で出た年度
                        pstmt3.setString(3, examData.getExamCd());					//項番6で出た模試コード
                        pstmt3.setString(4, examData.getExamYear());			    //項番6で出た年度

                        rs3 = pstmt3.executeQuery();

                        Hensa = new I11KyokaHensaListBean();
                        while(rs3.next()){
                            //[2] change
                            //偏差値が-999だったら追加しない
                            if(rs3.getFloat(2) != -999.0){
                                //英語＋リスニングなら優先して、入れる
                                if(rs3.getString("SUBCD").equals(ELSUBCD)) {
                                    Hensa.setStrKyokamei(rs3.getString(1));	//教科名
                                    Hensa.setFloHensa(rs3.getFloat(2));		//偏差値
                                    choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                            rs3.getFloat(2), rs3.getString("ACADEMICLEVEL"));
                                } else if(!eng_count) {
                                    //それ以外は、最初の一つを入れる
                                    Hensa.setStrKyokamei(rs3.getString(1));	//教科名
                                    Hensa.setFloHensa(rs3.getFloat(2));		//偏差値
                                    choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                            rs3.getFloat(2), rs3.getString("ACADEMICLEVEL"));
                                }
                                eng_count = true;//確認フラグ
                            }
                        }

                        //[2] add
                        //確認フラグがオンなら、追加
                        if(eng_count){
                            KyokaData.add(Hensa);
                        }

                        if(pstmt3 != null) pstmt3.close();
                        if(rs3 != null) rs3.close();

                        /**英語の偏差値取得(↑の条件で一軒もヒットしなかったらこっち)*****************************************/
                        if(eng_count == false){
                            pstmt9 = conn.prepareStatement(QueryLoader.getInstance().load("i12_english_score2").toString());
                            pstmt9.setString(1, printData.getIndividualid());			//項番2で出たID
                            pstmt9.setString(2, examData.getExamYear());			    //項番6で出た年度
                            pstmt9.setString(3, examData.getExamCd());					//項番6で出た模試コード
                            pstmt9.setString(4, examData.getExamYear());			    //項番6で出た年度

                            rs9 = pstmt9.executeQuery();

                            while(rs9.next()){
                                Hensa.setStrKyokamei(rs9.getString(1));	//教科名
                                Hensa.setFloHensa(rs9.getFloat(2));		//偏差値
                                choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                        rs9.getFloat(2), rs9.getString("ACADEMICLEVEL"));
                                break;									//表示順序の一番目
                            }
                            KyokaData.add(Hensa);
                            if(pstmt9 != null) pstmt9.close();
                            if(rs9 != null) rs9.close();

                        }
                        /**数学の偏差値取得(科目配点が200の物)***********************************************************************/
                        boolean math_count = false;	//数学の検索条件

                        pstmt4 = conn.prepareStatement(QueryLoader.getInstance().load("i12_math_score1").toString());
                        pstmt4.setString(1, printData.getIndividualid());			//項番2で出たID
                        pstmt4.setString(2, examData.getExamYear());			    //項番6で出た年度
                        pstmt4.setString(3, examData.getExamCd());					//項番6で出た模試コード
                        pstmt4.setString(4, examData.getExamYear());			    //項番6で出た年度
                        rs4 = pstmt4.executeQuery();

                        Hensa = new I11KyokaHensaListBean();
                        while(rs4.next()){
                            //[3] change
                            if(rs4.getFloat(2) != -999.0){
                                if(math_count == false) {
                                    Hensa.setStrKyokamei(rs4.getString(1));	//教科名
                                    Hensa.setFloHensa(rs4.getFloat(2));		//偏差値
                                    choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                            rs4.getFloat(2), rs4.getString("ACADEMICLEVEL"));
                                    KyokaData.add(Hensa);
                                }
                                math_count = true;		//確認フラグ
                            }
                        }
                        if(pstmt4 != null) pstmt4.close();
                        if(rs4 != null) rs4.close();


                        /**数学の偏差値取得(↑の条件で一軒もヒットしなかったらこっち)********************************************/
                        if(math_count == false){
                            pstmt5 = conn.prepareStatement(QueryLoader.getInstance().load("i12_math_score2").toString());
                            pstmt5.setString(1, printData.getIndividualid());			//項番2で出たID
                            pstmt5.setString(2, examData.getExamYear());			    //項番6で出た年度
                            pstmt5.setString(3, examData.getExamCd());					//項番6で出た模試コード
                            pstmt5.setString(4, examData.getExamYear());			    //項番6で出た年度
                            rs5 = pstmt5.executeQuery();

                            while(rs5.next()){
                                Hensa.setStrKyokamei(rs5.getString(1));	//教科名
                                Hensa.setFloHensa(rs5.getFloat(2));		//偏差値
                                choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                        rs5.getFloat(2), rs5.getString("ACADEMICLEVEL"));
                                break;									//表示順序の一番目
                            }
                            //教科名・偏差値リスト
                            KyokaData.add(Hensa);

                            if(pstmt5 != null) pstmt5.close();
                            if(rs5 != null) rs5.close();
                        }

                        /**国語の偏差値取得(科目配点が200の物)***********************************************************************/
                        boolean japan_count = false;

                        pstmt8 = conn.prepareStatement(QueryLoader.getInstance().load("i12_japanLang_score1").toString());
                        pstmt8.setString(1, printData.getIndividualid());			//項番2で出たID
                        pstmt8.setString(2, examData.getExamYear());			    //項番6で出た年度
                        pstmt8.setString(3, examData.getExamCd());					//項番6で出た模試コード
                        pstmt8.setString(4, examData.getExamYear());			    //項番6で出た年度
                        rs8 = pstmt8.executeQuery();

                        Hensa = new I11KyokaHensaListBean();
                        while(rs8.next()){
                            //[2] change
                            if(rs8.getFloat(2) != -999.0){
                                if(japan_count == false) {
                                    Hensa.setStrKyokamei(rs8.getString(1));	//教科名
                                    Hensa.setFloHensa(rs8.getFloat(2));		//偏差値
                                    choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                            rs8.getFloat(2), rs8.getString("ACADEMICLEVEL"));
                                    KyokaData.add(Hensa);
                                }
                                japan_count = true;		//確認フラグ
                            }
                        }
                        if(pstmt8 != null) pstmt8.close();
                        if(rs8 != null) rs8.close();

                        /**国語の偏差値取得(↑の条件で一軒もヒットしなかったらこっち)***********************************************/
                        if(japan_count == false){
                            pstmt6 = conn.prepareStatement(QueryLoader.getInstance().load("i12_japanLang_score2").toString());
                            pstmt6.setString(1, printData.getIndividualid());				//項番2で出たID
                            pstmt6.setString(2, examData.getExamYear());			    //項番6で出た年度
                            pstmt6.setString(3, examData.getExamCd());					//項番6で出た模試コード
                            pstmt6.setString(4, examData.getExamYear());			    //項番6で出た年度
                            rs6 = pstmt6.executeQuery();

                            while(rs6.next()){
                                Hensa.setStrKyokamei(rs6.getString(1));	//教科名
                                Hensa.setFloHensa(rs6.getFloat(2));		//偏差値
                                choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                        rs6.getFloat(2), rs6.getString("ACADEMICLEVEL"));
                                break;									//表示順序の一番目
                            }
                            KyokaData.add(Hensa);
                            if(pstmt6 != null) pstmt6.close();
                            if(rs6 != null) rs6.close();
                        }

                        /**理科・地歴公民の偏差値取得*******************************************************************************/
                        //1教科複数受験　平均1or良い方2or第1解答科目3//プロファイルから1教科複数受験取得

                        if(MultiExam == 3){
                            if(KNUtil.isAns1st(examData.getExamCd())){
                                //第1解答科目対応模試
                                pstmt7 = conn.prepareStatement(QueryLoader.getInstance().load("i12_sm_ans1st_score").toString());
                                pstmt7.setString(1, printData.getIndividualid());			//項番2で出たID
                                pstmt7.setString(2, examData.getExamYear());			    //項番6で出た年度
                                pstmt7.setString(3, examData.getExamCd());					//項番6で出た模試コード
                                pstmt7.setString(4, printData.getIndividualid());			//項番2で出たID
                                pstmt7.setString(5, examData.getExamYear());			    //項番6で出た年度
                                pstmt7.setString(6, examData.getExamCd());					//項番6で出た模試コード
                                pstmt7.setString(7, examData.getExamYear());				//項番6で出た年度
                            }else{
                                //平均
                                pstmt7 = conn.prepareStatement(QueryLoader.getInstance().load("i12_sm_avg_score").toString());
                                pstmt7.setString(1, printData.getIndividualid());				//項番2で出たID
                                pstmt7.setString(2, examData.getExamYear());			    //項番6で出た年度
                                pstmt7.setString(3, examData.getExamCd());					//項番6で出た模試コード
                                pstmt7.setString(4, examData.getExamYear());
                            }
                        }else if(MultiExam == 1){
                            //平均ならこのSQL
                            pstmt7 = conn.prepareStatement(QueryLoader.getInstance().load("i12_sm_avg_score").toString());
                            pstmt7.setString(1, printData.getIndividualid());				//項番2で出たID
                            pstmt7.setString(2, examData.getExamYear());			    //項番6で出た年度
                            pstmt7.setString(3, examData.getExamCd());					//項番6で出た模試コード
                            pstmt7.setString(4, examData.getExamYear());			    //項番6で出た年度
                        }else{
                            //良い方ならこのSQL
                            pstmt7 = conn.prepareStatement(QueryLoader.getInstance().load("i12_sm_max_score").toString());
                            pstmt7.setString(1, printData.getIndividualid());				//項番2で出たID
                            pstmt7.setString(2, examData.getExamYear());			    //項番6で出た年度
                            pstmt7.setString(3, examData.getExamCd());					//項番6で出た模試コード
                            pstmt7.setString(4, examData.getExamYear());			    //項番6で出た年度
                        }

                        rs7 = pstmt7.executeQuery();

                        //理科で一番点数の良い科目コードをだす。
                        String s_subname = "";
                        float s_subscor = -999;
                        String s_sublevel = "";
                        int s_ans1st=0;
                        //地歴公民で一番点数の良い科目コードをだす。
                        String t_subname = "";
                        float t_subscor = -999;
                        String t_sublevel = "";
                        int t_ans1st=0;

                        while(rs7.next()){
                            if ("4000".equals(rs7.getString(1))){
                                if (s_subscor < rs7.getFloat(4)){
                                    s_subname = rs7.getString(2);
                                    s_subscor = rs7.getFloat(4);
                                    s_sublevel = rs7.getString(5);
                                    s_ans1st = rs7.getInt(6);
                                }
                            }else{
                                if (t_subscor < rs7.getFloat(4)){
                                    t_subname = rs7.getString(2);
                                    t_subscor = rs7.getFloat(4);
                                    t_sublevel = rs7.getString(5);
                                    t_ans1st = rs7.getInt(6);
                                }
                            }
                        }

                        // 理科
                        Hensa = new I11KyokaHensaListBean();
                        Hensa.setStrKyokamei(s_subname);	//教科名
                        Hensa.setFloHensa(s_subscor);		//偏差値
                        //学力レベル
                        if (MultiExam == 3){
                            if (s_ans1st == 1){
                                choiceScholarLevel(Hensa, 2, s_subscor, s_sublevel);	//第1解答科目
                            }else{
                                choiceScholarLevel(Hensa, 1, s_subscor, s_sublevel);	//平均
                            }
                        }else{
                            choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(), s_subscor, s_sublevel);	//平均or良い方
                        }
                        KyokaData.add(Hensa);

                        //地歴公民
                        Hensa = new I11KyokaHensaListBean();
                        Hensa.setStrKyokamei(t_subname);	//教科名
                        Hensa.setFloHensa(t_subscor);		//偏差値
                        //学力レベル
                        if (MultiExam == 3){
                            if (t_ans1st == 1){
                                choiceScholarLevel(Hensa, 2, t_subscor, t_sublevel);	//第1解答科目
                            }else{
                                choiceScholarLevel(Hensa, 1, t_subscor, t_sublevel);	//平均
                            }
                        }else{
                            choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(), t_subscor, t_sublevel);	//平均or良い方
                        }
                        KyokaData.add(Hensa);

                        if(pstmt7 != null) pstmt7.close();
                        if(rs7 != null) rs7.close();

                        bean.setI11KyokaHensaList(KyokaData);

                        }
                    }//模試

                }finally{
                    if(pstmt != null) pstmt.close();
                    if(rs != null) rs.close();
                    if(pstmt1 != null) pstmt1.close();
                    if(rs1 != null) rs1.close();
                    if(pstmt2 != null) pstmt2.close();
                    if(rs2 != null) rs2.close();
                    if(pstmt3 != null) pstmt3.close();
                    if(rs3 != null) rs3.close();
                    if(pstmt4 != null) pstmt4.close();
                    if(rs4 != null) rs4.close();
                    if(pstmt5 != null) pstmt5.close();
                    if(rs5 != null) rs5.close();
                    if(pstmt6 != null) pstmt6.close();
                    if(rs6 != null) rs6.close();
                    if(pstmt7 != null) pstmt7.close();
                    if(rs7 != null) rs7.close();
                    if(pstmt8 != null) pstmt8.close();
                    if(rs8 != null) rs8.close();
                    if(pstmt9 != null) pstmt9.close();
                    if(rs9 != null) rs9.close();
                }

            }//個人情報

            /******************************************************************************************/

    }//flag条件

}

    /* (非 Javadoc)
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
     */
    protected String getCategory() {
        return IProfileCategory.I_SCORE_TRANS;
    }

    /**
     * 画面の選択状態（平均値／良い方）で、出力する学力レベルを選択する。
     * @param hensa
     * @param flg
     * @param avg
     * @param lvl
     */
    private void choiceScholarLevel(I11KyokaHensaListBean hensa,
            final int flg, final float avg, final String lvl) {

        // 	平均 or 第1解等科目
        //  第1解等科目で理科・地歴公民以外の場合は平均とする
        if (flg == 1 || flg == 3) {
            //平均値の場合、偏差値から学力レベルを出力
            hensa.setStrScholarLevel(IExamData.makeScholarLevel(""+avg));
        }
        else {
            //良い方の場合、SQLから取得した学力レベルを出力
            hensa.setStrScholarLevel(GeneralUtil.toBlank(lvl));
        }

    }


}

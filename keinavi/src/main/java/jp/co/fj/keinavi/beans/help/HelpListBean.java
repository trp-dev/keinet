/*
 * 作成日: 2004/09/09
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.help;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import com.fjh.beans.DefaultBean;

import jp.co.fj.keinavi.data.help.HelpList;
import jp.co.fj.keinavi.util.KNCommonProperty;


/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class HelpListBean extends DefaultBean {

    private String formId     = ""; 		// フォームＩＤ
    private String freeWord   = ""; 		// フリーワード
    private String useful     = "";		// 役立ち度
    private String comment    = "";		// コメント
    private String name       = "";		// 名前
    private String detailBack = "";		// 詳細表示("h003")からの戻り先
    private String page       = "1";		// ページ

                                            // ヘルプマスタデータ
    private String helpId     = null; 	//   ヘルプＩＤ
    private String titleStr   = null; 	//   タイトル文字列
    private String category   = null; 	//   カテゴリ
    private String dispSequence = null;	//   表示順
    private String explan     = null; 	//   説明文
    private String update     = null; 	//   更新日時

    // ヘルプリスト
    private List helpLists = new ArrayList();

    private String SQL_BASE = "";


    /* (非 Javadoc)
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws SQLException, Exception {
        // TODO 自動生成されたメソッド・スタブ

        if ( "h001".equals(getFormId())) {
            // ヘルプトップリスト
            SQL_BASE = "SELECT  MAX(HELPID)       AS H_HELPID       " +
                       "       ,MAX(TITLEMEI)     AS H_TITLEMEI     " +
                       "       ,CATEGORY          AS H_CATEGORY     " +
                       "       ,MAX(DISPSEQUENCE) AS H_DISPSEQUENCE " +
                       "       ,MAX(RENEWALDATE)  AS H_RENEWALDATE  " +
                     //[2004/11/02 nino] TEXTがLONG型のためエラーとなる（ダミー扱いとする）
                     //"       ,MAX(TEXT)         AS H_TEXT         " +
                       "       ,'TEXT'            AS H_TEXT         " +
                       "  FROM  HELP               " +
                       "  GROUP BY CATEGORY        " +
                       // ------------------------------------------------------------
                       // 2004..
                       // Yoshimoto KAWAI - Totec
                       // ソートはカテゴリの名前順
                       //"  ORDER BY H_RENEWALDATE DESC" ;
                       "  ORDER BY CATEGORY ASC" ;
                       // ------------------------------------------------------------
            PreparedStatement ps = conn.prepareStatement(SQL_BASE);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HelpList helpList = new HelpList();
                helpList.setHelpId(rs.getString(1));
                helpList.setTitleStr(rs.getString(2));
                helpList.setCategory(rs.getString(3));
                helpList.setDispSequence(rs.getString(4));
                helpList.setUpdate(rs.getString(5));
                helpList.setExplan(rs.getString(6));
                helpLists.add(helpList);
            }
            rs.close();
            ps.close();
        }else if( "h002".equals(getFormId())) {
            // カテゴリー別一覧リスト
            SQL_BASE = "SELECT  *           " +
                       "  FROM  HELP        " +
                       "  WHERE CATEGORY =  ?  " +
                       "  ORDER BY DISPSEQUENCE ASC, RENEWALDATE DESC" ;
            PreparedStatement ps = conn.prepareStatement(SQL_BASE);
            ps.setString(1, category);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HelpList helpList = new HelpList();
                helpList.setHelpId(rs.getString(1));
                helpList.setTitleStr(rs.getString(2));
                helpList.setCategory(rs.getString(3));
                helpList.setDispSequence(rs.getString(4));
                helpList.setUpdate(rs.getString(5));
                helpList.setExplan(rs.getString(6));
                helpLists.add(helpList);
            }
            rs.close();
            ps.close();
        }else if( "h003".equals(getFormId()) || "h003p".equals(getFormId())) {
            // ヘルプ詳細
            SQL_BASE = "SELECT  *           " +
                       "  FROM  HELP        " +
                       "  WHERE HELPID = ? ";
            PreparedStatement ps = conn.prepareStatement(SQL_BASE);
            ps.setString(1, helpId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                // 各々の文字列変数に格納
                setHelpId(rs.getString(1));
                setTitleStr(rs.getString(2));
                setCategory(rs.getString(3));
                setDispSequence(rs.getString(4));
                setUpdate( getDispDate(rs.getString(5)) );
                setExplan(rs.getString(6));
            }
            rs.close();
            ps.close();
        }else if( "h004".equals(getFormId())) {
            // キーワード一覧リスト
            SQL_BASE = "SELECT  *           " +
                       "  FROM  HELP        " +
                       "  WHERE TITLEMEI LIKE '%" + getFreeWord() + "%'" +
                       "  ORDER BY H_RENEWALDATE DESC" ;
            PreparedStatement ps = conn.prepareStatement(SQL_BASE);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HelpList helpList = new HelpList();
                helpList.setHelpId(rs.getString(1));
                helpList.setTitleStr(rs.getString(2));
                helpList.setCategory(rs.getString(3));
                helpList.setDispSequence(rs.getString(4));
                helpList.setUpdate(rs.getString(5));
                helpList.setExplan(rs.getString(6));
                helpLists.add(helpList);
            }
            rs.close();
            ps.close();

        }else if( "h005".equals(getFormId())) {
            // 送信完了
        }


    }

    /**
     * ヘルプ日付がＮＥＷであるかをチェック
     *
     ** @return  Newチェック情報
     *               true :Newである
     *               false:Newでない
     */
    public boolean chkNewHelpLimit(String renewal) {
        boolean chkFlag = false;
        int  newHelpLimit = 0;
        String strDate = "";

        if (renewal == null || renewal.equals("")) {
            chkFlag = false;
            return chkFlag;
        }

        // NEW 表示制限日数を取得
        try {
            newHelpLimit = KNCommonProperty.getNewHelpLimit();
        } catch (Exception e) {
        }

        // NEW 表示制限日付けを取得
        Date date = new Date();
        long time = (long) (24*60*60)*1000*newHelpLimit;
        Date date2 = new Date(date.getTime() - time );
        GregorianCalendar cal2 = new GregorianCalendar(Locale.JAPAN);
        cal2.setTime(date2);
        int year = cal2.get(Calendar.YEAR);
        int month = cal2.get(Calendar.MONTH) + 1;
        int mday = cal2.get(Calendar.DAY_OF_MONTH);
        String s = "";
        s = "0000" + String.valueOf(year);
        s = s.substring(s.length()-4, s.length());
        strDate += s;
        s = "00" + String.valueOf(month);
        s = s.substring(s.length()-2, s.length());
        strDate += s;
        s = "00" + String.valueOf(mday);
        s = s.substring(s.length()-2, s.length());
        strDate += s;

        // 更新日付≧NEW 表示制限日付のチェック
        if ( Long.parseLong(renewal) >= Long.parseLong(strDate)) {
            chkFlag = true;
        } else {
            chkFlag = false;
        }

        return chkFlag;
    }

    /**
     * 日付（yyyy/mm/dd)を取得
     * 	 * @param string
     */
    public String getDispDate(String date) {
        if (date == null || date.equals("")) {
            return "";
        } else {
            return date.substring(0,4) + "/" + date.substring(4,6) + "/" + date.substring(6,8);
        }
    }

    /**
     * ヘルプリストサイズ
     * @return
     */
    public int getHelpListSize() {
        return helpLists.size();
    }
    /**
     * ヘルプリスト
     * 	 * @return
     */
    public List getHelpLists() {
        return helpLists;
    }

    /**
      * フォームＩＤ
    * @return
    */
    public String getFormId() {
        return formId;
    }
    /**
     * @param string
     */
    public void setFormId(String string) {
        formId = string;
    }

    /**
    * フリーワード
    * @return
    */
    public String getFreeWord() {
        return freeWord;
    }

    /**
     * @param string
     */
    public void setFreeWord(String string) {
        freeWord = string;
    }

    /** -----------------------------------------------*/
    /**
     * @return
     */
    public String getHelpId() {
        return helpId;
    }

    /**
     * @return
     */
    public String getTitleStr() {
        return titleStr;
    }

    /**
     * @return
     */
    public String getCategory() {
        return category;
    }

    /**
     * @return
     */
    public String getDispSequence() {
        return dispSequence;
    }

    /**
     * @return
     */
    public String getExplan() {
        return explan;
    }

    /**
     * @return
     */
    public String getUpdate() {
        return update;
    }

    /**
     * @param string
     */
    public void setHelpId(String string) {
        helpId = string;
    }

    /**
     * @param string
     */
    public void setTitleStr(String string) {
        titleStr = string;
    }

    /**
     * @param string
     */
    public void setCategory(String string) {
        category = string;
    }

    /**
     * @param string
     */
    public void setDispSequence(String string) {
        dispSequence = string;
    }

    /**
     * @param string
     */
    public void setExplan(String string) {
        explan = string;
    }

    /**
     * @param string
     */
    public void setUpdate(String string) {
        update = string;
    }


    //-------------------------------------------------
    /**
     * @return
     */
    public String getUseful() {
        return useful;
    }

    /**
     * @param string
     */
    public void setUseful(String string) {
        useful = string;
    }

    /**
     * @return
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param string
     */
    public void setComment(String string) {
        comment = string;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @param string
     */
    public void setName(String string) {
        name = string;
    }

    /**
     * @return
     */
    public String getDetailBack() {
        return detailBack;
    }

    /**
     * @param string
     */
    public void setDetailBack(String string) {
        detailBack = string;
    }

    /**
     * @return
     */
    public String getPage() {
        return page;
    }

    /**
     * @param string
     */
    public void setPage(String string) {
        page = string;
    }

}

package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import jp.co.fj.keinavi.data.maintenance.KNStudentData;
import jp.co.fj.keinavi.data.maintenance.KNStudentExtData;
import jp.co.fj.keinavi.data.maintenance.TempExamineeData;
import jp.co.fj.keinavi.util.JpnStringConv;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 生徒リストBean
 * 
 * 2006.09.28	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class StudentListBean extends DefaultBean {

	// 現年度
	private final int currentYear = Integer.parseInt(KNUtil.getCurrentYear());
	// 学校コード
	private final String schoolCd;
	
	// 対象年度
	private String year;
	// 対象年度と現年度との差
	private int yearDiff;
	// 学年
	private int grade = -1;
	// クラス
	private String className;
	// 個人ID
	private String individualId;
	// 絞り込み条件
	private String conditionSelect;
	// 絞り込み条件（テキスト）
	private String conditionText;
	// 絞り込み条件（サブ条件）
	private String conditionSubSelect;
	// 非表示生徒のみを取得するか
	private boolean isNotDisplayMode = false;
	
	// 生徒情報データのリスト
	private final List studentList = new ArrayList();
	// 表示する生徒情報データのリスト
	private final List dispStudentList = new ArrayList();
	
	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 学校コード
	 */
	public StudentListBean(final String pSchoolCd) {
		if (pSchoolCd == null) {
			throw new IllegalArgumentException(
					"学校コードがNULLです。");
		} else {
			this.schoolCd = pSchoolCd;
		}
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		// 対象年度は必須
		if (year != null) {
			studentList.clear();
			dispStudentList.clear();
			createStudentList();
		}
	}
	
	// 生徒リストを作る
	private void createStudentList() throws Exception {
		
		// あいまい検索かどうか
		final boolean isFuzzy = "1".equals(
				conditionSelect) && "4".equals(conditionSubSelect);
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = createPreparedStatement();
			ps.setFetchSize(1000);
			rs = ps.executeQuery();
			KNStudentExtData data = null;
			while (rs.next()) {
				if (data == null
						|| !rs.getString(1).equals(data.getIndividualId())) {
					data = (KNStudentExtData) createKSStudentData(rs);
					if (!isFuzzy || fuzzyMatch(data.getNameKana())) {
						studentList.add(data);
					}
				}
				data.addTakenExam(
							rs.getString(20), rs.getString(21));
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
		
	// あいまい検索マッチング処理
	private boolean fuzzyMatch(final String pNameKana) {
		
		// 変換
		final String nameKana = toSearchStr(pNameKana);
		
		// Nullは非マッチ
		if (pNameKana == null) {
			return false;
		}

		// 先頭から見ていく
		char[] c = conditionText.toCharArray();
		for (int i = 0; i < c.length && i < nameKana.length(); i++) {
			if (nameKana.charAt(i) != c[i] && nameKana.charAt(i) != '.') {
				return false;
			}
		}
		
		return true;
	}

	// SQLを作る
	private String createQuery() throws SQLException {
		
		final Query query = QueryLoader.getInstance().load("m22");

		// 個人ID
		if (isIndividualIdMode()) {
			query.append("AND bi.individualid = ? ");
		// 個人ID指定がなければ
		// 対象年度の履歴がある生徒のみ
		} else {
			query.append("AND " + getGradeColumn() + " IS NOT NULL ");
		}
		
		// 学年
		if (isGradeMode()) {
			query.append("AND " + getGradeColumn() + " = ? ");
		}

		// クラス
		if (isClassNameMode()) {
			query.append("AND " + getClassColumn() + " = ? ");
		}
		
		// かな氏名絞り込み
		if (isKanaNarrow()) {
			//「と一致」
			if ("1".equals(conditionSubSelect)) {
				query.append("AND TO_SEARCHSTR(bi.name_kana) = ? ");
			//「から始まる」
			} else if ("2".equals(conditionSubSelect)) {
				query.append("AND TO_SEARCHSTR(bi.name_kana) LIKE ? || '%' ");
			//「を含む」
			} else if ("3".equals(conditionSubSelect)) {
				query.append("AND TO_SEARCHSTR(bi.name_kana) LIKE '%' || ? || '%' ");
			}
		}
		
		// 非表示生徒
		if (isNotDisplayMode) {
			query.append("AND bi.notdisplayflg IS NOT NULL ");
		} else {
			query.append("AND bi.notdisplayflg IS NULL ");
		}

		// ORDER BY
		query.append("ORDER BY individualid");
		
		return query.toString();
	}

	// 学年の条件に使う列
	// ※対象年度によって異なる
	private String getGradeColumn() {
		return "h" + (3 - yearDiff) + ".grade"; 
	}
	
	// クラスの条件に使う列
	// ※対象年度によって異なる
	private String getClassColumn() {
		return "h" + (3 - yearDiff) + ".class"; 
	}
	
	/**
	 * @return PreparedStatement
	 */
	private PreparedStatement createPreparedStatement() throws Exception {
		
		final PreparedStatement ps = conn.prepareStatement(createQuery());

		int index = 1;
		
		// 前々年度
		ps.setString(index++, String.valueOf(currentYear - 2));
		// 前年度
		ps.setString(index++, String.valueOf(currentYear - 1));
		// 今年度
		ps.setString(index++, String.valueOf(currentYear));
		// 学校コード
		ps.setString(index++, schoolCd);
		
		// 学年
		if (isGradeMode()) {
			ps.setInt(index++, grade);
		}
		
		// クラス
		if (isClassNameMode()) {
			ps.setString(index++, className);
		}
		
		// 個人ID
		if (isIndividualIdMode()) {
			ps.setString(index++, individualId);
		}
		
		// かな氏名絞り込み
		if (isKanaNarrow()) {
			ps.setString(index++, conditionText);
		}
		
		return ps;
	}
	
	// 学年を条件に利用するかどうか
	private boolean isGradeMode() {
		return grade > 0;
	}

	// クラスを条件に利用するかどうか
	private boolean isClassNameMode() {
		return className != null;
	}
	
	// 個人IDを条件に利用するかどうか
	private boolean isIndividualIdMode() {
		return individualId != null;
	}
	
	// かな氏名絞り込みが有効かどうか
	private boolean isKanaNarrow() {
		// あいまい検索は除く
		return "1".equals(conditionSelect) && !"4".equals(conditionSubSelect);
	}
	
	/**
	 * ResultSetから生徒情報データを作る
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	protected KNStudentData createKSStudentData(
			final ResultSet rs) throws SQLException {

		final KNStudentExtData data = new KNStudentExtData(yearDiff);
		
		// 個人ID
		data.setIndividualId(rs.getString(1));
		// 性別
		data.setSex(rs.getString(2));
		// カタカナ氏名
		data.setNameKana(rs.getString(3));
		// ひらがな氏名
		data.setNameHiragana(
				JpnStringConv.kkanaHan2Hkana(rs.getString(3)));
		// 漢字氏名
		data.setNameKanji(rs.getString(4));
		// 誕生日
		data.setBirthday(rs.getDate(5));
		// 電話番号
		data.setTelNo(rs.getString(6));
		// 前々年度学年
		data.setBeforeLastGrade(rs.getInt(7));
		// 前々年度クラス
		data.setBeforeLastClassName(rs.getString(8));
		// 前々年度クラス番号
		data.setBeforeLastClassNo(rs.getString(9));
		// 前年度学年
		data.setLastGrade(rs.getInt(10));
		// 前年度クラス
		data.setLastClassName(rs.getString(11));
		// 前年度クラス番号
		data.setLastClassNo(rs.getString(12));
		// 学年
		data.setCurrentGrade(rs.getInt(13));
		// クラス
		data.setCurrentClassName(rs.getString(14));
		// クラス番号
		data.setCurrentClassNo(rs.getString(15));
		// 学年ソートキー（前々年度）
		data.setGradeKey1(rs.getString(16));
		// 学年ソートキー（前年度）
		data.setGradeKey2(rs.getString(17));
		// 学年ソートキー（今年度）
		data.setGradeKey3(rs.getString(18));
		// 識別マーク
		data.setCmark(rs.getString(19));
		// 対象年度
		data.setYear(year);
		
		// 再集計処理用に変更前の値を保持する
		// 変更前の今年度学年
		data.setOCurrentGrade(data.getCurrentGrade());
		// 変更前の今年度クラス
		data.setOCurrentClassName(data.getCurrentClassName());
		// 変更前の前年度学年
		data.setOLastGrade(data.getLastGrade());
		// 変更前の前年度クラス
		data.setOLastClassName(data.getLastClassName());
		// 変更前の前々年度学年
		data.setOBeforeLastGrade(data.getBeforeLastGrade());
		// 変更前の前々年度クラス
		data.setOBeforeLastClassName(data.getBeforeLastClassName());
		
		return data;
	}
	
	/**
	 * 表示生徒リストの初期化処理
	 * 
	 * @param bean 受験模試情報
	 * @param temp 仮受験者情報
	 * @param c 表示リストのComparator
	 */
	public void initDispStudentList(final ScoreExamListBean bean,
			final TempExamineeData temp,
			final Comparator c) {
		
		dispStudentList.clear();
		
		// 表示模試の設定がなければすべて
		if (bean.getDispExamMap().isEmpty()) {
			dispStudentList.addAll(studentList);
			sort(c);
			return;
		}
		
		// 表示対象がなければすべて
		if (!bean.isExaminee() && !bean.isTemporary()
				&& !bean.isNotTake()) {
			dispStudentList.addAll(studentList);
			sort(c);
			return;
		}
		
		// 表示模試セット
		final Set dispExamSet = bean.getDispExamMap().keySet();
		
		// ひとりずつ評価する
		for (final Iterator ite = studentList.iterator(); ite.hasNext();) {
			
			final KNStudentExtData data = (KNStudentExtData) ite.next();
			final Set takenExamSet = data.getTakenExamMap().keySet();
			
			// 仮受験・未受験判定
			// 模試をひとつずつ評価していく
			for (final Iterator ite2 = dispExamSet.iterator(
					); ite2.hasNext();) {
				
				// 模試ID
				final String examId = (String) ite2.next();
				
				// 受験
				if (takenExamSet.contains(examId)) {
					if (bean.isExaminee()) {
						dispStudentList.add(data);
						break;
					}
				// 仮受験
				} else if (temp.isTempExaminee(examId, data.getIndividualId())) {
					if (bean.isTemporary()) {
						dispStudentList.add(data);
						break;
					}
				// 未受験
				} else {
					if (bean.isNotTake()) {
						dispStudentList.add(data);
						break;
					}
				}
			}
		}
		
		// 最後にソートする
		sort(c);
	}
	
	/**
	 * 表示生徒リストの初期化処理
	 * （模試受験者情報なし版）
	 * 
	 * @param c 表示リストのComparator
	 */
	public void initDispStudentList(final Comparator c) {
		dispStudentList.clear();
		dispStudentList.addAll(studentList);
		sort(c);
	}

	/**
	 * @param pYear 設定する year。
	 */
	public void setYear(final String pYear) {
		
		// 年度がなければここまで
		if (StringUtils.isEmpty(pYear)) {
			year = null;
			return;
		}
		
		year = pYear;
		yearDiff = currentYear - Integer.parseInt(pYear);
		
		// 過去３年しかありえない
		if (yearDiff < 0 || yearDiff > 3) {
			throw new IllegalArgumentException(
					"不正な年度の指定です。");
		}
	}
	
	/**
	 * @param pClassName 設定する className。
	 */
	public void setClassName(final String pClassName) {
		
		// 全クラスは "NULL" で保持している
		if ("all".equals(pClassName)) {
			className = null;
		// 全クラス以外
		} else {
			className = pClassName;
		}
	}

	/**
	 * @param pGrade 設定する grade。
	 */
	public void setGrade(final String pGrade) {
		
		// 全学年は "-1" で保持している
		if ("all".equals(pGrade)) {
			grade = -1;
		// 全学年以外
		} else {
			grade = Integer.parseInt(pGrade);
		}
	}

	/**
	 * @param pConditionSelect 設定する conditionSelect。
	 * @param pConditionSubSelect 設定する conditionSubSelect。
	 * @param pConditionText 設定する conditionText。
	 */
	public void setCondition(final String pConditionSelect,
			final String pConditionSubSelect,
			final String pConditionText) {
		
		// 絞り込み条件
		conditionSelect = pConditionSelect;
		conditionSubSelect = pConditionSubSelect;
		
		// 絞り込み条件
		if ("1".equals(conditionSelect)) {
			conditionText = toSearchStr(pConditionText);
		}
	}

	/**
	 * @param individualId 個人ID
	 * @return 指定した個人IDを持つ生徒データ
	 */
	public KNStudentExtData getStudentData(final String individualId) {
		
		if (StringUtils.isEmpty(individualId)) {
			return null;
		}
		
		for (Iterator ite = studentList.iterator(); ite.hasNext();) {
			final KNStudentExtData data = (KNStudentExtData) ite.next();
			if (data.getIndividualId().equals(individualId)) {
				return data;
			}
		}
		
		return null;
	}
	
	/**
	 * @return 最初の生徒データ
	 */
	public KNStudentExtData getFirstStudentData() {
		if (studentList.isEmpty()) {
			return null;
		} else {
			return (KNStudentExtData) studentList.get(0);
		}
	}
	
	/**
	 * 生徒リストのソート処理
	 * 
	 * @param c comparator
	 */
	public void sort(final Comparator c) {
		Collections.sort(dispStudentList, c);
	}
	
	// かな氏名検索文字列変換
	private String toSearchStr(final String pValue) {
		
		if (pValue == null) {
			return null;
		}
		
		// ひらがな→半角カタカナ
		final String value = JpnStringConv.hkana2Han(pValue);
		// 変換して返す
		return StringUtils.replaceChars(value, "-ｧｨｩｪｫｬｭｮｯ ﾞﾟ", "ｰｱｲｳｴｵﾔﾕﾖﾂ");
	}
	
	/**
	 * 非表示生徒取得モード
	 */
	public void setNotDisplayMode() {
		isNotDisplayMode = true;
	}
	
	/**
	 * @return studentList を戻します。
	 */
	public List getStudentList() {
		return studentList;
	}

	/**
	 * @return dispStudentList を戻します。
	 */
	public List getDispStudentList() {
		return dispStudentList;
	}

	/**
	 * @return year を戻します。
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @return className を戻します。
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @return grade を戻します。
	 */
	public int getGrade() {
		return grade;
	}

	/**
	 * @param pIndividualId 設定する individualId。
	 */
	public void setIndividualId(String pIndividualId) {
		individualId = pIndividualId;
	}

}

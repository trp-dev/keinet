/*
 * 作成日: 2004/11/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.individual.ExamPlanUnivData;

import com.fjh.beans.DefaultSearchBean;

/**
 * @author T.Yamada
 * 受験予定大学一覧の取得
 */
public class ExamPlanUnivSearchBean extends DefaultSearchBean {

	/**
	 * 生徒ID
	 */
	private String individualId;
	/**
	 * 大学コード
	 */
	private String univCd;
	/**
	 * 学部コード
	 */
	private String facultyCd;
	/**
	 * 学科コード
	 */
	private String deptCd;
	/**
	 * 入試形態コード
	 */
	private String entExamModeCd;
	
	private String year;
	private String examDiv;

	private String query = ""+
	"SELECT "+
	"INDIVIDUALID "+",UNIVCD "+",FACULTYCD "+",DEPTCD "+
	",ENTEXAMMODECD "+",ENTEXAMDIV1 "+",ENTEXAMDIV2 "+",ENTEXAMDIV3 "+
	",YEAR "+",EXAMDIV "+",REMARKS "+
	",EXAMPLANDATE1 "+",EXAMPLANDATE2 "+",EXAMPLANDATE3 "+",EXAMPLANDATE4 "+",EXAMPLANDATE5 "+
	",EXAMPLANDATE6 "+",EXAMPLANDATE7 "+",EXAMPLANDATE8 "+",EXAMPLANDATE9 "+",EXAMPLANDATE10 "+
	",PROVENTEXAMSITE1 "+",PROVENTEXAMSITE2 "+",PROVENTEXAMSITE3 "+",PROVENTEXAMSITE4 "+",PROVENTEXAMSITE5 "+
	",PROVENTEXAMSITE6 "+",PROVENTEXAMSITE7 "+",PROVENTEXAMSITE8 "+",PROVENTEXAMSITE9 "+",PROVENTEXAMSITE10 "+
	" FROM EXAMPLANUNIV WHERE 1 =1 ";


	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		if(individualId != null){
			query += "AND INDIVIDUALID = '"+individualId+"' ";
		}
		if(entExamModeCd != null){
			query += "AND ENTEXAMMODECD = '"+entExamModeCd+"' ";
		}
		if(univCd != null){
			query += "AND UNIVCD = '"+univCd+"' ";
		}
		if(facultyCd != null){
			query += "AND FACULTYCD = '"+facultyCd+"' ";
		}
		if(deptCd != null){
			query += "AND DEPTCD = '"+deptCd+"' ";
		}
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while(rs.next()){
				ExamPlanUnivData epud = new ExamPlanUnivData();
				epud.setIndividualId(rs.getString("INDIVIDUALID"));
				epud.setUnivCd(rs.getString("UNIVCD"));
				epud.setFacultyCd(rs.getString("FACULTYCD"));
				epud.setDeptCd(rs.getString("DEPTCD"));
				epud.setEntExamModeCd(rs.getString("ENTEXAMMODECD"));
				epud.setEntExamDiv1(rs.getString("ENTEXAMDIV1"));
				epud.setEntExamDiv2(rs.getString("ENTEXAMDIV2"));
				epud.setEntExamDiv3(rs.getString("ENTEXAMDIV3"));
				epud.setYear(rs.getString("YEAR"));
				epud.setExamDiv(rs.getString("EXAMDIV"));
				epud.setRemarks(rs.getString("REMARKS"));
				epud.setExamPlanDate1(rs.getString("EXAMPLANDATE1"));
				epud.setExamPlanDate2(rs.getString("EXAMPLANDATE2"));
				epud.setExamPlanDate3(rs.getString("EXAMPLANDATE3"));
				epud.setExamPlanDate4(rs.getString("EXAMPLANDATE4"));
				epud.setExamPlanDate5(rs.getString("EXAMPLANDATE5"));
				epud.setExamPlanDate6(rs.getString("EXAMPLANDATE6"));
				epud.setExamPlanDate7(rs.getString("EXAMPLANDATE7"));
				epud.setExamPlanDate8(rs.getString("EXAMPLANDATE8"));
				epud.setExamPlanDate9(rs.getString("EXAMPLANDATE9"));
				epud.setExamPlanDate10(rs.getString("EXAMPLANDATE10"));
				epud.setProvEntExamSite1(rs.getString("PROVENTEXAMSITE1"));
				epud.setProvEntExamSite2(rs.getString("PROVENTEXAMSITE2"));
				epud.setProvEntExamSite3(rs.getString("PROVENTEXAMSITE3"));
				epud.setProvEntExamSite4(rs.getString("PROVENTEXAMSITE4"));
				epud.setProvEntExamSite5(rs.getString("PROVENTEXAMSITE5"));
				epud.setProvEntExamSite6(rs.getString("PROVENTEXAMSITE6"));
				epud.setProvEntExamSite7(rs.getString("PROVENTEXAMSITE7"));
				epud.setProvEntExamSite8(rs.getString("PROVENTEXAMSITE8"));
				epud.setProvEntExamSite9(rs.getString("PROVENTEXAMSITE9"));
				epud.setProvEntExamSite10(rs.getString("PROVENTEXAMSITE10"));
				
				recordSet.add(epud);
			}
		}catch(SQLException sqle){
			System.out.println(query);
			throw sqle;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
		
	}

	/**
	 * @return
	 */
	public String getExamDiv() {
		return examDiv;
	}

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @return
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param string
	 */
	public void setExamDiv(String string) {
		examDiv = string;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

	/**
	 * @param string
	 */
	public void setYear(String string) {
		year = string;
	}

	/**
	 * @return
	 */
	public String getEntExamModeCd() {
		return entExamModeCd;
	}

	/**
	 * @param string
	 */
	public void setEntExamModeCd(String string) {
		entExamModeCd = string;
	}

	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

}

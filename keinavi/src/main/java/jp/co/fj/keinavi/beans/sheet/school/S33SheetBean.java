/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.beans.sheet.NoQuestionException;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.excel.data.school.S33ClassListBean;
import jp.co.fj.keinavi.excel.data.school.S33Item;
import jp.co.fj.keinavi.excel.data.school.S33KokugoClassListBean;
import jp.co.fj.keinavi.excel.data.school.S33KokugoListBean;
import jp.co.fj.keinavi.excel.data.school.S33KokugoQuestionClassListBean;
import jp.co.fj.keinavi.excel.data.school.S33KokugoQuestionListBean;
import jp.co.fj.keinavi.excel.data.school.S33ListBean;
import jp.co.fj.keinavi.excel.data.school.S33StatusBean;
import jp.co.fj.keinavi.excel.data.school.S33StatusClassListBean;
import jp.co.fj.keinavi.excel.data.school.S33StatusListBean;
import jp.co.fj.keinavi.excel.school.S33;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 *
 * 2009.10.07   Fujito URAKAWA - Totec
 *              「全体成績」追加対応
 *
 * @author kawai
 *
 */
public class S33SheetBean extends AbstractSheetBean {

	// データクラス
	private final S33Item data = new S33Item();

	// 2019/09/05 QQ)Ooseto 共通テスト対応 ADD START
	// 科目コード
	private String SUBCD = "";
	// 設問番号
	private final String[] QUESTIONNO = {"01","02","03"};
	// 2019/09/05 QQ)Ooseto 共通テスト対応 ADD END

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		String[] code = super.getSubjectCDArray(); // 型・科目設定値
		List graph = super.getSubjectGraphList(); // 型・科目グラフ表示対象

		String tmp_kmkcd = "";	// 科目コードチェック用変数

		// 設問別成績存在チェック
		if (hasAllNoQuestionSubject(code)) {
			throw new NoQuestionException();
		}

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntHyouFlg(super.getIntFlag(IProfileItem.CHART)); // 表フラグ
		data.setIntGraphFlg(super.getIntFlag(IProfileItem.GRAPH)); // グラフフラグ
		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ

		// 2019/09/04 QQ)Tanioka 記述式問題詳細分析 追加 ADD START
		// マーク模試正答状況フラグはマーク系の模試でなければ0
		if (!"1".equals(exam.getDockingType())) {
			data.setIntKokugoFlg(0);
			data.setIntStatusFlg(0);
		// マーク系の模試なら設定値
		} else {
			data.setIntKokugoFlg(getIntFlag(IProfileItem.OPTION_CHECK_KOKUGO));
			data.setIntStatusFlg(getIntFlag(IProfileItem.OPTION_CHECK_STATUS));
		}
		// 2019/09/04 QQ)Tanioka 記述式問題詳細分析 追加 ADD END

		// ワークテーブルのセットアップ
		super.insertIntoCountCompClass();

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		// 2019/09/05 QQ)Ooseto 共通テスト対応 ADD START
                PreparedStatement ps6 = null;
		// 2019/09/05 QQ)Ooseto 共通テスト対応 ADD END
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		// 2019/09/05 QQ)Ooseto 共通テスト対応 ADD START
                ResultSet rs4 = null;
                ResultSet rs5 = null;
                ResultSet rs6 = null;
		// 2019/09/05 QQ)Ooseto 共通テスト対応 ADD END

		try {
			// データリスト
			Query query = QueryLoader.getInstance().load("s33_1");
			query.setStringArray(1, code); // 型・科目コード

			ps1 = conn.prepareStatement(query.toString());
			ps1.setString(1, exam.getExamYear()); // 模試年度
			ps1.setString(2, exam.getExamCD()); // 模試コード

			// クラスデータリスト（自校）
			ps2 = conn.prepareStatement(QueryLoader.getInstance().load("s33_2").toString());
			ps2.setString(1, exam.getExamYear()); // 模試年度
			ps2.setString(2, exam.getExamCD()); // 模試コード
			ps2.setString(3, profile.getBundleCD()); // 一括コード

			// クラスデータリスト（クラス）
			ps3 = conn.prepareStatement(QueryLoader.getInstance().load("s33_3").toString());
			ps3.setString(1, exam.getExamYear()); // 模試年度
			ps3.setString(2, exam.getExamCD()); // 模試コード
			ps3.setString(3, profile.getBundleCD()); // 一括コード
			ps3.setString(6, exam.getExamYear()); // 模試年度
			ps3.setString(7, exam.getExamCD()); // 模試コード
			ps3.setString(8, profile.getBundleCD()); // 一括コード
			ps3.setString(11, exam.getExamYear()); // 模試年度
			ps3.setString(12, exam.getExamCD()); // 模試コード

			// 科目別クラスデータリスト（自校）
			ps4 = conn.prepareStatement(QueryLoader.getInstance().load("s33_4").toString());
			ps4.setString(1, exam.getExamYear());
			ps4.setString(2, exam.getExamCD());
			ps4.setString(3, profile.getBundleCD());

			// 科目別クラスデータリスト（クラス）
			{
				ps5 = conn.prepareStatement(QueryLoader.getInstance().load("s33_5").toString());
				ps5.setString(1, exam.getExamYear());
				ps5.setString(2, exam.getExamCD());
				ps5.setString(3, profile.getBundleCD());
				ps5.setString(5, exam.getExamYear());
				ps5.setString(6, exam.getExamCD());
				ps5.setString(8, profile.getBundleCD());
			}


			rs1 = ps1.executeQuery();
			while (rs1.next()) {

				// 科目コードが異なる場合のみ、全体成績データをセットする
				if (!tmp_kmkcd.equals(rs1.getString(1))) {

					putTotalSubjectData(rs1, ps4, ps5, graph, profile);

				}

				S33ListBean bean = new S33ListBean();
				bean.setStrKmkCd(rs1.getString(1));
				bean.setStrKmkmei(rs1.getString(2));
				bean.setIntDispKmkFlg(graph.contains(rs1.getString(1)) ? 1 : 0);
				bean.setStrHaitenKmk(rs1.getString(3));
				bean.setStrSetsuNo(rs1.getString(4));
				bean.setStrSetsuMei1(rs1.getString(5));
				bean.setStrSetsuHaiten(rs1.getString(6));
				data.getS33List().add(bean);

				// 科目コード
				ps2.setString(4, rs1.getString(1));
				ps3.setString(4, rs1.getString(1));
				ps3.setString(9, rs1.getString(1));
				ps3.setString(13, rs1.getString(1));
				// 設問番号
				ps2.setInt(5, rs1.getInt(7));
				ps3.setInt(5, rs1.getInt(7));
				ps3.setInt(10, rs1.getInt(7));
				ps3.setInt(14, rs1.getInt(7));

				// 自校
				{
					S33ClassListBean c = new S33ClassListBean();
					c.setStrGakkomei(profile.getBundleName());
					c.setIntDispClassFlg(1);

					rs2 = ps2.executeQuery();

					if (rs2.next()) {
						c.setIntNinzu(rs2.getInt(1));
						c.setFloTokuritsu(rs2.getFloat(2));
					} else {
						c.setIntNinzu(-999);
						c.setFloTokuritsu(-999);
					}

					rs2.close();

					bean.getS33ClassList().add(c);
				}

				// クラス
				{
					rs3 = ps3.executeQuery();
					while (rs3.next()) {
						// Beanに詰める
						S33ClassListBean c = new S33ClassListBean();
						c.setStrGrade(rs3.getString(1));
						c.setStrClass(rs3.getString(2));
						c.setIntDispClassFlg(rs3.getInt(3));
						c.setIntNinzu(rs3.getInt(4));
						c.setFloTokuritsu(rs3.getFloat(5));
						bean.getS33ClassList().add(c);
					}
					rs3.close();
				}

				// 処理中の科目コードをセット
				tmp_kmkcd = rs1.getString(1);

			}
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(rs3);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(ps3);
			DbUtils.closeQuietly(ps4);
			DbUtils.closeQuietly(ps5);
		}

		// 2019/09/05 QQ)Ooseto 共通テスト対応 ADD START
		// 国語 評価別人数フラグが1なら国語 評価別人数データリストを作る
		if (data.getIntKokugoFlg() == 1) {

		    // 科目コード
		    SUBCD = "3915"; // 国語記述：3915

		    int target = 0;

		    S33KokugoListBean bean = new S33KokugoListBean();

			try {

			    // 学校名取得
			    ps1 = conn.prepareStatement(QueryLoader.getInstance().load("s33_6").toString());
			    ps1.setString(1, profile.getBundleCD()); // 自校

			    rs1 = ps1.executeQuery();
			    while (rs1.next()) {
			        bean.setBundleName(rs1.getString(1));
			    }

			    // 対象模試取得
			    ps2 = conn.prepareStatement(QueryLoader.getInstance().load("s33_7").toString());
			    ps2.setString(1, exam.getExamYear()); // 模試年度
			    ps2.setString(2, exam.getExamCD()); // 模試コード

			    rs2 = ps2.executeQuery();
			    while (rs2.next()) {
			        bean.setExamName(rs2.getString(1));
			        bean.setTergetGrade(rs2.getInt(2));
			        target = rs2.getInt(2);
			    }

			    // 総合評価-校内全体データリスト取得
			    ps3 = conn.prepareStatement(QueryLoader.getInstance().load("s33_8").toString());
			    ps3.setString(1, exam.getExamYear()); // 模試年度
			    ps3.setString(2, exam.getExamCD()); // 模試コード
			    ps3.setString(3, profile.getBundleCD()); // 自校
			    ps3.setString(4, SUBCD); // 科目コード

			    rs3 = ps3.executeQuery();
			    while (rs3.next()) {
			        bean.setNumbers(rs3.getInt(1));
			        bean.setEvalACompratio(rs3.getFloat(2));
			        bean.setEvalBCompratio(rs3.getFloat(3));
			        bean.setEvalCCompratio(rs3.getFloat(4));
			        bean.setEvalDCompratio(rs3.getFloat(5));
			        bean.setEvalECompratio(rs3.getFloat(6));
			        bean.setEvalANumbers(rs3.getInt(7));
			        bean.setEvalBNumbers(rs3.getInt(8));
			        bean.setEvalCNumbers(rs3.getInt(9));
			        bean.setEvalDNumbers(rs3.getInt(10));
			        bean.setEvalENumbers(rs3.getInt(11));
			    }

			    // 総合評価-クラスデータリスト取得
			    ps4 = conn.prepareStatement(QueryLoader.getInstance().load("s33_9").toString());
			    // 単体クラスデータ取得用パラメータ
			    ps4.setString(1, exam.getExamYear()); // 模試年度
			    ps4.setString(2, exam.getExamCD()); // 模試コード
			    ps4.setString(3, profile.getBundleCD()); // 自校
			    ps4.setString(4, SUBCD); // 科目コード
			    ps4.setInt(5, target); // 対象学年
                            ps4.setString(6, exam.getExamYear()); // 模試年度
                            ps4.setString(7, exam.getExamCD()); // 模試コード
                            ps4.setString(8, profile.getBundleCD()); // 自校
                            ps4.setString(9, SUBCD); // 科目コード
                            ps4.setInt(10, target); // 対象学年
			    // 複合クラスデータ取得用パラメータ
                            ps4.setString(11, exam.getExamYear()); // 模試年度
                            ps4.setString(12, exam.getExamCD()); // 模試コード
                            ps4.setString(13, profile.getBundleCD()); // 自校
                            ps4.setString(14, SUBCD); // 科目コード
                            ps4.setInt(15, target); // 対象学年
                            ps4.setString(16, exam.getExamYear()); // 模試年度
                            ps4.setString(17, exam.getExamCD()); // 模試コード
                            ps4.setString(18, profile.getBundleCD()); // 自校
                            ps4.setString(19, SUBCD); // 科目コード
                            ps4.setInt(20, target); // 対象学年

			    rs4 = ps4.executeQuery();
			    while (rs4.next()) {
			        S33KokugoClassListBean list = new S33KokugoClassListBean();
			        if(rs4.getInt(13) != -1) {
			            list.setKokugoClass(rs4.getString(1).trim());
			            list.setNumbers(rs4.getInt(2));
			            list.setEvalACompratio(rs4.getFloat(3));
			            list.setEvalBCompratio(rs4.getFloat(4));
			            list.setEvalCCompratio(rs4.getFloat(5));
			            list.setEvalDCompratio(rs4.getFloat(6));
			            list.setEvalECompratio(rs4.getFloat(7));
			            list.setEvalANumbers(rs4.getInt(8));
			            list.setEvalBNumbers(rs4.getInt(9));
			            list.setEvalCNumbers(rs4.getInt(10));
			            list.setEvalDNumbers(rs4.getInt(11));
			            list.setEvalENumbers(rs4.getInt(12));
			            list.setDispsequence(rs4.getInt(13));
			            bean.getS33KokugoClassList().add(list);
			        }
			    }

			    // 問別評価-校内全体データリスト取得
			    ps5 = conn.prepareStatement(QueryLoader.getInstance().load("s33_10").toString());
			    ps5.setString(1, exam.getExamYear()); // 模試年度
			    ps5.setString(2, exam.getExamCD()); // 模試コード
			    ps5.setString(3, profile.getBundleCD()); // 自校
			    ps5.setString(4, SUBCD); // 科目コード

			    rs5 = ps5.executeQuery();
			    while (rs5.next()) {
			        S33KokugoQuestionListBean list = new S33KokugoQuestionListBean();
			        list.setQuestionNo(rs5.getString(1));
			        // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD START
			        list.setEvalACompratio(rs5.getFloat(2));
			        list.setEvalAAstCompratio(rs5.getFloat(3));
			        //list.setEvalBCompratio(rs5.getFloat(3));
			        list.setEvalBCompratio(rs5.getFloat(4));
                                list.setEvalBAstCompratio(rs5.getFloat(5));
			        //list.setEvalCCompratio(rs5.getFloat(4));
                                list.setEvalCCompratio(rs5.getFloat(6));
			        //list.setEvalDCompratio(rs5.getFloat(5));
			        //list.setEvalANumbers(rs5.getInt(6));
                                list.setEvalANumbers(rs5.getInt(7));
                                list.setEvalAAstNumbers(rs5.getInt(8));
			        //list.setEvalBNumbers(rs5.getInt(7));
                                list.setEvalBNumbers(rs5.getInt(9));
                                list.setEvalBAstNumbers(rs5.getInt(10));
			        //list.setEvalCNumbers(rs5.getInt(8));
                                list.setEvalCNumbers(rs5.getInt(11));
			        //list.setEvalDNumbers(rs5.getInt(9));
			        // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD END
			        bean.getS33KokugoQuestionList().add(list);
			    }

			    // 問別評価-クラスデータリスト取得
			    for(int i = 0; i < 3; i++ ) {
			        ps6 = conn.prepareStatement(QueryLoader.getInstance().load("s33_11").toString());
                                // 単体クラスデータ取得用パラメータ
			        ps6.setString(1, QUESTIONNO[i]); // 設問番号
                                ps6.setString(2, exam.getExamYear()); // 模試年度
                                ps6.setString(3, exam.getExamCD()); // 模試コード
                                ps6.setString(4, profile.getBundleCD()); // 自校
                                ps6.setString(5, SUBCD); // 科目コード
                                ps6.setInt(6, target); // 対象学年
                                ps6.setString(7, QUESTIONNO[i]); // 設問番号
                                // 複合クラスデータ取得用パラメータ
                                ps6.setString(8, QUESTIONNO[i]); // 設問番号
                                ps6.setString(9, exam.getExamYear()); // 模試年度
                                ps6.setString(10, exam.getExamCD()); // 模試コード
                                ps6.setString(11, profile.getBundleCD()); // 自校
                                ps6.setString(12, SUBCD); // 科目コード
                                ps6.setInt(13, target); // 対象学年
                                ps6.setString(14, QUESTIONNO[i]); // 設問番号

			        rs6 = ps6.executeQuery();
			        while (rs6.next()) {
			            S33KokugoQuestionClassListBean list = new S33KokugoQuestionClassListBean();
			            list.setKokugoClass(rs6.getString(1).trim());
			            list.setQuestionNo(rs6.getString(2));
			            // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD START
			            list.setEvalACompratio(rs6.getFloat(3));
                                    list.setEvalAAstCompratio(rs6.getFloat(4));
			            //list.setEvalBCompratio(rs6.getFloat(4));
                                    list.setEvalBCompratio(rs6.getFloat(5));
                                    list.setEvalBAstCompratio(rs6.getFloat(6));
			            //list.setEvalCCompratio(rs6.getFloat(5));
                                    list.setEvalCCompratio(rs6.getFloat(7));
			            //list.setEvalDCompratio(rs6.getFloat(6));
			            //list.setEvalANumbers(rs6.getInt(7));
			            list.setEvalANumbers(rs6.getInt(8));
			            list.setEvalAAstNumbers(rs6.getInt(9));
			            //list.setEvalBNumbers(rs6.getInt(8));
			            list.setEvalBNumbers(rs6.getInt(10));
			            list.setEvalBAstNumbers(rs6.getInt(11));
			            //list.setEvalCNumbers(rs6.getInt(9));
			            list.setEvalCNumbers(rs6.getInt(12));
			            //list.setEvalDNumbers(rs6.getInt(10));
			            //list.setDispsequence(rs6.getInt(11));
			            list.setDispsequence(rs6.getInt(13));
			            // 2019/11/27 QQ)Ooseto 国語記述設問変更対応 UPD END
			            bean.getS33KokugoQuestionClassList().add(list);
			        }
			    }

			} finally {
				DbUtils.closeQuietly(rs1);
				DbUtils.closeQuietly(rs2);
				DbUtils.closeQuietly(rs3);
				DbUtils.closeQuietly(rs4);
				DbUtils.closeQuietly(rs5);
				DbUtils.closeQuietly(rs6);
				DbUtils.closeQuietly(ps1);
				DbUtils.closeQuietly(ps2);
				DbUtils.closeQuietly(ps3);
				DbUtils.closeQuietly(ps4);
				DbUtils.closeQuietly(ps5);
				DbUtils.closeQuietly(ps6);
			}

			data.getS33KokugoList().add(bean);

		}
		// 2019/09/05 QQ)Ooseto 共通テスト対応 ADD END

	        // 2019/09/11 QQ)Ooseto 共通テスト対応 ADD START
		// 小設問別正答状況フラグが1なら小設問別正答状況データリストを作る
		if (data.getIntStatusFlg() == 1) {

                    // 科目コード
                    SUBCD = "3915"; // 国語記述：3915

                    int target = 0;

                    S33StatusBean bean = new S33StatusBean();

                    try {

                        // 学校名取得
                        ps1 = conn.prepareStatement(QueryLoader.getInstance().load("s33_6").toString());
                        ps1.setString(1, profile.getBundleCD()); // 自校

                        rs1 = ps1.executeQuery();
                        while (rs1.next()) {
                            bean.setBundleName(rs1.getString(1));
                        }

                        // 対象模試取得
                        ps2 = conn.prepareStatement(QueryLoader.getInstance().load("s33_7").toString());
                        ps2.setString(1, exam.getExamYear()); // 模試年度
                        ps2.setString(2, exam.getExamCD()); // 模試コード

                        rs2 = ps2.executeQuery();
                        while (rs2.next()) {
                            bean.setExamName(rs2.getString(1));
                            bean.setTergetGrade(rs2.getInt(2));
                            target = rs2.getInt(2);
                        }

                        // 校内全体データリスト取得
                        ps3 = conn.prepareStatement(QueryLoader.getInstance().load("s33_12").toString());
                        ps3.setString(1, exam.getExamYear()); // 模試年度
                        ps3.setString(2, exam.getExamCD()); // 模試コード
                        ps3.setString(3, profile.getBundleCD()); // 自校
                        ps3.setString(4, SUBCD); // 科目コード

                        rs3 = ps3.executeQuery();
                        while (rs3.next()) {
                            S33StatusListBean list = new S33StatusListBean();
                            list.setQuestionNo(rs3.getString(1));
                            list.setNumbers(rs3.getInt(2));
                            list.setCorrectAnsrate1(rs3.getFloat(3));
                            list.setCorrectAnsrate2(rs3.getFloat(4));
                            list.setCorrectAnsrate3(rs3.getFloat(5));
                            list.setCorrectAnsrate4(rs3.getFloat(6));
                            list.setCorrectAnsrate5(rs3.getFloat(7));
                            list.setCorrectAnsrate6(rs3.getFloat(8));
                            list.setCorrectAnsrate7(rs3.getFloat(9));
                            list.setCorrectPersonnum1(rs3.getInt(10));
                            list.setCorrectPersonnum2(rs3.getInt(11));
                            list.setCorrectPersonnum3(rs3.getInt(12));
                            list.setCorrectPersonnum4(rs3.getInt(13));
                            list.setCorrectPersonnum5(rs3.getInt(14));
                            list.setCorrectPersonnum6(rs3.getInt(15));
                            list.setCorrectPersonnum7(rs3.getInt(16));
                            bean.getS33StatusList().add(list);
                        }

                        // クラスデータリスト取得
                        for(int i = 0; i < 3; i++ ) {
                            ps4 = conn.prepareStatement(QueryLoader.getInstance().load("s33_13").toString());
                            // 単体クラスデータ取得用パラメータ
                            ps4.setString(1, QUESTIONNO[i]); // 設問番号
                            ps4.setString(2, exam.getExamYear()); // 模試年度
                            ps4.setString(3, exam.getExamCD()); // 模試コード
                            ps4.setString(4, profile.getBundleCD()); // 自校
                            ps4.setString(5, SUBCD); // 科目コード
                            ps4.setInt(6, target); // 対象学年
                            ps4.setString(7, QUESTIONNO[i]); // 設問番号
                            // 複合クラスデータ取得用パラメータ
                            ps4.setString(8, QUESTIONNO[i]); // 設問番号
                            ps4.setString(9, exam.getExamYear()); // 模試年度
                            ps4.setString(10, exam.getExamCD()); // 模試コード
                            ps4.setString(11, profile.getBundleCD()); // 自校
                            ps4.setString(12, SUBCD); // 科目コード
                            ps4.setInt(13, target); // 対象学年
                            ps4.setString(14, QUESTIONNO[i]); // 設問番号

                            rs4 = ps4.executeQuery();
                            while (rs4.next()) {
                                S33StatusClassListBean list = new S33StatusClassListBean();
                                list.setQuestionNo(rs4.getString(1));
                                list.setKokugoClass(rs4.getString(2).trim());
                                list.setNumbers(rs4.getInt(3));
                                list.setCorrectAnsrate1(rs4.getFloat(4));
                                list.setCorrectAnsrate2(rs4.getFloat(5));
                                list.setCorrectAnsrate3(rs4.getFloat(6));
                                list.setCorrectAnsrate4(rs4.getFloat(7));
                                list.setCorrectAnsrate5(rs4.getFloat(8));
                                list.setCorrectAnsrate6(rs4.getFloat(9));
                                list.setCorrectAnsrate7(rs4.getFloat(10));
                                list.setCorrectPersonnum1(rs4.getInt(11));
                                list.setCorrectPersonnum2(rs4.getInt(12));
                                list.setCorrectPersonnum3(rs4.getInt(13));
                                list.setCorrectPersonnum4(rs4.getInt(14));
                                list.setCorrectPersonnum5(rs4.getInt(15));
                                list.setCorrectPersonnum6(rs4.getInt(16));
                                list.setCorrectPersonnum7(rs4.getInt(17));
                                list.setDispsequence(rs4.getInt(18));
                                bean.getS33StatusClassList().add(list);
                            }
                        }

                    } finally {
                            DbUtils.closeQuietly(rs1);
                            DbUtils.closeQuietly(rs2);
                            DbUtils.closeQuietly(rs3);
                            DbUtils.closeQuietly(rs4);
                            DbUtils.closeQuietly(ps1);
                            DbUtils.closeQuietly(ps2);
                            DbUtils.closeQuietly(ps3);
                            DbUtils.closeQuietly(ps4);
                    }

                    data.getS33StatusList().add(bean);

		}
	        // 2019/09/11 QQ)Ooseto 共通テスト対応 ADD END
	}

	/**
	 * 設問別成績の先頭に「全体成績」データをセットする
	 * @param rs1
	 * @param ps4
	 * @param ps5
	 * @param graph
	 * @param profile
	 * @throws SQLException
	 */
	private void putTotalSubjectData(ResultSet rs1,
			PreparedStatement ps4, PreparedStatement ps5, List graph,
			Profile profile) throws SQLException{

		ResultSet rs4 = null;
		ResultSet rs5 = null;

		try {
			S33ListBean bean = new S33ListBean();
			bean.setStrKmkCd(rs1.getString(1));
			bean.setStrKmkmei(rs1.getString(2));
			bean.setIntDispKmkFlg(graph.contains(rs1.getString(1)) ? 1 : 0);
			bean.setStrHaitenKmk(rs1.getString(3));
			bean.setStrSetsuNo("");							// 設問Noに「""」をセット
			bean.setStrSetsuMei1("全体成績");				// 設問名に「"全体成績"」をセット
			bean.setStrSetsuHaiten(rs1.getString(3));		// 配点に科目配点をセット
			data.getS33List().add(bean);

			// 科目コード
			ps4.setString(4, rs1.getString(1));
			ps5.setString(4, rs1.getString(1));
			ps5.setString(7, rs1.getString(1));

			// 自校
			{
				S33ClassListBean c = new S33ClassListBean();
				c.setStrGakkomei(profile.getBundleName());
				c.setIntDispClassFlg(1);
				rs4 = ps4.executeQuery();
				if (rs4.next()) {
					c.setIntNinzu(rs4.getInt(1));
					c.setFloTokuritsu(rs4.getFloat(2));
				} else {
					c.setIntNinzu(-999);
					c.setFloTokuritsu(-999);
				}
				rs4.close();
				bean.getS33ClassList().add(c);

			}
			// クラスの科目別成績セット
			{
				rs5 = ps5.executeQuery();
				while (rs5.next()) {
					// Beanに詰める
					S33ClassListBean c = new S33ClassListBean();
					c.setStrGrade(rs5.getString(1));
					c.setStrClass(rs5.getString(2));
					c.setIntDispClassFlg(rs5.getInt(3));
					c.setIntNinzu(rs5.getInt(4));
					c.setFloTokuritsu(rs5.getFloat(5));
					bean.getS33ClassList().add(c);
				}
				rs5.close();
			}
		}
		finally {
			DbUtils.closeQuietly(rs4);
			DbUtils.closeQuietly(rs5);
		}

	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S33_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_CLASS_QUE;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S33()
			.s33(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
	}

}

package jp.co.fj.keinavi.beans.sheet.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.sheet.ExtI11ListBean;
// 2019/09/19 QQ)Tanouchi 共通テスト対応 ADD START
import jp.co.fj.keinavi.excel.cm.CM;
// 2019/09/19 QQ)Tanouchi 共通テスト対応 ADD END
import jp.co.fj.keinavi.excel.data.personal.I13HyoukaListBean;
import jp.co.fj.keinavi.excel.data.personal.I13KyokaSeisekiListBean;
import jp.co.fj.keinavi.excel.data.personal.I13SeisekiListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * 個人成績分析−成績分析面談 連続個人成績表データリスト印刷用Bean
 *
 * 200X.XX.XX [新規作成]
 *
 *
 * @author Keisuke KONDO - TOTEC
 * @version 1.0
 *
 * [1]  2005.02.02 k.KONDO      外部データ開放日が同一の場合、模試コード順にソートするよう修正
 * [2]  2005.04.14 k.KONDO      面談シート１のとき、面談シートフラグが設定されるように修正
 * [3]  2005.04.14 k.KONDO      対象模試が新テストだった場合、種別フラグに"1"を入れるように修正
 * [4]  2005.04.19 k.KONDO      成績で新テストが含まれていた場合、最後尾に空データを入れその後に追加するように修正
 * [5]  2005.09.16 k.KONDO      科目取得の際EXAMSUBJECTではなく、Viewを参照するように修正
 * [6]  2006.08.24 t.YAMADA     試験一覧の表示順を「外部（内部）データ開放日」から「実施日」に変更
 *
 * 2009.11.25   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 * 2010.01.18   Shoji HASE - Totec
 *              ・「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 * 　　　　　　 　「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 *      　	　　・「HS12_UNIV：高１・２大学マスタ(判定＆志望校名称)」テーブルを
 * 　　　　       「H12_UNIVMASTER：高１２用大学マスタ」テーブルに置き換え
 *
 */
public class I13SheetBean extends IndAbstractSheetBean {


    private final Short menSheetFlg;
    /**
     * コンストラクタです。
     * @param menSheetFlg
     */
    public I13SheetBean(Short menSheetFlg) {
        super();
        this.menSheetFlg = menSheetFlg;
    }
    /**
     * コンストラクタです。
     */
    public I13SheetBean() {
        super();
        this.menSheetFlg = null;
    }

    /** serialVersionUID */
    private static final long serialVersionUID = 8073050959094038858L;

    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws SQLException, Exception {

        int examtype = 0;
        int dispGrade = 0;

	// 2019/09/19 QQ)Tanouchi 共通テスト対応 ADD START
        String KokugoKijutuScore = "";
	// 2019/09/19 QQ)Tanouchi 共通テスト対応 ADD END
        // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD START
        // 面談シートフラグ
        String flag = "0";
        // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD END

        ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);//[4]

        if(!"i003".equals(backward)){
            /**連続個人成績表の場合プロファイル**/
            //連続個人成績フラグ
            data.setIntSeisekiFlg(1);
            //連続個人成績シートセキュリティスタンプ
            data.setIntSeisekiSecuFlg(((Short) profile.getItemMap(IProfileCategory.I_SCORE_SHEET).get(IProfileItem.PRINT_STAMP)).shortValue());
            data.setIntSeisekiShubetsuFlg(getNewTestFlg());//[3] add 新テストフラグ（成績シート）
            //模試種類
            Short examType = (Short) profile.getItemMap(IProfileCategory.I_SCORE_SHEET).get(IProfileItem.EXAM_TYPE);
            examtype = Integer.parseInt(examType.toString());
            //表示生徒
            Short studentDispGrade = (Short)profile.getItemMap(IProfileCategory.I_SCORE_SHEET).get(IProfileItem.STUDENT_DISP_GRADE);
            dispGrade = Integer.parseInt(studentDispGrade.toString());

            // 2020/02/22 QQ)nagai 共通テスト対応 ADD START
            Short MenSheet = menSheetFlg != null ? menSheetFlg : (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_SHEET);
            flag = MenSheet.toString();
            // 2020/02/22 QQ)nagai 共通テスト対応 ADD END

        }else{
            /**簡単印刷の場合プロファイル**/
            Short Seiseki = (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_REPORT);
            int Seisekiflg = Integer.parseInt(Seiseki.toString());
            //面談シートフラグ
            Short MenSheet = menSheetFlg != null ? menSheetFlg : (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_SHEET);
            int MenSheetFlg = Integer.parseInt(MenSheet.toString());
            // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD START
            flag = MenSheet.toString();
            // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD END
            //模試種類
            examtype = 1;
            //表示生徒
            Short studentDispGrade = (Short)profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.STUDENT_DISP_GRADE);
            dispGrade = Integer.parseInt(studentDispGrade.toString());
            //成績シートが選択されたか、面談シートが選択されたのか
            if(Seisekiflg == 1){
                data.setIntSeisekiFlg(((Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_REPORT)).shortValue());
                data.setIntSeisekiSecuFlg(((Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.PRINT_STAMP)).shortValue());
                data.setIntSeisekiShubetsuFlg(getNewTestFlg());//[3] add 新テストフラグ（成績シート）
            }
            //if(MenSheetFlg == 2 || MenSheetFlg == 4){//[2] delete
            if(MenSheetFlg == 2 || MenSheetFlg == 3 || MenSheetFlg == 4 || MenSheetFlg == 5){//[2] add
                data.setIntMenSheetFlg(MenSheet);
                data.setIntMenSheetSecuFlg(((Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.PRINT_STAMP)).shortValue());
                data.setIntMenSheetShubetsuFlg(getNewTestFlg());//[3] add 新テストフラグ（面談シート）
            }
            //面談シート１は受験学力測定テストNG
            if (MenSheetFlg == 5 && KNUtil.TYPECD_ABILITY.equals(iCommonMap.getTargetExamTypeCode())) {
                data.setIntMenSheetFlg(0);
            }
            //面談シート１の制限
            if (MenSheetFlg == 5 && !isAvailableSheet1()) {
                data.setIntMenSheetFlg(0);
            }
        }

        //個人共通
//		ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);//[4]

        /*下記条件の時に値を格納
        ・面談シートフラグ=2or4
        ・教科分析シートフラグ=1*/
        if (data.getIntMenSheetFlg() != 1 || data.getIntSeisekiFlg() == 1) {
            PreparedStatement	 ps2 = null;
            PreparedStatement	 ps3 = null;
            PreparedStatement	 ps4 = null;
            // 2019/08/23 Hics)Ueta 共通テスト対応 ADD START
            PreparedStatement    ps5 = null;
            // 2019/08/23 Hics)Ueta 共通テスト対応 ADD END
            ResultSet			 rs1 = null;
            ResultSet			 rs2 = null;
            ResultSet			 rs3 = null;
            ResultSet			 rs4 = null;
            // 2019/08/23 Hics)Ueta 共通テスト対応 ADD START
            ResultSet                    rs5 = null;

            // 2019/08/23 Hics)Ueta 共通テスト対応 ADD END

            //----------testData---------//
            String thisYear		 = super.getThisYear();			//現年度
            String examYear		 = iCommonMap.getTargetExamYear();	//模試年度
            String examCd		 = iCommonMap.getTargetExamCode();	//模試コード
            boolean gradeFlg		 = false;						//学年フラグ
            boolean typeFlg		 = false;						//模試種類フラグ
            if(examtype == 2) {typeFlg = true;}
            if(dispGrade == 1) {gradeFlg = true;}
            //----------testData---------//
            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
			// 2019/09/19 QQ)Tanouchi 共通テスト対応 ADD START
            //String cefr = "";
			// 2019/09/19 QQ)Tanouchi 共通テスト対応 ADD END
            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
            //１．全生徒
            for (java.util.Iterator it=data.getI11List().iterator(); it.hasNext();) {
                ExtI11ListBean printData = (ExtI11ListBean) it.next();

                // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD START
                // dataが処理中の帳票のデータでない場合(面談シートフラグが異なる場合)、処理しない
                if(!printData.getMenSheetFlg().equals(flag.toString())) continue;
                // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD END

                //[4] add start
                boolean printTypeFlg = false;//印刷タイプ（true:三年生＆現学年＆全て false:それ以外）
                List newTestList = new ArrayList();//新テストリスト
                //[4] add end

                //受験学力測定テスト
                final List abilities = new ArrayList();

                try{

                    String recordDataSql = null;

                    if(Integer.parseInt(studentGrade) >= 3 && gradeFlg && typeFlg == false) {		//studentGrade項番�Aで取得した生徒の学年
                        //学年＝３　＆＆　現学年 ＆＆　全ての種類
                        recordDataSql = recordDataSql(printData.getIndividualid(), thisYear, examYear, examCd);
                        data.setIntSeisekiFormatFlg(2);
                        printTypeFlg = true;//[4] add
                    } else {
                        //それ以外
                        recordDataSql = recordDataSql(printData.getIndividualid(), thisYear, examYear, examCd, gradeFlg, typeFlg);
                        data.setIntSeisekiFormatFlg(1);
                        printTypeFlg = false;//[4] add
                    }
                    //----------------------------生徒が変わったらＳＱＬ作り直し
                    ps2 = conn.prepareStatement(recordDataSql);
                    rs2 = ps2.executeQuery();
                    //２．全模試
                    while(rs2.next()) {
                        //模試情報----------------------------------------------
                        I13SeisekiListBean recordData = new I13SeisekiListBean();
                        recordData.setStrMshCd(rs2.getString("EXAMCD"));			//模試コード
                        recordData.setStrMshmei(rs2.getString("EXAMNAME_ABBR"));	//模試名
                        recordData.setStrMshDate(rs2.getString("INPLEDATE"));		//模試実施基準日

                        //模試種類コード
                        recordData.setStrMshTypeCd(rs2.getString("examtypecd"));

                        //----------------------------------------------模試情報
                        ps3 = conn.prepareStatement(courseDataSql());
                        ps3.setString( 1, examYear);
                        ps3.setString( 2, examCd);
                        ps3.setString( 3, printData.getIndividualid());
                        ps3.setString( 4, rs2.getString("EXAMYEAR"));
                        ps3.setString( 5, rs2.getString("EXAMCD"));
                        ps3.setString( 6, examYear);
                        ps3.setString( 7, examCd);
                        ps3.setString( 8, printData.getIndividualid());
                        ps3.setString( 9, rs2.getString("EXAMYEAR"));
                        ps3.setString(10, rs2.getString("EXAMCD"));
                        rs3 = ps3.executeQuery();

                        //全教科
                        while(rs3.next()) {
                            I13KyokaSeisekiListBean courseData = new I13KyokaSeisekiListBean();
                            courseData.setStrKyokaCd(rs3.getString("COURSECD"));
                            courseData.setStrKmkmei(rs3.getString("SUBADDRNAME"));
                            courseData.setStrHaitenKmk(rs3.getString("SUBALLOTPNT"));
                            // 2019/09/17 QQ)Tanouchi 共通テスト対応 ADD START
                            courseData.setSubCd(rs3.getString("SUBCD"));
                            if(!("3915").equals(courseData.getSubCd())) {
                            // 2019/09/17 QQ)Tanouchi 共通テスト対応 ADD END
                            	courseData.setIntTokuten(rs3.getInt("SCORE"));
                            // 2019/09/17 QQ)Tanouchi 共通テスト対応 ADD START
                            }else {
                            	if(rs3.getString("EXAMTYPECD").equals("01")) {
                            	KokugoKijutuScore = rs3.getString("KOKUGO_DESC_Q1_RATING")+rs3.getString("KOKUGO_DESC_Q2_RATING")+rs3.getString("KOKUGO_DESC_Q3_RATING")+rs3.getString("KOKUGO_DESC_COMP_RATING");
                            	courseData.setKokugoTokuten(KokugoKijutuScore);
                            	}
                            }
                            // 2019/09/17 QQ)Tanouchi 共通テスト対応 ADD END
                            courseData.setIntKansanTokuten(rs3.getInt("CVSSCORE"));
                            courseData.setFloHensa(rs3.getFloat("A_DEVIATION"));
                            courseData.setStrScholarLevel(GeneralUtil.toBlank((rs3.getString("ACADEMICLEVEL"))));
                            courseData.setIntScope(rs3.getInt("SCOPE"));
                            courseData.setBasicFlg(rs3.getString("BASICFLG"));

                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                            // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
//                            courseData.setExcdtype(rs3.getString("EXAMTYPECD"));
//                            cefr = cm.cefr(rs2.getString("EXAMYEAR"), rs2.getString("EXAMCD"), printData.getIndividualid(), conn);
//                            courseData.setCefrScore(cefr);
                            // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END
                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END

                            recordData.getI13KyokaSeisekiList().add(courseData);
                        }
                        if(rs3 != null) {rs3.close();}
                        if(ps3 != null) {ps3.close();}

                        ps4 = conn.prepareStatement(candidateDataSql());
                        ps4.setString(1, examYear);
                        ps4.setString(2, examCd);
                        ps4.setString(3, printData.getIndividualid());
                        ps4.setString(4, rs2.getString("EXAMYEAR"));
                        ps4.setString(5, rs2.getString("EXAMCD"));
                        ps4.setString(6, examYear);
                        ps4.setString(7, examCd);
                        ps4.setString(8, printData.getIndividualid());
                        ps4.setString(9, rs2.getString("EXAMYEAR"));
                        ps4.setString(10, rs2.getString("EXAMCD"));
                        rs4 = ps4.executeQuery();

                        //全志望大学
                        while(rs4.next()) {
                            I13HyoukaListBean univData = new I13HyoukaListBean();
                            univData.setStrShiboRank(rs4.getString("CANDIDATERANK"));
                            univData.setStrDaigakuMei(rs4.getString("UNIVNAME_ABBR"));
                            univData.setStrGakubuMei(rs4.getString("FACULTYNAME_ABBR"));
                            univData.setStrGakkaMei(rs4.getString("DEPTNAME_ABBR"));
                            univData.setIntTeiin(rs4.getInt("CAPACITY"));
                            univData.setStrTeiinRel(rs4.getString("EXAMCAPAREL"));
                            univData.setStrHyoukaMark(rs4.getString("CENTERRATING"));
                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
//                            univData.setStrHyoukaMark_engpt(rs4.getString("CENTERRATING_ENGPT"));
                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
                            univData.setStrHyoukaKijyutsu(rs4.getString("SECONDRATING"));
                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                            // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
//                            univData.setHyoukaJodge(rs4.getString("APP_QUAL_JUDGE"));
//                            univData.setHyoukaNotice(rs4.getString("APP_QUAL_NOTICE"));
                            // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END
                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
                            univData.setStrHyoukaAll(rs4.getString("TOTALRATING"));
                            recordData.getI13HyoukaList().add(univData);
                        }
                        if(rs4 != null) {rs4.close();}
                        if(ps4 != null) {ps4.close();}

                        //printData.getI13SeisekiList().add(recordData);//[4] delete
                        //[4] add start
                        final String NEWTEST_EXAMTYPECD = "31";
                        String typeCd = rs2.getString("EXAMTYPECD");

                        //受験学力測定
                        if (!printTypeFlg && KNUtil.TYPECD_ABILITY.equals(typeCd)) {
                            abilities.add(recordData);
                        //新テスト
                        } else if (!printTypeFlg && typeCd.equals(NEWTEST_EXAMTYPECD)) {
                            newTestList.add(recordData);
                        //その他
                        } else {
                            printData.getI13SeisekiList().add(recordData);
                        }
                        //[4] add end

                    }
                    if(ps2 != null) {ps2.close();}
                    if(rs2 != null) {rs2.close();}

                    //[4] add start 新テストリスト追加
                    if(newTestList.size() > 0) {
                        if(printData.getI13SeisekiList().size() != 0) {
                            printData.getI13SeisekiList().add(new I13SeisekiListBean());//空行
                        }
                        printData.getI13SeisekiList().addAll(newTestList);
                    }
                    //[4] add end

                    //受験学力測定が存在する
                    if (abilities.size() > 0) {
                        if (printData.getI13SeisekiList().size() != 0) {
                            //空を挿入して一行あける
                            printData.getI13SeisekiList().add(new I13SeisekiListBean());
                        }
                        printData.getI13SeisekiList().addAll(abilities);
                    }

                }finally{
                    if(ps2 != null) {ps2.close();}
                    if(ps3 != null) {ps3.close();}
                    if(ps4 != null) {ps4.close();}
                    // 2019/08/23 Hics)Ueta 共通テスト対応 ADD START
                    if(ps5 != null) {ps5.close();}
                    // 2019/08/23 Hics)Ueta 共通テスト対応 ADD END
                    if(rs1 != null) {rs1.close();}
                    if(rs2 != null) {rs2.close();}
                    if(rs3 != null) {rs3.close();}
                    if(rs4 != null) {rs4.close();}
                    // 2019/08/23 Hics)Ueta 共通テスト対応 ADD START
                    if(rs5 != null) {rs5.close();}
                    // 2019/08/23 Hics)Ueta 共通テスト対応 ADD END
                }
            }
        }
    }

    /*個人成績表*/
    //３年、現学年のみのとき
    private String recordDataSql(String individualId, String nowYear, String examYear, String examCd) {
        String DATAOPENDATE = null;
        if(!login.isHelpDesk()){
            //ヘルプデスク以外（外部データ開放日）
            DATAOPENDATE = "OUT_DATAOPENDATE";
        } else {
            //ヘルプデスク（内部データ開放日）
            DATAOPENDATE = "IN_DATAOPENDATE";
        }

        StringBuffer strBuf = new StringBuffer();
        strBuf.append("SELECT ");
        //strBuf.append("EX.EXAMYEAR, EX.EXAMCD, EX.EXAMNAME_ABBR, EX.INPLEDATE, EX.DISPSEQUENCE, ").append(DATAOPENDATE);//[4] delete
        strBuf.append("EX.EXAMYEAR, EX.EXAMCD, EX.EXAMNAME_ABBR, EX.INPLEDATE, EX.DISPSEQUENCE, ").append(DATAOPENDATE).append(", EX.EXAMTYPECD");//[4] add
        strBuf.append(" FROM ");
        strBuf.append("EXAMINATION EX ");
        strBuf.append("WHERE ");
        strBuf.append("EX.EXAMYEAR = '").append(nowYear).append("' ");//セッション（年度）
        strBuf.append("AND EX.EXAMTYPECD < '10' ");
        strBuf.append("AND EX.TERGETGRADE = 3 ");	//対象学年３年生

        strBuf.append("UNION ALL ");

        strBuf.append("SELECT ");
        strBuf.append(" /*+ INDEX(SI PK_SUBRECORD_I) */ ");//12/5 sql hint
        //strBuf.append("SI.EXAMYEAR, SI.EXAMCD, EX.EXAMNAME_ABBR, EX.INPLEDATE, EX.DISPSEQUENCE, ").append(DATAOPENDATE);//[4] delete
        strBuf.append("SI.EXAMYEAR, SI.EXAMCD, EX.EXAMNAME_ABBR, EX.INPLEDATE, EX.DISPSEQUENCE, ").append(DATAOPENDATE).append(", EX.EXAMTYPECD");//[4] add
        strBuf.append(" FROM ");
        strBuf.append("EXAMINATION EX ");
        strBuf.append("INNER JOIN ");
        strBuf.append("SUBRECORD_I SI ");
        strBuf.append("ON EX.EXAMYEAR = SI.EXAMYEAR AND EX.EXAMCD = SI.EXAMCD ");
        strBuf.append("WHERE ");
        strBuf.append("SI.INDIVIDUALID = '").append(individualId).append("' ");	//学籍履歴情報（個人ＩＤ）
        strBuf.append("AND SI.EXAMYEAR = '").append(nowYear).append("' ");		//セッション（現在の年度）

        strBuf.append("AND EX.").append(DATAOPENDATE).append(" <= (");
        strBuf.append("SELECT ").append(DATAOPENDATE).append(" FROM EXAMINATION ");
        strBuf.append("WHERE ");
        strBuf.append("EXAMYEAR = '").append(examYear).append("' ");//セッション（年度）
        strBuf.append("AND EXAMCD = '").append(examCd).append("' ");//セッション（コード）
        strBuf.append(") AND EX.EXAMTYPECD >= '10' ");
        strBuf.append("GROUP BY ");
        //strBuf.append("SI.EXAMYEAR, SI.EXAMCD, EX.EXAMNAME_ABBR, EX.INPLEDATE, EX.DISPSEQUENCE ,").append(DATAOPENDATE);//[4] delete
        strBuf.append("SI.EXAMYEAR, SI.EXAMCD, EX.EXAMNAME_ABBR, EX.INPLEDATE, EX.DISPSEQUENCE ,").append(DATAOPENDATE).append(", EX.EXAMTYPECD");//[4] add
        strBuf.append(" ORDER BY ");

        strBuf.append(" inpledate DESC");

        strBuf.append(",EXAMCD DESC ");//[1] add
        return strBuf.toString();
    }

    //それ以外
    private String recordDataSql(String individualId, String nowYear,
        String examYear, String examCd, boolean grade, boolean type) {

        StringBuffer strBuf = new StringBuffer();
        //----------連続個人成績表-----------//
        strBuf.append("SELECT ");
        strBuf.append(" /*+ INDEX(SI PK_SUBRECORD_I) */ ");//12/5 sql hint
        //strBuf.append("SI.EXAMYEAR, SI.EXAMCD, EX.EXAMNAME_ABBR, EX.INPLEDATE, EX.DISPSEQUENCE ");
        strBuf.append("SI.EXAMYEAR, SI.EXAMCD, EX.EXAMNAME_ABBR, EX.INPLEDATE, EX.DISPSEQUENCE, EX.EXAMTYPECD ");
        strBuf.append("FROM ");
        strBuf.append("EXAMINATION EX ");
        strBuf.append("INNER JOIN ");
        strBuf.append("SUBRECORD_I SI ");
        strBuf.append("ON EX.EXAMYEAR = SI.EXAMYEAR AND EX.EXAMCD = SI.EXAMCD, ");
        strBuf.append("(");
        strBuf.append("SELECT OUT_DATAOPENDATE, IN_DATAOPENDATE, EXAMTYPECD ");
        strBuf.append("FROM EXAMINATION ");
        strBuf.append("WHERE ");
        strBuf.append("EXAMYEAR = '").append(examYear).append("' ");// --セッション（年度）
        strBuf.append("AND EXAMCD = '").append(examCd).append("' ");// -- セッション（コード）
        strBuf.append(") A ");
        strBuf.append("WHERE ");
        strBuf.append("SI.INDIVIDUALID = '").append(individualId).append("' ");;//学籍履歴情報（個人ＩＤ）

        if(!login.isHelpDesk()) {
            //ヘルプデスク以外（外部データ開放日）
            strBuf.append("AND EX.OUT_DATAOPENDATE <= A.OUT_DATAOPENDATE ");
        } else {
            //ヘルプデスク（内部データ開放日）
            strBuf.append("AND EX.IN_DATAOPENDATE <= A.IN_DATAOPENDATE ");
        }
        if(grade) {
            //現学年のみ
            strBuf.append("AND SI.EXAMYEAR = '").append(nowYear).append("' ");//セッション（現年度）
        }
        if(type) {
            //同系統の模試
            strBuf.append("AND EX.EXAMTYPECD = A.EXAMTYPECD ");
        }

        if(!login.isHelpDesk()){
            //ヘルプデスク以外
            strBuf.append("GROUP BY ");
            //strBuf.append("SI.EXAMYEAR, SI.EXAMCD, EX.EXAMNAME_ABBR, EX.INPLEDATE, EX.DISPSEQUENCE, EX.OUT_DATAOPENDATE ");//delete
            strBuf.append("SI.EXAMYEAR, SI.EXAMCD, EX.EXAMNAME_ABBR, EX.INPLEDATE, EX.DISPSEQUENCE, EX.OUT_DATAOPENDATE, EX.EXAMTYPECD ");//add
        }else{
            //ヘルプデスク
            strBuf.append("GROUP BY ");
            //strBuf.append("SI.EXAMYEAR, SI.EXAMCD, EX.EXAMNAME_ABBR, EX.INPLEDATE, EX.DISPSEQUENCE, EX.IN_DATAOPENDATE ");//[4] delete
            strBuf.append("SI.EXAMYEAR, SI.EXAMCD, EX.EXAMNAME_ABBR, EX.INPLEDATE, EX.DISPSEQUENCE, EX.IN_DATAOPENDATE, EX.EXAMTYPECD ");//[4] add
        }

        strBuf.append(" ORDER BY");
        strBuf.append(" ex.inpledate DESC,");
        strBuf.append(" si.examcd DESC");

        return strBuf.toString();
    }

    /*教科データ*/
    private String courseDataSql() {
        String DATAOPENDATE = null;
        if(!login.isHelpDesk()){
            //ヘルプデスク以外（外部データ開放日）
            DATAOPENDATE = "OUT_DATAOPENDATE";
        } else {
            //ヘルプデスク（内部データ開放日）
            DATAOPENDATE = "IN_DATAOPENDATE";
        }
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("SELECT ");
        strBuf.append(" /*+ INDEX(SR_I PK_SUBRECORD_I) */ ");//12/5 sql hint
        strBuf.append("CO.COURSECD, ");
        // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
        strBuf.append("ES.SUBCD, "); // 科目コード
        strBuf.append("EX.EXAMTYPECD, "); // 模試種類コード
        // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END
        strBuf.append("ES.SUBADDRNAME, ");
        strBuf.append("ES.SUBALLOTPNT, ");
        strBuf.append("NVL(SR_I.SCORE, -999) SCORE, ");
        strBuf.append("NVL(SR_I.CVSSCORE, -999) CVSSCORE, ");
        strBuf.append("NVL(SR_I.A_DEVIATION, -999.0) A_DEVIATION, ");
        strBuf.append("SR_I.ACADEMICLEVEL ACADEMICLEVEL, ");
        strBuf.append("ES.DISPSEQUENCE, ");
        strBuf.append("CASE WHEN SR_I.EXAMCD IN ('01','02','03','04','38') THEN ");
        strBuf.append("    CASE WHEN SR_I.SCOPE=9 OR SR_I.SCOPE IS NULL THEN ES.DISPSEQUENCE ");
        strBuf.append("    ELSE CASE SUBSTR(SR_I.SUBCD,1,1) WHEN '4' THEN 4000 ");
        strBuf.append("                                     WHEN '5' THEN 5000 ");
        strBuf.append("         ELSE ES.DISPSEQUENCE ");
        strBuf.append("         END ");
        strBuf.append("    END ");
        strBuf.append("ELSE ES.DISPSEQUENCE ");
        strBuf.append("END DISP, ");
        strBuf.append("NVL(SR_I.SCOPE, -999.0) SCOPE, ");
        strBuf.append("GETCHARFLAGVALUE(ES.NOTDISPLAYFLG, 5) BASICFLG, ");
        // ADD START 2019/08/22 Hics)Ueta 共通テスト対応
        strBuf.append("TRIM(KD_I.KOKUGO_DESC_Q1_RATING) KOKUGO_DESC_Q1_RATING, ");
        strBuf.append("TRIM(KD_I.KOKUGO_DESC_Q2_RATING) KOKUGO_DESC_Q2_RATING, ");
        strBuf.append("TRIM(KD_I.KOKUGO_DESC_Q3_RATING) KOKUGO_DESC_Q3_RATING, ");
        strBuf.append("TRIM(KD_I.KOKUGO_DESC_COMP_RATING) KOKUGO_DESC_COMP_RATING ");
        // ADD END   2019/08/22 Hics)Ueta 共通テスト対応
        strBuf.append("FROM ");
        strBuf.append("COURSE CO ");//教科マスタ
        strBuf.append("LEFT JOIN ");
        strBuf.append("SUBRECORD_I SR_I "); //科目別成績
        strBuf.append("ON CO.YEAR = SR_I.EXAMYEAR ");
        strBuf.append("AND SUBSTR(CO.COURSECD, 0, 1) = SUBSTR(SR_I.SUBCD, 0, 1) ");//教科に対する科目
        // ADD START 2019/08/22 Hics)Ueta 共通テスト対応
        strBuf.append("LEFT JOIN ");
        strBuf.append("KOKUGO_DESC_I KD_I "); //国語記述式評価
        strBuf.append("ON KD_I.INDIVIDUALID = SR_I.INDIVIDUALID ");
        strBuf.append("AND KD_I.EXAMYEAR = SR_I.EXAMYEAR ");
        strBuf.append("AND KD_I.EXAMCD = SR_I.EXAMCD ");
        strBuf.append("AND KD_I.SUBCD = SR_I.SUBCD ");
        // ADD END   2019/08/22 Hics)Ueta 共通テスト対応
        strBuf.append("INNER JOIN ");
        strBuf.append("EXAMSUBJECT ES ");//模試科目
        strBuf.append("ON ES.EXAMYEAR = SR_I.EXAMYEAR ");
        strBuf.append("AND ES.EXAMCD = SR_I.EXAMCD ");
        strBuf.append("AND ES.SUBCD = SR_I.SUBCD ");
        strBuf.append("AND GETCHARFLAGVALUE(ES.NOTDISPLAYFLG, 3) = '0' ");
        strBuf.append("INNER JOIN ");
        strBuf.append("EXAMINATION EX ");//模試マスタ
        strBuf.append("ON EX.EXAMYEAR = SR_I.EXAMYEAR ");
        strBuf.append("AND EX.EXAMCD = SR_I.EXAMCD ");
        strBuf.append("AND EX.").append(DATAOPENDATE).append(" <= (SELECT ").append(DATAOPENDATE).append(" FROM EXAMINATION WHERE EXAMYEAR = ? AND EXAMCD = ?) ");
        strBuf.append("WHERE ");

        //kondo 1228 修正 (小論文の取得) start
        //strBuf.append("CO.COURSECD <= '5000' ");//５教科
        strBuf.append("CO.COURSECD <= '6000' ");//５教科＋小論文
        //kondo 1228 修正 (小論文の取得) end

        strBuf.append("AND SR_I.INDIVIDUALID = ? ");	//項番２（個人ＩＤ）
        strBuf.append("AND SR_I.EXAMYEAR = ? ");			//項番３（模試年度）
        strBuf.append("AND SR_I.EXAMCD = ? ");				//項番３（模試コード）

        //kondo 1228 修正 (小論文の取得) start
        //strBuf.append("AND SR_I.SUBCD < '6000'");//５教科の科目
        strBuf.append("AND SR_I.SUBCD < '7000'");//５教科の科目＋小論文
        //kondo 1228 修正 (小論文の取得) end

        strBuf.append("UNION ALL ");
        strBuf.append("SELECT ");
        strBuf.append(" /*+ INDEX(SR_I PK_SUBRECORD_I) */ ");//12/5 sql hint
        strBuf.append("CO.COURSECD, ");
        // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
        strBuf.append("ES.SUBCD, "); // 科目コード
        strBuf.append("EX.EXAMTYPECD, "); // 模試種類コード
        // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END
        strBuf.append("ES.SUBADDRNAME, ");
        strBuf.append("ES.SUBALLOTPNT, ");
        strBuf.append("NVL(SR_I.SCORE, -999) SCORE, ");
        strBuf.append("NVL(SR_I.CVSSCORE, -999) CVSSCORE, ");
        strBuf.append("NVL(SR_I.A_DEVIATION, -999.0) A_DEVIATION, ");
        strBuf.append("SR_I.ACADEMICLEVEL, ");
        strBuf.append("ES.DISPSEQUENCE, ");
        strBuf.append("ES.DISPSEQUENCE DISP, ");
        strBuf.append("NVL(SR_I.SCOPE,-999.0) SCOPE, ");
        strBuf.append("NULL BASICFLG, ");
        // ADD START 2019/08/22 Hics)Ueta 共通テスト対応
        strBuf.append("TRIM(KD_I.KOKUGO_DESC_Q1_RATING) KOKUGO_DESC_Q1_RATING, ");
        strBuf.append("TRIM(KD_I.KOKUGO_DESC_Q2_RATING) KOKUGO_DESC_Q2_RATING, ");
        strBuf.append("TRIM(KD_I.KOKUGO_DESC_Q3_RATING) KOKUGO_DESC_Q3_RATING, ");
        strBuf.append("TRIM(KD_I.KOKUGO_DESC_COMP_RATING) KOKUGO_DESC_COMP_RATING ");
        // ADD END   2019/08/22 Hics)Ueta 共通テスト対応
        strBuf.append("FROM ");
        strBuf.append("COURSE CO ");//教科マスタ
        strBuf.append("LEFT JOIN ");
        strBuf.append("SUBRECORD_I SR_I ");//科目別成績
        strBuf.append("ON CO.YEAR = SR_I.EXAMYEAR ");
        strBuf.append("AND SUBSTR(CO.COURSECD, 0, 1) = SUBSTR(SR_I.SUBCD, 0, 1) ");//教科に対する科目
        // ADD START 2019/08/22 Hics)Ueta 共通テスト対応
        strBuf.append("LEFT JOIN ");
        strBuf.append("KOKUGO_DESC_I KD_I "); //国語記述式評価
        strBuf.append("ON KD_I.INDIVIDUALID = SR_I.INDIVIDUALID ");
        strBuf.append("AND KD_I.EXAMYEAR = SR_I.EXAMYEAR ");
        strBuf.append("AND KD_I.EXAMCD = SR_I.EXAMCD ");
        strBuf.append("AND KD_I.SUBCD = SR_I.SUBCD ");
        // ADD END   2019/08/22 Hics)Ueta 共通テスト対応
        strBuf.append("INNER JOIN ");
        //strBuf.append("EXAMSUBJECT ES ");//模試科目マスタ//[5] delete
        strBuf.append("V_I_CMEXAMSUBJECT ES ");//科目View//[5] add
        strBuf.append("ON ES.EXAMYEAR = SR_I.EXAMYEAR ");
        strBuf.append("AND ES.EXAMCD = SR_I.EXAMCD ");
        strBuf.append("AND ES.SUBCD = SR_I.SUBCD ");
        strBuf.append("INNER JOIN ");
        strBuf.append("EXAMINATION EX ");//模試マスタ
        strBuf.append("ON EX.EXAMYEAR = SR_I.EXAMYEAR ");
        strBuf.append("AND EX.EXAMCD = SR_I.EXAMCD ");

        strBuf.append("AND EX.").append(DATAOPENDATE).append(" <= (SELECT ").append(DATAOPENDATE).append(" FROM EXAMINATION WHERE EXAMYEAR = ? AND EXAMCD = ?) ");

        strBuf.append("WHERE ");
        strBuf.append("SR_I.INDIVIDUALID = ? ");	//項番２（個人ＩＤ）
        strBuf.append("AND SR_I.EXAMYEAR = ? ");		//項番３（模試年度）
        strBuf.append("AND SR_I.EXAMCD = ? ");			//項番３（模試コード）
        strBuf.append("AND SR_I.SUBCD >= '7000' ");
        strBuf.append("AND SR_I.SUBCD < '8000' ");
        strBuf.append("ORDER BY ");
        strBuf.append("COURSECD ASC, BASICFLG ASC, DISP ASC, SCOPE DESC ");

        //strBuf.append("");
        //strBuf.append("").append().append("' ");

        return strBuf.toString();
    }

    /*志望校評価*/
    private String candidateDataSql() {
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("( ");
        strBuf.append("SELECT ");
        strBuf.append("/*+ INDEX(CR PK_CANDIDATERATING) */");//12/3インデックスヒント追加
        strBuf.append("CR.CANDIDATERANK, ");
        strBuf.append("HS.UNINAME_ABBR UNIVNAME_ABBR, ");
        strBuf.append("HS.FACULTYNAME_ABBR, ");
        strBuf.append("HS.DEPTNAME_ABBR, ");
        strBuf.append("CR.CENTERRATING, ");
        // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
        // 2019/08/27 Hics)Ueta 共通テスト対応 ADD START
//        strBuf.append("CR.CENTERRATING_ENGPT, ");
//        strBuf.append("CR.APP_QUAL_JUDGE, ");
//        strBuf.append("CR.APP_QUAL_NOTICE, ");
        // 2019/08/27 Hics)Ueta 共通テスト対応 ADD END
        // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
        strBuf.append("CR.SECONDRATING, ");
        strBuf.append("CR.TOTALRATING, ");
        strBuf.append("'8' AS EXAMCAPAREL, ");
        strBuf.append("-999 CAPACITY ");
        strBuf.append("FROM ");
        strBuf.append("EXAMINATION EX ");
        strBuf.append("INNER JOIN ");
        strBuf.append("CANDIDATERATING CR ");
        strBuf.append("ON EX.EXAMYEAR = CR.EXAMYEAR ");
        strBuf.append("AND EX.EXAMCD = CR.EXAMCD ");

        if (login.isHelpDesk()) {
            strBuf.append("AND EX.IN_DATAOPENDATE <= (SELECT IN_DATAOPENDATE FROM EXAMINATION WHERE EXAMYEAR = ? AND EXAMCD = ?) ");
        } else {
            strBuf.append("AND EX.OUT_DATAOPENDATE <= (SELECT OUT_DATAOPENDATE FROM EXAMINATION WHERE EXAMYEAR = ? AND EXAMCD = ?) ");
        }

        strBuf.append("AND EX.MASTERDIV = '1' ");
        strBuf.append("LEFT JOIN ");
        strBuf.append("H12_UNIVMASTER HS ");
        strBuf.append("ON HS.EVENTYEAR = CR.EXAMYEAR ");
        strBuf.append("AND HS.EXAMDIV = EX.EXAMDIV ");
        strBuf.append("AND HS.UNIVCD = CR.UNIVCD ");
        strBuf.append("AND HS.FACULTYCD = CR.FACULTYCD ");
        strBuf.append("AND HS.DEPTCD = CR.DEPTCD ");
        strBuf.append("WHERE ");
        strBuf.append("CR.INDIVIDUALID = ? ");
        strBuf.append("AND CR.EXAMYEAR = ? ");
        strBuf.append("AND CR.EXAMCD = ? ");
        strBuf.append(") UNION ALL ( ");
        strBuf.append("SELECT ");
        strBuf.append("/*+ INDEX(CR PK_CANDIDATERATING) */");//12/3 sql hint
        strBuf.append("CR.CANDIDATERANK, ");
        strBuf.append("HS.UNINAME_ABBR UNIVNAME_ABBR, ");
        strBuf.append("HS.FACULTYNAME_ABBR, ");
        strBuf.append("HS.DEPTNAME_ABBR, ");
        strBuf.append("CR.CENTERRATING, ");
        // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
        // 2019/08/27 Hics)Ueta 共通テスト対応 ADD START
//        strBuf.append("CR.CENTERRATING_ENGPT, ");
//        strBuf.append("CR.APP_QUAL_JUDGE, ");
//        strBuf.append("CR.APP_QUAL_NOTICE, ");
        // 2019/08/27 Hics)Ueta 共通テスト対応 ADD END
        // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
        strBuf.append("CR.SECONDRATING, ");
        strBuf.append("CR.TOTALRATING, ");
        strBuf.append("HS.EXAMCAPAREL, ");
        strBuf.append("NVL(HS.PUBENTEXAMCAPA, -999) CAPACITY ");
        strBuf.append("FROM ");
        strBuf.append("EXAMINATION EX ");
        strBuf.append("INNER JOIN ");
        strBuf.append("CANDIDATERATING CR ");
        strBuf.append("ON EX.EXAMYEAR = CR.EXAMYEAR ");
        strBuf.append("AND EX.EXAMCD = CR.EXAMCD ");

        if (login.isHelpDesk()) {
            strBuf.append("AND EX.IN_DATAOPENDATE <= (SELECT IN_DATAOPENDATE FROM EXAMINATION WHERE EXAMYEAR = ? AND EXAMCD = ?) ");
        } else {
            strBuf.append("AND EX.OUT_DATAOPENDATE <= (SELECT OUT_DATAOPENDATE FROM EXAMINATION WHERE EXAMYEAR = ? AND EXAMCD = ?) ");
        }

        strBuf.append("AND EX.MASTERDIV = '2' ");
        strBuf.append("LEFT JOIN ");
        strBuf.append("UNIVMASTER_BASIC HS ");
        strBuf.append("ON HS.EVENTYEAR = CR.EXAMYEAR ");
        strBuf.append("AND HS.EXAMDIV = EX.EXAMDIV ");
        strBuf.append("AND HS.UNIVCD = CR.UNIVCD ");
        strBuf.append("AND HS.FACULTYCD = CR.FACULTYCD ");
        strBuf.append("AND HS.DEPTCD = CR.DEPTCD ");
        strBuf.append("WHERE ");
        strBuf.append("CR.INDIVIDUALID = ? ");
        strBuf.append("AND CR.EXAMYEAR = ? ");
        strBuf.append("AND CR.EXAMCD = ? ");
        strBuf.append(") ");
        strBuf.append("ORDER BY ");
        strBuf.append("CANDIDATERANK ");

        return strBuf.toString();
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
     */
    protected String getCategory() {
        return IProfileCategory.I_SCORE_SHEET;
    }
	// 2019/09/19 QQ)Tanouchi 共通テスト対応 ADD START
    private CM cm = new CM();
	// 2019/09/19 QQ)Tanouchi 共通テスト対応 ADD END
}

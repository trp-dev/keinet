/*
 * 作成日: 2004/08/11
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.interfaces;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public interface ILogin {

	/**
	 * 高校・私塾の通常モード
	 */
	int SCHOOL_NORMAL = 11;

	/**
	 * 営業部　→　高校・私塾（契約校）への成り代わりモード
	 */
	int SALES_SCHOOL = 21;

	/**
	 * 営業部の通常モード
	 */
	int SALES_NORMAL = 22;

	/**
	 * 校舎の通常モード
	 */
	int CRAM_NORMAL = 33;

	/**
	 * ヘルプデスク　→　高校・私塾（契約校）への成り代わりモード
	 */
	int HELP_SCHOOL = 41;

	/**
	 * ヘルプデスク　→　営業部への成り代わりモード
	 */
	int HELP_SALES = 42;

	/**
	 * ヘルプデスク　→　校舎への成り代わりモード
	 */
	int HELP_CRAM = 43;

	/**
	 * ユーザ区分：私塾
	 */
	short USER_PREP = 3;

	/**
	 * ユーザ区分：教育委員会
	 */
	short USER_BOARD = 5;

	/**
	 * 契約区分：契約校
	 */
	short PACT_NORMAL = 1;

	/**
	 * 契約区分：非契約校
	 */
	short NO_PACT = 2;

	/**
	 * 契約区分：ヘルプデスク
	 */
	short PACT_HELP = 5;
}

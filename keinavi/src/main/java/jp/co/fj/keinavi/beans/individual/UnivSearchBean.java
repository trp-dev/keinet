/*
 * 作成日: 2004/08/19
 *
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.data.individual.NestedUnivData;
import jp.co.fj.keinavi.data.individual.UnivData;

import com.fjh.beans.DefaultSearchBean;

/**
 * @author Totec) T.Yamada
 *
 * 2005.3.10 	T.Yamada 	[1]学部・学科名が存在しないものは' '(空)を出力する
 * 2005.5.26	K.Kondo		[2]高３大学マスタのＳＱＬにインデックスを追加
 * 2005.6.14 	T.Yamada 	[3]SQLセキュリティ対策?を使用
 *
 * 2010.10.08	S Hase		[4]「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 * 　　　　             	   「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 *
 * 2016.01.15   Hiroyuki Nishiyama - QuiQsoft
 *              大規模改修：ソート順変更
 *
 */
public class UnivSearchBean extends DefaultSearchBean{

	/** 入力パラメター */
	private String hiraganaFull;
	private String univCd;
	private String initialCharFull;

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		if((hiraganaFull != null && !hiraganaFull.trim().equals(""))){
			executeByHiragana(hiraganaFull);
		}else if(univCd != null && !univCd.trim().equals("")){
			executeByUnivCd(univCd);
		}else if((initialCharFull != null && !initialCharFull.trim().equals(""))){
			executeByHiragana(initialCharFull);
		}else{
			//executeByAll("");//[3] del
			executeByAll("", "");//[3] add
		}
	}
	public int getNestedUnivListSize() throws Exception{
		return getNestedUnivList().size();
	}
	public List getNestedUnivList() throws Exception{
		List list = new ArrayList();
		try{
			String[] tempData = new String[3];//for comparing
			List rList = getRecordSet();
			Iterator ite = rList.iterator();
			while(ite.hasNext()){
				UnivData data = (UnivData)ite.next();

				//初めてのUNIV
				NestedUnivData univData;
				if(tempData[0] == null || !data.getUnivCd().equals(tempData[0]) || list.size() == 0){
					univData = new NestedUnivData();
					univData.setUnivCd(data.getUnivCd());
					univData.setUnivNameAbbr(data.getUnivNameAbbr());
					univData.setFacultyList(new ArrayList());
					list.add(univData);
				}
				univData = (NestedUnivData) list.get(list.size() -1 );

				//初めてのFACULTY
				NestedUnivData.FacultyData facultyData;
				if(tempData[1] == null || !data.getFacultyCd().equals(tempData[1]) || univData.getFacultyList().size() == 0){
					facultyData = univData.new FacultyData();
					facultyData.setFacultyCd(data.getFacultyCd());
					facultyData.setFacultyNameAbbr(data.getFacultyNameAbbr());
					facultyData.setDeptList(new ArrayList());
					univData.getFacultyList().add(facultyData);
				}
				facultyData = (NestedUnivData.FacultyData) univData.getFacultyList().get(univData.getFacultyList().size() - 1);

				//初めてのDEPT
				NestedUnivData.FacultyData.DeptData deptData;
				if(tempData[2] == null || !data.getDeptCd().equals(tempData[2]) || facultyData.getDeptList().size() == 0){
					deptData = facultyData.new DeptData();
					deptData.setDeptCd(data.getDeptCd());
					deptData.setDeptNameAbbr(data.getDeptNameAbbr());
					deptData.setSuperAbbrName(data.getSuperAbbrName());
					facultyData.getDeptList().add(deptData);
				}

				tempData[0] = data.getUnivCd();
				tempData[1] = data.getFacultyCd();
				tempData[2] = data.getDeptCd();
			}

		}catch(Exception e){
			throw e;
		}
		return list;
	}
	private void executeByHiragana(String hiraganaFull) throws SQLException, Exception{
		if(hiraganaFull == null) hiraganaFull = "";
		// 2016/01/15 QQ)Nishiyama 大規模改修 UPD START
		//executeByAll(" AND TO_SEARCHSTR(UNIVNAME_KANA) LIKE CONCAT(TO_SEARCHSTR(?), '%')", hiraganaFull);//[3] add
		executeByAll(" AND TO_SEARCHSTR(t1.UNIVNAME_KANA) LIKE CONCAT(TO_SEARCHSTR(?), '%')", hiraganaFull);//[3] add
		// 2016/01/15 QQ)Nishiyama 大規模改修 UPD END
		//executeByAll(" AND TO_SEARCHSTR(UNIVNAME_KANA) LIKE CONCAT(TO_SEARCHSTR('"+hiraganaFull+"'), '%')");//[3] del
	}
	private void executeByUnivCd(String univCd) throws SQLException, Exception{
		// 2016/01/15 QQ)Nishiyama 大規模改修 UPD START
		//executeByAll(" AND UNIVCD LIKE CONCAT(?, '%')", univCd);//[3] add
		executeByAll(" AND t1.UNIVCD LIKE CONCAT(?, '%')", univCd);//[3] add
		// 2016/01/15 QQ)Nishiyama 大規模改修 UPD END
		//executeByAll(" AND UNIVCD LIKE '"+univCd+"%'");//[3] del
	}
	//private void executeByAll(String queryOption) throws SQLException, Exception{//[3] del
	private void executeByAll(String queryOption, String option) throws SQLException, Exception{//[3] add

		StringBuffer query = new StringBuffer("");

		//query.append("SELECT DISTINCT UNIVCD, FACULTYCD, DEPTCD");//[2] delete
		// 2016/01/15 QQ)Nishiyama 大規模改修 DEL START
		//query.append("SELECT /*+ INDEX(UNIVMASTER_BASIC PK_UNIVMASTER_BASIC) */ DISTINCT UNIVCD, FACULTYCD, DEPTCD");//[2] add
		//query.append(",NVL(DEPTSORTKEY, ' ') DEPTSORTKEY");
		// 2016/01/15 QQ)Nishiyama 大規模改修 DEL END
		//query.append(" ,UNIVNAME_ABBR, FACULTYNAME_ABBR, DEPTNAME_ABBR, SUPERABBRNAME");//[1] del
		// 2016/01/15 QQ)Nishiyama 大規模改修 DEL START
		//query.append(" ,NVL(UNINAME_ABBR, ' ') UNIVNAME_ABBR, NVL(FACULTYNAME_ABBR, ' ') FACULTYNAME_ABBR, NVL(DEPTNAME_ABBR, ' ') DEPTNAME_ABBR, ' ' SUPERABBRNAME");//[1] add
		//query.append(" ,EVENTYEAR EXAMYEAR");
		//query.append(" FROM UNIVMASTER_BASIC WHERE 1 = 1");
		//query.append(queryOption);
		//query.append(" AND EVENTYEAR = (SELECT MAX(EVENTYEAR) FROM UNIVMASTER_BASIC)");
		//query.append(" AND EXAMDIV = (SELECT MAX(EXAMDIV) FROM UNIVMASTER_BASIC");
		//query.append(" WHERE EVENTYEAR = (SELECT MAX(EVENTYEAR) FROM UNIVMASTER_BASIC))");
		//query.append(" ORDER BY UNIVCD, FACULTYCD, DEPTCD");
		// 2016/01/15 QQ)Nishiyama 大規模改修 DEL END

		// 2016/01/15 QQ)Nishiyama 大規模改修 ADD START
		query.append(" SELECT ");
		query.append("   /*+ INDEX(UNIVMASTER_BASIC PK_UNIVMASTER_BASIC) */ ");
		query.append("    t1.UNIDIV");
		query.append("  , NVL(t2.KANA_NUM, ' ')         KANA_NUM");
		query.append("  , t1.UNIVCD");
		query.append("  , t1.FACULTYCD");
		query.append("  , t1.UNINIGHTDIV");
		query.append("  , t1.FACULTYCONCD");
		query.append("  , t1.UNIGDIV");
		query.append("  , t1.DEPTSERIAL_NO");
		query.append("  , t1.SCHEDULESYS");
		query.append("  , t1.SCHEDULESYSBRANCHCD");
		query.append("  , t1.DEPTNAME_KANA");
		query.append("  , t1.DEPTCD");
		query.append("  , NVL(t1.DEPTSORTKEY, ' ')      DEPTSORTKEY");
		query.append("  , NVL(t1.UNINAME_ABBR, ' ')     UNIVNAME_ABBR");
		query.append("  , NVL(t1.FACULTYNAME_ABBR, ' ') FACULTYNAME_ABBR");
		query.append("  , NVL(t1.DEPTNAME_ABBR, ' ')    DEPTNAME_ABBR");
		query.append("  , ' ' SUPERABBRNAME");
		query.append("  , t1.EVENTYEAR EXAMYEAR");
		query.append("  FROM UNIVMASTER_BASIC t1");
		query.append(" LEFT JOIN  ");
		query.append("       UNIVMASTER_INFO t2");
		query.append("    ON t1.eventyear = t2.eventyear");
		query.append("   AND t1.univcd    = t2.univcd");
		query.append(" WHERE 1 = 1");
		query.append(queryOption);
		query.append("   AND t1.EVENTYEAR = (SELECT MAX(EVENTYEAR)");
		query.append("                         FROM UNIVMASTER_BASIC)");
		query.append("   AND t1.EXAMDIV   = (SELECT MAX(EXAMDIV)");
		query.append("                         FROM UNIVMASTER_BASIC");
		query.append("                        WHERE EVENTYEAR = (SELECT MAX(EVENTYEAR) FROM UNIVMASTER_BASIC))");
		query.append(" ORDER BY");
		query.append("    DECODE(t1.UNIDIV, '01', '02', t1.UNIDIV)");
		query.append("  , KANA_NUM");
		query.append("  , t1.UNIVCD");
		query.append("  , t1.UNINIGHTDIV");
		query.append("  , t1.FACULTYCONCD");
		query.append("  , t1.UNIGDIV");
		query.append("  , t1.DEPTSERIAL_NO");
		query.append("  , t1.SCHEDULESYS");
		query.append("  , t1.SCHEDULESYSBRANCHCD");
		query.append("  , t1.DEPTNAME_KANA");
		query.append("  , t1.DEPTCD");
		// 2016/01/15 QQ)Nishiyama 大規模改修 ADD END

		//System.out.println(query.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement(query.toString());
			if(!queryOption.trim().equals("")) ps.setString(1, option);//[3] add
			rs = ps.executeQuery();
			List list = new ArrayList();
			while (rs.next()) {
				UnivData data = new UnivData();
				data.setUnivCd(rs.getString("UNIVCD"));
				data.setFacultyCd(rs.getString("FACULTYCD"));
				data.setDeptCd(rs.getString("DEPTCD"));
				data.setDeptSortKey(rs.getString("DEPTSORTKEY"));
				data.setUnivNameAbbr(rs.getString("UNIVNAME_ABBR"));
				data.setFacultyNameAbbr(rs.getString("FACULTYNAME_ABBR"));
				data.setDeptNameAbbr(rs.getString("DEPTNAME_ABBR"));
				data.setSuperAbbrName(rs.getString("SUPERABBRNAME"));
				recordSet.add(data);
			}
		}catch(Exception e){
			throw e;
		}finally{
			if(ps != null) ps.close();
			if(rs != null) rs.close();
		}

		//大学・学部・学科ソートキーでソート
		// 2016/01/15 QQ)Nishiyama 大規模改修 DEL START
		//if(recordSet != null && recordSet.size() > 0){
		//	Collections.sort(recordSet);
		//}
		// 2016/01/15 QQ)Nishiyama 大規模改修 DEL END
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

	/**
	 * @return
	 */
	public String getInitialCharFull() {
		return initialCharFull;
	}


	/**
	 * @param string
	 */
	public void setInitialCharFull(String string) {
		initialCharFull = string;
	}

	/**
	 * @return
	 */
	public String getHiraganaFull() {
		return hiraganaFull;
	}

	/**
	 * @param string
	 */
	public void setHiraganaFull(String string) {
		hiraganaFull = string;
	}

}

/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * @author Administrator
 *
 * 2005.4.26 	K.Kondo 	[1]画面スクロール座標の追加
 */
public class I003Form extends ActionForm{

	/**
     *
     */
    private static final long serialVersionUID = -6382500224295986219L;
    private String targetPersonId;
	private String targetExamYear;
	private String targetExamCode;
	private String targetExamName;
	private String targetGrade;
	private String scrollX;//[1] add サブミット前、表示座標Ｘ
	private String scrollY;//[1] add サブミット前、表示座標Ｙ
        private String[] interviewSheet;
        //面談用帳票
        private String[] interviewForms;
        //個人成績分析共通-印刷対象生徒（判定対象生徒)
        private String printStudent;
        //セキュリティスタンプ
        private String printStamp;
        //個人成績用
        private String report;
        //印刷ボタン押下時パラメータ
        private String printFlag;

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * @return
	 */
	public String getTargetExamName() {
		return targetExamName;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamName(String string) {
		targetExamName = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ

	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[1] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[1] add
	}

        /**
         * @return targetGrade を戻します。
         */
        public String getTargetGrade() {
                return targetGrade;
        }

        /**
         * @param pTargetGrade 設定する targetGrade。
         */
        public void setTargetGrade(String pTargetGrade) {
                targetGrade = pTargetGrade;
        }

        public String[] getInterviewSheet() {
            return interviewSheet;
        }

        public void setInterviewSheet(String[] interviewSheet) {
            this.interviewSheet = interviewSheet;
        }

        public String[] getInterviewForms() {
            return interviewForms;
        }

        public void setInterviewForms(String[] interviewForms) {
            this.interviewForms = interviewForms;
        }

        public String getPrintStudent() {
            return printStudent;
        }

        public void setPrintStudent(String printStudent) {
            this.printStudent = printStudent;
        }

        public String getPrintStamp() {
            return printStamp;
        }

        public void setPrintStamp(String printStamp) {
            this.printStamp = printStamp;
        }

        public String getReport() {
            return report;
        }

        public void setReport(String report) {
            this.report = report;
        }

        public String getPrintFlag() {
            return printFlag;
        }

        public void setPrintFlag(String printFlag) {
            this.printFlag = printFlag;
        }

}

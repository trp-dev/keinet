/**
 * 校内成績分析−クラス比較　偏差値分布-構成比グラフ（5.0pt）
 * 	Excelファイル編集
 * 作成日: 2004/08/10
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S32BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S32ClassListBean;
import jp.co.fj.keinavi.excel.data.school.S32Item;
import jp.co.fj.keinavi.excel.data.school.S32ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S32_09 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	final private int	intMaxSheetSr	= 40;			// 最大シート数	
	final private String	masterfile0		= "S32_09";		// ファイル名
	final private String	masterfile1		= "NS32_09";	// ファイル名
	private String	masterfile		= "";					// ファイル名
	/*
	 * 	Excel編集メイン
	 * 		S32Item s32Item: データクラス
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int s32_09EditExcel(S32Item s32Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("S32_09","S32_09帳票作成開始","");
		
		//テンプレートの決定
		if (s32Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			
			// データセット
			ArrayList	s32List			= s32Item.getS32List();
			Iterator	itr				= s32List.iterator();
			int		row				= 0;	// 列
			int 		setRow			= -1;	// *セット用
			float 		koseihi			= 0;	// *作成用
			int		writeCnt		= 0;	// 書き込みカウンター
			float		sumKoseihi		= 0;	// 人数計算用
			float		min				= 75.0f;// 偏差基準値
			int		classCnt		= 0;	// 値範囲：０〜９　(クラスカウンター)
			int		allClassCnt		= 0;	// 全クラスカウンター
			int		maxClassCnt		= 0;	// クラスカウンター
			int		maxSheetIndex	= 0;	// シートカウンター
			int		intBookCngCount	= 0;	// ブックカウンター
			boolean	bolFirstDataFlg	= true;// true:List.Next()を実行
			boolean	bolGakkouFlg	= true;// true:学校情報表示　（学校情報表示フラグ）
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
//add 2004/10/27 T.Sakai データ0件対応
			int		dispClassFlgCnt		= 0;		// グラフ表示フラグカウンタ
//add end
//add 2004/10/27 T.Sakai セル計算対応
			float		ruikeiKoseihi		= 0;		// 累計構成比
//add end

			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			// 基本ファイルを読込む
			S32ListBean s32ListBean = new S32ListBean();

			while( itr.hasNext() ) {
				s32ListBean = (S32ListBean)itr.next();

				// 科目データ表示処理
				if ( s32ListBean.getIntDispKmkFlg()==1 ) {

					// 基本ファイルを読み込む
					S32ClassListBean	s32ClassListBean	= new S32ClassListBean();
	
					//データの保持
					ArrayList	s32ClassList		= s32ListBean.getS32ClassList();
					Iterator	itrS32Class		= s32ClassList.iterator();

					// 表示クラス数取得
					maxClassCnt=0;
					while(itrS32Class.hasNext()){
						s32ClassListBean	= (S32ClassListBean) itrS32Class.next();
						if(s32ClassListBean.getIntDispClassFlg()==1){
							maxClassCnt++;
						}
					}
					itrS32Class		= s32ClassList.iterator();
	
					bolFirstDataFlg=true;
	
					//　クラスデータセット
					classCnt = 0;
					allClassCnt = 0;
					while(itrS32Class.hasNext()){
	
						if( bolBookCngFlg == true ){
							// マスタExcel読み込み
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook==null ){
								return errfread;
							}
							bolSheetCngFlg	= true;
							bolBookCngFlg	= false;
							maxSheetIndex	= 0;
						}
	
						if(bolSheetCngFlg){
							// データセットするシートの選択
							workSheet = workbook.getSheet(Integer.toString(maxSheetIndex+1));
							maxSheetIndex++;
	
							// ヘッダ右側に帳票作成日時を表示する
							cm.setHeader(workbook, workSheet);
	
							// セキュリティスタンプセット
							String secuFlg = cm.setSecurity( workbook, workSheet, s32Item.getIntSecuFlg() ,32 ,33 );
							workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
							workCell.setCellValue(secuFlg);
	
							// 学校名セット
							workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
							workCell.setCellValue( "学校名　　　：" + cm.toString(s32Item.getStrGakkomei()) );
	
							// 対象模試セット
							String moshi =cm.setTaisyouMoshi( s32Item.getStrMshDate() );	// 模試月取得
							workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
							workCell.setCellValue( cm.getTargetExamLabel() + "　　：" + cm.toString(s32Item.getStrMshmei()) + moshi);
	
							// 型名・配点セット
							String haiten = "";
							if ( !cm.toString(s32ListBean.getStrHaitenKmk()).equals("") ) {
								haiten = "（" + s32ListBean.getStrHaitenKmk() + "）";
							}
							workCell = cm.setCell( workSheet, workRow, workCell, 6, 0 );
							workCell.setCellValue( "型・科目：" + cm.toString(s32ListBean.getStrKmkmei()) + haiten );
	
							bolSheetCngFlg=false;
							bolGakkouFlg=true;
						}
	
						if(bolGakkouFlg){
							s32ClassListBean	= (S32ClassListBean) s32ClassList.get(0);
							if(bolFirstDataFlg){
								s32ClassListBean	= (S32ClassListBean) itrS32Class.next();
								bolFirstDataFlg=false;
							}
						}else{
							s32ClassListBean	= (S32ClassListBean) itrS32Class.next();
						}

						// クラスデータ表示フラグ判定
						if ( s32ClassListBean.getIntDispClassFlg()==1 ) {
//add 2004/10/27 T.Sakai データ0件対応
							dispClassFlgCnt++;
//add end
							S32BnpListBean s32BnpListBean = new S32BnpListBean();
							ArrayList	s32BnpList	= s32ClassListBean.getS32BnpList();
							Iterator	itrS32Bnp	= s32BnpList.iterator();
		
							// 偏差値データセット
							row=0;
							if (s32Item.getIntShubetsuFlg() == 1){
								min = 180.0f;
							} else{
								min = 75.0f;
							}
							writeCnt=0;
							sumKoseihi = 0;
//add 2004/10/27 T.Sakai セル計算対応
							ruikeiKoseihi = 0;
//add end
							
							while(itrS32Bnp.hasNext()){
								s32BnpListBean = (S32BnpListBean) itrS32Bnp.next();
		
								if(row==0){
									if(bolGakkouFlg){
										// 高校名セット
										workCell = cm.setCell( workSheet, workRow, workCell, 41, 1 );
										workCell.setCellValue( s32Item.getStrGakkomei() );
		
										bolGakkouFlg=false;
									}else{
										// クラス名セット
										workCell = cm.setCell( workSheet, workRow, workCell, 41, 1+3*classCnt );
										workCell.setCellValue( cm.toString(s32ClassListBean.getStrGrade())+"年 "+cm.toString(s32ClassListBean.getStrClass())+"クラス" );
									}
		
									// 合計人数セット
									if(s32ClassListBean.getIntNinzu()!=-999){
										workCell = cm.setCell( workSheet, workRow, workCell, 53, 1+3*classCnt );
										workCell.setCellValue( s32ClassListBean.getIntNinzu() );
									}
		
									// 平均点セット
									if(s32ClassListBean.getFloHeikin()!=-999.0){
										workCell = cm.setCell( workSheet, workRow, workCell, 54, 1+3*classCnt );
										workCell.setCellValue( s32ClassListBean.getFloHeikin() );
									}
		
									// 平均偏差値セット
									if(s32ClassListBean.getFloHensa()!=-999.0){
										workCell = cm.setCell( workSheet, workRow, workCell, 55, 1+3*classCnt );
										workCell.setCellValue( s32ClassListBean.getFloHensa() );
									}
								}
			
								// 人数セット
								if ( s32BnpListBean.getFloBnpMin()==min ) {
									if ( s32BnpListBean.getFloKoseihi() != -999.0 ) {
										sumKoseihi = sumKoseihi + s32BnpListBean.getFloKoseihi();
									}
//add 2004/10/27 T.Sakai セル計算対応
									// 累計構成比セット
									workCell = cm.setCell( workSheet, workRow, workCell, 43+writeCnt, 3+3*classCnt  );
									ruikeiKoseihi = ruikeiKoseihi + sumKoseihi;
									workCell.setCellValue( ruikeiKoseihi );

									// 得点用のときは処理しない
									if (s32Item.getIntShubetsuFlg() == 1){
									} else{
										if (s32BnpListBean.getFloBnpMin()==60.0f) {
											// 偏差値60以上の構成比セット
											workCell = cm.setCell( workSheet, workRow, workCell, 56, 1+3*classCnt );
											workCell.setCellValue( ruikeiKoseihi );
										}
										if (s32BnpListBean.getFloBnpMin()==50.0f) {
											// 偏差値50以上の構成比セット
											workCell = cm.setCell( workSheet, workRow, workCell, 57, 1+3*classCnt );
											workCell.setCellValue( ruikeiKoseihi );
										}
									}
//add end
									if ( sumKoseihi != 0 ) {
										workCell = cm.setCell( workSheet, workRow, workCell, 43+writeCnt, 2+3*classCnt  );
										workCell.setCellValue( sumKoseihi );
			
										// *セット準備
										if( sumKoseihi > koseihi ){
											koseihi = sumKoseihi;
											setRow = writeCnt;
										}
									}
									writeCnt++;
									sumKoseihi = 0;
									if (s32Item.getIntShubetsuFlg() == 1){
										min -= 20.0f;
									} else{
										min -= 5.0f;
									}
								} else {
									if ( s32BnpListBean.getFloKoseihi() != -999.0 ) {
										sumKoseihi = sumKoseihi + s32BnpListBean.getFloKoseihi();
									}
			
									// *セット
									if(s32BnpListBean.getFloBnpMin() == -999.0){
//add 2004/10/27 T.Sakai セル計算対応
										// 累計構成比セット
										workCell = cm.setCell( workSheet, workRow, workCell, 43+writeCnt, 3+3*classCnt  );
										ruikeiKoseihi = ruikeiKoseihi + sumKoseihi;
										workCell.setCellValue( ruikeiKoseihi );
//add end
										if ( sumKoseihi != 0 ) {
											workCell = cm.setCell( workSheet, workRow, workCell, 43+writeCnt, 2+3*classCnt  );
											workCell.setCellValue( sumKoseihi );
			
											// *セット準備
											if( sumKoseihi > koseihi ){
												koseihi = sumKoseihi;
												setRow = writeCnt;
											}
										}
										sumKoseihi = 0;
		

										if(setRow!=-1){
											workCell = cm.setCell( workSheet, workRow, workCell, 43+setRow, 1+3*classCnt  );
											workCell.setCellValue("*");
										}
										koseihi=0;
										setRow=-1;
									}
								}
		
								row++;
		
							}

							classCnt++;
							allClassCnt++;
	
							// 以降にクラス情報がなければループ脱出
							if(maxClassCnt==allClassCnt){
								break;
							}
	
							// データが存在する場合
							if(classCnt>=11){
								classCnt=0;
								allClassCnt--;
								bolSheetCngFlg=true;
							}

							// ファイルを閉じる処理
							if(bolSheetCngFlg){
								if((maxSheetIndex >= intMaxSheetSr)&&(itrS32Class.hasNext())){
									bolBookCngFlg = true;
								}
		
						
								if( bolBookCngFlg == true){
									boolean bolRet = false;
									// Excelファイル保存
									if(intBookCngCount==0){
										if(itr.hasNext() == true){
											bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
										}
										else{
											bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
										}
									}else{
										bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
									}
		
									if( bolRet == false ){
										return errfwrite;
									}
									intBookCngCount++;
								}
							}
						}

						// 学校が非表示指定(表示フラグ≠１)だった場合（エラー）
						bolGakkouFlg=false;
					}

					// ファイルを閉じる処理
					if(itr.hasNext()){
						classCnt=0;
						bolSheetCngFlg=true;
	
						if(maxSheetIndex >= intMaxSheetSr){
							bolBookCngFlg = true;
						}
	
					
						if( bolBookCngFlg == true){
	
							boolean bolRet = false;
							// Excelファイル保存
							if(intBookCngCount==0){
								if(itr.hasNext() == true){
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
								}
								else{
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
								}
							}else{
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
							}
	
							if( bolRet == false ){
								return errfwrite;
							}
							intBookCngCount++;
						}
					}
				}

			}
//add 2004/10/27 T.Sakai データ0件対応
			if ( dispClassFlgCnt==0 ) {
				// マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				bolSheetCngFlg	= true;
				bolBookCngFlg	= false;
				maxSheetIndex	= 0;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet( Integer.toString(maxSheetIndex+1) );
				maxSheetIndex++;
	
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
	
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s32Item.getIntSecuFlg() ,32 ,33 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
				workCell.setCellValue(secuFlg);
	
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　　　：" + cm.toString(s32Item.getStrGakkomei()) );
	
				// 対象模試セット
				String moshi =cm.setTaisyouMoshi( s32Item.getStrMshDate() );	// 模試月取得
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "　　：" + cm.toString(s32Item.getStrMshmei()) + moshi);
			}
//add end
			// ファイルを閉じる処理
			if( bolBookCngFlg == false){

				boolean bolRet = false;
				// Excelファイル保存
				if(intBookCngCount!=0){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}
			
		} catch(Exception e) {
			log.Err("S32_09","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S32_09","S32_09帳票作成終了","");
		return noerror;
	}

}
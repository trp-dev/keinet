/**
 * 校内成績分析−校内成績　偏差値分布（構成比無し）
 * 	Excelファイル編集
 * 作成日: 2004/07/07
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S12BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S12Item;
import jp.co.fj.keinavi.excel.data.school.S12ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S12_01 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
//	private String		strArea			= "A1:AK61";	//印刷範囲
	
	final private String masterfile0 = "S12_01";
	final private String masterfile1 = "S12_01";
	private String masterfile = "";
	final private int intMaxSheetSr = 49;	// 最大シート数-1の値を入れる

/*
 * 	Excel編集メイン
 * 		S12Item s12Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s12_01EditExcel(S12Item s12Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s12Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S12ListBean s12ListBean = new S12ListBean();
		
		try {
			int		row					= 0;		// 行
			int		col					= 0;		// 列
			int		ninzu				= 0;		// *作成用
			int		setRow				= 0;		// *セット用
			int		kmkCnt				= 0;		// 型・科目数カウンタ
			int		hyouCnt				= 0;		// 表カウンタ
			int		intMaxSheetIndex	= 0;		// シートカウンタ
			int		intBookCngCount		= 0;		// ファイルカウンタ
			boolean	bolSheetCngFlg		= true;	// 改シートフラグ
			boolean	bolBookCngFlg		= true;	// 改ファイルフラグ
			boolean	bolkmkCngFlg		= true;	// 型→科目切替わりフラグ
//add 2004/10/25 T.Sakai セル計算対応
			int		ruikeiNinzu			= 0;		// 累計人数
			int		ninzu60				= 0;		// 偏差値60以上の人数
			int		ninzu50				= 0;		// 偏差値50以上の人数
//add end

			// データセット
			ArrayList s12List = s12Item.getS12List();
			Iterator itr = s12List.iterator();
			
			while( itr.hasNext() ) {
				s12ListBean = (S12ListBean)itr.next();
				
				//型から科目に変わる時のチェック
				if ( s12ListBean.getStrKmkCd().compareTo("7000") < 0 ) {
					if (bolkmkCngFlg) {
						hyouCnt++;
						kmkCnt = 0;
						col = 1;
						bolkmkCngFlg = false;
					} else {
						if (kmkCnt>=12) {
							hyouCnt++;
							kmkCnt = 0;
							col = 1;
						}
					}
				} else {
					if (kmkCnt>=12) {
						hyouCnt++;
						kmkCnt = 0;
						col = 1;
					}
				}
				if (hyouCnt >= 2) {
					bolSheetCngFlg = true;
					if (bolSheetCngFlg && intMaxSheetIndex>intMaxSheetSr) {
						bolBookCngFlg = true;
					}
				}
				
				if (bolSheetCngFlg) {
					if (bolBookCngFlg) {
						// Excelファイル保存
						if (intMaxSheetIndex!=0) {
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
							intBookCngCount++;
							if( bolRet == false ){
								return errfwrite;					
							}
						}
						//マスタExcel読み込み
						workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
						if( workbook==null ){
							return errfread;
						}
						intMaxSheetIndex = 0;
					}
					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);
					intMaxSheetIndex++;

					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);
					
					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, s12Item.getIntSecuFlg() ,35 ,36 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 35 );
					workCell.setCellValue(secuFlg);
					
					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( "学校名　：" + cm.toString(s12Item.getStrGakkomei()) );
			
					// 模試月取得
					String moshi =cm.setTaisyouMoshi( s12Item.getStrMshDate() );
					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
					workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s12Item.getStrMshmei()) + moshi);

					bolSheetCngFlg = false;
					bolBookCngFlg = false;
					hyouCnt = 0;
					kmkCnt = 0;
					col = 1;
				}
				
				if ( hyouCnt==0 ){
					row = 4;
				} else {
					row = 33;
				}
				
				// 型・科目名セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
				workCell.setCellValue( s12ListBean.getStrKmkmei() );
				// 配点セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
				if ( !cm.toString(s12ListBean.getStrHaitenKmk()).equals("") ) {
					workCell.setCellValue( "（" + s12ListBean.getStrHaitenKmk() + "）" );
				}
				if ( hyouCnt==0 ) {
					row = 8;
				} else {
					row = 37;
				}
				
				// 基本ファイルを読込む
				S12BnpListBean s12BnpListBean = new S12BnpListBean();
				
				// 偏差値データセット
				ArrayList s12BnpList = s12ListBean.getS12BnpList();
				Iterator itrBnp = s12BnpList.iterator();
				setRow = 0;
//add 2004/10/25 T.Sakai セル計算対応
				ruikeiNinzu = 0;
				ninzu60 = 0;
				ninzu50 = 0;
//add end
				
				while( itrBnp.hasNext() ) {
					s12BnpListBean = (S12BnpListBean)itrBnp.next();
//add 2004/10/25 T.Sakai セル計算対応
					// 累計人数セット
					workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
					if ( s12BnpListBean.getIntNinzu() != -999 ) {
						ruikeiNinzu = ruikeiNinzu + s12BnpListBean.getIntNinzu();
					}
					workCell.setCellValue( ruikeiNinzu );
					if (s12BnpListBean.getFloBnpMin()==60.0f) {
						ninzu60 = ruikeiNinzu;
					}
					if (s12BnpListBean.getFloBnpMin()==50.0f) {
						ninzu50 = ruikeiNinzu;
					}
//add end
					// 人数・校内セット
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col+1 );
					if ( s12BnpListBean.getIntNinzu() != -999 ) {
						workCell.setCellValue( s12BnpListBean.getIntNinzu() );
					}
					// *作成用
					if ( ninzu < s12BnpListBean.getIntNinzu() ) {
						ninzu = s12BnpListBean.getIntNinzu();
						setRow = row - 1;
					}
				}
				// *セット
				workCell = cm.setCell( workSheet, workRow, workCell, setRow, col );
				if ( setRow!=0 ) {
					workCell.setCellValue("*");
				}
				if ( hyouCnt==0 ) {
					row = 27;
				} else {
					row = 56;
				}
				// 合計人数セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
				if ( s12ListBean.getIntNinzu() != -999 ) {
					workCell.setCellValue( s12ListBean.getIntNinzu() );
				}
				// 平均点（高校）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
				if ( s12ListBean.getFloHeikin() != -999.0 ) {
					workCell.setCellValue( s12ListBean.getFloHeikin() );
				}
				// 平均偏差値（高校）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
				if ( s12ListBean.getFloHensa() != -999.0 ) {
					workCell.setCellValue( s12ListBean.getFloHensa() );
				}
//add 2004/10/25 T.Sakai セル計算対応
				// 偏差値60.0以上の人数セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
				workCell.setCellValue( ninzu60 );
				// 偏差値50.0以上の人数セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
				workCell.setCellValue( ninzu50 );
//add end
				kmkCnt++;
				col = col + 3;
				ninzu = 0;
			}
//add 2004/10/25 T.Sakai データ0件対応
			if ( s12List.size()==0 ) {
				//マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				intMaxSheetIndex = 0;
				// シートテンプレートのコピー
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex++;
				
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s12Item.getIntSecuFlg() ,35 ,36 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 35 );
				workCell.setCellValue(secuFlg);
				
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s12Item.getStrGakkomei()) );
				
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s12Item.getStrMshDate() );
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s12Item.getStrMshmei()) + moshi);
			}
//add end
			// Excelファイル保存
			boolean bolRet = false;
			if(intBookCngCount != 0){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}
			
			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("S12_01","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

}

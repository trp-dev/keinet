/**
 * 校内成績分析−校内成績　成績概況
 * 	Excelファイル編集
 * 作成日: 2004/07/05
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.io.*;
import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.excel.cm.*;

import org.apache.poi.hssf.usermodel.*;
import jp.co.fj.keinavi.util.log.*;

public class S11_02 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
//	private String		strArea			= "A1:V91";	//印刷範囲	
	
	final private String masterfile0 = "S11_02";
	final private String masterfile1 = "S11_02";
	private String masterfile = "";
	final private int intMaxSheetSr = 10;	// 最大シート数	

/*
 * 	Excel編集メイン
 * 		S11Item s11Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s11_02EditExcel(S11Item s11Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		FileInputStream	fin			= null;
		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s11Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S11KataListBean s11KataListBean = new S11KataListBean();
		S11KmkListBean s11KmkListBean = new S11KmkListBean();

		try {
			int		col					= 0;	//型・科目用の列位置
			int		row					= 0;	//型・科目用の行位置
			int		colData				= 0;	//データ列位置
			int		ninzu				= 0;	//*作成用
			float		hensa				= 0;	//*作成用
			int		intMaxSheetIndex	= 1;	//シートカウンタ
			int		kmkCnt				= 0;	//型・科目数カウンタ
			int		hyouCnt				= 0;	//表カウンタ
			int		intBookCngCount		= 0;	//ファイルカウンタ
			boolean	bolSheetCngFlg	= true;	//改シートフラグ
			boolean	bolBookCngFlg	= true;	//改ファイルフラグ
			int		kataCnt				= 0;	//型数 2005/5/19 追加
			
			log.Ep("S11_02","型データセット開始","");
			// 型データセット
			ArrayList s11KataList = s11Item.getS11KataList();
			Iterator itrKata = s11KataList.iterator();
			
			//コメントの受験型取得
			while( itrKata.hasNext() ) {
				s11KataListBean = (S11KataListBean)itrKata.next();

				// *作成用 → 2005/03/16 科目ｺｰﾄﾞが7000番台を対象とする。
//				if ( !cm.toString(s11KataListBean.getStrHaitenKmk()).equals("") && ninzu < s11KataListBean.getIntNinzuHome() ) {
				if ( Integer.parseInt(s11KataListBean.getStrKmkCd()) < 8000 && ninzu < s11KataListBean.getIntNinzuHome() ) {
					ninzu = s11KataListBean.getIntNinzuHome();
					hensa = s11KataListBean.getFloHensaHome();
					s11Item.setStrComment( s11KataListBean.getStrKmkmei() );
				}
			}
			
			itrKata = s11KataList.iterator();
			
			while( itrKata.hasNext() ) {
				s11KataListBean = (S11KataListBean)itrKata.next();
				
				if (bolSheetCngFlg) {
					if (bolBookCngFlg) {
						// Excelファイル保存
						if (intMaxSheetIndex!=1) {
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex-1);
							intBookCngCount++;
							if( bolRet == false ){
								return errfwrite;					
							}
						}
						//マスタExcel読み込み
						workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
						if( workbook==null ){
							return errfread;
						}
						intMaxSheetIndex = 1;
					}
					// データセットするシートの選択
					workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex));
					intMaxSheetIndex++;
			
					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);
			
					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, s11Item.getIntSecuFlg() ,19 ,21 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 19 );
					workCell.setCellValue(secuFlg);
			
					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
					workCell.setCellValue( "学校名　：" + cm.toString(s11Item.getStrGakkomei()) );
			
					// 模試月取得＋対象模試セット
					String moshi =cm.setTaisyouMoshi( s11Item.getStrMshDate() );
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( "対象模試：" + cm.toString(s11Item.getStrMshmei()) + moshi);
			
					// 学校名ヘッダデータセット（上段）
					workCell = cm.setCell( workSheet, workRow, workCell, 36, 0 );
					workCell.setCellValue( s11Item.getStrGakkomei() );
			
					// 学校名ヘッダデータセット（下段）
					workCell = cm.setCell( workSheet, workRow, workCell, 82, 0 );
					workCell.setCellValue( s11Item.getStrGakkomei() );
			
					// 県名ヘッダデータセット（上段）
					workCell = cm.setCell( workSheet, workRow, workCell, 39, 0 );
					workCell.setCellValue( s11Item.getStrKenmei() );
			
					// 県名ヘッダデータセット（下段）
					workCell = cm.setCell( workSheet, workRow, workCell, 85, 0 );
					workCell.setCellValue( s11Item.getStrKenmei() );
					
					// コメントデータセット
					if ( !cm.toString(s11Item.getStrComment()).equals("") ) {
						// コメントデータセット（上段）
						workCell = cm.setCell( workSheet, workRow, workCell, 32, 10 );
						workCell.setCellValue( "※校内の受験人数が最も多い受験型「"+ s11Item.getStrComment() + "」の平均偏差値を");
						workCell = cm.setCell( workSheet, workRow, workCell, 33, 10 );
						workCell.setCellValue( "下回る科目の平均偏差値に\"*\"を表示しています。　 　　　　" );
						// コメントデータセット（下段）
						workCell = cm.setCell( workSheet, workRow, workCell, 78, 10 );
						workCell.setCellValue( "※校内の受験人数が最も多い受験型「"+ s11Item.getStrComment() + "」の平均偏差値を");
						workCell = cm.setCell( workSheet, workRow, workCell, 79, 10 );
						workCell.setCellValue( "下回る科目の平均偏差値に\"*\"を表示しています。　 　　　　" );
					}
					bolSheetCngFlg = false;
					bolBookCngFlg = false;
					hyouCnt = 0;
					kmkCnt = 0;
					col = 2;
					colData = 0;
				}
				if ( hyouCnt >= 1 ) {
					row = 80;
				} else {
					row = 34;
				}
				// 型名セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
				workCell.setCellValue( s11KataListBean.getStrKmkmei() );
				// 配点セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
				if ( !cm.toString(s11KataListBean.getStrHaitenKmk()).equals("") ) {
					workCell.setCellValue( "（" + s11KataListBean.getStrHaitenKmk() + "）" );
				}
				
				colData = col + 1;
				
				// 人数（高校）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KataListBean.getIntNinzuHome() != -999 ) {
					workCell.setCellValue( s11KataListBean.getIntNinzuHome() );
				}
				// 平均点（高校）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KataListBean.getFloHeikinHome() != -999.0 ) {
					workCell.setCellValue( s11KataListBean.getFloHeikinHome() );
				}
				// 平均偏差値（高校）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KataListBean.getFloHensaHome() != -999.0 ) {
					workCell.setCellValue( s11KataListBean.getFloHensaHome() );
				}
				// 人数（県）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KataListBean.getIntNinzuKen() != -999 ) {
					workCell.setCellValue( s11KataListBean.getIntNinzuKen() );
				}
				// 平均点（県）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KataListBean.getFloHeikinKen() != -999.0 ) {
					workCell.setCellValue( s11KataListBean.getFloHeikinKen() );
				}
				// 平均偏差値（県）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KataListBean.getFloHensaKen() != -999.0 ) {
					workCell.setCellValue( s11KataListBean.getFloHensaKen() );
				}
				// 人数（全国）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KataListBean.getIntNinzuAll() != -999 ) {
					workCell.setCellValue( s11KataListBean.getIntNinzuAll() );
				}
				// 平均点（全国）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KataListBean.getFloHeikinAll() != -999.0 ) {
					workCell.setCellValue( s11KataListBean.getFloHeikinAll() );
				}
				// 平均偏差値（全国）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KataListBean.getFloHensaAll() != -999.0 ) {
					workCell.setCellValue( s11KataListBean.getFloHensaAll() );
				}
				col = col + 2;
				kmkCnt++;
				kataCnt++;
				if (kmkCnt >= 10) {
					hyouCnt++;
					kmkCnt = 0;
					col = 2;
					colData = 0;
					if (hyouCnt >= 2) {
						bolSheetCngFlg = true;
						if (bolSheetCngFlg && intMaxSheetIndex>intMaxSheetSr) {
							bolBookCngFlg = true;
						}
					}
				}
			}

			// 型数が10のときは改段しない 2005/5/19 追加
			if (kataCnt == 10){
				hyouCnt--;
			}

			log.Ep("S11_02","型データセット終了","");

			
			log.Ep("S11_02","科目データセット開始","");
			// 科目データセット
			ArrayList s11KmkList = s11Item.getS11KmkList();
			Iterator itrKmk = s11KmkList.iterator();
			if (bolSheetCngFlg==false) {
				hyouCnt++;
				if (hyouCnt >= 2) {
					bolSheetCngFlg = true;
				}
			}
			kmkCnt = 0;
			col = 2;
			colData = 0;
			
			while( itrKmk.hasNext() ) {
				s11KmkListBean = (S11KmkListBean)itrKmk.next();
				
				if (bolSheetCngFlg) {
					if (bolBookCngFlg) {
						// Excelファイル保存
						if (intMaxSheetIndex!=1) {
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex-1);
							intBookCngCount++;
							if( bolRet == false ){
								return errfwrite;					
							}
						}
						//マスタExcel読み込み
						workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
						if( workbook==null ){
							return errfread;
						}
						intMaxSheetIndex = 1;
					}
					// データセットするシートの選択
					workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex));
					intMaxSheetIndex++;
			
					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);
			
					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, s11Item.getIntSecuFlg() ,19 ,21 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 19 );
					workCell.setCellValue(secuFlg);
			
					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
					workCell.setCellValue( "学校名　：" + cm.toString(s11Item.getStrGakkomei()) );
			
					// 模試月取得＋対象模試セット
					String moshi =cm.setTaisyouMoshi( s11Item.getStrMshDate() );
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( "対象模試：" + cm.toString(s11Item.getStrMshmei()) + moshi);
			
					// 学校名ヘッダデータセット（上段）
					workCell = cm.setCell( workSheet, workRow, workCell, 36, 0 );
					workCell.setCellValue( s11Item.getStrGakkomei() );
			
					// 学校名ヘッダデータセット（下段）
					workCell = cm.setCell( workSheet, workRow, workCell, 82, 0 );
					workCell.setCellValue( s11Item.getStrGakkomei() );
			
					// 県名ヘッダデータセット（上段）
					workCell = cm.setCell( workSheet, workRow, workCell, 39, 0 );
					workCell.setCellValue( s11Item.getStrKenmei() );
			
					// 県名ヘッダデータセット（下段）
					workCell = cm.setCell( workSheet, workRow, workCell, 85, 0 );
					workCell.setCellValue( s11Item.getStrKenmei() );
					
					// コメントデータセット
					if ( !cm.toString(s11Item.getStrComment()).equals("") ) {
						// コメントデータセット（上段）
						workCell = cm.setCell( workSheet, workRow, workCell, 32, 10 );
						workCell.setCellValue( "※校内の受験人数が最も多い受験型「"+ s11Item.getStrComment() + "」の平均偏差値を");
						workCell = cm.setCell( workSheet, workRow, workCell, 33, 10 );
						workCell.setCellValue( "下回る科目の平均偏差値に\"*\"を表示しています。　 　　　　" );
						// コメントデータセット（下段）
						workCell = cm.setCell( workSheet, workRow, workCell, 78, 10 );
						workCell.setCellValue( "※校内の受験人数が最も多い受験型「"+ s11Item.getStrComment() + "」の平均偏差値を");
						workCell = cm.setCell( workSheet, workRow, workCell, 79, 10 );
						workCell.setCellValue( "下回る科目の平均偏差値に\"*\"を表示しています。　 　　　　" );
					}
					bolSheetCngFlg = false;
					bolBookCngFlg = false;
					hyouCnt = 0;
					kmkCnt = 0;
					col = 2;
					colData = 0;
				}
				if ( hyouCnt >= 1 ) {
					row = 80;
				} else {
					row = 34;
				}
				// 科目名セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
				workCell.setCellValue( s11KmkListBean.getStrKmkmei() );
				// 配点セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
				if ( !cm.toString(s11KmkListBean.getStrHaitenKmk()).equals("") ) {
					workCell.setCellValue( "（" + s11KmkListBean.getStrHaitenKmk() + "）" );
				}
			
				colData = col + 1;
			
				// 人数（高校）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KmkListBean.getIntNinzuHome() != -999 ) {
					workCell.setCellValue( s11KmkListBean.getIntNinzuHome() );
				}
				// 平均点（高校）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KmkListBean.getFloHeikinHome() != -999.0 ) {
					workCell.setCellValue( s11KmkListBean.getFloHeikinHome() );
				}
				// 平均偏差値（高校）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KmkListBean.getFloHensaHome() != -999.0 ) {
					workCell.setCellValue( s11KmkListBean.getFloHensaHome() );
				}
				// *セット
				if ( hensa > s11KmkListBean.getFloHensaHome() ) {
					workCell = cm.setCell( workSheet, workRow, workCell, row-1, colData-1 );
					workCell.setCellValue("*");
				}
				// 人数（県）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KmkListBean.getIntNinzuKen() != -999 ) {
					workCell.setCellValue( s11KmkListBean.getIntNinzuKen() );
				}
				// 平均点（県）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KmkListBean.getFloHeikinKen() != -999.0 ) {
					workCell.setCellValue( s11KmkListBean.getFloHeikinKen() );
				}
				// 平均偏差値（県）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KmkListBean.getFloHensaKen() != -999.0 ) {
					workCell.setCellValue( s11KmkListBean.getFloHensaKen() );
				}
				// 人数（全国）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KmkListBean.getIntNinzuAll() != -999 ) {
					workCell.setCellValue( s11KmkListBean.getIntNinzuAll() );
				}
				// 平均点（全国）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KmkListBean.getFloHeikinAll() != -999.0 ) {
					workCell.setCellValue( s11KmkListBean.getFloHeikinAll() );
				}
				// 平均偏差値（全国）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, colData );
				if ( s11KmkListBean.getFloHensaAll() != -999.0 ) {
					workCell.setCellValue( s11KmkListBean.getFloHensaAll() );
				}
				col = col + 2;
				kmkCnt++;
				if (kmkCnt >= 10) {
					hyouCnt++;
					kmkCnt = 0;
					col = 2;
					colData = 0;
					if (hyouCnt >= 2) {
						bolSheetCngFlg = true;
						if (bolSheetCngFlg && intMaxSheetIndex>intMaxSheetSr) {
							bolBookCngFlg = true;
						}
					}
				}
			}
			log.Ep("S11_02","科目データセット終了","");
//add 2004/10/25 T.Sakai データ0件対応
			if ( s11KataList.size()==0 && s11KmkList.size()==0 ) {
				//マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				intMaxSheetIndex = 1;
				// データセットするシートの選択
				workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex));
				intMaxSheetIndex++;
				
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s11Item.getIntSecuFlg() ,19 ,21 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 19 );
				workCell.setCellValue(secuFlg);
				
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　：" + s11Item.getStrGakkomei() );
				
				// 模試月取得＋対象模試セット
				String moshi =cm.setTaisyouMoshi( s11Item.getStrMshDate() );
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "対象模試：" + s11Item.getStrMshmei() + moshi);
				
				// 学校名ヘッダデータセット（上段）
				workCell = cm.setCell( workSheet, workRow, workCell, 36, 0 );
				workCell.setCellValue( s11Item.getStrGakkomei() );
				
				// 学校名ヘッダデータセット（下段）
				workCell = cm.setCell( workSheet, workRow, workCell, 82, 0 );
				workCell.setCellValue( s11Item.getStrGakkomei() );
				
				// 県名ヘッダデータセット（上段）
				workCell = cm.setCell( workSheet, workRow, workCell, 39, 0 );
				workCell.setCellValue( s11Item.getStrKenmei() );
				
				// 県名ヘッダデータセット（下段）
				workCell = cm.setCell( workSheet, workRow, workCell, 85, 0 );
				workCell.setCellValue( s11Item.getStrKenmei() );
				
				// コメントデータセット
				if ( !cm.toString(s11Item.getStrComment()).equals("") ) {
					// コメントデータセット（上段）
					workCell = cm.setCell( workSheet, workRow, workCell, 32, 10 );
					workCell.setCellValue( "※校内の受験人数が最も多い受験型「"+ s11Item.getStrComment() + "」の平均偏差値を");
					workCell = cm.setCell( workSheet, workRow, workCell, 33, 10 );
					workCell.setCellValue( "下回る科目の平均偏差値に\"*\"を表示しています。　 　　　　" );
					// コメントデータセット（下段）
					workCell = cm.setCell( workSheet, workRow, workCell, 78, 10 );
					workCell.setCellValue( "※校内の受験人数が最も多い受験型「"+ s11Item.getStrComment() + "」の平均偏差値を");
					workCell = cm.setCell( workSheet, workRow, workCell, 79, 10 );
					workCell.setCellValue( "下回る科目の平均偏差値に\"*\"を表示しています。　 　　　　" );
				}
			}
//add end
			// Excelファイル保存
			boolean bolRet = false;
			if(intBookCngCount != 0){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex-1);
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex-1);
			}
			
			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("S11_02","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

}

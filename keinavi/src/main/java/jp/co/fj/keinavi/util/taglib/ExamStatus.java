/*
 * 作成日: 2004/07/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import jp.co.fj.keinavi.beans.exam.ExamFilterManager;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.BaseForm;

/**
 * 選択された模試によってイメージ名・背景色を出力するTaglib
 *
 * @author kawai
 */
public class ExamStatus extends TagSupport {

	private String screen; // 画面ID
	private String out; // 出力アイテム（image or color）

	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {

		try {
			// イメージ名
			if ("image".equals(out)) {
				pageContext.getOut().print(execute() ? "ora" : "gray");
			// イメージ名(指定されたもの以外)
				// 2019/09/11 QQ)Tanioka 背景色変更処理追加 ADD START
			} else if ("!image".equals(out)) {
					pageContext.getOut().print(execute() ? "gray" : "ora");
				// 2019/09/11 QQ)Tanioka 背景色変更処理追加 ADD END
			// 背景色
			} else if ("color".equals(out)) {
				pageContext.getOut().print(execute() ? "#F2BD74" : "#E6E6E6");
				// 2019/09/11 QQ)Tanioka 背景色変更処理追加 ADD START
			// 背景色(指定されたもの以外)
			} else if ("!color".equals(out)) {
				pageContext.getOut().print(execute() ? "#E6E6E6" : "#F2BD74");
				// 2019/09/11 QQ)Tanioka 背景色変更処理追加 ADD END
			// デフォルト（フラグ値を返す）
			} else {
				pageContext.getOut().print(execute() ? "1" : "0");
			}
		} catch (final IOException e) {
			throw new JspException(e);
		}

		return EVAL_PAGE;
	}


	private boolean execute() {

		// キャッシュを探す
		final Boolean result = (Boolean) pageContext.getAttribute(getKey());

		// 見つかった
		if (result != null) {
			return result.booleanValue();
		}

		// アクションフォーム
		final BaseForm form = (BaseForm) pageContext.getRequest(
				).getAttribute("form");
		// ログインセッション
		final LoginSession login = (LoginSession) pageContext.getSession(
				).getAttribute(LoginSession.SESSION_KEY);
		// 模試セッション
		final ExamSession examSession = (ExamSession) pageContext.getSession(
				).getAttribute(ExamSession.SESSION_KEY);
		// 模試データ
		final ExamData exam = examSession.getExamData(
				form.getTargetYear(), form.getTargetExam());

		// 判定
		boolean flag = ExamFilterManager.execute(
				screen, login, examSession, exam);
		// 結果はキャッシュする
		pageContext.setAttribute(getKey(), Boolean.valueOf(flag));

		return flag;
	}

	// キャッシュのキーを返す
	private String getKey() {
		return "_result_" + screen;
	}

	/**
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	public int doStartTag() throws JspException {
		return SKIP_BODY;
	}

	/**
	 * @param string
	 */
	public void setScreen(String string) {
		screen = string;
	}

	/**
	 * @param string
	 */
	public void setOut(String string) {
		out = string;
	}

}

package jp.co.fj.keinavi.beans.profile;

import java.io.ObjectOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.date.DateUtil;
import jp.co.fj.keinavi.util.date.ShortDateUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;
import oracle.sql.BLOB;

/**
 *
 *
 * 2005.10.12	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 *
 * @author kawai
 *
 */
public class ProfileUpdateBean extends DefaultBean {

    private Profile profile; // プロファイル
    private LoginSession loginSession; // ログイン情報
    private int overwriteMode; // 上書き禁止モード
    private boolean isStandard = false; // ひな型作成モードか

    private static final String SQL_PROFILE_ID =
        "SELECT "
        + "TO_CHAR(SYSDATE, 'YY') || TO_CHAR(profileidcount.nextval, 'FM00000') "
        + "FROM DUAL";

    /* (非 Javadoc)
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws SQLException, Exception {
        // 日付用のクラス
        DateUtil date = new ShortDateUtil();
        // 更新日
        profile.setUpdateDate(new Date());

        // IDがNULLなら新規
        if (profile.getProfileID() == null) {
            // 新規作成日
            profile.setCreateDate(profile.getUpdateDate());

            PreparedStatement ps = null;
            ResultSet rs = null;

            // IDを取得する
            try {
                ps = conn.prepareStatement(SQL_PROFILE_ID);
                rs = ps.executeQuery();
                while (rs.next()) {
                    profile.setProfileID(rs.getString(1));
                }
            } finally {
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(ps);
            }

            // INSERT
            try {
                ps = conn.prepareStatement(QueryLoader.getInstance().load("w02").toString());
                ps.setString(1, profile.getProfileID());
                ps.setString(2, profile.getProfileName());
                ps.setString(3, profile.getFolderID());
                ps.setString(4, profile.getComment());
                ps.setString(5, date.date2String(profile.getCreateDate()));
                ps.setString(6, date.date2String(profile.getUpdateDate()));
                ps.setString(7, String.valueOf(overwriteMode));
                ps.setString(8, profile.getUserID());
                ps.setString(9, profile.getBundleCD());
                ps.setString(10, profile.getSectorSortingCD());
                ps.execute();
            } finally {
                DbUtils.closeQuietly(ps);
            }

            // カテゴリマップをBLOBへ
            try {
                ps = conn.prepareStatement(
                    "INSERT INTO profileparam VALUES (?, empty_blob())");
                ps.setString(1, profile.getProfileID());
                ps.execute();
            } finally {
                DbUtils.closeQuietly(ps);
            }

            // BLOB取得
            try {
                ps = conn.prepareStatement(
                    "SELECT parameter FROM profileparam WHERE profileid = ?");
                ps.setString(1, profile.getProfileID());
                rs = ps.executeQuery();
                rs.next();
                BLOB blob = (BLOB) rs.getBlob(1);
                // serialize
                ObjectOutputStream oos = new ObjectOutputStream(blob.getBinaryOutputStream());
                oos.writeObject(profile.getCategoryMap());
                oos.close();
            } finally {
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(ps);
            }

            // フォルダは変更されているかもしれないので取り直す
            profile.setFolderName(getProfileFolderNanme(profile.getFolderID()));

            // 営業部の高校代行の場合、
            // ひな型プロファイルは無制限
            if (loginSession.isDeputy() && isStandard) {
                return;
            }

            // プロファイル数の制限値チェック（楽観的なロック）
            try {
                Query query = new Query("SELECT count(profileid) FROM profile ");

                // 100個になると別名保存できないので
                // ひな型なら制限値を減らす
                final int limit = loginSession.isDeputy() ? 20 : (isStandard ? 99 : 100);

                // 高校
                if (loginSession.isSchool()) {
                    query.append("WHERE bundlecd = ? AND sectorsortingcd IS NULL");
                    ps = conn.prepareStatement(query.toString());
                    ps.setString(1, profile.getBundleCD()); // 一括コード

                // 営業部または校舎
                } else if(loginSession.isBusiness()) {
                    query.append("WHERE sectorsortingcd = ? AND userid IS NULL ");
                    query.append("AND bundlecd IS NULL");

                    ps = conn.prepareStatement(query.toString());
                    ps.setString(1, loginSession.getSectorSortingCD()); // 部門分類コード

                // 営業部 → 高校
                } else if(loginSession.isDeputy()) {
                    query.append("WHERE bundlecd = ? AND sectorsortingcd = ?");
                    ps = conn.prepareStatement(query.toString());
                    ps.setString(1, profile.getBundleCD()); // 一括コード
                    ps.setString(2, loginSession.getSectorSortingCD()); // 部門分類コード
                }

                rs = ps.executeQuery();

                // 制限値を超えていたらエラー
                if (rs.next() && rs.getInt(1) > limit) {
                    KNServletException e =
                        new KNServletException("プロファイルの制限数を超えています。");
                    e.setErrorCode("1");
                    throw e;
                }
            } finally {
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(ps);
            }

        // そうでなければUPDATE
        } else {
            Query query = new Query();
            query.append("UPDATE profile ");
            query.append("SET profilename = ?, profilefid = ?, com = ?, ");
            query.append("renewaldate = ?, sectorsortingcd = ?, userid = ? ");
            query.append("WHERE profileid = ? ");

            // 営業 → 高校
            // 高校のプロファイルは更新できない
            if (loginSession.isDeputy()) {
                query.append("AND sectorsortingcd IS NOT NULL");
            }

            PreparedStatement ps1 = null;
            PreparedStatement ps2 = null;
            ResultSet rs = null;
            try {
                ps1 = conn.prepareStatement(query.toString());
                ps1.setString(1, profile.getProfileName());
                ps1.setString(2, profile.getFolderID());
                ps1.setString(3, profile.getComment());
                ps1.setString(4, date.date2String(profile.getUpdateDate()));
                ps1.setString(5, profile.getSectorSortingCD());
                ps1.setString(6, profile.getUserID());
                ps1.setString(7, profile.getProfileID());

                // 成功ならBLOBもUpdate
                if (ps1.executeUpdate() == 1) {
                    // BLOB取得
                    // SELECT FOR UPDATE しないといけない
                    ps2 = conn.prepareStatement(
                        "SELECT parameter FROM profileparam WHERE profileid = ? FOR UPDATE");
                    ps2.setString(1, profile.getProfileID());

                    rs = ps2.executeQuery();
                    if (rs.next()) {
                        BLOB blob = (BLOB) rs.getBlob(1);
                        ObjectOutputStream oos =
                            new ObjectOutputStream(blob.getBinaryOutputStream());
                        oos.writeObject(profile.getCategoryMap());
                        oos.close();
                    }

                    // 変更フラグを落とす
                    profile.setChanged(false);
                }
            } finally {
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(ps1);
                DbUtils.closeQuietly(ps2);
            }
        }
    }

    // プロファイルフォルダ名を取得する
    private String getProfileFolderNanme(final String id) throws SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(
                    "SELECT f.profilefname FROM profilefolder f "
                    + "WHERE f.profilefid = ?");
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            } else {
                return "";
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    /**
     * @param profile
     */
    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    /**
     * @param i
     */
    public void setOverwriteMode(int i) {
        overwriteMode = i;
    }

    /**
     * @param session
     */
    public void setLoginSession(LoginSession session) {
        loginSession = session;
    }

    /**
     * @param pIsStandard 設定する isStandard。
     */
    public void setStandard() {
        isStandard = true;
    }

}

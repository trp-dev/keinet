/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class IEasyPrintForm extends ActionForm{

	private String targetPersonId;

	private String meetingSheet;
	private String styleSheetAnalize;
	private String styleSheetTransit;
	private String styleSheetScore;
	private String scoreSheetCont;
	private String scoreSheetExam;
	private String examMaterialList;

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ
		
	}

	/**
	 * @return
	 */
	public String getExamMaterialList() {
		return examMaterialList;
	}

	/**
	 * @return
	 */
	public String getMeetingSheet() {
		return meetingSheet;
	}

	/**
	 * @return
	 */
	public String getScoreSheetCont() {
		return scoreSheetCont;
	}

	/**
	 * @return
	 */
	public String getScoreSheetExam() {
		return scoreSheetExam;
	}

	/**
	 * @return
	 */
	public String getStyleSheetAnalize() {
		return styleSheetAnalize;
	}

	/**
	 * @return
	 */
	public String getStyleSheetScore() {
		return styleSheetScore;
	}

	/**
	 * @return
	 */
	public String getStyleSheetTransit() {
		return styleSheetTransit;
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @param string
	 */
	public void setExamMaterialList(String string) {
		examMaterialList = string;
	}

	/**
	 * @param string
	 */
	public void setMeetingSheet(String string) {
		meetingSheet = string;
	}

	/**
	 * @param string
	 */
	public void setScoreSheetCont(String string) {
		scoreSheetCont = string;
	}

	/**
	 * @param string
	 */
	public void setScoreSheetExam(String string) {
		scoreSheetExam = string;
	}

	/**
	 * @param string
	 */
	public void setStyleSheetAnalize(String string) {
		styleSheetAnalize = string;
	}

	/**
	 * @param string
	 */
	public void setStyleSheetScore(String string) {
		styleSheetScore = string;
	}

	/**
	 * @param string
	 */
	public void setStyleSheetTransit(String string) {
		styleSheetTransit = string;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

}

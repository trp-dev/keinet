package jp.co.fj.keinavi.beans.login;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import jp.co.fj.keinavi.auth.KNAuthentication;
import jp.co.fj.keinavi.data.login.SectorData;
import jp.co.fj.keinavi.data.login.SectorSession;
import jp.co.fj.keinavi.interfaces.SpecialAppliMenu;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.message.MessageLoader;
import jp.co.fj.keinavi.util.sql.SQLUtil;
import jp.co.kcn.sb.servlet.common.Authentication;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 職員向けログイン認証Bean
 *
 * 2005.02.28 Yoshimoto KAWAI - Totec 個人単位での認証を追加
 *
 * 2010.04.06 Fujito URAKAWA - Totec 職員認証方式をSORPからRESTに変更
 *
 * @author kawai
 */
public class StaffLoginBean extends DefaultBean {

        /** serialVersionUID */
        private static final long serialVersionUID = -4512688649816855512L;

        /** ID */
        private String userID;

        /** パスワード */
        private String userPassword;

        /** 部門セッション */
        private final SectorSession sectorSession = new SectorSession();

        /** 利用可能メニューIDセット */
        private final Set menuIdSet = new HashSet();

        /**
         * @see com.fjh.beans.DefaultBean#execute()
         */
        public void execute() throws Exception {

                try {

                        // 認証クラス
                        KNAuthentication auth = new KNAuthentication(
                                        KNCommonProperty.getRestUrl());

                        // 職員認証１（Kei-Navi利用権限チェック）
                        Map result = auth.staffAuthentication(userID, userPassword, "44");
                        if (!"OK".equals(result.get(Authentication.AUTH_RESULT))) {
                                KNServletException e = new KNServletException(
                                                "職員認証に失敗しました。userID=" + userID);
                                e.setErrorCode("1");
                                e.setErrorMessage(MessageLoader.getInstance().getMessage(
                                                "w029a"));
                                throw e;
                        }

                        // セッションID１（Kei-Navi利用権限チェック）
                        String sessionId = (String) result.get(Authentication.SESSION_ID);

                        // 画面認証１（Kei-Navi利用権限チェック）
                        if (!"OK".equals(auth.validateCanUsingScreen(sessionId, "44", "01",
                                        "0101"))) {
                                KNServletException e = new KNServletException(
                                                "画面認証に失敗しました。userID=" + userID);
                                e.setErrorCode("1");
                                e.setErrorMessage(MessageLoader.getInstance().getMessage(
                                                "w030a"));
                                throw e;
                        }

                        // 経理部門コードを取得して部門セッションを作る
                        String[] code = auth.getAccountingSectionList(sessionId);
                        if (code.length == 0) {
                                KNServletException e = new KNServletException(
                                                "経理部門コードの取得に失敗しました。userID=" + userID);
                                e.setErrorCode("1");
                                e.setErrorMessage(MessageLoader.getInstance().getMessage(
                                                "w030a"));
                                throw e;
                        }

                        // 職員認証２（特例成績データ作成承認システム権限チェック）
                        Map result2 = auth.staffAuthentication(userID, userPassword, "94");
                        if ("OK".equals(result2.get(Authentication.AUTH_RESULT))) {

                                // セッションID２（特例成績データ作成承認システム権限チェック）
                                String sessionId2 = (String) result2.get(Authentication.SESSION_ID);

                                // 特例成績データ作成承認システムの利用可能メニューを判定する
                                for (Iterator ite = SpecialAppliMenu.LIST.iterator(); ite.hasNext();) {
                                        SpecialAppliMenu menu = (SpecialAppliMenu) ite.next();
                                        if ("OK".equals(auth.validateCanUsingScreen(sessionId2,
                                                        menu.gymcod, menu.mnucod, menu.gmnban))) {
                                                menuIdSet.add(menu.id);
                                        }
                                }
                        }




                        PreparedStatement ps = null;
                        ResultSet rs = null;
                        try {
                                StringBuffer query = new StringBuffer();
                                query.append("SELECT * FROM sectorsorting ");

                                query.append("WHERE sectorcd IN(");
                                query.append(SQLUtil.concatComma(code));
                                query.append(")");


                                ps = conn.prepareStatement(query.toString());
                                rs = ps.executeQuery();
                                while (rs.next()) {
                                        SectorData data = new SectorData();
                                        data.setSectorCD(rs.getString(1));
                                        data.setHsBusiDivCD(rs.getString(2));
                                        data.setSectorSortingCD(rs.getString(3));
                                        data.setSectorName(rs.getString(4));
                                        sectorSession.getSectorList().add(data);
                                }
                        } finally {
                                DbUtils.closeQuietly(null, ps, rs);
                        }

// ★↓テスト↓★
//                        // 特例成績データ作成承認システムの利用可能メニューを判定する
//                        for (Iterator ite = SpecialAppliMenu.LIST.iterator(); ite.hasNext();) {
//                                SpecialAppliMenu menu = (SpecialAppliMenu) ite.next();
//                                menuIdSet.add(menu.id);
//                        }
// ★↑テスト↑★


                } catch (KNServletException e) {
                        throw e;


                } catch (Exception e) {
                        e.printStackTrace();
                        KNServletException k = new KNServletException("職員認証に失敗しました。userID="
                                        + userID);
                        k.setErrorCode("1");
                        k.setErrorMessage("ＩＤまたはパスワードが違います。");
                        // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
                        throw k;
                        //throw new Exception(e);
                        // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
                }
        }

        /**
         * @param string
         */
        public void setUserPassword(String string) {
                userPassword = string;
        }

        /**
         * @param string
         */
        public void setUserID(String string) {
                userID = string;
        }

        /**
         * @return
         */
        public SectorSession getSectorSession() {
                return sectorSession;
        }

        /**
         * 利用可能メニューIDセットを返します。
         *
         * @return 利用可能メニューIDセット
         */
        public Set getMenuIdSet() {
                return menuIdSet;
        }

}

package jp.co.fj.keinavi.excel.personal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;

import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.beans.UnivDivBean;
import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.personal.I11Item;
import jp.co.fj.keinavi.excel.data.personal.I11ListBean;
import jp.co.fj.keinavi.excel.data.personal.I15CalendarListBean;
import jp.co.fj.keinavi.excel.data.personal.I15HanteiRirekiListBean;
import jp.co.fj.keinavi.excel.data.personal.I15HensaListBean;
import jp.co.fj.keinavi.excel.data.personal.I15HyoukaListBean;
import jp.co.fj.keinavi.excel.data.personal.I15KmkHensaListBean;
import jp.co.fj.keinavi.excel.data.personal.I15ScheduleListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * 個人成績分析　受験大学スケジュール表 Excelファイル編集
 *
 *
 * 2004.09.16   Y ITO - THS
 * 				新規作成
 *
 * <2010年度改修>
 * 2009.12.07   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 *
 * 2009.11.30   Tomohisa YAMADA - Totec
 *              5教科判定対応
 *
 * 2012.11.15   Tomohisa YAMADA - Totec
 *              判定履歴の模試名出力不具合対応
 *
 * @author Y ITO - THS
 * @version 1.0
 *
 *
 *
 */
public class I15_01 {

    private int	noerror		= 0;	// 正常終了
    private int	errfread	= 1;	// ファイルreadエラー
    private int	errfwrite	= 2;	// ファイルwriteエラー
    private int	errfdata	= 3;	// データ設定エラー

    private CM		cm			= new CM();	// 共通関数用クラス インスタンス

    //2005.04.26 Add 新テスト対応
    final private String 	masterfile0_t 	= "I15_01";	// ファイル名１(_01:先生用)(新テスト対応用)
    final private String 	masterfile1_t 	= "I15_01";	// ファイル名１(_01:先生用)(新テスト対応用)
    final private String 	masterfile0_s 	= "I15_02";	// ファイル名２(_02:生徒用)(新テスト対応用)
    final private String 	masterfile1_s 	= "I15_02";	// ファイル名２(_02:生徒用)(新テスト対応用)

    private String masterfile_t = "";
    private String masterfile_s = "";

    //センタ評価を表示している列番号
    private final int COL_JCENTER = 38;
    //二次評価を表示している列番号
    private final int COL_JSECOND = 40;


    final private String[] weekName		= {"日","月","火","水","木","金","土"};
    //2004.11.17 Update
    final private String	 centNtiCd		= "1";
    //UpdateEnd

    //2004.10.26 Add 2004.11.2 Update
    final private String[] HanteiCount	= {"A", "B", "C", "D", "E"};
    //Add end

    //センターボーダー得点率 26 >> 26
    final private static int COL_CENTER_BORDER = 26;
    //難易ランク偏差値 27 >> 28
    final private static int COL_RANK = 28;
    //地方試験地　28 >> 30
    final private static int COL_PLACE = 30;
    //出願締切日 29 >> 32
    final private static int COL_DEADLINE_APP = 32;
    //合格発表日 30 >> 34
    final private static int COL_DATE_ANN = 34;
    //入学手続締切日 31 >> 36
    final private static int COL_DEADLINE_PROC = 36;

    //模試：32 >> 38
    final private static int COL_EXAM_1 = 38;

    //履歴：34 >> 42
    final private static int COL_JUDGE_HISTORY = 42;

    final private static int MAX_COL_JUDGE_HISTORY = 54;

    //判定履歴の横セル数
    final private static int COL_COUNT_JUDGE_HISTORY = 2;

    //二次教科配点
    final private static int COL_COURSE_ALLOT = 56;
    //配点：備考 51 >>76
    final private static int COL_ALLOT_REMARK = 76;

/*
 * 	Excel編集メイン
 * 		I11Item i11Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
     public int i15_01EditExcel(I11Item i11Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
        KNLog log = KNLog.getInstance(null,null,null);

        //2005.04.26 Add 新テスト対応
        //テンプレートの決定
        if (i11Item.getIntHanteiShubetsuFlg() == 1){
            masterfile_t = masterfile1_t;
            masterfile_s = masterfile1_s;
        } else{
            masterfile_t = masterfile0_t;
            masterfile_s = masterfile0_s;
        }


        try {

            // データセット
            ArrayList	i11List			= i11Item.getI11List();
            Iterator	itr				= i11List.iterator();

            int	maxSheetIndex	= 0;	// シートカウンター
            int	lastdayOfMonth	= 0;
            int	firstWeekNum	= 0;

            //★★★★★ TOTEC T.Yamada WORKING ★★★★★★★★★ → 模試年度は使用しない
            //int	nendo			= Integer.parseInt(i11Item.getStrBaseMshDate().substring(0,4));//★ deleted
            //★★★★★★★★★★ add start
            //開始日が１・２・３で今月が１・２・３月以外なら「年+１」をする
            Calendar calendar = Calendar.getInstance( );
            int yearN = calendar.get(Calendar.YEAR);

            int thisMonth = calendar.get(Calendar.MONTH) + 1;

            if(thisMonth == 1 || thisMonth == 2 || thisMonth == 3 || thisMonth == 4){
                yearN --;
            }
            int nendo = yearN;
            //★★★★★★★★★★ add end

            String	msh				= "";
            boolean mshHyojiFlg	= false; //模試名表示フラグ 2004.11.20
            boolean rirekiExistFlg	= false; //判定履歴有無フラグ 2004.11.20

            // マスタExcel読み込み
            HSSFWorkbook	workbook	= null;
            HSSFSheet		workSheet	= null;
            HSSFRow			workRow		= null;
            HSSFCell		workCell	= null;

            // 基本ファイルを読込む
            I11ListBean i11ListBean = new I11ListBean();

            log.Ep("I15","基本データデータセット開始","");

            /** データリスト **/
            while( itr.hasNext() ) {
                i11ListBean = (I11ListBean) itr.next();

                // マスタExcel読み込み
                workbook = cm.getMasterWorkBook(masterfile_t, intSaveFlg);
                if( workbook==null ){
                    return errfread;
                }

                // シートテンプレートのコピー
                workSheet = workbook.cloneSheet(0);

                // ヘッダ右側に帳票作成日時を表示する
                cm.setHeader(workbook, workSheet);

                // セキュリティスタンプセット
                //2004.10.15 Update
                //2005.01.13 Update 表示列を＋２する
                String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntScheduleSecuFlg() ,78 ,81 );
                //Update End
                workCell = cm.setCell( workSheet, workRow, workCell, 0, 78 );
                workCell.setCellValue(secuFlg);

                // 学校名セット
                workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
                workCell.setCellValue( "学校名　：" + cm.toString(i11ListBean.getStrGakkomei()) + "　学年：" + cm.toString(i11ListBean.getStrGrade() )
                + "　クラス名：" + cm.toString(i11ListBean.getStrClass()) + "　クラス番号：" + cm.toString(i11ListBean.getStrClassNum()) );

                // 氏名・性別セット
                String strSex = "";
                if( cm.toString(i11ListBean.getStrSex()).equals("1") ){
                    strSex = "男";
                }else if( cm.toString(i11ListBean.getStrSex()).equals("2") ){
                    strSex = "女";
                }else{
                    strSex = "不明";
                }

                // カレンダー処理(曜日・2/29の書込)
                int intYear = nendo+1;	//2004.10.26 Add
                //2004.10.26 Update
                Calendar cald = Calendar.getInstance();
                cald.set(intYear,01,01);
                lastdayOfMonth = cald.getActualMaximum(Calendar.DAY_OF_MONTH);
                firstWeekNum = ((intYear-1) + (intYear-1)/4 - (intYear-1)/100 + (intYear-1)/400 + (26*14 + 16)/10 + 1) % 7;

                //Update End
                boolean bolUruuFlg = false;	//2004.11.10 Add
                if(lastdayOfMonth > 28){
                    workCell = cm.setCell( workSheet, workRow, workCell, 58, 0 );
                    workCell.setCellValue("2/29");
                    bolUruuFlg = true;
                }
                for(int a=0;a<lastdayOfMonth;a++ ){
                    workCell = cm.setCell( workSheet, workRow, workCell, 30+a, 1 );
                    workCell.setCellValue(weekName[((firstWeekNum+a)%7)]);
                }

                //２月以降の受験データ格納クラスを定義 → 2005/03/22 格納する処理の前に移動
                ArrayList mi15Calendar = new ArrayList();
                int cntKensu = 0;
                //2004.10.24 Update
                ArrayList no2jukenbiList = new ArrayList();
                //Update End
                ArrayList workjukenbiList = new ArrayList();	//2004.11.16 Add

                //2004.11.8 Add
                int intCntFebBefore = 0;
                int intCntFebAfter = 0;
                int intCntShidai = 0;
                //Add End

                //2004.10.26 Add
                int sdiSrtRow = 0;
                //AddEnd

                /** 受験大学スケジュール用データリスト **/
                // データセット
                ArrayList i15ScheduleList = i11ListBean.getI15ScheduleList();
                Iterator itrI15Schedule = i15ScheduleList.iterator();

                log.Ep("I15","受験大学スケジュール用データセット開始","");
                while(itrI15Schedule.hasNext()){
                    I15ScheduleListBean i15ScheduleListBean = (I15ScheduleListBean) itrI15Schedule.next();

                    /** 模試別偏差値データリスト **/
                    // データセット
                    ArrayList i115HensaList = i15ScheduleListBean.getI15HensaList();
                    Iterator itrI15Hensa = i115HensaList.iterator();

                    int mshRow = 0;

                    log.Ep("I15","受模試別偏差値データセット開始","");
                    while(itrI15Hensa.hasNext()){
                        I15HensaListBean i15HensaListBean = (I15HensaListBean) itrI15Hensa.next();

                        // 模試名
                        msh = cm.setTaisyouMoshi( i15HensaListBean.getStrMshDate() );
                        //2004.11.17 Update
                        if( i15HensaListBean.getStrMshmei()!=null){
                            workCell = cm.setCell( workSheet, workRow, workCell, 8+mshRow, 0 );
                            workCell.setCellValue( cm.toString(i15HensaListBean.getStrMshmei()) + msh );
                        }
                        //Update end

                        /** 教科別偏差値データリスト **/
                        // データセット
                        ArrayList i115KmkHensaList = i15HensaListBean.getI15KmkHensaList();
                        Iterator itrI15KmkHensa = i115KmkHensaList.iterator();

                        int kyoukaCol = 0;
                        int strtCol = 0 ;
                        int maxNum = 0 ;
                        //2004.10.25 Add
                        String kykcode = "";
                        String strHojiKykcode = "";
                        //Add End
                        boolean bolHyoujiFlg = false;
                        // 偏差値をプロットする型
                        I15KmkHensaListBean plot = null;

                        log.Ep("I15","教科別偏差値データセット開始","");
                        while(itrI15KmkHensa.hasNext()){
                            I15KmkHensaListBean i15KmkHensaListBean = (I15KmkHensaListBean) itrI15KmkHensa.next();
                            //2004.10.25 Add
                            kykcode = cm.toString(i15KmkHensaListBean.getStrKyokaCd());
                            if( kykcode.equals(strHojiKykcode) == false ){
                                bolHyoujiFlg = true;
                                kykcode = kykcode + " ";
                                String code = kykcode.substring(0, 1);
                                switch( Integer.parseInt(code) ){
                                    case 1:	//英語
                                        strtCol = 34;
                                        // 2005.01.13 maxNum=2 → 3
                                        maxNum = 3;
                                        break;
                                    case 2:	//数学
                                        //2005.01.13 strtCol=20 → 22
                                        strtCol = 46;
                                        maxNum = 3;
                                        break;
                                    case 3:	//国語
                                        //2005.01.13 strtCol=26 → 28
                                        strtCol = 58;
                                        maxNum = 1;
                                        break;
                                    case 4:	//理科
                                        //2005.01.13 strtCol=28 → 30
                                        strtCol = 62;
                                        maxNum = 3;
                                        break;
                                    case 5:	//地歴公民
                                        //2005.01.13 strtCol=34 → 36
                                        strtCol = 74;
                                        maxNum = 2;
                                        break;
                                    case 7:	//総合
                                        strtCol = 26;
                                        maxNum = 1;
                                        break;
                                    case 8:	//総合
                                        if( strHojiKykcode.equals("7") ){
                                            strtCol = 26;
                                            maxNum = 1;
                                        }
                                        else{
                                            strtCol = 30;
                                            maxNum = 1;
                                        }
                                        break;
                                    default:
                                        maxNum = 0;
                                        bolHyoujiFlg=false;
                                        break;

                                }
                                // 偏差値をプロットするのは最初の型
                                if (Integer.parseInt(code) >= 7
                                        && plot == null) {
                                    plot = i15KmkHensaListBean;
                                }
                                strHojiKykcode = cm.toString(i15KmkHensaListBean.getStrKyokaCd());
                                kyoukaCol=0;
                            }
                            //Add End
                            if(bolHyoujiFlg==true){
                                //理科の先頭科目が基礎科目なら、先頭は空欄にする
                                if (i15KmkHensaListBean.getStrKyokaCd().startsWith("4") && kyoukaCol == 0 && "1".equals(i15KmkHensaListBean.getBasicFlg())) {
                                    kyoukaCol++;
                                }

                                // 型・科目名
                                workCell = cm.setCell( workSheet, workRow, workCell, 8+mshRow, strtCol+4*kyoukaCol );
                                workCell.setCellValue( i15KmkHensaListBean.getStrKmkmei() );

                                // 偏差値セット
                                if( i15KmkHensaListBean.getFloHensa() != -999.0 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, 8+mshRow, strtCol+4*kyoukaCol+2 );
                                    workCell.setCellValue( i15KmkHensaListBean.getFloHensa() );
                                }

                                // 学力レベルセット
                                if( !"".equals(i15KmkHensaListBean.getStrScholarLevel()) ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, 8+mshRow, strtCol+4*kyoukaCol+3 );
                                    workCell.setCellValue( i15KmkHensaListBean.getStrScholarLevel() );
                                }

                            }

                            kyoukaCol++;
                            if( kyoukaCol >= maxNum ){
                                bolHyoujiFlg=false;
                            }
                        }

                         // 偏差値帯をプロットする
                        plotDeviation(plot, workSheet, 8 + mshRow);

                        log.Ep("I15","教科別偏差値データセット終了","");

                        mshRow++;
                        if( mshRow >= 7 ){
                            break;
                        }
                    }
                    log.Ep("I15","受模試別偏差値データセット終了","");

                    /** 受験大学別スケジュールリスト **/
                    // データセット
                    ArrayList i115CalendarList = i15ScheduleListBean.getI15CalendarList();
                    Iterator itrI15Calendar = i115CalendarList.iterator();

                    sdiSrtRow = 0;
                    int cntRow	= 0;
                    String beforeUNIVNAME = "";

                    log.Ep("I15","受験大学別スケジュールデータセット開始","");
                    while(itrI15Calendar.hasNext()){
                        I15CalendarListBean i15CalendarListBean = (I15CalendarListBean) itrI15Calendar.next();

                        // 日付処理
                        ArrayList jukenbiList = new ArrayList();
                        // 試験地処理
                        ArrayList ShikentiList = new ArrayList();

                        if(i15CalendarListBean.getStrJukenbi1()!=null){
                            jukenbiList.add(i15CalendarListBean.getStrJukenbi1());
                            ShikentiList.add(i15CalendarListBean.getStrShikenti1());
                        }
                        if(i15CalendarListBean.getStrJukenbi2()!=null){
                            jukenbiList.add(i15CalendarListBean.getStrJukenbi2());
                            ShikentiList.add(i15CalendarListBean.getStrShikenti2());
                        }
                        if(i15CalendarListBean.getStrJukenbi3()!=null){
                            jukenbiList.add(i15CalendarListBean.getStrJukenbi3());
                            ShikentiList.add(i15CalendarListBean.getStrShikenti3());
                        }
                        if(i15CalendarListBean.getStrJukenbi4()!=null){
                            jukenbiList.add(i15CalendarListBean.getStrJukenbi4());
                            ShikentiList.add(i15CalendarListBean.getStrShikenti4());
                        }
                        if(i15CalendarListBean.getStrJukenbi5()!=null){
                            jukenbiList.add(i15CalendarListBean.getStrJukenbi5());
                            ShikentiList.add(i15CalendarListBean.getStrShikenti5());
                        }
                        if(i15CalendarListBean.getStrJukenbi6()!=null){
                            jukenbiList.add(i15CalendarListBean.getStrJukenbi6());
                            ShikentiList.add(i15CalendarListBean.getStrShikenti6());
                        }
                        if(i15CalendarListBean.getStrJukenbi7()!=null){
                            jukenbiList.add(i15CalendarListBean.getStrJukenbi7());
                            ShikentiList.add(i15CalendarListBean.getStrShikenti7());
                        }
                        if(i15CalendarListBean.getStrJukenbi8()!=null){
                            jukenbiList.add(i15CalendarListBean.getStrJukenbi8());
                            ShikentiList.add(i15CalendarListBean.getStrShikenti8());
                        }
                        if(i15CalendarListBean.getStrJukenbi9()!=null){
                            jukenbiList.add(i15CalendarListBean.getStrJukenbi9());
                            ShikentiList.add(i15CalendarListBean.getStrShikenti9());
                        }
                        if(i15CalendarListBean.getStrJukenbi10()!=null){
                            jukenbiList.add(i15CalendarListBean.getStrJukenbi10());
                            ShikentiList.add(i15CalendarListBean.getStrShikenti10());
                        }
                        int hdkStrtRow = 0;

                        Iterator itrJukenbi = jukenbiList.iterator();
                        Iterator itrShikenti = ShikentiList.iterator();
                        while(itrJukenbi.hasNext()){
                            String jukenbi = (String) itrJukenbi.next();
                            if(jukenbi.substring(0,2).equals("02")){
                                hdkStrtRow = 29 + Integer.parseInt(jukenbi.substring(2,4));

                            }else{
                                if(lastdayOfMonth <= 28){
                                    hdkStrtRow = 58+cntRow;
                                }else{
                                    hdkStrtRow = 59+cntRow;
                                }
                                cntRow++;
                            }

                            //2004.11.8 Add
                            String strJuken = "";
                            if( Integer.parseInt(jukenbi.substring(0,2)) >= 1 && Integer.parseInt(jukenbi.substring(0,2)) <= 3 ){
                                strJuken = Integer.toString(nendo+1) + jukenbi;
                            }
                            else{
                                strJuken = Integer.toString(nendo) + jukenbi;
                            }

                            boolean bolSet = true;
                            int intListSize = workjukenbiList.size();
                            for( int a = 0; a < intListSize; a++ ){
                                String strWork = (String)workjukenbiList.get(a);
                                if( strJuken.equals(strWork) == true ){
                                    bolSet = false;
                                    break;
                                }
                            }
                            if(bolSet == true){
                                workjukenbiList.add(strJuken);
                            }
                            //Add End
                            //２月のみ処理
                            if(jukenbi.substring(0,2).equals("02")){

                                log.Ep("I15","２月分スケジュール処理開始","");

                                // 大学名セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 2 );
                                workCell.setCellValue( i15CalendarListBean.getStrDaigakuMei() );

                                // 学部名セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 3 );
                                workCell.setCellValue( i15CalendarListBean.getStrGakubuMei() );

                                // 学科名セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 4 );
                                workCell.setCellValue( i15CalendarListBean.getStrGakkaMei() );

                                // [複]セット
                                if( i15CalendarListBean.getIntPluralFlg() == 1 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 5 );
                                    workCell.setCellValue( "[複]" );
                                }

                                //地方試験地セット
                                String chihoSkn = (String) itrShikenti.next();

                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_PLACE );
                                workCell.setCellValue( chihoSkn );

                                // 出願締切セット
                                //2004.10.25 Add
                                String strDate = "";
                                if( cm.toString(i15CalendarListBean.getStrSyutsuganbi()).length() == 4 ){
                                    strDate = String.valueOf(Integer.parseInt(cm.toString(i15CalendarListBean.getStrSyutsuganbi()).substring(0,2)))
                                                + "/"
                                                + String.valueOf(Integer.parseInt(cm.toString(i15CalendarListBean.getStrSyutsuganbi()).substring(2)));
                                }
                                else{
                                    strDate = cm.toString(i15CalendarListBean.getStrSyutsuganbi());
                                }
                                //Add End
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DEADLINE_APP );
                                //2004.10.25 Update
                                workCell.setCellValue( strDate );
                                //Update End

                                // 合格発表セット
                                //2004.10.25 Add
                                strDate = "";
                                if( cm.toString(i15CalendarListBean.getStrGoukakubi()).length() == 4){
                                    strDate = String.valueOf(Integer.parseInt(cm.toString(i15CalendarListBean.getStrGoukakubi()).substring(0,2)))
                                            + "/"
                                            + String.valueOf(Integer.parseInt(cm.toString(i15CalendarListBean.getStrGoukakubi()).substring(2)));
                                }
                                else{
                                    strDate = cm.toString(i15CalendarListBean.getStrGoukakubi());
                                }
                                //Add End
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DATE_ANN );
                                //2004.10.25 Update
                                workCell.setCellValue( strDate );
                                //Update End

                                // 入学手続き締切日セット
                                //2004.10.25 Add
                                strDate = "";
                                if( cm.toString(i15CalendarListBean.getStrTetsudukibi()).length() == 4 ){
                                    strDate = String.valueOf(Integer.parseInt(cm.toString(i15CalendarListBean.getStrTetsudukibi()).substring(0,2)))
                                            + "/"
                                            + String.valueOf(Integer.parseInt(cm.toString(i15CalendarListBean.getStrTetsudukibi()).substring(2)));
                                }
                                else{
                                    strDate = cm.toString(i15CalendarListBean.getStrTetsudukibi());
                                }
                                //Add End
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DEADLINE_PROC );
                                //2004.10.25 Update
                                workCell.setCellValue( strDate );
                                //Update End

                                // 難易度ランク（得点率）セット 2004.11.19 無効値を"--"にする
                                if( i15CalendarListBean.getFloBorderTokuritsu() != -999.0 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_CENTER_BORDER );
                                    workCell.setCellValue( i15CalendarListBean.getFloBorderTokuritsu() );
                                }else{
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_CENTER_BORDER );
                                    workCell.setCellValue( "--" );
                                }

                                // 難易度ランク（偏差値）セット 2004.11.19 無効値を"--"にする
                                if( i15CalendarListBean.getFloRankHensa() == 0 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                    workCell.setCellValue( JudgementConstants.Univ.RANK_STR_NONE );
                                }else if( i15CalendarListBean.getFloRankHensa() != -999.0 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                    workCell.setCellValue( i15CalendarListBean.getFloRankHensa() );
                                }else{
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                    workCell.setCellValue( "--" );
                                }

                                // 難易度プロット
                                plotDifficulty(workSheet, hdkStrtRow,
                                        i15CalendarListBean.getStrRankName(),
                                        i15CalendarListBean.getFloBorderTokuritsu());

                                /** 合格可能性判定リスト **/
                                // データセット
                                ArrayList i15HyoukaList = i15CalendarListBean.getI15HyoukaList();
                                Iterator itrI15Hyouka = i15HyoukaList.iterator();

                                log.Ep("I15","合格可能性判定リストデータセット開始","");
                                while(itrI15Hyouka.hasNext()){
                                    I15HyoukaListBean i15HyoukaListBean = (I15HyoukaListBean) itrI15Hyouka.next();

                                    int col = COL_EXAM_1;
                                    boolean bolHyoujiFlg = true;
                                    if( cm.toString(i15HyoukaListBean.getStrHyoukaCenter()).equals("1") ){
                                    }else if( cm.toString(i15HyoukaListBean.getStrHyoukaCenter()).equals("2") ){
                                        col = col + 2;
                                    }else{
                                        bolHyoujiFlg = false;
                                    }
                                    if(bolHyoujiFlg){
                                        // 模試名セット
                                        msh = cm.setTaisyouMoshi( i15HyoukaListBean.getStrMshDate() );
                                        //2004.11.17 Update
                                        if( i15HyoukaListBean.getStrMshmei() != null){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 18, col );
                                            workCell.setCellValue( i15HyoukaListBean.getStrMshmei() + msh );
                                        }
                                        //Update End

                                        // 評価セット
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, col );
                                        workCell.setCellValue( i15HyoukaListBean.getStrHyouka() );

                                    }
                                }
                                log.Ep("I15","合格可能性判定リストデータセット終了","");

                                /** 判定履歴リスト **/
                                // データセット
                                ArrayList i15HnteiRrk = i15CalendarListBean.getI15HanteiRirekiList();
                                Iterator itrI15HanteiRrk = i15HnteiRrk.iterator();

                                int hanteiCol = COL_JUDGE_HISTORY;

                                log.Ep("I15","判定履歴リストデータセット開始","");
                                while(itrI15HanteiRrk.hasNext()){
                                    I15HanteiRirekiListBean i15HanteiRirekiListBean = (I15HanteiRirekiListBean) itrI15HanteiRrk.next();

                                    //2004.11.20 １回出力したら出力しない
                                    rirekiExistFlg = true;
                                    if( mshHyojiFlg == false){
                                        // 模試月取得
                                        msh = cm.setTaisyouMoshi( i15HanteiRirekiListBean.getStrMshDate() );
                                        // 模試名セット
                                        //2004.11.17 Update
                                        if( i15HanteiRirekiListBean.getStrMshmei()!=null){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 18, hanteiCol );
                                            workCell.setCellValue( i15HanteiRirekiListBean.getStrMshmei() + msh );
                                        }
                                        //Update End
                                    }

                                    // 評価セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, hanteiCol );
                                    workCell.setCellValue( i15HanteiRirekiListBean.getStrHyouka() );

                                    hanteiCol = hanteiCol + COL_COUNT_JUDGE_HISTORY;
                                    if(hanteiCol>MAX_COL_JUDGE_HISTORY){
                                        break;
                                    }
                                }
                                if( rirekiExistFlg == true){
                                    mshHyojiFlg = true;
                                }

                                log.Ep("I15","判定履歴リストデータセット終了","");

                                // ２次教科配点セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_COURSE_ALLOT );
                                workCell.setCellValue( i15CalendarListBean.getStrCourseAllot() );

                                // 備考セット
                                //2005.01.13 表示列を右に2つ移動(34→36)
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_ALLOT_REMARK );
                                workCell.setCellValue( i15CalendarListBean.getStrBiko() );

                                log.Ep("I15","２月分スケジュール処理終了","");
                            }


                            //２月以外のデータをセット
                            else{

                                log.Ep("I15","２月以外のスケジュール処理開始","");

                                ArrayList mi15Hyouka = new ArrayList();
                                ArrayList mi15HanteiRireki = new ArrayList();

                                mI15CalendarList mi15CalendarList = new mI15CalendarList();
                                String chihoSkn;

                                //受験日 → 2005/03/22 ４月は３月以降とする
                                mi15CalendarList.setStrJukenbi(jukenbi);

                                //2004.11.8 Add
                                String strJukenDate = "";
                                if( Integer.parseInt(jukenbi.substring(0,2)) >= 1 && Integer.parseInt(jukenbi.substring(0,2)) <= 4 ){
                                    strJukenDate = Integer.toString(nendo+1) + jukenbi;
                                }
                                else{
                                    strJukenDate = Integer.toString(nendo) + jukenbi;
                                }

                                boolean bolSetFlag = true;
                                int intJukenbiListSize = no2jukenbiList.size();
                                for( int a = 0; a < intJukenbiListSize; a++ ){
                                    String strWork = (String)no2jukenbiList.get(a);
                                    if( strJukenDate.equals(strWork) == true ){
                                        bolSetFlag = false;
                                        break;
                                    }
                                }

                                if( bolSetFlag == true ){

                                    int intMonth = Integer.parseInt(jukenbi.substring(0,2));

                                    if( intMonth < 2 || ( intMonth >= 5 && intMonth <= 12 ) ){

                                        intCntFebBefore++;
                                    }
                                    else if(intMonth > 2 && intMonth <= 4){
                                        intCntFebAfter++;
                                    }

                                    //Add end
                                    //2004.10.26 Update
                                    if( Integer.parseInt(jukenbi.substring(0,2)) <= 1 || Integer.parseInt(jukenbi.substring(0,2)) >= 3 ){
                                        no2jukenbiList.add(strJukenDate);
                                    }

                                    //Update End

                                    //地方試験地セット
                                    chihoSkn = (String) itrShikenti.next();
                                    mi15CalendarList.setStrShikenti(chihoSkn);
                                    //大学名セット
                                    mi15CalendarList.setStrDaigakuMei(i15CalendarListBean.getStrDaigakuMei());
                                    // 学部名セット
                                    mi15CalendarList.setStrGakubuMei(i15CalendarListBean.getStrGakubuMei());
                                    // 学科名セット
                                    mi15CalendarList.setStrGakkaMei(i15CalendarListBean.getStrGakkaMei());
                                    // [複]セット
                                    mi15CalendarList.setIntPluralFlg(i15CalendarListBean.getIntPluralFlg());
                                    // 出願締切セット
                                    mi15CalendarList.setStrSyutsuganbi(i15CalendarListBean.getStrSyutsuganbi());
                                    // 合格発表セット
                                    mi15CalendarList.setStrGoukakubi(i15CalendarListBean.getStrGoukakubi());
                                    // 入学手続き締切日セット
                                    mi15CalendarList.setStrTetsudukibi(i15CalendarListBean.getStrTetsudukibi());
                                    // 難易度ランク（得点率）セット
                                    mi15CalendarList.setFloBorderTokuritsu(i15CalendarListBean.getFloBorderTokuritsu());
                                    // 難易度ランク（偏差値）セット
                                    mi15CalendarList.setFloRankHensa(i15CalendarListBean.getFloRankHensa());
                                    // 難易ランク名セット
                                    mi15CalendarList.setStrRankName(i15CalendarListBean.getStrRankName());

                                    /** 合格可能性判定リスト **/
                                    // データセット
                                    ArrayList i15HyoukaList = i15CalendarListBean.getI15HyoukaList();
                                    Iterator itrI15Hyouka = i15HyoukaList.iterator();

                                    log.Ep("I15","合格可能性判定リストデータセット開始","");
                                    while(itrI15Hyouka.hasNext()){
                                        mI15HyoukaList mi15HyoukaList = new mI15HyoukaList();
                                        I15HyoukaListBean i15HyoukaListBean = (I15HyoukaListBean) itrI15Hyouka.next();

                                        mi15HyoukaList.setStrMshDate(i15HyoukaListBean.getStrMshDate());
                                        mi15HyoukaList.setStrMshmei(i15HyoukaListBean.getStrMshmei());
                                        mi15HyoukaList.setStrHyouka(i15HyoukaListBean.getStrHyouka());
                                        mi15HyoukaList.setStrHyoukaCenter(i15HyoukaListBean.getStrHyoukaCenter());

                                        mi15Hyouka.add(mi15HyoukaList);
                                    }
                                    log.Ep("I15","合格可能性判定リストデータセット終了","");

                                    /** 判定履歴リスト **/
                                    // データセット
                                    ArrayList i15HnteiRrk = i15CalendarListBean.getI15HanteiRirekiList();
                                    Iterator itrI15HanteiRrk = i15HnteiRrk.iterator();

                                    log.Ep("I15","判定履歴リストデータセット開始","");
                                    while(itrI15HanteiRrk.hasNext()){
                                        mI15HanteiRirekiList mi15HanteiRirekiList = new mI15HanteiRirekiList();
                                        I15HanteiRirekiListBean i15HanteiRirekiListBean = (I15HanteiRirekiListBean) itrI15HanteiRrk.next();

                                        mi15HanteiRirekiList.setStrMshDate(i15HanteiRirekiListBean.getStrMshDate());
                                        mi15HanteiRirekiList.setStrMshmei(i15HanteiRirekiListBean.getStrMshmei());
                                        mi15HanteiRirekiList.setStrHyouka(i15HanteiRirekiListBean.getStrHyouka());

                                        mi15HanteiRireki.add(mi15HanteiRirekiList);

                                    }
                                    log.Ep("I15","判定履歴リストデータセット終了","");

                                    mi15CalendarList.setMi15HyoukaList(mi15Hyouka);
                                    mi15CalendarList.setMi15HanteiRirekiList(mi15HanteiRireki);

                                    // ２次教科配点セット
                                    mi15CalendarList.setStrCourseAllot(i15CalendarListBean.getStrCourseAllot());
                                    // 備考セット
                                    mi15CalendarList.setStrBiko(i15CalendarListBean.getStrBiko());

                                    mi15Calendar.add(mi15CalendarList);
                                    cntKensu++;
                                }	//2004.11.8 Add end
                                log.Ep("I15","２月以外のスケジュール処理終了","");
                            }
                        }

                        // センター私大セット処理
                        if(UnivDivBean.isShirituUniv(cm.toString(i15CalendarListBean.getStrDaigakuKbn()))){

                            if(cm.toString(i15CalendarListBean.getStrNittei()).equals(centNtiCd)){
                                log.Ep("I15","センター私大セット処理開始","");

                                String nowUNIVNAME = i15CalendarListBean.getStrDaigakuMei()
                                                        + i15CalendarListBean.getStrGakubuMei()
                                                        + i15CalendarListBean.getStrGakkaMei();

                                if(nowUNIVNAME.equals(beforeUNIVNAME)){
                                    if(i15CalendarListBean.getStrJukenbi1() == null){
                                        continue;
                                    }
                                    else{
                                        sdiSrtRow--;
                                        intCntShidai--;
                                    }
                                }

                                if(sdiSrtRow < 10){

                                    // セ私大セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, 0);
                                    workCell.setCellValue( "セ私大" );

                                    // 大学名セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, 2 );
                                    workCell.setCellValue( i15CalendarListBean.getStrDaigakuMei() );

                                    // 学部名セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, 3 );
                                    workCell.setCellValue( i15CalendarListBean.getStrGakubuMei() );

                                    // 学科名セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, 4 );
                                    workCell.setCellValue( i15CalendarListBean.getStrGakkaMei() );

                                    // [複]セット
                                    if( i15CalendarListBean.getIntPluralFlg() == 1 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, 5 );
                                        workCell.setCellValue( "[複]" );
                                    }

                                    // 出願締切セット
                                    String strDate = "";
                                    if(cm.toString(i15CalendarListBean.getStrSyutsuganbi()).length() == 4){
                                        strDate = String.valueOf(Integer.parseInt(i15CalendarListBean.getStrSyutsuganbi().substring(0,2)))
                                                + "/"
                                                + String.valueOf(Integer.parseInt(i15CalendarListBean.getStrSyutsuganbi().substring(2)));
                                    }
                                    else{
                                        strDate = i15CalendarListBean.getStrSyutsuganbi();
                                    }

                                    workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, COL_DEADLINE_APP );
                                    workCell.setCellValue( strDate );

                                    // 合格発表セット
                                    strDate = "";
                                    if( cm.toString(i15CalendarListBean.getStrGoukakubi()).length() == 4 ){
                                        strDate = String.valueOf(Integer.parseInt(cm.toString(i15CalendarListBean.getStrGoukakubi()).substring(0,2)))
                                                + "/"
                                                + String.valueOf(Integer.parseInt(cm.toString(i15CalendarListBean.getStrGoukakubi()).substring(2)));
                                    }
                                    else{
                                        strDate = cm.toString(i15CalendarListBean.getStrGoukakubi());

                                    }
                                    workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, COL_DATE_ANN );
                                    workCell.setCellValue( strDate );

                                    // 入学手続き締切日セット
                                    strDate = "";
                                    if( cm.toString(i15CalendarListBean.getStrTetsudukibi()).length() == 4 ){
                                        strDate = String.valueOf(Integer.parseInt(cm.toString(i15CalendarListBean.getStrTetsudukibi()).substring(0,2)))
                                                + "/"
                                                + String.valueOf(Integer.parseInt(cm.toString(i15CalendarListBean.getStrTetsudukibi()).substring(2)));
                                    }
                                    workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, COL_DEADLINE_PROC );
                                    workCell.setCellValue( strDate );

                                    // 難易度ランク（得点率）セット 2004.11.19 無効値を"--"にする
                                    if( i15CalendarListBean.getFloBorderTokuritsu() != -999.0 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, COL_CENTER_BORDER );
                                        workCell.setCellValue( i15CalendarListBean.getFloBorderTokuritsu() );
                                    }else{
                                        workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, COL_CENTER_BORDER );
                                        workCell.setCellValue( "--" );
                                    }

                                    // 難易度ランク（偏差値）セット 2004.11.19 無効値を"--"にする
                                    if( i15CalendarListBean.getFloRankHensa() == 0 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, COL_RANK );
                                        workCell.setCellValue( JudgementConstants.Univ.RANK_STR_NONE );
                                    }else if( i15CalendarListBean.getFloRankHensa() != -999.0 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, COL_RANK );
                                        workCell.setCellValue( i15CalendarListBean.getFloRankHensa() );
                                    }else{
                                        workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, COL_RANK );
                                        workCell.setCellValue( "--" );
                                    }

                                    // 難易度プロット
                                    plotDifficulty(workSheet, 20+sdiSrtRow,
                                            i15CalendarListBean.getStrRankName(),
                                            i15CalendarListBean.getFloBorderTokuritsu());

                                    /** 合格可能性判定リスト **/
                                    // データセット
                                    ArrayList i15HyoukaList = i15CalendarListBean.getI15HyoukaList();
                                    Iterator itrI15Hyouka = i15HyoukaList.iterator();

                                    log.Ep("I15","合格可能性判定リストデータセット開始","");
                                    while(itrI15Hyouka.hasNext()){
                                        I15HyoukaListBean i15HyoukaListBean = (I15HyoukaListBean) itrI15Hyouka.next();

                                        int col = COL_EXAM_1;
                                        boolean bolHyoujiFlg = true;
                                        if( cm.toString(i15HyoukaListBean.getStrHyoukaCenter()).equals("1") ){
                                        }else if( cm.toString(i15HyoukaListBean.getStrHyoukaCenter()).equals("2") ){
                                            col = col + 2;
                                        }else{
                                            bolHyoujiFlg = false;
                                        }
                                        if(bolHyoujiFlg){
                                            // 模試名セット
                                            msh = cm.setTaisyouMoshi( i15HyoukaListBean.getStrMshDate() );
                                            //2004.11.17 Update
                                            if( i15HyoukaListBean.getStrMshmei()!=null){
                                                workCell = cm.setCell( workSheet, workRow, workCell, 18, col );
                                                workCell.setCellValue( i15HyoukaListBean.getStrMshmei() + msh );
                                            }
                                            //Update End

                                            // 評価セット
                                            workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, col );
                                            workCell.setCellValue( i15HyoukaListBean.getStrHyouka() );

                                        }
                                    }
                                    log.Ep("I15","合格可能性判定リストデータセット終了","");

                                    /** 判定履歴リスト **/
                                    // データセット
                                    ArrayList i15HnteiRrk = i15CalendarListBean.getI15HanteiRirekiList();
                                    Iterator itrI15HanteiRrk = i15HnteiRrk.iterator();

                                    int hanteiCol = COL_JUDGE_HISTORY;

                                    log.Ep("I15","判定履歴リストデータセット開始","");
                                    while(itrI15HanteiRrk.hasNext()){
                                        I15HanteiRirekiListBean i15HanteiRirekiListBean = (I15HanteiRirekiListBean) itrI15HanteiRrk.next();

                                        //2004.11.20 １回出力したら出力しない
                                        rirekiExistFlg = true;
                                        if( mshHyojiFlg == false){
                                            // 模試月取得
                                            msh = cm.setTaisyouMoshi( i15HanteiRirekiListBean.getStrMshDate() );
                                            // 模試名セット
                                            //2004.11.17 Update
                                            if( i15HanteiRirekiListBean.getStrMshmei() != null){
                                                workCell = cm.setCell( workSheet, workRow, workCell, 18, hanteiCol );
                                                workCell.setCellValue( i15HanteiRirekiListBean.getStrMshmei() + msh );
                                            }
                                            //Update End
                                        }

                                        // 評価セット
                                        workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, hanteiCol );
                                        workCell.setCellValue( i15HanteiRirekiListBean.getStrHyouka() );

                                        hanteiCol = hanteiCol + COL_COUNT_JUDGE_HISTORY;
                                        if(hanteiCol>MAX_COL_JUDGE_HISTORY){
                                            break;
                                        }
                                    }
                                    if( rirekiExistFlg == true){
                                        mshHyojiFlg = true;
                                    }

                                    log.Ep("I15","判定履歴リストデータセット終了","");

                                    // ２次教科配点セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, COL_COURSE_ALLOT );
                                    workCell.setCellValue( i15CalendarListBean.getStrCourseAllot() );

                                    // 備考セット
                                    //2005.01.13 表示列を右に2つ移動(34→36)
                                    workCell = cm.setCell( workSheet, workRow, workCell, 20+sdiSrtRow, COL_ALLOT_REMARK );
                                    workCell.setCellValue( i15CalendarListBean.getStrBiko() );

                                    sdiSrtRow++;
                                    intCntShidai++;	//2004.11.9 add

                                    beforeUNIVNAME = nowUNIVNAME;
                                }
                                log.Ep("I15","センター私大セット処理終了","");

                            }
                        }
                    }


                }


                //2004.11.9 Add
                //センター私大セット数算出
                int intDataCnt = intCntFebBefore + intCntFebAfter;
                int intShidaiDispCnt = 10;		//センター私大表示数
                int intCntNotDisp = 0;			//非表示件数
                int intCntAfterDispRow = -1;	//3月以降のデータの最終セット行
                int intCntBeforeDispRow = -1;	//1/31以前のデータの最終セット行
                int intCntAfterDisp = 0;		//3月以降のデータの表示済み件数
                int intCntBeforeDisp = 0;		//1/31以前の表示済み件数

                int intCntSpace = 22;
                if( bolUruuFlg == true ){
                    intCntSpace = 21;
                }

                if( intDataCnt >= intCntSpace ){
                    intShidaiDispCnt = 0;
                }
                else{
                    //2004.11.18 センタ私大の表示数
                    intShidaiDispCnt = intCntSpace - intDataCnt;
                    if(intShidaiDispCnt > intCntShidai){
                        intShidaiDispCnt = intCntShidai;
                    }
                }

                //非表示(読み飛ばし)件数取得
                if( intDataCnt > intCntSpace ){
                    intCntNotDisp = intDataCnt - intCntSpace;
                }

                //2004.11.9 Add End

                //三月以降のデータを出力 → 2005/03/22 ４月は３月以降とする
                if(cntKensu != 0){
                    log.Ep("I15","３月以降分スケジュール出力処理開始","");

                    int hdkStrtRow = 0;
                    int hdkStrtRow2 = 0;

                    if(lastdayOfMonth <= 28){
                        hdkStrtRow = 58;
                        hdkStrtRow2 = 58;
                    }else{
                        hdkStrtRow = 59;
                        hdkStrtRow2 = 59;
                    }

                    // ソート処理
                    Object[] Sort = no2jukenbiList.toArray();
                    Arrays.sort(Sort);

                    Iterator mi15Calendaritr = mi15Calendar.iterator();
                    while(mi15Calendaritr.hasNext()){
                        mI15CalendarList mi15CalendarList2 = (mI15CalendarList) mi15Calendaritr.next();
                        String jukenbi = mi15CalendarList2.getStrJukenbi();

                        //2004.10.26 Add
                        int intJukenMonth = 0;
                        if( jukenbi.length() >= 2){
                            intJukenMonth = Integer.parseInt(jukenbi.substring(0,2));
                        }
                        //Add End


                        if( intJukenMonth > 2 && intJukenMonth < 5 ){

                            //2004.10.26 Update
                            //行数判定
                            int x = -1;
                            String strHojiJukenbi = "";

                            //2004.11.10 Update
                            boolean bolDispFlg = false;
                            for(int s=intCntNotDisp;s<cntKensu;s++){
                            //UpdateEnd
                                String str = (String)Sort[s];
                                int month = Integer.parseInt(str.substring(4,6));

                                if( (month > 2 && month < 5) ){
                                    if( strHojiJukenbi.equals(str.substring(4)) == false ){
                                        x++;
                                        strHojiJukenbi = str.substring(4);
                                    }
                                }

                                if (jukenbi.equals(str.substring(4))){
                                    hdkStrtRow = hdkStrtRow + x;
                                    bolDispFlg = true;
                                    if( s > intCntAfterDisp && hdkStrtRow < 70 ){
                                        intCntAfterDispRow = hdkStrtRow;
                                        intCntAfterDisp = s;
                                    }
                                    break;
                                }
                            }
                            //Update End

                            //Max件数を超える日付はカット
                            if(hdkStrtRow < 70 && bolDispFlg == true){

                                //曜日計算
                                //2004.10.26 Update
                                int intYear2 = nendo+1;
                                //Update End
                                int intMonth1 = Integer.parseInt(jukenbi.substring(0,2));
                                int intMonth2 = Integer.parseInt(jukenbi.substring(0,2));
                                int intDay = Integer.parseInt(jukenbi.substring(2));

                                /* 年、月、日から曜日を計算する */
                                if (intMonth2 == 1 || intMonth2 == 2) {
                                    intYear2--;
                                    intMonth2 += 12;
                                }
                                int nIdx = (intYear2 + (int)(intYear2/4) - (int)(intYear2/100) + (int)(intYear2/400)
                                             + ((int)((13*intMonth2+8)/5) + intDay)) % 7;

                                //試験日セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 0 );
                                workCell.setCellValue( intMonth1 + "/" + intDay );

                                //曜日セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 1 );
                                workCell.setCellValue( weekName[nIdx] );

                                // 大学名セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 2 );
                                workCell.setCellValue( mi15CalendarList2.getStrDaigakuMei() );

                                // 学部名セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 3 );
                                workCell.setCellValue( mi15CalendarList2.getStrGakubuMei() );

                                // 学科名セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 4 );
                                workCell.setCellValue( mi15CalendarList2.getStrGakkaMei() );

                                // [複]セット
                                if( mi15CalendarList2.getIntPluralFlg() == 1 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 5 );
                                    workCell.setCellValue( "[複]" );
                                }

                                // 試験地セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_PLACE );
                                workCell.setCellValue( mi15CalendarList2.getStrShikenti() );

                                // 出願締切セット
                                //2004.10.25 Add
                                String strDate = "";
                                if( cm.toString(mi15CalendarList2.getStrSyutsuganbi()).length() == 4){
                                    strDate = String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrSyutsuganbi()).substring(0,2)))
                                            + "/"
                                            + String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrSyutsuganbi()).substring(2)));
                                }
                                else{
                                    strDate = cm.toString(mi15CalendarList2.getStrSyutsuganbi());
                                }
                                //Add End
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DEADLINE_APP );
                                //2004.10.25 Update
                                workCell.setCellValue( strDate );
                                //Update End

                                // 合格発表セット
                                strDate = "";
                                if(cm.toString(mi15CalendarList2.getStrGoukakubi()).length() == 4 ){
                                    strDate = String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrGoukakubi()).substring(0,2)))
                                            + "/"
                                            + String.valueOf(cm.toString(mi15CalendarList2.getStrGoukakubi()).substring(2));
                                }
                                else{
                                    strDate = cm.toString(mi15CalendarList2.getStrGoukakubi());
                                }
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DATE_ANN );
                                //2004.10.25 Update
                                workCell.setCellValue( strDate );
                                //Update End

                                // 入学手続き締切日セット
                                //2004.10.25 Add
                                strDate = "";
                                if( cm.toString(mi15CalendarList2.getStrTetsudukibi()).length() == 4 ){
                                    strDate = String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrTetsudukibi()).substring(0,2)))
                                            + "/"
                                            + String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrTetsudukibi()).substring(2)));
                                }
                                else{
                                    strDate = cm.toString(mi15CalendarList2.getStrTetsudukibi());
                                }
                                //Add End
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DEADLINE_PROC );
                                //2004.10.25 Update
                                workCell.setCellValue( strDate );
                                //Update End

                                // 難易度ランク（得点率）セット 2004.11.19 無効値を"--"にする
                                if( mi15CalendarList2.getFloBorderTokuritsu() != -999.0 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_CENTER_BORDER );
                                    workCell.setCellValue( mi15CalendarList2.getFloBorderTokuritsu() );
                                }else{
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_CENTER_BORDER );
                                    workCell.setCellValue( "--" );
                                }

                                // 難易度ランク（偏差値）セット 2004.11.19 無効値を"--"にする
                                if( mi15CalendarList2.getFloRankHensa() == 0 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                    workCell.setCellValue( JudgementConstants.Univ.RANK_STR_NONE );
                                }else if( mi15CalendarList2.getFloRankHensa() != -999.0 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                    workCell.setCellValue( mi15CalendarList2.getFloRankHensa() );
                                }else{
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                    workCell.setCellValue( "--" );
                                }

                                // 難易度プロット
                                plotDifficulty(workSheet, hdkStrtRow,
                                        mi15CalendarList2.getStrRankName(),
                                        mi15CalendarList2.getFloBorderTokuritsu());

                                /** 合格可能性判定リスト **/
                                // データセット
                                ArrayList mi15Hyouka2 = mi15CalendarList2.getMi15HyoukaList();
                                Iterator mi15Hyoukaitr = mi15Hyouka2.iterator();

                                log.Ep("I15","合格可能性判定リストデータセット開始","");
                                while(mi15Hyoukaitr.hasNext()){
                                    mI15HyoukaList mi15HyoukaList2 = (mI15HyoukaList) mi15Hyoukaitr.next();

                                    int col = COL_EXAM_1;
                                    boolean bolHyoujiFlg = true;
                                    if( cm.toString(mi15HyoukaList2.getStrHyoukaCenter()).equals("1") ){
                                    }else if( cm.toString(mi15HyoukaList2.getStrHyoukaCenter()).equals("2") ){
                                        col = col + 2;
                                    }else{
                                        bolHyoujiFlg = false;
                                    }
                                    if(bolHyoujiFlg){

                                        // 模試名セット
                                        msh = cm.setTaisyouMoshi( mi15HyoukaList2.getStrMshDate() );
                                        //2004.11.17 Update
                                        if( mi15HyoukaList2.getStrMshmei()!=null){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 18, col );
                                            workCell.setCellValue( mi15HyoukaList2.getStrMshmei() + msh );
                                        }

                                        // 評価セット
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, col );
                                        workCell.setCellValue( mi15HyoukaList2.getStrHyouka() );
                                    }
                                }
                                log.Ep("I15","合格可能性判定リストデータセット終了","");

                                /** 判定履歴リスト **/
                                // データセット
                                ArrayList mi15HanteiRireki2 = mi15CalendarList2.getMi15HanteiRirekiList();
                                Iterator mi15HanteiRirekiitr = mi15HanteiRireki2.iterator();

                                int hanteiCol = COL_JUDGE_HISTORY;

                                log.Ep("I15","判定履歴リストデータセット開始","");
                                while(mi15HanteiRirekiitr.hasNext()){
                                    mI15HanteiRirekiList mi15HanteiRirekiList2 = (mI15HanteiRirekiList) mi15HanteiRirekiitr.next();

                                    //2004.11.20 １回出力したら出力しない
                                    rirekiExistFlg = true;
                                    if( mshHyojiFlg == false){
                                        // 模試名セット
                                        msh = cm.setTaisyouMoshi( mi15HanteiRirekiList2.getStrMshDate() );
                                        //2004.11.17 Update
                                        if( mi15HanteiRirekiList2.getStrMshmei()!=null){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 18, hanteiCol );
                                            workCell.setCellValue( mi15HanteiRirekiList2.getStrMshmei() + msh );
                                        }
                                    }

                                    // 評価セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, hanteiCol );
                                    workCell.setCellValue( mi15HanteiRirekiList2.getStrHyouka() );

                                    hanteiCol = hanteiCol + COL_COUNT_JUDGE_HISTORY;
                                    if(hanteiCol>MAX_COL_JUDGE_HISTORY){
                                        break;
                                    }
                                }
                                if( rirekiExistFlg == true){
                                    mshHyojiFlg = true;
                                }

                                log.Ep("I15","判定履歴リストデータセット終了","");

                                // ２次教科配点セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_COURSE_ALLOT );
                                workCell.setCellValue( mi15CalendarList2.getStrCourseAllot() );

                                // 備考セット
                                //2005.01.13 表示列を右に2つ移動(34→36)
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_ALLOT_REMARK );
                                workCell.setCellValue( mi15CalendarList2.getStrBiko() );
                            }
                            hdkStrtRow = hdkStrtRow2;
                        }
                    }
                }
                log.Ep("I15","３月以降分スケジュール出力処理終了","");

                //2004.10.26 Add
                //2月より前のデータを出力 → 2005/03/22 ４月は３月以降とする
                if(cntKensu != 0){
                    log.Ep("I15","2月より前のスケジュール出力処理開始","");

                    //2004.11.09 Update
                    int hdkStrtRow = 20 + intShidaiDispCnt;
                    int hdkStrtRow2 = 20 + intShidaiDispCnt;
                    //Update End


                    // ソート処理
                    Object[] Sort = no2jukenbiList.toArray();
                    Arrays.sort(Sort);
                    boolean bolDispFlg = false;

                    Iterator mi15Calendaritr = mi15Calendar.iterator();
                    while(mi15Calendaritr.hasNext()){
                        mI15CalendarList mi15CalendarList2 = (mI15CalendarList) mi15Calendaritr.next();
                        String jukenbi = mi15CalendarList2.getStrJukenbi();

                        int intJukenMonth = 0;
                        if( jukenbi.length() >= 2){
                            intJukenMonth = Integer.parseInt(jukenbi.substring(0,2));
                        }

                        if( ( intJukenMonth >= 5 && intJukenMonth <= 12 ) || intJukenMonth == 1 ){
                            //行数判定
                            int x = -1;
                            String strHojiJukenbi = "";
                            cntKensu = no2jukenbiList.size();
                            bolDispFlg = false;
                            for(int s=intCntNotDisp;s<cntKensu;s++){
                                String str = (String)Sort[s];
                                int month = Integer.parseInt(str.substring(4,6));

                                if( (month >= 5 && month<=12) || month == 1 ){
                                    if( strHojiJukenbi.equals(str.substring(4)) == false ){
                                        x++;
                                        strHojiJukenbi = str.substring(4);
                                    }
                                }

                                if (jukenbi.equals(str.substring(4))){
                                    hdkStrtRow = hdkStrtRow + x;
                                    bolDispFlg = true;
                                    if( s > intCntBeforeDisp && hdkStrtRow < 30 ){
                                        intCntBeforeDispRow = hdkStrtRow;
                                        intCntBeforeDisp = s;
                                    }
                                    break;
                                }
                            }

                            //Max件数を超える日付はカット
                            if(hdkStrtRow < 30 && bolDispFlg == true){

                                //曜日計算
                                int intYear2 = nendo;
                                int intMonth1 = Integer.parseInt(jukenbi.substring(0,2));
                                int intMonth2 = Integer.parseInt(jukenbi.substring(0,2));
                                int intDay = Integer.parseInt(jukenbi.substring(2));

                                if( intMonth2 >=4 && intMonth2 <= 12){
                                    intYear2 = nendo;
                                }
                                else{
                                    intYear2 = nendo+1;
                                }

                                /* 年、月、日から曜日を計算する */
                                if (intMonth2 == 1 || intMonth2 == 2) {
                                    intYear2--;
                                    intMonth2 += 12;
                                }
                                int nIdx = (intYear2 + (int)(intYear2/4) - (int)(intYear2/100) + (int)(intYear2/400)
                                             + ((int)((13*intMonth2+8)/5) + intDay)) % 7;

                                //試験日セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 0 );
                                workCell.setCellValue( intMonth1 + "/" + intDay );

                                //曜日セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 1 );
                                workCell.setCellValue( weekName[nIdx] );

                                // 大学名セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 2 );
                                workCell.setCellValue( mi15CalendarList2.getStrDaigakuMei() );

                                // 学部名セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 3 );
                                workCell.setCellValue( mi15CalendarList2.getStrGakubuMei() );

                                // 学科名セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 4 );
                                workCell.setCellValue( mi15CalendarList2.getStrGakkaMei() );

                                // [複]セット
                                if( mi15CalendarList2.getIntPluralFlg() == 1 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 5 );
                                    workCell.setCellValue( "[複]" );
                                }

                                // 試験地セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_PLACE );
                                workCell.setCellValue( mi15CalendarList2.getStrShikenti() );

                                // 出願締切セット
                                String strDate = "";
                                if( cm.toString(mi15CalendarList2.getStrSyutsuganbi()).length() == 4){
                                    strDate = String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrSyutsuganbi()).substring(0,2)))
                                            + "/"
                                            + String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrSyutsuganbi()).substring(2)));
                                }
                                else{
                                    strDate = cm.toString(mi15CalendarList2.getStrSyutsuganbi());
                                }
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DEADLINE_APP );
                                workCell.setCellValue( strDate );

                                // 合格発表セット
                                strDate = "";
                                if(cm.toString(mi15CalendarList2.getStrGoukakubi()).length() == 4 ){
                                    strDate = String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrGoukakubi()).substring(0,2)))
                                            + "/"
                                            + String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrGoukakubi()).substring(2)));
                                }
                                else{
                                    strDate = cm.toString(mi15CalendarList2.getStrGoukakubi());
                                }
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DATE_ANN );
                                workCell.setCellValue( strDate );

                                // 入学手続き締切日セット
                                strDate = "";
                                if( cm.toString(mi15CalendarList2.getStrTetsudukibi()).length() == 4 ){
                                    strDate = String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrTetsudukibi()).substring(0,2)))
                                            + "/"
                                            + String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrTetsudukibi()).substring(2)));
                                }
                                else{
                                    strDate = cm.toString(mi15CalendarList2.getStrTetsudukibi());
                                }
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DEADLINE_PROC );
                                workCell.setCellValue( strDate );

                                // 難易度ランク（得点率）セット 2004.11.19 無効値を"--"にする
                                if( mi15CalendarList2.getFloBorderTokuritsu() != -999.0 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_CENTER_BORDER );
                                    workCell.setCellValue( mi15CalendarList2.getFloBorderTokuritsu() );
                                }else{
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_CENTER_BORDER );
                                    workCell.setCellValue( "--" );
                                }

                                // 難易度ランク（偏差値）セット 2004.11.19 無効値を"--"にする
                                if( mi15CalendarList2.getFloRankHensa() == 0 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                    workCell.setCellValue( JudgementConstants.Univ.RANK_STR_NONE );
                                }else if( mi15CalendarList2.getFloRankHensa() != -999.0 ){
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                    workCell.setCellValue( mi15CalendarList2.getFloRankHensa() );
                                }else{
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                    workCell.setCellValue( "--" );
                                }

                                // 難易度プロット
                                plotDifficulty(workSheet, hdkStrtRow,
                                        mi15CalendarList2.getStrRankName(),
                                        mi15CalendarList2.getFloBorderTokuritsu());

                                /** 合格可能性判定リスト **/
                                // データセット
                                ArrayList mi15Hyouka2 = mi15CalendarList2.getMi15HyoukaList();
                                Iterator mi15Hyoukaitr = mi15Hyouka2.iterator();

                                log.Ep("I15","合格可能性判定リストデータセット開始","");
                                while(mi15Hyoukaitr.hasNext()){
                                    mI15HyoukaList mi15HyoukaList2 = (mI15HyoukaList) mi15Hyoukaitr.next();

                                    int col = COL_EXAM_1;
                                    boolean bolHyoujiFlg = true;
                                    if( cm.toString(mi15HyoukaList2.getStrHyoukaCenter()).equals("1") ){
                                    }else if( cm.toString(mi15HyoukaList2.getStrHyoukaCenter()).equals("2") ){
                                        col = col + 2;
                                    }else{
                                        bolHyoujiFlg = false;
                                    }
                                    if(bolHyoujiFlg){

                                        // 模試名セット
                                        msh = cm.setTaisyouMoshi( mi15HyoukaList2.getStrMshDate() );
                                        //2004.11.17 Update
                                        if( mi15HyoukaList2.getStrMshmei()!=null){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 18, col );
                                            workCell.setCellValue( mi15HyoukaList2.getStrMshmei() + msh );
                                        }

                                        // 評価セット
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, col );
                                        workCell.setCellValue( mi15HyoukaList2.getStrHyouka() );
                                    }
                                }
                                log.Ep("I15","合格可能性判定リストデータセット終了","");

                                /** 判定履歴リスト **/
                                // データセット
                                ArrayList mi15HanteiRireki2 = mi15CalendarList2.getMi15HanteiRirekiList();
                                Iterator mi15HanteiRirekiitr = mi15HanteiRireki2.iterator();

                                int hanteiCol = COL_JUDGE_HISTORY;

                                log.Ep("I15","判定履歴リストデータセット開始","");
                                while(mi15HanteiRirekiitr.hasNext()){
                                    mI15HanteiRirekiList mi15HanteiRirekiList2 = (mI15HanteiRirekiList) mi15HanteiRirekiitr.next();

                                    //2004.11.20 １回出力したら出力しない
                                    rirekiExistFlg = true;
                                    if( mshHyojiFlg == false){
                                        // 模試名セット
                                        msh = cm.setTaisyouMoshi( mi15HanteiRirekiList2.getStrMshDate() );
                                        //2004.11.17 Update
                                        if( mi15HanteiRirekiList2.getStrMshmei()!=null){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 18, hanteiCol );
                                            workCell.setCellValue( mi15HanteiRirekiList2.getStrMshmei() + msh );
                                        }
                                    }

                                    // 評価セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, hanteiCol );
                                    workCell.setCellValue( mi15HanteiRirekiList2.getStrHyouka() );

                                    hanteiCol = hanteiCol + COL_COUNT_JUDGE_HISTORY;
                                    if(hanteiCol>MAX_COL_JUDGE_HISTORY){
                                        break;
                                    }
                                }
                                if( rirekiExistFlg == true){
                                    mshHyojiFlg = true;
                                }

                                log.Ep("I15","判定履歴リストデータセット終了","");

                                // ２次教科配点セット
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_COURSE_ALLOT );
                                workCell.setCellValue( mi15CalendarList2.getStrCourseAllot() );

                                // 備考セット
                                //2005.01.13 表示列を右に2つ移動(34→36)
                                workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_ALLOT_REMARK );
                                workCell.setCellValue( mi15CalendarList2.getStrBiko() );
                            }
                            hdkStrtRow = hdkStrtRow2;
                        }
                    }
                }
                log.Ep("I15","2月より前のスケジュール出力処理終了","");
                //2004.10.26 Add End

                //表示し切れなかった分を出力
                //2004.11.10 Add
                if( intCntBeforeDispRow < 30 ){
                    //三月以降のデータを出力 → 2005/03/22 ４月は３月以降とする
                    if(cntKensu != 0){
                        log.Ep("I15","３月以降分スケジュール出力処理開始","");

                        int hdkStrtRow = intCntBeforeDispRow;
                        int hdkStrtRow2 = intCntBeforeDispRow;

                        if( intCntBeforeDispRow < 0 ){
                            hdkStrtRow = 20 + intCntBeforeDispRow + 1 + intShidaiDispCnt + intCntFebBefore;
                            hdkStrtRow2 = 20 + intCntBeforeDispRow + 1 + intShidaiDispCnt + intCntFebBefore;
                        }
                        else{
                            hdkStrtRow = intCntBeforeDispRow+1;
                            hdkStrtRow2 = intCntBeforeDispRow+1;
                        }
                        int intLoopStartIndex = intCntAfterDisp + 1;

                        // ソート処理
                        Object[] Sort = no2jukenbiList.toArray();
                        Arrays.sort(Sort);

                        Iterator mi15Calendaritr = mi15Calendar.iterator();
                        while(mi15Calendaritr.hasNext()){
                            mI15CalendarList mi15CalendarList2 = (mI15CalendarList) mi15Calendaritr.next();
                            String jukenbi = mi15CalendarList2.getStrJukenbi();

                            //2004.10.26 Add
                            int intJukenMonth = 0;
                            if( jukenbi.length() >= 2){
                                intJukenMonth = Integer.parseInt(jukenbi.substring(0,2));
                            }
                            //Add End


                            if( intJukenMonth > 2 && intJukenMonth < 5 ){
                                //行数判定
                                int x = -1;
                                String strHojiJukenbi = "";

                                //2004.11.10 Update
                                boolean bolDispFlg = false;
                                for(int s=intLoopStartIndex;s<cntKensu;s++){
                                //UpdateEnd
                                    String str = (String)Sort[s];
                                    int month = Integer.parseInt(str.substring(4,6));

                                    if( (month > 2 && month < 5) ){
                                        if( strHojiJukenbi.equals(str.substring(4)) == false ){
                                            x++;
                                            strHojiJukenbi = str.substring(4);
                                        }
                                    }

                                    if (jukenbi.equals(str.substring(4))){
                                        hdkStrtRow = hdkStrtRow + x;
                                        bolDispFlg = true;
                                        break;
                                    }
                                }
                                //Update End

                                //Max件数を超える日付はカット
                                if(hdkStrtRow < 29 && bolDispFlg == true){

                                    //曜日計算
                                    //2004.10.26 Update
                                    int intYear2 = nendo+1;
                                    //Update End
                                    int intMonth1 = Integer.parseInt(jukenbi.substring(0,2));
                                    int intMonth2 = Integer.parseInt(jukenbi.substring(0,2));
                                    int intDay = Integer.parseInt(jukenbi.substring(2));

                                    /* 年、月、日から曜日を計算する */
                                    if (intMonth2 == 1 || intMonth2 == 2) {
                                        intYear2--;
                                        intMonth2 += 12;
                                    }
                                    int nIdx = (intYear2 + (int)(intYear2/4) - (int)(intYear2/100) + (int)(intYear2/400)
                                                 + ((int)((13*intMonth2+8)/5) + intDay)) % 7;

                                    //試験日セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 0 );
                                    workCell.setCellValue( intMonth1 + "/" + intDay );

                                    //曜日セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 1 );
                                    workCell.setCellValue( weekName[nIdx] );

                                    // 大学名セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 2 );
                                    workCell.setCellValue( mi15CalendarList2.getStrDaigakuMei() );

                                    // 学部名セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 3 );
                                    workCell.setCellValue( mi15CalendarList2.getStrGakubuMei() );

                                    // 学科名セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 4 );
                                    workCell.setCellValue( mi15CalendarList2.getStrGakkaMei() );

                                    // [複]セット
                                    if( mi15CalendarList2.getIntPluralFlg() == 1 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 5 );
                                        workCell.setCellValue( "[複]" );
                                    }

                                    // 試験地セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_PLACE );
                                    workCell.setCellValue( mi15CalendarList2.getStrShikenti() );

                                    // 出願締切セット
                                    //2004.10.25 Add
                                    String strDate = "";
                                    if( cm.toString(mi15CalendarList2.getStrSyutsuganbi()).length() == 4){
                                        strDate = String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrSyutsuganbi()).substring(0,2)))
                                                + "/"
                                                + String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrSyutsuganbi()).substring(2)));
                                    }
                                    else{
                                        strDate = cm.toString(mi15CalendarList2.getStrSyutsuganbi());
                                    }
                                    //Add End
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DEADLINE_APP );
                                    //2004.10.25 Update
                                    workCell.setCellValue( strDate );
                                    //Update End

                                    // 合格発表セット
                                    strDate = "";
                                    if(cm.toString(mi15CalendarList2.getStrGoukakubi()).length() == 4 ){
                                        strDate = String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrGoukakubi()).substring(0,2)))
                                                + "/"
                                                + String.valueOf(cm.toString(mi15CalendarList2.getStrGoukakubi()).substring(2));
                                    }
                                    else{
                                        strDate = cm.toString(mi15CalendarList2.getStrGoukakubi());
                                    }
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DATE_ANN );
                                    //2004.10.25 Update
                                    workCell.setCellValue( strDate );
                                    //Update End

                                    // 入学手続き締切日セット
                                    //2004.10.25 Add
                                    strDate = "";
                                    if( cm.toString(mi15CalendarList2.getStrTetsudukibi()).length() == 4 ){
                                        strDate = String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrTetsudukibi()).substring(0,2)))
                                                + "/"
                                                + String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrTetsudukibi()).substring(2)));
                                    }
                                    else{
                                        strDate = cm.toString(mi15CalendarList2.getStrTetsudukibi());
                                    }
                                    //Add End
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DEADLINE_PROC );
                                    //2004.10.25 Update
                                    workCell.setCellValue( strDate );
                                    //Update End

                                    // 難易度ランク（得点率）セット 2004.11.19 無効値を"--"にする
                                    if( mi15CalendarList2.getFloBorderTokuritsu() != -999.0 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_CENTER_BORDER );
                                        workCell.setCellValue( mi15CalendarList2.getFloBorderTokuritsu() );
                                    }else{
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_CENTER_BORDER );
                                        workCell.setCellValue( "--" );
                                    }

                                    // 難易度ランク（偏差値）セット 2004.11.19 無効値を"--"にする
                                    if( mi15CalendarList2.getFloRankHensa() == 0 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                        workCell.setCellValue( JudgementConstants.Univ.RANK_STR_NONE );
                                    }else if( mi15CalendarList2.getFloRankHensa() != -999.0 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                        workCell.setCellValue( mi15CalendarList2.getFloRankHensa() );
                                    }else{
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                        workCell.setCellValue( "--" );
                                    }

                                    // 難易度プロット
                                    plotDifficulty(workSheet, hdkStrtRow,
                                            mi15CalendarList2.getStrRankName(),
                                            mi15CalendarList2.getFloBorderTokuritsu());

                                    /** 合格可能性判定リスト **/
                                    // データセット
                                    ArrayList mi15Hyouka2 = mi15CalendarList2.getMi15HyoukaList();
                                    Iterator mi15Hyoukaitr = mi15Hyouka2.iterator();

                                    log.Ep("I15","合格可能性判定リストデータセット開始","");
                                    while(mi15Hyoukaitr.hasNext()){
                                        mI15HyoukaList mi15HyoukaList2 = (mI15HyoukaList) mi15Hyoukaitr.next();

                                        int col = COL_EXAM_1;
                                        boolean bolHyoujiFlg = true;
                                        if( cm.toString(mi15HyoukaList2.getStrHyoukaCenter()).equals("1") ){
                                        }else if( cm.toString(mi15HyoukaList2.getStrHyoukaCenter()).equals("2") ){
                                            col = col + 2;
                                        }else{
                                            bolHyoujiFlg = false;
                                        }
                                        if(bolHyoujiFlg){

                                            // 模試名セット
                                            msh = cm.setTaisyouMoshi( mi15HyoukaList2.getStrMshDate() );
                                            //2004.11.17 Update
                                            if( mi15HyoukaList2.getStrMshmei()!=null){
                                                workCell = cm.setCell( workSheet, workRow, workCell, 18, col );
                                                workCell.setCellValue( mi15HyoukaList2.getStrMshmei() + msh );
                                            }
                                            // 評価セット
                                            workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, col );
                                            workCell.setCellValue( mi15HyoukaList2.getStrHyouka() );
                                        }
                                    }
                                    log.Ep("I15","合格可能性判定リストデータセット終了","");

                                    /** 判定履歴リスト **/
                                    // データセット
                                    ArrayList mi15HanteiRireki2 = mi15CalendarList2.getMi15HanteiRirekiList();
                                    Iterator mi15HanteiRirekiitr = mi15HanteiRireki2.iterator();

                                    int hanteiCol = COL_JUDGE_HISTORY;

                                    log.Ep("I15","判定履歴リストデータセット開始","");
                                    while(mi15HanteiRirekiitr.hasNext()){
                                        mI15HanteiRirekiList mi15HanteiRirekiList2 = (mI15HanteiRirekiList) mi15HanteiRirekiitr.next();

                                        //2004.11.20 １回出力したら出力しない
                                        rirekiExistFlg = true;
                                        if( mshHyojiFlg == false){
                                            // 模試名セット
                                            msh = cm.setTaisyouMoshi( mi15HanteiRirekiList2.getStrMshDate() );
                                            //2004.11.17 Update
                                            if( mi15HanteiRirekiList2.getStrMshmei()!=null){
                                                workCell = cm.setCell( workSheet, workRow, workCell, 18, hanteiCol );
                                                workCell.setCellValue( mi15HanteiRirekiList2.getStrMshmei() + msh );
                                            }
                                        }

                                        // 評価セット
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, hanteiCol );
                                        workCell.setCellValue( mi15HanteiRirekiList2.getStrHyouka() );

                                        hanteiCol = hanteiCol + COL_COUNT_JUDGE_HISTORY;
                                        if(hanteiCol>MAX_COL_JUDGE_HISTORY){
                                            break;
                                        }
                                    }
                                    if( rirekiExistFlg == true){
                                        mshHyojiFlg = true;
                                    }

                                    log.Ep("I15","判定履歴リストデータセット終了","");

                                    // ２次教科配点セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_COURSE_ALLOT );
                                    workCell.setCellValue( mi15CalendarList2.getStrCourseAllot() );

                                    // 備考セット
                                    //2005.01.13 表示列を右に2つ移動(34→36)
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_ALLOT_REMARK );
                                    workCell.setCellValue( mi15CalendarList2.getStrBiko() );
                                }
                                hdkStrtRow = hdkStrtRow2;
                            }
                        }
                    }
                    log.Ep("I15","３月以降分スケジュール出力処理終了","");
                }

                if( intCntAfterDispRow < 70 ){
                    //2月より前のデータを出力 → 2005/03/22 ４月は３月以降とする
                    if(cntKensu != 0){
                        log.Ep("I15","2月より前のスケジュール出力処理開始","");

                        int hdkStrtRow = 0;
                        int hdkStrtRow2 = 0;
                        int intLoopStart = intCntBeforeDisp + 1;

                        if( intCntAfterDispRow < 0 ){
                            if(lastdayOfMonth <= 28){
                                hdkStrtRow = 58+intCntAfterDispRow+1;
                                hdkStrtRow2 = 58+intCntAfterDispRow+1;
                            }else{
                                hdkStrtRow = 59+intCntAfterDispRow+1;
                                hdkStrtRow2 = 59+intCntAfterDispRow+1;
                            }
                        }
                        else{
                            hdkStrtRow = intCntAfterDispRow+1;
                            hdkStrtRow2 = intCntAfterDispRow+1;
                        }

                        // ソート処理
                        Object[] Sort = no2jukenbiList.toArray();
                        Arrays.sort(Sort);
                        boolean bolDispFlg = false;

                        Iterator mi15Calendaritr = mi15Calendar.iterator();
                        while(mi15Calendaritr.hasNext()){
                            mI15CalendarList mi15CalendarList2 = (mI15CalendarList) mi15Calendaritr.next();
                            String jukenbi = mi15CalendarList2.getStrJukenbi();

                            int intJukenMonth = 0;
                            if( jukenbi.length() >= 2){
                                intJukenMonth = Integer.parseInt(jukenbi.substring(0,2));
                            }

                            if( ( intJukenMonth >= 5 && intJukenMonth <= 12 ) || intJukenMonth == 1 ){
                                //行数判定
                                int x = -1;
                                String strHojiJukenbi = "";
                                cntKensu = no2jukenbiList.size();
                                bolDispFlg = false;
                                for(int s=intLoopStart;s<cntKensu;s++){
                                    String str = (String)Sort[s];
                                    int month = Integer.parseInt(str.substring(4,6));

                                    if( (month >= 5 && month<=12) || month == 1 ){
                                        if( strHojiJukenbi.equals(str.substring(4)) == false ){
                                            x++;
                                            strHojiJukenbi = str.substring(4);
                                        }
                                    }

                                    if (jukenbi.equals(str.substring(4))){
                                        hdkStrtRow = hdkStrtRow + x;
                                        bolDispFlg = true;
                                        break;
                                    }
                                }

                                //Max件数を超える日付はカット
                                if(hdkStrtRow < 70 && bolDispFlg == true ){

                                    //曜日計算
                                    int intYear2 = nendo;
                                    int intMonth1 = Integer.parseInt(jukenbi.substring(0,2));
                                    int intMonth2 = Integer.parseInt(jukenbi.substring(0,2));
                                    int intDay = Integer.parseInt(jukenbi.substring(2));

                                    if( intMonth2 >=4 && intMonth2 <= 12){
                                        intYear2 = nendo;
                                    }
                                    else{
                                        intYear2 = nendo+1;
                                    }

                                    /* 年、月、日から曜日を計算する */
                                    if (intMonth2 == 1 || intMonth2 == 2) {
                                        intYear2--;
                                        intMonth2 += 12;
                                    }
                                    int nIdx = (intYear2 + (int)(intYear2/4) - (int)(intYear2/100) + (int)(intYear2/400)
                                                 + ((int)((13*intMonth2+8)/5) + intDay)) % 7;

                                    //試験日セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 0 );
                                    workCell.setCellValue( intMonth1 + "/" + intDay );

                                    //曜日セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 1 );
                                    workCell.setCellValue( weekName[nIdx] );

                                    // 大学名セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 2 );
                                    workCell.setCellValue( mi15CalendarList2.getStrDaigakuMei() );

                                    // 学部名セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 3 );
                                    workCell.setCellValue( mi15CalendarList2.getStrGakubuMei() );

                                    // 学科名セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 4 );
                                    workCell.setCellValue( mi15CalendarList2.getStrGakkaMei() );

                                    // [複]セット
                                    if( mi15CalendarList2.getIntPluralFlg() == 1 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, 5 );
                                        workCell.setCellValue( "[複]" );
                                    }

                                    // 試験地セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_PLACE );
                                    workCell.setCellValue( mi15CalendarList2.getStrShikenti() );

                                    // 出願締切セット
                                    String strDate = "";
                                    if( cm.toString(mi15CalendarList2.getStrSyutsuganbi()).length() == 4){
                                        strDate = String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrSyutsuganbi()).substring(0,2)))
                                                + "/"
                                                + String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrSyutsuganbi()).substring(2)));
                                    }
                                    else{
                                        strDate = cm.toString(mi15CalendarList2.getStrSyutsuganbi());
                                    }
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DEADLINE_APP );
                                    workCell.setCellValue( strDate );

                                    // 合格発表セット
                                    strDate = "";
                                    if(cm.toString(mi15CalendarList2.getStrGoukakubi()).length() == 4 ){
                                        strDate = String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrGoukakubi()).substring(0,2)))
                                                + "/"
                                                + String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrGoukakubi()).substring(2)));
                                    }
                                    else{
                                        strDate = cm.toString(mi15CalendarList2.getStrGoukakubi());
                                    }
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DATE_ANN );
                                    workCell.setCellValue( strDate );

                                    // 入学手続き締切日セット
                                    strDate = "";
                                    if( cm.toString(mi15CalendarList2.getStrTetsudukibi()).length() == 4 ){
                                        strDate = String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrTetsudukibi()).substring(0,2)))
                                                + "/"
                                                + String.valueOf(Integer.parseInt(cm.toString(mi15CalendarList2.getStrTetsudukibi()).substring(2)));
                                    }
                                    else{
                                        strDate = cm.toString(mi15CalendarList2.getStrTetsudukibi());
                                    }
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_DEADLINE_PROC );
                                    workCell.setCellValue( strDate );

                                    // 難易度ランク（得点率）セット 2004.11.19 無効値を"--"にする
                                    if( mi15CalendarList2.getFloBorderTokuritsu() != -999.0 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_CENTER_BORDER );
                                        workCell.setCellValue( mi15CalendarList2.getFloBorderTokuritsu() );
                                    }else{
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_CENTER_BORDER );
                                        workCell.setCellValue( "--" );
                                    }

                                    // 難易度ランク（偏差値）セット 2004.11.19 無効値を"--"にする
                                    if( mi15CalendarList2.getFloRankHensa() == 0 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                        workCell.setCellValue( JudgementConstants.Univ.RANK_STR_NONE );
                                    }else if( mi15CalendarList2.getFloRankHensa() != -999.0 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                        workCell.setCellValue( mi15CalendarList2.getFloRankHensa() );
                                    }else{
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_RANK );
                                        workCell.setCellValue( "--" );
                                    }

                                    // 難易度プロット
                                    plotDifficulty(workSheet, hdkStrtRow,
                                            mi15CalendarList2.getStrRankName(),
                                            mi15CalendarList2.getFloBorderTokuritsu());

                                    /** 合格可能性判定リスト **/
                                    // データセット
                                    ArrayList mi15Hyouka2 = mi15CalendarList2.getMi15HyoukaList();
                                    Iterator mi15Hyoukaitr = mi15Hyouka2.iterator();

                                    log.Ep("I15","合格可能性判定リストデータセット開始","");
                                    while(mi15Hyoukaitr.hasNext()){
                                        mI15HyoukaList mi15HyoukaList2 = (mI15HyoukaList) mi15Hyoukaitr.next();

                                        int col = COL_EXAM_1;
                                        boolean bolHyoujiFlg = true;
                                        if( cm.toString(mi15HyoukaList2.getStrHyoukaCenter()).equals("1") ){
                                        }else if( cm.toString(mi15HyoukaList2.getStrHyoukaCenter()).equals("2") ){
                                            col = col + 2;
                                        }else{
                                            bolHyoujiFlg = false;
                                        }
                                        if(bolHyoujiFlg){

                                            // 模試名セット
                                            msh = cm.setTaisyouMoshi( mi15HyoukaList2.getStrMshDate() );
                                            //2004.11.17 Update
                                            if( mi15HyoukaList2.getStrMshmei()!=null){
                                                workCell = cm.setCell( workSheet, workRow, workCell, 18, col );
                                                workCell.setCellValue( mi15HyoukaList2.getStrMshmei() + msh );
                                            }

                                            // 評価セット
                                            workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, col );
                                            workCell.setCellValue( mi15HyoukaList2.getStrHyouka() );
                                        }
                                    }
                                    log.Ep("I15","合格可能性判定リストデータセット終了","");

                                    /** 判定履歴リスト **/
                                    // データセット
                                    ArrayList mi15HanteiRireki2 = mi15CalendarList2.getMi15HanteiRirekiList();
                                    Iterator mi15HanteiRirekiitr = mi15HanteiRireki2.iterator();

                                    int hanteiCol = COL_JUDGE_HISTORY;

                                    log.Ep("I15","判定履歴リストデータセット開始","");
                                    while(mi15HanteiRirekiitr.hasNext()){
                                        mI15HanteiRirekiList mi15HanteiRirekiList2 = (mI15HanteiRirekiList) mi15HanteiRirekiitr.next();

                                        //2004.11.20 １回出力したら出力しない
                                        rirekiExistFlg = true;
                                        if( mshHyojiFlg == false){
                                            // 模試名セット
                                            msh = cm.setTaisyouMoshi( mi15HanteiRirekiList2.getStrMshDate() );
                                            //2004.11.17 Update
                                            if( mi15HanteiRirekiList2.getStrMshmei()!=null){
                                                workCell = cm.setCell( workSheet, workRow, workCell, 18, hanteiCol );
                                                workCell.setCellValue( mi15HanteiRirekiList2.getStrMshmei() + msh );
                                            }
                                        }

                                        // 評価セット
                                        workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, hanteiCol );
                                        workCell.setCellValue( mi15HanteiRirekiList2.getStrHyouka() );

                                        hanteiCol = hanteiCol + COL_COUNT_JUDGE_HISTORY;
                                        if(hanteiCol>MAX_COL_JUDGE_HISTORY){
                                            break;
                                        }
                                    }
                                    if( rirekiExistFlg == true){
                                        mshHyojiFlg = true;
                                    }

                                    log.Ep("I15","判定履歴リストデータセット終了","");

                                    // ２次教科配点セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_COURSE_ALLOT );
                                    workCell.setCellValue( mi15CalendarList2.getStrCourseAllot() );

                                    // 備考セット
                                    //2005.01.13 表示列を右に2つ移動(34→36)
                                    workCell = cm.setCell( workSheet, workRow, workCell, hdkStrtRow, COL_ALLOT_REMARK );
                                    workCell.setCellValue( mi15CalendarList2.getStrBiko() );

                                    intCntBeforeDisp++;
                                }
                                hdkStrtRow = hdkStrtRow2;
                            }
                        }
                    }
                    log.Ep("I15","2月より前のスケジュール出力処理終了","");
                }

                if( intCntBeforeDisp < intCntFebBefore ){
                }

                //11.10 AddEnd

                //2004.11.16 Add 最長連続受験日数出力
                //2004.11.18 連続日数初期値を1にする
                // ソート処理
                Object[] Sort = workjukenbiList.toArray();
                Arrays.sort(Sort);
                //カウント処理
                int intListSize = workjukenbiList.size();
                int intMaxRenzokuDay = 0;
                int intRenzokuDay = 1;
                for( int i=0; i<intListSize-1; i++ ){
                    String strBuff = (String)Sort[i];
                    int intyear = Integer.parseInt(strBuff.substring(0,4));
                    //2004.11.18 Update
                    int intmonth = Integer.parseInt(strBuff.substring(4,6))-1;
                    //Update End
                    int intday = Integer.parseInt(strBuff.substring(6));
                    Calendar cal1 = Calendar.getInstance();
                    cal1.set(intyear,intmonth,intday);
                    cal1.add(Calendar.DATE,1);

                    String strBuff2 = (String)Sort[i+1];
                    int intyear2 = Integer.parseInt(strBuff2.substring(0,4));
                    //2004.11.18 Update
                    int intmonth2 = Integer.parseInt(strBuff2.substring(4,6))-1;
                    //Update End
                    int intday2 = Integer.parseInt(strBuff2.substring(6));
                    Calendar cal2 = Calendar.getInstance();
                    cal2.set(intyear2, intmonth2, intday2);

                    //2004.11.18 Update
                    if( cal1.get(Calendar.YEAR)==cal2.get(Calendar.YEAR)
                        && cal1.get(Calendar.MONTH)+1==cal2.get(Calendar.MONTH)+1
                        && cal1.get(Calendar.DATE)==cal2.get(Calendar.DATE)){
                        intRenzokuDay++;
                    }
                    else{
                        if( intMaxRenzokuDay < intRenzokuDay ){
                            intMaxRenzokuDay = intRenzokuDay;
                        }
                        intRenzokuDay = 1;
                    }
                }
                if( intMaxRenzokuDay < intRenzokuDay ){
                    intMaxRenzokuDay = intRenzokuDay;
                }
                workCell = cm.setCell(workSheet, workRow, workCell, 4, 26);
                workCell.setCellValue("最長連続受験日数：" + intMaxRenzokuDay + "日");
                //Add End


                //2004.10.26 Add 判定個数カウント処理
                //判定個数カウント用配列を定義
                log.Ep("I15","判定個数カウント処理開始","");
                int HanteiCountCenter[] = {0,0,0,0,0};
                int HanteiCountSecond[] = {0,0,0,0,0};

                //判定個数
                for( int introw =20; introw<70; introw++ ){
                    workCell = cm.setCell( workSheet, workRow, workCell, introw, COL_JCENTER );
                    String strCenterHantei = workCell.getStringCellValue();

                    workCell = cm.setCell( workSheet, workRow, workCell, introw, COL_JSECOND );
                    String strSecondHantei = workCell.getStringCellValue();

                    //2004.11.2 Update G判定、H判定もカウントする→2004.11.21 カウントしない
                    for( int index=0; index<5; index++){
                        if( HanteiCount[index].equals(strCenterHantei) == true){
                            HanteiCountCenter[index]++;
                        }
                        if( HanteiCount[index].equals(strSecondHantei) == true){
                            HanteiCountSecond[index]++;
                        }
                    }
                    // end Update
                }

                //判定個数出力
                String strHanteiCountCenter = "";
                String strHanteiCountSecond = "";
                for( int index=0; index<5; index++ ){
                    if(index>0){
                        strHanteiCountCenter+=", ";
                        strHanteiCountSecond+=", ";
                    }
                    strHanteiCountCenter += HanteiCount[index] + "(" + String.valueOf(HanteiCountCenter[index]) + ")";
                    strHanteiCountSecond += HanteiCount[index] + "(" + String.valueOf(HanteiCountSecond[index]) + ")";
                }

                workCell = cm.setCell(workSheet, workRow, workCell, 3, 4);
                workCell.setCellValue(strHanteiCountCenter);

                workCell = cm.setCell(workSheet, workRow, workCell, 4, 4);
                workCell.setCellValue(strHanteiCountSecond);

                log.Ep("I15","判定個数カウント処理終了","");
                //Add End


                // Excelファイル保存
                boolean bolRet = false;
                String strFileName = "_" + cm.toString(i11ListBean.getStrGrade()) + cm.toString(i11ListBean.getStrClass()) + cm.toString(i11ListBean.getStrClassNum());
                if (  i11Item.getIntScheduleFormatFlg()==1){
                    //2005.01.13 表示列を右に2つ移動
                    workCell = cm.setCell( workSheet, workRow, workCell, 1, 78 );
                    workCell.setCellValue( "（先生用）" );
                    workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
                    //2004.10.25 Update
                    if( cm.toString(i11ListBean.getStrShimei()).equals("") == true){
                        workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrKana()) + "　性別：" + strSex + "　電話番号：" + cm.toString(i11ListBean.getStrTelNo() ) );
                    }
                    else{
                        workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrShimei()) + "(" + cm.toString(i11ListBean.getStrKana()) + ")" + "　性別：" + strSex + "　電話番号：" + cm.toString(i11ListBean.getStrTelNo() ) );
                    }
                    //Update End
                    bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile_t + strFileName, maxSheetIndex+1);
                }else if (  i11Item.getIntScheduleFormatFlg()==2){
                    //2005.01.13 表示列を右に2つ移動
                    workCell = cm.setCell( workSheet, workRow, workCell, 1, 78 );
                    workCell.setCellValue( "（生徒用）" );
                    workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
                    //2004.10.25 Update
                    if( cm.toString(i11ListBean.getStrShimei()).equals("") == true){
                        workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrKana()) + "　性別：" + strSex );
                    }
                    else{
                        workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrShimei()) + "(" + cm.toString(i11ListBean.getStrKana()) + ")" + "　性別：" + strSex );
                    }
                    //Update End
                    bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile_s + strFileName, maxSheetIndex+1);
                }else if (  i11Item.getIntScheduleFormatFlg()==3){
                    //2005.01.13 表示列を右に2つ移動
                    workCell = cm.setCell( workSheet, workRow, workCell, 1, 78 );
                    workCell.setCellValue( "（先生用）" );
                    workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
                    //2004.10.25 Update
                    if( cm.toString(i11ListBean.getStrShimei()).equals("") == true){
                        workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrKana()) + "　性別：" + strSex + "　電話番号：" + cm.toString(i11ListBean.getStrTelNo() ) );
                    }
                    else{
                        workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrShimei()) + "(" + cm.toString(i11ListBean.getStrKana()) + ")" + "　性別：" + strSex + "　電話番号：" + cm.toString(i11ListBean.getStrTelNo() ) );
                    }
                    bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile_t + strFileName, maxSheetIndex+1);

                    //2005.01.13 表示列を右に2つ移動
                    workCell = cm.setCell( workSheet, workRow, workCell, 1, 78 );
                    workCell.setCellValue( "（生徒用）" );
                    workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
                    //2004.10.25 Update
                    if( cm.toString(i11ListBean.getStrShimei()).equals("") == true ){
                        workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrKana()) + "　性別：" + strSex );
                    }
                    else{
                        workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrShimei()) + "(" + cm.toString(i11ListBean.getStrKana()) + ")" + "　性別：" + strSex );
                    }
                    bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile_s + strFileName, maxSheetIndex+1);
                }

                if( bolRet == false ){
                    return errfwrite;
                }

            }	// while( itr.hasNext() )

        } catch(Exception e) {
            e.printStackTrace();
            log.Err("I15","データセットエラー",e);
            return errfdata;
        }

        return noerror;
    }

     // 偏差値帯をプロットする
    private void plotDeviation(final I15KmkHensaListBean bean,
            final HSSFSheet sheet, final int row) {

        // 型がなければここまで
        if (bean == null) {
            return;
        }

        // 無効値ならここまで
        if ((int )bean.getFloHensa() == -999) {
            return;
        }

        final double min = 35.0;
        final double max = 82.5;
        final double interval = 2.5;

        // 総プロット数(20)
        final int upper = (int) ((max - min) / interval);

        // プロットインデックス 1〜20
        int position = (int)
            (((double) (bean.getFloHensa() - min)) / interval);

        // 上限を超えていたら、上限に設定
        if (position > upper) {
            position = upper;
        }
        // 下限を超えていたら、下限に設定
        if (position < 1) {
            position = 0;
        }

        cm.setCell(sheet, null, null, row, 6 + position).setCellValue("●");
    }

    // 難易度をプロットする
    private void plotDifficulty(final HSSFSheet sheet,
            final int row, final String rankName, final float rate) {

        // 難易ランクで表示
        if ((int) rate  == -999) {
            //「BF」ランク以外プロット
            if (rankName != null && !"BF".equals(rankName.toUpperCase())) {
                    plotByRank(sheet, row, rankName);
            }

        // センターボーダー得点率で表示
        } else {
            plotByBorder(sheet, row, rate);
        }
    }

    // 難易ランクでプロットする
    private void plotByRank(final HSSFSheet sheet,
            final int row, final String rankName) {

        final String name = rankName.toUpperCase();

        int index = 0;

        if ("BF".equals(name)) {
            throw new IllegalArgumentException("BFは許可されません。" + name);
        } else if ("M1".equals(name)) {
            index =+ 2;
        } else if ("M".equals(name)) {
            index =+ 1;
        } else {
            index =- Integer.parseInt(name);
        }

        cm.setCell(sheet, null, null, row, 22 + index).setCellValue("●");
    }

    // センターボーダ得点率でプロットする
    private void plotByBorder(final HSSFSheet sheet,
            final int row, float rate) {

        final double interval = 10.0 / 3.0;
        final double min = 30 + interval;
        final double max = 90 + interval * 3;

        // 総プロット数
        final int upper = (int) Math.round((max - min) / interval);

        // プロットインデックス
        int position = (int) (rate / interval) - (int) (min / interval);

        // 上限を超えていたら、上限に設定
        if (position >= upper) {
            position = upper - 1;
        }
        // 下限を超えていたら、下限に設定
        if (position < 0) {
            position = 0;
        }

        cm.setCell(sheet, null, null, row, 6 + position).setCellValue("●");
    }

}

//２月以降の受験データを格納するクラス

//大学スケジュールクラス
class mI15CalendarList{
    //受験日
    private String strJukenbi = "";
    //地方試験地
    private String strShikenti = "";
    //大学名
    private String strDaigakuMei = "";
    //学部
    private String strGakubuMei = "";
    //学科
    private String strGakkaMei = "";
    //複数パターンフラグ
    private int intPluralFlg = 0;
    //大学区分
    private String strDaigakuKbn = "";
    //日程コード
    private String strNittei = "";
    //出願締切日
    private String strSyutsuganbi = "";
    //合格発表日
    private String strGoukakubi = "";
    //入学手続締切日
    private String strTetsudukibi = "";
    //センターボーダー得点率
    private float floBorderTokuritsu = 0;
    //難易ランク偏差値
    private float floRankHensa = 0;
    //難易ランク名
    private String strRankName;
    //合格可能性判定リスト
    private ArrayList mi15HyoukaList = new ArrayList();
    //判定履歴リスト
    private ArrayList mi15HanteiRirekiList = new ArrayList();
    //２次教科配点
    private String strCourseAllot = "";
    //備考
    private String strBiko = "";

    /*----------*/
    /* Get      */
    /*----------*/

    public String getStrJukenbi() {
        return strJukenbi;
    }
    public String getStrShikenti() {
        return strShikenti;
    }
    public float getFloBorderTokuritsu() {
        return floBorderTokuritsu;
    }
    public float getFloRankHensa() {
        return floRankHensa;
    }
    public int getIntPluralFlg() {
        return intPluralFlg;
    }
    public ArrayList getMi15HanteiRirekiList() {
        return mi15HanteiRirekiList;
    }
    public ArrayList getMi15HyoukaList() {
        return mi15HyoukaList;
    }
    public String getStrBiko() {
        return strBiko;
    }
    public String getStrCourseAllot() {
        return strCourseAllot;
    }
    public String getStrDaigakuKbn() {
        return strDaigakuKbn;
    }
    public String getStrDaigakuMei() {
        return strDaigakuMei;
    }
    public String getStrGakkaMei() {
        return strGakkaMei;
    }
    public String getStrGakubuMei() {
        return strGakubuMei;
    }
    public String getStrGoukakubi() {
        return strGoukakubi;
    }
    public String getStrNittei() {
        return strNittei;
    }
    public String getStrSyutsuganbi() {
        return strSyutsuganbi;
    }
    public String getStrTetsudukibi() {
        return strTetsudukibi;
    }

    /*----------*/
    /* Set      */
    /*----------*/

    public void setStrJukenbi(String string) {
        strJukenbi = string;
    }
    public void setStrShikenti(String string) {
        strShikenti = string;
    }
    public void setFloBorderTokuritsu(float f) {
        floBorderTokuritsu = f;
    }
    public void setFloRankHensa(float f) {
        floRankHensa = f;
    }
    public void setIntPluralFlg(int i) {
        intPluralFlg = i;
    }
    public void setMi15HanteiRirekiList(ArrayList list) {
        mi15HanteiRirekiList = list;
    }
    public void setMi15HyoukaList(ArrayList list) {
        mi15HyoukaList = list;
    }
    public void setStrBiko(String string) {
        strBiko = string;
    }
    public void setStrDaigakuKbn(String string) {
        strDaigakuKbn = string;
    }
    public void setStrDaigakuMei(String string) {
        strDaigakuMei = string;
    }
    public void setStrCourseAllot(String string) {
        strCourseAllot = string;
    }
    public void setStrGakkaMei(String string) {
        strGakkaMei = string;
    }
    public void setStrGakubuMei(String string) {
        strGakubuMei = string;
    }
    public void setStrGoukakubi(String string) {
        strGoukakubi = string;
    }
    public void setStrNittei(String string) {
        strNittei = string;
    }
    public void setStrSyutsuganbi(String string) {
        strSyutsuganbi = string;
    }
    public void setStrTetsudukibi(String string) {
        strTetsudukibi = string;
    }
    /**
     * @return strRankName を戻します。
     */
    public String getStrRankName() {
        return strRankName;
    }
    /**
     * @param pStrRankName 設定する strRankName。
     */
    public void setStrRankName(String pStrRankName) {
        strRankName = pStrRankName;
    }

}

//合格可能判定クラス
class mI15HyoukaList{
    //評価区分
    private String strHyoukaCenter = "";
    //模試名
    private String strMshmei = "";
    //模試実施基準日
    private String strMshDate = "";
    //評価
    private String strHyouka = "";

    /*----------*/
    /* Get      */
    /*----------*/
    public String getStrHyoukaCenter() {
        return this.strHyoukaCenter;
    }
    public String getStrMshmei() {
        return this.strMshmei;
    }
    public String getStrMshDate() {
        return this.strMshDate;
    }
    public String getStrHyouka() {
        return this.strHyouka;
    }

    /*----------*/
    /* Set      */
    /*----------*/
    public void setStrHyoukaCenter(String strHyoukaCenter) {
        this.strHyoukaCenter = strHyoukaCenter;
    }
    public void setStrMshmei(String strMshmei) {
        this.strMshmei = strMshmei;
    }
    public void setStrMshDate(String strMshDate) {
        this.strMshDate = strMshDate;
    }
    public void setStrHyouka(String strHyouka) {
        this.strHyouka = strHyouka;
    }

}

//判定履歴クラス
class mI15HanteiRirekiList{
    //模試名
    private String strMshmei = "";
    //模試実施基準日
    private String strMshDate = "";
    //評価
    private String strHyouka = "";

    /*----------*/
    /* Get      */
    /*----------*/
    public String getStrMshmei() {
        return this.strMshmei;
    }
    public String getStrMshDate() {
        return this.strMshDate;
    }
    public String getStrHyouka() {
        return this.strHyouka;
    }

    /*----------*/
    /* Set      */
    /*----------*/
    public void setStrMshmei(String strMshmei) {
        this.strMshmei = strMshmei;
    }
    public void setStrMshDate(String strMshDate) {
        this.strMshDate = strMshDate;
    }
    public void setStrHyouka(String strHyouka) {
        this.strHyouka = strHyouka;
    }
}
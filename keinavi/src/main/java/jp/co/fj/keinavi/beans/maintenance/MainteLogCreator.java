package jp.co.fj.keinavi.beans.maintenance;

import java.io.Writer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * メンテログ作成Bean
 * 
 * 2006.11.08	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class MainteLogCreator extends DefaultBean {

	// 学校コード
	private final String schoolCd;
	// Writer
	private final Writer writer;
	
	// コンストラクタ
	public MainteLogCreator(final String pSchoolCd,
			final Writer pWriter) {
		schoolCd = pSchoolCd;
		writer = pWriter;
	}
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT "
					+ "TO_CHAR(logtime,'YYYY/MM/DD HH24:MI:SS.FF3') "
					+ "|| ',' || logdata "
					+ "FROM maintelog WHERE schoolcd = ? "
					+ "ORDER BY logtime");
			ps.setString(1, schoolCd);
			rs = ps.executeQuery();
			while (rs.next()) {
				writer.write(rs.getString(1) + "\r\n");
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

}

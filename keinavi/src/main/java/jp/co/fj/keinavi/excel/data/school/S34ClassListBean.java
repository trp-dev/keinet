package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * 志望大学評価別人数（校内成績）クラス別評価人数リスト
 * 作成日: 2004/07/08
 * @author	T.Kuzuno
 */
public class S34ClassListBean {
	//学校名
	private String strGakkomei = "";
	//学年
	private String strGrade = "";
	//クラス名
	private String strClass = "";
	//評価別人数データリスト
	private ArrayList s34HyoukaNinzuList = new ArrayList();
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrGrade() {
		return this.strGrade;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public ArrayList getS34HyoukaNinzuList() {
		return this.s34HyoukaNinzuList;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setS34HyoukaNinzuList(ArrayList s34HyoukaNinzuList) {
		this.s34HyoukaNinzuList = s34HyoukaNinzuList;
	}

}

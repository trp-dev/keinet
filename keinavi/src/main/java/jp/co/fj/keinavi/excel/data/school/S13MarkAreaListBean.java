package jp.co.fj.keinavi.excel.data.school;

/**
 * 設問別成績（校内成績）マーク模試高校別設問成績層正答率データリスト
 * 作成日: 2009/10/20
 * @author	Fujito URAKAWA
 */
public class S13MarkAreaListBean {

	//型・科目名コード
	private String strKmkCd = "";
	//型・科目名
	private String strKmkmei = "";
	//配点（型・科目）
	private String strHaitenKmk = "";
	//受験人数（校内）
	private int intNinzu = 0;
	//受験人数（全国）
	private int intNinzuAll = 0;
	//解答番号
	private String strKaitoNo = "";
	//完答レコードフラグ
	private int intKantoRcFlg = 0;
	//完答問題フラグ
	private int intKantoFlg = 0;
	// 問題番号
	private String strMonnum = "";
	// 大門・内容
	private String strDaimonNaiyo = "";
	// 正答率[校内]（全体）
	private float floSeitoritsuHome = 0;
	// 正答率[全国]（全体）
	private float floSeitoritsuAll = 0;
	// 受験人数（成績層S）
	private int intNinzuS = 0;
	// 正答率[校内]（成績層S）
	private float floSeitoritsuHomeS = 0;
	// 正答率[全国]（成績層S）
	private float floSeitoritsuAllS = 0;
	// 受験人数（成績層A）
	private int intNinzuA = 0;
	// 正答率[校内]（成績層A）
	private float floSeitoritsuHomeA = 0;
	// 正答率[全国]（成績層A）
	private float floSeitoritsuAllA = 0;
	// 受験人数（成績層B）
	private int intNinzuB = 0;
	// 正答率[校内]（成績層B）
	private float floSeitoritsuHomeB = 0;
	// 正答率[全国]（成績層B）
	private float floSeitoritsuAllB = 0;
	// 受験人数（成績層C）
	private int intNinzuC = 0;
	// 正答率[校内]（成績層C）
	private float floSeitoritsuHomeC = 0;
	// 正答率[全国]（成績層C）
	private float floSeitoritsuAllC = 0;
	// 受験人数（成績層D）
	private int intNinzuD = 0;
	// 正答率[校内]（成績層D）
	private float floSeitoritsuHomeD = 0;
	// 正答率[全国]（成績層D）
	private float floSeitoritsuAllD = 0;
	// 受験人数（成績層E）
	private int intNinzuE = 0;
	// 正答率[校内]（成績層E）
	private float floSeitoritsuHomeE = 0;
	// 正答率[全国]（成績層E）
	private float floSeitoritsuAllE = 0;
	// 受験人数（成績層F）
	private int intNinzuF = 0;
	// 正答率[校内]（成績層F）
	private float floSeitoritsuHomeF = 0;
	// 正答率[全国]（成績層F）
	private float floSeitoritsuAllF = 0;
	//2019/09/20 QQ)Oosaki 共通テスト対応 ADD START
	//レコードフラグ
        private int intRecordFlg = 0;
        //セット問題フラグ
        private int intSetqFlg = 0;
	//2019/09/20 QQ)Oosaki 共通テスト対応 ADD END

	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return strKmkCd;
	}
	public String getStrKmkmei() {
		return strKmkmei;
	}
	public String getStrHaitenKmk() {
		return strHaitenKmk;
	}
	public int getIntNinzu() {
		return intNinzu;
	}
	public String getStrKaitoNo() {
		return strKaitoNo;
	}
	public int getIntKantoRcFlg() {
		return intKantoRcFlg;
	}
	public int getIntKantoFlg() {
		return intKantoFlg;
	}
	public String getStrMonnum() {
		return strMonnum;
	}
	public String getStrDaimonNaiyo() {
		return strDaimonNaiyo;
	}
	public int getIntNinzuS() {
		return intNinzuS;
	}
	public float getFloSeitoritsuHomeS() {
		return floSeitoritsuHomeS;
	}
	public float getFloSeitoritsuAllS() {
		return floSeitoritsuAllS;
	}
	public int getIntNinzuA() {
		return intNinzuA;
	}
	public float getFloSeitoritsuHomeA() {
		return floSeitoritsuHomeA;
	}
	public float getFloSeitoritsuAllA() {
		return floSeitoritsuAllA;
	}
	public int getIntNinzuB() {
		return intNinzuB;
	}
	public float getFloSeitoritsuHomeB() {
		return floSeitoritsuHomeB;
	}
	public float getFloSeitoritsuAllB() {
		return floSeitoritsuAllB;
	}
	public int getIntNinzuC() {
		return intNinzuC;
	}
	public float getFloSeitoritsuHomeC() {
		return floSeitoritsuHomeC;
	}
	public float getFloSeitoritsuAllC() {
		return floSeitoritsuAllC;
	}
	public int getIntNinzuD() {
		return intNinzuD;
	}
	public float getFloSeitoritsuHomeD() {
		return floSeitoritsuHomeD;
	}
	public float getFloSeitoritsuAllD() {
		return floSeitoritsuAllD;
	}
	public int getIntNinzuE() {
		return intNinzuE;
	}
	public float getFloSeitoritsuHomeE() {
		return floSeitoritsuHomeE;
	}
	public float getFloSeitoritsuAllE() {
		return floSeitoritsuAllE;
	}
	public int getIntNinzuF() {
		return intNinzuF;
	}
	public float getFloSeitoritsuHomeF() {
		return floSeitoritsuHomeF;
	}
	public float getFloSeitoritsuAllF() {
		return floSeitoritsuAllF;
	}
	public int getIntNinzuAll() {
		return intNinzuAll;
	}
	public float getFloSeitoritsuHome() {
		return floSeitoritsuHome;
	}
	public float getFloSeitoritsuAll() {
		return floSeitoritsuAll;
	}
	//2019/09/20 QQ)Oosaki 共通テスト対応 ADD START
	public int getIntRecordFlg() {
	    return intRecordFlg;
	}
	public int getIntSetqFlg() {
	    return intSetqFlg;
	}
	//2019/09/20 QQ)Oosaki 共通テスト対応 ADD END
	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setStrKaitoNo(String strKaitoNo) {
		this.strKaitoNo = strKaitoNo;
	}
	public void setIntKantoRcFlg(int intKantoRcFlg) {
		this.intKantoRcFlg = intKantoRcFlg;
	}
	public void setIntKantoFlg(int intKantoFlg) {
		this.intKantoFlg = intKantoFlg;
	}
	public void setStrMonnum(String strMonnum) {
		this.strMonnum = strMonnum;
	}
	public void setStrDaimonNaiyo(String strDaimonNaiyo) {
		this.strDaimonNaiyo = strDaimonNaiyo;
	}
	public void setIntNinzuS(int intNinzuS) {
		this.intNinzuS = intNinzuS;
	}
	public void setFloSeitoritsuHomeS(float floSeitoritsuHomeS) {
		this.floSeitoritsuHomeS = floSeitoritsuHomeS;
	}
	public void setFloSeitoritsuAllS(float floSeitoritsuAllS) {
		this.floSeitoritsuAllS = floSeitoritsuAllS;
	}
	public void setIntNinzuA(int intNinzuA) {
		this.intNinzuA = intNinzuA;
	}
	public void setFloSeitoritsuHomeA(float floSeitoritsuHomeA) {
		this.floSeitoritsuHomeA = floSeitoritsuHomeA;
	}
	public void setFloSeitoritsuAllA(float floSeitoritsuAllA) {
		this.floSeitoritsuAllA = floSeitoritsuAllA;
	}
	public void setIntNinzuB(int intNinzuB) {
		this.intNinzuB = intNinzuB;
	}
	public void setFloSeitoritsuHomeB(float floSeitoritsuHomeB) {
		this.floSeitoritsuHomeB = floSeitoritsuHomeB;
	}
	public void setFloSeitoritsuAllB(float floSeitoritsuAllB) {
		this.floSeitoritsuAllB = floSeitoritsuAllB;
	}
	public void setIntNinzuC(int intNinzuC) {
		this.intNinzuC = intNinzuC;
	}
	public void setFloSeitoritsuHomeC(float floSeitoritsuHomeC) {
		this.floSeitoritsuHomeC = floSeitoritsuHomeC;
	}
	public void setFloSeitoritsuAllC(float floSeitoritsuAllC) {
		this.floSeitoritsuAllC = floSeitoritsuAllC;
	}
	public void setIntNinzuD(int intNinzuD) {
		this.intNinzuD = intNinzuD;
	}
	public void setFloSeitoritsuHomeD(float floSeitoritsuHomeD) {
		this.floSeitoritsuHomeD = floSeitoritsuHomeD;
	}
	public void setFloSeitoritsuAllD(float floSeitoritsuAllD) {
		this.floSeitoritsuAllD = floSeitoritsuAllD;
	}
	public void setIntNinzuE(int intNinzuE) {
		this.intNinzuE = intNinzuE;
	}
	public void setFloSeitoritsuHomeE(float floSeitoritsuHomeE) {
		this.floSeitoritsuHomeE = floSeitoritsuHomeE;
	}
	public void setFloSeitoritsuAllE(float floSeitoritsuAllE) {
		this.floSeitoritsuAllE = floSeitoritsuAllE;
	}
	public void setIntNinzuF(int intNinzuF) {
		this.intNinzuF = intNinzuF;
	}
	public void setFloSeitoritsuHomeF(float floSeitoritsuHomeF) {
		this.floSeitoritsuHomeF = floSeitoritsuHomeF;
	}
	public void setFloSeitoritsuAllF(float floSeitoritsuAllF) {
		this.floSeitoritsuAllF = floSeitoritsuAllF;
	}
	public void setIntNinzuAll(int intNinzuAll) {
		this.intNinzuAll = intNinzuAll;
	}
	public void setFloSeitoritsuHome(float floSeitoritsuHome) {
		this.floSeitoritsuHome = floSeitoritsuHome;
	}
	public void setFloSeitoritsuAll(float floSeitoritsuAll) {
		this.floSeitoritsuAll = floSeitoritsuAll;
	}
	//2019/09/20 QQ)Oosaki 共通テスト対応 ADD START
	public void setIntRecordFlg(int intRecordFlg) {
	    this.intRecordFlg = intRecordFlg;
	}
	public void setIntSetqFlg(int intSetqFlg) {
	    this.intSetqFlg = intSetqFlg;
	}
	//2019/09/20 QQ)Oosaki 共通テスト対応 ADD END
}

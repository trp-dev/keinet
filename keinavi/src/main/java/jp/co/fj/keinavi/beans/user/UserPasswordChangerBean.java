package jp.co.fj.keinavi.beans.user;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.util.KNValidator;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 利用者パスワード変更Bean
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class UserPasswordChangerBean extends DefaultBean {

	// 学校コード
	private final String schoolCd;
	// 利用者ID
	private final String loginId;
	// 旧パスワード
	private final String prePassword;
	// 新パスワード
	private final String newPassword;
	
	/**
	 * コンストラクタ
	 */
	public UserPasswordChangerBean(final String schoolCd, final String loginId,
			final String prePassword, final String newPassword) {
		
		if (schoolCd == null) {
			throw new IllegalArgumentException("学校コードがNULLです。");
		}
		
		if (prePassword == null) {
			throw new IllegalArgumentException("旧パスワードがNULLです。");
		}
		
		if (!KNValidator.isLoginPassword(newPassword)) {
			throw new IllegalArgumentException("新パスワードが不正です。" + newPassword);
		}
		
		this.schoolCd = schoolCd;
		this.loginId = loginId;
		this.prePassword = prePassword;
		this.newPassword = newPassword;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		PreparedStatement ps = null;
		try {
			// 利用者IDがNULL（ヘルプデスクの代行）
			if (loginId == null) {
				ps = conn.prepareStatement(
						QueryLoader.getInstance().getQuery("u08"));
				// 新パスワード
				ps.setString(1, newPassword);
				// 学校コード
				ps.setString(2, schoolCd);
				// 旧パスワード
				ps.setString(3, prePassword);
			// それ以外
			} else {
				ps = conn.prepareStatement(
						QueryLoader.getInstance().getQuery("u04"));
				// 新パスワード
				ps.setString(1, newPassword);
				// 学校コード
				ps.setString(2, schoolCd);
				// 利用者ID
				ps.setString(3, loginId);
				// 旧パスワード
				ps.setString(4, prePassword);
			}
			
			if (ps.executeUpdate() == 0) {
				throw new KNBeanException("旧パスワードが一致しません。", "1");
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
}

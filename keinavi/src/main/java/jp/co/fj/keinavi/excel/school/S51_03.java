/**
 * 校内成績分析−過回・過年度比較　成績概況
 * 	Excelファイル編集
 * 作成日: 2004/08/19
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S51Item;
import jp.co.fj.keinavi.excel.data.school.S51ListBean;
import jp.co.fj.keinavi.excel.data.school.S51YearListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S51_03 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー
	
	private CM		cm			= new CM();	//共通関数用クラス インスタンス
	
	final private String	masterfile0	= "S51_03";		// ファイル名
	final private String	masterfile1	= "NS51_03";	// ファイル名
	private String	masterfile	= "";					// ファイル名
//	final private String	strArea		= "A1:U75";	// 印刷範囲
	final private int[]	tabRow		= {5,17,29,41,53,65};	// 表の基準点
	final private int[]	tabCol		= {1,5,9,13,17};		// 表の基準点

/*
 * 	Excel編集メイン
 * 		S51Item s51Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
	public int s51_03EditExcel(S51Item s51Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		HSSFWorkbook		workbook		= null;
		HSSFSheet			workSheet		= null;
		HSSFRow				workRow			= null;
		HSSFCell			workCell		= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s51Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S51ListBean s51ListBean = new S51ListBean();
		
		try {
			int		row					= 0;		// 行
			int		intMaxSheetIndex	= 0;		// シートカウンター
			float		hensa				= 0;		// 最新模試の偏差値保持
			int		hyouCnt				= 0;		// 値範囲：０〜５(表カウンター)
			int		yearCnt				= 0;		// 値範囲：０〜４(年度カウンター)
			boolean	setFlg				= true;	// *セット判断フラグ
			boolean	bolSheetCngFlg		= true;	// 改シートフラグ
			
			//マスタExcel読み込み
			workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
			if( workbook==null ){
				return errfread;
			}
			
			// 型データセット
			ArrayList	s51List	= s51Item.getS51List();
			Iterator	itr		= s51List.iterator();
			
			while( itr.hasNext() ) {
				s51ListBean = (S51ListBean)itr.next();
				
				//改シートチェック
				if ( hyouCnt >= 6 ) {
					bolSheetCngFlg = true;
					hyouCnt = 0;
				}
				
				if(bolSheetCngFlg){
					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);
					intMaxSheetIndex++;
		
					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);
		
					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, s51Item.getIntSecuFlg() ,18 ,20 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 18 );
					workCell.setCellValue(secuFlg);
		
					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( "学校名　：" + cm.toString(s51Item.getStrGakkomei()) );
		
			
					// 対象模試セット
					String moshi =cm.setTaisyouMoshi( s51Item.getStrMshDate() );	// 模試月取得
					workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
					workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s51Item.getStrMshmei()) + moshi);
					bolSheetCngFlg = false;
				}
				
				// 型・科目名+配点セット
				String haiten = "";
				if ( !cm.toString(s51ListBean.getStrHaitenKmk()).equals("") ) {
					haiten="(" + s51ListBean.getStrHaitenKmk() + ")";
				}
				workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]-1, 0 );
				workCell.setCellValue( "型・科目：" + cm.toString(s51ListBean.getStrKmkmei()) + haiten );
				
				// 基本ファイルを読込む
				S51YearListBean s51YearListBean = new S51YearListBean();
				
				// 過年度データセット
				ArrayList s51YearList = s51ListBean.getS51YearList();
				Iterator itrYear = s51YearList.iterator();
				
				hensa = 0;
				yearCnt = 0;
				setFlg = true;
				String nendo = "";
				
				while ( itrYear.hasNext() ){
					s51YearListBean = (S51YearListBean)itrYear.next();
					
					if( !cm.toString(nendo).equals(cm.toString(s51YearListBean.getStrNendo()))){
						if (!cm.toString(nendo).equals("")) {
							yearCnt++;		
						}
						nendo = s51YearListBean.getStrNendo();
						row = 0;
					}
					if(yearCnt == 0){
						// 模試名セット
						String moshi =cm.setTaisyouMoshi( s51YearListBean.getStrMshDate() );	// 模試月取得
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, 0 );
						workCell.setCellValue( cm.toString(s51YearListBean.getStrMshmei()) + moshi );
						//最新模試の偏差値保持
						if ( s51YearListBean.getFloHensa()!=-999.0 && setFlg ) {
							hensa = s51YearListBean.getFloHensa();	
							setFlg = false;
						}
						// *セット
						if (s51YearListBean.getFloHensa() != -999.0) {
							if ( hensa < s51YearListBean.getFloHensa() ) {
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[0]+2 );
								workCell.setCellValue("*");
							}
						}
					}
					
					if( row == 0 ){
						// 年度セット
						if ( !cm.toString(s51YearListBean.getStrNendo()).equals("") ) {
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+row, tabCol[yearCnt] );
							workCell.setCellValue( s51YearListBean.getStrNendo() + "年度" );
						}
					}
					
					// 人数セット
					if ( s51YearListBean.getIntNinzu() != -999 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[yearCnt] );
						workCell.setCellValue( s51YearListBean.getIntNinzu() );
					}

					// 平均点セット
					if ( s51YearListBean.getFloHeikin() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[yearCnt]+1 );
						workCell.setCellValue( s51YearListBean.getFloHeikin() );
					}

					// 平均偏差値セット
					if ( s51YearListBean.getFloHensa() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+3+row, tabCol[yearCnt]+3 );
						workCell.setCellValue( s51YearListBean.getFloHensa() );
					}
					row++;
				}
				hyouCnt++;
			}
			
			// Excelファイル保存
			boolean bolRet = false;
			bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);

			if( bolRet == false ){
				return errfwrite;					
			}
		
		} catch(Exception e) {
			log.Err("S51_03","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

}

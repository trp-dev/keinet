package jp.co.fj.keinavi.servlets.maintenance;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 * 作業中宣言ダイアログサーブレット
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class X011Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		super.forward(request, response, JSP_X011);
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#isLogin(javax.servlet.http.HttpServletRequest)
	 */
	protected boolean isLogin(HttpServletRequest request) {
		return true;
	}

}

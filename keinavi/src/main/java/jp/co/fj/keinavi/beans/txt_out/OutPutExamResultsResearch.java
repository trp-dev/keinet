package jp.co.fj.keinavi.beans.txt_out;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 2005.11.01	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 * 
 * @author 奥村ゆかり
 *
 */
public class OutPutExamResultsResearch extends AbstractSheetBean{

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return true;
	}

	public void execute()throws IOException, SQLException, Exception{
		
		// 担当クラスを一時テーブルへ展開する
		super.setupCountChargeClass();
		
		final OutPutParamSet set = new OutPutParamSet(login, exam, profile, "t202", sessionKey);
		final List header = set.getHeader();
		
		// 出力対象は全て
		final List target = new LinkedList();
		{
			Iterator ite = header.iterator();
			while (ite.hasNext()) {
				String[] value = (String[]) ite.next();
				target.add(new Integer(value[0]));
			}
		}
		
		OutputTextBean out = new OutputTextBean();
		out.setOutFile(set.getOutPutFile());	
		out.setOutputTextList(getValues());
		out.setHeadTextList(header);
		out.setOutTarget((Integer[]) target.toArray(new Integer[0]));
		out.setOutType(set.getOutType()); // CSV固定
		out.execute();
		
		this.outfileList.add(set.getOutPutFile().getPath());
	}
	

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.T_COUNTING_SCORE;
	}
	

	/**
	 * 	成績概況を取得
	 * 
	 * @return list 成績概況のレコードオブジェクト
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @throws SQLException
	 * @throws IOException
	 * @see java.sql.SQLException
	 */
	public LinkedList getValues() throws SQLException, IOException {
		
		final LinkedList list = new LinkedList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("t05").toString());
			ps.setString(1, login.getUserID());
			ps.setString(2, exam.getExamYear());
			rs = ps.executeQuery();
			
			while (rs.next()) {
				LinkedHashMap data = new LinkedHashMap();
				data.put(new Integer("1"), rs.getString(1));
				data.put(new Integer("2"), rs.getString(2));
				data.put(new Integer("3"), rs.getString(3));
				data.put(new Integer("4"), rs.getString(4));
				data.put(new Integer("5"), rs.getString(5));
				data.put(new Integer("6"), rs.getString(6));
				data.put(new Integer("7"), rs.getString(7));
				data.put(new Integer("8"), rs.getString(8));
				data.put(new Integer("9"), rs.getString(9));
				data.put(new Integer("10"), rs.getString(10));
				data.put(new Integer("11"), rs.getString(11));
				data.put(new Integer("12"), rs.getString(12));
				list.add(data);
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}

		return list;
	}

}
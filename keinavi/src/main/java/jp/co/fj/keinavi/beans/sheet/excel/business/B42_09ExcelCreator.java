package jp.co.fj.keinavi.beans.sheet.excel.business;

import java.util.List;

import jp.co.fj.keinavi.beans.sheet.common.ISheetData;

/**
 *
 * 高校成績分析 - 高校間比較 - 偏差値分布
 * 受験学力測定テスト専用（前々年度まで）
 * 
 * 2006.09.04	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class B42_09ExcelCreator extends B42_08ExcelCreator {

	/**
	 * コンストラクタ
	 * 
	 * @param pData
	 * @param pSequenceId
	 * @param pOutFileList
	 * @param pPrintFlag
	 * @throws Exception
	 */
	public B42_09ExcelCreator(final ISheetData pData, final String pSequenceId,
			final List pOutFileList, final int pPrintFlag,
			final int pStartRowIndex, final int pMaxSchoolIndex) throws Exception {
		
		super(pData, pSequenceId, pOutFileList, pPrintFlag,
				pStartRowIndex, pMaxSchoolIndex);
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
	 * 			#getSheetId()
	 */
	protected String getSheetId() {
		return "B42_09";
	}

}

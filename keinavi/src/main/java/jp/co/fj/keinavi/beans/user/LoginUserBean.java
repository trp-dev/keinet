package jp.co.fj.keinavi.beans.user;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.data.user.LoginUserData;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 利用者情報Bean
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class LoginUserBean extends DefaultBean {

	// 学校コード
	private final String schoolCd;
	
	// 利用者ID
	private String loginId;
	// 利用者データのリスト
	private final List loginUserList = new ArrayList();
	
	/**
	 * コンストラクタ
	 */
	public LoginUserBean(final String schoolCd) {
		
		if (schoolCd == null) {
			throw new IllegalArgumentException("学校コードがNULLです。");
		}
		
		this.schoolCd = schoolCd;
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		// 利用者データのリストを作る
		createLoginUserList();
		// 利用者の担当クラスをセットアップする
		setupChargeClass();
	}

	/**
	 * SQLを生成する
	 * 
	 * @return
	 * @throws SQLException
	 */
	private String createQuery() throws SQLException {
		
		final StringBuffer query = new StringBuffer();
		query.append(QueryLoader.getInstance().load("u01"));
		
		// 利用者IDの指定がなければ並べ替える（一意にならない）
		if (loginId == null) {
			query.append("ORDER BY manage_flg, loginid");
			
		// 利用者IDの指定があるなら絞込条件を加える（一意になる）
		} else {
			query.append("AND m.loginid = ? ");
		}
		
		return query.toString();
	}

	/**
	 * 利用者データリストを作る
	 */
	private void createLoginUserList() throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(createQuery());
			
			// 学校コード
			ps.setString(1, schoolCd);
			
			// 利用者の指定があるなら絞り込む
			if (loginId != null) {
				ps.setString(2, loginId);
			}
			
			rs = ps.executeQuery();
			while (rs.next()) {
				final LoginUserData data = new LoginUserData();
				data.setLoginId(rs.getString(1));
				data.setOriginalLoginId(rs.getString(1));
				data.setLoginName(rs.getString(2));
				data.setDefaultPwd(rs.getString(4));
				data.setChangedPwd(!rs.getString(4).equals(rs.getString(3)));
				data.setManager("1".equals(rs.getString(5)));
				data.setMaintainer("1".equals(rs.getString(6)));
				data.setKnFunctionFlag(rs.getShort(7));
				data.setScFunctionFlag(rs.getShort(8));
				data.setEeFunctionFlag(rs.getShort(9));
				data.setAbFunctionFlag(rs.getShort(10));
				loginUserList.add(data);
			}
			
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	/**
	 * 利用者の担当クラス設定をセットする
	 */
	private void setupChargeClass() throws Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("u02").toString());
			
			// 学校コード
			ps.setString(1, schoolCd);
			
			final Iterator ite = loginUserList.iterator();
			while (ite.hasNext()) {
				final LoginUserData data = (LoginUserData) ite.next();
				
				// 利用者ID
				ps.setString(2, data.getLoginId());
				
				rs = ps.executeQuery();
				
				while (rs.next()) {
					data.addChargeClass(rs.getString(1), rs.getInt(2), rs.getString(3));
				}
				
				rs.close();
			}
			
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	/**
	 * @return loginUserList を戻します。
	 */
	public List getLoginUserList() {
		return loginUserList;
	}

	/**
	 * @param loginId 設定する loginId。
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	
}

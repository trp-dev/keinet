/*
 * 作成日: 2004/10/29
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.individual;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import jp.co.fj.keinavi.data.individual.PlanUnivData;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class PlanUnivSorter {
	
	public class SortByUnivToExamMode implements Comparator{

		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 * 大学コード・学部コード・学科ソートキー・入試形態コード・入試区分3・入試区分2・入試区分1・順
		 */
		public int compare(Object o1, Object o2) {
			PlanUnivData d1 = (PlanUnivData) o1;
			PlanUnivData d2 = (PlanUnivData) o2;
			if(Integer.parseInt(d1.getUnivCd()) < Integer.parseInt(d2.getUnivCd())){
				return -1;
			}else if(Integer.parseInt(d1.getUnivCd()) > Integer.parseInt(d2.getUnivCd())){
				return 1;
			}else{
				if(Integer.parseInt(d1.getFacultyCd()) < Integer.parseInt(d2.getFacultyCd())){
					return -1;
				}else if(Integer.parseInt(d1.getFacultyCd()) > Integer.parseInt(d2.getFacultyCd())){
					return 1;
				}else{
					if(d1.getDeptSortKey().compareTo(d2.getDeptSortKey()) < 0){
						return -1;
					}else if(d1.getDeptSortKey().compareTo(d2.getDeptSortKey()) > 0){
						return 1;
					}else{
						if(Integer.parseInt(d1.getEntExamTypeCd()) < Integer.parseInt(d2.getEntExamTypeCd())){
							return -1;
						}else if(Integer.parseInt(d1.getEntExamTypeCd()) > Integer.parseInt(d2.getEntExamTypeCd())){
							return 1;
						}else{
							return 0;
						}
					}
				}
			}
		}
		
	}
}
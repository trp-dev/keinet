/**
 * 校内成績分析−校内成績　設問別成績
 * 	Excelファイル編集
 * 作成日: 2004/07/13
 * @author	T.Sakai
 * 
 * コメント履歴帳票データセットクラス
 * 
 * 2009.10.06	Fujito URAKAWA - Totec
 * 			修正：全体成績表示処理を追加
 * 
 * 2010.01.21	Fujito URAKAWA - Totec
 *              成績層別成績が6段階（A〜F）から7段階に変更
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S13Item;
import jp.co.fj.keinavi.excel.data.school.S13SeisekiListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S13_01 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
//	private String		strArea			= "A1:AF63";	//印刷範囲
	
	final private String masterfile0 = "S13_01";
	final private String masterfile1 = "S13_01";
	private String masterfile = "";

/*
 * 	Excel編集メイン
 * 		S13Item s13Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s13_01EditExcel(S13Item s13Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
 		
		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s13Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		//マスタExcel読み込み
		workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
		if( workbook==null ){
			return errfread;
		}
		
		// 基本ファイルを読込む
		S13SeisekiListBean s13SeisekiListBean = new S13SeisekiListBean();
		
		try {
			// データセット
			ArrayList s13SeisekiList = s13Item.getS13SeisekiList();
			Iterator itr = s13SeisekiList.iterator();
			int	row					= 0;	//行
			int	col					= 0;	//列
			int	setsumonCnt			= 0;	//設問カウンタ
			int	hyouCnt				= 0;	//科目カウンタ
			int	intMaxSheetIndex	= 0;	//シートカウンタ
			int	setRow				= 0;	//*セット用
			String	kmk					= "";	//科目チェック用
			
			while( itr.hasNext() ) {
				s13SeisekiListBean = (S13SeisekiListBean)itr.next();
				//科目が変わる時のチェック
				if ( !cm.toString(kmk).equals(cm.toString(s13SeisekiListBean.getStrKmkmei())) ) {
					setsumonCnt = 0;
					if ( !cm.toString(kmk).equals("") ) {
						hyouCnt++;
						if ( hyouCnt==4 ) {
							hyouCnt = 0;
						}
					}
				}
				if (cm.toString(kmk).equals(cm.toString(s13SeisekiListBean.getStrKmkmei())) && setsumonCnt==0 ) {
					hyouCnt++;
					if ( hyouCnt==4 ) {
						hyouCnt = 0;
					}
				}
				//4表ごとに改シート
				if ( hyouCnt==0 && setsumonCnt==0 ) {
					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);
					intMaxSheetIndex++;

					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);
					
					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, s13Item.getIntSecuFlg(), 42, 45 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 42 );
					workCell.setCellValue(secuFlg);
					
					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
					workCell.setCellValue( "学校名　：" + cm.toString(s13Item.getStrGakkomei()) );

					// 模試月取得
					String moshi =cm.setTaisyouMoshi( s13Item.getStrMshDate() );
					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s13Item.getStrMshmei()) + moshi);
					
					// 学校名ヘッダデータセット
					workCell = cm.setCell( workSheet, workRow, workCell, 9, 0 );
					workCell.setCellValue( s13Item.getStrGakkomei() );
					workCell = cm.setCell( workSheet, workRow, workCell, 25, 0 );
					workCell.setCellValue( s13Item.getStrGakkomei() );
					workCell = cm.setCell( workSheet, workRow, workCell, 41, 0 );
					workCell.setCellValue( s13Item.getStrGakkomei() );
					workCell = cm.setCell( workSheet, workRow, workCell, 57, 0 );
					workCell.setCellValue( s13Item.getStrGakkomei() );
					// 県名ヘッダデータセット
					workCell = cm.setCell( workSheet, workRow, workCell, 10, 0 );
					workCell.setCellValue( s13Item.getStrKenmei() );
					workCell = cm.setCell( workSheet, workRow, workCell, 26, 0 );
					workCell.setCellValue( s13Item.getStrKenmei() );
					workCell = cm.setCell( workSheet, workRow, workCell, 42, 0 );
					workCell.setCellValue( s13Item.getStrKenmei() );
					workCell = cm.setCell( workSheet, workRow, workCell, 58, 0 );
					workCell.setCellValue( s13Item.getStrKenmei() );
				}
				if ( hyouCnt==0 ) {
					row = 4;
				}
				if ( hyouCnt==1 ) {
					row = 20;
				}
				if ( hyouCnt==2 ) {
					row = 36;
				}
				if ( hyouCnt==3 ) {
					row = 52;
				}
				if ( setsumonCnt==0 ) {
					// 型・科目名＋配点セット
					if ( hyouCnt==0 || !cm.toString(kmk).equals(cm.toString(s13SeisekiListBean.getStrKmkmei())) ) {
						String haiten = "";
						if ( !cm.toString(s13SeisekiListBean.getStrHaitenKmk()).equals("") ) {
							haiten = "（" + s13SeisekiListBean.getStrHaitenKmk() + "）";
						}
						workCell = cm.setCell( workSheet, workRow, workCell, row, 0 );
						workCell.setCellValue( "型・科目：" + cm.toString(s13SeisekiListBean.getStrKmkmei()) + haiten );
					}
					col = 2;
				}
				row++;
				// 設問番号セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
				workCell.setCellValue( s13SeisekiListBean.getStrSetsuNo() );
				// 設問内容セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
				workCell.setCellValue( s13SeisekiListBean.getStrSetsuMei1() );
				// 設問配点セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
				if ( !cm.toString(s13SeisekiListBean.getStrSetsuHaiten()).equals("") ) {
					workCell.setCellValue( "（" + s13SeisekiListBean.getStrSetsuHaiten() + "）" );
				}
				row++;
				// 人数・校内セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col );
				if ( s13SeisekiListBean.getIntNinzuHome() != -999 ) {
					workCell.setCellValue( s13SeisekiListBean.getIntNinzuHome() );
				}
				// 得点率・校内セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col+2 );
				if ( s13SeisekiListBean.getFloTokuritsuHome() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getFloTokuritsuHome() );
				}
				setRow = row - 1;
				// 人数・県セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col );
				if ( s13SeisekiListBean.getIntNinzuKen() != -999 ) {
					workCell.setCellValue( s13SeisekiListBean.getIntNinzuKen() );
				}
				// 得点率・県セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col+2 );
				if ( s13SeisekiListBean.getFloTokuritsuKen() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getFloTokuritsuKen() );
				}
				// 人数・全国セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col );
				if ( s13SeisekiListBean.getIntNinzuAll() != -999 ) {
					workCell.setCellValue( s13SeisekiListBean.getIntNinzuAll() );
				}
				// 得点率・全国セット
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col+2 );
				if ( s13SeisekiListBean.getFloTokuritsuAll() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getFloTokuritsuAll() );
				}
				
				// 人数・成績層Sセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col );
				if ( s13SeisekiListBean.getIntNinzuHomeS() != -999 ) {
					workCell.setCellValue( s13SeisekiListBean.getIntNinzuHomeS() );
				}
				// 得点率・成績層Sセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
				if ( s13SeisekiListBean.getFloTokuritsuHomeS() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getFloTokuritsuHomeS() );
					// *セット
					if ( s13SeisekiListBean.getAvgrateADevzoneS() > s13SeisekiListBean.getFloTokuritsuHomeS() && s13SeisekiListBean.getAvgrateADevzoneS() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						workCell.setCellValue("*");
					}
				}
				// 得点率・成績層Sセット（全国）
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col+3 );
				if ( s13SeisekiListBean.getAvgrateADevzoneS() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getAvgrateADevzoneS() );
				}
				// 人数・成績層Aセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col );
				if ( s13SeisekiListBean.getIntNinzuHomeA() != -999 ) {
					workCell.setCellValue( s13SeisekiListBean.getIntNinzuHomeA() );
				}
				// 得点率・成績層Aセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
				if ( s13SeisekiListBean.getFloTokuritsuHomeA() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getFloTokuritsuHomeA() );
					// *セット
					if ( s13SeisekiListBean.getAvgrateADevzoneA() > s13SeisekiListBean.getFloTokuritsuHomeA() && s13SeisekiListBean.getAvgrateADevzoneA() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						workCell.setCellValue("*");
					}
				}
				// 得点率・成績層Aセット（全国）
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col+3 );
				if ( s13SeisekiListBean.getAvgrateADevzoneA() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getAvgrateADevzoneA() );
				}
				// 人数・成績層Bセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col );
				if ( s13SeisekiListBean.getIntNinzuHomeB() != -999 ) {
					workCell.setCellValue( s13SeisekiListBean.getIntNinzuHomeB() );
				}
				// 得点率・成績層Bセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
				if ( s13SeisekiListBean.getFloTokuritsuHomeB() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getFloTokuritsuHomeB() );
					// *セット
					if ( s13SeisekiListBean.getAvgrateADevzoneB() > s13SeisekiListBean.getFloTokuritsuHomeB() && s13SeisekiListBean.getAvgrateADevzoneB() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						workCell.setCellValue("*");
					}
				}
				// 得点率・成績層Bセット（全国）
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col+3 );
				if ( s13SeisekiListBean.getAvgrateADevzoneB() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getAvgrateADevzoneB() );
				}
				// 人数・成績層Cセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col );
				if ( s13SeisekiListBean.getIntNinzuHomeC() != -999 ) {
					workCell.setCellValue( s13SeisekiListBean.getIntNinzuHomeC() );
				}
				// 得点率・成績層Cセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
				if ( s13SeisekiListBean.getFloTokuritsuHomeC() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getFloTokuritsuHomeC() );
					// *セット
					if ( s13SeisekiListBean.getAvgrateADevzoneC() > s13SeisekiListBean.getFloTokuritsuHomeC() && s13SeisekiListBean.getAvgrateADevzoneC() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						workCell.setCellValue("*");
					}
				}
				// 得点率・成績層Cセット（全国）
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col+3 );
				if ( s13SeisekiListBean.getAvgrateADevzoneC() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getAvgrateADevzoneC() );
				}
				// 人数・成績層Dセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col );
				if ( s13SeisekiListBean.getIntNinzuHomeD() != -999 ) {
					workCell.setCellValue( s13SeisekiListBean.getIntNinzuHomeD() );
				}
				// 得点率・成績層Dセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
				if ( s13SeisekiListBean.getFloTokuritsuHomeD() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getFloTokuritsuHomeD() );
					// *セット
					if ( s13SeisekiListBean.getAvgrateADevzoneD() > s13SeisekiListBean.getFloTokuritsuHomeD() && s13SeisekiListBean.getAvgrateADevzoneD() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						workCell.setCellValue("*");
					}
				}
				// 得点率・成績層Dセット（全国）
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col+3 );
				if ( s13SeisekiListBean.getAvgrateADevzoneD() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getAvgrateADevzoneD() );
				}
				// 人数・成績層Eセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col );
				if ( s13SeisekiListBean.getIntNinzuHomeE() != -999 ) {
					workCell.setCellValue( s13SeisekiListBean.getIntNinzuHomeE() );
				}
				// 得点率・成績層Eセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
				if ( s13SeisekiListBean.getFloTokuritsuHomeE() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getFloTokuritsuHomeE() );
					// *セット
					if ( s13SeisekiListBean.getAvgrateADevzoneE() > s13SeisekiListBean.getFloTokuritsuHomeE() && s13SeisekiListBean.getAvgrateADevzoneE() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						workCell.setCellValue("*");
					}
				}
				// 得点率・成績層Eセット（全国）
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col+3 );
				if ( s13SeisekiListBean.getAvgrateADevzoneE() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getAvgrateADevzoneE() );
				}
				// 人数・成績層Fセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col );
				if ( s13SeisekiListBean.getIntNinzuHomeF() != -999 ) {
					workCell.setCellValue( s13SeisekiListBean.getIntNinzuHomeF() );
				}
				// 得点率・成績層Fセット
				workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
				if ( s13SeisekiListBean.getFloTokuritsuHomeF() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getFloTokuritsuHomeF() );
					// *セット
					if ( s13SeisekiListBean.getAvgrateADevzoneF() > s13SeisekiListBean.getFloTokuritsuHomeF() && s13SeisekiListBean.getAvgrateADevzoneF() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						workCell.setCellValue("*");
					}
				}
				// 得点率・成績層Fセット（全国）
				workCell = cm.setCell( workSheet, workRow, workCell, row++, col+3 );
				if ( s13SeisekiListBean.getAvgrateADevzoneF() != -999.0 ) {
					workCell.setCellValue( s13SeisekiListBean.getAvgrateADevzoneF() );
				}
				// *セット
				if ( s13SeisekiListBean.getFloTokuritsuAll() > s13SeisekiListBean.getFloTokuritsuHome() && s13SeisekiListBean.getFloTokuritsuHome() != -999.0 ) {
					workCell = cm.setCell( workSheet, workRow, workCell, setRow, col+1 );
					workCell.setCellValue("*");
				}
				// 全体成績追加に伴い、「9」から「10」に変更
				if ( setsumonCnt==10 ) {
					setsumonCnt = 0;
					if ( hyouCnt==4 ) {
						hyouCnt = 0;
					}
				}else {
					setsumonCnt++;
				}
				
				col = col + 4;
				kmk = s13SeisekiListBean.getStrKmkmei();
			}
//add 2004/10/25 T.Sakai データ0件対応
			if ( s13SeisekiList.size()==0 ) {
				// シートテンプレートのコピー
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex++;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
					
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s13Item.getIntSecuFlg(), 42, 45 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 42 );
				workCell.setCellValue(secuFlg);
					
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s13Item.getStrGakkomei()) );

				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s13Item.getStrMshDate() );
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s13Item.getStrMshmei()) + moshi);
					
				// 学校名ヘッダデータセット
				workCell = cm.setCell( workSheet, workRow, workCell, 9, 0 );
				workCell.setCellValue( s13Item.getStrGakkomei() );
				workCell = cm.setCell( workSheet, workRow, workCell, 25, 0 );
				workCell.setCellValue( s13Item.getStrGakkomei() );
				workCell = cm.setCell( workSheet, workRow, workCell, 41, 0 );
				workCell.setCellValue( s13Item.getStrGakkomei() );
				workCell = cm.setCell( workSheet, workRow, workCell, 57, 0 );
				workCell.setCellValue( s13Item.getStrGakkomei() );
				// 県名ヘッダデータセット
				workCell = cm.setCell( workSheet, workRow, workCell, 10, 0 );
				workCell.setCellValue( s13Item.getStrKenmei() );
				workCell = cm.setCell( workSheet, workRow, workCell, 26, 0 );
				workCell.setCellValue( s13Item.getStrKenmei() );
				workCell = cm.setCell( workSheet, workRow, workCell, 42, 0 );
				workCell.setCellValue( s13Item.getStrKenmei() );
				workCell = cm.setCell( workSheet, workRow, workCell, 58, 0 );
				workCell.setCellValue( s13Item.getStrKenmei() );
			}
//add end
			// Excelファイル保存
			boolean bolRet = false;
			bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("S13_01","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

}
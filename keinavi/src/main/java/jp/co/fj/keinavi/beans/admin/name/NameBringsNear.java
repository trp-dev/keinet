package jp.co.fj.keinavi.beans.admin.name;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import jp.co.fj.keinavi.beans.admin.FormatChecker;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 名寄せ判定クラス
 *
 *
 * @author TOTEC)OKUMURA.Yukari
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)YAMADA.Tomohisa
 *
 */
public class NameBringsNear extends DefaultBean {

    /** serialVersionUID */
    private static final long serialVersionUID = -2189622044945216096L;

    /** 学校コード/在卒校コードの初期値 */
    private static final String INIT_SCHOOL_CD = "98998";

    /** 結合状態 */
    private int combiJudge = 1;

    /** 名寄せ対象の個表データ */
    private HashMap inputData;

    /** 結合結果レコード */
    private List result = new ArrayList();

    /**
     * {@inheritDoc}
     */
    public void execute() throws SQLException {

        this.result.clear();

        /* 学校コードが初期値なら名寄せしない */
        if (INIT_SCHOOL_CD.equals(inputData.get("SCHOOLCD"))) {
            combiJudge = 3;
            return;
        }

        /* 判定条件１を判定する(顧客番号がオール9なら判定しない) */
        if (!"99999999999".equals(inputData.get("CUSTOMER_NO"))) {
            judgment1();
        }

        if (this.result.size() == 0) {
            /* 判定条件１の一致データなし */

            /* 判定条件２を判定する */
            List data2 = judgment2();

            if (data2.size() == 0) {
                /* 判定条件２の一致データなし */

                /* 判定条件３を判定する */
                List data3 = judgment3();

                if (data3.size() == 0) {
                    /* 判定条件３の一致データなし→結合状態�V */
                    combiJudge = 3;
                } else {
                    /* 判定条件３の一致データあり */

                    /* 【電話番号】または、【生年月日】のいずれかが一致する「学籍基本情報」を探す */
                    getJudgeValue3(data3);

                    if (this.result.size() > 0) {
                        /* 一致データあり */
                        if (isExistsHistoryInfo()) {
                            /* 当該年度の学籍履歴情報あり→結合状態�T */
                            combiJudge = 1;
                        } else {
                            /* 当該年度の学籍履歴情報あり→結合状態�U */
                            combiJudge = 2;
                        }
                    } else {
                        /* 一致データなし→結合状態�V */
                        combiJudge = 3;
                    }
                }
            } else {
                /* 判定条件２の一致データあり */

                /*
                 * 『【カナ氏名】が完全一致』
                 * または『【電話番号】一致かつ【カナ氏名】一部一致』
                 * または『【生年月日】一致かつ【カナ氏名】一部一致』
                 * を満たす「学籍基本情報」を探す
                 */
                getJudgeValue2(data2);

                if (this.result.size() > 0) {
                    /* 一致データあり→結合状態�T */
                    combiJudge = 1;
                } else {
                    /* 一致データなし→結合状態�V */
                    combiJudge = 3;
                }
            }
        } else {
            /* 判定条件１の一致データあり */
            if (isExistsHistoryInfo()) {
                /* 当該年度の学籍履歴情報あり→結合状態�T */
                combiJudge = 1;
            } else {
                /* 当該年度の学籍履歴情報なし→結合状態�U */
                combiJudge = 2;
            }
        }
    }

    /**
     * 『【カナ氏名】が完全一致』
     * または『【電話番号】一致かつ【カナ氏名】一部一致』
     * または『【生年月日】一致かつ【カナ氏名】一部一致』
     * を満たす「学籍基本情報」を探す。
     *
     * @param data 判定条件２で一致したデータ
     */
    private void getJudgeValue2(List data) {

        /* 個表：かな氏名 */
        String kanaNameI = (String) inputData.get("NAME_KANA");

        /* 個表：生年月日 */
        String birthDateI =
                (String) inputData.get("BIRTHYEAR") + inputData.get("BIRTHMONTH") + inputData.get("BIRTHDATE");

        /* 個表：電話番号 */
        String telNumberI = (String) inputData.get("TEL_NO");

        /* かな氏名、電話番号、生年月日のいずれかが一致する */
        Iterator ite = data.iterator();
        while (ite.hasNext()) {
            Map map = (Map) ite.next();

            boolean match = false;

            /* 【カナ氏名】が完全一致 */
            String kanaNameS = (String) map.get("NAME_KANA");
            if (FormatChecker.compareValue(kanaNameI, kanaNameS)) {
                match = true;
            }

            /* 【電話番号】一致かつ【カナ氏名】一部一致 */
            if (!match) {
                String telNumberS = (String) map.get("TEL_NO");
                if (compareTelNo(telNumberI, telNumberS) && comparePartialNameKana(kanaNameI, kanaNameS)) {
                    match = true;
                }
            }

            /* 【生年月日】一致かつ【カナ氏名】一部一致 */
            if (!match) {
                String birthDateS = (String) map.get("BIRTHDAY");
                if (FormatChecker.compareValue(birthDateI, birthDateS)
                        && comparePartialNameKana(kanaNameI, kanaNameS)) {
                    match = true;
                }
            }

            /* どれか一致していればOK */
            if (match) {
                this.result.add(map.get("INDIVIDUALID"));
            }
        }
    }

    /**
     * 電話番号を比較します。
     *
     * @param telNo1 電話番号1
     * @param telNo2 電話番号2
     * @return 一致するならtrue
     */
    private boolean compareTelNo(String telNo1, String telNo2) {
        return FormatChecker.compareValue(FormatChecker.StringDelIndex(telNo1, "-()"),
                FormatChecker.StringDelIndex(telNo2, "-()"));
    }

    /**
     * カナ氏名を部分一致で比較します。
     *
     * @param nameKana1 カナ氏名1
     * @param nameKana2 カナ氏名2
     * @return 一致するならtrue
     */
    private boolean comparePartialNameKana(String nameKana1, String nameKana2) {

        /* 初期値(NULL)ならここまで */
        if (nameKana1 == null || nameKana1.length() == 0 || nameKana2 == null || nameKana2.length() == 0) {
            return false;
        }

        /* 比較する相互のカナ氏名から「ﾞ」･「ﾟ」･「空白」を除く */
        String searchChars = "ﾞﾟ ";
        String kana1 = StringUtils.replaceChars(nameKana1, searchChars, null);
        String kana2 = StringUtils.replaceChars(nameKana2, searchChars, null);

        /* 文字数が一致していていなければ不一致とする */
        if (kana1.length() != kana2.length()) {
            return false;
        }

        if (kana1.length() <= 3) {
            /* 文字数が3文字以下の場合：文字列が完全に一致している場合に一致とする */
            return kana1.equals(kana2);
        } else {
            /* 文字数が4文字以上の場合：1文字違いまでを一致とする */
            int mismatchCnt = 0;
            for (int i = 0; i < kana1.length(); i++) {
                if (kana1.charAt(i) != kana2.charAt(i)) {
                    if (mismatchCnt++ == 1) {
                        return false;
                    }
                }
            }
            return true;
        }
    }

    /**
     * 【電話番号】または、【生年月日】のいずれかが一致する「学籍基本情報」を探す。
     *
     * @param data 判定条件３で一致したデータ
     */
    private void getJudgeValue3(List data) {

        /* 個表：生年月日 */
        String birthDateI =
                (String) inputData.get("BIRTHYEAR") + inputData.get("BIRTHMONTH") + inputData.get("BIRTHDATE");

        /* 個表：電話番号 */
        String telNumberI = (String) inputData.get("TEL_NO");

        Iterator ite = data.iterator();
        while (ite.hasNext()) {
            Map map = (Map) ite.next();

            boolean match = false;

            /* 【電話番号】一致 */
            String telNumberS = (String) map.get("TEL_NO");
            if (compareTelNo(telNumberI, telNumberS)) {
                match = true;
            }

            /* 【生年月日】一致 */
            if (!match) {
                String birthDateS = (String) map.get("BIRTHDAY");
                if (FormatChecker.compareValue(birthDateI, birthDateS)) {
                    match = true;
                }
            }

            /* どれか一致していればOK */
            if (match) {
                this.result.add(map.get("INDIVIDUALID"));
            }
        }
    }

    /**
     * 当該年度の「学籍履歴情報」を探す。
     *
     * @return 当該年度の「学籍履歴情報」が存在するならtrue
     * @throws SQLException SQL例外
     */
    private boolean isExistsHistoryInfo() throws SQLException {

        String query = "SELECT 1 FROM HISTORYINFO WHERE INDIVIDUALID = ? AND YEAR = ?";

        List valid = new LinkedList();
        List inValid = new LinkedList();

        String year = (String) inputData.get("EXAMYEAR");

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(query);
            ps.setString(2, year);

            Iterator ite = this.result.iterator();
            while (ite.hasNext()) {
                String individualId = (String) ite.next();

                ps.setString(1, individualId);
                rs = ps.executeQuery();

                if (rs.next()) {
                    valid.add(individualId);
                } else {
                    inValid.add(individualId);
                }

                rs.close();
            }
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(ps);
        }

        this.result.clear();
        this.result.addAll(valid);
        this.result.addAll(inValid);

        return valid.size() > 0;
    }

    /**
     * 判定条件１：【学校コード】【顧客番号】が一致する「学籍基本情報」を探す。
     *
     * @throws SQLException SQL例外
     */
    private void judgment1() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("SELECT b.INDIVIDUALID FROM BASICINFO b "
                    + "WHERE b.CUSTOMER_NO = ? AND b.SCHOOLCD = ? AND b.NOTDISPLAYFLG IS NULL");
            ps.setString(1, (String) inputData.get("CUSTOMER_NO"));
            ps.setString(2, (String) inputData.get("SCHOOLCD"));
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(rs.getString(1));
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    /**
     * 判定条件２：【学校コード】【在卒校コード】【学年】【クラス】【クラス番号】【性別】が一致する
     * 「学籍基本情報」及び「学籍履歴情報」を探す。
     *
     * @throws SQLException SQL例外
     */
    private LinkedList judgment2() throws SQLException {

        final LinkedList list = new LinkedList();

        /* 学年・クラス・性別・在卒校コードが初期値ならここまで */
        if ("9".equals(inputData.get("GRADE")) || "  ".equals(inputData.get("CLASS"))
                || "9".equals(inputData.get("SEX")) || INIT_SCHOOL_CD.equals(inputData.get("HOMESCHOOLCD"))) {
            return list;
        }

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("SELECT /*+ ORDERED USE_NL(b h) INDEX(BASICINFO_IDX) INDEX(h PK_HISTORYINFO) */ "
                    + "h.INDIVIDUALID, h.year, b.NAME_KANA, TRIM(b.BIRTHDAY), TRIM(b.TEL_NO) "
                    + "FROM BASICINFO b,HISTORYINFO h WHERE h.INDIVIDUALID = b.INDIVIDUALID AND b.SCHOOLCD = ? "
                    + "AND b.HOMESCHOOLCD = ? AND h.GRADE = ? AND UPPER(h.class) = UPPER(?) "
                    + "AND TO_NUMBER(h.class_no) = TO_NUMBER(?) AND b.SEX = ? AND h.year = ? "
                    + "AND b.NOTDISPLAYFLG IS NULL");
            ps.setString(1, (String) inputData.get("SCHOOLCD"));
            ps.setString(2, (String) inputData.get("HOMESCHOOLCD"));
            ps.setInt(3, Integer.parseInt((String) inputData.get("GRADE")));
            ps.setString(4, (String) inputData.get("CLASS"));
            ps.setString(5, (String) inputData.get("CLASS_NO"));
            ps.setString(6, (String) inputData.get("SEX"));
            ps.setString(7, (String) inputData.get("EXAMYEAR"));
            rs = ps.executeQuery();
            while (rs.next()) {
                HashMap data = new HashMap();
                data.put("INDIVIDUALID", rs.getString(1));
                data.put("YEAR", rs.getString(2));
                data.put("NAME_KANA", rs.getString(3));
                data.put("BIRTHDAY", rs.getString(4));
                data.put("TEL_NO", rs.getString(5));
                list.add(data);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }

        return list;
    }

    /**
     * 判定条件３：【学校コード】と【カナ氏名】が完全一致する「学籍基本情報」を探す。
     *
     * @throws SQLException SQL例外
     */
    private List judgment3() throws SQLException {

        final List list = new LinkedList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("SELECT b.INDIVIDUALID, TRIM(b.BIRTHDAY), TRIM(b.TEL_NO) FROM BASICINFO b "
                    + "WHERE b.SCHOOLCD = ? AND b.NAME_KANA = ? AND b.NOTDISPLAYFLG IS NULL");
            ps.setString(1, (String) inputData.get("SCHOOLCD"));
            ps.setString(2, (String) inputData.get("NAME_KANA"));
            rs = ps.executeQuery();
            while (rs.next()) {
                HashMap data = new HashMap();
                data.put("INDIVIDUALID", rs.getString(1));
                data.put("BIRTHDAY", rs.getString(2));
                data.put("TEL_NO", rs.getString(3));
                list.add(data);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }

        return list;
    }

    /**
     * 結合結果レコードを返します。
     *
     * @return 結合結果レコード
     */
    public List getResult() {
        return result;
    }

    /**
     * 名寄せ対象の個表データを設定します。
     *
     * @param map 名寄せ対象の個表データ
     */
    public void setInputData(HashMap map) {
        inputData = map;
    }

    /**
     * 結合状態を返します。
     *
     * @return 結合状態
     */
    public int getCombiJudge() {
        return combiJudge;
    }

}

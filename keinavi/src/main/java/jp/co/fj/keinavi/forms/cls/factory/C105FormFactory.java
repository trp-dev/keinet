/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.cls.factory;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.forms.cls.CMaxForm;

import com.fjh.forms.ActionForm;

/**
 * 
 * 2005.04.05	Yoshimoto KAWAI - Totec
 * 				過年度の表示対応
 * 
 * 2005.04.11	Yoshimoto KAWAI - Totec
 * 				日程・評価区分対応
 * 
 * @author kawai
 * 
 */
public class C105FormFactory extends AbstractCFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		// アクションフォーム
		CMaxForm form = super.createCMaxForm(request);
		// アイテムマップ
		Map map = getItemMap(request);
		
		//  表
		getChart( map, form );
		//  大学の表示順序
		getUnivOrder( map, form );
		//  大学集計区分
		getTotalUnivDiv( map, form );
		//  表示対象
		getUnivStudent( map, form );
		//  日程
		getUnivSchedule( map, form );
		//  評価区分
		getUnivRating( map, form );
		//  クラスの表示順序
		getClassOrder( map, form );
		//  セキュリティスタンプ
		getStamp( map, form );
		// 過年度の表示
		getDispUnivYear(map, form);
		
		return form;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
		CMaxForm form = (CMaxForm)request.getAttribute("form");
		// アイテムマップ
		Map map = getItemMap(request);
		
		//  表
		setChart( map, form );
		//  大学の表示順序
		setUnivOrder( map, form );
		//  大学集計区分
		setTotalUnivDiv( map, form );
		//  表示対象
		setUnivStudent( map, form );
		//  日程
		setUnivSchedule( map, form );
		//  評価区分
		setUnivRating( map, form );
		//  クラスの表示順序
		setClassOrder( map, form );
		//  セキュリティスタンプ
		setStamp( map, form );
		// 過年度の表示
		setDispUnivYear(map, form);
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap("030204");
	}

}

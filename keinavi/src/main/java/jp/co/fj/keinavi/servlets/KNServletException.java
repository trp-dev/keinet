/*
 * 作成日: 2004/08/20
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets;

import javax.servlet.ServletException;

/**
 * Kei-Navi用例外クラス
 * 
 * @author kawai
 */
public class KNServletException extends ServletException {

	private String errorCode = "0"; // エラーコード
	private String errorMessage; // エラーメッセージ

	// エラー画面に閉じるボタンを表示するかどうか
	// falseなら戻るボタンを表示する
	private boolean close;

	/**
	 * コンストラクタ
	 */
	public KNServletException() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param arg0
	 */
	public KNServletException(String message) {
		super(message);
		this.errorMessage = message;
	}

	/**
	 * エラーメッセージを表示するかどうか
	 * @return
	 */
	public boolean isShowMessage() {
		return this.getErrorCode().startsWith("1");
	}

	/**
	 * @return
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @return
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param string
	 */
	public void setErrorCode(String string) {
		errorCode = string;
	}

	/**
	 * @param string
	 */
	public void setErrorMessage(String string) {
		errorMessage = string;
	}

	/**
	 * @return
	 */
	public boolean isClose() {
		return close;
	}

	/**
	 * @param b
	 */
	public void setClose(boolean b) {
		close = b;
	}

}

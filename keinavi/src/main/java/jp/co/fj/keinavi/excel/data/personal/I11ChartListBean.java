package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
import java.util.ArrayList;
/**
 * �l���ѕ��́|���ѕ��͖ʒk �o�����X�`���[�g�f�[�^���X�g
 * �쐬��: 2004/07/20
 * @author	A.Iwata
 */
public class I11ChartListBean {
	//�͎���
	private String strMshmei = "";
	//�͎����{���
	private String strMshDate = "";
	//1���ȕ����󌱃t���O
	private int intKyokaDispFlg = 0;
	//���ȕʕ΍��l���X�g
	private ArrayList i11KyokaHensaList = new ArrayList();
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public int getIntKyokaDispFlg() {
		return this.intKyokaDispFlg;
	}
	public ArrayList getI11KyokaHensaList() {
		return this.i11KyokaHensaList;
	}
		
	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setIntKyokaDispFlg(int intKyokaDispFlg) {
		this.intKyokaDispFlg = intKyokaDispFlg;
	}
	public void setI11KyokaHensaList(ArrayList i11KyokaHensaList) {
		this.i11KyokaHensaList = i11KyokaHensaList;
	}
}

/*
 * 作成日: 2004/10/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.individual;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class IPropsLoader {
	
	//String centerExam = IPropsLoader.getInstance().getMessage("CENTER_RESEARCH_EXAM");

	// Singleton
	private static IPropsLoader instance = null;
	
	//プロパティファイル
	private final String properties = "iprops.properties";

	/*エラーメッセージを格納するプロパティオブジェクト*/
	private Properties props = new Properties();

	/**
	 * コンストラクタ
	 */
	private IPropsLoader() {
		// 設定を読み込む
		InputStream in = IPropsLoader.class.getResourceAsStream(properties);
		//InputStream in = new FileInputStream(IPROPS_FILE_PATH);

		if (in == null) {
			throw new IllegalArgumentException("properties file not found.");
		}

		try {
			props.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * このクラスのインスタンスを返す
	 * @return Singletonなインスタンス
	 */
	public static IPropsLoader getInstance() {
		if (instance == null) {
			instance = new IPropsLoader();
		}
		return instance;
	}

	/**
	 * エラーコードに対応したメッセージを取得するためのゲッターメソッド
	 * @param errorCode
	 * @return
	 */
	public String getMessage(String key) {
		return props.getProperty(key);
	}

}

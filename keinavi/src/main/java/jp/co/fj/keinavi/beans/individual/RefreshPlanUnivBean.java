package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 大学コード紐付けBean
 * 
 * 2005.12.20	[新規作成]
 * 				校内成績処理システムで開発した処理高速化モジュールを移植
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class RefreshPlanUnivBean extends DefaultBean{

	//最新年度
	private final String newExamYear;
	//最新模試区分
	private final String newExamDiv;
	//学校コード
	private final String schoolCd;
	
	// 個人ID
	private String individualId;
	// 更新対象を取得するSQL
	private String query;
	
	/**
	 * コンストラクタ
	 * 
	 * @param year 最新マスタ年度
	 * @param div 最新マスタ模試区分
	 * @param school 学校コード
	 */
	public RefreshPlanUnivBean(
			final String year, final String div, final String school) {

		if (year == null) {
			throw new IllegalArgumentException(
					"年度がNULLです。");
		}
		
		if (div == null) {
			throw new IllegalArgumentException(
					"区分がNULLです。");
		}
		if (school == null) {
			throw new IllegalArgumentException(
					"学校コードがNULLです。");
		}
		
        this.newExamYear = year;
        this.newExamDiv = div;
        this.schoolCd = school;
	}
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// SQL初期化処理
		initQuery();
		//入試結果情報の更新
		updatePlanUniv();
		//高３大学マスタにない大学を入試結果情報より削除
		deletePlanUniv();
	}

	private void initQuery() throws SQLException {
		
		if (query != null) {
			return;
		}
		
		final Query q = QueryLoader.getInstance().load("univrefresh_01");
		
		// デフォルトは学校コードでの絞り込みだが、
		// 個人IDが指定されているなら書き換える
		if (individualId != null) {
			q.replaceAll("bi.schoolcd = ?", "bi.individualid = ?");
			q.replaceAll("BASICINFO_IDX", "PK_BASICINFO");
		}
		
		query = q.toString();
	}

	//入試結果情報の更新
	private void updatePlanUniv() throws SQLException {
		
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		ResultSet rs = null;

		try {
			ps1 = conn.prepareStatement(query);
			ps2 = conn.prepareStatement(
					QueryLoader.getInstance().load("univrefresh_02").toString());
			ps3 = conn.prepareStatement(
					QueryLoader.getInstance().load("univrefresh_03").toString());
			ps4 = conn.prepareStatement(
					QueryLoader.getInstance().load("univrefresh_04").toString());

			//更新対象検索
			ps1.setString(1, newExamYear);
			ps1.setString(2, newExamDiv);
			ps1.setString(3, newExamYear);
			ps1.setString(4, newExamDiv);
			
			if (individualId == null) {
				ps1.setString(5, schoolCd);
			} else {
				ps1.setString(5, individualId);
			}

			rs = ps1.executeQuery();

			//結果に対して更新
			while (rs.next()) {
				final String individualId = rs.getString(1);
				final String oldUnivCd = rs.getString(2);
				final String oldFacultyCd = rs.getString(3);
				final String oldDeptCd = rs.getString(4);
				final String entExamModeCd = rs.getString(5);
				final String entExamDiv1 = rs.getString(6);
				final String entExamDiv2 = rs.getString(7);
				final String entExamDiv3 = rs.getString(8);
				final String newUnivCd = rs.getString(9);
				final String newFacultyCd = rs.getString(10);
				final String newDeptCd = rs.getString(11);

				//更新後の大学が同じ場合
				if (newUnivCd.equals(oldUnivCd)
						&& newFacultyCd.equals(oldFacultyCd)
						&& newDeptCd.equals(oldDeptCd)) {
					//入試結果情報を更新（年度・模試区分のみ）
					ps4.setString(1, newExamYear);
					ps4.setString(2, newExamDiv);
					ps4.setString(3, individualId);
					ps4.setString(4, oldUnivCd);
					ps4.setString(5, oldFacultyCd);
					ps4.setString(6, oldDeptCd);
					ps4.setString(7, entExamModeCd);
					ps4.setString(8, entExamDiv1);
					ps4.setString(9, entExamDiv2);
					ps4.setString(10, entExamDiv3);
					ps4.executeUpdate();
				} else {
					//入試結果情報を更新
					ps2.setString(1, newUnivCd);
					ps2.setString(2, newFacultyCd);
					ps2.setString(3, newDeptCd);
					ps2.setString(4, newExamYear);
					ps2.setString(5, newExamDiv);
					ps2.setString(6, individualId);
					ps2.setString(7, oldUnivCd);
					ps2.setString(8, oldFacultyCd);
					ps2.setString(9, oldDeptCd);
					ps2.setString(10, entExamModeCd);
					ps2.setString(11, entExamDiv1);
					ps2.setString(12, entExamDiv2);
					ps2.setString(13, entExamDiv3);
					ps2.setString(14, newUnivCd);
					ps2.setString(15, newFacultyCd);
					ps2.setString(16, newDeptCd);
					ps2.executeUpdate();

					//更新できなかったレコードを削除
					ps3.setString(1, individualId);
					ps3.setString(2, oldUnivCd);
					ps3.setString(3, oldFacultyCd);
					ps3.setString(4, oldDeptCd);
					ps3.setString(5, entExamModeCd);
					ps3.setString(6, entExamDiv1);
					ps3.setString(7, entExamDiv2);
					ps3.setString(8, entExamDiv3);
					ps3.executeUpdate();
				}
			}

		} finally {
			DbUtils.closeQuietly(null, ps1, rs);
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(ps3);
			DbUtils.closeQuietly(ps4);
		}
	}
	
	//存在しない大学の削除処理
	private void deletePlanUniv() throws Exception {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().load("univrefresh_05").toString());
			ps.setString(1, schoolCd);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

}

/*
 * 作成日: 2005/02/09
 */
package jp.co.fj.keinavi.beans.profile.corrector;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;

/**
 * プロファイルのバージョン間の違いを校正するための
 * モジュールインターフェース
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */
public interface ICorrector extends IProfileCategory, IProfileItem {

	/**
	 * 処理実行メソッド
	 * 
	 * @param profile プロファイル
	 * @param login ログイン情報
	 */
	public void execute(final Profile profile, final LoginSession login);
	
	/**
	 * モジュールの対応バージョンを取得する
	 * 
	 * @return バージョン
	 */
	public short getVersion();
}

/*
 * 作成日: 2004/07/16
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import jp.co.fj.keinavi.util.db.RecordProcessor;

/**
 * @author
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ExShortDatePlusUtil extends DateUtil {

	// SimpleDateFormatインスタンス
	private static SimpleDateFormat format; 
	
	/**
	 * @see jp.co.fj.keinavi.util.date.DateUtil#factoryMethod()
	 */
	public SimpleDateFormat factoryMethod() {
		if (format == null) format = new SimpleDateFormat("M/d");
		return format;
	}
	
	/**
	 * @see jp.co.fj.keinavi.util.date.DateUtil#string2Date(java.lang.String)
	 */
	public Date string2Date(String string) throws ParseException {

		int month = Integer.parseInt(RecordProcessor.trimZero(
				RecordProcessor.getMonthDigit(string)));
		int date = Integer.parseInt(RecordProcessor.trimZero(
				RecordProcessor.getDateDigit(string)));
		int year = Integer.parseInt(SpecialDateUtil.getThisJpnYearRev());
		
		/** 年度で計算します */
		if (month > 0 && month < 5){
			year ++;
		}
		
		GregorianCalendar ct = new GregorianCalendar(year, month-1 , date);
		return ct.getTime();
	}
	
	public String getFormattedString(String string) throws ParseException{
		return date2String(string2Date(string));
	}
	
	public static String getUnformattedString(String string){
		String returnStr = "";
		int index = string.indexOf('/');
		String month = string.substring(0, index);
		String date = string.substring(index+1);
		if(month.length() == 1)
			returnStr += "0";
		returnStr += month;
		if(date.length() == 1)
			returnStr += "0";
		returnStr += date;
		return returnStr;
	}
	
}

/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.txt_out.factory;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.beans.txt_out.TextInfoBean;
import jp.co.fj.keinavi.forms.txt_out.TMaxForm;
import jp.co.fj.keinavi.util.CollectionUtil;

import com.fjh.forms.ActionForm;

/**
 * 模試別成績データ（新形式）のActionFormFactory 
 * 
 * <河合塾2010年度マーク高２模試対応>
 * 2011.05.20   Tomohisa YAMADA - Totec
 *              倫政対応・マーク高２対応有
 * 
 * @author kawai
 */
public class T104FormFactory extends AbstractTFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		// アクションフォーム
		TMaxForm form = super.createTMaxForm(request);
		// アイテムマップ
		Map item = getItemMap(request);
		
		CommonFormFactory commForm = new CommonFormFactory(form, item);
		commForm.setOutTarget();
		commForm.setTxtType();
		return commForm.getForm();
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
		TMaxForm form = (TMaxForm)request.getAttribute("form");
		// アイテムマップ
		Map item = getItemMap(request);

		//対象模試が2011年度以前の場合かつ、
		//すでに24科目が選択されていればそれを引き継ぐ
		succeedSelection(form, item);
		
		CommonItemFactory commItem = new CommonItemFactory(form, item);
		commItem.setOutTarget();
		commItem.setTxtType();

	}
	
	/**
	 * 24科目目を引き継ぐ
	 * @param form
	 * @param item
	 */
	public void succeedSelection(TMaxForm form, Map item) {
		Set set = new HashSet();
		if (form.getTargetYear() != null 
				&& form.getTargetYear().compareTo(TextInfoBean.getYearFormat3()) < 0) {
			String[] selection = CollectionUtil.splitComma((String) item.get("1403"));
			for (int i = 0; i < selection.length; i ++) {
				for (int j = 0; j < TextInfoBean.SELECTION24.length; j ++) {
					if (selection[i].equals(TextInfoBean.SELECTION24[j])) {
						set.add(TextInfoBean.SELECTION24[j]);
					}
				}
			}
		}
		
		String[] container = new String[form.getOutTarget().length + set.size()];
		System.arraycopy(form.getOutTarget(), 0, container, 0, form.getOutTarget().length);

		int index = form.getOutTarget().length;
		for (Iterator ite = set.iterator(); ite.hasNext();) {			
			container[index] = (String) ite.next();
			index ++;
		}
		
		form.setOutTarget(container);
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap("050303");
	}

}
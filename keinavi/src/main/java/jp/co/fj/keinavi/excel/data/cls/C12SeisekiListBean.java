package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−成績表データリスト
 * 作成日: 2004/07/14
 * @author	H.Fujimoto
 */
public class C12SeisekiListBean {
	//学年
	private String strGrade = "";
	//クラス
	private String strClass = "";
	//クラス番号
	private String strClassNo = "";
	//氏名
	private String strKanaName = "";
	//性別
	private String strSex = "";
	//科目別成績データリスト
	private ArrayList c12KmkSeisekiList = new ArrayList();
	//志望校評価データリスト
	private ArrayList c12ShiboHyoukaList = new ArrayList();

	/*----------*/
	/* Get      */
	/*----------*/

	public ArrayList getC12KmkSeisekiList() {
		return this.c12KmkSeisekiList;
	}
	public ArrayList getC12ShiboHyoukaList() {
		return this.c12ShiboHyoukaList;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrClassNo() {
		return this.strClassNo;
	}
	public String getStrGrade() {
		return this.strGrade;
	}
	public String getStrKanaName() {
		return this.strKanaName;
	}
	public String getStrSex() {
		return this.strSex;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setC12KmkSeisekiList(ArrayList c12KmkSeisekiList) {
		this.c12KmkSeisekiList = c12KmkSeisekiList;
	}
	public void setC12ShiboHyoukaList(ArrayList c12ShiboHyoukaList) {
		this.c12ShiboHyoukaList = c12ShiboHyoukaList;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrClassNo(String strClassNo) {
		this.strClassNo = strClassNo;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}
	public void setStrKanaName(String strKanaName) {
		this.strKanaName = strKanaName;
	}
	public void setStrSex(String strSex) {
		this.strSex = strSex;
	}

}
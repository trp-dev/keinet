package jp.co.fj.keinavi.util.taglib.list;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 * ListにSPACER要素を追加するTaglib
 * 
 * @author kawai
 */
public class ListSpacerTag extends BodyTagSupport {
	
	// 変数名
	private String var = null;
	
	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	public int doStartTag() throws JspException {
		return (EVAL_BODY_BUFFERED);
	}
	
	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.IterationTag#doAfterBody()
	 */
	public int doAfterBody() throws JspException {
		return SKIP_BODY;
	}
	
	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {
		List container = (List) pageContext.getAttribute(getVar());
		if (bodyContent != null) {
			try {
				StringBuffer buff = new StringBuffer();
				BufferedReader reader = new BufferedReader(bodyContent.getReader());
				String line = null;
				while ((line = reader.readLine()) != null) {
					buff.append(line);
				}

				List c = new ArrayList();
				Iterator ite = container.iterator();
				while (ite.hasNext()) {
					String value = (String) ite.next();
					c.add(value);
					if (ite.hasNext()) {
						c.add(buff.toString());
					}
				}
				pageContext.setAttribute(getVar(), c);
			} catch (IOException e) {
				throw new JspException(e);
			}
		}
		return EVAL_PAGE;
	}

	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.BodyTag#doInitBody()
	 */
	public void doInitBody() throws JspException {
		super.doInitBody();
	}

	public String getVar() {
		return this.var;
	}
	public void setVar(String var) {
		this.var = var;
	}
}

/*
 * 作成日: 2004/10/11
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.txt_out;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.totec.config.ConfigResolver;

import org.apache.commons.configuration.Configuration;

import com.fjh.beans.DefaultBean;
/**
 *
 * 2005.02.04	Yoshimoto KAWAI - Totec
 *				入試結果調査票データのデータを追加
 *
 * 2005.02.09	Yoshimoto KAWAI - Totec
 *				・新課程対応で模試別成績データ（新形式）のデータを追加
 *				・データをキャッシュする仕様に変更
 *
 * 2005.04.20	Yoshimoto KAWAI - Totec
 * 				偏差値分布の新テスト対応
 *
 * <2010年度改修>
 * 2009.12.01   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 *
 * 2009.12.14	Tomohisa YAMADA - Totec
 * 				センター・リサーチ５段階評価対応
 *
 * 2010.01.18	Shoji HASE - Totec
 * 				「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 *              「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換えに伴い
 *				学科コードの桁数を変更
 *
 * 2010.03.13	Shoji HASE - Totec
 * 				テキスト出力−個人データ−模試別成績データにおいて
 * 				志望校コードの桁数が8桁を表示されている事を10桁に修正
 *
 * <2010年度マーク高２模試対応>
 * 2011.01.12   Tomohisa YAMADA - Totec
 *              倫政対応・マーク高２対応有
 *
 * <新過程対応>
 * 2013.04.23   Yuuichirou Morita - TOTEC
 *
 * 2015.03.30   Toshiyuki Shimasaki - QQ
 *                              個人科目別成績リスト（レイアウト�B）のコード「151」の備考欄
 *                              を修正（理科基礎対応）
 *
 * <共通テスト対応>
 * 2019.07.31   Rintaro Ueta - Hics
 * 2019.08.01   Rintaro Ueta - Hics
 *                              備考欄の文言追加、変更
 *                              個人科目別成績リスト項目追加
 *
 * @author 奥村ゆかり
 *
 */
public class TextInfoBean extends DefaultBean {

    private LinkedHashMap outputTableMap = null;
    private static final String NOTE_EXAM;
    private static final String NOTE_PREF;
    private static final String NOTE_SUBJECT;

    //24科目目のインデックス値
    public static final String[] SELECTION24 = {"228", "229", "230", "231", "232", "233"};

    //25科目目,26科目目のインデックス値
    public static final String[] SELECTION_add = {"234", "235", "236", "237", "238","239","240","241","242","243","244","245","246","247"};

    static {
        final Configuration note = ConfigResolver.getInstance(
                ).getConfiguration("note.properties");
        NOTE_EXAM = note.getString("exam");
        NOTE_PREF = note.getString("pref");
        NOTE_SUBJECT = note.getString("subject");
    }

    // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD START
    // 年度による定義ファイルの変更
    private static final String HEADER5 = "2020";
    // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD END

    /**
     * フォーマット形式�Bを採用する（以降）年度
     */
    public static String getYearFormat3() {
        return "2011";
    }

    /**
     * 対象模試によってSheetBeanを切り替える
     * @param exam 対象模試
     * @return SheetBean
     */
    public static AbstractSheetBean getOutputExamSeiseki(ExamData exam) {

        //担当クラスなし
        if (exam == null) {
            return new OutputExamSeiseki3();
        }

        //対象模試が2011年度以降場合は形式�B
        if (exam.getExamYear().compareTo(TextInfoBean.getYearFormat3()) >= 0) {
            return new OutputExamSeiseki3();
        }

        return new OutputExamSeiseki2();
    }

    /**
     * 対象模試によってTextInfoBeanの内容を切り替える
     * @param exam 対象模試
     * @return TextInfoBean
     */
    public static TextInfoBean getInstance(ExamData exam) throws SQLException, Exception {

        if (exam == null) {
            return new TextInfoBean(3, null);
        }

        //対象模試が2011年度以降場合は形式�B
        if (exam.getExamYear().compareTo(TextInfoBean.getYearFormat3()) >= 0) {
            return new TextInfoBean(3, exam);
        }

        return new TextInfoBean(2, exam);
    }

    /**
     * コンストラクタ
     * @param examYear 対象模試年度
     * @param examCd 対象模試コード
     * @throws Exception
     * @throws SQLException
     */
    public TextInfoBean(int typeVersion, ExamData targetExam) throws SQLException, Exception {

        //コネクションの設定
        super.setConnection(null, conn);

        this.outputTableMap = new LinkedHashMap();

        outputTableMap.put("t002", getSougouSeisekiValue());
        outputTableMap.put("t003a", getGakuryokuBunpuValue());
        outputTableMap.put("t003b", getGakuryokuBunpuValue2());
        outputTableMap.put("t004", getSetumonSeiseki());
        outputTableMap.put("t005", getHyoukaNinzu());
        outputTableMap.put("t103", getZbf560());
// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD START
        outputTableMap.put("t106", getDescExamValue());
// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD END
        outputTableMap.put("t202", getExamResultsResearchValue());

        switch (typeVersion) {
            //方式�A（新方式）
            case 2 : outputTableMap.put("t104", getExamSeiseki2());
                break;
            //方式�B
            // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
            //case 3 :outputTableMap.put("t104", getExamSeiseki3());
            case 3 :outputTableMap.put("t104", getExamSeiseki3(targetExam));
            // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                break;
            default :
                throw new InternalError("不正な方式が指定されました。" + typeVersion);
        }
    }

    public void execute() throws SQLException, Exception {
        throw new InternalError("このメソッドは使用できません。");
    }

    /**
     * 	テキスト情報リストを取得
     * @param id 画面ID
     * @return テキスト情報
     */
    public List getInfoList(int typeVersion, final String id, final ExamData exam) {
        if ("t002".equals(id)) return getSougouSeisekiValue();
        else if ("t003".equals(id) && KNUtil.isNewExam(exam)) return getGakuryokuBunpuValue2();
        else if ("t003".equals(id)) return getGakuryokuBunpuValue();
        else if ("t004".equals(id)) return getSetumonSeiseki();
        else if ("t005".equals(id)) return getHyoukaNinzu();
        else if ("t103".equals(id)) return getZbf560();
        else if ("t104".equals(id)) {
            switch (typeVersion) {
            //方式�A（新方式）
            case 2 : return getExamSeiseki2();
            //方式�B
            // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
            //case 3 :return getExamSeiseki3();
            case 3 :return getExamSeiseki3(exam);
            // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
            default :
                throw new InternalError("不正な方式が指定されました。" + typeVersion);
            }
        }
// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD START
        else if ("t106".equals(id)) return getDescExamValue();
// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD END
        else if ("t202".equals(id)) return getExamResultsResearchValue();
        else throw new RuntimeException("指定されたテキスト情報は存在しません。");
    }

    /**
     * 	成績概況項目リストを取得
     *
     * @return list 成績概況項目リスト(項目名,表示項目名,byte数,備考,文字列/数値)
     */
    private static List getSougouSeisekiValue() {
        List list = new ArrayList();
        list.add( new String[]{"1","年度","年度","4","西暦年4桁","1"});
        list.add( new String[]{"2","模試コード","模試コード","2",NOTE_EXAM,"1"});
        list.add( new String[]{"3","模試名","模試名","40","模試名。(模試短縮名ではない）","1"});
        list.add( new String[]{"4","集計単位","集計単位","1","1=全国、2=都道府県単位、3=高校単位、4=クラス単位","1"});
        list.add( new String[]{"5","都道府県コード","都道府県コ","2",NOTE_PREF,"1"});
        list.add( new String[]{"6","都道府県名","都道府県名","6","","1"});
        list.add( new String[]{"7","学校コード","学校コード","5","集計単位が、1のとき\"99999\"、2のとき\"都道府県コード+999\"","1"});
        list.add( new String[]{"8","学校名","学校名","28","集計単位が、1と2のとき（）付き都道府県名。","1"});
        list.add( new String[]{"9","学年","学年","2","集計単位がクラス単位の時に値が入り、それ以外は\"99\"","1"});
        list.add( new String[]{"10","クラス","クラス","2","集計単位がクラス単位の時に値が入り、それ以外は\"99\"","1"});
        list.add( new String[]{"11","型・科目コード","型・科目コ","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"12","型・科目名","型・科目名","20","型・科目名。（短縮名ではない）","1"});
        list.add( new String[]{"13","型・科目配点","型・科目配","4","配点が存在しない場合は、空値。","0"});
        list.add( new String[]{"14","人数","人数","6","","0"});
        list.add( new String[]{"15","平均点","平均点","5","平均点が存在しない場合は、空値。","0"});
        list.add( new String[]{"16","平均得点率","平均得点率","5","","0"});
        list.add( new String[]{"17","平均偏差値","平均偏差値","5","","0"});
        return list;
    }

    /**
     * 	偏差値分布項目リストを取得
     *
     * @return list 偏差値分布項目リスト(項目名,表示項目名,byte数,備考,文字列/数値)
     */
    private static List getGakuryokuBunpuValue() {
        List list = new ArrayList();
        list.add( new String[]{"1","年度","年度","4","西暦年4桁","1"});
        list.add( new String[]{"2","模試コード","模試コード","2",NOTE_EXAM,"1"});
        list.add( new String[]{"3","模試名","模試名","40","模試名。(模試短縮名ではない）","1"});
        list.add( new String[]{"4","集計単位","集計単位","1","1=全国、2=都道府県単位、3=高校単位、4=クラス単位","1"});
        list.add( new String[]{"5","都道府県コード","都道府県コ","2",NOTE_PREF,"0"});
        list.add( new String[]{"6","都道府県名","都道府県名","6","","1"});
        list.add( new String[]{"7","学校コード","学校コード","5","集計単位が、1のとき\"99999\"、2のとき\"都道府県コード+999\"","1"});
        list.add( new String[]{"8","学校名","学校名","28","集計単位が、1と2のとき（）付き都道府県名。","1"});
        list.add( new String[]{"9","学年","学年","2","集計単位がクラス単位の時に値が入り、それ以外は\"99\"","1"});
        list.add( new String[]{"10","クラス","クラス","2","集計単位がクラス単位の時に値が入り、それ以外は\"99\"","1"});
        list.add( new String[]{"11","型・科目コード","型・科目コ","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"12","型・科目名","型・科目名","20","型・科目名。(短縮名ではない)","1"});
        list.add( new String[]{"13","型・科目配点","型・科目配","4","配点が存在しない場合は、空値。","0"});
        list.add( new String[]{"14","分布１","〜32.4","6","偏差値が[    〜32.4]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"15","分布２","〜34.9","6","偏差値が[32.5〜34.9]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"16","分布３","〜37.4","6","偏差値が[35.0〜37.4]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"17","分布４","〜39.9","6","偏差値が[37.5〜39.9]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"18","分布５","〜42.4","6","偏差値が[40.0〜42.4]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"19","分布６","〜44.9","6","偏差値が[42.5〜44.9]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"20","分布７","〜47.4","6","偏差値が[45.0〜47.4]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"21","分布８","〜49.9","6","偏差値が[47.5〜49.9]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"22","分布９","〜52.4","6","偏差値が[50.0〜52.4]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"23","分布１０","〜54.9","6","偏差値が[52.5〜54.9]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"24","分布１１","〜57.4","6","偏差値が[55.0〜57.4]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"25","分布１２","〜59.9","6","偏差値が[57.5〜59.9]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"26","分布１３","〜62.4","6","偏差値が[60.0〜62.4]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"27","分布１４","〜64.9","6","偏差値が[62.5〜64.9]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"28","分布１５","〜67.4","6","偏差値が[65.0〜67.4]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"29","分布１６","〜69.9","6","偏差値が[67.5〜69.9]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"30","分布１７","〜72.4","6","偏差値が[70.0〜72.4]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"31","分布１８","〜74.9","6","偏差値が[72.5〜74.9]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"32","分布１９","75.0〜","6","偏差値が[75.0〜    ]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"33","構成比１","〜32.4","5","偏差値が[    〜32.4]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"34","構成比２","〜34.9","5","偏差値が[32.5〜34.9]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"35","構成比３","〜37.4","5","偏差値が[35.0〜37.4]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"36","構成比４","〜39.9","5","偏差値が[37.5〜39.9]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"37","構成比５","〜42.4","5","偏差値が[40.0〜42.4]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"38","構成比６","〜44.9","5","偏差値が[42.5〜44.9]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"39","構成比７","〜47.4","5","偏差値が[45.0〜47.4]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"40","構成比８","〜49.9","5","偏差値が[47.5〜49.9]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"41","構成比９","〜52.4","5","偏差値が[50.0〜52.4]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"42","構成比１０","〜54.9","5","偏差値が[52.5〜54.9]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"43","構成比１１","〜57.4","5","偏差値が[55.0〜57.4]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"44","構成比１２","〜59.9","5","偏差値が[57.5〜59.9]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"45","構成比１３","〜62.4","5","偏差値が[60.0〜62.4]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"46","構成比１４","〜64.9","5","偏差値が[62.5〜64.9]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"47","構成比１５","〜67.4","5","偏差値が[65.0〜67.4]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"48","構成比１６","〜69.9","5","偏差値が[67.5〜69.9]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"49","構成比１７","〜72.4","5","偏差値が[70.0〜72.4]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"50","構成比１８","〜74.9","5","偏差値が[72.5〜74.9]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"51","構成比１９","75.0〜","5","偏差値が[75.0〜    ]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"52","合計人数","合計人数","6","集計単位ごとの全体の人数","0"});
        list.add( new String[]{"53","平均偏差値","平均偏差値","5","集計単位ごとの全体の平均偏差値","0"});
        return list;
    }

    /**
     * 偏差値分布項目リストを取得
     * （新テスト用）
     *
     * @return list 偏差値分布項目リスト(項目名,表示項目名,byte数,備考,文字列/数値)
     */
    private static List getGakuryokuBunpuValue2() {
        List list = new ArrayList();
        list.add( new String[]{"1","年度","年度","4","西暦年4桁","1"});
        list.add( new String[]{"2","模試コード","模試コード","2",NOTE_EXAM,"1"});
        list.add( new String[]{"3","模試名","模試名","40","模試名。(模試短縮名ではない）","1"});
        list.add( new String[]{"4","集計単位","集計単位","1","1=全国、2=都道府県単位、3=高校単位、4=クラス単位","1"});
        list.add( new String[]{"5","都道府県コード","都道府県コ","2",NOTE_PREF,"0"});
        list.add( new String[]{"6","都道府県名","都道府県名","6","","1"});
        list.add( new String[]{"7","学校コード","学校コード","5","集計単位が、1のとき\"99999\"、2のとき\"都道府県コード+999\"","1"});
        list.add( new String[]{"8","学校名","学校名","28","集計単位が、1と2のとき（）付き都道府県名。","1"});
        list.add( new String[]{"9","学年","学年","2","集計単位がクラス単位の時に値が入り、それ以外は\"99\"","1"});
        list.add( new String[]{"10","クラス","クラス","2","集計単位がクラス単位の時に値が入り、それ以外は\"99\"","1"});
        list.add( new String[]{"11","型・科目コード","型・科目コ","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"12","型・科目名","型・科目名","20","型・科目名。(短縮名ではない)","1"});
        list.add( new String[]{"13","型・科目配点","型・科目配","4","配点が存在しない場合は、空値。","0"});
        list.add( new String[]{"14","分布１","〜19","6","得点が[  〜19]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"15","分布２","〜29","6","得点が[20〜29]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"16","分布３","〜39","6","得点が[30〜39]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"17","分布４","〜49","6","得点が[40〜49]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"18","分布５","〜59","6","得点が[50〜59]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"19","分布６","〜69","6","得点が[60〜69]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"20","分布７","〜79","6","得点が[70〜79]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"21","分布８","〜89","6","得点が[80〜89]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"22","分布９","〜99","6","得点が[90〜99]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"23","分布１０","〜109","6","得点が[100〜109]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"24","分布１１","〜119","6","得点が[110〜119]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"25","分布１２","〜129","6","得点が[120〜129]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"26","分布１３","〜139","6","得点が[130〜139]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"27","分布１４","〜149","6","得点が[140〜149]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"28","分布１５","〜159","6","得点が[150〜159]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"29","分布１６","〜169","6","得点が[160〜169]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"30","分布１７","〜179","6","得点が[170〜179]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"31","分布１８","〜189","6","得点が[180〜189]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"32","分布１９","190〜","6","得点が[190〜   ]に該当する人数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"33","構成比１","〜19","5","得点が[  〜19]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"34","構成比２","〜29","5","得点が[20〜29]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"35","構成比３","〜39","5","得点が[30〜39]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"36","構成比４","〜49","5","得点が[40〜49]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"37","構成比５","〜59","5","得点が[50〜59]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"38","構成比６","〜69","5","得点が[60〜69]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"39","構成比７","〜79","5","得点が[70〜79]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"40","構成比８","〜89","5","得点が[80〜89]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"41","構成比９","〜99","5","得点が[90〜99]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"42","構成比１０","〜109","5","得点が[100〜109]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"43","構成比１１","〜119","5","得点が[110〜119]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"44","構成比１２","〜129","5","得点が[120〜129]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"45","構成比１３","〜139","5","得点が[130〜139]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"46","構成比１４","〜149","5","得点が[140〜149]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"47","構成比１５","〜159","5","得点が[150〜159]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"48","構成比１６","〜169","5","得点が[160〜169]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"49","構成比１７","〜179","5","得点が[170〜179]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"50","構成比１８","〜189","5","得点が[180〜189]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"51","構成比１９","190〜","5","得点が[190〜    ]に該当する構成比。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"52","合計人数","合計人数","6","集計単位ごとの全体の人数","0"});
        list.add( new String[]{"53","平均偏差値","平均偏差値","5","集計単位ごとの全体の平均偏差値","0"});
        return list;
    }

    /**
     * 	設問成績項目リストを取得
     *
     * @return list 設問成績項目リスト(項目名,表示項目名,byte数,備考,文字列/数値)
     */
    private static List getSetumonSeiseki() {
        List list = new ArrayList();
        list.add( new String[]{"1","年度","年度","4","西暦年4桁","1"});
        list.add( new String[]{"2","模試コード","模試コード","2",NOTE_EXAM,"1"});
        list.add( new String[]{"3","模試名","模試名","40","模試名。(模試短縮名ではない）","1"});
        list.add( new String[]{"4","集計単位","集計単位","1","1=全国、2=都道府県単位、3=高校単位、4=クラス単位","1"});
        list.add( new String[]{"5","都道府県コード","都道府県コ","2",NOTE_PREF,"1"});
        list.add( new String[]{"6","都道府県名","都道府県名","6","","1"});
        list.add( new String[]{"7","学校コード","学校コード","5","集計単位が、1のとき\"99999\"、2のとき\"都道府県コード+999\"","1"});
        list.add( new String[]{"8","学校名","学校名","28","集計単位が、1と2のとき（）付き都道府県名。","1"});
        list.add( new String[]{"9","学年","学年","2","集計単位がクラス単位の時に値が入り、それ以外は\"99\"","1"});
        list.add( new String[]{"10","クラス","クラス","2","集計単位がクラス単位の時に値が入り、それ以外は\"99\"","1"});
        list.add( new String[]{"11","型・科目コード","型・科目コ","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"12","型・科目名","型・科目名","20","型・科目名。（短縮名ではない）","1"});
        list.add( new String[]{"13","設問番号","設問番号","2","","1"});
        list.add( new String[]{"14","設問名","設問名","30","","1"});
        list.add( new String[]{"15","設問配点","設問配点","4","","0"});
        list.add( new String[]{"16","人数","人数","6","","0"});
        list.add( new String[]{"17","平均点","平均点","5","","0"});
        list.add( new String[]{"18","平均得点率","平均得点率","5","","0"});
        return list;
    }

    /**
     * 	志望大学評価別人数項目リストを取得
     *
     * @return list 志望大学評価別人数項目リスト(ID,項目名,表示項目名,byte数,備考,文字列型/数値型)
     */
    private static List getHyoukaNinzu() {
        List list = new ArrayList();
        list.add( new String[]{"1","年度","年度","4","西暦年4桁","1"});
        list.add( new String[]{"2","模試コード","模試コード","2",NOTE_EXAM,"1"});
        list.add( new String[]{"3","模試名","模試名","40","模試名。(模試短縮名ではない）","1"});
        list.add( new String[]{"4","集計単位","集計単位","1","1=全国、2=都道府県単位、3=高校単位、4=クラス単位","1"});
        list.add( new String[]{"5","都道府県コード","都道府県コード","2",NOTE_PREF,"1"});
        list.add( new String[]{"6","都道府県名","都道府県名","6","","1"});
        list.add( new String[]{"7","学校コード","学校コード","5","集計単位が、1のとき\"99999\"、2のとき\"都道府県コード+999\"","1"});
        list.add( new String[]{"8","学校名","学校名","28","集計単位が、1と2のとき（）付き都道府県名。","1"});
        list.add( new String[]{"9","学年","学年","2","集計単位がクラス単位の時に値が入り、それ以外は\"99\"","1"});
        list.add( new String[]{"10","クラス","クラス","2","集計単位がクラス単位の時に値が入り、それ以外は\"99\"","1"});
        //2019/08/02 QQ)nagai 共通テスト対応 UPD START
        //list.add( new String[]{"11","評価区分","評価区分","1","1=センター、2=2次、3＝総合","1"});
        list.add( new String[]{"11","評価区分","評価区分","1","1=共通テスト、2=2次、3＝総合","1"});
        //2019/08/02 QQ)nagai 共通テスト対応 UPD END
        list.add( new String[]{"12","大学集計区分","大学集計区","1","集計範囲を表す。0=大学、1=大学+日程、2＝学部まで、3=学部まで+日程、4=学科まで","1"});
        list.add( new String[]{"13","大学コード","大学コード","4","","1"});
        list.add( new String[]{"14","学部コード","学部コード","2","","1"});
        list.add( new String[]{"15","学科コード","学科コード","4","","1"});
        list.add( new String[]{"16","大学コード(5桁)","大学コード(5桁)","5","5桁コード","1"});
        list.add( new String[]{"17","大学名","大学名","14","","1"});
        list.add( new String[]{"18","学部名","学部名","10","","1"});
        list.add( new String[]{"19","学科名","学科名","12","","1"});
        //2019/08/02 QQ)nagai 共通テスト対応 UPD START
        //list.add( new String[]{"20","日程","日程","1","C=中期日程、D=前期日程、E=後期日程、1=センター利用私大","1"});
        list.add( new String[]{"20","日程","日程","1","C=中期日程、D=前期日程、E=後期日程、1=共通テスト利用私大、N=別日程","1"});
        //2019/08/02 QQ)nagai 共通テスト対応 UPD END
        list.add( new String[]{"21","現役高卒区分","現役高卒区","1","0=現役+高卒志望者、1=現の志望者、2=卒の志望者","1"});
        list.add( new String[]{"22","総志望者数","総志望数","6","","0"});
        list.add( new String[]{"23","第一志望者数","第一志望数","6","該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"24","評価別人数A","評価別数A","6","A評価の該当者数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"25","評価別人数B","評価別数B","6","B評価の該当者数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"26","評価別人数C","評価別数C","6","C評価の該当者数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"27","評価別人数D","評価別数D","6","D評価の該当者数。該当者なしの場合、0を出力。","0"});
        list.add( new String[]{"28","評価別人数E","評価別数E","6","E評価の該当者数。該当者なしの場合、0を出力。","0"});
        return list;
    }

    /**
     * 	個人設問成績項目リストを取得
     *
     * @return list 設問成績項目リスト(項目名,表示項目名,byte数,備考)
     */
    private static List getZbf560() {
        List list = new ArrayList();
        list.add( new String[]{"1","模試コード","模試コード","2",NOTE_EXAM,"1"});
        list.add( new String[]{"2","学校コード","学校コード","5","該当模試を一括受験した学校コード","1"});
        list.add( new String[]{"3","在卒校コード","出身コード","5","出身の学校コード","1"});
        list.add( new String[]{"4","学年","学年","2","","1"});
        list.add( new String[]{"5","クラス","クラス","2","","1"});
        list.add( new String[]{"6","クラス番号","クラス番号","5","","1"});
        list.add( new String[]{"7","カナ氏名","カナ氏名","14","半角ｶﾅ","1"});
        list.add( new String[]{"8","科目コード","型・科目コ","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"9","科目配点","科目配点","3","","0"});
        list.add( new String[]{"10","科目得点","科目得点","3","","0"});
        list.add( new String[]{"11","全国偏差値","全偏差値","5","","0"});
        list.add( new String[]{"12","校内偏差値","校内偏差値","5","","0"});
        list.add( new String[]{"13","配点1","設問1配点","3","","0"});
        list.add( new String[]{"14","得点1","設問1得点","3","","0"});
        list.add( new String[]{"15","全国偏差値1","設問1全偏","5","","0"});
        list.add( new String[]{"16","配点2","設問2配点","3","","0"});
        list.add( new String[]{"17","得点2","設問2得点","3","","0"});
        list.add( new String[]{"18","全国偏差値2","設問2全偏","5","","0"});
        list.add( new String[]{"19","配点3","設問3配点","3","","0"});
        list.add( new String[]{"20","得点3","設問3得点","3","","0"});
        list.add( new String[]{"21","全国偏差値3","設問3全偏","5","","0"});
        list.add( new String[]{"22","配点4","設問4配点","3","","0"});
        list.add( new String[]{"23","得点4","設問4得点","3","","0"});
        list.add( new String[]{"24","全国偏差値4","設問4全偏","5","","0"});
        list.add( new String[]{"25","配点5","設問5配点","3","","0"});
        list.add( new String[]{"26","得点5","設問5得点","3","","0"});
        list.add( new String[]{"27","全国偏差値5","設問5全偏","5","","0"});
        list.add( new String[]{"28","配点6","設問6配点","3","","0"});
        list.add( new String[]{"29","得点6","設問6得点","3","","0"});
        list.add( new String[]{"30","全国偏差値6","設問6全偏","5","","0"});
        list.add( new String[]{"31","配点7","設問7配点","3","","0"});
        list.add( new String[]{"32","得点7","設問7得点","3","","0"});
        list.add( new String[]{"33","全国偏差値7","設問7全偏","5","","0"});
        list.add( new String[]{"34","配点8","設問8配点","3","","0"});
        list.add( new String[]{"35","得点8","設問8得点","3","","0"});
        list.add( new String[]{"36","全国偏差値8","設問8全偏","5","","0"});
        list.add( new String[]{"37","配点9","設問9配点","3","","0"});
        list.add( new String[]{"38","得点9","設問9得点","3","","0"});
        list.add( new String[]{"39","全国偏差値9","設問9全偏","5","","0"});
        list.add( new String[]{"40","配点10","設問10配点","3","","0"});
        list.add( new String[]{"41","得点10","設問10得点","3","","0"});
        list.add( new String[]{"42","全国偏差値10","設問10全偏","5","","0"});
        list.add( new String[]{"43","配点11","設問11配点","3","","0"});
        list.add( new String[]{"44","得点11","設問11得点","3","","0"});
        list.add( new String[]{"45","全国偏差値11","設問11全偏","5","","0"});
        list.add( new String[]{"46","配点12","設問12配点","3","","0"});
        list.add( new String[]{"47","得点12","設問12得点","3","","0"});
        list.add( new String[]{"48","全国偏差値12","設問12全偏","5","","0"});
        list.add( new String[]{"49","配点13","設問13配点","3","","0"});
        list.add( new String[]{"50","得点13","設問13得点","3","","0"});
        list.add( new String[]{"51","全国偏差値13","設問13全偏","5","","0"});
        list.add( new String[]{"52","配点14","設問14配点","3","","0"});
        list.add( new String[]{"53","得点14","設問14得点","3","","0"});
        list.add( new String[]{"54","全国偏差値14","設問14全偏","5","","0"});
        list.add( new String[]{"55","配点15","設問15配点","3","","0"});
        list.add( new String[]{"56","得点15","設問15得点","3","","0"});
        list.add( new String[]{"57","全国偏差値15","設問15全偏","5","","0"});
        list.add( new String[]{"58","配点16","設問16配点","3","","0"});
        list.add( new String[]{"59","得点16","設問16得点","3","","0"});
        list.add( new String[]{"60","全国偏差値16","設問16全偏","5","","0"});
        list.add( new String[]{"61","配点17","設問17配点","3","","0"});
        list.add( new String[]{"62","得点17","設問17得点","3","","0"});
        list.add( new String[]{"63","全国偏差値17","設問17全偏","5","","0"});
        list.add( new String[]{"64","配点18","設問18配点","3","","0"});
        list.add( new String[]{"65","得点18","設問18得点","3","","0"});
        list.add( new String[]{"66","全国偏差値18","設問18全偏","5","","0"});
        list.add( new String[]{"67","配点19","設問19配点","3","","0"});
        list.add( new String[]{"68","得点19","設問19得点","3","","0"});
        list.add( new String[]{"69","全国偏差値19","設問19全偏","5","","0"});
        list.add( new String[]{"70","配点20","設問20配点","3","","0"});
        list.add( new String[]{"71","得点20","設問20得点","3","","0"});
        list.add( new String[]{"72","全国偏差値20","設問20全偏","5","","0"});
        return list;
    }

    /**
     * 入試結果調査項目リストを取得
     *
     * @return list 入試結果調査項目リスト(項目名,表示項目名,byte数,備考,文字列/数値)
     */
    private static List getExamResultsResearchValue() {
        List list = new ArrayList();
        list.add(new String[]{"1","クラス","クラス","2","","1"});
        list.add(new String[]{"2","クラス番号","クラス番号","5","","1"});
        list.add(new String[]{"3","性別","性別","1","","1"});
        list.add(new String[]{"4","氏名","氏名","14","","1"});
        list.add(new String[]{"5","メモ","メモ","50","","1"});
        list.add(new String[]{"6","出身中学校","出身中学校","30","","1"});
        list.add(new String[]{"7","大学学部学科コード","大学学部学科コード","8","","1"});
        list.add(new String[]{"8","大学学部学科名","大学学部学科名","36","","1"});
        list.add(new String[]{"9","入試日","入試日","8","","1"});
        list.add(new String[]{"10","入試形態","入試形態","1","","1"});
        list.add(new String[]{"11","結果区分","結果区分","1","","1"});
        list.add(new String[]{"12","入学区分","入学区分","1","","1"});
        return list;
    }

    /**
     * 個人科目別成績リスト（新レイアウト）を取得
     *
     * @return list 個人科目別成績リスト(項目名,表示項目名,byte数,備考,文字列/数値)
     */
    private static List getExamSeiseki2() {
        List list = new ArrayList();
        list.add( new String[]{"1","通番","通番","10","","1"});
        list.add( new String[]{"2","年度","年度","4","西暦年４桁。","1"});
        list.add( new String[]{"3","模試コード","模試コード","2",NOTE_EXAM,"1"});
        list.add( new String[]{"4","学校コード","学校コード","5","","1"});
        list.add( new String[]{"5","学年","学年","2","","1"});
        list.add( new String[]{"6","クラス","クラス","2","","1"});
        list.add( new String[]{"7","クラス番号","クラス番号","5","","1"});
        list.add( new String[]{"8","カナ氏名","カナ氏名","14","半角ｶﾅ","1"});

        list.add( new String[]{"9","科目コード1","科目コード01","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"10","科目名1","科目名01","20","","1"});
        list.add( new String[]{"11","配点／換算得点1","01配点","3","","0"});
        list.add( new String[]{"12","得点1","01得点","3","受験学力測定テストでは「スコア」となります。","0"});
        list.add( new String[]{"13","全国偏差値1","01偏差値","5","受験学力測定テストでは「到達エリア（アラビア数字で表現）」となります。","0"});
        list.add( new String[]{"14","学力レベル1","01レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"15","科目コード2","科目コード02","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"16","科目名2","科目名02","20","","1"});
        list.add( new String[]{"17","配点／換算得点2","02配点","3","","0"});
        list.add( new String[]{"18","得点2","02得点","3","","0"});
        list.add( new String[]{"19","全国偏差値2","02偏差値","5","","0"});
        list.add( new String[]{"20","学力レベル2","02レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"21","科目コード3","科目コード03","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"22","科目名3","科目名03","20","","1"});
        list.add( new String[]{"23","配点／換算得点3","03配点","3","","0"});
        list.add( new String[]{"24","得点3","03得点","3","受験学力測定テストでは「スコア」となります。","0"});
        list.add( new String[]{"25","全国偏差値3","03偏差値","5","受験学力測定テストでは「到達エリア（アラビア数字で表現）」となります。","0"});
        list.add( new String[]{"26","学力レベル3","03レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"27","科目コード4","科目コード04","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"28","科目名4","科目名04","20","","1"});
        list.add( new String[]{"29","配点／換算得点4","04配点","3","","0"});
        list.add( new String[]{"30","得点4","04得点","3","受験学力測定テストでは「スコア」となります。","0"});
        list.add( new String[]{"31","全国偏差値4","04偏差値","5","受験学力測定テストでは「到達エリア（アラビア数字で表現）」となります。","0"});
        list.add( new String[]{"32","学力レベル4","04レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"33","科目コード5","科目コード05","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"34","科目名5","科目名05","20","","1"});
        list.add( new String[]{"35","配点／換算得点5","05配点","3","","0"});
        list.add( new String[]{"36","得点5","05得点","3","","0"});
        list.add( new String[]{"37","全国偏差値5","05偏差値","5","","0"});
        list.add( new String[]{"38","学力レベル5","05レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"39","科目コード6","科目コード06","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"40","科目名6","科目名06","20","","1"});
        list.add( new String[]{"41","配点／換算得点6","06配点","3","","0"});
        list.add( new String[]{"42","得点6","06得点","3","受験学力測定テストでは「スコア」となります。","0"});
        list.add( new String[]{"43","全国偏差値6","06偏差値","5","受験学力測定テストでは「到達エリア（アラビア数字で表現）」となります。","0"});
        list.add( new String[]{"44","学力レベル6","06レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"45","科目コード7","科目コード07","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"46","科目名7","科目名07","20","","1"});
        list.add( new String[]{"47","配点／換算得点7","07配点","3","","0"});
        list.add( new String[]{"48","得点7","07得点","3","受験学力測定テストでは「スコア」となります。","0"});
        list.add( new String[]{"49","全国偏差値7","07偏差値","5","受験学力測定テストでは「到達エリア（アラビア数字で表現）」となります。","0"});
        list.add( new String[]{"50","学力レベル7","07レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"51","科目コード8","科目コード08","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"52","科目名8","科目名08","20","","1"});
        list.add( new String[]{"53","配点／換算得点8","08配点","3","","0"});
        list.add( new String[]{"54","得点8","08得点","3","受験学力測定テストでは「スコア」となります。","0"});
        list.add( new String[]{"55","全国偏差値8","08偏差値","5","受験学力測定テストでは「到達エリア（アラビア数字で表現）」となります。","0"});
        list.add( new String[]{"56","学力レベル8","08レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"57","科目コード9","科目コード09","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"58","科目名9","科目名09","20","","1"});
        list.add( new String[]{"59","配点／換算得点9","09配点","3","","0"});
        list.add( new String[]{"60","得点9","09得点","3","","0"});
        list.add( new String[]{"61","全国偏差値9","09偏差値","5","","0"});
        list.add( new String[]{"62","学力レベル9","09レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"63","科目コード10","科目コード10","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"64","科目名10","科目名10","20","","1"});
        list.add( new String[]{"65","配点／換算得点10","10配点","3","","0"});
        list.add( new String[]{"66","得点10","10得点","3","","0"});
        list.add( new String[]{"67","全国偏差値10","10偏差値","5","","0"});
        list.add( new String[]{"68","学力レベル10","10レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"69","科目コード11","科目コード11","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"70","科目名11","科目名11","20","","0"});
        list.add( new String[]{"71","配点／換算得点11","11配点","3","","0"});
        list.add( new String[]{"72","得点11","11得点","3","","0"});
        list.add( new String[]{"73","全国偏差値11","11偏差値","5","","0"});
        list.add( new String[]{"74","学力レベル11","11レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"75","科目コード12","科目コード12","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"76","科目名12","科目名12","20","","1"});
        list.add( new String[]{"77","配点／換算得点12","12配点","3","","0"});
        list.add( new String[]{"78","得点12","12得点","3","","0"});
        list.add( new String[]{"79","全国偏差値12","12偏差値","5","","0"});
        list.add( new String[]{"80","学力レベル12","12レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"81","科目コード13","科目コード13","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"82","科目名13","科目名13","20","","1"});
        list.add( new String[]{"83","配点／換算得点13","13配点","3","","0"});
        list.add( new String[]{"84","得点13","13得点","3","","0"});
        list.add( new String[]{"85","全国偏差値13","13偏差値","5","","0"});
        list.add( new String[]{"86","学力レベル13","13レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"87","科目コード14","科目コード14","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"88","科目名14","科目名14","20","","1"});
        list.add( new String[]{"89","配点／換算得点14","14配点","3","","0"});
        list.add( new String[]{"90","得点14","14得点","3","","0"});
        list.add( new String[]{"91","全国偏差値14","14偏差値","5","","0"});
        list.add( new String[]{"92","学力レベル14","14レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"93","科目コード15","科目コード15","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"94","科目名15","科目名15","20","","1"});
        list.add( new String[]{"95","配点／換算得点15","15配点","3","","0"});
        list.add( new String[]{"96","得点15","15得点","3","","0"});
        list.add( new String[]{"97","全国偏差値15","15偏差値","5","","0"});
        list.add( new String[]{"98","学力レベル15","15レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"99","科目コード16","科目コード16","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"100","科目名16","科目名16","20","","1"});
        list.add( new String[]{"101","配点／換算得点16","16配点","3","","0"});
        list.add( new String[]{"102","得点16","16得点","3","","0"});
        list.add( new String[]{"103","全国偏差値16","16偏差値","5","","0"});
        list.add( new String[]{"104","学力レベル16","16レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"105","科目コード17","科目コード17","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"106","科目名17","科目名17","20","","1"});
        list.add( new String[]{"107","配点／換算得点17","17配点","3","","0"});
        list.add( new String[]{"108","得点17","17得点","3","","0"});
        list.add( new String[]{"109","全国偏差値17","17偏差値","5","","0"});
        list.add( new String[]{"110","学力レベル17","17レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"111","科目コード18","科目コード18","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"112","科目名18","科目名18","20","","1"});
        list.add( new String[]{"113","配点／換算得点18","18配点","3","","0"});
        list.add( new String[]{"114","得点18","18得点","3","","0"});
        list.add( new String[]{"115","全国偏差値18","18偏差値","5","","0"});
        list.add( new String[]{"116","学力レベル18","18レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"117","科目コード19","科目コード19","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"118","科目名19","科目名19","20","","1"});
        list.add( new String[]{"119","配点／換算得点19","19配点","3","","0"});
        list.add( new String[]{"120","得点19","19得点","3","","0"});
        list.add( new String[]{"121","全国偏差値19","19偏差値","5","","0"});
        list.add( new String[]{"122","学力レベル19","19レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"123","科目コード20","科目コード20","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"124","科目名20","科目名20","20","","1"});
        list.add( new String[]{"125","配点／換算得点20","20配点","3","","0"});
        list.add( new String[]{"126","得点20","20得点","3","","0"});
        list.add( new String[]{"127","全国偏差値20","20偏差値","5","","0"});
        list.add( new String[]{"128","学力レベル20","20レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"129","科目コード21","科目コード21","4",NOTE_SUBJECT,"0"});
        list.add( new String[]{"130","科目名21","科目名21","20","","1"});
        list.add( new String[]{"131","配点／換算得点21","21配点","3","","0"});
        list.add( new String[]{"132","得点21","21得点","3","","0"});
        list.add( new String[]{"133","全国偏差値21","21偏差値","5","","0"});
        list.add( new String[]{"134","学力レベル21","21レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"135","科目コード22","科目コード22","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"136","科目名22","科目名22","20","","1"});
        list.add( new String[]{"137","配点／換算得点22","22配点","3","","0"});
        list.add( new String[]{"138","得点22","22得点","3","","0"});
        list.add( new String[]{"139","全国偏差値22","22偏差値","5","","0"});
        list.add( new String[]{"140","学力レベル22","22レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"141","科目コード23","科目コード23","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{"142","科目名23","科目名23","20","","1"});
        list.add( new String[]{"143","配点／換算得点23","23配点","3","","0"});
        list.add( new String[]{"144","得点23","23得点","3","","0"});
        list.add( new String[]{"145","全国偏差値23","23偏差値","5","","0"});
        list.add( new String[]{"146","学力レベル23","23レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{"147","英語＋リスニング","未使用A","5","","0"});
        list.add( new String[]{"148","現文換算得点","未使用B","3","マーク系模試でセンター大を判定する場合の国語現代文部分の換算得点","0"});
        list.add( new String[]{"149","古文換算得点","未使用C","3","マーク系模試でセンター大を判定する場合の国語古文部分の換算得点","0"});
        list.add( new String[]{"150","漢文換算得点","未使用D","3","マーク系模試でセンター大を判定する場合の国語漢文部分の換算得点","0"});
        list.add( new String[]{"151","英語リスニング選択","未使用E","1","","0"});
        list.add( new String[]{"152","物理選択","未使用F","1","","0"});
        list.add( new String[]{"153","化学選択","未使用G","1","","0"});
        list.add( new String[]{"154","生物選択","未使用H","1","","0"});
        list.add( new String[]{"155","地学選択","未使用I","1","","0"});

        list.add( new String[]{"156","志望大1コード(5桁)","志望大1コード5桁","5","5桁コード","0"});
        list.add( new String[]{"157","志望大1コード","志望大1コード10桁","10","","0"});
        list.add( new String[]{"158","志望大学名1","志望大1名称","36","短縮名","1"});
        list.add( new String[]{"159","評価偏差値評価得点1","志望大1評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
        list.add( new String[]{"160","一次評価1","志望大1セ評価","3","","0"});
        list.add( new String[]{"161","二次評価1","志望大1二評価","3","","0"});
        list.add( new String[]{"162","総合評価ポイント1","志望大1総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{"163","総合評価1","志望大1総評価","3","","0"});

        list.add( new String[]{"164","志望大2コード(5桁)","志望大2コード5桁","5","5桁コード","0"});
        list.add( new String[]{"165","志望大2コード","志望大2コード10桁","10","","0"});
        list.add( new String[]{"166","志望大学名2","志望大2名称","36","短縮名","1"});
        list.add( new String[]{"167","評価偏差値評価得点2","志望大2評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
        list.add( new String[]{"168","一次評価2","志望大2セ評価","3","","0"});
        list.add( new String[]{"169","二次評価2","志望大2二評価","3","","0"});
        list.add( new String[]{"170","総合評価ポイント2","志望大2総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{"171","総合評価2","志望大2総評価","3","","0"});

        list.add( new String[]{"172","志望大3コード(5桁)","志望大3コード5桁","5","5桁コード","0"});
        list.add( new String[]{"173","志望大3コード","志望大3コード10桁","10","","0"});
        list.add( new String[]{"174","志望大学名3","志望大3名称","36","短縮名","1"});
        list.add( new String[]{"175","評価偏差値評価得点3","志望大3評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
        list.add( new String[]{"176","一次評価3","志望大3セ評価","3","","0"});
        list.add( new String[]{"177","二次評価3","志望大3二評価","3","","0"});
        list.add( new String[]{"178","総合評価ポイント3","志望大3総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{"179","総合評価3","志望大3総評価","3","","0"});

        list.add( new String[]{"180","志望大4コード(5桁)","志望大4コード5桁","5","5桁コード","0"});
        list.add( new String[]{"181","志望大4コード","志望大4コード10桁","10","","0"});
        list.add( new String[]{"182","志望大学名4","志望大4名称","36","短縮名","1"});
        list.add( new String[]{"183","評価偏差値評価得点4","志望大4評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
        list.add( new String[]{"184","一次評価4","志望大4セ評価","3","","0"});
        list.add( new String[]{"185","二次評価4","志望大4二評価","3","","0"});
        list.add( new String[]{"186","総合評価ポイント4","志望大4総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{"187","総合評価4","志望大4総評価","3","","0"});

        list.add( new String[]{"188","志望大5コード(5桁)","志望大5コード5桁","5","5桁コード","0"});
        list.add( new String[]{"189","志望大5コード","志望大5コード10桁","10","","0"});
        list.add( new String[]{"190","志望大学名5","志望大5名称","36","短縮名","1"});
        list.add( new String[]{"191","評価偏差値評価得点5","志望大5評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
        list.add( new String[]{"192","一次評価5","志望大5セ評価","3","","0"});
        list.add( new String[]{"193","二次評価5","志望大5二評価","3","","0"});
        list.add( new String[]{"194","総合評価ポイント5","志望大5総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{"195","総合評価5","志望大5総評価","3","","0"});

        list.add( new String[]{"196","志望大6コード(5桁)","志望大6コード5桁","5","5桁コード","0"});
        list.add( new String[]{"197","志望大6コード","志望大6コード10桁","10","","0"});
        list.add( new String[]{"198","志望大学名6","志望大6名称","36","短縮名","1"});
        list.add( new String[]{"199","評価偏差値評価得点6","志望大6評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
        list.add( new String[]{"200","一次評価6","志望大6セ評価","3","","0"});
        list.add( new String[]{"201","二次評価6","志望大6二評価","3","","0"});
        list.add( new String[]{"202","総合評価ポイント6","志望大6総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{"203","総合評価6","志望大6総評価","3","","0"});

        list.add( new String[]{"204","志望大7コード(5桁)","志望大7コード5桁","5","5桁コード","0"});
        list.add( new String[]{"205","志望大7コード","志望大7コード10桁","10","","0"});
        list.add( new String[]{"206","志望大学名7","志望大7名称","36","短縮名","1"});
        list.add( new String[]{"207","評価偏差値評価得点7","志望大7評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
        list.add( new String[]{"208","一次評価7","志望大7セ評価","3","","0"});
        list.add( new String[]{"209","二次評価7","志望大7二評価","3","","0"});
        list.add( new String[]{"210","総合評価ポイント7","志望大7総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{"211","総合評価7","志望大7総評価","3","","0"});

        list.add( new String[]{"212","志望大8コード(5桁)","志望大8コード5桁","5","5桁コード","0"});
        list.add( new String[]{"213","志望大8コード","志望大8コード10桁","10","","0"});
        list.add( new String[]{"214","志望大学名8","志望大8名称","36","短縮名","1"});
        list.add( new String[]{"215","評価偏差値評価得点8","志望大8評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
        list.add( new String[]{"216","一次評価8","志望大8セ評価","3","","0"});
        list.add( new String[]{"217","二次評価8","志望大8二評価","3","","0"});
        list.add( new String[]{"218","総合評価ポイント8","志望大8総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{"219","総合評価8","志望大8総評価","3","","0"});

        list.add( new String[]{"220","志望大9コード(5桁)","志望大9コード5桁","5","5桁コード","0"});
        list.add( new String[]{"221","志望大9コード","志望大9コード10桁","10","","0"});
        list.add( new String[]{"222","志望大学名9","志望大9名称","36","短縮名","1"});
        list.add( new String[]{"223","評価偏差値評価得点9","志望大9評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
        list.add( new String[]{"224","一次評価9","志望大9セ評価","3","","0"});
        list.add( new String[]{"225","二次評価9","志望大9二評価","3","","0"});
        list.add( new String[]{"226","総合評価ポイント9","志望大9総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{"227","総合評価9","志望大9総評価","3","","0"});

        return list;
    }

    /**
     * 個人科目別成績リスト（レイアウト�B）を取得
     *
     * @return list 個人科目別成績リスト(項目名,表示項目名,byte数,備考,文字列/数値)
     */
    // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
//    private static List getExamSeiseki3() {
//        List list = new ArrayList();
//        list.add( new String[]{"1","通番","通番","10","","1"});
//        list.add( new String[]{"2","年度","年度","4","西暦年４桁。","1"});
//        list.add( new String[]{"3","模試コード","模試コード","2",NOTE_EXAM,"1"});
//        list.add( new String[]{"4","学校コード","学校コード","5","","1"});
//        list.add( new String[]{"5","学年","学年","2","","1"});
//        list.add( new String[]{"6","クラス","クラス","2","","1"});
//        list.add( new String[]{"7","クラス番号","クラス番号","5","","1"});
//        list.add( new String[]{"8","カナ氏名","カナ氏名","14","半角ｶﾅ","1"});
//
//
//        list.add( new String[]{"246","受験型","受験型","1","受験届に記入した受験型。(マーク・センタプレ)1=5-7理系、2=5-7文系、3=5-6型、4=5-5型、5=英数理、6=英国地公、7=英数国　（記述）1=国理、2=国文、3=私理、4=私文","0"});
//        list.add( new String[]{"247","文理コード","文理コード","1","1=文系、2=理系　※「受験届に記入なし」または「無効な記入」の場合は、当塾で文系または理系を判断します。","0"});
//
//        list.add( new String[]{"9","科目コード1","科目コード01","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"10","科目名1","科目名01","20","","1"});
//        list.add( new String[]{"11","配点／換算得点1","01配点","3","","0"});
//        list.add( new String[]{"12","得点1","01得点","3","受験学力測定テストでは「スコア」となります。","0"});
//        list.add( new String[]{"13","全国偏差値1","01偏差値","5","受験学力測定テストでは「到達エリア（アラビア数字で表現）」となります。","0"});
//        list.add( new String[]{"14","学力レベル1","01レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"15","科目コード2","科目コード02","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"16","科目名2","科目名02","20","","1"});
//        list.add( new String[]{"17","配点／換算得点2","02配点","3","","0"});
//        list.add( new String[]{"18","得点2","02得点","3","","0"});
//        list.add( new String[]{"19","全国偏差値2","02偏差値","5","","0"});
//        list.add( new String[]{"20","学力レベル2","02レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"21","科目コード3","科目コード03","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"22","科目名3","科目名03","20","","1"});
//        list.add( new String[]{"23","配点／換算得点3","03配点","3","","0"});
//        list.add( new String[]{"24","得点3","03得点","3","受験学力測定テストでは「スコア」となります。","0"});
//        list.add( new String[]{"25","全国偏差値3","03偏差値","5","受験学力測定テストでは「到達エリア（アラビア数字で表現）」となります。","0"});
//        list.add( new String[]{"26","学力レベル3","03レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"27","科目コード4","科目コード04","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"28","科目名4","科目名04","20","","1"});
//        list.add( new String[]{"29","配点／換算得点4","04配点","3","","0"});
//        list.add( new String[]{"30","得点4","04得点","3","受験学力測定テストでは「スコア」となります。","0"});
//        list.add( new String[]{"31","全国偏差値4","04偏差値","5","受験学力測定テストでは「到達エリア（アラビア数字で表現）」となります。","0"});
//        list.add( new String[]{"32","学力レベル4","04レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"33","科目コード5","科目コード05","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"34","科目名5","科目名05","20","","1"});
//        list.add( new String[]{"35","配点／換算得点5","05配点","3","","0"});
//        list.add( new String[]{"36","得点5","05得点","3","","0"});
//        list.add( new String[]{"37","全国偏差値5","05偏差値","5","","0"});
//        list.add( new String[]{"38","学力レベル5","05レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"39","科目コード6","科目コード06","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"40","科目名6","科目名06","20","","1"});
//        list.add( new String[]{"41","配点／換算得点6","06配点","3","","0"});
//        list.add( new String[]{"42","得点6","06得点","3","受験学力測定テストでは「スコア」となります。","0"});
//        list.add( new String[]{"43","全国偏差値6","06偏差値","5","受験学力測定テストでは「到達エリア（アラビア数字で表現）」となります。","0"});
//        list.add( new String[]{"44","学力レベル6","06レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"45","科目コード7","科目コード07","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"46","科目名7","科目名07","20","","1"});
//        list.add( new String[]{"47","配点／換算得点7","07配点","3","","0"});
//        list.add( new String[]{"48","得点7","07得点","3","受験学力測定テストでは「スコア」となります。","0"});
//        list.add( new String[]{"49","全国偏差値7","07偏差値","5","受験学力測定テストでは「到達エリア（アラビア数字で表現）」となります。","0"});
//        list.add( new String[]{"50","学力レベル7","07レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"51","科目コード8","科目コード08","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"52","科目名8","科目名08","20","","1"});
//        list.add( new String[]{"53","配点／換算得点8","08配点","3","","0"});
//        list.add( new String[]{"54","得点8","08得点","3","受験学力測定テストでは「スコア」となります。","0"});
//        list.add( new String[]{"55","全国偏差値8","08偏差値","5","受験学力測定テストでは「到達エリア（アラビア数字で表現）」となります。","0"});
//        list.add( new String[]{"56","学力レベル8","08レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"57","科目コード9","科目コード09","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"58","科目名9","科目名09","20","","1"});
//        list.add( new String[]{"59","配点／換算得点9","09配点","3","","0"});
//        list.add( new String[]{"60","得点9","09得点","3","","0"});
//        list.add( new String[]{"61","全国偏差値9","09偏差値","5","","0"});
//        list.add( new String[]{"62","学力レベル9","09レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"63","科目コード10","科目コード10","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"64","科目名10","科目名10","20","","1"});
//        list.add( new String[]{"65","配点／換算得点10","10配点","3","","0"});
//        list.add( new String[]{"66","得点10","10得点","3","","0"});
//        list.add( new String[]{"67","全国偏差値10","10偏差値","5","","0"});
//        list.add( new String[]{"68","学力レベル10","10レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"69","科目コード11","科目コード11","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"70","科目名11","科目名11","20","","0"});
//        list.add( new String[]{"71","配点／換算得点11","11配点","3","","0"});
//        list.add( new String[]{"72","得点11","11得点","3","","0"});
//        list.add( new String[]{"73","全国偏差値11","11偏差値","5","","0"});
//        list.add( new String[]{"74","学力レベル11","11レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"75","科目コード12","科目コード12","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"76","科目名12","科目名12","20","","1"});
//        list.add( new String[]{"77","配点／換算得点12","12配点","3","","0"});
//        list.add( new String[]{"78","得点12","12得点","3","","0"});
//        list.add( new String[]{"79","全国偏差値12","12偏差値","5","","0"});
//        list.add( new String[]{"80","学力レベル12","12レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"81","科目コード13","科目コード13","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"82","科目名13","科目名13","20","","1"});
//        list.add( new String[]{"83","配点／換算得点13","13配点","3","","0"});
//        list.add( new String[]{"84","得点13","13得点","3","","0"});
//        list.add( new String[]{"85","全国偏差値13","13偏差値","5","","0"});
//        list.add( new String[]{"86","学力レベル13","13レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"87","科目コード14","科目コード14","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"88","科目名14","科目名14","20","","1"});
//        list.add( new String[]{"89","配点／換算得点14","14配点","3","","0"});
//        list.add( new String[]{"90","得点14","14得点","3","","0"});
//        list.add( new String[]{"91","全国偏差値14","14偏差値","5","","0"});
//        list.add( new String[]{"92","学力レベル14","14レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"93","科目コード15","科目コード15","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"94","科目名15","科目名15","20","","1"});
//        list.add( new String[]{"95","配点／換算得点15","15配点","3","","0"});
//        list.add( new String[]{"96","得点15","15得点","3","","0"});
//        list.add( new String[]{"97","全国偏差値15","15偏差値","5","","0"});
//        list.add( new String[]{"98","学力レベル15","15レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"99","科目コード16","科目コード16","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"100","科目名16","科目名16","20","","1"});
//        list.add( new String[]{"101","配点／換算得点16","16配点","3","","0"});
//        list.add( new String[]{"102","得点16","16得点","3","","0"});
//        list.add( new String[]{"103","全国偏差値16","16偏差値","5","","0"});
//        list.add( new String[]{"104","学力レベル16","16レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"105","科目コード17","科目コード17","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"106","科目名17","科目名17","20","","1"});
//        list.add( new String[]{"107","配点／換算得点17","17配点","3","","0"});
//        list.add( new String[]{"108","得点17","17得点","3","","0"});
//        list.add( new String[]{"109","全国偏差値17","17偏差値","5","","0"});
//        list.add( new String[]{"110","学力レベル17","17レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"111","科目コード18","科目コード18","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"112","科目名18","科目名18","20","","1"});
//        list.add( new String[]{"113","配点／換算得点18","18配点","3","","0"});
//        list.add( new String[]{"114","得点18","18得点","3","","0"});
//        list.add( new String[]{"115","全国偏差値18","18偏差値","5","","0"});
//        list.add( new String[]{"116","学力レベル18","18レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"117","科目コード19","科目コード19","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"118","科目名19","科目名19","20","","1"});
//        list.add( new String[]{"119","配点／換算得点19","19配点","3","","0"});
//        list.add( new String[]{"120","得点19","19得点","3","","0"});
//        list.add( new String[]{"121","全国偏差値19","19偏差値","5","","0"});
//        list.add( new String[]{"122","学力レベル19","19レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"123","科目コード20","科目コード20","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"124","科目名20","科目名20","20","","1"});
//        list.add( new String[]{"125","配点／換算得点20","20配点","3","","0"});
//        list.add( new String[]{"126","得点20","20得点","3","","0"});
//        list.add( new String[]{"127","全国偏差値20","20偏差値","5","","0"});
//        list.add( new String[]{"128","学力レベル20","20レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"129","科目コード21","科目コード21","4",NOTE_SUBJECT,"0"});
//        list.add( new String[]{"130","科目名21","科目名21","20","","1"});
//        list.add( new String[]{"131","配点／換算得点21","21配点","3","","0"});
//        list.add( new String[]{"132","得点21","21得点","3","","0"});
//        list.add( new String[]{"133","全国偏差値21","21偏差値","5","","0"});
//        list.add( new String[]{"134","学力レベル21","21レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"135","科目コード22","科目コード22","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"136","科目名22","科目名22","20","","1"});
//        list.add( new String[]{"137","配点／換算得点22","22配点","3","","0"});
//        list.add( new String[]{"138","得点22","22得点","3","","0"});
//        list.add( new String[]{"139","全国偏差値22","22偏差値","5","","0"});
//        list.add( new String[]{"140","学力レベル22","22レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"141","科目コード23","科目コード23","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"142","科目名23","科目名23","20","","1"});
//        list.add( new String[]{"143","配点／換算得点23","23配点","3","","0"});
//        list.add( new String[]{"144","得点23","23得点","3","","0"});
//        list.add( new String[]{"145","全国偏差値23","23偏差値","5","","0"});
//        list.add( new String[]{"146","学力レベル23","23レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"228","科目コード24","科目コード24","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"229","科目名24","科目名24","20","","1"});
//        list.add( new String[]{"230","配点／換算得点24","24配点","3","","0"});
//        list.add( new String[]{"231","得点24","24得点","3","","0"});
//        list.add( new String[]{"232","全国偏差値24","24偏差値","5","","0"});
//        list.add( new String[]{"233","学力レベル24","24レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"234","科目コード25","科目コード25","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"235","科目名25","科目名25","20","","1"});
//        list.add( new String[]{"236","配点／換算得点25","25配点","3","","0"});
//        list.add( new String[]{"237","得点25","25得点","3","","0"});
//        list.add( new String[]{"238","全国偏差値25","25偏差値","5","","0"});
//        list.add( new String[]{"239","学力レベル25","25レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//        list.add( new String[]{"240","科目コード26","科目コード26","4",NOTE_SUBJECT,"1"});
//        list.add( new String[]{"241","科目名26","科目名26","20","","1"});
//        list.add( new String[]{"242","配点／換算得点26","26配点","3","","0"});
//        list.add( new String[]{"243","得点26","26得点","3","","0"});
//        list.add( new String[]{"244","全国偏差値26","26偏差値","5","","0"});
//        list.add( new String[]{"245","学力レベル26","26レベル","1","全国偏差値を7段階に分け指標化したものです。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});
//
//
//        list.add( new String[]{"147","英語＋リスニング","未使用A","5","（マーク系模試のみ）","0"});
//        list.add( new String[]{"148","現文換算得点","未使用B","3","マーク系模試でセンター大を判定する場合の国語現代文部分の換算得点（センター・リサーチ時は得点）","0"});
//        list.add( new String[]{"149","古文換算得点","未使用C","3","マーク系模試でセンター大を判定する場合の国語古文部分の換算得点（センター・リサーチ時は得点）","0"});
//        list.add( new String[]{"150","漢文換算得点","未使用D","3","マーク系模試でセンター大を判定する場合の国語漢文部分の換算得点（センター・リサーチ時は得点）","0"});
///*** 2015.03.30 MOD-S ***/
//        //        list.add( new String[]{"151","英語リスニング選択","未使用E","1","マーク模試、センタープレテスト、センター・リサーチ時は理科の第１解答科目を示します。0=物理、1=化学、2=生物、3=地学、4=理科総合A、5=理科総合B","0"});
//        list.add( new String[]{"151","英語リスニング選択","未使用E","1","マーク模試、センタープレテスト、センター・リサーチ時は理科の第１解答科目を示します。0=物理、1=化学、2=生物、3=地学","0"});
///*** 2015.03.30 MOD-E ***/
//        list.add( new String[]{"152","物理選択","未使用F","1","マーク模試、センタープレテスト、センター・リサーチ時は地歴・公民の第１解答科目を示します。0=世界史B、1=日本史B、2=地理B、3=倫政、4=政治経済、5=現代社会、6=倫理、7=世界史A、8=日本史A、9=地理A","0"});
//        list.add( new String[]{"153","化学選択","未使用G","1","","0"});
//        list.add( new String[]{"154","生物選択","未使用H","1","","0"});
//        list.add( new String[]{"155","地学選択","未使用I","1","","0"});
//
//        list.add( new String[]{"156","志望大1コード(5桁)","志望大1コード5桁","5","5桁コード","0"});
//        list.add( new String[]{"157","志望大1コード","志望大1コード10桁","10","","0"});
//        list.add( new String[]{"158","志望大学名1","志望大1名称","36","短縮名","1"});
//        list.add( new String[]{"159","評価偏差値評価得点1","志望大1評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
//        list.add( new String[]{"160","センタ評価1","志望大1セ評価","3","","0"});
//        list.add( new String[]{"161","二次評価1","志望大1二評価","3","","0"});
//        list.add( new String[]{"162","総合評価ポイント1","志望大1総評ポ","2","総合評価を指数化したもの","0"});
//        list.add( new String[]{"163","総合評価1","志望大1総評価","3","","0"});
//
//        list.add( new String[]{"164","志望大2コード(5桁)","志望大2コード5桁","5","5桁コード","0"});
//        list.add( new String[]{"165","志望大2コード","志望大2コード10桁","10","","0"});
//        list.add( new String[]{"166","志望大学名2","志望大2名称","36","短縮名","1"});
//        list.add( new String[]{"167","評価偏差値評価得点2","志望大2評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
//        list.add( new String[]{"168","センタ評価2","志望大2セ評価","3","","0"});
//        list.add( new String[]{"169","二次評価2","志望大2二評価","3","","0"});
//        list.add( new String[]{"170","総合評価ポイント2","志望大2総評ポ","2","総合評価を指数化したもの","0"});
//        list.add( new String[]{"171","総合評価2","志望大2総評価","3","","0"});
//
//        list.add( new String[]{"172","志望大3コード(5桁)","志望大3コード5桁","5","5桁コード","0"});
//        list.add( new String[]{"173","志望大3コード","志望大3コード10桁","10","","0"});
//        list.add( new String[]{"174","志望大学名3","志望大3名称","36","短縮名","1"});
//        list.add( new String[]{"175","評価偏差値評価得点3","志望大3評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
//        list.add( new String[]{"176","センタ評価3","志望大3セ評価","3","","0"});
//        list.add( new String[]{"177","二次評価3","志望大3二評価","3","","0"});
//        list.add( new String[]{"178","総合評価ポイント3","志望大3総評ポ","2","総合評価を指数化したもの","0"});
//        list.add( new String[]{"179","総合評価3","志望大3総評価","3","","0"});
//
//        list.add( new String[]{"180","志望大4コード(5桁)","志望大4コード5桁","5","5桁コード","0"});
//        list.add( new String[]{"181","志望大4コード","志望大4コード10桁","10","","0"});
//        list.add( new String[]{"182","志望大学名4","志望大4名称","36","短縮名","1"});
//        list.add( new String[]{"183","評価偏差値評価得点4","志望大4評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
//        list.add( new String[]{"184","センタ評価4","志望大4セ評価","3","","0"});
//        list.add( new String[]{"185","二次評価4","志望大4二評価","3","","0"});
//        list.add( new String[]{"186","総合評価ポイント4","志望大4総評ポ","2","総合評価を指数化したもの","0"});
//        list.add( new String[]{"187","総合評価4","志望大4総評価","3","","0"});
//
//        list.add( new String[]{"188","志望大5コード(5桁)","志望大5コード5桁","5","5桁コード","0"});
//        list.add( new String[]{"189","志望大5コード","志望大5コード10桁","10","","0"});
//        list.add( new String[]{"190","志望大学名5","志望大5名称","36","短縮名","1"});
//        list.add( new String[]{"191","評価偏差値評価得点5","志望大5評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
//        list.add( new String[]{"192","センタ評価5","志望大5セ評価","3","","0"});
//        list.add( new String[]{"193","二次評価5","志望大5二評価","3","","0"});
//        list.add( new String[]{"194","総合評価ポイント5","志望大5総評ポ","2","総合評価を指数化したもの","0"});
//        list.add( new String[]{"195","総合評価5","志望大5総評価","3","","0"});
//
//        list.add( new String[]{"196","志望大6コード(5桁)","志望大6コード5桁","5","5桁コード","0"});
//        list.add( new String[]{"197","志望大6コード","志望大6コード10桁","10","","0"});
//        list.add( new String[]{"198","志望大学名6","志望大6名称","36","短縮名","1"});
//        list.add( new String[]{"199","評価偏差値評価得点6","志望大6評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
//        list.add( new String[]{"200","センタ評価6","志望大6セ評価","3","","0"});
//        list.add( new String[]{"201","二次評価6","志望大6二評価","3","","0"});
//        list.add( new String[]{"202","総合評価ポイント6","志望大6総評ポ","2","総合評価を指数化したもの","0"});
//        list.add( new String[]{"203","総合評価6","志望大6総評価","3","","0"});
//
//        list.add( new String[]{"204","志望大7コード(5桁)","志望大7コード5桁","5","5桁コード","0"});
//        list.add( new String[]{"205","志望大7コード","志望大7コード10桁","10","","0"});
//        list.add( new String[]{"206","志望大学名7","志望大7名称","36","短縮名","1"});
//        list.add( new String[]{"207","評価偏差値評価得点7","志望大7評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
//        list.add( new String[]{"208","センタ評価7","志望大7セ評価","3","","0"});
//        list.add( new String[]{"209","二次評価7","志望大7二評価","3","","0"});
//        list.add( new String[]{"210","総合評価ポイント7","志望大7総評ポ","2","総合評価を指数化したもの","0"});
//        list.add( new String[]{"211","総合評価7","志望大7総評価","3","","0"});
//
//        list.add( new String[]{"212","志望大8コード(5桁)","志望大8コード5桁","5","5桁コード","0"});
//        list.add( new String[]{"213","志望大8コード","志望大8コード10桁","10","","0"});
//        list.add( new String[]{"214","志望大学名8","志望大8名称","36","短縮名","1"});
//        list.add( new String[]{"215","評価偏差値評価得点8","志望大8評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
//        list.add( new String[]{"216","センタ評価8","志望大8セ評価","3","","0"});
//        list.add( new String[]{"217","二次評価8","志望大8二評価","3","","0"});
//        list.add( new String[]{"218","総合評価ポイント8","志望大8総評ポ","2","総合評価を指数化したもの","0"});
//        list.add( new String[]{"219","総合評価8","志望大8総評価","3","","0"});
//
//        list.add( new String[]{"220","志望大9コード(5桁)","志望大9コード5桁","5","5桁コード","0"});
//        list.add( new String[]{"221","志望大9コード","志望大9コード10桁","10","","0"});
//        list.add( new String[]{"222","志望大学名9","志望大9名称","36","短縮名","1"});
//        list.add( new String[]{"223","評価偏差値評価得点9","志望大9評価成績","5","マーク模試時は評価得点、記述模試時は評価偏差値","0"});
//        list.add( new String[]{"224","センタ評価9","志望大9セ評価","3","","0"});
//        list.add( new String[]{"225","二次評価9","志望大9二評価","3","","0"});
//        list.add( new String[]{"226","総合評価ポイント9","志望大9総評ポ","2","総合評価を指数化したもの","0"});
//        list.add( new String[]{"227","総合評価9","志望大9総評価","3","","0"});
//
//        return list;
//    }
    private static List getExamSeiseki3(ExamData exam) {

        // 年度による表示項目名変更フラグ
        boolean flag = false;

        if(exam != null) {
            // 2020年度以降 -> flag=true
            if (exam.getExamYear().compareTo(HEADER5) >= 0) {
                flag = true;
            }
        }

        List list = new ArrayList();
        int i = 0;

        list.add( new String[]{String.valueOf(++i),"通番","通番","10","","1"});
        list.add( new String[]{String.valueOf(++i),"年度","年度","4","西暦年４桁。","1"});
        list.add( new String[]{String.valueOf(++i),"模試コード","模試コード","2",NOTE_EXAM,"1"});
        list.add( new String[]{String.valueOf(++i),"学校コード","学校コード","5","","1"});
        list.add( new String[]{String.valueOf(++i),"学年","学年","2","","1"});
        list.add( new String[]{String.valueOf(++i),"クラス","クラス","2","","1"});
        list.add( new String[]{String.valueOf(++i),"クラス番号","クラス番号","5","","1"});
        list.add( new String[]{String.valueOf(++i),"カナ氏名","カナ氏名","14","半角ｶﾅ","1"});

        list.add( new String[]{String.valueOf(++i),"受験型","受験型","1","受験届に記入した受験型。(共通テスト系模試)1=5-7理系、2=5-7文系、3=5-6型、4=5-5型、5=英数理、6=英国地公、7=英数国　（記述模試・記述高２模試・プライムステージ・高２プライムステージ）1=国理、2=国文、3=私理、4=私文、（高１・２模試・高１プライムステージ）1=英数、2=英国、3=英数国","0"});
        list.add( new String[]{String.valueOf(++i),"文理コード","文理コード","1","1=文系、2=理系　※「受験届に記入なし」または「無効な記入」の場合は、当塾で文系または理系を判断します。","0"});

        list.add( new String[]{String.valueOf(++i),"科目コード1","科目コード01","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名1","科目名01","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点1","01配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点1","01得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値1","01偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル1","01レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード2","科目コード02","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名2","科目名02","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点2","02配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点2","02得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値2","02偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル2","02レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード3","科目コード03","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名3","科目名03","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点3","03配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点3","03得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値3","03偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル3","03レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード4","科目コード04","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名4","科目名04","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点4","04配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点4","04得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値4","04偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル4","04レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード5","科目コード05","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名5","科目名05","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点5","05配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点5","05得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値5","05偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル5","05レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード6","科目コード06","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名6","科目名06","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点6","06配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点6","06得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値6","06偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル6","06レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード7","科目コード07","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名7","科目名07","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点7","07配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点7","07得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値7","07偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル7","07レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード8","科目コード08","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名8","科目名08","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点8","08配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点8","08得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値8","08偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル8","08レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード9","科目コード09","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名9","科目名09","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点9","09配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点9","09得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値9","09偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル9","09レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード10","科目コード10","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名10","科目名10","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点10","10配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点10","10得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値10","10偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル10","10レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード11","科目コード11","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名11","科目名11","20","","0"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点11","11配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点11","11得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値11","11偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル11","11レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード12","科目コード12","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名12","科目名12","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点12","12配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点12","12得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値12","12偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル12","12レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード13","科目コード13","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名13","科目名13","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点13","13配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点13","13得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値13","13偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル13","13レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード14","科目コード14","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名14","科目名14","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点14","14配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点14","14得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値14","14偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル14","14レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード15","科目コード15","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名15","科目名15","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点15","15配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点15","15得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値15","15偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル15","15レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード16","科目コード16","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名16","科目名16","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点16","16配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点16","16得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値16","16偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル16","16レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード17","科目コード17","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名17","科目名17","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点17","17配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点17","17得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値17","17偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル17","17レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード18","科目コード18","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名18","科目名18","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点18","18配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点18","18得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値18","18偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル18","18レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード19","科目コード19","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名19","科目名19","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点19","19配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点19","19得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値19","19偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル19","19レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード20","科目コード20","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名20","科目名20","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点20","20配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点20","20得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値20","20偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル20","20レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード21","科目コード21","4",NOTE_SUBJECT,"0"});
        list.add( new String[]{String.valueOf(++i),"科目名21","科目名21","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点21","21配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点21","21得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値21","21偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル21","21レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード22","科目コード22","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名22","科目名22","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点22","22配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点22","22得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値22","22偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル22","22レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード23","科目コード23","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名23","科目名23","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点23","23配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点23","23得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値23","23偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル23","23レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード24","科目コード24","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名24","科目名24","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点24","24配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点24","24得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値24","24偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル24","24レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード25","科目コード25","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名25","科目名25","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点25","25配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点25","25得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値25","25偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル25","25レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"科目コード26","科目コード26","4",NOTE_SUBJECT,"1"});
        list.add( new String[]{String.valueOf(++i),"科目名26","科目名26","20","","1"});
        list.add( new String[]{String.valueOf(++i),"配点／換算得点26","26配点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"得点26","26得点","3","","0"});
        list.add( new String[]{String.valueOf(++i),"全国偏差値26","26偏差値","5","","0"});
        list.add( new String[]{String.valueOf(++i),"学力レベル26","26レベル","1","全国偏差値を7段階に分け指標化したもの。S=65.0以上、A=60.0〜64.9、B=55.0〜59.9、C=50.0〜54.9、D=45.0〜49.9、E=40.0〜44.9、F=40.0未満","1"});

        list.add( new String[]{String.valueOf(++i),"英語＋リスニング偏差値","未使用A","5","（共通テスト系模試、共通テストリサーチのみ）","0"});
        list.add( new String[]{String.valueOf(++i),"現文換算得点","未使用B","3","共通テスト系模試で共通テスト利用私大を判定する場合の国語現代文部分の換算得点（共通テストリサーチ時は得点）","0"});
        list.add( new String[]{String.valueOf(++i),"古文換算得点","未使用C","3","共通テスト系模試で共通テスト利用私大を判定する場合の国語古文部分の換算得点（共通テストリサーチ時は得点）","0"});
        list.add( new String[]{String.valueOf(++i),"漢文換算得点","未使用D","3","共通テスト系模試で共通テスト利用私大を判定する場合の国語漢文部分の換算得点（共通テストリサーチ時は得点）","0"});

        list.add( new String[]{String.valueOf(++i),"理科第１解答科目／英語リスニング選択","未使用E","1","共通テスト系模試、共通テストリサーチでは理科の第１解答科目を示します。0=物理、1=化学、2=生物、3=地学 ※共通テスト高２模試は対象外<br>" +
                "記述模試では1=英語受験・リスニング非選択、2=英語受験・リスニング選択、9=英語未受験<br>記述高２模試では2=英語受験、9=英語未受験（リスニング選択を区別しません）","0"});

        list.add( new String[]{String.valueOf(++i),"地歴・公民第１解答科目","未使用F","1","共通テスト系模試、共通テストリサーチでは地歴・公民の第１解答科目を示します。0=世界史B、1=日本史B、2=地理B、3=倫政、4=政治経済、5=現代社会、6=倫理、7=世界史A、8=日本史A、9=地理A　※共通テスト高２模試は対象外","0"});
        list.add( new String[]{String.valueOf(++i),"（未使用）","未使用G","1","","0"});
        list.add( new String[]{String.valueOf(++i),"（未使用）","未使用H","1","","0"});
        list.add( new String[]{String.valueOf(++i),"（未使用）","未使用I","1","","0"});

        list.add( new String[]{String.valueOf(++i),"志望大1コード(5桁)","志望大1コード5桁","5","5桁コード","0"});
        list.add( new String[]{String.valueOf(++i),"志望大1コード","志望大1コード10桁","10","","0"});
        list.add( new String[]{String.valueOf(++i),"志望大学名1","志望大1名称","36","短縮名","1"});
        list.add( new String[]{String.valueOf(++i),"評価偏差値評価得点1","志望大1評価成績","5","共通テスト系模試・共通テストリサーチ時は評価得点、記述系模試時は評価偏差値","0"});
        if (flag) {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価1","志望大1テ評価","3","","0"});
        }
        else {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価1","志望大1セ評価","3","","0"});
        }
        list.add( new String[]{String.valueOf(++i),"二次評価1","志望大1二評価","3","","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価ポイント1","志望大1総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価1","志望大1総評価","3","","0"});

        list.add( new String[]{String.valueOf(++i),"志望大2コード(5桁)","志望大2コード5桁","5","5桁コード","0"});
        list.add( new String[]{String.valueOf(++i),"志望大2コード","志望大2コード10桁","10","","0"});
        list.add( new String[]{String.valueOf(++i),"志望大学名2","志望大2名称","36","短縮名","1"});
        list.add( new String[]{String.valueOf(++i),"評価偏差値評価得点2","志望大2評価成績","5","共通テスト系模試・共通テストリサーチ時は評価得点、記述系模試時は評価偏差","0"});
        if (flag) {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価2","志望大2テ評価","3","","0"});
        }
        else {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価2","志望大2セ評価","3","","0"});
        }
        list.add( new String[]{String.valueOf(++i),"二次評価2","志望大2二評価","3","","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価ポイント2","志望大2総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価2","志望大2総評価","3","","0"});

        list.add( new String[]{String.valueOf(++i),"志望大3コード(5桁)","志望大3コード5桁","5","5桁コード","0"});
        list.add( new String[]{String.valueOf(++i),"志望大3コード","志望大3コード10桁","10","","0"});
        list.add( new String[]{String.valueOf(++i),"志望大学名3","志望大3名称","36","短縮名","1"});
        list.add( new String[]{String.valueOf(++i),"評価偏差値評価得点3","志望大3評価成績","5","共通テスト系模試・共通テストリサーチ時は評価得点、記述系模試時は評価偏差値","0"});
        if (flag) {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価3","志望大3テ評価","3","","0"});
        }
        else {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価3","志望大3セ評価","3","","0"});
        }
        list.add( new String[]{String.valueOf(++i),"二次評価3","志望大3二評価","3","","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価ポイント3","志望大3総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価3","志望大3総評価","3","","0"});

        list.add( new String[]{String.valueOf(++i),"志望大4コード(5桁)","志望大4コード5桁","5","5桁コード","0"});
        list.add( new String[]{String.valueOf(++i),"志望大4コード","志望大4コード10桁","10","","0"});
        list.add( new String[]{String.valueOf(++i),"志望大学名4","志望大4名称","36","短縮名","1"});
        list.add( new String[]{String.valueOf(++i),"評価偏差値評価得点4","志望大4評価成績","5","共通テスト系模試・共通テストリサーチ時は評価得点、記述系模試時は評価偏差値","0"});
        if (flag) {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価4","志望大4テ評価","3","","0"});
        }
        else {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価4","志望大4セ評価","3","","0"});
        }
        list.add( new String[]{String.valueOf(++i),"二次評価4","志望大4二評価","3","","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価ポイント4","志望大4総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価4","志望大4総評価","3","","0"});

        list.add( new String[]{String.valueOf(++i),"志望大5コード(5桁)","志望大5コード5桁","5","5桁コード","0"});
        list.add( new String[]{String.valueOf(++i),"志望大5コード","志望大5コード10桁","10","","0"});
        list.add( new String[]{String.valueOf(++i),"志望大学名5","志望大5名称","36","短縮名","1"});
        list.add( new String[]{String.valueOf(++i),"評価偏差値評価得点5","志望大5評価成績","5","共通テスト系模試・共通テストリサーチ時は評価得点、記述系模試時は評価偏差値","0"});
        if (flag) {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価5","志望大5テ評価","3","","0"});
        }
        else {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価5","志望大5セ評価","3","","0"});
        }
        list.add( new String[]{String.valueOf(++i),"二次評価5","志望大5二評価","3","","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価ポイント5","志望大5総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価5","志望大5総評価","3","","0"});

        list.add( new String[]{String.valueOf(++i),"志望大6コード(5桁)","志望大6コード5桁","5","5桁コード","0"});
        list.add( new String[]{String.valueOf(++i),"志望大6コード","志望大6コード10桁","10","","0"});
        list.add( new String[]{String.valueOf(++i),"志望大学名6","志望大6名称","36","短縮名","1"});
        list.add( new String[]{String.valueOf(++i),"評価偏差値評価得点6","志望大6評価成績","5","共通テスト系模試・共通テストリサーチ時は評価得点、記述系模試時は評価偏差値","0"});
        if (flag) {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価6","志望大6テ評価","3","","0"});
        }
        else {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価6","志望大6セ評価","3","","0"});
        }
        list.add( new String[]{String.valueOf(++i),"二次評価6","志望大6二評価","3","","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価ポイント6","志望大6総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価6","志望大6総評価","3","","0"});

        list.add( new String[]{String.valueOf(++i),"志望大7コード(5桁)","志望大7コード5桁","5","5桁コード","0"});
        list.add( new String[]{String.valueOf(++i),"志望大7コード","志望大7コード10桁","10","","0"});
        list.add( new String[]{String.valueOf(++i),"志望大学名7","志望大7名称","36","短縮名","1"});
        list.add( new String[]{String.valueOf(++i),"評価偏差値評価得点7","志望大7評価成績","5","共通テスト系模試・共通テストリサーチ時は評価得点、記述系模試時は評価偏差値","0"});
        if (flag) {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価7","志望大7テ評価","3","","0"});
        }
        else {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価7","志望大7セ評価","3","","0"});
        }
        list.add( new String[]{String.valueOf(++i),"二次評価7","志望大7二評価","3","","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価ポイント7","志望大7総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価7","志望大7総評価","3","","0"});

        list.add( new String[]{String.valueOf(++i),"志望大8コード(5桁)","志望大8コード5桁","5","5桁コード","0"});
        list.add( new String[]{String.valueOf(++i),"志望大8コード","志望大8コード10桁","10","","0"});
        list.add( new String[]{String.valueOf(++i),"志望大学名8","志望大8名称","36","短縮名","1"});
        list.add( new String[]{String.valueOf(++i),"評価偏差値評価得点8","志望大8評価成績","5","共通テスト系模試・共通テストリサーチ時は評価得点、記述系模試時は評価偏差値","0"});
        if (flag) {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価8","志望大8テ評価","3","","0"});
        }
        else {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価8","志望大8セ評価","3","","0"});
        }
        list.add( new String[]{String.valueOf(++i),"二次評価8","志望大8二評価","3","","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価ポイント8","志望大8総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価8","志望大8総評価","3","","0"});

        list.add( new String[]{String.valueOf(++i),"志望大9コード(5桁)","志望大9コード5桁","5","5桁コード","0"});
        list.add( new String[]{String.valueOf(++i),"志望大9コード","志望大9コード10桁","10","","0"});
        list.add( new String[]{String.valueOf(++i),"志望大学名9","志望大9名称","36","短縮名","1"});
        list.add( new String[]{String.valueOf(++i),"評価偏差値評価得点9","志望大9評価成績","5","共通テスト系模試・共通テストリサーチ時は評価得点、記述系模試時は評価偏差値","0"});
        if (flag) {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価9","志望大9テ評価","3","","0"});
        }
        else {
            list.add( new String[]{String.valueOf(++i),"共通テスト評価9","志望大9セ評価","3","","0"});
        }
        list.add( new String[]{String.valueOf(++i),"二次評価9","志望大9二評価","3","","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価ポイント9","志望大9総評ポ","2","総合評価を指数化したもの","0"});
        list.add( new String[]{String.valueOf(++i),"総合評価9","志望大9総評価","3","","0"});

        return list;
    }
    // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END

    // 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD START
       /**
        *  （記述系模試）学力要素別成績データ項目リストを取得
        *
        * @return list （記述系模試）学力要素別成績データ項目リスト(項目名,表示項目名,byte数,備考,文字列 1/数値 0)
        */
       private static List getDescExamValue() {
           List list = new ArrayList();
           int i = 1;
           list.add( new String[]{String.valueOf(i++),"通番","通番","10","","1"});
           list.add( new String[]{String.valueOf(i++),"年度","年度","4","西暦年４桁.","1"});
           list.add( new String[]{String.valueOf(i++),"模試コード","模試コード","2",NOTE_EXAM,"1"});
           list.add( new String[]{String.valueOf(i++),"学校コード","学校コード","5","該当模試を一括受験した学校コード。","1"});
           list.add( new String[]{String.valueOf(i++),"学年","学年","2","","1"});
           list.add( new String[]{String.valueOf(i++),"クラス","クラス","2","","1"});
           list.add( new String[]{String.valueOf(i++),"番号","番号","5","","1"});
           list.add( new String[]{String.valueOf(i++),"氏名","氏名","14","半角カナ","1"});
           list.add( new String[]{String.valueOf(i++),"受験型","受験型","1","受験届に記入した受験型。（記述模試・記述高２模試・プライムステージ・高２プライムステージ）1=国理、2=国文、3=私理、4=私文、（高１・２模試・高１プライムステージ）1=英数、2=英国、3=英数国","0"});
           list.add( new String[]{String.valueOf(i++),"文理コード","文理コード","1","1=文系、2=理系　※「受験届に記入なし」または「無効な記入」の場合は、当塾で文系または理系を判断します。","0"});
           list.add( new String[]{String.valueOf(i++),"科目コード01","科目コード01","4",NOTE_SUBJECT,"1"});
           list.add( new String[]{String.valueOf(i++),"科目名01","科目名01","20","","1"});
           list.add( new String[]{String.valueOf(i++),"科目01知識技能得点率","科目01知識技能得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目01思考力判断力得点率","科目01思考力判断力得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目01表現力得点率","科目01表現力得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目コード02","科目コード02","4",NOTE_SUBJECT,"1"});
           list.add( new String[]{String.valueOf(i++),"科目名02","科目名02","20","","1"});
           list.add( new String[]{String.valueOf(i++),"科目02知識技能得点率","科目02知識技能得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目02思考力判断力得点率","科目02思考力判断力得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目02表現力得点率","科目02表現力得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目コード03","科目コード03","4",NOTE_SUBJECT,"1"});
           list.add( new String[]{String.valueOf(i++),"科目名03","科目名03","20","","1"});
           list.add( new String[]{String.valueOf(i++),"科目03知識技能得点率","科目03知識技能得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目03思考力判断力得点率","科目03思考力判断力得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目03表現力得点率","科目03表現力得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目コード04","科目コード04","4",NOTE_SUBJECT,"1"});
           list.add( new String[]{String.valueOf(i++),"科目名04","科目名04","20","","1"});
           list.add( new String[]{String.valueOf(i++),"科目04知識技能得点率","科目04知識技能得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目04思考力判断力得点率","科目04思考力判断力得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目04表現力得点率","科目04表現力得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目コード05","科目コード05","4",NOTE_SUBJECT,"1"});
           list.add( new String[]{String.valueOf(i++),"科目名05","科目名05","20","","1"});
           list.add( new String[]{String.valueOf(i++),"科目05知識技能得点率","科目05知識技能得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目05思考力判断力得点率","科目05思考力判断力得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目05表現力得点率","科目05表現力得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目コード06","科目コード06","4",NOTE_SUBJECT,"1"});
           list.add( new String[]{String.valueOf(i++),"科目名06","科目名06","20","","1"});
           list.add( new String[]{String.valueOf(i++),"科目06知識技能得点率","科目06知識技能得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目06思考力判断力得点率","科目06思考力判断力得点率","5","「999.9」の形。","0"});
           list.add( new String[]{String.valueOf(i++),"科目06表現力得点率","科目06表現力得点率","5","「999.9」の形。","0"});

           return list;
       }
    // 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD END

    /**
     * @return
     */
    public LinkedHashMap getOutputTableMap() {
        return outputTableMap;
    }

}

package jp.co.fj.keinavi.servlets.com_set;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.com_set.CompClassBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.com_set.CompClassData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.AbstractActionFormFactory;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM501Form;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM501Servlet extends AbstractCMServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 共通処理
		super.execute(request, response);

		// 共通アクションフォーム - session scope
		CM001Form scform = super.getCommonForm(request);
		// 個別アクションフォーム - session scope
		CM501Form siform = (CM501Form)scform.getActionForm("CM501Form");

		// ログイン情報
		LoginSession login = super.getLoginSession(request);
		// プロファイル
		Profile profile = super.getProfile(request);
		// 模試セッション
		ExamSession examSession = super.getExamSession(request);
		// 対象模試データ
		ExamData exam = examSession.getExamData(scform.getTargetYear(), scform.getTargetExam());

		// JSP
		if ("cm501".equals(getForward(request))) {
			// 個別アクションフォームの初期化
			if (siform == null) {
				siform = (CM501Form) AbstractActionFormFactory
										.getFactory("cm501").createActionForm(request);
				
				scform.setActionForm("CM501Form", siform);
			}

			Connection con = null; // DBコネクション
			try {
				con = super.getConnectionPool(request);

				// 比較対象クラスBean
				CompClassBean bean = new CompClassBean();
				bean.setConnection(null, con);
				bean.setTargetYear(scform.getTargetYear());
				bean.setTargetExam(scform.getTargetExam());
				bean.setBundleCD(getBundleCd(login, profile, scform.getTargetExam()));
				bean.setSchoolCD(login.getUserID());
				bean.execute();
				request.setAttribute("ClassBean", bean);

				// 個別選択の比較対象がNULLなら初期値を設定
				if (siform.getCompare2() == null) {
					CompClassData data = ProfileUtil.getCompClassValue(
						profile,
						examSession,
						exam,
						bean,
						false);

					siform.setCompare2(ProfileUtil.getCompClassIndValue(data, false));
				}

				// Beanのデータ格納順をソートする
				bean.setKeyArrayAll(siform.getCompare1());
				bean.setKeyArrayInd(siform.getCompare2());
				bean.sort();

			} catch (Exception e) {
				throw createServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}
			
			super.forward(request, response, JSP_CM501);
			
		// 不明なら転送
		} else {
			// アクションフォームを取得
			CM001Form rcform = (CM001Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.com_set.CM001Form");
			CM501Form riform = (CM501Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.com_set.CM501Form");

			// フォームの値をセッションに保持する
			siform.setSelection(riform.getSelection());
			
			siform.setCompare1(riform.getCompare1());
			siform.setGraph1(riform.getGraph1());
			
			// 比較対象にNULLは入れない
			if (riform.getCompare2() == null) siform.setCompare2(new String[0]);
			else siform.setCompare2(riform.getCompare2());

			siform.setGraph2(riform.getGraph2());

			// 変更フラグ
			scform.setChanged(rcform.getChanged());

			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

/*
 * クラス成績分析−クラス成績概況
 * 出力する帳票の判断
 */
 
package jp.co.fj.keinavi.excel.cls;
import jp.co.fj.keinavi.util.log.*;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.cls.C11Item;

public class C11 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean c11( C11Item c11Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {

		KNLog log = KNLog.getInstance(null,null,null);

		try{
			int ret = 0;
			//C11Itemから各帳票を出力
			if ((  c11Item.getIntHyouFlg()==0 )&&(  c11Item.getIntGraphFlg()==0 )&&
				( c11Item.getIntNinzuFlg()==0 )) {
				throw new Exception("C11 ERROR : フラグ異常");
			}
			if (  c11Item.getIntHyouFlg()==1 ) {
				C11_01 exceledit = new C11_01();
				ret = exceledit.c11_01EditExcel( c11Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C11_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C11_01");
			}
			if (  c11Item.getIntGraphFlg()==1 ) {
				C11_02 exceledit = new C11_02();
				ret = exceledit.c11_02EditExcel( c11Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C11_02","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C11_02");
			}
			if (  c11Item.getIntNinzuFlg()==1 && c11Item.getIntPitchFlg()==1  ) {
				C11_03 exceledit = new C11_03();
				ret = exceledit.c11_03EditExcel( c11Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C11_03","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C11_03");
			}
			if (  c11Item.getIntNinzuFlg()==1 && c11Item.getIntPitchFlg()==2  ) {
				C11_04 exceledit = new C11_04();
				ret = exceledit.c11_04EditExcel( c11Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C11_04","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C11_04");
			}
			if (  c11Item.getIntNinzuFlg()==1 && c11Item.getIntPitchFlg()==3  ) {
				C11_05 exceledit = new C11_05();
				ret = exceledit.c11_05EditExcel( c11Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C11_05","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C11_05");
			}

		} catch(Exception e) {
			log.Err("99C11","データセットエラー",e.toString());
			return false;
		}
		return true;
	}

}
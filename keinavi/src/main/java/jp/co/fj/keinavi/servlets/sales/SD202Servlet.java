package jp.co.fj.keinavi.servlets.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.interfaces.SpecialAppliMenu;
import jp.co.fj.keinavi.servlets.sales.base.BaseSpecialAppliServlet;

/**
 * 
 * 特例成績データ作成申請完了画面のサーブレットです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD202Servlet extends BaseSpecialAppliServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = 6723880342787599797L;

	/**
	 * @see jp.co.fj.keinavi.servlets.sales.base.BaseSpecialAppliServlet#getMenu()
	 */
	protected SpecialAppliMenu getMenu() {
		return SpecialAppliMenu.APPLY;
	}

	/**
	 * JSP表示処理を行います。
	 */
	public void index(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		forward2Jsp(request, response);
	}

}

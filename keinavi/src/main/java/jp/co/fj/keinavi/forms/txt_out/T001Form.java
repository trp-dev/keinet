/*
 * 作成日: 2004/10/12
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.txt_out;

/**
 * @author 奥村ゆかり
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
import jp.co.fj.keinavi.forms.BaseForm;

public class T001Form extends BaseForm {
	
	private String changed;
	private String[] outItem;
	private String packageTxtType;
	/**
	 * @return
	 */
	public String[] getOutItem() {
		return outItem;
	}

	/**
	 * @param strings
	 */
	public void setOutItem(String[] strings) {
		outItem = strings;
	}

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * @return
	 */
	public String getPackageTxtType() {
		return packageTxtType;
	}

	/**
	 * @param string
	 */
	public void setPackageTxtType(String string) {
		packageTxtType = string;
	}

	/**
	 * @return
	 */
	public String getChanged() {
		return changed;
	}

	/**
	 * @param string
	 */
	public void setChanged(String string) {
		changed = string;
	}

}

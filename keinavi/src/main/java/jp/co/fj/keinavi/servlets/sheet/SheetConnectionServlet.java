package jp.co.fj.keinavi.servlets.sheet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.sheet.SheetExecutorBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.sheet.SheetStatus;
import jp.co.fj.keinavi.data.sheet.SweeperSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.servlets.individual.BaseJudgeThread;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.commons.lang.StringUtils;

/**
 *
 * アプレットとの通信を行うサーブレット
 *
 * 2004.7.26	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class SheetConnectionServlet extends DefaultHttpServlet {

    /**
     * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
     * 			javax.servlet.http.HttpServletRequest,
     * 			javax.servlet.http.HttpServletResponse)
     */
    protected void execute(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        // セッション
        final HttpSession session = request.getSession(false);
        // ログイン情報
        final LoginSession login = getLoginSession(request);
        // 判定処理ステータス
        final String judegeStatus = BaseJudgeThread.getSheetStatus(session);

        // 掃除屋さん
        SweeperSession sweeper = (SweeperSession) session.getAttribute(SweeperSession.SESSION_KEY);
        if (sweeper == null) {
            sweeper = new SweeperSession();
            session.setAttribute(SweeperSession.SESSION_KEY, sweeper);
        }

        //		final DataInputStream dis = new DataInputStream(request.getInputStream());
        //		final DataOutputStream dos = new DataOutputStream(response.getOutputStream());

        //		try {
        //			final String input = dis.readUTF();
        final String[] command = getCommand(request);

        //			// 判定中
        //			if (judegeStatus != null && judegeStatus == BaseJudgeThread.STATUS_PROG) {
        //				dos.writeUTF("ONJUDGE");
        //
        //			// 判定エラー
        //			} else if (judegeStatus != null && judegeStatus == BaseJudgeThread.STATUS_ERR) {
        //				dos.writeUTF("NG");

        // 帳票を作成する
        //			} else if ("PREPARE".equals(command[0])) {

        // セッションキーを決める
        final String sessionkey = "SHEET-" + session.getId() + "-" + System.currentTimeMillis();

        // 掃除依頼をしておく
        sweeper.addKey(sessionkey);

        // 帳票出力状況
        SheetStatus status = new SheetStatus();
        session.setAttribute(sessionkey, status);

        // 帳票作成スレッドを起こす
        SheetExecutorBean bean = createSheetExecutorBean(status, command);
        bean.setSession(session);
        bean.setLogin(login);
        bean.setProfile(getProfile(request));
        bean.setExamSession(getExamSession(request));
        bean.setUnivAll(getUnivAll());
        bean.setUnivInfoMap(getUnivInfoMap());
        bean.setSessionkey(sessionkey);
        bean.setDbKey(getDbKey(request));
        bean.setLog(KNLog.getInstance(getRemoteAddr(request), login.getUserID(), login.getSectorSortingCD()));
        bean.setRemoteAddr(getRemoteAddr(request));
        bean.setRemoteHost(request.getRemoteHost());
        try {
            bean.execute();
        } catch (Exception e) {
            throw new ServletException(e);
        }
        //				new Thread(bean).start();
        //
        //				dos.writeUTF("OK," + sessionkey);
        status = (SheetStatus) session.getAttribute(sessionkey);
        //                                dos.writeUTF("OK," + status.getSheetNumber()
        //                                + ","+ status.getAllOutfileName());
        // ディレクトリ
        File dir = new File(getKNTempPath() + File.separator + sessionkey);
        String files[] = status.getAllOutfileName().split(",");
        File file = null;
        if (files.length == 0 || StringUtils.isEmpty(files[0])) {
            response.getWriter().print("NO_DATA");
            return;
        } else if (files.length == 1) {
            response.getWriter().println("SINGLE");

// MOD START 2019/08/16 QQ)K.Hisakawa 共通テスト対応
//            file = new File(dir.getPath() + File.separator + files[0]);
            File tempFile1 = new File(dir.getPath() + File.separator + files[0]);
// 2019/08/27 QQ)nagai 利用者一覧での保存ボタンの障害を修正 UPD START
            //File tempFile2 = new File(dir.getPath() + File.separator + files[0].replace("##", command[3]));
            File tempFile2;
            if(command[3] == null) {
            	tempFile2 = new File(dir.getPath() + File.separator + files[0]);
            }else {

            	tempFile2 = new File(dir.getPath() + File.separator + files[0].replace("##", command[3]));
            }
// 2019/08/27 QQ)nagai 利用者一覧での保存ボタンの障害を修正 UPD END
            tempFile1.renameTo(tempFile2);

            file = tempFile2;
// MOD END   2019/08/16 QQ)K.Hisakawa 共通テスト対応

        } else {
            response.getWriter().println("MULTI");
            file = createZip(dir, files, command[3]);
        }

        // セッションに作成したファイル名を格納
        session.setAttribute("downloadfile", file.getAbsolutePath());
        /*


        // レスポンスヘッダを設定する。
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("content-disposition", String.format("attachment; filename=\"%s\"", file.getName()));
        //                                response.setCharacterEncoding("UTF-8");
        byte buffer[]  = new byte[4096];
        // ファイル内容の出力
        try (OutputStream out = response.getOutputStream(); FileInputStream fin = new FileInputStream(file);) {
            int size;
            while((size = fin.read(buffer))!=-1) {
                out.write(buffer,0, size);
            }

        }

        // ファイル削除
        file.delete();
        // ディレクトリは空なら削除
        if (dir.list().length == 0) {
                dir.delete();
        }
        */
        //			// ファイルを出力する
        //			} else if ("OUTPUT".equals(command[0])) {
        //
        //				BufferedInputStream in = null;
        //				BufferedOutputStream out = null;
        //				try {
        //					// セッションID
        //					String sessionID = command[1];
        //					// ディレクトリ
        //					File dir = new File(getKNTempPath() + File.separator + sessionID);
        //					// ファイル
        //					File file = new File(dir.getPath() + File.separator + command[2]);
        //
        //					in = new BufferedInputStream(new FileInputStream(file));
        //					out = new BufferedOutputStream(response.getOutputStream());
        //
        //					int data;
        //					while ((data = in.read()) != -1) {
        //						out.write(data);
        //					}
        //
        //					out.close();
        //					in.close();
        //
        //					// ファイル削除
        //					file.delete();
        //					// ディレクトリは空なら削除
        //					if (dir.list().length == 0) {
        //						dir.delete();
        //					}
        //				} catch (final Exception e) {
        //					throw createServletException(e);
        //				} finally {
        //					if (out != null) out.close();
        //					if (in != null) in.close();
        //				}
        //
        //			// PINGコマンド（セッション切断防止）
        //			} else if ("PING".equals(command[0])) {
        //
        //				final String sessionKey = command[1];
        //				final SheetStatus status = (SheetStatus) session.getAttribute(sessionKey);
        //
        //				// ログインセッションがなければNG
        //				if (login == null) {
        //					dos.writeUTF("NG,0");
        //				// 出力状況セッションがなければ準備中
        //				} else if (status == null) {
        //					dos.writeUTF("IN_PROCESS,0,0,0");
        //				// あるなら判定する
        //				} else {
        //					// 異常終了
        //					if (status.isFinish() && status.hasError()) {
        //						dos.writeUTF("NG," + status.getErrorCode());
        //						session.removeAttribute(sessionKey);
        //						sweeper.deleteKey(sessionKey);
        //
        //					// 正常終了
        //					} else if (status.isFinish() && !status.hasError()) {
        //						dos.writeUTF("OK," + status.getSheetNumber()
        //								+ ","+ status.getAllOutfileName());
        //						session.removeAttribute(sessionKey);
        //					// 処理中
        //					} else {
        //						dos.writeUTF("IN_PROCESS," + status.getTotal()
        //								+ "," + status.getCurrent()
        //								+ "," + status.getSheetNumber());
        //					}
        //				}
        //			// 不明なコマンド
        //			} else {
        //				throw new IOException("コマンドエラーです。");
        //			}
        //
        //		} finally {
        //			dos.close();
        //			dis.close();
        //		}
    }

    /***
     * リクエストパラメーターをString配列に変換
     * @param request
     * @return
     */
    private String[] getCommand(final HttpServletRequest request) {
        String[] command = new String[13];
        final Map<String, String[]> map = request.getParameterMap();

        command[0] = "PREPARE";
        for (final Iterator ite = map.entrySet().iterator(); ite.hasNext();) {
            final Map.Entry entry = (Map.Entry) ite.next();
            final String[] value = (String[]) entry.getValue();

        }
        if (map.containsKey("printFlag")) {
            command[1] = map.get("printFlag")[0];
        }
        if (map.containsKey("targetYear")) {
            command[2] = map.get("targetYear")[0];
        }
        if (map.containsKey("targetExam")) {
            command[3] = map.get("targetExam")[0];
        }
        if (map.containsKey("pastExam1")) {
            command[4] = map.get("pastExam1")[0];
        }
        if (map.containsKey("pastExam2")) {
            command[5] = map.get("pastExam2")[0];
        }
        if (map.containsKey("backward")) {
            command[6] = map.get("backward")[0];
        }
        if (map.containsKey("targetExamCode1")) {
            command[7] = map.get("targetExamCode1")[0];
        }
        if (map.containsKey("targetExamCode2")) {
            command[8] = map.get("targetExamCode2")[0];
        }
        if (map.containsKey("targetExamCode3")) {
            command[9] = map.get("targetExamCode3")[0];
        }
        if (map.containsKey("targetCandidateRank1")) {
            command[10] = map.get("targetCandidateRank1")[0];
        }
        if (map.containsKey("targetCandidateRank2")) {
            command[11] = map.get("targetCandidateRank2")[0];
        }
        if (map.containsKey("targetCandidateRank3")) {
            command[12] = map.get("targetCandidateRank3")[0];
        }
        if (map.containsKey("button")) {
            command[13] = map.get("button")[0];
        }
        return command;
    }

    /**
     * 一時ディレクトリのパスを取得する
     * @return
     * @throws IOException
     */
    private String getKNTempPath() throws IOException {
        try {
            return KNCommonProperty.getKNTempPath();
        } catch (Exception e) {
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
            //throw new IOException(e.toString());
            throw new IOException(e);
            // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
        }
    }

    /**
     * 帳票作成実行Beanのインスタンスを作る
     *
     * @param status
     * @param command
     * @return
     */
    protected SheetExecutorBean createSheetExecutorBean(final SheetStatus status, final String[] command) {

        return new SheetExecutorBean(status, command);
    }

    /**
     * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#hasAuthority(
     * 			javax.servlet.http.HttpServletRequest)
     */
    protected boolean hasAuthority(final HttpServletRequest request) throws ServletException {
        return true;
    }

    /**
     * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#accessLog(
     * 			javax.servlet.http.HttpServletRequest)
     */
    protected void accessLog(final HttpServletRequest request) {
        // readUTFメソッドを呼ぶ前に
        // パラメータを取得するとエラーとなるため
        // スーパークラスでログは出力しない
    }

    /***
     *
     * @param files 圧縮したいファイル配列
     * @return
     */
    private File createZip(File dir, String[] files, String targetExam) {
// 2019/08/27 QQ)nagai 利用者一覧での保存ボタンの障害を修正 UPD START
//// MOD START 2019/08/16 QQ)K.Hisakawa 共通テスト対応
////        String zipName = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + ".zip";
//// MOD END   2019/08/16 QQ)K.Hisakawa 共通テスト対応
//    	String zipName = "一括_" + targetExam + "_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".zip";
    	String zipName;

    	if(targetExam == null) {
    		zipName = "一括_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".zip";

    	}
    	else {
    		zipName = "一括_" + targetExam + "_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".zip";
    	}
// 2019/08/27 QQ)nagai 利用者一覧での保存ボタンの障害を修正 UPD END
        File zipFile = new File(dir.getPath() + File.separator + zipName);
// UPD START 2019/08/20 Hics)Ueta 共通テスト対応
//        try (ZipOutputStream zos =
//                new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile)));) {
          try (ZipOutputStream zos =
                  new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile)), Charset.forName("MS932"));) {
// UPD END   2019/08/20 Hics)Ueta 共通テスト対応
              createZip(dir, zos, files, targetExam);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return zipFile;
    }

    private void createZip(File dir, ZipOutputStream zos, String[] files, String targetExam) throws IOException {
        byte[] buf = new byte[1024];
// 2019/08/27 QQ)nagai 利用者一覧での保存ボタンの障害を修正 UPD START
        int i = 0;
// 2019/08/27 QQ)nagai 利用者一覧での保存ボタンの障害を修正 UPD END
        for (String file : files) {

            File f = new File(dir.getPath() + File.separator + file);
// 2019/08/27 QQ)nagai 利用者一覧での保存ボタンの障害を修正 UPD START
            //File f2 = new File(dir.getPath() + File.separator + file.replace("##", targetExam));
            File f2;
            if(targetExam == null) {
            	f2 = new File(dir.getPath() + File.separator + files[i]);
            }else {

            	f2 = new File(dir.getPath() + File.separator + files[i].replace("##",targetExam));
            }
// 2019/08/27 QQ)nagai 利用者一覧での保存ボタンの障害を修正 UPD END
            f.renameTo(f2);

            ZipEntry entry = new ZipEntry(f2.getName());
            zos.putNextEntry(entry);

            try (InputStream is = new BufferedInputStream(new FileInputStream(f2));) {
                int len = 0;
                while ((len = is.read(buf, 0, buf.length)) != -1) {
                    zos.write(buf, 0, len);
                }
            }
// 2019/08/27 QQ)nagai 利用者一覧での保存ボタンの障害を修正 UPD START
            i++;
// 2019/08/27 QQ)nagai 利用者一覧での保存ボタンの障害を修正 UPD END
        }
    }
}

/*
 * 作成日: 2004/11/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.sheet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * 帳票出力状況を保持するクラス
 * 
 * @author Yoshimoto KAWAI - Totec
 */
public class SheetStatus implements Serializable {

	private ArrayList outfileList = new ArrayList(); // 出力Excelファイル名のリスト
	private ArrayList tempfileList = new ArrayList(); // 出力Excelファイル名のリスト（データ帳票？）

	private int total; // 全フェーズ数
	private int current; // 現在フェーズ
	private int errorCode = -1; // エラーコード
	private boolean finish; // 処理が終わったかどうか
	private int sheetNumber = 0; // 印刷枚数

	/**
	 * 処理がひとつ終わったら呼ぶ
	 */
	public void countUp() {
		this.current++;
	}

	/**
	 * 出力Excelファイル名をカンマで結合して返す
	 * @return
	 */
	public String getAllOutfileName() {
		StringBuffer buff = new StringBuffer(); // バッファ

		// outfileList
		{
			Iterator ite = this.outfileList.iterator();
			while (ite.hasNext()) {
				String s = (String) ite.next();

				if (buff.length() != 0) buff.append(",");
				
				// セパレータ
				char separator = '/';
				// スラッシュが無ければ\
				if (s.indexOf('/') < 0) separator = '\\';
				
				buff.append(s.substring(s.lastIndexOf(separator) + 1, s.length()));
			}
		}

		// tempfileList
		{
			// outfileListとのセパレータを入れておく
			if (tempfileList.size() > 0) buff.append(",/");

			Iterator ite = this.tempfileList.iterator();
			while (ite.hasNext()) {
				String s = (String) ite.next();
				buff.append(","); 
				buff.append(s.substring(s.lastIndexOf('/') + 1, s.length()));
			}			
		}

		return buff.toString();	
	}

	/**
	 * @return
	 */
	public ArrayList getOutfileList() {
		return outfileList;
	}

	/**
	 * @return
	 */
	public ArrayList getTempfileList() {
		return tempfileList;
	}

	/**
	 * @param list
	 */
	public void setOutfileList(ArrayList list) {
		outfileList = list;
	}

	/**
	 * @param list
	 */
	public void setTempfileList(ArrayList list) {
		tempfileList = list;
	}

	/**
	 * @return
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @return
	 */
	public boolean isFinish() {
		return finish;
	}

	/**
	 * @param b
	 */
	public void setFinish(boolean b) {
		finish = b;
	}

	/**
	 * @param i
	 */
	public void setTotal(int i) {
		total = i;
	}

	/**
	 * @param b
	 */
	public void setErrorCode(int pErrorCode) {
		this.errorCode = pErrorCode;
	}

	/**
	 * @return
	 */
	public boolean hasError() {
		return errorCode > 0;
	}

	/**
	 * @return
	 */
	public int getCurrent() {
		return current;
	}

	/**
	 * @return errorCode を戻します。
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * @return sheetNumber を戻します。
	 */
	public int getSheetNumber() {
		return sheetNumber;
	}

	/**
	 * @param pSheetNumber 設定する sheetNumber。
	 */
	public void setSheetNumber(int pSheetNumber) {
		sheetNumber = pSheetNumber;
	}

}

package jp.co.fj.keinavi.beans.sheet.school.data;

import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.common.ISheetData;

/**
 *
 * S42_15データクラス
 * 
 * 2006.08.31	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class S42_15Data implements ISheetData {

	// 一括コード
	private String bundleCd;
	// 学校名
	private String bundleName;
	// 模試年度
	private String examYear;
	// 模試コード
	private String examCd;
	// 模試名
	private String examName;
	// 実施日
	private String inpleDate;
	// 過年度の表示
	private int dispPast;
	// 高校の表示順序
	private int dispSchoolOrder;
	// セキュリティスタンプ
	private int securityStamp;
	// 科目リスト
	private final List subjectListData = new ArrayList();
	
	/**
	 * @param data 追加する科目リストデータ
	 */
	public void addSubjectListData(final S42_15SubjectListData data) {
		data.setData(this);
		subjectListData.add(data);
	}
	
	/**
	 * @return 科目リストデータのインスタンス
	 */
	public S42_15SubjectListData createSubjectListData() {
		return new S42_15SubjectListData();
	}

	/**
	 * @return センター到達指標リストデータのインスタンス
	 */
	public S42_15ReachLevelListData createReachLevelListData() {
		return new S42_15ReachLevelListData();
	}

	/**
	 * @return 高校リストデータのインスタンス
	 */
	public S42_15BundleListData createBundleListData() {
		return new S42_15BundleListData();
	}
	
	/**
	 * @return スコア帯リストデータのインスタンス
	 */
	public S42_15ScoreZoneListData createScoreZoneListData() {
		return new S42_15ScoreZoneListData();
	}
	
	/**
	 * @return S42_15SubjectListData を戻します。
	 */
	public S42_15SubjectListData getSubjectListData(int index) {
		return (S42_15SubjectListData) subjectListData.get(index);
	}
	
	// 以下は自動生成 --------------------------------------------------
	
	/**
	 * @return bundleCd を戻します。
	 */
	public String getBundleCd() {
		return bundleCd;
	}
	/**
	 * @param pBundleCd 設定する bundleCd。
	 */
	public void setBundleCd(String pBundleCd) {
		bundleCd = pBundleCd;
	}
	/**
	 * @return bundleName を戻します。
	 */
	public String getBundleName() {
		return bundleName;
	}
	/**
	 * @param pBundleName 設定する bundleName。
	 */
	public void setBundleName(String pBundleName) {
		bundleName = pBundleName;
	}
	/**
	 * @return dispPast を戻します。
	 */
	public int getDispPast() {
		return dispPast;
	}
	/**
	 * @param pDispPast 設定する dispPast。
	 */
	public void setDispPast(int pDispPast) {
		dispPast = pDispPast;
	}
	/**
	 * @return dispSchoolOrder を戻します。
	 */
	public int getDispSchoolOrder() {
		return dispSchoolOrder;
	}
	/**
	 * @param pDispSchoolOrder 設定する dispSchoolOrder。
	 */
	public void setDispSchoolOrder(int pDispSchoolOrder) {
		dispSchoolOrder = pDispSchoolOrder;
	}
	/**
	 * @return examCd を戻します。
	 */
	public String getExamCd() {
		return examCd;
	}
	/**
	 * @param pExamCd 設定する examCd。
	 */
	public void setExamCd(String pExamCd) {
		examCd = pExamCd;
	}
	/**
	 * @return examName を戻します。
	 */
	public String getExamName() {
		return examName;
	}
	/**
	 * @param pExamName 設定する examName。
	 */
	public void setExamName(String pExamName) {
		examName = pExamName;
	}
	/**
	 * @return examYear を戻します。
	 */
	public String getExamYear() {
		return examYear;
	}
	/**
	 * @param pExamYear 設定する examYear。
	 */
	public void setExamYear(String pExamYear) {
		examYear = pExamYear;
	}
	/**
	 * @return inpleDate を戻します。
	 */
	public String getInpleDate() {
		return inpleDate;
	}
	/**
	 * @param pInpleDate 設定する inpleDate。
	 */
	public void setInpleDate(String pInpleDate) {
		inpleDate = pInpleDate;
	}
	/**
	 * @return securityStamp を戻します。
	 */
	public int getSecurityStamp() {
		return securityStamp;
	}
	/**
	 * @param pSecurityStamp 設定する securityStamp。
	 */
	public void setSecurityStamp(int pSecurityStamp) {
		securityStamp = pSecurityStamp;
	}
	/**
	 * @return subjectListData を戻します。
	 */
	public List getSubjectListData() {
		return subjectListData;
	}
	
}

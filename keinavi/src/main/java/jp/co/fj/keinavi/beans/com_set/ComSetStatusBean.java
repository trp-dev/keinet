/*
 * 作成日: 2004/07/30
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.com_set;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import jp.co.fj.keinavi.beans.exam.ExamFilterManager;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.com_set.ComClassData;
import jp.co.fj.keinavi.data.com_set.ComSubjectData;
import jp.co.fj.keinavi.data.com_set.CompClassData;
import jp.co.fj.keinavi.data.com_set.SubjectData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 分析メニュートップの
 * 共通項目設定ボタンの状態を保持するBean
 *
 * 2005.05.29	Yoshimoto KAWAI - Totec
 * 				有効な比較対象年度のみを評価するように変更
 * 				それに伴ってログインセッションと模試セッションを必須メンバとした
 *
 *
 * @author kawai
 *
 */
public class ComSetStatusBean extends DefaultBean {

	private Map statusMap = new HashMap(); // 結果を入れておくマップ

	private Profile profile; // プロファイル
	private ExamData exam; // 模試データ

	private boolean sales; // 営業かどうか

	// 動作モード
	// 1 ... 校内成績分析
	// 2 ... クラス成績分析
	// 4 ... テキスト出力
	private int mode;

	private boolean parseType; // 型
	private boolean parseCourse; // 科目
	private boolean parseYear; // 比較対象年度
	private boolean parseUniv; // 志望大学
	private boolean parseClass; // 比較対象クラス
	private boolean parseSchool; // 比較対象高校

	private LoginSession login;
	private ExamSession examSession;

	/**
	 * コンストラクタ
	 */
	public ComSetStatusBean() {
		statusMap.put("type", "00");
		statusMap.put("course", "00");
		statusMap.put("year", "00");
		statusMap.put("univ", "00");
		statusMap.put("class", "00");
		statusMap.put("school", "00");
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// 模試データがなければ評価しない
		if (exam == null) return;

		// アイテムマップを取得する
		Map item = profile.getItemMap("010000");

		// クラス成績分析
		if (mode == 2) {
			// 型（一括出力対象かつ共通項目設定利用のもの）
			setOutputType("030101");
			setOutputType("030201");
			setOutputType("030202");

			// クラス成績概況（個人成績推移）は型・科目別比較対象であるべき
			if (!parseType && isOutputSubject(IProfileCategory.C_COND_TRANS, IProfileItem.TYPE_USAGE)) {
				Short value = (Short) profile
					.getItemMap(IProfileCategory.C_COND_TRANS)
					.get(IProfileItem.IND_COMP);

				parseType = new Short("11").equals(value) || new Short("12").equals(value);
			}

			// 科目（一括出力対象かつ共通項目設定利用のもの）
			setOutputCourse("030101");
			setOutputCourse("030104");
			setOutputCourse("030201");
			setOutputCourse("030202");
			setOutputCourse("030203");

			// クラス成績概況（個人成績推移）は型・科目別比較対象であるべき
			if (!parseCourse && isOutputSubject(IProfileCategory.C_COND_TRANS, IProfileItem.COURSE_USAGE)) {
				Short value = (Short) profile
					.getItemMap(IProfileCategory.C_COND_TRANS)
					.get(IProfileItem.IND_COMP);

				parseCourse = new Short("11").equals(value) || new Short("12").equals(value);
			}

			// 志望大学（一括出力対象のもの）
			setOutputUniv("030105");
			setOutputUniv("030204");

			// 比較対象クラス（一括出力対象のもの）
			setOutputClass("030201");
			setOutputClass("030202");
			setOutputClass("030203");
			setOutputClass("030204");

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD START
//			// 共通テスト対応の新しいチェックボックスが、読み込まれたプロファイルに無い場合追加する。
//
//			// クラス比較 偏差値分布
//			if (!profile.getItemMap(IProfileCategory.C_COMP_DEV).containsKey(IProfileItem.OPTION_CHECK_BOX)) {
//				profile.getItemMap(IProfileCategory.C_COMP_DEV).put(IProfileItem.OPTION_CHECK_BOX, Short.parseShort("1"));
//			}
//// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		// 校内成績分析
		} else if (mode == 1){
			// 型（一括出力対象かつ共通項目設定利用のもの）
			setOutputType("020202");
			setOutputType("020302");
			setOutputType("020401");
			setOutputType("020402");
			setOutputType("020501");
			setOutputType("020502");
			setOutputType("020601");
			setOutputType("020602");
			// 科目（一括出力対象かつ共通項目設定利用のもの）
			setOutputCourse("020202");
			setOutputCourse("020203");
			setOutputCourse("020302");
			setOutputCourse("020401");
			setOutputCourse("020402");
			setOutputCourse("020403");
			setOutputCourse("020501");
			setOutputCourse("020502");
			setOutputCourse("020503");
			setOutputCourse("020601");
			setOutputCourse("020602");
			// 志望大学（一括出力対象のもの）
			setOutputUniv("020303");
			setOutputUniv("020404");
			setOutputUniv("020504");
			// 比較対象クラス（一括出力対象のもの）
			setOutputClass("020401");
			setOutputClass("020402");
			setOutputClass("020403");
			setOutputClass("020404");
			// 比較対象年度
			setOutputYear("020301");
			setOutputYear("020302");
			setOutputYear("020303");
			// 比較対象高校
			setOutputSchool("020501");
			setOutputSchool("020502");
			setOutputSchool("020503");
			setOutputSchool("020504");
			if (this.sales) setOutputSchool("020601");

// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD START
			// 共通テスト対応の新しいチェックボックスが、読み込まれたプロファイルに無い場合追加する。

			// 高校成績分析
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//			// 校内成績 偏差値分布
//			if (!profile.getItemMap(IProfileCategory.S_SCHOOL_DEV).containsKey(IProfileItem.OPTION_CHECK_BOX)) {
//				profile.getItemMap(IProfileCategory.S_SCHOOL_DEV).put(IProfileItem.OPTION_CHECK_BOX, Short.parseShort("1"));
//			}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
			// 校内成績 設問別成績
			if (!profile.getItemMap(IProfileCategory.S_SCHOOL_QUE).containsKey(IProfileItem.OPTION_CHECK_KOKUGO)) {
				profile.getItemMap(IProfileCategory.S_SCHOOL_QUE).put(IProfileItem.OPTION_CHECK_KOKUGO, Short.parseShort("0"));
			}
			if (!profile.getItemMap(IProfileCategory.S_SCHOOL_QUE).containsKey(IProfileItem.OPTION_CHECK_STATUS)) {
				profile.getItemMap(IProfileCategory.S_SCHOOL_QUE).put(IProfileItem.OPTION_CHECK_STATUS, Short.parseShort("0"));
			}
			if (!profile.getItemMap(IProfileCategory.S_SCHOOL_QUE).containsKey(IProfileItem.OPTION_CHECK_RECORD)) {
				profile.getItemMap(IProfileCategory.S_SCHOOL_QUE).put(IProfileItem.OPTION_CHECK_RECORD, Short.parseShort("0"));
			}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//			// クラス比較 偏差値分布
//			if (!profile.getItemMap(IProfileCategory.S_CLASS_DEV).containsKey(IProfileItem.OPTION_CHECK_BOX)) {
//				profile.getItemMap(IProfileCategory.S_CLASS_DEV).put(IProfileItem.OPTION_CHECK_BOX, Short.parseShort("1"));
//			}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
			// クラス比較 設問別成績
			if (!profile.getItemMap(IProfileCategory.S_CLASS_QUE).containsKey(IProfileItem.OPTION_CHECK_KOKUGO)) {
				profile.getItemMap(IProfileCategory.S_CLASS_QUE).put(IProfileItem.OPTION_CHECK_KOKUGO, Short.parseShort("2"));
			}
			if (!profile.getItemMap(IProfileCategory.S_CLASS_QUE).containsKey(IProfileItem.OPTION_CHECK_STATUS)) {
				profile.getItemMap(IProfileCategory.S_CLASS_QUE).put(IProfileItem.OPTION_CHECK_STATUS, Short.parseShort("2"));
			}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//			// 過年度比較 偏差値分布
//			if (!profile.getItemMap(IProfileCategory.S_PREV_DEV).containsKey(IProfileItem.OPTION_CHECK_BOX)) {
//				profile.getItemMap(IProfileCategory.S_PREV_DEV).put(IProfileItem.OPTION_CHECK_BOX, Short.parseShort("1"));
//			}
//			// 過回比較 偏差値分布
//			if (!profile.getItemMap(IProfileCategory.S_PAST_DEV).containsKey(IProfileItem.OPTION_CHECK_BOX)) {
//				profile.getItemMap(IProfileCategory.S_PAST_DEV).put(IProfileItem.OPTION_CHECK_BOX, Short.parseShort("1"));
//			}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//			// 高校成績分析
//			// 高校間比較 偏差値分布
//			if (!profile.getItemMap(IProfileCategory.S_OTHER_DEV).containsKey(IProfileItem.OPTION_CHECK_BOX)) {
//				profile.getItemMap(IProfileCategory.S_OTHER_DEV).put(IProfileItem.OPTION_CHECK_BOX, Short.parseShort("1"));
//			}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD END

		// テキスト出力
		} else if (mode == 4) {
			// 比較対象高校
			// 一括出力対象 && 他校比較オプション有効 && センターリサーチ以外
			if (new Short("1").equals(profile.getItemMap("050201").get("0403"))
					&& !KNUtil.isCenterResearch(exam)) {
				setOutputSchool("050201");
			}
			if (new Short("1").equals(profile.getItemMap("050202").get("0403"))
					&& !KNUtil.isCenterResearch(exam)) {
				setOutputSchool("050202");
			}
			if (new Short("1").equals(profile.getItemMap("050203").get("0403"))
					&& !KNUtil.isCenterResearch(exam)) {
				setOutputSchool("050203");
			}
			if (new Short("1").equals(profile.getItemMap("050204").get("0403"))
					&& !KNUtil.isCenterResearch(exam)) {
				setOutputSchool("050204");
			}

// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD START
			// 共通テスト対応の新しいチェックボックスが、読み込まれたプロファイルに無い場合追加する。

			//（記述系模試）学力要素別成績データ
			if (!profile.getItemMap(IProfileCategory.T_IND_DESCEXAM).containsKey(IProfileItem.TEXT_OUT_TARGET)) {
				profile.getItemMap(IProfileCategory.T_IND_DESCEXAM).put(IProfileItem.TEXT_OUT_TARGET, Short.parseShort("1"));
				profile.getItemMap(IProfileCategory.T_IND_DESCEXAM).put(IProfileItem.TEXT_OUT_ITEM, getOutTargetValue(40));
			}

			// 2020/03/04 QQ)Ooseto 共通テスト対応(不具合修正) UPD START
			// プロファイルの出力項目に項目番号248以上のものがある場合、出力項目を再設定する
			// 模試別成績データ（新形式）
			//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			//try {
			StringTokenizer stn = new StringTokenizer(profile.getItemMap(IProfileCategory.T_IND_EXAM2).get(IProfileItem.TEXT_OUT_ITEM).toString(), ",");
                        String textOutItem = "";
			while (stn.hasMoreTokens()) {
			    int value = new Integer(stn.nextToken());
			    if (value <= 247) {
			        textOutItem += String.valueOf(value);
			        textOutItem += ",";
			    }
			}
			textOutItem = textOutItem.substring(0, textOutItem.length() - 1);
			profile.getItemMap(IProfileCategory.T_IND_EXAM2).put(IProfileItem.TEXT_OUT_ITEM, textOutItem);
			    //if (profile.getUpdateDate().before(dateFormat.parse("2020/03/06"))) {
			    //    profile.getItemMap(IProfileCategory.T_IND_EXAM2).put(IProfileItem.TEXT_OUT_TARGET, Short.parseShort("1"));
			    //    profile.getItemMap(IProfileCategory.T_IND_EXAM2).put(IProfileItem.TEXT_OUT_ITEM, getOutTargetValue(247));
			    //}
		        //} catch ( ParseException e ) {
		        //    ;
		        //}
			// 2020/03/04 QQ)Ooseto 共通テスト対応(不具合修正) UPD END
// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD END

		// モード不正
		} else {
			throw new IllegalArgumentException("動作モードの指定が不正です。");
		}

		// 営業は比較対象年度と比較対象クラスはない
		if (sales) {
			parseYear = false;
			parseClass = false;
		}

		// 型
		if (parseType) {
			// 個別選択の場合のみチェック
			if (new Short("2").equals(item.get("0104"))) {
				statusMap.put("type", getSubjectSituation((List)item.get("0101")));
			}
		}

		// 科目
		if (parseCourse) {
			// 個別選択の場合のみチェック
			if (new Short("2").equals(item.get("0204"))) {
				statusMap.put("course", getSubjectSituation((List)item.get("0201")));
			}
		}

		// 比較対象年度
		if (parseYear) {
			//String[] years = (String[]) item.get(IProfileItem.PREV_COMP_YEAR);
			String[] years = ProfileUtil.compYearFilter((String[]) item
					.get(IProfileItem.PREV_COMP_YEAR), exam);

			// 過年度比較可能なら設定は必要
			if (years.length == 0
					&& ExamFilterManager.execute("prev", login, examSession, exam)) {
				statusMap.put("year", "02");

			// 受験人数が0人のものがないか調べる
			} else if (years.length > 0) {

				// 模試コード変換テーブルのセットアップ
				KNUtil.buildExamCdTransTable(conn, new ExamData[]{exam});

				PreparedStatement ps = null;
				ResultSet rs = null;
				try {
					ps = conn.prepareStatement(
							QueryLoader.getInstance().getQuery("cm10"));
					ps.setString(1, profile.getBundleCD());
					ps.setString(3, exam.getExamCD());

					for (int i=0; i<years.length; i++) {
						ps.setString(
							2,
							String.valueOf(
								Integer.parseInt(exam.getExamYear())
									- Integer.parseInt(years[i])
							)
						);

						rs = ps.executeQuery();
						if (!rs.next()) {
							statusMap.put("year", "01");
							break;
						}
						rs.close();
					}
				} finally {
					DbUtils.closeQuietly(rs);
					DbUtils.closeQuietly(ps);
				}
			}
		}

		// 志望大学
		if (parseUniv) {
			// 個別選択の場合のみチェック
			if (new Short("2").equals(item.get("0307"))) {

				String[] univ = (String[])item.get("0301");

				// 大学がなければ駄目
				if (univ == null || univ.length == 0) {
					statusMap.put("univ", "02");

				// 受験人数が0人のものがないか調べる
				} else {
					statusMap.put("univ", "00");

					PreparedStatement ps = null;
					ResultSet rs = null;
					try {
						StringBuffer query = new StringBuffer();

						// 営業
						if (this.sales) {
							query.append("SELECT totalcandidatenum ");
							query.append("FROM ratingnumber_a ");
							query.append("WHERE examyear = ? AND examcd = ? ");
							query.append("AND ratingdiv = ? AND univcountingdiv = '0' ");
							query.append("AND univcd = ? AND studentdiv = '0'");

							ps = conn.prepareStatement(query.toString());
							ps.setString(1, exam.getExamYear());
							ps.setString(2, exam.getExamCD());
							ps.setString(3, KNUtil.getRatingDiv(exam));

						// それ以外
						} else {
							query.append("SELECT totalcandidatenum ");
							query.append("FROM ratingnumber_s ");
							query.append("WHERE examyear = ? AND examcd = ? ");
							query.append("AND bundlecd = ? AND univcd = ? ");
							query.append("AND univcountingdiv = '0' AND ratingdiv = ? ");
							query.append("AND studentdiv = '0'");

							ps = conn.prepareStatement(query.toString());
							ps.setString(1, exam.getExamYear());
							ps.setString(2, exam.getExamCD());
							ps.setString(3, profile.getBundleCD());
							ps.setString(5, KNUtil.getRatingDiv(exam));
						}

						for (int i=0; i < univ.length; i++) {
							ps.setString(4, univ[i]);

							rs = ps.executeQuery();
							if (!rs.next()) {
								statusMap.put("univ", "01");
								break;
							}
							rs.close();
						}
					} finally {
						DbUtils.closeQuietly(rs);
						DbUtils.closeQuietly(ps);
					}
				}
			}
		}

		// 比較対象クラス
		if (parseClass) {
			// 個別選択の場合のみチェック
			if (new Short("2").equals(item.get("0505"))) {
				// 設定値を取得する
				List container = (List)item.get("0504");
				int index = container.indexOf(
					new CompClassData(exam.getExamYear(), exam.getExamCD()));

				// ある場合
				if (index >= 0) {
					CompClassData data = (CompClassData) container.get(index);

					// 個別選択が空なら駄目
					if (data.getIClassData().size() == 0) {
						statusMap.put("class", "02");

					// 受験人数が0人のものがないか調べる
					} else {
						PreparedStatement ps = null;
						ResultSet rs = null;
						try {
							ps = conn.prepareStatement(QueryLoader.getInstance().load("cm04").toString());
							ps.setString(1, exam.getExamYear());
							ps.setString(2, exam.getExamCD());
							ps.setString(3, profile.getBundleCD());
							ps.setString(6, exam.getExamYear());
							ps.setString(7, exam.getExamCD());
							ps.setString(8, profile.getBundleCD());

							Iterator ite = data.getIClassData().iterator();
							while (ite.hasNext()) {
								ComClassData c = (ComClassData) ite.next();

								ps.setShort(4, c.getGrade());
								ps.setString(5, c.getClassName());
								ps.setString(9, c.getClassName());

								rs = ps.executeQuery();
								if (rs.next() && rs.getInt(1) == 0) {
									statusMap.put("class", "01");
									break;
								}
								rs.close();
							}
						} finally {
							DbUtils.closeQuietly(rs);
							DbUtils.closeQuietly(ps);
						}
					}
				// ない場合
				} else {
					statusMap.put("class", "02");
				}
			}
		}

		// 比較対象高校
		if (parseSchool) {
			// 設定値を取得する
			String[] value = ProfileUtil.getCompSchoolValue(profile, false);

			// 設定値がなければ駄目
			if (value == null || value.length == 0) {
				statusMap.put("school", "02");

			// 受験人数が0人のものがないか調べる
			} else {
				// 全国
				StringBuffer query1 = new StringBuffer();
				query1.append("SELECT ra.numbers ");
				query1.append("FROM examtakenum_a ra ");
				query1.append("WHERE ra.examyear = ? AND ra.examcd = ? ");

				// 県
				StringBuffer query2 = new StringBuffer();
				query2.append("SELECT rp.numbers ");
				query2.append("FROM examtakenum_p rp ");
				query2.append("WHERE rp.examyear = ? AND rp.examcd = ? ");
				query2.append("AND rp.prefcd = ?");

				// 高校
				StringBuffer query3 = new StringBuffer();
				query3.append("SELECT rs.numbers ");
				query3.append("FROM examtakenum_s rs ");
				query3.append("WHERE rs.examyear = ? AND rs.examcd = ? ");
				query3.append("AND rs.bundlecd = ?");

				PreparedStatement ps1 = null;
				PreparedStatement ps2 = null;
				PreparedStatement ps3 = null;
				ResultSet rs = null;
				try {
					ps1 = conn.prepareStatement(query1.toString());
					ps1.setString(1, exam.getExamYear());
					ps1.setString(2, exam.getExamCD());

					ps2 = conn.prepareStatement(query2.toString());
					ps2.setString(1, exam.getExamYear());
					ps2.setString(2, exam.getExamCD());

					ps3 = conn.prepareStatement(query3.toString());
					ps3.setString(1, exam.getExamYear());
					ps3.setString(2, exam.getExamCD());

					for (int i=0; i<value.length; i++) {
						// 全国
						if ("99999".equals(value[i])) {
							rs = ps1.executeQuery();

						// 県
						} else if ("999".equals(value[i].substring(2, 5))) {
							ps2.setString(3, KNUtil.bundleCD2PrefCD(value[i])); // 県コード
							rs = ps2.executeQuery();

						// 高校
						} else {
							ps3.setString(3, value[i]); // 一括コード
							rs = ps3.executeQuery();
						}

						if (!rs.next()) {
							statusMap.put("school", "01");
							break;
						}
						rs.close();
					}
				} finally {
					DbUtils.closeQuietly(rs);
					DbUtils.closeQuietly(ps1);
					DbUtils.closeQuietly(ps2);
					DbUtils.closeQuietly(ps3);
				}
			}
		}
	}

	/**
	 * @param container
	 * @return
	 * @throws Exception
	 */
	private String getSubjectSituation(List container) throws Exception {
		// 設定値のindex
		int index = container.indexOf(
			new ComSubjectData(exam.getExamYear(), exam.getExamCD()));

		// ある場合
		if (index >= 0) {
			ComSubjectData data = (ComSubjectData)container.get(index);

			// 個別選択が空なら駄目
			if (data.getISubjectData().size() == 0) {
				return "02";

			// 受験人数が0人のものがないか調べる
			} else {
				PreparedStatement ps = null;
				ResultSet rs = null;
				try {
					StringBuffer query = new StringBuffer();

					// 営業
					if (this.sales) {
						query.append("SELECT numbers ");
						query.append("FROM subrecord_a ");
						query.append("WHERE examyear = ? AND examcd = ? ");
						query.append("AND subcd = ?");

						ps = conn.prepareStatement(query.toString());
						ps.setString(1, exam.getExamYear());
						ps.setString(2, exam.getExamCD());

					// それ以外
					} else {
						query.append("SELECT numbers ");
						query.append("FROM subrecord_s ");
						query.append("WHERE examyear = ? AND examcd = ? ");
						query.append("AND subcd = ? AND bundlecd = ? ");

						ps = conn.prepareStatement(query.toString());
						ps.setString(1, exam.getExamYear());
						ps.setString(2, exam.getExamCD());
						ps.setString(4, profile.getBundleCD());
					}


					Iterator ite = data.getISubjectData().iterator();
					while (ite.hasNext()) {
						SubjectData s = (SubjectData) ite.next();

						ps.setString(3, s.getSubjectCD());

						rs = ps.executeQuery();
						if (!rs.next()) {
							return "01";
						}
						rs.close();
					}

				} finally {
					DbUtils.closeQuietly(rs);
					DbUtils.closeQuietly(ps);
				}

				return "00";
			}

		// ない場合
		} else {
			return "02";
		}
	}

	/**
	 * 指定されたカテゴリにおいて一括出力対象かつ共通項目設定利用かどうか
	 * @param category
	 * @param item
	 * @return
	 * @throws Exception
	 */
	private boolean isOutputSubject(String category, String item) throws Exception {
		return new Short("1").equals(profile.getItemMap(category).get("1301"))
				&& new Short("1").equals(profile.getItemMap(category).get(item));
	}

	/**
	 * 指定されたカテゴリで型が評価対象かどうかをセットする
	 * @param category
	 * @param item
	 * @return
	 * @throws Exception
	 */
	private void setOutputType(String category) throws Exception {
		if (parseType) return;
		parseType= isOutputSubject(category, "0103");
	}

	/**
	 * 指定されたカテゴリで科目が評価対象かどうかをセットする
	 * @param category
	 * @param item
	 * @return
	 * @throws Exception
	 */
	private void setOutputCourse(String category) throws Exception {
		if (parseCourse) return;
		parseCourse = isOutputSubject(category, "0203");
	}

	/**
	 * 指定されたカテゴリにおいて一括出力対象かどうか
	 * @param category
	 * @param item
	 * @return
	 * @throws Exception
	 */
	private boolean isOutput(String category) throws Exception {
		return new Short("1").equals(profile.getItemMap(category).get("1301"));
	}

	/**
	 * 指定されたカテゴリで志望大学が評価対象かどうかをセットする
	 * @param category
	 * @param item
	 * @return
	 * @throws Exception
	 */
	private void setOutputUniv(String category) throws Exception {
		if (parseUniv) return;
		parseUniv = isOutput(category);
	}

	/**
	 * 指定されたカテゴリで比較対象クラスが評価対象かどうかをセットする
	 * @param category
	 * @param item
	 * @return
	 * @throws Exception
	 */
	private void setOutputClass(String category) throws Exception {
		if (parseClass) return;
		parseClass = isOutput(category);
	}

	/**
	 * 指定されたカテゴリで比較対象高校が評価対象かどうかをセットする
	 * @param category
	 * @param item
	 * @return
	 * @throws Exception
	 */
	private void setOutputSchool(String category) throws Exception {
		if (parseSchool) return;
		parseSchool = isOutput(category);
	}

	/**
	 * 指定されたカテゴリで比較対象年度が評価対象かどうかをセットする
	 * @param category
	 * @param item
	 * @return
	 * @throws Exception
	 */
	private void setOutputYear(String category) throws Exception {
		if (parseYear) return;
		parseYear = isOutput(category);
	}

	/**
	 * @return
	 */
	public Map getStatusMap() {
		return statusMap;
	}

	/**
	 * @param data
	 */
	public void setExam(ExamData data) {
		exam = data;
	}

	/**
	 * @param profile
	 */
	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	/**
	 * @param i
	 */
	public void setMode(int i) {
		mode = i;
	}

	/**
	 * @param b
	 */
	public void setSales(boolean b) {
		sales = b;
	}

	public void setExamSession(ExamSession examSession) {
		this.examSession = examSession;
	}
	public void setLogin(LoginSession login) {
		this.login = login;
	}

// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD START
	//	出力対象のプロファイル初期値を生成
	public static String getOutTargetValue(int size){
		String str ="";
		String index[] = new String[size];
		for(int i=0; i<size; i++){	index[i] = String.valueOf(i+1);	}
		Set outTarget = CollectionUtil.array2Set(index);
		Iterator it = outTarget.iterator();
		int count = 0;
		while(it.hasNext()){
			str += it.next();
			if(count!=size) str+=",";
		}
		return str;
	}
// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD END
}

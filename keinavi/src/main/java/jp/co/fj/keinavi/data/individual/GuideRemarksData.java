/*
 * 作成日: 2004/11/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class GuideRemarksData {

	private String univCd;
	private String facultyCd;
	private String deptCd;
	private String remarks;
	
	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setRemarks(String string) {
		remarks = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

}

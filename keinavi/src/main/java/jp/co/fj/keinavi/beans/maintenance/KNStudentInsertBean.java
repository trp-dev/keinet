package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.beans.admin.name.NameBringsNear;
import jp.co.fj.keinavi.data.maintenance.KNStudentData;
import jp.co.fj.keinavi.util.message.MessageLoader;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 生徒情報登録Bean
 * 
 * 2005.12.06 [新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class KNStudentInsertBean extends DefaultBean {

	/**  生年月日のフォーマッタ */
	public static final DateFormat BIRTHDAY_FORMAT =
			new SimpleDateFormat("yyyyMMdd");
	
	// 学校コード
	private final String schoolCd;
	// 生徒データ
	private KNStudentData student;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 学校コード
	 */
	public KNStudentInsertBean(final String pSchoolCd) {
		
		if (pSchoolCd == null) {
			throw new IllegalArgumentException(
					"学校コードがNULLです。");
		}
		
		this.schoolCd = pSchoolCd;
	}

	/**
	 * @see jp.co.fj.keispro.beans.common.csv.CSVLineRegisterBean#execute()
	 */
	public void execute() throws Exception {
		
		// 名寄せ判定
		final NameBringsNear name = new NameBringsNear();
		name.setConnection(null, conn);
		name.setInputData(createInputData());
		name.execute();
		
		// 進級処理
		if (name.getCombiJudge() == 2) {
			student.setIndividualId((String) name.getResult().get(0));
			
			// 学籍履歴情報のみ登録
			insertHistoryinfo(student.getIndividualId(),
					student.getYear(), student.getGrade(),
					student.getClassName(), student.getClassNo());
			
		// 重複エラー
		} else if (name.getCombiJudge() == 1) {
			throw new KNBeanException("同一人物と思われる生徒が既に登録されています。");
			
		// 新規登録
		} else {
			// 個人IDを割り振る
			student.setIndividualId(createIndividualId(student.getYear()));
			
			// 重複チェック
			if (isDuplication(student.getIndividualId(), 
					student.getYear(), student.getGrade(),
					student.getClassName(), student.getClassNo())) {
				throw new KNBeanException(MessageLoader.getInstance().getMessage("x010a"));
			}
			
			// 学籍基本情報
			insertBasicinfo(student.getIndividualId(), student.getNameKana(),
					student.getNameKanji(), student.getSex(),
					BIRTHDAY_FORMAT.format(student.getBirthday()), student.getTelNo());
			
			// 学籍履歴情報
			insertHistoryinfo(student.getIndividualId(),
					student.getYear(), student.getGrade(),
					student.getClassName(), student.getClassNo());
		}
	}

	private HashMap createInputData() {
		
		final String birthday = BIRTHDAY_FORMAT.format(student.getBirthday());
		
		final HashMap inputData = new HashMap();
		inputData.put("YEAR02", student.getYear().substring(2, 4));
		inputData.put("EXAMYEAR", student.getYear());
		inputData.put("GRADE", String.valueOf(student.getGrade()));
		inputData.put("CLASS", student.getClassName());
		inputData.put("CLASS_NO", student.getClassNo());
		inputData.put("SEX", student.getSex());
		inputData.put("NAME_KANA", student.getNameKana());
		inputData.put("NAME_KANJI", student.getNameKanji());
		inputData.put("BIRTHYEAR", birthday.substring(0, 4));
		inputData.put("BIRTHMONTH", birthday.substring(4, 6));
		inputData.put("BIRTHDATE", birthday.substring(6, 8));
		inputData.put("TEL_NO", student.getTelNo());
		inputData.put("SCHOOLCD", schoolCd);
		inputData.put("HOMESCHOOLCD", schoolCd);
		return inputData;
	}

	/**
	 * 学籍基本情報を登録する
	 * 
	 * @param individualId
	 * @param nameKana
	 * @param nameKanji
	 * @param sex
	 * @param birthday
	 * @param telNo
	 * @throws SQLException
	 */
	public void insertBasicinfo(final String individualId,
			final String nameKana, final String nameKanji,
			final String sex, final String birthday,
			final String telNo) throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(createBasicinfoQuery());
			ps.setString(1, individualId);
			ps.setString(2, schoolCd);
			ps.setString(3, schoolCd);
			ps.setString(4, nameKana);
			ps.setString(5, nameKanji);
			ps.setString(6, sex);
			ps.setString(7, birthday);
			ps.setString(8, telNo);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 学籍履歴情報を登録する
	 * 
	 * @param individualId
	 * @param year
	 * @param grade
	 * @param className
	 * @param classNo
	 * @throws SQLException
	 */
	public void insertHistoryinfo(final String individualId,
			final String year, final int grade,
			final String className, final String classNo) throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					"INSERT INTO " + getHistoryinfoName()
					+ " VALUES(?,?,?,?,?,NULL,NULL,NULL,NULL)");
			ps.setString(1, individualId);
			ps.setString(2, year);
			ps.setInt(3, grade);
			ps.setString(4, className);
			ps.setString(5, classNo);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 個人IDを生成する
	 * 
	 * @throws SQLException
	 */
	public String createIndividualId(final String year) throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					"SELECT TO_CHAR(" + getSequenceName()
					+ ".NEXTVAL, 'FM00000000') FROM DUAL");
			rs = ps.executeQuery();
			
			if (rs.next()) {
				return 	year.substring(2, 4) + rs.getString(1);
			} else {
				throw new SQLException("個人IDの取得に失敗しました。");
			}

		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	/**
	 * 自分以外の同じ年度・学年・クラス・クラス番号の生徒が存在するかどうか
	 * 
	 * @param individualId 個人ID
	 * @param year 年度
	 * @param grade 学年
	 * @param className クラス
	 * @param classNo クラス番号
	 * @return
	 * @throws SQLException
	 */
	public boolean isDuplication(final String individualId, final String year,
			final int grade, final String className,
			final String classNo) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(createDuplicationQuery());
			ps.setString(1, year);
			ps.setInt(2, grade);
			ps.setString(3, className);
			ps.setString(4, classNo);
			ps.setString(5, schoolCd);
			
			// NULL値の演算はFALSEとなるためダミー文字列
			if (individualId.length() == 0) {
				ps.setString(6, "NULL");
			} else {
				ps.setString(6, individualId);
			}
			rs = ps.executeQuery();
			return rs.next();
			
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	protected String createDuplicationQuery() throws SQLException {
		return QueryLoader.getInstance().load("m16").toString();
	}
	
	protected String createBasicinfoQuery() throws SQLException {
		return "INSERT INTO basicinfo VALUES(?,?,?,?,?,?,?,?,NULL)";
	}
	
	protected String getHistoryinfoName() {
		return "historyinfo";
	}
	
	protected String getSequenceName() {
		return "s_idcount";
	}
	
	/**
	 * @param pStudent 設定する student。
	 */
	public void setStudent(final KNStudentData pStudent) {
		this.student = pStudent;
	}

	/**
	 * @return schoolCd を戻します。
	 */
	protected String getSchoolCd() {
		return schoolCd;
	}

	/**
	 * @return student を戻します。
	 */
	protected KNStudentData getStudent() {
		return student;
	}

}

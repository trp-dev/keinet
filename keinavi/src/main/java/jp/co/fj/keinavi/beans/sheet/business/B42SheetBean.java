package jp.co.fj.keinavi.beans.sheet.business;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.sheet.SheetSchoolData;
import jp.co.fj.keinavi.excel.business.B42;
import jp.co.fj.keinavi.excel.data.business.B42BnpListBean;
import jp.co.fj.keinavi.excel.data.business.B42GakkoListBean;
import jp.co.fj.keinavi.excel.data.business.B42Item;
import jp.co.fj.keinavi.excel.data.business.B42ListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 高校成績分析 - 高校間比較 - 偏差値分布
 *
 *
 * 2004.09.08	[新規作成]
 *
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 *
 * 2005.04.21	Yoshimoto KAWAI - Totec
 * 				新テスト対応
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class B42SheetBean extends AbstractSheetBean {

	// データクラス
	private final B42Item data = new B42Item();

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//	// 英語CEFR試験別取得状況 最大出力明細数
//	private final int B42CEFRLIST_MAX_COUNT = 300;
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		final String[] code = super.getSubjectCDArray(); // 型・科目設定値
		final List compSchoolList = super.getCompSchoolList(); // 比較対象高校

		// データを詰める
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntNendoFlg(getIntFlag(IProfileItem.PREV_DISP)); // 過年度表示比較フラグ
		data.setIntSecuFlg(getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(getOutputType()); // 出力種別フラグ

		// 表
		switch (getIntFlag(IProfileItem.CHART_COMP_RATIO)) {
			case  1: data.setIntHyouFlg(0); data.setIntKoseihiFlg(1); break;
			case  2: data.setIntHyouFlg(0); data.setIntKoseihiFlg(2); break;
			case 11: data.setIntHyouFlg(1); data.setIntKoseihiFlg(1); break;
			case 12: data.setIntHyouFlg(1); data.setIntKoseihiFlg(2); break;
		}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/05 QQ)Tanioka 共通テスト対応 ADD START
//		switch (super.getIntFlag(IProfileItem.OPTION_CHECK_BOX)) {
//			case  1: data.setIntCheckBoxFlg(0); data.setIntTargetCheckBoxFlg(1); break;
//			case  2: data.setIntCheckBoxFlg(0); data.setIntTargetCheckBoxFlg(2); break;
//			case 11: data.setIntCheckBoxFlg(1); data.setIntTargetCheckBoxFlg(1); break;
//			case 12: data.setIntCheckBoxFlg(1); data.setIntTargetCheckBoxFlg(2); break;
//		}
//// 2019/09/05 QQ)Tanioka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		// ワークテーブルのセットアップ
		insertIntoSubCDTrans(new ExamData[]{exam});

		// 対象年度
		final String[] years;
		switch (getIntFlag(IProfileItem.PREV_DISP)) {
			// 今年度のみ
			case 1:
				years = new String[] {exam.getExamYear()};
				break;

			// 前年度まで
			case 2:
				years = new String[] {
					exam.getExamYear(),
					String.valueOf(Integer.parseInt(exam.getExamYear()) - 1)
				};
				data.setIntNendoFlg(getExistsExamCount(years));
				break;

			// 前々年度まで
			case 3:
				years = new String[] {
					exam.getExamYear(),
					String.valueOf(Integer.parseInt(exam.getExamYear()) - 1),
					String.valueOf(Integer.parseInt(exam.getExamYear()) - 2)
				};
				data.setIntNendoFlg(getExistsExamCount(years));
				break;

			// パラメータエラー
			default:
				throw new IllegalArgumentException("過年度の表示の設定値が不正です。");
		}

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		PreparedStatement ps6 = null;
		PreparedStatement ps7 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		try {
			List c1 = new LinkedList(); // 型の入れ物
			List c2 = new LinkedList(); // 科目の入れ物

			// データリストの取得
			{
				Query query = QueryLoader.getInstance().load("s22_1");
				query.setStringArray(1, code); // 型・科目コード

				ps1 = conn.prepareStatement(query.toString());
				ps1.setString(1, exam.getExamYear()); // 対象年度
				ps1.setString(2, exam.getExamCD()); // 対象模試
			}

			// 学校データリスト（全国）
			{
				Query query = QueryLoader.getInstance().load("b42_1");
				query.setStringArray(1, years); // 年度

				ps2 = conn.prepareStatement(query.toString());
				ps2.setString(2, getMinStudentDiv()); // 現役高卒区分
				ps2.setString(3, exam.getExamCD()); // 模試コード
			}

			// 学校データリスト（県）
			{
				Query query = QueryLoader.getInstance().load("b42_2");
				query.setStringArray(1, years); // 年度

				ps3 = conn.prepareStatement(query.toString());
				ps3.setString(3, exam.getExamCD()); // 模試コード
			}

			// 学校データリスト（高校）
			{
				Query query = QueryLoader.getInstance().load("b42_3");
				super.outDate2InDate(query); // データ開放日書き換え
				query.setStringArray(1, years); // 年度

				ps4 = conn.prepareStatement(query.toString());
				ps4.setString(3, exam.getExamCD()); // 模試コード
			}

			// 偏差値分布データリスト（全国）
			ps5 = conn.prepareStatement(getQueryWithDeviationZone("b42_4").toString());

			// 偏差値分布データリスト（県）
			ps6 = conn.prepareStatement(getQueryWithDeviationZone("b42_5").toString());

			// 偏差値分布データリスト（高校）
			ps7 = conn.prepareStatement(getQueryWithDeviationZone("b42_6").toString());

			rs1 = ps1.executeQuery();

			while (rs1.next()) {
				B42ListBean bean = new B42ListBean();
				bean.setStrKmkCd(rs1.getString(1));
				bean.setStrKmkmei(rs1.getString(2));
				bean.setStrHaiten(rs1.getString(3));

				if (rs1.getInt(1) >= 7000) c1.add(bean); // 型
				else c2.add(bean); // 科目

				// 型・科目コード
				ps2.setString(1, bean.getStrKmkCd());
				ps3.setString(2, bean.getStrKmkCd());
				ps4.setString(2, bean.getStrKmkCd());

				// 学校分繰り返す
				Iterator ite = compSchoolList.iterator();
				while (ite.hasNext()) {
					SheetSchoolData school = (SheetSchoolData) ite.next();

					switch (school.getSchoolTypeCode()) {
						// 全国
						case 1:
							rs2 = ps2.executeQuery();
							break;

						// 都道府県
						case 2:
							// 県コード
							ps3.setString(1, school.getPrefCD());
							ps6.setString(3, school.getPrefCD());

							rs2 = ps3.executeQuery();
							break;
						// 高校
						case 3:
							// 一括コード
							ps4.setString(1, school.getSchoolCD());
							ps7.setString(3, school.getSchoolCD());

							rs2 = ps4.executeQuery();
							break;
					}

					// 学校データリスト
					while (rs2.next()) {
						B42GakkoListBean g = new B42GakkoListBean();
						g.setStrGakkomei(rs2.getString(1));
						g.setStrNendo(rs2.getString(2));
						g.setIntNinzu(rs2.getInt(3));
						g.setFloHeikin(rs2.getFloat(4));
						g.setFloHensa(rs2.getFloat(5));
						bean.getB42GakkoList().add(g);

						switch (school.getSchoolTypeCode()) {
							// 全国
							case 1:
								ps5.setString(1, rs2.getString(2)); // 模試年度
								ps5.setString(2, rs2.getString(7)); // 模試コード
								ps5.setString(3, rs2.getString(6)); // 型･科目コード
								rs3 = ps5.executeQuery();
								break;

							// 都道府県
							case 2:
								ps6.setString(1, rs2.getString(2)); // 模試年度
								ps6.setString(2, rs2.getString(7)); // 模試コード
								ps6.setString(4, rs2.getString(6)); // 型･科目コード
								rs3 = ps6.executeQuery();
									break;
							// 高校
							case 3:
								ps7.setString(1, rs2.getString(2)); // 模試年度
								ps7.setString(2, rs2.getString(7)); // 模試コード
								ps7.setString(4, rs2.getString(6)); // 型･科目コード
								rs3 = ps7.executeQuery();
								break;
						}

						// 偏差値分布データリスト
						while (rs3.next()) {
							B42BnpListBean b = new B42BnpListBean();
							b.setFloBnpMax(rs3.getFloat(1));
							b.setFloBnpMin(rs3.getFloat(2));
							b.setIntNinzu(rs3.getInt(3));
							b.setFloKoseihi(rs3.getFloat(4));
							g.getB42BnpList().add(b);
						}
						rs3.close();
					}
					rs2.close();
				}
			}

			// 型・科目の順番に詰める
			data.getB42List().addAll(c1);
			data.getB42List().addAll(c2);

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//			// 2019/09/30 QQ) 共通テスト対応 ADD START
//			if (data.getIntCheckBoxFlg() == 1) {
//				//B42_07_英語CEFR試験別取得状況
//				execute07(compSchoolList);
//			}
//			// 2019/09/30 QQ) 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(rs3);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(ps3);
			DbUtils.closeQuietly(ps4);
			DbUtils.closeQuietly(ps5);
			DbUtils.closeQuietly(ps6);
			DbUtils.closeQuietly(ps7);
		}
	}

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//	/**
//	 * B42_07_英語CEFR試験別取得状況 データ取得
//	 * @param compSchoolList       選択学校情報List
//	 * @param prefCdList           選択県コードList
//	 * @param schoolCdList         選択高校コードList
//	 * @throws Exception
//	 */
//	private void execute07(List compSchoolList)  throws Exception {
//
//		PreparedStatement psExam = null;
//		PreparedStatement psAllA = null;
//		PreparedStatement psAllP = null;
//		PreparedStatement psAllS = null;
//		PreparedStatement psEachA = null;
//		PreparedStatement psEachP = null;
//		PreparedStatement psEachS = null;
//		ResultSet rsExam = null;
//		ResultSet rsData = null;
//		try {
//			B42CefrItem cefrItem = new B42CefrItem();
//
//			// 抽出対象を取得
//			ArrayList<String> prefCdList = new ArrayList();
//			ArrayList<String> schoolCdList = new ArrayList();
//			prefCdList.add("Dummy");
//			schoolCdList.add("Dummy");
//			boolean selectedA = false;
//			boolean selectedP = false;
//			boolean selectedS = false;
//
//
//			// 全試験用情報取得
//			{
//				//「全国」が選択されているかどうかにかかわらず、レベル取得のため、全試験・全国情報を取得
//				Query query10 = QueryLoader.getInstance().load("b42_10");	//全試験（全国）
//				psAllA = conn.prepareStatement(query10.toString());
//				psAllA.setString(1, exam.getExamYear());	//年度
//				psAllA.setString(2, exam.getExamCD());
//				rsData = psAllA.executeQuery();
//				while (rsData.next()) {
//					cefrItem.getCefrLevelList().add(rsData.getString(2));
//				}
//				rsData.close();
//
//				//選択試験該当なし
//				if ( cefrItem.getCefrLevelList().size() == 0) {
//					return;
//				}
//
//
//				Query query11 = QueryLoader.getInstance().load("b42_11");	//全試験（県）
//				psAllP = conn.prepareStatement(query11.toString());
//
//				Query query12 = QueryLoader.getInstance().load("b42_12");	//全試験（高校）
//				psAllS = conn.prepareStatement(query12.toString());
//
//
//				int dataCnt = 0;
//				Iterator ite = compSchoolList.iterator();
//				while (ite.hasNext()) {
//					SheetSchoolData school = (SheetSchoolData) ite.next();
//					B42CefrListBean bean = new B42CefrListBean();
//
//					switch (school.getSchoolTypeCode()) {
//
//						// 全国
//						case 1:
//							selectedA = true;
//
//							rsData = psAllA.executeQuery();
//							bean = convertToCefrBean(rsData);
//							cefrItem.getB42AllList().add(bean);
//							break;
//
//						// 都道府県
//						case 2:
//							selectedP = true;
//							prefCdList.add(school.getPrefCD());
//
//							psAllP.setString(1, school.getPrefCD());	//都道府県コード
//							psAllP.setString(2, exam.getExamYear());	//年度
//							psAllP.setString(3, exam.getExamCD());
//							rsData = psAllP.executeQuery();
//							bean = convertToCefrBean(rsData);
//							cefrItem.getB42AllList().add(bean);
//							break;
//						// 高校
//						case 3:
//							selectedS = true;
//							schoolCdList.add(school.getSchoolCD());
//
//							psAllS.setString(1, school.getSchoolCD());	//一括コード
//							psAllS.setString(2, exam.getExamYear());	//年度
//							psAllS.setString(3, exam.getExamCD());
//							rsData = psAllS.executeQuery();
//							bean = convertToCefrBean(rsData);
//							cefrItem.getB42AllList().add(bean);
//							break;
//					}
//					dataCnt++;
//					//300件を超えたら表示対象外
//					if (dataCnt >= B42CEFRLIST_MAX_COUNT) {
//						break;
//					}
//
//				}
//				cefrItem.setIntDetailCount(dataCnt);
//			}
//
//			//各試験情報取得
//			{
//
//				Query query7 = QueryLoader.getInstance().load("b42_7");	//各試験（全国）
//				psEachA = conn.prepareStatement(query7.toString());
//
//				Query query8 = QueryLoader.getInstance().load("b42_8");	//各試験（県）
//				psEachP = conn.prepareStatement(query8.toString());
//
//				Query query9 = QueryLoader.getInstance().load("b42_9");	//各試験（高校）
//				psEachS = conn.prepareStatement(query9.toString());
//
//
//				Query query = QueryLoader.getInstance().load("b42_13");	//参加試験コード、参加試験レベルコード取得
//				query.setStringArray(1, schoolCdList.toArray(new String[0]));
//				query.setStringArray(2, prefCdList.toArray(new String[0]));
//
//				psExam = conn.prepareStatement(query.toString());
//				psExam.setString(1, exam.getExamYear());	//年度
//				psExam.setString(2, exam.getExamCD());
//				rsExam = psExam.executeQuery();
//
//				int examCnt = 0;
//				//参加試験でループ
//				while (rsExam.next()) {
//					// 受験有無フラグ
//					String existFlgS = rsExam.getString(8).trim();
//					String existFlgP = rsExam.getString(9).trim();
//					String existFlgA = rsExam.getString(10).trim();
//					//対象が選択高校のみ？
//					if (data.getIntTargetCheckBoxFlg() == 1) {
//						if ((selectedA == true && "1".equals(existFlgA)) ||		//全国選択あり、英語認定試験別・合計CEFR取得状況（全国）データあり
//							(selectedP == true && "1".equals(existFlgP)) ||		//都道府県選択あり、英語認定試験別・合計CEFR取得状況（県）データあり
//							(selectedS == true && "1".equals(existFlgS))) {		//高校選択あり、英語認定試験別・合計CEFR取得状況（高校）データあり
//							//OK
//						}
//						else {
//							continue;
//						}
//					}
//					String engptCd = rsExam.getString(1);					//認定試験情報マスタ.参加試験コード
//					String engptLvlCd = rsExam.getString(5);				//認定試験詳細情報マスタ.参加試験レベルコード
//					String examName = rsExam.getString(11);					//試験名
//
//					//試験名
//					cefrItem.getB42EachExamNameList().add(examName);
//					ArrayList<B42CefrListBean> examList = new ArrayList();
//
//					int dataCnt = 0;
//					Iterator ite = compSchoolList.iterator();
//					while (ite.hasNext()) {
//						SheetSchoolData school = (SheetSchoolData) ite.next();
//						B42CefrListBean bean = new B42CefrListBean();
//
//						switch (school.getSchoolTypeCode()) {
//
//							// 全国
//							case 1:
//								psEachA.setString(1, engptCd);
//								psEachA.setString(2, engptLvlCd);
//								psEachA.setString(3, exam.getExamYear());	//年度
//								psEachA.setString(4, exam.getExamCD());
//								rsData = psEachA.executeQuery();
//								bean = convertToCefrBean(rsData);
//								examList.add(bean);
//								break;
//
//							// 都道府県
//							case 2:
//								psEachP.setString(1, school.getPrefCD());	//都道府県コード
//								psEachP.setString(2, engptCd);
//								psEachP.setString(3, engptLvlCd);
//								psEachP.setString(4, exam.getExamYear());	//年度
//								psEachP.setString(5, exam.getExamCD());
//								rsData = psEachP.executeQuery();
//								bean = convertToCefrBean(rsData);
//								examList.add(bean);
//								break;
//							// 高校
//							case 3:
//								psEachS.setString(1, school.getSchoolCD());	//一括コード
//								psEachS.setString(2, engptCd);
//								psEachS.setString(3, engptLvlCd);
//								psEachS.setString(4, exam.getExamYear());	//年度
//								psEachS.setString(5, exam.getExamCD());
//								rsData = psEachS.executeQuery();
//								bean = convertToCefrBean(rsData);
//								examList.add(bean);
//								break;
//						}
//						dataCnt++;
//						//300件を超えたら表示対象外
//						if (dataCnt >= B42CEFRLIST_MAX_COUNT) {
//							break;
//						}
//
//					}
//					cefrItem.getB42EachList().add(examCnt, examList);
//					examCnt++;
//				}
//
//			}
//
//			//受け渡しクラスに設定
//			data.setIteB42Cefr(cefrItem);
//
//		}
//		finally {
//			DbUtils.closeQuietly(rsData);
//			DbUtils.closeQuietly(rsExam);
//			DbUtils.closeQuietly(psExam);
//			DbUtils.closeQuietly(psAllA);
//			DbUtils.closeQuietly(psAllP);
//			DbUtils.closeQuietly(psAllS);
//			DbUtils.closeQuietly(psEachA);
//			DbUtils.closeQuietly(psEachP);
//			DbUtils.closeQuietly(psEachS);
//		}
//	}
//
//
//	/**
//	 * 取得した情報を縦横変換し、B42CefrListBeanとして返却
//	 * @param rs	取得情報
//	 * @return	B42CefrListBean
//	 * @throws Exception
//	 */
//	private B42CefrListBean convertToCefrBean(ResultSet rs)  throws Exception {
//		B42CefrListBean bean = new B42CefrListBean();
//
//		int idx = 0;
//		while (rs.next()) {
//			if(idx == 0) {
//				bean.setStrName(rs.getString(4));
//			}
//			bean.setIntNumbers(idx, rs.getInt(5));
//			bean.setFloCompRatio(idx, rs.getFloat(6));
//			idx++;
//		}
//
//		return bean;
//	}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("B42_01");
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_OTHER_DEV;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new B42().b42(data, outfileList, getAction(), sessionKey, sheetLog);
	}

}

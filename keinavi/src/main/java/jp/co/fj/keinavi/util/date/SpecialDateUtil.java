/*
 * 作成日: 2004/10/07
 *
 */
package jp.co.fj.keinavi.util.date;

import java.util.Calendar;
import java.util.Locale;

/**
 * @author Totec) T.Yamada
 *
 *	2005.3.10 	T.Yamada 	[1]年度計算を５月〜４月にできるメソッドを追加
 */
public class SpecialDateUtil {
	
	/**
	 * 今年度を返す。年度は(4〜3月)
	 * @param year
	 * @return
	 */
	public static String getThisJpnYear(){
		Calendar calendar = Calendar.getInstance(Locale.JAPAN);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		if(month == 1 || month == 2 || month == 3){
			year --;
		}
		return Integer.toString(year);
	}
	
	/**
	 * 今年度を返す。年度は(5〜4月) ※I306で使用
	 * @return
	 */
	public static String getThisJpnYearRev(){
		Calendar calendar = Calendar.getInstance(Locale.JAPAN);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		if(month >= 1 && month <= 4){
			year --;
		}
		return Integer.toString(year);
	}

	/**
	 * 年度は(5〜4月)を基準として、指定の月が何年（×年度）かを返す
	 * @param month
	 * @return
	 */
	public static String getYearOfMonthRev(int month){
		
		final int MAX_JPNMONTH_REV = 4;
		
		Calendar calendar = Calendar.getInstance(Locale.JAPAN);
		int thisYear = calendar.get(Calendar.YEAR);
		int thisMonth = calendar.get(Calendar.MONTH) + 1;

		//指定の月が1，2，3, 4を指定している
		if(month >= 1 && month <= MAX_JPNMONTH_REV){
			//今月が1，2，3, 4でない
			if(!(thisMonth >= 1 && thisMonth <= MAX_JPNMONTH_REV)){
				//今年の翌年が指定年
				thisYear ++;
			}
		}
		//指定の月が5〜12月を指定している
		else{
			//今月が1，2，3, 4である
			if(thisMonth >= 1 && thisMonth <=MAX_JPNMONTH_REV){
				//今年の前年が指定年
				thisYear --;
			} 						
		}
		return Integer.toString(thisYear);
	}
}

package jp.co.fj.keinavi.excel.data.school;

//import java.util.ArrayList;
/**
 * �Z�����ѕ��́|�ߔN�x��r�|�΍��l���z �W�v�f�[�^���X�g
 * �쐬��: 2004/07/21
 * @author	A.Iwata
 */
public class S22TotalListBean {
	
	//�͎��N�x
	private String strNendo = "";
	//���v�l��
	private int intTotalNinzu = 0;
	//���ϓ_
	private float floHeikin = 0;
	//���ϕ΍��l
	private float floHensa = 0;

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrNendo() {
		return strNendo;
	}
	public int getIntTotalNinzu() {
		return this.intTotalNinzu;
	}
	public float getFloHeikin() {
		return this.floHeikin;
	}
	public float getFloHensa() {
		return this.floHensa;
	}


	/*----------*/
	/* Set      */
	/*----------*/

	public void setStrNendo(String strNendo) {
		this.strNendo = strNendo;
	}
	public void setIntTotalNinzu(int intTotalNinzu) {
		this.intTotalNinzu = intTotalNinzu;
	}
	public void setFloHeikin(float floHeikin) {
		this.floHeikin = floHeikin;
	}
	public void setFloHensa(float floHensa) {
		this.floHensa = floHensa;
	}
}

/**
 * �Z�����ѕ��́|�N���X��r�@�ݖ�ʐ��сi�N���X��r�j
 *      ����]���ʐl�� �����]��-�N���X �f�[�^�N���X
 * �쐬��: 2019/09/05
 * @author      M.Ooseto
 */

package jp.co.fj.keinavi.excel.data.school;

public class S33KokugoClassListBean {
    // �N���X�|���o��
    private String kokugoClass = "";
    // �N���X�|�󌱎Ґ�
    private int numbers = 0;
    // �N���X�|A�]���@����
    private float evalACompratio = 0;
    // �N���X�|B�]���@����
    private float evalBCompratio = 0;
    // �N���X�|C�]���@����
    private float evalCCompratio = 0;
    // �N���X�|D�]���@����
    private float evalDCompratio = 0;
    // �N���X�|E�]���@����
    private float evalECompratio = 0;
    // �N���X�|A�]���@�l��
    private int evalANumbers = 0;
    // �N���X�|B�]���@�l��
    private int evalBNumbers = 0;
    // �N���X�|C�]���@�l��
    private int evalCNumbers = 0;
    // �N���X�|D�]���@�l��
    private int evalDNumbers = 0;
    // �N���X�|E�]���@�l��
    private int evalENumbers = 0;
    // �\�[�g��
    private int dispsequence = 0;

    /*-----*/
    /* Get */
    /*-----*/
    public String getKokugoClass() {
        return this.kokugoClass;
    }
    public int getNumbers() {
        return this.numbers;
    }
    public float getEvalACompratio() {
        return this.evalACompratio;
    }
    public float getEvalBCompratio() {
        return this.evalBCompratio;
    }
    public float getEvalCCompratio() {
        return this.evalCCompratio;
    }
    public float getEvalDCompratio() {
        return this.evalDCompratio;
    }
    public float getEvalECompratio() {
        return this.evalECompratio;
    }
    public int getEvalANumbers() {
        return this.evalANumbers;
    }
    public int getEvalBNumbers() {
        return this.evalBNumbers;
    }
    public int getEvalCNumbers() {
        return this.evalCNumbers;
    }
    public int getEvalDNumbers() {
        return this.evalDNumbers;
    }
    public int getEvalENumbers() {
        return this.evalENumbers;
    }
    public int getDispsequence() {
        return this.dispsequence;
    }

    /*-----*/
    /* Set */
    /*-----*/
    public void setKokugoClass(String kokugoClass) {
        this.kokugoClass = kokugoClass;
    }
    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }
    public void setEvalACompratio(float evalACompratio) {
        this.evalACompratio = evalACompratio;
    }
    public void setEvalBCompratio(float evalBCompratio) {
        this.evalBCompratio = evalBCompratio;
    }
    public void setEvalCCompratio(float evalCCompratio) {
        this.evalCCompratio = evalCCompratio;
    }
    public void setEvalDCompratio(float evalDCompratio) {
        this.evalDCompratio = evalDCompratio;
    }
    public void setEvalECompratio(float evalECompratio) {
        this.evalECompratio = evalECompratio;
    }
    public void setEvalANumbers(int evalANumbers) {
        this.evalANumbers = evalANumbers;
    }
    public void setEvalBNumbers(int evalBNumbers) {
        this.evalBNumbers = evalBNumbers;
    }
    public void setEvalCNumbers(int evalCNumbers) {
        this.evalCNumbers = evalCNumbers;
    }
    public void setEvalDNumbers(int evalDNumbers) {
        this.evalDNumbers = evalDNumbers;
    }
    public void setEvalENumbers(int evalENumbers) {
        this.evalENumbers = evalENumbers;
    }
    public void setDispsequence(int dispsequence) {
        this.dispsequence = dispsequence;
    }

}

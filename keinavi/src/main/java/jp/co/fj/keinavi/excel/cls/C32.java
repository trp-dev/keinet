/**
 * クラス成績分析−クラス比較
 * 出力する帳票の判断
 * 作成日: 2004/08/17
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.cls;
import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.cls.C32Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

public class C32 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean c32( C32Item c32Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {
		KNLog log = KNLog.getInstance(null,null,null);

		try{
			//C32SItemから各帳票を出力
			int ret = 0;
			if (( c32Item.getIntHyouFlg()==0 )&&( c32Item.getIntNinzuGraphFlg()==0 )&&
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD START
//// 2019/09/06 QQ)Tanioka 共通テスト英語認定試験CEFR取得状況 UPD START
////			( c32Item.getIntKoseiGraphFlg()==0 )&&( c32Item.getIntBnpGraphFlg()==0 )) {
//				( c32Item.getIntKoseiGraphFlg()==0 )&&( c32Item.getIntBnpGraphFlg()==0 )&&
//				( c32Item.getIntCheckBoxFlg()==0 ) ) {
//// 2019/09/06 QQ)Tanioka 共通テスト英語認定試験CEFR取得状況 UPD END
				( c32Item.getIntKoseiGraphFlg()==0 )&&( c32Item.getIntBnpGraphFlg()==0 )) {
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD END
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
			    //throw new Exception("C32 ERROR : フラグ異常");
			    throw new IllegalStateException("C32 ERROR : フラグ異常");
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			}
			if (( c32Item.getIntHyouFlg()==1 )&&(c32Item.getIntKoseihiFlg()==2)) {
				C32_01 exceledit = new C32_01();
				ret = exceledit.c32_01EditExcel( c32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C32_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C32_01");
			}
			if (( c32Item.getIntHyouFlg()==1 )&&(c32Item.getIntKoseihiFlg()==1)) {
				C32_02 exceledit = new C32_02();
				ret = exceledit.c32_02EditExcel( c32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C32_02","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C32_02");
			}
			if (( c32Item.getIntBnpGraphFlg()==1 )&&(c32Item.getIntAxisFlg()==1)) {
				C32_03 exceledit = new C32_03();
				ret = exceledit.c32_03EditExcel( c32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C32_03","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C32_03");
			}
			if (( c32Item.getIntBnpGraphFlg()==1 )&&(c32Item.getIntAxisFlg()==2)) {
				C32_04 exceledit = new C32_04();
				ret = exceledit.c32_04EditExcel( c32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C32_04","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C32_04");
			}
			if (( c32Item.getIntNinzuGraphFlg()==1 )&&(c32Item.getIntNinzuPitchFlg()==1)) {
				C32_05 exceledit = new C32_05();
				ret = exceledit.c32_05EditExcel( c32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C32_05","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C32_05");
			}
			if (( c32Item.getIntNinzuGraphFlg()==1 )&&(c32Item.getIntNinzuPitchFlg()==2)) {
				C32_06 exceledit = new C32_06();
				ret = exceledit.c32_06EditExcel( c32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C32_06","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C32_06");
			}
			if (( c32Item.getIntNinzuGraphFlg()==1 )&&(c32Item.getIntNinzuPitchFlg()==3)) {
				C32_07 exceledit = new C32_07();
				ret = exceledit.c32_07EditExcel( c32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C32_07","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C32_07");
			}
			if (( c32Item.getIntKoseiGraphFlg()==1 )&&(c32Item.getIntKoseiPitchFlg()==1)) {
				C32_08 exceledit = new C32_08();
				ret = exceledit.c32_08EditExcel( c32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C32_08","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C32_08");
			}
			if (( c32Item.getIntKoseiGraphFlg()==1 )&&(c32Item.getIntKoseiPitchFlg()==2)) {
				C32_09 exceledit = new C32_09();
				ret = exceledit.c32_09EditExcel( c32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C32_09","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C32_09");
			}
			if (( c32Item.getIntKoseiGraphFlg()==1 )&&(c32Item.getIntKoseiPitchFlg()==3)) {
				C32_10 exceledit = new C32_10();
				ret = exceledit.c32_10EditExcel( c32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C32_10","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C32_10");
			}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/06 QQ)Tanioka 共通テスト英語認定試験CEFR取得状況 ADD START
//			if ( c32Item.getIntCheckBoxFlg()==1 ) {
//				C32_11 exceledit = new C32_11();
//				ret = exceledit.c32_11EditExcel( c32Item, outfilelist, saveFlg, UserID );
//				if (ret!=0) {
//					log.Err("0"+ret+"C32_11","帳票作成エラー","");
//					return false;
//				}
//				sheetLog.add("C32_11");
//			}
//// 2019/09/06 QQ)Tanioka 共通テスト英語認定試験CEFR取得状況 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		} catch(Exception e) {
			log.Err("99C32","データセットエラー",e.toString());
			return false;
		}
		return true;
	}

}
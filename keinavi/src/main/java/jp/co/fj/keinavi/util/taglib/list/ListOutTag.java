package jp.co.fj.keinavi.util.taglib.list;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Listの内容を出力するTaglib
 * 
 * @author kawai
 */
public class ListOutTag extends TagSupport {

	// 変数名
	private String var = null;

	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {
		List container = (List) pageContext.getAttribute(getVar());
		try {
			StringBuffer buff = new StringBuffer();
			Iterator ite = container.iterator();
			while (ite.hasNext()) {
				String value = (String) ite.next();
				buff.append(value);
			}
			pageContext.getOut().print(buff.toString());
		} catch (IOException e) {
			throw new JspException(e);
		}

		return EVAL_PAGE;
	}

	public String getVar() {
		return this.var;
	}
	public void setVar(String var) {
		this.var = var;
	}
}

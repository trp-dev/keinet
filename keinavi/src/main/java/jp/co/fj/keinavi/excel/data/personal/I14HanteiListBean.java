package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
import java.util.ArrayList;
/**
 * �l���ѕ��́|�󌱗p�����|�u�]�Z���� �u�]�Z����f�[�^���X�g
 * �쐬��: 2004/07/21
 * @author	A.Iwata
 */
public class I14HanteiListBean {
	//������� �n��E�s���{��
	private String strArea = "";
	//������� ��w����
	private String strDaigaku = "";
	//������� �w���n��
	private String strKeitou = "";
	//������� �ۂ��ۂ��Ȃ��t���O
	private int intAssignFlg = 0;
//	S 2004.09.03 A.Hasegawa
	//������� (�Z���^���p��)2����Ǝ��������ۂ��Ȃ�
	private int intSecondFlg = 0;
//	E 2004.09.03
	//������� �񎟉Ȗ�
	private String strNijiKmk = "";
	//������� �w�Z�敪
	private String strGakkoKbn = "";
	//������� ������(�J�n�j
	private String strNyushibiStart = "";
	//������� ������(�I���j
	private String strNyushibiEnd = "";
	//������� �]���͈̓t���O
	private int intHyoukaHanniFlg = 0;
	//������� �]���͈�
	private String strHyoukaHanni = "";
	//������� �ΏۊO
	private String strTaisyougai = "";
	//�u�]�Z�]�����X�g
	private ArrayList i14HyoukaList = new ArrayList();
	//���_���X�g
	private ArrayList i14TokutenList = new ArrayList();
	//�u�]�Z����p�f�[�^���X�g
	private ArrayList i14ShibouHanteiList = new ArrayList();
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrArea() {
		return this.strArea;
	}
	public String getStrDaigaku() {
		return this.strDaigaku;
	}
	public String getStrKeitou() {
		return this.strKeitou;
	}
	public int getIntAssignFlg() {
		return this.intAssignFlg;
	}
//	S 2004.09.03 A.Hasegawa
	public int getIntSecondFlg() {
		return this.intSecondFlg;
	}
//	E 2004.09.03
	public String getStrNijiKmk() {
		return this.strNijiKmk;
	}
	public String getStrGakkoKbn() {
		return this.strGakkoKbn;
	}
	public String getStrNyushibiStart() {
		return this.strNyushibiStart;
	}
	public String getStrNyushibiEnd() {
		return this.strNyushibiEnd;
	}
	public int getIntHyoukaHanniFlg() {
		return this.intHyoukaHanniFlg;
	}
	public String getStrHyoukaHanni() {
		return this.strHyoukaHanni;
	}
	public String getStrTaisyougai() {
		return this.strTaisyougai;
	}
	public ArrayList getI14HyoukaList() {
		return this.i14HyoukaList;
	}
	public ArrayList getI14ShibouHanteiList() {
		return this.i14ShibouHanteiList;
	}
	public ArrayList getI14TokutenList() {
		return this.i14TokutenList;
	}
		
	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrArea(String strArea) {
		this.strArea = strArea;
	}
	public void setStrDaigaku(String strDaigaku) {
		this.strDaigaku = strDaigaku;
	}
	public void setStrKeitou(String strKeitou) {
		this.strKeitou = strKeitou;
	}
	public void setIntAssignFlg(int intAssignFlg) {
		this.intAssignFlg = intAssignFlg;
	}
//	S 2004.09.03 A.Hasegawa
	public void setIntSecondFlg(int intSecondFlg) {
		this.intSecondFlg = intSecondFlg;
	}
//	E 2004.09.03
	public void setStrNijiKmk(String strNijiKmk) {
		this.strNijiKmk = strNijiKmk;
	}
	public void setStrGakkoKbn(String strGakkoKbn) {
		this.strGakkoKbn = strGakkoKbn;
	}
	public void setStrNyushibiStart(String strNyushibiStart) {
		this.strNyushibiStart = strNyushibiStart;
	}
	public void setStrNyushibiEnd(String strNyushibiEnd) {
		this.strNyushibiEnd = strNyushibiEnd;
	}
	public void setIntHyoukaHanniFlg(int intHyoukaHanniFlg) {
		this.intHyoukaHanniFlg = intHyoukaHanniFlg;
	}
	public void setStrHyoukaHanni(String strHyoukaHanni) {
		this.strHyoukaHanni = strHyoukaHanni;
	}
	public void setStrTaisyougai(String strTaisyougai) {
		this.strTaisyougai = strTaisyougai;
	}
	public void setI14HyoukaList(ArrayList i14HyoukaList) {
		this.i14HyoukaList = i14HyoukaList;
	}
	public void setI14ShibouHanteiList(ArrayList i14ShibouHanteiList) {
		this.i14ShibouHanteiList = i14ShibouHanteiList;
	}
	public void setI14TokutenList(ArrayList i14TokutenList) {
		this.i14TokutenList = i14TokutenList;
	}

}

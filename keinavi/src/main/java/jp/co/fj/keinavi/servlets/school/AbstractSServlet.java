package jp.co.fj.keinavi.servlets.school;

import java.sql.Connection;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.beans.com_set.SubjectCountBean;
import jp.co.fj.keinavi.beans.exam.ExamFilterManager;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.totec.config.ConfigResolver;

import org.apache.commons.configuration.Configuration;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public abstract class AbstractSServlet extends DefaultHttpServlet {

	/** トップ画面の画面IDセット */
	public static final Set topSet;
	/** 詳細画面の画面IDセット */
	public static final Set detailSet;
	/** 型の設定がある画面IDセット */
	public static final Set typeSet;
	/** 科目の設定がある画面IDセット */
	public static final Set courseSet;
	
	static {
		final Configuration config = ConfigResolver.getInstance(
				).getConfiguration("screen.properties");
		topSet = new HashSet(Arrays.asList(config.getStringArray("S_TOP_PAGE")));
		detailSet = new HashSet(Arrays.asList(config.getStringArray("S_DETAIL_PAGE")));
		typeSet = new HashSet(Arrays.asList(config.getStringArray("S_TYPE_PAGE")));
		courseSet = new HashSet(Arrays.asList(config.getStringArray("S_COURSE_PAGE")));
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#getExamSession(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	protected ExamSession getExamSession(final HttpServletRequest request) {
		
		return ExamFilterManager.rebuild(getForward(request),
				getLoginSession(request), super.getExamSession(request));
	}
	
	/**
	 * @param con DBコネクション
	 * @param schoolCd 学校コード
	 * @param examYear 模試年度
	 * @param examCd 模試コード
	 * @return 科目カウントBean
	 * @throws Exception SQL例外
	 */
	protected SubjectCountBean createSubjectCountBean(
			final Connection con, final String schoolCd,
			final String examYear, final String examCd) throws Exception{
		
		SubjectCountBean bean = new SubjectCountBean(schoolCd);
		bean.setConnection(null, con);
		bean.setExamYear(examYear);
		bean.setExamCd(examCd);
		bean.execute();
		return bean;
	}

}

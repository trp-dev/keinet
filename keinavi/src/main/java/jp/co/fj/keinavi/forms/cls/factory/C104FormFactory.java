/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.cls.factory;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.forms.cls.CMaxForm;

import com.fjh.forms.ActionForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class C104FormFactory extends AbstractCFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		// アクションフォーム
		CMaxForm form = super.createCMaxForm(request);
		// アイテムマップ
		Map map = getItemMap(request);
		
		//  科目・共通項目設定利用
		getCommonCourseAndAnalyzeCourse( map, form );
		//  表
		getChart( map, form );
		//  グラフ
		getGraph( map, form );
		//  クラスの表示順序
		getClassOrder( map, form );
		//  セキュリティスタンプ
		getStamp( map, form );

		return form;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
		CMaxForm form = (CMaxForm)request.getAttribute("form");
		// アイテムマップ
		Map map = getItemMap(request);
		
		//  科目・共通項目設定利用
		setCommonCourseAndAnalyzeCourse( map, form );
		//  表
		setChart( map, form );
		//  グラフ
		setGraph( map, form );
		//  クラスの表示順序
		setClassOrder( map, form );
		//  セキュリティスタンプ
		setStamp( map, form );
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap("030203");
	}

}

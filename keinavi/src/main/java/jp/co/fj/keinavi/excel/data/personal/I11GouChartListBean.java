package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
import java.util.ArrayList;
/**
 * 個人成績分析−成績分析面談 バランスチャート（合格者平均）データリスト
 * 作成日: 2004/07/20
 * @author	A.Iwata
 * 
 * 2009.11.26   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 */

public class I11GouChartListBean {
	//教科名
	private String strKyokamei = "";
	//1教科複数受験フラグ
	private int intKyokaDispFlg = 0;
	//対象模試偏差値
	private float floMshHensa = 0;
	//学力レベル
	private String strScholarLevel = "";
	//志望校別合格者平均リスト
	private ArrayList i11ShiboGouList = new ArrayList();
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKyokamei() {
		return this.strKyokamei;
	}
	public int getIntKyokaDispFlg() {
		return this.intKyokaDispFlg;
	}
	public float getFloMshHensa() {
		return this.floMshHensa;
	}
	public ArrayList getI11ShiboGouList() {
		return this.i11ShiboGouList;
	}
	public String getStrScholarLevel() {
		return strScholarLevel;
	}
		
	/*----------*/
	/* Set      */
	/*----------*/
	
	public void setStrKyokamei(String strKyokamei) {
		this.strKyokamei = strKyokamei;
	}
	public void setIntKyokaDispFlg(int intKyokaDispFlg) {
		this.intKyokaDispFlg = intKyokaDispFlg;
	}
	public void setFloMshHensa(float floMshHensa) {
		this.floMshHensa = floMshHensa;
	}
	public void setI11ShiboGouList(ArrayList i11ShiboGouList) {
		this.i11ShiboGouList = i11ShiboGouList;
	}
	public void setStrScholarLevel(String string) {
		this.strScholarLevel = string;
	}
}

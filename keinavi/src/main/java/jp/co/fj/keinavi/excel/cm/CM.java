package jp.co.fj.keinavi.excel.cm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import jp.co.fj.keinavi.beans.CefrBean;
import jp.co.fj.keinavi.data.sheet.ExcelName;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.totec.config.ConfigResolver;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.contrib.HSSFRegionUtil;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 *
 * Excel共通関数
 *
 *
 * 2004.09.15	[新規作成]
 *
 * 2006.02.08	Yoshimoto KAWAI - TOTEC
 * 				「対象模試」の文字列を外部ファイル化
 *
 *
 * @author THS
 * @version 1.0
 *
 */
public class CM {

	// 設定
	private static final Configuration CONFIG = ConfigResolver.getInstance(
			).getConfiguration("excel.properties");

//	final private String  	PROPERTIES_NAME = "C:/app/eclipse/workspace/kntest/WEB-INF/src/jp/co/fj/keinavi/resources/common.properties";
	final private String		mastersheet 	= "tmp";		//テンプレートシート名
	final private String 		pramsheet  		= "prm";
	KNLog log = KNLog.getInstance(null,null,null);

/*
 * 	データセット関数
 * 		int row: 行位置
 * 		int col: 列位置
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public HSSFCell setCell(HSSFSheet workSheet, HSSFRow workRow, HSSFCell workCell, int row , int col ) {

		// データセット
		workRow = workSheet.getRow(row);
		if (workRow == null) workRow = workSheet.createRow((short)row);
		workCell = workRow.getCell((short)col);
		if (workCell == null) workCell = workRow.createCell((short)col);
		workCell.setEncoding(HSSFCell.ENCODING_UTF_16);

		return workCell;
	}

/*
 * 	セキュリティスタンプ取得関数
 * 		int secuFlg: セキュリティフラグ
 * 		int startCol: 行位置
 * 		int endCol: 列位置
 * 		戻り値: securityStump
 */
	public String setSecurity(HSSFWorkbook workbook, HSSFSheet sheet, int secuFlg, int startCol, int endCol ) {

		String securityStump = "";
		try {
			if (secuFlg == 1){
				securityStump="機密";
			} else if (secuFlg == 2 ) {
				securityStump="部内限り";
			} else if (secuFlg == 3 ) {
				securityStump="校内限り";
			} else if (secuFlg == 4 ) {
				securityStump="取扱注意";
			} else if (secuFlg == 5 ) {
				securityStump="";
			} else if (secuFlg == 11 ) {
				securityStump="機密";
			} else if (secuFlg == 12 ) {
				securityStump="部外秘";
			} else if (secuFlg == 13 ) {
				securityStump="塾外秘";
			} else if (secuFlg == 14 ) {
				securityStump="取扱注意";
			}

			if (secuFlg != 5) {
				// 罫線設定（範囲指定）
				Region region = new Region( 0, (short)startCol, 0, (short)endCol );
				// 範囲指定による太線設定
				try {
					HSSFRegionUtil.setBorderBottom	(HSSFCellStyle.BORDER_MEDIUM,region, sheet, workbook );
					HSSFRegionUtil.setBorderTop		(HSSFCellStyle.BORDER_MEDIUM,region, sheet, workbook );
					HSSFRegionUtil.setBorderLeft	(HSSFCellStyle.BORDER_MEDIUM,region, sheet, workbook );
					HSSFRegionUtil.setBorderRight	(HSSFCellStyle.BORDER_MEDIUM,region, sheet, workbook );
					// 範囲指定による線色設定
					HSSFRegionUtil.setBottomBorderColor	(HSSFColor.RED.index, region, sheet, workbook);
					HSSFRegionUtil.setTopBorderColor	(HSSFColor.RED.index, region, sheet, workbook);
					HSSFRegionUtil.setLeftBorderColor	(HSSFColor.RED.index, region, sheet, workbook);
					HSSFRegionUtil.setRightBorderColor	(HSSFColor.RED.index, region, sheet, workbook);
				} catch (Exception e) {
					log.Err("","セキュリティスタンプ罫線セットエラー",e.toString());
					// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL START
					//e.printStackTrace();
					// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL END
				}
			}
			return securityStump;

		} catch (Exception e) {
			log.Err("","セキュリティスタンプ取得エラー",e.toString());
			// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL START
			//e.printStackTrace();
			// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL END
			return securityStump;
		}
	}

/*
 * 	模試月取得関数
 * 		String date: 模試実施日
 * 		戻り値: moshi
 */
	public String setTaisyouMoshi(String date) {

		// 模試月を取得
		String moshi ="";
		if (!toString(date).equals("")) {
			moshi = date.substring(4,6);
		}

		if ( !moshi.equals("") ) {
			if ( moshi.equals("01") ) {
				moshi="（１月）";
			} else if ( moshi.equals("02") ) {
				moshi="（２月）";
			} else if ( moshi.equals("03") ) {
				moshi="（３月）";
			} else if ( moshi.equals("04") ) {
				moshi="（４月）";
			} else if ( moshi.equals("05") ) {
				moshi="（５月）";
			} else if ( moshi.equals("06") ) {
				moshi="（６月）";
			} else if ( moshi.equals("07") ) {
				moshi="（７月）";
			} else if ( moshi.equals("08") ) {
				moshi="（８月）";
			} else if ( moshi.equals("09") ) {
				moshi="（９月）";
			} else if ( moshi.equals("10") ) {
				moshi="（１０月）";
			} else if ( moshi.equals("11") ) {
				moshi="（１１月）";
			} else if ( moshi.equals("12") ) {
				moshi="（１２月）";
			} else {
				log.Err("","模試月取得エラー","");
			}
		}
		return moshi;

	}

	/*************************************
	 * 	ヘッダ右側に帳票作成日時を表示する
	 *************************************/
	public void setHeader(HSSFWorkbook workbook, HSSFSheet sheet) {
		try {
			HSSFHeader header;
			Date date = new Date();
			SimpleDateFormat DF = new SimpleDateFormat("yyyy/MM/dd");
			String strDate = DF.format(date);

			// ヘッダ右側に帳票作成日時を書き込む
			//sheet = workbook.getSheetAt(snum);
			header = sheet.getHeader();
			header.setRight( HSSFHeader.font("ＭＳ ゴシック","標準") + strDate );
			// フォントサイズ
			HSSFFont font = workbook.createFont();
			font.setFontHeightInPoints((short)11);
			font.setFontName("MSゴシック");
			// フォント設定
			HSSFCellStyle style = workbook.createCellStyle();
			style.setFont(font);
		} catch (Exception e) {
			log.Err("","帳票作成日時セットエラー",e.toString());
			// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL START
			//e.printStackTrace();
			// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL END
		}
	}

/*
 * 	罫線出力関数（）
 * 		HSSFWorkbook 	workbook:
 * 		HSSFSheet		sheet:
 * 		short			shLineStyle:
 * 		short			shLineColor:
 * 		int				startRow:
 * 		int				endRow:
 * 		int				startCol:
 * 		int				endCol:
 *
 * 		戻り値: true = 正常終了 false = 異常終了
 */
	public boolean setLine(HSSFWorkbook workbook, HSSFSheet sheet,
								short shLineStyle, short shLineColor,
								int startRow, int endRow, int startCol, int endCol){

		// 罫線設定（範囲指定）
		Region region = new Region( 0, (short)startCol, 0, (short)endCol );
		// 範囲指定による太線設定
		try {
			HSSFRegionUtil.setBorderBottom	(shLineStyle,region, sheet, workbook );
			HSSFRegionUtil.setBorderTop		(shLineStyle,region, sheet, workbook );
			HSSFRegionUtil.setBorderLeft	(shLineStyle,region, sheet, workbook );
			HSSFRegionUtil.setBorderRight	(shLineStyle,region, sheet, workbook );
			// 範囲指定による線色設定
			HSSFRegionUtil.setBottomBorderColor	(shLineColor, region, sheet, workbook);
			HSSFRegionUtil.setTopBorderColor	(shLineColor, region, sheet, workbook);
			HSSFRegionUtil.setLeftBorderColor	(shLineColor, region, sheet, workbook);
			HSSFRegionUtil.setRightBorderColor	(shLineColor, region, sheet, workbook);
		} catch (Exception e) {
			log.Err("","罫線出力エラー",e.toString());
			// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL START
			//e.printStackTrace();
			// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL END
			return false;
		}
		return true;
	}

/*
 * 	罫線出力関数（
 * 	罫線出力位置指定
 * 		HSSFWorkbook 	workbook:
 * 		HSSFSheet		sheet:
 * 		short			shLineStyle:
 * 		short			shLineColor:
 * 		int				startRow:
 * 		int				endRow:
 * 		int				startCol:
 * 		int				endCol:
 * 		boolean			bolBottom:
 * 		boolean			bolTop:
 * 		boolean			bolLeft:
 * 		boolean			bolRight:
 *
 * 		戻り値: true = 正常終了 false = 異常終了
 */
	public boolean setLine(HSSFWorkbook workbook, HSSFSheet sheet,
						short shLineStyle, short shLineColor,
							int startRow, int endRow, int startCol, int endCol,
								boolean bolBottom, boolean bolTop, boolean bolLeft, boolean bolRight){

		// 罫線設定（範囲指定）
		Region region = new Region( (short)startRow, (short)startCol, (short)endRow, (short)endCol );
		// 範囲指定による太線設定
		try {
			if(bolBottom == true ){
				HSSFRegionUtil.setBorderBottom	(shLineStyle,region, sheet, workbook );
				HSSFRegionUtil.setBottomBorderColor	(shLineColor, region, sheet, workbook);
			}
			if(bolTop == true){
				HSSFRegionUtil.setBorderTop		(shLineStyle,region, sheet, workbook );
				HSSFRegionUtil.setTopBorderColor	(shLineColor, region, sheet, workbook);
			}
			if(bolLeft == true){
				HSSFRegionUtil.setBorderLeft	(shLineStyle,region, sheet, workbook );
				HSSFRegionUtil.setLeftBorderColor	(shLineColor, region, sheet, workbook);
			}
			if( bolRight == true){
				HSSFRegionUtil.setBorderRight	(shLineStyle,region, sheet, workbook );
				HSSFRegionUtil.setRightBorderColor	(shLineColor, region, sheet, workbook);
			}

		} catch (Exception e) {
			log.Err("","罫線出力エラー",e.toString());
			// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL START
			//e.printStackTrace();
			// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL END
			return false;
		}

		return true;

	}

/*
 * SessionIDのフォルダを作成する
 * 		String			SessionID:セッションID
 * 		string			strPath:作成したフォルダのフルパス
 *
 * 		戻り値: true = 正常終了 false = 異常終了
 */
	public String strCreateSessionFolder(String SessionID){
		try{

//			String strXlsPath = PropertiesFile.getParam(PROPERTIES_NAME, "KNTempPath");
			String strXlsPath = KNCommonProperty.getKNTempPath();

			if(strXlsPath.equals("")==true){
				strXlsPath = System.getProperty("user.dir");
			}

			String strPath = strXlsPath + "/" + SessionID;

			File dir = new File( strPath );
			if( dir.isDirectory() == false ){
				if(dir.mkdirs() == false){
					strPath = "";
					return strPath;
				}
			}
			return strPath;
		}
		catch( Exception e ){
			log.Err(SessionID,"フォルダ作成失敗",e.toString());
			// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL START
			//e.printStackTrace();
			// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL END
			return "";
		}
	}

/*
 * 	Excelファイル保存
 * 		int				intSaveFlg:	1=表示 2=印刷 3=一括印刷(保存なし) 4=一括印刷（保存あり） 5=保存
 * 						個人成績分析の場合	12=印刷(保存なし) 13=一括印刷（保存なし） 14=一括印刷（保存あり） 15=印刷(保存あり）
 * 											6=かんたん印刷（保存なし） 7=かんたん印刷（保存あり）
 * 		ArrayList		outfilelist:
 * 		HSSFWorkbook 	workbook:
 * 		String			SessionID:セッションID
 * 		int				intSeqNo:連番
 * 		String			strReportName:帳票固定名称
 * 		int 			intMaxSheetSr:有効シート数
 *
 * 		戻り値: true = 正常終了 false = 異常終了
 */
        public boolean bolFileSave( int intSaveFlg, ArrayList outfilelist, HSSFWorkbook workbook, String SessionID, int intSeqNo, String strReportName, int intMaxSheetSr ){

		try{
			//全データ0件のときマスタExcel読み込み
			if (workbook==null) {
				workbook = getMasterWorkBook(strReportName, intSaveFlg);
				if( workbook==null ){
					log.Err("02"+strReportName,"帳票作成エラー","");
					return false;
				}
			}

			log.Ep(strReportName,"Excelファイル保存開始","");
			FileOutputStream fout = null;
			String strSaveFile = "";

			//SessionIDフォルダ作成
			String strFilePath = strCreateSessionFolder(SessionID);

			if( strFilePath .equals("") ){
				return false;
			}

			//現在日時取得
			Thread.sleep(10); // ファイル名の重複防止

// DEL START 2019/07/26 QQ)K.Hisakawa 共通テスト対応
//			Date date = new Date();
//			SimpleDateFormat DF = new SimpleDateFormat("yyyyMMddHHmmssSSS");
//			String strDate = DF.format(date);
// DEL END   2019/07/26 QQ)K.Hisakawa 共通テスト対応

			//2004.10.13 Add
			String strSaveFileName = "";
			//Add end

                        //ファイル作成
                        //ファイル名設定
// MOD START 2019/07/26 QQ)K.Hisakawa 共通テスト対応
//			switch(intSaveFlg){
//				case 5:	//保存
//					if(intSeqNo > 0){
//						strSaveFile = strFilePath + "/" +  createBeaseName(strReportName) + "_" + StringUtils.right("00" + intSeqNo, 2) + ".xls";
//					}
//					else{
//						strSaveFile = strFilePath + "/" + createBeaseName(strReportName) + ".xls";
//					}
//					break;
//				case 6:
//				case 7:
//					strSaveFile = strFilePath + "/" + strDate + ".xls";
//					strSaveFileName = strDate + ".xls";
//					break;
//				default:
//					strSaveFile = strFilePath + "/" + strDate + ".xls";
//					break;
//			}

                        strSaveFile = strFilePath + "/" +  createBeaseName(strReportName) + ".xls";
// MOD END   2019/07/26 QQ)K.Hisakawa 共通テスト対応

			//有効シート数が０の場合は空シートを作成し、有効シート数は１とする
			if( intMaxSheetSr <= 0 ){
				if( workbook.getSheetIndex(mastersheet) >= 0 ){
					workbook.cloneSheet(workbook.getSheetIndex(mastersheet));
				}
				intMaxSheetSr = 1;

			}

// DEL START 2019/07/26 QQ)K.Hisakawa 共通テスト対応
//			switch( intSaveFlg ){
//				case 1:	//表示
//				case 5:	//保存
//				case 6:	//かんたん印刷（保存なし）
//				case 7:	//かんたん印刷（保存あり）
//				case 11: //表示（個人成績分析）
// DEL END   2019/07/26 QQ)K.Hisakawa 共通テスト対応

					//テンプレートシート削除
					if( workbook.getSheetIndex(mastersheet) >= 0){
//						if (intMaxSheetSr <= 0) {
//							workbook.cloneSheet(0);
//						}
						workbook.removeSheetAt(workbook.getSheetIndex(mastersheet));
					}

					int intSheetSr = workbook.getNumberOfSheets();
					if( intSheetSr > intMaxSheetSr ){
						//有効シート数以上のシートが存在する場合は削除を実行
						for( int i = intSheetSr-1; i >= intMaxSheetSr; i-- ){
							workbook.removeSheetAt(i);
//							int intSheetCnt = workbook.getNumberOfSheets();
//							if (intSheetCnt!=1) {
//								workbook.removeSheetAt(i);
//							}
						}
					}

					//シート名変更
					intSheetSr = workbook.getNumberOfSheets();
					for( int i = 0; i < intSheetSr; i++){
						workbook.setSheetName(i, String.valueOf(i+1), HSSFWorkbook.ENCODING_UTF_16);
					}

// DEL START 2019/07/26 QQ)K.Hisakawa 共通テスト対応
//					break;
//				default:
////				case 2:
////				case 3:
////				case 4:
//					if (intMaxSheetSr <= 0 && workbook.getSheetIndex(mastersheet) >= 0) {
//						workbook.cloneSheet(0);
//					}
//
//					//2004.10.1 Added By C.Murata
//					if( workbook.getSheetIndex(mastersheet) >= 0){
//						//シート名変更
//						intSheetSr = workbook.getNumberOfSheets();
//						for( int i = 2; i < intSheetSr; i++ ){
//							workbook.setSheetName(i, String.valueOf(i-1), HSSFWorkbook.ENCODING_UTF_16);
//						}
//						intMaxSheetSr = intSheetSr - 2;
//					}
//					//Add End
//
//					//パラメータシート作成
//					boolean ret = CreatePSheet( intSaveFlg, workbook, intSeqNo, strReportName, strSaveFile, intMaxSheetSr );
//					if( ret == false ){
//						return false;
//					}
//				 	break;
//			}
// DEL END   2019/07/26 QQ)K.Hisakawa 共通テスト対応

			//Excelファイル保存
			fout = null;
			try{
				fout = new FileOutputStream(strSaveFile);
				workbook.write(fout);
				fout.close();
			} catch(IOException e) {
				log.Err(strReportName,"Excelファイル保存エラー",e.toString());
				// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL START
				//System.out.println(e.toString());
				// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL END
				return false;
			// 2019/10/16 QQ)Ooseto 共通テスト対応 ADD START
			} finally {
                            if(fout != null) {
                                try {
                                    fout.close();
                                } catch(IOException e) {
                                    log.Err("","Excelファイル解放エラー",e.toString());
                                }
                            }
			}
			// 2019/10/16 QQ)Ooseto 共通テスト対応 ADD START

			//印刷用Excelファイル名をリストへ格納
			//2004.10.13 Update
			//outfilelist.add((String)strSaveFile);
			if( intSaveFlg == 6 || intSaveFlg == 7){
				outfilelist.add(strSaveFileName);
			}
			else{
				outfilelist.add((String)strSaveFile);
			}
			//Update End


		} catch(Exception e) {
			log.Err(strReportName,"Excelファイル保存エラー",e.toString());
			// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL START
			//System.out.println(e.toString());
			// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL END
			return false;
		}
		log.Ep(strReportName,"Excelファイル保存終了","");
		return true;
	}

/*
 * 	パラメータシート作成
 *
 * 		int				intSaveFlg:1=表示 2=印刷 3=一括印刷(保存なし) 4=一括印刷（保存あり） 5=保存
 * 		HSSFWorkbook 	workbook:
 * 		int				intSeqNo:連番
 * 		String			strReportName:帳票固定名称
 * 		String 			strPrintFileNm:印刷用テンプレート名
 * 		int				intMaxSheetSr:有効シート数
 *
 * 		戻り値: true = 正常終了 false = 異常終了
 */
	private boolean CreatePSheet( int intSaveFlg, HSSFWorkbook workbook, int intSeqNo, String strReportName, String strPrintFileNm, int intMaxSheetSr ){

		try{
			HSSFSheet 		pSheet = null;
			HSSFRow			row = null;;
			HSSFCell		cell = null;

			workbook.getSheetAt(workbook.getSheetIndex(pramsheet)).setSelected(false);
			pSheet = workbook.getSheetAt(workbook.getSheetIndex(pramsheet));

			//保存実行指示フラグ
			cell  = setCell(  pSheet, row, cell, 1, 1 );
			switch( intSaveFlg ){
				//保存なし
				case 1:
				case 2:
				case 3:
				case 12:
				case 13:
					cell.setCellValue(0);		//保存なし
					break;
				default:
					cell.setCellValue(1);		//保存あり
					break;
			}
//			if( intSaveFlg <= 3 ){
//				cell.setCellValue(0);		//保存なし
//			}else{
//				cell.setCellValue(1);		//保存あり
//			}

			//保存ファイル名
			cell = setCell( pSheet, row, cell, 2, 1 );
			if(intSeqNo > 0){
				cell.setCellValue( createBeaseName(strReportName) + "_" + StringUtils.right("00" + intSeqNo, 2) + ".xls" );
			}
			else{
				cell.setCellValue( createBeaseName(strReportName) + ".xls" );
			}

			//印刷実行指示フラグ
			cell = setCell( pSheet, row, cell, 3, 1 );
			switch( intSaveFlg ){
				//印刷なし
				case 1:	//表示
				case 5:	//保存
					cell.setCellValue(0);
					break;
				//印刷あり
				default:
//				case 2:	//印刷
//				case 3:	//一括印刷(保存なし)
//				case 4:	//一括印刷(保存あり)
					cell.setCellValue(1);
					break;
			}

			//有効シート数
			cell = setCell( pSheet, row, cell, 4, 1 );
			cell.setCellValue(intMaxSheetSr);

//			//MaxSheetSr
//			cell = setCell( pSheet, row, cell, 5, 1 );
//			cell.setCellValue(intMaxSheetSr);

		}
		catch( Exception e){
			log.Err("","パラメータシート作成エラー",e.toString());
			return false;
		}

		return true;
	}

	private String createBeaseName(final String id) {

		final StringBuffer buff = new StringBuffer();

		// 個人成績分析は【帳票ID_[学年/クラス/番号]】の形式で来る
		if (id.startsWith("I")) {
// MOD START 2019/08/16 QQ)K.Hisakawa 共通テスト対応
//			buff.append(getExcelName(id.substring(0, 6)));
//			buff.append(id.substring(6));
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD START
//		        String excelName = getExcelName(id.substring(0, 6)).replace("yyyymmddhhmmss", getTimeStamp());
		        String excelName = getExcelName(id.substring(0, 6)).replace("yyyymmddhhmmssSSS", getTimeStamp());
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD END

		        //excelName = excelName.replace("gccxxx", id.substring(7)).replaceAll(" ","");
		        excelName = excelName.replace("gccxxx", id.substring(7));

                        buff.append(excelName);
// MOD END   2019/08/16 QQ)K.Hisakawa 共通テスト対応

		// それ以外は帳票IDが渡ってくる
		} else {
// MOD START 2019/07/26 QQ)K.Hisakawa 共通テスト対応
//                        buff.append(getExcelName(id));
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD START
//			buff.append(getExcelName(id).replace("yyyymmddhhmmss", getTimeStamp()));
			buff.append(getExcelName(id).replace("yyyymmddhhmmssSSS", getTimeStamp()));
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD END
// MOD END   2019/07/26 QQ)K.Hisakawa 共通テスト対応
		}

// DEL START 2019/07/26 QQ)K.Hisakawa 共通テスト対応
//		buff.append("_");
//		buff.append(getTimeStamp());
// DEL END   2019/07/26 QQ)K.Hisakawa 共通テスト対応

		return buff.toString();
	}

	private String getExcelName(final String id) {
		return ExcelName.getInstance().getName(id);
	}

	private String getTimeStamp() {
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD START
//		return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD END
	}

	/*
	 * 	マスターファイルパス取得
	 *
	 * 		戻り値: マスターファイルパス(以上終了時は空文字（""）
	 * 		最終的には削除します
	 */
	public String getMasterXlsPath(){
		try{
			String strPath = "";

//			String xlsPath = PropertiesFile.getParam(PROPERTIES_NAME, "KNDataPath");
			String xlsPath = KNCommonProperty.getKNDataPath();

			if( xlsPath.equals("") ){
				strPath = "";
			}
			else{
				strPath = xlsPath;
			}

			return strPath;

		}
		catch( Exception e){
		    // 2019/10/10 QQ)Ooseto 共通テスト対応 ADD START
		    log.Err("","マスターファイルパス取得エラー",e.toString());
		    // 2019/10/10 QQ)Ooseto 共通テスト対応 ADD END
			return "";

		}
	}

	/*
	 * 	マスターファイル取得
	 * 		String			masterfile:マスターファイル名
	 * 		int				intSaveFlg:1=表示 2=印刷 3=一括印刷(保存なし) 4=一括印刷（保存あり） 5=保存	6/7=かんたん印刷
	 *
	 * 		戻り値: WorkBookオブジェクト(異常終了時はNull)
	 */
	 public HSSFWorkbook getMasterWorkBook(String masterfile, int intSaveFlg){
		HSSFWorkbook mWB = null;
		try{
			log.Ep(masterfile,"マスタExcel読み込み開始","");
			String strMasterXls = "";
//			String strMasterXlsPath  = PropertiesFile.getParam(PROPERTIES_NAME, "KNDataPath");
			String strMasterXlsPath = KNCommonProperty.getKNDataPath();


			log.It("intSaveFlg：",Integer.toString(intSaveFlg),"");

			switch( intSaveFlg ){
				case 1:
				case 5:
//2004.10.04 Added by C.Murata
				case 6:	//かんたん印刷（保存なし）
				case 7:	//かんたん印刷（保存あり）
				case 11: // 表示（個人成績分析）
//				10.04 Add End
					strMasterXls = strMasterXlsPath + "/" + masterfile + ".xls";
					break;
//2004.10.04 Update C.Murata
//				case 2:
//				case 3:
//				case 4:
				default:
//10.04 Update End
					strMasterXls = strMasterXlsPath + "/" + masterfile + "P.xls";
					break;
			}

			log.It("strMasterXls：",strMasterXls,"");
			// 2019/10/10 QQ)Ooseto 共通テスト対応 UPD START
			FileInputStream fin = null;
			try{
				//FileInputStream fin = new FileInputStream(strMasterXls);
				fin = new FileInputStream(strMasterXls);
				POIFSFileSystem fsystem = new POIFSFileSystem(fin);
				mWB = new HSSFWorkbook(fsystem);
				fin.close();
			} catch(IOException e) {
	                    log.Err("","マスタExcel読み込みエラー",e.toString());
	                    //System.out.println(e.toString());
	                    // 2019/10/10 QQ)Ooseto 共通テスト対応 UPD END
				return null;
                        // 2019/10/16 QQ)Ooseto 共通テスト対応 ADD START
                        } finally {
                            if(fin != null) {
                                try {
                                    fin.close();
                                } catch(IOException e) {
                                    log.Err("","マスタExcel解放エラー",e.toString());
                                }
                            }
                        }
                        // 2019/10/16 QQ)Ooseto 共通テスト対応 ADD START
			log.Ep(masterfile,"マスタExcel読み込み終了","");
	 		return mWB;
	 	}
	 	catch( Exception e ){
			log.Err(masterfile,"マスタExcel読み込みエラー",e.toString());
	 		return mWB;
	 	}

	 }

	/*
	 * 	文字列変換
	 * 		String			strData:文字列データ
	 *
	 * 		戻り値: strData(異常終了時はNull)
	 */
	 public String toString(String strData){
		try{
			return (strData==null?"":strData);
		}
		catch( Exception e ){
			log.Err(strData,"Null文字変換エラー",e.toString());
			return strData;
		}
	 }

// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD START
	/**
	 * 指定した範囲のセルをコピーする。
	 * @param workSheet   - コピー先シート
	 * @param tempSheet   - コピー元シート
	 * @param cpStClm     - コピー開始列位置
	 * @param cpStRow     - コピー開始行位置
	 * @param cpEdClm     - コピー終了列位置
	 * @param cpEdRow     - コピー終了行位置
	 * @param trgtClm     - 貼付列位置
	 * @param trgtRow     - 貼付行位置
	 */
	public void copyCellArea(HSSFSheet workSheet, HSSFSheet tempSheet, int cpStClm, int cpStRow, int cpEdClm, int cpEdRow, int trgtClm, int trgtRow) {

		// 開始行が終了行以下の間ループ
		for (int i = 0; cpStRow + i <= cpEdRow; i++) {
			// 開始列が終了列以下の間ループ
			for (int j = 0; cpStClm + j <= cpEdClm; j++) {
				// 開始行列
				HSSFCell srcCell = getCell(tempSheet, (short) (cpStClm + j), cpStRow + i);
				// 貼付行列
				HSSFCell destCell = getCell(workSheet, (short) (trgtClm + j), trgtRow + i);
				// 値を取得
				if (StringUtils.isNotEmpty(srcCell.getStringCellValue())) {
				    destCell.setCellValue(srcCell.getStringCellValue());
				}
				// スタイルを取得
				destCell.setCellStyle(srcCell.getCellStyle());
			}
		}
	}

	/**
	 * Cellを返す。
	 * @param workSheet 作業シート
	 * @param column 列
	 * @param row 行
	 * @return Cell
	 */
	private HSSFCell getCell(HSSFSheet workSheet, short column, int row) {
		HSSFRow rowObj = this.getRow(row, workSheet);
		return this.getCell(rowObj, column);
	}

	/**
	 * Rowを返す。
	 * @param rowIndex 行数
	 * @param sheet 作業シート
	 * @return Row
	 */
	private HSSFRow getRow(int rowIndex, HSSFSheet sheet) {
		HSSFRow row = sheet.getRow(rowIndex);
		if (row == null) {
			row = sheet.createRow(rowIndex);
		}
		return row;
	}

	/**
	 * Cellを返す。
	 * @param rowIndex 行数
	 * @param sheet 作業シート
	 * @return Cell
	 */
	private HSSFCell getCell(HSSFRow row, short columnIndex) {
		HSSFCell cell = row.getCell(columnIndex);
		if (cell == null) {
			cell = row.createCell(columnIndex);
		}
		return cell;
	}
// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD END

	/**
	 * @return targetExamLabel を戻します。
	 */
	public String getTargetExamLabel() {
		return CONFIG.getString("TARGET_EXAM_LABEL");
	}

	/**
	 * @return targetExamLabel を戻します。
	 */
	public String getThisExamLabel() {
		return CONFIG.getString("THIS_EXAM_LABEL");
	}

	/**
	 * @return targetExamLabel を戻します。
	 */
	public String getLastExamLabel() {
		return CONFIG.getString("LAST_EXAM_LABEL");
	}

	/**
	 * @return targetExamLabel を戻します。
	 */
	public String getLastBeforeExamLabel() {
		return CONFIG.getString("LAST_BEFORE_EXAM_LABEL");
	}
// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD START
	/**
	 * @return cefrTargetLabel を戻します。
	 */
	public String getCefrTargetLabel() {
		return CONFIG.getString("CEFR_TARGET_LABEL");
	}

	/**
	 * @return cefrMySchoolOnlyLabel を戻します。
	 */
	public String getCefrMySchoolOnlyLabel() {
		return CONFIG.getString("CEFR_MY_SCHOOL_ONLY_LABEL");
	}
// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD END
// 2019/09/30 QQ) 共通テスト対応 ADD START
	/**
	 * @return cefrMySchoolOnlyLabel を戻します。
	 */
	public String getCefrMySchoolOnlyLabel2() {
		return CONFIG.getString("CEFR_MY_SCHOOL_ONLY_LABEL2");
	}

	/**
	 * @return cefrMySchoolOnlyLabel を戻します。
	 */
	public String getCefrMySchoolOnlyLabel3() {
		return CONFIG.getString("CEFR_MY_SCHOOL_ONLY_LABEL3");
	}

	/**
	 * @return cefrSelectedSchoolOnlyLabel を戻します。
	 */
	public String getCefrSelectedSchoolOnlyLabel() {
		return CONFIG.getString("CEFR_SELECTED_SCHOOL_ONLY_LABEL");
	}
// 2019/09/30 QQ) 共通テスト対応 ADD END

	// 2019/08/27 Hics)Ueta 共通テスト対応 ADD START
	 /* 半角文字時空白埋め
	  *
	  */
	public  String padRight(String target,String charname, int length){
		int byteDiff = (getByteLength(target, Charset.forName(charname))-target.length())/2;
		return String.format("%-"+(length-byteDiff)+"s", target);
	}

	public  int getByteLength(String string, Charset charset) {
		return string.getBytes(charset).length;
	}
	// 2019/08/27 Hics)Ueta 共通テスト対応 ADD END
	//2019/09/11 QQ)Oosaki 共通テスト対応 ADD START
	/**
	 * CEFRレベル良いほうを取得
	 */
	public String cefr(String examyear, String examcd, String individualId, Connection conn) {
		String cefr = "";
		try {
			CefrBean cefrBean = new CefrBean();
			cefr = cefrBean.cefr(examyear, examcd, individualId, conn);
		} catch (Exception e) {
                    cefr = "-";
                    // 2019/10/10 QQ)Ooseto 共通テスト対応 ADD START
		    log.Err("","CEFRレベル取得エラー",e.toString());
		    // 2019/10/10 QQ)Ooseto 共通テスト対応 ADD END
		}
		return cefr;
	}
	//2019/09/11 QQ)Oosaki 共通テスト対応 ADD END

	// 2019/09/25 QQ)Ooseto 共通テスト対応 ADD START
	/**
	 * セキュリティスタンプ取得(行列セル結合対応)
	 * @param workbook Excel情報
	 * @param sheet Excelシート情報
	 * @param secuFlg セキュリティフラグ
	 * @param startRow 開始位置(行)
	 * @param endRow 終了位置(行)
	 * @param startCol 開始位置(列)
	 * @param endCol 終了位置(列)
	 * @return セキュリティスタンプ
	 */
	public String setSecurity2(HSSFWorkbook workbook, HSSFSheet sheet, int secuFlg, int startRow, int endRow, int startCol, int endCol ) {

		String securityStump = "";

		try {
			switch(secuFlg) {
				case 1:
					securityStump="機密";
					break;
				case 2:
					securityStump="部内限り";
					break;
				case 3:
					securityStump="校内限り";
					break;
				case 4:
					securityStump="取扱注意";
					break;
				case 11:
					securityStump="機密";
					break;
				case 12:
					securityStump="部外秘";
					break;
				case 13:
					securityStump="塾外秘";
					break;
				case 14:
					securityStump="取扱注意";
					break;
				default:
					securityStump="";
					break;
			}

			if (secuFlg != 5) {
				// 罫線設定（範囲指定）
				Region region = new Region((short)startRow, (short)startCol, (short)endRow, (short)endCol );
				// 範囲指定による太線設定
				try {
					HSSFRegionUtil.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM,region, sheet, workbook );
					HSSFRegionUtil.setBorderTop(HSSFCellStyle.BORDER_MEDIUM,region, sheet, workbook );
					HSSFRegionUtil.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM,region, sheet, workbook );
					HSSFRegionUtil.setBorderRight(HSSFCellStyle.BORDER_MEDIUM,region, sheet, workbook );
					// 範囲指定による線色設定
					HSSFRegionUtil.setBottomBorderColor(HSSFColor.RED.index, region, sheet, workbook);
					HSSFRegionUtil.setTopBorderColor(HSSFColor.RED.index, region, sheet, workbook);
					HSSFRegionUtil.setLeftBorderColor(HSSFColor.RED.index, region, sheet, workbook);
					HSSFRegionUtil.setRightBorderColor(HSSFColor.RED.index, region, sheet, workbook);
				} catch (Exception e) {
					log.Err("","セキュリティスタンプ罫線セットエラー",e.toString());
					// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL START
					//e.printStackTrace();
					// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL END
				}
			}

			return securityStump;

		} catch (Exception e) {
			log.Err("","セキュリティスタンプ取得エラー",e.toString());
			// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL START
			//e.printStackTrace();
			// 2019/10/10 QQ)Ooseto 共通テスト対応 DEL END
			return securityStump;
		}
	}
	// 2019/09/25 QQ)Ooseto 共通テスト対応 ADD END

}

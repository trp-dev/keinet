/**
 * 校内成績分析−クラス比較　偏差値分布-度数分布グラフ（人数）
 * 	Excelファイル編集
 * 作成日: 2004/08/09
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.cls;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C32BnpListBean;
import jp.co.fj.keinavi.excel.data.cls.C32ClassListBean;
import jp.co.fj.keinavi.excel.data.cls.C32Item;
import jp.co.fj.keinavi.excel.data.cls.C32ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class C32_03 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	final private int	intMaxSheetSr	= 40;			// 最大シート数	
	final private String	masterfile0		= "C32_03";		// ファイル名
	final private String	masterfile1		= "NC32_03";	// ファイル名
	private String	masterfile		= "";					// ファイル名
	/*
	 * 	Excel編集メイン
	 * 		C32Item c32Item: データクラス
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int c32_03EditExcel(C32Item c32Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("C32_03","C32_03帳票作成開始","");
		
		//テンプレートの決定
		if (c32Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			
			// データセット
			ArrayList	c32List			= c32Item.getC32List();
			Iterator	itr				= c32List.iterator();
			int		col				= 0;	// 列
			int 		setCol			= -1;	// *セット用
			int 		ninzu			= 0;	// *作成用
			int		classCnt		= 0;	// クラスカウンター
			int		maxSheetIndex	= 0;	// シートカウンター
			int		allClassCnt		= 0;	// 全クラスカウンター
			int		maxClassCnt		= 0;	// クラスカウンター
			int		intBookCngCount	= 0;	// ブックカウンター
			boolean	bolGakkouFlg	= true;// true:学校情報表示　（学校情報表示フラグ）
			boolean	bolFirstDataFlg	= true;// true:List.Next()を実行
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
//add 2004/10/26 T.Sakai データ0件対応
			int		dispClassFlgCnt		= 0;		// グラフ表示フラグカウンタ
//add end

			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			// 基本ファイルを読込む
			C32ListBean c32ListBean = new C32ListBean();

			while( itr.hasNext() ) {
				c32ListBean = (C32ListBean)itr.next();
				// グラフ表示フラグ判定
				if ( c32ListBean.getIntDispKmkFlg()==1 ) {
	
					// 基本ファイルを読み込む
					C32ClassListBean	c32ClassListBean	= new C32ClassListBean();
	
					//データの保持
					ArrayList	c32ClassList		= c32ListBean.getC32ClassList();
					Iterator	itrC32Class		= c32ClassList.iterator();

					// 表示クラス数取得
					maxClassCnt=0;
					while(itrC32Class.hasNext()){
						c32ClassListBean	= (C32ClassListBean) itrC32Class.next();
						if(c32ClassListBean.getIntDispClassFlg()==1){
							maxClassCnt++;
						}
					}
					itrC32Class		= c32ClassList.iterator();
	
					// 型・科目の最初のデータ(=学校データ)
					bolFirstDataFlg=true;
	
					//　クラスデータセット
					classCnt = 0;
					allClassCnt = 0;
					while(itrC32Class.hasNext()){
	
						// WorkBook作成
						if( bolBookCngFlg == true ){
							// マスタExcel読み込み
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if(workbook==null ){
								return errfread;
							}
							bolSheetCngFlg	= true;
							bolBookCngFlg	= false;
							maxSheetIndex	= 0;
						}
	
						// シート作成(初期値格納)
						if(bolSheetCngFlg){
							// データセットするシートの選択
							workSheet = workbook.getSheet(Integer.toString(maxSheetIndex+1));
							maxSheetIndex++;
	
							// ヘッダ右側に帳票作成日時を表示する
							cm.setHeader(workbook, workSheet);
	
							// セキュリティスタンプセット
							String secuFlg = cm.setSecurity( workbook, workSheet, c32Item.getIntSecuFlg() ,41 ,42 );
							workCell = cm.setCell( workSheet, workRow, workCell, 0, 41 );
							workCell.setCellValue(secuFlg);
	
							// 学校名セット
							workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
							workCell.setCellValue( "学校名　　　：" + cm.toString(c32Item.getStrGakkomei()) );
	
							// 対象模試セット
							String moshi =cm.setTaisyouMoshi( c32Item.getStrMshDate() );	// 模試月取得
							workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
							workCell.setCellValue( cm.getTargetExamLabel() + "　　：" + cm.toString(c32Item.getStrMshmei()) + moshi);
	
							// 型名・配点セット
							String haiten = "";
							if ( !cm.toString(c32ListBean.getStrHaitenKmk()).equals("") ) {
								haiten = "（" + c32ListBean.getStrHaitenKmk() + "）";
							}
							workCell = cm.setCell( workSheet, workRow, workCell, 5, 0 );
							workCell.setCellValue( "型・科目：" + cm.toString(c32ListBean.getStrKmkmei()) + haiten );
							
//add 2004/10/26 T.Sakai スペース対応
  							if (c32Item.getIntShubetsuFlg() == 1){
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
								workCell.setCellValue( "〜 19" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
								workCell.setCellValue( " 20〜\n 29" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
								workCell.setCellValue( " 30〜\n 39" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
								workCell.setCellValue( " 40〜\n 49" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
								workCell.setCellValue( " 50〜\n 59" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
								workCell.setCellValue( " 60〜\n 69" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
								workCell.setCellValue( " 70〜\n 79" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
								workCell.setCellValue( " 80〜\n 89" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
								workCell.setCellValue( " 90〜\n 99" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
								workCell.setCellValue( "100〜\n109" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
								workCell.setCellValue( "110〜\n119" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
								workCell.setCellValue( "120〜\n129" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
								workCell.setCellValue( "130〜\n139" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
								workCell.setCellValue( "140〜\n149" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
								workCell.setCellValue( "150〜\n159" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
								workCell.setCellValue( "160〜\n169" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
								workCell.setCellValue( "170〜\n179" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
								workCell.setCellValue( "180〜\n189" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
								workCell.setCellValue( "190〜" );
  							} else{
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
								workCell.setCellValue( "〜32.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
								workCell.setCellValue( "32.5〜\n34.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
								workCell.setCellValue( "35.0〜\n37.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
								workCell.setCellValue( "37.5〜\n39.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
								workCell.setCellValue( "40.0〜\n42.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
								workCell.setCellValue( "42.5〜\n44.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
								workCell.setCellValue( "45.0〜\n47.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
								workCell.setCellValue( "47.5〜\n49.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
								workCell.setCellValue( "50.0〜\n52.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
								workCell.setCellValue( "52.5〜\n54.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
								workCell.setCellValue( "55.0〜\n57.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
								workCell.setCellValue( "57.5〜\n59.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
								workCell.setCellValue( "60.0〜\n62.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
								workCell.setCellValue( "62.5〜\n64.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
								workCell.setCellValue( "65.0〜\n67.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
								workCell.setCellValue( "67.5〜\n69.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
								workCell.setCellValue( "70.0〜\n72.4" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
								workCell.setCellValue( "72.5〜\n74.9" );
								workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
								workCell.setCellValue( "75.0〜" );
  							}
//add end
							
							bolSheetCngFlg=false;
							bolGakkouFlg=true;
						}
	
						//　学校フラグ判定(trueの時は学校情報表示：falseの時はクラス情報表示)
						if(bolGakkouFlg){
							c32ClassListBean	= (C32ClassListBean) c32ClassList.get(0);

							// 学校情報読み飛ばし
							if(bolFirstDataFlg){
								c32ClassListBean	= (C32ClassListBean) itrC32Class.next();
								bolFirstDataFlg=false;
							}
						}else{
							// クラス情報取得
							c32ClassListBean	= (C32ClassListBean) itrC32Class.next();
						}

						// データ表示フラグ判定
						if ( c32ClassListBean.getIntDispClassFlg()==1 ) {
//add 2004/10/26 T.Sakai データ0件対応
							dispClassFlgCnt++;
//add end
							C32BnpListBean c32BnpListBean = new C32BnpListBean();
							ArrayList	c32BnpList	= c32ClassListBean.getC32BnpList();
							Iterator	itrC32Bnp	= c32BnpList.iterator();
		
							// 偏差値データセット
							col=0;
							while(itrC32Bnp.hasNext()){
								c32BnpListBean = (C32BnpListBean) itrC32Bnp.next();
		
								if(col==0){
									if(bolGakkouFlg){
										// 高校名セット
										workCell = cm.setCell( workSheet, workRow, workCell, 51, 1 );
										workCell.setCellValue( c32Item.getStrGakkomei() );
										workCell = cm.setCell( workSheet, workRow, workCell, 56, 1 );
										workCell.setCellValue( c32Item.getStrGakkomei() );
		
										bolGakkouFlg=false;
									}else{
										// クラス名セット
										workCell = cm.setCell( workSheet, workRow, workCell, 51+classCnt, 1 );
										workCell.setCellValue( cm.toString(c32ClassListBean.getStrGrade())+"年 "+cm.toString(c32ClassListBean.getStrClass())+"クラス" );
										workCell = cm.setCell( workSheet, workRow, workCell, 56+classCnt, 1 );
										workCell.setCellValue( cm.toString(c32ClassListBean.getStrGrade())+"年 "+cm.toString(c32ClassListBean.getStrClass())+"クラス" );
									}
		
									// 合計人数セット
									if(c32ClassListBean.getIntNinzu()!=-999){
										workCell = cm.setCell( workSheet, workRow, workCell, 51+classCnt, 40 );
										workCell.setCellValue( c32ClassListBean.getIntNinzu() );
									}
		
									// 平均点セット
									if(c32ClassListBean.getFloHeikin()!=-999.0){
										workCell = cm.setCell( workSheet, workRow, workCell, 51+classCnt, 41 );
										workCell.setCellValue( c32ClassListBean.getFloHeikin() );
									}
		
									// 平均偏差値セット
									if(c32ClassListBean.getFloHensa()!=-999.0){
										workCell = cm.setCell( workSheet, workRow, workCell, 51+classCnt, 42 );
										workCell.setCellValue( c32ClassListBean.getFloHensa() );
									}
								}
		
								// 人数セット
								if(c32BnpListBean.getIntNinzu()!=-999){
									workCell = cm.setCell( workSheet, workRow, workCell, 51+classCnt, 39-2*col );
									workCell.setCellValue( c32BnpListBean.getIntNinzu() );
		
									// *セット準備
									if( c32BnpListBean.getIntNinzu() > ninzu ){
										ninzu = c32BnpListBean.getIntNinzu();
										setCol = col;
									}
								}
		
								// 構成比セット
								if(c32BnpListBean.getFloKoseihi()!=-999.0){
									workCell = cm.setCell( workSheet, workRow, workCell, 56+classCnt, 39-2*col );
									workCell.setCellValue( c32BnpListBean.getFloKoseihi() );
								}
								
								col++;
		
								// *セット
								if(col >=19){
									if(setCol!=-1){
										workCell = cm.setCell( workSheet, workRow, workCell,  51+classCnt, 38-2*setCol );
										workCell.setCellValue("*");
									}
									ninzu=0;
									setCol=-1;
									break;
								}
		
							}

							classCnt++;
							allClassCnt++;

							// 以降にクラス情報がなければループ脱出
							if(maxClassCnt==allClassCnt){
								break;
							}
	
							// クラス情報が５件になったら
							if(classCnt>=5){
								classCnt=0;
								allClassCnt--;
								bolSheetCngFlg=true;
							}

							// ファイルを閉じる処理
							if(bolSheetCngFlg){
								if((maxSheetIndex >= intMaxSheetSr)&&(itrC32Class.hasNext())){
									bolBookCngFlg = true;
								}
		
						
								if( bolBookCngFlg == true){
									boolean bolRet = false;
									// Excelファイル保存
									if(intBookCngCount==0){
										if(itr.hasNext() == true){
											bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
										}
										else{
											bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
										}
									}else{
										bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
									}
		
									if( bolRet == false ){
										return errfwrite;
									}
									intBookCngCount++;
								}
							}
						}

						// 学校が非表示指定(表示フラグ≠１)だった場合（エラー）
						bolGakkouFlg=false;
					}


					// ファイルを閉じる処理
					if( itr.hasNext() ){
						classCnt=0;
						bolSheetCngFlg=true;
	
						if(maxSheetIndex >= intMaxSheetSr){
							bolBookCngFlg = true;
						}
	
					
						if( bolBookCngFlg == true){
	
							boolean bolRet = false;
							// Excelファイル保存
							if(intBookCngCount==0){
								if(itr.hasNext() == true){
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
								}
								else{
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
								}
							}else{
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
							}
	
							if( bolRet == false ){
								return errfwrite;
							}
							intBookCngCount++;
						}
					}
				}

			}
//add 2004/10/26 T.Sakai データ0件対応
			if ( dispClassFlgCnt==0 ) {
				// マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				bolBookCngFlg	= false;
				maxSheetIndex	= 0;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet( Integer.toString(maxSheetIndex+1) );
				maxSheetIndex++;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);

				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, c32Item.getIntSecuFlg() ,41 ,42 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 41 );
				workCell.setCellValue(secuFlg);
	
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　　　：" + cm.toString(c32Item.getStrGakkomei()) );
	
				// 対象模試セット
				String moshi =cm.setTaisyouMoshi( c32Item.getStrMshDate() );	// 模試月取得
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "　　：" + cm.toString(c32Item.getStrMshmei()) + moshi);
				
//add 2004/10/26 T.Sakai スペース対応
  				if (c32Item.getIntShubetsuFlg() == 1){
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
					workCell.setCellValue( "〜 19" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
					workCell.setCellValue( " 20〜\n 29" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
					workCell.setCellValue( " 30〜\n 39" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
					workCell.setCellValue( " 40〜\n 49" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
					workCell.setCellValue( " 50〜\n 59" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
					workCell.setCellValue( " 60〜\n 69" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
					workCell.setCellValue( " 70〜\n 79" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
					workCell.setCellValue( " 80〜\n 89" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
					workCell.setCellValue( " 90〜\n 99" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
					workCell.setCellValue( "100〜\n109" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
					workCell.setCellValue( "110〜\n119" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
					workCell.setCellValue( "120〜\n129" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
					workCell.setCellValue( "130〜\n139" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
					workCell.setCellValue( "140〜\n149" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
					workCell.setCellValue( "150〜\n159" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
					workCell.setCellValue( "160〜\n169" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
					workCell.setCellValue( "170〜\n179" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
					workCell.setCellValue( "180〜\n189" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
					workCell.setCellValue( "190〜" );
  				} else{
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 2 );
					workCell.setCellValue( "〜32.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 4 );
					workCell.setCellValue( "32.5〜\n34.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 6 );
					workCell.setCellValue( "35.0〜\n37.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 8 );
					workCell.setCellValue( "37.5〜\n39.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 10 );
					workCell.setCellValue( "40.0〜\n42.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 12 );
					workCell.setCellValue( "42.5〜\n44.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 14 );
					workCell.setCellValue( "45.0〜\n47.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 16 );
					workCell.setCellValue( "47.5〜\n49.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 18 );
					workCell.setCellValue( "50.0〜\n52.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 20 );
					workCell.setCellValue( "52.5〜\n54.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 22 );
					workCell.setCellValue( "55.0〜\n57.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 24 );
					workCell.setCellValue( "57.5〜\n59.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 26 );
					workCell.setCellValue( "60.0〜\n62.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 28 );
					workCell.setCellValue( "62.5〜\n64.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 30 );
					workCell.setCellValue( "65.0〜\n67.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 32 );
					workCell.setCellValue( "67.5〜\n69.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 34 );
					workCell.setCellValue( "70.0〜\n72.4" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 36 );
					workCell.setCellValue( "72.5〜\n74.9" );
					workCell = cm.setCell( workSheet, workRow, workCell, 50, 38 );
					workCell.setCellValue( "75.0〜" );
  				}
//add end
			}
//add end
			// ファイルを閉じる処理
			if( bolBookCngFlg == false){

				boolean bolRet = false;
				// Excelファイル保存
				if(intBookCngCount!=0){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}
			
		} catch(Exception e) {
			log.Err("C32_03","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("C32_03","C32_03帳票作成終了","");
		return noerror;
	}

}
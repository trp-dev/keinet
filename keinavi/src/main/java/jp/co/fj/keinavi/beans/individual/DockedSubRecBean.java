/*
 * 作成日: 2004/10/08
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.SQLException;
import java.util.List;

import jp.co.fj.keinavi.data.ExamSession;

import com.fjh.beans.DefaultBean;

/**
 * @author T.Yamada
 * ドッキング元の模試年度とコードからドッキング先の模試を取得して
 * それぞれの成績を取得する
 */
public class DockedSubRecBean extends DefaultBean {

	//TODO このクラスは変更した方がよい

	/**
	 * 
	 */
	private String individualId;
	/**
	 * 
	 */
	private String examYear;
	/**
	 * 
	 */
	private String examCd;
	/**
	 * 
	 */
	private ExamSession examSession;
	
	/**
	 * for out
	 * マーク模試の成績リスト(SubRecordDataのリスト)
	 */
	private List mark;
	
	/**
	 * for out
	 * 記述模試の成績リスト(SubRecordDataのリスト)
	 */
	private List wrtn;
	
	/**
	 * for out
	 * ドッキング元・先を保持する
	 * マーク: dockedBean.getExaminationDatas()[0]
	 * 記述  : dockedBean.getExaminationDatas()[1]
	 */
	private DockedExamsBean dockedBean;
	
	/**
	 * 除外する模試コード
	 */
	private String[] excludeExamCds;
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		//DockedExamの取得
		dockedBean = new DockedExamsBean();
		dockedBean.setExamYear(examYear);
		dockedBean.setExamCd(examCd);
		dockedBean.setExamSession(examSession);
		dockedBean.setConnection("", conn);
		dockedBean.execute();
		
		//この試験が除外するものならnullにする
		if(excludeExamCds != null && excludeExamCds.length >0){
			for(int i=0; i<excludeExamCds.length; i++){
				if(dockedBean.getExaminationDatas()[0] != null && dockedBean.getExaminationDatas()[0].getExamCd().equals(excludeExamCds[i])){
					dockedBean.getExaminationDatas()[0] = null;
				}
				if(dockedBean.getExaminationDatas()[1] != null && dockedBean.getExaminationDatas()[1].getExamCd().equals(excludeExamCds[i])){
					dockedBean.getExaminationDatas()[1] = null;
				}
			}
		}
		
		//マーク模試の成績取得
		mark = null;
		if(dockedBean.getExaminationDatas()[0] != null){
			//System.out.println("[0]cd : "+dockedBean.getExaminationDatas()[0].getExamCd());	
			SubRecordSearchBean rb = new SubRecordSearchBean();
			rb.setIndividualId(individualId);
			rb.setExamYear(dockedBean.getExaminationDatas()[0].getExamYear());
			rb.setExamCd(dockedBean.getExaminationDatas()[0].getExamCd());
			rb.setConnection("", conn);
			rb.execute();
			if(rb.getRecordSet() != null && rb.getRecordSet().size() > 0){
				//System.out.println("[0]size : "+rb.getRecordSet().size());
				mark = rb.getRecordSet();	
			}
		}
		//記述模試の成績取得
		wrtn = null;
		if(dockedBean.getExaminationDatas()[1] != null){
			SubRecordSearchBean rb = new SubRecordSearchBean();
			rb.setIndividualId(individualId);
			rb.setExamYear(dockedBean.getExaminationDatas()[1].getExamYear());
			rb.setExamCd(dockedBean.getExaminationDatas()[1].getExamCd());
			rb.setConnection("", conn);
			rb.execute();
			if(rb.getRecordSet() != null && rb.getRecordSet().size() > 0){
				//System.out.println("[1]size : "+rb.getRecordSet().size());
				wrtn = rb.getRecordSet();	
			}
		}
		
	}

	/**
	 * @return
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * @return
	 */
	public ExamSession getExamSession() {
		return examSession;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @return
	 */
	public List getMark() {
		return mark;
	}

	/**
	 * @return
	 */
	public List getWrtn() {
		return wrtn;
	}

	/**
	 * @param string
	 */
	public void setExamCd(String string) {
		examCd = string;
	}

	/**
	 * @param session
	 */
	public void setExamSession(ExamSession session) {
		examSession = session;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

	/**
	 * @param list
	 */
	public void setMark(List list) {
		mark = list;
	}

	/**
	 * @param list
	 */
	public void setWrtn(List list) {
		wrtn = list;
	}

	/**
	 * @return
	 */
	public DockedExamsBean getDockedBean() {
		return dockedBean;
	}

	/**
	 * @param bean
	 */
	public void setDockedBean(DockedExamsBean bean) {
		dockedBean = bean;
	}

	/**
	 * @return
	 */
	public String[] getExcludeExamCds() {
		return excludeExamCds;
	}

	/**
	 * @param strings
	 */
	public void setExcludeExamCds(String[] strings) {
		excludeExamCds = strings;
	}

}

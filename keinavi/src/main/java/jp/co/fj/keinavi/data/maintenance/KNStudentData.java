package jp.co.fj.keinavi.data.maintenance;

import java.util.Date;

/**
 *
 * 生徒情報データ
 * 
 * 2005.11.24	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class KNStudentData {

	private final int yearDiff;
	
	// 対象年度
	private String year;
	// 今年度学年
	private int currentGrade = -1;
	// 今年度クラス
	private String currentClassName;
	// 今年度クラス番号
	private String currentClassNo;
	// 性別
	private String sex;
	// カタカナ氏名
	private String nameKana;
	// ひらがな氏名
	private String nameHiragana;
	// 漢字氏名
	private String nameKanji;
	// 生年月日
	private Date birthday;
	// 電話番号
	private String telNo;
	// 前年度学年
	private int lastGrade = -1;
	// 前年度クラス
	private String lastClassName;
	// 前年度クラス番号
	private String lastClassNo;
	// 前々年度学年
	private int beforeLastGrade = -1;
	// 前々年度クラス
	private String beforeLastClassName;
	// 前々年度クラス番号
	private String beforeLastClassNo;
	// 個人ID
	private String individualId;
	
	// 変更前の今年度学年
	private int oCurrentGrade = - 1;
	// 変更前の今年度クラス
	private String oCurrentClassName;
	// 変更前の前年度学年
	private int oLastGrade = -1;
	// 変更前の前年度クラス
	private String oLastClassName;
	// 変更前の前々年度学年
	private int oBeforeLastGrade = -1;
	// 変更前の前々年度クラス
	private String oBeforeLastClassName;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pYearDiff 対象年度と現年度との差
	 */
	public KNStudentData(final int pYearDiff) {
		
		// 過去３年しかありえない
		if (pYearDiff < 0 || pYearDiff > 3) {
			throw new IllegalArgumentException(
					"不正な年度の指定です。");
		}
		
		this.yearDiff = pYearDiff;
	}
	
	/**
	 * 対象年度の学年を取得する
	 * 
	 * @return grade を戻します。
	 */
	public int getGrade() {
		
		if (yearDiff == 1) {
			return getLastGrade();
		} else if (yearDiff == 2) {
			return getBeforeLastGrade();
		} else {
			return getCurrentGrade();
		}
	}
	
	/**
	 * 対象年度のクラス名を取得する
	 * 
	 * @return className を戻します。
	 */
	public String getClassName() {
		
		if (yearDiff == 1) {
			return getLastClassName();
		} else if (yearDiff == 2) {
			return getBeforeLastClassName();
		} else {
			return getCurrentClassName();
		}
	}
	
	/**
	 * 対象年度のクラス番号を取得する
	 * 
	 * @return classNo を戻します。
	 */
	public String getClassNo() {
		
		if (yearDiff == 1) {
			return getLastClassNo();
		} else if (yearDiff == 2) {
			return getBeforeLastClassNo();
		} else {
			return getCurrentClassNo();
		}
	}
	
	/**
	 * 対象年度の学年を設定する
	 * 
	 * @param pGrade 設定する grade。
	 */
	public void setGrade(final int pGrade) {
		
		if (yearDiff == 1) {
			setLastGrade(pGrade);
		} else if (yearDiff == 2) {
			setBeforeLastGrade(pGrade);
		} else {
			setCurrentGrade(pGrade);
		}
	}
	
	/**
	 * 対象年度のクラス名を設定する
	 * 
	 * @param pClassName 設定する className。
	 */
	public void setClassName(final String pClassName) {
		
		if (yearDiff == 1) {
			setLastClassName(pClassName);
		} else if (yearDiff == 2) {
			setBeforeLastClassName(pClassName);
		} else {
			setCurrentClassName(pClassName);
		}
	}
	
	/**
	 * 対象年度のクラス番号を設定する
	 * 
	 * @param pClassNo 設定する classNo。
	 */
	public void setClassNo(final String pClassNo) {
		
		if (yearDiff == 1) {
			setLastClassNo(pClassNo);
		} else if (yearDiff == 2) {
			setBeforeLastClassNo(pClassNo);
		} else {
			setCurrentClassNo(pClassNo);
		}
	}
	
	/**
	 * @return beforeLastClassName を戻します。
	 */
	public String getBeforeLastClassName() {
		return beforeLastClassName;
	}
	/**
	 * @param s 設定する beforeLastClassName。
	 */
	public void setBeforeLastClassName(final String s) {
		this.beforeLastClassName = s;
	}
	/**
	 * @return beforeLastClassNo を戻します。
	 */
	public String getBeforeLastClassNo() {
		return beforeLastClassNo;
	}
	/**
	 * @param s 設定する beforeLastClassNo。
	 */
	public void setBeforeLastClassNo(final String s) {
		this.beforeLastClassNo = s;
	}
	/**
	 * @return beforeLastGrade を戻します。
	 */
	public int getBeforeLastGrade() {
		return beforeLastGrade;
	}
	/**
	 * @param i 設定する beforeLastGrade。
	 */
	public void setBeforeLastGrade(final int i) {
		this.beforeLastGrade = i;
	}
	/**
	 * @return birthday を戻します。
	 */
	public Date getBirthday() {
		return birthday;
	}
	/**
	 * @param d 設定する birthday。
	 */
	public void setBirthday(final Date d) {
		this.birthday = d;
	}
	/**
	 * @return individualId を戻します。
	 */
	public String getIndividualId() {
		return individualId;
	}
	/**
	 * @param s 設定する individualId。
	 */
	public void setIndividualId(final String s) {
		this.individualId = s;
	}
	/**
	 * @return lastClassName を戻します。
	 */
	public String getLastClassName() {
		return lastClassName;
	}
	/**
	 * @param s 設定する lastClassName。
	 */
	public void setLastClassName(final String s) {
		this.lastClassName = s;
	}
	/**
	 * @return lastClassNo を戻します。
	 */
	public String getLastClassNo() {
		return lastClassNo;
	}
	/**
	 * @param s 設定する lastClassNo。
	 */
	public void setLastClassNo(final String s) {
		this.lastClassNo = s;
	}
	/**
	 * @return lastGrade を戻します。
	 */
	public int getLastGrade() {
		return lastGrade;
	}
	/**
	 * @param i 設定する lastGrade。
	 */
	public void setLastGrade(final int i) {
		this.lastGrade = i;
	}
	/**
	 * @return nameKana を戻します。
	 */
	public String getNameKana() {
		return nameKana;
	}
	/**
	 * @param s 設定する nameKana。
	 */
	public void setNameKana(final String s) {
		this.nameKana = s;
	}
	/**
	 * @return nameKanji を戻します。
	 */
	public String getNameKanji() {
		return nameKanji;
	}
	/**
	 * @param s 設定する nameKanji。
	 */
	public void setNameKanji(final String s) {
		this.nameKanji = s;
	}
	/**
	 * @return sex を戻します。
	 */
	public String getSex() {
		return sex;
	}
	/**
	 * @param s 設定する sex。
	 */
	public void setSex(final String s) {
		this.sex = s;
	}
	/**
	 * @return telNo を戻します。
	 */
	public String getTelNo() {
		return telNo;
	}
	/**
	 * @param s 設定する telNo。
	 */
	public void setTelNo(final String s) {
		this.telNo = s;
	}
	/**
	 * @return year を戻します。
	 */
	public String getYear() {
		return year;
	}
	/**
	 * @param s 設定する year。
	 */
	public void setYear(final String s) {
		this.year = s;
	}
	/**
	 * @return nameHiragana を戻します。
	 */
	public String getNameHiragana() {
		return nameHiragana;
	}
	/**
	 * @param s 設定する nameHiragana。
	 */
	public void setNameHiragana(final String s) {
		this.nameHiragana = s;
	}

	/**
	 * @return currentClassName を戻します。
	 */
	public String getCurrentClassName() {
		return currentClassName;
	}

	/**
	 * @param pCurrentClassName 設定する currentClassName。
	 */
	public void setCurrentClassName(final String pCurrentClassName) {
		this.currentClassName = pCurrentClassName;
	}

	/**
	 * @return currentClassNo を戻します。
	 */
	public String getCurrentClassNo() {
		return currentClassNo;
	}

	/**
	 * @param pCurrentClassNo 設定する currentClassNo。
	 */
	public void setCurrentClassNo(final String pCurrentClassNo) {
		this.currentClassNo = pCurrentClassNo;
	}

	/**
	 * @return currentGrade を戻します。
	 */
	public int getCurrentGrade() {
		return currentGrade;
	}

	/**
	 * @param pCurrentGrade 設定する currentGrade。
	 */
	public void setCurrentGrade(final int pCurrentGrade) {
		this.currentGrade = pCurrentGrade;
	}

	/**
	 * @return oBeforeLastClassName を戻します。
	 */
	public String getOBeforeLastClassName() {
		return oBeforeLastClassName;
	}

	/**
	 * @param pBeforeLastClassName 設定する oBeforeLastClassName。
	 */
	public void setOBeforeLastClassName(final String pBeforeLastClassName) {
		oBeforeLastClassName = pBeforeLastClassName;
	}

	/**
	 * @return oBeforeLastGrade を戻します。
	 */
	public int getOBeforeLastGrade() {
		return oBeforeLastGrade;
	}

	/**
	 * @param pBeforeLastGrade 設定する oBeforeLastGrade。
	 */
	public void setOBeforeLastGrade(final int pBeforeLastGrade) {
		oBeforeLastGrade = pBeforeLastGrade;
	}

	/**
	 * @return oCurrentClassName を戻します。
	 */
	public String getOCurrentClassName() {
		return oCurrentClassName;
	}

	/**
	 * @param pCurrentClassName 設定する oCurrentClassName。
	 */
	public void setOCurrentClassName(final String pCurrentClassName) {
		oCurrentClassName = pCurrentClassName;
	}

	/**
	 * @return oCurrentGrade を戻します。
	 */
	public int getOCurrentGrade() {
		return oCurrentGrade;
	}

	/**
	 * @param pCurrentGrade 設定する oCurrentGrade。
	 */
	public void setOCurrentGrade(final int pCurrentGrade) {
		oCurrentGrade = pCurrentGrade;
	}

	/**
	 * @return oLastClassName を戻します。
	 */
	public String getOLastClassName() {
		return oLastClassName;
	}

	/**
	 * @param pLastClassName 設定する oLastClassName。
	 */
	public void setOLastClassName(final String pLastClassName) {
		oLastClassName = pLastClassName;
	}

	/**
	 * @return oLastGrade を戻します。
	 */
	public int getOLastGrade() {
		return oLastGrade;
	}

	/**
	 * @param pLastGrade 設定する oLastGrade。
	 */
	public void setOLastGrade(final int pLastGrade) {
		oLastGrade = pLastGrade;
	}

}

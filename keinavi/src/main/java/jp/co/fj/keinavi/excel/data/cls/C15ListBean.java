package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * �N���X���ъT���|�u�]��w�ʌl�]���f�[�^���X�g
 * �쐬��: 2004/08/30
 * @author	Ito.Y
 */
public class C15ListBean {
	//�w�N
	private String strGrade = "";
	//�N���X
	private String strClass = "";
	//�u�]��w���X�g
	private ArrayList c15ShiboList = new ArrayList();

	/*----------*/
	/* Get      */
	/*----------*/
	public ArrayList getC15ShiboList() {
		return c15ShiboList;
	}
	public String getStrClass() {
		return strClass;
	}
	public String getStrGrade() {
		return strGrade;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setC15ShiboList(ArrayList list) {
		c15ShiboList = list;
	}
	public void setStrClass(String string) {
		strClass = string;
	}
	public void setStrGrade(String string) {
		strGrade = string;
	}

}
package jp.co.fj.keinavi.util;

/**
 * 指定されたプロパティファイルから値を取得する。
 *
 * @Date	2005/06/17
 * @author	TOTEC)Nishiyama
 */
public class KNProperty {
	
	/**
	 * プロパティファイルのキー
	 */
	private String PropKey;
	
	/**
	 * KNPropertyクラスのインスタンスを取得する。
	 *
	 * @param FilePath	ファイル名
	 * @return KNPropertyクラスのインスタンス
	 */
	public static KNProperty getInstance(String FilePath) {
		KNProperty Prop = new KNProperty();
		Prop.PropKey = FilePath;
		
		return Prop;
	}
		
	/**
	 * 値をStringで返す
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @return    		取得値
	 */
	public static String getStrProperty(String FilePath,
																			String ParamKey) throws Exception {
		return KNPropertyCtrl.GetString(FilePath, ParamKey);
	}

	/**
	 * 値をString配列で返す
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @return    		取得値
	 */
	public static String[] getStrProperties(String FilePath, 
																					String ParamKey) throws Exception{
		return KNPropertyCtrl.GetStringList(FilePath, ParamKey);
	}
		
	/**
	 * 値をIntで返す
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @return    		取得値
	 */
	public static int getIntProperty(String FilePath,
																		String ParamKey) throws Exception{
		return KNPropertyCtrl.GetInt(FilePath, ParamKey);
	}

	/**
	 * 値をint配列で返す
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @return    		取得値
	 */
	public static int[] getIntProperties(String FilePath,
																				String ParamKey) throws Exception{
		return KNPropertyCtrl.GetIntList(FilePath, ParamKey);
	}

	/**
	 * Stringの値を設定する
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @param Parameter	値
	 */
	public static synchronized void setStrProperty(String FilePath,
																										String ParamKey,
																										String Parameter) throws Exception{
		KNPropertyCtrl.SetString(FilePath, ParamKey, Parameter);
	}

	/**
	 * Stringの値を設定する
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @param Parameter	値
	 */
	public static synchronized void setStrProperties(String FilePath,
																											String ParamKey,
																											String Parameter) throws Exception{
		KNPropertyCtrl.SetString(FilePath, ParamKey, Parameter);
	}
		
	/**
	 * intの値を設定する
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @param Parameter	値
	 */
	public static synchronized void setIntProperty(String FilePath,
																										String ParamKey,
																										int Parameter) throws Exception{
		KNPropertyCtrl.SetInt(FilePath, ParamKey, Parameter);
	}

	/**
	 * intの値を設定する
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @param Parameter	値
	 */
	public static synchronized void setIntProperties(String FilePath,
																											String ParamKey,
																											int Parameter) throws Exception{
		KNPropertyCtrl.SetInt(FilePath, ParamKey, Parameter);
	}
	
	/**
	 * 値をStringで返す
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @return    		取得値
	 */
	public String getStrProperty(String ParamKey) throws Exception {
		return KNPropertyCtrl.GetString(PropKey, ParamKey);
	}

	/**
	 * 値をString配列で返す
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @return    		取得値
	 */
	public String[] getStrProperties(String ParamKey) throws Exception{
		return KNPropertyCtrl.GetStringList(PropKey, ParamKey);
	}
		
	/**
	 * 値をIntで返す
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @return    		取得値
	 */
	public int getIntProperty(String ParamKey) throws Exception{
		return KNPropertyCtrl.GetInt(PropKey, ParamKey);
	}

	/**
	 * 値をint配列で返す
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @return    		取得値
	 */
	public int[] getIntProperties(String ParamKey) throws Exception{
		return KNPropertyCtrl.GetIntList(PropKey, ParamKey);
	}

	/**
	 * Stringの値を設定する
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @param Parameter	値
	 */
	public synchronized void setStrProperty(String ParamKey,
																						String Parameter) throws Exception{
		KNPropertyCtrl.SetString(PropKey, ParamKey, Parameter);
	}

	/**
	 * Stringの値を設定する
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @param Parameter	値
	 */
	public synchronized void setStrProperties(String ParamKey,
																							String Parameter) throws Exception{
		KNPropertyCtrl.SetString(PropKey, ParamKey, Parameter);
	}
	
	/**
	 * intの値を設定する
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @param Parameter	値
	 */
	public synchronized void setIntProperty(String ParamKey,
																						int Parameter) throws Exception{
		KNPropertyCtrl.SetInt(PropKey, ParamKey, Parameter);
	}

	/**
	 * intの値を設定する
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @param Parameter	値
	 */
	public synchronized void setIntProperties(String ParamKey,
																							int Parameter) throws Exception{
		KNPropertyCtrl.SetInt(PropKey, ParamKey, Parameter);
	}
}
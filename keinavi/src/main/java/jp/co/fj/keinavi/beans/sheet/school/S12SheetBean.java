/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.excel.data.school.S12BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S12Item;
import jp.co.fj.keinavi.excel.data.school.S12ListBean;
import jp.co.fj.keinavi.excel.school.S12;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 *
 * @author kawai
 *
 */
public class S12SheetBean extends AbstractSheetBean {

	// データクラス
	private final S12Item data = new S12Item();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		String[] code = super.getSubjectCDArray(); // 型・科目設定値
		List graph = super.getSubjectGraphList(); // 型・科目グラフ表示対象

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ

		// 表
		switch (super.getIntFlag(IProfileItem.CHART_COMP_RATIO)) {
			case  1: data.setIntHyouFlg(0); data.setIntKoseihiFlg(1); break;
			case  2: data.setIntHyouFlg(0); data.setIntKoseihiFlg(2); break;
			case 11: data.setIntHyouFlg(1); data.setIntKoseihiFlg(1); break;
			case 12: data.setIntHyouFlg(1); data.setIntKoseihiFlg(2); break;
		}

		// 偏差値帯別度数分布グラフ
		switch (super.getIntFlag(IProfileItem.GRAPH_DIST)) {
			case  1: data.setIntBnpGraphFlg(0); data.setIntAxisFlg(1); break;
			case  2: data.setIntBnpGraphFlg(0); data.setIntAxisFlg(2); break;
			case 11: data.setIntBnpGraphFlg(1); data.setIntAxisFlg(1); break;
			case 12: data.setIntBnpGraphFlg(1); data.setIntAxisFlg(2); break;
		}

		// 偏差値帯別人数積み上げグラフ
		switch (super.getIntFlag(IProfileItem.GRAPH_BUILDUP)) {
			case  1: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(1); break;
			case  2: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(2); break;
			case  3: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(3); break;
			case 11: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(1); break;
			case 12: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(2); break;
			case 13: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(3); break;
		}

		// 偏差値帯別構成比グラフ
		switch (super.getIntFlag(IProfileItem.GRAPH_COMP_RATIO)) {
			case  1: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(1); break;
			case  2: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(2); break;
			case  3: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(3); break;
			case 11: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(1); break;
			case 12: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(2); break;
			case 13: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(3); break;
		}

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//		// 2019/07/23 QQ)Tanioka 共通テスト対応 ADD START
//		// 共通テスト英語認定試験CEFR取得状況
//		switch (super.getIntFlag(IProfileItem.OPTION_CHECK_BOX)) {
//			case  1: data.setIntCheckBoxFlg(0); data.setIntTargetCheckBoxFlg(1); break;
//			case  2: data.setIntCheckBoxFlg(0); data.setIntTargetCheckBoxFlg(2); break;
//			case 11: data.setIntCheckBoxFlg(1); data.setIntTargetCheckBoxFlg(1); break;
//			case 12: data.setIntCheckBoxFlg(1); data.setIntTargetCheckBoxFlg(2); break;
//		}
//		// 2019/07/23 QQ)Tanioka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//		// 2019/09/30 QQ) 共通テスト対応 ADD START
//		PreparedStatement ps3 = null;
//		PreparedStatement ps4 = null;
//		PreparedStatement ps5 = null;
//		ResultSet rs3 = null;
//		ResultSet rs4 = null;
//		ResultSet rs5 = null;
//		// 2019/09/30 QQ) 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
		try {
			List c1 = new LinkedList(); // 型の入れ物
			List c2 = new LinkedList(); // 科目の入れ物

			// データリスト
			Query query = QueryLoader.getInstance().load("s12_1");
			query.setStringArray(1, code);

			ps1 = conn.prepareStatement(query.toString());
			ps1.setString(1, exam.getExamYear()); // 模試年度
			ps1.setString(2, exam.getExamCD()); // 模試コード
			ps1.setString(3, profile.getBundleCD()); // 一括コード

			// 偏差値分布データリスト
			ps2 = conn.prepareStatement(QueryLoader.getInstance().load("s12_2").toString());
			ps2.setString(1, exam.getExamYear()); // 模試年度
			ps2.setString(2, exam.getExamCD()); // 模試コード
			ps2.setString(3, profile.getBundleCD()); // 一括コード

			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				S12ListBean bean = new S12ListBean();
				bean.setStrKmkCd(rs1.getString(1));
				bean.setStrKmkmei(rs1.getString(2));
				bean.setIntDispKmkFlg(graph.contains(rs1.getString(1)) ? 1 : 0);
				bean.setStrHaitenKmk(rs1.getString(3));
				bean.setIntNinzu(rs1.getInt(4));
				bean.setFloHeikin(rs1.getFloat(5));
				bean.setFloHensa(rs1.getFloat(6));

				if (rs1.getInt(1) >= 7000) c1.add(bean); // 型
				else c2.add(bean); // 科目

				// 科目コード
				ps2.setString(4, bean.getStrKmkCd());

				rs2 = ps2.executeQuery();
				while (rs2.next()) {
					S12BnpListBean bnp = new S12BnpListBean();
					bnp.setFloBnpMax(rs2.getFloat(1));
					bnp.setFloBnpMin(rs2.getFloat(2));
					bnp.setIntNinzu(rs2.getInt(3));
					bnp.setFloKoseihi(rs2.getFloat(4));
					bean.getS12BnpList().add(bnp);
				}
				rs2.close();
			}

			// 型・科目の順番に詰める
			data.getS12List().addAll(c1);
			data.getS12List().addAll(c2);

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//			// 2019/09/30 QQ) 共通テスト対応 ADD START
//			if (data.getIntCheckBoxFlg() == 1) {
//				//S12_11_共通テスト英語認定試験CEFR取得状況
//				S12CefrMain cMain = new S12CefrMain();
//
//				// 名称データ取得
//				Query query3 = QueryLoader.getInstance().load("s12_11_1");
//				ps3 = conn.prepareStatement(query3.toString());
//				ps3.setString(1, exam.getExamYear());		// 模試年度
//				ps3.setString(2, exam.getExamCD());		// 模試コード
//				ps3.setString(3, exam.getExamYear());		// 模試年度
//				ps3.setString(4, exam.getExamCD());		// 模試コード
//				ps3.setString(5, profile.getBundleCD());	// 一括コード
//				int cnt = 0;
//				rs3 = ps3.executeQuery();
//				while (rs3.next()) {
//					if (cnt == 0) {
//						cMain.setStrBundleName(rs3.getString(1));	//学校名
//						cMain.setStrPrefCd(rs3.getString(2));		//県コード
//						cMain.setStrPrefName(rs3.getString(3));		//都道府県名
//						cMain.setIntExamCount(rs3.getInt(5));		//認定試験数
//					}
//					cnt++;
//					if ("".equals(rs3.getString(4)) == false && rs3.getString(4) != null) {
//						cMain.getS12EngPtCdList().add(rs3.getString(4));
//					}
//
//				}
//				rs3.close();
//
//				//全試験データ取得
//				Query query4 = QueryLoader.getInstance().load("s12_11_2");
//				ps4 = conn.prepareStatement(query4.toString());
//// 2019/11/06 QQ)Tanioka CEFR帳票が出力されない障害の修正。 UPD START
////				ps4.setString(1, exam.getExamYear());	// 模試年度
////				ps4.setString(2, exam.getExamCD());	// 模試コード
//				ps4.setString(1, profile.getBundleCD());	// 一括コード
//				ps4.setString(2, cMain.getStrPrefCd());		// 県コード
//				ps4.setString(3, exam.getExamYear());		// 模試年度
//				ps4.setString(4, exam.getExamCD());			//	模試コード
//// 2019/11/06 QQ)Tanioka CEFR帳票が出力されない障害の修正。 UPD END
//				rs4 = ps4.executeQuery();
//				while (rs4.next()) {
//					S12CefrItem item = new S12CefrItem();
//					item.setStrShortName("");
//					item.setStrCerfLevelNameAbbr(rs4.getString(1));
//					item.setIntNumbersS(    rs4.getInt  (2));
//					item.setFloCompRatioS(  rs4.getFloat(3));
//					item.setIntNumbersP(    rs4.getInt  (4));
//					item.setFloCompRatioP(  rs4.getFloat(5));
//					item.setIntNumbersA(    rs4.getInt  (6));
//					item.setFloCompRatioA(  rs4.getFloat(7));
//
//					cMain.getS12AllList().add(item);
//				}
//				rs4.close();
//
//				//各試験データ取得
//				Query query5 = QueryLoader.getInstance().load("s12_11_3");
//				ps5 = conn.prepareStatement(query5.toString());
//				ps5.setString(1, profile.getBundleCD());	// 一括コード
//				ps5.setString(2, cMain.getStrPrefCd());		// 県コード
//				ps5.setString(3, exam.getExamYear());		// 模試年度
//				ps5.setString(4, exam.getExamCD());		// 模試コード
//				rs5 = ps5.executeQuery();
//				while (rs5.next()) {
//					//自校の生徒が受験した試験を対象とするチェックボックス
//					if (data.getIntTargetCheckBoxFlg() == 1) {
//
//						String engPtCd = rs5.getString(3);
//						if (cMain.getS12EngPtCdList().size() == 0 ||
//							cMain.getS12EngPtCdList().contains(engPtCd) == false) {
//							//対象外の英語試験コードは除外
//							continue;
//						}
//					}
//
//					S12CefrItem item = new S12CefrItem();
//					item.setStrShortName(rs5.getString(1));
//					item.setStrCerfLevelNameAbbr(rs5.getString(2));
//					item.setStrEngPtCd(rs5.getString(3));
//					item.setIntNumbersS(rs5.getInt  (4));
//					item.setFloCompRatioS(rs5.getFloat(5));
//					item.setIntNumbersP(rs5.getInt  (6));
//					item.setFloCompRatioP(rs5.getFloat(7));
//					item.setIntNumbersA(rs5.getInt  (8));
//					item.setFloCompRatioA(rs5.getFloat(9));
//
//					cMain.getS12EachList().add(item);
//				}
//				rs5.close();
//
//				data.setS12CefrMain(cMain);
//
//			}
//			// 2019/09/30 QQ) 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//			// 2019/09/30 QQ) 共通テスト対応 ADD START
//			DbUtils.closeQuietly(rs3);
//			DbUtils.closeQuietly(rs4);
//			DbUtils.closeQuietly(rs5);
//			DbUtils.closeQuietly(ps3);
//			DbUtils.closeQuietly(ps4);
//			DbUtils.closeQuietly(ps5);
//			// 2019/09/30 QQ) 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S12_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_SCHOOL_DEV;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S12()
			.s12(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
	}

}

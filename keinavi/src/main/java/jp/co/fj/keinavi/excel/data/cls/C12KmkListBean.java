package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−型・科目リスト
 * 作成日: 2004/07/14
 * @author	H.Fujimoto
 */
public class C12KmkListBean {
	//型・科目コード
	private String strKmkCd = "";
	//型・科目名
	private String strKmkmei = "";
	//配点
	private String strHaitenkmk = "";
	//受験人数
	private int intNinzu = 0;
	//順位表データリスト
	private ArrayList c12JyuniPersonalList = new ArrayList();

	/*----------*/
	/* Get      */
	/*----------*/
	
	public ArrayList getC12JyuniPersonalList() {
		return this.c12JyuniPersonalList;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public String getStrHaitenkmk() {
		return this.strHaitenkmk;
	}
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}


	/*----------*/
	/* Set      */
	/*----------*/
	
	public void setC12JyuniPersonalList(ArrayList c12JyuniPersonalList) {
		this.c12JyuniPersonalList = c12JyuniPersonalList;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setStrHaitenkmk(String strHaitenkmk) {
		this.strHaitenkmk = strHaitenkmk;
	}
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}

}
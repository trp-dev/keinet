/*
 * 作成日: 2004/09/08
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.help;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.beans.help.HelpListBean;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 * ヘルプ画面サーブレット<BR>
 *
 * @author nino
 *
 */
public class H001Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ

		// セッション
		HttpSession session = request.getSession();

		// ログイン情報
		LoginSession login = super.getLoginSession(request);


		if(("h001").equals(getForward(request)) || getForward(request) == null) {
			Connection con = null; // コネクション
			try {
				con = super.getConnectionPool(request); 	// コネクション取得
				HelpListBean bean = new HelpListBean();
				bean.setFormId("h001");						// ヘルプトップ
				bean.setCategory(null);
				bean.setConnection(null, con);
				bean.execute();
	
				request.setAttribute("HelpListBean", bean);

		} catch (Exception e) {
				throw new ServletException(e.toString());
			} finally {
				super.releaseConnectionPool(request, con); // コネクション解放
			}
	
			super.forward(request, response, JSP_H001);

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

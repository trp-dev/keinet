package jp.co.fj.keinavi.excel.data.business;

/**
 * �Z�����ѕ��́|�S���������ъT���E�Ȗڕʐ��у��X�g
 * �쐬��: 2004/07/30
 * @author	Ito.Y
 */
public class B00KmkListBean {
	//�ȖڃR�[�h
	private String strKmkCd = "";
	//�Ȗږ�
	private String strKmkmei = "";
	//�z�_
	private String strHaitenKmk = "";
	//���ϓ_�E�S��
	private float floHeikinAll = 0;
	//���ϓ_�E����
	private float floHeikinGen = 0;
	//���ϓ_�E����
	private float floHeikinSotu = 0;
	//�W���΍�
	private float floStdHensa = 0;
	//���ϕ΍��l�E�S��
	private float floStdHensaAll = 0;
	//���ϕ΍��l�E����
	private float floStdHensaGen = 0;
	//���ϕ΍��l�E����
	private float floStdHensaSotu = 0;
	//�ō��_
	private int intMaxTen = 0;
	//�Œ�_
	private int intMinTen = 0;
	//�l���E�S��
	private int intNinzuAll = 0;
	//�l���E����
	private int intNinzuGen = 0;
	//�l���E����
	private int intNinzuSotu = 0;

	/*----------*/
	/* Get      */
	/*----------*/

	public float getFloHeikinAll() {
		return this.floHeikinAll;
	}
	public float getFloHeikinGen() {
		return this.floHeikinGen;
	}
	public float getFloHeikinSotu() {
		return this.floHeikinSotu;
	}
	public float getFloStdHensa() {
		return this.floStdHensa;
	}
	public float getFloStdHensaAll() {
		return this.floStdHensaAll;
	}
	public float getFloStdHensaGen() {
		return this.floStdHensaGen;
	}
	public float getFloStdHensaSotu() {
		return this.floStdHensaSotu;
	}
	public int getIntMaxTen() {
		return this.intMaxTen;
	}
	public int getIntMinTen() {
		return this.intMinTen;
	}
	public int getIntNinzuAll() {
		return this.intNinzuAll;
	}
	public int getIntNinzuGen() {
		return this.intNinzuGen;
	}
	public int getIntNinzuSotu() {
		return this.intNinzuSotu;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setFloHeikinAll(float floHeikinAll) {
		this.floHeikinAll = floHeikinAll;
	}
	public void setFloHeikinGen(float floHeikinGen) {
		this.floHeikinGen = floHeikinGen;
	}
	public void setFloHeikinSotu(float floHeikinSotu) {
		this.floHeikinSotu = floHeikinSotu;
	}
	public void setFloStdHensa(float floStdHensa) {
		this.floStdHensa = floStdHensa;
	}
	public void setFloStdHensaAll(float floStdHensaAll) {
		this.floStdHensaAll = floStdHensaAll;
	}
	public void setFloStdHensaGen(float floStdHensaGen) {
		this.floStdHensaGen = floStdHensaGen;
	}
	public void setFloStdHensaSotu(float floStdHensaSotu) {
		this.floStdHensaSotu = floStdHensaSotu;
	}
	public void setIntMaxTen(int intMaxTen) {
		this.intMaxTen = intMaxTen;
	}
	public void setIntMinTen(int intMinTen) {
		this.intMinTen = intMinTen;
	}
	public void setIntNinzuAll(int intNinzuAll) {
		this.intNinzuAll = intNinzuAll;
	}
	public void setIntNinzuGen(int intNinzuGen) {
		this.intNinzuGen = intNinzuGen;
	}
	public void setIntNinzuSotu(int intNinzuSotu) {
		this.intNinzuSotu = intNinzuSotu;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}

}
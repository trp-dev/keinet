package jp.co.fj.keinavi.excel.school;

//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.ListIterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S34ClassListBean;
import jp.co.fj.keinavi.excel.data.school.S34HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.school.S34Item;
import jp.co.fj.keinavi.excel.data.school.S34ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * 作成日: 2004/09/09
 * @author Ito.Y
 *
 * 校内成績分析−クラス比較−志望大学評価別人数　大学（学部・学科あり）出力処理
 *
 * <2010年度改修>
 * 2010.01.06   Tomohisa YAMADA - Totec
 *              センター・リサーチ５段階評価対応
 *
 */
public class S34_05 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow		= 50;			//MAX行数

	// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
	// 2019/09/30 QQ) 共通テスト対応 UPD START
	private int		intMaxClassSr   = 5;			//MAXクラス数
	//private int		intMaxClassSr   = 3;			//MAXクラス数
	// 2019/09/30 QQ) 共通テスト対応 UPD END
	// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END

	private int intDataStartRow = 8;					//データセット開始行

	// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
	// 2019/09/30 QQ) 共通テスト対応 UPD START
	private int intDataStartCol = 10;					//データセット開始列
	//private int intDataStartCol = 13;					//データセット開始列
	// 2019/09/30 QQ) 共通テスト対応 UPD END
        private int MAXCOL = 34;
        //1クラスの項目数
        private int MAXITEM = 5;
        //1クラスの評価別項目数
        private int MAXHYOUKA = 3;
	// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END

	final private String masterfile0 = "S34_05" ;
	final private String masterfile1 = "S34_05" ;
	private String masterfile = "" ;

	final private String strCenterResearch = "38";

	/*
	 * 	Excel編集メイン
	 * 		S34Item S34Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int s34_05EditExcel(S34Item s34Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("S34_05","S34_05帳票作成開始","");

		//テンプレートの決定
		if (s34Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		/** セット処理 **/
		boolean bolBookCngFlg = true;			//改ファイルフラグ
		boolean bolSheetCngFlg = true;			//改シートフラグ

		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFCell	workCell		= null;
		HSSFRow		workRow			= null;
		HSSFWorkbook	tmpworkbook = null;

		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intCol = 0;

		String strGenekiKbn = "";
		String strDaigakuCode = "";
		String strGkbCode = "";
		String strGkaCode = "";
		String strHyoukaKbn = "";

		//2004.09.27Add
		String strKokushiKbn = "";
		String strStKokushiKbn = "";
		int intChangeSheetRow = 0;
		//2004.09.27AddEnd

		int intLoopCnt = 0;
		int intBookCngCount = 1;

		boolean bolDispHyoukaFlg = false;
		boolean bolDispGkbFlg = false;
		boolean bolDispGkaFlg = false;
		boolean bolDispDaigakuFlg = false;
		String strHyouka = "";

		String strDispTarget = "";

		ArrayList WorkBookList = new ArrayList();
		ArrayList WorkSheetList = new ArrayList();

		int intSheetCnt=0;

		int intBookIndex = -1;
		int intSheetIndex = -1;

		int intSetRow = intDataStartRow;

		boolean bolClassDispFlg=true;

		ArrayList HyoukaTitleList = new ArrayList();

		// 基本ファイルを読込む
		S34ListBean s34ListBean = new S34ListBean();

		try{
			/** S34Item編集 **/
			s34Item  = editS34( s34Item );

			HyoukaTitleList.add(0,"A");
			HyoukaTitleList.add(1,"B");
			HyoukaTitleList.add(2,"C");

			// 基本ファイルを読込む
			ArrayList s34List = s34Item.getS34List();
			ListIterator itr = s34List.listIterator();

//			if( itr.hasNext() == false ){
//				return errfdata;
//			}

			/** 大学・学部・日程・評価 **/
			// S34ListBean
			while( itr.hasNext() ){
				s34ListBean = (S34ListBean)itr.next();

				// 大学名
				bolDispDaigakuFlg = false;
				if( !cm.toString(strDaigakuCode).equals(cm.toString(s34ListBean.getStrDaigakuCd()))){
					bolDispDaigakuFlg = true;
					//大学名表示
					strDaigakuCode = s34ListBean.getStrDaigakuCd();
				}

				// 学部
				bolDispGkbFlg = false;
				if( !cm.toString(strGkbCode).equals(cm.toString(s34ListBean.getStrGakubuCd())) ){
					bolDispGkbFlg = true;
					strGkbCode = s34ListBean.getStrGakubuCd();
				}

				// 日程
				bolDispGkaFlg = false;
				if( !cm.toString(strGkaCode).equals(cm.toString(s34ListBean.getStrGakkaCd())) ){
					bolDispGkaFlg = true;
					strGkaCode = s34ListBean.getStrGakkaCd();
				}

				// 評価区分
				bolDispHyoukaFlg = false;
				if( !cm.toString(strHyoukaKbn).equals(cm.toString(s34ListBean.getStrHyouka())) ){
					bolDispHyoukaFlg = true;
					//評価区分表示
					strHyoukaKbn = s34ListBean.getStrHyouka();
					strHyouka = "";
					switch( Integer.parseInt(strHyoukaKbn) ){
						case 1:
							// 2019/09/30 QQ) 共通テスト対応 UPD START
							//strHyouka = "センター";
							strHyouka = "共テ";
							// 2019/09/30 QQ) 共通テスト対応 UPD END
							break;
						case 2:
							strHyouka = "2次・私大";
							break;
						case 3:
							strHyouka = "総合";
							break;
					}
				}

				// 表示対象
				if( !cm.toString(strGenekiKbn).equals(cm.toString(s34ListBean.getStrGenKouKbn())) ){
					strGenekiKbn = s34ListBean.getStrGenKouKbn();
					if( cm.toString(strGenekiKbn).equals("0") ){
						strDispTarget = "全体";
					}
					else if( cm.toString(strGenekiKbn).equals("1") ){
						strDispTarget = "現役生";
					}
					else if( cm.toString(strGenekiKbn).equals("2") ){
						strDispTarget = "高卒生";
					}

					if( bolSheetCngFlg == false ){
						bolSheetCngFlg = true;
					}
				}

				//2004.9.27 Add
				int intDaigakuCd = (int)Integer.parseInt(s34ListBean.getStrDaigakuCd());
				if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
					strKokushiKbn = "国公立大";
				}
				if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
					strKokushiKbn = "私立大";
				}
				if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
					strKokushiKbn = "短大";
				}
				if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
					strKokushiKbn = "その他";
				}

				if( !cm.toString(strKokushiKbn).equals(cm.toString(strStKokushiKbn)) ){
					bolSheetCngFlg = true;
					strStKokushiKbn = strKokushiKbn;
				}
				//2004.9.27 Add End

				//クラス別評価人数リスト読み込み クラス名をExcelへセット
				ArrayList s34ClassList = s34ListBean.getS34ClassList();
				ListIterator itr2 = s34ClassList.listIterator();

				// Listの取得
				S34ClassListBean s34ClassListBean = new S34ClassListBean();
				S34ClassListBean HomeDataBean = new S34ClassListBean();

				//クラス数取得
				int intClassSr = s34ClassList.size();

//				int intSheetSr = (int)Math.ceil(intClassSr / intMaxClassSr);
//				int intSheetSr = intClassSr / intMaxClassSr;
//				if(intClassSr%intMaxClassSr!=0){
//					intSheetSr++;
//				}
				int intSheetSr = (intClassSr - 1) / intMaxClassSr;
				if((intClassSr - 1)%intMaxClassSr!=0){
					intSheetSr++;
				}

				int intLoopCnt2 = 0;
				int intNendoCount = 0;
				intChangeSheetRow = 0;

				/** 学校名・学年・クラス **/
				// S34ClassListBean
				while( itr2.hasNext() ){
					s34ClassListBean = (S34ClassListBean)itr2.next();

					// Listの取得
					ArrayList s34HyoukaNinzuList = s34ClassListBean.getS34HyoukaNinzuList();
					S34HyoukaNinzuListBean s34HyoukaNinzuListBean = new S34HyoukaNinzuListBean();

					if( intLoopCnt2 == 0 ){
						HomeDataBean = s34ClassListBean;
						s34ClassListBean = (S34ClassListBean)itr2.next();
						s34HyoukaNinzuList = s34ClassListBean.getS34HyoukaNinzuList();
						intNendoCount = s34HyoukaNinzuList.size();
						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
						intLoopCnt2++;
					}
					ListIterator itr3 = s34HyoukaNinzuList.listIterator();
//					ArrayList s34HyoukaNinzuList = s34ClassListBean.getS34HyoukaNinzuList();
//					S34HyoukaNinzuListBean s34HyoukaNinzuListBean = new S34HyoukaNinzuListBean();
//					ListIterator itr3 = s34HyoukaNinzuList.listIterator();
//
//					// 自校情報表示判定
//					if( intLoopCnt2 == 0 ){
//						bolHomeDataDispFlg = true;
//						HomeDataBean = s34ClassListBean;
//						s34ClassListBean = (S34ClassListBean)itr2.next();
//						intNendoCount = s34HyoukaNinzuList.size();
//						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
//						intLoopCnt2++;
//					}

					// 始点情報（Col）
					if( intLoopCnt2 % intMaxClassSr == 0 ){
					        // 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
						// 2019/09/30 QQ) 共通テスト対応 UPD START
						//intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 5 );
						//intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 8 );
						// 2019/09/30 QQ) 共通テスト対応 UPD END
						intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * MAXITEM );
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
					}else{
					        // 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
						// 2019/09/30 QQ) 共通テスト対応 UPD START
						//intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 5 );
						//intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 8 );
						// 2019/09/30 QQ) 共通テスト対応 UPD END
                                                intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * MAXITEM );
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
					}

//					intCol = intDataStartCol + ( ( intMaxClassSr - ( intLoopCnt2 % intMaxClassSr ) ) * 5 );

					/** 年度・志望者数 **/
					// S34HyoukaNinzuListBean
					int intHyoukaNinzuListIndex = 0;
					while( itr3.hasNext() ){
						s34HyoukaNinzuListBean = (S34HyoukaNinzuListBean)itr3.next();

						// 規定行数を表示したら線を引く
						if(( intSetRow  >= intChangeSheetRow )||((bolSheetCngFlg == true)&&(intMaxSheetIndex != 0))){
							bolSheetCngFlg = true;
							for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//								int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
								int intBIndex = (intSindex + 1) / intMaxSheetSr;
								if((intSindex + 1)%intMaxSheetSr!=0){
									intBIndex++;
								}
								workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
								workSheet = (HSSFSheet)WorkSheetList.get(intSindex);

								// 罫線出力(セルの上部に太線を引く）

								// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
								// 2019/09/30 QQ) 共通テスト対応 UPD START
								//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//		intSetRow, intSetRow, 0, 34, false, true, false, false );
								//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//									intSetRow, intSetRow, 0, 36, false, true, false, false );
                                                                cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                                        intSetRow, intSetRow, 0, MAXCOL, false, true, false, false );
								// 2019/09/30 QQ) 共通テスト対応 UPD END
								// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END

								if( workSheet != null ){
									int intRemoveStartRow = intChangeSheetRow;
									if( intMaxRow + intDataStartRow > intRemoveStartRow ){
										for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
											//行削除処理
											workSheet.removeRow(workSheet.getRow(index));
										}
										//罫線出力（セルの上部に太線を出力）

										// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
										// 2019/09/30 QQ) 共通テスト対応 UPD START
										//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
										//		intRemoveStartRow, intRemoveStartRow, 0, 34, false, true, false, false);
										//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
										//				intRemoveStartRow, intRemoveStartRow, 0, 36, false, true, false, false);
                                                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
                                                                                        intRemoveStartRow, intRemoveStartRow, 0, MAXCOL, false, true, false, false);
										// 2019/09/30 QQ) 共通テスト対応 UPD END
										// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
									}
								}

							}

							////////////////////保存処理//////////////////
							int intSheetNum = 0;
							switch( intSaveFlg ){
								case 1:
								case 5:
									intSheetNum = ((HSSFWorkbook)WorkBookList.get(0)).getNumberOfSheets()-1;
									break;
								case 2:
								case 3:
								case 4:
									intSheetNum = ((HSSFWorkbook)WorkBookList.get(0)).getNumberOfSheets()-2;
									break;
							}

							if( intSheetNum >= intMaxSheetSr ){
								tmpworkbook = (HSSFWorkbook)WorkBookList.get(0);

								boolean bolRet = false;
								// Excelファイル保存
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, tmpworkbook, UserID, intBookCngCount, masterfile, intSheetNum);

								if( bolRet == false ){
									return errfwrite;
								}
								intBookCngCount++;

								// 削除
								WorkBookList.remove(0);
								for(int a=0; a<intMaxSheetSr;a++){
									WorkSheetList.remove(0);
								}
								tmpworkbook=null;
								intBookIndex--;
								intSheetIndex -= intMaxSheetSr;
							}
						}

						// 改シート判定
						if( bolSheetCngFlg == true ){

							bolDispHyoukaFlg = true;
							bolDispDaigakuFlg = true;

							intSetRow = intDataStartRow;
							bolClassDispFlg = true;

							for( int intIndex = 0; intIndex < intSheetSr; intIndex ++ ){

								if( ( intMaxSheetIndex >= intMaxSheetSr ) || ( bolBookCngFlg == true ) ){
									// マスタExcel読み込み
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook==null ){
										return errfread;
									}
									WorkBookList.add(workbook);
									intBookIndex++;

									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
									workbook = (HSSFWorkbook)WorkBookList.get(intBookIndex);
								}

								//シート作成
								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);
								WorkSheetList.add(workSheet);
								intSheetIndex++;
								workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex);

								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);

								// セキュリティスタンプセット
								// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
								//2019/10/03 QQ)Oosaki 共通テスト対応 UPD START
								//// 2019/09/30 QQ) 共通テスト対応 UPD START
								////String secuFlg = cm.setSecurity( workbook, workSheet, s34Item.getIntSecuFlg(), 32, 34 );
								////workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
								//String secuFlg = cm.setSecurity( workbook, workSheet, s34Item.getIntSecuFlg(), 31, 36 );
								//workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
								//// 2019/09/30 QQ) 共通テスト対応 UPD END
								//String secuFlg = cm.setSecurity( workbook, workSheet, s34Item.getIntSecuFlg(), 34, 36 );
								//workCell = cm.setCell( workSheet, workRow, workCell, 0, 34 );
                                                                String secuFlg = cm.setSecurity( workbook, workSheet, s34Item.getIntSecuFlg(), 32, 34 );
                                                                workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
								//2019/10/03 QQ)Oosaki 共通テスト対応 UPD END
								// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
								workCell.setCellValue(secuFlg);

								// 学校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
								workCell.setCellValue( "学校名　：" + s34Item.getStrGakkomei() );

								// 模試月取得
								String moshi =cm.setTaisyouMoshi( s34Item.getStrMshDate() );

								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
								workCell.setCellValue( cm.getTargetExamLabel() + "：" + s34Item.getStrMshmei() + moshi);

								//表示対象
								workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
								workCell.setCellValue( "表示対象：" + strDispTarget );

								//評価別人数 見出し行
								for( int i = 0; i <= intMaxClassSr; i++ ){
								        // 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
									// 2019/09/30 QQ) 共通テスト対応 UPD START
									//int intTitleSetCol = (i * 5) + ( intDataStartCol - 3 );
									//int intTitleSetCol = (i * 8) + ( intDataStartCol - 6 );
									// 2019/09/30 QQ) 共通テスト対応 UPD END
                                                                        int intTitleSetCol = (i * MAXITEM) + ( intDataStartCol - MAXHYOUKA );
									// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END

									//出願予定・第1志望
									//2019/10/03 QQ)Oosaki 共通テスト対応 DEL START
									//// 2019/09/30 QQ) 共通テスト対応 UPD START
									//
									////if (strKokushiKbn == "国公立大") {
									////	workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
									////	workCell.setCellValue( "出願予定" );
									////} else {
									////	workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
									////	workCell.setCellValue( "第１志望" );
									////}
									//workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
									//workCell.setCellValue( "第１志望" );
									//// 2019/09/30 QQ) 共通テスト対応 UPD END
									//2019/10/03 QQ)Oosaki 共通テスト対応 DEL END

									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol );
									workCell.setCellValue( (String)HyoukaTitleList.get(0) );

									// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
									// 2019/09/30 QQ) 共通テスト対応 UPD START
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+1 );
									//workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+2 );
									// 2019/09/30 QQ) 共通テスト対応 UPD END
                                                                        // 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
									workCell.setCellValue( (String)HyoukaTitleList.get(1) );

									// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
									// 2019/09/30 QQ) 共通テスト対応 UPD START
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+2 );
									//workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+4 );
									// 2019/09/30 QQ) 共通テスト対応 UPD END
									// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
									workCell.setCellValue( (String)HyoukaTitleList.get(2) );
								}

								intMaxSheetIndex++;		//シート数カウンタインクリメント
								bolSheetCngFlg = false;

								intLoopCnt = 0;
							}	//ForEnd

							bolDispHyoukaFlg = true;
							bolDispGkaFlg = true;
							bolDispGkbFlg = true;
							bolDispDaigakuFlg = true;
						}	//if( bolSheetCngFlg == true )

//						intSheetCnt = (int)Math.ceil( (double)intLoopCnt2 / (double)intMaxClassSr );
						intSheetCnt = intLoopCnt2 / intMaxClassSr;
						if(intLoopCnt2%intMaxClassSr!=0){
							intSheetCnt++;
						}
						workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex -  (intSheetSr - intSheetCnt)  );

						int intSheetNo = intSheetIndex -  (intSheetSr - intSheetCnt);
//						int intBIndex = (int)Math.ceil((double)(intSheetNo + 1) / (double)intMaxSheetSr );
						int intBIndex = (intSheetNo + 1) / intMaxSheetSr;
						if((intSheetNo + 1)%intMaxSheetSr!=0){
							intBIndex++;
						}
						workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);

						if( intCol == intDataStartCol ){

							//データセット
							//自校データ・大学名ほか表示
							//自校学校名
							workCell = cm.setCell( workSheet, workRow, workCell, 5, 5 );
							workCell.setCellValue(HomeDataBean.getStrGakkomei());
							//自校データ
							ListIterator itrHome = HomeDataBean.getS34HyoukaNinzuList().listIterator();
							int intLoopIndex = 0;
							while( itrHome.hasNext() ){
								S34HyoukaNinzuListBean HomeHyoukaNinzuListBean = (S34HyoukaNinzuListBean) itrHome.next();
								boolean ret = setData( workSheet, HomeHyoukaNinzuListBean, intSetRow + intLoopIndex , 5 );
								if( ret == false ){
									return errfdata;
								}
								intLoopIndex++;
							}

							if( bolDispHyoukaFlg == true ){
								//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
															intSetRow, intSetRow, 3, 3, false, true, false, false);
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 3);
								workCell.setCellValue( strHyouka );

							}

							if( bolDispGkaFlg == true ){
								//罫線出力(セルの上部（日程の列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
															intSetRow, intSetRow, 2, 3, false, true, false, false);
								//日程表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( s34ListBean.getStrGakkaMei() );
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 3);
								workCell.setCellValue( strHyouka );

							}

							if( bolDispGkbFlg == true ){
								//罫線出力(セルの上部（学部の列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
															intSetRow, intSetRow, 1, 3, false, true, false, false);
								//学部表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
								workCell.setCellValue( s34ListBean.getStrGakubuMei() );
								//日程表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( s34ListBean.getStrGakkaMei() );
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 3);
								workCell.setCellValue( strHyouka );

							}

							if( bolDispDaigakuFlg == true ){
								//罫線出力(セルの上部に太線を引く）

							        // 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
								// 2019/09/30 QQ) 共通テスト対応 UPD START
								//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//		intSetRow, intSetRow, 0, 34, false, true, false, false );
								//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//									intSetRow, intSetRow, 0, 36, false, true, false, false );
                                                                cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                                        intSetRow, intSetRow, 0, MAXCOL, false, true, false, false );
								// 2019/09/30 QQ) 共通テスト対応 UPD END
								// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END

								//大学名表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 0 );
								workCell.setCellValue(s34ListBean.getStrDaigakuMei());
								//学部表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
								workCell.setCellValue( s34ListBean.getStrGakubuMei() );
								//日程表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( s34ListBean.getStrGakkaMei() );
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 3);
								workCell.setCellValue( strHyouka );
							}

						}

						boolean ret = setData( workSheet, s34HyoukaNinzuListBean, intSetRow + intHyoukaNinzuListIndex, intCol);
						if( ret != true){
							return errfdata;
						}
						else{
							intHyoukaNinzuListIndex++;
							intLoopCnt++;
						}

						//

					}	//WhileEnd( itr3 )


					//クラス名
					if( bolClassDispFlg == true ){
						String strGakunen = s34ClassListBean.getStrGrade();
						String strClass = s34ClassListBean.getStrClass();
						workCell = cm.setCell( workSheet, workRow, workCell, 5, intCol );
						workCell.setCellValue(strGakunen + "年 " + strClass + "クラス");
					}
					intLoopCnt2++;
				}	//WhileEnd( itr2 )

				intSetRow += intNendoCount;
				bolClassDispFlg = false;

				log.It("S34_05","大学名：",s34ListBean.getStrDaigakuMei());

			}	//WhileEnd( itr )

			for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//				int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr ); /** 20040930 **/
				int intBIndex = (intSindex + 1) / intMaxSheetSr;
				if((intSindex + 1)%intMaxSheetSr!=0){
					intBIndex++;
				}
				workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
				workSheet = (HSSFSheet)WorkSheetList.get(intSindex);

				//罫線出力(セルの上部に太線を引く）

				// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
				// 2019/09/30 QQ) 共通テスト対応 UPD START
				//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
				//		intSetRow, intSetRow, 0, 34, false, true, false, false );
				//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
				//									intSetRow, intSetRow, 0, 36, false, true, false, false );
                                cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                        intSetRow, intSetRow, 0, MAXCOL, false, true, false, false );
				// 2019/09/30 QQ) 共通テスト対応 UPD END
				// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END

				if( workSheet != null ){
					int intRemoveStartRow = intChangeSheetRow;
					if( intMaxRow + intDataStartRow > intRemoveStartRow ){
						for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
							//行削除処理
							workSheet.removeRow(workSheet.getRow(index));
						}
						//罫線出力（セルの上部に太線を出力）

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
						// 2019/09/30 QQ) 共通テスト対応 UPD START
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
						//		intRemoveStartRow, intRemoveStartRow, 0, 34, false, true, false, false);
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
						//				intRemoveStartRow, intRemoveStartRow, 0, 36, false, true, false, false);
                                                cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
                                                        intRemoveStartRow, intRemoveStartRow, 0, MAXCOL, false, true, false, false);
						// 2019/09/30 QQ) 共通テスト対応 UPD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
					}
				}

			}

			//ファイル保存
			for( int intBookSr = 0; intBookSr < WorkBookList.size(); intBookSr++ ){
				workbook = (HSSFWorkbook)WorkBookList.get(intBookSr);

				boolean bolRet = false;
				// Excelファイル保存
				if( (intBookCngCount != 1)||((intBookCngCount == 1)&&(WorkBookList.size()>1)) ){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, intMaxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}

			//データ無し
			if ( WorkBookList.size() == 0 ){
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex = 1;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);

				// セキュリティスタンプセット

				// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
				//2019/10/03 QQ)Oosaki 共通テスト対応 UPD START
				//// 2019/09/30 QQ) 共通テスト対応 UPD START
				////String secuFlg = cm.setSecurity( workbook, workSheet, s34Item.getIntSecuFlg(), 32, 34 );
				////workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
				//String secuFlg = cm.setSecurity( workbook, workSheet, s34Item.getIntSecuFlg(), 31, 36 );
				//workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
				//// 2019/09/30 QQ) 共通テスト対応 UPD END
				//String secuFlg = cm.setSecurity( workbook, workSheet, s34Item.getIntSecuFlg(), 34, 36 );
				//workCell = cm.setCell( workSheet, workRow, workCell, 0, 34 );
                                String secuFlg = cm.setSecurity( workbook, workSheet, s34Item.getIntSecuFlg(), 32, 34 );
                                workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
				//2019/10/03 QQ)Oosaki 共通テスト対応 UPD END
				// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
				workCell.setCellValue(secuFlg);

				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s34Item.getStrGakkomei()) );

				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s34Item.getStrMshDate() );

				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + s34Item.getStrMshmei() + moshi);

				//表示対象
				workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
				workCell.setCellValue( "表示対象：" );
			}

		}
		catch( Exception e ){
			log.Err("S34_05","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S34_05","S34_05帳票作成終了","");
		return noerror;
	}

	/**
	 * S34Item加算・編集
	 * @param s34Item
	 * @return
	 */
	private S34Item editS34( S34Item s34Item ) throws Exception{
		try{
			S34Item		s34ItemEdt	= new S34Item();
			String		daigakuCd	= "";
			String		gakubuCd	= "";
			String 		genekiKbn 	= "";

			// 学部計用
			boolean	centerGKBFlg	= false;
			boolean	secondGKBFlg	= false;
			boolean	totalGKBFlg	= false;

			ArrayList s34ClassListGKBAdd		= new ArrayList();
			ArrayList s34ClassListGKBAddCnt	= new ArrayList();
			ArrayList s34ClassListGKBAddSnd	= new ArrayList();
			ArrayList s34ClassListGKBAddTtl	= new ArrayList();
			ArrayList s34NinzuListGKBAdd		= new ArrayList();

			S34ListBean s34ListBeanGKBAddCnt = new S34ListBean();
			S34ListBean s34ListBeanGKBAddSnd = new S34ListBean();
			S34ListBean s34ListBeanGKBAddTtl = new S34ListBean();

			// 日程計用
			boolean	zenCenterNTIFlg	= false;
			boolean	zenSecondNTIFlg	= false;
			boolean	zenTotalNTIFlg	= false;
			boolean	chuCenterNTIFlg	= false;
			boolean	chuSecondNTIFlg	= false;
			boolean	chuTotalNTIFlg	= false;
			boolean	kokCenterNTIFlg	= false;
			boolean	kokSecondNTIFlg	= false;
			boolean	kokTotalNTIFlg	= false;
			boolean	ipnCenterNTIFlg	= false;
			boolean	ipnSecondNTIFlg	= false;
			boolean	ipnTotalNTIFlg	= false;
			boolean	cenCenterNTIFlg	= false;
			boolean	cenSecondNTIFlg	= false;
			boolean	cenTotalNTIFlg	= false;

			ArrayList s34ClassListNTIAdd		= new ArrayList();
			ArrayList s34ClassListNTIAddZenCnt		= new ArrayList();
			ArrayList s34ClassListNTIAddZenSnd		= new ArrayList();
			ArrayList s34ClassListNTIAddZenTtl		= new ArrayList();
			ArrayList s34ClassListNTIAddChuCnt		= new ArrayList();
			ArrayList s34ClassListNTIAddChuSnd		= new ArrayList();
			ArrayList s34ClassListNTIAddChuTtl		= new ArrayList();
			ArrayList s34ClassListNTIAddKokCnt		= new ArrayList();
			ArrayList s34ClassListNTIAddKokSnd		= new ArrayList();
			ArrayList s34ClassListNTIAddKokTtl		= new ArrayList();
			ArrayList s34ClassListNTIAddIpnCnt		= new ArrayList();
			ArrayList s34ClassListNTIAddIpnSnd		= new ArrayList();
			ArrayList s34ClassListNTIAddIpnTtl		= new ArrayList();
			ArrayList s34ClassListNTIAddCenCnt		= new ArrayList();
			ArrayList s34ClassListNTIAddCenSnd		= new ArrayList();
			ArrayList s34ClassListNTIAddCenTtl		= new ArrayList();
			ArrayList s34NinzuListNTIAdd		= new ArrayList();

			S34ListBean s34ListBeanNTIAddZenCnt = new S34ListBean();
			S34ListBean s34ListBeanNTIAddZenSnd = new S34ListBean();
			S34ListBean s34ListBeanNTIAddZenTtl = new S34ListBean();
			S34ListBean s34ListBeanNTIAddChuCnt = new S34ListBean();
			S34ListBean s34ListBeanNTIAddChuSnd = new S34ListBean();
			S34ListBean s34ListBeanNTIAddChuTtl = new S34ListBean();
			S34ListBean s34ListBeanNTIAddKokCnt = new S34ListBean();
			S34ListBean s34ListBeanNTIAddKokSnd = new S34ListBean();
			S34ListBean s34ListBeanNTIAddKokTtl = new S34ListBean();
			S34ListBean s34ListBeanNTIAddIpnCnt = new S34ListBean();
			S34ListBean s34ListBeanNTIAddIpnSnd = new S34ListBean();
			S34ListBean s34ListBeanNTIAddIpnTtl = new S34ListBean();
			S34ListBean s34ListBeanNTIAddCenCnt = new S34ListBean();
			S34ListBean s34ListBeanNTIAddCenSnd = new S34ListBean();
			S34ListBean s34ListBeanNTIAddCenTtl = new S34ListBean();

			// 大学計用
			boolean	centerDGKFlg	= false;
			boolean	secondDGKFlg	= false;
			boolean	totalDGKFlg	= false;

			ArrayList s34ClassListDGKAdd		= new ArrayList();
			ArrayList s34ClassListDGKAddCnt		= new ArrayList();
			ArrayList s34ClassListDGKAddSnd		= new ArrayList();
			ArrayList s34ClassListDGKAddTtl		= new ArrayList();
			ArrayList s34NinzuListDGKAdd		= new ArrayList();

			S34ListBean s34ListBeanDGKAddCnt = new S34ListBean();
			S34ListBean s34ListBeanDGKAddSnd = new S34ListBean();
			S34ListBean s34ListBeanDGKAddTtl = new S34ListBean();

			s34ItemEdt.setStrGakkomei( s34Item.getStrGakkomei() );
			s34ItemEdt.setStrMshmei( s34Item.getStrMshmei() );
			s34ItemEdt.setStrMshDate( s34Item.getStrMshDate() );
			s34ItemEdt.setStrMshCd( s34Item.getStrMshCd() );
			s34ItemEdt.setIntSecuFlg( s34Item.getIntSecuFlg() );
			s34ItemEdt.setIntDaiTotalFlg( s34Item.getIntDaiTotalFlg() );


			/** 大学・学部・学科・日程・評価 **/
			ArrayList s34List		= s34Item.getS34List();
			ArrayList s34ListEdt	= new ArrayList();
			ListIterator itr = s34List.listIterator();
			while( itr.hasNext() ){
				S34ListBean s34ListBean		= (S34ListBean) itr.next();
				S34ListBean s34ListBeanEdt	= new S34ListBean();

				//-----------------------------------------------------------------------------------------
				//大学計・学部計を出力
				//-----------------------------------------------------------------------------------------
				if( !cm.toString(gakubuCd).equals( cm.toString(s34ListBean.getStrGakubuCd()) ) ||
						!cm.toString(daigakuCd).equals( cm.toString(s34ListBean.getStrDaigakuCd()))
						|| !cm.toString(genekiKbn).equals(cm.toString(s34ListBean.getStrGenKouKbn()))) {

					gakubuCd = s34ListBean.getStrGakubuCd();

					if(centerGKBFlg == true){
						s34ListBeanGKBAddCnt.setS34ClassList( deepCopyCl( s34ClassListGKBAddCnt ) );
						s34ListEdt.add( s34ListBeanGKBAddCnt );
						s34ClassListGKBAddCnt = new ArrayList();
						centerGKBFlg = false;
					}
					if(secondGKBFlg == true){
						s34ListBeanGKBAddSnd.setS34ClassList( (s34ClassListGKBAddSnd) );
						s34ListEdt.add( s34ListBeanGKBAddSnd );
						s34ClassListGKBAddSnd = new ArrayList();
						secondGKBFlg = false;
					}
					if(totalGKBFlg == true){
						s34ListBeanGKBAddTtl.setS34ClassList( deepCopyCl(s34ClassListGKBAddTtl) );
						s34ListEdt.add( s34ListBeanGKBAddTtl );
						s34ClassListGKBAddTtl = new ArrayList();
						totalGKBFlg = false;
					}
					s34ListBeanGKBAddCnt = new S34ListBean();
					s34ListBeanGKBAddSnd = new S34ListBean();
					s34ListBeanGKBAddTtl = new S34ListBean();
					s34NinzuListGKBAdd = new ArrayList();
				}
				// 大学コードが変わる→日程計・大学計の保存
//////////////////
//				if( !cm.toString(daigakuCd).equals( cm.toString(s34ListBean.getStrDaigakuCd()) ) ){
//////////////////
				if( (!cm.toString(daigakuCd).equals( cm.toString(s34ListBean.getStrDaigakuCd())))
					||( !cm.toString(genekiKbn).equals(cm.toString(s34ListBean.getStrGenKouKbn()))) ){
					genekiKbn = s34ListBean.getStrGenKouKbn();

					daigakuCd = s34ListBean.getStrDaigakuCd();

					// 日程計
					if(zenCenterNTIFlg == true){
						s34ListBeanNTIAddZenCnt.setS34ClassList( s34ClassListNTIAddZenCnt );
						s34ListEdt.add( s34ListBeanNTIAddZenCnt );
						s34ClassListNTIAddZenCnt = new ArrayList();
						zenCenterNTIFlg = false;
					}
					if(zenSecondNTIFlg == true){
						s34ListBeanNTIAddZenSnd.setS34ClassList( s34ClassListNTIAddZenSnd );
						s34ListEdt.add( s34ListBeanNTIAddZenSnd );
						s34ClassListNTIAddZenSnd = new ArrayList();
						zenSecondNTIFlg = false;
					}
					if(zenTotalNTIFlg == true){
						s34ListBeanNTIAddZenTtl.setS34ClassList( s34ClassListNTIAddZenTtl );
						s34ListEdt.add( s34ListBeanNTIAddZenTtl );
						s34ClassListNTIAddZenTtl = new ArrayList();
						zenTotalNTIFlg = false;
					}
					if(chuCenterNTIFlg == true){
						s34ListBeanNTIAddChuCnt.setS34ClassList( s34ClassListNTIAddChuCnt );
						s34ListEdt.add( s34ListBeanNTIAddChuCnt );
						s34ClassListNTIAddChuCnt = new ArrayList();
						chuCenterNTIFlg = false;
					}
					if(chuSecondNTIFlg == true){
						s34ListBeanNTIAddChuSnd.setS34ClassList( s34ClassListNTIAddChuSnd );
						s34ListEdt.add( s34ListBeanNTIAddChuSnd );
						s34ClassListNTIAddChuSnd = new ArrayList();
						chuSecondNTIFlg = false;
					}
					if(chuTotalNTIFlg == true){
						s34ListBeanNTIAddChuTtl.setS34ClassList( s34ClassListNTIAddChuTtl );
						s34ListEdt.add( s34ListBeanNTIAddChuTtl );
						s34ClassListNTIAddChuTtl = new ArrayList();
						chuTotalNTIFlg = false;
					}
					if(kokCenterNTIFlg == true){
						s34ListBeanNTIAddKokCnt.setS34ClassList( s34ClassListNTIAddKokCnt );
						s34ListEdt.add( s34ListBeanNTIAddKokCnt );
						s34ClassListNTIAddKokCnt = new ArrayList();
						kokCenterNTIFlg = false;
					}
					if(kokSecondNTIFlg == true){
						s34ListBeanNTIAddKokSnd.setS34ClassList( s34ClassListNTIAddKokSnd );
						s34ListEdt.add( s34ListBeanNTIAddKokSnd );
						s34ClassListNTIAddKokSnd = new ArrayList();
						kokSecondNTIFlg = false;
					}
					if(kokTotalNTIFlg == true){
						s34ListBeanNTIAddKokTtl.setS34ClassList( s34ClassListNTIAddKokTtl );
						s34ListEdt.add( s34ListBeanNTIAddKokTtl );
						s34ClassListNTIAddKokTtl = new ArrayList();
						kokTotalNTIFlg = false;
					}
					if(ipnCenterNTIFlg == true){
						s34ListBeanNTIAddIpnCnt.setS34ClassList( s34ClassListNTIAddIpnCnt );
						s34ListEdt.add( s34ListBeanNTIAddIpnCnt );
						s34ClassListNTIAddIpnCnt = new ArrayList();
						ipnCenterNTIFlg = false;
					}
					if(ipnSecondNTIFlg == true){
						s34ListBeanNTIAddIpnSnd.setS34ClassList( s34ClassListNTIAddIpnSnd );
						s34ListEdt.add( s34ListBeanNTIAddIpnSnd );
						s34ClassListNTIAddIpnSnd = new ArrayList();
						ipnSecondNTIFlg = false;
					}
					if(ipnTotalNTIFlg == true){
						s34ListBeanNTIAddIpnTtl.setS34ClassList( s34ClassListNTIAddIpnTtl );
						s34ListEdt.add( s34ListBeanNTIAddIpnTtl );
						s34ClassListNTIAddIpnTtl = new ArrayList();
						ipnTotalNTIFlg = false;
					}
					if(cenCenterNTIFlg == true){
						s34ListBeanNTIAddCenCnt.setS34ClassList( s34ClassListNTIAddCenCnt );
						s34ListEdt.add( s34ListBeanNTIAddCenCnt );
						s34ClassListNTIAddCenCnt = new ArrayList();
						cenCenterNTIFlg = false;
					}
					if(cenSecondNTIFlg == true){
						s34ListBeanNTIAddCenSnd.setS34ClassList( s34ClassListNTIAddCenSnd );
						s34ListEdt.add( s34ListBeanNTIAddCenSnd );
						s34ClassListNTIAddCenSnd = new ArrayList();
						cenSecondNTIFlg = false;
					}
					if(cenTotalNTIFlg == true){
						s34ListBeanNTIAddCenTtl.setS34ClassList( s34ClassListNTIAddCenTtl );
						s34ListEdt.add( s34ListBeanNTIAddCenTtl );
						s34ClassListNTIAddCenTtl = new ArrayList();
						cenTotalNTIFlg = false;
					}

					// 大学計
					if(centerDGKFlg == true){
						s34ListBeanDGKAddCnt.setS34ClassList( s34ClassListDGKAddCnt );
						s34ListEdt.add( s34ListBeanDGKAddCnt );
						s34ClassListDGKAddCnt = new ArrayList();
						centerDGKFlg = false;
					}
					if(secondDGKFlg == true){
						s34ListBeanDGKAddSnd.setS34ClassList( s34ClassListDGKAddSnd );
						s34ListEdt.add( s34ListBeanDGKAddSnd );
						s34ClassListDGKAddSnd = new ArrayList();
						secondDGKFlg = false;
					}
					if(totalDGKFlg == true){
						s34ListBeanDGKAddTtl.setS34ClassList( s34ClassListDGKAddTtl );
						s34ListEdt.add( s34ListBeanDGKAddTtl );
						s34ClassListDGKAddTtl = new ArrayList();
						totalDGKFlg = false;
					}

					s34ListBeanNTIAddZenCnt = new S34ListBean();
					s34ListBeanNTIAddZenSnd = new S34ListBean();
					s34ListBeanNTIAddZenTtl = new S34ListBean();
					s34ListBeanNTIAddChuCnt = new S34ListBean();
					s34ListBeanNTIAddChuSnd = new S34ListBean();
					s34ListBeanNTIAddChuTtl = new S34ListBean();
					s34ListBeanNTIAddKokCnt = new S34ListBean();
					s34ListBeanNTIAddKokSnd = new S34ListBean();
					s34ListBeanNTIAddKokTtl = new S34ListBean();
					s34ListBeanNTIAddIpnCnt = new S34ListBean();
					s34ListBeanNTIAddIpnSnd = new S34ListBean();
					s34ListBeanNTIAddIpnTtl = new S34ListBean();
					s34ListBeanNTIAddCenCnt = new S34ListBean();
					s34ListBeanNTIAddCenSnd = new S34ListBean();
					s34ListBeanNTIAddCenTtl = new S34ListBean();
					s34NinzuListNTIAdd = new ArrayList();

					s34ListBeanDGKAddCnt = new S34ListBean();
					s34ListBeanDGKAddSnd = new S34ListBean();
					s34ListBeanDGKAddTtl = new S34ListBean();
					s34NinzuListDGKAdd = new ArrayList();
				}

				// 基本部分の移し変え
				s34ListBeanEdt.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
				s34ListBeanEdt.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
				s34ListBeanEdt.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
				s34ListBeanEdt.setStrGakubuCd( s34ListBean.getStrGakubuCd() );
				s34ListBeanEdt.setStrGakubuMei( s34ListBean.getStrGakubuMei() );
				s34ListBeanEdt.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
				s34ListBeanEdt.setStrGakkaMei( s34ListBean.getStrGakkaMei() );
				s34ListBeanEdt.setStrNtiCd( s34ListBean.getStrNtiCd() );
				s34ListBeanEdt.setStrNittei( s34ListBean.getStrNittei() );
				s34ListBeanEdt.setStrHyouka( s34ListBean.getStrHyouka() );

				// 学部計値の保存部
				switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
					case 1:
						s34ListBeanGKBAddCnt.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanGKBAddCnt.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanGKBAddCnt.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanGKBAddCnt.setStrGakubuCd( s34ListBean.getStrGakubuCd() );
						s34ListBeanGKBAddCnt.setStrGakubuMei( s34ListBean.getStrGakubuMei() );
						s34ListBeanGKBAddCnt.setStrGakkaCd( "GKK-GKB" );
						s34ListBeanGKBAddCnt.setStrGakkaMei( "学部計" );
						s34ListBeanGKBAddCnt.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanGKBAddCnt.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanGKBAddCnt.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListGKBAdd = deepCopyCl( s34ClassListGKBAddCnt );
						centerGKBFlg = true;
						break;
					case 2:
						s34ListBeanGKBAddSnd.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanGKBAddSnd.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanGKBAddSnd.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanGKBAddSnd.setStrGakubuCd( s34ListBean.getStrGakubuCd() );
						s34ListBeanGKBAddSnd.setStrGakubuMei( s34ListBean.getStrGakubuMei() );
						s34ListBeanGKBAddSnd.setStrGakkaCd( "GKK-GKB" );
						s34ListBeanGKBAddSnd.setStrGakkaMei( "学部計" );
						s34ListBeanGKBAddSnd.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanGKBAddSnd.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanGKBAddSnd.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListGKBAdd = deepCopyCl( s34ClassListGKBAddSnd );
						secondGKBFlg = true;
						break;
					case 3:
						s34ListBeanGKBAddTtl.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanGKBAddTtl.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanGKBAddTtl.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanGKBAddTtl.setStrGakubuCd( s34ListBean.getStrGakubuCd() );
						s34ListBeanGKBAddTtl.setStrGakubuMei( s34ListBean.getStrGakubuMei() );
						s34ListBeanGKBAddTtl.setStrGakkaCd( "GKK-GKB" );
						s34ListBeanGKBAddTtl.setStrGakkaMei( "学部計" );
						s34ListBeanGKBAddTtl.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanGKBAddTtl.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanGKBAddTtl.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListGKBAdd = deepCopyCl( s34ClassListGKBAddTtl );
						totalGKBFlg = true;
						break;
				}

				// 大学計値の保存部
				switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
					case 1:
						s34ListBeanDGKAddCnt.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanDGKAddCnt.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanDGKAddCnt.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanDGKAddCnt.setStrGakubuCd( "GKK-DGK" );
						s34ListBeanDGKAddCnt.setStrGakubuMei( "大学計" );
//						s34ListBeanDGKAddCnt.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanDGKAddCnt.setStrGakkaCd( "01" );
						s34ListBeanDGKAddCnt.setStrGakkaMei( "" );
						s34ListBeanDGKAddCnt.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanDGKAddCnt.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanDGKAddCnt.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListDGKAdd = deepCopyCl( s34ClassListDGKAddCnt );
						centerDGKFlg = true;
						break;
					case 2:
						s34ListBeanDGKAddSnd.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanDGKAddSnd.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanDGKAddSnd.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanDGKAddSnd.setStrGakubuCd( "GKK-DGK" );
						s34ListBeanDGKAddSnd.setStrGakubuMei( "大学計" );
//						s34ListBeanDGKAddSnd.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanDGKAddSnd.setStrGakkaCd( "01" );
						s34ListBeanDGKAddSnd.setStrGakkaMei( "" );
						s34ListBeanDGKAddSnd.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanDGKAddSnd.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanDGKAddSnd.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListDGKAdd = deepCopyCl( s34ClassListDGKAddSnd );
						secondDGKFlg = true;
						break;
					case 3:
						s34ListBeanDGKAddTtl.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanDGKAddTtl.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanDGKAddTtl.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanDGKAddTtl.setStrGakubuCd( "GKK-DGK" );
						s34ListBeanDGKAddTtl.setStrGakubuMei( "大学計" );
//						s34ListBeanDGKAddTtl.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanDGKAddTtl.setStrGakkaCd( "01" );
						s34ListBeanDGKAddTtl.setStrGakkaMei( "" );
						s34ListBeanDGKAddTtl.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanDGKAddTtl.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanDGKAddTtl.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListDGKAdd = deepCopyCl( s34ClassListDGKAddTtl );
						totalDGKFlg = true;
						break;
				}

				// 日程計値の保存部
				if( cm.toString(s34ListBean.getStrNtiCd()).equals("D") ){		 // 前期
					switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
						case 1:
						s34ListBeanNTIAddZenCnt.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddZenCnt.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddZenCnt.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddZenCnt.setStrGakubuCd( "GKK-ZEN" );
						s34ListBeanNTIAddZenCnt.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddZenCnt.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddZenCnt.setStrGakkaMei( "" );
						s34ListBeanNTIAddZenCnt.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddZenCnt.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddZenCnt.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddZenCnt );
						zenCenterNTIFlg = true;
							break;
						case 2:
						s34ListBeanNTIAddZenSnd.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddZenSnd.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddZenSnd.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddZenSnd.setStrGakubuCd( "GKK-ZEN" );
						s34ListBeanNTIAddZenSnd.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddZenSnd.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddZenSnd.setStrGakkaMei( "" );
						s34ListBeanNTIAddZenSnd.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddZenSnd.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddZenSnd.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddZenSnd );
						zenSecondNTIFlg = true;
							break;
						case 3:
						s34ListBeanNTIAddZenTtl.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddZenTtl.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddZenTtl.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddZenTtl.setStrGakubuCd( "GKK-ZEN" );
						s34ListBeanNTIAddZenTtl.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddZenTtl.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddZenTtl.setStrGakkaMei( "" );
						s34ListBeanNTIAddZenTtl.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddZenTtl.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddZenTtl.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddZenTtl );
						zenTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(s34ListBean.getStrNtiCd()).equals("E") ){	 // 中期
					switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
						case 1:
						s34ListBeanNTIAddChuCnt.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddChuCnt.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddChuCnt.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddChuCnt.setStrGakubuCd( "GKK-CHU" );
						s34ListBeanNTIAddChuCnt.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddChuCnt.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddChuCnt.setStrGakkaMei( "" );
						s34ListBeanNTIAddChuCnt.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddChuCnt.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddChuCnt.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddChuCnt );
						chuCenterNTIFlg = true;
							break;
						case 2:
						s34ListBeanNTIAddChuSnd.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddChuSnd.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddChuSnd.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddChuSnd.setStrGakubuCd( "GKK-CHU" );
						s34ListBeanNTIAddChuSnd.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddChuSnd.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddChuSnd.setStrGakkaMei( "" );
						s34ListBeanNTIAddChuSnd.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddChuSnd.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddChuSnd.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddChuSnd );
						chuSecondNTIFlg = true;
							break;
						case 3:
						s34ListBeanNTIAddChuTtl.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddChuTtl.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddChuTtl.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddChuTtl.setStrGakubuCd( "GKK-CHU" );
						s34ListBeanNTIAddChuTtl.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddChuTtl.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddChuTtl.setStrGakkaMei( "" );
						s34ListBeanNTIAddChuTtl.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddChuTtl.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddChuTtl.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddChuTtl );
						chuTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(s34ListBean.getStrNtiCd()).equals("C") ){	 // 後期
					switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
						case 1:
						s34ListBeanNTIAddKokCnt.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddKokCnt.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddKokCnt.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddKokCnt.setStrGakubuCd( "GKK-KOK" );
						s34ListBeanNTIAddKokCnt.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddKokCnt.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddKokCnt.setStrGakkaMei( "" );
						s34ListBeanNTIAddKokCnt.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddKokCnt.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddKokCnt.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddKokCnt );
						kokCenterNTIFlg = true;
							break;
						case 2:
						s34ListBeanNTIAddKokSnd.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddKokSnd.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddKokSnd.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddKokSnd.setStrGakubuCd( "GKK-KOK" );
						s34ListBeanNTIAddKokSnd.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddKokSnd.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddKokSnd.setStrGakkaMei( "" );
						s34ListBeanNTIAddKokSnd.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddKokSnd.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddKokSnd.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddKokSnd );
						kokSecondNTIFlg = true;
							break;
						case 3:
						s34ListBeanNTIAddKokTtl.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddKokTtl.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddKokTtl.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddKokTtl.setStrGakubuCd( "GKK-KOK" );
						s34ListBeanNTIAddKokTtl.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddKokTtl.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddKokTtl.setStrGakkaMei( "" );
						s34ListBeanNTIAddKokTtl.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddKokTtl.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddKokTtl.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddKokTtl );
						kokTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(s34ListBean.getStrNtiCd()).equals("N") ){	 // 一般型
					switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
						case 1:
						s34ListBeanNTIAddIpnCnt.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddIpnCnt.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddIpnCnt.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddIpnCnt.setStrGakubuCd( "GKK-IPN" );
						s34ListBeanNTIAddIpnCnt.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddIpnCnt.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddIpnCnt.setStrGakkaMei( "" );
						s34ListBeanNTIAddIpnCnt.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddIpnCnt.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddIpnCnt.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddIpnCnt );
						ipnCenterNTIFlg = true;
							break;
						case 2:
						s34ListBeanNTIAddIpnSnd.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddIpnSnd.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddIpnSnd.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddIpnSnd.setStrGakubuCd( "GKK-IPN" );
						s34ListBeanNTIAddIpnSnd.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddIpnSnd.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddIpnSnd.setStrGakkaMei( "" );
						s34ListBeanNTIAddIpnSnd.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddIpnSnd.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddIpnSnd.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddIpnSnd );
						ipnSecondNTIFlg = true;
							break;
						case 3:
						s34ListBeanNTIAddIpnTtl.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddIpnTtl.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddIpnTtl.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddIpnTtl.setStrGakubuCd( "GKK-IPN" );
						s34ListBeanNTIAddIpnTtl.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddIpnTtl.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddIpnTtl.setStrGakkaMei( "" );
						s34ListBeanNTIAddIpnTtl.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddIpnTtl.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddIpnTtl.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddIpnTtl );
						ipnTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(s34ListBean.getStrNtiCd()).equals("1") ){	 // センター型
					switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
						case 1:
						s34ListBeanNTIAddCenCnt.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddCenCnt.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddCenCnt.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddCenCnt.setStrGakubuCd( "GKK-CEN" );
						s34ListBeanNTIAddCenCnt.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddCenCnt.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddCenCnt.setStrGakkaMei( "" );
						s34ListBeanNTIAddCenCnt.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddCenCnt.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddCenCnt.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddCenCnt );
						cenCenterNTIFlg = true;
							break;
						case 2:
						s34ListBeanNTIAddCenSnd.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddCenSnd.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddCenSnd.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddCenSnd.setStrGakubuCd( "GKK-CEN" );
						s34ListBeanNTIAddCenSnd.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddCenSnd.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddCenSnd.setStrGakkaMei( "" );
						s34ListBeanNTIAddCenSnd.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddCenSnd.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddCenSnd.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddCenSnd );
						cenSecondNTIFlg = true;
							break;
						case 3:
						s34ListBeanNTIAddCenTtl.setStrGenKouKbn( s34ListBean.getStrGenKouKbn() );
						s34ListBeanNTIAddCenTtl.setStrDaigakuCd( s34ListBean.getStrDaigakuCd() );
						s34ListBeanNTIAddCenTtl.setStrDaigakuMei( s34ListBean.getStrDaigakuMei() );
						s34ListBeanNTIAddCenTtl.setStrGakubuCd( "GKK-CEN" );
						s34ListBeanNTIAddCenTtl.setStrGakubuMei( s34ListBean.getStrNittei() + "計" );
						s34ListBeanNTIAddCenTtl.setStrGakkaCd( s34ListBean.getStrGakkaCd() );
						s34ListBeanNTIAddCenTtl.setStrGakkaMei( "" );
						s34ListBeanNTIAddCenTtl.setStrNtiCd( s34ListBean.getStrNtiCd() );
						s34ListBeanNTIAddCenTtl.setStrNittei( s34ListBean.getStrNittei() );
						s34ListBeanNTIAddCenTtl.setStrHyouka( s34ListBean.getStrHyouka() );
						s34ClassListNTIAdd = deepCopyCl( s34ClassListNTIAddCenTtl );
						cenTotalNTIFlg = true;
							break;
					}
				}

				//
				ArrayList s34ClassList		= s34ListBean.getS34ClassList();
				ArrayList s34ClassListEdt	= new ArrayList();
				ListIterator itrClass = s34ClassList.listIterator();
				int b = 0;
				boolean firstFlgGKB3 = true;
				boolean firstFlgDGK3 = true;
				boolean firstFlgNTI3 = true;
				if(s34ClassListGKBAdd.size() == 0){
					firstFlgGKB3 = false;
				}
				if(s34ClassListDGKAdd.size() == 0){
					firstFlgDGK3 = false;
				}
				if(s34ClassListNTIAdd.size() == 0){
					firstFlgNTI3 = false;
				}
				/** 学校・学年・クラス **/
				while( itrClass.hasNext() ){
					S34ClassListBean s34ClassListBean = (S34ClassListBean)itrClass.next();
					S34ClassListBean s34ClassListBeanEdt = new S34ClassListBean();
					S34ClassListBean s34ClassListBeanDGKAdd = null;
					S34ClassListBean s34ClassListBeanGKBAdd = null;
					S34ClassListBean s34ClassListBeanNTIAdd = null;

					// 移し変え
					s34ClassListBeanEdt.setStrGakkomei( s34ClassListBean.getStrGakkomei() );
					s34ClassListBeanEdt.setStrGrade( s34ClassListBean.getStrGrade() );
					s34ClassListBeanEdt.setStrClass( s34ClassListBean.getStrClass() );

					// 大学計用データ呼び出し
					if(firstFlgDGK3 == true){
						s34ClassListBeanDGKAdd = (S34ClassListBean) s34ClassListDGKAdd.get(b);
						s34NinzuListDGKAdd = deepCopyNz( s34ClassListBeanDGKAdd.getS34HyoukaNinzuList() );
					}else{
						s34ClassListBeanDGKAdd = new S34ClassListBean();

						s34ClassListBeanDGKAdd.setStrGakkomei( s34ClassListBean.getStrGakkomei() );
						s34ClassListBeanDGKAdd.setStrGrade( s34ClassListBean.getStrGrade() );
						s34ClassListBeanDGKAdd.setStrClass( s34ClassListBean.getStrClass() );
					}

					// 学部計用データ呼び出し
					if(firstFlgGKB3 == true){
						s34ClassListBeanGKBAdd = (S34ClassListBean) s34ClassListGKBAdd.get(b);
						s34NinzuListGKBAdd = deepCopyNz( s34ClassListBeanGKBAdd.getS34HyoukaNinzuList() );
					}else{
						s34ClassListBeanGKBAdd = new S34ClassListBean();

						s34ClassListBeanGKBAdd.setStrGakkomei( s34ClassListBean.getStrGakkomei() );
						s34ClassListBeanGKBAdd.setStrGrade( s34ClassListBean.getStrGrade() );
						s34ClassListBeanGKBAdd.setStrClass( s34ClassListBean.getStrClass() );
					}

					// 日程計用データ呼び出し
					if(firstFlgNTI3 == true){
						s34ClassListBeanNTIAdd = (S34ClassListBean) s34ClassListNTIAdd.get(b);
						s34NinzuListNTIAdd = deepCopyNz( s34ClassListBeanNTIAdd.getS34HyoukaNinzuList() );
					}else{
						s34ClassListBeanNTIAdd = new S34ClassListBean();

						s34ClassListBeanNTIAdd.setStrGakkomei( s34ClassListBean.getStrGakkomei() );
						s34ClassListBeanNTIAdd.setStrGrade( s34ClassListBean.getStrGrade() );
						s34ClassListBeanNTIAdd.setStrClass( s34ClassListBean.getStrClass() );
					}

					/** 年度・人数 **/
					ArrayList s34NinzuList = s34ClassListBean.getS34HyoukaNinzuList();
					ArrayList s34NinzuListEdt = new ArrayList();
					ListIterator itrNinzu = s34NinzuList.listIterator();
					int a = 0;
					boolean firstFlgDGK2 = true;
					boolean firstFlgGKB2 = true;
					boolean firstFlgNTI2 = true;
					if(s34NinzuListDGKAdd.size() == 0){
						firstFlgDGK2 = false;
					}
					if(s34NinzuListGKBAdd.size() == 0){
						firstFlgGKB2 = false;
					}
					if(s34NinzuListNTIAdd.size() == 0){
						firstFlgNTI2 = false;
					}
					while( itrNinzu.hasNext() ){
						S34HyoukaNinzuListBean s34NinzuBean = (S34HyoukaNinzuListBean)itrNinzu.next();
						S34HyoukaNinzuListBean s34NinzuBeanEdt = new S34HyoukaNinzuListBean();
						S34HyoukaNinzuListBean s34NinzuBeanDGKAdd = null;
						S34HyoukaNinzuListBean s34NinzuBeanGKBAdd = null;
						S34HyoukaNinzuListBean s34NinzuBeanNTIAdd = null;

						// 移し変え
						s34NinzuBeanEdt.setStrNendo( s34NinzuBean.getStrNendo() );
						s34NinzuBeanEdt.setIntSoushibo( s34NinzuBean.getIntSoushibo() );
						s34NinzuBeanEdt.setIntDai1shibo( s34NinzuBean.getIntDai1shibo() );
						s34NinzuBeanEdt.setIntHyoukaA( s34NinzuBean.getIntHyoukaA() );
						s34NinzuBeanEdt.setIntHyoukaB( s34NinzuBean.getIntHyoukaB() );
						s34NinzuBeanEdt.setIntHyoukaC( s34NinzuBean.getIntHyoukaC() );

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						// 2019/09/30 QQ) 共通テスト対応 ADD START
//						s34NinzuBeanEdt.setIntHyoukaA_Hukumu( s34NinzuBean.getIntHyoukaA_Hukumu() );
//						s34NinzuBeanEdt.setIntHyoukaB_Hukumu( s34NinzuBean.getIntHyoukaB_Hukumu() );
//						s34NinzuBeanEdt.setIntHyoukaC_Hukumu( s34NinzuBean.getIntHyoukaC_Hukumu() );
						// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						s34NinzuListEdt.add( s34NinzuBeanEdt );

						// 加算処理(大学計用)
						if(firstFlgDGK2 == true){
							s34NinzuBeanDGKAdd = (S34HyoukaNinzuListBean) s34NinzuListDGKAdd.get(a);
						}else{
							s34NinzuBeanDGKAdd = new S34HyoukaNinzuListBean();

							s34NinzuBeanDGKAdd.setStrNendo( s34NinzuBean.getStrNendo() );
							s34NinzuBeanDGKAdd.setIntSoushibo( -999 );
							s34NinzuBeanDGKAdd.setIntDai1shibo( -999 );
							s34NinzuBeanDGKAdd.setIntHyoukaA( -999 );
							s34NinzuBeanDGKAdd.setIntHyoukaB( -999 );
							s34NinzuBeanDGKAdd.setIntHyoukaC( -999 );

							// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
							// 2019/09/30 QQ) 共通テスト対応 ADD START
//							s34NinzuBeanDGKAdd.setIntHyoukaA_Hukumu( -999 );
//							s34NinzuBeanDGKAdd.setIntHyoukaB_Hukumu( -999 );
//							s34NinzuBeanDGKAdd.setIntHyoukaC_Hukumu( -999 );
							// 2019/09/30 QQ) 共通テスト対応 ADD END
							// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END
						}

						if(s34NinzuBean.getIntSoushibo()!=-999){
//							s34NinzuBeanDGKAdd.setIntSoushibo( s34NinzuBeanDGKAdd.getIntSoushibo() + s34NinzuBean.getIntSoushibo() );
							if(s34NinzuBeanDGKAdd.getIntSoushibo()!=-999){
								s34NinzuBeanDGKAdd.setIntSoushibo(s34NinzuBeanDGKAdd.getIntSoushibo() + s34NinzuBean.getIntSoushibo());
							}else{
								s34NinzuBeanDGKAdd.setIntSoushibo(s34NinzuBean.getIntSoushibo());
							}
						}
						if(s34NinzuBean.getIntDai1shibo()!=-999){
//							s34NinzuBeanDGKAdd.setIntDai1shibo( s34NinzuBeanDGKAdd.getIntDai1shibo() + s34NinzuBean.getIntDai1shibo() );
							if(s34NinzuBeanDGKAdd.getIntDai1shibo()!=-999){
								s34NinzuBeanDGKAdd.setIntDai1shibo(s34NinzuBeanDGKAdd.getIntDai1shibo() + s34NinzuBean.getIntDai1shibo());
							}else{
								s34NinzuBeanDGKAdd.setIntDai1shibo(s34NinzuBean.getIntDai1shibo());
							}
						}
						if(s34NinzuBean.getIntHyoukaA()!=-999){
//							s34NinzuBeanDGKAdd.setIntHyoukaA( s34NinzuBeanDGKAdd.getIntHyoukaA() + s34NinzuBean.getIntHyoukaA() );
							if(s34NinzuBeanDGKAdd.getIntHyoukaA()!=-999){
								s34NinzuBeanDGKAdd.setIntHyoukaA(s34NinzuBeanDGKAdd.getIntHyoukaA() + s34NinzuBean.getIntHyoukaA());
							}else{
								s34NinzuBeanDGKAdd.setIntHyoukaA(s34NinzuBean.getIntHyoukaA());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						// 2019/09/30 QQ) 共通テスト対応 ADD START
//						if(s34NinzuBean.getIntHyoukaA_Hukumu()!=-999){
//							if(s34NinzuBeanDGKAdd.getIntHyoukaA_Hukumu()!=-999){
//								s34NinzuBeanDGKAdd.setIntHyoukaA_Hukumu(s34NinzuBeanDGKAdd.getIntHyoukaA_Hukumu() + s34NinzuBean.getIntHyoukaA_Hukumu());
//							}else{
//								s34NinzuBeanDGKAdd.setIntHyoukaA_Hukumu(s34NinzuBean.getIntHyoukaA_Hukumu());
//							}
//						}
						// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(s34NinzuBean.getIntHyoukaB()!=-999){
//							s34NinzuBeanDGKAdd.setIntHyoukaB( s34NinzuBeanDGKAdd.getIntHyoukaB() + s34NinzuBean.getIntHyoukaB() );
							if(s34NinzuBeanDGKAdd.getIntHyoukaB()!=-999){
								s34NinzuBeanDGKAdd.setIntHyoukaB(s34NinzuBeanDGKAdd.getIntHyoukaB() + s34NinzuBean.getIntHyoukaB());
							}else{
								s34NinzuBeanDGKAdd.setIntHyoukaB(s34NinzuBean.getIntHyoukaB());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						// 2019/09/30 QQ) 共通テスト対応 ADD START
//						if(s34NinzuBean.getIntHyoukaB_Hukumu()!=-999){
//							if(s34NinzuBeanDGKAdd.getIntHyoukaB_Hukumu()!=-999){
//								s34NinzuBeanDGKAdd.setIntHyoukaB_Hukumu(s34NinzuBeanDGKAdd.getIntHyoukaB_Hukumu() + s34NinzuBean.getIntHyoukaB_Hukumu());
//							}else{
//								s34NinzuBeanDGKAdd.setIntHyoukaB_Hukumu(s34NinzuBean.getIntHyoukaB_Hukumu());
//							}
//						}
						// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START

						if(s34NinzuBean.getIntHyoukaC()!=-999){
//							s34NinzuBeanDGKAdd.setIntHyoukaC( s34NinzuBeanDGKAdd.getIntHyoukaC() + s34NinzuBean.getIntHyoukaC() );
							if(s34NinzuBeanDGKAdd.getIntHyoukaC()!=-999){
								s34NinzuBeanDGKAdd.setIntHyoukaC(s34NinzuBeanDGKAdd.getIntHyoukaC() + s34NinzuBean.getIntHyoukaC());
							}else{
								s34NinzuBeanDGKAdd.setIntHyoukaC(s34NinzuBean.getIntHyoukaC());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						// 2019/09/30 QQ) 共通テスト対応 ADD START
//						if(s34NinzuBean.getIntHyoukaC_Hukumu()!=-999){
//							if(s34NinzuBeanDGKAdd.getIntHyoukaC_Hukumu()!=-999){
//								s34NinzuBeanDGKAdd.setIntHyoukaC_Hukumu(s34NinzuBeanDGKAdd.getIntHyoukaC_Hukumu() + s34NinzuBean.getIntHyoukaC_Hukumu());
//							}else{
//								s34NinzuBeanDGKAdd.setIntHyoukaC_Hukumu(s34NinzuBean.getIntHyoukaC_Hukumu());
//							}
//						}
						// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START

						if(firstFlgDGK2 == true){
							s34NinzuListDGKAdd.set(a,s34NinzuBeanDGKAdd);
						}else{
							s34NinzuListDGKAdd.add(s34NinzuBeanDGKAdd);
						}

						// 加算処理(学部計用)
						if(firstFlgGKB2 == true){
							s34NinzuBeanGKBAdd = (S34HyoukaNinzuListBean) s34NinzuListGKBAdd.get(a);
						}else{
							s34NinzuBeanGKBAdd = new S34HyoukaNinzuListBean();

							s34NinzuBeanGKBAdd.setStrNendo( s34NinzuBean.getStrNendo() );
							s34NinzuBeanGKBAdd.setIntSoushibo( -999 );
							s34NinzuBeanGKBAdd.setIntDai1shibo( -999 );
							s34NinzuBeanGKBAdd.setIntHyoukaA( -999 );
							s34NinzuBeanGKBAdd.setIntHyoukaB( -999 );
							s34NinzuBeanGKBAdd.setIntHyoukaC( -999 );

							// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
							// 2019/09/30 QQ) 共通テスト対応 ADD START
//							s34NinzuBeanGKBAdd.setIntHyoukaA_Hukumu( -999 );
//							s34NinzuBeanGKBAdd.setIntHyoukaB_Hukumu( -999 );
//							s34NinzuBeanGKBAdd.setIntHyoukaC_Hukumu( -999 );
							// 2019/09/30 QQ) 共通テスト対応 ADD END
							// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END
						}

						if(s34NinzuBean.getIntSoushibo()!=-999){
//							s34NinzuBeanGKBAdd.setIntSoushibo( s34NinzuBeanGKBAdd.getIntSoushibo() + s34NinzuBean.getIntSoushibo() );
							if(s34NinzuBeanGKBAdd.getIntSoushibo()!=-999){
								s34NinzuBeanGKBAdd.setIntSoushibo(s34NinzuBeanGKBAdd.getIntSoushibo() + s34NinzuBean.getIntSoushibo());
							}else{
								s34NinzuBeanGKBAdd.setIntSoushibo(s34NinzuBean.getIntSoushibo());
							}
						}
						if(s34NinzuBean.getIntDai1shibo()!=-999){
//							s34NinzuBeanGKBAdd.setIntDai1shibo( s34NinzuBeanGKBAdd.getIntDai1shibo() + s34NinzuBean.getIntDai1shibo() );
							if(s34NinzuBeanGKBAdd.getIntDai1shibo()!=-999){
								s34NinzuBeanGKBAdd.setIntDai1shibo(s34NinzuBeanGKBAdd.getIntDai1shibo() + s34NinzuBean.getIntDai1shibo());
							}else{
								s34NinzuBeanGKBAdd.setIntDai1shibo(s34NinzuBean.getIntDai1shibo());
							}
						}
						if(s34NinzuBean.getIntHyoukaA()!=-999){
//							s34NinzuBeanGKBAdd.setIntHyoukaA( s34NinzuBeanGKBAdd.getIntHyoukaA() + s34NinzuBean.getIntHyoukaA() );
							if(s34NinzuBeanGKBAdd.getIntHyoukaA()!=-999){
								s34NinzuBeanGKBAdd.setIntHyoukaA(s34NinzuBeanGKBAdd.getIntHyoukaA() + s34NinzuBean.getIntHyoukaA());
							}else{
								s34NinzuBeanGKBAdd.setIntHyoukaA(s34NinzuBean.getIntHyoukaA());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						// 2019/09/30 QQ) 共通テスト対応 ADD START
//						if(s34NinzuBean.getIntHyoukaA_Hukumu()!=-999){
//							if(s34NinzuBeanGKBAdd.getIntHyoukaA_Hukumu()!=-999){
//								s34NinzuBeanGKBAdd.setIntHyoukaA_Hukumu(s34NinzuBeanGKBAdd.getIntHyoukaA_Hukumu() + s34NinzuBean.getIntHyoukaA_Hukumu());
//							}else{
//								s34NinzuBeanGKBAdd.setIntHyoukaA_Hukumu(s34NinzuBean.getIntHyoukaA_Hukumu());
//							}
//						}
						// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(s34NinzuBean.getIntHyoukaB()!=-999){
//							s34NinzuBeanGKBAdd.setIntHyoukaB( s34NinzuBeanGKBAdd.getIntHyoukaB() + s34NinzuBean.getIntHyoukaB() );
							if(s34NinzuBeanGKBAdd.getIntHyoukaB()!=-999){
								s34NinzuBeanGKBAdd.setIntHyoukaB(s34NinzuBeanGKBAdd.getIntHyoukaB() + s34NinzuBean.getIntHyoukaB());
							}else{
								s34NinzuBeanGKBAdd.setIntHyoukaB(s34NinzuBean.getIntHyoukaB());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						// 2019/09/30 QQ) 共通テスト対応 ADD START
//						if(s34NinzuBean.getIntHyoukaB_Hukumu()!=-999){
//							if(s34NinzuBeanGKBAdd.getIntHyoukaB_Hukumu()!=-999){
//								s34NinzuBeanGKBAdd.setIntHyoukaB_Hukumu(s34NinzuBeanGKBAdd.getIntHyoukaB_Hukumu() + s34NinzuBean.getIntHyoukaB_Hukumu());
//							}else{
//								s34NinzuBeanGKBAdd.setIntHyoukaB_Hukumu(s34NinzuBean.getIntHyoukaB_Hukumu());
//							}
//						}
						// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(s34NinzuBean.getIntHyoukaC()!=-999){
//							s34NinzuBeanGKBAdd.setIntHyoukaC( s34NinzuBeanGKBAdd.getIntHyoukaC() + s34NinzuBean.getIntHyoukaC() );
							if(s34NinzuBeanGKBAdd.getIntHyoukaC()!=-999){
								s34NinzuBeanGKBAdd.setIntHyoukaC(s34NinzuBeanGKBAdd.getIntHyoukaC() + s34NinzuBean.getIntHyoukaC());
							}else{
								s34NinzuBeanGKBAdd.setIntHyoukaC(s34NinzuBean.getIntHyoukaC());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						// 2019/09/30 QQ) 共通テスト対応 ADD START
//						if(s34NinzuBean.getIntHyoukaC_Hukumu()!=-999){
//							if(s34NinzuBeanGKBAdd.getIntHyoukaC_Hukumu()!=-999){
//								s34NinzuBeanGKBAdd.setIntHyoukaC_Hukumu(s34NinzuBeanGKBAdd.getIntHyoukaC_Hukumu() + s34NinzuBean.getIntHyoukaC_Hukumu());
//							}else{
//								s34NinzuBeanGKBAdd.setIntHyoukaC_Hukumu(s34NinzuBean.getIntHyoukaC_Hukumu());
//							}
//						}
						// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(firstFlgGKB2 == true){
							s34NinzuListGKBAdd.set(a,s34NinzuBeanGKBAdd);
						}else{
							s34NinzuListGKBAdd.add(s34NinzuBeanGKBAdd);
						}

						// 加算処理(日程計用)
						if(firstFlgNTI2 == true){
							s34NinzuBeanNTIAdd = (S34HyoukaNinzuListBean) s34NinzuListNTIAdd.get(a);
						}else{
							s34NinzuBeanNTIAdd = new S34HyoukaNinzuListBean();

							s34NinzuBeanNTIAdd.setStrNendo( s34NinzuBean.getStrNendo() );
							s34NinzuBeanNTIAdd.setIntSoushibo( -999 );
							s34NinzuBeanNTIAdd.setIntDai1shibo( -999 );
							s34NinzuBeanNTIAdd.setIntHyoukaA( -999 );
							s34NinzuBeanNTIAdd.setIntHyoukaB( -999 );
							s34NinzuBeanNTIAdd.setIntHyoukaC( -999 );

							// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
							// 2019/09/30 QQ) 共通テスト対応 ADD START
//							s34NinzuBeanNTIAdd.setIntHyoukaA_Hukumu( -999 );
//							s34NinzuBeanNTIAdd.setIntHyoukaB_Hukumu( -999 );
//							s34NinzuBeanNTIAdd.setIntHyoukaC_Hukumu( -999 );
							// 2019/09/30 QQ) 共通テスト対応 ADD END
							// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END
						}

						if(s34NinzuBean.getIntSoushibo()!=-999){
//							s34NinzuBeanNTIAdd.setIntSoushibo( s34NinzuBeanNTIAdd.getIntSoushibo() + s34NinzuBean.getIntSoushibo() );
							if(s34NinzuBeanNTIAdd.getIntSoushibo()!=-999){
								s34NinzuBeanNTIAdd.setIntSoushibo(s34NinzuBeanNTIAdd.getIntSoushibo() + s34NinzuBean.getIntSoushibo());
							}else{
								s34NinzuBeanNTIAdd.setIntSoushibo(s34NinzuBean.getIntSoushibo());
							}
						}
						if(s34NinzuBean.getIntDai1shibo()!=-999){
//							s34NinzuBeanNTIAdd.setIntDai1shibo( s34NinzuBeanNTIAdd.getIntDai1shibo() + s34NinzuBean.getIntDai1shibo() );
							if(s34NinzuBeanNTIAdd.getIntDai1shibo()!=-999){
								s34NinzuBeanNTIAdd.setIntDai1shibo(s34NinzuBeanNTIAdd.getIntDai1shibo() + s34NinzuBean.getIntDai1shibo());
							}else{
								s34NinzuBeanNTIAdd.setIntDai1shibo(s34NinzuBean.getIntDai1shibo());
							}
						}
						if(s34NinzuBean.getIntHyoukaA()!=-999){
//							s34NinzuBeanNTIAdd.setIntHyoukaA( s34NinzuBeanNTIAdd.getIntHyoukaA() + s34NinzuBean.getIntHyoukaA() );
							if(s34NinzuBeanNTIAdd.getIntHyoukaA()!=-999){
								s34NinzuBeanNTIAdd.setIntHyoukaA(s34NinzuBeanNTIAdd.getIntHyoukaA() + s34NinzuBean.getIntHyoukaA());
							}else{
								s34NinzuBeanNTIAdd.setIntHyoukaA(s34NinzuBean.getIntHyoukaA());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						// 2019/09/30 QQ) 共通テスト対応 ADD START
//						if(s34NinzuBean.getIntHyoukaA_Hukumu()!=-999){
//							if(s34NinzuBeanNTIAdd.getIntHyoukaA_Hukumu()!=-999){
//								s34NinzuBeanNTIAdd.setIntHyoukaA_Hukumu(s34NinzuBeanNTIAdd.getIntHyoukaA_Hukumu() + s34NinzuBean.getIntHyoukaA_Hukumu());
//							}else{
//								s34NinzuBeanNTIAdd.setIntHyoukaA_Hukumu(s34NinzuBean.getIntHyoukaA_Hukumu());
//							}
//						}
						// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(s34NinzuBean.getIntHyoukaB()!=-999){
//							s34NinzuBeanNTIAdd.setIntHyoukaB( s34NinzuBeanNTIAdd.getIntHyoukaB() + s34NinzuBean.getIntHyoukaB() );
							if(s34NinzuBeanNTIAdd.getIntHyoukaB()!=-999){
								s34NinzuBeanNTIAdd.setIntHyoukaB(s34NinzuBeanNTIAdd.getIntHyoukaB() + s34NinzuBean.getIntHyoukaB());
							}else{
								s34NinzuBeanNTIAdd.setIntHyoukaB(s34NinzuBean.getIntHyoukaB());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						// 2019/09/30 QQ) 共通テスト対応 ADD START
//						if(s34NinzuBean.getIntHyoukaB_Hukumu()!=-999){
//							if(s34NinzuBeanNTIAdd.getIntHyoukaB_Hukumu()!=-999){
//								s34NinzuBeanNTIAdd.setIntHyoukaB_Hukumu(s34NinzuBeanNTIAdd.getIntHyoukaB_Hukumu() + s34NinzuBean.getIntHyoukaB_Hukumu());
//							}else{
//								s34NinzuBeanNTIAdd.setIntHyoukaB_Hukumu(s34NinzuBean.getIntHyoukaB_Hukumu());
//							}
//						}
						// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(s34NinzuBean.getIntHyoukaC()!=-999){
//							s34NinzuBeanNTIAdd.setIntHyoukaC( s34NinzuBeanNTIAdd.getIntHyoukaC() + s34NinzuBean.getIntHyoukaC() );
							if(s34NinzuBeanNTIAdd.getIntHyoukaC()!=-999){
								s34NinzuBeanNTIAdd.setIntHyoukaC(s34NinzuBeanNTIAdd.getIntHyoukaC() + s34NinzuBean.getIntHyoukaC());
							}else{
								s34NinzuBeanNTIAdd.setIntHyoukaC(s34NinzuBean.getIntHyoukaC());
							}
						}

						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
						// 2019/09/30 QQ) 共通テスト対応 ADD START
//						if(s34NinzuBean.getIntHyoukaC_Hukumu()!=-999){
//							if(s34NinzuBeanNTIAdd.getIntHyoukaC_Hukumu()!=-999){
//								s34NinzuBeanNTIAdd.setIntHyoukaC_Hukumu(s34NinzuBeanNTIAdd.getIntHyoukaC_Hukumu() + s34NinzuBean.getIntHyoukaC_Hukumu());
//							}else{
//								s34NinzuBeanNTIAdd.setIntHyoukaC_Hukumu(s34NinzuBean.getIntHyoukaC_Hukumu());
//							}
//						}
						// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

						if(firstFlgNTI2 == true){
							s34NinzuListNTIAdd.set(a,s34NinzuBeanNTIAdd);
						}else{
							s34NinzuListNTIAdd.add(s34NinzuBeanNTIAdd);
						}
						a++;
					}

					// 格納
					s34ClassListBeanEdt.setS34HyoukaNinzuList( s34NinzuListEdt );
					s34ClassListEdt.add( s34ClassListBeanEdt );

					// 大学計用
					s34ClassListBeanDGKAdd.setS34HyoukaNinzuList( deepCopyNz(s34NinzuListDGKAdd) );
					if(firstFlgDGK3 == true){
						s34ClassListDGKAdd.set(b,s34ClassListBeanDGKAdd);
					}else{
						s34ClassListDGKAdd.add(s34ClassListBeanDGKAdd);
					}
					// 学部計用
					s34ClassListBeanGKBAdd.setS34HyoukaNinzuList( deepCopyNz(s34NinzuListGKBAdd) );
					if(firstFlgGKB3 == true){
						s34ClassListGKBAdd.set(b,s34ClassListBeanGKBAdd);
					}else{
						s34ClassListGKBAdd.add(s34ClassListBeanGKBAdd);
					}
					// 日程計用
					s34ClassListBeanNTIAdd.setS34HyoukaNinzuList( deepCopyNz(s34NinzuListNTIAdd) );
					if(firstFlgNTI3 == true){
						s34ClassListNTIAdd.set(b,s34ClassListBeanNTIAdd);
					}else{
						s34ClassListNTIAdd.add(s34ClassListBeanNTIAdd);
					}
					s34NinzuListDGKAdd = new ArrayList();
					s34NinzuListGKBAdd = new ArrayList();
					s34NinzuListNTIAdd = new ArrayList();
					b++;
				}
				// 格納
				s34ListBeanEdt.setS34ClassList( s34ClassListEdt );
				s34ListEdt.add( s34ListBeanEdt );
				// 大学計用
				switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
					case 1:
						s34ClassListDGKAddCnt = deepCopyCl( s34ClassListDGKAdd );
						break;
					case 2:
						s34ClassListDGKAddSnd = deepCopyCl( s34ClassListDGKAdd );
						break;
					case 3:
						s34ClassListDGKAddTtl = deepCopyCl( s34ClassListDGKAdd );
						break;
				}
				// 学部計用
				switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
					case 1:
						s34ClassListGKBAddCnt = deepCopyCl( s34ClassListGKBAdd );
						break;
					case 2:
						s34ClassListGKBAddSnd = deepCopyCl( s34ClassListGKBAdd );
						break;
					case 3:
						s34ClassListGKBAddTtl = deepCopyCl( s34ClassListGKBAdd );
						break;
				}
				// 日程計用
				if( cm.toString(s34ListBean.getStrNtiCd()).equals("D") ){		 // 前期
					switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
						case 1:
							s34ClassListNTIAddZenCnt = deepCopyCl( s34ClassListNTIAdd );
							break;
						case 2:
							s34ClassListNTIAddZenSnd = deepCopyCl( s34ClassListNTIAdd );
							break;
						case 3:
							s34ClassListNTIAddZenTtl = deepCopyCl( s34ClassListNTIAdd );
							break;
					}
				}else if( cm.toString(s34ListBean.getStrNtiCd()).equals("E") ){	 // 中期
					switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
						case 1:
							s34ClassListNTIAddChuCnt = deepCopyCl( s34ClassListNTIAdd );
							break;
						case 2:
							s34ClassListNTIAddChuSnd = deepCopyCl( s34ClassListNTIAdd );
							break;
						case 3:
							s34ClassListNTIAddChuTtl = deepCopyCl( s34ClassListNTIAdd );
							break;
					}
				}else if( cm.toString(s34ListBean.getStrNtiCd()).equals("C") ){	 // 後期
					switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
						case 1:
							s34ClassListNTIAddKokCnt = deepCopyCl( s34ClassListNTIAdd );
							break;
						case 2:
							s34ClassListNTIAddKokSnd = deepCopyCl( s34ClassListNTIAdd );
							break;
						case 3:
							s34ClassListNTIAddKokTtl = deepCopyCl( s34ClassListNTIAdd );
							break;
					}
				}else if( cm.toString(s34ListBean.getStrNtiCd()).equals("N") ){	 // 一般型
					switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
						case 1:
							s34ClassListNTIAddIpnCnt = deepCopyCl( s34ClassListNTIAdd );
							break;
						case 2:
							s34ClassListNTIAddIpnSnd = deepCopyCl( s34ClassListNTIAdd );
							break;
						case 3:
							s34ClassListNTIAddIpnTtl = deepCopyCl( s34ClassListNTIAdd );
							break;
					}
				}else if( cm.toString(s34ListBean.getStrNtiCd()).equals("1") ){	 // センター型
					switch( Integer.parseInt(s34ListBean.getStrHyouka()) ){
						case 1:
							s34ClassListNTIAddCenCnt = deepCopyCl( s34ClassListNTIAdd );
							break;
						case 2:
							s34ClassListNTIAddCenSnd = deepCopyCl( s34ClassListNTIAdd );
							break;
						case 3:
							s34ClassListNTIAddCenTtl = deepCopyCl( s34ClassListNTIAdd );
							break;
					}
				}

				s34ClassListDGKAdd = new ArrayList();
				s34ClassListGKBAdd = new ArrayList();
				s34ClassListNTIAdd = new ArrayList();
			}
			//最後に小計・日程計・大学計の保存
			// 学部コードが変わる→学部計の保存
			if(centerGKBFlg == true){
				s34ListBeanGKBAddCnt.setS34ClassList( s34ClassListGKBAddCnt );
				s34ListEdt.add( s34ListBeanGKBAddCnt );
				s34ClassListGKBAddCnt = new ArrayList();
				centerGKBFlg = false;
			}
			if(secondGKBFlg == true){
				s34ListBeanGKBAddSnd.setS34ClassList( s34ClassListGKBAddSnd );
				s34ListEdt.add( s34ListBeanGKBAddSnd );
				s34ClassListGKBAddSnd = new ArrayList();
				secondGKBFlg = false;
			}
			if(totalGKBFlg == true){
				s34ListBeanGKBAddTtl.setS34ClassList( s34ClassListGKBAddTtl );
				s34ListEdt.add( s34ListBeanGKBAddTtl );
				s34ClassListGKBAddTtl = new ArrayList();
				totalGKBFlg = false;
			}

			// 大学コードが変わる→日程計・大学計の保存
			// 日程計
			if(zenCenterNTIFlg == true){
				s34ListBeanNTIAddZenCnt.setS34ClassList( s34ClassListNTIAddZenCnt );
				s34ListEdt.add( s34ListBeanNTIAddZenCnt );
				s34ClassListNTIAddZenCnt = new ArrayList();
				zenCenterNTIFlg = false;
			}
			if(zenSecondNTIFlg == true){
				s34ListBeanNTIAddZenSnd.setS34ClassList( s34ClassListNTIAddZenSnd );
				s34ListEdt.add( s34ListBeanNTIAddZenSnd );
				s34ClassListNTIAddZenSnd = new ArrayList();
				zenSecondNTIFlg = false;
			}
			if(zenTotalNTIFlg == true){
				s34ListBeanNTIAddZenTtl.setS34ClassList( s34ClassListNTIAddZenTtl );
				s34ListEdt.add( s34ListBeanNTIAddZenTtl );
				s34ClassListNTIAddZenTtl = new ArrayList();
				zenTotalNTIFlg = false;
			}
			if(chuCenterNTIFlg == true){
				s34ListBeanNTIAddChuCnt.setS34ClassList( s34ClassListNTIAddChuCnt );
				s34ListEdt.add( s34ListBeanNTIAddChuCnt );
				s34ClassListNTIAddChuCnt = new ArrayList();
				chuCenterNTIFlg = false;
			}
			if(chuSecondNTIFlg == true){
				s34ListBeanNTIAddChuSnd.setS34ClassList( s34ClassListNTIAddChuSnd );
				s34ListEdt.add( s34ListBeanNTIAddChuSnd );
				s34ClassListNTIAddChuSnd = new ArrayList();
				chuSecondNTIFlg = false;
			}
			if(chuTotalNTIFlg == true){
				s34ListBeanNTIAddChuTtl.setS34ClassList( s34ClassListNTIAddChuTtl );
				s34ListEdt.add( s34ListBeanNTIAddChuTtl );
				s34ClassListNTIAddChuTtl = new ArrayList();
				chuTotalNTIFlg = false;
			}
			if(kokCenterNTIFlg == true){
				s34ListBeanNTIAddKokCnt.setS34ClassList( s34ClassListNTIAddKokCnt );
				s34ListEdt.add( s34ListBeanNTIAddKokCnt );
				s34ClassListNTIAddKokCnt = new ArrayList();
				kokCenterNTIFlg = false;
			}
			if(kokSecondNTIFlg == true){
				s34ListBeanNTIAddKokSnd.setS34ClassList( s34ClassListNTIAddKokSnd );
				s34ListEdt.add( s34ListBeanNTIAddKokSnd );
				s34ClassListNTIAddKokSnd = new ArrayList();
				kokSecondNTIFlg = false;
			}
			if(kokTotalNTIFlg == true){
				s34ListBeanNTIAddKokTtl.setS34ClassList( s34ClassListNTIAddKokTtl );
				s34ListEdt.add( s34ListBeanNTIAddKokTtl );
				s34ClassListNTIAddKokTtl = new ArrayList();
				kokTotalNTIFlg = false;
			}
			if(ipnCenterNTIFlg == true){
				s34ListBeanNTIAddIpnCnt.setS34ClassList( s34ClassListNTIAddIpnCnt );
				s34ListEdt.add( s34ListBeanNTIAddIpnCnt );
				s34ClassListNTIAddIpnCnt = new ArrayList();
				ipnCenterNTIFlg = false;
			}
			if(ipnSecondNTIFlg == true){
				s34ListBeanNTIAddIpnSnd.setS34ClassList( s34ClassListNTIAddIpnSnd );
				s34ListEdt.add( s34ListBeanNTIAddIpnSnd );
				s34ClassListNTIAddIpnSnd = new ArrayList();
				ipnSecondNTIFlg = false;
			}
			if(ipnTotalNTIFlg == true){
				s34ListBeanNTIAddIpnTtl.setS34ClassList( s34ClassListNTIAddIpnTtl );
				s34ListEdt.add( s34ListBeanNTIAddIpnTtl );
				s34ClassListNTIAddIpnTtl = new ArrayList();
				ipnTotalNTIFlg = false;
			}
			if(cenCenterNTIFlg == true){
				s34ListBeanNTIAddCenCnt.setS34ClassList( s34ClassListNTIAddCenCnt );
				s34ListEdt.add( s34ListBeanNTIAddCenCnt );
				s34ClassListNTIAddCenCnt = new ArrayList();
				cenCenterNTIFlg = false;
			}
			if(cenSecondNTIFlg == true){
				s34ListBeanNTIAddCenSnd.setS34ClassList( s34ClassListNTIAddCenSnd );
				s34ListEdt.add( s34ListBeanNTIAddCenSnd );
				s34ClassListNTIAddCenSnd = new ArrayList();
				cenSecondNTIFlg = false;
			}
			if(cenTotalNTIFlg == true){
				s34ListBeanNTIAddCenTtl.setS34ClassList( s34ClassListNTIAddCenTtl );
				s34ListEdt.add( s34ListBeanNTIAddCenTtl );
				s34ClassListNTIAddCenTtl = new ArrayList();
				cenTotalNTIFlg = false;
			}

			// 大学計
			if(centerDGKFlg == true){
				s34ListBeanDGKAddCnt.setS34ClassList( s34ClassListDGKAddCnt );
				s34ListEdt.add( s34ListBeanDGKAddCnt );
				s34ClassListDGKAddCnt = new ArrayList();
				centerDGKFlg = false;
			}
			if(secondDGKFlg == true){
				s34ListBeanDGKAddSnd.setS34ClassList( s34ClassListDGKAddSnd );
				s34ListEdt.add( s34ListBeanDGKAddSnd );
				s34ClassListDGKAddSnd = new ArrayList();
				secondDGKFlg = false;
			}
			if(totalDGKFlg == true){
				s34ListBeanDGKAddTtl.setS34ClassList( s34ClassListDGKAddTtl );
				s34ListEdt.add( s34ListBeanDGKAddTtl );
				s34ClassListDGKAddTtl = new ArrayList();
				totalDGKFlg = false;
			}

			// 格納
			s34ItemEdt.setS34List( s34ListEdt );

			return s34ItemEdt;
		}catch(Exception e){
			throw e;
		}
	}

	/**
	 *
	 * @param s34NinzuList
	 * @return
	 */
	private ArrayList deepCopyNz(ArrayList s34NinzuList){
		ListIterator itrNinzu = s34NinzuList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrNinzu.hasNext()){
			S34HyoukaNinzuListBean s34NinzuBean = (S34HyoukaNinzuListBean)itrNinzu.next();
			S34HyoukaNinzuListBean s34NinzuBeanCopy = new S34HyoukaNinzuListBean();

			s34NinzuBeanCopy.setStrNendo( s34NinzuBean.getStrNendo() );
			s34NinzuBeanCopy.setIntSoushibo( s34NinzuBean.getIntSoushibo() );
			s34NinzuBeanCopy.setIntDai1shibo( s34NinzuBean.getIntDai1shibo() );
			s34NinzuBeanCopy.setIntHyoukaA( s34NinzuBean.getIntHyoukaA() );
			s34NinzuBeanCopy.setIntHyoukaB( s34NinzuBean.getIntHyoukaB() );
			s34NinzuBeanCopy.setIntHyoukaC( s34NinzuBean.getIntHyoukaC() );

			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
			// 2019/09/30 QQ) 共通テスト対応 ADD START
//			s34NinzuBeanCopy.setIntHyoukaA_Hukumu( s34NinzuBean.getIntHyoukaA_Hukumu() );
//			s34NinzuBeanCopy.setIntHyoukaB_Hukumu( s34NinzuBean.getIntHyoukaB_Hukumu() );
//			s34NinzuBeanCopy.setIntHyoukaC_Hukumu( s34NinzuBean.getIntHyoukaC_Hukumu() );
			// 2019/09/30 QQ) 共通テスト対応 ADD END
			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

			copyList.add( s34NinzuBeanCopy );
		}

		return copyList;
	}

	/**
	 *
	 * @param s34ClassList
	 * @return
	 */
	private ArrayList deepCopyCl(ArrayList s34ClassList){
		ListIterator itrClass = s34ClassList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrClass.hasNext()){
			S34ClassListBean s34ClassBean = (S34ClassListBean)itrClass.next();
			S34ClassListBean s34ClassBeanCopy = new S34ClassListBean();

			s34ClassBeanCopy.setStrClass( s34ClassBean.getStrClass() );
			s34ClassBeanCopy.setStrGakkomei( s34ClassBean.getStrGakkomei() );
			s34ClassBeanCopy.setStrGrade( s34ClassBean.getStrGrade() );
			s34ClassBeanCopy.setS34HyoukaNinzuList( deepCopyNz(s34ClassBean.getS34HyoukaNinzuList()) );

			copyList.add( s34ClassBeanCopy );
		}

		return copyList;
	}

	/*
	 * 	Excel編集
	 * 		HSSFWorkbook 	workbook: Excelワークブック
	 * 		ArrayList 		s34HyoukaNinzuList:評価別人数リスト
	 * 		int				intRow:開始行
	 * 		int				intCol:開始列
	 * 		戻り値: なし
	 */
	private boolean setData(HSSFSheet workSheet, S34HyoukaNinzuListBean s34HyoukaNinzuListBean, int intRow, int intCol ){

		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		int		row = intRow;


		// 基本ファイルを読込む
		try{

			//模試年度
			workCell = cm.setCell( workSheet, workRow, workCell, row, 4);
			workCell.setCellValue( s34HyoukaNinzuListBean.getStrNendo() + "年度" );

			//全国総志望者数
			if( s34HyoukaNinzuListBean.getIntSoushibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol);
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntSoushibo() );
			}

			//志望者数（第一志望）
			if( s34HyoukaNinzuListBean.getIntDai1shibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 1);
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntDai1shibo() );
			}

			//評価別人数
			if( s34HyoukaNinzuListBean.getIntHyoukaA() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 2 );
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntHyoukaA() );
			}

			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
			// 2019/09/30 QQ) 共通テスト対応 ADD START
//			if( s34HyoukaNinzuListBean.getIntHyoukaA_Hukumu() != -999 ){
//				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
//				workCell.setCellValue( s34HyoukaNinzuListBean.getIntHyoukaA_Hukumu() );
//			}
			// 2019/09/30 QQ) 共通テスト対応 ADD END
			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

			if( s34HyoukaNinzuListBean.getIntHyoukaB() != -999 ){
			        // 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
				// 2019/09/30 QQ) 共通テスト対応 UPD START
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
				//workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
				// 2019/09/30 QQ) 共通テスト対応 UPD END
				// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntHyoukaB() );
			}

			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
			// 2019/09/30 QQ) 共通テスト対応 ADD START
//			if( s34HyoukaNinzuListBean.getIntHyoukaB_Hukumu() != -999 ){
//				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 5 );
//				workCell.setCellValue( s34HyoukaNinzuListBean.getIntHyoukaB_Hukumu() );
//			}
			// 2019/09/30 QQ) 共通テスト対応 ADD END
			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

			if( s34HyoukaNinzuListBean.getIntHyoukaC() != -999 ){
			        // 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD START
				// 2019/09/30 QQ) 共通テスト対応 UPD START
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
				//workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 6 );
				// 2019/09/30 QQ) 共通テスト対応 UPD END
				// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 UPD END
				workCell.setCellValue( s34HyoukaNinzuListBean.getIntHyoukaC() );
			}

			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL START
			// 2019/09/30 QQ) 共通テスト対応 ADD START
//			if( s34HyoukaNinzuListBean.getIntHyoukaC_Hukumu() != -999 ){
//				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 7 );
//				workCell.setCellValue( s34HyoukaNinzuListBean.getIntHyoukaC_Hukumu() );
//			}
			// 2019/09/30 QQ) 共通テスト対応 ADD END
			// 2019/11/20 QQ)Ooseto 英語認定試験延期対応 DEL END

		}
		catch(Exception e){
			return false;
		}

		return true;
	}

}
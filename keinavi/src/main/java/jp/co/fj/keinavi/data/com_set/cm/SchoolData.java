/*
 * 作成日: 2004/07/01
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.com_set.cm;

import java.io.Serializable;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class SchoolData implements Serializable {

	// 高校コード
	private String schoolCode = null;
	// 高校名
	private String schoolName = null;
	// ランク
	private String rank = null;
	// 受験人数
	private int examinees = 0;
	// 所在地
	private String location = null;
	// 登録済みかどうか
	private boolean registed = false;

	/**
	 * コンストラクタ
	 */
	public SchoolData(String schoolCode) {
		super();
		this.schoolCode = schoolCode;
	}

	/**
	 * コンストラクタ
	 */
	public SchoolData() {
		super();
	}

	/**
	 * @return
	 */
	public int getExaminees() {
		return examinees;
	}

	/**
	 * @return
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return
	 */
	public String getRank() {
		return rank;
	}

	/**
	 * @return
	 */
	public String getSchoolCode() {
		return schoolCode;
	}

	/**
	 * @return
	 */
	public String getSchoolName() {
		return schoolName;
	}

	/**
	 * @param i
	 */
	public void setExaminees(int i) {
		examinees = i;
	}

	/**
	 * @param string
	 */
	public void setLocation(String string) {
		location = string;
	}

	/**
	 * @param string
	 */
	public void setRank(String string) {
		rank = string;
	}

	/**
	 * @param string
	 */
	public void setSchoolCode(String string) {
		schoolCode = string;
	}

	/**
	 * @param string
	 */
	public void setSchoolName(String string) {
		schoolName = string;
	}

	/**
	 * @return
	 */
	public boolean isRegisted() {
		return registed;
	}

	/**
	 * @param b
	 */
	public void setRegisted(boolean b) {
		registed = b;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return schoolCode.equals(((SchoolData)obj).getSchoolCode());
	}

}

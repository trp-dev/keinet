package jp.co.fj.keinavi.forms.profile;

/**
 * 
 * 
 * 2005.02.25	Yoshimoto KAWAI - Totec
 * 				年度対応
 *            
 * 2005.10.12	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 * 
 * @author kawai
 * 
 */
public class W005Form extends W004Form {

	// 担当クラス
	private String[] classes = null;
	// 年度
	private String year = null;
	
	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * @return classes を戻します。
	 */
	public String[] getClasses() {
		return classes;
	}

	/**
	 * @param classes 設定する classes。
	 */
	public void setClasses(String[] classes) {
		this.classes = classes;
	}

	/**
	 * @return year を戻す。
	 */
	public String getYear() {
		return this.year;
	}
	/**
	 * @param year year を設定する。
	 */
	public void setYear(String year) {
		this.year = year;
	}
}

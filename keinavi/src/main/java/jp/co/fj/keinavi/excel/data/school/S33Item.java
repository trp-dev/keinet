package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * 設問別成績（校内成績）データクラス
 * 作成日: 2004/07/08
 * @author	T.Kuzuno
 */
public class S33Item {
	//学校名
	private String strGakkomei = "";
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//表フラグ
	private int intHyouFlg = 0;
	//グラフフラグ
	private int intGraphFlg = 0;
	// 2019/09/04 QQ)Tanioka  ADD START
	//国語 評価別人数フラグ
	private int intKokugoFlg = 0;
	//小設問別正答状況フラグ
	private int intStatusFlg = 0;
	// 2019/09/04 QQ)Tanioka  ADD END
	//セキュリティスタンプフラグ
	private int intSecuFlg = 0;
	//データリスト
	private ArrayList s33List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;
	// 2019/09/05 QQ)Ooseto 共通テスト対応 ADD START
	// 国語評価別人数データリスト
	private ArrayList s33KokugoList = new ArrayList();
        // 2019/09/05 QQ)Ooseto 共通テスト対応 ADD END
	// 2019/09/11 QQ)Ooseto 共通テスト対応 ADD START
	// 小設問別正答状況データリスト
	private ArrayList s33StatusList = new ArrayList();
        // 2019/09/11 QQ)Ooseto 共通テスト対応 ADD END

	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public int getIntHyouFlg() {
		return this.intHyouFlg;
	}
	public int getIntGraphFlg() {
		return this.intGraphFlg;
	}
	// 2019/09/04 QQ)Tanioka  ADD START
	public int getIntKokugoFlg() {
		return this.intKokugoFlg;
	}
	public int getIntStatusFlg() {
		return this.intStatusFlg;
	}
	// 2019/09/04 QQ)Tanioka  ADD END
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public ArrayList getS33List() {
		return this.s33List;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}
	// 2019/09/05 QQ)Ooseto 共通テスト対応 ADD START
	public ArrayList getS33KokugoList() {
	    return this.s33KokugoList;
	}
        // 2019/09/05 QQ)Ooseto 共通テスト対応 ADD END
        // 2019/09/11 QQ)Ooseto 共通テスト対応 ADD START
	public ArrayList getS33StatusList() {
	    return this.s33StatusList;
	}
        // 2019/09/11 QQ)Ooseto 共通テスト対応 ADD END

	/*---------------*/
	/* Set */
	/*---------------*/
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setIntHyouFlg(int intHyouFlg) {
		this.intHyouFlg = intHyouFlg;
	}
	public void setIntGraphFlg(int intGraphFlg) {
		this.intGraphFlg = intGraphFlg;
	}
	// 2019/09/04 QQ)Tanioka  ADD START
	public void setIntKokugoFlg(int intKokugoFlg) {
		this.intKokugoFlg = intKokugoFlg;
	}
	public void setIntStatusFlg(int intStatusFlg) {
		this.intStatusFlg = intStatusFlg;
	}
	// 2019/09/04 QQ)Tanioka  ADD END
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setS33List(ArrayList s33List) {
		this.s33List = s33List;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}
	// 2019/09/05 QQ)Ooseto 共通テスト対応 ADD START
	public void setS33KokugoList(ArrayList s33KokugoList) {
	    this.s33KokugoList = s33KokugoList;
	}
        // 2019/09/05 QQ)Ooseto 共通テスト対応 ADD END
        // 2019/09/11 QQ)Ooseto 共通テスト対応 ADD START
	public void setS33StatusList(ArrayList s33StatusList) {
	    this.s33StatusList = s33StatusList;
	}
        // 2019/09/11 QQ)Ooseto 共通テスト対応 ADD END

}

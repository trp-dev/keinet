package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.data.individual.PlanUnivData;
import jp.co.fj.keinavi.data.individual.ScheduleDetailData;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.date.ShortDatePlusUtil;
import jp.co.fj.keinavi.util.sql.QueryLoader;

/**
 *
 * 受験予定大学更新
 * 
 * <2010年度改修>
 * 2010.01.19	Tomohisa YAMADA - TOTEC
 * 				新規作成
 *
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class PlanUnivUpdateBean extends PlanUnivBean {
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		if (univValue != null) {
			single(individualId, univValues, univValue, updateUnivValue);
		}
		else {
			multi(individualId, univValues);
		}
	}
	
	/**
	 * 単体更新
	 * ※PlanUnivBeanから移動
	 */
	private void single(
			String individualId,
			String[] univValues, 
			String univValue,
			String updateUnivValue) throws Exception {
		
		//編集前　→　大学
		String[] oldUniv = CollectionUtil.splitComma(univValue);
		String oldEntExamTypeCd	 = oldUniv[3];	//変更前　→　入試形態
		
		//編集後　→　大学
		Collection planDatas = createInsertUnivDatas(univValues, updateUnivValue);
		if(planDatas == null) {
			return;
		}
		
		String[] univ = CollectionUtil.splitComma(updateUnivValue);
		//大学
		String univCd			 = univ[0];	//大学
		String facultyCd		 = univ[1];	//学部
		String deptCd			 = univ[2];	//学科
		String entExamTypeCd	 = univ[3];	//入試形態
		
		//登録のチェック条件
		//１．重複していない　→　新規登録扱い
		//２．編集前の入試形態と編集後の入試形態が同じ場合　→　更新処理扱い
		//データを削除してから、新しいデータを登録するため、同じ処理でいける。
		if(execChackPlanUniv(getIndividualId(), univCd, facultyCd, deptCd, entExamTypeCd) || 
			entExamTypeCd.equals(oldEntExamTypeCd)) {
			//登録可能
			//１，１，１のデータが無いようであれば、作成し追加
			planDatas = setNecessaryData(planDatas);
			delete(individualId, univValue);
			execInsertPlanUniv(individualId, planDatas);//登録処理
		} else {
			setErrorMessage("受験予定が既に登録されています\\nこの受験予定は登録できません");
		}
	}
	
	/**
	 * 複数更新
	 * ※PlanUnivBeanから移動
	 */
	private void multi(String individualId, String[] univValues) throws Exception {
		
		final int MAX_DATECOUNT = 10;
		
		//データ無し(ありえない)
		if(univValues == null || univValues.length == 0) {
			return;
		}
		
		//univValues -> planDatas
		Map planDataMap = new HashMap();
		for(int i = 0; i < univValues.length; i++) {
			String[] univValue = CollectionUtil.deconcatComma(univValues[i]);
			String planKey = univValue[0]+univValue[1]+univValue[2]+univValue[3]+univValue[4]+univValue[5]+univValue[6];
			
			if(univValue[10].equals("off") && !(univValue[4].equals("1") && univValue[5].equals("1") && univValue[6].equals("1"))) {
				continue;
			}
			
			//Map -> get
			PlanUnivData planData = (PlanUnivData)planDataMap.get(planKey);
			
			if (planData == null) {
				//planData -> new
				planData = new PlanUnivData();
				planData.setExamPlanDates(new String[10]);//入試日
				planData.setProvEntExamSite(new String[10]);//試験地
				planData.setUnivCd(univValue[0]);	//大学コード
				planData.setFacultyCd(univValue[1]);	//学部コード
				planData.setDeptCd(univValue[2]);	//学科コード
				planData.setEntExamTypeCd(univValue[3]);	//入試形態コード
				planData.setYear(univValue[8]);
				planData.setExamDiv(univValue[9]);
				
				planData.setSchedule(new ScheduleDetailData(
						Integer.parseInt(univValue[4]), 
						Integer.parseInt(univValue[5]), 
						Integer.parseInt(univValue[6])));
			}
			
			if(univValue.length < 11) {
				//何もしない
			} else if(univValue[10].equals("on")) {
				//日程の空きを探す
				int j = 0;
				while(j < MAX_DATECOUNT){
					if(planData.getExamPlanDates()[j] == null) {break;}
					j++;
				}
				//既に１０日入っている
				if(j >= MAX_DATECOUNT){
					setErrorMessage("試験日が１０件を超えました。登録できる試験日は１０件までです。");
					return;
				}
				//日程を詰める
				planData.getExamPlanDates()[j] = ShortDatePlusUtil.getUnformattedString(univValue[7]);
				if(univValue.length >= 12) {
					planData.getProvEntExamSite()[j] = univValue[11];
				}
			}
			//Map -> set
			planDataMap.put(planKey, planData);
		}
		
		PreparedStatement ps1 = null;//delete
		PreparedStatement ps2 = null;//update

		try {
			
			//区分1が「1」または区分2が「1」または区分3が「01」のレコードを削除
			ps1 = conn.prepareStatement(QueryLoader.getInstance().load("i11").toString());
			ps1.setString(1, individualId);
			ps1.setString(2, "1");
			ps1.setString(3, "1");
			ps1.setString(4, "01");
			ps1.executeUpdate();
			
			//実施日と試験地を更新
			ps2 = conn.prepareStatement(QueryLoader.getInstance().load("i10").toString());
			ps2.setString(26, "1");
			ps2.setString(27, "1");
			ps2.setString(28, "01");
						
			for(java.util.Iterator it = planDataMap.values().iterator(); it.hasNext();) {
				
				PlanUnivData plan = (PlanUnivData)it.next();
								
				//更新（区分１，１，１）
				if (plan.getSchedule().getEntExamDiv_1_2order() == 1 
						&& plan.getSchedule().getSchoolProventDiv() == 1 
						&& plan.getSchedule().getEntExamDiv_1_2term() == 1) {
					
					ps2.setString(1,  plan.getExamPlanDates()[0]);
					ps2.setString(2,  plan.getExamPlanDates()[1]);
					ps2.setString(3,  plan.getExamPlanDates()[2]);
					ps2.setString(4,  plan.getExamPlanDates()[3]);
					ps2.setString(5,  plan.getExamPlanDates()[4]);
					ps2.setString(6,  plan.getExamPlanDates()[5]);
					ps2.setString(7,  plan.getExamPlanDates()[6]);
					ps2.setString(8,  plan.getExamPlanDates()[7]);
					ps2.setString(9,  plan.getExamPlanDates()[8]);
					ps2.setString(10, plan.getExamPlanDates()[9]);
					ps2.setString(11, plan.getProvEntExamSite()[0]);
					ps2.setString(12, plan.getProvEntExamSite()[1]);
					ps2.setString(13, plan.getProvEntExamSite()[2]);
					ps2.setString(14, plan.getProvEntExamSite()[3]);
					ps2.setString(15, plan.getProvEntExamSite()[4]);
					ps2.setString(16, plan.getProvEntExamSite()[5]);
					ps2.setString(17, plan.getProvEntExamSite()[6]);
					ps2.setString(18, plan.getProvEntExamSite()[7]);
					ps2.setString(19, plan.getProvEntExamSite()[8]);
					ps2.setString(20, plan.getProvEntExamSite()[9]);
					ps2.setString(21, individualId);
					ps2.setString(22, plan.getUnivCd());
					ps2.setString(23, plan.getFacultyCd());
					ps2.setString(24, plan.getDeptCd());
					ps2.setString(25, plan.getEntExamTypeCd());
					ps2.executeUpdate();
					
				}
				//追加
				else {
					
					PlanUnivInsertBean insert = 
						new PlanUnivInsertBean(
								individualId, 
								plan.getUnivCd(), 
								plan.getFacultyCd(), 
								plan.getDeptCd(), 
								plan.getEntExamTypeCd(), 
								plan.getSchedule().getEntExamDiv_1_2order(), 
								plan.getSchedule().getSchoolProventDiv(), 
								plan.getSchedule().getEntExamDiv_1_2term(), 
								plan.getYear(), 
								plan.getExamDiv(), 
								"", 
								plan.getExamPlanDates()[0], 
								plan.getExamPlanDates()[1], 
								plan.getExamPlanDates()[2], 
								plan.getExamPlanDates()[3], 
								plan.getExamPlanDates()[4], 
								plan.getExamPlanDates()[5], 
								plan.getExamPlanDates()[6], 
								plan.getExamPlanDates()[7], 
								plan.getExamPlanDates()[8], 
								plan.getExamPlanDates()[9], 
								plan.getProvEntExamSite()[0], 
								plan.getProvEntExamSite()[1], 
								plan.getProvEntExamSite()[2], 
								plan.getProvEntExamSite()[3], 
								plan.getProvEntExamSite()[4], 
								plan.getProvEntExamSite()[5], 
								plan.getProvEntExamSite()[6], 
								plan.getProvEntExamSite()[7], 
								plan.getProvEntExamSite()[8], 
								plan.getProvEntExamSite()[9]);
					insert.setConnection(null, conn);
					insert.execute();
				}
			}
			
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
			
			setErrorMessage("受験予定大学を更新しました");
			
		} finally {
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
		}
	}
	
	/**
	 * 削除
	 */
	private void delete(
			final String individualId,
			final String univValue) throws Exception{

		// 対象はカンマ区切りで渡される
		final String[] univValues = CollectionUtil.deconcatComma(univValue);
		
		PlanUnivDeleteBean bean = 
			new PlanUnivDeleteBean(individualId);
		bean.setUnivcd(univValues[0]);
		bean.setFacultycd(univValues[1]);
		bean.setDeptcd(univValues[2]);
		bean.setMode(univValues[3]);
		bean.setConnection(null, conn);
		bean.execute();
	}
	
}

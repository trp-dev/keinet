/*
 * 作成日: 2004/09/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.admin;

/**
 * @author 奥村ゆかり
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
import java.text.CharacterIterator;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

import jp.co.fj.keinavi.util.JpnStringConv;

public class FormatChecker {

	public static void main(String[] args) {
		String str ="0123456789";
		System.out.println(getByteCutStr(str, 0, 4));
		System.out.println(isDate("19860422"));
	}
	
	// 対象となるデータを抽出
	public static String getSelData(
		LinkedList subCountData,
		int index,
		String key)
		 {
		return (String) ((HashMap) subCountData.get(index)).get(key);
	}

	/**
	 * 2004.11.12
	 * Yoshimoto KAWAI - Totec
	 * 
	 * 文字バイト配列を指定されたバイトで切り出す
	 * （オール９はNULLで返す）
	 * @param bytes 文字バイト配列
	 * @param start バイト切り出し開始位置
	 * @param end バイト切り出し終了位置
	 * @return 文字列
	 */
	public static String byte2RecordValue(byte[] array, int start, int end, boolean conv) {
		if (array == null) return null;
	
		byte[] container = new byte[end - start];

		for (int i = start; i < end; i++) {
			container[i - start] = array[i];
		}

		String value = new String(container);

		// 999... ならNULL
		if (conv) {
			for (int i=0; i<value.length(); i++) {
				if (value.charAt(i) != '9') return value;
			}

			return null;
			
		// そのまま
		} else {
			return value;
		}
	}
	
	/**
	 * @param array
	 * @param start
	 * @param end
	 * @return
	 */
	public static String byte2RecordValue(byte[] array, int start, int end) {
		return byte2RecordValue(array, start, end, true);
	}
	
	/**
	* 文字列を指定されたバイトで切り出す
	* @param _str 対象の値
	* @param start バイト切り出し開始位置
	* @param close バイト切り出し終了位置
	* @param judge 999 ・・をNULLにする
	* @return 文字列
	*/
	public static String getByteCutStr(String str, int start, int close) {
		return getByteCutStr(str, start, close, true);
	}
	
	
	/**
	* 文字列を指定されたバイトで切り出す
	* @param _str 対象の値
	* @param start バイト切り出し開始位置
	* @param close バイト切り出し終了位置
	* @param judge 999 ・・をNULLにする
	* @return 文字列
	*/
	public static String getByteCutStr(String str, int start, int close, boolean judge) {
		if (str != null) {
			byte allBy[] = str.getBytes();
			byte newBy[] = new byte[close - start];

			int count = 0;
			for (int i = start; i < close; i++) {
				newBy[count] = allBy[i];
				count++;
			}
			if(judge)return chgStr( new String(newBy));
			else return new String(newBy);
		} else {
			return str;
		}

	}


	/**
	* 999・・をNULLに変換
	* @param _str 対象の値
	* @return 文字列
	*/
	private static String chgStr(String str){
		boolean judge = false;
		if(str==null)return str;
		else{
			int size = str.length();
			for(int n=0; n<size; n++){
				if(!str.substring(n,n+1).equals("9")){
					judge = true;
					break;
				}
			}
		}
		if(judge == false)str = null;
		 return str;
	}
	
	// char
	public static String str2Char32(String str, int index) {
		String s = "";
		if (str != null) {

			int count = str.getBytes().length;
			int n = index - count;
			for (int i = 0; i < n; i++) {
				s += " ";
			}
			return str + s;
		} else {
			return str;
		}

	}

	/**
	* YEAR値チェック：4桁の数値か
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isYear(String _str) {
		boolean bl = false;
		try {
			if (isEqualSize(4, _str) && isNumber(_str))
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	 * GRADE値チェック:1〜5の数値か
	 * @param _str
	 * @return 判定
	 */
	public static boolean isGrade(String _str) {
		boolean bl = false;
		try {
			if (isRangeList("12345", _str) && isNumber(_str))
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	 * Class値チェック
	 * @param _str
	 * @return
	 */
	public static boolean isClass(String _str) {
		// ------------------------------------------------------------
		// 2004.11.09
		// Yoshimoto KAWAI - Totec
		// 長さ0はﾀﾞﾒ
		if (_str.length() == 0) return false;
		// ------------------------------------------------------------

		boolean bl = false;
		try {
			if (isUnderSize(2, _str)
				&& isRangeList(
					"abcdefghijklmnopqrstuvwxyz"
						+ "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
						+ "0123456789",
					_str))
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	 * classNumber値チェック
	 * @param _str
	 * @return
	 */
	public static boolean isClassNumber(String _str) {
		// ------------------------------------------------------------
		// 2004.11.09
		// Yoshimoto KAWAI - Totec
		// 長さ0はﾀﾞﾒ
		if (_str.length() == 0) return false;
		// ------------------------------------------------------------

		boolean bl = false;
		try {
			if (isUnderSize(5, _str) && isHankaku(_str))
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	 * Distinction値チェック
	 * @param _str
	* @return 判定
	 */
	public static boolean isDistinction(String _str) {
		boolean bl = false;
		try {
			if (_str.equals("1") || _str.equals("2") || _str.equals("9"))
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* kanaName値チェック
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isKanaName(String _str) {
		// ------------------------------------------------------------
		// 2004.11.09
		// Yoshimoto KAWAI - Totec
		// 長さ0はﾀﾞﾒ
		if (_str.length() == 0) return false;
		// ------------------------------------------------------------

		boolean bl = false;
		try {
			_str = StringConv(_str);
			if (isUnderSize(14, _str)
				&& isRangeList(
					"ｦｧｨｩｪｫｬｭｮｯﾂｰｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜﾝﾞﾟ ",
					_str))
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* kanjiName値チェック
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isKanjiName(String _str) {
		boolean bl = false;
		try {
			if (isUnderSize(10, _str) && isZenkaku(_str))
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* BirthYear値チェック
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isBirthYear(String _str) {
		boolean bl = false;
		try {
			if (isEqualSize(4, _str) && isNumber(_str))
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* BirthMonth値チェック
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isBirthMonth(String _str) {
		boolean bl = false;
		try {
			if (isEqualSize(2, _str) && isNumber(_str))
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* BirthDate値チェック
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isBirthDate(String _str) {
		boolean bl = false;
		try {
			if (isEqualSize(2, _str) && isNumber(_str))
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* telNumber値チェック
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isTelNumber(String _str) {
		boolean bl = false;
		try {
			if (isUnderSize(11, _str) && isNumber(_str))
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* 全角文字であるか
	* @param _str 対象の値
	* @return 判定
	*/

	public static boolean isZenkaku(String str) {
		int num1 = str.getBytes().length;
		int num2 = str.length() * 2;
		return num1 == num2;
	}

	/**
	* 半角文字であるか
	* @param _str 対象の値
	* @return 判定
	*/
	public static boolean isHankaku(String str) {
		int num1 = str.getBytes().length;
		int num2 = str.length();
		return num1 == num2;
	}

	/**
	* 日付の文字列が日付として正しいか
	* @param sDate 対象の値
	* @return 判定
	*/
	public static boolean isDate(String sDate) {
		boolean judge = true;
		SimpleDateFormat sdf01 = new SimpleDateFormat("yyyyMMdd");
		
		//日付/時刻解析を厳密に行うかどうかを設定。
		//falseの場合は厳密な解析
		sdf01.setLenient(false);
		try {
			//日付/時刻文字列を解析
			sdf01.parse(sDate);
			
			String inputDate = sDate.substring(0,4);
			
			if( Integer.parseInt( getCurrentYear()) >= Integer.parseInt(inputDate)&&
			Integer.parseInt( "1900") <= Integer.parseInt(inputDate)
			){
				judge = true;
			}else{
				judge = false;
			}
		} catch (ParseException px) {
			judge = false;
		} catch (Exception ex) {
			judge = false;
		}
		return judge;
	}

	
	/**
	*
	* 今年度の取得
	*
	* @param index 書式
	* @return 現在の日付を設定された書式の文字型で返します。
	*/
	private static String getCurrentYear() {
		Date currentTime = Calendar.getInstance().getTime();
		DateFormat form = new SimpleDateFormat("yyyy");
		String timeStr = form.format(currentTime);
		return timeStr;
	}
	
	
	/**
	 * 
	 * 全角かな(カナ)を半角カナに変換
	 * 
	 * @param str
	 * @return 変換後文字列
	 */
	public static String StringConv(String str) {
		if(str!=null)
			str = JpnStringConv.hkana2Han(str);
		return str;
	}

	/**
	 * 
	 * 半角カナを全角かなに変換
	 * 
	 * @param str
	 * @return 変換後文字列
	 */
	public static String StringConvView(String str) {
		if(str!=null)
			str = JpnStringConv.kkana2Hkana(JpnStringConv.kkanaHan2Zen(str));
		return str;
	}

	/**
	 * 文字列からindexを消去
	 * 
	 * 2004.11.19
	 * Yoshimoto KAWAI - Totec
	 * 高速化改善
	 * 
	 * @param str
	 * @return 変換後文字列
	 */
	public static String StringDelIndex(String str, String indexs) {
		if (str == null) return str;
		
		StringBuffer buff = new StringBuffer();
		for (int i=0; i<str.length(); i++) {
			char current = str.charAt(i);
			boolean judge = true;

			for (int j=0; j<indexs.length(); j++) {
				if (current == indexs.charAt(j)) {
					judge = false;
					break;
				} 
			}

			if (judge) buff.append(current);
		}

		return buff.toString();
	}

	/**
	* 
	* add 2004/04/06 okumura
	* 任意の使用不可文字チェック
	* 
	* @param _permitted 許可される文字のリスト
	* @param _str 対象の値
	* @return 許可されない文字列がセットされた場合trueを返す。それ以外はflaseを返す。
	*/
	private static boolean isNoRangeList(String _permitted, String _str) {
		boolean bl = false;
		try {
			//_permitted = "1234567890abcdefABCDEF";  // 許可されない文字のリスト
			StringCharacterIterator sci_input =
				new StringCharacterIterator(_str);
			for (char c_input = sci_input.first();
				c_input != CharacterIterator.DONE;
				c_input = sci_input.next()) {
				if (_permitted.indexOf((int) c_input) != -1) {
					bl = true;
					break;
				}
			}
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* 任意の使用可能文字チェック
	* @param _permitted 許可される文字のリスト
	* @param _str 対象の値
	* @return 許可された文字列がセットされた場合trueを返す。それ以外はflaseを返す。
	*/
	private static boolean isRangeList(String _permitted, String _str) {
		boolean bl = true;
		try {
			//_permitted = "1234567890abcdefABCDEF";  // 許される文字のリスト
			StringCharacterIterator sci_input =
				new StringCharacterIterator(_str);
			for (char c_input = sci_input.first();
				c_input != CharacterIterator.DONE;
				c_input = sci_input.next()) {
				if (_permitted.indexOf((int) c_input) == -1) {
					bl = false;
					break;
				}
			}
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* 任意の桁範囲チェック >
	* @param _start_figure 対象の範囲数値最少
	* @param _close_figure 対象の範囲数値最大
	* @param _number 対象の数値
	* @return 判定
	*/
	private static boolean isRangeSize(
		int _start_figure,
		int _close_figure,
		int _number) {
		boolean bl = false;
		try {
			if (((String) Long.toString(_number)).length() >= _start_figure
				&& ((String) Long.toString(_number)).length() <= _close_figure)
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* 任意の桁範囲チェック >
	* @param _start_figure 対象の範囲数値最少
	* @param _close_figure 対象の範囲数値最大
	* @param _str 対象の値
	* @return 判定
	*/
	private static boolean isRangeSize(
		int _start_figure,
		int _close_figure,
		String _str) {
		boolean bl = false;
		try {
			if (_str.length() >= _start_figure
				&& _str.length() <= _close_figure)
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* 任意の桁越えチェック >
	* @param _figure 桁数
	* @param _number 対象の数値
	* @return 判定
	*/
	private static boolean isUnderSize(int _figure, long _number) {
		boolean bl = false;
		try {
			if (((String) Long.toString(_number)).length() <= _figure)
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* 任意の桁越えチェック >
	* @param _figure 桁数
	* @param _str 対象の値
	* @return 判定
	*/
	private static boolean isUnderSize(int _figure, String _str) {
		boolean bl = false;
		try {
			if (_str.length() <= _figure)
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* 任意の桁数チェック =
	* @param _figure 桁数
	* @param _number 対象の数値
	* @return 判定
	*/
	private static boolean isEqualSize(int _figure, long _number) {
		boolean bl = false;
		try {
			if (((String) Long.toString(_number)).length() == _figure)
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* 任意の桁チェック =
	* @param _figure 桁数
	* @param _str 対象の値
	* @return 判定
	*/
	private static boolean isEqualSize(int _figure, String _str) {
		boolean bl = false;
		try {
			if (_str.length() == _figure)
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* 桁数の取得
	* @param _number 対象の数値
	* @return 桁数
	*/
	private static int getSize(long _number) {
		int figure = 0;
		try {
			figure = ((String) Long.toString(_number)).length();
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return figure;
	}

	/**
	* 桁数の取得
	* @param _str 対象の値
	* @return 桁数
	*/
	private static int getSize(String _str) {
		int figure = 0;
		try {
			figure = _str.length();
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return figure;
	}

	/**
	* 数値の判定
	* @param _str 対象の値
	* @return 判定
	*/
	private static boolean isNumber(String _str) {
		boolean bl = false;
		try {
			Long nm = new Long(_str);
			if (nm != null)
				bl = true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bl;
	}

	/**
	* 数値の取得
	* @param _str 対象の値
	* @return long数値
	*/
	private static long toLongNumber(String _str) {
		long num = 0L;
		try {
			num = Long.parseLong(_str);
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return num;
	}

	/**
	* 数値の取得
	* @param _str 対象の値
	* @return int数値
	*/
	private static int toIntNumber(String _str) {
		int num = 0;
		try {
			num = Integer.parseInt(_str);
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return num;
	}

	/**
	 * 2004.11.13
	 * Yoshimoto KAWAI - Totec
	 * ２つの文字列を比較する
	 * ただし、どちらかがNULLまたは空文字列なら偽
	 * 
	 * @return
	 */
	public static boolean compareValue(String s1, String s2) {
		if (s1 == null || "".equals(s1)) return false;
		if (s2 == null || "".equals(s2)) return false;
		return s1.equals(s2);
	}
	
}

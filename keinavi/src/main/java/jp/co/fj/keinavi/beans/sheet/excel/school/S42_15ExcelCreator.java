package jp.co.fj.keinavi.beans.sheet.excel.school;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator;
import jp.co.fj.keinavi.beans.sheet.common.ISheetData;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15BundleListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15Data;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15ReachLevelListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15ScoreZoneListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15SubjectListData;
import jp.co.fj.keinavi.util.KNUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;

/**
 *
 * 校内成績分析 - 他校比較 - 偏差値分布
 * 受験学力測定テスト専用（今年度のみ）
 *
 * 2006.09.01	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class S42_15ExcelCreator extends BaseExcelCreator {

    // 行開始インデックス
    private final int startRowIndex;
    // 学校情報の最終インデックス
    private final int maxSchoolIndex;

    // 科目インデックス
    private int subIndex = 0;

    /**
     * コンストラクタ
     *
     * @param pData
     * @param pSequenceId
     * @param pOutFileList
     * @param pPrintFlag
     * @throws Exception
     */
    public S42_15ExcelCreator(final ISheetData pData, final String pSequenceId,
            final List pOutFileList, final int pPrintFlag,
            final int pStartRowIndex, final int pMaxSchoolIndex) throws Exception {

        super(pData, pSequenceId, pOutFileList, pPrintFlag, 50);
        this.startRowIndex = pStartRowIndex;
        this.maxSchoolIndex = pMaxSchoolIndex;
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
     * 			#initSheet(org.apache.poi.hssf.usermodel.HSSFSheet)
     */
    protected void initSheet(final ISheetData pData) throws Exception {

        // データクラス
        final S42_15Data data = (S42_15Data) pData;
        // 当該シートの科目リストデータ
        final S42_15SubjectListData subData = getCurrentSubjectListData(data);

        // 作成日
        setCreateDate();
        // セキュリティスタンプ
        setSecurityStamp(data.getSecurityStamp(), 33, 34);
        // 学校名
        setCellValue("A2", "学校名　　　：" + data.getBundleName());
        // 対象模試
        setCellValue("A3", createTargetExamLabel(
                data.getExamName(), data.getInpleDate()));
        // 過年度の表示
        setCellValue("A4", createDispPastLavel(data.getDispPast()));
        // 科目
        setCellValue("A6", "科目：" + subData.getSubName());
        // センター到達エリア
        setCenterReachArea(subData.getReachLevelListData());
    }

    // 現在処理している科目リストデータを返す
    protected S42_15SubjectListData getCurrentSubjectListData(
            final S42_15Data data) {
        return data.getSubjectListData(subIndex);
    }

    //「対象模試」のラベルを作る
    protected String createTargetExamLabel(
            final String examName, final String inpleDate) {

        final StringBuffer buff = new StringBuffer();
        buff.append("対象模試　　：");
        buff.append(examName);

        if (inpleDate != null && inpleDate.length() >= 6) {
            buff.append("（");
            final int month = Integer.parseInt(inpleDate.substring(4, 6));
            buff.append(Character.toString((char) (65297 + month - 1)));
            buff.append("月）");
        }

        return buff.toString();
    }

    //「過年度の表示」のラベルを作る
    protected String createDispPastLavel(final int dispPast) {
        if (dispPast == 1) {
            return "過年度の表示：対象年度のみ";
        } else if (dispPast == 2) {
            return "過年度の表示：対象年度の前年度まで";
        } else {
            return "過年度の表示：対象年度の前々年度まで";
        }
    }

    // センター到達エリアをセットする
    protected void setCenterReachArea(final List list) {
        setCenterReachArea(list, 0);
    }

    // センター到達エリアをセットする
    protected void setCenterReachArea(final List list,
            final int colIndex) {

        // 前ループのセンター到達エリア
        String beforeCenterReachArea = null;
        // 前ループのセンター予想得点率
        String beforeCenterExScoreRate = null;
        // 行位置
        int rowIndex = startRowIndex;

        // 上部罫線スタイル
        final HSSFCellStyle style = createCellStyle();
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        // フォントを設定
        final HSSFFont font = getBook().createFont();
        font.setFontHeightInPoints((short)11);
        font.setFontName("ＭＳ ゴシック");
        style.setFont(font);

        for (final Iterator ite = list.iterator(); ite.hasNext();) {
            final S42_15ReachLevelListData data = (
                    S42_15ReachLevelListData) ite.next();
            // スコア帯「01」は処理しない
            if ("01".equals(data.getDevZoneCd())) {
                return;
            }
            // 到達レベルがNULLなら飛ばす
            if (data.getDispStr_CenterReachArea() == null) {
                rowIndex++;
                continue;
            }

            // センター到達エリア
            if (!data.getDispStr_CenterReachArea().equals(beforeCenterReachArea)) {
                // 開始行は必ず保存値と違う為、出力しない
                if (rowIndex != startRowIndex) {
                    // センター到達エリアが変わった時に1つ前のセンター到達エリアを出力する
                    setCellValue(rowIndex-2, colIndex, beforeCenterReachArea);
                    setCellValue(rowIndex-1, colIndex, beforeCenterExScoreRate);
                    // 罫線を引く
                    getCell(rowIndex, colIndex).setCellStyle(style);
                }
                // 最下位のセンター到達エリアを出力する
                if (data.getCenterReachArea().equals(KNUtil.minCENTERREACHAREA)) {
                    setCellValue(rowIndex, colIndex, data.getDispStr_CenterReachArea());
                    setCellValue(rowIndex+1, colIndex, data.getCenterExScoreRate());
                }

                // セットした値は保持する
                beforeCenterReachArea = data.getDispStr_CenterReachArea();
                beforeCenterExScoreRate = data.getCenterExScoreRate();
            }
            rowIndex++;
        }
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.BaseSheetCreator
     * 			#createSheet(jp.co.fj.keinavi.beans.sheet.common.ISheetData)
     */
    protected void createSheet(ISheetData pData) throws Exception {

        // データクラス
        final S42_15Data data = (S42_15Data) pData;
        // 科目でループする
        for (final Iterator ite = data.getSubjectListData(
                ).iterator(); ite.hasNext();) {

            final S42_15SubjectListData subData = (S42_15SubjectListData) ite.next();
            createSheet(subData.getBundleListData(),
                    subData.getData().getDispPast());
            // 科目単位で改シート
            subIndex++;
            if (ite.hasNext()) {
                breakSheet();
            }
        }
    }

    protected void createSheet(final List list,
            final int dispPast) throws Exception {

        // シート内の位置
        int index = 0;
        // 学校単位のカウンタ
        int count = 0;
        // 前ループの一括コード
        String beforeBundleCd = null;

        // 自校と他校に分ける
        final List ownList = new ArrayList();
        final List otherList = new ArrayList();
        // 営業は全て他校になるよう細工・・・
        String ownBundleCd = getSheetId().startsWith("S") ? null : "ZZZZZZ";
        for (final Iterator ite = list.iterator(); ite.hasNext();) {
            final S42_15BundleListData bundleData = (
                    S42_15BundleListData) ite.next();
            if (ownBundleCd == null
                    || ownBundleCd.equals(bundleData.getBundleCd())) {
                ownBundleCd = bundleData.getBundleCd();
                ownList.add(bundleData);
            } else {
                otherList.add(bundleData);
            }
        }

        for (final Iterator ite = otherList.iterator(); ite.hasNext();) {
            final S42_15BundleListData bundleData = (
                    S42_15BundleListData) ite.next();

            // 前ループと学校が異なる
            if (beforeBundleCd != null
                    && !bundleData.getBundleCd().equals(beforeBundleCd)) {
                // 過年度のデータ不足分ずらす
                if (count < dispPast) {
                    index += dispPast - count;
                    // 最後になったら改シート
                    if (index >= maxSchoolIndex) {
                        breakSheet();
                        index = 0;
                    }
                }
                count = 0;
            }

            // シート変更宣言
            setModifiedSheet();
            // 先頭は自校となる
            if (index == 0 && !ownList.isEmpty()) {
                int ownCount = 0;
                for (final Iterator it = ownList.iterator(); it.hasNext();) {
                    final S42_15BundleListData ownData = (
                            S42_15BundleListData) it.next();
                    createSheet(ownData, index++);
                    ownCount++;
                }
                // 過年度のデータ不足分ずらす
                if (ownCount < dispPast) {
                    index += dispPast - ownCount;
                }
            }
            // データセット
            createSheet(bundleData, index);
            // 処理加算
            count++;
            // 処理した一括コードを保持する
            beforeBundleCd = bundleData.getBundleCd();
            // 最後になったら改シート
            if (++index >= maxSchoolIndex) {
                breakSheet();
                index = 0;
            }
        }
    }

    protected void createSheet(final S42_15BundleListData bundleData,
            final int index) {

        // 列位置
        final int cIndex = 2 + 3 * index;

        // 過年度なし
        if (bundleData.getSubjectListData().getData().getDispPast() == 1) {
            // 学校名
            setCellValue(startRowIndex - 2, cIndex,
                    bundleData.getBundleName());
        // 過年度あり
        } else {
            // 学校名
            setCellValue(startRowIndex - 3, cIndex,
                    bundleData.getBundleName());
            // 年度
            setCellValue(startRowIndex - 2, cIndex,
                    bundleData.getExamYear() + "年度");
        }

        // 合計人数
        setCellValue(startRowIndex + 61, cIndex, bundleData.getNumbers());
        // 平均スコア
        setCellValue(startRowIndex + 62, cIndex, bundleData.getAvgPnt());

        // スコア帯データをセット
        if (!bundleData.getScoreZoneListMap().isEmpty()) {
            createSheet(bundleData.getScoreZoneListMap(), cIndex);
        }
    }

    protected void createSheet(final Map scoreZoneListMap,
            final int cIndex) {

        // データがなければここまで
        if (scoreZoneListMap.isEmpty()) {
            return;
        }

        // 累計人数
        int total = 0;
        // 最大人数
        int max = 0;
        // 最大人数の行位置
        int maxRowIndex = -1;

        for (int i = 0; i < 61; i++) {
            // 処理するスコア帯コード
            final String devZoneCd = StringUtils.leftPad((62 - i) + "", 2, '0');
            // 行位置確定
            final int rowIndex = i + startRowIndex;
            // データクラス
            final S42_15ScoreZoneListData scoreData = (
                    S42_15ScoreZoneListData) scoreZoneListMap.get(devZoneCd);
            // データあり
            if (scoreData != null) {
                // 人数
                setCellValue(rowIndex, cIndex + 1, scoreData.getNumbers());
                // 累計人数加算
                total += scoreData.getNumbers();
                // 最大人数を保持
                if (scoreData.getNumbers() > max) {
                    max = scoreData.getNumbers();
                    maxRowIndex = rowIndex;
                }
            }
            // 累計人数
            if (total > 0) {
                setCellValue(rowIndex, cIndex + 2, total);
            }
        }

        // *表示
        if (maxRowIndex >= 0) {
            setCellValue(maxRowIndex, cIndex, "*");
        }
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
     * 			#getSheetId()
     */
    protected String getSheetId() {
        return "S42_15";
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
     * 			#isGraphSheet()
     */
    protected boolean isGraphSheet() {
        return false;
    }

}

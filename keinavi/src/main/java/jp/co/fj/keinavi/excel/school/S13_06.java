/**
 * 校内成績分析−校内成績　設問別成績
 * 	Excelファイル編集
 * 作成日: 2009/10/21
 * @author	Fujito URAKAWA
 *
 * 2019.9.10 Yuko NAGAI - QQ
 *              共通テスト対応
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S13Item;
import jp.co.fj.keinavi.excel.data.school.S13KokugoQueListBean;
import jp.co.fj.keinavi.excel.data.school.S13MarkAreaListBean;
import jp.co.fj.keinavi.excel.data.school.S13SugakuKokugoQuiListBean;
import jp.co.fj.keinavi.excel.data.school.S13SugakuQueListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S13_06 {

    // 帳票作成結果
    // 0:正常終了
    // 1:ファイル読込みエラー
    // 2:ファイル書込みエラー
    // 3:データセットエラー
    private int noerror = 0;
    private int errfread = 1;
    private int errfwrite = 2;
    private int errfdata = 3;

    // 共通関数用クラス インスタンス
    private CM cm = new CM();

    // 帳票テンプレートファイル
    private String masterfile = "S13_06";

    // 出力固定値
    private String ERRORMESSAGE = "データセットエラー";

	   // 出力項目位置
    public enum CURSOL {
     // 数学
     SUGAKU_START(10,0),
     SUGAKU_END(12,14),
	 // 国語
     KOKUGO_START(17,5),
     KOKUGO_END(37,7);

     private final int col;
     private final int row;

     private CURSOL(int row, int col) {
         this.col = col;
         this.row = row;
     }
     public int getCol() {
         return this.col;
     }
     public int getRow() {
         return this.row;
     }
    }

 /**
  * Excel編集メイン
  * @param s33Item データクラス
  * @param outfilelist 出力Excelファイル名（フルパス）
  * @param intSaveFlg 1:保存 2:印刷 3:保存／印刷
  * @param UserID ユーザーID
  * @return 帳票作成結果
  */
  public int s13_06EditExcel(S13Item s13Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

     KNLog log = KNLog.getInstance(null,null,null);
     HSSFWorkbook workbook = null;
     HSSFSheet workSheet = null;
     HSSFRow workRow = null;
     HSSFCell workCell = null;

     // マスタExcel読み込み
     workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);

     // マスタExcel読込みエラー
     if( workbook==null ){
         return errfread;
     }

     workSheet = workbook.cloneSheet(0);

     // データ検索結果取得
     S13SugakuKokugoQuiListBean result = (S13SugakuKokugoQuiListBean) s13Item.getS13SugakuKokugoQueList().get(0);

    try {
         // 行
         int row = 0;
         // 列
         int col = 0;

         int index = 0;

         setUpbaseData(workbook, workSheet, s13Item, null);	// 基本情報セット

		//数学データ出力
		int a = 17;

         List <S13SugakuQueListBean> list1 = result.getS13SugakuQueList();

         //2019/09/27 QQ)Oosaki ロジック修正 ADD START
         //国語データ出力
         List <S13KokugoQueListBean> list2 = result.getS13KokugoQueList();
         //2019/09/27 QQ)Oosaki ロジック修正 ADD END

         Iterator itr = list1.iterator();

         //2019/09/27 QQ)Oosaki ロジック修正 ADD START
         Iterator itrKokugo = list2.iterator();
         //2019/09/27 QQ)Oosaki ロジック修正 ADD END

         // データが存在しない場合には、ヘッダ情報をセットして終了！
         //2019/09/27 QQ)Oosaki ロジック修正 UPD START
         //if( itr.hasNext() == false ){
         if( itr.hasNext() == false && itrKokugo.hasNext() == false ){
         //2019/09/27 QQ)Oosaki ロジック修正 UPD END
        	 workSheet = initWorkSheet(workbook);				// ワークシート初期化
        	 setUpbaseData(workbook, workSheet, s13Item, null);	// 基本情報セット
         }
         else
         {

           //2019/09/27 QQ)Oosaki ロジック修正 ADD START
           if( itr.hasNext() == true ) {
           //2019/09/27 QQ)Oosaki ロジック修正 ADD END
          		log.Ep("S13_06","マークシートデータセット開始","");

               String KmkCd = list1.get(0).getStrKmkCd();

               index = 0;
               row = CURSOL.SUGAKU_START.getRow();
               col = CURSOL.SUGAKU_START.getCol();

               int col2 = 0;

               while(index < list1.size()) {
              	 S13SugakuQueListBean data = list1.get(index);

                if(!KmkCd.equals(list1.get(index).getStrKmkCd())) {
                   col += a;
                   KmkCd = list1.get(index).getStrKmkCd();
                   row = CURSOL.SUGAKU_START.getRow();
                   col2 += a;
                }

                // 科目名
                workCell = cm.setCell( workSheet, workRow, workCell, 6, col+3 );
                workCell.setCellValue( data.getStrKmkmei());

                // 設問番号
                workCell = cm.setCell( workSheet, workRow, workCell, row, col );
                workCell.setCellValue( data.getStrMonnum());
                col += 1;

                // 設問名称
                workCell = cm.setCell( workSheet, workRow, workCell, row, col );
                workCell.setCellValue( data.getStrQuestionName());
                col += 6;

                // 配点
                workCell = cm.setCell( workSheet, workRow, workCell, row, col );
                workCell.setCellValue( data.getStrHaiten());
                col += 3;


                if(data.getFloHomeAvgpnt() != 0 || data.getFloAllAvgpnt() != 0 ) {
                	//校内平均点
                	workCell = cm.setCell( workSheet, workRow, workCell, row, col );
                	setUpFlortValue(workCell, data.getFloHomeAvgpnt());
                	col += 2;
                	// 全国平均点
                	workCell = cm.setCell( workSheet, workRow, workCell, row, col );
                	setUpFlortValue(workCell, data.getFloAllAvgpnt());
                	col += 2;
                	// 差
                	workCell = cm.setCell( workSheet, workRow, workCell, row, col );
                	setUpDiffFlortValue(workCell, data.getFloAllAvgpnt(), data.getFloHomeAvgpnt());
                }

                col = CURSOL.SUGAKU_START.getCol()+col2;
                row++;
                index++;
               }

             }
         }


         //2019/09/27 QQ)Oosaki ロジック修正 DEL START
	     ////国語データ出力
	     //List <S13KokugoQueListBean> list2 = result.getS13KokugoQueList();
         //2019/09/27 QQ)Oosaki ロジック修正 DEL END

	     log.Ep("S13_06","マークシートデータセット開始","");

	     index = 0;
	     row = CURSOL.KOKUGO_START.getRow();
	     col = CURSOL.KOKUGO_START.getCol();
	     while(index < list2.size() && row <= CURSOL.KOKUGO_END.getRow()) {
	       S13KokugoQueListBean data2 = list2.get(index);

	       // �@
	       if(data2.getFloSeitoritsuHome1() != 0 || data2.getFloSeitoritsuAll1() != 0 ) {
	    	   //校内正答率1
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col );
	    	   setUpFlortValue(workCell, data2.getFloSeitoritsuHome1());

	    	   //全国正答率1
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
	    	   setUpFlortValue(workCell, data2.getFloSeitoritsuAll1());

	    	   // 差
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col+4 );
	    	   setUpDiffFlortValue(workCell, data2.getFloSeitoritsuAll1(), data2.getFloSeitoritsuHome1());
	       }
	       row +=1;

	       // �A
	       if(data2.getFloSeitoritsuHome2() != 0 || data2.getFloSeitoritsuAll2() != 0 ) {
	    	   //校内正答率2
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col );
	    	   setUpFlortValue(workCell, data2.getFloSeitoritsuHome2());

	    	   //全国正答率2
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
	    	   setUpFlortValue(workCell, data2.getFloSeitoritsuAll2());

	    	   // 差
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col+4 );
	    	   setUpDiffFlortValue(workCell, data2.getFloSeitoritsuAll2(), data2.getFloSeitoritsuHome2());
	       }
	       row +=1;

	       // �B
	       if(data2.getFloSeitoritsuHome3() != 0 || data2.getFloSeitoritsuAll3() != 0 ) {
	    	   //校内正答率3
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col );
	    	   setUpFlortValue(workCell, data2.getFloSeitoritsuHome3());

	    	   //全国正答率3
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
	    	   setUpFlortValue(workCell, data2.getFloSeitoritsuAll3());

	    	   // 差
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col+4 );
	    	   setUpDiffFlortValue(workCell, data2.getFloSeitoritsuAll3(), data2.getFloSeitoritsuHome3());
	       }
	       row +=1;

	       // �C
	       if(data2.getFloSeitoritsuHome4() != 0 || data2.getFloSeitoritsuAll4() != 0 ) {
	    	   //校内正答率4
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col );
	    	   setUpFlortValue(workCell, data2.getFloSeitoritsuHome4());

	    	   //全国正答率4
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
	    	   setUpFlortValue(workCell, data2.getFloSeitoritsuAll4());

	    	   // 差
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col+4 );
	    	   setUpDiffFlortValue(workCell, data2.getFloSeitoritsuAll4(), data2.getFloSeitoritsuHome4());
	       }
	       row +=1;

	       // �D
	       if(data2.getFloSeitoritsuHome5() != 0 || data2.getFloSeitoritsuAll5() != 0 ) {
    	  //校内正答率5
 	        workCell = cm.setCell( workSheet, workRow, workCell, row, col );
 	     	setUpFlortValue(workCell, data2.getFloSeitoritsuHome5());

 	      //全国正答率5
 	     	workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
 	     	setUpFlortValue(workCell, data2.getFloSeitoritsuAll5());
	      // 差
	     	workCell = cm.setCell( workSheet, workRow, workCell, row, col+4 );
			setUpDiffFlortValue(workCell, data2.getFloSeitoritsuAll5(), data2.getFloSeitoritsuHome5());
	       }
	       row +=1;

	       // �E
	       if(data2.getFloSeitoritsuHome6() != 0 || data2.getFloSeitoritsuAll6() != 0 ) {
	    	   //校内正答率6
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col );
	    	   setUpFlortValue(workCell, data2.getFloSeitoritsuHome6());

	    	   //全国正答率6
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
	    	   setUpFlortValue(workCell, data2.getFloSeitoritsuAll6());
	    	   // 差
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col+4 );
	    	   setUpDiffFlortValue(workCell, data2.getFloSeitoritsuAll6(), data2.getFloSeitoritsuHome6());
	       }
	       row +=1;

	       // �F
	       if(data2.getFloSeitoritsuHome7() != 0 || data2.getFloSeitoritsuAll7() != 0 ) {
	    	   //校内正答率7
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col );
	    	   setUpFlortValue(workCell, data2.getFloSeitoritsuHome7());

	    	   //全国正答率7
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
	    	   setUpFlortValue(workCell, data2.getFloSeitoritsuAll7());
	    	   // 差
	    	   workCell = cm.setCell( workSheet, workRow, workCell, row, col+4 );
	    	   setUpDiffFlortValue(workCell, data2.getFloSeitoritsuAll7(), data2.getFloSeitoritsuHome7());
	       }

	        col = CURSOL.KOKUGO_START.getCol();
	        row++;
	        index++;
	     }

	     // アクティブシートを選択
	     workbook.getSheetAt(1).setSelected(true);

         // Excel書込み
         boolean bolRet = false;
         bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, 1);

         // Excel書込みエラー
         if( bolRet == false ){
             return errfwrite;
         }

     }catch(Exception e){
         log.Err(masterfile, ERRORMESSAGE, e.toString());
         return errfdata;
     }

     return noerror;

   }

	/**
	 * 基本情報をExcelにセットする
	 * @param workbook
	 * @param workSheet
	 * @param item
	 * @param data
	 * @param workCell
	 */
	private void setUpbaseData(HSSFWorkbook workbook, HSSFSheet workSheet, S13Item item, S13MarkAreaListBean data) {

		HSSFRow	workRow	= null;
		HSSFCell workCell = null;

	 // ヘッダ右側に帳票作成日時を表示する
     //2019/09/27 QQ)Oosaki 日時設定 UPD START
     //cm.setHeader(workbook, workbook.cloneSheet(0));
     cm.setHeader(workbook, workSheet);
     //2019/09/27 QQ)Oosaki 日時設定 UPD END

     // セキュリティスタンプセット
     String secuFlg = cm.setSecurity(workbook, workSheet, item.getIntSecuFlg() ,34 ,38);
     workCell = cm.setCell(workSheet, workRow, workCell, 0, 34);
     workCell.setCellValue(secuFlg);

		// 学校名セット
		workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
		workCell.setCellValue( "学校名　：" + cm.toString(item.getStrGakkomei()) );

		// 模試月取得
		String moshi =cm.setTaisyouMoshi( item.getStrMshDate() );
		// 対象模試セット
		workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
		workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(item.getStrMshmei()) + moshi);

	}

    /**
     * Excelにデータ出力
     * @param workSheet Excelシート情報
     * @param workRow Excel行情報
     * @param row 行
     * @param col 列
     * @param Data Stringのデータ
     */
	 public void setValueString(HSSFCell workCell,String data) {


	 }

	/**
	 * セルにfloatデータをセットする。
	 * ※float値が「-999.0」の場合には、データをセットしない
	 * @param workCell
	 * @param floSeitoritsuHome
	 */
	 private void setUpFlortValue(HSSFCell workCell, float floValue) {

		if (floValue != -999.0) {
			workCell.setCellValue( floValue );
		}

	 }

	/**
	 * シートのテンプレートをコピーする（初期化）
	 * @param workbook
	 * @return HSSFSheet
	 */
	 private HSSFSheet initWorkSheet(HSSFWorkbook workbook) {

		return workbook.cloneSheet(0);

	 }

	/**
	 * セルにfloatデータの差をセットする。
	 * ※どちらかの値が「-999.0」の場合には、データをセットしない
	 * @param workCell
	 * @param floA
	 * @param floB
	 */
     private void setUpDiffFlortValue(HSSFCell workCell, float floA, float floB) {

        //2019/09/30 QQ)Oosaki 計算式変更 UPD START
		//workCell.setCellValue( floA - floB );
		workCell.setCellValue( floB - floA );
		//2019/09/30 QQ)Oosaki 計算式変更 UPD END
	 }

}

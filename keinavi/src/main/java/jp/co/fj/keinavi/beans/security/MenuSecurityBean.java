package jp.co.fj.keinavi.beans.security;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.keinavi.beans.security.config.Function;
import jp.co.fj.keinavi.beans.security.config.SecurityConfig;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.util.KNCommonProperty;

import org.apache.commons.collections.CollectionUtils;


/**
 *
 * メニューセキュリティを実現するBean
 *
 *
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class MenuSecurityBean implements Serializable {

	/** セッションキー */
	public static final String SESSION_KEY = "MenuSecurity";

	/**
	 * 権限があるメニューIDを保持するマップ
	 * KEY: メニューID
	 * VALUE: Boolean(true)
	 */
	private final Map menu = new HashMap();

	/**
	 * コンストラクタ
	 *
	 * @param login
	 */
	public MenuSecurityBean(final LoginSession login, final SecurityConfig config) {

		init(login, config);

		// 特殊処理

		// 校内成績処理システムは成績分析か入試結果情報が必要
		if (!isValid(IMenuDefine.MENU_KOUNAI_SCORE)
				&& !isValid(IMenuDefine.MENU_KOUNAI_ENT_EXAM)) {

			removeMenu(IMenuDefine.MENU_KOUNAI);
		}


		// 入試結果調査の有効・無効は設定ファイルで決まる
		if (!"1".equals(KNCommonProperty.getEnableExamResultsResearch())) {

			removeMenu(IMenuDefine.MENU_TEXT_RESULT);
		}

		// 体験版は利用者情報不可
		// 2015/12/17 QQ)Hisakawa 大規模改修 DEL START
//		if (login.isTrial()) {
//			removeMenu("801");
//		}
		// 2015/12/17 QQ)Hisakawa 大規模改修 DEL END
	}

	/**
	 * メニューマップの初期化を行う
	 */
	protected void init(final LoginSession login, final SecurityConfig config) {

		// 利用者の権限設定
		final List userList;

		// 営業
		if (login.isSales()) {
			userList = getFunctionList(config, "business");

		// 営業以外の契約校
		} else if (login.isPact()) {
			userList = getFunctionListWithoutBusiness(login, config);

		// 営業以外の非契約校
		// たとえば私塾の非契約校なら「私塾」と「非契約校」の積集合となる
		} else {
			userList = (List) CollectionUtils.intersection(
					getFunctionListWithoutBusiness(login, config),
					getFunctionList(config, "nopact"));
		}

		// 設定をマップに反映する
		final Iterator ite = userList.iterator();
		while (ite.hasNext()) {
			addMenu((String) ite.next());
		}

		// 機能別フラグの評価
		setupFunctionFlag(login, config);
		// 管理者フラグの評価
		setupManagerFlag(login, config);
		// メンテナンスフラグの評価
		setupMaintenanceFlag(login, config);
	}

	/**
	 * 営業以外の利用者の権限設定を取得する
	 *
	 * @param login
	 * @param config
	 * @return
	 */
	private List getFunctionListWithoutBusiness(final LoginSession login, final SecurityConfig config) {

		// P保護
		if (login.isPrivacyProtection()) {
			return getFunctionList(config, "privacy");

		// 私塾
		} else if (login.isPrep()) {
			return getFunctionList(config, "prep");

		// 教育委員会
		} else if (login.isBoard()) {
			return getFunctionList(config, "board");

		// 高校
		} else {
			return getFunctionList(config, "school");
		}
	}

	/**
	 * 機能別フラグを評価する
	 *
	 * @param login
	 * @param config
	 */
	private void setupFunctionFlag(final LoginSession login, final SecurityConfig config) {

		// 機能詳細設定を処理していく
		final Iterator ite = config.getFunctionList().iterator();
		while (ite.hasNext()) {
			final Function function = (Function) ite.next();

			// 評価する機能権限フラグ
			final short flag;

			// Kei-Navi
			if ("kn".equals(function.getCategory())) {
				flag = login.getKnFunctionFlag();

			// 校内成績処理
			} else if ("sc".equals(function.getCategory())) {
				flag = login.getScFunctionFlag();

			// 入試結果調査
			} else if ("ee".equals(function.getCategory())) {
				flag = login.getEeFunctionFlag();

			// カテゴリなしはフラグを評価しない
			} else {
				flag = 1;
			}

			// 集計系のみ可能で個人系メニュー　または　全機能不可　であるなら機能を削除
			if ((flag == 2 && function.isIndividual()) || flag == 3) {
				removeMenu(function.getId());
			}
		}
	}

	/**
	 * 管理者フラグを評価する
	 *
	 * @param login
	 * @param config
	 */
	private void setupManagerFlag(final LoginSession login, final SecurityConfig config) {

		// 管理者なら評価しない
		if (login.isManager()) return;

		// 機能詳細設定を処理していく
		final Iterator ite = config.getFunctionList().iterator();
		while (ite.hasNext()) {
			final Function function = (Function) ite.next();

			// 管理者メニューなら削除
			if (function.isManager()) removeMenu(function.getId());
		}
	}

	/**
	 * メンテナンスフラグを評価する
	 *
	 * @param login
	 * @param config
	 */
	private void setupMaintenanceFlag(final LoginSession login, final SecurityConfig config) {

		// メンテナンス権限があるなら評価しない
		if (login.isMaintainer()) return;

		// 機能詳細設定を処理していく
		final Iterator ite = config.getFunctionList().iterator();
		while (ite.hasNext()) {
			final Function function = (Function) ite.next();

			// メンテナンスメニューなら削除
			if (function.isMaintenance()) removeMenu(function.getId());
		}
	}

	/**
	 * 指定した利用者種別の権限設定を取得する
	 *
	 * @param config
	 * @param type
	 * @return
	 */
	private List getFunctionList(final SecurityConfig config, final String type) {

		return (List) config.getUserSecurityMap().get(type);
	}

	/**
	 * 機能IDを追加する
	 *
	 * @param id
	 */
	private void addMenu(final String id) {

		this.menu.put(id, Boolean.TRUE);
	}

	/**
	 * 機能IDを削除する
	 *
	 * @param id
	 */
	private void removeMenu(final String id) {

		this.menu.remove(id);
	}

	/**
	 * メニューマップを取得する
	 *
	 * @return
	 */
	public Map getMenu() {

		return this.menu;
	}

	/**
	 * 有効なメニューかどうか
	 * @param id
	 * @return
	 */
	public boolean isValid(final String id) {
		return this.menu.containsKey(id);
	}

}

package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.keinavi.beans.individual.judgement.SubjectPageBean;
import jp.co.fj.keinavi.forms.individual.I206Form;
import jp.co.fj.keinavi.servlets.KNServletException;

/**
 *
 * 公表科目表示画面のサーブレットです。
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KONDO.Keisuke
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class I206Servlet extends IndividualServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = 2526763192363440222L;

    /**
     * {@inheritDoc}
     */
    protected void execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if ("i206".equals(getForward(request))) {
            I206Form form = (I206Form) getActionForm(request, I206Form.class.getName());
            try {
                SubjectPageBean subjectPageBean = new SubjectPageBean(findUniv(form.getUniqueKey()));
                subjectPageBean.execute(getUnivInfoMap());
                request.setAttribute("pageBean", subjectPageBean);
            } catch (Exception e) {
                throw createServletException(e);
            }
            forward(request, response, JSP_I206);
        } else {
            forward(request, response, SERVLET_DISPATCHER);
        }
    }

    /**
     * 大学インスタンスをメモリから取得します。
     *
     * @param uniqueKey 大学キー
     * @return {@link UnivData}
     */
    private UnivData findUniv(String uniqueKey) throws KNServletException {
        UnivData univ = (UnivData) getUnivMap().get(uniqueKey);
        if (univ != null) {
            return univ;
        } else {
            throw new RuntimeException("大学インスタンスの取得に失敗しました。" + uniqueKey);
        }
    }

}

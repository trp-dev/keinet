/*
 * 作成日: 2004/09/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.help;

import com.fjh.forms.ActionForm;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class H002Form extends ActionForm {

	// フリーワード検索とカテゴリー一覧の切り分けを「category」で行う。
	private String freeWord = null;	// フリーワード
	private String category = null;	// カテゴリー

	// 表示対象となるページ番号
	private String page = null;

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}


	/**
	 * @return
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param string
	 */
	public void setCategory(String string) {
		category = string;
	}


	/**
	 * @return
	 */
	public String getFreeWord() {
		return freeWord;
	}

	/**
	 * @param string
	 */
	public void setFreeWord(String string) {
		freeWord = string;
	}


	/**
	 * @return
	 */
	public String getPage() {
		return page;
	}

	/**
	 * @param string
	 */
	public void setPage(String string) {
		page = string;
	}

}

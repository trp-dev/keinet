package jp.co.fj.keinavi.interfaces;

import java.io.Serializable;

/**
 * 
 * 特例成績データ作成申請状態を管理する定数クラスです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SpecialAppliStatus implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = -3195084966676994486L;

	/** 承認待ち（営業部） */
	public static final SpecialAppliStatus PENDING_EIGYO = new SpecialAppliStatus(
			"00");

	/** 承認待ち（高校事業企画部） */
	public static final SpecialAppliStatus PENDING_KIKAKU = new SpecialAppliStatus(
			"10");

	/** 承認待ち（模試運用管理部） */
	public static final SpecialAppliStatus PENDING_KANRI = new SpecialAppliStatus(
			"20");

	/** 承認済 */
	public static final SpecialAppliStatus ACCEPTED = new SpecialAppliStatus(
			"99");

	/** 状態を表すコード */
	public final String code;

	/**
	 * コンストラクタです。
	 */
	private SpecialAppliStatus(String code) {
		this.code = code;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SpecialAppliStatus)) {
			return false;
		}
		SpecialAppliStatus other = (SpecialAppliStatus) obj;
		if (code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!code.equals(other.code)) {
			return false;
		}
		return true;
	}

}

/*
 * 作成日: 2004/07/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.taglib.cmfactory;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;

import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * 担当クラスの設定状況を表示するTaglib
 * 
 * 2005.02.28 Yoshimoto KAWAI - Totec
 *            年度表示対応
 * 
 * @author kawai
 *
 */
public class CM701TagFactory extends AbstractCMTagFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.taglib.cmfactory.AbstractCMTagFactory#createCMStatus(javax.servlet.ServletRequest)
	 */
	public String createCMStatus(ServletRequest request, HttpSession session) throws JspException {
		String result = null;
		Map item = getItemMap(session);
		List list = (List)item.get("0501");
		if (list == null || list.size() == 0) {
			result = "設定なし";
		} else {
			result = ProfileUtil.getChargeClassYear(super.getProfile(session))
				+ "年度 "
				+ list.size()
				+ "クラス";
		}
		return result;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.taglib.cmfactory.AbstractCMTagFactory#createTempCMStatus(javax.servlet.ServletRequest, javax.servlet.http.HttpSession)
	 */
	public String createTempCMStatus(ServletRequest request, HttpSession session) throws JspException {
		return this.createCMStatus(request, session);
	}

}

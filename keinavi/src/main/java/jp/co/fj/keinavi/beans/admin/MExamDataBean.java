package jp.co.fj.keinavi.beans.admin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.data.admin.MExamData;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 受験届修正・コンボ内容を保持するBean
 * 
 * 2004.09.10	[新規作成]
 * 2006.10.06	リファクタリング
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class MExamDataBean extends DefaultBean {

	// クラスリスト
	private final List examDataList = new LinkedList();
	
	private String schoolCd; // 学校コード
	private boolean isHelpDesk; // ヘルプデスクかどうか

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final Query query = QueryLoader.getInstance().load("m03");
			
			// ヘルプデスクならデータ開放日書き換え
			if (isHelpDesk) {
				query.replaceAll("out_dataopendate", "in_dataopendate");
			} 
			
			ps = conn.prepareStatement(query.toString());
			ps.setFetchSize(200);
			ps.setString(1, schoolCd);
			ps.setString(2, schoolCd);
			rs = ps.executeQuery();
			while (rs.next()) {
				MExamData data = new MExamData();
				data.setExamYear(rs.getString(1));
				data.setExamCd(rs.getString(2));
				data.setGrade(rs.getInt(3));
				data.setClassName(rs.getString(4));
				data.setExamName(rs.getString(6));
				data.setInpleDate(rs.getString(7));
				data.setDispSequence(rs.getInt(8));
				examDataList.add(data);	
			}
		} finally {
			DbUtils.closeQuietly(null, ps ,rs);
		}
	}

	/**
	 * @return
	 */
	public List getExamDataList() {
		return examDataList;
	}

	/**
	 * @param string
	 */
	public void setSchoolCd(final String string) {
		schoolCd = string;
	}

	/**
	 * @param b
	 */
	public void setHelpDesk(final boolean b) {
		isHelpDesk = b;
	}

}

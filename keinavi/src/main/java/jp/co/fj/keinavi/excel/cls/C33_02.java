/**
 * 校内成績分析−クラス比較　設問別成績（クラス比較）
 * 	Excelファイル編集
 * 作成日: 2004/08/10
 * @author	T.Sakai
 * 
 * 2009.10.13   Fujito URAKAWA - Totec
 *              「全体成績」追加対応
 */

package jp.co.fj.keinavi.excel.cls;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C33ClassListBean;
import jp.co.fj.keinavi.excel.data.cls.C33Item;
import jp.co.fj.keinavi.excel.data.cls.C33ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class C33_02 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private boolean bolBookCngFlg = true;			//改ファイルフラグ
	private boolean bolSheetCngFlg = true;			//改シートフラグ
	
	final private String masterfile0 = "C33_02";
	final private String masterfile1 = "C33_02";
	private String masterfile = "";
	final private int intMaxSheetSr = 40;	//MAXシート数の値を入れる

/*
 * 	Excel編集メイン
 * 		C33Item c33Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
	public int c33_02EditExcel(C33Item c33Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
 		
		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (c33Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		int		intMaxSheetIndex	= 0;		//ファイルのシートの最大数
		int		row					= 0;		//行
		int		col					= 0;		//列
		int		classCnt			= 0;		//クラスカウンタ(自校＋クラス)
		int		setsumonCnt			= 0;		//設問カウンタ
		String		kmk					= "";		//科目チェック用
		int		maxClass			= 0;		//MAXクラス数(自校＋クラス)
		int		sheetListIndex		= 0;		//シートカウンター
		int		fileIndex			= 1;		//ファイルカウンター
		int		houjiNum			= 5;		//1シートに表示できるクラス数
		int		sheetRowCnt			= 0;		//クラス用改シート格納用カウンタ
		ArrayList	workbookList	= new ArrayList();
		ArrayList	workSheetList	= new ArrayList();
//add 2004/10/26 T.Sakai データ0件対応
		int		dispClassFlgCnt		= 0;		// グラフ表示フラグカウンタ
//add end

		// 基本ファイルを読込む
		C33ListBean c33ListBean = new C33ListBean();
		
		try {
			
			// データセット
			ArrayList c33List = c33Item.getC33List();
			Iterator itr = c33List.iterator();
			
			if( itr.hasNext() == false ){
				return errfdata;
			}
			
			while( itr.hasNext() ) {
				c33ListBean = (C33ListBean)itr.next();

//[add 2004.10.22 設問名Null時の処理回避] → 2005.05.24 [2004.12.21 設問がない科目も出力する]の反映漏れ対応
//				if ( !cm.toString(c33ListBean.getStrSetsuMei1()).equals("") ) {
//[add end]
					if (c33ListBean.getIntDispKmkFlg()==1) {
						//科目が変わる時のチェック
						if ( !cm.toString(kmk).equals(cm.toString(c33ListBean.getStrKmkCd())) ) {
							setsumonCnt = 0;
							bolSheetCngFlg = true;
						}
						//10設問ごとにシート名リスト初期化
						if (setsumonCnt==0) {
							workSheetList	= new ArrayList();
							sheetListIndex = 0;
						}
						
						// workbookListにWorkBookが２つ以上格納されている時は出力処理を実行
						int listSize = workbookList.size();
						if(listSize>1){
							if(setsumonCnt == 0){
								workbook = (HSSFWorkbook)workbookList.get(0);
			
								// Excelファイル保存
								boolean bolRet = false;
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, intMaxSheetSr);
								fileIndex++;
								if( bolRet == false ){
									return errfwrite;
								}
		
								// ファイル出力したデータは削除
								workbookList.remove(0);
		
								// WorkBook・変数を書き込んでる途中のものに戻す
								workbook = (HSSFWorkbook)workbookList.get(0);
							}
						}
						
						// 基本ファイルを読込む
						C33ClassListBean c33ClassListBean = new C33ClassListBean();
						
						// クラスデータセット
						ArrayList c33ClassList = c33ListBean.getC33ClassList();
						Iterator itrClass = c33ClassList.iterator();
						
						maxClass = 0;
						//クラスデータ件数取得
						while ( itrClass.hasNext() ){
							c33ClassListBean = (C33ClassListBean)itrClass.next();
							if (c33ClassListBean.getIntDispClassFlg()==1) {
								maxClass++;
							}
						}
						// クラス表示に必要なシート数の計算
						sheetRowCnt = (maxClass-1)/(houjiNum-1);
						if((maxClass-1)%(houjiNum-1)!=0){
							sheetRowCnt++;
						}
						if (sheetRowCnt==0) {
							sheetRowCnt++;
						}
						bolSheetCngFlg = true;
						
						int ninzu = 0;
						float tokuritsu = 0;
						
						itrClass = c33ClassList.iterator();
						
						while ( itrClass.hasNext() ){
							c33ClassListBean = (C33ClassListBean)itrClass.next();
							if (c33ClassListBean.getIntDispClassFlg()==1) {
								if( bolBookCngFlg == true ){
									//マスタExcel読み込み
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook==null ){
										return errfread;
									}
									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
									workbookList.add(workbook);
								}
								//クラス数が5以上または10設問ごとに改シート
								if ( bolSheetCngFlg == true ) {
									// データセットするシートの選択
									if (setsumonCnt==0) {
										//クラス用改シート
										workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex+1));
										workSheetList.add(sheetListIndex, workSheet);
										intMaxSheetIndex++;
										sheetListIndex++;
									} else {
										//次列用シートの呼び出し
										workSheet = (HSSFSheet)workSheetList.get(sheetListIndex-sheetRowCnt);
										sheetRowCnt--;
									}
									if (setsumonCnt==0) {
										// ヘッダ右側に帳票作成日時を表示する
										cm.setHeader(workbook, workSheet);
									
										// セキュリティスタンプセット
										String secuFlg = cm.setSecurity( workbook, workSheet, c33Item.getIntSecuFlg() ,31 ,33 );
										workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
										workCell.setCellValue(secuFlg);
									
										// 注釈セット
										if (c33Item.getIntShubetsuFlg() == 1){
										} else{
											workCell = cm.setCell( workSheet, workRow, workCell, 45, 33 );
											workCell.setCellValue( "※校内の平均得点率を下回るクラスの平均得点率に\"*\"を表示しています。" );
										}

										// 学校名セット
										workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
										workCell.setCellValue( "学校名　：" + cm.toString(c33Item.getStrGakkomei()) );
				
										// 模試月取得
										String moshi =cm.setTaisyouMoshi( c33Item.getStrMshDate() );
										// 対象模試セット
										workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
										workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(c33Item.getStrMshmei()) + moshi);
										
										// 型・科目名＋配点セット
										String haiten = "";
										if ( !cm.toString(c33ListBean.getStrHaitenKmk()).equals("") ) {
											haiten = "（" + c33ListBean.getStrHaitenKmk() + "）";
										}
										workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
										workCell.setCellValue( "型・科目：" + cm.toString(c33ListBean.getStrKmkmei()) + haiten );
										col = 1;
									}
									row = 46;
//delete 2004.10.29 T.Sakai 設問セット条件削除
//									if ( setsumonCnt==0 || !cm.toString(setsumon).equals(cm.toString(c33ListBean.getStrSetsuMei1())) ) {
//delete end
										// 設問番号セット
										workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
										workCell.setCellValue( c33ListBean.getStrSetsuNo() );
										// 設問内容セット
										workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
										workCell.setCellValue( c33ListBean.getStrSetsuMei1() );
										// 設問配点セット
										workCell = cm.setCell( workSheet, workRow, workCell, row, col );
										if (!cm.toString(c33ListBean.getStrSetsuHaiten()).equals("") ){
											workCell.setCellValue( "（" + c33ListBean.getStrSetsuHaiten() + "）" );
										}
//delete 2004.10.29 T.Sakai 設問セット条件削除
//									}
//delete end
									bolSheetCngFlg = false;
								}
								if ( classCnt==0 ) {
									row = 50;
									if (col==1) {
										// 学校名セット
										workCell = cm.setCell( workSheet, workRow, workCell, row, col-1 );
										workCell.setCellValue( c33Item.getStrGakkomei() );
									}
									// 人数セット
									ninzu = c33ClassListBean.getIntNinzu();
									workCell = cm.setCell( workSheet, workRow, workCell, row, col );
									if ( c33ClassListBean.getIntNinzu() != -999 ) {
										workCell.setCellValue( c33ClassListBean.getIntNinzu() );
									}
									// 得点率セット
									tokuritsu = c33ClassListBean.getFloTokuritsu();
									workCell = cm.setCell( workSheet, workRow, workCell, row++, col+2 );
									if ( c33ClassListBean.getFloTokuritsu() != -999.0 ) {
										workCell.setCellValue( c33ClassListBean.getFloTokuritsu() );
									}
								} else {
									if ( classCnt%houjiNum==0 ) {
										row = 50;
										if (col==1) {
											// 学校名セット
											workCell = cm.setCell( workSheet, workRow, workCell, row, col-1 );
											workCell.setCellValue( c33Item.getStrGakkomei() );
										}
										// 人数セット
										workCell = cm.setCell( workSheet, workRow, workCell, row, col );
										if ( ninzu != -999 ) {
											workCell.setCellValue( ninzu );
										}
										// 得点率セット
										workCell = cm.setCell( workSheet, workRow, workCell, row++, col+2 );
										if ( tokuritsu != -999.0 ) {
											workCell.setCellValue( tokuritsu );
										}
										classCnt++;
									}
									if (col==1) {
										// 学年+クラス名セット
										workCell = cm.setCell( workSheet, workRow, workCell, row, col-1 );
										workCell.setCellValue( cm.toString(c33ClassListBean.getStrGrade())+"年 "+cm.toString(c33ClassListBean.getStrClass())+"クラス" );
									}
									// 人数セット
									workCell = cm.setCell( workSheet, workRow, workCell, row, col );
									if ( c33ClassListBean.getIntNinzu() != -999 ) {
										workCell.setCellValue( c33ClassListBean.getIntNinzu() );
									}
									// *セット
									// 得点用のときは処理しない
									if (c33Item.getIntShubetsuFlg() == 1){
									} else{
										if ( c33ClassListBean.getFloTokuritsu() != -999.0 ) {
											if ( tokuritsu != -999.0 ) {
												if ( c33ClassListBean.getFloTokuritsu() < tokuritsu ) {
													workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
													workCell.setCellValue("*");
												}
											}
										}
									}
									// 得点率セット
									workCell = cm.setCell( workSheet, workRow, workCell, row++, col+2 );
									if ( c33ClassListBean.getFloTokuritsu() != -999.0 ) {
										workCell.setCellValue( c33ClassListBean.getFloTokuritsu() );
									}
								}
								classCnt++;
								
								if(classCnt%houjiNum==0){
									if(itrClass.hasNext()){
										if(setsumonCnt==0){
											if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
												bolBookCngFlg = true;
											}
										}
										bolSheetCngFlg = true;
									}
								}
								if(itrClass.hasNext()==false){
									if(setsumonCnt==0){
										if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
											bolBookCngFlg = true;
										}
										bolSheetCngFlg = true;
									}
								}
//add 2004/10/26 T.Sakai データ0件対応
								dispClassFlgCnt++;
//add end
							}
						}
						kmk = c33ListBean.getStrKmkCd();
						col = col + 3;
						classCnt = 0;
						setsumonCnt++;
						if ( setsumonCnt==11 ) {
							bolSheetCngFlg = true;
							setsumonCnt = 0;
							if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
								bolBookCngFlg = true;
							}
						}
		
						// workbookListにWorkBookが２つ以上格納されている時は出力処理を実行
						listSize = workbookList.size();
						if(listSize>1){
							if(setsumonCnt == 0){
								workbook = (HSSFWorkbook)workbookList.get(0);
			
								// Excelファイル保存
								boolean bolRet = false;
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, intMaxSheetSr);
								fileIndex++;
								if( bolRet == false ){
									return errfwrite;
								}
		
								// ファイル出力したデータは削除
								workbookList.remove(0);
		
								// WorkBook・変数を書き込んでる途中のものに戻す
								workbook = (HSSFWorkbook)workbookList.get(0);
							}
						}
					}
//[add 2004.10.22 設問名Null時の処理回避] → 2005.05.24 [2004.12.21 設問がない科目も出力する]の反映漏れ対応
//				}
//[add end]
			}
			
			// 改ブック後に1科目しかないと保存されずにここに来る
			// workbookListにWorkBookが２つ以上格納されている時は出力処理を実行
			if(workbookList.size() > 1){
				workbook = (HSSFWorkbook)workbookList.get(0);

				// Excelファイル保存
				boolean bolRet = false;
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, intMaxSheetSr);
				fileIndex++;
				if( bolRet == false ){
					return errfwrite;
				}

				// ファイル出力したデータは削除
				workbookList.remove(0);

				// WorkBook・変数を書き込んでる途中のものに戻す
				workbook = (HSSFWorkbook)workbookList.get(0);
			}
			
//add 2004/10/26 T.Sakai データ0件対応
			if ( dispClassFlgCnt==0 ) {
				//マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				intMaxSheetIndex = 0;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex+1));
				intMaxSheetIndex++;
				
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
						
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, c33Item.getIntSecuFlg() ,31 ,33 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
				workCell.setCellValue(secuFlg);
									
				// 注釈セット
				if (c33Item.getIntShubetsuFlg() == 1){
				} else{
					workCell = cm.setCell( workSheet, workRow, workCell, 45, 30 );
					workCell.setCellValue( "※校内の平均得点率を下回るクラスの平均得点率に\"*\"を表示しています。" );
				}

				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(c33Item.getStrGakkomei()) );
				
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( c33Item.getStrMshDate() );
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(c33Item.getStrMshmei()) + moshi);
			}
//add end
			
			// 現ワークブックに有効シートがあるなら保存
			if (intMaxSheetIndex > 0) {
				// Excelファイル保存
				boolean bolRet = false;
				if(fileIndex != 1){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, intMaxSheetIndex);
				}
				else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}
	
				if( bolRet == false ){
					return errfwrite;					
				}
			}
			
		} catch(Exception e) {
			log.Err("C33_02","データセットエラー",e.toString());
			return errfdata;
		}
		
		return noerror;
	}

}
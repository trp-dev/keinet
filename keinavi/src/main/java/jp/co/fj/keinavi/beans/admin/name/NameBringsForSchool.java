/*
 * 作成日: 2004/10/07
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.admin.name;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.fj.keinavi.beans.admin.FormatChecker;
import jp.co.fj.keinavi.beans.admin.StudentTableAccess;

import org.apache.commons.dbutils.DbUtils;

import oracle.jdbc.OraclePreparedStatement;

/**
 * 学校向け名寄せ処理クラス
 * ※名寄せバッチからのみアクセス可能
 *
 * 2005.06.07	Yoshimoto KAWAI - Totec
 * 				科目別成績の範囲区分追加対応
 *
 * 2009.10.27   Shoji HASE - Totec
 *				CANDIDATERATINGnからDEPTCDの抽出を「7文字目から2文字」→「7文字目から4文字」に変更。
 *				その他INDIVIDUALRECORD_TMPのレイアウト変更にともない各項目の抽出位置と、文字列長を修正する。
 *
 * 2010.01.20	Fujito URAKAWA - Totec
 * 				「学力レベル」追加対応
 * 				SUBRECORDnの最後尾に追加された学力レベルをSUBRECORD_Iの"ACADEMICLEVEL"にINSERTする
 * 				処理を追加する。
 *
 * 2020.04.10 FJ)Tanaka
 * 	学校マスタ番号帯拡大対応
 *		高校（特進）（一括コード 73xxx）もしくは 中学一括校（一括コード 74xxx）なら、親学校コードを使用する
 *			↓
 *		高校（特進）（一括コード 73xxx 79xxx）もしくは 中学一括校（一括コード 74xxx）なら、親学校コードを使用する
 *
 * @author 奥村ゆかり
 */
public class NameBringsForSchool {

	// Singleton
	private static final NameBringsForSchool instance = new NameBringsForSchool(new NameBringsNear());

	private static final String MSG_FMT = "レコード長が異なります 要求[%d]、実際[%d] データ[%s]";

	// DBコネクション
	Connection con;
	// 名寄せ判定クラスインスタンス
	private NameBringsNear nameIdentifyInstance;

	// 英語の設問８をオーラルとする模試コード
	private static final Set HOLD_EXAM_CD = new HashSet();
	// オープン模試の模試コード
	private static final Set OPEN_EXAM_CD = new HashSet();

	static {
		HOLD_EXAM_CD.add("71");
		HOLD_EXAM_CD.add("72");
		HOLD_EXAM_CD.add("73");
		HOLD_EXAM_CD.add("74");
		HOLD_EXAM_CD.add("61");
		HOLD_EXAM_CD.add("62");
		HOLD_EXAM_CD.add("63");

		OPEN_EXAM_CD.add("12");
		OPEN_EXAM_CD.add("13");
		OPEN_EXAM_CD.add("15");
		OPEN_EXAM_CD.add("16");
		OPEN_EXAM_CD.add("18");
		OPEN_EXAM_CD.add("19");
		OPEN_EXAM_CD.add("21");
		OPEN_EXAM_CD.add("22");
		OPEN_EXAM_CD.add("24");
		OPEN_EXAM_CD.add("25");
		OPEN_EXAM_CD.add("26");
		OPEN_EXAM_CD.add("27");
		OPEN_EXAM_CD.add("31");
		OPEN_EXAM_CD.add("32"); // 医進模試
		OPEN_EXAM_CD.add("41");
		OPEN_EXAM_CD.add("42");
		OPEN_EXAM_CD.add("43");
	}

	/**
	 * コンストラクタ
	 */
	private NameBringsForSchool(NameBringsNear name) {
		this.nameIdentifyInstance = name;
	}

	/**
	 * 名寄せ処理
	 *
	 * @param inputData 個表データ
	 * @return void
	 * @see java.sql.SQLException
	 * @see java.sql.Exception
	 */
	public void doCombi(HashMap inputData) throws SQLException, IOException {

		// 学校コード取得
		inputData.put("SCHOOLCD", SelSchoolCd(con, (String) inputData.get("BUNDLECD")));

		// 名寄せ判定
		nameIdentifyInstance.setConnection(null, con);
		nameIdentifyInstance.setInputData(inputData);
		nameIdentifyInstance.execute();
		List record = nameIdentifyInstance.getResult();
		//System.out.println("status:"+nameIdentifyInstance.getCombiJudge());
		switch (nameIdentifyInstance.getCombiJudge()) {
			case 1:
				//			SystemLogWriter.printLog( this,"結合状態１：個表データから各マスタへ＆個表データのIDと状態を変更",SystemLogWriter.INFO);
				//			log.Lv1("0M202",0,"結合状態１：個表データから各マスタへ＆個表データのIDと状態を変更");
				// ----------------
				//  結合状態1
				// ----------------
                inputData.put("INDIVIDUALID", (String) record.get(0));
				// 個表データから各マスタへ＆個表データのIDと状態を変更
				InsWorkToMaster(con, inputData, (String) record.get(0));
 
				break;
			case 2:
				//			SystemLogWriter.printLog( this,"結合状態２：学籍履歴情報の登録及び個表データのIDと状態を変更",SystemLogWriter.INFO);
				//			log.Lv1("0M202",0,"結合状態２：学籍履歴情報の登録及び個表データのIDと状態を変更");
				// ----------------
				//  結合状態2
				// ----------------
				// 個人IDの取得
				inputData.put("INDIVIDUALID", (String) record.get(0));

				// 学籍履歴情報の登録
				StudentTableAccess.InsHistoryInfo(con, inputData);
				// 個表データから各マスタへ＆個表データのIDと状態を変更
				InsWorkToMaster(con, inputData, (String) record.get(0));
				break;
			case 3:
				//			SystemLogWriter.printLog( this,"結合状態３：学籍情報（基本＆履歴）の登録及び個表データのIDと状態を変更",SystemLogWriter.INFO);
				//			log.Lv1("0M202",0,"結合状態３：学籍情報（基本＆履歴）の登録及び個表データのIDと状態を変更");
				//----------------
				//  結合状態３
				// ----------------

				// 個人IDの取得
				String INDIVIDUALID = (String) inputData.get("YEAR02") + StudentTableAccess.GetIndividualID(con);
				inputData.put("INDIVIDUALID", FormatChecker.str2Char32(INDIVIDUALID, 10));

				// 学籍基本情報の登録
				StudentTableAccess.InsBasicInfo(con, inputData);

				//			// 個人IDの抽出
				//			//System.out.println("学籍基本情報の登録："+inputData);
				//			String INDIVIDUALID = StudentTableAccess.IsBasicInfoCountIndex(conn,inputData);
				//			if(INDIVIDUALID ==null)throw new SQLException("個人IDの抽出に失敗しました");

				// 学籍履歴情報の登録
				StudentTableAccess.InsHistoryInfo(con, inputData);
				// 個表データから各マスタへ＆個表データのIDと状態を変更
				InsWorkToMaster(con, inputData, INDIVIDUALID);
				break;
		}
	}

	/**
	 *
	 * ---------------------------------------------------
	 * 設問別成績、科目別成績、志望校評価にレコードを追加する
	 * ---------------------------------------------------
	 *
	 * @param con DBコネクション
	 * @param combiData 結合条件に合致した生徒情報 inputData 個表データ
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public void InsWorkToMaster(Connection con, HashMap inputData, String individualId) throws SQLException {

		//		SystemLogWriter.printLog(
		//			NameBringsBean.class,
		//			"成績レコードインサート開始",
		//			SystemLogWriter.INFO
		//		);

		String year = (String) inputData.get("EXAMYEAR");
		String examCd = (String) inputData.get("EXAMCD");
		String bundleCd = (String) inputData.get("BUNDLECD");
		String answerSheetNo = (String) inputData.get("ANSWERSHEET_NO");

		// センターリサーチかどうか
		boolean cr = "38".equals(examCd);
		// OP模試かどうか
		boolean open = OPEN_EXAM_CD.contains(examCd);

		// 科目別成績
		{
			PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("INS_SUBRECORD_I");

			if (ps == null) {
				String query = "INSERT INTO SUBRECORD_I " + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";

				ps = con.prepareStatement(query);
				((OraclePreparedStatement) ps).setExecuteBatch(100);
				NameBringsBean.STATEMENT.put("INS_SUBRECORD_I", ps);
			}

			for (int i = 1; i < 27; i++) {
				String value = (String) inputData.get("SUBRECORD" + i);
				if (value == null)
					continue;

				byte subValue[] = value.getBytes(NameBringsBean.CHARSET);

				// レコード長が32でなければ次
				if (!validateLength(value, 32))
					continue;

				// 科目コード
				String subCd = byte2RecordValue(subValue, 0, 4);
				// NULLなら次
				if (subCd == null)
					continue;

				ps.setString(1, individualId);
				ps.setString(2, year);
				ps.setString(3, examCd);
				ps.setString(4, subCd);
				ps.setString(5, bundleCd);
				NameBringsTableAccess.setInt(ps, 6, byte2RecordValue(subValue, 4, 7));
				NameBringsTableAccess.setInt(ps, 7, byte2RecordValue(subValue, 7, 10));
				NameBringsTableAccess.setInt(ps, 8, byte2RecordValue(subValue, 10, 14));
				NameBringsTableAccess.setInt(ps, 9, byte2RecordValue(subValue, 14, 22));
				NameBringsTableAccess.setDouble(ps, 10, byte2RecordValue(subValue, 22, 26));
				NameBringsTableAccess.setDouble(ps, 11, byte2RecordValue(subValue, 26, 30));
				NameBringsTableAccess.setInt(ps, 12, byte2RecordValue(subValue, 30, 31, false).trim());
				NameBringsTableAccess.setString(ps, 13, byte2RecordValue(subValue, 31, 32, false).trim());

				ps.executeUpdate();
			}

			((OraclePreparedStatement) ps).sendBatch();
		}

		// 設問別成績
		{
			PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("INS_QRECORD_I");

			if (ps == null) {
				String query = "INSERT INTO QRECORD_I" + " VALUES(?,?,?,?,?,?,?,?,?)";

				ps = con.prepareStatement(query);
				((OraclePreparedStatement) ps).setExecuteBatch(300);
				NameBringsBean.STATEMENT.put("INS_QRECORD_I", ps);
			}

			for (int i = 1; i < 11; i++) {
				String value = (String) inputData.get("QUESTIONRECORD" + i);
				if (value == null)
					continue;

				byte queValue[] = value.getBytes(NameBringsBean.CHARSET);

				// レコード長が284でなければ次
				if (!validateLength(value, 284))
					continue;

				// 科目コード
				String subCd = byte2RecordValue(queValue, 0, 4);
				// NULLなら次
				if (subCd == null)
					continue;

				String questionNo1 = null;
				for (int s = 0; s < 266; s = s + 14) {
					// 設問番号
					String questionNo = byte2RecordValue(queValue, 4 + s, 7 + s);
					// NULLなら次
					if (questionNo == null)
						continue;

					// 一番初めの設問番号を保持する
					if (questionNo1 == null)
						questionNo1 = questionNo;

					ps.setString(1, individualId);
					ps.setString(2, year);
					ps.setString(3, examCd);
					ps.setString(4, subCd);
					NameBringsTableAccess.setInt(ps, 5, questionNo);
					ps.setString(6, bundleCd);
					NameBringsTableAccess.setInt(ps, 7, byte2RecordValue(queValue, 7 + s, 10 + s));
					NameBringsTableAccess.setDouble(ps, 8, byte2RecordValue(queValue, 10 + s, 14 + s));
					NameBringsTableAccess.setDouble(ps, 9, byte2RecordValue(queValue, 14 + s, 18 + s));

					// 設問別成績：科目コード1110の場合設問番号８のデータのみ別途
					// 設問別成績として作成。
					if (isOraruExamCd(examCd) && "1110".equals(subCd) && s == 98) {
						ps.setString(4, "1620");
						NameBringsTableAccess.setInt(ps, 5, questionNo1);
					}

					ps.executeUpdate();
				}
			}

			((OraclePreparedStatement) ps).sendBatch();
		}

		// 志望校別評価成績
		{
			PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("INS_CANDIDATERATING");

			if (ps == null) {
				ps = con.prepareStatement("INSERT INTO CANDIDATERATING" + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				((OraclePreparedStatement) ps).setExecuteBatch(50);
				NameBringsBean.STATEMENT.put("INS_CANDIDATERATING", ps);
			}

			for (int i = 1; i < 10; i++) {
				String value = (String) inputData.get("CANDIDATERATING" + i);
				if (value == null)
					continue;

				byte canValue[] = value.getBytes(NameBringsBean.CHARSET);

				// レコード長が83でなければ次
				if (!validateLength(value, 83))
					continue;

				// 大学コード
				String univCd = byte2RecordValue(canValue, 0, 4);
				// NULLなら次
				if (univCd == null)
					continue;

				// 志望順位
				String candidateRank = byte2RecordValue(canValue, 15, 16, false);
				// 0なら次
				if ("0".equals(candidateRank))
					continue;

				ps.setString(1, individualId);
				ps.setString(2, year);
				ps.setString(3, examCd);
				NameBringsTableAccess.setInt(ps, 4, candidateRank);
				ps.setString(5, univCd);
				ps.setString(6, byte2RecordValue(canValue, 4, 6));
				ps.setString(7, byte2RecordValue(canValue, 6, 10));
				ps.setString(8, bundleCd);

				// OP模試は評価得点は入れない
				if (open) {
					ps.setString(9, null);
				} else {
					NameBringsTableAccess.setInt(ps, 9, byte2RecordValue(canValue, 16, 20));
				}

				NameBringsTableAccess.setDouble(ps, 10, byte2RecordValue(canValue, 20, 24));
				NameBringsTableAccess.setInt(ps, 11, byte2RecordValue(canValue, 24, 27));

				// センターリサーチなら評価を変換
				//if (cr) {
				//	ps.setString(12, this.convRating(byte2RecordValue(canValue, 27, 29).trim()));
				//	ps.setString(13, this.convRating(byte2RecordValue(canValue, 29, 31).trim()));
				//	ps.setString(14, this.convRating(byte2RecordValue(canValue, 31, 33).trim()));
				// そうでなければそのまま
				//} else {
				ps.setString(12, byte2RecordValue(canValue, 27, 29).trim());
				ps.setString(13, byte2RecordValue(canValue, 29, 31).trim());
				ps.setString(14, byte2RecordValue(canValue, 31, 33).trim());
				//}

				NameBringsTableAccess.setInt(ps, 15, byte2RecordValue(canValue, 33, 41));
				NameBringsTableAccess.setInt(ps, 16, byte2RecordValue(canValue, 41, 49));

				// 評価得点（英語参加試験含む）
				NameBringsTableAccess.setInt(ps, 17, byte2RecordValue(canValue, 49, 53));
				// センター評価（英語参加試験含む）
				ps.setString(18, byte2RecordValue(canValue, 53, 55));
				// 英語資格要件−出願資格判定
				ps.setString(19, byte2RecordValue(canValue, 55, 57));
				// 英語資格要件−出願基準非対応告知
				ps.setString(20, byte2RecordValue(canValue, 57, 59));
				// 総志望者数（英語参加試験含む）
				NameBringsTableAccess.setInt(ps, 21, byte2RecordValue(canValue, 59, 67));
				// 総志望内順位（英語参加試験含む）
				NameBringsTableAccess.setInt(ps, 22, byte2RecordValue(canValue, 67, 75));
				// 共通テスト満点値
				NameBringsTableAccess.setInt(ps, 23, byte2RecordValue(canValue, 75, 79));
				// 共通テスト満点値（英語参加試験含む）
				NameBringsTableAccess.setInt(ps, 24, byte2RecordValue(canValue, 79, 83));

				ps.executeUpdate();
			}

			((OraclePreparedStatement) ps).sendBatch();
		}

		// 国語記述
		insertJapDesc(inputData, individualId, year, examCd, bundleCd);

		// 英語認定試験
		insertEngPt(inputData, individualId, year, examCd, bundleCd);

		// 科目別学力要素
		insertAcademic(inputData, individualId, year, examCd, bundleCd);

		// 個表の個人IDと状態を変更
		NameBringsTableAccess.UpdIndiviDualRec(con, individualId, year, examCd, answerSheetNo);

		//		SystemLogWriter.printLog(
		//			NameBringsBean.class,
		//			"成績レコードインサート終了",
		//			SystemLogWriter.INFO
		//		);
	}

	/**
	 * 文字バイト配列を指定されたバイトで切り出す
	 * （オール9はNULLで返す）
	 * マルチバイト文字対応版
	 *
	 * @param value 文字バイト配列
	 * @param st バイト切り出し開始位置
	 * @param ed バイト切り出し終了位置
	 * @return 切り出された文字
	 */
	private String byte2RecordValue(byte value[], int st, int ed) {
		return byte2RecordValue(value, st, ed, true);
	}

	/**
	 * 文字バイト配列を指定されたバイトで切り出す
	 * マルチバイト文字対応版
	 *
	 * @param value 文字バイト配列
	 * @param st バイト切り出し開始位置
	 * @param ed バイト切り出し終了位置
	 * @param isConv オール9の場合、nullに置き換えるかどうか
	 * @return 切り出された文字
	 */
	private String byte2RecordValue(byte value[], int st, int ed, boolean isConv) {
		int len = ed - st;
		String result = new String(value, st, len, NameBringsBean.CHARSET);
		/* オール9はnullに置き換える */
		if (isConv) {
			return "".equals(result.replaceAll("[9]", "")) ? null : result;
		} else {
			return result;
		}
	}

	/**
	 * 国語記述を登録する。
	 * @throws SQLException
	 *
	 */
	private void insertJapDesc(HashMap inputData, String individualId, String year, String examCd, String bundleCd) throws SQLException {
		// 国語記述
		PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("INS_KOKUGO_DESC_I");
		if (ps == null) {
			String query = "INSERT INTO KOKUGO_DESC_I VALUES(?,?,?,?,?,?,?,?,?)";

			ps = con.prepareStatement(query);
			((OraclePreparedStatement) ps).setExecuteBatch(300);
			NameBringsBean.STATEMENT.put("INS_KOKUGO_DESC_I", ps);
		}

		String value = (String) inputData.get("JAPDESC");
		if (value == null)
			return;

		byte queValue[] = value.getBytes(NameBringsBean.CHARSET);

		// レコード長が7でなければ次
		if (!validateLength(value, 7))
			return;

		//個人ID
		ps.setString(1, individualId);
		//模試年度
		ps.setString(2, year);
		//模試コード
		ps.setString(3, examCd);
		//科目コード
		ps.setString(4, "3915");
		//一括コード
		ps.setString(5, bundleCd);
		//国語（記述）　問１評価
		ps.setString(6, byte2RecordValue(queValue, 0, 2));
		//国語（記述）　問２評価
		ps.setString(7, byte2RecordValue(queValue, 2, 4));
		//国語（記述）　問３評価
		ps.setString(8, byte2RecordValue(queValue, 4, 6));
		//国語（記述）　総合評価
		ps.setString(9, byte2RecordValue(queValue, 6, 7));
		ps.executeUpdate();
		((OraclePreparedStatement) ps).sendBatch();

	}

	/**
	 * 英語認定試験を登録する。
	 * @throws SQLException
	 *
	 */
	private void insertEngPt(HashMap inputData, String individualId, String year, String examCd, String bundleCd) throws SQLException {
		// 英語認定試験
		PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("INS_CEFR_RECORD_I");
		if (ps == null) {
			String query = "INSERT INTO CEFR_RECORD_I VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			ps = con.prepareStatement(query);
			((OraclePreparedStatement) ps).setExecuteBatch(300);
			NameBringsBean.STATEMENT.put("INS_CEFR_RECORD_I", ps);
		}

		String value = (String) inputData.get("ENGPT");
		if (value == null)
			return;

		byte queValue[] = value.getBytes(NameBringsBean.CHARSET);

		// レコード長が38でなければ次
		if (!validateLength(value, 38))
			return;

		//個人ID
		ps.setString(1, individualId);
		//模試年度
		ps.setString(2, year);
		//模試コード
		ps.setString(3, examCd);
		//一括コード
		ps.setString(4, bundleCd);
		//認定試験コード1
		ps.setString(5, byte2RecordValue(queValue, 0, 2));
		//認定試験レベルコード1
		ps.setString(6, byte2RecordValue(queValue, 2, 4));
		//スコア1
		NameBringsTableAccess.setDouble(ps, 7, byte2RecordValue(queValue, 4, 9));
		//スコア−補正フラグ1
		ps.setString(8, byte2RecordValue(queValue, 9, 10));
		//合否区分1
		ps.setString(12, byte2RecordValue(queValue, 10, 11));
		//合否区分−補正フラグ1
		ps.setString(13, byte2RecordValue(queValue, 11, 12));
		//CEFRレベルコード1
		ps.setString(9, byte2RecordValue(queValue, 12, 14));
		//CEFRレベル正式名1
		ps.setString(10, byte2RecordValue(queValue, 14, 18));
		//CEFRレベル−補正フラグ1
		ps.setString(11, byte2RecordValue(queValue, 18, 19));
		//試験コード2
		ps.setString(14, byte2RecordValue(queValue, 19, 21));
		//試験詳細コード2
		ps.setString(15, byte2RecordValue(queValue, 21, 23));
		//スコア2
		NameBringsTableAccess.setDouble(ps, 16, byte2RecordValue(queValue, 23, 28));
		//スコア−補正フラグ2
		ps.setString(17, byte2RecordValue(queValue, 28, 29));
		//合否区分2
		ps.setString(21, byte2RecordValue(queValue, 29, 30));
		//合否区分−補正フラグ2
		ps.setString(22, byte2RecordValue(queValue, 30, 31));
		//CEFRレベルコード2
		ps.setString(18, byte2RecordValue(queValue, 31, 33));
		//CEFRレベル正式名2
		ps.setString(19, byte2RecordValue(queValue, 33, 37));
		//CEFRレベル−補正フラグ2
		ps.setString(20, byte2RecordValue(queValue, 37, 38));
		ps.executeUpdate();
		((OraclePreparedStatement) ps).sendBatch();

	}

	private void insertAcademic(HashMap inputData, String individualId, String year, String examCd, String bundleCd) throws SQLException {
		PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("INS_SUBACADEMIC_I");

		if (ps == null) {
			ps = con.prepareStatement("INSERT INTO SUBACADEMIC_I" + " VALUES(?,?,?,?,?,?,?,?,?)");
			((OraclePreparedStatement) ps).setExecuteBatch(50);
			NameBringsBean.STATEMENT.put("INS_SUBACADEMIC_I", ps);
		}

		for (int i = 1; i < 7; i++) {
			String value = (String) inputData.get("ACADEMIC" + i);
			if (value == null)
				continue;

			byte canValue[] = value.getBytes(NameBringsBean.CHARSET);

			// レコード長が16でなければ次
			if (!validateLength(value, 16))
				continue;

			// 科目コード
			String subCd = byte2RecordValue(canValue, 0, 4);
			// NULLなら次
			if (subCd == null)
				continue;

			//個人ID
			ps.setString(1, individualId);
			//模試年度
			ps.setString(2, year);
			//模試コード
			ps.setString(3, examCd);
			//科目コード
			ps.setString(4, subCd);
			//一括コード
			ps.setString(5, bundleCd);
			//並び順
			ps.setString(6, String.valueOf(i));
			//知識技能得点率
			NameBringsTableAccess.setDouble(ps, 7, byte2RecordValue(canValue, 4, 8));
			//思考力判断力得点率
			NameBringsTableAccess.setDouble(ps, 8, byte2RecordValue(canValue, 8, 12));
			//表現力得点率
			NameBringsTableAccess.setDouble(ps, 9, byte2RecordValue(canValue, 12, 16));

			ps.executeUpdate();
		}

		((OraclePreparedStatement) ps).sendBatch();
	}

	/**
	 *
	 * ----------------------------------------------------------------------------
	 * 設問別成績(P保護用)、科目別成績(P保護用)、志望校評価(P保護用)レコードを追加する
	 * ----------------------------------------------------------------------------
	 *
	 * @param con DBコネクション
	 * @param inputData 個表データ
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public void InsWorkToMasterPP(HashMap workRecord) throws SQLException {
		// 個表ワークデータを取得
		String targetYear = (String) workRecord.get("EXAMYEAR");
		String targetExam = (String) workRecord.get("EXAMCD");
		String answerSheet_No = (String) workRecord.get("ANSWERSHEET_NO");

		String bundleCd = (String) workRecord.get("BUNDLECD");
		String targetGrade = (String) workRecord.get("GRADE");
		String targetClass = (String) workRecord.get("CLASS");

		// センターリサーチかどうか
		boolean cr = "38".equals(targetExam);
		// オープン模試かどうか
		boolean open = OPEN_EXAM_CD.contains(targetExam);

		//	科目別成績（４・５年以外）
		if (!"4".equals(targetGrade) && !"5".equals(targetGrade)) {
			PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("INS_SUBRECORD_PP");

			if (ps == null) {
				String query = "INSERT INTO SUBRECORD_PP " + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

				ps = con.prepareStatement(query);
				((OraclePreparedStatement) ps).setExecuteBatch(100);
				NameBringsBean.STATEMENT.put("INS_SUBRECORD_PP", ps);
			}

			for (int i = 1; i < 27; i++) {
				String value = (String) workRecord.get("SUBRECORD" + i);
				if (value == null)
					continue;

				byte subValue[] = value.getBytes(NameBringsBean.CHARSET);

				// レコード長が32でなければ次
				if (!validateLength(value, 32))
					continue;

				// 科目コード
				String subCd = byte2RecordValue(subValue, 0, 4);
				// NULLなら次
				if (subCd == null)
					continue;

				ps.setString(1, targetYear);
				ps.setString(2, targetExam);
				ps.setString(3, answerSheet_No);
				ps.setString(4, subCd);
				ps.setString(5, bundleCd);
				NameBringsTableAccess.setInt(ps, 6, targetGrade);
				ps.setString(7, targetClass);
				NameBringsTableAccess.setInt(ps, 8, byte2RecordValue(subValue, 4, 7));
				NameBringsTableAccess.setInt(ps, 9, byte2RecordValue(subValue, 7, 10));
				NameBringsTableAccess.setInt(ps, 10, byte2RecordValue(subValue, 10, 14));
				NameBringsTableAccess.setInt(ps, 11, byte2RecordValue(subValue, 14, 22));
				NameBringsTableAccess.setDouble(ps, 12, byte2RecordValue(subValue, 22, 26));
				NameBringsTableAccess.setDouble(ps, 13, byte2RecordValue(subValue, 26, 30));
				NameBringsTableAccess.setInt(ps, 14, byte2RecordValue(subValue, 30, 31, false).trim());
				NameBringsTableAccess.setString(ps, 15, byte2RecordValue(subValue, 31, 32, false).trim());

				ps.executeUpdate();
			}

			((OraclePreparedStatement) ps).sendBatch();
		}

		// 設問別成績（４・５年以外）
		if (!"4".equals(targetGrade) && !"5".equals(targetGrade)) {
			PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("INS_QRECORD_PP");

			if (ps == null) {
				String query = "INSERT INTO QRECORD_PP" + " VALUES(?,?,?,?,?,?,?,?,?,?,?)";

				ps = con.prepareStatement(query);
				((OraclePreparedStatement) ps).setExecuteBatch(300);
				NameBringsBean.STATEMENT.put("INS_QRECORD_PP", ps);
			}

			for (int i = 1; i < 11; i++) {
				String value = (String) workRecord.get("QUESTIONRECORD" + i);
				if (value == null)
					continue;

				byte queValue[] = value.getBytes(NameBringsBean.CHARSET);

				// レコード長が284でなければ次
				if (!validateLength(value, 284))
					continue;

				// 科目コード
				String subCd = byte2RecordValue(queValue, 0, 4);
				// NULLなら次
				if (subCd == null)
					continue;

				String questionNo1 = null;
				for (int s = 0; s < 266; s = s + 14) {
					// 設問番号
					String questionNo = byte2RecordValue(queValue, 4 + s, 7 + s);
					// NULLなら次
					if (questionNo == null)
						continue;

					// 一番初めの設問番号を保持する
					if (questionNo1 == null)
						questionNo1 = questionNo;

					ps.setString(1, targetYear);
					ps.setString(2, targetExam);
					ps.setString(3, answerSheet_No);
					ps.setString(4, subCd);
					NameBringsTableAccess.setInt(ps, 5, questionNo);
					ps.setString(6, bundleCd);
					NameBringsTableAccess.setInt(ps, 7, targetGrade);
					ps.setString(8, targetClass);
					NameBringsTableAccess.setInt(ps, 9, byte2RecordValue(queValue, 7 + s, 10 + s));
					NameBringsTableAccess.setDouble(ps, 10, byte2RecordValue(queValue, 10 + s, 14 + s));
					NameBringsTableAccess.setDouble(ps, 11, byte2RecordValue(queValue, 14 + s, 18 + s));

					// 設問別成績：科目コード1110の場合設問番号８のデータのみ別途
					// 設問別成績として作成。
					if (isOraruExamCd(targetExam) && "1110".equals(subCd) && s == 98) {
						ps.setString(4, "1620");
						NameBringsTableAccess.setInt(ps, 5, questionNo1);
					}

					ps.executeUpdate();
				}
			}

			((OraclePreparedStatement) ps).sendBatch();
		}

		// 志望校別評価成績
		{
			PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("INS_CANDIDATERATING_PP");

			if (ps == null) {
				String query = "INSERT INTO CANDIDATERATING_PP" + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

				ps = con.prepareStatement(query);
				((OraclePreparedStatement) ps).setExecuteBatch(50);
				NameBringsBean.STATEMENT.put("INS_CANDIDATERATING_PP", ps);
			}

			for (int i = 1; i < 10; i++) {
				String value = (String) workRecord.get("CANDIDATERATING" + i);
				if (value == null)
					continue;

				byte canValue[] = value.getBytes(NameBringsBean.CHARSET);

				// レコード長が83でなければ次
				if (!validateLength(value, 83))
					continue;

				// 大学コード
				String univCd = byte2RecordValue(canValue, 0, 4);
				// NULLなら次
				if (univCd == null)
					continue;

				// 志望順位
				String candidateRank = byte2RecordValue(canValue, 15, 16, false);
				// 0なら次
				if ("0".equals(candidateRank))
					continue;
				ps.setString(1, targetYear);
				ps.setString(2, targetExam);
				ps.setString(3, answerSheet_No);
				NameBringsTableAccess.setInt(ps, 4, candidateRank);
				ps.setString(5, univCd);
				ps.setString(6, byte2RecordValue(canValue, 4, 6));
				ps.setString(7, byte2RecordValue(canValue, 6, 10));
				ps.setString(8, bundleCd);
				NameBringsTableAccess.setInt(ps, 9, targetGrade);
				ps.setString(10, targetClass);

				// OP模試は評価得点は入れない
				if (open) {
					ps.setString(11, null);
				} else {
					NameBringsTableAccess.setInt(ps, 11, byte2RecordValue(canValue, 16, 20));
				}

				NameBringsTableAccess.setDouble(ps, 12, byte2RecordValue(canValue, 20, 24));
				NameBringsTableAccess.setInt(ps, 13, byte2RecordValue(canValue, 24, 27));

				// センターリサーチなら評価を変換
				//if (cr) {
				//    ps.setString(14, this.convRating(byte2RecordValue(canValue, 27, 29).trim()));
				//    ps.setString(15, this.convRating(byte2RecordValue(canValue, 29, 31).trim()));
				//    ps.setString(16, this.convRating(byte2RecordValue(canValue, 31, 33).trim()));
				// そうでなければそのまま
				//} else {
				ps.setString(14, byte2RecordValue(canValue, 27, 29).trim());
				ps.setString(15, byte2RecordValue(canValue, 29, 31).trim());
				ps.setString(16, byte2RecordValue(canValue, 31, 33).trim());
				//}
				NameBringsTableAccess.setInt(ps, 17, byte2RecordValue(canValue, 33, 41));
				NameBringsTableAccess.setInt(ps, 18, byte2RecordValue(canValue, 41, 49));
				// 評価得点（英語参加試験含む）
				NameBringsTableAccess.setInt(ps, 19, byte2RecordValue(canValue, 49, 53));
				// センター評価（英語参加試験含む）
				ps.setString(20, byte2RecordValue(canValue, 53, 55));
				// 英語資格要件−出願資格判定
				ps.setString(21, byte2RecordValue(canValue, 55, 57));
				// 英語資格要件−出願基準非対応告知
				ps.setString(22, byte2RecordValue(canValue, 57, 59));
				// 総志望者数（英語参加試験含む）
				NameBringsTableAccess.setInt(ps, 23, byte2RecordValue(canValue, 59, 67));
				// 総志望内順位（英語参加試験含む）
				NameBringsTableAccess.setInt(ps, 24, byte2RecordValue(canValue, 67, 75));
				// 共通テスト満点値
				NameBringsTableAccess.setInt(ps, 25, byte2RecordValue(canValue, 75, 79));
				// 共通テスト満点値（英語参加試験含む）
				NameBringsTableAccess.setInt(ps, 26, byte2RecordValue(canValue, 79, 83));

				ps.executeUpdate();
			}

			((OraclePreparedStatement) ps).sendBatch();
		}

		// 国語記述
		insertJapDescPP(workRecord, targetYear, targetExam, answerSheet_No, bundleCd, targetGrade, targetClass);

		// 英語認定試験
		insertEngPtPP(workRecord, targetYear, targetExam, answerSheet_No, bundleCd, targetGrade, targetClass);

		// 科目別学力要素
		insertAcademicPP(workRecord, targetYear, targetExam, answerSheet_No, bundleCd, targetGrade, targetClass);

		// 個表の個人IDと状態を変更
		NameBringsTableAccess.UpdIndiviDualRec(con, "ZZZZZZZZZZ", targetYear, targetExam, answerSheet_No);
	}

	/**
	 * 国語記述を登録する。
	 * @throws SQLException
	 *
	 */
	private void insertJapDescPP(HashMap inputData, String year, String examCd, String answerNo, String bundleCd, String targetGrade, String targetClass) throws SQLException {
		// 国語記述
		PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("INS_KOKUGO_DESC_I_PP");
		if (ps == null) {
			String query = "INSERT INTO KOKUGO_DESC_I_PP VALUES(?,?,?,?,?,?,?,?,?,?,?)";

			ps = con.prepareStatement(query);
			((OraclePreparedStatement) ps).setExecuteBatch(300);
			NameBringsBean.STATEMENT.put("INS_KOKUGO_DESC_I_PP", ps);
		}

		String value = (String) inputData.get("JAPDESC");
		if (value == null)
			return;

		byte queValue[] = value.getBytes(NameBringsBean.CHARSET);

		// レコード長が7でなければ次
		if (!validateLength(value, 7))
			return;

		//模試年度
		ps.setString(1, year);
		//模試コード
		ps.setString(2, examCd);
		//解答用紙番号
		ps.setString(3, answerNo);
		//科目コード
		ps.setString(4, "3915");
		//学校コード
		ps.setString(5, bundleCd);
		//学年
		NameBringsTableAccess.setInt(ps, 6, targetGrade);
		//クラス
		ps.setString(7, targetClass);
		//国語（記述）　問１評価
		ps.setString(8, byte2RecordValue(queValue, 0, 2));
		//国語（記述）　問２評価
		ps.setString(9, byte2RecordValue(queValue, 2, 4));
		//国語（記述）　問３評価
		ps.setString(10, byte2RecordValue(queValue, 4, 6));
		//国語（記述）　総合評価
		ps.setString(11, byte2RecordValue(queValue, 6, 7));
		ps.executeUpdate();
		((OraclePreparedStatement) ps).sendBatch();

	}

	/**
	 * 英語認定試験を登録する。
	 * @throws SQLException
	 *
	 */
	private void insertEngPtPP(HashMap inputData, String year, String examCd, String answerNo, String bundleCd, String targetGrade, String targetClass) throws SQLException {
		// 国語記述
		PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("INS_CEFR_RECORD_I_PP");
		if (ps == null) {
			String query = "INSERT INTO CEFR_RECORD_I_PP VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			ps = con.prepareStatement(query);
			((OraclePreparedStatement) ps).setExecuteBatch(300);
			NameBringsBean.STATEMENT.put("INS_CEFR_RECORD_I_PP", ps);
		}

		String value = (String) inputData.get("ENGPT");
		if (value == null)
			return;

		byte queValue[] = value.getBytes(NameBringsBean.CHARSET);

		// レコード長が38でなければ次
		if (!validateLength(value, 38))
			return;

		//模試年度
		ps.setString(1, year);
		//模試コード
		ps.setString(2, examCd);
		//解答用紙番号
		ps.setString(3, answerNo);
		//学校コード
		ps.setString(4, bundleCd);
		//学年
		NameBringsTableAccess.setInt(ps, 5, targetGrade);
		//クラス
		ps.setString(6, targetClass);
		//認定試験コード1
		ps.setString(7, byte2RecordValue(queValue, 0, 2));
		//認定試験レベルコード1
		ps.setString(8, byte2RecordValue(queValue, 2, 4));
		//スコア1
		NameBringsTableAccess.setDouble(ps, 9, byte2RecordValue(queValue, 4, 9));
		//スコア−補正フラグ1
		ps.setString(10, byte2RecordValue(queValue, 9, 10));
		//合否区分1
		ps.setString(14, byte2RecordValue(queValue, 10, 11));
		//合否区分−補正フラグ1
		ps.setString(15, byte2RecordValue(queValue, 11, 12));
		//CEFRレベルコード1
		ps.setString(11, byte2RecordValue(queValue, 12, 14));
		//CEFRレベル正式名1
		ps.setString(12, byte2RecordValue(queValue, 14, 18));
		//CEFRレベル−補正フラグ1
		ps.setString(13, byte2RecordValue(queValue, 18, 19));
		//試験コード2
		ps.setString(16, byte2RecordValue(queValue, 19, 21));
		//試験詳細コード2
		ps.setString(17, byte2RecordValue(queValue, 21, 23));
		//スコア2
		NameBringsTableAccess.setDouble(ps, 18, byte2RecordValue(queValue, 23, 28));
		//スコア−補正フラグ2
		ps.setString(19, byte2RecordValue(queValue, 28, 29));
		//合否区分2
		ps.setString(23, byte2RecordValue(queValue, 29, 30));
		//合否区分−補正フラグ2
		ps.setString(24, byte2RecordValue(queValue, 30, 31));
		//CEFRレベルコード2
		ps.setString(20, byte2RecordValue(queValue, 31, 33));
		//CEFRレベル正式名2
		ps.setString(21, byte2RecordValue(queValue, 33, 37));
		//CEFRレベル−補正フラグ2
		ps.setString(22, byte2RecordValue(queValue, 37, 38));
		ps.executeUpdate();
		((OraclePreparedStatement) ps).sendBatch();

	}

	private void insertAcademicPP(HashMap inputData, String year, String examCd, String answerNo, String bundleCd, String targetGrade, String targetClass) throws SQLException {
		PreparedStatement ps = (PreparedStatement) NameBringsBean.STATEMENT.get("INS_SUBACADEMIC_I_PP");

		if (ps == null) {
			ps = con.prepareStatement("INSERT INTO SUBACADEMIC_I_PP" + " VALUES(?,?,?,?,?,?,?,?,?,?,?)");
			((OraclePreparedStatement) ps).setExecuteBatch(50);
			NameBringsBean.STATEMENT.put("INS_SUBACADEMIC_I_PP", ps);
		}

		for (int i = 1; i < 7; i++) {
			String value = (String) inputData.get("ACADEMIC" + i);
			if (value == null)
				continue;

			byte canValue[] = value.getBytes(NameBringsBean.CHARSET);

			// レコード長が16でなければ次
			if (!validateLength(value, 16))
				continue;

			// 科目コード
			String subCd = byte2RecordValue(canValue, 0, 4);
			// NULLなら次
			if (subCd == null)
				continue;

			//模試年度
			ps.setString(1, year);
			//模試コード
			ps.setString(2, examCd);
			//解答用紙番号
			ps.setString(3, answerNo);
			//科目コード
			ps.setString(4, subCd);
			//学校コード
			ps.setString(5, bundleCd);
			//学年
			NameBringsTableAccess.setInt(ps, 6, targetGrade);
			//クラス
			ps.setString(7, targetClass);
			//並び順
			ps.setString(8, String.valueOf(i));
			//知識技能得点率
			NameBringsTableAccess.setDouble(ps, 9, byte2RecordValue(canValue, 4, 8));
			//思考力判断力得点率
			NameBringsTableAccess.setDouble(ps, 10, byte2RecordValue(canValue, 8, 12));
			//表現力得点率
			NameBringsTableAccess.setDouble(ps, 11, byte2RecordValue(canValue, 12, 16));

			ps.executeUpdate();
		}

		((OraclePreparedStatement) ps).sendBatch();
	}

	// 模試コードが範囲に合致するか
	private boolean isOraruExamCd(String key) {
		return HOLD_EXAM_CD.contains(key);
	}

	/**
	 * 学校コードの取得
	 *
	 * @param con DBコネクション
	 * @param bundredCd 一括コード
	 * @return code 学校コード
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public String SelSchoolCd(Connection con, String bundredCd) throws SQLException {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		String code = null;
		try {
			if (bundredCd == null)
				throw new SQLException("学校コードの取得に失敗しました。(NULL)");

			// ------------------------------------------------------------
			// 2020.04.09 FJ)Tanaka 学校マスタ番号帯拡大対応 MOD STA
			// ------------------------------------------------------------
			if (bundredCd.startsWith("73") || bundredCd.startsWith("74") || bundredCd.startsWith("79")) {
			// ------------------------------------------------------------
			// 2020.04.09 FJ)Tanaka 学校マスタ番号帯拡大対応 MOD END
			// ------------------------------------------------------------

				stmt = con.prepareStatement("SELECT PARENTSCHOOLCD FROM SCHOOL WHERE BUNDLECD = ?");
				stmt.setString(1, bundredCd);

				rs = stmt.executeQuery();
				if (rs.next())
					code = rs.getString(1);

				if (code == null)
					throw new SQLException("学校コードの取得に失敗しました。(" + bundredCd + ")");

			} else {
				code = bundredCd;
			}

		} catch (SQLException e) {
			throw e;

		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
		}

		return code;
	}

	/**
	 * 文字列長をチェックします。
	 *
	 * @param value 値
	 * @param length 項目長
	 * @return true:一致、false:不一致
	 */
	private boolean validateLength(String value, int length) {
		byte bytes[] = value.getBytes(NameBringsBean.CHARSET);
		int len = bytes.length;
		boolean result = len == length;
		// レコード長一致しなければログを出力
		if (!result) {
			SystemLogWriter.printLog(NameBringsForSchool.class, String.format(MSG_FMT, length, len, value), SystemLogWriter.WARNING);
		}
		return result;
	}

	/**
	 * 評価を変換する
	 * @param value
	 * @return
	 */
	private String convRating(String value) {
		if (value == null)
			return null;

		StringBuffer buff = new StringBuffer();
		for (int i = 0; i < value.length(); i++) {
			switch (value.charAt(i)) {
				case 'A':
					buff.append("◎");
					break;
				case 'B':
					buff.append("○");
					break;
				case 'C':
					buff.append("△");
					break;
				case 'D':
					buff.append("▲");
					break;
				default:
					buff.append(value.charAt(i));
			}
		}
		return buff.toString();
	}

	/**
	 * @param connection
	 */
	public void setConnection(Connection connection) {
		con = connection;
	}

	/**
	 * @return
	 */
	public static NameBringsForSchool getInstance() {
		return instance;
	}

}

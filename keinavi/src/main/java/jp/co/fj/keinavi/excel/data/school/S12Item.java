package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * 偏差値分布（校内成績）データクラス
 * 作成日: 2004/07/06
 * @author	T.Kuzuno
 */
public class S12Item {
	//学校名
	private String strGakkomei = "";
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//表フラグ
	private int intHyouFlg = 0;
	//構成比フラグ
	private int intKoseihiFlg = 0;
	//度数分布グラフフラグ
	private int intBnpGraphFlg = 0;
	//度数分布グラフ軸フラグ
	private int intAxisFlg = 0;
	//人数積み上げグラフフラグ
	private int intNinzuGraphFlg = 0;
	//人数積み上げグラフピッチフラグ
	private int intNinzuPitchFlg = 0;
	//構成比グラフフラグ
	private int intKoseiGraphFlg = 0;
	//構成比グラフピッチフラグ
	private int intKoseiPitchFlg = 0;

	// 2019/07/23 QQ)Tanioka 共通テスト対応 ADD START
	//共通テスト英語認定試験CEFR取得状況フラグ
	private int intCheckBoxFlg = 0;
	//自校受験者がいる試験を対象にするフラグ
	private int intTargetCheckBoxFlg = 0;
	// 2019/07/23 QQ)Tanioka 共通テスト対応 ADD END

	//セキュリティスタンプフラグ
	private int intSecuFlg = 0;
	//データリスト
	private ArrayList s12List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;

	// 2019/09/30 QQ) 共通テスト対応 ADD START
	private S12CefrMain s12CefrMain = new S12CefrMain();
	// 2019/09/30 QQ) 共通テスト対応 ADD END

	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public int getIntHyouFlg() {
		return this.intHyouFlg;
	}
	public int getIntKoseihiFlg() {
		return this.intKoseihiFlg;
	}
	public int getIntBnpGraphFlg() {
		return this.intBnpGraphFlg;
	}
	public int getIntAxisFlg() {
		return this.intAxisFlg;
	}
	public int getIntNinzuGraphFlg() {
		return this.intNinzuGraphFlg;
	}
	public int getIntNinzuPitchFlg() {
		return this.intNinzuPitchFlg;
	}
	public int getIntKoseiGraphFlg() {
		return this.intKoseiGraphFlg;
	}
	public int getIntKoseiPitchFlg() {
		return this.intKoseiPitchFlg;
	}
	// 2019/07/23 QQ)Tanioka 共通テスト対応 ADD START
	public int getIntCheckBoxFlg() {
		return this.intCheckBoxFlg;
	}
	public int getIntTargetCheckBoxFlg() {
		return this.intTargetCheckBoxFlg;
	}
	// 2019/07/23 QQ)Tanioka 共通テスト対応 ADD END
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public ArrayList getS12List() {
		return this.s12List;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}
	// 2019/09/30 QQ) 共通テスト対応 ADD START
	public S12CefrMain getS12CefrMain() {
		return s12CefrMain;
	}
	// 2019/09/30 QQ) 共通テスト対応 ADD END

	/*---------------*/
	/* Set */
	/*---------------*/
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setIntHyouFlg(int intHyouFlg) {
		this.intHyouFlg = intHyouFlg;
	}
	public void setIntKoseihiFlg(int intKoseihiFlg) {
		this.intKoseihiFlg = intKoseihiFlg;
	}
	public void setIntBnpGraphFlg(int intBnpGraphFlg) {
		this.intBnpGraphFlg = intBnpGraphFlg;
	}
	public void setIntAxisFlg(int intAxisFlg) {
		this.intAxisFlg = intAxisFlg;
	}
	public void setIntNinzuGraphFlg(int intNinzuGraphFlg) {
		this.intNinzuGraphFlg = intNinzuGraphFlg;
	}
	public void setIntNinzuPitchFlg(int intNinzuPitchFlg) {
		this.intNinzuPitchFlg = intNinzuPitchFlg;
	}
	public void setIntKoseiGraphFlg(int intKoseiGraphFlg) {
		this.intKoseiGraphFlg = intKoseiGraphFlg;
	}
	public void setIntKoseiPitchFlg(int intKoseiPitchFlg) {
		this.intKoseiPitchFlg = intKoseiPitchFlg;
	}
	// 2019/07/23 QQ)Tanioka 共通テスト対応 ADD START
	public void setIntCheckBoxFlg(int intCheckBoxFlg) {
		this.intCheckBoxFlg = intCheckBoxFlg;
	}
	public void setIntTargetCheckBoxFlg(int intTargetCheckBoxFlg) {
		this.intTargetCheckBoxFlg = intTargetCheckBoxFlg;
	}
	// 2019/07/23 QQ)Tanioka 共通テスト対応 ADD END
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setS12List(ArrayList s12List) {
		this.s12List = s12List;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}
	// 2019/09/30 QQ) 共通テスト対応 ADD START
	public void setS12CefrMain(S12CefrMain s12CefrMain) {
		this.s12CefrMain = s12CefrMain;
	}
	// 2019/09/30 QQ) 共通テスト対応 ADD END

}

/*
 * 作成日: 2004/07/027
 *
 * 成績分析／成績推移グラフ表示用サーブレット
 *
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.graph.LineGraphApplet;
import jp.co.fj.keinavi.beans.help.OnepointBean;
import jp.co.fj.keinavi.beans.individual.I106Bean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.IExamData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I106Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * @author Totec) K.Kondo
 *
 * 成績分析／成績推移グラフを表示する。
 * プロファイルからデータ取得するが、I106で再表示のときはフォームからデータ取得
 *
 * 2005.3.8 	Totec) K.Kondo 		[1]I106Beanに担当クラスの年度を渡すように修正
 * 2005.8.23 	Totec) T.Yamada 	[2]印刷での対象模試がセンターリサーチの時のチェックボックス無効時の対応
 * 2016.1.14	QQ) K.Hisakawa		サーブレット化対応
 *
 */
public class I106Servlet extends IndividualServlet {

	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		super.execute(request, response);

		//フォームデータを取得 ::::::::
		final I106Form i106Form = (I106Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.individual.I106Form");

		// ログイン情報
		final LoginSession login = getLoginSession(request);

		//プロファイル情報
		final Profile profile = getProfile(request);
		Short examType = (Short)profile.getItemMap(IProfileCategory.I_SCORE_TRANS).get(IProfileItem.EXAM_TYPE);
		String devRanges = (String)profile.getItemMap(IProfileCategory.I_SCORE_TRANS).get(IProfileItem.DEV_RANGE);
		Short indMultiExam = (Short)profile.getItemMap(IProfileCategory.I_SCORE_TRANS).get(IProfileItem.IND_MULTI_EXAM);
		Short printStamp = (Short) profile.getItemMap(IProfileCategory.I_SCORE_BC).get(IProfileItem.PRINT_STAMP);
		Short StudentDispGrade = (Short) profile.getItemMap(IProfileCategory.I_SCORE_TRANS).get(IProfileItem.STUDENT_DISP_GRADE);
		Short printStudent = (Short) profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);

		//個人共通
		final ICommonMap iCommonMap = getICommonMap(request);

		// 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
        Collection cCourseDatas = null;
        Collection cExaminationDatas = null;
        String sLineItemName = null;
        int iExaminationDatasSize = 0;
        // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

		// 転送元がJSPなら対象模試情報を初期化
		if ("i106".equals(getBackward(request))) {
			initTargetExam(request);
		}

		// JSP転送
		if ("i106".equals(getForward(request))) {
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);

				//ワンポイントアドバイス
				OnepointBean onebean = new OnepointBean();
				onebean.setConnection(null, con);
				onebean.setScreenID("i106");
				onebean.execute();
				request.setAttribute("OnepointBean", onebean);

				I106Bean i106Bean = new I106Bean();
				//ログインフラグ
				i106Bean.setHelpflg(login.isHelpDesk());
				i106Bean.setClassYear(ProfileUtil.getChargeClassYear(profile));//[1] add
				if (!"i106".equals(getBackward(request))
						|| !iCommonMap.getTargetPersonId().equals(i106Form.getTargetPersonId())
						|| isChangedMode(request) || isChangedExam(request)){

					// 生徒を切り替えたとき
					// モードを切り替えたとき
					// 対象模試を変えたとき
	                if ("i106".equals(getBackward(request))) {

						i106Bean.setFirstAccess(true);
						i106Bean.setTargetGrade(i106Form.getTargetGrade());
						i106Bean.setTargetExamType(i106Form.getTargetExamType());
						i106Bean.setSearchCondition(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(),
														CollectionUtil.deSplitComma(new String[]{i106Form.getLowerRange(), i106Form.getUpperRange()}),
														i106Form.getTargetPersonId(),i106Bean.getTargetGrade(),i106Bean.getTargetExamType());

						// 教科複数受験フラグをbeanにセット
						i106Bean.setAvgChoice(i106Form.getAvgChoice());

						//検索実行
						i106Bean.setConnection("", con);
						i106Bean.execute();

						//1.印刷対象生徒をフォームにセット
						String[] printTarget = {"0","0","0","0"};
						if(i106Form.getPrintTarget() != null){
							for(int i=0; i<i106Form.getPrintTarget().length; i++){
								printTarget[Integer.parseInt(i106Form.getPrintTarget()[i])] = "1";
							}
						}
						//[2] add start
						final String CENTER_EXAMCD = "38";
						if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
							printTarget[3] = "1";
						}
						//[2] add end
						i106Form.setPrintTarget(printTarget);

						// 対象模試＋比較対象模試が第1解答課目模試以外の場合、１教科複数受験は平均とする
						// i106Bean.execute()で判断し変更している
						i106Form.setAvgChoice(i106Bean.getAvgChoice());

						// 初回アクセス
	                } else {
						i106Bean.setFirstAccess(true);
						i106Bean.setTargetGrade(StudentDispGrade.toString());
						i106Bean.setTargetExamType(examType.toString());
						i106Bean.setSearchCondition(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(),
														devRanges, i106Form.getTargetPersonId(),
														i106Bean.getTargetGrade(),i106Bean.getTargetExamType());

						// 教科複数受験フラグをbeanにセット
						i106Bean.setAvgChoice(""+indMultiExam);

						//検索実行
						i106Bean.setConnection("", con);
						i106Bean.execute();

						//初回用フォーム作成
						i106Form.setTargetGrade(i106Bean.getTargetGrade());
						i106Form.setTargetExamType(i106Bean.getTargetExamType());

						//個人成績分析共通-印刷対象生保存をセット
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
						//i106Form.setPrintStudent(printStudent.toString());
						i106Form.setPrintStudent(iCommonMap.getPrintStudent());
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END

						//1.印刷対象生徒をセット
						String printTarget = (String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET);
						i106Form.setPrintTarget(CollectionUtil.splitComma(printTarget));

						//2.表示する学年をセット
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
						//i106Form.setTargetGrade(StudentDispGrade.toString());
						i106Form.setTargetGrade(iCommonMap.getTargetGrade());
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END

						//3.模試の種類をセット
						i106Form.setTargetExamType(examType.toString());

						//4.偏差値範囲をセット
						i106Form.setLowerRange(CollectionUtil.deconcatComma(devRanges)[0]);
						i106Form.setUpperRange(CollectionUtil.deconcatComma(devRanges)[1]);

						//3.教科複数受験を保存
						// 対象模試＋比較対象模試が第1解答課目模試以外の場合、１教科複数受験は平均とする
						// i106Bean.execute()で判断し変更している
						i106Form.setAvgChoice(i106Bean.getAvgChoice());

						//6.セキュリティスタンプを保存
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
						//i106Form.setPrintStamp(printStamp.toString());
						i106Form.setPrintStamp(iCommonMap.getPrintStamp());
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
	                }

	            // 再表示アクセス
				} else {
					i106Bean.setFirstAccess(false);
					i106Bean.setTargetGrade(i106Form.getTargetGrade());
					i106Bean.setTargetExamType(i106Form.getTargetExamType());
					i106Bean.setSearchCondition(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(),
													CollectionUtil.deSplitComma(new String[]{i106Form.getLowerRange(), i106Form.getUpperRange()}),
													i106Form.getTargetPersonId(),i106Bean.getTargetGrade(),
													i106Bean.getTargetExamType());

					/** フォームの値をプロファイルに保存　*/

					//1.印刷対象生徒をフォームにセット
					String[] printTarget = {"0","0","0","0"};
					if(i106Form.getPrintTarget() != null){
						for(int i=0; i<i106Form.getPrintTarget().length; i++){
							printTarget[Integer.parseInt(i106Form.getPrintTarget()[i])] = "1";
						}
					}
					//[2] add start
					final String CENTER_EXAMCD = "38";
					if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
						printTarget[3] = "1";
					}
					//[2] add end
					i106Form.setPrintTarget(printTarget);

					//2.表示する学年を保存
					profile.getItemMap(IProfileCategory.I_SCORE_TRANS).put(IProfileItem.STUDENT_DISP_GRADE, new Short(i106Form.getTargetGrade()));

					//3.模試の種類を保存
					profile.getItemMap(IProfileCategory.I_SCORE_TRANS).put(IProfileItem.EXAM_TYPE, new Short(i106Form.getTargetExamType()));

					//4.偏差値範囲を保存
					String devRangeUL = CollectionUtil.deSplitComma(new String[]{i106Form.getLowerRange(),i106Form.getUpperRange()});
					profile.getItemMap(IProfileCategory.I_SCORE_TRANS).put(IProfileItem.DEV_RANGE, devRangeUL);

					//5.教科複数受験を保存
					profile.getItemMap(IProfileCategory.I_SCORE_TRANS).put(IProfileItem.IND_MULTI_EXAM, new Short(i106Form.getAvgChoice()));

					// 教科複数受験フラグをbeanにセット
					i106Bean.setAvgChoice(i106Form.getAvgChoice());
					//検索実行
					i106Bean.setConnection("", con);
					i106Bean.execute();

					// 対象模試＋比較対象模試が第1解答課目模試以外の場合、１教科複数受験は平均とする
					// i106Bean.execute()で判断し変更している
					i106Form.setAvgChoice(i106Bean.getAvgChoice());
				}

				request.setAttribute("i106Bean", i106Bean);

                // 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
				cCourseDatas = i106Bean.getCourseDatas();
		        cExaminationDatas = i106Bean.getExaminationDatas();
		        sLineItemName = i106Bean.getLineItemName();
                iExaminationDatasSize = i106Bean.getExaminationDatasSize();
                // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

			} catch(final Exception e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			request.setAttribute("form", i106Form);

			iCommonMap.setTargetPersonId(i106Form.getTargetPersonId());

			// 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
			String dispSelect = "on";
		    if(iExaminationDatasSize <= 0) {
		        dispSelect = "off";
		    }

			String moshiString = "";
			//列は７固定、教科数によって行が変わる。

			int subMax = cCourseDatas.size() -1;

			String subAllString = "";
			String[] dataStrings = new String[subMax];

			for(int i = 0; i < subMax; i++) {
			    dataStrings[i] = "";
			}

			int subCount = 0;

			for (java.util.Iterator it=cExaminationDatas.iterator(); it.hasNext();) {

			    IExamData data = (IExamData)it.next();

			    if(data.getExamName().equals("")) {
			    } else {
			        if(subCount != 0){
			            moshiString += ",";
			            subAllString += ",";
			            for(int i = 0; i < subMax; i++) {
			                dataStrings[i] += ",";
			            }
			        }
			        moshiString += data.getExamName();
			        subAllString += data.getAvgAll();

			        if(i106Form.getAvgChoice().equals("1")){
			            if(subMax > 3) {
			                dataStrings[0] += GeneralUtil.to999(data.getAvgEng());
			                dataStrings[1] += GeneralUtil.to999(data.getAvgMath());
			                dataStrings[2] += GeneralUtil.to999(data.getAvgJap());
			                dataStrings[3] += GeneralUtil.to999(data.getAvgSci());
			                dataStrings[4] += GeneralUtil.to999(data.getAvgSoc());
			            } else {
			                dataStrings[0] += GeneralUtil.to999(data.getAvgEng());
			                dataStrings[1] += GeneralUtil.to999(data.getAvgMath());
			                dataStrings[2] += GeneralUtil.to999(data.getAvgJap());
			            }
			        } else if(i106Form.getAvgChoice().equals("3")){
			            if(subMax > 3) {
			                dataStrings[0] += GeneralUtil.to999(data.getAvgEng());
			                dataStrings[1] += GeneralUtil.to999(data.getAvgMath());
			                dataStrings[2] += GeneralUtil.to999(data.getAvgJap());
			                dataStrings[3] += GeneralUtil.to999(data.getAns1stSci());
			                dataStrings[4] += GeneralUtil.to999(data.getAns1stSoc());
			            } else {
			                dataStrings[0] += GeneralUtil.to999(data.getAvgEng());
			                dataStrings[1] += GeneralUtil.to999(data.getAvgMath());
			                dataStrings[2] += GeneralUtil.to999(data.getAvgJap());
			            }
			        } else {
			            if(subMax > 3) {
			                dataStrings[0] += GeneralUtil.to999(data.getMaxEng());
			                dataStrings[1] += GeneralUtil.to999(data.getMaxMath());
			                dataStrings[2] += GeneralUtil.to999(data.getMaxJap());
			                dataStrings[3] += GeneralUtil.to999(data.getMaxSci());
			                dataStrings[4] += GeneralUtil.to999(data.getMaxSoc());
			            } else {
			                dataStrings[0] += GeneralUtil.to999(data.getMaxEng());
			                dataStrings[1] += GeneralUtil.to999(data.getMaxMath());
			                dataStrings[2] += GeneralUtil.to999(data.getMaxJap());
			            }
			        }
			    }
			    subCount ++;
			}

            LineGraphApplet App = new LineGraphApplet();
            App.setParameter("dispSelect", dispSelect);
            App.setParameter("HensaZoneLow", String.valueOf((int)Double.parseDouble(i106Form.getLowerRange())));
            App.setParameter("HensaZoneHigh", String.valueOf((int)Double.parseDouble(i106Form.getUpperRange())));
            App.setParameter("LineItemNum", String.valueOf(subMax));
            App.setParameter("LineItemName", sLineItemName);
            App.setParameter("LineItemNameS", "総合");
            App.setParameter("yTitle", "偏差値");
            App.setParameter("colorDAT", "1,0,2,12,15,13,14");
            App.setParameter("examValueS", subAllString);
            App.setParameter("examName", moshiString);
            for(int i=0; i<dataStrings.length; i++){
                App.setParameter("examValue" + String.valueOf(i), dataStrings[i]);
            }
            App.setParameter("BarSize", "7");

            App.init();

            HttpSession session = request.getSession(true);

            session.setAttribute("GraphServlet", App);
            session.setAttribute("GraphType", "LineGraph");

            App = null;
            dataStrings = null;
            // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

			super.forward(request, response, JSP_I106);

		//不明ならServlet転送
		}else {

			//Forwordがsheetならプロファイルを保存する
			if ("sheet".equals(getForward(request))) {

				/** フォームの値をプロファイルに保存　*/

				//分析モードなら個人成績分析共通-印刷対象生保存をセット
				if(iCommonMap.isBunsekiMode()){
					profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(i106Form.getPrintStudent()));
				}

				//1.印刷対象生徒をフォームに保存
				String[] printTarget = {"0","0","0","0"};
				if(i106Form.getPrintTarget() != null){
					for(int i=0; i<i106Form.getPrintTarget().length; i++){
						printTarget[Integer.parseInt(i106Form.getPrintTarget()[i])] = "1";
					}
				}
				//[2] add start
				final String CENTER_EXAMCD = "38";
				if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
					printTarget[3] = "1";
				}
				//[2] add end
				profile.getItemMap(IProfileCategory.I_SCORE_COMMON).put(IProfileItem.PRINT_TARGET, CollectionUtil.deSplitComma(printTarget));

				//2.表示する学年を保存
				profile.getItemMap(IProfileCategory.I_SCORE_TRANS).put(IProfileItem.STUDENT_DISP_GRADE, new Short(i106Form.getTargetGrade()));

				//3.模試の種類を保存
				profile.getItemMap(IProfileCategory.I_SCORE_TRANS).put(IProfileItem.EXAM_TYPE, new Short(i106Form.getTargetExamType()));

				//4.偏差値範囲を保存
				String devRangeUL = CollectionUtil.deSplitComma(new String[]{i106Form.getLowerRange(),i106Form.getUpperRange()});
				profile.getItemMap(IProfileCategory.I_SCORE_TRANS).put(IProfileItem.DEV_RANGE, devRangeUL);

				//5.教科複数受験を保存
				profile.getItemMap(IProfileCategory.I_SCORE_TRANS).put(IProfileItem.IND_MULTI_EXAM, new Short(i106Form.getAvgChoice()));

				//6.セキュリティスタンプを保存
				profile.getItemMap(IProfileCategory.I_SCORE_BC).put(IProfileItem.PRINT_STAMP, new Short(i106Form.getPrintStamp()));

			} else {
				/** フォームの値をプロファイルに保存　*/

				//分析モードなら個人成績分析共通-印刷対象生保存をセット
				if(iCommonMap.isBunsekiMode()){
					profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(i106Form.getPrintStudent()));
				}

				//1.印刷対象生徒をフォームに保存
				String[] printTarget = {"0","0","0","0"};
				if(i106Form.getPrintTarget() != null){
					for(int i=0; i<i106Form.getPrintTarget().length; i++){
						printTarget[Integer.parseInt(i106Form.getPrintTarget()[i])] = "1";
					}
				}
				//[2] add start
				final String CENTER_EXAMCD = "38";
				if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
					printTarget[3] = "1";
				}
				//[2] add end
				profile.getItemMap(IProfileCategory.I_SCORE_COMMON).put(IProfileItem.PRINT_TARGET, CollectionUtil.deSplitComma(printTarget));

				//6.セキュリティスタンプを保存
				profile.getItemMap(IProfileCategory.I_SCORE_BC).put(IProfileItem.PRINT_STAMP, new Short(i106Form.getPrintStamp()));
			}

			iCommonMap.setTargetPersonId(i106Form.getTargetPersonId());
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD START
                        iCommonMap.setTargetGrade(i106Form.getTargetGrade());
                        iCommonMap.setPrintStamp(i106Form.getPrintStamp());
                        iCommonMap.setPrintStudent(i106Form.getPrintStudent());
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD END
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

/*
 * 作成日: 2004/07/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.profile;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;


/**
 * プロファイルの初期値を保持するクラス
 *
 * 2005.02.09 Yoshimoto KAWAI - Totec
 *            テキスト出力（模試別成績データ・新形式）を追加
 *
 * 2009.10.20 Fujito URAKAWA - Totec
 *            新校内資料「科目・設問別正答率（レベル別）」対応
 *
 * <2010年度マーク高２模試対応>
 * 2011.01.11   Tomohisa YAMADA - Totec
 *              倫政対応・マーク高２対応有
 *
 * <新過程対応>
 * 2013.04.23   Yuuichirou Morita - TOTEC
 *
 *
 */
public class DefaultProfile extends AbstractDefaultProfile implements IProfileCategory, IProfileItem {

    /**
     * カテゴリマップに初期値をセットする
     */
    public DefaultProfile() {
        // 1.共通項目設定
        getItemMap(CM).put(TYPE_SELECTION, new Short("1"));
        getItemMap(CM).put(COURSE_SELECTION, new Short("1"));
        getItemMap(CM).put(PREV_COMP_YEAR, new String[]{"1", "2"});
        getItemMap(CM).put(UNIV_SELECTION_FORMULA, new Short("1"));
        getItemMap(CM).put(CLASS_SELECTION, new Short("1"));
        getItemMap(CM).put(TYPE_COMMON, new ArrayList());
        getItemMap(CM).put(TYPE_IND, new ArrayList());
        getItemMap(CM).put(COURSE_COMMON, new ArrayList());
        getItemMap(CM).put(COURSE_IND, new ArrayList());
        getItemMap(CM).put(CLASS_COMP_CLASS, new ArrayList());
        getItemMap(CM).put(CLASS, new ArrayList());

        // 2.校内成績分析
        // 校内成績（成績概況）
        setPrintOutTarget(S_SCHOOL_SCORE);
        getItemMap(S_SCHOOL_SCORE).put(CHART, new Short("1"));
        getItemMap(S_SCHOOL_SCORE).put(GRAPH, new Short("0"));
        setStamp(S_SCHOOL_SCORE);
        // 校内成績（偏差値分布）
        setPrintOutTarget(S_SCHOOL_DEV);
        setType(S_SCHOOL_DEV);
        setCourse(S_SCHOOL_DEV);
        getItemMap(S_SCHOOL_DEV).put(CHART_COMP_RATIO, new Short("12"));
        getItemMap(S_SCHOOL_DEV).put(GRAPH_DIST, new Short("1"));
        getItemMap(S_SCHOOL_DEV).put(GRAPH_BUILDUP, new Short("1"));
        getItemMap(S_SCHOOL_DEV).put(GRAPH_COMP_RATIO, new Short("1"));
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD START
//        getItemMap(S_SCHOOL_DEV).put(OPTION_CHECK_BOX, new Short("1"));
//// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
        setStamp(S_SCHOOL_DEV);
        // 校内成績（設問別成績）
        setPrintOutTarget(S_SCHOOL_QUE);
        setCourse(S_SCHOOL_QUE);
        getItemMap(S_SCHOOL_QUE).put(CHART, new Short("1"));
        getItemMap(S_SCHOOL_QUE).put(GRAPH, new Short("0"));
// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD START
        getItemMap(S_SCHOOL_QUE).put(OPTION_CHECK_KOKUGO, new Short("0"));
        getItemMap(S_SCHOOL_QUE).put(OPTION_CHECK_STATUS, new Short("0"));
        getItemMap(S_SCHOOL_QUE).put(OPTION_CHECK_RECORD, new Short("0"));
// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD END
        getItemMap(S_SCHOOL_QUE).put(IND_MARK_EXAM, new Short("0"));
        getItemMap(S_SCHOOL_QUE).put(IND_MARK_EXAM_AREA, new Short("0"));
        setStamp(S_SCHOOL_QUE);
        // 過年度比較（成績概況）
        setPrintOutTarget(S_PREV_SCORE);
        getItemMap(S_PREV_SCORE).put(CHART, new Short("1"));
        getItemMap(S_PREV_SCORE).put(GRAPH, new Short("0"));
        setStamp(S_PREV_SCORE);
        // 過年度比較（偏差値分布）
        setPrintOutTarget(S_PREV_DEV);
        setType(S_PREV_DEV);
        setCourse(S_PREV_DEV);
        getItemMap(S_PREV_DEV).put(CHART_COMP_RATIO, new Short("12"));
        getItemMap(S_PREV_DEV).put(GRAPH_DIST, new Short("1"));
        getItemMap(S_PREV_DEV).put(GRAPH_BUILDUP, new Short("1"));
        getItemMap(S_PREV_DEV).put(GRAPH_COMP_RATIO, new Short("1"));
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD START
//        getItemMap(S_PREV_DEV).put(OPTION_CHECK_BOX, new Short("1"));
//// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
        setStamp(S_PREV_DEV);
        // 過年度比較（志望大学評価別人数）
        setPrintOutTarget(S_PREV_UNIV);
        getItemMap(S_PREV_UNIV).put(UNIV_ORDER, new Short("1"));
        getItemMap(S_PREV_UNIV).put(UNIV_COUNTING_DIV, new Short("3"));
        getItemMap(S_PREV_UNIV).put(UNIV_RATING, new String[]{"1", "2", "3"});
        setStamp(S_PREV_UNIV);
        // クラス比較（成績概況）
        setPrintOutTarget(S_CLASS_SCORE);
        setType(S_CLASS_SCORE);
        setCourse(S_CLASS_SCORE);
        getItemMap(S_CLASS_SCORE).put(CHART, new Short("1"));
        getItemMap(S_CLASS_SCORE).put(GRAPH, new Short("0"));
        getItemMap(S_CLASS_SCORE).put(CLASS_ORDER, new Short("1"));
        setStamp(S_CLASS_SCORE);
        // クラス比較（偏差値分布）
        setPrintOutTarget(S_CLASS_DEV);
        setType(S_CLASS_DEV);
        setCourse(S_CLASS_DEV);
        getItemMap(S_CLASS_DEV).put(CHART_COMP_RATIO, new Short("12"));
        getItemMap(S_CLASS_DEV).put(GRAPH_DIST, new Short("1"));
        getItemMap(S_CLASS_DEV).put(GRAPH_BUILDUP, new Short("1"));
        getItemMap(S_CLASS_DEV).put(GRAPH_COMP_RATIO, new Short("1"));
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD START
//        getItemMap(S_CLASS_DEV).put(OPTION_CHECK_BOX, new Short("1"));
//// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
        getItemMap(S_CLASS_DEV).put(CLASS_ORDER, new Short("1"));
        setStamp(S_CLASS_DEV);
        // クラス比較（設問別成績）
        setPrintOutTarget(S_CLASS_QUE);
        setCourse(S_CLASS_QUE);
        getItemMap(S_CLASS_QUE).put(CHART, new Short("1"));
        getItemMap(S_CLASS_QUE).put(GRAPH, new Short("0"));
// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD START
        getItemMap(S_CLASS_QUE).put(OPTION_CHECK_KOKUGO, new Short("0"));
        getItemMap(S_CLASS_QUE).put(OPTION_CHECK_STATUS, new Short("0"));
// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD END
        getItemMap(S_CLASS_QUE).put(CLASS_ORDER, new Short("1"));
        setStamp(S_CLASS_QUE);
        // クラス比較（志望大学評価別人数）
        setPrintOutTarget(S_CLASS_UNIV);
        getItemMap(S_CLASS_UNIV).put(CHART, new Short("1"));
        getItemMap(S_CLASS_UNIV).put(UNIV_ORDER, new Short("1"));
        getItemMap(S_CLASS_UNIV).put(UNIV_COUNTING_DIV, new Short("3"));
        getItemMap(S_CLASS_UNIV).put(CLASS_ORDER, new Short("1"));
        getItemMap(S_CLASS_UNIV).put(PREV_DISP, new String[]{"1", "2"});
        getItemMap(S_CLASS_UNIV).put(UNIV_RATING, new String[]{"1", "2", "3"});
        setStamp(S_CLASS_UNIV);
        // 他校比較（成績概況）
        setPrintOutTarget(S_OTHER_SCORE);
        setType(S_OTHER_SCORE);
        setCourse(S_OTHER_SCORE);
        getItemMap(S_OTHER_SCORE).put(CHART, new Short("1"));
        getItemMap(S_OTHER_SCORE).put(GRAPH, new Short("0"));
        getItemMap(S_OTHER_SCORE).put(SCHOOL_ORDER, new Short("1"));
        setStamp(S_OTHER_SCORE);
        // 他校比較比較（偏差値分布）
        setPrintOutTarget(S_OTHER_DEV);
        setType(S_OTHER_DEV);
        setCourse(S_OTHER_DEV);
        getItemMap(S_OTHER_DEV).put(CHART_COMP_RATIO, new Short("12"));
        getItemMap(S_OTHER_DEV).put(GRAPH_DIST, new Short("1"));
        getItemMap(S_OTHER_DEV).put(GRAPH_BUILDUP, new Short("1"));
        getItemMap(S_OTHER_DEV).put(GRAPH_COMP_RATIO, new Short("1"));
        getItemMap(S_OTHER_DEV).put(PREV_DISP, new Short("1"));
        getItemMap(S_OTHER_DEV).put(SCHOOL_ORDER, new Short("1"));
        setStamp(S_OTHER_DEV);
        // 他校比較比較（設問別成績）
        setPrintOutTarget(S_OTHER_QUE);
        setCourse(S_OTHER_QUE);
        getItemMap(S_OTHER_QUE).put(CHART, new Short("1"));
        getItemMap(S_OTHER_QUE).put(GRAPH, new Short("0"));
        getItemMap(S_OTHER_QUE).put(SCHOOL_ORDER, new Short("1"));
        setStamp(S_OTHER_QUE);
        // 他校比較比較（志望大学評価別人数）
        setPrintOutTarget(S_OTHER_UNIV);
        getItemMap(S_OTHER_UNIV).put(UNIV_ORDER, new Short("1"));
        getItemMap(S_OTHER_UNIV).put(UNIV_COUNTING_DIV, new Short("3"));
        getItemMap(S_OTHER_UNIV).put(SCHOOL_ORDER, new Short("1"));
        getItemMap(S_OTHER_UNIV).put(PREV_DISP, new String[]{"1", "2"});
        getItemMap(S_OTHER_UNIV).put(UNIV_RATING, new String[]{"1", "2", "3"});
        setStamp(S_OTHER_UNIV);
        // 過回比較（成績概況）
        setPrintOutTarget(S_PAST_SCORE);
        setType(S_PAST_SCORE);
        setCourse(S_PAST_SCORE);
        getItemMap(S_PAST_SCORE).put(CHART, new Short("1"));
        getItemMap(S_PAST_SCORE).put(GRAPH, new Short("0"));
        getItemMap(S_PAST_SCORE).put(PAST_COMP_PREV, new Short("0"));
        getItemMap(S_PAST_SCORE).put(PAST_COMP_CLASS, new Short("0"));
        getItemMap(S_PAST_SCORE).put(PAST_COMP_OTHER, new Short("0"));
        getItemMap(S_PAST_SCORE).put(SCHOOL_ORDER, new Short("1"));
        getItemMap(S_PAST_SCORE).put(CLASS_ORDER, new Short("1"));
        setStamp(S_PAST_SCORE);
        // 過回比較（偏差値分布）
        setPrintOutTarget(S_PAST_DEV);
        setType(S_PAST_DEV);
        setCourse(S_PAST_DEV);
        getItemMap(S_PAST_DEV).put(CHART_COMP_RATIO, new Short("12"));
        getItemMap(S_PAST_DEV).put(GRAPH_DIST, new Short("1"));
        getItemMap(S_PAST_DEV).put(GRAPH_BUILDUP, new Short("1"));
        getItemMap(S_PAST_DEV).put(GRAPH_COMP_RATIO, new Short("1"));
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD START
//        getItemMap(S_PAST_DEV).put(OPTION_CHECK_BOX, new Short("1"));
//// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
        setStamp(S_PAST_DEV);

        // 3.クラス成績分析
        // クラス成績概況（偏差値分布）
        setPrintOutTarget(C_COND_DEV);
        setType(C_COND_DEV);
        setCourse(C_COND_DEV);
        getItemMap(C_COND_DEV).put(CHART, new Short("1"));
        getItemMap(C_COND_DEV).put(GRAPH_DIST_NO_AXIS, new Short("0"));
        getItemMap(C_COND_DEV).put(GRAPH_BUILDUP, new Short("1"));
        setStamp(C_COND_DEV);
        // クラス成績概況（個人成績）
        setPrintOutTarget(C_COND_IND);
        getItemMap(C_COND_IND).put(CHART, new Short("1"));
        getItemMap(C_COND_IND).put(IND_SCORE_RANK, new Short("0"));
        getItemMap(C_COND_IND).put(UNIV_DISP, new Short("1"));
        getItemMap(C_COND_IND).put(PRINT_STAMP, new Short("1"));
        // クラス成績概況（個人成績推移）
        setPrintOutTarget(C_COND_TRANS);
        setType(C_COND_TRANS);
        setCourse(C_COND_TRANS);
        getItemMap(C_COND_TRANS).put(CHART, new Short("1"));
        getItemMap(C_COND_TRANS).put(IND_COMP, new Short("1"));
        getItemMap(C_COND_TRANS).put(STUDENT_ORDER, new Short("1"));
        getItemMap(C_COND_TRANS).put(IND_FLUCT_RANGE, new Short("0"));
        getItemMap(C_COND_TRANS).put(DEV, new Short("3"));
        getItemMap(C_COND_TRANS).put(PRINT_STAMP, new Short("1"));
        // クラス成績概況（設問別個人成績）
        setPrintOutTarget(C_COND_QUE);
        setCourse(C_COND_QUE);
        getItemMap(C_COND_QUE).put(STUDENT_ORDER, new Short("1"));
        getItemMap(C_COND_QUE).put(PRINT_STAMP, new Short("1"));
        // クラス成績概況（志望大学別個人評価）
        setPrintOutTarget(C_COND_UNIV);
        getItemMap(C_COND_UNIV).put(UNIV_DIV, new Short("1"));
        getItemMap(C_COND_UNIV).put(UNIV_CHOICE_ORDER, new Short("1"));
        getItemMap(C_COND_UNIV).put(UNIV_ORDER, new Short("3"));
        getItemMap(C_COND_UNIV).put(UNIV_SELECTION, new Short("1"));
        getItemMap(C_COND_UNIV).put(UNIV_CHOICE, new Short("2"));
        getItemMap(C_COND_UNIV).put(UNIV_CRIT, new String[]{"A", "B", "C", "D","E"});
        getItemMap(C_COND_UNIV).put(PRINT_STAMP, new Short("1"));
        // クラス比較（成績概況）
        setPrintOutTarget(C_COMP_SCORE);
        setType(C_COMP_SCORE);
        setCourse(C_COMP_SCORE);
        getItemMap(C_COMP_SCORE).put(CHART, new Short("1"));
        getItemMap(C_COMP_SCORE).put(GRAPH, new Short("0"));
        getItemMap(C_COMP_SCORE).put(CLASS_ORDER, new Short("1"));
        setStamp(C_COMP_SCORE);
        // クラス比較（偏差値分布）
        setPrintOutTarget(C_COMP_DEV);
        setType(C_COMP_DEV);
        setCourse(C_COMP_DEV);
        getItemMap(C_COMP_DEV).put(CHART_COMP_RATIO, new Short("12"));
        getItemMap(C_COMP_DEV).put(GRAPH_DIST, new Short("1"));
        getItemMap(C_COMP_DEV).put(GRAPH_BUILDUP, new Short("1"));
        getItemMap(C_COMP_DEV).put(GRAPH_COMP_RATIO, new Short("1"));
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD START
//        getItemMap(C_COMP_DEV).put(OPTION_CHECK_BOX, new Short("1"));
//// 2019/09/30 QQ)Tanioka プロファイル追加対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
        getItemMap(C_COMP_DEV).put(CLASS_ORDER, new Short("1"));
        setStamp(C_COMP_DEV);
        // クラス比較（設問別成績）
        setPrintOutTarget(C_COMP_QUE);
        setCourse(C_COMP_QUE);
        getItemMap(C_COMP_QUE).put(CHART, new Short("1"));
        getItemMap(C_COMP_QUE).put(GRAPH, new Short("0"));
        getItemMap(C_COMP_QUE).put(CLASS_ORDER, new Short("1"));
        setStamp(C_COMP_QUE);
        // クラス比較（志望大学別評価人数）
        setPrintOutTarget(C_COMP_UNIV);
        getItemMap(C_COMP_UNIV).put(CHART, new Short("1"));
        getItemMap(C_COMP_UNIV).put(UNIV_ORDER, new Short("1"));
        getItemMap(C_COMP_UNIV).put(UNIV_COUNTING_DIV, new Short("3"));
        getItemMap(C_COMP_UNIV).put(CLASS_ORDER, new Short("1"));
        getItemMap(C_COMP_UNIV).put(PREV_DISP, new String[]{"1", "2"});
        getItemMap(C_COMP_UNIV).put(UNIV_RATING, new String[]{"1", "2", "3"});
        setStamp(C_COMP_UNIV);

        //4 		個人成績分析
        //4.1 		個人成績分析共通
        //4.1.0 	選択中の生徒（分析モード）・個人成績分析共通-印刷対象生徒（判定対象生徒)
        getItemMap(I_COMMON).put(STUDENT_SELECTED, new String(""));
        getItemMap(I_COMMON).put(PRINT_STUDENT, new Short("2"));
        //4.2 		成績分析
        //4.2.1 	成績分析共通
        //4.2.1.0	印刷対象
        getItemMap(I_SCORE_COMMON).put(PRINT_TARGET, new String("0,0,0,0"));
        //4.2.2		個人成績表
        //表示する学年・模試の種類・セキュリティスタンプ
        getItemMap(I_SCORE_SHEET).put(STUDENT_DISP_GRADE, new Short("1"));
        getItemMap(I_SCORE_SHEET).put(EXAM_TYPE, new Short("1"));
        getItemMap(I_SCORE_SHEET).put(PRINT_STAMP, new Short("1"));
        //4.2.3		バランスチャート
        //4.2.3.0	偏差値範囲・１教科複数受験・セキュリティスタンプ
        getItemMap(I_SCORE_BC).put(DEV_RANGE, new String("30.0,70.0"));
        getItemMap(I_SCORE_BC).put(IND_MULTI_EXAM, new Short("1"));
        getItemMap(I_SCORE_BC).put(PRINT_STAMP, new Short("1"));
        //4.2.4		バランスチャート（合格者平均）
        //4.2.4.0	偏差値範囲・１教科複数受験・セキュリティスタンプ
        getItemMap(I_SCORE_BC_AVG).put(DEV_RANGE, new String("30.0,70.0"));
        getItemMap(I_SCORE_BC_AVG).put(IND_MULTI_EXAM, new Short("1"));
        //getItemMap(I_SCORE_BC_AVG).put(PRINT_STAMP, new Short("1"));
        //4.2.5		成績推移グラフ
        //4.2.5.0	表示する学年・模試の種類・偏差値範囲・１教科複数受験・セキュリティスタンプ
        getItemMap(I_SCORE_TRANS).put(STUDENT_DISP_GRADE, new Short("1"));
        getItemMap(I_SCORE_TRANS).put(EXAM_TYPE, new Short("1"));
        getItemMap(I_SCORE_TRANS).put(DEV_RANGE, new String("30.0,70.0"));
        getItemMap(I_SCORE_TRANS).put(IND_MULTI_EXAM, new Short("1"));
        //getItemMap(I_SCORE_TRANS).put(PRINT_STAMP, new Short("1"));
        //4.2.6		科目別成績推移グラフ
        //4.2.6.0	表示する学年・偏差値範囲・セキュリティスタンプ
        getItemMap(I_SCORE_COURSE_TRANS).put(STUDENT_DISP_GRADE, new Short("1"));
        getItemMap(I_SCORE_COURSE_TRANS).put(DEV_RANGE, new String("30.0,70.0"));
        getItemMap(I_SCORE_COURSE_TRANS).put(PRINT_STAMP, new Short("1"));
        //4.2.7		設問別成績
        //4.2.7.0	セキュリティスタンプ
        getItemMap(I_SCORE_QUE).put(PRINT_STAMP, new Short("1"));
        //4.3 判定
        //4.3.1 条件保存されていない生徒の処理
        getItemMap(I_UNIV_JUDGE).put(IND_STUDENT_USAGE, new Short("1"));
        //4.3.2 印刷対象生徒（判定対象生徒）・セキュリティスタンプ
        getItemMap(I_UNIV_RESULTS).put(PRINT_STUDENT, new Short("2"));
        getItemMap(I_UNIV_RESULTS).put(PRINT_STAMP, new Short("1"));
        //4.4		個人手帳
        //4.4.1  	セキュリティスタンプ
        getItemMap(I_PERSONAL_NOTE).put(PRINT_STAMP, new Short("1"));
        getItemMap(I_PERSONAL_NOTE).put(TEXT_OUT_FORMAT, new Short("3"));
        //4.4		簡単印刷
        //4.4.1  	成績分析面談シート･面談用帳票 ･個人成績用･受験用資料･セキュリティスタンプ･
        getItemMap(I_EASY_PRINT).put(IND_INTERVIEW_SHEET, new Short("1"));
        getItemMap(I_EASY_PRINT).put(IND_INTERVIEW_FORMS, new String("0,0,0"));
        getItemMap(I_EASY_PRINT).put(IND_REPORT, new Short("0"));//
        getItemMap(I_EASY_PRINT).put(IND_EXAM_DOC, new Short("0"));//
        getItemMap(I_EASY_PRINT).put(PRINT_STAMP, new Short("1"));//

        //5.5		テキスト出力
        getItemMap(T_COMMON).put(TEXT_FILE_FORMAT, new Short("1"));

        setPrintOutTarget(T_COUNTING_SCORE);
        getItemMap(T_COUNTING_SCORE).put(TEXT_OUT_TARGET, new Short("1"));
        getItemMap(T_COUNTING_SCORE).put(TEXT_OUT_ITEM, getOutTargetValue(17));
        getItemMap(T_COUNTING_SCORE).put(SCHOOL_COMP_OTHER, new Short("0"));
        getItemMap(T_COUNTING_SCORE).put(CLASS_COMP, new Short("0"));

        setPrintOutTarget(T_COUNTING_DEV);
        getItemMap(T_COUNTING_DEV).put(TEXT_OUT_TARGET, new Short("1"));
        getItemMap(T_COUNTING_DEV).put(TEXT_OUT_ITEM, getOutTargetValue(53));
        getItemMap(T_COUNTING_DEV).put(SCHOOL_COMP_OTHER, new Short("0"));
        getItemMap(T_COUNTING_SCORE).put(CLASS_COMP, new Short("0"));

        setPrintOutTarget(T_COUNTING_QUE);
        getItemMap(T_COUNTING_QUE).put(TEXT_OUT_TARGET, new Short("1"));
        getItemMap(T_COUNTING_QUE).put(TEXT_OUT_ITEM, getOutTargetValue(18));
        getItemMap(T_COUNTING_QUE).put(SCHOOL_COMP_OTHER, new Short("0"));
        getItemMap(T_COUNTING_SCORE).put(CLASS_COMP, new Short("0"));

        setPrintOutTarget(T_COUNTING_UNIV);
        getItemMap(T_COUNTING_UNIV).put(TEXT_OUT_TARGET, new Short("1"));
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD START
//        //2019/08/08 QQ)nagai 共通テスト対応 UPD START
//        //getItemMap(T_COUNTING_UNIV).put(TEXT_OUT_ITEM, getOutTargetValue(28));
//        getItemMap(T_COUNTING_UNIV).put(TEXT_OUT_ITEM, getOutTargetValue(33));
//        //2019/08/08 QQ)nagai 共通テスト対応 UPD END
        getItemMap(T_COUNTING_UNIV).put(TEXT_OUT_ITEM, getOutTargetValue(28));
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD END
        getItemMap(T_COUNTING_UNIV).put(SCHOOL_COMP_OTHER, new Short("0"));
        getItemMap(T_COUNTING_SCORE).put(CLASS_COMP, new Short("0"));

        setPrintOutTarget(T_IND_QUE);
        getItemMap(T_IND_QUE).put(TEXT_OUT_TARGET, new Short("1"));
        getItemMap(T_IND_QUE).put(TEXT_OUT_ITEM, getOutTargetValue(72));

        setPrintOutTarget(T_IND_EXAM2);
        getItemMap(T_IND_EXAM2).put(TEXT_OUT_TARGET, new Short("1"));
        // 2020/03/04 QQ)Ooseto 共通テスト対応(不具合修正) UPD START
        // 2019/09/17 QQ)Ooseto 共通テスト対応 UPD START
        getItemMap(T_IND_EXAM2).put(TEXT_OUT_ITEM, getOutTargetValue(247));
        //getItemMap(T_IND_EXAM2).put(TEXT_OUT_ITEM, getOutTargetValue(348));
        // 2019/09/17 QQ)Ooseto 共通テスト対応 UPD END
        // 2020/03/04 QQ)Ooseto 共通テスト対応(不具合修正) UPD END

// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD START
        setPrintOutTarget(T_IND_DESCEXAM);
        getItemMap(T_IND_DESCEXAM).put(TEXT_OUT_TARGET, new Short("1"));
        getItemMap(T_IND_DESCEXAM).put(TEXT_OUT_ITEM, getOutTargetValue(40));
// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD END
    }

    //	出力対象のプロファイル初期値を生成
    public static String getOutTargetValue(int size){
        String str ="";
        String index[] = new String[size];
        for(int i=0; i<size; i++){	index[i] = String.valueOf(i+1);	}
        Set outTarget = CollectionUtil.array2Set(index);
        Iterator it = outTarget.iterator();
        int count = 0;
        while(it.hasNext()){
            str += it.next();
            if(count!=size) str+=",";
        }
        return str;
    }

}

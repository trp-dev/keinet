/*
 * 作成日: 2004/08/19
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.login;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * 部門データ
 * 
 * <2010年度マーク高２模試対応>
 * 2011.01.24	Tomohisa YAMADA
 * 				アップロード機能
 * 
 * @author kawai
 */
public class SectorData implements Serializable {

	// 部門コード
	private String sectorCD = null;
	// 校舎営業部判別コード
	private String hsBusiDivCD = null;
	// 部門分類コード
	private String sectorSortingCD = null;
	// 部門名称
	private String sectorName = null;
	
	//ダウンロード可能高校数
	private int downloadableCount;

	/**
	 * コンストラクタ
	 */
	public SectorData() {
	}

	/**
	 * コンストラクタ
	 */
	public SectorData(final String pSectorCD,
			final String pHsBusiDivCD) {
		sectorCD = pSectorCD;
		hsBusiDivCD = pHsBusiDivCD;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(final Object obj) {
		
		if (!(obj instanceof SectorData)) {
			return false;
		}

		SectorData data = (SectorData) obj;
		
		return new EqualsBuilder()
				.append(getSectorCD(), data.getSectorCD())
				.append(getHsBusiDivCD(), data.getHsBusiDivCD())
				.isEquals();
	}

	/**
	 * @return
	 */
	public String getHsBusiDivCD() {
		return hsBusiDivCD;
	}

	/**
	 * @return
	 */
	public String getSectorCD() {
		return sectorCD;
	}

	/**
	 * @return
	 */
	public String getSectorSortingCD() {
		return sectorSortingCD;
	}

	/**
	 * @param string
	 */
	public void setHsBusiDivCD(String string) {
		hsBusiDivCD = string;
	}

	/**
	 * @param string
	 */
	public void setSectorCD(String string) {
		sectorCD = string;
	}

	/**
	 * @param string
	 */
	public void setSectorSortingCD(String string) {
		sectorSortingCD = string;
	}

	/**
	 * @return
	 */
	public String getSectorName() {
		return sectorName;
	}

	/**
	 * @param string
	 */
	public void setSectorName(String string) {
		sectorName = string;
	}

	/**
	 * ダウンロード可能高校数を返す。
	 * @return
	 */
	public int getDownloadableCount() {
		return downloadableCount;
	}

	/**
	 * ダウンロード可能高校数を設定する。
	 * @param downloadableCount
	 */
	public void setDownloadableCount(int downloadableCount) {
		this.downloadableCount = downloadableCount;
	}

}

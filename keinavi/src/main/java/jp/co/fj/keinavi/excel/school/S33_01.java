/**
 * 校内成績分析−クラス比較　設問別成績（クラス比較）
 * 	Excelファイル編集
 * 作成日: 2004/08/09
 * @author	T.Sakai
 * 
 * 2009.10.07   Fujito URAKAWA - Totec
 *              「全体成績」追加対応
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S33ClassListBean;
import jp.co.fj.keinavi.excel.data.school.S33Item;
import jp.co.fj.keinavi.excel.data.school.S33ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S33_01 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
	private boolean bolBookCngFlg = true;			//改ファイルフラグ
	private boolean bolSheetCngFlg = true;			//改シートフラグ
//add end
	
	final private String masterfile0 = "S33_01";
	final private String masterfile1 = "S33_01";
	private String masterfile = "";
	final private int intMaxSheetSr = 50;	//MAXシート数の値を入れる

/*
 * 	Excel編集メイン
 * 		S33Item s33Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
	public int s33_01EditExcel(S33Item s33Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		int		intMaxSheetIndex	= 0;
		int		intFileIndex	= 1;
		
		//テンプレートの決定
		if (s33Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S33ListBean s33ListBean = new S33ListBean();

		try {
			
			// データセット
			ArrayList s33List = s33Item.getS33List();
			Iterator itr = s33List.iterator();
			int row = 0;				//行
			int col = 1;				//列
			int classCnt = 0;			//クラスカウンタ
			int setsumonCnt = 0;		//設問カウンタ
			int hyouCnt = 0;			//表カウンタ（0…上段、1…下段）
			String kmk = "";			//型・科目チェック用
			int maxClass = 0;			//MAXクラス数
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
			int		sheetListIndex	= 0;		//シートカウンター
			int		houjiNum		= 41;		//1シートに表示できるクラス数(自校＋クラス)
			int		sheetRowCnt		= 0;		//クラス用改シート格納用カウンタ
			ArrayList	workbookList	= new ArrayList();
			ArrayList	workSheetList	= new ArrayList();
//add end
			
			while( itr.hasNext() ) {
				s33ListBean = (S33ListBean)itr.next();
				
				// 基本ファイルを読込む
				S33ClassListBean s33ClassListBean = new S33ClassListBean();
				
				// クラスデータセット
				ArrayList s33ClassList = s33ListBean.getS33ClassList();
				Iterator itrClass = s33ClassList.iterator();
				
				maxClass = 0;
				maxClass = s33ClassList.size();
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
				// クラス表示に必要なシート数の計算
				sheetRowCnt = (maxClass-1)/(houjiNum-1);
				if((maxClass-1)%(houjiNum-1)!=0){
					sheetRowCnt++;
				}
				if (sheetRowCnt==0) {
					sheetRowCnt++;
				}
				
//add 2004.10.22 設問名Null時の処理回避 → 2004.12.21 設問がない科目も出力する
//				if ( !cm.toString(s33ListBean.getStrSetsuMei1()).equals("") ) {
//add end
					//クラス数が20以下
					if ((maxClass - 1)<=(houjiNum-1)/2) {
						//科目、設問が変わる時のチェック
//update 2004.10.29 T.Sakai 改表、改シート、改ファイル条件変更 → 設問名なしの場合あり 2005.05.25
//						if ( !cm.toString(setsumon).equals(cm.toString(s33ListBean.getStrSetsuMei1())) 
//							&& !cm.toString(kmk).equals(cm.toString(s33ListBean.getStrKmkCd())) 
//							&& !cm.toString(s33ListBean.getStrSetsuMei1()).equals("") ) {
//						if ( !cm.toString(kmk).equals(cm.toString(s33ListBean.getStrKmkCd())) 
//							&& !cm.toString(s33ListBean.getStrSetsuMei1()).equals("") ) {
						if ( !cm.toString(kmk).equals(cm.toString(s33ListBean.getStrKmkCd())) ){
//update end
							col = 1;
							setsumonCnt = 0;
							if (hyouCnt==0) {
								hyouCnt = 1;
							} else if (hyouCnt==1) {
								hyouCnt = 0;
								bolSheetCngFlg = true;
								if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
									bolBookCngFlg = true;
								}
							}
						}
						//設問数が11のときの改表、改シート、改ファイル判断
						if (setsumonCnt==11) {
							col = 1;
							setsumonCnt = 0;
							if (hyouCnt==0) {
								hyouCnt = 1;
							} else if (hyouCnt==1) {
								hyouCnt = 0;
								bolSheetCngFlg = true;
								if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
									bolBookCngFlg = true;
								}
							}
						} 

						//2004.12.21 設問がない科目の処理 → 削除 2005.05.25
//						if ( cm.toString(s33ListBean.getStrSetsuMei1()).equals("") ) {
//							col = 1;
//							if (hyouCnt==0) {
//								hyouCnt = 1;
//							} else if (hyouCnt==1) {
//								hyouCnt = 0;
//								bolSheetCngFlg = true;
//							}
//						}

					} else {
						//科目が変わる時のチェック
						if (!cm.toString(kmk).equals(cm.toString(s33ListBean.getStrKmkCd()))) {
							col = 1;
							setsumonCnt = 0;
							bolSheetCngFlg = true;
							if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
								bolBookCngFlg = true;
							}
						}
						//設問数が11のとき改シート
						if (setsumonCnt==11) {
							col = 1;
							setsumonCnt = 0;
							bolSheetCngFlg = true;
							if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
								bolBookCngFlg = true;
							}
						}
						hyouCnt = 0;
					}
//add end
					int ninzu = 0;
					float tokuritsu = 0;
					
					while ( itrClass.hasNext() ){
						s33ClassListBean = (S33ClassListBean)itrClass.next();
						
						if( bolBookCngFlg == true ){
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
							if (maxClass<=houjiNum){
								if(intMaxSheetIndex >= intMaxSheetSr){
									// Excelファイル保存
									boolean bolRet = false;
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intFileIndex, masterfile, intMaxSheetIndex);
									intFileIndex++;
									if( bolRet == false ){
										return errfwrite;					
									}
								}
							}
//add end
							//マスタExcel読み込み
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook==null ){
								return errfread;
							}
							intMaxSheetIndex=0;
							bolBookCngFlg = false;
							workbookList.add(workbook);
						}
						//クラス数が41以上または11設問ごとに改シート
						if ( bolSheetCngFlg ) {
							if (maxClass<=(houjiNum-1)/2) {
								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);
								intMaxSheetIndex++;
							}else{
								// データセットするシートの選択
								if (setsumonCnt==0) {
									// シートテンプレートのコピー
									workSheet = workbook.cloneSheet(0);
									workSheetList.add(sheetListIndex, workSheet);
									intMaxSheetIndex++;
									sheetListIndex++;
								} else {
									//次列用シートの呼び出し
									workSheet = (HSSFSheet)workSheetList.get(sheetListIndex-sheetRowCnt);
									sheetRowCnt--;
								}
							}
							// ヘッダ右側に帳票作成日時を表示する
							cm.setHeader(workbook, workSheet);
							
							// セキュリティスタンプセット
							String secuFlg = cm.setSecurity( workbook, workSheet, s33Item.getIntSecuFlg() ,31 ,33 );
							workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
							workCell.setCellValue(secuFlg);
							
							// 注釈セット
							if (s33Item.getIntShubetsuFlg() == 1){
							} else{
								workCell = cm.setCell( workSheet, workRow, workCell, 4, 33 );
								workCell.setCellValue( "※校内の平均得点率を下回るクラスの平均得点率に\"*\"を表示しています。" );
							}

							// 学校名セット
							workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
							workCell.setCellValue( "学校名　：" + cm.toString(s33Item.getStrGakkomei()) );
		
							// 模試月取得
							String moshi =cm.setTaisyouMoshi( s33Item.getStrMshDate() );
							// 対象模試セット
							workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
							workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s33Item.getStrMshmei()) + moshi);
							
							hyouCnt = 0;
							bolSheetCngFlg = false;
						}
						if (classCnt==0) {
							if ( hyouCnt==0 ) {
								row = 4;
							}
							if ( hyouCnt==1 ) {
								row = 32;
							}
							if ( !cm.toString(kmk).equals(cm.toString(s33ListBean.getStrKmkCd())) || hyouCnt==0 ) {
								// 型・科目名＋配点セット
								String haiten = "";
								if ( !cm.toString(s33ListBean.getStrHaitenKmk()).equals("") ) {
									haiten = "（" + s33ListBean.getStrHaitenKmk() + "）";
								}
								workCell = cm.setCell( workSheet, workRow, workCell, row, 0 );
								workCell.setCellValue( "型・科目：" + cm.toString(s33ListBean.getStrKmkmei()) + haiten );
							}
						
							if ( hyouCnt==0 ) {
								row = 5;
							}
							if ( hyouCnt==1 ) {
								row = 33;
							}
//delete 2004.10.29 T.Sakai 設問セット条件削除
//							if ( !cm.toString(setsumon).equals(cm.toString(s33ListBean.getStrSetsuMei1())) ) {
//delete end
								// 設問番号セット
								workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
								workCell.setCellValue( s33ListBean.getStrSetsuNo() );
								// 設問内容セット
								workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
								workCell.setCellValue( s33ListBean.getStrSetsuMei1() );
								// 設問配点セット
								workCell = cm.setCell( workSheet, workRow, workCell, row, col );
								if (!cm.toString(s33ListBean.getStrSetsuHaiten()).equals("") ){
									workCell.setCellValue( "（" + s33ListBean.getStrSetsuHaiten() + "）" );
								}
//delete 2004.10.29 T.Sakai 設問セット条件削除
//							}
//delete end
							if ( hyouCnt==0 ) {
								row = 9;
							}
							if ( hyouCnt==1 ) {
								row = 37;
							}
							if (col==1) {
								// 学校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, row, col-1 );
								workCell.setCellValue( s33Item.getStrGakkomei() );
							}
							// 受験人数セット
							ninzu = s33ClassListBean.getIntNinzu();
							workCell = cm.setCell( workSheet, workRow, workCell, row, col );
							if ( s33ClassListBean.getIntNinzu() != -999 ) {
								workCell.setCellValue( s33ClassListBean.getIntNinzu() );
							}
							// 得点率セット
							tokuritsu = s33ClassListBean.getFloTokuritsu();
							workCell = cm.setCell( workSheet, workRow, workCell, row++, col+2 );
							if ( s33ClassListBean.getFloTokuritsu() != -999.0 ) {
								workCell.setCellValue( s33ClassListBean.getFloTokuritsu() );
							}
						} else {
							if((classCnt-1)%((houjiNum-1)/2)==0 && classCnt!=1){
								if (maxClass<=(houjiNum-1)/2) {
									if((classCnt-1)%((houjiNum-1)/2)==0){
										row = 4;
									}else{
										row = 32;
									}
									if ( !cm.toString(kmk).equals(cm.toString(s33ListBean.getStrKmkCd())) || hyouCnt==0 ) {
										// 型・科目名＋配点セット
										String haiten = "";
										if ( !cm.toString(s33ListBean.getStrHaitenKmk()).equals("") ) {
											haiten = "（" + s33ListBean.getStrHaitenKmk() + "）";
										}
										workCell = cm.setCell( workSheet, workRow, workCell, row, 0 );
										workCell.setCellValue( "型・科目：" + cm.toString(s33ListBean.getStrKmkmei()) + haiten );
									}
								}else{
									if((classCnt-1)%(houjiNum-1)==0){
										row = 4;
										if ( !cm.toString(kmk).equals(cm.toString(s33ListBean.getStrKmkCd())) || hyouCnt==0 ) {
											// 型・科目名＋配点セット
											String haiten = "";
											if ( !cm.toString(s33ListBean.getStrHaitenKmk()).equals("") ) {
												haiten = "（" + s33ListBean.getStrHaitenKmk() + "）";
											}
											workCell = cm.setCell( workSheet, workRow, workCell, row, 0 );
											workCell.setCellValue( "型・科目：" + cm.toString(s33ListBean.getStrKmkmei()) + haiten );
										}
									}
								}
	
								
								if (maxClass<=(houjiNum-1)/2) {
									if((classCnt-1)%((houjiNum-1)/2)==0){
										row = 5;
									}else{
										row = 33;
									}
								}else{
									if((classCnt-1)%(houjiNum-1)==0){
										row = 5;
									}else{
										row = 33;
									}
								}
								
//delete 2004.10.29 T.Sakai 設問セット条件削除
//								if ( setsumonCnt==0 || !cm.toString(setsumon).equals(cm.toString(s33ListBean.getStrSetsuMei1())) ) {
//delete end
									// 設問番号セット
									workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
									workCell.setCellValue( s33ListBean.getStrSetsuNo() );
									// 設問内容セット
									workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
									workCell.setCellValue( s33ListBean.getStrSetsuMei1() );
									// 設問配点セット
									workCell = cm.setCell( workSheet, workRow, workCell, row, col );
									if (!cm.toString(s33ListBean.getStrSetsuHaiten()).equals("") ){
										workCell.setCellValue( "（" + s33ListBean.getStrSetsuHaiten() + "）" );
									}
//delete 2004.10.29 T.Sakai 設問セット条件削除
//								}
//delete end
								if (maxClass<=(houjiNum-1)/2) {
									if((classCnt-1)%((houjiNum-1)/2)==0){
										row = 9;
									}else{
										row = 37;
									}
								}else{
									if((classCnt-1)%(houjiNum-1)==0){
										row = 9;
									}else{
										row = 37;
									}
								}
								if (col==1) {
									// 学校名セット
									workCell = cm.setCell( workSheet, workRow, workCell, row, col-1 );
									workCell.setCellValue( s33Item.getStrGakkomei() );
								}
								// 人数セット
								workCell = cm.setCell( workSheet, workRow, workCell, row, col );
								if ( ninzu != -999 ) {
									workCell.setCellValue( ninzu );
								}
								// 得点率セット
								workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
								if ( tokuritsu != -999.0 ) {
									workCell.setCellValue( tokuritsu );
								}
								row++;
								hyouCnt = 1;
							}
							if (col==1) {
								// 学年+クラス名セット
								workCell = cm.setCell( workSheet, workRow, workCell, row, col-1 );
								workCell.setCellValue( cm.toString(s33ClassListBean.getStrGrade())+"年 "+cm.toString(s33ClassListBean.getStrClass())+"クラス" );
							}
							// 人数セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col );
							if ( s33ClassListBean.getIntNinzu() != -999 ) {
								workCell.setCellValue( s33ClassListBean.getIntNinzu() );
							}
							// *セット
							// 得点用のときは処理しない
							if (s33Item.getIntShubetsuFlg() == 1){
							} else{
								if ( s33ClassListBean.getFloTokuritsu() != -999.0 ) {
									if ( tokuritsu != -999.0 ) {
										if ( s33ClassListBean.getFloTokuritsu() < tokuritsu ) {
											workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
											workCell.setCellValue("*");
										}
									}
								}
							}
							// 得点率セット
							workCell = cm.setCell( workSheet, workRow, workCell, row++, col+2 );
							if ( s33ClassListBean.getFloTokuritsu() != -999.0 ) {
								workCell.setCellValue( s33ClassListBean.getFloTokuritsu() );
							}
						}
						classCnt++;
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
						if (maxClass>houjiNum){
							if((classCnt-1)%(houjiNum-1)==0 && classCnt!=1){
								//クラスが改シート、改ファイルでまたぐとき
								if(itrClass.hasNext()){
									if(setsumonCnt==0){
										if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
											bolBookCngFlg = true;
										}
									}
									bolSheetCngFlg = true;
								}
							}
						}
//add end
					}
//add 2004.10.22 設問名Null時の処理回避 → 2004.12.21 設問がない科目も出力する
//				}
//add end
				
				classCnt = 0;
				setsumonCnt++;
				col = col + 3;
				kmk = s33ListBean.getStrKmkCd();
				
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
				if (maxClass>houjiNum){
					if ( setsumonCnt==11 ) {
						col = 1;
						setsumonCnt = 0;
					}
					bolSheetCngFlg = true;
					if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
						bolBookCngFlg = true;
					}
					// workbookListにWorkBookが２つ以上格納されている時は出力処理を実行
					int listSize = workbookList.size();
					if(listSize>1){
						if(setsumonCnt == 0){
							workbook = (HSSFWorkbook)workbookList.get(0);
			
							// Excelファイル保存
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intFileIndex, masterfile, intMaxSheetSr);
							intFileIndex++;
							if( bolRet == false ){
								return errfwrite;
							}
		
							// ファイル出力したデータは削除
							workbookList.remove(0);
		
							// WorkBook・変数を書き込んでる途中のものに戻す
							workbook = (HSSFWorkbook)workbookList.get(0);
						}
					}
				}
//add end
			}
			
			// Excelファイル保存
			boolean bolRet = false;
			if(intFileIndex==1){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intFileIndex, masterfile, intMaxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("S33_01","データセットエラー",e.toString());
			return errfdata;
		}
		
		return noerror;
	}

}
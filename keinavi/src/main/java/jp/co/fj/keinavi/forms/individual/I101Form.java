/*
 * 作成日: 2004/07/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * @author Administrator
 *
 * 2005.4.26 	K.Kondo 	[1]画面スクロール座標の追加
 */
public class I101Form extends ActionForm{

	private String targetPersonId;
	private String targetGrade;
	private String targetExamType;
	
	//個人成績分析共通-印刷対象生徒（判定対象生徒)
	private String printStudent;
	//成績分析共通：印刷対象
	private String[] printTarget;
	//セキュリティスタンプ
	private String printStamp;
	//印刷ボタン押下時パラメータ
	private String printStatus;
	
	private String scrollX;//[1] add サブミット前、表示座標Ｘ
	private String scrollY;//[1] add サブミット前、表示座標Ｙ

	// 対象模試年度
	private String targetExamYear;
	// 対象模試コード
	private String targetExamCode;
	
	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	/**
	 * @return
	 */
	public String getTargetGrade() {
		return targetGrade;
	}

	/**
	 * @param string
	 */
	public void setTargetGrade(String string) {
		targetGrade = string;
	}

	/**
	 * @return
	 */
	public String getTargetExamType() {
		return targetExamType;
	}

	/**
	 * @param string
	 */
	public void setTargetExamType(String string) {
		targetExamType = string;
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @return
	 */
	public String[] getPrintTarget() {
		return printTarget;
	}

	/**
	 * @param strings
	 */
	public void setPrintTarget(String[] strings) {
		printTarget = strings;
	}

	/**
	 * @return
	 */
	public String getPrintStamp() {
		return printStamp;
	}

	/**
	 * @param string
	 */
	public void setPrintStamp(String string) {
		printStamp = string;
	}

	/**
	 * @return
	 */
	public String getPrintStudent() {
		return printStudent;
	}

	/**
	 * @param string
	 */
	public void setPrintStudent(String string) {
		printStudent = string;
	}

	/**
	 * @return
	 */
	public String getPrintStatus() {
		return printStatus;
	}

	/**
	 * @param string
	 */
	public void setPrintStatus(String string) {
		printStatus = string;
	}

	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[1] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[1] add
	}

	/**
	 * @return targetExamCode を戻します。
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * @param pTargetExamCode 設定する targetExamCode。
	 */
	public void setTargetExamCode(String pTargetExamCode) {
		targetExamCode = pTargetExamCode;
	}

	/**
	 * @return targetExamYear を戻します。
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @param pTargetExamYear 設定する targetExamYear。
	 */
	public void setTargetExamYear(String pTargetExamYear) {
		targetExamYear = pTargetExamYear;
	}

}

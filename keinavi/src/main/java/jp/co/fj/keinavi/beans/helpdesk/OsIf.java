/*
 * 作成日: 2004/10/02
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.helpdesk;

import java.io.*;
import java.util.*;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class OsIf {

	Vector vecOsOutput;

	// Constructor

	public OsIf() {
	}

	/**
	 * Store InputStream into String in Vector
	 *
	 * @param    String[] sCmd 
	 * @return  
	 *
	 */
	private Vector StreamToVector(InputStream is) throws Exception {
		Vector tb = new Vector();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String s;
		while ((s = br.readLine()) != null) {
			tb.add(s);
		}
		return tb;
	}

	/**
	 * Execute Command and get Standard Output
	 *
	 * @param    String[] sCmd 
	 * @return  
	 *
	 */
	public int exec (String[] sCmd) throws Exception {
		System.out.println("Exec:"+sCmd);
		Runtime rt = Runtime.getRuntime();
		Process pr = rt.exec(sCmd);
		InputStream is = pr.getInputStream();
		vecOsOutput = StreamToVector(is);
		try {
		  pr.waitFor();
		}
		catch (Exception ex) {
		  System.out.println("interrupted!!!");
		  throw ex;
		}
		int value = pr.exitValue();
		System.out.print("exec() の戻り値=" + value);
		return value;
	}

	/**
	 * for Debugging
	 *
	 * @param   
	 * @return  
	 *
	 */
	public void debugPrint() {
		for(int i = 0; i < vecOsOutput.size(); i++ ) {
		  String s = (String)vecOsOutput.elementAt(i);
		  System.out.println(i + "\t" + s);
		}
	}


	/**
	 * main() to try me
	 *
	 * @param   
	 * @return  
	 *
	 */
	//public static void main(String argv[]) {
	//    OsIf o = new OsIf();
	//    String[] ss = new String[4];
	//    try {
	//        ss[0] = new String("namazu");
	//        ss[1] = new String("-l");
	//        ss[2] = new String("-a");
	//        ss[3] = new String("oracle and index");
	//        o.exec(ss);
	//        o.debugPrint();
	//        System.exit(0);
	//    }
	//    catch (Exception ex) {
	//        System.exit(1);
	//    }
	//}

}


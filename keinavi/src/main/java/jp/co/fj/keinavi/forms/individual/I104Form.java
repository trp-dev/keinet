package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * バランスチャート(合格者平均)用フォーム
 * @author kondo
 *
 * 2005.4.26 	K.Kondo 	[1]画面スクロール座標の追加
 */
public class I104Form extends ActionForm {

	private String mode;				//モード（分析、面談）

	private String targetPersonId;		//個人ＩＤ
	private String targetExamCode;		//模試コード
	private String targetExamYear;		//模試年度

	private String targetCandidateRank1;	//大学志望順位コード１
	private String targetCandidateRank2;	//大学志望順位コード２
	private String targetCandidateRank3;	//大学志望順位コード３
	private String lowerRange;				//偏差値範囲（下限）
	private String upperRange;				//偏差値範囲（上限）
	private String avgChoice;				//偏差値計算方法（平均、良い方）
	private boolean markexamflg;			//マーク模試だったらtrue
	private boolean dispflg;			//マーク模試だったらtrue
	//個人成績分析共通-印刷対象生徒（判定対象生徒)
	private String printStudent;
	
	//成績分析共通：印刷対象
	private String[] printTarget;
	
	//セキュリティスタンプ
	private String printStamp;
	
	//印刷ボタン押下時パラメータ
	private String printStatus;
	
	private String scrollX;//[1] add サブミット前、表示座標Ｘ
	private String scrollY;//[1] add サブミット前、表示座標Ｙ

	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}
	
	/**
	 * @return
	 */
	public String getAvgChoice() {
		return avgChoice;
	}

	/**
	 * @return
	 */
	public String getLowerRange() {
		return lowerRange;
	}

	/**
	 * @return
	 */
	public String getUpperRange() {
		return upperRange;
	}

	/**
	 * @param string
	 */
	public void setAvgChoice(String string) {
		avgChoice = string;
	}

	/**
	 * @param string
	 */
	public void setLowerRange(String string) {
		lowerRange = string;
	}

	/**
	 * @param string
	 */
	public void setUpperRange(String string) {
		upperRange = string;
	}

	/**
	 * @return
	 */
	public String getTargetCandidateRank1() {
		return targetCandidateRank1;
	}

	/**
	 * @return
	 */
	public String getTargetCandidateRank2() {
		return targetCandidateRank2;
	}

	/**
	 * @return
	 */
	public String getTargetCandidateRank3() {
		return targetCandidateRank3;
	}

	/**
	 * @param string
	 */
	public void setTargetCandidateRank1(String string) {
		targetCandidateRank1 = string;
	}

	/**
	 * @param string
	 */
	public void setTargetCandidateRank2(String string) {
		targetCandidateRank2 = string;
	}

	/**
	 * @param string
	 */
	public void setTargetCandidateRank3(String string) {
		targetCandidateRank3 = string;
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * @return
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param string
	 */
	public void setMode(String string) {
		mode = string;
	}

	/**
	 * @return
	 */
	public String[] getPrintTarget() {
		return printTarget;
	}

	/**
	 * @param strings
	 */
	public void setPrintTarget(String[] strings) {
		printTarget = strings;
	}

	/**
	 * @return
	 */
	public String getPrintStamp() {
		return printStamp;
	}

	/**
	 * @param string
	 */
	public void setPrintStamp(String string) {
		printStamp = string;
	}

	/**
	 * @return
	 */
	public String getPrintStudent() {
		return printStudent;
	}

	/**
	 * @param string
	 */
	public void setPrintStudent(String string) {
		printStudent = string;
	}

	/**
	 * @return
	 */
	public String getPrintStatus() {
		return printStatus;
	}

	/**
	 * @param string
	 */
	public void setPrintStatus(String string) {
		printStatus = string;
	}

	/**
	 * @return
	 */
	public boolean isMarkexamflg() {
		return markexamflg;
	}

	/**
	 * @param b
	 */
	public void setMarkexamflg(boolean b) {
		markexamflg = b;
	}

	/**
	 * @return
	 */
	public boolean isDispflg() {
		return dispflg;
	}

	/**
	 * @param b
	 */
	public void setDispflg(boolean b) {
		dispflg = b;
	}
	
	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[1] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[1] add
	}
	
}

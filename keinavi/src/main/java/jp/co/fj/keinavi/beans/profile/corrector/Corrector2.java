/*
 * 作成日: 2005/02/09
 */
package jp.co.fj.keinavi.beans.profile.corrector;

import java.util.Map;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.DefaultProfile;
import jp.co.fj.keinavi.data.profile.Profile;

/**
 * バージョン１→２
 * テキスト出力・模試別成績データ（新形式）の追加に対応
 * 
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */
public class Corrector2 implements ICorrector {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.profile.corrector.ICorrector#execute(jp.co.fj.keinavi.data.profile.Profile)
	 */
	public void execute(final Profile profile, final LoginSession login) {
		// テキスト出力のプロファイルがなければここまで
		if (!profile.getCategoryMap().containsKey(T_IND_EXAM)) return;
		
		Map item = profile.getItemMap(T_IND_EXAM2);
		
		// 一括出力対象
		item.put(PRINT_OUT_TARGET, new Short("1"));
		// CSV形式
		item.put(TEXT_OUT_TARGET, new Short("1"));
		// 出力対象
		item.put(TEXT_OUT_ITEM, DefaultProfile.getOutTargetValue(195));
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.profile.corrector.ICorrector#getVersion()
	 */
	public short getVersion() {
		return 2;
	}

}

/*
 * 作成日: 2004/08/11
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.com_set.ChargeClassData;
import jp.co.fj.keinavi.data.individual.ExaminationData;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class IUtilBean {
	
	/**
	 * プロファイルにある担当クラスのコレクションを
	 * 個人用のBeanに合うように配列に変える
	 * @param chargeClassDatas
	 * @return
	 */
	public static String[][] convert2ClassArray(List chargeClassDatas){
		String[][] classArray = new String[chargeClassDatas.size()][2];
		int count = 0;
		for (Iterator it=chargeClassDatas.iterator(); it.hasNext();) {
			ChargeClassData chargeClassData = (ChargeClassData)it.next();
			classArray[count][0] = Short.toString(chargeClassData.getGrade());
			classArray[count][1] = chargeClassData.getClassName();		
			count ++;
		}
		return classArray;
	}
	/**
	 * セッションから来る利用可能模試データを
	 * 個人用のBeanに合うようにExamDataから
	 * ExaminationDataに変える
	 * @param eSession
	 * @return
	 */
	public static List convert2ExaminationData(ExamSession eSession){
		List examinationDatas = new ArrayList();
		for (Iterator it=((Map)eSession.getExamMap()).entrySet().iterator(); it.hasNext(); ) {
			Map.Entry entry = (Map.Entry)it.next();
			List examDatas = (List)entry.getValue();
			for (Iterator its=examDatas.iterator(); its.hasNext();) {
				ExamData examData = (ExamData)its.next();
				ExaminationData examinationData = new ExaminationData();
				examinationData.setExamYear(examData.getExamYear());
				examinationData.setExamCd(examData.getExamCD());
				examinationDatas.add(examinationData);
			}
		}
		return examinationDatas;
	}
	/**
	 * カンマ区切り文字列をStringの配列で返す
	 * @param array
	 * @return
	 */
	public static String[] deconcatComma(String string) {
		StringTokenizer tokenizer = new StringTokenizer(string, ",");
		int tokenNum = tokenizer.countTokens();
		String[] array = new String[tokenNum];
		while(tokenizer.hasMoreTokens( )) { 
			array[tokenNum - tokenizer.countTokens()] = tokenizer.nextToken( );
		}
		return array;
	}
	/**
	 * 文字列配列をカンマで結合して返す
	 * @param array
	 * @return
	 */
	public static String concatComma(String[] array) {
		StringBuffer buff = new StringBuffer();
		for (int i=0; i<array.length; i++) {
			if (i > 0) buff.append(",");
			buff.append(array[i]);
		}
		return buff.toString();
	}
}

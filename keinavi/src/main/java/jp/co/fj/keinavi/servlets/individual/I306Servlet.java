package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.keinavi.beans.individual.ExamSearchBean;
import jp.co.fj.keinavi.beans.individual.I306Bean;
import jp.co.fj.keinavi.beans.individual.JudgeByUnivBean;
import jp.co.fj.keinavi.beans.individual.PlanUnivSearchBean2;
import jp.co.fj.keinavi.beans.individual.ScoreSearchBean;
import jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean;
import jp.co.fj.keinavi.beans.individual.judgement.JudgementListBean;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.PlanUnivData2;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I306Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.date.SpecialDateUtil;
import jp.co.fj.keinavi.util.individual.IPropsLoader;
import jp.co.fj.keinavi.util.individual.PlanUniv2Sorter;

/**
 * 
 * 2004.08.03   Tomohisa YAMADA - Totec
 *              新規作成
 *
 * 2005.02.23	Tomohisa YAMADA - Totec
 * 				判定結果をDetailPageBeanに一本化する
 * 
 * 2005.03.08	Tomohisa YAMADA - Totec
 * 				最新模試取得の時に「担当クラスの年度よりも若い」制限を加える
 * 
 * 2005.03.10 	Tomohisa YAMADA - Totec
 * 				4月〜3月の表示を5月〜4月に変更
 * 
 * 2005.04.07 	Tomohisa YAMADA - Totec
 * 				ScoreBean[35]に伴う修正
 * 
 * 2005.06.20	Tomohisa YAMADA - Totec
 * 				カレンダー横移動用ボタン追加に伴う修正
 * 
 * 2005.08.02 	Tomohisa YAMADA - Totec
 * 				マーク模試を受けていないときの、帳票エラー修正
 *  
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 *              マスタデータの持ち方変更
 *              
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 *
 */
public class I306Servlet extends IndividualServlet {
	
	//表示日数
	private final int DISP_DAY_LENGTH = Integer.parseInt(
			IPropsLoader.getInstance().getMessage("DISP_DAY_LENGTH"));//[5] add
	
	/**
	 * @see jp.co.fj.keinavi.servlets.individual.IndividualServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			 javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(final HttpServletRequest request,
		final HttpServletResponse response)
		throws ServletException, IOException {
			
		super.execute(request, response);
		
		// フォームデータを取得
		final I306Form form = (I306Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.individual.I306Form");
		
		// HTTPセッション
		final HttpSession session = request.getSession();
		// 個人共通
		final ICommonMap iCommonMap = getICommonMap(request);
		// プロファイル情報
		final Profile profile = getProfile(request);
		// 模試データ
		final ExamSession examSession = getExamSession(request);

		// セキュリティスタンプ
		final Short printStamp = (Short) profile.getItemMap(
				IProfileCategory.I_PERSONAL_NOTE).get(IProfileItem.PRINT_STAMP);
		// 印刷対象生徒
		final Short printStudent = (Short) profile.getItemMap(
				IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);
		// テキストフォーマット
		final Short textFormat = (Short) profile.getItemMap(
				IProfileCategory.I_PERSONAL_NOTE).get(IProfileItem.TEXT_OUT_FORMAT);
		
		// 転送:JSP
		if ("i306".equals(getForward(request))) {
			if(!"i306".equals(getBackward(request))
					|| !iCommonMap.getTargetPersonId().equals(form.getTargetPersonId())
					|| isChangedMode(request)){
				//対象生徒が変わった時
				if ("i306".equals(getBackward(request))) {
					//2.出力フォーマットをセット
					if (form.getTextFormat() != null) {
						if (form.getTextFormat().length == 2) {
							form.setTextFormat(CollectionUtil.splitComma("1,2"));
						} else {
							if (form.getTextFormat()[0] != null
									&& form.getTextFormat()[0].equals("1")) {
								form.setTextFormat(CollectionUtil.splitComma("1,0"));
							} else {
								form.setTextFormat(CollectionUtil.splitComma("0,2"));
							}
						}
					} else {
						form.setTextFormat(CollectionUtil.splitComma("0,0"));
					}
					
				//初回アクセス
				} else {
					//個人成績分析共通-印刷対象生保存をセット
					if(iCommonMap.isBunsekiMode()){
						form.setPrintStudent(printStudent.toString());
					}
					//1.セキュリティスタンプをセット
					form.setPrintStamp(printStamp.toString());
					
					//1.出力フォーマットをセット
					switch (textFormat.shortValue()) {
						case 1:
						form.setTextFormat(CollectionUtil.splitComma("1,0"));
						break;
						
						case 2:
						form.setTextFormat(CollectionUtil.splitComma("0,2"));
						break;
						
						case 3:
						form.setTextFormat(CollectionUtil.splitComma("1,2"));
						break;
						
						case 0:
						form.setTextFormat(CollectionUtil.splitComma("0,0"));
						break;
					}
				}	
				
			//２回目以降アクセス
			} else {	
				//2.出力フォーマットをセット
				if (form.getTextFormat() != null) {
					if (form.getTextFormat().length == 2) {
						form.setTextFormat(CollectionUtil.splitComma("1,2"));
					} else {
						if (form.getTextFormat()[0] != null
							&& form.getTextFormat()[0].equals("1")) {
							form.setTextFormat(CollectionUtil.splitComma("1,0"));
						} else {
							form.setTextFormat(CollectionUtil.splitComma("0,2"));
						}
					}
				} else {
					form.setTextFormat(CollectionUtil.splitComma("0,0"));
				}
			}
			
			Connection con = null;
			try {
				con = getConnectionPool(request);
				
				// ■ステップ１．模試成績の獲得
				//対象模試情報の取得
				ExamSearchBean esb = new ExamSearchBean();
				esb.setExamYear(iCommonMap.getTargetExamYear());
				esb.setExamCd(iCommonMap.getTargetExamCode());
				esb.setConnection("", con);
				esb.execute();
				//絶対に取れるはずだから、最初のを取得
				ExaminationData targetExam = (ExaminationData) esb.getRecordSet().get(0);
				
				//成績のリストを取得
				ScoreSearchBean ssb = new ScoreSearchBean();
				ssb.setLimitedYear(ProfileUtil.getChargeClassYear(profile));//[2] add
				ssb.setExamSession(examSession);
				ssb.setIndividualIds(new String[]{form.getTargetPersonId()});
				ssb.setConnection("", con);
				ssb.setTargetExam(targetExam);//[3] add
				ssb.execute();
				Score sb = (Score)ssb.getRecordSet().get(0);

				//■ ステップ２．実際の判定
				// 出力一覧の取得 
				PlanUnivSearchBean2 pusb2 = new PlanUnivSearchBean2();
				pusb2.setIndividualId(sb.getIndividualId());
				pusb2.setConnection("", con);
				pusb2.execute();
					
				// 判定の実行 
				String[][] univArray = new String[pusb2.getRecordSet().size()][3];
				int count = 0;
				Iterator ite2 = pusb2.getRecordSet().iterator();
				while(ite2.hasNext()){
					PlanUnivData2 data = (PlanUnivData2)ite2.next();
					univArray[count][0] = data.getUnivCd();
					univArray[count][1] = data.getFacultyCd();
					univArray[count][2] = data.getDeptCd();
					count ++;
				}
				JudgeByUnivBean jbub = new JudgeByUnivBean();
				jbub.setUnivAllList(super.getUnivAll());
				jbub.setUnivArray(univArray);
				jbub.setScoreBean(sb);
				jbub.execute();	
	
				List judgedList = jbub.getRecordSet();
				JudgementListBean listBean = new JudgementListBean();
				listBean.createListBean(judgedList, sb);

				//「表示中の生徒一人」の結果一覧を作成してセッションに追加
				session.setAttribute("useListBean", listBean);
				request.setAttribute("markExamName", listBean.getCExamName());
				request.setAttribute("wrtnExamName", listBean.getSExamName());
					
				List judgedList2 = listBean.getRecordSet();
				// 判定結果(大学指定)の追加 
				//JudgementBean[] jbs = (JudgementBean[]) judgedList2.toArray(new JudgementBean[judgedList2.size()]);//[1] del
				DetailPageBean[] jbs = (DetailPageBean[]) judgedList2.toArray(new DetailPageBean[judgedList2.size()]);//[1] add
				Iterator ite22 = pusb2.getRecordSet().iterator();
				while(ite22.hasNext()){
					PlanUnivData2 data = (PlanUnivData2)ite22.next();
					//受験日があるかどうかのチェック
					boolean isPlanDateExist = false;//受験日があるかどうか
					if(data.getPlanMap() != null && data.getPlanMap().size() > 0){
						for (Iterator it=data.getPlanMap().values().iterator(); it.hasNext(); ) {
							PlanUnivData2 pud2 =(PlanUnivData2)it.next();
							if(pud2.getExamPlanDate() != null && pud2.getExamPlanDate().length > 0){
								isPlanDateExist = true;
							}
						}
					}
					//日程コードが１でなくて、受験日のないものは表示しない
					if(!data.getScheduleCd().equals("1") && !isPlanDateExist){
						ite22.remove();
					}
					//受験日のあるものだけ表示する
					else{
						for(int i=0; i<jbs.length; i++){
							//if(data.getUnivCd().equals(jbs[i].university.univCd) && data.getFacultyCd().equals(jbs[i].university.facultyCd) && data.getDeptCd().equals(jbs[i].university.deptCd)){//[1] del
							if(data.getUnivCd().equals(jbs[i].getUnivCd()) && data.getFacultyCd().equals(jbs[i].getFacultyCd()) && data.getDeptCd().equals(jbs[i].getDeptCd())){//[1] add
								DetailPageBean makeBean = jbs[i];//[1] add
														
								//3.結果セット
								data.setCoursePntEn(makeBean.getAllot1Eng());		//英語(入試科目配点)
								data.setCoursePntMa(makeBean.getAllot1Mat());		//数学(入試科目配点)
								data.setCoursePntJap(makeBean.getAllot1Jap());		//国語(入試科目配点)
								data.setCoursePntSc(makeBean.getAllot1Sci());		//理科(入試科目配点)
								data.setCoursePntSo(makeBean.getAllot1Soc());		//地歴(入試科目配点)

								//data.setCoursePntEt("R");		//その他(入試科目配点)
								//下段
								data.setCoursePntEn2(makeBean.getAllot2Eng());		//英語(入試科目配点)
								data.setCoursePntMa2(makeBean.getAllot2Mat());		//数学(入試科目配点)
								data.setCoursePntJap2(makeBean.getAllot2Jap());		//国語(入試科目配点)
								data.setCoursePntSc2(makeBean.getAllot2Sci());		//理科(入試科目配点)
								data.setCoursePntSo2(makeBean.getAllot2Soc());		//地歴(入試科目配点)

								//data.setCoursePntEt2("R");		//その他(入試科目配点)
				
								// この部分は判定のデータを格納している 
								data.setCenterRating(makeBean.getCLetterJudgement());//センター(合格可能性判定)
								data.setSecondRating(makeBean.getSLetterJudgement());//二次(合格可能性判定)
								data.setTotalRating(makeBean.getTLetterJudgement());//総合(合格可能性判定)			
							}
						}
					}
				}
				List resultList = pusb2.getRecordSet();
				if(resultList != null && resultList.size() > 0){
					//大学・学部・学科・入試形態の順にソート
					Collections.sort(resultList, new PlanUniv2Sorter().new SortByExamPlanDateWCenter());
				}
				
				// 日付設定
				I306Bean bean = new I306Bean();
				bean.setResultList(resultList);
				
				//初回アクセス=デフォルトの表示開始日
				if (!"i306".equals(getBackward(request))) {
					bean.execute();
					form.setYear(SpecialDateUtil.getThisJpnYearRev());//[3] add
					form.setMonth(Integer.toString(bean.getMonth()));
					form.setDate(Integer.toString(bean.getDate()));
				}
				//二回目以降=任意の表示開始日
				else{
					
					//現在表示中の日・月・年
					int originalDate = Integer.parseInt(form.getOriginalDate());
					int originalMonth = Integer.parseInt(form.getOriginalMonth());
					int originalYear = Integer.parseInt(SpecialDateUtil.getYearOfMonthRev(originalMonth));
					
					//選択の日・月・年
					int selectedDate = Integer.parseInt(form.getDate());
					int selectedMonth = Integer.parseInt(form.getMonth());
					
					//■現在表示中の基準カレンダー作成(表示開始日→)
					Calendar calendar = new GregorianCalendar(originalYear, originalMonth - 1, originalDate);
					//■最大開始日のカレンダーを作成 4月30日 - DISP_DAY_LENGHT(ex.27) = ex 4月4日
					Calendar calMax = new GregorianCalendar(Integer.parseInt(SpecialDateUtil.getYearOfMonthRev(4)), 4 - 1, 30);
					calMax.add(Calendar.DATE, - DISP_DAY_LENGTH + 1);
					//■最小開始日のカレンダーを作成 5月1日
					Calendar calMin = new GregorianCalendar(Integer.parseInt(SpecialDateUtil.getYearOfMonthRev(5)), 5 - 1, 1);				
					
					//●【→】右ボタンを押した
					if(form.getRefType().equals(I306Form.DATE_FORWARD)){
						//表示期間をプラス
						calendar.add(Calendar.DATE, DISP_DAY_LENGTH);
					}
					//●【←】左ボタンを押した
					else if(form.getRefType().equals(I306Form.DATE_BACKWARD)){
						//表示期間をマイナス
						calendar.add(Calendar.DATE, - DISP_DAY_LENGTH);
					}
					//●日付コンボで更新ボタンを押した
					else if(form.getRefType().equals(I306Form.DATE_REFRESH)){
						//指定の日付でカレンダーを作成(表示開始日)
						calendar.set(Integer.parseInt(SpecialDateUtil.getYearOfMonthRev(selectedMonth)), selectedMonth - 1, selectedDate);
					}
					//●生徒を変更した
					else{
						//上記と同じ処理 - 指定の日付でカレンダーを作成(表示開始日)
						calendar.set(Integer.parseInt(SpecialDateUtil.getYearOfMonthRev(selectedMonth)), selectedMonth - 1, selectedDate);
					}
					
					//[5] add start
					//終了日の算出
					//指定の月日が最大と同じまたはより大きければ、指定の月日(表示開始日)を最大に変更
					if(calendar.after(calMax) || calendar.equals(calMax)){
						calendar = calMax;
						form.setMax(true);
					}
					//指定の月日が最小と同じまたはより小さければ、指定の月日(表示開始日)を最小に変更
					else if(calendar.before(calMin) || calendar.equals(calMin)){
						calendar = calMin;
						form.setMin(true);
					}
					
					selectedMonth = calendar.get(Calendar.MONTH) + 1;
					selectedDate = calendar.get(Calendar.DATE);
					//[5] add end
					
					bean.setMonth(selectedMonth);//[5] add
					bean.setDate(selectedDate);//[5] add
					bean.execute();

					//年ではなくて年度をセットする(javascriptがそう期待しているから)
					form.setYear(SpecialDateUtil.getThisJpnYearRev());//[3] add
					form.setMonth(Integer.toString(selectedMonth));//[5] add
					form.setDate(Integer.toString(selectedDate));//[5] add			
				}
				
				request.setAttribute("i306Bean", bean);

			} catch (final Exception e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			request.setAttribute("form", form);
			
			iCommonMap.setTargetPersonId(form.getTargetPersonId());
			forward(request, response, JSP_I306);
			
		// 転送:SERVLET
		} else {
			//個人成績分析共通-印刷対象生保存をセット
			if(iCommonMap.isBunsekiMode()){
				profile.getItemMap(IProfileCategory.I_COMMON).put(
						IProfileItem.PRINT_STUDENT, new Short(form.getPrintStudent()));	
			}
			//1.セキュリティスタンプをセット
			profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(
					IProfileItem.PRINT_STAMP, new Short(form.getPrintStamp()));

			//2.出力フォーマットをセット
			if (form.getTextFormat() != null) {
				if (form.getTextFormat().length == 2) {
					profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(
							IProfileItem.TEXT_OUT_FORMAT, new Short("3"));
				} else {
					if (form.getTextFormat()[0] != null
							&& form.getTextFormat()[0].equals("1")) {
						profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(
								IProfileItem.TEXT_OUT_FORMAT, new Short("1"));
					} else {
						profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(
								IProfileItem.TEXT_OUT_FORMAT, new Short("2"));
					}
				}
			} else {
				profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(
						IProfileItem.TEXT_OUT_FORMAT, new Short("0"));
			}
			
			// 対象生徒を保持する
			iCommonMap.setTargetPersonId(form.getTargetPersonId());
			
			// 帳票印刷が選択されたら判定を行う
			if (getForward(request).equals("sheet")) {
				// 判定処理スレッド起動
				new Thread(new JudgeThread(form.getPrintStudent(),
						getDbKey(request), session, iCommonMap,
						examSession, getUnivAll(), profile)).start();
			}
			
			dispatch(request, response);
		}
	}
	
	// 判定処理スレッド
	private class JudgeThread extends BaseJudgeThread {
	
		private final String[] selectedIndividualIds;
		private final ICommonMap iCommonMap;
		private final ExamSession examSession;
		private final List univAll;
		private final Profile profile;

		// コンストラクタ
		private JudgeThread(final String judgeStudent,
				final String dBKey,  final HttpSession pSession,
				final ICommonMap pICommonMap, final ExamSession pExamSession,
				final List pUnivAll, final Profile pProfile){
			super(pSession, dBKey);
			selectedIndividualIds = getSelectedIndividualIds(pICommonMap, judgeStudent);
			iCommonMap = pICommonMap;
			examSession = pExamSession;
			univAll = pUnivAll;
			profile = pProfile;
		}
		
		/**
		 * @see jp.co.fj.keinavi.servlets.individual.BaseJudgeThread#execute(
		 * 			java.sql.Connection)
		 */
		protected Map execute(final Connection con) throws Exception {

			// 判定結果マップ
			final Map judgedMap = new HashMap();
						
			//対象模試情報の取得
			ExamSearchBean esb = new ExamSearchBean();
			esb.setExamYear(iCommonMap.getTargetExamYear());
			esb.setExamCd(iCommonMap.getTargetExamCode());
			esb.setConnection("", con);
			esb.execute();
			//絶対に取れるはずだから、最初のを取得
			ExaminationData targetExam = (ExaminationData) esb.getRecordSet().get(0);
			
			//成績のリストを取得
			ScoreSearchBean ssb = new ScoreSearchBean();
			ssb.setLimitedYear(ProfileUtil.getChargeClassYear(profile));//[2] add
			ssb.setExamSession(examSession);
			ssb.setIndividualIds(selectedIndividualIds);
			ssb.setConnection("", con);
			ssb.setTargetExam(targetExam);
			ssb.execute();
					
			List scoreList = ssb.getRecordSet();
			for (int i = 0; i < scoreList.size(); i++) {
				Score sb = (Score) scoreList.get(i);
					
				// 出力一覧の取得 
				PlanUnivSearchBean2 pusb2 = new PlanUnivSearchBean2();
				pusb2.setIndividualId(sb.getIndividualId());
				pusb2.setConnection("", con);
				pusb2.execute();
					
				// 判定の実行 
				String[][] univArray = new String[pusb2.getRecordSet().size()][3];
				int count = 0;
				Iterator ite2 = pusb2.getRecordSet().iterator();
				while(ite2.hasNext()){
					PlanUnivData2 data = (PlanUnivData2)ite2.next();
					univArray[count][0] = data.getUnivCd();
					univArray[count][1] = data.getFacultyCd();
					univArray[count][2] = data.getDeptCd();
					count ++;
				}
						
				// 大学指定判定実行
				JudgementListBean listBean = new JudgementListBean();
				JudgeByUnivBean jbub = new JudgeByUnivBean();
				jbub.setUnivAllList(univAll);
				jbub.setUnivArray(univArray);
				jbub.setScoreBean(sb);
				jbub.execute();	
						
				List judgedList = jbub.getRecordSet();
				listBean = new JudgementListBean();
				listBean.createListBean(judgedList, sb);
				judgedMap.put(sb.getIndividualId(), listBean);
			}

			return judgedMap;
		}
	}
				
}

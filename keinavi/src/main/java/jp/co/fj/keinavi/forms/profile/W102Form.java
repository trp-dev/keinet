/*
 * 作成日: 2004/06/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.profile;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class W102Form extends BaseForm {

	private String profileName;// プロファイル名称
	private String comment; // コメント
	private String current; // カレントフォルダ
	private String profileID; // プロファイルID
	private String countingDivCode; // 集計区分コード
	private String origin; // 呼び出し元

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	/**
	 * @return
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @return
	 */
	public String getCurrent() {
		return current;
	}

	/**
	 * @return
	 */
	public String getProfileName() {
		return profileName;
	}

	/**
	 * @param string
	 */
	public void setComment(String string) {
		comment = string;
	}

	/**
	 * @param string
	 */
	public void setCurrent(String string) {
		current = string;
	}

	/**
	 * @param string
	 */
	public void setProfileName(String string) {
		profileName = string;
	}

	/**
	 * @return
	 */
	public String getCountingDivCode() {
		return countingDivCode;
	}

	/**
	 * @return
	 */
	public String getProfileID() {
		return profileID;
	}

	/**
	 * @param string
	 */
	public void setCountingDivCode(String string) {
		countingDivCode = string;
	}

	/**
	 * @param string
	 */
	public void setProfileID(String string) {
		profileID = string;
	}

	/**
	 * @return
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * @param string
	 */
	public void setOrigin(String string) {
		origin = string;
	}

}

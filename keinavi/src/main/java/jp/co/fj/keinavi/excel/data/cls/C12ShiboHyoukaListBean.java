package jp.co.fj.keinavi.excel.data.cls;

/**
 * �N���X���ъT���|�u�]�Z�]���f�[�^���X�g
 * �쐬��: 2004/07/14
 * @author	H.Fujimoto
 */
public class C12ShiboHyoukaListBean {
	//�u�]��w
	private String strDaiMei = "";
	//�u�]�w��
	private String strGkbMei = "";
	//�u�]�w��
	private String strGkkMei = "";
	//���u�]�Ґ�
	private int intShibosyaAll = 0;
	//�u�]�Z������
	private int intShibokounaiJyuni = 0;
	//�Z���^�[�����@�]�����_
	private int intHyoukaTokuten = 0;
	//�Z���^�[�����@�]��
	private String strHyoukaMark = "";
	//�ʎ����@�]���΍��l
	private float floKijyutsuHensa = 0;
	//�ʎ����@�]��
	private String strHyoukaKijyutsu = "";
	//�����|�C���g
	private int intPointAll = 0;
	//�����]��
	private String strHyoukaAll = "";

	// 2019/11/25 QQ)Ooseto �p��F�莎�������Ή� UPD START
	//2019/09/26 QQ)Oosaki ���ʃe�X�g�Ή� ADD START
	//���ʃe�X�g�]��(�p��F�莎���܂�)
	//private String strCenterratingEngpt;
	//2019/09/26 QQ)Oosaki ���ʃe�X�g�Ή� ADD END
	// 2019/09/19 QQ)Tanouchi ���ʃe�X�g�Ή� ADD START
	// �p�ꎑ�i�v���\�o�莑�i����
	//private String strHyoukaJodge = "";
	// �p�ꎑ�i�v���\�o����Ή����m
	//private String strHyoukaNotice = "";
	// 2019/09/19 QQ)Tanouchi ���ʃe�X�g�Ή� ADD END
	// 2019/09/24 QQ)Tanouchi ���ʃe�X�g�Ή� ADD START
	// ���ʃe�X�g�@�]�����_(�p�ꎎ���܂�)
	//private int intHyoukaEngTokuten = 0;
	// ���ʃe�X�g�@�]�����_(�p�ꎎ���܂܂Ȃ�)
	private int intHyoukaFullTokuten = 0;
	// ���ʃe�X�g�@�]�����_(�p�ꎎ���܂�)
	//private int intHyoukaEngFullTokuten = 0;
	// 2019/09/24 QQ)Tanouchi ���ʃe�X�g�Ή� ADD END
	// 2019/11/25 QQ)Ooseto �p��F�莎�������Ή� UPD END

	/*----------*/
	/* Get      */
	/*----------*/

	public float getFloKijyutsuHensa() {
		return this.floKijyutsuHensa;
	}
	public int getIntHyoukaTokuten() {
		return this.intHyoukaTokuten;
	}
	public int getIntPointAll() {
		return this.intPointAll;
	}
	public int getIntShibosyaAll() {
		return this.intShibosyaAll;
	}
	public int getIntShibokounaiJyuni() {
		return this.intShibokounaiJyuni;
	}
	public String getStrDaiMei() {
		return this.strDaiMei;
	}
	public String getStrGkbMei() {
		return this.strGkbMei;
	}
	public String getStrGkkMei() {
		return this.strGkkMei;
	}
	public String getStrHyoukaAll() {
		return this.strHyoukaAll;
	}
	public String getStrHyoukaKijyutsu() {
		return this.strHyoukaKijyutsu;
	}
	public String getStrHyoukaMark() {
		return this.strHyoukaMark;
	}
	// 2019/11/25 QQ)Ooseto �p��F�莎�������Ή� UPD START
	//2019/09/26 QQ)Oosaki ���ʃe�X�g�Ή� ADD START
//	public String getStrCenterratingEngpt() {
//		return strCenterratingEngpt;
//	}
	//2019/06/26 QQ)Oosaki ���ʃe�X�g�Ή� ADD END
	// 2019/09/19 QQ)Tanouchi ���ʃe�X�g�Ή� ADD START
//	public String getStrHyoukaJodge() {
//	return strHyoukaJodge;
//	}
//	public String getStrHyoukaNotice() {
//		return strHyoukaNotice;
//	}
	// 2019/09/19 QQ)Tanouchi ���ʃe�X�g�Ή� ADD END
	// 2019/09/24 QQ)Tanouchi ���ʃe�X�g�Ή� ADD START
//	public int getIntHyoukaEngTokuten() {
//		return intHyoukaEngTokuten;
//	}
	public int getIntHyoukaFullTokuten() {
		return intHyoukaFullTokuten;
	}
//	public int getIntHyoukaEngFullTokuten() {
//		return intHyoukaEngFullTokuten;
//	}
	// 2019/09/24 QQ)Tanouchi ���ʃe�X�g�Ή� ADD END
	// 2019/11/25 QQ)Ooseto �p��F�莎�������Ή� UPD END

	/*----------*/
	/* Set      */
	/*----------*/

	public void setFloKijyutsuHensa(float floKijyutsuHensa) {
		this.floKijyutsuHensa = floKijyutsuHensa;
	}
	public void setIntHyoukaTokuten(int intHyoukaTokuten) {
		this.intHyoukaTokuten = intHyoukaTokuten;
	}
	public void setIntPointAll(int intPointAll) {
		this.intPointAll = intPointAll;
	}
	public void setIntShibosyaAll(int intShibosyaAll) {
		this.intShibosyaAll = intShibosyaAll;
	}
	public void setIntShibokounaiJyuni(int intShibokounaiJyuni) {
		this.intShibokounaiJyuni = intShibokounaiJyuni;
	}
	public void setStrDaiMei(String strDaiMei) {
		this.strDaiMei = strDaiMei;
	}
	public void setStrGkbMei(String strGkbMei) {
		this.strGkbMei = strGkbMei;
	}
	public void setStrGkkMei(String strGkkMei) {
		this.strGkkMei = strGkkMei;
	}
	public void setStrHyoukaAll(String strHyoukaAll) {
		this.strHyoukaAll = strHyoukaAll;
	}
	public void setStrHyoukaKijyutsu(String strHyoukaKijyutsu) {
		this.strHyoukaKijyutsu = strHyoukaKijyutsu;
	}
	public void setStrHyoukaMark(String strHyoukaMark) {
		this.strHyoukaMark = strHyoukaMark;
	}

	// 2019/11/25 QQ)Ooseto �p��F�莎�������Ή� UPD START
	//2019/09/26 QQ)Oosaki ���ʃe�X�g�Ή� ADD START
//	public void setStrCenterratingEngpt(String strCenterratingEngpt) {
//		this.strCenterratingEngpt = strCenterratingEngpt;
//	}
	//2019/09/26 QQ)Oosaki ���ʃe�X�g�Ή� ADD END
	// 2019/09/19 QQ)Tanouchi ���ʃe�X�g�Ή� ADD START
//	public void setStrHyoukaJodge(String strHyoukaJodge) {
//		this.strHyoukaJodge = strHyoukaJodge;
//	}
//	public void setStrHyoukaNotice(String strHyoukaNotice) {
//		this.strHyoukaNotice = strHyoukaNotice;
//	}
	// 2019/09/19 QQ)Tanouchi ���ʃe�X�g�Ή� ADD END
	// 2019/09/24 QQ)Tanouchi ���ʃe�X�g�Ή� ADD START
//	public void setIntHyoukaEngTokuten(int intHyoukaEngTokuten) {
//		this.intHyoukaEngTokuten = intHyoukaEngTokuten;
//	}
	public void setIntHyoukaFullTokuten(int intHyoukaFullTokuten) {
		this.intHyoukaFullTokuten = intHyoukaFullTokuten;
	}
//	public void setIntHyoukaEngFullTokuten(int intHyoukaEngFullTokuten) {
//		this.intHyoukaEngFullTokuten = intHyoukaEngFullTokuten;
//	}
	// 2019/09/24 QQ)Tanouchi ���ʃe�X�g�Ή� ADD END
	// 2019/11/25 QQ)Ooseto �p��F�莎�������Ή� UPD END

}
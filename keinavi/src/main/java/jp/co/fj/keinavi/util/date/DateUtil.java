/*
 * 作成日: 2004/07/16
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public abstract class DateUtil {

	// 現在の日付
	private Date now = new Date();

	/**
	 * FactoryMethodパターン 
	 * @return
	 */
	public abstract SimpleDateFormat factoryMethod();

	/**
	 * Dateオブジェクトから文字列へ形式へ変換する
	 * @param date
	 * @return
	 */
	public String date2String(Date date) {
		return factoryMethod().format(date);
	}

	/**
	 * 現在の日付から文字列へ変換する
	 * @param date
	 * @return
	 */
	public String date2String() {
		return factoryMethod().format(now);
	}

	/**
	 * 文字列からDateオブジェクトに変換する
	 * @param string
	 * @return
	 * @throws ParseException
	 */
	public Date string2Date(String string) throws ParseException {
		return factoryMethod().parse(string);
	}
	
	/**
	 * @return
	 */
	public Date getNow() {
		return now;
	}

	/**
	 * @param date
	 */
	public void setNow(Date date) {
		now = date;
	}

}

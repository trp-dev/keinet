package jp.co.fj.keinavi.servlets.profile;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.profile.ProfileTransactionBean;
import jp.co.fj.keinavi.beans.profile.ProfileUpdateBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.profile.W103Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 * 2005.10.12	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 *
 * @author kawai
 *
 */
public class W103Servlet extends DefaultHttpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		if ("w103".equals(getForward(request))) {
			// プロファイルを取得
			final Profile profile = getProfile(request);
			// ログイン情報
			final LoginSession login = getLoginSession(request);
			// アクションフォーム
			final W103Form form = (W103Form) getActionForm(request,
				"jp.co.fj.keinavi.forms.profile.W103Form");

			Connection con = null; // DBコネクション
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(false);

				// プロファイルUpdateBean
				final ProfileUpdateBean bean = new ProfileUpdateBean();
				bean.setConnection(null, con);
				bean.setProfile(profile);
				bean.setOverwriteMode(1);
				bean.setLoginSession(login);

				// 複製でなければ作成者を更新する
				if (!"1".equals(request.getParameter("copyFlag"))) {
					profile.setUserID(login.getAccount());
					profile.setUserName(login.getAccountName());
				}

				// 別名保存
				if ("w102".equals(getBackward(request))) {
					// 読み取り専用でなければコピー元の読み取り専用を解除する
					if (profile.getOverwriteMode() == 0) {
						ProfileTransactionBean t = new ProfileTransactionBean();
						t.setConnection(null, con);
						t.setProfileID(profile.getProfileID());
						t.setMode("0");
						t.execute();
					}

					// 値を書き換える
					profile.setProfileID(null);
					profile.setProfileName(form.getProfileName());
					profile.setComment(form.getComment());
					profile.setFolderID(form.getCurrent());
					profile.setOverwriteMode(0);

					// 属性の書き換え
					// 高校
					if (login.isSchool()) {
						profile.setBundleCD(profile.getBundleCD());
						profile.setSectorSortingCD(null);

					// 営業 → 高校
					} else if (login.isDeputy()) {
						profile.setBundleCD(profile.getBundleCD());
						profile.setSectorSortingCD(login.getSectorSortingCD());
					}
				}

				bean.execute();

				// アクセスログ
				actionLog(request, "204", profile.getProfileID());

				con.commit();

				/* 2016/03/10 QQ)Nishiyama 大規模改修 ADD START */
				// 保存したタイミングでテンプレートフラグをのける
				profile.setTemplate(false);
				/* 2016/03/10 QQ)Nishiyama 大規模改修 ADD END */

			} catch (final Exception e) {
				rollback(con);
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			forward(request, response, JSP_W103);

		// 不明なら転送
		} else {
			dispatch(request, response);
		}
	}

}

/*
 * 作成日: 2004/07/14
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.date.ShortDateUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import com.fjh.beans.DefaultBean;

/**
 * 模試セッションを作るBean
 * 
 * @author kawai
 */
public class ExamBean extends DefaultBean {

	private ExamSession examSession = new ExamSession(); // 模試セッション
	private LoginSession loginSession; // ログインセッション
	private String bundleCD; // 一括コード
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// 今年度
		String current = KNUtil.getCurrentYear();
		// 今日の日付
		String today = new ShortDateUtil().date2String();
		// 評価対象は7年前から
		String begin = String.valueOf(Integer.parseInt(current) - 6);
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			{
				// SQL
				Query query = null;
			
				// 営業
				if (bundleCD == null) query = QueryLoader.getInstance().load("w05_1");
				// その他
				else query = QueryLoader.getInstance().load("w05_2");
				
				// ヘルプデスクなら内部データ開放日
				if (loginSession.isHelpDesk()) {
					query.replaceAll("out_dataopendate", "in_dataopendate");
				}
			
				ps = conn.prepareStatement(query.toString());
				ps.setString(1, begin);
				ps.setString(2, today);
				if (bundleCD != null) ps.setString(3, bundleCD);

				// 模試マップをセットアップ
				rs = ps.executeQuery();
				while (rs.next()) {
					String year = rs.getString(1); // 年度
					List examDataList = null; // 模試リスト

					// 模試マップから年度をキーとして模試リストを取得する
					if (examSession.getExamMap().containsKey(year)) {
						examDataList = (List)examSession.getExamMap().get(year);
					// なければ作る
					} else {
						examDataList = new ArrayList();
						examSession.getExamMap().put(year, examDataList);
					}

					// 模試データを作る
					ExamData data = new ExamData();
					data.setExamYear(year);
					data.setExamCD(rs.getString(2));
					data.setExamName(rs.getString(3));
					data.setExamNameAbbr(rs.getString(4));
					data.setExamTypeCD(rs.getString(5));
					data.setExamST(rs.getString(6));
					data.setExamDiv(rs.getString(7));
					data.setTargetGrade(rs.getString(8));
					data.setInpleDate(rs.getString(9));
					data.setDataOpenDate(rs.getString(10));
					data.setOutDataOpenDate(rs.getString(10));
					data.setInDataOpenDate(rs.getString(10));
					data.setDockingExamCD(rs.getString(11));
					data.setDockingType(rs.getString(12));
					data.setDispSequence(rs.getInt(13));
					data.setMasterDiv(rs.getString(14));
					examDataList.add(data);	
				}
			
				rs.close();
				ps.close();
			}
			
			// 今年度の公開済み模試データを取得する
			{
				Query query = QueryLoader.getInstance().load("w05_3");
				
				// ヘルプデスクなら内部データ開放日
				if (loginSession.isHelpDesk()) {
					query.replaceAll("out_dataopendate", "in_dataopendate");
				}
			
				ps = conn.prepareStatement(query.toString());
				ps.setString(1, current);
				ps.setString(2, today);

				rs = ps.executeQuery();
				while (rs.next()) {
					examSession.getPublicExamSet().add(rs.getString(1));
				}
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}

		// 模試データがなければ例外
		if (examSession.getExamMap().keySet().size() == 0) {
			KNServletException e =
				new KNServletException("受験生がいる模試データが存在しません。");
			e.setErrorCode("10W002990001");
			throw e;
		}
		
		// 年度のリストを作る
		List container = new ArrayList();
		Iterator ite = examSession.getExamMap().keySet().iterator();
		while (ite.hasNext()) {
			container.add(ite.next());
		}
		// ソート
		Collections.sort(container);
		// 降順
		Collections.reverse(container);
		// 値をセットする
		examSession.setYears((String[])container.toArray(new String[0]));
	}

	/**
	 * @param session
	 */
	public void setLoginSession(LoginSession session) {
		loginSession = session;
	}

	/**
	 * @param string
	 */
	public void setBundleCD(String string) {
		bundleCD = string;
	}

	/**
	 * @return
	 */
	public ExamSession getExamSession() {
		return examSession;
	}

}

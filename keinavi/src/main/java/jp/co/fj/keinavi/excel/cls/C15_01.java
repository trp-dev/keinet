package jp.co.fj.keinavi.excel.cls;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C15Item;
import jp.co.fj.keinavi.excel.data.cls.C15ListBean;
import jp.co.fj.keinavi.excel.data.cls.C15PersonalDataListBean;
import jp.co.fj.keinavi.excel.data.cls.C15ShiboListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * 作成日: 2004/08/30
 * @author Ito.Y
 *
 * クラス成績分析−志望大学別個人評価　出力処理
 *
 *
 * <2010年度改修>
 * 2010.01.06   Tomohisa YAMADA - Totec
 *              センター・リサーチ５段階評価対応
 *
 */
public class C15_01 {

	private int noerror	= 0;		// 正常終了
	private int errfread	= 1;		// ファイルreadエラー
	private int errfwrite	= 2;		// ファイルwriteエラー
	private int errfdata	= 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow		= 87;			//MAX行数

	private boolean bolBookCngFlg = true;			//改ファイルフラグ
	private boolean bolBookClsFlg = false;			//改ファイルフラグ
	private boolean bolSheetCngFlg = true;			//改シートフラグ
	private int intDataStartRow = 9;					//データセット開始行

	final private String masterfile0	= "C15_01";
	final private String masterfile1	= "C15_01";
	private String masterfile	= "";
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 DEL START
//	final private String cntMshCd		= "38";		// センター模試コード
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 DEL END

	/*
	 * 	Excel編集メイン
	 * 		C15Item c15Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int c15_01EditExcel(C15Item c15Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("C15_01","C15_01帳票作成開始","");

		//テンプレートの決定
		if (c15Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try{
			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			int intMaxSheetIndex = 0;					//シート作成数カウンタ

			int intRow = 0;
//			int intTisyoKbn = 0;
			String strGakkoMei = "";
			String strDaigakuCode = "";
			String strNtiCode = "";
			String strGkbCode = "";
			String strGrade = null;
			String strClass = null;

//2004.10.8Add
			String strKokushiKbn = "";
			String strStKokushiKbn = "";
//2004.10.8AddEnd

			int intBookCngCount = 0;

// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 DEL START
//			//2019/09/09 QQ)Oosaki 共通テスト対応 ADD START
//			final String HALFSIZESPACE = " ";
//			//2019/09/09 QQ)Oosaki 共通テスト対応 ADD END
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 DEL END
//2004.11.29 add
			boolean	bolDataExistFlg	= false;	//ﾃﾞｰﾀ有無ﾌﾗｸﾞ

			// 基本ファイルを読込む
			C15ListBean c15ListBean = new C15ListBean();

			// データセット
			ArrayList c15List = c15Item.getC15List();
			ListIterator itr = c15List.listIterator();

			// 表示対象
//			intTisyoKbn = c15Item.getIntDispFlg();
//			if( intTisyoKbn== 1 ){
//				strDispTarget = "すべて";
//			}
//			else if( intTisyoKbn==2 ){
//				strDispTarget = "国公立大";
//			}
//			else if( intTisyoKbn==3 ){
//				strDispTarget = "私大・短大・その他";
//			}


			while( itr.hasNext() ){
				c15ListBean = (C15ListBean)itr.next();

				ArrayList c15ShiboList = c15ListBean.getC15ShiboList();

				// 学年・クラスが変わった時に改シートする
				if( (strGrade!=null)&&(strClass!=null) ){
					if( (!cm.toString(strGrade).equals( cm.toString(c15ListBean.getStrGrade()) )||!cm.toString(strClass).equals( cm.toString(c15ListBean.getStrClass()) )) ){
						//改シート
						//罫線出力（セルの上部に太線を出力）
						if( !(intRow >= intMaxRow) ){
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//						        //2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//							//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
//							//									intRow, intRow, 0, 19, false, true, false, false);
//							cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
//																intRow, intRow, 0, 25, false, true, false, false);
//							//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
							cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
																intRow, intRow, 0, 19, false, true, false, false);
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END
						}

						strDaigakuCode = "";
						strGkbCode = "";
						strNtiCode = "";
						bolSheetCngFlg = true;
						if( intMaxSheetIndex >= intMaxSheetSr ){
							//改ファイル
							bolBookCngFlg = true;
							bolBookClsFlg = true;
						}
					}
				}

				/** 志望大学データセット **/
				//個人データリストの内容をセット
				C15ShiboListBean c15ShiboListBean = new C15ShiboListBean();
				Iterator itrShibo = c15ShiboList.iterator();
				while(itrShibo.hasNext()){

					//ﾃﾞｰﾀあり 2004.11.29 add
					bolDataExistFlg = true;

					c15ShiboListBean = (C15ShiboListBean) itrShibo.next();

//2004.10.8Add
					// 表示対象
					int intDaigakuCd = (int)Integer.parseInt(c15ShiboListBean.getStrDaigakuCd());
					if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
						strKokushiKbn = "国公立大";
					}
					if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
						strKokushiKbn = "私立大";
					}
					if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
						strKokushiKbn = "短大";
					}
					if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
						strKokushiKbn = "その他";
					}

					if( (!cm.toString(strKokushiKbn).equals(cm.toString(strStKokushiKbn))) ){
						if( !cm.toString(strStKokushiKbn).equals("") ){
							//罫線出力（セルの上部に太線を出力）
							if( !(intRow >= intMaxRow) ){
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//							    //2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//							    //cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
//							    //									intRow, intRow, 0, 19, false, true, false, false);
//							    cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
//							    									intRow, intRow, 0, 25, false, true, false, false);
//							    //2019/08/14 Hics)Ueta 共通テスト対応 UPD END
							    cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
							    									intRow, intRow, 0, 19, false, true, false, false);
 // 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END
							}
						}
						strStKokushiKbn = strKokushiKbn;
						bolSheetCngFlg = true;
					}
//2004.10.8AddEnd
					/** 個人データセット **/
					//個人データリストの内容をセット
					C15PersonalDataListBean c15PersonalDataListBean = new C15PersonalDataListBean();
					Iterator itrPerson = c15ShiboListBean.getC15PersonalDataList().iterator();

					while( itrPerson.hasNext() ){
						//データリストより各項目を取得する
						c15PersonalDataListBean = (C15PersonalDataListBean)itrPerson.next();


						if( bolSheetCngFlg == true ){

							if( bolBookCngFlg == true ){
								/** 修正・ココはカウンターのチェックでは処理できない **/
								if( bolBookClsFlg ){

									boolean bolRet = false;
									// Excelファイル保存
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);

									if( bolRet == false ){
										return errfwrite;
									}
									intBookCngCount++;
									bolBookClsFlg = false;
								}

								// マスタExcel読み込み
								workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
								if( workbook==null ){
									return errfread;
								}

								bolBookCngFlg = false;
								intMaxSheetIndex = 0;
							}

							//シート作成
							// シートテンプレートのコピー
							workSheet = workbook.cloneSheet(0);

							// ヘッダ右側に帳票作成日時を表示する
							cm.setHeader(workbook, workSheet);

							// セキュリティスタンプセット
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//							//2019/09/10 QQ)Oosaki 共通テスト対応 UPD START
//							//String secuFlg = cm.setSecurity( workbook, workSheet, c15Item.getIntSecuFlg(), 17, 19 );
//							//workCell = cm.setCell( workSheet, workRow, workCell, 0, 17 );
//							String secuFlg = cm.setSecurity( workbook, workSheet, c15Item.getIntSecuFlg(), 23, 25 );
//							workCell = cm.setCell( workSheet, workRow, workCell, 0, 23 );
//							//2019/09/10 QQ)Oosaki 共通テスト対応 UPD END
							String secuFlg = cm.setSecurity( workbook, workSheet, c15Item.getIntSecuFlg(), 17, 19 );
							workCell = cm.setCell( workSheet, workRow, workCell, 0, 17 );
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END
							workCell.setCellValue(secuFlg);

							// 学校名セット
							strGakkoMei = "学校名　：" + cm.toString(c15Item.getStrGakkomei()) + "　学年：" + cm.toString(c15ListBean.getStrGrade() )
							+ "　クラス名：" + cm.toString(c15ListBean.getStrClass());
							workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
							workCell.setCellValue( strGakkoMei );
							strGrade = c15ListBean.getStrGrade();
							strClass = c15ListBean.getStrClass();

							// 模試月取得
							String moshi =cm.setTaisyouMoshi( c15Item.getStrMshDate() );
							// 対象模試セット
							workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
							workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(c15Item.getStrMshmei()) + moshi);

							//表示対象
							workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
//							workCell.setCellValue( "表示対象：" + cm.toString(strDispTarget) );
							workCell.setCellValue( "表示対象：" + cm.toString(strKokushiKbn) );

							workCell = cm.setCell( workSheet, workRow, workCell, 6, 5 );
							workCell.setCellValue( "A" );
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//							//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//							//workCell = cm.setCell( workSheet, workRow, workCell, 6, 6 );
//							workCell = cm.setCell( workSheet, workRow, workCell, 6, 7 );
//							//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//							workCell.setCellValue( "B" );
//							//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//							//workCell = cm.setCell( workSheet, workRow, workCell, 6, 7 );
//							workCell = cm.setCell( workSheet, workRow, workCell, 6, 9 );
//							//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//							workCell.setCellValue( "C" );
//							//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//							//workCell = cm.setCell( workSheet, workRow, workCell, 6, 8 );
//							workCell = cm.setCell( workSheet, workRow, workCell, 6, 11 );
//							//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//							workCell.setCellValue( "D" );
//							//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//							//workCell = cm.setCell( workSheet, workRow, workCell, 6, 9 );
//							workCell = cm.setCell( workSheet, workRow, workCell, 6, 13 );
//							//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//							workCell.setCellValue( "E" );
							workCell = cm.setCell( workSheet, workRow, workCell, 6, 6 );
							workCell.setCellValue( "B" );
							workCell = cm.setCell( workSheet, workRow, workCell, 6, 7 );
							workCell.setCellValue( "C" );
							workCell = cm.setCell( workSheet, workRow, workCell, 6, 8 );
							workCell.setCellValue( "D" );
							workCell = cm.setCell( workSheet, workRow, workCell, 6, 9 );
							workCell.setCellValue( "E" );
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END

							intMaxSheetIndex++;		//シート数カウンタインクリメント
							bolSheetCngFlg = false;

							intRow = intDataStartRow;
						}


						//日程セット
						if( !cm.toString(strDaigakuCode).equals(cm.toString(c15ShiboListBean.getStrDaigakuCd()))
							|| !cm.toString(strGkbCode).equals(cm.toString(c15ShiboListBean.getStrGakubuCd()))
							|| !cm.toString(strNtiCode).equals(cm.toString(c15ShiboListBean.getStrNtiCd())) ){
							//評価別人数Eセット
							if( c15ShiboListBean.getIntHyoukaE() != -999 ){
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//								//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//								//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 9);
//								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 13);
//								//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//								workCell.setCellValue( c15ShiboListBean.getIntHyoukaE() );
//								//2019/09/06 QQ)Oosaki 共通テスト対応 DEL START
//								////2019/08/14 Hics)Ueta 共通テスト対応 ADD START
//								//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 14);
//								//workCell.setCellValue(  c15PersonalDataListBean.getIntRatingnumE() );
//								////2019/08/14 Hics)Ueta 共通テスト対応 ADD END
//								//2019/09/06 QQ)Oosaki 共通テスト対応 DEL END
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 9);
								workCell.setCellValue( c15ShiboListBean.getIntHyoukaE() );
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END
							}
							//評価別人数Dセット
							if( c15ShiboListBean.getIntHyoukaD() != -999 ){
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//								//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//								//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 8);
//								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 11);
//								//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//								workCell.setCellValue( c15ShiboListBean.getIntHyoukaD() );
//								//2019/09/06 QQ)Oosaki 共通テスト対応 DEL START
//								////2019/08/14 Hics)Ueta 共通テスト対応 ADD START
//								//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 12);
//								//workCell.setCellValue( c15PersonalDataListBean.getIntRatingnumD() );
//								////2019/08/14 Hics)Ueta 共通テスト対応 ADD END
//								//2019/09/06 QQ)Oosaki 共通テスト対応 DEL END
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 8);
								workCell.setCellValue( c15ShiboListBean.getIntHyoukaD() );
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END
							}
							//評価別人数Cセット
							if( c15ShiboListBean.getIntHyoukaC() != -999 ){
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//								//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//								//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 7);
//								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 9);
//								//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//								workCell.setCellValue( c15ShiboListBean.getIntHyoukaC() );
//								//2019/09/06 QQ)Oosaki 共通テスト対応 DEL START
//								////2019/08/14 Hics)Ueta 共通テスト対応 ADD START
//								//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 10);
//								//workCell.setCellValue( c15PersonalDataListBean.getIntRatingnumC() );
//								////2019/08/14 Hics)Ueta 共通テスト対応 ADD END
//								//2019/09/06 QQ)Oosaki 共通テスト対応 DEL END
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 7);
								workCell.setCellValue( c15ShiboListBean.getIntHyoukaC() );
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END
							}
							//評価別人数Bセット
							if( c15ShiboListBean.getIntHyoukaB() != -999 ){
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//								//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//								//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 6);
//								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 7);
//								//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//								workCell.setCellValue( c15ShiboListBean.getIntHyoukaB() );
//								//2019/09/06 QQ)Oosaki 共通テスト対応 DEL START
//								////2019/08/14 Hics)Ueta 共通テスト対応 ADD START
//								//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 8);
//								//workCell.setCellValue( c15PersonalDataListBean.getIntRatingnumB() );
//								////2019/08/14 Hics)Ueta 共通テスト対応 ADD END
//								//2019/09/06 QQ)Oosaki 共通テスト対応 DEL END
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 6);
								workCell.setCellValue( c15ShiboListBean.getIntHyoukaB() );
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END
							}
							//評価別人数Aセット
							if( c15ShiboListBean.getIntHyoukaA() != -999 ){
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 5);
								workCell.setCellValue( c15ShiboListBean.getIntHyoukaA() );
								//2019/09/06 QQ)Oosaki 共通テスト対応 DEL START
								////2019/08/14 Hics)Ueta 共通テスト対応 ADD START
								//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 6);
								//workCell.setCellValue( c15PersonalDataListBean.getIntRatingnumA() );
								////2019/08/14 Hics)Ueta 共通テスト対応 ADD END
								//2019/09/06 QQ)Oosaki 共通テスト対応 DEL START
							}
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 DEL START
//							//2019/09/06 QQ)Oosaki 共通テスト対応 ADD START
//							//共通テスト評価(英語認定試験含む)の人数をセット
//							//評価別人数Eセット(英語認定試験含む)
//                                                        if(c15ShiboListBean.getIntRatingnumE() != -999) {
//                                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow, 14);
//                                                        workCell.setCellValue( c15ShiboListBean.getIntRatingnumE() );
//                                                        }
//                                                      //評価別人数Dセット(英語認定試験含む)
//                                                        if(c15ShiboListBean.getIntRatingnumD() != -999) {
//                                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow, 12);
//                                                        workCell.setCellValue( c15ShiboListBean.getIntRatingnumD() );
//                                                        }
//                                                      //評価別人数Cセット(英語認定試験含む)
//                                                        if(c15ShiboListBean.getIntRatingnumC() != -999) {
//                                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow, 10);
//                                                        workCell.setCellValue( c15ShiboListBean.getIntRatingnumC() );
//                                                        }
//                                                      //評価別人数Bセット(英語認定試験含む)
//                                                        if(c15ShiboListBean.getIntRatingnumB() != -999) {
//                                                        workCell = cm.setCell( workSheet, workRow, workCell, intRow, 8);
//                                                        workCell.setCellValue( c15ShiboListBean.getIntRatingnumB() );
//                                                        }
//							//評価別人数Aセット(英語認定試験含む)
//							if(c15ShiboListBean.getIntRatingnumA() != -999) {
//							workCell = cm.setCell( workSheet, workRow, workCell, intRow, 6);
//							workCell.setCellValue( c15ShiboListBean.getIntRatingnumA() );
//							}
//							//2019/09/06 QQ)Oosaki 共通テスト対応 ADD END
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 DEL END
							//出願予定セット
							if( c15ShiboListBean.getIntYoteiNinzu() != -999 ){
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 4);
								workCell.setCellValue( c15ShiboListBean.getIntYoteiNinzu() );
							}
							//総志望セット
							if( c15ShiboListBean.getIntSoshibo() != -999 ){
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 3);
								workCell.setCellValue( c15ShiboListBean.getIntSoshibo() );
							}

							//日程セット
							workCell = cm.setCell( workSheet, workRow, workCell, intRow, 2);
							workCell.setCellValue( c15ShiboListBean.getStrNittei() );
							strNtiCode = c15ShiboListBean.getStrNtiCd();

							//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//							//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//							//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
//							//									intRow, intRow, 2, 9, false, true, false, false);
//							cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
//																intRow, intRow, 2, 14, false, true, false, false);
//							//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
							cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
									intRow, intRow, 2, 9, false, true, false, false);
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END

						}

						//学部セット
						if( !cm.toString(strGkbCode).equals(cm.toString(c15ShiboListBean.getStrGakubuCd())) || !cm.toString(strDaigakuCode).equals( cm.toString(c15ShiboListBean.getStrDaigakuCd()) )){
							workCell = cm.setCell( workSheet, workRow, workCell, intRow, 1);
							workCell.setCellValue( c15ShiboListBean.getStrGakubuMei() );
							strGkbCode = c15ShiboListBean.getStrGakubuCd();

							//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
							cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
																intRow, intRow, 1, 9, false, true, false, false);

						}

						//大学名セット
						if( !cm.toString(strDaigakuCode).equals( cm.toString(c15ShiboListBean.getStrDaigakuCd()) )){
							workCell = cm.setCell( workSheet, workRow, workCell, intRow, 0);
							workCell.setCellValue( c15ShiboListBean.getStrDaigakuMei() );
							strDaigakuCode = c15ShiboListBean.getStrDaigakuCd();

//							if( intRow == intDataStartRow ){
								//罫線出力(セルの上部に太線を引く）
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//								//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//								//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
//								//									intRow, intRow, 0, 19, false, true, false, false );
//								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
//																	intRow, intRow, 0, 25, false, true, false, false );
//								//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
							cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
									intRow, intRow, 0, 19, false, true, false, false );
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END
//							}else{
//								//罫線出力(セルの上部に太線を引く）
//								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
//																	intRow, intRow, 0, 9, false, true, false, false );
//							}
						}

						//学年
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//						//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 10);
//						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 15);
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//						workCell.setCellValue( c15PersonalDataListBean.getStrGrade() );
//
//						//クラス
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//						//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 11);
//						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 16);
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//						workCell.setCellValue( c15PersonalDataListBean.getStrClass() );
//
//						//クラス番号
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//						//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 12);
//						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 17);
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//						workCell.setCellValue( c15PersonalDataListBean.getStrClassNo() );
//
//						//氏名
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//						//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 13);
//						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 18);
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 10);
						workCell.setCellValue( c15PersonalDataListBean.getStrGrade() );
						//クラス
						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 11);
						workCell.setCellValue( c15PersonalDataListBean.getStrClass() );
						//クラス番号
						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 12);
						workCell.setCellValue( c15PersonalDataListBean.getStrClassNo() );
						//氏名
						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 13);
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END
						workCell.setCellValue( c15PersonalDataListBean.getStrKanaName() );

						//性別
						int sexKbn = Integer.parseInt( c15PersonalDataListBean.getStrSex() );
						String strSex = "";
						if( sexKbn == 1 ){
							strSex = "男";
						}else if( sexKbn == 2 ){
							strSex = "女";
						}else if( sexKbn == 9 ){
							strSex = "不";
						}
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//						//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 14 );
//						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 19 );
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//						workCell.setCellValue( strSex );
//
//						//志望学科・日程
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//						//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 15 );
//						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 20 );
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//						workCell.setCellValue( c15PersonalDataListBean.getStrGakkaMei() );
//
//						//評価（センター）
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//						//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 16 );
//						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 21 );
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//						workCell.setCellValue( c15PersonalDataListBean.getStrHyoukaMark() );
//
//						//2019/09/06 QQ)Oosaki 共通テスト対応 ADD START
//                                                //共通テスト評価(英語認定試験含む)
//						String strComEvaEng = "";
//						if("×".equals(c15PersonalDataListBean.getStrAppQuelJudge()) ) {
//						    strComEvaEng = cm.padRight(nullToEmpty(c15PersonalDataListBean.getStrHyoukaMarkEngPt()), "UTF-8", 2) + HALFSIZESPACE + nullToEmpty(c15PersonalDataListBean.getStrAppQuelJudge()) + cm.padRight(nullToEmpty(c15PersonalDataListBean.getStrAppQuelNotice()), "UTF-8", 2);
//						} else {
//						    strComEvaEng = cm.padRight(nullToEmpty(c15PersonalDataListBean.getStrHyoukaMarkEngPt()), "UTF-8", 2) + HALFSIZESPACE + cm.padRight(nullToEmpty(c15PersonalDataListBean.getStrAppQuelJudge()), "UTF-8", 2) + cm.padRight(nullToEmpty(c15PersonalDataListBean.getStrAppQuelNotice()), "UTF-8", 2);
//						}
//                                                workCell = cm.setCell( workSheet, workRow, workCell, intRow, 22 );
//                                                workCell.setCellValue(strComEvaEng);
//                                                //workCell.setCellValue( strComEvaEng );
//                                                //2019/09/06 QQ)Oosaki 共通テスト対応 ADD END
//
//						//評価（２次）
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//						//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 17 );
//						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 23 );
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
//						workCell.setCellValue( c15PersonalDataListBean.getStrHyoukaKijyutsu() );
//
//						//評価（総合）
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//						//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 18 );
//						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 24 );
//						//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 14 );
						workCell.setCellValue( strSex );
						//志望学科・日程
						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 15 );
						workCell.setCellValue( c15PersonalDataListBean.getStrGakkaMei() );
						//評価（センター）
						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 16 );
						workCell.setCellValue( c15PersonalDataListBean.getStrHyoukaMark() );
						//評価（２次）
						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 17 );
						workCell.setCellValue( c15PersonalDataListBean.getStrHyoukaKijyutsu() );
						//評価（総合）
						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 18 );
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END
						workCell.setCellValue( c15PersonalDataListBean.getStrHyoukaAll() );

						//出願予定
						int yoteiFlg =  c15PersonalDataListBean.getIntYoteiFlg();
						if( yoteiFlg != -999 ){
							String strYotei = "";
							if( yoteiFlg == 1 ){
								strYotei = "○";
							}
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//							//2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//							//workCell = cm.setCell( workSheet, workRow, workCell, intRow, 19 );
//							workCell = cm.setCell( workSheet, workRow, workCell, intRow, 25 );
							workCell = cm.setCell( workSheet, workRow, workCell, intRow, 19 );
							//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END
							workCell.setCellValue( strYotei );
						}
						//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
						intRow++;

						if( intRow >= intMaxRow ){
							//改シート
							strDaigakuCode = "";
							strGkbCode = "";
							strNtiCode = "";
							bolSheetCngFlg = true;
							if( intMaxSheetIndex >= intMaxSheetSr ){
								//改ファイル
								bolBookCngFlg = true;
								bolBookClsFlg = true;
							}
						}

					}

				}

			}

			/** 修正・改ファイルフラグがtrueでもfalseでも処理を実行する **/
			//罫線出力（セルの上部に太線を出力）→ ﾃﾞｰﾀありの場合のみ 2004.11.29
			if( !(intRow >= intMaxRow) && bolDataExistFlg == true ){
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START
//			        //2019/08/14 Hics)Ueta 共通テスト対応 UPD START
//				//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
//				//									intRow, intRow, 0, 19, false, true, false, false);
//				cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
//													intRow, intRow, 0, 25, false, true, false, false);
//				//2019/08/14 Hics)Ueta 共通テスト対応 UPD END
				cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
						intRow, intRow, 0, 19, false, true, false, false);
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END
			}

			// Excelファイル保存
			boolean bolRet = false;

			if(intBookCngCount > 0){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;

			}

		}
		catch( Exception e ){
			log.Err("C15_01","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("C15_01","C15_01帳票作成終了","");
		return noerror;
	}

// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 DEL START
//	    //2019/09/09 QQ)Oosaki 共通テスト対応 ADD START
//	    /**
//	     * nullが来たら空白にする
//	     */
//	    private String nullToEmpty(String str) {
//	        if(str == null) {
//	            return "";
//	        }
//	        if(str.equals("null")) {
//	            return "";
//	        }
//	        return str;
//	    }
//	    //2019/09/09 QQ)Oosaki 共通テスト対応 ADD END
// 2019/11/21 QQ)Tanioka 英語認定試験延期対応 DEL END

}

/*
 * 作成日: 2004/10/14
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.help;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.beans.help.H101Bean;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.forms.help.H101Form;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class H101Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ

		// セッション
		HttpSession session = request.getSession();

		// ログイン情報
		LoginSession login = super.getLoginSession(request);

		// アクションフォーム
		H101Form form = null;
		// requestから取得する
		try {
			form = (H101Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.help.H101Form");
		} catch (Exception e) {
			throw new ServletException(e);
		}
		
		Connection con = null; // コネクション
		try {
			con = super.getConnectionPool(request); 	// コネクション取得
			H101Bean bean = new H101Bean();
			bean.setConn(con);
			bean.execute();
			request.setAttribute("H101Bean", bean);
		} catch (Exception e) {
			throw new ServletException(e.toString());
		} finally {
			super.releaseConnectionPool(request, con); // コネクション解放
		}

		// アクションフォーム
		request.setAttribute("form", form);
		super.forward(request, response, JSP_H101);
		


	}

}

/*
 * 作成日: 2004/09/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.excel.personal;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.personal.I11Item;
import jp.co.fj.keinavi.excel.data.personal.I11ListBean;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

//import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


/**
 * 個人成績分析
 * @author C.Murata
 */
public class Personal {

	private CM cm = new CM();		//共通関数用クラス インスタンス

	private final String MacroFileName = "Personal";
	private final String PrmSheetName= "prm";

	/*
	 * フラグにより帳票出力の判断をする。
	 *
	 * I11Item:個人成績分析帳票出力用データクラス
	 * outfilelist:実行するブック名を格納するリスト
	 * tmpfilelist:帳票ブック名を格納するリスト
	 * saveFlg:1…1=未使用 2=印刷・一括印刷 3/4/5=未使用 6=かんたん印刷
	 * 戻り値: true:正常終了、false:異常終了
	 */
	 public boolean personal( I11Item i11Item, ArrayList outfilelist, ArrayList tmpfilelist, int saveFlg, String SessionID, KNSheetLog sheetLog, boolean mendanFlg ){

		KNLog log = KNLog.getInstance(null,null,null);
		boolean bolRet = false;

	 	try{

			log.Ep("Personal","個人成績分析 帳票作成開始","");


// MOD START 2019/08/16 QQ)K.Hisakawa 共通テスト対応
//			int intSaveFlg = 0;
//			if(saveFlg == 6 || saveFlg == 7){
//				intSaveFlg = saveFlg;
//			}
//			else{
//				intSaveFlg = saveFlg + 10;
//			}

			// 帳票出力フラグを "5"(保存) に固定
			int intSaveFlg = 5;
// MOD END   2019/08/16 QQ)K.Hisakawa 共通テスト対応

	 		switch( intSaveFlg ){
	 			case 6:	//かんたん印刷（保存なし）
	 			case 7:	//かんたん印刷（保存あり）
					log.Ep("Personal","個人成績分析 帳票作成（かんたん印刷）開始","");

					ArrayList i11List = i11Item.getI11List();
					I11ListBean i11ListBean = new I11ListBean();
					Iterator itrPersonalInfo = i11List.iterator();
					String strSaveFileName = "";
					ArrayList SaveFileList = new ArrayList();

					while( itrPersonalInfo.hasNext() ){

						I11Item i11PersonalItem = new I11Item();
						ArrayList i11PersonalList = new ArrayList();

						i11ListBean = (I11ListBean)itrPersonalInfo.next();
						String strGakunen = i11ListBean.getStrGrade();
						String strClass = i11ListBean.getStrClass();
						String strClassNo = i11ListBean.getStrClassNum();

						i11PersonalList.add((I11ListBean)i11ListBean);

						i11PersonalItem.setStrBaseMshmei(i11Item.getStrBaseMshmei());				//模試名
						i11PersonalItem.setStrBaseMshDate( i11Item.getStrBaseMshDate());			//模試実施基準日
						i11PersonalItem.setStrBaseGakunen(i11Item.getStrBaseGakunen());				//模試対象学年
						//2004.10.18 Add
						i11PersonalItem.setStrMshmei(i11Item.getStrMshmei());						//模試短縮名
						//Add End
						i11PersonalItem.setIntBaseMshKyokaSr(i11Item.getIntBaseMshKyokaSr());		//模試教科数
						i11PersonalItem.setIntMenSheetFlg(i11Item.getIntMenSheetFlg());				//面談シートフラグ
						i11PersonalItem.setIntMenSheetSecuFlg(i11Item.getIntMenSheetSecuFlg());		//面談シートキュリティスタンプ
						i11PersonalItem.setIntKyoSheetFlg(i11Item.getIntKyoSheetFlg());				//教科分析シートフラグ
						i11PersonalItem.setIntKyoSheetSecuFlg(i11Item.getIntKyoSheetSecuFlg());		//教科分析シートセキュリティスタンプ
						i11PersonalItem.setIntSuiiFlg(i11Item.getIntSuiiFlg());						//科目別成績推移グラフフラグ
						i11PersonalItem.setIntSuiiSecuFlg(i11Item.getIntSuiiSecuFlg());				//科目別成績推移グラフセキュリティスタンプ
						i11PersonalItem.setIntSetsumonFlg(i11Item.getIntSetsumonFlg());				//設問別成績グラフフラグ
						i11PersonalItem.setIntSetsumonSecuFlg(i11Item.getIntSetsumonSecuFlg());		//設問別成績グラフセキュリティスタンプ
						i11PersonalItem.setIntSeisekiFlg(i11Item.getIntSeisekiFlg());				//連続個人成績表フラグ
						i11PersonalItem.setIntSeisekiSecuFlg(i11Item.getIntSeisekiSecuFlg());		//連続個人成績表セキュリティスタンプ
						//2004.10.17 Add
						i11PersonalItem.setIntSeisekiFormatFlg(i11Item.getIntSeisekiFormatFlg());	//連続個人成績表出力フォーマット
						//Add end
						i11PersonalItem.setIntHanteiFlg(i11Item.getIntHanteiFlg());					//志望大学判定一覧フラグ
						i11PersonalItem.setIntHanteiSecuFlg(i11Item.getIntHanteiSecuFlg());			//志望大学判定データセキュリティスタンプ
						i11PersonalItem.setIntScheduleFlg(i11Item.getIntScheduleFlg());				//受験大学スケジュールフラグ
						i11PersonalItem.setIntScheduleFormatFlg(i11Item.getIntScheduleFormatFlg());	//受験大学スケジュール出力フォーマット
						i11PersonalItem.setIntScheduleSecuFlg(i11Item.getIntScheduleSecuFlg());		//受験大学スケジュールセキュリティスタンプ
						//2005.05.25 Add
						i11PersonalItem.setIntMenSheetShubetsuFlg(i11Item.getIntMenSheetShubetsuFlg());	//面談シート出力種別フラグ
						i11PersonalItem.setIntKyoSheetShubetsuFlg(i11Item.getIntKyoSheetShubetsuFlg());	//教科分析シート出力種別フラグ
						i11PersonalItem.setIntSuiiShubetsuFlg(i11Item.getIntSuiiShubetsuFlg());			//科目別成績推移グラフ出力種別フラグ
						i11PersonalItem.setIntSetsumonShubetsuFlg(i11Item.getIntSetsumonShubetsuFlg());	//設問別成績推移グラフ出力種別フラグ
						i11PersonalItem.setIntSeisekiShubetsuFlg(i11Item.getIntSeisekiShubetsuFlg());	//連続個人成績表出力種別フラグ
						i11PersonalItem.setIntHanteiShubetsuFlg(i11Item.getIntHanteiShubetsuFlg());		//志望大学判定出力種別フラグ
						i11PersonalItem.setIntScheduleShubetsuFlg(i11Item.getIntScheduleShubetsuFlg());	//受験大学スケジュール出力種別フラグ
						//Add end
						i11PersonalItem.setI11List(i11PersonalList);								//データリスト

						//面談シート(I11)
						ArrayList i11OutFileList = new ArrayList();
						ArrayList i11SaveFileList = new ArrayList();
						if( i11Item.getIntMenSheetFlg() == 2 || i11Item.getIntMenSheetFlg() == 3 || i11Item.getIntMenSheetFlg() == 4 ) {
                                                    I11 i11 = new I11();
                                                    bolRet = i11.i11(i11PersonalItem, i11OutFileList, intSaveFlg, SessionID, sheetLog);
                                                    if( bolRet != true ){
                                                            return false;
                                                    }
                                                    strSaveFileName = "I11_" + strGakunen + strClass + strClassNo + ".xls";
                                                    i11SaveFileList.add(strSaveFileName);
						}

						//教科分析シート(I12)
						ArrayList i12OutFileList = new ArrayList();
						ArrayList i12SaveFileList = new ArrayList();
						if( i11Item.getIntKyoSheetFlg() != 0 || i11Item.getIntSuiiFlg() != 0 || i11Item.getIntSetsumonFlg() != 0){
							I12 i12 = new I12();
							bolRet = i12.i12(i11PersonalItem, i12OutFileList, intSaveFlg, SessionID, sheetLog);
							if( bolRet != true ){
								return false;
							}

							if( i12OutFileList.size() > 1 ){
								for( int fileindex = 1; fileindex <= i12OutFileList.size(); fileindex++ ){
									strSaveFileName = "I12_" + strGakunen + strClass + strClassNo + "_" + String.valueOf(fileindex) + ".xls";;
									i12SaveFileList.add(strSaveFileName);
								}
							}
							else{
								strSaveFileName = "I12_" + strGakunen + strClass + strClassNo + ".xls";;
								i12SaveFileList.add(strSaveFileName);
							}
						}

						//連続個人成績表(I13)
						ArrayList i13OutFileList = new ArrayList();
						ArrayList i13SaveFileList = new ArrayList();
						if( i11Item.getIntSeisekiFlg() != 0 ){
							I13 i13 = new I13();
							bolRet = i13.i13( i11PersonalItem, i13OutFileList, intSaveFlg, SessionID, sheetLog );
							if( bolRet != true ){
								return false;
							}
							strSaveFileName = "I13_" + strGakunen + strClass + strClassNo + ".xls";;
							i13SaveFileList.add(strSaveFileName);
						}

						//志望校判定(I14)
						ArrayList i14OutFileList = new ArrayList();
						ArrayList i14SaveFileList = new ArrayList();
						if( i11Item.getIntHanteiFlg() != 0 ){
							I14 i14 = new I14();
							bolRet = i14.i14( i11PersonalItem, i14OutFileList, intSaveFlg, SessionID, sheetLog );
							if( bolRet != true ){
								return false;
							}
							strSaveFileName = "I14_" + strGakunen + strClass + strClassNo + ".xls";;
							i14SaveFileList.add(strSaveFileName);
						}

						//印刷・保存用実行ファイル作成
						HSSFWorkbook 	workbook 	= cm.getMasterWorkBook(MacroFileName,intSaveFlg);
						HSSFSheet 		worksheet 	= null;
						HSSFCell 		workcell = null;
						HSSFRow 		workrow = null;

						//SessionIDフォルダ作成
						String strFilePath = cm.strCreateSessionFolder(SessionID);
						if( strFilePath .equals("") ){
							return false;
						}

						if( workbook == null ){
							throw new Exception("Personal ERROR : マクロ付きテンプレート読み込み失敗");
						}

						//パラメータファイル書き込み
						worksheet = workbook.getSheet(PrmSheetName);
						if( worksheet != null ){
							//保存実行指示フラグ
							workcell  = cm.setCell(  worksheet, workrow, workcell, 1, 1 );
							if( saveFlg == 6 ){
								//保存なし
								workcell.setCellValue(0);
							}
							else{
								//保存あり
								workcell.setCellValue(1);
							}
							//保存ブック名
							for( int fileindex = 0; fileindex < i11SaveFileList.size(); fileindex++ ){
								workcell  = cm.setCell(  worksheet, workrow, workcell, 2 , 1+fileindex );
								workcell.setCellValue((String)i11SaveFileList.get(fileindex));
							}
							for( int fileindex = 0; fileindex < i12SaveFileList.size(); fileindex++ ){
								workcell  = cm.setCell(  worksheet, workrow, workcell, 3 , 1+fileindex );
								workcell.setCellValue((String)i12SaveFileList.get(fileindex));
							}
							for( int fileindex = 0; fileindex < i13SaveFileList.size(); fileindex++ ){
								workcell  = cm.setCell(  worksheet, workrow, workcell, 4 , 1+fileindex );
								workcell.setCellValue((String)i13SaveFileList.get(fileindex));
							}
							for( int fileindex = 0; fileindex < i14SaveFileList.size(); fileindex++ ){
								workcell  = cm.setCell(  worksheet, workrow, workcell, 5 , 1+fileindex );
								workcell.setCellValue((String)i14SaveFileList.get(fileindex));
							}

							//印刷実行指示
							workcell  = cm.setCell(  worksheet, workrow, workcell, 6, 1 );
							workcell.setCellValue(1);
							//印刷実行ブック名
							for( int fileindex = 0; fileindex < i11OutFileList.size(); fileindex++ ){
								workcell  = cm.setCell(  worksheet, workrow, workcell, 7 , 1+fileindex );
								workcell.setCellValue((String)i11OutFileList.get(fileindex));
								tmpfilelist.add(i11OutFileList.get(fileindex));
							}
							for( int fileindex = 0; fileindex < i12OutFileList.size(); fileindex++ ){
								workcell  = cm.setCell(  worksheet, workrow, workcell, 8 , 1+fileindex );
								workcell.setCellValue((String)i12OutFileList.get(fileindex));
								tmpfilelist.add(i12OutFileList.get(fileindex));
							}
							for( int fileindex = 0; fileindex < i13OutFileList.size(); fileindex++ ){
								workcell  = cm.setCell(  worksheet, workrow, workcell, 9 , 1+fileindex );
								workcell.setCellValue((String)i13OutFileList.get(fileindex));
								tmpfilelist.add(i13OutFileList.get(fileindex));
							}
							for( int fileindex = 0; fileindex < i14OutFileList.size(); fileindex++ ){
								workcell  = cm.setCell(  worksheet, workrow, workcell, 10 , 1+fileindex );
								workcell.setCellValue((String)i14OutFileList.get(fileindex));
								tmpfilelist.add(i14OutFileList.get(fileindex));
							}
							//印刷実行ブック削除フラグ
							workcell  = cm.setCell(  worksheet, workrow, workcell, 11 , 1 );
							workcell.setCellValue(1);

							//現在日時取得
							Date date = new Date();
							SimpleDateFormat DF = new SimpleDateFormat("yyyyMMddHHmmssSS");
							String strDate = DF.format(date);
							//ファイル名設定
							String strSaveFile = strFilePath + "/" + strDate + ".xls";
							//ファイル保存
							FileOutputStream fout = null;
							try{
								fout = new FileOutputStream(strSaveFile);
								workbook.write(fout);
								fout.close();
							} catch(IOException e) {
								log.Err(strSaveFile,"Excelファイル保存エラー",e.toString());
								System.out.println(e.toString());
								return false;
							// 2019/10/16 QQ)Ooseto 共通テスト対応 ADD START
							} finally {
				                            if(fout != null) {
				                                try {
				                                    fout.close();
				                                } catch(IOException e) {
				                                    log.Err("","Excelファイル解放エラー",e.toString());
				                                }
				                            }
					                }
					                // 2019/10/16 QQ)Ooseto 共通テスト対応 ADD START
							outfilelist.add(strSaveFile);

						}
						else{
							throw new Exception("Personal ERROR : マクロ付きテンプレート書き込み失敗");
						}


					}
					break;
				default:	//印刷・一括印刷

					log.Ep("Personal","個人成績分析 帳票作成（印刷・一括印刷）開始","");

				// 2005.03.10 一括印刷時の印刷順序変更対応 START
					ArrayList i11ListP = i11Item.getI11List();
					I11ListBean i11ListBeanP = new I11ListBean();
					Iterator itrPersonalInfoP = i11ListP.iterator();
					String strSaveFileNameP = "";
					ArrayList SaveFileListP = new ArrayList();

					//引数として渡された選択生徒情報を、個人単位に分けて、各Ｅｘｃｅｌ帳票を出力する
	 				while(itrPersonalInfoP.hasNext())
	 				{
						I11Item i11PersonalItem = new I11Item();
						ArrayList i11PersonalList = new ArrayList();

						i11ListBean = (I11ListBean)itrPersonalInfoP.next();
						String strGakunen = i11ListBean.getStrGrade();
						String strClass = i11ListBean.getStrClass();
						String strClassNo = i11ListBean.getStrClassNum();

						//個人単位のオブジェクトに、元の選択生徒情報オブジェクトから値をセット
						i11PersonalList.add((I11ListBean)i11ListBean);

						i11PersonalItem.setStrBaseMshmei(i11Item.getStrBaseMshmei());				//模試名
						i11PersonalItem.setStrBaseMshDate( i11Item.getStrBaseMshDate());			//模試実施基準日
						i11PersonalItem.setStrBaseGakunen(i11Item.getStrBaseGakunen());				//模試対象学年
						i11PersonalItem.setStrMshmei(i11Item.getStrMshmei());						//模試短縮名
						i11PersonalItem.setIntBaseMshKyokaSr(i11Item.getIntBaseMshKyokaSr());		//模試教科数
						i11PersonalItem.setIntMenSheetFlg(i11Item.getIntMenSheetFlg());				//面談シートフラグ
						i11PersonalItem.setIntMenSheetSecuFlg(i11Item.getIntMenSheetSecuFlg());		//面談シートキュリティスタンプ
						i11PersonalItem.setIntKyoSheetFlg(i11Item.getIntKyoSheetFlg());				//教科分析シートフラグ
						i11PersonalItem.setIntKyoSheetSecuFlg(i11Item.getIntKyoSheetSecuFlg());		//教科分析シートセキュリティスタンプ
						i11PersonalItem.setIntSuiiFlg(i11Item.getIntSuiiFlg());						//科目別成績推移グラフフラグ
						i11PersonalItem.setIntSuiiSecuFlg(i11Item.getIntSuiiSecuFlg());				//科目別成績推移グラフセキュリティスタンプ
						i11PersonalItem.setIntSetsumonFlg(i11Item.getIntSetsumonFlg());				//設問別成績グラフフラグ
						i11PersonalItem.setIntSetsumonSecuFlg(i11Item.getIntSetsumonSecuFlg());		//設問別成績グラフセキュリティスタンプ
						i11PersonalItem.setIntSeisekiFlg(i11Item.getIntSeisekiFlg());				//連続個人成績表フラグ
						i11PersonalItem.setIntSeisekiSecuFlg(i11Item.getIntSeisekiSecuFlg());		//連続個人成績表セキュリティスタンプ
						i11PersonalItem.setIntSeisekiFormatFlg(i11Item.getIntSeisekiFormatFlg());	//連続個人成績表出力フォーマット
						i11PersonalItem.setIntHanteiFlg(i11Item.getIntHanteiFlg());					//志望大学判定一覧フラグ
						i11PersonalItem.setIntHanteiSecuFlg(i11Item.getIntHanteiSecuFlg());			//志望大学判定データセキュリティスタンプ
						i11PersonalItem.setIntScheduleFlg(i11Item.getIntScheduleFlg());				//受験大学スケジュールフラグ
						i11PersonalItem.setIntScheduleFormatFlg(i11Item.getIntScheduleFormatFlg());	//受験大学スケジュール出力フォーマット
						i11PersonalItem.setIntScheduleSecuFlg(i11Item.getIntScheduleSecuFlg());		//受験大学スケジュールセキュリティスタンプ
						//2005.05.25 Add
						i11PersonalItem.setIntMenSheetShubetsuFlg(i11Item.getIntMenSheetShubetsuFlg());	//面談シート出力種別フラグ
						i11PersonalItem.setIntKyoSheetShubetsuFlg(i11Item.getIntKyoSheetShubetsuFlg());	//教科分析シート出力種別フラグ
						i11PersonalItem.setIntSuiiShubetsuFlg(i11Item.getIntSuiiShubetsuFlg());			//科目別成績推移グラフ出力種別フラグ
						i11PersonalItem.setIntSetsumonShubetsuFlg(i11Item.getIntSetsumonShubetsuFlg());	//設問別成績推移グラフ出力種別フラグ
						i11PersonalItem.setIntSeisekiShubetsuFlg(i11Item.getIntSeisekiShubetsuFlg());	//連続個人成績表出力種別フラグ
						i11PersonalItem.setIntHanteiShubetsuFlg(i11Item.getIntHanteiShubetsuFlg());		//志望大学判定出力種別フラグ
						i11PersonalItem.setIntScheduleShubetsuFlg(i11Item.getIntScheduleShubetsuFlg());	//受験大学スケジュール出力種別フラグ
						//Add end
						i11PersonalItem.setI11List(i11PersonalList);								//データリスト

						//2005.03.10 印刷クラスに渡す、生徒情報を個人単位のものに変更(I11Item → i11PersonalItem)
						//面談シート(I11)
						///印刷フラグをチェック
						if( i11Item.getIntMenSheetFlg() != 0 ){
                                                    if (mendanFlg) {
                                                        i11PersonalItem.setIntMenSheetFlg(5);
                                                        I11 i11_1 = new I11();
                                                        bolRet = i11_1.i11(i11PersonalItem, outfilelist, intSaveFlg, SessionID, sheetLog);
                                                        if( bolRet != true ){
                                                                return false;
                                                        }

                                                        i11PersonalItem.setIntMenSheetFlg(2);
                                                        I11 i11_2 = new I11();
                                                        bolRet = i11_2.i11(i11PersonalItem, outfilelist, intSaveFlg, SessionID, sheetLog);
                                                        if( bolRet != true ){
                                                                return false;
                                                        }
                                                    } else {
                                                        //印刷対象なら、印刷処理を行う
                                                        I11 i11 = new I11();
                                                        bolRet = i11.i11(i11PersonalItem, outfilelist, intSaveFlg, SessionID, sheetLog);
                                                        if( bolRet != true ){
                                                                throw new Exception("I11 ERROR : 帳票作成失敗");
                                                        }
                                                    }


						}

						//2005.03.10 印刷クラスに渡す、生徒情報を個人単位のものに変更(I11Item → i11PersonalItem)
						//2004.11.09 I13→I12の順で出力する
						//連続個人成績表(I13)
						///印刷フラグをチェック
						if( i11Item.getIntSeisekiFlg() != 0 ){
							//印刷対象なら、印刷処理を行う
							I13 i13 = new I13();
							bolRet = i13.i13( i11PersonalItem, outfilelist, intSaveFlg, SessionID, sheetLog );
							if( bolRet != true ){
								throw new Exception("I13 ERROR : 帳票作成失敗");
							}
						}

						//2005.03.10 印刷クラスに渡す、生徒情報を個人単位のものに変更(I11Item → i11PersonalItem)
						//教科分析シート(I12)
						///印刷フラグをチェック
						if( i11Item.getIntKyoSheetFlg() != 0 || i11Item.getIntSuiiFlg() != 0 || i11Item.getIntSetsumonFlg() != 0){
							//印刷対象なら、印刷処理を行う
							I12 i12 = new I12();
							bolRet = i12.i12(i11PersonalItem, outfilelist, intSaveFlg, SessionID, sheetLog);
							if( bolRet != true ){
								throw new Exception("I12 ERROR : 帳票作成失敗");
							}
						}

						//2005.03.10 印刷クラスに渡す、生徒情報を個人単位のものに変更(I11Item → i11PersonalItem)
						//志望校判定(I14)
						///印刷フラグをチェック
						if( i11Item.getIntHanteiFlg() != 0 ){
							//印刷対象なら、印刷処理を行う
							I14 i14 = new I14();
							bolRet = i14.i14( i11PersonalItem, outfilelist, intSaveFlg, SessionID, sheetLog );
							if( bolRet != true ){
								throw new Exception("I14 ERROR : 帳票作成失敗");
							}
						}

						//2005.03.10 印刷クラスに渡す、生徒情報を個人単位のものに変更(I11Item → i11PersonalItem)
						//受験大学スケジュール(I15)
						///印刷フラグをチェック
						if( i11Item.getIntScheduleFlg() != 0 ){
							//印刷対象なら、印刷処理を行う
							I15 i15 = new I15();
							bolRet = i15.i15( i11PersonalItem, outfilelist, intSaveFlg, SessionID, sheetLog );
							if( bolRet != true ){
								throw new Exception("I15 ERROR : 帳票作成失敗");
							}
						}


	 				}
				// 2005.03.10 一括印刷時の印刷順序変更対応 END

	 		}

			log.Ep("Personal","個人成績分析 帳票作成終了","");
			return true;
	 	}
	 	catch( Exception e){
			log.Err("09P00","帳票作成エラー",e.toString());
			return false;
	 	}
	 }

}

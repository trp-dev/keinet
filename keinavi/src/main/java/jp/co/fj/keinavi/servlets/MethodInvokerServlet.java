package jp.co.fj.keinavi.servlets;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URLEncoder;
import java.sql.Connection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.fjh.beans.DefaultBean;
import com.fjh.forms.ActionForm;

import jp.co.fj.keinavi.util.KNUtil;

/**
 * 
 * パラメータ「operation」で指定されたサブクラスのメソッドを起動するサーブレットです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public abstract class MethodInvokerServlet extends DefaultHttpServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = 4727647358429847829L;

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	public final void execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		/* メソッド名を決める */
		String name = StringUtils.defaultString(request.getParameter("operation"),
				"index");

		if (getScreenId().equals(getForward(request))) {
			/* 転送先がこの画面ならメソッドを起動する */
			try {
				Method method = getMethod(name);
				prepare(request, response, name);
				ActionForm form = createActionForm(request, method);
				method.invoke(this, form == null ? new Object[] { request,
						response } : new Object[] { request, response, form });
			} catch (Exception e) {
				e.printStackTrace();
				throw new ServletException(e);
			}
		} else {
			/* 転送先が不明ならDISPATCHERに飛ばす */
			dispatch(request, response);
		}
	}

	/**
	 * メソッド実行前の事前処理を行います。
	 */
	protected abstract void prepare(HttpServletRequest request,
			HttpServletResponse response, String methodName) throws Exception;

	/**
	 * 画面IDをサーブレット名から抽出します。
	 */
	private String getScreenId() {
		String name = getSimpleName();
		return name.substring(0, name.indexOf("Servlet")).toLowerCase();
	}

	/**
	 * このサーブレットの名前を返します。
	 */
	private String getSimpleName() {
		String className = getClass().getName();
		return className.substring(className.lastIndexOf('.') + 1);
	}

	/**
	 * メソッド名と一致するpublicメソッドを返します。
	 */
	private Method getMethod(String name) {

		Method[] methods = getClass().getMethods();
		for (int i = 0; i < methods.length; i++) {
			Method method = methods[i];
			if (method.getName().equals(name)
					&& Modifier.isPublic(method.getModifiers())) {
				return method;
			}
		}

		throw new IllegalArgumentException("サーブレットに未定義のメソッドです。" + name);
	}

	/**
	 * ActionFormを生成します。
	 */
	private ActionForm createActionForm(HttpServletRequest request,
			Method method) throws ServletException {

		if (method.getParameterTypes().length <= 2) {
			return null;
		}

		String className = method.getParameterTypes()[2].getName();
		ActionForm form = getActionForm(request, className);

		/* リクエストスコープにセットする */
		request.setAttribute(
				className.substring(className.lastIndexOf('.') + 1), form);

		return form;
	}

	/**
	 * 指定した画面IDにリダイレクトします。
	 */
	protected final void redirect(HttpServletRequest request,
			HttpServletResponse response, String id) throws IOException {
		redirect(request, response, id, null);
	}

	/**
	 * 指定した画面IDに任意のパラメータを付加してリダイレクトします。
	 */
	protected final void redirect(HttpServletRequest request,
			HttpServletResponse response, String id, Map param)
			throws IOException {

		StringBuffer sb = new StringBuffer(128);
		sb.append(response.encodeURL(KNUtil.getContextPath(request) + "/"
				+ getSimpleName()));
		sb.append("?forward=");
		sb.append(id);
		sb.append("&backward=");
		sb.append(getScreenId());

		/* 任意のパラメータを付加する */
		if (param != null) {
			for (Iterator ite = param.entrySet().iterator(); ite.hasNext();) {
				Entry entry = (Entry) ite.next();
				sb.append("&");
				sb.append(encode(entry.getKey().toString()));
				sb.append("=");
				sb.append(encode(entry.getValue().toString()));
			}
		}

		response.sendRedirect(sb.toString());
	}

	/**
	 * 文字列をURLエンコードします。
	 */
	private String encode(String value) throws UnsupportedEncodingException {

		if (value == null) {
			return "";
		}

		return URLEncoder.encode(value, "Windows-31J");
	}

	/**
	 * 指定したBeanを実行します。
	 */
	protected final DefaultBean executeBean(HttpServletRequest request,
			DefaultBean bean) throws Exception {

		Connection con = null;
		try {
			con = getConnectionPool(request);
			bean.setConnection(null, con);
			bean.execute();
			if (bean.getErrorCount() > 0) {
				con.rollback();
			} else {
				con.commit();
			}
			return bean;
		} catch (Exception e) {
			con.rollback();
			throw e;
		} finally {
			releaseConnectionPool(request, con);
		}
	}

}

package jp.co.fj.keinavi.forms.helpdesk;

import com.fjh.forms.ActionForm;



/**
 * 2004.10.05 	Yoshimoto KAWAI
 * 				新規作成
 * 
 * <2010年度マーク高２模試対応>
 * 2011.02.23	Tomohisa YAMADA
 * 				アップロード機能
 * 
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 */
public class HD002Form extends ActionForm {

	private String menu         = null;	// 利用メニュー
	private String schoolCode   = null;	// 学校コード
	private String buildingCode = null;	// 校舎部門コード
	private String businessCode = null;	// 営業部門コード
	private String salesCode = null;	// 営業部門コード（高校作成ファイルダウンロード用）
	 

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {


	}

	/**
	 * @return
	 */
	public String getMenu() {
		return menu;
	}

	/**
	 * @param string
	 */
	public void setMenu(String string) {
		menu = string;
	}

	/**
	 * @return
	 */
	public String getSchoolCode() {
		return schoolCode;
	}

	/**
	 * @param string
	 */
	public void setSchoolCode(String string) {
		schoolCode = string;
	}

	/**
	 * @return
	 */
	public String getBuildingCode() {
		return buildingCode;
	}

	/**
	 * @param string
	 */
	public void setBuildingCode(String string) {
		buildingCode = string;
	}

	/**
	 * @return
	 */
	public String getBusinessCode() {
		return businessCode;
	}

	/**
	 * @param string
	 */
	public void setBusinessCode(String string) {
		businessCode = string;
	}


	/**
	 * @return
	 */
	public String getSalesCode() {
		return salesCode;
	}

	/**
	 * @param string
	 */
	public void setSalesCode(String salesCode) {
		this.salesCode = salesCode;
	}

}

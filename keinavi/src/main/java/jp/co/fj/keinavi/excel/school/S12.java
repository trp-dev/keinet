/**
 * 校内成績分析−校内成績　偏差値分布
 * 出力する帳票の判断
 * 作成日: 2004/07/01
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.school.S12Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

public class S12 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean s12( S12Item s12Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {

		KNLog log = KNLog.getInstance(null,null,null);

		try{
			int ret = 0;
			//S12Itemから各帳票を出力
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD START
//// 2019/09/30 QQ) 共通テスト対応 MOD START
////			if (( s12Item.getIntHyouFlg()==0 )&&( s12Item.getIntBnpGraphFlg()==0 )&&
////				( s12Item.getIntNinzuGraphFlg()==0 )&&( s12Item.getIntKoseiGraphFlg()==0 )) {
//			if ( (s12Item.getIntHyouFlg() == 0) &&
//				(s12Item.getIntBnpGraphFlg() == 0) &&
//				(s12Item.getIntNinzuGraphFlg() == 0) &&
//				(s12Item.getIntKoseiGraphFlg() == 0) &&
//				(s12Item.getIntCheckBoxFlg() == 0)) {
//// 2019/09/30 QQ) 共通テスト対応 MOD END
			if (( s12Item.getIntHyouFlg()==0 )&&( s12Item.getIntBnpGraphFlg()==0 )&&
				( s12Item.getIntNinzuGraphFlg()==0 )&&( s12Item.getIntKoseiGraphFlg()==0 )) {
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD END
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
			    //throw new Exception("S12 ERROR : フラグ異常");
			    throw new IllegalStateException("S12 ERROR : フラグ異常");
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			}
			if ( s12Item.getIntHyouFlg()==1 && s12Item.getIntKoseihiFlg()==2 ) {
				log.Ep("S12_01","S12_01帳票作成開始","");
				S12_01 exceledit = new S12_01();
				ret = exceledit.s12_01EditExcel( s12Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S12_01","帳票作成エラー","");
					return false;
				}
				log.Ep("S12_01","S12_01帳票作成終了","");
				sheetLog.add("S12_01");
			}
			if ( s12Item.getIntHyouFlg()==1 && s12Item.getIntKoseihiFlg()==1 ) {
				log.Ep("S12_02","S12_02帳票作成開始","");
				S12_02 exceledit = new S12_02();
				ret = exceledit.s12_02EditExcel( s12Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S12_02","帳票作成エラー","");
					return false;
				}
				log.Ep("S12_02","S12_02帳票作成終了","");
				sheetLog.add("S12_02");
			}
			if ( s12Item.getIntBnpGraphFlg()==1 && s12Item.getIntAxisFlg()==1 ) {
				log.Ep("S12_03","S12_03帳票作成開始","");
				S12_03 exceledit = new S12_03();
				ret = exceledit.s12_03EditExcel( s12Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S12_03","帳票作成エラー","");
					return false;
				}
				log.Ep("S12_03","S12_03帳票作成終了","");
				sheetLog.add("S12_03");
			}
			if ( s12Item.getIntBnpGraphFlg()==1 && s12Item.getIntAxisFlg()==2 ) {
				log.Ep("S12_04","S12_04帳票作成開始","");
				S12_04 exceledit = new S12_04();
				ret = exceledit.s12_04EditExcel( s12Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S12_04","帳票作成エラー","");
					return false;
				}
				log.Ep("S12_04","S12_04帳票作成終了","");
				sheetLog.add("S12_04");
			}
			if ( s12Item.getIntNinzuGraphFlg()==1 && s12Item.getIntNinzuPitchFlg()==1 ) {
				log.Ep("S12_05","S12_05帳票作成開始","");
				S12_05 exceledit = new S12_05();
				ret = exceledit.s12_05EditExcel( s12Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S12_05","帳票作成エラー","");
					return false;
				}
				log.Ep("S12_05","S12_05帳票作成終了","");
				sheetLog.add("S12_05");
			}
			if ( s12Item.getIntNinzuGraphFlg()==1 && s12Item.getIntNinzuPitchFlg()==2 ) {
				log.Ep("S12_06","S12_06","");
				S12_06 exceledit = new S12_06();
				ret = exceledit.s12_06EditExcel( s12Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S12_06","帳票作成エラー","");
					return false;
				}
				log.Ep("S12_06","S12_06帳票作成終了","");
				sheetLog.add("S12_06");
			}
			if ( s12Item.getIntNinzuGraphFlg()==1 && s12Item.getIntNinzuPitchFlg()==3 ) {
				log.Ep("S12_07","S12_07帳票作成開始","");
				S12_07 exceledit = new S12_07();
				ret = exceledit.s12_07EditExcel( s12Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S12_07","帳票作成エラー","");
					return false;
				}
				log.Ep("S12_07","S12_07帳票作成終了","");
				sheetLog.add("S12_07");
			}
			if ( s12Item.getIntKoseiGraphFlg()==1 && s12Item.getIntKoseiPitchFlg()==1 ) {
				log.Ep("S12_08","SS12_08帳票作成開始","");
				S12_08 exceledit = new S12_08();
				ret = exceledit.s12_08EditExcel( s12Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S12_08","帳票作成エラー","");
					return false;
				}
				log.Ep("S12_08","S12_08帳票作成終了","");
				sheetLog.add("S12_08");
			}
			if ( s12Item.getIntKoseiGraphFlg()==1 && s12Item.getIntKoseiPitchFlg()==2 ) {
				log.Ep("S12_09","S12_09帳票作成開始","");
				S12_09 exceledit = new S12_09();
				ret = exceledit.s12_09EditExcel( s12Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S12_09","帳票作成エラー","");
					return false;
				}
				log.Ep("S12_09","S12_09帳票作成終了","");
				sheetLog.add("S12_09");
			}
			if ( s12Item.getIntKoseiGraphFlg()==1 && s12Item.getIntKoseiPitchFlg()==3 ) {
				log.Ep("S12_10","S12_10帳票作成開始","");
				S12_10 exceledit = new S12_10();
				ret = exceledit.s12_10EditExcel( s12Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S12_10","帳票作成エラー","");
					return false;
				}
				log.Ep("S12_10","S12_10帳票作成終了","");
				sheetLog.add("S12_10");
			}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//			// 2019/09/30 QQ) 共通テスト対応 ADD START
//			if (s12Item.getIntCheckBoxFlg() == 1) {
//				log.Ep("S12_11","S12_11帳票作成開始","");
//				S12_11 exceledit = new S12_11();
//				ret = exceledit.s12_11EditExcel( s12Item, outfilelist, saveFlg, UserID );
//				if (ret!=0) {
//					log.Err("0"+ret+"S12_11","帳票作成エラー","");
//					return false;
//				}
//				log.Ep("S12_11","S12_11帳票作成終了","");
//				sheetLog.add("S12_11");
//			}
//			// 2019/09/30 QQ) 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		} catch(Exception e) {
			log.Err("99S12","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
package jp.co.fj.keinavi.forms;

import com.fjh.forms.ActionForm;

/**
 * 基本アクションフォーム
 * 
 * 2005.04.05	Yoshimoto KAWAI - Totec
 * 				forwardとbackwardを追加
 * 
 * @author kawai
 * 
 */
public abstract class BaseForm extends ActionForm {
	
	private String targetYear; // 対象年度
	private String targetExam; // 対象模試
	private String scrollX; // Submit時のスクロールX座標
	private String scrollY; // Submit時のスクロールY座標
	private String forward; // 転送先画面ID
	private String backward; // 転送元画面ID
	
	/**
	 * @return
	 */
	public String getTargetExam() {
		return targetExam;
	}

	/**
	 * @return
	 */
	public String getTargetYear() {
		return targetYear;
	}

	/**
	 * @param string
	 */
	public void setTargetExam(String string) {
		targetExam = string;
	}

	/**
	 * @param string
	 */
	public void setTargetYear(String string) {
		targetYear = string;
	}

	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;
	}

	public String getBackward() {
		return this.backward;
	}
	public void setBackward(String backward) {
		this.backward = backward;
	}
	public String getForward() {
		return this.forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
}

/*
 * 作成日: 2004/08/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * @author Administrator
 *
 *	2005.3.2	T.Yamada	[1]I205Servletの[2]を適用する
 *  2005.4.26	K.Kondo		[2]画面スクロール座標の追加
 *  2005.6.23   T.Yamada    [3]judgeNoの初期値を1とする
 */
public class I205Form extends ActionForm{

	private String targetPersonId;
	private String univValue4Detail;
	private String dispSubject;
	private String examPattern;
	
	/**
	 * [1] add
	 * 複数ある判定リストのどちらを選ぶか
	 */
	//private String judgeNo;//[3] del
	private String judgeNo = "1";//[3] add

	String uniqueKey;//大学コード
	//from banzai
	String actionValue;
	String selectBranch;//選択された枝番 // 2003.12.12 nakao
			
	private String scrollX;//[2] add サブミット前、表示座標Ｘ
	private String scrollY;//[2] add サブミット前、表示座標Ｙ
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.individual.ICommonForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ
		
	}
	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @return
	 */
	public String getUnivValue4Detail() {
		return univValue4Detail;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @param string
	 */
	public void setUnivValue4Detail(String string) {
		univValue4Detail = string;
	}

	/**
	 * @return
	 */
	public String getDispSubject() {
		return dispSubject;
	}

	/**
	 * @return
	 */
	public String getExamPattern() {
		return examPattern;
	}

	/**
	 * @param string
	 */
	public void setDispSubject(String string) {
		dispSubject = string;
	}

	/**
	 * @param string
	 */
	public void setExamPattern(String string) {
		examPattern = string;
	}
	/**
	 * @return
	 */
	public String getActionValue() {
		return actionValue;
	}

	/**
	 * @return
	 */
	public String getSelectBranch() {
		return selectBranch;
	}

	/**
	 * @param string
	 */
	public void setActionValue(String string) {
		actionValue = string;
	}

	/**
	 * @param string
	 */
	public void setSelectBranch(String string) {
		selectBranch = string;
	}

	/**
	 * @return
	 */
	public String getUniqueKey() {
		return uniqueKey;
	}

	/**
	 * @param string
	 */
	public void setUniqueKey(String string) {
		uniqueKey = string;
	}

	/**
	 * @return
	 */
	public String getJudgeNo() {
		return judgeNo;
	}

	/**
	 * @param string
	 */
	public void setJudgeNo(String string) {
		judgeNo = string;
	}
	
	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[2] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[2] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[2] add
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[2] add
	}
}
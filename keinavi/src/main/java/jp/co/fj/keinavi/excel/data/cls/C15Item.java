package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−志望大学別個人評価データクラス
 * 作成日: 2004/08/30
 * @author	Ito.Y
 */
public class C15Item{
	//学校名
	private String strGakkomei = "";
	//対象年度
	private String strYear = "";
	//対象模試　模試名
	private String strMshmei = "";
	//対象模試　模試実施基準日
	private String strMshDate = "";
	//対象コード
	private String strMshCd = "";
	//表示対象フラグ
	private int intDispFlg = 0;
	//セキュリティスタンプ
	private int intSecuFlg = 0;
	//データリスト
	private ArrayList c15List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;


	/*----------*/
	/* Get      */
	/*----------*/
	
	public ArrayList getC15List() {
		return this.c15List;
	}
	public int getIntDispFlg() {
		return this.intDispFlg;
	}
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrYear() {
		return this.strYear;
	}
	public String getStrMshCd() {
		return strMshCd;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	
	public void setC15List(ArrayList c15List) {
		this.c15List = c15List;
	}
	public void setIntDispFlg(int intDispFlg) {
		this.intDispFlg = intDispFlg;
	}
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrYear(String strYear) {
		this.strYear = strYear;
	}
	public void setStrMshCd(String string) {
		strMshCd = string;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}
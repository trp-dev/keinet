package jp.co.fj.keinavi.excel.business;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.cm.CefrCommon;
import jp.co.fj.keinavi.excel.data.business.B42CefrItem;
import jp.co.fj.keinavi.excel.data.business.B42CefrListBean;
import jp.co.fj.keinavi.excel.data.business.B42Item;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * 校内成績分析−高校間比較−偏差値分布 英語CEFR試験別取得状況
 *      Excelファイル編集
 * 作成日: 2019/09/30
 * @author QQ)
 *
 */
public class B42_07 {

    private int noerror = 0;    // 正常終了
    private int errfread = 1;   // ファイルreadエラー
    private int errfwrite = 2;  // ファイルwriteエラー
    private int errfdata = 3;   // データ設定エラー

    private CM cm = new CM();   // 共通関数用クラス インスタンス
    private CefrCommon cefrCm = null;

    private String masterfile = "B42_07";               // ファイル名

    private static final int MIN_ROW_COUNT = 20;        // 最小行数
    private static final int MAX_ROW_COUNT = 79;        // 最大行数

    private int intTotalPageCnt = 0;                        //１試験の全ページ数
    private int intDetailCnt = 0;                           //１試験の明細数
    private int intCefrLvlCnt = 0;                          //CEFRレベル数

    private int intSecuFlg = 0; // セキュリティスタンプフラグ
    private String strMshmei = ""; // 模試名
    private String strMshDate = ""; // 模試実施基準日

    // Enum(Excel座標 列,行)
    public enum CURSOL {
            SECURITY_STAMP(29, 0),
            TEMPLATE_ST_CELL(0, 0),                     //templateのコピー開始セル(20行の場合)
            TEMPLATE_EN_CELL(15, 22),                   //templateのコピー終了セル(20行の場合)
            HEADER_ST_CELL(0, 0),                       //templateのヘッダのみ開始セル
            HEADER_EN_CELL(15, 2),                      //templateのヘッダのみ終了セル
            PASTE_LEFT( 0, 5),                          //左側先頭貼り付け位置
            PASTE_RIGHT( 16, 5),                        //右側先頭貼り付け位置
            PASTE_SHIFT(-1, 23),                        //貼り付けシフト(20行の場合)
            DATA_LEFT(1, 8),                            //左側データ出力位置
            DATA_RIGHT(16, 8),
            HEADER_SIZE(1, 2),
            TERMINAL(0,0);

            private final int col;
            private final int row;

            private CURSOL(int col, int row) {
                    this.col = col;
                    this.row = row;
            }

            public int getCol() {
                    return this.col;
            }

            public int getRow() {
                    return this.row;
            }

    }

    public int b42_07EditExcel(B42Item b42Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
        KNLog log = KNLog.getInstance(null,null,null);
        log.Ep("B42_07","B42_07帳票作成開始","");

        HSSFWorkbook workbook = null;

        // マスタExcel読み込み
        workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
        if( workbook==null ){
                return errfread;
        }

        try {

                // Excel認定試験CEFR取得状況共通関数インスタンス生成
                cefrCm = new CefrCommon(workbook);

                // シート内最大行数
                cefrCm.setMaxRowCount(MAX_ROW_COUNT);
                // テンプレートシート指定
                cefrCm.setTemplateWorkbook();

                // セキュリティスタンプフラグ
                this.intSecuFlg = b42Item.getIntSecuFlg();
                // 模試名
                this.strMshmei = b42Item.getStrMshmei();
                // 模試実施基準日
                this.strMshDate = b42Item.getStrMshDate();
                // 自校受験者がいる試験を対象にするフラグ
                cefrCm.setIntTargetCheckBoxFlg(b42Item.getIntTargetCheckBoxFlg());


                // 新ページ作成処理
                this.newCreateSheet(cm);


                B42CefrItem b42CefrMain = b42Item.getIteB42Cefr();              // B42_07情報
                ArrayList allList = b42CefrMain.getB42AllList();                // 全試験データ
                ArrayList eachList = b42CefrMain.getB42EachList();

                if (allList.size() > 0 || eachList.size() > 0) {

                        // テンプレートテーブルに学校名/県名/CEFR基準をセットしておく
                        this.setTemplateNames(cm, b42CefrMain);

                        // 全試験 情報出力
                        this.setAllExamData(cm, allList);

                        // 各試験 情報出力
                        this.setEachExamData(cm, b42CefrMain);

                        // テンプレートシート削除
                        if( workbook.getSheetIndex(CefrCommon.templateSheetName) >= 0){
                                workbook.removeSheetAt(workbook.getSheetIndex(CefrCommon.templateSheetName));
                        }



                        //2019/10/03 QQ)Oosaki アクティブシートを選択 ADD START
                        workbook.getSheetAt(1).setSelected(true);
                        //2019/10/03 QQ)Oosaki アクティブシートを選択 ADD END

                        // Excelファイル保存
                        boolean bolRet = false;
                        bolRet = cm.bolFileSave(intSaveFlg, outfilelist, cefrCm.getWorkbook(), UserID, 0, masterfile, cefrCm.getMaxSheetIndex());

                        if( bolRet == false ){
                                return errfwrite;
                        }
                }

        } catch(Exception e) {
                log.Err("B42_07","データセットエラー",e.toString());
                e.printStackTrace();
                return errfdata;
        }

        log.Ep("B42_07","B42_07帳票作成終了","");
        return noerror;
    }
    /**
     * <DL>
     * <DT>改ページ処理
     * </DL>
     * @param cm Excel共通関数インスタンス
     */
    private void newCreateSheet(CM cm) {

            cefrCm.initSheetSetting();

            // ヘッダ右側に帳票作成日時を表示する
            cm.setHeader(cefrCm.getWorkbook(), cefrCm.getWorkSheet());

            // セキュリティスタンプセット
            cefrCm.setCurCol(CURSOL.SECURITY_STAMP.getCol());
            cefrCm.setCurRow(CURSOL.SECURITY_STAMP.getRow());
            String secuFlg = cm.setSecurity(cefrCm.getWorkbook(), cefrCm.getWorkSheet(), intSecuFlg, cefrCm.getCurCol(), cefrCm.getCurCol() + 1);
            cefrCm.setCellValueAddRowCountUp(cm, secuFlg);

            // 対象模試セット
            cefrCm.setCurCol(0);
            cefrCm.setCurRow(3);
            String moshi = cm.setTaisyouMoshi(strMshDate); // 模試月取得
            cefrCm.setCellValueAddRowCountUp(cm, cm.getTargetExamLabel() + "：" + cm.toString(strMshmei) + moshi);

            // 表示対象セット
            String cefrMySchoolOnlyLabel = "";
            if (cefrCm.checkIntTargetCheckBoxFlg()) {
                    cefrMySchoolOnlyLabel = cm.getCefrSelectedSchoolOnlyLabel();
            }
            cefrCm.setCellValueAddRowCountUp(cm, cm.getCefrTargetLabel() + "：" + cefrMySchoolOnlyLabel);
    }

    /**
     * ExcelのAcitiveSheetのセルに値を設定する
     * @param cm        Excel共通関数インスタンス
     * @param col       出力列Index
     * @param row       出力行Index
     * @param val       出力文字列
     */
    private void setCellValue(CM cm, int col, int row, String val) {

        // カーソル位置指定
        cefrCm.setCurCol(col);
        cefrCm.setCurRow(row);

        cefrCm.setCellValueAddRowCountUp(cm, val);

    }

    /**
     * ExcelのAcitiveSheetのセルに値を設定する
     * @param cm        Excel共通関数インスタンス
     * @param col       出力列Index
     * @param row       出力行Index
     * @param val       出力値
     */
    private void setCellValue(CM cm, int col, int row, int val) {

        // カーソル位置指定
        cefrCm.setCurCol(col);
        cefrCm.setCurRow(row);

        cefrCm.setCellValueAddRowCountUp(cm, val);

    }
    /**
     * Excelの指定Sheetのセルに値を設定する
     * @param sheet     出力Sheet
     * @param col       出力列Index
     * @param row       出力行Index
     * @param val       出力文字列
     */
    private void setCellValue(HSSFSheet sheet, int col, int row, String val) {
        HSSFRow workRow = sheet.getRow(row);
        HSSFCell workCell = workRow.getCell((short)col);
        workCell.setEncoding(HSSFCell.ENCODING_UTF_16);

        workCell.setCellValue(val);
    }
    /**
     * テンプレートテーブルに学校名/県名/CEFR基準をセットしておく
     * @param cm                Excel共通関数インスタンス
     * @param b42CefrMain       データクラス
     */
    private void setTemplateNames(CM cm, B42CefrItem b42CefrMain) {

        // テンプレートシート指定
        HSSFSheet tempSheet = cefrCm.getTemplateWorkbook();

        this.intDetailCnt = b42CefrMain.getIntDetailCount();

        //明細数が20件超えなら、明細行をコピーしておく
        this.intTotalPageCnt = (int)((intDetailCnt - 1) / MAX_ROW_COUNT) + 1;
        if (intDetailCnt > MIN_ROW_COUNT) {

            // テンプレートカーソル位置設定
            int cpStCol = 0;
            int cpEnCol = CURSOL.TEMPLATE_EN_CELL.getCol() + 1;
            int cpRow = CURSOL.HEADER_EN_CELL.getRow() + 1;
            int dstStRow = CURSOL.TEMPLATE_EN_CELL.getRow() + 1;
            int dstEnRow = dstStRow + intDetailCnt - MIN_ROW_COUNT;

            // 表テンプレート行コピー
            this.copyRowToRows(tempSheet, cpStCol, cpEnCol, cpRow, dstStRow, dstEnRow);
        }

        //CEFR基準を設定しておく
        setCellValue(tempSheet, 1, 2, "延人数");
        ArrayList<String> cefrLvl = b42CefrMain.getCefrLevelList();
        intCefrLvlCnt = cefrLvl.size();
        for (int i = 1; i <= 7; i++) {
            //形式がずれるのでスペースを設定しておく
            setCellValue(tempSheet, (i * 2), 2, " ");
        }
        for (int i = 1; i < intCefrLvlCnt; i++) {                              //合計を除いた先頭から
            String val = cefrLvl.get(i);
            setCellValue(tempSheet, (i * 2), 2, val);
        }

        //高校名などを設定しておく
        ArrayList<B42CefrListBean> allList = b42CefrMain.getB42AllList();
        int size = allList.size();
        for (int i=0; i < size; i++) {
            String val = allList.get(i).getStrName();
            setCellValue(tempSheet, 0, 3 + i, val);
        }
    }

    /**
     * テンプレートテーブルのコピー
     * @param cm                Excel共通関数インスタンス
     * @param isLeft            出力位置 true:左側（学校名あり） false:右側（学校名なし）
     * @param pasteRow          貼り付け位置Row
     * @param page              出力ページ
     */
    private void copyTemplateTable(CM cm, boolean isLeft, int pasteRow, int page) {
        // テンプレートカーソル位置設定
        int stCol = CURSOL.TEMPLATE_ST_CELL.getCol();
        int stRow = CURSOL.TEMPLATE_ST_CELL.getRow();
        int enCol = CURSOL.TEMPLATE_EN_CELL.getCol();
        int enRow = CURSOL.TEMPLATE_EN_CELL.getRow();

        int pasteCol = isLeft ? CURSOL.PASTE_LEFT.getCol() : CURSOL.PASTE_RIGHT.getCol();

        //右側は高校名なし
        if (isLeft == false) {
            stCol = CURSOL.TEMPLATE_ST_CELL.getCol() + CURSOL.HEADER_SIZE.getCol();
        }



        // 表テンプレートコピー
        if (this.intDetailCnt <= MIN_ROW_COUNT) {
            cm.copyCellArea(cefrCm.getWorkSheet(), cefrCm.getTemplateWorkbook(), stCol, stRow, enCol, enRow, pasteCol, pasteRow);
        }
        else {
            if (page == 1) {
                stRow = CURSOL.TEMPLATE_ST_CELL.getRow();
                if (this.intDetailCnt >= MAX_ROW_COUNT) {
                    enRow = MAX_ROW_COUNT + CURSOL.HEADER_SIZE.getRow();
                }
                else {
                    enRow = this.intDetailCnt + CURSOL.HEADER_SIZE.getRow();
                }
                cm.copyCellArea(cefrCm.getWorkSheet(), cefrCm.getTemplateWorkbook(), stCol, stRow, enCol, enRow, pasteCol, pasteRow);
            }
            else {
                //ヘッダを先にコピーする
                int hedStCol = CURSOL.HEADER_ST_CELL.getCol();
                int hedStRow = CURSOL.HEADER_ST_CELL.getRow();
                int hedEnCol = CURSOL.HEADER_EN_CELL.getCol();
                int hedEnRow = CURSOL.HEADER_EN_CELL.getRow();
                if (isLeft == false) {
                    hedStCol = CURSOL.HEADER_ST_CELL.getCol() + CURSOL.HEADER_SIZE.getCol();
                }
                cm.copyCellArea(cefrCm.getWorkSheet(), cefrCm.getTemplateWorkbook(), hedStCol, hedStRow, hedEnCol, hedEnRow, pasteCol, pasteRow);

                pasteRow += CURSOL.HEADER_SIZE.getRow() + 1;

                //明細行開始位置を算出
                stRow = CURSOL.HEADER_EN_CELL.getRow() + (page - 1) * MAX_ROW_COUNT + 1;
                if (page < this.intTotalPageCnt) {
                    enRow = stRow + MAX_ROW_COUNT - 1;
                }
                else {
                    enRow = stRow + this.intDetailCnt -  (page - 1) * MAX_ROW_COUNT + 1;
                }
                cm.copyCellArea(cefrCm.getWorkSheet(), cefrCm.getTemplateWorkbook(), stCol, stRow, enCol, enRow, pasteCol, pasteRow);
            }
        }
    }

    /**
     * 同シート間の行コピー
     * @param sheet
     * @param cpStCol   コピー元開始列
     * @param cpEnCol   コピー元終了列
     * @param cpRow     コピー元行
     * @param dstStRow  コピー先開始行
     * @param dstEnRow  コピー先終了行
     */
    private void copyRowToRows(HSSFSheet sheet, int cpStCol, int cpEnCol, int cpRow, int dstStRow, int dstEnRow) {

        for (int sCol = cpStCol; sCol < cpEnCol; sCol++) {

            //コピー元セル
            HSSFCell srcCell = getCell(sheet, (short) sCol, cpRow);

            for (int dRow = dstStRow; dRow < dstEnRow; dRow++) {
                //コピー先セル
                HSSFCell dstCell = getCell(sheet, (short) sCol, dRow);

                // 値を取得
                if (StringUtils.isNotEmpty(srcCell.getStringCellValue())) {
                    dstCell.setCellValue(srcCell.getStringCellValue());
                }
                // スタイルを取得
                dstCell.setCellStyle(srcCell.getCellStyle());

            }
        }
    }

    /**
     * Cellを返す。
     * @param workSheet 作業シート
     * @param column 列
     * @param row 行
     * @return Cell
     */
    private HSSFCell getCell(HSSFSheet workSheet, short column, int row) {
            HSSFRow rowObj = this.getRow(row, workSheet);
            return this.getCell(rowObj, column);
    }

    /**
     * Rowを返す。
     * @param rowIndex 行数
     * @param sheet 作業シート
     * @return Row
     */
    private HSSFRow getRow(int rowIndex, HSSFSheet sheet) {
            HSSFRow row = sheet.getRow(rowIndex);
            if (row == null) {
                    row = sheet.createRow(rowIndex);
            }
            return row;
    }

    /**
     * Cellを返す。
     * @param rowIndex 行数
     * @param sheet 作業シート
     * @return Cell
     */
    private HSSFCell getCell(HSSFRow row, short columnIndex) {
            HSSFCell cell = row.getCell(columnIndex);
            if (cell == null) {
                    cell = row.createCell(columnIndex);
            }
            return cell;
    }

    /**
     *テーブルデータ情報セット
     * @param cm                Excel共通関数インスタンス
     * @param dataList          出力対象データリスト
     * @param page              出力ページ
     * @param col               出力テーブル列位置
     * @param row               出力テーブル行位置
     * @param startDataIdx      出力データの開始位置
     */
    private void setTableData(CM cm, ArrayList<B42CefrListBean> dataList,int page, int col, int row, int startDataIdx) {

        //値セット
        int cnt = 0;
        int iVal = 0;
        float fVal = 0;
        int lastDataCnt = page * MAX_ROW_COUNT;
        if (page == this.intTotalPageCnt) {
            //最終ページ
            lastDataCnt = this.intDetailCnt - (page - 1) * MAX_ROW_COUNT;
        }

        for(int i=startDataIdx; i<startDataIdx+lastDataCnt; i++) {

            int outputRow = row + cnt;
                int[] numbersList = dataList.get(i).getIntNumbersList();
                float[] pratioList = dataList.get(i).getFloCompRatioList();

                //実人数or延人数(index=0は合計)
                iVal = dataList.get(i).getIntNumbers(0);
                if (iVal != 0) {
                    setCellValue(cm, col,  outputRow, iVal);
                }

                for (int lvl=1; lvl<intCefrLvlCnt; lvl++) {
                    iVal = numbersList[lvl];
                    fVal = pratioList[lvl];

                    if (iVal != 0) {
                        setCellValue(cm, col + (lvl - 1) * 2 + 1, outputRow, iVal);                     //人数
                    }
                    if (fVal != 0) {
                        setCellValue(cm, col + (lvl - 1) * 2 + 2, outputRow, "(" + fVal + ")");         //構成比
                    }

                }
                cnt++;
                if (i >= dataList.size() - 1 ||  cnt >= MAX_ROW_COUNT) break;
        }

    }


    /**
     * 各試験情報出力
     * @param cm                Excel共通関数インスタンス
     * @param b42CefrMain       試験情報データ
     */
    private void setEachExamData(CM cm, B42CefrItem b42CefrMain) {

        int pasteRow = CURSOL.PASTE_LEFT.getRow();
        int page = 1;

        int startDataIdx = 0;

        int curCol = 0;
        int curRow = 0;

        boolean isFirst = true;

        ArrayList<ArrayList<B42CefrListBean>> eachList = b42CefrMain.getB42EachList();
        int examCnt = b42CefrMain.getB42EachExamNameList().size();
        for (int i=0; i<examCnt; i++) {

            //右側は出力済みなので飛ばす
            if ((i % 2) == 1) continue;

            boolean hasRight = ((i + 1) <= (examCnt - 1));         //右側があるかどうか

            ArrayList<B42CefrListBean> dataListLeft = eachList.get(i);
            ArrayList<B42CefrListBean> dataListRight = new ArrayList();
            for (int pIdx=0; pIdx<this.intTotalPageCnt; pIdx++) {

                page = pIdx + 1;

                if ((page > 1) || (isFirst == true) ||                                                                // 同一試験での改ページまたは初回
                    (page == 1 && this.intDetailCnt > MIN_ROW_COUNT) ||                                              //20明細以上ある場合の改ページ
                    (this.intDetailCnt <= MIN_ROW_COUNT && (pasteRow > (CURSOL.PASTE_SHIFT.getRow() * 3)))) {      //20明細で3段出力した後の改ページ
                        // 新ページ作成処理
                        isFirst = false;
                        startDataIdx = 0;
                        this.newCreateSheet(cm);
                        pasteRow = CURSOL.PASTE_LEFT.getRow();
                }

                if (page > 1) {
                    startDataIdx = (page - 1) * MAX_ROW_COUNT;
                }

                //左側
                {
                    curCol = CURSOL.PASTE_LEFT.getCol();
                    //テンプレート表をコピー
                    this.copyTemplateTable(cm, true, pasteRow, page);

                    // 試験名
                    curCol = CURSOL.PASTE_LEFT.getCol() + 1;
                    curRow = pasteRow + 1;
                    String examName = b42CefrMain.getB42EachExamNameList().get(i);
                    this.setCellValue(cm, curCol, curRow, examName);

                    curCol = CURSOL.DATA_LEFT.getCol();
                    curRow = pasteRow + 3;
                    this.setTableData(cm, dataListLeft, page, curCol, curRow, startDataIdx);
                }

                //右側
                if (hasRight) {
                    dataListRight = eachList.get(i+1);
                    curCol = CURSOL.PASTE_RIGHT.getCol();
                    //テンプレート表をコピー
                    this.copyTemplateTable(cm, false, pasteRow, page);

                    // 試験名
                    curRow = pasteRow + 1;
                    String examName = b42CefrMain.getB42EachExamNameList().get(i+1);
                    this.setCellValue(cm, curCol, curRow, examName);

                    curCol = CURSOL.DATA_RIGHT.getCol();
                    curRow = pasteRow + 3;
                    this.setTableData(cm, dataListRight, page, curCol, curRow, startDataIdx);
                }
            }

            //次の貼り付け位置を設定
            pasteRow += CURSOL.PASTE_SHIFT.getRow();
        }

    }

    /**
     * 全試験情報出力
     * @param cm         Excel共通関数インスタンス
     * @param allList   全試験情報データリスト
     */
    private void setAllExamData(CM cm, ArrayList<B42CefrListBean> allList) {

        int curCol = CURSOL.DATA_LEFT.getCol();
        int curRow = CURSOL.DATA_LEFT.getRow();
        int pasteRow = CURSOL.PASTE_LEFT.getRow();
        int startDataIdx = 0;

        for (int pIdx=0; pIdx<this.intTotalPageCnt; pIdx++) {

            int page = pIdx + 1;

            if (page > 1) {
                    // 新ページ作成処理
                    startDataIdx = (page - 1) * MAX_ROW_COUNT;
                    this.newCreateSheet(cm);
            }

            //テンプレート表をコピー
            this.copyTemplateTable(cm, true, pasteRow, page);

            //「実人数」を設定
            this.setCellValue(cm, 1, 7, "実人数");

            curRow = CURSOL.DATA_LEFT.getRow();
            this.setTableData(cm, allList, page, curCol, curRow, startDataIdx);
        }

    }


}

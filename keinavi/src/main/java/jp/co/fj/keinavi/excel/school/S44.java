/*
 * 校内成績分析−過年度比較　志望大学評価別人数
 * 作成日: 2004/07/13
 * @author	C.Murata
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.school.S44Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

/**
 * @author keinet
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class S44 {

	/*
	 * 出力帳票選択処理
	 * フラグにより帳票出力の判断をする。
	 * 		S44Item s44Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID:ユーザID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */

	 public boolean s44(S44Item s44Item, ArrayList outfilelist, int intSaveFlg, String UserID, KNSheetLog sheetLog) {

		KNLog log = KNLog.getInstance(null,null,null);

	 	try{
	 		int intRunFlg = s44Item.getIntDaiTotalFlg();

			int ret = 0;
			//S44Itemから各帳票を出力
			switch(intRunFlg) {
				case 1:	//大学（日程あり）
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL START
//					S44_02 exceledit02 = new S44_02();
//					ret = exceledit02.s44_02EditExcel(s44Item,outfilelist,intSaveFlg,UserID);
//					if( ret != 0 ){
//						log.Err("0"+ret+"S44_02","帳票作成エラー","");
//						return false;
//					}
//					sheetLog.add("S44_02");
//					break;
//				case 2:	//大学（日程なし）
//					S44_01 exceledit01 = new S44_01();
//					ret = exceledit01.s44_01EditExcel(s44Item,outfilelist,intSaveFlg,UserID);
//					if( ret != 0 ){
//						log.Err("0"+ret+"S44_01","帳票作成エラー","");
//						return false;
//					}
//					sheetLog.add("S44_01");
//					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL END
// 2019/10/02 QQ)Tanioka 廃止帳票 ADD START
				case 2:	//大学（日程なし）
				case 4:	//学部（日程なし）
// 2019/10/02 QQ)Tanioka 廃止帳票 ADD END
				case 3:	//学部（日程あり）
					S44_04 exceledit04 = new S44_04();
					ret = exceledit04.s44_04EditExcel(s44Item,outfilelist,intSaveFlg,UserID);
					if( ret != 0 ){
						log.Err("0"+ret+"S44_04","帳票作成エラー","");
						return false;
					}
					sheetLog.add("S44_04");
					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL START
//				case 4:	//学部（日程なし）
//					S44_03 exceledit03 = new S44_03();
//					ret = exceledit03.s44_03EditExcel(s44Item,outfilelist,intSaveFlg,UserID);
//					if( ret != 0 ){
//						log.Err("0"+ret+"S44_03","帳票作成エラー","");
//						return false;
//					}
//					sheetLog.add("S44_03");
//					break;
// 2019/10/02 QQ)Tanioka 廃止帳票 DEL END
				case 5:	//学科
					S44_05 exceledit05 = new S44_05();
					ret = exceledit05.s44_05EditExcel(s44Item,outfilelist,intSaveFlg,UserID);
					if( ret != 0 ){
						log.Err("0"+ret+"S44_05","帳票作成エラー","");
						return false;
					}
					sheetLog.add("S44_05");
					break;

				default:
				    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
				    //throw new Exception("S44 ERROR : フラグ異常 ");
				    throw new IllegalStateException("S44 ERROR : フラグ異常");
				    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			}
	 	}
	 	catch(Exception e){
			log.Err("99S44","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
	 	}

	 	return true;

	 }

}

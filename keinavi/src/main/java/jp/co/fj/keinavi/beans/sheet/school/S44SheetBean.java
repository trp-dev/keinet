package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.sheet.SheetSchoolData;
import jp.co.fj.keinavi.excel.data.school.S44GakkoListBean;
import jp.co.fj.keinavi.excel.data.school.S44HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.school.S44Item;
import jp.co.fj.keinavi.excel.data.school.S44ListBean;
import jp.co.fj.keinavi.excel.school.S44;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 校内成績分析 - 他校比較（志望大学評価別人数）
 * 
 * 2004.08.04	[新規作成]
 * 
 * 2005.04.06	Yoshimoto KAWAI - TOTEC
 * 				出力種別フラグ対応
 * 
 * 2005.04.06	Yoshimoto KAWAI - TOTEC
 * 				比較対象年度を過年度の表示から取得するように変更
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class S44SheetBean extends AbstractSheetBean {

	// データクラス
	private final S44Item data = new S44Item();

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		// 比較対象年度
		final String[] years = getUnivYearArray();
		// 大学集計区分
		final String univCountingDiv = getUnivCountngDiv();
		// 比較対象高校
		final List compSchoolList = getCompSchoolList();

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setStrMshCd(exam.getExamCD()); // 模試コード
		data.setIntDaiTotalFlg(getDaiTotalFlg()); // 大学集計区分
		data.setIntSecuFlg(getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(getOutputType()); // 出力種別フラグ

		// ワークテーブルのセットアップ
		insertIntoExistsExam();
		insertIntoCountUniv();
		insertIntoExamCdTrans();

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		try {
			// データリスト
			ps1 = conn.prepareStatement(getRatingQuery().toString());
			ps1.setString(1, exam.getExamYear()); // 模試年度
			ps1.setString(2, exam.getExamCD()); // 模試コード
			ps1.setString(3, profile.getBundleCD()); // 一括コード
			ps1.setString(4, univCountingDiv); // 大学集計区分
			ps1.setString(5, exam.getExamDiv()); // 模試区分

			// 評価別人数データリスト（自校）
			{
				final Query query = QueryLoader.getInstance().load("s44_1");
				query.setStringArray(1, years); // 比較対象年度

				ps2 = conn.prepareStatement(query.toString());			
				ps2.setString(9, exam.getExamCD()); // 模試コード
				ps2.setString(1, profile.getBundleCD()); // 一括コード
				ps2.setString(3, univCountingDiv); // 大学集計区分
			}

			// 評価別人数データリスト（全国）
			{
				final Query query = QueryLoader.getInstance().load("s44_2");
				query.setStringArray(1, years); // 比較対象年度

				ps3 = conn.prepareStatement(query.toString());
				ps3.setString(8, exam.getExamCD()); // 模試コード
				ps3.setString(2, univCountingDiv); // 大学集計区分
			}

			// 評価別人数データリスト（県）
			{
				final Query query = QueryLoader.getInstance().load("s44_3");
				query.setStringArray(1, years); // 比較対象年度

				ps4 = conn.prepareStatement(query.toString());
				ps4.setString(8, exam.getExamCD()); // 模試コード
				ps4.setString(2, univCountingDiv); // 大学集計区分
			}

			// 評価別人数データリスト（他校）
			{
				final Query query = QueryLoader.getInstance().load("s44_4");
				outDate2InDate(query); // データ開放日
				query.setStringArray(1, years); // 比較対象年度

				ps5 = conn.prepareStatement(query.toString());
				ps5.setString(8, exam.getExamCD()); // 模試コード
				ps5.setString(2, univCountingDiv); // 大学集計区分
			}

			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				S44ListBean bean = new S44ListBean();
				bean.setStrGenKouKbn(rs1.getString(1));
				bean.setStrDaigakuCd(rs1.getString(2));
				bean.setStrDaigakuMei(rs1.getString(3));
				bean.setStrGakubuCd(rs1.getString(4));
				bean.setStrGakubuMei(rs1.getString(5));
				bean.setStrGakkaCd(rs1.getString(6));
				bean.setStrGakkaMei(rs1.getString(7));
				bean.setStrNtiCd(rs1.getString(8));
				bean.setStrNittei(rs1.getString(9));
				bean.setStrHyouka(rs1.getString(10));
				data.getS44List().add(bean);

				// 現役高卒区分
				ps2.setString(2, rs1.getString(1));
				ps3.setString(1, rs1.getString(1));
				ps4.setString(1, rs1.getString(1));
				ps5.setString(1, rs1.getString(1));
				// 大学コード
				ps2.setString(4, rs1.getString(2));
				ps3.setString(3, rs1.getString(2));
				ps4.setString(3, rs1.getString(2));
				ps5.setString(3, rs1.getString(2));
				// 学部コード
				ps2.setString(5, rs1.getString(4));
				ps3.setString(4, rs1.getString(4));
				ps4.setString(4, rs1.getString(4));
				ps5.setString(4, rs1.getString(4));
				// 学科コード
				ps2.setString(6, rs1.getString(6));
				ps3.setString(5, rs1.getString(6));
				ps4.setString(5, rs1.getString(6));
				ps5.setString(5, rs1.getString(6));
				// 日程コード
				ps2.setString(7, rs1.getString(8));
				ps3.setString(6, rs1.getString(8));
				ps4.setString(6, rs1.getString(8));
				ps5.setString(6, rs1.getString(8));
				// 評価区分
				ps2.setString(8, rs1.getString(10));
				ps3.setString(7, rs1.getString(10));
				ps4.setString(7, rs1.getString(10));
				ps5.setString(7, rs1.getString(10));

				// 自校
				{
					S44GakkoListBean g = new S44GakkoListBean();
					g.setStrGakkomei(profile.getBundleName());
					bean.getS44GakkoList().add(g);

					rs2 = ps2.executeQuery();
					while (rs2.next()) {
						S44HyoukaNinzuListBean n = new S44HyoukaNinzuListBean();
						n.setStrNendo(rs2.getString(1));
						n.setIntSoshibo(rs2.getInt(2));
						n.setIntDai1shibo(rs2.getInt(3));
						n.setIntHyoukaA(rs2.getInt(4));
						n.setIntHyoukaB(rs2.getInt(5));
						n.setIntHyoukaC(rs2.getInt(6));
						n.setIntHyoukaD(rs2.getInt(7));
						n.setIntHyoukaE(rs2.getInt(8));

						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 共通テスト対応 ADD START
						//n.setIntHyoukaA_Hukumu(rs2.getInt(9));
						//n.setIntHyoukaB_Hukumu(rs2.getInt(10));
						//n.setIntHyoukaC_Hukumu(rs2.getInt(11));
						//n.setIntHyoukaD_Hukumu(rs2.getInt(12));
						//n.setIntHyoukaE_Hukumu(rs2.getInt(13));
						//// 2019/09/30 共通テスト対応 ADD END
						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

						g.getS44HyoukaNinzuList().add(n);
					}
					rs2.close();
				}

				// 比較対象高校分繰り返す
				Iterator ite = compSchoolList.iterator();
				while (ite.hasNext()) {
					SheetSchoolData	school = (SheetSchoolData) ite.next();

					switch (school.getSchoolTypeCode()) {
						// 全国
						case 1:
							rs3 = ps3.executeQuery();
							break;
							
						// 県
						case 2:
							ps4.setString(9, school.getPrefCD()); // 県コード
							rs3 = ps4.executeQuery();
							break;
						
						// 高校
						case 3:
							ps5.setString(9, school.getSchoolCD()); // 一括コード
							rs3 = ps5.executeQuery();
							break;
					}

					S44GakkoListBean g = null;
					while (rs3.next()) {
						// NULLなら作る
						if (g == null) {
							g = new S44GakkoListBean();
							g.setStrGakkomei(rs3.getString(1));
							bean.getS44GakkoList().add(g);
						}
						
						S44HyoukaNinzuListBean n = new S44HyoukaNinzuListBean();
						n.setStrNendo(rs3.getString(2));
						n.setIntSoshibo(rs3.getInt(3));
						n.setIntDai1shibo(rs3.getInt(4));
						n.setIntHyoukaA(rs3.getInt(5));
						n.setIntHyoukaB(rs3.getInt(6));
						n.setIntHyoukaC(rs3.getInt(7));
						n.setIntHyoukaD(rs3.getInt(8));
						n.setIntHyoukaE(rs3.getInt(9));

						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 共通テスト対応 ADD START
						//n.setIntHyoukaA_Hukumu(rs3.getInt(10));
						//n.setIntHyoukaB_Hukumu(rs3.getInt(11));
						//n.setIntHyoukaC_Hukumu(rs3.getInt(12));
						//n.setIntHyoukaD_Hukumu(rs3.getInt(13));
						//n.setIntHyoukaE_Hukumu(rs3.getInt(14));
						//// 2019/09/30 共通テスト対応 ADD END
						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

						g.getS44HyoukaNinzuList().add(n);
					}
					rs3.close();
				}
			}
		} finally {
			DbUtils.closeQuietly(null, ps1, rs1);
			DbUtils.closeQuietly(null, ps2, rs2);
			DbUtils.closeQuietly(null, ps3, rs3);
			DbUtils.closeQuietly(ps4);
			DbUtils.closeQuietly(ps5);
		}
	
		KNLog.Ep(null, null, null, null, "S44", "データクラス作成完了", "");
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S44_01");
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_OTHER_UNIV;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S44().s44(data, outfileList, getAction(), sessionKey, sheetLog);
	}

}

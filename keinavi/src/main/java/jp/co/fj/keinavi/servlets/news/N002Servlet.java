/*
 * 作成日: 2004/09/17
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.news;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.DefaultLoginServlet;
import jp.co.fj.keinavi.beans.news.InformDetailBean;
import jp.co.fj.keinavi.forms.news.N002Form;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class N002Servlet extends DefaultLoginServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(

		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ

		// セッション
		HttpSession session = request.getSession();

		// ログイン情報
		LoginSession login = super.getLoginSession(request);

		// アクションフォーム
		N002Form form = null;
		// requestから取得する
		try {
			form = (N002Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.news.N002Form");
		} catch (Exception e) {
			throw new ServletException(e);
		}
		
		if(("n002").equals(getForward(request)) || getForward(request) == null ) {

			// 全画面がお知らせトップの場合は「戻る」を有効にするためセット			
			String formId = null;
			if (getBackward(request) == null ) {
				formId = " ";
			} else {
				formId = getBackward(request);
			}
			if ( form.getDisplayDiv() == null ) {
				form.setDisplayDiv("0"); 
			}
			
			String infoId = null;
			infoId = form.getInfoId();
			
			Connection con = null; // コネクション
			try {
				con = super.getConnectionPool(request); 	// コネクション取得
				InformDetailBean bean = new InformDetailBean();
				bean.setDisplayDiv(form.getDisplayDiv());	// 表示区分 "0":お知らせ "1":Kei-Net情報
				bean.setInfoId(form.getInfoId());			// お知らせID
				bean.setFormId(formId);						// 呼び出し元画面を設定
				bean.setConnection(null, con);
				bean.execute();
	
				request.setAttribute("InformDetailBean", bean);
			} catch (Exception e) {
				throw new ServletException(e.toString());
			} finally {
				super.releaseConnectionPool(request, con); // コネクション解放
			}
	
			// アクションフォーム
			request.setAttribute("form", form);
	
			super.forward(request, response, JSP_N002);

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}


}

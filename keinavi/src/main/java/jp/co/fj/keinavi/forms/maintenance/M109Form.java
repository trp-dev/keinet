package jp.co.fj.keinavi.forms.maintenance;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 *
 * 情報不備生徒候補画面アクションフォーム
 * 
 * 2006.10.04	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M109Form extends BaseForm {
	
	// アクションモード
	private String actionMode;
	// 対象模試ID
	private String[] targetExamId;
	// 模試受験者
	private String examinee;
	// 仮受験者扱い
	private String temporary;
	// 未受験者
	private String notTake;
	// ソートキー
	private String sortKey;

	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	/**
	 * @return targetExamId を戻します。
	 */
	public String[] getTargetExamId() {
		return targetExamId;
	}

	/**
	 * @param pTargetExamId 設定する targetExamId。
	 */
	public void setTargetExamId(String[] pTargetExamId) {
		targetExamId = pTargetExamId;
	}

	/**
	 * @return examinee を戻します。
	 */
	public String getExaminee() {
		return examinee;
	}

	/**
	 * @param pExaminee 設定する examinee。
	 */
	public void setExaminee(String pExaminee) {
		examinee = pExaminee;
	}

	/**
	 * @return notTake を戻します。
	 */
	public String getNotTake() {
		return notTake;
	}

	/**
	 * @param pNotTake 設定する notTake。
	 */
	public void setNotTake(String pNotTake) {
		notTake = pNotTake;
	}

	/**
	 * @return temporary を戻します。
	 */
	public String getTemporary() {
		return temporary;
	}

	/**
	 * @param pTemporary 設定する temporary。
	 */
	public void setTemporary(String pTemporary) {
		temporary = pTemporary;
	}

	/**
	 * @return actionMode を戻します。
	 */
	public String getActionMode() {
		return actionMode;
	}

	/**
	 * @param pActionMode 設定する actionMode。
	 */
	public void setActionMode(String pActionMode) {
		actionMode = pActionMode;
	}

	/**
	 * @return sortKey を戻します。
	 */
	public String getSortKey() {
		return sortKey;
	}

	/**
	 * @param pSortKey 設定する sortKey。
	 */
	public void setSortKey(String pSortKey) {
		sortKey = pSortKey;
	}

}

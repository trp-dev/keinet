package jp.co.fj.keinavi.servlets.sales;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.freemenu.data.ExamScoreData;
import jp.co.fj.freemenu.data.ExamScoreSession;
import jp.co.fj.keinavi.beans.sales.SD201ApplyBean;
import jp.co.fj.keinavi.beans.sales.SD201ExamBean;
import jp.co.fj.keinavi.beans.sales.SD201SchoolBean;
import jp.co.fj.keinavi.forms.sales.SD201Form;
import jp.co.fj.keinavi.interfaces.SpecialAppliMenu;
import jp.co.fj.keinavi.servlets.sales.base.BaseSpecialAppliServlet;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * 特例成績データ作成申請画面のサーブレットです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD201Servlet extends BaseSpecialAppliServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = -355472946677742375L;

	/**
	 * @see jp.co.fj.keinavi.servlets.sales.base.BaseSpecialAppliServlet#getMenu()
	 */
	protected SpecialAppliMenu getMenu() {
		return SpecialAppliMenu.APPLY;
	}

	/**
	 * JSP表示処理を行います。
	 */
	public void index(HttpServletRequest request, HttpServletResponse response,
			SD201Form form) throws Exception {

		/* 模試情報をリクエストスコープにセットする */
		request.setAttribute("exam", findExamScoreSession(request));

		/* 学校名をリクエストスコープにセットする */
		if (!StringUtils.isEmpty(form.getSchoolCd())) {
			request.setAttribute("schoolName", findSchoolName(request, form));
		}

		forward2Jsp(request, response);
	}

	/**
	 * 模試情報をDBから取得します。
	 */
	private ExamScoreSession findExamScoreSession(HttpServletRequest request)
			throws Exception {
		return ((SD201ExamBean) executeBean(request, new SD201ExamBean()))
				.getExamScoreSession();
	}

	/**
	 * 申請処理を行います。
	 */
	public void apply(HttpServletRequest request, HttpServletResponse response,
			SD201Form form) throws Exception {

		/* 入力値をチェックする */
		validate(request, form);

		/* 入力エラーがあるなら再表示する */
		if (form.getErrorCount() > 0) {
			index(request, response, form);
			return;
		}

		/* 申請処理を実行する */
		SD201ApplyBean bean = new SD201ApplyBean(getLoginSession(request)
				.getLoginID(), form, getSectorCdArray(request));
		executeBean(request, bean);

		if (bean.getErrorCount() > 0) {
			/* 申請処理でエラーがあった場合は再表示する */
			form.setErrorMessage("common", bean.getErrorMessage());
			index(request, response, form);
		} else {
			redirect(request, response, "sd202");
		}
	}

	/**
	 * 入力値のチェックを行います。
	 */
	private void validate(HttpServletRequest request, SD201Form form)
			throws Exception {

		/* 対象年度・対象模試をチェックする */
		validateExam(request, form);

		/* 学校コードをチェックする */
		if (StringUtils.isEmpty(form.getSchoolCd())) {
			form.setErrorMessage("schoolCd", "学校コードを入力してください。");
		} else if (StringUtils.isEmpty(findSchoolName(request, form))) {
			form.setErrorMessage("schoolCd", "入力された学校コードは存在しません。");
		}
	}

	/**
	 * 対象年度・対象模試をチェックします。
	 */
	private void validateExam(HttpServletRequest request, SD201Form form)
			throws Exception {

		for (Iterator ite = findExamScoreSession(request).getExamMap().values()
				.iterator(); ite.hasNext();) {

			List list = (List) ite.next();
			for (Iterator ite2 = list.iterator(); ite2.hasNext();) {
				ExamScoreData data = (ExamScoreData) ite2.next();
				if (data.getExamYear().equals(form.getExamYear())
						&& data.getExamCd().equals(form.getExamCd())) {
					return;
				}
			}
		}

		throw new RuntimeException("利用できない対象年度・対象模試です。" + form.getExamYear()
				+ form.getExamCd());
	}

	/**
	 * 学校名を返します。
	 */
	public void getSchoolName(HttpServletRequest request,
			HttpServletResponse response, SD201Form form) throws Exception {

		response.setContentType("text/html;charset=UTF-8");

		if (StringUtils.isEmpty(form.getSchoolCd())) {
			response.getWriter().write(
					"<script>alert('学校コードを入力してください。');</script>");
			return;
		}

		String schoolName = findSchoolName(request, form);

		if (StringUtils.isEmpty(schoolName)) {
			response.getWriter().write(
					"<script>alert('入力された学校コードは存在しません。');</script>");
		} else {
			response.getWriter().write(schoolName);
		}
	}

	/**
	 * 学校名をDBから取得します。
	 */
	private String findSchoolName(HttpServletRequest request, SD201Form form)
			throws Exception {

		return ((SD201SchoolBean) executeBean(request,
				new SD201SchoolBean(form.getSchoolCd()))).getSchoolName();
	}

}

/*
 * 作成日: 2004/10/08
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.help;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import jp.co.fj.keinavi.data.help.HelpList;

import com.fjh.beans.DefaultBean;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.date.DateUtil;
import jp.co.fj.keinavi.util.date.FullDateUtil;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class OnepointNewBean extends DefaultBean {

	private List helpList = new ArrayList();

	/* 
	 * 
	 * (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// TODO 自動生成されたメソッド・スタブ

		// 日付用クラス
		DateUtil date = new FullDateUtil();

		// ------------------------------------------------------------
		// 2004.11.21
		// Yoshimoto KAWAI - Totec
		// ワンポイントマスタに存在しないヘルプは出さない
		// ------------------------------------------------------------
		// SQLをセットアップ
		StringBuffer query = new StringBuffer();
		query.append("SELECT H.* ");
		query.append("FROM HELP H ");
		query.append("WHERE EXISTS (");
		query.append("SELECT O.ROWID FROM ONEPOINT O WHERE O.HELPID = H.HELPID");
		query.append(") ORDER BY CASE WHEN H.RENEWALDATE IS NULL THEN '1' ELSE '0' END, H.RENEWALDATE DESC, H.HELPID DESC ");

		PreparedStatement ps = null;
		ResultSet rs = null;
		int     reccnt = 0;
		String  keisai  = null; 
		String  dispDiv = null; 
		try {
			ps = conn.prepareStatement(query.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				// 表示行数を超えたら終了
				reccnt++;
				if (reccnt > getOnePointCountLimt() ) {
					break;
				}
				HelpList list = new HelpList();
				list.setHelpId(rs.getString(1));
				list.setTitleStr(rs.getString(2));
				list.setCategory(rs.getString(3));
				list.setDispSequence(rs.getString(4));
				list.setUpdate(getDispDate(rs.getString(5)));
				list.setExplan(rs.getString(6));
				helpList.add(list);
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}


	}

	/**
	 * 表示件数を取得する。
	 * 
	 * @param* @return 表示件数
	 * @throws Exception
	 */
	public int getOnePointCountLimt() {
		int  count = 0;
		try {
			count = KNCommonProperty.getOnePointCountLimt();
		} catch (Exception e) {
			new Exception(e);
		}
		return count;
	}

	/**
	 * 日付（yyyy/mm/dd)を取得
	 * 	 * @param string
	 */
	public String getDispDate(String date) {
		if (date == null || date.equals("")) {
			return "";
		} else {
			return date.substring(0,4) + "/" + date.substring(4,6) + "/" + date.substring(6,8);
		}
	}

	/**
	 * @return
	 */
	public List getHelpList() {
		return helpList;
	}

	/**
	 * @param list
	 */
	public void setHelpList(List list) {
		helpList = list;
	}

}

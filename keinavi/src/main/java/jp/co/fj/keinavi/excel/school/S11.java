/**
 * 校内成績分析−校内成績　成績概況
 * 出力する帳票の判断
 * 作成日: 2004/07/01
 * @author	T.Sakai
 */
 
package jp.co.fj.keinavi.excel.school;

import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.util.log.*;

public class S11 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean s11( S11Item s11Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog) {
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		try{
			int ret = 0;
			//S11Itemから各帳票を出力
			if (( s11Item.getIntHyouFlg()==0 ) && ( s11Item.getIntGraphFlg()==0 )){
				throw new Exception("S11 ERROR : フラグ異常 ");
			}
			if ( s11Item.getIntHyouFlg()==1 ) {
				log.Ep("S11_01","S11_01帳票作成開始","");
				S11_01 exceledit = new S11_01();
				ret = exceledit.s11_01EditExcel( s11Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S11_01","帳票作成エラー","");
					return false;
				}
				log.Ep("S11_01","S11_01帳票作成終了","");
				sheetLog.add("S11_01");
			}
			if ( s11Item.getIntGraphFlg()==1 ) {
				log.Ep("S11_02","S11_02帳票作成開始","");
				S11_02 exceledit = new S11_02();
				ret = exceledit.s11_02EditExcel( s11Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S11_02","帳票作成エラー","");
					return false;
				}
				log.Ep("S11_02","S11_02帳票作成終了","");
				sheetLog.add("S11_02");
			}
			
		} catch(Exception e) {
			log.Err("99S11","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * �N���X���ъT���|�l���ѐ��ڃf�[�^�N���X
 * �쐬��: 2004/07/14
 * @author	H.Fujimoto
 */
public class C13Item {
	//�w�Z��
	private String strGakkomei = "";
	//�Ώ۔N�x
	private String strYear = "";
	//�Ώ͎ۖ��@�͎���
	private String strMshmeiNow = "";
	//�Ώ͎ۖ��@�͎��Z�k��
	private String strMshmeiShortNow = "";
	//�Ώ͎ۖ��@�͎����{���
	private String strMshDateNow = "";
	//�O��͎��@�͎���
	private String strMshmeiLast1 = "";
	//�O��͎��@�͎��Z�k��
	private String strMshmeiShortLast1 = "";
	//�O��͎��@�͎����{���
	private String strMshDateLast1 = "";
	//�O�X��͎��@�͎���
	private String strMshmeiLast2 = "";
	//�O�X��͎��@�͎��Z�k��
	private String strMshmeiShortLast2 = "";
	//�O�X��͎��@�͎����{���
	private String strMshDateLast2 = "";
	//�\�t���O
	private int intHyouFlg = 0;
	//�^�E�Ȗڕʔ�r�t���O
	private int intKmkHikakuFlg = 0;
	//��r�񐔃t���O
	private int intHikakuCntFlg = 0;
	//�\�������@�΍��l
	private String strKeyHensa = "";
	//�\�������@�ϓ���
	private String strKeyPitch = "";
	//�\�������t���O
	private int intSortFlg = 0;
	//�Z�L�����e�B�X�^���v
	private int intSecuFlg = 0;
	//�������уf�[�^���X�g
	private ArrayList c13List = new ArrayList();
	//�^�E�Ȗڕʃf�[�^���X�g
	private ArrayList c13KmkDataList = new ArrayList();
	//�o�͎�ʃt���O �� �V�e�X�g�p�ɒǉ�
	private int intShubetsuFlg = 0;


	/*----------*/
	/* Get      */
	/*----------*/

	public ArrayList getC13List() {
		return this.c13List;
	}
	public ArrayList getC13KmkDataList() {
		return this.c13KmkDataList;
	}
	public int getIntHikakuCntFlg() {
		return this.intHikakuCntFlg;
	}
	public int getIntHyouFlg() {
		return this.intHyouFlg;
	}
	public int getIntKmkHikakuFlg() {
		return this.intKmkHikakuFlg;
	}
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public int getIntSortFlg() {
		return this.intSortFlg;
	}
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrKeyHensa() {
		return this.strKeyHensa;
	}
	public String getStrKeyPitch() {
		return this.strKeyPitch;
	}
	public String getStrMshDateNow() {
		return this.strMshDateNow;
	}
	public String getStrMshmeiNow() {
		return this.strMshmeiNow;
	}
	public String getStrMshmeiShortNow() {
		return this.strMshmeiShortNow;
	}
	public String getStrMshDateLast1() {
		return this.strMshDateLast1;
	}
	public String getStrMshDateLast2() {
		return this.strMshDateLast2;
	}
	public String getStrMshmeiLast1() {
		return this.strMshmeiLast1;
	}
	public String getStrMshmeiLast2() {
		return this.strMshmeiLast2;
	}
	public String getStrMshmeiShortLast1() {
		return this.strMshmeiShortLast1;
	}
	public String getStrMshmeiShortLast2() {
		return this.strMshmeiShortLast2;
	}
	public String getStrYear() {
		return this.strYear;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	
	/*----------*/
	/* Set      */
	/*----------*/	

	public void setC13List(ArrayList c13List) {
		this.c13List = c13List;
	}
	public void setC13KmkDataList(ArrayList c13KmkDataList) {
		this.c13KmkDataList = c13KmkDataList;
	}
	public void setIntHikakuCntFlg(int intHikakuCntFlg) {
		this.intHikakuCntFlg = intHikakuCntFlg;
	}
	public void setIntHyouFlg(int intHyouFlg) {
		this.intHyouFlg = intHyouFlg;
	}
	public void setIntKmkHikakuFlg(int intKmkHikakuFlg) {
		this.intKmkHikakuFlg = intKmkHikakuFlg;
	}
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setIntSortFlg(int intSortFlg) {
		this.intSortFlg = intSortFlg;
	}
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrKeyHensa(String strKeyHensa) {
		this.strKeyHensa = strKeyHensa;
	}
	public void setStrKeyPitch(String strKeyPitch) {
		this.strKeyPitch = strKeyPitch;
	}
	public void setStrMshDateNow(String strMshDateNow) {
		this.strMshDateNow = strMshDateNow;
	}
	public void setStrMshmeiNow(String strMshmeiNow) {
		this.strMshmeiNow = strMshmeiNow;
	}
	public void setStrMshmeiShortNow(String strMshmeiShortNow) {
		this.strMshmeiShortNow = strMshmeiShortNow;
	}
	public void setStrMshDateLast1(String strMshDateLast1) {
		this.strMshDateLast1 = strMshDateLast1;
	}
	public void setStrMshDateLast2(String strMshDateLast2) {
		this.strMshDateLast2 = strMshDateLast2;
	}
	public void setStrMshmeiLast1(String strMshmeiLast1) {
		this.strMshmeiLast1 = strMshmeiLast1;
	}
	public void setStrMshmeiLast2(String strMshmeiLast2) {
		this.strMshmeiLast2 = strMshmeiLast2;
	}
	public void setStrMshmeiShortLast1(String strMshmeiShortLast1) {
		this.strMshmeiShortLast1 = strMshmeiShortLast1;
	}
	public void setStrMshmeiShortLast2(String strMshmeiShortLast2) {
		this.strMshmeiShortLast2 = strMshmeiShortLast2;
	}
	public void setStrYear(String strYear) {
		this.strYear = strYear;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}
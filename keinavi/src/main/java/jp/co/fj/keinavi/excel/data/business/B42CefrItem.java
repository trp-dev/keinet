package jp.co.fj.keinavi.excel.data.business;

import java.util.ArrayList;

/**
 * Zà¬ÑªÍ|ZÔär|Î·lªz pêCEFR±Êæ¾óµ
 * ì¬ú: 2019/09/30
 * @author QQ)
 *
 */
public class B42CefrItem {
        //S±pzñ
        private ArrayList<B42CefrListBean> B42AllList = new ArrayList();

        //e±pzñ
        private ArrayList<ArrayList<B42CefrListBean>> B42EachList = new ArrayList(new ArrayList());

        //e±p±¼zñ
        private ArrayList<String> B42EachExamNameList = new ArrayList();

        //CEFRx¼
        private ArrayList<String> CefrLevelList = new ArrayList();

        //¾×
        private int intDetailCount = 0;



        /*----------*/
        /* Get      */
        /*----------*/
        public ArrayList<B42CefrListBean> getB42AllList() {
            return B42AllList;
        }

        public ArrayList<ArrayList<B42CefrListBean>> getB42EachList() {
            return B42EachList;
        }

        public ArrayList<String> getB42EachExamNameList() {
            return B42EachExamNameList;
        }
        public ArrayList<String> getCefrLevelList() {
            return CefrLevelList;
        }

        public int getIntDetailCount() {
            return intDetailCount;
        }

        /*----------*/
        /* Set      */
        /*----------*/
        public void setB42AllList(ArrayList<B42CefrListBean> b42AllList) {
            B42AllList = b42AllList;
        }

        public void setB42EachList(ArrayList<ArrayList<B42CefrListBean>> b42EachList) {
            B42EachList = b42EachList;
        }

        public void setB42EachExamNameList(ArrayList<String> b42EachExamNameList) {
            B42EachExamNameList = b42EachExamNameList;
        }

        public void setCefrLevelList(ArrayList<String> cefrLevelList) {
            CefrLevelList = cefrLevelList;
        }

        public void setIntDetailCount(int intDetailCount) {
            this.intDetailCount = intDetailCount;
        }



}

package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * 偏差値分布（クラス比較）データクラス
 * 作成日: 2004/08/17
 * @author	Ito.Y
 */
public class C31ListBean {
	//型・科目名コード
	private String strKmkCd = "";
	//型・科目名
	private String strKmkmei = "";
	//配点
	private String strHaitenKmk = "";
	//型･科目用グラフ表示フラグ
	private int intDispKmkFlg = 0;
	//クラスデータリスト
	private ArrayList c31ClassList = new ArrayList();
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public int getIntDispKmkFlg() {
		return this.intDispKmkFlg;
	}
	public ArrayList getC31ClassList() {
		return this.c31ClassList;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setIntDispKmkFlg(int intDispKmkFlg) {
		this.intDispKmkFlg = intDispKmkFlg;
	}
	public void setC31ClassList(ArrayList c31ClassList) {
		this.c31ClassList = c31ClassList;
	}
}

/*
 * 作成日: 2004/10/07
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ProvisoData {
	
	private String individualId;//個人ID
	private String districtProviso;//地区条件(県コードprefectureをセット)
	private String searchStemmaDiv;//系統選択　中・文・理
	private String stemmaProviso;//分野コード
	private String descImposeDiv;//2次試験課し区分 1:課さない
	private String subImposeDiv;//試験科目課し区分 0:課さない、1:課す
	private String imposeSub;//課し試験科目
	private String schoolDivProviso;//学校区分条件
	private String startDateProviso;//入試開始日条件
	private String endDateProviso;//入試終了日条件
	private String raingDiv;//評価試験区分 1：センターまたは2次・私大、2：総合（ドッキング）
	private String raingProviso;//評価範囲条件
	private String outOfTergetProviso;//対象外条件
	private String dateSearchDiv;//入試日区分


	/**
	 * @return
	 */
	public String getDescImposeDiv() {
		return descImposeDiv;
	}

	/**
	 * @return
	 */
	public String getDistrictProviso() {
		return districtProviso;
	}

	/**
	 * @return
	 */
	public String getEndDateProviso() {
		return endDateProviso;
	}

	/**
	 * @return
	 */
	public String getImposeSub() {
		return imposeSub;
	}

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @return
	 */
	public String getOutOfTergetProviso() {
		return outOfTergetProviso;
	}

	/**
	 * @return
	 */
	public String getRaingDiv() {
		return raingDiv;
	}

	/**
	 * @return
	 */
	public String getRaingProviso() {
		return raingProviso;
	}

	/**
	 * @return
	 */
	public String getSchoolDivProviso() {
		return schoolDivProviso;
	}

	/**
	 * @return
	 */
	public String getSearchStemmaDiv() {
		return searchStemmaDiv;
	}

	/**
	 * @return
	 */
	public String getStartDateProviso() {
		return startDateProviso;
	}

	/**
	 * @return
	 */
	public String getStemmaProviso() {
		return stemmaProviso;
	}

	/**
	 * @return
	 */
	public String getSubImposeDiv() {
		return subImposeDiv;
	}

	/**
	 * @param string
	 */
	public void setDescImposeDiv(String string) {
		descImposeDiv = string;
	}

	/**
	 * @param string
	 */
	public void setDistrictProviso(String string) {
		districtProviso = string;
	}

	/**
	 * @param string
	 */
	public void setEndDateProviso(String string) {
		endDateProviso = string;
	}

	/**
	 * @param string
	 */
	public void setImposeSub(String string) {
		imposeSub = string;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

	/**
	 * @param string
	 */
	public void setOutOfTergetProviso(String string) {
		outOfTergetProviso = string;
	}

	/**
	 * @param string
	 */
	public void setRaingDiv(String string) {
		raingDiv = string;
	}

	/**
	 * @param string
	 */
	public void setRaingProviso(String string) {
		raingProviso = string;
	}

	/**
	 * @param string
	 */
	public void setSchoolDivProviso(String string) {
		schoolDivProviso = string;
	}

	/**
	 * @param string
	 */
	public void setSearchStemmaDiv(String string) {
		searchStemmaDiv = string;
	}

	/**
	 * @param string
	 */
	public void setStartDateProviso(String string) {
		startDateProviso = string;
	}

	/**
	 * @param string
	 */
	public void setStemmaProviso(String string) {
		stemmaProviso = string;
	}

	/**
	 * @param string
	 */
	public void setSubImposeDiv(String string) {
		subImposeDiv = string;
	}

	/**
	 * @return
	 */
	public String getDateSearchDiv() {
		return dateSearchDiv;
	}

	/**
	 * @param string
	 */
	public void setDateSearchDiv(String string) {
		dateSearchDiv = string;
	}

}

package jp.co.fj.keinavi.util.taglib;

import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import org.apache.taglibs.standard.tag.common.core.NullAttributeException;
import org.apache.taglibs.standard.tag.el.core.ExpressionUtil;

import jp.co.fj.keinavi.data.ExamData;

/**
 *
 * 指定のリストに特定の試験の種類が存在するかを判断するタグクラス
 * 
 * 2006/08/30    [新規作成]
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class ExamContainTag extends ExamIfTag {
    
    /**
     * オペランド
     */
    private String items;
    
    /**
     * 試験リスト
     */
    private List exams;
    
    /**
     * @param string
     */
    public void setItems(String string) {
        items = string;
    }
    
    /**
     * @see javax.servlet.jsp.tagext.Tag#release()
     */
    public void release() {
        super.release();
        init();
    }
    
    /**
     * @see jp.co.fj.keinavi.util.taglib.ExamIfTag#init()
     */
    protected void init() {
        super.init();
        exams = null;
    }
    
    /**
     * @see jp.co.fj.keinavi.util.taglib.ExamIfTag#evaluateSelf()
     */
    protected boolean evaluateSelf() throws JspTagException {
        
        final Iterator ite = exams.iterator();
        
        while (ite.hasNext()) {
            
            final ExamData exam = getExam(ite.next());
            
            //条件に合致した試験が存在
            if (isExamIdentified(exam)) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * @throws JspException
     */
    protected void evaluateExpressions() throws JspException {
        try {
            exams = (List) ExpressionUtil.evalNotNull(
                    "examIf", "items", items, List.class, this, pageContext);
        } catch (NullAttributeException ex) {
            throw new JspTagException(items + "はnullです。評価できません。");
        }
    }

}

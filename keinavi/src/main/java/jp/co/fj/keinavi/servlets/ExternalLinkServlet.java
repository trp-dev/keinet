package jp.co.fj.keinavi.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.auth.KWebSysManager;

/**
 * 
 * 外部リンク用Servlet
 * ※http://www.adobe.com/jp/support/jrun/ts/documents/tn18242.htm
 * 
 * 2010.08.20	Tomohisa Yamada - TOTEC
 * 				[新規作成]				
 * 
 * 
 */
public class ExternalLinkServlet extends DefaultHttpServlet {
	
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		try {
			//handle(request, response);
			handleByJavascript(request, response);
		} catch (Exception e) {
			throw createServletException(e);
		}
	}
	
	/**
	 * javascriptを使ってPOSTを実行する
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	private void handleByJavascript(		
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		response.setContentType("text/html; charset=Windows-31J");

	    StringBuffer buf = null;
	    
	    PrintWriter out = null;
	    
	    try {
	    	
	    	
	    	buf = new StringBuffer();
	    	
	    	buf.append("\n<HTML>\n");
	    	buf.append("\n<HEAD>");
	    	buf.append("\n<meta http-equiv=\"Pragma\" content=\"no-cache\">");
	    	buf.append("\n<meta http-equiv=\"Cache-Control\" content=\"no-cache\">");
	    	buf.append("\n<SCRIPT TYPE=\"TEXT/JAVASCRIPT\">");
	    	buf.append("\n<!--");
	    	buf.append("\n	function submitForm() {");
	    	buf.append("\n		document.forms[0].submit();");
	    	buf.append("\n	}");
	    	buf.append("\n// -->");
	    	buf.append("\n</SCRIPT>");
	    	buf.append("\n</HEAD>");
	    	buf.append("\n<BODY onload=\"submitForm();\">");
	    	
	    	//k-webの場合
	    	if ("k-web".equals(request.getParameter("url"))) {
	    		build4Kweb(request, response, buf);
	    	}
	    	
	    	buf.append("\n</form>");
	    	buf.append("\n</BODY>");
	    	buf.append("\n</HTML>");
	    	
	    	//HTML書き出し
	    	out = response.getWriter();
	    	out.print(buf.toString());
		    
	    } finally {
	    	
	    	if (out != null) {
	    		out.flush();
	    	}
	    	
	    	if (out != null) {
	    		out.close();
	    	}
	    }
	}
	
	/**
	 * K-Webリンク用フォーム値作成
	 * @param request
	 * @param response
	 * @param buf
	 * @throws Exception
	 */
	private void build4Kweb(		
			HttpServletRequest request,
			HttpServletResponse response,
			StringBuffer buf) throws Exception {
		
		//外部リンク用パラメタ作成・設定
		final KWebSysManager elm = 
			new KWebSysManager(
					request, 
					getLoginSession(request), 
					getProfile(request));
				
		buf.append(createAction(elm.getUrl()));
		buf.append(createHidden("sciv", elm.getSciv()));
		buf.append(createHidden("scid", elm.getScid()));
		buf.append(createHidden("scnm", elm.getScnm()));
		buf.append(createHidden("sccd", elm.getSccd()));
		buf.append(createHidden("sstart", elm.getSstart()));
		buf.append(createHidden("usragt", elm.getUsragt()));
	}
	
	/**
	 * action部分作成
	 * @param value
	 * @return
	 */
	private String createAction(String value) {
		return "\n<form action=\"" + value + "\" method=\"POST\">";
	}
	
	/**
	 * hidden部分作成
	 * @param key
	 * @param value
	 * @return
	 */
	private String createHidden(String key, String value) {
		return "\n	<input type=\"hidden\" name=\"" + key + "\" value=\""+ value + "\">";
	}
	
//	private void handle(		
//			HttpServletRequest request,
//			HttpServletResponse response) throws Exception {
//		
//		HttpURLConnection conn = null;
//		BufferedReader in = null;
//		PrintWriter out = null;
//		
//		try {
//			
//			URL url = new URL("http://localhost/test");
//			
//			conn = (HttpURLConnection) url.openConnection();
//			//methodName = request.getMethod();
//			conn.setRequestMethod("POST");
//			conn.setDoOutput(true);
//			conn.setDoInput(true);
////			conn.setFollowRedirects(false);
////			conn.setUseCaches(true);//urlConn.setUseCaches (false);
//			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");//text/html;charset=Windows-31J
//		
//			PrintWriter x = new PrintWriter(conn.getOutputStream());
//			x.print("sciv=" + request.getParameter("sciv"));
//			//x.print("&scid=" + request.getParameter("scid"));
//			x.close();
//
//			//転送先に接続
//			//conn.connect();
//			
////			String message = conn.getResponseMessage();
////			String method = conn.getRequestMethod();
////			int code = conn.getResponseCode();
//			
//			in = new BufferedReader(
//					new InputStreamReader(conn.getInputStream()));
//			
//			out = response.getWriter();
//			
//			//転送先にデータがあるかどうか
//			boolean hasData = false;
//			
//			String result;
//			
//			while ((result = in.readLine()) != null) {
//				
//				//返答をそのまま書き出し
//				out.write(result);
//				
//				if (!hasData) {
//					hasData = true;
//				}
//			}
//			
//			//必要ないかも...
//			if (!hasData) {
//				throw new Exception("不明な転送先です。");
//				//code = 404;
//			}
//			
//		} catch (FileNotFoundException fnfe) {
//			throw new Exception("不明な転送先です。");
//			//code = 404;
//		} catch (IOException ioe) {
//			throw ioe;
//		} finally {
//			
//			if (in != null) {
//				in.close();
//			}
//		
//			if (out != null) {
//				out.close();
//			}
//			
//			if (conn != null) {
//				conn.disconnect();
//			}
//		}
//	}
}

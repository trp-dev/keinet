package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * 校内成績分析−過回比較−成績概況 データリスト
 * 作成日: 2004/07/21
 * @author	A.Iwata
 */
public class S51ListBean {
	//型・科目名コード
	private String strKmkCd = "";
	//型・科目名
	private String strKmkmei = "";
	//型･科目用グラフ表示フラグ
	private int intDispKmkFlg = 0;
	//配点
	private String strHaitenKmk = "";
	//過回比較用データリスト
	private ArrayList s51KakaiList = new ArrayList();
	//過年度比較用データリスト
	private ArrayList s51YearList = new ArrayList();
	//クラス比較用データリスト
	private ArrayList s51ClassList = new ArrayList();
	//他校比較用データリスト
	private ArrayList s51TakouList = new ArrayList();
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}
	public int getIntDispKmkFlg() {
		return this.intDispKmkFlg;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public ArrayList getS51KakaiList() {
		return this.s51KakaiList;
	}
	public ArrayList getS51YearList() {
		return this.s51YearList;
	}
	public ArrayList getS51ClassList() {
		return this.s51ClassList;
	}
	public ArrayList getS51TakouList() {
		return this.s51TakouList;
	}


	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setIntDispKmkFlg(int intDispKmkFlg) {
		this.intDispKmkFlg = intDispKmkFlg;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setS51KakaiList(ArrayList s51KakaiList) {
		this.s51KakaiList = s51KakaiList;
	}
	public void setS51YearList(ArrayList s51YearList) {
		this.s51YearList = s51YearList;
	}
	public void setS51ClassList(ArrayList s51ClassList) {
		this.s51ClassList = s51ClassList;
	}
	public void setS51TakouList(ArrayList s51TakouList) {
		this.s51TakouList = s51TakouList;
	}
}

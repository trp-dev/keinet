package jp.co.fj.keinavi.excel.school;

//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S24HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.school.S24Item;
import jp.co.fj.keinavi.excel.data.school.S24ListBean;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * 作成日: 2004/08/23
 * @author Ito.Y
 *
 * 校内成績分析−過年度比較−志望大学評価別人数　大学（日程あり）出力処理
 * 
 * <2010年度改修>
 * 2010.01.06   Tomohisa YAMADA - Totec
 *              センター・リサーチ５段階評価対応
 *              
 */
public class S24_02 {
	
	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow		= 80;			//MAX行数

	private boolean bolBookCngFlg = true;			//改ファイルフラグ	
	private boolean bolBookClsFlg = false;			//改ファイルフラグ	
	private boolean bolSheetCngFlg = true;			//改シートフラグ
	private int intDataStartRow = 7;					//データセット開始行
	
	final private String masterfile0 = "S24_02" ;
	final private String masterfile1 = "S24_02" ;
	private String masterfile = "" ;

	//2004.9.27 Add
	final private String strCenterResearch = "38";
	//2004.9.27 Add End

	/*
	 * 	Excel編集メイン
	 * 		S24Item s24Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int s24_02EditExcel(S24Item s24Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
			
			
		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFCell	workCell		= null;
		HSSFRow		workRow			= null;
		
		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intRow = 0;
		String strGenekiKbn = "";
		String strDaigakuCode = "";
		String strDaigakuCodeHoji = "";
		String strDaigakuMei = "";
		String strNitteiCode = "";
		
		int intLoopCnt = 0;
		int intNinzuListCnt = 0;
		int intSheetCngCount = 0;
		int intBookCngCount = 0;
		
		String strDispTarget = "";
		//2004.09.27Add
		String strKokushiKbn = "";
		String strStKokushiKbn = "";
		//2004.09.27AddEnd
		
		//テンプレートの決定
		if (s24Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S24ListBean s24ListBean = new S24ListBean();
		ArrayList	addCenterList	= null;
		ArrayList	addSecondList	= null;
		ArrayList	addTotalList	= null;
		ArrayList	tmpAddList	= null;
		
		
		try{
			
			// Listの取得
			ArrayList s24List = s24Item.getS24List();
			ListIterator itr = s24List.listIterator();
			
			/** 2004.9.28 Remove
			if( itr.hasNext() == false ){
				return errfdata;
			}
			**/

			// 24Listの展開
			while( itr.hasNext() ){
				
				s24ListBean = (S24ListBean)itr.next();
				ArrayList s24HyoukaNinzuList = s24ListBean.getS24HyoukaNinzuList();

				// 現役生・高卒生区分判定(区分が異なっていたら改シート)
				if(( !cm.toString(strGenekiKbn).equals(cm.toString(s24ListBean.getStrGenKouKbn())) )&&
					(addCenterList == null)&&(addSecondList == null)&&(addTotalList == null)){
					strGenekiKbn = s24ListBean.getStrGenKouKbn();
					if( cm.toString(strGenekiKbn).equals("0") ){
						strDispTarget = "全体";
					}
					else if( cm.toString(strGenekiKbn).equals("1") ){
						strDispTarget = "現役生";
					}
					else if( cm.toString(strGenekiKbn).equals("2") ){
						strDispTarget = "高卒生";
					}
					
					// 改シート・改ブック判定
					strDaigakuCode = "";
					strNitteiCode = "";		
					if( bolSheetCngFlg == false ){
						//罫線出力（セルの上部に太線を出力）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
															intRow, intRow, 0, 11, false, true, false, false);
						bolSheetCngFlg = true;
						//intMaxSheetIndex++;
						if( intMaxSheetIndex >= intMaxSheetSr ){
							//改ファイル
							bolBookCngFlg = true; 
							bolBookClsFlg = true;
						}
					}
				}
				
				//2004.9.27 Add
				int intDaigakuCd = (int)Integer.parseInt(s24ListBean.getStrDaigakuCd());
				if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
					strKokushiKbn = "国公立大";
				}
				if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
					strKokushiKbn = "私立大";
				}
				if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
					strKokushiKbn = "短大";
				}
				if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
					strKokushiKbn = "その他";
				}
				
				if( (!cm.toString(strKokushiKbn).equals(cm.toString(strStKokushiKbn))) && (addCenterList == null)&&(addSecondList == null)&&(addTotalList == null) ){
					// 改シート・改ブック判定
					strDaigakuCode = "";
					strNitteiCode = "";		
					strStKokushiKbn = strKokushiKbn;
					if( bolSheetCngFlg == false ){
						//罫線出力（セルの上部に太線を出力）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
															intRow, intRow, 0, 11, false, true, false, false);
						bolSheetCngFlg = true;
						//intMaxSheetIndex++;
						if( intMaxSheetIndex >= intMaxSheetSr ){
							//改ファイル
							bolBookCngFlg = true; 
							bolBookClsFlg = true;
						}
					}
				}
				//2004.9.27 Add End
				
				// 改シート・改ブック処理
				if( bolSheetCngFlg == true ){

					if( bolBookCngFlg == true ){
						
						if(bolBookClsFlg == true ){
							//WorkBook保存
							boolean bolRet = false;
							// Excelファイル保存
							bolRet = clsBook( workbook, intMaxSheetIndex, intBookCngCount,
									intSaveFlg, outfilelist, UserID, itr.hasNext(), false );
							if( bolRet == false ){
								return errfwrite;
							}
							intBookCngCount++;
							bolBookClsFlg = false;
						}
						
						// マスタExcel読み込み
						workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
						//2004.9.28 Update 
//						if( workbook.equals(null) ){
						if( workbook == null ){
						//2004.9.28 Update End
							return errfread;
						}
					
						bolBookCngFlg = false;
						intMaxSheetIndex = 0;
					}
					
					//2004.9.27 Add
					if( workSheet != null ){
						int intRemoveStartRow = ( intSheetCngCount * intNinzuListCnt ) + intDataStartRow;
						if( intMaxRow + intDataStartRow > intRemoveStartRow ){
							for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
								//行削除処理
								workSheet.removeRow(workSheet.getRow(index));
							}
							//罫線出力（セルの上部に太線を出力）
							cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
											intRemoveStartRow, intRemoveStartRow, 0, 11, false, true, false, false);
						}
					}
					//2004.9.27 Add End
					
					//シート作成
					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);

					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);

					// ヘッダー
					//2004.9.28 Update 
//					setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
//										s24Item.getStrMshmei(),	s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget );
					setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
										s24Item.getStrMshmei(),	s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget, strKokushiKbn );
					//2004.9.28 Update End

					intMaxSheetIndex++;		//シート数カウンタインクリメント
					bolSheetCngFlg = false;

					intRow = intDataStartRow;
					intLoopCnt = 0;

				}

				NumberFormat nf = NumberFormat.getInstance();
				intNinzuListCnt = (int)s24HyoukaNinzuList.size();			//評価別人数リスト データ数取得
				intSheetCngCount =nf.parse(nf.format(intMaxRow / intNinzuListCnt)).intValue(); 


				// 小計セット
////////////////////////
//			  	if( !cm.toString(strDaigakuCodeHoji).equals(cm.toString(s24ListBean.getStrDaigakuCd()))){
////////////////////////
				if(( !cm.toString(strDaigakuCodeHoji).equals(cm.toString(s24ListBean.getStrDaigakuCd())))
					||( !cm.toString(strGenekiKbn).equals(cm.toString(s24ListBean.getStrGenKouKbn())) )){
					// データセット
					boolean firstFlg = true;
					for(int a=0;a<3;a++){
						String strHyokaKbn = "";
						if(a==0){
							tmpAddList = addCenterList;
							strHyokaKbn = "センター";
						}else if(a==1){
							tmpAddList = addSecondList;
							strHyokaKbn = "2次・私大";
						}else if(a==2){
							tmpAddList = addTotalList;
							strHyokaKbn = "総合";
						}else{
							break;
						}

						if(tmpAddList!=null){
							//評価区分セット
							workCell = cm.setCell( workSheet, workRow, workCell, intRow, 2);
							workCell.setCellValue( strHyokaKbn );
							//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
							cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
																intRow, intRow, 2, 3, false, true, false, false);

							//日程セット
							if((firstFlg)||(intLoopCnt==0)){
								workCell = cm.setCell( workSheet, workRow, workCell, intRow, 1);
								//2004.9.27 Update
//								workCell.setCellValue( "小計" );
								workCell.setCellValue( "大学計" );
								//2004.9.27 Update End
								//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
																	intRow, intRow, 1, 3, false, true, false, false);

								firstFlg = false;
							}

							//大学名セット
							if( (intLoopCnt==0) ){
								if(strDaigakuMei!=null){
									workCell = cm.setCell( workSheet, workRow, workCell, intRow, 0);
									workCell.setCellValue( strDaigakuMei );
								}
								//罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intRow, intRow, 0, 11, false, true, false, false );
							}

							//評価別人数データリストの内容をセット					
							boolean ret = setData( workSheet, tmpAddList, null, intRow );
							if( ret != true){
								return errfdata;
							}
							else{
								intRow += intNinzuListCnt;
								intLoopCnt++;
							}
						}
			
						// 改シート・改ブック処理
						if( intLoopCnt >= intSheetCngCount ){
							//改シート
							//罫線出力（セルの上部に太線を出力）
							cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
																intRow, intRow, 0, 11, false, true, false, false);

							strDaigakuCode = "";
							strNitteiCode = "";																	
							bolSheetCngFlg = true;
							if( intMaxSheetIndex >= intMaxSheetSr ){
								//改ファイル
								bolBookCngFlg = true; 
								bolBookClsFlg = true; 
							}
			
							if( bolSheetCngFlg == true ){
				
								if( bolBookCngFlg == true ){

									if(bolBookClsFlg == true ){
										//WorkBook保存
										boolean bolRet = false;
										bolRet = clsBook( workbook, intMaxSheetIndex, intBookCngCount,
												intSaveFlg, outfilelist, UserID, itr.hasNext(), false );
										if( bolRet == false ){
											return errfwrite;
										}
										intBookCngCount++;
										bolBookClsFlg = false;
									}
					
									// マスタExcel読み込み
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									//2004.9.28 Update 
//									if( workbook.equals(null) ){
									if( workbook == null ){
									//2004.9.28 Update End
										return errfread;
									}
				
									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
								}
				
								//2004.9.27 Add
								//行削除処理
								if( workSheet != null ){
									int intRemoveStartRow = ( intSheetCngCount * intNinzuListCnt ) + intDataStartRow;
									if( intMaxRow + intDataStartRow > intRemoveStartRow ){
										for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
											//行削除処理
											workSheet.removeRow(workSheet.getRow(index));
										}
										//罫線出力（セルの上部に太線を出力）
										cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
														intRemoveStartRow, intRemoveStartRow, 0, 11, false, true, false, false);
									}
								}
								//2004.9.27 Add End

								//シート作成
								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);

								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);

								// ヘッダー
								//2004.9.28 Update
//								setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
//													 s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget );
								setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
													 s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget, strKokushiKbn );
								//2004.9.28 Update End

								intMaxSheetIndex++;		//シート数カウンタインクリメント
								bolSheetCngFlg = false;

								intRow = intDataStartRow;
								intLoopCnt = 0;

							}
						}
					}
					addCenterList	= null;
					addSecondList	= null;
					addTotalList	= null;
					tmpAddList		= null;

//					if( !cm.toString(strGenekiKbn).equals(cm.toString(s24ListBean.getStrGenKouKbn())) && intLoopCnt!=0 ){
					if( !cm.toString(strGenekiKbn).equals(cm.toString(s24ListBean.getStrGenKouKbn())) ){
						strGenekiKbn = s24ListBean.getStrGenKouKbn();
						if( cm.toString(strGenekiKbn).equals("0") ){
							strDispTarget = "全体";
						}
						else if( cm.toString(strGenekiKbn).equals("1") ){
							strDispTarget = "現役生";
						}
						else if( cm.toString(strGenekiKbn).equals("2") ){
							strDispTarget = "高卒生";
						}
						
						if(intLoopCnt == 0){
							// ヘッダー
							setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
											s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget, strKokushiKbn );
						}
						else{
							
							// 改シート・改ブック判定
							strDaigakuCode = "";
							strNitteiCode = "";		
							if( bolSheetCngFlg == false ){
								//罫線出力（セルの上部に太線を出力）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
																	intRow, intRow, 0, 11, false, true, false, false);
								bolSheetCngFlg = true;
								//intMaxSheetIndex++;
								if( intMaxSheetIndex >= intMaxSheetSr ){
									//改ファイル
									bolBookCngFlg = true; 
									bolBookClsFlg = true;
								}
								// 改シート・改ブック処理
								if( bolSheetCngFlg == true ){

									if( bolBookCngFlg == true ){
							
										if(bolBookClsFlg == true ){
											//WorkBook保存
											boolean bolRet = false;
											// Excelファイル保存
											bolRet = clsBook( workbook, intMaxSheetIndex, intBookCngCount,
													intSaveFlg, outfilelist, UserID, itr.hasNext(), false );
											if( bolRet == false ){
												return errfwrite;
											}
											intBookCngCount++;
											bolBookClsFlg = false;
										}
							
										// マスタExcel読み込み
										workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
										//2004.9.28 Update 
	//									if( workbook.equals(null) ){
										if( workbook == null ){
										//2004.9.28 Update End
											return errfread;
										}
						
										bolBookCngFlg = false;
										intMaxSheetIndex = 0;
									}
						
									//2004.9.27 Add
									//行削除処理
									if( workSheet != null ){
										int intRemoveStartRow = ( intSheetCngCount * intNinzuListCnt ) + intDataStartRow;
										if( intMaxRow + intDataStartRow > intRemoveStartRow ){
											for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
												//行削除処理
												workSheet.removeRow(workSheet.getRow(index));
											}
											//罫線出力（セルの上部に太線を出力）
											cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
															intRemoveStartRow, intRemoveStartRow, 0, 11, false, true, false, false);
										}
									}
									//2004.9.27 Add End

									//シート作成
									// シートテンプレートのコピー
									workSheet = workbook.cloneSheet(0);

									// ヘッダ右側に帳票作成日時を表示する
									cm.setHeader(workbook, workSheet);

									// ヘッダー
									//2004.9.28 Update 
	//								setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
	//													s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget );
									setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
														s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget, strKokushiKbn );
									//2004.9.28 Update End 

									intMaxSheetIndex++;		//シート数カウンタインクリメント
									bolSheetCngFlg = false;

									intRow = intDataStartRow;
									intLoopCnt = 0;

								}
							}
						}
					}
					
					//2004.9.27 Add
					intDaigakuCd = (int)Integer.parseInt(s24ListBean.getStrDaigakuCd());
					if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
						strKokushiKbn = "国公立大";
					}
					if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
						strKokushiKbn = "私立大";
					}
					if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
						strKokushiKbn = "短大";
					}
					if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
						strKokushiKbn = "その他";
					}
				
					if( (!cm.toString(strKokushiKbn).equals(cm.toString(strStKokushiKbn))) && intLoopCnt!=0 ){
						strStKokushiKbn = strKokushiKbn;
						// 改シート・改ブック判定
						strDaigakuCode = "";
						strNitteiCode = "";		
						if( bolSheetCngFlg == false ){
							//罫線出力（セルの上部に太線を出力）
							cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
																intRow, intRow, 0, 11, false, true, false, false);
							bolSheetCngFlg = true;
							//intMaxSheetIndex++;
							if( intMaxSheetIndex >= intMaxSheetSr ){
								//改ファイル
								bolBookCngFlg = true; 
								bolBookClsFlg = true;
							}
							// 改シート・改ブック処理
							if( bolSheetCngFlg == true ){

								if( bolBookCngFlg == true ){
						
									if(bolBookClsFlg == true ){
										//WorkBook保存
										boolean bolRet = false;
										// Excelファイル保存
										bolRet = clsBook( workbook, intMaxSheetIndex, intBookCngCount,
												intSaveFlg, outfilelist, UserID, itr.hasNext(), false );
										if( bolRet == false ){
											return errfwrite;
										}
										intBookCngCount++;
										bolBookClsFlg = false;
									}
						
									// マスタExcel読み込み
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									//2004.9.28 Update 
//									if( workbook.equals(null) ){
									if( workbook == null ){
									//2004.9.28 Update End
										return errfread;
									}
					
									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
								}
					
								//2004.9.27 Add
								//行削除処理
								if( workSheet != null ){
									int intRemoveStartRow = ( intSheetCngCount * intNinzuListCnt ) + intDataStartRow;
									if( intMaxRow + intDataStartRow > intRemoveStartRow ){
										for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
											//行削除処理
											workSheet.removeRow(workSheet.getRow(index));
										}
										//罫線出力（セルの上部に太線を出力）
										cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
														intRemoveStartRow, intRemoveStartRow, 0, 11, false, true, false, false);
									}
								}
								//2004.9.27 Add End

								//シート作成
								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);

								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);

								// ヘッダー
								//2004.9.28 Update
//								setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
//													s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget );
								setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
													s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget, strKokushiKbn );
								//2004.9.28 Update End

								intMaxSheetIndex++;		//シート数カウンタインクリメント
								bolSheetCngFlg = false;

								intRow = intDataStartRow;
								intLoopCnt = 0;

							}
						}
//						bolSheetCngFlg = true;
					}
					//2004.9.27 Add End
				}
				
				//評価区分セット
//				if( !cm.toString(strHyoukaKbn).equals(cm.toString(s24ListBean.getStrHyouka())) ){
				String strHyoka = "";
				switch( Integer.parseInt(s24ListBean.getStrHyouka()) ){
					case 1:
						strHyoka = "センター";
						if(addCenterList!=null){
							tmpAddList = addCenterList;
						}
						break;
					case 2:
						strHyoka = "2次・私大";
						if(addSecondList!=null){
							tmpAddList = addSecondList;
						}
						break;
					case 3:
						strHyoka = "総合";
						if(addTotalList!=null){
							tmpAddList = addTotalList;
						}
						break;
				}
				workCell = cm.setCell( workSheet, workRow, workCell, intRow, 2);
				workCell.setCellValue( strHyoka );
				
				//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
				cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
													intRow, intRow, 2, 3, false, true, false, false);
					
//				}
				
				//日程セット → 大学が変わったときは処理する 2005.05.25
//				if( !cm.toString(strNitteiCode).equals(cm.toString(s24ListBean.getStrNtiCd()))){
				if( !cm.toString(strNitteiCode).equals(cm.toString(s24ListBean.getStrNtiCd())) ||
						!cm.toString(strDaigakuCode).equals(cm.toString(s24ListBean.getStrDaigakuCd())) ){
					if( s24ListBean.getStrNittei()!=null ){
						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 1);
						workCell.setCellValue( s24ListBean.getStrNittei() );
					}
					strNitteiCode = s24ListBean.getStrNtiCd();

					//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
					cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
														intRow, intRow, 1, 3, false, true, false, false);
				}

				//大学名セット
				if( !cm.toString(strDaigakuCode).equals(cm.toString(s24ListBean.getStrDaigakuCd()))){
					if( s24ListBean.getStrDaigakuMei()!=null ){
						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 0);
						workCell.setCellValue( s24ListBean.getStrDaigakuMei() );
					}
					strDaigakuCode = s24ListBean.getStrDaigakuCd();
					strDaigakuCodeHoji = s24ListBean.getStrDaigakuCd();
					strDaigakuMei = s24ListBean.getStrDaigakuMei();
					//罫線出力(セルの上部に太線を引く）
					cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
														intRow, intRow, 0, 11, false, true, false, false );
					
				}

				//評価別人数データリストの内容をセット					
				boolean ret = setData( workSheet, deepCopy( s24HyoukaNinzuList ), tmpAddList, intRow );
				if( ret != true){
					return errfdata;
				}
				else{
					intRow += intNinzuListCnt;
					intLoopCnt++;
				}

				// set Total
				switch( Integer.parseInt(s24ListBean.getStrHyouka()) ){
					case 1:
						if(tmpAddList==null){
							addCenterList = deepCopy( s24ListBean.getS24HyoukaNinzuList() );
						}else{
							addCenterList = deepCopy( tmpAddList );
						}
						break;
					case 2:
						if(tmpAddList==null){
							addSecondList = deepCopy( s24ListBean.getS24HyoukaNinzuList() );
						}else{
							addSecondList = deepCopy( tmpAddList );
						}
						break;
					case 3:
						if(tmpAddList==null){
							addTotalList = deepCopy( s24ListBean.getS24HyoukaNinzuList() );
						}else{
							addTotalList = deepCopy( tmpAddList );
						}
						break;
				}
				
				if( intLoopCnt >= intSheetCngCount ){
					//改シート
					//罫線出力（セルの上部に太線を出力）
					cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
														intRow, intRow, 0, 11, false, true, false, false);

					strDaigakuCode = "";
					strNitteiCode = "";																	
					bolSheetCngFlg = true;
					if( intMaxSheetIndex >= intMaxSheetSr ){
						//改ファイル
						bolBookCngFlg = true; 
						bolBookClsFlg = true; 
					}
				}
				
				
			}

			// 最後に小計の書き込み
			// データセット
			boolean firstFlg = true;
			for(int a=0;a<3;a++){
				
				String strHyokaKbn = "";
				if(a==0){
					tmpAddList = addCenterList;
					strHyokaKbn = "センター";
				}else if(a==1){
					tmpAddList = addSecondList;
					strHyokaKbn = "2次・私大";
				}else if(a==2){
					tmpAddList = addTotalList;
					strHyokaKbn = "総合";
				}else{
					break;
				}
				
				if( tmpAddList != null && bolSheetCngFlg == true ){
					if( intMaxSheetIndex >= intMaxSheetSr ){
						//改ファイル
						bolBookCngFlg = true; 
						bolBookClsFlg = true; 
					}
			
					if( bolSheetCngFlg == true ){
				
						if( bolBookCngFlg == true ){

							if(bolBookClsFlg == true ){
								//WorkBook保存
								boolean bolRet = false;
								bolRet = clsBook( workbook, intMaxSheetIndex, intBookCngCount,
										intSaveFlg, outfilelist, UserID, itr.hasNext(), false );
								if( bolRet == false ){
									return errfwrite;
								}
								intBookCngCount++;
								bolBookClsFlg = false;
							}
					
							// マスタExcel読み込み
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							//2004.9.28 Update 
//							if( workbook.equals(null) ){
							if( workbook == null ){
							//2004.9.28 Update End
								return errfread;
							}
				
							bolBookCngFlg = false;
							intMaxSheetIndex = 0;
						}
				
						//2004.9.27 Add
						//行削除処理
						if( workSheet != null ){
							int intRemoveStartRow = ( intSheetCngCount * intNinzuListCnt ) + intDataStartRow;
							if( intMaxRow + intDataStartRow > intRemoveStartRow ){
								for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
									//行削除処理
									workSheet.removeRow(workSheet.getRow(index));
								}
								//罫線出力（セルの上部に太線を出力）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
												intRemoveStartRow, intRemoveStartRow, 0, 11, false, true, false, false);
							}
						}
						//2004.9.27 Add End

						//シート作成
						// シートテンプレートのコピー
						workSheet = workbook.cloneSheet(0);

						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);

						// ヘッダー
						//2004.9.28 Update
//						setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
//											s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget );
						setHeader( workbook, workSheet, workCell, workRow, s24Item.getIntSecuFlg(), s24Item.getStrGakkomei(),
											s24Item.getStrMshmei(), s24Item.getStrMshDate(), s24Item.getStrMshCd(), strDispTarget, strKokushiKbn );

						intMaxSheetIndex++;		//シート数カウンタインクリメント
						bolSheetCngFlg = false;

						intRow = intDataStartRow;
						intLoopCnt = 0;

					}
				}
				
				if(tmpAddList!=null){
					//評価区分セット
					workCell = cm.setCell( workSheet, workRow, workCell, intRow, 2);
					workCell.setCellValue( strHyokaKbn );
					//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
					cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
														intRow, intRow, 2, 3, false, true, false, false);

					//日程セット
					if((firstFlg)||(intLoopCnt==0)){
						workCell = cm.setCell( workSheet, workRow, workCell, intRow, 1);
						//2004.9.27 Update
//						workCell.setCellValue( "小計" );
						workCell.setCellValue( "大学計" );
						//2004.9.27 Update End
						//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
															intRow, intRow, 1, 3, false, true, false, false);

						firstFlg = false;
					}

					//大学名セット
					if( (intLoopCnt==0) ){
						if(strDaigakuMei!=null){
							workCell = cm.setCell( workSheet, workRow, workCell, intRow, 0);
							workCell.setCellValue( strDaigakuMei );
						}
						//罫線出力(セルの上部に太線を引く）
						cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
															intRow, intRow, 0, 11, false, true, false, false );
						firstFlg = true;

				
					}

					//評価別人数データリストの内容をセット					
					boolean ret = setData( workSheet, tmpAddList, null, intRow );
					if( ret != true){
						return errfdata;
					}
					else{
						intRow += intNinzuListCnt;
						intLoopCnt++;
					}
				}
			
				// 改シート・改ブック処理
				if(( intLoopCnt >= intSheetCngCount )&&
					(((a==0)&&(addSecondList!=null||addTotalList!=null))||(a==1&&addTotalList!=null)) ){
					//改シート
					//罫線出力（セルの上部に太線を出力）
					cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
														intRow, intRow, 0, 11, false, true, false, false);

					strDaigakuCode = "";
					strNitteiCode = "";																	
					bolSheetCngFlg = true;
				}
			}
			addCenterList	= null;
			addSecondList	= null;
			addTotalList	= null;
			tmpAddList		= null;
			
			//2004.7.28 Update 
//			if(bolBookCngFlg == false){
//
//				//罫線出力（セルの上部に太線を出力）
//				cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
//													intRow, intRow, 0, 11, false, true, false, false);
//				// Excelファイル保存
//				boolean bolRet = false;
//				bolRet = clsBook( workbook, workSheet, workCell, workRow, intMaxSheetIndex,
//									intBookCngCount, intSaveFlg, outfilelist, UserID, itr.hasNext(), true );
//				if( bolRet == false ){
//					return errfwrite;
//						
//				}
//			}
			if(workbook != null){

				//罫線出力（セルの上部に太線を出力）
				cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
													intRow, intRow, 0, 11, false, true, false, false);
													
				//行削除処理
				if( workSheet != null ){
					int intRemoveStartRow = ( intSheetCngCount * intNinzuListCnt ) + intDataStartRow;
					if( intMaxRow + intDataStartRow > intRemoveStartRow ){
						for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
							//行削除処理
							workSheet.removeRow(workSheet.getRow(index));
						}
						//罫線出力（セルの上部に太線を出力）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
										intRemoveStartRow, intRemoveStartRow, 0, 11, false, true, false, false);
					}
				}

			}
			// Excelファイル保存
			boolean bolRet = false;
			bolRet = clsBook( workbook, intMaxSheetIndex, intBookCngCount, intSaveFlg,
					outfilelist, UserID, itr.hasNext(), true );
			if( bolRet == false ){
				return errfwrite;
					
			}
			//2004.7.28 Update End
			
		}
		catch( Exception e ){
			e.printStackTrace();
			return errfdata;
		}

		return noerror;
	}
	
	/**
	 * 
	 * @param workSheet
	 * @param s24HyoukaNinzuList
	 * @param addList
	 * @param intRow
	 * @return
	 */
	private boolean clsBook( HSSFWorkbook workbook, int intMaxSheetIndex,
			int intBookCngCount, int intSaveFlg, ArrayList outfilelist,
			String UserID, boolean hasNext, boolean lastFlg ) throws Exception {
		
		try{
			//WorkBook保存

			boolean bolRet = false;
			// Excelファイル保存
			if(lastFlg==false){
				if(hasNext == true){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
				}
				else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}
			}else{
				if(intBookCngCount > 0 ){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex);
				}
				else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}
			}

			return bolRet;

		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 
	 * @param workSheet
	 * @param s24HyoukaNinzuList
	 * @param addList
	 * @param intRow
	 * @return
	 */
	//2004.9.28 Update
//	private void setHeader( HSSFWorkbook workbook, HSSFSheet workSheet, HSSFCell workCell, HSSFRow workRow,
//							int secuFlg, String gakkomei, String mshmei, String mshDate, String mshCd, String dispTarget )
//							throws Exception{
	private void setHeader( HSSFWorkbook workbook, HSSFSheet workSheet, HSSFCell workCell, HSSFRow workRow,
							int secuFlg, String gakkomei, String mshmei, String mshDate, String mshCd, String dispTarget, String KokushiKbn )
							throws Exception{
	//2004.9.28 Update End
		try{
			
			// セキュリティスタンプセット
			String secuMes = cm.setSecurity( workbook, workSheet, secuFlg, 9, 11 );
			workCell = cm.setCell( workSheet, workRow, workCell, 0, 9 );
			workCell.setCellValue(secuMes);
			
			// 学校名セット
			workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
			workCell.setCellValue( "学校名　：" + cm.toString(gakkomei) );
	
			// 模試月取得
			String moshi =cm.setTaisyouMoshi( mshDate );
			// 対象模試セット
			workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
			workCell.setCellValue( cm.getTargetExamLabel() + "：" +  cm.toString(mshmei) + moshi);
				
			//表示対象
			workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
			workCell.setCellValue( "表示対象：" +  dispTarget );
				
			//表示対象
			workCell = cm.setCell( workSheet, workRow, workCell, 6, 7 );
			workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_A);
			workCell = cm.setCell( workSheet, workRow, workCell, 6, 8 );
			workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_B);
			workCell = cm.setCell( workSheet, workRow, workCell, 6, 9 );
			workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_C);
			workCell = cm.setCell( workSheet, workRow, workCell, 6, 10 );
			workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_D);
			workCell = cm.setCell( workSheet, workRow, workCell, 6, 11 );
			workCell.setCellValue(JudgementConstants.Judge.JUDGE_STR_E);
			
			//2004.9.28 Add
			//国私区分
			if( KokushiKbn.equals("国公立大")){
				workCell = cm.setCell( workSheet, workRow, workCell, 6, 6 );
				workCell.setCellValue( "出願予定" );
			}
			else{
				workCell = cm.setCell( workSheet, workRow, workCell, 6, 6 );
				workCell.setCellValue( "第1志望" );
			}
			//2004.9.28 Add End
			
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * ListのDeepCopyを行う。
	 * @param inList
	 * @return
	 */
	private ArrayList deepCopy( ArrayList inList )throws Exception{
		try{
			ArrayList outList = new ArrayList(5);

			if(( inList==null )||(inList.size()==0)){
				return null;
			}else{
				for(int a=0; a<inList.size();a++){
					S24HyoukaNinzuListBean inS24HyoukaNinzuListBean		= new S24HyoukaNinzuListBean();
					S24HyoukaNinzuListBean outS24HyoukaNinzuListBean	= new S24HyoukaNinzuListBean();
					inS24HyoukaNinzuListBean = (S24HyoukaNinzuListBean)inList.get(a);

					// データの移し変え
					outS24HyoukaNinzuListBean.setFloHensaAll( inS24HyoukaNinzuListBean.getFloHensaAll() );
					outS24HyoukaNinzuListBean.setFloHensaHome( inS24HyoukaNinzuListBean.getFloHensaHome() );
					outS24HyoukaNinzuListBean.setFloTokuritsuAll( inS24HyoukaNinzuListBean.getFloTokuritsuAll() );
					outS24HyoukaNinzuListBean.setFloTokuritsuHome( inS24HyoukaNinzuListBean.getFloTokuritsuHome() );
					outS24HyoukaNinzuListBean.setIntDai1shibo( inS24HyoukaNinzuListBean.getIntDai1shibo() );
					outS24HyoukaNinzuListBean.setIntHyoukaA( inS24HyoukaNinzuListBean.getIntHyoukaA() );
					outS24HyoukaNinzuListBean.setIntHyoukaB( inS24HyoukaNinzuListBean.getIntHyoukaB() );
					outS24HyoukaNinzuListBean.setIntHyoukaC( inS24HyoukaNinzuListBean.getIntHyoukaC() );
					outS24HyoukaNinzuListBean.setIntHyoukaD( inS24HyoukaNinzuListBean.getIntHyoukaD() );
					outS24HyoukaNinzuListBean.setIntHyoukaE( inS24HyoukaNinzuListBean.getIntHyoukaE() );
					outS24HyoukaNinzuListBean.setIntSoshiboAll( inS24HyoukaNinzuListBean.getIntSoshiboAll() );
					outS24HyoukaNinzuListBean.setIntSoshiboHome( inS24HyoukaNinzuListBean.getIntSoshiboHome() );
					outS24HyoukaNinzuListBean.setStrNendo(inS24HyoukaNinzuListBean.getStrNendo());

					outList.add( outS24HyoukaNinzuListBean );
				}

				return outList;
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
		
	/*
	 * 	Excel編集
	 * 		HSSFWorkbook 	workbook: Excelワークブック
	 * 		ArrayList 		s24HyoukaNinzuList:評価別人数リスト
	 * 		int				intRow:開始行
	 * 		戻り値: なし
	 */
	private boolean setData(HSSFSheet workSheet, ArrayList s24HyoukaNinzuList, ArrayList addList, int intRow ){

		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		int		rownum			= 0;
		int		row = intRow;
		boolean	nullFlg 		= true;
		
		
		// 基本ファイルを読込む
		S24HyoukaNinzuListBean s24HyoukaNinzuListBean = new S24HyoukaNinzuListBean();
		S24HyoukaNinzuListBean s24AddNinzuListBean = new S24HyoukaNinzuListBean();

		try{
			if( addList != null ){
				nullFlg=false;
			}
			
			Iterator itr = s24HyoukaNinzuList.iterator();
			
			while( itr.hasNext() ){
				
				//データリストより各項目を取得する
				s24HyoukaNinzuListBean = (S24HyoukaNinzuListBean)itr.next();
				if(nullFlg==false){
					s24AddNinzuListBean = (S24HyoukaNinzuListBean)addList.get(rownum);
				}

				//模試年度
				if(s24HyoukaNinzuListBean.getStrNendo()!=null){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 3);
					workCell.setCellValue( s24HyoukaNinzuListBean.getStrNendo() + "年度" );
				}
				
				//全国総志望者数
				if( s24HyoukaNinzuListBean.getIntSoshiboAll() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 4);
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntSoshiboAll() );

					if(nullFlg==false){
						if(s24AddNinzuListBean.getIntSoshiboAll()!=-999.0){
							s24AddNinzuListBean.setIntSoshiboAll(s24AddNinzuListBean.getIntSoshiboAll() + s24HyoukaNinzuListBean.getIntSoshiboAll());
						}else{
							s24AddNinzuListBean.setIntSoshiboAll(s24HyoukaNinzuListBean.getIntSoshiboAll());
						}
					}
				}
				
				//志望者数（総志望）
				if( s24HyoukaNinzuListBean.getIntSoshiboHome() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 5);
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntSoshiboHome() );

					if(nullFlg==false){
						if(s24AddNinzuListBean.getIntSoshiboHome()!=-999.0){
							s24AddNinzuListBean.setIntSoshiboHome(s24AddNinzuListBean.getIntSoshiboHome() + s24HyoukaNinzuListBean.getIntSoshiboHome());
						}else{
							s24AddNinzuListBean.setIntSoshiboHome(s24HyoukaNinzuListBean.getIntSoshiboHome());
						}
					}
				}
				
				//志望者数（第一志望）
				if( s24HyoukaNinzuListBean.getIntDai1shibo() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 6);
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntDai1shibo() );

					if(nullFlg==false){
						if(s24AddNinzuListBean.getIntDai1shibo()!=-999.0){
							s24AddNinzuListBean.setIntDai1shibo(s24AddNinzuListBean.getIntDai1shibo() + s24HyoukaNinzuListBean.getIntDai1shibo());
						}else{
							s24AddNinzuListBean.setIntDai1shibo(s24HyoukaNinzuListBean.getIntDai1shibo());
						}
					}
				}
				
				//評価別人数
				if( s24HyoukaNinzuListBean.getIntHyoukaA() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 7 );
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntHyoukaA() );

					if(nullFlg==false){
						if(s24AddNinzuListBean.getIntHyoukaA()!=-999.0){
							s24AddNinzuListBean.setIntHyoukaA(s24AddNinzuListBean.getIntHyoukaA() + s24HyoukaNinzuListBean.getIntHyoukaA());
						}else{
							s24AddNinzuListBean.setIntHyoukaA(s24HyoukaNinzuListBean.getIntHyoukaA());
						}
					}
				}

				if( s24HyoukaNinzuListBean.getIntHyoukaB() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 8 );
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntHyoukaB() );

					if(nullFlg==false){
						if(s24AddNinzuListBean.getIntHyoukaB()!=-999.0){
							s24AddNinzuListBean.setIntHyoukaB(s24AddNinzuListBean.getIntHyoukaB() + s24HyoukaNinzuListBean.getIntHyoukaB());
						}else{
							s24AddNinzuListBean.setIntHyoukaB(s24HyoukaNinzuListBean.getIntHyoukaB());
						}
					}
				}

				if( s24HyoukaNinzuListBean.getIntHyoukaC() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 9 );
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntHyoukaC() );

					if(nullFlg==false){
						if(s24AddNinzuListBean.getIntHyoukaC()!=-999.0){
							s24AddNinzuListBean.setIntHyoukaC(s24AddNinzuListBean.getIntHyoukaC() + s24HyoukaNinzuListBean.getIntHyoukaC());
						}else{
							s24AddNinzuListBean.setIntHyoukaC(s24HyoukaNinzuListBean.getIntHyoukaC());
						}
					}
				}

				if( s24HyoukaNinzuListBean.getIntHyoukaD() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 10 );
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntHyoukaD() );

					if(nullFlg==false){
						if(s24AddNinzuListBean.getIntHyoukaD()!=-999.0){
							s24AddNinzuListBean.setIntHyoukaD(s24AddNinzuListBean.getIntHyoukaD() + s24HyoukaNinzuListBean.getIntHyoukaD());
						}else{
							s24AddNinzuListBean.setIntHyoukaD(s24HyoukaNinzuListBean.getIntHyoukaD());
						}
					}
				}

				if( s24HyoukaNinzuListBean.getIntHyoukaE() != -999 ){
					workCell = cm.setCell( workSheet, workRow, workCell, row, 11 );
					workCell.setCellValue( s24HyoukaNinzuListBean.getIntHyoukaE() );

					if(nullFlg==false){
						if(s24AddNinzuListBean.getIntHyoukaE()!=-999.0){
							s24AddNinzuListBean.setIntHyoukaE(s24AddNinzuListBean.getIntHyoukaE() + s24HyoukaNinzuListBean.getIntHyoukaE());
						}else{
							s24AddNinzuListBean.setIntHyoukaE(s24HyoukaNinzuListBean.getIntHyoukaE());
						}
					}
				}

				if(nullFlg==false){
					addList.set(rownum,s24AddNinzuListBean);
				}

				row++;
				rownum++;

			}
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
}

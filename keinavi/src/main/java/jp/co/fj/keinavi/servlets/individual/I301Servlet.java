package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.keinavi.beans.NewestExamDivSearchBean;
import jp.co.fj.keinavi.beans.SubRecordASearchBean;
import jp.co.fj.keinavi.beans.individual.DetermHstDeleteBean;
import jp.co.fj.keinavi.beans.individual.DockedSubRecBean;
import jp.co.fj.keinavi.beans.individual.ExamSearchBean;
import jp.co.fj.keinavi.beans.individual.I301Bean;
import jp.co.fj.keinavi.beans.individual.JudgeByUnivBean;
import jp.co.fj.keinavi.beans.individual.judgement.JudgementListBean;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.I301Data;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I301Form;
import jp.co.fj.keinavi.judgement.beans.ScoreKeiNavi;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.individual.IPropsLoader;

/**
 *
 * 受験予定大学の登録
 * ・受験予定大学数は一人あたりＭａｘ40学科とし、受験予定帳から受験予定大学に登録時に40を超えるか
 *　すでに40登録されているときは、警告ダイアログを表示させ、登録できないようにする。
 * ・各個人が模試に記入した志望大学について
 *　判定履歴画面にて判定結果一覧の大学と模試に記入した志望大学を表示し、
 *　受験予定大学に登録が可能
 *
 *  一次：判定対象模試は 前回判定した模試
 *  
 *	2004.8.3	[新規作成]
 *  2005.3.2	T.Yamada	[1]二次：判定対象模試は�@模試で記入された大学は選択中の模試�A判定履歴は前回判定した模試
 *  2005.2.24 	K.Kondo 	[2]change 模試で記入された大学は、対象模試を使用するように修正
 * 	2005.03.08 	K.Kondo     [3]I301Beanに担当クラスの年度を渡すように修正
 *  2005.4.6	T.Yamada	[4]模試で記入された大学は対象模試がマークと記述以外ならば判定せずに、詳細リンクを出さない
 *  2005.4.8 	T.Yamada 	[5]ScoreBeanの[35]に伴う修正
 *  
 * <2010年度改修>
 * 2009.10.02	Tomohisa YAMADA - Totec
 * 				入試日程変更
 * 
 * 2010.04.08	Tomohisa YAMADA - Totec
 * 				数学�@の偏差値を動的に算出する
 *
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class I301Servlet extends IndividualServlet {
	
	/**
	 * @see jp.co.fj.keinavi.servlets.individual.IndividualServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
			
		super.execute(request, response);	
		
		// フォームデータを取得
		final I301Form i301Form = (I301Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.individual.I301Form");
		
		// 大学データが未ロードならエラーページに遷移する
		if (super.getUnivAll() == null || super.getUnivAll().size() == 0) {
			request.setAttribute("form", i301Form);
			forward(request, response, JSP_ONLOAD_UNIV);
			return;
		}
			
		// HTTPセッション
		final HttpSession session = request.getSession();
		// プロファイル情報
		final Profile profile = getProfile(request);
		// 個人共通
		final ICommonMap iCommonMap = getICommonMap(request);
		// 模試データ
		final ExamSession examSession = getExamSession(request);
		// 最新大学マスタ情報
		final NewestExamDivSearchBean newest = getNewestExamDivSearchBean();

		// 転送:JSP
		if ("i301".equals(getForward(request))) {
			
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(false);
				
				/** 一覧情報 */
				// 模試で記入されたのから表示
				// 判定履歴からの表示
				final I301Bean i301Bean = new I301Bean();
				i301Bean.setConnection("", con);
				i301Bean.setTargetExamYear(iCommonMap.getTargetExamYear());//[2] add
				i301Bean.setTargetExamCd(iCommonMap.getTargetExamCode());//[2] add
				i301Bean.setIndividualId(i301Form.getTargetPersonId());
				i301Bean.setClassYear(ProfileUtil.getChargeClassYear(profile));//[3] add
				i301Bean.setNewestExamDiv(newest);
				
				String message = null;
				
				// 登録
				if ("registerFromJudge".equals(i301Form.getButton())) {
					// 模試で記入された大学を登録
					i301Bean.setUnivValueAtExam(i301Form.getUnivValueAtExam());
					// 判定履歴の大学を登録
					i301Bean.setUnivValueAtJudge(i301Form.getUnivValueAtJudge());
					
					//メッセージを一旦記憶
					message = i301Bean.getErrorMessage();
			   	} 
				//判定履歴の大学を削除
				else if ("deleteFromJudge".equals(i301Form.getButton())) {			   		
			   		//メッセージを一旦記憶
			   		message = delete(con, i301Form);
			   	}
				
				//登録・削除がある場合はかならずその後に実行
				i301Bean.execute();
				
				//削除処理を分離した為、メッセージを再設定する必要がある
				i301Bean.setErrorMessage(message);
				
				//●対象模試情報の取得 12/18 add
				boolean isJudgable = false;//[4] add
				for (int i = 0; i < 2; i++) {
					String targetYear;
					String targetCode;
					ExaminationData targetExam;
					//■�@模試で記入された大学
					if (i == 0) {
						targetYear = iCommonMap.getTargetExamYear();//選択中の模試年度
						targetCode = iCommonMap.getTargetExamCode();//選択中の模試コード
						targetExam = getTargetExam(con, targetYear, targetCode);
						//[4] add start
						//マークと記述のみ判定可能
						if (targetExam.getExamTypeCd().equals(IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_MARK"))
								|| targetExam.getExamTypeCd().equals(IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_WRTN"))) {
							isJudgable = true;
						}
						//[4] add end
					}
					//■�A判定履歴
					else {
						targetYear = i301Bean.getExamYear();//判定履歴模試年度
						targetCode = i301Bean.getExamCd();//判定履歴模試コード
						targetExam = getTargetExam(con, targetYear, targetCode);
						//絶対に判定する
						isJudgable = true;//[4] add
					}
					
					if(i == 1 || (i == 0 && isJudgable)){//[4] add
											
						//▲ドッキング先模試情報の取得
						DockedSubRecBean dsrb2 = new DockedSubRecBean();
						dsrb2.setIndividualId(i301Form.getTargetPersonId());
						dsrb2.setExamYear(targetYear);
						dsrb2.setExamCd(targetCode);
						dsrb2.setExamSession(examSession);
						dsrb2.setConnection("", con);
						dsrb2.execute();
						//▲ScoreBeanに取得してきた成績をセット
						Score sb2 = new ScoreKeiNavi(								
								SubRecordASearchBean.searchNullSet(
										dsrb2.getDockedBean().getExaminationDatas()[0], 
										con));
						
						sb2.setIndividualId(i301Form.getTargetPersonId());
						sb2.setMark(dsrb2.getMark());
						sb2.setWrtn(dsrb2.getWrtn());
						sb2.setCExam(dsrb2.getDockedBean().getExaminationDatas()[0]);
						sb2.setSExam(dsrb2.getDockedBean().getExaminationDatas()[1]);
						//sb2.setTargetExam(sb2.getCExam());//4段階または5段階の決定に必要 //[5] del
						sb2.setTargetExam(targetExam);//[5] add
						sb2.execute();
						
						Collection i301Datas = null;
						//■�@模試で記入された大学
						if(i == 0){
							i301Datas = i301Bean.getI301DatasAtExam();
						}
						//■�A判定履歴
						else if(i == 1){
							i301Datas = i301Bean.getI301DatasAtJudge();
						}
						Set set = new HashSet();
						Iterator ite = i301Datas.iterator();
						while(ite.hasNext()){
							I301Data data = (I301Data)ite.next();
							String[] univValue = new String[3];
							univValue[0] = data.getUnivCd();//univCd
							univValue[1] = data.getFacultyCd();//facultyCd
							univValue[2] = data.getDeptCd();//deptCd
							set.add(univValue);
						}
						String[][] univArray = (String[][]) set.toArray(new String[set.size()][3]);
						
						//●大学指定判定実行
						JudgementListBean listBean = new JudgementListBean();

						JudgeByUnivBean jbub = new JudgeByUnivBean();
						jbub.setUnivAllList(super.getUnivAll());
						jbub.setUnivArray(univArray);
						jbub.setScoreBean(sb2);
						jbub.setPrecedeCenterPre();
						jbub.execute();	
	
						List judgedList = jbub.getRecordSet();

						listBean.createListBean(judgedList, sb2);

					
						//■�@模試で記入された大学
						if(i == 0){
							session.setAttribute("useListBean", listBean);
							request.setAttribute("isJudgable", new Boolean(true));
						}
						//■�A判定履歴
						else if(i == 1){
							session.setAttribute("useListBean2", listBean);
						}
					//[4] add start	
					}else if(i == 0 && !isJudgable){
						request.setAttribute("isJudgable", new Boolean(false));
					}
					//[4] add end

				}
				
				con.commit();
				
				request.setAttribute("i301Bean", i301Bean);
			} catch (final Exception e) {
				rollback(con);
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			request.setAttribute("form", i301Form);
			
			iCommonMap.setTargetPersonId(i301Form.getTargetPersonId());
			forward(request, response, JSP_I301);
			
		// 転送:SERVLET
		} else {
			iCommonMap.setTargetPersonId(i301Form.getTargetPersonId());
			dispatch(request, response);
		}
	}

	// 対象模試オブジェクトを取得
	private ExaminationData getTargetExam(Connection con,
			final String examYear, final String examCd) throws Exception {
		
		ExamSearchBean bean = new ExamSearchBean();
		bean.setExamYear(examYear);
		bean.setExamCd(examCd);
		bean.setConnection("", con);
		bean.execute();
		return (ExaminationData) bean.getRecordSet().get(0);
	}
	
	/**
	 * 判定履歴を削除
	 * @param con
	 * @param i301Form
	 * @return
	 * @throws Exception
	 */
	private String delete(Connection con, I301Form i301Form) throws Exception {
   		DetermHstDeleteBean bean = 
   			new DetermHstDeleteBean(
   					i301Form.getTargetPersonId(), 
   					i301Form.getUnivDeleteValue());
   		bean.setConnection(null, con);
   		bean.execute();
   		
   		//メッセージを一旦記憶
   		return "判定履歴から削除しました。";
	}
}

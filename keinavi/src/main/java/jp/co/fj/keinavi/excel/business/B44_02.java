package jp.co.fj.keinavi.excel.business;

//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.io.FileInputStream;
//import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.business.B44GakkoListBean;
import jp.co.fj.keinavi.excel.data.business.B44HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.business.B44Item;
import jp.co.fj.keinavi.excel.data.business.B44ListBean;

import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
//import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 * 作成日: 2004/09/
 * @author A.Hasegawa
 *
 * 校内成績分析−過年度比較−志望大学評価別人数　大学（日程あり）出力処理
 * 
 * <2010年度改修>
 * 2010.01.06   Tomohisa YAMADA - Totec
 *              センター・リサーチ５段階評価対応
 * 
 */
public class B44_02 {
	
	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private String		strArea			= "A1:AH58";	//印刷範囲	
	private String		mastersheet 	= "tmp";		//テンプレートシート名
	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow		= 54;			//MAX行数
	private int		intMaxClassSr   = 6;			//MAXクラス数

	private int intDataStartRow = 6;					//データセット開始行
	private int intDataStartCol = 4;					//データセット開始列
	
	final private String masterfile0 = "B44_02" ;
	final private String masterfile1 = "B44_02" ;
	private String masterfile = "" ;
	
	final private String strCenterResearch = "38";

	/*
	 * 	Excel編集メイン
	 * 		B44Item B44Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int b44_02EditExcel(B44Item b44Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		//テンプレートの決定
		if (b44Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		/** セット処理 **/
		boolean bolBookCngFlg = true;			//改ファイルフラグ	
		boolean bolSheetCngFlg = true;			//改シートフラグ
			
		FileInputStream	fin = null;
		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFCell	workCell		= null;
		HSSFRow		workRow			= null;
		HSSFWorkbook	tmpworkbook = null;
		
		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intRow = 0;
		int intCol = 0;

		String strGenekiKbn = "";
		String strDaigakuCode = "";
		String strNtiCode = "";
		String strHyoukaKbn = "";
		String strNendo = "";
		
		//2004.09.27Add
		String strKokushiKbn = "";
		String strStKokushiKbn = "";
		int intChangeSheetRow = 0;
		//2004.09.27AddEnd

		int intLoopCnt = 0;
		int intNinzuListCnt = 0;
		int intSheetCngCount = 0;
		int intBookCngCount = 1;
		
		boolean bolDispHyoukaFlg = false;
		boolean bolDispNtiFlg = false;
		boolean bolDispDaigakuFlg = false;
		String strHyouka = "";
		
		String strDispTarget = "";
		
		ArrayList WorkBookList = new ArrayList();
		ArrayList WorkSheetList = new ArrayList();
		
		int intSheetCnt=0;
		
		int intBookIndex = -1;
		int intSheetIndex = -1;
		
		int intSheetMei = -1;
		
		int intSetRow = intDataStartRow;
		
		boolean bolClassDispFlg=true;
		
		KNLog log = KNLog.getInstance(null,null,null);
		ArrayList HyoukaTitleList = new ArrayList();
		
		log.Ep("B44_02","B44_02帳票作成開始","");

		// 基本ファイルを読込む
		B44ListBean b44ListBean = new B44ListBean();
		
		try{
			/** B44Item編集 **/
			b44Item  = editB44( b44Item );
			
			HyoukaTitleList.add(0,"A");
			HyoukaTitleList.add(1,"B");
			HyoukaTitleList.add(2,"C");

			// 基本ファイルを読込む
			ArrayList b44List = b44Item.getB44List();
			ListIterator itr = b44List.listIterator();
			
//			if( itr.hasNext() == false ){
//				return errfdata;
//			}

			/** 大学・日程・評価 **/
			// B44ListBean 
			while( itr.hasNext() ){
				b44ListBean = (B44ListBean)itr.next();
				
				// 大学名
				bolDispDaigakuFlg = false;
				if( !cm.toString(strDaigakuCode).equals(cm.toString(b44ListBean.getStrDaigakuCd()))){
					bolDispDaigakuFlg = true;
					strHyoukaKbn = "";
					strNtiCode = "";
					//大学名表示
					strDaigakuCode = b44ListBean.getStrDaigakuCd();
				}
				
				// 日程
				bolDispNtiFlg = false;
				if( !cm.toString(strNtiCode).equals(cm.toString(b44ListBean.getStrNtiCd())) ){
					bolDispNtiFlg = true;
					strHyoukaKbn = "";
					strNtiCode = b44ListBean.getStrNtiCd();
				}

				// 評価区分
				bolDispHyoukaFlg = false;
				if( !cm.toString(strHyoukaKbn).equals(cm.toString(b44ListBean.getStrHyoukaKbn())) ){
					bolDispHyoukaFlg = true;
					//評価区分表示
					strHyoukaKbn = b44ListBean.getStrHyoukaKbn();
					strHyouka = "";
					switch( Integer.parseInt(strHyoukaKbn) ){
						case 1:
							strHyouka = "センター";
							break;
						case 2:
							strHyouka = "2次・私大";
							break;
						case 3:
							strHyouka = "総合";
							break;
					}
				}

				// 表示対象
				if( !cm.toString(strGenekiKbn).equals(cm.toString(b44ListBean.getStrGenKouKbn())) ){
					strGenekiKbn = b44ListBean.getStrGenKouKbn();
					if( strGenekiKbn.equals("0") ){
						strDispTarget = "全体";
					}
					else if( strGenekiKbn.equals("1") ){
						strDispTarget = "現役生";
					}
					else if( strGenekiKbn.equals("2") ){
						strDispTarget = "高卒生";
					}

					if( bolSheetCngFlg == false ){
						bolSheetCngFlg = true;
					}
				}
				
				//2004.9.27 Add
				int intDaigakuCd = (int)Integer.parseInt(b44ListBean.getStrDaigakuCd());
				if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
					strKokushiKbn = "国公立大";
				}
				if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
					strKokushiKbn = "私立大";
				}
				if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
					strKokushiKbn = "短大";
				}
				if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
					strKokushiKbn = "その他";
				}
				
				if( (!cm.toString(strKokushiKbn).equals(cm.toString(strStKokushiKbn))) ){
					strStKokushiKbn = strKokushiKbn;
					bolSheetCngFlg = true;
				}
				//2004.9.27 Add End

				//クラス別評価人数リスト読み込み クラス名をExcelへセット
				ArrayList b44GakkoList = b44ListBean.getB44GakkoList();
				ListIterator itr2 = b44GakkoList.listIterator();

				// Listの取得
				B44GakkoListBean b44GakkoListBean = new B44GakkoListBean();
				B44GakkoListBean b44GakkoListBeanHomeData = new B44GakkoListBean();
				
				B44GakkoListBean HomeDataBean = new B44GakkoListBean();

				//クラス数取得
				int intClassSr = b44GakkoList.size();
				
				int intSheetSr = 0;
//				if( (intClassSr - 1) % intMaxClassSr > 0 || intClassSr == 1){
//					intSheetSr = (int)Math.ceil((intClassSr - 1) / intMaxClassSr) + 1;
//				}else{
//					intSheetSr = (int)Math.ceil((intClassSr - 1) / intMaxClassSr);
//				}
				if( intClassSr % intMaxClassSr > 0 ){
					intSheetSr = (int)Math.ceil(intClassSr / intMaxClassSr) + 1;
				}else{
					intSheetSr = (int)Math.ceil(intClassSr / intMaxClassSr);
				}
				
				int intLoopCnt2 = 0;
				int intNendoCount = 0;
				intChangeSheetRow = 0;
				
				boolean bolHomeDataDispFlg=false; 
				/** 学校名・学年・クラス **/
				// B44GakkoListBean
				while( itr2.hasNext() ){
					String strClassmei = "";
					b44GakkoListBean = (B44GakkoListBean)itr2.next();

					// Listの取得
					ArrayList b44HyoukaNinzuList = b44GakkoListBean.getB44HyoukaNinzuList();
					B44HyoukaNinzuListBean b44HyoukaNinzuListBean = new B44HyoukaNinzuListBean();
					ListIterator itr3 = b44HyoukaNinzuList.listIterator();

					// 自校情報表示判定
					if( intLoopCnt2 == 0 ){
						bolHomeDataDispFlg = true;
						HomeDataBean = b44GakkoListBean;
//						b44GakkoListBean = (B44GakkoListBean)itr2.next();
						intNendoCount = b44HyoukaNinzuList.size();
						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
						intLoopCnt2++;
					}

					// 始点情報（Col）
					if( intLoopCnt2 % intMaxClassSr == 0 ){
						intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 5 );
					}else{
						intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 5 );
					}
					
//					intCol = intDataStartCol + ( ( intMaxClassSr - ( intLoopCnt2 % intMaxClassSr ) ) * 5 );

					/** 年度・志望者数 **/
					// B44HyoukaNinzuListBean
					int intHyoukaNinzuListIndex = 0;
					while( itr3.hasNext() ){
						b44HyoukaNinzuListBean = (B44HyoukaNinzuListBean)itr3.next();

						// 規定行数を表示したら線を引く
						if( intSetRow  >= intChangeSheetRow  ||((bolSheetCngFlg == true)&&(intMaxSheetIndex != 0))){
							bolSheetCngFlg = true;
							for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//								int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
								int intBIndex = (intSindex + 1) / intMaxSheetSr;
								if((intSindex + 1)%intMaxSheetSr!=0){
									intBIndex++;
								}
								workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
								workSheet = (HSSFSheet)WorkSheetList.get(intSindex);
								
								// 罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intSetRow, intSetRow, 0, 33, false, true, false, false );
			
								if( workSheet != null ){
									int intRemoveStartRow = intChangeSheetRow;
									if( intMaxRow + intDataStartRow > intRemoveStartRow ){
										for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
											//行削除処理
											workSheet.removeRow(workSheet.getRow(index));
										}
										//罫線出力（セルの上部に太線を出力）
										cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
														intRemoveStartRow, intRemoveStartRow, 0, 32, false, true, false, false);
									}
								}
							}

							// 保存処理
							if( ((HSSFWorkbook)WorkBookList.get(0)).getNumberOfSheets() > intMaxSheetSr ){
								tmpworkbook = (HSSFWorkbook)WorkBookList.get(0);
				
								boolean bolRet = false;
								// Excelファイル保存
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, tmpworkbook, UserID, intBookCngCount, masterfile, intMaxSheetSr);

								if( bolRet == false ){
									return errfwrite;
								}
								intBookCngCount++;
								
								// 削除
								WorkBookList.remove(0);
								for(int a=0; a<intMaxSheetSr;a++){
									WorkSheetList.remove(0);
								}
								tmpworkbook=null;
								intBookIndex--;
								intSheetIndex -= intMaxSheetSr;
							}
						}

						// 改シート判定
						if( bolSheetCngFlg == true ){
							
							bolDispHyoukaFlg = true;
							bolDispDaigakuFlg = true;
							bolDispNtiFlg = true;
														
							intSetRow = intDataStartRow;
							bolClassDispFlg = true;
							bolHomeDataDispFlg = true;
							
							for( int intIndex = 0; intIndex < intSheetSr; intIndex ++ ){
								
								if( ( intMaxSheetIndex >= intMaxSheetSr ) || ( bolBookCngFlg == true ) ){
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook.equals(null) ){
										return errfread;
									}
									WorkBookList.add(workbook);
									intBookIndex++;
					
									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
									workbook = (HSSFWorkbook)WorkBookList.get(intBookIndex);
								}
								
								//シート作成
								// シートテンプレートのコピー
								intSheetMei = intMaxSheetIndex + 1;
								workSheet = workbook.cloneSheet(0);
								WorkSheetList.add(workSheet);
								intSheetIndex++;
								workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex);

								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);
				
								// セキュリティスタンプセット
								String secuFlg = cm.setSecurity( workbook, workSheet, b44Item.getIntSecuFlg(), 31, 33 );
								workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
								workCell.setCellValue(secuFlg);
				
//								// 学校名セット
//								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
//								workCell.setCellValue( "学校名　：" + b44Item.getStrGakkomei() );
		
								// 模試月取得
								String moshi =cm.setTaisyouMoshi( b44Item.getStrMshDate() );
								
								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
								workCell.setCellValue( "対象模試：" + cm.toString(b44Item.getStrMshmei()) + moshi);
					
								//表示対象
								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
								workCell.setCellValue( "表示対象：" + strDispTarget );
								
								//評価別人数 見出し行
								for( int i = 1; i <= intMaxClassSr; i++ ){
									int intTitleSetCol = (i * 5) + ( intDataStartCol -3 );
									
									//出願予定・第1志望
									if (strKokushiKbn == "国公立大") { 
										workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol-1 );
										workCell.setCellValue( "出願予定" );
									} else {
										workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol-1 );
										workCell.setCellValue( "第一志望" );
									}
								
									workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol );
									workCell.setCellValue( (String)HyoukaTitleList.get(0) );
									
									workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol+1 );
									workCell.setCellValue( (String)HyoukaTitleList.get(1) );
									
									workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol+2 );
									workCell.setCellValue( (String)HyoukaTitleList.get(2) );
								}
							
								intMaxSheetIndex++;		//シート数カウンタインクリメント
								bolSheetCngFlg = false;
						
								intLoopCnt = 0;
							}	//ForEnd
							
						}	//if( bolSheetCngFlg == true )
						
						intSheetCnt = (int)Math.ceil( (double)intLoopCnt2 / (double)intMaxClassSr );
						workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex -  (intSheetSr - intSheetCnt)  );

						int intSheetNo = intSheetIndex -  (intSheetSr - intSheetCnt);
						int intBIndex = (int)Math.ceil((double)(intSheetNo + 1) / (double)intMaxSheetSr );
						workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
										

						
						if( intCol == intDataStartCol ){
							
							//データセット
							//自校データ・大学名ほか表示
							//自校学校名
//							workCell = cm.setCell( workSheet, workRow, workCell, 5, 4 );
//							workCell.setCellValue(HomeDataBean.getStrGakkomei());
							//自校データ
//							ListIterator itrHome = HomeDataBean.getB44HyoukaNinzuList().listIterator();
//							int intLoopIndex = 0;
//							while( itrHome.hasNext() ){
//								B44HyoukaNinzuListBean HomeHyoukaNinzuListBean = (B44HyoukaNinzuListBean) itrHome.next();
//								boolean ret = setData( workSheet, HomeHyoukaNinzuListBean, intSetRow + intLoopIndex , 4 );
//								if( ret == false ){
//									return errfdata;
//								}
//								intLoopIndex++;
//							}
							
							if( bolDispHyoukaFlg == true ){
								//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
															intSetRow, intSetRow, 2, 2, false, true, false, false);
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( strHyouka );
				
							}
							
							if( bolDispNtiFlg == true ){
								//罫線出力(セルの上部（日程の列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index, 
															intSetRow, intSetRow, 1, 2, false, true, false, false);
								//日程表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
								workCell.setCellValue( b44ListBean.getStrNtiMei() );
				
							}

							if( bolDispDaigakuFlg == true ){
								//罫線出力(セルの上部に太線を引く）
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
																	intSetRow, intSetRow, 0, 33, false, true, false, false );
								//大学名表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 0 );
								workCell.setCellValue(b44ListBean.getStrDaigakuMei());
							}
							
						}
					

						bolHomeDataDispFlg = false;

						boolean ret = setData( workSheet, b44HyoukaNinzuListBean, intSetRow + intHyoukaNinzuListIndex, intCol);
						if( ret != true){
							return errfdata;
						}
						else{
							intHyoukaNinzuListIndex++;
							intLoopCnt++;
						}
						
						//
						
					}	//WhileEnd( itr3 )
					
					
					//クラス名
					if( bolClassDispFlg == true ){
						String strGakko = b44GakkoListBean.getStrGakkomei();
						workCell = cm.setCell( workSheet, workRow, workCell, 3, intCol );
						workCell.setCellValue(strGakko);
					}
					intLoopCnt2++;
				}	//WhileEnd( itr2 )

				intSetRow += intNendoCount;
				bolClassDispFlg = false;

			}	//WhileEnd( itr )
			
			for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//				int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
				int intBIndex = (intSindex + 1) / intMaxSheetSr;
				if((intSindex + 1)%intMaxSheetSr!=0){
					intBIndex++;
				}
				workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
				workSheet = (HSSFSheet)WorkSheetList.get(intSindex);
								
				//罫線出力(セルの上部に太線を引く）
				cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index, 
													intSetRow, intSetRow, 0, 33, false, true, false, false );
			
				if( workSheet != null ){
					int intRemoveStartRow = intChangeSheetRow;
					if( intMaxRow + intDataStartRow > intRemoveStartRow ){
						for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
							//行削除処理
							workSheet.removeRow(workSheet.getRow(index));
						}
						//罫線出力（セルの上部に太線を出力）
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index, 
										intRemoveStartRow, intRemoveStartRow, 0, 32, false, true, false, false);
					}
				}
			}

			//ファイル保存
			for( int intBookSr = 0; intBookSr < WorkBookList.size(); intBookSr++ ){
				workbook = (HSSFWorkbook)WorkBookList.get(intBookSr);
				
				boolean bolRet = false;
				// Excelファイル保存
				if( (intBookCngCount != 1)||((intBookCngCount == 1)&&(WorkBookList.size()>1)) ){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, intMaxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}
			
			//データ無し
			if ( WorkBookList.size() == 0 ){
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex = 1;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, b44Item.getIntSecuFlg(), 31, 33 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
				workCell.setCellValue(secuFlg);
				
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( b44Item.getStrMshDate() );
								
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "対象模試：" + b44Item.getStrMshmei() + moshi);

				//表示対象
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "表示対象：" );
								
				boolean bolRet = false;
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}
			
		}
		catch( Exception e ){
			log.Err("99B44_2","データセットエラー",e.toString());
			e.printStackTrace();
			return errfdata;
		}

		log.Ep("B44_02","B44_02帳票作成終了","");
		return noerror;
	}

	/**
	 * B44Item加算・編集
	 * @param b44Item
	 * @return
	 */
	private B44Item editB44( B44Item b44Item ) throws Exception{
		try{
			B44Item b44ItemEdt = new B44Item();
			String daigakuCd = "";
			String gakubuCd = "";
			String gakkaCd = "";
			String genekiKbn = "";
			boolean firstFlg = true;
			boolean centerFlg = false;
			boolean secondFlg = false;
			boolean totalFlg = false;

			ArrayList b44GakkoListAdd = new ArrayList();
			ArrayList b44GakkoListAddCnt = new ArrayList();
			ArrayList b44GakkoListAddSnd = new ArrayList();
			ArrayList b44GakkoListAddTtl = new ArrayList();
			ArrayList b44NinzuListAdd = new ArrayList();

//			b44ItemEdt.setStrGakkomei( b44Item.getStrGakkomei() );
			b44ItemEdt.setStrMshmei( b44Item.getStrMshmei() );
			b44ItemEdt.setStrMshDate( b44Item.getStrMshDate() );
			b44ItemEdt.setStrMshCd( b44Item.getStrMshCd() );
			b44ItemEdt.setIntSecuFlg( b44Item.getIntSecuFlg() );
			b44ItemEdt.setIntDaiTotalFlg( b44Item.getIntDaiTotalFlg() );

			B44ListBean b44ListBeanAddCnt = new B44ListBean();
			B44ListBean b44ListBeanAddSnd = new B44ListBean();
			B44ListBean b44ListBeanAddTtl = new B44ListBean();
			/** 大学・学部・学科・日程・評価 **/
			ArrayList b44List = b44Item.getB44List();
			ArrayList b44ListEdt = new ArrayList();
			ListIterator itr = b44List.listIterator();
			while( itr.hasNext() ){
				B44ListBean b44ListBean = (B44ListBean) itr.next();
				B44ListBean b44ListBeanEdt = new B44ListBean();

				// 大学コードが変わる→小計の保存
//////////////////
//				if( !cm.toString(daigakuCd).equals( cm.toString(b44ListBean.getStrDaigakuCd()) ) ){
//////////////////
				if( (!cm.toString(daigakuCd).equals( cm.toString(b44ListBean.getStrDaigakuCd())))
					||( !cm.toString(genekiKbn).equals(cm.toString(b44ListBean.getStrGenKouKbn()))) ){
					genekiKbn = b44ListBean.getStrGenKouKbn();

					daigakuCd = b44ListBean.getStrDaigakuCd();

					if(centerFlg == true){
						b44ListBeanAddCnt.setB44GakkoList( b44GakkoListAddCnt );
						b44ListEdt.add( b44ListBeanAddCnt );
						b44GakkoListAddCnt = new ArrayList();
						centerFlg = false;
					}
					if(secondFlg == true){
						b44ListBeanAddSnd.setB44GakkoList( b44GakkoListAddSnd );
						b44ListEdt.add( b44ListBeanAddSnd );
						b44GakkoListAddSnd = new ArrayList();
						secondFlg = false;
					}
					if(totalFlg == true){
						b44ListBeanAddTtl.setB44GakkoList( b44GakkoListAddTtl );
						b44ListEdt.add( b44ListBeanAddTtl );
						b44GakkoListAddTtl = new ArrayList();
						totalFlg = false;
					}
					b44ListBeanAddCnt = new B44ListBean();
					b44ListBeanAddSnd = new B44ListBean();
					b44ListBeanAddTtl = new B44ListBean();
					b44NinzuListAdd = new ArrayList();
					firstFlg = false;
				}

				// 基本部分の移し変え
				b44ListBeanEdt.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
				b44ListBeanEdt.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
				b44ListBeanEdt.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
				b44ListBeanEdt.setStrGakubuCd( b44ListBean.getStrGakubuCd() );
				b44ListBeanEdt.setStrGakubuMei( b44ListBean.getStrGakubuMei() );
				b44ListBeanEdt.setStrGakkaCd( b44ListBean.getStrGakkaCd() );
				b44ListBeanEdt.setStrGakkaMei( b44ListBean.getStrGakkaMei() );
				b44ListBeanEdt.setStrNtiCd( b44ListBean.getStrNtiCd() );
				b44ListBeanEdt.setStrNtiMei( b44ListBean.getStrNtiMei() );
				b44ListBeanEdt.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );

				// 小計値の保存部
				switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
					case 1:
						b44ListBeanAddCnt.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanAddCnt.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanAddCnt.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanAddCnt.setStrGakubuCd( b44ListBean.getStrGakubuCd() );
						b44ListBeanAddCnt.setStrGakubuMei( b44ListBean.getStrGakubuMei() );
						b44ListBeanAddCnt.setStrGakkaCd( b44ListBean.getStrGakkaCd() );
						b44ListBeanAddCnt.setStrGakkaMei( b44ListBean.getStrGakkaMei() );
						b44ListBeanAddCnt.setStrNtiCd( "NTI-SYOU" );
						b44ListBeanAddCnt.setStrNtiMei( "大学計" );
						b44ListBeanAddCnt.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListAdd = deepCopyCl( b44GakkoListAddCnt );
						centerFlg = true;
						break;
					case 2:
						b44ListBeanAddSnd.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanAddSnd.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanAddSnd.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanAddSnd.setStrGakubuCd( b44ListBean.getStrGakubuCd() );
						b44ListBeanAddSnd.setStrGakubuMei( b44ListBean.getStrGakubuMei() );
						b44ListBeanAddSnd.setStrGakkaCd( b44ListBean.getStrGakkaCd() );
						b44ListBeanAddSnd.setStrGakkaMei( b44ListBean.getStrGakkaMei() );
						b44ListBeanAddSnd.setStrNtiCd( "NTI-SYOU" );
						b44ListBeanAddSnd.setStrNtiMei( "大学計" );
						b44ListBeanAddSnd.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListAdd = deepCopyCl( b44GakkoListAddSnd );
						secondFlg = true;
						break;
					case 3:
						b44ListBeanAddTtl.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanAddTtl.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanAddTtl.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanAddTtl.setStrGakubuCd( b44ListBean.getStrGakubuCd() );
						b44ListBeanAddTtl.setStrGakubuMei( b44ListBean.getStrGakubuMei() );
						b44ListBeanAddTtl.setStrGakkaCd( b44ListBean.getStrGakkaCd() );
						b44ListBeanAddTtl.setStrGakkaMei( b44ListBean.getStrGakkaMei() );
						b44ListBeanAddTtl.setStrNtiCd( "NTI-SYOU" );
						b44ListBeanAddTtl.setStrNtiMei( "大学計" );
						b44ListBeanAddTtl.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListAdd = deepCopyCl( b44GakkoListAddTtl );
						totalFlg = true;
						break;
				}

				// 
				ArrayList b44GakkoList = b44ListBean.getB44GakkoList();
				ArrayList b44GakkoListEdt = new ArrayList();
				ListIterator itrClass = b44GakkoList.listIterator();
				int b = 0;
				boolean firstFlg3 = true;
				if(b44GakkoListAdd.size() == 0){
					firstFlg3 = false;
				}
				/** 学校・学年・クラス **/
				while( itrClass.hasNext() ){
					B44GakkoListBean b44GakkoListBean = (B44GakkoListBean)itrClass.next();
					B44GakkoListBean b44GakkoListBeanEdt = new B44GakkoListBean();
					B44GakkoListBean b44GakkoListBeanAdd = null;

					// 移し変え
					b44GakkoListBeanEdt.setStrGakkomei( b44GakkoListBean.getStrGakkomei() );

					// 小計用データ呼び出し
					if(firstFlg3 == true){
						b44GakkoListBeanAdd = (B44GakkoListBean) b44GakkoListAdd.get(b);
						b44NinzuListAdd = deepCopyNz( b44GakkoListBeanAdd.getB44HyoukaNinzuList() );
					}else{
						b44GakkoListBeanAdd = new B44GakkoListBean();

						b44GakkoListBeanAdd.setStrGakkomei( b44GakkoListBean.getStrGakkomei() );
					}

					/** 年度・人数 **/
					ArrayList b44NinzuList = b44GakkoListBean.getB44HyoukaNinzuList();
					ArrayList b44NinzuListEdt = new ArrayList();
					ListIterator itrNinzu = b44NinzuList.listIterator();
					int a = 0;
					boolean firstFlg2 = true;
					if(b44NinzuListAdd.size() == 0){
						firstFlg2 = false;
					}
					while( itrNinzu.hasNext() ){
						B44HyoukaNinzuListBean b44NinzuBean = (B44HyoukaNinzuListBean)itrNinzu.next();
						B44HyoukaNinzuListBean b44NinzuBeanEdt = new B44HyoukaNinzuListBean();
						B44HyoukaNinzuListBean b44NinzuBeanAdd = null;

						// 移し変え
						b44NinzuBeanEdt.setStrNendo( b44NinzuBean.getStrNendo() );
						b44NinzuBeanEdt.setIntSouShibo( b44NinzuBean.getIntSouShibo() );
						b44NinzuBeanEdt.setIntDai1Shibo( b44NinzuBean.getIntDai1Shibo() );
						b44NinzuBeanEdt.setIntHyoukaA( b44NinzuBean.getIntHyoukaA() );
						b44NinzuBeanEdt.setIntHyoukaB( b44NinzuBean.getIntHyoukaB() );
						b44NinzuBeanEdt.setIntHyoukaC( b44NinzuBean.getIntHyoukaC() );

						b44NinzuListEdt.add( b44NinzuBeanEdt );

						// 加算処理
						if(firstFlg2 == true){
							b44NinzuBeanAdd = (B44HyoukaNinzuListBean) b44NinzuListAdd.get(a);
						}else{
							b44NinzuBeanAdd = new B44HyoukaNinzuListBean();

							b44NinzuBeanAdd.setStrNendo( b44NinzuBean.getStrNendo() );
//							b44NinzuBeanAdd.setIntSouShibo( 0 );
//							b44NinzuBeanAdd.setIntDai1Shibo( 0 );
//							b44NinzuBeanAdd.setIntHyoukaA( 0 );
//							b44NinzuBeanAdd.setIntHyoukaB( 0 );
//							b44NinzuBeanAdd.setIntHyoukaC( 0 );
						}

						if(b44NinzuBean.getIntSouShibo()!=-999){
//							b44NinzuBeanAdd.setIntSouShibo( b44NinzuBeanAdd.getIntSouShibo() + b44NinzuBean.getIntSouShibo() );
							if(b44NinzuBeanAdd.getIntSouShibo()!=-999){
								b44NinzuBeanAdd.setIntSouShibo(b44NinzuBeanAdd.getIntSouShibo() + b44NinzuBean.getIntSouShibo());
							}else{
								b44NinzuBeanAdd.setIntSouShibo(b44NinzuBean.getIntSouShibo());
							}
						}
						if(b44NinzuBean.getIntDai1Shibo()!=-999){
//							b44NinzuBeanAdd.setIntDai1Shibo( b44NinzuBeanAdd.getIntDai1Shibo() + b44NinzuBean.getIntDai1Shibo() );
							if(b44NinzuBeanAdd.getIntDai1Shibo()!=-999){
								b44NinzuBeanAdd.setIntDai1Shibo(b44NinzuBeanAdd.getIntDai1Shibo() + b44NinzuBean.getIntDai1Shibo());
							}else{
								b44NinzuBeanAdd.setIntDai1Shibo(b44NinzuBean.getIntDai1Shibo());
							}
						}
						if(b44NinzuBean.getIntHyoukaA()!=-999){
//							b44NinzuBeanAdd.setIntHyoukaA( b44NinzuBeanAdd.getIntHyoukaA() + b44NinzuBean.getIntHyoukaA() );
							if(b44NinzuBeanAdd.getIntHyoukaA()!=-999){
								b44NinzuBeanAdd.setIntHyoukaA(b44NinzuBeanAdd.getIntHyoukaA() + b44NinzuBean.getIntHyoukaA());
							}else{
								b44NinzuBeanAdd.setIntHyoukaA(b44NinzuBean.getIntHyoukaA());
							}
						}
						if(b44NinzuBean.getIntHyoukaB()!=-999){
//							b44NinzuBeanAdd.setIntHyoukaB( b44NinzuBeanAdd.getIntHyoukaB() + b44NinzuBean.getIntHyoukaB() );
							if(b44NinzuBeanAdd.getIntHyoukaB()!=-999){
								b44NinzuBeanAdd.setIntHyoukaB(b44NinzuBeanAdd.getIntHyoukaB() + b44NinzuBean.getIntHyoukaB());
							}else{
								b44NinzuBeanAdd.setIntHyoukaB(b44NinzuBean.getIntHyoukaB());
							}
						}
						if(b44NinzuBean.getIntHyoukaC()!=-999){
//							b44NinzuBeanAdd.setIntHyoukaC( b44NinzuBeanAdd.getIntHyoukaC() + b44NinzuBean.getIntHyoukaC() );
							if(b44NinzuBeanAdd.getIntHyoukaC()!=-999){
								b44NinzuBeanAdd.setIntHyoukaC(b44NinzuBeanAdd.getIntHyoukaC() + b44NinzuBean.getIntHyoukaC());
							}else{
								b44NinzuBeanAdd.setIntHyoukaC(b44NinzuBean.getIntHyoukaC());
							}
						}

						if(firstFlg2 == true){
							b44NinzuListAdd.set(a,b44NinzuBeanAdd);
						}else{
							b44NinzuListAdd.add(b44NinzuBeanAdd);
						}
						a++;
					}

					// 格納
					b44GakkoListBeanEdt.setB44HyoukaNinzuList( b44NinzuListEdt );
					b44GakkoListEdt.add( b44GakkoListBeanEdt );

					// 計算結果格納
					b44GakkoListBeanAdd.setB44HyoukaNinzuList( deepCopyNz(b44NinzuListAdd) );
					b44NinzuListAdd = new ArrayList();
					if(firstFlg3 == true){
						b44GakkoListAdd.set(b,b44GakkoListBeanAdd);
					}else{
						b44GakkoListAdd.add(b44GakkoListBeanAdd);
					}
					b++;
				}
				// 格納
				b44ListBeanEdt.setB44GakkoList( b44GakkoListEdt );
				b44ListEdt.add( b44ListBeanEdt );
				switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
					case 1:
						b44GakkoListAddCnt = deepCopyCl( b44GakkoListAdd );
						break;
					case 2:
						b44GakkoListAddSnd = deepCopyCl( b44GakkoListAdd );
						break;
					case 3:
						b44GakkoListAddTtl = deepCopyCl( b44GakkoListAdd );
						break;
				}
				b44GakkoListAdd = new ArrayList();
			}

			// 大学コードが変わる→小計の保存
			if(centerFlg == true){
				b44ListBeanAddCnt.setB44GakkoList( b44GakkoListAddCnt );
				b44ListEdt.add( b44ListBeanAddCnt );
				b44GakkoListAddCnt = new ArrayList();
				centerFlg = false;
			}
			if(secondFlg == true){
				b44ListBeanAddSnd.setB44GakkoList( b44GakkoListAddSnd );
				b44ListEdt.add( b44ListBeanAddSnd );
				b44GakkoListAddSnd = new ArrayList();
				secondFlg = false;
			}
			if(totalFlg == true){
				b44ListBeanAddTtl.setB44GakkoList( b44GakkoListAddTtl );
				b44ListEdt.add( b44ListBeanAddTtl );
				b44GakkoListAddTtl = new ArrayList();
				totalFlg = false;
			}
			b44ListBeanAddCnt = new B44ListBean();
			b44ListBeanAddSnd = new B44ListBean();
			b44ListBeanAddTtl = new B44ListBean();
			b44NinzuListAdd = new ArrayList();
			firstFlg = false;

			// 格納
			b44ItemEdt.setB44List( b44ListEdt );

			return b44ItemEdt;
		}catch(Exception e){
			e.toString();
			throw e;
		}
	}

	/**
	 * 
	 * @param b44NinzuList
	 * @return
	 */
	private ArrayList deepCopyNz(ArrayList b44NinzuList){
		ListIterator itrNinzu = b44NinzuList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrNinzu.hasNext()){
			B44HyoukaNinzuListBean b44NinzuBean = (B44HyoukaNinzuListBean)itrNinzu.next();
			B44HyoukaNinzuListBean b44NinzuBeanCopy = new B44HyoukaNinzuListBean();

			b44NinzuBeanCopy.setStrNendo( b44NinzuBean.getStrNendo() );
			b44NinzuBeanCopy.setIntSouShibo( b44NinzuBean.getIntSouShibo() );
			b44NinzuBeanCopy.setIntDai1Shibo( b44NinzuBean.getIntDai1Shibo() );
			b44NinzuBeanCopy.setIntHyoukaA( b44NinzuBean.getIntHyoukaA() );
			b44NinzuBeanCopy.setIntHyoukaB( b44NinzuBean.getIntHyoukaB() );
			b44NinzuBeanCopy.setIntHyoukaC( b44NinzuBean.getIntHyoukaC() );

			copyList.add( b44NinzuBeanCopy );
		}
		
		return copyList;
	}

	/**
	 * 
	 * @param b44GakkoList
	 * @return
	 */
	private ArrayList deepCopyCl(ArrayList b44GakkoList){
		ListIterator itrClass = b44GakkoList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrClass.hasNext()){
			B44GakkoListBean b44ClassBean = (B44GakkoListBean)itrClass.next();
			B44GakkoListBean b44ClassBeanCopy = new B44GakkoListBean();

			b44ClassBeanCopy.setStrGakkomei( b44ClassBean.getStrGakkomei() );
			b44ClassBeanCopy.setB44HyoukaNinzuList( deepCopyNz(b44ClassBean.getB44HyoukaNinzuList()) );

			copyList.add( b44ClassBeanCopy );
		}
		
		return copyList;
	}
	
	/*
	 * 	Excel編集
	 * 		HSSFWorkbook 	workbook: Excelワークブック
	 * 		ArrayList 		b44HyoukaNinzuList:評価別人数リスト
	 * 		int				intRow:開始行
	 * 		int				intCol:開始列
	 * 		戻り値: なし
	 */
	private boolean setData(HSSFSheet workSheet, B44HyoukaNinzuListBean b44HyoukaNinzuListBean, int intRow, int intCol ){

		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		int		maxSheetIndex	= 0;
		int		row = intRow;
		
		
		// 基本ファイルを読込む
		try{
			
			//模試年度
			workCell = cm.setCell( workSheet, workRow, workCell, row, 3);
			workCell.setCellValue( b44HyoukaNinzuListBean.getStrNendo() + "年度" );
			
			//全国総志望者数
			if( b44HyoukaNinzuListBean.getIntSouShibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol);
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntSouShibo() );
			}
			
			//志望者数（第一志望）
			if( b44HyoukaNinzuListBean.getIntDai1Shibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 1);
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntDai1Shibo() );
			}
			
			//評価別人数
			if( b44HyoukaNinzuListBean.getIntHyoukaA() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 2 );
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntHyoukaA() );
			}

			if( b44HyoukaNinzuListBean.getIntHyoukaB() != -999 ){			
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntHyoukaB() );
			}
			
			if( b44HyoukaNinzuListBean.getIntHyoukaC() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntHyoukaC() );
			}

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
}
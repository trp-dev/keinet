package jp.co.fj.keinavi.beans.sheet.school.data;

/**
 *
 * センター到達指標・到達レベルリスデータ
 *
 * 2006.08.31	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class S42_15ReachLevelListData {

    // スコア帯コード
    private String devZoneCd;
    // 上限値
    private double highLimit;
    // 下限値
    private double lowLimit;
    // センター到達指標（順位）
    private double rank;
    // センター到達指標（過去5年平均点）
    private double avgPnt;
    // 到達レベル
    private String reachLevel;
    // 到達レベル（表示文字列）
    private String dispStr;
    // センター到達エリア
    private String centerReachArea;
    // センター到達エリア（表示文字列）
    private String dispStr_CenterReachArea;
    // センター予想得点率
    private String centerExScoreRate;
    // 親データクラス
    private S42_15SubjectListData subjectListData;

    // コンストラクタ
    protected S42_15ReachLevelListData() {
    }

    /**
     * @return avgPnt を戻します。
     */
    public double getAvgPnt() {
        return avgPnt;
    }
    /**
     * @param pAvgPnt 設定する avgPnt。
     */
    public void setAvgPnt(double pAvgPnt) {
        avgPnt = pAvgPnt;
    }
    /**
     * @return devZoneCd を戻します。
     */
    public String getDevZoneCd() {
        return devZoneCd;
    }
    /**
     * @param pDevZoneCd 設定する devZoneCd。
     */
    public void setDevZoneCd(String pDevZoneCd) {
        devZoneCd = pDevZoneCd;
    }
    /**
     * @return disp_Str を戻します。
     */
    public String getDispStr() {
        return dispStr;
    }
    /**
     * @param pDisp_Str 設定する disp_Str。
     */
    public void setDispStr(String pDispStr) {
        dispStr = pDispStr;
    }
    /**
     * @return highLimit を戻します。
     */
    public double getHighLimit() {
        return highLimit;
    }
    /**
     * @param pHighLimit 設定する highLimit。
     */
    public void setHighLimit(double pHighLimit) {
        highLimit = pHighLimit;
    }
    /**
     * @return lowLimit を戻します。
     */
    public double getLowLimit() {
        return lowLimit;
    }
    /**
     * @param pLowLimit 設定する lowLimit。
     */
    public void setLowLimit(double pLowLimit) {
        lowLimit = pLowLimit;
    }
    /**
     * @return rank を戻します。
     */
    public double getRank() {
        return rank;
    }
    /**
     * @param pRank 設定する rank。
     */
    public void setRank(double pRank) {
        rank = pRank;
    }
    /**
     * @return reachLevel を戻します。
     */
    public String getReachLevel() {
        return reachLevel;
    }
    /**
     * @param pReachLevel 設定する reachLevel。
     */
    public void setReachLevel(String pReachLevel) {
        reachLevel = pReachLevel;
    }

    /**
     * @return centerReachArea
     */
    public String getCenterReachArea() {
        return centerReachArea;
    }

    /**
     * @param centerReachArea セットする centerReachArea
     */
    public void setCenterReachArea(String centerReachArea) {
        this.centerReachArea = centerReachArea;
    }

    /**
     * @return dispStr_CenterReachArea
     */
    public String getDispStr_CenterReachArea() {
        return dispStr_CenterReachArea;
    }

    /**
     * @param dispStr_CenterReachArea セットする dispStr_CenterReachArea
     */
    public void setDispStr_CenterReachArea(String dispStr_CenterReachArea) {
        this.dispStr_CenterReachArea = dispStr_CenterReachArea;
    }

    /**
     * @return centerExScoreRate
     */
    public String getCenterExScoreRate() {
        return centerExScoreRate;
    }

    /**
     * @param centerExScoreRate セットする centerExScoreRate
     */
    public void setCenterExScoreRate(String centerExScoreRate) {
        this.centerExScoreRate = centerExScoreRate;
    }

    /**
     * @return subjectListData を戻します。
     */
    public S42_15SubjectListData getSubjectListData() {
        return subjectListData;
    }

    /**
     * @param pSubjectListData 設定する subjectListData。
     */
    public void setSubjectListData(S42_15SubjectListData pSubjectListData) {
        subjectListData = pSubjectListData;
    }

}

package jp.co.fj.keinavi.data.maintenance;

/**
 *
 * 受験届データ
 * 
 * 2006.10.10	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class AnswerSheetData extends KNStudentData {

	// 解答用紙番号
	private String answerSheetNo;
	// 模試年度
	private String examYear;
	// 模試コード
	private String examCd;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pYearDiff 現年度との差分
	 */
	public AnswerSheetData(int pYearDiff) {
		super(pYearDiff);
	}

	/**
	 * @return answerSheetNo を戻します。
	 */
	public String getAnswerSheetNo() {
		return answerSheetNo;
	}

	/**
	 * @param pAnswerSheetNo 設定する answerSheetNo。
	 */
	public void setAnswerSheetNo(String pAnswerSheetNo) {
		answerSheetNo = pAnswerSheetNo;
	}

	/**
	 * @return examCd を戻します。
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * @param pExamCd 設定する examCd。
	 */
	public void setExamCd(String pExamCd) {
		examCd = pExamCd;
	}

	/**
	 * @return examYear を戻します。
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @param pExamYear 設定する examYear。
	 */
	public void setExamYear(String pExamYear) {
		examYear = pExamYear;
	}

}

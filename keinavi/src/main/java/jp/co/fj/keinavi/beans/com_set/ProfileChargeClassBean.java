package jp.co.fj.keinavi.beans.com_set;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.data.com_set.ChargeClassData;
import jp.co.fj.keinavi.data.com_set.cm.ClassData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.ProfileUtil;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * プロファイルの担当クラス設定値を取得するBean
 * ※複合クラスの名前の解決のためだけにある・・・
 * 
 * 2005.10.12	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class ProfileChargeClassBean extends DefaultBean {

	// プロファイル
	private final Profile profile;
	
	// 年度
	private final String year;
	// 設定値リスト
	private final List classList = new ArrayList();
	
	/**
	 * コンストラクタ
	 */
	public ProfileChargeClassBean(final Profile profile) {
		
		this.profile = profile;
		this.year = ProfileUtil.getChargeClassYear(profile);
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		// 担当クラスリスト
		final List container = (List) profile.getItemMap(
				IProfileCategory.CM).get(IProfileItem.CLASS);
		
		for (final Iterator ite = container.iterator(); ite.hasNext(); ) {
			final ChargeClassData charge = (ChargeClassData) ite.next();
			
			// 通常のクラス
			if (charge.getClassName().length() == 2) {
				final ClassData data = new ClassData();
				data.setGrade(charge.getGrade());
				data.setClassName(charge.getClassName() + "クラス");
				data.setKey(charge.getKey());
				classList.add(data);

			// 複合クラス
			} else {
				addCompositeClass(charge);
			}
		}
		
		// ソート
		Collections.sort(classList);
	}
	
	/**
	 * @param charge
	 */
	private void addCompositeClass(final ChargeClassData charge) throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					"SELECT g.classgname FROM classgroup g "
					 + "WHERE g.classgcd = ?" );
			ps.setString(1, charge.getClassName());
			rs = ps.executeQuery();
			if (rs.next()) {
				final ClassData data = new ClassData();
				data.setGrade(charge.getGrade());
				data.setClassName(rs.getString(1) + "クラス");
				data.setKey(charge.getKey());
				classList.add(data);
			}
			
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	/**
	 * @return classList を戻します。
	 */
	public List getClassList() {
		return classList;
	}

	/**
	 * @return year を戻します。
	 */
	public String getYear() {
		return year;
	}

}

/**
 * 校内成績分析−他校比較　成績概況（他校比較）
 * 出力する帳票の判断
 * 作成日: 2004/08/11
 * @author	T.Sakai
 */
 
package jp.co.fj.keinavi.excel.school;

import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.util.log.*;

public class S41 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean s41( S41Item s41Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		try{
			int ret = 0;
			//S41Itemから各帳票を出力
			if (( s41Item.getIntHyouFlg()==0 ) && ( s41Item.getIntGraphFlg()==0 )){
				throw new Exception("S41 ERROR : フラグ異常 ");
			}
			if ( s41Item.getIntHyouFlg()==1 ) {
				log.Ep("S41_01","S41_01帳票作成開始","");
				S41_01 exceledit = new S41_01();
				ret = exceledit.s41_01EditExcel( s41Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S41_01","帳票作成エラー","");
					return false;
				}
				log.Ep("S41_01","S41_01帳票作成終了","");
				sheetLog.add("S41_01");
			}
			if ( s41Item.getIntGraphFlg()==1 ) {
				log.Ep("S41_02","S41_02帳票作成開始","");
				S41_02 exceledit = new S41_02();
				ret = exceledit.s41_02EditExcel( s41Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S41_02","帳票作成エラー","");
					return false;
				}
				log.Ep("S41_02","S41_02帳票作成終了","");
				sheetLog.add("S41_02");
			}
			
		} catch(Exception e) {
			log.Err("99S41","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
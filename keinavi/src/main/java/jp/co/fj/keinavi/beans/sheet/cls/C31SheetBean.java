/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.cls;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.excel.cls.C31;
import jp.co.fj.keinavi.excel.data.cls.C31ClassListBean;
import jp.co.fj.keinavi.excel.data.cls.C31Item;
import jp.co.fj.keinavi.excel.data.cls.C31ListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * @author kawai
 *
 */
public class C31SheetBean extends AbstractSheetBean {

	// データクラス
	private final C31Item data = new C31Item();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		String[] code = super.getSubjectCDArray(); // 型・科目設定値
		List graph = super.getSubjectGraphList(); // 型・科目グラフ表示対象
		boolean isNewExam = KNUtil.isNewExam(exam); // 新テストかどうか
		
		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntHyouFlg(super.getIntFlag(IProfileItem.CHART)); // 表フラグ
		data.setIntGraphFlg(super.getIntFlag(IProfileItem.GRAPH)); // グラフフラグ
		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ

		// ワークテーブルのセットアップ
		super.insertIntoCountCompClass();

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		try {
			List c1 = new LinkedList(); // 型の入れ物
			List c2 = new LinkedList(); // 科目の入れ物

			// データリストの取得
			Query query1 = QueryLoader.getInstance().load("s22_1");
			query1.setStringArray(1, code); // 型・科目コード

			ps1 = conn.prepareStatement(query1.toString());
			ps1.setString(1, exam.getExamYear()); // 対象年度
			ps1.setString(2, exam.getExamCD()); // 対象模試

			// クラスデータリスト（自校）
			ps2 = conn.prepareStatement(QueryLoader.getInstance().load("s31_1").toString());
			ps2.setString(1, exam.getExamYear()); // 模試年度
			ps2.setString(2, exam.getExamCD()); // 模試コード
			ps2.setString(3, profile.getBundleCD()); // 一括コード

			// クラスデータリスト（クラス）
			ps3 = conn.prepareStatement(QueryLoader.getInstance().load("s31_2").toString());
			ps3.setString(1, exam.getExamYear()); // 模試年度
			ps3.setString(2, exam.getExamCD()); // 模試コード
			ps3.setString(3, profile.getBundleCD()); // 一括コード
			ps3.setString(5, exam.getExamYear()); // 模試年度
			ps3.setString(6, exam.getExamCD()); // 模試コード
			ps3.setString(7, exam.getExamYear()); // 模試年度
			ps3.setString(8, exam.getExamCD()); // 模試コード
			ps3.setString(9, profile.getBundleCD()); // 一括コード
			ps3.setString(11, exam.getExamYear()); // 模試年度
			ps3.setString(12, exam.getExamCD()); // 模試コード

			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				C31ListBean bean = new C31ListBean();
				bean.setStrKmkCd(rs1.getString(1));
				bean.setStrKmkmei(rs1.getString(2));
				bean.setStrHaitenKmk(rs1.getString(3));
				bean.setIntDispKmkFlg(graph.contains(rs1.getString(1)) ? 1 : 0);

				if (rs1.getInt(1) >= 7000) c1.add(bean); // 型
				else c2.add(bean); // 科目

				// 型・科目コード
				ps2.setString(4, bean.getStrKmkCd()); 			
				ps3.setString(4, bean.getStrKmkCd());			
				ps3.setString(10, bean.getStrKmkCd());			
				ps3.setString(13, bean.getStrKmkCd());
				
				// 自校
				{
					C31ClassListBean c = new C31ClassListBean();
					c.setStrGakkomei(profile.getBundleName());
					c.setIntDispClassFlg(1);

					rs2 = ps2.executeQuery();

					if (rs2.next()) {
						c.setIntNinzu(rs2.getInt(1));
						
						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							c.setFloHeikin(-999);
							c.setFloHensa(rs2.getFloat(2));
						} else {
							c.setFloHeikin(rs2.getFloat(2));
							c.setFloHensa(rs2.getFloat(3));
						}

					} else {
						c.setIntNinzu(-999);
						c.setFloHeikin(-999);
						c.setFloHensa(-999);
					}

					rs2.close();

					bean.getC31ClassList().add(c);
				}

				// クラス
				{
					rs3 = ps3.executeQuery();

					while (rs3.next()) {
						C31ClassListBean c = new C31ClassListBean();
						c.setStrGrade(rs3.getString(1));
						c.setStrClass(rs3.getString(2));
						c.setIntNinzu(rs3.getInt(4));
						c.setIntDispClassFlg(rs3.getInt(7));

						// 新テストなら平均点→無効値／平均偏差値→平均点
						if (isNewExam) {
							c.setFloHeikin(-999);
							c.setFloHensa(rs3.getFloat(5));
						} else {
							c.setFloHeikin(rs3.getFloat(5));
							c.setFloHensa(rs3.getFloat(6));
						}

						bean.getC31ClassList().add(c);
					}

					rs3.close();
				}
			}

			// 型・科目の順番に詰める
			data.getC31List().addAll(c1);
			data.getC31List().addAll(c2);
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(rs3);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(ps3);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("C31_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.C_COMP_SCORE;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new C31()
			.c31(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
	}

}

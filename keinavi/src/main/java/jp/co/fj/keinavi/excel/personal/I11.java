/**
 * 個人成績分析−面談シート
 * @author Ito.Y
 */
 
package jp.co.fj.keinavi.excel.personal;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.personal.I11Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

public class I11 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean i11( I11Item i11Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog) {
		
		KNLog log = KNLog.getInstance(null,null,null);

		try{
//			//I11Itemから各帳票を出力
//			I11_01 exceledit1 = new I11_01();
//			exceledit1.i11_01EditExcel( i11Item, outfilelist, saveFlg, UserID );
//			I11_02 exceledit2 = new I11_02();
//			exceledit2.i11_02EditExcel( i11Item, outfilelist, saveFlg, UserID );
//			I11_03 exceledit3 = new I11_03();
//			exceledit3.i11_03EditExcel( i11Item, outfilelist, saveFlg, UserID );
//			I11_04 exceledit4 = new I11_04();
//			exceledit4.i11_04EditExcel( i11Item, outfilelist, saveFlg, UserID );
//			I11_05 exceledit5 = new I11_05();
//			exceledit5.i11_05EditExcel( i11Item, outfilelist, saveFlg, UserID );
//			I11_06 exceledit6 = new I11_06();
//			exceledit6.i11_06EditExcel( i11Item, outfilelist, saveFlg, UserID );

			int ret = 0;
			if ( i11Item.getIntMenSheetFlg()==2 ){
				if( i11Item.getIntBaseMshKyokaSr() > 3 ){
					I11_01 exceledit1 = new I11_01();
					ret = exceledit1.i11_01EditExcel( i11Item, outfilelist, saveFlg, UserID );
					if( ret != 0 ){
						log.Err("0"+ret+"I11_01","帳票作成エラー","");
						return false;
					}
					sheetLog.add("I11_01");
				}
				else{
					I11_02 exceledit2 = new I11_02();
					ret = exceledit2.i11_02EditExcel( i11Item, outfilelist, saveFlg, UserID );
					if( ret != 0 ){
						log.Err("0"+ret+"I11_02","帳票作成エラー","");
						return false;
					}
					sheetLog.add("I11_02");
				}
//				if ( i11Item.getStrBaseMshCd().equals("") ){
//					I11_01 exceledit1 = new I11_01();
//					ret = exceledit1.i11_01EditExcel( i11Item, outfilelist, saveFlg, UserID );
//					if( ret != 0 ){
//						log.Err("0"+ret+"I11_01","帳票作成エラー","");
//						return false;
//					}
//				}
//				if ( i11Item.getStrBaseMshCd().equals("") ){
//					I11_02 exceledit2 = new I11_02();
//					ret = exceledit2.i11_02EditExcel( i11Item, outfilelist, saveFlg, UserID );
//					if( ret != 0 ){
//						log.Err("0"+ret+"I11_02","帳票作成エラー","");
//						return false;
//					}
//				}
			}
			if ( i11Item.getIntMenSheetFlg()==3 ){
				if( i11Item.getIntBaseMshKyokaSr() > 3 ){
					I11_03 exceledit3 = new I11_03();
					ret = exceledit3.i11_03EditExcel( i11Item, outfilelist, saveFlg, UserID );
					if( ret != 0 ){
						log.Err("0"+ret+"I11_03","帳票作成エラー","");
						return false;
					}
					sheetLog.add("I11_03");
				}
				else{
					I11_04 exceledit4 = new I11_04();
					ret = exceledit4.i11_04EditExcel( i11Item, outfilelist, saveFlg, UserID );
					if( ret != 0 ){
						log.Err("0"+ret+"I11_04","帳票作成エラー","");
						return false;
					}
					sheetLog.add("I11_04");
				}
				
//				if ( i11Item.getStrBaseMshCd().equals("") ){
//					I11_03 exceledit3 = new I11_03();
//					ret = exceledit3.i11_03EditExcel( i11Item, outfilelist, saveFlg, UserID );
//					if( ret != 0 ){
//						log.Err("0"+ret+"I11_03","帳票作成エラー","");
//						return false;
//					}
//				}
//				if ( i11Item.getStrBaseMshCd().equals("") ){
//					I11_04 exceledit4 = new I11_04();
//					ret = exceledit4.i11_04EditExcel( i11Item, outfilelist, saveFlg, UserID );
//					if( ret != 0 ){
//						log.Err("0"+ret+"I11_04","帳票作成エラー","");
//						return false;
//					}
//				}
			}
			if ( i11Item.getIntMenSheetFlg()==4 ){
				if( i11Item.getIntBaseMshKyokaSr() > 3 ){
					I11_05 exceledit5 = new I11_05();
					ret = exceledit5.i11_05EditExcel( i11Item, outfilelist, saveFlg, UserID );
					if( ret != 0 ){
						log.Err("0"+ret+"I11_05","帳票作成エラー","");
						return false;
					}
					sheetLog.add("I11_05");
				}
				else{
					I11_06 exceledit6 = new I11_06();
					exceledit6.i11_06EditExcel( i11Item, outfilelist, saveFlg, UserID );
					if( ret != 0 ){
						log.Err("0"+ret+"I11_06","帳票作成エラー","");
						return false;
					}
					sheetLog.add("I11_06");
				}
//				if ( i11Item.getStrBaseMshCd().equals("") ){
//					I11_05 exceledit5 = new I11_05();
//					ret = exceledit5.i11_05EditExcel( i11Item, outfilelist, saveFlg, UserID );
//					if( ret != 0 ){
//						log.Err("0"+ret+"I11_05","帳票作成エラー","");
//						return false;
//					}
//				}
//				if ( i11Item.getStrBaseMshCd().equals("") ){
//					I11_06 exceledit6 = new I11_06();
//					exceledit6.i11_06EditExcel( i11Item, outfilelist, saveFlg, UserID );
//					if( ret != 0 ){
//						log.Err("0"+ret+"I11_06","帳票作成エラー","");
//						return false;
//					}
//				}
			}
			if ( i11Item.getIntMenSheetFlg()==5 ){
				I11_07 exceledit7 = new I11_07();
				ret = exceledit7.i11_07EditExcel( i11Item, outfilelist, saveFlg, UserID );
				if( ret != 0 ){
					log.Err("0"+ret+"I11_07","帳票作成エラー","");
					return false;
				}
				sheetLog.add("I11_07");
			}


		} catch(Exception e) {
			e.printStackTrace();
			log.Err("99I11","帳票作成エラー",e.toString());
			return false;
		}
		return true;
	}

}
package jp.co.fj.keinavi.forms.admin;

/**
 * 
 * 
 * @author nino
 * 
 */
public class M302Form extends M301Form {

	private String className = null;	// クラス名
	private String comment   = null; 	// コメント
	private String[] classList = null; 	// クラスリスト

	/**
	 * @return
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param string
	 */
	public void setClassName(String string) {
		className = string;
	}

	/**
	 * @return
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param string
	 */
	public void setComment(String string) {
		comment = string;
	}

	/**
	 * @return
	 */
	public String[] getClassList() {
		return classList;
	}

	/**
	 * @param strings
	 */
	public void setClassList(String[] strings) {
		classList = strings;
	}

}

package jp.co.fj.keinavi.beans;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.UploadFileKindData;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultSearchBean;

/**
 *
 * アップロードファイル種類Bean
 * 
 * <2010年度マーク高２模試対応>
 * 2011.01.24	Tomohisa YAMADA
 * 				アップロード機能
 * 
 */
public class UploadFileKindBean extends DefaultSearchBean {
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() {
		throw new InternalError("このメソッドは使用できません。");
	}
	
	/**
	 * ファイル種類をすべて検索する。
	 * @throws SQLException
	 */
	public void searchAll() throws SQLException {
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("SELECT ufk.filetype_id, ufk.filetype_name, ufk.filetype_abbrname FROM uploadfilekind ufk ");
			rs = ps.executeQuery();
			while (rs.next()) {
				recordSet.add(new UploadFileKindData(
						rs.getString("filetype_id"), 
						rs.getString("filetype_name"),
						rs.getString("filetype_abbrname")));			
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

}

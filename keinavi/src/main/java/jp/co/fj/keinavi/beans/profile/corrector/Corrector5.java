/*
 * 作成日: 2005/02/09
 */
package jp.co.fj.keinavi.beans.profile.corrector;

import java.util.Map;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;

/**
 * バージョン４→５
 * 評価区分対応
 * 
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */
public class Corrector5 implements ICorrector {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.profile.corrector.ICorrector#execute(jp.co.fj.keinavi.data.profile.Profile)
	 */
	public void execute(final Profile profile, final LoginSession login) {
		this.setValue(profile.getItemMap(S_PREV_UNIV));
		this.setValue(profile.getItemMap(S_CLASS_UNIV));
		this.setValue(profile.getItemMap(S_OTHER_UNIV));
		this.setValue(profile.getItemMap(C_COMP_UNIV));	
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.profile.corrector.ICorrector#getVersion()
	 */
	public short getVersion() {
		return 5;
	}

	/**
	 * デフォルト値をセットする
	 * 
	 * @param item
	 */
	private void setValue(Map item) {
		if (item == null) return;		
		item.put(UNIV_RATING, new String[]{"1", "2", "3"});
	}
	
}

/*
 * 作成日: 2004/09/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.profile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.data.LoginSession;

/**
 * プロファイルフォルダを表現するクラス
 * 
 * @author kawai
 */
public class ProfileFolder implements Serializable {

	private String folderID; // プロファイルフォルダID
	private String folderName; // プロファイルフォルダ名称
	private Date createDate; // 作成日時
	private Date updateDate; // 更新日時
	private String comment; // コメント
	private String userID; // ユーザID
	private String bundleCD; // 一括コード（集計区分コード）
	private String sectorSortingCD; // 部門分類コード

	private LoginSession loginSession; // ログイン情報

	private final List profileList = new ArrayList(); // プロファイルリスト
	private List ownProfileList; // 自分が所有しているプロファイルリスト

	/**
	 * コンストラクタ
	 */
	public ProfileFolder() {
	}

	/**
	 * コンストラクタ
	 */
	public ProfileFolder(String folderID) {
		this.folderID = folderID;
	}

	/**
	 * 営業（高校代行）のフォルダかどうか
	 * @return
	 */
	public boolean isDeputy() {
		
		return sectorSortingCD != null && bundleCD != null;
	}

	/**
	 * プロファイルリストの要素数を返す
	 * @return
	 */
	public int getProfileListSize() {
		return profileList.size();
	}

	/**
	 * 自分が所有しているプロファイルリストを返す
	 * @return
	 */
	public List getOwnProfileList() {
		if (ownProfileList == null) {
			ownProfileList = new LinkedList();
			
			Iterator ite = profileList.iterator();
			while (ite.hasNext()) {
				Profile profile = (Profile) ite.next();
						
				// 高校
				if (loginSession.isSchool()) {
					if (!profile.isDeputy()) ownProfileList.add(profile);
					
				// 営業または校舎						
				} else if (loginSession.isBusiness()) {
					ownProfileList.add(profile);

				// 営業 → 高校
				} else if (loginSession.isDeputy()) {
					if (profile.isDeputy()) ownProfileList.add(profile);
				}
			}
		}
		
		return ownProfileList;
	}

	/**
	 * @return
	 */
	public String getBundleCD() {
		return bundleCD;
	}

	/**
	 * @return
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @return
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @return
	 */
	public String getFolderID() {
		return folderID;
	}

	/**
	 * @return
	 */
	public String getFolderName() {
		return folderName;
	}

	/**
	 * @return
	 */
	public String getSectorSortingCD() {
		return sectorSortingCD;
	}

	/**
	 * @return
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @return
	 */
	public String getUserID() {
		return userID;
	}

	/**
	 * @param string
	 */
	public void setBundleCD(String string) {
		bundleCD = string;
	}

	/**
	 * @param string
	 */
	public void setComment(String string) {
		comment = string;
	}

	/**
	 * @param date
	 */
	public void setCreateDate(Date date) {
		createDate = date;
	}

	/**
	 * @param string
	 */
	public void setFolderID(String string) {
		folderID = string;
	}

	/**
	 * @param string
	 */
	public void setFolderName(String string) {
		folderName = string;
	}

	/**
	 * @param string
	 */
	public void setSectorSortingCD(String string) {
		sectorSortingCD = string;
	}

	/**
	 * @param date
	 */
	public void setUpdateDate(Date date) {
		updateDate = date;
	}

	/**
	 * @param string
	 */
	public void setUserID(String string) {
		userID = string;
	}

	/**
	 * @return
	 */
	public List getProfileList() {
		return profileList;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return this.getFolderID().equals(((ProfileFolder)obj).getFolderID());
	}

	/**
	 * @param session
	 */
	public void setLoginSession(LoginSession session) {
		loginSession = session;
	}

}

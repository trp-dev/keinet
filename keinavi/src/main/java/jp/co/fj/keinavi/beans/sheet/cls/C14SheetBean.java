/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.cls;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.beans.sheet.NoQuestionException;
import jp.co.fj.keinavi.excel.cls.C14;
import jp.co.fj.keinavi.excel.data.cls.C14Item;
import jp.co.fj.keinavi.excel.data.cls.C14KmkListBean;
import jp.co.fj.keinavi.excel.data.cls.C14ListBean;
import jp.co.fj.keinavi.excel.data.cls.C14PersonalDataListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 2005.02.28 Yoshimoto KAWAI - Totec
 *            担当クラスの年度対応
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * 2005.05.25	Yoshimoto KAWAI - Totec
 * 				新テスト対応
 * 
 * 2009.10.09   Fujito URAKAWA - Totec
 *              「全体成績」追加対応 
 *              
 * <2010年度＃マーク対応>  
 * 2010.10.26   Tomohisa YAMADA - Totec
 *              数学の集計対応          
 * 
 * @author kawai
 *
 */
public class C14SheetBean extends AbstractSheetBean {

	// データクラス
	private final C14Item data = new C14Item();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		String[] code = super.getSubjectCDArray(); // 型・科目設定値
		String current = ProfileUtil.getChargeClassYear(profile);
		final boolean isNewExam = KNUtil.isNewExam(exam); // 新テストかどうか
		
		String tmp_kmkcd = "";	// 科目コードチェック用変数
		int tmp_grade = -1; // 学年チェック用変数
		String tmp_classcd = ""; // クラスコードチェック用変数
		
		// 設問別成績存在チェック
		if (hasAllNoQuestionSubject(code)) {
			throw new NoQuestionException();
		}

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setStrYear(exam.getExamYear()); // 対象年度
		data.setIntSortFlg(super.getIntFlag(IProfileItem.STUDENT_ORDER)); // 生徒の表示順序
		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ

		// ワークテーブルへデータを入れる
		super.insertIntoCountChargeClass();

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;	// 科目別成績（全国・県・校内）
		PreparedStatement ps5 = null;	// 科目別成績（クラス）
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		
		try {
			// 担当クラス
			ps1 = conn.prepareStatement(QueryLoader.getInstance().load("c11_1").toString());

			// 科目別成績データリスト
			{
				Query query = QueryLoader.getInstance().load("c14_1");
				query.setStringArray(1, code); // 科目コード
				query.setStringArray(2, code); // 科目コード

				ps2 = conn.prepareStatement(query.toString());
				ps2.setString(1, login.getPrefCD()); // 県コード
				ps2.setString(2, profile.getBundleCD()); // 一括コード		
				ps2.setString(3, exam.getExamYear()); // 模試年度
				ps2.setString(4, exam.getExamCD()); // 模試コード
				ps2.setString(5, profile.getBundleCD()); // 一括コード
				ps2.setString(8, exam.getExamYear()); // 模試年度
				ps2.setString(9, exam.getExamCD()); // 模試コード
				ps2.setString(10, profile.getBundleCD()); // 一括コード
				ps2.setString(12, exam.getExamYear()); // 模試年度
				ps2.setString(13, exam.getExamCD()); // 模試コード
				ps2.setString(14, profile.getBundleCD()); // 一括コード
				ps2.setString(17, exam.getExamYear()); // 模試年度
				ps2.setString(18, exam.getExamCD()); // 模試コード
				ps2.setString(19, profile.getBundleCD()); // 一括コード
				ps2.setString(21, exam.getExamYear()); // 模試年度
				ps2.setString(22, exam.getExamCD()); // 模試コード
			}

			// 教科別成績データリスト
			{
				Query query = QueryLoader.getInstance().load("c14_2");

				switch (data.getIntSortFlg()) {
					case  1: query.append("ORDER BY 1 DESC, 2, 3"); break; 
					case  2: query.append("ORDER BY 8,1 DESC, 2, 3"); break; 
				}
		
				ps3 = conn.prepareStatement(query.toString());
				ps3.setString(1, login.getUserID()); // 学校コード
				ps3.setString(2, current); // 現在の年度
				ps3.setString(5, exam.getExamYear()); // 模試年度
				ps3.setString(6, exam.getExamCD()); // 模試コード
				ps3.setString(9, login.getUserID()); // 学校コード
				ps3.setString(10, current); // 現在の年度
				ps3.setString(12, exam.getExamYear()); // 模試年度
				ps3.setString(13, exam.getExamCD()); // 模試コード
			}
			
			// 科目別成績データリスト（全体成績）
			{
				Query query = QueryLoader.getInstance().load("c14_3");
				
				ps4 = conn.prepareStatement(query.toString());
				ps4.setString(1, login.getPrefCD());
				ps4.setString(2, profile.getBundleCD());
				ps4.setString(3, exam.getExamYear());
				ps4.setString(4, exam.getExamCD());
				ps4.setString(5, profile.getBundleCD());
				//6:grade
				//7:class
				ps4.setString(8, exam.getExamYear());
				ps4.setString(9, exam.getExamCD());
				ps4.setString(10, profile.getBundleCD());
				//11:classgcd
				ps4.setString(12, exam.getExamYear());
				ps4.setString(13, exam.getExamCD());
				ps4.setString(14, profile.getBundleCD());
				//15:grade
				//16:class
				ps4.setString(17, exam.getExamYear());
				ps4.setString(18, exam.getExamCD());
				ps4.setString(19, profile.getBundleCD());
				//20:classgcd
				ps4.setString(21, exam.getExamYear());
				ps4.setString(22, exam.getExamCD());
				//23:subcd
				ps4.setString(24, exam.getExamYear());
				ps4.setString(25, exam.getExamCD());
				
			}
			
			// 個人成績データリスト
			{
				Query query = QueryLoader.getInstance().load("c14_4");
				
				switch (data.getIntSortFlg()) {
					case  1: query.append("ORDER BY 1 DESC, 2, 3"); break; 
					case  2: query.append("ORDER BY 8,1 DESC, 2, 3"); break; 
				}
				
				ps5 = conn.prepareStatement(query.toString());
				
				ps5.setString(1, login.getUserID()); // 学校コード
				ps5.setString(2, current);
				// 3 学年
				// 4 クラス
				ps5.setString(5, exam.getExamYear());
				ps5.setString(6, exam.getExamCD());
				// 7 科目
				ps5.setString(8, login.getUserID()); // 学校コード
				ps5.setString(9, current);
				// 10 クラス
				ps5.setString(11, exam.getExamYear());
				ps5.setString(12, exam.getExamCD());
				// 13 科目
				
			}
		
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				C14ListBean bean = new C14ListBean();
				bean.setStrGrade(rs1.getString(1));
				bean.setStrClass(rs1.getString(3));
				data.getC14List().add(bean);

				// 学年
				int grade = rs1.getInt(1);
				ps2.setInt(6, grade);
				ps2.setInt(15, grade);
				ps3.setInt(3, grade);
				// クラス
				String className = rs1.getString(2);
				ps2.setString(7, className);
				ps2.setString(11, className);
				ps2.setString(16, className);
				ps2.setString(20, className);
				ps3.setString(4, className);
				ps3.setString(11, className);
				
				rs2 = ps2.executeQuery();
				while (rs2.next()) {
					
					//科目コード/対象クラス/対象学年の何れかが異なる場合に
					//全体成績を設定する
					if (!tmp_kmkcd.equals(rs2.getString(1))
							|| !tmp_classcd.equals(rs1.getString(2))
							|| tmp_grade != rs1.getInt(1)) {
						putTotalSubjectData(bean, rs1, rs2, ps4, ps5, isNewExam);
					}
					
					C14KmkListBean k = new C14KmkListBean();
					k.setStrKmkCd(rs2.getString(1));
					k.setStrKmkmei(rs2.getString(2));
					k.setStrHaitenKmk(rs2.getString(3));
					k.setStrSetsuNo(rs2.getString(4));
					k.setStrSetsuMei1(rs2.getString(5));
					k.setStrSetsuHaiten(rs2.getString(6));
					k.setIntNinzu(rs2.getInt(8));
					
					// 新テストなら全国平均・県平均は無効値
					if (isNewExam) {
						k.setFloHeikinAll(-999);
						k.setFloHeikinKen(-999);
					} else {
						k.setFloHeikinAll(rs2.getFloat(9));
						k.setFloHeikinKen(rs2.getFloat(10));
					}
					
					k.setFloHeikinHome(rs2.getFloat(11));
					k.setFloHeikinCls(rs2.getFloat(12));
					bean.getC14KmkList().add(k);

					// 科目コード
					ps3.setString(7, k.getStrKmkCd());
					ps3.setString(14, k.getStrKmkCd());
					// 設問番号
					int questionNo = rs2.getInt(7);
					ps3.setInt(8, questionNo);
					ps3.setInt(15, questionNo);

					rs3 = ps3.executeQuery();
					while (rs3.next()) {
						C14PersonalDataListBean p = new C14PersonalDataListBean();
						p.setStrGrade(rs3.getString(1));
						p.setStrClass(rs3.getString(2));
						p.setStrClassNo(rs3.getString(3));
						p.setStrKanaName(rs3.getString(4));
						p.setStrSex(rs3.getString(5));
						p.setIntTokuten(rs3.getInt(6));
						p.setFloTokuritsu(rs3.getFloat(7));
						k.getC14PersonalDataList().add(p);
					}
					rs3.close();
					// 処理中の科目コードをセット
					tmp_kmkcd = rs2.getString(1);
					// 処理中のクラスコードをセット
					tmp_classcd = rs1.getString(2);
					// 処理中の学年をセット
					tmp_grade = rs1.getInt(1);
				}
				rs2.close();
			}
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(rs3);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(ps3);
			DbUtils.closeQuietly(ps4);
			DbUtils.closeQuietly(ps5);
		}
	}

	/**
	 * 設問別成績の先頭に「全体成績」データをセットする
	 * @param bean
	 * @param rs1
	 * @param rs2
	 * @param ps4
	 * @param ps5
	 * @param isNewExam
	 * @throws SQLException 
	 */
	private void putTotalSubjectData(C14ListBean bean, ResultSet rs1, ResultSet rs2, PreparedStatement ps4,
			PreparedStatement ps5, boolean isNewExam) throws SQLException {
		
		ResultSet rs4 = null;
		ResultSet rs5 = null;
		
		try {
			C14KmkListBean k = new C14KmkListBean();
			k.setStrKmkCd(rs2.getString(1));
			k.setStrKmkmei(rs2.getString(2));
			k.setStrHaitenKmk(rs2.getString(3));
			k.setStrSetsuNo("");					// 設問Noに「""」をセット
			k.setStrSetsuMei1("全体成績");			// 設問名に「"全体成績"」をセット
			k.setStrSetsuHaiten(rs2.getString(3));	// 配点に科目配点をセット
			k.setIntNinzu(rs2.getInt(8));
			
			//6:grade
			ps4.setInt(6, rs1.getInt(1));
			//7:class
			ps4.setString(7, rs1.getString(2));
			//11:classgcd
			ps4.setString(11, rs1.getString(2));
			//15:grade
			ps4.setInt(15, rs1.getInt(1));
			//16:class
			ps4.setString(16, rs1.getString(2));
			//20:classgcd
			ps4.setString(20, rs1.getString(2));
			//23:subcd
			ps4.setString(23, k.getStrKmkCd());
			
			rs4 = ps4.executeQuery();
			if (rs4.next() && !isNewExam) {
				k.setFloHeikinAll(rs4.getFloat(3));
				k.setFloHeikinKen(rs4.getFloat(4));
				k.setFloHeikinHome(rs4.getFloat(5));
				k.setFloHeikinCls(rs4.getFloat(6));
			}
			else {
				k.setFloHeikinAll(-999);
				k.setFloHeikinKen(-999);
				k.setFloHeikinHome(-999);
				k.setFloHeikinCls(-999);
			}

			bean.getC14KmkList().add(k);
	
			// 科目コード
			ps5.setString(7, k.getStrKmkCd());
			ps5.setString(13, k.getStrKmkCd());
			// 学年
			ps5.setInt(3, rs1.getInt(1));
			// クラス
			ps5.setString(4, rs1.getString(2));
			ps5.setString(10, rs1.getString(2));
				
			rs5 = ps5.executeQuery();
			while (rs5.next()) {
				C14PersonalDataListBean p = new C14PersonalDataListBean();
				p.setStrGrade(rs5.getString(1));
				p.setStrClass(rs5.getString(2));
				p.setStrClassNo(rs5.getString(3));
				p.setStrKanaName(rs5.getString(4));
				p.setStrSex(rs5.getString(5));
				p.setIntTokuten(rs5.getInt(6));
				p.setFloTokuritsu(rs5.getFloat(7));
				k.getC14PersonalDataList().add(p);
			}
			rs5.close();
			
		}
		finally {
			DbUtils.closeQuietly(rs4);
			DbUtils.closeQuietly(rs5);
		}
		
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("C14_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.C_COND_QUE;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new C14()
			.c14(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
	}

}

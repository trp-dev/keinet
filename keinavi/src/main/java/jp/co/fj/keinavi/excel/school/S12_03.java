/**
 * 校内成績分析−校内成績　偏差値分布（偏差値帯別度数分布グラフ（人数））
 * 	Excelファイル編集
 * 作成日: 2004/07/09
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S12BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S12Item;
import jp.co.fj.keinavi.excel.data.school.S12ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S12_03 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
//	private String		strArea			= "A1:AQ58";	//印刷範囲
	
	final private String masterfile0 = "S12_03";
	final private String masterfile1 = "S12_03";
	private String masterfile = "";
	final private int intMaxSheetSr = 10;	// 最大シート数の値を入れる

/*
 * 	Excel編集メイン
 * 		S12Item s12Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s12_03EditExcel(S12Item s12Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s12Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		// 基本ファイルを読込む
		S12ListBean s12ListBean = new S12ListBean();
		
		try {
			int		row					= 0;		// 行
			int		col					= 0;		// 列
			int		ninzu				= 0;		// *作成用
			int		setCol				= 0;		// *セット用
			int		kmkCnt				= 0;		// 型・科目数カウンタ
			int		intMaxSheetIndex	= 1;		// シートカウンタ
			int		intBookCngCount		= 0;		// ファイルカウンタ
			boolean	bolSheetCngFlg		= true;	// 改シートフラグ
			boolean	bolBookCngFlg		= true;	// 改ファイルフラグ
			boolean	bolkmkCngFlg		= true;	// 型→科目切替わりフラグ
//add 2004/10/25 T.Sakai データ0件対応
			int		dispKmkFlgCnt		= 0;		// グラフ表示フラグカウンタ
//add end

			// データセット
			ArrayList s12List = s12Item.getS12List();
			Iterator itr = s12List.iterator();
			
			while( itr.hasNext() ) {
				s12ListBean = (S12ListBean)itr.next();
				
				if ( s12ListBean.getIntDispKmkFlg()==1 ) {
					//型から科目に変わる時のチェック
					if ( s12ListBean.getStrKmkCd().compareTo("7000") < 0 ) {
						if (bolkmkCngFlg) {
							bolSheetCngFlg = true;
							bolkmkCngFlg = false;
						} else {
							if (kmkCnt>=4) {
								bolSheetCngFlg = true;
							}
						}
					} else {
						if (kmkCnt>=4) {
							bolSheetCngFlg = true;
						}
					}
					if (bolSheetCngFlg && intMaxSheetIndex>intMaxSheetSr) {
						bolBookCngFlg = true;
					}
					if (bolSheetCngFlg) {
						if (bolBookCngFlg) {
							// Excelファイル保存
							if (intMaxSheetIndex!=1) {
								boolean bolRet = false;
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex-1);
								intBookCngCount++;
								if( bolRet == false ){
									return errfwrite;					
								}
							}
							//マスタExcel読み込み
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook==null ){
								return errfread;
							}
							intMaxSheetIndex = 1;
						}
						// データセットするシートの選択
						workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex));
						intMaxSheetIndex++;

						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);
						
						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, s12Item.getIntSecuFlg() ,41 ,42 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 41 );
						workCell.setCellValue(secuFlg);
						
						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
						workCell.setCellValue( "学校名　：" + cm.toString(s12Item.getStrGakkomei()) );
				
						// 模試月取得
						String moshi =cm.setTaisyouMoshi( s12Item.getStrMshDate() );
						// 対象模試セット
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s12Item.getStrMshmei()) + moshi);
//add 2004/10/25 T.Sakai スペース対応
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 2 );
						workCell.setCellValue( "〜32.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 4 );
						workCell.setCellValue( "32.5〜\n34.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 6 );
						workCell.setCellValue( "35.0〜\n37.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 8 );
						workCell.setCellValue( "37.5〜\n39.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 10 );
						workCell.setCellValue( "40.0〜\n42.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 12 );
						workCell.setCellValue( "42.5〜\n44.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 14 );
						workCell.setCellValue( "45.0〜\n47.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 16 );
						workCell.setCellValue( "47.5〜\n49.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 18 );
						workCell.setCellValue( "50.0〜\n52.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 20 );
						workCell.setCellValue( "52.5〜\n54.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 22 );
						workCell.setCellValue( "55.0〜\n57.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 24 );
						workCell.setCellValue( "57.5〜\n59.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 26 );
						workCell.setCellValue( "60.0〜\n62.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 28 );
						workCell.setCellValue( "62.5〜\n64.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 30 );
						workCell.setCellValue( "65.0〜\n67.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 32 );
						workCell.setCellValue( "67.5〜\n69.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 34 );
						workCell.setCellValue( "70.0〜\n72.4" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 36 );
						workCell.setCellValue( "72.5〜\n74.9" );
						workCell = cm.setCell( workSheet, workRow, workCell, 49, 38 );
						workCell.setCellValue( "75.0〜" );
//add end
						bolSheetCngFlg = false;
						bolBookCngFlg = false;
						kmkCnt = 0;
						row = 50;
					}
					col = 1;
					// 型・科目名＋配点セット
					String haiten = "";
					if ( !cm.toString(s12ListBean.getStrHaitenKmk()).equals("") ) {
						haiten = "（" + s12ListBean.getStrHaitenKmk() + "）";
					}
					workCell = cm.setCell( workSheet, workRow, workCell, row, col );
					workCell.setCellValue( s12ListBean.getStrKmkmei() + haiten );
					workCell = cm.setCell( workSheet, workRow, workCell, row+4, col );
					workCell.setCellValue( s12ListBean.getStrKmkmei() + haiten );
					
					// 基本ファイルを読込む
					S12BnpListBean s12BnpListBean = new S12BnpListBean();
					
					// 偏差値データセット
					ArrayList s12BnpList = s12ListBean.getS12BnpList();
					Iterator itrBnp = s12BnpList.iterator();
					
					col = 39;
					// 合計人数セット
					workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
					if ( s12ListBean.getIntNinzu() != -999 ) {
						workCell.setCellValue( s12ListBean.getIntNinzu() );
					}
					// 平均点（高校）セット
					workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
					if ( s12ListBean.getFloHeikin() != -999.0 ) {
						workCell.setCellValue( s12ListBean.getFloHeikin() );
					}
					// 平均偏差値（高校）セット
					workCell = cm.setCell( workSheet, workRow, workCell, row, col+3 );
					if ( s12ListBean.getFloHensa() != -999.0 ) {
						workCell.setCellValue( s12ListBean.getFloHensa() );
					}
					setCol = 0;
					while( itrBnp.hasNext() ) {
						s12BnpListBean = (S12BnpListBean)itrBnp.next();
						// 人数・校内セット
						workCell = cm.setCell( workSheet, workRow, workCell, row, col );
						if ( s12BnpListBean.getIntNinzu() != -999 ) {
							workCell.setCellValue( s12BnpListBean.getIntNinzu() );
						}
						// 構成比・校内セット
						workCell = cm.setCell( workSheet, workRow, workCell, row+4, col );
						if ( s12BnpListBean.getFloKoseihi() != -999.0 ) {
							workCell.setCellValue( s12BnpListBean.getFloKoseihi() );
						}
						// *作成用
						if ( ninzu < s12BnpListBean.getIntNinzu() ) {
							ninzu = s12BnpListBean.getIntNinzu();
							setCol = col - 1;
						}
						col = col - 2;
					}
					// *セット
					workCell = cm.setCell( workSheet, workRow, workCell, row, setCol );
					if ( setCol!=0 ) {
						workCell.setCellValue("*");
					}
					kmkCnt++;
					row++;
					ninzu = 0;
//add 2004/10/25 T.Sakai データ0件対応
					dispKmkFlgCnt++;
//add end
				}
			}
//add 2004/10/25 T.Sakai データ0件対応
			if ( s12List.size()==0 || dispKmkFlgCnt==0 ) {
				//マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				intMaxSheetIndex = 1;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex));
				intMaxSheetIndex++;
				
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s12Item.getIntSecuFlg() ,41 ,42 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 41 );
				workCell.setCellValue(secuFlg);
				
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s12Item.getStrGakkomei()) );
				
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s12Item.getStrMshDate() );
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s12Item.getStrMshmei()) + moshi);
//add 2004/10/25 T.Sakai スペース対応
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 2 );
				workCell.setCellValue( "〜32.4" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 4 );
				workCell.setCellValue( "32.5〜\n34.9" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 6 );
				workCell.setCellValue( "35.0〜\n37.4" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 8 );
				workCell.setCellValue( "37.5〜\n39.9" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 10 );
				workCell.setCellValue( "40.0〜\n42.4" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 12 );
				workCell.setCellValue( "42.5〜\n44.9" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 14 );
				workCell.setCellValue( "45.0〜\n47.4" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 16 );
				workCell.setCellValue( "47.5〜\n49.9" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 18 );
				workCell.setCellValue( "50.0〜\n52.4" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 20 );
				workCell.setCellValue( "52.5〜\n54.9" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 22 );
				workCell.setCellValue( "55.0〜\n57.4" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 24 );
				workCell.setCellValue( "57.5〜\n59.9" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 26 );
				workCell.setCellValue( "60.0〜\n62.4" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 28 );
				workCell.setCellValue( "62.5〜\n64.9" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 30 );
				workCell.setCellValue( "65.0〜\n67.4" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 32 );
				workCell.setCellValue( "67.5〜\n69.9" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 34 );
				workCell.setCellValue( "70.0〜\n72.4" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 36 );
				workCell.setCellValue( "72.5〜\n74.9" );
				workCell = cm.setCell( workSheet, workRow, workCell, 49, 38 );
				workCell.setCellValue( "75.0〜" );
//add end
			}
//add end
			// Excelファイル保存
			boolean bolRet = false;
			if(intBookCngCount != 0){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, intMaxSheetIndex-1);
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex-1);
			}

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("S12_03","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

}
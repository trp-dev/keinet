package jp.co.fj.keinavi.servlets.txt_out;

import java.io.IOException;
import java.sql.Connection;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.help.OnepointBean;
import jp.co.fj.keinavi.beans.txt_out.TextInfoBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.AbstractActionFormFactory;
import jp.co.fj.keinavi.forms.txt_out.TMaxForm;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.log.KNLog;

/**
 *
 * テキスト出力 - 詳細画面
 *
 * 2005.02.15	Yoshimoto KAWAI - Totec
 *				対象模試を保持するように変更
 *
 * 2005.04.20	Yoshimoto KAWAI - Totec
 * 				requestへ新テストフラグをセット
 *
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 * <2010年度マーク高２模試対応>
 * 2011.01.11   Tomohisa YAMADA - Totec
 *              倫政対応・マーク高２対応有
 *
 * @author kawai
 *
 */
public class T002Servlet extends AbstractTServlet {

	protected static final Set detailSet = new HashSet(); // 詳細画面の画面IDセット

	static {
		detailSet.add("t002");
		detailSet.add("t003");
		detailSet.add("t004");
		detailSet.add("t005");
		detailSet.add("t102");
		detailSet.add("t103");
		detailSet.add("t104");
// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD START
		detailSet.add("t106");
// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD END
		detailSet.add("t202");
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// アクションフォームの取得 - request scope
		TMaxForm form = (TMaxForm) getActionForm(request,
				"jp.co.fj.keinavi.forms.txt_out.TMaxForm");

		// ログイン情報
		final LoginSession login = getLoginSession(request);
		// ログのインスタンス
		final KNLog log = KNLog.getInstance(
				getRemoteAddr(request),login.getUserID(),login.getSectorSortingCD());
		// プロファイル
		final Profile profile = getProfile(request);
		// 模試セッション
		final ExamSession examSession = getClassExamSession(request);
		// フィルタリングを有効にするためリクエストにセットする
		request.setAttribute(ExamSession.SESSION_KEY, examSession);
		// 対象模試データ
		ExamData exam = examSession.getExamData(form.getTargetYear(), form.getTargetExam());

		// 一括出力　出力形式初期化
		profile.getItemMap("050100").put("1402", new Short("0"));

		// 転送元が詳細画面
		if (detailSet.contains(getBackward(request))) {
			// 模試を変更したら設定値はクリアする
			if ("1".equals(form.getChangeExam())) {
				form.setOutTarget(null);
			}

			// 対象模試が存在しなければ初期化する
			if (exam == null) {
				// 対象年度のレコードがあるならリストの最上位
				if (examSession.getExamMap().containsKey(form.getTargetYear())) {
					exam = (ExamData) examSession.getExamList(form.getTargetYear()).get(0);

				// なければ最新模試
				} else {
					exam = KNUtil.getLatestExamData(examSession);
				}

				if (exam != null) {
					form.setTargetYear(exam.getExamYear());
					form.setTargetExam(exam.getExamCD());
				}
			}

			// 対象模試を保持する
			// NULLが来る可能性があるので排除
			if (form.getTargetYear() != null) {
				profile.setTargetYear(form.getTargetYear());
				profile.setTargetExam(form.getTargetExam());
			}

		// そうでなければ初期化する
		} else {
			form =	(TMaxForm) AbstractActionFormFactory.getFactory(getForward(request)).createActionForm(request);
			// 対象模試を取得し直す
			exam =	examSession.getExamData(form.getTargetYear(),form.getTargetExam());
		}
		// リクエストへアクションフォームをセット
		request.setAttribute("form", form);

		// 詳細画面の画面IDなら遷移する
		if (detailSet.contains(getForward(request))) {

			// 転送先が個人成績・入試結果調査なら担当クラスをセットする
			if (getForward(request).startsWith("t10") || getForward(request).startsWith("t20")) {
				super.setupChargeClass(request);
			}

			Connection con = null; // DBコネクション
			try {
				con = super.getConnectionPool(request);
				//---------------------------------------------------
				// 画面単位のワンポイントアドバイス（画面ID指定）
				//---------------------------------------------------
				OnepointBean onebean = new OnepointBean();
				onebean.setConnection(null, con);
				onebean.setScreenID(getForward(request));
				onebean.execute();
				request.setAttribute("OnepointBean", onebean);
				//---------------------------------------------------

				// リクエストへ出力対象項目をセット
				request.setAttribute("TextInfoBean", TextInfoBean.getInstance(exam));
				// リクエストへ対象模試データをセット
				request.setAttribute("ExamData", exam);
			} catch (Exception ex) {
				ex.printStackTrace();
				log.Err("0T002990101", ex.getMessage(), ex);
				throw new ServletException(ex);
			} finally {
				super.releaseConnectionPool(request, con);
			}

			// 対象模試が新テストかどうか
			request.setAttribute("NewExam",
					Boolean.valueOf(KNUtil.isNewExam(exam)));
			// センターリサーチフラグ
			request.setAttribute("isCenterResearch",
					Boolean.valueOf(KNUtil.isCenterResearch(exam)));

			super.forward(request, response, JSP_T002);

		// 不明なら転送
		} else {
			// 保存処理
			if ("1".equals(form.getSave())) {
				AbstractActionFormFactory.getFactory(getBackward(request)).restore(	request);
				profile.setChanged(true);
			}
			// requestスコープの模試セッションは消しておく
			request.removeAttribute(ExamSession.SESSION_KEY);
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}


}

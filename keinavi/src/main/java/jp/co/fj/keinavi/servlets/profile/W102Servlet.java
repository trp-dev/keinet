package jp.co.fj.keinavi.servlets.profile;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.profile.CountingDivBean;
import jp.co.fj.keinavi.beans.profile.ProfileBean;
import jp.co.fj.keinavi.beans.profile.ProfileFolderBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.profile.W102Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class W102Servlet extends DefaultHttpServlet {

	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// プロファイル別名保存画面への遷移
		if ("w102".equals(getForward(request))) {
			// ログイン情報
			LoginSession login = super.getLoginSession(request);
			// プロファイル
			Profile profile = super.getProfile(request);
		
			// アクションフォームの取得
			W102Form form = (W102Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.profile.W102Form");

			// 転送元がw10*でなければ呼び出し元をセットする
			if (!super.getBackward(request).startsWith("w10")) {
				form.setOrigin(super.getBackward(request));
			}

			Connection con = null; // DBコネクション
			try {
				con = super.getConnectionPool(request);

				// プロファイルがなければプロファイル選択画面からの遷移
				if (profile == null) {
					ProfileBean bean = new ProfileBean();
					bean.setConnection(null, con);
					bean.setLoginSession(login);
					bean.setProfileID(form.getProfileID());
					bean.setBundleCD(form.getCountingDivCode());
					bean.execute();

					// セッションに入れる
					profile = bean.getProfile();
					profile.setDbKey(login.getDbKey()); // DB-KEY
					request.getSession(false).setAttribute(Profile.SESSION_KEY, profile);
				}
		
				// 集計区分Bean
				{
					CountingDivBean bean = new CountingDivBean();
					bean.setConnection(null, con);
					bean.execute();
					request.setAttribute("CountingDivBean", bean);
				}

				// プロファイルフォルダBean
				{
					ProfileFolderBean bean = new ProfileFolderBean();
					bean.setConnection(null, con);
					bean.setLoginSession(login);
					bean.setBundleCD(profile.getBundleCD());
					bean.execute();
					request.setAttribute("ProfileFolderBean", bean);
				}
			} catch (Exception e) {
				throw new ServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}

			// 現在時刻をセット
			request.setAttribute("now", new Date());

			// アクションフォームをセット
			request.setAttribute("form", form);

			super.forward(request, response, JSP_W102);

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);			
		}

	}

}

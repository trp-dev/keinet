/*
 * 作成日: 2004/07/01
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.com_set;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.data.com_set.cm.SchoolData;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class SchoolBean extends AbstractComBean {

	private String targetYear; // 対象年度
	private String targetExam; // 対象模試
	private String[] schoolCodeArray; // 高校コードの配列
	private List schoolList = new ArrayList(); // 高校オブジェクトのリスト

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		if (schoolCodeArray == null || schoolCodeArray.length == 0) return;

		List container = new LinkedList(); // 入れ物

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Query query = QueryLoader.getInstance().load("cm03");
			query.setStringArray(1, schoolCodeArray); // 一括コード

			ps = conn.prepareStatement(query.toString());
			ps.setString(1, this.targetYear); // 対象年度
			ps.setString(2, this.targetExam); // 対象模試
			ps.setString(3, this.targetYear); // 対象年度
			ps.setString(4, this.targetExam); // 対象模試
			ps.setString(5, this.targetYear); // 対象年度
			ps.setString(6, this.targetExam); // 対象模試
			ps.setString(7, this.targetYear); // 対象年度
			ps.setString(8, this.targetExam); // 対象模試

			rs = ps.executeQuery();
			while (rs.next()) {
				SchoolData data = new SchoolData();
				data.setSchoolCode(rs.getString(1));
				data.setSchoolName(rs.getString(2));
				data.setRank(rs.getString(3));
				data.setLocation(rs.getString(4));
				data.setExaminees(rs.getInt(5));
				container.add(data);
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}

		// 高校コードの配列の順序で格納する
		for (int i=0; i<schoolCodeArray.length; i++) {
			int index = container.indexOf(new SchoolData(schoolCodeArray[i]));
			if (index >= 0) {
				schoolList.add(container.get(index));
			}
		} 
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.com_set.AbstractComBean#getFullList()
	 */
	public List getFullList() {
		return schoolList;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.com_set.AbstractComBean#getPartList()
	 */
	public List getPartList() {
		return null;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.com_set.AbstractComBean#getFullListSize()
	 */
	public int getFullListSize() {
		return schoolList.size();
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.com_set.AbstractComBean#getPartListSize()
	 */
	public int getPartListSize() {
		return 0;
	}

	/**
	 * @param strings
	 */
	public void setSchoolCodeArray(String[] strings) {
		schoolCodeArray = strings;
	}

	/**
	 * @param string
	 */
	public void setTargetExam(String string) {
		targetExam = string;
	}

	/**
	 * @param string
	 */
	public void setTargetYear(String string) {
		targetYear = string;
	}

}

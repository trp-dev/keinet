package jp.co.fj.keinavi.auth;


import jp.co.fj.keinavi.data.LoginSession;

/**
 *
 * 模試受付システム連携用マネージャ
 * 
 * 2007.10.30	[新規作成]
 * 
 *
 * @author Fujito URAKAWA - TOTEC
 * @version 1.0
 * 
 */
public class MoshiSysManager extends KnetCookieManager {

	private String sciv = null;
	private String scid = null;
	private String scnm = null;
	
	/**
	 * コンストラクタ
	 */
	public MoshiSysManager() {
		super("www.kawa1-juku.a4.jp");
	}
	
	public void create(final LoginSession login) throws Exception {
		
		// IV
		sciv = encode(ec.getIv());
		// 学校会員ID
		scid = toValue(login.getLoginID());
		// 学校名
		scnm = toValue(login.getUserName());
	}

	public String getScid() {
		return scid;
	}

	public String getSciv() {
		return sciv;
	}

	public String getScnm() {
		return scnm;
	}

}

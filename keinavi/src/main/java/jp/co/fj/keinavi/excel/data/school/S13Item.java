package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * 設問別成績（校内成績）データクラス
 * 作成日: 2004/07/06
 * @author	T.Kuzuno
 *
 * 2009.10.20 Fujito URAKAWA - Totec
 *            新校内資料「科目・設問別正答率（レベル別）」対応
 */
public class S13Item {
	//学校名
	private String strGakkomei = "";
	//県名
	private String strKenmei = "";
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//表フラグ
	private int intHyouFlg = 0;
	//グラフフラグ
	private int intGraphFlg = 0;
	//マーク模試正答状況フラグ
	private int intMarkFlg = 0;
	//マーク模試高校別設問成績層正答率フラグ
	private int intMarkAreaFlg = 0;
	// 2019/09/04 QQ)Tanioka  ADD START
	//国語 評価別人数フラグ
	private int intKokugoFlg = 0;
	//小設問別正答状況フラグ
	private int intStatusFlg = 0;
	//小設問別成績フラグ
	private int intRecordFlg = 0;
	// 2019/09/04 QQ)Tanioka  ADD END
	//セキュリティスタンプフラグ
	private int intSecuFlg = 0;
	//設問別成績データリスト
	private ArrayList s13SeisekiList = new ArrayList();
	//マーク模試正答状況データリスト
	private ArrayList s13MarkList = new ArrayList();
	//マーク模試高校別設問成績層正答率データリスト
	private ArrayList s13MarkAreaList = new ArrayList();
	// 2019/09/12 QQ)Tanouchi  ADD START
	//国語評価別人数データリスト
	private ArrayList s13KokugoSetumon = new ArrayList();
	//国語評価別人数データリスト
	private ArrayList s13KokugoSougou = new ArrayList();
	// 2019/09/12 QQ)Tanouchi  ADD END
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;
	//2019/09/10 QQ)nagai 共通テスト対応 ADD START
	//小設問別正答状況データリスト
	private ArrayList s13SugakuKokugoQueList = new ArrayList();
	//2019/09/10 QQ)nagai 共通テスト対応 ADD END
	// 2019/09/25 QQ)Ooseto 共通テスト対応 ADD START
        // 記述系模試小設問別成績データリスト
        private ArrayList s13RecordList = new ArrayList();
        // 2019/09/25 QQ)Ooseto 共通テスト対応 ADD END



	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrKenmei() {
		return this.strKenmei;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public int getIntHyouFlg() {
		return this.intHyouFlg;
	}
	public int getIntGraphFlg() {
		return this.intGraphFlg;
	}
	public int getIntMarkFlg() {
		return this.intMarkFlg;
	}
	public int getIntMarkAreaFlg() {
		return this.intMarkAreaFlg;
	}
	// 2019/09/04 QQ)Tanioka  ADD START
	public int getIntKokugoFlg() {
		return this.intKokugoFlg;
	}
	public int getIntStatusFlg() {
		return this.intStatusFlg;
	}
	public int getIntRecordFlg() {
		return this.intRecordFlg;
	}
	// 2019/09/04 QQ)Tanioka  ADD END
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public ArrayList getS13SeisekiList() {
		return this.s13SeisekiList;
	}
	public ArrayList getS13MarkList() {
		return this.s13MarkList;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}
	public ArrayList getS13MarkAreaList() {
		return s13MarkAreaList;
	}
	// 2019/09/12 QQ)Tanouchi  ADD START
	public ArrayList getS13KokugoSetumon() {
		return  s13KokugoSetumon;
	}
	public ArrayList getS13KokugoSougou() {
		return  s13KokugoSougou;
	}
	// 2019/09/12 QQ)Tanouchi  ADD END
	//2019/09/10 QQ)nagai 共通テスト対応 ADD START
	public ArrayList getS13SugakuKokugoQueList() {
		return this.s13SugakuKokugoQueList;
	}
	//2019/09/10 QQ)nagai 共通テスト対応 ADD END
        // 2019/09/25 QQ)Ooseto 共通テスト対応 ADD START
        public ArrayList getS13RecordList() {
            return s13RecordList;
        }
        // 2019/09/25 QQ)Ooseto 共通テスト対応 ADD END

	/*---------------*/
	/* Set */
	/*---------------*/
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrKenmei(String strKenmei) {
		this.strKenmei = strKenmei;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setIntHyouFlg(int intHyouFlg) {
		this.intHyouFlg = intHyouFlg;
	}
	public void setIntGraphFlg(int intGraphFlg) {
		this.intGraphFlg = intGraphFlg;
	}
	public void setIntMarkFlg(int intMarkFlg) {
		this.intMarkFlg = intMarkFlg;
	}
	public void setIntMarkAreaFlg(int intMarkAreaFlg) {
		this.intMarkAreaFlg = intMarkAreaFlg;
	}
	// 2019/09/04 QQ)Tanioka  ADD START
	public void setIntKokugoFlg(int intKokugoFlg) {
		this.intKokugoFlg = intKokugoFlg;
	}
	public void setIntStatusFlg(int intStatusFlg) {
		this.intStatusFlg = intStatusFlg;
	}
	public void setIntRecordFlg(int intRecordFlg) {
		this.intRecordFlg = intRecordFlg;
	}
	// 2019/09/04 QQ)Tanioka  ADD END
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setS13SeisekiList(ArrayList s13SeisekiList) {
		this.s13SeisekiList = s13SeisekiList;
	}
	public void setS13MarkList(ArrayList s13MarkList) {
		this.s13MarkList = s13MarkList;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}
	public void setS13MarkAreaList(ArrayList markAreaList) {
		s13MarkAreaList = markAreaList;
	}
	// 2019/09/12 QQ)Tanouchi  ADD START
	public void setS13KokugoSetumon(ArrayList KokugoSetumon) {
		s13KokugoSetumon = KokugoSetumon;
	}
	public void setS13KokugoSougou(ArrayList KokugoSougou) {
		s13KokugoSougou = KokugoSougou;
	}
	// 2019/09/12 QQ)Tanouchi  ADD END
	//2019/09/10 QQ)nagai 共通テスト対応 ADD START
	public void setS13SugakuKokugoQueList(ArrayList s13SugakuKokugoQueList) {
		this.s13SugakuKokugoQueList = s13SugakuKokugoQueList;
	}
	//2019/09/10 QQ)nagai 共通テスト対応 ADD END
        // 2019/09/25 QQ)Ooseto 共通テスト対応 ADD START
        public void setS13RecordList(ArrayList s13RecordList) {
            this.s13RecordList = s13RecordList;
        }
        // 2019/09/25 QQ)Ooseto 共通テスト対応 ADD END

}

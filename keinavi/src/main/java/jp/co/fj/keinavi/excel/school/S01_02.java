/**
 * 校内成績分析−全国総合成績概況(高２模試)
 * 	Excelファイル編集
 * 作成日: 2004/07/16
 * @author	H.Fujimoto
 */

package jp.co.fj.keinavi.excel.school;

import java.io.*;
import java.util.*;
import java.text.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.excel.cm.*;

import org.apache.poi.hssf.usermodel.*;
import jp.co.fj.keinavi.util.log.*;

public class S01_02 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス
	
//	private String		strArea			= "A1:H77";	//印刷範囲	
	
	final private String masterfile0 = "S01_02";
	final private String masterfile1 = "S01_02";
	private String masterfile = "";

/*
 * 	Excel編集メイン
 * 		S01Item s01Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s01_02EditExcel(S01Item s01Item, ArrayList outfilelist, int intSaveFlg, String UserID ) {

		FileInputStream	fin			= null;
		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		int			intMaxSheetIndex	= 0;	//シートカウンタ
		int			kmkMaxSheetCnt		= 1;	//科目用シートカウンタ
		int			kataCnt				= 0;	//型カウンタ
		int			kmkCnt				= 0;	//科目カウンタ
		NumberFormat nf = NumberFormat.getInstance();
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s01Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		//マスタExcel読み込み
		workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
		if( workbook==null ){
			return errfread;
		}

		// 基本ファイルを読込む
		S01KataListBean s01KataListBean = new S01KataListBean();
		S01KmkListBean s01KmkListBean = new S01KmkListBean();
		
		try {
			// 型データセット
			ArrayList s01KataList = s01Item.getS01KataList();
			Iterator itrKata = s01KataList.iterator();
			int row = 9;
			
			log.Ep("S01_02","型データセット開始","");
			
			while( itrKata.hasNext() ) {
				s01KataListBean = (S01KataListBean)itrKata.next();
				if (kataCnt == 0) {
					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);
					intMaxSheetIndex++;
					row = 9;
					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);

					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( cm.toString(s01Item.getStrMshmei()) + "　成績表");
			
					// 受験者数セット
					workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
//					workCell.setCellValue( "受験者総数：" + nf.format(s01Item.getIntGenNinzu())+ "名");
					workCell.setCellValue( "受験者総数：" + nf.format(s01Item.getIntAllNinzu())+ "名");
				}
				// 型名セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 0 );
				workCell.setCellValue( s01KataListBean.getStrKmkmei() );
				// 配点セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 1 );
				if ( !cm.toString(s01KataListBean.getStrHaitenKmk()).equals("") ) {
					workCell.setCellValue( Integer.parseInt( s01KataListBean.getStrHaitenKmk() ) );
				}
				// 平均点・現役セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 2 );
//				if ( s01KataListBean.getFloHeikinGen() != -999.0 ) {
//					workCell.setCellValue( s01KataListBean.getFloHeikinGen() );
				if ( s01KataListBean.getFloHeikinAll() != -999.0 ) {
					workCell.setCellValue( s01KataListBean.getFloHeikinAll() );
				}
				// 標準偏差セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 3 );
				if ( s01KataListBean.getFloStdHensa() != -999.0 ) {
					workCell.setCellValue( s01KataListBean.getFloStdHensa() );
				}
				// 平均偏差値・現役セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 4 );
//				if ( s01KataListBean.getFloHensaGen() != -999.0 ) {
//					workCell.setCellValue( s01KataListBean.getFloHensaGen() );
				if ( s01KataListBean.getFloHensaAll() != -999.0 ) {
					workCell.setCellValue( s01KataListBean.getFloHensaAll() );
				}
				// 最高点
				workCell = cm.setCell( workSheet, workRow, workCell, row, 5 );
				if ( s01KataListBean.getIntMaxTen() != -999 ) {
					workCell.setCellValue( s01KataListBean.getIntMaxTen() );
				}
				// 最低点
				workCell = cm.setCell( workSheet, workRow, workCell, row, 6 );
				if ( s01KataListBean.getIntMinTen() != -999 ) {
					workCell.setCellValue( s01KataListBean.getIntMinTen() );
				}
				// 人数・現役セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 7 );
//				if ( s01KataListBean.getIntNinzuGen() != -999 ) {
//					workCell.setCellValue( s01KataListBean.getIntNinzuGen() );
				if ( s01KataListBean.getIntNinzuAll() != -999 ) {
					workCell.setCellValue( s01KataListBean.getIntNinzuAll() );
				}
				row++;
				kataCnt++;
				log.It("S01_02","型カウント",String.valueOf(kataCnt));
				if (kataCnt >= 20) {
					kataCnt = 0;
				}
			}
			log.Ep("S01_02","型データセット終了","");

			// 科目データセット
			ArrayList s01KmkList = s01Item.getS01KmkList();
			Iterator itrKmk = s01KmkList.iterator();
			row = 37;
			
			log.Ep("S01_02","科目データセット開始","");
			
			if ( intMaxSheetIndex!=0 ) {
				if (intSaveFlg==1 || intSaveFlg==5) {
					workSheet = workbook.getSheetAt(kmkMaxSheetCnt);
				} else {
					workSheet = workbook.getSheetAt(kmkMaxSheetCnt+1);
				}
			}
			
			while( itrKmk.hasNext() ) {
				s01KmkListBean = (S01KmkListBean)itrKmk.next();
				if (kmkCnt == 0) {
					if (kmkMaxSheetCnt>intMaxSheetIndex) {
						// シートテンプレートのコピー
						workSheet = workbook.cloneSheet(0);
						intMaxSheetIndex++;
						kmkMaxSheetCnt++;
					} else {
						if (intSaveFlg==1 || intSaveFlg==5) {
							workSheet = workbook.getSheetAt(kmkMaxSheetCnt);
						} else {
							workSheet = workbook.getSheetAt(kmkMaxSheetCnt+1);
						}
						kmkMaxSheetCnt++;
					}
					row = 37;
					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);

					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( cm.toString(s01Item.getStrMshmei()) + "　成績表");
			
					// 受験者数セット
					workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
//					workCell.setCellValue( "受験者総数：" + nf.format(s01Item.getIntGenNinzu())+ "名");
					workCell.setCellValue( "受験者総数：" + nf.format(s01Item.getIntAllNinzu())+ "名");
				}
				// 科目名セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 0 );
				workCell.setCellValue( s01KmkListBean.getStrKmkmei() );
				// 配点セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 1 );
				if ( !cm.toString(s01KmkListBean.getStrHaitenKmk()).equals("") ) {
					workCell.setCellValue( Integer.parseInt( s01KmkListBean.getStrHaitenKmk() ) );
				}
				// 平均点・現役セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 2 );
//				if ( s01KmkListBean.getFloHeikinGen() != -999.0 ) {
//					workCell.setCellValue( s01KmkListBean.getFloHeikinGen() );
				if ( s01KmkListBean.getFloHeikinAll() != -999.0 ) {
					workCell.setCellValue( s01KmkListBean.getFloHeikinAll() );
				}
				// 標準偏差セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 3 );
				if ( s01KmkListBean.getFloStdHensa() != -999.0 ) {
					workCell.setCellValue( s01KmkListBean.getFloStdHensa() );
				}
				// 平均偏差値・現役セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 4 );
//				if ( s01KmkListBean.getFloHensaGen() != -999.0 ) {
//					workCell.setCellValue( s01KmkListBean.getFloHensaGen() );
				if ( s01KmkListBean.getFloHensaAll() != -999.0 ) {
					workCell.setCellValue( s01KmkListBean.getFloHensaAll() );
				}
				// 最高点
				workCell = cm.setCell( workSheet, workRow, workCell, row, 5 );
				if ( s01KmkListBean.getIntMaxTen() != -999 ) {
					workCell.setCellValue( s01KmkListBean.getIntMaxTen() );
				}
				// 最低点
				workCell = cm.setCell( workSheet, workRow, workCell, row, 6 );
				if ( s01KmkListBean.getIntMinTen() != -999 ) {
					workCell.setCellValue( s01KmkListBean.getIntMinTen() );
				}
				// 人数・現役セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 7 );
//				if ( s01KmkListBean.getIntNinzuGen() != -999 ) {
//					workCell.setCellValue( s01KmkListBean.getIntNinzuGen() );
				if ( s01KmkListBean.getIntNinzuAll() != -999 ) {
					workCell.setCellValue( s01KmkListBean.getIntNinzuAll() );
				}
				row++;
				kmkCnt++;
				log.It("S01_02","科目カウント",String.valueOf(kmkCnt));
				if (kmkCnt >= 40) {
					kmkCnt = 0;
				}
			}
			log.Ep("S01_02","科目データセット終了","");
//add 2004/10/25 T.Sakai データ0件対応
			if ( s01KataList.size()==0 && s01KmkList.size()==0 ) {
				// シートテンプレートのコピー
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex++;
				row = 9;
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.toString(s01Item.getStrMshmei()) + "　成績表");
				
				// 受験者数セット
				workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
				workCell.setCellValue( "受験者総数：" + nf.format(s01Item.getIntAllNinzu())+ "名");
			}
//add end
			// Excelファイル保存
			boolean bolRet = false;
			bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch( Exception e ) {
			log.Err("S01_02","データセットエラー",e.toString());
			return errfdata;
		}
		
		return noerror;
	}

}

package jp.co.fj.keinavi.forms;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * {@link jp.co.fj.keinavi.servlets.MethodInvokerServlet}を利用する画面のアクションフォームです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public abstract class MethodInvokerForm extends BaseForm {

	/** serialVersionUID */
	private static final long serialVersionUID = -7466138218743213240L;

	/** メソッド名 */
	private String operation;

	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public final void validate() {

		/* validate + メソッド名を起動する */
		Method target = getMethod("validate"
				+ StringUtils.capitalize(StringUtils.defaultString(operation,
						"index")));

		if (target == null) {
			return;
		}

		try {
			target.invoke(this, null);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 指定した名前と一致するメソッドを返します。
	 */
	private Method getMethod(String name) {

		Method[] methods = getClass().getMethods();
		for (int i = 0; i < methods.length; i++) {
			Method method = methods[i];
			if (method.getName().equals(name)
					&& Modifier.isPublic(method.getModifiers())) {
				return method;
			}
		}

		return null;
	}

	/**
	 * メソッド名をセットします。
	 * 
	 * @param operation
	 *            メソッド名
	 */
	public void setOperation(String method) {
		this.operation = method;
	}

}

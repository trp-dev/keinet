/*
 * 作成日: 2004/08/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.com_set;

import java.util.List;

import com.fjh.beans.DefaultBean;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
abstract public class AbstractComBean extends DefaultBean {

	/**
	 * 全てのデータ
	 * @return
	 */
	abstract public List getFullList();

	/**
	 * 部分的なデータ
	 * （受験生が１人以上など）
	 * @return
	 */
	abstract public List getPartList();

	/**
	 * 全てのデータの要素数
	 * @return
	 */
	abstract public int getFullListSize();
	
	/**
	 * 部分的なデータの要素数
	 * @return
	 */
	abstract public int getPartListSize();

	/**
	 * 全てのデータの要素数の半値
	 * @return
	 */
	public int getFullListHalfSize() {
		return Math.round((float)getFullListSize() / 2);
	}

	/**
	 * 部分的なデータの要素数の半値
	 * @return
	 */
	public int getPartListHalfSize() {
		return Math.round((float)getPartListSize() / 2);
	}
}

package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * 校内成績分析−他校比較−成績概況データクラス
 * 作成日: 2004/07/16
 * @author	A.Iwata
 */
public class S41Item {
	//学校名
	private String strGakkomei = "";
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//表フラグ
	private int intHyouFlg = 0;
	//グラフフラグ
	private int intGraphFlg = 0;
	//セキュリティスタンプフラグ
	private int intSecuFlg = 0;
	//データリスト
	private ArrayList s41List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public int getIntHyouFlg() {
		return this.intHyouFlg;
	}
	public int getIntGraphFlg() {
		return this.intGraphFlg;
	}
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public ArrayList getS41List() {
		return this.s41List;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setIntHyouFlg(int intHyouFlg) {
		this.intHyouFlg = intHyouFlg;
	}
	public void setIntGraphFlg(int intGraphFlg) {
		this.intGraphFlg = intGraphFlg;
	}
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setS41List(ArrayList s41List) {
		this.s41List = s41List;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}

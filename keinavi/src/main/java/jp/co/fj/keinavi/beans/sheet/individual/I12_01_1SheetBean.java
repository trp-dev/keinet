package jp.co.fj.keinavi.beans.sheet.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.IExamData;
import jp.co.fj.keinavi.data.sheet.ExtI11ListBean;
import jp.co.fj.keinavi.excel.data.personal.I11ChartListBean;
import jp.co.fj.keinavi.excel.data.personal.I11KyokaHensaListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 個人成績分析−成績分析面談 バランスチャートデータリスト印刷用Bean
 *
 * 200X.XX.XX [新規作成]
 *
 *
 * @author Keisuke KONDO - TOTEC
 * @version 1.0
 *
 * [1]    2005.02.02 kondo      change 外部データ開放日が同一の場合、模試コード順にソートするよう修正
 * [2]    2005.01.21 kondo      change ２次 英語＋リスニング追加対応
 * [3]    2005.01.21 kondo      change 配点200点の科目が複数ある場合最初の1つをとるように修正
 * [4]    2005.04.14 kondo      対象模試が新テストだった場合、種別フラグに"1"を入れるように修正
 * [5]    2006.09.06 Totec)T.Yamada     受験学力測定対応 - 省く処理を付加
 *
 * 2009.11.25   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 *
 */
public class I12_01_1SheetBean extends IndAbstractSheetBean {

    private final Short menSheetFlg;

    /**
     * コンストラクタです。
     * @param menSheetFlg
     */
    public I12_01_1SheetBean(Short menSheetFlg) {
        super();
        this.menSheetFlg = menSheetFlg;
    }
    /**
     * コンストラクタです。
     */
    public I12_01_1SheetBean() {
        super();
        this.menSheetFlg = null;
    }

    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws SQLException, Exception {

        final String ELSUBCD = "1190";//[2] add
        // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD START
        // 面談シートフラグ
        String flag = "0";
        // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD END

        //セッション情報
        ICommonMap commonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);
        int savaMultiExam;

        //プロファイル関連
        int MultiExam; //１教科複数受験
        if(!"i003".equals(backward)){
            /**i103バランスチャートの場合**/

            //セキュリティスタンプ
            Short printStamp = (Short) profile.getItemMap(IProfileCategory.I_SCORE_BC).get(IProfileItem.PRINT_STAMP);
            int SecuStanp = Integer.parseInt(printStamp.toString());

            data.setIntKyoSheetFlg(1) ;															// 教科分析フラグ
            data.setIntKyoSheetSecuFlg(SecuStanp);												// 教科シートセキュリティスタンプ
            data.setIntKyoSheetShubetsuFlg(getNewTestFlg());//[4] add 新テストフラグ（教科シート）

            //１教科複数受験
            Short indMultiExam = (Short)profile.getItemMap(IProfileCategory.I_SCORE_BC).get(IProfileItem.IND_MULTI_EXAM);
            savaMultiExam = Integer.parseInt(indMultiExam.toString());

            // 2020/02/14 QQ)Ooseto 共通テスト対応 ADD START
            flag = "1";
            // 2020/02/14 QQ)Ooseto 共通テスト対応 ADD END

        }else{
            /**i003簡単印刷の場合**/
            //教科分析シートフラグ
            String []KyoSheetFlg = CollectionUtil.splitComma((String) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_FORMS));

            //面談シートフラグ
            Short MenSheet = menSheetFlg != null ? menSheetFlg : (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_SHEET);
            int MenSheetFlg = Integer.parseInt(MenSheet.toString());
            // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD START
            flag = MenSheet.toString();
            // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD END

            //セキュリティスタンプ
            Short printStamp = (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.PRINT_STAMP);
            int SecuStanp = Integer.parseInt(printStamp.toString());

            //教科分析シートが選択されたか、面談シートが選択されたのか
            if(Integer.parseInt(KyoSheetFlg[0]) == 1){
                data.setIntKyoSheetFlg(Integer.parseInt(KyoSheetFlg[0])) ;							// 教科分析フラグ
                data.setIntKyoSheetSecuFlg(SecuStanp);												// 教科分析シートセキュリティスタンプ
                data.setIntKyoSheetShubetsuFlg(getNewTestFlg());//[4] add 新テストフラグ（教科シート）
            }
            if((MenSheetFlg == 2 || MenSheetFlg == 4)){
                data.setIntMenSheetFlg(MenSheetFlg);												// 面談シートフラグ
                data.setIntMenSheetSecuFlg(SecuStanp);												// 面談シートフラグセキュリティスタンプ
                data.setIntMenSheetShubetsuFlg(getNewTestFlg());//[4] add 新テストフラグ（面談シート）
            }
            //教科分析シートは受験学力測定テストNG
            if (Integer.parseInt(KyoSheetFlg[0]) == 1
                    && KNUtil.TYPECD_ABILITY.equals(commonMap.getTargetExamTypeCode())) {
                data.setIntKyoSheetFlg(0);
            }

            //１教科複数受験→第1解答科目で固定
            savaMultiExam = 3;
        }

        /*下記条件の時に値を格納
        ・面談シートフラグ=2or4
        ・教科分析シートフラグ=1*/

        if (data.getIntMenSheetFlg() == 2 || data.getIntMenSheetFlg() == 4 || data.getIntKyoSheetFlg() == 1) {

            /*******項番２SQL＿選択生徒情報取得***********************************************************/

            PreparedStatement pstmt1 = null;
            ResultSet rs1 = null;
            PreparedStatement pstmt2 = null;
            ResultSet rs2 = null;
            PreparedStatement pstmt3 = null;
            ResultSet rs3 = null;
            PreparedStatement pstmt4 = null;
            ResultSet rs4 = null;
            PreparedStatement pstmt5 = null;
            ResultSet rs5 = null;
            PreparedStatement pstmt6 = null;
            ResultSet rs6 = null;

            PreparedStatement pstmt8 = null;
            ResultSet rs8 = null;
            PreparedStatement pstmt9 = null;
            ResultSet rs9 = null;


            for (java.util.Iterator it=data.getI11List().iterator(); it.hasNext();) {
                ExtI11ListBean printData = (ExtI11ListBean) it.next();

                // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD START
                // dataが処理中の帳票のデータでない場合(面談シートフラグが異なる場合)、処理しない
                if(!printData.getMenSheetFlg().equals(flag.toString())) continue;
                // 2020/02/07 QQ)Ooseto 共通テスト対応 ADD END

                try{

                    final List allows = new ArrayList();
                    String[] examCd = new String[3];

                    //対象模試が受験学力測定なら対象から省く
                    if (!KNUtil.TYPECD_ABILITY.equals(commonMap.getExamData().getExamTypeCD())) {
                        allows.add(commonMap.getExamData().getExamCD());
                    }

                    //比較対象模試が選択されていないので、初期化する（= かんたん印刷・生徒切替）
                    if(!"i103".equals(backward)
                            || !commonMap.getTargetPersonId().equals(printData.getIndividualid())){
                        List examlist = new ArrayList();
                        examlist = getSelectives(printData.getIndividualid(), commonMap);
                        allows.addAll(examlist);

                        for (int i=0; i<examlist.size(); i++) {
                            examCd[i] = (String)examlist.get(i);
                        }
                    } else {
                        allows.add(targetExamCode1);
                        allows.add(targetExamCode2);
                        allows.add(targetExamCode3);

                        examCd[0] = targetExamCode1;
                        examCd[1] = targetExamCode2;
                        examCd[2] = targetExamCode3;
                    }

                    //対象も比較もない
                    if (allows.size() == 0) {
                        continue;
                    }

                    //１教科複数受験
                    // 第1解答科目を表示で、対象模試と比較対象模試が第1解答科目対応模試以外→ 平均を表示
                    if(!(KNUtil.isAns1st(commonMap.getTargetExamCode()) || KNUtil.isAns1st(examCd[0]) || KNUtil.isAns1st(examCd[1]) || KNUtil.isAns1st(examCd[2]))
                        && savaMultiExam == 3){
                        MultiExam = 1;
                    }else {
                        MultiExam = savaMultiExam;
                    }


                    final Query query = QueryLoader.getInstance().load("i12_select_exam");

                    query.setStringArray(1, (String[]) allows.toArray(new String[allows.size()]));

                    if (login.isHelpDesk()) {
                        query.replaceAll("out_dataopendate", "in_dataopendate");
                    }

                    pstmt1 = conn.prepareStatement(query.toString());

                    pstmt1.setString(1, printData.getIndividualid());
                    pstmt1.setString(2, commonMap.getExamData().getExamYear());

                    rs1 = pstmt1.executeQuery();

                    while(rs1.next()){

                        I11ChartListBean bean = new I11ChartListBean();
                        printData.getI11ChartList().add(bean);

                        bean.setStrMshmei(rs1.getString(3));			//模試短縮名
                        bean.setStrMshDate(rs1.getString(4));			//模試実施基準日
                        bean.setIntKyokaDispFlg(MultiExam);				//1教科複数受験フラグ

                        /*******項番９SQL＿科目偏差値の取得********************************************************************/

                        ArrayList KyokaData = new ArrayList();
                        //その生徒が受けた模試の偏差値を全て出す。
                        boolean eng_count = false;
                        boolean math_count = false;	//数学の検索条件
                        boolean japan_count = false;	//数学の検索条件

                        /**英語の偏差値取得(科目配点が200の物)*****************************************************************/
                        pstmt2 = conn.prepareStatement(QueryLoader.getInstance().load("i12_english_score1").toString());
                        pstmt2.setString(1, printData.getIndividualid());	//項番2で出たID
                        pstmt2.setString(2, rs1.getString(1));			    //項番8で出た年度
                        pstmt2.setString(3, rs1.getString(2));				//項番8で出た模試コード
                        pstmt2.setString(4, rs1.getString(1));			    //項番8で出た年度
                        rs2 = pstmt2.executeQuery();

                        I11KyokaHensaListBean Hensa = new I11KyokaHensaListBean();
                        while(rs2.next()){
                            //[2] add start
                            //偏差値が-999だったら追加しない
                            if(rs2.getFloat(2) != -999.0){
                                //英語＋リスニングなら優先して、入れる
                                if(rs2.getString("SUBCD").equals(ELSUBCD)) {
                                    Hensa.setStrKyokamei(rs2.getString(1));	//教科名
                                    Hensa.setFloHensa(rs2.getFloat(2));		//偏差値
                                    choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                            rs2.getFloat(2), rs2.getString("ACADEMICLEVEL"));
                                } else if(!eng_count) {
                                    //それ以外は、最初の一つを入れる
                                    Hensa.setStrKyokamei(rs2.getString(1));	//教科名
                                    Hensa.setFloHensa(rs2.getFloat(2));		//偏差値
                                    choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                            rs2.getFloat(2), rs2.getString("ACADEMICLEVEL"));
                                }
                                eng_count = true;//確認フラグ
                            }
                        }

                        //[1] add 確認フラグがオンなら、追加
                        if(eng_count){
                            KyokaData.add(Hensa);
                        }

                        //教科名・偏差値リスト
                        if(pstmt2 != null) pstmt2.close();
                        if(rs2 != null) rs2.close();

                        /**英語の偏差値取得***********************************************************************/
                        if(eng_count == false){
                            pstmt8 = conn.prepareStatement(QueryLoader.getInstance().load("i12_english_score2").toString());
                            pstmt8.setString(1, printData.getIndividualid());	//項番2で出たID
                            pstmt8.setString(2, rs1.getString(1));			    //項番8で出た年度
                            pstmt8.setString(3, rs1.getString(2));				//項番8で出た模試コード
                            pstmt8.setString(4, rs1.getString(1));			    //項番8で出た年度
                            rs8 = pstmt8.executeQuery();

                            while(rs8.next()){
                                Hensa.setStrKyokamei(rs8.getString(1));	//教科名
                                Hensa.setFloHensa(rs8.getFloat(2));		//偏差値
                                choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                        rs8.getFloat(2), rs8.getString("ACADEMICLEVEL"));
                                break;									//表示順序の一番目
                            }
                            KyokaData.add(Hensa);
                            //教科名・偏差値リスト
                            if(pstmt8 != null) pstmt8.close();
                            if(rs8 != null) rs8.close();
                        }

                        /**数学の偏差値取得(科目配点が200の物)***********************************************************************/
                        pstmt3 = conn.prepareStatement(QueryLoader.getInstance().load("i12_math_score1").toString());
                        pstmt3.setString(1, printData.getIndividualid());				//項番2で出たID
                        pstmt3.setString(2, rs1.getString(1));							//項番8で出た年度
                        pstmt3.setString(3, rs1.getString(2));							//項番8で出た模試コード
                        pstmt3.setString(4, rs1.getString(1));							//項番8で出た年度

                        rs3 = pstmt3.executeQuery();

                        Hensa = new I11KyokaHensaListBean();
                        while(rs3.next()){
                            //[3] add start
                            //偏差値が-999だったら追加しない
                            if(rs3.getFloat(2) != -999.0){
                                if(math_count == false) {
                                    Hensa.setStrKyokamei(rs3.getString(1));	//教科名
                                    Hensa.setFloHensa(rs3.getFloat(2));		//偏差値
                                    choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                            rs3.getFloat(2), rs3.getString("ACADEMICLEVEL"));
                                    KyokaData.add(Hensa);
                                }
                                math_count = true;		//確認フラグ
                            }
                        }
                        if(pstmt3 != null) pstmt3.close();
                        if(rs3 != null) rs3.close();

                        /**数学の偏差値取得(↑の条件で一軒もヒットしなかったらこっち)********************************************/
                        if(math_count == false){
                            pstmt4 = conn.prepareStatement(QueryLoader.getInstance().load("i12_math_score2").toString());
                            pstmt4.setString(1, printData.getIndividualid());	//項番2で出たID
                            pstmt4.setString(2, rs1.getString(1));				//項番8で出た年度
                            pstmt4.setString(3, rs1.getString(2));				//項番8で出た模試コード
                            pstmt4.setString(4, rs1.getString(1));				//項番8で出た年度
                            rs4 = pstmt4.executeQuery();

                            while(rs4.next()){
                                Hensa.setStrKyokamei(rs4.getString(1));	//教科名
                                Hensa.setFloHensa(rs4.getFloat(2));		//偏差値
                                choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                        rs4.getFloat(2), rs4.getString("ACADEMICLEVEL"));
                                break;									//表示順序の一番目
                            }
                            //教科名・偏差値リスト
                            KyokaData.add(Hensa);
                            if(pstmt4 != null) pstmt4.close();
                            if(rs4 != null) rs4.close();
                        }

                        /**国語の偏差値取得(科目配点が200の物)*******************************************************************/
                        pstmt8 = conn.prepareStatement(QueryLoader.getInstance().load("i12_japanLang_score1").toString());
                        pstmt8.setString(1, printData.getIndividualid());	//項番2で出たID
                        pstmt8.setString(2, rs1.getString(1));				//項番8で出た年度
                        pstmt8.setString(3, rs1.getString(2));				//項番8で出た模試コード
                        pstmt8.setString(4, rs1.getString(1));				//項番8で出た年度
                        rs8 = pstmt8.executeQuery();

                        Hensa = new I11KyokaHensaListBean();
                        while(rs8.next()){
                            //[3] add start
                            //教科名・偏差値リスト
                            //偏差値が-999だったら追加しない
                            if(rs8.getFloat(2) != -999.0){
                                if(japan_count == false) {
                                    Hensa.setStrKyokamei(rs8.getString(1));	//教科名
                                    Hensa.setFloHensa(rs8.getFloat(2));		//偏差値
                                    choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                            rs8.getFloat(2), rs8.getString("ACADEMICLEVEL"));
                                    KyokaData.add(Hensa);
                                }
                                japan_count = true;		//確認フラグ
                            }
                        }
                        if(pstmt8 != null) pstmt8.close();
                        if(rs8 != null) rs8.close();

                        /**国語の偏差値取得(↑の条件で一軒もヒットしなかったらこっち)****************************************/
                        if(japan_count == false){
                            pstmt5 = conn.prepareStatement(QueryLoader.getInstance().load("i12_japanLang_score2").toString());
                            pstmt5.setString(1, printData.getIndividualid());	//項番2で出たID
                            pstmt5.setString(2, rs1.getString(1));				//項番8で出た年度
                            pstmt5.setString(3, rs1.getString(2));				//項番8で出た模試コード
                            pstmt5.setString(4, rs1.getString(1));				//項番8で出た年度
                            rs5 = pstmt5.executeQuery();

                            while(rs5.next()){

                                Hensa.setStrKyokamei(rs5.getString(1));	//教科名
                                Hensa.setFloHensa(rs5.getFloat(2));		//偏差値
                                choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(),
                                        rs5.getFloat(2), rs5.getString("ACADEMICLEVEL"));
                                break;									//表示順序の一番目
                            }
                            KyokaData.add(Hensa);
                            if(pstmt5 != null) pstmt5.close();
                            if(rs5 != null) rs5.close();
                        }

                        /**理科・地歴公民の偏差値取得*******************************************************************************/
                        //1教科複数受験　平均1or良い方2or第1解答科目3//プロファイルから1教科複数受験取得

                        if(bean.getIntKyokaDispFlg() == 3){
                            if(KNUtil.isAns1st(rs1.getString(2))){
                                //第1解答科目対応模試
                                pstmt6 = conn.prepareStatement(QueryLoader.getInstance().load("i12_sm_ans1st_score").toString());
                                pstmt6.setString(1, printData.getIndividualid());	//項番2で出たID
                                pstmt6.setString(2, rs1.getString(1));				//項番8で出た年度
                                pstmt6.setString(3, rs1.getString(2));				//項番8で出た模試コード
                                pstmt6.setString(4, printData.getIndividualid());	//項番2で出たID
                                pstmt6.setString(5, rs1.getString(1));				//項番8で出た年度
                                pstmt6.setString(6, rs1.getString(2));				//項番8で出た模試コード
                                pstmt6.setString(7, rs1.getString(1));				//項番8で出た年度
                            }else{
                                //平均
                                pstmt6 = conn.prepareStatement(QueryLoader.getInstance().load("i12_sm_avg_score").toString());
                                pstmt6.setString(1, printData.getIndividualid());	//項番2で出たID
                                pstmt6.setString(2, rs1.getString(1));				//項番8で出た年度
                                pstmt6.setString(3, rs1.getString(2));				//項番8で出た模試コード
                                pstmt6.setString(4, rs1.getString(1));				//項番8で出た年度
                            }
                        }else if(bean.getIntKyokaDispFlg() == 1){
                            //平均ならこのSQL
                            pstmt6 = conn.prepareStatement(QueryLoader.getInstance().load("i12_sm_avg_score").toString());
                            pstmt6.setString(1, printData.getIndividualid());	//項番2で出たID
                            pstmt6.setString(2, rs1.getString(1));				//項番8で出た年度
                            pstmt6.setString(3, rs1.getString(2));				//項番8で出た模試コード
                            pstmt6.setString(4, rs1.getString(1));				//項番8で出た年度
                        }else{
                            //良い方ならこのSQL
                            pstmt6 = conn.prepareStatement(QueryLoader.getInstance().load("i12_sm_max_score").toString());
                            pstmt6.setString(1, printData.getIndividualid());	//項番2で出たID
                            pstmt6.setString(2, rs1.getString(1));				//項番8で出た年度
                            pstmt6.setString(3, rs1.getString(2));				//項番8で出た模試コード
                            pstmt6.setString(4, rs1.getString(1));				//項番8で出た年度
                        }

                        rs6 = pstmt6.executeQuery();

                        //理科で一番点数の良い科目コードをだす。
                        String s_subname = "";
                        float s_subscor = -999;
                        String s_sublevel = "";
                        int s_ans1st=0;
                        //地歴公民で一番点数の良い科目コードをだす。
                        String t_subname = "";
                        float t_subscor = -999;
                        String t_sublevel = "";
                        int t_ans1st=0;

                        while(rs6.next()){
                            if ("4000".equals(rs6.getString(1))){
                                if (s_subscor < rs6.getFloat(4)){
                                    s_subname = rs6.getString(2);
                                    s_subscor = rs6.getFloat(4);
                                    s_sublevel = rs6.getString(5);
                                    s_ans1st = rs6.getInt(6);
                                }
                            }else{
                                if (t_subscor < rs6.getFloat(4)){
                                    t_subname = rs6.getString(2);
                                    t_subscor = rs6.getFloat(4);
                                    t_sublevel = rs6.getString(5);
                                    t_ans1st = rs6.getInt(6);
                                }
                            }
                        }

                        // 理科
                        Hensa = new I11KyokaHensaListBean();
                        Hensa.setStrKyokamei(s_subname);	//教科名
                        Hensa.setFloHensa(s_subscor);		//偏差値
                        //学力レベル
                        if (bean.getIntKyokaDispFlg() == 3){
                            if (s_ans1st == 1){
                                choiceScholarLevel(Hensa, 2, s_subscor, s_sublevel);	//第1解答科目
                            }else{
                                choiceScholarLevel(Hensa, 1, s_subscor, s_sublevel);	//平均
                            }
                        }else{
                            choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(), s_subscor, s_sublevel);	//平均or良い方
                        }
                        KyokaData.add(Hensa);

                        //地歴公民
                        Hensa = new I11KyokaHensaListBean();
                        Hensa.setStrKyokamei(t_subname);	//教科名
                        Hensa.setFloHensa(t_subscor);		//偏差値
                        //学力レベル
                        if (bean.getIntKyokaDispFlg() == 3){
                            if (t_ans1st == 1){
                                choiceScholarLevel(Hensa, 2, t_subscor, t_sublevel);	//第1解答科目
                            }else{
                                choiceScholarLevel(Hensa, 1, t_subscor, t_sublevel);	//平均
                            }
                        }else{
                            choiceScholarLevel(Hensa, bean.getIntKyokaDispFlg(), t_subscor, t_sublevel);	//平均or良い方
                        }
                        KyokaData.add(Hensa);

                        if(pstmt6 != null) pstmt6.close();
                        if(rs6 != null) rs6.close();

                        bean.setI11KyokaHensaList(KyokaData);

                    }//模試コード

                }finally{
                    if(pstmt1 != null) pstmt1.close();
                    if(rs1 != null) rs1.close();
                    if(pstmt2 != null) pstmt2.close();
                    if(rs2 != null) rs2.close();
                    if(pstmt3 != null) pstmt3.close();
                    if(rs3 != null) rs3.close();
                    if(pstmt4 != null) pstmt4.close();
                    if(rs4 != null) rs4.close();
                    if(pstmt5 != null) pstmt5.close();
                    if(rs5 != null) rs5.close();
                    if(pstmt6 != null) pstmt6.close();
                    if(rs6 != null) rs6.close();

                    if(pstmt8 != null) pstmt8.close();
                    if(rs8 != null) rs8.close();
                    if(pstmt9 != null) pstmt9.close();
                    if(rs9 != null) rs9.close();
                }

            }//個人情報

        }//flag条件

    }

    /**
     * 画面の選択状態（平均値／良い方）で、出力する学力レベルを選択する。
     * @param hensa
     * @param flg
     * @param avg
     * @param lvl
     */
    private void choiceScholarLevel(I11KyokaHensaListBean hensa,
            final int flg, final float avg, final String lvl) {

        // 	平均 or 第1解等科目
        //  第1解等科目で理科・地歴公民以外の場合は平均とする
        if (flg == 1 || flg == 3) {
            //平均値の場合、偏差値から学力レベルを出力
            hensa.setStrScholarLevel(IExamData.makeScholarLevel(""+avg));
        }
        else {
            //良い方の場合、SQLから取得した学力レベルを出力
            hensa.setStrScholarLevel(GeneralUtil.toBlank(lvl));
        }

    }

    /* (非 Javadoc)
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
     */
    protected String getCategory() {
        return IProfileCategory.I_SCORE_BC;
    }

    private List getSelectives(
            final String individualId,
            final ICommonMap commonMap) throws SQLException {

        final Query query =
            QueryLoader.getInstance().load("i12_examconbo");

        String option= "";

        if (login.isHelpDesk()) {
            option =
            " AND EXAMINATION.IN_DATAOPENDATE <= ( SELECT EXAMINATION.IN_DATAOPENDATE FROM EXAMINATION WHERE EXAMINATION.EXAMYEAR = '"+commonMap.getTargetExamYear()+"' AND EXAMINATION.EXAMCD = '"+commonMap.getTargetExamCode()+"' ) "+
            "GROUP BY SUBRECORD_I.INDIVIDUALID,SUBRECORD_I.EXAMYEAR,SUBRECORD_I.EXAMCD,EXAMINATION.EXAMNAME,SUBRECORD_I.EXAMYEAR,EXAMINATION.IN_DATAOPENDATE "+
            " ,examination.examtypecd " +
            "ORDER BY EXAMINATION.IN_DATAOPENDATE DESC, SUBRECORD_I.EXAMCD DESC";//[1] add
        } else {
            option =
            " AND EXAMINATION.OUT_DATAOPENDATE <= ( SELECT EXAMINATION.OUT_DATAOPENDATE FROM EXAMINATION WHERE EXAMINATION.EXAMYEAR = '"+commonMap.getTargetExamYear()+"' AND EXAMINATION.EXAMCD = '"+commonMap.getTargetExamCode()+"' ) "+
            "GROUP BY SUBRECORD_I.INDIVIDUALID,SUBRECORD_I.EXAMYEAR,SUBRECORD_I.EXAMCD,EXAMINATION.EXAMNAME,SUBRECORD_I.EXAMYEAR,EXAMINATION.OUT_DATAOPENDATE "+
            " ,examination.examtypecd " +
            "ORDER BY EXAMINATION.OUT_DATAOPENDATE DESC, SUBRECORD_I.EXAMCD DESC";//[1] add
        }

        query.append(option);

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            ps = conn.prepareStatement(query.toString());

            ps.setString(1, individualId);
            ps.setString(2, commonMap.getTargetExamYear());
            ps.setString(3, commonMap.getTargetExamCode());

            rs = ps.executeQuery();

            final List list = new ArrayList();

            while (rs.next()) {

                //対象模試から受験学力測定テストを省く
                if (KNUtil.TYPECD_ABILITY.equals(rs.getString("examtypecd"))) {
                    continue;
                }

                //最大3件に限定
                if (list.size() == 3) {
                    break;
                }

                list.add(rs.getString("EXAMCD"));
            }

            return list;

        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }
}

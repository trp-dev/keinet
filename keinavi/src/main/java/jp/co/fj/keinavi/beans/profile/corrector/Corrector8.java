package jp.co.fj.keinavi.beans.profile.corrector;

import jp.co.fj.keinavi.beans.txt_out.TextInfoBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;

/**
 * プロファイルコレクタ８
 *
 * <新過程対応>
 * 2013.04.23   Yuuichirou Morita - TOTEC
 *              新規作成
 *
 * @author Yuuichirou Morita - TOTEC
 * @version
 */
public class Corrector8 implements ICorrector {

    public void execute(final Profile profile, final LoginSession login) {

        // 営業部以外の場合にのみ
        // テキスト出力（個人データ−模試別成績データ）を初期化する
        if (!login.isBusiness()) {
            StringBuffer selection = new StringBuffer((String)profile.getItemMap(T_IND_EXAM2).get(TEXT_OUT_ITEM));
            for (int i = 0; i < TextInfoBean.SELECTION_add.length; i ++) {
                selection.append(",");
                selection.append(TextInfoBean.SELECTION_add[i]);
            }
            profile.getItemMap(T_IND_EXAM2).put(TEXT_OUT_ITEM, selection.toString());
        }
    }

    /* (非 Javadoc)
     * @see jp.co.fj.keinavi.beans.profile.corrector.ICorrector#getVersion()
     */
    public short getVersion() {
        return 8;
    }

}
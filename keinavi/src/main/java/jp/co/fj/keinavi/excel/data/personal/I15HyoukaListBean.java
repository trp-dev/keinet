package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
//import java.util.ArrayList;
/**
 * �󌱗p�����|�󌱑�w�X�P�W���[���\ ���i�\�����胊�X�g
 * �쐬��: 2004/07/20
 * @author	A.Iwata
 */
public class I15HyoukaListBean {
	//�]���敪
	private String strHyoukaCenter = "";
	//�͎���
	private String strMshmei = "";
	//�͎����{���
	private String strMshDate = "";
	//�]��
	private String strHyouka = "";
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrHyoukaCenter() {
		return this.strHyoukaCenter;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public String getStrHyouka() {
		return this.strHyouka;
	}
		
	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrHyoukaCenter(String strHyoukaCenter) {
		this.strHyoukaCenter = strHyoukaCenter;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setStrHyouka(String strHyouka) {
		this.strHyouka = strHyouka;
	}
}

package jp.co.fj.keinavi.util;

import java.util.*;
import java.io.*;

/**
 * プロパティファイルから値を取得する。
 *
 * @Date	2005/06/17
 * @author	TOTEC)Nishiyama
 */
public class KNPropertyCtrl {

	/**
   * 配列の区切り文字
   */
	private static final char CONNECT_CHAR = ',';

  /**
   * commonファイルの格納パス
   */
	// サーブレット起動時に初期化するように修正
	// Yoshimoto KAWAI - Totec
	// getPath()もコメントアウト済み
	//public static String FilePath = getPath();
	public static String FilePath;

  /**
   * ロード済みのプロパティキャッシュ
   * キー：　FilePath(String)
   * 値：　　PropertyCacheクラス
   */
  private static Map cache = new HashMap(32);

	/**
	 * PropertyCacheクラスを作成する。
	 * 
	 * @return PropertyCacheクラス
	 */
  private PropertyCache createPropertyCache() { return new PropertyCache(); }

  
//  /**
//   * 共通プロパティのルートパスを取得する。
//   * 
//   * @throws Exception
//   */
//	private static String getPath() {
//    try {
//      String path = System.getProperty("user.dir");
//
//      Properties prop = new Properties();
//      prop.load(new FileInputStream(new File(path + "/KNIni.properties")));
//      return prop.getProperty("FilePath");
//    } catch(Exception e) {
//      e.printStackTrace();
//    }
//    return "";
//	}

	/**
	 * 値をStringで返す
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @return 取得値
	 */
	public static synchronized String GetString(String FilePath, 
																								String ParamKey) throws Exception{
    return _GetStringValue(FilePath, ParamKey);
	}

	/**
	 * 値をIntで返す
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @return 取得値
	 */
	public static synchronized int GetInt(String FilePath,
																						String ParamKey) throws Exception{
		int intRtnVal = 0;

 		String strRtnVal = _GetStringValue(FilePath, ParamKey);
		if (strRtnVal == null) {
			intRtnVal = 0;
		} else {
			intRtnVal = Integer.valueOf(strRtnVal).intValue();
		}

		return intRtnVal;
	}


	/**
	 * 値をString配列で返す
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @return 取得値
	 */
	public static synchronized String[] GetStringList(String FilePath,
																											String ParamKey) throws Exception{
		String[]	strRtnVal = null;

    String sRtnVal = _GetStringValue(FilePath, ParamKey);
    if(sRtnVal == null){
      strRtnVal = new String[0];
    }else{
      strRtnVal = _ToStringArray(sRtnVal);
    }

		return strRtnVal;
	}

	/**
	 * 値をint配列で返す
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @return 取得値
	 */
	public static synchronized int[] GetIntList(String FilePath,
																									String ParamKey) throws Exception{
		Vector	list	= new Vector();
		int[]	intRtnVal	= null;

    String sRtnVal = _GetStringValue(FilePath, ParamKey);
    if(sRtnVal == null){
      intRtnVal = new int[0];
    }else{
      intRtnVal = _ToIntArray(sRtnVal);
    }

		return intRtnVal;
	}

	/**
	 * Stringの値を設定する
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @param Parameter	値
	 */
	public static synchronized void SetString(String FilePath,
																								String ParamKey,
																								String sParameter) throws Exception{
    _SetStringValue(FilePath, ParamKey, sParameter);
	}
	
	/**
	 * Stringの値を設定する
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @param Parameter	値
	 */
	public static synchronized void SetStringList(String FilePath,
																										String ParamKey,
																										String sParameters[]) throws Exception{
		_SetStringValue(FilePath, ParamKey, _ArrayToString(sParameters));
	}

	/**
	 * intの値を設定する
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @param Parameter	値
	 */
	public static synchronized void SetInt(String FilePath, 
																						String ParamKey, 
																						int iParameter) throws Exception{
		String sParameter = null;
		sParameter = String.valueOf(iParameter);
    _SetStringValue(FilePath, ParamKey, sParameter);
	}
	
	/**
	 * intの値を設定する
	 *
	 * @param FilePath	ファイル名
	 * @param ParamKey	キー
	 * @param Parameter	値
	 */
	public static synchronized void SetIntList(String FilePath, 
																								String ParamKey, 
																								int iParameters[]) throws Exception{
		String sParameter = null;
		sParameter = _ArrayToString(iParameters);
		_SetStringValue(FilePath, ParamKey, sParameter);
	}

  /**
   * プロパティ値を取得する
   * 
   * @param FilePath
   * @param ParamKey
   * @return
   * @throws Exception
   */
  private static String _GetStringValue(String FilePath, 
  																				String ParamKey)throws Exception {
    Properties prop = _GetCache(FilePath);
          
    return prop.getProperty(ParamKey);
  }

  /**
   * プロパティ値を設定する
   * 
   * @param GroupName
   * @param ParamKey
   * @param sParameter
   * @throws Exception
   */
  private static void _SetStringValue(String FilePath, 
  																			String ParamKey, 
  																			String sParameter)throws Exception {
    Properties prop = _GetCache(FilePath);
    prop.setProperty(ParamKey, sParameter);
          
    _SetCache(FilePath, prop);
    prop.store(new FileOutputStream(FilePath + FilePath), "Stored by GetProperty");
  }

  /**
   * キャッシュしたプロパティ情報を取得する
   * 
   * @param FilePath
   * @return Propertiesクラス
   * @throws Exception
   */
  private static Properties _GetCache(String FilePath)throws Exception {

    PropertyCache data;
    if (!__IsCached(FilePath)) {
      data = __LoadProperty(FilePath);
    } else {
      data = (PropertyCache) cache.get(FilePath);
    }
    if (data.CheckTime()) {
      data = __LoadProperty(FilePath);
    }

    return data.prop;
  }
        
  /**
   * キャッシュしたプロパティ情報をセットする
   * 
   * @param FilePath
   * @param Prop
   * @throws Exception
   */
  private static void _SetCache(String FilePath, 
  																Properties Prop)throws Exception {
		KNPropertyCtrl me = new KNPropertyCtrl();
    PropertyCache data = me.createPropertyCache();
    data.prop = Prop;
    if(cache.containsKey(FilePath)) {
     	cache.remove(FilePath);
    }
    cache.put(FilePath, data);
  }

  /**
   * キャッシュにプロパティ情報があるかどうか判定する
   * 
   * @param FilePath
   * @return TRUE:FALSE
   */
  private static boolean __IsCached(String FilePath) {
    return cache.containsKey(FilePath);
  }

  /**
   * プロパティ情報をロードする
   * 
   * @param FilePath
   * @return PropertyCacheクラス
   * @throws Exception
   */
  private static PropertyCache __LoadProperty(String FilePath)throws Exception{
    Properties prop = new Properties();
    prop.load(new FileInputStream(FilePath));
		KNPropertyCtrl me = new KNPropertyCtrl();
    PropertyCache data = me.createPropertyCache();
    data.prop = prop;
    cache.put(FilePath, data);
    return data;
  }

  /**
   * 文字列をString配列に変換する
   * @param sRtnVal
   * @return String配列
   */
  private static String[] _ToStringArray(String sRtnVal) {
    Vector list	= new Vector();

    while (sRtnVal.length() > 0) {
      if (sRtnVal.indexOf(CONNECT_CHAR) != -1) {
        list.add(sRtnVal.substring(0, sRtnVal.indexOf(CONNECT_CHAR)));
        sRtnVal = sRtnVal.substring(sRtnVal.indexOf(CONNECT_CHAR) + 1);
      } else {
        list.add(sRtnVal);
        break;
      }
    }
    
    String[]	strRtnVal = new String[list.size()];
    for (int i = 0; i < list.size(); i++) {
      strRtnVal[i] = (String) list.get(i);
    }

    return strRtnVal;
  }
  
  /**
   * String配列を文字列に変換する。
   * 
   * @param str String配列
   * @return 文字列
   */
  private static String _ArrayToString(String Val[]) {
  	StringBuffer RtnVal = new StringBuffer();
  	
  	for(int i=0;i<Val.length;i++) {
  		if(i != Val.length) {
  			RtnVal.append(Val[i] + CONNECT_CHAR);
  		} else {
  			RtnVal.append(Val[i]);
  		}
  	}
  	return RtnVal.toString();
  }

  /**
   * 文字列をint配列に変換する
   * 
   * @param sRtnVal
   * @return int配列
   */
  private static int[] _ToIntArray(String sRtnVal) {
    Vector	list	= new Vector();
    int[]	intRtnVal	= null;

    while (sRtnVal.length() > 0) {
      if (sRtnVal.indexOf(CONNECT_CHAR) != -1) {
        list.add(sRtnVal.substring(0, sRtnVal.indexOf(CONNECT_CHAR)));
        sRtnVal = sRtnVal.substring(sRtnVal.indexOf(CONNECT_CHAR) + 1);
      } else {
        list.add(sRtnVal);
        break;
      }
    }

    intRtnVal = new int[list.size()];
    for (int j = 0; j < list.size(); j++) {
      if ( ( (String) list.get(j) == null) || "".equals( (String) list.get(j))) {
        intRtnVal[j] = 0;
      } else {
        intRtnVal[j] = Integer.valueOf( (String) list.get(j)).intValue();
      }
    }
    return intRtnVal;
  }
	
	/**
	 * int配列を文字列に変換する。
	 * 
	 * @param Val int配列
	 * @return 文字列
	 */
	private static String _ArrayToString(int Val[]) {
		StringBuffer RtnVal = new StringBuffer();
		
		for(int i=0;i<Val.length;i++) {
			if(i != Val.length) {
				RtnVal.append(String.valueOf(Val[i]) + CONNECT_CHAR);
			} else {
				RtnVal.append(String.valueOf(Val[i]));
			}
		}
		return RtnVal.toString();
	}
	
	/**
	 * プロパティキャッシュクラス
	 * 
	 * @Date	2005/06/17
	 * @author TOTEC)Nishiyama
	 */
	private class PropertyCache {
		/**
		 * プロパティクラスのインスタンス
		 */
		public Properties prop = null;
		
		/**
		 * 読み込まれた時間
		 */
		public long recentLoadTime = System.currentTimeMillis();
		
		/**
		 * キャッシュの保持時間
		 */
		public long cacheTerm = 300 * 1000;

		/**
		 * コンストラクタ
		 */
		public PropertyCache() {}
		public PropertyCache(long term) { cacheTerm = term; }

		/**
		 * キャッシュの寿命をチェックする
		 * @return TRUE:FALSE
		 * @throws Exception
		 */
		public boolean CheckTime() throws Exception{
			long time = System.currentTimeMillis();
			return time - recentLoadTime > cacheTerm ? true : false;
		}
	}
}

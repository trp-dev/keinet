package jp.co.fj.keinavi.servlets.jnlp;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;

import jp.co.fj.keinavi.servlets.DefaultLoginServlet;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * JNLPサーブレット
 * 
 * 2008.02.08	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class JnlpServlet extends DefaultLoginServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {
				
		final String codeBase = createCodeBase(request);
		
		request.setAttribute("CodeBase", codeBase);
		
		/* Cookieを設定する */
		StringBuffer sb = new StringBuffer();
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (i > 0) {
					sb.append("; ");
				}
				Cookie cookie = cookies[i];
				sb.append(URLEncoder.encode(cookie.getName(), "UTF-8"));
				sb.append("=");
				sb.append(URLEncoder.encode(cookie.getValue(), "UTF-8"));
			}
		}
		request.setAttribute("Cookie", StringEscapeUtils.escapeXml(sb.toString()));
		
		forward2Jsp(request, response);
	}
	
	// codebaseを生成する
	protected String createCodeBase(final HttpServletRequest request) {

		final StringBuffer url = createServerUrl(request.getScheme(),
				request.getServerName(), request.getServerPort());

		return url.append(KNUtil.getContextPath(request)).toString();
	}
	
	// context-pathまでのURLを作る
	private StringBuffer createServerUrl(String scheme,String server,int port) {
	
		final StringBuffer url = new StringBuffer();

		if (port < 0) {
			port = 80;
		}
		
		url.append(scheme);
		url.append("://");
		url.append(server);

		if ((scheme.equals("http") && (port != 80))
				|| (scheme.equals("https") && (port != 443))) {
			url.append(':');
	        url.append(port);
	    }
		
	    return url;	
	}
	
}

/*
 * 作成日: 2004/06/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.com_set.ProfileChargeClassBean;
import jp.co.fj.keinavi.beans.individual.I002Bean;
import jp.co.fj.keinavi.beans.individual.IUtilBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.com_set.ChargeClassData;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I002Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.db.RecordProcessor;


/**
 * 
 * 
 * 2005.02.28	Yoshimoto KAWAI - Totec
 * 				担当クラスの年度対応
 * 
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 * 
 * @author Administrator
 * 
 */
public class I002Servlet extends IndividualServlet {

	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
	
		super.execute(request, response);
		
		//フォームからmodeデータの取得
		I002Form i002Form = null;
		try {
			i002Form = (I002Form)getActionForm(request, "jp.co.fj.keinavi.forms.individual.I002Form");
		} catch (Exception e) {
			KNServletException k = new KNServletException("0I002にてフォームデータの収得に失敗しました。");
			k.setErrorCode("00I002010101");
			throw k;
		}
		
		HttpSession session = request.getSession();
		
		// ログイン情報
		LoginSession login = super.getLoginSession(request);
		//プロファイル情報
		Profile profile = (Profile) session.getAttribute(Profile.SESSION_KEY);
		List chargeClassDatas = (List) profile.getItemMap(IProfileCategory.CM).get(IProfileItem.CLASS);
		String studentSelected = (String) profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.STUDENT_SELECTED);
		//個人共通
		ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);		
		
		if (getForward(request).startsWith("i002")) {
			// JSP転送
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(false);
			
				// 担当クラスの年度
				String year = ProfileUtil.getChargeClassYear(profile);
				
				I002Bean i002Bean = new I002Bean();
				i002Bean.setConnection("", con);
				i002Bean.setClasses(IUtilBean.convert2ClassArray(chargeClassDatas));
				i002Bean.setSchoolCd(login.getUserID());
				i002Bean.setBundleCd(profile.getBundleCD());
				i002Bean.setHelpDesk(login.isHelpDesk());
				i002Bean.setYear(year);
				
				// 分析モード		
				if(iCommonMap.isBunsekiMode()){
					// ２回目以降
					if (getBackward(request).startsWith("i002") && !isChangedMode(request)) {
						i002Bean.setNewMode(false);
						i002Bean.setExamYear(i002Form.getTargetExamYear());
						i002Bean.setExamCode(i002Form.getTargetExamCode());
						i002Bean.setExamCodePrev(i002Form.getTargetExamCodePrev());
						i002Bean.setSubCd(i002Form.getTargetSubject());
						i002Bean.setSelectGrade(i002Form.getTargetGrade());
						i002Bean.setSelectClass(i002Form.getTargetClass());
						i002Bean.setSelectPerson(i002Form.getPersonId());
						i002Bean.execute();
					
					// 初回またはモード切替
					} else {
						i002Bean.setSelectPerson(CollectionUtil.deconcatComma(studentSelected));
						i002Bean.setNewMode(true);					
						i002Bean.execute();
					}

					// フォーム初期化
					i002Form.setPersonId(i002Bean.getSelectPerson());
					i002Form.setTargetExamYear(RecordProcessor.trimNull(i002Bean.getFormExamYear()));
					i002Form.setTargetExamCode(RecordProcessor.trimNull(i002Bean.getFormExamCd()));
					i002Form.setTargetExamName(RecordProcessor.trimNull(i002Bean.getFormExamName()));
					i002Form.setTargetExamCodePrev(RecordProcessor.trimNull(i002Bean.getFormPExamCd()));
					i002Form.setTargetSubject(RecordProcessor.trimNull(i002Bean.getFormSubCd()));
					i002Form.setTargetGrade(RecordProcessor.trimNull(i002Bean.getFormGrade()));

					// 対象模試の初期化
					if (i002Bean.getFormExamYear() != null
						&& !"".equals(i002Bean.getFormExamYear())) {
								
						iCommonMap.setTargetExamYear(i002Bean.getFormExamYear());
						iCommonMap.setTargetExamCode(i002Bean.getFormExamCd());						
					}
				
				// 面談モード	
				} else {
					// ２回目以降
					if (getBackward(request).startsWith("i002") && !isChangedMode(request)) {
						i002Bean.setNewMode(false);
						i002Bean.setExamYear(i002Form.getTargetExamYear());
						i002Bean.setExamCode(i002Form.getTargetExamCode());
						i002Bean.setExamCodePrev(i002Form.getTargetExamCodePrev());
						i002Bean.setSubCd(i002Form.getTargetSubject());
						i002Bean.setSelectGrade(i002Form.getTargetGrade());
						i002Bean.setSelectClass(i002Form.getTargetClass());
						i002Bean.execute();

					// 初回またはモード切替
					}else{
						i002Bean.setSelectPerson(request.getParameterValues("targetPersonId"));
						i002Bean.setNewMode(true);
						i002Bean.execute();
						
						//1130 kondo 削除
						// 対象模試の初期化	
						//iCommonMap.setTargetExamYear(profile.getTargetYear());
						//iCommonMap.setTargetExamCode(profile.getTargetExam());
					}

					// フォーム初期化
					i002Form.setPersonId(i002Bean.getSelectPerson());
					i002Form.setTargetExamYear(RecordProcessor.trimNull(i002Bean.getFormExamYear()));
					i002Form.setTargetExamCode(RecordProcessor.trimNull(i002Bean.getFormExamCd()));
					i002Form.setTargetExamName(RecordProcessor.trimNull(i002Bean.getFormExamName()));
					i002Form.setTargetExamCodePrev(RecordProcessor.trimNull(i002Bean.getFormPExamCd()));
					i002Form.setTargetSubject(RecordProcessor.trimNull(i002Bean.getFormSubCd()));
					i002Form.setTargetGrade(RecordProcessor.trimNull(i002Bean.getFormGrade()));
					
					//1130 kondo 追加
					//面談モード時、対象模試(最新)をセット
					iCommonMap.setTargetExamYear(i002Bean.getFormExamYear());
					iCommonMap.setTargetExamCode(i002Bean.getFormExamCd());
				}

				request.setAttribute("studentSelected", CollectionUtil.deconcatComma(studentSelected));//デフォルトチェックボックス
				request.setAttribute("chargeClassDatas", chargeClassDatas);	//トップに表示するため

				request.setAttribute("recordList", i002Bean.getRecordList());
				request.setAttribute("i002Bean", i002Bean);;//examComboData
				request.setAttribute("subjectList", i002Bean.getSubjectList());//科目コンボ
				request.setAttribute("ExamDataList", i002Bean.getExamDataList()); // 模試データリスト
				request.setAttribute("ExamYearList", i002Bean.getExamYearList()); // 模試年度リスト

				//checkBean(i002Bean);
				/** コンボボックスをセッションに入れよーか */
				iCommonMap.setExamComboData(i002Bean.getExamComboData());
                
                //選択模試試験が存在すれば、ExamData として設定し直す
                if (i002Bean.getExamComboData().getYearDatas().size() > 0) {
                    iCommonMap.setTargetExam(i002Form.getTargetExamYear(), i002Form.getTargetExamCode());
                }
				
				// 2004.10.25
				// Yoshimoto KAWAI - Totec
				// 担当クラスの情報の取得（複合クラス対応）
				{
					final ProfileChargeClassBean bean =
							new ProfileChargeClassBean(super.getProfile(request));
					
					bean.setConnection(null, con);
					bean.execute();

					request.setAttribute("ChargeClass", bean);
				}
				
				// 学年のプルダウン
				{
					Set set = new HashSet(); // 学年セット
					Iterator ite = chargeClassDatas.iterator();
					while (ite.hasNext()) {
						ChargeClassData cd = (ChargeClassData)ite.next();
						set.add(String.valueOf(cd.getGrade()));
					}
					List container = new LinkedList();
		
					String[] grade = new String[] {"3", "2", "1", "4", "5", "9"};
					for (int i=0; i<grade.length; i++) {
						if (set.contains(grade[i])) container.add(grade[i]);
					}

					request.setAttribute("GradeList", container);					
				}
				
				con.commit();
			}catch(Exception e){
				super.rollback(con);
				e.printStackTrace();
				KNServletException k = new KNServletException("0I002にてエラーが発生しました。");
				k.setErrorCode("00I002010101");
				throw k;
			} finally {
				try {releaseConnectionPool(request, con);
				} catch (Exception e) {
					KNServletException k1 = new KNServletException("0I002にてコネクションの解放に失敗しました。");
					k1.setErrorCode("00I002010101");
					throw k1;
				}
			}
			
			request.setAttribute("i002Form", i002Form);
			
			super.forward(request, response, JSP_I002);
				
		} else {// 不明ならServlet転送
			
			try{
				// 転送先がi003のみプロファイル保存処理を行う
				if ("i003".equals(super.getForward(request))) {
					//分析モード時のみプロファイルに選択された生徒の(P)PERSONIDsを格納
					if(iCommonMap.isBunsekiMode()){
						if(i002Form.getPersonId() != null){
							profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.STUDENT_SELECTED, CollectionUtil.deSplitComma(i002Form.getPersonId()));
						}
					}
					//分析モード・選択モード関係なしに選択した生徒の一番最初を表示する生徒にする
					iCommonMap.setTargetPersonId(i002Form.getPersonId()[0]);
					/*
					else{
					//ICOMMONMAPに選択された生徒の(S)PERSONIDを格納
						if(i002Form.getPersonId() != null){
							iCommonMap.setTargetPersonId(i002Form.getPersonId()[0]);
						}
					}
					*/
					
					//profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.STUDENT_SELECTED, CollectionUtil.deSplitComma(i002Form.getPersonId()));
					
					//1130 kondo 追加
					if(iCommonMap.isBunsekiMode()){
						//分析モードの場合は、画面の値をセッションに
						iCommonMap.setTargetExamYear(i002Form.getTargetExamYear());
						iCommonMap.setTargetExamName(i002Form.getTargetExamName());
						iCommonMap.setTargetExamCode(i002Form.getTargetExamCode());
					} else {
						//面談モードの場合は、コンボが無いのでセッションの値をそのまま使う
					}
					
					// 変更フラグ
					profile.setChanged(true);
				}			
				super.forward(request, response, SERVLET_DISPATCHER);
			}catch(Exception e){
				e.printStackTrace();
				KNServletException k = new KNServletException("0I002にてプロファイルの保存に失敗しました。" + e);
				k.setErrorCode("00I002070203");
				throw k;
			}
		}	
	}
	
}

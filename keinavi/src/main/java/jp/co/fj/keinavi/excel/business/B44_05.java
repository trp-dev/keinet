package jp.co.fj.keinavi.excel.business;

//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.io.FileInputStream;
//import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.business.B44GakkoListBean;
import jp.co.fj.keinavi.excel.data.business.B44HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.business.B44Item;
import jp.co.fj.keinavi.excel.data.business.B44ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
//import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 * 作成日: 2004/09/09
 * @author A.Hasegawa
 *
 * 校内成績分析−過年度比較−志望大学評価別人数　大学（学部・学科あり）出力処理
 *
 * <2010年度改修>
 * 2010.01.06   Tomohisa YAMADA - Totec
 *              センター・リサーチ５段階評価対応
 *
 */
public class B44_05 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	private String		strArea			= "A1:AI58";	//印刷範囲
	private String		mastersheet 	= "tmp";		//テンプレートシート名
	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow		= 54;			//MAX行数

	// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
	//// 2019/09/30 QQ) 共通テスト対応 UPD START
	//private int		intMaxClassSr   = 4;			//MAXクラス数
	//// 2019/09/30 QQ) 共通テスト対応 UPD END
	private int            intMaxClassSr   = 6;                    //MAXクラス数
	// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD END

	private int intDataStartRow = 6;					//データセット開始行
	private int intDataStartCol = 5;					//データセット開始列

	final private String masterfile0 = "B44_05" ;
	final private String masterfile1 = "B44_05" ;
	private String masterfile = "" ;

	final private String strCenterResearch = "38";

	/*
	 * 	Excel編集メイン
	 * 		B44Item B44Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int b44_05EditExcel(B44Item b44Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		//テンプレートの決定
		if (b44Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		/** セット処理 **/
		boolean bolBookCngFlg = true;			//改ファイルフラグ
		boolean bolSheetCngFlg = true;			//改シートフラグ

		FileInputStream	fin = null;
		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFCell	workCell		= null;
		HSSFRow		workRow			= null;
		HSSFWorkbook	tmpworkbook = null;

		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intRow = 0;
		int intCol = 0;

		String strGenekiKbn = "";
		String strDaigakuCode = "";
		String strGkbCode = "";
		String strGkaCode = "";
		String strHyoukaKbn = "";
		String strNendo = "";

		//2004.09.27Add
		String strKokushiKbn = "";
		String strStKokushiKbn = "";
		int intChangeSheetRow = 0;
		//2004.09.27AddEnd

		int intLoopCnt = 0;
		int intNinzuListCnt = 0;
		int intSheetCngCount = 0;
		int intBookCngCount = 1;

		boolean bolDispHyoukaFlg = false;
		boolean bolDispGkbFlg = false;
		boolean bolDispGkaFlg = false;
		boolean bolDispDaigakuFlg = false;
		String strHyouka = "";

		String strDispTarget = "";

		ArrayList WorkBookList = new ArrayList();
		ArrayList WorkSheetList = new ArrayList();

		int intSheetCnt=0;

		int intBookIndex = -1;
		int intSheetIndex = -1;

		int intSheetMei = -1;

		int intSetRow = intDataStartRow;

		boolean bolClassDispFlg=true;

		KNLog log = KNLog.getInstance(null,null,null);
		ArrayList HyoukaTitleList = new ArrayList();

		log.Ep("B44_05","B44_05帳票作成開始","");

		// 基本ファイルを読込む
		B44ListBean b44ListBean = new B44ListBean();

		try{
			/** B44Item編集 **/
			b44Item  = editB44( b44Item );

			HyoukaTitleList.add(0,"A");
			HyoukaTitleList.add(1,"B");
			HyoukaTitleList.add(2,"C");

			// 基本ファイルを読込む
			ArrayList b44List = b44Item.getB44List();
			ListIterator itr = b44List.listIterator();

//			if( itr.hasNext() == false ){
//				return errfdata;
//			}

			/** 大学・学部・日程・評価 **/
			// B44ListBean
			while( itr.hasNext() ){
				b44ListBean = (B44ListBean)itr.next();

				// 大学名
				bolDispDaigakuFlg = false;
				if( !cm.toString(strDaigakuCode).equals(cm.toString(b44ListBean.getStrDaigakuCd()))){
					bolDispDaigakuFlg = true;
					strHyoukaKbn = "";
					strGkaCode = "";
					strGkbCode = "";
					//大学名表示
					strDaigakuCode = b44ListBean.getStrDaigakuCd();
				}

				// 学部
				bolDispGkbFlg = false;
				if( !cm.toString(strGkbCode).equals(cm.toString(b44ListBean.getStrGakubuCd())) ){
					bolDispGkbFlg = true;
					strHyoukaKbn = "";
					strGkaCode = "";
					strGkbCode = b44ListBean.getStrGakubuCd();
				}

				// 学科
				bolDispGkaFlg = false;
				if( !cm.toString(strGkaCode).equals(cm.toString(b44ListBean.getStrGakkaCd())) ){
					bolDispGkaFlg = true;
					strHyoukaKbn = "";
					strGkaCode = b44ListBean.getStrGakkaCd();
				}

				// 評価区分
				bolDispHyoukaFlg = false;
				if( !cm.toString(strHyoukaKbn).equals(cm.toString(b44ListBean.getStrHyoukaKbn())) || bolDispGkaFlg == true ){
					bolDispHyoukaFlg = true;
					//評価区分表示
					strHyoukaKbn = b44ListBean.getStrHyoukaKbn();
					strHyouka = "";
					switch( Integer.parseInt(strHyoukaKbn) ){
						case 1:
							// 2019/09/30 QQ) 共通テスト対応 UPD START
							//strHyouka = "センター";
							strHyouka = "共テ";
							// 2019/09/30 QQ) 共通テスト対応 UPD END
							break;
						case 2:
							strHyouka = "2次・私大";
							break;
						case 3:
							strHyouka = "総合";
							break;
					}
				}

				// 表示対象
				if( !cm.toString(strGenekiKbn).equals(cm.toString(b44ListBean.getStrGenKouKbn())) ){
					strGenekiKbn = b44ListBean.getStrGenKouKbn();
					if( strGenekiKbn.equals("0") ){
						strDispTarget = "全体";
					}
					else if( strGenekiKbn.equals("1") ){
						strDispTarget = "現役生";
					}
					else if( strGenekiKbn.equals("2") ){
						strDispTarget = "高卒生";
					}

					if( bolSheetCngFlg == false ){
						bolSheetCngFlg = true;
					}
				}

				//2004.9.27 Add
				int intDaigakuCd = (int)Integer.parseInt(b44ListBean.getStrDaigakuCd());
				if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
					strKokushiKbn = "国公立大";
				}
				if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
					strKokushiKbn = "私立大";
				}
				if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
					strKokushiKbn = "短大";
				}
				if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
					strKokushiKbn = "その他";
				}

				if( (!cm.toString(strKokushiKbn).equals(cm.toString(strStKokushiKbn))) ){
					strStKokushiKbn = strKokushiKbn;
					bolSheetCngFlg = true;
				}
				//2004.9.27 Add End

				//クラス別評価人数リスト読み込み クラス名をExcelへセット
				ArrayList b44GakkoList = b44ListBean.getB44GakkoList();
				ListIterator itr2 = b44GakkoList.listIterator();

				// Listの取得
				B44GakkoListBean b44GakkoListBean = new B44GakkoListBean();
				B44GakkoListBean b44GakkoListBeanHomeData = new B44GakkoListBean();

				B44GakkoListBean HomeDataBean = new B44GakkoListBean();

				//クラス数取得
				int intClassSr = b44GakkoList.size();

				int intSheetSr = 0;
				if( intClassSr % intMaxClassSr > 0 ){
					intSheetSr = (int)Math.ceil(intClassSr / intMaxClassSr) + 1;
				}else{
					intSheetSr = (int)Math.ceil(intClassSr / intMaxClassSr);
				}

				int intLoopCnt2 = 0;
				int intNendoCount = 0;
				intChangeSheetRow = 0;

				boolean bolHomeDataDispFlg=false;
				/** 学校名・学年・クラス **/
				// B44GakkoListBean
				while( itr2.hasNext() ){
					String strClassmei = "";
					b44GakkoListBean = (B44GakkoListBean)itr2.next();

					// Listの取得
					ArrayList b44HyoukaNinzuList = b44GakkoListBean.getB44HyoukaNinzuList();
					B44HyoukaNinzuListBean b44HyoukaNinzuListBean = new B44HyoukaNinzuListBean();
					ListIterator itr3 = b44HyoukaNinzuList.listIterator();

					// 自校情報表示判定
					if( intLoopCnt2 == 0 ){
						bolHomeDataDispFlg = true;
						HomeDataBean = b44GakkoListBean;
//						b44GakkoListBean = (B44GakkoListBean)itr2.next();
						intNendoCount = b44HyoukaNinzuList.size();
						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
						intLoopCnt2++;
					}

					// 始点情報（Col）
					if( intLoopCnt2 % intMaxClassSr == 0 ){
					        // 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
						//// 2019/09/30 QQ) 共通テスト対応 UPD START
						//intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 8 );
						//// 2019/09/30 QQ) 共通テスト対応 UPD END
					        intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 5 );
						// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
					}else{
					        // 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
						//// 2019/09/30 QQ) 共通テスト対応 UPD START
						//intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 8 );
						//// 2019/09/30 QQ) 共通テスト対応 UPD END
					        intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 5 );
					        // 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD END
					}

//					intCol = intDataStartCol + ( ( intMaxClassSr - ( intLoopCnt2 % intMaxClassSr ) ) * 5 );

					/** 年度・志望者数 **/
					// B44HyoukaNinzuListBean
					int intHyoukaNinzuListIndex = 0;
					while( itr3.hasNext() ){
						b44HyoukaNinzuListBean = (B44HyoukaNinzuListBean)itr3.next();

						// 規定行数を表示したら線を引く
						if( intSetRow  >= intChangeSheetRow||((bolSheetCngFlg == true)&&(intMaxSheetIndex != 0)) ){
							bolSheetCngFlg = true;
							for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//								int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
								int intBIndex = (intSindex + 1) / intMaxSheetSr;
								if((intSindex + 1)%intMaxSheetSr!=0){
									intBIndex++;
								}
								workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
								workSheet = (HSSFSheet)WorkSheetList.get(intSindex);

								// 罫線出力(セルの上部に太線を引く）

								// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
								//// 2019/09/30 QQ) 共通テスト対応 UPD START
								//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//									intSetRow, intSetRow, 0, 36, false, true, false, false );
								//// 2019/09/30 QQ) 共通テスト対応 UPD END
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
	                                                                              intSetRow, intSetRow, 0, 34, false, true, false, false );
								// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD END

								if( workSheet != null ){
									int intRemoveStartRow = intChangeSheetRow;
									if( intMaxRow + intDataStartRow > intRemoveStartRow ){
										for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
											//行削除処理
											workSheet.removeRow(workSheet.getRow(index));
										}
										//罫線出力（セルの上部に太線を出力）

										// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
										//// 2019/09/30 QQ) 共通テスト対応 UPD START
										//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
										//				intRemoveStartRow, intRemoveStartRow, 0, 36, false, true, false, false);
										//// 2019/09/30 QQ) 共通テスト対応 UPD END
										cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
	                                                                                              intRemoveStartRow, intRemoveStartRow, 0, 32, false, true, false, false);
										// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD END
									}
								}
							}

							////////////////////保存処理//////////////////
							if( ((HSSFWorkbook)WorkBookList.get(0)).getNumberOfSheets() > intMaxSheetSr ){
								tmpworkbook = (HSSFWorkbook)WorkBookList.get(0);

								boolean bolRet = false;
								// Excelファイル保存
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, tmpworkbook, UserID, intBookCngCount, masterfile, intMaxSheetSr);

								if( bolRet == false ){
									return errfwrite;
								}
								intBookCngCount++;

								// 削除
								WorkBookList.remove(0);
								for(int a=0; a<intMaxSheetSr;a++){
									WorkSheetList.remove(0);
								}
								tmpworkbook=null;
								intBookIndex--;
								intSheetIndex -= intMaxSheetSr;
							}
						}

						// 改シート判定
						if( bolSheetCngFlg == true ){

							bolDispHyoukaFlg = true;
							bolDispDaigakuFlg = true;
							bolDispGkbFlg = true;
							bolDispGkaFlg = true;

							intSetRow = intDataStartRow;
							bolClassDispFlg = true;
							bolHomeDataDispFlg = true;

							for( int intIndex = 0; intIndex < intSheetSr; intIndex ++ ){

								if( ( intMaxSheetIndex >= intMaxSheetSr ) || ( bolBookCngFlg == true ) ){
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook.equals(null) ){
										return errfread;
									}
									WorkBookList.add(workbook);
									intBookIndex++;

									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
									workbook = (HSSFWorkbook)WorkBookList.get(intBookIndex);
								}

								//シート作成
								// シートテンプレートのコピー
								intSheetMei = intMaxSheetIndex + 1;
								workSheet = workbook.cloneSheet(0);
								WorkSheetList.add(workSheet);
								intSheetIndex++;
								workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex);

								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);

								// セキュリティスタンプセット

								// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
								//// 2019/09/30 QQ) 共通テスト対応 UPD START
								//String secuFlg = cm.setSecurity( workbook, workSheet, b44Item.getIntSecuFlg(), 34, 36 );
								//workCell = cm.setCell( workSheet, workRow, workCell, 0, 34 );
								//// 2019/09/30 QQ) 共通テスト対応 UPD END
								String secuFlg = cm.setSecurity( workbook, workSheet, b44Item.getIntSecuFlg(), 32, 34 );
                                                                workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
                                                                // 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD END
								workCell.setCellValue(secuFlg);

//								// 学校名セット
//								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
//								workCell.setCellValue( "学校名　：" + b44Item.getStrGakkomei() );

								// 模試月取得
								String moshi =cm.setTaisyouMoshi( b44Item.getStrMshDate() );

								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
								workCell.setCellValue( "対象模試：" + cm.toString(b44Item.getStrMshmei()) + moshi);

								//表示対象
								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
								workCell.setCellValue( "表示対象：" + strDispTarget );

								//評価別人数 見出し行
								for( int i = 1; i <= intMaxClassSr; i++ ){

								        // 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
									//// 2019/09/30 QQ) 共通テスト対応 UPD START
									//int intTitleSetCol = (i * 8) + ( intDataStartCol - 6 );
									//// 2019/09/30 QQ) 共通テスト対応 UPD END
								        int intTitleSetCol = (i * 5) + ( intDataStartCol - 3 );
								        // 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD END

									//出願予定・第1志望

									//2019/10/03 QQ)Oosaki 共通テスト対応 DEL START
									//// 2019/09/30 QQ) 共通テスト対応 UPD START
									////if (strKokushiKbn == "国公立大") {
									////	workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol-1 );
									////	workCell.setCellValue( "出願予定" );
									////} else {
									////	workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol-1 );
									////	workCell.setCellValue( "第一志望" );
									////}
									//workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol-1 );
									//workCell.setCellValue( "第１志望" );
									//// 2019/09/30 QQ) 共通テスト対応 UPD END
									//2019/10/03 QQ)Oosaki 共通テスト対応 DEL END

									workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol );
									workCell.setCellValue( (String)HyoukaTitleList.get(0) );

									// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
									//// 2019/09/30 QQ) 共通テスト対応 UPD START
									//workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol+2 );
									//// 2019/09/30 QQ) 共通テスト対応 UPD END
									workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol+1 );
									// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD END
									workCell.setCellValue( (String)HyoukaTitleList.get(1) );

									// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
									//// 2019/09/30 QQ) 共通テスト対応 UPD START
									//workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol+4 );
									//// 2019/09/30 QQ) 共通テスト対応 UPD END
									workCell = cm.setCell( workSheet, workRow, workCell, 5, intTitleSetCol+2 );
									// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD END
									workCell.setCellValue( (String)HyoukaTitleList.get(2) );
								}

								intMaxSheetIndex++;		//シート数カウンタインクリメント
								bolSheetCngFlg = false;

								intLoopCnt = 0;
							}	//ForEnd

						}	//if( bolSheetCngFlg == true )

						intSheetCnt = (int)Math.ceil( (double)intLoopCnt2 / (double)intMaxClassSr );
						workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex -  (intSheetSr - intSheetCnt)  );

						int intSheetNo = intSheetIndex -  (intSheetSr - intSheetCnt);
						int intBIndex = (int)Math.ceil((double)(intSheetNo + 1) / (double)intMaxSheetSr );
						workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);

						if( intCol == intDataStartCol ){

							//データセット
							//自校データ・大学名ほか表示
							//自校学校名
//							workCell = cm.setCell( workSheet, workRow, workCell, 3, 5 );
//							workCell.setCellValue(HomeDataBean.getStrGakkomei());
							//自校データ
//							ListIterator itrHome = HomeDataBean.getB44HyoukaNinzuList().listIterator();
//							int intLoopIndex = 0;
//							while( itrHome.hasNext() ){
//								B44HyoukaNinzuListBean HomeHyoukaNinzuListBean = (B44HyoukaNinzuListBean) itrHome.next();
//								boolean ret = setData( workSheet, HomeHyoukaNinzuListBean, intSetRow + intLoopIndex , 4 );
//								if( ret == false ){
//									return errfdata;
//								}
//								intLoopIndex++;
//							}

							if( bolDispHyoukaFlg == true ){
								//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
															intSetRow, intSetRow, 3, 3, false, true, false, false);
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 3);
								workCell.setCellValue( strHyouka );

							}

							if( bolDispGkaFlg == true ){
								//罫線出力(セルの上部（日程の列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
															intSetRow, intSetRow, 2, 3, false, true, false, false);
								//学科表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( b44ListBean.getStrGakkaMei() );

							}

							if( bolDispGkbFlg == true ){
								//罫線出力(セルの上部（学部の列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
															intSetRow, intSetRow, 1, 3, false, true, false, false);
								//学部表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
								workCell.setCellValue( b44ListBean.getStrGakubuMei() );

							}

							if( bolDispDaigakuFlg == true ){
								//罫線出力(セルの上部に太線を引く）

							        // 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
								//// 2019/09/30 QQ) 共通テスト対応 UPD START
								//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//									intSetRow, intSetRow, 0, 36, false, true, false, false );
								//// 2019/09/30 QQ) 共通テスト対応 UPD END
							        cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
							                                                                intSetRow, intSetRow, 0, 34, false, true, false, false );
								// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD END

								//大学名表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 0 );
								workCell.setCellValue(b44ListBean.getStrDaigakuMei());
							}

						}


						bolHomeDataDispFlg = false;

						boolean ret = setData( workSheet, b44HyoukaNinzuListBean, intSetRow + intHyoukaNinzuListIndex, intCol);
						if( ret != true){
							return errfdata;
						}
						else{
							intHyoukaNinzuListIndex++;
							intLoopCnt++;
						}

						//

					}	//WhileEnd( itr3 )


					//高校名
					if( bolClassDispFlg == true ){
						String strGakko = b44GakkoListBean.getStrGakkomei();
						workCell = cm.setCell( workSheet, workRow, workCell, 3, intCol );
						workCell.setCellValue(strGakko);
					}
					intLoopCnt2++;
				}	//WhileEnd( itr2 )

				intSetRow += intNendoCount;
				bolClassDispFlg = false;

			}	//WhileEnd( itr )

			for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//				int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
				int intBIndex = (intSindex + 1) / intMaxSheetSr;
				if((intSindex + 1)%intMaxSheetSr!=0){
					intBIndex++;
				}
				workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
				workSheet = (HSSFSheet)WorkSheetList.get(intSindex);

				//罫線出力(セルの上部に太線を引く）

				// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
				//// 2019/09/30 QQ) 共通テスト対応 UPD START
				//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
				//									intSetRow, intSetRow, 0, 36, false, true, false, false );
				//// 2019/09/30 QQ) 共通テスト対応 UPD END
				cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
	                                              intSetRow, intSetRow, 0, 34, false, true, false, false );
				// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD END

				if( workSheet != null ){
					int intRemoveStartRow = intChangeSheetRow;
					if( intMaxRow + intDataStartRow > intRemoveStartRow ){
						for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
							//行削除処理
							workSheet.removeRow(workSheet.getRow(index));
						}
						//罫線出力（セルの上部に太線を出力）

						// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
						//// 2019/09/30 QQ) 共通テスト対応 UPD START
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
						//				intRemoveStartRow, intRemoveStartRow, 0, 36, false, true, false, false);
						//// 2019/09/30 QQ) 共通テスト対応 UPD END
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
	                                                                intRemoveStartRow, intRemoveStartRow, 0, 32, false, true, false, false);
						// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD END
					}
				}

			}

			//ファイル保存
			for( int intBookSr = 0; intBookSr < WorkBookList.size(); intBookSr++ ){
				workbook = (HSSFWorkbook)WorkBookList.get(intBookSr);

				boolean bolRet = false;
				// Excelファイル保存
				if( (intBookCngCount != 1)||((intBookCngCount == 1)&&(WorkBookList.size()>1)) ){
					if(intBookSr + 1 < WorkBookList.size()){
						// 最終ブック以外は、フルシートを使っているはず
						bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, intMaxSheetSr);
					}
					else{
						// 最終ブックは、フルシートを使っているとは限らない
						bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, intMaxSheetIndex);
					}
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}

			//データ無し
			if ( WorkBookList.size() == 0 ){
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex = 1;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);

				// セキュリティスタンプセット

				// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
				//// 2019/09/30 QQ) 共通テスト対応 UPD START
				//String secuFlg = cm.setSecurity( workbook, workSheet, b44Item.getIntSecuFlg(), 34, 36 );
				//workCell = cm.setCell( workSheet, workRow, workCell, 0, 34 );
				//// 2019/09/30 QQ) 共通テスト対応 UPD END
				String secuFlg = cm.setSecurity( workbook, workSheet, b44Item.getIntSecuFlg(), 32, 34 );
                                workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
				// 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD END

				workCell.setCellValue(secuFlg);

				// 模試月取得
				String moshi =cm.setTaisyouMoshi( b44Item.getStrMshDate() );

				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "対象模試：" + b44Item.getStrMshmei() + moshi);

				//表示対象
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "表示対象：" );

				boolean bolRet = false;
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}


		}
		catch( Exception e ){
			log.Err("99B44_05","データセットエラー",e.toString());
			e.printStackTrace();
			return errfdata;
		}

		log.Ep("B44_05","B44_05帳票作成終了","");
		return noerror;
	}

	/**
	 * B44Item加算・編集
	 * @param b44Item
	 * @return
	 */
	private B44Item editB44( B44Item b44Item ) throws Exception{
		try{
			B44Item		b44ItemEdt	= new B44Item();
			String		daigakuCd	= "";
			String		gakubuCd	= "";
			String		gakkaCd		= "";
			String 		genekiKbn 	= "";

			// 学部計用
			boolean	centerGKBFlg	= false;
			boolean	secondGKBFlg	= false;
			boolean	totalGKBFlg	= false;

			ArrayList b44GakkoListGKBAdd		= new ArrayList();
			ArrayList b44GakkoListGKBAddCnt	= new ArrayList();
			ArrayList b44GakkoListGKBAddSnd	= new ArrayList();
			ArrayList b44GakkoListGKBAddTtl	= new ArrayList();
			ArrayList b44NinzuListGKBAdd		= new ArrayList();

			B44ListBean b44ListBeanGKBAddCnt = new B44ListBean();
			B44ListBean b44ListBeanGKBAddSnd = new B44ListBean();
			B44ListBean b44ListBeanGKBAddTtl = new B44ListBean();

			// 日程計用
			boolean	zenCenterNTIFlg	= false;
			boolean	zenSecondNTIFlg	= false;
			boolean	zenTotalNTIFlg	= false;
			boolean	chuCenterNTIFlg	= false;
			boolean	chuSecondNTIFlg	= false;
			boolean	chuTotalNTIFlg	= false;
			boolean	kokCenterNTIFlg	= false;
			boolean	kokSecondNTIFlg	= false;
			boolean	kokTotalNTIFlg	= false;
			boolean	ipnCenterNTIFlg	= false;
			boolean	ipnSecondNTIFlg	= false;
			boolean	ipnTotalNTIFlg	= false;
			boolean	cenCenterNTIFlg	= false;
			boolean	cenSecondNTIFlg	= false;
			boolean	cenTotalNTIFlg	= false;

			ArrayList b44GakkoListNTIAdd		= new ArrayList();
			ArrayList b44GakkoListNTIAddZenCnt		= new ArrayList();
			ArrayList b44GakkoListNTIAddZenSnd		= new ArrayList();
			ArrayList b44GakkoListNTIAddZenTtl		= new ArrayList();
			ArrayList b44GakkoListNTIAddChuCnt		= new ArrayList();
			ArrayList b44GakkoListNTIAddChuSnd		= new ArrayList();
			ArrayList b44GakkoListNTIAddChuTtl		= new ArrayList();
			ArrayList b44GakkoListNTIAddKokCnt		= new ArrayList();
			ArrayList b44GakkoListNTIAddKokSnd		= new ArrayList();
			ArrayList b44GakkoListNTIAddKokTtl		= new ArrayList();
			ArrayList b44GakkoListNTIAddIpnCnt		= new ArrayList();
			ArrayList b44GakkoListNTIAddIpnSnd		= new ArrayList();
			ArrayList b44GakkoListNTIAddIpnTtl		= new ArrayList();
			ArrayList b44GakkoListNTIAddCenCnt		= new ArrayList();
			ArrayList b44GakkoListNTIAddCenSnd		= new ArrayList();
			ArrayList b44GakkoListNTIAddCenTtl		= new ArrayList();
			ArrayList b44NinzuListNTIAdd		= new ArrayList();

			B44ListBean b44ListBeanNTIAddZenCnt = new B44ListBean();
			B44ListBean b44ListBeanNTIAddZenSnd = new B44ListBean();
			B44ListBean b44ListBeanNTIAddZenTtl = new B44ListBean();
			B44ListBean b44ListBeanNTIAddChuCnt = new B44ListBean();
			B44ListBean b44ListBeanNTIAddChuSnd = new B44ListBean();
			B44ListBean b44ListBeanNTIAddChuTtl = new B44ListBean();
			B44ListBean b44ListBeanNTIAddKokCnt = new B44ListBean();
			B44ListBean b44ListBeanNTIAddKokSnd = new B44ListBean();
			B44ListBean b44ListBeanNTIAddKokTtl = new B44ListBean();
			B44ListBean b44ListBeanNTIAddIpnCnt = new B44ListBean();
			B44ListBean b44ListBeanNTIAddIpnSnd = new B44ListBean();
			B44ListBean b44ListBeanNTIAddIpnTtl = new B44ListBean();
			B44ListBean b44ListBeanNTIAddCenCnt = new B44ListBean();
			B44ListBean b44ListBeanNTIAddCenSnd = new B44ListBean();
			B44ListBean b44ListBeanNTIAddCenTtl = new B44ListBean();

			// 大学計用
			boolean	centerDGKFlg	= false;
			boolean	secondDGKFlg	= false;
			boolean	totalDGKFlg	= false;

			ArrayList b44GakkoListDGKAdd		= new ArrayList();
			ArrayList b44GakkoListDGKAddCnt		= new ArrayList();
			ArrayList b44GakkoListDGKAddSnd		= new ArrayList();
			ArrayList b44GakkoListDGKAddTtl		= new ArrayList();
			ArrayList b44NinzuListDGKAdd		= new ArrayList();

			B44ListBean b44ListBeanDGKAddCnt = new B44ListBean();
			B44ListBean b44ListBeanDGKAddSnd = new B44ListBean();
			B44ListBean b44ListBeanDGKAddTtl = new B44ListBean();

//			b44ItemEdt.setStrGakkomei( b44Item.getStrGakkomei() );
			b44ItemEdt.setStrMshmei( b44Item.getStrMshmei() );
			b44ItemEdt.setStrMshDate( b44Item.getStrMshDate() );
			b44ItemEdt.setStrMshCd( b44Item.getStrMshCd() );
			b44ItemEdt.setIntSecuFlg( b44Item.getIntSecuFlg() );
			b44ItemEdt.setIntDaiTotalFlg( b44Item.getIntDaiTotalFlg() );


			/** 大学・学部・学科・日程・評価 **/
			ArrayList b44List		= b44Item.getB44List();
			ArrayList b44ListEdt	= new ArrayList();
			ListIterator itr = b44List.listIterator();
			while( itr.hasNext() ){
				B44ListBean b44ListBean		= (B44ListBean) itr.next();
				B44ListBean b44ListBeanEdt	= new B44ListBean();

				//-----------------------------------------------------------------------------------------
				//大学計・学部計を出力
				//-----------------------------------------------------------------------------------------
				if( !cm.toString(gakubuCd).equals( cm.toString(b44ListBean.getStrGakubuCd()) ) ||
						!cm.toString(daigakuCd).equals( cm.toString(b44ListBean.getStrDaigakuCd()))
						|| !cm.toString(genekiKbn).equals(cm.toString(b44ListBean.getStrGenKouKbn()))) {

					gakubuCd = b44ListBean.getStrGakubuCd();

					if(centerGKBFlg == true){
						b44ListBeanGKBAddCnt.setB44GakkoList( b44GakkoListGKBAddCnt );
						b44ListEdt.add( b44ListBeanGKBAddCnt );
						b44GakkoListGKBAddCnt = new ArrayList();
						centerGKBFlg = false;
					}
					if(secondGKBFlg == true){
						b44ListBeanGKBAddSnd.setB44GakkoList( b44GakkoListGKBAddSnd );
						b44ListEdt.add( b44ListBeanGKBAddSnd );
						b44GakkoListGKBAddSnd = new ArrayList();
						secondGKBFlg = false;
					}
					if(totalGKBFlg == true){
						b44ListBeanGKBAddTtl.setB44GakkoList( b44GakkoListGKBAddTtl );
						b44ListEdt.add( b44ListBeanGKBAddTtl );
						b44GakkoListGKBAddTtl = new ArrayList();
						totalGKBFlg = false;
					}
					b44ListBeanGKBAddCnt = new B44ListBean();
					b44ListBeanGKBAddSnd = new B44ListBean();
					b44ListBeanGKBAddTtl = new B44ListBean();
					b44NinzuListGKBAdd = new ArrayList();
				}
				// 大学コードが変わる→日程計・大学計の保存
//////////////////
//				if( !cm.toString(daigakuCd).equals( cm.toString(b44ListBean.getStrDaigakuCd()) ) ){
//////////////////
				if( (!cm.toString(daigakuCd).equals( cm.toString(b44ListBean.getStrDaigakuCd())))
					||( !cm.toString(genekiKbn).equals(cm.toString(b44ListBean.getStrGenKouKbn()))) ){
					genekiKbn = b44ListBean.getStrGenKouKbn();

					daigakuCd = b44ListBean.getStrDaigakuCd();

					// 日程計
					if(zenCenterNTIFlg == true){
						b44ListBeanNTIAddZenCnt.setB44GakkoList( b44GakkoListNTIAddZenCnt );
						b44ListEdt.add( b44ListBeanNTIAddZenCnt );
						b44GakkoListNTIAddZenCnt = new ArrayList();
						zenCenterNTIFlg = false;
					}
					if(zenSecondNTIFlg == true){
						b44ListBeanNTIAddZenSnd.setB44GakkoList( b44GakkoListNTIAddZenSnd );
						b44ListEdt.add( b44ListBeanNTIAddZenSnd );
						b44GakkoListNTIAddZenSnd = new ArrayList();
						zenSecondNTIFlg = false;
					}
					if(zenTotalNTIFlg == true){
						b44ListBeanNTIAddZenTtl.setB44GakkoList( b44GakkoListNTIAddZenTtl );
						b44ListEdt.add( b44ListBeanNTIAddZenTtl );
						b44GakkoListNTIAddZenTtl = new ArrayList();
						zenTotalNTIFlg = false;
					}
					if(chuCenterNTIFlg == true){
						b44ListBeanNTIAddChuCnt.setB44GakkoList( b44GakkoListNTIAddChuCnt );
						b44ListEdt.add( b44ListBeanNTIAddChuCnt );
						b44GakkoListNTIAddChuCnt = new ArrayList();
						chuCenterNTIFlg = false;
					}
					if(chuSecondNTIFlg == true){
						b44ListBeanNTIAddChuSnd.setB44GakkoList( b44GakkoListNTIAddChuSnd );
						b44ListEdt.add( b44ListBeanNTIAddChuSnd );
						b44GakkoListNTIAddChuSnd = new ArrayList();
						chuSecondNTIFlg = false;
					}
					if(chuTotalNTIFlg == true){
						b44ListBeanNTIAddChuTtl.setB44GakkoList( b44GakkoListNTIAddChuTtl );
						b44ListEdt.add( b44ListBeanNTIAddChuTtl );
						b44GakkoListNTIAddChuTtl = new ArrayList();
						chuTotalNTIFlg = false;
					}
					if(kokCenterNTIFlg == true){
						b44ListBeanNTIAddKokCnt.setB44GakkoList( b44GakkoListNTIAddKokCnt );
						b44ListEdt.add( b44ListBeanNTIAddKokCnt );
						b44GakkoListNTIAddKokCnt = new ArrayList();
						kokCenterNTIFlg = false;
					}
					if(kokSecondNTIFlg == true){
						b44ListBeanNTIAddKokSnd.setB44GakkoList( b44GakkoListNTIAddKokSnd );
						b44ListEdt.add( b44ListBeanNTIAddKokSnd );
						b44GakkoListNTIAddKokSnd = new ArrayList();
						kokSecondNTIFlg = false;
					}
					if(kokTotalNTIFlg == true){
						b44ListBeanNTIAddKokTtl.setB44GakkoList( b44GakkoListNTIAddKokTtl );
						b44ListEdt.add( b44ListBeanNTIAddKokTtl );
						b44GakkoListNTIAddKokTtl = new ArrayList();
						kokTotalNTIFlg = false;
					}
					if(ipnCenterNTIFlg == true){
						b44ListBeanNTIAddIpnCnt.setB44GakkoList( b44GakkoListNTIAddIpnCnt );
						b44ListEdt.add( b44ListBeanNTIAddIpnCnt );
						b44GakkoListNTIAddIpnCnt = new ArrayList();
						ipnCenterNTIFlg = false;
					}
					if(ipnSecondNTIFlg == true){
						b44ListBeanNTIAddIpnSnd.setB44GakkoList( b44GakkoListNTIAddIpnSnd );
						b44ListEdt.add( b44ListBeanNTIAddIpnSnd );
						b44GakkoListNTIAddIpnSnd = new ArrayList();
						ipnSecondNTIFlg = false;
					}
					if(ipnTotalNTIFlg == true){
						b44ListBeanNTIAddIpnTtl.setB44GakkoList( b44GakkoListNTIAddIpnTtl );
						b44ListEdt.add( b44ListBeanNTIAddIpnTtl );
						b44GakkoListNTIAddIpnTtl = new ArrayList();
						ipnTotalNTIFlg = false;
					}
					if(cenCenterNTIFlg == true){
						b44ListBeanNTIAddCenCnt.setB44GakkoList( b44GakkoListNTIAddCenCnt );
						b44ListEdt.add( b44ListBeanNTIAddCenCnt );
						b44GakkoListNTIAddCenCnt = new ArrayList();
						cenCenterNTIFlg = false;
					}
					if(cenSecondNTIFlg == true){
						b44ListBeanNTIAddCenSnd.setB44GakkoList( b44GakkoListNTIAddCenSnd );
						b44ListEdt.add( b44ListBeanNTIAddCenSnd );
						b44GakkoListNTIAddCenSnd = new ArrayList();
						cenSecondNTIFlg = false;
					}
					if(cenTotalNTIFlg == true){
						b44ListBeanNTIAddCenTtl.setB44GakkoList( b44GakkoListNTIAddCenTtl );
						b44ListEdt.add( b44ListBeanNTIAddCenTtl );
						b44GakkoListNTIAddCenTtl = new ArrayList();
						cenTotalNTIFlg = false;
					}

					// 大学計
					if(centerDGKFlg == true){
						b44ListBeanDGKAddCnt.setB44GakkoList( b44GakkoListDGKAddCnt );
						b44ListEdt.add( b44ListBeanDGKAddCnt );
						b44GakkoListDGKAddCnt = new ArrayList();
						centerDGKFlg = false;
					}
					if(secondDGKFlg == true){
						b44ListBeanDGKAddSnd.setB44GakkoList( b44GakkoListDGKAddSnd );
						b44ListEdt.add( b44ListBeanDGKAddSnd );
						b44GakkoListDGKAddSnd = new ArrayList();
						secondDGKFlg = false;
					}
					if(totalDGKFlg == true){
						b44ListBeanDGKAddTtl.setB44GakkoList( b44GakkoListDGKAddTtl );
						b44ListEdt.add( b44ListBeanDGKAddTtl );
						b44GakkoListDGKAddTtl = new ArrayList();
						totalDGKFlg = false;
					}
					b44ListBeanNTIAddZenCnt	= new B44ListBean();
					b44ListBeanNTIAddZenSnd	= new B44ListBean();
					b44ListBeanNTIAddZenTtl	= new B44ListBean();
					b44ListBeanNTIAddChuCnt	= new B44ListBean();
					b44ListBeanNTIAddChuSnd	= new B44ListBean();
					b44ListBeanNTIAddChuTtl	= new B44ListBean();
					b44ListBeanNTIAddKokCnt	= new B44ListBean();
					b44ListBeanNTIAddKokSnd	= new B44ListBean();
					b44ListBeanNTIAddKokTtl	= new B44ListBean();
					b44ListBeanNTIAddIpnCnt	= new B44ListBean();
					b44ListBeanNTIAddIpnSnd	= new B44ListBean();
					b44ListBeanNTIAddIpnTtl	= new B44ListBean();
					b44ListBeanNTIAddCenCnt	= new B44ListBean();
					b44ListBeanNTIAddCenSnd	= new B44ListBean();
					b44ListBeanNTIAddCenTtl	= new B44ListBean();
					b44NinzuListNTIAdd = new ArrayList();

					b44ListBeanDGKAddCnt = new B44ListBean();
					b44ListBeanDGKAddSnd = new B44ListBean();
					b44ListBeanDGKAddTtl = new B44ListBean();
					b44NinzuListDGKAdd = new ArrayList();
				}

				// 基本部分の移し変え
				b44ListBeanEdt.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
				b44ListBeanEdt.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
				b44ListBeanEdt.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
				b44ListBeanEdt.setStrGakubuCd( b44ListBean.getStrGakubuCd() );
				b44ListBeanEdt.setStrGakubuMei( b44ListBean.getStrGakubuMei() );
				b44ListBeanEdt.setStrGakkaCd( b44ListBean.getStrGakkaCd() );
				b44ListBeanEdt.setStrGakkaMei( b44ListBean.getStrGakkaMei() );
				b44ListBeanEdt.setStrNtiCd( b44ListBean.getStrNtiCd() );
				b44ListBeanEdt.setStrNtiMei( b44ListBean.getStrNtiMei() );
				b44ListBeanEdt.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );

				// 学部計値の保存部
				switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
					case 1:
						b44ListBeanGKBAddCnt.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanGKBAddCnt.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanGKBAddCnt.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanGKBAddCnt.setStrGakubuCd( b44ListBean.getStrGakubuCd() );
						b44ListBeanGKBAddCnt.setStrGakubuMei( b44ListBean.getStrGakubuMei() );
						b44ListBeanGKBAddCnt.setStrGakkaCd( "NTI-GKB" );
						b44ListBeanGKBAddCnt.setStrGakkaMei( "学部計" );
						b44ListBeanGKBAddCnt.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanGKBAddCnt.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanGKBAddCnt.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListGKBAdd = deepCopyCl( b44GakkoListGKBAddCnt );
						centerGKBFlg = true;
						break;
					case 2:
						b44ListBeanGKBAddSnd.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanGKBAddSnd.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanGKBAddSnd.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanGKBAddSnd.setStrGakubuCd( b44ListBean.getStrGakubuCd() );
						b44ListBeanGKBAddSnd.setStrGakubuMei( b44ListBean.getStrGakubuMei() );
						b44ListBeanGKBAddSnd.setStrGakkaCd( "NTI-GKB" );
						b44ListBeanGKBAddSnd.setStrGakkaMei( "学部計" );
						b44ListBeanGKBAddSnd.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanGKBAddSnd.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanGKBAddSnd.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListGKBAdd = deepCopyCl( b44GakkoListGKBAddSnd );
						secondGKBFlg = true;
						break;
					case 3:
						b44ListBeanGKBAddTtl.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanGKBAddTtl.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanGKBAddTtl.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanGKBAddTtl.setStrGakubuCd( b44ListBean.getStrGakubuCd() );
						b44ListBeanGKBAddTtl.setStrGakubuMei( b44ListBean.getStrGakubuMei() );
						b44ListBeanGKBAddTtl.setStrGakkaCd( "NTI-GKB" );
						b44ListBeanGKBAddTtl.setStrGakkaMei( "学部計" );
						b44ListBeanGKBAddTtl.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanGKBAddTtl.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanGKBAddTtl.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListGKBAdd = deepCopyCl( b44GakkoListGKBAddTtl );
						totalGKBFlg = true;
						break;
				}

				// 大学計値の保存部
				switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
					case 1:
						b44ListBeanDGKAddCnt.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanDGKAddCnt.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanDGKAddCnt.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanDGKAddCnt.setStrGakubuCd( "NTI-DGK" );
						b44ListBeanDGKAddCnt.setStrGakubuMei( "大学計" );
						b44ListBeanDGKAddCnt.setStrGakkaCd( "" );
						b44ListBeanDGKAddCnt.setStrGakkaMei( "" );
						b44ListBeanDGKAddCnt.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanDGKAddCnt.setStrNtiMei(  b44ListBean.getStrNtiMei() );
						b44ListBeanDGKAddCnt.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListDGKAdd = deepCopyCl( b44GakkoListDGKAddCnt );
						centerDGKFlg = true;
						break;
					case 2:
						b44ListBeanDGKAddSnd.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanDGKAddSnd.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanDGKAddSnd.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanDGKAddSnd.setStrGakubuCd( "NTI-DGK" );
						b44ListBeanDGKAddSnd.setStrGakubuMei( "大学計" );
						b44ListBeanDGKAddSnd.setStrGakkaCd( "" );
						b44ListBeanDGKAddSnd.setStrGakkaMei( "" );
						b44ListBeanDGKAddSnd.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanDGKAddSnd.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanDGKAddSnd.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListDGKAdd = deepCopyCl( b44GakkoListDGKAddSnd );
						secondDGKFlg = true;
						break;
					case 3:
						b44ListBeanDGKAddTtl.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanDGKAddTtl.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanDGKAddTtl.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanDGKAddTtl.setStrGakubuCd( "NTI-DGK" );
						b44ListBeanDGKAddTtl.setStrGakubuMei( "大学計" );
						b44ListBeanDGKAddTtl.setStrGakkaCd( "" );
						b44ListBeanDGKAddTtl.setStrGakkaMei( "" );
						b44ListBeanDGKAddTtl.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanDGKAddTtl.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanDGKAddTtl.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListDGKAdd = deepCopyCl( b44GakkoListDGKAddTtl );
						totalDGKFlg = true;
						break;
				}

				// 学科計値の保存部
				if( cm.toString(b44ListBean.getStrNtiCd()).equals("D") ){		 // 前期
					switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
						case 1:
						b44ListBeanNTIAddZenCnt.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddZenCnt.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddZenCnt.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddZenCnt.setStrGakubuCd( "NTI-ZEN" );
						b44ListBeanNTIAddZenCnt.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddZenCnt.setStrGakkaCd( "" );
						b44ListBeanNTIAddZenCnt.setStrGakkaMei( "" );
						b44ListBeanNTIAddZenCnt.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddZenCnt.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddZenCnt.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListNTIAdd = deepCopyCl( b44GakkoListNTIAddZenCnt );
						zenCenterNTIFlg = true;
							break;
						case 2:
						b44ListBeanNTIAddZenSnd.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddZenSnd.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddZenSnd.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddZenSnd.setStrGakubuCd( "NTI-ZEN" );
						b44ListBeanNTIAddZenSnd.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddZenSnd.setStrGakkaCd( "" );
						b44ListBeanNTIAddZenSnd.setStrGakkaMei( "" );
						b44ListBeanNTIAddZenSnd.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddZenSnd.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddZenSnd.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListDGKAdd = deepCopyCl( b44GakkoListNTIAddZenSnd );
						zenSecondNTIFlg = true;
							break;
						case 3:
						b44ListBeanNTIAddZenTtl.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddZenTtl.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddZenTtl.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddZenTtl.setStrGakubuCd( "NTI-ZEN" );
						b44ListBeanNTIAddZenTtl.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddZenTtl.setStrGakkaCd( "" );
						b44ListBeanNTIAddZenTtl.setStrGakkaMei( "" );
						b44ListBeanNTIAddZenTtl.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddZenTtl.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddZenTtl.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListDGKAdd = deepCopyCl( b44GakkoListNTIAddZenTtl );
						zenTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(b44ListBean.getStrNtiCd()).equals("E") ){	 // 中期
					switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
						case 1:
						b44ListBeanNTIAddChuCnt.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddChuCnt.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddChuCnt.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddChuCnt.setStrGakubuCd( "NTI-CHU" );
						b44ListBeanNTIAddChuCnt.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddChuCnt.setStrGakkaCd( "" );
						b44ListBeanNTIAddChuCnt.setStrGakkaMei( "" );
						b44ListBeanNTIAddChuCnt.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddChuCnt.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddChuCnt.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListNTIAdd = deepCopyCl( b44GakkoListNTIAddChuCnt );
						chuCenterNTIFlg = true;
							break;
						case 2:
						b44ListBeanNTIAddChuSnd.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddChuSnd.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddChuSnd.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddChuSnd.setStrGakubuCd( "NTI-CHU" );
						b44ListBeanNTIAddChuSnd.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddChuSnd.setStrGakkaCd( "" );
						b44ListBeanNTIAddChuSnd.setStrGakkaMei( "" );
						b44ListBeanNTIAddChuSnd.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddChuSnd.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddChuSnd.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListDGKAdd = deepCopyCl( b44GakkoListNTIAddChuSnd );
						chuSecondNTIFlg = true;
							break;
						case 3:
						b44ListBeanNTIAddChuTtl.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddChuTtl.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddChuTtl.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddChuTtl.setStrGakubuCd( "NTI-CHU" );
						b44ListBeanNTIAddChuTtl.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddChuTtl.setStrGakkaCd( "" );
						b44ListBeanNTIAddChuTtl.setStrGakkaMei( "" );
						b44ListBeanNTIAddChuTtl.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddChuTtl.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddChuTtl.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListDGKAdd = deepCopyCl( b44GakkoListNTIAddChuTtl );
						chuTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(b44ListBean.getStrNtiCd()).equals("C") ){	 // 後期
					switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
						case 1:
						b44ListBeanNTIAddKokCnt.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddKokCnt.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddKokCnt.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddKokCnt.setStrGakubuCd( "NTI-KOK" );
						b44ListBeanNTIAddKokCnt.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddKokCnt.setStrGakkaCd( "" );
						b44ListBeanNTIAddKokCnt.setStrGakkaMei( "" );
						b44ListBeanNTIAddKokCnt.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddKokCnt.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddKokCnt.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListNTIAdd = deepCopyCl( b44GakkoListNTIAddKokCnt );
						kokCenterNTIFlg = true;
							break;
						case 2:
						b44ListBeanNTIAddKokSnd.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddKokSnd.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddKokSnd.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddKokSnd.setStrGakubuCd( "NTI-KOK" );
						b44ListBeanNTIAddKokSnd.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddKokSnd.setStrGakkaCd( "" );
						b44ListBeanNTIAddKokSnd.setStrGakkaMei( "" );
						b44ListBeanNTIAddKokSnd.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddKokSnd.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddKokSnd.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListDGKAdd = deepCopyCl( b44GakkoListNTIAddKokSnd );
						kokSecondNTIFlg = true;
							break;
						case 3:
						b44ListBeanNTIAddKokTtl.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddKokTtl.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddKokTtl.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddKokTtl.setStrGakubuCd( "NTI-KOK" );
						b44ListBeanNTIAddKokTtl.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddKokTtl.setStrGakkaCd( "" );
						b44ListBeanNTIAddKokTtl.setStrGakkaMei( "" );
						b44ListBeanNTIAddKokTtl.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddKokTtl.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddKokTtl.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListDGKAdd = deepCopyCl( b44GakkoListNTIAddKokTtl );
						kokTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(b44ListBean.getStrNtiCd()).equals("N") ){	 // 一般型
					switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
						case 1:
						b44ListBeanNTIAddIpnCnt.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddIpnCnt.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddIpnCnt.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddIpnCnt.setStrGakubuCd( "NTI-IPN" );
						b44ListBeanNTIAddIpnCnt.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddIpnCnt.setStrGakkaCd( "" );
						b44ListBeanNTIAddIpnCnt.setStrGakkaMei( "" );
						b44ListBeanNTIAddIpnCnt.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddIpnCnt.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddIpnCnt.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListNTIAdd = deepCopyCl( b44GakkoListNTIAddIpnCnt );
						ipnCenterNTIFlg = true;
							break;
						case 2:
						b44ListBeanNTIAddIpnSnd.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddIpnSnd.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddIpnSnd.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddIpnSnd.setStrGakubuCd( "NTI-IPN" );
						b44ListBeanNTIAddIpnSnd.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddIpnSnd.setStrGakkaCd( "" );
						b44ListBeanNTIAddIpnSnd.setStrGakkaMei( "" );
						b44ListBeanNTIAddIpnSnd.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddIpnSnd.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddIpnSnd.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListDGKAdd = deepCopyCl( b44GakkoListNTIAddIpnSnd );
						ipnSecondNTIFlg = true;
							break;
						case 3:
						b44ListBeanNTIAddIpnTtl.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddIpnTtl.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddIpnTtl.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddIpnTtl.setStrGakubuCd( "NTI-IPN" );
						b44ListBeanNTIAddIpnTtl.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddIpnTtl.setStrGakkaCd( "" );
						b44ListBeanNTIAddIpnTtl.setStrGakkaMei( "" );
						b44ListBeanNTIAddIpnTtl.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddIpnTtl.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddIpnTtl.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListDGKAdd = deepCopyCl( b44GakkoListNTIAddIpnTtl );
						ipnTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(b44ListBean.getStrNtiCd()).equals("1") ){	 // センター型
					switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
						case 1:
						b44ListBeanNTIAddCenCnt.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddCenCnt.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddCenCnt.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddCenCnt.setStrGakubuCd( "NTI-CEN" );
						b44ListBeanNTIAddCenCnt.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddCenCnt.setStrGakkaCd( "" );
						b44ListBeanNTIAddCenCnt.setStrGakkaMei( "" );
						b44ListBeanNTIAddCenCnt.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddCenCnt.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddCenCnt.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListNTIAdd = deepCopyCl( b44GakkoListNTIAddCenCnt );
						cenCenterNTIFlg = true;
							break;
						case 2:
						b44ListBeanNTIAddCenSnd.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddCenSnd.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddCenSnd.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddCenSnd.setStrGakubuCd( "NTI-CEN" );
						b44ListBeanNTIAddCenSnd.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddCenSnd.setStrGakkaCd( "" );
						b44ListBeanNTIAddCenSnd.setStrGakkaMei( "" );
						b44ListBeanNTIAddCenSnd.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddCenSnd.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddCenSnd.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListDGKAdd = deepCopyCl( b44GakkoListNTIAddCenSnd );
						cenSecondNTIFlg = true;
							break;
						case 3:
						b44ListBeanNTIAddCenTtl.setStrGenKouKbn( b44ListBean.getStrGenKouKbn() );
						b44ListBeanNTIAddCenTtl.setStrDaigakuCd( b44ListBean.getStrDaigakuCd() );
						b44ListBeanNTIAddCenTtl.setStrDaigakuMei( b44ListBean.getStrDaigakuMei() );
						b44ListBeanNTIAddCenTtl.setStrGakubuCd( "NTI-CEN" );
						b44ListBeanNTIAddCenTtl.setStrGakubuMei( cm.toString(b44ListBean.getStrNtiMei()) + "計" );
						b44ListBeanNTIAddCenTtl.setStrGakkaCd( "" );
						b44ListBeanNTIAddCenTtl.setStrGakkaMei( "" );
						b44ListBeanNTIAddCenTtl.setStrNtiCd( b44ListBean.getStrNtiCd() );
						b44ListBeanNTIAddCenTtl.setStrNtiMei( b44ListBean.getStrNtiMei() );
						b44ListBeanNTIAddCenTtl.setStrHyoukaKbn( b44ListBean.getStrHyoukaKbn() );
						b44GakkoListDGKAdd = deepCopyCl( b44GakkoListNTIAddCenTtl );
						cenTotalNTIFlg = true;
							break;
					}
				}

				//
				ArrayList b44GakkoList		= b44ListBean.getB44GakkoList();
				ArrayList b44GakkoListEdt	= new ArrayList();
				ListIterator itrClass = b44GakkoList.listIterator();
				int b = 0;
				boolean firstFlgGKB3 = true;
				boolean firstFlgDGK3 = true;
				boolean firstFlgNTI3 = true;
				if(b44GakkoListGKBAdd.size() == 0){
					firstFlgGKB3 = false;
				}
				if(b44GakkoListDGKAdd.size() == 0){
					firstFlgDGK3 = false;
				}
				if(b44GakkoListNTIAdd.size() == 0){
					firstFlgNTI3 = false;
				}
				/** 学校・学年・クラス **/
				while( itrClass.hasNext() ){
					B44GakkoListBean b44GakkoListBean = (B44GakkoListBean)itrClass.next();
					B44GakkoListBean b44GakkoListBeanEdt = new B44GakkoListBean();
					B44GakkoListBean b44GakkoListBeanDGKAdd = null;
					B44GakkoListBean b44GakkoListBeanGKBAdd = null;
					B44GakkoListBean b44GakkoListBeanNTIAdd = null;

					// 移し変え
					b44GakkoListBeanEdt.setStrGakkomei( b44GakkoListBean.getStrGakkomei() );

					// 大学計用データ呼び出し
					if(firstFlgDGK3 == true){
						b44GakkoListBeanDGKAdd = (B44GakkoListBean) b44GakkoListDGKAdd.get(b);
						b44NinzuListDGKAdd = deepCopyNz( b44GakkoListBeanDGKAdd.getB44HyoukaNinzuList() );
					}else{
						b44GakkoListBeanDGKAdd = new B44GakkoListBean();

						b44GakkoListBeanDGKAdd.setStrGakkomei( b44GakkoListBean.getStrGakkomei() );
					}

					// 学部計用データ呼び出し
					if(firstFlgGKB3 == true){
						b44GakkoListBeanGKBAdd = (B44GakkoListBean) b44GakkoListGKBAdd.get(b);
						b44NinzuListGKBAdd = deepCopyNz( b44GakkoListBeanGKBAdd.getB44HyoukaNinzuList() );
					}else{
						b44GakkoListBeanGKBAdd = new B44GakkoListBean();

						b44GakkoListBeanGKBAdd.setStrGakkomei( b44GakkoListBean.getStrGakkomei() );
					}

					// 日程計用データ呼び出し
					if(firstFlgNTI3 == true){
						b44GakkoListBeanNTIAdd = (B44GakkoListBean) b44GakkoListNTIAdd.get(b);
						b44NinzuListNTIAdd = deepCopyNz( b44GakkoListBeanNTIAdd.getB44HyoukaNinzuList() );
					}else{
						b44GakkoListBeanNTIAdd = new B44GakkoListBean();

						b44GakkoListBeanNTIAdd.setStrGakkomei( b44GakkoListBean.getStrGakkomei() );
					}

					/** 年度・人数 **/
					ArrayList b44NinzuList = b44GakkoListBean.getB44HyoukaNinzuList();
					ArrayList b44NinzuListEdt = new ArrayList();
					ListIterator itrNinzu = b44NinzuList.listIterator();
					int a = 0;
					boolean firstFlgDGK2 = true;
					boolean firstFlgGKB2 = true;
					boolean firstFlgNTI2 = true;
					if(b44NinzuListDGKAdd.size() == 0){
						firstFlgDGK2 = false;
					}
					if(b44NinzuListGKBAdd.size() == 0){
						firstFlgGKB2 = false;
					}
					if(b44NinzuListNTIAdd.size() == 0){
						firstFlgNTI2 = false;
					}
					while( itrNinzu.hasNext() ){
						B44HyoukaNinzuListBean b44NinzuBean = (B44HyoukaNinzuListBean)itrNinzu.next();
						B44HyoukaNinzuListBean b44NinzuBeanEdt = new B44HyoukaNinzuListBean();
						B44HyoukaNinzuListBean b44NinzuBeanDGKAdd = null;
						B44HyoukaNinzuListBean b44NinzuBeanGKBAdd = null;
						B44HyoukaNinzuListBean b44NinzuBeanNTIAdd = null;

						// 移し変え
						b44NinzuBeanEdt.setStrNendo( b44NinzuBean.getStrNendo() );
						b44NinzuBeanEdt.setIntSouShibo( b44NinzuBean.getIntSouShibo() );
						b44NinzuBeanEdt.setIntDai1Shibo( b44NinzuBean.getIntDai1Shibo() );
						b44NinzuBeanEdt.setIntHyoukaA( b44NinzuBean.getIntHyoukaA() );
						b44NinzuBeanEdt.setIntHyoukaB( b44NinzuBean.getIntHyoukaB() );
						b44NinzuBeanEdt.setIntHyoukaC( b44NinzuBean.getIntHyoukaC() );

						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//b44NinzuBeanEdt.setIntHyoukaA_Hukumu( b44NinzuBean.getIntHyoukaA_Hukumu() );
						//b44NinzuBeanEdt.setIntHyoukaB_Hukumu( b44NinzuBean.getIntHyoukaB_Hukumu() );
						//b44NinzuBeanEdt.setIntHyoukaC_Hukumu( b44NinzuBean.getIntHyoukaC_Hukumu() );
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL END

						b44NinzuListEdt.add( b44NinzuBeanEdt );

						// 加算処理(大学計用)
						if(firstFlgDGK2 == true){
							b44NinzuBeanDGKAdd = (B44HyoukaNinzuListBean) b44NinzuListDGKAdd.get(a);
						}else{
							b44NinzuBeanDGKAdd = new B44HyoukaNinzuListBean();

							b44NinzuBeanDGKAdd.setStrNendo( b44NinzuBean.getStrNendo() );
//							b44NinzuBeanDGKAdd.setIntSouShibo( 0 );
//							b44NinzuBeanDGKAdd.setIntDai1Shibo( 0 );
//							b44NinzuBeanDGKAdd.setIntHyoukaA( 0 );
//							b44NinzuBeanDGKAdd.setIntHyoukaB( 0 );
//							b44NinzuBeanDGKAdd.setIntHyoukaC( 0 );
						}

						if(b44NinzuBean.getIntSouShibo()!=-999){
//							b44NinzuBeanDGKAdd.setIntSouShibo( b44NinzuBeanDGKAdd.getIntSouShibo() + b44NinzuBean.getIntSouShibo() );
							if(b44NinzuBeanDGKAdd.getIntSouShibo()!=-999){
								b44NinzuBeanDGKAdd.setIntSouShibo(b44NinzuBeanDGKAdd.getIntSouShibo() + b44NinzuBean.getIntSouShibo());
							}else{
								b44NinzuBeanDGKAdd.setIntSouShibo(b44NinzuBean.getIntSouShibo());
							}
						}
						if(b44NinzuBean.getIntDai1Shibo()!=-999){
//							b44NinzuBeanDGKAdd.setIntDai1Shibo( b44NinzuBeanDGKAdd.getIntDai1Shibo() + b44NinzuBean.getIntDai1Shibo() );
							if(b44NinzuBeanDGKAdd.getIntDai1Shibo()!=-999){
								b44NinzuBeanDGKAdd.setIntDai1Shibo(b44NinzuBeanDGKAdd.getIntDai1Shibo() + b44NinzuBean.getIntDai1Shibo());
							}else{
								b44NinzuBeanDGKAdd.setIntDai1Shibo(b44NinzuBean.getIntDai1Shibo());
							}
						}
						if(b44NinzuBean.getIntHyoukaA()!=-999){
//							b44NinzuBeanDGKAdd.setIntHyoukaA( b44NinzuBeanDGKAdd.getIntHyoukaA() + b44NinzuBean.getIntHyoukaA() );
							if(b44NinzuBeanDGKAdd.getIntHyoukaA()!=-999){
								b44NinzuBeanDGKAdd.setIntHyoukaA(b44NinzuBeanDGKAdd.getIntHyoukaA() + b44NinzuBean.getIntHyoukaA());
							}else{
								b44NinzuBeanDGKAdd.setIntHyoukaA(b44NinzuBean.getIntHyoukaA());
							}
						}

						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(b44NinzuBean.getIntHyoukaA_Hukumu()!=-999){
						//	if(b44NinzuBeanDGKAdd.getIntHyoukaA_Hukumu()!=-999){
						//		b44NinzuBeanDGKAdd.setIntHyoukaA_Hukumu(b44NinzuBeanDGKAdd.getIntHyoukaA_Hukumu() + b44NinzuBean.getIntHyoukaA_Hukumu());
						//	}else{
						//		b44NinzuBeanDGKAdd.setIntHyoukaA_Hukumu(b44NinzuBean.getIntHyoukaA_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL END

						if(b44NinzuBean.getIntHyoukaB()!=-999){
//							b44NinzuBeanDGKAdd.setIntHyoukaB( b44NinzuBeanDGKAdd.getIntHyoukaB() + b44NinzuBean.getIntHyoukaB() );
							if(b44NinzuBeanDGKAdd.getIntHyoukaB()!=-999){
								b44NinzuBeanDGKAdd.setIntHyoukaB(b44NinzuBeanDGKAdd.getIntHyoukaB() + b44NinzuBean.getIntHyoukaB());
							}else{
								b44NinzuBeanDGKAdd.setIntHyoukaB(b44NinzuBean.getIntHyoukaB());
							}
						}

						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(b44NinzuBean.getIntHyoukaB_Hukumu()!=-999){
						//	if(b44NinzuBeanDGKAdd.getIntHyoukaB_Hukumu()!=-999){
						//		b44NinzuBeanDGKAdd.setIntHyoukaB_Hukumu(b44NinzuBeanDGKAdd.getIntHyoukaB_Hukumu() + b44NinzuBean.getIntHyoukaB_Hukumu());
						//	}else{
						//		b44NinzuBeanDGKAdd.setIntHyoukaB_Hukumu(b44NinzuBean.getIntHyoukaB_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL END

						if(b44NinzuBean.getIntHyoukaC()!=-999){
//							b44NinzuBeanDGKAdd.setIntHyoukaC( b44NinzuBeanDGKAdd.getIntHyoukaC() + b44NinzuBean.getIntHyoukaC() );
							if(b44NinzuBeanDGKAdd.getIntHyoukaC()!=-999){
								b44NinzuBeanDGKAdd.setIntHyoukaC(b44NinzuBeanDGKAdd.getIntHyoukaC() + b44NinzuBean.getIntHyoukaC());
							}else{
								b44NinzuBeanDGKAdd.setIntHyoukaC(b44NinzuBean.getIntHyoukaC());
							}
						}

						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(b44NinzuBean.getIntHyoukaC_Hukumu()!=-999){
						//	if(b44NinzuBeanDGKAdd.getIntHyoukaC_Hukumu()!=-999){
						//		b44NinzuBeanDGKAdd.setIntHyoukaC_Hukumu(b44NinzuBeanDGKAdd.getIntHyoukaC_Hukumu() + b44NinzuBean.getIntHyoukaC_Hukumu());
						//	}else{
						//		b44NinzuBeanDGKAdd.setIntHyoukaC_Hukumu(b44NinzuBean.getIntHyoukaC_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL END

						if(firstFlgDGK2 == true){
							b44NinzuListDGKAdd.set(a,b44NinzuBeanDGKAdd);
						}else{
							b44NinzuListDGKAdd.add(b44NinzuBeanDGKAdd);
						}

						// 加算処理(学部計用)
						if(firstFlgGKB2 == true){
							b44NinzuBeanGKBAdd = (B44HyoukaNinzuListBean) b44NinzuListGKBAdd.get(a);
						}else{
							b44NinzuBeanGKBAdd = new B44HyoukaNinzuListBean();

							b44NinzuBeanGKBAdd.setStrNendo( b44NinzuBean.getStrNendo() );
//							b44NinzuBeanGKBAdd.setIntSouShibo( 0 );
//							b44NinzuBeanGKBAdd.setIntDai1Shibo( 0 );
//							b44NinzuBeanGKBAdd.setIntHyoukaA( 0 );
//							b44NinzuBeanGKBAdd.setIntHyoukaB( 0 );
//							b44NinzuBeanGKBAdd.setIntHyoukaC( 0 );
						}

						if(b44NinzuBean.getIntSouShibo()!=-999){
//							b44NinzuBeanGKBAdd.setIntSouShibo( b44NinzuBeanGKBAdd.getIntSouShibo() + b44NinzuBean.getIntSouShibo() );
							if(b44NinzuBeanGKBAdd.getIntSouShibo()!=-999){
								b44NinzuBeanGKBAdd.setIntSouShibo(b44NinzuBeanGKBAdd.getIntSouShibo() + b44NinzuBean.getIntSouShibo());
							}else{
								b44NinzuBeanGKBAdd.setIntSouShibo(b44NinzuBean.getIntSouShibo());
							}
						}
						if(b44NinzuBean.getIntDai1Shibo()!=-999){
//							b44NinzuBeanGKBAdd.setIntDai1Shibo( b44NinzuBeanGKBAdd.getIntDai1Shibo() + b44NinzuBean.getIntDai1Shibo() );
							if(b44NinzuBeanGKBAdd.getIntDai1Shibo()!=-999){
								b44NinzuBeanGKBAdd.setIntDai1Shibo(b44NinzuBeanGKBAdd.getIntDai1Shibo() + b44NinzuBean.getIntDai1Shibo());
							}else{
								b44NinzuBeanGKBAdd.setIntDai1Shibo(b44NinzuBean.getIntDai1Shibo());
							}
						}
						if(b44NinzuBean.getIntHyoukaA()!=-999){
//							b44NinzuBeanGKBAdd.setIntHyoukaA( b44NinzuBeanGKBAdd.getIntHyoukaA() + b44NinzuBean.getIntHyoukaA() );
							if(b44NinzuBeanGKBAdd.getIntHyoukaA()!=-999){
								b44NinzuBeanGKBAdd.setIntHyoukaA(b44NinzuBeanGKBAdd.getIntHyoukaA() + b44NinzuBean.getIntHyoukaA());
							}else{
								b44NinzuBeanGKBAdd.setIntHyoukaA(b44NinzuBean.getIntHyoukaA());
							}
						}

						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(b44NinzuBean.getIntHyoukaA_Hukumu()!=-999){
						//	if(b44NinzuBeanGKBAdd.getIntHyoukaA_Hukumu()!=-999){
						//		b44NinzuBeanGKBAdd.setIntHyoukaA_Hukumu(b44NinzuBeanGKBAdd.getIntHyoukaA_Hukumu() + b44NinzuBean.getIntHyoukaA_Hukumu());
						//	}else{
						//		b44NinzuBeanGKBAdd.setIntHyoukaA_Hukumu(b44NinzuBean.getIntHyoukaA_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL END

						if(b44NinzuBean.getIntHyoukaB()!=-999){
//							b44NinzuBeanGKBAdd.setIntHyoukaB( b44NinzuBeanGKBAdd.getIntHyoukaB() + b44NinzuBean.getIntHyoukaB() );
							if(b44NinzuBeanGKBAdd.getIntHyoukaB()!=-999){
								b44NinzuBeanGKBAdd.setIntHyoukaB(b44NinzuBeanGKBAdd.getIntHyoukaB() + b44NinzuBean.getIntHyoukaB());
							}else{
								b44NinzuBeanGKBAdd.setIntHyoukaB(b44NinzuBean.getIntHyoukaB());
							}
						}

						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(b44NinzuBean.getIntHyoukaB_Hukumu()!=-999){
						//	if(b44NinzuBeanGKBAdd.getIntHyoukaB_Hukumu()!=-999){
						//		b44NinzuBeanGKBAdd.setIntHyoukaB_Hukumu(b44NinzuBeanGKBAdd.getIntHyoukaB_Hukumu() + b44NinzuBean.getIntHyoukaB_Hukumu());
						//	}else{
						//		b44NinzuBeanGKBAdd.setIntHyoukaB_Hukumu(b44NinzuBean.getIntHyoukaB_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL END

						if(b44NinzuBean.getIntHyoukaC()!=-999){
//							b44NinzuBeanGKBAdd.setIntHyoukaC( b44NinzuBeanGKBAdd.getIntHyoukaC() + b44NinzuBean.getIntHyoukaC() );
							if(b44NinzuBeanGKBAdd.getIntHyoukaC()!=-999){
								b44NinzuBeanGKBAdd.setIntHyoukaC(b44NinzuBeanGKBAdd.getIntHyoukaC() + b44NinzuBean.getIntHyoukaC());
							}else{
								b44NinzuBeanGKBAdd.setIntHyoukaC(b44NinzuBean.getIntHyoukaC());
							}
						}

						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(b44NinzuBean.getIntHyoukaC_Hukumu()!=-999){
						//	if(b44NinzuBeanGKBAdd.getIntHyoukaC_Hukumu()!=-999){
						//		b44NinzuBeanGKBAdd.setIntHyoukaC_Hukumu(b44NinzuBeanGKBAdd.getIntHyoukaC_Hukumu() + b44NinzuBean.getIntHyoukaC_Hukumu());
						//	}else{
						//		b44NinzuBeanGKBAdd.setIntHyoukaC_Hukumu(b44NinzuBean.getIntHyoukaC_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL END

						if(firstFlgGKB2 == true){
							b44NinzuListGKBAdd.set(a,b44NinzuBeanGKBAdd);
						}else{
							b44NinzuListGKBAdd.add(b44NinzuBeanGKBAdd);
						}

						// 加算処理(日程計用)
						if(firstFlgNTI2 == true){
							b44NinzuBeanNTIAdd = (B44HyoukaNinzuListBean) b44NinzuListNTIAdd.get(a);
						}else{
							b44NinzuBeanNTIAdd = new B44HyoukaNinzuListBean();

							b44NinzuBeanNTIAdd.setStrNendo( b44NinzuBean.getStrNendo() );
//							b44NinzuBeanNTIAdd.setIntSouShibo( 0 );
//							b44NinzuBeanNTIAdd.setIntDai1Shibo( 0 );
//							b44NinzuBeanNTIAdd.setIntHyoukaA( 0 );
//							b44NinzuBeanNTIAdd.setIntHyoukaB( 0 );
//							b44NinzuBeanNTIAdd.setIntHyoukaC( 0 );
						}

						if(b44NinzuBean.getIntSouShibo()!=-999){
//							b44NinzuBeanNTIAdd.setIntSouShibo( b44NinzuBeanNTIAdd.getIntSouShibo() + b44NinzuBean.getIntSouShibo() );
							if(b44NinzuBeanNTIAdd.getIntSouShibo()!=-999){
								b44NinzuBeanNTIAdd.setIntSouShibo(b44NinzuBeanNTIAdd.getIntSouShibo() + b44NinzuBean.getIntSouShibo());
							}else{
								b44NinzuBeanNTIAdd.setIntSouShibo(b44NinzuBean.getIntSouShibo());
							}
						}
						if(b44NinzuBean.getIntDai1Shibo()!=-999){
//							b44NinzuBeanNTIAdd.setIntDai1Shibo( b44NinzuBeanNTIAdd.getIntDai1Shibo() + b44NinzuBean.getIntDai1Shibo() );
							if(b44NinzuBeanNTIAdd.getIntHyoukaA()!=-999){
								b44NinzuBeanNTIAdd.setIntHyoukaA(b44NinzuBeanNTIAdd.getIntHyoukaA() + b44NinzuBean.getIntHyoukaA());
							}else{
								b44NinzuBeanNTIAdd.setIntHyoukaA(b44NinzuBean.getIntHyoukaA());
							}
						}
						if(b44NinzuBean.getIntHyoukaA()!=-999){
//							b44NinzuBeanNTIAdd.setIntHyoukaA( b44NinzuBeanNTIAdd.getIntHyoukaA() + b44NinzuBean.getIntHyoukaA() );
							if(b44NinzuBeanNTIAdd.getIntHyoukaA()!=-999){
								b44NinzuBeanNTIAdd.setIntHyoukaA(b44NinzuBeanNTIAdd.getIntHyoukaA() + b44NinzuBean.getIntHyoukaA());
							}else{
								b44NinzuBeanNTIAdd.setIntHyoukaA(b44NinzuBean.getIntHyoukaA());
							}
						}

						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(b44NinzuBean.getIntHyoukaA_Hukumu()!=-999){
						//	if(b44NinzuBeanNTIAdd.getIntHyoukaA_Hukumu()!=-999){
						//		b44NinzuBeanNTIAdd.setIntHyoukaA_Hukumu(b44NinzuBeanNTIAdd.getIntHyoukaA_Hukumu() + b44NinzuBean.getIntHyoukaA_Hukumu());
						//	}else{
						//		b44NinzuBeanNTIAdd.setIntHyoukaA_Hukumu(b44NinzuBean.getIntHyoukaA_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL END

						if(b44NinzuBean.getIntHyoukaB()!=-999){
//							b44NinzuBeanNTIAdd.setIntHyoukaB( b44NinzuBeanNTIAdd.getIntHyoukaB() + b44NinzuBean.getIntHyoukaB() );
							if(b44NinzuBeanNTIAdd.getIntHyoukaB()!=-999){
								b44NinzuBeanNTIAdd.setIntHyoukaB(b44NinzuBeanNTIAdd.getIntHyoukaB() + b44NinzuBean.getIntHyoukaB());
							}else{
								b44NinzuBeanNTIAdd.setIntHyoukaB(b44NinzuBean.getIntHyoukaB());
							}
						}

						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(b44NinzuBean.getIntHyoukaB_Hukumu()!=-999){
						//	if(b44NinzuBeanNTIAdd.getIntHyoukaB_Hukumu()!=-999){
						//		b44NinzuBeanNTIAdd.setIntHyoukaB_Hukumu(b44NinzuBeanNTIAdd.getIntHyoukaB_Hukumu() + b44NinzuBean.getIntHyoukaB_Hukumu());
						//	}else{
						//		b44NinzuBeanNTIAdd.setIntHyoukaB_Hukumu(b44NinzuBean.getIntHyoukaB_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL END

						if(b44NinzuBean.getIntHyoukaC()!=-999){
//							b44NinzuBeanNTIAdd.setIntHyoukaC( b44NinzuBeanNTIAdd.getIntHyoukaC() + b44NinzuBean.getIntHyoukaC() );
							if(b44NinzuBeanNTIAdd.getIntHyoukaC()!=-999){
								b44NinzuBeanNTIAdd.setIntHyoukaC(b44NinzuBeanNTIAdd.getIntHyoukaC() + b44NinzuBean.getIntHyoukaC());
							}else{
								b44NinzuBeanNTIAdd.setIntHyoukaC(b44NinzuBean.getIntHyoukaC());
							}
						}

						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(b44NinzuBean.getIntHyoukaC_Hukumu()!=-999){
						//	if(b44NinzuBeanNTIAdd.getIntHyoukaC_Hukumu()!=-999){
						//		b44NinzuBeanNTIAdd.setIntHyoukaC_Hukumu(b44NinzuBeanNTIAdd.getIntHyoukaC_Hukumu() + b44NinzuBean.getIntHyoukaC_Hukumu());
						//	}else{
						//		b44NinzuBeanNTIAdd.setIntHyoukaC_Hukumu(b44NinzuBean.getIntHyoukaC_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL END

						if(firstFlgNTI2 == true){
							b44NinzuListNTIAdd.set(a,b44NinzuBeanNTIAdd);
						}else{
							b44NinzuListNTIAdd.add(b44NinzuBeanNTIAdd);
						}
						a++;
					}

					// 格納
					b44GakkoListBeanEdt.setB44HyoukaNinzuList( b44NinzuListEdt );
					b44GakkoListEdt.add( b44GakkoListBeanEdt );

					// 大学計用
					b44GakkoListBeanDGKAdd.setB44HyoukaNinzuList( deepCopyNz(b44NinzuListDGKAdd) );
					if(firstFlgDGK3 == true){
						b44GakkoListDGKAdd.set(b,b44GakkoListBeanDGKAdd);
					}else{
						b44GakkoListDGKAdd.add(b44GakkoListBeanDGKAdd);
					}
					// 学部計用
					b44GakkoListBeanGKBAdd.setB44HyoukaNinzuList( deepCopyNz(b44NinzuListGKBAdd) );
					if(firstFlgGKB3 == true){
						b44GakkoListGKBAdd.set(b,b44GakkoListBeanGKBAdd);
					}else{
						b44GakkoListGKBAdd.add(b44GakkoListBeanGKBAdd);
					}
					// 日程計用
					b44GakkoListBeanNTIAdd.setB44HyoukaNinzuList( deepCopyNz(b44NinzuListNTIAdd) );
					if(firstFlgNTI3 == true){
						b44GakkoListNTIAdd.set(b,b44GakkoListBeanNTIAdd);
					}else{
						b44GakkoListNTIAdd.add(b44GakkoListBeanNTIAdd);
					}

					b44NinzuListDGKAdd = new ArrayList();
					b44NinzuListGKBAdd = new ArrayList();
					b44NinzuListNTIAdd = new ArrayList();
					b++;

				}
				// 格納
				b44ListBeanEdt.setB44GakkoList( b44GakkoListEdt );
				b44ListEdt.add( b44ListBeanEdt );
				// 大学計用
				switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
					case 1:
						b44GakkoListDGKAddCnt = deepCopyCl( b44GakkoListDGKAdd );
						break;
					case 2:
						b44GakkoListDGKAddSnd = deepCopyCl( b44GakkoListDGKAdd );
						break;
					case 3:
						b44GakkoListDGKAddTtl = deepCopyCl( b44GakkoListDGKAdd );
						break;
				}
				// 学部計用
				switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
					case 1:
						b44GakkoListGKBAddCnt = deepCopyCl( b44GakkoListGKBAdd );
						break;
					case 2:
						b44GakkoListGKBAddSnd = deepCopyCl( b44GakkoListGKBAdd );
						break;
					case 3:
						b44GakkoListGKBAddTtl = deepCopyCl( b44GakkoListGKBAdd );
						break;
				}
				// 日程計用
				if( cm.toString(b44ListBean.getStrNtiCd()).equals("D") ){		 // 前期
					switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
						case 1:
							b44GakkoListNTIAddZenCnt = deepCopyCl( b44GakkoListNTIAdd );
							break;
						case 2:
							b44GakkoListNTIAddZenSnd = deepCopyCl( b44GakkoListNTIAdd );
							break;
						case 3:
							b44GakkoListNTIAddZenTtl = deepCopyCl( b44GakkoListNTIAdd );
							break;
					}
				}else if( cm.toString(b44ListBean.getStrNtiCd()).equals("E") ){	 // 中期
					switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
						case 1:
							b44GakkoListNTIAddChuCnt = deepCopyCl( b44GakkoListNTIAdd );
							break;
						case 2:
							b44GakkoListNTIAddChuSnd = deepCopyCl( b44GakkoListNTIAdd );
							break;
						case 3:
							b44GakkoListNTIAddChuTtl = deepCopyCl( b44GakkoListNTIAdd );
							break;
					}
				}else if( cm.toString(b44ListBean.getStrNtiCd()).equals("C") ){	 // 後期
					switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
						case 1:
							b44GakkoListNTIAddKokCnt = deepCopyCl( b44GakkoListNTIAdd );
							break;
						case 2:
							b44GakkoListNTIAddKokSnd = deepCopyCl( b44GakkoListNTIAdd );
							break;
						case 3:
							b44GakkoListNTIAddKokTtl = deepCopyCl( b44GakkoListNTIAdd );
							break;
					}
				}else if( cm.toString(b44ListBean.getStrNtiCd()).equals("N") ){	 // 一般型
					switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
						case 1:
							b44GakkoListNTIAddIpnCnt = deepCopyCl( b44GakkoListNTIAdd );
							break;
						case 2:
							b44GakkoListNTIAddIpnSnd = deepCopyCl( b44GakkoListNTIAdd );
							break;
						case 3:
							b44GakkoListNTIAddIpnTtl = deepCopyCl( b44GakkoListNTIAdd );
							break;
					}
				}else if( cm.toString(b44ListBean.getStrNtiCd()).equals("1") ){	 // センター型
					switch( Integer.parseInt(b44ListBean.getStrHyoukaKbn()) ){
						case 1:
							b44GakkoListNTIAddCenCnt = deepCopyCl( b44GakkoListNTIAdd );
							break;
						case 2:
							b44GakkoListNTIAddCenSnd = deepCopyCl( b44GakkoListNTIAdd );
							break;
						case 3:
							b44GakkoListNTIAddCenTtl = deepCopyCl( b44GakkoListNTIAdd );
							break;
					}
				}

				b44GakkoListDGKAdd = new ArrayList();
				b44GakkoListGKBAdd = new ArrayList();
				b44GakkoListNTIAdd = new ArrayList();
			}

			// 学部コードが変わる→学部計の保存
			if(centerGKBFlg == true){
				b44ListBeanGKBAddCnt.setB44GakkoList( b44GakkoListGKBAddCnt );
				b44ListEdt.add( b44ListBeanGKBAddCnt );
				b44GakkoListGKBAddCnt = new ArrayList();
				centerGKBFlg = false;
			}
			if(secondGKBFlg == true){
				b44ListBeanGKBAddSnd.setB44GakkoList( b44GakkoListGKBAddSnd );
				b44ListEdt.add( b44ListBeanGKBAddSnd );
				b44GakkoListGKBAddSnd = new ArrayList();
				secondGKBFlg = false;
			}
			if(totalGKBFlg == true){
				b44ListBeanGKBAddTtl.setB44GakkoList( b44GakkoListGKBAddTtl );
				b44ListEdt.add( b44ListBeanGKBAddTtl );
				b44GakkoListGKBAddTtl = new ArrayList();
				totalGKBFlg = false;
			}

			// 大学コードが変わる→日程計・大学計の保存
			// 日程計
			if(zenCenterNTIFlg == true){
				b44ListBeanNTIAddZenCnt.setB44GakkoList( b44GakkoListNTIAddZenCnt );
				b44ListEdt.add( b44ListBeanNTIAddZenCnt );
				b44GakkoListNTIAddZenCnt = new ArrayList();
				zenCenterNTIFlg = false;
			}
			if(zenSecondNTIFlg == true){
				b44ListBeanNTIAddZenSnd.setB44GakkoList( b44GakkoListNTIAddZenSnd );
				b44ListEdt.add( b44ListBeanNTIAddZenSnd );
				b44GakkoListNTIAddZenSnd = new ArrayList();
				zenSecondNTIFlg = false;
			}
			if(zenTotalNTIFlg == true){
				b44ListBeanNTIAddZenTtl.setB44GakkoList( b44GakkoListNTIAddZenTtl );
				b44ListEdt.add( b44ListBeanNTIAddZenTtl );
				b44GakkoListNTIAddZenTtl = new ArrayList();
				zenTotalNTIFlg = false;
			}
			if(chuCenterNTIFlg == true){
				b44ListBeanNTIAddChuCnt.setB44GakkoList( b44GakkoListNTIAddChuCnt );
				b44ListEdt.add( b44ListBeanNTIAddChuCnt );
				b44GakkoListNTIAddChuCnt = new ArrayList();
				chuCenterNTIFlg = false;
			}
			if(chuSecondNTIFlg == true){
				b44ListBeanNTIAddChuSnd.setB44GakkoList( b44GakkoListNTIAddChuSnd );
				b44ListEdt.add( b44ListBeanNTIAddChuSnd );
				b44GakkoListNTIAddChuSnd = new ArrayList();
				chuSecondNTIFlg = false;
			}
			if(chuTotalNTIFlg == true){
				b44ListBeanNTIAddChuTtl.setB44GakkoList( b44GakkoListNTIAddChuTtl );
				b44ListEdt.add( b44ListBeanNTIAddChuTtl );
				b44GakkoListNTIAddChuTtl = new ArrayList();
				chuTotalNTIFlg = false;
			}
			if(kokCenterNTIFlg == true){
				b44ListBeanNTIAddKokCnt.setB44GakkoList( b44GakkoListNTIAddKokCnt );
				b44ListEdt.add( b44ListBeanNTIAddKokCnt );
				b44GakkoListNTIAddKokCnt = new ArrayList();
				kokCenterNTIFlg = false;
			}
			if(kokSecondNTIFlg == true){
				b44ListBeanNTIAddKokSnd.setB44GakkoList( b44GakkoListNTIAddKokSnd );
				b44ListEdt.add( b44ListBeanNTIAddKokSnd );
				b44GakkoListNTIAddKokSnd = new ArrayList();
				kokSecondNTIFlg = false;
			}
			if(kokTotalNTIFlg == true){
				b44ListBeanNTIAddKokTtl.setB44GakkoList( b44GakkoListNTIAddKokTtl );
				b44ListEdt.add( b44ListBeanNTIAddKokTtl );
				b44GakkoListNTIAddKokTtl = new ArrayList();
				kokTotalNTIFlg = false;
			}
			if(ipnCenterNTIFlg == true){
				b44ListBeanNTIAddIpnCnt.setB44GakkoList( b44GakkoListNTIAddIpnCnt );
				b44ListEdt.add( b44ListBeanNTIAddIpnCnt );
				b44GakkoListNTIAddIpnCnt = new ArrayList();
				ipnCenterNTIFlg = false;
			}
			if(ipnSecondNTIFlg == true){
				b44ListBeanNTIAddIpnSnd.setB44GakkoList( b44GakkoListNTIAddIpnSnd );
				b44ListEdt.add( b44ListBeanNTIAddIpnSnd );
				b44GakkoListNTIAddIpnSnd = new ArrayList();
				ipnSecondNTIFlg = false;
			}
			if(ipnTotalNTIFlg == true){
				b44ListBeanNTIAddIpnTtl.setB44GakkoList( b44GakkoListNTIAddIpnTtl );
				b44ListEdt.add( b44ListBeanNTIAddIpnTtl );
				b44GakkoListNTIAddIpnTtl = new ArrayList();
				ipnTotalNTIFlg = false;
			}
			if(cenCenterNTIFlg == true){
				b44ListBeanNTIAddCenCnt.setB44GakkoList( b44GakkoListNTIAddCenCnt );
				b44ListEdt.add( b44ListBeanNTIAddCenCnt );
				b44GakkoListNTIAddCenCnt = new ArrayList();
				cenCenterNTIFlg = false;
			}
			if(cenSecondNTIFlg == true){
				b44ListBeanNTIAddCenSnd.setB44GakkoList( b44GakkoListNTIAddCenSnd );
				b44ListEdt.add( b44ListBeanNTIAddCenSnd );
				b44GakkoListNTIAddCenSnd = new ArrayList();
				cenSecondNTIFlg = false;
			}
			if(cenTotalNTIFlg == true){
				b44ListBeanNTIAddCenTtl.setB44GakkoList( b44GakkoListNTIAddCenTtl );
				b44ListEdt.add( b44ListBeanNTIAddCenTtl );
				b44GakkoListNTIAddCenTtl = new ArrayList();
				cenTotalNTIFlg = false;
			}

			// 大学計
			if(centerDGKFlg == true){
				b44ListBeanDGKAddCnt.setB44GakkoList( b44GakkoListDGKAddCnt );
				b44ListEdt.add( b44ListBeanDGKAddCnt );
				b44GakkoListDGKAddCnt = new ArrayList();
				centerDGKFlg = false;
			}
			if(secondDGKFlg == true){
				b44ListBeanDGKAddSnd.setB44GakkoList( b44GakkoListDGKAddSnd );
				b44ListEdt.add( b44ListBeanDGKAddSnd );
				b44GakkoListDGKAddSnd = new ArrayList();
				secondDGKFlg = false;
			}
			if(totalDGKFlg == true){
				b44ListBeanDGKAddTtl.setB44GakkoList( b44GakkoListDGKAddTtl );
				b44ListEdt.add( b44ListBeanDGKAddTtl );
				b44GakkoListDGKAddTtl = new ArrayList();
				totalDGKFlg = false;
			}

			// 格納
			b44ItemEdt.setB44List( b44ListEdt );

			return b44ItemEdt;
		}catch(Exception e){
			e.toString();
			throw e;
		}
	}

	/**
	 *
	 * @param b44NinzuList
	 * @return
	 */
	private ArrayList deepCopyNz(ArrayList b44NinzuList){
		ListIterator itrNinzu = b44NinzuList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrNinzu.hasNext()){
			B44HyoukaNinzuListBean b44NinzuBean = (B44HyoukaNinzuListBean)itrNinzu.next();
			B44HyoukaNinzuListBean b44NinzuBeanCopy = new B44HyoukaNinzuListBean();

			b44NinzuBeanCopy.setStrNendo( b44NinzuBean.getStrNendo() );
			b44NinzuBeanCopy.setIntSouShibo( b44NinzuBean.getIntSouShibo() );
			b44NinzuBeanCopy.setIntDai1Shibo( b44NinzuBean.getIntDai1Shibo() );
			b44NinzuBeanCopy.setIntHyoukaA( b44NinzuBean.getIntHyoukaA() );
			b44NinzuBeanCopy.setIntHyoukaB( b44NinzuBean.getIntHyoukaB() );
			b44NinzuBeanCopy.setIntHyoukaC( b44NinzuBean.getIntHyoukaC() );

			// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL START
			//// 2019/09/30 QQ) 共通テスト対応 ADD START
			//b44NinzuBeanCopy.setIntHyoukaA_Hukumu( b44NinzuBean.getIntHyoukaA_Hukumu() );
			//b44NinzuBeanCopy.setIntHyoukaB_Hukumu( b44NinzuBean.getIntHyoukaB_Hukumu() );
			//b44NinzuBeanCopy.setIntHyoukaC_Hukumu( b44NinzuBean.getIntHyoukaC_Hukumu() );
			//// 2019/09/30 QQ) 共通テスト対応 ADD END
			// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL END

			copyList.add( b44NinzuBeanCopy );
		}

		return copyList;
	}

	/**
	 *
	 * @param b44GakkoList
	 * @return
	 */
	private ArrayList deepCopyCl(ArrayList b44GakkoList){
		ListIterator itrClass = b44GakkoList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrClass.hasNext()){
			B44GakkoListBean b44ClassBean = (B44GakkoListBean)itrClass.next();
			B44GakkoListBean b44ClassBeanCopy = new B44GakkoListBean();

			b44ClassBeanCopy.setStrGakkomei( b44ClassBean.getStrGakkomei() );
			b44ClassBeanCopy.setB44HyoukaNinzuList( deepCopyNz(b44ClassBean.getB44HyoukaNinzuList()) );

			copyList.add( b44ClassBeanCopy );
		}

		return copyList;
	}

	/**
	 *
	 * @param b44List
	 * @return
	 */
	private ArrayList deepCopy(ArrayList b44List){
		ListIterator itr = b44List.listIterator();
		ArrayList copyList = new ArrayList();
		while(itr.hasNext()){
			B44ListBean b44Bean = (B44ListBean)itr.next();
			B44ListBean b44BeanCopy = new B44ListBean();

			b44BeanCopy.setB44GakkoList(  deepCopyCl(b44Bean.getB44GakkoList()) );
			b44BeanCopy.setStrDaigakuCd( b44Bean.getStrDaigakuCd() );
			b44BeanCopy.setStrDaigakuMei( b44Bean.getStrDaigakuMei() );
			b44BeanCopy.setStrGakkaCd( b44Bean.getStrGakkaCd() );
			b44BeanCopy.setStrGakkaMei( b44Bean.getStrGakkaMei() );
			b44BeanCopy.setStrGakubuCd( b44Bean.getStrGakubuCd() );
			b44BeanCopy.setStrGakubuMei( b44Bean.getStrGakubuMei() );
			b44BeanCopy.setStrGenKouKbn( b44Bean.getStrGenKouKbn() );
			b44BeanCopy.setStrHyoukaKbn( b44Bean.getStrHyoukaKbn() );
			b44BeanCopy.setStrNtiCd( b44Bean.getStrNtiCd() );
			b44BeanCopy.setStrNtiMei( b44Bean.getStrNtiCd() );

			copyList.add( b44BeanCopy );
		}

		return copyList;
	}

	/*
	 * 	Excel編集
	 * 		HSSFWorkbook 	workbook: Excelワークブック
	 * 		ArrayList 		b44HyoukaNinzuList:評価別人数リスト
	 * 		int				intRow:開始行
	 * 		int				intCol:開始列
	 * 		戻り値: なし
	 */
	private boolean setData(HSSFSheet workSheet, B44HyoukaNinzuListBean b44HyoukaNinzuListBean, int intRow, int intCol ){

		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		int		maxSheetIndex	= 0;
		int		row = intRow;


		// 基本ファイルを読込む
		try{

			//模試年度
			workCell = cm.setCell( workSheet, workRow, workCell, row, 4);
			workCell.setCellValue( b44HyoukaNinzuListBean.getStrNendo() + "年度" );

			//全国総志望者数
			if( b44HyoukaNinzuListBean.getIntSouShibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol);
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntSouShibo() );
			}

			//志望者数（第一志望）
			if( b44HyoukaNinzuListBean.getIntDai1Shibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 1);
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntDai1Shibo() );
			}

			//評価別人数
			if( b44HyoukaNinzuListBean.getIntHyoukaA() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 2 );
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntHyoukaA() );
			}

			// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL START
			//// 2019/09/30 QQ) 共通テスト対応 ADD START
			//if( b44HyoukaNinzuListBean.getIntHyoukaA_Hukumu() != -999 ){
			//	workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
			//	workCell.setCellValue( b44HyoukaNinzuListBean.getIntHyoukaA_Hukumu() );
			//}
			//// 2019/09/30 QQ) 共通テスト対応 ADD END
			// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL END

			if( b44HyoukaNinzuListBean.getIntHyoukaB() != -999 ){
			        // 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
				//// 2019/09/30 QQ) 共通テスト対応 UPD START
				//workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
				//// 2019/09/30 QQ) 共通テスト対応 UPD END
			        workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
			        // 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD END
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntHyoukaB() );
			}

			// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL START
			//// 2019/09/30 QQ) 共通テスト対応 ADD START
			//if( b44HyoukaNinzuListBean.getIntHyoukaB_Hukumu() != -999 ){
			//	workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 5 );
			//	workCell.setCellValue( b44HyoukaNinzuListBean.getIntHyoukaB_Hukumu() );
			//}
			//// 2019/09/30 QQ) 共通テスト対応 ADD END
			// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL END

			if( b44HyoukaNinzuListBean.getIntHyoukaC() != -999 ){
			        // 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD START
				//// 2019/09/30 QQ) 共通テスト対応 UPD START
				//workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 6 );
				//// 2019/09/30 QQ) 共通テスト対応 UPD END
			        workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
			        // 2019/11/27 QQ)nagai 英語認定試験延期対応 UPD END
				workCell.setCellValue( b44HyoukaNinzuListBean.getIntHyoukaC() );
			}

			// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL START
			//// 2019/09/30 QQ) 共通テスト対応 ADD START
			//if( b44HyoukaNinzuListBean.getIntHyoukaC_Hukumu() != -999 ){
			//	workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 7 );
			//	workCell.setCellValue( b44HyoukaNinzuListBean.getIntHyoukaC_Hukumu() );
			//}
			//// 2019/09/30 QQ) 共通テスト対応 ADD END
			// 2019/11/27 QQ)nagai 英語認定試験延期対応 DEL END

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}

		return true;
	}

}
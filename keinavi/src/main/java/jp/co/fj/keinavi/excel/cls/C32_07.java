/**
 * 校内成績分析−クラス比較　偏差値分布-人数積上げグラフ（10.0pt）
 * 	Excelファイル編集
 * 作成日: 2004/08/10
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.cls;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C32BnpListBean;
import jp.co.fj.keinavi.excel.data.cls.C32ClassListBean;
import jp.co.fj.keinavi.excel.data.cls.C32Item;
import jp.co.fj.keinavi.excel.data.cls.C32ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class C32_07 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	final private int	intMaxSheetSr	= 40;			// 最大シート数	
	final private String	masterfile0		= "C32_07";		// ファイル名
	final private String	masterfile1		= "NC32_07";	// ファイル名
	private String	masterfile		= "";					// ファイル名
	/*
	 * 	Excel編集メイン
	 * 		C32Item c32Item: データクラス
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int c32_07EditExcel(C32Item c32Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("C32_07","C32_07帳票作成開始","");
		
		//テンプレートの決定
		if (c32Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			
			// データセット
			ArrayList	c32List			= c32Item.getC32List();
			Iterator	itr				= c32List.iterator();
			int		row				= 0;	// 列
			int 		setRow			= -1;	// *セット用
			int 		ninzu			= 0;	// *作成用
			int		writeCnt		= 0;	// 書き込みカウンター
			int		sumNinzu		= 0;	// 人数計算用
			float		min				= 70.0f;// 偏差基準値
			int		classCnt		= 0;	// 値範囲：０〜９　(クラスカウンター)
			int		allClassCnt		= 0;	// 全クラスカウンター
			int		maxClassCnt		= 0;	// クラスカウンター
			int		maxSheetIndex	= 0;	// シートカウンター
			int		intBookCngCount	= 0;	// ブックカウンター
			boolean	bolFirstDataFlg	= true;// true:List.Next()を実行
			boolean	bolGakkouFlg	= true;// true:学校情報表示　（学校情報表示フラグ）
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
//add 2004/10/26 T.Sakai データ0件対応
			int		dispClassFlgCnt		= 0;		// グラフ表示フラグカウンタ
//add end
//add 2004/10/26 T.Sakai セル計算対応
			int		ruikeiNinzu			= 0;		// 累計人数
//add end

			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			// 基本ファイルを読込む
			C32ListBean c32ListBean = new C32ListBean();

			while( itr.hasNext() ) {
				c32ListBean = (C32ListBean)itr.next();
				if ( c32ListBean.getIntDispKmkFlg()==1 ) {

					// 基本ファイルを読み込む
					C32ClassListBean	c32ClassListBean	= new C32ClassListBean();
	
					//データの保持
					ArrayList	c32ClassList		= c32ListBean.getC32ClassList();
					Iterator	itrC32Class		= c32ClassList.iterator();

					// 表示クラス数取得
					maxClassCnt=0;
					while(itrC32Class.hasNext()){
						c32ClassListBean	= (C32ClassListBean) itrC32Class.next();
						if(c32ClassListBean.getIntDispClassFlg()==1){
							maxClassCnt++;
						}
					}
					itrC32Class		= c32ClassList.iterator();
	
					bolFirstDataFlg=true;
	
					//　クラスデータセット
					classCnt = 0;
					allClassCnt = 0;
					while(itrC32Class.hasNext()){
	
						if( bolBookCngFlg == true ){
							// マスタExcel読み込み
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook==null ){
								return errfread;
							}
							bolSheetCngFlg	= true;
							bolBookCngFlg	= false;
							maxSheetIndex	= 0;
						}
	
						if(bolSheetCngFlg){
							// データセットするシートの選択
							workSheet = workbook.getSheet(Integer.toString(maxSheetIndex+1));
							maxSheetIndex++;
	
							// ヘッダ右側に帳票作成日時を表示する
							cm.setHeader(workbook, workSheet);
	
							// セキュリティスタンプセット
							String secuFlg = cm.setSecurity( workbook, workSheet, c32Item.getIntSecuFlg() ,32 ,33 );
							workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
							workCell.setCellValue(secuFlg);
	
							// 学校名セット
							workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
							workCell.setCellValue( "学校名　　　：" + c32Item.getStrGakkomei() );
	
							// 対象模試セット
							String moshi =cm.setTaisyouMoshi( c32Item.getStrMshDate() );	// 模試月取得
							workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
							workCell.setCellValue( cm.getTargetExamLabel() + "　　：" + c32Item.getStrMshmei() + moshi);
	
							// 型名・配点セット
							String haiten = "";
							if ( !cm.toString(c32ListBean.getStrHaitenKmk()).equals("") ) {
								haiten = "（" + c32ListBean.getStrHaitenKmk() + "）";
							}
							workCell = cm.setCell( workSheet, workRow, workCell, 6, 0 );
							workCell.setCellValue( "型・科目：" + c32ListBean.getStrKmkmei() + haiten );
	
							bolSheetCngFlg=false;
							bolGakkouFlg=true;
						}
	
						if(bolGakkouFlg){
							c32ClassListBean	= (C32ClassListBean) c32ClassList.get(0);
							if(bolFirstDataFlg){
								c32ClassListBean	= (C32ClassListBean) itrC32Class.next();
								bolFirstDataFlg=false;
							}
						}else{
							c32ClassListBean	= (C32ClassListBean) itrC32Class.next();
						}

						// データ表示フラグ判定
						if ( c32ClassListBean.getIntDispClassFlg()==1 ) {
//add 2004/10/26 T.Sakai データ0件対応
							dispClassFlgCnt++;
//add end
							C32BnpListBean c32BnpListBean = new C32BnpListBean();
							ArrayList	c32BnpList	= c32ClassListBean.getC32BnpList();
							Iterator	itrC32Bnp	= c32BnpList.iterator();
		
							// 偏差値データセット
							row=0;
							if (c32Item.getIntShubetsuFlg() == 1){
								min = 16.0f;
							} else{
								min = 70.0f;
							}
							writeCnt=0;
							sumNinzu = 0;
//add 2004/10/26 T.Sakai セル計算対応
							ruikeiNinzu = 0;
//add end
							while(itrC32Bnp.hasNext()){
								c32BnpListBean = (C32BnpListBean) itrC32Bnp.next();
		
								if(row==0){
									if(bolGakkouFlg){
										// 高校名セット
										workCell = cm.setCell( workSheet, workRow, workCell, 41, 1 );
										workCell.setCellValue( c32Item.getStrGakkomei() );
		
										bolGakkouFlg=false;
									}else{
										// クラス名セット
										workCell = cm.setCell( workSheet, workRow, workCell, 41, 1+3*classCnt );
										workCell.setCellValue( cm.toString(c32ClassListBean.getStrGrade())+"年 "+cm.toString(c32ClassListBean.getStrClass())+"クラス" );
									}
		
									// 合計人数セット
									if(c32ClassListBean.getIntNinzu()!=-999){
										workCell = cm.setCell( workSheet, workRow, workCell, 48, 1+3*classCnt );
										workCell.setCellValue( c32ClassListBean.getIntNinzu() );
									}
		
									// 平均点セット
									if(c32ClassListBean.getFloHeikin()!=-999.0){
										workCell = cm.setCell( workSheet, workRow, workCell, 49, 1+3*classCnt );
										workCell.setCellValue( c32ClassListBean.getFloHeikin() );
									}
		
									// 平均偏差値セット
									if(c32ClassListBean.getFloHensa()!=-999.0){
										workCell = cm.setCell( workSheet, workRow, workCell, 50, 1+3*classCnt );
										workCell.setCellValue( c32ClassListBean.getFloHensa() );
									}
								}
			
								// 人数セット
								if ( c32BnpListBean.getFloBnpMin()==min ) {
									if ( c32BnpListBean.getIntNinzu() != -999 ) {
										sumNinzu = sumNinzu + c32BnpListBean.getIntNinzu();
									}
//add 2004/10/26 T.Sakai セル計算対応
									// 累計人数セット
									workCell = cm.setCell( workSheet, workRow, workCell, 43+writeCnt, 3+3*classCnt  );
									ruikeiNinzu = ruikeiNinzu + sumNinzu;
									workCell.setCellValue( ruikeiNinzu );

									// 得点用のときは処理しない
									if (c32Item.getIntShubetsuFlg() == 1){
									} else{
										if (c32BnpListBean.getFloBnpMin()==60.0f) {
											// 偏差値60以上の人数セット
											workCell = cm.setCell( workSheet, workRow, workCell, 51, 1+3*classCnt );
											workCell.setCellValue( ruikeiNinzu );
										}
										if (c32BnpListBean.getFloBnpMin()==50.0f) {
											// 偏差値50以上の人数セット
											workCell = cm.setCell( workSheet, workRow, workCell, 52, 1+3*classCnt );
											workCell.setCellValue( ruikeiNinzu );
										}
									}
//add end
									if ( sumNinzu != 0 ) {
										workCell = cm.setCell( workSheet, workRow, workCell, 43+writeCnt, 2+3*classCnt  );
										workCell.setCellValue( sumNinzu );
			
										// *セット準備
										if( sumNinzu > ninzu ){
											ninzu = sumNinzu;
											setRow = writeCnt;
										}
									}
									writeCnt++;
									sumNinzu = 0;
									if (c32Item.getIntShubetsuFlg() == 1){
										min -= 40.0f;
									} else{
										min -= 10.0f;
									}
								} else {
									if ( c32BnpListBean.getIntNinzu() != -999 ) {
										sumNinzu = sumNinzu + c32BnpListBean.getIntNinzu();
									}
			
									// *セット
									if(c32BnpListBean.getFloBnpMin() == -999.0){
//add 2004/10/26 T.Sakai セル計算対応
										// 累計人数セット
										workCell = cm.setCell( workSheet, workRow, workCell, 43+writeCnt, 3+3*classCnt  );
										ruikeiNinzu = ruikeiNinzu + sumNinzu;
										workCell.setCellValue( ruikeiNinzu );
//add end
										if ( sumNinzu != 0 ) {
											workCell = cm.setCell( workSheet, workRow, workCell, 43+writeCnt, 2+3*classCnt  );
											workCell.setCellValue( sumNinzu );
			
											// *セット準備
											if( sumNinzu > ninzu ){
												ninzu = sumNinzu;
												setRow = writeCnt;
											}
										}
										sumNinzu = 0;
		

										if(setRow!=-1){
											workCell = cm.setCell( workSheet, workRow, workCell, 43+setRow, 1+3*classCnt  );
											workCell.setCellValue("*");
										}
										ninzu=0;
										setRow=-1;
									}
								}
		
								row++;
		
							}

							classCnt++;
							allClassCnt++;
	
							// 以降にクラス情報がなければループ脱出
							if(maxClassCnt==allClassCnt){
								break;
							}
	
							// データが存在する場合
							if(classCnt>=11){
								classCnt=0;
								allClassCnt--;
								bolSheetCngFlg=true;
							}
		
							if(bolSheetCngFlg){
								//
								if((maxSheetIndex >= intMaxSheetSr)&&(itrC32Class.hasNext())){
									bolBookCngFlg = true;
								}
		
						
								if( bolBookCngFlg == true){
									boolean bolRet = false;
									// Excelファイル保存
									if(intBookCngCount==0){
										if(itr.hasNext() == true){
											bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
										}
										else{
											bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
										}
									}else{
										bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
									}
		
									if( bolRet == false ){
										return errfwrite;
									}
									intBookCngCount++;
								}
							}
						}

						// 学校が非表示指定(表示フラグ≠１)だった場合（エラー）
						bolGakkouFlg=false;
					}

	
					if(itr.hasNext()){
						classCnt=0;
						bolSheetCngFlg=true;
	
						if(maxSheetIndex >= intMaxSheetSr){
							bolBookCngFlg = true;
						}
	
					
						if( bolBookCngFlg == true){
	
							boolean bolRet = false;
							// Excelファイル保存
							if(intBookCngCount==0){
								if(itr.hasNext() == true){
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
								}
								else{
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
								}
							}else{
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
							}
	
							if( bolRet == false ){
								return errfwrite;
							}
							intBookCngCount++;
						}
					}
				}

			}
//add 2004/10/26 T.Sakai データ0件対応
			if ( dispClassFlgCnt==0 ) {
				// マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				bolSheetCngFlg	= true;
				bolBookCngFlg	= false;
				maxSheetIndex	= 0;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet( Integer.toString(maxSheetIndex+1) );
				maxSheetIndex++;
	
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
	
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, c32Item.getIntSecuFlg() ,32 ,33 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
				workCell.setCellValue(secuFlg);
	
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　　　：" + cm.toString(c32Item.getStrGakkomei()) );
	
				// 対象模試セット
				String moshi =cm.setTaisyouMoshi( c32Item.getStrMshDate() );	// 模試月取得
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "　　：" + cm.toString(c32Item.getStrMshmei()) + moshi);
			}
//add end
			if( bolBookCngFlg == false){

				boolean bolRet = false;
				// Excelファイル保存
				if(intBookCngCount!=0){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}
			
		} catch(Exception e) {
			log.Err("C32_07","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("C32_07","C32_07帳票作成終了","");
		return noerror;
	}

}
/*
 * 作成日: 2004/09/15
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.com_set.CompClassBean;
import jp.co.fj.keinavi.beans.com_set.SubjectBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.com_set.ComClassData;
import jp.co.fj.keinavi.data.com_set.ComSubjectData;
import jp.co.fj.keinavi.data.com_set.CompClassData;
import jp.co.fj.keinavi.data.com_set.CompSchoolData;
import jp.co.fj.keinavi.data.com_set.SubjectData;
import jp.co.fj.keinavi.data.com_set.cm.ClassData;
import jp.co.fj.keinavi.data.com_set.cm.Subject;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;

/**
 * プロファイルに関するユーティリティクラス
 * 
 * 2005.02.28	Yoshimoto KAWAI - Totec
 * 				担当クラスの年度を取得するメソッドを追加
 * 
 * 2005.05.29	Yoshimoto KAWAI - Totec
 * 				有効な比較対象年度のみを返すフィルタリングメソッドを追加
 * 
 * @author kawai
 */
public class ProfileUtil {
	
	/**
	 * プロファイルから比較対象高校の設定値を取得する
	 * @param profile
	 * @param graph グラフ表示対象のみを取得するかどうか
	 * @return
	 */
	public static String[] getCompSchoolValue(Profile profile, boolean graph) {
		List container = new LinkedList(); // 入れ物

		// 設定値
		List value = (List) profile.getItemMap(
			IProfileCategory.CM).get(IProfileItem.SCHOOL_COMP_SCHOOL);

		if (value != null) {
			Iterator ite = value.iterator();
			while (ite.hasNext()) {
				CompSchoolData data = (CompSchoolData) ite.next();
				
				if (graph && data.getGraphDisp() == 0) continue;
				container.add(data.getSchoolCD());
			}
		}

		return (String[]) container.toArray(new String[0]);
	}
	
	/**
	 * 比較対象クラスデータから設定値を取得する
	 * @param data
	 * @param graph グラフ表示対象のみを取得するかどうか
	 * @param mode 全選択か個別選択か
	 * @return
	 */
	private static String[] getCompClassValue(CompClassData data, boolean graph, int mode) {
		List container = null; // 設定値リスト
		
		switch (mode) {
			// 全選択
			case 1:
				container = data.getAClassData(); break;
			// 個別選択
			case 2:
				container = data.getIClassData(); break;
			// 不明
			default:
				throw new IllegalArgumentException("不正なモード指定です。");
		}

		// 設定値がなければここまで
		if (container == null) return null;

		List value = new LinkedList(); // 入れ物
		
		Iterator ite = container.iterator();
		while (ite.hasNext()) {
			ComClassData c = (ComClassData) ite.next();
			
			if (graph && c.getGraphDisp() == 0) continue;
			
			value.add(c.getKey());
		}
		
		return (String[]) value.toArray(new String[0]);	
	}
	
	/**
	 * 比較対象クラスデータから設定値を取得する（全選択）
	 * @param data
	 * @param graph グラフ表示対象のみを取得するかどうか
	 * @return
	 */
	public static String[] getCompClassAllValue(CompClassData data, boolean graph) {
		return ProfileUtil.getCompClassValue(data, graph, 1);		
	}
	
	/**
	 * 比較対象クラスデータから設定値を取得する（個別選択）
	 * @param data
	 * @param graph グラフ表示対象のみを取得するかどうか
	 * @return
	 */
	public static String[] getCompClassIndValue(CompClassData data, boolean graph) {
		return ProfileUtil.getCompClassValue(data, graph, 2);		
	}
	
	/**
	 * 型・科目データから設定値を取得する
	 * @param data
	 * @param graph グラフ表示対象のみを取得するかどうか
	 * @param mode 全選択か個別選択か
	 * @return
	 */
	private static String[] getSubjectValue(ComSubjectData data, boolean graph, int mode) {
		List container = null; // 設定値リスト
		
		switch (mode) {
			// 全選択
			case 1:
				container = data.getASubjectData(); break;
			// 個別選択
			case 2:
				container = data.getISubjectData(); break;
			// 不明
			default:
				throw new IllegalArgumentException("不正なモード指定です。");
		}

		// 設定値がなければここまで
		if (container == null) return null;

		List value = new LinkedList();	
			
		Iterator ite = container.iterator();
		while (ite.hasNext()) {
			SubjectData s = (SubjectData) ite.next();
			
			if (graph && s.getGraphDisp() == 0) continue;
			
			value.add(s.getSubjectCD());
		}
		
		return (String[]) value.toArray(new String[0]);	
	}
	
	/**
	 * 型・科目データから設定値を取得する（全選択）
	 * @param data
	 * @param graph グラフ表示対象のみを取得するかどうか
	 * @param mode 全選択か個別選択か
	 * @return
	 */
	public static String[] getSubjectAllValue(ComSubjectData data, boolean graph) {
		return ProfileUtil.getSubjectValue(data, graph, 1);
	}
	
	/**
	 * 型・科目データから設定値を取得する（個別選択）
	 * @param data
	 * @param graph グラフ表示対象のみを取得するかどうか
	 * @param mode 全選択か個別選択か
	 * @return
	 */
	public static String[] getSubjectIndValue(ComSubjectData data, boolean graph) {
		return ProfileUtil.getSubjectValue(data, graph, 2);
	}
	
	/**
	 * プロファイルから型の設定値を取得する
	 * @param profile
	 * @param examSession
	 * @param exam
	 * @return
	 */
	public static ComSubjectData getTypeValue(Profile profile, ExamSession examSession, ExamData exam, SubjectBean bean, boolean save) {
		return ProfileUtil.getSubjectValue(
			(List) profile.getItemMap(IProfileCategory.CM).get(IProfileItem.TYPE_COMMON),
			examSession,
			exam,
			bean,
			save);
	}
	
	/**
	 * プロファイルから科目の設定値を取得する
	 * @param profile
	 * @param examSession
	 * @param exam
	 * @return
	 */
	public static ComSubjectData getCourseValue(Profile profile, ExamSession examSession, ExamData exam, SubjectBean bean, boolean save) {
		return ProfileUtil.getSubjectValue(
			(List) profile.getItemMap(IProfileCategory.CM).get(IProfileItem.COURSE_COMMON),
			examSession,
			exam,
			bean,
			save);
	}
	
	/**
	 * プロファイルから型・科目の設定値を取得する
	 * @param container 設定値リスト
	 * @param examSession 模試セッション
	 * @param exam 対象模試データ
	 * @param bean 型・科目Bean
	 * @param save 設定値がなかった場合にプロファイルに保存するかどうか
	 * @return
	 */
	private static ComSubjectData getSubjectValue(List container, ExamSession es, ExamData exam, SubjectBean bean, boolean save) {
		// 模試データがなければここまで
		if (exam == null) return null;

		// 設定値がある場合
		{
			int index = container
				.indexOf(new ComSubjectData(exam.getExamYear(), exam.getExamCD()));

			if (index >= 0) return (ComSubjectData) container.get(index);
		}

		// なければ過去の同系統の模試について調べる
		{
			Iterator ite = ProfileUtil.getReverseOrderExamList(es).iterator();
			while (ite.hasNext()) {
				ExamData past = (ExamData) ite.next();	
		
				// 同系統でなければスキップ
				if (!past.getExamTypeCD().equals(exam.getExamTypeCD())) continue;
				// 過去の模試である必要がある
				if (past.getDataOpenDate().compareTo(exam.getDataOpenDate()) >= 0) continue;
		
				// 設定を取得する
				int index = container.indexOf(
					new ComSubjectData(past.getExamYear(), past.getExamCD()));

				// ある場合はコピーを作る
				if (index >= 0
				        && ((ComSubjectData) container.get(index)).getISubjectData() != null) {

				    ComSubjectData data = (ComSubjectData) container.get(index);
				
					ComSubjectData c = new ComSubjectData();
					c.setExamYear(exam.getExamYear());
					c.setExamCD(exam.getExamCD());
					c.setASubjectData(new ArrayList());
					c.setISubjectData(new ArrayList());

					Iterator it = data.getISubjectData().iterator();
					while (it.hasNext()) {
						SubjectData s = (SubjectData) it.next();
					
						// 存在するコードのみ入れる
						if (bean.getCodeSet().contains(s.getSubjectCD())) {
							c.getISubjectData().add(s.clone());
						}
					}
				
					// プロファイルに保存
					if (save) container.add(c);						

					return c;
				} 
			}
		}
		
		// さらに無ければ初期値
		{
			ComSubjectData c = new ComSubjectData();
			c.setExamYear(exam.getExamYear());
			c.setExamCD(exam.getExamCD());
			c.setASubjectData(new ArrayList());
			c.setISubjectData(new ArrayList());
			
			Iterator ite = bean.getPartList().iterator();
			while (ite.hasNext()) {
				Subject data = (Subject) ite.next();
				
				SubjectData s = new SubjectData();
				s.setSubjectCD(data.getSubCD());
				c.getISubjectData().add(s);	
			}
			
			// プロファイルに保存
			if (save) container.add(c);						
			
			return c;
		}
	}
	
	/**
	 * プロファイルから比較対象クラスの設定値を取得する
	 * @param profile プロファイル
	 * @param examSession 模試セッション
	 * @param exam 対象模試データ
	 * @param bean 比較対象クラスBean
	 * @param save 設定値がなかった場合にプロファイルに保存するかどうか
	 * @return
	 */
	public static CompClassData getCompClassValue(Profile profile, ExamSession examSession, ExamData exam, CompClassBean bean, boolean save) {
		// 模試データがなければここまで
		if (exam == null) return null;

		// 設定値リスト
		List container = (List) profile
			.getItemMap(IProfileCategory.CM)
			.get(IProfileItem.CLASS_COMP_CLASS);

		// 設定値がある場合
		{
			int index = container
				.indexOf(new CompClassData(exam.getExamYear(), exam.getExamCD()));
				
			if (index >= 0) return (CompClassData) container.get(index);
			
		}

		// なければ過去の模試について調べる
		{
			Iterator ite = ProfileUtil.getReverseOrderExamList(examSession).iterator();
			while (ite.hasNext()) {
				ExamData past = (ExamData) ite.next();	
		
				// 過去の模試である必要がある
				if (past.getDataOpenDate().compareTo(exam.getDataOpenDate()) >= 0) continue;
		
				// 設定を取得する
				int index = container
					.indexOf(new CompClassData(past.getExamYear(), past.getExamCD()));

				// ある場合はコピーを作る
				if (index >= 0
				        && ((CompClassData) container.get(index)).getIClassData() != null) {

				    CompClassData data = (CompClassData) container.get(index);
				
					CompClassData c = new CompClassData();
					c.setExamYear(exam.getExamYear());
					c.setExamCD(exam.getExamCD());
					c.setAClassData(new ArrayList());
					c.setIClassData(new ArrayList());

					Iterator it = data.getIClassData().iterator();
					while (it.hasNext()) {
						ComClassData com = (ComClassData) it.next();
					
						// 存在するクラスのみ入れる
						if (bean.getKeySet().contains(com.getKey())) {
							c.getIClassData().add(com.clone());
						}
					}

					// プロファイルに保存
					if (save) container.add(c);						
				
					return c;
				} 
			}
		}
		
		// さらに無ければ初期値
		{
			CompClassData c = new CompClassData();
			c.setExamYear(exam.getExamYear());
			c.setExamCD(exam.getExamCD());
			c.setAClassData(new ArrayList());
			c.setIClassData(new ArrayList());
			
			Iterator ite = bean.getFullList().iterator();
			while (ite.hasNext()) {
				ClassData data = (ClassData) ite.next();

				// 40クラスまで
				if (c.getIClassData().size() >= 40) break;
				// 受験生がいれば入れる			
				if (data.getExaminees() > 0)
					c.getIClassData().add(new ComClassData(data.getKey()));	
			}
			
			// プロファイルに保存
			if (save) container.add(c);						
			
			return c;
		}
	}
	
	/**
	 * 模試セッションに含まれる全ての模試データリストを
	 * データ開放日の降順でソートして返す
	 * @param examSession
	 * @return
	 */
	public static List getReverseOrderExamList(ExamSession s) {
		List container = new ArrayList(); // 入れ物
		
		// 全ての年度について取得する
		String[] years = s.getYears();
		for (int i=0; i<years.length; i++) {
			container.addAll((List) s.getExamMap().get(years[i]));
		}
		
		Collections.sort(container);

		return container;
	}
	
	/**
	 * 型・科目の共通項目設定値を取得する
	 * @param profile
	 * @param exam
	 * @param mode
	 * @return
	 */
	private static ComSubjectData getComSubjectData(Profile profile, ExamData exam, int mode) {
		List container = null;

		switch (mode) {
			// 型
			case 1:
				container = (List) profile
					.getItemMap(IProfileCategory.CM)
					.get(IProfileItem.TYPE_COMMON);
				break;
			// 科目
			case 2:
				container = (List) profile
					.getItemMap(IProfileCategory.CM)
					.get(IProfileItem.COURSE_COMMON);
				break;
		}
	
		int index = container.indexOf(
			new ComSubjectData(exam.getExamYear(), exam.getExamCD()));
		
		if (index >= 0) return (ComSubjectData) container.get(index);
		else return null;
	}
	
	/**
	 * 型の共通項目設定値を取得する
	 * @param profile
	 * @param exam
	 * @return
	 */
	public static ComSubjectData getComTypeData(Profile profile, ExamData exam) {
		return ProfileUtil.getComSubjectData(profile, exam, 1);
	}
	
	/**
	 * 科目の共通項目設定値を取得する
	 * @param profile
	 * @param exam
	 * @return
	 */
	public static ComSubjectData getComCourseData(Profile profile, ExamData exam) {
		return ProfileUtil.getComSubjectData(profile, exam, 2);
	}
	
	/**
	 * 一括出力対象をセットする
	 * @param profile
	 * @param category
	 * @param flag
	 */
	public static void setOutItem(Profile profile, String category, boolean flag) {
		profile
			.getItemMap(category)
			.put(IProfileItem.PRINT_OUT_TARGET, flag ? new Short("1") : new Short("0"));	
	}

	/**
	 * 一括出力対象をセットする
	 * @param profile
	 * @param category
	 */
	public static void setOutItem(Profile profile, String category) {
		profile
			.getItemMap(category)
			.put(IProfileItem.PRINT_OUT_TARGET, new Short("1"));	
	}

	/**
	 * 担当クラスの年度を取得する
	 * 
	 * @param profile
	 * @return
	 */
	public static String getChargeClassYear(final Profile profile) {
		
		// 担当クラスの設定値
		List container = (List) profile
			.getItemMap(IProfileCategory.CM)
			.get(IProfileItem.CLASS);

		// 現年度
		String current = KNUtil.getCurrentYear();
		// 設定値
		String year = 
			(String) profile.getItemMap(IProfileCategory.CM)
			.get(IProfileItem.CLASS_YEAR);
		
		// 設定値を評価する
		// 担当クラスの設定があり、3年以内である必要がある
		if (container.size() > 0 && year != null
			&& Integer.parseInt(year) + 2 >= Integer.parseInt(current))
				return year;

		// ここまで来たら現年度
		return current;
	}
	
	/**
	 * 有効な比較対象年度のみを返す
	 * 
	 * @param years
	 * @param exam
	 * @return
	 */
	public static String[] compYearFilter(final String[] years, final ExamData exam) {
		List container = new LinkedList();
		if (years != null && exam != null) {
			int current = Integer.parseInt(KNUtil.getCurrentYear());
			int target = Integer.parseInt(exam.getExamYear());
			for (int i=0; i<years.length; i++) {
				if (target - Integer.parseInt(years[i]) >= current - 6) {
					container.add(years[i]);
				}
			}
		}
		return (String[]) container.toArray(new String[0]);
	}
	
}

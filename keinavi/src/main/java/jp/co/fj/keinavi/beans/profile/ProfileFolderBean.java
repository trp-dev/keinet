package jp.co.fj.keinavi.beans.profile;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.data.profile.ProfileFolder;
import jp.co.fj.keinavi.util.date.DateUtil;
import jp.co.fj.keinavi.util.date.ShortDateUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応 
 * 
 * @author kawai
 *
 */
public class ProfileFolderBean extends DefaultBean {

	private LoginSession loginSession; // ログイン情報
	private String bundleCD; // 一括コード

	private List folderList = new ArrayList(); // フォルダのリスト
	private List ownFolderList; // 自分が所有しているフォルダのリスト 

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// 日付用のクラス
		DateUtil date = new ShortDateUtil();

		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			StringBuffer query = new StringBuffer();
			query.append("SELECT * FROM profilefolder f ");

			// 高校		
			if (loginSession.isSchool()) {
				query.append("WHERE f.bundlecd = ? ");
				query.append("AND (f.sectorsortingcd IS NULL OR ");
				query.append("EXISTS (");
				query.append("SELECT p.profileid FROM profile p ");
				query.append("WHERE p.profilefid = f.profilefid ");
				query.append("AND p.bundlecd = ? AND p.sectorsortingcd IS NULL");
				query.append(")) ");
				query.append("ORDER BY f.profilefname");

				ps1 = conn.prepareStatement(query.toString());
				ps1.setString(1, bundleCD); // 一括コード
				ps1.setString(2, bundleCD); // 一括コード

			// 営業部または校舎
			} else if(loginSession.isBusiness()) {
				query.append("WHERE f.sectorsortingcd = ? AND f.userid IS NULL ");
				query.append("AND f.bundlecd IS NULL ");
				query.append("ORDER BY f.profilefname");

				ps1 = conn.prepareStatement(query.toString());
				ps1.setString(1, loginSession.getSectorSortingCD()); // 部門分類コード

			// 営業部 → 高校
			} else if(loginSession.isDeputy()) {
				query.append("WHERE (f.bundlecd = ? AND f.sectorsortingcd = ?) ");
				query.append("OR (f.bundlecd = ? AND f.sectorsortingcd IS NULL) ");
				query.append("ORDER BY f.profilefname");

				ps1 = conn.prepareStatement(query.toString());
				ps1.setString(1, bundleCD); // 一括コード
				ps1.setString(2, loginSession.getSectorSortingCD()); // 部門分類コード
				ps1.setString(3, bundleCD); // 一括コード
			}

			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				ProfileFolder folder = new ProfileFolder();
				folder.setFolderID(rs1.getString(1));
				folder.setFolderName(rs1.getString(2));
				folder.setCreateDate(date.string2Date(rs1.getString(3)));
				folder.setUpdateDate(date.string2Date(rs1.getString(4)));
				folder.setComment(rs1.getString(5));
				folder.setUserID(rs1.getString(6));
				folder.setBundleCD(rs1.getString(7));
				folder.setSectorSortingCD(rs1.getString(8));
				folder.setLoginSession(loginSession);
				folderList.add(folder);
			}
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(ps1);
		}
		
		// プロファイルリストを作る
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		try {
			Query query = QueryLoader.getInstance().load("w07");
	 
			// 高校だけは営業専用プロファイルが見えてはいけない
			if (loginSession.isSchool()) {
				query.append("AND p.sectorsortingcd IS NULL ");
			}
		
			// ソート
			query.append("ORDER BY p.profilename");

			ps2 = conn.prepareStatement(query.toString());
			ps2.setString(1, loginSession.getUserID()); // 学校コード	

			Iterator ite = folderList.iterator();
			while (ite.hasNext()) {
				ProfileFolder folder = (ProfileFolder) ite.next();

				ps2.setString(2, folder.getFolderID());

				rs2 = ps2.executeQuery();
				while (rs2.next()) {
					Profile profile = new Profile();
					profile.setProfileID(rs2.getString(1));
					profile.setProfileName(rs2.getString(2));
					profile.setFolderID(rs2.getString(3));
					profile.setFolderName(folder.getFolderName());
					profile.setComment(rs2.getString(4));
					profile.setCreateDate(date.string2Date(rs2.getString(5)));
					profile.setUpdateDate(date.string2Date(rs2.getString(6)));
					profile.setOverwriteMode(rs2.getInt(7));
					profile.setUserID(rs2.getString(8));
					profile.setBundleCD(rs2.getString(9));
					profile.setSectorSortingCD(rs2.getString(10));
					profile.setUserName(rs2.getString(11));
					folder.getProfileList().add(profile);
				}
				rs2.close();
			}
		} finally {
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(ps2);
		}
	}

	/**
	 * プロファイルフォルダの要素数を返す
	 * @return
	 */
	public int getFolderListSize() {
		return folderList.size();
	}

	/**
	 * 自分が所有しているフォルダリストを返す
	 * @return
	 */
	public List getOwnFolderList() {
		if (ownFolderList == null) {
			ownFolderList = new LinkedList();
			
			Iterator ite = folderList.iterator();
			while (ite.hasNext()) {
				ProfileFolder folder = (ProfileFolder) ite.next();
						
				// 高校
				if (loginSession.isSchool()) {
					if (!folder.isDeputy()) ownFolderList.add(folder);
					
				// 営業または校舎						
				} else if (loginSession.isBusiness()) {
					ownFolderList.add(folder);

				// 営業 → 高校
				} else if (loginSession.isDeputy()) {
					if (folder.isDeputy()) ownFolderList.add(folder);
				}
			}
		}
		
		return ownFolderList;
	}

	/**
	 * @return
	 */
	public List getFolderList() {
		return folderList;
	}

	/**
	 * @param session
	 */
	public void setLoginSession(LoginSession session) {
		loginSession = session;
	}

	/**
	 * @param string
	 */
	public void setBundleCD(String string) {
		bundleCD = string;
	}

}

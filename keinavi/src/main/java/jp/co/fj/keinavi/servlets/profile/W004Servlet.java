package jp.co.fj.keinavi.servlets.profile;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.com_set.CompClassBean;
import jp.co.fj.keinavi.beans.com_set.SubjectBean;
import jp.co.fj.keinavi.beans.profile.CountingDivBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.profile.W004Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class W004Servlet extends DefaultHttpServlet {

	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// ログイン情報
		LoginSession login = super.getLoginSession(request);
		// プロファイル
		Profile profile = super.getProfile(request);
		// 模試セッション
		ExamSession examSession = super.getExamSession(request);

		// アクションフォーム
		W004Form form = (W004Form)super.getActionForm(request,
			"jp.co.fj.keinavi.forms.profile.W004Form");

		if ("w004".equals(super.getForward(request))) {
			Connection con = null; // DBコネクション
			try {
				con = super.getConnectionPool(request);

				// フォームの初期化はw003で行われているため
				// 年度は常に有効値が入る
				// 年度変更時に対象模試データがなければリストの一番上
				if (examSession.getExamData(
					form.getTargetYear(), form.getTargetExam()) == null) {
						
					List container =
						(List) examSession.getExamMap().get(form.getTargetYear());							
						
					ExamData exam = (ExamData) container.get(0);					
					form.setTargetExam(exam.getExamCD());
				}

				// 集計区分Beanを作る
				if (login.isCountingDiv()) {
					CountingDivBean c = new CountingDivBean();
					c.setConnection(null, con);
					c.setSchoolCD(login.getUserID());
					c.execute();
					request.setAttribute("CountingDivBean", c);
				}
				
				// 型Bean
				// Taglibでの評価用
				{
					SubjectBean bean = new SubjectBean(login);
					bean.setConnection(null, con);
					bean.setMode("1");
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.execute();
					request.setAttribute("TypeBean", bean);
				}

				// 科目Bean
				// Taglibでの評価用
				{
					SubjectBean bean = new SubjectBean(login);
					bean.setConnection(null, con);
					bean.setMode("2");
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.execute();
					request.setAttribute("CourseBean", bean);
				}
				
				// 比較対象クラスBean
				// Taglibでの評価用
				{
					CompClassBean bean = new CompClassBean();
					bean.setConnection(null, con);
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.setSchoolCD(login.getUserID());
					bean.execute();
					request.setAttribute("CompClassBean", bean);
				}
			} catch (Exception e) {
				throw createServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}

			request.setAttribute("form", form);

			super.forward(request, response, JSP_W004);

		// 不明なら転送
		} else {
			
			// 対象模試をプロファイルに保持する
			saveExamType(profile, examSession.getExamData(
					form.getTargetYear(), form.getTargetExam()));
			
			// デフォルト取得（同系統の最新が取れる）
			final ExamData exam = getDefaultExamData(profile, examSession);
			
			// プロファイルの対象模試を書き換え
			profile.setTargetYear(exam.getExamYear());
			profile.setTargetExam(exam.getExamCD());
			
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

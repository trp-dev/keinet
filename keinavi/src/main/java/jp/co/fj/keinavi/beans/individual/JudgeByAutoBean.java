package jp.co.fj.keinavi.beans.individual;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.kawaijuku.judgement.beans.detail.UnivDetail;
import jp.co.fj.kawaijuku.judgement.beans.judge.Judgement;
import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.data.individual.UnivKeiNavi;
import jp.co.fj.keinavi.data.individual.UnivStemmaData;
import jp.co.fj.keinavi.data.individual.WishUnivData;
import jp.co.fj.keinavi.util.individual.IPropsLoader;
import jp.co.fj.keinavi.util.individual.JudgementSorterV2;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 2004.09.23   Tomohisa YAMADA - Totec
 *              新規作成
 * 
 * 2005.03.08   Tomohisa YAMADA - Totec
 * 				[1]結果表示件数の変更
 * 
 * 2005.03.15   Tomohisa YAMADA - Totec
 * 				[2]ソート仕様の変更に伴う修正：国公立優先
 * 
 * 2005.04.07   Tomohisa YAMADA - Totec
 * 				[3]ScoreBeanの[35]に伴う修正
 * 
 * 2005.04.21   Tomohisa YAMADA - Totec
 * 				[4]分かりやすく修正
 * 
 * 2005.06.27   Tomohisa YAMADA - Totec
 * 				[5]UnivFactoryDBBranchの[17]を引き継ぐ
 * 	
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 *              センターリサーチ５段階評価対応
 *              マスタデータの持ち方変更
 *              
 * 2009.12.03	S.Hase - Totec
 * 				[7]UNIVDIVCD(１桁)→UNIDIV（２桁）変更にともない、条件分岐定数の変更
 *
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 *
 */
public class JudgeByAutoBean extends DefaultBean{
	
	// IN PARAMETER
	private List univAllList;
	private List wishUnivList;
	//private boolean isUseNewLogic;//[3] del
	private Score scoreBean;
	
	// OUT PARAMETER
	private List recordSet = new ArrayList();
	
	// INTERNAL PARAMETER
	int publicMaxNum = 0;//最大表示件数（国公立）
	int privateMaxNum = 0;//最大表示件数（私立）
	
	// 対象模試がセンタープレの場合、
	// 一般私大、短大の2次判定を＃３記述ではなく
	// センタープレの成績を優先利用するかどうか
	// インターネット模試判定において「優先しない」とするため
	// デフォルト値はfalseとする
	private boolean precedeCenterPre = false;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		List judgeTargetList = new ArrayList();//対象となるList(比較対照UnivList)
		judgeTargetList.addAll(univAllList);
		
		//■このスコアの所有者が男性なら、女子大を自動的に排除する
		if(scoreBean.isMale){
			removeFemaleSchool(judgeTargetList);
		}
		
		List finalPubList = new ArrayList();//国公立蓄積
		List finalPriList = new ArrayList();//私立蓄積
		
		if(wishUnivList != null && wishUnivList.size() > 0){
			
			//■公私大学の判別＝表示件数の設定 (01 = 国立大，02 = 公立大)
			boolean pub = false, pri = false;
			Iterator ite = wishUnivList.iterator();
			while(ite.hasNext()){
				WishUnivData d = (WishUnivData)ite.next();
				if(d.getUnivDivCd().equals("01") || d.getUnivDivCd().equals("02")) pub = true;
				else pri = true;
			}
			if(pub && pri){
				//publicMaxNum = 5;//仕様 5 //[1] del
				//privateMaxNum = 15;//仕様 15 //[1] del
				publicMaxNum = Integer.parseInt(IPropsLoader.getInstance().getMessage("DISP_NUM_PUB_AUTO"));//[1] add
				privateMaxNum = Integer.parseInt(IPropsLoader.getInstance().getMessage("DISP_NUM_PRI_AUTO"));//[1] add
			}else if(pub){
				//publicMaxNum = 20;//仕様 20 //[1] del
				publicMaxNum = Integer.parseInt(IPropsLoader.getInstance().getMessage("DISP_NUM_PUB_AUTO")) + Integer.parseInt(IPropsLoader.getInstance().getMessage("DISP_NUM_PRI_AUTO"));//[1] add
				privateMaxNum = 0;//仕様 0
			}else{
				publicMaxNum = 0;//仕様 0
				//privateMaxNum = 20;//仕様 20 //[1] del
				privateMaxNum = Integer.parseInt(IPropsLoader.getInstance().getMessage("DISP_NUM_PUB_AUTO")) + Integer.parseInt(IPropsLoader.getInstance().getMessage("DISP_NUM_PRI_AUTO"));//[1] add
			}
			
			//publicMaxNum = 0;
			//privateMaxNum = 1;
			
			//■第N志望の地区と分野で検索
			if(judgeTargetList != null && judgeTargetList.size() > 0){
				
				int wishIndex4Reg = 0;
				while(finalPubList.size() < publicMaxNum || finalPriList.size() < privateMaxNum){
					if(wishUnivList.size() <= wishIndex4Reg){
						break;//志望校がこれ以上ない
					}
										
					//第N志望と同じ分野の大学
					List tempRegList = new ArrayList();
					tempRegList.addAll(judgeTargetList);
					searchUnivRegMatch(tempRegList, ((WishUnivData)wishUnivList.get(wishIndex4Reg)).getUnivStemmaList());
					
					int wishIndex4Dist = 0;
					while(finalPubList.size() < publicMaxNum || finalPriList.size() < privateMaxNum){
						if(wishUnivList.size() <= wishIndex4Dist){
							break;//志望校がこれ以上ない
						}
						
						//第N志望と同じ地区の大学
						List tempDistList = new ArrayList();
						tempDistList.addAll(tempRegList);
						searchUnivDistMatch(tempDistList, ((WishUnivData)wishUnivList.get(wishIndex4Dist)).getDistrictCd());
						
						//System.out.println("第"+(wishIndex4Reg+1)+"志望の分野と第"+(wishIndex4Dist+1)+"志望の地区 : "+tempDistList.size());
						
						//ABCのみ許可
						doX(finalPubList, finalPriList, tempDistList);
						
						//System.out.println("ABCのみ許可 : "+(finalPubList.size()+finalPriList.size()));
						
						wishIndex4Dist ++;
						
					}
					
					wishIndex4Reg ++;
				}
			}
			
			//■件数が超過していたら、判定・ランクで上のほうからとる
			//国公立大学リストの取得
			//if(finalPubList.size() > publicMaxNum){//[4] del
				//if(scoreBean.getTargetExam().getExamTypeCd().equals(IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_MARK"))){//[3] del
				if(scoreBean.isTargetMarkExam()){//[3] add
					//選択対象模試がマークならばセンター評価順にソート
					Collections.sort(finalPubList, new JudgementSorterV2().new SortByCenterScoreRank());
				}else{
					//選択対象模試が記述模試ならば二次評価順にソート
					Collections.sort(finalPubList, new JudgementSorterV2().new SortBySecondScoreRank());
				}
			//最大数を超過していれば、良い方から最大数分だけ取る
			if(finalPubList.size() > publicMaxNum){//[4] add
				finalPubList = finalPubList.subList(0, publicMaxNum);
			}
			//私立大学リストの取得
			//if(finalPriList.size() > privateMaxNum){//[4] del
				//if(scoreBean.getTargetExam().getExamTypeCd().equals(IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_MARK"))){//[3] del
				if(scoreBean.isTargetMarkExam()){//[3] add
					//選択対象模試がマークならばセンター評価順にソート
					Collections.sort(finalPriList, new JudgementSorterV2().new SortByCenterScoreRank());
				}else{
					//選択対象模試が記述模試ならば二次評価順にソート
					Collections.sort(finalPriList, new JudgementSorterV2().new SortBySecondScoreRank());
				}
			//最大数を超過していれば、良い方から最大数分だけ取る
			if(finalPriList.size() > privateMaxNum){//[4] add
				finalPriList = finalPriList.subList(0, privateMaxNum);
			}
			//[4] del start 必要ない
			//[1] add start
			//subListした後に、ソートがされていないのでもう一度する
			//if(scoreBean.getTargetExam().getExamTypeCd().equals(IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_MARK"))){//[3] del
//			if(scoreBean.isTargetMarkExam()){//[3] add
//				//選択対象模試がマークならばセンター評価順にソート
//				Collections.sort(finalPubList, new JudgementSorterV2().new SortByCenterScoreRank());
//				Collections.sort(finalPriList, new JudgementSorterV2().new SortByCenterScoreRank());
//			}else{
//				//選択対象模試が記述模試ならば二次評価順にソート
//				Collections.sort(finalPubList, new JudgementSorterV2().new SortBySecondScoreRank());
//				Collections.sort(finalPriList, new JudgementSorterV2().new SortBySecondScoreRank());
//			}
			//[1] add end
			//[4] del end
			//■公私のリストを合体(国公立が先)
			recordSet.addAll(finalPubList);
			recordSet.addAll(finalPriList);
			
			// 公私を合体させた最終リストをソート
			//[1] del start
//			if(scoreBean.getTargetExam().getExamTypeCd().equals(IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_MARK"))){
//				//選択対象模試がマークならばセンター評価順にソート
//				Collections.sort(recordSet, new JudgementSorterV2().new SortByCenterScoreRank());
//			}else{
//				//選択対象模試が記述模試ならば二次評価順にソート
//				Collections.sort(recordSet, new JudgementSorterV2().new SortBySecondScoreRank());
//			}
			//[1] del end
		}
	}
	
	/**
	 * 分野コードで絞り込む
	 * @param compairData
	 * @param univStemmaList
	 */
	private void searchUnivRegMatch(List compairData, List univStemmaList){
		int count = 0;
		Iterator ite = compairData.iterator();
		if(ite != null){
			while(ite.hasNext()){
				UnivKeiNavi univ = (UnivKeiNavi)ite.next();
				boolean removeFlg = true;
				List regionList = univ.getRegionList();//分野のリスト
				if(regionList != null && regionList.size() > 0){
					Iterator ite2 = regionList.iterator();
					while(ite2.hasNext()){
						String[] region = (String[]) ite2.next();//String[]{"分野コード", "分野名"}
						if(region[0] != null){
							if(univStemmaList != null && univStemmaList.size() > 0){
								Iterator ite3 = univStemmaList.iterator();
								while(ite3.hasNext()){
									UnivStemmaData usd = (UnivStemmaData) ite3.next();
									if(usd.getStemmaDiv() != null && usd.getStemmaDiv().equals("3") && region[0].equals(usd.getRegionCd())){
										removeFlg = false;
									}	
								}
							}
						}	
					}
				}
				if(removeFlg){
					ite.remove();//リストから削除
				}else{
					count ++;
				}
			}
		}
		//System.out.println("分野コードによる絞り込み結果 : "+count+"/"+totalCount);
	}
	
	/**
	 * 地区コードで絞り込む
	 * @param compairData
	 * @param searchDistrictCd
	 */
	private void searchUnivDistMatch(List compairData, String searchDistrictCd){
		int count = 0;
		Iterator ite = compairData.iterator();
		if(ite != null){
			while(ite.hasNext()){
				UnivKeiNavi univ = (UnivKeiNavi)ite.next();
				if(!univ.getDistrictCd().equals(searchDistrictCd)){
					ite.remove();//リストから削除
				}else{
					count ++;
				}
			}
		}
		//System.out.println("地区コード絞り込みによる結果 : "+count+"/"+totalCount);
	}
	
	/**
	 * 大学のリストから女子大を排除する
	 * @param list
	 */
	private void removeFemaleSchool(List list){
		int count = 0;
		if(list != null && list.size() > 0){
			Iterator ite = list.iterator();
			while(ite.hasNext()){
				UnivKeiNavi univ = (UnivKeiNavi) ite.next();
				//1：共学、2：女子大
				if(univ.getFlag_women() == Integer.parseInt(IPropsLoader.getInstance().getMessage("CODE_SCHOOL_FEMAIL"))){
					//System.out.println(univ.univName+" "+univ.deptName+" "+univ.subName+" を削除しました");
					ite.remove();
					count ++;
				}
			}
		}
		//System.out.println("この生徒は男なので女子大を"+count+"/"+totalCount+"件削除しました");
	}
	
	/**
	 * 表示件数にあるように絞込みを行う
	 * ※同じ大学は重複しない
	 * @param finalPubList
	 * @param finalPriList
	 * @param judgeTargetList
	 * @throws Exception
	 */
	private void doX(List finalPubList, List finalPriList, List judgeTargetList) throws Exception{

		List judgedList = judge(judgeTargetList);
			
		//この状態で公私の表示件数を満たしているか確認(同時に多ければ切る)
		Iterator judgedIte;
		
		//Cﾗﾝｸ以上を許可する
		int judgeLimit = JudgementConstants.JUDGE_C;
			
		/** 国公立 */
		if((judgeTargetList != null && judgeTargetList.size() > 0)){// && finalPubList.size() < publicMaxNum){//国公立が足りない
			/*
			if(basePubGradeNum < judgeLimit){//D,E判定は無視
				break;//A,B,C判定がこれ以上ない
			}
			*/
			judgedIte = judgedList.iterator();
			while(judgedIte.hasNext()){// && finalPubList.size() < publicMaxNum){	
				
				Judgement j = (Judgement) judgedIte.next();
				
				UnivKeiNavi univ = (UnivKeiNavi) j.getUniv();
				
				//C判定以上のみ許可
				//if(j.t_ghjudgement != 1 && j.t_ghjudgement != 2 && j.t_judgement >= judgeLimit && j.university.c_maximumScale > 0 && j.university.s_maximumScale > 0){
				//対象模試がマークの場合は、マーク判定で絞込み
				int judgement = 0;
				int ghjudgement = 0;
				int criteria = 0;
				//if(j.score.getTargetExam().getExamTypeCd().equals(IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_MARK"))){//[3] del

				if(j.getScore().isTargetMarkExam()){//[3] add
					//[1] add start
					//マーク模試で二次を判定していた場合は、二次結果を代わりに見る
					if(j.isSecondByMark()){
						judgement = j.getSJudgement();
						ghjudgement = j.getSGhJudgement();
						//criteria = j.university.s_maximumScale;//[5] del
						criteria = ((UnivDetail)univ.getDetailHash().get(j.getSelectedBranch())).getSAllotPointRate();//[5] add
					}
					//通常
					else{
					//[1] add end
						judgement = j.getCJudgement();
						ghjudgement = j.getCGhJudgement();
						//criteria = j.university.c_maximumScale;//マーク模試が有効かどうか//[5] del
						criteria = ((UnivDetail)univ.getDetailHash().get(j.getSelectedBranch())).getCAllotPointRate();//[5] add
					}//[1] add
				}
				//対象模試が記述の場合は、記述判定で絞込み
				else{
					judgement = j.getSJudgement();
					ghjudgement = j.getSGhJudgement();
					//criteria = j.university.s_maximumScale;//記述模試が有効かどうか//[5] del
					criteria = ((UnivDetail)univ.getDetailHash().get(j.getSelectedBranch())).getSAllotPointRate();//[5] add
				}
				if(ghjudgement == JudgementConstants.Judge.JUDGE_GH_NA 
						&& judgement >= judgeLimit && criteria > 0){
				//判定がbasePubGradeNumのもののみ、国公立のリストに追加
					if(univ.getUnivDivCd().trim().equals(IPropsLoader.getInstance().getMessage("CODE_SCHOOL_PUBLIC"))){
						if(!containJB(finalPubList, j)){
							finalPubList.add(j);
						}
					}
				}
			}
			//basePubGradeNum --;//次の判定
		}
		/** 私立 */
		if((judgeTargetList != null && judgeTargetList.size() > 0)){// && finalPriList.size() < privateMaxNum){//私立が足りない
			/*
			if(basePriGradeNum < judgeLimit){//D,E判定は無視
				break;//A,B,C判定がこれ以上ない
			}
			*/
			judgedIte = judgedList.iterator();
			while(judgedIte.hasNext()){// &&  finalPriList.size() < privateMaxNum){	
				
				Judgement j = (Judgement) judgedIte.next();
				
				UnivKeiNavi univ = (UnivKeiNavi) j.getUniv();
				
				//C判定以上のみ許可
				//if(j.t_ghjudgement != 1 && j.t_ghjudgement != 2 && j.t_judgement >= judgeLimit && j.university.c_maximumScale > 0 && j.university.s_maximumScale > 0){
				//対象模試がマークの場合は、マーク判定で絞込み
				int judgement = 0;
				int ghjudgement = 0;
				int criteria = 0;
				//if(j.score.getTargetExam().getExamTypeCd().equals(IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_MARK"))){//[3] del
				if(j.getScore().isTargetMarkExam()){//[3] add
					//[1] add start
					if(j.isSecondByMark()){
						judgement = j.getSJudgement();
						ghjudgement = j.getSGhJudgement();
						//criteria = j.university.s_maximumScale;//記述模試が有効かどうか//[5] del
						criteria = ((UnivDetail)univ.getDetailHash().get(j.getSelectedBranch())).getSAllotPointRate();//[5] add
					}else{
					//[1] add end
						judgement = j.getCJudgement();
						ghjudgement = j.getCGhJudgement();
						//criteria = j.university.c_maximumScale;//マーク模試が有効かどうか//[5] del
						criteria = ((UnivDetail)univ.getDetailHash().get(j.getSelectedBranch())).getCAllotPointRate();//[5] add
					}//[1] add
				}
				//対象模試が記述の場合は、記述判定で絞込み
				else{
					judgement = j.getSJudgement();
					ghjudgement = j.getSGhJudgement();
					//criteria = j.university.s_maximumScale;//記述模試が有効かどうか//[5] del
					criteria = ((UnivDetail)univ.getDetailHash().get(j.getSelectedBranch())).getSAllotPointRate();//[5] add
				}
				//判定がbasePubGradeNumのもののみ、私立のリストに追加
				if(ghjudgement == JudgementConstants.Judge.JUDGE_GH_NA 
						&& judgement >= judgeLimit && criteria > 0){
					if(!univ.getUnivDivCd().trim().equals(IPropsLoader.getInstance().getMessage("CODE_SCHOOL_PUBLIC"))){
						if(!containJB(finalPriList, j)){
							finalPriList.add(j);
						}
					}
				}
			}
			//basePriGradeNum --;//次の判定
		}
	}
	/**
	 * リストにこの判定結果の大学があるかどうか
	 * @param list
	 * @param jb
	 * @return
	 */
	private boolean containJB(List list, Judgement jb){
		boolean rb = false;
		Iterator ite = list.iterator();
		while(ite.hasNext()){
			Judgement jb2 = (Judgement)ite.next();
			if(jb2.getUniv().getUnivCd().equals(jb.getUniv().getUnivCd()) && jb2.getUniv().getFacultyCd().equals(jb.getUniv().getFacultyCd()) && jb2.getUniv().getDeptCd().equals(jb.getUniv().getDeptCd())){
				rb = true;
			}
		}
		return rb;
	}
	
	/**
	 * 判定処理
	 * @param list
	 * @return
	 */
	private List judge(final List list) throws Exception {
		final JudgementManager judge = new JudgementManager(
				scoreBean, precedeCenterPre);
		judge.setUnivList(list);
		judge.execute();
		return judge.getResults();
	}

	/**
	 * センタープレでの成績を差し替えを有効化
	 */
	public void setPrecedeCenterPre() {
		precedeCenterPre = true;
	}

	/**
	 * @return
	 */
	public List getUnivAllList() {
		return univAllList;
	}

	/**
	 * @return
	 */
	public List getWishUnivList() {
		return wishUnivList;
	}

	/**
	 * @param list
	 */
	public void setUnivAllList(List list) {
		univAllList = list;
	}

	/**
	 * @param list
	 */
	public void setWishUnivList(List list) {
		wishUnivList = list;
	}
//[3] del start
//	/**
//	 * @return
//	 */
//	public boolean isUseNewLogic() {
//		return isUseNewLogic;
//	}
//	
//	/**
//	 * @param b
//	 */
//	public void setUseNewLogic(boolean b) {
//		isUseNewLogic = b;
//	}
//[3] del end	
	/**
	 * @return
	 */
	public Score getScoreBean() {
		return scoreBean;
	}

	/**
	 * @param bean
	 */
	public void setScoreBean(Score bean) {
		scoreBean = bean;
	}

	/**
	 * @return
	 */
	public List getRecordSet() {
		return recordSet;
	}

	/**
	 * @param list
	 */
	public void setRecordSet(List list) {
		recordSet = list;
	}

}
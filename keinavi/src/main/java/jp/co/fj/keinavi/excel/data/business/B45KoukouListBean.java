/*
 * �쐬��: 2004/09/08
 * ���Z���ѕ��́|���ъT���i�߉��r)
 * @author cmurata
 */
package jp.co.fj.keinavi.excel.data.business;

public class B45KoukouListBean {
	
	//���Z��
	private String strGakkomei="";
	//�͎���
	private String strKakaiMshmei = "";
	//�͎����{���
	private String strKakaiMshDate = "";
	//�l��
	private int intNinzu=0;
	//���ϓ_
	private float floHeikin = 0;
	//���ϕ΍��l
	private float floHensa = 0;

	/**
	 * @GET
	 */
	public float getFloHeikin() {
		return this.floHeikin;
	}

	public float getFloHensa() {
		return this.floHensa;
	}

	public int getIntNinzu() {
		return this.intNinzu;
	}

	public String getStrGakkomei() {
		return this.strGakkomei;
	}

	public String getStrKakaiMshDate() {
		return this.strKakaiMshDate;
	}

	public String getStrKakaiMshmei() {
		return this.strKakaiMshmei;
	}

	/**
	 * @SET
	 */
	public void setFloHeikin(float floHeikin) {
		this.floHeikin = floHeikin;
	}
		
	public void setFloHensa(float floHensa) {
		this.floHensa = floHensa;
	}

	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}

	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}

	public void setStrKakaiMshDate(String strKakaiMshDate) {
		this.strKakaiMshDate = strKakaiMshDate;
	}

	public void setStrKakaiMshmei(String strKakaiMshmei) {
		this.strKakaiMshmei = strKakaiMshmei;
	}

}

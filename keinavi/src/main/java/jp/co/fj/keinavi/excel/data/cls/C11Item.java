package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−偏差値分布データクラス
 * 作成日: 2004/07/13
 * @author	H.Fujimoto
 */
public class C11Item {
	//学校名
	private String strGakkomei = "";
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//表フラグ
	private int intHyouFlg = 0;
	//度数分布グラフフラグ
	private int intGraphFlg = 0;
	//人数積み上げグラフフラグ
	private int intNinzuFlg = 0;
	//ピッチフラグ
	private int intPitchFlg = 0;
	//セキュリティスタンプ
	private int intSecuFlg = 0;
    //データリスト
	private ArrayList c11List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;


	/*----------*/
	/* Get      */
	/*----------*/
	
	public ArrayList getC11List() {
		return this.c11List;
	}
	public int getIntGraphFlg() {
		return this.intGraphFlg;
	}
	public int getIntHyouFlg() {
		return this.intHyouFlg;
	}
	public int getIntNinzuFlg() {
		return this.intNinzuFlg;
	}
	public int getIntPitchFlg() {
		return this.intPitchFlg;
	}
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setC11List(ArrayList c11List) {
		this.c11List = c11List;
	}
	public void setIntGraphFlg(int intGraphFlg) {
		this.intGraphFlg = intGraphFlg;
	}
	public void setIntHyouFlg(int intHyouFlg) {
		this.intHyouFlg = intHyouFlg;
	}
	public void setIntNinzuFlg(int intNinzuFlg) {
		this.intNinzuFlg = intNinzuFlg;
	}
	public void setIntPitchFlg(int intPitchFlg) {
		this.intPitchFlg = intPitchFlg;
	}
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}
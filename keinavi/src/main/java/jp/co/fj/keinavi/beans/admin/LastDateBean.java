/*
 * 作成日: 2004/08/31
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.admin;
import java.util.HashMap;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Calendar;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */

public class LastDateBean {


	/**
	 * 学校マスタから生徒情報管理の最終更新日を取得する。
	 * 
	 * @param conn コネクション 
	 * @return dateData 更新日の格納されたHashMap
	 * 
	 */
	public static HashMap getLastDate(Connection conn, String code)
		throws SQLException {

		HashMap dateData = new HashMap();
		PreparedStatement stmt = null;
		ResultSet rs = null;

		String query =
			"SELECT RENEWALDATE1,RENEWALDATE2,RENEWALDATE3 FROM school WHERE BUNDLECD = ? ";

		try {
			stmt = conn.prepareStatement(query);
			stmt.setString(1, code);
			rs = stmt.executeQuery();
			while (rs.next()) {
				dateData.put(
					"RENEWALDATE1",
					toDateFormat8(rs.getString("RENEWALDATE1")));
				dateData.put(
					"RENEWALDATE2",
					toDateFormat8(rs.getString("RENEWALDATE2")));
				dateData.put(
					"RENEWALDATE3",
					toDateFormat8(rs.getString("RENEWALDATE3")));
			}
			if (dateData.size() == 0)
				throw new SQLException("更新日の取得に失敗しました。");
		} catch (SQLException ex) {
			throw ex;
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return dateData;
	}


	/**
	 * 学校マスタに生徒情報管理の最終更新日を設定する。
	 * 
	 * @param conn コネクション 
	 * @return dateData 更新日の格納されたHashMap
	 * 
	 */
	public static int setLastDate1(Connection conn,  String code)
		throws SQLException {

		HashMap dateData = new HashMap();
		PreparedStatement stmt = null;
		int count  = 0;

		String query =
			"UPDATE school SET RENEWALDATE1 =? WHERE BUNDLECD = ? ";
		try {
			stmt = conn.prepareStatement(query);
			stmt.setString(1, getFormatedTimeString("yyyyMMdd"));
			stmt.setString(2, code);
			count = stmt.executeUpdate();
			if (count == 0)
				throw new SQLException("更新日の設定に失敗しました。");
		} catch (SQLException ex) {
			throw ex;
		} finally {
			if (stmt != null)
				stmt.close();
		}
		return count;
	}
	
	/**
	 * 学校マスタに生徒情報管理の最終更新日を設定する。
	 * 
	 * @param conn コネクション 
	 * @return dateData 更新日の格納されたHashMap
	 * 
	 */
	public static int setLastDate2(Connection conn, String code)
		throws SQLException {

		HashMap dateData = new HashMap();
		PreparedStatement stmt = null;
		int count  = 0;

		String query =
			"UPDATE school SET RENEWALDATE2 = ? WHERE BUNDLECD = ? ";
		try {
			stmt = conn.prepareStatement(query);
			stmt.setString(1, getFormatedTimeString("yyyyMMdd"));
			stmt.setString(2, code);
			count = stmt.executeUpdate();
			if (count == 0)
				throw new SQLException("更新日の設定に失敗しました。");
		} catch (SQLException ex) {
			throw ex;
		} finally {
			if (stmt != null)
				stmt.close();
		}
		return count;
	}
	
	/**
	 * 学校マスタに生徒情報管理の最終更新日を設定する。
	 * 
	 * @param conn コネクション 
	 * @return dateData 更新日の格納されたHashMap
	 * 
	 */
	public static int setLastDate3(Connection conn, String code)
		throws SQLException {

		HashMap dateData = new HashMap();
		PreparedStatement stmt = null;
		int count  = 0;

		String query =
			"UPDATE school SET RENEWALDATE3 =? WHERE BUNDLECD = ? ";
		try {
			stmt = conn.prepareStatement(query);
			stmt.setString(1, getFormatedTimeString("yyyyMMdd"));
			stmt.setString(2, code);
			count = stmt.executeUpdate();
			if (count == 0)
				throw new SQLException("更新日の設定に失敗しました。");
		} catch (SQLException ex) {
			throw ex;
		} finally {
			if (stmt != null)
				stmt.close();
		}
		return count;
	}
	/**
	* 日付書式に変換 yyyy/mm/dd
	* @param str 日付文字
	* @return 判定
	*/
	public static String toDateFormat8(String str) {
		if (str == null || !isSizeS(8, str) || !isNumber(str)) {
			return str;
		} else {
			String year = str.substring(0, 4);
			String month = str.substring(4, 6);
			String day = str.substring(6, 8);
			str = year + "/" + month + "/" + day;
		}
		return str;
	}


	/**
	* 日付書式に変換 yyyy/mm/dd
	* @param str 日付文字
	* @return 判定
	*/
	public static String toDateFormat6(String str) {
		
		if (str == null || !isSizeS(6, str) || !isNumber(str)) {
			str = "";
		} else {
			String year = str.substring(0, 2);
			String month = str.substring(2, 4);
			String day = str.substring(4, 6);
			
			int nowYear = Integer.parseInt(getFormatedTimeString("yyyy"));
			int birthYear = Integer.parseInt(19 + year);
			if( (nowYear-birthYear) < 100)year = 19 + year;
			else year = 20 + year;
			str = year + "/" + month + "/" + day;
		}
		return str;
	}
	
	

	/**
	* 
	* タイムスタンプ
	* 
	* @param index 書式
	* @return 現在の日付で設定された書式の文字型で返します。
	*/
	public static String getFormatedTimeString(String index) {
		Date currentTime = Calendar.getInstance().getTime();
		DateFormat form = new SimpleDateFormat(index);
		String timeStr = form.format(currentTime);
		return timeStr;
	}
	
	
	/**
	* 任意の桁越えチェック
	* @param  int _figure, String _str
	* @return 判定
	*/
	public static boolean isSizeS(int _figure, String _str) {
		boolean bl = true;
		int figure = _figure;
		String str = _str;
		if (str.length() != figure)
			bl = false;
		return bl;
	}

	/**
	* 数値かどうかチェック
	* @param _str
	* @return 判定
	*/
	private static boolean isNumber(String _str) {
		boolean bl = true;
		long num = 0;
		String str = _str;
		try {
			num = Long.parseLong(str);
		} catch (Exception ex) {
			bl = false;
		}
		return bl;
	}
}

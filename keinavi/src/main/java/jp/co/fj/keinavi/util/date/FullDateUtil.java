/*
 * 作成日: 2004/07/16
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.date;

import java.text.SimpleDateFormat;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class FullDateUtil extends DateUtil {

	// SimpleDateFormatインスタンス
	private static SimpleDateFormat format ; 

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.date.DateUtil#getInstance()
	 */
	public SimpleDateFormat factoryMethod() {
		if (format == null) format = new SimpleDateFormat("yyyyMMddHHmmss");
		return format;
	}
	
}

package jp.co.fj.keinavi.excel.data.business;

import java.util.ArrayList;
/**
 * 高校成績分析−高校間比較−成績概況 データクラス
 * 作成日: 2004/07/22
 * @author	A.Iwata
 */
public class B41Item {
	//模試名
	private String strMshmei = "";
	//模試基準日
	private String strMshDate = "";
	//表フラグ
	private int intHyouFlg = 0;
	//成績順位フラグ
	private int intRankFlg = 0;
	//過年度表示比較フラグ
	private int intPastYearFlg = 0;
	//全体平均表示フラグ
	private int intHeikinAllFlg = 0;
	//現役平均表示フラグ
	private int intHeikinGenFlg = 0;
	//高卒平均表示フラグ
	private int intHeikinSotuFlg = 0;
	//表示学校数
	private int intGakkouNum = 0;
	//上位校の＊表示フラグ
	private int intBestFlg = 0;
	//セキュリティスタンプ
	private int intSecuFlg = 0;
	//データリスト
	private ArrayList b41List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return strMshDate;
	}
	public int getIntHyouFlg() {
		return intHyouFlg;
	}
	public int getIntRankFlg() {
		return this.intRankFlg;
	}
	public int getIntPastYearFlg() {
		return this.intPastYearFlg;
	}
	public int getIntHeikinAllFlg() {
		return this.intHeikinAllFlg;
	}
	public int getIntHeikinGenFlg() {
		return this.intHeikinGenFlg;
	}
	public int getIntHeikinSotuFlg() {
		return this.intHeikinSotuFlg;
	}
	public int getIntBestFlg() {
		return this.intBestFlg;
	}
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public ArrayList getB41List() {
		return this.b41List;
	}
	public int getIntGakkouNum() {
		return intGakkouNum;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setIntHyouFlg(int intHyouFlg) {
		this.intHyouFlg = intHyouFlg;
	}
	public void setIntRankFlg(int intRankFlg) {
		this.intRankFlg = intRankFlg;
	}
	public void setIntPastYearFlg(int intPastYearFlg) {
		this.intPastYearFlg = intPastYearFlg;
	}
	public void setIntHeikinAllFlg(int intHeikinAllFlg) {
		this.intHeikinAllFlg = intHeikinAllFlg;
	}
	public void setIntHeikinGenFlg(int intHeikinGenFlg) {
		this.intHeikinGenFlg = intHeikinGenFlg;
	}
	public void setIntHeikinSotuFlg(int intHeikinSotuFlg) {
		this.intHeikinSotuFlg = intHeikinSotuFlg;
	}
	public void setIntBestFlg(int intBestFlg) {
		this.intBestFlg = intBestFlg;
	}
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setB41List(ArrayList b41List) {
		this.b41List = b41List;
	}
	public void setIntGakkouNum(int intGakkouNum) {
		this.intGakkouNum = intGakkouNum;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}


}

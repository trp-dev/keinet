/*
 * 作成日: 2004/09/01
 *
 * 判定結果一覧サーブレット
 * 
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.PrefBean;
import jp.co.fj.keinavi.beans.individual.ProvisoSearchBean;
import jp.co.fj.keinavi.beans.individual.ProvisoStoreBean;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.ProvisoData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I202Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.date.SpecialDateUtil;
import jp.co.fj.keinavi.util.db.RecordProcessor;

/**
 *
 * @author [0]		T.Yamada 	2004/8/10	作成 				
 *          [1] 	T.Yamada 	2005/1/10	センターリサーチ対応
 * 			[2]		K.Kondo		2005/5/9	再表示時のスクロール座標を引き継ぐように修正
 * 			[3] 	K.Kondo		2005/6/29	中系統のチェック条件から「地学・他（3800）」を抜いた
 *
 */
public class I202Servlet extends IndividualServlet {
	
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		super.execute(request, response);
		
		//フォームのデータを取得 ::::::::
		I202Form form = null;
		try {
			form = (I202Form)super.getActionForm(request, "jp.co.fj.keinavi.forms.individual.I202Form");
		} catch (Exception e) {
			KNServletException k = new KNServletException("0I202にてフォームデータの収得に失敗しました。" + e);
			k.setErrorCode("00I202010101");
			throw k;
		}
		
		String scrollX = form.getScrollX();//[2] add
		String scrollY = form.getScrollY();//[2] add
		
		HttpSession session = request.getSession();
		
		//プロファイル情報
		Profile profile = (Profile) session.getAttribute(Profile.SESSION_KEY);
		//条件保存されていない生徒の処理
		Short indStudentUsage = (Short)profile.getItemMap(IProfileCategory.I_UNIV_JUDGE).get(IProfileItem.IND_STUDENT_USAGE);
		
		//個人共通
		ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);
		
		//判定対象生徒人数
		Short JudgeStudent = (Short)profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);

		if ("i202".equals(getForward(request))) {
		// JSP転送
			Connection con = null;
			try{
				con = getConnectionPool(request);
				con.setAutoCommit(false);
				
				//初回アクセス
				if(!"i202".equals(getBackward(request))
						|| !iCommonMap.getTargetPersonId().equals(form.getTargetPersonId())
						|| isChangedMode(request)) {

				//二回目以降(条件保存を押した)
				} else {
					/** 条件の保存 */
					ProvisoStoreBean psb = new ProvisoStoreBean();
					psb.setIndividualId(form.getTargetPersonId());//個人ID
					psb.setDistrictProviso(form.getPref());//地区条件
					psb.setSearchStemmaDiv(form.getSearchStemmaDiv());//検索系統区分
					psb.setStemma2Code(form.getStemma2Code());//中系統コード
					psb.setStemma11Code(form.getStemma11Code());//小系統文系コード
					psb.setStemma10Code(form.getStemma10Code());//小系統理系コード
					//分野コードは中系統・文系・理系で値が重なることがあるので一意にする
					Set set = CollectionUtil.array2Set(form.getStemmaCode());
					String[] stemmaCodes = (String[]) set.toArray(new String[set.size()]);
					psb.setStemmaCode(stemmaCodes);//分野コード
					psb.setDescImposeDiv(form.getDescImposeDiv());// 1:（センター利用大）2次・独自試験を課さない 
					psb.setSubImposeDiv(form.getSubImposeDiv());//試験科目課し区分
					psb.setImposeSub(form.getImposeSub());//課し試験科目
					psb.setSchoolDivProviso(form.getSchoolDivProviso());//学校区分条件
					psb.setStartMonth(form.getStartMonth());
					psb.setStartDate(form.getStartDate());
					psb.setEndMonth(form.getEndMonth());
					psb.setEndDate(form.getEndDate());
					psb.setRaingDiv(form.getRaingDiv());//評価試験区分
					psb.setRaingProviso(form.getRaingProviso());//評価範囲条件
					psb.setOutOfTergetProviso(form.getOutOfTergetProviso());//対象外条件
					psb.setDateSearchDiv(form.getDateSearchDiv());//入試日区分
					psb.setConnection("", con);
					psb.execute();
					
					//条件保存ダイアログをだすフラグ
					form.setSaveflg(true);
				}
			
				/** フォームの復活を"必ず"行う */
				//プロファイルだけ初回のみセット
				if (!"i202".equals(getBackward(request))) {
					//条件保存されていない生徒の処理プロファイル
					form.setIndStudentUsage(Short.toString(indStudentUsage.shortValue()));
					if(iCommonMap.isBunsekiMode()){
						form.setJudgeStudent(JudgeStudent.toString());
					}
				}

				ProvisoSearchBean psb = new ProvisoSearchBean();
				psb.setIndividualId(form.getTargetPersonId());
				psb.setConnection("", con);
				psb.execute();
				// 都道府県Bean
				PrefBean pref = PrefBean.getInstance(con);
				request.setAttribute("PrefBean",pref);
				if(psb.getRecordSet() != null && psb.getRecordSet().size() > 0){
					//保存してあれば復活
					//restoreForm(psb, pref, form);//[1] DEL
					restoreForm(psb, pref, form, iCommonMap);//[1] ADD
				}else{
					//保存していなければデフォルト値のみセット
					/* 12/20 add
					 * 引き継ぐ箇所以外は初期化
					 */
					I202Form form2 = new I202Form();
					form2.setTargetPersonId(form.getTargetPersonId());
					form2.setButton(form.getButton());
					form2.setIndStudentUsage(form.getIndStudentUsage());
					form2.setJudgeStudent(form.getJudgeStudent());
					form2.setPrintFlag(form.getPrintFlag());
					form2.setPrintStamp(form.getPrintStamp());
					form2.setPrintStudent(form.getPrintStudent());
					form2.setTargetExam(form.getTargetExam());
					form2.setTargetExamCode(form.getTargetExamCode());
					form2.setTargetExamName(form.getTargetExamName());
					form2.setTargetExamYear(form.getTargetExamYear());
					
					//入試日
					form2.setStartMonth("04");
					form2.setStartDate("01");
					form2.setEndMonth("03");
					form2.setEndDate("31");
					
					//学校区分
					form2.setSchoolDivProviso(CollectionUtil.deconcatComma("1"));

					//評価範囲
					form2.setRaingProviso(CollectionUtil.deconcatComma("1,2,3,4,5"));
					/*
					//[1] ADD start
					if(iCommonMap.getTargetExamCode().trim().equals(IPropsLoader.getInstance().getMessage("CODE_EXAM_CENTERRESEARCH"))){
					//対象模試がセンターリサーチ
						form2.setRaingProviso(CollectionUtil.deconcatComma("1,2,3,4"));
					}else{
					//対象模試がセンターリサーチ以外
						form2.setRaingProviso(CollectionUtil.deconcatComma("1,2,3,4,5"));
					}
					//[1] ADD end
					 */
					
					//課し区分
					form2.setSubImposeDiv("2");
					
					//入試日区分
					form2.setDateSearchDiv("0");
					
					form = form2;
				}
				//今年度
				form.setThisYear(SpecialDateUtil.getThisJpnYear());//DatePicker用
				
				con.commit();
				
			}catch(Exception e){
				e.printStackTrace();
				try {
					con.rollback();
				} catch (SQLException e1) {
					//TODO ロールバックできなかったときの処理
				}
				KNServletException k = new KNServletException("0I202にてエラーが発生しました。");
				k.setErrorCode("00I202070203");
				throw k;
			}finally {
				try {if (con != null) releaseConnectionPool(request, con);
				} catch (Exception e) {
					KNServletException k = new KNServletException("0I202にてコネクションの解放に失敗しました。" + e);
					k.setErrorCode("00I202010101");
					throw k;
				}
			}
			
			form.setScrollX(scrollX);//[2] add
			form.setScrollY(scrollY);//[2] add
			request.setAttribute("form", form);
			
			iCommonMap.setTargetPersonId(form.getTargetPersonId());
			super.forward(request, response, JSP_I202);
			
		} else {
		// 不明ならServlet転送
		// 転送先がi202 or i204ならプロファイルに保存する
		try{
			if ("i204".equals(getForward(request))) {
				//条件保存されていない生徒の処理プロファイル
				profile.getItemMap(IProfileCategory.I_UNIV_JUDGE).put(IProfileItem.IND_STUDENT_USAGE, new Short(form.getIndStudentUsage()));
				//判定対象生徒プロファイル
				if(iCommonMap.isBunsekiMode()){
					profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(form.getJudgeStudent()));
				}
			}
		}catch(Exception e){
			KNServletException k = new KNServletException("0I202にてプロファイル保存に失敗しました。" + e);
			k.setErrorCode("00I202070203");
			throw k;
		}
		
			iCommonMap.setTargetPersonId(form.getTargetPersonId());
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}
	/**
	 * フォームを復活させる
	 * @param psb
	 */
	//private void restoreForm(ProvisoSearchBean psb, PrefBean pref, I202Form form) throws Exception{//[1] DEL
	private void restoreForm(ProvisoSearchBean psb, PrefBean pref, I202Form form, ICommonMap iCommonMap) throws Exception{//[1] ADD
		ProvisoData pd;
		if(psb.getRecordSet() != null && psb.getRecordSet().size() > 0){
			pd = (ProvisoData) psb.getRecordSet().get(0);//最高一件しか存在しない
			form.setPref(CollectionUtil.deconcatComma(pd.getDistrictProviso()));
						
			String[] prefs = CollectionUtil.splitComma(GeneralUtil.toBlank(pd.getDistrictProviso()));
			int Count = 0;
			StringBuffer blocks = new StringBuffer();			//地区コード
			ArrayList prefList = new ArrayList();
			for (int i = 0; i < prefs.length; i++) {
				if(!prefs[i].equals("")){
					prefList.add(prefs[i]);
				}
			}
						
			if(prefList.contains("01")) {
				//北海道地区
				blocks.append("01");
				Count ++;	
			}
			if(prefList.contains("02") && prefList.contains("03") && prefList.contains("04") &&
					  prefList.contains("05") && prefList.contains("06") && prefList.contains("07")){
				//東北地区
				if(Count != 0){
					blocks.append(",");
				}
				blocks.append("02");

				Count ++;	
			}
			if(prefList.contains("08") && prefList.contains("09") && prefList.contains("10") && 
					  prefList.contains("11") && prefList.contains("12") && prefList.contains("13") && prefList.contains("14")){
				//関東地区
				if(Count != 0){
					blocks.append(",");
				}
				blocks.append("03");

				Count ++;
			}
			if(prefList.contains("15") && prefList.contains("19") && prefList.contains("20")){
				//甲信越地区
				if(Count != 0){
					blocks.append(",");
				}
				blocks.append("04");

				Count ++;
			}
			if(prefList.contains("16") && prefList.contains("17") && prefList.contains("18")){
				//北陸地区
				if(Count != 0){
					blocks.append(",");
				}
				blocks.append("05");

				Count ++;
		   }
		   if(prefList.contains("21") && prefList.contains("22") && prefList.contains("23") && prefList.contains("24")){
				//東海地区
				if(Count != 0){
					blocks.append(",");
				}
				blocks.append("06");

				Count ++;
			}
			if(prefList.contains("25") && prefList.contains("26") && prefList.contains("27") && prefList.contains("28") && prefList.contains("29") && prefList.contains("30")){
				//近畿地区
				if(Count != 0){
					blocks.append(",");
				}
				blocks.append("07");

				Count ++;
			}
			if(prefList.contains("31") && prefList.contains("32") && prefList.contains("33") && prefList.contains("34") && prefList.contains("35")){
				//中国地区
				if(Count != 0){
					blocks.append(",");
				}
				blocks.append("08");

				Count ++;
			}
			if(prefList.contains("36") && prefList.contains("37") && prefList.contains("38") && prefList.contains("39")){
				//四国地区
				if(Count != 0){
					blocks.append(",");
				}
				blocks.append("09");

				Count ++;
			}
			if(prefList.contains("40") && prefList.contains("41") && prefList.contains("42") && prefList.contains("43") &&
					  prefList.contains("44") && prefList.contains("45") && prefList.contains("46") && prefList.contains("47")){
				//九州・沖縄地区
				if(Count != 0){
					blocks.append(",");
				}
				blocks.append("10");
		
				Count ++;
			}
						
			form.setBlock(CollectionUtil.deconcatComma(blocks.toString()));//地区コード
			form.setSearchStemmaDiv(pd.getSearchStemmaDiv());
			if(pd.getSearchStemmaDiv().equals("1")){//中系統
							
				String[] stemmaCode = CollectionUtil.splitComma(GeneralUtil.toBlank(pd.getStemmaProviso()));
							
				ArrayList MstemmaList = new ArrayList();
				for (int i = 0; i < stemmaCode.length; i++) {
					if(!stemmaCode[i].equals("")){
						MstemmaList.add(stemmaCode[i]);
					}
				}
				//中系統フラグtrue
				form.setMstemmaflg(true);
				//文系フラグ
				form.setStemmaAflg(false);
							
				if(MstemmaList.contains("0011")){
					form.setMstemma0011("1");
				}
				if(MstemmaList.contains("0021")){
					form.setMstemma0021("1");
				}
				if(MstemmaList.contains("0022")){
					form.setMstemma0022("1");
				}
				if(MstemmaList.contains("0023")){
					form.setMstemma0023("1");
				}
				if(MstemmaList.contains("0031")){
					form.setMstemma0031("1");
				}
				if(MstemmaList.contains("0032")){
					form.setMstemma0032("1");
				}
				if(MstemmaList.contains("0041")){
					form.setMstemma0041("1");
				}
				if(MstemmaList.contains("0042")){
					form.setMstemma0042("1");
				}
				if(MstemmaList.contains("0043")){
					form.setMstemma0043("1");
				}
				if(MstemmaList.contains("0051")){
					form.setMstemma0051("1");
				}
				if(MstemmaList.contains("0061")){
					form.setMstemma0061("1");
				}
				if(MstemmaList.contains("0071")){
					form.setMstemma0071("1");
				}
				if(MstemmaList.contains("0062")){
					form.setMstemma0062("1");
				}
											
			}else {//文系・理系
							
				String[] stemmaCode = CollectionUtil.splitComma(GeneralUtil.toBlank(pd.getStemmaProviso()));
				int Count2 = 0;
				StringBuffer stemma2Code = new StringBuffer();	
				ArrayList stemma2List = new ArrayList();
				for (int i = 0; i < stemmaCode.length; i++) {
					if(!stemmaCode[i].equals("")){
						stemma2List.add(stemmaCode[i]);
					}
				}
							
				/**文系・理系の中系統**/
				if(stemma2List.contains("0100") && stemma2List.contains("0200") && stemma2List.contains("0300") && stemma2List.contains("0400") &&
				   stemma2List.contains("0500") && stemma2List.contains("0600") && stemma2List.contains("0700") && stemma2List.contains("0800") && stemma2List.contains("0900")) {
					//文・人文
					form.setMstemma0011("1");
				}
								
				if(stemma2List.contains("1000") && stemma2List.contains("1100") && stemma2List.contains("1200") && stemma2List.contains("1300") && stemma2List.contains("1400")){
					//社会・国際
					form.setMstemma0021("1");
			   }
			   if(stemma2List.contains("1500") && stemma2List.contains("1600")){
				   //法・政治
					form.setMstemma0022("1");
			   }
			   if(stemma2List.contains("1700") && stemma2List.contains("1800") && stemma2List.contains("1900") && stemma2List.contains("2000")){
				   //経済・経営・商
					form.setMstemma0023("1");
			   }
			   if(stemma2List.contains("2100") && stemma2List.contains("2200") && stemma2List.contains("2300") && stemma2List.contains("2400") && stemma2List.contains("2500") && stemma2List.contains("2600")){
				   //教員養成
					form.setMstemma0031("1");
			   }
			   if(stemma2List.contains("2700") && stemma2List.contains("2800") && stemma2List.contains("2900") && stemma2List.contains("3000") && stemma2List.contains("3100") && stemma2List.contains("3200") &&
				  stemma2List.contains("3300")){
				   //総合科学課程
				   form.setMstemma0032("1");
			   }
			   if(stemma2List.contains("3400") && stemma2List.contains("3500") && stemma2List.contains("3600") && stemma2List.contains("3700") && stemma2List.contains("3800")){
				 //理
				 form.setMstemma0041("1");
			   } 
			   //if(stemma2List.contains("3800") && stemma2List.contains("3900") && stemma2List.contains("4000") && stemma2List.contains("4100") && stemma2List.contains("4200") &&//[3] delete
			   if(stemma2List.contains("3900") && stemma2List.contains("4000") && stemma2List.contains("4100") && stemma2List.contains("4200") &&//[3] add
					   stemma2List.contains("4300") && stemma2List.contains("4400") && stemma2List.contains("4500") && stemma2List.contains("4600") && stemma2List.contains("4700") &&
					   stemma2List.contains("4800") && stemma2List.contains("4900") && stemma2List.contains("5000") && stemma2List.contains("5100") && stemma2List.contains("5200")){
				 //工
				 form.setMstemma0042("1");
			   }
			   if(stemma2List.contains("5300") && stemma2List.contains("5400") && stemma2List.contains("5500") && stemma2List.contains("5600") && stemma2List.contains("5700") && stemma2List.contains("5800")){
				   //農
				   form.setMstemma0043("1");
			   }
			   if(stemma2List.contains("5900") && stemma2List.contains("6000") && stemma2List.contains("6100") && stemma2List.contains("6200") && stemma2List.contains("6300") && stemma2List.contains("6400")){
				   //医・歯・薬・保険
					form.setMstemma0051("1");
			   }
			   if(stemma2List.contains("6500") && stemma2List.contains("6600") && stemma2List.contains("6700") && stemma2List.contains("6800") && stemma2List.contains("6900")){
				   //家政・生活科学
					form.setMstemma0061("1");
			   }
			   if(stemma2List.contains("7000") && stemma2List.contains("7100") && stemma2List.contains("7200") && stemma2List.contains("7300")){
				   //総合・環境・人間・情報
					form.setMstemma0071("1");
			   }
			   if(stemma2List.contains("7400") && stemma2List.contains("7500") && stemma2List.contains("7600") && stemma2List.contains("7700") && stemma2List.contains("7800")){
				   //芸術・体育・他
					form.setMstemma0062("1");
			   }
							 
				/**文系・理系の小系統**/  
				if(stemma2List.contains("0100") && stemma2List.contains("0200")){
					//文学
					form.setSstemma01("1");
				}
				if(stemma2List.contains("0300")){
					//外国文
					form.setSstemma02("1");
				}
				if(stemma2List.contains("0400") && stemma2List.contains("0500") && stemma2List.contains("0600") &&
					stemma2List.contains("0700") && stemma2List.contains("0800") && stemma2List.contains("0900")){
					//哲・史・教・心
					form.setSstemma03("1");
				}
				if(stemma2List.contains("1000") && stemma2List.contains("1100")){
					//社会・福祉
					form.setSstemma04("1");
				}
				if(stemma2List.contains("1200") && stemma2List.contains("1300") && stemma2List.contains("1400")){
					//国際
					form.setSstemma05("1");
				}
				if(stemma2List.contains("1500") && stemma2List.contains("1600")){
					//法政治政
					form.setSstemma06("1");
				}
				if(stemma2List.contains("1700")){
					//経済
					form.setSstemma07("1");
				}
				if(stemma2List.contains("1800") && stemma2List.contains("1900") && stemma2List.contains("2000")){
					//経営・商・会計
					form.setSstemma08("1");
				}
				if(stemma2List.contains("2100")){
					//教員養成-教科
					form.setSstemma09("1");
				}
				if(stemma2List.contains("2200")){
					//教員養成-実技
					form.setSstemma10("1");
				}
				if(stemma2List.contains("2300") && stemma2List.contains("2400") && stemma2List.contains("2500") && stemma2List.contains("2600")){
					//教員養成-その他
					form.setSstemma11("1");
				}
				if(stemma2List.contains("2700") && stemma2List.contains("2800") && stemma2List.contains("2900") && stemma2List.contains("3000") &&
					stemma2List.contains("3100") && stemma2List.contains("3200") && stemma2List.contains("3300")){
					//総合科学課程
					form.setSstemma12("1");
				}
				if(stemma2List.contains("3400") && stemma2List.contains("3500") && stemma2List.contains("3600") && stemma2List.contains("3700") && stemma2List.contains("3800")){
					//理
					form.setSstemma13("1");
				}
				if(stemma2List.contains("3900") && stemma2List.contains("4000")){
					//機械・航空
					form.setSstemma14("1");
				}
				if(stemma2List.contains("4100") && stemma2List.contains("4200")){
					//電気電子・情報
					form.setSstemma15("1");
				}
				if(stemma2List.contains("4300") && stemma2List.contains("4400")){
					//土木・建築
					form.setSstemma16("1");
				}
				if(stemma2List.contains("4500") && stemma2List.contains("4600") && stemma2List.contains("4700") && stemma2List.contains("4800")){
					//応化・応物・資
					form.setSstemma17("1");
				}
				if(stemma2List.contains("4900") && stemma2List.contains("5000") && stemma2List.contains("5100") && stemma2List.contains("5200")){
					//機械・航空
					form.setSstemma18("1");
				}
				if(stemma2List.contains("5300") && stemma2List.contains("5400") && stemma2List.contains("5500") && stemma2List.contains("5600") &&
				   stemma2List.contains("5700") && stemma2List.contains("5800")){
					//農
					form.setSstemma19("1");
				}
				if(stemma2List.contains("5900") && stemma2List.contains("6000")){
					//医・歯
					form.setSstemma20("1");
				}
				if(stemma2List.contains("6100") && stemma2List.contains("6200") && stemma2List.contains("6300") && stemma2List.contains("6400")){
					//薬・看護・保険
					form.setSstemma21("1");
				}
				if(stemma2List.contains("6500") && stemma2List.contains("6600") && stemma2List.contains("6700") && stemma2List.contains("6800")){
					//家政・生活科学
					form.setSstemma22("1");
				}
				if(stemma2List.contains("7000") && stemma2List.contains("7100") && stemma2List.contains("7200") && stemma2List.contains("7300")){
					//総・環・人・情
					form.setSstemma23("1");
				}
				if(stemma2List.contains("7400") && stemma2List.contains("7500") && stemma2List.contains("7600") && stemma2List.contains("7700") && stemma2List.contains("7800")){
					//総・環・人・情
					form.setSstemma24("1");
				}
							
				/**文系・理系の分野コード**/
				if(stemma2List.contains("0100")){
					form.setRegion0100("1");
					form.setRegionflg01(true);
				}
				if(stemma2List.contains("0200")){
					form.setRegion0200("1");
					form.setRegionflg01(true);
				}
				if(stemma2List.contains("0300")){
					form.setRegion0300("1");
					form.setRegionflg02(true);
				}
				if(stemma2List.contains("0400")){
					form.setRegion0400("1");
					form.setRegionflg03(true);
				}
				if(stemma2List.contains("0500")){
					form.setRegion0500("1");
					form.setRegionflg03(true);
				}
				if(stemma2List.contains("0600")){
					form.setRegion0600("1");
					form.setRegionflg03(true);
				}
				if(stemma2List.contains("0700")){
					form.setRegion0700("1");
					form.setRegionflg03(true);
				}
				if(stemma2List.contains("0800")){
					form.setRegion0800("1");
					form.setRegionflg03(true);
				}
				if(stemma2List.contains("0900")){
					form.setRegion0900("1");
					form.setRegionflg03(true);
				}
				if(stemma2List.contains("1000")){
					form.setRegion1000("1");
					form.setRegionflg04(true);
				}
				if(stemma2List.contains("1100")){
					form.setRegion1100("1");
					form.setRegionflg04(true);
				}
				if(stemma2List.contains("1200")){
					form.setRegion1200("1");
					form.setRegionflg05(true);
				}
				if(stemma2List.contains("1300")){
					form.setRegion1300("1");
					form.setRegionflg05(true);
				}
				if(stemma2List.contains("1400")){
					form.setRegion1400("1");
					form.setRegionflg05(true);
				}
				if(stemma2List.contains("1500")){
					form.setRegion1500("1");
					form.setRegionflg06(true);
				}
				if(stemma2List.contains("1600")){
					form.setRegion1600("1");
					form.setRegionflg06(true);
				}
				if(stemma2List.contains("1700")){
					form.setRegion1700("1");
					form.setRegionflg07(true);
				}
				if(stemma2List.contains("1800")){
					form.setRegion1800("1");
					form.setRegionflg08(true);
				}
				if(stemma2List.contains("1900")){
					form.setRegion1900("1");
					form.setRegionflg08(true);
				}
				if(stemma2List.contains("2000")){
					form.setRegion2000("1");
					form.setRegionflg08(true);
				}
				if(stemma2List.contains("2100")){
					form.setRegion2100("1");
					form.setRegionflg09(true);
				}
				if(stemma2List.contains("2200")){
					form.setRegion2200("1");
					form.setRegionflg10(true);
				}
				if(stemma2List.contains("2300")){
					form.setRegion2300("1");
					form.setRegionflg11(true);
				}
				if(stemma2List.contains("2400")){
					form.setRegion2400("1");
					form.setRegionflg11(true);
				}
				if(stemma2List.contains("2500")){
					form.setRegion2500("1");
					form.setRegionflg11(true);
				}
				if(stemma2List.contains("2600")){
					form.setRegion2600("1");
					form.setRegionflg11(true);
				}
				if(stemma2List.contains("2700")){
					form.setRegion2700("1");
					form.setRegionflg12(true);
				}
				if(stemma2List.contains("2800")){
					form.setRegion2800("1");
					form.setRegionflg12(true);
				}
				if(stemma2List.contains("2900")){
					form.setRegion2900("1");
					form.setRegionflg12(true);
				}
				if(stemma2List.contains("3000")){
					form.setRegion3000("1");
					form.setRegionflg12(true);
				}
				if(stemma2List.contains("3100")){
					form.setRegion3100("1");
					form.setRegionflg12(true);
				}
				if(stemma2List.contains("3200")){
					form.setRegion3200("1");
					form.setRegionflg12(true);
				}
				if(stemma2List.contains("3300")){
					form.setRegion3300("1");
					form.setRegionflg12(true);
				}
				if(stemma2List.contains("3400")){
					form.setRegion3400("1");
					form.setRegionflg13(true);
				}
				if(stemma2List.contains("3500")){
					form.setRegion3500("1");
					form.setRegionflg13(true);
				}
				if(stemma2List.contains("3600")){
					form.setRegion3600("1");
					form.setRegionflg13(true);
				}
				if(stemma2List.contains("3700")){
					form.setRegion3700("1");
					form.setRegionflg13(true);
				}
				if(stemma2List.contains("3800")){
					form.setRegion3800("1");
					form.setRegionflg13(true);
				}
				if(stemma2List.contains("3900")){
					form.setRegion3900("1");
					form.setRegionflg14(true);
				}
				if(stemma2List.contains("4000")){
					form.setRegion4000("1");
					form.setRegionflg14(true);
				}
				if(stemma2List.contains("4100")){
					form.setRegion4100("1");
					form.setRegionflg15(true);
				}
				if(stemma2List.contains("4200")){
					form.setRegion4200("1");
					form.setRegionflg15(true);
				}
				if(stemma2List.contains("4300")){
					form.setRegion4300("1");
					form.setRegionflg16(true);
				}
				if(stemma2List.contains("4400")){
					form.setRegion4400("1");
					form.setRegionflg16(true);
				}
				if(stemma2List.contains("4500")){
					form.setRegion4500("1");
					form.setRegionflg17(true);
				}
				if(stemma2List.contains("4600")){
					form.setRegion4600("1");
					form.setRegionflg17(true);
				}
				if(stemma2List.contains("4700")){
					form.setRegion4700("1");
					form.setRegionflg17(true);
				}
				if(stemma2List.contains("4800")){
					form.setRegion4800("1");
					form.setRegionflg17(true);
				}
				if(stemma2List.contains("4900")){
					form.setRegion4900("1");
					form.setRegionflg18(true);
				}
				if(stemma2List.contains("5000")){
					form.setRegion5000("1");
					form.setRegionflg18(true);
				}
				if(stemma2List.contains("5100")){
					form.setRegion5100("1");
					form.setRegionflg18(true);
				}
				if(stemma2List.contains("5200")){
					form.setRegion5200("1");
					form.setRegionflg18(true);
				}
				if(stemma2List.contains("5300")){
					form.setRegion5300("1");
					form.setRegionflg19(true);
				}
				if(stemma2List.contains("5400")){
					form.setRegion5400("1");
					form.setRegionflg19(true);
				}
				if(stemma2List.contains("5500")){
					form.setRegion5500("1");
					form.setRegionflg19(true);
				}
				if(stemma2List.contains("5600")){
					form.setRegion5600("1");
					form.setRegionflg19(true);
				}
				if(stemma2List.contains("5700")){
					form.setRegion5700("1");
					form.setRegionflg19(true);
				}
				if(stemma2List.contains("5800")){
					form.setRegion5800("1");
					form.setRegionflg19(true);
				}
				if(stemma2List.contains("5900")){
					form.setRegion5900("1");
					form.setRegionflg20(true);
				}
				if(stemma2List.contains("6000")){
					form.setRegion6000("1");
					form.setRegionflg20(true);
				}
				if(stemma2List.contains("6100")){
					form.setRegion6100("1");
					form.setRegionflg21(true);
				}
				if(stemma2List.contains("6200")){
					form.setRegion6200("1");
					form.setRegionflg21(true);
				}
				if(stemma2List.contains("6300")){
					form.setRegion6300("1");
					form.setRegionflg21(true);
				}
				if(stemma2List.contains("6400")){
					form.setRegion6400("1");
					form.setRegionflg21(true);
				}
				if(stemma2List.contains("6500")){
					form.setRegion6500("1");
					form.setRegionflg22(true);
				}
				if(stemma2List.contains("6600")){
					form.setRegion6600("1");
					form.setRegionflg22(true);
				}
				if(stemma2List.contains("6700")){
					form.setRegion6700("1");
					form.setRegionflg22(true);
				}
				if(stemma2List.contains("6800")){
					form.setRegion6800("1");
					form.setRegionflg22(true);
				}
				if(stemma2List.contains("6900")){
					form.setRegion6900("1");
					form.setRegionflg22(true);
				}
				if(stemma2List.contains("7000")){
					form.setRegion7000("1");
					form.setRegionflg23(true);
				}
				if(stemma2List.contains("7100")){
					form.setRegion7100("1");
					form.setRegionflg23(true);
				}
				if(stemma2List.contains("7200")){
					form.setRegion7200("1");
					form.setRegionflg23(true);
				}
				if(stemma2List.contains("7300")){
					form.setRegion7300("1");
					form.setRegionflg23(true);
				}
				if(stemma2List.contains("7400")){
					form.setRegion7400("1");
					form.setRegionflg24(true);
				}
				if(stemma2List.contains("7500")){
					form.setRegion7500("1");
					form.setRegionflg24(true);
				}
				if(stemma2List.contains("7600")){
					form.setRegion7600("1");
					form.setRegionflg24(true);
				}
				if(stemma2List.contains("7700")){
					form.setRegion7700("1");
					form.setRegionflg24(true);
				}
				if(stemma2List.contains("7800")){
					form.setRegion7800("1");
					form.setRegionflg24(true);
				}

				if(pd.getSearchStemmaDiv().equals("2")){
					//文系フラグ
					form.setStemmaAflg(true);
			   }else{
					//理系フラグ
					form.setStemmaSflg(true);
			   }
						   
			}			
			//プロファイルからデータ読み込み
			form.setDescImposeDiv(pd.getDescImposeDiv());
			form.setSubImposeDiv(pd.getSubImposeDiv());
			form.setImposeSub(CollectionUtil.deconcatComma(pd.getImposeSub()));
			form.setSchoolDivProviso(CollectionUtil.deconcatComma(pd.getSchoolDivProviso()));//[1] DEL
			form.setStartMonth(RecordProcessor.getMonthDigit(pd.getStartDateProviso()));
			form.setStartDate(RecordProcessor.getDateDigit(pd.getStartDateProviso()));
			form.setEndMonth(RecordProcessor.getMonthDigit(pd.getEndDateProviso()));
			form.setEndDate(RecordProcessor.getDateDigit(pd.getEndDateProviso()));
			form.setRaingDiv(pd.getRaingDiv());
			form.setRaingProviso(CollectionUtil.deconcatComma(pd.getRaingProviso()));//[1] DEL		
			form.setOutOfTergetProviso(CollectionUtil.deconcatComma(pd.getOutOfTergetProviso()));
			form.setDateSearchDiv(pd.getDateSearchDiv());
		}
	}

}

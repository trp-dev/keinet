	/*
 * 作成日: 2004/07/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.taglib.cmfactory;

import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;

import jp.co.fj.keinavi.beans.com_set.CompClassBean;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.com_set.CompClassData;
import jp.co.fj.keinavi.forms.BaseForm;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM501Form;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.ProfileUtil;
/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM501TagFactory extends AbstractCMTagFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.taglib.cmfactory.AbstractCMTagFactory#createCMStatus(javax.servlet.ServletRequest)
	 */
	public String createCMStatus(ServletRequest request, HttpSession session) throws JspException {
		BaseForm form = getBaseForm(request);
		Map item = getItemMap(session);
		ExamSession	examSession = super.getExamSession(session);

		// 全選択
		if (new Short("1").equals(item.get(IProfileItem.CLASS_SELECTION))) {
			return "受験者のいる全クラスを対象にする";

		// 個別選択かつ設定値がある場合
		} else {
			// 比較対象クラスBean
			CompClassBean bean = (CompClassBean) request.getAttribute("CompClassBean");

			// 設定値を取得する
			CompClassData data = ProfileUtil.getCompClassValue(
				super.getProfile(session),
				examSession,
				examSession.getExamData(form.getTargetYear(), form.getTargetExam()),
				bean,
				true);

			if (data != null && data.getIClassData().size() > 0)
				return data.getIClassData().size() + "クラス";
		}

		return "設定なし";
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.util.taglib.cmfactory.AbstractCMTagFactory#createTempCMStatus(javax.servlet.ServletRequest, javax.servlet.http.HttpSession)
	 */
	public String createTempCMStatus(ServletRequest request, HttpSession session) throws JspException {
		CM001Form f001 = super.getCommonForm(session);
		CM501Form f501 = (CM501Form) f001.getActionForm("CM501Form");
		
		// フォームの選択方法がNULLなら設定画面を開いていないので
		// プロファイルの値を評価する
		if (f501 == null) {
			request.setAttribute("form", f001);
			return this.createCMStatus(request, session);

		// セッション設定値
		} else {
			// 全選択
			if ("1".equals(f501.getSelection())) {
				return "受験者のいる全クラスを対象にする";

			// 個別選択
			} else {
				if (f501.getCompare2() == null || f501.getCompare2().length == 0) {
					return "設定なし";
				} else {
					return f501.getCompare2().length + "クラス";
				}
			}
		}
	}

}

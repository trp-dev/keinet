package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.data.maintenance.KNStudentData;
import jp.co.fj.keinavi.util.JpnStringConv;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 受験届修正 - 生徒情報リストBean
 * 
 * 2006.10.06	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class MStudentListBean extends DefaultBean {

	// 学校コード
	private final String schoolCd;
	
	// 対象年度
	private String targetYear;
	// 対象学年
	private int targetGrade = -1;
	// 対象クラス
	private String targetClass;
	// 頭文字
	private String initialKana;
	
	// 生徒情報データのリスト
	private final List studentList = new ArrayList();
	
	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 学校コード
	 */
	public MStudentListBean(final String pSchoolCd) {
		this.schoolCd = pSchoolCd;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = createPreparedStatement();
			rs = ps.executeQuery();
			while (rs.next()) {
				final KNStudentData data = new KNStudentData(0);
				// 個人ID
				data.setIndividualId(rs.getString(1));
				// 年度
				data.setYear(rs.getString(2));
				// 学年
				data.setGrade(rs.getInt(3));
				// クラス
				data.setClassName(rs.getString(4));
				// クラス番号
				data.setClassNo(rs.getString(5));
				// 性別
				data.setSex(rs.getString(6));
				// ひらがな氏名
				data.setNameHiragana(
						JpnStringConv.kkanaHan2Hkana(rs.getString(7)));
				studentList.add(data);
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	// SQLを作る
	private String createQuery() throws SQLException {
		
		final Query query = QueryLoader.getInstance().load("m23");

		// 頭文字
		// 50音以外
		if (initialKana == null) {
			query.append("AND (b.name_kana IS NULL OR TRANSLATE(");
			query.append("TO_SEARCHSTR(SUBSTR(b.name_kana, 1, 1)), ");
			query.append("'.ｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜ', '.') ");
			query.append("IS NOT NULL) ");
		// 50音
		} else if (initialKana.length() > 0) {
			query.append("AND TO_SEARCHSTR(SUBSTR(b.name_kana, 1, 1)) = ? ");
		}
		
		// ORDER BY
		query.append("ORDER BY class_no, sex, name_kana, individualid");
		
		return query.toString();
	}

	/**
	 * @return PreparedStatement
	 */
	private PreparedStatement createPreparedStatement() throws Exception {
		
		final PreparedStatement ps = conn.prepareStatement(createQuery());

		// 対象年度
		ps.setString(1, targetYear);
		// 対象学年
		ps.setInt(2, targetGrade);
		// 対象クラス
		ps.setString(3, targetClass);
		// 学校コード
		ps.setString(4, schoolCd);
		// 頭文字
		if (!StringUtils.isEmpty(initialKana)) {
			ps.setString(5, initialKana);
		}
		
		return ps;
	}
		
	/**
	 * @return targetYear を戻します。
	 */
	public String getTargetYear() {
		return targetYear;
	}

	/**
	 * @return studentList を戻します。
	 */
	public List getStudentList() {
		return studentList;
	}

	/**
	 * @param pTargetGrade 設定する targetGrade。
	 */
	public void setTargetGrade(final String pTargetGrade) {
		targetGrade = Integer.parseInt(pTargetGrade);
	}

	/**
	 * @param pTargetYear 設定する targetYear。
	 */
	public void setTargetYear(final String pTargetYear) {
		targetYear = pTargetYear;
	}

	/**
	 * @param pTargetClass 設定する targetClass。
	 */
	public void setTargetClass(String pTargetClass) {
		targetClass = pTargetClass;
	}

	/**
	 * @param pTargetGrade 設定する targetGrade。
	 */
	public void setTargetGrade(int pTargetGrade) {
		targetGrade = pTargetGrade;
	}

	/**
	 * @param pInitialKana 設定する initialKana。
	 */
	public void setInitialKana(String pInitialKana) {
		initialKana = pInitialKana;
	}

}

package jp.co.fj.keinavi.beans.graph;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * 校内成績分析 - 校内成績 - 設問別成績
 * 
 * 2006.07.27	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class S004GraphTargetBean extends BaseGraphCheckerBean {

	/**
	 * コンストラクタ
	 * 
	 * @param id プロファイルカテゴリID
	 */
	public S004GraphTargetBean(final String id) {
		super(id);
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.GraphCheckerBeanAdapter#hasSubjectGraphSheet(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	public boolean hasSubjectGraphSheet(HttpServletRequest request) {
		// グラフ
		return isValidValue(request, GRAPH);
	}

}

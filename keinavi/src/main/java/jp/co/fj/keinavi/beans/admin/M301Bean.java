package jp.co.fj.keinavi.beans.admin;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 複合クラス削除Bean
 * 
 * 2004.09.17	[新規作成]
 * 
 * 2005.12.28	Yoshimoto KAWAI - TOTEC
 * 				リファクタリング＋削除条件に学校コードを加えた
 *
 * @author 奥村ゆかり
 * @version 1.0
 * 
 */
public class M301Bean extends DefaultBean {

	private String schoolCd;
	private String targetYear;
	private String targetGrade;
	private String targetClassGCd;
	
	public void execute() throws SQLException, Exception {

		deleteClassGroupInfo();
		deleteGroupInfo();
	}

	/**
	 * @throws SQLException
	 */
	private void deleteClassGroupInfo() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			// 複合キーインデックスは先頭が条件にないとFULLSCANとなるため
			// 年度・学校コード・学年を絞込条件に加えた
			ps = conn.prepareStatement(
					"DELETE class_group WHERE year = ? AND bundlecd = ? AND grade = ? AND classgcd = ?");
			ps.setString(1, targetYear);
			ps.setString(2, schoolCd);
			ps.setInt(3, Integer.parseInt(targetGrade));
			ps.setString(4, targetClassGCd);
			
			if (ps.executeUpdate() == 0) {
				throw new SQLException(
						"複合クラスの削除に失敗しました。");
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * @throws SQLException
	 */
	private void deleteGroupInfo() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("DELETE CLASSGROUP WHERE CLASSGCD = ?");
			ps.setString(1, targetClassGCd);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * @param schoolCd 設定する schoolCd。
	 */
	public void setSchoolCd(String schoolCd) {
		this.schoolCd = schoolCd;
	}

	/**
	 * @param targetGrade 設定する targetGrade。
	 */
	public void setTargetGrade(String targetGrade) {
		this.targetGrade = targetGrade;
	}

	/**
	 * @param targetYear 設定する targetYear。
	 */
	public void setTargetYear(String targetYear) {
		this.targetYear = targetYear;
	}

	/**
	 * @param targetClassGCd 設定する targetClassGCd。
	 */
	public void setTargetClassGCd(String targetClassGCd) {
		this.targetClassGCd = targetClassGCd;
	}


}
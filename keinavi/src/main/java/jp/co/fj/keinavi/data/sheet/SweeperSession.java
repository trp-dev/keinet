/*
 * 作成日: 2004/07/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.sheet;

import java.io.File;
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import jp.co.fj.keinavi.util.KNCommonProperty;

/**
 * 帳票出力用の一時ディレクトリの掃除屋
 * 
 * @author kawai
 */
public class SweeperSession implements Serializable, HttpSessionBindingListener {

	public static final String SESSION_KEY = "SweeperSession";

	private String path; // 一時フォルダのパス
	private List keyList = new LinkedList(); // セッションキーのリスト

	/**
	 * コンストラクタ
	 */
	public SweeperSession() {
		try {
			this.path = KNCommonProperty.getKNTempPath();
		} catch (Exception e) {
			throw new InternalError(e.getMessage());
		}
	}

	/* (非 Javadoc)
	 * @see javax.servlet.http.HttpSessionBindingListener#valueBound(javax.servlet.http.HttpSessionBindingEvent)
	 */
	public void valueBound(HttpSessionBindingEvent arg0) {
	}

	/* (非 Javadoc)
	 * @see javax.servlet.http.HttpSessionBindingListener#valueUnbound(javax.servlet.http.HttpSessionBindingEvent)
	 */
	public void valueUnbound(HttpSessionBindingEvent e) {
		// 全ての一時フォルダを削除する
		Iterator ite = this.keyList.iterator();
		while (ite.hasNext()) {
			String key = (String) ite.next();
			this.remove(key);			
		}
	}

	/**
	 * 指定されたセッションキーの一時ディレクトリを消す
	 * @param key
	 */
	private void remove(String key) {
		File dir = new File(path + File.separator + key);
		if (dir == null) return;

		File[] files = dir.listFiles();
		for (int i=0; files != null && i<files.length; i++) files[i].delete();
		dir.delete();			
	}

	/**
	 * 監視中のセッションキーを削除する
	 * @param key
	 */
	public void deleteKey(String key) {
		this.remove(key);
		keyList.remove(key);
	}

	/**
	 * 監視するセッションキーを追加する
	 * @return
	 */
	public void addKey(String key) {
		this.keyList.add(key);
	}

}

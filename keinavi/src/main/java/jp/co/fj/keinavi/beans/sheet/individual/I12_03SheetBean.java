/*
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.sheet.ExtI11ListBean;
import jp.co.fj.keinavi.excel.data.personal.I12SetsumonListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.sql.QueryLoader;

/**
 * 個人成績分析−成績分析面談 設問別成績データリスト印刷用Bean
 * 
 * @author
 *		2005.04.14	kondo		[1]	対象模試が新テストだった場合、種別フラグに"1"を入れるように修正
 *		2005.05.25	kondo		[2] 対象模試が新テストだった場合、全国得点全国得点率を無効値(-999.0f)とする
 *
 * 2009.10.13   Fujito URAKAWA - Totec
 *              「全体成績」追加対応
 * 2009.11.09   Fujito URAKAWA - Totec
 *              「学力レベル」項目を生かした新メニュー対応 
 */
public class I12_03SheetBean extends IndAbstractSheetBean {

	final String NEWTEST_EXAMTYPECD = "31";//[2] add
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		int userCount = 0;

		if(!"i401".equals(backward)){
			/**設問別成績の場合**/
			//設問フラグ
			data.setIntSetsumonFlg(1);
			//面談シートセキュリティスタンプ
			data.setIntSetsumonSecuFlg(((Short) profile.getItemMap(IProfileCategory.I_SCORE_QUE).get(IProfileItem.PRINT_STAMP)).shortValue());
			data.setIntSetsumonShubetsuFlg(getNewTestFlg());//[1] add 新テストフラグ（設問別成績シート）
		}else{
			/**簡単印刷の場合**/
			//設問フラグ
			String[] KyoSheetFlg = CollectionUtil.splitComma((String) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_FORMS));
			if(KyoSheetFlg.length != 0) {data.setIntSetsumonFlg(Integer.parseInt(KyoSheetFlg[2]));}
			//セキュリティスタンプ
			data.setIntSetsumonSecuFlg(((Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.PRINT_STAMP)).shortValue());
			data.setIntSetsumonShubetsuFlg(getNewTestFlg());//[1] add 新テストフラグ（設問別成績シート）
		}

		//個人共通
		ICommonMap iCommonMap =	(ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);

		/*下記条件の時に値を格納
		・面談シートフラグ=2or4
		・教科分析シートフラグ=1*/
		if (data.getIntSetsumonFlg() == 1) {
			PreparedStatement ps2 = null;
			PreparedStatement ps3 = null;
			PreparedStatement ps4 = null;
			PreparedStatement ps5 = null;
			
			ResultSet rs2 = null;
			ResultSet rs3 = null;
			
			String tmp_kmkcd = "";	// 科目コードチェック用変数
			String tmp_scholar_level = "";	//学力レベル文字列
			
			//----------testData---------//
			String nowYear = super.getThisYear(); //現年度
			String examYear = iCommonMap.getTargetExamYear(); //模試年度
			String examCd = iCommonMap.getTargetExamCode(); //模試コード
			//----------testData---------//

			String examTypeCd = iCommonMap.getTargetExamTypeCode();//[2] add

			//１．全生徒
			for (Iterator it=data.getI11List().iterator(); it.hasNext();) {
				
					try {
						
						ExtI11ListBean printData = (ExtI11ListBean) it.next();
						
						//設問別成績検索 
						ps2 = conn.prepareStatement(QueryLoader.getInstance().load("i12_03_1").toString()); //ＳＱＬ２
						ps2.setString(1, printData.getIndividualid());
						ps2.setString(2, examYear);
						ps2.setString(3, examCd);
						ps2.setString(4, examYear);
						ps2.setString(5, examCd);
						ps2.setString(6, printData.getIndividualid());
						ps2.setString(7, profile.getBundleCD());
						
						// 科目別成績検索
						ps3 = conn.prepareStatement(QueryLoader.getInstance().load("i12_03_2").toString());
						ps3.setString(1, printData.getIndividualid());
						ps3.setString(3, profile.getBundleCD());
						ps3.setString(4, examYear);
						ps3.setString(5, examCd);
						ps3.setString(6, examYear);
						ps3.setString(7, examCd);
						
						// 学力レベル検索
						ps4 = conn.prepareStatement(QueryLoader.getInstance().load("i12_03_3").toString());
						ps4.setString(1, printData.getIndividualid());
						ps4.setString(2, examYear);
						ps4.setString(3, examCd);
						
						ps5 = conn.prepareStatement(QueryLoader.getInstance().load("i12_03_4").toString());
						ps5.setString(1, examYear);
						ps5.setString(2, examCd);
						
						rs2 = ps2.executeQuery();
						while (rs2.next()) {

							// 科目名が異なる場合のみ、全体成績データをセットする。
							if (!tmp_kmkcd.equals(rs2.getString("SUBCD"))) {
								// 設問別成績画面からの印刷は、画面に表示されている学力レベルを使用する
								if ("i108".equals(backward)) {
									HashMap map = iCommonMap.getIndSubScholarLevels();
									String levelkey = iCommonMap.makeScholarLevelMapKey(examYear, examCd, 
											printData.getIndividualid(), rs2.getString("SUBCD"));
									if (map.containsKey(levelkey)) {
										tmp_scholar_level = (String)map.get(levelkey);
									}
									// 画面で学力レベルを指定していない科目については対象生徒／科目毎の学力レベルを使用する。
									else {
										tmp_scholar_level = putScholarLevel(rs2, ps4);
									}
								}
								//設問別成績画面以外は、対象生徒／科目毎の学力レベルを使用する。
								else {
									tmp_scholar_level = putScholarLevel(rs2, ps4);
								}
								
								putTotalSubjectData(printData, rs2, ps3, examTypeCd, tmp_scholar_level);
							}
							
							//設問別成績new
							I12SetsumonListBean questionData = new I12SetsumonListBean();
							//設問別成績set
							questionData.setStrKmkmei(rs2.getString("SUBNAME"));				//科目名
							questionData.setStrSetsuNo(rs2.getString("DISPLAY_NO"));			//表示番号
							questionData.setIntTokutenSelf(rs2.getInt("QSCORE"));				//個人得点
							questionData.setFloTokuritsuSelf(rs2.getFloat("SCORERATE"));		//個人得点率
							if(rs2.getInt("QSCORE") == -999 && rs2.getFloat("SCORERATE") == -999.0f) {
								questionData.setStrSetsuMei1(null);			//設問内容
								questionData.setStrSetsuHaiten(null);			//設問配点
								questionData.setFloTokutenHome(-999.0f);		//校内得点
								questionData.setFloTokuritsuHome(-999.0f);		//校内得点率
								questionData.setFloTokutenAll(-999.0f);			//全国得点
								questionData.setFloTokuritsuAll(-999.0f);		//全国得点率
								questionData.setStrScholarLevel("");
								questionData.setFloScholarLevel(-999.0f);
							} else {
								questionData.setStrSetsuMei1(rs2.getString("QUESTIONNAME1"));		//設問内容
								questionData.setStrSetsuHaiten(rs2.getString("QALLOTPNT"));			//設問配点
								questionData.setFloTokutenHome(rs2.getFloat("AVGPNT_S"));			//校内得点
								questionData.setFloTokuritsuHome(rs2.getFloat("AVGSCORERATE_S"));	//校内得点率
								
								if(examTypeCd.equals(NEWTEST_EXAMTYPECD)) {
									questionData.setFloTokutenAll(-999.0f);			//全国得点
									questionData.setFloTokuritsuAll(-999.0f);		//全国得点率
									questionData.setStrScholarLevel("");
									questionData.setFloScholarLevel(-999.0f);
								} else {
									questionData.setFloTokutenAll(rs2.getFloat("AVGPNT_A"));			//全国得点
									questionData.setFloTokuritsuAll(rs2.getFloat("AVGSCORERATE_A"));	//全国得点率
									
									ps5.setString(3, rs2.getString("SUBCD"));
									ps5.setString(4, rs2.getString("QUESTION_NO"));
									ps5.setString(5, tmp_scholar_level);
									rs3 = ps5.executeQuery();
									questionData.setStrScholarLevel(tmp_scholar_level);
									if (rs3.next()) {
										questionData.setFloScholarLevel(rs3.getFloat("AVGSCORERATE_LEVEL"));
									}
									else {
										questionData.setFloScholarLevel(-999.0f);
									}
									rs3.close();
								}
							}
							
							//設問別成績List.add
							printData.getI12SetsumonList().add(questionData);
							// 処理中の科目コードをセット
							tmp_kmkcd = rs2.getString("SUBCD");
						}
						//生徒List.add
						//data.getI11List().add(printData);
					
				} finally {
					DbUtils.closeQuietly(ps2);
					DbUtils.closeQuietly(ps3);
					DbUtils.closeQuietly(ps4);
					DbUtils.closeQuietly(ps5);
					DbUtils.closeQuietly(rs2);
					DbUtils.closeQuietly(rs3);
				}
			}
		}
		// 帳票を作成する
//		if (!new Personal().personal(data,	outfileList, new ArrayList(), Integer.parseInt(action), sessionID)) {
//			throw new Exception("帳票の作成に失敗しました。");
//		}

	}
	
	/**
	 * 対象生徒の学力レベルを取得する
	 * @param rs2
	 * @param ps
	 * @return 存在しない場合、または取得した学力レベルがnullの場合には空文字を返す
	 * @throws SQLException 
	 */
	private String putScholarLevel(ResultSet rs2, PreparedStatement ps) throws SQLException {
		
		ResultSet rs = null;
		try {
			ps.setString(4, rs2.getString("SUBCD"));
			rs = ps.executeQuery();
			
			if (rs.next()) {
				return GeneralUtil.toBlank(rs.getString("ACADEMICLEVEL"));
			}
			
			return "";
		}
		finally {
			DbUtils.closeQuietly(rs);
		}
	}

	/**
	 * 設問別成績の先頭に「全体成績」データをセットする
	 * @param printData
	 * @param rs2
	 * @param ps3
	 * @param examTypeCd 
	 * @param lebel
	 * @throws SQLException 
	 */
	private void putTotalSubjectData(ExtI11ListBean printData, ResultSet rs2,
			PreparedStatement ps3, String examTypeCd, String level) throws SQLException {
		
		ResultSet rs3 = null;
		
		try {
			ps3.setString(2, level);
			ps3.setString(8, rs2.getString("SUBCD"));
			
			rs3 = ps3.executeQuery();
			
			if (rs3.next()) {
				//設問別成績new
				I12SetsumonListBean questionData = new I12SetsumonListBean();
				//設問別成績set
				questionData.setStrKmkmei(rs3.getString("SUBNAME"));				//科目名
				questionData.setStrSetsuMei1("全体成績");							//設問内容（"全体成績"をセットする）
				questionData.setIntTokutenSelf(rs3.getInt("SSCORE"));				//個人得点
				questionData.setFloTokuritsuSelf(rs3.getFloat("SCORERATE"));		//個人得点率
				if(rs3.getInt("SSCORE") == -999 && rs3.getFloat("SCORERATE") == -999.0f) {
					questionData.setStrSetsuMei1(null);			//設問内容
					questionData.setStrSetsuHaiten(null);			//設問配点
					questionData.setFloTokutenHome(-999.0f);		//校内得点
					questionData.setFloTokuritsuHome(-999.0f);		//校内得点率
					questionData.setFloTokutenAll(-999.0f);			//全国得点
					questionData.setFloTokuritsuAll(-999.0f);		//全国得点率
				} else {
					questionData.setStrSetsuNo("");								//表示番号（""をセットする）
					questionData.setStrSetsuHaiten(rs3.getString("SUBALLOTPNT"));		//設問配点
					questionData.setFloTokutenHome(rs3.getFloat("AVGPNT_S"));			//校内得点
					questionData.setFloTokuritsuHome(rs3.getFloat("AVGSCORERATE_S"));	//校内得点率
					if(examTypeCd.equals(NEWTEST_EXAMTYPECD)) {
						questionData.setFloTokutenAll(-999.0f);			//全国得点
						questionData.setFloTokuritsuAll(-999.0f);		//全国得点率
					} else {
						questionData.setFloTokutenAll(rs3.getFloat("AVGPNT_A"));			//全国得点
						questionData.setFloTokuritsuAll(rs3.getFloat("AVGSCORERATE_A"));	//全国得点率
					}
					//[2] add end
				}
				questionData.setStrScholarLevel(level);
				questionData.setFloScholarLevel(rs3.getFloat("AVGSCORERATE_LEVEL"));
				//設問別成績List.add
				printData.getI12SetsumonList().add(questionData);
			}
		}
		finally {
			DbUtils.closeQuietly(rs3);
		}
		
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.I_SCORE_QUE;
	}

}

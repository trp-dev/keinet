package jp.co.fj.keinavi.beans.sheet.common;

import java.util.List;

/**
 *
 * 帳票作成基本クラス
 * 
 * 2005.10.31	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public abstract class BaseSheetCreator {

	// 帳票データクラス
	private final ISheetData data;
	// シーケンスID
	private final String sequenceId;
	// 出力ファイルリスト
	private final List outFileList;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pData
	 * @param pSequenceId
	 * @param pOutFileList
	 */
	public BaseSheetCreator(final ISheetData pData,
			final String pSequenceId, final List pOutFileList) {
		
		if (pData == null) {
			throw new IllegalArgumentException(
					"帳票データクラスがNULLです。"); 
		}
		
		if (pSequenceId == null) {
			throw new IllegalArgumentException(
					"シーケンスIDがNULLです。");
		}
		
		if (pOutFileList == null) {
			throw new IllegalArgumentException(
					"出力ファイルリストがNULLです。");
		}
		
		this.data = pData;
		this.sequenceId = pSequenceId;
		this.outFileList = pOutFileList;
	}

	/**
	 * 帳票データから帳票を作る
	 * 
	 * @param data
	 * @throws Exception
	 */
	protected abstract void createSheet(ISheetData pData) throws Exception;

	/**
	 * 出力ファイルを追加する
	 * 
	 * @param path
	 */
	protected void addOutFile(final String path) {
		
		outFileList.add(path);
	}

	/**
	 * 出力ファイルを入れ替える
	 * 
	 * @param srcPath
	 * @param destPath
	 */
	protected void replaceOutFile(final String srcPath, final String destPath) {
		
		final int index = outFileList.indexOf(srcPath);
		if (index >= 0) {
			outFileList.remove(index);
			outFileList.add(index, destPath);
		}
	}
	
	/**
	 * @throws Exception
	 */
	protected void initSheet() throws Exception {
		
	}
	
	/**
	 * @throws Exception
	 */
	public void execute() throws Exception {

		initSheet();
		createSheet(data);
		destroySheet();
	}

	/**
	 * @throws Exception
	 */
	protected void destroySheet() throws Exception {
		
	}
	
	/**
	 * @return sequenceId を戻します。
	 */
	protected String getSequenceId() {
		return sequenceId;
	}

}

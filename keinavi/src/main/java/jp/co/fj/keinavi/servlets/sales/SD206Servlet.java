package jp.co.fj.keinavi.servlets.sales;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.sales.SD206ListBean;
import jp.co.fj.keinavi.data.sales.SD206Data;
import jp.co.fj.keinavi.forms.sales.SD206Form;
import jp.co.fj.keinavi.interfaces.SpecialAppliMenu;
import jp.co.fj.keinavi.servlets.sales.base.BaseSpecialAppliServlet;
import jp.co.fj.keinavi.util.KNUtil;

import org.apache.commons.lang.StringUtils;

/**
 *
 * 特例成績データ　申請一覧確認（パスワード通知書ダウンロード）画面のサーブレットです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SD206Servlet extends BaseSpecialAppliServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = -538036133101014760L;

	/**
	 * @see jp.co.fj.keinavi.servlets.sales.base.BaseSpecialAppliServlet#getMenu()
	 */
	protected SpecialAppliMenu getMenu() {
		return SpecialAppliMenu.ACCEPT_KANRI;
	}

	/**
	 * JSP表示処理を行います。
	 */
	public void index(HttpServletRequest request, HttpServletResponse response,
			SD206Form form) throws Exception {

		/* 現年度をリクエストスコープにセットする */
		request.setAttribute("currentYear", KNUtil.getCurrentYear());

		/* 申請リストをリクエストスコープにセットする */
		request.setAttribute("results", findResults(request, form));

		forward2Jsp(request, response);
	}

	/**
	 * 申請リストを取得します。
	 */
	private List findResults(HttpServletRequest request, SD206Form form)
			throws Exception {

		/* 対象年度の初期値をセットする */
		if (StringUtils.isEmpty(form.getExamYear())) {
			form.setExamYear(KNUtil.getCurrentYear());
		}

		/* 絞込み条件の初期値をセットする */
		if (StringUtils.isEmpty(form.getCondition())) {
			form.setCondition("1");
		}

		/* ソートキーの初期値をセットする */
		if (StringUtils.isEmpty(form.getSortKey())) {
			form.setSortKey("5");
		}

		SD206ListBean bean = new SD206ListBean(form.getExamYear(),
				form.getCondition(), form.getSortKey());
		executeBean(request, bean);

		return bean.getResults();
	}

	/**
	 * DBから指定した申請IDのデータを取得します。
	 */
	private SD206Data findData(HttpServletRequest request, SD206Form form,
			String appId) throws Exception {

		List results = findResults(request, form);

		for (Iterator ite = results.iterator(); ite.hasNext();) {
			SD206Data data = (SD206Data) ite.next();
			if (data.getAppId().equals(appId)) {
				return data;
			}
		}

		// 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
		//throw new RuntimeException("指定された申請データは利用できません。" + appId);
		throw new IllegalStateException("指定された申請データは利用できません。" + appId);
		// 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
	}

	/**
	 * 印刷処理を行います。
	 */
	public void print(HttpServletRequest request, HttpServletResponse response,
			SD206Form form) throws Exception {

		/* 申請IDをセッションに保持する */
		request.getSession(false).setAttribute("SD206Form.appId",
				findData(request, form, form.getAppId()).getAppId());

		/* 終了時にファイル削除処理を起動するように、セッションにフラグを保持する */
		request.getSession(false).setAttribute("SheetPrinted", Boolean.TRUE);

		/* 印刷フラグをパラメータに追加する */
		Map param = new HashMap();

		// 2019/09/04 QQ)Tanioka 特例成績データ申請一覧確認画面 ダウンロード障害修正 UPD START
		//param.put("printFlag", "2");
		param.put("printFlag", "5");
		// 2019/09/04 QQ)Tanioka 特例成績データ申請一覧確認画面 ダウンロード障害修正 UPD END

		redirect(request, response, "sheet", param);
	}

}

/**
 * 校内成績分析−過回比較　偏差値分布
 * 	Excelファイル編集
 * 作成日: 2004/08/17
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S52BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S52Item;
import jp.co.fj.keinavi.excel.data.school.S52ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S52_06 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	final private int	intMaxSheetSr	= 20;			// 最大シート数	
	final private String	masterfile0		= "S52_06";		// ファイル名
	final private String	masterfile1		= "NS52_06";	// ファイル名
	private String	masterfile		= "";					// ファイル名
	final private int[]	tabCol			= {0,23};		// 表の基準点


/*
 * 	Excel編集メイン
 * 		S52Item s52Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s52_06EditExcel(S52Item s52Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("S52_06","S52_06帳票作成開始","");
		
		//テンプレートの決定
		if (s52Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			
			// データセット
			ArrayList	s52List			= s52Item.getS52List();
			Iterator	itr				= s52List.iterator();

			float		min				= 75.0f;// 偏差値
			int		writeCnt		= 0;	// 書き込みカウンター
			int		sumNinzu		= 0;	// 人数計算用
			int		row				= 0;	// 行
			int		ninzu			= 0;	// *作成用
			int 		setRow			= -1;	// *セット用
			int		hyouCnt			= 0;	// 値範囲：０〜１　(表カウンター)
			int		kakaiCnt		= 0;	// 回カウンター
			int		maxSheetIndex	= 0;	// シートカウンター
			int		intBookCngCount	= 0;	// ブックカウンター
			int		kmkCd			= 0;	// 科目コード保持
			String		moshi			= "";	// 模試月
			boolean	kataFlg			= true;// false:改行済(型・科目名変更済)　
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	kmkmeiFlg		= true;// true:型・科目名表示
//add 2004/10/27 T.Sakai データ0件対応
			int		dispKmkFlgCnt	= 0;	// グラフ表示フラグカウンタ
//add end
//add 2004/10/27 T.Sakai セル計算対応
			int		ruikeiNinzu		= 0;	// 累計人数
//add end

			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
			if( workbook==null ){
				return errfread;
			}

			// 基本ファイルを読込む
			S52ListBean s52ListBean = new S52ListBean();

			while( itr.hasNext() ) {
				s52ListBean = (S52ListBean)itr.next();
				if ( s52ListBean.getIntDispKmkFlg()==1 ) {
//add 2004/10/27 T.Sakai データ0件対応
					dispKmkFlgCnt++;
//add end
					// 基本ファイルを読み込む
					S52BnpListBean		s52BnpListBean		= new S52BnpListBean();
	
					//データの保持
					ArrayList	s52BnpList		= s52ListBean.getS52BnpList();
					Iterator	itrS52Bnp		= s52BnpList.iterator();

					// カウンターチェック
					if (kakaiCnt!=0){
						if ( ( (Integer.parseInt(s52ListBean.getStrKmkCd()) != kmkCd )||(kakaiCnt>=7) )){
							kakaiCnt = 0;
							kmkmeiFlg=true;
							hyouCnt++;
							if(hyouCnt >= 2){
								hyouCnt = 0;
								bolSheetCngFlg = true;
							}
						}
					}
	
	
					//型から科目に変わる時のチェック
					if ( s52ListBean.getStrKmkCd().compareTo("7000") < 0 ) {
	
						//型・科目切替えの初回のみ処理する
						if (kataFlg){
							if(hyouCnt==1){
								hyouCnt	= 0;
								kataFlg	= false;
								bolSheetCngFlg	= true;
							}
							if(hyouCnt==0){
								kataFlg	= false;
							}
						}
					}
					
	
					if(bolSheetCngFlg){
						if(maxSheetIndex >= intMaxSheetSr){
							// Excelファイル保存
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, maxSheetIndex);

							if( bolRet == false ){
								return errfwrite;					
							}

							//マスタExcel読み込み
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook==null ){
								return errfread;
							}
							intBookCngCount++;
							maxSheetIndex = 0;
						
						}
						// データセットするシートの選択
						workSheet = workbook.getSheet(Integer.toString(maxSheetIndex+1));
						maxSheetIndex++;
	
						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);
	
						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, s52Item.getIntSecuFlg() ,42 ,44 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 42 );
						workCell.setCellValue(secuFlg);
	
						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
						workCell.setCellValue( "学校名　：" + cm.toString(s52Item.getStrGakkomei()) );
	
						// 対象模試セット
						moshi =cm.setTaisyouMoshi( s52Item.getStrMshDate() );	// 模試月取得
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s52Item.getStrMshmei()) + moshi);
	
						bolSheetCngFlg =false;
					}
	
					// 型名・配点セット
					if(kmkmeiFlg==true){
						String haiten = "";
						if ( !cm.toString(s52ListBean.getStrHaitenKmk()).equals("") ) {
							haiten = "（" + s52ListBean.getStrHaitenKmk() + "）";
						}
						workCell = cm.setCell( workSheet, workRow, workCell, 6, tabCol[hyouCnt] );
						workCell.setCellValue( "型・科目：" + cm.toString(s52ListBean.getStrKmkmei()) + haiten );
						kmkCd=Integer.parseInt(s52ListBean.getStrKmkCd());

						kmkmeiFlg=false;
					}

					// 模試名セット
					moshi =cm.setTaisyouMoshi( s52ListBean.getStrMshDate() );	// 模試月取得
					workCell = cm.setCell( workSheet, workRow, workCell, 40, tabCol[hyouCnt]+1+3*kakaiCnt );
					workCell.setCellValue( cm.toString(s52ListBean.getStrMshmei()) + moshi);

					// 合計人数セット
					if ( s52ListBean.getIntNinzu() != -999 ) {
						workCell = cm.setCell( workSheet, workRow, workCell,  52, tabCol[hyouCnt]+1+3*kakaiCnt );
						workCell.setCellValue( s52ListBean.getIntNinzu() );
					}
	
					// 平均点セット
					if ( s52ListBean.getFloHeikin() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell,  53, tabCol[hyouCnt]+1+3*kakaiCnt );
						workCell.setCellValue( s52ListBean.getFloHeikin() );
					}
	
					// 平均偏差値セット
					if ( s52ListBean.getFloHensa() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell,  54, tabCol[hyouCnt]+1+3*kakaiCnt );
						workCell.setCellValue( s52ListBean.getFloHensa()); 
					}

					//　偏差値データセット
					row	= 0;
					if (s52Item.getIntShubetsuFlg() == 1){
						min = 180.0f;
					} else{
						min = 75.0f;
					}
					writeCnt=0;
					sumNinzu = 0;
//add 2004/10/27 T.Sakai セル計算対応
					ruikeiNinzu = 0;
//add end
					
					while(itrS52Bnp.hasNext()){
						s52BnpListBean	= (S52BnpListBean) itrS52Bnp.next();
	
						// 人数セット
						if ( s52BnpListBean.getFloBnpMin()==min ) {
							if ( s52BnpListBean.getIntNinzu() != -999 ) {
								sumNinzu += s52BnpListBean.getIntNinzu();
							}
//add 2004/10/27 T.Sakai セル計算対応
							// 累計人数セット
							workCell = cm.setCell( workSheet, workRow, workCell,42+writeCnt, tabCol[hyouCnt]+3+3*kakaiCnt);
							ruikeiNinzu = ruikeiNinzu + sumNinzu;
							workCell.setCellValue( ruikeiNinzu );

							// 得点用のときは処理しない
							if (s52Item.getIntShubetsuFlg() == 1){
							} else{
								if (s52BnpListBean.getFloBnpMin()==60.0f) {
									// 偏差値60以上の人数セット
									workCell = cm.setCell( workSheet, workRow, workCell,  55, tabCol[hyouCnt]+1+3*kakaiCnt );
									workCell.setCellValue( ruikeiNinzu );
								}
								if (s52BnpListBean.getFloBnpMin()==50.0f) {
									// 偏差値50以上の人数セット
									workCell = cm.setCell( workSheet, workRow, workCell,  56, tabCol[hyouCnt]+1+3*kakaiCnt );
									workCell.setCellValue( ruikeiNinzu );
								}
							}
//add end
							if ( sumNinzu != 0 ) {
								workCell = cm.setCell( workSheet, workRow, workCell,42+writeCnt, tabCol[hyouCnt]+2+3*kakaiCnt);
								workCell.setCellValue( sumNinzu );
	
								// *セット準備
								if( sumNinzu > ninzu ){
									ninzu = sumNinzu;
									setRow = writeCnt;
								}
							}
							writeCnt++;
							sumNinzu = 0;
							if (s52Item.getIntShubetsuFlg() == 1){
								min -= 20.0f;
							} else{
								min -= 5.0f;
							}
						} else {
							if ( s52BnpListBean.getIntNinzu() != -999 ) {
								sumNinzu +=s52BnpListBean.getIntNinzu();
							}
	
							// *セット
							if(s52BnpListBean.getFloBnpMin() == -999.0){
//add 2004/10/27 T.Sakai セル計算対応
								// 累計人数セット
								workCell = cm.setCell( workSheet, workRow, workCell,42+writeCnt, tabCol[hyouCnt]+3+3*kakaiCnt);
								ruikeiNinzu = ruikeiNinzu + sumNinzu;
								workCell.setCellValue( ruikeiNinzu );
//add end
								if ( sumNinzu != 0 ) {
									workCell = cm.setCell( workSheet, workRow, workCell,42+writeCnt, tabCol[hyouCnt]+2+3*kakaiCnt);
									workCell.setCellValue( sumNinzu );
	
									// *セット準備
									if( sumNinzu > ninzu ){
										ninzu = sumNinzu;
										setRow = writeCnt;
									}
								}
								sumNinzu = 0;
	
								if(setRow!=-1){
									workCell = cm.setCell( workSheet, workRow, workCell,42+setRow, tabCol[hyouCnt]+1+3*kakaiCnt );
									workCell.setCellValue("*");
								}
								ninzu=0;
								setRow=-1;
							}
						}
	
						row++;
					}
					kakaiCnt++;
				}
			}
//add 2004/10/27 T.Sakai データ0件対応
			if ( dispKmkFlgCnt==0 ) {
				// マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				maxSheetIndex	= 0;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet(Integer.toString(maxSheetIndex+1));
				maxSheetIndex++;
	
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
	
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s52Item.getIntSecuFlg() ,42 ,44 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 42 );
				workCell.setCellValue(secuFlg);
	
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s52Item.getStrGakkomei()) );
	
				// 対象模試セット
				moshi =cm.setTaisyouMoshi( s52Item.getStrMshDate() );	// 模試月取得
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s52Item.getStrMshmei()) + moshi);
			}
//add end
			boolean bolRet = false;
			// Excelファイル保存
			if(intBookCngCount == 1){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
			}else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, maxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;
			}

			
		} catch(Exception e) {
			log.Err("S52_06","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S52_06","S52_06帳票作成終了","");
		return noerror;
	}

}
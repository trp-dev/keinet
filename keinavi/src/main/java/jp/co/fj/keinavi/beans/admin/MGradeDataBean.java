/*
 * 作成日: 2004/09/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.admin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.data.admin.MGradeData;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 学年データを保持するBean
 * 
 * @author kawai
 */
public class MGradeDataBean extends DefaultBean {

	List gradeDataList = new LinkedList(); // 学年データリスト
	List yearList; // 年度のリスト
	String schoolCD; // 学校コード

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("m05").toString());
			ps.setString(1, this.schoolCD);
			ps.setString(2, this.schoolCD);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				MGradeData data = new MGradeData();
				data.setYear(rs.getString(1));	
				data.setGrade(rs.getString(2));	
				this.gradeDataList.add(data);	
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * @return
	 */
	public List getYearList() {
		if (yearList == null) {
			yearList = new ArrayList();
			Iterator ite = this.gradeDataList.iterator();
			while (ite.hasNext()) {
				MGradeData data = (MGradeData) ite.next();
				if (!yearList.contains(data.getYear())) yearList.add(data.getYear());
			}
		}
		return yearList;
	}

	/**
	 * @return
	 */
	public String getSchoolCD() {
		return schoolCD;
	}
	/**
	 * @param string
	 */
	public void setSchoolCD(String string) {
		schoolCD = string;
	}

	/**
	 * @return
	 */
	public List getGradeDataList() {
		return gradeDataList;
	}

}

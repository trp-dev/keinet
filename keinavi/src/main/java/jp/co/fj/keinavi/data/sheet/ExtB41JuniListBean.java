/*
 * 作成日: 2004/10/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.sheet;

import jp.co.fj.keinavi.excel.data.business.B41JuniListBean;

/**
 * 高校成績分析・高校間比較・成績概況
 * 拡張データクラス
 * 
 * 2005.04.20	Yoshimoto KAWAI - Totec
 * 				新テストの場合はソートに平均偏差値ではなく
 * 				平均得点を利用するようにした
 * 
 * 2005.05.13	Yoshimoto KAWAI - Totec
 * 				項目入れ替えに伴って常に平均偏差値を見るようにした
 * 				（つまり、修正前と同じ動作）
 * 
 * @author kawai
 */
public class ExtB41JuniListBean extends B41JuniListBean implements Comparable {

	private String schoolCD; // 学校コード
	private boolean newExam; // 新テストかどうか
	
	/**
	 * コンストラクタ
	 * 
	 * @param newExam
	 */
	public ExtB41JuniListBean(boolean newExam) {
		super();
		this.newExam = newExam;
	}
	
	/* (非 Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		ExtB41JuniListBean j = (ExtB41JuniListBean) o;
		
		final float a;
		final float b;
		// 新テストは平均得点
		//if (newExam) {
		//	a = this.getFloHeikinNow();
		//	b = j.getFloHeikinNow();

		// それ以外は平均偏差値
		//} else {
			a = this.getFloStdHensaNow();
			b = j.getFloStdHensaNow();
		//}
		
		// 平均得点または平均偏差値の降順・学校コードの昇順
		if (a > b) {
			return -1;
		} else if (a < b) {
			return 1;
		} else {
			return this.getSchoolCD().compareTo(j.getSchoolCD());
		}
	}

	/**
	 * @return
	 */
	public String getSchoolCD() {
		return schoolCD;
	}

	/**
	 * @param string
	 */
	public void setSchoolCD(String string) {
		schoolCD = string;
	}


}

/*
 * 作成日: 2004/10/07
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.individual.ProvisoData;

import com.fjh.beans.DefaultSearchBean;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ProvisoSearchBean extends DefaultSearchBean{
	
	private static final long serialVersionUID = -4940779635644104038L;
	
	private String individualId;
	
	private String query = "" +
	"SELECT"+
	" NVL(INDIVIDUALID, ' ') INDIVIDUALID"+",NVL(DISTRICTPROVISO, ' ') DISTRICTPROVISO"+
	",NVL(SEARCHSTEMMADIV, '1') SEARCHSTEMMADIV"+",NVL(STEMMAPROVISO, ' ') STEMMAPROVISO"+
	",NVL(DESCIMPOSEDIV, '0') DESCIMPOSEDIV"+",NVL(SUBIMPOSEDIV, '1') SUBIMPOSEDIV"+
	",NVL(IMPOSESUB, ' ') IMPOSESUB"+",NVL(SCHOOLDIVPROVISO, ' ') SCHOOLDIVPROVISO"+
	",NVL(STARTDATEPROVISO, ' ') STARTDATEPROVISO"+",NVL(ENDDATEPROVISO, ' ') ENDDATEPROVISO"+
	",NVL(RAINGDIV, '1') RAINGDIV"+",NVL(RAINGPROVISO, ' ') RAINGPROVISO"+",NVL(OUTOFTERGETPROVISO, ' ') OUTOFTERGETPROVISO"+
	",NVL(DATESEARCHDIV, '0') DATESEARCHDIV "+
	" FROM HANGUPPROVISO WHERE 1=1 ";

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		if(individualId != null){
			query += " AND INDIVIDUALID = ?";
		}
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = conn.prepareStatement(query);
			if(individualId != null){
				pstmt.setString(1, individualId);
			}
			rs = pstmt.executeQuery();
			while(rs.next()){
				ProvisoData pd = new ProvisoData();
				pd.setIndividualId(rs.getString("INDIVIDUALID"));
				pd.setDistrictProviso(rs.getString("DISTRICTPROVISO"));
				pd.setSearchStemmaDiv(rs.getString("SEARCHSTEMMADIV"));
				pd.setStemmaProviso(rs.getString("STEMMAPROVISO"));
				pd.setDescImposeDiv(rs.getString("DESCIMPOSEDIV"));
				pd.setSubImposeDiv(rs.getString("SUBIMPOSEDIV"));
				pd.setImposeSub(rs.getString("IMPOSESUB"));
				pd.setSchoolDivProviso(rs.getString("SCHOOLDIVPROVISO"));
				pd.setStartDateProviso(rs.getString("STARTDATEPROVISO"));
				pd.setEndDateProviso(rs.getString("ENDDATEPROVISO"));
				pd.setRaingDiv(rs.getString("RAINGDIV"));
				pd.setRaingProviso(rs.getString("RAINGPROVISO"));
				pd.setOutOfTergetProviso(rs.getString("OUTOFTERGETPROVISO"));
				pd.setDateSearchDiv(rs.getString("DATESEARCHDIV"));
				recordSet.add(pd);
			}
		}catch(SQLException sqle){
			System.out.println(query);
			throw sqle;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

}

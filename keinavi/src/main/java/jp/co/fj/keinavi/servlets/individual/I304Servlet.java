package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.individual.EntExamModeBean;
import jp.co.fj.keinavi.beans.individual.ExamPlanUnivSearchBean;
import jp.co.fj.keinavi.beans.individual.GuideRemarksSearchBean;
import jp.co.fj.keinavi.beans.individual.PlanUnivCopyBean;
import jp.co.fj.keinavi.beans.individual.PlanUnivCreateBean;
import jp.co.fj.keinavi.beans.individual.PlanUnivSearchBean;
import jp.co.fj.keinavi.beans.individual.PlanUnivSearchUnivBean;
import jp.co.fj.keinavi.beans.individual.PlanUnivUpdateBean;
import jp.co.fj.keinavi.beans.individual.UnivSearchBean;
import jp.co.fj.keinavi.data.individual.ExamPlanUnivData;
import jp.co.fj.keinavi.data.individual.GuideRemarksData;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.NestedPlanUnivData;
import jp.co.fj.keinavi.forms.individual.I304Form;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.date.SpecialDateUtil;
import jp.co.fj.keinavi.util.db.RecordProcessor;
import jp.co.fj.keinavi.util.individual.ExamPlanUnivSorter;
import jp.co.fj.keinavi.util.individual.IPropsLoader;

import jp.co.fj.keinavi.servlets.KNServletException;

/**
 * 受験予定大学詳細画面
 * 
 * 
 * 2004/08/03 	Tomohisa YAMADA - Totec
 * 				新規作成
 * 
 * 2005.03.10 	Tomohisa YAMADA - Totec
 * 				jspに渡す為の年度をフォームに設定
 * 
 * <2010年度改修>
 * 2009.01.19   Tomohisa YAMADA - Totec
 *              入試日程変更
 *
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 *
 */
public class I304Servlet extends IndividualServlet{
	
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
			
		//フォームデータを取得
		I304Form form = null;
		try {
			form = (I304Form)super.getActionForm(request, "jp.co.fj.keinavi.forms.individual.I304Form");
		} catch (Exception e) {
			KNServletException k = new KNServletException("0I304にてフォームデータの収得に失敗しました。" + e);
			k.setErrorCode("00I304010101");
			throw k;
		}
		
		HttpSession session = request.getSession();
		
		//個人共通
		ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);
		
		if ("i304".equals(getForward(request))) {//転送:JSP
			
			String errorMessage = null;
			Connection con = null;
			String forwardDestination = JSP_I304;
			
			try{
				con = getConnectionPool(request);
				con.setAutoCommit(false);
				
				//「更新」ボタンを押した
				if (form.getButton() != null 
						&& form.getButton().equals("update")) {
					
					errorMessage = update(errorMessage, form, con);
					
					//正常
					if(errorMessage == null) {
						forwardDestination = JSP_CLOSE;
					}
					//エラーならば、画面に戻って、エラー表示
					else {
						form.setErrorMessage(errorMessage);
						form.setButton(form.getPreButton());
					}
				}

				//「編集」「複製」「新規」の何れかは再表示処理を実行
				if (form.getButton() != null 
						&& !form.getButton().equals("update") 
						|| errorMessage != null) {

					//表示用受験予定大学の検索
					//「更新」時以外は再表示を行う
					final PlanUnivSearchUnivBean planUnivSearchBean = 
						searchPlanUniv(request, form, con, errorMessage);
					request.setAttribute("planUnivSearchBean", planUnivSearchBean);
					form.setRemarks(planUnivSearchBean.getRemarks());
						
					/** 入試日・受験日が一日でもあるかどうか(表示の為) */
					request.setAttribute("isDateExist", new Boolean(isDateExist(planUnivSearchBean)));
						
					/** 入試形態一覧の取得 */
					EntExamModeBean entExamModeBean = new EntExamModeBean();
					entExamModeBean.setConnection("", con);
					entExamModeBean.execute();
					request.setAttribute("entExamModeBean", entExamModeBean);
					
					/** 学部・学科一覧の取得 */
					//データが全くなかった場合にも大学名などは必要
					UnivSearchBean usb = new UnivSearchBean();
					usb.setUnivCd(planUnivSearchBean.getUnivCd());
					usb.setConnection("", con);
					usb.execute();
					request.setAttribute("univSearchBean", usb);
					
					/** ガイドラインの取得 */
					GuideRemarksSearchBean grsb = new GuideRemarksSearchBean();
					grsb.setUnivCd(form.getUnivCd());
					grsb.setFacultyCd(form.getFacultyCd());
					grsb.setDeptCd(form.getDeptCd());
					grsb.setConnection("", con);
					grsb.execute();
					if(grsb.getRecordSet() != null && grsb.getRecordSet().size() > 0){
						request.setAttribute("guideRemarksData", (GuideRemarksData)grsb.getRecordSet().get(0));
					}else{
						request.setAttribute("guideRemarksData", null);
					}
					
				}
				
				con.commit();
				
			} catch(Exception e){
				e.printStackTrace();
				KNServletException k = new KNServletException("0I304にてエラーが発生しました。" + e);
				k.setErrorCode("00I304010101");
				try {
					con.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
					KNServletException k1 = new KNServletException("0I304にてコネクションロールバックに失敗しました。" + e);
					k1.setErrorCode("00I304010101");
					throw k1;
				}
				throw k;
			}finally {
				try {if (con != null) releaseConnectionPool(request, con);
				} catch (Exception e) {
					KNServletException k2 = new KNServletException("0I304にてコネクションの解放に失敗しました。" + e);
					k2.setErrorCode("00I304010101");
					throw k2;
				}
			}
			
			form.setYear(SpecialDateUtil.getThisJpnYear());//[1] add
			
			request.setAttribute("form", form);
				
			iCommonMap.setTargetPersonId(form.getTargetPersonId());
			
			super.forward(request, response, forwardDestination);
			
		}else{
			//転送:SERVLET
			iCommonMap.setTargetPersonId(form.getTargetPersonId());
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	
	}
	
	private boolean isDateOverlappedScreen(I304Form form) {
		// 1. エラーがなければ受験予定日を画面上からチェック
		boolean sameDateExist = false;
		//画面上で既に重複していないかチェック
		if(form.getMonth() != null && form.getMonth().length > 0){
			//受験日の重複チェック
			Map dateMap = new HashMap();
			for(int i=0; i<form.getMonth().length; i++){
				dateMap.put(form.getMonth()[i]+"/"+form.getDate()[i],new String[]{form.getMonth()[i], form.getDate()[i]});
			}
			if(dateMap.size() != form.getMonth().length){
				sameDateExist = true;
			}
		}
		if(sameDateExist){
			//重複した受験予定日を登録しようとしているので、エラー表示
			return true;
		}
		
		return false;
	}
	
	private boolean isDateOverlapedDB(I304Form form, Connection con) throws SQLException, Exception {
		//画面上で重複が存在しなければ、今度はDBデータと比較
		//この生徒の受験予定をすべて取得
		boolean sameDateExist = false;
		ExamPlanUnivSearchBean epusb = new ExamPlanUnivSearchBean();
		epusb.setIndividualId(form.getTargetPersonId());
		epusb.setConnection("", con);
		epusb.execute();
		if(epusb.getRecordSet() != null && epusb.getRecordSet().size() > 0){
			Iterator ite = epusb.getRecordSet().iterator();
			while(ite.hasNext()){
				ExamPlanUnivData epud = (ExamPlanUnivData)ite.next();
				if(!(epud.getUnivCd().equals(form.getUnivCd()) && epud.getFacultyCd().equals(form.getFacultyCd()) && epud.getDeptCd().equals(form.getDeptCd()) && epud.getEntExamModeCd().equals(form.getEntExamModeCd()))){
					//この大学・学部・学科はチェックに含めない
					if(epud.getExamPlanDateArray() != null && epud.getExamPlanDateArray().length > 0){
						for(int i=0; i<epud.getExamPlanDateArray().length; i++){
							String thisMonth = RecordProcessor.trimZero(RecordProcessor.getMonthDigit(epud.getExamPlanDateArray()[i]));
							String thisDate = RecordProcessor.trimZero(RecordProcessor.getDateDigit(epud.getExamPlanDateArray()[i]));
							if(form.getMonth() != null && form.getMonth().length > 0){
								//受験日の重複チェック
								for(int j=0; j<form.getMonth().length; j++){
									if(thisMonth.equals(form.getMonth()[j]) && thisDate.equals(form.getDate()[j])){
										sameDateExist = true;
									}
								}
							}
						}
					}
				}
			}
		}
		if(sameDateExist){
			return true;
		}
		
		return false;
	}
	
	private boolean isOver40Registered(I304Form form, Connection con) throws Exception {
		int ufdCount = 0;//登録されてる受験予定大学・学部・学科(一意)の数
		boolean isUnivExist = false;//更新しようとしている大学学部学科が既に存在するかどうか
		ExamPlanUnivSearchBean epusb = new ExamPlanUnivSearchBean();
		epusb.setIndividualId(form.getTargetPersonId());
		epusb.setConnection("", con);
		epusb.execute();
		if(epusb.getRecordSet() != null && epusb.getRecordSet().size() > 0){
			//大学・学部・学科の順にソート
			Collections.sort(epusb.getRecordSet(), new ExamPlanUnivSorter().new SortByUFD());
			Iterator ite = epusb.getRecordSet().iterator();
			String ufdKey = "";
			while(ite.hasNext()){
				ExamPlanUnivData epud = (ExamPlanUnivData)ite.next();
				//登録されてる受験予定大学・学部・学科(一意)の数を取得
				if(!ufdKey.equals(epud.getUFDKey())){
					ufdCount ++;
				}
				//更新しようとしている大学学部学科が既に存在するかどうか
				if(epud.getUFDKey().equals(form.getUnivCd()+form.getFacultyCd()+form.getDeptCd())){
					isUnivExist = true;
				}
				ufdKey = epud.getUFDKey();
			}
		}
		//登録しようとしている大学が既に存在しなかった場合、登録後に40件より多くならないかチェック
		if(!isUnivExist && ufdCount >= Integer.parseInt(IPropsLoader.getInstance().getMessage("MAX_NUM_EXAMPLAN"))){
			//最大件数以上の受験予定大学を登録しようとしているので、エラー表示
			return true;
		}
		
		return false;
	}
		
	/**
	 * 受験予定大学の更新
	 * @param errorMessage
	 * @param form
	 * @param con
	 * @return
	 * @throws Exception
	 */
	private String update(String errorMessage, I304Form form, Connection con) throws Exception {

		//すでにエラーが存在すればそのエラーを返す
		if (errorMessage != null) {
			return errorMessage;
		}
		
		//受験予定日を画面上からチェック
		if (isDateOverlappedScreen(form)) {
			return IPropsLoader.getInstance().getMessage("ERROR_DATE_EXIST");
		}
		
		//受験予定日をDBからチェック
		if (isDateOverlapedDB(form, con)) {
			return IPropsLoader.getInstance().getMessage("ERROR_DATE_EXIST");
		}

		//受験予定大学が40件を超えていないかをチェック
		if (isOver40Registered(form, con)) {
			return IPropsLoader.getInstance().getMessage("ERROR_40_EXCEED");
		}

		//TODO 既にこの大学・学部・学科・入試形態が存在するかの
		//チェックはbean内ではなくてここで行うべき
		
		//フォームより受け取った、受験予定データ（入試形態, 区分１, 区分２, 区分３, 日程, 場所）
		String planValues[] = null;
		
		if (form.getDate() != null){
			
			planValues = new String[form.getDate().length];
			
			for (int i = 0; form.getDate().length > i; i++) {
				
				planValues[i] = "";
				
				//入試形態が「一般（01）」の場合は
				//画面で指定された区分をそれぞれ使用する
				if (form.getEntExamModeCd().equals("01")) {
					planValues[i] =	form.getEntExamDiv1()[i]+ ","+ form.getEntExamDiv2()[i]	+ ","+ form.getEntExamDiv3()[i]	+ ",";
				}
				//入試形態がそれ以外「推薦（02）．．．」の場合は
				//強制的に区分を「1」「1」「01」で挿入する
				else {
					planValues[i] = "1,1,01,";
				}
				planValues[i] += form.getMonth()[i] + "/" + form.getDate()[i] + "," + form.getProvEntExamSite()[i];
			}
			
		}
		//日付を設定していない場合も日付なしで登録する
		//この場合区分を「1」「1」「01」で挿入する
		else{
			planValues = new String[1];
			planValues[0] = "1,1,01,0/0,";
		}
			
		//変更前の大学（大学, 学部, 学科, 入試形態）
		String[] univValue4Detail =	CollectionUtil.splitComma(form.getUnivValue4Detail());

		//複製
		//→　null	＋	学部	＋	学科	＋	入試形態	変更可
		if (form.getPreButton().equals("copy")) {
			form.setUnivValue4Detail(
					univValue4Detail[0]	+ 
					","+ 
					form.getFacultyCd()	+ 
					","+ 
					form.getDeptCd()	+ 
					","+ 
					form.getEntExamModeCd());
			PlanUnivCopyBean planUnivBean = new PlanUnivCopyBean();
			planUnivBean.setIndividualId(form.getTargetPersonId());//個人ＩＤ
			planUnivBean.setUnivValues(planValues); //予定
			planUnivBean.setRemarks(form.getRemarks()); //備考
			planUnivBean.setUnivValue(form.getUnivValue4Detail());
			planUnivBean.setConnection("", con);
			planUnivBean.execute();
			
			return planUnivBean.getErrorMessage();
		} 
		//編集
		//→　null	＋	null	＋	null	＋	入試形態	変更可
		else if (form.getPreButton().equals("edit")) {
			PlanUnivUpdateBean planUnivBean = new PlanUnivUpdateBean();
			planUnivBean.setIndividualId(form.getTargetPersonId());//個人ＩＤ
			planUnivBean.setUnivValues(planValues); //予定
			planUnivBean.setRemarks(form.getRemarks()); //備考
			planUnivBean.setUpdateUnivValue(
					univValue4Detail[0]	+ 
					"," + 
					univValue4Detail[1]
					+ 
					"," + 
					univValue4Detail[2]+ 
					","	+ 
					form.getEntExamModeCd());
			planUnivBean.setUnivValue(form.getUnivValue4Detail());
			planUnivBean.setConnection("", con);
			planUnivBean.execute();
			
			return planUnivBean.getErrorMessage();
		} 
		//新規
		//→　null	＋	null	＋	null	＋	入試形態	変更可
		else {
			
			form.setUnivValue4Detail(
					univValue4Detail[0]+ 
					","+ 
					univValue4Detail[1]+ 
					","+ 
					univValue4Detail[2]+ 
					","+ 
					form.getEntExamModeCd());
			
			PlanUnivCreateBean bean = new PlanUnivCreateBean();
			bean.setIndividualId(form.getTargetPersonId());
			bean.setUnivValues(planValues);
			bean.setRemarks(form.getRemarks());
			bean.setUnivValue(form.getUnivValue4Detail());
			bean.setConnection("", con);
			bean.execute();
			
			return bean.getErrorMessage();
		}
	}
	
	private PlanUnivSearchUnivBean searchPlanUniv(
			HttpServletRequest request, 
			I304Form form, 
			Connection con, 
			String errorMessage) throws Exception {

		/** 編集・複製・新規 */
			//初回アクセス-表示のみ
			/** 受験予定大学情報の取得 */
			PlanUnivSearchUnivBean planUnivSearchBean = new PlanUnivSearchUnivBean();
			planUnivSearchBean.setIndividualCd(form.getTargetPersonId());
				
			if(form.getButton() != null && (form.getButton().equals("edit") || form.getButton().equals("copy"))){
			/** 編集・複製 */
				StringTokenizer tk = new StringTokenizer(form.getUnivValue4Detail(), ",");
				String[] values = new String[4];
				while(tk.hasMoreTokens()){
					values[4 - tk.countTokens()] = tk.nextToken();
				}
				
				planUnivSearchBean.setUnivCd(values[0]);
				form.setUnivCd(values[0]);
				
				if(!"i304".equals(getBackward(request))){
					//I302で選択された受験予定大学　→　学部・学科・入試形態
					form.setFacultyCd(values[1]);
					form.setDeptCd(values[2]);
					form.setEntExamModeCd(values[3]);
					planUnivSearchBean.setFacultyCd(values[1]);
					planUnivSearchBean.setDeptCd(values[2]);
					planUnivSearchBean.setEntExamModeCd(values[3]);
				}else{
					//学部、学科コード　→　変更された　→　設定
					if(form.getButton().equals("copy")) {
					/** 複製 */
							//画面で選択された受験予定大学　→　学部・学科
						planUnivSearchBean.setFacultyCd(form.getFacultyCd());
						planUnivSearchBean.setDeptCd(form.getDeptCd());
					} else {
					/** 編集 */
						//I302で選択された受験予定大学　→　学部・学科
						form.setFacultyCd(values[1]);
						form.setDeptCd(values[2]);
						planUnivSearchBean.setFacultyCd(values[1]);
						planUnivSearchBean.setDeptCd(values[2]);
					}
					//入試形態を変更した
					planUnivSearchBean.setEntExamModeCd(form.getEntExamModeCd());
					planUnivSearchBean.setFirst(false);
					//エラーの時は初回表示と同じ扱い
					if(errorMessage != null){
						planUnivSearchBean.setFirst(true);
					}
				}
									
			}else{
			/** 新規 */
				//大学・学部・学科は、存在するはず
				//新規作成時　→　受験予定大学がある　→　検索してはいけない。
				planUnivSearchBean.setFirst(false);
				
				if(!"i304".equals(getBackward(request))){
					//入試形態が無い　→　初回アクセス時は一般で
					planUnivSearchBean.setEntExamModeCd("01");
					form.setEntExamModeCd("01");
					StringBuffer strBuf = new StringBuffer();
					strBuf.append(form.getUnivCd()).append(",");
					strBuf.append(form.getFacultyCd()).append(",");
					strBuf.append(form.getDeptCd()).append(",");
					strBuf.append(form.getEntExamModeCd());
					form.setUnivValue4Detail(strBuf.toString());

				}else{

					StringTokenizer tk = new StringTokenizer(form.getUnivValue4Detail(), ",");
					String[] values = new String[4];
					while(tk.hasMoreTokens()){
						values[4 - tk.countTokens()] = tk.nextToken();
					}

					form.setUnivCd(values[0]);
					form.setFacultyCd(values[1]);
					form.setDeptCd(values[2]);

					//入試形態を変更した
					planUnivSearchBean.setEntExamModeCd(form.getEntExamModeCd());

				}
				//新規を押した
				planUnivSearchBean.setUnivCd(form.getUnivCd());
				planUnivSearchBean.setFacultyCd(form.getFacultyCd());
				planUnivSearchBean.setDeptCd(form.getDeptCd());
				form.setButton("new");
			}// end of else
			
			planUnivSearchBean.setConnection("", con);
			planUnivSearchBean.execute();			
			
			return planUnivSearchBean;
	}
	
	private boolean isDateExist(PlanUnivSearchBean planUnivSearchBean) throws Exception {
		boolean isDateExist = false;
		if(planUnivSearchBean.getNestedPlanUnivList() != null && planUnivSearchBean.getNestedPlanUnivList().size() > 0){
			for(int i=0; i<planUnivSearchBean.getNestedPlanUnivList().size(); i++){
				NestedPlanUnivData npud = (NestedPlanUnivData)planUnivSearchBean.getNestedPlanUnivList().get(i);
				if(npud.getDivDataMap() != null && npud.getDivDataMap().size() > 0){
					Iterator ite = npud.getDivDataMap().entrySet().iterator();
					while(ite.hasNext()){
						List list = (List)((Map.Entry)ite.next()).getValue();
						if(list != null && list.size() > 0){
							for(int j=0; j<list.size(); j++){
								NestedPlanUnivData.InpleDateDivData data = (NestedPlanUnivData.InpleDateDivData)list.get(j);
								if(data != null && data.getEntExamInpleDate() != null && !data.getEntExamInpleDate().trim().equals("")){
									isDateExist = true;
								}
							}
						}
							
					}
				}
			}
		}
		return isDateExist;
	}

}

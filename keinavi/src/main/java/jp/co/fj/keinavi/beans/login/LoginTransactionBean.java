package jp.co.fj.keinavi.beans.login;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.KNCommonProperty;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * セッション数を更新するBean
 * 
 * 
 * 2005.10.14	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 * 
 * 
 * @author kawai
 */
public class LoginTransactionBean extends DefaultBean {

	private static final String QUERY_SELECT;
	private static final String QUERY_UPDATE;
	
	static {
		
		final StringBuffer select = new StringBuffer();
		
		select.append("SELECT ");
		
		if ("kounai".equals(KNCommonProperty.getRealTimeSessionMode())) {
			select.append("ko_maxsession, ");
			select.append("ko_realtimesession, ");
		} else {
			select.append("maxsession, ");
			select.append("realtimesession, ");
		}
		
		select.append("rowid ");
		select.append("FROM pactschool ");
		select.append("WHERE userid = ? AND maxsession < 999 ");
		select.append("AND pactdiv = '1' FOR UPDATE");

		QUERY_SELECT = select.toString();
		
		
		final StringBuffer update = new StringBuffer();
		
		update.append("UPDATE pactschool SET ");
		
		if ("kounai".equals(KNCommonProperty.getRealTimeSessionMode())) {
			update.append("ko_realtimesession = ? ");
		} else {
			update.append("realtimesession = ? ");
		}
		
		update.append("WHERE rowid = ?");
		
		QUERY_UPDATE = update.toString();
	}
	
	private String userID; // ユーザID
	private boolean countUp; // カウントアップするかどうか

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs1 = null;
		try {
			ps1 = conn.prepareStatement(QUERY_SELECT);
			ps1.setString(1, userID);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				final int max = rs1.getInt(1); // 最大セッション数
				int real = rs1.getInt(2); // リアルタイムセッション数
				final String rowId = rs1.getString(3); // ROWID
				
				// 制限オーバー
				if (countUp && max <= real) {
					KNServletException e =
						new KNServletException("接続数の制限を超えています");
					e.setErrorCode("1");
					throw e;
				}
				
				// カウントアップ	
				if (countUp) {
					real++;
					
				// ゼロより大きければカウントダウン
				} else if (real > 0) {
					real--;
				}
					
				ps2 = conn.prepareStatement(QUERY_UPDATE);
				ps2.setInt(1, real);
				ps2.setString(2, rowId);
				ps2.executeUpdate();
			}
			
		} finally {
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(null, ps1, rs1);
		}
	}

	/**
	 * @param string
	 */
	public void setUserID(String string) {
		userID = string;
	}


	/**
	 * @param b
	 */
	public void setCountUp(boolean b) {
		countUp = b;
	}

}

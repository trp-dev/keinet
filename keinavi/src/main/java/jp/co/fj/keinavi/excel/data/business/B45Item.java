/*
 * 作成日: 2004/09/08
 *
 * 高校成績分析−成績分析(過回比較)
 * @author cmurata
 */
package jp.co.fj.keinavi.excel.data.business;

import java.util.ArrayList;

public class B45Item {
//	//学校名
//	private String strGakkomei = "";
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//セキュリティスタンプ
	private int intSecuFlg = 0;
	//データリスト
	private ArrayList b45List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;


	/**
	 * @GET
	 */
	public ArrayList getB45List() {
		return this.b45List;
	}

	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}

//	public String getStrGakkomei() {
//		return this.strGakkomei;
//	}

	public String getStrMshDate() {
		return this.strMshDate;
	}

	public String getStrMshmei() {
		return this.strMshmei;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/**
	 * @SET
	 */
	public void setB45List(ArrayList b45List) {
		this.b45List = b45List;
	}

	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}

//	public void setStrGakkomei(String strGakkomei) {
//		this.strGakkomei = strGakkomei;
//	}

	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}

	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}

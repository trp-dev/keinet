/*
 * 作成日: 2004/07/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data;

import java.io.Serializable;

/**
 * 模試データ（模試マスタの内容をそのまま保持する）
 * @author kawai
 *
 */
public class ExamData implements Serializable, Comparable {

	private String examYear; // 模試年度
	private String examCD; // 模試コード
	private String examName; // 模試名
	private String examNameAbbr; // 模試短縮名
	private String examTypeCD; // 模試種類コード
	private String examST; // 模試回
	private String examDiv; // 模試区分
	private String targetGrade; // 対象学年
	private String inpleDate; // 模試実施基準日
	private String inDataOpenDate; // 内部データ開放日
	private String outDataOpenDate; // 外部データ開放日
	private String dockingExamCD; // ﾄﾞｯｷﾝｸﾞ先模試コード
	private String dockingType; // ﾄﾞｯｷﾝｸﾞｾﾝﾀｰ2次種別
	private int dispSequence; // 表示順序
	private String masterDiv; // 大学マスタ区分

	// データ開放日
	// ヘルプデスクなら内部データ開放日
	// それ以外は外部データ開放日となる
	private String dataOpenDate;

	// 2019/09/24 QQ)Ooseto 共通テスト対応 ADD START
	// ファイル存在
	// 1:科目別成績・志望大学データあり 学力指標データあり
	// 2:科目別成績・志望大学データあり 学力指標データなし
        // 3:科目別成績・志望大学データなし 学力指標データあり
        // 4:科目別成績・志望大学データなし 学力指標データなし
	private String fileExists = "4";
	// 2019/09/24 QQ)Ooseto 共通テスト対応 ADD END

	/**
	 * コンストラクタ
	 */
	public ExamData() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param examYear
	 * @param examCD
	 */
	public ExamData(String examYear, String examCD) {
		super();
		this.examYear = examYear;
		this.examCD = examCD;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return getExamId().equals(((ExamData) obj).getExamId());
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		// データ開放日の降順でソートする
		return ((ExamData) o).dataOpenDate.compareTo(this.dataOpenDate);
	}

	/**
	 * @return 模試ID
	 */
	public String getExamId() {
		return examYear + examCD;
	}

	/**
	 * @return
	 */
	public String getDockingExamCD() {
		return dockingExamCD;
	}

	/**
	 * @return
	 */
	public String getDockingType() {
		return dockingType;
	}

	/**
	 * @return
	 */
	public String getExamCD() {
		return examCD;
	}

	/**
	 * @return
	 */
	public String getExamDiv() {
		return examDiv;
	}

	/**
	 * @return
	 */
	public String getExamName() {
		return examName;
	}

	/**
	 * @return
	 */
	public String getExamNameAbbr() {
		return examNameAbbr;
	}

	/**
	 * @return
	 */
	public String getExamST() {
		return examST;
	}

	/**
	 * @return
	 */
	public String getExamTypeCD() {
		return examTypeCD;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @return
	 */
	public String getMasterDiv() {
		return masterDiv;
	}

	/**
	 * @return
	 */
	public String getTargetGrade() {
		return targetGrade;
	}

	/**
	 * @param string
	 */
	public void setDockingExamCD(String string) {
		dockingExamCD = string;
	}

	/**
	 * @param string
	 */
	public void setDockingType(String string) {
		dockingType = string;
	}

	/**
	 * @param string
	 */
	public void setExamCD(String string) {
		examCD = string;
	}

	/**
	 * @param string
	 */
	public void setExamDiv(String string) {
		examDiv = string;
	}

	/**
	 * @param string
	 */
	public void setExamName(String string) {
		examName = string;
	}

	/**
	 * @param string
	 */
	public void setExamNameAbbr(String string) {
		examNameAbbr = string;
	}

	/**
	 * @param string
	 */
	public void setExamST(String string) {
		examST = string;
	}

	/**
	 * @param string
	 */
	public void setExamTypeCD(String string) {
		examTypeCD = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @param string
	 */
	public void setMasterDiv(String string) {
		masterDiv = string;
	}

	/**
	 * @param string
	 */
	public void setTargetGrade(String string) {
		targetGrade = string;
	}

	/**
	 * @return
	 */
	public String getInpleDate() {
		return inpleDate;
	}

	/**
	 * @param string
	 */
	public void setInpleDate(String string) {
		inpleDate = string;
	}

	/**
	 * @return
	 */
	public String getInDataOpenDate() {
		return inDataOpenDate;
	}

	/**
	 * @return
	 */
	public String getOutDataOpenDate() {
		return outDataOpenDate;
	}

	/**
	 * @param string
	 */
	public void setInDataOpenDate(String string) {
		inDataOpenDate = string;
	}

	/**
	 * @param string
	 */
	public void setOutDataOpenDate(String string) {
		outDataOpenDate = string;
	}
	/**
	 * @return
	 */
	public int getDispSequence() {
		return dispSequence;
	}

	/**
	 * @param i
	 */
	public void setDispSequence(int i) {
		dispSequence = i;
	}

	/**
	 * @return
	 */
	public String getDataOpenDate() {
		return dataOpenDate;
	}

	/**
	 * @param string
	 */
	public void setDataOpenDate(String string) {
		dataOpenDate = string;
	}

	// 2019/09/24 QQ)Ooseto 共通テスト対応 ADD START
	/**
	 * @return
	 */
        public String getFileExists() {
            return fileExists;
        }

        /**
         * @param string
         */
        public void setFileExists(String string) {
            fileExists = string;
        }
	// 2019/09/24 QQ)Ooseto 共通テスト対応 ADD END

}

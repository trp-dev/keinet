package jp.co.fj.keinavi.excel.cm;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * Excel認定試験CEFR取得状況共通関数
 *
 * 2019.08.20   [新規作成]
 *
 * @author
 * @version 1.0
 */
public class CefrCommon {

	// テンプレートシート名
	public static final String templateSheetName = "template";

	// 合計行CEFRレベルコード
	public final String totalCefrCd = "ZZ";

	// シート内最大行数
	private int maxRowCount = 0;
	// 年度表示最大数
	private int maxYearCount = 0;
	// シートカウンター
	private int maxSheetIndex = 0;
	// 行カウンター
	private int maxRowIndex = 0;
	// 表カウンター(A:左側、B:右側)
	private SIDE side = SIDE.A;
	// カーソル初期位置
	private int initialCurCol = 0;
	private int initialCurRow = 0;
	// カーソルカレント位置
	private int curCol = 0;
	private int curRow = 0;
	// 年度
	private String[] nendo = null;

	// 改ページ・改表キー
	private String keyTable = "";
	private String keyRow = "";

	// 自校受験者がいる試験を対象にするフラグ
	private int intTargetCheckBoxFlg = 0;

	// Excelブック
	private HSSFWorkbook workbook = null;
	// Excelシート
	private HSSFSheet workSheet = null;
	// Excel行
	private HSSFRow workRow = null;
	// Excelセル
	private HSSFCell workCell = null;
	// Excelテンプレートシート
	private HSSFSheet templateSheet = null;

	// Enum(表位置)
	public enum SIDE {
		A, // 左側の表
		B  // 右側の表
	}

	/**
	 * <DL>
	 * <DT>コンストラクタ
	 * </DL>
	 * @param workbook
	 */
	public CefrCommon(HSSFWorkbook workbook) {
		// Excelブック
		this.workbook = workbook;
	}

//	/**
//	 * <DL>
//	 * <DT>年度配列作成
//	 * <DD>DB検索結果全件を調べ、年度を抽出し配列に格納する
//	 * </DL>
//	 * @param list
//	 */
//	public void createNendoArray(ArrayList list) {
//
//		nendo = new String[maxYearCount];
//
//		Iterator itrList = list.iterator();
//		while( itrList.hasNext() ) {
//
//			S22PtCefrAcqStatusListBean bean = (S22PtCefrAcqStatusListBean)itrList.next();
//
//			if (StringUtils.isNotEmpty(bean.getExamyear())
//					&& StringUtils.isEmpty(nendo[0])) {
//				nendo[0] = bean.getExamyear();
//			}
//			if (StringUtils.isNotEmpty(bean.getExamyear2())
//					&& StringUtils.isEmpty(nendo[1])) {
//				nendo[1] = bean.getExamyear2();
//			}
//			if (StringUtils.isNotEmpty(bean.getExamyear3())
//					&& StringUtils.isEmpty(nendo[2])) {
//				nendo[2] = bean.getExamyear3();
//			}
//			if (StringUtils.isNotEmpty(bean.getExamyear4())
//					&& StringUtils.isEmpty(nendo[3])) {
//				nendo[3] = bean.getExamyear4();
//			}
//			if (StringUtils.isNotEmpty(bean.getExamyear5())
//					&& StringUtils.isEmpty(nendo[4])) {
//				nendo[4] = bean.getExamyear5();
//			}
//		}
//	}

	/**
	 * <DL>
	 * <DT>改表判定実施
	 * <DD>判定キーが前回キーと異なる場合、対象と判定
	 * </DL>
	 * @param key 判定キー
	 * @return 判定結果(true:対象外、false:改表対象)
	 */
	public boolean checkKeyTable(String key) {

		boolean ret = true;

		// キーが異なる場合、改表判定
		if (!key.equals(this.keyTable)) {
			ret = false;
		}

		// 前回キー情報に現在キー情報を設定
		this.keyTable = key;

		return ret;
	}

	/**
	 * <DL>
	 * <DT>改行判定実施
	 * <DD>判定キーが前回キーと異なる場合、対象と判定
	 * </DL>
	 * @param key 判定キー
	 * @return 判定結果(true:対象外、false:改行対象)
	 */
	public boolean checkKeyRow(String key) {

		boolean ret = true;

		// キーが異なる場合、改行判定
		if (!key.equals(this.keyRow)) {
			ret = false;
		}

		// 前回キー情報に現在キー情報を設定
		this.keyRow = key;

		return ret;
	}

	/**
	 * <DL>
	 * <DT>シートテンプレートのコピー
	 * </DL>
	 */
	public void createCloneSheet() {
		workSheet = workbook.cloneSheet(0);
		maxSheetIndex++;
	}

	/**
	 * <DL>
	 * <DT>改行判定実施
	 * <DD>右側表作画後にさらに表を追加する場合、対象と判定
	 * </DL>
	 * @param key 判定キー
	 * @return 判定結果(true:対象外、false:改行対象)
	 */
	public boolean checkTableSide() {

		boolean ret = true;

		// 改行となる場合は、falseを返却
		if (!this.side.equals(SIDE.A)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * <DL>
	 * <DT>改ページ判定実施
	 * <DD>１ページ最大表示件数を超える場合、対象と判定
	 * </DL>
	 * @return 判定結果(true:対象外、false:改ページ対象)
	 */
	public boolean checkNewPage() {
	    // 2019/09/03 QQ)M.Ooseto 共通テスト対応 UPD START
		//if (this.maxRowIndex > maxRowCount) {
	    if (this.maxRowIndex >= maxRowCount) {
	        return false;
	    }
	    // 2019/09/03 QQ)M.Ooseto 共通テスト対応 UPD END
		return true;
	}

	/**
	 * <DL>
	 * <DT>表位置設定
	 * </DL>
	 * @param side 表位置
	 */
	public void setTableSide(SIDE side) {

		// 表配置場所を設定
		this.side = side;
		// 左側表位置指定時は、行カウンタをカウントアップ
		if (side.equals(SIDE.A)) {
			maxRowIndex++;
		}
	}

	/**
	 * <DL>
	 * <DT>英語認定試験名・試験レベル名設定
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 * @param str 英語認定試験名・試験レベル名
	 */
	public void setTableName(CM cm, String str) {
		// カーソル位置指定
		this.curCol = this.initialCurCol;
		this.curRow = this.initialCurRow;

		this.setCellValueAddRowCountUp(cm, str);
	}


	/**
	 * <DL>
	 * <DT>延人数設定
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 * @param str 延人数
	 */
	public void setTableHeaderPerson(CM cm, String str) {
		// カーソル位置指定
		this.curCol = this.initialCurCol;
		this.curRow = this.initialCurRow + 1;

		this.setCellValueAddRowCountUp(cm, str);
	}

	/**
	 * <DL>
	 * <DT>試験結果を出力するか判定
	 * <DD>[当年度自校受験者がいる試験を対象にする]チェックボックスOFF→全て出力対象
	 * <DD>[当年度自校受験者がいる試験を対象にする]チェックボックスON ＆ 自校受験者がいる試験→出力対象
	 * <DD>[当年度自校受験者がいる試験を対象にする]チェックボックスON ＆ 自校受験者がいない試験→出力対象外
	 * </DL>
	 * @param examFlg 自校受験者がいるか(1:いる、0:いない)
	 * @return 戻り値(1:出力対象、0:出力対象外)
	 */
	public String setExamFlg(String examFlg) {

		String ret = "1";

		if (this.checkIntTargetCheckBoxFlg()) {
			// 当年度自校受験者がいる試験を対象にするフラグON
			ret = examFlg;
		}
		return ret;
	}

	// 2019/10/10 QQ)Ooseto 共通テスト対応 UPD START
//	/**
//	 * <DL>
//	 * <DT>人数をCellに設定
//	 * </DL>
//	 * @param cm Excel共通関数インスタンス
//	 * @param cefrlevelcd CEFRレベルコード
//	 * @param number 人数
//	 * @param flg 自校受験者がいるか(1:いる、0:いない)
//	 */
//	public void setCerfNumber(CM cm, String cefrlevelcd, int number, String flg) {
//
//		// 人数が0の場合は表示しない
//		if (number != 0) {
//			// 合計行か判定
//			if (StringUtils.isNotEmpty(cefrlevelcd)
//				&& cefrlevelcd.equals(totalCefrCd)) {
//				// カーソル位置指定
//				this.curCol = this.initialCurCol;
//
//				this.setCellValueAddRowCountUpWithSkipFlg(cm, number, this.setExamFlg(flg));
//			} else {
//				this.setCellValueAddRowCountUpWithSkipFlg(cm, number, this.setExamFlg(flg));
//			}
//		} else {
//		    this.curRow++;
//		}
//	}

	// 2019/09/03 QQ)M.Ooseto 共通テスト対応 ADD START
	/**
	 * <DL>
	 * <DT>人数をCellに設定(人数または構成比が0の場合、表示しない)
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 * @param cefrlevelcd CEFRレベルコード
	 * @param number 人数
	 * @param flg 自校受験者がいるか(1:いる、0:いない)
	 */
	//public void setCerfNumber2(CM cm, String cefrlevelcd, int number, float compratio, String flg) {
	public void setCerfNumber(CM cm, String cefrlevelcd, int number, float compratio, String flg) {
		// 人数または構成比が0の場合は表示しない
		if (number != 0 && compratio != Float.parseFloat("0.0")) {
			// 合計行か判定
			if (StringUtils.isNotEmpty(cefrlevelcd)
				&& cefrlevelcd.equals(totalCefrCd)) {
				// カーソル位置指定
				this.curCol = this.initialCurCol;

				this.setCellValueAddRowCountUpWithSkipFlg(cm, number, this.setExamFlg(flg));
			} else {
				this.setCellValueAddRowCountUpWithSkipFlg(cm, number, this.setExamFlg(flg));
			}
		} else {
			this.curRow++;
		}
	}
	// 2019/09/03 QQ)M.Ooseto 共通テスト対応 ADD END

//	/**
//	 * <DL>
//	 * <DT>構成比をCellに設定
//	 * </DL>
//	 * @param cm Excel共通関数インスタンス
//	 * @param cefrlevelcd CEFRレベルコード
//	 * @param compratio 構成比
//	 * @param flg 自校受験者がいるか(1:いる、0:いない)
//	 */
//	public void setCerfCompratio(CM cm, String cefrlevelcd, int number, float compratio, String flg) {
//
//		// 人数が0の場合は表示しない
//		if (number != 0) {
//			// 合計行か判定
//			if (StringUtils.isNotEmpty(cefrlevelcd)
//					&& cefrlevelcd.equals(totalCefrCd)) {
//				// 合計行の場合、なにもしない
//			} else {
//				this.setCellValueAddRowCountUpWithSkipFlg(cm, String.format("(%1$.1f)", compratio), this.setExamFlg(flg));
//			}
//		} else {
//			this.curRow++;
//		}
//	}

	// 2019/09/03 QQ)M.Ooseto 共通テスト対応 ADD START
	/**
	 * <DL>
	 * <DT>構成比をCellに設定(人数または構成比が0の場合、表示しない)
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 * @param cefrlevelcd CEFRレベルコード
	 * @param compratio 構成比
	 * @param flg 自校受験者がいるか(1:いる、0:いない)
	 */
	//public void setCerfCompratio2(CM cm, String cefrlevelcd, int number, float compratio, String flg) {
	public void setCerfCompratio(CM cm, String cefrlevelcd, int number, float compratio, String flg) {
		// 人数または構成比が0の場合は表示しない
		if (number != 0 && compratio != Float.parseFloat("0.0")) {
			// 合計行か判定
			if (StringUtils.isNotEmpty(cefrlevelcd) && cefrlevelcd.equals(totalCefrCd)) {
				// 合計行の場合、なにもしない
				;
			} else {
				this.setCellValueAddRowCountUpWithSkipFlg(cm, String.format("(%1$.1f)", compratio), this.setExamFlg(flg));
			}
		} else {
			this.curRow++;
		}
	}
	// 2019/09/03 QQ)M.Ooseto 共通テスト対応 ADD END

	// 2019/09/30 QQ) 共通テスト対応 ADD START
//	/**
//	* <DL>
//	* <DT>構成比をCellに設定(構成比が0の場合、表示しない)
//	* </DL>
//	* @param cm Excel共通関数インスタンス
//	* @param cefrlevelcd CEFRレベルコード
//	* @param compratio 構成比
//	* @param flg 自校受験者がいるか(1:いる、0:いない)
//	 */
//	public void setCerfCompratio3(CM cm, String cefrlevelcd, float compratio, String flg) {
//		// 人数または構成比が0の場合は表示しない
//		if (compratio != Float.parseFloat("0.0")) {
//			// 合計行か判定
//			if (StringUtils.isNotEmpty(cefrlevelcd) && cefrlevelcd.equals(totalCefrCd)) {
//				// 合計行の場合、なにもしない
//				;
//			} else {
//				this.setCellValueAddRowCountUpWithSkipFlg(cm, String.format("(%1$.1f)", compratio), this.setExamFlg(flg));
//			}
//		} else {
//			this.curRow++;
//		}
//	}
	// 2019/09/30 QQ) 共通テスト対応 ADD END
	// 2019/10/10 QQ)Ooseto 共通テスト対応 UPD START

	/**
	 * <DL>
	 * <DT>セル設定
	 * <DD>設定有無に関わらず、カーソル行を1つ進める。
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 * @param value 表示内容
	 * @param flg 表示フラグ(1:対象、1以外:対象外)
	 */
	public void setCellValueAddRowCountUp(CM cm, int value) {
		if (value != 0) {
			workCell = cm.setCell(workSheet, workRow, workCell, curRow, curCol);
			workCell.setCellValue(value);
		}
		curRow++;
	}

	// 2019/09/30 QQ) 共通テスト対応 ADD START
	/**
	 * <DL>
	 * <DT>セル設定
	 * <DD>設定有無に関わらず、カーソル行を1つ進める。
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 * @param value 表示内容
	 */
	public void setCellValueAddRowCountUp(CM cm, float value) {
		if (value != 0) {
			workCell = cm.setCell(workSheet, workRow, workCell, curRow, curCol);
			workCell.setCellValue(value);
		}
		curRow++;
	}
	// 2019/09/30 QQ) 共通テスト対応 ADD END

	/**
	 * <DL>
	 * <DT>セル設定
	 * <DD>設定有無に関わらず、カーソル行を1つ進める。
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 * @param value 表示内容
	 * @param flg 表示フラグ(1:対象、1以外:対象外)
	 */
	public void setCellValueAddRowCountUp(CM cm, String value) {
		if (StringUtils.isNotEmpty(value)) {
			workCell = cm.setCell(workSheet, workRow, workCell, curRow, curCol);
			workCell.setCellValue(value);
		}
		curRow++;
	}

	/**
	 * <DL>
	 * <DT>セル設定
	 * <DD>設定有無に関わらず、カーソル行を1つ進める。表示フラグ制御あり。
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 * @param value 表示内容
	 * @param flg 表示フラグ(1:対象、1以外:対象外)
	 */
	public void setCellValueAddRowCountUpWithSkipFlg(CM cm, int value, String flg) {
		if (StringUtils.isNotEmpty(flg)
				&& flg.equals("1")) {
			this.setCellValueAddRowCountUp(cm, value);
		} else {
			curRow++;
		}
	}

	/**
	 * <DL>
	 * <DT>セル設定
	 * <DD>設定有無に関わらず、カーソル行を1つ進める。表示フラグ制御あり。
	 * </DL>
	 * @param cm Excel共通関数インスタンス
	 * @param value 表示内容
	 * @param flg 表示フラグ(1:対象、1以外:対象外)
	 */
	public void setCellValueAddRowCountUpWithSkipFlg(CM cm, String value, String flg) {
		if (StringUtils.isNotEmpty(flg)
				&& flg.equals("1")) {
			this.setCellValueAddRowCountUp(cm, value);
		} else {
			curRow++;
		}
	}

	/**
	 * <DL>
	 * <DD>Excelシート初期設定
	 * </DL>
	 */
	public void initSheetSetting() {

		this.side = SIDE.A;
		this.maxRowIndex = 0;

		// シートテンプレートのコピー
		this.createCloneSheet();
	}

	/**
	 * <DL>
	 * <DD>自校受験者がいる試験を対象にするフラグ取得
	 * </DL>
	 * @return 自校受験者がいる試験を対象にするフラグ
	 */
	public int getIntTargetCheckBoxFlg() {
		return this.intTargetCheckBoxFlg;
	}

	/**
	 * <DL>
	 * <DD>自校受験者がいる試験を対象にするフラグ設定
	 * </DL>
	 * @param 自校受験者がいる試験を対象にするフラグ
	 */
	public void setIntTargetCheckBoxFlg(int intTargetCheckBoxFlg) {
		this.intTargetCheckBoxFlg = intTargetCheckBoxFlg;
	}

	/**
	 * <DL>
	 * <DD>自校受験者がいる試験を対象にするか判定
	 * </DL>
	 * @return 戻り値(true:対象、false:対象としない)
	 */
	public boolean checkIntTargetCheckBoxFlg() {
		if (intTargetCheckBoxFlg == 1) {
			return true;
		}
		return false;
	}

	/**
	 * <DL>
	 * <DT>Excelテンプレートシート指定を指定する
	 * </DL>
	 */
	public void setTemplateWorkbook() {
		// Excelテンプレートシート指定
		this.templateSheet = workbook.getSheet(templateSheetName);
	}

	/**
	 * <DL>
	 * <DT>Excelテンプレートシート指定を返却する
	 * </DL>
	 */
	public HSSFSheet getTemplateWorkbook() {
		return this.templateSheet;
	}

	/**
	 * <DL>
	 * <DT>作成したExcelブックを返却する
	 * </DL>
	 * @return 作成したExcelブック
	 */
	public HSSFWorkbook getWorkbook() {
		return this.workbook;
	}

	/**
	 * <DL>
	 * <DT>現在シート作成枚数を返却する
	 * </DL>
	 * @return 現在シート作成枚数
	 */
	public int getMaxSheetIndex() {
		return this.maxSheetIndex;
	}

	/**
	 * <DL>
	 * <DD>Excelシート取得
	 * </DL>
	 * @return Excelシート
	 */
	public HSSFSheet getWorkSheet() {
		return this.workSheet;
	}

	/**
	 * <DL>
	 * <DD>Excelシート設定
	 * </DL>
	 * @param Excelシート
	 */
	public void setWorkSheet(HSSFSheet workSheet) {
		this.workSheet = workSheet;
	}

	/**
	 * <DL>
	 * <DD>カーソルカレント列位置取得
	 * </DL>
	 * @return カーソルカレント列位置
	 */
	public int getCurCol() {
		return this.curCol;
	}

	/**
	 * <DL>
	 * <DD>カーソルカレント列位置設定
	 * </DL>
	 * @param カーソルカレント列位置
	 */
	public void setCurCol(int curCol) {
		this.curCol = curCol;
	}

	/**
	 * <DL>
	 * <DD>カーソルカレント行位置取得
	 * </DL>
	 * @return カーソルカレント行位置
	 */
	public int getCurRow() {
		return this.curRow;
	}

	/**
	 * <DL>
	 * <DD>カーソルカレント行位置設定
	 * </DL>
	 * @param カーソルカレント行位置
	 */
	public void setCurRow(int curRow) {
		this.curRow = curRow;
	}

	/**
	 * <DL>
	 * <DD>カーソル初期列位置取得
	 * </DL>
	 * @return カーソル初期列位置
	 */
	public int getInitialCurCol() {
		return this.initialCurCol;
	}

	/**
	 * <DL>
	 * <DD>カーソル初期列位置設定
	 * </DL>
	 * @param カーソル初期列位置
	 */
	public void setInitialCurCol(int initialCurCol) {
		this.initialCurCol = initialCurCol;
	}

	/**
	 * <DL>
	 * <DD>カーソル初期行位置取得
	 * </DL>
	 * @return カーソル初期行位置
	 */
	public int getInitialCurRow() {
		return this.initialCurRow;
	}

	/**
	 * <DL>
	 * <DD>カーソル初期行位置設定
	 * </DL>
	 * @param カーソル初期行位置
	 */
	public void setInitialCurRow(int initialCurRow) {
		this.initialCurRow = initialCurRow;
	}

	/**
	 * <DL>
	 * <DD>表カウンター取得
	 * </DL>
	 * @return 表カウンター
	 */
	public SIDE getSide() {
		return this.side;
	}

	/**
	 * <DL>
	 * <DD>表カウンター設定
	 * </DL>
	 * @param 表カウンター
	 */
	public void setSide(SIDE side) {
		this.side = side;
	}

	/**
	 * <DL>
	 * <DD>シート内最大行数取得
	 * </DL>
	 * @return シート内最大行数
	 */
	public int getMaxRowCount() {
		return this.maxRowCount;
	}

	/**
	 * <DL>
	 * <DD>シート内最大行数設定
	 * </DL>
	 * @param シート内最大行数
	 */
	public void setMaxRowCount(int maxRowCount) {
		this.maxRowCount = maxRowCount;
	}

	/**
	 * <DL>
	 * <DD>行カウンター取得
	 * </DL>
	 * @return 行カウンター
	 */
	public int getMaxRowIndex() {
		return this.maxRowIndex;
	}

	/**
	 * <DL>
	 * <DD>行カウンター設定
	 * </DL>
	 * @param 行カウンター
	 */
	public void setMaxRowIndex(int maxRowIndex) {
		this.maxRowIndex = maxRowIndex;
	}

	/**
	 * <DL>
	 * <DD>年度表示最大数取得
	 * </DL>
	 * @return 年度表示最大数
	 */
	public int getMaxYearCount() {
		return this.maxYearCount;
	}

	/**
	 * <DL>
	 * <DD>年度表示最大数取得
	 * </DL>
	 * @return 年度表示最大数
	 */
	public void setMaxYearCount(int maxYearCount) {
		this.maxYearCount = maxYearCount;
	}
}

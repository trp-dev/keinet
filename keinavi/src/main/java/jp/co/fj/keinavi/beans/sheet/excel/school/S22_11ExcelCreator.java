package jp.co.fj.keinavi.beans.sheet.excel.school;

import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.common.ISheetData;
import jp.co.fj.keinavi.beans.sheet.school.data.S22_11Data;
import jp.co.fj.keinavi.beans.sheet.school.data.S22_11SubjectListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S22_11YearListData;

/**
 *
 * 校内成績分析 - 過年度比較 - 偏差値分布
 * 受験学力測定テスト専用
 *
 * 2007.07.30	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class S22_11ExcelCreator extends S42_15ExcelCreator {

    // 行開始インデックス
    private final int startRowIndex = 7;

    /**
     * コンストラクタ
     *
     * @param pData
     * @param pSequenceId
     * @param pOutFileList
     * @param pPrintFlag
     * @throws Exception
     */
    public S22_11ExcelCreator(final ISheetData pData, final String pSequenceId,
            final List pOutFileList, final int pPrintFlag) throws Exception {

        super(pData, pSequenceId, pOutFileList, pPrintFlag, 7, -1);
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
     * 			#initSheet(org.apache.poi.hssf.usermodel.HSSFSheet)
     */
    protected void initSheet(final ISheetData pData) throws Exception {

        // データクラス
        final S22_11Data data = (S22_11Data) pData;

        // 作成日
        setCreateDate();
        // セキュリティスタンプ
        setSecurityStamp(data.getSecurityStamp(), 33, 34);
        // 学校名
        setCellValue("A2", "学校名　　　：" + data.getBundleName());
        // 対象模試
        setCellValue("A3", createTargetExamLabel(
                data.getExamName(), data.getInpleDate()));
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.BaseSheetCreator
     * 			#createSheet(jp.co.fj.keinavi.beans.sheet.common.ISheetData)
     */
    protected void createSheet(final ISheetData pData) throws Exception {

        // データクラス
        final S22_11Data data = (S22_11Data) pData;

        // 表示位置
        int position = 0;

        // 科目でループする
        int count = 0;
        for (final Iterator ite = data.getSubjectListData(
                ).iterator(); ite.hasNext();) {

            createSheet((S22_11SubjectListData) ite.next(), position);

            position = ++count % 2;

            if (ite.hasNext() && position == 0) {
                breakSheet();
            }
        }
    }

    protected void createSheet(final S22_11SubjectListData subData,
            final int position) throws Exception {

        // 科目
        if (position == 0) {
            setCellValue("A5", "科目：" + subData.getSubName());
        } else {
            setCellValue("S5", "科目：" + subData.getSubName());
        }

        // センター到達エリア
        setCenterReachArea(subData.getReachLevelListData(), 18 * position);

        // シート内の位置
        int index = 0;

        for (final Iterator ite = subData.getYearListData().iterator(); ite.hasNext();) {

            final S22_11YearListData yearData = (
                    S22_11YearListData) ite.next();

            // シート変更宣言
            setModifiedSheet();
            // データセット
            createSheet(yearData, index, position);

            index++;
        }
    }

    private void createSheet(final S22_11YearListData yearData,
            final int index, final int position) {

        // 列位置
        final int cIndex = (2 + 18 * position) + 3 * index;

        setCellValue(startRowIndex - 2, cIndex,
                yearData.getExamYear() + "年度");

        // 合計人数
        setCellValue(startRowIndex + 61, cIndex, yearData.getNumbers());
        // 平均スコア
        setCellValue(startRowIndex + 62, cIndex, yearData.getAvgPnt());

        // スコア帯データをセット
        if (!yearData.getScoreZoneListMap().isEmpty()) {
            createSheet(yearData.getScoreZoneListMap(), cIndex);
        }
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
     * 			#getSheetId()
     */
    protected String getSheetId() {
        return "S22_11";
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator
     * 			#isGraphSheet()
     */
    protected boolean isGraphSheet() {
        return false;
    }

}

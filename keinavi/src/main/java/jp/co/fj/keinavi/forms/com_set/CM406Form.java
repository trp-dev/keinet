package jp.co.fj.keinavi.forms.com_set;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM406Form extends CM404Form {

	// 表示対象となるページ番号
	// デフォルトは１ページ目
	private String page = null;
	// 追加対象となる大学コードの配列
	private String[] univ = null;
	// ページに表示されている大学コードの配列
	private String[] master = null;
	// 保存するかどうか
	private String save = null;

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 
		
	}

	/**
	 * @return
	 */
	public String getPage() {
		return page;
	}

	/**
	 * @return
	 */
	public String[] getUniv() {
		return univ;
	}

	/**
	 * @param string
	 */
	public void setPage(String string) {
		page = string;
	}

	/**
	 * @param strings
	 */
	public void setUniv(String[] strings) {
		univ = strings;
	}

	/**
	 * @return
	 */
	public String[] getMaster() {
		return master;
	}

	/**
	 * @param strings
	 */
	public void setMaster(String[] strings) {
		master = strings;
	}

	/**
	 * @return
	 */
	public String getSave() {
		return save;
	}

	/**
	 * @param string
	 */
	public void setSave(String string) {
		save = string;
	}

}

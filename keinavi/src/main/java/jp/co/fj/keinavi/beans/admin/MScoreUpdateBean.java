/*
 * 作成日: 2004/11/30
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.admin;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 個人成績の個人IDをUPDATEするBean
 *
 * 2005.06.22	Yoshimoto KAWAI - Totec
 * 				対象模試の成績のみUPDATEするように修正
 *
 * @author Yoshimoto KAWAI - Totec
 */
public class MScoreUpdateBean extends DefaultBean {

	private String masterId; // 書き換えられるID
	private String rewriteId; // 書き換えるID
	private String examYear; // 模試年度
	private String examCd; // 模試コード

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		PreparedStatement ps = null;

		// 科目別成績
		try {
			ps = conn.prepareStatement(
				"UPDATE subrecord_i "
				+ "SET individualid = ? "
				+ "WHERE individualid = ? "
				+ "AND examyear = ? "
				+ "AND examcd = ?");
			ps.setString(1, this.rewriteId);
			ps.setString(2, this.masterId);
			ps.setString(3, this.examYear);
			ps.setString(4, this.examCd);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}

		// 設問別成績
		try {
			ps = conn.prepareStatement(
				"UPDATE qrecord_i "
				+ "SET individualid = ? "
				+ "WHERE individualid = ? "
				+ "AND examyear = ? "
				+ "AND examcd = ?");
			ps.setString(1, this.rewriteId);
			ps.setString(2, this.masterId);
			ps.setString(3, this.examYear);
			ps.setString(4, this.examCd);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}

		// 志望校評価
		try {
			ps = conn.prepareStatement(
				"UPDATE candidaterating "
				+ "SET individualid = ? "
				+ "WHERE individualid = ? "
				+ "AND examyear = ? "
				+ "AND examcd = ?");
			ps.setString(1, this.rewriteId);
			ps.setString(2, this.masterId);
			ps.setString(3, this.examYear);
			ps.setString(4, this.examCd);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}

                // 学力要素
                try {
                        ps = conn.prepareStatement(
                                "UPDATE SUBACADEMIC_I "
                                + "SET individualid = ? "
                                + "WHERE individualid = ? "
                                + "AND examyear = ? "
                                + "AND examcd = ?");
                        ps.setString(1, this.rewriteId);
                        ps.setString(2, this.masterId);
                        ps.setString(3, this.examYear);
                        ps.setString(4, this.examCd);
                        ps.executeUpdate();
                } finally {
                        DbUtils.closeQuietly(ps);
                }

	}

	/**
	 * @param string
	 */
	public void setMasterId(String string) {
		masterId = string;
	}

	/**
	 * @param string
	 */
	public void setRewriteId(String string) {
		rewriteId = string;
	}

	public void setExamCd(String examCd) {
		this.examCd = examCd;
	}
	public void setExamYear(String examYear) {
		this.examYear = examYear;
	}
}

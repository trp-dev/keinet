package jp.co.fj.keinavi.data.individual;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.kawaijuku.judgement.data.Examination;
import jp.co.fj.kawaijuku.judgement.util.MathUtil;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * 試験成績データクラス
 * 
 * <2010年度改修>
 * 2004.07.16	Tomohisa YAMADA - Totec
 * 				新規作成
 * 
 * 2005.03.14 	Tomohisa YAMADA - Totec
 * 				この模試が正常かどうかを判断するメソッド追加
 * 
 * 2005.08.04 	Tomohisa YAMADA - Totec
 * 				偏差値算出時の丸め誤差修正
 * 
 * <2006年度対応>
 * 2006.05.22    Tomohisa YAMADA - Totec
 * 				getAvg教科Single()メソッドにおけるKeiNaviとKeiSproの統合
 *
 * <2010年度改修>
 * 2009.12.25	Tomohisa YAMADA - Totec
 * 				5教科判定対応
 * 
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 */
public class ExaminationData extends Examination implements Serializable{
    
	//対象学年
	protected String tergetGrade;
	
	//内部公開日
	protected String inDataOpenDate;
	
	//外部公開日
	protected String outDataOpenData;
	
	//ドッキング対象模試コード
	protected String dockingExamCd;
	
	//ドッキングタイプ
	protected String dockingType;
	
	//模試回
	protected String examSt;
	
	//模試区分
	protected String examDiv;
	
	//ソート文字列
	protected String dispSequence;
	
	//大学マスタ区分
	protected String masterDiv;
	
    /**
     * @return 科目コード1000番を英語の成績として取得
     */
    public String getAvgEngSingle(){
        return format2CDev(searchSubRecord(ENG));
    }
    
    /**
     * @return 科目コード2000番を数学の成績として取得
     */
    public String getAvgMathSingle(){
        return format2CDev(searchSubRecord(MAT));
    }
    
    /**
     * @return 科目コード3000番を国語の成績として取得
     */
    public String getAvgJapSingle(){
        return format2CDev(searchSubRecord(JAP));
    }
    
    /**
     * @return 科目コード4000番を理科の成績として取得
     */
    public String getAvgSciSingle(){
        return format2CDev(searchSubRecord(SCI));
    }
    
    /**
     * @return 科目コード5000番を社会の成績として取得
     */
    public String getAvgSocSingle(){
        return format2CDev(searchSubRecord(SOC));
    }
    
    /**
     * @return 科目コード7000番を総合の成績として取得
     */
    public String getAvgAllSingle(){
        return format2CDev(searchSubRecord(ALL));
    }
	
    /**
     * 偏差値データを成績オブジェクトから取得して戻す
     * @param record 成績データ
     * @return フォーマット化された偏差値
     */
    private String format2CDev(final SubRecordData record) {
        
        String deviation = "";
        
        if (record != null) {
        
            final String dev = record.getCDeviation();
            
            if (dev != null && dev.trim().length() > 0) {                
                
                //構造上、偏差値データが文字列で取得される
                double d = Double.parseDouble(dev);
                
                //小数点第二位を四捨五入して、文字列に戻す
                deviation = Double.toString(MathUtil.to2ndDecimal(d));
                
            }
            
        }

        return deviation;
        
    }
    
    /**
     * 指定の科目コードの科目データを取得する
     * @param code 科目コード
     * @return 
     */
    private SubRecordData searchSubRecord(final String code) {
        
        final List records = (List) getSubRecordDatas();
        
        final Iterator ite = records.iterator();
        
        while (ite.hasNext()) {
            
            SubRecordData target = (SubRecordData) ite.next();

            if (code.equals(target.getSubCd())) {
                return target;
            }
            
        }
        
        return null;
        
    }
    
	/**
     * 試験種別コード
     */
    private String examDivCd;
    
    /**
     * @return 試験種別コード
     */
    public String getExamDivCd() {
        return examDivCd;
    }
    
    /**
     * @param pExamDivCd 試験種別コード
     */
    public void setExamDivCd(final String pExamDivCd) {
        examDivCd = pExamDivCd;
    }
	
	/**
	 * [1] add
	 * この模試が正常かどうか
	 * @return
	 */
	public boolean isValidExam(){
		if(examYear != null && !examYear.trim().equals("") && examCd != null && !examCd.trim().equals("")){
			return true;
		}else{
			return false;
		}
	}
	
	//:::::::::::: 科目コード値範囲 :::::::::::::::::::::::
	private static final String ENG = "1000";//英語コード
	private static final String MAT = "2000";//数学コード
	private static final String JAP = "3000";//国語コード
	private static final String SCI = "4000";//理科コード
	private static final String SOC = "5000";//社会コード
	private static final String ALL = "7000";//総合コード
	private static final String MIN_ENG = "1001";//英語コード
	private static final String MAX_ENG = "1999";
	private static final String MIN_MAT = "2001";//数学コード
	private static final String MAX_MAT = "2999";
	private static final String MIN_JAP = "3001";//国語コード
	private static final String MAX_JAP = "3999";
	private static final String MIN_SCI = "4001";//理科コード
	private static final String MAX_SCI = "4999";
	private static final String MIN_SOC = "5001";//社会コード
	private static final String MAX_SOC = "5999";
	private static final String MIN_ALL = "7001";//総合コード
	private static final String MAX_ALL = "7999";
	
	
	private String targetEngSubCd;//対象偏差値科目
	private String targetMathSubCd;
	private String targetJapSubCd;
	private String targetSciSubCd;
	private String targetSocSubCd;
	private String targetAns1stSciSubCd;	// 第1解答課目（理科）の科目コード
	private String targetAns1stSocSubCd;	// 第1解答課目（地歴公民）の科目コード
	private String targetExamCd;			// 対象模試コード
	
	private Collection subRecordDatas;
	private Collection courseDatas;
	private Collection candidateRatingDatas;
	private String candidateRatingLength;

	//:::::::::::: JSTL Coreタグ用にサイズが必要 :::::::::::
	public String getSubRecorDataSize(){
		return getCollectionSize(getSubRecordDatas());
	}
	public String getCourseDataSize(){
		return getCollectionSize(getCourseDatas());
	}
	public String getCandidateRatingDataSize(){
		return getCollectionSize(getCandidateRatingDatas());
	}
	public String getCandidateRatingLength(){
		return getCollectionSize(getCandidateRatingDatas());
	}
	//:::::::::::: Size取得用メソッド
	public String getCollectionSize(Collection collection){
		int size = 0;
		if(collection != null)
			size = collection.size();
		return Integer.toString(size);	
	}

	/**
	 * その教科に属するSubRecordのx個目を取得する
	 * 存在しなければ空のSubRecordを返す
	 * @param
	 * @return
	 */
	public SubRecordData getEng(int x){
		if(getEngCourse().size() >= x){
			return (SubRecordData) ((List)getEngCourse()).get(x-1);
		}else{
			return SubRecordData.getEmptyData();	
		}
	}
	public SubRecordData getMat(int x){
		if(getMatCourse().size() >= x){
			return (SubRecordData) ((List)getMatCourse()).get(x-1);
		}else{
			return SubRecordData.getEmptyData();	
		}
	}
	public SubRecordData getJap(int x){
		if(getJapCourse().size() >= x){
			return (SubRecordData) ((List)getJapCourse()).get(x-1);
		}else{
			return SubRecordData.getEmptyData();	
		}
	}
	public SubRecordData getSci(int x){
		if(getSciCourse().size() >= x){
			return (SubRecordData) ((List)getSciCourse()).get(x-1);
		}else{
			return SubRecordData.getEmptyData();	
		}
	}
	public SubRecordData getSoc(int x){
		if(getSocCourse().size() >= x){
			return (SubRecordData) ((List)getSocCourse()).get(x-1);
		}else{
			return SubRecordData.getEmptyData();	
		}
	}
	public SubRecordData getAll(int x){
		if(getAllCourse().size() >= x){
			return (SubRecordData) ((List)getAllCourse()).get(x-1);
		}else{
			return SubRecordData.getEmptyData();	
		}
	}
	/**
	 * 指定教科(SubRecordData)のコレクション
	 * @param courseId
	 * @return
	 */
	public Collection getCourse(String courseId){
		Collection tempCollection = new ArrayList();
		for(Iterator it=getSubRecordDatas().iterator(); it.hasNext();){
			SubRecordData subRecordData = (SubRecordData)it.next();
			if(subRecordData.getSubCd() != null && subRecordData.getSubCd().substring(0, 1).equals(courseId.substring(0, 1))){
				tempCollection.add(subRecordData);
			}
		}
		return tempCollection;
	}
	public Collection getEngCourse(){
		return getCourse(ENG);
	}
	public Collection getMatCourse(){
		return getCourse(MAT);
	}
	public Collection getJapCourse(){
		return getCourse(JAP);
	}
	public Collection getSciCourse(){
		return getCourse(SCI);
	}
	public Collection getSocCourse(){
		return getCourse(SOC);
	}
	public Collection getAllCourse(){
		return getCourse(ALL);
	}
	/**
	 * 指定教科の範囲からのコレクション
	 * @param minSub
	 * @param maxSub
	 * @return FloatのCollectionを返す
	 */
	public Collection getCDevis(String minSub, String maxSub){
		Collection tempCollection = new ArrayList();
		for(Iterator it=getSubRecordDatas().iterator(); it.hasNext();){
			SubRecordData subRecordData = (SubRecordData)it.next();
			if(subRecordData.getCDeviation() == null){
				subRecordData.setCDeviation("0.0f");
			}
			if(subRecordData.getSubCd() == null){
				subRecordData.setSubCd("0");
			}
            
			//if(Integer.valueOf(subRecordData.getSubCd()).intValue() >= Integer.valueOf(minSub).intValue() && Integer.valueOf(subRecordData.getSubCd()).intValue() <= Integer.valueOf(maxSub).intValue()){
			if (subRecordData.getSubCd().charAt(0) == minSub.charAt(0) && subRecordData.getSubCd().charAt(0) == maxSub.charAt(0)) {
				//tempCollection.add(Float.valueOf(subRecordData.getCDeviation()));//[2] del
				tempCollection.add(Double.valueOf(subRecordData.getCDeviation()));//[2] add
			}
		}
		return tempCollection;
	}
	public Collection getEnglishCDevis(){
		return getCDevis(MIN_ENG, MAX_ENG);
	}
	public Collection getMathCDevis(){
		return getCDevis(MIN_MAT, MAX_MAT);
	}
	public Collection getJapaneseCDevis(){
		return getCDevis(MIN_JAP, MAX_JAP);
	}
	public Collection getScienceCDevis(){
		return getCDevis(MIN_SCI, MAX_SCI);
	}
	public Collection getSocietyCDevis(){
		return getCDevis(MIN_SOC, MAX_SOC);
	}
	public Collection getAllCDevis(){
		return getCDevis(MIN_ALL, MAX_ALL);
	}
	/**
	 * 指定科目(SubRecorData)を返す
	 * @param subCd
	 * @return
	 */
	public SubRecordData getSubRecordData(String subCd){
		SubRecordData subRecordData = null;
		for(Iterator it=getSubRecordDatas().iterator(); it.hasNext();){
			SubRecordData data = (SubRecordData)it.next();
			if(data.getSubCd() != null) {
				if(data.getSubCd().equals(subCd)){
					subRecordData = data;
				}
			}
		}
		if(subRecordData == null)
			subRecordData = SubRecordData.getEmptyData();
		return subRecordData;
	}
	public SubRecordData getMaxEngSubRecordData(){
		return getSubRecordData(ENG);	
	}
	public SubRecordData getMaxMathSubRecordData(){
		return getSubRecordData(MAT);	
	}
	public SubRecordData getMaxJapSubRecordData(){
		return getSubRecordData(JAP);	
	}
	public SubRecordData getMaxSciSubRecordData(){
		return getSubRecordData(SCI);	
	}
	public SubRecordData getMaxSocSubRecordData(){
		return getSubRecordData(SOC);	
	}

	/**
	 * 
	 * @param collection その教科に所属する科目の偏差値データのCollection
	 * @return その教科における平均値を計算して返す 注：データない場合は空白を返す
	 */
	private String getAvgSub(Collection collection){
		if(collection.size() == 0)
			return "";

		double sumValue = 0.0d;
		for (Iterator it=collection.iterator(); it.hasNext(); ) {
			
			//浮動小数点対応のため
			//一旦整数値に変換して加算を行う
			sumValue += (int) Math.round(((Double)it.next()).doubleValue() * 10);
		}

		double result = sumValue / (double)collection.size();

		if(Double.isNaN(result)){
			result =  0.0f;
		}

		//浮動小数点対応のため
		//整数値で算出した結果を小数に戻す
		result = MathUtil.to2ndDecimal((double)result/10);

		return Double.toString(result);
	}
	public String getAvgEng(){
		return getAvgSub(getEnglishCDevis());
	}
	public String getAvgMath(){
		return getAvgSub(getMathCDevis());
	}
	public String getAvgJap(){
		return getAvgSub(getJapaneseCDevis());
	}
	public String getAvgSci(){
		return getAvgSub(getScienceCDevis());
	}
	public String getAvgSoc(){
		return getAvgSub(getSocietyCDevis());
	}
	public String getAvgAll(){
		return getAvgSub(getAllCDevis());
	}
	/**
	 * 
	 * @param collection その教科に所属する科目の偏差値データのCollection
	 * @return その教科における最大値を計算して返す 注：データない場合は空白を返す
	 */
	private String getMaxSub(Collection collection){
		if(collection.size() == 0)
			return "";
		//float maxValue = 0.0f;//[2] del
		double maxValue = 0.0d;//[2] add
		for (Iterator it=collection.iterator(); it.hasNext(); ) {
			//float thisValue = ((Float)it.next()).floatValue();//[2] del
			double thisValue = ((Double)it.next()).doubleValue();//[2] add
			if(thisValue > maxValue)
				maxValue = thisValue;
		}
		//return Float.toString(maxValue);//[2] del
		return Double.toString(maxValue);//[2] add
		
	}
	public String getMaxEng(){
		return getMaxSub(getEnglishCDevis());
	}
	public String getMaxMath(){
		return getMaxSub(getMathCDevis());
	}
	public String getMaxJap(){
		return getMaxSub(getJapaneseCDevis());
	}
	public String getMaxSci(){
		return getMaxSub(getScienceCDevis());
	}
	public String getMaxSoc(){
		return getMaxSub(getSocietyCDevis());
	}
	public String getMaxAll(){
		return getMaxSub(getAllCDevis());
	}
	/**
	 * 指定の教科において偏差値が最大のものの科目コードを取得
	 * @param minSub
	 * @param maxSub
	 * @return
	 */
	public String getMaxSubjectCode(String minSub, String maxSub){
		String tempSubCode = null;
		float tempMaxValue = 0.0f;
		for(Iterator it=getSubRecordDatas().iterator(); it.hasNext();){
			SubRecordData subRecordData = (SubRecordData)it.next();
			//if(Integer.valueOf(subRecordData.getSubCd()).intValue() >= Integer.valueOf(minSub).intValue() && Integer.valueOf(subRecordData.getSubCd()).intValue() <= Integer.valueOf(maxSub).intValue()){
			if (subRecordData.getSubCd().charAt(0) == minSub.charAt(0) && subRecordData.getSubCd().charAt(0) == maxSub.charAt(0)) {
				if(tempMaxValue <= Float.valueOf(subRecordData.getCDeviation()).floatValue()){
					tempMaxValue = Float.valueOf(subRecordData.getCDeviation()).floatValue();
					tempSubCode = subRecordData.getSubCd();
				}
			}
		}
		return tempSubCode;
	}	
	public String getMaxEngCode(){
		return getMaxSubjectCode(MIN_ENG, MAX_ENG);
	}
	public String getMaxMathCode(){
		return getMaxSubjectCode(MIN_MAT, MAX_MAT);
	}
	public String getMaxJapCode(){
		return getMaxSubjectCode(MIN_JAP, MAX_JAP);
	}
	public String getMaxSciCode(){
		return getMaxSubjectCode(MIN_SCI, MAX_SCI);
	}
	public String getMaxSocCode(){
		return getMaxSubjectCode(MIN_SOC, MAX_SOC);
	}	

	/**
	 * 指定の教科において第1解答科目（範囲区分=1）の科目コードを取得
	 * @param minSub
	 * @param maxSub
	 * @return 科目コード or null
	 */
	public String getAns1stSubjectCode(String minSub, String maxSub){
		for(Iterator it=getSubRecordDatas().iterator(); it.hasNext();){
			SubRecordData subRecordData = (SubRecordData)it.next();
			if (subRecordData.getSubCd().charAt(0) == minSub.charAt(0) && subRecordData.getSubCd().charAt(0) == maxSub.charAt(0)) {
				// 第1解答科目（範囲区分=1）
				if(("1").equals(subRecordData.getScope())){
					return subRecordData.getSubCd();
				}
			}
		}
		// 第1解答科目（範囲区分=1）が無い場合はnullを返す
		return null;
	}

	/**
	 * 理科において第1解答科目（範囲区分=1）の科目コードを取得
	 * @return 科目コード or null
	 */
	public String getAns1stSubjectCodeSci(){
		return getAns1stSubjectCode(MIN_SCI, MAX_SCI);
	}

	/**
	 * 地歴公民において第1解答科目（範囲区分=1）の科目コードを取得
	 * @return 科目コード or null
	 */
	public String getAns1stSubjectCodeSoc(){
		return getAns1stSubjectCode(MIN_SOC, MAX_SOC);
	}

	/**
	 * 理科の第1解答科目の偏差値を返す
	 * 		第1解答科目模試で無い場合は平均偏差値を返す
	 * 		第1解答科目模試でも第1解答科目が無い場合は平均偏差値を返す
	 * @return 偏差値
	 */
	public String getAns1stSci(){
		// 第1解答科目模試
		if (KNUtil.isAns1st(getExamCd())) {
			String subcd = getAns1stSubjectCodeSci();
			if (subcd != null) {
				// 第1解答科目の偏差値
				SubRecordData subRecordData = getSubRecordData(subcd);
				return subRecordData.getCDeviation();
			} else {
				// 平均偏差値
				return getAvgSci();
			}
		} else {
			// 平均偏差値
			return getAvgSci();
		}
	}

	/**
	 * 地歴公民の第1解答科目の偏差値を返す
	 * 		第1解答科目模試で無い場合は平均偏差値を返す
	 * 		第1解答科目模試でも第1解答科目が無い場合は平均偏差値を返す
	 * @return 偏差値
	 */
	public String getAns1stSoc(){
		// 第1解答科目模試
		if (KNUtil.isAns1st(getExamCd())) {
			String subcd = getAns1stSubjectCodeSoc();
			if (subcd != null) {
				// 第1解答科目の偏差値
				SubRecordData subRecordData = getSubRecordData(subcd);
				return subRecordData.getCDeviation();
			} else {
				// 平均偏差値
				return getAvgSoc();
			}
		}
		else {
			// 平均偏差値
			return getAvgSoc();
		}
	}

	/**
	 * 空のExaminationDataを返す。NullPointerが起こらないように
	 * すべてを非Nullにする。
	 * @return
	 */
	public static ExaminationData getEmptyExaminationData(){
		ExaminationData data = new ExaminationData();
		data.setExamYear(new String());
		data.setExamCd(new String());
		data.setExamName(new String());
		data.setExamAddrName(new String());
		data.setExamTypeCd(new String());
		data.setExamSt(new String());
		data.setExamDiv(new String());
		data.setDispSequence(new String());
		data.setMasterDiv(new String());
		data.setSubRecordDatas(new ArrayList());
		data.setCourseDatas(new ArrayList());
		data.setCandidateRatingDatas(new ArrayList());
		return data;
	}
	
	/**
	 * 対象の科目コードの偏差値を返す
	 * @return 偏差値
	 */
	public String getTargetSubEng() {
		String dev = getSubRecordData(getTargetEngSubCd()).getCDeviation();
		if(dev == null) {return "0.0f";}
		return dev;
	}
	public String getTargetSubMath() {
		String dev = getSubRecordData(getTargetMathSubCd()).getCDeviation();
		if(dev == null) {return "0.0f";}
		return dev;
	}
	public String getTargetSubJap() {
		String dev = getSubRecordData(getTargetJapSubCd()).getCDeviation();
		if(dev == null) {return "0.0f";}
		return dev;
	}
	public String getTargetSubSci() {
		String dev = getSubRecordData(getTargetSciSubCd()).getCDeviation();
		if(dev == null) {return "0.0f";}
		return dev;
	}
	public String getTargetSubSoc() {
		String dev = getSubRecordData(getTargetSocSubCd()).getCDeviation();
		if(dev == null) {return "0.0f";}
		return dev;
	}

	// 第1解答科目（理科）の科目コードの偏差値を返す
	// 大学の合格者平均のみ使用可
	public String getTargetAns1stSubSci() {
		if (KNUtil.isAns1st(getTargetExamCd())) {
			if (getTargetAns1stSciSubCd() != null) {
				String dev = getSubRecordData(getTargetAns1stSciSubCd()).getCDeviation();
				if(dev == null) {return "0.0f";}
				return dev;
			} else {
				return getAvgSciSingle();
			}
		}
		return getAvgSciSingle();
	}

	// 第1解答科目（地歴公民）の科目コードの偏差値を返す
	// 大学の合格者平均のみ使用可
	public String getTargetAns1stSubSoc() {
		if (KNUtil.isAns1st(getTargetExamCd())) {
			if (getTargetAns1stSocSubCd() != null) {
				String dev = getSubRecordData(getTargetAns1stSocSubCd()).getCDeviation();
				if(dev == null) {return "0.0f";}
				return dev;
			} else {
				return getAvgSocSingle();
			}
		}
		return getAvgSocSingle();
	}

	/**
	 * @return
	 */
	public Collection getSubRecordDatas() {
		return subRecordDatas;
	}

	/**
	 * @param collection
	 */
	public void setSubRecordDatas(Collection collection) {
		subRecordDatas = collection;
	}

	/**
	 * @return
	 */
	public Collection getCourseDatas() {
		return courseDatas;
	}

	/**
	 * @param collection
	 */
	public void setCourseDatas(Collection collection) {
		courseDatas = collection;
	}

	/**
	 * @return
	 */
	public Collection getCandidateRatingDatas() {
		return candidateRatingDatas;
	}

	/**
	 * @param collection
	 */
	public void setCandidateRatingDatas(Collection collection) {
		candidateRatingDatas = collection;
	}

	/**
	 * @return
	 */
	public String getMAX_ALL() {
		return MAX_ALL;
	}

	/**
	 * @return
	 */
	public String getMAX_ENG() {
		return MAX_ENG;
	}

	/**
	 * @return
	 */
	public String getMAX_JAP() {
		return MAX_JAP;
	}

	/**
	 * @return
	 */
	public String getMAX_MAT() {
		return MAX_MAT;
	}

	/**
	 * @return
	 */
	public String getMAX_SCI() {
		return MAX_SCI;
	}

	/**
	 * @return
	 */
	public String getMAX_SOC() {
		return MAX_SOC;
	}

	/**
	 * @return
	 */
	public String getMIN_ALL() {
		return MIN_ALL;
	}

	/**
	 * @return
	 */
	public String getMIN_ENG() {
		return MIN_ENG;
	}

	/**
	 * @return
	 */
	public String getMIN_JAP() {
		return MIN_JAP;
	}

	/**
	 * @return
	 */
	public String getMIN_MAT() {
		return MIN_MAT;
	}

	/**
	 * @return
	 */
	public String getMIN_SCI() {
		return MIN_SCI;
	}

	/**
	 * @return
	 */
	public String getMIN_SOC() {
		return MIN_SOC;
	}

	/**
	 * @return
	 */
	public String getENG() {
		return ENG;
	}

	/**
	 * @return
	 */
	public String getJAP() {
		return JAP;
	}

	/**
	 * @return
	 */
	public String getMAT() {
		return MAT;
	}

	/**
	 * @return
	 */
	public String getSCI() {
		return SCI;
	}

	/**
	 * @return
	 */
	public String getSOC() {
		return SOC;
	}

	/**
	 * @return
	 */
	public String getALL() {
		return ALL;
	}
	/**
	 * @return
	 */
	public String getTargetEngSubCd() {
		return targetEngSubCd;
	}

	/**
	 * @return
	 */
	public String getTargetJapSubCd() {
		return targetJapSubCd;
	}

	/**
	 * @return
	 */
	public String getTargetMathSubCd() {
		return targetMathSubCd;
	}

	/**
	 * @return
	 */
	public String getTargetSciSubCd() {
		return targetSciSubCd;
	}

	/**
	 * @return
	 */
	public String getTargetSocSubCd() {
		return targetSocSubCd;
	}

	/**
	 * @return
	 */
	public String getTargetAns1stSciSubCd() {
		return targetAns1stSciSubCd;
	}

	/**
	 * @return
	 */
	public String getTargetAns1stSocSubCd() {
		return targetAns1stSocSubCd;
	}

	/**
	 * @return
	 */
	public String getTargetExamCd() {
		return targetExamCd;
	}

	/**
	 * @param string
	 */
	public void setTargetEngSubCd(String string) {
		targetEngSubCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetJapSubCd(String string) {
		targetJapSubCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetMathSubCd(String string) {
		targetMathSubCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetSciSubCd(String string) {
		targetSciSubCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetSocSubCd(String string) {
		targetSocSubCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetAns1stSciSubCd(String string) {
		targetAns1stSciSubCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetAns1stSocSubCd(String string) {
		targetAns1stSocSubCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCd(String string) {
		targetExamCd = string;
	}

	/**
	 * 対象学年を返す
	 * @return
	 */
	public String getTergetGrade() {
		return tergetGrade;
	}
	
	/**
	 * 内部データ開放日を返す
	 * @return
	 */
	public String getInDataOpenDate() {
		return inDataOpenDate;
	}
	
	/**
	 * 外部データ開放日を返す
	 * @return
	 */
	public String getOutDataOpenData() {
		return outDataOpenData;
	}
	
	/**
	 * ドッキング先模試コードを返す
	 * @return
	 */
	public String getDockingExamCd() {
		return dockingExamCd;
	}
	
	/**
	 * ドッキングタイプを返す
	 * @return
	 */
	public String getDockingType() {
		return dockingType;
	}
	
	
	/**
	 * 対象学年を設定する
	 * @param tergetGrade
	 */
	public void setTergetGrade(String tergetGrade) {
		this.tergetGrade = tergetGrade;
	}
	
	/**
	 * 内部データ開放日を設定する
	 * @param inDataOpenDate
	 */
	public void setInDataOpenDate(String inDataOpenDate) {
		this.inDataOpenDate = inDataOpenDate;
	}
	
	/**
	 * 外部データ開放日を設定する
	 * @param outDataOpenData
	 */
	public void setOutDataOpenData(String outDataOpenData) {
		this.outDataOpenData = outDataOpenData;
	}
	
	/**
	 * ドッキング先模試コードを設定する
	 * @param dockingExamCd
	 */
	public void setDockingExamCd(String dockingExamCd) {
		this.dockingExamCd = dockingExamCd;
	}
	
	/**
	 * ドッキングタイプを設定する
	 * @param dockingType
	 */
	public void setDockingType(String dockingType) {
		this.dockingType = dockingType;
	}
	
	/**
	 * 模試回を返す
	 * @return
	 */
	public String getExamSt() {
		return examSt;
	}
	
	/**
	 * 模試区分を返す
	 * @return
	 */
	public String getExamDiv() {
		return examDiv;
	}
	
	/**
	 * 表示用シーケンスを返す
	 * @return
	 */
	public String getDispSequence() {
		return dispSequence;
	}
	
	/**
	 * 大学マスタ区分を返す
	 * @return
	 */
	public String getMasterDiv() {
		return masterDiv;
	}
	
	/**
	 * 模試回を設定する
	 * @param examSt
	 */
	public void setExamSt(String examSt) {
		this.examSt = examSt;
	}
	
	/**
	 * 模試区分を設定する
	 * @param examDiv
	 */
	public void setExamDiv(String examDiv) {
		this.examDiv = examDiv;
	}
	
	/**
	 * 表示用シーケンスを設定する
	 * @param dispSequence
	 */
	public void setDispSequence(String dispSequence) {
		this.dispSequence = dispSequence;
	}
	
	/**
	 * 大学マスタ区分を設定する
	 * @param masterDiv
	 */
	public void setMasterDiv(String masterDiv) {
		this.masterDiv = masterDiv;
	}
}

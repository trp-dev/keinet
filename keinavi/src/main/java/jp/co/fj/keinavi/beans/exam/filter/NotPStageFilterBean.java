package jp.co.fj.keinavi.beans.exam.filter;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * プライムステージ用の模試フィルタ
 *
 * 2019.10.10	[新規作成]
 *
 *
 * @author KAWAI - TOTEC
 * @version 1.0
 *
 */
public class NotPStageFilterBean implements IExamFilterBean {

	/**
	 * @see jp.co.fj.keinavi.util.taglib.exam.IExamFilterBean#execute(
	 * 			jp.co.fj.keinavi.data.LoginSession,
	 * 			jp.co.fj.keinavi.data.ExamSession,
	 * 			jp.co.fj.keinavi.data.ExamData)
	 */
	public boolean execute(final LoginSession login,
			final ExamSession examSession, final ExamData exam) {

		// プライムステージならOK
		return KNUtil.isPStage(exam);
	}

}

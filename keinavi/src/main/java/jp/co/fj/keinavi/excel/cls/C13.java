/*
 * 校内成績分析−過年度比較　志望大学評価別人数
 * 作成日: 2004/07/13
 * @author	C.Murata
 */
 
package jp.co.fj.keinavi.excel.cls;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.cls.C13Item;

import jp.co.fj.keinavi.util.log.*;

/**
 * @author keinet
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class C13 {
	
	/*
	 * 出力帳票選択処理
	 * フラグにより帳票出力の判断をする。
	 * 		c13Item c13Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID:ユーザID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	 
	 public boolean c13(C13Item c13Item, ArrayList outfilelist, int intSaveFlg, String UserID, KNSheetLog sheetLog) {
	 	
		KNLog log = KNLog.getInstance(null,null,null);

	 	try{
	 		
			//C13Itemから各帳票を出力
	 		int ret = 0;
			if ( c13Item.getIntHyouFlg()==0 && c13Item.getIntKmkHikakuFlg()==0 ) {
				throw new Exception("C13 ERROR : フラグ異常");
			}
			
	 		if (c13Item.getIntHyouFlg() == 1) {
				C13_01 exceledit01 = new C13_01();
				ret = exceledit01.c13_01EditExcel(c13Item,outfilelist,intSaveFlg,UserID);
				if (ret!=0) {
					log.Err("0"+ret+"C13_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C13_01");
			}
			
			if (c13Item.getIntKmkHikakuFlg() == 1){
				if (c13Item.getIntHikakuCntFlg() == 1){
					C13_02 exceledit02 = new C13_02();
					ret = exceledit02.c13_02EditExcel(c13Item,outfilelist,intSaveFlg,UserID);
					if (ret!=0) {
						log.Err("0"+ret+"C13_02","帳票作成エラー","");
						return false;
					}
					sheetLog.add("C13_02");
				}
				if (c13Item.getIntHikakuCntFlg() == 2){
					C13_03 exceledit03 = new C13_03();
					ret = exceledit03.c13_03EditExcel(c13Item,outfilelist,intSaveFlg,UserID);
					if (ret!=0) {
						log.Err("0"+ret+"C13_03","帳票作成エラー","");
						return false;
					}
					sheetLog.add("C13_03");
				}
			}
			//c13Itemから各帳票を出力
	 	}
	 	catch(Exception e){
			log.Err("99C13","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
	 	}
	 	
	 	return true;
	 	
	 }

}

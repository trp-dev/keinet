package jp.co.fj.keinavi.util.taglib;

import org.apache.taglibs.standard.tag.common.core.ChooseTag;

/**
 *
 * keinavi 用に制限をなくした choose タグクラス
 * 
 * 2006/08/25    [新規作成]
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class KNChooseTag extends ChooseTag {    

    //なにもしない
    
}

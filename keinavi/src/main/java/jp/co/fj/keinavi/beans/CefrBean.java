package jp.co.fj.keinavi.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;



public class CefrBean extends DefaultBean {

    /**
     *
     */
    private static final long serialVersionUID = -3099847510413988391L;


    /**
     * @return
     * @throws Exception
     */
    public String cefr(String examyear, String examcd, String individualId, Connection conn) throws Exception{

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String cefr = "";
        try{
            pstmt = conn.prepareStatement(cefrDataSql());
            pstmt.setString(1, examyear);
            pstmt.setString(2, examcd);
            pstmt.setString(3, individualId);

            rs = pstmt.executeQuery();

            while(rs.next()) {
                if(rs.getInt("SORT1") == 0 && rs.getInt("SORT2") == 0) {
                    cefr = "-";
                } else {
                    if(rs.getInt("SORT1") == 0){
                        cefr = rs.getString("CERFLEVELNAME_ABBR2");
                    } else if(rs.getInt("SORT2") == 0) {
                        cefr = rs.getString("CERFLEVELNAME_ABBR1");
                    } else if(rs.getInt("SORT1") <= rs.getInt("SORT2")) {
                        cefr = rs.getString("CERFLEVELNAME_ABBR1");
                    } else {
                        cefr = rs.getString("CERFLEVELNAME_ABBR2");
                    }
                }
            }

            if(cefr.equals("")) {
                cefr = "-";
            }

        return cefr;

        } finally {
            DbUtils.closeQuietly(null, pstmt, rs);
        }

    }

    /**
     * @return
     */
    private String cefrDataSql() {
        String sql =
        "SELECT"+
        " CI.CERFLEVELNAME_ABBR CERFLEVELNAME_ABBR1"+
        " ,CI2.CERFLEVELNAME_ABBR CERFLEVELNAME_ABBR2"+
        " ,CI.SORT SORT1"+
        " ,CI2.SORT SORT2"+
        " FROM "+
        " CEFR_RECORD_I CR_I "+
        " LEFT JOIN "+
        " EXAMINATION EX "+
        " ON EX.EXAMYEAR = CR_I.EXAMYEAR "+
        " AND EX.EXAMCD = CR_I.EXAMCD "+
        " LEFT JOIN "+
        " CEFR_INFO CI "+
        " ON CR_I.EXAMYEAR = CI.EVENTYEAR "+
        " AND CR_I.CEFRLEVELCD1 = CI.CEFRLEVELCD "+
        " AND EX.EXAMDIV = CI.EXAMDIV "+
        " LEFT JOIN "+
        " CEFR_INFO CI2 "+
        " ON CR_I.EXAMYEAR = CI2.EVENTYEAR "+
        " AND CR_I.CEFRLEVELCD2 = CI2.CEFRLEVELCD "+
        " AND EX.EXAMDIV = CI2.EXAMDIV "+
        " WHERE "+
        " CR_I.EXAMYEAR = ?"+
        " AND CR_I.EXAMCD = ?"+
        " AND CR_I.INDIVIDUALID = ?";

        return sql;
    }

    @Override
    public void execute() throws SQLException, Exception {
        // TODO 自動生成されたメソッド・スタブ

    }
}

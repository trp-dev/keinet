/*
 * 作成日: 2004/09/16
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.news;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;
import jp.co.fj.keinavi.data.news.InformList;

import com.fjh.beans.DefaultSearchBean;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.date.DateUtil;
import jp.co.fj.keinavi.util.date.FullDateUtil;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class InformSearchBean extends DefaultSearchBean {

	private String userID     = null; // ユーザID
	private int    userMode  = 0; 	// 利用者モード
	private String displayDiv = null; // 表示区分


	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// TODO 自動生成されたメソッド・スタブ

		// 日付用クラス
		DateUtil date = new FullDateUtil();

		// SQLをセットアップ
		StringBuffer query = new StringBuffer();
		query.append("SELECT  INF.INFOID        ");
		query.append("       ,INF.DISPLAYDIV    ");
		query.append("       ,INF.TITLE         ");
		query.append("       ,INF.DISPSTARTDTIM ");
		query.append("       ,INF.DISPENDDTIM   ");
		query.append("       ,INF.TEXT          ");
		query.append("       ,IDP.KEISAI        ");
		query.append("       ,IDP.DISPSEQUENCE  ");
		query.append("  FROM  INFORMATION INF, INFODISP IDP");
		query.append("  WHERE INF.INFOID     = IDP.INFOID  ");

		//　お知らせ表示区分
		//    0:お知らせ情報、1:Kei-Net注目コンテンツ
		query.append("    AND INF.DISPLAYDIV = '" + displayDiv + "' ");	

		// 掲載開始日時("yyyymmddhh")〜掲載終了日時("yyyymmddhh")の範囲
		String nowDate = date.date2String().substring(0,10);
		query.append("    AND ( INF.DISPSTARTDTIM <= '" + nowDate + "' AND '" + nowDate + "' < INF.DISPENDDTIM ) " );

		// 表示区分がお知らせの場合以下の条件を指定
		//
		// ログイン情報により掲載場所を決定する
		//   掲載場所が”共通情報”の場合      ： どのＩＤでも表示
		//   掲載場所が”県単位の情報”の場合  ： ＩＤの県コードと一致するお知らせのみ表示
		//   掲載場所が”学校単位の情報”の場合： ＩＤ（高校コード）と一致するお知らせのみ表示
		//
		//   高校、営業の切り分けは利用者モード下１桁目による
		//     1:高校  2:営業  3:校舎（工事中）
		//   高校の場合
		//     共通情報("99999")、県コード２桁＋"999"、高校コード
		//   営業の場合
		//     共通情報("99999")
		//
		if ( displayDiv.equals("0") ) {
			if ( !(userID == null || userID.equals("")) ) {
				if( userMode % 10 == 1 ) {				// 高校
					query.append("    AND (   IDP.KEISAI = '99999' " );
					query.append("         OR IDP.KEISAI = '" + userID.substring(0,2) + "999' " );
					query.append("         OR IDP.KEISAI = '" + userID + "' " );
					query.append("        )    " );
				} else if (userMode % 10 == 2) { 		// 営業
					query.append("    AND  IDP.KEISAI = '99999' " );
				} else if (userMode % 10 == 3) { 		// 校舎
					query.append("    AND  IDP.KEISAI = '99999' " );
				} else {								// その他
					query.append("    AND  IDP.KEISAI = '99999' " );
				}
			} else {
				if( userMode % 10 == 1 ) {				// 高校
					query.append("    AND  IDP.KEISAI = '99999' " );
				} else if (userMode % 10 == 2) { 		// 営業
					query.append("    AND  IDP.KEISAI = '99999' " );
				} else if (userMode % 10 == 3) { 		// 校舎
					query.append("    AND  IDP.KEISAI = '99999' " );
				} else {								// その他
					query.append("    AND  IDP.KEISAI = '99999' " );
				}
			}
		}
		// 表示順を昇順で表示
		// ------------------------------------------------------------
		// 2004.11.22
		// Yoshimoto KAWAI - Totec
		// さらに掲載開始日の降順でソートする
		// ------------------------------------------------------------
		query.append("  ORDER BY IDP.DISPSEQUENCE ASC, INF.DISPSTARTDTIM DESC  ");

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query.toString());

			rs = ps.executeQuery();
			
			getPageNo();
			getEndCount();
			while (rs.next()) {
				InformList list = new InformList();
				list.setInfoId(rs.getString(1));
				list.setDisplayDiv(rs.getString(2));
				list.setTitle(rs.getString(3));
				list.setDispStartDTim(rs.getString(4));
				list.setDispEndDTim(rs.getString(5));
				list.setText(rs.getString(6));
				list.setDispDate(getDispDate(list.getDispStartDTim()));
				// 掲載区分（"一般"、"高校別"、"県別"をセット）
				String keisai  =rs.getString(7);	// 掲載場所
				String dispDiv = null;				// 掲載区分
				if (keisai.equals("99999") ) {
					dispDiv = "一般";	
				} else if ( keisai.substring(2).equals("999")) {
					dispDiv = "県別";	
				} else {
					dispDiv = "高校別";	
				}
				list.setDispDiv(dispDiv);
				
				recordSet.add(list);
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}


	}

	/**
	 * 表示件数を取得する。
	 * 
	 * @param  string
	 *            "0":お知らせ表示件数  
	 *            "1":Kei-Net注目情報表示件数
	 * @return お知らせ表示件数
	 * @throws Exception
	 */
	public int getInfoDisplayCountLimit(String string) {
		int  count = 0;
		try {
			if (string.equals("0")) {
				count = KNCommonProperty.getInfoDisplayCountLimit();
			} else {
				count = KNCommonProperty.getNoteInfoDisplayCountLimit();
			}
		} catch (Exception e) {
			new Exception(e);
		}
		return count;
	}

	/**
	 * 掲載開始日付（yyyy/mm/dd)を取得
	 * 	 * @param string
	 */
	public String getDispDate(String date) {
		return date.substring(0,4) + "/" + date.substring(4,6) + "/" + date.substring(6,8);
	}

	/**
	 * ログインユーザID
	 * @return
	 */
	public String getUserID() {
		return userID;
	}

	/**
	 * @param LoginSession
	 */
	public void setUserID(String string) {
		userID = string;
	}


	/**
	 * 利用者モード
	 * 	 * @param i
	 */
	public void setUserMode(int i) {
		userMode = i;
	}
	/**
	 * @return
	 */
	public int getUserMode() {
		return userMode;
	}

	/**
	 * @return
	 */
	public String getDisplayDiv() {
		return displayDiv;
	}

	/**
	 * @param string
	 */
	public void setDisplayDiv(String string) {
		displayDiv = string;
	}

}

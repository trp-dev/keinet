package jp.co.fj.keinavi.beans.admin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 複合クラスBean
 * 
 * 2004.09.08	[新規作成]
 * 
 * 2005.12.28	Yoshimoto KAWAI - TOTEC
 * 				リファクタリング
 *
 * @author 奥村ゆかり
 * @version 1.0
 * 
 */
public class CompositeBean extends DefaultBean {

	// 複合クラスリスト
	private final List compositeList = new ArrayList();
	
	// 対象年度
	private String targetYear;	
	// 対象学年
	private String targetGrade;
	// 対象複合クラス
	private String targetClassgCd;
	// 学校コード
	private String schoolCd;
	// 対象学年（int）
	private int intTargetGrade;

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		intTargetGrade = Integer.parseInt(targetGrade);
		
		// 一覧モード
		if (targetClassgCd == null) {
			execList();
		// 登録・編集画面モード
		} else {
			execEdit();
		}
	}
	
	private void execList() throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().load("m18").toString());
			ps.setString(1, targetYear);
			ps.setString(2, schoolCd);
			ps.setInt(3, intTargetGrade);
			ps.setString(4, targetYear);
			ps.setInt(5, intTargetGrade);
			ps.setString(6, schoolCd);
			rs = ps.executeQuery();
			while (rs.next()) {
				final CompositeInfo info = new CompositeInfo();
				info.setGroupCode(rs.getString(1));
				info.setClassName(rs.getString(2));
				info.setComment(rs.getString(3));
				info.setNumber(rs.getString(4));
				compositeList.add(info);
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	private void execEdit() throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().load("m19").toString());
			ps.setString(1, targetClassgCd);
			ps.setString(2, targetClassgCd);
			ps.setString(3, targetYear);
			ps.setInt(4, intTargetGrade);
			ps.setString(5, schoolCd);
			ps.setString(6, targetClassgCd);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				final CompositeInfo info = new CompositeInfo();
				info.setGroupCode(targetClassgCd);
				info.setClassName(rs.getString(1));
				info.setComment(rs.getString(2));
				info.setClas(rs.getString(3));
				info.setNumber(rs.getString(4));
				info.setSelectJudge(rs.getInt(5) == 1);
				compositeList.add(info);
			}
			
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	public int getSize() {
		return compositeList.size();
	}
	
	public int getHalfSize() {
		return (int) Math.round((double) compositeList.size() / 2);
	}
	
	/**
	 * @return
	 */
	public List getCompositeList() {
		return compositeList;
	}

	/**
	 * @param schoolCd 設定する schoolCd。
	 */
	public void setSchoolCd(String schoolCd) {
		this.schoolCd = schoolCd;
	}

	/**
	 * @param targetClassgCd 設定する targetClassgCd。
	 */
	public void setTargetClassgCd(String targetClassgCd) {
		this.targetClassgCd = targetClassgCd;
	}

	/**
	 * @param targetGrade 設定する targetGrade。
	 */
	public void setTargetGrade(String targetGrade) {
		this.targetGrade = targetGrade;
	}

	/**
	 * @param targetYear 設定する targetYear。
	 */
	public void setTargetYear(String targetYear) {
		this.targetYear = targetYear;
	}
	
}

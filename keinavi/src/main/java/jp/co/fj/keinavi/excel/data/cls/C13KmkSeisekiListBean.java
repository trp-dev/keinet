package jp.co.fj.keinavi.excel.data.cls;

/**
 * クラス成績概況−個人成績推移型・科目別成績データクラス
 * 作成日: 2004/07/14
 * @author	H.Fujimoto
 * 
 * 2009.11.24   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応 
 */
public class C13KmkSeisekiListBean {
	//学年
	private String strGrade = "";
	//クラス
	private String strClass = "";
	//クラス番号
	private String strClassNo = "";
	//氏名
	private String strKanaName = "";
	//性別
	private String strSex = "";
	//今回配点
	private String strHaitenNow = "";
	//今回得点
	private int intTokutenNow = 0;
	//今回全国偏差値
	private float floHensaAllNow = 0;
	//前回配点
	private String strHaitenLast1 = "";
	//前回得点
	private int intTokutenLast1 = 0;
	//前回全国偏差値
	private float floHensaAllLast1 = 0;
	//前々回配点
	private String strHaitenLast2 = "";
	//前々回得点
	private int intTokutenLast2 = 0;
	//前々回全国偏差値
	private float floHensaAllLast2 = 0;
	//今回学力レベル
	private String strScholarLevelNow = "";
	//前回学力レベル
	private String strScholarLevelLast1 = "";
	//前々回学力レベル
	private String strScholarLevelLast2 = "";
	

	/*----------*/
	/* Get      */
	/*----------*/

	public float getFloHensaAllLast1() {
		return this.floHensaAllLast1;
	}
	public float getFloHensaAllLast2() {
		return this.floHensaAllLast2;
	}
	public float getFloHensaAllNow() {
		return this.floHensaAllNow;
	}
	public int getIntTokutenLast1() {
		return this.intTokutenLast1;
	}
	public int getIntTokutenLast2() {
		return this.intTokutenLast2;
	}
	public int getIntTokutenNow() {
		return this.intTokutenNow;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrClassNo() {
		return this.strClassNo;
	}
	public String getStrGrade() {
		return this.strGrade;
	}
	public String getStrHaitenLast1() {
		return this.strHaitenLast1;
	}
	public String getStrHaitenLast2() {
		return this.strHaitenLast2;
	}
	public String getStrHaitenNow() {
		return this.strHaitenNow;
	}
	public String getStrKanaName() {
		return this.strKanaName;
	}
	public String getStrSex() {
		return this.strSex;
	}
	public String getStrScholarLevelNow() {
		return strScholarLevelNow;
	}
	public String getStrScholarLevelLast1() {
		return strScholarLevelLast1;
	}
	public String getStrScholarLevelLast2() {
		return strScholarLevelLast2;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setFloHensaAllLast1(float floHensaAllLast1) {
		this.floHensaAllLast1 = floHensaAllLast1;
	}
	public void setFloHensaAllLast2(float floHensaAllLast2) {
		this.floHensaAllLast2 = floHensaAllLast2;
	}
	public void setFloHensaAllNow(float floHensaAllNow) {
		this.floHensaAllNow = floHensaAllNow;
	}
	public void setIntTokutenLast1(int intTokutenLast1) {
		this.intTokutenLast1 = intTokutenLast1;
	}
	public void setIntTokutenLast2(int intTokutenLast2) {
		this.intTokutenLast2 = intTokutenLast2;
	}
	public void setIntTokutenNow(int intTokutenNow) {
		this.intTokutenNow = intTokutenNow;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrClassNo(String strClassNo) {
		this.strClassNo = strClassNo;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}
	public void setStrHaitenLast1(String strHaitenLast1) {
		this.strHaitenLast1 = strHaitenLast1;
	}
	public void setStrHaitenLast2(String strHaitenLast2) {
		this.strHaitenLast2 = strHaitenLast2;
	}
	public void setStrHaitenNow(String strHaitenNow) {
		this.strHaitenNow = strHaitenNow;
	}
	public void setStrKanaName(String strKanaName) {
		this.strKanaName = strKanaName;
	}
	public void setStrSex(String strSex) {
		this.strSex = strSex;
	}
	public void setStrScholarLevelNow(String string) {
		this.strScholarLevelNow = string;
	}
	public void setStrScholarLevelLast1(String string) {
		this.strScholarLevelLast1 = string;
	}
	public void setStrScholarLevelLast2(String string) {
		this.strScholarLevelLast2 = string;
	}

}
package jp.co.fj.keinavi.util.individual;

import java.util.Comparator;

import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.keinavi.data.individual.UnivKeiNavi;

import org.apache.commons.lang.StringUtils;
/**
 *
 * 2004.11.08   Tomohisa YAMADA - Totec
 *              新規作成
 *
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 *              マスタデータの持ち方変更

 * 2016.01.12   Hiroyuki Nishiyama - QuiQsoft
 *              大規模改修：ソート順変更

 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 *
 */
public class UnivSorter {

	public class SortByUFDeptSortKey implements Comparator{

		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 * 大学・学部・学科ソートキー順
		 */
		public int compare(Object o1, Object o2) {

			UnivKeiNavi u1 = (UnivKeiNavi) o1;
			UnivKeiNavi u2 = (UnivKeiNavi) o2;

			// 2016/01/12 QQ)Nishiyama 大規模改修 ADD START
			int retVal = 0;

			// 大学のソート
			retVal = this.sortUniv(u1, u2);
			if (retVal != 0) {
				return retVal;
			}

			// 学部のソート
			retVal = this.sortFaculty(u1, u2);
			if (retVal != 0) {
				return retVal;
			}

			// 学科のソート
			retVal = this.sortDept(u1, u2);

			return retVal;
			// 2016/01/12 QQ)Nishiyama 大規模改修 ADD END


			// 2016/01/12 QQ)Nishiyama 大規模改修 DEL START
			/*
			int univCd1 = Integer.parseInt(u1.getUnivCd());
			int univCd2 = Integer.parseInt(u2.getUnivCd());

			if(univCd1 < univCd2){
				return -1;
			}else if(univCd1 > univCd2){
				return 1;
			}else{
				int facultyCd1 = Integer.parseInt(u1.getFacultyCd());
				int facultyCd2 = Integer.parseInt(u2.getFacultyCd());
				if(facultyCd1 < facultyCd2){
					return -1;
				}else if(facultyCd1 > facultyCd2){
					return 1;
				}else{
					String deptSortKey1 = u1.getDeptSortKey();
					String deptSortKey2 = u2.getDeptSortKey();
					if(deptSortKey1.compareTo(deptSortKey2) > 0){
						return 1;
					}else if(deptSortKey1.compareTo(deptSortKey2) < 0){
						return -1;
					}else{
						return 0;
					}
				}
			}
			*/
			// 2016/01/12 QQ)Nishiyama 大規模改修 DEL END


		}

		// 2016/01/12 QQ)Nishiyama 大規模改修 ADD START
		//
		// 大学のソート
		//
		private int sortUniv (UnivKeiNavi u1, UnivKeiNavi u2) {

			int comVal = 0;

			// 1.大学区分
			String uniDiv1 = u1.getUnivDivCd();
			String uniDiv2 = u2.getUnivDivCd();

			// 01：国立 を 02：公立 に見立てる
			if ("01".equals(uniDiv1)) uniDiv1 = "02";
			if ("01".equals(uniDiv2)) uniDiv2 = "02";

			comVal = this.strCompare(uniDiv1, uniDiv2);
			if (comVal != 0) {
				return comVal;
			}

			// 2.カナ付50音順番号
			String kanaNum1 = u1.getKanaNum();
			String kanaNum2 = u2.getKanaNum();
			comVal = this.strCompare(kanaNum1, kanaNum2);
			if (comVal != 0) {
				return comVal;
			}

			// 3.大学コード
			String univCd1 = u1.getUnivCd();
			String univCd2 = u2.getUnivCd();
			comVal = this.strCompare(univCd1, univCd2);
			if (comVal != 0) {
				return comVal;
			}

			return 0;
		}

		//
		// 学部のソート
		//
		private int sortFaculty (UnivKeiNavi u1, UnivKeiNavi u2) {

			int comVal = 0;

			// 1.大学夜間部区分
			int uniNightDiv1 = u1.getFlag_night();
			int uniNightDiv2 = u2.getFlag_night();
			if (uniNightDiv1 < uniNightDiv2){
				return -1;
			} else if (uniNightDiv1 > uniNightDiv2) {
				return 1;
			}

			// 2.学部内容コード
			String facultyConCd1 = u1.getFacultyConCd();
			String facultyConCd2 = u2.getFacultyConCd();
			comVal = this.strCompare(facultyConCd1, facultyConCd2);
			if (comVal != 0) {
				return comVal;
			}

			return 0;
		}

		//
		// 学科のソート
		//
		private int sortDept (UnivKeiNavi u1, UnivKeiNavi u2) {

			int comVal = 0;

			// 1.大学グループ区分（日程コード）
			String unigDiv1 = u1.getSchedule();
			String unigDiv2 = u2.getSchedule();
			comVal = this.strCompare(unigDiv1, unigDiv2);
			if (comVal != 0) {
				return comVal;
			}

			// 2.学科通番
			String deptSerialNo1 = u1.getDeptSerialNo();
			String deptSerialNo2 = u2.getDeptSerialNo();
			comVal = this.strCompare(deptSerialNo1, deptSerialNo2);
			if (comVal != 0) {
				return comVal;
			}

			// 3.日程方式
			String scheduleSys1 = u1.getScheduleSys();
			String scheduleSys2 = u2.getScheduleSys();
			comVal = this.strCompare(scheduleSys1, scheduleSys2);
			if (comVal != 0) {
				return comVal;
			}

			// 4.日程方式枝番
			String scheduleSysBranchCd1 = u1.getScheduleSysBranchCd();
			String scheduleSysBranchCd2 = u2.getScheduleSysBranchCd();
			comVal = this.strCompare(scheduleSysBranchCd1, scheduleSysBranchCd2);
			if (comVal != 0) {
				return comVal;
			}

			// 5.学科カナ名
			String deptNameKana1 = ((UnivData) u1).getSubNameKana();
			String deptNameKana2 = ((UnivData) u2).getSubNameKana();
			comVal = this.strCompare(deptNameKana1, deptNameKana2);
			if (comVal != 0) {
				return comVal;
			}

			// 6.学科コード
			String deptCd1 = u1.getDeptCd();
			String deptCd2 = u2.getDeptCd();
			comVal = this.strCompare(deptCd1, deptCd2);
			if (comVal != 0) {
				return comVal;
			}

			return 0;
		}

		//
		// 文字列比較（Nullは空白に変換して比較）
		//
		private int strCompare (String str1, String str2) {

			if (StringUtils.isEmpty(str1)) str1 = "";
			if (StringUtils.isEmpty(str2)) str2 = "";

			return str1.compareTo(str2);
		}

		// 2016/01/12 QQ)Nishiyama 大規模改修 ADD END


	}
}

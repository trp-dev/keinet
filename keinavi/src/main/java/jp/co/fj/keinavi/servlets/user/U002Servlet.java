package jp.co.fj.keinavi.servlets.user;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.user.LoginUserBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.user.LoginUserData;
import jp.co.fj.keinavi.forms.user.U002Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 * 利用者管理−登録編集画面サーブレット
 *
 * 2005.10.12	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class U002Servlet extends DefaultHttpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// HTTPセッション
		final HttpSession session = request.getSession(false);
		// アクションフォーム
		final U002Form form = (U002Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.user.U002Form");

		// 転送元がJSP
		if ("u002".equals(getBackward(request))) {
			// フォームの値をセッションに保持する
			refrectFormValue((LoginUserData) session.getAttribute("LoginUserData"), form);
		}

		// 転送先がJSP
		if ("u002".equals(getForward(request))) {

			// ログインセッション
			final LoginSession login = getLoginSession(request);

			// 転送元が一覧画面であるなら初期化処理をする
			if ("u001".equals(getBackward(request))) {

				final LoginUserData data;

				// 対象利用者IDがなければ新規
				if ("".equals(form.getTargetLoginId())) {
					data = new LoginUserData();
					// デフォルトはメンテナンス権限なし
					data.setMaintainer(false);
					// 機能はすべて可
					data.setKnFunctionFlag((short) 1);
					data.setScFunctionFlag((short) 1);
					data.setEeFunctionFlag((short) 1);
					data.setAbFunctionFlag((short) 1);

				// 編集ならDBからとってくる
				} else {
					data = getLoginUserData(request, login, form.getTargetLoginId());
				}

				// セッションに入れる
				session.setAttribute(LoginUserData.SESSION_KEY, data);
			}

			request.setAttribute("form", form);

			super.forward(request, response, JSP_U002);

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

	/**
	 * 利用者情報データを取得する
	 *
	 * @param request
	 * @throws ServletException
	 */
	public LoginUserData getLoginUserData(final HttpServletRequest request,
			final LoginSession login, final String loginId) throws ServletException {

		// 利用者情報Bean
		final LoginUserBean bean = new LoginUserBean(login.getUserID());

		Connection con = null;
		try {
			con = getConnectionPool(request);

			bean.setConnection(null, con);
			bean.setLoginId(loginId);
			bean.execute();

		} catch (final Exception e) {
			throw new ServletException(e);

		} finally {
			releaseConnectionPool(request, con);
		}

		if (bean.getLoginUserList().size() == 0) {
			throw new ServletException("利用者情報の取得に失敗しました。");
		}

		return (LoginUserData) bean.getLoginUserList().get(0);
	}

	/**
	 * フォーム値を利用者データに反映する
	 *
	 * @param data
	 * @param form
	 */
	protected void refrectFormValue(final LoginUserData data, final U002Form form) {

		// 利用者ID
		if (form.getNewLoginId() != null) {
			data.setLoginId(form.getNewLoginId());
		}

		// 利用者名
		data.setLoginName(form.getLoginName());

		// メンテ権限
		data.setMaintainer("1".equals(form.getAuthMa()));

		// Kei-Navi機能権限
		data.setKnFunctionFlag(Short.parseShort(form.getAuthKn()));

		// 2016/04/28 QQ)Hisakawa 大規模改修 UPD START
		// 校内成績機能権限
		// data.setScFunctionFlag(Short.parseShort(form.getAuthSc()));
		//
		// 入試結果調査機能権限
		// data.setEeFunctionFlag(Short.parseShort(form.getAuthEe()));


		// 校内成績機能権限
		data.setScFunctionFlag(Short.parseShort("1"));

		// 入試結果調査機能権限
		data.setEeFunctionFlag(Short.parseShort("1"));
		// 2016/04/28 QQ)Hisakawa 大規模改修 UPD END

		// 答案閲覧機能権限
		data.setAbFunctionFlag(Short.parseShort(form.getAuthAb()));
	}

}

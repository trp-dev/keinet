package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean;
import com.fjh.beans.DefaultBean;

/**
 * 
 * 判定結果登録Bean
 * 
 * 2004.9.21   [新規作成]
 * 
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 *
 * 2005.2.23	Totec) T.Yamada     [1]判定結果をDetailPageBeanに一本化する
 *
 *【2006年度対応】
 * 2006.5.12    Totec) T.Yamada     [2]配点比（学科＋その他）
 *
 */
public class JudgementStoreBean extends DefaultBean{

	private String insQuery = ""+
	"INSERT INTO DETERMHISTORY("+ 
	"INDIVIDUALID "+",DISPRANK "+",YEAR "+",EXAMCD "+
	",UNIVCD "+",FACULTYCD "+",DEPTCD "+
	",RATINGCVSSCORE "+",RATINGDEVIATION "+",RATINGPOINT "+",CENTERRATING "+",SECONDRATING "+",TOTALRATING "+
	",CENTALLOTPNTRATE "+",SECALLOTPNTRATE "+
	" ) VALUES (" +" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?" +")";
	
	private String delQuery = "DELETE FROM DETERMHISTORY WHERE INDIVIDUALID = ?";

	//In Parameter
	private String individualId;
	private String examYear;
	private String examCd;
	private List resultList;
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		PreparedStatement pstmt = null;
		PreparedStatement pstmt2 = null;
		ResultSet rs = null;
		try{
			//1/2 : 削除処理
			pstmt = conn.prepareStatement(delQuery);
			pstmt.setString(1, individualId);
			pstmt.execute();
			//2/2 : 挿入処理
			pstmt2 = conn.prepareStatement(insQuery);
			Iterator ite = resultList.iterator();
			int count = 1;
			while(ite.hasNext()){
				//[1] del start
//				JudgementBean jb = (JudgementBean)ite.next();
//				pstmt2.setString(1, individualId);
//				pstmt2.setString(2, Integer.toString(count));//表示順（いい順 ← 仕様）
//				pstmt2.setString(3, examYear);
//				pstmt2.setString(4, examCd);
//				pstmt2.setString(5, jb.university.univCd);
//				pstmt2.setString(6, jb.university.facultyCd);
//				pstmt2.setString(7, jb.university.deptCd);
//				pstmt2.setDouble(8, jb.c_score);//RATINGCVSSCORE
//				pstmt2.setDouble(9, jb.s_score);//RATINGDEVIATION
//				pstmt2.setDouble(10, jb.t_score);//RATINGPOINT
//				pstmt2.setString(11, getJudgeStrCenter(jb.c_judgement, jb.c_ghjudgement, jb));//CENTERRATING
//				pstmt2.setString(12, getJudgeStrSecond(jb.s_judgement, jb.s_ghjudgement, jb));//SECONDRATING
//				pstmt2.setString(13, getJudgeStrTotal(jb.t_judgement, jb.t_ghjudgement, jb));//TOTALRATING
//				pstmt2.setDouble(14, jb.university.c_maximumScale);
//				pstmt2.setDouble(15, jb.university.s_maximumScale);
				//[1] del end
				//[1] add start
				DetailPageBean jb = (DetailPageBean)ite.next();
				pstmt2.setString(1, individualId);
				pstmt2.setString(2, Integer.toString(count));//表示順（いい順 ← 仕様）
				pstmt2.setString(3, examYear);
				pstmt2.setString(4, examCd);
				pstmt2.setString(5, jb.getUnivCd());
				pstmt2.setString(6, jb.getFacultyCd());
				pstmt2.setString(7, jb.getDeptCd());
//				pstmt2.setDouble(8, jb.getC_scoreStr());//RATINGCVSSCORE
//				pstmt2.setDouble(9, jb.getS_scoreStr());//RATINGDEVIATION
//				pstmt2.setDouble(10, jb.getT_scoreStr());//RATINGPOINT
				pstmt2.setDouble(8, jb.getC_score());//RATINGCVSSCORE//TODO
				pstmt2.setDouble(9, jb.getS_score());//RATINGDEVIATION//TODO
				pstmt2.setDouble(10, jb.getT_score());//RATINGPOINT//TODO
				pstmt2.setString(11, jb.getCLetterJudgement());//CENTERRATING
				pstmt2.setString(12, jb.getSLetterJudgement());//SECONDRATING
				pstmt2.setString(13, jb.getTLetterJudgement());//TOTALRATING
//				pstmt2.setDouble(14, jb.getC_maximumScale());
//				pstmt2.setDouble(15, jb.getS_maximumScale());		
                
				//int 値を挿入したいのでgetDetailした後、配点比を取得  
				pstmt2.setDouble(14, jb.getDetail().getCAllotPntRateAll());
				pstmt2.setDouble(15, jb.getDetail().getSAllotPntRateAll());   
                
				//[1] add end
				pstmt2.execute();
				count ++;
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(pstmt2 != null) pstmt2.close();
			if(rs != null) rs.close();	
		}
	}
	
	//[1] del start
//	private String getJudgeStrCenter(int judge, int ghjudge, JudgementBean jb){
//		if(jb.university.c_maximumScale > 0){
//			return getJudgeStr(judge, ghjudge, jb);
//		}else{
//			return "*";	
//		}
//	}
//	private String getJudgeStrSecond(int judge, int ghjudge, JudgementBean jb){
//		if(jb.university.s_maximumScale > 0){
//			return getJudgeStr(judge, ghjudge, jb);
//		}else{
//			return "*";	
//		}
//	}
//	private String getJudgeStrTotal(int judge, int ghjudge, JudgementBean jb){
//		if(jb.university.c_maximumScale > 0 && jb.university.s_maximumScale > 0){
//			return getJudgeStr(judge, ghjudge, jb);
//		}else{
//			return "*";	
//		}
//	}	
	/**
	 * 
	 * @param judge
	 * @param ghjudge
	 * @param jb
	 * @return
	 */
//	private String getJudgeStr(int judge, int ghjudge, JudgementBean jb){
//		String cJudge = "*";
//		if(examCd.equals(IPropsLoader.getInstance().getMessage("CODE_EXAM_CENTERRESEARCH"))){
//			switch(judge){
//				case 0 :
//					cJudge = "*";
//					break;
//				case 1 :
//					cJudge = "▲";
//					break;
//				case 2 :
//					cJudge = "△";
//					break;
//				case 3 :
//					cJudge = "○";
//					break;
//				case 4 :
//					cJudge = "◎";
//					break;
//				default :
//					cJudge = "*";
//					break;
//			}
//		}else{
//			switch(judge){
//				case 0 :
//					cJudge = "*";
//					break;
//				case 1 :
//					cJudge = "E";
//					break;
//				case 2 :
//					cJudge = "D";
//					break;
//				case 3 :
//					cJudge = "C";
//					break;
//				case 4 :
//					cJudge = "B";
//					break;
//				case 5 :
//					cJudge = "A";
//					break;
//				default :
//					cJudge = "*";
//					break;
//			}
//		}
//		if(!cJudge.equals("*")){
//			switch(ghjudge){
//				case 1 :
//					cJudge += "H";
//					break;
//				case 2 :
//					cJudge += "G";
//					break;
//				default :
//					break;
//			}
//		}
//		return cJudge;
//	}
	//[1] del end
	
	/**
	 * @param string
	 */
	public void setExamCd(String string) {
		examCd = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

	/**
	 * @param list
	 */
	public void setResultList(List list) {
		resultList = list;
	}

}

package jp.co.fj.keinavi.excel.data.cls;

/**
 * クラス成績概況−個人成績推移教科別成績データクラス
 * 作成日: 2004/08/06
 * @author	A.Iwata
 *
 * 2009.11.24   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 */
public class C13KyokaDataListBean {

    //教科コード
    private String strKyokaCd = "";
    //教科名
    private String strKyokamei = "";
    //型・科目コード
    private String strKmkCd = "";
    //型・科目名
    private String strKmkmei = "";
    //今回全国偏差値
    private float floHensaAllNow = 0;
    //前回全国偏差値
    private float floHensaAllLast = 0;
    //前回差
    private float floDiffer = 0;
    //学力レベル
    private String strScholarLevel = "";
    //基礎科目フラグ
    private String basicFlg;

    /*----------*/
    /* Get      */
    /*----------*/

    public float getFloDiffer() {
        return this.floDiffer;
    }
    public float getFloHensaAllLast() {
        return this.floHensaAllLast;
    }
    public float getFloHensaAllNow() {
        return this.floHensaAllNow;
    }
    public String getStrKmkCd() {
        return this.strKmkCd;
    }
    public String getStrKmkmei() {
        return this.strKmkmei;
    }
    public String getStrKyokaCd() {
        return this.strKyokaCd;
    }
    public String getStrKyokamei() {
        return this.strKyokamei;
    }
    public String getStrScholarLevel() {
        return strScholarLevel;
    }
    public String getBasicFlg() {
        return basicFlg;
    }

    /*----------*/
    /* Set      */
    /*----------*/

    public void setFloDiffer(float floDiffer) {
        this.floDiffer = floDiffer;
    }
    public void setFloHensaAllLast(float floHensaAllLast) {
        this.floHensaAllLast = floHensaAllLast;
    }
    public void setFloHensaAllNow(float floHensaAllNow) {
        this.floHensaAllNow = floHensaAllNow;
    }
    public void setStrKmkCd(String strKmkCd) {
        this.strKmkCd = strKmkCd;
    }
    public void setStrKmkmei(String strKmkmei) {
        this.strKmkmei = strKmkmei;
    }
    public void setStrKyokaCd(String strKyokaCd) {
        this.strKyokaCd = strKyokaCd;
    }
    public void setStrKyokamei(String strKyokamei) {
        this.strKyokamei = strKyokamei;
    }
    public void setStrScholarLevel(String string) {
        this.strScholarLevel = string;
    }
    public void setBasicFlg(String basicFlg) {
        this.basicFlg = basicFlg;
    }

}

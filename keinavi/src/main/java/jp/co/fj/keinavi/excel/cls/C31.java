/**
 * クラス成績分析−クラス比較
 * 出力する帳票の判断
 * 作成日: 2004/07/26
 * @author	T.Sakai
 */
 
package jp.co.fj.keinavi.excel.cls;
import jp.co.fj.keinavi.util.log.*;

import java.util.*;

import jp.co.fj.keinavi.excel.data.cls.*;

public class C31 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean c31( C31Item c31Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {

		KNLog log = KNLog.getInstance(null,null,null);

		try{
			//C31Itemから各帳票を出力
			int ret = 0;
			if (( c31Item.getIntHyouFlg()==0 )&&( c31Item.getIntGraphFlg()==0 )) {
				throw new Exception("C31 ERROR : フラグ異常");
			}
			if ( c31Item.getIntHyouFlg()==1 ) {
				C31_01 exceledit = new C31_01();
				ret = exceledit.c31_01EditExcel( c31Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C31_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C31_01");
			}
			if ( c31Item.getIntGraphFlg()==1 ) {
				C31_02 exceledit = new C31_02();
				ret = exceledit.c31_02EditExcel( c31Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C31_02","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C31_02");
			}
			
		} catch(Exception e) {
			log.Err("99C31","帳票作成エラー",e.toString());
			return false;
		}
		return true;
	}

}
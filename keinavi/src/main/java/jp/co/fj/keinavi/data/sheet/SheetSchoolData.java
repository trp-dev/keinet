/*
 * 作成日: 2004/09/30
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.sheet;

import java.io.Serializable;

import jp.co.fj.keinavi.util.KNUtil;

/**
 * 帳票出力時に利用する学校データ
 * 
 * @author kawai
 */
public class SheetSchoolData implements Serializable, Comparable {

	// 全国の学校コード
	public static final String SCHOOL_CD_ALL = "99999";
	// 都道府県の学校コード下３桁
	public static final String SCHOOL_CD_PREF = "999";

	private String schoolCD; // 学校コード
	private int graphFlag; // グラフ表示
	private String prefCD; // 県コード

	/**
	 * コンストラクタ
	 */
	public SheetSchoolData() {
	}

	/**
	 * コンストラクタ
	 */
	public SheetSchoolData(String schoolCD) {
		this.schoolCD = schoolCD;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return this.getSchoolCD().equals(((SheetSchoolData) obj).getSchoolCD());
	}

	/* (非 Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		return this.getSchoolCD().compareTo(((SheetSchoolData) o).getSchoolCD());
	}

	/**
	 * 学校コードが全国・都道府県・高校のどれか
	 * @return
	 */
	public int getSchoolTypeCode() {
		if (SheetSchoolData.SCHOOL_CD_ALL.equals(this.getSchoolCD())) return 1;
		else if (SheetSchoolData.SCHOOL_CD_PREF.equals(this.getSchoolCD().substring(2, 5))) return 2;
		else return 3;
	}

	/**
	 * @return
	 */
	public String getPrefCD() {
		if (prefCD == null) {
			prefCD = KNUtil.bundleCD2PrefCD(this.getSchoolCD());
		}
		return prefCD;
	}

	/**
	 * @return
	 */
	public int getGraphFlag() {
		return graphFlag;
	}

	/**
	 * @return
	 */
	public String getSchoolCD() {
		return schoolCD;
	}

	/**
	 * @param i
	 */
	public void setGraphFlag(int i) {
		graphFlag = i;
	}

	/**
	 * @param string
	 */
	public void setSchoolCD(String string) {
		if (string == null || string.length() != 5) {
			throw new IllegalArgumentException("学校コードは５桁である必要があります。");
		}
		schoolCD = string;
	}

}

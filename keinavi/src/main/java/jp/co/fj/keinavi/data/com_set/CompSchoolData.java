/*
 * 作成日: 2004/07/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.com_set;

import java.io.Serializable;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CompSchoolData implements Serializable, Comparable {

	// 学校コード
	private String schoolCD = null;
	// グラフ表示
	private short graphDisp = 0;

	/* (非 Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		return this.getSchoolCD().compareTo(((CompSchoolData)o).getSchoolCD());
	}

	/**
	 * @return
	 */
	public short getGraphDisp() {
		return graphDisp;
	}

	/**
	 * @return
	 */
	public String getSchoolCD() {
		return schoolCD;
	}

	/**
	 * @param s
	 */
	public void setGraphDisp(short s) {
		graphDisp = s;
	}

	/**
	 * @param string
	 */
	public void setSchoolCD(String string) {
		schoolCD = string;
	}

}

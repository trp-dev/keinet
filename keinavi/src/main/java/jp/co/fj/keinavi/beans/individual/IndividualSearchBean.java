/*
 * 作成日: 2004/09/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.individual.StudentData;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultSearchBean;

/**
 * 
 * 2005.02.28 Yoshimoto KAWAI - Totec
 *            担当クラスの年度対応
 * 
 * @author Administrator
 * 
 */
public class IndividualSearchBean extends DefaultSearchBean {

	private String schoolCd;
	private String personIdsString;
	private String[][] classes;
	private String selectGrade; // 選択学年
	private String selectClass; // 選択クラス
	private String year; // 年度
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs = null;
		try{
			// 担当クラスを一時表へ入れる
			ps1 = conn.prepareStatement("INSERT INTO countchargeclass VALUES(?, ?, 0)");

			// 学年か選択クラスがNULLなら絞り込まない
			if (this.selectGrade == null || this.selectClass == null) {
				for (int i=0; classes != null && i<classes.length; i++) {
					ps1.setInt(1, Integer.parseInt(classes[i][0]));
					ps1.setString(2, classes[i][1]);
					ps1.executeUpdate();
				}
			
			// 全担当クラスなら特定の学年
			} else if ("all".equals(this.selectClass)) {
				for (int i=0; classes != null && i<classes.length; i++) {
					if (classes[i][0].equals(this.selectGrade)) {
						ps1.setInt(1, Integer.parseInt(classes[i][0]));
						ps1.setString(2, classes[i][1]);
						ps1.executeUpdate();
					}
				}
				
			// そうでなければ特定のクラス
			} else {
				String[] charge = this.selectClass.split(":", 2);
				ps1.setInt(1, Integer.parseInt(charge[0]));
				ps1.setString(2, charge[1]);
				ps1.executeUpdate();
			}
			
			ps1.close();
			
			// 個人を取得
			Query query = QueryLoader.getInstance().load("i01");

			// 個人IDがあるなら絞り込み
			if (this.personIdsString != null && !"".equals(this.personIdsString)) {
				query.append("WHERE individualid IN (#)");
				query.setStringArray(1, IUtilBean.deconcatComma(this.personIdsString));
			}

			ps2 = conn.prepareStatement(query.toString());
			
			/// 現在の年度
			//String current = KNUtil.getCurrentYear();
			
			ps2.setString(1, year); // 年度
			ps2.setString(2, schoolCd); // 学校コード
			ps2.setString(3, year); // 年度
			ps2.setString(4, schoolCd); // 学校コード
			
			rs = ps2.executeQuery();
			while(rs.next()){
				StudentData studentData = new StudentData();
				studentData.setStudentPersonId(rs.getString(1));
				studentData.setStudentYear(rs.getString(2));
				studentData.setStudentGrade(rs.getString(3));
				studentData.setStudentClass(rs.getString(4));
				studentData.setStudentClassNo(rs.getString(5));
				studentData.setStudentNameKana(rs.getString(6));
				studentData.setStudentNameKanji(rs.getString(7));
				studentData.setStudentSortNo(rs.getString(8));
				studentData.setStudentSex(rs.getString(9));
				recordSet.add(studentData);
			}
			
			conn.commit();
			
		} catch (Exception e) {
			conn.rollback();
			throw e;
			
		} finally {
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps2);
		}
	}

	/**
	 * @return
	 */
	public String getSchoolCd() {
		return schoolCd;
	}

	/**
	 * @param string
	 */
	public void setSchoolCd(String string) {
		schoolCd = string;
	}

	/**
	 * @return
	 */
	public String getPersonIdsString() {
		return personIdsString;
	}

	/**
	 * @param string
	 */
	public void setPersonIdsString(String string) {
		personIdsString = string;
	}

	/**
	 * @return
	 */
	public String[][] getClasses() {
		return classes;
	}

	/**
	 * @param strings
	 */
	public void setClasses(String[][] strings) {
		classes = strings;
	}

	/**
	 * @return
	 */
	public String getSelectClass() {
		return selectClass;
	}

	/**
	 * @return
	 */
	public String getSelectGrade() {
		return selectGrade;
	}

	/**
	 * @param string
	 */
	public void setSelectClass(String string) {
		selectClass = string;
	}

	/**
	 * @param string
	 */
	public void setSelectGrade(String string) {
		selectGrade = string;
	}

	/**
	 * @return year を戻す。
	 */
	public String getYear() {
		return this.year;
	}
	/**
	 * @param year year を設定する。
	 */
	public void setYear(String year) {
		this.year = year;
	}
}

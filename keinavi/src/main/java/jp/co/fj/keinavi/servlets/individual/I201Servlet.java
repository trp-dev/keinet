/*
 *
 * 
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I201Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.servlets.KNServletException;

public class I201Servlet extends IndividualServlet {

	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		
		super.execute(request, response);
		
		//フォームからのデータを取得 ::::::::
		I201Form form = null;
		try {
			form = (I201Form)super.getActionForm(request, "jp.co.fj.keinavi.forms.individual.I201Form");
		} catch (Exception e) {
			KNServletException k = new KNServletException("0I201にてフォームデータの収得に失敗しました。" + e);
			k.setErrorCode("00I201010101");
			throw k;
		}
		
		//大学データが未ロードならエラーページに遷移する
		if(super.getUnivAll() == null || super.getUnivAll().size() == 0){
			request.setAttribute("form", form);
			super.forward(request, response, JSP_ONLOAD_UNIV);
		}
		//大学データロードが完了している
		else{
				
			HttpSession session = request.getSession();
			//個人共通
			ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);
			
			//プロファイル情報
			Profile profile = (Profile) session.getAttribute(Profile.SESSION_KEY);
			
			//判定対象生徒
			Short JudgeStudent = (Short) profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);
			
			try{	
					if ("i201".equals(getForward(request))) {
						
						//生徒を変えたときはプロファイルをセットしない
						//モードを切り替えたときは対象外
						if (!"i201".equals(getBackward(request))) {
							//判定対象生徒セット
							if(iCommonMap.isBunsekiMode()){	
								form.setJudgeStudent(JudgeStudent.toString());
							}
						}
						
						// JSP転送				
						iCommonMap.setTargetPersonId(form.getTargetPersonId());
						request.setAttribute("form", form);
						super.forward(request, response, JSP_I201);
						
					} else {
					// 不明ならServlet転送
					//判定対象生徒保存
					if("i204".equals(getForward(request)) && iCommonMap.isBunsekiMode()){
						profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(form.getJudgeStudent()));
					}
						iCommonMap.setTargetPersonId(form.getTargetPersonId());
						super.forward(request, response, SERVLET_DISPATCHER);
					}
			   }catch(Exception e){
			   		e.printStackTrace();
					KNServletException k = new KNServletException("0I201にてエラーが発生しました。" + e);
					k.setErrorCode("00I201010101");
					throw k;
			   }
		}
	}
}	


/**
 * 校内成績分析−高校間比較
 * 出力する帳票の判断
 * 作成日: 2004/08/02
 * @author	Ito.Y
 */
 
package jp.co.fj.keinavi.excel.business;

import java.util.*;

import jp.co.fj.keinavi.excel.data.business.*;
import jp.co.fj.keinavi.util.log.*;

public class B41 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean b41(B41Item b41Item, ArrayList outfilelist, int intSaveFlg, String UserID, KNSheetLog sheetLog) {
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		try{
			int ret = 0;
			//B41Itemから各帳票を出力
			if ( b41Item.getIntHyouFlg()==0 && b41Item.getIntRankFlg()==0 ) {
				throw new Exception("B41 ERROR : フラグ異常");
			}
			if ( b41Item.getIntHyouFlg()==1 && b41Item.getIntPastYearFlg()==1 ) {
				B41_01 exceledit = new B41_01();
				ret = exceledit.b41_01EditExcel( b41Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"B41_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("B41_01");
			}
			if ( b41Item.getIntHyouFlg()==1 && b41Item.getIntPastYearFlg()==2 ) {
				B41_02 exceledit = new B41_02();
				ret = exceledit.b41_02EditExcel( b41Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"B41_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("B41_02");
			}
//			if ( b41Item.getIntRankFlg()==1 && b41Item.getIntPastYearFlg()==2 ) {
			if ( b41Item.getIntRankFlg()==1 && b41Item.getIntPastYearFlg()==1 ) {
				B41_03 exceledit = new B41_03();
				ret = exceledit.b41_03EditExcel( b41Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"B41_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("B41_03");
			}
			if ( b41Item.getIntRankFlg()==1 && b41Item.getIntPastYearFlg()==2 ) {
				B41_04 exceledit = new B41_04();
				ret = exceledit.b41_04EditExcel( b41Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"B41_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("B41_04");
			}
			
		} catch(Exception e) {
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
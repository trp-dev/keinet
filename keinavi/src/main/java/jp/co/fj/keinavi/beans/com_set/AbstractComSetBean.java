/*
 * 作成日: 2004/08/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.com_set;

import java.util.ArrayList;
import java.util.List;

import com.fjh.beans.DefaultBean;

/**
 * 共通項目設定用の抽象Bean
 * 
 * @author kawai
 */
abstract public class AbstractComSetBean extends DefaultBean {

	protected List fullList = new ArrayList(); // 全てのリスト
	protected List partList = null; // 部分的なリスト

	/**
	 * 全てのデータ
	 * @return
	 */
	public List getFullList() {
		return fullList;
	}

	/**
	 * 部分的なデータ
	 * 具体的なフィルタリングはサブクラスに任せる
	 * @return
	 */
	abstract public List getPartList();

	/**
	 * 全てのデータの要素数
	 * @return
	 */
	public int getFullListSize() {
		return fullList.size();
	}
	
	/**
	 * 部分的なデータの要素数
	 * @return
	 */
	public int getPartListSize() {
		return this.getPartList().size();
	}

	/**
	 * 全てのデータの要素数の半値
	 * @return
	 */
	public int getFullListHalfSize() {
		return Math.round((float)this.getFullListSize() / 2);
	}

	/**
	 * 部分的なデータの要素数の半値
	 * @return
	 */
	public int getPartListHalfSize() {
		return Math.round((float)this.getPartListSize() / 2);
	}
}

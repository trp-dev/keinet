/*
 * 作成日: 2004/07/28
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.school.factory;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import jp.co.fj.keinavi.data.com_set.ComSubjectData;
import jp.co.fj.keinavi.forms.school.SMaxForm;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 *
 * 2005.04.05	Yoshimoto KAWAI - Totec
 * 				s205とs305の「過年度の表示」対応
 *
 * 2005.04.11	Yoshimoto KAWAI - Totec
 * 				日程・評価区分対応
 *
 * 2009.10.20 Fujito URAKAWA - Totec
 *            新校内資料「科目・設問別正答率（レベル別）」対応
 *
 * @author 奥村ゆかり
 *
 */
public class CommonFormFactory {

	SMaxForm form; // アクションフォーム
	Map item; // アイテムマップ

	List format = new LinkedList(); // フォーマット
	List options = new LinkedList(); // オプション

	/**
	 * コンストラクタ
	 * @param form
	 * @param item
	 */
	public CommonFormFactory(SMaxForm form, Map item) {
		this.form = form;
		this.item = item;
	}

	/**
	 * 型・共通項目設定利用をフォームにセットする
	 */
	public void setForm0103() {
		// 型・共通項目設定利用
		form.setCommonType(item.get(IProfileItem.TYPE_USAGE).toString());

		// 型（個別設定）
		ComSubjectData data = (ComSubjectData)item.get(IProfileItem.TYPE_IND);

		if (data != null) {
			form.setAnalyzeType(ProfileUtil.getSubjectIndValue(data, false));
			form.setGraphType(ProfileUtil.getSubjectIndValue(data, true));
		}
	}


	/**
	 * 科目・共通設定利用をフォームにセットする
	 */
	public void setForm0203() {
		// 科目・共通設定利用
		form.setCommonCourse(item.get(IProfileItem.COURSE_USAGE).toString());

		// 科目（個別設定）
		ComSubjectData data = (ComSubjectData)item.get(IProfileItem.COURSE_IND);

		if (data != null) {
			form.setAnalyzeCourse(ProfileUtil.getSubjectIndValue(data, false));
			form.setGraphCourse(ProfileUtil.getSubjectIndValue(data, true));
		}
	}

	/**
	 *
	 * 表をフォームにセットする
	 *
	 * 0:未選択　1:選択
	 *
	 * @return void
	 *
	 */
	public void setForm0601() {
		if (item.containsKey("0601")) {
			if (((Short) item.get("0601")).shortValue() == 1) {
				format.add("Chart");
			}
			// 初期値
		} else {
			format.add("Chart");
			// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 ADD START
			form.setCompRatio("12");
			// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 ADD END
		}
	}


	/**
	 *
	 * 表をフォームにセットする
	 *
	 * 0:表なし 1:表なし（構成比表示あり） 2:表なし（構成比表示なし）
	 * 11:表有り（構成比表示あり）        12:表有り（構成比表示なし）
	 *
	 * @return void
	 *
	 */
	public void setForm0602() {
		if (item.containsKey("0602")) {
			short value = ((Short) item.get("0602")).shortValue();
			switch (value) {
				case 1 :
					//format.add("Chart");
					break;
				case 2 :
					//format.add("Chart");
					break;
				case 11 :
					format.add("Chart");
					break;
				case 12 :
					format.add("Chart");
					break;
			}
			form.setCompRatio(Short.toString(value));
			// 初期値
		} else {
			format.add("Chart");
			form.setCompRatio("12");
		}
	}

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//	// 2019/07/17 QQ)Tanioka 共通テスト対応 ADD START
//	/**
//	 *
//	 * 共通テスト英語認定試験CEFR取得状況をフォームにセットする
//	 *
//	 * 0:チェックなし
//	 * 1:チェック有り
//	 *
//	 * @return void
//	 *
//	 */
//	public void setForm1601() {
//		if (item.containsKey("1601")) {
//			if (((Short) item.get("1601")).shortValue() == 1) {
//				format.add("Option");
//			}
//		}
//	}
//
//	/**
//	 *
//	 * 共通テスト英語認定試験CEFR取得状況をフォームにセットする
//	 *
//	 * 1:チェックなし(テーブル内チェックなし)  11:チェック有り(テーブル内チェックなし)
//	 * 2:チェックなし(テーブル内チェック有り)  12:チェック有り(テーブル内チェック有り)
//	 *
//	 * @return void
//	 *
//	 */
//	public void setForm1602() {
//		// 共通テスト英語認定試験CEFR取得状況
//		if (item.containsKey("1602")) {
//			short value = ((Short) item.get("1602")).shortValue();
//			switch (value) {
//				case 1 :
//					//format.add("Option");
//					break;
//				case 2 :
//					//format.add("Option");
//					break;
//				case 11 :
//					options.add("OptionCheckBox");
//					break;
//				case 12 :
//					options.add("OptionCheckBox");
//					break;
//			}
//			form.setTargetCheckBox(Short.toString(value));
//			// 初期値
//		} else {
//			item.put(IProfileItem.OPTION_CHECK_BOX, new Short("1"));
//			form.setTargetCheckBox("1");
//		}
//	}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

	/**
	 *
	 * 国語 評価別人数をフォームにセットする
	 *
	 * 0:未選択　1:選択
	 *
	 * @return void
	 *
	 */
	public void setForm1701() {
		// 国語 評価別人数
		if (item.containsKey("1701")) {
			if (((Short) item.get("1701")).shortValue() == 1) {
				options.add("targetCheckKokugo");
			}
		}
	}

	/**
	 *
	 * 小設問別正答状況をフォームにセットする
	 *
	 * 0:未選択　1:選択
	 *
	 * @return void
	 *
	 */
	public void setForm1702() {
		// 小設問別正答状況
		if (item.containsKey("1702")) {
			if (((Short) item.get("1702")).shortValue() == 1) {
				options.add("targetCheckStatus");
			}
		}
	}

	/**
	 *
	 * 小設問別成績をフォームにセットする
	 *
	 * 0:未選択　1:選択
	 *
	 * @return void
	 *
	 */
	public void setForm1703() {
		// 小設問別成績
		if (item.containsKey("1703")) {
			if (((Short) item.get("1703")).shortValue() == 1) {
				options.add("targetCheckRecord");
			}
		}
	}
	// 2019/07/17 QQ)Tanioka 共通テスト対応 ADD END

	/**
	 *
	 * グラフをフォームにセットする
	 *
	 * 0:グラフなし
	 * 1:グラフ有り
	 *
	 * @return void
	 *
	 */
	public void setForm0701() {
		if (item.containsKey("0701")) {
			if (((Short) item.get("0701")).shortValue() == 1) {
				format.add("Graph");
			}
		}
	}


	/**
	 *
	 * 偏差値帯別構成比グラフをフォームにセットする
	 *
	 * 0:グラフなし  1:グラフなし(2.5)　 2:グラフなし(5)　 3:グラフなし(10.0)
	 * 1:グラフ有り 11:グラフ有り(2.5)　12:グラフ有り(5)　13:グラフ有り(10.0)
	 *
	 * @return void
	 *
	 */
	public void setForm0702() {
		// 偏差値帯別構成比グラフ
		if (item.containsKey("0702")) {
			short value = ((Short) item.get("0702")).shortValue();
			switch (value) {
				case 1 :
					//format.add("Chart");
					break;
				case 2 :
					//format.add("Chart");
					break;
				case 11 :
					options.add("GraphCompRatio");
					break;
				case 12 :
					options.add("GraphCompRatio");
					break;
				case 13 :
					options.add("GraphCompRatio");
					break;
			}
			form.setPitchCompRatio(Short.toString(value));
			// 初期値
		} else {
			//options.add("GraphCompRatio");
			form.setPitchCompRatio("1");
		}
	}


	/**
	 *
	 * 偏差値帯別度数分布グラフをフォームにセットする
	 *
	 * 0:グラフなし  1:グラフなし(人数)　 2:グラフなし(構成比)　
	 * 1:グラフ有り 11:グラフ有り(人数)　12:グラフ有り(構成比)　
	 *
	 * @return void
	 *
	 */
	public void setForm0703() {
		// 偏差値帯別度数分布グラフ
		if (item.containsKey("0703")) {
			short value = ((Short) item.get("0703")).shortValue();
			switch (value) {
				case 1 :
					//format.add("Chart");
					break;
				case 2 :
					//format.add("Chart");
					break;
				case 11 :
					format.add("GraphDist");
					break;
				case 12 :
					format.add("GraphDist");
					break;
			}
			form.setAxis(Short.toString(value));
			// 初期値
		} else {
			//format.add("GraphDist");
			form.setAxis("1");
		}
	}


	/**
	 *
	 * 偏差値帯別人数積み上げグラフをフォームにセットする
	 *
	 * 0:グラフなし  1:グラフなし(2.5)　 2:グラフなし(5)　 3:グラフなし(10.0)
	 * 1:グラフ有り 11:グラフ有り(2.5)　12:グラフ有り(5)　13:グラフ有り(10.0)
	 *
	 * @return void
	 *
	 */
	public void setForm0705() {
		// 偏差値帯別人数積み上げグラフ
		if (item.containsKey("0705")) {
			short value = ((Short) item.get("0705")).shortValue();
			switch (value) {
				case 1 :
					//format.add("Chart");
					break;
				case 2 :
					//format.add("Chart");
					break;
				case 11 :
					options.add("GraphBuildup");
					break;
				case 12 :
					options.add("GraphBuildup");
					break;
				case 13 :
					options.add("GraphBuildup");
					break;
			}
			form.setPitchBuildup(Short.toString(value));
			// 初期値
		} else {
			//options.add("GraphBuildup");
			form.setPitchBuildup("1");
		}
	}



	/**
	 *
	 * 大学集計区分をフォームにセットする
	 *
	 * 1:大学(日程有り)　 2:大学(日程なし)　 3:学部(日程有り)　4:学部(日程なし)
	 *
	 * @return void
	 *
	 */
	public void setForm0309() {
		if (item.containsKey("0309")) {
			form.setDispUnivCount(((Short) item.get("0309")).toString());
			//初期値
		} else {
			form.setDispUnivCount("3");
		}
	}


	/**
	 *
	 * 大学の表示順をフォームにセットする
	 *
	 * 1:選択順　 2:コード順　 3:志望者数順
	 *
	 * @return void
	 *
	 */
	public void setForm0310() {
		if (item.containsKey("0310")) {
			form.setDispUnivOrder(((Short) item.get("0310")).toString());
			//初期値
		} else {
			form.setDispUnivOrder("1");
		}
	}

	/**
	 * 表示対象（志望大学）をフォームにセットする
	 * 現役のみ(1) 高卒のみ(2) 現役＋高卒(3)
	 */
	public void setForm0311() {
		if (item.containsKey("0311")) {
			form.setDispUnivStudent((String[]) item.get("0311"));
		} else {
			form.setDispUnivStudent(new String[]{"1"});
		}
	}

	/**
	 * 日程をフォームにセットする
	 * 前期日程のみ(1)
	 * ※簡易処理・種類が増えたらちゃんと書かないとだめ
	 */
	public void setForm0312() {
		if (item.get("0312") == null) {
			form.setDispUnivSchedule(null);
		} else {
			form.setDispUnivSchedule("1");
		}
	}

	/**
	 * 評価区分をフォームにセットする
	 */
	public void setForm0313() {
		form.setDispUnivRating((String[]) item.get("0313"));
	}

	/**
	 *
	 *  高校の表示順序をセットする
	 *
	 * 1:選択順　 2:コード順
	 *
	 * @return void
	 *
	 */
	public void setForm0402() {

		if (item.containsKey("0402")) {
			form.setDispSchoolOrder(((Short) item.get("0402")).toString());
			// 初期値
		} else {
			form.setDispSchoolOrder("1");

		}
	}


	/**
	 *
	 * クラスの表示順序にセットする
	 *
	 * 1:選択順　 2:コード順
	 *
	 * @return void
	 *
	 */
	public void setForm0502() {
		if (item.containsKey("0502")) {
			form.setDispClassOrder(((Short) item.get("0502")).toString());
			//初期値
		} else {
			form.setDispClassOrder("1");
		}
	}


	/**
	 *
	 *  過年度の表示をセットする
	 *
	 * 1:今年度のみ　 2:前年度まで　3:前々年度まで
	 *
	 * @return void
	 *
	 */
	public void setForm1102() {

		if (item.containsKey("1102")) {
			form.setDispPast(((Short) item.get("1102")).toString());
			// 初期値
		} else {
			form.setDispPast("1");
		}
	}

	/**
	 * 過年度の表示をセットする
	 *
	 * @return void
	 */
	public void setForm1102_2() {
		form.setDispUnivYear((String[]) item.get("1102"));
	}

	/**
	 *
	 *  過回・過年度をセットする
	 *
	 * 0:未選択　1:選択
	 *
	 * @return void
	 *
	 */
	public void setForm1201() {

		if (item.containsKey("1201")) {
			if (((Short) item.get("1201")).shortValue() == 1) {
				options.add("CompPrev");
			}
		} else {
			//options.add("CompPrev");
		}
	}


	/**
	 *
	 *  過回・クラス比較をセットする
	 *
	 * 0:未選択　1:選択
	 *
	 * @return void
	 *
	 */
	public void setForm1202() {

		if (item.containsKey("1202")) {
			if (((Short) item.get("1202")).shortValue() == 1) {
				options.add("CompClass");
			}
		} else {
			//options.add("CompClass");
		}
	}


	/**
	 *
	 *  過回・他校比較をセットする
	 *
	 * 0:未選択　1:選択
	 *
	 * @return void
	 *
	 */
	public void setForm1203() {

		if (item.containsKey("1203")) {
			if (((Short) item.get("1203")).shortValue() == 1) {
				options.add("CompOther");
			}
		} else {
			//options.add("CompOther");
		}
	}


	/**
	 *
	 * セキュリティスタンプをフォームにセットする
	 *
	 *  1:機密  2:部内限り 3:校内限り  4:取扱注意  5:なし
	 * 11:機密 12:部外秘  13:塾外秘  14:取扱注意
	 *
	 * @return void
	 *
	 */
	public void setForm1305() {
		setForm1305(false);
	}


	public void setForm1305(boolean isSales) {
		// セキュリティスタンプ
		if (item.containsKey("1305")) {
			form.setStamp(((Short) item.get("1305")).toString());
			// 初期値
		} else {
			if(!isSales)form.setStamp("4");
			else form.setStamp("14");
		}
	}


	/**
	 *
	 * マーク模試正答状況をフォームにセットする
	 *
	 * 0:未選択　1:選択
	 *
	 * @return void
	 *
	 */
	public void setForm1504() {
		// マーク模試正答状況
		if (item.containsKey("1504")) {
			if (((Short) item.get("1504")).shortValue() == 1) {
				options.add("MarkExam");
			}
		}
	}

	/**
	 *
	 * マーク模試高校別設問成績層正答率をフォームにセットする
	 *
	 * 0:未選択　1:選択
	 *
	 * @return void
	 */
	public void setForm1516() {
		// マーク模試高校別設問成績層正答率
		if (item.containsKey("1516")) {
			if (((Short) item.get("1516")).shortValue() == 1) {
				options.add("MarkExamArea");
			}
		}
	}


	/**
	 *
	 *  型・科目別成績順位をセットする
	 *
	 * 0:未選択　1:選択
	 *
	 * @return void
	 *
	 */
	public void setForm1501() {

		if (item.containsKey("1501")) {
			if (((Short) item.get("1501")).shortValue() == 1) {
				options.add("RankOrder");
			}
		} else {
			//options.add("RankOrder");
		}
	}

	/**
	 *
	 *  全体平均表示をセットする
	 *
	 * 1:あり　2:なし
	 *
	 * @return void
	 *
	 */
	public void setForm1511() {

		if (item.containsKey("1511")) {
			form.setDispTotalAvg(((Short) item.get("1511")).toString());
			// 初期値
		} else {
			form.setDispTotalAvg("1");
		}
	}


	/**
	 *
	 *  現役全体平均表示をセットする
	 *
	 * 1:あり　2:なし
	 *
	 * @return void
	 *
	 */
	public void setForm1512() {

		if (item.containsKey("1512")) {
			form.setDispActiveAvg(((Short) item.get("1512")).toString());
			// 初期値
		} else {
			form.setDispActiveAvg("1");
		}
	}


	/**
	 *
	 *  高卒全体平均表示をセットする
	 *
	 * 1:あり　2:なし
	 *
	 * @return void
	 *
	 */
	public void setForm1513() {

		if (item.containsKey("1513")) {
			form.setDispGradAvg(((Short) item.get("1513")).toString());
			// 初期値
		} else {
			form.setDispGradAvg("1");
		}
	}


	/**
	 *
	 *  表示高校数をセットする
	 *
	 * 1:50校　2:100校　3:150校　4:200校　5:250校　6:300校
	 *
	 * @return void
	 *
	 */
	public void setForm1514() {

		if (item.containsKey("1514")) {
			form.setDispSchoolNum(((Short) item.get("1514")).toString());
			// 初期値
		} else {
			form.setDispSchoolNum("1");
		}
	}

	/**
	 *
	 *  上位校の*印表示をセットする
	 *
	 * 0:なし　1:5校まで　2:10校まで
	 *
	 * @return void
	 *
	 */
	public void setForm1515() {

		if (item.containsKey("1515")) {
			form.setDispUpperMark(((Short) item.get("1515")).toString());
			// 初期値
		} else {
			form.setDispUpperMark("2");
		}
	}

	/**
	 * フォーマットをフォームにセットする
	 */
	public void setFormat() {
		form.setFormatItem((String[]) format.toArray(new String[0]));
	}


	/**
	 * オプションをフォームにセットする
	 */
	public void setOption() {
		form.setOptionItem((String[]) options.toArray(new String[0]));
	}

	/**
	 * @return
	 */
	public SMaxForm getForm() {
		return form;
	}

	/**
	 * @param form
	 */
	public void setForm(SMaxForm form) {
		this.form = form;
	}

}

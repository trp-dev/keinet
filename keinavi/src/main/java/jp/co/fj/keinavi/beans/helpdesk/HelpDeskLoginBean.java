/*
 * 作成日: 2004/10/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.helpdesk;

import java.security.cert.X509Certificate;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.login.Certification;
import jp.co.fj.keinavi.data.login.PactSchoolData;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.date.ShortDateUtil;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;


/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class HelpDeskLoginBean extends DefaultBean {

	private LoginSession loginSession = new LoginSession(); // ログインセッション
	private X509Certificate[] cert; 	// 証明書オブジェクト
	private String account; 			// ＩＤ
	private String password; 			// パスワード

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// TODO 自動生成されたメソッド・スタブ

		String today = new ShortDateUtil().date2String();
		// 契約校マスタを取得する
		PactSchoolData pact = new PactSchoolData(); // 契約校データ
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			ps1 = conn.prepareStatement(QueryLoader.getInstance().load("w04").toString());
			ps1.setString(1, account);
			rs1 = ps1.executeQuery();

			// データを詰める
			if (rs1.next()) {
				pact.setSchoolCD(rs1.getString(1));
				pact.setPactDiv(rs1.getShort(2));
				pact.setCertCN(rs1.getString(3));
				pact.setCertOU(rs1.getString(4));
				pact.setCertO(rs1.getString(5));
				pact.setCertL(rs1.getString(6));
				pact.setCertST(rs1.getString(7));
				pact.setCertC(rs1.getString(8));
				pact.setCertUserDiv(rs1.getShort(9));
				pact.setPasswd(rs1.getString(10));
				pact.setFeatprovMode(rs1.getShort(11));
				pact.setValidDate(rs1.getString(12));
				pact.setCertFlag(rs1.getString(13));
				pact.setPactTerm(rs1.getString(14));

			// 無ければ例外
			} else {
				KNServletException e =
					new KNServletException("契約校データの取得に失敗しました。");
				e.setErrorCode("1");
				e.setErrorMessage("ＩＤまたはパスワードが違います。");
				throw e;
			}
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(ps1);
		}

		// 契約区分のチェック
		if (pact.getPactDiv() != 5) {
			KNServletException e =
				new KNServletException("契約区分が異なります。");
			e.setErrorCode("1");
			e.setErrorMessage("ＩＤまたはパスワードが違います。");
			throw e;
		}

		// 証明書ユーザ区分のチェック
		if (pact.getCertUserDiv() != 6) {
			KNServletException e =
				new KNServletException("証明書ユーザ区分が異なります。");
			e.setErrorCode("1");
			e.setErrorMessage("ＩＤまたはパスワードが違います。");
			throw e;
		}

		// 証明書有効フラグのチェック
		if (!"1".equals(pact.getCertFlag())) {
			KNServletException e =
				new KNServletException("証明書有効フラグが無効です。");
			e.setErrorCode("1");
			e.setErrorMessage("ＩＤまたはパスワードが違います。");
			throw e;
		}

		// 契約期限のチェック
		if (today.compareTo(pact.getPactTerm()) > 0) {
			KNServletException e =
				new KNServletException("契約期限を過ぎています。");
			e.setErrorCode("1");
			e.setErrorMessage("ＩＤまたはパスワードが違います。");
			throw e;
		}

		// 証明書を認証する
		if (cert == null || cert.length == 0) {
		} else {
			Certification c = new Certification(cert[0]);

			if (!pact.equals(c)) {
				System.out.println("DB:   " + pact.toString());
				System.out.println("Cert: " + c.toString());

				KNServletException e = new KNServletException("証明書の認証に失敗しました。");
				e.setErrorCode("1");
				throw e;
			}
			// 証明書あり
			loginSession.setCert(true);
		}

		// パスワードチェック
		if (!pact.getPasswd().equals(password)) {
			KNServletException e =
				new KNServletException("ＩＤまたはパスワードが違います。");
			e.setErrorCode("1");
			throw e;
		}

		// ログインセッションを作る
		//   下記項目はヘルプデスクメニュー選択画面でセット
		//      userID(利用者ID)
		//      userMode(利用者モード)
		//      sectorSortingCD(部門分類コード)
		//      userName(利用者名称)
		loginSession.setLoginID(this.account);
		loginSession.setUserID(pact.getSchoolCD());
		loginSession.setPactDiv(pact.getPactDiv());
		loginSession.setUserDiv(pact.getCertUserDiv());
		loginSession.setFeatprovMode(pact.getFeatprovMode());
		loginSession.setDbKey(KNCommonProperty.getNDBSID());
		loginSession.setUserName("ヘルプデスク");
	}


	/**
	 * @return
	 */
	public LoginSession getLoginSession() {
		return loginSession;
	}

	/**
	 * @param session
	 */
	public void setLoginSession(LoginSession session) {
		loginSession = session;
	}

	/**
	 * @return
	 */
	public X509Certificate[] getCert() {
		return cert;
	}

	/**
	 * @param certificates
	 */
	public void setCert(X509Certificate[] certificates) {
		cert = certificates;
	}

	/**
	 * @return
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @param string
	 */
	public void setAccount(String string) {
		account = string;
	}

	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param string
	 */
	public void setPassword(String string) {
		password = string;
	}

}

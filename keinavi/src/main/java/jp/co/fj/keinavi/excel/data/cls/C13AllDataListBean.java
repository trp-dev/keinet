package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−個人成績推移総合成績データクラス
 * 作成日: 2004/07/14
 * @author	H.Fujimoto
 */
public class C13AllDataListBean {
	//学年
	private String strGrade = "";
	//クラス
	private String strClass = "";
	//クラス番号
	private String strClassNo = "";
	//氏名
	private String strKanaName = "";
	//性別
	private String strSex = "";
	//教科別成績データリスト
	private ArrayList c13KyokaDataList = new ArrayList();
	
	/*----------*/
	/* Get      */
	/*----------*/
	public ArrayList getC13KyokaDataList() {
		return this.c13KyokaDataList;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrClassNo() {
		return this.strClassNo;
	}
	public String getStrGrade() {
		return this.strGrade;
	}
	public String getStrKanaName() {
		return this.strKanaName;
	}
	public String getStrSex() {
		return this.strSex;
	}
	
	/*----------*/
	/* Set      */
	/*----------*/
	
	public void setC13KyokaDataList(ArrayList c13KyokaDataList) {
		this.c13KyokaDataList = c13KyokaDataList;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrClassNo(String strClassNo) {
		this.strClassNo = strClassNo;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}
	public void setStrKanaName(String strKanaName) {
		this.strKanaName = strKanaName;
	}
	public void setStrSex(String strSex) {
		this.strSex = strSex;
	}

}

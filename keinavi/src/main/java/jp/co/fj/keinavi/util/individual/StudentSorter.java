/*
 * 作成日: 2004/10/20
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.individual;

import java.util.Comparator;

import jp.co.fj.keinavi.data.individual.StudentData;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class StudentSorter {
	
	public class SortByGradeClass implements Comparator{
		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {
			StudentData e1 = (StudentData) o1;
			StudentData e2 = (StudentData) o2;
		
			int grade1 = Integer.parseInt(e1.getStudentGrade().trim());
			int grade2 = Integer.parseInt(e2.getStudentGrade().trim());
		
			if(grade1 != 5 && grade1 != 4){
				if (Integer.parseInt(e1.getStudentGrade().trim()) > Integer.parseInt(e2.getStudentGrade().trim())) {
					return -1;
				} else if (Integer.parseInt(e1.getStudentGrade().trim()) < Integer.parseInt(e2.getStudentGrade().trim())) {
					return 1;
				} else {
					if(e1.getStudentClass().compareTo(e2.getStudentClass()) < 0){
						return -1;
					}else if(e1.getStudentClass().compareTo(e2.getStudentClass()) > 0){
						return 1;
					}else{
						return 0;
					}
				}
			}else{
				if (Integer.parseInt(e1.getStudentGrade().trim()) < Integer.parseInt(e2.getStudentGrade().trim())) {
					return -1;
				} else if (Integer.parseInt(e1.getStudentGrade().trim()) >Integer.parseInt(e2.getStudentGrade().trim())) {
					return 1;
				} else {
					if(e1.getStudentClass().compareTo(e2.getStudentClass()) < 0){
						return -1;
					}else if(e1.getStudentClass().compareTo(e2.getStudentClass()) > 0){
						return 1;
					}else{
						return 0;
					}
				}
			}
		}
	}
	
}

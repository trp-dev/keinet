package jp.co.fj.keinavi.servlets.individual;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.StudentData;

import com.fjh.db.DBManager;

/**
 *
 * 判定処理スレッドの基本クラス
 * 
 * 2008.02.13	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public abstract class BaseJudgeThread implements Runnable {

	// ★重要ポイント
	// 帳票作成中に画面から志望大学判定を実行可能なため、
	// 画面と帳票で処理スレッドの状態を独立して保持できるように考慮する
	
	// ステータス保持セッションキー
	// 画面
	private static final String STATUS_KEY_SCREEN = "ScreenJudgeThreadStatus";
	// 帳票
	private static final String STATUS_KEY_SHEET = "SheetJudgeThreadStatus";

	// 判定結果保持セッションキー
	// 画面
	private static final String JUDGEDMAP_KEY_SCREEN = "ScreenJudgedMap";
	// 帳票
	private static final String JUDGEDMAP_KEY_SHEET = "SheetJudgedMap";
	
	// ステータスコード
	// 処理中
	public static final String STATUS_PROG = "0001";
	// 正常終了
	public static final String STATUS_DONE = "0002";
	// エラー
	public static final String STATUS_ERR = "1001";

	// HTTPセッション
	private final HttpSession session;
	// DBキー
	private final String dbKey;
	// このインスタンス内でのステータス保持セッションキー
	private final String statusKey;
	// このインスタンス内での判定結果保持セッションキー
	private final String judgedMapKey;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pSession HTTPセッション
	 * @param pDbKey コネクション識別キー
	 * @param isSheetMode モードフラグ（帳票or画面）
	 */
	public BaseJudgeThread(final HttpSession pSession,
			final String pDbKey, final boolean isSheetMode) {

		session = pSession;
		dbKey = pDbKey;
		
		// 帳票モード
		if (isSheetMode) {
			statusKey = STATUS_KEY_SHEET;
			judgedMapKey = JUDGEDMAP_KEY_SHEET;
		// 画面モード	
		} else {
			statusKey = STATUS_KEY_SCREEN;
			judgedMapKey = JUDGEDMAP_KEY_SCREEN;
		}
		
		// 判定結果クリア
		session.setAttribute(judgedMapKey, null);
		// 処理中に移行
		session.setAttribute(statusKey, STATUS_PROG);
	}
	
	/**
	 * 帳票モードのコンストラクタ
	 * 
	 * @param pSession  HTTPセッション
	 * @param pDbKey コネクション識別キー
	 */
	public BaseJudgeThread(final HttpSession pSession,
			final String pDbKey) {
		this(pSession, pDbKey, true);
	}
	
	/**
	 * @see java.lang.Runnable#run()
	 */
	public final void run() {
		
		Map judgedMap;
		Connection con = null;
		try {
			con = getConnectionPool();
			con.setAutoCommit(false);
			judgedMap = execute(con);
		} catch (final Exception e) {
			e.printStackTrace();
			synchronized (session) {
				session.setAttribute(statusKey, STATUS_ERR);
			}
			return;
		} finally {
			releaseConnectionPool(con);
		}
		
		// 判定結果を保持する
		synchronized (session) {
			session.setAttribute(judgedMapKey, judgedMap);
			session.setAttribute(statusKey, STATUS_DONE);
		}
	}

	/**
	 * 判定処理を実装する
	 * 
	 * @param con DBコネクション
	 */
	abstract protected Map execute(Connection con) throws Exception;

	private Connection getConnectionPool() throws Exception {
		return DBManager.getConnectionPool(dbKey);
	}
	
	private void releaseConnectionPool(final Connection con) {
		try {
			if (con != null) {
				DBManager.releaseConnectionPool(dbKey, con);
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param iCommonMap 個人成績分析共通Map
	 * @param judgeStudent 判定対象生徒範囲
	 * @return 処理対象の生徒データ
	 */
	protected List getSelectedIndividuals(final ICommonMap iCommonMap,
			final String judgeStudent) {

		// 分析モードで表示中の生徒ひとり
		if (iCommonMap.isBunsekiMode() && "2".equals(judgeStudent)) {
			final List list = new ArrayList(1);
			list.add(iCommonMap.getTargetPerson());
			return list;
		// 面談モード or 分析モードで全員
		} else {
			return iCommonMap.getSelectedIndividuals();
		}
	}
	
	/**
	 * @param iCommonMap 個人成績分析共通Map
	 * @param judgeStudent 判定対象生徒範囲
	 * @return 処理対象の個人ID
	 */
	protected String[] getSelectedIndividualIds(final ICommonMap iCommonMap,
			final String judgeStudent) {
		
		final List list = new ArrayList();

		final List individuals = getSelectedIndividuals(iCommonMap, judgeStudent);

		for (Iterator ite = individuals.iterator(); ite.hasNext();) {
			StudentData data = (StudentData) ite.next();
			list.add(data.getStudentPersonId());
		}
		
		return (String[]) list.toArray(new String[list.size()]);
	}

	/**
	 * @param pSession HTTPセッション
	 * @return 画面の判定処理スレッドの状態
	 */
	public static String getScreenStatus(final HttpSession pSession) {
		synchronized (pSession) {
			return (String) pSession.getAttribute(STATUS_KEY_SCREEN);
		}
	}
	
	/**
	 * @param pSession HTTPセッション
	 * @return 帳票の判定処理スレッドの状態
	 */
	public static String getSheetStatus(final HttpSession pSession) {
		synchronized (pSession) {
			return (String) pSession.getAttribute(STATUS_KEY_SHEET);
		}
	}
	
	/**
	 * @param pSession HTTPセッション
	 * @return 画面の判定結果
	 */
	public static Map getScreenJudgedMap(final HttpSession pSession) {
		return (Map) pSession.getAttribute(JUDGEDMAP_KEY_SCREEN);
	}
	
	/**
	 * @param pSession HTTPセッション
	 * @return 帳票の判定結果
	 */
	public static Map getSheetJudgedMap(final HttpSession pSession) {
		return (Map) pSession.getAttribute(JUDGEDMAP_KEY_SHEET);
	}
	
}

package jp.co.fj.keinavi.beans.graph;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * グラフ出力対象チェックの基本クラス
 * 
 * 2006.07.27	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class BaseGraphCheckerBean extends GraphCheckerBeanAdapter {

	/**
	 * コンストラクタ
	 * 
	 * @param id プロファイルカテゴリID
	 */
	public BaseGraphCheckerBean(final String id) {
		super(id);
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.GraphTargetBeanAdapter#checkTypeConfig(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	public final int checkTypeConfig(
			final HttpServletRequest request, final boolean isBundle) {

		// 一括出力でグラフ帳票なし
		if (isBundle && !hasSubjectGraphSheet(request)){
			return 0;
		}
		
		// 共通項目設定利用または個別出力
		if (isValidValue(request, TYPE_USAGE) || !isBundle) {
			return checkCommonSubject(request, 1);
		// 個別設定利用
		} else {
			return checkIndSubject(request, 1);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.GraphTargetBeanAdapter#checkCourseConfig(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	public final int checkCourseConfig(
			final HttpServletRequest request, final boolean isBundle) {
		
		// 一括出力でグラフ帳票なし
		if (isBundle && !hasSubjectGraphSheet(request)){
			return 0;
		}
		
		// 共通項目設定利用または個別出力
		if (isValidValue(request, COURSE_USAGE) || !isBundle) {
			return checkCommonSubject(request, 2);
		// 個別設定利用
		} else {
			return checkIndSubject(request, 2);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.GraphCheckerBeanAdapter#checkClassConfig(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	public final int checkClassConfig(
			final HttpServletRequest request, final boolean isBundle) {

		// 一括出力でグラフ帳票なし
		if (isBundle && !hasClassGraphSheet(request)){
			return 0;
		}
		
		return checkCommonClass(request);
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.GraphCheckerBeanAdapter#checkSchoolConfig(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	public final int checkSchoolConfig(
			final HttpServletRequest request, final boolean isBundle) {

		// 一括出力でグラフ帳票なし
		if (isBundle && !hasSchoolGraphSheet(request)){
			return 0;
		}
		
		return checkCommonSchool(request);
	}
	
}

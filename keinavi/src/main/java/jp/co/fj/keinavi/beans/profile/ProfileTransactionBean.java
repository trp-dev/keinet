/*
 * 作成日: 2004/07/02
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.profile;


import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.fjh.beans.DefaultBean;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ProfileTransactionBean extends DefaultBean {

	// プロファイルID
	private String profileID = null;
	// モード
	private String mode = null;

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		PreparedStatement ps = conn.prepareStatement(
			"UPDATE profile SET updngflg = ? WHERE profileid = ?");
		ps.setString(1, mode);
		ps.setString(2, profileID);
		ps.execute();
		ps.close();
	}

	/**
	 * @return
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @return
	 */
	public String getProfileID() {
		return profileID;
	}

	/**
	 * @param string
	 */
	public void setMode(String string) {
		mode = string;
	}

	/**
	 * @param string
	 */
	public void setProfileID(String string) {
		profileID = string;
	}

}

package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 学校内で個人成績が存在する模試のリストを保持するBean
 * 
 * 2006.09.28	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class ScoreExamListBean extends DefaultBean {

	// 表示最大数
	private static final int DISP_MAX = 9;
	
	// 学校コード
	private final String schoolCd;
	// ヘルプデスクかどうか
	private final boolean isHelpDesk;
	// 模試リスト
	private final List examList = new ArrayList();
	// 表示模試リスト
	private final List dispExamList = new ArrayList();
	// 表示模試マップ
	// key ... 模試ID
	// value ... Boolean(true)
	private final Map dispExamMap = new HashMap();
	
	// 表示対象フラグ
	// 模試受験者
	private boolean isExaminee = false;
	// 仮受験者扱い
	private boolean isTemporary = false;
	// 未受験者
	private boolean isNotTake = false;
	
	// ページ番号
	private int pageNo = 1;
	
	/**
	 * @param pSchoolCd 学校コード
	 * @param pIsHelpDesk ヘルプデスクかどうか
	 */
	public ScoreExamListBean(final String pSchoolCd,
			final boolean pIsHelpDesk) {
		schoolCd = pSchoolCd;
		isHelpDesk = pIsHelpDesk;
	}
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final Query query = QueryLoader.getInstance().load("m20");
			
			// ヘルプデスクなら内部データ開放日
			if (isHelpDesk) {
				query.replaceAll("out_dataopendate", "in_dataopendate");
			}
			
			ps = conn.prepareStatement(query.toString());
			ps.setString(1, schoolCd);
			ps.setString(2, schoolCd);
			rs = ps.executeQuery();
			while (rs.next()) {
				final ExamData data = new ExamData();
				data.setExamYear(rs.getString(1));
				data.setExamCD(rs.getString(2));
				data.setExamName(rs.getString(3));
				examList.add(data);
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
		
		// 表示模試リスト初期化
		initDispExamList();
	}
	
	/**
	 * @return examList を戻します。
	 */
	public List getExamList() {
		return examList;
	}

	/**
	 * @return dispExamMap を戻します。
	 */
	public Map getDispExamMap() {
		return dispExamMap;
	}

	/**
	 * @return dispExamList を戻します。
	 */
	public List getDispExamList() {
		if (pageNo * DISP_MAX > dispExamList.size()) {
			return dispExamList.subList(
					(pageNo - 1) * DISP_MAX, dispExamList.size());
		} else {
			return dispExamList.subList(
					(pageNo - 1) * DISP_MAX, pageNo * DISP_MAX);
		}
	}
	
	/**
	 * @return dispExamListのサイズ を戻します。
	 */
	public int getDispExamListSize() {
		return getDispExamList().size();
	}
	
	/**
	 * @return 最初のページかどうか
	 */
	public boolean isFirstPage() {
		return pageNo == 1;
	}

	/**
	 * @return 最後のページかどうか
	 */
	public boolean isLastPage() {
		return pageNo * DISP_MAX >= dispExamList.size();
	}
	
	/**
	 * @param pPageNo 設定する pageNo。
	 */
	public void setPageNo(final int pPageNo) {
		pageNo = pPageNo;
	}
	
	/**
	 * @param target 表示対象模試IDの配列
	 */
	public void setTargetExam(final String[] target) {
		
		dispExamMap.clear();
		
		// マップに保持する
		if (target != null) {
			for (int i = 0; i < target.length; i++) {
				dispExamMap.put(target[i], Boolean.TRUE);
			}
		}
		
		// 表示模試リスト初期化
		initDispExamList();
	}
	
	// 表示模試リストの初期化
	private void initDispExamList() {
		
		dispExamList.clear();

		// 対象模試が選択されていなければ全て
		if (dispExamMap.isEmpty()) {
			dispExamList.addAll(examList);
		// 選択されているならそれだけ詰める
		} else {
			for (Iterator ite = examList.iterator(); ite.hasNext();) {
				final ExamData data = (ExamData) ite.next();
				if (dispExamMap.containsKey(data.getExamId())) {
					dispExamList.add(data);
				}
			}
		}
	}

	/**
	 * @return isExaminee を戻します。
	 */
	public boolean isExaminee() {
		return isExaminee;
	}

	/**
	 * @param pIsExaminee 設定する isExaminee。
	 */
	public void setExaminee(boolean pIsExaminee) {
		isExaminee = pIsExaminee;
	}

	/**
	 * @return isNotTake を戻します。
	 */
	public boolean isNotTake() {
		return isNotTake;
	}

	/**
	 * @param pIsNotTake 設定する isNotTake。
	 */
	public void setNotTake(boolean pIsNotTake) {
		isNotTake = pIsNotTake;
	}

	/**
	 * @return isTemporary を戻します。
	 */
	public boolean isTemporary() {
		return isTemporary;
	}

	/**
	 * @param pIsTemporary 設定する isTemporary。
	 */
	public void setTemporary(boolean pIsTemporary) {
		isTemporary = pIsTemporary;
	}

}

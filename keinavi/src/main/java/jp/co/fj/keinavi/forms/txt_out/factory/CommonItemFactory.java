/*
 * 作成日: 2004/07/28
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.txt_out.factory;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import jp.co.fj.keinavi.forms.txt_out.TMaxForm;
import jp.co.fj.keinavi.util.CollectionUtil;
/**
 * @author 奥村ゆかり
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CommonItemFactory {

	TMaxForm form; // アクションフォーム
	Map item; // アイテムマップ
	Set outTarget; // 出力対象項目


	/**
	 * @param form
	 * @param item
	 */
	public CommonItemFactory(TMaxForm form, Map item) {
		this.form = form;
		this.item = item;
		this.outTarget = CollectionUtil.array2Set(form.getOutTarget());
	}

	// ファイル出力形式の保存
	public void setTxtType() {
		item.put("1402", new Short(form.getTxtType()));
	}
	
	// 出力項目の保存
	public void setOutTarget() {
		String str ="";
		Iterator it = outTarget.iterator();
		int size = outTarget.size();
		int count = 1;
		while(it.hasNext()){
			str += it.next();
			if(count!=size) str+=",";
			count++;
		}
		
		item.put("1403",str);
	}
	
}

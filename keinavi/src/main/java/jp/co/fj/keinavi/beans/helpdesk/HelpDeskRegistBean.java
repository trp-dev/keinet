package jp.co.fj.keinavi.beans.helpdesk;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.beans.maintenance.MainteLogBean;
import jp.co.fj.keinavi.data.helpdesk.RegistError;
import jp.co.fj.keinavi.forms.helpdesk.HD103Form;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 2005.10.13	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 * 2006.01.11	Yoshimoto KAWAI - TOTEC
 * 				校内成績処理システム対応
 *
 * 2006.02.14	Yoshimoto KAWAI - TOTEC
 * 				校内成績処理システムSTEP2対応
 *
 * @author nino
 *
 */
public class HelpDeskRegistBean extends DefaultBean {

	private LoginId4DLBean loginId4DLBean; // DLサービス校の利用者情報登録Bean

	private int     lineNo      = 0;		// 行番号
	private boolean judgeAll   = true;	// 判定

	// Kei-Navi連携様CSVファイルフォーマットF
	private String	schoolCode	 =null;	// 学校コード
	private String	userId		 =null;	// ユーザID
	private String	firstPass	 =null;	// 初期パスワード
	private String	userKubun	 =null;	// ユーザ区分
	private String	contractKubun=null;	// 契約区分
	private String	contractTerm =null;	// 契約期限
	private String	validityTerm =null;	// 証明書有効期限
	private String	sessionNumber=null;	// 最大セッション数
	private String	postFunction =null;	// 私書箱機能提供フラグ
	private String	rgdate		 =null;	// 登録日
	private String	update		 =null;	// 更新日

	// 一括登録エラーリスト
	private List registError     = new ArrayList();
	private int registUser      = 0; 		// 登録されたユーザ数
	private int updateUser      = 0; 		// 更新されたユーザ数
	private int deleteUser      = 0; 		// 削除されたユーザ数

	// 一括登録リスト
	private List registList      = new ArrayList();

	// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START
	// ログ出力モジュール
	private final MainteLogBean log = new MainteLogBean("HELP");
	// 実行日付のチェック
	// ラジオボタン
	private String	content		= null;	// 処理の内容
	// 処理の内容SQL
	private StringBuffer contractSb = null;

	// 処理判定用
	private boolean isDeleteContract = false;	// 契約更新データ削除
	private boolean isMainteLog      = false;	// ログ出力
	// 処理結果
	private boolean isDateCheckErr = false;		// 実行日付のチェック結果
	// エラー通知パターン
	//  0:hd101 - 証明書用TSV作成
	//  1:hd102 - 契約校マスタ登録・更新
	//  2:hd102 - 契約更新データ登録
	private String errPattern      = null;
	// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		try {

			// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START
			// 契約更新データの削除
			if(this.isDeleteContract) {
				DeleteContractUpdate(this.conn);
				return;
			}

			// ログ出力
			if(this.isMainteLog) {
				log.setConnection(null, conn);
				// 一括更新
				if ("0".equals(content)) {
					log.etcLog("ユーザ一括更新：契約校マスタを" + this.registUser + "件登録");
					log.etcLog("ユーザ一括更新：契約校マスタを" + this.updateUser + "件更新");
				} else {
					log.etcLog("契約更新ファイル登録【年次】：契約更新ファイルを" + this.registUser + "件登録");
				}
				return;
			}
			// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END


			// パラメタジャッジ
			judgeAll =
				isParamFormat(
					lineNo,
					schoolCode,
					userId,
					firstPass,
					userKubun,
					contractKubun,
					contractTerm,
					validityTerm,
					sessionNumber,
					postFunction,
					rgdate,
					update   );

			if (judgeAll) {
				// keys[ 0]学校コード     [ 1]ユーザID         [ 2]初期パスワード
				// keys[ 3]ユーザ区分     [ 4]契約区分         [ 5]契約期限
				// keys[ 6]証明書有効期限 [ 7]最大セッション数 [ 8]私書箱機能提供
				// keys[ 9]登録日         [10]更新日
				String keys[] = new String[11];
				keys[ 0] = FormatChecker.str2Char32(schoolCode, 5);
				keys[ 1] = FormatChecker.str2Char32(userId, 8);
				keys[ 2] = firstPass;
				keys[ 3] = userKubun;
				keys[ 4] = FormatChecker.str2Char32(contractKubun, 1);
				keys[ 5] = contractTerm;
				keys[ 6] = validityTerm;
				keys[ 7] = sessionNumber;
				keys[ 8] = FormatChecker.str2Char32(postFunction, 1);
				keys[ 9] = rgdate;
				keys[10] = update;
				String[] pact = SelectPactSchool(conn, keys);
				pact[0] = FormatChecker.str2Char32(pact[0], 8);

				// 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD START

				/*
				// 既に契約マスタに存在する場合
				if (pact[0] != null) {
					if (UpdatePactSchool(conn, keys)) {
						updateUser++;		// 更新されたユーザ数をカウント
					} else {
						setErrorList(lineNo, schoolCode, userId, "契約マスタの更新に失敗しました。");
						judgeAll = false;
					}
					// 契約区分変更の場合は利用者情報削除
					// 契約校→DLサービス校
					// DLサービス校→契約校
					if ((isPactDiv(contractKubun) && isDLDiv(pact[1]))
							|| (isDLDiv(contractKubun) && isPactDiv(pact[1]))) {
						deleteLoginIdManage();
					}
				} else {
					if (InsertPactSchool(conn, keys)) {
						registUser++;		// 登録されたユーザ数をカウント
					} else {
						setErrorList(lineNo, schoolCode, userId, "契約マスタの登録に失敗しました。");
						judgeAll = false;
					}
				}

				// 契約校であるなら各種マスタレコードを作る
				if (isPactDiv(contractKubun)) {
					// 管理者の利用者情報
					insertManagerRecord();
					// 属性
					insertAttributeRecord();
					// 属性項目
					insertAttributeItemRecord();
					// 試験種類
					insertExamType();
					// 科目
					insertSubject();
				}

				// DLサービス校であるなら利用者情報を作る
				if (isDLDiv(contractKubun)) {
					insertLoginIdManage4DL();
				}
				*/

				// 年次登録
				if ("1".equals(content)) {
					if (pact[0] == null) {
						setErrorList(lineNo, schoolCode, userId, "契約マスタに存在しません。");
						judgeAll = false;
					}

					// 正常の場合
					if (judgeAll) {
						// 契約更新データの登録
						if (InsertContractUpdate(conn, keys)) {
							registUser++;
						} else {
							setErrorList(lineNo, schoolCode, userId, "契約更新データの登録に失敗しました。");
							judgeAll = false;
						}

					}

				// ユーザ更新
				} else {
					// 既に契約マスタに存在する場合
					if (pact[0] != null) {
						if (UpdatePactSchool(conn, keys)) {
							updateUser++;		// 更新されたユーザ数をカウント
						} else {
							setErrorList(lineNo, schoolCode, userId, "契約マスタの更新に失敗しました。");
							judgeAll = false;
						}
						// 契約区分変更の場合は利用者情報削除
						// 契約校→DLサービス校
						// DLサービス校→契約校
						if ((isPactDiv(contractKubun) && isDLDiv(pact[1]))
								|| (isDLDiv(contractKubun) && isPactDiv(pact[1]))) {
							deleteLoginIdManage();
						}
					} else {
						if (InsertPactSchool(conn, keys)) {
							registUser++;		// 登録されたユーザ数をカウント
						} else {
							setErrorList(lineNo, schoolCode, userId, "契約マスタの登録に失敗しました。");
							judgeAll = false;
						}
					}

					// 契約校であるなら各種マスタレコードを作る
					if (isPactDiv(contractKubun)) {
						// 管理者の利用者情報
						insertManagerRecord();
						// 属性
						insertAttributeRecord();
						// 属性項目
						insertAttributeItemRecord();
						// 試験種類
						insertExamType();
						// 科目
						insertSubject();
					}

					// DLサービス校であるなら利用者情報を作る
					if (isDLDiv(contractKubun)) {
						insertLoginIdManage4DL();
					}
				}
				// 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD END
			}

			// [2004/09/30] 削除処理は必要ない（登録・変更のみ）
			// 登録済リストから該当ユーザIDを消し込む（削除対象リスト）
			//RegistListUserClear(userId);

		} finally{
			this.setJudge(judgeAll);
		}

	}

	// 区分が契約校であるか
	private boolean isPactDiv(final String div) {
		return "1".equals(div);
	}

	// 区分がDLサービス校であるか
	private boolean isDLDiv(final String div) {
		return "2".equals(div) || "3".equals(div) || "4".equals(div);
	}

	// DLサービス校の利用者情報を登録
	private void insertLoginIdManage4DL() throws Exception {

		if (loginId4DLBean == null) {
			loginId4DLBean = new LoginId4DLBean();
			loginId4DLBean.setConnection(null, conn);
		}
		loginId4DLBean.setSchoolCd(schoolCode);
		loginId4DLBean.setDefaultPassword(firstPass);
		loginId4DLBean.execute();
	}

	// 利用者情報を削除
	private void deleteLoginIdManage() throws Exception {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("hd08"));

			// 学校コード
			ps.setString(1, schoolCode);

			ps.executeUpdate();

		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 管理者の利用者データを登録する
	 *
	 * @throws SQLException
	 */
	private void insertManagerRecord() throws SQLException {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("hd01").toString());

			// 学校コード
			ps.setString(1, schoolCode);
			// ログインIDは固定
			ps.setString(2, "admin");
			// ログインパスワード
			ps.setString(3, firstPass);
			// デフォルトパスワード
			ps.setString(4, firstPass);
			// 学校コード
			ps.setString(5, schoolCode);

			ps.executeUpdate();

		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 学校の属性データを登録する
	 *
	 * @throws SQLException
	 */
	private void insertAttributeRecord() throws SQLException {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("hd02").toString());

			// 学校コード
			ps.setString(1, schoolCode);
			// 学校コード
			ps.setString(2, schoolCode);

			ps.executeUpdate();

		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 学校の属性項目データを登録する
	 *
	 * @throws SQLException
	 */
	private void insertAttributeItemRecord() throws SQLException {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("hd03").toString());

			// 学校コード
			ps.setString(1, schoolCode);
			// 学校コード
			ps.setString(2, schoolCode);

			ps.executeUpdate();

		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	// 試験種類マスタを登録する
	private void insertExamType() throws SQLException {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().load("hd04").toString());
			// 学校コード
			ps.setString(1, schoolCode);
			ps.setString(2, schoolCode);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	// 科目マスタを登録する
	private void insertSubject() throws SQLException {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().load("hd05").toString());
			// 学校コード
			ps.setString(1, schoolCode);
			ps.setString(2, schoolCode);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 *
	 * 契約校マスタに存在するかどうか
	 *
	 * @param con     DBコネクション
	 * @param keys[]  keys[1]:ユーザID
	 * @return count
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static String[] SelectPactSchool(Connection con, String keys[])
		throws SQLException {

		String[] value = new String[2];

		PreparedStatement stmt = null;
		ResultSet rs = null;
		String query =
			"SELECT USERID, PACTDIV "
				+ "  FROM  PACTSCHOOL  "
				+ " WHERE  USERID = ?  " ;
		try {
			stmt = con.prepareStatement(query);
			stmt.setString(1, keys[1]);

			rs = stmt.executeQuery();
			while (rs.next()) {
				value[0] = rs.getString("USERID");
				value[1] = rs.getString("PACTDIV");
			}
		} catch (SQLException ex) {
			throw new SQLException("契約校マスタの取得に失敗しました。");
		} catch (Exception ex) {
			throw new SQLException("契約校マスタの取得に失敗しました。");
		} finally {
			DbUtils.closeQuietly(null, stmt, rs);
		}
		return value;
	}

	/**
	 *
	 * 契約校マスタに新規追加する
	 *
	 * @param con     DBコネクション
	 * @param keys[]  keys[0]〜[10]
	 * @return count
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static boolean InsertPactSchool(Connection con, String keys[])
		throws SQLException {
		boolean judge = false;
		int count = 0;
		PreparedStatement stmt = null;
		String query = null;

		query =
			"INSERT INTO PACTSCHOOL     "
				+ " (                   "
				+ "   USERID            "	// [ 1]ユーザID
				+ "  ,SCHOOLCD          "	// [ 0]学校コード
				+ "  ,PACTDIV           "	// [ 4]契約区分
				+ "  ,MAXSESSION        "	// [ 7]最大セッション数
				+ "  ,REALTIMESESSION   "
				+ "  ,CERT_CN           "
				+ "  ,CERT_OU           "
				+ "  ,CERT_O            "
				+ "  ,CERT_L            "
				+ "  ,CERT_S            "
				+ "  ,CERT_C            "
				+ "  ,CERT_BEFOREDATA   "	// [ 6]証明書有効期限
				+ "  ,CERT_USERDIV      "	// [ 3]ユーザ区分
				+ "  ,CERT_FLG          "
				+ "  ,ISSUED_BEFOREDATE "
				+ "  ,PACTTERM          "   // [ 5]契約期限
				+ "  ,REGISTRYDATE      "	// [ 9]登録日
				+ "  ,RENEWALDATE       "	// [10]更新日
				+ "  ,LOGINPWD          "
				+ "  ,DEFAULTPWD        "	// [ 2]初期パスワード
				+ "  ,FEATPROVFLG       "	// [ 8]私書箱機能提供フラグ
				+ "  ,KO_MAXSESSION  　 "	// [ 7]校内成績最大セッション数
				+ "  ,KO_REALTIMESESSION"
				+ " ) VALUES (          "
				+ "     ?,    ?,    ?,    ?, null, "
				+ "  null, null, null, null, null, "
				+ "  null,    ?,    ?, null, null, "
				+ "     ?,    ?,    ?,    ?,    ?, "
				+ "     ?,    ?,    null           "
				+ " ) ";

		try {
			stmt = con.prepareStatement(query);
			stmt.setString( 1, keys[ 1]);
			stmt.setString( 2, keys[ 0]);
			stmt.setString( 3, keys[ 4]);
			stmt.setString( 4, keys[ 7]);
			stmt.setString( 5, keys[ 6]);
			stmt.setString( 6, keys[ 3]);
			stmt.setString( 7, keys[ 5]);
			stmt.setString( 8, keys[ 9]);
			stmt.setString( 9, keys[10]);
			stmt.setString(10, keys[ 2]);
			stmt.setString(11, keys[ 2]);
			stmt.setString(12, keys[ 8]);
			stmt.setString(13, keys[ 7]);

			count = stmt.executeUpdate();

			if (count == 0)
				throw new SQLException("契約校マスタのINSERTに失敗しました。");

			judge = true;
		} catch (SQLException ex) {
			throw new SQLException(
					"契約校マスタのINSERTに失敗しました。" + ex.getMessage());
		} finally {
			if (stmt != null)
				stmt.close();
		}
		return judge;
	}

	/**
	 *
	 * 契約校マスタを更新する
	 *
	 * @param con     DBコネクション
	 * @param keys[]  keys[0]〜[10]
	 * @return count
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static boolean UpdatePactSchool(Connection con, String keys[])
		throws SQLException {
		boolean judge = false;
		int count = 0;
		PreparedStatement stmt = null;
		String query = null;

		query =
			"UPDATE PACTSCHOOL SET       "
				+ "   USERID          =? "		// [ 1]ユーザID
				+ "  ,SCHOOLCD        =? "		// [ 0]学校コード
				+ "  ,PACTDIV         =? "		// [ 4]契約区分
				+ "  ,MAXSESSION      =? "		// [ 7]最大セッション数
				+ "  ,CERT_BEFOREDATA =? "		// [ 6]証明書有効期限
				+ "  ,CERT_USERDIV    =? "		// [ 3]ユーザ区分
				+ "  ,PACTTERM        =? " 		// [ 5]契約期限
				+ "  ,REGISTRYDATE    =? "		// [ 9]登録日
				+ "  ,RENEWALDATE     =? "		// [10]更新日
				+ "  ,DEFAULTPWD      =? "		// [ 2]初期パスワード
				+ "  ,FEATPROVFLG     =? "		// [ 8]私書箱機能提供フラグ
				+ "  ,KO_MAXSESSION   =? "		// [ 7]校内成績最大セッション数
				+ " WHERE  USERID     =? ";		// [ 1]ユーザID （キー）

		try {
			stmt = con.prepareStatement(query);
			stmt.setString( 1, keys[ 1]);
			stmt.setString( 2, keys[ 0]);
			stmt.setString( 3, keys[ 4]);
			stmt.setString( 4, keys[ 7]);
			stmt.setString( 5, keys[ 6]);
			stmt.setString( 6, keys[ 3]);
			stmt.setString( 7, keys[ 5]);
			stmt.setString( 8, keys[ 9]);
			stmt.setString( 9, keys[10]);
			stmt.setString(10, keys[ 2]);
			stmt.setString(11, keys[ 8]);
			stmt.setString(12, keys[ 7]);
			stmt.setString(13, keys[ 1]);

			count = stmt.executeUpdate();

			if (count == 0)
				throw new SQLException("契約校マスタのUPDATEに失敗しました。");

			judge = true;
		} catch (SQLException ex) {
			throw new SQLException("契約校マスタのUPDATEに失敗しました。");
		} finally {
			if (stmt != null)
				stmt.close();
		}
		return judge;
	}

	/**
	 *
	 * 契約校マスタから削除する
	 *
	 * @param con     DBコネクション
	 * @param userId  ユーザID
	 * @return count
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static boolean DeletePactSchool(Connection con, String userID)
		throws SQLException {

		boolean judge = false;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String query =
			"DELETE  FROM  PACTSCHOOL  "
				+ " WHERE  USERID = ?  " ;
		try {
			stmt = con.prepareStatement(query);
			stmt.setString(1, userID);

			rs = stmt.executeQuery();

			judge = true;
		} catch (SQLException ex) {
			throw new SQLException("契約校マスタの削除に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return judge;
	}

	// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START
	/**
	 *
	 * システム日付を取得する
	 *
	 * @param con     DBコネクション
	 * @return count
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private String SelSysDate(Connection con)
		throws SQLException {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		String sysDate = "";

		try {
			sb.append("SELECT TO_CHAR(SYSDATE, 'YYYYMMDD') FROM DUAL");
			stmt = con.prepareStatement(sb.toString());

			rs = stmt.executeQuery();
			while (rs.next()) {
				sysDate = rs.getString(1);
			}

		} catch (SQLException ex) {
			throw new SQLException("システム日付の取得に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return sysDate;
	}

	/**
	 *
	 * 契約更新データを削除する
	 *
	 * @param con     DBコネクション
	 * @return 成功 or 失敗
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private boolean DeleteContractUpdate(Connection con)
		throws SQLException {

		boolean judge = false;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();

		try {
			sb.append("DELETE FROM CONTRACT_UPDATE");
			stmt = con.prepareStatement(sb.toString());

			rs = stmt.executeQuery();

			judge = true;
		} catch (SQLException ex) {
			throw new SQLException("契約更新データの削除に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return judge;
	}

	/**
	 *
	 * 契約更新データに新規追加する
	 *
	 * @param con     DBコネクション
	 * @param keys[]  keys[0]〜[10]
	 * @return 成功 or 失敗
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private boolean InsertContractUpdate(Connection con, String keys[])
		throws SQLException {
		boolean judge = false;
		int count = 0;
		PreparedStatement stmt = null;

		if (contractSb == null) {
			contractSb = new StringBuffer();
			contractSb.append("INSERT INTO CONTRACT_UPDATE");
			contractSb.append(" (");
			contractSb.append("    SCHOOLCD");
			contractSb.append("  , USERID");
			contractSb.append("  , DEFAULTPWD");
			contractSb.append("  , USERDIV");
			contractSb.append("  , PACTDIV");
			contractSb.append("  , PACTTERM");
			contractSb.append("  , CERT_BEFOREDATA");
			contractSb.append("  , MAXSESSION");
			contractSb.append("  , FEATPROVFLG");
			contractSb.append("  , REGISTRYDATE");
			contractSb.append("  , RENEWALDATE");
			contractSb.append("  , CERT_OU");
			contractSb.append("  , CERT_O");
			contractSb.append("  , CERT_L");
			contractSb.append("  , CERT_S");
			contractSb.append("  , CERT_C");
			contractSb.append(" ) VALUES (");
			contractSb.append("    ?");		// 学校コード
			contractSb.append("  , ?");		// ユーザID
			contractSb.append("  , ?");		// 初期パスワード
			contractSb.append("  , ?");		// ユーザ区分
			contractSb.append("  , ?");		// 契約区分
			contractSb.append("  , ?");		// 契約期限
			contractSb.append("  , ?");		// 証明書有効期限
			contractSb.append("  , ?");		// 最大セッション数
			contractSb.append("  , ?");		// 私書箱機能提供フラグ
			contractSb.append("  , ?");		// 登録日
			contractSb.append("  , ?");		// 更新日
			contractSb.append("  , '" + Certificates.getInstance().getOu()  + "'");		// 証明書情報OU
			contractSb.append("  , '" + Certificates.getInstance().getO()   + "'");		// 証明書情報O
			contractSb.append("  , '" + Certificates.getInstance().getL()   + "'");		// 証明書情報L
			contractSb.append("  , '" + Certificates.getInstance().getSt()  + "'");		// 証明書情報S
			contractSb.append("  , '" + Certificates.getInstance().getC()   + "'");		// 証明書情報C
			contractSb.append(" )");
		}

		try {
			stmt = con.prepareStatement(contractSb.toString());
			stmt.setString( 1, keys[ 0]);
			stmt.setString( 2, keys[ 1]);
			stmt.setString( 3, keys[ 2]);
			stmt.setString( 4, keys[ 3]);
			stmt.setString( 5, keys[ 4]);
			stmt.setString( 6, keys[ 5]);
			stmt.setString( 7, keys[ 6]);
			stmt.setString( 8, keys[ 7]);
			stmt.setString( 9, keys[ 8]);
			stmt.setString(10, keys[ 9]);
			stmt.setString(11, keys[10]);

			count = stmt.executeUpdate();

			if (count == 0)
				throw new SQLException("契約更新データのINSERTに失敗しました。");

			judge = true;
		} catch (SQLException ex) {
			throw new SQLException(
					"契約更新データのINSERTに失敗しました。" + ex.getMessage());
		} finally {
			if (stmt != null)
				stmt.close();
		}
		return judge;
	}
	// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END

	/**
	 *
	 * パラメタ判定
	 *
	 * @param String schoolCode
	 * @param String userId
	 * @param String firstPass
	 * @param String userKubun
	 * @param String contractKubun
	 * @param String contractTerm
	 * @param String validityTerm
	 * @param String sessionNumber
	 * @param String postFunction
	 * @param String rgdate
	 * @param String update
	 * @return boolean 判定
	 */
	private boolean isParamFormat(
		int lineNo,
		String schoolCode,
		String userId,
		String firstPass,
		String userKubun,
		String contractKubun,
		String contractTerm,
		String validityTerm,
		String sessionNumber,
		String postFunction,
		String rgdate,
		String update ) {

		boolean judge = true;

		// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START
		boolean isHelpDesk = false;
		// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END

		// 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD START
		/*
		if (!FormatChecker.isSchoolCode(schoolCode)) {
			setErrorList(lineNo, schoolCode, userId, "学校コードの指定に誤りがあります。");
			judge = false;
		}
		*/

		// ヘルプデスクの判定
		if ("6".equals(userKubun)) {
			isHelpDesk = true;
		}

		// ヘルプデスクの場合、学校コードはチェックしなし
		if (!isHelpDesk) {
			if (!FormatChecker.isSchoolCode(schoolCode)) {
				setErrorList(lineNo, schoolCode, userId, "学校コードの指定に誤りがあります。");
				judge = false;
			}
		}
		// 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD END

		if (!FormatChecker.isUserId(userId)) {
			setErrorList(lineNo, schoolCode, userId, "ユーザＩＤの指定に誤りがあります。");
			judge = false;
		}
		if (!FormatChecker.isFirstPass(firstPass)) {
			setErrorList(lineNo, schoolCode, userId, "初期パスワードの指定に誤りがあります。");
			judge = false;
		}
		if (!FormatChecker.isUserKubun(userKubun)) {
			setErrorList(lineNo, schoolCode, userId, "ユーザー区分の指定に誤りがあります。");
			judge = false;
		}

		// 2015/10/27 QQ)Nishiyama デジタル証明書対応 UPD START
		//if (!FormatChecker.isContractKubun(contractKubun)) {
		//	setErrorList(lineNo, schoolCode, userId, "契約区分の指定に誤りがあります。");
		//	judge = false;
		//}

		if (!FormatChecker.isContractKubun(contractKubun)) {
			setErrorList(lineNo, schoolCode, userId, "契約区分の指定に誤りがあります。");
			judge = false;

		// ヘルプデスクの場合、ユーザ区分・契約区分の整合性チェックを行う
		} else if (isHelpDesk && "5".equals(contractKubun) == false) {
			setErrorList(lineNo, schoolCode, userId, "契約区分の指定に誤りがあります。");
			judge = false;
		}
		// 2015/10/27 QQ)Nishiyama デジタル証明書対応 UPD END

		if (!FormatChecker.isDate(contractTerm)) {
			setErrorList(lineNo, schoolCode, userId, "契約期限の指定に誤りがあります。");
			judge = false;
		}
		if (!FormatChecker.isDate(validityTerm)) {
			setErrorList(lineNo, schoolCode, userId, "証明書有効期限の指定に誤りがあります。");
			judge = false;
		}
		if (!FormatChecker.isSessionNumber(sessionNumber)) {
			setErrorList(lineNo, schoolCode, userId, "最大セッション数の指定に誤りがあります。");
			judge = false;
		}
		if (!FormatChecker.isPostFunction(postFunction)) {
			setErrorList(lineNo, schoolCode, userId, "私書箱機能提供の指定に誤りがあります。");
			judge = false;
		}
		if (!FormatChecker.isDate(rgdate)) {
			setErrorList(lineNo, schoolCode, userId, "登録日の指定に誤りがあります。");
			judge = false;
		}
		if (!FormatChecker.isDate(update)) {
			setErrorList(lineNo, schoolCode, userId, "更新日の指定に誤りがあります。");
			judge = false;
		}
		return judge;
	}

	/**
	 *
	 * 契約校マスタの一括登録リストを作成する
	 *
	 * @param con     DBコネクション
	 * @param keys[]  keys[1]:ユーザID
	 * @return count
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public boolean SelListPactSchool(Connection con)
		throws SQLException {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		String query =
			"SELECT USERID, SCHOOLCD  "
				+ "  FROM  PACTSCHOOL ";
		try {
			stmt = con.prepareStatement(query);

			rs = stmt.executeQuery();
			while (rs.next()) {
				setRegistList(rs.getString("SCHOOLCD"), rs.getString("USERID"));
			}
		} catch (SQLException ex) {
			throw new SQLException("契約校マスタの取得に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return true;
	}

	/**
	 * [2004/09/30] 削除処理は必要ない（登録・変更のみ）
	 * 登録済リストの該当ユーザIDで契約校マスタから削除する
	 *
	 * 	 * @param userId
	 * @return void
	 */
	public boolean DeletePactSchoolUserId(Connection con) throws SQLException, Exception {
		boolean 	brtc 		= true;
		String 		strUserId 	= null;

		for (Iterator it=getRegistList().iterator(); it.hasNext();) {
			RegistError list = (RegistError)it.next();
			strUserId = list.getUserId();
			if ( !(strUserId.equals("") || strUserId == null) )  {
				brtc = DeletePactSchool(con, strUserId);
				if ( !brtc ) {
					break;
				}
				deleteUser++;		// 削除されたユーザ数をカウント
			}
		}
		return brtc;
	}

	/**
	 * [2004/09/30] 削除処理は必要ない（登録・変更のみ）
	 * 登録済リストの該当するユーザIDを消し込む
	 *   （全件登録・更新処理後に残ったユーザIDにより削除処理を行う）
	 *
	 * 	 * @param userId
	 * @return void
	 */
	public void RegistListUserClear(String userId) {
		for (Iterator it=getRegistList().iterator(); it.hasNext();) {
			RegistError list = (RegistError)it.next();
			if ( list.getUserId().equals(userId) ) {
				list.setUserId("");
				break;
			}
		}
	}


	/**
	 * @param lineNo
	 * @param schoolCd
	 * @param userId
	 * @param errorContents
	 * @return void
	 */
	public void setErrorList(
		int lineNo,
		String schoolCode,
		String userId,
		String errorContents) {
		RegistError error = new RegistError();
		error.setLine(String.valueOf(lineNo));
		error.setSchoolCode(schoolCode);
		error.setUserId(userId);
		error.setErrorContents(errorContents);
		registError.add(error);
	}

	/**
	 * @return errorList
	 */
	public List getErrorList() {
		return registError;
	}

	/**
	 * [2004/09/30] 削除処理は必要ない（登録・変更のみ）
	 * （未使用）
	 * 	 * @param schoolCd
	 * @param userId
	 * @return void
	 */
	public void setRegistList(
		String schoolCode,
		String userId ) {
		RegistError list = new RegistError();
		list.setLine(null);
		list.setSchoolCode(schoolCode);
		list.setUserId(userId);
		list.setErrorContents(null);
		registList.add(list);
	}

	/**
	 * [2004/09/30] 削除処理は必要ない（登録・変更のみ）
	 * （未使用）
	 * @return registList
	 */
	public List getRegistList() {
		return registList;
	}

	/**
	 * @return
	 */
	public boolean getJudge() {
		return judgeAll;
	}

	/**
	 * @param b
	 */
	public void setJudge(boolean b) {
		judgeAll = b;
	}

	// ---------------------------------------------
	// HD103Form 情報
	/**
	 * @param HD103Form
	 */
	public void setHD103Form(HD103Form form) {
		// 何もしない
	}

	// ---------------------------------------------

	/**
	* @return
	*/
	public int getLineNo() {
		return lineNo;
	}

	/**
	 * @param string
	 */
	public void setLineNo(int no) {
		lineNo = no;
	}

	/**
	* @return
	*/
	public String getSchoolCode() {
		return schoolCode;
	}

	/**
	 * @param string
	 */
	public void setSchoolCode(String string) {
		schoolCode = string;
	}

	/**
	* @return
	*/
	public String getUserId() {
		return userId;
	}

	/**
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}

	/**
	* @return
	*/
	public String getFirstPass() {
		return firstPass;
	}

	/**
	 * @param string
	 */
	public void setFirstPass(String string) {
		firstPass = string;
	}

	/**
	* @return
	*/
	public String getUserKubun() {
		return userKubun;
	}

	/**
	 * @param string
	 */
	public void setUserKubun(String string) {
		userKubun = string;
	}

	/**
	* @return
	*/
	public String getContractKubun() {
		return contractKubun;
	}

	/**
	 * @param string
	 */
	public void setContractKubun(String string) {
		contractKubun = string;
	}

	/**
	* @return
	*/
	public String getContractTerm() {
		return contractTerm;
	}

	/**
	 * @param string
	 */
	public void setContractTerm(String string) {
		contractTerm = string;
	}

	/**
	* @return
	*/
	public String getValidityTerm() {
		return validityTerm;
	}

	/**
	 * @param string
	 */
	public void setValidityTerm(String string) {
		validityTerm = string;
	}

	/**
	* @return
	*/
	public String getSessionNumber() {
		return sessionNumber;
	}

	/**
	 * @param string
	 */
	public void setSessionNumber(String string) {
		sessionNumber = string;
	}

	/**
	* @return
	*/
	public String getPostFunction() {
		return postFunction;
	}

	/**
	 * @param string
	 */
	public void setPostFunction(String string) {
		postFunction = string;
	}

	/**
	* @return
	*/
	public String getRgdate() {
		return rgdate;
	}

	/**
	 * @param string
	 */
	public void setRgdate(String string) {
		rgdate = string;
	}

	/**
	* @return
	*/
	public String getUpdate() {
		return update;
	}

	/**
	 * @param string
	 */
	public void setUpdate(String string) {
		update = string;
	}

	/**
	* @return
	*/
	public int getRegistUser() {
		return registUser;
	}

	/**
	 * @param string
	 */
	public void setRegistUser(int i) {
		registUser = i;
	}

	/**
	* @return
	*/
	public int getUpdateUser() {
		return updateUser;
	}

	/**
	 * @param string
	 */
	public void setUpdateUser(int i) {
		updateUser = i;
	}

	/**
	* @return
	*/
	public int getDeleteUser() {
		return  deleteUser;
	}

	/**
	 * @param string
	 */
	public void setDeleteUser(int i) {
		deleteUser = i;
	}

	// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START
	/**
	 * @return
	 */
	public boolean getIsDateCheckErr() {
		return isDateCheckErr;
	}

	/**
	 * @param boolean
	 */
	public void setIsDateCheckErr(boolean flg) {
		isDateCheckErr = flg;
	}

	/**
	 * @return
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param string
	 */
	public void setContent(String string) {
		content = string;
	}

	/**
	 * @return
	 */
	public boolean getIsDeleteContract() {
		return isDeleteContract;
	}

	/**
	 * @param boolean
	 */
	public void setIsDeleteContract(boolean flg) {
		isDeleteContract = flg;
	}

	/**
	 * @return
	 */
	public boolean getIsMainteLog() {
		return isMainteLog;
	}

	/**
	 * @param boolean
	 */
	public void setIsMainteLog(boolean flg) {
		isMainteLog = flg;
	}

	/**
	 * @return
	 */
	public String getErrPattern() {
		return errPattern;
	}

	/**
	 * @param string
	 */
	public void setErrPattern(String string) {
		errPattern = string;
	}
	// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END

}

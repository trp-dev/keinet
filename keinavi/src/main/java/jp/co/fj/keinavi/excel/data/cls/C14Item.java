package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−設問別個人成績データクラス
 * 作成日: 2004/07/15
 * @author	H.Fujimoto
 */
public class C14Item {
	//学校名
	private String strGakkomei = "";
	//対象年度
	private String strYear = "";
	//対象模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//表示順序フラグ
	private int intSortFlg = 0;
	//セキュリティスタンプ
	private int intSecuFlg = 0;
	//総合成績データリスト
	private ArrayList c14List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;


	/*----------*/
	/* Get      */
	/*----------*/

	public ArrayList getC14List() {
		return this.c14List;
	}
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public int getIntSortFlg() {
		return this.intSortFlg;
	}
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrYear() {
		return this.strYear;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setC14List(ArrayList c14List) {
		this.c14List = c14List;
	}
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setIntSortFlg(int intSortFlg) {
		this.intSortFlg = intSortFlg;
	}
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrYear(String strYear) {
		this.strYear = strYear;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}
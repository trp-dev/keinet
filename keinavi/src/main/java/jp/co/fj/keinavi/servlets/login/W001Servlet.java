package jp.co.fj.keinavi.servlets.login;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.auth.KnetCookieManager;
import jp.co.fj.keinavi.beans.csv.CSVLine;
import jp.co.fj.keinavi.beans.csv.CSVReader;
import jp.co.fj.keinavi.beans.login.SchoolLoginBean;
import jp.co.fj.keinavi.beans.news.BannerBean;
import jp.co.fj.keinavi.beans.news.InformListBean;
import jp.co.fj.keinavi.beans.security.IMenuDefine;
import jp.co.fj.keinavi.beans.security.MenuSecurityBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.login.W001Form;
import jp.co.fj.keinavi.servlets.DefaultLoginServlet;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.log.KNAccessLog;

/**
 *
 * 高校用ログイン画面サーブレット
 *
 *
 * 2005.03.01	Yoshimoto KAWAI - Totec
 * 				クライアント証明書の取得方法を変更
 *
 * 2005.03.18	Yoshimoto KAWAI - Totec
 * 				利用者ＩＤ認証を追加
 *
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 * 2005.10.18	Yoshimoto KAWAI - TOTEC
 * 				ダウンロードメニューアクセスログ対応
 *
 * @author kawai
 *
 */
public class W001Servlet extends DefaultLoginServlet {

    /**
     * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
     * 			javax.servlet.http.HttpServletRequest,
     * 			javax.servlet.http.HttpServletResponse)
     */

    //2014/10 バナー広告追加対応 start
    //表示用バナーを格納する
    public  BannerBean bannerArray[];
    //2014/10 バナー広告追加対応 end

    public void execute(final HttpServletRequest request,
            final HttpServletResponse response)
            throws ServletException, IOException {

    //2014/10 バナー広告追加対応 start
        if(request.getParameter("col")!=null&&Integer.parseInt(request.getParameter("col"))!=-1)
        {
              //  int col=Integer.parseInt(request.getParameter("col"));
                bannerClick(request,Integer.parseInt(request.getParameter("col")));
                return;
        }
    //2014/10 バナー広告追加対応 end

        // アクションフォーム
        final W001Form form = (W001Form) getActionForm(request,
            "jp.co.fj.keinavi.forms.login.W001Form");

        // シンプルモードフラグ
        form.setSimple(getServletConfig().getInitParameter("simple"));
        // リクエストへセット
        request.setAttribute("form", form);

        // セッションの初期化
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        session = request.getSession(true);
        String first = (String) request.getParameter("first");

        // 転送元がログイン画面
       if (("w001".equals(getBackward(request))
           || "w001s".equals(getBackward(request)))) {
           //       if ("w001".equals(getBackward(request))
           //       || "w001s".equals(getBackward(request))) {
            // ログインセッション
            LoginSession login = new LoginSession();
            // 利用者ＩＤ
            login.setAccount(form.getUser());

            // 証明書オブジェクト
            final X509Certificate[] cert = getCertificate(request);

            Connection con = null; // コネクション
            try {
                con = getConnectionPool(request);
                con.setAutoCommit(false);

                // ログインセッション初期化する
                SchoolLoginBean bean = new SchoolLoginBean();
                bean.setConnection(null, con);
                bean.setAccount(form.getAccount());
                bean.setPassword(form.getPassword());
                bean.setCert(cert);
                bean.setLoginSession(login);
                bean.setSimple(form.getSimple());
                bean.setUser(form.getUser());
                bean.execute();
                con.commit();

            // ログイン失敗
            } catch (final KNServletException e) {
                rollback(con);
                setErrorMessage(request, e);
                actionLog(request, "102", e.getMessage());
            // 想定外の例外
            } catch (final Exception e) {
                rollback(con);
                throw createServletException(e);
            } finally {
                releaseConnectionPool(request, con);
            }

            // ログインセッションを保持する
            session.setAttribute(LoginSession.SESSION_KEY, login);

            // エラーメッセージがない場合
            if (getErrorMessage(request) == null) {
                // 体験版 もしくは
                // 契約校で証明書があり、通常ログイン画面からなら
                // プロファイル選択画面へ遷移する
                if (login.isTrial()
                        || (login.isPact() && login.isCert() && !login.isSimple())) {

                    // メニューセキュリティ
                    setupMenuSecurity(request);

                    // Kei-Navi利用権限がなければ元の画面へ
                    final MenuSecurityBean bean = getMenuSecurityBean(request);
                    if (bean.isValid(IMenuDefine.MENU_KEINAVI)) {
                        actionLog(request, "101");
                        createKeiNetCookie(response, login);
                        forward(request, response, SERVLET_DISPATCHER);
                    } else {
                        actionLog(request, "102", createLoginErrorMessage(
                                form.getAccount(), form.getUser(),
                                "利用権限がありません。"));
                        setupInformListBean(request);
                        setErrorMessage(request, createAuthCheckException(request));
                        forward(request, response, JSP_W001);
                    }

                // 非契約校が /Login から来たら権限エラー
                } else if (!login.isPact() && !login.isSimple()) {
                    actionLog(request, "102", createLoginErrorMessage(
                            form.getAccount(), form.getUser(),
                            "契約校向けログイン画面は利用できません。"));
                    setupInformListBean(request);
                    setErrorMessage(request, createAuthCheckException(request));
                    forward(request, response, JSP_W001);
                // それ以外はダウンロードメニューへ
                } else {
                    actionLog(request, "101");
                    createKeiNetCookie(response, login);
                    forward(request, response, "/FreeMenuTop");
                }

            // エラーメッセージがあるなら元の画面へ
            } else {
                setupInformListBean(request);
                forward(request, response, JSP_W001);
            }

        // それ以外ならログイン画面
        } else {
            setupInformListBean(request);
            forward(request, response, JSP_W001);
        }
    }

    //2014/10 バナー広告追加対応 start
    public void bannerClick(final HttpServletRequest request,int i)
    {
       // BannerBean bannerArray[] =(BannerBean [])request.getParameterValues("bannerArray");
        try {
          //  request.setCharacterEncoding("Windows-31J");
            String name = request.getParameter("bannerName");
            byte[] byteData = name.getBytes("ISO_8859_1");
            name = new String(byteData,"Windows-31J");
            KNAccessLog.lv1s("2",getRemoteAddr(request),request.getServerName(),request.getParameter("bannercode1"),request.getParameter("bannercode2"),name,request.getParameter("url"));
        } catch (UnsupportedEncodingException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }

       // KNAccessLog.lv1s("2",request.getRemoteAddr(),"0001","01","東京大学","http://www.google.com/");
    }

    /**
     * バナー設定ファイルを読み込む
     * @throws Exception
     *
     *
     */
    public BannerBean[] readBannerInfo(final HttpServletRequest request) throws Exception
    {
        ArrayList bannerList = new ArrayList();//全てのバナーを格納する
        int length=0;//表示用バナー数
        int byteread = 0;
        byte filename[] = new byte[3630000];
        InputStream in = new BufferedInputStream(new FileInputStream("/usr/local/tomcat/webapps2/keinavi/DLBanner_settings.csv"));
        while ((byteread = in.read(filename)) != -1) {
            System.out.write(filename, 0, byteread);
        }

        CSVReader reader = new CSVReader(new String(filename));//ファイル名指定.
        CSVLine line = null;
        int lineNo=0;//行数;



        //大学情報を取得処理
        while ((line = reader.readLine()) != null)
        {
            BannerBean tempBean= new BannerBean();

            String[] items = line.getItems();
            if(items.length==5&&!items[0].startsWith("#")){
                String filepath = "/usr/local/tomcat/webapps2/keinavi/"+items[3].replaceAll("\\./","");
                File item3fullpath = new File(filepath);
                if (item3fullpath.exists()){
                    tempBean.setBannerCode1(items[0]);
                    tempBean.setBannerCode2(items[1]);
                    tempBean.setBannerName(items[2]);
                    tempBean.setImgSrc(items[3]);
                    tempBean.setUrl(items[4]);
                    bannerList.add(tempBean);
                    lineNo++;
                 }
            }
        }
        if(lineNo == 0){
            //設定表に何もない場合
        }
        else if(lineNo>=5)
        {
            length=5;
        }
        else if(lineNo<5&&lineNo>0)
        {
            length=lineNo;
        }
        bannerArray=new BannerBean[length];
        //５件以上の設定数がある場合
        if(length==5)
        {
         // 0-lineNoまでの重複しないランダム数生成
         int rdnInt[] = new int[5];
         for(int i =0;i<length;)
         {
             int temp=(int)(Math.random()*(lineNo));
             int j=0;
             for(j=0;j<i;j++)
             {
                 if(temp==rdnInt[j])
                 {
                     break;
                 }
             }
             if(j==i)
             {
                 rdnInt[i] =temp;
                 bannerArray[i]=(BannerBean)bannerList.get(temp);
                 KNAccessLog.lv1s("1",getRemoteAddr(request),request.getServerName(),bannerArray[i].getBannerCode1(),bannerArray[i].getBannerCode2(),bannerArray[i].getBannerName(),bannerArray[i].getUrl());
                 i++;
             }
         }
        }
        //５件未満の場合
        else
        {
            for(int i =0;i<length;i++)
            {
               bannerArray[i]=(BannerBean)bannerList.get(i);
               KNAccessLog.lv1s("1",getRemoteAddr(request),request.getServerName(),bannerArray[i].getBannerCode1(),bannerArray[i].getBannerCode2(),bannerArray[i].getBannerName(),bannerArray[i].getUrl());
            }
        }
         return bannerArray;
    }
    //2014/10 バナー広告追加対応 end

    // Kei-Net連携用Cookie作成
    private void createKeiNetCookie(final HttpServletResponse response,
            final LoginSession login) throws ServletException {

        try {
            // 体験版でなければCookie作成
            if (!login.isTrial()) {
                new KnetCookieManager().create(response, login);
            }
        } catch (final Exception e) {
            throw createServletException(e);
        }
    }

    /**
     * @param account 学校ID
     * @param user 利用者ID
     * @param message メッセージ詳細
     * @return ログインエラーメッセージ
     */
    public String createLoginErrorMessage(final String account,
            final String user, final String message) {

        final StringBuffer buff = new StringBuffer();
        buff.append(account);
        buff.append(" ");
        if (user != null) {
            buff.append(user);
            buff.append(" ");
        }
        buff.append(message);

        return buff.toString();
    }

    /**
     * お知らせリストのセットアップ
     *
     * @param request
     * @throws ServletException
     */
    public void setupInformListBean(final HttpServletRequest request) throws ServletException {

//2014/10/3 DB接続処理をコメントアウト START
        Connection con = null;
        try {
            con = super.getConnectionPool(request);
            con.setAutoCommit(false);

            InformListBean infbean = new InformListBean();
            infbean.setConnection(null, con);
            infbean.setUserID(""); 			// ユーザID
            infbean.setUserMode(0); 		// ユーザーモード
            infbean.setDisplayDiv("0"); 	// 表示区分 "0":お知らせ
            infbean.setTop(true);
            infbean.execute();
            request.setAttribute("InformListBean", infbean);
        } catch (final Exception e) {
            throw new ServletException(e.getMessage());
        } finally {
            super.releaseConnectionPool(request, con);
        }
//2014/10/3 DB接続処理をコメントアウト END
    }

    /**
     * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#getForward(
     * 			javax.servlet.http.HttpServletRequest)
     */
    protected String getForward(final HttpServletRequest request) {
        final String forward = super.getForward(request);
        if (forward == null) {
            final boolean isDLLogin = "1".equals(
                    getServletConfig().getInitParameter("simple"));
            return isDLLogin ? "w001s" : "w001";
        } else {
            return forward;
        }
    }

}

/*
 * 作成日: 2004/07/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.taglib.message;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import jp.co.fj.keinavi.util.message.MessageLoader;

/**
 * メッセージを表示するTaglib
 * 
 * @author kawai
 */
public class MessageTag extends TagSupport {

	private String id; // メッセージID

	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {
		try {
			String message = MessageLoader.getInstance().getMessage(id);
			if (message != null) {
				pageContext.getOut().print(message);
			} 
		} catch (IOException e) {
			throw new JspException(e);
		}

		return EVAL_PAGE;
	}

	/**
	 * @param string
	 */
	public void setId(String string) {
		id = string;
	}

}

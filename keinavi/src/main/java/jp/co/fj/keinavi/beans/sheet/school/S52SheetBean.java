/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.sheet.ExtS52ListBean;
import jp.co.fj.keinavi.excel.data.school.S52BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S52Item;
import jp.co.fj.keinavi.excel.school.S52;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * 2005.04.21	Yoshimoto KAWAI - Totec
 * 				新テスト対応
 * 
 * @author kawai
 *
 */
public class S52SheetBean extends AbstractSheetBean {

	// データクラス
	private final S52Item data = new S52Item();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		String[] code = super.getSubjectCDArray(); // 型・科目設定値
		List graph = super.getSubjectGraphList(); // 型・科目グラフ表示対象
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//		String strWkFlg = "0";
//		String strBakEngptlevelcd = "";
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ

		// 表
		switch (super.getIntFlag(IProfileItem.CHART_COMP_RATIO)) {
			case  1: data.setIntHyouFlg(0); data.setIntKoseihiFlg(1); break; 
			case  2: data.setIntHyouFlg(0); data.setIntKoseihiFlg(2); break; 
			case 11: data.setIntHyouFlg(1); data.setIntKoseihiFlg(1); break; 
			case 12: data.setIntHyouFlg(1); data.setIntKoseihiFlg(2); break;
		}

		// 偏差値帯別度数分布グラフ
		switch (super.getIntFlag(IProfileItem.GRAPH_DIST)) {
			case  1: data.setIntBnpGraphFlg(0); data.setIntAxisFlg(1); break; 
			case  2: data.setIntBnpGraphFlg(0); data.setIntAxisFlg(2); break; 
			case 11: data.setIntBnpGraphFlg(1); data.setIntAxisFlg(1); break; 
			case 12: data.setIntBnpGraphFlg(1); data.setIntAxisFlg(2); break;
		}

		// 偏差値帯別人数積み上げグラフ
		switch (super.getIntFlag(IProfileItem.GRAPH_BUILDUP)) {
			case  1: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(1); break; 
			case  2: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(2); break; 
			case  3: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(3); break; 
			case 11: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(1); break; 
			case 12: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(2); break;
			case 13: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(3); break;
		}

		// 偏差値帯別構成比グラフ
		switch (super.getIntFlag(IProfileItem.GRAPH_COMP_RATIO)) {
			case  1: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(1); break; 
			case  2: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(2); break; 
			case  3: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(3); break; 
			case 11: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(1); break; 
			case 12: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(2); break;
			case 13: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(3); break;
		}

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/25 DP)Nonaka 共通テスト英語認定試験CEFR取得状況 ADD START
//		switch (super.getIntFlag(IProfileItem.OPTION_CHECK_BOX)) {
//			case  1: data.setIntCheckBoxFlg(0); data.setIntTargetCheckBoxFlg(1); break;
//			case  2: data.setIntCheckBoxFlg(0); data.setIntTargetCheckBoxFlg(2); break;
//			case 11: data.setIntCheckBoxFlg(1); data.setIntTargetCheckBoxFlg(1); break;
//			case 12: data.setIntCheckBoxFlg(1); data.setIntTargetCheckBoxFlg(2); break;
//		}
//// 2019/09/25 DP)Nonaka 共通テスト英語認定試験CEFR取得状況 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		// ワークテーブルのセットアップ
		super.insertIntoExistsExam();
		// 同系統で過去の模試の配列
		ExamData[] past = super.getPastExamArray();
		// 模試科目コード変換テーブル
		super.insertIntoSubCDTrans(past);

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/25 DP)Nonaka 共通テスト対応 ADD START
//// 共通テスト英語認定試験CEFR取得状況
//		PreparedStatement ps3 = null;
//		PreparedStatement ps4 = null;
//		PreparedStatement ps5 = null;
//		PreparedStatement ps6 = null;
//		PreparedStatement ps7 = null;
//// 2019/09/25 DP)Nonaka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
		ResultSet rs1 = null;
		ResultSet rs2 = null;
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/24 DP)Nonaka 共通テスト対応 ADD START
//// 共通テスト英語認定試験CEFR取得状況
//		ResultSet rs3 = null;
//		ResultSet rs4 = null;
//		ResultSet rs5 = null;
//		ResultSet rs6 = null;
//		ResultSet rs7 = null;
//// 2019/09/24 DP)Nonaka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
		try {
			List container = new ArrayList(); // 入れ物
			List c1 = new LinkedList(); // 型の入れ物
			List c2 = new LinkedList(); // 科目の入れ物
			
			// データリストの取得
			Query query = QueryLoader.getInstance().load("s52_1");
			query.setStringArray(1, code); // 型・科目コード

			ps1 = conn.prepareStatement(query.toString());
			ps1.setString(3, profile.getBundleCD()); // 一括コード
			ps1.setString(4, exam.getExamYear()); // 模試年度
			ps1.setString(5, exam.getExamCD()); // 模試コード

			// 偏差値分布データリスト
			//ps2 = conn.prepareStatement(QueryLoader.getInstance().load("s52_2").toString());
			ps2 = conn.prepareStatement(super.getQueryWithDeviationZone("s52_2").toString());
			ps2.setString(3, profile.getBundleCD()); // 一括コード

			for (int i=0; i<past.length; i++) {
				// 模試年度
				ps1.setString(1, past[i].getExamYear());
				ps2.setString(1, past[i].getExamYear());
				// 模試コード
				ps1.setString(2, past[i].getExamCD());
				ps2.setString(2, past[i].getExamCD());

				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					ExtS52ListBean bean = new ExtS52ListBean();
					bean.setStrMshmei(past[i].getExamNameAbbr()); // 模試短縮名
					bean.setStrMshDate(past[i].getInpleDate()); // 模試実施基準日
					bean.setExamYear(past[i].getExamYear()); // 模試年度
					bean.setDataOpenDate(past[i].getDataOpenDate()); // データ開放日
					
					bean.setStrKmkCd(rs1.getString(1)); // 対象模試の型・科目コード
					bean.setStrKmkmei(rs1.getString(2));
					bean.setIntDispKmkFlg(graph.contains(rs1.getString(1)) ? 1 : 0);
					bean.setStrHaitenKmk(rs1.getString(3));
					bean.setSubDispSeq(rs1.getInt(4));
					bean.setIntNinzu(rs1.getInt(6));
					bean.setFloHeikin(rs1.getFloat(7));
					bean.setFloHensa(rs1.getFloat(8));

					container.add(bean);

					// 過回当時の型・科目コード
					ps2.setString(4, rs1.getString(5));	
									
					rs2 = ps2.executeQuery();
					while (rs2.next()) {
						S52BnpListBean bnp = new S52BnpListBean();
						bnp.setFloBnpMax(rs2.getFloat(1));
						bnp.setFloBnpMin(rs2.getFloat(2));
						bnp.setIntNinzu(rs2.getInt(3));
						bnp.setFloKoseihi(rs2.getFloat(4));
						bean.getS52BnpList().add(bnp);
					}
					rs2.close();
				}
				rs1.close();
			}
			
			// ソートして詰める
			Collections.sort(container);

			Iterator ite = container.iterator();
			while (ite.hasNext()) {
				ExtS52ListBean bean = (ExtS52ListBean) ite.next();
				
				if ("7000".compareTo(bean.getStrKmkCd()) <= 0) c1.add(bean);
				else c2.add(bean);
			}

			data.getS52List().addAll(c1);
			data.getS52List().addAll(c2);

// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/25 DP)Nonaka 共通テスト対応 ADD START
//// 共通テスト英語認定試験CEFR取得状況
//
//			// 模試コードリスト
//			List<String> examCdList = new ArrayList<String>();
//			for(var exam : past) {
//				if(examCdList.contains(exam.getExamCD())) continue;
//				examCdList.add(exam.getExamCD());
//			}
//
//			//各試験
//			Query query3 = QueryLoader.getInstance().load("s22_7");
//			ps3 = conn.prepareStatement(query3.toString());
//			ps3.setString(1, exam.getExamYear()); // 年度
//			ps3.setString(2, exam.getExamDiv()); // 模試区分
//
//			rs3 = ps3.executeQuery();
//			while (rs3.next()) {
//
//				if (data.getIntTargetCheckBoxFlg() == 1 && !rs3.getString(5).toString().equals(strBakEngptlevelcd)
//						&& examCdList.size() > 0) {
//					strWkFlg = "0";
//					/* 試験毎に自校受験者がいるか確認するSQLを実行 */
//					/* 0件の時にはflg=0,以外はflg=1 */
//					Query query7 = QueryLoader.getInstance().load("s52_6");
//
//					query7.setStringArray(1, examCdList.toArray(new String[0]));	// 模試コード
//
//					ps7 = conn.prepareStatement(query7.toString());
//					ps7.setString(1, exam.getExamYear());     // 年度
//					ps7.setString(2, profile.getBundleCD());  // 一括コード
//					ps7.setString(3, rs3.getString(1));
//					ps7.setString(4, rs3.getString(5));
//					rs7 = ps7.executeQuery();
//					if (rs7.next()) {
//						if (rs7.getInt(1) != 0) {
//							strWkFlg = "1";
//						}
//					}
//					rs7.close();
//					ps7.close();
//					strBakEngptlevelcd = rs3.getString(5).toString();
//
//					if("0".equals(strWkFlg)) continue;
//				}
//
//				for (int i=0; i<past.length; i++) {
//
//					Query query5 = QueryLoader.getInstance().load("s52_4");
//					ps5 = conn.prepareStatement(query5.toString());
//
//					ps5.setString(1, past[i].getExamYear());
//					ps5.setString(2, past[i].getExamCD());
//					ps5.setString(3, past[i].getExamYear());
//					ps5.setString(4, past[i].getExamCD());
//					ps5.setString(5, past[i].getExamYear());
//					ps5.setString(6, past[i].getExamCD());
//					ps5.setString(7, profile.getBundleCD());
//					ps5.setString(8, rs3.getString(1));
//					ps5.setString(9, rs3.getString(5));
//					ps5.setString(10, rs3.getString(8));
//					ps5.setString(11, profile.getBundleCD());
//					ps5.setString(12, rs3.getString(8));
//
//					rs4 = ps5.executeQuery();
//					while (rs4.next()) {
//
//						S52PtCefrAcqStatusListBean pcaslb = new S52PtCefrAcqStatusListBean();
//						pcaslb.setEngptcd(rs3.getString(1));
//						pcaslb.setEngptname_abbr(rs3.getString(2));
//						pcaslb.setLevelflg(rs3.getString(3));
//						pcaslb.setEi_sort(rs3.getInt(4));
//						pcaslb.setEngptlevelcd(rs3.getString(5));
//						pcaslb.setEngptlevelname_abbr(rs3.getString(6));
//						pcaslb.setEdi_sort(rs3.getInt(7));
//						pcaslb.setCefrlevelcd(rs3.getString(8));
//						pcaslb.setCerflevelname_abbr(rs3.getString(9));
//						pcaslb.setCi_sort(rs3.getInt(10));
//						pcaslb.setExamname(past[i].getExamNameAbbr());
//						pcaslb.setNumbers(rs4.getInt(2));
//						pcaslb.setCompratio(rs4.getFloat(3));
//						pcaslb.setFlg(strWkFlg);
//						data.getS52PtCefrAcqStatusList().add(pcaslb);
//
//						break;
//
//					}
//					rs4.close();
//					ps5.close();
//				}
//
//			}
//			rs3.close();
//			ps3.close();
//
//			//全試験
//			Query query4 = QueryLoader.getInstance().load("s22_8");
//			ps4 = conn.prepareStatement(query4.toString());
//			ps4.setString(1, exam.getExamYear()); // 年
//			ps4.setString(2, exam.getExamDiv()); // 模試区分
//
//			rs5 = ps4.executeQuery();
//			while (rs5.next()) {
//
//				for (int i=0; i<past.length; i++) {
//
//					Query query6 = QueryLoader.getInstance().load("s52_5");
//					ps5 = conn.prepareStatement(query6.toString());
//
//					ps5.setString(1,  past[i].getExamYear());
//					ps5.setString(2,  past[i].getExamCD());
//					ps5.setString(3,  past[i].getExamYear());
//					ps5.setString(4,  past[i].getExamCD());
//					ps5.setString(5,  past[i].getExamYear());
//					ps5.setString(6,  past[i].getExamCD());
//					ps5.setString(7, profile.getBundleCD());
//					ps5.setString(8, rs5.getString(1));
//					ps5.setString(9, profile.getBundleCD());
//					ps5.setString(10, rs5.getString(1));
//
//					rs6 = ps5.executeQuery();
//					while (rs6.next()) {
//
//						S52CefrAcqStatusListBean caslb = new S52CefrAcqStatusListBean();
//						caslb.setEngptcd(" ");
//						caslb.setEngptname_abbr(" ");
//						caslb.setLevelflg(" ");
//						caslb.setEi_sort(0);
//						caslb.setEngptlevelcd(" ");
//						caslb.setEngptlevelname_abbr(" ");
//						caslb.setEdi_sort(0);
//						caslb.setCefrlevelcd(rs5.getString(1));
//						caslb.setCerflevelname_abbr(rs5.getString(2));
//						caslb.setCi_sort(0);
//						caslb.setExamname(past[i].getExamNameAbbr());
//						caslb.setDispsequence(0);
//						caslb.setNumbers(rs6.getInt(2));
//						caslb.setCompratio(rs6.getFloat(3));
//						caslb.setFlg(rs6.getString(4));
//						data.getS52CefrAcqStatusList().add(caslb);
//
//						break;
//
//					}
//					rs6.close();
//					ps5.close();
//				}
//			}
//			rs5.close();
//			ps4.close();
//
//// 2019/09/25 DP)Nonaka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/25 DP)Nonaka 共通テスト対応 ADD START
//// 共通テスト英語認定試験CEFR取得状況
//			DbUtils.closeQuietly(rs3);
//			DbUtils.closeQuietly(rs4);
//			DbUtils.closeQuietly(rs5);
//			DbUtils.closeQuietly(rs6);
//			DbUtils.closeQuietly(rs7);
//// 2019/09/25 DP)Nonaka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/25 DP)Nonaka 共通テスト対応 ADD START
//// 共通テスト英語認定試験CEFR取得状況
//			DbUtils.closeQuietly(ps3);
//			DbUtils.closeQuietly(ps4);
//			DbUtils.closeQuietly(ps5);
//			DbUtils.closeQuietly(ps6);
//			DbUtils.closeQuietly(ps7);
//// 2019/09/25 DP)Nonaka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S52_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_PAST_DEV;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S52().s52(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
	}

}

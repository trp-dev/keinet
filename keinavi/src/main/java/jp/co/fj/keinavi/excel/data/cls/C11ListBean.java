package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−偏差値分布データリスト
 * 作成日: 2004/07/13
 * @author	H.Fujimoto
 */
public class C11ListBean {
	//学年
	private String strGrade = "";
	//クラス
	private String strClass = "";
	//型・科目別データリスト
	private ArrayList c11KmkDataList = new ArrayList();	
	
	/*----------*/
	/* Get      */
	/*----------*/

	public ArrayList getC11KmkDataList() {
		return this.c11KmkDataList;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrGrade() {
		return this.strGrade;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setC11KmkDataList(ArrayList c11KmkDataList) {
		this.c11KmkDataList = c11KmkDataList;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}
}
package jp.co.fj.keinavi.beans.graph;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import jp.co.totec.config.ConfigResolver;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;

/**
 *
 * グラフ出力対象Beanの管理クラス
 * 
 * 2006.07.27	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class GraphCheckerBeanFactory {

	// インスタンスキャッシュ
	private static final Map CACHE = new HashMap();
	
	static {
		final Configuration config = ConfigResolver.getInstance(
				).getConfiguration("graphchecker.properties");
		
		for (final Iterator ite = config.getKeys(); ite.hasNext();) {
			// 画面ID
			final String key = (String) ite.next();
			// クラス名;プロファイルカテゴリID
			final String[] value = StringUtils.split(config.getString(key), ';');

			try {
				final Class cls = Class.forName(value[0]);
				final Constructor constructor = cls.getConstructor(
						new Class[]{String.class});
				
				// 詳細画面
				if (value.length == 1) {
					CACHE.put(null, constructor.newInstance(new Object[]{null}));
				// 一括出力画面
				} else {
					CACHE.put(key, constructor.newInstance(new Object[]{value[1]}));
				}
			} catch (final Exception e) {
				throw new IllegalArgumentException(
						"グラフ出力対象Beanのインスタンス生成に失敗しました。" + value[0]);
			}
		}
	}

	/**
	 * @param id 機能ごとのID
	 * @return グラフ出力対象Bean
	 */
	public static final AbstractGraphCheckerBean getInstance(final String id) {

		if (CACHE.containsKey(id)) {
			return (AbstractGraphCheckerBean) CACHE.get(id);
		} else {
			throw new InternalError(
					"不明なグラフ出力対象BeanのIDです。" + id);
		}
	}
	
}

package jp.co.fj.keinavi.beans.maintenance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.beans.admin.IndivChangeInfoTableAccess;
import jp.co.fj.keinavi.data.maintenance.KNStudentExtData;
import jp.co.fj.keinavi.util.message.MessageLoader;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 生徒統合処理Bean
 *
 * 2006.10.31	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class StudentIntegrationBean extends DefaultBean {

	// ログ出力モジュール
	private final MainteLogBean log;
	// 再集計対象登録Bean
	private final IntegrationRecountBean recount;

	// 主生徒データ
	private KNStudentExtData mainStudentData;
	// 従生徒データ
	private KNStudentExtData subStudentData;

	// コンストラクタ
	public StudentIntegrationBean(final String pSchoolCd) {
		log = new MainteLogBean(pSchoolCd);
		recount = new IntegrationRecountBean();
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws  Exception {

		// 模試重複チェック
		checkExamDuplicate();
		// 個人成績の統合
		integrateSubRecord();
		integrateQRecord();
		integrateCandidate();
		integrateSubAcademic();
		// 従生徒にあって主生徒にない履歴を作る
		createHistoryInfo();
		// 再集計対象を登録
		registRecount();

		//更新：変更成績情報テーブル
		IndivChangeInfoTableAccess.integrateIndividual(conn,
				mainStudentData.getIndividualId(),
				subStudentData.getIndividualId());

		// 個表データの統合
		integrateIndividualRecord();
		// 従生徒を非表示に
		updateNotDisplayFlg();
		// 生徒統合ログ
		log.studentIntegrationLog(
				mainStudentData.getIndividualId(),
				subStudentData.getIndividualId());
	}

	// 模試情報重複チェック
	private void checkExamDuplicate() throws Exception {

		// エラーメッセージバッファ
		final StringBuffer buff = new StringBuffer();

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("m34"));
			ps.setString(1, mainStudentData.getIndividualId());
			ps.setString(2, subStudentData.getIndividualId());
			rs = ps.executeQuery();
			while (rs.next()) {
				buff.append(rs.getString(1) + "\\n");
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}

		// 重複あり
		if (buff.length() > 0) {
			throw new KNBeanException(
					MessageLoader.getInstance().getMessage("m013a")
					+ "\\n\\n" + buff);
		}
	}

	// 再集計対象を登録する
	private void registRecount() throws Exception {
		recount.setBeforeIndividualId(subStudentData.getIndividualId());
		recount.setAfterIndividualId(mainStudentData.getIndividualId());
		recount.execute();
	}

	// 従生徒にあって主生徒にない履歴を作る
	private void createHistoryInfo() throws SQLException {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("m30"));
			ps.setString(1, mainStudentData.getIndividualId());
			ps.setString(2, subStudentData.getIndividualId());
			ps.setString(3, mainStudentData.getIndividualId());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	// 科目別成績の統合
	private void integrateSubRecord() throws SQLException {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("UPDATE subrecord_i "
					+ "SET individualid = ? "
					+ "WHERE individualid = ?");
			ps.setString(1, mainStudentData.getIndividualId());
			ps.setString(2, subStudentData.getIndividualId());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	// 設問別成績の統合
	private void integrateQRecord() throws SQLException {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("UPDATE qrecord_i "
					+ "SET individualid = ? "
					+ "WHERE individualid = ?");
			ps.setString(1, mainStudentData.getIndividualId());
			ps.setString(2, subStudentData.getIndividualId());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	// 志望校評価の統合
	private void integrateCandidate() throws SQLException {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("UPDATE candidaterating "
					+ "SET individualid = ? "
					+ "WHERE individualid = ?");
			ps.setString(1, mainStudentData.getIndividualId());
			ps.setString(2, subStudentData.getIndividualId());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

       // 科目別学力要素の統合
        private void integrateSubAcademic() throws SQLException {

                PreparedStatement ps = null;
                try {
                        ps = conn.prepareStatement("UPDATE SUBACADEMIC_I "
                                        + "SET individualid = ? "
                                        + "WHERE individualid = ?");
                        ps.setString(1, mainStudentData.getIndividualId());
                        ps.setString(2, subStudentData.getIndividualId());
                        ps.executeUpdate();
                } finally {
                        DbUtils.closeQuietly(ps);
                }
        }

	// 個表データの統合
	private void integrateIndividualRecord() throws SQLException {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("m32"));
			ps.setString(1, mainStudentData.getIndividualId());
			ps.setString(2, subStudentData.getIndividualId());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	// 従生徒を非表示にする
	private void updateNotDisplayFlg() throws SQLException {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("UPDATE basicinfo "
					+ "SET notdisplayflg = '1' "
					+ "WHERE individualid = ?");
			ps.setString(1, subStudentData.getIndividualId());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * @see com.fjh.beans.DefaultBean#setConnection(
	 * 			java.lang.String, java.sql.Connection)
	 */
	public void setConnection(final String pName, final Connection pConn) {
		super.setConnection(pName, pConn);
		log.setConnection(pName, pConn);
		recount.setConnection(pName, pConn);
	}

	/**
	 * @param pSubStudentData 設定する subStudentData。
	 */
	public void setSubStudentData(KNStudentExtData pSubStudentData) {
		subStudentData = pSubStudentData;
	}

	/**
	 * @param pMainStudentData 設定する mainStudentData。
	 */
	public void setMainStudentData(KNStudentExtData pMainStudentData) {
		mainStudentData = pMainStudentData;
	}

}

/*
 * 作成日: 2004/06/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.profile;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class W008Form extends BaseForm {

	// プロファイルID
	private String profileID = null;
	// 集計区分コード
	private String countingDivCode = null;
	// ページ番号
	private String page = null;

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * @return
	 */
	public String getProfileID() {
		return profileID;
	}

	/**
	 * @param string
	 */
	public void setProfileID(String string) {
		profileID = string;
	}

	/**
	 * @return
	 */
	public String getCountingDivCode() {
		return countingDivCode;
	}

	/**
	 * @param string
	 */
	public void setCountingDivCode(String string) {
		countingDivCode = string;
	}

	/**
	 * @return
	 */
	public String getPage() {
		return page;
	}

	/**
	 * @param string
	 */
	public void setPage(String string) {
		page = string;
	}

}

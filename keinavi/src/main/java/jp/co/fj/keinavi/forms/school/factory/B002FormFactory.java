/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.school.factory;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import jp.co.fj.keinavi.forms.school.SMaxForm;
import jp.co.fj.keinavi.data.LoginSession;
import com.fjh.forms.ActionForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class B002FormFactory extends AbstractSFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 * 成績概況
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		// アクションフォーム
		SMaxForm form = super.createSMaxForm(request);
		// アイテムマップ
		Map item = getItemMap(request);
		// ログイン情報
		LoginSession login =
			(LoginSession)request.getSession().getAttribute(LoginSession.SESSION_KEY);
			
		CommonFormFactory commForm = new CommonFormFactory(form, item);
		commForm.setForm0103();
		commForm.setForm0203();
		commForm.setForm0601();
		commForm.setForm0701();
		commForm.setForm1501();
		commForm.setForm0402();
		commForm.setForm1102();
		commForm.setForm1511();		
		commForm.setForm1512();	
		commForm.setForm1513();	
		commForm.setForm1514();	
		commForm.setForm1515();
		commForm.setForm1305(login.isSales());
		commForm.setFormat();
		commForm.setOption();
		return commForm.getForm();
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
//		SMaxForm form = (SMaxForm)request.getSession().getAttribute(SMaxForm.SESSION_KEY);
		SMaxForm form = (SMaxForm)request.getAttribute("form");
		// アイテムマップ
		Map item = getItemMap(request);
		// ログイン情報
		LoginSession login =
			(LoginSession)request.getSession().getAttribute(LoginSession.SESSION_KEY);	
				
		CommonItemFactory commItem = new CommonItemFactory(form, item);
		commItem.setItem0103();
		commItem.setItem0203();
		commItem.setItem0601();
		commItem.setItem0701();
		commItem.setItem1501();
		commItem.setItem0402();
		commItem.setItem1102();
		commItem.setItem1511();		
		commItem.setItem1512();	
		commItem.setItem1513();	
		commItem.setItem1514();	
		commItem.setItem1515();
		commItem.setItem1305();
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap("020501");
	}

}

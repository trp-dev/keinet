/**
 * 校内成績分析−クラス比較　設問別成績
 * 出力する帳票の判断
 * 作成日: 2004/08/09
 * @author	T.Sakai
 */
 
package jp.co.fj.keinavi.excel.cls;
import jp.co.fj.keinavi.util.log.*;

import java.util.*;

import jp.co.fj.keinavi.excel.data.cls.*;

public class C33 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean c33( C33Item c33Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		try{
			//C33Itemから各帳票を出力
			int ret = 0;
			if (( c33Item.getIntHyouFlg()==0 )&&( c33Item.getIntGraphFlg()==0 )) {
				throw new Exception("C33 ERROR : フラグ異常");
			}
			if ( c33Item.getIntHyouFlg()==1 ) {
				C33_01 exceledit = new C33_01();
				ret = exceledit.c33_01EditExcel( c33Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C33_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C33_01");
			}
			if ( c33Item.getIntGraphFlg()==1 ) {
				C33_02 exceledit = new C33_02();
				ret = exceledit.c33_02EditExcel( c33Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C33_02","帳票作成エラー","");
					return false;
				}
				sheetLog.add("C33_02");
			}
			
		} catch(Exception e) {
			log.Err("99C33","帳票作成エラー",e.toString());
			return false;
		}
		return true;
	}

}
package jp.co.fj.keinavi.data.individual;

import java.text.ParseException;
import java.util.Date;

/**
 *
 * 受験予定大学一覧用データクラス
 *
 *
 * 2004.08.23	Tomohisa YAMADA - Totec
 * 				新規作成
 *
 * <2010年度改修>
 * 2009.10.02	Tomohisa YAMADA - Totec
 * 				入試日程変更対応
 *
 * 2016.01.15   Hiroyuki Nishiyama - QuiQsoft
 *              大規模改修：ソート順変更
 *
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 *
 */
public class PlanUnivData implements Comparable {


	private boolean isUnited;						//「統合・分割」:true;
	private boolean isRegistered; 				//入試予定または入試詳細のデータを判別する

	protected String univCd;							//共：大学コード
	protected String univNameAbbr;					//共：大学名
	protected String facultyCd;						//共：学部コード
	protected String facultyNameAbbr;				//共：学部
	protected String deptCd;							//共：学科コード
	protected String deptNameAbbr;					//共：学科
	protected String scheduleCd;						//日程コード(１はセンター)
	protected String deptSortKey; //学科ソートキー
	private String entExamTypeCd;					//予：入試形態コード 01:一般 02:推薦 03:AO 04:特奨・給費生 05:その他
	private String entExamOdeName;					//予：入試形態名

	private String[] provEntExamSite;						//予：地方試験地1〜10
	private String[] examPlanDates;						//予：受験予定日1〜10
	private String year;									//共：年度
	private String examDiv;								//共：模試区分

	private String[] entExamInpleUniqueDate;				//他：相関を使って実地日を一意にした文字列配列
	private String remarks;								//予：備考
	//ガイドライン
	private String guideLine = new String();
	private String univDivCd;								//大学区分コード
	private String prefCdHq;								//本部県コード

	protected String branchName;

	protected ScheduleDetailData schedule;

	/* 2016/01/15 QQ)Nishiyama 大規模改修 ADD START */
	protected String kanaNum;                // カナ付50音順番号
	protected String uniNightDiv;            // 大学夜間部区分
	protected String facultyConCd;           // 学部内容コード
	protected String deptSerialNo;           // 学科通番
	protected String scheduleSys;            // 日程方式
	protected String scheduleSysBranchCd;    // 日程方式枝番
	protected String deptNameKana;           // 学科カナ名
	/* 2016/01/15 QQ)Nishiyama 大規模改修 ADD END */

	public void setSchedule(ScheduleDetailData schedule) {
		this.schedule = schedule;
	}

	public ScheduleDetailData getSchedule() {
		return schedule;
	}

	public PlanUnivData() {}

	public PlanUnivData(
			String univCd,
			String univNameAbbr,
			String facultyCd,
			String facultyNameAbbr,
			String deptCd,
			String deptNameAbbr,
			String scheduleCd,
			String deptSortKey,
			String entExamTypeCd,
			String entExamOdeName,
			String year,
			String examDiv,
			String univDivCd,
			String prefCdHq,
			String entExamDiv_1_2term,
			String entExamDiv_1_2order,
			String schoolProventDiv,
			Date entExamInpleDate1_1,
			Date entExamInpleDate1_2,
			Date entExamInpleDate1_3,
			Date entExamInpleDate1_4,
			Date entExamInpleDate2_1,
			Date entExamInpleDate2_2,
			Date entExamInpleDate2_3,
			Date entExamInpleDate2_4,
			String pleDateDiv1_1,
			String pleDateDiv1_2,
			String pleDateDiv1_3,
			String pleDateUniteDiv,
			String pleDateDiv2_1,
			String pleDateDiv2_2,
			String pleDateDiv2_3,
			String branchName) throws NumberFormatException, ParseException {
		this.univCd = univCd;
		this.univNameAbbr = univNameAbbr;
		this.facultyCd = facultyCd;
		this.facultyNameAbbr = facultyNameAbbr;
		this.deptCd = deptCd;
		this.deptNameAbbr = deptNameAbbr;
		this.scheduleCd = scheduleCd;
		this.deptSortKey = deptSortKey;
		this.entExamTypeCd = entExamTypeCd;
		this.entExamOdeName = entExamOdeName;
		this.year = year;
		this.examDiv = examDiv;
		this.univDivCd = univDivCd;
		this.prefCdHq = prefCdHq;
		this.branchName = branchName;

		schedule = new ScheduleDetailData(
				entExamInpleDate1_1,
	            entExamInpleDate1_2,
	            entExamInpleDate1_3,
	            entExamInpleDate1_4,
	            pleDateDiv1_1,
	            pleDateDiv1_2,
	            pleDateDiv1_3,
	            entExamInpleDate2_1,
	            entExamInpleDate2_2,
	            entExamInpleDate2_3,
	            entExamInpleDate2_4,
	            pleDateDiv2_1,
	            pleDateDiv2_2,
	            pleDateDiv2_3,
	            pleDateUniteDiv,
	            branchName,
	            Integer.parseInt(entExamDiv_1_2order),
	            Integer.parseInt(schoolProventDiv),
            	Integer.parseInt(entExamDiv_1_2term));
	}

	/* (非 Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 * 大学・学部・学科・入試形態の順
	 */
	public int compareTo(Object o) {
		PlanUnivData d2 = (PlanUnivData) o;
		if(Integer.parseInt(this.univCd) < Integer.parseInt(d2.getUnivCd())){
			return -1;
		}else if(Integer.parseInt(this.univCd) > Integer.parseInt(d2.getUnivCd())){
			return 1;
		}else{
			if(Integer.parseInt(this.facultyCd) < Integer.parseInt(d2.getFacultyCd())){
				return -1;
			}else if(Integer.parseInt(this.facultyCd) > Integer.parseInt(d2.getFacultyCd())){
				return 1;
			}else{
				if(Integer.parseInt(this.deptCd) < Integer.parseInt(d2.getDeptCd())){
					return -1;
				}else if(Integer.parseInt(this.deptCd) > Integer.parseInt(d2.getDeptCd())){
					return 1;
				}else{
					if(Integer.parseInt(this.entExamTypeCd) < Integer.parseInt(d2.getEntExamTypeCd())){
						return -1;
					}else if(Integer.parseInt(this.entExamTypeCd) > Integer.parseInt(d2.getEntExamTypeCd())){
						return 1;
					}else{
						return 0;
					}
				}
			}
		}
	}

	/**
	 * このデータを一意に識別する為のキー
	 * @return
	 */
	public String getDataKey(){
		return getUnivCd()+","+getFacultyCd()+","+getDeptCd()+","+getEntExamTypeCd();
	}


	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchName() {
		return branchName;
	}

	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getDeptNameAbbr() {
		return deptNameAbbr;
	}

	/**
	 * @return
	 */
	public String[] getEntExamInpleUniqueDate() {
		return entExamInpleUniqueDate;
	}

	/**
	 * @return
	 */
	public String getEntExamOdeName() {
		return entExamOdeName;
	}

	/**
	 * @return
	 */
	public String getEntExamTypeCd() {
		return entExamTypeCd;
	}

	/**
	 * @return
	 */
	public String[] getExamPlanDates() {
		return examPlanDates;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getFacultyNameAbbr() {
		return facultyNameAbbr;
	}

	/**
	 * @return
	 */
	public boolean isRegistered() {
		return isRegistered;
	}

	/**
	 * @return
	 */
	public String[] getProvEntExamSite() {
		return provEntExamSite;
	}

	/**
	 * @return
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @return
	 */
	public String getUnivNameAbbr() {
		return univNameAbbr;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setDeptNameAbbr(String string) {
		deptNameAbbr = string;
	}

	/**
	 * @param strings
	 */
	public void setEntExamInpleUniqueDate(String[] strings) {
		entExamInpleUniqueDate = strings;
	}

	/**
	 * @param string
	 */
	public void setEntExamOdeName(String string) {
		entExamOdeName = string;
	}

	/**
	 * @param string
	 */
	public void setEntExamTypeCd(String string) {
		entExamTypeCd = string;
	}

	/**
	 * @param strings
	 */
	public void setExamPlanDates(String[] strings) {
		examPlanDates = strings;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyNameAbbr(String string) {
		facultyNameAbbr = string;
	}

	/**
	 * @param b
	 */
	public void setRegistered(boolean b) {
		isRegistered = b;
	}

	/**
	 * @param strings
	 */
	public void setProvEntExamSite(String[] strings) {
		provEntExamSite = strings;
	}

	/**
	 * @param string
	 */
	public void setRemarks(String string) {
		remarks = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

	/**
	 * @param string
	 */
	public void setUnivNameAbbr(String string) {
		univNameAbbr = string;
	}

	/**
	 * @return
	 */
	public boolean isUnited() {
		return isUnited;
	}

	/**
	 * @param b
	 */
	public void setUnited(boolean b) {
		isUnited = b;
	}

	/**
	 * @return
	 */
	public String getGuideLine() {
		return guideLine;
	}

	/**
	 * @param string
	 */
	public void setGuideLine(String string) {
		guideLine = string;
	}

	/**
	 * @return
	 */
	public String getExamDiv() {
		return examDiv;
	}

	/**
	 * @return
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param string
	 */
	public void setExamDiv(String string) {
		examDiv = string;
	}

	/**
	 * @param string
	 */
	public void setYear(String string) {
		year = string;
	}

	/**
	 * @return
	 */
	public String getDeptSortKey() {
		return deptSortKey;
	}

	/**
	 * @param string
	 */
	public void setDeptSortKey(String string) {
		deptSortKey = string;
	}

	/**
	 * @return prefCdHq を戻します。
	 */
	public String getPrefCdHq() {
		return prefCdHq;
	}

	/**
	 * @param pPrefCdHq 設定する prefCdHq。
	 */
	public void setPrefCdHq(String pPrefCdHq) {
		prefCdHq = pPrefCdHq;
	}

	/**
	 * @return univDivCd を戻します。
	 */
	public String getUnivDivCd() {
		return univDivCd;
	}

	/**
	 * @param pUnivDivCd 設定する univDivCd。
	 */
	public void setUnivDivCd(String pUnivDivCd) {
		univDivCd = pUnivDivCd;
	}

	public String getScheduleCd() {
		return scheduleCd;
	}

	public void setScheduleCd(String scheduleCd) {
		this.scheduleCd = scheduleCd;
	}

	// 2016/01/15 QQ)Nishiyama 大規模改修 ADD START
	/**
	 * カナ付50音順番号を取得する
	 *
	 * @return カナ付50音順番号
	 * */
	 public String getKanaNum() {
	    return kanaNum;
	 }	/**

	 * カナ付50音順番号を設定する
	 *
	 * @param kanaNum カナ付50音順番号
	 * */
	 public void setKanaNum(String kanaNum) {
	    this.kanaNum = kanaNum;
	 }

	/**
	 * 大学夜間部区分を取得する
	 *
	 * @return 大学夜間部区分
	 * */
	 public String getUniNightDiv() {
	    return uniNightDiv;
	 }

	/**
	 * 大学夜間部区分を設定する
	 *
	 * @param uniNightDiv 大学夜間部区分
	 * */
	 public void setUniNightDiv(String uniNightDiv) {
	    this.uniNightDiv = uniNightDiv;
	 }

	/**
	 * 学部内容コードを取得する
	 *
	 * @return 学部内容コード
	 * */
	 public String getFacultyConCd() {
	    return facultyConCd;
	 }

	/**
	 * 学部内容コードを設定する
	 *
	 * @param facultyConCd 学部内容コード
	 * */
	 public void setFacultyConCd(String facultyConCd) {
	    this.facultyConCd = facultyConCd;
	 }

	/**
	 * 学科通番を取得する
	 *
	 * @return 学科通番
	 * */
	 public String getDeptSerialNo() {
	    return deptSerialNo;
	 }

	/**
	 * 学科通番を設定する
	 *
	 * @param deptSerialNo 学科通番
	 * */
	 public void setDeptSerialNo(String deptSerialNo) {
	    this.deptSerialNo = deptSerialNo;
	 }

	/**
	 * 日程方式を取得する
	 *
	 * @return 日程方式
	 * */
	 public String getScheduleSys() {
	    return scheduleSys;
	 }

	/**
	 * 日程方式を設定する
	 *
	 * @param scheduleSys 日程方式
	 * */
	 public void setScheduleSys(String scheduleSys) {
	    this.scheduleSys = scheduleSys;
	 }

	/**
	 * 日程方式枝番を取得する
	 *
	 * @return 日程方式枝番
	 * */
	 public String getScheduleSysBranchCd() {
	    return scheduleSysBranchCd;
	 }

	/**
	 * 日程方式枝番を設定する
	 *
	 * @param scheduleSysBranchCd 日程方式枝番
	 * */
	 public void setScheduleSysBranchCd(String scheduleSysBranchCd) {
	    this.scheduleSysBranchCd = scheduleSysBranchCd;
	 }

	/**
	 * 学科カナ名を取得する
	 *
	 * @return 学科カナ名
	 */
	 public String getDeptNameKana() {
	    return deptNameKana;
	 }

	/**
	 * 学科カナ名を設定する
	 *
	 * @param deptNameKana 学科カナ名
	 * */
	 public void setDeptNameKana(String deptNameKana) {
	    this.deptNameKana = deptNameKana;
	 }
	// 2016/01/15 QQ)Nishiyama 大規模改修 ADD END

}

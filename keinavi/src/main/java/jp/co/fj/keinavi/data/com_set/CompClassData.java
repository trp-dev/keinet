/*
 * 作成日: 2004/07/12
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.com_set;

import java.io.Serializable;
import java.util.List;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CompClassData implements Serializable {

	private String examYear; // 模試年度
	private String examCD; // 模試コード
	private List aClassData; // クラスデータ（全選択）
	private List iClassData; // クラスデータ（個別選択）

	public CompClassData() {
	}

	/**
	 * コンストラクタ
	 * @param examYear
	 * @param examCD
	 */
	public CompClassData(String examYear, String examCD) {
		this.examYear = examYear;
		this.examCD = examCD;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		CompClassData data = (CompClassData) obj;
		
		return this.getExamYear().equals(data.getExamYear())
			&& this.getExamCD().equals(data.getExamCD());
	}

	/**
	 * @return
	 */
	public List getAClassData() {
		return aClassData;
	}

	/**
	 * @return
	 */
	public String getExamCD() {
		return examCD;
	}

	/**
	 * @return
	 */
	public List getIClassData() {
		return iClassData;
	}

	/**
	 * @param list
	 */
	public void setAClassData(List list) {
		aClassData = list;
	}

	/**
	 * @param string
	 */
	public void setExamCD(String string) {
		examCD = string;
	}

	/**
	 * @param list
	 */
	public void setIClassData(List list) {
		iClassData = list;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

}

/*
 * 作成日: 2004/07/27
 *
 * 面談メモ削除・新規作成・編集Bean
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.data.individual.InterviewData;
import jp.co.fj.keinavi.util.db.RecordProcessor;

import com.fjh.beans.DefaultBean;

/*
 * 
 * @author Administrator
 * 面談メモ削除・新規作成・編集
 * 
 * 
 */
public class InterviewBean extends DefaultBean {

	/** 出力パラメター */
	private InterviewData i502Data;

	/** 入力パラメター */
	private String individualId;
	private String koIndividualId;
	private String title;
	private String text;
	private String firstRegistrYdt;
	private String renewalDate;
	
	/** toDoパラメター。これによってアクションを決定する */
	private boolean isEdit = false;
	private boolean isUpdate = false;
	private boolean isNew = false;
	private boolean isIns = false;
	private boolean isDel = false;

	/** Temporaryパラメター。フォームに渡す */
	private String Year;
	private String Month;
	private String Date;

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		if(isEdit()){
			//メモのデータ取得
			setI502Data(execI502Data(getIndividualId(),
					getKoIndividualId(), getFirstRegistrYdt()));
		}else if(isUpdate()){
			//実際の上書き処理
			if (!execUpdateMemo(getIndividualId(), getKoIndividualId(),
					getFirstRegistrYdt(), getCreateDate(), getTitle(), getText())) {
				//上書きに失敗なら新規挿入処理
				execInsertMemo(getIndividualId(),
						getTitle(), getText(), getCreateDate());
			}
		}else if(isNew()){
			//新規処理
			setI502Data(execI502Data());
		} else if(isDel()){
			//削除処理
			execDeleteMemo();
		}else{
			//新規挿入処理
			execInsertMemo(getIndividualId(),
					getTitle(), getText(), getCreateDate());
		}
	}
	
	/**
	 * 新規
	 * @param individualId
	 * @param title
	 * @param text
	 * @param dateStr
	 * @throws SQLException
	 */
	private void execInsertMemo(String individualId, String title, String text, String dateStr) throws SQLException{
	
		String firstRegistrYdt = "";

		//カレンダー
		Calendar cal1 = Calendar.getInstance();

		//初回登録日時
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmss");//デートフォーマット
		firstRegistrYdt = sdf1.format(cal1.getTime());
		//更新日時
		String renewalDate = firstRegistrYdt.substring(0,8);

		//SQL実行
		PreparedStatement ps = null;
		try {
			//個人ID、初回登録日時、作成日、タイトル、テキスト
			StringBuffer strBuf = new StringBuffer();
			
			// Kei-Naviのレコード
			if (koIndividualId.trim().length() == 0) {
				strBuf.append("INSERT INTO INTERVIEW (INDIVIDUALID, KO_INDIVIDUALID, FIRSTREGISTRYDT, CREATEDATE, TITLE, TEXT, RENEWALDATE) ");
				strBuf.append("VALUES (?,'          ',?,?,?,?,?)");
				ps = conn.prepareStatement(strBuf.toString());
				ps.setString(1, individualId);
				
			// 校内成績処理システムのレコード
			} else {
				strBuf.append("INSERT INTO INTERVIEW (KO_INDIVIDUALID, INDIVIDUALID, FIRSTREGISTRYDT, CREATEDATE, TITLE, TEXT, RENEWALDATE) ");
				strBuf.append("VALUES (?,'          ',?,?,?,?,?)");
				ps = conn.prepareStatement(strBuf.toString());
				ps.setString(1, koIndividualId);
			}
			
			ps.setString(2, firstRegistrYdt);
			ps.setString(3, dateStr);
			ps.setString(4, title);
			ps.setString(5, text);
			ps.setString(6, renewalDate);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	/**
	 * 作成日時を返します
	 * @return 作成日時
	 */
	private String getCreateDate() {
		return RecordProcessor.getYMD8(getYear(), getMonth(), getDate());
	}
	
	/**
	 * 更新
	 * @param individualId
	 * @param firstRegistrYdt
	 * @param date
	 * @param title
	 * @param text
	 * @throws Exception
	 */
	private boolean execUpdateMemo(String individualId, 
			String otherIndividualId, String firstRegistrYdt,
			String date, String title, String text)
		throws Exception {

		//更新日時の取得
		Calendar cal1 = Calendar.getInstance();
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd"); //デートフォーマット
		String renewalDate = sdf1.format(cal1.getTime());

		//キー（個人ID、初回登録年月日）
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("UPDATE INTERVIEW SET CREATEDATE = ?, TITLE = ?, TEXT = ? , RENEWALDATE = ?");
		strBuf.append("WHERE INDIVIDUALID = ? AND KO_INDIVIDUALID = ? AND FIRSTREGISTRYDT = ? ");

		//文字列ながさが１５文字以下
		//文字列長さが２００字以下
		//SQL実行
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(strBuf.toString());
			ps.setString(1, date);
			ps.setString(2, title);
			ps.setString(3, text);
			ps.setString(4, renewalDate);
			ps.setString(5, individualId);
			ps.setString(6, otherIndividualId);
			ps.setString(7, firstRegistrYdt);

			return ps.executeUpdate() == 1;
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	/**
	 * 新規登録用
	 * i502.jspを再利用するために、空のデータを返す
	 * 現在の日付を使います00は省く
	 * @return
	 */
	private InterviewData execI502Data(){
		Calendar time = Calendar.getInstance();
		String thisYear = Integer.toString(time.get(Calendar.YEAR));
		String thisMonth = Integer.toString(time.get(Calendar.MONTH) + 1);
		String thisDate = Integer.toString(time.get(Calendar.DATE));

		InterviewData data = new InterviewData();
		setYear(thisYear);
		setMonth(thisMonth);
		setDate(thisDate);
		data.setCreateDate(thisYear+thisMonth+thisDate);
		data.setTitle("");
		data.setText("");
		data.setIndividualId(individualId);
		return data;
	}
	/**
	 * 編集用に選択されたこの生徒の面談メモを取得
	 * @param individualId
	 * @return
	 * @throws I501DatasException
	 */
	private InterviewData execI502Data(String individualId,
			String otherIndividualId, String firstRegistrYdt) throws Exception {
		String sql = ""+
			"SELECT CREATEDATE, TITLE, " +
			"TEXT FROM INTERVIEW WHERE INDIVIDUALID = ? " +
			"AND KO_INDIVIDUALID = ? AND FIRSTREGISTRYDT = ?";
		
		InterviewData data = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//面談メモ情報取得
			ps = conn.prepareStatement(sql);
			ps.setString(1, individualId);
			ps.setString(2, otherIndividualId);
			ps.setString(3, firstRegistrYdt);
			rs = ps.executeQuery();

			if (rs.next()) {
				data = new InterviewData();
				setYear(RecordProcessor.getYearDigit(rs.getString("CREATEDATE")));
				setMonth(RecordProcessor.trimZero(RecordProcessor.getMonthDigit(rs.getString("CREATEDATE"))));
				setDate(RecordProcessor.trimZero(RecordProcessor.getDateDigit(rs.getString("CREATEDATE"))));
				data.setCreateDate(rs.getString("CREATEDATE"));
				data.setTitle(rs.getString("TITLE"));
				data.setText(rs.getString("TEXT"));
				data.setFirstRegistrYdt(firstRegistrYdt);
				data.setIndividualId(individualId);
				data.setKoIndividualId(koIndividualId);
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
		
		return data;
	}
	
	/**
	 * 選択されたメモを削除します
	 * @return
	 */
	public void execDeleteMemo() throws Exception{

		final String deleteSql = "DELETE FROM INTERVIEW WHERE INDIVIDUALID = ? "
		 		+ "AND KO_INDIVIDUALID = ? AND FIRSTREGISTRYDT = ?";
		
		//SQL実行
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(deleteSql);
			ps.setString(1, individualId);
			ps.setString(2, koIndividualId);
			ps.setString(3, firstRegistrYdt);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	/**
	 * @return
	 */
	public InterviewData getI502Data() {
		return i502Data;
	}

	/**
	 * @return
	 */
	public boolean isEdit() {
		return isEdit;
	}

	/**
	 * @param data
	 */
	public void setI502Data(InterviewData data) {
		i502Data = data;
	}

	/**
	 * @param b
	 */
	public void setEdit(boolean b) {
		isEdit = b;
	}

	/**
	 * @return
	 */
	public boolean isUpdate() {
		return isUpdate;
	}

	/**
	 * @param b
	 */
	public void setUpdate(boolean b) {
		isUpdate = b;
	}

	/**
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param string
	 */
	public void setText(String string) {
		text = string;
	}

	/**
	 * @param string
	 */
	public void setTitle(String string) {
		title = string;
	}

	/**
	 * @return
	 */
	public boolean isNew() {
		return isNew;
	}

	/**
	 * @param b
	 */
	public void setNew(boolean b) {
		isNew = b;
	}


	/**
	 * @return
	 */
	public String getDate() {
		return Date;
	}

	/**
	 * @return
	 */
	public String getMonth() {
		return Month;
	}

	/**
	 * @return
	 */
	public String getYear() {
		return Year;
	}

	/**
	 * @param string
	 */
	public void setDate(String string) {
		Date = string;
	}

	/**
	 * @param string
	 */
	public void setMonth(String string) {
		Month = string;
	}

	/**
	 * @param string
	 */
	public void setYear(String string) {
		Year = string;
	}

	/**
	 * @return
	 */
	public String getFirstRegistrYdt() {
		return firstRegistrYdt;
	}

	/**
	 * @param string
	 */
	public void setFirstRegistrYdt(String string) {
		firstRegistrYdt = string;
	}


	/**
	 * @return
	 */
	public boolean isIns() {
		return isIns;
	}

	/**
	 * @param b
	 */
	public void setIns(boolean b) {
		isIns = b;
	}

	/**
	 * @return
	 */
	public boolean isDel() {
		return isDel;
	}

	/**
	 * @param b
	 */
	public void setDel(boolean b) {
		isDel = b;
	}

	/**
	 * @return
	 */
	public String getRenewalDate() {
		return renewalDate;
	}

	/**
	 * @param string
	 */
	public void setRenewalDate(String string) {
		renewalDate = string;
	}

	/**
	 * @return individualId を戻します。
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @param pIndividualId 設定する individualId。
	 */
	public void setIndividualId(String pIndividualId) {
		individualId = pIndividualId;
	}

	/**
	 * @return koIndividualId を戻します。
	 */
	public String getKoIndividualId() {
		return koIndividualId;
	}

	/**
	 * @param pKoIndividualId 設定する koIndividualId。
	 */
	public void setKoIndividualId(String pKoIndividualId) {
		koIndividualId = pKoIndividualId;
	}

}

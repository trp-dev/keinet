package jp.co.fj.keinavi.beans.recount;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.recount.data.RecountRequest;
import jp.co.fj.keinavi.beans.recount.data.RecountTask;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * 再集計依頼を処理するワーカ
 * 
 * 2006.02.13	[新規作成]
 * 
 * 2007.08.07	校内成績処理システムより移植
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class RecountWorker implements Runnable {

	// ログインスタンス
	private static final Log LOG = LogFactory.getLog(RecountWorker.class);

	// デバッグ用
	// スレッド停止時間（秒）
	private static final int RECOUNT_WAIT_TIME = KNCommonProperty.getIntValue(
			"RecountWaitTime", 0);
	
	// クラス再集計Bean
	private final ClassRecountBean recount = new ClassRecountBean();
	
	private final RecountChannel channel;
	private final String dbDriver;
	private final String dbUrl;
	private final String dbUserId;
	private final String dbPassword;
	private final Thread th;
	
	/**
	 * @param name スレッド識別名
	 * @param pChannel 自分がプールされているチャンネルオブジェクト
	 * @param pDbDriver DBドライバ
	 * @param pDbUrl DBurl
	 * @param pDbUserId DBuserId
	 * @param pDbPassword DBpassword
	 */
	RecountWorker(final String name, final RecountChannel pChannel,
			final String pDbDriver, final String pDbUrl,
			final String pDbUserId, final String pDbPassword) {
		channel = pChannel;
		dbDriver = pDbDriver;
		dbUrl = pDbUrl;
		dbUserId = pDbUserId;
		dbPassword = pDbPassword;
		th = new Thread(this, name);
		th.start();
	}

	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run() {

		LOG.info("再集計処理スレッドを開始します。" + th.getName());
		
		Connection con = null;
		try {
			Class.forName(dbDriver);
			con = DriverManager.getConnection(dbUrl, dbUserId, dbPassword);
			con.setAutoCommit(false);
			// Bean初期化
			recount.setConnection(con);
			// メイン処理
			execute(con);
		} catch (final Exception e) {
			LOG.error("DBコネクションの取得に失敗しました。" + th.getName(), e);
		} finally {
			DbUtils.closeQuietly(con);
		}
		
		LOG.info("再集計処理スレッドを停止します。" + th.getName());
	}

	// 処理ループ
	private void execute(final Connection con) {
		
		while (true) {
			RecountRequest request = null;
			try {
				// チャンネルからリクエストをもらう
				// リクエストが来るまでロックされる
				request = channel.takeRequest();

				// NULLが戻ってくる＝チャンネル停止依頼が出されたのでスレッドを抜ける
				if (request == null) {
					break;
				// 処理をする
				} else {
					LOG.info("再集計処理を開始します。" + request);
					final long start = System.currentTimeMillis();
					recount(con, request);
					final long m = System.currentTimeMillis() - start;
					LOG.info("再集計処理が終了しました。（" + m + "msec）" + request);
				}
			} catch (final Throwable e) {
				errorExecute(con, request, e);
			}
		}
	}
	
	// executeのエラー処理
	private void errorExecute(final Connection con,
			final RecountRequest request, final Throwable e) {
		
		LOG.error("再集計処理に失敗しました。" + th.getName() + ":" + request, e);
		
		rollback(con);
		
		LOG.info("エラーのため処理できなかった再集計対象を削除します..." + request);
		
		try {
			if (deleteRecounting(con, request)) {
				LOG.info("エラー発生再集計対象:" + request);
			}
			con.commit();
		} catch (final SQLException s) {
			rollback(con);
			LOG.error("再集計対象のクリーンに失敗しました。" + request, s);
		}
	}

	// 再集計処理
	private void recount(final Connection con,
			final RecountRequest request) throws Exception {
		
		// 再集計処理依頼をロック取得
		if (selectRecounting(con, request)) {
			
			// ロックデバッグ用
			if (RECOUNT_WAIT_TIME > 0) {
				LOG.info(RECOUNT_WAIT_TIME + "秒待ちます...");
				Thread.sleep(RECOUNT_WAIT_TIME * 1000);
			}
			
			// 再集計タスクを取得
			final RecountTask[] task = getRecountTask(con, request.getSchoolCd());

			// タスク分繰り返す
			for (int i = 0; i < task.length; i++) {
				recount.setRecountTask(task[i]);
				recount.execute();
			}
			
			// 再集計依頼削除
			deleteRecounting(con, request);
			
			// ここまで来たらコミット
			con.commit();
		}
	}

	// 再集計タスクを取得する
	private RecountTask[] getRecountTask(final Connection con,
			final String schoolCd) throws Exception {
		
		final List list = new LinkedList();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement(
					QueryLoader.getInstance().getQuery("recount03"));
			ps.setString(1, schoolCd);
			ps.setString(2, schoolCd);
			ps.setString(3, schoolCd);
			ps.setString(4, schoolCd);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new RecountTask(rs));
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
		
		return (RecountTask[]) list.toArray(new RecountTask[list.size()]);
	}

	// 再集計依頼を取得する
	private boolean selectRecounting(final Connection con,
			final RecountRequest request) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement(
					QueryLoader.getInstance().getQuery("recount06"));
			ps.setString(1, request.getSchoolCd());
			rs = ps.executeQuery();
			return rs.next();
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	// 再集計依頼を削除する
	private boolean deleteRecounting(final Connection con,
			final RecountRequest request) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement(
					QueryLoader.getInstance().getQuery("recount07"));
			ps.setString(1, request.getSchoolCd());
			rs = ps.executeQuery();
			return rs.next();
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	private void rollback(final Connection con) {
		try {
			con.rollback();
		} catch (final SQLException e) {
			LOG.error("ロールバックに失敗しました。", e);
		}
	}

}
 

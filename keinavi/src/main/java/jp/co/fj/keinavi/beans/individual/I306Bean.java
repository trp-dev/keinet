package jp.co.fj.keinavi.beans.individual;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import jp.co.fj.keinavi.data.individual.PlanUnivData2;
import jp.co.fj.keinavi.data.individual.YearMonthDateData;
import jp.co.fj.keinavi.data.individual.YearMonthDateData.YearData;
import jp.co.fj.keinavi.data.individual.YearMonthDateData.YearData.MonthData;
import jp.co.fj.keinavi.util.db.RecordProcessor;
import jp.co.fj.keinavi.util.individual.IPropsLoader;

import com.fjh.beans.DefaultSearchBean;

/**
 * 
 * スケジュール（カレンダー）専用Beanクラス
 * 
 * 2004.07.27	Tomohisa YAMADA - Totec
 * 				新規作成
 * 
 * 2005.1.24 	Tomohisa YAMADA - Totec
 * 				システム使用月が１・２・３月の場合の曜日の表示修正
 * 
 * 2005.03.10	Tomohisa YAMADA - Totec
 * 				4月〜3月の表示を5月〜4月に変更
 * 
 * 2005.06.20	Tomohisa YAMADA - Totec
 * 				カレンダー横移動用ボタン追加に伴う修正
 * 
 * 2005.06.21 	Tomohisa YAMADA - Totec
 * 				year, month, dateの扱いをStringからintに変更
 *
 * <2010年度改修>
 * 2009.11.20	Tomohisa YAMADA - Totec
 * 				入試日程変更対応
 *
 * @author Tomohisa YAMADA - Totec
 * @version 2.0
 *
 */
public class I306Bean extends DefaultSearchBean{

	/**
	 * 表示範囲
	 */
	//private final int DISP_DAY_LENGTH = 27;//[4] del
	private final int DISP_DAY_LENGTH = Integer.parseInt(IPropsLoader.getInstance().getMessage("DISP_DAY_LENGTH"));//[5] add
	
	/**
	 * デフォルト表示開始月
	 */
//	private final String DEFAULT_MONTH = "2";//[5] del
	private final int DEFAULT_MONTH = Integer.parseInt(IPropsLoader.getInstance().getMessage("DISP_DEFAULT_MONTH"));//[5] add
	/**
	 * デフォルト表示開始日
	 */
//	private final String DEFAULT_DATE = "1";//[5] del
	private final int DEFAULT_DATE = Integer.parseInt(IPropsLoader.getInstance().getMessage("DISP_DEFAULT_DATE"));//[5] add
	
	/**
	 * 曜日文字列
	 */
	private final String[] DAY_STR = {"日","月","火","水","木","金","土"};
		
	/** 出力パラメター */
	private YearMonthDateData yearMonthDateData;
	
	/** 入力パラメター */
	private List resultList;
//	private String year;//[5] del
//	private String month;//[5] del
//	private String date;//[5] del
	private int year;//[5] add
	private int month;//[5] add
	private int date;//[5] add


	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
			  
		//表示用カレンダー情報の取得
//		if(month == null || date == null){//[5] del
		if(month == 0 || date == 0){//[5] add
			month = DEFAULT_MONTH;
			date = DEFAULT_DATE;
			//受験日、出願締切日、合格発表日、入学手続締切日の中で最初の日付をデフォルト開始日とする
			setDefaultMonthAndDate(resultList);
		}
		
		//[2] add start
//		//開始日が１・２・３で今月が１・２・３月以外なら「年+１」をする //[3] del
		//開始日が１・２・３で今月が１・２・３・４月以外なら「年+１」をする //[3] add
		Calendar calendar = Calendar.getInstance( );
		int yearN = calendar.get(Calendar.YEAR);
		int thisMonth = calendar.get(Calendar.MONTH) + 1;
		//[3] del start
//		if(Integer.parseInt(month.trim()) == 1 || Integer.parseInt(month.trim()) == 2 || Integer.parseInt(month.trim()) == 3){
//			if(4 <= thisMonth && thisMonth <= 12){
		//[3] del end
		//[3] add start
		if(month == 1 || month == 2 || month == 3 || month == 4){
			if(5 <= thisMonth && thisMonth <= 12){
		//[3] add end
				yearN ++;	
			}
		}
//		//開始日が４〜１２月なで今月が１・２・３月なら「年-1」をする //[3] del
		//[3] del start
//		if(4 <= Integer.parseInt(month.trim()) && Integer.parseInt(month.trim()) <= 12){
//			if(thisMonth == 1 || thisMonth == 2 || thisMonth == 3){
		//[3] del end
		//[3] add start
		if(5 <= month && month <= 12){
			if(thisMonth == 1 || thisMonth == 2 || thisMonth == 3 || thisMonth == 4){
		//[3] add end
				yearN --;	
			}
		}
		//[2] add end
		//年の設定
		year = yearN;
		
		// ヘッダーの作成
		setYearMonthDateData(execDateArray(year, month, date));
		// 一覧データの取得
		execDatas(resultList, getYearMonthDateData().getFlatArray());
	}
	
	
	private static final SimpleDateFormat formatter = new SimpleDateFormat("M/d");
	private static final SimpleDateFormat formatter2 = new SimpleDateFormat("MMdd");
	
	/**
	 * 受験日、出願締切日、合格発表日、入学手続締切日の中で最初の日付をデフォルト開始日とする(データがあれば)
	 * @param resultList
	 * @throws ParseException 
	 * @throws NumberFormatException 
	 */
	private void setDefaultMonthAndDate(List resultList) throws NumberFormatException, ParseException{
		int earliestMD = 9999;
		if(resultList != null && resultList.size() > 0){
			Iterator ite = resultList.iterator();
			while(ite.hasNext()){
				PlanUnivData2 data = (PlanUnivData2)ite.next();
				if(data.getPlanMap() != null && data.getPlanMap().size() > 0){//入試区分ごとにマップがある
					Set set = data.getPlanMap().entrySet();
					Iterator ite2 = set.iterator();
					while(ite2.hasNext()){
						Map.Entry me = (Map.Entry)ite2.next();
						PlanUnivData2 pd = (PlanUnivData2)me.getValue();
						int appliDeadLine = 9999;
						int sucAnnDate = 9999;
						int procDeadLine = 9999;
						int examPlanDate = 9999;
						//出願締切日
						if(pd.getAppliDeadline() != null && !pd.getAppliDeadline().trim().equals("")){
							appliDeadLine = Integer.parseInt(formatter2.format(formatter.parse(pd.getAppliDeadline())));
							//if(appliDeadLine < 400){//1月・2月・3月は12月の後ろとして計算 //[3] del
							if(appliDeadLine < 500){//1月・2月・3月・4月は12月の後ろとして計算 //[3] add
								appliDeadLine += 1200;
							}
							if(appliDeadLine < earliestMD) earliestMD = appliDeadLine;
						}
						//合格発表日
						if(pd.getSucAnnDate() != null && !pd.getSucAnnDate().trim().equals("")){
							sucAnnDate = Integer.parseInt(formatter2.format(formatter.parse(pd.getSucAnnDate())));
							//if(sucAnnDate < 400){//[3] del
							if(sucAnnDate < 500){//[3] add
								sucAnnDate += 1200;
							}
							if(sucAnnDate < earliestMD) earliestMD = sucAnnDate;
						}
						//入学手続締切日
						if(pd.getProceDeadLine() != null && !pd.getProceDeadLine().trim().equals("")){
							procDeadLine = Integer.parseInt(formatter2.format(formatter.parse(pd.getProceDeadLine())));
							//if(procDeadLine < 400){//[3] del
							if(procDeadLine < 500){//[3] add
								procDeadLine += 1200;
							}
							if(procDeadLine < earliestMD) earliestMD = procDeadLine;
						}
						//受験日
						if(pd.getExamPlanDate() != null && pd.getExamPlanDate().length > 0){
							for(int i=0; i<pd.getExamPlanDate().length; i++){
								if(pd.getExamPlanDate()[i] != 0){
									examPlanDate = pd.getExamPlanDate()[i];
									//if(examPlanDate < 400){//[3] del
									if(examPlanDate < 500){//[3] add
										examPlanDate += 1200;
									}
									if(examPlanDate < earliestMD) earliestMD = examPlanDate;
								}
							}
						}
					}
				}				
			}
		}
		if(earliestMD != 9999 && earliestMD > 1300) earliestMD -= 1200;
		if(earliestMD != 9999){//何らかの日付が存在したときのみセットする
			String earliestMDStr = Integer.toString(earliestMD);
			if(earliestMD < 999){
				earliestMDStr = "0"+Integer.toString(earliestMD);
			}
			month = Integer.parseInt(RecordProcessor.getMonthDigit(earliestMDStr));
			date = Integer.parseInt(RecordProcessor.getDateDigit(earliestMDStr));
		}
	}
	
	/**
	 * 
	 * @param individualId
	 * @param dateArray
	 */
	private void execDatas(List resultList, String[][] dateArray){

		Iterator ite = resultList.iterator();
		while(ite.hasNext()){
			//日付がかぶった場合は 1:出(2) 2:締(4) 3:受(1) 4:合(3) ()は表示優先順位
			PlanUnivData2 data = (PlanUnivData2)ite.next();
			data.setDateArray(new String[DISP_DAY_LENGTH]);
			for(int dateCount=0; dateCount<DISP_DAY_LENGTH; dateCount++){
				Set set = data.getPlanMap().entrySet();
				Iterator ite2 = set.iterator();
				while(ite2.hasNext()){
					Map.Entry me = (Map.Entry)ite2.next();
					PlanUnivData2 pd = (PlanUnivData2)me.getValue();
					//どれでもない
					if(data.getDateArray()[dateCount] == null){
						data.getDateArray()[dateCount] = "";
					}
					//締め切り日
					String proceDeadLine = RecordProcessor.nonDateWSlash(pd.getProceDeadLine());
					String proceDeadLineMonth = RecordProcessor.trimZero(RecordProcessor.getMonthDigit(proceDeadLine));
					String proceDeadLineDate = RecordProcessor.trimZero(RecordProcessor.getDateDigit(proceDeadLine));
					if(dateArray[dateCount][0].equals(proceDeadLineMonth) && dateArray[dateCount][1].equals(proceDeadLineDate)){
						if(!data.getDateArray()[dateCount].equals("3") && !data.getDateArray()[dateCount].equals("1") && !data.getDateArray()[dateCount].equals("4")){
							data.getDateArray()[dateCount] = "2";	
						}
					}
					//合格発表日
					String sucAnnDate = RecordProcessor.nonDateWSlash(pd.getSucAnnDate());
					String sucAnnDateMonth = RecordProcessor.trimZero(RecordProcessor.getMonthDigit(sucAnnDate));
					String sucAnnDateDate = RecordProcessor.trimZero(RecordProcessor.getDateDigit(sucAnnDate));
					if(dateArray[dateCount][0].equals(sucAnnDateMonth) && dateArray[dateCount][1].equals(sucAnnDateDate)){
						if(!data.getDateArray()[dateCount].equals("3") && !data.getDateArray()[dateCount].equals("1")){
							data.getDateArray()[dateCount] = "4";	
						}
					}
					//出願締め切り日
					String appliDeadLine = RecordProcessor.nonDateWSlash(pd.getAppliDeadline());
					String appliMonth = RecordProcessor.trimZero(RecordProcessor.getMonthDigit(appliDeadLine));
					String appliDate = RecordProcessor.trimZero(RecordProcessor.getDateDigit(appliDeadLine));
					if(dateArray[dateCount][0].equals(appliMonth) && dateArray[dateCount][1].equals(appliDate)){
						if(!data.getDateArray()[dateCount].equals("3")){
							//受験日が既に入っていなければ上書き
							data.getDateArray()[dateCount] = "1";
						}
					}
					//受験日
					String[] examPlanDate = pd.getExamPlanDateStr();
					if(examPlanDate != null && examPlanDate.length > 0){
						for(int i=0; i<examPlanDate.length; i++){
							String date = RecordProcessor.nonDateWSlash(examPlanDate[i]);
							String dateMonth = RecordProcessor.trimZero(RecordProcessor.getMonthDigit(date));
							String dateDate = RecordProcessor.trimZero(RecordProcessor.getDateDigit(date));
							if(dateArray[dateCount][0].equals(dateMonth) && dateArray[dateCount][1].equals(dateDate)){
								data.getDateArray()[dateCount] = "3";
							}
						}
					}
				}

			}
		}

			/*
			List list = new ArrayList();
			for(int i=0; i<temp.length; i++){
				int j = 0;
				I306Data data = new I306Data();
				data.setUnivName(temp[i][j++]);
				data.setFacultyName(temp[i][j++]);
				data.setDeptName(temp[i][j++]);
				data.setAppliDeadline(temp[i][j++]);
				data.setProceDeadline(temp[i][j++]);
				data.setEntExamInpleDate(temp[i][j++]);
				data.setSucAnnDate(temp[i][j++]);
				data.setDateArray(new String[DISP_DAY_LENGTH]);
				String month1 = RecordProcessor.trimZero(RecordProcessor.getMonthDigit(data.getAppliDeadline()));
				String month2 = RecordProcessor.trimZero(RecordProcessor.getMonthDigit(data.getProceDeadline()));
				String month3 = RecordProcessor.trimZero(RecordProcessor.getMonthDigit(data.getEntExamInpleDate()));
				String month4 = RecordProcessor.trimZero(RecordProcessor.getMonthDigit(data.getSucAnnDate()));
				String date1 = RecordProcessor.trimZero(RecordProcessor.getDateDigit(data.getAppliDeadline()));
				String date2 = RecordProcessor.trimZero(RecordProcessor.getDateDigit(data.getProceDeadline()));
				String date3 = RecordProcessor.trimZero(RecordProcessor.getDateDigit(data.getEntExamInpleDate()));
				String date4 = RecordProcessor.trimZero(RecordProcessor.getDateDigit(data.getSucAnnDate()));
					
				for(int dateCount=0; dateCount<DISP_DAY_LENGTH; dateCount++){
					if(dateArray[dateCount][0].equals(month1) && dateArray[dateCount][1].equals(date1))
						data.getDateArray()[dateCount] = "1";
					else if(dateArray[dateCount][0].equals(month2) && dateArray[dateCount][1].equals(date2))
						data.getDateArray()[dateCount] = "2";
					else if(dateArray[dateCount][0].equals(month3) && dateArray[dateCount][1].equals(date3))
						data.getDateArray()[dateCount] = "3";
					else if(dateArray[dateCount][0].equals(month4) && dateArray[dateCount][1].equals(date4))
						data.getDateArray()[dateCount] = "4";
					else
						data.getDateArray()[dateCount] = "-";
				}
				
				//この部分は判定の時に入れる。今は一時データを格納している 
				data.setCenterRating("T");		//センター(合格可能性判定)
				data.setSecondRating("T");		//二次(合格可能性判定)
				data.setTotalRating("T");		//総合(合格可能性判定)
				
				recordSet.add(data);
			}
			*/
	}
	/**
	 * ヘッダー作成
	 * [data] -> [yearList] -> [monthList] -> [dateList]
	 * @param yearStr 年度
	 * @param monthStr
	 * @param dateStr
	 * @return
	 */
	//private YearMonthDateData execDateArray(String yearStr, String monthStr, String dateStr){//[5] del
	private YearMonthDateData execDateArray(int yearStr, int monthStr, int dateStr){//[5] add
		try {
			int year = yearStr;
			int month = monthStr - 1;
			int date = dateStr;
			Calendar calendar = new GregorianCalendar(year, month, date);
			String[] tempDate = new String[3];
			YearMonthDateData data = new YearMonthDateData();
			data.setYearList(new ArrayList());
			YearMonthDateData.YearData yearData = data.new YearData();
			yearData.setYear(Integer.toString(yearStr));
			data.getYearList().add(yearData);
			yearData = (YearData) ((List)data.getYearList()).get(data.getYearList().size() -1 );
			yearData.setYear(Integer.toString(yearStr));
			yearData.setMonthList(new ArrayList());
			for(int i=0; i<DISP_DAY_LENGTH; i++){
				//初めてのMONTH
				YearMonthDateData.YearData.MonthData monthData;
				if(tempDate[0] == null || !Integer.toString(calendar.get(Calendar.MONTH)+1).equals(tempDate[0])){
					monthData = yearData.new MonthData();
					monthData.setMonth(Integer.toString(calendar.get(Calendar.MONTH)+1));
					monthData.setDateList(new ArrayList());
					yearData.getMonthList().add(monthData);
				}
				monthData = (MonthData) yearData.getMonthList().get(yearData.getMonthList().size() - 1);
				
				//初めてのDate
				YearMonthDateData.YearData.MonthData.DateData dateData;
				if(tempDate[1] == null || !Integer.toString(calendar.get(Calendar.DATE)).equals(tempDate[1])){
					dateData = monthData.new DateData();
					dateData.setDate(Integer.toString(calendar.get(Calendar.DATE)));
					dateData.setDay(DAY_STR[calendar.get(Calendar.DAY_OF_WEEK)-1]);
					monthData.getDateList().add(dateData);
				}
				
				tempDate[0] = Integer.toString(calendar.get(Calendar.MONTH)+1);
				tempDate[1] = Integer.toString(calendar.get(Calendar.DATE));
				tempDate[2] = DAY_STR[calendar.get(Calendar.DAY_OF_WEEK)-1];
				
				calendar.add(Calendar.DATE, 1);
			}
			return data;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @return
	 */
	public YearMonthDateData getYearMonthDateData() {
		return yearMonthDateData;
	}

	/**
	 * @param data
	 */
	public void setYearMonthDateData(YearMonthDateData data) {
		yearMonthDateData = data;
	}

	/**
	 * @return
	 */
	public List getResultList() {
		return resultList;
	}

	/**
	 * @param list
	 */
	public void setResultList(List list) {
		resultList = list;
	}

	/**
	 * @return
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * @return
	 */
	public int getYear() {
		return year;
	}
	
	/**
	 * @param i
	 */
	public void setMonth(int i) {
		month = i;
	}
	
	/**
	 * @param i
	 */
	public void setYear(int i) {
		year = i;
	}

	/**
	 * @return
	 */
	public int getDate() {
		return date;
	}

	/**
	 * @param i
	 */
	public void setDate(int i) {
		date = i;
	}

}

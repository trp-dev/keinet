/*
 * 作成日: 2004/08/18
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class YearMonthDateData {
	private List yearList;

	/**
	 * @return
	 */
	public List getYearList() {return yearList;}

	/**
	 * @param list
	 */
	public void setYearList(List list) {yearList = list;}
	public int getYearListSize(){
		int size = 0;
		if(getYearList() != null)
			size = getYearList().size();
		return size;
	}
	/**
	 * [0] = month, [1] = date, [2] = day
	 * ex. [11][15][土],[11][16][日],[11][17][月]
	 * @return
	 */
	public String[][] getFlatArray(){
		List list = new ArrayList();
		int count = 0;
		for (java.util.Iterator it=getYearList().iterator(); it.hasNext(); ) {
			YearMonthDateData.YearData yearData = (YearMonthDateData.YearData)it.next();
			for (java.util.Iterator it2=yearData.getMonthList().iterator(); it2.hasNext(); ) {
				YearMonthDateData.YearData.MonthData monthData = (YearMonthDateData.YearData.MonthData)it2.next();
				for (java.util.Iterator it3=monthData.getDateList().iterator(); it3.hasNext(); ) {
					YearMonthDateData.YearData.MonthData.DateData dateData = (YearMonthDateData.YearData.MonthData.DateData)it3.next();
					String[] flatArray = new String[3];
					flatArray[0] = monthData.getMonth();
					flatArray[1] = dateData.getDate();
					flatArray[2] = dateData.getDay();
					list.add(flatArray);
				}
			}
		}
		return (String[][]) list.toArray(new String[0][0]);
	}
	public class YearData{
		private String year;
		private List monthList;
		/**
		 * @return
		 */
		public List getMonthList() {return monthList;}

		/**
		 * @return
		 */
		public String getYear() {return year;}

		/**
		 * @param list
		 */
		public void setMonthList(List list) {monthList = list;}

		/**
		 * @param string
		 */
		public void setYear(String string) {year = string;}
		public int getMonthListSize(){
			int size = 0;
			if(getMonthList() != null)
				size = getMonthList().size();
			return size;
		}
		public class MonthData{
			private String month;
			private List dateList;
			/**
			 * @return
			 */
			public List getDateList() {return dateList;}

			/**
			 * @return
			 */
			public String getMonth() {return month;}

			/**
			 * @param list
			 */
			public void setDateList(List list) {dateList = list;}

			/**
			 * @param string
			 */
			public void setMonth(String string) {month = string;}
			public int getDateListSize(){
				int size = 0;
				if(getDateList() != null)
					size = getDateList().size();
				return size;
			}
			public class DateData{
				private String date;
				private String day;
				/**
				 * @return
				 */
				public String getDate() {return date;}

				/**
				 * @return
				 */
				public String getDay() {return day;}

				/**
				 * @param string
				 */
				public void setDate(String string) {date = string;}

				/**
				 * @param string
				 */
				public void setDay(String string) {day = string;}

			}

		}
	}
}

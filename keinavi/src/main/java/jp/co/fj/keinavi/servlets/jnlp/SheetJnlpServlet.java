package jp.co.fj.keinavi.servlets.jnlp;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.profile.Profile;

/**
 *
 * 帳票ダウンロードマネージャのJNLPサーブレット
 * 
 * 2008.02.07	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class SheetJnlpServlet extends DefaultJnlpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.jnlp.DefaultJnlpServlet#createParameter(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	protected String createParameter(final HttpServletRequest request) {
		
		final StringBuffer buff = new StringBuffer();
		
		// 全てのパラメータを引き継ぐ
		buff.append(getAllParameter(request));
		
		// セッションID
		buff.append("&sessionId=" + request.getSession(false).getId());
		
		// 志望大学評価別人数一括出力対象
		final Profile profile = getProfile(request);
		if (profile != null) {
			buff.append("&univFlag1=" + profile.getItemMap("020303").get("1301"));
			buff.append("&univFlag2=" + profile.getItemMap("020404").get("1301"));
			buff.append("&univFlag3=" + profile.getItemMap("020504").get("1301"));
			buff.append("&univFlag4=" + profile.getItemMap("030105").get("1301"));
			buff.append("&univFlag5=" + profile.getItemMap("030204").get("1301"));
			buff.append("&univFlag6=" + profile.getItemMap("020504").get("1301"));
		}
				
		return buff.toString();
	}
	
}

package jp.co.fj.keinavi.forms.user;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * 
 * 利用者管理−担当クラス設定画面アクションフォーム
 * 
 * 
 * 2005.10.13	[新規作成]
 * 
 * 
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class U003Form extends BaseForm {
	
	// 設定モード
	private String mode;
	// 担当クラス
	private String[] classes;
	
	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	/**
	 * @return classes を戻します。
	 */
	public String[] getClasses() {
		return classes;
	}

	/**
	 * @param classes 設定する classes。
	 */
	public void setClasses(String[] classes) {
		this.classes = classes;
	}

	/**
	 * @return mode を戻します。
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param mode 設定する mode。
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}
	
}

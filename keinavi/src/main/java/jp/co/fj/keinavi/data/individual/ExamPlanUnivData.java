/*
 * 作成日: 2004/11/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ExamPlanUnivData {
	
	private String individualId;
	private String univCd;
	private String facultyCd;
	private String deptCd;
	private String entExamModeCd;
	private String entExamDiv1;
	private String entExamDiv2;
	private String entExamDiv3;
	private String year;
	private String examDiv;
	private String remarks;
	private String examPlanDate1;
	private String examPlanDate2;
	private String examPlanDate3;
	private String examPlanDate4;
	private String examPlanDate5;
	private String examPlanDate6;
	private String examPlanDate7;
	private String examPlanDate8;
	private String examPlanDate9;
	private String examPlanDate10;
	private String provEntExamSite1;
	private String provEntExamSite2;
	private String provEntExamSite3;
	private String provEntExamSite4;
	private String provEntExamSite5;
	private String provEntExamSite6;
	private String provEntExamSite7;
	private String provEntExamSite8;
	private String provEntExamSite9;
	private String provEntExamSite10;

	/**
	 * 大学・学部・学科・入試形態の組み合わせキー
	 * @return
	 */
	public String getUFDExamModeKey(){
		return getUFDKey()+entExamModeCd;
	}

	/**
	 * 大学・学部・学科の組み合わせキー
	 * @return
	 */
	public String getUFDKey(){
		return univCd+facultyCd+deptCd;
	}

	/**
	 * 受験予定日のあるものだけを配列にして返す
	 * @return
	 */
	public String[] getExamPlanDateArray(){
		List list = new ArrayList();
		if(examPlanDate1 != null && examPlanDate1.trim().length() > 0) list.add(examPlanDate1);
		if(examPlanDate2 != null && examPlanDate2.trim().length() > 0) list.add(examPlanDate2);
		if(examPlanDate3 != null && examPlanDate3.trim().length() > 0) list.add(examPlanDate3);
		if(examPlanDate4 != null && examPlanDate4.trim().length() > 0) list.add(examPlanDate4);
		if(examPlanDate5 != null && examPlanDate5.trim().length() > 0) list.add(examPlanDate5);
		if(examPlanDate6 != null && examPlanDate6.trim().length() > 0) list.add(examPlanDate6);
		if(examPlanDate7 != null && examPlanDate7.trim().length() > 0) list.add(examPlanDate7);
		if(examPlanDate8 != null && examPlanDate8.trim().length() > 0) list.add(examPlanDate8);
		if(examPlanDate9 != null && examPlanDate9.trim().length() > 0) list.add(examPlanDate9);
		if(examPlanDate10 != null && examPlanDate10.trim().length() > 0) list.add(examPlanDate10);
		
		if(list.size() > 0){
			return (String[]) list.toArray(new String[list.size()]);
		}else{
			return null;
		}
		
	}
	
	/**
	 * 受験予定地にあるものだけを配列にして返す
	 * @return
	 */
	public String[] getProvEntExamSiteArray(){
		List list = new ArrayList();
		if(provEntExamSite1 != null && provEntExamSite1.trim().length() > 0) list.add(provEntExamSite1);
		if(provEntExamSite2 != null && provEntExamSite2.trim().length() > 0) list.add(provEntExamSite2);
		if(provEntExamSite3 != null && provEntExamSite3.trim().length() > 0) list.add(provEntExamSite3);
		if(provEntExamSite4 != null && provEntExamSite4.trim().length() > 0) list.add(provEntExamSite4);
		if(provEntExamSite5 != null && provEntExamSite5.trim().length() > 0) list.add(provEntExamSite5);
		if(provEntExamSite6 != null && provEntExamSite6.trim().length() > 0) list.add(provEntExamSite6);
		if(provEntExamSite7 != null && provEntExamSite7.trim().length() > 0) list.add(provEntExamSite7);
		if(provEntExamSite8 != null && provEntExamSite8.trim().length() > 0) list.add(provEntExamSite8);
		if(provEntExamSite9 != null && provEntExamSite9.trim().length() > 0) list.add(provEntExamSite9);
		if(provEntExamSite10 != null && provEntExamSite10.trim().length() > 0) list.add(provEntExamSite10);
		
		if(list.size() > 0){
			return (String[]) list.toArray(new String[list.size()]);
		}else{
			return null;
		}
	}
	
	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getEntExamDiv1() {
		return entExamDiv1;
	}

	/**
	 * @return
	 */
	public String getEntExamDiv2() {
		return entExamDiv2;
	}

	/**
	 * @return
	 */
	public String getEntExamDiv3() {
		return entExamDiv3;
	}

	/**
	 * @return
	 */
	public String getExamDiv() {
		return examDiv;
	}

	/**
	 * @return
	 */
	public String getExamPlanDate1() {
		return examPlanDate1;
	}

	/**
	 * @return
	 */
	public String getExamPlanDate10() {
		return examPlanDate10;
	}

	/**
	 * @return
	 */
	public String getExamPlanDate2() {
		return examPlanDate2;
	}

	/**
	 * @return
	 */
	public String getExamPlanDate3() {
		return examPlanDate3;
	}

	/**
	 * @return
	 */
	public String getExamPlanDate4() {
		return examPlanDate4;
	}

	/**
	 * @return
	 */
	public String getExamPlanDate5() {
		return examPlanDate5;
	}

	/**
	 * @return
	 */
	public String getExamPlanDate6() {
		return examPlanDate6;
	}

	/**
	 * @return
	 */
	public String getExamPlanDate7() {
		return examPlanDate7;
	}

	/**
	 * @return
	 */
	public String getExamPlanDate8() {
		return examPlanDate8;
	}

	/**
	 * @return
	 */
	public String getExamPlanDate9() {
		return examPlanDate9;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @return
	 */
	public String getProvEntExamSite1() {
		return provEntExamSite1;
	}

	/**
	 * @return
	 */
	public String getProvEntExamSite10() {
		return provEntExamSite10;
	}

	/**
	 * @return
	 */
	public String getProvEntExamSite2() {
		return provEntExamSite2;
	}

	/**
	 * @return
	 */
	public String getProvEntExamSite3() {
		return provEntExamSite3;
	}

	/**
	 * @return
	 */
	public String getProvEntExamSite4() {
		return provEntExamSite4;
	}

	/**
	 * @return
	 */
	public String getProvEntExamSite5() {
		return provEntExamSite5;
	}

	/**
	 * @return
	 */
	public String getProvEntExamSite6() {
		return provEntExamSite6;
	}

	/**
	 * @return
	 */
	public String getProvEntExamSite7() {
		return provEntExamSite7;
	}

	/**
	 * @return
	 */
	public String getProvEntExamSite8() {
		return provEntExamSite8;
	}

	/**
	 * @return
	 */
	public String getProvEntExamSite9() {
		return provEntExamSite9;
	}

	/**
	 * @return
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @return
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setEntExamDiv1(String string) {
		entExamDiv1 = string;
	}

	/**
	 * @param string
	 */
	public void setEntExamDiv2(String string) {
		entExamDiv2 = string;
	}

	/**
	 * @param string
	 */
	public void setEntExamDiv3(String string) {
		entExamDiv3 = string;
	}

	/**
	 * @param string
	 */
	public void setExamDiv(String string) {
		examDiv = string;
	}

	/**
	 * @param string
	 */
	public void setExamPlanDate1(String string) {
		examPlanDate1 = string;
	}

	/**
	 * @param string
	 */
	public void setExamPlanDate10(String string) {
		examPlanDate10 = string;
	}

	/**
	 * @param string
	 */
	public void setExamPlanDate2(String string) {
		examPlanDate2 = string;
	}

	/**
	 * @param string
	 */
	public void setExamPlanDate3(String string) {
		examPlanDate3 = string;
	}

	/**
	 * @param string
	 */
	public void setExamPlanDate4(String string) {
		examPlanDate4 = string;
	}

	/**
	 * @param string
	 */
	public void setExamPlanDate5(String string) {
		examPlanDate5 = string;
	}

	/**
	 * @param string
	 */
	public void setExamPlanDate6(String string) {
		examPlanDate6 = string;
	}

	/**
	 * @param string
	 */
	public void setExamPlanDate7(String string) {
		examPlanDate7 = string;
	}

	/**
	 * @param string
	 */
	public void setExamPlanDate8(String string) {
		examPlanDate8 = string;
	}

	/**
	 * @param string
	 */
	public void setExamPlanDate9(String string) {
		examPlanDate9 = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

	/**
	 * @param string
	 */
	public void setProvEntExamSite1(String string) {
		provEntExamSite1 = string;
	}

	/**
	 * @param string
	 */
	public void setProvEntExamSite10(String string) {
		provEntExamSite10 = string;
	}

	/**
	 * @param string
	 */
	public void setProvEntExamSite2(String string) {
		provEntExamSite2 = string;
	}

	/**
	 * @param string
	 */
	public void setProvEntExamSite3(String string) {
		provEntExamSite3 = string;
	}

	/**
	 * @param string
	 */
	public void setProvEntExamSite4(String string) {
		provEntExamSite4 = string;
	}

	/**
	 * @param string
	 */
	public void setProvEntExamSite5(String string) {
		provEntExamSite5 = string;
	}

	/**
	 * @param string
	 */
	public void setProvEntExamSite6(String string) {
		provEntExamSite6 = string;
	}

	/**
	 * @param string
	 */
	public void setProvEntExamSite7(String string) {
		provEntExamSite7 = string;
	}

	/**
	 * @param string
	 */
	public void setProvEntExamSite8(String string) {
		provEntExamSite8 = string;
	}

	/**
	 * @param string
	 */
	public void setProvEntExamSite9(String string) {
		provEntExamSite9 = string;
	}

	/**
	 * @param string
	 */
	public void setRemarks(String string) {
		remarks = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

	/**
	 * @param string
	 */
	public void setYear(String string) {
		year = string;
	}

	/**
	 * @return
	 */
	public String getEntExamModeCd() {
		return entExamModeCd;
	}

	/**
	 * @param string
	 */
	public void setEntExamModeCd(String string) {
		entExamModeCd = string;
	}

}

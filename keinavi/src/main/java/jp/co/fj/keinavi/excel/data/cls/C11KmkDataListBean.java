package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−偏差値分布 型・科目別データリスト
 * 作成日: 2004/08/06
 * @author	A.Iwata
 */
public class C11KmkDataListBean {
	//型・科目名コード
	private String strKmkCd = "";
	//型・科目名
	private String strKmkmei = "";
	//配点
	private String strHaitenKmk = "";
	//型・科目グラフ表示フラグ
	private int intDispKmkFlg = 0;
	//人数・クラス
	private int intNinzuCls = 0;
	//人数・校内
	private int intNinzuHome = 0;
	//平均点・クラス
	private float floHeikinCls = 0;
	//平均点・校内
	private float floHeikinHome = 0;
	//平均偏差値・クラス
	private float floHensaCls = 0;
	//平均偏差値・校内
	private float floHensaHome = 0;
	//偏差値分布リスト
	private ArrayList c11BnpList = new ArrayList();	
	
	/*----------*/
	/* Get      */
	/*----------*/

	public ArrayList getC11BnpList() {
		return this.c11BnpList;
	}
	public float getFloHeikinCls() {
		return this.floHeikinCls;
	}
	public float getFloHeikinHome() {
		return this.floHeikinHome;
	}
	public float getFloHensaCls() {
		return this.floHensaCls;
	}
	public float getFloHensaHome() {
		return this.floHensaHome;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public int getIntDispKmkFlg() {
		return this.intDispKmkFlg;
	}
	public int getIntNinzuCls() {
		return this.intNinzuCls;
	}
	public int getIntNinzuHome() {
		return this.intNinzuHome;
	}
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setC11BnpList(ArrayList c11BnpList) {
		this.c11BnpList = c11BnpList;
	}
	public void setFloHeikinCls(float floHeikinCls) {
		this.floHeikinCls = floHeikinCls;
	}
	public void setFloHeikinHome(float floHeikinHome) {
		this.floHeikinHome = floHeikinHome;
	}
	public void setFloHensaCls(float floHensaCls) {
		this.floHensaCls = floHensaCls;
	}
	public void setFloHensaHome(float floHensaHome) {
		this.floHensaHome = floHensaHome;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setIntDispKmkFlg(int intDispKmkFlg) {
		this.intDispKmkFlg = intDispKmkFlg;
	}
	public void setIntNinzuCls(int intNinzuCls) {
		this.intNinzuCls = intNinzuCls;
	}
	public void setIntNinzuHome(int intNinzuHome) {
		this.intNinzuHome = intNinzuHome;
	}
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
}
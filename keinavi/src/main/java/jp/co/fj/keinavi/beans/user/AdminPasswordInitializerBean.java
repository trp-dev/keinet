package jp.co.fj.keinavi.beans.user;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 管理者パスワード初期化Bean
 * 
 * <2010年度マーク高２模試対応>
 * 2011.02.28	Tomohisa YAMADA - TOTEC
 * 				[新規作成]
 * 
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class AdminPasswordInitializerBean extends DefaultBean {

	// 利用者ID
	private final String userId;
	
	/**
	 * コンストラクタ
	 */
	public AdminPasswordInitializerBean(final String userId) {
		
		if (userId == null) {
			throw new IllegalArgumentException("利用者IDがNULLです。");
		}
		
		this.userId = userId;
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("u11").toString());

			//利用者ID
			ps.setString(1, userId);
			
			if (ps.executeUpdate() > 1) {
				throw new Exception("管理者が複数人存在しています。");
			}
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
}

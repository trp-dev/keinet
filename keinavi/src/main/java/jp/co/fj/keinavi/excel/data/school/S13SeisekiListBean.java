package jp.co.fj.keinavi.excel.data.school;

/**
 * 設問別成績（校内成績）設問別成績データリスト
 * 作成日: 2004/07/06
 * @author	T.Kuzuno
 * 2009.10.14 Fujito URAKAWA - Totec
 *            「成績層に"S"」追加対応
 */
public class S13SeisekiListBean {
	//科目コード
	private String strKmkCd = "";
	//科目名
	private String strKmkmei = "";
	//型・科目用グラフ表示フラグ
	private int intDispKmkFlg= 0;	
	//配点
	private String strHaitenKmk = "";
	//設問番号
	private String strSetsuNo = "";
	//設問内容
	private String strSetsuMei1 = "";
	//設問配点
	private String strSetsuHaiten = "";
	//人数・校内
	private int intNinzuHome= 0;	
	//得点率・校内
	private float floTokuritsuHome = 0;
	//人数・県
	private int intNinzuKen= 0;	
	//得点率・県
	private float floTokuritsuKen = 0;
	//人数・全国
	private int intNinzuAll= 0;	
	//得点率・全国
	private float floTokuritsuAll = 0;
	//人数・成績層S
	private int intNinzuHomeS= 0;	
	//得点率・成績層S
	private float floTokuritsuHomeS = 0;
	//人数・成績層A
	private int intNinzuHomeA= 0;	
	//得点率・成績層A
	private float floTokuritsuHomeA = 0;
	//人数・成績層B
	private int intNinzuHomeB= 0;	
	//得点率・成績層B
	private float floTokuritsuHomeB = 0;
	//人数・成績層C
	private int intNinzuHomeC= 0;	
	//得点率・成績層C
	private float floTokuritsuHomeC = 0;
	//人数・成績層D
	private int intNinzuHomeD= 0;	
	//得点率・成績層D
	private float floTokuritsuHomeD = 0;
	//人数・成績層E
	private int intNinzuHomeE= 0;	
	//得点率・成績層E
	private float floTokuritsuHomeE = 0;
	//人数・成績層F
	private int intNinzuHomeF= 0;	
	//得点率・成績層F
	private float floTokuritsuHomeF = 0;
	
	// 人数（全国・成績層S）
	private int numADevzoneS;
	// 得点率（全国・成績層S）
	private float avgrateADevzoneS;
	// 人数（全国・成績層A）
	private int numADevzoneA;
	// 得点率（全国・成績層A）
	private float avgrateADevzoneA;
	// 人数（全国・成績層B）
	private int numADevzoneB;
	// 得点率（全国・成績層B）
	private float avgrateADevzoneB;
	// 人数（全国・成績層C）
	private int numADevzoneC;
	// 得点率（全国・成績層C）
	private float avgrateADevzoneC;
	// 人数（全国・成績層D）
	private int numADevzoneD;
	// 得点率（全国・成績層D）
	private float avgrateADevzoneD;
	// 人数（全国・成績層E）
	private int numADevzoneE;
	// 得点率（全国・成績層E）
	private float avgrateADevzoneE;
	// 人数（全国・成績層F）
	private int numADevzoneF;
	// 得点率（全国・成績層F）
	private float avgrateADevzoneF;
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}
	public int getIntDispKmkFlg() {
		return this.intDispKmkFlg;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public String getStrSetsuNo() {
		return this.strSetsuNo;
	}
	public String getStrSetsuMei1() {
		return this.strSetsuMei1;
	}
	public String getStrSetsuHaiten() {
		return this.strSetsuHaiten;
	}
	public int getIntNinzuHome() {
		return this.intNinzuHome;
	}
	public float getFloTokuritsuHome() {
		return this.floTokuritsuHome;
	}
	public int getIntNinzuKen() {
		return this.intNinzuKen;
	}
	public float getFloTokuritsuKen() {
		return this.floTokuritsuKen;
	}
	public int getIntNinzuAll() {
		return this.intNinzuAll;
	}
	public float getFloTokuritsuAll() {
		return this.floTokuritsuAll;
	}
	public int getIntNinzuHomeA() {
		return this.intNinzuHomeA;
	}
	public float getFloTokuritsuHomeA() {
		return this.floTokuritsuHomeA;
	}
	public int getIntNinzuHomeB() {
		return this.intNinzuHomeB;
	}
	public float getFloTokuritsuHomeB() {
		return this.floTokuritsuHomeB;
	}
	public int getIntNinzuHomeC() {
		return this.intNinzuHomeC;
	}
	public float getFloTokuritsuHomeC() {
		return this.floTokuritsuHomeC;
	}
	public int getIntNinzuHomeD() {
		return this.intNinzuHomeD;
	}
	public float getFloTokuritsuHomeD() {
		return this.floTokuritsuHomeD;
	}
	public int getIntNinzuHomeE() {
		return this.intNinzuHomeE;
	}
	public float getFloTokuritsuHomeE() {
		return this.floTokuritsuHomeE;
	}
	public int getIntNinzuHomeF() {
		return this.intNinzuHomeF;
	}
	public float getFloTokuritsuHomeF() {
		return this.floTokuritsuHomeF;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setIntDispKmkFlg(int intDispKmkFlg) {
		this.intDispKmkFlg = intDispKmkFlg;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setStrSetsuNo(String strSetsuNo) {
		this.strSetsuNo = strSetsuNo;
	}
	public void setStrSetsuMei1(String strSetsuMei1) {
		this.strSetsuMei1 = strSetsuMei1;
	}
	public void setStrSetsuHaiten(String strSetsuHaiten) {
		this.strSetsuHaiten = strSetsuHaiten;
	}
	public void setIntNinzuHome(int intNinzuHome) {
		this.intNinzuHome = intNinzuHome;
	}
	public void setFloTokuritsuHome(float floTokuritsuHome) {
		this.floTokuritsuHome = floTokuritsuHome;
	}
	public void setIntNinzuKen(int intNinzuKen) {
		this.intNinzuKen = intNinzuKen;
	}
	public void setFloTokuritsuKen(float floTokuritsuKen) {
		this.floTokuritsuKen = floTokuritsuKen;
	}
	public void setIntNinzuAll(int intNinzuAll) {
		this.intNinzuAll = intNinzuAll;
	}
	public void setFloTokuritsuAll(float floTokuritsuAll) {
		this.floTokuritsuAll = floTokuritsuAll;
	}
	public void setIntNinzuHomeA(int intNinzuHomeA) {
		this.intNinzuHomeA = intNinzuHomeA;
	}
	public void setFloTokuritsuHomeA(float floTokuritsuHomeA) {
		this.floTokuritsuHomeA = floTokuritsuHomeA;
	}
	public void setIntNinzuHomeB(int intNinzuHomeB) {
		this.intNinzuHomeB = intNinzuHomeB;
	}
	public void setFloTokuritsuHomeB(float floTokuritsuHomeB) {
		this.floTokuritsuHomeB = floTokuritsuHomeB;
	}
	public void setIntNinzuHomeC(int intNinzuHomeC) {
		this.intNinzuHomeC = intNinzuHomeC;
	}
	public void setFloTokuritsuHomeC(float floTokuritsuHomeC) {
		this.floTokuritsuHomeC = floTokuritsuHomeC;
	}
	public void setIntNinzuHomeD(int intNinzuHomeD) {
		this.intNinzuHomeD = intNinzuHomeD;
	}
	public void setFloTokuritsuHomeD(float floTokuritsuHomeD) {
		this.floTokuritsuHomeD = floTokuritsuHomeD;
	}
	public void setIntNinzuHomeE(int intNinzuHomeE) {
		this.intNinzuHomeE = intNinzuHomeE;
	}
	public void setFloTokuritsuHomeE(float floTokuritsuHomeE) {
		this.floTokuritsuHomeE = floTokuritsuHomeE;
	}
	public void setIntNinzuHomeF(int intNinzuHomeF) {
		this.intNinzuHomeF = intNinzuHomeF;
	}
	public void setFloTokuritsuHomeF(float floTokuritsuHomeF) {
		this.floTokuritsuHomeF = floTokuritsuHomeF;
	}
	/**
	 * @return avgrateADevzoneA
	 */
	public float getAvgrateADevzoneA() {
		return avgrateADevzoneA;
	}
	/**
	 * @param pAvgrateADevzoneA 設定する avgrateADevzoneA
	 */
	public void setAvgrateADevzoneA(float pAvgrateADevzoneA) {
		avgrateADevzoneA = pAvgrateADevzoneA;
	}
	/**
	 * @return avgrateADevzoneB
	 */
	public float getAvgrateADevzoneB() {
		return avgrateADevzoneB;
	}
	/**
	 * @param pAvgrateADevzoneB 設定する avgrateADevzoneB
	 */
	public void setAvgrateADevzoneB(float pAvgrateADevzoneB) {
		avgrateADevzoneB = pAvgrateADevzoneB;
	}
	/**
	 * @return avgrateADevzoneC
	 */
	public float getAvgrateADevzoneC() {
		return avgrateADevzoneC;
	}
	/**
	 * @param pAvgrateADevzoneC 設定する avgrateADevzoneC
	 */
	public void setAvgrateADevzoneC(float pAvgrateADevzoneC) {
		avgrateADevzoneC = pAvgrateADevzoneC;
	}
	/**
	 * @return avgrateADevzoneD
	 */
	public float getAvgrateADevzoneD() {
		return avgrateADevzoneD;
	}
	/**
	 * @param pAvgrateADevzoneD 設定する avgrateADevzoneD
	 */
	public void setAvgrateADevzoneD(float pAvgrateADevzoneD) {
		avgrateADevzoneD = pAvgrateADevzoneD;
	}
	/**
	 * @return avgrateADevzoneE
	 */
	public float getAvgrateADevzoneE() {
		return avgrateADevzoneE;
	}
	/**
	 * @param pAvgrateADevzoneE 設定する avgrateADevzoneE
	 */
	public void setAvgrateADevzoneE(float pAvgrateADevzoneE) {
		avgrateADevzoneE = pAvgrateADevzoneE;
	}
	/**
	 * @return avgrateADevzoneF
	 */
	public float getAvgrateADevzoneF() {
		return avgrateADevzoneF;
	}
	/**
	 * @param pAvgrateADevzoneF 設定する avgrateADevzoneF
	 */
	public void setAvgrateADevzoneF(float pAvgrateADevzoneF) {
		avgrateADevzoneF = pAvgrateADevzoneF;
	}
	/**
	 * @return numADevzoneA
	 */
	public int getNumADevzoneA() {
		return numADevzoneA;
	}
	/**
	 * @param pNumADevzoneA 設定する numADevzoneA
	 */
	public void setNumADevzoneA(int pNumADevzoneA) {
		numADevzoneA = pNumADevzoneA;
	}
	/**
	 * @return numADevzoneB
	 */
	public int getNumADevzoneB() {
		return numADevzoneB;
	}
	/**
	 * @param pNumADevzoneB 設定する numADevzoneB
	 */
	public void setNumADevzoneB(int pNumADevzoneB) {
		numADevzoneB = pNumADevzoneB;
	}
	/**
	 * @return numADevzoneC
	 */
	public int getNumADevzoneC() {
		return numADevzoneC;
	}
	/**
	 * @param pNumADevzoneC 設定する numADevzoneC
	 */
	public void setNumADevzoneC(int pNumADevzoneC) {
		numADevzoneC = pNumADevzoneC;
	}
	/**
	 * @return numADevzoneD
	 */
	public int getNumADevzoneD() {
		return numADevzoneD;
	}
	/**
	 * @param pNumADevzoneD 設定する numADevzoneD
	 */
	public void setNumADevzoneD(int pNumADevzoneD) {
		numADevzoneD = pNumADevzoneD;
	}
	/**
	 * @return numADevzoneE
	 */
	public int getNumADevzoneE() {
		return numADevzoneE;
	}
	/**
	 * @param pNumADevzoneE 設定する numADevzoneE
	 */
	public void setNumADevzoneE(int pNumADevzoneE) {
		numADevzoneE = pNumADevzoneE;
	}
	/**
	 * @return numADevzoneF
	 */
	public int getNumADevzoneF() {
		return numADevzoneF;
	}
	/**
	 * @param pNumADevzoneF 設定する numADevzoneF
	 */
	public void setNumADevzoneF(int pNumADevzoneF) {
		numADevzoneF = pNumADevzoneF;
	}
	public int getIntNinzuHomeS() {
		return intNinzuHomeS;
	}
	public void setIntNinzuHomeS(int intNinzuHomeS) {
		this.intNinzuHomeS = intNinzuHomeS;
	}
	public float getFloTokuritsuHomeS() {
		return floTokuritsuHomeS;
	}
	public void setFloTokuritsuHomeS(float floTokuritsuHomeS) {
		this.floTokuritsuHomeS = floTokuritsuHomeS;
	}
	public int getNumADevzoneS() {
		return numADevzoneS;
	}
	public float getAvgrateADevzoneS() {
		return avgrateADevzoneS;
	}
	public void setNumADevzoneS(int numADevzoneS) {
		this.numADevzoneS = numADevzoneS;
	}
	public void setAvgrateADevzoneS(float avgrateADevzoneS) {
		this.avgrateADevzoneS = avgrateADevzoneS;
	}
}

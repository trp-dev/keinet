package jp.co.fj.keinavi.servlets.com_set;

import java.io.IOException;
import java.sql.Connection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.PrefBean;
import jp.co.fj.keinavi.beans.com_set.SchoolSearchBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM601Form;
import jp.co.fj.keinavi.forms.com_set.CM604Form;
import jp.co.fj.keinavi.util.CollectionUtil;

/**
 * 共通項目設定 - 比較対象高校（検索結果）サーブレット
 * 
 * 2005.02.05 Yoshimoto KAWAI - Totec
 *            検索対象として自校を除く対応のために一括コードを
 *         　 Beanのパラメータに追加
 * 
 * @author kawai
 * 
 */
public class CM604Servlet extends AbstractCMServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 共通処理
		super.execute(request, response);

		// ログイン情報
		LoginSession login = super.getLoginSession(request);

		// アクションフォーム - request scope
		CM001Form rcform = (CM001Form)super.getActionForm(request,
			"jp.co.fj.keinavi.forms.com_set.CM001Form");;
		CM604Form riform = (CM604Form)super.getActionForm(request,
			"jp.co.fj.keinavi.forms.com_set.CM604Form");

		// アクションフォーム - session scope
		CM001Form scform = super.getCommonForm(request);
		CM604Form siform = (CM604Form)scform.getActionForm("CM604Form");
		CM601Form f601 = (CM601Form)scform.getActionForm("CM601Form");

		// 検索フォームからの遷移
		if ("cm603".equals(getBackward(request))) {
			// はじめは１ページ
			siform.setPage("1");
			// チェックもクリア
			siform.setSchool(null);
			// 検索条件をセッションに保持する
			siform.setPref(riform.getPref());
			siform.setBlock(riform.getBlock());
			siform.setSchoolDiv(riform.getSchoolDiv());
			siform.setRank(riform.getRank());
			siform.setExaminees(riform.getExaminees());
			siform.setView(riform.getView());
			siform.setSearchMode(riform.getSearchMode());
			siform.setSearchStr(riform.getSearchStr());
			siform.setButton(riform.getButton());
		} else {
			// 志望大学コードの追加／削除処理
			String[] master = riform.getMaster();
			// 戻るボタンでの遷移
			if (master == null) {
				
			// それ以外
			} else {
				siform.setPage(riform.getPage());
				// requestで渡された志望大学コード
				Set rSchool = CollectionUtil.array2Set(riform.getSchool());
				// sessionに保存されていた志望大学コード
				List sSchool = CollectionUtil.array2List(siform.getSchool());
				for (int i=0; i<master.length; i++) {
					// チェックされている場合
					if (rSchool.contains(master[i])) {
						// なければ追加する
						if (!sSchool.contains(master[i])) {
							sSchool.add(master[i]);
						}
					// チェックされていない場合
					} else {
						sSchool.remove(master[i]);
					}
				}
				siform.setSchool((String[])sSchool.toArray(new String[0]));
			}
			// 変更フラグ
			scform.setChanged(rcform.getChanged());
		}
		
		// JSP
		if ("cm604".equals(getForward(request))) {
			// プロファイル
			Profile profile = super.getProfile(request);
			
			// DBコネクション
			Connection con = null;
			try {
				con = super.getConnectionPool(request);

				// 都道府県Bean
				PrefBean pref = PrefBean.getInstance(con);
				request.setAttribute("PrefBean",pref);

				// 高校検索Bean
				SchoolSearchBean bean = new SchoolSearchBean();
				bean.setConnection(null, con);
				bean.setBundleCd(profile.getBundleCD());
				bean.setTargetYear(scform.getTargetYear());
				bean.setTargetExam(scform.getTargetExam());
				bean.setButton(siform.getButton());
				bean.setPref(siform.getPref());
				bean.setSchoolDiv(siform.getSchoolDiv());
				bean.setRank(siform.getRank());
				bean.setExaminees(siform.getExaminees());
				bean.setView(siform.getView());
				bean.setSearchMode(siform.getSearchMode());
				bean.setSearchStr(siform.getSearchStr());
				bean.setRegistedSchool(f601.getSchool());
				bean.setSales(login.isSales());
				bean.setPageSize(20); // ページサイズは20で固定
				bean.setPageNo(Integer.parseInt(siform.getPage())); // ページ番号
				bean.execute();

				request.setAttribute("SearchBean", bean);
			} catch (Exception e) {
				throw new ServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}
			
			request.setAttribute("HiddenCodeMap", hiddenCodeMap);
			super.forward(request, response, JSP_CM604);

		// 不明なら転送
		} else {
			// 登録処理
			if ("1".equals(riform.getSave())) {
				List container = new LinkedList();
				// 元々選択済みの学校コード
				container.addAll(CollectionUtil.array2List(f601.getSchool()));
				// 今回選択された学校コード
				container.addAll(CollectionUtil.array2List(siform.getSchool()));
				// 再設定する
				f601.setSchool((String[]) container.toArray(new String[0]));				
			}
			
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

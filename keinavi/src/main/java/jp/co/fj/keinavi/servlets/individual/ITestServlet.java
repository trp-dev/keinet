/*
 * 作成日: 2004/06/30
 *
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Totec) T.Yamada
 *
 * 個人テスト用サーブレット
 * 中身はなんなりと書き換えて構わない
 */
public class ITestServlet extends IndividualServlet{
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		
		
		
		System.out.println("This is KeiNavi. Not MyKei-Naivi!");
	}
}

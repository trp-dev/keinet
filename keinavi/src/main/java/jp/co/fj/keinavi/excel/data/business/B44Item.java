/*
 * 作成日: 2004/09/06
 *
 * 高校成績分析−志望大学評価別人数（高校間比較）
 * @author cmurata
 */
package jp.co.fj.keinavi.excel.data.business;

import java.util.ArrayList;

public class B44Item {
	
//	//学校名
//	private String strGakkomei = "";
	//模試コード
	private String strMshCd = "";
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//大学集計区分
	private int intDaiTotalFlg = 0;
	//セキュリティスタンプ
	private int intSecuFlg = 0;
	//データリスト
	private ArrayList b44List = new ArrayList(); 
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;


	/**
	 * @GET
	 */
	public ArrayList getB44List() {
		return this.b44List;
	}

	public int getIntDaiTotalFlg() {
		return this.intDaiTotalFlg;
	}

	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}

//	public String getStrGakkomei() {
//		return this.strGakkomei;
//	}

	public String getStrMshCd() {
		return this.strMshCd;
	}

	public String getStrMshDate() {
		return this.strMshDate;
	}

	public String getStrMshmei() {
		return this.strMshmei;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/**
	 * @SET
	 */
	public void setB44List(ArrayList b44List) {
		this.b44List = b44List;
	}

	public void setIntDaiTotalFlg(int intDaiTotalFlg) {
		this.intDaiTotalFlg = intDaiTotalFlg;
	}

	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}

//	public void setStrGakkomei(String strGakkomei) {
//		this.strGakkomei = strGakkomei;
//	}

	public void setStrMshCd(String strMshCd) {
		this.strMshCd = strMshCd;
	}	

	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}

	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}

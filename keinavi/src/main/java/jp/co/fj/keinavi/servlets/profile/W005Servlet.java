package jp.co.fj.keinavi.servlets.profile;

import java.io.IOException;
import java.sql.Connection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import jp.co.fj.keinavi.beans.com_set.ChargeClassBean;
import jp.co.fj.keinavi.beans.profile.CountingDivBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.com_set.ChargeClassData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.profile.W005Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * 
 * 
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 * 
 * @author kawai
 *
 */
public class W005Servlet extends DefaultHttpServlet {

	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// ログイン情報
		LoginSession login = super.getLoginSession(request);
		// プロファイル
		Profile profile = super.getProfile(request);
		// 担当クラスリスト
		List container = (List) profile.getItemMap("010000").get("0501");

		// アクションフォーム
		W005Form form = (W005Form) super.getActionForm(request,
				"jp.co.fj.keinavi.forms.profile.W005Form");

		// 年度の初期値
		if (form.getYear() == null) {
			form.setYear(ProfileUtil.getChargeClassYear(profile));
		// あるなら上書き
		} else {
			profile.getItemMap(IProfileCategory.CM).put(IProfileItem.CLASS_YEAR, form.getYear());
		}

		// 転送元がw005ならプロファイル更新
		if ("w005".equals(super.getBackward(request))) {
			// クリア
			container.clear();

			if (form.getClasses() != null) {
				// 詰める
				final String[] classes = form.getClasses();
				for (int i=0; i<classes.length; i++) {
					final String[] values = StringUtils.split(classes[i], ',');
					container.add(new ChargeClassData(values[1], values[2]));
				}

				// 年度
				profile
					.getItemMap(IProfileCategory.CM)
					.put(IProfileItem.CLASS_YEAR, form.getYear());
			}
		}
		
		// JSP
		if ("w005".equals(super.getForward(request))) {
			// DBコネクション
			Connection con = null;
			try {
				con = super.getConnectionPool(request);

				// 集計区分Beanを作る
				if (login.isCountingDiv()) {
					CountingDivBean c = new CountingDivBean();
					c.setConnection(null, con);
					c.setSchoolCD(login.getUserID());
					c.execute();
					request.setAttribute("CountingDivBean", c);
				}

				// 担当クラスBean
				ChargeClassBean bean =
						new ChargeClassBean(login.getUserID(), login.getAccount());
				bean.setConnection(null, con);
				bean.execute();
				request.setAttribute("ClassBean", bean);

				// キーを入れる
				List c = new LinkedList();
				Iterator ite = container.iterator();
				while (ite.hasNext()) {
					final ChargeClassData data = (ChargeClassData) ite.next();		
					c.add(form.getYear() + "," + data.getGrade() + "," + data.getClassName());
				}
				form.setClasses((String[]) c.toArray(new String[0]));
					
			} catch (Exception e) {
				throw new ServletException(e.getMessage());
			} finally {
				super.releaseConnectionPool(request, con);
			}

			request.setAttribute("form", form);
			request.setAttribute("CheckedMap", CollectionUtil.array2TrueMap(form.getClasses()));
			
			super.forward(request, response, JSP_W005);

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

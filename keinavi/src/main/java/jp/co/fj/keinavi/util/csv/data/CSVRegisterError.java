package jp.co.fj.keinavi.util.csv.data;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 *
 * CSV登録エラーデータ
 * 
 * 2005.10.25	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class CSVRegisterError {

	// エラー発生行番号
	private final int lineNo;
	// エラー発生項目名（列名）
	private final String errorItemName;	
	// エラー内容
	private final String errorContent;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pLineNo エラー発生行番号
	 * @param pErrorItemName エラー発生項目名（列名）
	 * @param pErrorContent エラー内容
	 */
	CSVRegisterError(final int pLineNo, final String pErrorItemName,
			final String pErrorContent) {
		
		this.lineNo = pLineNo;
		this.errorItemName = pErrorItemName;
		this.errorContent = pErrorContent;
	}

	/**
	 * コンストラクタ
	 * 
	 * @param pLineNo エラー発生行番号
	 * @param pErrorContent エラー内容
	 */
	CSVRegisterError(final int pLineNo, final String pErrorContent) {
		
		this.lineNo = pLineNo;
		this.errorItemName = null;
		this.errorContent = pErrorContent;
	}

	/**
	 * @return errorContent を戻します。
	 */
	public String getErrorContent() {
		return errorContent;
	}

	/**
	 * @return errorItemName を戻します。
	 */
	public String getErrorItemName() {
		return errorItemName;
	}

	/**
	 * @return lineNo を戻します。
	 */
	public int getLineNo() {
		return lineNo;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return
				new ToStringBuilder(this)
						.append("LineNo", lineNo)
						.append("Name", errorItemName)
						.append("Content", errorContent)
						.toString();
	}
}

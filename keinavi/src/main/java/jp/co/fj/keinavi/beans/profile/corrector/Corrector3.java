/*
 * 作成日: 2005/02/09
 */
package jp.co.fj.keinavi.beans.profile.corrector;

import java.util.List;
import java.util.Map;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;

/**
 * バージョン２→３
 * 担当クラスの年度対応
 * 
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */
public class Corrector3 implements ICorrector {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.profile.corrector.ICorrector#execute(jp.co.fj.keinavi.data.profile.Profile)
	 */
	public void execute(final Profile profile, final LoginSession login) {
		Map item = profile.getItemMap(CM);
		// 担当クラスの設定があれば2004年度を入れる
		if (item.containsKey(CLASS) && ((List) item.get(CLASS)).size() > 0) {
			item.put(CLASS_YEAR, "2004");
		}
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.profile.corrector.ICorrector#getVersion()
	 */
	public short getVersion() {
		return 3;
	}

}

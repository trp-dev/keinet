package jp.co.fj.keinavi.forms.individual;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 *
 * σ±\θεwσβζΚActionForm
 * 
 * 2004.08.10	[VKμ¬]
 * 2005.04.26	K.Kondo		[1]ζΚXN[ΐWΜΗΑ
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class I301Form extends BaseForm {

	// ΞΫΒlID
	private String targetPersonId;

	// {^νΚ
	private String button;

	// o^ΞΫiΝΕLό³κ½εwj
	private String[] univValueAtExam;

	// o^ΞΫi»θπj
	private String[] univValueAtJudge;

	// νΞΫ
	private String[] univDeleteValue;

	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @return
	 */
	public String[] getUnivValueAtExam() {
		return univValueAtExam;
	}

	/**
	 * @return
	 */
	public String[] getUnivValueAtJudge() {
		return univValueAtJudge;
	}

	/**
	 * @param strings
	 */
	public void setUnivValueAtExam(String[] strings) {
		univValueAtExam = strings;
	}

	/**
	 * @param strings
	 */
	public void setUnivValueAtJudge(String[] strings) {
		univValueAtJudge = strings;
	}

	/**
	 * @return
	 */
	public String getButton() {
		return button;
	}

	/**
	 * @param string
	 */
	public void setButton(String string) {
		button = string;
	}

	/**
	 * @return univDeleteValue
	 */
	public String[] getUnivDeleteValue() {
		return univDeleteValue;
	}

	/**
	 * @param pUnivDeleteValue έθ·ι univDeleteValue
	 */
	public void setUnivDeleteValue(String[] pUnivDeleteValue) {
		univDeleteValue = pUnivDeleteValue;
	}
	
}

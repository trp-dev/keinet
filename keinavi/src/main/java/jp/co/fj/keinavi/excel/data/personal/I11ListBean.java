package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
import java.util.ArrayList;
/**
 * 個人成績分析−成績分析面談−連続個人成績表 クラスデータリスト
 * 作成日: 2004/07/20
 * @author	A.Iwata
 */
public class I11ListBean {
	
	//学校名
	private String strGakkomei = "";
	//学年
	private String strGrade = "";
	//クラス名
	private String strClass = "";
	//クラス番号
	private String strClassNum = "";
	//氏名
	private String strShimei = "";
	//カナ氏名
	private String strKana = "";
	//性別
	private String strSex = "";
	//電話番号
	private String strTelNo = "";
	//連続個人成績表データリスト
	private ArrayList i13SeisekiList = new ArrayList();
	//教科別成績推移グラフデータリスト
	private ArrayList i11KyokaSuiiList = new ArrayList();
	//バランスチャートデータリスト
	private ArrayList i11ChartList = new ArrayList();
	//バランスチャート（合格者平均）データリスト
	private ArrayList i11GouChartList = new ArrayList();
	//科目別成績推移グラフデータリスト
	private ArrayList i12KmkSuiiList = new ArrayList();
	//設問別成績グラフデータリスト
	private ArrayList i12SetsumonList = new ArrayList();
	//志望校判定データリスト
	private ArrayList i14HanteiList = new ArrayList();
	//受験大学スケジュール用データリスト
	private ArrayList i15ScheduleList = new ArrayList();

	//志望大学判定帳票において
	//生徒毎に対象模試が異なる場合があるため
	//以下の項目はI11Itemではなく、このクラスに保持する
	//今回模試名
	private String strBaseMshmei = "";
	//今回模試実施基準日
	private String strBaseMshDate = "";
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrGrade() {
		return this.strGrade;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrClassNum() {
		return this.strClassNum;
	}
	public String getStrShimei() {
		return this.strShimei;
	}
	public String getStrKana() {
		return this.strKana;
	}
	public String getStrSex() {
		return this.strSex;
	}
	public String getStrTelNo() {
		return this.strTelNo;
	}
	public ArrayList getI13SeisekiList() {
		return this.i13SeisekiList;
	}
	public ArrayList getI11KyokaSuiiList() {
		return this.i11KyokaSuiiList;
	}
	public ArrayList getI11ChartList() {
		return this.i11ChartList;
	}
	public ArrayList getI11GouChartList() {
		return this.i11GouChartList;
	}
	public ArrayList getI12KmkSuiiList() {
		return this.i12KmkSuiiList;
	}
	public ArrayList getI12SetsumonList() {
		return this.i12SetsumonList;
	}
	public ArrayList getI14HanteiList() {
		return this.i14HanteiList;
	}
	public ArrayList getI15ScheduleList() {
		return this.i15ScheduleList;
	}
	public String getStrBaseMshmei() {
		return this.strBaseMshmei;
	}
	public String getStrBaseMshDate() {
		return this.strBaseMshDate;
	}
		
	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrClassNum(String strClassNum) {
		this.strClassNum = strClassNum;
	}
	public void setStrShimei(String strShimei) {
		this.strShimei = strShimei;
	}
	public void setStrKana(String strKana) {
		this.strKana = strKana;
	}
	public void setStrSex(String strSex) {
		this.strSex = strSex;
	}
	public void setStrTelNo(String strTelNo) {
		this.strTelNo = strTelNo;
	}
	public void setI13SeisekiList(ArrayList i13SeisekiList) {
		this.i13SeisekiList = i13SeisekiList;
	}
	public void setI11KyokaSuiiList(ArrayList i11KyokaSuiiList) {
		this.i11KyokaSuiiList = i11KyokaSuiiList;
	}
	public void setI11ChartList(ArrayList i11ChartList) {
		this.i11ChartList = i11ChartList;
	}
	public void setI11GouChartList(ArrayList i11GouChartList) {
		this.i11GouChartList = i11GouChartList;
	}
	public void setI12KmkSuiiList(ArrayList i12KmkSuiiList) {
		this.i12KmkSuiiList = i12KmkSuiiList;
	}
	public void setI12SetsumonList(ArrayList i12SetsumonList) {
		this.i12SetsumonList = i12SetsumonList;
	}
	public void setI14HanteiList(ArrayList i14HanteiList) {
		this.i14HanteiList = i14HanteiList;
	}
	public void setI15ScheduleList(ArrayList i15ScheduleList) {
		this.i15ScheduleList = i15ScheduleList;
	}
	public void setStrBaseMshmei(String strBaseMshmei) {
		this.strBaseMshmei = strBaseMshmei;
	}
	public void setStrBaseMshDate(String strBaseMshDate) {
		this.strBaseMshDate = strBaseMshDate;
	}
	
}

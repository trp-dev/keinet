package jp.co.fj.keinavi.servlets.admin;

import java.io.IOException;
import java.sql.Connection;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.admin.LastDateBean;
import jp.co.fj.keinavi.beans.recount.RecountChannel;
import jp.co.fj.keinavi.beans.recount.RecountManager;
import jp.co.fj.keinavi.data.maintenance.TempExamineeData;
import jp.co.fj.keinavi.forms.admin.M001Form;
import jp.co.fj.keinavi.servlets.maintenance.AbstractMServlet;

/**
 * 
 * メンテナンストップ画面サーブレット
 * 
 * 2004.11.20 	Yoshimoto KAWAI - Totec
 * 				再集計処理を学校単位で実行するように修正
 * 
 * 2005.04.05	Yoshimoto KAWAI - Totec
 * 				requestにアクションフォームをセットするようにした
 * 
 * @author okumura
 *
 */
public class M001Servlet extends AbstractMServlet implements ActionMode {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// 学校コード
		final String schoolCd = getSchoolCd(request);
		// アクションフォームの取得
		final M001Form form = (M001Form) getActionForm(request,
			"jp.co.fj.keinavi.forms.admin.M001Form");

		// 生徒情報管理のセッション初期化
		if ("w002".equals(getBackward(request))) {
			final HttpSession session = request.getSession(false);
			session.removeAttribute("ScoreExamListBean");
			session.removeAttribute("ClassListBean");
			session.removeAttribute("StudentSearchBean");
			session.removeAttribute("SelectedStudentBean");
			session.removeAttribute("M101Form");
			// 仮受験者データの初期化
			if (getTempExamineeData(request) == null) {
				session.setAttribute("TempExamineeData",
						new TempExamineeData());
			}
		}
		
		// JSP
		if ("m001".equals(getForward(request))) {

			final Map dateData;
			Connection con = null;
			try {
				con = getConnectionPool(request);
				
				// 更新日付の取得
				dateData = LastDateBean.getLastDate(con, schoolCd);

				// クラス再集計処理
				final RecountManager manager = new RecountManager();
				if (form.getActionMode().equals(RECOUNT_MODE)){
					// スレッドに登録
					if (manager.regist(con, schoolCd)) {
						con.commit();
						RecountChannel.getInstance().putRequest(schoolCd);
					}
					// 再集計処理中フラグ
					request.setAttribute("RecountProcess", Boolean.TRUE);
					// アクセスログ
					actionLog(request, "501");
					
				// 再集計処理中かどうか調べる
				} else {
					request.setAttribute("RecountProcess", Boolean.valueOf(
							manager.isProcess(con, schoolCd)));
				}
			} catch (final Exception e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			// 生徒情報管理最終更新日
			request.setAttribute(
				"studentInfLastDate",
				(String) dateData.get("RENEWALDATE1"));
			// 受験届修正
			request.setAttribute(
				"correctionLastDate",
				(String) dateData.get("RENEWALDATE2"));
			// 複合クラス管理
			request.setAttribute(
				"compClassLastDate",
				(String) dateData.get("RENEWALDATE3"));
			// ActionForm
			request.setAttribute("form", form);
			
			forward(request, response, JSP_M001);

		// 不明なら転送
		} else {
			forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

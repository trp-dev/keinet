/*
 * 作成日: 2004/10/11
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.helpdesk;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import jp.co.fj.keinavi.beans.csv.CSVLine;
import jp.co.fj.keinavi.beans.csv.CSVWriter;
import jp.co.fj.keinavi.util.CollectionUtil;

import com.fjh.beans.DefaultBean;

/**
 * @author Hiroyuki Nishiyama - QuiQsoft
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class OutputTsvBean extends DefaultBean {
	/* データオブジェクト */
	private ArrayList outputTextList = null;
	/* 出力ファイル名 */
	private File outFile = null;
	/* ヘッダー情報 */
	private List headTextList = null;
	/* 出力項目対象 */
	private Integer outTarget[] = null;
	/* 出力ファイル種別 0:CSV　1:txt　*/
	private int outType = 0;

	/* 行番号 */
	private int rowIdx = 0;
	// ファイル分割単位
	private int divNum = 0;

	public void execute() throws IOException {

		// CSV出力
		doCsvExport();
	}

	/**
	 * 	エクスポートファイル：ヘッダー情報生成
	 *
	 * @return header ヘッダー情報
	 */
	protected String[] getHeader(List headerList){

		Integer target[] = getOutTarget();

		if (headerList == null || target == null) {
			return null;
		}

		String header[] = new String[target.length];
		Set outSet = CollectionUtil.array2Set( target);
		int count = 0;
		for(int i=0; i<headerList.size(); i++){
			Integer key = new Integer(((String[])headerList.get(i))[0]);
			if(outSet.contains(key)){
				header[count] = ((String[])headerList.get(i))[2];
				count++;
			}
		}
		return header;
	}


	/**
	*
	* CSVファイルの書込み。
	*
	* @return void
	*/
	public void doCsvExport() throws IOException {

		OutputStream inf_out = null;
		CSVWriter inf_cw = null;

		try {
			// ディレクトリの作成
			File parent = new File(outFile.getParentFile().getPath());
			if(!parent.exists())parent.mkdirs();

			// ヘッダー情報の抽出
			String header[] = getHeader(getHeadTextList());
			// CSVファイル書き出しストリームを格納
			inf_out = new FileOutputStream(outFile.getPath(), false);

			// CSV出力用オブジェクトを格納
			if (header == null) {
				inf_cw = new CSVWriter(inf_out, "MS932");
			} else {
				inf_cw = new CSVWriter(inf_out, header,"MS932");
			}
			inf_cw.setDelimiter("\t");

			String[] record = null;
			CSVLine inf_csvLine = null;
			Set outSet = CollectionUtil.array2Set(outTarget);
			int recSize = outSet.size();

			int count = 0;

			// 行単位でループ
			// 分割単位に合わせて開始位置を変える
			for (int i = rowIdx; i < rowIdx + divNum; i++) {

				// 最終レコードまで処理したら終わり
				if (i >= outputTextList.size()) {
					break;
				}

				// １行分のデータをMAPで取得
				TreeMap textValue = new TreeMap((Map) outputTextList.get(i));

				// 出力項目を格納する配列を作成
				record = new String[recSize];

				// 項目のキーを取得
				Set keyset = textValue.keySet();
				Iterator ite = keyset.iterator();
				int recCount = 0;

				// 項目（キー）単位でループ
				while (ite.hasNext()) {
					// キーを取得
					Integer key = (Integer) ite.next();
					// 値を取得
					String value = (String) textValue.get(key);

					if(value == null) value="";

					// 出力対象にセットされた項目ならば出力
					if(outSet.contains(key)){
						record[recCount] = value;
						recCount++;
					}
				}

				inf_csvLine = new CSVLine(record);
				inf_csvLine.setCount(recSize);
				inf_cw.writeLine(inf_csvLine);
				count++;
			}

			// 開始位置を進める
			rowIdx += count;

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException("ＣＳＶファイルの出力に失敗しました。" + ex.getMessage());
		} finally {
			if(inf_cw!=null)inf_cw.flush();
			if(inf_cw!=null)inf_cw.close();
		}
	}

	/**
	 * @param list
	 */
	public void setOutputTextList(ArrayList list) {
		outputTextList = list;

	}
	/**
	 * @param list
	 */
	public ArrayList getOutputTextList() {
		return outputTextList;
	}

	/**
	 * @param file
	 */
	public void setOutFile(File file) {
		outFile = file;
	}

	/**
	 * @return
	 */
	public int getOutType() {
		return outType;
	}

	/**
	 * @param i
	 */
	public void setOutType(int i) {
		outType = i;
	}

	/**
	 * @return
	 */
	public int getRowIdx() {
		return rowIdx;
	}

	/**
	 * @param i
	 */
	public void setRowIdx(int i) {
		rowIdx = i;
	}

	/**
	 * @return
	 */
	public int getDivNum() {
		return divNum;
	}

	/**
	 * @return
	 */
	public void setDivNum(int num) {
		divNum = num;
	}

	/**
	 * @param list
	 */
	public void setHeadTextList(List list) {
		headTextList = list;
	}

	/**
	 * @return
	 */
	public List getHeadTextList() {
		return headTextList;
	}

	/**
	 * @return
	 */
	public Integer[] getOutTarget() {
		return outTarget;
	}

	/**
	 * @param integers
	 */
	public void setOutTarget(Integer[] integers) {
		outTarget = integers;
	}

    /**
     * @return outFile を戻します。
     */
    public File getOutFile() {
        return outFile;
    }
}

package jp.co.fj.keinavi.excel.data.school;

//import java.util.ArrayList;
/**
 * �Z�����ѕ��́|�߉��r�|���ъT�� ���Z��r�p�f�[�^���X�g
 * �쐬��: 2004/07/21
 * @author	A.Iwata
 */
public class S51ClassListBean {
	//�w�N
	private String strGrade = "";
	//�N���X
	private String strClass = "";
	//�͎���
	private String strMshmei = "";
	//�͎����{���
	private String strMshDate = "";
	//�󌱐l��
	private int intNinzu = 0;
	//���ϓ_
	private float floHeikin = 0;
	//���ϕ΍��l
	private float floHensa = 0;
	//�N���X�p�O���t�\���t���O
	private int intDispClassFlg = 0;
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGrade() {
		return this.strGrade;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public float getFloHeikin() {
		return this.floHeikin;
	}
	public float getFloHensa() {
		return this.floHensa;
	}
	public int getIntDispClassFlg() {
		return this.intDispClassFlg;
	}

	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setFloHeikin(float floHeikin) {
		this.floHeikin = floHeikin;
	}
	public void setFloHensa(float floHensa) {
		this.floHensa = floHensa;
	}
	public void setIntDispClassFlg(int intDispClassFlg) {
		this.intDispClassFlg = intDispClassFlg;
	}

}

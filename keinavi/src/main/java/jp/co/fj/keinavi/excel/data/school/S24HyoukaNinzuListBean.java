package jp.co.fj.keinavi.excel.data.school;

/**
 * �쐬��: 2004/07/13
 * @author C.Murata
 *
 * �Z�����ѕ��́|�ߔN�x��r�|�u�]��w�]���ʐl�� �]���ʐl���f�[�^���X�g
 */
public class S24HyoukaNinzuListBean {

	//�͎��N�x
	private String strNendo = "";
	//�S�����u�]�Ґ�
	private int intSoshiboAll = -999;
	//�Z�����u�]�Ґ�
	private int intSoshiboHome = -999;
	//���u�]�Ґ�
	private int intDai1shibo = -999;
	//�S�����ϓ��_��
	private float floTokuritsuAll = -999.0f;
	//�Z�����ϓ��_��
	private float floTokuritsuHome = -999.0f;
	//�S�����ϕ΍��l
	private float floHensaAll = -999.0f;
	//�Z�����ϕ΍��l
	private float floHensaHome = -999.0f;
	//�]���ʐl�� A-E
	private int intHyoukaA = -999;
	private int intHyoukaB = -999;
	private int intHyoukaC = -999;
	private int intHyoukaD = -999;
	private int intHyoukaE = -999;

	// 2019/11/18 QQ)Ooseto �p��F�莎�������Ή� DEL START
	// 2019/09/30 QQ) ���ʃe�X�g�Ή� ADD START
	//�]���ʐl�� A-E �܂�
//	private int intHyoukaA_Hukumu = -999;
//	private int intHyoukaB_Hukumu = -999;
//	private int intHyoukaC_Hukumu = -999;
//	private int intHyoukaD_Hukumu = -999;
//	private int intHyoukaE_Hukumu = -999;
	// 2019/09/30 QQ) ���ʃe�X�g�Ή� ADD END
	// 2019/11/18 QQ)Ooseto �p��F�莎�������Ή� DEL END

	/*
	 * GET
	 */
	public float getFloTokuritsuAll() {
		return floTokuritsuAll;
	}
	public float getFloTokuritsuHome() {
		return floTokuritsuHome;
	}
	public float getFloHensaAll() {
		return floHensaAll;
	}
	public float getFloHensaHome() {
		return floHensaHome;
	}
	public int getIntDai1shibo() {
		return intDai1shibo;
	}
	public int getIntHyoukaA() {
		return intHyoukaA;
	}
	public int getIntHyoukaB() {
		return intHyoukaB;
	}
	public int getIntHyoukaC() {
		return intHyoukaC;
	}
	public int getIntHyoukaD() {
		return intHyoukaD;
	}
	public int getIntHyoukaE() {
		return intHyoukaE;
	}
	public int getIntSoshiboAll() {
		return intSoshiboAll;
	}
	public int getIntSoshiboHome() {
		return intSoshiboHome;
	}
	public String getStrNendo() {
		return strNendo;
	}

	// 2019/11/18 QQ)Ooseto �p��F�莎�������Ή� DEL START
	// 2019/09/30 QQ) ���ʃe�X�g�Ή� ADD START
//	public int getIntHyoukaA_Hukumu() {
//		return intHyoukaA_Hukumu;
//	}
//	public int getIntHyoukaB_Hukumu() {
//		return intHyoukaB_Hukumu;
//	}
//	public int getIntHyoukaC_Hukumu() {
//		return intHyoukaC_Hukumu;
//	}
//	public int getIntHyoukaD_Hukumu() {
//		return intHyoukaD_Hukumu;
//	}
//	public int getIntHyoukaE_Hukumu() {
//		return intHyoukaE_Hukumu;
//	}
	// 2019/09/30 QQ) ���ʃe�X�g�Ή� ADD END
	// 2019/11/18 QQ)Ooseto �p��F�莎�������Ή� DEL END


	/*
	 * SET
	 */
	public void setFloTokuritsuAll(float f) {
		floTokuritsuAll = f;
	}
	public void setFloTokuritsuHome(float f) {
		floTokuritsuHome = f;
	}
	public void setFloHensaAll(float f) {
		floHensaAll = f;
	}
	public void setFloHensaHome(float f) {
		floHensaHome = f;
	}
	public void setIntDai1shibo(int i) {
		intDai1shibo = i;
	}
	public void setIntHyoukaA(int i) {
		intHyoukaA = i;
	}
	public void setIntHyoukaB(int i) {
		intHyoukaB = i;
	}
	public void setIntHyoukaC(int i) {
		intHyoukaC = i;
	}
	public void setIntHyoukaD(int i) {
		intHyoukaD = i;
	}
	public void setIntHyoukaE(int i) {
		intHyoukaE = i;
	}
	public void setIntSoshiboAll(int i) {
		intSoshiboAll = i;
	}
	public void setIntSoshiboHome(int i) {
		intSoshiboHome = i;
	}
	public void setStrNendo(String string) {
		strNendo = string;
	}

	// 2019/11/18 QQ)Ooseto �p��F�莎�������Ή� DEL START
	// 2019/09/30 QQ) ���ʃe�X�g�Ή� ADD START
//	public void setIntHyoukaA_Hukumu(int i) {
//		intHyoukaA_Hukumu = i;
//	}
//	public void setIntHyoukaB_Hukumu(int i) {
//		intHyoukaB_Hukumu = i;
//	}
//	public void setIntHyoukaC_Hukumu(int i) {
//		intHyoukaC_Hukumu = i;
//	}
//	public void setIntHyoukaD_Hukumu(int i) {
//		intHyoukaD_Hukumu = i;
//	}
//	public void setIntHyoukaE_Hukumu(int i) {
//		intHyoukaE_Hukumu = i;
//	}
	// 2019/09/30 QQ) ���ʃe�X�g�Ή� ADD END
	// 2019/11/18 QQ)Ooseto �p��F�莎�������Ή� DEL START

}

package jp.co.fj.keinavi.beans.exam.filter;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * 記述テスト用の模試フィルタ
 * 設問別成績、学力要素別成績データ用
 *
 * 2019.09.10	[新規作成]
 *
 *
 * @author KAWAI - TOTEC
 * @version 1.0
 *
 */
public class WrtnExam2FilterBean implements IExamFilterBean {

	/**
	 * @see jp.co.fj.keinavi.util.taglib.exam.IExamFilterBean#execute(
	 * 			jp.co.fj.keinavi.data.LoginSession,
	 * 			jp.co.fj.keinavi.data.ExamSession,
	 * 			jp.co.fj.keinavi.data.ExamData)
	 */
	public boolean execute(final LoginSession login,
			final ExamSession examSession, final ExamData exam) {

		// 記述テスト以外ならOK
		return !KNUtil.isWrtnExam2(exam);
	}

}

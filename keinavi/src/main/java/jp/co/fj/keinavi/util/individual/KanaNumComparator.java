package jp.co.fj.keinavi.util.individual;

import java.util.Comparator;

import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.lang.StringUtils;

import jp.co.fj.kawaijuku.judgement.data.UnivSortKey;

/**
 *
 * 大学データ（{@link UnivSortKey}）をカナ付50音順番号で並べ替える {@link Comparator} です。<br>
 * ・カナ付50音順番号<br>
 * ・大学コード<br>
 * ・大学夜間区分<br>
 * ・学部内容コード<br>
 * ・大学グループ区分（日程コード）<br>
 * ・学科通番<br>
 * ・日程方式<br>
 * ・日程方式枝番<br>
 * ・学科カナ名<br>
 * ・学科コード<br>
 * の順で並べ替えます。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class KanaNumComparator extends ComparatorChain {

    /** serialVersionUID */
    private static final long serialVersionUID = -9092738685854268937L;

    /** Singletonインスタンス */
    private static final KanaNumComparator INSTANCE = new KanaNumComparator();

    /**
     * コンストラクタです。
     */
    private KanaNumComparator() {

        /**
         * {@link UnivSortKey} の {@link Comparator}。
         */
        abstract class UnivComparator implements Comparator {

            /**
             * {@inheritDoc}
             */
            public int compare(Object o1, Object o2) {
                if (o1 == null && o2 == null) {
                    return 0;
                } else if (o2 == null) {
                    return -1;
                } else if (o1 == null) {
                    return 1;
                } else {
                    return compare((UnivSortKey) o1, (UnivSortKey) o2);
                }
            }

            /**
             * {@link UnivSortKey} を比較します。
             *
             * @param d1 比較対象1
             * @param d2 比較対象2
             */
            abstract public int compare(UnivSortKey u1, UnivSortKey u2);

            /**
             * 文字列を比較します。
             *
             * @param d1 比較対象1
             * @param d2 比較対象2
             * @return 比較結果
             */
            protected final int compare(String str1, String str2) {
                return StringUtils.defaultString(str1).compareTo(StringUtils.defaultString(str2));
            }

        }

        /* 「カナ付50音順番号」で並べ替える */
        addComparator(new UnivComparator() {

            /**
             * {@inheritDoc}
             */
            public int compare(UnivSortKey u1, UnivSortKey u2) {
                return compare(u1.getKanaNum(), u2.getKanaNum());
            }

        });

        /* 「大学コード」で並べ替える */
        addComparator(new UnivComparator() {

            /**
             * {@inheritDoc}
             */
            public int compare(UnivSortKey u1, UnivSortKey u2) {
                return compare(u1.getUnivCd(), u2.getUnivCd());
            }

        });

        /* 「大学夜間区分」で並べ替える */
        addComparator(new UnivComparator() {

            /**
             * {@inheritDoc}
             */
            public int compare(UnivSortKey u1, UnivSortKey u2) {
                return new Integer(u1.getFlag_night()).compareTo(new Integer(u2.getFlag_night()));
            }

        });

        /* 「学部内容コード」で並べ替える */
        addComparator(new UnivComparator() {

            /**
             * {@inheritDoc}
             */
            public int compare(UnivSortKey u1, UnivSortKey u2) {
                return compare(u1.getFacultyConCd(), u2.getFacultyConCd());
            }

        });

        /* 「大学グループ区分（日程コード）」で並べ替える */
        addComparator(new UnivComparator() {

            /**
             * {@inheritDoc}
             */
            public int compare(UnivSortKey u1, UnivSortKey u2) {
                return compare(u1.getUnivGDiv(), u2.getUnivGDiv());
            }

        });

        /* 「学科通番」で並べ替える */
        addComparator(new UnivComparator() {

            /**
             * {@inheritDoc}
             */
            public int compare(UnivSortKey u1, UnivSortKey u2) {
                return compare(u1.getDeptSerialNo(), u2.getDeptSerialNo());
            }

        });

        /* 「日程方式」で並べ替える */
        addComparator(new UnivComparator() {

            /**
             * {@inheritDoc}
             */
            public int compare(UnivSortKey u1, UnivSortKey u2) {
                return compare(u1.getScheduleSys(), u2.getScheduleSys());
            }

        });

        /* 「日程方式枝番」で並べ替える */
        addComparator(new UnivComparator() {

            /**
             * {@inheritDoc}
             */
            public int compare(UnivSortKey u1, UnivSortKey u2) {
                return compare(u1.getScheduleSysBranchCd(), u2.getScheduleSysBranchCd());
            }

        });

        /* 「学科カナ名」で並べ替える */
        addComparator(new UnivComparator() {

            /**
             * {@inheritDoc}
             */
            public int compare(UnivSortKey u1, UnivSortKey u2) {
                return StringUtils.defaultString(u1.getSubNameKana())
                        .compareTo(StringUtils.defaultString(u2.getSubNameKana()));
            }

        });

        /* 「学科コード」で並べ替える */
        addComparator(new UnivComparator() {

            /**
             * {@inheritDoc}
             */
            public int compare(UnivSortKey u1, UnivSortKey u2) {
                return compare(u1.getDeptCd(), u2.getDeptCd());
            }

        });
    }

    /**
     * {@link KanaNumComparator} のインスタンスを返します。
     *
     * @return {@link KanaNumComparator}
     */
    public static KanaNumComparator getInstance() {
        return INSTANCE;
    }

}

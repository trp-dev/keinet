package jp.co.fj.keinavi.beans.sales;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.fj.freemenu.data.ExamScoreData;
import jp.co.fj.freemenu.data.ExamScoreSession;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 画面に表示する模試情報を生成するBeanです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD201ExamBean extends DefaultBean {

	/** serialVersionUID */
	private static final long serialVersionUID = 6485352796167211838L;

	/** 模試を格納するデータクラス */
	private final ExamScoreSession examScoreSession = new ExamScoreSession();

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Map map = new HashMap();
			String currentYear = KNUtil.getCurrentYear();

			ps = conn.prepareStatement(QueryLoader.getInstance()
					.load("sd201_exam").toString());
			ps.setString(1, currentYear);
			ps.setString(2, currentYear);

			rs = ps.executeQuery();
			while (rs.next()) {
				ExamScoreData data = createExamScoreData(rs);

				/* 年度単位の入れ物が無ければ作る */
				List container = (List) map.get(data.getExamYear());
				if (container == null) {
					container = new ArrayList(40);
					map.put(data.getExamYear(), container);
				}

				container.add(data);
			}

			examScoreSession.setExamMap(map);

		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}

		/* 年度のリストを作る */
		List years = new ArrayList(examScoreSession.getExamMap().keySet());
		Collections.sort(years);
		Collections.reverse(years);
		examScoreSession.setYears(years);
	}

	/**
	 * ResultSetからデータクラスを生成します。
	 */
	private ExamScoreData createExamScoreData(ResultSet rs) throws SQLException {
		ExamScoreData data = new ExamScoreData();
		data.setExamYear(rs.getString(1));
		data.setExamCd(rs.getString(2));
		data.setExamName(rs.getString(3));
		data.setExamTypeCD(rs.getString(4));
		return data;
	}

	/**
	 * 模試を格納するデータクラスを返します。
	 * 
	 * @return 模試を格納するデータクラス
	 */
	public ExamScoreSession getExamScoreSession() {
		return examScoreSession;
	}

}

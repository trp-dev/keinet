package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.common.ISheetData;
import jp.co.fj.keinavi.beans.sheet.excel.school.S52_11ExcelCreator;
import jp.co.fj.keinavi.beans.sheet.school.data.S52_11Data;
import jp.co.fj.keinavi.beans.sheet.school.data.S52_11ExamListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S52_11ScoreZoneListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S52_11SubjectListData;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 校内成績分析 - 過回比較 - 偏差値分布
 * 受験学力測定テスト用のSheetBean
 *
 * 2007.07.30	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class S52AbilitySheetBean extends S42AbilitySheetBean {

    // 帳票データ
    private final S52_11Data data = (S52_11Data) getData();

    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws Exception {

        // ワークテーブルのセットアップ
        insertIntoExistsExam();
        // 同系統で過去の模試の配列
        final ExamData[] past = getPastExamArray();
        // 科目変換テーブルのセットアップ
        insertIntoSubCDTrans(past);
        // 基本情報のセット
        setBasicInfo();
        // 科目情報のセット
        setSubjectInfo();
        // センター到達エリアのセット
        setCenterInfo();
        // 過回データのセット
        setExamInfo(past);
        // スコア帯データのセット
        setScoreInfo();
    }

    // 基本情報のセット
    private void setBasicInfo() {
        // 一括コード
        data.setBundleCd(profile.getBundleCD());
        // 学校名
        data.setBundleName(profile.getBundleName());
        // 模試年度
        data.setExamYear(exam.getExamYear());
        // 模試コード
        data.setExamCd(exam.getExamCD());
        // 模試名
        data.setExamName(exam.getExamName());
        // 実施日
        data.setInpleDate(exam.getInpleDate());
        // セキュリティスタンプ
        data.setSecurityStamp(getIntFlag(PRINT_STAMP));
    }

    // 過回データのセット
    private void setExamInfo(final ExamData[] past) throws SQLException {

        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(
                    QueryLoader.getInstance().getQuery("s52_3"));

            // 一括コード
            ps.setString(1, data.getBundleCd());

            for (int i = 0; i < past.length; i++) {
                setExamInfo(ps, past[i]);
            }
        } finally {
            DbUtils.closeQuietly(ps);
        }
    }

    private void setExamInfo(final PreparedStatement ps,
            final ExamData exam) throws SQLException {

        // 模試年度
        ps.setString(2, exam.getExamYear());
        // 模試コード
        ps.setString(3, exam.getExamCD());

        ResultSet rs = null;
        try {
            // 全ての科目について取得する
            for (final Iterator ite = data.getSubjectListData(
                    ).iterator(); ite.hasNext();) {

                final S52_11SubjectListData s = (
                        S52_11SubjectListData) ite.next();

                final S52_11ExamListData e = data.createExamListData();
                e.setExamYear(exam.getExamYear());
                e.setExamCd(exam.getExamCD());
                e.setExamName(exam.getExamNameAbbr());
                e.setInpleDate(exam.getInpleDate());
                s.addExamListData(e);

                // 科目コード
                ps.setString(4, s.getSubCd());

                rs = ps.executeQuery();
                if (rs.next()) {
                    e.setSubCd(rs.getString(1));
                    e.setAvgPnt(rs.getDouble(2));
                    e.setNumbers(rs.getInt(3));
                }
                rs.close();
            }
        } finally {
            DbUtils.closeQuietly(rs);
        }
    }

    // スコア帯リストデータをセットする
    private void setScoreInfo() throws SQLException {

        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(
                    QueryLoader.getInstance().getQuery("s42_3_7"));
            for (final Iterator ite = data.getSubjectListData(
                    ).iterator(); ite.hasNext();) {
                final S52_11SubjectListData s = (
                        S52_11SubjectListData) ite.next();
                setScoreInfo(ps, s.getExamListData());
            }
        } finally {
            DbUtils.closeQuietly(ps);
        }
    }

    private void setScoreInfo(final PreparedStatement ps,
            final List yearListData) throws SQLException {

        for (final Iterator ite = yearListData.iterator(
                ); ite.hasNext();) {

            final S52_11ExamListData e = (S52_11ExamListData) ite.next();

            // 科目コードが無ければ処理しない
            // ※成績が存在しない過年度など
            if (e.getSubCd() == null) {
                continue;
            }

            // 模試年度
            ps.setString(1, e.getExamYear());
            // 科目コード
            ps.setString(2, e.getSubCd());
            // 一括コード
            ps.setString(3, data.getBundleCd());
            // 模試コード
            ps.setString(4, e.getExamCd());

            ResultSet rs = null;
            try {
                rs = ps.executeQuery();
                while (rs.next()) {
                    final S52_11ScoreZoneListData s = (
                            S52_11ScoreZoneListData) data.createScoreZoneListData();
                    s.setDevZoneCd(rs.getString(1));
                    s.setHighLimit(rs.getDouble(2));
                    s.setLowLimit(rs.getDouble(3));
                    s.setNumbers(rs.getInt(4));
                    e.addScoreZoneListData(s);
                }
            } finally {
                DbUtils.closeQuietly(rs);
            }
        }
    }

    /**
     * @return データクラス
     */
    protected ISheetData createData() {
        return new S52_11Data();
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
     */
    protected double calcNumberOfPrint() {
        return getNumberOfPrint("S42_01");
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
     */
    protected String getCategory() {
        return S_PAST_DEV;
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.AbstractExtSheetBean#create()
     */
    protected void create() throws Exception {
        sheetLog.add("S52_11");
        new S52_11ExcelCreator(data, sessionKey, outfileList, getAction()).execute();
    }

}

package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.keinavi.beans.SubRecordASearchBean;
import jp.co.fj.keinavi.beans.individual.DockedExamsBean;
import jp.co.fj.keinavi.beans.individual.ExamSearchBean;
import jp.co.fj.keinavi.beans.individual.JudgeByUnivBean;
import jp.co.fj.keinavi.beans.individual.PlanUnivDeleteBean;
import jp.co.fj.keinavi.beans.individual.PlanUnivSearchIndBean;
import jp.co.fj.keinavi.beans.individual.PlanUnivUpdateBean;
import jp.co.fj.keinavi.beans.individual.ScoreSearchBean;
import jp.co.fj.keinavi.beans.individual.SubRecordSearchBean;
import jp.co.fj.keinavi.beans.individual.TakenExamSearchBean;
import jp.co.fj.keinavi.beans.individual.judgement.JudgementListBean;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.NestedPlanUnivData;
import jp.co.fj.keinavi.data.individual.PlanUnivData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I302Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.judgement.beans.ScoreKeiNavi;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.individual.ExamSorter;
import jp.co.fj.keinavi.util.individual.IPropsLoader;
import jp.co.fj.keinavi.util.individual.JudgementSorterV2;

/**
 *
 * 受験予定大学一覧
 * 
 * 2004.08.03	Tomohisa YAMADA - Totec
 * 				[新規作成]
 * 
 * 2005.02.23	Tomohisa YAMADA - Totec
 * 				[1]受験した最新の模試が存在しない場合に、
 * 				詳細画面に大学情報のみ表示する
 * 
 * 2005.03.08	Tomohisa YAMADA - Totec
 * 				[2]最新模試取得の時に
 * 				「担当クラスの年度よりも若い」制限を加える
 * 
 * 2005.04.07 	Tomohisa YAMADA - Totec
 * 				[3]ScoreBeanの[35]に伴う修正
 * 
 * 2005.08.02 	Tomohisa YAMADA - Totec
 * 				[4]マーク模試を受けていないときの帳票エラー修正
 *
 * <2010年度改修>
 * 2009.10.02	Tomohisa YAMADA - Totec
 * 				入試日程変更
 * 
 * 2010.04.08	Tomohisa YAMADA - Totec
 * 				数学�@の偏差値を動的に算出する
 * 
 * 
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class I302Servlet extends IndividualServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.individual.IndividualServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			 javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(final HttpServletRequest request,
		final HttpServletResponse response)
		throws ServletException, IOException {

		super.execute(request, response);
		
		// フォームデータを取得
		final I302Form form = (I302Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.individual.I302Form");
	
		// HTTPセッション
		final HttpSession session = request.getSession(false);
	
		// 個人共通
		final ICommonMap iCommonMap = getICommonMap(request);
			
		// 模試データ
		final ExamSession examSession = getExamSession(request);	
			
		// プロファイル情報
		final Profile profile = getProfile(request);
		
		// セキュリティスタンプ
		Short printStamp = (Short) profile.getItemMap(
				IProfileCategory.I_PERSONAL_NOTE).get(IProfileItem.PRINT_STAMP);
		// 印刷対象生徒単位
		Short printStudent = (Short) profile.getItemMap(
				IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);
		// 出力フォーマット
		Short textFormat = (Short) profile.getItemMap(
				IProfileCategory.I_PERSONAL_NOTE).get(IProfileItem.TEXT_OUT_FORMAT);
		int textFormat_ck = Integer.parseInt(textFormat.toString());
		// 帳票種類
		Short textFileFormat = (Short) profile.getItemMap(
				IProfileCategory.I_PERSONAL_NOTE).get(IProfileItem.TEXT_FILE_FORMAT);
		
		//転送:JSP
		if ("i302".equals(getForward(request))) {
			if(!"i302".equals(getBackward(request))
					|| !iCommonMap.getTargetPersonId().equals(form.getTargetPersonId())
					|| isChangedMode(request)) {
				
				// 対象生徒が変わった
				if ("i302".equals(getBackward(request))) {
                    // 2.出力フォーマットをセット
					if (form.getTextFormat() != null) {
						if (form.getTextFormat().length == 2) {
							form.setTextFormat(CollectionUtil.splitComma("1,2"));
						} else {
							if (form.getTextFormat()[0] != null
									&& form.getTextFormat()[0].equals("1")) {
								form.setTextFormat(CollectionUtil.splitComma("1,0"));
							} else {
								form.setTextFormat(CollectionUtil.splitComma("0,2"));
							}
						}
					} else {
						form.setTextFormat(CollectionUtil.splitComma("0,0"));
					}
				}
				//初回アクセス
				else {
					// 個人成績分析共通-印刷対象生保存をセット
					if (iCommonMap.isBunsekiMode()) {
						form.setPrintStudent(printStudent.toString());
					}
					// 1.セキュリティスタンプをセット
					form.setPrintStamp(printStamp.toString());	
					// 2.出力フォーマットをセット
					switch (textFormat_ck) {
						case 1:
							form.setTextFormat(CollectionUtil.splitComma("1,0"));
							break;
						case 2:
							form.setTextFormat(CollectionUtil.splitComma("0,2"));
							break;
						case 3:
							form.setTextFormat(CollectionUtil.splitComma("1,2"));
							break;
						case 0:
							form.setTextFormat(CollectionUtil.splitComma("0,0"));
							break;
					}
					// 3.帳票種類をセット
					if (textFileFormat != null) {
						form.setTextFileFormat(textFileFormat.toString());
					}
				}
			}
			//2回目以降アクセス
			else{
				//2.出力フォーマットをセット
				if(form.getTextFormat() != null){
					if (form.getTextFormat().length == 2) {
						form.setTextFormat(CollectionUtil.splitComma("1,2"));
					} else {
						if (form.getTextFormat()[0] != null
								&& form.getTextFormat()[0].equals("1")) {
							form.setTextFormat(CollectionUtil.splitComma("1,0"));
						} else {
							form.setTextFormat(CollectionUtil.splitComma("0,2"));
						}
				 	}
				} else {
				  form.setTextFormat(CollectionUtil.splitComma("0,0"));
				}
			}
			
			String errorMessage = null;
			Connection con = null;
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(false);
				
				//更新ボタンを押した
				if(form.getButton() != null && form.getButton().equals("update")){
					//エラーがなければ受験予定日を画面上からチェック
					if(errorMessage == null){
						boolean sameDateExist = false;
						//画面上で既に重複していないかチェック
						if(form.getMonth() != null && form.getMonth().length > 0){
							//受験日の重複チェック
							Map dateMap = new HashMap();
							for(int i=0; i<form.getMonth().length; i++){
								dateMap.put(form.getMonth()[i]+"/"+form.getDate()[i],new String[]{form.getMonth()[i], form.getDate()[i]});
							}
							if(dateMap.size() != form.getMonth().length){
								sameDateExist = true;
							}
						}
						//重複した受験予定日を登録しようとしているので、エラー表示
						if(sameDateExist){
							errorMessage = IPropsLoader.getInstance().getMessage("ERROR_DATE_EXIST");
						}
					}
					//エラーがなければ受験予定日をDBからチェック
					if(errorMessage == null){
						//既に画面上に全ての受験日があるのでチェックは必要ない
					}					
					//3.エラーがなければ更新を押下：受験地等をアップデートする
					if(errorMessage == null){
						PlanUnivUpdateBean planUnivBean = new PlanUnivUpdateBean();
						planUnivBean.setIndividualId(form.getTargetPersonId());
						planUnivBean.setUnivValues(form.getUnivValue());//<-チェックボックスで選択されたキーコード
						planUnivBean.setConnection("", con);
						planUnivBean.execute();
						if(planUnivBean.getErrorMessage() != null) {
							errorMessage = planUnivBean.getErrorMessage();
						}
					}
					//正常
					if(errorMessage == null) {
						//同じ画面に戻るので、何もしない
					}
					//エラーならば、画面に戻って、エラー表示
					else {
						form.setErrorMessage(errorMessage);
					}
				}
				//削除ボタンを押した
				else if(form.getButton() != null && form.getButton().equals("delete")){
					
					final String[] values = 
						CollectionUtil.deconcatComma(form.getUnivValue4Detail());
					
					PlanUnivDeleteBean bean = 
						new PlanUnivDeleteBean(form.getTargetPersonId());
					bean.setUnivcd(values[0]);
					bean.setFacultycd(values[1]);
					bean.setDeptcd(values[2]);
					bean.setMode(values[3]);
					bean.setConnection(null, con);
					bean.execute();
				}
				
				// 常に一覧表示
				final PlanUnivSearchIndBean planUnivSearchBean = createPlanUnivSearchBean(
						con, form.getTargetPersonId());
				request.setAttribute("planUnivSearchBean", planUnivSearchBean);
				
				// ガイドラインがあるかないかチェックする（列の表示・非表示用）
				boolean isGuideExist = false;
				for (final Iterator ite = planUnivSearchBean.getRecordSetAll(
						).iterator(); ite.hasNext();) {
					PlanUnivData puddy = (PlanUnivData) ite.next();
					if (puddy.getGuideLine() != null
							&& puddy.getGuideLine().trim().length() > 0) {
						isGuideExist = true;
						break;
					}
				}
				request.setAttribute("isGuideExist", Boolean.valueOf(isGuideExist));
				
				// ステップ１．模試成績の獲得
				// 最新模試を取得
				final ExaminationData latestExam = getLatestExam(
						con, form.getTargetPersonId(),
						ProfileUtil.getChargeClassYear(profile), examSession);
				
				// 最新模試＋ドッキングを取得
				final ExaminationData[] exams = getDockedExams(
						con, latestExam, examSession);

				// マーク模試成績の取得（模試を全く受けていない場合以外は絶対に存在する）
				final SubRecordSearchBean mark = getSubRecordSearchBean(
						con, form.getTargetPersonId(), exams[0]);

				// 記述模試成績の取得（マーク模試選択時はドッキング先が存在しない）
				final SubRecordSearchBean wrtn = getSubRecordSearchBean(
						con, form.getTargetPersonId(), exams[1]);
						
				// ScoreBeanに取得してきた成績をセット
				final Score score = new ScoreKeiNavi(
						SubRecordASearchBean.searchNullSet(
								exams[0], 
								con));
				score.setIndividualId(form.getTargetPersonId());
				
				if (mark != null && !mark.getRecordSetAll().isEmpty()) {
					score.setMark(mark.getRecordSetAll());
				} else {
					score.setMark(null);
				}
				
				if (wrtn != null && !wrtn.getRecordSetAll().isEmpty()) {
					score.setWrtn(wrtn.getRecordSetAll());
				} else {
					score.setWrtn(null);
				}
				
				score.setCExam(exams[0]);//[0]はマーク模試
				score.setSExam(exams[1]);//[1]は記述模試
				// 対象模試は選択中の模試
				score.setTargetExam(getTargetExam(con, iCommonMap));
				score.execute();

				// ステップ２．実際の判定
				// 大学指定判定実行
				JudgementListBean listBean = new JudgementListBean();

				JudgeByUnivBean jbub = new JudgeByUnivBean();
				jbub.setUnivAllList(super.getUnivAll());
				jbub.setUnivArray(toPlanUnivArray(planUnivSearchBean));
				jbub.setScoreBean(score);
				jbub.setPrecedeCenterPre();
				jbub.execute();	

				List judgedList = jbub.getRecordSet();
                
				listBean.createListBean(judgedList, score);
				
				// 結果一覧を作成してセッションに追加
				session.setAttribute("useListBean", listBean);
				
				con.commit();

			} catch (final Exception e) {
				rollback(con);
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			request.setAttribute("form", form);

			iCommonMap.setTargetPersonId(form.getTargetPersonId());
			forward(request, response, JSP_I302);

		//転送:SERVLET
		} else {
			// フォームの値をプロファイルに保存
			if (iCommonMap.isBunsekiMode()) {
				//個人成績分析共通-印刷対象生保存を保存
				profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT,new Short(form.getPrintStudent()));
			}

			// 1.セキュリティスタンプを保存
			profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(IProfileItem.PRINT_STAMP,new Short(form.getPrintStamp()));
			
			// 2.出力フォーマットを保存
			if (form.getTextFormat() != null) {
				if (form.getTextFormat().length == 2) {
					profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(IProfileItem.TEXT_OUT_FORMAT,new Short("3"));
				} else {
					if (form.getTextFormat()[0] != null && form.getTextFormat()[0].equals("1")) {
						profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(IProfileItem.TEXT_OUT_FORMAT,new Short("1"));
					} else {
						profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(IProfileItem.TEXT_OUT_FORMAT,new Short("2"));
					}
				}
			}else{
				profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(IProfileItem.TEXT_OUT_FORMAT, new Short("0"));
			}
			
			// 3:帳票種類を保存
			profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(
					IProfileItem.TEXT_FILE_FORMAT, Short.valueOf(form.getTextFileFormat()));
			
			// 対象生徒を保持する
			iCommonMap.setTargetPersonId(form.getTargetPersonId());

			// 帳票印刷が選択されたら判定を行う
			if (getForward(request).equals("sheet")) {
				// 志望校判定
				if ("2".equals(form.getTextFileFormat())) {
					new Thread(new JudgeThreadI204(form.getPrintStudent(),
							getDbKey(request), session, iCommonMap, examSession,
							profile, getUnivAll())).start();
				// 受験大学スケジュール表
				} else {
					new Thread(new JudgeThread(form.getPrintStudent(),
							getDbKey(request), session, iCommonMap, examSession,
							getUnivAll(), profile)).start();
				}
			}
			
			dispatch(request, response);
		}
	}
	
	// 指定した個人IDの受験予定大学一覧を取得
	private PlanUnivSearchIndBean createPlanUnivSearchBean(final Connection con,
			final String individualId) throws Exception {
		
		final PlanUnivSearchIndBean bean = new PlanUnivSearchIndBean();
		bean.setConnection("", con);
		bean.setIndividualCd(individualId);
		bean.execute();
		return bean;
	}
	
	// 受験予定大学を配列にする
	private String[][] toPlanUnivArray(
			final PlanUnivSearchIndBean bean) throws Exception {
		
		// 重複を省く
		final Set set = new HashSet();
		final List list = new ArrayList();
		for (final Iterator ite = bean.getNestedPlanUnivList(
				).iterator(); ite.hasNext();){
			final NestedPlanUnivData univData = (NestedPlanUnivData) ite.next();
			if (set.add(univData.getUnivCd() + univData.getFacultyCd()
					+ univData.getDeptCd())) {
				String[] univValue = new String[3];
				univValue[0] = univData.getUnivCd();
				univValue[1] = univData.getFacultyCd();
				univValue[2] = univData.getDeptCd();
				list.add(univValue);
			}
		}
		
		return  (String[][]) list.toArray(new String[set.size()][3]);
	}
	
	// 対象模試を取得
	private ExaminationData getTargetExam(final Connection con,
			final ICommonMap iCommonMap) throws Exception {
		
		final ExamSearchBean bean = new ExamSearchBean();
		bean.setConnection(null, con);
		bean.setExamYear(iCommonMap.getTargetExamYear());
		bean.setExamCd(iCommonMap.getTargetExamCode());
		bean.execute();
		
		// 絶対に取れるはずだから、最初のを取得
		return (ExaminationData) bean.getRecordSetAll().get(0);
	}
	
	// 指定した生徒が受験した最新模試を取得
	private ExaminationData getLatestExam(final Connection con,
			final String individualId, final String chargeClassYear,
			final ExamSession examSession) throws Exception {
		
		final TakenExamSearchBean bean = new TakenExamSearchBean();
		bean.setConnection(null, con);
		bean.setIndividualId(individualId);
		bean.setExamSession(examSession);
		
		// 担当クラス年度よりも以前のものに限定する
		bean.setLimitedYear(chargeClassYear);
		
		// 対象を模試区分01,02に限定する
		bean.setExamTypeCds(new String[]{
				IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_MARK"), 
				IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_WRTN")});
		
		// センターリサーチ・高２模試を除く
		bean.setExcludeExamCds(new String[]{
				IPropsLoader.getInstance().getMessage("CODE_EXAM_CENTERRESEARCH"),
				IPropsLoader.getInstance().getMessage("CODE_EXAM_MARK_2NDGRADE"),
				IPropsLoader.getInstance().getMessage("CODE_EXAM_WRTN_2NDGRADE")});
		bean.execute();
		
		final List results = bean.getRecordSetAll();
		
		if (!results.isEmpty()) {
			// 外部開放日順にソート
			Collections.sort(results, new ExamSorter().new SortByOutDataOpenDate());
			// この生徒が受けた全ての模試の一番最初が最新の模試
			return (ExaminationData) results.get(0);
		} else {
			return null;
		}
	}
	
	// 指定した模試＋ドッキングを取得
	private ExaminationData[] getDockedExams(final Connection con,
			final ExaminationData exam,
			final ExamSession examSession) throws Exception {
		
		if (exam == null) {
			ExaminationData[] exams = new ExaminationData[] {null, null};
			exams[0] = new ExaminationData();
			exams[0].setExamYear("");
			exams[0].setExamCd("");
			return exams;
		} else {
			DockedExamsBean deb = new DockedExamsBean();
			deb.setConnection(null, con);
			deb.setExamYear(exam.getExamYear());
			deb.setExamCd(exam.getExamCd());
			deb.setExamSession(examSession);
			deb.execute();
			return deb.getExaminationDatas();
		}
	}
	
	// 指定した生徒の指定した模試成績を取得
	private SubRecordSearchBean getSubRecordSearchBean(
			final Connection con, final String individualId,
			final ExaminationData exam) throws Exception {
		
		// 模試がなければここまで
		if (exam == null) {
			return null;
		}
		
		final SubRecordSearchBean bean = new SubRecordSearchBean();
		bean.setConnection(null, con);
		bean.setIndividualId(individualId);
		bean.setExamYear(exam.getExamYear());
		bean.setExamCd(exam.getExamCd());
		bean.execute();
		return bean;
	}
	
	// 判定処理スレッド
	private class JudgeThread extends BaseJudgeThread {
	
		private final String[] selectedIndividualIds;
		private final ICommonMap iCommonMap;
		private final ExamSession examSession;
		private final List univAll;
		private final Profile profile;
	
		// コンストラクタ
		private JudgeThread(final String judgeStudent,
				final String dbKey, final HttpSession pSession,
				final ICommonMap pICommonMap, final ExamSession pExamSession,
				final List pUnivAll, final Profile pProfile) {
			super(pSession, dbKey);
			selectedIndividualIds = getSelectedIndividualIds(pICommonMap, judgeStudent);
			iCommonMap = pICommonMap;
			examSession = pExamSession;
			univAll = pUnivAll;
			profile = pProfile;
		}
		
		/**
		 * @see jp.co.fj.keinavi.servlets.individual.BaseJudgeThread#execute(
		 * 			java.sql.Connection)
		 */
		protected Map execute(final Connection con) throws Exception {

			// 判定結果マップ
			final Map judgedMap = new HashMap();
			
			//成績のリストを取得
			//対象模試情報の取得
			final ExaminationData targetExam = getTargetExam(con, iCommonMap);
			
			//ScoreSearchBean =「この生徒が受けた」最新のマーク模試と最新の記述模試
			ScoreSearchBean ssb = new ScoreSearchBean();
			ssb.setExamSession(examSession);
			ssb.setIndividualIds(selectedIndividualIds);
			ssb.setLimitedYear(ProfileUtil.getChargeClassYear(profile));//[2] add
			ssb.setConnection("", con);
			ssb.setTargetExam(targetExam);//[3] add
			ssb.execute();
				
			List scoreList = ssb.getRecordSetAll();
			for(int i=0; i<scoreList.size(); i++){
				//ScoreBeanを一人ずつ取得
				Score sb = (Score) scoreList.get(i);
				
				// この生徒の受験予定大学一覧を取得
				final PlanUnivSearchIndBean planUnivSearchBean = createPlanUnivSearchBean(
						con, sb.getIndividualId());
			
				/// 大学指定判定実行
				JudgementListBean listBean = new JudgementListBean();
				JudgeByUnivBean jbub = new JudgeByUnivBean();
				jbub.setUnivAllList(univAll);
				jbub.setUnivArray(toPlanUnivArray(planUnivSearchBean));
				jbub.setScoreBean(sb);
				jbub.execute();	
				List judgedList = jbub.getRecordSet();
				listBean = new JudgementListBean();
				listBean.createListBean(judgedList, sb);
				judgedMap.put(sb.getIndividualId(), listBean);
			}
			
			return judgedMap;
		}
	}
	
	// 志望校判定用のスレッド
	private class JudgeThreadI204 extends BaseJudgeThread {
		
		private final String[] selectedIndividualIds;
		private final ExamSession examSession;
		private final Profile profile;
		private final List univAll;
	
		// コンストラクタ
		private JudgeThreadI204(final String judgeStudent,
				final String dBKey, final HttpSession pSession,
				final ICommonMap iCommonMap, final ExamSession pExamSession,
				final Profile pProfile, 	final List pUnivAll) {
			super(pSession, dBKey);
			selectedIndividualIds = getSelectedIndividualIds(iCommonMap, judgeStudent);
			examSession = pExamSession;
			profile = pProfile;
			univAll = pUnivAll;
		}
		
		/**
		 * @see jp.co.fj.keinavi.servlets.individual.BaseJudgeThread#execute(
		 * 			java.sql.Connection)
		 */
		protected Map execute(final Connection con) throws Exception {

			// 判定結果マップ
			final Map judgedMap = new HashMap();
	
			/** ステップ１：成績の取得 */
			final List scoreList = new ArrayList();
			if (selectedIndividualIds != null
					&& selectedIndividualIds.length > 0) {
				for (int i = 0; i < selectedIndividualIds.length; i++) {
					scoreList.add(createScoreBean(selectedIndividualIds[i], con));
				}
			}

			for (final Iterator ite = scoreList.iterator(); ite.hasNext();) {
				
				final Score score = (Score) ite.next();
				final JudgementListBean listBean = new JudgementListBean();
				
				// マーク模試も記述模試も受けていないので判定を行わない
				if (score.getMark() == null && score.getWrtn() == null) {
					listBean.setJudged(false);
					listBean.setExamTaken(false);
					
				// マーク模試または記述模試どちらか一方でも受けているので判定する
				} else {
					// この生徒の受験予定大学一覧を取得
					final PlanUnivSearchIndBean planUnivSearchBean = createPlanUnivSearchBean(
							con, score.getIndividualId());
			
					/** 大学指定判定実行 */
					JudgeByUnivBean jbub = new JudgeByUnivBean();
					jbub.setUnivAllList(univAll);
					jbub.setUnivArray(toPlanUnivArray(planUnivSearchBean));
					jbub.setScoreBean(score);
					jbub.setPrecedeCenterPre();
					jbub.execute();	
					final List judgedList = jbub.getRecordSet();
					
					// 選択対象模試がマークならばセンター評価順にソート
					if (score.isTargetMarkExam()) {
						Collections.sort(judgedList,
								new JudgementSorterV2().new SortByCenterScoreRank());
					// 選択対象模試が記述模試ならば二次評価順にソート
					} else {
						Collections.sort(judgedList,
								new JudgementSorterV2().new SortBySecondScoreRank());
					}
	
					listBean.createListBean(judgedList, score);
				}
				
				judgedMap.put(score.getIndividualId(), listBean);
			}
			
			return judgedMap;
		}
		
		// 成績情報を作る
		private Score createScoreBean(final String individualId,
				final Connection con) throws Exception {
			
			// 最新模試を取得
			final ExaminationData latestExam = getLatestExam(
					con, individualId,
					ProfileUtil.getChargeClassYear(profile), examSession);
			
			// 最新模試＋ドッキングを取得
			final ExaminationData[] exams = getDockedExams(
					con, latestExam, examSession);

			// マーク模試成績の取得（模試を全く受けていない場合以外は絶対に存在する）
			final SubRecordSearchBean mark = getSubRecordSearchBean(
					con, individualId, exams[0]);

			// 記述模試成績の取得（マーク模試選択時はドッキング先が存在しない）
			final SubRecordSearchBean wrtn = getSubRecordSearchBean(
					con, individualId, exams[1]);
					
			// ScoreBeanに取得してきた成績をセット
			final Score score = new ScoreKeiNavi(
					SubRecordASearchBean.searchNullSet(
							exams[0], 
							con));
			score.setIndividualId(individualId);
			
			if (mark != null && !mark.getRecordSetAll().isEmpty()) {
				score.setMark(mark.getRecordSetAll());
			} else {
				score.setMark(null);
			}
			
			if (wrtn != null && !wrtn.getRecordSetAll().isEmpty()) {
				score.setWrtn(wrtn.getRecordSetAll());
			} else {
				score.setWrtn(null);
			}
			
			score.setCExam(exams[0]);
			score.setSExam(exams[1]);
			score.setTargetExam(latestExam);
			score.execute();
			
			return score;
		}	
	}

}

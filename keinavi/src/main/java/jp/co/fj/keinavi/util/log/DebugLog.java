package jp.co.fj.keinavi.util.log;

import java.util.logging.*;

/**
 * デバッグログをファイルに出力するクラス
 * 
 * 2005.02.23 Yoshimoto KAWAI - Totec
 *            利用者ＩＤの出力に対応
 *
 * @Date		2004/06/09
 * @author	TOTEC)Nishiyama
 */
public class DebugLog extends Log {

	/**
	 * ログ出力オブジェクト
	 */
	protected static DebugLogWriter dlog = (DebugLogWriter) DebugLogWriter.factory();

	/**
	 * デバッグ種別
	 */
	public static final int DebLevelPT = 1;
	public static final int DebLevelIT = 2;
	public static final int DebLevelEP = 3;
	public static final String[] DebugLevel = { "PT ", "IT ", "EP " };
		
	/**
	 * PTログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	protected static void Pt(String IPAddress,
							String ContractID,
							String EmployeeID,
							String account,
							String ErrCode,
							String ErrContents,
							String SupplementInfo) {

		LogRecord rec = new LogRecord(Level.INFO, "");
		setCallStack2Record(rec, Depth);
		_DebugLog(rec, DebLevelPT, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * PTログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param Supplement			補足情報
	 */
	protected static void Pt(String IPAddress,
							String ContractID,
							String EmployeeID,
							String account,
							String ErrCode,
							String ErrContents,
							Throwable Supplement) {

		LogRecord rec = new LogRecord(Level.INFO, "");
		setCallStack2Record(rec, Depth);
		_DebugLog(rec, DebLevelPT, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, Supplement);
	}

	/**
	 * ITログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	protected static void It(String IPAddress,
							String ContractID,
							String EmployeeID,
							String account,
							String ErrCode,
							String ErrContents,
							String SupplementInfo) {

		LogRecord rec = new LogRecord(Level.INFO, "");
		setCallStack2Record(rec, Depth);
		_DebugLog(rec, DebLevelIT, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * ITログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param Supplement			補足情報
	 */
	protected static void It(String IPAddress,
							String ContractID,
							String EmployeeID,
							String account,
							String ErrCode,
							String ErrContents,
							Throwable Supplement) {

		LogRecord rec = new LogRecord(Level.INFO, "");
		setCallStack2Record(rec, Depth);
		_DebugLog(rec, DebLevelIT, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, Supplement);
	}

	/**
	 * EPログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	protected static void Ep(String IPAddress,
							String ContractID,
							String EmployeeID,
							String account,
							String ErrCode,
							String ErrContents,
							String SupplementInfo) {

		LogRecord rec = new LogRecord(Level.INFO, "");
		setCallStack2Record(rec, Depth);
		_DebugLog(rec, DebLevelEP, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, SupplementInfo);
	}

	/**
	 * EPログを出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param Supplement			補足情報
	 */
	protected static void Ep(String IPAddress,
							String ContractID,
							String EmployeeID,
							String account,
							String ErrCode,
							String ErrContents,
							Throwable Supplement) {

		LogRecord rec = new LogRecord(Level.INFO, "");
		setCallStack2Record(rec, Depth);
		_DebugLog(rec, DebLevelEP, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, Supplement);
	}

	/**
	 *
	 * デバッグログを出力する(パターン1)
	 *
	 * @param rec						ログレコードクラス
	 * @param Mode						エラー種別
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	private static void _DebugLog(LogRecord rec,
									int Mode,
									String IPAddress,
									String ContractID,
									String EmployeeID,
									String account,
									String ErrCode,
									String ErrContents,
									String SupplementInfo) {
		
		try {
			__DebugLog(rec, Mode, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, SupplementInfo);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 *
	 * デバッグログを出力する(パターン2)
	 *
	 * @param rec						ログレコードクラス
	 * @param Mode						エラー種別
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param Supplement			補足情報
	 */
	private static void _DebugLog(LogRecord rec,
									int Mode,
									String IPAddress,
									String ContractID,
									String EmployeeID,
									String account,
									String ErrCode,
									String ErrContents,
									Throwable Supplement) {

		try {			
			// 例外をログレコードに登録する。
			rec.setThrown(Supplement);
			
			// スタックトレースを取得する。
			String note = getStack(Supplement);
			
			__DebugLog(rec, Mode, IPAddress, ContractID, EmployeeID, account, ErrCode, ErrContents, note);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * デバッグログを出力する
	 *
	 * @param rec						ログレコードクラス
	 * @param Mode						エラー種別
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param ErrCode				エラーコード
	 * @param ErrContents		エラー内容
	 * @param SupplementInfo	補足情報
	 */
	private static void __DebugLog(LogRecord rec,
									int Mode,
									String IPAddress,
									String ContractID,
									String EmployeeID,
									String account,
									String ErrCode,
									String ErrContents,
									String SupplementInfo) throws Exception {
		// 重要度レベルと表示設定レベルの比較
		if (!dlog.checkDebugLevel(Mode)) {
			// デバッグログを出さない
			return;
		}

		// デバッグログを出力する
		String msg = ___DebugLevel(Mode) + " " +
					convert(IPAddress) + " " +
					convert(ContractID) + " " +
					convert(EmployeeID) + " " +
					convert(account) + " " +
					rec.getSourceClassName() + "." + rec.getSourceMethodName() + ": " +
					/*IntToHex8(*/convert(ErrCode)/*)*/ + " " +
					convert(ErrContents) + " " +
					convert(SupplementInfo) +
					(rec.getThrown() == null ? "" : getStack(rec.getThrown()));
					
		rec.setMessage(msg);
		dlog.LogOutput(rec);
	}
	
	/**
	 * デバッグログを出力する
	 * 
	 * @param rec ログレコード
	 */
	protected static void __DebugLog(LogRecord rec) throws Exception {
		dlog.LogOutput(rec);
	}
	
	/**
	 * デバッグレベルの文字列を返す
	 * 
	 * @param iMode デバッグレベル
	 * @return ログ出力文字列
	 */
	private final static String ___DebugLevel(int iMode) {

		try {
			return DebugLevel[iMode - 1];
		} catch(ArrayIndexOutOfBoundsException e) {
			return "Unknown_Debug_Level";
		}
	}
}

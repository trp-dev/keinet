package jp.co.fj.keinavi.excel.data.school;

/**
 * �ߔN�x��r�|���ъT���f�[�^�N���X
 * �쐬��: 2004/07/09
 * @author	C.Murata
 */
public class S21ListBean {
	
	//�^�E�ȖڃR�[�h
	private String strKmkCd = "";
	//�^��Ȗږ�
	private String strKmkmei = "";
	//�z�_
	private String strHaitenKmk = "";
	//�͎��N�x
	private String strNendo = "";
	//�l��
	private int intNinzu = 0;
	//���ϓ_
	private float floHeikin = 0;
	//���ϕ΍��l
	private float floHensa = 0;

	
	/*----------*/
	/* Get      */
	/*----------*/
	public float getFloHeikin() {
		return floHeikin;
	}
	public float getFloHensa() {
		return floHensa;
	}
	public int getIntNinzu() {
		return intNinzu;
	}
	public String getStrKmkCd() {
		return strKmkCd;
	}
	public String getStrKmkmei() {
		return strKmkmei;
	}
	public String getStrNendo() {
		return strNendo;
	}
	public String getStrHaitenKmk() {
		return strHaitenKmk;
	}


	/*----------*/
	/* Set      */
	/*----------*/

	public void setFloHeikin(float f) {
		floHeikin = f;
	}
	public void setFloHensa(float f) {
		floHensa = f;
	}
	public void setIntNinzu(int i) {
		intNinzu = i;
	}
	public void setStrKmkCd(String string) {
		strKmkCd = string;
	}
	public void setStrKmkmei(String string) {
		strKmkmei = string;
	}
	public void setStrNendo(String string) {
		strNendo = string;
	}
	public void setStrHaitenKmk(String string) {
		strHaitenKmk = string;
	}

}

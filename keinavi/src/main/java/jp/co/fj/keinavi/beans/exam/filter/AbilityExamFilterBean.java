package jp.co.fj.keinavi.beans.exam.filter;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * 受験学力測定テスト用の模試フィルタ
 * 
 * 2006.08.23	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class AbilityExamFilterBean implements IExamFilterBean {

	/**
	 * @see jp.co.fj.keinavi.util.taglib.exam.IExamFilterBean#execute(
	 * 			jp.co.fj.keinavi.data.LoginSession,
	 * 			jp.co.fj.keinavi.data.ExamSession,
	 * 			jp.co.fj.keinavi.data.ExamData)
	 */
	public boolean execute(final LoginSession login,
			final ExamSession examSession, final ExamData exam) {
		
		return !KNUtil.isAbilityExam(exam);
	}

}

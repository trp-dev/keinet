/**
 * 高校成績分析−高校間比較
 * 	Excelファイル編集
 * 作成日: 2004/08/19
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.business;

import java.io.*;
import java.util.*;

import jp.co.fj.keinavi.excel.data.business.*;
import jp.co.fj.keinavi.excel.cm.*;
import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.*;

public class B42_06 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	final private int	intMaxSheetSr	= 50;			// 最大シート数	
	final private String	strArea			= "A1:AE66";	// 印刷範囲	
	final private String	masterfile0		= "B42_06";		// ファイル名
	final private String	masterfile1		= "NB42_06";	// ファイル名
	private String	masterfile		= "";					// ファイル名
	final private String	mastersheet 	= "tmp";		//テンプレートシート名
	final private int[]	tabRow			= {4,34};		// 表の基準点
	final private int[]	tabCol			= {1,6,11,16,21,26};	// 表の基準点

	/*
	 * 	Excel編集メイン
	 * 		B42Item b42Item: データクラス
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
 	public int b42_06EditExcel(B42Item b42Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (b42Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			
			// データセット
			ArrayList	b42List			= b42Item.getB42List();
			Iterator	itr				= b42List.iterator();
			int		row				= 0;	// 行
			int 		setRow			= 0;	// *セット用
			int 		ninzu			= -999;	// *作成用
			int		hyouCnt			= 0;	// 値範囲：０〜１　(表カウンター)
			int		gakkoCnt		= 0;	// 値範囲：０〜９　(学校カウンター)
			int		maxSheetIndex	= 0;	// シートカウンター
			int		intBookCngCount	= 0;	// ブックカウンター
			int		kmkCd			= 0;	// 科目コード保持
			boolean	kataFlg			= true;// false:改行済(型・科目名変更済)　
			boolean	bolGakkouFlg	= true;// true:学校情報表示　（学校情報表示フラグ）
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
			boolean	bolFirstDataFlg	= true;// true:List.Next()を実行
			int		intFirstCnt		= 0;	// 読み飛ばし用カウンター
			int		intJikouCnt		= 0;	// 過年度分自校情報表示カウンター

			// 2004.10.25 Add
			int		intRuikeiN		= -999;
			float		floRuikeiK		= -999;
			// 2004.10.25 AddEnd

			// マスタExcel読み込み
			FileInputStream	fin			= null;
			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			// 基本ファイルを読込む
			B42ListBean b42ListBean = new B42ListBean();

			while( itr.hasNext() ) {
				b42ListBean = (B42ListBean)itr.next();

				// 基本ファイルを読み込む
				B42GakkoListBean	b42GakkoListBean	= new B42GakkoListBean();

				//データの保持
				ArrayList	b42GakkoList		= b42ListBean.getB42GakkoList();
				Iterator	itrB42Gakko		= b42GakkoList.iterator();

				// 型・科目の最初のデータ(=学校データ)
				bolFirstDataFlg=true;


				//　学校データセット
				gakkoCnt = 0;
				while(itrB42Gakko.hasNext()){

					if( bolBookCngFlg == true ){
						// マスタExcel読み込み
						workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
						if( workbook==null ){
							return errfread;
						}
						bolSheetCngFlg	= true;
						bolBookCngFlg	= false;
						maxSheetIndex	= 0;
					}

					if(bolSheetCngFlg){
						// シートテンプレートのコピー
						workSheet = workbook.cloneSheet(0);
						maxSheetIndex++;

						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);

						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, b42Item.getIntSecuFlg() ,29 ,30 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 29 );
						workCell.setCellValue(secuFlg);

						// 対象模試セット
						String moshi =cm.setTaisyouMoshi( b42Item.getStrMshDate() );	// 模試月取得
						workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
						workCell.setCellValue( "対象模試　　：" + cm.toString(b42Item.getStrMshmei()) + moshi);

						kataFlg=true;
						bolSheetCngFlg=false;
					}

					if(kmkCd!=Integer.parseInt(b42ListBean.getStrKmkCd())){
						kataFlg=true;
					}
					// 型名・配点セット 2004.11.9 配点がないとき括弧はなし
					if(kataFlg){
						String haiten = "";
						if ( !cm.toString(b42ListBean.getStrHaiten()).equals("") ) {
							haiten = "（" + b42ListBean.getStrHaiten() + "）";
						}
						workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt], 0 );
						workCell.setCellValue( "型・科目：" + cm.toString(b42ListBean.getStrKmkmei()) + haiten );
//						workCell.setCellValue( "型・科目：" + cm.toString(b42ListBean.getStrKmkmei()) +" (" + cm.toString(b42ListBean.getStrHaiten()) + ")" );

						kmkCd=Integer.parseInt(b42ListBean.getStrKmkCd());
						bolGakkouFlg=true;
						kataFlg=false;
					}

					//2004.11.22 営業用では自校がない
//					if(bolGakkouFlg){
//						b42GakkoListBean	= (B42GakkoListBean) b42GakkoList.get(intJikouCnt%3);
//						if(intJikouCnt%3==2){
//							bolGakkouFlg=false;
//						}
//						intJikouCnt++;
//
//						// 学校情報読み飛ばし
//						if(bolFirstDataFlg){
//							b42GakkoListBean	= (B42GakkoListBean) itrB42Gakko.next();
//							if(intFirstCnt%3==2){
//								bolFirstDataFlg=false;
//							}
//							intFirstCnt++;
//						}
//					}else{
//						b42GakkoListBean	= (B42GakkoListBean) itrB42Gakko.next();
//					}
					b42GakkoListBean	= (B42GakkoListBean) itrB42Gakko.next();

					B42BnpListBean b42BnpListBean = new B42BnpListBean();
					ArrayList	b42BnpList		= b42GakkoListBean.getB42BnpList();
					Iterator	itrB42Bnp;
					itrB42Bnp		= b42BnpList.iterator();

					// 人数データセット
					row=0;
					intRuikeiN = -999;
					floRuikeiK = -999;
					while(itrB42Bnp.hasNext()){
						b42BnpListBean = (B42BnpListBean) itrB42Bnp.next();

						if(row==0){
							if(gakkoCnt%3==0){
								// 高校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+1, tabCol[gakkoCnt] );
								workCell.setCellValue( b42GakkoListBean.getStrGakkomei() );
							}

							// 年度セット
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+2, tabCol[gakkoCnt] );
							workCell.setCellValue( cm.toString(b42GakkoListBean.getStrNendo())+"年度" );

							// 合計人数セット
							if(b42GakkoListBean.getIntNinzu()!=-999){
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+24, tabCol[gakkoCnt] );
								workCell.setCellValue( b42GakkoListBean.getIntNinzu() );
							}

							// 平均点セット
							if(b42GakkoListBean.getFloHeikin()!=-999.0){
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+25, tabCol[gakkoCnt] );
								workCell.setCellValue( b42GakkoListBean.getFloHeikin() );
							}

							// 平均偏差値セット
							if(b42GakkoListBean.getFloHensa()!=-999.0){
								workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+26, tabCol[gakkoCnt] );
								workCell.setCellValue( b42GakkoListBean.getFloHensa() );
							}
						}

						// 人数セット
						if(b42BnpListBean.getIntNinzu()!=-999){
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+row+5, tabCol[gakkoCnt]+1 );
							workCell.setCellValue( b42BnpListBean.getIntNinzu() );

							// *セット準備
							if( b42BnpListBean.getIntNinzu() > ninzu ){
								ninzu = b42BnpListBean.getIntNinzu();
								setRow = row;
							}

							//2004.10.25 累計をPGにて算出ST
							// 累計人数算出
							if(intRuikeiN==-999){
								intRuikeiN = b42BnpListBean.getIntNinzu();
							}else{
								intRuikeiN = intRuikeiN + b42BnpListBean.getIntNinzu();
							}
							//2004.10.25 累計をPGにて算出ED
						}

						// 構成比セット
						if(b42BnpListBean.getFloKoseihi()!=-999.0){
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+row+5, tabCol[gakkoCnt]+3 );
							workCell.setCellValue( b42BnpListBean.getFloKoseihi() );

							//2004.10.25 累計をPGにて算出ST
							// 累計人数算出
							if(floRuikeiK==-999){
								floRuikeiK = b42BnpListBean.getFloKoseihi();
							}else{
								floRuikeiK = floRuikeiK + b42BnpListBean.getFloKoseihi();
							}
							//2004.10.25 累計をPGにて算出ED
						}
						
						//2004.10.25 累計をPGにて算出ST
						// 累計人数セット
						if(intRuikeiN!=-999){
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+row+5, tabCol[gakkoCnt]+2 );
							workCell.setCellValue( intRuikeiN );

							//2005.04.27 Update 新テスト対応　 得点用のとき(intShubetsuFlgが1)は処理しない
							if (b42Item.getIntShubetsuFlg() != 1){
								if (row == 6){
									workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+27, tabCol[gakkoCnt] );
									workCell.setCellValue( intRuikeiN );
								}
	
								if (row == 10){
									workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+28, tabCol[gakkoCnt] );
									workCell.setCellValue( intRuikeiN );
								}
							}
						}
						// 累計構成比セット
						if(floRuikeiK!=-999){
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+row+5, tabCol[gakkoCnt]+4 );
							workCell.setCellValue( floRuikeiK );

							//2005.04.27 Update 新テスト対応　 得点用のとき(intShubetsuFlgが1)は処理しない
							if (b42Item.getIntShubetsuFlg() != 1){
								if (row == 6){
									workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+27, tabCol[gakkoCnt]+3 );
									workCell.setCellValue( floRuikeiK );
								}
	
								if (row == 10){
									workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+28, tabCol[gakkoCnt]+3 );
									workCell.setCellValue( floRuikeiK );
								}
							}
						}
						//2004.10.25 累計をPGにて算出ED
						
						row++;

						// *セット
						if((row >=19) && (ninzu!=-999)){
							workCell = cm.setCell( workSheet, workRow, workCell, tabRow[hyouCnt]+setRow+5, tabCol[gakkoCnt] );
							workCell.setCellValue("*");
							ninzu=-999;
							setRow=0;
							break;
						}

					}

					gakkoCnt++;
					if(gakkoCnt>=6){
						gakkoCnt=0;
						hyouCnt++;
						if(hyouCnt>1){
							bolSheetCngFlg=true;
							hyouCnt=0;
						}
					}

					if(bolSheetCngFlg){
						//
						if((maxSheetIndex >= intMaxSheetSr)&&(itrB42Gakko.hasNext())){
							bolBookCngFlg = true;
						}

				
						if( bolBookCngFlg == true){
							boolean bolRet = false;
							// Excelファイル保存
							if(intBookCngCount==0){
								if(itr.hasNext() == true){
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
								}
								else{
									bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
								}
							}else{
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
							}

							if( bolRet == false ){
								return errfwrite;
							}
							intBookCngCount++;
						}
					}
				}

				if(gakkoCnt!=0){
					hyouCnt++;
					gakkoCnt=0;
					if(hyouCnt>1){
						bolSheetCngFlg=true;
						hyouCnt=0;
					}
				}

				if(bolSheetCngFlg){
					//
					if(maxSheetIndex >= intMaxSheetSr){
						bolBookCngFlg = true;
					}

				
					if( bolBookCngFlg == true){
						boolean bolRet = false;
						// Excelファイル保存
						if(intBookCngCount==0){
							if(itr.hasNext() == true){
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
							}
							else{
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
							}
						}else{
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
						}

						if( bolRet == false ){
							return errfwrite;
						}
						intBookCngCount++;
					}
				}

			}

			if( bolBookCngFlg == false){
				boolean bolRet = false;
				// Excelファイル保存
				if(intBookCngCount!=0){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount+1, masterfile, maxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
				}

				if( bolRet == false ){
					return errfwrite;
				}
				intBookCngCount++;
			}
			
		} catch(Exception e) {
			log.Err("B42_06","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

}
package jp.co.fj.keinavi.beans.sheet.excel.sales;

import java.util.Date;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator;
import jp.co.fj.keinavi.beans.sheet.common.ISheetData;
import jp.co.fj.keinavi.beans.sheet.sales.data.SD01_01Data;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * パスワード通知書作成クラスです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD01_01ExcelCreator extends BaseExcelCreator {

	/**
	 * コンストラクタです。
	 */
	public SD01_01ExcelCreator(final ISheetData pData,
			final String pSequenceId, final List pOutFileList,
			final int pPrintFlag) throws Exception {
		super(pData, pSequenceId, pOutFileList, pPrintFlag, 1);
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator#initSheet(jp.co.
	 *      fj.keinavi.beans.sheet.common.ISheetData)
	 */
	protected void initSheet(ISheetData pData) {
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.BaseSheetCreator#createSheet(jp.co.fj.keinavi.beans.sheet.common.ISheetData)
	 */
	protected void createSheet(ISheetData pData) throws Exception {

		final SD01_01Data data = (SD01_01Data) pData;

		/* シートの変更を宣言する */
		setModifiedSheet();

		getCell("B2").setCellValue(
				"〒" + StringUtils.defaultString(data.getPostCode()));
		getCell("B3").setCellValue(data.getAddress());
		getCell("B6").setCellValue(data.getBundleName() + " 高等学校");
		getCell("B7").setCellValue(data.getTeacherName() + " 先生");
		getCell("F8").setCellValue("(" + data.getBundleCd() + ")");

		getCell("I4").setCellValue(new Date());

		getCell("B14").setCellValue(data.getExamName());

		getCell("F33").setCellValue(data.getPassword());

		getCell("G41").setCellValue(data.getExamName());
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator#getSheetId()
	 */
	protected String getSheetId() {
		return "SD01_01";
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator#isGraphSheet()
	 */
	protected boolean isGraphSheet() {
		return false;
	}

}

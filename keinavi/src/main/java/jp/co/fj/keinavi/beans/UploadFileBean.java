package jp.co.fj.keinavi.beans;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.UploadFileData;
import jp.co.fj.keinavi.util.FileUtil;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultSearchBean;

/**
 *
 * アップロードファイル管理Bean
 *
 * <2010年度マーク高２模試対応>
 * 2011.01.24	Tomohisa YAMADA
 * 				新規作成
 *
 * <大規模改修>
 * 2016.02.01	QuiQ soft)Nishiyama
 * 				JSP用のコンストラクタ作成
 *
 */
public class UploadFileBean extends DefaultSearchBean {

	//一時ファイルのネーミング用日付フォーマッタ
	private static final SimpleDateFormat TEMPORARY_NAMER_FORMATTER = new SimpleDateFormat("yyyyMMddHHmmssSSSZ");

	//サーバファイルネーミング用日付フォーマッタ
	private static final SimpleDateFormat SERVER_NAMER_FORMATTER = new SimpleDateFormat(KNCommonProperty.getUploadFileServerNamerFormatter());

	//河合塾ファイルファイル種類ID
	private static final String JUKU_FILE_TYPE_ID = KNCommonProperty.getUploadFileJukuFileTypeID();

	//河合塾ファイル拡張子
	private static final String JUKU_FILE_EXTENSION = KNCommonProperty.getUploadFileJukuFileExtension();

	//ダウンロードファイル作成用一時ディレクトリ
	private static final String TEMPORARY_DIRECTORY_PATH = KNCommonProperty.getUploadFileTemporaryDirectoryPath();

	//アップロードファイル保存ディレクトリ
	private static final String SERVER_DIRECTORY_PATH = KNCommonProperty.getUploadFileServerDirectoryPath();

	//ダウンロード可能ファイル保存ディレクトリ
	private static final String DOWNLOADBLE_DIRECTORY_PATH = KNCommonProperty.getUploadFileDownloadableDirectoryPath();

	//ダウンロード一覧表示時の月日制限
	private static final int MONTHS_AFTER_DOWNLOAD = KNCommonProperty.getUploadFileMonthsAfterDownload();

	//履歴表示時の月日制限
	private static final int MONTHS_AFTER_HISTORY = KNCommonProperty.getUploadFileMonthsAfterHistory();

	//ウィルス検出ファイル表示時の日付制限
	private static final int DAYS_AFTER_VIRUS = KNCommonProperty.getUploadFileDaysAfterVirus();

	private byte[] file;

	private String clientFileName;

	private File serverFile;

	private String fileTypeID;

	private String uploadComment;

	private String schoolCD;

	private String loginID;

	private String businessCode;

	private String zipFileName;

	private String[] uploadFileIDs;

	// 2016/02/01 QQ)Nishiyama 大規模改修 ADD START
    /**
     * コンストラクタです。
     *
     * JSPのコンパイル時に呼び出されるデフォルトコンストラクタです。<br>
     * Javaからの呼び出しは禁止します。
     */
	public UploadFileBean() {
	}
	// 2016/02/01 QQ)Nishiyama 大規模改修 ADD END

	/**
	 * コンストラクタ
	 * @param file アップロードファイル
	 * @param clientFileName アップロードファイル名
	 * @param fileTypeID ファイル種類ID
	 * @param uploadComment コメント
	 * @param loginSession ログインセッション
	 */
	public UploadFileBean(
			byte[] file,
			String clientFileName,
			String fileTypeID,
			String uploadComment,
			LoginSession loginSession) {
		this.file = file;
		this.clientFileName = clientFileName;
		this.fileTypeID = fileTypeID;
		this.uploadComment = uploadComment;
		this.schoolCD = loginSession.getUserID();
		this.loginID = loginSession.getLoginID();
	}

	/**
	 * コンストラクタ
	 * @param loginSession ログインセッション
	 */
	public UploadFileBean(
			LoginSession loginSession) {
		this.schoolCD = loginSession.getUserID();
	}

	/**
	 * コンストラクタ
	 * @param businessCode 営業部コード
	 */
	public UploadFileBean(
			String businessCode) {
		this.businessCode = businessCode;
	}

	/**
	 * コンストラクタ
	 * @param zipFileName 圧縮先ファイル名
	 * @param uploadFileIDs 圧縮対象アップロードファイルID
	 */
	public UploadFileBean(String zipFileName, String[] uploadFileIDs) {
		this.zipFileName = zipFileName;
		this.uploadFileIDs = uploadFileIDs;
	}

	/**
	 * 実行
	 * @deprecated
	 */
	public void execute() throws SQLException, Exception {
		throw new InternalError("このメソッドは実行できません。");
	}

	/**
	 * 営業部用ダウンロードファイル一覧取得（ダウンロード可能ファイルのみ）
	 * @throws Exception
	 */
	public void searchSafeByBusiness(int sortIndex) throws Exception {
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {

			Query query = QueryLoader.getInstance().load("sd01_business");

			switch (sortIndex) {
				case 1 : query.replaceAll("/* CONDITION */", "uf.schoolcd ASC, uf.update_date DESC");
					break;
				case -1 : query.replaceAll("/* CONDITION */", "uf.schoolcd DESC, uf.update_date DESC");
					break;
				case 2 : query.replaceAll("/* CONDITION */", "s.bundlename ASC, uf.update_date DESC");
					break;
				case -2 : query.replaceAll("/* CONDITION */", "s.bundlename DESC, uf.update_date DESC");
					break;
				case 3 : query.replaceAll("/* CONDITION */", "uf.update_date DESC");
					break;
				case -3 : query.replaceAll("/* CONDITION */", "uf.update_date ASC");
					break;
				case 4 : query.replaceAll("/* CONDITION */", "ufk.filetype_abbrname ASC, uf.update_date DESC");
					break;
				case -4 : query.replaceAll("/* CONDITION */", "ufk.filetype_abbrname DESC, uf.update_date DESC");
					break;
				case 7 : query.replaceAll("/* CONDITION */", "uf.last_download_date DESC, uf.update_date DESC");
					break;
				case -7 : query.replaceAll("/* CONDITION */", "uf.last_download_date ASC, uf.update_date DESC");
					break;
				default : throw new IllegalAccessException("ソートインデックスが正しくありません。" + sortIndex);
			}

			ps = conn.prepareStatement(query.toString());
			ps.setInt(1, MONTHS_AFTER_DOWNLOAD);
			ps.setString(2, businessCode);
			rs = ps.executeQuery();
			while (rs.next()) {
				recordSet.add(new UploadFileData(
						rs.getString("uploadfile_id"),
						rs.getString("schoolcd"),
						rs.getString("loginid"),
						rs.getTimestamp("update_date"),
						rs.getString("uploadfilename"),
						rs.getString("serverfilename"),
						rs.getString("filetype_id"),
						rs.getString("upload_comment"),
						rs.getInt("status"),
						rs.getTimestamp("last_download_date"),
						rs.getString("filetype_abbrname"),
						rs.getString("bundlename"),
						rs.getString("bundlename_kana")));
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * 学校用アップロード履歴一覧取得（ウィルス検知以外）
	 * @throws Exception
	 */
	public void searchSafeBySchool() throws Exception {
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("f011_school").toString());
			ps.setString(1, schoolCD);
			ps.setInt(2, MONTHS_AFTER_HISTORY);
			rs = ps.executeQuery();
			while (rs.next()) {
				recordSet.add(new UploadFileData(
						rs.getString("uploadfile_id"),
						rs.getString("schoolcd"),
						rs.getString("loginid"),
						rs.getTimestamp("update_date"),
						rs.getString("uploadfilename"),
						rs.getString("serverfilename"),
						rs.getString("filetype_id"),
						rs.getString("upload_comment"),
						rs.getInt("status"),
						rs.getTimestamp("last_download_date"),
						rs.getString("filetype_name")));
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * 学校用ウィルス検知ファイル一覧
	 * @throws SQLException
	 */
	public void searchVirusDetected() throws SQLException {
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("f004_virus").toString());
			ps.setString(1, schoolCD);
			ps.setInt(2, DAYS_AFTER_VIRUS);
			rs = ps.executeQuery();
			while (rs.next()) {
				recordSet.add(new UploadFileData(
						rs.getString("uploadfile_id"),
						rs.getString("schoolcd"),
						rs.getString("loginid"),
						rs.getTimestamp("update_date"),
						rs.getString("uploadfilename"),
						rs.getString("serverfilename"),
						rs.getString("filetype_id"),
						rs.getString("upload_comment"),
						rs.getInt("status"),
						rs.getTimestamp("last_download_date")));
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * ダウンロードを実行する。
	 * @param response
	 * @throws Exception
	 */
	public synchronized void download(HttpServletResponse response) throws Exception {

		//圧縮
		File zippedFile = makeZipFile();
		try {
			//ダウンロード
			FileUtil.download(response, zippedFile.getAbsolutePath(), this.zipFileName + ".zip");

			//DB更新
			updateDownloadDate();

		} finally {
			//zipファイルの削除
			if (zippedFile != null && zippedFile.exists()) {
			    zippedFile.delete();
			}
		}
	}

	/**
	 * ダウンロード日付を最新の状態に更新
	 * @throws SQLException
	 */
	private void updateDownloadDate() throws SQLException {
        PreparedStatement stmt = null;
        try {
			Query query = new Query("UPDATE uploadfile uf SET uf.last_download_date = SYSDATE WHERE uf.uploadfile_id IN (#)");
			query.setStringArray(1, uploadFileIDs);
			stmt = conn.prepareStatement(query.toString());
        	stmt.executeUpdate();
        } finally {
        	DbUtils.closeQuietly(stmt);
        }
	}

	/**
	 * ファイルを圧縮する。
	 * @return 圧縮後ファイル
	 * @throws Exception
	 */
	private File makeZipFile() throws Exception {

		List uploadFileList = searchServerFileNames(uploadFileIDs);

		File[] targetFiles = new File[uploadFileList.size()];

		int count = 0;

		for (Iterator ite = uploadFileList.iterator(); ite.hasNext();) {

			UploadFileData ufd = (UploadFileData) ite.next();

			targetFiles[count] = new File(createDownloadableFilePath(
					ufd.getSchoolCD(),
					ufd.getServerFileName()));

			count ++;
		}

		String tempFileName = TEMPORARY_NAMER_FORMATTER.format(new Date()) + ".zip";
		String tempFilePath = TEMPORARY_DIRECTORY_PATH + File.separator + tempFileName;

		//zip化
		return FileUtil.zip(tempFilePath, targetFiles);
	}

	/**
	 * アップロードファイルIDから実際のファイル名を検索する。
	 * @param uploadFileIDs
	 * @return
	 * @throws SQLException
	 */
	private List searchServerFileNames(String[] uploadFileIDs) throws SQLException {
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {

			Query query = new Query("SELECT uf.serverfilename, uf.schoolcd FROM uploadfile uf WHERE uf.uploadfile_id IN (#)");
			query.setStringArray(1, uploadFileIDs);
			ps = conn.prepareStatement(query.toString());
			rs = ps.executeQuery();
			List container = new ArrayList();
			while (rs.next()) {
				container.add(new UploadFileData(
						rs.getString("serverfilename"),
						rs.getString("schoolcd")));
			}
			return container;
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * アップロードを実行する。
	 * @throws Exception
	 */
	public void upload() throws Exception {

		//アップロード日時
		final Date uploadedTime = new Date();

		//サーバ側でファイルを自動ネーミング
		final String serverFileName = nameServerFile(this.fileTypeID, uploadedTime);

		//ファイルをアップロード
		this.serverFile = uploadFile(serverFileName);

		//DBに書き込み
		insert(this.serverFile.getName(), uploadedTime);
	}

	/**
	 * サーバファイルを削除する。
	 */
	public void deleteServerFile() {
		if (this.serverFile.exists()) {
			this.serverFile.delete();
		}
	}

	/**
	 * アップロードを実行する。
	 * @param serverFileName アップロード先ファイル名（リネーム）
	 * @throws Exception
	 */
	private File uploadFile(String serverFileName) throws Exception {
		return FileUtil.upload(file, createUploadedDirectoryPath(this.schoolCD), serverFileName);
	}

	/**
	 * アップロードファイル情報の書き込みをする。
	 * @param serverFileName
	 * @param uploadedTime
	 * @throws SQLException
	 */
	private void insert(String serverFileName, Date uploadedTime) throws SQLException {

        final String query = QueryLoader.getInstance().load("f010_insert").toString();

        PreparedStatement stmt = null;
        try {

        	stmt = conn.prepareStatement(query);

        	int index = 1;

        	stmt.setString(index++, schoolCD);
        	stmt.setString(index++, loginID);
        	stmt.setTimestamp(index++, new Timestamp(uploadedTime.getTime()));
        	stmt.setString(index++, clientFileName);
        	stmt.setString(index++, serverFileName);
        	stmt.setString(index++, fileTypeID);
        	stmt.setString(index++, uploadComment);

        	stmt.executeUpdate();

        } finally {
        	DbUtils.closeQuietly(stmt);
        }
	}

	/**
	 * 命名規約に従いアップロードファイルを命名する。
	 * @param fileTypeID
	 * @param uploadedTime
	 * @return
	 */
	private String nameServerFile(String fileTypeID, Date uploadedTime) {

		//拡張子を除いたファイル名（パスは含まない）
		final String fileName = FileUtil.dropExtension(FileUtil.extractFileName(clientFileName));

		//自動付加する日付文字列
		final String dateString = SERVER_NAMER_FORMATTER.format(uploadedTime);

		//ファイル種類が”河合塾ファイル”の場合　
		if (JUKU_FILE_TYPE_ID.equals(fileTypeID)) {
			return fileName + dateString + "." + JUKU_FILE_EXTENSION;
		}

		//ファイル種類が”河合塾ファイル”以外の場合　
		return fileName + dateString + "." + FileUtil.extractExtension(clientFileName);
	}

	/**
	 * アップロードファイル保存先ファイルパスを作成する。
	 * @param schoolCD
	 * @param serverFileName
	 * @return
	 */
	private String createUploadedDirectoryPath(String schoolCD) {
		return SERVER_DIRECTORY_PATH + File.separator + schoolCD;
	}

	/**
	 * ダウンロードファイル保存先ファイルパスを作成する。
	 * ※ウィルスチェック後にアップロードファイル保存先からここに移動される。
	 * @param schoolCD
	 * @param serverFileName
	 * @return
	 */
	private String createDownloadableFilePath(String schoolCD, String serverFileName) {
		return DOWNLOADBLE_DIRECTORY_PATH + File.separator + schoolCD + File.separator + serverFileName;
	}

}

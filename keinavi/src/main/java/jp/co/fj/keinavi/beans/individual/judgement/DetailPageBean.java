package jp.co.fj.keinavi.beans.individual.judgement;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.kawaijuku.judgement.Constants.English;
import jp.co.fj.kawaijuku.judgement.beans.JudgedResult;
import jp.co.fj.kawaijuku.judgement.beans.detail.CenterDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.UnivDetail;
import jp.co.fj.kawaijuku.judgement.beans.judge.HandledPoint;
import jp.co.fj.kawaijuku.judgement.beans.judge.Judgement;
import jp.co.fj.kawaijuku.judgement.beans.score.CenterScore;
import jp.co.fj.kawaijuku.judgement.beans.score.EngSSData;
import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.kawaijuku.judgement.util.JudgedUtil;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;
import jp.co.fj.kawaijuku.judgement.util.JudgementProperties;
import jp.co.fj.kawaijuku.judgement.util.StringUtil;
import jp.co.fj.keinavi.beans.individual.OpenJudgeSearchBean;
import jp.fujitsu.keinet.mst.db.model.ScheduleDetailData;

/**
 *
 * Kei-Navi用判定結果詳細データ
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class DetailPageBean extends JudgedResult {

    // serialVersionUID
    private static final long serialVersionUID = 6675544391761414942L;

    //願書締切日
    protected String appliDeadLine;

    //合格発表日
    protected String sucAnnDate;

    //入学締切日
    protected String proceDeadLine;

    //校舎県コード
    protected String prefCd_sh;

    //校舎県名
    protected String prefName_sh;

    //入試日文字列
    protected String dateCorrelation;

    /**
     * 分野リスト
     */
    protected List regionList;

    /**
     * 小系統リスト
     */
    protected List sStemmaList;

    /**
     * 中系統リスト
     */
    protected List mStemmaList;

    //日付フォーマッター
    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy/M/d");

    /** 本人成績：センタ：外国語 */
    private String score1Eng;

    /** 本人成績：センタ：リスニング */
    private String score1Lis;

    /** 本人成績：センタ：数学 */
    private String score1Mat;

    /** 本人成績：センタ：国語 */
    private String score1Jap;

    /** 本人成績：センタ：理科 */
    private String score1Sci;

    /** 本人成績：センタ：地歴公民 */
    private String score1Soc;

    /** 本人成績：二次：外国語 */
    private String score2Eng;

    /** 本人成績：二次：数学 */
    private String score2Mat;

    /** 本人成績：二次：国語 */
    private String score2Jap;

    /** 本人成績：二次：理科 */
    private String score2Sci;

    /** 本人成績：二次：地歴公民 */
    private String score2Soc;

    /** 大学配点：センタ：リスニング */
    private String allot1Lis;

    /**
     * コンストラクタです。
     *
     * @param bean 判定実行部
     */
    public DetailPageBean(Judgement bean) {
        super(bean);
    }

    /**
     * コンストラクタです。
     *
     * @deprecated JSPのコンパイル時に呼び出されるデフォルトコンストラクタです。<br>
     *             Javaからの呼び出しは禁止します。
     */
    public DetailPageBean() {
    }

    /**
     * 変数クリア
     */
    public void clear() {

        super.clear();

        //願書締切日
        appliDeadLine = "";

        //合格発表日
        sucAnnDate = "";

        //入学締切日
        proceDeadLine = "";

        //校舎県コード
        prefCd_sh = "";

        //校舎県名
        prefName_sh = "";

        //入試日文字列
        dateCorrelation = "";

        //分野リスト
        regionList = new ArrayList();
        //小系統リスト
        sStemmaList = new ArrayList();
        //中系統リスト
        mStemmaList = new ArrayList();
    }

    /**
     * 実行部
     */
    public void execute() throws Exception {

        //共通実行処理
        super.execute();

        UnivData univ = (UnivData) this.univ;

        this.prefCd_sh = univ.getPrefCd_sh();
        this.prefName_sh = univ.getPrefName_sh();

        //出願日など
        setVariousDates(univ);

        //入試日程文字列
        List sche = univ.getSchedules();
        synchronized (sche) {
            if (sche.size() == 0) {
                dateCorrelation = "";
            } else {
                dateCorrelation = univ.toInpleDateStrFull();
            }
        }

        //ガイドライン備考(あればセット、なければ入試日程文字列)
        if (univ.getRemarks() != null && univ.getRemarks().trim().length() > 0) {
            this.guideRemarks = univ.getRemarks();
        } else {
            this.guideRemarks = dateCorrelation;
        }

        //分野・小系統・中系統リスト
        this.regionList = univ.getRegionList();
        this.sStemmaList = univ.getSStemmaList();
        this.mStemmaList = univ.getMStemmaList();

        /* ドッキング判定が不要な国公立大学は、センタ配点を「-」とする */
        if (!univ.isDockingNeeded() && ("01".equals(univ.getUnivDivCd()) || "02".equals(univ.getUnivDivCd()))) {
            this.cAllotPntRateAll = "-";
        }
    }

    /**
     * 公表科目表示のためのロジックをここに移動
     */
    public JudgeOpenBean makeOpenPageBean(Connection conn, String branch) throws Exception {

        OpenJudgeSearchBean ojsb = new OpenJudgeSearchBean(this.bean.getUniv().getUnivCd(), this.bean.getUniv().getFacultyCd(), this.bean.getUniv().getDeptCd());
        ojsb.setConnection(null, conn);
        ojsb.execute();

        Map openDatas = ojsb.getOpenDatas();

        //公表科目が存在しない場合はNULLを設定
        if (openDatas.size() == 0) {
            return null;
        }

        UnivDetail detail = (UnivDetail) openDatas.get(branch);

        JudgeOpenBean open = new JudgeOpenBean(detail, openDatas.size());

        open.execute();

        return open;
    }

    /**
     * 判定結果を判定結果詳細インスタンスに包んで返す
     * @param judgement
     * @return
     */
    protected JudgedResult wrap(Judgement judgement) {
        return new DetailPageBean(judgement);
    }

    /**
     * 本人成績の情報を作成します。<br>
     * 教科別配点を個人成績表形式の大学配点に書き換えるので注意してください。
     */
    public void executeScore() {

        /*
         * 高得点操作後のセンタ英語大学配点は、
         * 本人成績と大学配点の処理の両方で利用するため、ここで取得しておく
         */
        BigDecimal centerEngAllot = calcAllot(Detail.English.COURSE_INDEX, getBean().getCAdoptedScore());

        // グループインデックスを取得
        int groupIndex = JudgedUtil.findGroupIndex(getBean().getCAdoptedScore(), Detail.English.COURSE_INDEX);

        /*
         * センタ英語筆記・リスニングの配点算出は、高コスト処理かつ
         * 本人成績と大学配点の処理の両方で利用するため、ここで取得しておく
         */
        CenterDetailHolder detail = (CenterDetailHolder) getDetail().getCenter().getForJudgement();
        double centerWtgAllot = detail.getWritingAllot(groupIndex);
        double centerLngAllot = detail.getListenigAllot(groupIndex);

        /* センタ本人成績の情報を作成する */
        executeCenterScore(score.getCenter(), centerEngAllot, centerWtgAllot, centerLngAllot);

        /* 二次本人成績の情報を作成する */
        executeSecondScore();

        /* センタ大学配点の情報を作成する */
        executeCenterAllot(score.getCenter(), centerEngAllot, centerWtgAllot, centerLngAllot);

        /* 二次大学配点の情報を作成する */
        executeSecondAllot();
    }

    /**
     * センタ本人成績の情報を作成します。
     *
     * @param score (@see CenterScore)
     * @param engAllotAfter 英語大学配点
     * @param wtgAllotBefore 筆記の配点
     * @param lngAllotBefore リスニングの配点
     */
    private void executeCenterScore(CenterScore score, BigDecimal engAllotAfter, double wtgAllotBefore, double lngAllotBefore) {

        String subCdR = JudgementProperties.getInstance().getMessage("mark.subcd.ewriting");
        String subCdL = JudgementProperties.getInstance().getMessage("mark.subcd.elistening");
        /* 英語本人得点(高得点操作後) */
        BigDecimal engPointAfter = calcPoint(Detail.English.COURSE_INDEX);

        /* 英語大学配点(高得点操作前) */
        double engAllotBefore = 0;
        if (wtgAllotBefore > 0 && wtgAllotBefore != JudgementConstants.Detail.ALLOT_NA) {
            engAllotBefore += wtgAllotBefore;
        }
        if (lngAllotBefore > 0 && lngAllotBefore != JudgementConstants.Detail.ALLOT_NA) {
            engAllotBefore += lngAllotBefore;
        }
        HandledPoint rScore = getAdopted(subCdR);
        HandledPoint lScore = getAdopted(subCdL);

        /*
         * TODO: 英語得点の基本的な考え方は以下の通り
         *  ・R or Lが判定に採用されていれば、その科目得点を求める。
         *  ・R or Lのどちらかが判定に採用されてない場合は、採用された科目は判定ロジックから返却された得点をそのまま採用する。
         *  ・R・L両方が判定に採用されている場合、得点の求め方が異なる。
         *  ・R or Lの配点が未設定の場合、判定ロジックから返却された得点をそのまま採用する。
         *  ・R・L両方の配点が設定されている場合、従来どおりの計算式で科目の得点を求める。
         *
         */
        /* 英語筆記 */
        if (rScore != null) {
            // リスニングが判定に採用されてない場合
            if (lScore == null) {
                score1Eng = toIntString(calcEnglishPoint(subCdR));
            } else {
                // 筆記 or リスニングの配点が未設定の場合
                if (wtgAllotBefore == JudgementConstants.Detail.ALLOT_NA || lngAllotBefore == JudgementConstants.Detail.ALLOT_NA) {
                    // 判定に採用された科目(筆記)の配点を設定
                    score1Eng = toIntString(calcEnglishPoint(subCdR));
                } else {
                    // 英語(筆記)の本人得点を算出
                    score1Eng = toIntString(calcEnglishPoint(wtgAllotBefore, English.PERFECT_SCORE_WRITING, score.getWriting().getPoint(), engPointAfter, engAllotBefore, engAllotAfter));
                }
            }
        }

        /* 英語リスニング */
        if (lScore != null) {
            // 筆記が判定に採用されてない場合
            if (rScore == null) {
                // 英語の配点を設定
                score1Lis = toIntString(calcEnglishPoint(subCdL));
            } else {
                // 筆記 or リスニングの配点が未設定の場合
                if (wtgAllotBefore == JudgementConstants.Detail.ALLOT_NA || lngAllotBefore == JudgementConstants.Detail.ALLOT_NA) {
                    // 判定に採用された科目(リスニング)の配点を設定
                    score1Lis = toIntString(calcEnglishPoint(subCdL));
                } else {
                    // 英語(リスニング)の本人得点を算出
                    score1Lis = toIntString(calcEnglishPoint(lngAllotBefore, English.PERFECT_SCORE_LISTENING, score.getListening().getPoint(), engPointAfter, engAllotBefore, engAllotAfter));
                }
            }
        }

        /* 数学 */
        score1Mat = toIntString(calcPoint(Detail.Math.COURSE_INDEX));

        /* 国語 */
        score1Jap = toIntString(calcPoint(Detail.Japanese.COURSE_INDEX));

        /* 理科 */
        score1Sci = toIntString(calcPoint(Detail.Science.COURSE_INDEX));

        /* 地歴公民 */
        score1Soc = toIntString(calcPoint(Detail.Social.COURSE_INDEX));
    }

    /**
     * BigDecimalを整数の文字列に変換します。
     *
     * @param point センタ本人得点
     * @return 整数の文字列
     */
    private String toIntString(BigDecimal point) {
        return point == null ? "" : Integer.toString(point.setScale(0, RoundingMode.HALF_UP).intValue());
    }

    /**
     * センタ英語の本人得点を算出します。
     *
     * @param subAllotBefore 科目大学配点(高得点操作前)
     * @param perfect センタ満点
     * @param subPointBefore 科目本人得点(高得点操作前)
     * @param pointAfter 英語本人得点(高得点操作後)
     * @param allotBefore 英語大学配点(高得点操作前)
     * @param allotAfter 英語大学配点(高得点操作後)
     * @return センタ英語の本人得点
     */
    private BigDecimal calcEnglishPoint(double subAllotBefore, double perfect, double subPointBefore, BigDecimal pointAfter, double allotBefore, BigDecimal allotAfter) {
        if (subAllotBefore > 0 && subAllotBefore != JudgementConstants.Detail.ALLOT_NA && allotBefore > 0 && subPointBefore > -1 && pointAfter != null && allotAfter != null) {
            return new BigDecimal(subPointBefore * subAllotBefore).multiply(allotAfter).divide(new BigDecimal(perfect * allotBefore), 0, RoundingMode.HALF_UP);
        } else {
            return null;
        }
    }

    /**
     * 指定した教科の本人得点を計算します。
     *
     * @param courseIndex 教科インデックス
     * @return 本人得点
     */
    private BigDecimal calcPoint(int courseIndex) {
        BigDecimal total = null;
        for (Iterator ite = getBean().getCAdoptedScore().iterator(); ite.hasNext();) {
            HandledPoint point = (HandledPoint) ite.next();
            if (point.getCourseIndex() == courseIndex && point.getAllot() > 0) {
                if (total == null) {
                    total = new BigDecimal(point.getPoint());
                } else {
                    total = total.add(new BigDecimal(point.getPoint()));
                }
            }
        }
        return total;
    }

    /**
     * センタ英語(リーディング or リスニング)の本人得点を算出します。
     *
     * @param subCd 科目コード
     * @return 本人得点
     */
    private BigDecimal calcEnglishPoint(String subCd) {
        BigDecimal total = null;
        for (Iterator ite = getBean().getCAdoptedScore().iterator(); ite.hasNext();) {
            HandledPoint point = (HandledPoint) ite.next();
            if (point.getCourseIndex() == Detail.English.COURSE_INDEX && point.getAllot() > 0) {
                if (point.getPairPoint() == null) {
                    if (subCd.equals(point.getSortKey())) {
                        total = new BigDecimal(point.getPoint());
                        break;
                    }
                } else {
                    for (Iterator itePair = point.getPairPoint().iterator(); itePair.hasNext();) {
                        HandledPoint pair = (HandledPoint) itePair.next();
                        if (subCd.equals(pair.getSortKey())) {
                            total = new BigDecimal(pair.getPoint());
                            break;
                        }
                    }
                }
            }
        }
        return total;
    }

    /**
     * 二次本人成績の情報を作成します。
     */
    private void executeSecondScore() {

        /* 英語 */
        score2Eng = toSecondScore(calcDeviation(Detail.English.COURSE_INDEX));

        /* 数学 */
        score2Mat = toSecondScore(calcDeviation(Detail.Math.COURSE_INDEX));

        /* 国語 */
        score2Jap = toSecondScore(calcDeviation(Detail.Japanese.COURSE_INDEX));

        /* 理科 */
        score2Sci = toSecondScore(calcDeviation(Detail.Science.COURSE_INDEX));

        /* 地歴公民 */
        score2Soc = toSecondScore(calcDeviation(Detail.Social.COURSE_INDEX));
    }

    /**
     * 二次本人偏差値を二次本人成績（文字列）に変換します。
     *
     * @param deviation 二次本人偏差値
     * @return 二次本人成績（文字列）
     */
    private String toSecondScore(double deviation) {
        return deviation > -1 ? Double.toString(deviation) : "";
    }

    /**
     * 指定した教科の本人偏差値を算出します。
     *
     * @param courseIndex 教科インデックス
     * @return 本人偏差値
     */
    private double calcDeviation(int courseIndex) {

        List list = new ArrayList(2);
        for (Iterator ite = getBean().getSAdoptedScore().iterator(); ite.hasNext();) {
            HandledPoint point = (HandledPoint) ite.next();
            if (point.getCourseIndex() == courseIndex && point.getAllot() > 0) {
                list.add(point);
            }
        }

        if (list.isEmpty()) {
            return -1;
        } else if (list.size() == 1) {
            return ((HandledPoint) list.get(0)).getPoint();
        } else {
            BigDecimal totalPoint = BigDecimal.valueOf(0);
            BigDecimal totalAllot = BigDecimal.valueOf(0);
            for (Iterator ite = list.iterator(); ite.hasNext();) {
                HandledPoint point = (HandledPoint) ite.next();
                BigDecimal allot = new BigDecimal(point.getAllot());
                totalPoint = totalPoint.add(new BigDecimal(point.getPoint()).multiply(allot));
                totalAllot = totalAllot.add(allot);
            }
            return totalPoint.divide(totalAllot, 1, RoundingMode.HALF_UP).doubleValue();
        }
    }

    /**
     * センタ大学配点の情報を作成します。
     *
     * @param score {@link CenterScore}
     * @param engAllotAfter 英語大学配点(高得点操作後)
     * @param wtgAllotBefore 英語筆記大学配点(高得点操作前)
     * @param lngAllotBefore 英語リスニング大学配点(高得点操作前)
     */
    private void executeCenterAllot(CenterScore score, BigDecimal engAllotAfter, double wtgAllotBefore, double lngAllotBefore) {

        HandledPoint rScore = getAdopted(JudgementProperties.getInstance().getMessage("mark.subcd.ewriting"));
        HandledPoint lScore = getAdopted(JudgementProperties.getInstance().getMessage("mark.subcd.elistening"));

        /* 英語筆記 */
        if (rScore != null) {
            // リスニングが判定に採用されてない場合
            if (lScore == null) {
                allot1Eng = toIntString(new BigDecimal(wtgAllotBefore));
            } else {
                // 筆記 or リスニングの配点が未設定の場合
                if (wtgAllotBefore == JudgementConstants.Detail.ALLOT_NA || lngAllotBefore == JudgementConstants.Detail.ALLOT_NA) {
                    // 判定に採用された科目(筆記)の配点を設定
                    allot1Eng = toIntString(new BigDecimal(rScore.getAllot()));
                } else {
                    // 英語(筆記)の本人得点を算出
                    allot1Eng = toIntString(engAllotAfter.multiply(new BigDecimal(wtgAllotBefore)).divide(new BigDecimal(wtgAllotBefore + lngAllotBefore), RoundingMode.HALF_UP));
                }
            }
        } else {
            allot1Eng = "";
        }

        /* 英語リスニング */
        if (lScore != null) {
            // リスニングが判定に採用されてない場合
            if (rScore == null) {
                allot1Lis = toIntString(new BigDecimal(lngAllotBefore));
            } else {
                // 筆記 or リスニングの配点が未設定の場合
                if (wtgAllotBefore == JudgementConstants.Detail.ALLOT_NA || lngAllotBefore == JudgementConstants.Detail.ALLOT_NA) {
                    // 判定に採用された科目(筆記)の配点を設定
                    allot1Lis = toIntString(new BigDecimal(lScore.getAllot()));
                } else {
                    // 英語(筆記)の本人得点を算出
                    allot1Lis = toIntString(engAllotAfter.multiply(new BigDecimal(lngAllotBefore)).divide(new BigDecimal(wtgAllotBefore + lngAllotBefore), RoundingMode.HALF_UP));
                }
            }
        } else {
            allot1Lis = "";
        }

        /* 数学 */
        allot1Mat = toIntString(calcAllot(Detail.Math.COURSE_INDEX, getBean().getCAdoptedScore()));

        /* 国語 */
        allot1Jap = toIntString(calcAllot(Detail.Japanese.COURSE_INDEX, getBean().getCAdoptedScore()));

        /* 理科 */
        allot1Sci = toIntString(calcAllot(Detail.Science.COURSE_INDEX, getBean().getCAdoptedScore()));

        /* 地歴公民 */
        allot1Soc = toIntString(calcAllot(Detail.Social.COURSE_INDEX, getBean().getCAdoptedScore()));
    }

    /**
     * 二次大学配点の情報を作成します。
     */
    private void executeSecondAllot() {

        /* 英語 */
        allot2Eng = toIntString(calcAllot(Detail.English.COURSE_INDEX, getBean().getSAdoptedScore()));

        /* 数学 */
        allot2Mat = toIntString(calcAllot(Detail.Math.COURSE_INDEX, getBean().getSAdoptedScore()));

        /* 国語 */
        allot2Jap = toIntString(calcAllot(Detail.Japanese.COURSE_INDEX, getBean().getSAdoptedScore()));

        /* 理科 */
        allot2Sci = toIntString(calcAllot(Detail.Science.COURSE_INDEX, getBean().getSAdoptedScore()));

        /* 地歴公民 */
        allot2Soc = toIntString(calcAllot(Detail.Social.COURSE_INDEX, getBean().getSAdoptedScore()));
    }

    /**
     * 指定した教科の大学配点を算出します。
     *
     * @param courseIndex 教科インデックス
     * @param adoptedScoreList 判定採用成績リスト
     * @return 大学配点
     */
    private BigDecimal calcAllot(int courseIndex, List adoptedScoreList) {
        BigDecimal total = null;
        for (Iterator ite = adoptedScoreList.iterator(); ite.hasNext();) {
            HandledPoint point = (HandledPoint) ite.next();
            if (point.getCourseIndex() == courseIndex && point.getAllot() > 0) {
                if (total == null) {
                    total = BigDecimal.valueOf(0);
                }
                total = total.add(new BigDecimal(point.getAllot()));
            }
        }
        return total;
    }

    /**
     * 指定した英語科目(R or L)の判定用操作得点データクラスを取得します。
     *
     * @param subCd 科目コード
     * @return
     */
    private HandledPoint getAdopted(String subCd) {
        List adoptedScoreList = getBean().getCAdoptedScore();
        for (Iterator ite = adoptedScoreList.iterator(); ite.hasNext();) {
            HandledPoint point = (HandledPoint) ite.next();
            // 教科が英語で配点が設定されている場合
            if (point.getCourseIndex() == Detail.English.COURSE_INDEX && point.getAllot() > 0) {
                // ペア科目が未設定の場合
                if (point.getPairPoint() == null) {
                    // 科目コードが一致する場合
                    if (subCd.equals(point.getSortKey())) {
                        return point;
                    }
                } else {
                    // ペア科目から対象となる科目を取得する
                    if (point.getPairPoint().size() == 1 && subCd.equals(point.getSortKey())) {
                        return point;
                    } else {
                        for (Iterator itePair = point.getPairPoint().iterator(); itePair.hasNext();) {
                            HandledPoint pair = (HandledPoint) itePair.next();
                            // 科目コードが一致する場合
                            if (subCd.equals(pair.getSortKey())) {
                                // 配点が未設定の場合、元の科目から配点を設定
                                if (JudgementConstants.Detail.ALLOT_NA == pair.getAllot()) {
                                    pair.setAllot(point.getAllot());
                                }
                                return pair;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * 出願締切日を返す
     * @return
     */
    public String getAppliDeadLine() {
        return appliDeadLine;
    }

    /**
     * 入学手続締切日を返す
     * @return
     */
    public String getProceDeadLine() {
        return proceDeadLine;
    }

    /**
     * 合格発表日を返す
     * @return
     */
    public String getSucAnnDate() {
        return sucAnnDate;
    }

    /**
     * 願書〆切日、合格発表日、手続き〆切日などの設定
     * @param univ
     */
    protected void setVariousDates(UnivData univ) {

        int count = 0;
        for (Iterator ite = univ.getSchedules().iterator(); ite.hasNext();) {

            ScheduleDetailData sdd = (ScheduleDetailData) ite.next();
            if (count != 0) {
                this.appliDeadLine += ",";
                this.sucAnnDate += ",";
                this.proceDeadLine += ",";
            }

            if (sdd.getAppliDeadline() != null) {
                this.appliDeadLine += FORMATTER.format(sdd.getAppliDeadline());
            }

            if (sdd.getSucAnnDate() != null) {
                this.sucAnnDate += FORMATTER.format(sdd.getSucAnnDate());
            }

            if (sdd.getProceDeadLine() != null) {
                this.proceDeadLine += FORMATTER.format(sdd.getProceDeadLine());
            }

            count++;
        }
    }

    /**
     * 校舎県コードを取得
     */
    public String getPrefCd_sh() {
        return prefCd_sh;
    }

    /**
     * 校舎件名を取得
     */
    public String getPrefName_sh() {
        return prefName_sh;
    }

    /**
     * 校舎県コードを設定
     */
    public void setPrefCd_sh(String string) {
        prefCd_sh = string;
    }

    /**
     * 校舎件名を設定
     */
    public void setPrefName_sh(String string) {
        prefName_sh = string;
    }

    /**
     * 相関関係で算出した日付文字列を返す
     * @return
     */
    public String getDateCorrelation() {
        return dateCorrelation;
    }

    /**
     * 分野リストを返す
     * @return
     */
    public List getRegionList() {
        return regionList;
    }

    /**
     * 中系統リストを返す
     * @return
     */
    public List getMStemmaList() {
        return mStemmaList;
    }

    /**
     * 小系統リストを返す
     * @return
     */
    public List getSStemmaList() {
        return sStemmaList;
    }

    /**
     * 入試定員信頼性を返す。
     *
     * @return 入試定員信頼性
     */
    public String getExamCapaRel() {
        return univ.getExamCapaRel();
    }

    /**
     * 本人成績：センタ：外国語を返します。
     *
     * @return 本人成績：センタ：外国語
     */
    public String getScore1Eng() {
        return score1Eng;
    }

    /**
     * 本人成績：センタ：リスニングを返します。
     *
     * @return 本人成績：センタ：リスニング
     */
    public String getScore1Lis() {
        return score1Lis;
    }

    /**
     * 本人成績：センタ：数学を返します。
     *
     * @return 本人成績：センタ：数学
     */
    public String getScore1Mat() {
        return score1Mat;
    }

    /**
     * 本人成績：センタ：国語を返します。
     *
     * @return 本人成績：センタ：国語
     */
    public String getScore1Jap() {
        return score1Jap;
    }

    /**
     * 本人成績：センタ：理科を返します。
     *
     * @return 本人成績：センタ：理科
     */
    public String getScore1Sci() {
        return score1Sci;
    }

    /**
     * 本人成績：センタ：地歴公民を返します。
     *
     * @return 本人成績：センタ：地歴公民
     */
    public String getScore1Soc() {
        return score1Soc;
    }

    /**
     * 本人成績：二次：外国語を返します。
     *
     * @return 本人成績：二次：外国語
     */
    public String getScore2Eng() {
        return score2Eng;
    }

    /**
     * 本人成績：二次：数学を返します。
     *
     * @return 本人成績：二次：数学
     */
    public String getScore2Mat() {
        return score2Mat;
    }

    /**
     * 本人成績：二次：国語を返します。
     *
     * @return 本人成績：二次：国語
     */
    public String getScore2Jap() {
        return score2Jap;
    }

    /**
     * 本人成績：二次：理科を返します。
     *
     * @return 本人成績：二次：理科
     */
    public String getScore2Sci() {
        return score2Sci;
    }

    /**
     * 本人成績：二次：地歴公民を返します。
     *
     * @return 本人成績：二次：地歴公民
     */
    public String getScore2Soc() {
        return score2Soc;
    }

    /**
     * 大学配点：センタ：リスニングを返します。
     *
     * @return 大学配点：センタ：リスニング
     */
    public String getAllot1Lis() {
        return allot1Lis;
    }

    /**
     * センター評価を返却します。
     *
     * @return
     */
    public String getCenterLetterJudgement() {
        String result;
        if (this.cLetterJudgement.trim().length() > 0) {
            if ("*".equals(this.cLetterJudgement.trim())) {
                result = "kome";
            } else {
                result = this.cLetterJudgement.toLowerCase().replaceAll("#", "s");
            }
        } else {
            result = "none";
        }
        return result;
    }

    /**
     * 二次評価を返却します。
     *
     * @return
     */
    public String getSecondLetterJudgement() {
        String result;
        if (this.sLetterJudgement.trim().length() > 0) {
            if ("*".equals(this.sLetterJudgement.trim())) {
                result = "kome";
            } else {
                result = this.sLetterJudgement.toLowerCase().replaceAll("#", "s");
            }
        } else {
            result = "none";
        }
        return result;
    }

    /**
     * 英語認定試験を含む評価（英語出願要件）を返却します。
     *
     * @return
     */
    public String getCenterLetterIncludeJudgement() {
        String result = this.cLetterJudgement.trim();
        switch (this.bean.getEngSSSutuGanHanTeiKbn()) {
            case 1:
                result += "○";
                break;
            case 2:
                result += "△";
                break;
            case 3:
                result += "×";
                break;
            default:
                break;
        }
        switch (this.bean.getEngSSHKSutuGanHTKokuchi()) {
            case 1:
                result += "！";
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * 英語認定試験出願資格判定区分を返却します。
     *
     * @return 英語認定試験出願資格判定区分
     */
    public int getEngPtHantei() {
        return this.bean.getEngSSSutuGanHanTeiKbn();
    }

    /**
     * 英語認定試験評価･出願基準非対応告知区分を返却します。
     *
     * @return 英語認定試験評価･出願基準非対応告知区分
     */
    public int getEngPtKokuchi() {
        return this.bean.getEngSSHKSutuGanHTKokuchi();
    }

    /**
     * 英語認定試験の配点を返却します。
     *
     * @return
     */
    public String getEngPtAllot() {
        String result = "";
        EngSSData engPt = this.bean.getEngSSData();
        if (engPt.getKatenKubun() == 1) {
            result += "+";
        }
        result += Long.toString(Math.round(engPt.getSankaExamSiboHaiten()));
        return result;
    }

    /**
     * 英語認定試験の得点を返却します。
     *
     * @return
     */
    public String getEngPtScore() {
        String result = "";
        EngSSData engPt = this.bean.getEngSSData();
        if ("1".equals(engPt.getTokutenKsRiyoKubun())) {
            result += "!";
        }
        result += Long.toString(Math.round(engPt.getSankaExamSiboTokuten()));
        return result;
    }

    /**
     * 英語認定試験が判定に採用されたかどうかを返却します。
     *
     * @return true:採用する、false:採用しない
     */
    public boolean isEngPtAdopt() {
        return this.bean.getEngSSData().getSankaExamSiboTokuten() != -1;
    }

    /**
     * 英語出願要件の利用有無を返却します。
     *
     * @return true:利用する、false:利用しない
     */
    public boolean isUseAppRequirement() {
        return !StringUtil.isEmptyTrim(univ.getAppReqPaternCd());
    }

    /**
     * 英語認定試験の得点換算利用有無を返却します。
     *
     * @return true:利用する、false:利用しない
     */
    public boolean isUseScoreConversion() {
        return univ.isSSUse() != 0;
    }

    /**
     * 英語＋Ｌの得点を返却します。
     *
     * @return
     */
    public String getScore1English() {
        return toIntString(calcPoint(Detail.English.COURSE_INDEX));
    }

    /**
     * 英語＋Ｌの配点を返却します。
     *
     * @return
     */
    public String getAllot1English() {
        return toIntString(calcAllot(Detail.English.COURSE_INDEX, getBean().getCAdoptedScore()));
    }
}

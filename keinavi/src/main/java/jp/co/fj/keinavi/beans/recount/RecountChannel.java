package jp.co.fj.keinavi.beans.recount;

import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.recount.data.RecountRequest;
import jp.co.fj.keinavi.util.KNCommonProperty;

/**
 *
 * 再集計処理スレッドの管理をするクラス
 * 
 * 2006.02.13	[新規作成]
 * 
 * 2007.08.07	校内成績処理システムより移植
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public final class RecountChannel {

	// Singleton
	private static final RecountChannel INSTANCE = new RecountChannel();
	
	// キュー
	private final List requestQueue = new LinkedList();
	// スレッドプールサイズ
	private final int size;
	// 再集計処理ワーカ
	private final RecountWorker[] worker;

	// チャンネルの停止要求があったかどうか
	private volatile boolean isRequestedTermination = false;
	
	private RecountChannel() {
		
		final int pSize = KNCommonProperty.getRecountThreadSize();
		if (pSize <= 0) {
			throw new IllegalArgumentException(
					"スレッドプールサイズは0以上で指定してください。");
		}
		size = pSize;
		worker = new RecountWorker[pSize];
	}

	/**
	 * @return インスタンス
	 */
	public static RecountChannel getInstance() {
		return INSTANCE;
	}
	
	/**
	 * スレッドプールの初期化を行う
	 * 
	 * @param dbDriver DBドライバ
	 * @param dbUrl DBurl
	 * @param dbUserId DBuserId
	 * @param dbPassword DBpassword
	 */
	public void init(final String dbDriver, final String dbUrl,
			final String dbUserId, final String dbPassword) {
		for (int i = 1; i <= size; i++) {
			worker[i - 1] = new RecountWorker("RECOUNT_WORKER_" + i,
					this, dbDriver, dbUrl, dbUserId, dbPassword);
		}
	}
	
	/**
	 * 再集計を依頼する
	 * 
	 * @param schoolCd 学校コード
	 */
	public synchronized void putRequest(final String schoolCd) {

		putRequest(new RecountRequest(schoolCd));
	}

	/**
	 * 再集計を依頼する
	 * 
	 * @param request 再集計依頼オブジェクト
	 */
	public synchronized void putRequest(
			final RecountRequest request) {

		// 停止処理中は受け付けられない
		if (isRequestedTermination()) {
			throw new IllegalStateException(
					"停止処理中のため再集計はできません。");
		}
		
		requestQueue.add(request);
		
		// リクエストを待っていたワーカを起こす
		notifyAll();
	}

	/**
	 * @return キューサイズ
	 */
	public synchronized int getQueueSize() {
		return requestQueue.size();
	}
	
	/**
	 * @return 再集計要求
	 */
	public synchronized RecountRequest takeRequest() {
		
		// キューが空っぽかつ停止依頼が出されていなければ待つ・・・
		while (requestQueue.size() == 0 && !isRequestedTermination()) {
			try {
				wait();
			} catch (InterruptedException e) {
				throw new InternalError(
						"再集計処理待ち中に割り込みがありました。");
			}
		}

		// 停止要求でwaitを抜けた場合はキューが空っぽの場合がある
		if (requestQueue.size() > 0) {
			return (RecountRequest) requestQueue.remove(0);
		} else {
			return null;
		}
	}
		
	/**
	 * スレッドプールの停止処理を行う
	 */
	public synchronized void terminate() {
		
		isRequestedTermination = true;
		
		// ワーカスレッドを起こす
		notifyAll();
	}
	
	/**
	 * @return チャンネルの停止要求があったかどうか
	 */
	public boolean isRequestedTermination() {
		return isRequestedTermination;
	}
	
}

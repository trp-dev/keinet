package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * 校内成績−成績概況データクラス
 * 作成日: 2004/06/28
 * @author	T.Sakai
 */
public class S11Item {
	//学校名
	private String strGakkomei = "";
	//県名
	private String strKenmei = "";
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//セキュリティスタンプフラグ
	private int intSecuFlg = 0;
	//コメントの受験型
	private String strComment = "";
	//表フラグ
	private int intHyouFlg = 0;
	//グラフフラグ
	private int intGraphFlg = 0;
	//型別データリスト
	private ArrayList s11KataList = new ArrayList();
	//科目別データリスト
	private ArrayList s11KmkList = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrKenmei() {
		return this.strKenmei;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public String getStrComment() {
		return this.strComment;
	}
	public int getIntHyouFlg() {
		return this.intHyouFlg;
	}
	public int getIntGraphFlg() {
		return this.intGraphFlg;
	}
	public ArrayList getS11KataList() {
		return this.s11KataList;
	}
	public ArrayList getS11KmkList() {
		return this.s11KmkList;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrKenmei(String strKenmei) {
		this.strKenmei = strKenmei;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setStrComment(String strComment) {
		this.strComment = strComment;
	}
	public void setIntHyouFlg(int intHyouFlg) {
		this.intHyouFlg = intHyouFlg;
	}
	public void setIntGraphFlg(int intGraphFlg) {
		this.intGraphFlg = intGraphFlg;
	}
	public void setS11KataList(ArrayList s11KataList) {
		this.s11KataList = s11KataList;
	}
	public void setS11KmkList(ArrayList s11KmkList) {
		this.s11KmkList = s11KmkList;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}

/*
 * 作成日: 2004/10/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.admin.name;

/*
 * $(#) $ld$
 *
 * All Rights Reserved, Copyright(c) 2004 FUJITSU KANSAI-CHUBU NETTECH LIMITED.
 *
 */
import java.io.*;
import java.util.*;
import java.text.*;
import jp.co.fj.keinavi.beans.admin.application.*;
/**
 *
 * ログ出力クラス
 *
 * @author $Author$
 * @author Yukari OKUMURA
 * @version $Revision: 1.00 $, $Date: 2004/10/30 17:30:00 $
 * @since 1.00
 *
 */
public class SystemLogWriter {

  	/** ログ出力レベル　デバッグ */
	public static final int DEBUG = 0;
  	/** ログ出力レベル　通知 */
	public static final int INFO = 1;
 	/** ログ出力レベル　エラー */
	public static final int ERROR = 2;
  	/** ログ出力レベル　警告 */
	public static final int WARNING = 3;

	private static boolean logWrite = false;
	private static PrintWriter pw = null;
	private static File logfile = null;

	/** ログ設置場所 */
	private static String path=null; 
	/** ディレクトリのデミリッタ */
	private static String sep = System.getProperty("file.separator");
	
	private static final SimpleDateFormat TIMESTAMP_FORMAT =
		new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
	
	/**
	 *
	 * クラスローダから呼び出されたら、初期設定を行う
	 *
	 */
	static {
		initialize();
	}

	/**
	 *
	 * 初期設定を行う
	 *
	 * @return void
	 */
	private static void initialize() {
		try {
			//プロパティファイルを使った初期化メソッドを呼び出す
			SystemLogWriter.initLogPath(new File(Application.getResourcesDir(), "common.properties"));

			// ログファイル
			logfile = new File(path + sep + "syslog"+ getFormatedTimeString("yyyyMMdd") + ".txt");

			// 一週間前のログファイルは消去
			new File(path + sep + "syslog" + getFormatedTimeString("yyyyMMdd", 31) + ".txt").delete();
			
			pw =
				new PrintWriter(
					new BufferedOutputStream(
						new FileOutputStream(logfile, true)
					)
				);
		} catch (IOException e) {
			throw new InternalError(e.getMessage());
		}
	}


	/**
	 *
	 * ログファイルに書込み。
	 *
	 * @param obj オブジェクト（クラス名取得用）
	 * @param massage メッセージ
	 * @param level ログレベル
	 * @return void
	 */
	public static synchronized void printLog(Object obj, String massage, int level) {
		SystemLogWriter.printLog(obj.getClass(), massage, level);	
	}

	/**
	 *
	 * ログファイルに書込み。
	 *
	 * @param obj オブジェクト（クラス名取得用）
	 * @param massage メッセージ
	 * @param level ログレベル
	 * @return void
	 */
	public static synchronized void printLog(
		Class obj,
		String massage,
		int level
		) {
		try {
			String str =
				getTimeStamp()
					+ " "
					+ obj.getName()
					+ " ["
					+ getLevelName(level)
					+ "] "
					+ ":"
					+ massage;
			if(logWrite ){
				pw.println(str);
				pw.flush();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	/**
	 *
	 * ファイルのクローズ処理。
	 *
	 * @return void
	 */
	public static synchronized void close() {
		try {
			pw.flush();
			pw.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 *
	 * ログレベルに準じた文字列を返す。
	 *
	 * @return ログレベル文字列を返します。
	 */
	private static String getLevelName(int level) {
		String name = null;
		switch (level) {
			case DEBUG :
				name = "DEBUG";
				break;
			case INFO :
				name = "INFO";
				break;
			case ERROR :
				name = "ERROR";
				break;
			case WARNING :
				name = "WARNING";
				break;
			default :
				name = "DEBUG";
				break;
		}
		return name;
	}
	

	/**
	*
	* タイムスタンプ
	*
	* @param index 書式
	* @return 現在の日付を設定された書式の文字型で返します。
	*/
	public static String getTimeStamp() {
		return TIMESTAMP_FORMAT.format(new Date(System.currentTimeMillis()));
	}


	/**
	*
	* タイムスタンプ
	*
	* @param index 書式
	* @return 現在の日付を設定された書式の文字型で返します。
	*/
	public static String getFormatedTimeString(String index) {
		Date currentTime = Calendar.getInstance().getTime();
		DateFormat form = new SimpleDateFormat(index);
		String timeStr = form.format(currentTime);
		return timeStr;
	}

	/**
	*
	* タイムスタンプ（今日時）
	*
	* @param index タイムスタンプ書式
	* @param diff 今日の日付との差分
	* @return 現在の日付を設定された書式の文字型で返します。
	*/
	public static String getFormatedTimeString(String index, int diff) {

		long diff_time = diff * 1000 * 60 * 60 * 24;
		Date currentTime =
			new Date((Calendar.getInstance().getTime().getTime()) - diff_time);
		DateFormat form = new SimpleDateFormat(index);
		String timeStr = form.format(currentTime);
		return timeStr;
	}
	
	/**
	 * ログのパスを初期化する
	 *
	 * @param propertyFile ファイル
	 * @return void
	 * @throws java.lang.RuntimeException dbconf.propertiesの読み込みに失敗した場合にスローされる。
	 * @exception java.lang.RuntimeException dbconf.propertiesの読み込みに失敗した場合にスローされる。
	 * @see java.lang.RuntimeException
	 */
	private static void initLogPath(File property) throws IOException {
		FileInputStream in = null;
		try {
			//-----------------------------------------
			// -- プロパティの取得
			//-----------------------------------------
			in = new FileInputStream(property);
			Properties props = new Properties();
			props.load(in);

			path = props.getProperty("KNLogRootPath");
			if (path == null){
				throw new IOException("KNLogRootPathプロパティが指定されていません。");
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new IOException("common.propertiesファイルが見つかりません。");
		} finally {
			if (in != null) in.close();
		}
	}

	/**
	 * @return
	 */
	public static boolean isLogWrite() {
		return logWrite;
	}

	/**
	 * @param b
	 */
	public static void setLogWrite(boolean b) {
		logWrite = b;
	}

}

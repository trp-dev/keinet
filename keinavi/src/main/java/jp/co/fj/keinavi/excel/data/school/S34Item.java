package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * 志望大学評価別人数（校内成績）データクラス
 * 作成日: 2004/07/08
 * @author	T.Kuzuno
 */
public class S34Item {
	//学校名
	private String strGakkomei = "";
	//模試名
	private String strMshmei = "";
	//模試実施基準日
	private String strMshDate = "";
	//模試コード
	private String strMshCd = "";
	//セキュリティスタンプフラグ
	private int intSecuFlg = 0;
	//大学集計区分フラグ
	private int intDaiTotalFlg = 0;
	//データリスト
	private ArrayList s34List = new ArrayList();
	//出力種別フラグ → 新テスト用に追加
	private int intShubetsuFlg = 0;

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public String getStrMshCd() {
		return this.strMshCd;
	}
	public int getIntSecuFlg() {
		return this.intSecuFlg;
	}
	public int getIntDaiTotalFlg() {
		return this.intDaiTotalFlg;
	}
	public ArrayList getS34List() {
		return this.s34List;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setStrMshCd(String strMshCd) {
		this.strMshCd = strMshCd;
	}
	public void setIntSecuFlg(int intSecuFlg) {
		this.intSecuFlg = intSecuFlg;
	}
	public void setIntDaiTotalFlg(int intDaiTotalFlg) {
		this.intDaiTotalFlg = intDaiTotalFlg;
	}
	public void setS34List(ArrayList s34List) {
		this.s34List = s34List;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}

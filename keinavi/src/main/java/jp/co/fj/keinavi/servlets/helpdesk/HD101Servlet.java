/*
 * 作成日: 2004/09/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.helpdesk;

import java.io.IOException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.helpdesk.CertificatesTsvCheckBean;
import jp.co.fj.keinavi.beans.helpdesk.HelpDeskRegistBean;
import jp.co.fj.keinavi.beans.helpdesk.UserListBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.helpdesk.HD101Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;


/**
 * 2005.02.18 Yoshimoto KAWAI - Totec
 *            デフォルトの契約区分をヘルプデスクにした
 *
 * @author nino
 *
 */
public class HD101Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ


		// セッション
		HttpSession session = request.getSession();

		// ログイン情報
		LoginSession login = super.getLoginSession(request);

		// アクションフォーム
		HD101Form hd101Form = null;
		// requestから取得する
		try {
			hd101Form = (HD101Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.helpdesk.HD101Form");
		} catch (Exception e) {
			throw new ServletException(e);
		}

		// 2015/10/22 QQ)Nishiyama デジタル証明書対応 ADD START
		// エラー通知用
		HelpDeskRegistBean hdrBean = null;
		CertificatesTsvCheckBean tsvCheckBean = null;
		// タイムスタンプ
		Date dt = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		// 2015/10/22 QQ)Nishiyama デジタル証明書対応 ADD END

		// HD101Form情報保持
		// HD101画面(ヘルプデスクリスト)から他の画面に遷移する場合
		HD101Form hd101FormTemp = null;
		hd101FormTemp = (HD101Form) session.getAttribute("hd101Form");
		if (hd101FormTemp == null) {
			session.setAttribute("hd101Form", hd101Form );
		}
		if ( ("hd101").equals(getBackward(request)) ) {
			session.setAttribute("hd101Form", hd101Form );
		} else if ( "hd002".equals(getBackward(request)) ) {
			// 転送元がヘルプデスクメニュー選択の場合
			hd101Form = new HD101Form();
			// デフォルトでヘルプデスクチェック
			hd101Form.setPactDiv(new String[]{"5"});

		}

		// 2015/10/28 QQ)Nishiyama デジタル証明書対応 ADD START
		// 新規登録画面に遷移
		if ( ("hd102").equals(getForward(request)) ) {
			dt = new Date();
			request.setAttribute("serverDate", format.format(dt));
		}
		// 2015/10/28 QQ)Nishiyama デジタル証明書対応 ADD END

		if(("hd101").equals(getForward(request)) || getForward(request) == null) {

			// HD101Form情報取得　
			// （HD002画面、HD101画面以外から戻ってきた場合）
			if ( !( ("hd002").equals(getBackward(request))  || "hd101".equals(getBackward(request)) ) ) {
				hd101Form = (HD101Form) session.getAttribute("hd101Form");
			}

			// 2015/10/21 QQ)Nishiyama デジタル証明書対応 ADD START
			// CSV登録画面から戻った
			if (("hd107").equals(getBackward(request)) ) {

				hdrBean = (HelpDeskRegistBean)session.getAttribute("HelpDeskRegistBean");

				// 一括登録エラー画面へ
				if (hdrBean.getErrorList().size() > 0) {
					request.setAttribute("HelpDeskRegistBean", hdrBean);
					super.forward(request, response, JSP_HD103);
					return;
				}

			} else {
				session.setAttribute("HelpDeskRegistBean", null);
			}
			// 2015/10/21 QQ)Nishiyama デジタル証明書対応 ADD END


			Connection con = null; // コネクション
			try {
				con = super.getConnectionPool(request); 	// コネクション取得
				UserListBean bean = new UserListBean();
				bean.setConnection(null, con);
				bean.setHD101Form(hd101Form);
				bean.execute();

				request.setAttribute("UserListBean", bean);
				// 2015/10/22 QQ)Nishiyama デジタル証明書対応 ADD START
				// 削除ユーザTSVの対象が存在しない場合、セッションから一覧を復元する
				session.setAttribute("UserListBean", bean);
				// 2015/10/22 QQ)Nishiyama デジタル証明書対応 ADD END

			} catch (Exception e) {
				throw new ServletException(e.toString());
			} finally {
				super.releaseConnectionPool(request, con); // コネクション解放
			}


			// アクションフォーム
			request.setAttribute("hd101Form", hd101Form);

			super.forward(request, response, JSP_HD101);

		// 2015/10/21 QQ)Nishiyama デジタル証明書対応 ADD START
		// TSV出力処理
		} else if(("sheet").equals(getForward(request))) {

			// TSV出力前のチェック
			Connection con = null; // コネクション
			try {
				con = super.getConnectionPool(request); 	// コネクション取得
				tsvCheckBean = new CertificatesTsvCheckBean();
				tsvCheckBean.setConnection(null, con);
				tsvCheckBean.setHD101Form(hd101Form);
				tsvCheckBean.execute();

				// 処理結果を取得
				hdrBean = tsvCheckBean.getHdrBean();
				hdrBean.setErrPattern("0");

			} catch (Exception e) {
				throw new ServletException(e.toString());
			} finally {
				super.releaseConnectionPool(request, con); // コネクション解放
			}

			// 一括登録エラー画面へ
			if (hdrBean.getErrorList().size() > 0) {
				request.setAttribute("HelpDeskRegistBean", hdrBean);
				super.forward(request, response, JSP_HD103);

			// 削除ユーザなしの通知用
			} else if (tsvCheckBean.getIsDelUserExist() == false){
				// 一覧画面へ
				hd101Form.setDelUserProc("1");
				request.setAttribute("hd101Form", hd101Form);
				request.setAttribute("UserListBean", (UserListBean)session.getAttribute("UserListBean"));
				super.forward(request, response, JSP_HD101);

			} else {
				// ダウンロードマネージャーに転送
				session.setAttribute("hd101Form", hd101Form );
				super.forward(request, response, SERVLET_DISPATCHER);
			}
		// 2015/10/21 QQ)Nishiyama デジタル証明書対応 ADD END


		// 不明なら転送
		} else {

			super.forward(request, response, SERVLET_DISPATCHER);
		}


	}



}

package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 統合処理における再集計対象登録Bean
 * 
 * 2006.11.15	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class IntegrationRecountBean extends MAnswerSheetRecountBean {

	// 統合前個人ID
	private String beforeIndividualId;
	// 統合後個人ID
	private String afterIndividualId;
	
	/**
	 * @see jp.co.fj.keinavi.beans.maintenance.MAnswerSheetRecountBean#initTargetList()
	 */
	protected void initTargetList() throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("m31"));
			ps.setString(1, beforeIndividualId);
			ps.setString(2, afterIndividualId);
			ps.setString(3, afterIndividualId);
			ps.setString(4, beforeIndividualId);
			rs = ps.executeQuery();
			while (rs.next()) {
				addTarget(rs.getString(1), rs.getString(2),
						rs.getInt(3), rs.getString(4));
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * @param pAfterIndividualId 設定する afterIndividualId。
	 */
	public void setAfterIndividualId(String pAfterIndividualId) {
		super.setAfterIndividualId(pAfterIndividualId);
		afterIndividualId = pAfterIndividualId;
	}

	/**
	 * @param pBeforeIndividualId 設定する beforeIndividualId。
	 */
	public void setBeforeIndividualId(String pBeforeIndividualId) {
		beforeIndividualId = pBeforeIndividualId;
	}
	
}

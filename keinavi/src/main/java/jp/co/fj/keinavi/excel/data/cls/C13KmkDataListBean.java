package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;

/**
 * �N���X���ъT���|�l���ѐ��ڌ^�E�Ȗڕʃf�[�^�N���X
 * �쐬��: 2004/07/14
 * @author	H.Fujimoto
 */
public class C13KmkDataListBean {
	//�w�N
	private String strGrade = "";
	//�N���X
	private String strClass = "";
	//�^�E�Ȗڃ��X�g
	private ArrayList c13KmkList = new ArrayList();


	/*----------*/
	/* Get      */
	/*----------*/
	
	public ArrayList getC13KmkList() {
		return this.c13KmkList;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrGrade() {
		return this.strGrade;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	
	public void setC13KmkList(ArrayList c13KmkList) {
		this.c13KmkList = c13KmkList;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}

}
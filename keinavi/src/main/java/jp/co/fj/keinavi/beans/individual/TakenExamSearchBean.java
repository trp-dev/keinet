package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.IExamData;

import com.fjh.beans.DefaultSearchBean;

/**
 *
 * 指定の生徒が受けたことのある模試を取得
 * 
 * 2004/10/26    [新規作成]
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 * 2005.3.8 Totec)T.Yamada    [1]I302Servletの[2]を受け継ぐ
 * 2006.9.4 Totec)T.Yamada    [2]受験学力測定対応
 * 
 */
public class TakenExamSearchBean extends DefaultSearchBean{
	
	private static final long serialVersionUID = 7318158726120489647L;
	
	/**
	 * 個人ＩＤ
	 */
	private String individualId;
    
    /**
     * 模試年度
     */
    private String examYear;
    
    /**
     * 外部開放日の上限
     */
    private String outOpenDate;
    
    /**
     * 内部開放日の上限
     */
    private String inOpenDate;
    
    /**
     * ソートフラグ
     */
    private int order = 0;
    
    /**
     * 利用不可模試種類コード
     */
    private String[] examTypeCDNot;
    
	/**
	 * [1] add
	 * 年度制限
	 */
	private String limitedYear;
    
	/**
	 * 模試セッション(利用可能模試)
	 */
	private ExamSession examSession;
    
	/**
	 * 利用不可模試コード
	 */
	private String[] excludeExamCds;
    
	/**
	 * 利用可能模試タイプコード
	 */
	private String[] examTypeCds;

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
        
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			
            ps = conn.prepareStatement(createQuery());
			if (individualId != null) {
				ps.setString(1, individualId);
			}
			
            rs = ps.executeQuery();
            
			while (rs.next()) {

				recordSet.add(new IExamData(
                        rs.getString("examyear"), 
                        rs.getString("examcd"), 
                        rs.getString("examname"), 
                        rs.getString("examname_abbr"), 
                        rs.getString("examtypecd"), 
                        rs.getString("examst"), 
                        rs.getString("examdiv"), 
                        rs.getString("tergetgrade"), 
                        rs.getString("inpledate"), 
                        rs.getString("in_dataopendate"), 
                        rs.getString("out_dataopendate"), 
                        rs.getString("dockingexamcd"), 
                        rs.getString("dockingtype"), 
                        rs.getString("dispsequence"), 
                        rs.getString("masterdiv")));
			}

		} finally {
            DbUtils.closeQuietly(null, ps, rs);
		}
	}
    
    private String createQuery() {
        
        final StringBuffer buf = new StringBuffer();
        
        buf.append("SELECT DISTINCT "+
        " /*+ INDEX(I PK_SUBRECORD_I) */ "+
        "E.EXAMYEAR "+",E.EXAMCD "+",E.EXAMNAME "+",E.EXAMNAME_ABBR "+",E.EXAMTYPECD "+",E.EXAMST "+
        ",E.EXAMDIV "+",E.TERGETGRADE "+",E.INPLEDATE "+",E.IN_DATAOPENDATE "+",E.OUT_DATAOPENDATE "+",E.DOCKINGEXAMCD "+
        ",E.DOCKINGTYPE "+",E.DISPSEQUENCE "+",E.MASTERDIV "+
        "FROM SUBRECORD_I I "+
        "INNER JOIN EXAMINATION E ON E.EXAMYEAR = I.EXAMYEAR AND E.EXAMCD = I.EXAMCD "+
        "WHERE 1=1 ");
        
        if (examTypeCds != null && examTypeCds.length > 0) {
            buf.append(" AND ( ");
            for (int i=0; i<examTypeCds.length; i++) {
                if (i != 0) {
                    buf.append(" OR ");
                }
                buf.append("E.EXAMTYPECD = '"+examTypeCds[i]+"' ");
            }
            buf.append(" ) ");
        }
        
        if (individualId != null) {
            buf.append(" AND I.INDIVIDUALID = ? ");
        }
        
        if (examSession != null) {
            buf.append(examSession.getExamOptionQuery());
        }
        
        if (excludeExamCds != null && excludeExamCds.length > 0) {
            buf.append(" AND E.EXAMCD NOT IN ( ");
            for(int i=0; i<excludeExamCds.length; i++){
                if(i != 0)
                    buf.append(",");
                buf.append("'"+excludeExamCds[i]+"' ");
            }
            buf.append(") ");
        }
        
        if(limitedYear != null){
            buf.append("AND TO_NUMBER(I.EXAMYEAR) <= "+limitedYear+" ");
        }
        
        //対象年度
        if (examYear != null) {
            buf.append(" AND e.examyear = '" + examYear + "' ");
        }
        
        //外部データ開放日の上限
        if (outOpenDate != null) {
            buf.append(" AND  e.out_dataopendate <= '" + outOpenDate + "' ");
        }
        
        //内部データ開放日の上限
        if (inOpenDate != null) {
            buf.append(" AND  e.in_dataopendate <= '" + inOpenDate + "' ");
        }
        
        //利用不可模試種類コード
        if (examTypeCDNot != null) {
            
            buf.append(" AND e.examtypecd NOT IN ( ");
            
            for (int i = 0; i < examTypeCDNot.length; i++) {
                if (i != 0) {
                    buf.append(" , ");
                }
                buf.append(" '");
                buf.append(examTypeCDNot[i]);
                buf.append("' ");
            }

            buf.append(" ) ");
        }
        
        //ソートタイプ１(データ開放日の降順、模試コードの降順)
        if (order == 1) {
            
            buf.append(" ORDER BY ");
            
            //外部データ開放日
            if (outOpenDate != null) {
                buf.append(" out_dataopendate DESC, ");
            //内部データ開放日    
            } else if (inOpenDate != null) {
                buf.append(" in_dataopendate DESC, ");
            //エラー    
            } else {
                throw new IllegalArgumentException("このソート条件では外部開放日または内部開放日が設定されていません。");
            }
            
            buf.append(" examcd DESC ");
        }
        
        return buf.toString();
    }

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}

	/**
	 * @return
	 */
	public ExamSession getExamSession() {
		return examSession;
	}

	/**
	 * @param session
	 */
	public void setExamSession(ExamSession session) {
		examSession = session;
	}

	/**
	 * @return
	 */
	public String[] getExcludeExamCds() {
		return excludeExamCds;
	}

	/**
	 * @param strings
	 */
	public void setExcludeExamCds(String[] strings) {
		excludeExamCds = strings;
	}

	/**
	 * @return
	 */
	public String[] getExamTypeCds() {
		return examTypeCds;
	}

	/**
	 * @param strings
	 */
	public void setExamTypeCds(String[] strings) {
		examTypeCds = strings;
	}

	/**
	 * @return
	 */
	public String getLimitedYear() {
		return limitedYear;
	}

	/**
	 * @param string
	 */
	public void setLimitedYear(String string) {
		limitedYear = string;
	}

    /**
     * @param pExamYear 模試年度
     */
    public void setExamYear(String pExamYear) {
        examYear = pExamYear;
    }

    /**
     * @param pInOpenDate 内部データ開放日の上限
     */
    public void setInOpenDate(String pInOpenDate) {
        inOpenDate = pInOpenDate;
    }

    /**
     * @param pOutOpenDate 外部データ開放日の上限
     */
    public void setOutOpenDate(String pOutOpenDate) {
        outOpenDate = pOutOpenDate;
    }

    /**
     * @param pOrder ソート条件
     */
    public void setOrder(int pOrder) {
        order = pOrder;
    }

    /**
     * @param pExamTypeCDNot 利用不可模試種類コード
     */
    public void setExamTypeCDNot(String[] pExamTypeCDNot) {
        examTypeCDNot = pExamTypeCDNot;
    }

}

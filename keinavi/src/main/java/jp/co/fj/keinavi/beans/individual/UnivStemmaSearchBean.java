/*
 * 作成日: 2004/11/05
 *
 * 更新日: 2009/10/01
 * 更新者: TOTEC)長谷
 * 内容　: 「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 * 　　　　「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.individual.UnivStemmaData;

import com.fjh.beans.DefaultSearchBean;

/**
 * @author Administrator
 *
 * 大学コードを入力して、その大学が所属する系統を取得
 */
public class UnivStemmaSearchBean extends DefaultSearchBean {
	
	private String year;
	private String univCd;
	private String facultyCd;
	private String deptCd;
	
	private String query = ""+
	"SELECT DISTINCT U.YEAR, U.UNIVCD, U.FACULTYCD, U.DEPTCD, U.STEMMADIV"+
	",S1.SSTEMMACD"+",S2.MSTEMMACD"+",S3.REGIONCD"+ 
	",S1.SSTEMMANAME"+",S2.MSTEMMANAME"+",S3.REGIONNAME"+ 
	" FROM UNIVSTEMMA U "+
	"LEFT JOIN STEMMA S1 ON U.STEMMADIV = '1' AND SUBSTR(U.STEMMACD, 3, 4) = S1.SSTEMMACD "+
	"LEFT JOIN STEMMA S2 ON U.STEMMADIV = '2' AND SUBSTR(U.STEMMACD, 3, 4) = S2.MSTEMMACD "+
	"LEFT JOIN STEMMA S3 ON U.STEMMADIV = '3' AND U.STEMMACD = S3.REGIONCD "+
	"WHERE 1 = 1 ";

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		if(year == null){//最新の年度を指定
			query += "AND U.YEAR = (SELECT MAX(EVENTYEAR) FROM UNIVMASTER_BASIC) ";
		}else{
			query += "AND U.YEAR ='"+year+"' ";	
		}
		if(univCd != null){
			query += "AND U.UNIVCD ='"+univCd+"' ";	
		}
		if(facultyCd != null){
			query += "AND U.FACULTYCD = '"+facultyCd+"' ";	
		}
		if(deptCd != null){
			query += "AND U.DEPTCD = '"+deptCd+"' ";	
		}
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while(rs.next()){
				UnivStemmaData usd = new UnivStemmaData();
				usd.setYear(rs.getString("YEAR"));
				usd.setUnivCd(rs.getString("UNIVCD"));
				usd.setFacultyCd(rs.getString("FACULTYCD"));
				usd.setDeptCd(rs.getString("DEPTCD"));
				usd.setStemmaDiv(rs.getString("STEMMADIV"));
				//usd.setHumaFlg();
				//usd.setSciFlg();
				usd.setRegionCd(rs.getString("REGIONCD"));
				usd.setRegionName(rs.getString("REGIONNAME"));
				usd.setSStemmaCd(rs.getString("SSTEMMACD"));
				usd.setSStemmaName(rs.getString("SSTEMMANAME"));
				usd.setMStemmaCd(rs.getString("MSTEMMACD"));
				usd.setMStemmaName(rs.getString("MSTEMMANAME"));
				
				recordSet.add(usd);				
			}
		}catch(SQLException sqle){
			System.out.println(query);
			throw sqle;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
		
	}

	/**
	 * @return
	 */
	public String getDeptCd() {
		return deptCd;
	}

	/**
	 * @return
	 */
	public String getFacultyCd() {
		return facultyCd;
	}

	/**
	 * @return
	 */
	public String getUnivCd() {
		return univCd;
	}

	/**
	 * @return
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param string
	 */
	public void setDeptCd(String string) {
		deptCd = string;
	}

	/**
	 * @param string
	 */
	public void setFacultyCd(String string) {
		facultyCd = string;
	}

	/**
	 * @param string
	 */
	public void setUnivCd(String string) {
		univCd = string;
	}

	/**
	 * @param string
	 */
	public void setYear(String string) {
		year = string;
	}

}

/*
 * 作成日: 2004/07/16
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.txt_out.factory;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.forms.AbstractActionFormFactory;
import jp.co.fj.keinavi.forms.txt_out.TMaxForm;
import jp.co.fj.keinavi.util.KNUtil;

/**
 * テキスト出力のアクションフォーム生成時に
 * 共通する処理を記述する。
 * 
 * 2005.02.15 Yoshimoto KAWAI - Totec
 *            前画面の対象模試を引き継ぐように変更
 * 
 * @author kawai
 * 
 * */
public abstract class AbstractTFormFactory extends AbstractActionFormFactory {

	/**
	 * TMaxFormを生成する
	 * @param request
	 * @return
	 */
	protected TMaxForm createTMaxForm(HttpServletRequest request) {
		TMaxForm form = new TMaxForm();

		// 模試セッション
		ExamSession examSession = (ExamSession) request.getAttribute(ExamSession.SESSION_KEY);

		// 対象模試
		ExamData exam =
			examSession.getExamData(
				request.getParameter("targetYear"),
				request.getParameter("targetExam")
			);
		
		// 対象模試がなければ最新にする
		if (exam == null) {
			exam = KNUtil.getLatestExamData(examSession);
		}

		// さらになければ空オブジェクト
		if (exam == null) {
			exam = new ExamData();
		}

		// 対象年度・模試は最新
		form.setTargetYear(exam.getExamYear());
		form.setTargetExam(exam.getExamCD());

		return form;
	}
}
package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.sheet.SheetSchoolData;
import jp.co.fj.keinavi.excel.data.school.S42BnpListBean;
import jp.co.fj.keinavi.excel.data.school.S42GakkoListBean;
import jp.co.fj.keinavi.excel.data.school.S42Item;
import jp.co.fj.keinavi.excel.data.school.S42ListBean;
import jp.co.fj.keinavi.excel.school.S42;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 校内成績分析 - 他校比較 - 偏差値分布
 * 
 * 
 * 2004.08.04	[新規作成]
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * 2005.04.21	Yoshimoto KAWAI - Totec
 * 				新テスト対応
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class S42SheetBean extends AbstractSheetBean {

	// データクラス
	private final S42Item data = new S42Item();

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		final String[] code = getSubjectCDArray(); // 型・科目設定値
		final List graph = getSubjectGraphList(); // 型・科目グラフ表示対象
		final List compSchoolList = getCompSchoolList(); // 比較対象高校

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntNendoFlg(getIntFlag(IProfileItem.PREV_DISP)); // 過年度の表示
		data.setIntSecuFlg(getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(getOutputType()); // 出力種別フラグ
	
		// 表
		switch (getIntFlag(IProfileItem.CHART_COMP_RATIO)) {
			case  1: data.setIntHyouFlg(0); data.setIntKoseihiFlg(1); break; 
			case  2: data.setIntHyouFlg(0); data.setIntKoseihiFlg(2); break; 
			case 11: data.setIntHyouFlg(1); data.setIntKoseihiFlg(1); break; 
			case 12: data.setIntHyouFlg(1); data.setIntKoseihiFlg(2); break;
		}

		// 偏差値帯別度数分布グラフ
		switch (getIntFlag(IProfileItem.GRAPH_DIST)) {
			case  1: data.setIntBnpGraphFlg(0); data.setIntAxisFlg(1); break; 
			case  2: data.setIntBnpGraphFlg(0); data.setIntAxisFlg(2); break; 
			case 11: data.setIntBnpGraphFlg(1); data.setIntAxisFlg(1); break; 
			case 12: data.setIntBnpGraphFlg(1); data.setIntAxisFlg(2); break;
		}

		// 偏差値帯別人数積み上げグラフ
		switch (getIntFlag(IProfileItem.GRAPH_BUILDUP)) {
			case  1: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(1); break; 
			case  2: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(2); break; 
			case  3: data.setIntNinzuGraphFlg(0); data.setIntNinzuPitchFlg(3); break; 
			case 11: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(1); break; 
			case 12: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(2); break;
			case 13: data.setIntNinzuGraphFlg(1); data.setIntNinzuPitchFlg(3); break;
		}

		// 偏差値帯別構成比グラフ
		switch (getIntFlag(IProfileItem.GRAPH_COMP_RATIO)) {
			case  1: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(1); break; 
			case  2: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(2); break; 
			case  3: data.setIntKoseiGraphFlg(0); data.setIntKoseiPitchFlg(3); break; 
			case 11: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(1); break; 
			case 12: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(2); break;
			case 13: data.setIntKoseiGraphFlg(1); data.setIntKoseiPitchFlg(3); break;
		}
	
		// ワークテーブルのセットアップ
		insertIntoExistsExam();
		insertIntoSubCDTrans(new ExamData[]{exam});
	
		// 年度の配列（過年度の表示フラグにより異なる）
		final String[] years;
		switch (data.getIntNendoFlg()) {
			// 今年度のみ
			case 1: 
				years = new String[] { exam.getExamYear() };
				break;
				
			// 前年度まで
			case 2: 
				years = new String[] {
					exam.getExamYear(),
					String.valueOf(Integer.parseInt(exam.getExamYear()) - 1)
				};
				data.setIntNendoFlg(getExistsExamCount(years));
				break;
				
			// 前々年度まで
			case 3:
				years = new String[] {
					exam.getExamYear(),
					String.valueOf(Integer.parseInt(exam.getExamYear()) - 1),
					String.valueOf(Integer.parseInt(exam.getExamYear()) - 2)
				};
				data.setIntNendoFlg(getExistsExamCount(years));
				break;
				
			// パラメータエラー
			default:
				throw new IllegalArgumentException("過年度の表示の設定値が不正です。");
		}

		PreparedStatement ps1 = null;
		PreparedStatement ps2a = null;
		PreparedStatement ps2b = null;
		PreparedStatement ps2c = null;
		PreparedStatement ps2d = null;
		PreparedStatement ps3a = null;
		PreparedStatement ps3b = null;
		PreparedStatement ps3c = null;
		PreparedStatement ps3d = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		try {
			// データリスト
			{
				Query query = QueryLoader.getInstance().load("s22_1");
				query.setStringArray(1, code); // 型・科目コード

				ps1 = conn.prepareStatement(query.toString());
				ps1.setString(1, exam.getExamYear()); // 対象年度
				ps1.setString(2, exam.getExamCD()); // 対象模試
			}

			// 学校データリスト（自校）
			{
				Query query = QueryLoader.getInstance().load("s42_1_1");
				query.setStringArray(1, years); // 模試年度

				ps2a = conn.prepareStatement(query.toString());
				ps2a.setString(2, profile.getBundleCD()); // 一括コード
				ps2a.setString(3, exam.getExamCD()); // 模試コード
			}
		
			// 学校データリスト（全国）
			{
				Query query = QueryLoader.getInstance().load("s42_1_2");
				query.setStringArray(1, years); // 対象年度
		
				ps2b = conn.prepareStatement(query.toString());
				ps2b.setString(2, getMinStudentDiv()); // 現役高卒区分
				ps2b.setString(3, exam.getExamCD()); // 模試コード
			}

			// 学校データリスト（県）
			{
				Query query = QueryLoader.getInstance().load("s42_1_3");
				query.setStringArray(1, years); // 対象年度
		
				ps2c = conn.prepareStatement(query.toString());
				ps2c.setString(3, exam.getExamCD()); // 模試コード
			}

			// 学校データリスト（他校）
			{
				Query query = QueryLoader.getInstance().load("s42_1_4");
				outDate2InDate(query); // データ開放日
				query.setStringArray(1, years); // 対象年度
		
				ps2d = conn.prepareStatement(query.toString());
				ps2d.setString(3, exam.getExamCD()); // 模試コード
			}

			// 偏差値分布（自校）
			ps3a = conn.prepareStatement(getQueryWithDeviationZone("s42_2_1").toString());
			ps3a.setString(3, profile.getBundleCD()); // 一括コード
	
			// 偏差値分布（全国）
			ps3b = conn.prepareStatement(getQueryWithDeviationZone("s42_2_2").toString());
	
			// 偏差値分布（県）
			ps3c = conn.prepareStatement(getQueryWithDeviationZone("s42_2_3").toString());
	
			// 偏差値分布（他校）
			ps3d = conn.prepareStatement(getQueryWithDeviationZone("s42_2_4").toString());
	
			List c1 = new LinkedList(); // 型の入れ物
			List c2 = new LinkedList(); // 科目の入れ物
				
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				S42ListBean bean = new S42ListBean();
				bean.setStrKmkCd(rs1.getString(1));
				bean.setStrKmkmei(rs1.getString(2));
				bean.setIntDispKmkFlg(graph.contains(rs1.getString(1)) ? 1 : 0);
				bean.setStrHaitenKmk(rs1.getString(3));
				
				if (rs1.getInt(1) >= 7000) c1.add(bean); // 型
				else c2.add(bean); // 科目

				// 型・科目コード
				ps2a.setString(1, bean.getStrKmkCd());
				ps2b.setString(1, bean.getStrKmkCd());
				ps2c.setString(2, bean.getStrKmkCd());
				ps2d.setString(2, bean.getStrKmkCd());
	
				// 自校
				{
					rs2 = ps2a.executeQuery();
					while (rs2.next()) {
						S42GakkoListBean g = new S42GakkoListBean();
						g.setStrGakkomei(profile.getBundleName());
						g.setStrNendo(rs2.getString(1));
						g.setIntNinzu(rs2.getInt(3));
						g.setFloHeikin(rs2.getFloat(4));
						g.setFloHensa(rs2.getFloat(5));

						// 対象年度なら1					
						if (exam.getExamYear().equals(rs2.getString(1))) {
							g.setIntDispGakkoFlg(1);
						// そうでなければ0
						} else {
							g.setIntDispGakkoFlg(0);
						}

						bean.getS42GakkoList().add(g);						

						// 模試年度
						ps3a.setString(1, g.getStrNendo());
						// 模試コード
						ps3a.setString(2, rs2.getString(6));
						// 型・科目コード
						ps3a.setString(4, rs2.getString(2));
					
						rs3 = ps3a.executeQuery();
						while (rs3.next()) {
							S42BnpListBean b = new S42BnpListBean();
							b.setFloBnpMax(rs3.getFloat(1));
							b.setFloBnpMin(rs3.getFloat(2));
							b.setIntNinzu(rs3.getInt(3));
							b.setFloKoseihi(rs3.getFloat(4));
							g.getS42BnpList().add(b);							
						}
						rs3.close();					
					}
					rs2.close();
				}

				// 比較対象高校分繰り返す
				Iterator ite = compSchoolList.iterator();
				while (ite.hasNext()) {
					SheetSchoolData school = (SheetSchoolData) ite.next();

					switch (school.getSchoolTypeCode()) {
						// 全国
						case 1:
							rs2 = ps2b.executeQuery();
							break;
							
						// 県
						case 2:
							ps2c.setString(1, school.getPrefCD()); // 県コード
							rs2 = ps2c.executeQuery();
							break;
						
						// 高校
						case 3:
							ps2d.setString(1, school.getSchoolCD()); // 一括コード
							rs2 = ps2d.executeQuery();
							break;
					}

					while (rs2.next()) {
						S42GakkoListBean g = new S42GakkoListBean();
						g.setStrGakkomei(rs2.getString(1));
						g.setStrNendo(rs2.getString(2));
						g.setIntNinzu(rs2.getInt(4));
						g.setFloHeikin(rs2.getFloat(5));
						g.setFloHensa(rs2.getFloat(6));

						// 対象年度なら設定値			
						if (exam.getExamYear().equals(rs2.getString(2))) {
							// 高校なら設定値
							if (school.getSchoolTypeCode() == 3) {
								g.setIntDispGakkoFlg(school.getGraphFlag());

							// 全国・県は出力対象なら２								
							} else {
								g.setIntDispGakkoFlg(school.getGraphFlag() == 1 ? 2 : 0);
							}
														
						// そうでなければ0で固定
						} else {
							g.setIntDispGakkoFlg(0);
						}

						bean.getS42GakkoList().add(g);			

						switch (school.getSchoolTypeCode()) {
							// 全国
							case 1:
								ps3b.setString(1, rs2.getString(2)); // 模試年度
								ps3b.setString(2, rs2.getString(7)); // 模試コード
								ps3b.setString(3, rs2.getString(3)); // 型・科目コード
								rs3 = ps3b.executeQuery();
								break;
							
							// 県
							case 2:
								ps3c.setString(1, rs2.getString(2)); // 模試年度			
								ps3c.setString(2, rs2.getString(7)); // 模試コード
								ps3c.setString(3, school.getPrefCD()); // 県コード			
								ps3c.setString(4, rs2.getString(3)); // 型・科目コード
								rs3 = ps3c.executeQuery();
								break;
						
							// 高校
							case 3:
								ps3d.setString(1, rs2.getString(2)); // 模試年度			
								ps3d.setString(2, rs2.getString(7)); // 模試コード
								ps3d.setString(3, school.getSchoolCD()); // 一括コード			
								ps3d.setString(4, rs2.getString(3)); // 型・科目コード
								rs3 = ps3d.executeQuery();
								break;
						}

						while (rs3.next()) {
							S42BnpListBean b = new S42BnpListBean();
							b.setFloBnpMax(rs3.getFloat(1));
							b.setFloBnpMin(rs3.getFloat(2));
							b.setIntNinzu(rs3.getInt(3));
							b.setFloKoseihi(rs3.getFloat(4));
							g.getS42BnpList().add(b);							
						}
						rs3.close();	
					}
					rs2.close();
				}
			}

			// 型・科目の順番で入れる
			data.getS42List().addAll(c1);
			data.getS42List().addAll(c2);
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(rs3);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2a);
			DbUtils.closeQuietly(ps2b);
			DbUtils.closeQuietly(ps2c);
			DbUtils.closeQuietly(ps2d);
			DbUtils.closeQuietly(ps3a);
			DbUtils.closeQuietly(ps3b);
			DbUtils.closeQuietly(ps3c);
			DbUtils.closeQuietly(ps3d);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S42_01");
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_OTHER_DEV;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S42().s42(data, outfileList, getAction(), sessionKey, sheetLog);
	}

}

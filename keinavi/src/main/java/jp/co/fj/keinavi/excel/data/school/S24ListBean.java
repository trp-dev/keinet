package jp.co.fj.keinavi.excel.data.school;

import java.util.*;

/**
 * �쐬��: 2004/07/13
 * @author C.Murata
 *
 * �Z�����ѕ��́|�ߔN�x��r�|�u�]��w�]���ʐl�� �f�[�^���X�g
 */
public class S24ListBean {
	//���������敪
	private String strGenKouKbn = "";
	//��w�R�[�h
	private String strDaigakuCd = "";
	//��w��
	private String strDaigakuMei = "";
	//�w���R�[�h
	private String strGakubuCd = "";
	//�w����
	private String strGakubuMei = "";
	//�w�ȃR�[�h
	private String strGakkaCd = "";
	//�w�Ȗ�
	private String strGakkaMei = "";
	//�����R�[�h
	private String strNtiCd = "";
	//����
	private String strNittei = "";
	//�]���敪
	private String strHyouka = "";
	//�]���ʐl���f�[�^���X�g
	private ArrayList s24HyoukaNinzuList = new ArrayList();

	/*
	 * GET
	 */
	public ArrayList getS24HyoukaNinzuList() {
		return s24HyoukaNinzuList;
	}
	public String getStrDaigakuCd() {
		return strDaigakuCd;
	}
	public String getStrDaigakuMei() {
		return strDaigakuMei;
	}
	public String getStrGakkaCd() {
		return strGakkaCd;
	}
	public String getStrGakkaMei() {
		return strGakkaMei;
	}
	public String getStrGakubuCd() {
		return strGakubuCd;
	}
	public String getStrGakubuMei() {
		return strGakubuMei;
	}
	public String getStrGenKouKbn() {
		return strGenKouKbn;
	}
	public String getStrHyouka() {
		return strHyouka;
	}
	public String getStrNtiCd() {
		return strNtiCd;
	}
	public String getStrNittei() {
		return strNittei;
	}

	/*
	 * SET
	 */
	public void setS24HyoukaNinzuList(ArrayList list) {
		s24HyoukaNinzuList = list;
	}
	public void setStrDaigakuCd(String string) {
		strDaigakuCd = string;
	}
	public void setStrDaigakuMei(String string) {
		strDaigakuMei = string;
	}
	public void setStrGakkaCd(String string) {
		strGakkaCd = string;
	}
	public void setStrGakkaMei(String string) {
		strGakkaMei = string;
	}
	public void setStrGakubuCd(String string) {
		strGakubuCd = string;
	}
	public void setStrGakubuMei(String string) {
		strGakubuMei = string;
	}
	public void setStrGenKouKbn(String string) {
		strGenKouKbn = string;
	}
	public void setStrHyouka(String string) {
		strHyouka = string;
	}
	public void setStrNtiCd(String string) {
		strNtiCd = string;
	}
	public void setStrNittei(String string) {
		strNittei = string;
	}

}

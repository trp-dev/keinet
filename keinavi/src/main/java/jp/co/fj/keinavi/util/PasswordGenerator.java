package jp.co.fj.keinavi.util;

import java.security.SecureRandom;

/**
 *
 * パスワード生成クラス
 *
 * 2005.10.11	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class PasswordGenerator {

	// 候補文字列
	private final String candidate;
	// 乱数生成クラスインスタンス
	final SecureRandom random = new SecureRandom();

	/**
	 * @param candidate
	 */
	public PasswordGenerator(final String candidate) {

		if (candidate == null) {
			throw new IllegalArgumentException("候補文字列がNULLです。");
		}

		this.candidate = candidate ;
	}

	/**
	 * @param length
	 * @return
	 */
	public String generate(final int length) {

		if (length <= 0) {
			throw new IllegalArgumentException("長さは0以上である必要があります。");
		}

		final StringBuffer buff = new StringBuffer();

		for (int i=0; i<length; i++) {
			buff.append(candidate.charAt(random.nextInt(candidate.length())));
		}

		return buff.toString();
	}

}

/*
 * 作成日: 2004/07/23
 * 成績推移グラフ（設問別推移）の配点・得点データ取得用Bean
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.fj.keinavi.data.individual.ExamComboData;

/**
 * 設問別成績グラフ<BR>
 * DBより偏差値を取得し、すべてのデータをコレクションに入れる。
 * @author TOTEC.ishiguro
 *
 */
public class I109Bean extends I108Bean {
	
	private String COMBO_SQL_BASE = "";

	private String comboSql;			//コンボＳＱＬ

	private ExamComboData examComboData;
	private boolean helpflg;			//ログイン情報
	private boolean isComboData;		//コンボデータがあるか

	/**
	 * 初回アクセス時は受験した科目一覧を取得する必要があります
	 * 一度目は、コンボボックスの選択項目と、コンボボックスの検索条件が一緒。
	 * execute()を実行する前に条件をしていする
	 * @param String year	対象模試年度
	 * @param String cd	対象模試コード
	 * @param String id	対象生徒ＩＤ
	 */
	public void setSearchConditionA(String year, String cd, String preYear, String preCd, String id){
		//キホンデータ設定
		setTargetExamYear(preYear);
		setTargetExamCode(preCd);
		setPersonId(id);
		
		//コンボＳＱＬの作成
		setComboSql(COMBO_SQL_BASE);
		appendComboSql(getComboCondition(year, cd, id));
	}
	/**
	 * 二回目以降のアクセス時
	 * execute()を実行する前に条件をしていする
	 * @param String year	対象模試年度
	 * @param String cd	対象模試コード
	 * @param String id	対象生徒ＩＤ
	 * @param String selectYear	選択模試年度
	 * @param String selectCd		選択模試コード
	 */
	public void setSearchConditionB(String year, String cd, String id, String selectYear, String selectCd){
		//キホンデータ設定
		setTargetExamYear(selectYear);
		setTargetExamCode(selectCd);
		setPersonId(id);
		
		//コンボＳＱＬの作成
		setComboSql(COMBO_SQL_BASE);
		appendComboSql(getComboCondition(year, cd, id));
	}

	/**
	 * コンボボックスを取得するSQL文にStringを付加する
	 * @param string
	 */
	private void appendComboSql(String string){
		if(getComboSql() == null)
			setComboSql("");
		comboSql += string;
	}
	/**
	 * 模試コンボボックスのリスト検索用ＳＱＬ取得
	 * @param String 対象模試年度
	 * @param String 対象個人ＩＤ
	 * @param String 対象模試コード
	 * @return String コンボリストＳＱＬ
	 */
	private String getComboCondition(String year, String cd, String id){
		
		String help1 = "";
		String help2 = "";
		String help3 = "";
		if(helpflg == true){
			help1 = "EXAMINATION.IN_DATAOPENDATE, ";
			help2 = "AND T2.IN_DATAOPENDATE < T3.IN_DATAOPENDATE ";
			help3 = "GROUP BY " +
					"T2.EXAMYEAR , " +
					"T2.EXAMCD , " +
					"T2.EXAMNAME , " +
					"T2.DISPSEQUENCE , " +
					"T2.IN_DATAOPENDATE " +
					"ORDER BY " +
					"T2.IN_DATAOPENDATE DESC";
			
		}else{
			help1 = "EXAMINATION.OUT_DATAOPENDATE, ";
			help2 = "AND T2.OUT_DATAOPENDATE < T3.OUT_DATAOPENDATE ";
			help3 = "GROUP BY " +
					"T2.EXAMYEAR , " +
					"T2.EXAMCD , " +
					"T2.EXAMNAME , " +
					"T2.DISPSEQUENCE , " +
					"T2.OUT_DATAOPENDATE " +
					"ORDER BY " +
					"T2.OUT_DATAOPENDATE DESC";
		}
		
		String sqlOption = "SELECT /*+ INDEX(T1 PK_SUBRECORD_I) */ T2.EXAMYEAR, T2.EXAMCD, T2.EXAMNAME, T2.DISPSEQUENCE "+
							"FROM " +
								"SUBRECORD_I T1 INNER JOIN EXAMINATION T2 " +
								"ON T1.EXAMYEAR = T2.EXAMYEAR " +
								"AND T1.EXAMCD = T2.EXAMCD ," +
								"(" +
									"SELECT " +
										"" + help1 + "" +
										"EXAMINATION.EXAMTYPECD " +
									"FROM " +
										"EXAMINATION " +
									"WHERE " +
										"EXAMINATION.EXAMYEAR = '" + year + "' " +
										"AND EXAMINATION.EXAMCD = '" + cd + "' " +
								") T3 " +
							"WHERE " +
								"T1.INDIVIDUALID = '" + id + "' " +
								"" + help2 + "" +
								"AND T2.EXAMTYPECD = T3.EXAMTYPECD " +
								"" + help3 + "";
		return sqlOption;
	}
	


	/**
	 * ＤＢより　対象年度、対象模試　コンボボックスの内容を取得します。
	 * @return
	 * @throws SQLException
	 */
	private ExamComboData execExamComboData() throws Exception{

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = conn.prepareStatement(getComboSql());
			rs = pstmt.executeQuery();
			ExamComboData examComboData = new ExamComboData();
			while(rs.next()){
				ExamComboData.YearData yearData = (ExamComboData.YearData) examComboData.get(rs.getString("EXAMYEAR"));
				if(yearData == null){			
					yearData = examComboData.getNewYearData();
					yearData.setYear(rs.getString("EXAMYEAR"));
					yearData.setExamDatas(new ArrayList());
					examComboData.addYearData(yearData);
				}
				yearData.addExam(rs.getString("EXAMYEAR"), rs.getString("EXAMCD"), rs.getString("EXAMNAME"));
				//ComboDataのフラグをオン
				setComboData(true);
			}
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
			
			return examComboData;
			
		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}

	/**
	 * @return
	 */
	public String getComboSql() {
		return comboSql;
	}

	/**
	 * @param string
	 */
	public void setComboSql(String string) {
		comboSql = string;
	}

	/**
	 * @return
	 */
	public ExamComboData getExamComboData() {
		return examComboData;
	}

	/**
	 * @param data
	 */
	public void setExamComboData(ExamComboData data) {
		examComboData = data;
	}

	/**
	 * @return
	 */
	public boolean isComboData() {
		return isComboData;
	}

	/**
	 * @param b
	 */
	public void setComboData(boolean b) {
		isComboData = b;
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		//コンボ情報取得
		setExamComboData(execExamComboData());
		
		/*----------科目リスト／設問情報の取得----------*/
		//模試、科目を取得する条件の設定
		super.setSearchCondition(getTargetExamYear(),getTargetExamCode(), getPersonId());
		//科目／模試の検索
		super.execute();
	}
	/**
	 * @return
	 */
	public boolean isHelpflg() {
		return helpflg;
	}

	/**
	 * @param b
	 */
	public void setHelpflg(boolean b) {
		helpflg = b;
	}

}
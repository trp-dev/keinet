/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.individual;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class I401Form extends IOnJudgeForm{

	private String targetGrade;
	
	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	/**
	 * @return targetGrade を戻します。
	 */
	public String getTargetGrade() {
		return targetGrade;
	}

	/**
	 * @param pTargetGrade 設定する targetGrade。
	 */
	public void setTargetGrade(String pTargetGrade) {
		targetGrade = pTargetGrade;
	}

}

package jp.co.fj.keinavi.excel.data.school;

import java.util.ArrayList;
/**
 * �Z�����ѕ��́|�S���������ъT���f�[�^�N���X
 * �쐬��: 2004/07/16
 * @author	H.Fujimoto
 */
public class S01Item {
	//�͎���
	private String strMshmei = "";
	//�󌱎ґ���
	private int intAllNinzu = 0;
	//����l��
	private int intGenNinzu = 0; 
	//�����l��
	private int intSotuNinzu = 0;
	//�͎��t���O
	private int intMshFlg = 0;
	//�^�ʐ��у��X�g
	private ArrayList s01KataList = new ArrayList();
	//�Ȗڕʐ��у��X�g
	private ArrayList s01KmkList = new ArrayList();
	//�o�͎�ʃt���O �� �V�e�X�g�p�ɒǉ�
	private int intShubetsuFlg = 0;
	
	
	/*----------*/
	/* Get      */
	/*----------*/
	
	public int getIntAllNinzu() {
		return this.intAllNinzu;
	}
	public int getIntGenNinzu() {
		return this.intGenNinzu;
	}
	public int getIntMshFlg() {
		return this.intMshFlg;
	}
	public int getIntSotuNinzu() {
		return this.intSotuNinzu;
	}
	public ArrayList getS01KataList() {
		return this.s01KataList;
	}
	public ArrayList getS01KmkList() {
		return this.s01KmkList;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public int getIntShubetsuFlg() {
		return this.intShubetsuFlg;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setIntAllNinzu(int intAllNinzu) {
		this.intAllNinzu = intAllNinzu;
	}
	public void setIntGenNinzu(int intGenNinzu) {
		this.intGenNinzu = intGenNinzu;
	}
	public void setIntMshFlg(int intMshFlg) {
		this.intMshFlg = intMshFlg;
	}
	public void setIntSotuNinzu(int intSotuNinzu) {
		this.intSotuNinzu = intSotuNinzu;
	}
	public void setS01KataList(ArrayList s01KataList) {
		this.s01KataList = s01KataList;
	}
	public void setS01KmkList(ArrayList s01KmkList) {
		this.s01KmkList = s01KmkList;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setIntShubetsuFlg(int intShubetsuFlg) {
		this.intShubetsuFlg = intShubetsuFlg;
	}

}
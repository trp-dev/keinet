package jp.co.fj.keinavi.beans.security;

/**
 *
 * 機能IDの定義クラス
 * 
 * 2005.09.29	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public interface IMenuDefine {
	
	/**
	 * メニューID（校内成績分析）
	 */
	String MENU_SCHOOL = "100";
	/**
	 * メニューID（校内成績分析・校内成績）
	 */
	String MENU_SCHOOL_SCORE = "101";
	/**
	 * メニューID（校内成績分析・過年度比較）
	 */
	String MENU_SCHOOL_PREV = "102";
	/**
	 * メニューID（校内成績分析・クラス比較）
	 */
	String MENU_SCHOOL_CLASS = "103";
	/**
	 * メニューID（校内成績分析・他校比較）
	 */
	String MENU_SCHOOL_OTHER = "104";
	/**
	 * メニューID（校内成績分析・過回比較）
	 */
	String MENU_SCHOOL_PAST = "105";
	/**
	 * メニューID（高校成績分析）
	 */
	String MENU_BUSINESS = "200";
	/**
	 * メニューID（高校成績分析・高校間比較）
	 */
	String MENU_BUSINESS_COMP = "201";
	/**
	 * メニューID（高校成績分析・過回比較）
	 */
	String MENU_BUSINESS_PAST = "202";
	/**
	 * メニューID（クラス成績分析）
	 */
	String MENU_CLASS = "300";
	/**
	 * メニューID（クラス成績分析・クラス成績概況）
	 */
	String MENU_CLASS_SCORE = "301";
	/**
	 * メニューID（クラス成績分析・クラス比較）
	 */
	String MENU_CLASS_COMP = "302";
	/**
	 * メニューID（個人成績分析）
	 */
	String MENU_INDIVIDUAL = "400";
	/**
	 * メニューID（テキスト出力）
	 */
	String MENU_TEXT = "500";
	/**
	 * メニューID（テキスト出力・集計データ）
	 */
	String MENU_TEXT_SUM = "501";
	/**
	 * メニューID（テキスト出力・集計データ・他校比較）
	 */
	String MENU_TEXT_OTHER = "502";
	/**
	 * メニューID（テキスト出力・集計データ・クラス比較）
	 */
	String MENU_TEXT_CLASS = "503";
	/**
	 * メニューID（テキスト出力・個人データ）
	 */
	String MENU_TEXT_INDIVIDUAL = "504";
	/**
	 * メニューID（テキスト出力・入試結果調査）
	 */
	String MENU_TEXT_RESULT = "505";
	/**
	 * メニューID（共通項目設定・対象年度）
	 */
	String MENU_COMMON_YEAR = "603";
	/**
	 * メニューID（共通項目設定・比較対象クラス）
	 */
	String MENU_COMMON_CLASS = "605";
	/**
	 * メニューID（共通項目設定・比較対象高校）
	 */
	String MENU_COMMON_SCHOOL = "606";
	/**
	 * メニューID（共通項目設定・担当クラス）
	 */
	String MENU_COMMON_CHARGE = "607";
	/**
	 * メニューID（管理者メニュー）
	 */
	String MENU_MAINTE = "700";
	/**
	 * メニューID（管理者メニュー・模試データ再集計）
	 */
	String MENU_MAINTE_RECOUNT = "701";
	/**
	 * メニューID（管理者メニュー・生徒情報管理）
	 */
	String MENU_MAINTE_INFO = "702";
	/**
	 * メニューID（管理者メニュー・受験届修正）
	 */
	String MENU_MAINTE_ANSWER = "703";
	/**
	 * メニューID（管理者メニュー・複合クラス管理）
	 */
	String MENU_MAINTE_CLASS = "704";
	/**
	 * メニューID（Kei-Navi）
	 */
	String MENU_KEINAVI = "900";
	/**
	 * メニューID（校内成績処理システム）
	 */
	String MENU_KOUNAI = "1000";
	/**
	 * メニューID（成績分析）
	 */
	String MENU_KOUNAI_SCORE = "1100";
	/**
	 * メニューID（入試結果情報）
	 */
	String MENU_KOUNAI_ENT_EXAM = "1200";

}

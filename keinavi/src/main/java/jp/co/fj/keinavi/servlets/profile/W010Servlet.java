package jp.co.fj.keinavi.servlets.profile;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.profile.ProfileBean;
import jp.co.fj.keinavi.beans.profile.ProfileFolderBean;
import jp.co.fj.keinavi.beans.profile.ProfileUpdateBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.data.profile.ProfileFolder;
import jp.co.fj.keinavi.forms.profile.W010Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class W010Servlet extends DefaultHttpServlet {

	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// ログイン情報
		LoginSession login = super.getLoginSession(request);

		// アクションフォーム
		W010Form form = (W010Form)super.getActionForm(request,
			"jp.co.fj.keinavi.forms.profile.W010Form");

		// 「登録して閉じる」
		if ("w010".equals(super.getBackward(request))) {
			Connection con = null; // DBコネクション
			try {
				con = super.getConnectionPool(request);
				con.setAutoCommit(false);

				// プロファイルを取得する
				Profile profile = null;
				{
					ProfileBean bean = new ProfileBean();
					bean.setConnection(null, con);
					bean.setLoginSession(login);
					bean.setProfileID(form.getProfileID());
					bean.setBundleCD(form.getCountingDivCode());
					bean.execute();
				
					// 書き換える
					profile = bean.getProfile();
					profile.setProfileName(form.getProfileName());
					profile.setFolderID(form.getCurrent());
					profile.setComment(form.getComment());

					// 公開するかどうか
					if ("1".equals(form.getPub())) {
						profile.setSectorSortingCD(null);
					} else {
						profile.setSectorSortingCD(login.getSectorSortingCD());
					}
				}

				// プロファイルUpdateBean
				{
					ProfileUpdateBean bean = new ProfileUpdateBean();
					bean.setConnection(null, con);
					bean.setProfile(profile);
					bean.setLoginSession(login);
					bean.execute();
				}

				// コミット
				con.commit();
				//	アクセスログ
				actionLog(request, "203", form.getProfileID());
			} catch (Exception e) {
				super.rollback(con);
				throw new ServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}
			super.forward(request, response, JSP_CLOSE);

		// 通常アクセス
		} else {
			Connection con = null; // DBコネクション
			try {
				con = super.getConnectionPool(request);

				// プロファイルフォルダBean
				ProfileFolderBean bean = new ProfileFolderBean();
				bean.setConnection(null, con);
				bean.setLoginSession(login);
				bean.setBundleCD(form.getCountingDivCode());
				bean.execute();
				request.setAttribute("ProfileFolderBean", bean);

				// 編集対象となるフォルダ
				ProfileFolder folder = 
					(ProfileFolder) bean.getFolderList().get(
						bean.getFolderList().indexOf(new ProfileFolder(form.getCurrent())));					

				request.setAttribute("folder", folder);

				// プロファイル
				request.setAttribute("profile",
					folder.getProfileList().get(
						folder.getProfileList().indexOf(new Profile(form.getProfileID()))));

			} catch (Exception e) {
				throw new ServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}

			request.setAttribute("form", form);

			super.forward(request, response, JSP_W010);
		}

	}

}

package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−データリスト
 * 作成日: 2004/07/14
 * @author	H.Fujimoto
 */
public class C12ListBean {
	//学年
	private String strGrade = "";
	//クラス
	private String strClass = "";
	//成績表データリスト
	private ArrayList c12SeisekiList = new ArrayList();

	/*----------*/
	/* Get      */
	/*----------*/
	
	public ArrayList getC12SeisekiList() {
		return this.c12SeisekiList;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrGrade() {
		return this.strGrade;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	
	public void setC12SeisekiList(ArrayList c12SeisekiList) {
		this.c12SeisekiList = c12SeisekiList;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}

}
/**
 * �Z�����ѕ��́|�Z�����с@�L�q�n�͎����ݖ�ʐ���
 *      �L�q�n�͎����ݖ�ʐ��� �f�[�^�N���X
 * �쐬��: 2019/09/25
 * @author      M.Ooseto
 */

package jp.co.fj.keinavi.excel.data.school;

public class S13RecordListBean {
    // �w�Z��
    private String bundleName = "";
    // �Ώ͎ۖ���
    private String examName = "";
    // �ȖڃR�[�h
    private String subCd = "";
    // ���Ȗ�
    private String subName = "";
    // �ݖ�ԍ�
    private String questionNo = "";
    // �ݖ���e
    private String questionName = "";
    // ����ԍ�
    private String kwebShoquestionName = "";
    // �z�_
    private int kwebShoquestionPnt = 0;
    // �Z�����ρ|���ϓ_
    private float ssAvgPnt = 0;
    // �Z�����ρ|���_��
    private float ssAvgScoreRate = 0;
    // �S�����ρ|���ϓ_
    private float saAvgPnt = 0;
    // �S�����ρ|���_��
    private float saAvgScoreRate = 0;
    // �m���Z�\
    private String acAdemicCd1 = "";
    // �v�l�́E���f��
    private String acAdemicCd2 = "";
    // �\����
    private String acAdemicCd3 = "";

    /*-----*/
    /* Get */
    /*-----*/
    public String getBundleName() {
        return this.bundleName;
    }
    public String getExamName() {
        return this.examName;
    }
    public String getSubCd() {
        return this.subCd;
    }
    public String getSubName() {
        return this.subName;
    }
    public String getQuestionNo() {
        return this.questionNo;
    }
    public String getQuestionName() {
        return this.questionName;
    }
    public String getKwebShoquestionName() {
        return this.kwebShoquestionName;
    }
    public int getKwebShoquestionPnt() {
        return this.kwebShoquestionPnt;
    }
    public float getSsAvgPnt() {
        return this.ssAvgPnt;
    }
    public float getSsAvgScoreRate() {
        return this.ssAvgScoreRate;
    }
    public float getSaAvgPnt() {
        return this.saAvgPnt;
    }
    public float getSaAvgScoreRate() {
        return this.saAvgScoreRate;
    }
    public String getAcAdemicCd1() {
        return this.acAdemicCd1;
    }
    public String getAcAdemicCd2() {
        return this.acAdemicCd2;
    }
    public String getAcAdemicCd3() {
        return this.acAdemicCd3;
    }

    /*-----*/
    /* Set */
    /*-----*/
    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }
    public void setExamName(String examName) {
        this.examName = examName;
    }
    public void setSubCd(String subCd) {
        this.subCd = subCd;
    }
    public void setSubName(String subName) {
        this.subName = subName;
    }
    public void setQuestionNo(String questionNo) {
        this.questionNo = questionNo;
    }
    public void setQuestionName(String questionName) {
        this.questionName = questionName;
    }
    public void setKwebShoquestionName(String kwebShoquestionName) {
        this.kwebShoquestionName = kwebShoquestionName;
    }
    public void setKwebShoquestionPnt(int kwebShoquestionPnt) {
        this.kwebShoquestionPnt = kwebShoquestionPnt;
    }
    public void setSsAvgPnt(float ssAvgPnt) {
        this.ssAvgPnt = ssAvgPnt;
    }
    public void setSsAvgScoreRate(float ssAvgScoreRate) {
        this.ssAvgScoreRate = ssAvgScoreRate;
    }
    public void setSaAvgPnt(float saAvgPnt) {
        this.saAvgPnt = saAvgPnt;
    }
    public void setSaAvgScoreRate(float saAvgScoreRate) {
        this.saAvgScoreRate = saAvgScoreRate;
    }
    public void setAcAdemicCd1(String acAdemicCd1) {
        this.acAdemicCd1 = acAdemicCd1;
    }
    public void setAcAdemicCd2(String acAdemicCd2) {
        this.acAdemicCd2 = acAdemicCd2;
    }
    public void setAcAdemicCd3(String acAdemicCd3) {
        this.acAdemicCd3 = acAdemicCd3;
    }
}

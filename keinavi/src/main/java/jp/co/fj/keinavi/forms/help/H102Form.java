/*
 * 作成日: 2004/10/14
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.help;

import com.fjh.forms.ActionForm;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class H102Form extends ActionForm {

	private String  title    =null; // タイトル
	private String  text     =null; // 本文
	private String  name     =null; // 名前
	private String  telNumber=null; // 電話番号
	private String  mailAddr =null; // メールアドレス


	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ

	}

	/**
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param string
	 */
	public void setTitle(String string) {
		title = string;
	}

	/**
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param string
	 */
	public void setText(String string) {
		text = string;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param string
	 */
	public void setName(String string) {
		name = string;
	}

	/**
	 * @return
	 */
	public String getTelNumber() {
		return telNumber;
	}

	/**
	 * @param string
	 */
	public void setTelNumber(String string) {
		telNumber = string;
	}

	/**
	 * @return
	 */
	public String getMailAddr() {
		return mailAddr;
	}

	/**
	 * @param string
	 */
	public void setMailAddr(String string) {
		mailAddr = string;
	}

}

/*
 * 作成日: 2004/08/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class I502Form extends ActionForm{

	private String targetPersonId;
	private String individualId;
	private String koIndividualId;
	private String button;
	private String year;				//作成年
	private String month;				//作成月
	private String date;				//作成日
	private String title;				//タイトル
	private String text;				//テキスト
	private String firstRegistrYdt; 	//キー：初期登録日時
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.individual.ICommonForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ
		
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param string
	 */
	public void setText(String string) {
		text = string;
	}

	/**
	 * @param string
	 */
	public void setTitle(String string) {
		title = string;
	}

	/**
	 * @return
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @return
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @return
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param string
	 */
	public void setDate(String string) {
		date = string;
	}

	/**
	 * @param string
	 */
	public void setMonth(String string) {
		month = string;
	}

	/**
	 * @param string
	 */
	public void setYear(String string) {
		year = string;
	}

	/**
	 * @return
	 */
	public String getButton() {
		return button;
	}

	/**
	 * @param string
	 */
	public void setButton(String string) {
		button = string;
	}

	/**
	 * @return
	 */
	public String getFirstRegistrYdt() {
		return firstRegistrYdt;
	}

	/**
	 * @param string
	 */
	public void setFirstRegistrYdt(String string) {
		firstRegistrYdt = string;
	}

	/**
	 * @return individualId を戻します。
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @param pIndividualId 設定する individualId。
	 */
	public void setIndividualId(String pIndividualId) {
		individualId = pIndividualId;
	}

	/**
	 * @return koIndividualId を戻します。
	 */
	public String getKoIndividualId() {
		return koIndividualId;
	}

	/**
	 * @param pKoIndividualId 設定する koIndividualId。
	 */
	public void setKoIndividualId(String pKoIndividualId) {
		koIndividualId = pKoIndividualId;
	}

}

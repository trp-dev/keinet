package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * バランスチャート用フォーム
 * @author ishiguro
 *
 * 2005.4.26 	K.Kondo 	[1]画面スクロール座標の追加
 * 2009.10.27   Fujito URAKAWA - Totec
 *              「学力レベル」項目を生かした新メニュー対応 
 */
public class I108Form extends ActionForm {

	private String mode;

	private String targetPersonId;		//個人ＩＤ
	private String targetExamCode;		//模試コード
	private String targetExamYear;		//模試年度
	private String targetSubName;	//科目名
	private String targetSubCode;	//科目コード
	
	//個人成績分析共通-印刷対象生徒（判定対象生徒)
	private String printStudent;
	
	//成績分析共通：印刷対象
	private String[] printTarget;
	
	//セキュリティスタンプ
	private String printStamp;
	
	//印刷ボタン押下時パラメータ
	private String printStatus;
	
	private String dispSelect;
	
	private String scrollX;//[1] add サブミット前、表示座標Ｘ
	private String scrollY;//[1] add サブミット前、表示座標Ｙ
	
	
	private String targetScholarLevel;	// 対象生徒・対象科目学力レベル
	private String dispScholarLevel;	// 画面表示学力レベル
	
	private String dispSubCode;		// 画面表示科目コード
	
	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	/**
	 * @return
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @return
	 */
	public String getTargetSubCode() {
		return targetSubCode;
	}

	/**
	 * @return
	 */
	public String getTargetSubName() {
		return targetSubName;
	}

	/**
	 * @param string
	 */
	public void setMode(String string) {
		mode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @param string
	 */
	public void setTargetSubCode(String string) {
		targetSubCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetSubName(String string) {
		targetSubName = string;
	}

	/**
	 * @return
	 */
	public String[] getPrintTarget() {
		return printTarget;
	}

	/**
	 * @param strings
	 */
	public void setPrintTarget(String[] strings) {
		printTarget = strings;
	}

	/**
	 * @return
	 */
	public String getPrintStamp() {
		return printStamp;
	}

	/**
	 * @param string
	 */
	public void setPrintStamp(String string) {
		printStamp = string;
	}

	/**
	 * @return
	 */
	public String getPrintStudent() {
		return printStudent;
	}

	/**
	 * @param string
	 */
	public void setPrintStudent(String string) {
		printStudent = string;
	}

	/**
	 * @return
	 */
	public String getPrintStatus() {
		return printStatus;
	}

	/**
	 * @param string
	 */
	public void setPrintStatus(String string) {
		printStatus = string;
	}

	/**
	 * @return
	 */
	public String getDispSelect() {
		return dispSelect;
	}

	/**
	 * @param string
	 */
	public void setDispSelect(String string) {
		dispSelect = string;
	}
	
	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[1] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[1] add
	}

	public String getTargetScholarLevel() {
		return targetScholarLevel;
	}

	public String getDispScholarLevel() {
		return dispScholarLevel;
	}

	public void setTargetScholarLevel(String targetScholarLevel) {
		this.targetScholarLevel = targetScholarLevel;
	}

	public void setDispScholarLevel(String dispScholarLevel) {
		this.dispScholarLevel = dispScholarLevel;
	}

	public String getDispSubCode() {
		return dispSubCode;
	}

	public void setDispSubCode(String dispSubCode) {
		this.dispSubCode = dispSubCode;
	}
	
}

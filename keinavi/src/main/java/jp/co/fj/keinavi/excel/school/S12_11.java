package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.cm.CefrCommon;
import jp.co.fj.keinavi.excel.data.school.S12CefrItem;
import jp.co.fj.keinavi.excel.data.school.S12CefrMain;
import jp.co.fj.keinavi.excel.data.school.S12Item;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * 校内成績分析−校内成績−偏差値分布 S12_11_英語CEFR試験別取得状況
 *      Excelファイル編集
 * 作成日: 2019/09/30
 * @author QQ
 *
 */
public class S12_11 {

    private int noerror = 0;    // 正常終了
    private int errfread = 1;   // ファイルreadエラー
    private int errfwrite = 2;  // ファイルwriteエラー
    private int errfdata = 3;   // データ設定エラー

    private int cefrLevelCnt = 0;       // CEFRレベル数（合計も含む）

    private CM cm = new CM();   // 共通関数用クラス インスタンス
    private CefrCommon cefrCm = null;

    private String masterfile = "S12_11";               // ファイル名

    private static final int MAX_ROW_COUNT = 46;        // シート内最大行数

// 2019/10/03 QQ)Tanioka 枠を固定に修正 UPD START
//    private final int DETAIL_ROW_COUNT = 8;                // 表内最大CEFR列数
    private final int GOUKEI_ROW = 7;                // 表内合計行
// 2019/10/03 QQ)Tanioka 枠を固定に修正 ADD END

    private final int DETAIL_TOP_ROW = 4;                  // 表内明細開始行
    private final int NUMBER_COL_S = 1;                    // 校内 人数
    private final int COMPRATIO_COL_S = 2;                 // 校内 構成比
    private final int NUMBER_COL_P = 3;                    // 県内 人数
    private final int COMPRATIO_COL_P = 4;                 // 県内 構成比
    private final int NUMBER_COL_A = 5;                    // 全国 人数
    private final int COMPRATIO_COL_A = 6;                 // 全国 構成比

    private int intSecuFlg = 0; // セキュリティスタンプフラグ
    private String strGakkomei = ""; // 学校名
    private String strMshmei = ""; // 模試名
    private String strMshDate = ""; // 模試実施基準日

    // Enum(Excel座標 列,行)
    public enum CURSOL {
            SECURITY_STAMP(23, 0),
            SCHOOL_NAME(0, 3),
            ALL_EXAM_TABLE_COPY_START(0, 0),    //templateのコピー開始セル
            ALL_EXAM_TABLE_COPY_END(6, 11),     //templateのコピー終了セル
            EXAM_TABLE_COPY_START(1, 0),
            EXAM_TABLE_COPY_END(6, 11),
            PASTE_1( 0,  6),                         //先頭貼り付け位置
            TERMINAL(0,0);

            private final int col;
            private final int row;

            private CURSOL(int col, int row) {
                    this.col = col;
                    this.row = row;
            }

            public int getCol() {
                    return this.col;
            }

            public int getRow() {
                    return this.row;
            }
    }

    /*
     *      Excel編集メイン
     *              S12Item s12Item: データクラス
     *              String outfile: 出力Excelファイル名（フルパス）
     *              int             intSaveFlg: 1:保存 2:印刷 3:保存／印刷
     *              String  UserID：ユーザーID
     *              戻り値: 0:正常終了、0以外:異常終了
     */
            public int s12_11EditExcel(S12Item s12Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

                    KNLog log = KNLog.getInstance(null,null,null);
                    log.Ep("S12_11","S12_11帳票作成開始","");

                    HSSFWorkbook workbook = null;

                    // マスタExcel読み込み
                    workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
                    if( workbook==null ){
                            return errfread;
                    }

                    try {

                            // Excel認定試験CEFR取得状況共通関数インスタンス生成
                            cefrCm = new CefrCommon(workbook);

                            // シート内最大行数
                            cefrCm.setMaxRowCount(MAX_ROW_COUNT);
                            // テンプレートシート指定
                            cefrCm.setTemplateWorkbook();

                            // セキュリティスタンプフラグ
                            this.intSecuFlg = s12Item.getIntSecuFlg();
                            // 学校名
                            this.strGakkomei = s12Item.getStrGakkomei();
                            // 模試名
                            this.strMshmei = s12Item.getStrMshmei();
                            // 模試実施基準日
                            this.strMshDate = s12Item.getStrMshDate();
                            // 自校受験者がいる試験を対象にするフラグ
                            cefrCm.setIntTargetCheckBoxFlg(s12Item.getIntTargetCheckBoxFlg());


                            // 新ページ作成処理
                            this.newCreateSheet(cm);


                            S12CefrMain s12CefrMain = s12Item.getS12CefrMain();         // S12_11情報
                            ArrayList s12AllList = s12CefrMain.getS12AllList();         // 全試験データ
                            ArrayList s12EachList = s12CefrMain.getS12EachList();       // 各試験データ

                            int examCnt = s12CefrMain.getIntExamCount();                //出力対象試験数
                            int selfCnt = s12CefrMain.getS12EngPtCdList().size();       //自高対象試験数

                            //対象データが存在する場合、出力をおこなう
                            if ((s12Item.getIntTargetCheckBoxFlg() != 1 && examCnt > 0) ||
                                 (s12Item.getIntTargetCheckBoxFlg() == 1 && selfCnt > 0)) {

                                    // テンプレートテーブルに学校名/県名/CEFR基準をセットしておく
                                    this.setTemplateNames(cm, s12CefrMain, s12AllList);

                                    // 全試験 情報出力
                                    this.setAllExamData(cm, s12AllList);

                                    // 各試験 情報出力
                                    this.setEachExamData(cm, s12EachList);

                                    // テンプレートシート削除
                                    if( workbook.getSheetIndex(CefrCommon.templateSheetName) >= 0){
                                            workbook.removeSheetAt(workbook.getSheetIndex(CefrCommon.templateSheetName));
                                    }

                                    //2019/09/25 QQ)Oosaki アクティブシートを選択 ADD START
                                    workbook.getSheetAt(1).setSelected(true);
                                    //2019/09/25 QQ)Oosaki アクティブシートを選択 ADD END

                                    // Excelファイル保存
                                    boolean bolRet = false;
                                    bolRet = cm.bolFileSave(intSaveFlg, outfilelist, cefrCm.getWorkbook(), UserID, 0, masterfile, cefrCm.getMaxSheetIndex());

                                    if( bolRet == false ){
                                            return errfwrite;
                                    }
                            }

                    } catch(Exception e) {
                            log.Err("S12_11","データセットエラー",e.toString());
                            e.printStackTrace();
                            return errfdata;
                    }

                    log.Ep("S12_11","S12_11帳票作成終了","");
                    return noerror;
            }
            /**
             * <DL>
             * <DT>改ページ処理
             * </DL>
             * @param cm Excel共通関数インスタンス
             */
            private void newCreateSheet(CM cm) {

                    cefrCm.initSheetSetting();

                    // ヘッダ右側に帳票作成日時を表示する
                    cm.setHeader(cefrCm.getWorkbook(), cefrCm.getWorkSheet());

                    // セキュリティスタンプセット
                    cefrCm.setCurCol(CURSOL.SECURITY_STAMP.getCol());
                    cefrCm.setCurRow(CURSOL.SECURITY_STAMP.getRow());
                    String secuFlg = cm.setSecurity(cefrCm.getWorkbook(), cefrCm.getWorkSheet(), intSecuFlg, cefrCm.getCurCol(), cefrCm.getCurCol() + 1);
                    cefrCm.setCellValueAddRowCountUp(cm, secuFlg);

                    // 学校名セット
                    cefrCm.setCurCol(CURSOL.SCHOOL_NAME.getCol());
                    cefrCm.setCurRow(CURSOL.SCHOOL_NAME.getRow());
                    cefrCm.setCellValueAddRowCountUp(cm, "学校名　：" + cm.toString(strGakkomei));

                    // 対象模試セット
                    String moshi = cm.setTaisyouMoshi(strMshDate); // 模試月取得
                    cefrCm.setCellValueAddRowCountUp(cm, cm.getTargetExamLabel() + "：" + cm.toString(strMshmei) + moshi);

                    // 表示対象セット
                    String cefrMySchoolOnlyLabel = "";
                    if (cefrCm.checkIntTargetCheckBoxFlg()) {
                            cefrMySchoolOnlyLabel = cm.getCefrMySchoolOnlyLabel2();
                    }
                    cefrCm.setCellValueAddRowCountUp(cm, cm.getCefrTargetLabel() + "：" + cefrMySchoolOnlyLabel);
            }

            /**
             * ExcelのAcitiveSheetのセルに値を設定する
             * @param cm        Excel共通関数インスタンス
             * @param col       出力列Index
             * @param row       出力行Index
             * @param val       出力文字列
             */
            private void setCellValue(CM cm, int col, int row, String val) {

                // カーソル位置指定
                cefrCm.setCurCol(col);
                cefrCm.setCurRow(row);

                cefrCm.setCellValueAddRowCountUp(cm, val);

            }

            /**
             * ExcelのAcitiveSheetのセルに値を設定する
             * @param cm        Excel共通関数インスタンス
             * @param col       出力列Index
             * @param row       出力行Index
             * @param val       出力値
             */
            private void setCellValue(CM cm, int col, int row, int val) {

                // カーソル位置指定
                cefrCm.setCurCol(col);
                cefrCm.setCurRow(row);

                cefrCm.setCellValueAddRowCountUp(cm, val);

            }

            /**
             * ExcelのAcitiveSheetのセルに値を設定する
             * @param cm        Excel共通関数インスタンス
             * @param col       出力列Index
             * @param row       出力行Index
             * @param val       出力値
             */
            private void setCellValue(CM cm, int col, int row, float val) {

                // カーソル位置指定
                cefrCm.setCurCol(col);
                cefrCm.setCurRow(row);

                cefrCm.setCellValueAddRowCountUp(cm, val);

            }

            /**
             * Excelの指定Sheetのセルに値を設定する
             * @param sheet     出力Sheet
             * @param col       出力列Index
             * @param row       出力行Index
             * @param val       出力文字列
             */
            private void setCellValue(HSSFSheet sheet, int col, int row, String val) {
                HSSFRow workRow = sheet.getRow(row);
                HSSFCell workCell = workRow.getCell((short)col);
                workCell.setEncoding(HSSFCell.ENCODING_UTF_16);

                workCell.setCellValue(val);
            }
            /**
             * テンプレートテーブルに学校名/県名/CEFR基準をセットしておく
             * @param cm        Excel共通関数インスタンス
             * @param s12Item   データクラス
             */
            private void setTemplateNames(CM cm, S12CefrMain s12CefrMain, ArrayList<S12CefrItem> s12AllList) {

                // テンプレートシート指定
                HSSFSheet tempSheet = cefrCm.getTemplateWorkbook();

                //テンプレートに直接書いているのでセル位置注意
                setCellValue(tempSheet, NUMBER_COL_S, 2, s12CefrMain.getStrBundleName());         //学校名
                setCellValue(tempSheet, NUMBER_COL_P, 2, s12CefrMain.getStrPrefName());           //県名
                setCellValue(tempSheet, NUMBER_COL_S, 3, "延人数");
                setCellValue(tempSheet, NUMBER_COL_P, 3, "延人数");
                setCellValue(tempSheet, NUMBER_COL_A, 3, "延人数");

                //CEFR基準
                cefrLevelCnt = s12AllList.size();
                for(int i=0; i<cefrLevelCnt; i++) {

                    //テンプレートに直接書いているのでセル位置注意
                    String lvl = s12AllList.get(i).getStrCerfLevelNameAbbr().trim();
                    if ("ZZ".equals(lvl)) {
// 2019/11/06 QQ)Tanioka 枠を固定に修正 UPD START
//                        setCellValue(tempSheet, 0, DETAIL_TOP_ROW + i, "合計" );
                        if(GOUKEI_ROW + DETAIL_TOP_ROW != DETAIL_TOP_ROW + i) {
                            setCellValue(tempSheet, 0, DETAIL_TOP_ROW + i, "" );
                        }
// 2019/11/06 QQ)Tanioka 枠を固定に修正 ADD END
                    }
                    else {
                        setCellValue(tempSheet, 0, DETAIL_TOP_ROW + i, lvl );
                    }
                }
// 2019/10/03 QQ)Tanioka 枠を固定に修正 DEL START
//                //出力対象外行は削除しておく
//                if (cefrLevelCnt < DETAIL_ROW_COUNT) {
//                    for(int row=cefrLevelCnt + 4; row<12; row++) {
//                        HSSFRow workRow = tempSheet.getRow(row);
//                        tempSheet.removeRow(workRow);
//                    }
//                }
// 2019/10/03 QQ)Tanioka 枠を固定に修正 DEL END
            }

            /**
             * テンプレートテーブルのコピー
             * @param cm                Excel共通関数インスタンス
             * @param includeBaseTitle  CEFR基準を含めてコピーするかどうか true:含める
             * @param pasteCol          貼り付け位置Col
             * @param pasteRow          貼り付け位置Row
             */
            private void copyTemplateTable(CM cm, boolean includeBaseTitle, int pasteCol, int pasteRow) {
                // テンプレートカーソル位置設定
                int tempCurColS = CURSOL.EXAM_TABLE_COPY_START.getCol();
                int tempCurRowS = CURSOL.EXAM_TABLE_COPY_START.getRow();
                int tempCurColE = CURSOL.EXAM_TABLE_COPY_END.getCol();
                int tempCurRowE = CURSOL.EXAM_TABLE_COPY_END.getRow();

                //CEFR基準あり
                if (includeBaseTitle == true) {

                    tempCurColS = CURSOL.ALL_EXAM_TABLE_COPY_START.getCol();
                    tempCurRowS = CURSOL.ALL_EXAM_TABLE_COPY_START.getRow();
                    tempCurColE = CURSOL.ALL_EXAM_TABLE_COPY_END.getCol();
                    tempCurRowE = CURSOL.ALL_EXAM_TABLE_COPY_END.getRow();

                }

                // 表テンプレートコピー
                cm.copyCellArea(cefrCm.getWorkSheet(), cefrCm.getTemplateWorkbook(), tempCurColS, tempCurRowS, tempCurColE, tempCurRowE, pasteCol, pasteRow);
            }

            /**
             *テーブルデータ情報セット
             * @param cm                Excel共通関数インスタンス
             * @param dataList          出力対象データリスト
             * @param col               出力テーブル列位置
             * @param row               出力テーブル行位置
             * @param startDataIdx      出力データの開始位置
             */
            private void setTableData(CM cm, ArrayList<S12CefrItem> dataList, int col, int row, int startDataIdx) {

                //値セット
                int cnt = 0;
                int iVal = 0;
                float fVal = 0;

                for(int i=startDataIdx; i<startDataIdx+cefrLevelCnt; i++) {

// 2019/10/03 QQ)Tanioka 枠を固定に修正 ADD START
                	//合計行
                	if( i + 1 == startDataIdx+cefrLevelCnt ) {
                        //校内
                        iVal = dataList.get(i).getIntNumbersS();
                        fVal = dataList.get(i).getFloCompRatioS();
                        setCellValue(cm, col + NUMBER_COL_S,        row + DETAIL_TOP_ROW + GOUKEI_ROW, iVal);
                        setCellValue(cm, col + COMPRATIO_COL_S,     row + DETAIL_TOP_ROW + GOUKEI_ROW, fVal);
                        //県内
                        iVal = dataList.get(i).getIntNumbersP();
                        fVal = dataList.get(i).getFloCompRatioP();
                        setCellValue(cm, col + NUMBER_COL_P,        row + DETAIL_TOP_ROW + GOUKEI_ROW, iVal);
                        setCellValue(cm, col + COMPRATIO_COL_P,     row + DETAIL_TOP_ROW + GOUKEI_ROW, fVal);
                        //全国
                        iVal = dataList.get(i).getIntNumbersA();
                        fVal = dataList.get(i).getFloCompRatioA();
                        setCellValue(cm, col + NUMBER_COL_A,        row + DETAIL_TOP_ROW + GOUKEI_ROW, iVal);
                        setCellValue(cm, col + COMPRATIO_COL_A,     row + DETAIL_TOP_ROW + GOUKEI_ROW, fVal);
                        cnt++;

                    //各行
                    }else {
// 2019/10/03 QQ)Tanioka 枠を固定に修正 ADD END

                        //校内
                        iVal = dataList.get(i).getIntNumbersS();
                        fVal = dataList.get(i).getFloCompRatioS();
                        setCellValue(cm, col + NUMBER_COL_S,        row + DETAIL_TOP_ROW + cnt, iVal);
                        setCellValue(cm, col + COMPRATIO_COL_S,     row + DETAIL_TOP_ROW + cnt, fVal);
                        //県内
                        iVal = dataList.get(i).getIntNumbersP();
                        fVal = dataList.get(i).getFloCompRatioP();
                        setCellValue(cm, col + NUMBER_COL_P,        row + DETAIL_TOP_ROW + cnt, iVal);
                        setCellValue(cm, col + COMPRATIO_COL_P,     row + DETAIL_TOP_ROW + cnt, fVal);
                        //全国
                        iVal = dataList.get(i).getIntNumbersA();
                        fVal = dataList.get(i).getFloCompRatioA();
                        setCellValue(cm, col + NUMBER_COL_A,        row + DETAIL_TOP_ROW + cnt, iVal);
                        setCellValue(cm, col + COMPRATIO_COL_A,     row + DETAIL_TOP_ROW + cnt, fVal);
                        cnt++;
// 2019/10/03 QQ)Tanioka 枠を固定に修正 ADD START
                    }
// 2019/10/03 QQ)Tanioka 枠を固定に修正 ADD END
                }

            }


            /**
             * 各試験情報出力
             * @param cm                Excel共通関数インスタンス
             * @param s12EachList       各試験情報データリスト
             */
            private void setEachExamData(CM cm, ArrayList<S12CefrItem> s12EachList) {

                final int _COL_TABLE_COUNT = 4;                 // 横テーブル数
                final int _ROW_TABLE_COUNT = 3;                 // 縦テーブル数
                final int _DETAIL_COL = 6;                      // 明細列数
                final int _TABLE_ROW_COUNT = 12;                // ひな形テーブル行数

                int tblCnt = _COL_TABLE_COUNT + 1;              //全試験分加算しておく
                int page = 1;
                int pasteCol = 0;
                int pasteRow = CURSOL.PASTE_1.getRow();

                for(int dataIdx=0; dataIdx<s12EachList.size(); dataIdx+=cefrLevelCnt) {

                    //貼り付け位置調整

                    int pos = tblCnt % _COL_TABLE_COUNT;
                    boolean isTopCell = false;
                    if (pos == 1) {                                     // １列につき４明細で先頭ページか？
                        isTopCell = true;
                        //列を先頭から
                        pasteCol = CURSOL.PASTE_1.getCol();

                        int curPage = (int)((tblCnt - 1) / (_COL_TABLE_COUNT * _ROW_TABLE_COUNT)) + 1;
                        if (page != curPage) {
                            page = curPage;
                            // 新ページ作成処理
                            this.newCreateSheet(cm);

                            pasteRow = CURSOL.PASTE_1.getRow();
                        }
                        else {
                            pasteRow += _TABLE_ROW_COUNT + 1;           //+1は余白行
                        }
                    }
                    else {
                        pasteCol += _DETAIL_COL;
                        if (pos == 2) {
                            //基準分加算
                            pasteCol += 1;
                        }
                    }

                    //テンプレート表をコピー
                    this.copyTemplateTable(cm,  isTopCell, pasteCol,  pasteRow);

                    // 試験名
                    String examName = s12EachList.get(dataIdx).getStrShortName();
                    this.setCellValue(cm, isTopCell ? pasteCol + 1 : pasteCol, pasteRow + 1, examName);
                    //値セット
                    this.setTableData(cm, s12EachList, isTopCell ? pasteCol : pasteCol - 1, pasteRow, dataIdx);


                    tblCnt++;
                }


            }

            /**
             * 全試験情報出力
             * @param cm                Excel共通関数インスタンス
             * @param s12AllList        全試験情報データリスト
             */
            private void setAllExamData(CM cm, ArrayList<S12CefrItem> s12AllList) {

                //テンプレート表をコピー
                int col = CURSOL.PASTE_1.getCol();
                int row = CURSOL.PASTE_1.getRow();
                this.copyTemplateTable(cm,  true, col,  row);

                //タイトルセット
                setCellValue(cm, NUMBER_COL_S, row + 3, "実人数");
                setCellValue(cm, NUMBER_COL_P, row + 3, "実人数");
                setCellValue(cm, NUMBER_COL_A, row + 3, "実人数");

                //値セット
                this.setTableData(cm, s12AllList, col, row, 0);
            }


}



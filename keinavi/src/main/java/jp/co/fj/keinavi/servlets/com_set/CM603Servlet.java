package jp.co.fj.keinavi.servlets.com_set;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.PrefBean;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM604Form;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM603Servlet extends AbstractCMServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 共通処理
		super.execute(request, response);

		// HTTPセッション
		HttpSession session = request.getSession();
		// 共通アクションフォーム - session scope
		CM001Form scform = (CM001Form)session.getAttribute(CM001Form.SESSION_KEY);
		// 個別アクションフォーム - session scope
		CM604Form siform = (CM604Form)scform.getActionForm("CM604Form");

		// JSP
		if ("cm603".equals(getForward(request))) {
			// 転送元がcm601ならフォームは初期化する
			if ("cm601".equals(getBackward(request))) {
				siform.setPref(null);
				siform.setBlock(null);
				siform.setSchoolDiv(new String[]{"01", "02"});
				siform.setRank(new String[]{"A", "B", "C", "D", "E" ,"N" ,"なし"});
				siform.setExaminees("all");
				siform.setView("all");
				siform.setSearchMode("name");
				siform.setSearchStr("");
			}
			// DBコネクション
			Connection con = null;
			try {
				con = super.getConnectionPool(request);

				// 都道府県Bean
				PrefBean pref = PrefBean.getInstance(con);
				request.setAttribute("PrefBean",pref);
			} catch (Exception e) {
				throw new ServletException(e);
			} finally {
				super.releaseConnectionPool(request, con);
			}

			super.forward(request, response, JSP_CM603);
		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

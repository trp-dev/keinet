package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−設問別個人成績 科目別成績データリスト
 * 作成日: 2004/08/06
 * @author	A.Iwata
 */
public class C14KmkListBean {
	//科目コード
	private String strKmkCd = "";
	//科目名
	private String strKmkmei = "";
	//配点(科目)
	private String strHaitenKmk = "";
	//受験人数
	private int intNinzu = 0;
	//設問番号
	private String strSetsuNo = "";
	//設問内容
	private String strSetsuMei1 = "";
	//配点(設問)
	private String strSetsuHaiten = "";
	//全国平均点
	private float floHeikinAll = 0;
	//都道府県別平均点
	private float floHeikinKen = 0;
	//校内平均点
	private float floHeikinHome = 0;
	//クラス平均点
	private float floHeikinCls = 0;
	//個人成績データリスト
	private ArrayList c14PersonalDataList = new ArrayList();

	/*----------*/
	/* Get      */
	/*----------*/

	public ArrayList getC14PersonalDataList() {
		return this.c14PersonalDataList;
	}
	public float getFloHeikinAll() {
		return this.floHeikinAll;
	}
	public float getFloHeikinCls() {
		return this.floHeikinCls;
	}
	public float getFloHeikinHome() {
		return this.floHeikinHome;
	}
	public float getFloHeikinKen() {
		return this.floHeikinKen;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public String getStrHaitenKmk() {
		return this.strHaitenKmk;
	}
	public String getStrSetsuHaiten() {
		return this.strSetsuHaiten;
	}
	public String getStrKmkCd() {
		return this.strKmkCd;
	}
	public String getStrKmkmei() {
		return this.strKmkmei;
	}
	public String getStrSetsuMei1() {
		return this.strSetsuMei1;
	}
	public String getStrSetsuNo() {
		return this.strSetsuNo;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setC14PersonalDataList(ArrayList c14PersonalDataList) {
		this.c14PersonalDataList = c14PersonalDataList;
	}
	public void setFloHeikinAll(float floHeikinAll) {
		this.floHeikinAll = floHeikinAll;
	}
	public void setFloHeikinCls(float floHeikinCls) {
		this.floHeikinCls = floHeikinCls;
	}
	public void setFloHeikinHome(float floHeikinHome) {
		this.floHeikinHome = floHeikinHome;
	}
	public void setFloHeikinKen(float floHeikinKen) {
		this.floHeikinKen = floHeikinKen;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setStrHaitenKmk(String strHaitenKmk) {
		this.strHaitenKmk = strHaitenKmk;
	}
	public void setStrSetsuHaiten(String strSetsuHaiten) {
		this.strSetsuHaiten = strSetsuHaiten;
	}
	public void setStrKmkCd(String strKmkCd) {
		this.strKmkCd = strKmkCd;
	}
	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setStrSetsuMei1(String strSetsuMei1) {
		this.strSetsuMei1 = strSetsuMei1;
	}
	public void setStrSetsuNo(String strSetsuNo) {
		this.strSetsuNo = strSetsuNo;
	}

}
/*
 * 作成日: 2004/06/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.individual.ExamSearchBean;
import jp.co.fj.keinavi.beans.individual.I003Bean;
import jp.co.fj.keinavi.beans.individual.IUtilBean;
import jp.co.fj.keinavi.beans.individual.IndividualSearchBean;
import jp.co.fj.keinavi.beans.individual.TakenExamBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.StudentData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I003Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 *
 * 2005.02.28	Yoshimoto KAWAI - Totec
 * 				担当クラスの年度対応
 *
 * @author Administrator
 *
 *   Symbol Date       Person     Note
 *   [1]    2005.03.09 kondo      change 生徒選択コンボに表示するためのクラスと、生徒情報の追加
 */
public class I003Servlet extends IndividualServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		super.execute(request, response);

		//フォームデータの取得
		I003Form i003Form = null;
		try {
			i003Form = (I003Form)getActionForm(request, "jp.co.fj.keinavi.forms.individual.I003Form");
		} catch (Exception e) {
			KNServletException k = new KNServletException("0I003にてフォームデータの収得に失敗しました。");
			k.setErrorCode("00I003010101");
			// 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
			//throw k;
			throw new IOException(e);
			// 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
		}

		HttpSession session = request.getSession();

		// ログイン情報
		LoginSession login = super.getLoginSession(request);
		//プロファイル情報
                final Profile profile = getProfile(request);
		List chargeClassDatas = (List) profile.getItemMap(IProfileCategory.CM).get(IProfileItem.CLASS);
		String bundleCd = profile.getBundleCD();
		//個人共通
		ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);

		// 転送元がJSPなら対象模試情報を初期化
		if ("i003".equals(getBackward(request))) {
			initTargetExam(request);
		}

		if ("i003".equals(getForward(request))) {

			// JSP転送
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(false);

				/** 選択された模試のコンボボックス(生徒の一覧も) */
				I003Bean i003Bean = new I003Bean();
				if(iCommonMap.isMendanMode()){
					i003Bean.setSearchCondition(bundleCd, chargeClassDatas, iCommonMap.getTargetPersonId());
				}else{
					i003Bean.setSearchCondition(bundleCd, chargeClassDatas, (String) profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.STUDENT_SELECTED));
				}
				i003Bean.setConnection("", con);
				i003Bean.execute();
				request.setAttribute("i003Bean", i003Bean);
				//iCommonMap.setPersonComboData(i003Bean.getPersonComboData());

				/** 選択された生徒の一覧 */
				// この処理は生徒選択画面から来たときだけでよい
				if ("i002".equals(getBackward(request))) {
					IndividualSearchBean isb = new IndividualSearchBean();
					isb.setSchoolCd(login.getUserID());
					isb.setClasses(IUtilBean.convert2ClassArray(chargeClassDatas));
					if(iCommonMap.isMendanMode()){
						isb.setPersonIdsString(iCommonMap.getTargetPersonId());
					}else{
						isb.setPersonIdsString((String)profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.STUDENT_SELECTED));
					}
					isb.setYear(ProfileUtil.getChargeClassYear(profile));
					isb.setConnection("", con);
					isb.execute();
					iCommonMap.setSelectedIndividuals(isb.getRecordSet());

					/**  選択中の生徒がその模試を受けたかどうかのパラメターを付加する */
					final TakenExamBean bean = new TakenExamBean();
					bean.setConnection(null, con);
					bean.setStudentList(isb.getRecordSet());
					bean.execute();

					//[1] add start
					//選択クラス情報を取得します。
					//クラス名
					List vec = isb.getRecordSet();
					Set set = new LinkedHashSet();
					if(vec != null && vec.size() > 0){
						Iterator it = vec.iterator();
						while(it.hasNext()){
							StudentData sd = (StudentData) it.next();
							set.add(sd.getStudentClass());
						}
					}
					String[] selectedClasses = (String[])set.toArray(new String[set.size()]);
					//クラス━生徒（ＩＤ・名前）
					Map map = new LinkedHashMap();
					List list = new ArrayList();
					if(vec != null && vec.size() > 0){
						Iterator it = vec.iterator();
						while(it.hasNext()) {
							StudentData sd = (StudentData) it.next();
							List cl = (List)map.get(sd.getStudentClass());
							if(cl == null) {
								cl = new ArrayList();
							}
							cl.add(sd);
							map.put(sd.getStudentClass(), cl);
						}
						Collection co = map.values();
						Iterator itc = co.iterator();
						while(itc.hasNext()) {
							list.add(itc.next());
						}
					}
					iCommonMap.setDispClasses(selectedClasses);
					iCommonMap.setDispStudents(list);

                                        // 2020/04/07 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
					//final Short printStudent = (Short) profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);
					//final Short InterviewSheet = (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_SHEET);
					//final String InterviewForms = (String) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_INTERVIEW_FORMS);
					//final Short Report = (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.IND_REPORT);
					//final Short printStamp = (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.PRINT_STAMP);
					//final Short targetGrade = (Short) profile.getItemMap(IProfileCategory.I_EASY_PRINT).get(IProfileItem.STUDENT_DISP_GRADE);
					if(iCommonMap.isBunsekiMode()){
					    //個人成績分析共通-印刷対象生保存をセット
					    //i003Form.setPrintStudent(printStudent.toString());
					    // 初期値：表示中の生徒一人
					    i003Form.setPrintStudent("2");
					}
                                        //ExamData として設定し直す
                                        iCommonMap.setTargetExam(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode());
					//1.成績分析面談シートをセット
                                        String[] sheets = {"0", "0"};
					//if (InterviewSheet == 5) {
					//    sheets[0] = "1";
					//}
                                        //if (InterviewSheet == 2) {
                                        //    sheets[1] = "1";
                                        //}
                                        // 初期値：成績分析面談シート選択なし
					i003Form.setInterviewSheet(sheets);
					//2.面談用帳票をセット
					//i003Form.setInterviewForms(CollectionUtil.splitComma(InterviewForms));
					// 初期値：面談用帳票選択なし
                                        String[] interviewForm = {"0","0","0"};
					i003Form.setInterviewForms(interviewForm);
					//3.個人成績表をセット
					//i003Form.setReport(Report.toString());
					// 初期値：個人成績表選択なし
					i003Form.setReport("0");
					//5.セキュリティスタンプをセット
					//i003Form.setPrintStamp(printStamp.toString());
					// 初期値：セキュリティスタンプ 機密
					i003Form.setPrintStamp("1");
					//6.表示する学年
					//i003Form.setTargetGrade(targetGrade == null ? "2" : targetGrade.toString());
					// 初期値：現学年のみ
					i003Form.setTargetGrade("1");
					// 2020/04/07 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
				} else {
					iCommonMap.setTargetPersonId(i003Form.getTargetPersonId());
					// 2020/04/07 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD START
					i003Form.setPrintStudent(iCommonMap.getPrintStudent());
					i003Form.setPrintStamp(iCommonMap.getPrintStamp());
					i003Form.setTargetGrade(iCommonMap.getTargetGrade());
					i003Form.setReport(iCommonMap.getReport());
					if(iCommonMap.getInterviewSheet() != null){
					    String[] interviewSheet = {"0","0"};
					    for(int i=0; i < iCommonMap.getInterviewSheet().length; i++){
					        if ("5".equals(iCommonMap.getInterviewSheet()[i]) ) {
                                                    interviewSheet[0] = "1";
                                                }
                                                if ("2".equals(iCommonMap.getInterviewSheet()[i]) ) {
                                                    interviewSheet[1] = "1";
                                                }
					    }
					    i003Form.setInterviewSheet(interviewSheet);
		                        }
					if(iCommonMap.getInterviewForms() != null){
					    String[] interviewForm = {"0","0","0"};
					    for(int i=0; i < iCommonMap.getInterviewForms().length; i++){
					        interviewForm[Integer.parseInt(iCommonMap.getInterviewForms()[i])] = "1";
					    }
					    i003Form.setInterviewForms(interviewForm);
					}
					// 2020/04/07 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD END
				}

				con.commit();
			} catch (final Exception e) {
				rollback(con);
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			//testOutput(i003Bean);
                        // 2020/02/26 QQ)nagai 共通テスト対応 ADD START
                        if(i003Form.getTargetGrade() == null) {
                            i003Form.setTargetGrade("2");
                        }

                        if(i003Form.getPrintStamp() == null) {
                            i003Form.setPrintStamp("1");
                        }
                        // 2020/02/26 QQ)nagai 共通テスト対応 ADD END
			request.setAttribute("i003Form", i003Form);
			super.forward(request, response, JSP_I003);

		} else {
		// 不明ならServlet転送
		    // 対象生徒を保持する
		    iCommonMap.setTargetPersonId(i003Form.getTargetPersonId());

		    if ("sheet".equals(getForward(request))) {
                        // フォームの値をプロファイルに保存
                        //分析モード時のみ印刷対象生保存を保存
                        if (iCommonMap.isBunsekiMode()) {
                            profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, Short.parseShort(i003Form.getPrintStudent()));
                        }

                        //1.成績分析面談シート
                        String[] interviewSheet = {"0","0"};
                        if(i003Form.getInterviewSheet() != null){
                            for(int i=0; i < i003Form.getInterviewSheet().length; i++){
                                if ("5".equals(i003Form.getInterviewSheet()[i]) ) {
                                    interviewSheet[0] = "1";
                                }
                                if ("2".equals(i003Form.getInterviewSheet()[i]) ) {
                                    interviewSheet[1] = "1";
                                }
                            }
                            profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(IProfileItem.IND_INTERVIEW_SHEET, Short.parseShort(interviewSheet[0]));
                        }
		        iCommonMap.setInterviewSheet(interviewSheet);

                        //2.面談用帳票を保存
                        String[] interviewForm = {"0","0","0"};
                        if(i003Form.getInterviewForms() != null){
                                for(int i=0; i < i003Form.getInterviewForms().length; i++){
                                        interviewForm[Integer.parseInt(i003Form.getInterviewForms()[i])] = "1";
                                }
                        }
                        profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(IProfileItem.IND_INTERVIEW_FORMS,CollectionUtil.deSplitComma(interviewForm));
                        i003Form.setInterviewForms(interviewForm);

                        //3.個人成績表を保存
                        if(i003Form.getReport() != null){
                                profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(IProfileItem.IND_REPORT, Short.parseShort(i003Form.getReport()));
                        }else{
                                profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(IProfileItem.IND_REPORT, Short.parseShort("0"));
                        }

                        //5.セキュリティスタンプを保存
                        profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(IProfileItem.PRINT_STAMP, Short.parseShort(i003Form.getPrintStamp()));

                        //6.出力する学年を保存
                        profile.getItemMap(IProfileCategory.I_EASY_PRINT).put(IProfileItem.STUDENT_DISP_GRADE, Short.valueOf(i003Form.getTargetGrade()));
		    } else {
                        //対象模試と現在選択されている生徒を今後使い回す:::::::::::::::: : :  :   :     :          :
                        iCommonMap.setTargetPersonId(i003Form.getTargetPersonId());
                        iCommonMap.setTargetExamYear(i003Form.getTargetExamYear());
                        iCommonMap.setTargetExamCode(i003Form.getTargetExamCode());
                        iCommonMap.setTargetExamName(i003Form.getTargetExamName());
                        // 2020/04/07 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD START
                        iCommonMap.setTargetGrade(i003Form.getTargetGrade());
                        iCommonMap.setPrintStamp(i003Form.getPrintStamp());
                        iCommonMap.setPrintStudent(i003Form.getPrintStudent());
                        iCommonMap.setReport(i003Form.getReport());
                        iCommonMap.setInterviewSheet(i003Form.getInterviewSheet());
                        iCommonMap.setInterviewForms(i003Form.getInterviewForms());
                        // 2020/04/07 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD END

                        Connection con = null;
                        try {
                                con = getConnectionPool(request);
                                con.setAutoCommit(true);

                                /** 模試タイプの取得 */
                                //対象模試が存在するときのみ実行
                                if(iCommonMap.isTargetExamExist()){
                                        ExamSearchBean eb = new ExamSearchBean();
                                        eb.setExamYear(i003Form.getTargetExamYear());
                                        eb.setExamCd(i003Form.getTargetExamCode());
                                        eb.setConnection("", con);
                                        eb.execute();
                                        ExaminationData examData = (ExaminationData) eb.getRecordSet().get(eb.getRecordSet().size() - 1);
                                        iCommonMap.setTargetExamTypeCode(examData.getExamTypeCd());

                                        //ExamData として設定し直す
                                        iCommonMap.setTargetExam(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode());
                                }

                        } catch(final Exception e) {
                                e.printStackTrace();
                                KNServletException k = new KNServletException("0I003にて模試タイプの取得に失敗しました。");
                                k.setErrorCode("00I003010101");
                                // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
                                //throw k;
                                throw new IOException(e);
                                // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
                        } finally {
                                try {if (con != null) releaseConnectionPool(request, con);
                                } catch (Exception e) {
                                        KNServletException k1 = new KNServletException("0I003にてコネクションのクローズに失敗しました。");
                                        k1.setErrorCode("00I003010101");
                                        // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
                                        //throw k1;
                                        throw new IOException(e);
                                        // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
                                }
                        }
		    }
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}
}

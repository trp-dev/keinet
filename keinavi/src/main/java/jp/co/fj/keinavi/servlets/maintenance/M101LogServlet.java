package jp.co.fj.keinavi.servlets.maintenance;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.maintenance.MainteLogCreator;

/**
 *
 * メンテナンスログ出力サーブレット
 * 
 * 2006.11.08	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M101LogServlet extends AbstractMServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {
		
		// CSV出力処理
		if ("m101_log".equals(getForward(request))) {
			// contentTypeを出力
			response.setContentType("application/octet-stream-dummy");
			// ファイル名の送信
			response.setHeader("Content-Disposition", "inline; filename=\""
					+ createFileName(request) + "\"");
			// ログ出力
			final Writer writer = new BufferedWriter(
					new OutputStreamWriter(
							response.getOutputStream(), "Windows-31J"));
			execute(request, new MainteLogCreator(
					getSchoolCd(request), writer));
			writer.flush();
		// 不明ならエラー
		} else {
			throw new ServletException("不正なアクセスです。");
		}
	}

	// 出力ファイル名を作る
	private String createFileName(final HttpServletRequest request) {
		return "mainteLOG_" + getSchoolCd(request) + "_"
				+ new SimpleDateFormat("yyyyMMddHHmm").format(new Date()) + ".csv";
	}
	
}

package jp.co.fj.keinavi.servlets.jnlp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 * ダウンロードを行うサーブレット
 *
 * 2019.5.15	[新規作成]
 *
 *
 * @author kurimoto - QQ
 * @version 1.0
 *
 */
public class DownloadServlet extends DefaultHttpServlet {

    /**
    *
    */
    private static final long serialVersionUID = 1L;

    /**
     * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
     * 			javax.servlet.http.HttpServletRequest,
     * 			javax.servlet.http.HttpServletResponse)
     */
    protected void execute(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        // セッション
        final HttpSession session = request.getSession(false);

        try {
            // セッションキーを決める
            String fpath = (String) session.getAttribute("downloadfile");

            // ファイル
            File file = new File(fpath);
            // ディレクトリ
            File dir = new File(file.getParent());

            // レスポンスヘッダを設定する。
            if (file.getName().toLowerCase().endsWith("zip")) {
                response.setContentType("application/octet-stream");
            } else {
                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
            // 2020/01/10 QQ)Ooseto 共通テスト対応 UPD START
            //response.setHeader("content-disposition", String.format("attachment; filename=\"%s\"", URLEncoder.encode(file.getName(), "UTF-8")));
            response.setHeader("content-disposition", String.format("attachment; filename=\"%s\"", URLEncoder.encode(file.getName(), "UTF-8")).replace("+", "%20"));
            // 2020/01/10 QQ)Ooseto 共通テスト対応 UPD END
            byte buffer[] = new byte[4096];
            // ファイル内容の出力
            try (OutputStream out = response.getOutputStream(); FileInputStream fin = new FileInputStream(file);) {
                int size;
                while ((size = fin.read(buffer)) != -1) {
                    out.write(buffer, 0, size);
                }

            }

            // ファイル削除
            file.delete();
            // ディレクトリは空なら削除
            if (dir.list().length == 0) {
                dir.delete();
            }

        } finally {
            session.removeAttribute("downloadfile");
        }
    }
}

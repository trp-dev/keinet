/**
 * 校内成績分析−クラス比較　成績概況（他校比較）
 * 	Excelファイル編集
 * 作成日: 2004/08/11
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.io.*;
import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.excel.cm.*;
import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.*;

public class S41_02 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private String		strArea			= "A1:AO52";	//印刷範囲
	
	private boolean bolBookCngFlg = true;			//改ファイルフラグ
	private boolean bolSheetCngFlg = true;			//改シートフラグ
	private int intMaxSheetSr	= 40;					//MAXシート数の値を入れる
	
	final private String masterfile0 = "S41_02";
	final private String masterfile1 = "NS41_02";
	private  String masterfile = "";

/*
 * 	Excel編集メイン
 * 		S41Item s41Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s41_02EditExcel(S41Item s41Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
 		
		FileInputStream	fin			= null;
		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s41Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		int		intMaxSheetIndex	= 0;		//ファイルのシートの最大数
		int		sheetIndex			= 0;		//シート番号
		int		row					= 0;		//行
		int		col					= 0;		//列
		int		gakkoCnt			= 0;		//他校カウンタ(自校＋他校)
		int		kmkCnt				= 0;		//科目カウンタ
		String		kmk					= "";		//科目チェック用
		int		maxGakko			= 0;		//MAX他校数(自校＋他校)
		int		sheetListIndex		= 0;		//リスト格納用シートカウンター
		int		fileIndex			= 1;		//ファイルカウンター
		int		houjiNum			= 5;		//1シートに表示できる他校数
		int		sheetRowCnt			= 0;		//他校用改シート格納用カウンタ
		boolean	kmkFlg				= true;	//型→科目変更フラグ
		ArrayList	workbookList	= new ArrayList();
		ArrayList	workSheetList	= new ArrayList();
//add 2004/10/27 T.Sakai データ0件対応
		int		dispGakkoFlgCnt		= 0;		// グラフ表示フラグカウンタ
//add end

		// 基本ファイルを読込む
		S41ListBean s41ListBean = new S41ListBean();
		
		try {
			
			// データセット
			ArrayList s41List = s41Item.getS41List();
			Iterator itr = s41List.iterator();
			
			if( itr.hasNext() == false ){
				return errfdata;
			}
			
			while( itr.hasNext() ) {
				s41ListBean = (S41ListBean)itr.next();
				if (s41ListBean.getIntDispKmkFlg()==1) {
					//科目が変わる時のチェック
					if (kmkFlg==true) {
						//型から科目に変わる時のチェック
						if (Integer.parseInt(s41ListBean.getStrKmkCd()) < 7000) {
							kmkFlg = false;
							col = 0;
							kmkCnt = 0;
							bolSheetCngFlg = true;
							if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
								bolBookCngFlg = true;
							}
						}
					}
					//10型・科目ごとにシート名リスト初期化
					if (kmkCnt==0) {
						workSheetList	= new ArrayList();
						sheetListIndex = 0;
					}
					
					// workbookListにWorkBookが２つ以上格納されている時は出力処理を実行
					int listSize = workbookList.size();
					if(listSize>1){
						if(kmkCnt == 0){
							workbook = (HSSFWorkbook)workbookList.get(0);
		
							// Excelファイル保存
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, intMaxSheetSr);
							fileIndex++;
							if( bolRet == false ){
								return errfwrite;
							}
	
							// ファイル出力したデータは削除
							workbookList.remove(0);
	
							// WorkBook・変数を書き込んでる途中のものに戻す
							workbook = (HSSFWorkbook)workbookList.get(0);
						}
					}
					
					// 基本ファイルを読込む
					S41GakkoListBean s41GakkoListBean = new S41GakkoListBean();
					
					// 他校データセット
					ArrayList s41GakkoList = s41ListBean.getS41GakkoList();
					Iterator itrGakko = s41GakkoList.iterator();
					
					maxGakko = 0;
					//他校データ件数取得
					while ( itrGakko.hasNext() ){
						s41GakkoListBean = (S41GakkoListBean)itrGakko.next();
						if (s41GakkoListBean.getIntDispGakkoFlg()==1) {
							maxGakko++;
						}
					}
					// 他校表示に必要なシート数の計算
					sheetRowCnt = (maxGakko-1)/(houjiNum-1);
					if((maxGakko-1)%(houjiNum-1)!=0){
						sheetRowCnt++;
					}
					if (sheetRowCnt==0) {
						sheetRowCnt++;
					}
					bolSheetCngFlg=true;
					
					int ninzu = 0;
					float heikin = 0;
					float hensa = 0;
					
					itrGakko = s41GakkoList.iterator();
					
					while ( itrGakko.hasNext() ){
						s41GakkoListBean = (S41GakkoListBean)itrGakko.next();
						
						if (s41GakkoListBean.getIntDispGakkoFlg()==1) {
							if( bolBookCngFlg == true ){
								//マスタExcel読み込み
								workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
								if( workbook==null ){
									return errfread;
								}
								bolBookCngFlg = false;
								intMaxSheetIndex = 0;
								workbookList.add(workbook);
							}
							//他校数が5以上または10型･科目ごとに改シート
							if ( bolSheetCngFlg == true ) {
								// データセットするシートの選択
								if (kmkCnt==0) {
									//他校用改シート
									workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex+1));
									workSheetList.add(sheetListIndex, workSheet);
									intMaxSheetIndex++;
									sheetListIndex++;
								} else {
									//次列用シートの呼び出し
									workSheet = (HSSFSheet)workSheetList.get(sheetListIndex-sheetRowCnt);
									sheetRowCnt--;
								}
								if (kmkCnt==0) {
									// ヘッダ右側に帳票作成日時を表示する
									cm.setHeader(workbook, workSheet);
								
									// セキュリティスタンプセット
									String secuFlg = cm.setSecurity( workbook, workSheet, s41Item.getIntSecuFlg() ,38 ,40 );
									workCell = cm.setCell( workSheet, workRow, workCell, 0, 38 );
									workCell.setCellValue(secuFlg);
								
									// 学校名セット
									workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
									workCell.setCellValue( "学校名　：" + cm.toString(s41Item.getStrGakkomei()) );
			
									// 模試月取得
									String moshi =cm.setTaisyouMoshi( s41Item.getStrMshDate() );
									// 対象模試セット
									workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
									workCell.setCellValue( "対象模試：" + cm.toString(s41Item.getStrMshmei()) + moshi);
									col = 0;
								}
								row = 44;
								if ( kmkCnt==0 || !cm.toString(kmk).equals(cm.toString(s41ListBean.getStrKmkmei())) ) {
									// 型・科目名セット
									workCell = cm.setCell( workSheet, workRow, workCell, row++, col+1 );
									workCell.setCellValue( s41ListBean.getStrKmkmei() );
									// 配点セット
									if ( !cm.toString(s41ListBean.getStrHaitenKmk()).equals("") ) {
										workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
										workCell.setCellValue( "（" + s41ListBean.getStrHaitenKmk() + "）" );
									}
								}
								bolSheetCngFlg = false;
							}
							if ( gakkoCnt==0 ) {
								row = 47;
								if (col==0) {
									// 学校名セット
									workCell = cm.setCell( workSheet, workRow, workCell, row, col );
									workCell.setCellValue( s41Item.getStrGakkomei() );
								}
								// 人数セット
								ninzu = s41GakkoListBean.getIntNinzu();
								workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
								if ( s41GakkoListBean.getIntNinzu() != -999 ) {
									workCell.setCellValue( s41GakkoListBean.getIntNinzu() );
								}
								// 平均点セット
								heikin = s41GakkoListBean.getFloHeikin();
								workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
								if ( s41GakkoListBean.getFloHeikin() != -999.0 ) {
									workCell.setCellValue( s41GakkoListBean.getFloHeikin() );
								}
								// 平均偏差値セット
								hensa = s41GakkoListBean.getFloHensa();
								workCell = cm.setCell( workSheet, workRow, workCell, row++, col+4 );
								if ( s41GakkoListBean.getFloHensa() != -999.0 ) {
									workCell.setCellValue( s41GakkoListBean.getFloHensa() );
								}
							} else {
								if ( gakkoCnt%houjiNum==0 ) {
									row = 47;
									if (col==0) {
										// 学校名セット
										workCell = cm.setCell( workSheet, workRow, workCell, row, col );
										workCell.setCellValue( s41Item.getStrGakkomei() );
									}
									// 人数セット
									workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
									if ( ninzu != -999 ) {
										workCell.setCellValue( ninzu );
									}
									// 平均点セット
									workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
									if ( heikin != -999.0 ) {
										workCell.setCellValue( heikin );
									}
									// 平均偏差値セット
									workCell = cm.setCell( workSheet, workRow, workCell, row++, col+4 );
									if ( hensa != -999.0 ) {
										workCell.setCellValue( hensa );
									}
									gakkoCnt++;
								}
								if (col==0) {
									// 学年+他校名セット
									workCell = cm.setCell( workSheet, workRow, workCell, row, col );
									workCell.setCellValue( s41GakkoListBean.getStrGakkomei() );
								}
								// 人数セット
								workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
								if ( s41GakkoListBean.getIntNinzu() != -999 ) {
									workCell.setCellValue( s41GakkoListBean.getIntNinzu() );
								}
								// 平均点セット
								workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
								if ( s41GakkoListBean.getFloHeikin() != -999.0 ) {
									workCell.setCellValue( s41GakkoListBean.getFloHeikin() );
								}
								// *セット
								if ( s41GakkoListBean.getFloHensa() > hensa ) {
									workCell = cm.setCell( workSheet, workRow, workCell, row, col+3 );
									workCell.setCellValue("*");
								}
								// 平均偏差値セット
								workCell = cm.setCell( workSheet, workRow, workCell, row++, col+4 );
								if ( s41GakkoListBean.getFloHensa() != -999.0 ) {
									workCell.setCellValue( s41GakkoListBean.getFloHensa() );
								}
							}
							gakkoCnt++;
							
							if(gakkoCnt%houjiNum==0){
								if(itrGakko.hasNext()){
									if(kmkCnt==0){
										if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
											bolBookCngFlg = true;
										}
									}
									bolSheetCngFlg = true;
								}
							}
							if(itrGakko.hasNext()==false){
								if(kmkCnt==0){
									bolSheetCngFlg = true;
								}
							}
//add 2004/10/27 T.Sakai データ0件対応
							dispGakkoFlgCnt++;
//add end
						}
					}
					kmk = s41ListBean.getStrKmkmei();
					col = col + 4;
					gakkoCnt = 0;
					kmkCnt++;
					if ( kmkCnt==10 ) {
						bolSheetCngFlg = true;
						kmkCnt = 0;
						if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
							bolBookCngFlg = true;
						}
					}
	
					// workbookListにWorkBookが２つ以上格納されている時は出力処理を実行
					listSize = workbookList.size();
					if(listSize>1){
						if(kmkCnt == 0){
							workbook = (HSSFWorkbook)workbookList.get(0);
		
							// Excelファイル保存
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, intMaxSheetSr);
							fileIndex++;
							if( bolRet == false ){
								return errfwrite;
							}
	
							// ファイル出力したデータは削除
							workbookList.remove(0);
	
							// WorkBook・変数を書き込んでる途中のものに戻す
							workbook = (HSSFWorkbook)workbookList.get(0);
						}
					}
				}
			}
//add 2004/10/27 T.Sakai データ0件対応
			if ( dispGakkoFlgCnt==0 ) {
				//マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				intMaxSheetIndex = 0;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex+1));
				intMaxSheetIndex++;
				
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
						
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s41Item.getIntSecuFlg() ,38 ,40 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 38 );
				workCell.setCellValue(secuFlg);
								
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s41Item.getStrGakkomei()) );
			
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s41Item.getStrMshDate() );
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
				workCell.setCellValue( "対象模試：" + cm.toString(s41Item.getStrMshmei()) + moshi);
			}
//add end
			
			// Excelファイル保存
			boolean bolRet = false;
			if(fileIndex != 1){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, intMaxSheetIndex);
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("S41_02","データセットエラー",e.toString());
			return errfdata;
		}
		
		return noerror;
	}

}
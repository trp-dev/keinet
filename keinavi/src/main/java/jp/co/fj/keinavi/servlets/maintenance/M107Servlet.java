package jp.co.fj.keinavi.servlets.maintenance;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.beans.admin.IndivChangeInfoTableAccess;
import jp.co.fj.keinavi.beans.admin.LastDateBean;
import jp.co.fj.keinavi.beans.maintenance.SelectedStudentBean;
import jp.co.fj.keinavi.beans.maintenance.StudentIntegrationBean;
import jp.co.fj.keinavi.beans.maintenance.StudentUpdateBean;
import jp.co.fj.keinavi.beans.recount.RecountLockBean;
import jp.co.fj.keinavi.data.maintenance.KNStudentExtData;
import jp.co.fj.keinavi.forms.maintenance.M107Form;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * 統合画面サーブレット
 * 
 * 2006.10.26	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M107Servlet extends M105Servlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// アクションフォーム
		final M107Form form = (M107Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.maintenance.M107Form");

		// JSPへの遷移処理
		if ("m107".equals(getForward(request))) {
			// 一覧画面からの遷移ならフォーム初期化
			if ("m101".equals(getBackward(request))) {
				syncSelectedStudent(request); // 選択済み生徒を同期
				form.init(getMainStudentData(request));
			}
			request.setAttribute("CurrentYear", KNUtil.getCurrentYear());
			request.setAttribute("form", form);
			forward2Jsp(request, response);
			
		// 不明なら転送
		} else {
			
			if ("1".equals(form.getActionMode())) {
				// 学校コード
				final String schoolCd = getSchoolCd(request);
				
				Connection con = null;
				try {
					con = getConnectionPool(request);
					con.setAutoCommit(false);
					
					// 再集計ロック失敗
					if (!RecountLockBean.lock(con, schoolCd)) {
						con.rollback();
						request.setAttribute("ErrorMessage",
								RecountLockBean.getErrorMessage());
					} else {
						// データ更新日
						LastDateBean.setLastDate1(con, schoolCd);
						
						// 編集画面からの遷移なら主情報更新
						if ("1".equals(form.getEditFlag())) {
							update(con, schoolCd,
									getSelectedStudentBean(request), form);
							/* 更新：変更成績情報テーブル */
							IndivChangeInfoTableAccess.updateIndividual(con, form.getTargetIndividualId());
						}
						
						// 統合処理
						integrate(con, getMainStudentData(request),
								getSubStudentData(request), schoolCd);
						
						// ログ
						if ("1".equals(form.getEditFlag())) {
							actionLog(request, "802");
						}
						actionLog(request, "804");
						
						con.commit();
					}
				// エラーメッセージを保持
				} catch (final KNBeanException e) {
					rollback(con);
					request.setAttribute("ErrorMessage", e.getMessage());
				// 想定外のエラー
				} catch (final Exception e) {
					rollback(con);
					throw createServletException(e);
				} finally {
					releaseConnectionPool(request, con);
				}
				
				// 成功
				if (getErrorMessage(request) == null) {
					// 従生徒の選択はなくなる
					getSelectedStudentBean(request).removeSubStudent();
					// 生徒情報セッション初期化
					initStudentSession(request);
				// 失敗
				} else {
					request.setAttribute("CurrentYear", KNUtil.getCurrentYear());
					request.setAttribute("form", form);
					forward2Jsp(request, response, "m107");
					return;
				}
			}
			dispatch(request, response);
		}
	}

	// 生徒情報の更新処理
	private void update(final Connection con,
			final String schoolCd,
			final SelectedStudentBean bean,
			final M107Form form) throws Exception {
		
		final StudentUpdateBean updater = new StudentUpdateBean(
				schoolCd);
		updater.setConnection(null, con);
		updater.setStudent(form2OverwrittenData(bean, form));
		updater.execute();
	}
	
	// 統合処理
	private void integrate(final Connection con,
			final KNStudentExtData mainStudentData,
			final KNStudentExtData subStudentData,
			final String schoolCd) throws Exception {

		final StudentIntegrationBean bean = new StudentIntegrationBean(
				schoolCd);
		bean.setConnection(null, con);
		bean.setMainStudentData(mainStudentData);
		bean.setSubStudentData(subStudentData);
		bean.execute();
	}
	
}

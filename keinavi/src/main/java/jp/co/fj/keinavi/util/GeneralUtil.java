/*
 * 作成日: 2004/10/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class GeneralUtil {

	/**
	 * アプレット用にデータがnullならば-999に変換する
	 * @param input
	 * @return
	 */
	public static String to999(String input){
		if(input == null || input.trim().equals("")){
			return "-999";
		}else{
			return input;
		}
	}
	
	//数値、下限値、上限値
	public static String toRangeLimit(String input, String min, String max) {
		if(input == null || input.trim().equals("")){
			return "-999";
		}else if(Float.parseFloat(input) < Float.parseFloat(min)){
			return min;
		}else if(Float.parseFloat(max) < Float.parseFloat(input)){
			return max;
		}else{
			return input;
		}
	}
	
	/**
	 * 一般的にデータがnullならば空白に変換する
	 * @param input
	 * @return
	 */
	public static String toBlank(String input){
		if(input == null){
			return "";
		}else{
			return input;
		}
	}
	
	/**
	 * StringをFloatのフォーマットで返します。
	 * @return
	 */
	public static String nullToEmptyF(String str) {
		if(str == null || str.equals("")) {
			return "";
		} else {
			return String.valueOf(new Float(str).floatValue());
		}
	}
	
	/**
	 * 文字列を最大サイズで切り捨てて返す
	 * 最大サイズは半角カナに変換後のサイズ
	 * 
	 * @param string 変換元文字列
	 * @param index 最大サイズ
	 * @return 変換後文字列
	 */
	public static String truncateString(final String string, final int index) {
		if (string == null) return null;
		
		final String str = JpnStringConv.hkana2Han(string);
		
		if (str.length() < index) {
			return JpnStringConv.kkanaHan2Hkana(str);
		} else {
			return JpnStringConv.kkanaHan2Hkana(str.substring(0, index));
		}
	}
	
}

package jp.co.fj.keinavi.beans.com_set;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 科目カウントBean
 * 
 * 2006.10.19	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class SubjectCountBean extends DefaultBean {

	// 学校コード
	// Kei-Naviでは使わない
	private final String schoolCd;
	// 模試年度
	private String examYear;
	// 模試コード
	private String examCd;
	
	// 型カウント
	private int typeCount = 0;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 学校コード
	 */
	public SubjectCountBean(final String pSchoolCd) {
		schoolCd = pSchoolCd;
		if (pSchoolCd != null) {
			schoolCd.getClass(); // dummy...
		}
	}
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		// 対象模試がなければここまで
		if (StringUtils.isEmpty(examYear)
				|| StringUtils.isEmpty(examCd)) {
			return;
		}
		
		typeCount = count(createTypeQuery());
	}

	// 型カウント取得クエリを作る
	private String createTypeQuery() throws SQLException {
		return QueryLoader.getInstance().getQuery("cm11");
	}
	
	// カウント処理
	private int count(final String query) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, examYear);
			ps.setString(2, examCd);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt(1);
			} else {
				return 0;
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * @param pExamCd 設定する examCd。
	 */
	public void setExamCd(final String pExamCd) {
		examCd = pExamCd;
	}

	/**
	 * @param pExamYear 設定する examYear。
	 */
	public void setExamYear(final String pExamYear) {
		examYear = pExamYear;
	}

	/**
	 * @return typeCount を戻します。
	 */
	public int getTypeCount() {
		return typeCount;
	}
	
}

/**
 * 高校成績分析−高校間比較（過年度：前年度）
 * 	Excelファイル編集
 * 作成日: 2004/08/05
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.business;

import java.io.*;
import java.util.*;

import jp.co.fj.keinavi.excel.data.business.*;
import jp.co.fj.keinavi.excel.cm.*;
import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.*;

public class B41_04 {

	private int noerror	= 0;		// 正常終了
	private int errfread	= 1;		// ファイルreadエラー
	private int errfwrite	= 2;		// ファイルwriteエラー
	private int errfdata	= 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	final private String	masterfile0	= "B41_04";		// ファイル名
	final private String	masterfile1	= "NB41_04";	// ファイル名
	private String	masterfile	= "";					// ファイル名
	final private String	strArea		= "A1:V63";		// 印刷範囲	
	final private String	mastersheet = "tmp";		// テンプレートシート名
	final private int[]	tabColNow	= {1,8,15};		// 表の基準点(今年度)
	final private int[]	tabColPast	= {5,12,19};	// 表の基準点(今年度)


	/*
	 * 	Excel編集メイン
	 * 		B41Item b41Item: データクラス
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
 	public int b41_04EditExcel(B41Item b41Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (b41Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			// マスタExcel読み込み
			FileInputStream	fin			= null;
			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;
		
			// 基本ファイルを読込む
			B41ListBean b41ListBean = new B41ListBean();
			
			// データセット
			ArrayList	b41List			= b41Item.getB41List();
			ArrayList	workbookList	= new ArrayList();
			ArrayList	workSheetList	= new ArrayList();
			Iterator	itr				= b41List.iterator();
			int		row				= 0;	// 行
			int		allRow			= 1;	// 行
			int		kmkCnt			= 0;	// 科目カウンター
			int		maxSheetIndex	= 0;	// シートカウンター
			int		intMaxSheetSr	= 50;	// MAXシート数
			int		hyoujiNum		= 50;	// 最大表示件数
			int		allSheetIndex	= 0;	// シートカウンター
			int		fileIndex		= 1;	// ファイルカウンター
			int		dataSize		= 0;	// リストデータ件数
			int		shelSheetNum	= 0;	// シート番号保持
			int		sheetNumRow		= 0;	// シート枚数（Row方向）
			boolean	kataFlg			= true;// false:改行済(型・科目名変更済)　
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
			

			// b41Listのセット
			while( itr.hasNext() ) {

				b41ListBean = (B41ListBean)itr.next();

				// 基本ファイルを読み込む
				B41JuniListBean	b41JuniListBean	= new B41JuniListBean();

				//データの保持
				ArrayList	b41JuniList	= b41ListBean.getB41JuniList();
				Iterator	itrB41Juni	= b41JuniList.iterator();

				//型から科目に変わる時のチェック
				if ( Integer.parseInt(b41ListBean.getStrKmkCd()) < 7000 ) {

					//型・科目切替えの初回のみ処理する
					if (kataFlg){
						if(kmkCnt!=0){
							kataFlg	= false;
							kmkCnt=0;
							bolSheetCngFlg	= true;
						}
						if(kmkCnt==0){
							kataFlg	= false;
						}
					}
					allRow=1;
				}

				// 高校データセット
				ArrayList	bestListNow		= new ArrayList();
				ArrayList	bestListPast	= new ArrayList();
				dataSize = b41JuniList.size(); //データ件数
				shelSheetNum = 0;	// シート番号保持

				// 下方向のシート数の計算
				sheetNumRow = dataSize/hyoujiNum;
				if(dataSize%hyoujiNum!=0){
					sheetNumRow++;
				}
				if(sheetNumRow>1){
					bolSheetCngFlg=true;
				}
				// 変数の初期化
				row	= 0;
				while(itrB41Juni.hasNext()){
					b41JuniListBean	= (B41JuniListBean) itrB41Juni.next();

					//マスタExcel読み込み
					if(bolBookCngFlg){
						if((maxSheetIndex==intMaxSheetSr)||(maxSheetIndex==0)){
							workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
							if( workbook==null ){
								return errfread;
							}
							bolBookCngFlg = false;
							maxSheetIndex=0;
							workbookList.add(workbook);
						}
					}

					if ( bolSheetCngFlg ) {

						// シートの初めての型・科目なら新規シート・２科目め以降なら呼び出し
						if(kmkCnt==0){

							// シートテンプレートのコピー
							workSheet = workbook.cloneSheet(0);
							shelSheetNum = allSheetIndex;
							maxSheetIndex++;
							allSheetIndex++;

							// ヘッダ右側に帳票作成日時を表示する
							cm.setHeader(workbook, workSheet);

							// セキュリティスタンプセット
							String secuFlg = cm.setSecurity( workbook, workSheet, b41Item.getIntSecuFlg() ,19 ,21 );
							workCell = cm.setCell( workSheet, workRow, workCell, 0, 19 );
							workCell.setCellValue(secuFlg);

							// 対象模試セット
							String moshi =cm.setTaisyouMoshi( b41Item.getStrMshDate() );	// 模試月取得
							workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
							workCell.setCellValue( "対象模試　　：" + cm.toString(b41Item.getStrMshmei()) + moshi);

							// 表示対象セット
							workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
							workCell.setCellValue( "表示対象　　：" + b41Item.getIntGakkouNum() + "校");

						}else{
							// シートの呼び出し
							workSheet = (HSSFSheet)workSheetList.get(allSheetIndex-sheetNumRow);
							shelSheetNum = allSheetIndex-sheetNumRow;
							sheetNumRow--;
						}
						//　改シートフラグをFalseにする
						bolSheetCngFlg	=false;
						row=0;
					}

					if(row==0){
						// 型名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 4, tabColNow[kmkCnt] );
						workCell.setCellValue( b41ListBean.getStrKmkmei() );

						// 配点セット
						if( !cm.toString(b41ListBean.getStrHaiten()).equals("")){
							workCell = cm.setCell( workSheet, workRow, workCell, 5, tabColNow[kmkCnt] );
							workCell.setCellValue( " (" + cm.toString(b41ListBean.getStrHaiten()) + ")" );
						}

						// 今年度セット
						workCell = cm.setCell( workSheet, workRow, workCell, 6, tabColNow[kmkCnt]+1 );
						workCell.setCellValue( cm.toString(b41JuniListBean.getStrNendoNow()) + "年度" );

						// 前年度セット
						workCell = cm.setCell( workSheet, workRow, workCell, 6, tabColPast[kmkCnt] );
						workCell.setCellValue( cm.toString(b41JuniListBean.getStrNendoPast()) + "年度" );
					}

					// 順位セット
					if(kmkCnt == 0){
						workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabColNow[kmkCnt]-1 );
						workCell.setCellValue( Integer.toString(allRow));
					}

					// 高校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabColNow[kmkCnt] );
					workCell.setCellValue( b41JuniListBean.getStrGakkomei());

					// 今年度受験人数セット
					if ( b41JuniListBean.getIntNinzuNow() != -999 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabColNow[kmkCnt]+1);
						workCell.setCellValue( b41JuniListBean.getIntNinzuNow() );
					}

					// 今年度平均点セット
					if ( b41JuniListBean.getFloHeikinNow() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabColNow[kmkCnt]+2);
						workCell.setCellValue( b41JuniListBean.getFloHeikinNow() );
					}

					// 今年度平均偏差値セット
					if ( b41JuniListBean.getFloStdHensaNow() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabColNow[kmkCnt]+3);
						workCell.setCellValue( b41JuniListBean.getFloStdHensaNow() );
					}

					// 前年度受験人数セット
//					if ( b41JuniListBean.getIntNinzuNow() != -999 ) {
					if ( b41JuniListBean.getIntNinzuPast() != -999 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabColPast[kmkCnt]);
						workCell.setCellValue( b41JuniListBean.getIntNinzuPast() );
					}

					// 前年度平均点セット
//					if ( b41JuniListBean.getFloHeikinNow() != -999.0 ) {
					if ( b41JuniListBean.getFloHeikinPast() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabColPast[kmkCnt]+1);
						workCell.setCellValue( b41JuniListBean.getFloHeikinPast() );
					}

					// 前年度平均偏差値セット
//					if ( b41JuniListBean.getFloStdHensaNow() != -999.0 ) {
					if ( b41JuniListBean.getFloStdHensaPast() != -999.0 ) {
						workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabColPast[kmkCnt]+2);
						workCell.setCellValue( b41JuniListBean.getFloStdHensaPast() );
					}

					row++;
					allRow++;

					// リストにWorkSheetを格納
					if(row%hyoujiNum==0){
						if(itrB41Juni.hasNext()){
							if(kmkCnt==0){
								workSheetList.add(workSheet);
							}
							bolSheetCngFlg = true;
							if(workbook.getNumberOfSheets()==intMaxSheetSr){
								bolBookCngFlg = true;
							}
						}
					}
					if(itrB41Juni.hasNext()==false){
						if(kmkCnt==0){
							workSheetList.add(workSheet);
						}
					}
				}

				kmkCnt++;
				if(kmkCnt >= 3){
					kmkCnt=0;
					allRow=1;
					bolSheetCngFlg =true;
					if(workbook.getNumberOfSheets()==intMaxSheetSr){
						bolBookCngFlg = true;
					}
				}

				// ListにWorkBookが２つ以上格納されている時は出力処理を実行
				int listSize = workbookList.size();
				if(listSize>1){
					if(kmkCnt == 0){
						workbook = (HSSFWorkbook)workbookList.get(0);
	
						// Excelファイル保存
						boolean bolRet = false;
						bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, workbook.getNumberOfSheets());
						fileIndex++;
						if( bolRet == false ){
							return errfwrite;
						}

						// ファイル出力したデータは削除
						workbookList.remove(0);
						for(int a =0;a<intMaxSheetSr;a++){
							workSheetList.remove(0);
						}

						// WorkBook・変数を書き込んでる途中のものに戻す
						allSheetIndex = maxSheetIndex;
						workbook = (HSSFWorkbook)workbookList.get(0);
					}
				}
				
			}
		
			// Excelファイル保存
			boolean bolRet = false;
			if(fileIndex != 1){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, workbook.getNumberOfSheets());
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, workbook.getNumberOfSheets());
			}

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("B41_04","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}
}
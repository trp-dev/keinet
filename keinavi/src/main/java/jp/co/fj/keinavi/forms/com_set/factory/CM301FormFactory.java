/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.com_set.factory;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM301Form;

import com.fjh.forms.ActionForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CM301FormFactory extends AbstractCMFormFactory {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		CM301Form form = new CM301Form();
		// アイテムマップ
		Map item = getItemMap(request);
		// 対象年度
		form.setCompYear((String[])item.get("1101"));
		return form;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// セッション
		HttpSession session = request.getSession();
		// アイテムマップ
		Map item = getItemMap(request);
		// 共通アクションフォーム
		CM001Form f001 = (CM001Form)session.getAttribute(CM001Form.SESSION_KEY);
		// 個別アクションフォーム
		CM301Form f301 = (CM301Form)f001.getActionForm("CM301Form");
		// 比較対象年度
		item.put("1101", f301.getCompYear());
	}

}

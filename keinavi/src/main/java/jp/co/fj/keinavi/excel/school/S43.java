/**
 * 校内成績分析−他校比較　設問別成績
 * 出力する帳票の判断
 * 作成日: 2004/08/10
 * @author	T.Sakai
 */
 
package jp.co.fj.keinavi.excel.school;

import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.util.log.*;

public class S43 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean s43( S43Item s43Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		try{
			int ret = 0;
			//S43Itemから各帳票を出力
			if (( s43Item.getIntHyouFlg()==0 ) && ( s43Item.getIntGraphFlg()==0 )){
				throw new Exception("S43 ERROR : フラグ異常 ");
			}
			if ( s43Item.getIntHyouFlg()==1 ) {
				log.Ep("S43_01","S43_01帳票作成開始","");
				S43_01 exceledit = new S43_01();
				ret = exceledit.s43_01EditExcel( s43Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S43_01","帳票作成エラー","");
					return false;
				}
				log.Ep("S43_01","S43_01帳票作成終了","");
				sheetLog.add("S43_01");
			}
			if ( s43Item.getIntGraphFlg()==1 ) {
				log.Ep("S43_02","S43_02帳票作成開始","");
				S43_02 exceledit = new S43_02();
				ret = exceledit.s43_02EditExcel( s43Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S43_02","帳票作成エラー","");
					return false;
				}
				log.Ep("S43_02","S43_02帳票作成終了","");
				sheetLog.add("S43_02");
			}
			
		} catch(Exception e) {
			log.Err("99S43","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
/*
 * 作成日: 2004/09/16
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.com_set;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * 型・科目設定用のアクションフォーム
 * 
 * @author kawai
 */
abstract public class AbstractSubjectForm extends BaseForm {

	private String selection; // 選択方法
	private String[] analyze1; // 分析対象となる型・科目コードの配列（全選択）
	private String[] analyze2; // 分析対象となる型・科目コードの配列（個別選択）
	private String[] graph1; // グラフ表示対象となる型・科目コードの配列（全選択）
	private String[] graph2; // グラフ表示対象となる型・科目コードの配列（個別選択）

	/**
	 * @return
	 */
	public String[] getAnalyze1() {
		return analyze1;
	}

	/**
	 * @return
	 */
	public String[] getAnalyze2() {
		return analyze2;
	}

	/**
	 * @return
	 */
	public String[] getGraph1() {
		return graph1;
	}

	/**
	 * @return
	 */
	public String[] getGraph2() {
		return graph2;
	}

	/**
	 * @return
	 */
	public String getSelection() {
		return selection;
	}

	/**
	 * @param strings
	 */
	public void setAnalyze1(String[] strings) {
		analyze1 = strings;
	}

	/**
	 * @param strings
	 */
	public void setAnalyze2(String[] strings) {
		analyze2 = strings;
	}

	/**
	 * @param strings
	 */
	public void setGraph1(String[] strings) {
		graph1 = strings;
	}

	/**
	 * @param strings
	 */
	public void setGraph2(String[] strings) {
		graph2 = strings;
	}

	/**
	 * @param string
	 */
	public void setSelection(String string) {
		selection = string;
	}

}


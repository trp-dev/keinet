/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.com_set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.AbstractActionFormFactory;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CommonFormCreator {

	/**
	 * 共通アクションフォームを生成する
	 * @param request
	 * @return
	 */
	public static CM001Form createCommonForm(HttpServletRequest request) {

		// セッション
		HttpSession session = request.getSession(false);
		// ログイン情報
		LoginSession login = (LoginSession) session.getAttribute(LoginSession.SESSION_KEY);
		// プロファイル
		Profile profile = (Profile) session.getAttribute(Profile.SESSION_KEY);
		// 共通アクションフォーム
		CM001Form form = (CM001Form) session.getAttribute(CM001Form.SESSION_KEY);
		
		// 対象年度と対象模試を更新
		// パラメータがない場合
		if (request.getParameter("targetYear") == null) {
			// 設定されていなければプロファイルを採用
			if (form.getTargetYear() == null) {
				form.setTargetYear(profile.getTargetYear());
				form.setTargetExam(profile.getTargetExam());
			}

		// パラメータがある場合
		} else {
			form.setTargetYear(request.getParameter("targetYear"));
			form.setTargetExam(request.getParameter("targetExam"));
		}

		// それでもNULLならエラー
		if (form.getTargetYear() == null) throw new IllegalArgumentException("対象模試の設定をしてください。");

		form.setChanged("0");
		form.setSave("0");

		// チェックの有無
		String check = request.getParameter("check");
		if (check == null)
			// デフォルトはチェックする
			form.setCheck("1");
		else
			form.setCheck(check);

		// 個別アクションフォームを初期化して格納する
		// 型・科目・比較対象クラスは画面が開かれたタイミングで初期化する
		form.clearActionForms();

		form.setActionForm("CM301Form",
			AbstractActionFormFactory.getFactory("cm301").createActionForm(request));		

		form.setActionForm("CM401Form",
			AbstractActionFormFactory.getFactory("cm401").createActionForm(request));		

		form.setActionForm("CM404Form",
			AbstractActionFormFactory.getFactory("cm404").createActionForm(request));		

		form.setActionForm("CM406Form",
			AbstractActionFormFactory.getFactory("cm406").createActionForm(request));		

		form.setActionForm("CM601Form",
			AbstractActionFormFactory.getFactory("cm601").createActionForm(request));		
		
		form.setActionForm("CM603Form",
			AbstractActionFormFactory.getFactory("cm603").createActionForm(request));		
		
		form.setActionForm("CM604Form",
			AbstractActionFormFactory.getFactory("cm604").createActionForm(request));		
		
		return form;		

	}

}

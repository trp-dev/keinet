/*
 * 作成日: 2004/09/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.help;

import java.io.IOException;
import java.sql.Connection;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.help.HelpListBean;
import jp.co.fj.keinavi.beans.help.MailSender;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.help.H005Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.servlets.KNServletException;



/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class H005Servlet extends DefaultHttpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// ログイン情報
		final LoginSession login = getLoginSession(request);
		// アクションフォーム
		final H005Form form = (H005Form) getActionForm(request,
			       "jp.co.fj.keinavi.forms.help.H005Form");

		if(("h005").equals(getForward(request))) {
			Connection con = null; // コネクション
			try {

				con = super.getConnectionPool(request); 	// コネクション取得

				// ヘルプメッセージを取得して送信
				Vector mailContents = new Vector();
				mailContents.add(form.getHelpId());
				mailContents.add(form.getUseful());
				mailContents.add(form.getComment());
				mailContents.add(form.getName());
				mailContents.add(login.getLoginID());
				// メッセージ送信
				MailSender.sendMessage("Help", mailContents);	

				HelpListBean bean = new HelpListBean();
				bean.setFormId("h005");						// 送信完了
				bean.setHelpId(form.getHelpId());
				bean.setCategory(form.getCategory());
				bean.setFreeWord(form.getFreeWord());
				bean.setUseful(form.getUseful());
				bean.setComment(form.getComment());
				bean.setName(form.getName());
				bean.setDetailBack(form.getDetailBack());
				bean.setPage(form.getPage());
				bean.setConnection(null, con);
				bean.execute();

				request.setAttribute("HelpListBean", bean);

				//	アクセスログ
				actionLog(request, "2001", form.getHelpId());
			} catch (final Exception e) {
				KNServletException k = new KNServletException(e.getMessage());
				k.initCause(e);
				k.setErrorCode("1");
				k.setErrorMessage("メール送信に失敗しました。");
				throw k;
			} finally {
				super.releaseConnectionPool(request, con); // コネクション解放
			}

			// アクションフォーム
			request.setAttribute("form", form);

			super.forward(request, response, JSP_H005);

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

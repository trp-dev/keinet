package jp.co.fj.keinavi.forms.cls;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class C001Form extends BaseForm {

	private String[] outItem; // 一括出力対象
	private String changed; // フォームに変更を加えたかどうか

	/**
	 * @return
	 */
	public String[] getOutItem() {
		return outItem;
	}

	/**
	 * @param strings
	 */
	public void setOutItem(String[] strings) {
		outItem = strings;
	}

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {


	}

	/**
	 * @return
	 */
	public String getChanged() {
		return changed;
	}

	/**
	 * @param string
	 */
	public void setChanged(String string) {
		changed = string;
	}

}

/**
 * 個人成績分析−面談シート　設問別成績グラフ
 * 	Excelファイル編集
 * 作成日: 2004/09/16
 * @author	H.Fujimoto
 * 
 * 2009.10.13   Fujito URAKAWA - Totec
 *              「全体成績」追加対応 
 * 2009.11.10   Fujito URAKAWA - Totec
 *              「学力レベル」項目を生かした新メニュー対応 
 */

package jp.co.fj.keinavi.excel.personal;

import java.io.*;
import java.util.*;
import java.text.*;

import jp.co.fj.keinavi.excel.data.personal.*;
import jp.co.fj.keinavi.excel.cm.*;

import org.apache.poi.hssf.usermodel.*;
//import org.apache.poi.poifs.filesystem.*;
import jp.co.fj.keinavi.util.log.*;

public class I12_03 {

	private int	noerror		= 0;	// 正常終了
	private int	errfread	= 1;	// ファイルreadエラー
	private int	errfwrite	= 2;	// ファイルwriteエラー
	private int	errfdata	= 3;	// データ設定エラー

	private CM		cm			= new CM();	// 共通関数用クラス インスタンス

	//2004.10.28 Update
	//final private int	intMaxSheetSr	= 5;			// 最大シート数	
	final private int	intMaxSheetSr	= 3;			// 最大シート数
	//Update End	
	final private String	strArea			= "A1:Z69";		// 印刷範囲	


	//2005.04.26 Add 新テスト対応	
	//final private String	masterfile		= "I12_03";	// ファイル名
	final private String 	masterfile0 	= "I12_03";	// ファイル名１(新テスト対応用)
	final private String 	masterfile1 	= "I12_03";	// ファイル名２(新テスト対応用)
	private String masterfile = "";



	final private String	reportfile		= "I12_03";		// ActionID

	private boolean bolSheetCngFlg = true;			//改シートフラグ
	int		intMaxSheetIndex	= 1;				//シート数

/*
 * 	Excel編集メイン
 * 		I11Item i11Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
	public int i12_03EditExcel(I11Item i11Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		
		NumberFormat nf = NumberFormat.getInstance();
		KNLog log = KNLog.getInstance(null,null,null);

		//2005.04.26 Add 新テスト対応
		//テンプレートの決定
		if (i11Item.getIntSetsumonShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			
			// データセット
			ArrayList	i11List			= i11Item.getI11List();
			Iterator	itr				= i11List.iterator();

			int		fileIndex		= 1;	// ファイルカウンター

			// マスタExcel読み込み
			FileInputStream	fin			= null;
			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			// 基本ファイルを読込む
			I11ListBean i11ListBean = new I11ListBean();

			log.Ep("I12_03","基本データデータセット開始","");

			//2005.04.26 Update 新テスト対応 設問別成績グラフ出力種別フラグの値で、
			//					全国の値の表示を切り替える
			//設問別成績グラフ出力種別フラグ取得
			int intSSFlg = i11Item.getIntSetsumonShubetsuFlg();


			/** データリスト **/
			while( itr.hasNext() ) {
				fileIndex = 1;
				i11ListBean = (I11ListBean) itr.next();
				intMaxSheetIndex = 1;
				bolSheetCngFlg = true;

				// マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook.equals(null) ){
					return errfread;
				}
				
				ArrayList i12SetsumonList = i11ListBean.getI12SetsumonList();
				Iterator itrI12Setsumon = i12SetsumonList.iterator();
				
				// 基本ファイルを読込む
				I12SetsumonListBean i12SetsumonListBean = new I12SetsumonListBean();
				
				int row = 0;			//行
				int col = 0;			//列
				String kmk = "";		//科目チェック用
				int kmkCnt = 0;		//型・科目数 
				int setsumonCnt = 0;	//設問数 2005.05.25 追加

				log.Ep("I12_03","設問データデータセット開始","");
				
				//2004.10.21 Add ０件データ対応
				if( itrI12Setsumon.hasNext() == false ){
					// データセットするシートの選択
					workSheet = workbook.getSheet(String.valueOf(1));

					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);

					// セキュリティスタンプセット
					//2004.10.26 Update
					String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntSetsumonSecuFlg() ,25 ,25 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 25 );
					//Update end
					workCell.setCellValue(secuFlg);

					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
					workCell.setCellValue( "学校名　：" + i11ListBean.getStrGakkomei() + "　学年：" + i11ListBean.getStrGrade() + "　クラス名：" + i11ListBean.getStrClass() + "　クラス番号：" + i11ListBean.getStrClassNum() );

					// 氏名・性別セット	
					String strSex = "";
					if( cm.toString(i11ListBean.getStrSex()).equals("1") ){
						strSex = "男";
					}else if( cm.toString(i11ListBean.getStrSex()).equals("2") ){
						strSex = "女";
					}else{
						strSex = "不明";
					}
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );

					if( !cm.toString(i11ListBean.getStrShimei()).equals("")){
						workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrShimei()) + "（" + cm.toString(i11ListBean.getStrKana()) + "）　性別：" + strSex );
					}else{
						workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrKana()) + "　性別：" + strSex );
					}


					// 模試月取得
					String moshi =cm.setTaisyouMoshi( i11Item.getStrBaseMshDate() );
					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
					workCell.setCellValue( "対象模試：" + i11Item.getStrBaseMshmei() + moshi);
					
					//2005.04.26 Update 新テスト対応 設問別成績グラフ出力種別フラグの値で、
					//					全国の値の表示を切り替える
					//設問種別フラグが0のときは、全国のデータを表示する
					if (intSSFlg == 0)
					{ 
						//"全国"の項目名表示
						workCell = cm.setCell( workSheet, workRow, workCell, 35, 0 );
						workCell.setCellValue( "全国");
						workCell = cm.setCell( workSheet, workRow, workCell, 35, 13 );
						workCell.setCellValue( "全国");
						workCell = cm.setCell( workSheet, workRow, workCell, 67, 0 );
						workCell.setCellValue( "全国");
						workCell = cm.setCell( workSheet, workRow, workCell, 67, 13 );
						workCell.setCellValue( "全国");
						//平均点、得点率(％)の項目名表示
						workCell = cm.setCell( workSheet, workRow, workCell, 35, 1 );
						workCell.setCellValue( "平均点");
						workCell = cm.setCell( workSheet, workRow, workCell, 36, 1 );
						workCell.setCellValue( "得点率(%)");
						workCell = cm.setCell( workSheet, workRow, workCell, 35, 14 );
						workCell.setCellValue( "平均点");
						workCell = cm.setCell( workSheet, workRow, workCell, 36, 14 );
						workCell.setCellValue( "得点率(%)");
						workCell = cm.setCell( workSheet, workRow, workCell, 67, 1 );
						workCell.setCellValue( "平均点");
						workCell = cm.setCell( workSheet, workRow, workCell, 68, 1 );
						workCell.setCellValue( "得点率(%)");
						workCell = cm.setCell( workSheet, workRow, workCell, 67, 14 );
						workCell.setCellValue( "平均点");
						workCell = cm.setCell( workSheet, workRow, workCell, 68, 14 );
						workCell.setCellValue( "得点率(%)");

					}					
				}
				//Add End

				/** データリスト **/
				while( itrI12Setsumon.hasNext() ) {
					i12SetsumonListBean = (I12SetsumonListBean)itrI12Setsumon.next();
					//科目が変わる時のチェック
					if ( !kmk.equals(i12SetsumonListBean.getStrKmkmei()) ) {
						kmkCnt++;
						setsumonCnt = 0;
						if (kmkCnt == 5) {
							bolSheetCngFlg = true;
							kmkCnt = 1;
						}
					}else{
						//設問が10を超えたかﾁｪｯｸ 2005.05.25
						if (setsumonCnt==11){
							kmkCnt++;
							setsumonCnt = 0;
							if (kmkCnt == 5) {
								bolSheetCngFlg = true;
								kmkCnt = 1;
							}
						}
					}

					//４表毎に改シート
					if (bolSheetCngFlg == true) {

						//シート数がMAXを超えると改ファイル
						if (intMaxSheetIndex > intMaxSheetSr) {
							
							// シートの選択
							workbook.getSheet("1").setSelected(true);

							// Excelファイル保存
							boolean bolRet = false;
							String strFileName = masterfile + "_" + cm.toString(i11ListBean.getStrGrade()) + cm.toString(i11ListBean.getStrClass()) + cm.toString(i11ListBean.getStrClassNum());
							if( (itrI12Setsumon.hasNext()==false)&&(fileIndex == 1) ){
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, strFileName, intMaxSheetIndex-1);
							}else{
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, strFileName, intMaxSheetIndex-1);
							}
							fileIndex++;
								if( bolRet == false ){
								return errfwrite;					
							}
								intMaxSheetIndex = 1;
						}

						// データセットするシートの選択
						workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex));
						intMaxSheetIndex++;

						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);

						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntSetsumonSecuFlg() ,25 ,25 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 25 );
						workCell.setCellValue(secuFlg);

						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
						workCell.setCellValue( "学校名　：" + i11ListBean.getStrGakkomei() + "　学年：" + i11ListBean.getStrGrade() + "　クラス名：" + i11ListBean.getStrClass() + "　クラス番号：" + i11ListBean.getStrClassNum() );

						// 氏名・性別セット	
						String strSex = "";
						if( cm.toString(i11ListBean.getStrSex()).equals("1") ){
							strSex = "男";
						}else if( cm.toString(i11ListBean.getStrSex()).equals("2") ){
							strSex = "女";
						}else{
							strSex = "不明";
						}
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );

						if( !cm.toString(i11ListBean.getStrShimei()).equals("")){
							workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrShimei()) + "（" + cm.toString(i11ListBean.getStrKana()) + "）　性別：" + strSex );
						}else{
							workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrKana()) + "　性別：" + strSex );
						}


						// 模試月取得
						String moshi =cm.setTaisyouMoshi( i11Item.getStrBaseMshDate() );
						// 対象模試セット
						workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
						workCell.setCellValue( "対象模試：" + i11Item.getStrBaseMshmei() + moshi);


						//設問種別フラグが0のときは、全国のデータを表示する →　2005/5/23 追加
						if (intSSFlg == 0)
						{ 
							//"全国"の項目名表示
							workCell = cm.setCell( workSheet, workRow, workCell, 35, 0 );
							workCell.setCellValue( "全国");
							workCell = cm.setCell( workSheet, workRow, workCell, 35, 13 );
							workCell.setCellValue( "全国");
							workCell = cm.setCell( workSheet, workRow, workCell, 67, 0 );
							workCell.setCellValue( "全国");
							workCell = cm.setCell( workSheet, workRow, workCell, 67, 13 );
							workCell.setCellValue( "全国");
							//平均点、得点率(％)の項目名表示
							workCell = cm.setCell( workSheet, workRow, workCell, 35, 1 );
							workCell.setCellValue( "平均点");
							workCell = cm.setCell( workSheet, workRow, workCell, 36, 1 );
							workCell.setCellValue( "得点率(%)");
							workCell = cm.setCell( workSheet, workRow, workCell, 35, 14 );
							workCell.setCellValue( "平均点");
							workCell = cm.setCell( workSheet, workRow, workCell, 36, 14 );
							workCell.setCellValue( "得点率(%)");
							workCell = cm.setCell( workSheet, workRow, workCell, 67, 1 );
							workCell.setCellValue( "平均点");
							workCell = cm.setCell( workSheet, workRow, workCell, 68, 1 );
							workCell.setCellValue( "得点率(%)");
							workCell = cm.setCell( workSheet, workRow, workCell, 67, 14 );
							workCell.setCellValue( "平均点");
							workCell = cm.setCell( workSheet, workRow, workCell, 68, 14 );
							workCell.setCellValue( "得点率(%)");

						}					


						bolSheetCngFlg = false;
					}
					
					// 設問が１０を超えたときの対応 2005.05.25
					if ( !kmk.equals(i12SetsumonListBean.getStrKmkmei()) || setsumonCnt == 0 ) {
						if (kmkCnt == 1) {
							// 型・科目
							workCell = cm.setCell( workSheet, workRow, workCell, 6, 0 );
							workCell.setCellValue( i12SetsumonListBean.getStrKmkmei());
							// 学力レベル
							workCell = cm.setCell( workSheet, workRow, workCell, 32, 0 );
							if (!"".equals(i12SetsumonListBean.getStrScholarLevel())){
								workCell.setCellValue( i12SetsumonListBean.getStrScholarLevel()+"レベル");
							}
							row = 27;
							col = 2;						
						}else if (kmkCnt == 2) {
							// 型・科目
							workCell = cm.setCell( workSheet, workRow, workCell, 6, 13 );
							workCell.setCellValue( i12SetsumonListBean.getStrKmkmei());
							// 学力レベル
							workCell = cm.setCell( workSheet, workRow, workCell, 32, 13 );
							if (!"".equals(i12SetsumonListBean.getStrScholarLevel())){
								workCell.setCellValue( i12SetsumonListBean.getStrScholarLevel()+"レベル");
							}
							row = 27;
							col = 15;						
						}else if (kmkCnt == 3) {
							// 型・科目
							workCell = cm.setCell( workSheet, workRow, workCell, 38, 0 );
							workCell.setCellValue( i12SetsumonListBean.getStrKmkmei());
							// 学力レベル
							workCell = cm.setCell( workSheet, workRow, workCell, 64, 0 );
							if (!"".equals(i12SetsumonListBean.getStrScholarLevel())){
								workCell.setCellValue( i12SetsumonListBean.getStrScholarLevel()+"レベル");
							}
							row = 59;
							col = 2;						
						}else if (kmkCnt == 4) {
							// 型・科目
							workCell = cm.setCell( workSheet, workRow, workCell, 38, 13 );
							workCell.setCellValue( i12SetsumonListBean.getStrKmkmei());
							// 学力レベル
							workCell = cm.setCell( workSheet, workRow, workCell, 64, 13 );
							if (!"".equals(i12SetsumonListBean.getStrScholarLevel())){
								workCell.setCellValue( i12SetsumonListBean.getStrScholarLevel()+"レベル");
							}
							row = 59;
							col = 15;						
						}
					}else{
						if (kmkCnt == 1) {
							row = 27;
						}else if (kmkCnt == 2) {
							row = 27;
						}else if (kmkCnt == 3) {
							row = 59;
						}else if (kmkCnt == 4) {
							row = 59;
						}
					}
					
					// 設問番号
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					workCell.setCellValue( cm.toString(i12SetsumonListBean.getStrSetsuNo()));
					//Update end
					// 設問内容
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					workCell.setCellValue( cm.toString(i12SetsumonListBean.getStrSetsuMei1()));
					// 設問配点
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					if (!cm.toString(i12SetsumonListBean.getStrSetsuHaiten()).equals("")){
						workCell.setCellValue( Integer.parseInt(i12SetsumonListBean.getStrSetsuHaiten()));
					}
					// 個人得点
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					if( i12SetsumonListBean.getIntTokutenSelf() != -999 ){
						workCell.setCellValue( i12SetsumonListBean.getIntTokutenSelf());
					}
					// 個人得点率
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					if( i12SetsumonListBean.getFloTokuritsuSelf() != -999.0 ){
						workCell.setCellValue( i12SetsumonListBean.getFloTokuritsuSelf());
					}
					// 学力レベル別得点率
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					if( i12SetsumonListBean.getFloScholarLevel() != -999.0 ){
						workCell.setCellValue( i12SetsumonListBean.getFloScholarLevel());
					}
					// 校内平均点
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					if( i12SetsumonListBean.getFloTokutenHome() != -999.0 ){
						workCell.setCellValue( i12SetsumonListBean.getFloTokutenHome());
					}
					// 校内得点率
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
					if( i12SetsumonListBean.getFloTokuritsuHome() != -999.0 ){
						workCell.setCellValue( i12SetsumonListBean.getFloTokuritsuHome());
					}
					
					//2005.04.26 Update 新テスト対応 設問別成績グラフ出力種別フラグの値で、
					//					全国の値の表示を切り替える

					//設問種別フラグが0のときは、全国のデータを表示する
					if (intSSFlg == 0)
					{ 

						// 全国平均点
						workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
						if( i12SetsumonListBean.getFloTokutenAll() != -999.0 ){
							workCell.setCellValue( i12SetsumonListBean.getFloTokutenAll());
						}
						// 全国得点率
						workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
						if( i12SetsumonListBean.getFloTokuritsuAll() != -999.0 ){
							workCell.setCellValue( i12SetsumonListBean.getFloTokuritsuAll());
						}
					}

					kmk = i12SetsumonListBean.getStrKmkmei();
					col++;
					setsumonCnt++;
				}
				
				log.Ep("I12_03","設問データデータセット終了","");

				// シートの選択
				workbook.getSheet("1").setSelected(true);
	
				// Excelファイル保存
				boolean bolRet = false;
				String strFileName = masterfile + "_" + cm.toString(i11ListBean.getStrGrade()) + cm.toString(i11ListBean.getStrClass()) + cm.toString(i11ListBean.getStrClassNum());
				if( (itr.hasNext()==false)&&(fileIndex == 1) ){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, strFileName, intMaxSheetIndex-1);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, strFileName, intMaxSheetIndex-1);
				}
				fileIndex++;

				if( bolRet == false ){
					return errfwrite;					
				}
				
				kmkCnt = 0;
				
			}	// while( itr.hasNext() )

		} catch(Exception e) {
			log.Err("I12_03","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("I12_03","基本データデータセット終了","");
		return noerror;
	}

}
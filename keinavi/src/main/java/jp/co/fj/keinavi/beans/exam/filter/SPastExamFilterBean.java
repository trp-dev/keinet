package jp.co.fj.keinavi.beans.exam.filter;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * 校内成績分析・過回比較用の模試フィルタ
 * 
 * 2006.08.23	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class SPastExamFilterBean implements IExamFilterBean {

	/**
	 * @see jp.co.fj.keinavi.util.taglib.exam.IExamFilterBean#execute(
	 * 			jp.co.fj.keinavi.data.LoginSession,
	 * 			jp.co.fj.keinavi.data.ExamSession,
	 * 			jp.co.fj.keinavi.data.ExamData)
	 */
	public boolean execute(final LoginSession login,
			final ExamSession examSession, final ExamData exam) {
		
		// 今年度
		if (KNUtil.getPastExamList(examSession, exam).size() > 0) {
			return true;
		}
		
		// 前年度
		final ExamData e = getPrevExamData(exam);
		if (KNUtil.getPastExamList(examSession, e).size() > 0) {
			return true;
		}

		// 前々年度
		return KNUtil.getPastExamList(examSession, getPrevExamData(e)).size() > 0;
	}

	// 前年度の模試データを取得する
	private ExamData getPrevExamData(final ExamData exam) {
		final ExamData e = new ExamData();
		e.setExamYear(String.valueOf(Integer.parseInt(exam.getExamYear()) - 1));
		e.setExamCD(exam.getExamCD());
		e.setExamTypeCD(exam.getExamTypeCD());
		e.setTargetGrade("0" + (Integer.parseInt(exam.getTargetGrade()) - 1));
		e.setDataOpenDate(exam.getDataOpenDate());
		return e;
	}

}

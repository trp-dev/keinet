/**
 * クラス成績分析−クラス成績概況−個人成績
 * 	Excelファイル編集
 * 作成日: 2004/08/31
 * @author	Ito.Y
 *
 * 2009.11.24   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 *
 */

package jp.co.fj.keinavi.excel.cls;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C12Item;
import jp.co.fj.keinavi.excel.data.cls.C12KmkSeisekiListBean;
import jp.co.fj.keinavi.excel.data.cls.C12ListBean;
import jp.co.fj.keinavi.excel.data.cls.C12SeisekiListBean;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
// 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD END
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class C12_01 {

    private int noerror	= 0;		// 正常終了
    private int errfread	= 1;		// ファイルreadエラー
    private int errfwrite	= 2;		// ファイルwriteエラー
    private int errfdata	= 3;		// データ設定エラー

    private CM cm = new CM();		//共通関数用クラス インスタンス

    final private String	masterfile0	= "C12_01";	// ファイル名
    final private String	masterfile1	= "C12_01";	// ファイル名
    private String	masterfile	= "";				// ファイル名
//	final private int[]	tabCol	= {5,9,15,21,27,33};	// 表の基準点(今年度)

    /*
     * 	Excel編集メイン
     * 		C12Item c12Item: データクラス
     * 		String outfile: 出力Excelファイル名（フルパス）
     * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
     * 		String	UserID：ユーザーID
     * 		戻り値: 0:正常終了、0以外:異常終了
     */
     public int c12_01EditExcel(C12Item c12Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
        KNLog log = KNLog.getInstance(null,null,null);
        log.Ep("C12_01","C12_01帳票作成開始","");

        //テンプレートの決定
        if (c12Item.getIntShubetsuFlg() == 1){
            masterfile = masterfile1;
        } else{
            masterfile = masterfile0;
        }

        try {
            HSSFWorkbook	workbook	= null;
            HSSFSheet		workSheet	= null;
            HSSFRow			workRow		= null;
            HSSFCell		workCell	= null;

            int		row				= 0;	// 行(一人当たり)
            int		col				= 0;	// 列
            int		ninzuCnt		= 0;	// 人数カウンター
            int		maxSheetIndex	= 0;	// シートカウンター
            int		maxSheetNum		= 50;	// 最大シート数
            int		fileIndex		= 1;	// ファイルカウンター
            boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
            boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
            float		hensaAll		= 0;	// 全国偏差値

            // 基本ファイルを読込む
            C12ListBean c12ListBean = new C12ListBean();

            // データセット
            ArrayList	c12List			= c12Item.getC12List();
            Iterator	itr				= c12List.iterator();

            /** 学年・クラス情報 **/
            while( itr.hasNext() ) {
                c12ListBean = (C12ListBean)itr.next();

                //学年・クラスが変わる時
                bolSheetCngFlg = true;
                if(maxSheetIndex>=maxSheetNum){
                    bolBookCngFlg = true;
                }

                // 基本ファイルを読み込む
                C12SeisekiListBean	c12SeisekiListBean	= new C12SeisekiListBean();

                //データの保持
                ArrayList	c12SeisekiList	= c12ListBean.getC12SeisekiList();
                Iterator	itrC12Seiseki	= c12SeisekiList.iterator();

                /** 個人Data **/
                while(itrC12Seiseki.hasNext()){
                    c12SeisekiListBean = (C12SeisekiListBean) itrC12Seiseki.next();

                    if(bolBookCngFlg){
                        if(maxSheetIndex>=maxSheetNum){
                            // Excelファイル保存
                            boolean bolRet = false;
                            bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, maxSheetIndex);
                            fileIndex++;

                            if( bolRet == false ){
                                return errfwrite;
                            }
                        }

                        if((maxSheetIndex>=maxSheetNum)||(maxSheetIndex==0)){
                            //マスタExcel読み込み
                            workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
                            if( workbook==null ){
                                return errfread;
                            }



                            bolBookCngFlg = false;
                            maxSheetIndex=0;
                        }
                    }

                    // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
                    // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
                    // 国語記述用のスタイルを作成
                    //HSSFCellStyle cellStyle = this.createStyle(workbook);
                    // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END
                    // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END

                    if ( bolSheetCngFlg ) {
                        // シートテンプレートのコピー
                        workSheet = workbook.cloneSheet(0);
                        maxSheetIndex++;

                        // ヘッダ右側に帳票作成日時を表示する
                        cm.setHeader(workbook, workSheet);

                        // セキュリティスタンプセット
                        String secuFlg = cm.setSecurity( workbook, workSheet, c12Item.getIntSecuFlg() ,45 ,49 );
                        workCell = cm.setCell( workSheet, workRow, workCell, 0, 45 );
                        workCell.setCellValue(secuFlg);

                        // 注釈セット
                        if (c12Item.getIntShubetsuFlg() == 1){
                        } else{
                            workCell = cm.setCell( workSheet, workRow, workCell, 2, 49 );
                            workCell.setCellValue( "※総合成績の全国偏差値より2.5ポイント以上低い科目の全国偏差値に\"#\"を表示しています。" );
                        }

                        // ラベルセット
                        if (c12Item.getIntShubetsuFlg() == 1){
                        } else{
                            workCell = cm.setCell( workSheet, workRow, workCell, 5, 8 );
                            workCell.setCellValue( "全国\n偏差値" );
                            workCell = cm.setCell( workSheet, workRow, workCell, 5, 13 );
                            workCell.setCellValue( "全国\n偏差値" );
                            workCell = cm.setCell( workSheet, workRow, workCell, 5, 21 );
                            workCell.setCellValue( "全国\n偏差値" );
                            workCell = cm.setCell( workSheet, workRow, workCell, 5, 29 );
                            workCell.setCellValue( "全国\n偏差値" );
                            workCell = cm.setCell( workSheet, workRow, workCell, 5, 37 );
                            workCell.setCellValue( "全国\n偏差値" );
                            workCell = cm.setCell( workSheet, workRow, workCell, 5, 45 );
                            workCell.setCellValue( "全国\n偏差値" );
                            workCell = cm.setCell( workSheet, workRow, workCell, 5, 16 );
                            workCell.setCellValue( "換算\n得点" );
                            workCell = cm.setCell( workSheet, workRow, workCell, 5, 24 );
                            workCell.setCellValue( "換算\n得点" );
                            workCell = cm.setCell( workSheet, workRow, workCell, 5, 32 );
                            workCell.setCellValue( "換算\n得点" );
                            workCell = cm.setCell( workSheet, workRow, workCell, 5, 40 );
                            workCell.setCellValue( "換算\n得点" );
                            workCell = cm.setCell( workSheet, workRow, workCell, 5, 48 );
                            workCell.setCellValue( "換算\n得点" );
                        }

                        // 学校名セット
                        String strGakkoMei	="学校名　：" + cm.toString(c12Item.getStrGakkomei())
                                            + "　学年：" + cm.toString(c12ListBean.getStrGrade()) + "　クラス名：" + cm.toString(c12ListBean.getStrClass());
                        workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
                        workCell.setCellValue( strGakkoMei );

                        // 対象模試セット
                        String moshi =cm.setTaisyouMoshi( c12Item.getStrMshDate() );	// 模試月取得
                        workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
                        workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(c12Item.getStrMshmei()) + moshi);

                        //　改シートフラグをFalseにする
                        bolSheetCngFlg	=false;
                        ninzuCnt=0;
                    }

                    // 学年セット
                    workCell = cm.setCell( workSheet, workRow, workCell, 8+3*ninzuCnt, 0 );
                    workCell.setCellValue( c12SeisekiListBean.getStrGrade() );

                    // クラスセット
                    workCell = cm.setCell( workSheet, workRow, workCell, 8+3*ninzuCnt, 1 );
                    workCell.setCellValue( c12SeisekiListBean.getStrClass() );

                    // クラス番号セット
                    workCell = cm.setCell( workSheet, workRow, workCell, 8+3*ninzuCnt, 2 );
                    workCell.setCellValue( c12SeisekiListBean.getStrClassNo() );

                    // 氏名セット
                    workCell = cm.setCell( workSheet, workRow, workCell, 8+3*ninzuCnt, 3 );
                    workCell.setCellValue( c12SeisekiListBean.getStrKanaName() );

                    log.It("C12_01",c12SeisekiListBean.getStrKanaName(),"");

                    // 性別セット
                    int sexKbn = Integer.parseInt( cm.toString(c12SeisekiListBean.getStrSex()) );
                    String strSex = "";
                    if( sexKbn == 1 ){
                        strSex = "男";
                    }else if( sexKbn == 2 ){
                        strSex = "女";
                    }else{		//}else if( sexKbn == 9 ){
                        strSex = "不";
                    }
                    workCell = cm.setCell( workSheet, workRow, workCell, 8+3*ninzuCnt, 4 );
                    workCell.setCellValue( strSex );

                    // 基本ファイルを読み込む
                    C12KmkSeisekiListBean	c12KmkSeisekiListBean	= new C12KmkSeisekiListBean();

                    //データの保持
                    ArrayList	c12KmkSeisekiList	= c12SeisekiListBean.getC12KmkSeisekiList();
                    Iterator	itrC12KmkSeiseki	= c12KmkSeisekiList.iterator();

                    /** 個人詳細Data **/
                    String strKykHoji = "";
                    String kyouka = "";	//型の7000番台と8000番台の判断用
                    col = 0;
                    row = 0;
                    hensaAll = 0;

                    //第一解答科目の成績
                    C12KmkSeisekiListBean rikaSeiseki = null;
                    C12KmkSeisekiListBean chikouSeiseki = null;

                    while(itrC12KmkSeiseki.hasNext()){
                        c12KmkSeisekiListBean = (C12KmkSeisekiListBean) itrC12KmkSeiseki.next();

                        // 教科コードによって判定
                        String strKyouka = c12KmkSeisekiListBean.getStrKyokaCd()+" ";

                        log.It("C12_01",c12KmkSeisekiListBean.getStrKyokaCd() + " ",c12KmkSeisekiListBean.getStrKmkCd());

                        strKyouka = strKyouka.substring(0,1);
                        if( !cm.toString(strKyouka).equals(cm.toString(strKykHoji)) ){
                            switch( Integer.parseInt( strKyouka ) ){
                                case 1:	//英語
                                    col = 10;
                                    row = 0;
                                    break;
                                case 2:	//数学
                                    col = 18;
                                    row = 0;
                                    break;
                                case 3:	//国語
                                    col = 26;
                                    row = 0;
                                    break;
                                case 4:	//理科
                                    col = 34;
                                    row = 0;
                                    break;
                                case 5:	//地歴公民
                                    col = 42;
                                    row = 0;
                                    break;
                                case 7:	//総合
                                    col = 5;
                                    row = 0;
                                    break;
                                case 8:
                                    if( strKykHoji.equals("7") == false){
                                        row = 0;
                                    }
                                    col = 5;
                                    break;
                                //2004.10.22 Add
                                default:
                                    strKyouka = "X";
                                    break;
                                //Add End
                            }
                            strKykHoji = strKyouka;
//							row = 0;
                        }

                        if( strKyouka.equals("X") == false ){
                            if( strKyouka.equals("7") || strKyouka.equals("8") ){	//総合
                                if (row < 3 && cm.toString(strKyouka).equals(kyouka) == false) {
                                    // 科目名セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getStrKmkmei());

                                    // 得点セット
                                    if( c12KmkSeisekiListBean.getIntTokuten() != -999 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col+1 );
                                        workCell.setCellValue( c12KmkSeisekiListBean.getIntTokuten());
                                    }

                                    // 校内偏差セット
                                    if( c12KmkSeisekiListBean.getFloHensaHome() != -999.0 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col+2 );
                                        workCell.setCellValue( c12KmkSeisekiListBean.getFloHensaHome());
                                    }

                                    // 全国偏差セット
                                    if( c12KmkSeisekiListBean.getFloHensaAll() != -999.0 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col+3 );
                                        workCell.setCellValue( c12KmkSeisekiListBean.getFloHensaAll());
                                    }

                                    // 学力レベルセット
                                    if ( !"".equals(c12KmkSeisekiListBean.getStrScholarLevel())) {
                                        workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col+4 );
                                        workCell.setCellValue( c12KmkSeisekiListBean.getStrScholarLevel());
                                    }

                                    // *取得
                                    if( row==0 && (cm.toString(strKyouka).equals("7") || cm.toString(strKyouka).equals("8")) ){
                                        if( c12KmkSeisekiListBean.getFloHensaAll() != -999.0 ){
                                            hensaAll = c12KmkSeisekiListBean.getFloHensaAll();
                                        }
                                    }
                                    row++;
                                    kyouka = strKyouka;
                                }
                            }
                            else{		//総合以外の教科
                                if(row < 3){
                                    //理科の先頭科目が基礎科目なら、先頭は空欄にする
                                    if ("4".equals(strKyouka) && row == 0 && "1".equals(c12KmkSeisekiListBean.getBasicFlg())) {
                                        row++;
                                    }

                                    // 科目名セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col );
                                    workCell.setCellValue( c12KmkSeisekiListBean.getStrKmkmei());

                                    // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
                                    // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
                                    // 国語記述なら得点のみ出力する
//                                    if(("3915").equals(c12KmkSeisekiListBean.getStrKmkCd())) {
//                                        // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD START
//                                		//workCell = cm.setCell( workSheet, workRow, workCell, 8+3*ninzuCnt, 27 );
//                                		workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col+1 );
//                                	// 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD END
//                                		// 出力する際セルの中央に寄せる
//                                		workCell.setCellStyle(cellStyle);
//                                        workCell.setCellValue( c12KmkSeisekiListBean.getStrKokugoTotalHyouka() );
//                                    }else {
                                    // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END
                                    // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END

                                    // 得点セット
                                    if( c12KmkSeisekiListBean.getIntTokuten() != -999 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col+1 );
                                        workCell.setCellValue( c12KmkSeisekiListBean.getIntTokuten());
                                    }

                                    // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 DEL START
                                    // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
//                                    if(("01").equals(c12KmkSeisekiListBean.getStrMosiType())) {
//                                    	workCell = cm.setCell( workSheet, workRow, workCell, 9+3*ninzuCnt, 10 );
//                                        workCell.setCellValue( "CEFR" );
//                                        workCell = cm.setCell( workSheet, workRow, workCell, 9+3*ninzuCnt, 11 );
//                                        workCell.setCellValue( c12KmkSeisekiListBean.getStrCefrScore() );
//                                    }
                                    // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END
                                    // 2019/11/25 QQ)Ooseto 英語認定試験延期対応 DEL END

                                    // 校内偏差セット
                                    if( c12KmkSeisekiListBean.getFloHensaHome() != -999.0 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col+2 );
                                        workCell.setCellValue( c12KmkSeisekiListBean.getFloHensaHome());
                                    }

                                    // 全国偏差セット
                                    if( c12KmkSeisekiListBean.getFloHensaAll() != -999.0 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col+4 );
                                        workCell.setCellValue( c12KmkSeisekiListBean.getFloHensaAll());
                                    }

                                    // 学力レベルセット
                                    if ( !"".equals(c12KmkSeisekiListBean.getStrScholarLevel())) {
                                        workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col+5 );
                                        workCell.setCellValue( c12KmkSeisekiListBean.getStrScholarLevel());
                                    }

                                    // #セット
                                    // 得点用のときは処理しない
                                    if (c12Item.getIntShubetsuFlg() == 1){
                                    } else{
                                        if( c12KmkSeisekiListBean.getFloHensaAll() != -999.0 ){
                                            if ( hensaAll >= c12KmkSeisekiListBean.getFloHensaAll()+2.5f) {
                                                workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col+3 );
                                                workCell.setCellValue( "#");
                                            }
                                        }
                                    }

                                    // 換算得点セット
                                    if( c12KmkSeisekiListBean.getIntKansanTokuten() != -999 ){
                                        workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col+7 );
                                        workCell.setCellValue( c12KmkSeisekiListBean.getIntKansanTokuten());
                                    }

                                    // *セット
                                    // マーク模試の場合のみ、換算得点が第１解答科目＜第２解答科目の場合に*表示
                                    if (KNUtil.isAns1st(c12Item.getStrMshCd())){
                                        if (cm.toString(strKyouka).equals("4") && c12KmkSeisekiListBean.getIntScope() == 0 && !"1".equals(c12KmkSeisekiListBean.getBasicFlg())){
                                            if (rikaSeiseki != null){
                                                if (rikaSeiseki.getIntKansanTokuten() < c12KmkSeisekiListBean.getIntKansanTokuten()){
                                                    workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col+6 );
                                                    workCell.setCellValue( "*");
                                                }
                                            }
                                        }else if (cm.toString(strKyouka).equals("5") && c12KmkSeisekiListBean.getIntScope() == 0){
                                            if (chikouSeiseki != null){
                                                if (chikouSeiseki.getIntKansanTokuten() < c12KmkSeisekiListBean.getIntKansanTokuten()){
                                                    workCell = cm.setCell( workSheet, workRow, workCell, 7+3*ninzuCnt+row, col+6 );
                                                    workCell.setCellValue( "*");
                                                }
                                            }
                                        }
                                    }
                                    // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
//                                    }
                                    // 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END
                                    // 第１解答科目保存（マーク模試のみ）
                                    if (KNUtil.isAns1st(c12Item.getStrMshCd())){
                                        if (cm.toString(strKyouka).equals("4") && c12KmkSeisekiListBean.getIntScope() == 1){
                                            rikaSeiseki = c12KmkSeisekiListBean;
                                        }else if (cm.toString(strKyouka).equals("5") && c12KmkSeisekiListBean.getIntScope() == 1){
                                            chikouSeiseki = c12KmkSeisekiListBean;
                                        }
                                    }

                                    row++;
                                }
                            }
                            log.It("C12_01","科目名：" + row + " ",c12KmkSeisekiListBean.getStrKmkmei());
                            strKykHoji = strKyouka;
                        }
                        //Update End
                    }
                    ninzuCnt++;
                    if(ninzuCnt >= 20){
                        ninzuCnt=0;
                        bolSheetCngFlg =true;
                        if(maxSheetIndex>=maxSheetNum){
                            bolBookCngFlg = true;
                        }
                    }
                }
            }

            // Excelファイル保存
            boolean bolRet = false;
            if(fileIndex != 1){
                bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, maxSheetIndex);
            }
            else{
                bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
            }

            if( bolRet == false ){
                return errfwrite;
            }

        } catch(Exception e) {
            log.Err("C12_01","データセットエラー",e.toString());
            return errfdata;
        }

        log.Ep("C12_01","C12_01帳票作成終了","");
        return noerror;
    }

// 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
  // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
//     /**
//      * スタイルを作成する
//      * @param  workbook - ワークブック
//      * @return style    - スタイル
//      */
//     private HSSFCellStyle createStyle(HSSFWorkbook workbook) {
//    	 // スタイルを新規作成
//    	 HSSFCellStyle style = workbook.createCellStyle();
//
//         // フォントを定義する
//    	 HSSFFont font = workbook.createFont();
//         // フォント名
//         font.setFontName("ＭＳ ゴシック");
//         // フォントサイズ
//         font.setFontHeightInPoints( (short)10 );
//         // 設定
//         style.setFont(font);
//         // 中央揃え
//         style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
//
//         style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//         style.setBorderRight(HSSFCellStyle.BORDER_THIN);
//
//
//         return style;
//     }
     // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END
// 2020/03/31 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END


}
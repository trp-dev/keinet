/**
 * 校内成績分析−クラス比較　成績概況（クラス比較）
 * 	Excelファイル編集
 * 作成日: 2004/07/28
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S31ClassListBean;
import jp.co.fj.keinavi.excel.data.school.S31Item;
import jp.co.fj.keinavi.excel.data.school.S31ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S31_02 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private boolean bolBookCngFlg = true;			//改ファイルフラグ
	private boolean bolSheetCngFlg = true;			//改シートフラグ
	private int intMaxSheetSr	= 40;					//MAXシート数の値を入れる
	
	final private String masterfile0 = "S31_02";
	final private String masterfile1 = "NS31_02";
	private String masterfile = "";

/*
 * 	Excel編集メイン
 * 		S31Item s31Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s31_02EditExcel(S31Item s31Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
 		
		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s31Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		int		intMaxSheetIndex	= 0;		//ファイルのシートの最大数
		int		row					= 0;		//行
		int		col					= 0;		//列
		int		classCnt			= 0;		//クラスカウンタ(自校＋クラス)
		int		kmkCnt				= 0;		//科目カウンタ
		String		kmk					= "";		//科目チェック用
		int		maxClass			= 0;		//MAXクラス数(自校＋クラス)
		int		sheetListIndex		= 0;		//リスト格納用シートカウンター
		int		fileIndex			= 1;		//ファイルカウンター
		int		houjiNum			= 5;		//1シートに表示できるクラス数
		int		sheetRowCnt			= 0;		//クラス用改シート格納用カウンタ
		boolean	kmkFlg				= true;	//型→科目変更フラグ
		ArrayList	workbookList	= new ArrayList();
		ArrayList	workSheetList	= new ArrayList();
//add 2004/10/26 T.Sakai データ0件対応
		int		dispClassFlgCnt		= 0;		// グラフ表示フラグカウンタ
//add end
		
		// 基本ファイルを読込む
		S31ListBean s31ListBean = new S31ListBean();
		
		try {
			
			// データセット
			ArrayList s31List = s31Item.getS31List();
			Iterator itr = s31List.iterator();
			
			if( itr.hasNext() == false ){
				return errfdata;
			}
			
			while( itr.hasNext() ) {
				s31ListBean = (S31ListBean)itr.next();
				if (s31ListBean.getIntDispKmkFlg()==1) {
					//科目が変わる時のチェック
					if (kmkFlg==true) {
						//型から科目に変わる時のチェック
						if (s31ListBean.getStrKmkCd().compareTo("7000") < 0) {
							kmkFlg = false;
							col = 0;
							kmkCnt = 0;
							bolSheetCngFlg = true;
							if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
								bolBookCngFlg = true;
							}
						}
					}
					//10型・科目ごとにシート名リスト初期化
					if (kmkCnt==0) {
						workSheetList	= new ArrayList();
						sheetListIndex = 0;
					}
					
					// workbookListにWorkBookが２つ以上格納されている時は出力処理を実行
					int listSize = workbookList.size();
					if(listSize>1){
						if(kmkCnt == 0){
							workbook = (HSSFWorkbook)workbookList.get(0);
		
							// Excelファイル保存
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, intMaxSheetSr);
							fileIndex++;
							if( bolRet == false ){
								return errfwrite;
							}
	
							// ファイル出力したデータは削除
							workbookList.remove(0);
	
							// WorkBook・変数を書き込んでる途中のものに戻す
							workbook = (HSSFWorkbook)workbookList.get(0);
						}
					}
					
					// 基本ファイルを読込む
					S31ClassListBean s31ClassListBean = new S31ClassListBean();
					
					// クラスデータセット
					ArrayList s31ClassList = s31ListBean.getS31ClassList();
					Iterator itrClass = s31ClassList.iterator();
					
					maxClass = 0;
					//クラスデータ件数取得
					while ( itrClass.hasNext() ){
						s31ClassListBean = (S31ClassListBean)itrClass.next();
						if (s31ClassListBean.getIntDispClassFlg()==1) {
							maxClass++;
						}
					}
					// クラス表示に必要なシート数の計算
					sheetRowCnt = (maxClass-1)/(houjiNum-1);
					if((maxClass-1)%(houjiNum-1)!=0){
						sheetRowCnt++;
					}
					if (sheetRowCnt==0) {
						sheetRowCnt++;
					}
					bolSheetCngFlg=true;
					
					int ninzu = 0;
					float heikin = 0;
					float hensa = 0;
					
					itrClass = s31ClassList.iterator();
					
					while ( itrClass.hasNext() ){
						s31ClassListBean = (S31ClassListBean)itrClass.next();
						
						if (s31ClassListBean.getIntDispClassFlg()==1) {
							if( bolBookCngFlg == true ){
								//マスタExcel読み込み
								workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
								if( workbook==null ){
									return errfread;
								}
								bolBookCngFlg = false;
								intMaxSheetIndex = 0;
								workbookList.add(workbook);
							}
							//クラス数が5(自校＋4クラス)以上または10型･科目ごとに改シート
							if ( bolSheetCngFlg == true ) {
								// データセットするシートの選択
								if (kmkCnt==0) {
									//クラス用改シート
									workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex+1));
									workSheetList.add(sheetListIndex, workSheet);
									intMaxSheetIndex++;
									sheetListIndex++;
								} else {
									//次列用シートの呼び出し
									workSheet = (HSSFSheet)workSheetList.get(sheetListIndex-sheetRowCnt);
									sheetRowCnt--;
								}
								if (kmkCnt==0) {
									// ヘッダ右側に帳票作成日時を表示する
									cm.setHeader(workbook, workSheet);
								
									// セキュリティスタンプセット
									String secuFlg = cm.setSecurity( workbook, workSheet, s31Item.getIntSecuFlg() ,38 ,40 );
									workCell = cm.setCell( workSheet, workRow, workCell, 0, 38 );
									workCell.setCellValue(secuFlg);
								
									// 学校名セット
									workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
									workCell.setCellValue( "学校名　：" + cm.toString(s31Item.getStrGakkomei()) );
			
									// 模試月取得
									String moshi =cm.setTaisyouMoshi( s31Item.getStrMshDate() );
									// 対象模試セット
									workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
									workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s31Item.getStrMshmei()) + moshi);
									col = 0;
								}
								row = 44;
								if ( kmkCnt==0 || !cm.toString(kmk).equals(cm.toString(s31ListBean.getStrKmkmei())) ) {
									// 型・科目名セット
									workCell = cm.setCell( workSheet, workRow, workCell, row++, col+1 );
									workCell.setCellValue( s31ListBean.getStrKmkmei() );
									// 配点セット
									if ( !cm.toString(s31ListBean.getStrHaitenKmk()).equals("") ) {
										workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
										workCell.setCellValue( "（" + s31ListBean.getStrHaitenKmk() + "）" );
									}
								}
								bolSheetCngFlg = false;
							}
							if ( classCnt==0 ) {
								row = 47;
								if (col==0) {
									// 学校名セット
									workCell = cm.setCell( workSheet, workRow, workCell, row, col );
									workCell.setCellValue( s31Item.getStrGakkomei() );
								}
								// 人数セット
								ninzu = s31ClassListBean.getIntNinzu();
								workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
								if ( s31ClassListBean.getIntNinzu() != -999 ) {
									workCell.setCellValue( s31ClassListBean.getIntNinzu() );
								}
								// 平均点セット
								heikin = s31ClassListBean.getFloHeikin();
								workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
								if ( s31ClassListBean.getFloHeikin() != -999.0 ) {
									workCell.setCellValue( s31ClassListBean.getFloHeikin() );
								}
								// 平均偏差値セット
								hensa = s31ClassListBean.getFloHensa();
								workCell = cm.setCell( workSheet, workRow, workCell, row++, col+4 );
								if ( s31ClassListBean.getFloHensa() != -999.0 ) {
									workCell.setCellValue( s31ClassListBean.getFloHensa() );
								}
							} else {
								if ( classCnt%houjiNum==0 ) {
									row = 47;
									if (col==0) {
										// 学校名セット
										workCell = cm.setCell( workSheet, workRow, workCell, row, col );
										workCell.setCellValue( s31Item.getStrGakkomei() );
									}
									// 人数セット
									workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
									if ( ninzu != -999 ) {
										workCell.setCellValue( ninzu );
									}
									// 平均点セット
									workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
									if ( heikin != -999.0 ) {
										workCell.setCellValue( heikin );
									}
									// 平均偏差値セット
									workCell = cm.setCell( workSheet, workRow, workCell, row++, col+4 );
									if ( hensa != -999.0 ) {
										workCell.setCellValue( hensa );
									}
									classCnt++;
								}
								if (col==0) {
									// 学年+クラス名セット
									workCell = cm.setCell( workSheet, workRow, workCell, row, col );
									workCell.setCellValue( cm.toString(s31ClassListBean.getStrGrade())+"年 "+cm.toString(s31ClassListBean.getStrClass())+"クラス" );
								}
								// 人数セット
								workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
								if ( s31ClassListBean.getIntNinzu() != -999 ) {
									workCell.setCellValue( s31ClassListBean.getIntNinzu() );
								}
								// 平均点セット
								workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
								if ( s31ClassListBean.getFloHeikin() != -999.0 ) {
									workCell.setCellValue( s31ClassListBean.getFloHeikin() );
								}
								// *セット
								if ( s31ClassListBean.getFloHensa() != -999.0 ) {
									if ( hensa != -999.0 ) {
										if ( s31ClassListBean.getFloHensa() < hensa ) {
											workCell = cm.setCell( workSheet, workRow, workCell, row, col+3 );
											workCell.setCellValue("*");
										}
									}
								}
								// 平均偏差値セット
								workCell = cm.setCell( workSheet, workRow, workCell, row++, col+4 );
								if ( s31ClassListBean.getFloHensa() != -999.0 ) {
									workCell.setCellValue( s31ClassListBean.getFloHensa() );
								}
							}
							classCnt++;
							
							if(classCnt%houjiNum==0){
								if(itrClass.hasNext()){
									if(kmkCnt==0){
										if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
											bolBookCngFlg = true;
										}
									}
									bolSheetCngFlg = true;
								}
							}
							if(itrClass.hasNext()==false){
								if(kmkCnt==0){
									bolSheetCngFlg = true;
								}
							}
//add 2004/10/26 T.Sakai データ0件対応
							dispClassFlgCnt++;
//add end
						}
					}
					kmk = s31ListBean.getStrKmkmei();
					col = col + 4;
					classCnt = 0;
					kmkCnt++;
					if ( kmkCnt==10 ) {
						bolSheetCngFlg = true;
						kmkCnt = 0;
						if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
							bolBookCngFlg = true;
						}
					}
	
					// workbookListにWorkBookが２つ以上格納されている時は出力処理を実行
					listSize = workbookList.size();
					if(listSize>1){
						if(kmkCnt == 0){
							workbook = (HSSFWorkbook)workbookList.get(0);
		
							// Excelファイル保存
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, intMaxSheetSr);
							fileIndex++;
							if( bolRet == false ){
								return errfwrite;
							}
	
							// ファイル出力したデータは削除
							workbookList.remove(0);
	
							// WorkBook・変数を書き込んでる途中のものに戻す
							workbook = (HSSFWorkbook)workbookList.get(0);
						}
					}
				}
			}
//add 2004/10/26 T.Sakai データ0件対応
			if ( dispClassFlgCnt==0 ) {
				//マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				intMaxSheetIndex = 0;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex+1));
				intMaxSheetIndex++;
				
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
						
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s31Item.getIntSecuFlg() ,38 ,40 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 38 );
				workCell.setCellValue(secuFlg);
						
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s31Item.getStrGakkomei()) );
	
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s31Item.getStrMshDate() );
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
				workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(s31Item.getStrMshmei()) + moshi);
			}
//add end
			
			// Excelファイル保存
			boolean bolRet = false;
			if(fileIndex != 1){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, intMaxSheetIndex);
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("S31_02","データセットエラー",e.toString());
			return errfdata;
		}
		
		return noerror;
	}

}
/*
 * 作成日: 2004/10/20
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;

import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.individual.ExamSorter;
import jp.co.fj.keinavi.util.individual.ExamSorter.SortByOutDataOpenDate;

import com.fjh.beans.DefaultSearchBean;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class PreExamSearchBean extends DefaultSearchBean{
	
	/** IN パラメター */
	private String examYear;
	private String examCd;
	private ExamSession examSession;
	private boolean isSameYear = false;
	private String personId;
	private String examTypeCd;
	private boolean isHelpflg = false;
	private String[] examTypeCds;
	private String[] excludeExamCds;
	
	private String query = ""+
	"SELECT DISTINCT"+
	" /*+ INDEX(SUB PK_SUBRECORD_I) */ "+//12/5 sql hint
	" E.EXAMYEAR "+",E.EXAMCD "+",E.EXAMNAME "+",E.EXAMNAME_ABBR "+
	",E.EXAMTYPECD "+",E.EXAMST "+",E.EXAMDIV "+",E.TERGETGRADE "+",E.INPLEDATE"+
	",E.IN_DATAOPENDATE "+",E.OUT_DATAOPENDATE "+",E.DOCKINGEXAMCD "+",E.DOCKINGTYPE "+",E.DISPSEQUENCE "+",E.MASTERDIV "+
	"FROM SUBRECORD_I SUB "+
	"INNER JOIN "+
	"EXAMINATION E "+ 
	"ON SUB.EXAMYEAR = E.EXAMYEAR "+ 
	"AND SUB.EXAMCD = E.EXAMCD "+
	"WHERE 1=1 ";

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		if(personId != null){
			query +=	"AND SUB.INDIVIDUALID = '"+personId+"' ";
		}
		if(isSameYear && examYear != null){
			query +="AND SUB.EXAMYEAR = '"+examYear+"' ";
		}
		if(examTypeCd != null){
			query += "AND E.EXAMTYPECD = '"+examTypeCd+"' ";
		}
		/*
		 * 12/15 add
		 */
		if(examTypeCds != null && examTypeCds.length > 0){
			query += "AND E.EXAMTYPECD IN (";
			for(int i=0; i<examTypeCds.length; i++){
				if(i != 0) query += ",";
				query += "'"+examTypeCds[i]+"'";
			}
			query += ")";
		}
		if(examYear != null && examCd != null && examTypeCd == null && isHelpflg){
			query += "AND E.IN_DATAOPENDATE < (SELECT IN_DATAOPENDATE FROM EXAMINATION"+" WHERE EXAMYEAR = '"+examYear+"' AND EXAMCD = '"+examCd+"') ";
		}
		if(examYear != null && examCd != null && examTypeCd != null  && isHelpflg){
			query += "AND E.IN_DATAOPENDATE < (SELECT IN_DATAOPENDATE FROM EXAMINATION"+" WHERE EXAMYEAR = '"+examYear+"' AND EXAMCD = '"+examCd+"' AND EXAMTYPECD = '"+examTypeCd+"') ";
		}
		if(examYear != null && examCd != null && examTypeCd == null && isHelpflg == false){
			query += "AND E.OUT_DATAOPENDATE < (SELECT OUT_DATAOPENDATE FROM EXAMINATION"+" WHERE EXAMYEAR = '"+examYear+"' AND EXAMCD = '"+examCd+"') ";
		}
		if(examYear != null && examCd != null && examTypeCd != null && isHelpflg == false){
			query += "AND E.OUT_DATAOPENDATE < (SELECT OUT_DATAOPENDATE FROM EXAMINATION"+" WHERE EXAMYEAR = '"+examYear+"' AND EXAMCD = '"+examCd+"' AND EXAMTYPECD = '"+examTypeCd+"') ";
		}
		if(examSession != null){
			query += examSession.getExamOptionQuery();
		}
		/*
		 * 12/15 add
		 */
		if(excludeExamCds != null && excludeExamCds.length > 0){
			query += "AND E.EXAMCD NOT IN (";
			for(int i=0; i<excludeExamCds.length; i++){
				if(i != 0) query += ",";
				query += "'"+excludeExamCds[i]+"'";
			}
			query += ")";
		}
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			
			//System.out.println("oldExamSql=" + query);
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();

			while(rs.next()){
				ExaminationData data = new ExaminationData();
				data.setExamYear(GeneralUtil.toBlank(rs.getString("EXAMYEAR")));
				data.setExamCd(GeneralUtil.toBlank(rs.getString("EXAMCD")));
				data.setExamName(GeneralUtil.toBlank(rs.getString("EXAMNAME")));
				data.setExamNameAbbr(GeneralUtil.toBlank(rs.getString("EXAMNAME_ABBR")));
				data.setExamTypeCd(GeneralUtil.toBlank(rs.getString("EXAMTYPECD")));
				data.setExamSt(GeneralUtil.toBlank(rs.getString("EXAMST")));
				data.setExamDiv(GeneralUtil.toBlank(rs.getString("EXAMDIV")));
				data.setTergetGrade(GeneralUtil.toBlank(rs.getString("TERGETGRADE")));
				data.setInpleDate(GeneralUtil.toBlank(rs.getString("INPLEDATE")));
				data.setInDataOpenDate(GeneralUtil.toBlank(rs.getString("IN_DATAOPENDATE")));
				data.setOutDataOpenData(GeneralUtil.toBlank(rs.getString("OUT_DATAOPENDATE")));
				data.setDockingExamCd(GeneralUtil.toBlank(rs.getString("DOCKINGEXAMCD")));
				data.setDockingType(GeneralUtil.toBlank(rs.getString("DOCKINGTYPE")));
				data.setDispSequence(GeneralUtil.toBlank(rs.getString("DISPSEQUENCE")));
				data.setMasterDiv(GeneralUtil.toBlank(rs.getString("MASTERDIV")));

				recordSet.add(data);	
			}
			Collections.sort(recordSet, new ExamSorter().new SortByOutDataOpenDate());
		}catch(SQLException sqle){
			System.out.println("エラー="+ sqle);
			throw sqle;	
		}catch(Exception e){
			System.out.println("エラー="+ e);
			e.printStackTrace();
			throw e;
		}finally{
		if(pstmt != null) pstmt.close();
		if(rs != null) rs.close();
		}	
	}

	/**
	 * @return
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * @return
	 */
	public ExamSession getExamSession() {
		return examSession;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @param string
	 */
	public void setExamCd(String string) {
		examCd = string;
	}

	/**
	 * @param session
	 */
	public void setExamSession(ExamSession session) {
		examSession = session;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @return
	 */
	public boolean isSameYear() {
		return isSameYear;
	}

	/**
	 * @param b
	 */
	public void setSameYear(boolean b) {
		isSameYear = b;
	}

	/**
	 * @return
	 */
	public String getPersonId() {
		return personId;
	}

	/**
	 * @param string
	 */
	public void setPersonId(String string) {
		personId = string;
	}

	/**
	 * @return
	 */
	public String getExamTypeCd() {
		return examTypeCd;
	}

	/**
	 * @param string
	 */
	public void setExamTypeCd(String string) {
		examTypeCd = string;
	}

	/**
	 * @return
	 */
	public boolean isHelpflg() {
		return isHelpflg;
	}

	/**
	 * @param b
	 */
	public void setHelpflg(boolean b) {
		isHelpflg = b;
	}

	/**
	 * @return
	 */
	public String[] getExamTypeCds() {
		return examTypeCds;
	}

	/**
	 * @return
	 */
	public String[] getExcludeExamCds() {
		return excludeExamCds;
	}

	/**
	 * @param strings
	 */
	public void setExamTypeCds(String[] strings) {
		examTypeCds = strings;
	}

	/**
	 * @param strings
	 */
	public void setExcludeExamCds(String[] strings) {
		excludeExamCds = strings;
	}

}

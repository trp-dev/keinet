package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.kawaijuku.judgement.data.UnivInfoBean;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.beans.individual.ExamSearchBean;
import jp.co.fj.keinavi.beans.individual.JudgeByUnivBean;
import jp.co.fj.keinavi.beans.individual.PlanUnivSearchBean2;
import jp.co.fj.keinavi.beans.individual.ScoreSearchBean;
import jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean;
import jp.co.fj.keinavi.beans.individual.judgement.JudgementListBean;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.PlanUnivData2;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I305Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.individual.PlanUniv2Sorter;

/**
 * JudgementBeanロジック用インターフェース
 *
 * 2004.08.03   Tomohisa YAMADA - Totec
 * 				新規作成
 *
 * 2005.02.23   Tomohisa YAMADA - Totec
 * 				[1]判定結果をDetailPageBeanに一本化する
 *
 * 2005.03.08   Tomohisa YAMADA - Totec
 * 				[2]最新模試取得の時に
 * 				「担当クラスの年度よりも若い」制限を加える
 *
 * 2005.04.07   Tomohisa YAMADA - Totec
 * 				[3]ScoreBeanの[35]に伴う修正
 *
 * 2005.08.02   Tomohisa YAMADA - Totec
 * 				[4]マーク模試を受けていないときの、
 * 				帳票エラー修正
 *
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 *              ・センター・リサーチ５段階評価対応
 *              ・判定処理の隠蔽
 *
 * <2010年度＃マーク対応（判定軽量化）>
 * 2010.10.25	Tomohisa YAMADA - Totec
 * 				公表用データを随時取得
 *
 *
 */
public class I305Servlet extends IndividualServlet {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 892095431444943332L;

    /**
     * @see jp.co.fj.keinavi.servlets.individual.IndividualServlet#execute(
     * 			javax.servlet.http.HttpServletRequest,
     * 			 javax.servlet.http.HttpServletResponse)
     */
    protected void execute(final HttpServletRequest request,
        final HttpServletResponse response)
        throws ServletException, IOException {

        super.execute(request, response);

        // フォームデータを取得
        final I305Form form = (I305Form) getActionForm(
                request, "jp.co.fj.keinavi.forms.individual.I305Form");

        // HTTPセッション
        final HttpSession session = request.getSession();
        // プロファイル情報
        final Profile profile = getProfile(request);
        // 個人共通
        final ICommonMap iCommonMap = getICommonMap(request);
        // 模試データ
        final ExamSession examSession = getExamSession(request);

        // セキュリティスタンプ
        final Short printStamp = (Short) profile.getItemMap(
                IProfileCategory.I_PERSONAL_NOTE).get(IProfileItem.PRINT_STAMP);
        // 印刷対象生徒
        final Short PrintStudent = (Short) profile.getItemMap(
                IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);
        // テキストフォーマット
        final Short textFormat = (Short) profile.getItemMap(
                IProfileCategory.I_PERSONAL_NOTE).get(IProfileItem.TEXT_OUT_FORMAT);

        // 転送:JSP
        if ("i305".equals(getForward(request))) {
            if(!"i305".equals(getBackward(request))
                    || !iCommonMap.getTargetPersonId().equals(form.getTargetPersonId())
                    || isChangedMode(request)){

                if ("i305".equals(getBackward(request))) {
                    /** 対象生徒が変わった時 */
                    //2.出力フォーマットをセット
                    if (form.getTextFormat() != null) {
                        if (form.getTextFormat().length == 2) {
                            form.setTextFormat(CollectionUtil.splitComma("1,2"));
                        } else {
                          if (form.getTextFormat()[0] != null
                                    && form.getTextFormat()[0].equals("1")) {
                            form.setTextFormat(CollectionUtil.splitComma("1,0"));
                          } else {
                            form.setTextFormat(CollectionUtil.splitComma("0,2"));
                          }
                        }
                    } else {
                      form.setTextFormat(CollectionUtil.splitComma("0,0"));
                    }
                } else {

                    if (iCommonMap.isBunsekiMode()) {
                        //個人成績分析共通-印刷対象生保存をセット
                        form.setPrintStudent(PrintStudent.toString());
                    }

                    //1.セキュリティスタンプをセット
                    form.setPrintStamp(printStamp.toString());
                    //2.出力フォーマットをセット
                    switch (textFormat.shortValue()) {
                        case 1:
                        form.setTextFormat(CollectionUtil.splitComma("1,0"));
                        break;

                        case 2:
                        form.setTextFormat(CollectionUtil.splitComma("0,2"));
                        break;

                        case 3:
                        form.setTextFormat(CollectionUtil.splitComma("1,2"));
                        break;

                        case 0:
                        form.setTextFormat(CollectionUtil.splitComma("0,0"));
                        break;
                    }
                }

            /** 2回目以降 */
            } else {
                // 2.出力フォーマットをセット
                if (form.getTextFormat() != null) {
                    if (form.getTextFormat().length == 2) {
                        form.setTextFormat(CollectionUtil.splitComma("1,2"));
                    } else {
                        if (form.getTextFormat()[0] != null
                                && form.getTextFormat()[0].equals("1")) {
                            form.setTextFormat(CollectionUtil.splitComma("1,0"));
                        } else {
                            form.setTextFormat(CollectionUtil.splitComma("0,2"));
                        }
                    }
                } else {
                    form.setTextFormat(CollectionUtil.splitComma("0,0"));
                }
            }

            Connection con = null;
            try {
                con = getConnectionPool(request);

                // ■ステップ１．模試成績の獲得

                //[3] add start
                //対象模試情報の取得
                ExamSearchBean esb = new ExamSearchBean();
                esb.setExamYear(iCommonMap.getTargetExamYear());
                esb.setExamCd(iCommonMap.getTargetExamCode());
                esb.setConnection("", con);
                esb.execute();
                //絶対に取れるはずだから、最初のを取得
                ExaminationData targetExam = (ExaminationData) esb.getRecordSet().get(0);
                //[3] add end

                //成績のリストを取得
                ScoreSearchBean ssb = new ScoreSearchBean();
                ssb.setLimitedYear(ProfileUtil.getChargeClassYear(profile));//[2] add
                ssb.setExamSession(examSession);
                ssb.setIndividualIds(new String[]{form.getTargetPersonId()});
                ssb.setConnection("", con);
                ssb.setTargetExam(targetExam);
                ssb.execute();
                Score sb = (Score)ssb.getRecordSet().get(0);

                // 出力一覧の取得
                PlanUnivSearchBean2 pusb2 = new PlanUnivSearchBean2();
                pusb2.setIndividualId(sb.getIndividualId());
                pusb2.setConnection("", con);
                pusb2.execute();

                // 判定の実行
                String[][] univArray = new String[pusb2.getRecordSet().size()][3];
                int count = 0;
                Iterator ite2 = pusb2.getRecordSet().iterator();
                while(ite2.hasNext()){
                    PlanUnivData2 data = (PlanUnivData2)ite2.next();
                    univArray[count][0] = data.getUnivCd();
                    univArray[count][1] = data.getFacultyCd();
                    univArray[count][2] = data.getDeptCd();
                    count ++;
                }

                JudgeByUnivBean jbub = new JudgeByUnivBean();
                jbub.setUnivAllList(super.getUnivAll());
                jbub.setUnivArray(univArray);
                jbub.setScoreBean(sb);
                jbub.execute();

                List judgedList = jbub.getRecordSet();
                JudgementListBean listBean = new JudgementListBean();
                listBean.createListBean(judgedList, sb);

                //「表示中の生徒一人」の結果一覧を作成してセッションに追加
                session.setAttribute("useListBean", listBean);
                request.setAttribute("markExamName", listBean.getCExamName());
                request.setAttribute("wrtnExamName", listBean.getSExamName());

                //受験予定大学一覧に判定結果と教科配点を付加する
                pusb2 = attachJudge(
                        pusb2,
                        listBean.getRecordSet(),
                        con);

                List resultList = pusb2.getRecordSet();
                if(resultList != null && resultList.size() > 0){
                    //ソート
                    Collections.sort(resultList, new PlanUniv2Sorter().new SortByExamPlanDateWCenter());
                }
                request.setAttribute("resultList", resultList);

            } catch (final Exception e) {
                throw createServletException(e);
            } finally {
                releaseConnectionPool(request, con);
            }

            request.setAttribute("form", form);

            iCommonMap.setTargetPersonId(form.getTargetPersonId());
            forward(request, response, JSP_I305);

        // 転送:SERVLET
        } else {
            // フォームの値をプロファイルに保存
            if (iCommonMap.isBunsekiMode()) {
                //個人成績分析共通-印刷対象生保存を保存
                profile.getItemMap(IProfileCategory.I_COMMON).put(
                        IProfileItem.PRINT_STUDENT,new Short(form.getPrintStudent()));
            }

            // セキュリティスタンプを保存
            profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(
                    IProfileItem.PRINT_STAMP,new Short(form.getPrintStamp()));

            // 出力フォーマットを保存
            if (form.getTextFormat() != null) {
                if (form.getTextFormat().length == 2) {
                    profile.getItemMap(	IProfileCategory.I_PERSONAL_NOTE).put(
                            IProfileItem.TEXT_OUT_FORMAT,	new Short("3"));
                } else {
                    if (form.getTextFormat()[0] != null
                            && form.getTextFormat()[0].equals("1")) {
                        profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(
                                IProfileItem.TEXT_OUT_FORMAT,new Short("1"));
                    } else {
                        profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(
                                IProfileItem.TEXT_OUT_FORMAT,new Short("2"));
                    }
                }
            } else {
                profile.getItemMap(IProfileCategory.I_PERSONAL_NOTE).put(
                        IProfileItem.TEXT_OUT_FORMAT,new Short("0"));
            }

            // 対象生徒を保持する
            iCommonMap.setTargetPersonId(form.getTargetPersonId());

            // 帳票印刷（判定を行う）
            if (getForward(request).equals("sheet")) {
                // 判定処理スレッド起動
                new Thread(new JudgeThread(form.getPrintStudent(),
                        getDbKey(request), session, iCommonMap,
                        examSession, getUnivAll(), profile)).start();
            }

            dispatch(request, response);
        }
    }

    // 判定処理スレッド
    private class JudgeThread extends BaseJudgeThread {

        private final ICommonMap iCommonMap;
        private final ExamSession examSession;
        private final List univAll;
        private final String[] selectedIndividualIds;
        private final Profile profile;

        // コンストラクタ
        private JudgeThread(final String judgeStudent,
                final String dbKey, final HttpSession pSession,
                final ICommonMap pICommonMap, final ExamSession pExamSession,
                final List pUnivAll, final Profile pProfile) {
            super(pSession, dbKey);
            selectedIndividualIds = getSelectedIndividualIds(pICommonMap, judgeStudent);
            iCommonMap = pICommonMap;
            examSession = pExamSession;
            univAll = pUnivAll;
            profile = pProfile;
        }

        /**
         * @see jp.co.fj.keinavi.servlets.individual.BaseJudgeThread#execute(
         * 			java.sql.Connection)
         */
        protected Map execute(final Connection con) throws Exception {

            // 判定結果マップ
            final Map judgedMap = new HashMap();

            //対象模試情報の取得
            ExamSearchBean esb = new ExamSearchBean();
            esb.setExamYear(iCommonMap.getTargetExamYear());
            esb.setExamCd(iCommonMap.getTargetExamCode());
            esb.setConnection("", con);
            esb.execute();
            //絶対に取れるはずだから、最初のを取得
            ExaminationData targetExam = (ExaminationData) esb.getRecordSet().get(0);

            //成績のリストを取得
            ScoreSearchBean ssb = new ScoreSearchBean();
            ssb.setLimitedYear(ProfileUtil.getChargeClassYear(profile));
            ssb.setExamSession(examSession);
            ssb.setIndividualIds(selectedIndividualIds);
            ssb.setConnection("", con);
            ssb.setTargetExam(targetExam);//[3] add
            ssb.execute();

            List scoreList = ssb.getRecordSet();
            for (int i = 0; i < scoreList.size(); i++) {
                Score sb = (Score) scoreList.get(i);

                // 出力一覧の取得
                PlanUnivSearchBean2 pusb2 = new PlanUnivSearchBean2();
                pusb2.setIndividualId(sb.getIndividualId());
                pusb2.setConnection("", con);
                pusb2.execute();

                // 判定の実行
                String[][] univArray = new String[pusb2.getRecordSet().size()][3];
                int count = 0;
                Iterator ite2 = pusb2.getRecordSet().iterator();
                while(ite2.hasNext()){
                    PlanUnivData2 data = (PlanUnivData2)ite2.next();
                    univArray[count][0] = data.getUnivCd();
                    univArray[count][1] = data.getFacultyCd();
                    univArray[count][2] = data.getDeptCd();
                    count ++;
                }

                /// 大学指定判定実行
                JudgementListBean listBean = new JudgementListBean();
                JudgeByUnivBean jbub = new JudgeByUnivBean();
                jbub.setUnivAllList(univAll);
                jbub.setUnivArray(univArray);
                jbub.setScoreBean(sb);
                jbub.execute();

                List judgedList = jbub.getRecordSet();
                listBean = new JudgementListBean();
                listBean.createListBean(judgedList, sb);
                judgedMap.put(sb.getIndividualId(), listBean);
            }

            return judgedMap;
        }
    }

    /**
     * 受験予定大学に判定結果情報と教科配点を付加する
     *
     */
    private PlanUnivSearchBean2 attachJudge(PlanUnivSearchBean2 psb2, List judges, Connection conn) throws Exception {

        //受験予定大学を一件ずつループして該当の判定結果とマッチさせる
        for (Iterator i = psb2.getRecordSet().iterator(); i.hasNext();) {

            PlanUnivData2 data = (PlanUnivData2) i.next();

            for (Iterator i2 = judges.iterator(); i2.hasNext();) {

                DetailPageBean detail = (DetailPageBean) i2.next();

                //処理中の受験予定大学と判定結果一覧の受験予定大学が一致
                if (data.getUnivCd().equals(detail.getBean().getUniv().getUnivCd())
                        && data.getFacultyCd().equals(detail.getBean().getUniv().getFacultyCd())
                        && data.getDeptCd().equals(detail.getBean().getUniv().getDeptCd())) {

                    //判定結果を受験予定大学のインスタンスに設定する
                    data.setCenScoreRate(detail.getCenScoreRate());			//センターボーダー得点率
                    data.setCenScoreRateStr(detail.getCenScoreRateStr());
                    data.setRankLLimits(detail.getRankHLimits());			//２次ランク
                    data.setRankLLimitsStr(detail.getRank() == JudgementConstants.Univ.RANK_FORCE
                            ? JudgementConstants.Univ.RANK_STR_NONE : detail.getRankLLimitsStr());
                    data.setCenterRating(detail.getCLetterJudgement());		//センター(合格可能性判定)
                    data.setSecondRating(detail.getSLetterJudgement());		//二次(合格可能性判定)
                    data.setTotalRating(detail.getEvalTotal().getLetter());		//総合(合格可能性判定)

                    //本画面に表示する配点は情報誌用科目の最初の枝番のものを使用する
                    //強制的に最初の枝番で配点を出す（仕様）
                    SortedMap map = (SortedMap) getUnivInfoMap().get(detail.getUniqueKey());

                    if (map != null) {
                        UnivInfoBean infoBean = (UnivInfoBean) map.values().iterator().next();
                        data.setCourseAllot1(KNUtil.splitInfoAllot(infoBean.getAllot1())[1]);
                        data.setCourseAllot2(KNUtil.splitInfoAllot(infoBean.getAllot2())[1]);
                    }
                }
            }
        }

        return psb2;
    }

}

package jp.co.fj.keinavi.beans.graph;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * クラス成績分析 - クラス成績概況 - 偏差値分布
 * 
 * 2006.07.27	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class C002GraphTargetBean extends BaseGraphCheckerBean {

	/**
	 * コンストラクタ
	 * 
	 * @param id プロファイルカテゴリID
	 */
	public C002GraphTargetBean(final String id) {
		super(id);
	}
	
	/**
	 * @see jp.co.fj.keinavi.beans.graph.AbstractGraphCheckerBean#hasSubjectGraphSheet(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	public boolean hasSubjectGraphSheet(HttpServletRequest request) {
		
		// 偏差値帯別度数分布グラフ
		if (isValidValue(request, GRAPH_DIST_NO_AXIS)) {
			return true;
		}
		
		// 偏差値帯別人数積み上げグラフ
		if (isValidRange(request, GRAPH_BUILDUP)) {
			return true;
		}
		
		return false;
	}

}

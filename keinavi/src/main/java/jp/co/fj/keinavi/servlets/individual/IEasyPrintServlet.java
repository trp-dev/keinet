/*
 * 作成日: 2004/06/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jp.co.fj.keinavi.forms.individual.IEasyPrintForm;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class IEasyPrintServlet extends IndividualServlet {
	
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		//:::::::::: フォームから全画面からのデータの取得 ::::::::
		IEasyPrintForm iEasyPrintForm = null;
		try {
			iEasyPrintForm = (IEasyPrintForm)super.getActionForm(request, "jp.co.fj.keinavi.forms.individual.IEasyPrintForm");
		} catch (Exception e) {
			throw new ServletException(e);
		}
		for(int i=0; i<request.getParameterValues("selection").length; i++){
			System.out.println("fss :" + request.getParameterValues("selection")[i]);
		}
		System.out.println(request.getParameter("meetingSheet"));
		System.out.println(iEasyPrintForm.getTargetPersonId());
		System.out.println(iEasyPrintForm.getMeetingSheet());
		System.out.println(iEasyPrintForm.getStyleSheetAnalize());
		System.out.println(iEasyPrintForm.getStyleSheetTransit());
		System.out.println(iEasyPrintForm.getStyleSheetScore());
		System.out.println(iEasyPrintForm.getScoreSheetCont());
		System.out.println(iEasyPrintForm.getScoreSheetExam());
		System.out.println(iEasyPrintForm.getExamMaterialList());
		/*
		//:::::::::: 既に存在する（すべきだ！）iCommonDataを   ::::::::
		//:::::::::: セッションから取得	:::::::::::::::::::::::::::::::	
		//:::::::::: フォームデータデータをICommonDataにセット ::::::::
		iCommonData = (ICommonData) session.getAttribute(ICommonData.SESSION_KEY);
		iCommonData.setPersonIds(i003Form.getRenban());
		iCommonData.setTargetExamYear(i003Form.getTargetYear());
		iCommonData.setTargetExamCode(i003Form.getTargetExam());
		iCommonData.setTargetExamName(i003Form.getTargetName());
		//:::::::::: プロファイルに選択中の生徒のPERSONIDを保存 :::::::
		if(iCommonData.isBunsekiMode()){
			super.setSelectedPersonIds(i003Form.getRenban());
		}else if(iCommonData.isMendanMode()){
			super.setSelectedPersonId(i003Form.getRenban()[0]);
		}else{
			//ERROR!!!
		}
		
		//:::::::::: 実際の処理 選択されてPERSONIDからStudentListを作成する::::::
		I003Bean i003Bean = new I003Bean();
		i003Bean.setCondition(getMySchoolCd(), getClassInCharge(), iCommonData.getPersonIds());
		i003Bean.execute();
		
		//:::::::::::: iCommonDataセッションにここから必要な :::::::
		//:::::::::::: データをセット　作成されたStudentList :::::::
		iCommonData.setSelectedStudentList(i003Bean.getSelectedStudentsList());
				
		request.setAttribute("i003Bean", i003Bean);
		forward(request, response, "/jsp/individual/i003.jsp");
		*/
	}
}

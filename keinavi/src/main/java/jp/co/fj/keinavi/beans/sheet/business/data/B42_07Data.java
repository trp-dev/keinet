package jp.co.fj.keinavi.beans.sheet.business.data;

import jp.co.fj.keinavi.beans.sheet.school.data.S42_15BundleListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15Data;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15ReachLevelListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15ScoreZoneListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S42_15SubjectListData;

/**
 *
 * B42_07データクラス
 * 
 * 2006.08.31	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class B42_07Data extends S42_15Data {
	
	/**
	 * @see jp.co.fj.keinavi.beans.sheet.school.data.S42_15Data#createSubjectListData()
	 */
	public S42_15SubjectListData createSubjectListData() {
		return new B42_07SubjectListData();
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.school.data.S42_15Data#createReachLevelListData()
	 */
	public S42_15ReachLevelListData createReachLevelListData() {
		return new B42_07ReachLevelListData();
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.school.data.S42_15Data#createBundleListData()
	 */
	public S42_15BundleListData createBundleListData() {
		return new B42_07BundleListData();
	}
	
	/**
	 * @see jp.co.fj.keinavi.beans.sheet.school.data.S42_15Data#createScoreZoneListData()
	 */
	public S42_15ScoreZoneListData createScoreZoneListData() {
		return new B42_07ScoreZoneListData();
	}
		
}

package jp.co.fj.keinavi.data.com_set.cm;

import java.io.Serializable;

/**
 * 
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 * 
 * @author kawai
 *
 */
public class ClassData implements Serializable, Comparable {
	
	private String year; // 年度
	private String className; // クラス名
	private int grade; // 学年
	private int examinees; // 受験人数
	private String key; // クラスを識別するキー

	/**
	 * コンストラクタ
	 */
	public ClassData() {
	}

	/**
	 * コンストラクタ
	 */
	public ClassData(String key) {
		this.key = key;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return key.equals((((ClassData)obj).getKey()));
	}

	/**
	 * @param o
	 * @return
	 */
	public int compareTo(Object o) {
		
		final ClassData t = (ClassData) o;
		
		if (this.getGradeSeq() > t.getGradeSeq()) {
			return 1;
		} else if (this.getGradeSeq() < t.getGradeSeq()) {
			return -1;
		} else {
			return this.getClassName().compareTo(t.getClassName());
		}
	}

	/**
	 * 学年のソートキーを返す
	 * 
	 * @return
	 */
	private int getGradeSeq() {
		
		switch (this.grade) {
			case 1: return 3;
			case 3: return 1;
			default: return this.grade;
		}
	}
	
	/**
	 * @return
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @return
	 */
	public int getExaminees() {
		return examinees;
	}

	/**
	 * @return
	 */
	public int getGrade() {
		return grade;
	}

	/**
	 * @param string
	 */
	public void setClassName(String string) {
		className = string;
	}

	/**
	 * @param i
	 */
	public void setExaminees(int i) {
		examinees = i;
	}

	/**
	 * @param i
	 */
	public void setGrade(int i) {
		grade = i;
	}

	/**
	 * @return
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param string
	 */
	public void setKey(String string) {
		key = string;
	}

	/**
	 * @return year を戻します。
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year 設定する year。
	 */
	public void setYear(String year) {
		this.year = year;
	}

}

/**
 * 校内成績分析− 校内成績（設問別成績） 共通テスト国語記述問題評価別人数
 * 	Excelファイル編集
 * 作成日: 2019/09/10
 * @author	QQ)Tanouchi
 *
 * 2019.09.10 QQ)Tanouchi - 大規模改修・新規対応
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S13Item;
import jp.co.fj.keinavi.excel.data.school.S13SetumonList;
import jp.co.fj.keinavi.excel.data.school.S13SougouList;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S13_05 {

	private int noerror = 0; // 正常終了
	private int errfwrite = 2; // ファイルwriteエラー
	private int errfdata = 3; // データ設定エラー

	private CM cm = new CM(); //共通関数用クラス インスタンス

	private String masterfile = "S13_05";

	/*
	 * 	Excel編集メインaas
	 * 		S13Item s13Item: データクラス
	 * 		String masterfile: マスタExcelファイル名（フルパス）
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int s13_05EditExcel(S13Item s13Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null, null, null);

		HSSFWorkbook workbook = null;
		HSSFSheet workSheet = null;

		HSSFRow workRow = null;
		HSSFCell workCell = null;

		//テンプレートの決定
		S13SetumonList data = new S13SetumonList();
		S13SougouList data2 = new S13SougouList();
		try {
			// データ取得
			ArrayList datalist = s13Item.getS13KokugoSetumon();
			Iterator itr = datalist.iterator();
			ArrayList datalist2 = s13Item.getS13KokugoSougou();
			Iterator itr2 = datalist2.iterator();

			log.Ep("S13_05", "基本データデータセット開始", "");

			workbook = roadMasterFile(intSaveFlg); // Excelマスタファイル読込

			workSheet = initWorkSheet(workbook); // ワークシート初期化
			// 基本情報セット

			// ヘッダ右側に帳票作成日時を表示する
			cm.setHeader(workbook, workSheet);
			// セキュリティスタンプセット
			String secuFlg = cm.setSecurity(workbook, workSheet, s13Item.getIntSecuFlg(), 12, 13);
			workCell = cm.setCell(workSheet, workRow, workCell, 0, 12);
			workCell.setCellValue(secuFlg);
			// 学校名セット
			workCell = cm.setCell(workSheet, workRow, workCell, 2, 0);
			workCell.setCellValue("学校名　：" + cm.toString(s13Item.getStrGakkomei()));
			// 模試月取得
			String moshi = cm.setTaisyouMoshi(s13Item.getStrMshDate());
			// 対象模試セット
			workCell = cm.setCell(workSheet, workRow, workCell, 3, 0);
			workCell.setCellValue(cm.getTargetExamLabel() + "：" + cm.toString(s13Item.getStrMshmei()) + moshi);


			log.Ep("S13_05", "マークシートデータセット開始", "");

			int row = 7;
			int col = 2;

			while (itr.hasNext()) {
				data = (S13SetumonList) itr.next();
				if(data.getQuestion_No().equals("01")){
				}else if(data.getQuestion_No().equals("02")) {
					row += 2;
				}else if(data.getQuestion_No().equals("03")){
					row += 4;
				}
				// データセット実行
				// 2019/11/28 QQ)Ooseto 国語記述設問変更対応 UPD START
				// 校内-a評価 人数セット(0)
				workCell = cm.setCell(workSheet, workRow, workCell, row, col);
				NinKan(workCell, data.getS_aHyoukaNin());
				// 校内-a評価 割合％セット(1)
				workCell = cm.setCell(workSheet, workRow, workCell, row, col + 1);
				WariKan(workCell, data.getS_aHyoukaWari());
                                // 校内-a*評価 人数セット
                                workCell = cm.setCell(workSheet, workRow, workCell, row, col + 2);
                                NinKan(workCell, data.getS_aAstHyoukaNin());
                                // 校内-a*評価 割合％セット
                                workCell = cm.setCell(workSheet, workRow, workCell, row, col + 3);
                                WariKan(workCell, data.getS_aAstHyoukaWari());
				// 校内-b評価 人数セット(2)
				//workCell = cm.setCell(workSheet, workRow, workCell, row, col + 2);
				workCell = cm.setCell(workSheet, workRow, workCell, row, col + 4);
                                NinKan(workCell, data.getS_bHyoukaNin());
				// 校内-b評価 割合％セット(3)
				//workCell = cm.setCell(workSheet, workRow, workCell, row, col + 3);
				workCell = cm.setCell(workSheet, workRow, workCell, row, col + 5);
				WariKan(workCell, data.getS_bHyoukaWari());
                                // 校内-b*評価 人数セット
                                workCell = cm.setCell(workSheet, workRow, workCell, row, col + 6);
                                NinKan(workCell, data.getS_bAstHyoukaNin());
                                // 校内-b*評価 割合％セット
                                workCell = cm.setCell(workSheet, workRow, workCell, row, col + 7);
                                WariKan(workCell, data.getS_bAstHyoukaWari());
				// 校内-c評価 人数セット(4)
				//workCell = cm.setCell(workSheet, workRow, workCell, row, col + 4);
				workCell = cm.setCell(workSheet, workRow, workCell, row, col + 8);
				NinKan(workCell, data.getS_cHyoukaNin());
				// 校内-c評価 割合％セット(5)
				//workCell = cm.setCell(workSheet, workRow, workCell, row, col + 5);
				workCell = cm.setCell(workSheet, workRow, workCell, row, col + 9);
				WariKan(workCell, data.getS_cHyoukaWari());
				// 校内-d評価 人数セット(6)
				//workCell = cm.setCell(workSheet, workRow, workCell, row, col + 6);
				//NinKan(workCell, data.getS_dHyoukaNin());
				// 校内-d評価 割合％セット(7)
				//workCell = cm.setCell(workSheet, workRow, workCell, row, col + 7);
				//WariKan(workCell, data.getS_dHyoukaWari());
				row++;
				// 全国-a評価 人数セット(8)
				workCell = cm.setCell(workSheet, workRow, workCell, row, col);
				NinKan(workCell, data.getA_aHyoukaNin());
				// 全国-a評価 割合％セット(9)
				workCell = cm.setCell(workSheet, workRow, workCell, row, col + 1);
				WariKan(workCell, data.getA_aHyoukaWari());
                                // 全国-a*評価 人数セット
                                workCell = cm.setCell(workSheet, workRow, workCell, row, col + 2);
                                NinKan(workCell, data.getA_aAstHyoukaNin());
                                // 全国-a*評価 割合％セット
                                workCell = cm.setCell(workSheet, workRow, workCell, row, col + 3);
                                WariKan(workCell, data.getA_aAstHyoukaWari());
				// 全国-b評価 人数セット(10)
				//workCell = cm.setCell(workSheet, workRow, workCell, row, col + 2);
				workCell = cm.setCell(workSheet, workRow, workCell, row, col + 4);
				NinKan(workCell, data.getA_bHyoukaNin());
				// 全国-b評価 割合％セット(11)
				//workCell = cm.setCell(workSheet, workRow, workCell, row, col + 3);
				workCell = cm.setCell(workSheet, workRow, workCell, row, col + 5);
				WariKan(workCell, data.getA_bHyoukaWari());
                                // 全国-b*評価 人数セット
                                workCell = cm.setCell(workSheet, workRow, workCell, row, col + 6);
                                NinKan(workCell, data.getA_bAstHyoukaNin());
                                // 全国-b*評価 割合％セット
                                workCell = cm.setCell(workSheet, workRow, workCell, row, col + 7);
                                WariKan(workCell, data.getA_bAstHyoukaWari());
				// 全国-c評価 人数セット(12)
				//workCell = cm.setCell(workSheet, workRow, workCell, row, col + 4);
				workCell = cm.setCell(workSheet, workRow, workCell, row, col + 8);
				NinKan(workCell, data.getA_cHyoukaNin());
				// 全国-c評価 割合％セット(13)
				//workCell = cm.setCell(workSheet, workRow, workCell, row, col + 5);
				workCell = cm.setCell(workSheet, workRow, workCell, row, col + 9);
				WariKan(workCell, data.getA_cHyoukaWari());
				// 全国-d評価 人数セット(14)
				//workCell = cm.setCell(workSheet, workRow, workCell, row, col + 6);
				//NinKan(workCell, data.getA_dHyoukaNin());
				// 全国-d評価 割合％セット(15)
				//workCell = cm.setCell(workSheet, workRow, workCell, row, col + 7);
				//WariKan(workCell, data.getA_dHyoukaWari());
				// 2019/11/28 QQ)Ooseto 国語記述設問変更対応 UPD END
				row = 7;
			}
			while (itr2.hasNext()) {
				data2 = (S13SougouList) itr2.next();

				// 校内-A評価 人数セット(16)
				workCell = cm.setCell(workSheet, workRow, workCell, 16, 2);
				NinKan(workCell, data2.getS_AHyoukaNin());
				// 校内-A評価 割合％セット(17)
				workCell = cm.setCell(workSheet, workRow, workCell, 16, 3);
				WariKan(workCell, data2.getS_AHyoukaWari());
				// 校内-B評価 人数セット(18)
				workCell = cm.setCell(workSheet, workRow, workCell, 16, 4);
				NinKan(workCell, data2.getS_BHyoukaNin());
				// 校内-B評価 割合％セット(19)
				workCell = cm.setCell(workSheet, workRow, workCell, 16, 5);
				WariKan(workCell, data2.getS_BHyoukaWari());
				// 校内-C評価 人数セット(20)
				workCell = cm.setCell(workSheet, workRow, workCell, 16, 6);
				NinKan(workCell, data2.getS_CHyoukaNin());
				// 校内-C評価 割合％セット(22)
				workCell = cm.setCell(workSheet, workRow, workCell, 16, 7);
				WariKan(workCell, data2.getS_CHyoukaWari());
				// 校内-D評価 人数セット(23)
				workCell = cm.setCell(workSheet, workRow, workCell, 16, 8);
				NinKan(workCell, data2.getS_DHyoukaNin());
				// 校内-D評価 割合％セット(24)
				workCell = cm.setCell(workSheet, workRow, workCell, 16, 9);
				WariKan(workCell, data2.getS_DHyoukaWari());
				// 校内-E評価 人数セット(25)
				workCell = cm.setCell(workSheet, workRow, workCell, 16, 10);
				NinKan(workCell, data2.getS_EHyoukaNin());
				// 校内-E評価 割合％セット(26)
				workCell = cm.setCell(workSheet, workRow, workCell, 16, 11);
				WariKan(workCell, data2.getS_EHyoukaWari());

				// 全国-A評価 人数セット(27)
				workCell = cm.setCell(workSheet, workRow, workCell, 17, 2);
				NinKan(workCell, data2.getA_AHyoukaNin());
				// 全国-A評価 割合％セット(29)
				workCell = cm.setCell(workSheet, workRow, workCell, 17, 3);
				WariKan(workCell, data2.getA_AHyoukaWari());
				// 全国-B評価 人数セット(30)
				workCell = cm.setCell(workSheet, workRow, workCell, 17, 4);
				NinKan(workCell, data2.getA_BHyoukaNin());
				// 全国-B評価 割合％セット(32)
				workCell = cm.setCell(workSheet, workRow, workCell, 17, 5);
				WariKan(workCell, data2.getA_BHyoukaWari());
				// 全国-C評価 人数セット(33)
				workCell = cm.setCell(workSheet, workRow, workCell, 17, 6);
				NinKan(workCell, data2.getA_CHyoukaNin());
				// 全国-C評価 割合％セット(35)
				workCell = cm.setCell(workSheet, workRow, workCell, 17, 7);
				WariKan(workCell, data2.getA_CHyoukaWari());
				// 全国-D評価 人数セット(36)
				workCell = cm.setCell(workSheet, workRow, workCell, 17, 8);
				NinKan(workCell, data2.getA_DHyoukaNin());
				// 全国-D評価 割合％セット(38)
				workCell = cm.setCell(workSheet, workRow, workCell, 17, 9);
				WariKan(workCell, data2.getA_DHyoukaWari());
				// 全国-E評価 人数セット(39)
				workCell = cm.setCell(workSheet, workRow, workCell, 17, 10);
				NinKan(workCell, data2.getA_EHyoukaNin());
				// 全国-E評価 割合％セット(41)
				workCell = cm.setCell(workSheet, workRow, workCell, 17, 11);
				WariKan(workCell, data2.getA_EHyoukaWari());
			}

			// 処理行をインクリメント

			log.Ep("S13_05", "マークシートデータセット終了", "");


			//2019/10/03 QQ)Oosaki アクティブシートを選択 ADD START
            workbook.getSheetAt(1).setSelected(true);
            //2019/10/03 QQ)Oosaki アクティブシートを選択 ADD END

			// Excelファイル保存
			boolean bolRet = saveExcelFile(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, 1);

			if (bolRet == false)
				return errfwrite;

		} catch (Exception e) {
			log.Err("S13_05", "データセットエラー", e.toString());
			return errfdata;
		}

		log.Ep("S13_05", "基本データデータセット終了", "");
		return noerror;
	}

	/**
	 * ファイル保存メソッド
	 * @param intFlg
	 * @param list
	 * @param book
	 * @param uID
	 * @param intBookCount
	 * @param mfile
	 * @param intSheetCount
	 * @return false:失敗　true:成功　
	 */
	private boolean saveExcelFile(int intFlg, ArrayList list,
			HSSFWorkbook book, String uID, int intBookCount,
			String mfile, int intSheetCount) {

		return cm.bolFileSave(intFlg, list, book, uID, intBookCount, mfile, intSheetCount);

	}

	private void setUpbaseData(HSSFWorkbook workbook, HSSFSheet workSheet, S13Item item, S13SetumonList data) {


	}

	private void NinKan(HSSFCell workCell, int Nin) {
		if (Nin != 0) {
			workCell.setCellValue(Nin);
		}
	}

	private void WariKan(HSSFCell workCell, double Wari) {
		if (Wari != 0.0) {
			workCell.setCellValue(Wari);
		}
	}

	/**
	 * Excelマスタファイルをロードする
	 * @param intFlg
	 * @return
	 */
	private HSSFWorkbook roadMasterFile(int intFlg) {

		HSSFWorkbook workbook = cm.getMasterWorkBook(masterfile, intFlg);
		return workbook;

	}

	/**
	 * シートのテンプレートをコピーする（初期化）
	 * @param workbook
	 * @return HSSFSheet
	 */
	private HSSFSheet initWorkSheet(HSSFWorkbook workbook) {

		return workbook.cloneSheet(0);

	}

}
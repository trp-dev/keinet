/*
 * 作成日: 2004/10/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.admin;

import java.sql.SQLException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.fjh.beans.DefaultBean;
import jp.co.fj.keinavi.servlets.admin.ActionMode;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class M302Bean extends DefaultBean {

	private Connection conn = null;
	private String actionMode = null;
	private String targetYear = null;
	private String targetGrade = null;

	private String groupCode = null;	// グループコード
	private String className = null;	// クラス名
	private String comment   = null; 	// コメント
	private String pacakgeCode= null;	// 一括コード
	private String[] classList = null; 	// クラスリスト

	private boolean judgeAll   = true;	// 判定

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
	    
		try {

			
			if (actionMode.equals(ActionMode.COMPOSI_UPDATE_MODE) ) {
				// 更新モードなら複合クラスのキー情報を読み込む
				judgeAll = SelectClassGroup(conn, groupCode);

				// 一度グループコードで該当クラスを削除する
				judgeAll = DereteClassGroup(conn, groupCode);
				if ( judgeAll) {
					judgeAll = DereteClass_Group(conn, groupCode);
				}
			} else {
				// 登録モードの場合はキー値を取得する
				// 年度・学年は複合クラス一覧画面から引継ぐ

				// 新規グループコードを取得
				groupCode = getNewClassGroupCode(conn);
				if (groupCode == null) {
					judgeAll = false;
				}
			}

			if (judgeAll) {
				// グループクラス名マスタ追加
				judgeAll = InsertClassGroup(conn);
				if ( judgeAll) {
					// グループクラス対応マスタ追加
					String[] selectClass = getClassList(); 
					for ( int i = 0; i < selectClass.length; i++) {
						judgeAll = InsertClass_Group(conn, selectClass[i]);
						if (!judgeAll){
							break;
						}
					}
				}
			}
			
		} catch (SQLException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		} finally{
			this.setJudgeAll(judgeAll);
		}

	}


	/**
	 * 
	 * クラスグループマスタを取得する
	 *   年度、一括コード、学年、クラスグループコード   
	 *   
	 * @param con       DBコネクション
	 * @param classGCD  クラスグループコード
	 * @return boolean	 データ取得有無(True OR False)
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public boolean SelectClassGroup(Connection con, String classGCD)
		throws SQLException {
		boolean judge = false;

		PreparedStatement stmt = null;
		ResultSet rs = null;
		String query =
			"SELECT      C_G.YEAR     "
			+ 	"       ,C_G.BUNDLECD "
			+ 	"       ,C_G.GRADE    "
			+ 	"       ,C_G.CLASSGCD "
			+ 	"  FROM  CLASSGROUP CGP, CLASS_GROUP C_G   "
			+ 	" WHERE  CGP.CLASSGCD = C_G.CLASSGCD "
			+ 	"   AND  CGP.CLASSGCD = ?  " ;
		try {
			stmt = con.prepareStatement(query);
			stmt.setString(1, classGCD);

			rs = stmt.executeQuery();
			// 最初のレコードを取得
			if (rs.next()) {
				setTargetYear(rs.getString(1));		// 年度
				setPacakgeCode(rs.getString(2));	// 一括コード
				setTargetGrade(rs.getString(3));	// 学年
				setGroupCode(rs.getString(4));		// クラスグループコード
				judge = true;
			}
		} catch (SQLException ex) {
			throw new SQLException("クラスグループマスタの取得に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return judge;
	}

	/**
	 * 
	 * クラスグループ名マスタを削除する
	 *   
	 * @param con       DBコネクション
	 * @param classGCD  クラスグループコード
	 * @return boolean	 データ削除有無(True OR False)
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public boolean DereteClassGroup(Connection con, String classGCD)
		throws SQLException {
		boolean judge = false;

		PreparedStatement stmt = null;
		ResultSet rs = null;
		String query =
			"DELETE  FROM  CLASSGROUP WHERE CLASSGCD = ? ";
		try {
			stmt = con.prepareStatement(query);
			stmt.setString(1, classGCD);
			rs = stmt.executeQuery();
			judge = true;
		} catch (SQLException ex) {
			throw new SQLException("クラスグループ名マスタの削除に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return judge;
	}

	/**
	 * 
	 * クラスグループ対応マスタを削除する
	 *   
	 * @param con       DBコネクション
	 * @param classGCD  クラスグループコード
	 * @return boolean	 データ削除有無(True OR False)
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public boolean DereteClass_Group(Connection con, String classGCD)
		throws SQLException {
		boolean judge = false;

		PreparedStatement stmt = null;
		ResultSet rs = null;
		String query =
			"DELETE  FROM  CLASS_GROUP WHERE CLASSGCD = ? ";
		try {
			stmt = con.prepareStatement(query);
			stmt.setString(1, classGCD);
			rs = stmt.executeQuery();
			judge = true;
		} catch (SQLException ex) {
			throw new SQLException("クラスグループマスタの削除に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return judge;
	}

	/**
	 * 
	 * クラスグループマスタを追加する
	 *   
	 * @param con       DBコネクション
	 * @param classGCD  クラスグループコード
	 * @return boolean	 データ追加結果(True OR False)
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public boolean InsertClassGroup(Connection con)
		throws SQLException {
		int count = 0;
		boolean judge = false;

		PreparedStatement stmt = null;
		ResultSet rs = null;
		String query =
			"INSERT INTO   CLASSGROUP "
				+ " (              "
				+ "   CLASSGCD     "
				+ "  ,CLASSGNAME   "
				+ "  ,COM          "
				+ " ) VALUES (     "
				+ "  ?, ?, ? )";
		try {
			stmt = con.prepareStatement(query);
			stmt.setString( 1, getGroupCode());
			stmt.setString( 2, getClassName());
			stmt.setString( 3, getComment());

			count = stmt.executeUpdate();

			if (count == 0) {
				throw new SQLException("クラスグループマスタのINSERTに失敗しました。");
			} else {
				judge = true;
			}

		} catch (SQLException ex) {
			throw new SQLException("クラスグループマスタのINSERTに失敗しました");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return judge;
	}

	/**
	 * 
	 * クラスグループマスタを追加する
	 *   
	 * @param con       DBコネクション
	 * @param classGCD  クラスグループコード
	 * @return boolean	 データ追加結果(True OR False)
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public boolean InsertClass_Group(Connection con, String clas)
		throws SQLException {
		int count = 0;
		boolean judge = false;

		PreparedStatement stmt = null;
		ResultSet rs = null;
		String query =
			"INSERT INTO   CLASS_GROUP "
				+ " (              "
				+ "   YEAR         "
				+ "  ,BUNDLECD     "
				+ "  ,GRADE        "
				+ "  ,CLASS        "
				+ "  ,CLASSGCD     "
				+ " ) VALUES (     "
				+ "  ?, ?, ?, ?, ?)";
		try {
			stmt = con.prepareStatement(query);
			stmt.setString( 1, getTargetYear());
			stmt.setString( 2, getPacakgeCode());
			stmt.setString( 3, getTargetGrade());
			stmt.setString( 4, clas );
			stmt.setString( 5, getGroupCode());

			count = stmt.executeUpdate();

			if (count == 0) {
				throw new SQLException("クラスグループ対応マスタのINSERTに失敗しました。");
			} else {
				judge = true;
			}

			judge = true;
		} catch (SQLException ex) {
			throw new SQLException("クラスグループ対応マスタのINSERTに失敗しました");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return judge;
	}

	/**
	 * 
	 * 新しいクラスグループコードを取得する
	 *   
	 * @param con       DBコネクション
	 * @return String	 取得したクラスグループコード
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public String getNewClassGroupCode(Connection con)
		throws SQLException {
		boolean judge = false;
		String	 classGCD = null;
		String	 seqno = null;

		StringBuffer query = new StringBuffer();
		query.append("SELECT TO_CHAR(ADD_MONTHS(TO_DATE(SYSDATE,'rrrr/mm/dd'),-3),'yy') || TRIM(TO_CHAR(classgcount.nextval,'00000'))  FROM DUAL");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query.toString());
			rs = ps.executeQuery();
			if (rs.next()) {
				classGCD = rs.getString(1);
			}
		} finally {
			rs.close();
			ps.close();
		}

		return classGCD;
	}

	/**
	 * @return
	 */
	public String getActionMode() {
		return actionMode;
	}

	/**
	 * @param string
	 */
	public void setActionMode(String string) {
		actionMode = string;
	}

	/**
	 * @return
	 */
	public String getTargetYear() {
		return targetYear;
	}

	/**
	 * @param string
	 */
	public void setTargetYear(String string) {
		targetYear = string;
	}

	/**
	 * @return
	 */
	public String getTargetGrade() {
		return targetGrade;
	}

	/**
	 * @param string
	 */
	public void setTargetGrade(String string) {
		targetGrade = string;
	}

	/**
	 * @return
	 */
	public String getGroupCode() {
		return groupCode;
	}

	/**
	 * @param string
	 */
	public void setGroupCode(String string) {
		groupCode = string;
	}

	/**
	 * @return
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param string
	 */
	public void setClassName(String string) {
		className = string;
	}

	/**
	 * @return
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param string
	 */
	public void setComment(String string) {
		comment = string;
	}

	/**
	 * @return
	 */
	public String getPacakgeCode() {
		return pacakgeCode;
	}

	/**
	 * @param string
	 */
	public void setPacakgeCode(String string) {
		pacakgeCode = string;
	}

	/**
	 * @return
	 */
	public boolean isJudgeAll() {
		return judgeAll;
	}

	/**
	 * @param b
	 */
	public void setJudgeAll(boolean b) {
		judgeAll = b;
	}

	/**
	 * @return
	 */
	public Connection getConn() {
		return conn;
	}

	/**
	 * @param connection
	 */
	public void setConn(Connection connection) {
		conn = connection;
	}

	/**
	 * @return
	 */
	public String[] getClassList() {
		return classList;
	}

	/**
	 * @param strings
	 */
	public void setClassList(String[] strings) {
		classList = strings;
	}

}

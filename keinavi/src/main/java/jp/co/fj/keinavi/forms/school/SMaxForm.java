package jp.co.fj.keinavi.forms.school;

import jp.co.fj.keinavi.forms.BaseForm;


/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class SMaxForm extends BaseForm {

	private String changeExam; // 模試に対する変更を行ったかどうか
	private String save; // 保存処理を行うかどうか

	// 型の選択で共通項目設定を利用するかどうか
	private String commonType = null;
	// 科目の選択で共通項目設定を利用するかどうか
	private String commonCourse = null;
	// 分析対象となる型コードの配列
	private String[] analyzeType = null;
	// グラフ表示をする型コードの配列
	private String[] graphType = null;
	// 分析対象となる科目コードの配列
	private String[] analyzeCourse = null;

	// グラフ表示する科目コードの配列
	private String[] graphCourse = null;
	// フォーマットの配列
	private String[] formatItem = null;
    	// 構成比表示(あり/なし)
	private String compRatio = null;
	// グラフ軸
	private String axis = null;
	// オプションの配列
	private String[] optionItem = null;

	// 偏差値ピッチ(偏差値帯別人数積み上げグラフ)
	private String pitchBuildup = null;
	// 偏差値ピッチ(偏差値別構成比グラフ)
	private String pitchCompRatio = null;
	// 2019/07/17 QQ)Tanioka 共通テスト対応 ADD START
	// 自校受験者がいる試験を対象にする(共通テスト英語認定試験CEFR取得状況)
	private String targetCheckBox = null;

	// 国語 評価別人数(記述式問題詳細分析)
	private String targetCheckKokugo = null;
	// 小設問別正答状況(記述式問題詳細分析)
	private String targetCheckStatus = null;
	// 小設問別成績(記述式問題詳細分析)
	private String targetCheckRecord = null;
	// 2019/07/17 QQ)Tanioka 共通テスト対応 ADD END

	// 過年度の表示
	private String dispPast = null;
	// 全体平均表示
	private String dispTotalAvg = null;
	// 現役全体平均表示
	private String dispActiveAvg = null;

	// 高卒全体平均表示
	private String dispGradAvg = null;
	// 大学の表示順序
	private String dispUnivOrder = null;
	// 大学集計区分
	private String dispUnivCount = null;
	// 高校の表示順序
	private String dispSchoolOrder = null;
	// 上位校の*印表示
	private String dispUpperMark = null;
	// 表示対象（志望大学）
	private String[] dispUnivStudent = null;
	// 過年度の表示
	private String[] dispUnivYear;
	// 日程
	private String dispUnivSchedule;
	// 評価区分
	private String[] dispUnivRating;

    /**
     * @return dispUnivStudent を戻す。
     */
    public String[] getDispUnivStudent() {
        return dispUnivStudent;
    }
    /**
     * @param dispUnivStudent dispUnivStudent を設定する。
     */
    public void setDispUnivStudent(String[] dispUnivStudent) {
        this.dispUnivStudent = dispUnivStudent;
    }
	// 表示高校数
	private String dispSchoolNum = null;
	// クラスの表示順序
	private String dispClassOrder = null;
	// セキュリティスタンプ
	private String stamp = null;

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}


	/**
	 * @return
	 */
	public String[] getAnalyzeCourse() {
		return analyzeCourse;
	}

	/**
	 * @return
	 */
	public String[] getAnalyzeType() {
		return analyzeType;
	}

	/**
	 * @return
	 */
	public String getAxis() {
		return axis;
	}

	/**
	 * @return
	 */
	public String getCommonCourse() {
		return commonCourse;
	}

	/**
	 * @return
	 */
	public String getCommonType() {
		return commonType;
	}

	/**
	 * @return
	 */
	public String getCompRatio() {
		return compRatio;
	}

	/**
	 * @return
	 */
	public String getDispActiveAvg() {
		return dispActiveAvg;
	}

	/**
	 * @return
	 */
	public String getDispClassOrder() {
		return dispClassOrder;
	}

	/**
	 * @return
	 */
	public String getDispGradAvg() {
		return dispGradAvg;
	}

	/**
	 * @return
	 */
	public String getDispPast() {
		return dispPast;
	}

	/**
	 * @return
	 */
	public String getDispSchoolNum() {
		return dispSchoolNum;
	}

	/**
	 * @return
	 */
	public String getDispSchoolOrder() {
		return dispSchoolOrder;
	}

	/**
	 * @return
	 */
	public String getDispTotalAvg() {
		return dispTotalAvg;
	}

	/**
	 * @return
	 */
	public String getDispUnivCount() {
		return dispUnivCount;
	}

	/**
	 * @return
	 */
	public String getDispUnivOrder() {
		return dispUnivOrder;
	}

	/**
	 * @return
	 */
	public String getDispUpperMark() {
		return dispUpperMark;
	}

	/**
	 * @return
	 */
	public String[] getFormatItem() {
		return formatItem;
	}

	/**
	 * @return
	 */
	public String[] getGraphCourse() {
		return graphCourse;
	}

	/**
	 * @return
	 */
	public String[] getGraphType() {
		return graphType;
	}

	/**
	 * @return
	 */
	public String[] getOptionItem() {
		return optionItem;
	}

	/**
	 * @return
	 */
	public String getStamp() {
		return stamp;
	}

	/**
	 * @param strings
	 */
	public void setAnalyzeCourse(String[] strings) {
		analyzeCourse = strings;
	}

	/**
	 * @param strings
	 */
	public void setAnalyzeType(String[] strings) {
		analyzeType = strings;
	}

	/**
	 * @param string
	 */
	public void setAxis(String string) {
		axis = string;
	}

	/**
	 * @param string
	 */
	public void setCommonCourse(String string) {
		commonCourse = string;
	}

	/**
	 * @param string
	 */
	public void setCommonType(String string) {
		commonType = string;
	}

	/**
	 * @param string
	 */
	public void setCompRatio(String string) {
		compRatio = string;
	}

	/**
	 * @param string
	 */
	public void setDispActiveAvg(String string) {
		dispActiveAvg = string;
	}

	/**
	 * @param string
	 */
	public void setDispClassOrder(String string) {
		dispClassOrder = string;
	}

	/**
	 * @param string
	 */
	public void setDispGradAvg(String string) {
		dispGradAvg = string;
	}

	/**
	 * @param string
	 */
	public void setDispPast(String string) {
		dispPast = string;
	}

	/**
	 * @param string
	 */
	public void setDispSchoolNum(String string) {
		dispSchoolNum = string;
	}

	/**
	 * @param string
	 */
	public void setDispSchoolOrder(String string) {
		dispSchoolOrder = string;
	}

	/**
	 * @param string
	 */
	public void setDispTotalAvg(String string) {
		dispTotalAvg = string;
	}

	/**
	 * @param string
	 */
	public void setDispUnivCount(String string) {
		dispUnivCount = string;
	}

	/**
	 * @param string
	 */
	public void setDispUnivOrder(String string) {
		dispUnivOrder = string;
	}

	/**
	 * @param string
	 */
	public void setDispUpperMark(String string) {
		dispUpperMark = string;
	}

	/**
	 * @param strings
	 */
	public void setFormatItem(String[] strings) {
		formatItem = strings;
	}

	/**
	 * @param strings
	 */
	public void setGraphCourse(String[] strings) {
		graphCourse = strings;
	}

	/**
	 * @param strings
	 */
	public void setGraphType(String[] strings) {
		graphType = strings;
	}

	/**
	 * @param strings
	 */
	public void setOptionItem(String[] strings) {
		optionItem = strings;
	}

	/**
	 * @param string
	 */
	public void setStamp(String string) {
		stamp = string;
	}

	/**
	 * @return
	 */
	public String getPitchCompRatio() {
		return pitchCompRatio;
	}

	/**
	 * @param string
	 */
	public void setPitchCompRatio(String string) {
		pitchCompRatio = string;
	}

	/**
	 * @return
	 */
	public String getPitchBuildup() {
		return pitchBuildup;
	}

	/**
	 * @param string
	 */
	public void setPitchBuildup(String string) {
		pitchBuildup = string;
	}

	// 2019/07/17 QQ)Tanioka 共通テスト対応 ADD START
	/**
	 * @return
	 */
	public String getTargetCheckBox() {
		return targetCheckBox;
	}

	/**
	 * @param string
	 */
	public void setTargetCheckBox(String string) {
		targetCheckBox = string;
	}

	/**
	 * @return
	 */
	public String getTargetCheckKokugo() {
		return targetCheckKokugo;
	}

	/**
	 * @param string
	 */
	public void setTargetCheckKokugo(String string) {
		targetCheckKokugo = string;
	}

	/**
	 * @return
	 */
	public String getTargetCheckStatus() {
		return targetCheckStatus;
	}

	/**
	 * @param string
	 */
	public void setTargetCheckStatus(String string) {
		targetCheckStatus = string;
	}

	/**
	 * @return
	 */
	public String getTargetCheckRecord() {
		return targetCheckRecord;
	}

	/**
	 * @param string
	 */
	public void setTargetCheckRecord(String string) {
		targetCheckRecord = string;
	}
	// 2019/07/17 QQ)Tanioka 共通テスト対応 ADD END

	/**
	 * @return
	 */
	public String getChangeExam() {
		return changeExam;
	}

	/**
	 * @param string
	 */
	public void setChangeExam(String string) {
		changeExam = string;
	}

	/**
	 * @return
	 */
	public String getSave() {
		return save;
	}

	/**
	 * @param string
	 */
	public void setSave(String string) {
		save = string;
	}

	public String[] getDispUnivYear() {
		return this.dispUnivYear;
	}
	public void setDispUnivYear(String[] dispUnivYear) {
		this.dispUnivYear = dispUnivYear;
	}
	public String[] getDispUnivRating() {
		return this.dispUnivRating;
	}
	public void setDispUnivRating(String[] dispUnivRating) {
		this.dispUnivRating = dispUnivRating;
	}
	public String getDispUnivSchedule() {
		return this.dispUnivSchedule;
	}
	public void setDispUnivSchedule(String dispUnivSchedule) {
		this.dispUnivSchedule = dispUnivSchedule;
	}
}

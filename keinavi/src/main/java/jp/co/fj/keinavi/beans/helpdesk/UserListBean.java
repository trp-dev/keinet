/*
 * 作成日: 2004/09/23
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.helpdesk;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.fjh.beans.DefaultBean;
import jp.co.fj.keinavi.forms.helpdesk.HD101Form;
import jp.co.fj.keinavi.data.helpdesk.UserList;

/**
 * 2005.02.18 Yoshimoto KAWAI - Totec
 *            学校コードでの前方一致検索がおかしかったのをFIX
 * 
 * @author nino
 */
public class UserListBean extends DefaultBean {

	static String[] userDivName = {"","公立","私学","予備校","","教育委員会","ヘルプデスク","","",""};
	static String[] pactDivName = {"","契約校","非契約校","リサーチ参加校","非受験校","ヘルプデスク","","","",""};

	HD101Form 	hd101Form = new HD101Form();	// HD101Form 情報
	// ヘルプリスト
	private List userList = new ArrayList();


	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// TODO 自動生成されたメソッド・スタブ

		// HD101Form 情報取得
		HD101Form hd101Form = getHD101Form();

		
		// SQLをセットアップ
		StringBuffer query = new StringBuffer();
		query.append("SELECT  PAC.SCHOOLCD    ");		// 学校コード
		query.append("       ,SCH.BUNDLENAME  ");		// 学校名
		query.append("       ,PAC.USERID      ");		// ユーザＩＤ
		query.append("       ,PAC.CERT_USERDIV");		// ユーザ区分
		query.append("       ,PAC.PACTDIV     ");		// 契約区分
		query.append("       ,PAC.RENEWALDATE ");		// 更新日
		query.append("       ,PAC.PACTTERM    ");		// 契約期限
		query.append("       ,PAC.CERT_BEFOREDATA  ");	// 証明書有効期限
		query.append("       ,PAC.ISSUED_BEFOREDATE");	// 発行済証明書有効期限
		query.append("  FROM  PACTSCHOOL PAC, SCHOOL SCH     ");
		query.append(" WHERE  PAC.SCHOOLCD = SCH.BUNDLECD(+) ");

		// 契約区分
		//   1:契約校  2:非契約校  3:リサーチ参加校  4:非受験校  5:ヘルプデスク
		if ( hd101Form.getPactDiv() != null && hd101Form.getPactDiv().length > 0 ) {
			query.append(" AND ( " );
			for ( int i = 0 ; i < hd101Form.getPactDiv().length; i++) {
				if ( i > 0 ) {
					query.append(" OR " );
				}
				query.append(" PAC.PACTDIV = '" + hd101Form.getPactDiv()[i] + "' " );
			}
			query.append(" ) " );
		} else {
			// 契約区分が選択されていない場合は通常引き当てられない契約区分をセット
			//query.append(" AND  PAC.PACTDIV = ' ' " );
		}
		
		// ユーザ区分
		//   1:公立  2:私立  3:予備校  5:教育委員会  6:ヘルプデスク
		// 　契約区分のヘルプデスクにチェックが入っているかを調べる
		boolean helpDeskCheck = false;
		if ( hd101Form.getPactDiv() != null ) {
			if ( hd101Form.getPactDiv().length > 0 ){
				for ( int i = 0 ; i < hd101Form.getPactDiv().length; i++) {
					// 契約区分→5:ヘルプデスク
					if ( hd101Form.getPactDiv()[i].equals("5") ) {
						helpDeskCheck = true;
					}
				}
			}
		}
		if ( helpDeskCheck || (hd101Form.getUserDiv() != null && hd101Form.getUserDiv().length > 0) ) {
			query.append(" AND ( " );
			int userCount = 0;
			if ( hd101Form.getUserDiv() != null ) {
				for ( int i = 0 ; i < hd101Form.getUserDiv().length; i++) {
					userCount = i + 1;
					if ( i > 0 ) {
						query.append(" OR " );
					}
					query.append(" PAC.CERT_USERDIV = '" + hd101Form.getUserDiv()[i] + "' " );
					
				}
			}
			// ヘルプデスクチェックがあるか
			// ユーザ区分→6:ヘルプデスク
			if(helpDeskCheck) {
				if (userCount > 0){
					query.append(" OR " );
				}
				query.append(" PAC.CERT_USERDIV = '6' " );
			}
			query.append(" ) " );
		} else {
			// ユーザ区分が選択されていない場合は通常引き当てられないユーザ区分をセット
			//query.append(" AND  PAC.CERT_USERDIV = ' '  " );
		}

		// 学校コード
		if ( hd101Form.getSchoolCode() != null && hd101Form.getSchoolCode().length() > 0 ) {
			//query.append(" AND  PAC.SCHOOLCD >=  '" + hd101Form.getSchoolCode().trim() + "' " );
			query.append(" AND  PAC.SCHOOLCD LIKE '" + hd101Form.getSchoolCode().trim() + "%' " );
		}
		
		// 発行証明書日付を取得		
		// 有効
		String certEffeDate = getYyyyMmDd( hd101Form.getCertEffeYear()
										  ,hd101Form.getCertEffeMonth()
										  ,hd101Form.getCertEffeDay() );
		// 期限切れ
		String certOverDate = getYyyyMmDd( hd101Form.getCertOverYear()
										  ,hd101Form.getCertOverMonth()
										  ,hd101Form.getCertOverDay() );
		// 期限指定
		String certSpecDate = getYyyyMmDd( hd101Form.getCertSpecYear()
										  ,hd101Form.getCertSpecMonth()
										  ,hd101Form.getCertSpecDay() );

		// 発行済証明書
		if ( "0".equals(hd101Form.getJudge()) ) {			// すべて
		} else	if ( "1".equals(hd101Form.getJudge()) ) {	// 未発行
			query.append(" AND ( TRIM(PAC.ISSUED_BEFOREDATE) =  '' OR   (PAC.ISSUED_BEFOREDATE IS NULL) ) " );
			
		} else	if ( "2".equals(hd101Form.getJudge()) ) {	// 有効
			query.append(" AND  CERT_BEFOREDATA >=  '" + certEffeDate + "' " );

		} else	if ( "3".equals(hd101Form.getJudge()) ) {	// 期限切れ
			query.append(" AND  CERT_BEFOREDATA <  '" + certOverDate + "' " );

		} else	if ( "4".equals(hd101Form.getJudge()) ) {	// 期限指定
			query.append(" AND  CERT_BEFOREDATA =  '" + certSpecDate + "' " );

		}			
		
		// 学校コード昇順		
		query.append(" ORDER  BY PAC.SCHOOLCD ASC " );

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query.toString());

			rs = ps.executeQuery();
			
			String flag = null;
			while (rs.next()) {
				UserList list = new UserList();
				list.setSchoolCode(rs.getString(1));
				list.setSchoolName(rs.getString(2));
				list.setUserId(rs.getString(3));
				list.setUserDivNm( getUserDivName(rs.getString(4)) );
				list.setPactDivNm( getPactDivName(rs.getString(5)) );
				list.setRenewalDate(rs.getString(6));
				list.setPactTerm(rs.getString(7));
				list.setCert_BeforeDate(rs.getString(8));
				list.setIssued_BeforeDate(rs.getString(9));
				
				// 背景色変更のための期限チェック
				// ＜＜＜発行済証明書の選択があるもののみチェックする＞＞＞
				flag = "0";
				if (list.getIssued_BeforeDate() == null || list.getIssued_BeforeDate().equals("") ) {
					flag = "1";			// 証明書未発行
				}
				if ( "0".equals(hd101Form.getJudge()) || "1".equals(hd101Form.getJudge()) ) {
					if (list.getIssued_BeforeDate() == null || list.getIssued_BeforeDate().equals("") ) {
						flag = "1";			// 証明書未発行
					}
				} else if ( "3".equals(hd101Form.getJudge()) ) {
					// 発行済証明書有効期限＜入力の期限切れ年月日
					if ( Integer.parseInt(list.getCert_BeforeDate()) < Integer.parseInt( certOverDate ) ) {
						flag = "2";			// 期限切れ
					}
				} else if ( "4".equals(hd101Form.getJudge()) ) {
					// 発行済証明書有効期限!=入力の期限指定年月日
					if ( !list.getCert_BeforeDate().equals( certSpecDate )  ) {
						flag = "3";			// 有効期限不一致
					}
				}
				list.setChangeBackColor(flag);

				// 日付けの編集
				list.setRenewalDate( getFmtYmd(list.getRenewalDate()) );
				list.setPactTerm( getFmtYmd(list.getPactTerm()) );
				list.setCert_BeforeDate( getFmtYmd(list.getCert_BeforeDate()) );
				list.setIssued_BeforeDate( getFmtYmd(list.getIssued_BeforeDate()) );
				
				
				userList.add(list);
			}
		} finally {
			rs.close();
			ps.close();
		}

	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.com_set.AbstractComBean#getFullList()
	 */
	public List getFullList() {
		return userList;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.com_set.AbstractComBean#getFullListSize()
	 */
	public int getFullListSize() {
		return userList.size();
	}


	/**
	* ユーザ区分名取得
	* @return
	*/
	public String getUserDivName(String str) {
		String name = "";
		int no = Integer.parseInt(str);
		if ( 0 <= no && no <= 9) {
			name = userDivName[no];
		}
		return name;
	}

	/**
	* 契約区分名取得
	* @return
	*/
	public String getPactDivName(String str) {
		String name = "";
		int no = Integer.parseInt(str);
		if ( 0 <= no && no <= 9) {
			name = pactDivName[no];
		}
		return name;
	}

	/**
	 * 年、月、日を連結して８桁の年月日を取得
	 * 
	 * 	 * @return 連結した文字列日付("yyyymmdd")
	 */
	public String getYyyyMmDd(String y, String m, String d) {
		String yyyymmdd = "";
		String dt	= "";
		
		if (y == null ) { y = ""; }
		if (m == null ) { m = ""; }
		if (d == null ) { d = ""; }
		dt =  "0000" + y;
		dt =  dt.substring(dt.length() - 4);
		yyyymmdd += dt;
		dt =  "00" + m;
		dt =  dt.substring(dt.length() - 2);
		yyyymmdd += dt;
		dt =  "00" + d;
		dt =  dt.substring(dt.length() - 2);
		yyyymmdd += dt;
		return yyyymmdd;
	}

	/**
	 * ８桁文字列を"yyyy/mm/dd"に変換
	 * 
	 * 	 * @return 連結した文字列日付("yyyymmdd")
	 */
	public String getFmtYmd(String yyyymmdd) {
		if ( yyyymmdd == null || yyyymmdd.equals("") || yyyymmdd.length() != 8 ) {
			return yyyymmdd;
		} else {
			return yyyymmdd.substring(0,4) + "/" + yyyymmdd.substring(4,6) + "/" + yyyymmdd.substring(6,8);
		}
	}

	// HD101Form 情報
	/**
	 * @param HD101Form
	 */
	public void setHD101Form(HD101Form form) {
		hd101Form = form;
	}
	
	/**
	 * @return HD101Form
	 */
	public HD101Form getHD101Form() {
		return hd101Form;
	}


}

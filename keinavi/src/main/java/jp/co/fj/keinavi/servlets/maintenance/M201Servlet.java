package jp.co.fj.keinavi.servlets.maintenance;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.beans.admin.LastDateBean;
import jp.co.fj.keinavi.beans.admin.MExamDataBean;
import jp.co.fj.keinavi.beans.maintenance.ExamIntegrationBean;
import jp.co.fj.keinavi.beans.maintenance.InitM201FormBean;
import jp.co.fj.keinavi.beans.maintenance.KNClassListBean;
import jp.co.fj.keinavi.beans.maintenance.SelectedStudentBean;
import jp.co.fj.keinavi.beans.recount.RecountLockBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.maintenance.M201Form;
import jp.co.fj.keinavi.util.KNUtil;

import org.apache.commons.lang.StringUtils;

/**
 *
 * 受験届修正画面サーブレット
 * 
 * 2006.10.05	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M201Servlet extends AbstractMServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// HTTPセッション
		final HttpSession session = request.getSession(false);
		// アクションフォーム
		final M201Form form = (M201Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.maintenance.M201Form");

		// JSPへの遷移処理
		if ("m201".equals(getForward(request))) {
			
			// 個人成績が存在する模試リストのセットアップ
			if (getScoreExamListBean(request) == null) {
				session.setAttribute("ScoreExamListBean",
						createScoreExamListBean(request));
			}
			// 選択済み生徒情報の初期化
			if (getSelectedStudentBean(request) == null) {
				session.setAttribute("SelectedStudentBean",
						new SelectedStudentBean(getSchoolCd(request)));
			}
			
			// 初期値設定があるならフォーム初期化
			if (!StringUtils.isEmpty(form.getInitIndividualId())
					&& !StringUtils.isEmpty(form.getInitExamYear())
					&& !StringUtils.isEmpty(form.getInitExamCd())) {
				initForm(request, form);
			}
			
			// 結合処理
			if ("1".equals(form.getActionMode())
					&& "m201".equals(getBackward(request))) {
				integrate(request, form);
				updateDeclaration(request);
			}
			
			// 作業中宣言のロード
			loadDeclaration(request);
			
			// クラスリスト
			request.setAttribute("ClassListBean",
					createClassListBean(request));
			// 模試リスト
			request.setAttribute("ExamListBean",
					createExamListBean(request));
			// 現年度
			request.setAttribute("CurrentYear",
					KNUtil.getCurrentYear());
			// フォーム
			request.setAttribute("form", form);
			
			forward2Jsp(request, response);
			
		// 不明なら転送
		} else {
			
			// 編集画面への遷移
			if ("m105".equals(getForward(request))) {
				// 選択済み生徒情報を保持
				final SelectedStudentBean bean = getSelectedStudentBean(request);
				bean.setMainIndividualId(form.getEditIndividualId());
				bean.setSubIndividualId(null);
				// 作業中宣言を更新
				updateDeclaration(request);
			}
			
			dispatch(request, response);
		}
	}

	// 結合処理
	private void integrate(final HttpServletRequest request,
			final M201Form form) throws ServletException {
		
		// 学校コード
		final String schoolCd = getSchoolCd(request);
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);
			
			// 再集計ロック失敗
			if (!RecountLockBean.lock(con, schoolCd)) {
				con.rollback();
				request.setAttribute("ErrorMessage",
						RecountLockBean.getErrorMessage());
			} else {
				// データ更新日
				LastDateBean.setLastDate2(con, schoolCd);
				
				// 対象解答用紙ID
				//（模試年度＋模試コード＋解答用紙番号）
				final String[] id = StringUtils.split(
						form.getTargetSheetId(), ',');

				// 結合
				final ExamIntegrationBean bean = new ExamIntegrationBean(schoolCd);
				bean.setConnection(null, con);
				bean.setExamYear(id[0]);
				bean.setExamCd(id[1]);
				bean.setAnswerSheetNo(id[2]);
				bean.setAfterIndividualId(form.getTargetStudentId());
				bean.execute();
				
				// 結合により生徒が非表示になる場合があるので
				// 生徒情報は初期化する
				initStudentSession(request);
				
				// ログ
				actionLog(request, "901");
				
				con.commit();
			}
			
		// エラーメッセージを保持
		} catch (final KNBeanException e) {
			rollback(con);
			request.setAttribute("ErrorMessage", e.getMessage());
		// 想定外のエラー
		} catch (final Exception e) {
			rollback(con);
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
	}

	// フォーム値の初期化処理
	private void initForm(final HttpServletRequest request,
			final M201Form form) throws ServletException {

		final InitM201FormBean bean = new InitM201FormBean();
		bean.setForm(form);
		execute(request, bean);
	}

	// クラスリストを作る
	private KNClassListBean createClassListBean(
			final HttpServletRequest request) throws ServletException {
		
		final KNClassListBean bean = new KNClassListBean(
				getSchoolCd(request));
		execute(request, bean);
		return bean;
	}

	// 模試リストを作る
	private MExamDataBean createExamListBean(
			final HttpServletRequest request) throws ServletException {
		
		final LoginSession login = getLoginSession(request);
		final MExamDataBean bean = new MExamDataBean();
		bean.setSchoolCd(login.getUserID());
		bean.setHelpDesk(login.isHelpDesk());
		execute(request, bean);
		return bean;
	}

}

package jp.co.fj.keinavi.beans.txt_out;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Connection;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.beans.security.IMenuDefine;
import jp.co.fj.keinavi.beans.security.MenuSecurityBean;
import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import java.io.File;
import java.sql.ResultSet;

/**
 *
 * テキスト出力：集計データ：設問別成績
 * 
 * 2004.10.18	[新規作成]
 * 
 *
 * @author 二宮淳行 - TOTEC
 * @version 1.0
 * 
 */
public class OutPutSetsumonSeiseki extends AbstractSheetBean{

	// 対象年度
	private String targetYear = null;
	// 対象模試
	private String targetExam = null;
	// 学校コード
	private String schoolCd = null;
	// ヘッダー情報
	private List header = null;
	
	// 他校コード
	private String OtherSchools = null;
	// 他校比較
	private int schoolDefFlg = 0;
	// クラス比較
	private int classDefFlg = 0;
	// 出力テキスト種別
	private int outType = 0;
	// 出力対象項目
	private Integer target[] = null;

	// 出力ファイル
	private File outPutFile = null;
	// コネクション
	private Connection con;
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return true;
	}
	
	public void execute()throws IOException, SQLException, Exception{
		
		OutPutParamSet set = new OutPutParamSet(login, exam, profile, "t004", sessionKey);
		
		final MenuSecurityBean menuSecurity = getMenuSecurityBean();
		schoolDefFlg = menuSecurity.isValid(IMenuDefine.MENU_TEXT_OTHER) ? set.getSchoolDefFlg() : 0;
		classDefFlg = menuSecurity.isValid(IMenuDefine.MENU_TEXT_CLASS) ? set.getClassDefFlg() : 0;
		
		targetYear = set.getTargetYear();
		targetExam = set.getTargetExam();
		schoolCd = profile.getBundleCD();
		header = set.getHeader();
		OtherSchools = set.getOtherSchools();
		outType =set.getOutType();
		target = set.getTarget();
		outPutFile =set.getOutPutFile();
		con = super.conn;
	
		OutputTextBean out = new OutputTextBean();
		out.setOutFile(outPutFile);	
		out.setOutputTextList(getSetsumonSeiseki());
		out.setHeadTextList(header);
		out.setOutTarget(target);
		out.setOutType(outType);
		out.execute();
		
		this.outfileList.add(outPutFile.getPath());
		sheetLog.add(outPutFile.getName());
	}
	

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.T_COUNTING_QUE;
	}
	
	
	/**
	 * 設問別成績を取得
	 * 
	 * @return list 設問別成績のレコードオブジェクト
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public LinkedList getSetsumonSeiseki() throws SQLException {
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		LinkedList list = new LinkedList();

		String query = ""
		+" SELECT 0 DEFFLG, q.EXAMYEAR EXAMYEAR, q.EXAMCD EXAMCD, em.EXAMNAME,"
		// 全国の場合、都道府県名：'全国'  学校名：'(全国)' とする
		// +"        1 UNIT,'99' PREFCD, p.PREFNAME,"
		// +"        '99999' BUNDLECD, TO_CHAR('('|| p.PREFNAME ||')'), 99 GRADE, '99' CLASS,"
		+"        1 UNIT,'99' PREFCD, '全国',"
		+"        '99999' BUNDLECD, TO_CHAR('('|| '全国' ||')'), 99 GRADE, '99' CLASS,"
		+"        es.SUBCD SUBCD, es.SUBNAME,"
		+"        eq.QUESTION_NO QUESTION_NO, eq.QUESTIONNAME1, eq.QALLOTPNT,"
		+"        q.NUMBERS, q.AVGPNT, q.AVGSCORERATE, eq.DISPLAY_NO DISPLAY_NO"
		+" FROM   QRECORD_A q, QRECORD_P qp,"
		+"        EXAMINATION em, V_SM_QEXAMSUBJECT es, EXAMQUESTION eq,PREFECTURE p"
		+" WHERE  q.EXAMYEAR   =?"
		+"   AND  q.EXAMCD     =?"
		+"   AND  q.EXAMYEAR   =em.EXAMYEAR"
		+"   AND  q.EXAMCD     =em.EXAMCD"
		+"   AND  q.EXAMYEAR   =es.EXAMYEAR"
		+"   AND  q.EXAMCD     =es.EXAMCD"
		+"   AND  q.SUBCD      =es.SUBCD"
		+"   AND  q.EXAMYEAR   =eq.EXAMYEAR"
		+"   AND  q.EXAMCD     =eq.EXAMCD"
		+"   AND  q.SUBCD      =eq.SUBCD"
		+"   AND  q.QUESTION_NO=eq.QUESTION_NO"
		+"   AND  q.EXAMYEAR   =qp.EXAMYEAR"
		+"   AND  q.EXAMCD     =qp.EXAMCD"
		+"   AND  q.SUBCD      =qp.SUBCD"
		+"   AND  q.QUESTION_NO=qp.QUESTION_NO"
		+"   AND  qp.PREFCD    = p.PREFCD"
		+"   AND  qp.PREFCD IN (SELECT FACULTYCD FROM SCHOOL sc WHERE BUNDLECD = ?)"
		+" UNION ALL"
		+" SELECT 0 DEFFLG, q.EXAMYEAR EXAMYEAR, q.EXAMCD  EXAMCD,em.EXAMNAME,"
		+"        2 UNIT, q.PREFCD PREFCD, p.PREFNAME,"
		+"        q.PREFCD || '999' BUNDLECD,TO_CHAR('('|| p.PREFNAME ||')'), 99 GRADE,'99' CLASS,"
		+"        es.SUBCD SUBCD, es.SUBNAME,"
		+"        eq.QUESTION_NO QUESTION_NO, eq.QUESTIONNAME1, eq.QALLOTPNT,"
		+"        q.NUMBERS, q.AVGPNT, q.AVGSCORERATE, eq.DISPLAY_NO DISPLAY_NO"
		+" FROM   QRECORD_P q,"
		+"        EXAMINATION em, V_SM_QEXAMSUBJECT es, EXAMQUESTION eq,PREFECTURE p"
		+" WHERE  q.EXAMYEAR   =?"
		+"   AND  q.EXAMCD     =?"
		+"   AND  q.EXAMYEAR   =em.EXAMYEAR"
		+"   AND  q.EXAMCD     =em.EXAMCD"
		+"   AND  q.EXAMYEAR   =es.EXAMYEAR"
		+"   AND  q.EXAMCD     =es.EXAMCD"
		+"   AND  q.SUBCD      =es.SUBCD"
		+"   AND  q.EXAMYEAR   =eq.EXAMYEAR"
		+"   AND  q.EXAMCD     =eq.EXAMCD"
		+"   AND  q.SUBCD      =eq.SUBCD"
		+"   AND  q.QUESTION_NO=eq.QUESTION_NO"
		+"   AND  q.PREFCD     = p.PREFCD"
		+"   AND  q.PREFCD IN (SELECT FACULTYCD FROM SCHOOL sc WHERE BUNDLECD = ?)"
		+" UNION ALL"
		+" SELECT 0 DEFFLG, q.EXAMYEAR EXAMYEAR, q.EXAMCD EXAMCD,em.EXAMNAME,"
		+"        3 UNIT, sc.FACULTYCD PREFCD, p.PREFNAME,"
		+"        q.BUNDLECD BUNDLECD, sc.BUNDLENAME, 99 GRADE,'99' CLASS,"
		+"        es.SUBCD  SUBCD, es.SUBNAME,"
		+"        eq.QUESTION_NO QUESTION_NO, eq.QUESTIONNAME1, eq.QALLOTPNT,"
		+"        q.NUMBERS, q.AVGPNT, q.AVGSCORERATE, eq.DISPLAY_NO DISPLAY_NO"
		+" FROM   QRECORD_S q,"
		+"        EXAMINATION em,V_SM_QEXAMSUBJECT es, EXAMQUESTION eq,"
		+"        SCHOOL sc,PREFECTURE p"
		+" WHERE  q.EXAMYEAR   =?"
		+"   AND  q.EXAMCD     =?"
		+"   AND  q.BUNDLECD IN (?)"
		+"   AND  q.EXAMYEAR   =em.EXAMYEAR"
		+"   AND  q.EXAMCD     =em.EXAMCD"
		+"   AND  q.EXAMYEAR   =es.EXAMYEAR"
		+"   AND  q.EXAMCD     =es.EXAMCD"
		+"   AND  q.SUBCD      =es.SUBCD"
		+"   AND  q.EXAMYEAR   =eq.EXAMYEAR"
		+"   AND  q.EXAMCD     =eq.EXAMCD"
		+"   AND  q.SUBCD      =eq.SUBCD"
		+"   AND  q.QUESTION_NO=eq.QUESTION_NO"
		+"   AND  sc.BUNDLECD  = q.BUNDLECD"
		+"   AND  sc.FACULTYCD = p.PREFCD";
		if(classDefFlg == 1){
			query+=" UNION ALL"
			+" SELECT 0 DEFFLG, q.EXAMYEAR EXAMYEAR, q.EXAMCD EXAMCD,em.EXAMNAME,"
			+"        4 UNIT, sc.FACULTYCD PREFCD, p.PREFNAME,"
			+"        q.BUNDLECD BUNDLECD, sc.BUNDLENAME, q.GRADE GRADE,q.CLASS CLASS,"
			+"        es.SUBCD SUBCD, es.SUBNAME,"
			+"        eq.QUESTION_NO QUESTION_NO, eq.QUESTIONNAME1, eq.QALLOTPNT,"
			+"        q.NUMBERS, q.AVGPNT, q.AVGSCORERATE, eq.DISPLAY_NO DISPLAY_NO"
			+" FROM   QRECORD_C q,"
			+"        EXAMINATION em,V_SM_QEXAMSUBJECT es, EXAMQUESTION eq,"
			+"        SCHOOL sc,PREFECTURE p"
			+" WHERE  q.EXAMYEAR   =?"
			+"   AND  q.EXAMCD     =?"
			+"   AND  q.BUNDLECD IN (?)"
			+"   AND  q.EXAMYEAR   =em.EXAMYEAR"
			+"   AND  q.EXAMCD     =em.EXAMCD"
			+"   AND  q.EXAMYEAR   =es.EXAMYEAR"
			+"   AND  q.EXAMCD     =es.EXAMCD"
			+"   AND  q.SUBCD      =es.SUBCD"
			+"   AND  q.EXAMYEAR   =eq.EXAMYEAR"
			+"   AND  q.EXAMCD     =eq.EXAMCD"
			+"   AND  q.SUBCD      =eq.SUBCD"
			+"   AND  q.QUESTION_NO=eq.QUESTION_NO"
			+"   AND  sc.BUNDLECD  = q.BUNDLECD"
			+"   AND  sc.FACULTYCD = p.PREFCD";
		}
		if(schoolDefFlg==1){
			query+=" UNION ALL"
			+" SELECT 1 DEFFLG, q.EXAMYEAR EXAMYEAR, q.EXAMCD EXAMCD,em.EXAMNAME,"
			+"        3 UNIT, sc.FACULTYCD PREFCD, p.PREFNAME,"
			+"        q.BUNDLECD BUNDLECD, sc.BUNDLENAME, 99 GRADE,'99' CLASS,"
			+"        es.SUBCD SUBCD, es.SUBNAME,"
			+"        eq.QUESTION_NO QUESTION_NO, eq.QUESTIONNAME1,eq.QALLOTPNT,"
			+"        q.NUMBERS, q.AVGPNT, q.AVGSCORERATE, eq.DISPLAY_NO DISPLAY_NO"
			+" FROM   QRECORD_S q,"
			+"        EXAMINATION em,V_SM_QEXAMSUBJECT es, EXAMQUESTION eq,"
			+"        SCHOOL sc,PREFECTURE p"
			+" WHERE  q.EXAMYEAR   =?"
			+"   AND  q.EXAMCD     =?"
			+"   AND  q.BUNDLECD IN  (" + OtherSchools + ")"
			+"   AND  q.EXAMYEAR   =em.EXAMYEAR"
			+"   AND  q.EXAMCD     =em.EXAMCD"
			+"   AND  q.EXAMYEAR   =es.EXAMYEAR"
			+"   AND  q.EXAMCD     =es.EXAMCD"
			+"   AND  q.SUBCD      =es.SUBCD"
			+"   AND  q.EXAMYEAR   =eq.EXAMYEAR"
			+"   AND  q.EXAMCD     =eq.EXAMCD"
			+"   AND  q.SUBCD      =eq.SUBCD"
			+"   AND  q.QUESTION_NO=eq.QUESTION_NO"
			+"   AND  sc.BUNDLECD  = q.BUNDLECD"
			+"   AND  sc.FACULTYCD = p.PREFCD";
		}
//		if(schoolDefFlg==1 && classDefFlg == 1){
//			query+=" UNION ALL"
//			+" SELECT 1 DEFFLG, q.EXAMYEAR EXAMYEAR, q.EXAMCD EXAMCD,em.EXAMNAME,"
//			+"        4 UNIT, sc.FACULTYCD PREFCD, p.PREFNAME,"
//			+"        q.BUNDLECD BUNDLECD, sc.BUNDLENAME, q.GRADE GRADE, q.CLASS CLASS,"
//			+"        es.SUBCD SUBCD, es.SUBNAME,"
//			+"        eq.QUESTION_NO QUESTION_NO, eq.QUESTIONNAME1, eq.QALLOTPNT,"
//			+"        q.NUMBERS, q.AVGPNT, q.AVGSCORERATE "
//			+" FROM   QRECORD_C q,"
//			+"        EXAMINATION em,EXAMSUBJECT es, EXAMQUESTION eq,"
//			+"        SCHOOL sc,PREFECTURE p"
//			+" WHERE  q.EXAMYEAR   =?"
//			+"   AND  q.EXAMCD     =?"
//			+"   AND  q.BUNDLECD IN (" + OtherSchools + ")"
//			+"   AND  q.EXAMYEAR   =em.EXAMYEAR"
//			+"   AND  q.EXAMCD     =em.EXAMCD"
//			+"   AND  q.EXAMYEAR   =es.EXAMYEAR"
//			+"   AND  q.EXAMCD     =es.EXAMCD"
//			+"   AND  q.SUBCD      =es.SUBCD"
//			+"   AND  q.EXAMYEAR   =eq.EXAMYEAR"
//			+"   AND  q.EXAMCD     =eq.EXAMCD"
//			+"   AND  q.SUBCD      =eq.SUBCD"
//			+"   AND  q.QUESTION_NO=eq.QUESTION_NO"
//			+"   AND  sc.BUNDLECD  = q.BUNDLECD"
//			+"   AND  sc.FACULTYCD = p.PREFCD";
//		}
		query+=" ORDER BY  DEFFLG, EXAMYEAR, EXAMCD, UNIT, PREFCD, BUNDLECD, GRADE, CLASS, SUBCD, DISPLAY_NO, QUESTION_NO ";
		try {
			stmt = con.prepareStatement(query);
			stmt.setString(1, targetYear);
			stmt.setString(2, targetExam);
			stmt.setString(3, schoolCd);
			stmt.setString(4, targetYear);
			stmt.setString(5, targetExam);
			stmt.setString(6, schoolCd);
			stmt.setString(7, targetYear);
			stmt.setString(8, targetExam);
			stmt.setString(9, schoolCd);
			if(classDefFlg == 1){
				stmt.setString(10, targetYear);
				stmt.setString(11, targetExam);
				stmt.setString(12, schoolCd);
				if(schoolDefFlg==1){
					stmt.setString(13, targetYear);
					stmt.setString(14, targetExam);
				}
			} else {
				if(schoolDefFlg==1){
					stmt.setString(10, targetYear);
					stmt.setString(11, targetExam);
				}
			}
			rs = stmt.executeQuery();
            
			while (rs.next()) {
				LinkedHashMap data = new LinkedHashMap();
				data.put(new Integer("1"), rs.getString(2));
				data.put(new Integer("2"), rs.getString(3));
				data.put(new Integer("3"), rs.getString(4));
				data.put(new Integer("4"), rs.getString(5));
				data.put(new Integer("5"), rs.getString(6));
				data.put(new Integer("6"), rs.getString(7));
				data.put(new Integer("7"), rs.getString(8));
				data.put(new Integer("8"), rs.getString(9));
				data.put(new Integer("9"), rs.getString(10));
				data.put(new Integer("10"), rs.getString(11));
				data.put(new Integer("11"), rs.getString(12));
				data.put(new Integer("12"), rs.getString(13));
				data.put(new Integer("13"), rs.getString(20)); // 設問番号
				data.put(new Integer("14"), rs.getString(15));
				data.put(new Integer("15"), rs.getString(16));
				data.put(new Integer("16"), rs.getString(17));
				if ( rs.getString(18) == null ) {
					data.put(new Integer("17"), rs.getString(18));
				} else {
					data.put(new Integer("17"), String.valueOf(rs.getDouble(18)));
				}
				if ( rs.getString(19) == null ) {
					data.put(new Integer("18"), rs.getString(19));
				} else {
					data.put(new Integer("18"), String.valueOf(rs.getDouble(19)));
				}
				list.add(data);
			}
		} catch (SQLException ex) {
			throw new SQLException("設問別成績の抽出に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return list;
	}

	/**
	 * @param connection
	 */
	public void setCon(Connection connection) {
		con = connection;
	}

	/**
	 * @param string
	 */
	public void setOtherSchools(String string) {
		OtherSchools = string;
	}

	/**
	 * @param string
	 */
	public void setSchoolCd(String string) {
		schoolCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExam(String string) {
		targetExam = string;
	}

	/**
	 * @param string
	 */
	public void setTargetYear(String string) {
		targetYear = string;
	}

	/**
	 * @param list
	 */
	public void setHeader(LinkedList list) {
		header = list;
	}


	/**
	 * @param i
	 */
	public void setClassDefFlg(int i) {
		classDefFlg = i;
	}

	/**
	 * @param i
	 */
	public void setOutType(int i) {
		outType = i;
	}

	/**
	 * @param i
	 */
	public void setSchoolDefFlg(int i) {
		schoolDefFlg = i;
	}

}
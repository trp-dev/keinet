/**
 * 校内成績分析−校内成績　設問別成績
 * 	Excelファイル編集
 * 作成日: 2009/10/21
 * @author	Fujito URAKAWA
 *
 * 2009.10.21 Fujito URAKAWA - Totec
 *            新校内資料「科目・設問別正答率（レベル別）」対応
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S13Item;
import jp.co.fj.keinavi.excel.data.school.S13MarkAreaListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S13_04 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;		// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	final private String masterfile0 = "S13_04";
	final private String masterfile1 = "S13_04";
	private String masterfile = "";

	private static final int START_ROW = 9;		// データセット開始位置
	private static final int MAX_SHEET_NUM = 50;	// １ブック内最大シート数


	/*
	 * 	Excel編集メイン
	 * 		S13Item s13Item: データクラス
	 * 		String masterfile: マスタExcelファイル名（フルパス）
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int s13_04EditExcel(S13Item s13Item, ArrayList outfilelist, int intSaveFlg, String UserID ) {

		HSSFWorkbook	workbook 		= null;
		HSSFSheet		workSheet		= null;

		String tmp_kmkcd = "";				// 科目変更チェック変数
		int intChangeLineCount = 0;		// 改シートカウンタ
		int intChangeSheetCount = 0;		// 改シートカウンタ
		int intChangeBookCount = 0;			// 改ブックカウンタ

		int intRow = 0;						// 処理中の行

		KNLog log = KNLog.getInstance(null,null,null);

		//テンプレートの決定
		selectTemplete(s13Item.getIntShubetsuFlg());

		S13MarkAreaListBean data = new S13MarkAreaListBean();

		try {
			// データ取得
			ArrayList datalist = s13Item.getS13MarkAreaList();
			Iterator itr = datalist.iterator();

			log.Ep("S13_04","基本データデータセット開始","");

			workbook = roadMasterFile(intSaveFlg);		// Excelマスタファイル読込

			// データが存在しない場合には、ヘッダ情報をセットして終了！
			if( itr.hasNext() == false ){
				workSheet = initWorkSheet(workbook);				// ワークシート初期化
				setUpbaseData(workbook, workSheet, s13Item, null);	// 基本情報セット
			}

			log.Ep("S13_04","マークシートデータセット開始","");

			while( itr.hasNext() ) {
				data = (S13MarkAreaListBean)itr.next();

				// 初回＆科目コードが変更された場合の処理
				if (isChangeKmkCd(tmp_kmkcd, data.getStrKmkCd())) {

					/****************************************************/
					/** 50シートを超えている場合は保存してシート初期化 **/
					/****************************************************/
					if (intChangeSheetCount >= 50) {
						// 保存
						boolean isResult = saveExcelFile(intSaveFlg, outfilelist, workbook, UserID,
								intChangeBookCount, masterfile, MAX_SHEET_NUM);
						if (!isResult) return errfwrite;
						intChangeBookCount++;
						intChangeSheetCount = 0;						// 改シート数を初期化
					}
					else {
						intChangeSheetCount++;							// 改シート数をインクリメント
					}

					workSheet = initWorkSheet(workbook);				// ワークシート初期化
					if( workbook==null ) return errfread;				// ワークシート初期化不可ならreturn
					setUpbaseData(workbook, workSheet, s13Item, data);	// 基本情報セット
					intRow = START_ROW;									// 現在の実行行を初期化
					intChangeLineCount = 0;								// 改行数を初期化
				}

				//2019/09/13 QQ)Oosaki 共通テスト対応 UPD START
				///****************************************************/
				///****** 50行を超えている場合はしてシート初期化 ******/
				///****************************************************/
				//else if(intChangeLineCount >= 50){
				/****************************************************/
                                /****** 48行を超えている場合はしてシート初期化 ******/
                                /****************************************************/
                                else if(intChangeLineCount >= 48){
				//2019/09/13 QQ)Oosaki 共通テスト対応 UPD END
					// シート初期化
					workSheet = initWorkSheet(workbook);				// ワークシート初期化
					setUpbaseData(workbook, workSheet, s13Item, data);	// 基本情報セット
					intRow = START_ROW;									// 現在の実行行を初期化
					intChangeSheetCount++;								// 改シート数をインクリメント
					intChangeLineCount = 0;								// 改行数を初期化
				}

				// データセット実行
				executeDataSet(workSheet, data, intRow);

				// 処理行をインクリメント
				intRow++;
				// 改行数をインクリメント
				intChangeLineCount++;
				// 処理中の科目コードをセット
				tmp_kmkcd = data.getStrKmkCd();
			}

			log.Ep("S13_04","マークシートデータセット終了","");

			// Excelファイル保存
			boolean bolRet = saveExcelFile(intSaveFlg, outfilelist, workbook, UserID,
					intChangeBookCount, masterfile, intChangeSheetCount);

			if( bolRet == false ) return errfwrite;

		} catch(Exception e) {
			log.Err("S13_04","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("S13_04","基本データデータセット終了","");
		return noerror;
	}

	/**
	 * データセットメイン処理を行う
	 * @param workSheet データセットを行うシート
	 * @param data セットするデータ
	 * @param row 現在の処理行
	 */
	private void executeDataSet(HSSFSheet workSheet, S13MarkAreaListBean data, int row) {

		HSSFRow	workRow	= null;
		HSSFCell workCell = null;

		// 問題番号セット(0)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 0 );
		if (data.getIntKantoRcFlg() == 0) workCell.setCellValue( data.getStrMonnum() );
		// 大問・内容セット(1)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 1 );
		if (data.getIntKantoRcFlg() == 0) workCell.setCellValue( data.getStrDaimonNaiyo() );
		//2019/09/17 QQ)Oosaki 共通テスト対応 DEL START
		//// 完答問題フラグセット(2)
		//workCell = cm.setCell( workSheet, workRow, workCell, row, 2 );
		//if (data.getIntKantoRcFlg() == 0 && data.getIntKantoFlg() == 1) {
		//	workCell.setCellValue( "●" );
		//}else if (data.getIntKantoRcFlg() == 1 && data.getIntKantoFlg() == 0) {
		//	workCell.setCellValue( "完答" );
		//}
		//2019/09/17 QQ)Oosaki 共通テスト対応 DEL END
		// 解答番号(3)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 3 );
		if (data.getIntKantoRcFlg() == 0) workCell.setCellValue( data.getStrKaitoNo());
		// 正答率[全体]（校内）*(4)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 4 );
		if (data.getIntKantoFlg() == 0) setUpAstaMark(workCell, data.getFloSeitoritsuAll(), data.getFloSeitoritsuHome());
		// 正答率[全体]（校内）(5)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 5 );
		setUpFlortValue(workCell, data.getFloSeitoritsuHome());
		// 正答率[全体]（全国）(6)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 6 );
		setUpFlortValue(workCell, data.getFloSeitoritsuAll());
		// 正答率[S]（校内）*(7)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 7 );
		if (data.getIntKantoFlg() == 0) setUpAstaMark(workCell, data.getFloSeitoritsuAllS(), data.getFloSeitoritsuHomeS());
		// 正答率[S]（校内）(8)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 8 );
		setUpFlortValue(workCell, data.getFloSeitoritsuHomeS());
		// 正答率[S]（全国）(9)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 9 );
		setUpFlortValue(workCell, data.getFloSeitoritsuAllS());
		// 正答率[A]（校内）*(10)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 10 );
		if (data.getIntKantoFlg() == 0) setUpAstaMark(workCell, data.getFloSeitoritsuAllA(), data.getFloSeitoritsuHomeA());
		// 正答率[A]（校内）(11)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 11 );
		setUpFlortValue(workCell, data.getFloSeitoritsuHomeA());
		// 正答率[A]（全国）(12)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 12 );
		setUpFlortValue(workCell, data.getFloSeitoritsuAllA());
		// 正答率[B]（校内）*(13)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 13 );
		if (data.getIntKantoFlg() == 0) setUpAstaMark(workCell, data.getFloSeitoritsuAllB(), data.getFloSeitoritsuHomeB());
		// 正答率[B]（校内）(14)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 14 );
		setUpFlortValue(workCell, data.getFloSeitoritsuHomeB());
		// 正答率[B]（全国）(15)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 15 );
		setUpFlortValue(workCell, data.getFloSeitoritsuAllB());
		// 正答率[C]（校内）*(16)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 16 );
		if (data.getIntKantoFlg() == 0) setUpAstaMark(workCell, data.getFloSeitoritsuAllC(), data.getFloSeitoritsuHomeC());
		// 正答率[C]（校内）(17)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 17 );
		setUpFlortValue(workCell, data.getFloSeitoritsuHomeC());
		// 正答率[C]（全国）(18)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 18 );
		setUpFlortValue(workCell, data.getFloSeitoritsuAllC());
		// 正答率[D]（校内）*(19)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 19 );
		if (data.getIntKantoFlg() == 0) setUpAstaMark(workCell, data.getFloSeitoritsuAllD(), data.getFloSeitoritsuHomeD());
		// 正答率[D]（校内）(20)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 20 );
		setUpFlortValue(workCell, data.getFloSeitoritsuHomeD());
		// 正答率[D]（全国）(21)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 21 );
		setUpFlortValue(workCell, data.getFloSeitoritsuAllD());
		// 正答率[E]（校内）*(22)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 22 );
		if (data.getIntKantoFlg() == 0) setUpAstaMark(workCell, data.getFloSeitoritsuAllE(), data.getFloSeitoritsuHomeE());
		// 正答率[E]（校内）(23)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 23 );
		setUpFlortValue(workCell, data.getFloSeitoritsuHomeE());
		// 正答率[E]（全国）(24)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 24 );
		setUpFlortValue(workCell, data.getFloSeitoritsuAllE());
		// 正答率[F]（校内）*(25)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 25 );
		if (data.getIntKantoFlg() == 0) setUpAstaMark(workCell, data.getFloSeitoritsuAllF(), data.getFloSeitoritsuHomeF());
		// 正答率[F]（校内）(26)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 26 );
		setUpFlortValue(workCell, data.getFloSeitoritsuHomeF());
		// 正答率[F]（全国）(27)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 27 );
		setUpFlortValue(workCell, data.getFloSeitoritsuAllF());

		// 正答率の差[S-A]（校内）(29)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 29 );
		setUpDiffFlortValue(workCell, data.getFloSeitoritsuHomeS(), data.getFloSeitoritsuHomeA());
		// 正答率の差[S-A]（全国）(30)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 30 );
		setUpDiffFlortValue(workCell, data.getFloSeitoritsuAllS(), data.getFloSeitoritsuAllA());
		// 正答率の差[A-B]（校内）(32)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 32 );
		setUpDiffFlortValue(workCell, data.getFloSeitoritsuHomeA(), data.getFloSeitoritsuHomeB());
		// 正答率の差[A-B]（全国）(33)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 33 );
		setUpDiffFlortValue(workCell, data.getFloSeitoritsuAllA(), data.getFloSeitoritsuAllB());
		// 正答率の差[B-C]（校内）(35)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 35 );
		setUpDiffFlortValue(workCell, data.getFloSeitoritsuHomeB(), data.getFloSeitoritsuHomeC());
		// 正答率の差[B-C]（全国）(36)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 36 );
		setUpDiffFlortValue(workCell, data.getFloSeitoritsuAllB(), data.getFloSeitoritsuAllC());
		// 正答率の差[C-D]（校内）(38)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 38 );
		setUpDiffFlortValue(workCell, data.getFloSeitoritsuHomeC(), data.getFloSeitoritsuHomeD());
		// 正答率の差[C-D]（全国）(39)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 39 );
		setUpDiffFlortValue(workCell, data.getFloSeitoritsuAllC(), data.getFloSeitoritsuAllD());
		// 正答率の差[D-E]（校内）(41)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 41 );
		setUpDiffFlortValue(workCell, data.getFloSeitoritsuHomeD(), data.getFloSeitoritsuHomeE());
		// 正答率の差[D-E]（全国）(42)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 42 );
		setUpDiffFlortValue(workCell, data.getFloSeitoritsuAllD(), data.getFloSeitoritsuAllE());
		// 正答率の差[E-F]（校内）(44)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 44 );
		setUpDiffFlortValue(workCell, data.getFloSeitoritsuHomeE(), data.getFloSeitoritsuHomeF());
		// 正答率の差[E-F]（全国）(45)
		workCell = cm.setCell( workSheet, workRow, workCell, row, 45 );
		setUpDiffFlortValue(workCell, data.getFloSeitoritsuAllE(), data.getFloSeitoritsuAllF());
	}

	/**
	 * 全国＞校内の場合には「*」をセットする。
	 * ※どちらかの値が「-999.0」の場合には、データをセットしない
	 * @param workCell
	 * @param floA　
	 * @param floB
	 */
	private void setUpAstaMark(HSSFCell workCell, float floA,	float floB) {

		if (floA == -999.0) return;
		if (floB == -999.0) return;

		if ( floA > floB ) {
			workCell.setCellValue("*");
		}

	}

	/**
	 * セルにfloatデータの差をセットする。
	 * ※どちらかの値が「-999.0」の場合には、データをセットしない
	 * @param workCell
	 * @param floA
	 * @param floB
	 */
	private void setUpDiffFlortValue(HSSFCell workCell, float floA, float floB) {

		if (floA == -999.0) return;
		if (floB == -999.0) return;

		workCell.setCellValue( floA - floB );
	}

	/**
	 * セルにfloatデータをセットする。
	 * ※float値が「-999.0」の場合には、データをセットしない
	 * @param workCell
	 * @param floSeitoritsuHome
	 */
	private void setUpFlortValue(HSSFCell workCell, float floValue) {

		if (floValue != -999.0) {
			workCell.setCellValue( floValue );
		}

	}

	/**
	 * 基本情報をExcelにセットする
	 * @param workbook
	 * @param workSheet
	 * @param item
	 * @param data
	 * @param workCell
	 */
	private void setUpbaseData(HSSFWorkbook workbook, HSSFSheet workSheet, S13Item item, S13MarkAreaListBean data) {

		HSSFRow	workRow	= null;
		HSSFCell workCell = null;

		// ヘッダ右側に帳票作成日時を表示する
		cm.setHeader(workbook, workSheet);
		// セキュリティスタンプセット
		String secuFlg = cm.setSecurity( workbook, workSheet, item.getIntSecuFlg() ,41 ,45 );
		workCell = cm.setCell( workSheet, workRow, workCell, 0, 41 );
		workCell.setCellValue(secuFlg);
		// 学校名セット
		workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
		workCell.setCellValue( "学校名　：" + cm.toString(item.getStrGakkomei()) );
		// 模試月取得
		String moshi =cm.setTaisyouMoshi( item.getStrMshDate() );
		// 対象模試セット
		workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
		workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(item.getStrMshmei()) + moshi);
		// 型・科目名＋配点セット
		String haiten = "";
		if ( data != null && !cm.toString(data.getStrHaitenKmk()).equals("") ) {
			haiten = "（" + data.getStrHaitenKmk() + "）";
		}
		workCell = cm.setCell( workSheet, workRow, workCell, 5, 0 );
		if (data != null){
			workCell.setCellValue( "型・科目：" + cm.toString(data.getStrKmkmei()) + haiten );
		}
		// 受験者数（校内）セット
		workCell = cm.setCell( workSheet, workRow, workCell, 5, 5 );
		if ( data != null && data.getIntNinzu()!=-999 ) {
			workCell.setCellValue( "受験者数：" + data.getIntNinzu() + "人" );
		} else {
			workCell.setCellValue( "受験者数：" );
		}

		// 受験者数（校内）
		workCell = cm.setCell( workSheet, workRow, workCell, 8, 4 );
		if (data != null) {
			setUpFlortValue(workCell, (float)data.getIntNinzu());
		}
		// 受験者数（S）
		workCell = cm.setCell( workSheet, workRow, workCell, 8, 7 );
		if (data != null) {
			setUpFlortValue(workCell, (float)data.getIntNinzuS());
		}
		// 受験者数（A）
		workCell = cm.setCell( workSheet, workRow, workCell, 8, 10 );
		if (data != null) {
			setUpFlortValue(workCell, (float)data.getIntNinzuA());
		}
		// 受験者数（B）
		workCell = cm.setCell( workSheet, workRow, workCell, 8, 13 );
		if (data != null) {
			setUpFlortValue(workCell, (float)data.getIntNinzuB());
		}
		// 受験者数（C）
		workCell = cm.setCell( workSheet, workRow, workCell, 8, 16 );
		if (data != null) {
			setUpFlortValue(workCell, (float)data.getIntNinzuC());
		}
		// 受験者数（D）
		workCell = cm.setCell( workSheet, workRow, workCell, 8, 19 );
		if (data != null) {
			setUpFlortValue(workCell, (float)data.getIntNinzuD());
		}
		// 受験者数（E）
		workCell = cm.setCell( workSheet, workRow, workCell, 8, 22 );
		if (data != null) {
			setUpFlortValue(workCell, (float)data.getIntNinzuE());
		}
		// 受験者数（F）
		workCell = cm.setCell( workSheet, workRow, workCell, 8, 25 );
		if (data != null) {
			setUpFlortValue(workCell, (float)data.getIntNinzuF());
		}


	}

	/**
	 * ファイル保存メソッド
	 * @param intFlg
	 * @param list
	 * @param book
	 * @param uID
	 * @param intBookCount
	 * @param mfile
	 * @param intSheetCount
	 * @return false:失敗　true:成功　
	 */
	private boolean saveExcelFile(int intFlg, ArrayList list,
			HSSFWorkbook book, String uID, int intBookCount,
			String mfile, int intSheetCount) {

		return cm.bolFileSave(intFlg, list, book, uID, intBookCount, mfile, intSheetCount);

	}

	/**
	 * シートのテンプレートをコピーする（初期化）
	 * @param workbook
	 * @return HSSFSheet
	 */
	private HSSFSheet initWorkSheet(HSSFWorkbook workbook) {

		return workbook.cloneSheet(0);

	}

	/**
	 * Excelマスタファイルをロードする
	 * @param intFlg
	 * @return
	 */
	private HSSFWorkbook roadMasterFile(int intFlg) {

		HSSFWorkbook workbook = cm.getMasterWorkBook(masterfile, intFlg);
		return workbook;

	}

	/**
	 * 科目コードが変更したか
	 * @param kmkcd
	 * @param strKmkCd
	 * @return
	 */
	private boolean isChangeKmkCd(String kmkcd, String strKmkCd) {

		return !cm.toString(kmkcd).equals(cm.toString(strKmkCd));

	}

	/**
	 * 使用するテンプレートを決定する
	 * @param intShubetsuFlg
	 */
	private void selectTemplete(int intShubetsuFlg) {
		if (intShubetsuFlg == 1){
			this.masterfile = masterfile1;
		} else{
			this.masterfile = masterfile0;
		}

	}
}
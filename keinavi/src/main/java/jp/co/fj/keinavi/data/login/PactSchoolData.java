/*
 * 作成日: 2004/08/20
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.login;


/**
 * 契約校データ
 * 
 * @author kawai
 */
public class PactSchoolData extends Certification {

	private String schoolCD; // 学校コード
	private short pactDiv; // 契約区分
	private short certUserDiv; // 証明書ユーザ区分
	private String passwd; // パスワード
	private short featprovMode; // 私書箱機能モード
	private String pactTerm; // 契約期限
	private String certFlag; // 証明書有効フラグ
	
	/**
	 * @return
	 */
	public String getPasswd() {
		return passwd == null ? "" : passwd;
	}

	/**
	 * @return
	 */
	public String getSchoolCD() {
		return schoolCD;
	}

	/**
	 * @param string
	 */
	public void setPasswd(String string) {
		passwd = string;
	}

	/**
	 * @param string
	 */
	public void setSchoolCD(String string) {
		schoolCD = string;
	}

	/**
	 * @return
	 */
	public short getCertUserDiv() {
		return certUserDiv;
	}

	/**
	 * @return
	 */
	public short getPactDiv() {
		return pactDiv;
	}

	/**
	 * @param s
	 */
	public void setCertUserDiv(short s) {
		certUserDiv = s;
	}

	/**
	 * @param s
	 */
	public void setPactDiv(short s) {
		pactDiv = s;
	}

	/**
	 * @return
	 */
	public short getFeatprovMode() {
		return featprovMode;
	}

	/**
	 * @param s
	 */
	public void setFeatprovMode(short s) {
		featprovMode = s;
	}

	/**
	 * @return
	 */
	public String getCertFlag() {
		return certFlag;
	}

	/**
	 * @param string
	 */
	public void setCertFlag(String string) {
		certFlag = string;
	}

	/**
	 * @return
	 */
	public String getPactTerm() {
		return pactTerm;
	}

	/**
	 * @param string
	 */
	public void setPactTerm(String string) {
		pactTerm = string;
	}

}

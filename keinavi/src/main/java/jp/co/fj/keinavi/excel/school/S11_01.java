/**
 * 校内成績分析−校内成績　成績概況
 * 	Excelファイル編集
 * 作成日: 2004/07/02
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.io.*;
import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.excel.cm.*;

import org.apache.poi.hssf.usermodel.*;
import jp.co.fj.keinavi.util.log.*;

public class S11_01 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
//	private String		strArea			= "A1:L75";	//印刷範囲	
	
	final private String masterfile0 = "S11_01";
	final private String masterfile1 = "S11_01";
	private String masterfile = "";

/*
 * 	Excel編集メイン
 * 		S11Item s11Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s11_01EditExcel(S11Item s11Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		FileInputStream	fin			= null;
		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		int			intMaxSheetIndex	= 0;	//シートカウンタ
		int			kmkMaxSheetCnt		= 1;	//科目用シートカウンタ
		int			kataCnt				= 0;	//型カウンタ
		int			kmkCnt				= 0;	//科目カウンタ
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		// *作成用
		int	ninzu = 0;
		float	hensa = 0;
		
		//テンプレートの決定
		if (s11Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		//マスタExcel読み込み
		workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
		if( workbook==null ){
			return errfread;
		}
		
		// 基本ファイルを読込む
		S11KataListBean s11KataListBean = new S11KataListBean();
		S11KmkListBean s11KmkListBean = new S11KmkListBean();
		
		try {
			// 型データセット
			ArrayList s11KataList = s11Item.getS11KataList();
			Iterator itrKata = s11KataList.iterator();
			
			//コメントの受験型取得
			while( itrKata.hasNext() ) {
				s11KataListBean = (S11KataListBean)itrKata.next();
				
				// *作成用 → 2005/03/16 科目ｺｰﾄﾞが7000番台を対象とする。
//				if ( !cm.toString(s11KataListBean.getStrHaitenKmk()).equals("") && ninzu < s11KataListBean.getIntNinzuHome() ) {
				if ( Integer.parseInt(s11KataListBean.getStrKmkCd()) < 8000 && ninzu < s11KataListBean.getIntNinzuHome() ) {
					ninzu = s11KataListBean.getIntNinzuHome();
					hensa = s11KataListBean.getFloHensaHome();
					s11Item.setStrComment( s11KataListBean.getStrKmkmei() );
				}
			}
			
			itrKata = s11KataList.iterator();
			int row = 6;
			
			log.Ep("S11_01","型データセット開始","");
			
			while( itrKata.hasNext() ) {
				s11KataListBean = (S11KataListBean)itrKata.next();
				
				if (kataCnt == 0) {
					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);
					intMaxSheetIndex++;
					row = 6;
					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);

					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, s11Item.getIntSecuFlg() ,10 ,11 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 10 );
					workCell.setCellValue(secuFlg);
			
					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( "学校名　：" + cm.toString(s11Item.getStrGakkomei()) );
			
					// 模試月取得
					String moshi =cm.setTaisyouMoshi( s11Item.getStrMshDate() );
					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
					workCell.setCellValue( "対象模試：" + cm.toString(s11Item.getStrMshmei()) + moshi);
			
					// 型用学校名ヘッダデータセット
					workCell = cm.setCell( workSheet, workRow, workCell, 4, 2 );
					workCell.setCellValue( s11Item.getStrGakkomei() );
			
					// 型用県名ヘッダデータセット
					workCell = cm.setCell( workSheet, workRow, workCell, 4, 6 );
					workCell.setCellValue( s11Item.getStrKenmei() );
					
					// 科目用学校名ヘッダデータセット
					workCell = cm.setCell( workSheet, workRow, workCell, 33, 2 );
					workCell.setCellValue( s11Item.getStrGakkomei() );
					
					// 科目用県名ヘッダデータセット
					workCell = cm.setCell( workSheet, workRow, workCell, 33, 6 );
					workCell.setCellValue( s11Item.getStrKenmei() );
					
					// コメントデータセット
					if ( !cm.toString(s11Item.getStrComment()).equals("") ) {
						workCell = cm.setCell( workSheet, workRow, workCell, 31, 11 );
						workCell.setCellValue( "※校内の受験人数が最も多い受験型「"+ s11Item.getStrComment() + "」の平均偏差値を");
						workCell = cm.setCell( workSheet, workRow, workCell, 32, 11 );
						workCell.setCellValue( "下回る科目の平均偏差値に\"*\"を表示しています。　 　　　　" );
					}
				}
				// 型名セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 0 );
				workCell.setCellValue( s11KataListBean.getStrKmkmei() );
				// 配点セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 1 );
				if ( !cm.toString(s11KataListBean.getStrHaitenKmk()).equals("") ) {
					workCell.setCellValue( Integer.parseInt( s11KataListBean.getStrHaitenKmk() ) );
				}
				// 人数（高校）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 2 );
				if ( s11KataListBean.getIntNinzuHome() != -999 ) {
					workCell.setCellValue( s11KataListBean.getIntNinzuHome() );
				}
				// 平均点（高校）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 3 );
				if ( s11KataListBean.getFloHeikinHome() != -999.0 ) {
					workCell.setCellValue( s11KataListBean.getFloHeikinHome() );
				}
				// 平均偏差値（高校）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 5 );
				if ( s11KataListBean.getFloHensaHome() != -999.0 ) {
					workCell.setCellValue( s11KataListBean.getFloHensaHome() );
				}
				// 人数（県）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 6 );
				if ( s11KataListBean.getIntNinzuKen() != -999 ) {
					workCell.setCellValue( s11KataListBean.getIntNinzuKen() );
				}
				// 平均点（県）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 7 );
				if ( s11KataListBean.getFloHeikinKen() != -999.0 ) {
					workCell.setCellValue( s11KataListBean.getFloHeikinKen() );
				}
				// 平均偏差値（県）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 8 );
				if ( s11KataListBean.getFloHensaKen() != -999.0 ) {
					workCell.setCellValue( s11KataListBean.getFloHensaKen() );
				}
				// 人数（全国）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 9 );
				if ( s11KataListBean.getIntNinzuAll() != -999 ) {
					workCell.setCellValue( s11KataListBean.getIntNinzuAll() );
				}
				// 平均点（全国）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 10 );
				if ( s11KataListBean.getFloHeikinAll() != -999.0 ) {
					workCell.setCellValue( s11KataListBean.getFloHeikinAll() );
				}
				// 平均偏差値（全国）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 11 );
				if ( s11KataListBean.getFloHensaAll() != -999.0 ) {
					workCell.setCellValue( s11KataListBean.getFloHensaAll() );
				}
				row++;
				kataCnt++;
				if (kataCnt >= 20) {
					kataCnt = 0;
				}
			}
			
			log.Ep("S11_01","型データセット終了","");
			
			// 科目データセット
			ArrayList s11KmkList = s11Item.getS11KmkList();
			Iterator itrKmk = s11KmkList.iterator();
			row = 35;
			
			log.Ep("S11_01","科目データセット開始","");
			
			if ( intMaxSheetIndex!=0 ) {
				if (intSaveFlg==1 || intSaveFlg==5) {
					workSheet = workbook.getSheetAt(kmkMaxSheetCnt);
				} else {
					workSheet = workbook.getSheetAt(kmkMaxSheetCnt+1);
				}
			}
			
			while( itrKmk.hasNext() ) {
				s11KmkListBean = (S11KmkListBean)itrKmk.next();
				if (kmkCnt == 0) {
					if (kmkMaxSheetCnt>intMaxSheetIndex) {
						// シートテンプレートのコピー
						workSheet = workbook.cloneSheet(0);
						intMaxSheetIndex++;
						kmkMaxSheetCnt++;
					} else {
						if (intSaveFlg==1 || intSaveFlg==5) {
							workSheet = workbook.getSheetAt(kmkMaxSheetCnt);
						} else {
							workSheet = workbook.getSheetAt(kmkMaxSheetCnt+1);
						}
						kmkMaxSheetCnt++;
					}
					row = 35;
					kmkCnt = 0;
					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);

					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, s11Item.getIntSecuFlg() ,10 ,11 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 10 );
					workCell.setCellValue(secuFlg);
			
					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( "学校名　：" + cm.toString(s11Item.getStrGakkomei()) );
			
					// 模試月取得
					String moshi =cm.setTaisyouMoshi( s11Item.getStrMshDate() );
					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
					workCell.setCellValue( "対象模試：" + cm.toString(s11Item.getStrMshmei()) + moshi);
			
					// 型用学校名ヘッダデータセット
					workCell = cm.setCell( workSheet, workRow, workCell, 4, 2 );
					workCell.setCellValue( s11Item.getStrGakkomei() );
			
					// 型用県名ヘッダデータセット
					workCell = cm.setCell( workSheet, workRow, workCell, 4, 6 );
					workCell.setCellValue( s11Item.getStrKenmei() );
					
					// 科目用学校名ヘッダデータセット
					workCell = cm.setCell( workSheet, workRow, workCell, 33, 2 );
					workCell.setCellValue( s11Item.getStrGakkomei() );
					
					// 科目用県名ヘッダデータセット
					workCell = cm.setCell( workSheet, workRow, workCell, 33, 6 );
					workCell.setCellValue( s11Item.getStrKenmei() );
					
					// コメントデータセット
					if ( !cm.toString(s11Item.getStrComment()).equals("") ) {
						workCell = cm.setCell( workSheet, workRow, workCell, 31, 11 );
						workCell.setCellValue( "※校内の受験人数が最も多い受験型「"+ s11Item.getStrComment() + "」の平均偏差値を");
						workCell = cm.setCell( workSheet, workRow, workCell, 32, 11 );
						workCell.setCellValue( "下回る科目の平均偏差値に\"*\"を表示しています。　 　　　　" );

					}
				}
				// 科目名セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 0 );
				workCell.setCellValue( s11KmkListBean.getStrKmkmei() );
				// 配点セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 1 );
				if ( !cm.toString(s11KmkListBean.getStrHaitenKmk()).equals("") ) {
					workCell.setCellValue( Integer.parseInt( s11KmkListBean.getStrHaitenKmk() ) );
				}
				// 人数（高校）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 2 );
				if ( s11KmkListBean.getIntNinzuHome() != -999 ) {
					workCell.setCellValue( s11KmkListBean.getIntNinzuHome() );
				}
				// 平均点（高校）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 3 );
				if ( s11KmkListBean.getFloHeikinHome() != -999.0 ) {
					workCell.setCellValue( s11KmkListBean.getFloHeikinHome() );
				}
				// 平均偏差値（高校）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 5 );
				if ( s11KmkListBean.getFloHensaHome() != -999.0 ) {
					workCell.setCellValue( s11KmkListBean.getFloHensaHome() );
				}
				// 人数（県）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 6 );
				if ( s11KmkListBean.getIntNinzuKen() != -999 ) {
					workCell.setCellValue( s11KmkListBean.getIntNinzuKen() );
				}
				// 平均点（県）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 7 );
				if ( s11KmkListBean.getFloHeikinKen() != -999.0 ) {
					workCell.setCellValue( s11KmkListBean.getFloHeikinKen() );
				}
				// 平均偏差値（県）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 8 );
				if ( s11KmkListBean.getFloHensaKen() != -999.0 ) {
					workCell.setCellValue( s11KmkListBean.getFloHensaKen() );
				}
				// 人数（全国）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 9 );
				if ( s11KmkListBean.getIntNinzuAll() != -999 ) {
					workCell.setCellValue( s11KmkListBean.getIntNinzuAll() );
				}
				// 平均点（全国）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 10 );
				if ( s11KmkListBean.getFloHeikinAll() != -999.0 ) {
					workCell.setCellValue( s11KmkListBean.getFloHeikinAll() );
				}
				// 平均偏差値（全国）セット
				workCell = cm.setCell( workSheet, workRow, workCell, row, 11 );
				if ( s11KmkListBean.getFloHensaAll() != -999.0 ) {
					workCell.setCellValue( s11KmkListBean.getFloHensaAll() );
				}
				// *セット
				if ( hensa > s11KmkListBean.getFloHensaHome() ) {
					workCell = cm.setCell( workSheet, workRow, workCell, row, 4 );
					workCell.setCellValue("*");
				}
				row++;
				kmkCnt++;
				if (kmkCnt >= 40) {
					kmkCnt = 0;
				}
			}
			
			log.Ep("S11_01","科目データセット終了","");
//add 2004/10/25 T.Sakai データ0件対応
			if ( s11KataList.size()==0 && s11KmkList.size()==0 ) {
				// シートテンプレートのコピー
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex++;
				row = 6;
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
				
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s11Item.getIntSecuFlg() ,10 ,11 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 10 );
				workCell.setCellValue(secuFlg);
				
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s11Item.getStrGakkomei()) );
				
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s11Item.getStrMshDate() );
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
				workCell.setCellValue( "対象模試：" + cm.toString(s11Item.getStrMshmei()) + moshi);
				
				// 型用学校名ヘッダデータセット
				workCell = cm.setCell( workSheet, workRow, workCell, 4, 2 );
				workCell.setCellValue( s11Item.getStrGakkomei() );
				
				// 型用県名ヘッダデータセット
				workCell = cm.setCell( workSheet, workRow, workCell, 4, 6 );
				workCell.setCellValue( s11Item.getStrKenmei() );
				
				// 科目用学校名ヘッダデータセット
				workCell = cm.setCell( workSheet, workRow, workCell, 33, 2 );
				workCell.setCellValue( s11Item.getStrGakkomei() );
				
				// 科目用県名ヘッダデータセット
				workCell = cm.setCell( workSheet, workRow, workCell, 33, 6 );
				workCell.setCellValue( s11Item.getStrKenmei() );
				
				// コメントデータセット
				if ( !cm.toString(s11Item.getStrComment()).equals("") ) {
					workCell = cm.setCell( workSheet, workRow, workCell, 31, 11 );
					workCell.setCellValue( "※校内の受験人数が最も多い受験型「"+ s11Item.getStrComment() + "」の平均偏差値を");
					workCell = cm.setCell( workSheet, workRow, workCell, 32, 11 );
					workCell.setCellValue( "下回る科目の平均偏差値に\"*\"を表示しています。　 　　　　" );
				}
			}
//add end
			// Excelファイル保存
			boolean bolRet = false;
			bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("S11_01","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

}

/*
 * 作成日: 2004/07/23
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

import java.io.Serializable;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class SubjectData implements Serializable {
	
	private String subjectCd;
	private String subjectName;
	private String subAddrName;
	private String dispSequence;
	
	/**
	 * コンストラクタ
	 */
	public SubjectData() {
	}

	/**
	 * @param pSubjectCd
	 */
	public SubjectData(String pSubjectCd) {
		subjectCd = pSubjectCd;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(final Object o) {
		return getSubjectCd().equals(((SubjectData) o).getSubjectCd());
	}

	/**
	 * @return
	 */
	public String getSubjectName() {
		return subjectName;
	}

	/**
	 * @return
	 */
	public String getSubjectCd() {
		return subjectCd;
	}

	/**
	 * @param string
	 */
	public void setSubjectName(String string) {
		subjectName = string;
	}

	/**
	 * @param string
	 */
	public void setSubjectCd(String string) {
		subjectCd = string;
	}

	/**
	 * @return
	 */
	public String getDispSequence() {
		return dispSequence;
	}

	/**
	 * @return
	 */
	public String getSubAddrName() {
		return subAddrName;
	}

	/**
	 * @param string
	 */
	public void setDispSequence(String string) {
		dispSequence = string;
	}

	/**
	 * @param string
	 */
	public void setSubAddrName(String string) {
		subAddrName = string;
	}

}

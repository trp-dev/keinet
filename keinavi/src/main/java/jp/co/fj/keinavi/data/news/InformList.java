/*
 * 作成日: 2004/09/09
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.news;

import java.io.Serializable;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class InformList implements Serializable  {

	private String infoId        = null; 	// お知らせＩＤ
	private String displayDiv    = null; 	// 表示区分
	private String title         = null; 	// タイトル
	private String dispStartDTim = null; 	// 掲載開始日時
	private String dispEndDTim   = null; 	// 掲載終了日時
	private String text          = null; 	// 本文テキスト
	private String dispDate      = null; 	// 掲載日付("yyyy/MM/dd")
	private String dispDiv       = null; 	// 掲載区分("一般"、"県別"、"高校別")


	/** -----------------------------------------------*/
	/**
	 * @return
	 */
	public String getInfoId() {
		return infoId;
	}

	/**
	 * @return
	 */
	public String getDisplayDiv() {
		return displayDiv;
	}

	/**
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return
	 */
	public String getDispStartDTim() {
		return dispStartDTim;
	}

	/**
	 * @return
	 */
	public String getDispEndDTim() {
		return dispEndDTim;
	}

	/**
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return
	 */
	public String getDispDate() {
		return dispDate;
	}


	/**
	 * @param string
	 */
	public void setInfoId(String string) {
		infoId = string;
	}

	/**
	 * @param string
	 */
	public void setDisplayDiv(String string) {
		displayDiv = string;
	}

	/**
	 * @param string
	 */
	public void setTitle(String string) {
		title = string;
	}

	/**
	 * @param string
	 */
	public void setDispStartDTim(String string) {
		dispStartDTim = string;
	}

	/**
	 * @param string
	 */
	public void setDispEndDTim(String string) {
		dispEndDTim = string;
	}

	/**
	 * @param string
	 */
	public void setText(String string) {
		text = string;
	}


	/**
	 * @param string
	 */
	public void setDispDate(String string) {
		dispDate = string;
	}

	/**
	 * @return
	 */
	public String getDispDiv() {
		return dispDiv;
	}

	/**
	 * @param string
	 */
	public void setDispDiv(String string) {
		dispDiv = string;
	}

}

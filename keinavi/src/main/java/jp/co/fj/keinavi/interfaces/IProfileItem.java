/*
 * 作成日: 2004/07/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.interfaces;

/**
 * プロファイルアイテムのIDを定義するインターフェース
 *
 * 2005.02.09 Yoshimoto KAWAI - Totec
 *            プロファイルバージョンのアイテムIDを追加
 *
 * 2009.10.20 Fujito URAKAWA - Totec
 *            新校内資料「科目・設問別正答率（レベル別）」対応
 *
 * @author kawai
 */
public interface IProfileItem {

	// --------------------------------------------------------------------------------
	/** プロファイルバージョン（共通設定項目） */
	String VERSION = "0001";
	/** 対象年度 */
	String EXAM_YEAR = "0002";
	/** 対象模試 */
	String EXAM_CD = "0003";
	// --------------------------------------------------------------------------------
	/** 型（共通設定項目） */
	String TYPE_COMMON = "0101";
	/** 型（個別設定項目） */
	String TYPE_IND = "0102";
	/** 型・共通項目設定利用 */
	String TYPE_USAGE = "0103";
	/** 型・型選択方式 */
	String TYPE_SELECTION = "0104";
	// --------------------------------------------------------------------------------
	/** 科目（共通設定項目） */
	String COURSE_COMMON = "0201";
	/** 科目（個別設定項目） */
	String COURSE_IND = "0202";
	/** 科目・共通項目設定利用 */
	String COURSE_USAGE = "0203";
	/** 科目・型選択方式 */
	String COURSE_SELECTION = "0204";
	// --------------------------------------------------------------------------------
	/** 志望大学 */
	String UNIV = "0301";
	/** 志望大学選択方法 */
	String UNIV_SELECTION = "0302";
	/** 志望大学（志望者数） */
	String UNIV_CHOICE = "0303";
	/** 志望大学（評価基準） */
	String UNIV_CRIT = "0304";
	/** 志望大学の表示順序 */
	String UNIV_CHOICE_ORDER = "0305";
	/** 志望大学表示 */
	String UNIV_DISP = "0306";
	/** 志望大学選択方式 */
	String UNIV_SELECTION_FORMULA = "0307";
	/** 国私区分 */
	String UNIV_DIV = "0308";
	/** 大学集計区分 */
	String UNIV_COUNTING_DIV = "0309";
	/** 大学の表示順序 */
	String UNIV_ORDER = "0310";
	/** 表示対象（志望大学）*/
	String UNIV_STUDENT = "0311";
	/** 日程 */
	String UNIV_SCHEDULE = "0312";
	/** 評価区分 */
	String UNIV_RATING = "0313";
	// --------------------------------------------------------------------------------
	/** 比較対象高校 */
	String SCHOOL_COMP_SCHOOL = "0401";
	/** 高校の表示順序 */
	String SCHOOL_ORDER = "0402";
	/** 他校比較 */
	String SCHOOL_COMP_OTHER = "0403";
	// --------------------------------------------------------------------------------
	/** 担当クラス */
	String CLASS = "0501";
	/** クラスの表示順序 */
	String CLASS_ORDER = "0502";
	/** クラス比較 */
	String CLASS_COMP = "0503";
	/** 比較対象クラス */
	String CLASS_COMP_CLASS = "0504";
	/** 比較対象クラス選択方式 */
	String CLASS_SELECTION = "0505";
	/** 担当クラスの年度 */
	String CLASS_YEAR = "0506";
	// --------------------------------------------------------------------------------
	/** 表 */
	String CHART = "0601";
	/** 表（構成比選択あり） */
	String CHART_COMP_RATIO = "0602";
	// --------------------------------------------------------------------------------
	/** グラフ */
	String GRAPH = "0701";
	/** 偏差値帯別構成比グラフ（偏差値ピッチ選択あり） */
	String GRAPH_COMP_RATIO = "0702";
	/** 偏差値帯別度数分布グラフ（軸選択あり） */
	String GRAPH_DIST = "0703";
	/** 偏差値帯別度数分布グラフ（軸選択なし） */
	String GRAPH_DIST_NO_AXIS = "0704";
	/** 偏差値帯別人数積み上げグラフ（偏差値ピッチ選択あり） */
	String GRAPH_BUILDUP = "0705";
	// --------------------------------------------------------------------------------
	/** 表示する学年 */
	String STUDENT_DISP_GRADE = "0801";
	/** 生徒の表示順序 */
	String STUDENT_ORDER = "0802";
	/** 選択中の生徒（分析モード） */
	String STUDENT_SELECTED = "0803";
	// --------------------------------------------------------------------------------
	/** 模試の種類 */
	String EXAM_TYPE = "0901";
	/** 比較対象模試 */
	String EXAM_COMP = "0902";
	// --------------------------------------------------------------------------------
	/** 偏差値 */
	String DEV = "1001";
	/** 偏差値範囲 */
	String DEV_RANGE = "1002";
	// --------------------------------------------------------------------------------
	/** 比較対象年度 */
	String PREV_COMP_YEAR = "1101";
	/** 過年度の表示 */
	String PREV_DISP = "1102";
	// --------------------------------------------------------------------------------
	/** 過回・過年度比較 */
	String PAST_COMP_PREV = "1201";
	/** 過回・クラス比較 */
	String PAST_COMP_CLASS = "1202";
	/** 過回・他校比較 */
	String PAST_COMP_OTHER = "1203";
	// --------------------------------------------------------------------------------
	/** 一括出力対象 */
	String PRINT_OUT_TARGET = "1301";
	/** 印刷対象 */
	String PRINT_TARGET = "1302";
	/** 印刷対象クラス */
	String PRINT_CLASS = "1303";
	/** 印刷対象生徒（判定対象生徒） */
	String PRINT_STUDENT = "1304";
	/** セキュリティスタンプ */
	String PRINT_STAMP = "1305";
	// --------------------------------------------------------------------------------
	/** ファイル形式 */
	String TEXT_FILE_FORMAT = "1401";
	/** ファイル出力対象 */
	String TEXT_OUT_TARGET = "1402";
	/** 出力項目 */
	String TEXT_OUT_ITEM = "1403";
	/** 出力フォーマット */
	String TEXT_OUT_FORMAT = "1404";
	// --------------------------------------------------------------------------------
	/** 型・科目別成績順位 */
	String IND_SCORE_RANK = "1501";
	/** 型・科目別比較 */
	String IND_COMP = "1502";
	/** 変動幅 */
	String IND_FLUCT_RANGE = "1503";
	/** マーク模試正答状況 */
	String IND_MARK_EXAM = "1504";
	/** マーク模試高校別設問成績層正答率 */
	String IND_MARK_EXAM_AREA = "1516";
	/** 個人成績表 */
	String IND_REPORT = "1505";
	/** 受験用資料 */
	String IND_EXAM_DOC = "1506";
	/** 成績分析面談シート */
	String IND_INTERVIEW_SHEET = "1507";
	/** １教科複数受験 */
	String IND_MULTI_EXAM = "1508";
	/** 面談用帳票 */
	String IND_INTERVIEW_FORMS = "1509";
	/** 条件保存されていない生徒の処理 */
	String IND_STUDENT_USAGE = "1510";
	/** 全体平均表示 */
	String IND_TOTAL_AVG = "1511";
	/** 現役全体平均表示 */
	String IND_ACTIVE_AVG = "1512";
	/** 高卒全体平均表示 */
	String IND_GRAD_AVG = "1513";
	/** 表示高校数 */
	String IND_SCHOOL_NUM = "1514";
	/** 上位校の＊印表示 */
	String IND_UPPER_MARK = "1515";
	// --------------------------------------------------------------------------------
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//	// 2019/07/17 QQ)Tanioka 共通テスト対応 ADD START
//	/** 共通テスト英語認定試験CEFR取得状況 */
//	String OPTION_CEFL = "1601";
//	/** 共通テスト英語認定試験CEFR取得状況(チェックあり) */
//	String OPTION_CHECK_BOX = "1602";
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END
	// --------------------------------------------------------------------------------
	/** 国語 評価別人数(記述式問題詳細分析) */
	String OPTION_CHECK_KOKUGO = "1701";
	/** 小設問別正答状況(記述式問題詳細分析) */
	String OPTION_CHECK_STATUS = "1702";
	/** 小設問別成績(記述式問題詳細分析) */
	String OPTION_CHECK_RECORD = "1703";
	// --------------------------------------------------------------------------------
	// 2019/07/17 QQ)Tanioka 共通テスト対応 ADD END

}

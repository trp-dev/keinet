/*
 * 作成日: 2004/10/19
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.txt_out;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.com_set.CompSchoolData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.data.sheet.ExcelName;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.KNUtil;

import org.apache.commons.lang.StringUtils;

/**
 * 2005.02.04 Yoshimoto KAWAI - Totec
 *            入試結果調査票データに対応
 *
 * 2005.02.09 Yoshimoto KAWAI - Totec
 *            模試別成績データ（新形式）に対応
 *
 * <2010年度マーク高２模試対応>
 * 2011.01.12   Tomohisa YAMADA - Totec
 *              倫政対応・マーク高２対応有
 *
 * @author 奥村ゆかり
 *
 */
public class OutPutParamSet {

	// 対象年度
	private String targetYear = null;
	// 対象模試
	private String targetExam = null;
	// 学校コード
	private String schoolCd = null;
	// ヘッダー情報
	private List header = null;

	// 他校コード
	private String OtherSchools = null;
	// 他校比較
	private int schoolDefFlg = 0;
	// クラス比較
	private int classDefFlg = 0;
	// 出力テキスト種別
	private int outType = 0;
	// 出力対象項目
	private Integer target[] = null;

	// 出力ファイル
	private File outPutFile = null;
	// 出力ファイル名
	private String fileName = null;

	// 出力日時フォーマッタ
	private static final SimpleDateFormat format =
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD START
//	    new SimpleDateFormat("yyyyMMddHHmmss");
	    new SimpleDateFormat("yyyyMMddHHmmssSSS");
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD END

	/**
	 * コンストラクタ
	 * @param login
	 * @param exam
	 * @param profile
	 * @param id
	 * @param sessionID
	 * @throws Exception
	 */
	public OutPutParamSet(LoginSession login, ExamData exam,Profile profile, String id, String sessionID) throws Exception{
		this(2, login, exam, profile, id, sessionID);
	}

	/**
	 * コンストラクタ
	 * @param typeVersion 方式バージョン：〜3
	 * @param login
	 * @param exam
	 * @param profile
	 * @param id
	 * @param sessionID
	 * @throws Exception
	 */
	public OutPutParamSet(int typeVersion, LoginSession login, ExamData exam,Profile profile, String id, String sessionID) throws Exception{

		// 対象年度
		this.targetYear = exam.getExamYear();
		// 対象模試
		this.targetExam = exam.getExamCD();
		// 学校コード
		this.schoolCd = login.getUserID();
		// ヘッダー情報
		this.header = new TextInfoBean(typeVersion, exam).getInfoList(typeVersion, id, exam);
		// 出力形式
		this.outType = 0;

		if ("t002".equals(id)) {
			// 他校比較
		    schoolDefFlg = getSchoolDefFlg(profile, "050201", exam);
		    // クラス比較
	        classDefFlg = getIntFlagValue((Short) profile.getItemMap("050201").get("0503"));
			// 出力形式（一括出力指定がなければ個別の出力形式を取得）
			outType =((Short) profile.getItemMap("050100").get("1402")).intValue();
			if (outType == 0) {
				outType =((Short) profile.getItemMap("050201").get("1402")).intValue();
			}
			// 出力対象項目
			target = getOutTarget(profile.getItemMap("050201").get("1403"));

		} else if ("t003".equals(id)) {

			// 他校比較
		    schoolDefFlg = getSchoolDefFlg(profile, "050202", exam);
		    // クラス比較
	        classDefFlg = getIntFlagValue((Short) profile.getItemMap("050202").get("0503"));
			// 出力形式
			// 出力形式（一括出力指定がなければ個別の出力形式を取得）
			outType =((Short) profile.getItemMap("050100").get("1402")).intValue();
			if (outType == 0) {
				outType =((Short) profile.getItemMap("050202").get("1402")).intValue();
			}
			// 出力対象項目
			target = getOutTarget(profile.getItemMap("050202").get("1403"));

		} else if ("t004".equals(id)) {

			// 他校比較
		    schoolDefFlg = getSchoolDefFlg(profile, "050203", exam);
		    // クラス比較
	        classDefFlg = getIntFlagValue((Short) profile.getItemMap("050203").get("0503"));
			// 出力形式（一括出力指定がなければ個別の出力形式を取得）
			outType =((Short) profile.getItemMap("050100").get("1402")).intValue();
			if (outType == 0) {
				outType =((Short) profile.getItemMap("050203").get("1402")).intValue();
			}
			// 出力対象項目
			target = getOutTarget(profile.getItemMap("050203").get("1403"));

		} else if ("t005".equals(id)) {
			// 他校比較
		    schoolDefFlg = getSchoolDefFlg(profile, "050204", exam);
		    // クラス比較
	        classDefFlg = getIntFlagValue((Short) profile.getItemMap("050204").get("0503"));
			// 出力形式（一括出力指定がなければ個別の出力形式を取得）
			outType =((Short) profile.getItemMap("050100").get("1402")).intValue();
			if (outType == 0) {
				outType =((Short) profile.getItemMap("050204").get("1402")).intValue();
			}
			// 出力対象項目
			target = getOutTarget(profile.getItemMap("050204").get("1403"));

		} else if ("t102".equals(id)) {
			// 出力形式（一括出力指定がなければ個別の出力形式を取得）
			outType =((Short) profile.getItemMap("050100").get("1402")).intValue();
			if (outType == 0) {
				outType =((Short) profile.getItemMap("050301").get("1402")).intValue();
			}
			// 出力対象項目
			target = getOutTarget(profile.getItemMap("050301").get("1403"));

		} else if ("t103".equals(id)) {
			// 出力形式（一括出力指定がなければ個別の出力形式を取得）
			outType =((Short) profile.getItemMap("050100").get("1402")).intValue();
			if (outType == 0) {
				outType =((Short) profile.getItemMap("050302").get("1402")).intValue();
			}
			// 出力対象項目
			target = getOutTarget(profile.getItemMap("050302").get("1403"));

		} else if ("t104".equals(id)) {
			// 出力形式（一括出力指定がなければ個別の出力形式を取得）
			outType =((Short) profile.getItemMap("050100").get("1402")).intValue();
			if (outType == 0) {
				outType =((Short) profile.getItemMap("050303").get("1402")).intValue();
			}
			// 出力対象項目
			target = getOutTarget(profile.getItemMap("050303").get("1403"));

// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD START
		} else if ("t106".equals(id)) {
			// 出力形式（一括出力指定がなければ個別の出力形式を取得）
			outType =((Short) profile.getItemMap("050100").get("1402")).intValue();
			if (outType == 0) {
				outType =((Short) profile.getItemMap("050304").get("1402")).intValue();
			}
			// 出力対象項目
			target = getOutTarget(profile.getItemMap("050304").get("1403"));
// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD END
		// 入試結果調査
		} else if ("t202".equals(id)) {
			outType = 1; // CSV固定
		}

		// 出力ファイルの設定
		fileName = this.getOutputFileName(id, exam, outType);
		outPutFile =
		    new File(
		        KNCommonProperty.getKNTempPath()
		        + File.separator + sessionID
		        + File.separator + fileName
	        );

		// 他校コード
		if (schoolDefFlg == 1) {
			OtherSchools = "";
			java.util.ArrayList data = (java.util.ArrayList)profile.getItemMap("010000").get("0401");
			for(int n=0; n<data.size(); n++){
				OtherSchools += "'"+((CompSchoolData)data.get(n)).getSchoolCD()+"'";
				if(n!= data.size() -1)OtherSchools += ",";
			}
		}
	}

// ADD START 2019/08/23 QQ)H.Nishiyama 共通テスト対応
	private String getTimeStamp() {
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD START
//		return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD END
	}
// ADD END   2019/08/23 QQ)H.Nishiyama 共通テスト対応

	/**
	 * 出力ファイル名を取得する
	 * @param id
	 * @param exam
	 * @param type
	 * @return
	 */
	private String getOutputFileName(String id, ExamData exam, int type) {
	    StringBuffer buff = new StringBuffer();

	    // 入試結果調査
	    if ("t202".equals(id)) {
	    	buff.append("ts_" + this.schoolCd);

	    // それ以外
	    } else {
		    // 識別名
// MOD START 2019/08/23 QQ)H.Nishiyama 共通テスト対応
//		    if ("t002".equals(id))  buff.append("SougouSeiseki");
//		    else if ("t003".equals(id))  buff.append("GakuryokuBunpu");
//		    else if ("t004".equals(id))  buff.append("SetsumonSeiseki");
//		    else if ("t005".equals(id))  buff.append("HyoukaNinzu");
//		    else if ("t102".equals(id))  buff.append("zbf610");
//		    else if ("t103".equals(id))  buff.append("zbf560");
//		    else if ("t104".equals(id))  buff.append("zbf611");
//		    else throw new IllegalArgumentException("不正なIDです。" + id);

			// プロパティファイルからファイル名のベースを取得
		    String fileName = ExcelName.getInstance().getName( id );

			// ファイル名がとれない場合はエラー
		    if ( StringUtils.isEmpty( fileName ) ) {
		    	throw new IllegalArgumentException("不正なIDです。" + id);
		    }

			// リプレース
		    fileName = fileName.replace( "##", exam.getExamCD() ) ;
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD START
//		    fileName = fileName.replace( "yyyymmddhhmmss", this.getTimeStamp() ) ;
		    fileName = fileName.replace( "yyyymmddhhmmssSSS", this.getTimeStamp() ) ;
// 2019/09/27 QQ)Tanioka ファイル名変更 UPD END

			// ファイル名をセット
		    buff.append( fileName );
// MOD END   2019/08/23 QQ)H.Nishiyama 共通テスト対応


// DEL START 2019/08/23 QQ)H.Nishiyama 共通テスト対応
//		    // 模試コード
//		    buff.append("_" + exam.getExamCD());
//		    // 出力日時
//		    buff.append("_" + format.format(new Date()));
// DEL END   2019/08/23 QQ)H.Nishiyama 共通テスト対応

	    }

	    // 拡張子
	    switch (type) {
    		case 1: buff.append(".csv"); break;
    		case 2: buff.append(".txt"); break;
    		default: throw new IllegalArgumentException("不正な出力タイプです。" + type);
	    }

	    return buff.toString();
	}


	/**
	 * フラグ値をintに変換して返す
	 * @param value
	 * @return
	 */
	private static int getIntFlagValue(final Short value) {
	    if (value == null) return 0;
	    else return value.intValue();
	}

	/**
	 * 出力項目の取得
	 * @return
	 * @throws IOException
	 */
	private Integer[] getOutTarget(Object str) {
		StringTokenizer stn = new StringTokenizer(str.toString(), ",");
		List list = new LinkedList();
		while (stn.hasMoreTokens()) {
			list.add( new Integer(stn.nextToken()));
		}

		return (Integer[]) list.toArray(new Integer[0]);
	}

	/**
	 * @param profile プロファイル
	 * @param category カテゴリID
	 * @param exam 模試データ
	 * @return 他校比較オプション値
	 */
	private int getSchoolDefFlg(final Profile profile,
			final String category, final ExamData exam) {

		// センターリサーチは「0」固定
		if (KNUtil.isCenterResearch(exam)) {
			return 0;
		// そうでなければプロファイル値
		} else {
			return getIntFlagValue((Short) profile.getItemMap(category).get("0403"));
		}
	}

	/**
	 * @return
	 */
	public int getClassDefFlg() {
		return classDefFlg;
	}

	/**
	 * @return
	 */
	public List getHeader() {
		return header;
	}

	/**
	 * @return
	 */
	public String getOtherSchools() {
		return OtherSchools;
	}

	/**
	 * @return
	 */
	public File getOutPutFile() {
		return outPutFile;
	}

	/**
	 * @return
	 */
	public int getOutType() {
		return outType;
	}

	/**
	 * @return
	 */
	public String getSchoolCd() {
		return schoolCd;
	}

	/**
	 * @return
	 */
	public int getSchoolDefFlg() {
		return schoolDefFlg;
	}



	/**
	 * @return
	 */
	public String getTargetExam() {
		return targetExam;
	}

	/**
	 * @return
	 */
	public String getTargetYear() {
		return targetYear;
	}

	/**
	 * @return
	 */
	public Integer[] getTarget() {
		return target;
	}

	/**
	 * @return
	 */
	public String getFileName() {
		return fileName;
	}

}
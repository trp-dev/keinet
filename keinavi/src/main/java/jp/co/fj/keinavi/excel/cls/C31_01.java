/**
 * クラス成績分析−−クラス比較　成績概況（クラス比較）
 * 	Excelファイル編集
 * 作成日: 2004/08/18
 * @author	Ito.Y
 */

package jp.co.fj.keinavi.excel.cls;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C31ClassListBean;
import jp.co.fj.keinavi.excel.data.cls.C31Item;
import jp.co.fj.keinavi.excel.data.cls.C31ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class C31_01 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
	private boolean bolBookCngFlg = true;			//改ファイルフラグ
	private boolean bolSheetCngFlg = true;			//改シートフラグ
//add end

	final private String masterfile0 = "C31_01";
	final private String masterfile1 = "NC31_01";
	private String masterfile = "";
	final private int intMaxSheetSr = 50;	//MAXシート数の値を入れる

/*
 * 	Excel編集メイン
 * 		C31Item c31Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
	public int c31_01EditExcel(C31Item c31Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (c31Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		int		intMaxSheetIndex	= 0;
		int		intFileIndex	= 1;
		
		// 基本ファイルを読込む
		C31ListBean c31ListBean = new C31ListBean();

		try {
			
			// データセット
			ArrayList c31List = c31Item.getC31List();
			Iterator itr = c31List.iterator();
			int row = 0;				//行
			int col = 0;				//列
			int classCnt = 0;			//クラスカウンタ
			int kmkCnt = 0;			//科目カウンタ
			int hyouCnt = 0;			//表カウンタ（0…上段、1…下段）
			String kmk = "";			//科目チェック用
			int maxClass = 0;			//MAXクラス数
			boolean kmkFlg = true;	//型→科目変更フラグ
			boolean kataExistFlg = false;	//型有無フラグ

//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
			int		sheetListIndex	= 0;		//シートカウンター
			int		houjiNum		= 41;		//1シートに表示できるクラス数(自校＋クラス)
			int		sheetRowCnt		= 0;		//クラス用改シート格納用カウンタ
			ArrayList	workbookList	= new ArrayList();
			ArrayList	workSheetList	= new ArrayList();
//add end
			
			while( itr.hasNext() ) {
				c31ListBean = (C31ListBean)itr.next();
				
				//型有無チェック 2004.11.17 add
				if (c31ListBean.getStrKmkCd().compareTo("7000") >= 0) {
					kataExistFlg = true;
				}

				// 基本ファイルを読込む
				C31ClassListBean c31ClassListBean = new C31ClassListBean();
				
				// クラスデータセット
				ArrayList c31ClassList = c31ListBean.getC31ClassList();
				Iterator itrClass = c31ClassList.iterator();
				
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
				maxClass = 0;
				maxClass = c31ClassList.size();
				
				// クラス表示に必要なシート数の計算
				sheetRowCnt = (maxClass-1)/(houjiNum-1);
				if((maxClass-1)%(houjiNum-1)!=0){
					sheetRowCnt++;
				}
				if (sheetRowCnt==0) {
					sheetRowCnt++;
				}
				
				//科目が変わる時のチェック
				if ( !cm.toString(kmk).equals(cm.toString(c31ListBean.getStrKmkCd())) && !cm.toString(kmk).equals("") ) {
					//クラス数が20以下
					if ((maxClass-1)<=(houjiNum-1)/2) {
						if (kmkFlg==true) {
							//型があるときのみ型→科目のチェックをする 2004.11.17 add
							if (kataExistFlg == true){
								//型から科目に変わる時のチェック
								if (c31ListBean.getStrKmkCd().compareTo("7000") < 0) {
									col = 0;
									kmkCnt = 0;
									kmkFlg = false;
									//クラス数が20以下のときの改表、改シート、改ファイル判断
									if (hyouCnt==0) {
										hyouCnt = 1;
									} else if (hyouCnt==1) {
										hyouCnt = 0;
										bolSheetCngFlg = true;
										if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
											bolBookCngFlg = true;
										}
									}
								}
							}else{
								kmkFlg = false;
							}
							//型が10のときの改表、改シート、改ファイル判断
							if (kmkCnt==10) {
								col = 0;
								kmkCnt = 0;
								if (hyouCnt==0) {
									hyouCnt = 1;
								} else if (hyouCnt==1) {
									hyouCnt = 0;
									bolSheetCngFlg = true;
									if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
										bolBookCngFlg = true;
									}
								}
							}
						} else {
							//科目が10のときの改表、改シート、改ファイル判断
							if (kmkCnt==10) {
								col = 0;
								kmkCnt = 0;
								if (hyouCnt==0) {
									hyouCnt = 1;
								} else if (hyouCnt==1) {
									hyouCnt = 0;
									bolSheetCngFlg = true;
									if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
										bolBookCngFlg = true;
									}
								}
							}
						}
					} else {
						if (kmkFlg==true) {
							//型があるときのみ型→科目のチェックをする 2004.11.17 add
							if (kataExistFlg == true){
								//型から科目に変わる時のチェック
								if (c31ListBean.getStrKmkCd().compareTo("7000") < 0) {
									col = 0;
									kmkCnt = 0;
									bolSheetCngFlg = true;
									kmkFlg = false;
									if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
										bolBookCngFlg = true;
									}
								}
							}else{
								kmkFlg = false;
							}
							//型が10のとき改シート
							if (kmkCnt==10) {
								col = 0;
								kmkCnt = 0;
								bolSheetCngFlg = true;
								if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
									bolBookCngFlg = true;
								}
							}
						} else {
							//科目が10のとき改シート
							if (kmkCnt==10) {
								col = 0;
								kmkCnt = 0;
								bolSheetCngFlg = true;
								if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
									bolBookCngFlg = true;
								}
							}
						}
						hyouCnt = 0;
					}
				}
//add end
				
				int ninzu = 0;
				float heikin = 0;
				float hensa = 0;
				
				while ( itrClass.hasNext() ){
					c31ClassListBean = (C31ClassListBean)itrClass.next();
					
					if( bolBookCngFlg == true ){
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
						if (maxClass<=houjiNum){
							if(intMaxSheetIndex >= intMaxSheetSr){
								// Excelファイル保存
								boolean bolRet = false;
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intFileIndex, masterfile, intMaxSheetIndex);
								intFileIndex++;
								if( bolRet == false ){
									return errfwrite;					
								}
							}
						}
//add end			
						//マスタExcel読み込み
						workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
						if( workbook==null ){
							return errfread;
						}
						intMaxSheetIndex=0;
						bolBookCngFlg = false;
						workbookList.add(workbook);
					}
					//クラスが20以下のときは20型･科目ごと、クラスが21以上のときは10型･科目ごとに改シート
					if ( bolSheetCngFlg ) {
						if ((maxClass-1)<=(houjiNum-1)/2) {
							// シートテンプレートのコピー
							workSheet = workbook.cloneSheet(0);
							intMaxSheetIndex++;
						}else{
							// データセットするシートの選択
							if (kmkCnt==0) {
								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);
								workSheetList.add(sheetListIndex, workSheet);
								intMaxSheetIndex++;
								sheetListIndex++;
							} else {
								//次列用シートの呼び出し
								workSheet = (HSSFSheet)workSheetList.get(sheetListIndex-sheetRowCnt);
								sheetRowCnt--;
							}
						}
						// ヘッダ右側に帳票作成日時を表示する
						cm.setHeader(workbook, workSheet);
					
						// セキュリティスタンプセット
						String secuFlg = cm.setSecurity( workbook, workSheet, c31Item.getIntSecuFlg() ,38 ,40 );
						workCell = cm.setCell( workSheet, workRow, workCell, 0, 38 );
						workCell.setCellValue(secuFlg);
					
						// 学校名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
						workCell.setCellValue( "学校名　：" + cm.toString(c31Item.getStrGakkomei()) );

						// 模試月取得
						String moshi =cm.setTaisyouMoshi( c31Item.getStrMshDate() );
						// 対象模試セット
						workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
						workCell.setCellValue( cm.getTargetExamLabel() + "：" + cm.toString(c31Item.getStrMshmei()) + moshi);
						hyouCnt = 0;
						bolSheetCngFlg = false;
					}
					if (classCnt==0) {
						if ( hyouCnt==0 ) {
							row = 4;
						}
						if ( hyouCnt==1 ) {
							row = 33;
						}
					
						if ( kmkCnt==0 || !cm.toString(kmk).equals(cm.toString(c31ListBean.getStrKmkCd())) ) {
							// 型・科目名セット
							workCell = cm.setCell( workSheet, workRow, workCell, row++, col+1 );
							workCell.setCellValue( c31ListBean.getStrKmkmei() );
							// 配点セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
							if ( !cm.toString(c31ListBean.getStrHaitenKmk()).equals("") ) {
								workCell.setCellValue( "（" + c31ListBean.getStrHaitenKmk() + "）" );
							}
						}
						if ( hyouCnt==0 ) {
							row = 7;
						}
						if ( hyouCnt==1 ) {
							row = 36;
						}
						if (col==0) {
							// 学校名セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col );
							workCell.setCellValue( c31Item.getStrGakkomei() );
						}
						// 人数セット
						ninzu = c31ClassListBean.getIntNinzu();
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						if ( c31ClassListBean.getIntNinzu() != -999 ) {
							workCell.setCellValue( c31ClassListBean.getIntNinzu() );
						}
						// 平均点セット
						heikin = c31ClassListBean.getFloHeikin();
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
						if ( c31ClassListBean.getFloHeikin() != -999.0 ) {
							workCell.setCellValue( c31ClassListBean.getFloHeikin() );
						}
						// 平均偏差値セット
						hensa = c31ClassListBean.getFloHensa();
						workCell = cm.setCell( workSheet, workRow, workCell, row++, col+4 );
						if ( c31ClassListBean.getFloHensa() != -999.0 ) {
							workCell.setCellValue( c31ClassListBean.getFloHensa() );
						}
					} else {
						if((classCnt-1)%((houjiNum-1)/2)==0 && classCnt!=1){
							if ((maxClass-1)<=(houjiNum-1)/2) {
								if((classCnt-1)%((houjiNum-1)/2)==0){
									row = 4;
								}else{
									row = 33;
								}
							}else{
								if((classCnt-1)%(houjiNum-1)==0){
									row = 4;
								}else{
									row = 33;
								}
							}

							if ( kmkCnt==0 || !cm.toString(kmk).equals(cm.toString(c31ListBean.getStrKmkCd())) ) {
								// 型・科目名セット
								workCell = cm.setCell( workSheet, workRow, workCell, row++, col+1 );
								workCell.setCellValue( c31ListBean.getStrKmkmei() );
								// 配点セット
								if ( !cm.toString(c31ListBean.getStrHaitenKmk()).equals("") ) {
									workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
									workCell.setCellValue( "（" + c31ListBean.getStrHaitenKmk() + "）" );
								}
							}
							if ((maxClass-1)<=(houjiNum-1)/2) {
								if((classCnt-1)%((houjiNum-1)/2)==0){
									row = 7;
								}else{
									row = 36;
								}
							}else{
								if((classCnt-1)%(houjiNum-1)==0){
									row = 7;
								}else{
									row = 36;
								}
							}
							if (col==0) {
								// 学校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, row, col );
								workCell.setCellValue( c31Item.getStrGakkomei() );
							}
							// 人数セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
							if ( ninzu != -999 ) {
								workCell.setCellValue( ninzu );
							}
							// 平均点セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
							if ( heikin != -999.0 ) {
								workCell.setCellValue( heikin );
							}
							// 平均偏差値セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col+4 );
							if ( hensa != -999.0 ) {
								workCell.setCellValue( hensa );
							}
							row++;
							hyouCnt = 1;
						}
						if (col==0) {
							// 学年+クラス名セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col );
							workCell.setCellValue( cm.toString(c31ClassListBean.getStrGrade())+"年 "+cm.toString(c31ClassListBean.getStrClass())+"クラス" );
						}
						// 人数セット
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						if ( c31ClassListBean.getIntNinzu() != -999 ) {
							workCell.setCellValue( c31ClassListBean.getIntNinzu() );
						}
						// 平均点セット
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
						if ( c31ClassListBean.getFloHeikin() != -999.0 ) {
							workCell.setCellValue( c31ClassListBean.getFloHeikin() );
						}
						// *セット
						if( hensa != -999.0 ){
							if ( c31ClassListBean.getFloHensa() != -999.0 ) {
								if ( c31ClassListBean.getFloHensa() < hensa ) {
									workCell = cm.setCell( workSheet, workRow, workCell, row, col+3 );
									workCell.setCellValue("*");
								}
							}
						}
						// 平均偏差値セット
						workCell = cm.setCell( workSheet, workRow, workCell, row++, col+4 );
						if ( c31ClassListBean.getFloHensa() != -999.0 ) {
							workCell.setCellValue( c31ClassListBean.getFloHensa() );
						}
					}
					classCnt++;
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
					if (maxClass>houjiNum){
						if((classCnt-1)%(houjiNum-1)==0 && classCnt!=1){
							//クラスが改シート、改ファイルでまたぐとき
							if(itrClass.hasNext()){
								if(kmkCnt==0){
									if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
										bolBookCngFlg = true;
									}
								}
								bolSheetCngFlg = true;
							}
						}
					}
//add end
				}
				classCnt = 0;
				kmkCnt++;
				col = col + 4;
				kmk = c31ListBean.getStrKmkCd();
				
//add 2004/10/13 T.Sakai クラスが40以上来たときの改シート、改ファイル対応
				if (maxClass>houjiNum){
					if ( kmkCnt==10 ) {
						col = 0;
						kmkCnt = 0;
					}
					bolSheetCngFlg = true;
					if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
						bolBookCngFlg = true;
					}
					// workbookListにWorkBookが２つ以上格納されている時は出力処理を実行
					int listSize = workbookList.size();
					if(listSize>1){
						if(kmkCnt == 0){
							workbook = (HSSFWorkbook)workbookList.get(0);

							// Excelファイル保存
							boolean bolRet = false;
							bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intFileIndex, masterfile, intMaxSheetSr);
							intFileIndex++;
							if( bolRet == false ){
								return errfwrite;
							}

							// ファイル出力したデータは削除
							workbookList.remove(0);

							// WorkBook・変数を書き込んでる途中のものに戻す
							workbook = (HSSFWorkbook)workbookList.get(0);
						}
					}
				}
//add end
			}
			
			// Excelファイル保存
			boolean bolRet = false;
			if(intFileIndex==1){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intFileIndex, masterfile, intMaxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("C31_01","データセットエラー",e.toString());
			return errfdata;
		}
		
		return noerror;
	}

}
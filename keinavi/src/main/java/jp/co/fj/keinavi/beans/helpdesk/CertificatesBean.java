/*
 * 作成日: 2004/09/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.helpdesk;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import jp.co.fj.keinavi.beans.csv.CSVLine;
import jp.co.fj.keinavi.beans.csv.CSVWriter;
import jp.co.fj.keinavi.forms.helpdesk.HD106Form;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.date.DateUtil;
import jp.co.fj.keinavi.util.date.ShortDateUtil;

import com.fjh.beans.DefaultBean;

/**
 * ユーザ管理（証明書発行）
 *
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CertificatesBean extends DefaultBean {

	HD106Form 	hd106Form = new HD106Form();	// HD103Form 情報

	private static String	 schoolCode		=null;		// 学校コード
	private static String	 firstPass		=null;		// 初期パスワード
	private static String	 cert_BeforeDate=null;		// 証明書有効期限

	private int  successUser	=0;			// 証明書発行されたユーザ
	private int  failureUser	=0;			// 証明書発行に失敗したユーザ

	// 失敗したユーザIDの一覧
	private List failureList = new ArrayList();


	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// TODO 自動生成されたメソッド・スタブ

		String[] userId   = hd106Form.getCertificates();	// 選択されたユーザID
		DateUtil date     = new ShortDateUtil();
		String   userName = null;		// ユーザ名
		String   certDay  = null; 		// 証明書有効期限までの日数
		String 	 certNumber = null;	// 証明書番号
		String   nowDate  = null; 		// 発行日付
		boolean judge	  = true;
		OsIf 	 os = new OsIf();

		for ( int userPos = 0; userPos < userId.length; userPos++ ) {

			// 判定
			judge = true;

			try {
				// ユーザIDで契約校マスタから学校コード、初期パスワードを取得
				// 登録済のはずなのに存在しなければ発行失敗
				String selUserId = SelectPactSchool(conn, userId[userPos]);
				if ( selUserId == null ) {
					judge = false;
				}

				//-----------------------------------------
				// 証明書番号、年月日(yyyymmdd)を取得
				//   証明書番号："yyyy999999
				//                   yyyy  :西暦年(4桁)
				//                   999999:シーケンシャル番号(6桁)
				//
				if ( judge ) {
					StringBuffer query = new StringBuffer();
					//// 西暦年度の場合は↓↓↓こちら
					////query.append("SELECT  TO_CHAR(ADD_MONTHS(TO_DATE(SYSDATE,'rrrr/mm/dd'),-3),'yyyy') "
					// 西暦年の場合は↓↓↓こちら
					query.append("SELECT  TO_CHAR(SYSDATE, 'yyyy') "
							   + "        || TRIM(TO_CHAR(CERTSIDCOUNT.NEXTVAL,'000000')) "
					           //+ "       ,TO_CHAR(SYSDATE,'yyyymmdd') "
					           + "       ,TO_CHAR(SYSDATE,'YYYYMMDDMI') "
							   + "  FROM DUAL ");
					PreparedStatement ps = null;
					ResultSet rs = null;
					try {
						ps = conn.prepareStatement(query.toString());
						rs = ps.executeQuery();
						if (rs.next()) {
							certNumber = rs.getString(1);
							nowDate    = rs.getString(2);
						}
					} finally {
						rs.close();
						ps.close();
					}
				}

				if (certNumber == null){
					judge = false;
				} else {
					// ユーザ名[ユーザID(8桁)]+[発行年月日(yyMMddmm: 8桁)]
					userName = userId[userPos] + nowDate.substring(2);
					// 証明書有効期限までの日数を取得
					certDay = getDay(nowDate, getCert_BeforeDate());
				}

				String csvFileName = null;
				if ( judge ) {
					// 証明書内容（/C=〜/EMAIL=）とパスワードは""で囲む

					// CSVファイル作成
					String pathName = getCertsCsvPath();		// CSVファイル作成パス
					//// テスト用パス
					////pathName = "D:\\Temp\\";
					csvFileName = pathName + userId[userPos].trim() + ".csv";
					FileOutputStream csvOutFile = new FileOutputStream(csvFileName);
					////CSVWriter writer = new CSVWriter(csvOutFile, "SHIFT_JIS" );		// テスト用
					CSVWriter writer = new CSVWriter(csvOutFile, "JIS" );
					CSVLine lineData = new CSVLine();
					lineData.addString(certNumber);
					lineData.addString("\""
									 + "C="      + Certificates.getInstance().getC()	// 国
					  				 + "/ST="    + Certificates.getInstance().getSt()	// 都道府県
					  				 + "/L="     + Certificates.getInstance().getL()	// 市町村
					  				 + "/O="     + Certificates.getInstance().getO()	// ドメイン名
					  				 + "/OU="    + Certificates.getInstance().getOu()	// システム名
					  				 + "/CN="    + userName								// ユーザ名
					  				 + "/EMAIL=" + Certificates.getInstance().getEmail()
					  				 + "\"" );
					lineData.addString("rsa");
					lineData.addString("1024");
					lineData.addString("1");
					lineData.addString("\"" + getFirstPass().trim() + "\"");			// パスワード
					writer.writeLine(lineData);
					writer.close();
				}

				// テスト（DOSコマンド実行）
				//   ＜確認：コマンド実行ＯＫ＞
				/*----
				try {
					String[] sCmd = new String[5];
					sCmd[0] = new String("command.com");
					sCmd[1] = new String("/c");
					sCmd[2] = new String("copy");
					sCmd[3] = new String(csvFileName);
					sCmd[4] = new String("D:\\Temp\\Test.csv");
					if ( os.exec(sCmd) > 0 ){
						judge = false;
					}
				}
				catch (Exception ex) {
					judge = false;
				}
				----*/

				//-----------------------------------------
				// 証明書発行
				// （例）
				//   "rsh  10.13.6.7 -l root /usr/local/aica/rshcert.sh  365   xxxxxxxx.csv  yyyy999999 yyyyXXXXXXXX "
				//         ---------         -------------------------- -----  ------------  ---------- ------------
				//          サーバ                    シェル          有効期限  CSVファイル  証明書番号 年度+ユーザID
				if ( judge ) {
					try {
						String path = KNCommonProperty.getCertsCsvPath();

						String[] sCmd = new String[9];
						sCmd[0] = new String("rsh");
						sCmd[1] = new String(getCertsServer());
						sCmd[2] = new String("-l");
						sCmd[3] = new String("root");
						sCmd[4] = new String("/usr/local/aica/rshcert.sh");
						sCmd[5] = new String(certDay);
						sCmd[6] = new String(csvFileName);
						sCmd[7] = new String(certNumber);
						sCmd[8] = new String(certNumber.substring(0,4) + userId[userPos]);

						if ( os.exec(sCmd) > 0 ){
							judge = false;
						}

						// エラーファイルがあるなら失敗
						File file = new File(path + "cert.err");
						if (file.exists()) {
							judge = false;
							file.delete();
						}

					} catch (Exception e) {
						e.printStackTrace();
						judge = false;
					}
				}

				//-----------------------------------------
				// 証明書ファイル名を取得
				//   ファイル名："yyyy999999.p12"
				//                   yyyy  :西暦年(4桁)
				//                   999999:シーケンシャル番号(6桁)
				//
				String p12FileName = getCertsP12Path() + certNumber + ".p12";


				//-----------------------------------------
				// [2004/10/29 nino] shell で一括実行のため不必要
				//-----------------------------------------
				// ファイルコピー
				//   yyyy999999.p12 → XXXXXXXX  (ユーザID)
				//
				//String cerFileName = getCertsFilePath() + userId[userPos];
				//if ( judge ) {
				//	try {
				//		String[] sCmd = new String[3];
				//		sCmd[0] = new String("cp");
				//		sCmd[1] = new String(p12FileName);
				//		sCmd[2] = new String(cerFileName);
				//		if ( os.exec(sCmd) > 0 ){
				//			judge = false;
				//		}
				//	}
				//	catch (Exception ex) {
				//		judge = false;
				//	}
				//}

				//-----------------------------------------
				// p12ファイルの削除（削除しなくてよい）
				//   yyyy999999.p12
				//
				//if ( judge ) {
				//	try {
				//		String[] sCmd = new String[2];
				//		sCmd[0] = new String("rm");
				//		sCmd[1] = new String(p12FileName);
				//		if ( os.exec(sCmd) > 0 ){
				//			judge = false;
				//		}
				//	}
				//	catch (Exception ex) {
				//		judge = false;
				//	}
				//}

				//-----------------------------------------
				// 契約校マスタのユーザ情報を登録
				// keys[ 0]ユーザID         [ 1]証明書情報CN       [ 2]証明書情報OU
				// keys[ 3]証明書情報O      [ 4]証明書情報L        [ 5]証明書情報S
				// keys[ 6]証明書情報C      [ 7]発行済有効期限     [ 8]更新日
				// keys[ 9]ログインパスワード
				if ( judge ) {
					String keys[] = new String[10];
					keys[ 0] = userId[userPos];
					keys[ 1] = userName;
					keys[ 2] = Certificates.getInstance().getOu();
					keys[ 3] = Certificates.getInstance().getO();
					keys[ 4] = Certificates.getInstance().getL();
					keys[ 5] = Certificates.getInstance().getSt();
					keys[ 6] = Certificates.getInstance().getC();
					keys[ 7] = getCert_BeforeDate();
					keys[ 8] = nowDate.substring(0, 8);
					keys[ 9] = getFirstPass().trim();
					judge = UpdatePactSchool(conn, keys);
				}

				//---- 判定 --------------------------------------
				if (judge) {
					successUser++;		// 証明書発行されたユーザ
				} else {
					failureUser++;		// 証明書発行に失敗したユーザ
					failureList.add(userId[userPos]);
				}


			} catch (SQLException ex) {
				throw ex;
			} catch (Exception ex) {
				throw ex;
			} finally{

			}

		}

	}

	/**
	 *
	 * 契約校マスタから学校コード、初期パスワード、有効期限を取得する
	 *
	 * @param con     DBコネクション
	 * @param userId  ユーザID
	 * @return count
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static String SelectPactSchool(Connection con, String UserId)
		throws SQLException {

		String USERID = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String query =
			"SELECT  NVL(USERID,     ' ') "
				+ " ,NVL(SCHOOLCD,   ' ') "
			    + " ,NVL(DEFAULTPWD, ' ') "
				+ " ,NVL(CERT_BEFOREDATA, ' ') "
				+ "  FROM  PACTSCHOOL     "
				+ " WHERE  USERID = ?     " ;
		try {
			stmt = con.prepareStatement(query);
			stmt.setString(1, UserId);

			rs = stmt.executeQuery();
			while (rs.next()) {
				USERID = (rs.getString(1));
				setSchoolCode(rs.getString(2));
				setFirstPass(rs.getString(3));
				setCert_BeforeDate(rs.getString(4));
			}
		} catch (SQLException ex) {
			throw new SQLException("契約校マスタの取得に失敗しました。");
		} catch (Exception ex) {
			throw new SQLException("契約校マスタの取得に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return USERID;
	}

	/**
	 *
	 * 契約校マスタを更新する
	 *
	 * @param con     DBコネクション
	 * @param keys[]  keys[0]〜[8]
	 * @return count
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static boolean UpdatePactSchool(Connection con, String keys[])
		throws SQLException {
		boolean judge = false;
		int count = 0;
		PreparedStatement stmt = null;
		String query = null;

		query =
			"UPDATE PACTSCHOOL SET         "
				+ "   CERT_CN           =? "		// [ 1]証明書情報CN
				+ "  ,CERT_OU           =? "		// [ 2]証明書情報OU
				+ "  ,CERT_O            =? "		// [ 3]証明書情報O
				+ "  ,CERT_L            =? "		// [ 4]証明書情報L
				+ "  ,CERT_S            =? "		// [ 5]証明書情報S
				+ "  ,CERT_C            =? "		// [ 6]証明書情報C
				+ "  ,ISSUED_BEFOREDATE =? " 		// [ 7]発行済有効期限
				+ "  ,CERT_FLG          ='1' " 		// [--]証明書有効フラグ(1固定)
			    + "  ,RENEWALDATE       =? " 		// [ 8]更新日
				+ "  ,LOGINPWD          =? " 		// [ 9]ログインパスワード
				+ "  ,DEFAULTPWD        =? " 		// [ 9]初期パスワード
				+ " WHERE  USERID       =? ";		// [ 0]ユーザID （キー）

		try {
			stmt = con.prepareStatement(query);
			stmt.setString( 1, keys[ 1]);
			stmt.setString( 2, keys[ 2]);
			stmt.setString( 3, keys[ 3]);
			stmt.setString( 4, keys[ 4]);
			stmt.setString( 5, keys[ 5]);
			stmt.setString( 6, keys[ 6]);
			stmt.setString( 7, keys[ 7]);
			stmt.setString( 8, keys[ 8]);
			stmt.setString( 9, keys[ 9]);
			stmt.setString(10, keys[ 9]);
			stmt.setString(11, keys[ 0]);

			count = stmt.executeUpdate();

			if (count == 0)
				throw new SQLException("契約校マスタのUPDATEに失敗しました。");

			judge = true;
		} catch (SQLException ex) {
			con.rollback();
			throw new SQLException("契約校マスタのUPDATEに失敗しました。");
		} catch (Exception ex) {
			con.rollback();
			throw new SQLException("契約校マスタのUPDATEに失敗しました。");
		} finally {
			con.commit();
			if (stmt != null)
				stmt.close();
		}
		return judge;
	}

	/**
	 * 開始日付から終了日付までの日数を取得
	 *
	 * @param   開始日付("yyyymmdd")
	 * @param   終了日付("yyyymmdd")
	 * @return  日数
	 *
	 */
	public String getDay(String sta, String end) {
		int intYY;
		int intMM;
		int intDD;

		// 開始日付
		intYY = Integer.valueOf(sta.substring(0,4)).intValue();
		intMM = Integer.valueOf(sta.substring(4,6)).intValue();
		intDD = Integer.valueOf(sta.substring(6,8)).intValue();
		GregorianCalendar gcsta = new GregorianCalendar(intYY, intMM-1, intDD);

		// 終了日付
		intYY = Integer.valueOf(end.substring(0,4)).intValue();
		intMM = Integer.valueOf(end.substring(4,6)).intValue();
		intDD = Integer.valueOf(end.substring(6,8)).intValue();
		GregorianCalendar gcend = new GregorianCalendar(intYY, intMM-1, intDD);

		long time = gcend.getTimeInMillis() - gcsta.getTimeInMillis();
		int   day = (int) (time / ((24*60*60)*1000) );

		return String.valueOf(day);
	}



	/**
	 * @return HD106Form
	 */
	public HD106Form getHD106Form() {
		return hd106Form;
	}
	/**
	 * @param string
	 */
	public void setHD106Form(HD106Form form) {
		hd106Form = form;
	}


	/**
	* 学校コード
	* @return
	*/
	public String getSchoolCode() {
		return schoolCode;
	}

	/**
	 * @param string
	 */
	public static void setSchoolCode(String string) {
		schoolCode = string;
	}

	/**
	* 初期パスワード
	* @return
	*/
	public String getFirstPass() {
		return firstPass;
	}

	/**
	 * @param string
	 */
	public static void setFirstPass(String string) {
		firstPass = string;
	}

	/**
	* 失敗したユーザIDの一覧
	* @return
	*/
	public List getFailureList() {
		return failureList;
	}

	/**
	 * @return
	 */
	public String getCertsServer() {
		String CertsServer = null;
		try {
			CertsServer = KNCommonProperty.getCertsServer();
		} catch (Exception e) {
		}
		if ( CertsServer == null ) {
			CertsServer = "10.13.6.7";
		}
		return CertsServer;
	}

	/**
	 * @return
	 */
	public String getCertsCsvPath() {
		String CertsCsvPath = null;
		try {
			CertsCsvPath = KNCommonProperty.getCertsCsvPath();	// CSVファイル作成パス
		} catch (Exception e) {
		}
		if ( CertsCsvPath == null ) {
			CertsCsvPath = "/";
		}
		return CertsCsvPath;
	}

	/**
	 * @return
	 */
	public String getCertsP12Path() {
		String CertsP12Path = null;
		try {
			CertsP12Path = KNCommonProperty.getCertsP12Path();
		} catch (Exception e) {
		}
		if ( CertsP12Path == null ) {
			CertsP12Path = "/";
		}
		return CertsP12Path;
	}

	/**
	 * @return
	 */
	public String getCertsFilePath() {
		String CertsFilePath = "/";
		try {
			CertsFilePath = KNCommonProperty.getCertsFilePath();
		} catch (Exception e) {
		}
		if ( CertsFilePath == null ) {
			CertsFilePath = "/";
		}
		return CertsFilePath;
	}

	/**
	 * @param List
	 */
	public void setFailureList(List list) {
		failureList = list;
	}

	/**
	 * @return
	 */
	public int getSuccessUser() {
		return successUser;
	}

	/**
	 * @param i
	 */
	public void setSuccessUser(int i) {
		successUser = i;
	}

	/**
	 * @return
	 */
	public int getFailureUser() {
		return failureUser;
	}

	/**
	 * @param i
	 */
	public void setFailureUser(int i) {
		failureUser = i;
	}

	/**
	 * @return
	 */
	public static String getCert_BeforeDate() {
		return cert_BeforeDate;
	}

	/**
	 * @param string
	 */
	public static void setCert_BeforeDate(String string) {
		cert_BeforeDate = string;
	}

}

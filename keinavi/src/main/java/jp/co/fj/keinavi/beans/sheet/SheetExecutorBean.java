package jp.co.fj.keinavi.beans.sheet;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.exam.ExamFilterManager;
import jp.co.fj.keinavi.beans.helpdesk.OutPutCertIssue;
import jp.co.fj.keinavi.beans.helpdesk.OutPutUserDelete;
import jp.co.fj.keinavi.beans.security.IMenuDefine;
import jp.co.fj.keinavi.beans.security.MenuSecurityBean;
import jp.co.fj.keinavi.beans.sheet.business.B00SheetBean;
import jp.co.fj.keinavi.beans.sheet.business.B41SheetBean;
import jp.co.fj.keinavi.beans.sheet.business.B42AbilitySheetBean;
import jp.co.fj.keinavi.beans.sheet.business.B42SheetBean;
import jp.co.fj.keinavi.beans.sheet.business.B43SheetBean;
import jp.co.fj.keinavi.beans.sheet.business.B44SheetBean;
import jp.co.fj.keinavi.beans.sheet.business.B45SheetBean;
import jp.co.fj.keinavi.beans.sheet.cls.C11SheetBean;
import jp.co.fj.keinavi.beans.sheet.cls.C12SheetBean;
import jp.co.fj.keinavi.beans.sheet.cls.C13SheetBean;
import jp.co.fj.keinavi.beans.sheet.cls.C14SheetBean;
import jp.co.fj.keinavi.beans.sheet.cls.C15SheetBean;
import jp.co.fj.keinavi.beans.sheet.cls.C31SheetBean;
import jp.co.fj.keinavi.beans.sheet.cls.C32SheetBean;
import jp.co.fj.keinavi.beans.sheet.cls.C33SheetBean;
import jp.co.fj.keinavi.beans.sheet.cls.C34SheetBean;
import jp.co.fj.keinavi.beans.sheet.individual.IndSheetExecutorBean;
import jp.co.fj.keinavi.beans.sheet.sales.SD206SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S01SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S11SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S12SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S13SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S21SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S22AbilitySheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S22SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S24SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S31SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S32SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S33SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S34SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S41SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S42AbilitySheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S42SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S43SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S44SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S51SheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S52AbilitySheetBean;
import jp.co.fj.keinavi.beans.sheet.school.S52SheetBean;
import jp.co.fj.keinavi.beans.sheet.user.U001SheetBean;
import jp.co.fj.keinavi.beans.txt_out.OutPutExamResultsResearch;
import jp.co.fj.keinavi.beans.txt_out.OutPutGakuryokuBunpu;
import jp.co.fj.keinavi.beans.txt_out.OutPutHyoukaNinzu;
import jp.co.fj.keinavi.beans.txt_out.OutPutSetsumonSeiseki;
import jp.co.fj.keinavi.beans.txt_out.OutPutSougouSeiseki;
import jp.co.fj.keinavi.beans.txt_out.OutPutSubAcademic;
import jp.co.fj.keinavi.beans.txt_out.OutPutZbf560;
import jp.co.fj.keinavi.beans.txt_out.OutputExamSeiseki;
import jp.co.fj.keinavi.beans.txt_out.TextInfoBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.data.sheet.SheetStatus;
import jp.co.fj.keinavi.forms.helpdesk.HD101Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.servlets.school.AbstractSServlet;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

import com.fjh.beans.DefaultBean;
import com.fjh.db.DBManager;

/**
 *
 * 帳票出力を実行するクラス
 *
 *
 * 2005.02.04	Yoshimoto KAWAI - Totec
 *           	テキスト出力・入試結果調査票データに対応
 *
 * 2005.02.09	Yoshimoto KAWAI - Totec
 *           	テキスト出力・模試別成績データ（新形式）に対応
 *
 * 2005.03.30	Yoshimoto KAWAI - Totec
 * 				新テスト対応
 *
 * 2005.11.01	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 * <2010年度マーク高２模試対応>
 * 2011.01.12   Tomohisa YAMADA - Totec
 *              倫政対応・マーク高２対応有
 *
 * @author kawai
 *
 */
public class SheetExecutorBean extends DefaultBean implements Runnable {

    private final SheetStatus status; // 出力状況
    private final String[] command; // アプレットパラメータ
    private final String backward; // 転送元
    private final String targetYear; // 対象年度
    private final String targetExam; // 対象模試

    private KNLog log; // ログインスタンス
    private String dbKey; // DBキー
    private HttpSession session; // セッション
    private String sessionkey; // セッションキー

    private Profile profile; // プロファイル
    private ExamData exam; // 対象模試データ
    private ExamSession examSession; // 模試セッション
    private LoginSession login; // ログインセッション

    private KNSheetLog sheetLog; // Logger
    private String remoteAddr; // IPアドレス
    private String remoteHost; // ホスト名

    private ArrayList univAll;
    private Map univInfoMap;

    /**
     * コマンド
     *
     * @param status
     * @param command
     */
    public SheetExecutorBean(final SheetStatus status, final String[] command) {
        this.status = status;
        this.command = command;
        this.backward = command[6]; // 転送元
        this.targetYear = command[2]; // 対象年度
        this.targetExam = command[3]; // 対象模試
    }

    /**
     * @see java.lang.Runnable#run()
     */
    public void run() {
        try {
            // アクセスログインスタンス
            sheetLog = new KNSheetLog(remoteAddr, remoteHost, login, backward, command[1]);
            // 帳票作成実行
            execute();
            // ログ出力
            sheetLog.output();
        } catch (final Throwable e) {
            status.setErrorCode(1);
            log.Err("", e.getMessage(), e);
        } finally {
            status.setFinish(true);
        }
    }

    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws Exception {
        try {

            // アクセスログインスタンス
            sheetLog = new KNSheetLog(remoteAddr, remoteHost, login, backward, command[1]);

            // 個人は個人用に任せる
            if (backward.startsWith("i")) {
                IndSheetExecutorBean bean = new IndSheetExecutorBean(this.status);
                Connection con = null;
                try {
                    con = DBManager.getConnectionPool(this.dbKey);
                    bean.setConnection(null, con);
                    bean.setCommand(command);
                    bean.setExamSession(examSession);
                    bean.setProfile(profile);
                    bean.setLogin(login);
                    bean.setUnivAll(univAll);
                    bean.setUnivInfoMap(univInfoMap);
                    bean.setSessionkey(sessionkey);
                    bean.setSession(session);
                    bean.setExam(exam);
                    bean.setLog(log);
                    bean.setSheetLog(sheetLog);
                    bean.execute();
                    con.commit();
                } catch (final Exception e) {
                    con.rollback();
                    log.Err(null, e.getMessage(), e);
                    return;
                } finally {
                    DBManager.releaseConnectionPool(this.dbKey, con);
                }

                if (!bean.createSheet()) {
                    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
                    //throw new Exception("帳票の作成に失敗しました。");
                    throw new IllegalStateException("帳票の作成に失敗しました。");
                    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
                }

                // 個人以外
            } else {
                // 帳票出力Beanの入れ物
                final List container = new LinkedList();
                // 帳票出力Beanを選択する
                selectSheetBean(container);

                if (container.size() > 0) {
                    status.setTotal(container.size());
                }

                // 枚数を計算
                double number = 0;
                for (final Iterator ite = container.iterator(); ite.hasNext();) {
                    final AbstractSheetBean bean = (AbstractSheetBean) ite.next();
                    bean.setAction(command[1]);
                    bean.setProfile(profile);
                    bean.setExamSession(examSession);
                    bean.setLogin(login);
                    bean.setOutfileList(status.getOutfileList());
                    bean.setSessionKey(sessionkey);
                    bean.setExam(exam);
                    bean.setPastExam1(command[4]);
                    bean.setPastExam2(command[5]);
                    bean.setLog(log);
                    bean.setSession(session);
                    bean.setBackward(backward);
                    bean.setSheetLog(sheetLog);
                    number += bean.calcNumberOfPrint();
                }
                status.setSheetNumber((int) Math.round(number));

                // 全て実行する
                for (final Iterator ite = container.iterator(); ite.hasNext();) {
                    AbstractSheetBean bean = (AbstractSheetBean) ite.next();
                    Connection con = null;
                    try {
                        con = DBManager.getConnectionPool(dbKey);
                        bean.setConnection(null, con);
                        bean.execute();
                        con.commit();
                        // 設問別成績がない場合の例外
                    } catch (final NoQuestionException e) {
                        con.rollback();
                        status.setErrorCode(3);
                        return;
                        // そのほかの例外
                    } catch (final Exception e) {
                        con.rollback();
                        status.setErrorCode(2);
                        log.Err(null, e.getMessage(), e);
                        return;
                    } finally {
                        DBManager.releaseConnectionPool(dbKey, con);
                    }

                    if (!bean.createSheet()) {
                        // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
                        //throw new Exception("帳票の作成に失敗しました。");
                        throw new IllegalStateException("帳票の作成に失敗しました。");
                        // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
                    }

                    status.countUp();
                }
            }
            // ログ出力
            sheetLog.output();
        } catch (final Throwable e) {
            status.setErrorCode(1);
            log.Err("", e.getMessage(), e);
        } finally {
            status.setFinish(true);
        }
    }

    /**
     * 出力対象になっている帳票出力Beanのインスタンスを選択する
     *
     * @param container
     */
    protected void selectSheetBean(final List container) {

        final MenuSecurityBean security = getMenuSecurityBean();

        if ("s000".equals(backward)) {
            container.add(new S01SheetBean());

        } else if ("s002".equals(backward)) {
            container.add(new S11SheetBean());

        } else if ("s003".equals(backward)) {
            container.add(new S12SheetBean());

        } else if ("s004".equals(backward)) {
            container.add(new S13SheetBean());

        } else if ("s102".equals(backward)) {
            container.add(new S21SheetBean());

        } else if ("s103".equals(backward)) {
            if (KNUtil.isAbilityExam(exam)) {
                container.add(new S22AbilitySheetBean());
            } else {
                container.add(new S22SheetBean());
            }

        } else if ("s104".equals(backward)) {
            container.add(new S24SheetBean());

        } else if ("s202".equals(backward)) {
            container.add(new S31SheetBean());

        } else if ("s203".equals(backward)) {
            container.add(new S32SheetBean());

        } else if ("s204".equals(backward)) {
            container.add(new S33SheetBean());

        } else if ("s205".equals(backward)) {
            container.add(new S34SheetBean());

        } else if ("s302".equals(backward)) {
            container.add(new S41SheetBean());

        } else if ("s303".equals(backward)) {
            if (KNUtil.isAbilityExam(exam)) {
                container.add(new S42AbilitySheetBean());
            } else {
                container.add(new S42SheetBean());
            }

        } else if ("s304".equals(backward)) {
            container.add(new S43SheetBean());

        } else if ("s305".equals(backward)) {
            container.add(new S44SheetBean());

        } else if ("s402".equals(backward)) {
            container.add(new S51SheetBean());

        } else if ("s403".equals(backward)) {
            if (KNUtil.isAbilityExam(exam)) {
                container.add(new S52AbilitySheetBean());
            } else {
                container.add(new S52SheetBean());
            }

        } else if ("b000".equals(backward)) {
            container.add(new B00SheetBean());

        } else if ("b002".equals(backward)) {
            container.add(new B41SheetBean());

        } else if ("b003".equals(backward)) {
            if (KNUtil.isAbilityExam(exam)) {
                container.add(new B42AbilitySheetBean());
            } else {
                container.add(new B42SheetBean());
            }

        } else if ("b004".equals(backward)) {
            container.add(new B43SheetBean());

        } else if ("b005".equals(backward)) {
            container.add(new B44SheetBean());

        } else if ("b102".equals(backward)) {
            container.add(new B45SheetBean());

        } else if ("c002".equals(backward)) {
            container.add(new C11SheetBean());

        } else if ("c003".equals(backward)) {
            container.add(new C12SheetBean());

        } else if ("c004".equals(backward)) {
            container.add(new C13SheetBean());

        } else if ("c005".equals(backward)) {
            container.add(new C14SheetBean());

        } else if ("c006".equals(backward)) {
            container.add(new C15SheetBean());

        } else if ("c102".equals(backward)) {
            container.add(new C31SheetBean());

        } else if ("c103".equals(backward)) {
            container.add(new C32SheetBean());

        } else if ("c104".equals(backward)) {
            container.add(new C33SheetBean());

        } else if ("c105".equals(backward)) {
            container.add(new C34SheetBean());

            // 一括出力（校内成績分析・一般）
        } else if (backward.startsWith("s") && AbstractSServlet.topSet.contains(backward)) {

            // 全国総合成績
            if (isOutput(IProfileCategory.SCORE_TOTAL) && ExamFilterManager.execute("s000", login, examSession, exam)) {
                container.add(new S01SheetBean());
            }

            // 校内成績・成績概況
            if (isOutput(IProfileCategory.S_SCHOOL_SCORE) && ExamFilterManager.execute("s002", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_SCORE)) {
                container.add(new S11SheetBean());
            }

            // 校内成績・偏差値分布
            if (isOutput(IProfileCategory.S_SCHOOL_DEV) && ExamFilterManager.execute("s003", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_SCORE)) {
                container.add(new S12SheetBean());
            }

            // 校内成績・設問別成績
            if (isOutput(IProfileCategory.S_SCHOOL_QUE) && ExamFilterManager.execute("s004", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_SCORE)) {
                container.add(new S13SheetBean());
            }

            // 過年度比較・成績概況
            if (isOutput(IProfileCategory.S_PREV_SCORE) && ExamFilterManager.execute("s102", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_PREV)) {
                container.add(new S21SheetBean());
            }

            // 過年度比較・偏差値分布
            if (isOutput(IProfileCategory.S_PREV_DEV) && ExamFilterManager.execute("s103", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_PREV)) {
                if (KNUtil.isAbilityExam(exam)) {
                    container.add(new S22AbilitySheetBean());
                } else {
                    container.add(new S22SheetBean());
                }
            }

            // 過年度比較・志望大学評価別人数
            if (isOutput(IProfileCategory.S_PREV_UNIV) && ExamFilterManager.execute("s104", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_PREV)) {
                container.add(new S24SheetBean());
            }

            // クラス比較・成績概況
            if (isOutput(IProfileCategory.S_CLASS_SCORE) && ExamFilterManager.execute("s202", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_CLASS)) {
                container.add(new S31SheetBean());
            }

            // クラス比較・偏差値分布
            if (isOutput(IProfileCategory.S_CLASS_DEV) && ExamFilterManager.execute("s203", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_CLASS)) {
                container.add(new S32SheetBean());
            }

            // クラス比較・設問別成績
            if (isOutput(IProfileCategory.S_CLASS_QUE) && ExamFilterManager.execute("s204", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_CLASS)) {
                container.add(new S33SheetBean());
            }

            // クラス比較・志望大学評価別人数
            if (isOutput(IProfileCategory.S_CLASS_UNIV) && ExamFilterManager.execute("s205", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_CLASS)) {
                container.add(new S34SheetBean());
            }

            // 他校比較・成績概況
            if (isOutput(IProfileCategory.S_OTHER_SCORE) && ExamFilterManager.execute("s302", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_OTHER)) {
                container.add(new S41SheetBean());
            }

            // 他校比較・偏差値分布
            if (isOutput(IProfileCategory.S_OTHER_DEV) && ExamFilterManager.execute("s303", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_OTHER)) {
                if (KNUtil.isAbilityExam(exam)) {
                    container.add(new S42AbilitySheetBean());
                } else {
                    container.add(new S42SheetBean());
                }
            }

            // 他校成績・設問別成績
            if (isOutput(IProfileCategory.S_OTHER_QUE) && ExamFilterManager.execute("s304", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_OTHER)) {
                container.add(new S43SheetBean());
            }

            // 他校比較・志望大学評価別人数
            if (isOutput(IProfileCategory.S_OTHER_UNIV) && ExamFilterManager.execute("s305", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_OTHER)) {
                container.add(new S44SheetBean());
            }

            // 過回比較・成績概況
            if (isOutput(IProfileCategory.S_PAST_SCORE)
                    && ExamFilterManager.execute("s402_top", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_PAST)) {
                container.add(new S51SheetBean());
            }

            // 過回比較・偏差値分布
            if (isOutput(IProfileCategory.S_PAST_DEV) && ExamFilterManager.execute("s403_top", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_SCHOOL_PAST)) {
                if (KNUtil.isAbilityExam(exam)) {
                    container.add(new S52AbilitySheetBean());
                } else {
                    container.add(new S52SheetBean());
                }
            }

            // 一括出力（クラス成績分析）
        } else if ("c001".equals(backward) || "c101".equals(backward)) {

            // クラス成績概況・偏差値分布
            if (isOutput(IProfileCategory.C_COND_DEV) && ExamFilterManager.execute("c002", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_CLASS_SCORE)) {
                container.add(new C11SheetBean());
            }

            // クラス成績概況・個人成績
            if (isOutput(IProfileCategory.C_COND_IND) && ExamFilterManager.execute("c003", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_CLASS_SCORE)) {
                container.add(new C12SheetBean());
            }

            // クラス成績概況・個人成績推移
            if (isOutput(IProfileCategory.C_COND_TRANS)
                    && ExamFilterManager.execute("c004_top", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_CLASS_SCORE)) {
                container.add(new C13SheetBean());
            }

            // クラス成績概況・設問別個人成績
            if (isOutput(IProfileCategory.C_COND_QUE) && ExamFilterManager.execute("c005", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_CLASS_SCORE)) {
                container.add(new C14SheetBean());
            }

            // クラス成績概況・志望大学別個人評価
            if (isOutput(IProfileCategory.C_COND_UNIV) && ExamFilterManager.execute("c006", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_CLASS_SCORE)) {
                container.add(new C15SheetBean());
            }

            // クラス比較・成績概況
            if (isOutput(IProfileCategory.C_COMP_SCORE) && ExamFilterManager.execute("c102", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_CLASS_COMP)) {
                container.add(new C31SheetBean());
            }

            // クラス比較・偏差値分布
            if (isOutput(IProfileCategory.C_COMP_DEV) && ExamFilterManager.execute("c103", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_CLASS_COMP)) {
                container.add(new C32SheetBean());
            }

            // クラス成績・設問別成績
            if (isOutput(IProfileCategory.C_COMP_QUE) && ExamFilterManager.execute("c104", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_CLASS_COMP)) {
                container.add(new C33SheetBean());
            }

            // クラス比較・志望大学評価別人数
            if (isOutput(IProfileCategory.C_COMP_UNIV) && ExamFilterManager.execute("c105", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_CLASS_COMP)) {
                container.add(new C34SheetBean());
            }

            // 一括出力（高校成績分析・営業）
        } else if ("b001".equals(backward) || "b101".equals(backward)) {

            // 全国総合成績
            if (isOutput(IProfileCategory.SCORE_TOTAL) && ExamFilterManager.execute("s000", login, examSession, exam)) {
                container.add(new B00SheetBean());
            }

            // 高校間比較・成績概況
            if (isOutput(IProfileCategory.S_OTHER_SCORE) && ExamFilterManager.execute("b002", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_BUSINESS_COMP)) {
                container.add(new B41SheetBean());
            }

            // 高校間比較・偏差値分布
            if (isOutput(IProfileCategory.S_OTHER_DEV) && ExamFilterManager.execute("b003", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_BUSINESS_COMP)) {
                if (KNUtil.isAbilityExam(exam)) {
                    container.add(new B42AbilitySheetBean());
                } else {
                    container.add(new B42SheetBean());
                }
            }

            // 高校間比較・設問別成績
            if (isOutput(IProfileCategory.S_OTHER_QUE) && ExamFilterManager.execute("b004", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_BUSINESS_COMP)) {
                container.add(new B43SheetBean());
            }

            // 高校間比較・志望大学評価別人数
            if (isOutput(IProfileCategory.S_OTHER_UNIV) && ExamFilterManager.execute("b005", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_BUSINESS_COMP)) {
                container.add(new B44SheetBean());
            }

            // 過回比較・成績概況
            if (isOutput(IProfileCategory.S_PAST_SCORE) && ExamFilterManager.execute("b102", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_BUSINESS_PAST)) {
                container.add(new B45SheetBean());
            }

            // テキスト出力：一括出力
        } else if ("t001".equals(backward) || "t101".equals(backward) || "t201".equals(backward)) {

            // テキスト出力：集計データ：成績概況
            if (isOutput(IProfileCategory.T_COUNTING_SCORE)
                    && ExamFilterManager.execute("t002", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_TEXT_SUM)) {
                container.add(new OutPutSougouSeiseki());
            }

            // テキスト出力：集計データ：偏差値分布
            if (isOutput(IProfileCategory.T_COUNTING_DEV) && ExamFilterManager.execute("t003", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_TEXT_SUM)) {
                container.add(new OutPutGakuryokuBunpu());
            }

            // テキスト出力：集計データ：設問別成績
            if (isOutput(IProfileCategory.T_COUNTING_QUE) && ExamFilterManager.execute("t004", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_TEXT_SUM)) {
                container.add(new OutPutSetsumonSeiseki());
            }

            // テキスト出力：集計データ：志望大学評価別人数
            if (isOutput(IProfileCategory.T_COUNTING_UNIV)
                    && ExamFilterManager.execute("t005", login, examSession, exam)
                    && security.isValid(IMenuDefine.MENU_TEXT_SUM)) {
                container.add(new OutPutHyoukaNinzu());
            }

            // テキスト出力：個人データ：個人設問別成績
            if (isOutput(IProfileCategory.T_IND_QUE) && ExamFilterManager.execute("t103", login, examSession, exam)
                    && security.isValid("504")) {
                container.add(new OutPutZbf560());
            }

            // テキスト出力：個人データ：個人模試成績データ（新形式）
            if (isOutput(IProfileCategory.T_IND_EXAM2) && ExamFilterManager.execute("t104", login, examSession, exam)
                    && security.isValid("504")) {
                container.add(getOutputExamSeiseki());
            }

// 2019/09/18 QQ)Tanioka （記述系模試）学力要素別成績データ帳票追加 ADD START
            // テキスト出力：個人データ：個人模試成績データ（新形式）
            if (isOutput(IProfileCategory.T_IND_DESCEXAM) && ExamFilterManager.execute("t106", login, examSession, exam)
                    && security.isValid("504")) {
                container.add(new OutPutSubAcademic());
            }
// 2019/09/18 QQ)Tanioka （記述系模試）学力要素別成績データ帳票追加 ADD END

            // テキスト出力：成績概況
        } else if ("t002".equals(backward)) {
            container.add(new OutPutSougouSeiseki());

            // テキスト出力：偏差値分布
        } else if ("t003".equals(backward)) {
            container.add(new OutPutGakuryokuBunpu());

            // テキスト出力：設問別成績
        } else if ("t004".equals(backward)) {
            container.add(new OutPutSetsumonSeiseki());

            // テキスト出力：志望大学評価別人数
        } else if ("t005".equals(backward)) {
            container.add(new OutPutHyoukaNinzu());

            // テキスト出力：個人模試別成績データ（旧形式）
        } else if ("t102".equals(backward)) {
            container.add(new OutputExamSeiseki());

            // テキスト出力：個人設問別成績
        } else if ("t103".equals(backward)) {
            container.add(new OutPutZbf560());

            // テキスト出力：個人模試別成績データ（新形式）
        } else if ("t104".equals(backward)) {
            container.add(getOutputExamSeiseki());

// 2019/09/18 QQ)Tanioka （記述系模試）学力要素別成績データ帳票追加 ADD START
            // テキスト出力：（記述系模試）学力要素別成績データ
        } else if ("t106".equals(backward)) {
            container.add(new OutPutSubAcademic());
// 2019/09/18 QQ)Tanioka （記述系模試）学力要素別成績データ帳票追加 ADD END
            // テキスト出力：入試結果調査
        } else if ("t202".equals(backward)) {
            container.add(new OutPutExamResultsResearch());

            // 利用者通知
        } else if ("u001".equals(backward)) {
            container.add(new U001SheetBean());
            // パスワード通知
        } else if ("sd206".equals(backward)) {
            container.add(new SD206SheetBean());
        }
        // 2015/10/22 QQ)Nishiyama デジタル証明書対応 ADD START
        else if ("hd101".equals(backward)) {

            // ヘルプデスク_一覧画面の入力情報を取得
            HD101Form hd101Form = (HD101Form) session.getAttribute("hd101Form");

            // ユーザ削除
            if ("2".equals(hd101Form.getContent())) {
                container.add(new OutPutUserDelete());
                // 証明書・ユーザ登録
            } else {
                container.add(new OutPutCertIssue());
            }
        }
        // 2015/10/22 QQ)Nishiyama デジタル証明書対応 ADD END
    }

    /**
     * テキスト出力：個人模試成績データ用シートBeanを返す
     * @return
     */
    protected AbstractSheetBean getOutputExamSeiseki() {
        return TextInfoBean.getOutputExamSeiseki(exam);
    }

    /**
     * 一括出力対象かどうか
     * @return
     */
    protected boolean isOutput(String key) {
        return new Short((short) 1).equals((Short) profile.getItemMap(key).get(IProfileItem.PRINT_OUT_TARGET));
    }

    /**
     * @param session
     */
    public void setExamSession(ExamSession session) {

        examSession = session;

        // 模試データを取得する
        if (examSession != null) {
            exam = examSession.getExamData(targetYear, targetExam);
        }
    }

    protected MenuSecurityBean getMenuSecurityBean() {
        return (MenuSecurityBean) session.getAttribute(MenuSecurityBean.SESSION_KEY);
    }

    /**
     * @return session を戻します。
     */
    protected HttpSession getSession() {
        return session;
    }

    /**
     * @return backward を戻します。
     */
    protected String getBackward() {
        return backward;
    }

    /**
     * @return login を戻します。
     */
    protected LoginSession getLoginSession() {
        return login;
    }

    /**
     * @return examSession を戻します。
     */
    protected ExamSession getExamSession() {
        return examSession;
    }

    /**
     * @return exam を戻します。
     */
    protected ExamData getExam() {
        return exam;
    }

    /**
     * @param list
     */
    public void setUnivAll(ArrayList list) {
        univAll = list;
    }

    /**
     * @param session
     */
    public void setLogin(LoginSession session) {
        login = session;
    }

    /**
     * @param profile
     */
    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    /**
     * @param session
     */
    public void setSession(HttpSession session) {
        this.session = session;
    }

    /**
     * @param log
     */
    public void setLog(KNLog log) {
        this.log = log;
    }

    /**
     * @param string
     */
    public void setDbKey(String string) {
        dbKey = string;
    }

    /**
     * @param string
     */
    public void setSessionkey(String string) {
        sessionkey = string;
    }

    /**
     * @param pRemoteAddr 設定する remoteAddr。
     */
    public void setRemoteAddr(String pRemoteAddr) {
        remoteAddr = pRemoteAddr;
    }

    /**
     * @param pRemoteHost 設定する remoteHost。
     */
    public void setRemoteHost(String pRemoteHost) {
        remoteHost = pRemoteHost;
    }

    /**
     * @param map
     */
    public void setUnivInfoMap(Map map) {
        univInfoMap = map;
    }

}

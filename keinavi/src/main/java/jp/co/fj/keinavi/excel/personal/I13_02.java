/**
 * 個人成績分析−面談シート　連続個人成績表（３年連個）
 * 	Excelファイル編集
 * 作成日: 2004/09/14
 * @author	Ito.Y
 *
 * 2006/09/05   Totec)T.Yamada      [1]受験学力測定テスト対応
 *
 * 2009.11.30   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 */

package jp.co.fj.keinavi.excel.personal;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.personal.I11Item;
import jp.co.fj.keinavi.excel.data.personal.I11ListBean;
import jp.co.fj.keinavi.excel.data.personal.I13HyoukaListBean;
import jp.co.fj.keinavi.excel.data.personal.I13KyokaSeisekiListBean;
import jp.co.fj.keinavi.excel.data.personal.I13SeisekiListBean;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class I13_02 {

    private int	noerror		= 0;	// 正常終了
    private int	errfread	= 1;	// ファイルreadエラー
    private int	errfwrite	= 2;	// ファイルwriteエラー
    private int	errfdata	= 3;	// データ設定エラー

    private CM		cm			= new CM();	// 共通関数用クラス インスタンス

    //2005.04.26 Add 新テスト対応
    //final private String	masterfile		= "I13_02";	// ファイル名
    final private String 	masterfile0 	= "I13_02";	// ファイル名１(新テスト対応用)
    final private String 	masterfile1 	= "I13_02";	// ファイル名２(新テスト対応用)
    private String masterfile = "";

    final private String	cntMshCd 		= "38";		// センター模試コード

/**
 * 	Excel編集メイン
 * 		I11Item i11Item: データクラス
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 * 		String	UserID：ユーザーID
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
     public int i13_02EditExcel(I11Item i11Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
        KNLog log = KNLog.getInstance(null,null,null);

        //2005.04.26 Add 新テスト対応
        //テンプレートの決定
        if (i11Item.getIntSeisekiShubetsuFlg() == 1){
            masterfile = masterfile1;
        } else{
            masterfile = masterfile0;
        }

        try {

            // データセット
            ArrayList	i11List			= i11Item.getI11List();
            Iterator	itr				= i11List.iterator();

            int		maxSheetIndex	= 0;	// シートカウンター

            // マスタExcel読み込み
            HSSFWorkbook	workbook	= null;
            HSSFSheet		workSheet	= null;
            HSSFRow			workRow		= null;
            HSSFCell		workCell	= null;

            // 基本ファイルを読込む
            I11ListBean i11ListBean = new I11ListBean();

            /** データリスト **/
            while( itr.hasNext() ) {
                i11ListBean = (I11ListBean) itr.next();

                // マスタExcel読み込み
                workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
                if( workbook==null ){
                    return errfread;
                }

                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
		//2019/09/24 QQ)Oosaki 共通テスト対応 ADD START
		//セルスタイルを作成
//		HSSFCellStyle CellStyle = this.createStyle(workbook);
		//2019/09/24 QQ)Oosaki 共通テスト対応 ADD END
		// 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END

                // シートテンプレートのコピー
                workSheet = workbook.cloneSheet(0);
                maxSheetIndex++;

                // ヘッダ右側に帳票作成日時を表示する
                cm.setHeader(workbook, workSheet);

                // セキュリティスタンプセット
                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntSeisekiSecuFlg() ,64 ,65 );
                workCell = cm.setCell( workSheet, workRow, workCell, 0, 64 );
                //String secuFlg = cm.setSecurity( workbook, workSheet, i11Item.getIntSeisekiSecuFlg() ,68 ,69 );
                //workCell = cm.setCell( workSheet, workRow, workCell, 0, 68 );
                //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                workCell.setCellValue(secuFlg);

                // 学校名セット
                workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
                workCell.setCellValue( "学校名　：" + cm.toString(i11ListBean.getStrGakkomei()) + "　学年：" + cm.toString(i11ListBean.getStrGrade())
                + "　クラス名：" + cm.toString(i11ListBean.getStrClass()) + "　クラス番号：" + cm.toString(i11ListBean.getStrClassNum()) );

                // 氏名・性別セット
                String strSex = "";
                if( cm.toString(i11ListBean.getStrSex()).equals("1") ){
                    strSex = "男";
                }else if( cm.toString(i11ListBean.getStrSex()).equals("2") ){
                    strSex = "女";
                }else{
                    strSex = "不明";
                }
                workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );

                if( !cm.toString(i11ListBean.getStrShimei()).equals("")){
                    workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrShimei()) + "（" + cm.toString(i11ListBean.getStrKana()) + "）　性別：" + strSex );
                }else{
                    workCell.setCellValue( "氏名　　：" + cm.toString(i11ListBean.getStrKana()) + "　性別：" + strSex );
                }

                //受験学力測定テストの注釈
                boolean isAbilityExist = false;

                final Iterator ite = i11ListBean.getI13SeisekiList().iterator();

                while (ite.hasNext()) {

                    final I13SeisekiListBean seiseki =
                        (I13SeisekiListBean) ite.next();

                    if (KNUtil.TYPECD_ABILITY.equals(seiseki.getStrMshTypeCd())) {
                        isAbilityExist = true;
                    }
                }

                //模試一覧に受験学力測定が存在する
                if (isAbilityExist) {
                    cm.setCell( workSheet, workRow, workCell, 2, 54).setCellValue("※受験学力測定テストで国語2科目受験の場合、理科の欄に国語2科目めが表示されます。");
                }

                /** 連続個人成績表リスト **/
                // データセット
                ArrayList i13SeisekiList = i11ListBean.getI13SeisekiList();
                Iterator itrI13Seiseki = i13SeisekiList.iterator();

                int kyokaRow = 0;
                int moshiRow = 0;
                boolean centerReFlg = false;
                while(itrI13Seiseki.hasNext()){
                    I13SeisekiListBean i13seisekiListBean = (I13SeisekiListBean)itrI13Seiseki.next();

                    //センターリサーチ用
                    if(cm.toString(i13seisekiListBean.getStrMshCd()).equals(cntMshCd)){
                        centerReFlg = true;

                        // 対象模試セット
                        String moshi = cm.setTaisyouMoshi( i13seisekiListBean.getStrMshDate() );	// 模試月取得
                        workCell = cm.setCell( workSheet, workRow, workCell, 5, 0 );
                        workCell.setCellValue( cm.toString(i13seisekiListBean.getStrMshmei()) + moshi );

                        /** 教科別成績リスト **/
                        // データセット
                        ArrayList i13KyokaSeisekiList = i13seisekiListBean.getI13KyokaSeisekiList();
                        Iterator itrI13KyokaSeiseki = i13KyokaSeisekiList.iterator();

                        String strKykHoji = "";
                        int kyokaTabCol = 0;
                        int kyokaCol = 0;
                        int kyoukaCnt = 0;
                        while(itrI13KyokaSeiseki.hasNext()){
                            I13KyokaSeisekiListBean i13KyokaSeisekiListBean = (I13KyokaSeisekiListBean)itrI13KyokaSeiseki.next();

                            // 教科コードによって判定 → 2005.01.06 小論文は出力しない
                            String strKyouka = i13KyokaSeisekiListBean.getStrKyokaCd()+" ";
                            strKyouka = strKyouka.substring(0,1);
                            if( strKyouka.equals("6") == false ){
                                if( !strKyouka.equals(strKykHoji) ){
                                    switch( Integer.parseInt( strKyouka ) ){
                                        case 1:	//英語
                                            kyokaTabCol = 4;
                                            kyoukaCnt = 3;
                                            break;
                                        case 2:	//数学
                                            kyokaTabCol = 16;
                                            kyoukaCnt = 3;
                                            break;
                                        case 3:	//国語
                                            kyokaTabCol = 28;
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                            kyoukaCnt = 1;
                                            //kyoukaCnt = 2;
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                            break;
                                        case 4:	//理科
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                            kyokaTabCol = 32;
                                            //kyokaTabCol = 36;
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                            kyoukaCnt = 3;
                                            break;
                                        case 5:	//地歴公民
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                            kyokaTabCol = 44;
                                            //kyokaTabCol = 48;
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                            kyoukaCnt = 2;
                                            break;
                                        case 7:	//総合
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                            kyokaTabCol = 52;
                                            //kyokaTabCol = 56;
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                            kyoukaCnt = 1;
                                            break;
                                    }
                                    kyokaCol = 0;
                                    strKykHoji = strKyouka;
                                }
                                if (kyoukaCnt>0) {
                                    //理科の先頭科目が基礎科目なら、先頭は空欄にする
                                    if ("4".equals(strKyouka) && kyokaCol == 0 && "1".equals(i13KyokaSeisekiListBean.getBasicFlg())) {
                                        kyokaCol++;
                                    }

                                    // 科目名セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 5, kyokaTabCol+4*kyokaCol );
                                    workCell.setCellValue( i13KyokaSeisekiListBean.getStrKmkmei() );

                                    if (Integer.parseInt( strKyouka ) == 7){
                                        // 得点セット
                                        if(i13KyokaSeisekiListBean.getIntTokuten() != -999){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 7, kyokaTabCol+3*kyokaCol );
                                            workCell.setCellValue( i13KyokaSeisekiListBean.getIntTokuten() );
                                        }
                                        // 偏差値セット
                                        if(i13KyokaSeisekiListBean.getFloHensa() != -999.0){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 7, kyokaTabCol+3*kyokaCol+1 );
                                            workCell.setCellValue( i13KyokaSeisekiListBean.getFloHensa() );
                                        }

                                        // 学力レベルセット
                                        if( !"".equals(i13KyokaSeisekiListBean.getStrScholarLevel())){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 7, kyokaTabCol+3*kyokaCol+2 );
                                            workCell.setCellValue( i13KyokaSeisekiListBean.getStrScholarLevel() );
                                        }
                                    } else {
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
                                        //2019/09/24 QQ)Oosaki 共通テスト対応 ADD START
//                                        if(("3915").equals(i13KyokaSeisekiListBean.getSubCd())) {
//                                            workCell = cm.setCell( workSheet, workRow, workCell, 7, kyokaTabCol+4*kyokaCol+1 );
//                                            // 出力する際セルの中央に寄せる
//                                            workCell.setCellStyle(CellStyle);
//                                            workCell.setCellValue( i13KyokaSeisekiListBean.getKokugoTokuten() );
//                                        } else {
                                        //2019/09/24 QQ)Oosaki 共通テスト対応 ADD END
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END
                                            // 得点セット
                                            if(i13KyokaSeisekiListBean.getIntTokuten() != -999){
                                                workCell = cm.setCell( workSheet, workRow, workCell, 7, kyokaTabCol+4*kyokaCol+1 );
                                                workCell.setCellValue( i13KyokaSeisekiListBean.getIntTokuten() );
                                            }
                                            // 偏差値セット
                                            if(i13KyokaSeisekiListBean.getFloHensa() != -999.0){
                                                workCell = cm.setCell( workSheet, workRow, workCell, 7, kyokaTabCol+4*kyokaCol+2 );
                                                workCell.setCellValue( i13KyokaSeisekiListBean.getFloHensa() );
                                            }

                                            // 学力レベルセット
                                            if( !"".equals(i13KyokaSeisekiListBean.getStrScholarLevel())){
                                                workCell = cm.setCell( workSheet, workRow, workCell, 7, kyokaTabCol+4*kyokaCol+3 );
                                                workCell.setCellValue( i13KyokaSeisekiListBean.getStrScholarLevel() );
                                            }
                                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 ADD START
                                            //CEFR出力
//                                            if(("01").equals(i13KyokaSeisekiListBean.getExcdtype())) {
//                                                    workCell = cm.setCell( workSheet, workRow, workCell, 5, 12 );
//                                                workCell.setCellValue( "CEFR" );
//                                                workCell = cm.setCell( workSheet, workRow, workCell, 7, 13 );
//                                                workCell.setCellStyle(CellStyle);
//                                                workCell.setCellValue( i13KyokaSeisekiListBean.getCefrScore() );
//                                            }
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 ADD END
                                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
//                                        }
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END
                                    }
                                }
                                kyoukaCnt--;
                                kyokaCol++;
                            }
                        }
                        //2005.01.13 Update センターリサーチのデータにも志望大学別評価データリストを表示する
                        /** 志望大学別評価データリスト **/
                        // データセット
                        ArrayList i13HyoukaList = i13seisekiListBean.getI13HyoukaList();
                        Iterator itrI13Hyouka = i13HyoukaList.iterator();

                        int shiboRow = 0;
                        int shiboCol = 0;
                        while(itrI13Hyouka.hasNext()){
                            I13HyoukaListBean i13HyoukaListBean = (I13HyoukaListBean)itrI13Hyouka.next();

                            // 大学名セット
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                            workCell = cm.setCell( workSheet, workRow, workCell, 4+shiboRow+4*kyokaRow, 57+5*shiboCol );
                            //workCell = cm.setCell( workSheet, workRow, workCell, 4+shiboRow+4*kyokaRow, 61+5*shiboCol );
                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                            workCell.setCellValue( i13HyoukaListBean.getStrDaigakuMei() );

                            // 学部名セット
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                            workCell = cm.setCell( workSheet, workRow, workCell, 4+shiboRow+4*kyokaRow, 58+5*shiboCol );
                            //workCell = cm.setCell( workSheet, workRow, workCell, 4+shiboRow+4*kyokaRow, 62+5*shiboCol );
                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                            workCell.setCellValue( i13HyoukaListBean.getStrGakubuMei() );

                            // 学科名セット
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                            workCell = cm.setCell( workSheet, workRow, workCell, 4+shiboRow+4*kyokaRow, 59+5*shiboCol );
                            //workCell = cm.setCell( workSheet, workRow, workCell, 4+shiboRow+4*kyokaRow, 63+5*shiboCol );
                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                            workCell.setCellValue( i13HyoukaListBean.getStrGakkaMei() );

                            // 評価セット
                            if( cm.toString(i13HyoukaListBean.getStrHyoukaMark()).equals("") &&
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                                //2019/09/24 QQ)Oosaki 共通テスト対応 ADD START
                                //cm.toString(i13HyoukaListBean.getStrHyoukaMark_engpt()).equals("") &&
                                //2019/09/24 QQ)Oosaki 共通テスト対応 ADD END
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
                                cm.toString(i13HyoukaListBean.getStrHyoukaKijyutsu()).equals("") &&
                                cm.toString(i13HyoukaListBean.getStrHyoukaAll()).equals("") ){
                            }else{

                                String hyoka = "";
                                if( cm.toString(i13HyoukaListBean.getStrHyoukaMark()).equals("") == false ){
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                    //hyoka = i13HyoukaListBean.getStrHyoukaMark() + ":" ;
                                    hyoka = cm.padRight(i13HyoukaListBean.getStrHyoukaMark(), "UTF-8", 2) + ":" ;
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                }else{
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                    //hyoka = "　:" ;
                                    hyoka = "  :" ;
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                }

                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                                //2019/09/24 QQ)Oosaki 共通テスト対応 ADD START
//                                if( cm.toString(i13HyoukaListBean.getStrHyoukaMark_engpt()).equals("")  == false ){
//                                    hyoka += cm.padRight(i13HyoukaListBean.getStrHyoukaMark_engpt(), "UTF-8", 2);
//                                }else{
//                                    hyoka += "  ";
//                                }
//                                if( cm.toString(i13HyoukaListBean.getHyoukaJodge()).equals("")  == false ){
//                                    hyoka += " " + i13HyoukaListBean.getHyoukaJodge();
//                                }else{
//                                    hyoka += "   ";
//                                }
//                                if( cm.toString(i13HyoukaListBean.getHyoukaNotice()).equals("")  == false ){
//                                    hyoka += cm.padRight(i13HyoukaListBean.getHyoukaNotice(), "UTF-8", 2) + ":";
//                                }else{
//                                    hyoka += "  :";
//                                }
                                //2019/09/24 QQ)Oosaki 共通テスト対応 ADD END
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END

                                if( cm.toString(i13HyoukaListBean.getStrHyoukaKijyutsu()).equals("") == false ){
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                    //hyoka += i13HyoukaListBean.getStrHyoukaKijyutsu() + ":" ;
                                    hyoka += cm.padRight(i13HyoukaListBean.getStrHyoukaKijyutsu(), "UTF-8", 2) + ":";
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                }else{
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                    //hyoka += "　:" ;
                                    hyoka += "  :" ;
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                }
                                if( cm.toString(i13HyoukaListBean.getStrHyoukaAll()).equals("") == false ){
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                    //hyoka += i13HyoukaListBean.getStrHyoukaAll() ;
                                    hyoka += cm.padRight(i13HyoukaListBean.getStrHyoukaAll(), "UTF-8",2);
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                }else{
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                    //hyoka += " " ;
                                    hyoka += "  " ;
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                }
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                workCell = cm.setCell( workSheet, workRow, workCell, 4+shiboRow+4*kyokaRow, 60+5*shiboCol );
                                //workCell = cm.setCell( workSheet, workRow, workCell, 4+shiboRow+4*kyokaRow, 64+5*shiboCol );
                                //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                workCell.setCellValue( hyoka );
                            }

                            shiboRow++;
                            if(shiboRow >= 4){
                                shiboRow = 0;
                                shiboCol++;
                                if(shiboCol >= 2){
                                    break;
                                }
                            }
                        }

                    }else{
                        // 対象模試セット
                        String moshi = cm.setTaisyouMoshi( i13seisekiListBean.getStrMshDate() );	// 模試月取得
                        workCell = cm.setCell( workSheet, workRow, workCell, 8+4*kyokaRow, 0 );
                        workCell.setCellValue( i13seisekiListBean.getStrMshmei() + moshi );

                        // 「換算得点」文字列の表記（マーク系模試の時のみ出力）
                        String tmpExamCD = cm.toString( i13seisekiListBean.getStrMshCd() );
                        if(tmpExamCD == null){ tmpExamCD = ""; }
                        if( tmpExamCD.equals("01")
                            || tmpExamCD.equals("02")
                            || tmpExamCD.equals("03")
                            || tmpExamCD.equals("04")
                            || tmpExamCD.equals("66") ){
                            workCell = cm.setCell( workSheet, workRow, workCell, 11+4*kyokaRow, 2 );
                            workCell.setCellValue("換算得点");
                        }

                        //受験学力テスト
                        if (KNUtil.TYPECD_ABILITY.equals(i13seisekiListBean.getStrMshTypeCd())) {

                            workCell = cm.setCell(workSheet, workRow, workCell, 10+4*kyokaRow, 2);
                            workCell.setCellValue("スコア");

                            workCell = cm.setCell(workSheet, workRow, workCell, 11+4*kyokaRow, 3);
                            workCell.setCellValue("到達エリア");
                        }

                        /** 教科別成績リスト **/
                        // データセット
                        ArrayList i13KyokaSeisekiList = i13seisekiListBean.getI13KyokaSeisekiList();
                        Iterator itrI13KyokaSeiseki = i13KyokaSeisekiList.iterator();

                        String strKykHoji = "";
                        int kyokaTabCol = 0;
                        int kyokaCol = 0;
                        int kyoukaCnt = 0;

                        //第一解答科目の成績
                        I13KyokaSeisekiListBean rikaSeiseki = null;
                        I13KyokaSeisekiListBean chikouSeiseki = null;

                        while(itrI13KyokaSeiseki.hasNext()){
                            I13KyokaSeisekiListBean i13KyokaSeisekiListBean = (I13KyokaSeisekiListBean)itrI13KyokaSeiseki.next();

                            // 教科コードによって判定 → 2005.01.06 小論文は出力しない
                            String strKyouka = i13KyokaSeisekiListBean.getStrKyokaCd()+" ";
                            strKyouka = strKyouka.substring(0,1);
                            if( strKyouka.equals("6") == false ){
                                if( !strKyouka.equals(strKykHoji) ){
                                    switch( Integer.parseInt( strKyouka ) ){
                                        case 1:	//英語
                                            kyokaTabCol = 4;
                                            kyoukaCnt = 3;
                                            break;
                                        case 2:	//数学
                                            kyokaTabCol = 16;
                                            kyoukaCnt = 3;
                                            break;
                                        case 3:	//国語
                                            kyokaTabCol = 28;
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                            ////受験学力テスト
                                            //if (KNUtil.TYPECD_ABILITY.equals(i13seisekiListBean.getStrMshTypeCd())) {
                                            //    kyoukaCnt = 2;
                                            //} else {
                                            //    kyoukaCnt = 1;
                                            //}
                                            //kyoukaCnt = 2;
                                            kyoukaCnt = 1;
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                            break;
                                        case 4:	//理科
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                            kyokaTabCol = 32;
                                            //kyokaTabCol = 36;
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                            kyoukaCnt = 3;
                                            break;
                                        case 5:	//地歴公民
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                            kyokaTabCol = 44;
                                            //kyokaTabCol = 48;
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                            kyoukaCnt = 2;
                                            break;
                                        case 7:	//総合
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                            kyokaTabCol = 52;
                                            //kyokaTabCol = 56;
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                            kyoukaCnt = 1;
                                            break;
                                    }
                                    kyokaCol = 0;
                                    strKykHoji = strKyouka;
                                }
                                if (kyoukaCnt>0) {
                                    //理科の先頭科目が基礎科目なら、先頭は空欄にする
                                    if ("4".equals(strKyouka) && kyokaCol == 0 && "1".equals(i13KyokaSeisekiListBean.getBasicFlg())) {
                                        kyokaCol++;
                                    }

                                    // 科目名セット
                                    workCell = cm.setCell( workSheet, workRow, workCell, 8+4*kyokaRow, kyokaTabCol+4*kyokaCol );
                                    workCell.setCellValue( i13KyokaSeisekiListBean.getStrKmkmei() );

                                    if (Integer.parseInt( strKyouka ) == 7){
                                        // 得点セット
                                        if(i13KyokaSeisekiListBean.getIntTokuten() != -999){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 10+4*kyokaRow, kyokaTabCol+3*kyokaCol );
                                            workCell.setCellValue( i13KyokaSeisekiListBean.getIntTokuten() );
                                        }

                                        // 配点セット
                                        workCell = cm.setCell( workSheet, workRow, workCell, 10+4*kyokaRow, kyokaTabCol+3*kyokaCol+1 );
                                        workCell.setCellValue( i13KyokaSeisekiListBean.getStrHaitenKmk() );

                                        // 換算得点セット
                                        if(i13KyokaSeisekiListBean.getIntKansanTokuten() != -999){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 11+4*kyokaRow, kyokaTabCol+3*kyokaCol );
                                            workCell.setCellValue( i13KyokaSeisekiListBean.getIntKansanTokuten() );
                                        }

                                        // 偏差値セット
                                        if(i13KyokaSeisekiListBean.getFloHensa() != -999.0){

                                            workCell = cm.setCell( workSheet, workRow, workCell, 11+4*kyokaRow, kyokaTabCol+3*kyokaCol+1 );

                                            //受験学力測定
                                            if (KNUtil.TYPECD_ABILITY.equals(i13seisekiListBean.getStrMshTypeCd())) {
                                                workCell.setCellValue(KNUtil.deviationToCenterReachArea(i13KyokaSeisekiListBean.getFloHensa()));
                                            } else {
                                                workCell.setCellValue( i13KyokaSeisekiListBean.getFloHensa() );
                                            }
                                        }

                                        // 学力レベルセット
                                        if( !"".equals(i13KyokaSeisekiListBean.getStrScholarLevel())){
                                            workCell = cm.setCell( workSheet, workRow, workCell, 11+4*kyokaRow, kyokaTabCol+3*kyokaCol+2 );
                                            //受験学力測定以外の場合にセット
                                            if (!KNUtil.TYPECD_ABILITY.equals(i13seisekiListBean.getStrMshTypeCd())) {
                                                workCell.setCellValue( i13KyokaSeisekiListBean.getStrScholarLevel() );
                                            }
                                        }
                                    } else {
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
                                        //2019/09/24 QQ)Oosaki 共通テスト対応 ADD START
//                                        if(("3915").equals(i13KyokaSeisekiListBean.getSubCd())) {
//                                            workCell = cm.setCell( workSheet, workRow, workCell, 10+4*kyokaRow, kyokaTabCol+4*kyokaCol+1 );
//                                            // 出力する際セルの中央に寄せる
//                                            workCell.setCellStyle(CellStyle);
//                                            workCell.setCellValue( i13KyokaSeisekiListBean.getKokugoTokuten() );
//                                        } else {
                                        //2019/09/24 QQ)Oosaki 共通テスト対応 ADD END
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END
                                            // 得点セット
                                            if(i13KyokaSeisekiListBean.getIntTokuten() != -999){
                                                workCell = cm.setCell( workSheet, workRow, workCell, 10+4*kyokaRow, kyokaTabCol+4*kyokaCol+1 );
                                                workCell.setCellValue( i13KyokaSeisekiListBean.getIntTokuten() );
                                            }

                                            // 配点セット
                                            workCell = cm.setCell( workSheet, workRow, workCell, 10+4*kyokaRow, kyokaTabCol+4*kyokaCol+2 );
                                            workCell.setCellValue( i13KyokaSeisekiListBean.getStrHaitenKmk() );

                                            // 換算得点セット
                                            if(i13KyokaSeisekiListBean.getIntKansanTokuten() != -999){
                                                // 換算得点に*を付加
                                                // マーク模試の場合のみ、換算得点が第１解答科目＜第２解答科目の場合に*表示
                                                if (KNUtil.isAns1st(i13seisekiListBean.getStrMshCd())){
                                                    if (cm.toString(strKyouka).equals("4") && i13KyokaSeisekiListBean.getIntScope() == 0 && !"1".equals(i13KyokaSeisekiListBean.getBasicFlg())){
                                                        if (rikaSeiseki != null){
                                                            if (rikaSeiseki.getIntKansanTokuten() < i13KyokaSeisekiListBean.getIntKansanTokuten()){
                                                                workCell = cm.setCell( workSheet, workRow, workCell, 11+4*kyokaRow, kyokaTabCol+4*kyokaCol );
                                                                workCell.setCellValue( "*" );

                                                            }
                                                        }
                                                    }else if (cm.toString(strKyouka).equals("5") && i13KyokaSeisekiListBean.getIntScope() == 0){
                                                        if (chikouSeiseki != null){
                                                            if (chikouSeiseki.getIntKansanTokuten() < i13KyokaSeisekiListBean.getIntKansanTokuten()){
                                                                workCell = cm.setCell( workSheet, workRow, workCell, 11+4*kyokaRow, kyokaTabCol+4*kyokaCol );
                                                                workCell.setCellValue( "*" );

                                                            }
                                                        }
                                                    }
                                                }
                                                workCell = cm.setCell( workSheet, workRow, workCell, 11+4*kyokaRow, kyokaTabCol+4*kyokaCol+1 );
                                                workCell.setCellValue( i13KyokaSeisekiListBean.getIntKansanTokuten() );
                                            }

                                            // 偏差値セット
                                            if(i13KyokaSeisekiListBean.getFloHensa() != -999.0){

                                                workCell = cm.setCell( workSheet, workRow, workCell, 11+4*kyokaRow, kyokaTabCol+4*kyokaCol+2 );

                                                //受験学力測定
                                                if (KNUtil.TYPECD_ABILITY.equals(i13seisekiListBean.getStrMshTypeCd())) {
                                                    workCell.setCellValue(KNUtil.deviationToCenterReachArea(i13KyokaSeisekiListBean.getFloHensa()));
                                                } else {
                                                    workCell.setCellValue( i13KyokaSeisekiListBean.getFloHensa() );
                                                }
                                            }

                                            // 学力レベルセット
                                            if( !"".equals(i13KyokaSeisekiListBean.getStrScholarLevel())){
                                                workCell = cm.setCell( workSheet, workRow, workCell, 11+4*kyokaRow, kyokaTabCol+4*kyokaCol+3 );
                                                //受験学力測定以外の場合にセット
                                                if (!KNUtil.TYPECD_ABILITY.equals(i13seisekiListBean.getStrMshTypeCd())) {
                                                    workCell.setCellValue( i13KyokaSeisekiListBean.getStrScholarLevel() );
                                                }
                                            }

                                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 ADD START
                                            //CEFR出力
//                                            if(("01").equals(i13KyokaSeisekiListBean.getExcdtype())) {
//                                                workCell = cm.setCell( workSheet, workRow, workCell, 8+4*kyokaRow, 12 );
//                                                workCell.setCellValue( "CEFR" );
//                                                workCell = cm.setCell( workSheet, workRow, workCell, 10+4*kyokaRow, 13 );
//                                                workCell.setCellStyle(CellStyle);
//                                                workCell.setCellValue( i13KyokaSeisekiListBean.getCefrScore() );
//                                            }
                                            //2019/09/24 QQ)Oosaki 共通テスト対応 ADD END
                                            // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END

                                            // 第１解答科目保存（マーク模試のみ）
                                            if (KNUtil.isAns1st(i13seisekiListBean.getStrMshCd())){
                                                if (cm.toString(strKyouka).equals("4") && i13KyokaSeisekiListBean.getIntScope() == 1){
                                                    rikaSeiseki = i13KyokaSeisekiListBean;
                                                }else if (cm.toString(strKyouka).equals("5") && i13KyokaSeisekiListBean.getIntScope() == 1){
                                                    chikouSeiseki = i13KyokaSeisekiListBean;
                                                }
                                            }
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
//                                        }
                                        // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END
                                    }
                                }
                                kyoukaCnt--;
                                kyokaCol++;
                            }
                        }

                        /** 志望大学別評価データリスト **/
                        // データセット
                        ArrayList i13HyoukaList = i13seisekiListBean.getI13HyoukaList();
                        Iterator itrI13Hyouka = i13HyoukaList.iterator();

                        int shiboRow = 0;
                        int shiboCol = 0;
                        while(itrI13Hyouka.hasNext()){
                            I13HyoukaListBean i13HyoukaListBean = (I13HyoukaListBean)itrI13Hyouka.next();

                            // 大学名セット
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                            workCell = cm.setCell( workSheet, workRow, workCell, 8+shiboRow+4*kyokaRow, 57+5*shiboCol );
                            //workCell = cm.setCell( workSheet, workRow, workCell, 8+shiboRow+4*kyokaRow, 61+5*shiboCol );
                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                            workCell.setCellValue( i13HyoukaListBean.getStrDaigakuMei() );

                            // 学部名セット
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                            workCell = cm.setCell( workSheet, workRow, workCell, 8+shiboRow+4*kyokaRow, 58+5*shiboCol );
                            //workCell = cm.setCell( workSheet, workRow, workCell, 8+shiboRow+4*kyokaRow, 62+5*shiboCol );
                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                            workCell.setCellValue( i13HyoukaListBean.getStrGakubuMei() );

                            // 学科名セット
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                            workCell = cm.setCell( workSheet, workRow, workCell, 8+shiboRow+4*kyokaRow, 59+5*shiboCol );
                            //workCell = cm.setCell( workSheet, workRow, workCell, 8+shiboRow+4*kyokaRow, 63+5*shiboCol );
                            //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                            // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                            workCell.setCellValue( i13HyoukaListBean.getStrGakkaMei() );

                            // 評価セット
                            if( cm.toString(i13HyoukaListBean.getStrHyoukaMark()).equals("") &&
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                                //2019/09/24 QQ)Oosaki 共通テスト対応 ADD START
                                //cm.toString(i13HyoukaListBean.getStrHyoukaMark_engpt()).equals("") &&
                                //2019/09/24 QQ)Oosaki 共通テスト対応 ADD END
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
                                cm.toString(i13HyoukaListBean.getStrHyoukaKijyutsu()).equals("") &&
                                cm.toString(i13HyoukaListBean.getStrHyoukaAll()).equals("") ){
                            }else{

                                String hyoka = "";
                                if( cm.toString(i13HyoukaListBean.getStrHyoukaMark()).equals("") == false ){
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                    //hyoka = i13HyoukaListBean.getStrHyoukaMark() + ":" ;
                                    hyoka = cm.padRight(i13HyoukaListBean.getStrHyoukaMark(),"UTF-8",2) + ":";
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                }else{
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                    //hyoka = "　:" ;
                                    hyoka = "  :";
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                }

                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
                                //2019/09/24 QQ)Oosaki 共通テスト対応 ADD START
//                                if( cm.toString(i13HyoukaListBean.getStrHyoukaMark_engpt()).equals("")  == false ){
//                                    hyoka += cm.padRight(i13HyoukaListBean.getStrHyoukaMark_engpt(), "UTF-8", 2);
//                                }else{
//                                    hyoka += "  ";
//                                }
//                                if( cm.toString(i13HyoukaListBean.getHyoukaJodge()).equals("")  == false ){
//                                    hyoka += " " + i13HyoukaListBean.getHyoukaJodge();
//                                }else{
//                                    hyoka += "   ";
//                                }
//                                if( cm.toString(i13HyoukaListBean.getHyoukaNotice()).equals("")  == false ){
//                                    hyoka += cm.padRight(i13HyoukaListBean.getHyoukaNotice(), "UTF-8", 2) + ":";
//                                }else{
//                                    hyoka += "  :";
//                                }
                                //2019/09/24 QQ)Oosaki 共通テスト対応 ADD END
                                // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END

                                if( cm.toString(i13HyoukaListBean.getStrHyoukaKijyutsu()).equals("") == false ){
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                    //hyoka += i13HyoukaListBean.getStrHyoukaKijyutsu() + ":" ;
                                    hyoka += cm.padRight(i13HyoukaListBean.getStrHyoukaKijyutsu(),"UTF-8",2) + ":";
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                }else{
                                    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD START
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                    //hyoka += "　:" ;
                                    //hyoka = "  ";
                                    hyoka += "  :";
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 UPD END
                                }
                                if( cm.toString(i13HyoukaListBean.getStrHyoukaAll()).equals("") == false ){
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                    //hyoka += i13HyoukaListBean.getStrHyoukaAll() ;
                                    hyoka += cm.padRight(i13HyoukaListBean.getStrHyoukaAll(), "UTF-8",2);
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                }else{
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                    //hyoka += " " ;
                                    hyoka += "  ";
                                    //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                }
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
                                //2019/09/24 QQ)Oosaki 共通テスト対応 UPD START
                                workCell = cm.setCell( workSheet, workRow, workCell, 8+shiboRow+4*kyokaRow, 60+5*shiboCol );
                                //workCell = cm.setCell( workSheet, workRow, workCell, 8+shiboRow+4*kyokaRow, 64+5*shiboCol );
                                //2019/09/24 QQ)Oosaki 共通テスト対応 UPD END
                                // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END
                                workCell.setCellValue( hyoka );
                            }

                            shiboRow++;
                            if(shiboRow >= 4){
                                shiboRow = 0;
                                shiboCol++;
                                if(shiboCol >= 2){
                                    break;
                                }
                            }
                        }
                        kyokaRow++;
                        if(kyokaRow > 15){
                            break;
                        }
                    }
                    //2005.04.27 Add 新テスト対応　模試の表示行数をセンターリサーチ表示時は１４桁、
                    //							　表示しないときは１５行に制限
                    moshiRow++;
                    if(centerReFlg == true){
                        if(moshiRow > 15){
                            break;
                        }
                    }else{
                        if(moshiRow > 14){
                            break;
                        }
                    }

                }
                // Excelファイル保存
                boolean bolRet = false;
                String strFileName = masterfile + "_" + cm.toString(i11ListBean.getStrGrade()) + cm.toString(i11ListBean.getStrClass()) + cm.toString(i11ListBean.getStrClassNum());
                    bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, strFileName, maxSheetIndex);

                if( bolRet == false ){
                    return errfwrite;
                }
            }

        } catch(Exception e) {
            log.Err("I13_02","データセットエラー",e.toString());
            return errfdata;
        }

        return noerror;
    }

     // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
     // 2019/09/24 QQ)Oosaki 共通テスト対応 ADD START
//     /**
//      * スタイルを作成する
//      * @param  workbook - ワークブック
//      * @return style    - スタイル
//      */
//     private HSSFCellStyle createStyle(HSSFWorkbook workbook) {
//         // スタイルを新規作成
//         HSSFCellStyle style = workbook.createCellStyle();
//
//         // フォントを定義する
//         HSSFFont font = workbook.createFont();
//         // フォント名
//         font.setFontName("ＭＳ ゴシック");
//         // フォントサイズ
//         font.setFontHeightInPoints( (short)10 );
//         // 設定
//         style.setFont(font);
//         // 中央揃え
//         style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
//
//         return style;
//     }
     // 2019/09/24 QQ)Oosaki 共通テスト対応 ADD END
     // 2020/04/01 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END

}
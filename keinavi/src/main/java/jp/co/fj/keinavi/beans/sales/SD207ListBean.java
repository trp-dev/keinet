package jp.co.fj.keinavi.beans.sales;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.data.sales.SD207Data;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 特例成績データダウンロード画面のリストBeanです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD207ListBean extends DefaultBean {

	/** serialVersionUID */
	private static final long serialVersionUID = -7060896326983048214L;

	/** 対象年度 */
	private final String examYear;

	/** 絞込み条件 */
	private final String condition;

	/** ソートキー */
	private final String sortKey;

	/** 所属する部門コードの配列 */
	private final String[] sectorCdArray;

	/** データリスト */
	private final List results = new ArrayList();

	/**
	 * コンストラクタです。
	 */
	public SD207ListBean(String examYear, String condition, String sortKey,
			String[] sectorCdArray) {
		this.examYear = examYear;
		this.condition = condition;
		this.sortKey = sortKey;
		this.sectorCdArray = sectorCdArray;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(createQuery());
			ps.setString(1, examYear);
			ps.setString(2, condition);

			rs = ps.executeQuery();
			while (rs.next()) {
				SD207Data data = new SD207Data();
				data.setAppId(rs.getString(1));
				data.setExamYear(rs.getString(2));
				data.setExamCd(rs.getString(3));
				data.setExamName(rs.getString(4));
				data.setSchoolCd(rs.getString(5));
				data.setSchoolName(rs.getString(6));
				data.setPactDivName(rs.getString(7));
				data.setTeacherName(rs.getString(8));
				data.setAppComment(rs.getString(9));
				data.setAppDate(rs.getTimestamp(10));
				data.setDownloadDate(rs.getTimestamp(11));
				data.setStatusCd(rs.getString(12));
				data.setStatusName(rs.getString(13));
				data.setOpenDate(rs.getDate(14));
				data.setPassword(rs.getString(15));
				data.setTargetGrade(rs.getString(16));
				results.add(data);
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * SQLを生成します。
	 */
	private String createQuery() throws SQLException {
		Query query = QueryLoader.getInstance().load("sd207_list");
		query.setStringArray(1, sectorCdArray);
		query.append(createOrderBy());
		return query.toString();
	}

	/**
	 * ORDER BY句を生成します。
	 */
	private String createOrderBy() {

		if ("1".equals(sortKey)) {
			/* 対象年度 */
			return "EXAMYEAR, APP_DATE DESC, BUNDLECD";
		} else if ("2".equals(sortKey)) {
			/* 対象模試 */
			return "DISPSEQUENCE, EXAMCD, APP_DATE DESC, BUNDLECD";
		} else if ("3".equals(sortKey)) {
			/* 学校コード */
			return "BUNDLECD, APP_DATE DESC";
		} else if ("4".equals(sortKey)) {
			/* 申請日時 */
			return "APP_DATE DESC, BUNDLECD";
		} else if ("5".equals(sortKey)) {
			/* 最終ダウンロード日時 */
			return "LAST_DOWNLOAD_DATE, APP_DATE DESC, BUNDLECD";
		} else if ("6".equals(sortKey)) {
			/* 状態 */
			return "APP_STATUS_CODE, APP_DATE DESC, BUNDLECD";
		} else {
			/* 不正なソートキーなら例外とする */
			throw new RuntimeException("ソートキーが不正です。" + sortKey);
		}
	}

	/**
	 * データリストを返します。
	 * 
	 * @return データリスト
	 */
	public List getResults() {
		return results;
	}

}

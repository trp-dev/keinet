package jp.co.fj.keinavi.beans.maintenance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.beans.admin.IndivChangeInfoTableAccess;
import jp.co.fj.keinavi.beans.admin.MScoreUpdateBean;
import jp.co.fj.keinavi.util.message.MessageLoader;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 結合処理Bean
 * 
 * 2006.11.14	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class ExamIntegrationBean extends DefaultBean {

	// ログ出力モジュール
	private final MainteLogBean log;
	// 個人成績更新Bean
	// ※既存モジュールの再利用
	private final MScoreUpdateBean bean;
	// 再集計対象登録Bean
	private final MAnswerSheetRecountBean recount;

	// 模試年度
	private String examYear;
	// 模試コード
	private String examCd;
	// 解答用紙番号
	private String answerSheetNo;
	// 結合する個人ID
	private String afterIndividualId;
	
	// コンストラクタ
	public ExamIntegrationBean(final String pSchoolCd) {
		log = new MainteLogBean(pSchoolCd);
		bean = new MScoreUpdateBean();
		recount = new MAnswerSheetRecountBean();
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws  Exception {
		
		// 結合元の個人ID取得
		final String beforeIndividualId = getSIndividualId();
		
		// 個人成績の結合
		try {
			bean.setExamYear(examYear);
			bean.setExamCd(examCd);
			bean.setMasterId(beforeIndividualId);
			bean.setRewriteId(afterIndividualId);
			bean.execute();
		} catch (final SQLException e) {
			throw new KNBeanException(
					MessageLoader.getInstance().getMessage("m016a"));
		}
		
		// 再集計対象の登録
		recount.setExamYear(examYear);
		recount.setExamCd(examCd);
		recount.setAnswerSheetNo(answerSheetNo);
		recount.setAfterIndividualId(afterIndividualId);
		recount.execute();
		
		// 個表データの結合
		updateIndividualRecord();
		// 個人成績がなくなった生徒を非表示にする
		//updateNotDisplayFlg(beforeIndividualId);
		
		//更新：変更成績情報テーブル
		IndivChangeInfoTableAccess.updatePrimary(conn, examYear, examCd, answerSheetNo);

		// 結合ログ
		log.examIntegrationLog(
				examYear, examCd, beforeIndividualId, afterIndividualId);
	}

	// 現在結合している個人IDを取得する
	private String getSIndividualId() throws Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT s_individualid "
					+ "FROM individualrecord "
					+ "WHERE examyear = ? AND examcd = ? "
					+ "AND answersheet_no = ?");
			ps.setString(1, examYear);
			ps.setString(2, examCd);
			ps.setString(3, answerSheetNo);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString(1);
			} else {
				throw new Exception("個人IDの取得に失敗しました。");
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	// 個表の個人IDを更新する
	private void updateIndividualRecord() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("m35"));
			ps.setString(1, afterIndividualId);
			ps.setString(2, examYear);
			ps.setString(3, examCd);
			ps.setString(4, answerSheetNo);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	// 個人成績がなくなった生徒を非表示にする
//	private void updateNotDisplayFlg(
//			final String beforeIndividualId) throws SQLException {
//		PreparedStatement ps = null;
//		try {
//			ps = conn.prepareStatement(
//					QueryLoader.getInstance().getQuery("m33"));
//			ps.setString(1, beforeIndividualId);
//			ps.executeUpdate();
//		} finally {
//			DbUtils.closeQuietly(ps);
//		}
//	}

	/**
	 * @see com.fjh.beans.DefaultBean#setConnection(
	 * 			java.lang.String, java.sql.Connection)
	 */
	public void setConnection(final String pName, final Connection pConn) {
		super.setConnection(pName, pConn);
		log.setConnection(pName, pConn);
		bean.setConnection(pName, pConn);
		recount.setConnection(pName, pConn);
	}

	/**
	 * @param pAnswerSheetNo 設定する answerSheetNo。
	 */
	public void setAnswerSheetNo(String pAnswerSheetNo) {
		answerSheetNo = pAnswerSheetNo;
	}

	/**
	 * @param pExamCd 設定する examCd。
	 */
	public void setExamCd(String pExamCd) {
		examCd = pExamCd;
	}

	/**
	 * @param pExamYear 設定する examYear。
	 */
	public void setExamYear(String pExamYear) {
		examYear = pExamYear;
	}

	/**
	 * @param pAfterIndividualId 設定する afterIndividualId。
	 */
	public void setAfterIndividualId(String pAfterIndividualId) {
		afterIndividualId = pAfterIndividualId;
	}
	
}

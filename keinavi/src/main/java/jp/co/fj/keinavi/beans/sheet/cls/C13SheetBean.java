/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.cls;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.excel.cls.C13;
import jp.co.fj.keinavi.excel.data.cls.C13AllDataListBean;
import jp.co.fj.keinavi.excel.data.cls.C13Item;
import jp.co.fj.keinavi.excel.data.cls.C13KmkDataListBean;
import jp.co.fj.keinavi.excel.data.cls.C13KmkListBean;
import jp.co.fj.keinavi.excel.data.cls.C13KmkSeisekiListBean;
import jp.co.fj.keinavi.excel.data.cls.C13KyokaDataListBean;
import jp.co.fj.keinavi.excel.data.cls.C13ListBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 2005.02.28	Yoshimoto KAWAI - Totec
 *				担当クラスの年度対応
 *
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 *
 * 2005.04.20	Yoshimoto KAWAI - Totec
 * 				新テスト対応
 *
 * 2009.11.24   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 *
 * @author kawai
 *
 */
public class C13SheetBean extends AbstractSheetBean {

    private static final long serialVersionUID = -8507232012894511906L;

    // データクラス
    private final C13Item data = new C13Item();

    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws SQLException, Exception {

        //String current = KNUtil.getCurrentYear(); // 現在の年度
        String current = ProfileUtil.getChargeClassYear(profile);
        final boolean newExam = KNUtil.isNewExam(exam); // 新テストかどうか

        ExamData pastExam1 = examSession.getExamData(exam.getExamYear(), this.pastExam1);
        ExamData pastExam2 = examSession.getExamData(exam.getExamYear(), this.pastExam2);

        // 共にNULL
        if (pastExam1 == null && pastExam2 == null) {
            // 過回模試リスト
            List container = KNUtil.getPastExamList(examSession, exam);

            // ない場合
            if (container.size() == 0) {
                pastExam1 = new ExamData();
                pastExam2 = new ExamData();

            // ひとつある
            } else if (container.size() == 1) {
                pastExam1 = (ExamData) container.get(0);
                pastExam2 = new ExamData();

            // ふたつ以上
            } else {
                pastExam1 = (ExamData) container.get(0);
                pastExam2 = (ExamData) container.get(1);
            }

        // 1だけがNULL
        } else if (pastExam1 == null) {
            pastExam1 = pastExam2;
            pastExam2 = new ExamData();

        // 2だけがNULL
        } else if (pastExam2 == null) {
            pastExam2 = new ExamData();

        // 共に有効模試
        } else {
            // データ開放日が大きいほうが過回1になる
            if (pastExam1.getDataOpenDate().compareTo(pastExam2.getDataOpenDate()) < 0) {
                ExamData temp = pastExam2;
                pastExam2 = pastExam1;
                pastExam1 = temp;
            }
        }

        // データを詰める
        data.setStrGakkomei(profile.getBundleName()); // 学校名
        data.setStrYear(exam.getExamYear()); // 対象年度
        data.setStrMshmeiNow(exam.getExamName()); // 対象模試・模試名
        data.setStrMshmeiShortNow(exam.getExamNameAbbr()); // 対象模試・模試名
        data.setStrMshDateNow(exam.getInpleDate()); // 対象模試・模試実施基準日
        data.setStrMshmeiLast1(pastExam1.getExamName()); // 前回模試・模試名
        data.setStrMshmeiShortLast1(pastExam1.getExamNameAbbr()); // 前回模試・模試名
        data.setStrMshDateLast1(pastExam1.getInpleDate()); // 前回模試・模試実施基準日
        data.setStrMshmeiLast2(pastExam2.getExamName()); // 前々回模試・模試名
        data.setStrMshmeiShortLast2(pastExam2.getExamNameAbbr()); // 前々回模試・模試名
        data.setStrMshDateLast2(pastExam2.getInpleDate()); // 前々回模試・模試実施基準日
        data.setIntHyouFlg(super.getIntFlag(IProfileItem.CHART)); // 表フラグ
        data.setIntSortFlg(super.getIntFlag(IProfileItem.STUDENT_ORDER)); // 生徒の表示順序
        data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
        data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ

        // 型・科目別比較
        switch (super.getIntFlag(IProfileItem.IND_COMP)) {
            case  1: data.setIntKmkHikakuFlg(0); data.setIntHikakuCntFlg(1); break;
            case  2: data.setIntKmkHikakuFlg(0); data.setIntHikakuCntFlg(2); break;
            case 11: data.setIntKmkHikakuFlg(1); data.setIntHikakuCntFlg(1); break;
            case 12: data.setIntKmkHikakuFlg(1); data.setIntHikakuCntFlg(2); break;
        }

        // 変動幅
        // 新テストの場合
        if (KNUtil.isNewExam(exam)) {
            switch (super.getIntFlag(IProfileItem.IND_FLUCT_RANGE)) {
                case  0: data.setStrKeyPitch("10"); break;
                case  1: data.setStrKeyPitch("20"); break;
                case  2: data.setStrKeyPitch("40"); break;
            }

        // それ以外
        } else {
            switch (super.getIntFlag(IProfileItem.IND_FLUCT_RANGE)) {
                case  0: data.setStrKeyPitch("2.5"); break;
                case  1: data.setStrKeyPitch("5.0"); break;
                case  2: data.setStrKeyPitch("10.0"); break;
            }
        }

        // 偏差値
        // 新テストの場合
        if (KNUtil.isNewExam(exam)) {
            data.setStrKeyHensa(null);

        // それ以外
        } else {
            switch (super.getIntFlag(IProfileItem.DEV)) {
                case  11: data.setStrKeyHensa("40.0"); break;
                case  12: data.setStrKeyHensa("45.0"); break;
                case  13: data.setStrKeyHensa("50.0"); break;
                case  14: data.setStrKeyHensa("55.0"); break;
                case  15: data.setStrKeyHensa("60.0"); break;
                case  16: data.setStrKeyHensa("65.0"); break;
                case  17: data.setStrKeyHensa("70.0"); break;
                case  18: data.setStrKeyHensa("75.0"); break;
                default :  data.setStrKeyHensa("");
            }
        }

        // ワークテーブルへデータを入れる
        super.insertIntoCountChargeClass();
        super.insertIntoExistsExam();

        // 表フラグが1ならデータリストをセットする
        if (data.getIntHyouFlg() == 1) {

            // 新テストなら全国偏差値ではなく平均点を入れる
            final int scoreC;
            final int scoreP;
            if (newExam) {
                scoreC = 8;
                scoreP = 9;
            } else {
                scoreC = 5;
                scoreP = 6;
            }

            PreparedStatement ps1 = null;
            PreparedStatement ps2 = null;
            PreparedStatement ps3 = null;
            ResultSet rs1 = null;
            ResultSet rs2 = null;
            ResultSet rs3 = null;
            try {
                // 担当クラス
                ps1 = conn.prepareStatement(QueryLoader.getInstance().load("c11_1").toString());

                // 総合成績データリスト
                Query query1 = QueryLoader.getInstance().load("c13_1");

                // 偏差値での絞込み
                // 新テストは行わない
                if (!"".equals(data.getStrKeyHensa()) && !newExam) {
                    query1.append("AND a.a_deviation >= ? ");
                }

                // 成績順は新テストなら平均点
                // それ以外は全国偏差値を利用する
                switch (data.getIntSortFlg()) {
                    case  1:
                        query1.append("ORDER BY grade DESC, class, class_no");
                        break;
                    case  2:
                        if (newExam) {
                            query1.append("ORDER BY score DESC, grade DESC, class, class_no");
                        } else {
                            query1.append("ORDER BY deviation DESC, grade DESC, class, class_no");
                        }
                        break;
                    case  3:
                        if (newExam) {
                            query1.append("ORDER BY diff DESC, score DESC, grade DESC, class, class_no");
                        } else {
                            query1.append("ORDER BY diff DESC, deviation DESC, grade DESC, class, class_no");
                        }
                        break;
                }

                ps2 = conn.prepareStatement(query1.toString());
                ps2.setString(1, exam.getExamYear()); // 模試年度
                ps2.setString(2, exam.getExamCD()); // 模試コード
                ps2.setString(3, login.getUserID()); // 学校コード
                ps2.setString(4, current); // 現在の年度
                ps2.setString(7, exam.getExamYear()); // 模試年度
                ps2.setString(8, exam.getExamCD()); // 模試コード
                ps2.setString(9, login.getUserID()); // 学校コード
                ps2.setString(10, current); // 現在の年度
                ps2.setString(12, pastExam1.getExamYear()); // 前回模試年度
                ps2.setString(13, pastExam1.getExamCD()); // 前回模試コード
                ps2.setString(14, login.getUserID()); // 学校コード
                ps2.setString(15, current); // 現在の年度
                ps2.setString(18, pastExam1.getExamYear()); // 前回模試年度
                ps2.setString(19, pastExam1.getExamCD()); // 前回模試コード
                ps2.setString(20, login.getUserID()); // 学校コード
                ps2.setString(21, current); // 現在の年度

                if (!"".equals(data.getStrKeyHensa()) && !newExam) {
                    ps2.setFloat(23, Float.parseFloat(data.getStrKeyHensa()));
                }

                // 教科別成績データリスト
                ps3 = conn.prepareStatement(QueryLoader.getInstance().load("c13_2").toString());
                ps3.setString(1, pastExam1.getExamYear()); // 前回模試年度
                ps3.setString(2, pastExam1.getExamCD()); // 前回模試コード
                ps3.setString(4, exam.getExamYear()); // 模試年度
                ps3.setString(5, exam.getExamCD()); // 模試コード

                rs1 = ps1.executeQuery();
                while (rs1.next()) {
                    C13ListBean bean = new C13ListBean();
                    bean.setStrGrade(rs1.getString(1));
                    bean.setStrClass(rs1.getString(3));
                    data.getC13List().add(bean);

                    // 学年
                    ps2.setInt(5, rs1.getInt(1));
                    ps2.setInt(16, rs1.getInt(1));
                    // クラス
                    ps2.setString(6, rs1.getString(2));
                    ps2.setString(11, rs1.getString(2));
                    ps2.setString(17, rs1.getString(2));
                    ps2.setString(22, rs1.getString(2));

                    rs2 = ps2.executeQuery();
                    while (rs2.next()) {
                        C13AllDataListBean a = new C13AllDataListBean();
                        a.setStrGrade(rs2.getString(2));
                        a.setStrClass(rs2.getString(3));
                        a.setStrClassNo(rs2.getString(4));
                        a.setStrKanaName(rs2.getString(5));
                        a.setStrSex(rs2.getString(6));
                        bean.getC13AllDataList().add(a);

                        // 科目別成績データリスト
                        List c1 = new LinkedList(); // 型の入れ物
                        List c2 = new LinkedList(); // 科目の入れ物

                        // 個人ID
                        ps3.setString(3, rs2.getString(1));
                        ps3.setString(6, rs2.getString(1));

                        rs3 = ps3.executeQuery();
                        while (rs3.next()) {
                            C13KyokaDataListBean k = new C13KyokaDataListBean();
                            k.setStrKyokaCd(rs3.getString(1));
                            k.setStrKyokamei(rs3.getString(2));
                            k.setStrKmkCd(rs3.getString(3));
                            k.setStrKmkmei(rs3.getString(4));
                            k.setFloHensaAllNow(rs3.getFloat(scoreC));
                            // 偏差値から学力レベルを算出する
                            // 新テストの場合には、平均点なので学力レベルを設定しない。
                            if (!newExam) k.setStrScholarLevel(rs3.getString(10));
                            k.setFloHensaAllLast(rs3.getFloat(scoreP));
                            k.setFloDiffer(rs3.getFloat(7));
                            k.setBasicFlg(rs3.getString(11));

                            if (rs3.getInt(3) >= 7000) c1.add(k); // 型
                            else c2.add(k); // 科目
                        }
                        rs3.close();

                        // 型・科目の順番に詰める
                        a.getC13KyokaDataList().addAll(c1);
                        a.getC13KyokaDataList().addAll(c2);
                    }
                    rs2.close();
                }
            } finally {
                DbUtils.closeQuietly(rs1);
                DbUtils.closeQuietly(rs2);
                DbUtils.closeQuietly(rs3);
                DbUtils.closeQuietly(ps1);
                DbUtils.closeQuietly(ps2);
                DbUtils.closeQuietly(ps3);
            }
        }

        // 型・科目別比較フラグが1なら型・科目別データリストを作る
        if (data.getIntKmkHikakuFlg() == 1) {
            String[] code = super.getSubjectCDArray(); // 型・科目設定値

            PreparedStatement ps1 = null;
            PreparedStatement ps2 = null;
            PreparedStatement ps3 = null;
            ResultSet rs1 = null;
            ResultSet rs2 = null;
            ResultSet rs3 = null;
            try {
                // 担当クラス
                ps1 = conn.prepareStatement(QueryLoader.getInstance().load("c11_1").toString());

                // データリストの取得
                Query query1 = QueryLoader.getInstance().load("c13_4");
                query1.setStringArray(1, code); // 型・科目コード
                query1.setStringArray(2, code); // 型・科目コード

                ps2 = conn.prepareStatement(query1.toString());
                ps2.setString(1, exam.getExamYear()); // 対象年度
                ps2.setString(2, exam.getExamCD()); // 対象模試

                // 型・科目リスト
                Query query2 = QueryLoader.getInstance().load("c13_3");

                // 偏差値での絞込み
                if (!"".equals(data.getStrKeyHensa()) && !newExam) {
                    query2.append("AND ri.a_deviation >= ? ");
                }

                switch (data.getIntSortFlg()) {
                    case  1: query2.append("ORDER BY grade DESC, class, class_no"); break;
                    case  2: query2.append("ORDER BY a_deviation1 DESC, grade DESC, class, class_no"); break;
                    case  3: query2.append("ORDER BY diff DESC, a_deviation1 DESC, grade DESC, class, class_no"); break;
                }

                ps3 = conn.prepareStatement(query2.toString());
                ps3.setString(1, login.getUserID()); // 学校コード
                ps3.setString(2, current); // 現在の年度
                ps3.setString(5, login.getUserID()); // 学校コード
                ps3.setString(6, current); // 現在の年度
                ps3.setString(8, pastExam1.getExamYear()); // 前回模試年度
                ps3.setString(9, pastExam1.getExamCD()); // 前回模試コード
                ps3.setString(11, pastExam2.getExamYear()); // 前々回模試年度
                ps3.setString(12, pastExam2.getExamCD()); // 前々回模試コード
                ps3.setString(14, exam.getExamYear()); // 模試年度
                ps3.setString(15, exam.getExamCD()); // 模試コード

                if (!"".equals(data.getStrKeyHensa()) && !newExam) {
                    ps3.setFloat(17, Float.parseFloat(data.getStrKeyHensa()));
                }

                rs1 = ps1.executeQuery();
                while (rs1.next()) {
                    C13KmkDataListBean kb = new C13KmkDataListBean();
                    kb.setStrGrade(rs1.getString(1));
                    kb.setStrClass(rs1.getString(3));
                    data.getC13KmkDataList().add(kb);

                    // 型・科目別データリスト
                    List c1 = new LinkedList(); // 型の入れ物
                    List c2 = new LinkedList(); // 科目の入れ物

                    // 学年
                    ps3.setInt(3, rs1.getInt(1));
                    // クラス
                    ps3.setString(4, rs1.getString(2));
                    ps3.setString(7, rs1.getString(2));

                    rs2 = ps2.executeQuery();
                    while (rs2.next()) {
                        C13KmkListBean k = new C13KmkListBean();
                        k.setStrKmkCd(rs2.getString(1));
                        k.setStrKmkmei(rs2.getString(2));

                        if (rs2.getInt(1) >= 7000) c1.add(k); // 型
                        else c2.add(k); // 科目

                        // 型・科目コード
                        ps3.setString(10, rs2.getString(1));
                        ps3.setString(13, rs2.getString(1));
                        ps3.setString(16, rs2.getString(1));

                        rs3 = ps3.executeQuery();
                        while (rs3.next()) {
                            C13KmkSeisekiListBean s = new C13KmkSeisekiListBean();
                            s.setStrGrade(rs3.getString(1));
                            s.setStrClass(rs3.getString(2));
                            s.setStrClassNo(rs3.getString(3));
                            s.setStrKanaName(rs3.getString(4));
                            s.setStrSex(rs3.getString(5));
                            s.setStrHaitenNow(rs3.getString(6));
                            s.setIntTokutenNow(rs3.getInt(7));
                            s.setFloHensaAllNow(rs3.getFloat(8));
                            s.setStrHaitenLast1(rs3.getString(9));
                            s.setIntTokutenLast1(rs3.getInt(10));
                            s.setFloHensaAllLast1(rs3.getFloat(11));
                            s.setStrHaitenLast2(rs3.getString(12));
                            s.setIntTokutenLast2(rs3.getInt(13));
                            s.setFloHensaAllLast2(rs3.getFloat(14));
                            s.setStrScholarLevelNow(GeneralUtil.toBlank(rs3.getString(16)));
                            s.setStrScholarLevelLast1(GeneralUtil.toBlank(rs3.getString(17)));
                            s.setStrScholarLevelLast2(GeneralUtil.toBlank(rs3.getString(18)));
                            k.getC13KmkSeisekiList().add(s);
                        }
                        rs3.close();
                    }
                    rs2.close();

                    // 型・科目の順番に詰める
                    kb.getC13KmkList().addAll(c1);
                    kb.getC13KmkList().addAll(c2);
                }
            } finally {
                DbUtils.closeQuietly(rs1);
                DbUtils.closeQuietly(rs2);
                DbUtils.closeQuietly(rs3);
                DbUtils.closeQuietly(ps1);
                DbUtils.closeQuietly(ps2);
                DbUtils.closeQuietly(ps3);
            }
        }
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
     */
    protected double calcNumberOfPrint() {
        return getNumberOfPrint("C13_01");
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
     */
    protected String getCategory() {
        return IProfileCategory.C_COND_TRANS;
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
     */
    protected boolean createSheet() {
        return new C13()
            .c13(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
    }

}

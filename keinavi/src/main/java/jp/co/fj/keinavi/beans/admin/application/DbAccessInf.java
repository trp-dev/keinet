package jp.co.fj.keinavi.beans.admin.application;
/*
 * $(#) $ld$
 *
 * All Rights Reserved, Copyright(c) 2004 FUJITSU KANSAI-CHUBU NETTECH LIMITED.
 *
 */

import java.io.*;
import java.util.*;

import jp.co.fj.keinavi.beans.admin.name.SystemLogWriter;


/**
 *
 * DB設定値を取得するクラス。
 *
 * @author $Author$
 * @author Yukari OKUMURA
 * @version $Revision: 1.00 $, $Date: 2004/03/31 17:30:00 $
 * @since 1.00
 *
 */
public class DbAccessInf {

	private static final String CONST_VALIDATE_QUERY_ORACLE = "select * from dual";


	private static String _loginId;
	private static String _password;
	private static String _url;
	private static String _driver;

	/**
	 *
	 * クラスローダから呼び出されたら、DB接続初期設定を行う。
	 *
	 */
	static {
		initialize();
	}


	/**
	 *
	 * ＤＢ接続の初期設定ファイルの読み込み。
	 *
	 * @return void
	 * @throws java.lang.RuntimeException dbconf.propertiesの読み込みに失敗した場合にスローされる。
	 * @exception java.lang.RuntimeException dbconf.propertiesの読み込みに失敗した場合にスローされる。
	 * @see java.lang.RuntimeException
	 */
	private static void initialize(){

		// プロパティファイルを使った初期化メソッドを呼び出す
		try {
			propertyFileInit(new File(Application.getResourcesDir(), "common.properties"));
		} catch (IOException e) {
			throw new InternalError(e.getMessage());
		}
	}

	/**
	 *
	 * DB接続用初期設定。
	 *
	 * @param propertyFile ファイル
	 * @return void
	 * @throws java.lang.RuntimeException dbconf.propertiesの読み込みに失敗した場合にスローされる。
	 * @exception java.lang.RuntimeException dbconf.propertiesの読み込みに失敗した場合にスローされる。
	 * @see java.lang.RuntimeException
	 */
	private static void propertyFileInit(File propertyFile) throws IOException {
		FileInputStream fin = null;
		try{
			//-----------------------------------------
			// -- プロパティの取得
			//-----------------------------------------

			fin = new FileInputStream(propertyFile);
			Properties props = new Properties();
			props.load( fin );

			String login = props.getProperty("NDBConnUserID");
			if( login == null ){
				throw new IOException("NDBConnUserIDプロパティが指定されていません。");
			}
			String pass = props.getProperty("NDBConnUserPass");
			if( pass == null ){
				throw new IOException("NDBConnUserPassプロパティが指定されていません。");
			}

			String driver = props.getProperty("NDBDriver");
			if( driver == null ){
				throw new IOException("NDBDriverプロパティが指定されていません。");
			}

			String url = props.getProperty("NDBURL");
			if( url == null ){
				throw new IOException("NDBURLプロパティが指定されていません。");
			}
			init( login, pass, driver, url);

		} catch(FileNotFoundException e) {
			SystemLogWriter.printLog(new DbAccessInf(), e.getMessage(), SystemLogWriter.WARNING);
			throw new IOException("common.propertiesファイルが見つかりません。");
		} finally {
			if (fin != null) fin.close();
		}
	}


	/**
	 *
	 * DB接続の初期化。
	 *
	 * @param host ホスト
	 * @param loginId　ログインID
	 * @param password　パスワード
	 * @param url　URL
	 * @return void
	 * @throws java.lang.RuntimeException DB接続初期化に失敗した場合にスローされる。
	 * @exception java.lang.RuntimeException DB接続初期化に失敗した場合にスローされる。
	 * @see java.lang.RuntimeException
	 */
	private static void init(
		String loginId,
		String password,
		String driver,
		String url){

		_loginId = loginId;
		_password = password;
		_driver = driver;
		_url = url;
		//ドライバのロード
		try {
			Class.forName(_driver);
		} catch (ClassNotFoundException ex) {
			SystemLogWriter.printLog(new DbAccessInf(), ex.getMessage(), SystemLogWriter.WARNING);
			throw new RuntimeException(
				"JDBCドライバが見つかりません。");
		}

	}


	/**
	 *
	 * ログインIDを取得する。
	 *
	 * @return ログインID。
	 */
	public static String getLoginId(){
		return _loginId;
	}


	/**
	 *
	 * パスワードを取得する。
	 *
	 * @return パスワード。
	 */
	public static String getPassWord(){
		return _password;
	}


	/**
	 *
	 * ドライバを取得する。
	 *
	 * @return ドライバ。
	 */
	public static String getDriver(){
		return _driver;
	}


	/**
	 *
	 * URLを取得する。
	 *
	 * @return URL。
	 */
	public static String getURL(){
		return _url;
	}
}

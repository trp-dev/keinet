package jp.co.fj.keinavi.beans.security.config;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 *
 * 機能の詳細を表すクラス
 *
 * 2005.10.7	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class Function {

	private String id; // 機能ID
	private String category; // カテゴリ
	private boolean isIndividual; // 個人系メニューかどうか
	private boolean isMaintenance; // メンテナンスメニューかどうか
	private boolean isManager; // 利用者管理メニューかどうか

	/* (非 Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {

		return new ToStringBuilder(this)
			.append("ID", id)
			.append("Category", category)
			.append("isIndividual", isIndividual)
			.append("isMaintenance", isMaintenance)
			.append("isManager", isManager)
			.toString();
	}

	/**
	 * @return category を戻します。
	 */
	public String getCategory() {
		return category;
	}


	/**
	 * @param category 設定する category。
	 */
	public void setCategory(String category) {
		this.category = category;
	}


	/**
	 * @return id を戻します。
	 */
	public String getId() {
		return id;
	}


	/**
	 * @param id 設定する id。
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * @return isIndividual を戻します。
	 */
	public boolean isIndividual() {
		return isIndividual;
	}


	/**
	 * @param isIndividual 設定する isIndividual。
	 */
	public void setIndividual(boolean isIndividual) {
		this.isIndividual = isIndividual;
	}

	/**
	 * @return isMaintenance を戻します。
	 */
	public boolean isMaintenance() {
		return isMaintenance;
	}

	/**
	 * @param isMaintenance 設定する isMaintenance。
	 */
	public void setMaintenance(boolean isMaintenance) {
		this.isMaintenance = isMaintenance;
	}

	/**
	 * @return isManager を戻します。
	 */
	public boolean isManager() {
		return isManager;
	}

	/**
	 * @param isManager 設定する isManager。
	 */
	public void setManager(boolean isManager) {
		this.isManager = isManager;
	}

}

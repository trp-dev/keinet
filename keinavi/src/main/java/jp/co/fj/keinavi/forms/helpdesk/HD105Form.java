package jp.co.fj.keinavi.forms.helpdesk;

import com.fjh.forms.ActionForm;

/**
 * ユーザ管理（詳細参照）画面サーブレット
 * 
 * 2004.09.22	NINOMIYA - TOTEC
 * 				新規作成
 *
 * <2010年度マーク高２模試対応>
 * 2011.02.28	Tomohisa YAMADA - TOTEC
 * 				ヘルプデスクでid・pw初期化
 * 
 * @author NINOMIYA - TOTEC
 * @version 1.0
 */
public class HD105Form extends ActionForm {

	private String userId = null;
	
	//アクションモード 1:利用者IDを初期化、2:ログインパスワードを初期化
	private String actionMode;

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}

	/**
	 * ActionModeを取得する。
	 * @return
	 */
	public String getActionMode() {
		return actionMode;
	}

	/**
	 * ActionModeを設定する。
	 * @param actionMode
	 */
	public void setActionMode(String actionMode) {
		this.actionMode = actionMode;
	}

	/**
	 * @return
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}


}

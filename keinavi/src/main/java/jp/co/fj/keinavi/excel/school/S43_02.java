/**
 * 校内成績分析−他校比較　設問別成績（他校比較）
 * 	Excelファイル編集
 * 作成日: 2004/08/11
 * @author	T.Sakai
 * 
 *  2009.10.13   Fujito URAKAWA - Totec
 *              「全体成績」追加対応
 * 
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S43GakkoListBean;
import jp.co.fj.keinavi.excel.data.school.S43Item;
import jp.co.fj.keinavi.excel.data.school.S43ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S43_02 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
	private boolean bolBookCngFlg = true;			//改ファイルフラグ
	private boolean bolSheetCngFlg = true;			//改シートフラグ
	
	final private String masterfile0 = "S43_02";
	final private String masterfile1 = "S43_02";
	private String masterfile = "";
	final private int intMaxSheetSr = 40;	//MAXシート数の値を入れる

/*
 * 	Excel編集メイン
 * 		S43Item s43Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s43_02EditExcel(S43Item s43Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
 		
		HSSFWorkbook	workbook	= null;
		HSSFSheet		workSheet	= null;
		HSSFRow			workRow		= null;
		HSSFCell		workCell	= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s43Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		int		intMaxSheetIndex	= 0;		//ファイルのシートの最大数
		int		row					= 0;		//行
		int		col					= 0;		//列
		int		gakkoCnt			= 0;		//他校カウンタ(自校＋他校)
		int		setsumonCnt			= 0;		//設問カウンタ
		String		kmk					= "";		//科目チェック用
		int		maxGakko			= 0;		//MAX他校数(自校＋他校)
		int		sheetListIndex		= 0;		//各科目でのシートカウンター
		int		fileIndex			= 1;		//ファイルカウンター
		int		houjiNum			= 5;		//1シートに表示できる他校数
		int		sheetRowCnt			= 0;		//他校用改シート格納用カウンタ
		ArrayList	workbookList	= new ArrayList();
		ArrayList	workSheetList	= new ArrayList();
//add 2004/10/27 T.Sakai データ0件対応
		int		dispGakkoFlgCnt		= 0;		// グラフ表示フラグカウンタ
//add end

		// 基本ファイルを読込む
		S43ListBean s43ListBean = new S43ListBean();
		
		try {
			
			// データセット
			ArrayList s43List = s43Item.getS43List();
			Iterator itr = s43List.iterator();
			
			if( itr.hasNext() == false ){
				return errfdata;
			}
			
			while( itr.hasNext() ) {
				s43ListBean = (S43ListBean)itr.next();

//add 2004.10.22 設問名Null時の処理回避 → 2004.12.21 設問がない科目も出力する
//				if ( !cm.toString(s43ListBean.getStrSetsuMei1()).equals("") ) {
//add end					
					if (s43ListBean.getIntDispKmkFlg()==1) {
						//科目が変わる時のチェック
						if ( !cm.toString(kmk).equals(cm.toString(s43ListBean.getStrKmkCd())) ) {
							setsumonCnt = 0;
							bolSheetCngFlg = true;
						}
						//10設問ごとにシート名リスト初期化
						if (setsumonCnt==0) {
							workSheetList	= new ArrayList();
							sheetListIndex = 0;
						}
						// workbookListにWorkBookが２つ以上格納されている時は出力処理を実行
						int listSize = workbookList.size();
						if(listSize>1){
							if(setsumonCnt == 0){
								workbook = (HSSFWorkbook)workbookList.get(0);
			
								// Excelファイル保存
								boolean bolRet = false;
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, intMaxSheetSr);
								fileIndex++;
								if( bolRet == false ){
									return errfwrite;
								}
		
								// ファイル出力したデータは削除
								workbookList.remove(0);
		
								// WorkBook・変数を書き込んでる途中のものに戻す
								workbook = (HSSFWorkbook)workbookList.get(0);
							}
						}
						
						// 基本ファイルを読込む
						S43GakkoListBean s43GakkoListBean = new S43GakkoListBean();
						
						// 他校データセット
						ArrayList s43GakkoList = s43ListBean.getS43GakkoList();
						Iterator itrGakko = s43GakkoList.iterator();
						
						maxGakko = 0;
						//他校データ件数取得
						while ( itrGakko.hasNext() ){
							s43GakkoListBean = (S43GakkoListBean)itrGakko.next();
							if (s43GakkoListBean.getIntDispGakkoFlg()==1) {
								maxGakko++;
							}
						}
						// 他校表示に必要なシート数の計算
						sheetRowCnt = (maxGakko-1)/(houjiNum-1);
						if((maxGakko-1)%(houjiNum-1)!=0){
							sheetRowCnt++;
						}
						if (sheetRowCnt==0) {
							sheetRowCnt++;
						}
						bolSheetCngFlg = true;
						
						String gakko = "";
						int ninzu = 0;
						float tokuritsu = 0;
						
						itrGakko = s43GakkoList.iterator();
						
						while ( itrGakko.hasNext() ){
							s43GakkoListBean = (S43GakkoListBean)itrGakko.next();
							if (s43GakkoListBean.getIntDispGakkoFlg()==1) {
								if( bolBookCngFlg == true ){
									//マスタExcel読み込み
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook==null ){
										return errfread;
									}
									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
									workbookList.add(workbook);
								}
								//他校数が5以上または10設問ごとに改シート
								if ( bolSheetCngFlg == true ) {
									// データセットするシートの選択
									if (setsumonCnt==0) {
										//他校用改シート
										workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex+1));
										workSheetList.add(sheetListIndex, workSheet);
										intMaxSheetIndex++;
										sheetListIndex++;
									} else {
										//次列用シートの呼び出し
										workSheet = (HSSFSheet)workSheetList.get(sheetListIndex-sheetRowCnt);
										sheetRowCnt--;
									}
									if (setsumonCnt==0) {
										// ヘッダ右側に帳票作成日時を表示する
										cm.setHeader(workbook, workSheet);
									
										// セキュリティスタンプセット
										String secuFlg = cm.setSecurity( workbook, workSheet, s43Item.getIntSecuFlg() ,31 ,33 );
										workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
										workCell.setCellValue(secuFlg);
									
										// 注釈セット
										if (s43Item.getIntShubetsuFlg() == 1){
										} else{
											workCell = cm.setCell( workSheet, workRow, workCell, 45, 33 );
											workCell.setCellValue( "※校内の平均得点率を上回る高校の平均得点率に\"*\"を表示しています。" );
										}

										// 学校名セット
										workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
										workCell.setCellValue( "学校名　：" + cm.toString(s43Item.getStrGakkomei()) );
				
										// 模試月取得
										String moshi =cm.setTaisyouMoshi( s43Item.getStrMshDate() );
										// 対象模試セット
										workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
										workCell.setCellValue( "対象模試：" + cm.toString(s43Item.getStrMshmei()) + moshi);
										
										// 科目名＋配点セット
										String haiten = "";
										if ( !cm.toString(s43ListBean.getStrHaitenKmk()).equals("") ) {
											haiten = "（" + s43ListBean.getStrHaitenKmk() + "）";
										}
										workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
										workCell.setCellValue( "型・科目：" + cm.toString(s43ListBean.getStrKmkmei()) + haiten );
										col = 1;
									}
									row = 46;
//delete 2004.10.29 T.Sakai 設問セット条件削除
//									if ( setsumonCnt==0 || !cm.toString(setsumon).equals(cm.toString(s43ListBean.getStrSetsuMei1())) ) {
//delete end
										// 設問番号セット
										workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
										workCell.setCellValue( s43ListBean.getStrSetsuNo() );
										// 設問内容セット
										workCell = cm.setCell( workSheet, workRow, workCell, row++, col );
										workCell.setCellValue( s43ListBean.getStrSetsuMei1() );
										// 設問配点セット
										workCell = cm.setCell( workSheet, workRow, workCell, row, col );
										if (!cm.toString(s43ListBean.getStrSetsuHaiten()).equals("") ){
											workCell.setCellValue( "（" + s43ListBean.getStrSetsuHaiten() + "）" );
										}
//delete 2004.10.29 T.Sakai 設問セット条件削除
//									}
//delete end
									bolSheetCngFlg = false;
								}
								if ( gakkoCnt==0 ) {
									row = 50;
									if (col==1) {
										// 学校名セット
										gakko = s43GakkoListBean.getStrGakkomei();
										workCell = cm.setCell( workSheet, workRow, workCell, row, col-1 );
										workCell.setCellValue( s43GakkoListBean.getStrGakkomei() );
									}
									// 人数セット
									ninzu = s43GakkoListBean.getIntNinzu();
									workCell = cm.setCell( workSheet, workRow, workCell, row, col );
									if ( s43GakkoListBean.getIntNinzu() != -999 ) {
										workCell.setCellValue( s43GakkoListBean.getIntNinzu() );
									}
									// 得点率セット
									tokuritsu = s43GakkoListBean.getFloTokuritsu();
									workCell = cm.setCell( workSheet, workRow, workCell, row++, col+2 );
									if ( s43GakkoListBean.getFloTokuritsu() != -999.0 ) {
										workCell.setCellValue( s43GakkoListBean.getFloTokuritsu() );
									}
								} else {
									if ( gakkoCnt%houjiNum==0 ) {
										row = 50;
										if (col==1) {
											// 学校名セット
											workCell = cm.setCell( workSheet, workRow, workCell, row, col-1 );
											workCell.setCellValue( gakko );
										}
										// 人数セット
										workCell = cm.setCell( workSheet, workRow, workCell, row, col );
										if ( ninzu != -999 ) {
											workCell.setCellValue( ninzu );
										}
										// 得点率セット
										workCell = cm.setCell( workSheet, workRow, workCell, row++, col+2 );
										if ( tokuritsu != -999.0 ) {
											workCell.setCellValue( tokuritsu );
										}
										gakkoCnt++;
									}
									if (col==1) {
										// 学年+他校名セット
										workCell = cm.setCell( workSheet, workRow, workCell, row, col-1 );
										workCell.setCellValue( s43GakkoListBean.getStrGakkomei() );
									}
									// 人数セット
									workCell = cm.setCell( workSheet, workRow, workCell, row, col );
									if ( s43GakkoListBean.getIntNinzu() != -999 ) {
										workCell.setCellValue( s43GakkoListBean.getIntNinzu() );
									}
									// *セット
									// 得点用のときは処理しない
									if (s43Item.getIntShubetsuFlg() == 1){
									} else{
										if ( s43GakkoListBean.getFloTokuritsu() > tokuritsu ) {
											workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
											workCell.setCellValue("*");
										}
									}
									// 得点率セット
									workCell = cm.setCell( workSheet, workRow, workCell, row++, col+2 );
									if ( s43GakkoListBean.getFloTokuritsu() != -999.0 ) {
										workCell.setCellValue( s43GakkoListBean.getFloTokuritsu() );
									}
								}
								gakkoCnt++;
								
								if(gakkoCnt%houjiNum==0){
									if(itrGakko.hasNext()){
										if(setsumonCnt==0){
											if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
												bolBookCngFlg = true;
											}
										}
										bolSheetCngFlg = true;
									}
								}
								if(itrGakko.hasNext()==false){
									if(setsumonCnt==0){
										if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
											bolBookCngFlg = true;
										}
										bolSheetCngFlg = true;
									}
								}
							}
						}
						kmk = s43ListBean.getStrKmkCd();
						col = col + 3;
						gakkoCnt = 0;
						setsumonCnt++;
						if ( setsumonCnt==11 ) {
							bolSheetCngFlg = true;
							setsumonCnt = 0;
							if(intMaxSheetIndex%intMaxSheetSr==0 && intMaxSheetIndex!=0){
								bolBookCngFlg = true;
							}
						}
		
						// workbookListにWorkBookが２つ以上格納されている時は出力処理を実行
						listSize = workbookList.size();
						if(listSize>1){
							if(setsumonCnt == 0){
								workbook = (HSSFWorkbook)workbookList.get(0);
			
								// Excelファイル保存
								boolean bolRet = false;
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, intMaxSheetSr);
								fileIndex++;
								if( bolRet == false ){
									return errfwrite;
								}
		
								// ファイル出力したデータは削除
								workbookList.remove(0);
		
								// WorkBook・変数を書き込んでる途中のものに戻す
								workbook = (HSSFWorkbook)workbookList.get(0);
							}
						}
//add 2004/10/27 T.Sakai データ0件対応
							  dispGakkoFlgCnt++;
//add end
					}
//add 2004.10.22 設問名Null時の処理回避 → 2004.12.21 設問がない科目も出力する
//				}
//add end
			}

			// 改ブック後に1科目しかないと保存されずにここに来る
			// workbookListにWorkBookが２つ以上格納されている時は出力処理を実行
			if(workbookList.size() > 1){
				workbook = (HSSFWorkbook)workbookList.get(0);

				// Excelファイル保存
				boolean bolRet = false;
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, intMaxSheetSr);
				fileIndex++;
				if( bolRet == false ){
					return errfwrite;
				}

				// ファイル出力したデータは削除
				workbookList.remove(0);

				// WorkBook・変数を書き込んでる途中のものに戻す
				workbook = (HSSFWorkbook)workbookList.get(0);
			}

//add 2004/10/26 T.Sakai データ0件対応
			if ( dispGakkoFlgCnt==0 ) {
				//マスタExcel読み込み
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				if( workbook==null ){
					return errfread;
				}
				intMaxSheetIndex = 0;
				
				// データセットするシートの選択
				workSheet = workbook.getSheet(String.valueOf(intMaxSheetIndex+1));
				intMaxSheetIndex++;
				
				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);
						
				// セキュリティスタンプセット
				String secuFlg = cm.setSecurity( workbook, workSheet, s43Item.getIntSecuFlg() ,31 ,33 );
				workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
				workCell.setCellValue(secuFlg);
									
				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s43Item.getStrGakkomei()) );
				
				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s43Item.getStrMshDate() );
				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "対象模試：" + cm.toString(s43Item.getStrMshmei()) + moshi);
			}
//add end
			
			// 現ワークブックに有効シートがあるなら保存
			if (intMaxSheetIndex > 0) {
				// Excelファイル保存
				boolean bolRet = false;
				if(fileIndex != 1){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, intMaxSheetIndex);
				}
				else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}
	
				if( bolRet == false ){
					return errfwrite;					
				}
			}
			
		} catch(Exception e) {
			log.Err("S43_02","データセットエラー",e.toString());
			return errfdata;
		}
		
		return noerror;
	}

}
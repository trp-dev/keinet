package jp.co.fj.keinavi.servlets.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 *
 * 利用者情報確認画面サーブレット
 * 
 * 2005.10.13	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U101Servlet extends DefaultHttpServlet {

	// U002Servletのインスタンス
	private static final U002Servlet u002 = new U002Servlet();
	
	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		// 転送先がJSP
		if (getJspId().equals(getForward(request))) {

			// ログインセッション
			final LoginSession login = getLoginSession(request);

			// 利用者データをリクエストにセットする
			request.setAttribute("LoginUserData",
						u002.getLoginUserData(request, login, login.getAccount()));
			
			forward2Jsp(request, response);
			
		// 不明なら転送
		} else {
			dispatch(request, response);
		}
	}
	
	/**
	 * @return JSPのID
	 */
	protected String getJspId() {
		return "u101";
	}
	
}

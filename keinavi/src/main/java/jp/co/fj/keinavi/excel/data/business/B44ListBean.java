/*
 * �쐬��: 2004/09/06
 *
 * ���Z���ѕ��́|�u�]��w�]���ʐl���i���Z�Ԕ�r�j
 * @author cmurata
 */
package jp.co.fj.keinavi.excel.data.business;

import java.util.ArrayList;

public class B44ListBean {
	
	//���������敪
	private String strGenKouKbn = "";
	//��w�R�[�h
	private String strDaigakuCd = "";
	//��w��
	private String strDaigakuMei = "";
	//�w���R�[�h
	private String strGakubuCd = "";
	//�w����
	private String strGakubuMei = "";
	//�w�ȃR�[�h
	private String strGakkaCd = "";
	//�w�Ȗ�
	private String strGakkaMei = "";
	//�����R�[�h
	private String strNtiCd = "";
	//����
	private String strNtiMei = "";
	//�]���敪
	private String strHyoukaKbn = "";
	//�w�Z�ʕ]���l���f�[�^���X�g
	private ArrayList b44GakkoList = new ArrayList(); 

	/**
	 * @GET
	 */
	public ArrayList getB44GakkoList() {
		return this.b44GakkoList;
	}

	public String getStrHyoukaKbn() {
		return this.strHyoukaKbn;
	}

	public String getStrDaigakuCd() {
		return this.strDaigakuCd;
	}

	public String getStrDaigakuMei() {
		return this.strDaigakuMei;
	}

	public String getStrGakkaCd() {
		return this.strGakkaCd;
	}

	public String getStrGakkaMei() {
		return this.strGakkaMei;
	}

	public String getStrGakubuCd() {
		return this.strGakubuCd;
	}

	public String getStrGakubuMei() {
		return this.strGakubuMei;
	}

	public String getStrGenKouKbn() {
		return this.strGenKouKbn;
	}

	public String getStrNtiCd() {
		return this.strNtiCd;
	}

	public String getStrNtiMei() {
		return this.strNtiMei;
	}

	/**
	 * @SET
	 */
	public void setB44GakkoList(ArrayList b44GakkoList) {
		this.b44GakkoList = b44GakkoList;
	}

	public void setStrHyoukaKbn(String strHyoukaKbn) {
		this.strHyoukaKbn = strHyoukaKbn;
	}

	public void setStrDaigakuCd(String strDaigakuCd) {
		this.strDaigakuCd = strDaigakuCd;
	}

	public void setStrDaigakuMei(String strDaigakuMei) {
		this.strDaigakuMei = strDaigakuMei;
	}

	public void setStrGakkaCd(String strGakkaCd) {
		this.strGakkaCd = strGakkaCd;
	}

	public void setStrGakkaMei(String strGakkaMei) {
		this.strGakkaMei = strGakkaMei;
	}

	public void setStrGakubuCd(String strGakubuCd) {
		this.strGakubuCd = strGakubuCd;
	}

	public void setStrGakubuMei(String strGakubuMei) {
		this.strGakubuMei = strGakubuMei;
	}

	public void setStrGenKouKbn(String strGenKouKbn) {
		this.strGenKouKbn = strGenKouKbn;
	}

	public void setStrNtiCd(String strNtiCd) {
		this.strNtiCd = strNtiCd;
	}

	public void setStrNtiMei(String strNtiMei) {
		this.strNtiMei = strNtiMei;
	}

}

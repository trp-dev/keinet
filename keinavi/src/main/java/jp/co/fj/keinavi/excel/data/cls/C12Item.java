package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−個人成績データクラス
 * 作成日: 2004/07/14
 * @author      H.Fujimoto
 */
public class C12Item {
        //学校名
        private String strGakkomei = "";
        //模試名
        private String strMshmei = "";
        //模試実施基準日
        private String strMshDate = "";
        //表フラグ
        private int intHyouFlg = 0;
        //順位表フラグ
        private int intJyuniFlg = 0;
        //順位付けフラグ
        private int intRankFlg = 0;
        //志望大学表示フラグ
        private int intShiboFlg = 0;
        //セキュリティスタンプ
        private int intSecuFlg = 0;
        //成績表データリスト
        private ArrayList c12List = new ArrayList();
        //順位表データリスト
        private ArrayList c12JyuniList = new ArrayList();
        //出力種別フラグ → 新テスト用に追加
        private int intShubetsuFlg = 0;
        //模試コード
        private String strMshCd = "";


        /*----------*/
        /* Get      */
        /*----------*/

        public ArrayList getC12JyuniList() {
                return this.c12JyuniList;
        }
        public ArrayList getC12List() {
                return this.c12List;
        }
        public int getIntHyouFlg() {
                return this.intHyouFlg;
        }
        public int getIntJyuniFlg() {
                return this.intJyuniFlg;
        }
        public int getIntRankFlg() {
            return this.intRankFlg;
        }
        public int getIntSecuFlg() {
                return this.intSecuFlg;
        }
        public int getIntShiboFlg() {
                return this.intShiboFlg;
        }
        public String getStrGakkomei() {
                return this.strGakkomei;
        }
        public String getStrMshDate() {
                return this.strMshDate;
        }
        public String getStrMshmei() {
                return this.strMshmei;
        }
        public int getIntShubetsuFlg() {
                return this.intShubetsuFlg;
        }
        public String getStrMshCd() {
                return this.strMshCd;
        }

        /*----------*/
        /* Set      */
        /*----------*/

        public void setC12JyuniList(ArrayList c12JyuniList) {
                this.c12JyuniList = c12JyuniList;
        }
        public void setC12List(ArrayList c12List) {
                this.c12List = c12List;
        }
        public void setIntHyouFlg(int intHyouFlg) {
                this.intHyouFlg = intHyouFlg;
        }
        public void setIntJyuniFlg(int intJyuniFlg) {
                this.intJyuniFlg = intJyuniFlg;
        }
        public void setIntRankFlg(int intRankFlg) {
               this.intRankFlg = intRankFlg;
        }
        public void setIntSecuFlg(int intSecuFlg) {
                this.intSecuFlg = intSecuFlg;
        }
        public void setIntShiboFlg(int intShiboFlg) {
                this.intShiboFlg = intShiboFlg;
        }
        public void setStrGakkomei(String strGakkomei) {
                this.strGakkomei = strGakkomei;
        }
        public void setStrMshDate(String strMshDate) {
                this.strMshDate = strMshDate;
        }
        public void setStrMshmei(String strMshmei) {
                this.strMshmei = strMshmei;
        }
        public void setIntShubetsuFlg(int intShubetsuFlg) {
                this.intShubetsuFlg = intShubetsuFlg;
        }
        public void setStrMshCd(String strMshCd) {
                this.strMshCd = strMshCd;
        }
}
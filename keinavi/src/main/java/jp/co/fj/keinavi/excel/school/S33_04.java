/**
 * 校内成績分析−クラス比較　設問別成績（クラス比較）
 *      Excelファイル編集
 * 作成日: 2019/09/11
 * @author      M.Ooseto
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S33Item;
import jp.co.fj.keinavi.excel.data.school.S33StatusBean;
import jp.co.fj.keinavi.excel.data.school.S33StatusClassListBean;
import jp.co.fj.keinavi.excel.data.school.S33StatusListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S33_04 {

    // 帳票作成結果
    // 0:正常終了
    // 1:ファイル読込みエラー
    // 2:ファイル書込みエラー
    // 3:データセットエラー
    private int noerror = 0;
    private int errfread = 1;
    private int errfwrite = 2;
    private int errfdata = 3;

    // 共通関数用クラス インスタンス
    private CM cm = new CM();

    // 帳票テンプレートファイル
    private String masterfile = "S33_04";

    // 出力固定値
    private String BUNDLENAME = "学校名　：";
    private String EXAMNAME = "対象模試：";
    private String CLASSNAME = "クラス";
    private String ERRORMESSAGE = "データセットエラー";

    // 固定値
    private int QUESTION = 3;

    // 出力項目位置
    public enum CURSOL {
        // 問1
        STATUS_1_START(5,1),
        STATUS_1_END(25,16),
        // 問2
        STATUS_2_START(26,1),
        STATUS_2_END(46,16),
        // 問3
        STATUS_3_START(47,1),
        STATUS_3_END(67,16);

        private final int col;
        private final int row;

        private CURSOL(int row, int col) {
            this.col = col;
            this.row = row;
        }
        public int getCol() {
            return this.col;
        }
        public int getRow() {
            return this.row;
        }

    }

    /**
     * Excel編集メイン
     * @param s33Item データクラス
     * @param outfilelist 出力Excelファイル名（フルパス）
     * @param intSaveFlg 1:保存 2:印刷 3:保存／印刷
     * @param UserID ユーザーID
     * @return 帳票作成結果
     */
    public int s33_04EditExcel(S33Item s33Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

        KNLog log = KNLog.getInstance(null,null,null);
        HSSFWorkbook workbook = null;
        HSSFSheet workSheet = null;
        HSSFRow workRow = null;
        HSSFCell workCell = null;

        // マスタExcel読み込み
        workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);

        // マスタExcel読込みエラー
        if( workbook==null ){
            return errfread;
        }

        workSheet = workbook.cloneSheet(0);

        // データ検索結果取得
        S33StatusBean result = (S33StatusBean) s33Item.getS33StatusList().get(0);

        try {
            // 行
            int row = 0;
            // 列
            int col = 0;
            // 出力位置
            CURSOL cursol[][] = {
                    {CURSOL.STATUS_1_START,CURSOL.STATUS_1_END},
                    {CURSOL.STATUS_2_START,CURSOL.STATUS_2_END},
                    {CURSOL.STATUS_3_START,CURSOL.STATUS_3_END}};

            int index = 0;
            int classIndex = 0;
            int questionNo = 0;

            // ヘッダ右側に帳票作成日時を表示する
            cm.setHeader(workbook, workbook.cloneSheet(0));

            // セキュリティスタンプセット
            String secuFlg = cm.setSecurity(workbook, workSheet, s33Item.getIntSecuFlg() ,15 ,16);
            workCell = cm.setCell(workSheet, workRow, workCell, 0, 15);
            workCell.setCellValue(secuFlg);

            // 学校名
            workCell = cm.setCell(workSheet, workRow, workCell, 1, 0);
            workCell.setCellValue(BUNDLENAME + result.getBundleName());

            // 対象模試
            workCell = cm.setCell(workSheet, workRow, workCell, 2, 0);
            workCell.setCellValue(EXAMNAME + result.getExamName());

            // 小設問別正答状況データ
            List <S33StatusListBean> list1 = result.getS33StatusList();
            List <S33StatusClassListBean> list2 = result.getS33StatusClassList();
            row = cursol[questionNo][0].getRow();
            col = cursol[questionNo][0].getCol() + 1;
            while(index < list1.size()) {

                // 校内全体データ出力
                S33StatusListBean data1 = list1.get(index);
                // 受験者数出力
                setValue(workSheet, workRow, row, col, data1.getNumbers(), 0, 0);
                col++;
                // 人数(割合)出力
                setValue(workSheet, workRow, row, col, data1.getCorrectPersonnum1(), data1.getCorrectAnsrate1(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data1.getCorrectPersonnum2(), data1.getCorrectAnsrate2(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data1.getCorrectPersonnum3(), data1.getCorrectAnsrate3(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data1.getCorrectPersonnum4(), data1.getCorrectAnsrate4(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data1.getCorrectPersonnum5(), data1.getCorrectAnsrate5(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data1.getCorrectPersonnum6(), data1.getCorrectAnsrate6(), 1);
                col += 2;
                setValue(workSheet, workRow, row, col, data1.getCorrectPersonnum7(), data1.getCorrectAnsrate7(), 1);

                // クラスデータ出力
                col = cursol[questionNo][0].getCol();
                row++;
                while(classIndex < list2.size() && row <= cursol[questionNo][1].getRow()) {
                    S33StatusClassListBean data2 = list2.get(classIndex);
                    // 設問番号が異なる場合は次の設問番号データ出力へ
                    if(Integer.parseInt(data2.getQuestionNo()) != questionNo + 1) {
                        break;
                    }
                    // クラス名出力
                    workCell = cm.setCell(workSheet, workRow, workCell, row, col);
                    workCell.setCellValue(data2.getKokugoClass() + CLASSNAME);
                    col++;
                    // 受験者数出力
                    setValue(workSheet, workRow, row, col, data2.getNumbers(), 0, 0);
                    col++;
                    // 人数(割合)出力
                    setValue(workSheet, workRow, row, col, data2.getCorrectPersonnum1(), data2.getCorrectAnsrate1(), 1);
                    col += 2;
                    setValue(workSheet, workRow, row, col, data2.getCorrectPersonnum2(), data2.getCorrectAnsrate2(), 1);
                    col += 2;
                    setValue(workSheet, workRow, row, col, data2.getCorrectPersonnum3(), data2.getCorrectAnsrate3(), 1);
                    col += 2;
                    setValue(workSheet, workRow, row, col, data2.getCorrectPersonnum4(), data2.getCorrectAnsrate4(), 1);
                    col += 2;
                    setValue(workSheet, workRow, row, col, data2.getCorrectPersonnum5(), data2.getCorrectAnsrate5(), 1);
                    col += 2;
                    setValue(workSheet, workRow, row, col, data2.getCorrectPersonnum6(), data2.getCorrectAnsrate6(), 1);
                    col += 2;
                    setValue(workSheet, workRow, row, col, data2.getCorrectPersonnum7(), data2.getCorrectAnsrate7(), 1);

                    classIndex++;
                    row++;
                    col = cursol[questionNo][0].getCol();
                }

                questionNo++;
                if(questionNo > QUESTION - 1) {
                    break;
                }

                // 次の設問番号の出力位置
                row = cursol[questionNo][0].getRow();
                col = cursol[questionNo][0].getCol() + 1;

                index++;
            }

            // Excel書込み
            boolean bolRet = false;
            bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, 1);

            // Excel書込みエラー
            if( bolRet == false ){
                return errfwrite;
            }

        }catch(Exception e){
            log.Err(masterfile, ERRORMESSAGE, e.toString());
            return errfdata;
        }

        return noerror;
    }

    /**
     * Excelにデータ出力
     * @param workSheet Excelシート情報
     * @param workRow Excel行情報
     * @param row 行
     * @param col 列
     * @param number 人数
     * @param compratio 割合
     * @param flg 0:受験者数 1:人数(割合)
     */
    public void setValue(HSSFSheet workSheet, HSSFRow workRow, int row, int col, int number, float compratio,int flg) {

        HSSFCell workCell = null;

        if(flg == 0) {
            // 受験者数の表示
            if(number > 0) {
                workCell = cm.setCell(workSheet, workRow, workCell, row, col);
                workCell.setCellValue(number);
            }
        }
        else {
            // 人数、割合が0以外の場合は表示
            if(number > 0 && compratio > Float.parseFloat("0.0")) {
                workCell = cm.setCell(workSheet, workRow, workCell, row, col);
                workCell.setCellValue(number);
                workCell = cm.setCell(workSheet, workRow, workCell, row, col + 1 );
                workCell.setCellValue("(" + compratio + ")");
            }
        }

    }
}

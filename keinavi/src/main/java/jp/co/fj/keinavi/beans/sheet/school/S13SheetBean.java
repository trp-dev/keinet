/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.beans.sheet.NoQuestionException;
import jp.co.fj.keinavi.excel.data.school.S13Item;
import jp.co.fj.keinavi.excel.data.school.S13KokugoQueListBean;
import jp.co.fj.keinavi.excel.data.school.S13MarkAreaListBean;
import jp.co.fj.keinavi.excel.data.school.S13MarkListBean;
import jp.co.fj.keinavi.excel.data.school.S13RecordListBean;
import jp.co.fj.keinavi.excel.data.school.S13SeisekiListBean;
import jp.co.fj.keinavi.excel.data.school.S13SetumonList;
import jp.co.fj.keinavi.excel.data.school.S13SougouList;
import jp.co.fj.keinavi.excel.data.school.S13SugakuKokugoQuiListBean;
import jp.co.fj.keinavi.excel.data.school.S13SugakuQueListBean;
import jp.co.fj.keinavi.excel.school.S13;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 2005.02.16 Yoshimoto KAWAI - Totec
 *            県名をカッコで括るように修正
 *
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 *
 * <2010年度改修>
 * 2009.10.06 Fujito URAKAWA - Totec
 *            「全体成績」追加対応
 *
 * 2009.10.20 Fujito URAKAWA - Totec
 *            新校内資料「科目・設問別正答率（レベル別）」対応
 *
 * 2010.01.08   Tomohisa YAMADA - Totec
 *              センター・リサーチ５段階評価対応
 *
 * 2010.01.21	Fujito URAKAWA - Totec
 *              成績層別成績が6段階（A〜F）から7段階に変更
 *
 *
 * @author kawai
 *
 */
public class S13SheetBean extends AbstractSheetBean {

	// データクラス
	private final S13Item data = new S13Item();
	//2019/09/09 QQ)nagai 共通テスト対応 ADD START
	// 科目コード
	private String SUBCD = "";
	//2019/09/09 QQ)nagai 共通テスト対応 ADD END

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// 型・科目設定値
		final String[] code = getSubjectCDArray();
		// 型・科目グラフ表示対象
		final List graph = getSubjectGraphList();

		String tmp_kmkcd = "";	// 科目コードチェック用変数

		// 設問別成績存在チェック
		if (hasAllNoQuestionSubject(code)) {
			throw new NoQuestionException();
		}

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrKenmei("（" + getPrefName() + "）"); // 県名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntHyouFlg(getIntFlag(IProfileItem.CHART)); // 表フラグ
		data.setIntGraphFlg(getIntFlag(IProfileItem.GRAPH)); // グラフフラグ
		data.setIntSecuFlg(getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(getOutputType()); // 出力種別フラグ

		// マーク模試正答状況フラグはマーク系の模試でなければ0
		if (!"1".equals(exam.getDockingType())) {
			data.setIntMarkFlg(0);
			data.setIntMarkAreaFlg(0);
			// 2020/03/03 QQ)Ooseto 共通テスト対応(不具合修正) DEL START
			//// 2019/09/04 QQ)Tanioka 記述式問題詳細分析 追加 ADD START
			//data.setIntKokugoFlg(0);
			//data.setIntStatusFlg(0);
			//記述系模試の場合
			//if("2".equals(exam.getDockingType())) {
			//	data.setIntRecordFlg(getIntFlag(IProfileItem.OPTION_CHECK_RECORD));
			//}else {
			//	data.setIntRecordFlg(0);
			//}
			//// 2019/09/04 QQ)Tanioka 記述式問題詳細分析 追加 ADD END
			// 2020/03/03 QQ)Ooseto 共通テスト対応(不具合修正) DEL END
		// マーク系の模試なら設定値
		} else {
			data.setIntMarkFlg(getIntFlag(IProfileItem.IND_MARK_EXAM));
			data.setIntMarkAreaFlg(getIntFlag(IProfileItem.IND_MARK_EXAM_AREA));
			// 2020/03/03 QQ)Ooseto 共通テスト対応(不具合修正) DEL START
			//// 2019/09/04 QQ)Tanioka 記述式問題詳細分析 追加 ADD START
			//data.setIntKokugoFlg(getIntFlag(IProfileItem.OPTION_CHECK_KOKUGO));
			//data.setIntStatusFlg(getIntFlag(IProfileItem.OPTION_CHECK_STATUS));
			//data.setIntRecordFlg(0);
			//// 2019/09/04 QQ)Tanioka 記述式問題詳細分析 追加 ADD END
			// 2020/03/03 QQ)Ooseto 共通テスト対応(不具合修正) DEL END
		}

				// 2020/03/03 QQ)Ooseto 共通テスト対応(不具合修正) ADD START
				// 記述
				if (Boolean.valueOf(KNUtil.isWrtnExam2(exam)) || Boolean.valueOf(KNUtil.isPStage(exam))) {
					data.setIntKokugoFlg(0);
					data.setIntStatusFlg(0);
					data.setIntRecordFlg(getIntFlag(IProfileItem.OPTION_CHECK_RECORD));
				// マーク
				} else {
					data.setIntKokugoFlg(getIntFlag(IProfileItem.OPTION_CHECK_KOKUGO));
					data.setIntStatusFlg(getIntFlag(IProfileItem.OPTION_CHECK_STATUS));
					data.setIntRecordFlg(0);
				}
				// 2020/03/03 QQ)Ooseto 共通テスト対応(不具合修正) ADD END

		// 表またはグラフフラグが1なら設問別成績データリストを作る
		if (data.getIntHyouFlg() == 1 || data.getIntGraphFlg() == 1) {

			PreparedStatement ps = null;
			PreparedStatement ps1 = null;
			ResultSet rs = null;
			try {
				// 設問別成績データリスト
				final Query query = QueryLoader.getInstance().load("s13_1");
				query.setStringArray(1, code); // 科目コード

				ps = conn.prepareStatement(query.toString());
				ps.setString(1, login.getPrefCD()); // 県コード
				ps.setString(2, profile.getBundleCD()); // 一括コード
				ps.setString(3, exam.getExamYear()); // 対象年度
				ps.setString(4, exam.getExamCD()); // 対象模試

				// 科目別成績データリスト
				{
					ps1 = conn.prepareStatement(QueryLoader.getInstance().load("s13_3").toString());
					ps1.setString(1, exam.getExamYear()); // 対象年度
					ps1.setString(2, exam.getExamCD()); // 対象模試
					ps1.setString(3, login.getPrefCD()); // 県コード
					ps1.setString(4, profile.getBundleCD()); // 一括コード
					ps1.setString(5, exam.getExamYear()); // 対象年度
					ps1.setString(6, exam.getExamCD()); // 対象模試

				}

				rs = ps.executeQuery();
				while (rs.next()) {
					// 科目コードが異なる場合、「全体成績」をbeanにセットする。
					if (!tmp_kmkcd.equals(rs.getString(1))) {

						ps1.setString(7, rs.getString(1)); // 科目コード
						putTotalSubjectData(ps1, graph);

					}
					S13SeisekiListBean bean = new S13SeisekiListBean();
					bean.setStrKmkCd(rs.getString(1));
					bean.setStrKmkmei(rs.getString(2));
					bean.setIntDispKmkFlg(graph.contains(rs.getString(1)) ? 1 : 0);
					bean.setStrHaitenKmk(rs.getString(3));
					bean.setStrSetsuNo(rs.getString(4));
					bean.setStrSetsuMei1(rs.getString(5));
					bean.setStrSetsuHaiten(rs.getString(6));
					bean.setIntNinzuHome(rs.getInt(7));
					bean.setFloTokuritsuHome(rs.getFloat(8));
					bean.setIntNinzuKen(rs.getInt(9));
					bean.setFloTokuritsuKen(rs.getFloat(10));
					bean.setIntNinzuAll(rs.getInt(11));
					bean.setFloTokuritsuAll(rs.getFloat(12));

					bean.setIntNinzuHomeS(rs.getInt(13));
					bean.setFloTokuritsuHomeS(rs.getFloat(14));
					bean.setIntNinzuHomeA(rs.getInt(15));
					bean.setFloTokuritsuHomeA(rs.getFloat(16));
					bean.setIntNinzuHomeB(rs.getInt(17));
					bean.setFloTokuritsuHomeB(rs.getFloat(18));
					bean.setIntNinzuHomeC(rs.getInt(19));
					bean.setFloTokuritsuHomeC(rs.getFloat(20));
					bean.setIntNinzuHomeD(rs.getInt(21));
					bean.setFloTokuritsuHomeD(rs.getFloat(22));
					bean.setIntNinzuHomeE(rs.getInt(23));
					bean.setFloTokuritsuHomeE(rs.getFloat(24));
					bean.setIntNinzuHomeF(rs.getInt(25));
					bean.setFloTokuritsuHomeF(rs.getFloat(26));

					bean.setNumADevzoneS(rs.getInt(27));
					bean.setAvgrateADevzoneS(rs.getFloat(28));
					bean.setNumADevzoneA(rs.getInt(29));
					bean.setAvgrateADevzoneA(rs.getFloat(30));
					bean.setNumADevzoneB(rs.getInt(31));
					bean.setAvgrateADevzoneB(rs.getFloat(32));
					bean.setNumADevzoneC(rs.getInt(33));
					bean.setAvgrateADevzoneC(rs.getFloat(34));
					bean.setNumADevzoneD(rs.getInt(35));
					bean.setAvgrateADevzoneD(rs.getFloat(36));
					bean.setNumADevzoneE(rs.getInt(37));
					bean.setAvgrateADevzoneE(rs.getFloat(38));
					bean.setNumADevzoneF(rs.getInt(39));
					bean.setAvgrateADevzoneF(rs.getFloat(40));
					data.getS13SeisekiList().add(bean);

					// 処理中の科目コードをセット
					tmp_kmkcd = rs.getString(1);

				}
			} finally {
				DbUtils.closeQuietly(null, ps, rs);
				DbUtils.closeQuietly(ps1);
			}
		}

		// マーク模試正答状況フラグが1ならマーク模試正答状況データリストを作る
		if (data.getIntMarkFlg() == 1) {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				final Query query = QueryLoader.getInstance().load("s13_2");
				query.setStringArray(1, code); // 科目コード

				ps = conn.prepareStatement(query.toString());
				ps.setString(1, profile.getBundleCD()); // 一括コード
				ps.setString(2, exam.getExamYear()); // 対象年度
				ps.setString(3, exam.getExamCD()); // 対象模試

				rs = ps.executeQuery();
				while (rs.next()) {
					S13MarkListBean bean = new S13MarkListBean();
					bean.setStrKmkCd(rs.getString(1));
					bean.setStrKmkmei(rs.getString(2));
					bean.setStrHaitenKmk(rs.getString(3));
					bean.setIntNinzu(rs.getInt(4));
					bean.setStrKaitoNo(rs.getString(5));
					bean.setIntKantoRcFlg(rs.getInt(6));
					bean.setIntKantoFlg(rs.getInt(7));
					bean.setStrHaiten(rs.getString(8));
					bean.setStrSeito(rs.getString(9));
					bean.setFloSeitoritsuHome(rs.getFloat(10));
					bean.setFloSeitoritsuAll(rs.getFloat(11));
					bean.setStrGoto1(rs.getString(12));
					bean.setFloGoto1Markritsu(rs.getFloat(13));
					bean.setStrGoto2(rs.getString(14));
					bean.setFloGoto2Markritsu(rs.getFloat(15));
					bean.setStrGoto3(rs.getString(16));
					bean.setFloGoto3Markritsu(rs.getFloat(17));
					bean.setStrDaimonNaiyo(rs.getString(18));
					data.getS13MarkList().add(bean);
				}
			} finally {
				DbUtils.closeQuietly(null, ps , rs);
			}
		}

		// マーク模試レベル別設問別正答率フラグが1ならマーク模試レベル別設問別正答率データリストを作る
		if (data.getIntMarkAreaFlg() == 1) {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				final Query query = QueryLoader.getInstance().load("s13_4");
				query.setStringArray(1, code); // 科目コード

				ps = conn.prepareStatement(query.toString());
				ps.setString(1, exam.getExamYear());
				ps.setString(2, exam.getExamCD());
				//2019/09/13 QQ)Oosaki 共通テスト対応 UPD START
				//ps.setString(3, exam.getExamYear());
				//ps.setString(4, exam.getExamCD());
				//ps.setString(5, profile.getBundleCD());
				//ps.setString(6, profile.getBundleCD());
				//ps.setString(7, profile.getBundleCD());
				//ps.setString(8, profile.getBundleCD());
				//ps.setString(9, profile.getBundleCD());
				//ps.setString(10, profile.getBundleCD());
				//ps.setString(11, profile.getBundleCD());
				//ps.setString(12, profile.getBundleCD());
				//ps.setString(13, profile.getBundleCD());
				//ps.setString(14, profile.getBundleCD());
				//ps.setString(15, profile.getBundleCD());
				//ps.setString(16, profile.getBundleCD());
				//ps.setString(17, profile.getBundleCD());
				//ps.setString(18, profile.getBundleCD());
				//ps.setString(19, profile.getBundleCD());
				//ps.setString(20, profile.getBundleCD());
				//ps.setString(21, exam.getExamYear());
				//ps.setString(22, exam.getExamCD());
				ps.setString(3, profile.getBundleCD());
				ps.setString(4, profile.getBundleCD());
				ps.setString(5, profile.getBundleCD());
				ps.setString(6, profile.getBundleCD());
				ps.setString(7, profile.getBundleCD());
				ps.setString(8, profile.getBundleCD());
				ps.setString(9, profile.getBundleCD());
				ps.setString(10, profile.getBundleCD());
				ps.setString(11, profile.getBundleCD());
				ps.setString(12, exam.getExamYear());
                                ps.setString(13, exam.getExamCD());
				//2019/09/13 QQ)Oosaki 共通テスト対応 UPD END

				rs = ps.executeQuery();
				while (rs.next()) {
					S13MarkAreaListBean bean = new S13MarkAreaListBean();
					bean.setStrKmkCd(rs.getString(1));			// 科目コード
					bean.setStrKmkmei(rs.getString(2));			// 科目名
					bean.setStrHaitenKmk(rs.getString(3));		// 配点
					bean.setStrMonnum(rs.getString(4));			// 設問番号
					bean.setStrDaimonNaiyo(rs.getString(5));	// 大問・内容
					//2019/09/20 QQ)Oosaki 共通テスト対応 UPD START
					//bean.setIntKantoRcFlg(rs.getInt(6));		// 完答レコードフラグ
					//bean.setIntKantoFlg(rs.getInt(7));		// 完答問題フラグ
					bean.setIntRecordFlg(rs.getInt(6));         // レコードフラグ
					bean.setIntSetqFlg(rs.getInt(7));           // セット問題フラグ
					//2019/09/20 QQ)Oosaki 共通テスト対応 UPD END
					bean.setStrKaitoNo(rs.getString(8));		// 解答番号
					bean.setIntNinzuAll(rs.getInt(9));			// 科目別人数（全国）
					bean.setIntNinzu(rs.getInt(10));			// 科目別人数（校内）
					bean.setFloSeitoritsuAll(rs.getFloat(11));	// 科目別正答率[全国]（全体）
					bean.setFloSeitoritsuHome(rs.getFloat(12));	// 科目別正答率[校内]（全体）

					bean.setIntNinzuS(rs.getInt(13));			// 人数[成績層S]（校内）
					bean.setFloSeitoritsuAllS(rs.getFloat(14));	// 正答率[成績層S]（全国）
					bean.setFloSeitoritsuHomeS(rs.getFloat(15));// 正答率[成績層S]（校内）

					bean.setIntNinzuA(rs.getInt(16));			// 人数[成績層A]（校内）
					bean.setFloSeitoritsuAllA(rs.getFloat(17));	// 正答率[成績層A]（全国）
					bean.setFloSeitoritsuHomeA(rs.getFloat(18));// 正答率[成績層A]（校内）

					bean.setIntNinzuB(rs.getInt(19));			// 人数[成績層B]（校内）
					bean.setFloSeitoritsuAllB(rs.getFloat(20));	// 正答率[成績層B]（全国）
					bean.setFloSeitoritsuHomeB(rs.getFloat(21));// 正答率[成績層B]（校内）

					bean.setIntNinzuC(rs.getInt(22));			// 人数[成績層C]（校内）
					bean.setFloSeitoritsuAllC(rs.getFloat(23));	// 正答率[成績層C]（全国）
					bean.setFloSeitoritsuHomeC(rs.getFloat(24));// 正答率[成績層C]（校内）

					bean.setIntNinzuD(rs.getInt(25));			// 人数[成績層D]（校内）
					bean.setFloSeitoritsuAllD(rs.getFloat(26));	// 正答率[成績層D]（全国）
					bean.setFloSeitoritsuHomeD(rs.getFloat(27));// 正答率[成績層D]（校内）

					bean.setIntNinzuE(rs.getInt(28));			// 人数[成績層E]（校内）
					bean.setFloSeitoritsuAllE(rs.getFloat(29));	// 正答率[成績層E]（全国）
					bean.setFloSeitoritsuHomeE(rs.getFloat(30));// 正答率[成績層E]（校内）

					bean.setIntNinzuF(rs.getInt(31));			// 人数[成績層F]（校内）
					bean.setFloSeitoritsuAllF(rs.getFloat(32));	// 正答率[成績層F]（全国）
					bean.setFloSeitoritsuHomeF(rs.getFloat(33));// 正答率[成績層F]（校内）

					data.getS13MarkAreaList().add(bean);
				}
			}
			finally {
				DbUtils.closeQuietly(null, ps , rs);
			}
		}

		// 2019/09/04 QQ)Tanioka 帳票出力(暫定)処理追加 ADD START
		// 国語 評価別人数フラグが1なら国語 評価別人数データリストを作る
		if (data.getIntKokugoFlg() == 1) {

			// 2019/09/09 QQ)Tanouchi 帳票出力処理追加 ADD START
			PreparedStatement ps = null;
			PreparedStatement ps2 = null;
			ResultSet rs = null;
			ResultSet rs2 = null;
			SUBCD = "3915";				// 国語の科目コード

			try {
				{
					final Query query = QueryLoader.getInstance().load("s13_5_1");

					ps = conn.prepareStatement(query.toString());
					ps.setString(1, SUBCD);						// 科目コード
					ps.setString(2, profile.getBundleCD());		// 一括コード
					ps.setString(3, exam.getExamYear());		// 模試年度
					ps.setString(4, exam.getExamCD());			// 模試コード
				}
				{
					final Query query = QueryLoader.getInstance().load("s13_5_2");
					ps2 = conn.prepareStatement(query.toString());
					ps2.setString(1, SUBCD);					// 科目コード
					ps2.setString(2, profile.getBundleCD());	// 一括コード
					ps2.setString(3, exam.getExamYear());		// 模試年度
					ps2.setString(4, exam.getExamCD());			// 模試コード
				}

				rs = ps.executeQuery();
				while (rs.next()) {
					S13SetumonList Setumon = new S13SetumonList();
					Setumon.setQuestion_No(rs.getString(1));	// 設問番号
					// 2019/11/28 QQ)Ooseto 国語記述設問変更対応 UPD START
					Setumon.setS_aHyoukaNin(rs.getInt(2));		// 高校a評価人数
                                        Setumon.setS_aAstHyoukaNin(rs.getInt(3));       // 高校a*評価人数
					//Setumon.setS_bHyoukaNin(rs.getInt(3));	// 高校b評価人数
                                        Setumon.setS_bHyoukaNin(rs.getInt(4));
                                        Setumon.setS_bAstHyoukaNin(rs.getInt(5));       // 高校b*評価人数
					//Setumon.setS_cHyoukaNin(rs.getInt(4));	// 高校c評価人数
                                        Setumon.setS_cHyoukaNin(rs.getInt(6));
					//Setumon.setS_dHyoukaNin(rs.getInt(5));	// 高校d評価人数
					//Setumon.setS_aHyoukaWari(rs.getFloat(6));	// 高校a評価割合％
                                        Setumon.setS_aHyoukaWari(rs.getFloat(7));
                                        Setumon.setS_aAstHyoukaWari(rs.getFloat(8));    // 高校a*評価割合％
					//Setumon.setS_bHyoukaWari(rs.getFloat(7));	// 高校b評価割合％
                                        Setumon.setS_bHyoukaWari(rs.getFloat(9));
                                        Setumon.setS_bAstHyoukaWari(rs.getFloat(10));   // 高校b*評価割合％
					//Setumon.setS_cHyoukaWari(rs.getFloat(8));	// 高校c評価割合％
                                        Setumon.setS_cHyoukaWari(rs.getFloat(11));
					//Setumon.setS_dHyoukaWari(rs.getFloat(9));	// 高校d評価割合％
					//Setumon.setA_aHyoukaNin(rs.getInt(10));       // 全国a評価人数
					Setumon.setA_aHyoukaNin(rs.getInt(12));
					Setumon.setA_aAstHyoukaNin(rs.getInt(13));      // 全国a*評価人数
					//Setumon.setA_bHyoukaNin(rs.getInt(11));	// 全国b評価人数
					Setumon.setA_bHyoukaNin(rs.getInt(14));
					Setumon.setA_bAstHyoukaNin(rs.getInt(15));      // 全国b*評価人数
					//Setumon.setA_cHyoukaNin(rs.getInt(12));	// 全国c評価人数
					Setumon.setA_cHyoukaNin(rs.getInt(16));
					//Setumon.setA_dHyoukaNin(rs.getInt(13));	// 全国d評価人数
					//Setumon.setA_aHyoukaWari(rs.getFloat(14));	// 全国a評価割合％
					Setumon.setA_aHyoukaWari(rs.getFloat(17));
					Setumon.setA_aAstHyoukaWari(rs.getFloat(18));   // 全国a*評価割合％
					//Setumon.setA_bHyoukaWari(rs.getFloat(15));	// 全国b評価割合％
					Setumon.setA_bHyoukaWari(rs.getFloat(19));
					Setumon.setA_bAstHyoukaWari(rs.getFloat(20));   // 全国b*評価割合％
					//Setumon.setA_cHyoukaWari(rs.getFloat(16));	// 全国c評価割合％
					Setumon.setA_cHyoukaWari(rs.getFloat(21));
					//Setumon.setA_dHyoukaWari(rs.getFloat(17));	// 全国d評価割合％
					// 2019/11/28 QQ)Ooseto 国語記述設問変更対応 UPD END
					data.getS13KokugoSetumon().add(Setumon);
				}
				rs.close();

				rs2 = ps2.executeQuery();
				while (rs2.next()) {
					S13SougouList Sougou = new S13SougouList();
					Sougou.setS_AHyoukaNin(rs2.getInt(1));		// 高校A評価人数
					Sougou.setS_BHyoukaNin(rs2.getInt(2));		// 高校B評価人数
					Sougou.setS_CHyoukaNin(rs2.getInt(3));		// 高校C評価人数
					Sougou.setS_DHyoukaNin(rs2.getInt(4));		// 高校D評価人数
					Sougou.setS_EHyoukaNin(rs2.getInt(5));		// 高校E評価人数
					Sougou.setS_AHyoukaWari(rs2.getFloat(6));	// 高校A評価割合％
					Sougou.setS_BHyoukaWari(rs2.getFloat(7));	// 高校B評価割合％
					Sougou.setS_CHyoukaWari(rs2.getFloat(8));	// 高校C評価割合％
					Sougou.setS_DHyoukaWari(rs2.getFloat(9));	// 高校D評価割合％
					Sougou.setS_EHyoukaWari(rs2.getFloat(10));	// 高校E評価割合％
					Sougou.setA_AHyoukaNin(rs2.getInt(11));		// 全国A評価人数
					Sougou.setA_BHyoukaNin(rs2.getInt(12));		// 全国B評価人数
					Sougou.setA_CHyoukaNin(rs2.getInt(13));		// 全国C評価人数
					Sougou.setA_DHyoukaNin(rs2.getInt(14));		// 全国D評価人数
					Sougou.setA_EHyoukaNin(rs2.getInt(15));		// 全国E評価人数
					Sougou.setA_AHyoukaWari(rs2.getFloat(16));	// 全国A評価割合％
					Sougou.setA_BHyoukaWari(rs2.getFloat(17));	// 全国B評価割合％
					Sougou.setA_CHyoukaWari(rs2.getFloat(18));	// 全国C評価割合％
					Sougou.setA_DHyoukaWari(rs2.getFloat(19));	// 全国D評価割合％
					Sougou.setA_EHyoukaWari(rs2.getFloat(20));	// 全国E評価割合％
					data.getS13KokugoSougou().add(Sougou);
				}
				rs2.close();
			}
			finally {
				DbUtils.closeQuietly(null, ps , rs);
				DbUtils.closeQuietly(null, ps2 , rs2);
			}
		}
		// 2019/09/09 QQ)Tanouchi 帳票出力処理追加 ADD END

				//2019/09/09 QQ)nagai 共通テスト対応。 ADD START
		// 小設問別正答状況フラグが1なら小設問別正答状況データリストを作る
		if (data.getIntStatusFlg() == 1) {

			PreparedStatement ps1 = null;
			PreparedStatement ps2 = null;
			ResultSet rs1 = null;
			ResultSet rs2 = null;
			SUBCD = "3915"; //国語記述の科目コード
			S13SugakuKokugoQuiListBean bean = new S13SugakuKokugoQuiListBean();
			try {
				//数学のデータを取得
				{
					ps1 = conn.prepareStatement(QueryLoader.getInstance().load("s13_6s").toString());
				    ps1.setString(1, profile.getBundleCD()); // 自校
					ps1.setString(2, exam.getExamYear()); // 模試年度
				    ps1.setString(3, exam.getExamCD()); // 模試コード

				}
				//国語のデータを取得
				{
					ps2 = conn.prepareStatement(QueryLoader.getInstance().load("s13_6k").toString());
				    ps2.setString(1, profile.getBundleCD()); // 自校
					ps2.setString(2, exam.getExamYear()); // 模試年度
				    ps2.setString(3, exam.getExamCD()); // 模試コード
				    ps2.setString(4, SUBCD); // 科目コード

				}

					//数学のデータをセット
					rs1 = ps1.executeQuery();
					while (rs1.next()) {
						S13SugakuQueListBean list = new S13SugakuQueListBean();
						list.setStrKmkCd(rs1.getString(1));			// 科目コード
						list.setStrKmkmei(rs1.getString(2));		// 科目名
						list.setStrMonnum(rs1.getString(3));		// 設問番号
						list.setStrQuestionName(rs1.getString(4));	// 設問名称
						list.setStrHaiten(rs1.getString(5));		// 配点
						list.setFloHomeAvgpnt(rs1.getFloat(6));		// 校内平均点
						list.setFloAllAvgpnt(rs1.getFloat(7));		// 全国平均点

						bean.getS13SugakuQueList().add(list);

				}
					//国語のデータをセット
					rs2 = ps2.executeQuery();
					while (rs2.next()) {
						S13KokugoQueListBean list2 = new S13KokugoQueListBean();
						list2.setFloSeitoritsuHome1(rs2.getFloat(1));	// 校内正答率1
						list2.setFloSeitoritsuAll1(rs2.getFloat(2));	// 全国正答率1
						list2.setFloSeitoritsuHome2(rs2.getFloat(3));	// 校内正答率2
						list2.setFloSeitoritsuAll2(rs2.getFloat(4));	// 全国正答率2
						//2019/09/27 QQ)Oosaki ロジック修正 UPD START
						//list2.setFloSeitoritsuAll3(rs2.getFloat(5));	// 校内正答率3
						//list2.setFloSeitoritsuHome3(rs2.getFloat(6));	// 全国正答率3
						//list2.setFloSeitoritsuAll4(rs2.getFloat(7));	// 校内正答率4
						//list2.setFloSeitoritsuHome4(rs2.getFloat(8));	// 全国正答率4
						//list2.setFloSeitoritsuAll5(rs2.getFloat(9));	// 校内正答率5
						//list2.setFloSeitoritsuHome5(rs2.getFloat(10));// 全国正答率5
						//list2.setFloSeitoritsuAll6(rs2.getFloat(11));	// 校内正答率6
						//list2.setFloSeitoritsuHome6(rs2.getFloat(12));// 全国正答率6
						//list2.setFloSeitoritsuAll7(rs2.getFloat(13));	// 校内正答率7
						//list2.setFloSeitoritsuHome7(rs2.getFloat(14));// 全国正答率7
						list2.setFloSeitoritsuHome3(rs2.getFloat(5));   // 校内正答率3
						list2.setFloSeitoritsuAll3(rs2.getFloat(6));    // 全国正答率3
						list2.setFloSeitoritsuHome4(rs2.getFloat(7));   // 校内正答率4
						list2.setFloSeitoritsuAll4(rs2.getFloat(8));    // 全国正答率4
						list2.setFloSeitoritsuHome5(rs2.getFloat(9));   // 校内正答率5
						list2.setFloSeitoritsuAll5(rs2.getFloat(10));   // 全国正答率5
						list2.setFloSeitoritsuHome6(rs2.getFloat(11));  // 校内正答率6
						list2.setFloSeitoritsuAll6(rs2.getFloat(12));   // 全国正答率6
						list2.setFloSeitoritsuHome7(rs2.getFloat(13));  // 校内正答率7
						list2.setFloSeitoritsuAll7(rs2.getFloat(14));   // 全国正答率7
						//2019/09/27 QQ)Oosaki ロジック修正 UPD END
						bean.getS13KokugoQueList().add(list2);
				}
			}
			finally {
				DbUtils.closeQuietly(ps1);
				DbUtils.closeQuietly(ps2);
				DbUtils.closeQuietly(rs1);
				DbUtils.closeQuietly(rs2);
			}
			data.getS13SugakuKokugoQueList().add(bean);
		}
		//2019/09/09 QQ)nagai 共通テスト対応。 ADD END

		// 小設問別成績フラグが1なら小設問別成績データリストを作る
		if (data.getIntRecordFlg() == 1) {
		    // 2019/09/25 QQ)Ooseto 共通テスト対応 ADD START
		    PreparedStatement ps1 = null;
		    PreparedStatement ps2 = null;
                    PreparedStatement ps3 = null;
		    ResultSet rs1 = null;
                    ResultSet rs2 = null;
                    ResultSet rs3 = null;
                    String bundleName = "";
                    String examName = "";

		    try {

                        // 学校名取得
                        ps1 = conn.prepareStatement(QueryLoader.getInstance().load("s33_6").toString());
                        ps1.setString(1, profile.getBundleCD()); // 自校

                        rs1 = ps1.executeQuery();
                        while (rs1.next()) {
                            bundleName = rs1.getString(1);
                        }

                        // 対象模試取得
                        ps2 = conn.prepareStatement(QueryLoader.getInstance().load("s33_7").toString());
                        ps2.setString(1, exam.getExamYear()); // 模試年度
                        ps2.setString(2, exam.getExamCD()); // 模試コード

                        rs2 = ps2.executeQuery();
                        while (rs2.next()) {
                            examName = rs2.getString(1);
                        }

                        // 記述系模試小設問別成績データ取得
                        //ps3 = conn.prepareStatement(QueryLoader.getInstance().load("s13_7").toString());
                        final Query query = QueryLoader.getInstance().load("s13_7");
                        query.setStringArray(1, code); // 科目コード
                        ps3 = conn.prepareStatement(query.toString());
                        ps3.setString(1, profile.getBundleCD());
                        ps3.setString(2, exam.getExamYear());
                        ps3.setString(3, exam.getExamCD());
                        rs3 = ps3.executeQuery();
                        while(rs3.next()) {
                            S13RecordListBean bean = new S13RecordListBean();
                            bean.setBundleName(bundleName);
                            bean.setExamName(examName);
                            bean.setSubCd(rs3.getString(1));
                            bean.setSubName(rs3.getString(2));
                            bean.setQuestionNo(rs3.getString(3));
                            bean.setQuestionName(rs3.getString(4));
                            bean.setKwebShoquestionName(rs3.getString(5));
                            bean.setKwebShoquestionPnt(rs3.getInt(6));
                            bean.setSsAvgPnt(rs3.getFloat(7));
                            bean.setSsAvgScoreRate(rs3.getFloat(8));
                            bean.setSaAvgPnt(rs3.getFloat(9));
                            bean.setSaAvgScoreRate(rs3.getFloat(10));
                            bean.setAcAdemicCd1(rs3.getString(11));
                            bean.setAcAdemicCd2(rs3.getString(12));
                            bean.setAcAdemicCd3(rs3.getString(13));
                            data.getS13RecordList().add(bean);
                        }

                        // 該当データ0件の場合、学校名、対象模試名のみ取得
                        if(data.getS13RecordList().size() == 0) {
                            S13RecordListBean bean = new S13RecordListBean();
                            bean.setBundleName(bundleName);
                            bean.setExamName(examName);
                            data.getS13RecordList().add(bean);
                        }

		    } finally {
		        DbUtils.closeQuietly(null, ps1 , rs1);
                        DbUtils.closeQuietly(null, ps2 , rs2);
                        DbUtils.closeQuietly(null, ps3 , rs3);
		    }
		    // 2019/09/24 QQ)Ooseto 共通テスト対応 ADD END
		}
		// 2019/09/04 QQ)Tanioka 帳票出力(暫定)処理追加 ADD END
	}

	/**
	 * 全体成績のデータをセットする。
	 * @param rs
	 * @param ps1
	 * @param graph
	 * @throws SQLException
	 */
	private void putTotalSubjectData(PreparedStatement ps1,
			List graph) throws SQLException {

		ResultSet rs1 = null;

		try {
			rs1 = ps1.executeQuery();

			if (rs1.next()) {
				S13SeisekiListBean bean = new S13SeisekiListBean();
				bean.setStrKmkCd(rs1.getString(1));
				bean.setStrKmkmei(rs1.getString(2));
				bean.setIntDispKmkFlg(graph.contains(rs1.getString(1)) ? 1 : 0);
				bean.setStrHaitenKmk(rs1.getString(3));
				bean.setStrSetsuNo("");
				bean.setStrSetsuMei1("全体成績");
				bean.setStrSetsuHaiten(rs1.getString(3));
				bean.setIntNinzuHome(rs1.getInt(4));
				bean.setFloTokuritsuHome(rs1.getFloat(5));
				bean.setIntNinzuKen(rs1.getInt(6));
				bean.setFloTokuritsuKen(rs1.getFloat(7));
				bean.setIntNinzuAll(rs1.getInt(8));
				bean.setFloTokuritsuAll(rs1.getFloat(9));

				bean.setIntNinzuHomeS(rs1.getInt(10));
				bean.setFloTokuritsuHomeS(rs1.getFloat(11));
				bean.setIntNinzuHomeA(rs1.getInt(12));
				bean.setFloTokuritsuHomeA(rs1.getFloat(13));
				bean.setIntNinzuHomeB(rs1.getInt(14));
				bean.setFloTokuritsuHomeB(rs1.getFloat(15));
				bean.setIntNinzuHomeC(rs1.getInt(16));
				bean.setFloTokuritsuHomeC(rs1.getFloat(17));
				bean.setIntNinzuHomeD(rs1.getInt(18));
				bean.setFloTokuritsuHomeD(rs1.getFloat(19));
				bean.setIntNinzuHomeE(rs1.getInt(20));
				bean.setFloTokuritsuHomeE(rs1.getFloat(21));
				bean.setIntNinzuHomeF(rs1.getInt(22));
				bean.setFloTokuritsuHomeF(rs1.getFloat(23));

				bean.setNumADevzoneS(rs1.getInt(24));
				bean.setAvgrateADevzoneS(rs1.getFloat(25));
				bean.setNumADevzoneA(rs1.getInt(26));
				bean.setAvgrateADevzoneA(rs1.getFloat(27));
				bean.setNumADevzoneB(rs1.getInt(28));
				bean.setAvgrateADevzoneB(rs1.getFloat(29));
				bean.setNumADevzoneC(rs1.getInt(30));
				bean.setAvgrateADevzoneC(rs1.getFloat(31));
				bean.setNumADevzoneD(rs1.getInt(32));
				bean.setAvgrateADevzoneD(rs1.getFloat(33));
				bean.setNumADevzoneE(rs1.getInt(34));
				bean.setAvgrateADevzoneE(rs1.getFloat(35));
				bean.setNumADevzoneF(rs1.getInt(36));
				bean.setAvgrateADevzoneF(rs1.getFloat(37));
				data.getS13SeisekiList().add(bean);
			}
		}
		finally {
			DbUtils.closeQuietly(rs1);
		}

	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S13_01");
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_SCHOOL_QUE;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S13().s13(data, outfileList, getAction(), sessionKey, sheetLog);
	}

}

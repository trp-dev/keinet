package jp.co.fj.keinavi.servlets.profile;

import java.io.IOException;
import java.sql.Connection;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.profile.CountingDivBean;
import jp.co.fj.keinavi.beans.profile.ProfileFolderBean;
import jp.co.fj.keinavi.beans.profile.ProfileUpdateBean;
import jp.co.fj.keinavi.beans.security.MenuSecurityBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.data.profile.ProfileFolder;
import jp.co.fj.keinavi.forms.profile.W007Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class W007Servlet extends DefaultHttpServlet implements IProfileCategory {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// ログイン情報
		final LoginSession login = getLoginSession(request);
		// プロファイル
		final Profile profile = getProfile(request);

		// アクションフォーム
		final W007Form form = (W007Form) getActionForm(request,
				"jp.co.fj.keinavi.forms.profile.W007Form");

		if ("w007".equals(super.getForward(request))) {
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(false);

				// 集計区分Beanを作る
				if (login.isCountingDiv()) {
					CountingDivBean bean = new CountingDivBean();
					bean.setConnection(null, con);
					bean.setSchoolCD(login.getUserID());
					bean.execute();
					request.setAttribute("CountingDivBean", bean);
				}

				// プロファイルに保存（名称・コメント・保存先フォルダ）
				profile.setProfileName(form.getProfileName());
				profile.setComment(form.getComment());
				profile.setFolderID(form.getCurrent());

				// 一括出力対象をセットする
				{
					Set outItem = CollectionUtil.array2Set(form.getFunction());

					// メニューセキュリティ
					MenuSecurityBean bean = (MenuSecurityBean)
						request.getSession(false).getAttribute("MenuSecurity");

					// 校内成績分析・高校成績分析
					if (outItem.contains("school")) {
						// 校内成績分析・校内成績
						if (bean.isValid("101")) {
							ProfileUtil.setOutItem(profile, S_SCHOOL_SCORE);
							ProfileUtil.setOutItem(profile, S_SCHOOL_DEV);
							ProfileUtil.setOutItem(profile, S_SCHOOL_QUE);
						}
						// 校内成績分析・過年度比較
						if (bean.isValid("102")) {
							ProfileUtil.setOutItem(profile, S_PREV_SCORE);
							ProfileUtil.setOutItem(profile, S_PREV_DEV);
							//ProfileUtil.setOutItem(profile, S_PREV_UNIV);
						}
						// 校内成績分析・クラス比較
						if (bean.isValid("103")) {
							ProfileUtil.setOutItem(profile, S_CLASS_SCORE);
							ProfileUtil.setOutItem(profile, S_CLASS_DEV);
							ProfileUtil.setOutItem(profile, S_CLASS_QUE);
							//ProfileUtil.setOutItem(profile, S_CLASS_UNIV);
						}
						// 校内成績分析・他校比較
						if (bean.isValid("104")) {
							ProfileUtil.setOutItem(profile, S_OTHER_SCORE);
							ProfileUtil.setOutItem(profile, S_OTHER_DEV);
							ProfileUtil.setOutItem(profile, S_OTHER_QUE);
							//ProfileUtil.setOutItem(profile, S_OTHER_UNIV);
						}
						// 校内成績分析・過回比較
						if (bean.isValid("105")) {
							ProfileUtil.setOutItem(profile, S_PAST_SCORE);
							ProfileUtil.setOutItem(profile, S_PAST_DEV);
						}
						// 高校成績分析
						if (bean.isValid("200")) {
							ProfileUtil.setOutItem(profile, S_OTHER_SCORE);
							ProfileUtil.setOutItem(profile, S_OTHER_DEV);
							ProfileUtil.setOutItem(profile, S_OTHER_QUE);
							//ProfileUtil.setOutItem(profile, S_OTHER_UNIV);
							ProfileUtil.setOutItem(profile, S_PAST_SCORE);
						}
					}

					// クラス成績分析
					if (outItem.contains("class")) {
						ProfileUtil.setOutItem(profile, C_COND_DEV);
						ProfileUtil.setOutItem(profile, C_COND_IND);
						ProfileUtil.setOutItem(profile, C_COND_QUE);
						ProfileUtil.setOutItem(profile, C_COND_TRANS);
						//ProfileUtil.setOutItem(profile, C_COND_UNIV);
						ProfileUtil.setOutItem(profile, C_COMP_SCORE);
						ProfileUtil.setOutItem(profile, C_COMP_DEV);
						ProfileUtil.setOutItem(profile, C_COMP_QUE);
						//ProfileUtil.setOutItem(profile, C_COMP_UNIV, true);
					}

					// テキスト出力
					// ※セキュリティ設定はT001Servletにて行う
					if (outItem.contains("text")) {
						ProfileUtil.setOutItem(profile, T_COUNTING_SCORE);
						ProfileUtil.setOutItem(profile, T_COUNTING_DEV);
						ProfileUtil.setOutItem(profile, T_COUNTING_QUE);
						ProfileUtil.setOutItem(profile, T_COUNTING_UNIV);
						ProfileUtil.setOutItem(profile, T_IND_QUE);
						ProfileUtil.setOutItem(profile, T_IND_EXAM2);
						// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD START
						ProfileUtil.setOutItem(profile, T_IND_DESCEXAM);
						// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD END
					}
				}

				// プロファイルフォルダBean
				{
					ProfileFolderBean bean = new ProfileFolderBean();
					bean.setConnection(null, con);
					bean.setLoginSession(login);
					bean.setBundleCD(form.getCountingDivCode());
					bean.execute();

					// フォルダ名称をセットする
					int index = bean.getFolderList().indexOf(new ProfileFolder(profile.getFolderID()));
					if (index >= 0){
						ProfileFolder folder = (ProfileFolder) bean.getFolderList().get(index);
						profile.setFolderName(folder.getFolderName());
					}
				}

				// プロファイルUpdateBean
				{
					ProfileUpdateBean bean = new ProfileUpdateBean();
					bean.setConnection(null, con);
					bean.setLoginSession(login);
					bean.setProfile(profile);
					bean.setOverwriteMode(1);
					bean.execute();
				}

				// アクセスログ
				actionLog(request,"202",
						profile.getProfileID() + "," + profile.getBundleCD());

				con.commit();

			} catch (final Exception e) {
				rollback(con);
				profile.setProfileID(null);
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			request.setAttribute("form", form);

			super.forward(request, response, JSP_W007);

		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

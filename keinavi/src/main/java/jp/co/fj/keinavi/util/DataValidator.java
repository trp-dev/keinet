package jp.co.fj.keinavi.util;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;

/**
 *
 * データ検査メソッド群
 * 
 * 2005.11.08	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class DataValidator {

	// singleton
	private static final DataValidator INSTANCE = new DataValidator();
	// 日付評価用のフォーマッタ
	private final DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
	
	/**
	 * コンストラクタ
	 */
	protected DataValidator() {
		
		// 厳密なチェック
		format.setLenient(false);
	}

	/**
	 * @return DataValidatorのインスタンス
	 */
	public static DataValidator getInstance() {
		return INSTANCE;
	}
	
    /**
     * 文字列に機種依存文字が含まれていないかををチェックします。
     * @param value 文字列
     * @return 機種依存文字が含まれていなければtrue
     */
    public boolean hasPlatformDependent(String value) {
            /*
             * ASCII文字は「x-JIS0208」テーブルに含まれていないようなので削除する。
             * 加えて、Unicode と JIS X 0208 でマッピングが異なる文字も削除する。
             * 【波ダッシュ「〜」の例】
             * JIS X 0208としては「U+301C」にマッピングされることを期待しているが、
             * ユーザ入力値（MS932）をJavaに取り込む際に「U+FF5E」にマッピングされる。
             * 参考: http://www.ingrid.org/java/i18n/encoding/ja-conv.html
             */
            String str = value.replaceAll("[\\x00-\\x7F￣―〜‖−¢£¬]", "");

            /* 半角カナを取り除く */
            str = str.replaceAll("[ｱ-ﾝｦｧｨｩｪｫｯｬｭｮﾞﾟ]", "");

            try {
                    return !new String(str.getBytes("x-JIS0208"), "x-JIS0208").equals(str);
            } catch (UnsupportedEncodingException e) {
                    return true;
            }
    }
	
	/**
	 * 文字列が整数として解釈できるかどうか
	 * 
	 * @param value 検査する文字列
	 * @return 検査結果
	 */
	public boolean isInteger(final String value) {

		if (value == null || value.length() == 0) {
			return false;
		}
		
		int start = 0;
		if (value.charAt(0) == '-') {
			start = 1;
		}

		return isPositiveInteger(value.substring(start, value.length()));
	}

	/**
	 * 文字列が正の整数として解釈できるかどうか
	 * 
	 * @param value 検査する文字列
	 * @return 検査結果
	 */
	public boolean isPositiveInteger(final String value) {

		if (value == null || value.length() == 0) {
			return false;
		}
		
		for (int i = 0; i < value.length(); i++) {
			
			if (!isNumeric(value.charAt(i))) {
				return false;
			}
		}
		
		return true;
	}

	/**
	 * 文字列がアルファベット[A-Za-z]のみを含むかどうか
	 * 
	 * @param value 検査する文字列
	 * @return 検査結果
	 */
	public boolean isAlphabet(final String value) {

		if (value == null || value.length() == 0) {
			return false;
		}
		
		for (int i = 0; i < value.length(); i++) {
			char c = value.charAt(i);
			if (!isUpperCase(c) && !isLowerCase(c)) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * 文字列にダブルクォートまたはカンマを含むかどうか
	 * 
	 * @param value 検査する文字列
	 * @return 検査結果
	 */
	public boolean isWQuoteOrComma(final String value) {
		
		if (StringUtils.isEmpty(value)) {
			return false;
		} else {
			return value.indexOf('"') >= 0
					|| value.indexOf(',') >= 0 || value.indexOf('\\') >= 0;
		}
	}
	
	/**
	 * 文字列がアルファベットと数字のみ[0-9A-Za-z]を含むかどうか
	 * 
	 * @param value 検査する文字列
	 * @return 検査結果
	 */
	public boolean isAlphanumeric(final String value) {
		
		if (value == null || value.length() == 0) {
			return false;
		}
		
		for (int i = 0; i < value.length(); i++) {
			char c = value.charAt(i);
			if (!isNumeric(c) && !isUpperCase(c) && !isLowerCase(c)) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * 文字列（YYYY/MM/DD）が日付として正しいかどうか
	 * 
	 * 2005/11/8（正しい）
	 * 2005/13/11（エラー！）
	 * 2005/2/30（エラー！）
	 * 
	 * @param value 検査する文字列
	 * @return 検査結果
	 */
	public boolean isValidDate(final String value) {
		
		if (value == null || value.length() == 0) {
			return false;
		}
		
		try {
			format.parse(value);
			return true;
		} catch (final ParseException e) {
			return false;
		}
	}
	
	/**
	 * 文字列が全角文字列のみであるかどうか
	 * 
	 * @param value 検査する文字列
	 * @return 検査結果
	 */
	public boolean isMultiByteString(final String value) {

		if (value == null || value.length() == 0) {
			return false;
		}
		
		return value.length() * 2 == getByteLength(value);
	}
	
	/**
	 * 文字列に日本語を含んでいるかどうか
	 * 
	 * @param value 検査する文字列
	 * @return 検査結果
	 */
	public boolean hasJapanese(final String value) {
		if (hasMultiByteString(value)) {
			return true;
		}
		if (hasHankakuKana(value)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 文字列に全角文字列を含んでいるかどうか
	 * 
	 * @param value 検査する文字列
	 * @return 検査結果
	 */
	public boolean hasMultiByteString(final String value) {

		if (value == null || value.length() == 0) {
			return false;
		}

		return value.length() != getByteLength(value);
	}
	
	/**
	 * 文字列に半角カナを含んでいるかどうか
	 * 
	 * @param value 検査する文字列
	 * @return 検査結果
	 */
	public boolean hasHankakuKana(final String value) {
		for (int i = 0; i < value.length(); ++i) {
			char c = value.charAt(i);
			if (c >= 0xff61 && c <= 0xff9f) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 文字列のバイト長が指定した範囲内であるかどうか
	 * ※バイト長なので全角文字を含む場合は注意！
	 * 
	 * 最小長 <= 文字列のバイト長 <= 最大長
	 * 
	 * @param value 検査する文字列
	 * @param min 有効な最大長
	 * @param max 有効な最小長
	 * @return 検査結果
	 */
	public boolean isValidByteLength(
			final String value, final int min, final int max) {

		if (min < 0) {
			throw new IllegalArgumentException(
					"最小長は0以上で指定してください。");
		}
		
		if (min > max) {
			throw new IllegalArgumentException(
					"最大長は最小長以上で指定してください。");
		}
		
		final int size = getByteLength(value);

		return size >= min && size <= max;
	}
	
	/**
	 * 文字列を整数として解釈したとき、値が指定した範囲内であるかどうか
	 * 
	 * 最小値 <= 文字列の整数表現 <= 最大値
	 * 
	 * @param value 検査する文字列
	 * @param min 最小値
	 * @param max 最大値
	 * @return 検査結果
	 */
	public boolean isValidRange(
			final String value, final long min, final long max) {

		if (min > max) {
			throw new IllegalArgumentException(
					"最大値は最小値以上で指定してください。");
		}
		
		if (isInteger(value)) {
			final long i = Long.parseLong(value);
			return i >= min && i <= max;
		} else {
			return false;
		}
	}
	
	private boolean isNumeric(final char c) {
		return c >= '0' && c <= '9';
	}
	
	private boolean isUpperCase(final char c) {
		return c >= 'A' && c <= 'Z';
	}
	
	private boolean isLowerCase(final char c) {
		return c >= 'a' && c <= 'z';
	}
	
	private int getByteLength(final String value) {

		if (value == null) {
			return 0;
		}
		
		try {
			return value.getBytes("Windows-31J").length;
		} catch (final UnsupportedEncodingException e) {
			throw new InternalError(e.getMessage());
		}
	}
	
}

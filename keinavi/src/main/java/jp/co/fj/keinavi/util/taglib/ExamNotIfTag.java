package jp.co.fj.keinavi.util.taglib;

/**
 *
 * 指定の試験が特定の試験の種類かを判断するためのカスタムタグ
 * 
 * 2006/09/01    [新規作成]
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class ExamNotIfTag extends ExamIfTag {

    /**
     * Not として比較するかどうか
     */
    protected final boolean NOT = true;
    
    protected boolean getNOT() {
        return NOT;
    }

}

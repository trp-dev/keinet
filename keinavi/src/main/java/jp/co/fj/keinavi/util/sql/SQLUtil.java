/*
 * 作成日: 2004/07/16
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

/**
 * SQL生成に関するユーティリティクラス
 * 
 * @author kawai
 */
public class SQLUtil {

	/**
	 * 文字列をシングルクォートで囲って返す
	 * @param string 対象となる文字列
	 * @return 文字列
	 */
	public static String quote(String string) {
		StringBuffer buff = new StringBuffer(); // バッファ

		buff.append("'");

		if (string != null) {
			char[] c = string.toCharArray();
			for (int i=0; i<c.length; i++) {
				if (c[i] == '\'') {
					buff.append("''");
				} else {
					buff.append(c[i]);
				}
			}
		}
		
		buff.append("'");

		return buff.toString();
	}

	/**
	 * 文字列配列の要素をシングルクォートで囲いカンマで結合して返す。<BR>
	 * NULL要素は結合対象外。
	 * @param array 対象となる文字列配列
	 * @return 文字列
	 */
	public static String concatComma(String[] array) {
		StringBuffer buff = new StringBuffer(); // バッファ
		
		for (int i=0; i<array.length; i++) {
			if (array[i] == null) continue; // NULLは除く
			if (i != 0) buff.append(",");
			buff.append(quote(array[i]));
		}
		
		return buff.toString();
	}
	
	/**
	 * 特定のテーブルから全てのレコードを取得する
	 * （列は':'で結合され、行毎に改行が入る）
	 * デバッグ向けの機能
	 * @param con
	 * @param table
	 * @return
	 * @throws SQLException
	 */
	public static String getAllRecord(Connection con, String table) throws SQLException {
		StringBuffer buff = new StringBuffer();

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement("SELECT * FROM " + table);
			rs = ps.executeQuery();
			while (rs.next()) {
				ResultSetMetaData meta = rs.getMetaData();
				
				for (int i=0; i<meta.getColumnCount(); i++) {
					if (i != 0) buff.append(":");
					buff.append(rs.getString(i + 1));
				}

				buff.append('\n');

			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);			
		}
		
		return buff.toString();
	}	
}

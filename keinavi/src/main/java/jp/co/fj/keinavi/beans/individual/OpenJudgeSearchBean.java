package jp.co.fj.keinavi.beans.individual;

import java.sql.SQLException;
import java.util.Map;

import jp.co.fj.keinavi.beans.NewestExamDivSearchBean;
import jp.fujitsu.keinet.mst.db.factory.UnivFactoryDB;
import jp.fujitsu.keinet.mst.db.util.QueryLoader;

import com.fjh.beans.DefaultBean;

/**
*
* 公表データ検索
* 
* <2010年度改修＃マーク対応（判定軽量化）>
* 2010.10.25	Tomohisa YAMADA - TOTEC
* 				新規作成
*
* 
*/
public class OpenJudgeSearchBean extends DefaultBean {
	
	//大学コード
	private String univcd;
	//学部コード
	private String facultycd;
	//学科コード
	private String deptcd;
	
	//公表データ<大学キー, Map>
	private Map openDatas;
	
	/**
	 * コンストラクタ
	 * @param u 大学コード
	 * @param f 学部コード
	 * @param d 学科コード
	 */
	public OpenJudgeSearchBean(String u, String f, String d) {
		this.univcd = u;
		this.facultycd = f;
		this.deptcd = d;
	}

	/**
	 * 実行部
	 */
	public void execute() throws SQLException, Exception {
		
		//最新の模試年度と模試区分を設定
		NewestExamDivSearchBean neds = new NewestExamDivSearchBean();
		neds.setConnection(null, conn);
		neds.execute();
		
		//公表用データを取得
		this.openDatas = 
			UnivFactoryDB.searchOpenDatas(
					conn, 
					neds.getExamYear(),
					neds.getExamDiv(),
					univcd, facultycd, deptcd);
	}

	/**
	 * 公表用データマップ<枝番コード, UnivDetail>
	 * @return
	 */
	public Map getOpenDatas() {
		return openDatas;
	}

}

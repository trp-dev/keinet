package jp.co.fj.keinavi.servlets.sales.base;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.data.login.SectorData;
import jp.co.fj.keinavi.data.login.SectorSession;
import jp.co.fj.keinavi.interfaces.SpecialAppliMenu;
import jp.co.fj.keinavi.servlets.MethodInvokerServlet;

/**
 * 
 * 特例成績データ作成承認システムの基底サーブレットです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public abstract class BaseSpecialAppliServlet extends MethodInvokerServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = 451867454803262337L;

	/**
	 * @see jp.co.fj.keinavi.servlets.MethodInvokerServlet#prepare(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse, java.lang.String)
	 */
	protected void prepare(HttpServletRequest request,
			HttpServletResponse response, String methodName) throws Exception {

		Set menuIdSet = (Set) request.getSession(false).getAttribute(
				SpecialAppliMenu.SESSION_KEY);

		if (!menuIdSet.contains(getMenu().id)) {
			throw new Exception("メニューを利用する権限がありません。" + getMenu());
		}
	}

	/**
	 * このサーブレットに割り当てるメニューを返します。<br>
	 * セッション上にこのメニューが存在しない場合はアクセスを拒否します。
	 */
	protected abstract SpecialAppliMenu getMenu();

	/**
	 * 所属する部門コードの配列を返します。
	 */
	protected String[] getSectorCdArray(HttpServletRequest request) {

		SectorSession sectorSession = (SectorSession) request.getSession(false)
				.getAttribute(SectorSession.SESSION_KEY);

		List list = new ArrayList();
		for (Iterator ite = sectorSession.getSectorList().iterator(); ite
				.hasNext();) {
			SectorData data = (SectorData) ite.next();
			list.add(data.getSectorCD());
		}

		return (String[]) list.toArray(new String[list.size()]);
	}

}

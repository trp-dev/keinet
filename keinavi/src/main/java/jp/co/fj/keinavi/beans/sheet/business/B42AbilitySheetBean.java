package jp.co.fj.keinavi.beans.sheet.business;

import java.util.List;

import jp.co.fj.keinavi.beans.sheet.business.data.B42_07Data;
import jp.co.fj.keinavi.beans.sheet.common.BaseExcelCreator;
import jp.co.fj.keinavi.beans.sheet.common.ISheetData;
import jp.co.fj.keinavi.beans.sheet.excel.business.B42_07ExcelCreator;
import jp.co.fj.keinavi.beans.sheet.excel.business.B42_08ExcelCreator;
import jp.co.fj.keinavi.beans.sheet.excel.business.B42_09ExcelCreator;
import jp.co.fj.keinavi.beans.sheet.school.S42AbilitySheetBean;

/**
 *
 * 高校成績分析 - 高校間比較 - 偏差値分布
 * 受験学力測定テスト用のSheetBean
 * 
 * 2006.08.24	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class B42AbilitySheetBean extends S42AbilitySheetBean {

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.school.S42AbilitySheetBean#createCompSchoolList()
	 */
	protected List createCompSchoolList() {
		// 営業は自校なし
		return super.getCompSchoolList();
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.school.S42AbilitySheetBean#createData()
	 */
	protected ISheetData createData() {
		return new B42_07Data();
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("B42_01");
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.common.AbstractExtSheetBean#create()
	 */
	protected void create() throws Exception {
		
		final BaseExcelCreator creator;
		
		// 今年度のみ
		if (getData().getDispPast() == 1) {
			creator = new B42_07ExcelCreator(
					getData(), sessionKey, outfileList, getAction(), 7, 12);
			sheetLog.add("B42_07");
		// 前年度まで
		} else if (getData().getDispPast() == 2) {
			creator = new B42_08ExcelCreator(
					getData(), sessionKey, outfileList, getAction(), 8, 12);
			sheetLog.add("B42_08");
		// 前々年度まで
		} else {
			creator = new B42_09ExcelCreator(
					getData(), sessionKey, outfileList, getAction(), 8, 12);
			sheetLog.add("B42_09");
		}
		
		creator.execute();
	}

}

/*
 * 作成日: 2004/07/06
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class StudentData implements Serializable, Comparable{

	private String studentPersonId;
	private String studentYear;
	private String studentGrade;
	private String studentClass;
	private String studentClassNo;
	private String studentAttendGrade;
	private String studentRegion;
	private String studentSchoolHouse;
	private String studentSchoolCd;
	private String studentHomeSchoolCd;
	private String studentNameKana;
	private String studentNameKanji;
	private String studentSex;
	private String studentBirthday;
	private String studentTellNo;
	private String studentCustomerNo;
	private String studentSortNo;


	//この生徒が受けた特定の模試の特定の科目の科目別成績(i002で使用)
	private SubRecordData subRecordData;

	//この生徒が対象の模試を受けたかどうかを保持する入れ物
	private Map takenExam = new HashMap();

	private Collection examinationDatas;

	/**
	 * コンストラクタ
	 */
	public StudentData() {
	}

	/**
	 * コンストラクタ
	 */
	public StudentData(String studentPersonId) {
		this.studentPersonId = studentPersonId;
	}

	public String getExaminationDataLength() {
		int length = 0;
		if(getExaminationDatas() != null)
			length = getExaminationDatas().size();

		return Integer.toString(length);
	}
	public ExaminationData getExaminationData(String year, String code){
		ExaminationData data = null;
		if(getExaminationDatas() != null){
			for (java.util.Iterator it=getExaminationDatas().iterator(); it.hasNext(); ){
				ExaminationData examinationData = (ExaminationData)it.next();
				if(examinationData.getExamYear().equals(year) && examinationData.getExamCd().equals(code)){
					data = examinationData;
				}
			}
		}
		return data;
	}

	/* (非 Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		return 0;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return this.studentPersonId.equals(((StudentData) obj).studentPersonId);
	}

	/**
	 * @return
	 */
	public String getStudentAttendGrade() {
		return studentAttendGrade;
	}

	/**
	 * @return
	 */
	public String getStudentBirthday() {
		return studentBirthday;
	}

	/**
	 * @return
	 */
	public String getStudentClass() {
		return studentClass;
	}

	/**
	 * @return
	 */
	public String getStudentClassNo() {
		return studentClassNo;
	}

	/**
	 * @return
	 */
	public String getStudentCustomerNo() {
		return studentCustomerNo;
	}

	/**
	 * @return
	 */
	public String getStudentGrade() {
		return studentGrade;
	}

	/**
	 * @return
	 */
	public String getStudentHomeSchoolCd() {
		return studentHomeSchoolCd;
	}

	/**
	 * @return
	 */
	public String getStudentNameKana() {
		return studentNameKana;
	}

	/**
	 * @return
	 */
	public String getStudentNameKanji() {
		return studentNameKanji;
	}

	/**
	 * @return
	 */
	public String getStudentPersonId() {
		return studentPersonId;
	}

	/**
	 * @return
	 */
	public String getStudentRegion() {
		return studentRegion;
	}

	/**
	 * @return
	 */
	public String getStudentSchoolCd() {
		return studentSchoolCd;
	}

	/**
	 * @return
	 */
	public String getStudentSchoolHouse() {
		return studentSchoolHouse;
	}

	/**
	 * @return
	 */
	public String getStudentSex() {
		return studentSex;
	}

	/**
	 * @return
	 */
	public String getStudentTellNo() {
		return studentTellNo;
	}

	/**
	 * @return
	 */
	public String getStudentYear() {
		return studentYear;
	}

	/**
	 * @param string
	 */
	public void setStudentAttendGrade(String string) {
		studentAttendGrade = string;
	}

	/**
	 * @param string
	 */
	public void setStudentBirthday(String string) {
		studentBirthday = string;
	}

	/**
	 * @param string
	 */
	public void setStudentClass(String string) {
		studentClass = string;
	}

	/**
	 * @param string
	 */
	public void setStudentClassNo(String string) {
		studentClassNo = string;
	}

	/**
	 * @param string
	 */
	public void setStudentCustomerNo(String string) {
		studentCustomerNo = string;
	}

	/**
	 * @param string
	 */
	public void setStudentGrade(String string) {
		studentGrade = string;
	}

	/**
	 * @param string
	 */
	public void setStudentHomeSchoolCd(String string) {
		studentHomeSchoolCd = string;
	}

	/**
	 * @param string
	 */
	public void setStudentNameKana(String string) {
		studentNameKana = string;
	}

	/**
	 * @param string
	 */
	public void setStudentNameKanji(String string) {
		studentNameKanji = string;
	}

	/**
	 * @param string
	 */
	public void setStudentPersonId(String string) {
		studentPersonId = string;
	}

	/**
	 * @param string
	 */
	public void setStudentRegion(String string) {
		studentRegion = string;
	}

	/**
	 * @param string
	 */
	public void setStudentSchoolCd(String string) {
		studentSchoolCd = string;
	}

	/**
	 * @param string
	 */
	public void setStudentSchoolHouse(String string) {
		studentSchoolHouse = string;
	}

	/**
	 * @param string
	 */
	public void setStudentSex(String string) {
		studentSex = string;
	}

	/**
	 * @param string
	 */
	public void setStudentTellNo(String string) {
		studentTellNo = string;
	}

	/**
	 * @param string
	 */
	public void setStudentYear(String string) {
		studentYear = string;
	}

	/**
	 * @return
	 */
	public Collection getExaminationDatas() {
		return examinationDatas;
	}

	/**
	 * @param collection
	 */
	public void setExaminationDatas(Collection collection) {
		examinationDatas = collection;
	}

	/**
	 * @return
	 */
	public Map getTakenExam() {
		return takenExam;
	}

	/**
	 * @param string 受験した模試ID
	 */
	public void addTakenExam(final String id) {
		takenExam.put(id, Boolean.TRUE);
	}

	/**
	 * @return
	 */
	public SubRecordData getSubRecordData() {
		return subRecordData;
	}

	/**
	 * @param data
	 */
	public void setSubRecordData(SubRecordData data) {
		subRecordData = data;
	}

	/**
	 * @return
	 */
	public String getStudentSortNo() {
		return studentSortNo;
	}

	/**
	 * @param string
	 */
	public void setStudentSortNo(String string) {
		studentSortNo = string;
	}

}
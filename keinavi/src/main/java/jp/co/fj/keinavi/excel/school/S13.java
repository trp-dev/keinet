/**
 * 校内成績分析−校内成績　設問別成績＋マーク模試正答状況
 * 出力する帳票の判断
 * 作成日: 2004/07/13
 * @author	T.Sakai
 *
 * 2009.10.21 Fujito URAKAWA - Totec
 *            新校内資料「科目・設問別正答率（レベル別）」対応
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.school.S13Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

public class S13 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean s13( S13Item s13Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {

		KNLog log = KNLog.getInstance(null,null,null);

		try{
			int ret = 0;
			//S13Itemから各帳票を出力
			if (( s13Item.getIntHyouFlg()==0 )&&( s13Item.getIntGraphFlg()==0 )&&
				// 2019/09/04 QQ)Tanioka 帳票出力処理追加 UPD START
				//( s13Item.getIntMarkFlg()==0 )&&( s13Item.getIntMarkAreaFlg()==0)) {
				( s13Item.getIntMarkFlg()==0 )&&( s13Item.getIntMarkAreaFlg()==0)&&
				( s13Item.getIntKokugoFlg()==0 )&&( s13Item.getIntStatusFlg()==0)&&
				( s13Item.getIntRecordFlg()==0 )) {
				// 2019/09/04 QQ)Tanioka 帳票出力処理追加 UPD END
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
			    //throw new Exception("S13 ERROR : フラグ異常");
			    throw new IllegalStateException("S13 ERROR : フラグ異常");
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			}
			if ( s13Item.getIntHyouFlg()==1 ) {
				log.Ep("S13_01","S13_01帳票作成開始","");
				S13_01 exceledit = new S13_01();
				ret = exceledit.s13_01EditExcel( s13Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S13_01","帳票作成エラー","");
					return false;
				}
				log.Ep("S13_01","S13_01帳票作成終了","");
				sheetLog.add("S13_01");
			}
			if ( s13Item.getIntGraphFlg()==1 ) {
				log.Ep("S13_02","S13_02帳票作成開始","");
				S13_02 exceledit = new S13_02();
				ret = exceledit.s13_02EditExcel( s13Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S13_02","帳票作成エラー","");
					return false;
				}
				log.Ep("S13_02","S13_02帳票作成終了","");
				sheetLog.add("S13_02");
			}
			if ( s13Item.getIntMarkFlg()==1 ) {
				log.Ep("S13_03","S13_03帳票作成開始","");
				S13_03 exceledit = new S13_03();
				ret = exceledit.s13_03EditExcel( s13Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S13_03","帳票作成エラー","");
					return false;
				}
				log.Ep("S13_03","S13_03帳票作成終了","");
				sheetLog.add("S13_03");
			}
			if ( s13Item.getIntMarkAreaFlg()==1 ) {
				log.Ep("S13_04","S13_04帳票作成開始","");
				S13_04 exceledit = new S13_04();
				ret = exceledit.s13_04EditExcel( s13Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S13_04","帳票作成エラー","");
					return false;
				}
				log.Ep("S13_04","S13_04帳票作成終了","");
				sheetLog.add("S13_04");
			}
			// 2019/09/04 QQ)Tanioka 帳票出力処理追加 ADD START
			if ( s13Item.getIntKokugoFlg()==1 ) {
				//log.Ep("S13_04","S13_04帳票作成開始","");
				//S13_04 exceledit = new S13_04();
				//ret = exceledit.s13_04EditExcel( s13Item, outfilelist, saveFlg, UserID );
				log.Ep("S13_05","S13_05帳票作成開始","");
				S13_05 exceledit = new S13_05();
				ret = exceledit.s13_05EditExcel( s13Item, outfilelist, saveFlg, UserID );
				// 2019/09/09 QQ)Tanouchi 帳票出力処理追加 UPD END
				if (ret!=0) {
					// 2019/09/09 QQ)Tanouchi 帳票出力処理追加 UPD START
					//log.Err("0"+ret+"S13_04","帳票作成エラー","");
					log.Err("0"+ret+"S13_05","帳票作成エラー","");
					// 2019/09/09 QQ)Tanouchi 帳票出力処理追加 UPD END
					return false;
				}
				// 2019/09/09 QQ)Tanouchi 帳票出力処理追加 UPD START
				//log.Ep("S13_04","S13_04帳票作成終了","");
				//sheetLog.add("S13_04");
				log.Ep("S13_05","S13_05帳票作成終了","");
				sheetLog.add("S13_05");
				// 2019/09/09 QQ)Tanouchi 帳票出力処理追加 UPD END
			}
			if ( s13Item.getIntStatusFlg()==1 ) {
// 2019/09/10 QQ)nagai 共通テスト対応 ADD START
				log.Ep("S13_06","S13_06帳票作成開始","");
				S13_06 exceledit = new S13_06();
				ret = exceledit.s13_06EditExcel( s13Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S13_06","帳票作成エラー","");
					return false;
				}
				log.Ep("S13_06","S13_06帳票作成終了","");
				sheetLog.add("S13_06");
			}
// 2019/09/10 QQ)nagai 共通テスト対応 ADD END
			if ( s13Item.getIntRecordFlg()==1 ) {
				log.Ep("S13_07","S13_07帳票作成開始","");
				S13_07 exceledit = new S13_07();
				ret = exceledit.s13_07EditExcel( s13Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S13_07","帳票作成エラー","");
					return false;
				}
				log.Ep("S13_07","S13_01帳票作成終了","");
				sheetLog.add("S13_07");
			}
			// 2019/09/04 QQ)Tanioka 帳票出力処理追加 ADD END

		} catch(Exception e) {
			log.Err("99S13","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
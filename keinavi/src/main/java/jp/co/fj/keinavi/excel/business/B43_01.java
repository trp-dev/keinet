/**
 * 高校成績分析−高校間比較（過年度：前年度）
 * 	Excelファイル編集
 * 作成日: 2004/08/19
 * @author	Ito.Y
 * 
 * 2009.10.13   Fujito URAKAWA - Totec
 *              「全体成績」追加対応 
 */

package jp.co.fj.keinavi.excel.business;

import java.io.*;
import java.util.*;

import jp.co.fj.keinavi.excel.data.business.*;
import jp.co.fj.keinavi.excel.cm.*;
import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.*;

public class B43_01 {

	private int noerror	= 0;		// 正常終了
	private int errfread	= 1;		// ファイルreadエラー
	private int errfwrite	= 2;		// ファイルwriteエラー
	private int errfdata	= 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	final private String	masterfile0	= "B43_01";	// ファイル名
	final private String	masterfile1	= "B43_01";	// ファイル名
	private String	masterfile	= "";				// ファイル名
	final private String	strArea		= "A1:AE64";		// 印刷範囲	
	final private String	mastersheet = "tmp";		// テンプレートシート名
	final private int[]	tabCol		= {1,4,7,10,13,16,19,22,25,28,31};	// 表の基準点(今年度)


	/*
	 * 	Excel編集メイン
	 * 		B43Item b43Item: データクラス
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
 	public int b43_01EditExcel(B43Item b43Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (b43Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			// マスタExcel読み込み
			FileInputStream	fin			= null;
			HSSFWorkbook	workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFSheet		workSheet2	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;
		
			// 基本ファイルを読込む
			B43ListBean b43ListBean = new B43ListBean();
			
			// データセット
			ArrayList	b43List			= b43Item.getB43List();
			ArrayList	workbookList	= new ArrayList();
			ArrayList	workSheetList	= new ArrayList();
			Iterator	itr				= b43List.iterator();
			int		starCnt			= 1;	// *表示用カウンター
			float		shelHensa		= 0;	// *表示用偏差値退避変数
			boolean	bolhyouji		= true;// *表示用表示判定フラグ
			int		bestNo			= 0;	// 上位○件　○にあたる数字が入る(半角数字)
			String		joui			= "";	// 上位○件　○にあたる数字が入る(全角文字)
			int		row				= 0;	// 行
			int		setsuCnt		= 0;	// 設問カウンター
			int		kmkCnt			= 1;	// 科目カウンター
			int		kmkCd			= 0;	// 科目保持
			int		maxSheetIndex	= 0;	// シートカウンター
			int		allSheetIndex	= 0;	// シートカウンター
			int		intMaxSheetSr	= 50;	// MAXシート数
			int		fileIndex		= 1;	// ファイルカウンター
			int		dataSize		= 0;	// リストデータ件数
			int		houjiNum		= 51;	// エクセル表示可能件数
			int		shelSheetNum	= 0;	// シート番号保持
			int		sheetNumRow		= 0;	// シート枚数（Row方向）
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
			

			// b43Listのセット
			while( itr.hasNext() ) {

				b43ListBean = (B43ListBean)itr.next();

				// 基本ファイルを読み込む
				B43GakkoListBean	b43GakkoListBean	= new B43GakkoListBean();

				//データの保持
				ArrayList	b43GakkoList	= b43ListBean.getB43GakkoList();
				Iterator	itrB43Gakko	= b43GakkoList.iterator();

				//型から科目に変わる時のチェック
				if ( Integer.parseInt(b43ListBean.getStrKmkCd()) != kmkCd ) {

					//型・科目切替えの初回のみ処理する
					if(setsuCnt!=0){
						kmkCnt=1;
						setsuCnt=0;
						bolSheetCngFlg	= true;
					}
				}
				// Best表示用
				if(b43Item.getIntBestFlg()==1){
					joui = "１０";
					bestNo=10;
				}
				if(b43Item.getIntBestFlg()==2){
					joui = "５";
					bestNo=5;
				}

				// 高校データセット
				ArrayList	bestListNow		= new ArrayList();
				ArrayList	bestListPast	= new ArrayList();
				dataSize = b43GakkoList.size(); //データ件数
//				shelSheetNum = 0;	// シート番号保持

				// 下方向のシート数の計算
				sheetNumRow = dataSize/houjiNum;
				if(dataSize%houjiNum!=0){
					sheetNumRow++;
				}
				if(sheetNumRow>1){
					bolSheetCngFlg=true;
				}

				//設問が無ければ処理を回避	2004.10.22 設問名Null時の処理回避
				if ( !cm.toString(b43ListBean.getStrSetsuMei1()).equals("") ) {


					// 変数の初期化
					row	= 0;
					while(itrB43Gakko.hasNext()){
						b43GakkoListBean	= (B43GakkoListBean) itrB43Gakko.next();
	
						//マスタExcel読み込み
						if(bolBookCngFlg){
							if((maxSheetIndex==intMaxSheetSr)||(maxSheetIndex==0)){
								workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
								if( workbook==null ){
									return errfread;
								}
								bolBookCngFlg = false;
								maxSheetIndex=0;
								workbookList.add(workbook);
							}
						}
	
						if ( bolSheetCngFlg ) {
	
							// シートの初めての型・科目なら新規シート・２科目め以降なら呼び出し
							if(setsuCnt==0){
	
								// シートテンプレートのコピー
								workSheet = workbook.cloneSheet(0);
								shelSheetNum = allSheetIndex;
								maxSheetIndex++;
								allSheetIndex++;
	
								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);
	
								// セキュリティスタンプセット
								String secuFlg = cm.setSecurity( workbook, workSheet, b43Item.getIntSecuFlg() ,31 ,33 );
								workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
								workCell.setCellValue(secuFlg);
	
								// 対象模試セット
								String moshi =cm.setTaisyouMoshi( b43Item.getStrMshDate() );	// 模試月取得
								workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
								workCell.setCellValue( "対象模試　　：" + cm.toString(b43Item.getStrMshmei()) + moshi);
	
								// 型名セット 2004.11.9 配点がないとき括弧はなし
								String haiten = "";
								if ( !cm.toString(b43ListBean.getStrHaitenKmk()).equals("") ) {
									haiten = "（" + b43ListBean.getStrHaitenKmk() + "）";
								}
								workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
								workCell.setCellValue( "型・科目：" + cm.toString(b43ListBean.getStrKmkmei()) + haiten  );
//								workCell.setCellValue( "型・科目：" + cm.toString(b43ListBean.getStrKmkmei()) + " (" + cm.toString(b43ListBean.getStrHaitenKmk()) + ")"  );

								kmkCd = Integer.parseInt(b43ListBean.getStrKmkCd());
	
								// コメントセット
								if(b43Item.getIntBestFlg()!=3){
									workCell = cm.setCell( workSheet, workRow, workCell, 3, 33 );
									workCell.setCellValue( "※得点率が高い上位" + joui + "校に\"*\"を表示しています。");
								}
	
								shelSheetNum = allSheetIndex;
							}else{
								// シートの呼び出し
//								workSheet = (HSSFSheet)workSheetList.get(allSheetIndex-sheetNumRow);
								if (intSaveFlg==1 || intSaveFlg==5) {
									workSheet = workbook.getSheetAt(allSheetIndex-sheetNumRow+1);
								}else{
									workSheet = workbook.getSheetAt(allSheetIndex-sheetNumRow+1+1);
								}
								shelSheetNum = allSheetIndex-sheetNumRow+1;
								sheetNumRow--;
							}
							//　改シートフラグをFalseにする
							bolSheetCngFlg	=false;
							row=0;
						}
	
						if(row==0){
							// 設問番号セット
							workCell = cm.setCell( workSheet, workRow, workCell, 4, tabCol[setsuCnt] );
							workCell.setCellValue( b43ListBean.getStrSetsumonNo() );
	
							// 設問名セット
							workCell = cm.setCell( workSheet, workRow, workCell, 5, tabCol[setsuCnt] );
							workCell.setCellValue( b43ListBean.getStrSetsuMei1() );
	
							// 配点セット
							workCell = cm.setCell( workSheet, workRow, workCell, 6, tabCol[setsuCnt] );
							workCell.setCellValue( " (" + cm.toString(b43ListBean.getStrSetsuHaiten()) + ")" );
	
						}
	
						// 高校名セット
						if(setsuCnt == 0){
							workCell = cm.setCell( workSheet, workRow, workCell, 8+row, 0 );
							workCell.setCellValue( b43GakkoListBean.getStrGakkomei());
						}
	
						// 受験人数セット
						if ( b43GakkoListBean.getIntNinzu() != -999 ) {
							workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabCol[setsuCnt]);
							workCell.setCellValue( b43GakkoListBean.getIntNinzu() );
						}
	
						// 得点率セット
						if ( b43GakkoListBean.getFloTokuritsu() != -999.0 ) {
							workCell = cm.setCell( workSheet, workRow, workCell, 8+row, tabCol[setsuCnt]+2);
							workCell.setCellValue( b43GakkoListBean.getFloTokuritsu() );
	
							// *表示用リスト格納処理
							mkBestList(b43GakkoListBean.getFloTokuritsu(),shelSheetNum,row,bestListNow);
						}
	
						row++;
	
						// リストにWorkSheetを格納
						if(row%houjiNum==0){
							if(itrB43Gakko.hasNext()){
								if(setsuCnt==0){
									workSheetList.add(workSheet);
								}
								bolSheetCngFlg = true;
								if(workbook.getNumberOfSheets()==intMaxSheetSr){
									bolBookCngFlg = true;
								}
							}
						}
						if(itrB43Gakko.hasNext()==false){
							if(setsuCnt==0){
								workSheetList.add(workSheet);
							}
						}
					}
	
					// *表示処理(今年度)
					if(bestListNow.size()!=0){
						Iterator itera = bestListNow.iterator();
						starCnt = 1;
						shelHensa = 0;
						bolhyouji = true;
						while(itera.hasNext()){
							B43BestListBean b43BestListBean =(B43BestListBean)itera.next();
//							workSheet = (HSSFSheet)workSheetList.get(b43BestListBean.getIntSheetNum());
//							workSheet2 = workbook.getSheetAt(b43BestListBean.getIntSheetNum());
							if (intSaveFlg==1 || intSaveFlg==5) {
								workSheet2 = workbook.getSheetAt(b43BestListBean.getIntSheetNum());
							}else{
								workSheet2 = workbook.getSheetAt(b43BestListBean.getIntSheetNum()+1);
							}
	
							if(starCnt==bestNo){
								shelHensa = b43BestListBean.getFloTokuritsu();
							}
							if(starCnt<=bestNo){
								bolhyouji = true;
							}else{
								if(shelHensa==b43BestListBean.getFloTokuritsu()){
									bolhyouji = true;
								}else{
									break;
								}
							}
							if(bolhyouji){
								workCell = cm.setCell( workSheet2, workRow, workCell, 8+b43BestListBean.getIntRow(), tabCol[setsuCnt]+1 );
								workCell.setCellValue( "*" );
								
							}
							
							starCnt++;
						}
					}
	
					kmkCnt++;
					setsuCnt++;
					if(setsuCnt >= 11){
						setsuCnt=0;
						bolSheetCngFlg =true;
						if(workbook.getNumberOfSheets()==intMaxSheetSr){
							bolBookCngFlg = true;
						}
					}
	
				}
				
				// ListにWorkBookが２つ以上格納されている時は出力処理を実行
				int listSize = workbookList.size();
				if(listSize>1){
					if(setsuCnt == 0){
						workbook = (HSSFWorkbook)workbookList.get(0);
		
						// Excelファイル保存
						boolean bolRet = false;
						bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, workbook.getNumberOfSheets());
						fileIndex++;
						if( bolRet == false ){
							return errfwrite;
						}
	
						// ファイル出力したデータは削除
						workbookList.remove(0);
						for(int a =0;a<intMaxSheetSr;a++){
							workSheetList.remove(0);
						}
	
						// WorkBook・変数を書き込んでる途中のものに戻す
						allSheetIndex = maxSheetIndex;
						workbook = (HSSFWorkbook)workbookList.get(0);
					}
				}
			}		
			// Excelファイル保存
			boolean bolRet = false;
			if(fileIndex != 1){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, workbook.getNumberOfSheets());
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, workbook.getNumberOfSheets());
			}

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("B43_01","データセットエラー",e.toString());
			return errfdata;
		}

		return noerror;
	}

	/**
	 * 順位リスト作成メソッド
	 * 
	 * @param 偏差値
	 * @param シートNo
	 * @param 表示位置
	 * @param 格納リスト
	 */
	private void mkBestList(float tokuritsu,int sheetNum,int row, ArrayList bestList ) throws Exception {
		try{
			B43BestListBean	b43Best	= new B43BestListBean();
			b43Best.setFloTokuritsu(tokuritsu);
			b43Best.setIntRow(row);
			b43Best.setIntSheetNum(sheetNum);
		
			if(bestList.size()==0){
				bestList.add(b43Best);
			}else{
				Iterator itera = bestList.iterator();
				int cnt=0;
				while(itera.hasNext()){
					B43BestListBean	b43BestListBean = (B43BestListBean)itera.next();
					if(b43BestListBean.getFloTokuritsu()<=tokuritsu){
						bestList.add(cnt,b43Best); 
						break;
					}
					cnt++;
					if(itera.hasNext()==false){
						bestList.add(b43Best);
						break;
					}
				}
			}
		}catch(Exception e){
			throw e;
		}
	}

}
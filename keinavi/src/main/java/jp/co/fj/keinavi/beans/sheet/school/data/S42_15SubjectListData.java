package jp.co.fj.keinavi.beans.sheet.school.data;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * 科目リストデータ
 * 
 * 2006.08.31	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class S42_15SubjectListData {

	// 科目コード
	private String subCd;
	// 科目名
	private String subName;
	// センター到達指標・到達レベルリスト
	private final List reachLevelListData = new ArrayList();
	// 親データクラス
	private S42_15Data data;
	// 高校リスト
	private final List bundleListData = new ArrayList();
	
	// コンストラクタ
	protected S42_15SubjectListData() {
	}
	
	/**
	 * @param data 追加するセンター到達指標リストデータ
	 */
	public void addReachLevelListData(final S42_15ReachLevelListData data) {
		data.setSubjectListData(this);
		reachLevelListData.add(data);
	}
	
	/**
	 * @param data 追加する高校リストデータ
	 */
	public void addBundleListData(final S42_15BundleListData data) {
		data.setSubjectListData(this);
		bundleListData.add(data);
	}
	
	/**
	 * @return bundleListData を戻します。
	 */
	public List getBundleListData() {
		return bundleListData;
	}
	/**
	 * @return reachLevelListData を戻します。
	 */
	public List getReachLevelListData() {
		return reachLevelListData;
	}
	/**
	 * @return subCd を戻します。
	 */
	public String getSubCd() {
		return subCd;
	}
	/**
	 * @param pSubCd 設定する subCd。
	 */
	public void setSubCd(String pSubCd) {
		subCd = pSubCd;
	}
	/**
	 * @return subName を戻します。
	 */
	public String getSubName() {
		return subName;
	}
	/**
	 * @param pSubName 設定する subName。
	 */
	public void setSubName(String pSubName) {
		subName = pSubName;
	}

	/**
	 * @return data を戻します。
	 */
	public S42_15Data getData() {
		return data;
	}

	/**
	 * @param pData 設定する data。
	 */
	public void setData(S42_15Data pData) {
		data = pData;
	}
	
}

package jp.co.fj.keinavi.forms.maintenance;

import jp.co.fj.keinavi.forms.BaseUploadForm;

/**
 *
 * 生徒一括登録アクションフォーム
 * 
 * 2005.11.28	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M104Form extends BaseUploadForm {

	// 動作モード
	private String actionMode;

	// 対象年度
	private String targetYear;

	// 対象学年
	private String targetGrade;

	// 対象クラス
	private String targetClass;

	// 学年
	private String grade;

	// クラス
	private String className;

	// クラス番号
	private String classNo;

	// 性別
	private String sex;

	// かな氏名
	private String nameKana;

	// 漢字氏名
	private String nameKanji;

	// 電話番号
	private String telNo;

	// 生年月日・年
	private String birthYear;

	// 生年月日・月
	private String birthMonth;

	// 生年月日・日
	private String birthDate;

	// 絞り込み選択
	private String conditionSelect;

	// 絞り込み条件
	private String conditionText;

	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}
	
	/**
	 * @return actionMode
	 */
	public String getActionMode() {
		return actionMode;
	}

	/**
	 * @param pActionMode 設定する actionMode
	 */
	public void setActionMode(String pActionMode) {
		actionMode = pActionMode;
	}

	/**
	 * @return birthDate
	 */
	public String getBirthDate() {
		return birthDate;
	}

	/**
	 * @param pBirthDate 設定する birthDate
	 */
	public void setBirthDate(String pBirthDate) {
		birthDate = pBirthDate;
	}

	/**
	 * @return birthMonth
	 */
	public String getBirthMonth() {
		return birthMonth;
	}

	/**
	 * @param pBirthMonth 設定する birthMonth
	 */
	public void setBirthMonth(String pBirthMonth) {
		birthMonth = pBirthMonth;
	}

	/**
	 * @return birthYear
	 */
	public String getBirthYear() {
		return birthYear;
	}

	/**
	 * @param pBirthYear 設定する birthYear
	 */
	public void setBirthYear(String pBirthYear) {
		birthYear = pBirthYear;
	}

	/**
	 * @return className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param pClassName 設定する className
	 */
	public void setClassName(String pClassName) {
		className = pClassName;
	}

	/**
	 * @return classNo
	 */
	public String getClassNo() {
		return classNo;
	}

	/**
	 * @param pClassNo 設定する classNo
	 */
	public void setClassNo(String pClassNo) {
		classNo = pClassNo;
	}

	/**
	 * @return conditionSelect
	 */
	public String getConditionSelect() {
		return conditionSelect;
	}

	/**
	 * @param pConditionSelect 設定する conditionSelect
	 */
	public void setConditionSelect(String pConditionSelect) {
		conditionSelect = pConditionSelect;
	}

	/**
	 * @return conditionText
	 */
	public String getConditionText() {
		return conditionText;
	}

	/**
	 * @param pConditionText 設定する conditionText
	 */
	public void setConditionText(String pConditionText) {
		conditionText = pConditionText;
	}

	/**
	 * @return grade
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * @param pGrade 設定する grade
	 */
	public void setGrade(String pGrade) {
		grade = pGrade;
	}

	/**
	 * @return nameKana
	 */
	public String getNameKana() {
		return nameKana;
	}

	/**
	 * @param pNameKana 設定する nameKana
	 */
	public void setNameKana(String pNameKana) {
		nameKana = pNameKana;
	}

	/**
	 * @return nameKanji
	 */
	public String getNameKanji() {
		return nameKanji;
	}

	/**
	 * @param pNameKanji 設定する nameKanji
	 */
	public void setNameKanji(String pNameKanji) {
		nameKanji = pNameKanji;
	}

	/**
	 * @return sex
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @param pSex 設定する sex
	 */
	public void setSex(String pSex) {
		sex = pSex;
	}

	/**
	 * @return targetClass
	 */
	public String getTargetClass() {
		return targetClass;
	}

	/**
	 * @param pTargetClass 設定する targetClass
	 */
	public void setTargetClass(String pTargetClass) {
		targetClass = pTargetClass;
	}

	/**
	 * @return targetGrade
	 */
	public String getTargetGrade() {
		return targetGrade;
	}

	/**
	 * @param pTargetGrade 設定する targetGrade
	 */
	public void setTargetGrade(String pTargetGrade) {
		targetGrade = pTargetGrade;
	}

	/**
	 * @return targetYear
	 */
	public String getTargetYear() {
		return targetYear;
	}

	/**
	 * @param pTargetYear 設定する targetYear
	 */
	public void setTargetYear(String pTargetYear) {
		targetYear = pTargetYear;
	}

	/**
	 * @return telNo
	 */
	public String getTelNo() {
		return telNo;
	}

	/**
	 * @param pTelNo 設定する telNo
	 */
	public void setTelNo(String pTelNo) {
		telNo = pTelNo;
	}

	
}

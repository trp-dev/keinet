package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * バランスチャート用フォーム
 * @author ishiguro
 *
 * 2005.4.26 	K.Kondo 	[1]画面スクロール座標の追加
 */
public class I103Form extends ActionForm {

	private String mode;

	private String targetPersonId;
	private String targetExamCode;
	private String targetExamYear;

	private String targetExamCode1;
	private String targetExamCode2;
	private String targetExamCode3;
	
	//個人成績分析共通-印刷対象生徒（判定対象生徒)
	private String printStudent;
	//セキュリティスタンプ
	private String printStamp;
	//偏差値範囲
	private String lowerRange;
	private String upperRange;
	//教科複数受験
	private String avgChoice;
	//成績分析共通：印刷対象
	private String[] printTarget;
	//印刷ボタン押下時パラメータ
	private String printStatus;
	
	private String scrollX;//[1] add サブミット前、表示座標Ｘ
	private String scrollY;//[1] add サブミット前、表示座標Ｙ
	
	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}
	
	/**
	 * @return
	 */
	public String getAvgChoice() {
		return avgChoice;
	}

	/**
	 * @return
	 */
	public String getLowerRange() {
		return lowerRange;
	}

	/**
	 * @return
	 */
	public String getUpperRange() {
		return upperRange;
	}

	/**
	 * @param string
	 */
	public void setAvgChoice(String string) {
		avgChoice = string;
	}

	/**
	 * @param string
	 */
	public void setLowerRange(String string) {
		lowerRange = string;
	}

	/**
	 * @param string
	 */
	public void setUpperRange(String string) {
		upperRange = string;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode1() {
		return targetExamCode1;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode2() {
		return targetExamCode2;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode3() {
		return targetExamCode3;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode1(String string) {
		targetExamCode1 = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode2(String string) {
		targetExamCode2 = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode3(String string) {
		targetExamCode3 = string;
	}

	/**
	 * @return
	 */
	public String getTargetPersonId() {
		return targetPersonId;
	}

	/**
	 * @param string
	 */
	public void setTargetPersonId(String string) {
		targetPersonId = string;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * @return
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param string
	 */
	public void setMode(String string) {
		mode = string;
	}

	/**
	 * @return
	 */
	public String[] getPrintTarget() {
		return printTarget;
	}

	/**
	 * @param strings
	 */
	public void setPrintTarget(String[] strings) {
		printTarget = strings;
	}

	/**
	 * @return
	 */
	public String getPrintStamp() {
		return printStamp;
	}

	/**
	 * @param string
	 */
	public void setPrintStamp(String string) {
		printStamp = string;
	}

	/**
	 * @return
	 */
	public String getPrintStudent() {
		return printStudent;
	}

	/**
	 * @param string
	 */
	public void setPrintStudent(String string) {
		printStudent = string;
	}

	/**
	 * @return
	 */
	public String getPrintStatus() {
		return printStatus;
	}

	/**
	 * @param string
	 */
	public void setPrintStatus(String string) {
		printStatus = string;
	}
	
	/**
	 * @return
	 */
	public String getScrollX() {
		return scrollX;//[1] add
	}

	/**
	 * @return
	 */
	public String getScrollY() {
		return scrollY;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollX(String string) {
		scrollX = string;//[1] add
	}

	/**
	 * @param string
	 */
	public void setScrollY(String string) {
		scrollY = string;//[1] add
	}
	
}

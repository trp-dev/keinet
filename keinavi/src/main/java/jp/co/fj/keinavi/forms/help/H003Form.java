/*
 * 作成日: 2004/09/13
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.help;

import com.fjh.forms.ActionForm;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class H003Form extends ActionForm {

	private String freeWord = "";		// フリーワード
	private String category = null;	// カテゴリー
	private String helpId = "";		// ヘルプID
	private String useful = "";		// 役立ち度
	private String comment = "";		// コメント
	private String name = "";			// 名前
	private String detailBack = "";	// 詳細表示("h003")からの戻り先
	private String page = "1";			// 表示対象となるページ番号


	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		// 

	}


	/**
	 * @return
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param string
	 */
	public void setCategory(String string) {
		category = string;
	}


	/**
	 * @return
	 */
	public String getFreeWord() {
		return freeWord;
	}

	/**
	 * @param string
	 */
	public void setFreeWord(String string) {
		freeWord = string;
	}

	/**
	 * @return
	 */
	public String getHelpId() {
		return helpId;
	}

	/**
	 * @param string
	 */
	public void setHelpId(String string) {
		helpId = string;
	}

	/**
	 * @return
	 */
	public String getPage() {
		return page;
	}

	/**
	 * @param string
	 */
	public void setPage(String string) {
		page = string;
	}

	//-------------------------------------------------
	/**
	 * @return
	 */
	public String getUseful() {
		return useful;
	}

	/**
	 * @param string
	 */
	public void setUseful(String string) {
		useful = string;
	}

	/**
	 * @return
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param string
	 */
	public void setComment(String string) {
		comment = string;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param string
	 */
	public void setName(String string) {
		name = string;
	}

	/**
	 * @return
	 */
	public String getDetailBack() {
		return detailBack;
	}

	/**
	 * @param string
	 */
	public void setDetailBack(String string) {
		detailBack = string;
	}
	
}

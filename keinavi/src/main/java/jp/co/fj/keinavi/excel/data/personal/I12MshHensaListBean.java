package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
import java.util.ArrayList;
/**
 * �l���ѕ��́|���ѕ��͖ʒk �͎��ʕ΍��l���X�g
 * �쐬��: 2004/07/20
 * @author	A.Iwata
 * 
 * 2009.11.30   Fujito URAKAWA - Totec
 *              �u�w�̓��x���v�ǉ��Ή�
 */
public class I12MshHensaListBean {
	//�͎���
	private String strMshmei = "";
	//�͎����{���
	private String strMshDate = "";
//S 2004.09.03 A.Hasegawa
	//���_
	private int intTokuten = 0;
	//�΍��l
	private float floHensa = 0;
//E 2004.09.03
	//�u�]��w�ʃ��X�g
	private ArrayList i12KmkShiboList = new ArrayList();
	//�w�̓��x��
	private String strScholarLevel = "";
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
//S 2004.09.03 A.Hasegawa
  	public int getIntTokuten() {
	    return this.intTokuten;
	}
	public float getFloHensa() {
	 	return this.floHensa;
	}
//E 2004.09.02
	public ArrayList getI12KmkShiboList() {
		return this.i12KmkShiboList;
	}
	public String getStrScholarLevel() {
		return strScholarLevel;
	}
		
	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
//S 2004.09.03 A.Hasegawa
	public void setIntTokuten(int intTokuten) {
		this.intTokuten = intTokuten;
	}
	public void setFloHensa(float floHensa) {
		this.floHensa = floHensa;
	}
//E 2004.09.03
	public void setI12KmkShiboList(ArrayList i12KmkShiboList) {
		this.i12KmkShiboList = i12KmkShiboList;
	}
	public void setStrScholarLevel(String string) {
		this.strScholarLevel = string;
	}
}

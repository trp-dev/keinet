package jp.co.fj.keinavi.data.profile;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import jp.co.fj.keinavi.beans.profile.ProfileTransactionBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;

import com.fjh.db.DBManager;

/**
 *
 * プロファイルオブジェクト
 *
 *
 * 2005.02.09	Yoshimoto KAWAI - Totec
 * 				バージョンを導入
 *
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 * @author kawai
 *
 */
public class Profile implements Serializable, HttpSessionBindingListener {

	// セッションキー
	public static final String SESSION_KEY = "Profile";

	private String profileID; // プロファイルID
	private String profileName; // プロファイル名称
	private String folderID; // プロファイルフォルダID
	private Date createDate; // 作成日時
	private Date updateDate; // 更新日時
	private String comment; // コメント
	private int overwriteMode; // 上書き禁止モード
	private String userID; // ユーザID（作成者の利用者ID）
	private String userName; // ユーザ名（作成者の利用者名）
	private String bundleCD; // 一括コード（集計区分コード）
	private String sectorSortingCD; // 部門分類コード
	private String dbKey; // DBKEY
	private boolean isTemplate; // ひな型プロファイルか
	private boolean changed; // 変更を加えたかどうか
	private String folderName; // プロファイルフォルダ名称
	private String bundleName; // 一括名（集計区分名称）
	private String targetYear; // トップ画面の対象年度
	private String targetExam; // トップ画面の対象模試

	private Map categoryMap = new HashMap(); // カテゴリマップ

	/**
	 * コンストラクタ
	 */
	public Profile() {
	}

	/**
	 * コンストラクタ
	 */
	public Profile(String profileID) {
		this.profileID = profileID;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return this.getProfileID().equals(((Profile)obj).getProfileID());
	}

	/**
	 * 営業（高校代行）のプロファイルかどうか
	 * @return
	 */
	public boolean isDeputy() {

		return sectorSortingCD != null && bundleCD != null;
	}

	/**
	 * 指定されたアイテムマップを取得する
	 * @param key
	 * @return
	 */
	public Map getItemMap(String key) {
		// なければ作る
		if (!categoryMap.containsKey(key)) {
			categoryMap.put(key, new HashMap());
		}
		return (Map)categoryMap.get(key);
	}

	/* (非 Javadoc)
	 * @see javax.servlet.http.HttpSessionBindingListener#valueBound(javax.servlet.http.HttpSessionBindingEvent)
	 */
	public void valueBound(HttpSessionBindingEvent e) {
		// やることはない

	}

	/* (非 Javadoc)
	 * @see javax.servlet.http.HttpSessionBindingListener#valueUnbound(javax.servlet.http.HttpSessionBindingEvent)
	 */
	public void valueUnbound(HttpSessionBindingEvent event) {
		// 上書き禁止を解除する
		if (overwriteMode == 0 && profileID != null) {
			Connection con = null;
			try {
				con = DBManager.getConnectionPool(dbKey);
				ProfileTransactionBean ptb = new ProfileTransactionBean();
				ptb.setConnection(null, con);
				ptb.setProfileID(profileID);
				ptb.setMode("0");
				ptb.execute();
				con.commit();
			} catch (Exception e) {
				try {
					if (con != null) con.rollback();
				} catch (SQLException se) { }
				e.printStackTrace();
			} finally {
				try {
					if (con != null) DBManager.releaseConnectionPool(dbKey, con);
				} catch (Exception e) { }
			}
			System.out.println("exit:"+profileID);
		}
	}

	/**
	 * @return
	 */
	public short getVersion() {
		Short value =
			(Short) this.getItemMap(IProfileCategory.CM).get(IProfileItem.VERSION);

		return value == null ? 1 : value.shortValue();
	}

	/**
	 * @param version
	 */
	public void setVersion(short version) {
		this.getItemMap(IProfileCategory.CM).put(IProfileItem.VERSION, new Short(version));
	}

	/**
	 * @return
	 */
	public String getBundleCD() {
		return bundleCD;
	}

	/**
	 * @return
	 */
	public Map getCategoryMap() {
		return categoryMap;
	}

	/**
	 * @return
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @return
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @return
	 */
	public String getFolderID() {
		return folderID;
	}

	/**
	 * @return
	 */
	public int getOverwriteMode() {
		return overwriteMode;
	}

	/**
	 * @return
	 */
	public String getProfileID() {
		return profileID;
	}

	/**
	 * @return
	 */
	public String getProfileName() {
		return profileName;
	}

	/**
	 * @param string
	 */
	public void setBundleCD(String string) {
		bundleCD = string;
	}

	/**
	 * @param map
	 */
	public void setCategoryMap(Map map) {
		categoryMap = map;
	}

	/**
	 * @param string
	 */
	public void setComment(String string) {
		comment = string;
	}

	/**
	 * @param date
	 */
	public void setCreateDate(Date date) {
		createDate = date;
	}

	/**
	 * @param string
	 */
	public void setFolderID(String string) {
		folderID = string;
	}

	/**
	 * @param i
	 */
	public void setOverwriteMode(int i) {
		overwriteMode = i;
	}

	/**
	 * @param string
	 */
	public void setProfileID(String string) {
		profileID = string;
	}

	/**
	 * @param string
	 */
	public void setProfileName(String string) {
		profileName = string;
	}

	/**
	 * @return
	 */
	public String getSectorSortingCD() {
		return sectorSortingCD;
	}

	/**
	 * @param string
	 */
	public void setSectorSortingCD(String string) {
		sectorSortingCD = string;
	}

	/**
	 * @return
	 */
	public String getBundleName() {
		return bundleName;
	}

	/**
	 * @return
	 */
	public String getFolderName() {
		return folderName;
	}

	/**
	 * @param string
	 */
	public void setBundleName(String string) {
		bundleName = string;
	}

	/**
	 * @param string
	 */
	public void setFolderName(String string) {
		folderName = string;
	}

	/**
	 * @return
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param date
	 */
	public void setUpdateDate(Date date) {
		updateDate = date;
	}

	/**
	 * @return
	 */
	public boolean isChanged() {
		return changed;
	}

	/**
	 * @param b
	 */
	public void setChanged(boolean b) {
		changed = b;
	}

	/**
	 * @return
	 */
	public String getUserID() {
		return userID;
	}

	/**
	 * @param string
	 */
	public void setUserID(String string) {
		userID = string;
	}

	/**
	 * @param string
	 */
	public void setDbKey(String string) {
		dbKey = string;
	}

	/**
	 * @return
	 */
	public String getTargetExam() {
		return targetExam;
	}

	/**
	 * @return
	 */
	public String getTargetYear() {
		return targetYear;
	}

	/**
	 * @param string
	 */
	public void setTargetExam(String string) {
		targetExam = string;
	}

	/**
	 * @param string
	 */
	public void setTargetYear(String string) {
		targetYear = string;
	}

	/**
	 * @return userName を戻します。
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName 設定する userName。
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return isTemplate を戻します。
	 */
	public boolean isTemplate() {
		return isTemplate;
	}

	/**
	 * ひな型プロファイルとする
	 */
	public void setTemplate() {
		isTemplate = true;
	}

	/* 2016/03/10 QQ)Nishiyama 大規模改修 ADD START */
	/**
	 * isTemplate 設定する bool
	 */
	public void setTemplate(boolean bool) {
		isTemplate = bool;
	}
	/* 2016/03/10 QQ)Nishiyama 大規模改修 ADD END */

}

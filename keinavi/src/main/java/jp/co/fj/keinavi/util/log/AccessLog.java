package jp.co.fj.keinavi.util.log;

import java.util.logging.*;

/**
 * アクセスログをファイルに出力するクラス
 *
 * 2005.02.23 Yoshimoto KAWAI - Totec
 *            利用者ＩＤの出力に対応
 *
 * @Date		2004/06/07
 * @author	TOTEC)Nishiyama
 * 
 */
public class AccessLog extends Log {

	/**
	 * ログ出力オブジェクト
	 */
	protected static AccessLogWriter alog = (AccessLogWriter) AccessLogWriter.factory();

	/**
	 * アクセスレベル
	 */
	private static final String LEVEL_Lv1 = "Lv1";
	private static final String LEVEL_Lv2 = "Lv2";
	
	/**
	 * アクセスログ（Lv1）を出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param TransactionID	処理ID
	 * @param ExecutiveID		実行条件
	 * @param SupplementInfo	補足情報
	 */
	public static void Lv1(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String TransactionID,
						int ExecutiveID,
						String SupplementInfo) {

		LogRecord rec = new LogRecord(Level.INFO, "");
		setCallStack2Record(rec, Depth);
		_AccLog(rec, AccessLog.LEVEL_Lv1, IPAddress, ContractID, EmployeeID, account, TransactionID, ExecutiveID, SupplementInfo);
	}

	/**
	 * アクセスログ（Lv2）を出力する
	 *
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param TransactionID	処理ID
	 * @param ExecutiveID		実行条件
	 * @param SupplementInfo	補足情報
	 */
	public static void Lv2(String IPAddress,
						String ContractID,
						String EmployeeID,
						String account,
						String TransactionID,
						int ExecutiveID,
						String SupplementInfo) {

		LogRecord rec = new LogRecord(Level.INFO, "");
		setCallStack2Record(rec, Depth);
		_AccLog(rec, AccessLog.LEVEL_Lv2, IPAddress, ContractID, EmployeeID, account, TransactionID, ExecutiveID, SupplementInfo);
	}

	/**
	 * アクセスログを出力する
	 *
	 * @param rec						ログレコードクラス
	 * @param Mode						アクセスログ出力モード
	 * @param IPAddress			IPアドレス
	 * @param ContractID			契約ID
	 * @param EmployeeID			職員コード
	 * @param TransactionID	処理ID
	 * @param ExecutiveID		実行条件
	 * @param SupplementInfo	補足情報
	 */
	public static void _AccLog(LogRecord rec,
							String Mode,
							String IPAddress,
							String ContractID,
							String EmployeeID,
							String account,
							String TransactionID,
							int ExecutiveID,
							String SupplementInfo) {

		try {
			// 表示文字列の作成
			String msg = Mode + " " +
						convert(IPAddress) + " " +
						convert(ContractID) + " " +
						convert(EmployeeID) + " " +
						convert(account) + " " +
						rec.getSourceClassName() + "." + rec.getSourceMethodName() + " " +
						/*IntToHex8(*/TransactionID/*)*/ + " " +
						ExecutiveID + " " +
						convert(SupplementInfo);
			
			rec.setMessage(msg);
			alog.LogOutput(rec);
			
			// デバッグログを出力する。
			DebugLog.__DebugLog(rec);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

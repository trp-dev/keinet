package jp.co.fj.keinavi.excel.data.personal;

/**
 * 個人成績分析−個人成績表−連続個人成績表 教科別成績リスト
 * 作成日: 2004/07/20
 * @author	A.Iwata
 *
 * 2009.11.25   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 */
public class I13KyokaSeisekiListBean {

    //教科コード
    private String strKyokaCd = "";
    //型・科目名
    private String strKmkmei = "";
    //配点
    private String strHaitenKmk = "";
    //得点
    private int intTokuten = 0;
    //換算得点
    private int intKansanTokuten = 0;
    //偏差値
    private float floHensa = 0;
    //学力レベル
    private String strScholarLevel = "";
    //範囲区分
    private int intScope =0;
    //基礎科目フラグ
    private String basicFlg;

    // 2019/08/23 Hics)Ueta 共通テスト対応 ADD START
    //国語記述問1評価
    private String strKokugoDesk1 = "";
    //国語記述問2評価
    private String strKokugoDesk2 = "";
    //国語記述問3評価
    private String strKokugoDesk3 = "";
    //国語記述問4評価
    private String strKokugoDesk4 = "";
    // 2019/08/23 Hics)Ueta 共通テスト対応 ADD END
    // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
    // CEFR判定データ
    //private String cefrScore = "";
    // 模試種別コード
    //private String excdtype = "";
    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
    // 科目コード
    private String subCd = "";
    //国語記述得点
    private String kokugoTokuten = "";
    // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END

    /*----------*/
    /* Get      */
    /*----------*/
    public String getStrKyokaCd() {
        return this.strKyokaCd;
    }
    public String getStrKmkmei() {
        return this.strKmkmei;
    }
    public String getStrHaitenKmk() {
        return this.strHaitenKmk;
    }
    public int getIntTokuten() {
        return this.intTokuten;
    }
    public int getIntKansanTokuten() {
        return this.intKansanTokuten;
    }
    public float getFloHensa() {
        return this.floHensa;
    }
    public String getStrScholarLevel() {
        return strScholarLevel;
    }
    public int getIntScope() {
        return intScope;
    }
    public String getBasicFlg() {
        return basicFlg;
    }
    // 2019/08/23 Hics)Ueta 共通テスト対応 ADD START
    public String getStrKokugoDesk1() {
        return strKokugoDesk1;
    }
    public String getStrKokugoDesk2() {
        return strKokugoDesk2;
    }
    public String getStrKokugoDesk3() {
        return strKokugoDesk3;
    }
    public String getStrKokugoDesk4() {
        return strKokugoDesk4;
    }
    // 2019/08/23 Hics)Ueta 共通テスト対応 ADD END
    // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
//	public String getCefrScore() {
//		return cefrScore;
//	}
//	public String getExcdtype() {
//		return excdtype;
//	}
    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
    public String getSubCd() {
        return this.subCd;
    }
    public String getKokugoTokuten() {
        return this.kokugoTokuten;
    }
    // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END

    /*----------*/
    /* Set      */
    /*----------*/
    public void setStrKyokaCd(String strKyokaCd) {
        this.strKyokaCd = strKyokaCd;
    }
    public void setStrKmkmei(String strKmkmei) {
        this.strKmkmei = strKmkmei;
    }
    public void setStrHaitenKmk(String strHaitenKmk) {
        this.strHaitenKmk = strHaitenKmk;
    }
    public void setIntTokuten(int intTokuten) {
        this.intTokuten = intTokuten;
    }
    public void setIntKansanTokuten(int intKansanTokuten) {
        this.intKansanTokuten = intKansanTokuten;
    }
    public void setFloHensa(float floHensa) {
        this.floHensa = floHensa;
    }
    public void setStrScholarLevel(String strScholarLevel) {
        this.strScholarLevel = strScholarLevel;
    }
    public void setIntScope(int intScope) {
        this.intScope = intScope;
    }
    public void setBasicFlg(String basicFlg) {
        this.basicFlg = basicFlg;
    }
    // 2019/08/23 Hics)Ueta 共通テスト対応 ADD START
    public void setStrKokugoDesk1(String strKokugoDesk1) {
        this.strKokugoDesk1 = strKokugoDesk1;
    }
    public void setStrKokugoDesk2(String strKokugoDesk2) {
        this.strKokugoDesk2 = strKokugoDesk2;
    }
    public void setStrKokugoDesk3(String strKokugoDesk3) {
        this.strKokugoDesk3 = strKokugoDesk3;
    }
    public void setStrKokugoDesk4(String strKokugoDesk4) {
        this.strKokugoDesk4 = strKokugoDesk4;
    }
    // 2019/08/23 Hics)Ueta 共通テスト対応 ADD END
 // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD START
    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
//    public void setCefrScore(String cefrScore) {
//		this.cefrScore = cefrScore;
//	}
    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
    public void setSubCd(String subCd) {
        this.subCd = subCd;
    }
    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL START
//	public void setExcdtype(String excdtype) {
//		this.excdtype = excdtype;
//	}
    // 2019/11/21 QQ)Ooseto 英語認定試験延期対応 DEL END
    public void setKokugoTokuten(String kokugoTokuten) {
        this.kokugoTokuten = kokugoTokuten;
    }
    // 2019/09/18 QQ)Tanouchi 共通テスト対応 ADD END
}

package jp.co.fj.keinavi.beans.sheet.user.data;

import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.common.ISheetData;

/**
 *
 * 利用者登録通知書データクラス
 * 
 * 2005.11.01	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U01_01Data implements ISheetData {

	private final List u01_01ListData = new ArrayList();

	/**
	 * @return u01_01ListData を戻します。
	 */
	public List getU01_01ListData() {
		return u01_01ListData;
	}
	
}

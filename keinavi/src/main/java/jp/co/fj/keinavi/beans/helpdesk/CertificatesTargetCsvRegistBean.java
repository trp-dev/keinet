/*
 * 作成日: 2015/10/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.helpdesk;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.csv.CSVLine;
import jp.co.fj.keinavi.beans.csv.CSVReader;
import jp.co.fj.keinavi.forms.helpdesk.HD101Form;

import com.fjh.beans.DefaultBean;

/**
 * ユーザ管理（証明書TSV出力対象のCSV登録）
 *
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CertificatesTargetCsvRegistBean extends DefaultBean {

	private HD101Form hd101Form = null;	// HD101Form 情報
	private HelpDeskRegistBean hdrBean = new HelpDeskRegistBean();


	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		// コネクション設定
		this.hdrBean.setConnection(null, conn);

		// テンポラリ削除・登録
		this.deleteTemp(conn);
		this.readCsv(conn);

		// コミット
		conn.commit();
	}

	/**
	 *
	 * テンポラリを削除する
	 *
	 * @param con     DBコネクション
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private void deleteTemp(Connection con) throws SQLException {

		PreparedStatement stmt = null;
		StringBuffer sb = new StringBuffer();

		sb.append("TRUNCATE TABLE CERT_TARGET");
		stmt = con.prepareStatement(sb.toString());
		stmt.executeUpdate();

		if (stmt != null) stmt.close();

	}

	/**
	 *
	 * CSVファイルのアップロード結果をテンポラリに登録する
	 *
	 * @param con     DBコネクション
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private void readCsv(Connection con) throws Exception {

		CSVReader reader = new CSVReader(new String(hd101Form.getFileData()));
		CSVLine line = null;
		int lineNo = 0;

		String userId = "";

		boolean isErr = false;

		PreparedStatement stmt = null;
		StringBuffer sb = new StringBuffer();
		int count = 0;

		sb.append("INSERT INTO CERT_TARGET");
		sb.append(" (");
		sb.append("    LINENO");
		sb.append("  , USERID");
		sb.append(" ) VALUES (");
		sb.append("    ?");		// 行番号
		sb.append("  , ?");		// ユーザID
		sb.append(" )");

		stmt = con.prepareStatement(sb.toString());

		// CSVの内容をループ
		while ((line = reader.readLine()) != null) {
			isErr = false;
			lineNo++;
			String[] items = line.getItems();
			if (items.length == 3) {
				// ユーザID
				userId = items[1];

				// ユーザID
				if (!FormatChecker.isUserId(userId)) {
					isErr = true;
					hdrBean.setErrorList(lineNo, "", userId, "ユーザＩＤの指定に誤りがあります。");
				}
				if (!isErr) {
					// テンポラリに登録
					stmt.setString( 1, Integer.toString(lineNo));
					stmt.setString( 2, userId);
					count = stmt.executeUpdate();

					if (count == 0) {
						throw new SQLException("証明書発行対象データへのINSERTに失敗しました。");
					}
				}

			} else {
				hdrBean.setErrorList(lineNo, "", "", "項目数が違います。");
			}
		}
		if(lineNo == 0){
			hdrBean.setErrorList(lineNo, "", "", "ファイルが読み込めませんでした。");
		}

		if (stmt != null) stmt.close();
	}

	/**
	 * @return HD101Form
	 */
	public HD101Form getHD101Form() {
		return hd101Form;
	}
	/**
	 * @param string
	 */
	public void setHD101Form(HD101Form form) {
		hd101Form = form;
	}

	/**
	 * @return
	 */
	public HelpDeskRegistBean getHdrBean() {
		return hdrBean;
	}

	/**
	 * @param bean
	 */
	public void setHdrBean(HelpDeskRegistBean bean) {
		hdrBean = bean;
	}
}

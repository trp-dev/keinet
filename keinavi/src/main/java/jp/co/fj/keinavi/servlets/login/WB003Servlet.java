package jp.co.fj.keinavi.servlets.login;

import java.io.IOException;
import java.sql.Connection;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.auth.KnetCookieManager;
import jp.co.fj.keinavi.beans.login.DeputyLoginBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.login.SectorData;
import jp.co.fj.keinavi.data.login.SectorSession;
import jp.co.fj.keinavi.forms.login.WB003Form;
import jp.co.fj.keinavi.interfaces.ILogin;
import jp.co.fj.keinavi.interfaces.SpecialAppliMenu;
import jp.co.fj.keinavi.servlets.DefaultLoginServlet;
import jp.co.fj.keinavi.servlets.KNServletException;

/**
 * 
 * 営業部用ログインサーブレット
 * 
 * 2004.06.30 [新規作成]
 * 
 * <2010年度マーク高２模試対応> 2011.01.24 Tomohisa YAMADA アップロード機能
 * 
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class WB003Servlet extends DefaultLoginServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = 4907348553078749238L;

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {

		// ログイン情報
		final LoginSession login = getLoginSession(request);
		// アクションフォーム
		final WB003Form form = (WB003Form) getActionForm(request,
				"jp.co.fj.keinavi.forms.login.WB003Form");

		if ("logout".equals(getForward(request))) {
			// ログアウト
			dispatch(request, response);
		} else if ("wb003".equals(getBackward(request))) {
			// 転送元が営業選択画面

			// セッションから部門データを取得する
			final SectorSession sector = getSectorSession(request);
			int index = sector.getSectorList().indexOf(
					new SectorData(form.getSectorCD(), "1"));
			final SectorData data;
			if (index >= 0) {
				data = (SectorData) sector.getSectorList().get(index);
			} else {
				throw new ServletException("部門データの取得に失敗しました。");
			}

			if (!"2".equals(form.getMenu())) {
				// 営業部メニュー or 高校作成ファイルダウンロード
				login.setUserMode(ILogin.SALES_NORMAL);
				login.setSectorCd(data.getSectorCD());
				login.setSectorSortingCD(data.getSectorSortingCD());
				login.setUserName(data.getSectorName());
			} else {
				// 代行メニュー
				Connection con = null;
				try {
					con = getConnectionPool(request);

					final DeputyLoginBean bean = new DeputyLoginBean();
					bean.setConnection(null, con);
					bean.setSchoolCD(form.getSchoolCD());
					bean.setSectorCd(data.getSectorCD());
					bean.setSectorSortingCD(data.getSectorSortingCD());
					bean.setLoginSession(login);
					bean.execute();

					new KnetCookieManager().create(response, login);

					// 営業の代行は非契約校として扱う
					login.setPactDiv(ILogin.NO_PACT);

				} catch (final KNServletException e) {
					// 元の画面に戻る場合
					setErrorMessage(request, e);
				} catch (final Exception e) {
					// 想定外のエラー
					throw createServletException(e);
				} finally {
					releaseConnectionPool(request, con);
				}
			}

			if (getErrorMessage(request) == null) {
				// エラーメッセージがなければプロファイル選択画面へ
				setupMenuSecurity(request);
				forward(request, response, SERVLET_DISPATCHER);
			} else {
				// エラーメッセージがあるなら元の画面へ
				setupMenuInfo(request);
				forward(request, response, JSP_WB003);
			}

		} else {
			// それ以外はJSPへ
			setupMenuInfo(request);
			forward(request, response, JSP_WB003);
		}
	}

	/**
	 * 利用可能なメニュー情報をセットアップします。
	 */
	private void setupMenuInfo(HttpServletRequest request) {

		Set menuIdSet = (Set) request.getSession(false).getAttribute(
				SpecialAppliMenu.SESSION_KEY);

		for (Iterator ite = SpecialAppliMenu.LIST.iterator(); ite.hasNext();) {
			SpecialAppliMenu menu = (SpecialAppliMenu) ite.next();
			request.setAttribute(menu.id,
					Boolean.valueOf(menuIdSet.contains(menu.id)));
		}
	}

}

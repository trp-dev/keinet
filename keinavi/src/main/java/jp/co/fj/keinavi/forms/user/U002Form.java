package jp.co.fj.keinavi.forms.user;


/**
 *
 * 利用者情報管理−登録編集画面アクションフォーム
 * 
 * 2005.10.12	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U002Form extends U001Form {
	
	// 新規利用者ID
	private String newLoginId;
	// 変更前の利用者ID
	private String originalLoginId;
	// 利用者名
	private String loginName;
	// メンテナンス権限
	private String authMa;
	// Kei-Navi機能権限
	private String authKn;
	// 校内成績機能権限
	private String authSc;
	// 入試結果調査機能権限
	private String authEe;
	// 答案閲覧機能権限
	private String authAb;
	
	/**
	 * @return authEe を戻します。
	 */
	public String getAuthEe() {
		return authEe;
	}
	/**
	 * @param authEe 設定する authEe。
	 */
	public void setAuthEe(String authEe) {
		this.authEe = authEe;
	}
	/**
	 * @return authKn を戻します。
	 */
	public String getAuthKn() {
		return authKn;
	}
	/**
	 * @param authKn 設定する authKn。
	 */
	public void setAuthKn(String authKn) {
		this.authKn = authKn;
	}
	/**
	 * @return authMa を戻します。
	 */
	public String getAuthMa() {
		return authMa;
	}
	/**
	 * @param authMa 設定する authMa。
	 */
	public void setAuthMa(String authMa) {
		this.authMa = authMa;
	}
	/**
	 * @return authSc を戻します。
	 */
	public String getAuthSc() {
		return authSc;
	}
	/**
	 * @param authSc 設定する authSc。
	 */
	public void setAuthSc(String authSc) {
		this.authSc = authSc;
	}
	/**
	 * @return loginName を戻します。
	 */
	public String getLoginName() {
		return loginName;
	}
	/**
	 * @param loginName 設定する loginName。
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	/**
	 * @return newLoginId を戻します。
	 */
	public String getNewLoginId() {
		return newLoginId;
	}
	/**
	 * @param newLoginId 設定する newLoginId。
	 */
	public void setNewLoginId(String newLoginId) {
		this.newLoginId = newLoginId;
	}
	/**
	 * @return originalLoginId を戻します。
	 */
	public String getOriginalLoginId() {
		return originalLoginId;
	}
	/**
	 * @param pOriginalLoginId 設定する originalLoginId。
	 */
	public void setOriginalLoginId(String pOriginalLoginId) {
		originalLoginId = pOriginalLoginId;
	}
	/**
	 * @return authAb を戻します。
	 */
	public String getAuthAb() {
		return authAb;
	}
	/**
	 * @param authAb 設定する authAb。
	 */
	public void setAuthAb(String authAb) {
		this.authAb = authAb;
	}
}

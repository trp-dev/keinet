/**
 * 校内成績分析−クラス比較　成績概況（クラス比較）
 * 出力する帳票の判断
 * 作成日: 2004/07/26
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.school.S32Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

public class S32 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean s32( S32Item s32Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {

		KNLog log = KNLog.getInstance(null,null,null);

		try{
			int ret = 0;
			//S32Itemから各帳票を出力
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD START
//// 2019/09/13 DP)H.Nonaka 共通テスト対応 MOD START
////			if (( s32Item.getIntHyouFlg()==0 )&&( s32Item.getIntBnpGraphFlg()==0 )&&
////				( s32Item.getIntNinzuGraphFlg()==0 )&&( s32Item.getIntKoseiGraphFlg()==0 )) {
//			if (( s32Item.getIntHyouFlg()==0 )
//				&&( s32Item.getIntBnpGraphFlg()==0 )
//				&&( s32Item.getIntNinzuGraphFlg()==0 )
//				&&( s32Item.getIntKoseiGraphFlg()==0 )
//				&&( s32Item.getIntCheckBoxFlg()==0 )) {
//// 2019/09/13 DP)H.Nonaka 共通テスト対応 MOD END
			if (( s32Item.getIntHyouFlg()==0 )&&( s32Item.getIntBnpGraphFlg()==0 )&&
				( s32Item.getIntNinzuGraphFlg()==0 )&&( s32Item.getIntKoseiGraphFlg()==0 )) {
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD END
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
			    //throw new Exception("S32 ERROR : フラグ異常");
			    throw new IllegalStateException("S32 ERROR : フラグ異常");
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			}
			if (( s32Item.getIntHyouFlg()==1 )&&( s32Item.getIntKoseihiFlg()==2 )) {
				S32_01 exceledit = new S32_01();
				ret = exceledit.s32_01EditExcel( s32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S32_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S32_01");
			}
			if (( s32Item.getIntHyouFlg()==1 )&&( s32Item.getIntKoseihiFlg()==1 )) {
				S32_02 exceledit = new S32_02();
				ret = exceledit.s32_02EditExcel( s32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S32_02","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S32_02");
			}
			if (( s32Item.getIntBnpGraphFlg()==1 )&&( s32Item.getIntAxisFlg()==1 )) {
				S32_03 exceledit = new S32_03();
				ret = exceledit.s32_03EditExcel( s32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S32_03","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S32_03");
			}
			if (( s32Item.getIntBnpGraphFlg()==1 )&&( s32Item.getIntAxisFlg()==2 )) {
				S32_04 exceledit = new S32_04();
				ret = exceledit.s32_04EditExcel( s32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S32_04","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S32_04");
			}
			if (( s32Item.getIntNinzuGraphFlg()==1 )&&( s32Item.getIntNinzuPitchFlg()==1 )) {
				S32_05 exceledit = new S32_05();
				ret = exceledit.s32_05EditExcel( s32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S32_05","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S32_05");
			}
			if (( s32Item.getIntNinzuGraphFlg()==1 )&&( s32Item.getIntNinzuPitchFlg()==2 )) {
				S32_06 exceledit = new S32_06();
				ret = exceledit.s32_06EditExcel( s32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S32_06","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S32_06");
			}
			if (( s32Item.getIntNinzuGraphFlg()==1 )&&( s32Item.getIntNinzuPitchFlg()==3 )) {
				S32_07 exceledit = new S32_07();
				ret = exceledit.s32_07EditExcel( s32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S32_07","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S32_07");
			}
			if (( s32Item.getIntKoseiGraphFlg()==1 )&&( s32Item.getIntKoseiPitchFlg()==1 )) {
				S32_08 exceledit = new S32_08();
				ret = exceledit.s32_08EditExcel( s32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S32_08","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S32_08");
			}
			if (( s32Item.getIntKoseiGraphFlg()==1 )&&( s32Item.getIntKoseiPitchFlg()==2 )) {
				S32_09 exceledit = new S32_09();
				ret = exceledit.s32_09EditExcel( s32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S32_09","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S32_09");
			}
			if (( s32Item.getIntKoseiGraphFlg()==1 )&&( s32Item.getIntKoseiPitchFlg()==3 )) {
				S32_10 exceledit = new S32_10();
				ret = exceledit.s32_10EditExcel( s32Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S32_10","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S32_10");
			}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/09/13 DP)H.Nonaka 共通テスト対応 ADD START
//			if (( s32Item.getIntCheckBoxFlg()==1 )) {
//				S32_11 exceledit = new S32_11();
//				ret = exceledit.s32_11EditExcel( s32Item, outfilelist, saveFlg, UserID );
//				if (ret!=0) {
//					log.Err("0"+ret+"S32_11","帳票作成エラー","");
//					return false;
//				}
//				sheetLog.add("S32_11");
//			}
//// 2019/09/13 DP)H.Nonaka 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		} catch(Exception e) {
			log.Err("99S32","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
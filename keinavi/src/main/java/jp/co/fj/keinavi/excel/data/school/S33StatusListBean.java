/**
 * �Z�����ѕ��́|�N���X��r�@�ݖ�ʐ��сi�N���X��r�j
 *      ���ݖ�ʐ����� �Z���S�� �f�[�^�N���X
 * �쐬��: 2019/09/11
 * @author      M.Ooseto
 */

package jp.co.fj.keinavi.excel.data.school;

public class S33StatusListBean {
    // �ݖ�ԍ�
    private String questionNo = "";
    // �Z���S�́|�󌱎Ґ�
    private int numbers = 0;
    // �Z���S�́|�@�@����
    private float correctAnsrate1 = 0;
    // �Z���S�́|�A�@����
    private float correctAnsrate2 = 0;
    // �Z���S�́|�B�@����
    private float correctAnsrate3 = 0;
    // �Z���S�́|�C�@����
    private float correctAnsrate4 = 0;
    // �Z���S�́|�D�@����
    private float correctAnsrate5 = 0;
    // �Z���S�́|�E�@����
    private float correctAnsrate6 = 0;
    // �Z���S�́|�F�@����
    private float correctAnsrate7 = 0;
    // �Z���S�́|�@�@�l��
    private int correctPersonnum1 = 0;
    // �Z���S�́|�A�@�l��
    private int correctPersonnum2 = 0;
    // �Z���S�́|�B�@�l��
    private int correctPersonnum3 = 0;
    // �Z���S�́|�C�@�l��
    private int correctPersonnum4 = 0;
    // �Z���S�́|�D�@�l��
    private int correctPersonnum5 = 0;
    // �Z���S�́|�E�@�l��
    private int correctPersonnum6 = 0;
    // �Z���S�́|�F�@�l��
    private int correctPersonnum7 = 0;

    /*-----*/
    /* Get */
    /*-----*/
    public String getQuestionNo() {
        return this.questionNo;
    }
    public int getNumbers() {
        return this.numbers;
    }
    public float getCorrectAnsrate1() {
        return this.correctAnsrate1;
    }
    public float getCorrectAnsrate2() {
        return this.correctAnsrate2;
    }
    public float getCorrectAnsrate3() {
        return this.correctAnsrate3;
    }
    public float getCorrectAnsrate4() {
        return this.correctAnsrate4;
    }
    public float getCorrectAnsrate5() {
        return this.correctAnsrate5;
    }
    public float getCorrectAnsrate6() {
        return this.correctAnsrate6;
    }
    public float getCorrectAnsrate7() {
        return this.correctAnsrate7;
    }
    public int getCorrectPersonnum1() {
        return this.correctPersonnum1;
    }
    public int getCorrectPersonnum2() {
        return this.correctPersonnum2;
    }
    public int getCorrectPersonnum3() {
        return this.correctPersonnum3;
    }
    public int getCorrectPersonnum4() {
        return this.correctPersonnum4;
    }
    public int getCorrectPersonnum5() {
        return this.correctPersonnum5;
    }
    public int getCorrectPersonnum6() {
        return this.correctPersonnum6;
    }
    public int getCorrectPersonnum7() {
        return this.correctPersonnum7;
    }

    /*-----*/
    /* Set */
    /*-----*/
    public void setQuestionNo(String questionNo) {
        this.questionNo = questionNo;
    }
    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }
    public void setCorrectAnsrate1(float correctAnsrate1) {
        this.correctAnsrate1 = correctAnsrate1;
    }
    public void setCorrectAnsrate2(float correctAnsrate2) {
        this.correctAnsrate2 = correctAnsrate2;
    }
    public void setCorrectAnsrate3(float correctAnsrate3) {
        this.correctAnsrate3 = correctAnsrate3;
    }
    public void setCorrectAnsrate4(float correctAnsrate4) {
        this.correctAnsrate4 = correctAnsrate4;
    }
    public void setCorrectAnsrate5(float correctAnsrate5) {
        this.correctAnsrate5 = correctAnsrate5;
    }
    public void setCorrectAnsrate6(float correctAnsrate6) {
        this.correctAnsrate6 = correctAnsrate6;
    }
    public void setCorrectAnsrate7(float correctAnsrate7) {
        this.correctAnsrate7 = correctAnsrate7;
    }
    public void setCorrectPersonnum1(int correctPersonnum1) {
        this.correctPersonnum1 = correctPersonnum1;
    }
    public void setCorrectPersonnum2(int correctPersonnum2) {
        this.correctPersonnum2 = correctPersonnum2;
    }
    public void setCorrectPersonnum3(int correctPersonnum3) {
        this.correctPersonnum3 = correctPersonnum3;
    }
    public void setCorrectPersonnum4(int correctPersonnum4) {
        this.correctPersonnum4 = correctPersonnum4;
    }
    public void setCorrectPersonnum5(int correctPersonnum5) {
        this.correctPersonnum5 = correctPersonnum5;
    }
    public void setCorrectPersonnum6(int correctPersonnum6) {
        this.correctPersonnum6 = correctPersonnum6;
    }
    public void setCorrectPersonnum7(int correctPersonnum7) {
        this.correctPersonnum7 = correctPersonnum7;
    }
}

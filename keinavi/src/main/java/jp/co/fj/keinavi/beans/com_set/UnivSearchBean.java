/*
 *
 */
package jp.co.fj.keinavi.beans.com_set;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.com_set.cm.UnivData;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.JpnStringConv;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.message.MessageLoader;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;
import jp.co.fj.keinavi.util.sql.SQLUtil;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 共通項目設定 - 志望大学検索Bean
 *
 * 2004.06.30	[新規作成]
 *
 * 2009.09.29 Shoji.HASE - TOTEC
 				「HS3_UNIV_NAME：高３大学マスタ(志望用名称)」テーブルを
 * 				「UNIVMASTER_BASIC：大学マスタ基本情報」テーブルに置き換え
 *
 * 2009.10.21 Shoji.HASE - TOTEC
 *				UNIVDIVCD(１桁)→UNIDIV（２桁）変更にともない、条件分岐定数の変更
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 */
public class UnivSearchBean extends AbstractDefaultSearchBean {

	//「模試・志望大学検索」ボタン
	private static final String EXAM_BUTTON;
	static {
		try {
			EXAM_BUTTON = MessageLoader.getInstance().getMessage("cm001d");
		} catch (final IOException e) {
			final InternalError i = new InternalError(e.getMessage());
			i.initCause(e);
			throw i;
		}
		if (EXAM_BUTTON == null) {
			throw new NullPointerException(
					"「模試・志望大学検索」ボタン定義がありません。");
		}
	}

	private String button; // ボタン
	private String[] pref; // 県コードの配列
	private String[] univDiv; // 国私区分コードの配列
	private String searchMode; // 検索モード（大学名／大学コード）
	private String searchStr; // 大学名／大学コードの文字列
	private String[] registedUniv; // 登録済みの大学コード
	private ExamData exam; // 対象模試
	private String bundleCD; //一括コード

	/**
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws Exception {

		// 登録済みの大学コード
		final Set registed = CollectionUtil.array2Set(registedUniv);

		// 絞込み条件
		final StringBuffer cond = new StringBuffer();

		// 「上記の条件で検索」ボタン
		if ("上記の条件で検索".equals(button)) {
			// 県コード
			if (pref != null) {
				cond.append("AND u.prefcd_examho IN(");
				cond.append(SQLUtil.concatComma(pref));
				cond.append(") ");
			}

			// 国私区分
			addUnivDivCond(cond);

		//「模試・志望大学検索」ボタン
		} else if (EXAM_BUTTON.equals(button)) {
			// 国私区分
			addUnivDivCond(cond);

		// 「検索」ボタン
		} else if ("検索".equals(button)) {
			// 大学名
			if ("name".equals(searchMode)) {
				cond.append("AND TO_SEARCHSTR(u.univname_kana) LIKE CONCAT(TO_SEARCHSTR(");
				cond.append(SQLUtil.quote(JpnStringConv.hkana2Han(
						GeneralUtil.truncateString(searchStr, 12))));
				cond.append("), '%') ");

			// 大学コード
			} else if ("code".equals(searchMode)) {
				cond.append("AND u.univcd LIKE CONCAT(");
				cond.append(SQLUtil.quote(searchStr));
				cond.append(", '%') ");
			}

		// 大学名の頭文字
		} else {
			cond.append("AND u.univname_kana LIKE CONCAT(");
			cond.append(SQLUtil.quote(JpnStringConv.hkana2Han(button)));
			cond.append(", '%') ");
		}

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final Query query;
		    // 営業
		    if (bundleCD == null) {
		    	query = QueryLoader.getInstance().load("cm07_1");
		    // それ以外
		    } else {
		    	query = QueryLoader.getInstance().load("cm07_2");
		    }

		    query.replaceAll("#", cond.toString());

			if (EXAM_BUTTON.equals(button)) {
			    query.append("AND totalcandidatenum > 0 ");
			}

			// 2016/01/18 QQ)Nishiyama 大規模改修 UPD START
			//query.append("ORDER BY univcd");
			// 1.国私区分
			// 2.カナ
			// 3.大学コード
			query.append(" ORDER BY ");
			query.append("         DECODE(univdivcd, '01', '02', univdivcd)");
			query.append("       , univname_kana");
			query.append("       , univcd");
			// 2016/01/18 QQ)Nishiyama 大規模改修 UPD END

			// 書き換え
			KNUtil.rewriteUnivQuery(query, exam);

			ps = conn.prepareStatement(query.toString());
			ps.setString(1, exam.getExamYear()); // 模試年度
			ps.setString(2, exam.getExamDiv()); // 模試区分コード
			ps.setString(3, exam.getExamCD()); // 模試コード
			ps.setString(4, KNUtil.getRatingDiv(exam)); // 評価区分
			if (bundleCD != null) {
				ps.setString(5, bundleCD); // 一括コード
			}

			rs = ps.executeQuery();
			while (rs.next()) {
				UnivData data = new UnivData();
				data.setUnivCode(rs.getString(1));
				data.setUnivName(rs.getString(2));
				data.setUnivDiv(rs.getString(3));
				data.setLocation(rs.getString(4));
				data.setChoiceNum(rs.getInt(5));
				data.setRegisted(registed.contains(data.getUnivCode()));

				// 画面表示対象なら入れる
				if (isOutputItem()) {
					recordSet.add(data);
				// そうでなければダミー
				} else {
					recordSet.add(null);
				}
		   }
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}

		// ページ計算
		calculate();
	}

	/**
	 * 絞込条件に国私区分を追加する
	 * @param cond
	 */
	private void addUnivDivCond(StringBuffer cond) {

		if (univDiv == null) {
			return;
		}

		List container = new LinkedList(); // 入れ物

		for (int i=0; i<univDiv.length; i++) {
			// 国公立
			if ("01".equals(univDiv[i])) {
				container.add("01");
				container.add("02");

			// 私立
			} else if ("02".equals(univDiv[i])) {
				container.add("03");

			// 短大
			} else if ("03".equals(univDiv[i])) {
				container.add("04");
				container.add("05");

			// その他
			} else {
				container.add("06");
				container.add("07");
				container.add("08");
				container.add("09");
			}
		}

		cond.append("AND u.unidiv IN(");
		cond.append(SQLUtil.concatComma(
				(String[]) container.toArray(new String[0])));
		cond.append(") ");
	}

	/**
	 * @param string
	 */
	public void setBundleCD(String string) {
		bundleCD = string;
	}

	/**
	 * @param string
	 */
	public void setButton(String string) {
		button = string;
	}

	/**
	 * @param strings
	 */
	public void setPref(String[] strings) {
		pref = strings;
	}

	/**
	 * @param strings
	 */
	public void setRegistedUniv(String[] strings) {
		registedUniv = strings;
	}

	/**
	 * @param string
	 */
	public void setSearchMode(String string) {
		searchMode = string;
	}

	/**
	 * @param string
	 */
	public void setSearchStr(String string) {
		searchStr = string;
	}

	/**
	 * @param strings
	 */
	public void setUnivDiv(String[] strings) {
		univDiv = strings;
	}

	/**
	 * @param pExam 設定する exam
	 */
	public void setExam(ExamData pExam) {
		exam = pExam;
	}

}

package jp.co.fj.keinavi.excel.business;

/**
 * �Z�����ѕ��́|�S���������ъT���E�Ȗڕʐ��у��X�g
 * �쐬��: 2004/07/30
 * @author	Ito.Y
 */
public class B41BestListBean {
	//�΍��l
	private float	floHensa	= 0;
	//�u�b�N���
	private int	intBookNum	= 0;
	//�V�[�g���
	private int	intSheetNum	= 0;
	//�ʒu���
	private int	intRow		= 0;

	/*----------*/
	/* Get      */
	/*----------*/
	public float getFloHensa() {
		return floHensa;
	}
	public int getIntRow() {
		return intRow;
	}
	public int getIntSheetNum() {
		return intSheetNum;
	}
	public int getIntBookNum() {
		return intBookNum;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setFloHensa(float f) {
		floHensa = f;
	}
	public void setIntRow(int i) {
		intRow = i;
	}
	public void setIntSheetNum(int i) {
		intSheetNum = i;
	}
	public void setIntBookNum(int i) {
		intBookNum = i;
	}

}
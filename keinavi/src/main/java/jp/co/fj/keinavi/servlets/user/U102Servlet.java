package jp.co.fj.keinavi.servlets.user;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.beans.user.UserPasswordChangerBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.user.U102Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.util.message.MessageLoader;

/**
 *
 * 利用者パスワード変更画面サーブレット
 * 
 * 2005.10.13	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U102Servlet extends DefaultHttpServlet {

	// U002Servletのインスタンス
	private static final U002Servlet U002 = new U002Servlet();
	
	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		// ログインセッション
		final LoginSession login = getLoginSession(request);

		// 転送先がJSP
		if (getJspId().equals(getForward(request))) {
				
			// 利用者データをリクエストにセットする
			request.setAttribute("LoginUserData", 
					U002.getLoginUserData(request, login, login.getAccount()));
			
			forward2Jsp(request, response);
			
		// 確認画面への遷移
		} else {
			
			// アクションフォーム
			final U102Form form = (U102Form) getActionForm(
					request, "jp.co.fj.keinavi.forms.user.U102Form");
			
			// 実行
			if ("1".equals(form.getActionMode())) {
				
				Connection con = null;
				try {
					con = getConnectionPool(request);
					con.setAutoCommit(false);
					
					final UserPasswordChangerBean bean = new UserPasswordChangerBean(
							login.getUserID(), login.getAccount(), 
							form.getPrePassword(), form.getNewPassword());
					bean.setConnection(null, con);
					bean.execute();
					
					//	アクセスログ
					actionLog(request, "601");
					con.commit();

				// パスワード不一致エラー
				} catch (final KNBeanException e) {
					rollback(con);
					request.setAttribute("ErrorMessage",
							MessageLoader.getInstance().getMessage("u009a"));
					request.setAttribute("LoginUserData", 
							U002.getLoginUserData(request, login, login.getAccount()));
					forward2Jsp(request, response, getJspId());
					return;
					
				} catch (final Exception e) {
					rollback(con);
					throw new ServletException(e.getMessage());
				} finally {
					releaseConnectionPool(request, con);
				}
			}
			
			dispatch(request, response);
		}
	}

	/**
	 * @return JSPのID
	 */
	protected String getJspId() {
		return "u102";
	}
	
}

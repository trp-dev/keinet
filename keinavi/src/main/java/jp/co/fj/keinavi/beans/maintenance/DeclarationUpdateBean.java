package jp.co.fj.keinavi.beans.maintenance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 作業中宣言更新Bean
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class DeclarationUpdateBean extends DefaultBean {
	
	// 学校コード
	private final String schoolCd;
	// 登録するかどうか
	private final boolean isRegistMode;
	// 利用者ID
	private String loginId;
	
	/**
	 * コンストラクタ
	 */
	public DeclarationUpdateBean(final String pSchoolCd,
			final boolean pIsRegistMode) {
		
		if (pSchoolCd == null) {
			throw new IllegalArgumentException("学校コードがNULLです。");
		}
		
		this.schoolCd = pSchoolCd;
		this.isRegistMode = pIsRegistMode;
	}

	/**
	 * コンストラクタ
	 */
	public DeclarationUpdateBean(final String pSchoolCd) {

		this(pSchoolCd, true);
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		
		// 消す
		deleteDeclaration(conn, schoolCd);
		// 登録
		if (isRegistMode) {
	 		insertDeclaration(conn, schoolCd, loginId);
		}
	}

	/**
	 * 作業中宣言を削除する
	 * 
	 * @param con
	 * @param schoolCd
	 * @throws Exception
	 */
	protected void deleteDeclaration(final Connection con, final String schoolCd) throws Exception {

		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("DELETE FROM declaration WHERE schoolcd = ?");
			ps.setString(1, schoolCd);
			ps.executeUpdate();
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	/**
	 * 作業中宣言を登録する
	 * 
	 * @param con
	 * @param schoolCd
	 * @param loginId
	 * @throws Exception
	 */
	protected void insertDeclaration(final Connection con, final String schoolCd, final String loginId) throws Exception {

		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(
					"INSERT INTO declaration "
					+ "VALUES(?, ?, TO_CHAR(SYSDATE, 'YYYYMMDDHH24MI'))");
			ps.setString(1, schoolCd);
			ps.setString(2, loginId);
			ps.executeUpdate();
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	/**
	 * @param loginId 設定する loginId。
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

}

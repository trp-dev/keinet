package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 生徒情報削除Bean
 * 
 * 2005.11.24	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class KNStudentDeleteBean extends DefaultBean {

	private final String schoolCd;
	private final String individualId;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 処理する学校コード
	 * @param pIndividualId 削除する個人ID
	 */
	public KNStudentDeleteBean(final String pSchoolCd,
			final String pIndividualId) {

		if (pSchoolCd == null) {
			throw new IllegalArgumentException("学校コードがNULLです。");
		}
		
		if (pIndividualId == null) {
			throw new IllegalArgumentException("個人IDがNULLです。");
		}
		
		this.schoolCd = pSchoolCd;
		this.individualId = pIndividualId;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		// 個人レコードがあるならエラー
		if (existRecord()) {
			throw new KNBeanException(
					"個人レコードが存在するため削除できません。");
		}
		
		deleteBasicinfo();
		deleteHistoryinfo();
	}

	/**
	 * 個人レコード存在チェック
	 * 
	 * @return
	 * @throws Exception
	 */
	protected boolean existRecord() throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().load("m13").toString());
			ps.setString(1, individualId);
			rs = ps.executeQuery();
			return rs.next();
			
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	/**
	 * 学籍基本情報の削除
	 * 
	 * @throws SQLException
	 */
	private void deleteBasicinfo() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			
			ps = conn.prepareStatement(createBasicinfoQuery());
			ps.setString(1, individualId);
			ps.setString(2, schoolCd);
			
			if (ps.executeUpdate() == 0) {
				throw new SQLException(
						"学籍基本情報の削除に失敗しました。");
			}
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	protected String createBasicinfoQuery() throws SQLException {
		
		return "DELETE FROM basicinfo WHERE individualid = ? AND schoolcd = ?";
	}
	
	/**
	 * 学籍基本履歴の削除
	 * 
	 * @throws SQLException
	 */
	private void deleteHistoryinfo() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			
			ps = conn.prepareStatement(createHistoryinfoQuery());
			ps.setString(1, individualId);
			ps.executeUpdate();
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	protected String createHistoryinfoQuery() throws SQLException {
		
		return "DELETE FROM historyinfo WHERE individualid = ?";
	}

	/**
	 * @return individualId を戻します。
	 */
	protected String getIndividualId() {
		return individualId;
	}

	/**
	 * @return schoolCd を戻します。
	 */
	protected String getSchoolCd() {
		return schoolCd;
	}
	
}

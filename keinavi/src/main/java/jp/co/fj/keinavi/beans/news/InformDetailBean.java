/*
 * 作成日: 2004/09/17
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.news;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.fjh.beans.DefaultBean;


/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class InformDetailBean extends DefaultBean {

    private String infoId = null; 		// お知らせＩＤ
    private String displayDiv = null; 	// 表示区分
    private String title = null; 			// タイトル
    private String dispStartDTim = null; 	// 掲載開始日時
    private String dispEndDTim = null; 	// 掲載終了日時
    private String text = null; 			// 本文テキスト

    private String formId = null; 		// 呼び出し元フォームＩＤ

    private String SQL_BASE = "";


    /* (非 Javadoc)
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws SQLException, Exception {
        // TODO 自動生成されたメソッド・スタブ

        // ヘルプ詳細
        SQL_BASE = "SELECT  INFOID        " +
                   "       ,DISPLAYDIV    " +
                   "       ,TITLE         " +
                   "       ,DISPSTARTDTIM " +
                   "       ,DISPENDDTIM   " +
                   "       ,TEXT          " +
                   "  FROM  INFORMATION   " +
                   "  WHERE INFOID = ? ";
        PreparedStatement ps = conn.prepareStatement(SQL_BASE);
        ps.setString(1, infoId);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            // 各々の文字列変数に格納
            setInfoId(rs.getString(1));
            setDisplayDiv(rs.getString(2));
            setTitle(rs.getString(3));
            setDispStartDTim(rs.getString(4));
            setDispEndDTim(rs.getString(5));
            setText(rs.getString(6));
        }
        rs.close();
        ps.close();


    }

    /**
     * 呼び出し元フォームＩＤ
     * 　詳細画面のみかを判定させる
     * 	 * @return
     */
    public String getFormId() {
        return formId;
    }
    /**
     * @param string
     */
    public void setFormId(String string) {
        formId = string;
    }

    /**
     * 掲載開始日付（yyyy/mm/dd)を取得
     * 	 * @param string
     */
    public String getDispDate() {
        return dispStartDTim.substring(0,4) + "/" + dispStartDTim.substring(4,6) + "/" + dispStartDTim.substring(6,8);
    }

    /** -----------------------------------------------*/
    /**
     * @return
     */
    public String getInfoId() {
        return infoId;
    }

    /**
     * @return
     */
    public String getDisplayDiv() {
        return displayDiv;
    }

    /**
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return
     */
    public String getDispStartDTim() {
        return dispStartDTim;
    }

    /**
     * @return
     */
    public String getDispEndDTim() {
        return dispEndDTim;
    }

    /**
     * @return
     */
    public String getText() {
        return text;
    }

    /**
     * @param string
     */
    public void setInfoId(String string) {
        infoId = string;
    }

    /**
     * @param string
     */
    public void setDisplayDiv(String string) {
        displayDiv = string;
    }

    /**
     * @param string
     */
    public void setTitle(String string) {
        title = string;
    }

    /**
     * @param string
     */
    public void setDispStartDTim(String string) {
        dispStartDTim = string;
    }

    /**
     * @param string
     */
    public void setDispEndDTim(String string) {
        dispEndDTim = string;
    }

    /**
     * @param string
     */
    public void setText(String string) {
        text = string;
    }


}

package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.beans.NewestExamDivSearchBean;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

/**
 *
 * 受験予定大学検索（個人ID指定）
 * 
 * <2010年度改修>
 * 2010.01.19	Tomohisa YAMADA - TOTEC
 * 				新規作成
 *
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class PlanUnivSearchIndBean extends PlanUnivSearchBean {
		
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		univs = setupList(search());
	}
	
	/**
	 * 検索
	 * @return
	 * @throws Exception
	 */
	private List search() throws Exception {
		
		//最新の模試年度と模試区分を設定
		NewestExamDivSearchBean news = new NewestExamDivSearchBean();
		news.setConnection(null, conn);
		news.execute();

		PreparedStatement ps1 = null;
		
		try {
			
			Query query = QueryLoader.getInstance().load("i06");
			ps1 = conn.prepareStatement(query.toString());
			ps1.setString(1, individualCd);
			ps1.setString(2, "1");
			ps1.setString(3, "1");
			ps1.setString(4, "01");
			
			List result = searchPlans(ps1, news.getExamYear(), news.getExamDiv());
	
			//初回でない場合は受験予定大学データは取得せずに終了
			if (!isFirst) {
				return result;
			}
	
			//受験予定大学情報検索
			//�@の場合のときのみ実行が必要
			//�@の結果データとの結果比較を行って日程重複などをチェックする
			result.addAll(
					searchPlannedUnivs(
							news.getExamYear(), 
							news.getExamDiv(), 
							individualCd, 
							null, 
							null, 
							null, 
							null));
			
			return result;
			
		} finally {
			DbUtils.closeQuietly(ps1);
		}
	}
}

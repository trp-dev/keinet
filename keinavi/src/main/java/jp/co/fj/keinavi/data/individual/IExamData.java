package jp.co.fj.keinavi.data.individual;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.util.KNUtil;

/**
*
* 個人成績分析用試験データ
* 
* 2006/08/31    [新規作成]
*
* @author Tomohisa YAMADA - TOTEC
* @version 1.0
* 
* 2009.11.24   Fujito URAKAWA - Totec
*              「学力レベル」追加対応 
* 
*/
public class IExamData extends ExaminationData {
    
    /**
     * ExamData型データ
     */
    private ExamData examData = null;
    
    /**
     * コンストラクタ
     */
    public IExamData() {
        super();
    }
    
    /**
     * コンストラクタ２
     * @param year 年度
     * @param cd コード
     */
    public IExamData(final String year, final String cd) {
        setExamYear(year);
        setExamCd(cd);
    }
    
    /**
     * コンストラクタ３
     * @param year
     * @param cd
     * @param typecd
     * @param name
     * @param nameAbbr
     * @param outOpenDate
     * @param inpleDate
     * @param sequence
     */
    public IExamData(
            final String year, 
            final String cd, 
            final String name, 
            final String nameAbbr, 
            final String typecd, 
            final String st,
            final String div,
            final String grade,
            final String inpleDate,
            final String inOpenDate,
            final String outOpenDate, 
            final String dockcd,
            final String docktype,
            final String sequence,
            final String master) {
        
        super.setExamYear(year);
        super.setExamCd(cd);
        super.setExamName(name);
        super.setExamNameAbbr(nameAbbr);
        super.setExamTypeCd(typecd);
        super.setExamSt(st);
        super.setExamDiv(div);
        super.setTergetGrade(grade);
        super.setInpleDate(inpleDate);
        super.setInDataOpenDate(inOpenDate);
        super.setOutDataOpenData(outOpenDate);
        super.setDockingExamCd(dockcd);
        super.setDockingExamCd(docktype);
        super.setDispSequence(sequence);
        super.setMasterDiv(master);
    }
    
    /**
     * @return IExamData(ExaminationData)をExamData型に変換して戻す
     */
    public ExamData toExamData() {
        
        if (examData == null) {
            examData = new ExamData();
            examData.setExamYear(getExamYear());
            examData.setExamCD(getExamCd());
            examData.setExamTypeCD(getExamTypeCd());
            examData.setExamName(getExamName());
            examData.setExamNameAbbr(getExamNameAbbr());
            examData.setOutDataOpenDate(getOutDataOpenData());
            examData.setInpleDate(getInpleDate());
            examData.setTargetGrade(getTergetGrade());
            examData.setDispSequence(new Integer(getDispSequence()).intValue());
        }
        
        return examData;
    }
    
	/**
	 * 空のExaminationDataを返す。NullPointerが起こらないように
	 * すべてを非Nullにする。
	 * @return
	 */
    public static IExamData getEmptyIExamData() {
    	IExamData data = new IExamData();
		data.setExamYear(new String());
		data.setExamCd(new String());
		data.setExamName(new String());
		data.setExamAddrName(new String());
		data.setExamTypeCd(new String());
		data.setExamSt(new String());
		data.setExamDiv(new String());
		data.setDispSequence(new String());
		data.setMasterDiv(new String());
		data.setSubRecordDatas(new ArrayList());
		data.setCourseDatas(new ArrayList());
		data.setCandidateRatingDatas(new ArrayList());
		return data;
    }

	public SubRecordIData getSubRecordIData(String subCd) {
		
		SubRecordIData rtndata = null;
		for(Iterator it=getSubRecordDatas().iterator(); it.hasNext();){
			SubRecordIData data = (SubRecordIData)it.next();
			if(data.getSubCd() != null) {
				if(data.getSubCd().equals(subCd)){
					rtndata = data;
				}
			}
		}
		if(rtndata == null)
			rtndata = SubRecordIData.getSubRecordIDataEmptyData();
		return rtndata;
	}
    
	/**
	 * 偏差値から学力レベルを算出する
	 * @param dev
	 * @return 学力レベル S,A〜F
	 */
	public static String makeScholarLevel(String dev) {
		
		float iDev = 0.0F;
		
		try {
			if (dev == null || "".equals(dev) || "-999.0".equals(dev) || "-999".equals(dev)) return "";
			
			iDev = Float.parseFloat(dev);
		}
		catch (NumberFormatException e){
			return "";
		}
		
		if (iDev >= 65.0F) return "S";
		else if (60.0F <= iDev && iDev <= 64.9F) return "A";
		else if (55.0F <= iDev && iDev <= 59.9F) return "B";
		else if (50.0F <= iDev && iDev <= 54.9F) return "C";
		else if (45.0F <= iDev && iDev <= 49.9F) return "D";
		else if (40.0F <= iDev && iDev <= 44.9F) return "E";
		else return "F";
	}
	
	/**
	 * 対象の科目コードのインスタンスを返す
	 * @return 偏差値
	 */
	public SubRecordIData getInsTargetSubEng() {
		SubRecordIData dev = getSubRecordIData(getTargetEngSubCd());
		if(dev == null) {return new SubRecordIData();}
		return dev;
	}
	public SubRecordIData getInsTargetSubMath() {
		SubRecordIData dev = getSubRecordIData(getTargetMathSubCd());
		if(dev == null) {return new SubRecordIData();}
		return dev;
	}
	public SubRecordIData getInsTargetSubJap() {
		SubRecordIData dev = getSubRecordIData(getTargetJapSubCd());
		if(dev == null) {return new SubRecordIData();}
		return dev;
	}
	public SubRecordIData getInsTargetSubSci() {
		SubRecordIData dev = getSubRecordIData(getTargetSciSubCd());
		if(dev == null) {return new SubRecordIData();}
		return dev;
	}
	public SubRecordIData getInsTargetSubSoc() {
		SubRecordIData dev = getSubRecordIData(getTargetSocSubCd());
		if(dev == null) {return new SubRecordIData();}
		return dev;
	}
	
    /**
	 * 偏差値の学力レベルを取得する（総合／平均）
	 * @return
	 */
    public String getLevelAvgAll() {
    	String dev = super.getAvgAll();
    	return makeScholarLevel(dev);
    }
    
    /**
	 * 偏差値の学力レベルを取得する（総合／最大）
	 * @return
	 */
    public String getLevelMaxAll() {

    	SubRecordIData data = getMaxSubData(super.getAllCourse());
    	
    	if (data == null) return "";
    	
    	return data.getScholarlevel();
    }
    	
	/**
	 * 偏差値の学力レベルを取得する（英語／平均）
	 * @return
	 */
    public String getLevelAvgEng() {
    	String dev = super.getAvgEng();
    	return makeScholarLevel(dev);
    }
    
    /**
	 * 偏差値の学力レベルを取得する（英語／最大）
	 * @return
	 */
    public String getLevelMaxEng() {
    	
    	SubRecordIData data = getMaxSubData(super.getEngCourse());
    	if (data == null) return "";
    	
    	return data.getScholarlevel();
    }
    
    /**
	 * 偏差値の学力レベルを取得する（英語／指定科目）
	 * @return
	 */
    public String getLevelTargetEng() {
    	SubRecordIData data = getInsTargetSubEng();
    	return data.getScholarlevel();
    }
    
    /**
	 * 偏差値の学力レベルを取得する（数学／平均）
	 * @return
	 */
    public String getLevelAvgMath() {
    	String dev = super.getAvgMath();
    	return makeScholarLevel(dev);
    }
    
    /**
	 * 偏差値の学力レベルを取得する（数学／最大）
	 * @return
	 */
    public String getLevelMaxMath() {
    	
    	SubRecordIData data = getMaxSubData(super.getMatCourse());
    	if (data == null) return "";
    	
    	return data.getScholarlevel();
    }
    
    /**
	 * 偏差値の学力レベルを取得する（数学／指定科目）
	 * @return
	 */
    public String getLevelTargetMath() {
    	SubRecordIData data = getInsTargetSubMath();
    	return data.getScholarlevel();
    }
    
    /**
	 * 偏差値の学力レベルを取得する（国語／平均）
	 * @return
	 */
    public String getLevelAvgJap() {
    	String dev = super.getAvgJap();
    	return makeScholarLevel(dev);
    }
    
    /**
	 * 偏差値の学力レベルを取得する（国語／最大）
	 * @return
	 */
    public String getLevelMaxJap() {
    	
    	SubRecordIData data = getMaxSubData(super.getJapCourse());
    	if (data == null) return "";
    	
    	return data.getScholarlevel();
    }
    
    /**
	 * 偏差値の学力レベルを取得する（国語／指定科目）
	 * @return
	 */
    public String getLevelTargetJap() {
    	SubRecordIData data = getInsTargetSubJap();
    	return data.getScholarlevel();
    }
    
    /**
	 * 偏差値の学力レベルを取得する（理科／平均）
	 * @return
	 */
    public String getLevelAvgSci() {
    	String dev = super.getAvgSci();
    	return makeScholarLevel(dev);
    }
    
    /**
	 * 偏差値の学力レベルを取得する（理科／最大）
	 * @return
	 */
    public String getLevelMaxSci() {
    	
    	SubRecordIData data = getMaxSubData(super.getSciCourse());
    	if (data == null) return "";
    	
    	return data.getScholarlevel();
    }
    
    /**
	 * 偏差値の学力レベルを取得する（理科／指定科目）
	 * @return
	 */
    public String getLevelTargetSci() {
    	SubRecordIData data = getInsTargetSubSci();
    	return data.getScholarlevel();
    }

    /**
	 * 偏差値の学力レベルを取得する（地歴公民／平均）
	 * @return
	 */
    public String getLevelAvgSoc() {
    	String dev = super.getAvgSoc();
    	return makeScholarLevel(dev);
    }
    
    /**
	 * 偏差値の学力レベルを取得する（地歴公民／最大）
	 * @return
	 */
    public String getLevelMaxSoc() {
    	
    	SubRecordIData data = getMaxSubData(super.getSocCourse());
    	if (data == null) return "";
    	
    	return data.getScholarlevel();
    }
    
    /**
	 * 偏差値の学力レベルを取得する（地歴公民／指定科目）
	 * @return
	 */
    public String getLevelTargetSoc() {
    	SubRecordIData data = getInsTargetSubSoc();
    	return data.getScholarlevel();
    }
    
    /**
     * 偏差値が最大の科目データを取得する
     * @param col
     * @return 偏差値が最大の科目データ（SubRecordIData）
     */
    public SubRecordIData getMaxSubData(Collection col){
    	
    	SubRecordIData retdata = null;
    	
    	double maxValue = 0.0d;
		for (Iterator it = col.iterator(); it.hasNext(); ) {
			
			SubRecordIData data = (SubRecordIData)it.next();
			
			double thisValue = Double.valueOf(data.getCDeviation()).doubleValue();
			if(thisValue > maxValue) {
				retdata = data;
				maxValue = thisValue;
			}
		}
    	
    	return retdata;
    }

	/**
	 * 理科の第1解答科目の学力レベルを返す
	 * 		第1解答科目模試で無い場合は学力レベル（平均）を返す
	 * 		第1解答科目模試でも第1解答科目が無い場合は学力レベル（平均）を返す
	 * @return 偏差値
	 */
	public String getLevelAns1stSci(){
		// 第1解答科目模試
		if (KNUtil.isAns1st(getExamCd())) {
			String subcd = super.getAns1stSubjectCodeSci();
			if (subcd != null) {
				// 第1解答科目の学習レベル
				SubRecordIData data = getSubRecordIData(subcd);
				return data.getScholarlevel();
			} else {
				// 学力レベル（平均）
				return getLevelAvgSci();
			}
		} else {
			// 学力レベル（平均）
			return getLevelAvgSci();
		}
	}

	/**
	 * 地歴公民の第1解答科目の学力レベルを返す
	 * 		第1解答科目模試で無い場合は学力レベル（平均）を返す
	 * 		第1解答科目模試でも第1解答科目が無い場合は学力レベル（平均）を返す
	 * @return 偏差値
	 */
	public String getLevelAns1stSoc(){
		// 第1解答科目模試
		if (KNUtil.isAns1st(getExamCd())) {
			String subcd = super.getAns1stSubjectCodeSoc();
			if (subcd != null) {
				// 第1解答科目の学力レベル
				SubRecordIData data = getSubRecordIData(subcd);
				return data.getScholarlevel();
			} else {
				// 学力レベル（平均）
				return getLevelAvgSoc();
			}
		}
		else {
			// 学力レベル（平均）
			return getLevelAvgSoc();
		}
	}
}

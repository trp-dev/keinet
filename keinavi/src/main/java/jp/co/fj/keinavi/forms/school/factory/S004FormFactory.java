/*
 * 作成日: 2004/07/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.school.factory;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.forms.school.SMaxForm;
import jp.co.fj.keinavi.util.KNUtil;

import com.fjh.forms.ActionForm;

/**
 * @author 奥村ゆかり
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 * 校内成績：偏差値分布：詳細
 *
 * 2009.10.20 Fujito URAKAWA - Totec
 *            新校内資料「科目・設問別正答率（レベル別）」対応
 *
 */
public class S004FormFactory extends AbstractSFormFactory {
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 * 設問別成績
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		// アクションフォーム
		SMaxForm form = super.createSMaxForm(request);
		// アイテムマップ
		Map item = getItemMap(request);

		CommonFormFactory commForm = new CommonFormFactory(form, item);

		commForm.setForm0203();
		commForm.setForm0601();
		commForm.setForm0701();
		// 2019/07/25 QQ)Tanioka 共通テスト対応 ADD START
		// 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
		//commForm.setForm1701();
		//commForm.setForm1702();
		// 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END
		commForm.setForm1703();
		// 2019/07/25 QQ)Tanioka 共通テスト対応 ADD END
		commForm.setForm1504();
		commForm.setForm1516();
		commForm.setForm1305();
		commForm.setFormat();
		commForm.setOption();

		return commForm.getForm();
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
		//		SMaxForm form = (SMaxForm)request.getSession().getAttribute(SMaxForm.SESSION_KEY);
		SMaxForm form = (SMaxForm) request.getAttribute("form");
		// アイテムマップ
		Map item = getItemMap(request);

		CommonItemFactory commItem = new CommonItemFactory(form, item);
		commItem.setItem0203();
		commItem.setItem0601();
		commItem.setItem0701();
		commItem.setItem1305();

		// マーク模試正答状況・マーク模試高校別設問成績層正答率を
		// フォームにセットするはマーク系の模試の場合だけ保存する。
		ExamData exam = super.getExamSession(request).getExamData(
			super.getTargetYear(request),
			super.getTargetExam(request)
		);

		if ("1".equals(exam.getDockingType())) {
			commItem.setItem1504();
			commItem.setItem1516();
			// 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
			// 2019/07/25 QQ)Tanioka 共通テスト対応 ADD START
			//commItem.setItem1701();
			//commItem.setItem1702();
			// 2019/07/25 QQ)Tanioka 共通テスト対応 ADD END
			// 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END
		}
		// 2020/03/03 QQ)Ooseto 共通テスト対応(不具合修正) UPD START
		//// 2019/09/04 QQ)Tanioka 記述式問題詳細分析 追加 ADD START
		//else if("2".equals(exam.getDockingType())){
		//	commItem.setItem1703();
		//}
		//// 2019/09/04 QQ)Tanioka 記述式問題詳細分析 追加 ADD END
		else if(Boolean.valueOf(KNUtil.isWrtnExam2(exam)) || Boolean.valueOf(KNUtil.isPStage(exam))){
                    commItem.setItem1703();
                }
		// 2020/03/03 QQ)Ooseto 共通テスト対応(不具合修正) UPD START
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap("020203");
	}

}

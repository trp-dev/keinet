/*
 * 作成日: 2004/09/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.helpdesk;

import com.fjh.forms.ActionForm;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class HD103Form extends ActionForm {

	private String fileTitle = null;
	private byte[] fileName;

	// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START
	private String   content     = "0";   // 処理の内容
	// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
		//

	}


	/**
	 * @return
	 */
	public String getFileTitle() {
		return fileTitle;
	}

	/**
	 * @param string
	 */
	public void setFileTitle(String string) {
		fileTitle = string;
	}

	/**
	 * @return
	 */
	public byte[] getFileName() {
		return fileName;
	}

	/**
	 * @param bs
	 */
	public void setFileName(byte[] bs) {
		fileName = bs;
	}

	// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START
	/**
	 * @return
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param string
	 */
	public void setContent(String string) {
		content = string;
	}
	// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END

}

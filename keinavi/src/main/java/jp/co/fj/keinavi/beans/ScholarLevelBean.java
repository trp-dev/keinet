package jp.co.fj.keinavi.beans;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fjh.beans.DefaultBean;

/**
*
* 学力レベルBean
* 
* 2009.10.27	[新規作成]
* 
* @author Fujito URAKAWA - TOTEC
* @version 1.0
* 
*/
public class ScholarLevelBean extends DefaultBean {

	// 学力レベル
	private Map levelMap;
	
	// 学力レベル文字列配列
	private Collection scholarLevelList;
	
	// singleton
	private static ScholarLevelBean bean;
	
	/**
	 * コンストラクタ
	 */
	private ScholarLevelBean() {
	}
	
	/**
	 * 学力レベルbeanのインスタンスを取得する
	 * @param con
	 * @exception SQLException
	 */
	public static ScholarLevelBean getInstance(Connection con) throws SQLException{
		//NULLなら初期化
		if(bean == null) ScholarLevelBean.init(con);
		
		return bean;
	}
	

	/**
	 * 変数の初期化処理を行う
	 * @param con
	 * @exception SQLException
	 */
	private static synchronized void init(Connection con) throws SQLException {
		
		// dummy...
		if (con != null) {
			con.getClass();
		}
		
		if (bean != null) {
			return;
		}
		
		bean = new ScholarLevelBean();
		
		Map b = new LinkedHashMap();
		// 学力レベル「S」
		b.put("S", new String[]{"65.0",""});
		// 学力レベル「A」
		b.put("A", new String[]{"60.0","64.9"});
		// 学力レベル「B」
		b.put("B", new String[]{"55.0","59.9"});
		// 学力レベル「C」
		b.put("C", new String[]{"50.0","54.9"});
		// 学力レベル「D」
		b.put("D", new String[]{"45.0","49.9"});
		// 学力レベル「E」
		b.put("E", new String[]{"40.0","44.9"});
		// 学力レベル「F」
		b.put("F", new String[]{"","39.9"});
		
		bean.setLevelMap(b);
		
		List container = new ArrayList();
		Iterator ite = b.keySet().iterator();
		while (ite.hasNext()) {
			String key = (String) ite.next();
			container.add(key);
		}
		
		bean.setScholarLevelList(container);
		
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
	}

	public Map getLevelMap() {
		return levelMap;
	}

	public Collection getScholarLevelList() {
		return scholarLevelList;
	}

	public void setLevelMap(Map levelMap) {
		this.levelMap = levelMap;
	}

	public void setScholarLevelList(Collection list) {
		this.scholarLevelList = list;
	}




}

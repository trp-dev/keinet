/*
 * 作成日: 2004/10/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.admin;

import java.io.Serializable;

/**
 * 複合クラス管理用学年データ
 * 
 * @author kawai
 */
public class MGradeData implements Serializable {

	private String year; // 年度
	private String grade; // 学年

	/**
	 * コンストラクタ
	 */
	public MGradeData() {
	}

	/**
	 * コンストラクタ
	 */
	public MGradeData(String year, String grade, String className) {
		this.year = year;
		this.grade = grade;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		MGradeData data = (MGradeData) obj;

		return this.year.equals(data.year)
			&& this.grade.equals(data.grade);
	}

	/**
	 * @return
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * @return
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param string
	 */
	public void setGrade(String string) {
		grade = string;
	}

	/**
	 * @param string
	 */
	public void setYear(String string) {
		year = string;
	}

}

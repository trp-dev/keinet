/*
 * 作成日: 2004/08/10
 *
 */
package jp.co.fj.keinavi.forms.individual;

import com.fjh.forms.ActionForm;

/**
 * @author Totec) T.Yamada
 *
 * 2005.7.4 	T.Yamada 	[1]判定結果が存在しなければ公表科目も表示できないようにする
 */
public class I206Form extends ActionForm{

	/**
	 * 大学コード8桁
	 */
	private String uniqueKey;
	
	/**
	 * [1] add
	 * 複数ある判定リストのどちらを選ぶか
	 */
	private String judgeNo;

	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.individual.ICommonForm#validate()
	 */
	public void validate() {
		// TODO 自動生成されたメソッド・スタブ
		
	}

	/**
	 * @return
	 */
	public String getUniqueKey() {
		return uniqueKey;
	}

	/**
	 * @param string
	 */
	public void setUniqueKey(String string) {
		uniqueKey = string;
	}

	/**
	 * @return
	 */
	public String getJudgeNo() {
		return judgeNo;
	}

	/**
	 * @param string
	 */
	public void setJudgeNo(String string) {
		judgeNo = string;
	}

}
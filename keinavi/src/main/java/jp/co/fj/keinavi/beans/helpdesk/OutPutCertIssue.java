/*
 * 作成日: 2015/10/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.helpdesk;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import jp.co.fj.keinavi.beans.maintenance.MainteLogBean;
import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.forms.helpdesk.HD101Form;
import jp.co.fj.keinavi.util.KNUtil;

/**
 * ユーザ管理（証明書発行用・ユーザ登録用TSV出力処理）
 *
 * @author Hiroyuki Nishiyama - QuiQsoft
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class OutPutCertIssue extends AbstractSheetBean {

	// ファイル分割単位
	private int divNum = 100;

	// ヘッダー情報
	private List header = null;
	// 出力テキスト種別
	private int outType = 0;
	// 出力対象項目
	private Integer target[] = null;

	// 出力ファイル
	private File outPutFile = null;
	// コネクション
	private Connection con;

	// ヘルプデスク_一覧画面
	private HD101Form hd101Form = null;

	// 日時フォーマッタ
	private static final SimpleDateFormat format = new SimpleDateFormat("yyyyMMddmm");

	// ログ出力モジュール
	private final MainteLogBean mainteLog = new MainteLogBean("HELP");

	// 有効期間終了日
	private static final String END_DATE = "0501";

	// 有効期間
	private static final String YUKO_KIKAN = "24";

	// コンストラクタ
	public OutPutCertIssue(){

	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return true;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return "";
	}

	public void execute()throws IOException, SQLException, Exception{

		int fileCnt = 0;

		// 契約校マスタの取得結果
		ArrayList pactSchoolList = null;
		// 日付取得用
		Date dt = new Date();
		Calendar cal = null;

		// コネクション設定
		con = super.conn;
		mainteLog.setConnection(null, conn);

		try {

			// ヘルプデスク_一覧画面の入力情報を取得
			hd101Form = (HD101Form) session.getAttribute("hd101Form");
			String content = hd101Form.getContent();
			// 処理対象データを取得
			ArrayList userIdList = getTargetUser();

			// **************************************
			// SQL文を作成
			// **************************************
			// パラメータマスタSELECT
			StringBuffer conPrmSelSb = new StringBuffer();
			conPrmSelSb.append(" SELECT T1.SETTINGVALUE1");
			conPrmSelSb.append("   FROM CONTRACT_PARAM T1");
			conPrmSelSb.append("  WHERE 1=1");
			conPrmSelSb.append("    AND T1.PARAJUDGECD = ?");
			conPrmSelSb.append("    AND T1.PARAKEY     = ?");
			conPrmSelSb.append(" ORDER BY");
			conPrmSelSb.append("        T1.PARAKEY");

			// 契約校マスタSELECT
			StringBuffer schoolSelSb = new StringBuffer();
			schoolSelSb.append(" SELECT  NVL(USERID,     ' ')");
			schoolSelSb.append("      ,  NVL(DEFAULTPWD, ' ')");
			schoolSelSb.append("   FROM  PACTSCHOOL");
			schoolSelSb.append("  WHERE  USERID = ?");

			// 契約校マスタUPDATE
			StringBuffer schoolUpdSb = new StringBuffer();
			schoolUpdSb.append("UPDATE PACTSCHOOL");
			schoolUpdSb.append("   SET CERT_CN           = ?");                // 1:証明書情報CN
			schoolUpdSb.append("     , CERT_OU           = ?");                // 2:証明書情報OU
			schoolUpdSb.append("     , CERT_O            = ?");                // 3:証明書情報O
			schoolUpdSb.append("     , CERT_L            = ?");                // 4:証明書情報L
			schoolUpdSb.append("     , CERT_S            = ?");                // 5:証明書情報S
			schoolUpdSb.append("     , CERT_C            = ?");                // 6:証明書情報C
			schoolUpdSb.append("     , CERT_FLG          = '1'");              // X:証明書有効フラグ
			schoolUpdSb.append("     , ISSUED_BEFOREDATE = CERT_BEFOREDATA");  // X:発行済証明書有効期限
			schoolUpdSb.append("     , LOGINPWD          = DEFAULTPWD");       // X:ログインパスワード
			schoolUpdSb.append(" WHERE 1=1");
			schoolUpdSb.append("   AND USERID            = ?");                // 7:ユーザID

			// パラメータマスタを取得
			String contractId   = selectContractParam(conPrmSelSb, "00001", "00001");
			String div          = selectContractParam(conPrmSelSb, "00002", "00001");
			String userIdPreFix = selectContractParam(conPrmSelSb, "00003", "00001");
			String passWord     = selectContractParam(conPrmSelSb, "00004", "00001");

			// 分割単位を設定
			try {
				divNum = Integer.parseInt(div);
			} catch (NumberFormatException nEx) {
				divNum = 100;
			}

			// **************************************
			// ファイル出力用オブジェクト作成
			// **************************************
			// 証明書発行用
			ArrayList     certList     = new ArrayList();
			//LinkedHashMap certData    = new LinkedHashMap();
			HashMap       certDataInit = new HashMap();
			HashMap       certData     = null;
			OutputTsvBean certOutBean  = new OutputTsvBean();

			// ユーザ登録用
			ArrayList     registList     = new ArrayList();
			//LinkedHashMap registData    = new LinkedHashMap();
			HashMap       registDataInit = new HashMap();
			HashMap       registData     = null;
			OutputTsvBean registOutBean  = new OutputTsvBean();

			// **************************************
			// ファイル出力用パラメータ作成
			// **************************************
			OutPutParamSet certPrm   = new OutPutParamSet(login, sessionKey, "hd101", "0");
			OutPutParamSet registPrm = new OutPutParamSet(login, sessionKey, "hd101", "1");

			// **************************************
			// パラメータ設定
			// **************************************
			// 証明書発行用
			certPrm.setDivNum(divNum);                         // ファイル分割単位設定
			certOutBean.setOutType(certPrm.getOutType());      // 出力形式
			certOutBean.setOutTarget(certPrm.getTarget());     // 出力項目
			certOutBean.setHeadTextList(header);               // ヘッダー設定
			certOutBean.setDivNum(divNum);                     // ファイル分割単位設定

			// ユーザ登録用
			registPrm.setDivNum(divNum);                       // ファイル分割単位設定
			registOutBean.setOutType(registPrm.getOutType());  // 出力形式
			registOutBean.setOutTarget(registPrm.getTarget()); // 出力項目
			registOutBean.setHeadTextList(header);             // ヘッダー設定
			registOutBean.setDivNum(divNum);                   // ファイル分割単位設定


			// 処理対象データを出力形式に変換
			// 初期化
			for (int colNum = 1; colNum <= certOutBean.getOutTarget().length; colNum++) {
				certDataInit.put(new Integer(colNum), "");
			}
			// 証明書タイプID
			certDataInit.put(new Integer(3), Certificates.getInstance().getTypeId());
			// 鍵生成アルゴリズム
			certDataInit.put(new Integer(9), Certificates.getInstance().getAlgoRithm());
			// Key長
			certDataInit.put(new Integer(10), Certificates.getInstance().getKeyLength());

			// 証明書有効期間終了日
			// 随時
			if ( "0".equals(content) ) {
				// 日付の取得
				cal = Calendar.getInstance();
				cal.setTime(dt);

				// １月 〜 ４月は今年
				if (cal.get(Calendar.MONTH) + 1 < 5) {
					certDataInit.put(new Integer(11), cal.get(Calendar.YEAR)  + END_DATE);
				} else {
					certDataInit.put(new Integer(11), (cal.get(Calendar.YEAR)  + 1) + END_DATE);
				}

			// 年次
			} else {
				certDataInit.put(new Integer(11), getNendo(2, true) + END_DATE);
			}

			// 証明書有効期間
			certDataInit.put(new Integer(12), YUKO_KIKAN);

			for (int colNum = 1; colNum <= registOutBean.getOutTarget().length; colNum++) {
				registDataInit.put(new Integer(colNum), "");
			}
			// 初期パスワード
			registDataInit.put(new Integer(8), passWord);
			// メールアドレス
			registDataInit.put(new Integer(17), Certificates.getInstance().getEmail());
			// ユーザタイプ
			registDataInit.put(new Integer(18), "0");
			// 契約ID
			registDataInit.put(new Integer(21), contractId);
			// 証明書の更新方法
			registDataInit.put(new Integer(22), "1");

			// 処理対象でループ
			for (int i = 0; i < userIdList.size(); i++) {

				// 契約校マスタSELECT
				pactSchoolList = selectPactSchool(schoolSelSb, userIdList.get(i).toString());

				// *-*-*-*-*-*-*-*-*-*-*
				// 証明書発行用
				// *-*-*-*-*-*-*-*-*-*-*
				certData = new HashMap(certDataInit);
				// ユーザID
				//certData.put(new Integer(1), pactSchoolList.get(0).toString());
				certData.put(new Integer(1), userIdPreFix + pactSchoolList.get(0).toString());

				// 随時
				if ( "0".equals(content) ) {
					// CN --> ユーザID + yyMMddmm
					certData.put(new Integer(2), pactSchoolList.get(0).toString() + format.format(dt).substring(2));
				// 年次
				} else {
					// CN --> ユーザID + 翌年度 + 0501
					certData.put(new Integer(2), pactSchoolList.get(0).toString() + getNendo(1, false) + "0501");
				}

				certList.add(certData);

				// *-*-*-*-*-*-*-*-*-*-*
				// ユーザ登録用
				// *-*-*-*-*-*-*-*-*-*-*
				registData = new HashMap(registDataInit);
				// ユーザID
				//registData.put(new Integer(7), pactSchoolList.get(0).toString());
				registData.put(new Integer(7), userIdPreFix + pactSchoolList.get(0).toString());
				// 初期パスワード
				//registData.put(new Integer(8), pactSchoolList.get(1).toString());

				registList.add(registData);

				// 随時を選択
				if ( "0".equals(content) ) {
					// 契約校マスタ更新
					//UpdatePactSchool(schoolUpdSb, pactSchoolList.get(0).toString(), certData.get(Integer.valueOf(2)).toString() );
					UpdatePactSchool(schoolUpdSb, pactSchoolList.get(0).toString(), certData.get(Integer.valueOf("2")).toString() );
				}

				// ファイル分割
				// 分割単位に達した or 最終レコード
				if ( (i + 1) % divNum  == 0 || i >= userIdList.size() - 1) {
					fileCnt++;

					// 証明書発行用
					doBeanExcute(certOutBean, certPrm, certList, fileCnt, dt);

					// ユーザ登録用
					doBeanExcute(registOutBean, registPrm, registList, fileCnt, dt);

					// タイムスタンプを変更
					dt = new Date();
				}

			}

			// ログ出力
			if ("0".equals(content)) {
				mainteLog.etcLog("証明書発行：契約校マスタを" + userIdList.size() + "件更新");
				mainteLog.etcLog("証明書発行：証明書発行用ファイルを" + userIdList.size() + "件出力");
			} else {
				mainteLog.etcLog("証明書発行【年次】：証明書発行用ファイルを" + userIdList.size() + "件出力");
			}

		} catch (Exception e) {
			con.rollback();
			throw e;
		} finally {
			con.commit();
		}

	}

	/**
	 *
	 * 処理対象ユーザを取得する
	 *
	 * @return 取得結果
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private ArrayList getTargetUser() throws SQLException {

		// 返却値
		ArrayList userIdList = null;
		String[] selUser = null;	// 選択されたユーザID（表示項目）

		// 画面指定
		if ("0".equals(hd101Form.getTsvUser())) {
			selUser = hd101Form.getSelectUser().split(",");
			userIdList = new ArrayList();

			// 画面で選択したユーザIDを取得する
			// 明細の３カラム目のみセット
			for (int i = 2; i < selUser.length; i += 3 ) {
				userIdList.add(selUser[i]);
			}

		// ファイル指定
		} else {
			// テンポラリからユーザを取得
			userIdList = this.selectTemp();
		}

		return userIdList;
	}

	/**
	 *
	 * CSVファイルのアップロード結果から処理対象のユーザIDを取得する
	 *
	 * @return String[]
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private ArrayList selectTemp() throws SQLException {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		ArrayList userIdList = new ArrayList();

		try {
			sb.append(" SELECT  ");
			sb.append("        LINENO");
			sb.append("      , USERID");
			sb.append("   FROM CERT_TARGET");
			sb.append(" ORDER BY");
			sb.append("        LINENO");

			stmt = con.prepareStatement(sb.toString());

			rs = stmt.executeQuery();
			while (rs.next()) {
				userIdList.add(rs.getString(2));
			}
		} catch (SQLException ex) {
			throw new SQLException("CSVアップロード結果の取得に失敗しました。");
		} catch (Exception ex) {
			throw new SQLException("CSVアップロード結果の取得に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}

		return userIdList;
	}

	/**
	 *
	 * 契約校マスタからレコードを取得する
	 *
	 * @param sb      SQL文
	 * @param userId  ユーザID
	 * @return 取得結果
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private ArrayList selectPactSchool(StringBuffer sb, String UserId) throws SQLException {

		ArrayList pactSchoolList = new ArrayList();
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			stmt = con.prepareStatement(sb.toString());
			stmt.setString(1, UserId);

			rs = stmt.executeQuery();
			while (rs.next()) {
				pactSchoolList.add(rs.getString(1));
				pactSchoolList.add(rs.getString(2));
			}
		} catch (SQLException ex) {
			throw new SQLException("契約校マスタの取得に失敗しました。");
		} catch (Exception ex) {
			throw new SQLException("契約校マスタの取得に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return pactSchoolList;
	}

	/**
	 *
	 * 証明書発行マスタからパラメータを取得する
	 *
	 * @param sb           SQL文
	 * @param paraJudgeCd  パラメータ識別コード
	 * @param paraKey      パラメータキー
	 * @return 取得結果
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private String selectContractParam(StringBuffer sb, String paraJudgeCd, String paraKey)
		throws SQLException {

		String retVal = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			stmt = con.prepareStatement(sb.toString());
			stmt.setString(1, paraJudgeCd);
			stmt.setString(2, paraKey);

			rs = stmt.executeQuery();
			while (rs.next()) {
				retVal = rs.getString(1);
			}

			if (retVal == null) retVal = "";

		} catch (SQLException ex) {
			throw new SQLException("証明書発行マスタの取得に失敗しました。");
		} catch (Exception ex) {
			throw new SQLException("証明書発行マスタの取得に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return retVal;
	}

	/**
	 *
	 * 契約校マスタを更新する
	 *
	 * @param sb        SQL文
	 * @param UserId    ユーザID
	 * @param cn        CN
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private void UpdatePactSchool(StringBuffer sb, String UserId, String cn) throws SQLException {
		int count = 0;
		PreparedStatement stmt = null;

		try {
			stmt = con.prepareStatement(sb.toString());
			stmt.setString( 1, cn);
			stmt.setString( 2, Certificates.getInstance().getOu());
			stmt.setString( 3, Certificates.getInstance().getO());
			stmt.setString( 4, Certificates.getInstance().getL());
			stmt.setString( 5, Certificates.getInstance().getSt());
			stmt.setString( 6, Certificates.getInstance().getC());
			stmt.setString( 7, UserId);

			count = stmt.executeUpdate();

			if (count == 0)
				throw new SQLException("契約校マスタのUPDATEに失敗しました。");

		} catch (SQLException ex) {
			con.rollback();
			throw new SQLException("契約校マスタのUPDATEに失敗しました。");
		} catch (Exception ex) {
			con.rollback();
			throw new SQLException("契約校マスタのUPDATEに失敗しました。");
		} finally {
			if (stmt != null)
				stmt.close();
		}
	}


	/**
	 *
	 * ファイル出力処理を実行する
	 *
	 * @param outBaen   出力用Bean
	 * @param outPrm    出力用パラメータ
	 * @param dataList  出力データ
	 * @param fileCnt   ファイル連番
	 * @param dt        タイムスタンプ
	 * @throws Exception
	 */
	private void doBeanExcute(OutputTsvBean outBaen, OutPutParamSet outPrm, ArrayList dataList, int fileCnt, Date dt) throws Exception {

		// 処理対象データを出力用BEANにセット
		outBaen.setOutputTextList(dataList);
		// ファイルオブジェクト作成
		outPrm.setDate(dt);
		outPrm.createOutPutFile(fileCnt);
		outPutFile = outPrm.getOutPutFile();
		outBaen.setOutFile(outPutFile);
		// ファイル出力
		outBaen.execute();
		this.outfileList.add(outPutFile.getPath());
		sheetLog.add(outPutFile.getName());

	}

	// 取得開始年度
	private String getNendo(int calcVal, boolean isYearAll) {

		String nendo = String.valueOf( Integer.parseInt(KNUtil.getCurrentYear()) + (calcVal) );

		// 後ろ２ケタを返却
		if (!isYearAll) {
			nendo =  nendo.substring(nendo.length() - 2);
		}

		return nendo;
	}

}

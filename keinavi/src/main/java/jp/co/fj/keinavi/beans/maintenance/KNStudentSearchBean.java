package jp.co.fj.keinavi.beans.maintenance;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.data.maintenance.KNStudentData;
import jp.co.fj.keinavi.util.JpnStringConv;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 生徒情報検索Bean
 * 
 * 2005.11.24	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class KNStudentSearchBean extends DefaultBean {

	// 現年度
	private final int currentYear = Integer.parseInt(KNUtil.getCurrentYear());
	// 学校コード
	private final String schoolCd;
	// 対象年度
	private final String year;
	// 対象年度と現年度との差
	private final int yearDiff;
	// 学年
	private int grade = -1;
	// クラス
	private String className;
	// 個人ID
	private String individualId;
	
	// 生徒情報データのリスト
	private final List studentList = new ArrayList();
	
	/**
	 * @return 生徒情報データリストのサイズを戻す
	 */
	public int getSize() {
	    return studentList.size();
	}
	
	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 学校コード
	 * @param pYear 対象年度
	 */
	public KNStudentSearchBean(final String pSchoolCd, final String pYear) {
		
		if (pSchoolCd == null) {
			throw new IllegalArgumentException(
					"学校コードがNULLです。");
		}
		
		this.schoolCd = pSchoolCd;
		
		if (pYear == null || pYear.length() == 0) {
			this.year = null;
			this.yearDiff = 0;
		} else {
			this.year = pYear;
			this.yearDiff = currentYear - Integer.parseInt(pYear);
		}
		
		// 過去３年しかありえない
		if (yearDiff < 0 || yearDiff > 3) {
			throw new IllegalArgumentException(
					"不正な年度の指定です。");
		}
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		if (year == null) {
			return;
		}
		
		// 検索モード
		final int mode = judgeSearchMode(); 
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = createPreparedStatement(mode);
			rs = ps.executeQuery();
			while (rs.next()) {
				studentList.add(createKSStudentData(rs));
			}
			
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	/**
	 * 検索モードを判定する
	 * 
	 * @return
	 */
	protected int judgeSearchMode() {
		
		// 学年のみ
		if (grade >= 0 && className == null && individualId == null) {
			return 1;
		// 学年とクラス
		} else if (grade >= 0 && className != null && individualId == null) {
			return 2;
		// 個人ID
		} else if (grade < 0 && className == null && individualId != null) {
			return 3;
		// 不正
		} else {
			throw new IllegalArgumentException(
					"引数の指定が無効な組み合わせです。");
		}
	}
	
	/**
	 * SQLを生成する
	 * 
	 * @return SQL
	 * @throws IOException 
	 */
	protected String createQuery(final int mode) throws SQLException {
		
		final Query query = QueryLoader.getInstance().load("m11");

		// 学年
		if (isGradeMode(mode)) {
			query.append("AND " + getGradeColumn() + " = ? ");
		}

		// クラス
		if (isClassNameMode(mode)) {
			query.append("AND " + getClassColumn() + " = ? ");
		}
		
		// 個人ID
		if (isIndividualIdMode(mode)) {
			query.append("AND bi.individualid = ? ");
		}
		
		// ORDER BY
		query.append(createOrderBy());
		
		return query.toString();
	}

	// 学年の条件に使う列
	protected String getGradeColumn() {
		
		// 前年度
		if (yearDiff == 1) {
			return "h2.grade";
		// 前々年度
		} else if (yearDiff == 2) {
			return "h1.grade";
		// 今年度
		} else {
			return "h3.grade";
		}
	}
	
	// クラスの条件に使う列
	protected String getClassColumn() {
		
		// 前年度
		if (yearDiff == 1) {
			return "h2.class";
		// 前々年度
		} else if (yearDiff == 2) {
			return "h1.class";
		// 今年度
		} else {
			return "h3.class";
		}
	}
	
	// ORDER BY
	protected String createOrderBy() {
		
		final StringBuffer query = new StringBuffer();
		
		// 前年度
		if (yearDiff == 1) {
			query.append("ORDER BY key_grade2, key_class2, key_class_no2, ");
		// 前々年度
		} else if (yearDiff == 2) {
			query.append("ORDER BY key_grade1, key_class1, key_class_no1, ");
		// 今年度
		} else {
			query.append("ORDER BY key_grade3, key_class3, key_class_no3, ");
		}
		
		query.append("key_sex, key_name_kana");
		
		return query.toString();
	}
	
	/**
	 * PreparedStatementをセットアップする
	 * 
	 * @param mode 検索モード
	 * @return
	 */
	protected PreparedStatement createPreparedStatement(
			final int mode) throws Exception {
		
		final PreparedStatement ps = conn.prepareStatement(createQuery(mode));

		int index = 1;
		
		// 前々年度
		ps.setString(index++, String.valueOf(currentYear - 2));
		// 前年度
		ps.setString(index++, String.valueOf(currentYear - 1));
		// 今年度
		ps.setString(index++, String.valueOf(currentYear));
		// 学校コード
		ps.setString(index++, schoolCd);
		
		// 学年
		if (isGradeMode(mode)) {
			ps.setInt(index++, grade);
		}
		
		// クラス
		if (isClassNameMode(mode)) {
			ps.setString(index++, className);
		}
		
		// 個人ID
		if (isIndividualIdMode(mode)) {
			ps.setString(index++, individualId);
		}
		
		return ps;
	}
	
	// 学年を条件に利用するかどうか
	protected boolean isGradeMode(final int mode) {
		
		return mode == 1 || mode == 2;
	}

	// クラスを条件に利用するかどうか
	protected boolean isClassNameMode(final int mode) {
		
		return mode == 2;
	}
	
	// 個人IDを条件に利用するかどうか
	protected boolean isIndividualIdMode(final int mode) {
		
		return mode == 3;
	}
	
	/**
	 * ResultSetから生徒情報データを作る
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	protected KNStudentData createKSStudentData(
			final ResultSet rs) throws SQLException {

		final KNStudentData data = createKSStudentData(yearDiff);
		
		// 個人ID
		data.setIndividualId(rs.getString(1));
		// 性別
		data.setSex(rs.getString(2));
		// カタカナ氏名
		data.setNameKana(rs.getString(3));
		// ひらがな氏名
		data.setNameHiragana(
				JpnStringConv.kkanaHan2Hkana(rs.getString(3)));
		// 漢字氏名
		data.setNameKanji(rs.getString(4));
		// 誕生日
		data.setBirthday(rs.getDate(5));
		// 電話番号
		data.setTelNo(rs.getString(6));
		// 前々年度学年
		data.setBeforeLastGrade(rs.getInt(7));
		// 前々年度クラス
		data.setBeforeLastClassName(rs.getString(8));
		// 前々年度クラス番号
		data.setBeforeLastClassNo(rs.getString(9));
		// 前年度学年
		data.setLastGrade(rs.getInt(10));
		// 前年度クラス
		data.setLastClassName(rs.getString(11));
		// 前年度クラス番号
		data.setLastClassNo(rs.getString(12));
		// 学年
		data.setCurrentGrade(rs.getInt(13));
		// クラス
		data.setCurrentClassName(rs.getString(14));
		// クラス番号
		data.setCurrentClassNo(rs.getString(15));
		// 対象年度
		data.setYear(year);
		
		// 再集計処理用に変更前の値を保持する
		// 変更前の今年度学年
		data.setOCurrentGrade(data.getCurrentGrade());
		// 変更前の今年度クラス
		data.setOCurrentClassName(data.getCurrentClassName());
		// 変更前の前年度学年
		data.setOLastGrade(data.getLastGrade());
		// 変更前の前年度クラス
		data.setOLastClassName(data.getLastClassName());
		// 変更前の前々年度学年
		data.setOBeforeLastGrade(data.getBeforeLastGrade());
		// 変更前の前々年度クラス
		data.setOBeforeLastClassName(data.getBeforeLastClassName());
		
		return data;
	}
	
	/**
	 * [FactoryMethod]
	 * 
	 * @param pYearDiff
	 * @return KNStudentDataのインスタンス
	 * @throws SQLException
	 */
	protected KNStudentData createKSStudentData(
			int pYearDiff) throws SQLException {

		return new KNStudentData(pYearDiff);
	}
	
	/**
	 * @param pClassName 設定する className。
	 */
	public void setClassName(final String pClassName) {
		
		// 全クラスなら設定しない
		if ("all".equals(pClassName)) {
			return;
		}
		
		this.className = pClassName;
	}

	/**
	 * @param pGrade 設定する grade。
	 */
	public void setGrade(final String pGrade) {
		
		this.grade = "".equals(pGrade) ? 0 : Integer.parseInt(pGrade);
	}

	/**
	 * @param s 設定する individualId。
	 */
	public void setIndividualId(final String s) {
		this.individualId = s;
	}

	/**
	 * @return studentList を戻します。
	 */
	public List getStudentList() {
		return studentList;
	}

	/**
	 * @return className を戻します。
	 */
	protected String getClassName() {
		return className;
	}

	/**
	 * @return grade を戻します。
	 */
	protected int getGrade() {
		return grade;
	}

	/**
	 * @return individualId を戻します。
	 */
	protected String getIndividualId() {
		return individualId;
	}

	/**
	 * @return schoolCd を戻します。
	 */
	protected String getSchoolCd() {
		return schoolCd;
	}

	/**
	 * @return currentYear を戻します。
	 */
	protected int getCurrentYear() {
		return currentYear;
	}

}

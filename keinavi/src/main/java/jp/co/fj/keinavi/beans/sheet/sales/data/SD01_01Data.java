package jp.co.fj.keinavi.beans.sheet.sales.data;

import jp.co.fj.keinavi.beans.sheet.common.ISheetData;

/**
 *
 * パスワード通知書のデータクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SD01_01Data implements ISheetData {

	/** 郵便番号 */
	private String postCode;

	/** 住所 */
	private String address;

	/** 一括コード */
	private String bundleCd;

	/** 学校名 */
	private String bundleName;

	/** 担当先生名 */
	private String teacherName;

	/** 対象模試名 */
	private String examName;

	/** パスワード */
	private String password;

	// 2019/09/09 QQ)Tanioka 帳票出力名修正 ADD START
	/** 模試コード(出力ファイル名用) */
	private String examCd;
	// 2019/09/09 QQ)Tanioka 帳票出力名修正 ADD END

	/**
	 * 郵便番号を返します。
	 *
	 * @return 郵便番号
	 */
	public String getPostCode() {
		return postCode;
	}

	/**
	 * 郵便番号をセットします。
	 *
	 * @param postCode
	 *            郵便番号
	 */
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	/**
	 * 住所を返します。
	 *
	 * @return 住所
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * 住所をセットします。
	 *
	 * @param address
	 *            住所
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * 一括コードを返します。
	 *
	 * @return 一括コード
	 */
	public String getBundleCd() {
		return bundleCd;
	}

	/**
	 * 一括コードをセットします。
	 *
	 * @param bundleCd
	 *            一括コード
	 */
	public void setBundleCd(String bundleCd) {
		this.bundleCd = bundleCd;
	}

	/**
	 * 学校名を返します。
	 *
	 * @return 学校名
	 */
	public String getBundleName() {
		return bundleName;
	}

	/**
	 * 学校名をセットします。
	 *
	 * @param bundleName
	 *            学校名
	 */
	public void setBundleName(String bundleName) {
		this.bundleName = bundleName;
	}

	/**
	 * 担当先生名を返します。
	 *
	 * @return 担当先生名
	 */
	public String getTeacherName() {
		return teacherName;
	}

	/**
	 * 担当先生名をセットします。
	 *
	 * @param teacherName
	 *            担当先生名
	 */
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	/**
	 * 対象模試名を返します。
	 *
	 * @return 対象模試名
	 */
	public String getExamName() {
		return examName;
	}

	/**
	 * 対象模試名をセットします。
	 *
	 * @param examName
	 *            対象模試名
	 */
	public void setExamName(String examName) {
		this.examName = examName;
	}

	/**
	 * パスワードを返します。
	 *
	 * @return パスワード
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * パスワードをセットします。
	 *
	 * @param password
	 *            パスワード
	 */
	public void setPassword(String password) {
		this.password = password;
	}

// 2019/09/09 QQ)Tanioka 帳票出力名修正 ADD START
	/**
	 * パスワードを返します。
	 *
	 * @return パスワード
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * パスワードをセットします。
	 *
	 * @param password
	 *            パスワード
	 */
	public void setExamCd(String examCd) {
		this.examCd = examCd;
	}
// 2019/09/09 QQ)Tanioka 帳票出力名修正 ADD END

}

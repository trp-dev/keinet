/*
 * 作成日: 2004/07/21
 *
 * バランスチャート(合格者平均)表示用サーブレット
 *
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.graph.BalanceChartApplet;
import jp.co.fj.keinavi.beans.help.OnepointBean;
import jp.co.fj.keinavi.beans.individual.I104Bean;
import jp.co.fj.keinavi.data.individual.CourseData;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I104Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.KNUtil;

/**
 * @author Totec) K.Kondo
 *
 * バランスチャート(合格者平均)を表示する。
 * プロファイルからデータ取得するが、I104で再表示のときはフォームからデータ取得
 *
 * 2005.8.23 	Totec) T.Yamada 	[1]印刷での対象模試がセンターリサーチの時のチェックボックス無効時の対応
 * 2016.1.14	QQ) K.Hisakawa		サーブレット化対応
 *
 */
public class I104Servlet extends IndividualServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		final HttpServletRequest request,
		final HttpServletResponse response)
		throws ServletException, IOException{

		super.execute(request, response);

		//フォームデータを取得 ::::::::
		final I104Form i104Form = (I104Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.individual.I104Form");

		//プロファイル情報
		final Profile profile = getProfile(request);
		String devRanges = (String)profile.getItemMap(IProfileCategory.I_SCORE_BC_AVG).get(IProfileItem.DEV_RANGE);
		Short indMultiExam = (Short)profile.getItemMap(IProfileCategory.I_SCORE_BC_AVG).get(IProfileItem.IND_MULTI_EXAM);
		Short printStamp = (Short) profile.getItemMap(IProfileCategory.I_SCORE_BC).get(IProfileItem.PRINT_STAMP);
		Short printStudent = (Short) profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);

		//個人共通
		final ICommonMap iCommonMap = getICommonMap(request);

		// 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
        Collection cCourseDatas = null;
        Collection cExaminationDatas = null;
        // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

		// 転送元がJSPなら対象模試情報を初期化
		if ("i104".equals(getBackward(request))) {
			initTargetExam(request);
		}

		if ("i104".equals(getForward(request))) {
			// JSP転送
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);

				//ワンポイントアドバイス
				OnepointBean onebean = new OnepointBean();
				onebean.setConnection(null, con);
				onebean.setScreenID("i104");
				onebean.execute();
				request.setAttribute("OnepointBean", onebean);

				I104Bean i104Bean = new I104Bean();

				//模試種類セット
				i104Bean.setExamTypecd(iCommonMap.getTargetExamTypeCode());

				if(!"i104".equals(getBackward(request))
						|| !iCommonMap.getTargetPersonId().equals(i104Form.getTargetPersonId())
						|| isChangedExam(request) || isChangedMode(request)) {

					/** 初回アクセス */
					i104Bean.setFirstAccess(true);

					//検索条件の設定
					i104Bean.setSearchCondition(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(),
												devRanges, i104Form.getTargetPersonId());

					//教科複数受験フラグをbeanにセット
					// 第1解答課目模試以外の場合、１教科複数受験は平均とする
					if ("i104".equals(getBackward(request))){
						if (!KNUtil.isAns1st(iCommonMap.getTargetExamCode())
								&& i104Form.getAvgChoice().equals("3")) {
							i104Bean.setAvgChoice("1");
						} else {
							i104Bean.setAvgChoice(i104Form.getAvgChoice());
						}
					} else {
						if (!KNUtil.isAns1st(iCommonMap.getTargetExamCode())
								&& indMultiExam.toString().equals("3")) {
							i104Bean.setAvgChoice("1");
						} else {
							i104Bean.setAvgChoice(""+indMultiExam);
						}
					}

					//検索実行
					i104Bean.setConnection("", con);
					i104Bean.execute();

					//模試タイプ
					i104Form.setMarkexamflg(i104Bean.isExamflg());
					i104Form.setDispflg(i104Bean.isDispflg());

					// 生徒を切り替えたとき
					// モードを切り替えたとき
					// 対象模試を変えたとき
					if ("i104".equals(getBackward(request))){

						/** 生徒が変更された時 */
						//フォーム作成
						i104Form.setTargetCandidateRank1("1");//第一志望
						i104Form.setTargetCandidateRank2("2");//第二志望
						i104Form.setTargetCandidateRank3("3");//第三志望

						//1.印刷対象生徒をフォームにセットしなおす
						String[] printTarget = {"0","0","0","0"};
						if(i104Form.getPrintTarget() != null){
							for(int i=0; i<i104Form.getPrintTarget().length; i++){
								printTarget[Integer.parseInt(i104Form.getPrintTarget()[i])] = "1";
							}
						}
						//[1] add start
						final String CENTER_EXAMCD = "38";
						if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
							printTarget[3] = "1";
						}
						//[1] add end
						i104Form.setPrintTarget(printTarget);

						// 第1解答課目模試以外の場合、１教科複数受験は平均とする
						if (!KNUtil.isAns1st(iCommonMap.getTargetExamCode())
							&& i104Form.getAvgChoice().equals("3")) {
							i104Form.setAvgChoice("1");
						}

						// 初回アクセス
					} else {

						/** プロファイルデータをフォームにセットする */
						//初回用フォーム作成
						i104Form.setTargetCandidateRank1("1");//第一志望
						i104Form.setTargetCandidateRank2("2");//第二志望
						i104Form.setTargetCandidateRank3("3");//第三志望

						//個人成績分析共通-印刷対象生保存をセット
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
						//i104Form.setPrintStudent(printStudent.toString());
						i104Form.setPrintStudent(iCommonMap.getPrintStudent());
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END

						//1.印刷対象生徒をセット
						String printTarget = (String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET);
						i104Form.setPrintTarget(CollectionUtil.splitComma(printTarget));

						//2.偏差値範囲をセット
						i104Form.setLowerRange(CollectionUtil.deconcatComma(devRanges)[0]);
						i104Form.setUpperRange(CollectionUtil.deconcatComma(devRanges)[1]);

						//3.教科複数受験を保存
						// 第1解答課目模試以外の場合、１教科複数受験は平均とする
						if (!KNUtil.isAns1st(iCommonMap.getTargetExamCode())
								&& indMultiExam.toString().equals("3")) {
							i104Form.setAvgChoice("1");
						} else {
							i104Form.setAvgChoice(indMultiExam.toString());
						}

						//4.セキュリティスタンプセット
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
						//i104Form.setPrintStamp(printStamp.toString());
						i104Form.setPrintStamp(iCommonMap.getPrintStamp());
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END


					}

				// 再表示アクセス
				} else {
					//このranksはnullの可能性がある
					String[] ranks = new String[3];
					ranks[0] = i104Form.getTargetCandidateRank1();
					ranks[1] = i104Form.getTargetCandidateRank2();
					ranks[2] = i104Form.getTargetCandidateRank3();

					String[] ranges = {i104Form.getLowerRange(),
										i104Form.getUpperRange()};

					i104Bean.setFirstAccess(false);
					i104Bean.setSearchCondition(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(),
												ranges,	i104Form.getTargetPersonId(), ranks);

					//教科複数受験フラグをbeanにセット
					i104Bean.setAvgChoice(i104Form.getAvgChoice());

					//検索実行
					i104Bean.setConnection("", con);
					i104Bean.execute();

					//模試タイプ
					i104Form.setMarkexamflg(i104Bean.isExamflg());
					i104Form.setDispflg(i104Bean.isDispflg());

					/** フォームの値をプロファイルに保存　*/

					//1.印刷対象生徒をフォームにセットしなおす
					String[] printTarget = {"0","0","0","0"};
					if(i104Form.getPrintTarget() != null){
						for(int i=0; i<i104Form.getPrintTarget().length; i++){
							printTarget[Integer.parseInt(i104Form.getPrintTarget()[i])] = "1";
						}
					}
					//[1] add start
					final String CENTER_EXAMCD = "38";
					if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
						printTarget[3] = "1";
					}
					//[1] add end
					i104Form.setPrintTarget(printTarget);

					//2.偏差値範囲を保存
					String devRangeUL = CollectionUtil.deSplitComma(new String[]{i104Form.getLowerRange(),i104Form.getUpperRange()});
					profile.getItemMap(IProfileCategory.I_SCORE_BC_AVG).put(IProfileItem.DEV_RANGE, devRangeUL);

					//3.教科複数受験を保存
					profile.getItemMap(IProfileCategory.I_SCORE_BC_AVG).put(IProfileItem.IND_MULTI_EXAM, new Short(i104Form.getAvgChoice()));

				}

				request.setAttribute("i104Bean", i104Bean);

                // 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
				cCourseDatas = i104Bean.getCourseDatas();
                cExaminationDatas = i104Bean.getExaminationDatas();
                // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

			} catch(final  Exception e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			request.setAttribute("form", i104Form);
			iCommonMap.setTargetPersonId(i104Form.getTargetPersonId());

			// 2016/01/14 QQ)Hisakawa 大規模改修 ADD START
			int subCount = 0;
            String strSubjectDAT = "";

            for (java.util.Iterator it=cCourseDatas.iterator(); it.hasNext();) {
				if(subCount != 0) {
					strSubjectDAT = strSubjectDAT + ",";
					strSubjectDAT = strSubjectDAT + String.valueOf(((CourseData)it.next()).getCourseName());
				} else {
					strSubjectDAT = String.valueOf(((CourseData)it.next()).getCourseName());
				}
				subCount ++;
			}

            int iItemNUM = cExaminationDatas.size();

			String moshiString = "";
			String[] dataStrings = new String[cExaminationDatas.size()];
			int moshiCount = 0;
			String maxEngSubCd = "";
			String maxMathSubCd = "";
			String maxJapSubCd = "";
			String maxSciSubCd = "";
			String maxSocSubCd = "";

			for (java.util.Iterator it=cExaminationDatas.iterator(); it.hasNext();) {
			    ExaminationData data = (ExaminationData)it.next();

			    String lowRange = i104Form.getLowerRange();

			    String upRange = "100.0";

			    if(moshiCount != 0) {
			        //大学の場合
			        moshiString += ",";
			        dataStrings[moshiCount] += ",";

			        if(i104Form.getAvgChoice().equals("1")) {
			            //平均表示
			            if(cCourseDatas.size() > 3) {
			                    if(i104Form.isMarkexamflg() && i104Form.isDispflg()){
			                        //平均を表示だけど、平均値が存在するのは理科・地歴のみ、他は全部MAXでマーク模試
			                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
			                    }else{
			                        //平均を表示だけど、平均値が存在するのは理科・地歴のみ、他は全部MAXでマーク模試以外
			                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
			                    }
			            } else {
			                    if(i104Form.isMarkexamflg() && i104Form.isDispflg()){
			                        dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
			                    }else{
			                        dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
			                    }
			            }
			        } else if(i104Form.getAvgChoice().equals("3")) {
			            //第1解答科目
			            if(cCourseDatas.size() > 3) {
			                    if(i104Form.isMarkexamflg() && i104Form.isDispflg()){
			                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
			                        if (!maxSciSubCd.equals("-999")) {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+",";
			                        } else {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+",";
			                        }
			                        if (!maxSocSubCd.equals("-999")) {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
			                        } else {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
			                        }
			                    }else{
			                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
			                        if (!maxSciSubCd.equals("-999")) {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+",";
			                        } else {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+",";
			                        }
			                        if (!maxSocSubCd.equals("-999")) {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
			                        } else {
			                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
			                        }
			                    }
			            } else {
			                    if(i104Form.isMarkexamflg() && i104Form.isDispflg()){
			                        dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
			                    }else{
			                        dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
			                    }
			            }
			        } else {
			            //平均表示良いほう表示
			            if(cCourseDatas.size() > 3) {
			                    //良い方マーク模試
			                    if(i104Form.isMarkexamflg() && i104Form.isDispflg()){

			                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
			                    }else{
			                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
			                    }
			            } else {
			                    //良い方マーク模試以外
			                    if(i104Form.isMarkexamflg() && i104Form.isDispflg()){
			                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
			                    }else{
			                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
			                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
			                    }
			            }
			        }
			    } else {
			        //模試の場合
			        if(i104Form.getAvgChoice().equals("1")) {
			            //平均表示
			            if(cCourseDatas.size() > 3) {
			                    //平均を表示だけど、平均値が存在するのは理科・地歴のみ、他は全部MAX
			                    if(i104Form.isMarkexamflg() && i104Form.isDispflg()){
			                        dataStrings[moshiCount] =
			                                                GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAvgSci(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAvgSoc(), lowRange, upRange);

			                                                maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                                maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                                maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                    }else{
			                        dataStrings[moshiCount] =
			                                                GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAvgSci(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAvgSoc(), lowRange, upRange);

			                                                maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                                maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                                maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                    }
			            } else {

			                if(i104Form.isMarkexamflg() && i104Form.isDispflg()){
			                                dataStrings[moshiCount] =
			                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

			                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                                }else{
			                                dataStrings[moshiCount] =
			                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

			                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                                }
			            }
			        } else if(i104Form.getAvgChoice().equals("3")) {
			            // 第1解答科目表示
			            if(cCourseDatas.size() > 3) {
			                    if(i104Form.isMarkexamflg() && i104Form.isDispflg()){
			                        dataStrings[moshiCount] =
			                                                GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAns1stSci(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAns1stSoc(), lowRange, upRange);

			                                                maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                                maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                                maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                                                maxSciSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSci());
			                                                maxSocSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSoc());
			                    }else{
			                        dataStrings[moshiCount] =
			                                                GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAns1stSci(), lowRange, upRange)+","+
			                                                GeneralUtil.toRangeLimit(data.getAns1stSoc(), lowRange, upRange);

			                                                maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                                maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                                maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                                                maxSciSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSci());
			                                                maxSocSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSoc());
			                    }
			            } else {

			                if(i104Form.isMarkexamflg() && i104Form.isDispflg()){
			                                dataStrings[moshiCount] =
			                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

			                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                                }else{
			                                dataStrings[moshiCount] =
			                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

			                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                                }
			            }
			        } else {
			                //良い方表示
			                if(cCourseDatas.size() > 3) {
			                    dataStrings[moshiCount] =
			                                            GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                            GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                            GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
			                                            GeneralUtil.toRangeLimit(data.getMaxSci(), lowRange, upRange)+","+
			                                            GeneralUtil.toRangeLimit(data.getMaxSoc(), lowRange, upRange);

			                                            maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                            maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                            maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                                            maxSciSubCd = GeneralUtil.to999(data.getMaxSciCode());
			                                            maxSocSubCd = GeneralUtil.to999(data.getMaxSocCode());
			                } else {
			                    dataStrings[moshiCount] =
			                                            GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
			                                            GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
			                                            GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

			                                            maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
			                                            maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
			                                            maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
			                }
			            }
			    }
			    moshiString += data.getExamName();
			    moshiCount ++;
			}

			BalanceChartApplet App = new BalanceChartApplet();
            App.setParameter("dataSelect", "off");
            App.setParameter("dispSelect", "on");
            App.setParameter("dispZoneL", String.valueOf((int)Double.parseDouble(i104Form.getLowerRange())));
            App.setParameter("dispZoneH", String.valueOf((int)Double.parseDouble(i104Form.getUpperRange())));
            App.setParameter("subjectNUM", String.valueOf(cCourseDatas.size()));
            App.setParameter("subjectDAT", strSubjectDAT);
            App.setParameter("itemTITLE", "□対象生徒偏差値,■合格者平均偏差値");
            App.setParameter("itemNUM", String.valueOf(iItemNUM));
            App.setParameter("itemDAT", moshiString);
            for(int i=0; i<dataStrings.length; i++){
                App.setParameter("DAT" + String.valueOf(i+1), dataStrings[i]);
            }
            App.setParameter("colorDAT", "1,0,2,15,13,14");

            App.init();

            HttpSession session = request.getSession(true);

            session.setAttribute("GraphServlet", App);
            session.setAttribute("GraphType", "BalanceChart");

            App = null;
            dataStrings = null;
            // 2016/01/14 QQ)Hisakawa 大規模改修 ADD END

			super.forward(request, response, JSP_I104);

		//不明ならServlet転送
		} else {

			//Forwordがsheetならプロファイルを保存する
			if ("sheet".equals(getForward(request))) {

				/** フォームの値をプロファイルに保存　*/
				//分析モードなら個人成績分析共通-印刷対象生保存をセット
				if(iCommonMap.isBunsekiMode()){
					profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(i104Form.getPrintStudent()));
				}

				//1.印刷対象生徒をセット
				String[] printTarget = {"0","0","0","0"};
				if(i104Form.getPrintTarget() != null){
					for(int i=0; i<i104Form.getPrintTarget().length; i++){
						printTarget[Integer.parseInt(i104Form.getPrintTarget()[i])] = "1";
					}
				}
				//[1] add start
				final String CENTER_EXAMCD = "38";
				if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
					printTarget[3] = "1";
				}
				//[1] add end
				profile.getItemMap(IProfileCategory.I_SCORE_COMMON).put(IProfileItem.PRINT_TARGET, CollectionUtil.deSplitComma(printTarget));

				//2.偏差値範囲を保存
				String devRangeUL = CollectionUtil.deSplitComma(new String[]{i104Form.getLowerRange(),i104Form.getUpperRange()});
				profile.getItemMap(IProfileCategory.I_SCORE_BC_AVG).put(IProfileItem.DEV_RANGE, devRangeUL);

				//3.教科複数受験を保存
				profile.getItemMap(IProfileCategory.I_SCORE_BC_AVG).put(IProfileItem.IND_MULTI_EXAM, new Short(i104Form.getAvgChoice()));

				//4.セキュリティスタンプを保存
				profile.getItemMap(IProfileCategory.I_SCORE_BC).put(IProfileItem.PRINT_STAMP, new Short(i104Form.getPrintStamp()));

			} else {

				/** フォームの値をプロファイルに保存　*/
				//分析モードなら個人成績分析共通-印刷対象生保存をセット
				if(iCommonMap.isBunsekiMode()){
					profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(i104Form.getPrintStudent()));
				}

				//1.印刷対象生徒をセット
				String[] printTarget = {"0","0","0","0"};
				if(i104Form.getPrintTarget() != null){
					for(int i=0; i<i104Form.getPrintTarget().length; i++){
						printTarget[Integer.parseInt(i104Form.getPrintTarget()[i])] = "1";
					}
				}
				//[1] add start
				final String CENTER_EXAMCD = "38";
				if(CollectionUtil.splitComma(((String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET)))[3].equals("1") && iCommonMap.getTargetExamCode().equals(CENTER_EXAMCD)) {
					printTarget[3] = "1";
				}
				//[1] add end
				profile.getItemMap(IProfileCategory.I_SCORE_COMMON).put(IProfileItem.PRINT_TARGET, CollectionUtil.deSplitComma(printTarget));

				//4.セキュリティスタンプを保存
				profile.getItemMap(IProfileCategory.I_SCORE_BC).put(IProfileItem.PRINT_STAMP, new Short(i104Form.getPrintStamp()));
			}

			iCommonMap.setTargetPersonId(i104Form.getTargetPersonId());
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD START
                        iCommonMap.setPrintStamp(i104Form.getPrintStamp());
                        iCommonMap.setPrintStudent(i104Form.getPrintStudent());
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD END
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

/*
 * 作成日: 2004/09/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.helpdesk;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.forms.helpdesk.HD102Form;
import jp.co.fj.keinavi.beans.helpdesk.FileNameBean;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class HD102Servlet extends DefaultHttpServlet {

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO 自動生成されたメソッド・スタブ

		// セッション
		HttpSession session = request.getSession();

		// ログイン情報
		LoginSession login = super.getLoginSession(request);

		// アクションフォーム
		HD102Form hd102Form = null;
		// requestから取得する
		try {
			hd102Form = (HD102Form)super.getActionForm(request,
				"jp.co.fj.keinavi.forms.helpdesk.HD102Form");
		} catch (Exception e) {
			throw new ServletException(e);
		}
		
		if(("hd102").equals(getForward(request)) || getForward(request) == null) {

			Connection con = null; // コネクション
			try {
				con = super.getConnectionPool(request); 	// コネクション取得
				FileNameBean bean = new FileNameBean();
				bean.setConnection(null, con);
				bean.setHD102Form(hd102Form);
				bean.execute();
		
				request.setAttribute("UserListBean", bean);

			} catch (Exception e) {
				throw new ServletException(e.toString());
			} finally {
				super.releaseConnectionPool(request, con); // コネクション解放
			}
		

			super.forward(request, response, JSP_HD102);
	
		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}

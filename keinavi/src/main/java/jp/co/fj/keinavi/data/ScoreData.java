/*
 * 作成日: 2004/07/07
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data;

import java.io.Serializable;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class ScoreData implements Serializable {

	private String scorePersonId;
	private String scoreExamYear;
	private String scoreExamCd;
	private String scoreSubCd;
	private String scoreBundleCd;
	private int scoreScore;
	private int scoreCvsScore;
	private int scoreSRank;
	private int scoreCRank;
	private float scoreSDeviation;
	private float scoreCDeviation;
	
	private String scoreExamName;
	private String scoreExamAddrName;
	private String scoreExamTypeCd;
	private String scoreExamSt;
	private String scoreExamDiv;
	
	private String scoreSubName;
	private String scoreSubAddrName;
	private String scoreDispsequence;

	/**
	 * @return
	 */
	public String getScoreBundleCd() {
		return scoreBundleCd;
	}

	/**
	 * @return
	 */
	public float getScoreCDeviation() {
		return scoreCDeviation;
	}

	/**
	 * @return
	 */
	public int getScoreCRank() {
		return scoreCRank;
	}

	/**
	 * @return
	 */
	public int getScoreCvsScore() {
		return scoreCvsScore;
	}

	/**
	 * @return
	 */
	public String getScoreDispsequence() {
		return scoreDispsequence;
	}

	/**
	 * @return
	 */
	public String getScoreExamAddrName() {
		return scoreExamAddrName;
	}

	/**
	 * @return
	 */
	public String getScoreExamCd() {
		return scoreExamCd;
	}

	/**
	 * @return
	 */
	public String getScoreExamDiv() {
		return scoreExamDiv;
	}

	/**
	 * @return
	 */
	public String getScoreExamName() {
		return scoreExamName;
	}

	/**
	 * @return
	 */
	public String getScoreExamSt() {
		return scoreExamSt;
	}

	/**
	 * @return
	 */
	public String getScoreExamTypeCd() {
		return scoreExamTypeCd;
	}

	/**
	 * @return
	 */
	public String getScoreExamYear() {
		return scoreExamYear;
	}

	/**
	 * @return
	 */
	public String getScorePersonId() {
		return scorePersonId;
	}

	/**
	 * @return
	 */
	public int getScoreScore() {
		return scoreScore;
	}

	/**
	 * @return
	 */
	public float getScoreSDeviation() {
		return scoreSDeviation;
	}

	/**
	 * @return
	 */
	public int getScoreSRank() {
		return scoreSRank;
	}

	/**
	 * @return
	 */
	public String getScoreSubAddrName() {
		return scoreSubAddrName;
	}

	/**
	 * @return
	 */
	public String getScoreSubCd() {
		return scoreSubCd;
	}

	/**
	 * @return
	 */
	public String getScoreSubName() {
		return scoreSubName;
	}

	/**
	 * @param string
	 */
	public void setScoreBundleCd(String string) {
		scoreBundleCd = string;
	}

	/**
	 * @param f
	 */
	public void setScoreCDeviation(float f) {
		scoreCDeviation = f;
	}

	/**
	 * @param i
	 */
	public void setScoreCRank(int i) {
		scoreCRank = i;
	}

	/**
	 * @param i
	 */
	public void setScoreCvsScore(int i) {
		scoreCvsScore = i;
	}

	/**
	 * @param string
	 */
	public void setScoreDispsequence(String string) {
		scoreDispsequence = string;
	}

	/**
	 * @param string
	 */
	public void setScoreExamAddrName(String string) {
		scoreExamAddrName = string;
	}

	/**
	 * @param string
	 */
	public void setScoreExamCd(String string) {
		scoreExamCd = string;
	}

	/**
	 * @param string
	 */
	public void setScoreExamDiv(String string) {
		scoreExamDiv = string;
	}

	/**
	 * @param string
	 */
	public void setScoreExamName(String string) {
		scoreExamName = string;
	}

	/**
	 * @param string
	 */
	public void setScoreExamSt(String string) {
		scoreExamSt = string;
	}

	/**
	 * @param string
	 */
	public void setScoreExamTypeCd(String string) {
		scoreExamTypeCd = string;
	}

	/**
	 * @param string
	 */
	public void setScoreExamYear(String string) {
		scoreExamYear = string;
	}

	/**
	 * @param string
	 */
	public void setScorePersonId(String string) {
		scorePersonId = string;
	}

	/**
	 * @param i
	 */
	public void setScoreScore(int i) {
		scoreScore = i;
	}

	/**
	 * @param f
	 */
	public void setScoreSDeviation(float f) {
		scoreSDeviation = f;
	}

	/**
	 * @param i
	 */
	public void setScoreSRank(int i) {
		scoreSRank = i;
	}

	/**
	 * @param string
	 */
	public void setScoreSubAddrName(String string) {
		scoreSubAddrName = string;
	}

	/**
	 * @param string
	 */
	public void setScoreSubCd(String string) {
		scoreSubCd = string;
	}

	/**
	 * @param string
	 */
	public void setScoreSubName(String string) {
		scoreSubName = string;
	}

}

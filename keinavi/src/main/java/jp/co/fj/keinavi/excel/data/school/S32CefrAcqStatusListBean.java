package jp.co.fj.keinavi.excel.data.school;

import java.util.Objects;

/**
 * 校内成績分析−クラス比較−偏差値分布 CEFR取得状況データリスト
 * 作成日: 2019/09/12
 * @author      DP)H.Nonaka
 */
public class S32CefrAcqStatusListBean {

    private String engptcd;                                 //英語参加試験コード
    private String engptname_abbr;                          //参加試験短縮名
    private String levelflg;                                //レベルありフラグ
    private int ei_sort;                                    //並び順
    private String engptlevelcd;                            //英語参加試験レベルコード
    private String engptlevelname_abbr;                      //参加試験レベル短縮名
    private int edi_sort;                                   //並び順
    private String cefrlevelcd;                             //CEFRレベルコード
    private String cerflevelname_abbr;                       //ＣＥＦＲレベル短縮名
    private int ci_sort;                                    //並び順
    private String classes;                                 //クラス
    private int dispsequence;                               //並び順
    private int numbers;                                    //人数
    private float compratio;                                //構成比
    private String flg;                                      //受験有無フラグ

    /*----------*/
    /* Get      */
    /*----------*/
    public String getEngptcd() {
        return engptcd;
    }
    public String getEngptname_abbr() {
        return engptname_abbr;
    }
    public String getLevelflg() {
        return levelflg;
    }
    public int getEi_sort() {
        return ei_sort;
    }
    public String getEngptlevelcd() {
        return engptlevelcd;
    }
    public String getEngptlevelname_abbr() {
        return engptlevelname_abbr;
    }
    public int getEdi_sort() {
        return edi_sort;
    }
    public String getCefrlevelcd() {
        return cefrlevelcd;
    }
    public String getCerflevelname_abbr() {
        return cerflevelname_abbr;
    }
    public int getCi_sort() {
        return ci_sort;
    }
    public String getClasses() {
// 2019/10/03 QQ)Tanioka クラス名がnullの時に「null」と表示されないよう修正 UPD START
//    	return classes;
    	if( Objects.equals(classes, null) ) {
    		return "";
    	}else {
    		return classes;
    	}
// 2019/10/03 QQ)Tanioka クラス名がnullの時に「null」と表示されないよう修正 UPD END
    }
    public int getDispsequence() {
        return dispsequence;
    }
    public int getNumbers() {
        return numbers;
    }
    public float getCompratio() {
        return compratio;
    }
    public String getFlg() {
        return flg;
    }

    /*----------*/
    /* Set      */
    /*----------*/
    public void setEngptcd(String engptcd) {
        this.engptcd = engptcd;
    }
    public void setEngptname_abbr(String engptname_abbr) {
        this.engptname_abbr = engptname_abbr;
    }
    public void setLevelflg(String levelflg) {
        this.levelflg = levelflg;
    }
    public void setEi_sort(int ei_sort) {
        this.ei_sort = ei_sort;
    }
    public void setEngptlevelcd(String engptlevelcd) {
        this.engptlevelcd = engptlevelcd;
    }
    public void setEngptlevelname_abbr(String engptlevelname_abbr) {
        this.engptlevelname_abbr = engptlevelname_abbr;
    }
    public void setEdi_sort(int edi_sort) {
        this.edi_sort = edi_sort;
    }
    public void setCefrlevelcd(String cefrlevelcd) {
        this.cefrlevelcd = cefrlevelcd;
    }
    public void setCerflevelname_abbr(String cerflevelname_abbr) {
        this.cerflevelname_abbr = cerflevelname_abbr;
    }
    public void setCi_sort(int ci_sort) {
        this.ci_sort = ci_sort;
    }
    public void setClasses(String classes) {
        this.classes = classes;
    }
    public void setDispsequence(int dispsequence) {
        this.dispsequence = dispsequence;
    }
    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }
    public void setCompratio(float compratio) {
        this.compratio = compratio;
    }
    public void setFlg(String flg) {
        this.flg = flg;
    }
}

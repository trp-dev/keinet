/*
 * 作成日: 2004/07/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.school.factory;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.school.SMaxForm;

import com.fjh.forms.ActionForm;

/**
 * @author 奥村ゆかり
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 * 校内成績：偏差値分布：詳細
 */
public class B003FormFactory extends AbstractSFormFactory {
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#createActionForm(javax.servlet.http.HttpServletRequest)
	 * 偏差値分布
	 */
	public ActionForm createActionForm(HttpServletRequest request) {
		// アクションフォーム
		SMaxForm form = super.createSMaxForm(request);
		// アイテムマップ
		Map item = getItemMap(request);
		// ログイン情報
		LoginSession login =
			(LoginSession)request.getSession().getAttribute(LoginSession.SESSION_KEY);

		CommonFormFactory commForm = new CommonFormFactory(form, item);
		commForm.setForm0103();
		commForm.setForm0203();
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD START
//// 2019/09/13 QQ)Tanioka 表にチェックボックスを追加 UPD START
////		commForm.setForm0601();
//		commForm.setForm0602();
//// 2019/09/13 QQ)Tanioka 表にチェックボックスを追加 UPD END
//// 2019/09/05 QQ)Tanioka 共通テスト対応 ADD START
//		commForm.setForm1602();
//// 2019/09/05 QQ)Tanioka 共通テスト対応 ADD END
		commForm.setForm0601();
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD END
//		commForm.setForm0703();
//		commForm.setForm0705();
//		commForm.setForm0702();
		commForm.setForm1102();
		commForm.setForm0402();
		commForm.setForm1305(login.isSales());
		commForm.setFormat();
		commForm.setOption();
		return commForm.getForm();
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.com_set.creator.AbstractActionFormFactory#restore(javax.servlet.http.HttpServletRequest)
	 */
	public void restore(HttpServletRequest request) {
		// アクションフォーム
		//		SMaxForm form = (SMaxForm)request.getSession().getAttribute(SMaxForm.SESSION_KEY);
		SMaxForm form = (SMaxForm) request.getAttribute("form");

		// アイテムマップ
		Map item = getItemMap(request);
		CommonItemFactory commItem = new CommonItemFactory(form, item);

		commItem.setItem0103();
		commItem.setItem0203();
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD START
//// 2019/09/13 QQ)Tanioka 表にチェックボックスを追加 UPD START
////		commItem.setB003Item0602();
//		commItem.setItem0602();
//// 2019/09/13 QQ)Tanioka 表にチェックボックスを追加 UPD END
//// 2019/09/05 QQ)Tanioka 共通テスト対応 ADD START
//		commItem.setItem1602();
//// 2019/09/05 QQ)Tanioka 共通テスト対応 ADD END
		commItem.setB003Item0602();
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD END
//		commItem.setItem0703();
//		commItem.setItem0705();
//		commItem.setItem0702();
		commItem.setItem1102();
		commItem.setItem0402();
		commItem.setItem1305();
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.forms.school.factory.AbstractSFormFactory#getItemMap(javax.servlet.http.HttpServletRequest)
	 */
	public Map getItemMap(HttpServletRequest request) {
		return super.getProfile(request).getItemMap("020502");
	}

}

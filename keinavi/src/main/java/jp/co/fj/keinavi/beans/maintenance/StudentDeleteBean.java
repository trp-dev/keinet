package jp.co.fj.keinavi.beans.maintenance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 生徒情報削除Bean
 * 
 * 2006.11.01	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class StudentDeleteBean extends DefaultBean {

	// 学校コード
	private final String schoolCd;
	// ログ出力モジュール
	private final MainteLogBean log;
	
	// 対象個人ID
	private String[] individualId;
	
	// 削除対象テーブル
	private static final String[] TARGET = new String[] {
		"historyinfo", // 学籍履歴情報
		"examplanuniv", // 受験予定大学
		"interview", // 面談メモ
		"determhistory", // 志望大学判定の判定履歴（判定履歴トラン）
		"hangupproviso", // 志望大学判定（こだわり判定）の条件（こだわり条件データ）
		"autodetermination", // 志望大学判定（オマカセ判定）の判定結果（オマカセ判定トラン）
	};
	
	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 処理する学校コード
	 */
	public StudentDeleteBean(final String pSchoolCd) {
		schoolCd = pSchoolCd;
		log = new MainteLogBean(pSchoolCd);
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
		
		// 削除ログ
		log.deleteLog(individualId);
		
		// 削除処理
		deleteBasicinfo();
		for (int i = 0; i < TARGET.length; i++) {
			deleteRecord(TARGET[i]);
		}
	}

	// 学籍基本情報の削除
	private void deleteBasicinfo() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("DELETE FROM basicinfo "
					+ "WHERE schoolcd = ? AND individualid = ?");
			ps.setString(1, schoolCd);
			for (int i = 0; i < individualId.length; i++) {
				ps.setString(2, individualId[i]);
				// 必ず１レコードは削除されるはず
				if (ps.executeUpdate() == 0) {
					throw new SQLException(
							"学籍基本情報の削除に失敗しました。");
				}
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	// 各種データ削除
	private void deleteRecord(final String tableName) throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("DELETE FROM " + tableName
					+ " WHERE individualid = ?");
			for (int i = 0; i < individualId.length; i++) {
				ps.setString(1, individualId[i]);
				ps.executeUpdate();
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * @see com.fjh.beans.DefaultBean#setConnection(
	 * 			java.lang.String, java.sql.Connection)
	 */
	public void setConnection(final String pName, final Connection pConn) {
		super.setConnection(pName, pConn);
		log.setConnection(pName, pConn);
	}

	/**
	 * @param pIndividualId 設定する individualId。
	 */
	public void setIndividualId(String[] pIndividualId) {
		individualId = pIndividualId;
	}
	
}

package jp.co.fj.keinavi.beans.sheet.school.data;

/**
 *
 * 過回リストデータ
 * 
 * 2007.07.26	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class S52_11ExamListData extends S42_15BundleListData {
	
	// 模試コード
	private String examCd;
	
	// 模試名
	private String examName;
	
	// 模試実施基準日
	private String inpleDate;

	/**
	 * @return examCd
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * @param pExamCd 設定する examCd
	 */
	public void setExamCd(String pExamCd) {
		examCd = pExamCd;
	}

	/**
	 * @return examName
	 */
	public String getExamName() {
		return examName;
	}

	/**
	 * @param pExamName 設定する examName
	 */
	public void setExamName(String pExamName) {
		examName = pExamName;
	}

	/**
	 * @return inpleDate
	 */
	public String getInpleDate() {
		return inpleDate;
	}

	/**
	 * @param pInpleDate 設定する inpleDate
	 */
	public void setInpleDate(String pInpleDate) {
		inpleDate = pInpleDate;
	}
	
}

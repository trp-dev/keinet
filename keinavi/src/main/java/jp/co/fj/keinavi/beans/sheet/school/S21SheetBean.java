/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.excel.data.school.S21Item;
import jp.co.fj.keinavi.excel.data.school.S21ListBean;
import jp.co.fj.keinavi.excel.school.S21;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * @author kawai
 *
 */
public class S21SheetBean extends AbstractSheetBean {

	// データクラス
	private final S21Item data = new S21Item(); 

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// 比較対象年度
		final String[] years = getCompYearArray();
		// 受験生がいる型・科目コード
		final String[] code = (String[]) getExistsSubCDList(0).toArray(new String[0]);
		// 新テストかどうか
		final boolean isNewExam = KNUtil.isNewExam(exam);
		
		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntHyouFlg(getIntFlag(IProfileItem.CHART)); // 表フラグ
		data.setIntGraphFlg(getIntFlag(IProfileItem.GRAPH)); // グラフフラグ
		data.setIntSecuFlg(getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(getOutputType()); // 出力種別フラグ

		// ワークテーブルのセットアップ
		insertIntoExistsExam();
		insertIntoSubCDTrans(new ExamData[]{exam});
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// 設問別成績データリスト
			Query query = QueryLoader.getInstance().load("s21");
			query.setStringArray(1, code); // 型・科目コード
			query.setStringArray(2, years); // 比較対象年度
		
			ps = conn.prepareStatement(query.toString());
			ps.setString(1, exam.getExamYear()); // 模試年度
			ps.setString(2, exam.getExamCD()); // 模試コード
			ps.setString(3, profile.getBundleCD()); // 一括コード
			ps.setString(4, exam.getExamCD()); // 模試コード

			List c1 = new LinkedList(); // 型の入れ物
			List c2 = new LinkedList(); // 科目の入れ物

			rs = ps.executeQuery();
			while (rs.next()) {
				// データBeanを作って入れる
				S21ListBean bean = new S21ListBean();
				bean.setStrKmkCd(rs.getString(1));
				bean.setStrKmkmei(rs.getString(2));
				bean.setStrHaitenKmk(rs.getString(3));
				bean.setStrNendo(rs.getString(4));
				bean.setIntNinzu(rs.getInt(5));
				
				// 新テストなら平均点→無効値／平均偏差値→平均点
				if (isNewExam) {
					bean.setFloHeikin(-999);
					bean.setFloHensa(rs.getFloat(6));
				} else {
					bean.setFloHeikin(rs.getFloat(6));
					bean.setFloHensa(rs.getFloat(7));
				}

				if (rs.getInt(1) >= 7000) c1.add(bean); // 型
				else c2.add(bean); // 科目
			}

			// 型・科目の順番に詰める
			data.getS21List().addAll(c1);
			data.getS21List().addAll(c2);
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("S21_01");
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.S_PREV_SCORE;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new S21().s21(data, outfileList, getAction(), sessionKey, sheetLog);
	}

}

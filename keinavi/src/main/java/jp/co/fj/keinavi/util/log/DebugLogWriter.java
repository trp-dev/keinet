package jp.co.fj.keinavi.util.log;

import java.util.logging.*;
import java.io.*;

import jp.co.fj.keinavi.util.*;

/**
 *
 * デバッグログをファイルに出力するクラス
 *
 * @Date		2004/06/09
 * @author	TOTEC)Nishiyama
 */
public class DebugLogWriter extends KNLogWriter {

	/**
	 * デバッグログレベル
	 */
	public int debugLevel = 0;
	
	/**
	 * コンストラクタ
	 */
	public DebugLogWriter() {}

	/**
	 * デバッグログの名前を取得する
	 * 
	 * @return デバッグログ名
	 */
	public String getLogName() { return "Debug"; }
	
	/**
	 * ログのファイルサイズ上限値を取得する。
	 * 
	 * @return ログのファイルサイズ上限値
	 */
	protected int getLogLimit() {
		int size = 10 * 1024 * 1024;
		try{
			size = KNCommonProperty.getDebugLogFileSizeLimit();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return size;
	}
	
	/**
	 * ログのファイル履歴の上限値を取得する。
	 * 
	 * @return ログのファイル履歴上限値
	 */
	protected int getLogCount() {
		int count = 10;
		try{
			count = KNCommonProperty.getDebugLogCountLimit();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return count;
	}
	
	/**
	 * デバッグログレベルを取得する。
	 * 
	 * @return デバッグログレベル
	 */
	public int getDebugLevel() { return debugLevel; }

	/**
	 * デバッグログを出すかどうか判定する
	 * 
	 * @param level デバッグログレベル
	 * @return 判定結果
	 */
	public boolean checkDebugLevel(int level) {

		return debugLevel > level ? false : true;
	}
	
	/**
	 * ログ出力オブジェクトをインスタンス化する
	 * 
	 * @return ログのインスタンス
	 */
	public static KNLogWriter factory() {

		try {
			KNLogWriter me = new DebugLogWriter();
			me.initLog();
			return me;
		}	catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * ログを初期化する
	 */
	protected void initLog() throws Exception {
		
		_initLog();
		debugLevel = _getDebugLevel();
	}

	/**
	 * デバッグログファイルへの出力
	 *
	 * @param rec ログレコード
	 */
	protected void output(LogRecord rec) throws Exception {

		Logger logger = getLogger();
		rec.setLoggerName(getLogName());
		logger.log(rec);
	}

	/**
	 *
	 * 出力ファイル名の取得
	 *
	 * @return 出力ファイル名
	 */
	protected String getLogFileName() {

		String FilePath = getLogFilePath();
		return new File(FilePath, "debug_%g-%u.log").getAbsolutePath();
	}

	/**
	 *
	 * 出力ファイルのパスの取得
	 *
	 * @return 出力ファイルパス
	 */
	protected String getLogFilePath() {
//		ファイルセパレータの記述を環境依存しない形に修正
//		2004.09.21 Yoshimoto KAWAI - Totec

		// ファイルパスを返す
		String FilePath = getKNLOG();
		if (FilePath == null) {
//			FilePath = "%h\\";
			FilePath = "%h" + File.separator;
		} else {
//			File target = new File(FilePath, "Log\\DLog\\");
			File target = new File(FilePath, "Log" + File.separator + "DLog" + File.separator);
			if ( (! target.exists()) || (! target.isDirectory()) ) {
//				FilePath = "%h\\";
				FilePath = "%h" + File.separator;
			} else {
				FilePath = target.getAbsolutePath();
			}
		}
		return FilePath;
	}

	/**
	 *
	 * デバッグログの設定重要度レベル値の取得
	 *
	 * @return レベル値
	 */
	private int _getDebugLevel() {

		try {
			return KNCommonProperty.getDebugLogLevel();
		}	catch (Exception e) {
			return 0;
		}
	}
}

package jp.co.fj.keinavi.data.sheet;

/**
 *
 * �l���ѕ��͂̈�������v�Z�N���X
 * 
 * 2006.08.30	[�V�K�쐬]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class IPrintNumberCalculator {

	private final PrintNumberDefinition def;
	private final String examTypeCd;
	private final int studentNumber;

	// �������
	private int printNumber;
	
	/**
	 * �R���X�g���N�^
	 * 
	 * @param pDef ���������`
	 * @param pExamTypeCd �͎���ރR�[�h
	 * @param pStudentNumber ����l��
	 */
	public IPrintNumberCalculator(final PrintNumberDefinition pDef,
			final String pExamTypeCd, final int pStudentNumber) {
		def = pDef;
		examTypeCd = pExamTypeCd;
		studentNumber = pStudentNumber;
	}

	/**
	 * @param id
	 */
	public void add(final String id) {
		final double num = def.getNumber(id, examTypeCd);
		printNumber += Math.round(num * studentNumber);
	}
	
	/**
	 * @return printNumber ��߂��܂��B
	 */
	public int getPrintNumber() {
		return printNumber;
	}
	
}

package jp.co.fj.keinavi.data;

/**
 *
 * 科目別成績（全国）データクラス
 *
 *
 * <2010年度改修>
 * 2010.04.08	Tomohisa YAMADA - Totec
 * 				新規作成
 * 
 * 
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 */
public class SubRecordAData {
	
	//模試年度	
	private String examyear;
	//模試コード	
	private String examcd;
	//科目コード	
	private String subcd;
	//現役高卒区分	
	private String studentdiv;
	
	//平均点/平均スコア	
	private double avgpnt;
	//平均偏差値	
	private double avgdeviation;
	//平均得点率	
	private double avgscorerate;
	//標準偏差	
	private double stddeviation;
	//最高点	
	private int highestpnt;
	//最低点	
	private int lowestpnt;
	//人数
	private int numbers;
	
	/**
	 * コンストラクタ
	 * @param examyear
	 * @param examcd
	 * @param subcd
	 * @param studentdiv
	 * @param avgpnt
	 * @param avgdeviation
	 * @param avgscorerate
	 * @param stddeviation
	 * @param highestpnt
	 * @param lowestpnt
	 * @param numbers
	 */
	public SubRecordAData(
			String examyear, 
			String examcd, 
			String subcd,
			String studentdiv, 
			double avgpnt, 
			double avgdeviation, 
			double avgscorerate,
			double stddeviation, 
			int highestpnt, 
			int lowestpnt, 
			int numbers) {
		this.examyear = examyear;
		this.examcd = examcd;
		this.subcd = subcd;
		this.studentdiv = studentdiv;
		this.avgpnt = avgpnt;
		this.avgdeviation = avgdeviation;
		this.avgscorerate = avgscorerate;
		this.stddeviation = stddeviation;
		this.highestpnt = highestpnt;
		this.lowestpnt = lowestpnt;
		this.numbers = numbers;
	}

	/**
	 * 模試年度を取得する
	 * @return
	 */
	public String getExamyear() {
		return examyear;
	}

	/**
	 * 模試コードを取得する
	 * @return
	 */
	public String getExamcd() {
		return examcd;
	}

	/**
	 * 科目コードを取得する
	 * @return
	 */
	public String getSubcd() {
		return subcd;
	}

	/**
	 * 現役高卒区分を取得する
	 * @return
	 */
	public String getStudentdiv() {
		return studentdiv;
	}

	/**
	 * 平均点を取得する
	 * @return
	 */
	public double getAvgpnt() {
		return avgpnt;
	}

	/**
	 * 平均偏差値を取得する
	 * @return
	 */
	public double getAvgdeviation() {
		return avgdeviation;
	}

	/**
	 * 平均得点率を取得する
	 * @return
	 */
	public double getAvgscorerate() {
		return avgscorerate;
	}

	/**
	 * 標準偏差を取得する
	 * @return
	 */
	public double getStddeviation() {
		return stddeviation;
	}

	/**
	 * 最高点を取得する
	 * @return
	 */
	public int getHighestpnt() {
		return highestpnt;
	}

	/**
	 * 最低点を取得する
	 * @return
	 */
	public int getLowestpnt() {
		return lowestpnt;
	}

	/**
	 * 人数を取得する
	 * @return
	 */
	public int getNumbers() {
		return numbers;
	}
	
}

package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.fj.keinavi.data.maintenance.KNClassData;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 対象クラスリストBean
 * 
 * 2005.10.20	[新規作成]
 * 2006.11.01	2006年度改修
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class KNClassListBean extends DefaultBean {

	// 学校コード
	private final String schoolCd;
	
	// 対象クラスデータのリスト
	private final List classList = new ArrayList();
	// 年度セット
	private final Set yearSet = new HashSet();
	// 年度リスト
	private List yearList;
	// 非表示生徒のみを取得するか
	private boolean isNotDisplayMode = false;

	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 学校コード
	 */
	public KNClassListBean(final String pSchoolCd) {
		
		if (pSchoolCd == null) {
			throw new IllegalArgumentException(
					"学校コードがNULLです。");
		}
		
		schoolCd = pSchoolCd;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = createPreparedStatement();
			rs = ps.executeQuery();
			while (rs.next()) {
				// 年度を保持する
				yearSet.add(rs.getString(1));
				// クラスデータ
				classList.add(createKNClassData(
						rs.getString(1), rs.getInt(2), rs.getString(3)));
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}

		// 年度リストを構築
		yearList = new ArrayList(yearSet);
		Collections.sort(yearList);
		Collections.reverse(yearList);
	}
	
	/**
	 * PreparedStatementを生成する
	 * 
	 * @return PreparedStatement
	 * @throws SQLException SQL例外
	 */
	protected PreparedStatement createPreparedStatement() throws SQLException {
		final PreparedStatement ps = conn.prepareStatement(createQuery());
		ps.setString(1, schoolCd);
		return ps;
	}

	// SQLを作る
	private String createQuery() throws SQLException {
		
		final Query query = QueryLoader.getInstance().load("m10");
		
		// 非表示生徒
		if (isNotDisplayMode) {
			query.append("AND bi.notdisplayflg IS NOT NULL ");
		} else {
			query.append("AND bi.notdisplayflg IS NULL ");
		}

		// GROUP BY
		query.append("GROUP BY　hi.year, hi.grade, hi.class ");
		// ORDER BY
		query.append("ORDER BY year DESC, grade_seq, class");

		return query.toString();
	}
	
	/**
	 * @param year 年度
	 * @param grade 学年
	 * @param className クラス
	 * @return クラスデータ
	 */
	protected KNClassData createKNClassData(
			final String year, final int grade, final String className) {
		return new KNClassData(year, grade, className);
	}
	
	/**
	 * @return クラスリストの最初の要素
	 */
	public KNClassData getFirstClassData() {
		if (classList.isEmpty()) {
			return null;
		} else {
			return (KNClassData) classList.get(0);
		}
	}
	
	/**
	 * 非表示生徒取得モード
	 */
	public void setNotDisplayMode() {
		isNotDisplayMode = true;
	}
	
	/**
	 * @return
	 */
	public int getSize() {
		return classList.size();
	}
	
	/**
	 * @return classList を戻します。
	 */
	public List getClassList() {
		return classList;
	}

	/**
	 * @return yearList を戻します。
	 */
	public List getYearList() {
		return yearList;
	}

	/**
	 * @return schoolCd を戻します。
	 */
	protected String getSchoolCd() {
		return schoolCd;
	}

}

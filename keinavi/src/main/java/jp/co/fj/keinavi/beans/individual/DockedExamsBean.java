/*
 * 作成日: 2004/08/18
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.ExaminationData;

import com.fjh.beans.DefaultBean;

/**
 * @author T.Yamada
 * この生徒が受けた最新の模試とそのドッキング先の模試をサイズ２の
 * ExaminationDataの配列として返します。ちなみに[0]または[1]のどちらかは
 * 必ず存在します。
 */
public class DockedExamsBean extends DefaultBean{

	private String query = "SELECT" +
		" E1.EXAMYEAR EXAMYEAR1,E1.EXAMCD EXAMCD1,E1.EXAMNAME EXAMNAME1,E1.EXAMNAME_ABBR EXAMNAME_ABBR1" +
		" ,E1.EXAMTYPECD EXAMTYPECD1,E1.EXAMST EXAMST1,E1.EXAMDIV EXAMDIV1,E1.TERGETGRADE TERGETGRADE1,E1.INPLEDATE INPLEDATE1 " +
		" ,E1.IN_DATAOPENDATE IN_DATAOPENDATE1,E1.OUT_DATAOPENDATE OUT_DATAOPENDATE1,E1.DOCKINGEXAMCD DOCKINGEXAMCD1,E1.DOCKINGTYPE DOCKINGTYPE1 " +
		" ,E1.DISPSEQUENCE DISPSEQUENCE1,E1.MASTERDIV MASTERDIV1 " +
		" ,E2.EXAMYEAR EXAMYEAR2,E2.EXAMCD EXAMCD2,E2.EXAMNAME EXAMNAME2,E2.EXAMNAME_ABBR EXAMNAME_ABBR2" +
		" ,E2.EXAMTYPECD EXAMTYPECD2,E2.EXAMST EXAMST2,E2.EXAMDIV EXAMDIV2,E2.TERGETGRADE TERGETGRADE2,E2.INPLEDATE INPLEDATE2 " +
		" ,E2.IN_DATAOPENDATE IN_DATAOPENDATE2,E2.OUT_DATAOPENDATE OUT_DATAOPENDATE2,E2.DOCKINGEXAMCD DOCKINGEXAMCD2,E2.DOCKINGTYPE DOCKINGTYPE2" +
		" ,E2.DISPSEQUENCE DISPSEQUENCE2,E2.MASTERDIV MASTERDIV2 " +
		" FROM EXAMINATION E1 LEFT JOIN EXAMINATION E2 ON E1.EXAMYEAR = E2.EXAMYEAR " +
		" AND E1.DOCKINGEXAMCD = E2.EXAMCD WHERE 1=1 ";

	/** 出力パラメター */
	private ExaminationData[] examinationDatas = new ExaminationData[3];//[0]:センター [1]:二次　[2]:選択対象模試(nullだったら受けていない)

	/** 入力パラメター */
	private ExamSession examSession;//使用可能な模試
	private String examYear;//元になる対象模試年度
	private String examCd;//元になる対象模試コード

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception{
		
		ExaminationData one = new ExaminationData();
		ExaminationData two = new ExaminationData();
		
		if(examYear != null && examCd != null)
			query += " AND (E1.EXAMYEAR = '"+examYear+"' AND E1.EXAMCD = '"+examCd+"')";

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{	
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();

			while(rs.next()){
				one.setExamYear(rs.getString("EXAMYEAR1"));
				one.setExamCd(rs.getString("EXAMCD1"));
				one.setExamName(rs.getString("EXAMNAME1"));
				one.setExamNameAbbr(rs.getString("EXAMNAME_ABBR1"));
				one.setExamTypeCd(rs.getString("EXAMTYPECD1"));
				one.setInpleDate(rs.getString("INPLEDATE1"));
				two.setExamYear(rs.getString("EXAMYEAR2"));
				two.setExamCd(rs.getString("EXAMCD2"));
				two.setExamName(rs.getString("EXAMNAME2"));
				two.setExamNameAbbr(rs.getString("EXAMNAME_ABBR2"));
				two.setExamTypeCd(rs.getString("EXAMTYPECD2"));
				two.setInpleDate(rs.getString("INPLEDATE2"));
			}
			
			//もしtwo：ドッキング先の模試が使えない模試ならnullにする(元は既に使える)
			boolean isValidExam = false;
			for (Iterator it=((Map)examSession.getExamMap()).entrySet().iterator(); it.hasNext(); ) {
				Map.Entry entry = (Map.Entry)it.next();
				List examDatas = (List)entry.getValue();
				for (Iterator its=examDatas.iterator(); its.hasNext();) {
					ExamData examData = (ExamData)its.next();
					if(two.getExamYear() != null && two.getExamCd() != null){
						if(examData.getExamYear().equals(two.getExamYear()) && examData.getExamCD().equals(two.getExamCd())){
							isValidExam = true;
						}
					}
				}
			}
			//examinationDatas[2] = one;//元のデータ
			if(!isValidExam){
				two = null;
			}
			if(!one.getExamTypeCd().equals("01")){//スイッチする→[0]にはマーク模試[1]には記述模試
				examinationDatas[0] = two;
				examinationDatas[1] = one;	
			}else{
				examinationDatas[0] = one;
				examinationDatas[1] = two;
			}
			
		}catch(SQLException sqle){
			sqle.printStackTrace();
			throw sqle;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null)	rs.close();
		}
	}
	/**
	 * @return
	 */
	public ExaminationData[] getExaminationDatas() {
		return examinationDatas;
	}

	/**
	 * @return
	 */
	public ExamSession getExamSession() {
		return examSession;
	}

	/**
	 * @param datas
	 */
	public void setExaminationDatas(ExaminationData[] datas) {
		examinationDatas = datas;
	}

	/**
	 * @param session
	 */
	public void setExamSession(ExamSession session) {
		examSession = session;
	}

	/**
	 * @return
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @param string
	 */
	public void setExamCd(String string) {
		examCd = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

}

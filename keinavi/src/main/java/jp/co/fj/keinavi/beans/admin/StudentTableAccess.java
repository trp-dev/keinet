/*
 * 作成日: 2004/09/28
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;

import org.apache.commons.dbutils.DbUtils;

/**
 * @author 奥村ゆかり
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class StudentTableAccess {

	/**
	 *
	 * 学籍基本情報にINSERT
	 *
	 * @param con DBコネクション
	 * @param keys [0]年度２桁 [1]学校コード [2]在卒校コード [3]年度 [4]学年 [5]クラス [6]クラス番号
	 * 				[7]性別 [8]かな名前 [9]漢字名 [10]生年月日（年） [11]生年月日（月） [12]生年月日（日）
	 * 				[13]電話番号 [14]前年度
	 * @return judge 再集計対象ならtrueを返す
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static boolean InsBasicInfo(
		Connection con,
		HashMap keys)
		throws SQLException {
		String key[] = new String[18];
		key[0] = (String) keys.get("YEAR02");
		key[1] = (String) keys.get("SCHOOLCD");
		key[2] = (String) keys.get("HOMESCHOOLCD");
		key[3] = (String) keys.get("EXAMYEAR");
		key[4] = (String) keys.get("GRADE");
		key[5] = (String) keys.get("CLASS");
		key[6] = (String) keys.get("CLASS_NO");
		key[7] = (String) keys.get("SEX");
		key[8] = (String) keys.get("NAME_KANA");
		key[9] = (String) keys.get("NAME_KANJI");
		key[10] = (String) keys.get("BIRTHYEAR");
		key[11] = (String) keys.get("BIRTHMONTH");
		key[12] = (String) keys.get("BIRTHDATE");
		key[13] = (String) keys.get("TEL_NO");
		key[14] = (String) keys.get("ANSWERSHEETNO");
		key[15] = (String) keys.get("INDIVIDUALID");
		key[16] = (String) keys.get("EXAM");
		key[17] = (String) keys.get("CUSTOMER_NO");
		return InsBasicInfo(con, key);
	}

	/**
	 *
	 * 学籍基本情報にINSERT
	 *
	 * @param con DBコネクション
	 * @param keys [0]年度２桁 [1]学校コード [2]在卒校コード [3]年度 [4]学年 [5]クラス [6]クラス番号
	 * 				[7]性別 [8]かな名前 [9]漢字名 [10]生年月日（年） [11]生年月日（月） [12]生年月日（日）
	 * 				[13]電話番号 [14]前年度 [15]個人ID
	 * @return judge 再集計対象ならtrueを返す
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static boolean InsBasicInfo(
		Connection con,
		String keys[])
		throws SQLException {
		boolean judge = false;
		int count = 0;
		PreparedStatement stmt = null;
		String query = null;

		query =
				"INSERT /*+ APPEND */ INTO BASICINFO"
					+ " (INDIVIDUALID,SCHOOLCD,HOMESCHOOLCD,NAME_KANA,"
					+ " NAME_KANJI,SEX,BIRTHDAY,TEL_NO,CUSTOMER_NO) "
					+ " VALUES( ?,?,?,?,?,?,?,?,?)";
		try {
			stmt = con.prepareStatement(query);
			stmt.setString(1, keys[15]);
			stmt.setString(2, keys[1]);
			stmt.setString(3, keys[2]);
			stmt.setString(4, keys[8]);
			stmt.setString(5, keys[9]);
			stmt.setString(6, keys[7]);
			stmt.setString(7, keys[10] + keys[11] + keys[12]);
			stmt.setString(8, keys[13].trim());
			stmt.setString(9, keys[17]);
			count = stmt.executeUpdate();
			if (count == 0)
				throw new SQLException("学籍基本情報のINSERTに失敗しました。【0件登録】");
			judge = true;
		} catch (SQLException ex) {
			throw new SQLException(String.format("学籍基本情報のINSERTに失敗しました。【%s】", ex.getMessage()));
		} finally {
			if (stmt != null)
				stmt.close();
		}

		return judge;
	}

	/**
	 *
	 * 学籍基本情報にUPDATE
	 *
	 * @param con DBコネクション
	 * @param keys [0]年度２桁 [1]学校コード [2]在卒校コード [3]年度 [4]学年 [5]クラス [6]クラス番号
	 * 				[7]性別 [8]かな名前 [9]漢字名 [10]生年月日（年） [11]生年月日（月） [12]生年月日（日）
	 * 				[13]電話番号 [14]前年度[15]個人ID
	 * @return judge 成功ならtrueを返す
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java
	 * */
	public static boolean UpdBasicInfo(Connection con, HashMap keys)
		throws SQLException {
		String key[] = new String[17];
		key[0] = (String) keys.get("YEAR02");
		key[1] = (String) keys.get("SCHOOLCD");
		key[2] = (String) keys.get("HOMESCHOOLCD");
		key[3] = (String) keys.get("EXAMYEAR");
		key[4] = (String) keys.get("GRADE");
		key[5] = (String) keys.get("CLASS");
		key[6] = (String) keys.get("CLASS_NO");
		key[7] = (String) keys.get("SEX");
		key[8] = (String) keys.get("NAME_KANA");
		key[9] = (String) keys.get("NAME_KANJI");
		key[10] = (String) keys.get("BIRTHYEAR");
		key[11] = (String) keys.get("BIRTHMONTH");
		key[12] = (String) keys.get("BIRTHDATE");
		key[13] = (String) keys.get("TEL_NO");
		key[14] = (String) keys.get("ANSWERSHEETNO");
		key[15] = (String) keys.get("INDIVIDUALID");
		key[16] = (String) keys.get("EXAM");
		return UpdBasicInfo(con, key);
	}


	/**
	 *
	 * 学籍基本情報にUPDATE
	 *
	 * @param con DBコネクション
	 * @param keys [0]年度２桁 [1]学校コード [2]在卒校コード [3]年度 [4]学年 [5]クラス [6]クラス番号
	 * 				[7]性別 [8]かな名前 [9]漢字名 [10]生年月日（年） [11]生年月日（月） [12]生年月日（日）
	 * 				[13]電話番号 [14]前年度[15]個人ID
	 * @return judge 再集計対象ならtrueを返す
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static boolean UpdBasicInfo(Connection con, String keys[])
		throws SQLException {
		boolean judge = false;
		int count = 0;
		PreparedStatement stmt = null;
		String query = null;

		query =
			"UPDATE BASICINFO"
				+ " SET NAME_KANA=?, NAME_KANJI=?, TEL_NO=?,  SEX=? , BIRTHDAY=?"
				+ " WHERE INDIVIDUALID=?";
		try {
			stmt = con.prepareStatement(query);
			stmt.setString(1, keys[8]);
			stmt.setString(2, keys[9]);
			stmt.setString(3, keys[13].trim());
			stmt.setString(4, keys[7]);
			stmt.setString(5, keys[10] + keys[11] + keys[12]);
			stmt.setString(6, keys[15]);
			count = stmt.executeUpdate();
			if (count == 0)
				throw new SQLException("学籍基本情報のUPDATEに失敗しました。");
			judge = true;

		} catch (SQLException ex) {
			throw new SQLException(String.format("学籍基本情報のUPDATEに失敗しました。【%s】", ex.getMessage()));

		} finally {
			if (stmt != null) stmt.close();
		}

		return judge;
	}

	/**
	 *
	 * 学籍履歴情報にINSERT
	 *
	 * @param con DBコネクション
	 * @param keys [0]年度２桁 [1]学校コード [2]在卒校コード [3]年度 [4]学年 [5]クラス [6]クラス番号
	 * 				[7]性別 [8]かな名前 [9]漢字名 [10]生年月日（年） [11]生年月日（月） [12]生年月日（日）
	 * 				[13]電話番号 [14]前年度 [15]個人ID
	 * @return judge 成功ならtrueを返す
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static boolean InsHistoryInfo(Connection con, HashMap keys)
		throws SQLException {
		String key[] = new String[17];
		key[0] = (String) keys.get("YEAR02");
		key[1] = (String) keys.get("SCHOOLCD");
		key[2] = (String) keys.get("HOMESCHOOLCD");
		key[3] = (String) keys.get("EXAMYEAR");
		key[4] = (String) keys.get("GRADE");
		key[5] = (String) keys.get("CLASS");
		key[6] = (String) keys.get("CLASS_NO");
		key[7] = (String) keys.get("SEX");
		key[8] = (String) keys.get("NAME_KANA");
		key[9] = (String) keys.get("NAME_KANJI");
		key[10] = (String) keys.get("BIRTHYEAR");
		key[11] = (String) keys.get("BIRTHMONTH");
		key[12] = (String) keys.get("BIRTHDATE");
		key[13] = (String) keys.get("TEL_NO");
		key[14] = (String) keys.get("ANSWERSHEETNO");
		key[15] = (String) keys.get("INDIVIDUALID");
		key[16] = (String) keys.get("EXAM");
		return InsHistoryInfo(con, key);
	}

	/**
	 *
	 * 学籍履歴情報にINSERT
	 *
	 * @param con DBコネクション
	 * @param keys [0]年度２桁 [1]学校コード [2]在卒校コード [3]年度 [4]学年 [5]クラス [6]クラス番号
	 * 				[7]性別 [8]かな名前 [9]漢字名 [10]生年月日（年） [11]生年月日（月） [12]生年月日（日）
	 * 				[13]電話番号 [14]前年度 [15]個人ID
	 * @return judge 成功ならtrueを返す
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static boolean InsHistoryInfo(Connection con, String keys[])
		throws SQLException {
		boolean judge = false;
		int count = 0;
		PreparedStatement stmt = null;
		String query =
			"INSERT /*+ APPEND */ INTO HISTORYINFO (INDIVIDUALID,YEAR,GRADE,CLASS,"
				+ " CLASS_NO,STUDENT_NO,ATTENDGRADE,REGION,SCHOOLHOUSE)"
				+ " VALUES(?,?,?,?,?,null,null,null,null)";
		try {
			//System.out.println(query+keys[15]+" "+keys[3]+" "+keys[4]+" "+keys[5]+" "+keys[6]);
			stmt = con.prepareStatement(query);
			stmt.setString(1, keys[15]);
			stmt.setString(2, keys[3]);
			stmt.setInt(3, Integer.parseInt(keys[4]));
			stmt.setString(4, keys[5]);
			stmt.setString(5, keys[6]);

			count = stmt.executeUpdate();

			if (count == 0)
				throw new SQLException("学籍履歴情報のINSERTに失敗しました。【0件登録】");
			judge = true;

		} catch (SQLException ex) {
			throw new SQLException(String.format("学籍履歴情報のINSERTに失敗しました。【%s】", ex.getMessage()));

		} finally {
			if (stmt != null) stmt.close();
		}
		return judge;
	}


	/**
	 *
	 * 学籍履歴情報にUPDATE
	 *
	 * @param con DBコネクション
	 * @param year 年度, grade 学年, clasクラス, classNumberクラス番号,individualId 個人ID
	 * @return judge 再集計対象ならtrueを返す
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static boolean UpdHistoryInfo(
		Connection con,
		String year,
		String grade,
		String clas,
		String classNumber,
		String individualId)
		throws SQLException, Exception {
		boolean judge = false;
		int count = 0;
		PreparedStatement stmt = null;
		String query =
			"UPDATE HISTORYINFO"
				+ " SET YEAR=?,GRADE=?,"
				+ " CLASS=UPPER(?),CLASS_NO=?"
				+ " WHERE INDIVIDUALID=? AND YEAR=?";
		try {
			stmt = con.prepareStatement(query);
			stmt.setString(1, year);
			stmt.setInt(2, Integer.parseInt(grade));
			stmt.setString(3, clas);
			stmt.setString(4, classNumber);
			stmt.setString(5, individualId);
			stmt.setString(6, year);
			count = stmt.executeUpdate();
			if (count == 0)
				throw new SQLException("学籍履歴情報のUPDATEに失敗しました。【0件登録】");
			judge = true;
		} catch (SQLException ex) {
			throw new SQLException(String.format("学籍履歴情報のUPDATEに失敗しました。【%s】", ex.getMessage()));
		} finally {
			if (stmt != null)
				stmt.close();
		}
		return judge;
	}

	/**
	 *
	 * 個人IDの取得を行う
	 *
	 * @param con DBコネクション
	 * @return String
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static String GetIndividualID(Connection con) throws SQLException {

		String query = "SELECT TO_CHAR(S_IDCOUNT.NEXTVAL, 'FM00000000') FROM DUAL";

		String id = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				id = rs.getString(1);
			} else {
				throw new SQLException("個人IDの取得に失敗しました。");
			}

		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}

		return id;
	}

	/**
	 *
	 * 学籍履歴情報に対象年度、学年、クラス、クラス番号のデータが存在するかどうか
	 *
	 * @param con DBコネクション
	 * @param year 年度、grade 年度、clas クラス、classNumber クラス番号
	 * @return count
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static boolean IsHistoryCount(
		Connection con,
		String year,
		String schoolCD,
		String grade,
		String clas,
		String classNumber,
		String individualId)
		throws SQLException {
		int count = 0;
		boolean judge = false;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String query =
			"SELECT Count(*) COUNT FROM BASICINFO b, HISTORYINFO h"
				+ " WHERE b.INDIVIDUALID=h.INDIVIDUALID AND b.SCHOOLCD=? AND h.YEAR=? AND h.GRADE=? AND UPPER(h.CLASS)=UPPER(?) AND h.CLASS_NO=? AND h.INDIVIDUALID NOT IN( ?)";
//			"SELECT Count(*) COUNT FROM HISTORYINFO"
//				+ " WHERE YEAR=? AND GRADE=? AND UPPER(CLASS)=UPPER(?) AND CLASS_NO=? AND INDIVIDUALID NOT IN( ?)";
		try {
			stmt = con.prepareStatement(query);
			stmt.setString(1, schoolCD);
			stmt.setString(2, year);
			stmt.setInt(3, Integer.parseInt(grade));
			stmt.setString(4, clas);
			stmt.setString(5, classNumber);
			stmt.setString(6, individualId);
			rs = stmt.executeQuery();
			while (rs.next()) {
				count = rs.getInt("COUNT");
			}
			if (count != 0)
				judge = true;

		} catch (SQLException ex) {

			throw new SQLException("学籍履歴情報のカウントに失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return judge;
	}

	/**
	 *
	 * 年度、模試コード、解答用紙番号で条件付けた受験情報を取得する。
	 *
	 * @param con DBコネクション
	 * @param targetYear 対象年度  targetExam 対象模試コード　answerSheetNo 解答用紙番号
	 * @return list レコード
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public static LinkedList SelExamStudentInf(
		Connection con,
		String targetYear,
		String targetExam,
		String answerSheetNo)
		throws SQLException {

			LinkedList list = new LinkedList();
			PreparedStatement stmt = null;
			ResultSet rs = null;
			String query =
				" SELECT"+
				" S_INDIVIDUALID S_INDIVIDUALID,"+
				" EXAMYEAR IYEAR,"+
				" EXAMCD IEXAMCD,"+
				" GRADE IGRADE,"+
				" CLASS ICLASS,"+
				" CLASS_NO ICLASS_NO,"+
				" HOMESCHOOLCD IHOMESCHOOLCD,"+
				" BUNDLECD IBUNDLECD,"+
				" TRIM(NAME_KANA) INAME_KANA,"+
				" TRIM(NAME_KANJI) INAME_KANJI,"+
				" SEX ISEX,"+
				" TEL_NO ITEL_NO,"+
				" TRIM(BIRTHDAY) IBIRTHDAY,"+
				" ANSWERSHEET_NO IANSWERSHEET_NO,"+
				" CUSTOMER_NO ICUSTOMER_NO"+
				" FROM"+
				" INDIVIDUALRECORD"+
				" WHERE"+
				" EXAMYEAR=? AND"+
				" EXAMCD=? AND"+
				" ANSWERSHEET_NO=?";

			try {
				stmt = con.prepareStatement(query);
				stmt.setString(1, targetYear);
				stmt.setString(2, targetExam);
				stmt.setString(3, answerSheetNo);

				rs = stmt.executeQuery();
				while (rs.next()) {
					HashMap data = new HashMap();
					data.put("S_INDIVIDUALID", rs.getString("S_INDIVIDUALID"));
					data.put("IYEAR", rs.getString("IYEAR"));
					data.put("IEXAMCD", rs.getString("IEXAMCD"));
					data.put("IGRADE", rs.getString("IGRADE"));
					data.put("ICLASS", rs.getString("ICLASS"));
					data.put("ICLASS_NO", rs.getString("ICLASS_NO"));
					data.put("IHOMESCHOOLCD", rs.getString("IHOMESCHOOLCD"));
					data.put("IBUNDLECD", rs.getString("IBUNDLECD"));
					data.put("INAME_KANA", rs.getString("INAME_KANA"));
					data.put("INAME_KANJI", rs.getString("INAME_KANJI"));
					data.put("ISEX", rs.getString("ISEX"));
					data.put("ITEL_NO", rs.getString("ITEL_NO"));
					data.put("IBIRTHDAY", rs.getString("IBIRTHDAY"));
					data.put("IANSWERSHEET_NO", rs.getString("IANSWERSHEET_NO"));
					data.put("ICUSTOMER_NO", rs.getString("ICUSTOMER_NO"));
					list.add(data);
				}

			} catch (Exception ex) {
				ex.printStackTrace();
				throw new SQLException("受験情報の取得に失敗しました。");

			} finally {
				DbUtils.closeQuietly(rs);
				DbUtils.closeQuietly(stmt);
			}

			return list;
	}

}

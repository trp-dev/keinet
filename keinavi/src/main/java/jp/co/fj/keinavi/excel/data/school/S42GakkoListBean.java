package jp.co.fj.keinavi.excel.data.school;

//import org.apache.struts.action.ActionForm;
import java.util.ArrayList;
/**
 * �Z�����ѕ��́|���Z��r�|�΍��l���z �w�Z���X�g
 * �쐬��: 2004/07/19
 * @author	A.Iwata
 */
public class S42GakkoListBean {
	//�w�Z��
	private String strGakkomei = "";
	//���Z�p�O���t�\���t���O
	private int intDispGakkoFlg = 0;
	//�͋[�N�x
	private String strNendo = "";
	//�l��
	private int intNinzu = 0;
	//���ϓ_
	private float floHeikin = 0;
	//���ϕ΍��l
	private float floHensa = 0;
	//�΍��l���z���X�g
	private ArrayList s42BnpList = new ArrayList();

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public int getIntDispGakkoFlg() {
		return this.intDispGakkoFlg;
	}
	public String getStrNendo() {
		return this.strNendo;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public float getFloHeikin() {
		return this.floHeikin;
	}
	public float getFloHensa() {
		return this.floHensa;
	}
	public ArrayList getS42BnpList() {
		return this.s42BnpList;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setIntDispGakkoFlg(int intDispGakkoFlg) {
		this.intDispGakkoFlg = intDispGakkoFlg;
	}
	public void setStrNendo(String strNendo) {
		this.strNendo = strNendo;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setFloHeikin(float floHeikin) {
		this.floHeikin = floHeikin;
	}
	public void setFloHensa(float floHensa) {
		this.floHensa = floHensa;
	}
	public void setS42BnpList(ArrayList s42BnpList) {
		this.s42BnpList = s42BnpList;
	}
}

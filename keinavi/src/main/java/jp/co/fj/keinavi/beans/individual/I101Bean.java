package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.data.individual.IExamData;
import jp.co.fj.keinavi.data.individual.StudentData;
import jp.co.fj.keinavi.data.individual.SubRecordIData;
import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.util.KNUtil;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 個人成績表（科目別成績）専用検索Bean
 *
 *
 * @author TOTEC)KONDO.Keisuke
 * @author TOTEC)YAMADA.Tomoshisa
 * @author TOTEC)URAKAWA.Fujito
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class I101Bean extends DefaultBean {

    private static final long serialVersionUID = -8816294105466081042L;

    private String subRecordSql;

    private StudentData studentData = new StudentData();

    //IN
    private String individualId;
    private String targetExamYear;
    private String targetExamCode;
    private String grade;
    private String examType;
    private boolean helpflg;
    private String classYear;//[2] add

    /**
     * 初回アクセス
     * @param grade
     * @param individualId
     * @param targetExamYear
     * @param targetExamCode
     */
    public void setSearchCondition(String individualId, String targetExamYear, String targetExamCode, boolean targetHelpflg){
        //setSearchCondition(individualId, targetExamYear, targetExamCode, getGrade(), getExamType() ,isHelpflg());//[1] delete
        setSearchCondition(individualId, targetExamYear, targetExamCode, getGrade(), getExamType() ,targetHelpflg);//[1] add
    }
    /**
     * 二回目以降アクセス
     * @param individualId
     * @param targetExamYear
     * @param targetExamCode
     * @param grade
     * @param examType
     */
    public void setSearchCondition(String individualId, String targetExamYear, String targetExamCode, String grade, String examType, boolean helpflg){
        setIndividualId(individualId);
        setTargetExamYear(targetExamYear);
        setTargetExamCode(targetExamCode);
        setGrade(grade);
        setExamType(examType);
        setHelpflg(helpflg);

        appendSubRecordSql(getSubRecordSqlBase());
        appendSubRecordSql(getPersonCondition(getIndividualId()));
        if(getGrade().equals("1"))
            appendSubRecordSql(getCurrentYearCondition());
        if(getExamType().equals("2"))
            appendSubRecordSql(getSameTypeCondition(getTargetExamYear(), getTargetExamCode()));
        appendSubRecordSql(getPrevExamCondition(getTargetExamYear(), getTargetExamCode()));

        appendSubRecordSql(getOrderBy(helpflg));
    }

    /* (非 Javadoc)
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws SQLException, Exception {
        studentData.setExaminationDatas(execSubRecord());
    }

    /**
     * @return
     * @throws Exception
     */
    private List execSubRecord() throws Exception{

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        //2019/09/11 QQ)Oosaki 共通テスト対応 ADD START
        PreparedStatement pstmt1 = null;
        ResultSet rs1 = null;
        String japWriScore = "";
        String cefr = "";
        int excd = 0;
        //2019/09/11 QQ)Oosaki 共通テスト対応 ADD END
        try{

            pstmt = conn.prepareStatement(getSubRecordSql());
            pstmt.setString(1, individualId);

            rs = pstmt.executeQuery();

            final List exams = new ArrayList();

            //ループキー
            IExamData exam = null;
            //第一解答科目の成績
            SubRecordIData rikaSeiseki = null;
            SubRecordIData chikouSeiseki = null;
            boolean ans1stflg = false;

            while (rs.next()) {

                //初回、または前回と異なる試験
                if (exam == null
                        || !(exam.getExamCd() + exam.getExamYear()).equals(
                                rs.getString("examcd") + rs.getString("examyear"))) {

                    //新規に試験を作成
                    exam = new IExamData();

                    exam.setSubRecordDatas(new ArrayList());

                    exam.setExamYear(rs.getString("examyear"));
                    exam.setExamCd(rs.getString("examcd"));
                    exam.setExamName(rs.getString("examname"));
                    exam.setExamTypeCd(rs.getString("examtypecd"));
                    exam.setDispSequence(rs.getString("dispsequence"));
                    exam.setOutDataOpenData(rs.getString("out_dataopendate"));

                    //リストに追加
                    exams.add(exam);

                    rikaSeiseki = null;
                    chikouSeiseki = null;
                }

                //成績作成
                final SubRecordIData record = new SubRecordIData();

                record.setSubCd(null2Empty(rs.getString("subcd")));
                //2019/09/11 QQ)Oosaki 共通テスト対応 UPD START
                //record.setScore(null2Empty(rs.getString("score")));
                if(("3915").equals(record.getSubCd())) {
                    if(("01").equals(exam.getExamTypeCd())) {
                        pstmt1 = conn.prepareStatement(getKokugoDescIsqlBase());
                        pstmt1.setString(1, individualId);
                        pstmt1.setString(2, rs.getString("examyear"));
                        pstmt1.setString(3, rs.getString("examcd"));
                        pstmt1.setString(4, rs.getString("subcd"));
                        pstmt1.setString(5, rs.getString("bundlecd"));

                        rs1 = pstmt1.executeQuery();
                        while(rs1.next()) {
                        japWriScore = rs1.getString("Q1") + rs1.getString("Q2") + rs1.getString("Q3") + rs1.getString("COMP");
                        record.setScore(null2Empty(japWriScore));
                        }
                    }
                } else {
                    record.setScore(null2Empty(rs.getString("score")));
                }
                //2019/09/11 QQ)Oosaki 共通テスト対応 UPD END

                // 換算得点に*を表示
                // マーク模試の場合のみ、換算得点が第１解答科目＜第２解答科目の場合に*表示
                ans1stflg = false;
                if (rs.getString("cvsscore") != null) {
                    if (KNUtil.isAns1st(exam.getExamCd())){
                        String course = rs.getString("subcd").substring(0,1);
                        if (course.equals("4") && rs.getInt("scope") == 0 && !"1".equals(rs.getString("basicflg"))){
                            if (rikaSeiseki != null){
                                if (rikaSeiseki.getCvsScore() != null){
                                    if (Integer.parseInt(rikaSeiseki.getCvsScore()) < Integer.parseInt(rs.getString("cvsscore"))){
                                        ans1stflg = true;
                                    }
                                }
                            }
                        }else if (course.equals("5") && rs.getInt("scope") == 0){
                            if (chikouSeiseki != null){
                                if (chikouSeiseki.getCvsScore() != null){
                                    if (Integer.parseInt(chikouSeiseki.getCvsScore()) < Integer.parseInt(rs.getString("cvsscore"))){
                                        ans1stflg = true;
                                    }
                                }
                            }
                        }
                    }
                }
                if (ans1stflg) {
                    record.setCvsScore("*" + null2Empty(rs.getString("cvsscore")));
                }else {
                    record.setCvsScore(null2Empty(rs.getString("cvsscore")));
                }

                record.setCDeviation(nullToEmptyF(rs.getString("a_deviation")));
                record.setScholarlevel(null2Empty(rs.getString("academiclevel")));
                record.setSubName(null2Empty(rs.getString("subname")));
                record.setSubAllotPnt(null2Empty(rs.getString("suballotpnt")));
                record.setBasicFlg(rs.getString("basicflg"));

                //受験学力測定のときは、センター到達エリアを設定する
                if (KNUtil.isAbilityExam(exam.toExamData())) {
                    record.setLevel(KNUtil.deviationToCenterReachArea(rs.getDouble("a_deviation")));
                }

                // 2019/12/02 QQ)Ooseto 英語認定試験延期対応 ADD START
                if(("3915").equals(record.getSubCd())) {
                    record.setSubAllotPnt(null2Empty(null));
                    record.setCDeviation(nullToEmptyF(null));
                    record.setScholarlevel(null2Empty(null));
                    record.setCvsScore(null2Empty(null));
                }
                // 2019/12/02 QQ)Ooseto 英語認定試験延期対応 ADD END

                //現在の試験に成績を追加
                exam.getSubRecordDatas().add(record);

                // 第１解答科目保存（マーク模試のみ）
                if (KNUtil.isAns1st(exam.getExamCd())){
                    String course = rs.getString("subcd").substring(0,1);
                    if (course.equals("4") && rs.getInt("scope") == 1){
                        rikaSeiseki = record;
                    }else if (course.equals("5") && rs.getInt("scope") == 1){
                        chikouSeiseki = record;
                    }
                }
                // 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START
                //2019/09/11 QQ)Oosaki 共通テスト対応 ADD START
                //CEFRレベル取得
//                if(excd != rs.getInt("examcd")) {
//                    cefr = cm.cefr(rs.getString("examyear"), rs.getString("examcd"), individualId, conn);
//                    record.setCefr(cefr);
//                    excd = rs.getInt("examcd");
//                }
                //2019/09/11 QQ)Oosaki 共通テスト対応 ADD END
                // 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END
            }

            return exams;

        } finally {
            DbUtils.closeQuietly(null, pstmt, rs);
            // 2019/10/16 QQ)Ooseto 共通テスト対応 ADD START
            DbUtils.closeQuietly(null, pstmt1, rs1);
            // 2019/10/16 QQ)Ooseto 共通テスト対応 ADD START
        }

    }

    /**
     * 表示順文字列を作成して返す
     * @param isHelpDesk ヘルプデスクかどうか
     * @return 表示順文字列
     */
    private String getOrderBy(boolean isHelpDesk) {

        final StringBuffer buf = new StringBuffer();

        buf.append(" ORDER BY");
        buf.append(" examination.inpledate DESC,");
        buf.append(" examination.examcd DESC,");
        buf.append(" basicflg ASC,");
        buf.append(" case when SUBRECORD_I.EXAMCD in ('01','02','03','04','38') then");
        buf.append("     case when SUBRECORD_I.SCOPE=9 or SUBRECORD_I.SCOPE is null then EXAMSUBJECT.DISPSEQUENCE");
        buf.append("     else case substr(SUBRECORD_I.SUBCD,1,1) when '4' then 4000");
        buf.append("                                             when '5' then 5000");
        buf.append("          else EXAMSUBJECT.DISPSEQUENCE");
        buf.append("          end");
        buf.append("     end");
        buf.append(" else EXAMSUBJECT.DISPSEQUENCE");
        buf.append(" end ASC,");
        buf.append(" SUBRECORD_I.SCOPE desc");

        return buf.toString();
    }

    /**
     * 過回模試に絞り込む条件
     * 過回判断は試験実地日ではなく外部データ開放日
     * @param examYear
     * @param examCode
     * @return
     */
    private String getPrevExamCondition(String examYear, String examCode){
        /*
        String sqlOption = ""+
            " AND EXAMINATION.INPLEDATE <="+
            " (SELECT INPLEDATE FROM EXAMINATION WHERE EXAMYEAR = '"+examYear+"' AND EXAMCD = '"+examCode+"')";
        return sqlOption;
        */
        /**/
        String sqlOption = "";
        if(helpflg) {
            sqlOption = " AND EXAMINATION.IN_DATAOPENDATE <="+
            " (SELECT IN_DATAOPENDATE FROM EXAMINATION WHERE EXAMYEAR = '"+examYear+"' AND EXAMCD = '"+examCode+"')";
        } else {
            sqlOption = " AND EXAMINATION.OUT_DATAOPENDATE <="+
            " (SELECT OUT_DATAOPENDATE FROM EXAMINATION WHERE EXAMYEAR = '"+examYear+"' AND EXAMCD = '"+examCode+"')";
        }
        return sqlOption;
        /**/
    }
    private String getSameTypeCondition(String examYear, String examCode){
        String sqlOption = " AND EXAMINATION.EXAMTYPECD = "+
                        " (SELECT EXAMTYPECD FROM EXAMINATION WHERE EXAMYEAR = '"+examYear+"' AND EXAMCD = '"+examCode+"')";
        return sqlOption;
    }
    private String getPersonCondition(String id){
        String sqlOption = " AND SUBRECORD_I.INDIVIDUALID = ?";
        return sqlOption;
    }
    private String getCurrentYearCondition(){
        //String sqlOption = " AND SUBRECORD_I.EXAMYEAR = (SELECT MAX(EXAMYEAR) FROM EXAMINATION)";//[2] delete
        String sqlOption = " AND SUBRECORD_I.EXAMYEAR = " + classYear + " ";//[2] add
        return sqlOption;
    }

    /**
     * StringをFloatのフォーマットで返します。
     * @return
     */
    public String nullToEmptyF(String str) {
        if(str == null) {
            return "";
        } else{
                return String.valueOf(new Float(str).floatValue());
        }
    }

    /**
     * nullを空白にする
     * @return
     */
    private String null2Empty(String input){
        String rt = "";
        if(input != null) rt = input;
        return rt;
    }

    /**
     *
     * @return
     */
    private String getSubRecordSqlBase(){
        String DATAOPENDATE = null;
        if(isHelpflg()) {
            DATAOPENDATE = "IN_DATAOPENDATE";
        } else {
            DATAOPENDATE = "OUT_DATAOPENDATE";
        }
        String sql = ""+
        " SELECT"+
        " /*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */"+
        " EXAMINATION.EXAMYEAR"+
        " ,EXAMINATION.EXAMCD"+
        " ,EXAMINATION.EXAMNAME"+
        " ,EXAMINATION.EXAMTYPECD"+
        " ,EXAMSUBJECT.SUBCD"+
        " ,SUBRECORD_I.SCORE"+
        " ,SUBRECORD_I.CVSSCORE"+
        " ,SUBRECORD_I.A_DEVIATION"+
        " ,SUBRECORD_I.ACADEMICLEVEL"+
        " ,EXAMSUBJECT.SUBNAME"+
        " ,EXAMSUBJECT.SUBALLOTPNT"+
        " ,EXAMINATION.INPLEDATE"+
        " ,EXAMINATION.DISPSEQUENCE"+
        " ," + DATAOPENDATE + " OUT_DATAOPENDATE "+
        " ,NVL(SUBRECORD_I.SCOPE,-999) SCOPE "+
        " ,GETCHARFLAGVALUE(EXAMSUBJECT.NOTDISPLAYFLG, 5) BASICFLG "+
        //2019/09/11 QQ)Oosaki 共通テスト対応 ADD START
        " ,SUBRECORD_I.BUNDLECD"+
        //2019/09/11 QQ)Oosaki 共通テスト対応 ADD END
        " FROM SUBRECORD_I"+
        " INNER JOIN EXAMINATION ON SUBRECORD_I.EXAMYEAR = EXAMINATION.EXAMYEAR"+
        " AND SUBRECORD_I.EXAMCD = EXAMINATION.EXAMCD"+
        " INNER JOIN EXAMSUBJECT ON SUBRECORD_I.SUBCD = EXAMSUBJECT.SUBCD"+
        " AND SUBRECORD_I.EXAMYEAR = EXAMSUBJECT.EXAMYEAR"+
        " AND SUBRECORD_I.EXAMCD = EXAMSUBJECT.EXAMCD"+
        " AND GETCHARFLAGVALUE(EXAMSUBJECT.NOTDISPLAYFLG, 3) = '0' "+
        " WHERE 1 = 1";
        return sql;
    }

   //2019/09/11 QQ)Oosaki 共通テスト対応 ADD START
   /**
    * 国語記述式評価得点を取得する
    * @return
    */
   private String getKokugoDescIsqlBase(){
       String sql = ""+
       " SELECT"+
       " TRIM(KDI.KOKUGO_DESC_Q1_RATING) Q1"+
       " ,TRIM(KDI.KOKUGO_DESC_Q2_RATING) Q2"+
       " ,TRIM(KDI.KOKUGO_DESC_Q3_RATING) Q3"+
       " ,TRIM(KDI.KOKUGO_DESC_COMP_RATING) COMP"+
       " FROM KOKUGO_DESC_I KDI"+
       " WHERE "+
       " KDI.INDIVIDUALID = ?"+
       " AND KDI.EXAMYEAR = ?"+
       " AND KDI.EXAMCD = ?"+
       " AND KDI.SUBCD = ?"+
       " AND KDI.BUNDLECD = ?";
       return sql;
   }

   private CM cm = new CM();
   //2019/09/11 QQ)Oosaki 共通テスト対応 ADD END

    private void appendSubRecordSql(String string){
        if(getSubRecordSql() == null)
            setSubRecordSql("");
        subRecordSql += string;
    }

    /**
     * @return
     */
    public String getSubRecordSql() {
        return subRecordSql;
    }

    /**
     * @param string
     */
    public void setSubRecordSql(String string) {
        subRecordSql = string;
    }

    /**
     * @return
     */
    public StudentData getStudentData() {
        return studentData;
    }

    /**
     * @return
     */
    public String getExamType() {
        return examType;
    }

    /**
     * @return
     */
    public String getGrade() {
        return grade;
    }

    /**
     * @return
     */
    public String getIndividualId() {
        return individualId;
    }

    /**
     * @return
     */
    public String getTargetExamCode() {
        return targetExamCode;
    }

    /**
     * @return
     */
    public String getTargetExamYear() {
        return targetExamYear;
    }

    /**
     * @param string
     */
    public void setExamType(String string) {
        examType = string;
    }

    /**
     * @param string
     */
    public void setGrade(String string) {
        grade = string;
    }

    /**
     * @param string
     */
    public void setIndividualId(String string) {
        individualId = string;
    }

    /**
     * @param string
     */
    public void setTargetExamCode(String string) {
        targetExamCode = string;
    }

    /**
     * @param string
     */
    public void setTargetExamYear(String string) {
        targetExamYear = string;
    }


    /**
     * @return
     */
    public boolean isHelpflg() {
        return helpflg;
    }

    /**
     * @param b
     */
    public void setHelpflg(boolean b) {
        helpflg = b;
    }

    /**
     * @return
     */
    public String getClassYear() {//[2] add
        return classYear;//[2] add
    }//[2] add

    /**
     * @param string
     */
    public void setClassYear(String string) {//[2] add
        classYear = string;//[2] add
    }//[2] add

}



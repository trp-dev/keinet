/*
 * 作成日: 2004/10/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.helpdesk;

import java.io.IOException;
import java.security.cert.X509Certificate;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.news.InformListBean;
import jp.co.fj.keinavi.beans.helpdesk.HelpDeskLoginBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.helpdesk.HD001Form;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.servlets.DefaultLoginServlet;

/**
 * 
 * 2005.03.01	Yoshimoto KAWAI - Totec
 * 				証明書の取得方法を変更
 * 
 * @author nino
 *
 */
public class HD001Servlet extends DefaultLoginServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// アクションフォーム
		final HD001Form form = (HD001Form) getActionForm(request,
			"jp.co.fj.keinavi.forms.helpdesk.HD001Form");

		// リクエストへセット
		request.setAttribute("form", form);

		Connection con = null; // コネクション

		//---------------------------------------------------
		// お知らせリスト作成
		//   共通情報のみ
		//   （ログインユーザID、ユーザモードをセットしない）
		//---------------------------------------------------
		try {
			con = super.getConnectionPool(request);
			con.setAutoCommit(false);

			InformListBean infbean = new InformListBean();
			infbean.setConnection(null, con);
			infbean.setUserID(""); 			// ユーザID
			infbean.setUserMode(0); 		// ユーザーモード
			infbean.setDisplayDiv("0"); 	// 表示区分 "0":お知らせ
			infbean.setTop(true);
			infbean.execute();
			request.setAttribute("InformListBean", infbean);
		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			super.releaseConnectionPool(request, con);
		}
		//---------------------------------------------------


		// ＩＤがNULLならログイン画面を表示する
		if (form.getAccount() == null) {
			// セッションの初期化
			HttpSession session = request.getSession(false);
			if (session != null) session.invalidate();

			forward(request, response, JSP_HD001);

		// ログイン
		} else {

			// 証明書オブジェクト
			final X509Certificate[] cert = getCertificate(request);
	
			LoginSession login = new LoginSession(); // ログインセッション
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(false);
				
				// ログインセッションを作る
				HelpDeskLoginBean bean = new HelpDeskLoginBean();
				bean.setConnection(null, con);
				bean.setAccount(form.getAccount());
				bean.setPassword(form.getPassword());
				bean.setCert(cert);
				bean.setLoginSession(login);
				bean.execute();
				
				// セッションへ格納する
				request.getSession(true).setAttribute(
						LoginSession.SESSION_KEY, login);

				// 各メニューへ
				// 証明書があればヘルプデスクメニュー選択画面
				if (login.isCert()) {
					con.commit();
					actionLog(request, "101");
					login.setDbKey(KNCommonProperty.getNDBSID());
					super.forward(request, response, SERVLET_DISPATCHER);
				// それ以外は再度ログイン処理へ
				} else {
					final KNServletException e =
						new KNServletException("証明書エラー");
					e.setErrorCode("1");
					e.setErrorMessage("入力されたＩＤでは入場できません。");
					throw e;
				}

			// ログイン失敗
			} catch (final KNServletException e) {
				rollback(con);
				setErrorMessage(request, e);
				actionLog(request, "102", e.getMessage());
				forward(request, response, JSP_HD001);
			// 想定外の例外
			} catch (final Exception e) {
				rollback(con);
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}
		}
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#getForward(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	protected String getForward(final HttpServletRequest request) {
		final String forward = super.getForward(request);
		if (forward == null) {
			return "hd001";
		} else {
			return forward;
		}
	}

}

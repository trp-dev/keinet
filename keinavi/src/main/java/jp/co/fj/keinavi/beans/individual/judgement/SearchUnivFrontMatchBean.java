package jp.co.fj.keinavi.beans.individual.judgement;

import java.util.ArrayList;

import com.fjh.beans.DefaultBean;

import jp.co.fj.keinavi.data.individual.UnivKeiNavi;

/**
 * 
 *
 * 大学名（かな）検索
 * 
 * 
 * <2010年度改修>
 * 2009.12.01   Tomohisa YAMADA - Totec
 *              マスタデータの持ち方変更
 *              
 *
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 *
 */
public class SearchUnivFrontMatchBean	extends DefaultBean implements java.io.Serializable {

	/**
	 * 検索対象大学名
	 */
	private String searchUnivName = "";
	/**
	 * 比較対象データ
	 */
	private ArrayList compairData = new ArrayList();
	/**
	 * 結果リスト
	 */
	private ArrayList resultArrayList = new ArrayList();

	/**
	 * 大学名（かな）検索
	 */
	public void execute() {
		ArrayList iniArray = this.getCompairData();
		if (iniArray != null) {
			for (int i = 0; i < iniArray.size(); i++) {
				UnivKeiNavi univ = (UnivKeiNavi) iniArray.get(i);
				if (univ.getUnivNameKana().length()	>= this.getSearchUnivName().length()) {
					//選択された大学名と比較対照データの大学名を比較する
					if (univ.getUnivNameKana().substring(0, this.getSearchUnivName().length()).equals(this.getSearchUnivName())) {
						//選択大学の場合は比較対照用データとしてセットする
						resultArrayList.add(univ);
					}
				}
			}
		}
	}

	/**
	 * @return
	 */
	public ArrayList getCompairData() {
		return compairData;
	}

	/**
	 * @return
	 */
	public ArrayList getResultArrayList() {
		return resultArrayList;
	}

	/**
	 * @return
	 */
	public String getSearchUnivName() {
		return searchUnivName;
	}

	/**
	 * @param list
	 */
	public void setCompairData(ArrayList list) {
		compairData = list;
	}

	/**
	 * @param list
	 */
	public void setResultArrayList(ArrayList list) {
		resultArrayList = list;
	}

	/**
	 * @param string
	 */
	public void setSearchUnivName(String string) {
		searchUnivName = string;
	}

}
package jp.co.fj.keinavi.data.sheet;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 *
 * 印刷枚数定義クラス
 * 
 * 2006.08.04	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class PrintNumberDefinition {

	private final Map map = new HashMap();
	
	// 帳票ID
	private String id;
	
	/**
	 * @param examTypeCd 模試種類コード
	 * @param number 枚数
	 */
	public void add(final String examTypeCd, final String number) {
		
		if (examTypeCd == null) {
			throw new NullPointerException(
					"模試種類コードのNULLは許可されません。");
		}
		
		final Double num = Double.valueOf(number);
		
		// 複数
		if (examTypeCd.indexOf(',') >= 0) {
			final String[] code = StringUtils.split(examTypeCd, ',');
			for (int i = 0; i < code.length; i++) {
				map.put(createKey(id, code[i]), num);
			}
		// 単数
		} else {
			map.put(createKey(id, examTypeCd), num);
		}
	}
	
	/**
	 * @param id 帳票ID
	 * @param examTypeCd 模試種類コード
	 * @return 枚数
	 */
	public double getNumber(final String id, final String examTypeCd) {
		
		final String key = createKey(id, examTypeCd);
		
		// 設定あり
		if (map.containsKey(key)) {
			return ((Double) map.get(key)).doubleValue();
		// 設定なし
		} else {
			return 0;
		}
	}

	/**
	 * @param id 帳票ID
	 * @param examTypeCd 模試種類コード
	 * @return マップのキーを作る
	 */
	protected String createKey(final String id, final String examTypeCd) {
		return  id + "_" + examTypeCd;
	}

	/**
	 * @param pId 設定する id。
	 */
	public void setId(final String pId) {
		id = pId;
	}

	/**
	 * @return map を戻します。
	 */
	protected Map getMap() {
		return map;
	}
	
}

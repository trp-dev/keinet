/*
 * 作成日: 2004/09/05
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.sql;

import java.util.HashMap;
import java.util.Map;

/**
 * SQLを表現するクラス
 * 
 * @author kawai
 */
public class Query {

	private StringBuffer buff; // SQLバッファ
	private Map param; // パラメータマップ

	/**
	 * コンストラクタ
	 */
	public Query() {
		buff = new StringBuffer();
	}

	/**
	 * コンストラクタ
	 */
	public Query(String query) {
		buff = new StringBuffer(query);
	}

	/**
	 * SQLバッファ内の文字列を全て置換する
	 * @param marker 置換される文字列
	 * @param str 置換する文字列
	 * @return 置換した数
	 */
	public int replaceAll(String marker, String str) {
		int count = 0; // 置換数のカウント
		int size = str.length();
		int index = 0;
		while ((index = buff.indexOf(marker, index)) != -1) {
			count++;
			buff.replace(index, index + marker.length(), str);
			index += size;
		}
		return count;
	}
	
	/**
	 * SQLを出力する
	 * @return SQL文字列
	 */
	public String toString() {
		if (param != null) {
			int count = 0;
			int index;
			while ((index = buff.indexOf("#")) != -1) {
				count++;
				if (param.containsKey(String.valueOf(count))) {
					buff.replace(index, index + 1,
						param.get(String.valueOf(count)).toString());
				} else {
					throw new IllegalArgumentException("バインド変数がセットされていません。");
				}
			}
		}
		return buff.toString();
	}

	/**
	 * SQLに文字列を追加する
	 * @param string 文字列
	 */
	public void append(String string) {
		buff.append(string);
	}

	/**
	 * SQLに文字列バッファを追加する
	 * @param string 文字列バッファ
	 */
	public void append(StringBuffer b) {
		buff.append(b);
	}

	/**
	 * SQLの文字数を返す
	 * @return 文字数
	 */
	public int length() {
		return buff.length();
	}
	
	/**
	 * パラメータとして文字列をセットする
	 * @param index
	 * @param value
	 */
	public void setString(int index, String value) {
		if (param == null) param = new HashMap();

		if (value == null) param.put(String.valueOf(index), "NULL");
		else param.put(String.valueOf(index), SQLUtil.quote(value));
	}
	
	/**
	 * パラメータとしてint値をセットする
	 * @param index
	 * @param value
	 */
	public void setInt(int index, int value) {
		if (param == null) param = new HashMap();

		param.put(String.valueOf(index), String.valueOf(value));
	}
	
	/**
	 * パラメータとして文字列配列をセットする
	 * @param index
	 * @param value
	 */
	public void setStringArray(int index, String[] value) {
		if (value == null)
			throw new IllegalArgumentException("配列としてNULLは設定できません。");

		if (param == null) param = new HashMap();

		param.put(String.valueOf(index), SQLUtil.concatComma(value));
	}

	/**
	 * パラメータとして文字列配列をORで結合してセットする
	 * @param index
	 * @param value
	 */
	public void setOrCondition(int index, String[] value) {
		this.setCondition(index, value, "OR");
	}
	
	/**
	 * パラメータとして文字列配列をANDで結合してセットする
	 * @param index
	 * @param value
	 */
	public void setAndCondition(int index, String[] value) {
		this.setCondition(index, value, "AND");
	}
	
	/**
	 * パラメータとしてSQL文をセットする
	 * @param index
	 * @param value
	 */
	public void setQuery(int index, String value) {
		if (param == null) param = new HashMap();

		param.put(String.valueOf(index), value);
	}
	
	/**
	 * パラメータとして文字列配列を指定した文字列で結合してセットする
	 * @param index
	 * @param value
	 * @param node
	 */
	private void setCondition(int index, String[] value, String node) {
		if (value == null || value.length == 0)
			throw new IllegalArgumentException("配列は長さ1以上である必要があります。");
		
		StringBuffer buff = new StringBuffer();
		
		for (int i=0; i<value.length; i++) {
			if (i != 0) buff.append(node + " ");
			buff.append(value[i] + " ");
		}

		param.put(String.valueOf(index), buff.toString());
	}
	
}

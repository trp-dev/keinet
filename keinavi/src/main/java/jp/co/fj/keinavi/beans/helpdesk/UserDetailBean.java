/*
 * 作成日: 2004/09/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.helpdesk;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import jp.co.fj.keinavi.forms.helpdesk.HD105Form;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 2005.04.20	Yoshimoto KAWAI - Totec
 * 				・最大接続数の取得列名が間違っていたのをFIX
 *				・私書箱提供のフラグ表示が逆転していたのをFIX
 *				・私書箱提供フラグがNULLの場合例外が発生していたのをFIX
 *  
 * @author nino
 *
 */
public class UserDetailBean extends DefaultBean {

	private static String[] userDivName = {"","公立","私学","予備校","","教育委員会","ヘルプデスク","","",""};
	private static String[] pactDivName = {"","契約校","非契約校","リサーチ参加校","非受験校","ヘルプデスク","","","",""};
	private static String[] postFunctionName = {"提供しない","提供する"}; // FIXED

	private HD105Form 	hd105Form 	= new HD105Form();	// HD105Form 情報
	private String	schoolCode		= null;	// 学校コード
	private String	schoolName		= null;	// 学校名
	private String	userId			= null;	// ユーザID
	private String	firstPass		= null;	// 初期パスワード
	private String	loginPass		= null;	// ログインパスワード
	private String	userKubun		= null;	// ユーザ区分
	private String	contractKubun	= null;	// 契約区分
	private String	contractTerm	= null;	// 契約期限
	private String	validityTerm	= null;	// 証明書有効期限
	private String	finishingValidityTerm = null;	// 発行済証明書有効期限
	private String	sessionNumber	= null;	// 最大セッション数
	private String	postFunction	= null;	// 私書箱機能提供
	private String	rgdate			= null;	// 登録日
	private String	update			= null;	// 更新日
	private String loginId			= null;	// 利用者ID
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("hd06"));
			ps.setString(1, hd105Form.getUserId());
			rs = ps.executeQuery();
			
			if (rs.next()) {
				setSchoolCode(rs.getString(1));
				setSchoolName(rs.getString(2));
				setUserId(rs.getString(3));
				setFirstPass(rs.getString(4));
				setLoginPass(rs.getString(5));
				setUserKubun( getUserDivName( rs.getString(6) ) );
				setContractKubun( getPactDivName( rs.getString(7) ) );
				setContractTerm( getFmtYmd(rs.getString(8)) );
				setValidityTerm( getFmtYmd(rs.getString(9)) );
				setFinishingValidityTerm( getFmtYmd(rs.getString(10)) );
				setSessionNumber(rs.getString(11));
				setPostFunction( postFunctionName[ Integer.parseInt(rs.getString(12)) ] );
				setRgdate( getFmtYmd(rs.getString(13)) );
				setUpdate( getFmtYmd(rs.getString(14)) );
				setLoginId(rs.getString(15));
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	* ユーザ区分名取得
	* @return
	*/
	public String getUserDivName(String str) {
		String name = "";
		int no = Integer.parseInt(str);
		if ( 0 <= no && no <= 9) {
			name = userDivName[no];
		}
		return name;
	}

	/**
	* 契約区分名取得
	* @return
	*/
	public String getPactDivName(String str) {
		String name = "";
		int no = Integer.parseInt(str);
		if ( 0 <= no && no <= 9) {
			name = pactDivName[no];
		}
		return name;
	}

	// HD105Form 情報
	/**
	 * @param HD101Form
	 */
	public void setHD105Form(HD105Form form) {
		hd105Form = form;
	}
	
	/**
	 * @return HD101Form
	 */
	public HD105Form getHD105Form() {
		return hd105Form;
	}

	/**
	 * ８桁文字列を"yyyy/mm/dd"に変換
	 * 
	 * 	 * @return 連結した文字列日付("yyyymmdd")
	 */
	public String getFmtYmd(String yyyymmdd) {
		if ( yyyymmdd == null || yyyymmdd.trim().equals("") || yyyymmdd.length() != 8 ) {
			return yyyymmdd;
		} else {
			return yyyymmdd.substring(0,4) + "/" + yyyymmdd.substring(4,6) + "/" + yyyymmdd.substring(6,8);
		}
	}


	/**
	* @return
	*/
	public String getSchoolCode() {
		return schoolCode;
	}

	/**
	 * @param string
	 */
	public void setSchoolCode(String string) {
		schoolCode = string;
	}

	/**
	* @return
	*/
	public String getSchoolName() {
		return schoolName;
	}

	/**
	 * @param string
	 */
	public void setSchoolName(String string) {
		schoolName = string;
	}

	/**
	* @return
	*/
	public String getUserId() {
		return userId;
	}

	/**
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}

	/**
	* @return
	*/
	public String getFirstPass() {
		return firstPass;
	}

	/**
	 * @param string
	 */
	public void setFirstPass(String string) {
		firstPass = string;
	}

	/**
	* @return
	*/
	public String getLoginPass() {
		return loginPass;
	}

	/**
	 * @param string
	 */
	public void setLoginPass(String string) {
		loginPass = string;
	}

	/**
	* @return
	*/
	public String getUserKubun() {
		return userKubun;
	}

	/**
	 * @param string
	 */
	public void setUserKubun(String string) {
		userKubun = string;
	}

	/**
	* @return
	*/
	public String getContractKubun() {
		return contractKubun;
	}

	/**
	 * @param string
	 */
	public void setContractKubun(String string) {
		contractKubun = string;
	}

	/**
	* @return
	*/
	public String getContractTerm() {
		return contractTerm;
	}

	/**
	 * @param string
	 */
	public void setContractTerm(String string) {
		contractTerm = string;
	}

	/**
	* @return
	*/
	public String getValidityTerm() {
		return validityTerm;
	}

	/**
	 * @param string
	 */
	public void setValidityTerm(String string) {
		validityTerm = string;
	}

	/**
	* @return
	*/
	public String getFinishingValidityTerm() {
		return finishingValidityTerm;
	}

	/**
	 * @param string
	 */
	public void setFinishingValidityTerm(String string) {
		finishingValidityTerm = string;
	}

	/**
	* @return
	*/
	public String getSessionNumber() {
		return sessionNumber;
	}

	/**
	 * @param string
	 */
	public void setSessionNumber(String string) {
		sessionNumber = string;
	}

	/**
	* @return
	*/
	public String getPostFunction() {
		return postFunction;
	}

	/**
	 * @param string
	 */
	public void setPostFunction(String string) {
		postFunction = string;
	}

	/**
	* @return
	*/
	public String getRgdate() {
		return rgdate;
	}

	/**
	 * @param string
	 */
	public void setRgdate(String string) {
		rgdate = string;
	}

	/**
	* @return
	*/
	public String getUpdate() {
		return update;
	}

	/**
	 * @param string
	 */
	public void setUpdate(String string) {
		update = string;
	}

	/**
	 * @return loginId を戻します。
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * @param pLoginId 設定する loginId。
	 */
	public void setLoginId(String pLoginId) {
		loginId = pLoginId;
	}

}

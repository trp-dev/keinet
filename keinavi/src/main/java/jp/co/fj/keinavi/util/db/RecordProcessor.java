package jp.co.fj.keinavi.util.db;

import jp.co.fj.keinavi.data.individual.ScheduleDetailData;

/**
 *
 * データ変換ユーティリティ
 * ※DateUtilが新たにできたので見直しの必要あり。
 * 
 * 2004.08.20	Tomohisa YAMADA - TOTEC
 * 				[新規作成]
 * 
 * <2010年度改修>
 * 2009.11.26	Tomohisa YAMADA - TOTEC
 * 				入試日程変更対応
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class RecordProcessor {
	
	/**
	 * ８桁の文字列から年度の文字列のみを取り出す
	 * @param digit
	 * @return
	 */
	public static String getYearDigit(String digit){
		return ScheduleDetailData.getYearDigit(digit);
	}
	/**
	 * ８桁または４桁の文字列から月の文字列のみを取り出す
	 * @param digit
	 * @return
	 */
	public static String getMonthDigit(String digit){
		return ScheduleDetailData.getMonthDigit(digit);
	}
	/**
	 * ８桁または４桁の文字列から日の文字列のみを取り出す
	 * @param digit
	 * @return
	 */
	public static String getDateDigit(String digit){
		return ScheduleDetailData.getDateDigit(digit);
	}
	
	/**
	 * 8digitまたは4digitの並びの日付文字列にスラッシュを入れて返す
	 * trimZeroがtrueなら06/12は6/12になる
	 * ex: 200406012 -> 2004/06/12
	 * ex: 06012 -> 6/12
	 * @param digit
	 * @param trimZero ゼロを省くかどうか
	 * @return
	 */
	public static String dateWSlash(String digit, boolean trimZero){
		return ScheduleDetailData.dateWSlash(digit, trimZero);
	}
	
	/**
	 * スラッシュ入りの日付をゼロ詰めのdigitに戻す
	 * @param str
	 * @return
	 */
	public static String nonDateWSlash(String str){
		return ScheduleDetailData.nonDateWSlash(str);
	}
	
	/**
	 * digit文字列の最初にあるひとつの0を取り除いて返す
	 * @param digit
	 * @return
	 */
	public static String trimZero(String digit){
		return ScheduleDetailData.trimZero(digit);
	}
	/**
	 * 
	 * @param tYear
	 * @param tMonth
	 * @param tDate
	 * @return
	 */
	public static String getYMD8(String tYear, String tMonth, String tDate){
		String year = tYear;
		String month = tMonth.trim();
		if(month.length() == 1)
			month = "0"+tMonth.trim();
		String date = tDate.trim();
		if(date.length() == 1)
			date = "0"+tDate.trim();
		return year + month + date;
	}
	
	/**
	 * NullをNullにしない
	 * @param string
	 * @return
	 */
	public static String trimNull(String string){
			if(string == null)
				return "";
			else
				return string.trim();
	}
	
}

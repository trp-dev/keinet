package jp.co.fj.keinavi.beans.sales;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.forms.sales.SD201Form;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 申請処理を行うBeanです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD201ApplyBean extends DefaultBean {

	/** serialVersionUID */
	private static final long serialVersionUID = 4003050090135288403L;

	/** アクションフォーム */
	private final SD201Form form;

	/** ログインID */
	private final String loginId;

	/** 所属する部門コードの配列 */
	private final String[] sectorCdArray;

	/**
	 * コンストラクタです。
	 */
	public SD201ApplyBean(String loginId, SD201Form form, String[] sectorCdArray) {
		this.loginId = loginId;
		this.form = form;
		this.sectorCdArray = sectorCdArray;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		String appId = getAppId();
		if (insertApp(appId)) {
			insertSect(appId);
		} else {
			setError("申請された学校コードは、すでに申請済みです。");
		}
	}

	/**
	 * 特例データ申請IDを採番します。
	 */
	private String getAppId() throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance()
					.load("sd201_app_id").toString());
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString(1);
			} else {
				throw new Exception("特例データ申請IDの取得に失敗しました。");
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * 特例データ申請管理レコードを登録します。
	 */
	private boolean insertApp(String appId) throws SQLException {

		PreparedStatement ps = null;
		try {
			Query query = QueryLoader.getInstance().load("sd201_insert_app");
			query.setStringArray(1, sectorCdArray);

			ps = conn.prepareStatement(query.toString());
			ps.setString(1, appId);
			ps.setString(2, loginId);
			ps.setString(3, form.getExamYear());
			ps.setString(4, form.getExamCd());
			ps.setString(5, form.getSchoolCd());
			ps.setString(6, form.getTeacherName());
			ps.setString(7, form.getComments());
			ps.setString(8, form.getExamYear());
			ps.setString(9, form.getExamCd());
			ps.setString(10, form.getSchoolCd());

			return ps.executeUpdate() > 0;
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 特例データ申請部署レコードを登録します。
	 */
	private void insertSect(String appId) throws SQLException {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance()
					.load("sd201_insert_sect").toString());
			ps.setString(1, appId);
			for (int i = 0; i < sectorCdArray.length; i++) {
				ps.setString(2, sectorCdArray[i]);
				ps.executeUpdate();
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

}

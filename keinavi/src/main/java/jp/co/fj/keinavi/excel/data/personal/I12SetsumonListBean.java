package jp.co.fj.keinavi.excel.data.personal;

//import org.apache.struts.action.ActionForm;
//import java.util.ArrayList;
/**
 * 個人成績分析−面談用帳票 設問別成績グラフデータリスト
 * 作成日: 2004/07/20
 * @author	A.Iwata
 * 2009.10.26   Fujito URAKAWA - Totec
 *              「学力レベル」項目を生かした新メニュー対応 
 */
public class I12SetsumonListBean {
	//型・科目名
	private String strKmkmei = "";
	//設問番号
	private String strSetsuNo = "";
	//設問内容
	private String strSetsuMei1 = "";
	//設問配点
	private String strSetsuHaiten = "";
	//個人得点
	private int intTokutenSelf = 0;
	//個人得点率
	private float floTokuritsuSelf = 0;
	//校内平均点
	private float floTokutenHome = 0;
	//校内得点率
	private float floTokuritsuHome = 0;
	//全国平均点
	private float floTokutenAll = 0;
	//全国得点率
	private float floTokuritsuAll = 0;
	//学力レベル
	private String strScholarLevel = "";
	//学力レベル得点率
	private float floScholarLevel = 0;

	
	/*----------*/
	/* Get      */
	/*----------*/

	public String getStrKmkmei() {
		return this.strKmkmei;
	}
	public String getStrSetsuNo() {
		return this.strSetsuNo;
	}
	public String getStrSetsuMei1() {
		return this.strSetsuMei1;
	}
	public String getStrSetsuHaiten() {
		return this.strSetsuHaiten;
	}
	public int getIntTokutenSelf() {
		return this.intTokutenSelf;
	}
	public float getFloTokuritsuSelf() {
		return this.floTokuritsuSelf;
	}
	public float getFloTokutenHome() {
		return this.floTokutenHome;
	}
	public float getFloTokuritsuHome() {
		return this.floTokuritsuHome;
	}
	public float getFloTokutenAll() {
		return this.floTokutenAll;
	}
	public float getFloTokuritsuAll() {
		return this.floTokuritsuAll;
	}
	public String getStrScholarLevel() {
		return strScholarLevel;
	}
	public float getFloScholarLevel() {
		return floScholarLevel;
	}
	/*----------*/
	/* Set      */
	/*----------*/

	public void setStrKmkmei(String strKmkmei) {
		this.strKmkmei = strKmkmei;
	}
	public void setStrSetsuNo(String strSetsuNo) {
		this.strSetsuNo = strSetsuNo;
	}
	public void setStrSetsuMei1(String strSetsuMei1) {
		this.strSetsuMei1 = strSetsuMei1;
	}
	public void setStrSetsuHaiten(String strSetsuHaiten) {
		this.strSetsuHaiten = strSetsuHaiten;
	}
	public void setIntTokutenSelf(int intTokutenSelf) {
		this.intTokutenSelf = intTokutenSelf;
	}
	public void setFloTokuritsuSelf(float floTokuritsuSelf) {
		this.floTokuritsuSelf = floTokuritsuSelf;
	}
	public void setFloTokutenHome(float floTokutenHome) {
		this.floTokutenHome = floTokutenHome;
	}
	public void setFloTokuritsuHome(float floTokuritsuHome) {
		this.floTokuritsuHome = floTokuritsuHome;
	}
	public void setFloTokutenAll(float floTokutenAll) {
		this.floTokutenAll = floTokutenAll;
	}
	public void setFloTokuritsuAll(float floTokuritsuAll) {
		this.floTokuritsuAll = floTokuritsuAll;
	}
	public void setStrScholarLevel(String strScholarLevel) {
		this.strScholarLevel = strScholarLevel;
	}
	public void setFloScholarLevel(float floScholarLevel) {
		this.floScholarLevel = floScholarLevel;
	}
}

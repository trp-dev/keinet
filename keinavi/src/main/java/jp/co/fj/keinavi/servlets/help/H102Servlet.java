/*
 * 作成日: 2004/10/14
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.servlets.help;

import java.io.IOException;
import java.util.Vector;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.help.MailSender;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.help.H102Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.servlets.KNServletException;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class H102Servlet extends DefaultHttpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// ログイン情報
		LoginSession login = getLoginSession(request);

		String actionMode = request.getParameter("actionMode"); 

		// アクションフォーム
		final H102Form form = (H102Form) getActionForm(request,
				"jp.co.fj.keinavi.forms.help.H102Form");
		
		if("h102".equals(getForward(request))
				|| "h103".equals(getForward(request))) {
			// 送信モードの場合
			if ( actionMode.equals("Transmit") ) {
				
				try {
					Vector mailContents = new Vector();
					mailContents.add(form.getTitle());
					mailContents.add(form.getText());
					mailContents.add(login.getUserName());
					mailContents.add(form.getName());
					mailContents.add(form.getTelNumber());
					mailContents.add(form.getMailAddr());
					mailContents.add(login.getLoginID());
					// メッセージ送信
					MailSender.sendMessage("Inquire", mailContents);
					//	アクセスログ
					actionLog(request, "2011");
				} catch (MessagingException e) {
					KNServletException k = new KNServletException(e.getMessage());
					k.setErrorCode("1");
					k.setErrorMessage("メール送信に失敗しました。");
					throw k;
				}

				forward(request, response, JSP_H103);

			// JSP
			} else {
				// アクションフォーム
				request.setAttribute("form", form);
				forward(request, response, JSP_H102);
			}
		// 不明なら転送
		} else {
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

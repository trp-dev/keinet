/*
 * 作成日: 2004/09/03
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.helpdesk;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * SQLを外部ファイルからロードしてキャッシュしておくクラス
 * ※JakartaCommonsDbutils内のクラスをほぼそのままに拡張
 *
 * @author kawai
 */
public class Certificates {

	// Singleton
	private static final Certificates instance = new Certificates();

	// SQLをキャッシュしておくマップ
	private Map queryMap = new HashMap();

	// プロパティファイル
	private final String properties = "Certificates.properties";

	private String ou    = null; 	// システム名
	private String o     = null; 	// ドメイン名
	private String l     = null; 	// 市町村
	private String st    = null; 	// 都道府県
	private String c     = null; 	// 国
	private String email = null; 	// Email アドレス

	// 2015/10/18 QQ)Nishiyama デジタル証明書対応 ADD START
	private String typeId    = null;	// タイプＩＤ
	private String algoRithm = null;	// アルゴリズム
	private String keyLength = null;	// キー長
	// 2015/10/18 QQ)Nishiyama デジタル証明書対応 ADD END

	/**
	 * コンストラクタ
	 */
	private Certificates() {
		// 設定を読み込む
		InputStream in = Certificates.class.getResourceAsStream(properties);

		if (in == null) {
			throw new IllegalArgumentException("properties file not found.");
		}

		Properties props = new Properties();

		try {
			props.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}

		ou    = (String)props.get("OU");
		o     = (String)props.get("O");
		l     = (String)props.get("L");
		st    = (String)props.get("ST");
		c     = (String)props.get("C");
		email = (String)props.get("EMAIL");
		// 2015/10/18 QQ)Nishiyama デジタル証明書対応 ADD START
		typeId    = (String)props.get("TYPEID");
		algoRithm = (String)props.get("ALGORITHM");
		keyLength = (String)props.get("KEYLENGTH");
		// 2015/10/18 QQ)Nishiyama デジタル証明書対応 ADD END
	}

	/**
	 * このクラスのインスタンスを返す
	 * @return Singletonなインスタンス
	 */
	public static Certificates getInstance() {
		return instance;
	}


	/**
	 * @return
	 */
	public String getO() {
		return o;
	}

	/**
	 * @param string
	 */
	public void setO(String string) {
		o = string;
	}

	/**
	 * @return
	 */
	public String getOu() {
		return ou;
	}

	/**
	 * @param string
	 */
	public void setOu(String string) {
		ou = string;
	}

	/**
	 * @return
	 */
	public String getL() {
		return l;
	}

	/**
	 * @param string
	 */
	public void setL(String string) {
		l = string;
	}

	/**
	 * @return
	 */
	public String getSt() {
		return st;
	}

	/**
	 * @param string
	 */
	public void setSt(String string) {
		st = string;
	}

	/**
	 * @return
	 */
	public String getC() {
		return c;
	}

	/**
	 * @param string
	 */
	public void setC(String string) {
		c = string;
	}

	/**
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param string
	 */
	public void setEmail(String string) {
		email = string;
	}

	// 2015/10/18 QQ)Nishiyama デジタル証明書対応 ADD START
	/**
	 * @return
	 */
	public String getTypeId() {
		return typeId;
	}

	/**
	 * @param string
	 */
	public void setTypeId(String string) {
		typeId = string;
	}

	/**
	 * @return
	 */
	public String getAlgoRithm() {
		return algoRithm;
	}

	/**
	 * @param string
	 */
	public void setAlgoRithm(String string) {
		algoRithm = string;
	}

	/**
	 * @return
	 */
	public String getKeyLength() {
		return keyLength;
	}

	/**
	 * @param string
	 */
	public void setKeyLength(String string) {
		keyLength = string;
	}
	// 2015/10/18 QQ)Nishiyama デジタル証明書対応 ADD END

}

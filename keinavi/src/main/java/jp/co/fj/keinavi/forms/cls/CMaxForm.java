package jp.co.fj.keinavi.forms.cls;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 *
 * 2005.04.05	Yoshimoto KAWAI - Totec
 * 				過年度の表示を追加
 *
 * 2005.04.11	Yoshimoto KAWAI - Totec
 * 				日程・評価区分を追加
 *
 * @author kawai
 *
 */
public class CMaxForm extends BaseForm {

	private String changeExam; // 模試に対する変更を行ったかどうか
	private String save; // 保存処理を行うかどうか

	private String pastExam1;
	private String pastExam2;
	private String commonType;
	private String commonCourse;
	private String[] analyzeType;
	private String[] graphType;
	private String[] analyzeCourse;
	private String[] graphCourse;
	private String[] formatItem;
	private String[] optionItem;
	private String divPitch;
	private String compCount;
	private String dispChoiceUniv;
	private String printUnivDiv;
	private String strudentOrder;
	private String candidateOrder;
	private String univOrder;
	private String printClass;
	private String fluctRange;
	private String[] analyzeItem;
	private String deviation;
	private String choiceUnivMode;
	private String candRange;
	private String[] evaluation;
	private String stamp;
	private String axis;
	private String dispRatio;
	private String totalUnivDiv;
	private String[] dispUnivStudent;
	private String[] dispUnivYear;
	private String dispUnivSchedule;
	private String[] dispUnivRating;
        private String orderPitch;

    /**
     * @return dispUnivStudent を戻す。
     */
    public String[] getDispUnivStudent() {
        return dispUnivStudent;
    }
    /**
     * @param dispUnivStudent dispUnivStudent を設定する。
     */
    public void setDispUnivStudent(String[] dispUnivStudent) {
        this.dispUnivStudent = dispUnivStudent;
    }
	private String classOrder;
	private String pitchCompRatio;

// 2019/09/06 QQ)Tanioka  ADD START
	private String targetCheckBox;
// 2019/09/06 QQ)Tanioka  ADD END

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {

	}




	/**
	 * @return
	 */
	public String[] getAnalyzeCourse() {
		return analyzeCourse;
	}

	/**
	 * @return
	 */
	public String[] getAnalyzeItem() {
		return analyzeItem;
	}

	/**
	 * @return
	 */
	public String[] getAnalyzeType() {
		return analyzeType;
	}

	/**
	 * @return
	 */
	public String getCandidateOrder() {
		return candidateOrder;
	}

	/**
	 * @return
	 */
	public String getCandRange() {
		return candRange;
	}

	/**
	 * @return
	 */
	public String getChoiceUnivMode() {
		return choiceUnivMode;
	}

	/**
	 * @return
	 */
	public String getCommonCourse() {
		return commonCourse;
	}

	/**
	 * @return
	 */
	public String getCommonType() {
		return commonType;
	}

	/**
	 * @return
	 */
	public String getCompCount() {
		return compCount;
	}

	/**
	 * @return
	 */
	public String getDeviation() {
		return deviation;
	}

	/**
	 * @return
	 */
	public String getDispChoiceUniv() {
		return dispChoiceUniv;
	}

	/**
	 * @return
	 */
	public String getDivPitch() {
		return divPitch;
	}

       /**
         * @return
         */
        public String getOrderPitch() {
                return orderPitch;
        }

	/**
	 * @return
	 */
	public String getFluctRange() {
		return fluctRange;
	}

	/**
	 * @return
	 */
	public String[] getFormatItem() {
		return formatItem;
	}

	/**
	 * @return
	 */
	public String[] getGraphCourse() {
		return graphCourse;
	}

	/**
	 * @return
	 */
	public String[] getGraphType() {
		return graphType;
	}

	/**
	 * @return
	 */
	public String[] getOptionItem() {
		return optionItem;
	}

	/**
	 * @return
	 */
	public String getPastExam1() {
		return pastExam1;
	}

	/**
	 * @return
	 */
	public String getPastExam2() {
		return pastExam2;
	}

	/**
	 * @return
	 */
	public String getPrintClass() {
		return printClass;
	}

	/**
	 * @return
	 */
	public String getPrintUnivDiv() {
		return printUnivDiv;
	}

	/**
	 * @return
	 */
	public String getStamp() {
		return stamp;
	}

	/**
	 * @return
	 */
	public String getStrudentOrder() {
		return strudentOrder;
	}

	/**
	 * @return
	 */
	public String getUnivOrder() {
		return univOrder;
	}

	/**
	 * @param strings
	 */
	public void setAnalyzeCourse(String[] strings) {
		analyzeCourse = strings;
	}

	/**
	 * @param strings
	 */
	public void setAnalyzeItem(String[] strings) {
		analyzeItem = strings;
	}

	/**
	 * @param strings
	 */
	public void setAnalyzeType(String[] strings) {
		analyzeType = strings;
	}

	/**
	 * @param string
	 */
	public void setCandidateOrder(String string) {
		candidateOrder = string;
	}

	/**
	 * @param string
	 */
	public void setCandRange(String string) {
		candRange = string;
	}

	/**
	 * @param string
	 */
	public void setChoiceUnivMode(String string) {
		choiceUnivMode = string;
	}

	/**
	 * @param string
	 */
	public void setCommonCourse(String string) {
		commonCourse = string;
	}

	/**
	 * @param string
	 */
	public void setCommonType(String string) {
		commonType = string;
	}

	/**
	 * @param string
	 */
	public void setCompCount(String string) {
		compCount = string;
	}

	/**
	 * @param string
	 */
	public void setDeviation(String string) {
		deviation = string;
	}

	/**
	 * @param string
	 */
	public void setDispChoiceUniv(String string) {
		dispChoiceUniv = string;
	}

	/**
	 * @param string
	 */
	public void setDivPitch(String string) {
		divPitch = string;
	}

        /**
         * @param string
         */
        public void setOrderPitch(String string) {
                orderPitch = string;
        }

	/**
	 * @param string
	 */
	public void setFluctRange(String string) {
		fluctRange = string;
	}

	/**
	 * @param strings
	 */
	public void setFormatItem(String[] strings) {
		formatItem = strings;
	}

	/**
	 * @param strings
	 */
	public void setGraphCourse(String[] strings) {
		graphCourse = strings;
	}

	/**
	 * @param strings
	 */
	public void setGraphType(String[] strings) {
		graphType = strings;
	}

	/**
	 * @param strings
	 */
	public void setOptionItem(String[] strings) {
		optionItem = strings;
	}

	/**
	 * @param string
	 */
	public void setPastExam1(String string) {
		pastExam1 = string;
	}

	/**
	 * @param string
	 */
	public void setPastExam2(String string) {
		pastExam2 = string;
	}

	/**
	 * @param string
	 */
	public void setPrintClass(String string) {
		printClass = string;
	}

	/**
	 * @param string
	 */
	public void setPrintUnivDiv(String string) {
		printUnivDiv = string;
	}

	/**
	 * @param string
	 */
	public void setStamp(String string) {
		stamp = string;
	}

	/**
	 * @param string
	 */
	public void setStrudentOrder(String string) {
		strudentOrder = string;
	}

	/**
	 * @param string
	 */
	public void setUnivOrder(String string) {
		univOrder = string;
	}

	/**
	 * @return
	 */
	public String getAxis() {
		return axis;
	}

	/**
	 * @param string
	 */
	public void setAxis(String string) {
		axis = string;
	}

	/**
	 * @return
	 */
	public String getDispRatio() {
		return dispRatio;
	}

	/**
	 * @param string
	 */
	public void setDispRatio(String string) {
		dispRatio = string;
	}

	/**
	 * @return
	 */
	public String getTotalUnivDiv() {
		return totalUnivDiv;
	}

	/**
	 * @param string
	 */
	public void setTotalUnivDiv(String string) {
		totalUnivDiv = string;
	}

	/**
	 * @return
	 */
	public String getClassOrder() {
		return classOrder;
	}

	/**
	 * @return
	 */
	public String getPitchCompRatio() {
		return pitchCompRatio;
	}

// 2019/09/04 QQ)Tanioka  ADD START
	/**
	 * @return
	 */
	public String getTargetCheckBox() {
		return targetCheckBox;
	}
// 2019/09/04 QQ)Tanioka  ADD END

	/**
	 * @param string
	 */
	public void setClassOrder(String string) {
		classOrder = string;
	}

	/**
	 * @param string
	 */
	public void setPitchCompRatio(String string) {
		pitchCompRatio = string;
	}

// 2019/09/04 QQ)Tanioka  ADD START
	/**
	 * @param string
	 */
	public void setTargetCheckBox(String string) {
		targetCheckBox = string;
	}
// 2019/09/04 QQ)Tanioka  ADD END

	/**
	 * @return
	 */
	public String[] getEvaluation() {
		return evaluation;
	}

	/**
	 * @param strings
	 */
	public void setEvaluation(String[] strings) {
		evaluation = strings;
	}

	/**
	 * @return
	 */
	public String getChangeExam() {
		return changeExam;
	}

	/**
	 * @param string
	 */
	public void setChangeExam(String string) {
		changeExam = string;
	}

	/**
	 * @return
	 */
	public String getSave() {
		return save;
	}

	/**
	 * @param string
	 */
	public void setSave(String string) {
		save = string;
	}

	public String[] getDispUnivYear() {
		return this.dispUnivYear;
	}
	public void setDispUnivYear(String[] dispUnivYear) {
		this.dispUnivYear = dispUnivYear;
	}
	public String[] getDispUnivRating() {
		return this.dispUnivRating;
	}
	public void setDispUnivRating(String[] dispUnivRating) {
		this.dispUnivRating = dispUnivRating;
	}
	public String getDispUnivSchedule() {
		return this.dispUnivSchedule;
	}
	public void setDispUnivSchedule(String dispUnivSchedule) {
		this.dispUnivSchedule = dispUnivSchedule;
	}
}

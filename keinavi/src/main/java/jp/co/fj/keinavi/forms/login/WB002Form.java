/*
 * 作成日: 2004/06/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.login;

import com.fjh.forms.ActionForm;



/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class WB002Form extends ActionForm {

	// 利用メニュー
	private String menu = null;
	// ＩＤ
	private String account = null;
	 

	/* (非 Javadoc)
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {


	}
	/**
	 * @return
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @return
	 */
	public String getMenu() {
		return menu;
	}

	/**
	 * @param string
	 */
	public void setAccount(String string) {
		account = string;
	}

	/**
	 * @param string
	 */
	public void setMenu(String string) {
		menu = string;
	}

}

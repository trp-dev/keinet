/**
 * 校内成績分析−過年度比較　偏差値分布
 * @author Ito.Y
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.school.S22Item;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;

public class S22 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean s22( S22Item s22Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog) {

		KNLog log = KNLog.getInstance(null,null,null);

		try{
			int ret = 0;
			//S22Itemから各帳票を出力
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD START
//// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 MOD START
////			if (( s22Item.getIntHyouFlg()==0 )&&( s22Item.getIntBnpGraphFlg()==0 )&&
////				( s22Item.getIntNinzuGraphFlg()==0 )&&( s22Item.getIntKoseiGraphFlg()==0 )) {
//			if (( s22Item.getIntHyouFlg()==0 )
//				&&( s22Item.getIntBnpGraphFlg()==0 )
//				&&( s22Item.getIntNinzuGraphFlg()==0 )
//				&&( s22Item.getIntKoseiGraphFlg()==0 )
//				&&( s22Item.getIntCheckBoxFlg()==0 )) {
//// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 MOD END
			if (( s22Item.getIntHyouFlg()==0 )&&( s22Item.getIntBnpGraphFlg()==0 )&&
				( s22Item.getIntNinzuGraphFlg()==0 )&&( s22Item.getIntKoseiGraphFlg()==0 )) {
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD END
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
			    //throw new Exception("S22 ERROR : フラグ異常");
			    throw new IllegalStateException("S22 ERROR : フラグ異常");
			    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
			}
			if (( s22Item.getIntHyouFlg()==1 )&&( s22Item.getIntKoseihiFlg()==2 )) {
				S22_01 exceledit = new S22_01();
				ret = exceledit.s22_01EditExcel( s22Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S22_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S22_01");
			}
			if (( s22Item.getIntHyouFlg()==1 )&&( s22Item.getIntKoseihiFlg()==1 )) {
				S22_02 exceledit = new S22_02();
				ret = exceledit.s22_02EditExcel( s22Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S22_02","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S22_02");
			}
			if (( s22Item.getIntBnpGraphFlg()==1 )&&( s22Item.getIntAxisFlg()==1 )) {
				S22_03 exceledit = new S22_03();
				ret = exceledit.s22_03EditExcel( s22Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S22_03","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S22_03");
			}
			if (( s22Item.getIntBnpGraphFlg()==1 )&&( s22Item.getIntAxisFlg()==2 )) {
				S22_04 exceledit = new S22_04();
				ret = exceledit.s22_04EditExcel( s22Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S22_04","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S22_04");
			}
			if (( s22Item.getIntNinzuGraphFlg()==1 )&&( s22Item.getIntNinzuPitchFlg()==1 )) {
				S22_05 exceledit = new S22_05();
				ret = exceledit.s22_05EditExcel( s22Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S22_05","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S22_05");
			}
			if (( s22Item.getIntNinzuGraphFlg()==1 )&&( s22Item.getIntNinzuPitchFlg()==2 )) {
				S22_06 exceledit = new S22_06();
				ret = exceledit.s22_06EditExcel( s22Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S22_06","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S22_06");
			}
			if (( s22Item.getIntNinzuGraphFlg()==1 )&&( s22Item.getIntNinzuPitchFlg()==3 )) {
				S22_07 exceledit = new S22_07();
				ret = exceledit.s22_07EditExcel( s22Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S22_07","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S22_07");
			}
			if (( s22Item.getIntKoseiGraphFlg()==1 )&&( s22Item.getIntKoseiPitchFlg()==1 )) {
				S22_08 exceledit = new S22_08();
				ret = exceledit.s22_08EditExcel( s22Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S22_08","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S22_08");
			}
			if (( s22Item.getIntKoseiGraphFlg()==1 )&&( s22Item.getIntKoseiPitchFlg()==2 )) {
				S22_09 exceledit = new S22_09();
				ret = exceledit.s22_09EditExcel( s22Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S22_09","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S22_09");
			}
			if (( s22Item.getIntKoseiGraphFlg()==1 )&&( s22Item.getIntKoseiPitchFlg()==3 )) {
				S22_10 exceledit = new S22_10();
				ret = exceledit.s22_10EditExcel( s22Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"S22_10","帳票作成エラー","");
					return false;
				}
				sheetLog.add("S22_10");
			}
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START
//// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD START
//			if (( s22Item.getIntCheckBoxFlg()==1 )) {
//				S22_11 exceledit = new S22_11();
//				ret = exceledit.s22_11EditExcel( s22Item, outfilelist, saveFlg, UserID );
//				if (ret!=0) {
//					log.Err("0"+ret+"S22_11","帳票作成エラー","");
//					return false;
//				}
//				sheetLog.add("S22_11");
//			}
//// 2019/08/20 QQ)H.Yamasaki 共通テスト対応 ADD END
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END

		} catch(Exception e) {
			log.Err("99S22","帳票作成エラー",e.toString());
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
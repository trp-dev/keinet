/*
 * 作成日: 2004/06/30
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * @author kawai
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CollectionUtil {
	
	/**
	 * オブジェクトの配列をSetへ格納して返す
	 * @param obj
	 * @return
	 */
	public static Set array2Set(Object[] obj) {
		Set set = new HashSet();
		if (obj == null) return set;
		for (int i=0; i<obj.length; i++) {
			set.add(obj[i]);
		}
		return set;
	}

	/**
	 * 文字列配列から空文字列を除いて返す
	 * @param str
	 * @return
	 */
	public static String[] removeBlankString(String[] str) {
		if (str == null) return null;
		List c = new LinkedList();
		for (int i=0; i<str.length; i++) {
			if ("".equals(str[i])) continue;
			c.add(str[i]);
		}
		return (String[])c.toArray(new String[0]);
	}

	/**
	 * オブジェクトの配列をListへ格納して返す
	 * @param obj
	 * @return
	 */
	public static List array2List(Object[] obj) {
		List list = new ArrayList();
		if (obj == null) return list;
		for (int i=0; i<obj.length; i++) {
			list.add(obj[i]);
		}
		return list;
	}

	/**
	 * 文字列をカンマで分割して文字列配列にする
	 * ※空文字列も含む
	 * @param string
	 * @return
	 */
	public static String[] splitComma(String string) {
		List container = new LinkedList(); // 入れ物

		int index = 0;
		int start = 0;
		while ((index = string.indexOf(',', start)) != -1) {
			container.add(string.substring(start, index));
			start = index + 1;
		}
		container.add(string.substring(start, string.length()));

		return (String[]) container.toArray(new String[0]);
		
//		StringTokenizer st = new StringTokenizer(string, ",");
//		String[] array = new String[st.countTokens()];
//		for (int i=0; st.hasMoreElements();) {
//			array[i++] = st.nextToken();	
//		}
//		return array;
//		return StringUtils.split(string, ",");
	}

	/**
	 * 文字配列をカンマで結合して文字列にする
	 * @param array
	 * @return
	 */
	public static String deSplitComma(String[] array) {
		StringBuffer buff = new StringBuffer();
		for (int i=0; i<array.length; i++) {
			if (i > 0) buff.append(",");
			buff.append(array[i]);
		}
		return buff.toString();
	}
	
	/**
	 * カンマ区切り文字列をStringの配列で返す
	 * @param array
	 * @return
	 */
	public static String[] deconcatComma(String string) {
		String[] array = null;
		if(string != null && !string.trim().equals("")){
			StringTokenizer tokenizer = new StringTokenizer(string, ",");
			int tokenNum = tokenizer.countTokens();
			array = new String[tokenNum];
			while(tokenizer.hasMoreTokens( )) { 
				array[tokenNum - tokenizer.countTokens()] = tokenizer.nextToken( );
			}
		}
		return array;
	}
	
	/**
	 * 文字列配列から特定の文字列を削除して返す
	 * @param array 元になる文字列配列
	 * @param string 削除する文字
	 * @return
	 */
	public static String[] remove(String[] array, String string){
		String[] retArray = null;
		if(array != null && array.length > 0){
			List tempList = new ArrayList();
			tempList.addAll(Arrays.asList(array));
			ListIterator ite = tempList.listIterator();
			while(ite.hasNext()){
				String s = (String)ite.next();
				if(s != null && s.equals(string)){
					ite.remove();
				}
			}
			retArray = (String[])tempList.toArray(new String[tempList.size()]);
		}
		return retArray;
	}
	
	/**
	 * 文字列配列から特定の文字列群を削除して返す
	 * @param array 元になる文字列配列
	 * @param array2 削除する文字列配列
	 * @return
	 */
	public static String[] remove(String[] array, String[] array2){
		String[] retArray = array;
		for(int i=0; i<array2.length; i++){
			retArray = remove(retArray, array2[i]);
		}
		return retArray;
	}
	
	/**
	 * 文字列配列をキーとBoolean(true)を値に持つMapを作る
	 * 
	 * @return
	 */
	public static Map array2TrueMap(final String[] array) {

		final Map map = new HashMap();
		
		if (array == null) return map;
		
		for (int i = 0; i < array.length; i++) {
			map.put(array[i], Boolean.TRUE);
		}
		
		return map;
	}
	
}

/*
 * 作成日: 2004/07/23
 * 設問別成績推移グラフ表示用サーブレット
 *
 */
package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;
import java.sql.Connection;
import java.util.Collection;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.graph.LineGraphAppletQ;
import jp.co.fj.keinavi.beans.help.OnepointBean;
import jp.co.fj.keinavi.beans.individual.I108Bean;
import jp.co.fj.keinavi.data.individual.I108Data;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.individual.SubjectData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I108Form;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
/**
 * FILE：I108Servlet<BR>
 * 説明：設問別成績推移グラフを表示する。<BR>
 * 　　　プロファイルから以下のデータを取得する<BR>
 * 　　　　１、セキュリティスタンプ<BR>
 * 　　　　２、印刷対象チェックボックスデータ<BR>
 * 　　　　３、選択中の生徒１りOR全員<BR>
 * 　　　個人共通セッションから以下のデータを取得する<BR>
 * 　　　　１、対象模試<BR>
 * 　　　対象模試のデータを科目ごとにセット
 * @author totec.ishiguro
 *
 * 2009.10.27   Fujito URAKAWA - Totec
 *              「学力レベル」項目を生かした新メニュー対応
 *
 * 2016.1.18	QQ)Hisakawa
 * 				 サーブレット化対応の為、 src/main/webapp/applets から移動
 */
public class I108Servlet extends IndividualServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	protected void execute(
		final HttpServletRequest request,
		final HttpServletResponse response)
		throws ServletException, IOException{

		super.execute(request, response);

		//フォームデータの取得
		final I108Form i108Form = (I108Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.individual.I108Form");

		//プロファイル情報
		final Profile profile = getProfile(request);
		String bundleCd = profile.getBundleCD();
		Short printStamp = (Short) profile.getItemMap(IProfileCategory.I_SCORE_QUE).get(IProfileItem.PRINT_STAMP);
		Short printStudent = (Short) profile.getItemMap(IProfileCategory.I_COMMON).get(IProfileItem.PRINT_STUDENT);

		//個人共通
		final ICommonMap iCommonMap = getICommonMap(request);

		// 2016/01/18 QQ)Hisakawa 大規模改修 ADD START
        Collection cSubDatas = null;
        Collection cData = null;
        // 2016/01/18 QQ)Hisakawa 大規模改修 ADD END

		// 転送元がJSPなら対象模試情報を初期化
		if ("i108".equals(getBackward(request))) {
			initTargetExam(request);
		}

		// JSP転送
		if ("i108".equals(getForward(request))) {
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);

				//ワンポイントアドバイス
				OnepointBean onebean = new OnepointBean();
				onebean.setConnection(null, con);
				onebean.setScreenID("i108");
				onebean.execute();
				request.setAttribute("OnepointBean", onebean);

				I108Bean i108Bean = new I108Bean();

				// セッションに保持している学力レベル表示用Mapを取得する
				HashMap levelmap = iCommonMap.getIndSubScholarLevels();
				String levelkey = null;

				if(!"i108".equals(getBackward(request))
						|| !iCommonMap.getTargetPersonId().equals(i108Form.getTargetPersonId())
						|| isChangedMode(request) || isChangedExam(request)){

					// 生徒を切り替えたとき
					// モードを切り替えたとき
					// 対象模試を変えたとき
					if ("i108".equals(getBackward(request))){

						if(i108Form.getTargetSubCode().equals("")){
							/**前回表示した生徒の科目コードが空なので、科目を引き継がない**/
							i108Bean.setFirstAccess(true);
							i108Bean.setTargetBundleCd(bundleCd);
							i108Bean.setSearchCondition(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(),
														i108Form.getTargetPersonId());
							i108Bean.setDispScholarLevel("");	// 科目コードが「""」なので、学力レベルは「""」とする
							i108Bean.setConnection("", con);
							i108Bean.execute();

						} else {
							/**前回表示した生徒の科目コードがあるので、それを引き継ぐ**/
							i108Bean.setFirstAccess(false);
							i108Bean.setTargetBundleCd(bundleCd);
							i108Bean.setTargetSubCode(i108Form.getTargetSubCode());
							i108Bean.setSearchCondition(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(),
														i108Form.getTargetPersonId());
							// 前回表示した科目コードをBeanにセット
							i108Bean.setDispSubCode(i108Form.getDispSubCode());
							levelkey = iCommonMap.makeScholarLevelMapKey(iCommonMap.getTargetExamYear(),
									iCommonMap.getTargetExamCode(),	i108Form.getTargetPersonId(), i108Form.getTargetSubCode());
							i108Bean.setDispScholarLevel(selectScholarLevel(levelmap, levelkey));
							i108Bean.setConnection("", con);
							i108Bean.execute();

						}

						i108Form.setTargetSubCode(i108Bean.getTargetSubCode());
						i108Form.setTargetSubName(i108Bean.getTargetSubName());
						i108Form.setDispScholarLevel(i108Bean.getDispScholarLevel());

						//1.印刷対象生徒をフォームにセットしなおす
						String[] printTarget = {"0","0","0","0"};
						if(i108Form.getPrintTarget() != null){
							for(int i=0; i<i108Form.getPrintTarget().length; i++){
								printTarget[Integer.parseInt(i108Form.getPrintTarget()[i])] = "1";
							}
						}
						i108Form.setPrintTarget(printTarget);

					// 初回アクセス
					} else {

					    //個人成績分析共通-印刷対象生保存をセット
					    // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
					    //i108Form.setPrintStudent(printStudent.toString());
					    i108Form.setPrintStudent(iCommonMap.getPrintStudent());
					    // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END

						//1.印刷対象生徒をセット
						String printTarget = (String) profile.getItemMap(IProfileCategory.I_SCORE_COMMON).get(IProfileItem.PRINT_TARGET);
						i108Form.setPrintTarget(CollectionUtil.splitComma(printTarget));

						//4.セキュリティスタンプを保存
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
						//i108Form.setPrintStamp(printStamp.toString());
						i108Form.setPrintStamp(iCommonMap.getPrintStamp());
						// 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END

						i108Bean.setFirstAccess(true);
						i108Bean.setTargetBundleCd(bundleCd);
						i108Bean.setSearchCondition(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(),
													i108Form.getTargetPersonId());
						i108Bean.setConnection("", con);
						i108Bean.setDispScholarLevel("");	//初回アクセスなので学力レベルは「""」
						i108Bean.execute();
						//初回アクセス用フォーム
						i108Form.setTargetSubCode(i108Bean.getTargetSubCode());
						i108Form.setTargetSubName(i108Bean.getTargetSubName());
						i108Form.setDispScholarLevel(i108Bean.getTargetScholarLevel());
						//学力レベル一時格納用処理追加
						putICommonMapForScholarLevel(iCommonMap, i108Form, i108Bean);

					}

				// 再表示アクセス
				} else {
					i108Bean.setFirstAccess(false);
					i108Bean.setTargetBundleCd(bundleCd);
					i108Bean.setTargetSubCode(i108Form.getTargetSubCode());
					i108Bean.setTargetSubName(i108Form.getTargetSubName());
					i108Bean.setSearchCondition(iCommonMap.getTargetExamYear(), iCommonMap.getTargetExamCode(),
												i108Form.getTargetPersonId());

					//1.印刷対象生徒をフォームにセットしなおす
					String[] printTarget = {"0","0","0","0"};
					if(i108Form.getPrintTarget() != null){
						for(int i=0; i<i108Form.getPrintTarget().length; i++){
							printTarget[Integer.parseInt(i108Form.getPrintTarget()[i])] = "1";
						}
					}
					i108Form.setPrintTarget(printTarget);
					// 前回表示した科目コードをBeanにセット
					i108Bean.setDispSubCode(i108Form.getDispSubCode());
					// 再表示の際には、画面で設定した学力レベルをセットする。
					// 科目を変更した場合
					if(iCommonMap.getDispSubCd() != null &&
							!iCommonMap.getDispSubCd().equals(i108Form.getTargetSubCode())) {
						levelkey = iCommonMap.makeScholarLevelMapKey(iCommonMap.getTargetExamYear(),
								iCommonMap.getTargetExamCode(),	i108Form.getTargetPersonId(), i108Form.getTargetSubCode());
						i108Bean.setDispScholarLevel(selectScholarLevel(levelmap, levelkey));
					}
					// それ以外の場合
					else {
						i108Bean.setDispScholarLevel(i108Form.getDispScholarLevel());
					}
					i108Bean.setConnection("", con);
					i108Bean.execute();
					// 再表示学力レベルをセット
					i108Form.setDispScholarLevel(i108Bean.getDispScholarLevel());
					//学力レベル一時格納用処理追加
					putICommonMapForScholarLevel(iCommonMap, i108Form, i108Bean);

				}

				request.setAttribute("i108Bean", i108Bean);

                // 2016/01/18 QQ)Hisakawa 大規模改修 ADD START
				cSubDatas = i108Bean.getSubDatas();
				cData = i108Bean.getRecordSet();
                // 2016/01/18 QQ)Hisakawa 大規模改修 ADD END

			} catch (final Exception e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			request.setAttribute("form", i108Form);

			iCommonMap.setTargetPersonId(i108Form.getTargetPersonId());
			iCommonMap.setDispScholarLevel(i108Form.getDispScholarLevel());
			iCommonMap.setDispSubCd(i108Form.getTargetSubCode());

			// 2016/01/18 QQ)Hisakawa 大規模改修 ADD START
			//科目タブ切り替え
		    String dispSelect = "";	//グラフ表示／非表示
		    String sLineItemName = "校内平均";
		    I108Data Data = null;

		    java.util.Iterator it=cData.iterator();

		    Data = (I108Data) it.next();

		    if(iCommonMap.getTargetExamTypeCode() != "31") {
		    	sLineItemName += ",全国平均";
		    }

		    if(i108Form.getDispScholarLevel() != "") {
		    	sLineItemName += "," + i108Form.getDispScholarLevel() + "レベル";
		    }

		    java.util.Iterator cd = cSubDatas.iterator();
		    boolean flag = false;
		    while(cd.hasNext()) {
		        SubjectData data = (SubjectData)cd.next();
		        String temp = data.getSubjectCd();
		        if(temp.equals(i108Form.getTargetSubCode())) {
		            flag = true;
		            break;
		        }
		    }
		    if(flag==true){
		        dispSelect = "on";
		        i108Form.setDispSelect("on");
		    }else{
		        dispSelect = "off";
		        i108Form.setDispSelect("off");
		    }

            LineGraphAppletQ App = new LineGraphAppletQ();

            if(cData.size() != 0) {

	            App.setParameter("examName", String.valueOf(Data.getExamName()));
	            App.setParameter("dispSelect", dispSelect);
	            App.setParameter("yTitle", "得点率％");

	            if(iCommonMap.getTargetExamTypeCode() != "31") {
	            	App.setParameter("LineItemNum", "2");
	            } else {
	            	App.setParameter("LineItemNum", "1");
	            }

	            App.setParameter("LineItemNameS", "対象生徒");
	            App.setParameter("LineItemName", sLineItemName);
	            App.setParameter("examValueS", String.valueOf(Data.getExamValueS()));
	            App.setParameter("examValue0", String.valueOf(Data.getExamValue0()));

	            if(iCommonMap.getTargetExamTypeCode() != "31") {
	                App.setParameter("examValue1", String.valueOf(Data.getExamValue1()));
	            }

	            App.setParameter("examValueL", String.valueOf(Data.getExamValueL()));
	            App.setParameter("BarSize", "10");
	            App.setParameter("colorDAT", "1,0,13,14,13,14,2");

            } else {

	            App.setParameter("examName", "");
	            App.setParameter("dispSelect", "off");
	            App.setParameter("yTitle", "");
	            App.setParameter("LineItemNum", "0");
	            App.setParameter("LineItemNameS", "");
	            App.setParameter("LineItemName", "");
	            App.setParameter("examValueS", "");
	            App.setParameter("examValue0", "");
	            App.setParameter("examValue1", "");
	            App.setParameter("examValueL", "");
	            App.setParameter("BarSize", "10");
	            App.setParameter("colorDAT", "1,0,13,14,13,14");

            }

            App.init();

            HttpSession session = request.getSession(true);

            session.setAttribute("GraphServlet", App);
            session.setAttribute("GraphType", "LineGraphQ");

            App = null;
            // 2016/01/18 QQ)Hisakawa 大規模改修 ADD END

			super.forward(request, response, JSP_I108);

		// 不明ならServlet転送
		} else {

			if ("sheet".equals(getForward(request))) {
				/** 印刷ボタン押下時プロファイル保存 **/
				//分析モードなら個人成績分析共通-印刷対象生保存を保存
				if(iCommonMap.isBunsekiMode()){
					profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(i108Form.getPrintStudent()));
				}

				//1.印刷対象生徒を保存
				String[] printTarget = {"0","0","0","0"};
				if(i108Form.getPrintTarget() != null){
					for(int i=0; i<i108Form.getPrintTarget().length; i++){
						printTarget[Integer.parseInt(i108Form.getPrintTarget()[i])] = "1";
					}
				}
				profile.getItemMap(IProfileCategory.I_SCORE_COMMON).put(IProfileItem.PRINT_TARGET, CollectionUtil.deSplitComma(printTarget));

				//2.セキュリティスタンプを保存
				profile.getItemMap(IProfileCategory.I_SCORE_QUE).put(IProfileItem.PRINT_STAMP, new Short(i108Form.getPrintStamp()));

			} else {

				/** 不明な転送プロファイル保存 */
				//分析モードなら個人成績分析共通-印刷対象生保存を保存
				if(iCommonMap.isBunsekiMode()){
					profile.getItemMap(IProfileCategory.I_COMMON).put(IProfileItem.PRINT_STUDENT, new Short(i108Form.getPrintStudent()));
				}

				//1.印刷対象生徒を保存
				String[] printTarget = {"0","0","0","0"};
				if(i108Form.getPrintTarget() != null){
					for(int i=0; i<i108Form.getPrintTarget().length; i++){
						printTarget[Integer.parseInt(i108Form.getPrintTarget()[i])] = "1";
					}
				}
				profile.getItemMap(IProfileCategory.I_SCORE_COMMON).put(IProfileItem.PRINT_TARGET, CollectionUtil.deSplitComma(printTarget));

			}

			iCommonMap.setTargetPersonId(i108Form.getTargetPersonId());
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD START
                        iCommonMap.setPrintStamp(i108Form.getPrintStamp());
                        iCommonMap.setPrintStudent(i108Form.getPrintStudent());
                        // 2020/04/20 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD END
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

	/**
	 * ICommonMap内の学力レベルを選択する。
	 * @param levelMap2
	 * @param levelkey
	 * @param form
	 * @return
	 */
	private String selectScholarLevel(HashMap lvlMap, String lvlkey) {

		String lvl = new String();

		// 画面表示学力レベルを保持してる場合（画面で選択した学力レベル）
		if (lvlMap.containsKey(lvlkey)) {
			lvl = (String)lvlMap.get(lvlkey);
		}
		// 画面表示学力レベルを保持していない場合（対象生徒の学力レベル）
		else {
			lvl = "";
		}

		return lvl;

	}

	/**
	 * ICommonMapに画面で選択した学力レベル情報を格納する。
	 * @param commap iCommonMap
	 * @param form I108画面から取得した亜短
	 * @param bean I108Bean
	 */
	private void putICommonMapForScholarLevel(ICommonMap commap, I108Form form, I108Bean bean) {

		// 画面に表示する学力レベルが「""」ならば、セッションにセットしない。
		if ("".equals(form.getDispScholarLevel())) return;

		String key = new String();
		// レベル情報格納用キー作成
		key = commap.makeScholarLevelMapKey(commap.getTargetExamYear(),
				commap.getTargetExamCode(),	form.getTargetPersonId(), form.getTargetSubCode());
		// セッションに格納されているレベル情報を取得する
		HashMap levelmap = commap.getIndSubScholarLevels();
		// レベル情報再設定
		levelmap.put(key, bean.getDispScholarLevel());
		// 格納情報をセッションへセット
		commap.setIndSubScholarLevels(levelmap);

	}

}

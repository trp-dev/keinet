package jp.co.fj.keinavi.servlets.maintenance;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.admin.LastDateBean;
import jp.co.fj.keinavi.beans.maintenance.StudentCSVRegisterBean;
import jp.co.fj.keinavi.beans.recount.RecountLockBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.maintenance.M104Form;
import jp.co.fj.keinavi.util.csv.CSVRegisterBean;

/**
 *
 * 生徒情報一括登録画面サーブレット
 * 
 * 2005.10.21	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M104Servlet extends AbstractMServlet {
	
	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {
		
		// アクションフォーム
		final M104Form form = (M104Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.maintenance.M104Form");
		// HTTPセッション
		final HttpSession session = request.getSession(false);
		
		// JSPへの遷移処理
		if ("m104".equals(form.getForward())) {

			// ログイン情報
			final LoginSession login = getLoginSession(request);
			
			Connection con = null;
			CSVRegisterBean bean = null;
			try {
				con = getConnectionPool(request);
				
				// 再集計ロック失敗
				if (!RecountLockBean.lock(con, login.getUserID())) {
					con.rollback();
					request.setAttribute("ErrorMessage",
							RecountLockBean.getErrorMessage());
				} else {
					// データ更新日
					LastDateBean.setLastDate1(con, login.getUserID());
					
					bean = new CSVRegisterBean(
							form.getFile(),
							new StudentCSVRegisterBean(login.getUserID()), 11);
					bean.setConnection(null, con);
					bean.execute();

					// 登録処理が済んでいるならコミット
					if (bean.isCommitted()) {
						con.commit();
						actionLog(request, "801");
					} else {
						con.rollback();
					}
				}
			} catch (final Exception e) {
				rollback(con);
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}
			
			request.setAttribute("form", form);

			// 再集計中
			if (bean == null) {
				forward2Jsp(request, response, "m102");
			// コミットされたなら完了画面へ
			} else if (bean.isCommitted()) {
				initStudentSession(request);
				request.setAttribute("CSVRegisterBean", bean);
				forward2Jsp(request, response, "m104");
			// エラーを含むならエラー画面へ
			} else if (bean.hasError()) {
				request.setAttribute("CSVRegisterBean", bean);
				forward2Jsp(request, response, "m103");
			// それ以外は更新確認画面へ
			} else {
				session.setAttribute(CSVRegisterBean.SESSION_KEY, bean);
				forward2Jsp(request, response, "m106");
			}
		// 不明なら転送
		} else {
			dispatch(request, response);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#getBackward(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	protected String getBackward(final HttpServletRequest request) {
		return "m102";
	}

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#getForward(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	protected String getForward(final HttpServletRequest request) {
		return "m104";
	}
	
}

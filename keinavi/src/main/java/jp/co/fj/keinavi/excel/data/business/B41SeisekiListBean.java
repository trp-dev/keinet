package jp.co.fj.keinavi.excel.data.business;

//import org.apache.struts.action.ActionForm;
//import java.util.ArrayList;
/**
 * ���Z���ѕ��́|���Z�Ԕ�r�|���ъT�� ���Z�f�[�^���X�g
 * �쐬��: 2004/08/03
 * @author	Ito.Y
 */
public class B41SeisekiListBean {
	//�w�Z��
	private String strGakkomei = "";
	//�͎��N�x�i���N�x�j
	private String strNendoNow = "";
	//���N�x�l��
	private int intNinzuNow = 0;
	//���N�x���ϓ_
	private float floHeikinNow = 0;
	//���N�x���ϕ΍��l
	private float floStdHensaNow = 0;
	//�͎��N�x�i�O�N�x�j
	private String strNendoPast = "";
	//�O�N�x�l��
	private int intNinzuPast = 0;
	//�O�N�x���ϓ_
	private float floHeikinPast = 0;
	//�O�N�x���ϕ΍��l
	private float floStdHensaPast = 0;

	
	/*----------*/
	/* Get      */
	/*----------*/
	public float getFloHeikinNow() {
		return floHeikinNow;
	}
	public float getFloHeikinPast() {
		return floHeikinPast;
	}
	public float getFloStdHensaNow() {
		return floStdHensaNow;
	}
	public float getFloStdHensaPast() {
		return floStdHensaPast;
	}
	public int getIntNinzuNow() {
		return intNinzuNow;
	}
	public int getIntNinzuPast() {
		return intNinzuPast;
	}
	public String getStrGakkomei() {
		return strGakkomei;
	}
	public String getStrNendoNow() {
		return strNendoNow;
	}
	public String getStrNendoPast() {
		return strNendoPast;
	}


	/*----------*/
	/* Set      */
	/*----------*/
	public void setFloHeikinNow(float f) {
		floHeikinNow = f;
	}
	public void setFloHeikinPast(float f) {
		floHeikinPast = f;
	}
	public void setFloStdHensaNow(float f) {
		floStdHensaNow = f;
	}
	public void setFloStdHensaPast(float f) {
		floStdHensaPast = f;
	}
	public void setIntNinzuNow(int i) {
		intNinzuNow = i;
	}
	public void setIntNinzuPast(int i) {
		intNinzuPast = i;
	}
	public void setStrGakkomei(String string) {
		strGakkomei = string;
	}
	public void setStrNendoNow(String string) {
		strNendoNow = string;
	}
	public void setStrNendoPast(String string) {
		strNendoPast = string;
	}

}

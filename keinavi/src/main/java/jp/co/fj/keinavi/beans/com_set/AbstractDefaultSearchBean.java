/*
 * 作成日: 2004/10/14
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.com_set;

import com.fjh.beans.DefaultSearchBean;

/**
 * 検索処理用Bean
 * 
 * @author kawai
 */
abstract public class AbstractDefaultSearchBean extends DefaultSearchBean {

	private boolean init; // ページ番号またはページサイズを初期化したかどうか
	private int count; // レコードのカウンタ
	private int start; // ページ開始レコード番号
	private int end; // ページ終了レコード番号
	
	/**
	 * 変数の初期化をする
	 * ページ番号とページサイズが初期化されている必要がある
	 */
	private void init() {
		if (this.init) {
			this.end = this.pageSize * this.pageNo;
			this.start = this.end - this.pageSize;
		} else {
			this.init = true;
		}
	}

	/**
	 * 画面に表示するアイテムかどうか
	 * @param count レコード番号
	 * @return
	 */
	protected boolean isOutputItem() {
		this.count++;
		return this.count > this.start && this.count <= this.end;
	}

	/**
	 * ページの計算をする
	 * @throws Exception
	 */
	protected void calculate() throws Exception {
		super.setPageNo(this.pageNo); 
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#setPageNo(int)
	 */
	public void setPageNo(int pageNo) throws Exception {
		super.setPageNo(pageNo);
		this.init();
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#setPageSize(int)
	 */
	public void setPageSize(int pageSize) {
		super.setPageSize(pageSize);
		this.init();
	}

}

/**
 * クラス成績分析−クラス成績概況−個人成績（型・科目別成績順位）
 * 	Excelファイル編集
 * 作成日: 2004/09/02
 * @author	Ito.Y
 *
 * 2009.11.24   Fujito URAKAWA - Totec
 *              「学力レベル」追加対応
 */

package jp.co.fj.keinavi.excel.cls;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.cls.C12Item;
import jp.co.fj.keinavi.excel.data.cls.C12JyuniListBean;
import jp.co.fj.keinavi.excel.data.cls.C12JyuniPersonalListBean;
import jp.co.fj.keinavi.excel.data.cls.C12KmkListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class C12_04 {

	private int noerror	= 0;		// 正常終了
	private int errfread	= 1;		// ファイルreadエラー
	private int errfwrite	= 2;		// ファイルwriteエラー
	private int errfdata	= 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	final private String	masterfile0	= "C12_04";	// ファイル名
	final private String	masterfile1	= "C12_04";	// ファイル名
	private String	masterfile	= "";				// ファイル名
	final private int[]	tabCol		= {0,12,24};	// 表の基準点(今年度)


	/*
	 * 	Excel編集メイン
	 * 		C12Item c12Item: データクラス
	 * 		String outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
 	public int c12_04EditExcel(C12Item c12Item, ArrayList outfilelist, int intSaveFlg, String UserID) {
		KNLog log = KNLog.getInstance(null,null,null);
		log.Ep("C12_04","C12_04帳票作成開始","");

		//テンプレートの決定
		if (c12Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		try {
			HSSFWorkbook	        workbook	= null;
			HSSFSheet		workSheet	= null;
			HSSFRow			workRow		= null;
			HSSFCell		workCell	= null;

			int		row				= 0;	// 行
			int		col				= 0;	// 列
			String		kmk				= "";	// 科目チェック用
			int		maxSheetIndex	= 0;	// シートカウンター
			int		maxSheetNum		= 50;	// 最大シート数
			int		maxRowNum		= 50;	// 行最大数
			int		maxColNum		= 3;	// 列最大数
			int		fileIndex		= 1;	// ファイルカウンター
			boolean	bolSheetCngFlg	= true;// true:改表実行　（改シートフラグ）
			boolean	bolBookCngFlg	= true;// true:改表実行　（改ブックフラグ）
			boolean	kataFlg			= true;// false:改行済(型・科目名変更済)　

			// 基本ファイルを読込む
			C12JyuniListBean c12JyuniListBean = new C12JyuniListBean();

			// データセット
			ArrayList	c12JyuniList	= c12Item.getC12JyuniList();
			Iterator	itr		= c12JyuniList.iterator();

			/** 学年・クラス情報 **/
			// c12Listのセット
			while( itr.hasNext() ) {
				c12JyuniListBean = (C12JyuniListBean)itr.next();

				// 基本ファイルを読み込む
				C12KmkListBean	c12KmkListBean	= new C12KmkListBean();

				//データの保持
				ArrayList	c12KmkList	= c12JyuniListBean.getC12KmkList();
				Iterator	itrC12Kmk	= c12KmkList.iterator();

				/** 型・科目情報 **/
				row = 0;
				col=0;
				while(itrC12Kmk.hasNext()){
					c12KmkListBean = (C12KmkListBean) itrC12Kmk.next();

					// 改表判断（型・科目の切替わり時は改表する）
					if (row!=0) {
						col++;
						if (col>=maxColNum) {
							col=0;
						}
					}

					// 改シート判断（型・科目の切替わり時は改シートする）
					if (c12KmkListBean.getStrKmkCd().compareTo("7000") < 0 && kataFlg) {
						row = 0;
						col = 0;
						kataFlg = false;
					}

					if ( col==0 ) {
						bolSheetCngFlg = true;
						if(maxSheetIndex>=maxSheetNum){
							bolBookCngFlg = true;
						}
					}

					// 基本ファイルを読み込む
					C12JyuniPersonalListBean	c12JyuniPersonalListBean	= new C12JyuniPersonalListBean();

					//データの保持
					ArrayList	c12JyuniPersonalList	= c12KmkListBean.getC12JyuniPersonalList();
					Iterator	itrC12JyuniPersonal		= c12JyuniPersonalList.iterator();

					/** 個人順位情報 **/
					row = 0;
					while(itrC12JyuniPersonal.hasNext()){
						c12JyuniPersonalListBean = (C12JyuniPersonalListBean) itrC12JyuniPersonal.next();

						//マスタExcel読み込み
						if(bolBookCngFlg){
							if(maxSheetIndex>=maxSheetNum){

								// Excelファイル保存
								boolean bolRet = false;
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, maxSheetIndex);
								fileIndex++;

								if( bolRet == false ){
									return errfwrite;
								}
							}

							if((maxSheetIndex>=maxSheetNum)||(maxSheetIndex==0)){
								workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
								if( workbook==null ){
									return errfread;
								}
								bolBookCngFlg = false;
								maxSheetIndex=0;
							}
						}

						if ( bolSheetCngFlg ) {

							// シートテンプレートのコピー
							workSheet = workbook.cloneSheet(0);
							maxSheetIndex++;

							// ヘッダ右側に帳票作成日時を表示する
							cm.setHeader(workbook, workSheet);

							// セキュリティスタンプセット
							String secuFlg = cm.setSecurity( workbook, workSheet, c12Item.getIntSecuFlg() ,31 ,34 );
							workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
							workCell.setCellValue(secuFlg);

							// ラベルセット
							if (c12Item.getIntShubetsuFlg() == 1){
							} else{
								workCell = cm.setCell( workSheet, workRow, workCell, 5, 9 );
								workCell.setCellValue( "全国\n偏差値" );
								workCell = cm.setCell( workSheet, workRow, workCell, 5, 21 );
								workCell.setCellValue( "全国\n偏差値" );
								workCell = cm.setCell( workSheet, workRow, workCell, 5, 33 );
								workCell.setCellValue( "全国\n偏差値" );
							}

							// 学校名セット
							String strGakkoMei	="学校名　：" + c12Item.getStrGakkomei()
												+ "　学年：" + c12JyuniListBean.getStrGrade()+ "　クラス名：" + c12JyuniListBean.getStrClass();
							workCell = cm.setCell( workSheet, workRow, workCell, 1, 0 );
							workCell.setCellValue( strGakkoMei );

							// 対象模試セット
							String moshi =cm.setTaisyouMoshi( c12Item.getStrMshDate() );	// 模試月取得
							workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
							workCell.setCellValue( cm.getTargetExamLabel() + "：" + c12Item.getStrMshmei() + moshi);

							//表示対象セット
							String hyoji ="表示対象：選択クラス全員";
							workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
                                                        workCell.setCellValue( hyoji);


							//　改シートフラグをFalseにする
							bolSheetCngFlg	=false;
							row=0;
						}
						if ( (col==0 && row==0) || (row==0 && !cm.toString(kmk).equals(cm.toString(c12KmkListBean.getStrKmkmei()))) ) {
							// 型・科目＋配点セット
							String haiten = "";
							if ( !cm.toString(c12KmkListBean.getStrHaitenkmk()).equals("") ) {
								haiten = "（" + c12KmkListBean.getStrHaitenkmk() + "）";
							}
							workCell = cm.setCell( workSheet, workRow, workCell, 4, tabCol[col] );
							workCell.setCellValue( "型・科目：" + c12KmkListBean.getStrKmkmei() + haiten );

							// 受験人数セット
							workCell = cm.setCell( workSheet, workRow, workCell, 4, tabCol[col]+5 );
							if ( c12KmkListBean.getIntNinzu() == -999 ) {
								workCell.setCellValue( "受験人数：");
							} else {
								workCell.setCellValue( "受験人数：" + c12KmkListBean.getIntNinzu() + "人");
							}
						}

						// 校内順位セット
						if(  c12JyuniPersonalListBean.getIntJyuni() != -999 ){
							workCell = cm.setCell( workSheet, workRow, workCell, 7+row, tabCol[col] );
							workCell.setCellValue( c12JyuniPersonalListBean.getIntJyuni());
						}

						// 学年セット
						workCell = cm.setCell( workSheet, workRow, workCell, 7+row, tabCol[col]+1 );
						workCell.setCellValue( c12JyuniPersonalListBean.getStrGrade() );

						// クラスセット
						workCell = cm.setCell( workSheet, workRow, workCell, 7+row, tabCol[col]+2 );
						workCell.setCellValue( c12JyuniPersonalListBean.getStrClass() );

						// クラス番号セット
						workCell = cm.setCell( workSheet, workRow, workCell, 7+row, tabCol[col]+3 );
						workCell.setCellValue( c12JyuniPersonalListBean.getStrClassNo() );

						// 氏名セット
						workCell = cm.setCell( workSheet, workRow, workCell, 7+row, tabCol[col]+4 );
						workCell.setCellValue( c12JyuniPersonalListBean.getStrKanaName() );

						// 性別セット
						int sexKbn = Integer.parseInt( c12JyuniPersonalListBean.getStrSex() );
						String strSex = "";
						if( sexKbn == 1 ){
							strSex = "男";
						}else if( sexKbn == 2 ){
							strSex = "女";
						}else{		//}else if( sexKbn == 9 ){
							strSex = "不";
						}
						workCell = cm.setCell( workSheet, workRow, workCell, 7+row, tabCol[col]+6 );
						workCell.setCellValue( strSex );

						// 得点セット
						if(  c12JyuniPersonalListBean.getIntTokuten() != -999 ){
							workCell = cm.setCell( workSheet, workRow, workCell, 7+row, tabCol[col]+7 );
							workCell.setCellValue( c12JyuniPersonalListBean.getIntTokuten());
						}

						// 校内偏差セット
						if(  c12JyuniPersonalListBean.getFloHensaHome() != -999.0 ){
							workCell = cm.setCell( workSheet, workRow, workCell, 7+row, tabCol[col]+8 );
							workCell.setCellValue( c12JyuniPersonalListBean.getFloHensaHome());
						}

						// 全国偏差セット
						if(  c12JyuniPersonalListBean.getFloHensaAll() != -999.0 ){
							workCell = cm.setCell( workSheet, workRow, workCell, 7+row, tabCol[col]+9 );
							workCell.setCellValue( c12JyuniPersonalListBean.getFloHensaAll());
						}

						// 学力レベルセット
						if(  !"".equals(c12JyuniPersonalListBean.getStrScholarLevel())){
							workCell = cm.setCell( workSheet, workRow, workCell, 7+row, tabCol[col]+10 );
							workCell.setCellValue( c12JyuniPersonalListBean.getStrScholarLevel());
						}

						kmk = c12KmkListBean.getStrKmkmei();
						row++;
						if(row>=maxRowNum){
							col++;
							row=0;
							if(col>=maxColNum){
								col=0;
								bolSheetCngFlg=true;
								if(maxSheetIndex>=maxSheetNum){
									bolBookCngFlg=true;
								}
							}
						}
					}
				}
			}

			// Excelファイル保存
			boolean bolRet = false;
			if(fileIndex != 1){
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, fileIndex, masterfile, maxSheetIndex);
			}
			else{
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, maxSheetIndex);
			}

			if( bolRet == false ){
				return errfwrite;
			}

		} catch(Exception e) {
			log.Err("C12_04","データセットエラー",e.toString());
			return errfdata;
		}

		log.Ep("C12_04","C12_04帳票作成終了","");
		return noerror;
	}
}
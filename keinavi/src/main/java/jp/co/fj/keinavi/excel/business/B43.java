/**
 * 高校成績分析−高校間比較
 * 	Excelファイル編集
 * 作成日: 2004/08/19
 * @author	Ito.Y
 */
 
package jp.co.fj.keinavi.excel.business;

import java.util.*;

import jp.co.fj.keinavi.excel.data.business.*;
import jp.co.fj.keinavi.util.log.*;

public class B43 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean b43(B43Item b43Item, ArrayList outfilelist, int intSaveFlg, String UserID, KNSheetLog sheetLog) {
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		try{
			int ret = 0;
			//B43Itemから各帳票を出力
			if ( b43Item.getIntHyouFlg()==0 ) {
				throw new Exception("B43 ERROR : フラグ異常");
			}
			if ( b43Item.getIntHyouFlg()==1 ) {
				B43_01 exceledit = new B43_01();
				ret = exceledit.b43_01EditExcel( b43Item, outfilelist, intSaveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"B43_01","帳票作成エラー","");
					return false;
				}
				sheetLog.add("B43_01");
			}
						
		} catch(Exception e) {
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
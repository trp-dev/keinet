/*
 * 作成日: 2004/07/23
 * 成績推移グラフ（設問別推移）の配点・得点データ取得用Bean
 */
package jp.co.fj.keinavi.beans.individual;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.beans.ScholarLevelBean;
import jp.co.fj.keinavi.data.individual.I108Data;
import jp.co.fj.keinavi.data.individual.QuestionData;
import jp.co.fj.keinavi.data.individual.SubjectData;
import jp.co.fj.keinavi.util.GeneralUtil;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 設問別成績グラフ<BR>
 * DBより偏差値を取得し、すべてのデータをコレクションに入れる。
 * @author TOTEC.ishiguro
 *
 *	2005.05.24 kondo	[1]設問名が無かった場合も、アプレット用データの偏差値を設定するように修正
 *  2005.09.16 kondo    [2]科目取得の際EXAMSUBJECTではなく、Viewを参照するように修正
 *  2005.11.15 kondo    [3]ソート順を（DISPLAY_NO、QUESTION_NO）の順にする
 *  2009.10.14   Fujito URAKAWA - Totec
 *              「全体成績」追加対応
 *  2009.10.26   Fujito URAKAWA - Totec
 *              「学力レベル」項目を生かした新メニュー対応
 *
 *  2016.02.29   Hiroyuki Nishiyama - QuiQ soft
 *               getQuestionSql1のソートキーを明示的に指定
 *
 */
public class I108Bean extends DefaultBean {

	private static final long serialVersionUID = -4959373091402987300L;

	private String SUB_SQL_BASE = "";

	private String questionSql;
	private String subNumSql;

	/** 入力パラメター */
	private String targetExamYear;		//対象模試年度
	private String targetExamCode;		//対象模試コード
	private String personId;			//個人ＩＤ

	private String targetSubCode;		//対象教科コード
	private String targetSubName;		//対象教科名

	private String targetBundleCd;		//対象一括コード

	private boolean isFirstAccess;

	private Collection subDatas;		//教科リスト

	private Collection levelDatas;		//学力レベルリスト

	private String targetScholarLevel;	//対象生徒・対象科目学力レベル
	private String dispScholarLevel;	//画面表示学力レベル

	private String dispSubCode;		//画面表示科目コード

	/**
	 * 結果一覧（配列のほうが都合がよい）
	 */
	private List recordSet;

	/**
	 * 表示を分割すべき基準値
	 */
	private final int MOD_NUM = 11;

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		try{

			/** 学力レベル情報を取得する */
			selectLevelNames(conn);

			/** 科目名・科目情報　取得 */
			setSubDatas(execSubNames(getTargetExamYear(), getTargetExamCode(), getPersonId()));

			/** 対象生徒・対象科目の学力レベルを取得する */
			selectScholarLevel( getPersonId(), getTargetExamYear(), getTargetExamCode(),getTargetSubCode());

			/** 設問情報の取得 */
			List questionList = execQuestionDatas();

			/** 以上で取得したデータを表示用に配列に詰めなおす */
			/** 取得した配列をMOD_NUMを基準値にして分割してリストに詰める */
			recordSet = toDispArrayList(questionList, MOD_NUM);

			/** 取得したデータにアプレット用の値を付加する */
			prepareForApplet(recordSet);

		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 対象生徒・対象科目の学力レベルを取得する。
	 * @param personId2
	 * @param examyear
	 * @param examcd
	 * @param subcd
	 */
	private void selectScholarLevel(String id, String examyear, String examcd, String subcd) {

		PreparedStatement ps = null;
		ResultSet rs = null;

		// 対象生徒・対象科目の学力レベルを取得する
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("i12_03_3").toString());
			// パラメータをセット
			ps.setString(1, id);			// 個人ID
			ps.setString(2, examyear);		// 模試年度
			ps.setString(3, examcd);		// 模試コード
			ps.setString(4, subcd);			// 科目コード

			rs = ps.executeQuery();

			if (rs.next()) {
				setTargetScholarLevel(GeneralUtil.toBlank(rs.getString("ACADEMICLEVEL")));
			}
			else {
				setTargetScholarLevel("");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			DbUtils.closeQuietly(null, ps, rs);
		}



	}

	/**
	 * 学力レベル名を取得する
	 * @param con
	 * @throws SQLException
	 */
	private void selectLevelNames(Connection con) throws SQLException {

		ScholarLevelBean levelbean = ScholarLevelBean.getInstance(con);
		setLevelDatas(levelbean.getScholarLevelList());

	}

	/**
	 * アプレット表示に必要なデータをあらかじめ付随させる
	 * @param list
	 * @return
	 */
	private void prepareForApplet(List list){
		Iterator ite = list.iterator();
		while(ite.hasNext()){
			String questionName = "";	//設問名
			String scoreRate = "";		//得点率
			String avgscoreRte_L = "";	//レベル別得点率（全国）
			String avgscoreRte_S = "";	//得点率（校内）
			String avgscoreRte_A = "";	//得点率（全国）
			String dispSelect = "on";	//グラフ表示／非表示
			I108Data data = (I108Data)ite.next();
			for(int i=0; i<data.getTableData()[0].length; i++){
				if(data.getTableData()[1][i] == null) {
					//nullなら何もしない
				}else{
					if(i != 0){//0以上だったらカンマをつける
						questionName 	+= ",";//設問名
						scoreRate 		+= ",";//得点率
						avgscoreRte_L 	+= ",";//レベル別平均得点率（全国）
						avgscoreRte_S 	+= ",";//得点率（校内）
						avgscoreRte_A 	+= ",";//得点率（全国）
					}
					if(data.getTableData()[1][i].equals("")) {
						questionName 	+= "　";//設問名
					} else {
						questionName 	+= data.getTableData()[1][i];//設問名
					}
					if(data.getTableData()[4][i].equals("")) {
						scoreRate 		+= "-999.0f";//得点率
					} else {
						scoreRate 		+= data.getTableData()[4][i];//得点率
					}
					if(data.getTableData()[5][i].equals("") || i == 0) {	// 全体成績はAppletに表示しない条件を追加
						avgscoreRte_L   += "-999.0f";//レベル別平均得点率（全国）
					} else {
						avgscoreRte_L   += data.getTableData()[5][i];//レベル別平均得点率（全国）
					}
					if(data.getTableData()[7][i].equals("") || i == 0) {	// 全体成績はAppletに表示しない条件を追加
						avgscoreRte_S 	+= "-999.0f";//得点率（校内）
					} else {
						avgscoreRte_S 	+= data.getTableData()[7][i];//得点率（校内）
					}
					if(data.getTableData()[9][i].equals("") || i == 0) {	// 全体成績はAppletに表示しない条件を追加) {
						avgscoreRte_A 	+= "-999.0f";//得点率（全国）
					} else {
						avgscoreRte_A 	+= data.getTableData()[9][i];//得点率（全国）
					}
//[1] add end
				}
			}
			//対象生徒が対象もしを受けていないときに、グラフを表示しない。
			if(data.getTableData()[0].length == 0) {
				dispSelect = "off";
			}
			data.setExamName(questionName);
			data.setDispSelect(dispSelect);
			data.setExamValueS(scoreRate);
			data.setExamValue0(avgscoreRte_S);
			data.setExamValue1(avgscoreRte_A);
			data.setExamValueL(avgscoreRte_L);
		}
	}

	/**
	 * 表示が縦にループするから, 表示用にquestionDatasをArrayに変換する。
	 * このときついでに、MOD_NUMより少ない場合は空白を挿入して、
	 * 多いときはMOD_NUMの値で複数個に分割する。
	 *
	 * @param list
	 * @return
	 */
	private List toDispArrayList(List collection, int MOD_NUM){
		List returnList = new ArrayList();
		I108Data data = null;
		int x = 0;
		for (java.util.Iterator it=collection.iterator(); it.hasNext();) {
			QuestionData qdata = (QuestionData)it.next();
			if(x%MOD_NUM == 0){//MOD_NUMで割り切れたらNEW、リストに詰める
				data = new I108Data();
				data.setTableData(new String[10][MOD_NUM]);
				returnList.add(data);
				x = 0;
			}
			data = (I108Data) returnList.get(returnList.size()-1);
			//data.getTableData()[0][x] = qdata.getDisplay_no();		//設問番号
			data.getTableData()[0][x] = qdata.getDisplay_no();		//設問番号
			data.getTableData()[1][x] = qdata.getQuestionName1();	//設問名
			data.getTableData()[2][x] = qdata.getQallotpnt();		//配点
			data.getTableData()[3][x] = qdata.getQscore();			//対象生徒（得点）
			data.getTableData()[4][x] = qdata.getScoreRate();		//対象生徒（得点率）
			data.getTableData()[5][x] = qdata.getAvgScoreRate_LEVEL();//学力レベル別得点率
			data.getTableData()[6][x] = qdata.getAvgPnt_S();		//校内平均（得点）
			data.getTableData()[7][x] = qdata.getAvgScoreRate_S();	//校内平均（得点率）
			data.getTableData()[8][x] = qdata.getAvgPnt_A();		//全国平均（得点）
			data.getTableData()[9][x] = qdata.getAvgScoreRate_A();	//全国平均（得点率）
			x ++;
		}
		//最後のtableDataのサイズがMOD_NUMより小さかったら、空白でうめる
		if(data != null){
			if((x+1) < MOD_NUM){
				for(int i= x; i < MOD_NUM; i++){
					data.getTableData()[0][i] = "";
					data.getTableData()[1][i] = "";
					data.getTableData()[2][i] = "";
					data.getTableData()[3][i] = "";
					data.getTableData()[4][i] = "";
					data.getTableData()[5][i] = "";
					data.getTableData()[6][i] = "";
					data.getTableData()[7][i] = "";
					data.getTableData()[8][i] = "";
					data.getTableData()[9][i] = "";
				}
			}
		}
		return returnList;
	}
	/**
	 * 初回アクセス時は受験した科目一覧を取得する必要があります
	 * execute()を実行する前に条件をしていする
	 * @param string1
	 * @param string2
	 * @param stringss
	 * @param string3
	 */
	public void setSearchCondition(String str1, String str2,String str3){

		setTargetExamYear(str1);
		setTargetExamCode(str2);
		setPersonId(str3);

		//:::: SQL文の作成 ::::::::::

		//設問ＳＱＬの作成
		setQuestionSql(getQuestionSql1());
//		setQuestionSql(QESTION_SQL_BASE);
//		appendQuestionSql(getQuestionCondition(getPersonId(), getTargetExamYear(), getTargetExamCode(), getTargetBundleCd()));

		setSubNumSql(SUB_SQL_BASE);
	}

	/**
	 * 二回目以降のアクセス時
	 * execute()を実行する前に条件をしていする
	 * @param string1
	 * @param string2
	 * @param stringss
	 * @param b
	 * @param string3
	 */
	public void setSearchCondition(String strs, String str1, String str2,String str3){

		setTargetExamCode(strs);
		setSearchCondition(str1, str2, str3);

	}

	/**
	 * 設問情報の取得SQL
	 * @return
	 */
	private String getQuestionSql1() {
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("SELECT ");
		strBuf.append(" /*+ INDEX(QI PK_QRECORD_I) */ ");
		strBuf.append("EQ.DISPLAY_NO, ");
		strBuf.append("EQ.QUESTIONNAME1, ");
		strBuf.append("EQ.QALLOTPNT, ");
		strBuf.append("QI.QSCORE, ");
		strBuf.append("QI.SCORERATE, ");
		strBuf.append("QI.A_DEVIATION, ");
		strBuf.append("QRZA.AVGSCORERATE AVGSCORERATE_LEVEL, ");
		strBuf.append("QS.AVGPNT AVGPNT_S, ");
		strBuf.append("QS.AVGSCORERATE AVGSCORERATE_S, ");
		strBuf.append("QA.AVGPNT AVGPNT_A, ");
		strBuf.append("QA.AVGSCORERATE AVGSCORERATE_A ");
		strBuf.append("FROM ");
		strBuf.append("( ");
		strBuf.append("SELECT ");
		strBuf.append("EXAMYEAR, ");
		strBuf.append("EXAMCD, ");
		strBuf.append("SUBCD, ");
		strBuf.append("QUESTION_NO, ");
		strBuf.append("QALLOTPNT, ");
		strBuf.append("QUESTIONNAME1, ");
		strBuf.append("DISPLAY_NO ");
		strBuf.append("FROM EXAMQUESTION ");
		strBuf.append("WHERE SUBCD = ? ");//科目コード
		strBuf.append("AND EXAMYEAR = ? ");//模試年度
		strBuf.append("AND EXAMCD = ? ");//模試コード
		strBuf.append(") EQ  ");
		strBuf.append("LEFT JOIN ");
		strBuf.append("QRECORD_I QI ");
		strBuf.append("ON QI.EXAMYEAR = EQ.EXAMYEAR ");
		strBuf.append("AND QI.EXAMCD = EQ.EXAMCD ");
		strBuf.append("AND QI.SUBCD = EQ.SUBCD ");
		strBuf.append("AND QI.QUESTION_NO = EQ.QUESTION_NO ");
		strBuf.append("AND QI.INDIVIDUALID = ? ");//個人ID
		strBuf.append("LEFT JOIN QRECORDZONE_A QRZA ");
		strBuf.append("ON QRZA.EXAMYEAR = EQ.EXAMYEAR AND ");
		strBuf.append("QRZA.EXAMCD = EQ.EXAMCD AND ");
		strBuf.append("QRZA.SUBCD = EQ.SUBCD AND ");
		strBuf.append("QRZA.QUESTION_NO = EQ.QUESTION_NO AND ");
		strBuf.append("QRZA.DEVZONECD = ? ");
		strBuf.append("LEFT JOIN QRECORD_S QS ");
		strBuf.append("ON QS.EXAMYEAR = EQ.EXAMYEAR ");
		strBuf.append("AND QS.EXAMCD = EQ.EXAMCD ");
		strBuf.append("AND QS.SUBCD = EQ.SUBCD ");
		strBuf.append("AND QS.QUESTION_NO = EQ.QUESTION_NO ");
		strBuf.append("AND QS.BUNDLECD = ? ");//一括コード
		strBuf.append("LEFT JOIN QRECORD_A QA ");
		strBuf.append("ON QA.EXAMYEAR = EQ.EXAMYEAR ");
		strBuf.append("AND QA.EXAMCD = EQ.EXAMCD ");
		strBuf.append("AND QA.SUBCD = EQ.SUBCD ");
		strBuf.append("AND QA.QUESTION_NO = EQ.QUESTION_NO ");
		strBuf.append("ORDER BY ");
		//strBuf.append("TO_NUMBER(EQ.QUESTION_NO) ");//[3]

		/* 2016/02/29 QQ)Nishiyama エイリアス指定 UPD START */
		//strBuf.append("DISPLAY_NO, ");//[3]
		//strBuf.append("QUESTION_NO ");//[3]
		strBuf.append("EQ.DISPLAY_NO, ");//[3]
		strBuf.append("EQ.QUESTION_NO ");//[3]
		/* 2016/02/29 QQ)Nishiyama エイリアス指定 UPD END */

		return strBuf.toString();
	}

	/**
	 * 並べ替え条件設定
	 * 設問ＳＱＬの並べ替え条件を返します
	 * @return String
	 */
//	private String getDispOrderBy() {
//		String sqlOption = " ORDER BY to_number(EXAMQUESTION.DISPLAY_NO)";
//		return sqlOption;
//	}

	/**
	 * 科目一覧
	 * この年のこの模試の科目コードと名称をすべて取得
	 *
	 * @param String year	対象模試の年度
	 * @param String code	対象模試のコード
	 * @param String id	個人ＩＤ
	 * @return
	 */
	private Collection execSubNames(String year, String code, String id) throws Exception {
		//[2] 修正：設問を表示するViewに入っていない科目はタブに表示しない。
		String kmkGet =
			"SELECT " +
			" /*+ INDEX(QRECORD_I PK_QRECORD_I) */ "+//12/5 sql hint
			//" EXAMSUBJECT.SUBCD" +//[2] delete
			//",EXAMSUBJECT.SUBNAME " +//[2] delete
			" V_I_QEXAMSUBJECT.SUBCD" +//[2] add
			",V_I_QEXAMSUBJECT.SUBNAME " +//[2] add
			"FROM " +
			//"QRECORD_I INNER JOIN EXAMSUBJECT ON QRECORD_I.SUBCD = EXAMSUBJECT.SUBCD " +//[2] delete
			//"AND QRECORD_I.EXAMYEAR = EXAMSUBJECT.EXAMYEAR " +//[2] delete
			//"AND QRECORD_I.EXAMCD = EXAMSUBJECT.EXAMCD " +//[2] delete
			"QRECORD_I INNER JOIN V_I_QEXAMSUBJECT ON QRECORD_I.SUBCD = V_I_QEXAMSUBJECT.SUBCD " +//[2] add
			"AND QRECORD_I.EXAMYEAR = V_I_QEXAMSUBJECT.EXAMYEAR " +//[2] add
			"AND QRECORD_I.EXAMCD = V_I_QEXAMSUBJECT.EXAMCD " +//[2] add
			"WHERE QRECORD_I.EXAMYEAR = '"+ year +"' " +
			"AND QRECORD_I.EXAMCD = '"+ code + "' " +
			"AND QRECORD_I.INDIVIDUALID  = ? " +
			//"GROUP BY EXAMSUBJECT.SUBCD, EXAMSUBJECT.SUBNAME " +//[2] delete
			"GROUP BY V_I_QEXAMSUBJECT.SUBCD, V_I_QEXAMSUBJECT.SUBNAME " +//[2] add
			"ORDER BY " +
			//"EXAMSUBJECT.SUBCD ASC";//[2] delete
			"V_I_QEXAMSUBJECT.SUBCD ASC";//[2] add

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = conn.prepareStatement(kmkGet);
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			Collection subDatas = new ArrayList();
			int subCount = 0;
			while(rs.next()){
				SubjectData subData = new SubjectData();
				subData.setSubjectName(rs.getString("SUBNAME"));
				subData.setSubjectCd(rs.getString("SUBCD"));
				//初回検索時は、初期表示するためのサブコードを初期化
				if(isFirstAccess() && subCount == 0) {
					setTargetSubCode(subData.getSubjectCd());
					setTargetSubName(subData.getSubjectName());
					//現在表示している科目コードをBeanにセット
					setDispSubCode(subData.getSubjectCd());
				}
				if(Integer.parseInt(rs.getString("SUBCD").substring(0, 1)) > 6) {
					//総合だったら追加しない
				} else {
					//総合ではない
					subDatas.add(subData);
					subCount++;
				}
			}

			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();

			return subDatas;

		}catch(Exception e){
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
		}
	}

	/**
	 * 対象模試の、対象設問情報と、過去設問情報を
	 * すべて取得する。
	 * @return
	 */
	private List execQuestionDatas () throws Exception {

		PreparedStatement pstmt = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			pstmt = conn.prepareStatement(getQuestionSql());
			pstmt.setString(1, getTargetSubCode());
			pstmt.setString(2, getTargetExamYear());
			pstmt.setString(3, getTargetExamCode());
			pstmt.setString(4, getPersonId());

			// 画面表示用の学力レベルを取得
			String level = selectScholarLevel();

			// 画面表示用の学力レベルをセット
			setDispScholarLevel(level);
			pstmt.setString(5, level);
			pstmt.setString(6, getTargetBundleCd());

			rs = pstmt.executeQuery();
			List questionDatas = new ArrayList();
			QuestionData questionData = new QuestionData();

			// 全体成績データを設定する。
			{
				ps = conn.prepareStatement(QueryLoader.getInstance().load("i12_03_2").toString());
				ps.setString(1, getPersonId());
				ps.setString(2, level);
				ps.setString(3, getTargetBundleCd());
				ps.setString(4, getTargetExamYear());
				ps.setString(5, getTargetExamCode());
				ps.setString(6, getTargetExamYear());
				ps.setString(7, getTargetExamCode());
				ps.setString(8, getTargetSubCode());
			}

			setTotalSubjectData(questionDatas, ps);

			while(rs.next()){

				questionData = new QuestionData();
				questionData.setDisplay_no(nullToEmpty(rs.getString("DISPLAY_NO")));					//表示番号
				//questionData.setQuestionNo		(nullToEmpty(rs.getString("QUESTION_NO")));			//設問番号
				if(nullToEmpty(rs.getString("QSCORE")).equals("") && nullToEmpty(rs.getString("SCORERATE")).equals("")) {
					questionData.setQuestionName1	("");	//設問内容名
					questionData.setQallotpnt		("");	//配点
					questionData.setQscore			("");	//得点
					questionData.setScoreRate		("");	//得点率
					questionData.setAvgScoreRate_LEVEL("");	//レベル別得点率（全国）
					questionData.setAvgPnt_S		("");	//平均点（校内）
					questionData.setAvgScoreRate_S	("");	//平均得点率（校内）
					questionData.setAvgPnt_A		("");	//平均点（全国）
					questionData.setAvgScoreRate_A	("");	//平均得点率（全国）
				} else {
					questionData.setQuestionName1	(nullToEmpty(rs.getString("QUESTIONNAME1")));		//設問内容名
					questionData.setQallotpnt		(nullToEmpty(rs.getString("QALLOTPNT")));			//配点
					questionData.setQscore			(nullToEmpty(rs.getString("QSCORE")));				//得点
					questionData.setScoreRate		(nullToEmptyF(rs.getString("SCORERATE")));			//得点率
					questionData.setAvgScoreRate_LEVEL(nullToEmptyF(rs.getString("AVGSCORERATE_LEVEL")));	//学力レベル別得点率
					questionData.setAvgPnt_S		(nullToEmptyF(rs.getString("AVGPNT_S")));			//平均点（校内）
					questionData.setAvgScoreRate_S	(nullToEmptyF(rs.getString("AVGSCORERATE_S")));		//平均得点率（校内）
					questionData.setAvgPnt_A		(nullToEmptyF(rs.getString("AVGPNT_A")));			//平均点（全国）
					questionData.setAvgScoreRate_A	(nullToEmptyF(rs.getString("AVGSCORERATE_A")));		//平均得点率（全国）
				}
				questionDatas.add(questionData);
			}

			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();

			return questionDatas;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			DbUtils.closeQuietly(pstmt);
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * ICommonMap内の学力レベル選択状態、再表示（学力レベル変更or科目変更）を考慮し、
	 * 画面に表示する学力レベルを選択する。
	 * @param levelMap2
	 * @param levelkey
	 * @param dispScholarLevel2
	 * @param targetSubCode2
	 * @param dispSubCode2
	 * @return
	 */
	private String selectScholarLevel() {

		String lvl = new String();

		// 画面表示学力レベルを保持していない場合
		if ("".equals(getDispScholarLevel())
				|| getDispScholarLevel() == null) {
			lvl = getTargetScholarLevel();
		}

		// 画面表示学力レベルを保持している場合
		else {
			lvl = getDispScholarLevel();
		}

		return lvl;

	}

	/**
	 * 設問別成績の先頭に「全体成績」データをセットする
	 * @param questionDatas
	 * @param ps
	 * @throws SQLException
	 */
	private void setTotalSubjectData(List questionDatas,
			PreparedStatement ps) throws SQLException {

		ResultSet rs1 = null;
		QuestionData qData = new QuestionData();

		try {
			rs1 = ps.executeQuery();

			qData.setQuestionName1("全体成績");	//設問内容名

			if (rs1.next()) {
				qData.setDisplay_no("");
				qData.setQallotpnt(qqqToEmpty(rs1.getString("SUBALLOTPNT")));			//配点
				qData.setQscore(qqqToEmpty(rs1.getString("SSCORE")));					//得点
				qData.setScoreRate(qqqToEmpty(rs1.getString("SCORERATE")));			//得点率
				qData.setAvgScoreRate_LEVEL(qqqToEmpty(rs1.getString("AVGSCORERATE_LEVEL")));		//学力レベル別得点率
				qData.setAvgPnt_S(qqqToEmpty(rs1.getString("AVGPNT_S")));				//平均点（校内）
				qData.setAvgScoreRate_S(qqqToEmpty(rs1.getString("AVGSCORERATE_S")));	//平均得点率（校内）
				qData.setAvgPnt_A(qqqToEmpty(rs1.getString("AVGPNT_A")));				//平均点（全国）
				qData.setAvgScoreRate_A(qqqToEmpty(rs1.getString("AVGSCORERATE_A")));	//平均得点率（全国）
			}
			else {
				qData.setDisplay_no("");
				qData.setQallotpnt("");			//配点
				qData.setQscore("");			//得点
				qData.setScoreRate("");			//得点率
				qData.setAvgScoreRate_LEVEL("");//学力レベル別得点率
				qData.setAvgPnt_S("");			//平均点（校内）
				qData.setAvgScoreRate_S("");	//平均得点率（校内）
				qData.setAvgPnt_A("");			//平均点（全国）
				qData.setAvgScoreRate_A("");	//平均得点率（全国）

			}

			questionDatas.add(qData);
		}
		finally {
			DbUtils.closeQuietly(rs1);
		}

	}

	/**
	 * nullだったら""に変換
	 * @param map
	 */
	private String nullToEmpty(String str) {
		if(str == null) {
			return "";
		}
		return str;
	}

	/**
	 * StringをFloatのフォーマットで返します。
	 * @return
	 */
	public String nullToEmptyF(String str) {
		if(str == null) {
			return "";
		} else {
				return String.valueOf(new Float(str).floatValue());
		}
	}

	/**
	 * 「-999」を""に変換する。
	 */
	public String qqqToEmpty(String str) {

		if ("-999".equals(str) || "-999.0".equals(str)) {
			return "";
		}else {
			return str;
		}

	}

	/**
	 * @return
	 */
	public boolean isFirstAccess() {
		return isFirstAccess;
	}

	/**
	 * @return
	 */
	public String getPersonId() {
		return personId;
	}

	/**
	 * @return
	 */
	public String getQuestionSql() {
		return questionSql;
	}

	/**
	 * @return
	 */
	public Collection getSubDatas() {
		return subDatas;
	}

	/**
	 * @return
	 */
	public String getSubNumSql() {
		return subNumSql;
	}

	/**
	 * @return
	 */
	public String getTargetSubCode() {
		return targetSubCode;
	}

	/**
	 * @return
	 */
	public String getTargetSubName() {
		return targetSubName;
	}

	/**
	 * @return
	 */
	public String getTargetExamCode() {
		return targetExamCode;
	}

	/**
	 * @return
	 */
	public String getTargetExamYear() {
		return targetExamYear;
	}

	/**
	 * @param b
	 */
	public void setFirstAccess(boolean b) {
		isFirstAccess = b;
	}

	/**
	 * @param string
	 */
	public void setPersonId(String string) {
		personId = string;
	}

	/**
	 * @param string
	 */
	public void setQuestionSql(String string) {
		questionSql = string;
	}

	/**
	 * @param collection
	 */
	public void setSubDatas(Collection collection) {
		subDatas = collection;
	}

	/**
	 * @param string
	 */
	public void setSubNumSql(String string) {
		subNumSql = string;
	}

	/**
	 * @param string
	 */
	public void setTargetSubCode(String string) {
		targetSubCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetSubName(String string) {
		targetSubName = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamCode(String string) {
		targetExamCode = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExamYear(String string) {
		targetExamYear = string;
	}

	/**
	 * 一括コード（現在非使用／後日実装）
	 * @return
	 */
	public String getTargetBundleCd() {
		return targetBundleCd;
	}

	/**
	 * 一括コード（現在非使用／後日実装）
	 * @param string
	 */
	public void setTargetBundleCd(String string) {
		targetBundleCd = string;
	}




	/**
	 * @return
	 */
	public List getRecordSet() {
		return recordSet;
	}

	/**
	 * @param list
	 */
	public void setRecordSet(List list) {
		recordSet = list;
	}

	/**
	 * 学力レベルを取得する
	 * @return
	 */
	public Collection getLevelDatas() {
		return levelDatas;
	}

	/**
	 * 学力レベルをセットする
	 * @param levelDatas
	 */
	public void setLevelDatas(Collection collection) {
		this.levelDatas = collection;
	}

	public String getTargetScholarLevel() {
		return targetScholarLevel;
	}

	public void setTargetScholarLevel(String targetScholarLevel) {
		this.targetScholarLevel = targetScholarLevel;
	}

	public String getDispScholarLevel() {
		return dispScholarLevel;
	}

	public void setDispScholarLevel(String dispScholarLevel) {
		this.dispScholarLevel = dispScholarLevel;
	}

	public String getDispSubCode() {
		return dispSubCode;
	}

	public void setDispSubCode(String dispSubCode) {
		this.dispSubCode = dispSubCode;
	}

}
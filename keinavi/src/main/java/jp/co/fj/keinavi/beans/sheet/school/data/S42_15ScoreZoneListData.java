package jp.co.fj.keinavi.beans.sheet.school.data;

/**
 *
 * スコア帯リストデータ
 * 
 * 2006.08.31	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class S42_15ScoreZoneListData {

	// スコア帯コード
	private String devZoneCd;
	// 上限値
	private double highLimit;
	// 下限値
	private double lowLimit;
	// 人数
	private int numbers;
	// 親データクラス
	private S42_15BundleListData bundleListData;
	
	// コンストラクタ
	protected S42_15ScoreZoneListData() {
	}
	
	/**
	 * @return devZoneCd を戻します。
	 */
	public String getDevZoneCd() {
		return devZoneCd;
	}
	/**
	 * @param pDevZoneCd 設定する devZoneCd。
	 */
	public void setDevZoneCd(String pDevZoneCd) {
		devZoneCd = pDevZoneCd;
	}
	/**
	 * @return highLimit を戻します。
	 */
	public double getHighLimit() {
		return highLimit;
	}
	/**
	 * @param pHighLimit 設定する highLimit。
	 */
	public void setHighLimit(double pHighLimit) {
		highLimit = pHighLimit;
	}
	/**
	 * @return lowLimit を戻します。
	 */
	public double getLowLimit() {
		return lowLimit;
	}
	/**
	 * @param pLowLimit 設定する lowLimit。
	 */
	public void setLowLimit(double pLowLimit) {
		lowLimit = pLowLimit;
	}
	/**
	 * @return numbers を戻します。
	 */
	public int getNumbers() {
		return numbers;
	}
	/**
	 * @param pNumbers 設定する numbers。
	 */
	public void setNumbers(int pNumbers) {
		numbers = pNumbers;
	}

	/**
	 * @return bundleListData を戻します。
	 */
	public S42_15BundleListData getBundleListData() {
		return bundleListData;
	}

	/**
	 * @param pBundleListData 設定する bundleListData。
	 */
	public void setBundleListData(S42_15BundleListData pBundleListData) {
		bundleListData = pBundleListData;
	}
	
}

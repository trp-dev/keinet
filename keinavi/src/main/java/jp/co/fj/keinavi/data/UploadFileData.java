package jp.co.fj.keinavi.data;

import java.util.Date;

/**
 *
 * アップロードファイル管理Data
 * 
 * <2010年度マーク高２模試対応>
 * 2011.01.24	Tomohisa YAMADA
 * 				新規作成
 * 
 */
public class UploadFileData {
	
	//アップロードファイルID
	private String uploadFileID;
	
	//学校コード
	private String schoolCD;
	
	//学校名
	private String schoolName;
	
	//学校カナ名
	private String schoolNameKana;
	
	//ユーザID
	private String loginID;
	
	//アップロード日時
	private Date updateDate;
	
	//アップロードファイル名
	private String uploadFileName;
	
	/**
	 * サーバ側で命名規則に従い、命名されたファイル名
	 */
	private String serverFileName;
	
	//アップロードファイル種類ID
	private String fileTypeID;
	
	/**
	 * 現時点では、画面からは全角30文字(60byte)まで入力可能。
	 */
	private String uploadComment;
	
	/**
	 * 0: 準備中, 1: ダウンロード可能, 9: ダウンロード不可（ウィルス検知）
	 */
	private int status;
	
	//営業最終ダウンロード日時
	private Date lastDownloadDate;
	
	/**
	 * ファイル種類名
	 */
	private String fileTypeName;
	
	/**
	 * ファイル種類短縮名
	 */
	private String fileTypeAbbrname;
	
	/**
	 * コンストラクタ
	 * @param serverFileName
	 * @param schoolCD
	 */
	public UploadFileData(String serverFileName, String schoolCD) {
		this.serverFileName = serverFileName;
		this.schoolCD = schoolCD;
	}

	/**
	 * コンストラクタ
	 * @param uploadFileID
	 * @param schoolCD
	 * @param loginID
	 * @param updateDate
	 * @param uploadFileName
	 * @param serverFileName
	 * @param fileTypeID
	 * @param comments
	 * @param status
	 * @param lastDownloadDate
	 */
	public UploadFileData(String uploadFileID, String schoolCD,
			String loginID, Date updateDate, String uploadFileName,
			String serverFileName, String fileTypeID, String uploadComment,
			int status, Date lastDownloadDate) {
		this.uploadFileID = uploadFileID;
		this.schoolCD = schoolCD;
		this.loginID = loginID;
		this.updateDate = updateDate;
		this.uploadFileName = uploadFileName;
		this.serverFileName = serverFileName;
		this.fileTypeID = fileTypeID;
		this.uploadComment = uploadComment;
		this.status = status;
		this.lastDownloadDate = lastDownloadDate;
	}
	
	/**
	 * コンストラクタ
	 * @param uploadFileID
	 * @param schoolCD
	 * @param loginID
	 * @param updateDate
	 * @param uploadFileName
	 * @param serverFileName
	 * @param fileTypeID
	 * @param comments
	 * @param status
	 * @param lastDownloadDate
	 * @param fileTypeName
	 */
	public UploadFileData(String uploadFileID, String schoolCD,
			String loginID, Date updateDate, String uploadFileName,
			String serverFileName, String fileTypeID, String uploadComment,
			int status, Date lastDownloadDate, String fileTypeName) {
		this(uploadFileID, schoolCD, loginID, updateDate, uploadFileName, serverFileName, fileTypeID, uploadComment, status, lastDownloadDate);
		this.fileTypeName = fileTypeName;
	}
	
	/**
	 * コンストラクタ
	 * @param uploadFileID
	 * @param schoolCD
	 * @param loginID
	 * @param updateDate
	 * @param uploadFileName
	 * @param serverFileName
	 * @param fileTypeID
	 * @param comments
	 * @param status
	 * @param lastDownloadDate
	 * @param fileTypeAbbrname
	 * @param bundleName
	 */
	public UploadFileData(String uploadFileID, String schoolCD,
			String loginID, Date updateDate, String uploadFileName,
			String serverFileName, String fileTypeID, String uploadComment,
			int status, Date lastDownloadDate, String fileTypeAbbrname, String bundleName, String bundleNameKana) {
		this(uploadFileID, schoolCD, loginID, updateDate, uploadFileName, serverFileName, fileTypeID, uploadComment, status, lastDownloadDate);
		this.fileTypeAbbrname = fileTypeAbbrname;
		this.schoolName = bundleName;
		this.schoolNameKana = bundleNameKana;
	}

	public String getUploadFileID() {
		return uploadFileID;
	}

	public String getSchoolCD() {
		return schoolCD;
	}

	public String getLoginID() {
		return loginID;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public String getServerFileName() {
		return serverFileName;
	}

	public String getFileTypeID() {
		return fileTypeID;
	}

	public String getUploadComment() {
		return uploadComment;
	}

	public int getStatus() {
		return status;
	}

	public String getFileTypeName() {
		return fileTypeName;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public String getFileTypeAbbrname() {
		return fileTypeAbbrname;
	}

	public Date getLastDownloadDate() {
		return lastDownloadDate;
	}

	public String getSchoolNameKana() {
		return schoolNameKana;
	}
	
	
}

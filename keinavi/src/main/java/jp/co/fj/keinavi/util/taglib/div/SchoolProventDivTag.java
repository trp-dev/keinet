package jp.co.fj.keinavi.util.taglib.div;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.taglibs.standard.tag.el.core.ExpressionUtil;

import jp.co.fj.keinavi.data.individual.ScheduleDetailData;

/**
 * 入試本学地方区分を名称変換
 * 
 * <2010年度改修>
 * 2010.01.20	Tomohisa YAMADA - Totec
 * 				新規作成
 * 
 * 
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 */
public class SchoolProventDivTag extends TagSupport {

	/**
	 * 入試本学地方区分
	 */
	private String code;
	
	/**
	 * フォーマットスタイル
	 */
	private int style;
	
	/**
	 * 大学区分コード
	 */
	private String univDivCd;

	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {
		try {
			
            String _code = 
            	(String) ExpressionUtil.evalNotNull(
            			"schoolProventDiv", 
            			"code", 
            			code, 
            			String.class, 
            			this, pageContext);
            
            String _univDivCd =
            	(String) ExpressionUtil.evalNotNull(
            			"schoolProventDiv", 
            			"univDivCd", 
            			univDivCd, 
            			String.class, 
            			this, pageContext);
			
			String text = null;
			
			switch (style) {
				case 1 : text = ScheduleDetailData.provToShortName(_univDivCd, Integer.parseInt(_code));
					break;
				case 2 : text = ScheduleDetailData.provToName(_univDivCd, Integer.parseInt(_code));
					break;
				default : throw new JspException("styleの指定が不正です");
			}

			if (text != null) {
				pageContext.getOut().print(text);
			} 
			
		} catch (IOException e) {
			throw new JspException(e);
		}

		return EVAL_PAGE;
	}

	/**
	 * 区分を設定する
	 * @param string
	 */
	public void setCode(String string) {
		code = string;
	}
	
	/**
	 * フォーマットスタイルを設定する
	 * @param i
	 */
	public void setStyle(int i){
		style = i;
	}
	
	/**
	 * 大学区分コードを設定する
	 * @param string
	 */
	public void setUnivDivCd(String string) {
		univDivCd = string;
	}

}

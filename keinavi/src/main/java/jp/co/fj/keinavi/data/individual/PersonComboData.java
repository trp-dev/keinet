/*
 * 作成日: 2004/07/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class PersonComboData implements Serializable{
	
	private List personDatas;
	
	public String[] getPersonStrings(){
		if(getPersonDatas().size() < 1)
			return null;
		int count = 0;
		String[] personString = new String[getPersonDatas().size()];
		for (Iterator it=getPersonDatas().iterator(); it.hasNext();){
			StudentData personData = (StudentData)it.next();
			personString[count] = "personString["+count+"] = new Array(\""+personData.getStudentPersonId()+"\", \""+personData.getStudentGrade()+"\", \""+personData.getStudentClass()+"\", \""+personData.getStudentNameKana()+"\");";
			count ++;
		}
		return personString;
	}
	public void setPersonDatas(Collection collection){
		personDatas = (List)collection;
	}
	public Collection getPersonDatas(){
		return personDatas;
	}
	public int getPersonSize(){
		int size = 0;
		if(getPersonStrings() != null)
			size = getPersonStrings().length;
		return size;
	}
}

package jp.co.fj.keinavi.forms.maintenance;

import jp.co.fj.keinavi.forms.BaseForm;

/**
 *
 * 受験届修正画面アクションフォーム
 * 
 * 2006.10.06	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class M201Form extends BaseForm {

	// 頭文字変換テーブル
	private static final String[] KANA_TABLE = new String[] {
		"",
		"ｱ", "ｲ", "ｳ", "ｴ", "ｵ", 
		"ｶ", "ｷ", "ｸ", "ｹ", "ｺ", 
		"ｻ", "ｼ", "ｽ", "ｾ", "ｿ", 
		"ﾀ", "ﾁ", "ﾂ", "ﾃ", "ﾄ", 
		"ﾅ", "ﾆ", "ﾇ", "ﾈ", "ﾉ", 
		"ﾊ", "ﾋ", "ﾌ", "ﾍ", "ﾎ", 
		"ﾏ", "ﾐ", "ﾑ", "ﾒ", "ﾓ", 
		"ﾔ", "ﾕ", "ﾖ", "", "", 
		"ﾗ", "ﾘ", "ﾙ", "ﾚ", "ﾛ", 
		"ﾜ", "ｦ", "ﾝ", null, "",
	};
	
	// アクションモード
	private String actionMode;

	// 編集対象個人ID
	private String editIndividualId;
	// 初期表示個人ID
	private String initIndividualId;
	// 初期表示模試年度
	private String initExamYear;
	// 初期表示模試コード
	private String initExamCd;
	
	// 生徒情報
	// 対象年度
	private String targetYear1;
	// 対象学年
	private String targetGrade1;
	// 対象クラス
	private String targetClass1;
	// 頭文字
	private String initialKana1;
	// 対象生徒
	private String targetStudentId;
	
	// 模試情報
	// 対象年度
	private String targetYear2;
	// 対象学年
	private String targetGrade2;
	// 対象クラス
	private String targetClass2;
	// 頭文字
	private String initialKana2;
	// 対象模試
	private String targetExam;
	// 対象受験届
	private String targetSheetId;
	
	/**
	 * @see com.fjh.forms.ActionForm#validate()
	 */
	public void validate() {
	}

	// 半角カタカナへの変換処理
	private String toKatakana(final String value) {
		if (value.length() > 0) {
			return KANA_TABLE[Integer.parseInt(value)];
		} else {
			return "";
		}
	}

	/**
	 * @return targetClass1 を戻します。
	 */
	public String getTargetClass1() {
		return targetClass1;
	}

	/**
	 * @param pTargetClass1 設定する targetClass1。
	 */
	public void setTargetClass1(String pTargetClass1) {
		targetClass1 = pTargetClass1;
	}

	/**
	 * @return targetClass2 を戻します。
	 */
	public String getTargetClass2() {
		return targetClass2;
	}

	/**
	 * @param pTargetClass2 設定する targetClass2。
	 */
	public void setTargetClass2(String pTargetClass2) {
		targetClass2 = pTargetClass2;
	}

	/**
	 * @return targetGrade1 を戻します。
	 */
	public String getTargetGrade1() {
		return targetGrade1;
	}

	/**
	 * @param pTargetGrade1 設定する targetGrade1。
	 */
	public void setTargetGrade1(String pTargetGrade1) {
		targetGrade1 = pTargetGrade1;
	}

	/**
	 * @return targetGrade2 を戻します。
	 */
	public String getTargetGrade2() {
		return targetGrade2;
	}

	/**
	 * @param pTargetGrade2 設定する targetGrade2。
	 */
	public void setTargetGrade2(String pTargetGrade2) {
		targetGrade2 = pTargetGrade2;
	}

	/**
	 * @return initialKana1 を戻します。
	 */
	public String getInitialKana1() {
		return initialKana1;
	}

	/**
	 * @param pInitialKana1 設定する initialKana1。
	 */
	public void setInitialKana1(String pInitialKana1) {
		initialKana1 = toKatakana(pInitialKana1);
	}

	/**
	 * @return initialKana2 を戻します。
	 */
	public String getInitialKana2() {
		return initialKana2;
	}

	/**
	 * @param pInitialKana2 設定する initialKana2。
	 */
	public void setInitialKana2(String pInitialKana2) {
		initialKana2 = toKatakana(pInitialKana2);
	}

	/**
	 * @return targetYear1 を戻します。
	 */
	public String getTargetYear1() {
		return targetYear1;
	}

	/**
	 * @param pTargetYear1 設定する targetYear1。
	 */
	public void setTargetYear1(String pTargetYear1) {
		targetYear1 = pTargetYear1;
	}

	/**
	 * @return targetYear2 を戻します。
	 */
	public String getTargetYear2() {
		return targetYear2;
	}

	/**
	 * @param pTargetYear2 設定する targetYear2。
	 */
	public void setTargetYear2(String pTargetYear2) {
		targetYear2 = pTargetYear2;
	}

	/**
	 * @return actionMode を戻します。
	 */
	public String getActionMode() {
		return actionMode;
	}

	/**
	 * @param pActionMode 設定する actionMode。
	 */
	public void setActionMode(String pActionMode) {
		actionMode = pActionMode;
	}

	/**
	 * @return kANA_TABLE を戻します。
	 */
	public static String[] getKANA_TABLE() {
		return KANA_TABLE;
	}

	/**
	 * @return targetSheetId を戻します。
	 */
	public String getTargetSheetId() {
		return targetSheetId;
	}

	/**
	 * @param pTargetSheetId 設定する targetSheetId。
	 */
	public void setTargetSheetId(String pTargetSheetId) {
		targetSheetId = pTargetSheetId;
	}

	/**
	 * @return initExamCd を戻します。
	 */
	public String getInitExamCd() {
		return initExamCd;
	}

	/**
	 * @param pInitExamCd 設定する initExamCd。
	 */
	public void setInitExamCd(String pInitExamCd) {
		initExamCd = pInitExamCd;
	}

	/**
	 * @return initExamYear を戻します。
	 */
	public String getInitExamYear() {
		return initExamYear;
	}

	/**
	 * @param pInitExamYear 設定する initExamYear。
	 */
	public void setInitExamYear(String pInitExamYear) {
		initExamYear = pInitExamYear;
	}

	/**
	 * @return initIndividualId を戻します。
	 */
	public String getInitIndividualId() {
		return initIndividualId;
	}

	/**
	 * @param pInitIndividualId 設定する initIndividualId。
	 */
	public void setInitIndividualId(String pInitIndividualId) {
		initIndividualId = pInitIndividualId;
	}

	/**
	 * @return editIndividualId を戻します。
	 */
	public String getEditIndividualId() {
		return editIndividualId;
	}

	/**
	 * @param pEditIndividualId 設定する editIndividualId。
	 */
	public void setEditIndividualId(String pEditIndividualId) {
		editIndividualId = pEditIndividualId;
	}

	/**
	 * @return targetExam を戻します。
	 */
	public String getTargetExam() {
		return targetExam;
	}

	/**
	 * @param pTargetExam 設定する targetExam。
	 */
	public void setTargetExam(String pTargetExam) {
		targetExam = pTargetExam;
	}

	/**
	 * @return targetStudentId を戻します。
	 */
	public String getTargetStudentId() {
		return targetStudentId;
	}

	/**
	 * @param pTargetStudentId 設定する targetStudentId。
	 */
	public void setTargetStudentId(String pTargetStudentId) {
		targetStudentId = pTargetStudentId;
	}

}

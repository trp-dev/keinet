/**
 * 個人成績分析−連続個人成績表
 * @author Ito.Y
 */
 
package jp.co.fj.keinavi.excel.personal;

import java.util.ArrayList;

import jp.co.fj.keinavi.excel.data.personal.I11Item;
import jp.co.fj.keinavi.util.log.*;

public class I13 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */
	public boolean i13( I11Item i11Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog) {
		
		KNLog log = KNLog.getInstance(null,null,null);

		try{
			
			int ret = 0;
			if ( (i11Item.getIntSeisekiFormatFlg()!=1 && i11Item.getIntSeisekiFormatFlg()!=2) ){
				throw new Exception("I13 ERROR : フラグ異常");
			}
			//I13Itemから各帳票を出力
			if( i11Item.getIntSeisekiFormatFlg()==2 ){
				I13_01 exceledit1 = new I13_01();
				ret = exceledit1.i13_01EditExcel( i11Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"I13_01","帳票作成エラー","");
					return false;
				}
				log.Ep("I13_01","I13_01帳票作成終了","");
				sheetLog.add("I13_01");
			}
			if( i11Item.getIntSeisekiFormatFlg()==1 ){
				I13_02 exceledit2 = new I13_02();
				ret = exceledit2.i13_02EditExcel( i11Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"I13_02","帳票作成エラー","");
					return false;
				}
				log.Ep("I13_02","I13_01帳票作成終了","");
				sheetLog.add("I13_02");
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			log.Err("99I13","帳票作成エラー",e.toString());
			return false;
		}
		return true;
	}

}
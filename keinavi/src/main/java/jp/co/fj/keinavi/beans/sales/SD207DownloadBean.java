package jp.co.fj.keinavi.beans.sales;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.data.sales.SD207Data;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.QueryLoader;
import jp.co.totec.io.IOUtil;
import jp.hishidama.zip.ZipEntry;
import jp.hishidama.zip.ZipOutputStream;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * 特例成績データダウンロード画面のダウンロードBeanです。
 * 
 * 
 * @author TOTEC)KAWAI.Yoshimoto
 * 
 */
public class SD207DownloadBean extends DefaultBean {

	/** serialVersionUID */
	private static final long serialVersionUID = -8953085735585679709L;

	/** ダウンロードするデータ */
	private final SD207Data data;

	/** {@link HttpServletResponse} */
	private HttpServletResponse response;

	/**
	 * コンストラクタです。
	 */
	public SD207DownloadBean(SD207Data data, HttpServletResponse response) {
		this.data = data;
		this.response = response;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		File file = getFile();

		if (!file.exists()) {
			setError("申請されたデータは存在しません。");
		} else {
			download(file);
			updateTimestamp();
		}
	}

	/**
	 * ダウンロードするファイルオブジェクトを返します。
	 */
	private File getFile() {

		String path;
		if (KNUtil.CD_CENTER.equals(data.getExamCd())) {
			/* センターリサーチの場合 */
			path = KNCommonProperty.getCRExamScoreDLPath();
		} else {
			/* センターリサーチ以外の場合 */
			path = KNCommonProperty.getExamScoreDLPath();
		}

		return new File(path, data.getExamYear() + File.separator
				+ data.getExamCd() + File.separator + data.getSchoolCd()
				+ ".csv");
	}

	/**
	 * 指定したファイルのダウンロード処理を行います。
	 */
	private void download(File file) throws IOException {

		response.setContentType("application/octet-stream-dummy");
		response.setHeader("Content-Disposition", "inline; filename=\""
		+ URLEncoder.encode(createFileName(),"UTF-8") + "\"");

		ZipOutputStream out = null;
		InputStream in = null;
		try {
			in = new BufferedInputStream(new FileInputStream(file));

			out = new ZipOutputStream(new BufferedOutputStream(
					response.getOutputStream()));
			out.setPassword(data.getPassword().getBytes());

			ZipEntry ze = new ZipEntry(createCsvFileName());
			out.putNextEntry(ze);

			int length = -1;
			byte[] b = new byte[1024 * 16];
			while ((length = in.read(b)) > -1) {
				out.write(b, 0, length);
			}

		} finally {
			IOUtil.closeQuietly(in);
			IOUtil.closeQuietly(out);
		}
	}

	/**
	 * CSVファイル名を生成します。
	 */
	private String createCsvFileName() {

		StringBuffer sb = new StringBuffer(32);
		if (data.getExamCd().endsWith("_1")) {
			sb.append("SG1_");
		} else if (data.getExamCd().endsWith("_2")) {
			sb.append("SG2_");
		} else {
			sb.append("hs");
		}

		return sb.append(data.getSchoolCd()).append(data.getTargetGrade())
				.append(data.getExamCd().substring(0, 2)).append(".csv")
				.toString();
	}

	/**
	 * ダウンロードファイル名を生成します。
	 */
	private String createFileName() {
		return "kj" + data.getExamYear() + data.getExamCd() + "_"
				+ data.getSchoolCd() + "_" + data.getSchoolName() + ".zip";
	}

	/**
	 * 最終ダウンロード日時を更新します。
	 */
	private void updateTimestamp() throws SQLException {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance()
					.load("sd207_download").toString());
			ps.setString(1, data.getAppId());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

}

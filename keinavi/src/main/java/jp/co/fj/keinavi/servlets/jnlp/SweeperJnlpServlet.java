package jp.co.fj.keinavi.servlets.jnlp;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * 一時ファイル削除ツールのJNLPサーブレット
 * 
 * 2008.02.08	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class SweeperJnlpServlet extends DefaultJnlpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.jnlp.DefaultJnlpServlet#createParameter(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	protected String createParameter(final HttpServletRequest request) {
		
		// 全てのパラメータをそのまま引き継ぐ
		return getAllParameter(request);
	}
	
	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#isLogin(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	protected boolean isLogin(HttpServletRequest pRequest) {
		// ログインの必要なし
		return true;
	}
	
}

/*
 * 作成日: 2004/09/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.helpdesk;

import java.sql.SQLException;

import com.fjh.beans.DefaultBean;
import jp.co.fj.keinavi.forms.helpdesk.HD102Form;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class FileNameBean extends DefaultBean {

	HD102Form 	hd102Form = new HD102Form();	// HD102Form 情報

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		// TODO 自動生成されたメソッド・スタブ

		

	}

	// HD102Form 情報
	/**
	 * @param HD102Form
	 */
	public void setHD102Form(HD102Form form) {
		hd102Form = form;
	}
	
	/**
	 * @return HD102Form
	 */
	public HD102Form getHD102Form() {
		return hd102Form;
	}

}

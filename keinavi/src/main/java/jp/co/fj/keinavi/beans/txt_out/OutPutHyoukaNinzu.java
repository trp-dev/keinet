package jp.co.fj.keinavi.beans.txt_out;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import jp.co.fj.keinavi.beans.security.IMenuDefine;
import jp.co.fj.keinavi.beans.security.MenuSecurityBean;
import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 志望大学評価別人数
 *
 * 2004.10.18	[新規作成]
 *
 * <2010年度改修>
 * 2009.12.14	Tomohisa YAMADA - Totec
 * 				センター・リサーチ５段階評価対応
 *
 * 2010.01.19	Fujito URAKAWA - Totec
 * 				大学コード10／5桁化対応
 *
 *
 * @author 二宮淳行 - TOTEC
 * @version 1.0
 *
 */
public class OutPutHyoukaNinzu extends AbstractSheetBean{

	// 他校コード
	private String OtherSchools = null;
	// 他校比較
	private int schoolDefFlg = 0;
	// クラス比較
	private int classDefFlg = 0;

	// センターリサーチかどうか
	private boolean isCenterResearch;

	public void execute() throws Exception{

		final OutPutParamSet set = new OutPutParamSet(
				login, exam, profile, "t005", sessionKey);
		List header = set.getHeader();
		File outPutFile =set.getOutPutFile();

		final MenuSecurityBean menuSecurity = getMenuSecurityBean();
		schoolDefFlg = menuSecurity.isValid(IMenuDefine.MENU_TEXT_OTHER) ? set.getSchoolDefFlg() : 0;
		classDefFlg = menuSecurity.isValid(IMenuDefine.MENU_TEXT_CLASS) ? set.getClassDefFlg() : 0;

		OtherSchools = set.getOtherSchools();

		isCenterResearch = KNUtil.isCenterResearch(exam);
		sheetLog.add(outPutFile.getName());

		DirectOutputTextBean bean = DirectOutputTextBean.getInstance(set.getOutType());
		try {
			bean.setOutFile(outPutFile);
			bean.setHeadTextList(header);
			bean.setOutTarget(set.getTarget());
			bean.init();
		    createFile(bean);
		} finally {
		    bean.close();
		}

		outfileList.add(outPutFile.getPath());
	}

	// 志望大学評価別人数を取得
	private void createFile(final DirectOutputTextBean bean) throws SQLException {

		PreparedStatement ps = null;
		try {
		    // 全国
			ps = conn.prepareStatement(getQuery("t04_1").toString());
		    ps.setString(1, exam.getExamYear());
		    ps.setString(2, exam.getExamCD());
		    ps.setString(3, profile.getBundleCD());
		    createRecord(ps, bean);

		    // 県
			ps = conn.prepareStatement(getQuery("t04_2").toString());
		    ps.setString(1, login.getPrefCD());
		    ps.setString(2, exam.getExamYear());
		    ps.setString(3, exam.getExamCD());
		    ps.setString(4, profile.getBundleCD());
		    createRecord(ps, bean);

		    // 自校
			ps = conn.prepareStatement(getQuery("t04_3").toString());
		    ps.setString(1, exam.getExamYear());
		    ps.setString(2, exam.getExamCD());
		    ps.setString(3, profile.getBundleCD());
		    createRecord(ps, bean);

		    // クラス
		    if (classDefFlg == 1) {
				ps = conn.prepareStatement(getQuery("t04_4").toString());
			    ps.setString(1, exam.getExamYear());
			    ps.setString(2, exam.getExamCD());
			    ps.setString(3, profile.getBundleCD());
			    createRecord(ps, bean);
		    }

		    // 他校比較
		    if (schoolDefFlg == 1) {
		        Query query = getQuery("t04_5");
		        query.replaceAll("#", OtherSchools);
				ps = conn.prepareStatement(query.toString());
			    ps.setString(1, exam.getExamYear());
			    ps.setString(2, exam.getExamCD());
			    ps.setString(3, profile.getBundleCD());
			    createRecord(ps, bean);
		    }
		} catch (IOException e) {
		    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
		    //throw new SQLException(e.getMessage());
		    throw new SQLException(e);
		    // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
       } finally {
		    DbUtils.closeQuietly(ps);
       }
	}

	// クエリを取得する
	private Query getQuery(final String key) throws SQLException {

	    final Query query = QueryLoader.getInstance().load(key);

	    // 書き換え
	    rewriteUnivQuery(query);

		//System.out.println("ORDER BY句");
		//System.out.println(query.toString());

		return query;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return true;
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.T_COUNTING_UNIV;
	}

	// ステートメントから結果を取得してレコードを作る
	private void createRecord(final PreparedStatement ps,
			final DirectOutputTextBean bean) throws SQLException, IOException {

		ResultSet rs = null;
		try {
		    ps.setFetchSize(1000);
		    rs = ps.executeQuery();
		    while (rs.next()) {
		    	// ２番目（模試年度）は別処理
		    	// センターリサーチなら+1
		    	if (isCenterResearch) {
					bean.add(String.valueOf(rs.getInt(2) + 1));
		    	// それ以外
		    	} else {
					bean.add(rs.getString(2));
		    	}

				// ３番目（模試コード）〜２９番目（評価別人数E）までの２７項目を格納
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD START
//		    	//2019/08/08 QQ)nagai 共通テスト対応 UPD START
//				//for (int i = 2; i<=28; i++) {
//		    	//if ( i <= 21 ) {
//		    	for (int i = 2; i<=33; i++) {
//					if ( i <= 21 ) {
//				//2019/08/08 QQ)nagai 共通テスト対応 UPD END
				for (int i = 2; i<=28; i++) {
					if ( i <= 21 ) {
// 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD END
						bean.add(rs.getString(i+1));
					} else {
						bean.add(String.valueOf(rs.getLong(i+1)));
					}
				}
				bean.createLine();
		    }
       } finally {
		    DbUtils.closeQuietly(null, ps, rs);
       }
    }

}
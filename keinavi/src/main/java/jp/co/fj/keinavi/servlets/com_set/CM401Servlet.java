package jp.co.fj.keinavi.servlets.com_set;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.PrefBean;
import jp.co.fj.keinavi.beans.UnivDivBean;
import jp.co.fj.keinavi.beans.com_set.UnivBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.com_set.CM001Form;
import jp.co.fj.keinavi.forms.com_set.CM401Form;

/**
 *
 * 共通項目設定 - 志望大学
 * 
 * 2004.07.27	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class CM401Servlet extends AbstractCMServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.com_set.AbstractCMServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException, IOException {
		
		// 共通処理
		super.execute(request, response);

		// プロファイル
		final Profile profile = getProfile(request);
		// ログイン情報
		final LoginSession login = getLoginSession(request);
		// 模試セッション
		final ExamSession examSession = getExamSession(request);
		// 共通アクションフォーム - session scope
		final CM001Form scform = getCommonForm(request);
		// 個別アクションフォーム - session scope
		final CM401Form siform = (CM401Form) scform.getActionForm("CM401Form");

		// JSP
		if ("cm401".equals(getForward(request))) {
			// 対象模試データ
			final ExamData exam = examSession.getExamData(
				scform.getTargetYear(), scform.getTargetExam());
			// DBコネクション
			Connection con = null;
			try {
				con = getConnectionPool(request);

				// 都道府県Beanをセット
				request.setAttribute("PrefBean", PrefBean.getInstance(con));

				// 大学区分Bean
				UnivDivBean udb = new UnivDivBean();
				udb.setConnection(null, con);
				udb.execute();
				request.setAttribute("UnivDivBean", udb);

				// 大学Bean（全選択）※営業は不要
				if (!login.isSales()) {
					UnivBean ub1 = new UnivBean();
					ub1.setConnection(null, con);
					ub1.setExam(exam);
					ub1.setBundleCD(getBundleCd(login, profile, scform.getTargetExam()));
					ub1.execute();
					request.setAttribute("UnivBean1", ub1);
				}
				
				// 大学Bean（個別選択）
				if (siform.getUniv() == null) {
					request.setAttribute("UnivBean2", new UnivBean());
				} else {
					UnivBean ub2 = new UnivBean();
					ub2.setConnection(null, con);
					ub2.setUnivCodeArray(siform.getUniv());
					ub2.setExam(exam);
					ub2.setBundleCD(getBundleCd(login, profile, scform.getTargetExam()));
					ub2.execute();
					request.setAttribute("UnivBean2", ub2);
				}
				
			} catch (Exception e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}
			
			forward(request, response, JSP_CM401);
			
		// 不明なら転送
		} else {
			// アクションフォームを取得
			final CM001Form rcform = (CM001Form) getActionForm(request,
				"jp.co.fj.keinavi.forms.com_set.CM001Form");;
			final CM401Form riform = (CM401Form) getActionForm(request,
				"jp.co.fj.keinavi.forms.com_set.CM401Form");

			// フォームの値をセッションに保持する
			siform.setSelection(riform.getSelection());

			// NULLは入れないで空リストを入れる
			if (riform.getUniv() == null) {
				siform.setUniv(new String[0]);
			} else {
				siform.setUniv(riform.getUniv());
			}

			// 変更フラグ
			scform.setChanged(rcform.getChanged());
			
			forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

package jp.co.fj.keinavi.beans.recount.data;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.util.KNUtil;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 *
 * 再集計タスク
 * 
 * ※タスクの単位は（模試年度・模試コード・一括コード）
 * 
 * 2007.08.07	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class RecountTask {

	// 対象模試
	private final ExamData exam;
	
	// 一括コード
	private final String bundleCd;
		
	/**
	 * コンストラクタ
	 * 
	 * @param rs 結果セット
	 */
	public RecountTask(final ResultSet rs) throws SQLException {
		exam = new ExamData();
		exam.setExamYear(rs.getString(1));
		exam.setExamCD(rs.getString(2));
		exam.setExamTypeCD(rs.getString(3));
		exam.setExamDiv(rs.getString(4));
		exam.setMasterDiv(rs.getString(5));
		bundleCd = rs.getString(6);
	}
	
	/**
	 * @return 再集計対象が新テストであるか
	 */
	public boolean isNewExam() {
		return KNUtil.isNewExam(exam);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return new ToStringBuilder(this)
				.append("ExamYear", getExamYear())
				.append("ExamCd", getExamCd())
				.append("BundleCd", getBundleCd())
				.toString();
	}

	/**
	 * @return bundleCd
	 */
	public String getBundleCd() {
		return bundleCd;
	}

	/**
	 * @return 模試コード
	 */
	public String getExamCd() {
		return exam.getExamCD();
	}

	/**
	 * @return 模試年度
	 */
	public String getExamYear() {
		return exam.getExamYear();
	}
	
	/**
	 * @return 模試区分
	 */
	public String getExamDiv() {
		return exam.getExamDiv();
	}
	
	/**
	 * @return 高1・2大学マスタ利用模試かどうか
	 */
	public boolean isUniv12Exam() {
		return KNUtil.isUniv12Exam(exam);
	}
	
}

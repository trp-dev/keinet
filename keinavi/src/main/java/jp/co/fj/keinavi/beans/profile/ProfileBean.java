package jp.co.fj.keinavi.beans.profile;

import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import jp.co.fj.keinavi.beans.profile.corrector.Corrector2;
import jp.co.fj.keinavi.beans.profile.corrector.Corrector3;
import jp.co.fj.keinavi.beans.profile.corrector.Corrector4;
import jp.co.fj.keinavi.beans.profile.corrector.Corrector5;
import jp.co.fj.keinavi.beans.profile.corrector.Corrector6;
import jp.co.fj.keinavi.beans.profile.corrector.Corrector7;
import jp.co.fj.keinavi.beans.profile.corrector.Corrector8;
import jp.co.fj.keinavi.beans.profile.corrector.ICorrector;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.com_set.ChargeClassData;
import jp.co.fj.keinavi.data.profile.DefaultProfile;
import jp.co.fj.keinavi.data.profile.DefaultSalesProfile;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.date.DateUtil;
import jp.co.fj.keinavi.util.date.ShortDateUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

import oracle.sql.BLOB;

/**
 *
 * プロファイルオブジェクトの作成・復元を行うBean
 *
 *
 * 2005.02.09	Yoshimoto KAWAI - Totec
 * 				バージョンを導入
 *
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 * 2009.10.20 Fujito URAKAWA - Totec
 *            新校内資料「科目・設問別正答率（レベル別）」対応
 *
 * <河合塾2010年度マーク高２模試対応>
 * 2011.05.20   Tomohisa YAMADA - Totec
 *              倫政対応・マーク高２対応有
 *
 * <新過程対応>
 * 2013.04.23   Yuuichirou Morita - TOTEC
 *
 * @author kawai
 *
 */
public class ProfileBean extends DefaultBean {

    // バージョンを校正するモジュールリスト
    private static final List corrector = new LinkedList();

    static {
        corrector.add(new Corrector2());
        corrector.add(new Corrector3());
        corrector.add(new Corrector4());
        corrector.add(new Corrector5());
        corrector.add(new Corrector6());
        corrector.add(new Corrector7());
        corrector.add(new Corrector8());
    }

    private Profile profile = new Profile(); // プロファイル

    private LoginSession loginSession; // ログイン情報
    private String profileID; // プロファイルID
    private String bundleCD; // 一括コード
    private boolean isRegardable; // 担当クラスの設定を再評価すべきかどうか

    /* (非 Javadoc)
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws SQLException, Exception {
        // プロファイルIDの指定がなければ初期化して渡す
        if (profileID == null) {

            Date now = new Date();
            profile.setCreateDate(now);
            profile.setUpdateDate(now);
            profile.setProfileName("新規プロファイル");
            profile.setUserName(loginSession.getAccountName());

            // 高校
            if (loginSession.isSchool()) {
                profile.setUserID(loginSession.getAccount());
                profile.setBundleCD(bundleCD);
                profile.setCategoryMap(new DefaultProfile().getCategoryMap());

            // 営業または校舎
            } else if (loginSession.isBusiness()) {
                profile.setSectorSortingCD(loginSession.getSectorSortingCD());
                profile.setCategoryMap(new DefaultSalesProfile().getCategoryMap());

            // 営業 → 高校
            } else if (loginSession.isDeputy()) {
                profile.setBundleCD(bundleCD);
                profile.setSectorSortingCD(loginSession.getSectorSortingCD());
                profile.setCategoryMap(new DefaultProfile().getCategoryMap());
            }

            // 現在のバージョン
            short currentVersion = 0;
            for (Iterator ite = corrector.iterator(); ite.hasNext();) {
                ICorrector c = (ICorrector) ite.next();
                if (currentVersion < c.getVersion()) {
                    currentVersion = c.getVersion();
                }
            }
            profile.setVersion(currentVersion);

        // DBから取得する
        } else {
            // 日付用のクラス
            DateUtil date = new ShortDateUtil();

            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                Query query = QueryLoader.getInstance().load("w01");

                // 高校
                if (loginSession.isSchool()) {
                    query.append("AND p.bundlecd = ? AND p.sectorsortingcd IS NULL");

                    ps = conn.prepareStatement(query.toString());
                    ps.setString(3, bundleCD); // 一括コード

                // 営業部または校舎
                } else if(loginSession.isBusiness()) {
                    query.append("AND p.sectorsortingcd = ? AND p.userid IS NULL ");
                    query.append("AND p.bundlecd IS NULL");

                    ps = conn.prepareStatement(query.toString());
                    ps.setString(3, loginSession.getSectorSortingCD()); // 部門分類コード

                // 営業部 → 高校
                } else if(loginSession.isDeputy()) {
                    query.append("AND (");
                    query.append("(p.bundlecd = ? AND p.sectorsortingcd = ?) ");
                    query.append("OR (p.bundlecd = ? AND p.sectorsortingcd IS NULL)");
                    query.append(")");

                    ps = conn.prepareStatement(query.toString());
                    ps.setString(3, bundleCD); // 一括コード
                    ps.setString(4, loginSession.getSectorSortingCD()); // 部門分類コード
                    ps.setString(5, bundleCD); // 一括コード
                }

                ps.setString(1, loginSession.getUserID()); // 学校コード
                ps.setString(2, profileID); // プロファイルID

                rs = ps.executeQuery();
                if (rs.next()) {
                    String profileName = rs.getString(2) == null ? null : rs.getString(2).replace("マーク", "共通テスト");
                    String comment = rs.getString(5) == null ? null : rs.getString(5).replace("マーク", "共通テスト");
                    profile.setProfileID(rs.getString(1));
                    profile.setProfileName(profileName);
                    profile.setFolderID(rs.getString(3));
                    profile.setFolderName(rs.getString(4));
                    profile.setComment(comment);
                    profile.setCreateDate(date.string2Date(rs.getString(6)));
                    profile.setUpdateDate(date.string2Date(rs.getString(7)));
                    profile.setOverwriteMode(rs.getInt(8));
                    profile.setUserID(rs.getString(9));
                    profile.setBundleCD(rs.getString(10));
                    profile.setBundleName(rs.getString(11));
                    profile.setSectorSortingCD(rs.getString(12));
                    profile.setUserName(rs.getString(13));

                    // カテゴリマップ
                    BLOB blob = (BLOB) rs.getBlob(14);
                    ObjectInputStream ois = new ObjectInputStream(blob.getBinaryStream());
                    profile.setCategoryMap((Map)ois.readObject());
                } else {
                    throw new SQLException("プロファイルの取得に失敗しました。");
                }

            } finally {
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(ps);
            }

            // バージョンを校正する
            Iterator ite = corrector.iterator();
            while (ite.hasNext()) {
                ICorrector c = (ICorrector) ite.next();

                if (profile.getVersion() < c.getVersion()) {
                    c.execute(profile, loginSession);
                    profile.setVersion(c.getVersion());
                }
            }

            // 担当クラスを再評価する
            if (isRegardable) {
                checkChargeClassSetting(profile, loginSession);
            }
        }
    }

    /**
     * 担当クラスの設定値を評価する
     *
     * @param profile
     * @param login
     */
    private void checkChargeClassSetting(final Profile profile, final LoginSession login) throws SQLException {

        // 利用者IDがなければ評価の必要なし
        if (login.getAccount() == null) {
            return;
        }

        // 共通項目設定値
        final Map cm = profile.getItemMap(IProfileCategory.CM);

        // 担当クラスの設定値
        final List charge = (List) cm.get(IProfileItem.CLASS);

        // プロファイル作成者と利用者が異なるなら設定クリア
        if (!login.getAccount().equals(profile.getUserID())) {
            charge.clear();
            return;
        }

        // 利用者の担当クラス設定がない＝無制限ならここまで
        if (!hasUserChargeClass(conn, login)) {
            return;
        }

        // 担当クラスの年度
        final String year = ProfileUtil.getChargeClassYear(profile);

        // 担当クラス設定を評価して再設定する
        cm.put(IProfileItem.CLASS, correctChargeClassSetting(conn, year, charge, login));
    }

    /**
     * 利用者の担当クラス設定があるかどうか
     *
     * @param login
     * @return
     * @throws SQLException
     */
    private boolean hasUserChargeClass(final Connection con, final LoginSession login) throws SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(QueryLoader.getInstance().load("w08").toString());
            ps.setString(1, login.getUserID());
            ps.setString(2, login.getAccount());
            rs = ps.executeQuery();
            return rs.next();

        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    /**
     * 共通項目の担当クラス設定から利用者の担当クラスに存在しないクラスを除く
     *
     * @param login
     * @return
     * @throws SQLException
     */
    private List correctChargeClassSetting(final Connection con, final String year, final List charge, final LoginSession login) throws SQLException {

        // 新しい入れ物
        List container = new ArrayList();

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(QueryLoader.getInstance().load("w09").toString());
            ps.setString(1, login.getUserID());
            ps.setString(2, login.getAccount());
            ps.setString(3, year);
            ps.setString(6, year);
            ps.setString(7, login.getUserID());
            ps.setString(10, login.getAccount());
            ps.setString(11, year);
            ps.setString(12, login.getUserID());

            final Iterator ite = charge.iterator();
            while (ite.hasNext()) {
                final ChargeClassData data = (ChargeClassData) ite.next();

                // 学年
                ps.setInt(4, data.getGrade());
                ps.setInt(8, data.getGrade());
                ps.setInt(13, data.getGrade());
                // クラス
                ps.setString(5, data.getClassName());
                ps.setString(9, data.getClassName());
                ps.setString(14, data.getClassName());

                rs = ps.executeQuery();
                if (rs.next()) {
                    container.add(data);
                }
            }

        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }

        return container;
    }

    /**
     * @return
     */
    public Profile getProfile() {
        return profile;
    }

    /**
     * @param string
     */
    public void setBundleCD(String string) {
        bundleCD = string;
    }

    /**
     * @param string
     */
    public void setProfileID(String string) {
        profileID = string;
    }

    /**
     * @param session
     */
    public void setLoginSession(LoginSession session) {
        loginSession = session;
    }

    /**
     * @param isRegardable 設定する isRegardable。
     */
    public void setRegardable(boolean isRegardable) {
        this.isRegardable = isRegardable;
    }


}

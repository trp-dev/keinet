package jp.co.fj.keinavi.servlets.user;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.user.LoginUserBean;
import jp.co.fj.keinavi.beans.user.LoginUserDeleteBean;
import jp.co.fj.keinavi.beans.user.UserPasswordInitializerBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.user.LoginUserData;
import jp.co.fj.keinavi.forms.user.U001Form;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.CollectionUtil;

/**
 *
 * 利用者管理一覧画面サーブレット
 * 
 * 2005.10.12	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class U001Servlet extends DefaultHttpServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		// アクションフォーム
		final U001Form form = (U001Form) getActionForm(
				request, "jp.co.fj.keinavi.forms.user.U001Form");
		
		// 転送先がJSP
		if ("u001".equals(getForward(request))) {
			
			// ログインセッション
			final LoginSession login = getLoginSession(request);
			
			// 転送元がJSP
			if ("u001".equals(getBackward(request))) {
				
				// 削除
				if ("1".equals(form.getActionMode())) {
					deleteLoginUser(request, login, form);
					actionLog(request, "703");
				// パスワード初期化
				} else if ("2".equals(form.getActionMode())) {
					initPassword(request, login, form);
					actionLog(request, "602");
				// 不正
				} else {
					throw new ServletException(
							"不正な動作モードです。" + form.getActionMode());
				}
			}
			
			// 利用者情報Beanをセットアップ
			setupLoginUserBean(request, login);
			// 印刷指定のチェック状態
			request.setAttribute(
					"LoginIdChecked", CollectionUtil.array2TrueMap(form.getLoginId()));
			
			super.forward(request, response, JSP_U001);
		
		// 不明なら転送
		} else {
			
			// 印刷であるならセッションにアクションフォームをセットする
			if ("sheet".equals(getForward(request))) {
				request.getSession(false).setAttribute(
						"U001Form", form);
			}
			
			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

	/**
	 * 利用者情報Beanのセットアップ
	 * 
	 * @param request
	 * @throws ServletException
	 */
	protected void setupLoginUserBean(final HttpServletRequest request,
			final LoginSession login) throws ServletException {
		
		// 利用者情報Bean
		final LoginUserBean bean = new LoginUserBean(login.getUserID());
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			
			bean.setConnection(null, con);
			bean.execute();
			
		} catch (final Exception e) {
			throw new ServletException(e);
			
		} finally {
			releaseConnectionPool(request, con);				
		}
		
		// 先頭は管理者のはず
		if (bean.getLoginUserList().size() == 0
				|| !((LoginUserData) bean.getLoginUserList().get(0)).isManager()) {
			
			final KNServletException e = new KNServletException(
					"利用者データがありません。");
			e.setErrorCode("1");
			throw e;
		}
		
		request.setAttribute("LoginUserBean", bean);
	}
	
	/**
	 * 利用者を削除する
	 * 
	 * @param request
	 * @param login
	 * @throws ServletException 
	 */
	protected void deleteLoginUser(final HttpServletRequest request,
			final LoginSession login, final U001Form form) throws ServletException {

		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);
			
			final LoginUserDeleteBean bean = new LoginUserDeleteBean(
					login.getUserID(), form.getTargetLoginId());
			bean.setConnection(null, con);
			bean.execute();
			
			con.commit();
			
		} catch (final Exception e) {
			rollback(con);
			throw new ServletException(e);
			
		} finally {
			releaseConnectionPool(request, con);				
		}
	}
	
	/**
	 * 利用者のパスワードを初期化する
	 * 
	 * @param request
	 * @param login
	 * @throws ServletException 
	 */
	protected void initPassword(final HttpServletRequest request,
			final LoginSession login, final U001Form form) throws ServletException {
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);
			
			final UserPasswordInitializerBean bean = new UserPasswordInitializerBean(
					login.getUserID(), form.getTargetLoginId());
			bean.setConnection(null, con);
			bean.execute();
			
			con.commit();
			
		} catch (final Exception e) {
			rollback(con);
			throw new ServletException(e);
			
		} finally {
			releaseConnectionPool(request, con);				
		}
	}
}

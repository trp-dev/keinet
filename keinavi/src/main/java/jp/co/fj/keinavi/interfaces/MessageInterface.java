package jp.co.fj.keinavi.interfaces;


/**
 * タイトル:      MessageInterface
 * 説明:         アプリケーションで使用するインターフェースを定義する
 * 著作権:        Copyright (c) 2002
 * 会社名:         (株)富士通北陸システムズ
 * @author        t-aoki
 * @version       1.0
 *  history
 *   Symbol Date       Person     Note
 *          2003.12    N.Fujita   Created.
 *   add 1  2003.01.26 N.Fujita   change  メッセージ追加（Sessionに必要な情報がない場合　Z903）
 */

public interface MessageInterface {

  static final String MSG_LOGIN_OK                  = "000";  //  "正常処理"
  static final String MSG_CSV_ACCESS_ERROR          = "901";   // "CSVの取り込みに失敗しました"

  static final String MSG_UNIV_NO_INPUT_ERROR       = "Z001"; //  "大学名未入力エラー"
  static final String MSG_UNIV_NO_HIT_ERROR_Con     = "Z002"; //  "該当する大学は存在しません"
  static final String MSG_200_OVER_HIT_ERROR        = "Z003"; //  "200件以上ヒットしました"
  static final String MSG_UNIV_NO_HIT_ERROR_50      = "Z004"; //  "該当大学無し(５０音)"
  static final String MSG_FAVORITE_20_OVERE_ERROR   = "Z005"; //  "出願候補大学２０件"
  static final String MSG_FAVORITE_NO_SELECT_ERROR  = "Z006"; //  "追加対象が未選択"
  static final String MSG_SESSION_TIME_OUT_ERROR    = "Z007"; //  "SessionTimeOut"
  static final String MSG_UNIV_NO_HIT_ERROR_In      = "Z008"; //  "該当する大学は存在しません"
  static final String MSG_DEPT_NO_HIT_ERROR         = "Z901"; //  "該当学部無し"
  static final String MSG_SUB_NO_HIT_ERROR          = "Z902"; //  "該当学科無し"
  static final String MSG_SUB_NO_PAGE_ERROR         = "Z903"; //  "ページの有効期限切れ" add 1  2003.01.26 N.Fujita

  //得点入力系エラーメッセージ
  static final String MSG_B001 = "※得点を入力してください。";
  static final String MSG_B002 = "※科目を選択してください。";
  static final String MSG_B003 = "※数字を入力してください。";
  static final String MSG_B004 = "※満点を超えています。";
  static final String MSG_B005 = "※偏差値を入力してください。";
  static final String MSG_B006 = "※偏差値に誤りがあります。";
  static final String MSG_B007 = "※同一科目は選択できません。";

  //例外(Exception)メッセージ
  static final String MSG_EXCEPTION_ERROR          = "予期せぬエラー";    //予想外のエラー




}
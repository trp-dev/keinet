package jp.co.fj.keinavi.beans.csv;
import java.util.Vector;

/**
 * CSV形式の1行分のデータを保持するクラス。
 * CSV形式への書き出しでエンクォートの指定
 */
public class CSVLine {
	
	
	private String[] 	items;
	private Vector		vecItems;
	
	
	/**
	 * CSVLine のインスタンスを作成。
	 */
	public CSVLine() {
		vecItems = new Vector();
	}
	
	
	/**
	 * CSVLine のインスタンスを作成。
	 * @param	items	一行分のデータ
	 */
	public CSVLine(String[] items) {
		this.items 	= items;
		vecItems 	= new Vector();
		for(int i = 0; i < items.length; i++){
			vecItems.add(items[i]);
		}
	}
	
	
	/**
	 * 1行の項目数を返す。
	 * @return	項目数
	 */
	public int getCount() {
		
		return vecItems.size();
	}
	
	
	/**
	 * 最大セル数を設定。
	 * @param	count	最大セル数
	 */
	public void setCount(int count) {
		
		if(vecItems.size() > count) {
			for(int i = vecItems.size() -1; i >= count; i--){
				// データを削除
				vecItems.remove(i);
			}
		} else {
			// 今のサイズよりも大きい値が設定された場合
			for(int i = vecItems.size(); i < count; i++) {
				// 空文字を追加
				vecItems.add(i, "");
			}
		}
	}
	
	
	/**
	 * i番目の項目をStringで返す。
	 * @param	i	項目の番号[1 〜 size()]
	 * @return	i番目の文字列
	 */
	public String getString(int i) {
		return (String)vecItems.elementAt(i - 1);
	}
	
	
	/**
	 * 引数で指定された文字列を末尾に追加。
	 * @param	item	追加する文字列
	 */
	public void addString(String item) {
		
		vecItems.addElement(unwrap(item));
	}
	
	
	/**
	 * 引数1で指定されたセル位置の文字列を
	 * 引数2で指定された文字列に置換。
	 * @param	i	置換する位置
	 * @param	s	置換する文字列
	 */
	public void setString(int i, String s) {
		
		if(vecItems.size() > i - 1) {
			// データを追加
			vecItems.add(i - 1, unwrap(s));
			// データを削除
			vecItems.remove(i);
		}
	}
	
	
  	/**
  	 * 現在の配列を返却。
  	 * @return	現在の配列
  	 */
  	public String[] getItems() {
		
		return (String[])vecItems.toArray(new String[vecItems.size()]);
	}
  	
  	
  	/**
  	 * 現在の配列の内容を置き換え
  	 * @param	items	新しい配列
  	 */
  	public void setItems(String[] items) {
		this.items = items;
		vecItems.removeAllElements();
		for(int i = 0; i < items.length; i++) {
			vecItems.add(items[i]);
		}
	}
	
	
	/**
	 * 1行のCSV形式のデータを返す。
	 * @param	delimiter	区切り文字
	 * @return １行のCSV形式のデータ
	 */
	public String getLine(String delimiter) {
		StringBuffer list = new StringBuffer();
		for (int i = 0; i < vecItems.size(); i++) {
			// 要素を文字列に変換しカンマ区切りにする
			list.append(enquote(vecItems.elementAt(i).toString(), true, delimiter));
			if (vecItems.size() - 1 != i) {
				list.append(delimiter);
			}
		}
		return new String(list);
	}
	
	
	
	/**
	 * 1行のCSV形式のデータを返す。""は付けない。
	 * @param	delimiter	区切り文字
	 * @return １行のCSV形式のデータ
	 */
	public String getLineNOQuoMark(String delimiter) {
		StringBuffer list = new StringBuffer();
		for (int i = 0; i < vecItems.size(); i++) {

			list.append(vecItems.elementAt(i).toString());
			if (vecItems.size() - 1 != i) {
				list.append(delimiter);
			}
		}
		return new String(list);
	}
	
	/**
	 * 区切り記号を指定してエンクォートする。
	 *
	 * @param item 処理したい文字列
	 * @param enquote trueなら強制的にエンクォートする
	 * @param delimiter 区切り記号
	 * @return item を処理した文字列
	 */
	public static String enquote(String item, boolean enquote, String delimiter) {
	
		if (item.length() == 0) {
			return item;
		}
		if (item.indexOf('"') < 0 && item.indexOf(delimiter) < 0 && enquote == false) {
			return item;
        }
		
		// StringBufferのサイズは、最も異常な場合を想定した。
		// 文字列 """"" をエンクォートして出力するようなときのこと。
		
		StringBuffer sb = new StringBuffer(item.length() * 2 + 2);
		sb.append('"');
		for (int ind = 0; ind < item.length(); ind ++) {
			char ch = item.charAt(ind);
			if ('"' == ch) {
				sb.append("\"\"");
			} else {
				sb.append(ch);
			}
		}
		sb.append('"');
		
		return new String(sb);
	}
	
	
	/**
   	 * ダブルクオーテーションでラップされた文字列を元に戻します。
     * @param str  ラップ後文字列
     * @return     ラップ前文字列
     */
  	public String unwrap(String str){
    	// 安全のための保険
    	if(str == null) str = "";
		
    	// ダブルクォートで囲まれている文字列かどうか判断。
    	// 判断基準は行頭と行末がダブルクォートかどうか。
/*    	if(str.startsWith("\"") && str.endsWith("\"")){
      		// 行頭と行末がダブルクォート
      		// 行頭と行末のダブルクォートを取る
	      	str = str.substring(1,str.length()-1);
    	}
*/		
    	StringBuffer result = new StringBuffer();
		
    	for(int index = 0; index < str.length(); index++){
     		char c = str.charAt(index);
      		if(c == '\\'){
        		// スラッシュだった場合
        		if(index+1 >= str.length()) continue;
        		char nextChar = str.charAt(index+1);
        		if(nextChar == 'n'){
          			// "\n"だった場合は改行コードに変換する
          			result.append('\n');
          			index++;
          			continue;
        		}else if(nextChar == '\\'){
          			// "\\"だった場合は"\"に変換する
          			result.append('\\');
          			index++;
          			continue;
        		}
        		// '\'が単独で出てきた場合は'\'はappendせずに次の文字を無条件に設定する
        		result.append(nextChar);
        		index++;
        		continue;
      		}
			
      		result.append(c);
    	}
    	return result.toString();
  	}
	
	
	/**
	 * デバッグ用
	 */
	public String getDebug() {
		return vecItems.toString();
	}
}
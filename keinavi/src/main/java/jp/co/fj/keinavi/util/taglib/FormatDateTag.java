package jp.co.fj.keinavi.util.taglib;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.taglibs.standard.lang.support.ExpressionEvaluatorManager;

/**
 *
 * 日付フォーマットタグ
 * 
 * fmtタグを使うとロケールを使って文字コードの自動設定が行われ
 * 「−」などが文字化けするために作成
 * 
 * 2006.01.10	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class FormatDateTag extends TagSupport {

	private String value;
	private String pattern;
	private final Map formatMap = new HashMap();
	
	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {

		final Date date = (Date) ExpressionEvaluatorManager.evaluate(
			    "value", value, Date.class, this, pageContext);
		
		if (date == null) {
			return EVAL_PAGE;
		}
		
		try {
			pageContext.getOut().print(createDateFormat(pattern).format(date));
		} catch (final IOException e) {
			throw new JspException(e.getMessage(), e);
		}

		return EVAL_PAGE;
	}

	private DateFormat createDateFormat(final String p) {
		
		if (!formatMap.containsKey(p)) {
			initFormat(p);
		}
		
		return (DateFormat) formatMap.get(p);
	}
	
	private synchronized void initFormat(final String p) {
		
		if (formatMap.containsKey(p)) {
			return;
		}
		
		formatMap.put(p, new SimpleDateFormat(p));
	}

	/* (非 Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	public int doStartTag() throws JspException {
		return SKIP_BODY;
	}

	/**
	 * @param pPattern 設定する pattern。
	 */
	public void setPattern(String pPattern) {
		pattern = pPattern;
	}

	/**
	 * @param pValue 設定する value。
	 */
	public void setValue(String pValue) {
		value = pValue;
	}

}

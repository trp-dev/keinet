package jp.co.fj.keinavi.beans.sheet;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.PrefBean;
import jp.co.fj.keinavi.beans.com_set.UnivBean;
import jp.co.fj.keinavi.beans.security.MenuSecurityBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.com_set.ChargeClassData;
import jp.co.fj.keinavi.data.com_set.ComClassData;
import jp.co.fj.keinavi.data.com_set.ComSubjectData;
import jp.co.fj.keinavi.data.com_set.CompClassData;
import jp.co.fj.keinavi.data.com_set.CompSchoolData;
import jp.co.fj.keinavi.data.com_set.cm.UnivData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.data.sheet.PrintNumberDefinition;
import jp.co.fj.keinavi.data.sheet.SheetSchoolData;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.ProfileUtil;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.log.KNSheetLog;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;
import jp.co.totec.config.ConfigResolver;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 *
 * 2005.03.31	Yoshimoto KAWAI - Totec
 * 				得点帯マスタ対応
 *
 * 2005.04.05	Yoshimoto KAWAI - Totec
 * 				過年度の表示対応
 *
 * 2005.04.24	Yoshimoto KAWAI - Totec
 * 				過回対象期間を2年間から3年間とした
 *
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 * <大規模改修>
 * 2016.01.08 	Hiroyuki Nishiyama - QuiQsoft
 *
 * @author kawai
 *
 */
public abstract class AbstractSheetBean extends DefaultBean {

	private static final Map noQuestionMap = new HashMap();

	static {
		final Configuration config = ConfigResolver.getInstance(
				).getConfiguration("noquestion.properties");
		for (final Iterator ite = config.getKeys(); ite.hasNext();) {
			final String key = (String) ite.next();
			noQuestionMap.put(key, CollectionUtil.array2Set(
					config.getStringArray(key)));
		}
	}

	protected String pastExam1; // 過回模試１
	protected String pastExam2; // 過回模試２

	protected ArrayList outfileList; // 出力Excelファイル名のリスト

	protected Profile profile; // プロファイル
	protected ExamSession examSession; // 模試セッション
	protected ExamData exam; // 模試データ
	protected LoginSession login; // ログインセッション
	protected Map item; // 画面毎のアイテムマップ
	protected Map cmItem; // 共通項目設定のアイテムマップ
	protected String sessionKey; // セッションキー
	protected KNLog log; // ログインスタンス
	protected HttpSession session;	// HTTPセッション
	protected String backward; // 転送元
	protected KNSheetLog sheetLog; // 帳票用Logger

	private String action ; // アクション

	/**
	 * 帳票を作成する
	 * @return
	 */
	abstract protected boolean createSheet();

	/**
	 * カテゴリIDを取得する
	 * @return
	 */
	abstract protected String getCategory();

	/**
	 * メニュ権限を取得する。
	 * @return
	 */
	protected MenuSecurityBean getMenuSecurityBean() {
		return (MenuSecurityBean) session.getAttribute(
				MenuSecurityBean.SESSION_KEY);
	}

	/**
	 * 印刷フラグを取得する
	 * @return
	 */
	protected int getAction() {
		return Integer.parseInt(action);
	}

	/**
	 * 出力種別フラグを取得する
	 *
	 * @return 0...偏差値用 / 1...得点用
	 */
	protected int getOutputType() {
		return KNUtil.isNewExam(exam) ? 1 : 0;
	}

	/**
	 * @return 帳票の印刷枚数
	 */
	protected double calcNumberOfPrint() {
		return 0;
	}

	/**
	 * @return 印刷枚数定義から枚数を取得する
	 */
	protected double getNumberOfPrint(
			final String id, final String examTypeCd) {
		return ((PrintNumberDefinition) session.getServletContext(
				).getAttribute("SheetNumberDefinition")).getNumber(id, examTypeCd);
	}

	/**
	 * @return 印刷枚数定義から枚数を取得する
	 */
	protected double getNumberOfPrint(final String id) {
		return getNumberOfPrint(id, exam.getExamTypeCD());
	}

	/**
	 * 大学マスタを利用するクエリを書き換える
	 * （高１・２の場合のみ）
	 */
	protected void rewriteUnivQuery(final Query query) {
		KNUtil.rewriteUnivQuery(query, exam);
	}

	/**
	 * 偏差値帯マスタを利用しているQueryを取得する
	 *
	 * @param key
	 * @return
	 * @throws IOException
	 */
	protected Query getQueryWithDeviationZone(String key) throws SQLException {
		Query query = QueryLoader.getInstance().load(key);

		// 新テストは得点帯マスタを使う
		if (KNUtil.isNewExam(exam)) {
			query.replaceAll("deviationzone", "scorezone");
		}

		return query;
	}

	/**
	 * 大学集計区分を取得する
	 * @return
	 */
	protected String getUnivCountngDiv() {
		switch (getDaiTotalFlg()) {
			case 1: return "1";
			case 2: return "0";
			case 3: return "3";
			case 4: return "2";
			case 5: return "4";
			default: throw new IllegalArgumentException("大学集計区分の値が不正です。");
		}
	}

	/**
	 * @return 大学集計区分（プロファイル値）
	 */
	protected int getDaiTotalFlg() {

		final int value = ((Short)item.get(IProfileItem.UNIV_COUNTING_DIV)).intValue();

		// 高1・2大学マスタ利用の場合
		// 日程あり→日程なしとする
		if (KNUtil.isUniv12Exam(exam)) {
			if (value == 1) {
				return 2;
			} else if (value == 3) {
				return 4;
			} else {
				return value;
			}
		} else {
			return value;
		}
	}

	/**
	 * 志望大学を取得するQueryを返す
	 * @param flag
	 * @return
	 */
	protected Query getRatingQuery() throws SQLException {

		// 日程を評価するか
		boolean schedule = item.get(IProfileItem.UNIV_SCHEDULE) != null;

		final Query query;
		switch (getDaiTotalFlg()) {
			// 大学（日程あり）
			case 1:
				query = QueryLoader.getInstance().load("s24_1");
				if (schedule) {
					query.replaceAll(
						"/* SCHEDULE CONDITION */",
						"AND rs.agendacd IN ('D', 'N', '1', ' ')"
					);
				}
				break;
			// 大学（日程なし）
			case 2:
				query = QueryLoader.getInstance().load("s24_2");
				break;
			// 学部（日程あり）
			case 3:
				query = QueryLoader.getInstance().load("s24_3");
				if (schedule) {
					query.replaceAll(
						"/* SCHEDULE CONDITION */",
						"AND rs.agendacd IN ('D', 'N', '1', ' ')"
					);
				}
				break;
			// 学部（日程なし）
			case 4:
				query = QueryLoader.getInstance().load("s24_4");
				break;
			// 学科
			case 5:
				query = QueryLoader.getInstance().load("s24_5");
				if (schedule && "2".equals(exam.getMasterDiv())) {
					query.replaceAll(
						"/* SCHEDULE CONDITION */",
						"AND u.unigdiv IN ('D', 'N', '1', ' ')"
					);
				}
				break;
			// エラー
			default:
				throw new IllegalArgumentException("大学集計区分の値が不正です。");
		}

		// 2016/01/20 QQ)Nishiyama 大規模改修 ADD START
		// ORDER BY句の作成
		createOrderBy(query);
		// 2016/01/20 QQ)Nishiyama 大規模改修 ADD END

		// 現役高卒区分をセット
		query.setStringArray(1, getStudentDiv());
		// 評価区分をセット
		query.setStringArray(2, (String[]) item.get(IProfileItem.UNIV_RATING));
		// 書き換える
		rewriteUnivQuery(query);

		return query;
	}

	// 2016/01/20 QQ)Nishiyama 大規模改修 ADD START
	/**
	 * Order By句を作成する
	 *
	 * @return
	 */
	protected void createOrderBy(Query query) {

		// 大学集計区分
		int skKbn = getDaiTotalFlg();

		query.append (" ORDER BY ");

		// ====================
		// *-*-*   大学   *-*-*
		// ====================
		query.append ("    studentdiv");			// 現役高卒区分
		query.append ("  , univdiv");				// 大学区分（大学コードの先頭１桁の変換結果）

		// 選択順
		if (new Short((short) 1).equals(item.get(IProfileItem.UNIV_ORDER))) {
			query.append ("  , dispsequence");		// 表示順
		// 大学区分・名称順
		} else {
			query.append ("  , univname_kana");		// 大学名カナ
			query.append ("  , univcd");			// 大学コード
		}

		// ====================
		// *-*-*   学部   *-*-*
		// ====================
		// 学部　または　学科
		if (skKbn >= 3) {
			query.append ("  , uninightdiv");		// 大学夜間部区分
			query.append ("  , facultyconcd");		// 学部内容コード
		}

		// 日程あり
		if (skKbn == 1 || skKbn == 3) {
			query.append ("  , agendacd");			// 日程コード
		}

		// ====================
		// *-*-*   学科   *-*-*
		// ====================
		if (skKbn == 5) {
			query.append ("  , unigdiv");				// 大学グループ区分
			query.append ("  , deptserial_no");			// 学科通番
			query.append ("  , schedulesys");			// 日程方式
			query.append ("  , schedulesysbranchcd");	// 日程方式枝番
			query.append ("  , deptname_kana");			// 学科カナ名
			query.append ("  , deptcd");				// 学科コード
		}

		query.append ("  , ratingdiv");			// 評価区分

		/*
		System.out.println("表示順序　　：" + item.get(IProfileItem.UNIV_ORDER));
		System.out.println("大学集計区分：" + skKbn);
		System.out.println("ORDER BY句");
		System.out.println(query.toString());
		*/

	}
	// 2016/01/20 QQ)Nishiyama 大規模改修 ADD END

	/**
	 * 現役高卒区分の設定値を取得する
	 *
	 * @return
	 */
	protected String[] getStudentDiv() {
		if (item.containsKey(IProfileItem.UNIV_STUDENT)) {
			return
				item.get(IProfileItem.UNIV_STUDENT) == null
				? new String[]{"3"}
				: (String[]) item.get(IProfileItem.UNIV_STUDENT);
		} else {
			return new String[]{"1"};
		}
	}

	/**
	 * 指定された年度で存在する模試をカウントする
	 * 今年度から実施を始めた模試に対する対策
	 * @param years
	 * @return
	 * @throws SQLException
	 */
	protected int getExistsExamCount(String[] years) throws SQLException {
		int count = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Query query =
				new Query(
					"SELECT COUNT(e.examyear) FROM examcdtrans e "
					+ "WHERE e.examyear IN (#) AND e.curexamcd = ?"
				);
			query.setStringArray(1, years);
			ps = conn.prepareStatement(query.toString());
			ps.setString(1, exam.getExamCD());
			rs = ps.executeQuery();
			if (rs.next()) count = rs.getInt(1);

			// ゼロはありえない
			if (count == 0)
				throw new InternalError("模試マスタのカウントに失敗しました。");

		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}

		return count;
	}


	/**
	 * 外部データ開放日の表記を内部データ開放日にする
	 * @param query
	 */
	protected void outDate2InDate(Query query) {
		if (login.isHelpDesk()) query.replaceAll("out_dataopendate", "in_dataopendate");
	}

	/**
	 * プロファイルのフラグ値を取得する
	 * （汎用的）
	 * @return
	 */
	protected int getIntFlag(String key) {
		Short value = (Short)item.get(key);
		if (value == null) throw new IllegalArgumentException("フラグ値が設定されていません。"+key);
		return value.intValue();
	}

	/**
	 * プロファイルのフラグ値を取得する
	 * （汎用的）
	 * @param key
	 * @param value デフォルト値
	 * @return
	 */
	protected int getIntFlag(final String key, final int defValue) {
		Short value = (Short) item.get(key);
		if (value == null) return defValue;
		else return value.intValue();
	}

	/**
	 * 県名を取得する
	 * @return
	 */
	protected String getPrefName() throws SQLException {
		PrefBean pref = PrefBean.getInstance(conn);
		return (String) pref.getPrefMap().get(login.getPrefCD());
	}

	/**
	 * 型・科目の設定値を取得する
	 * @param graph グラフ表示対象の設定値のみ取得するかどうか
	 * @return 設定値（型・科目コード）のリスト
	 */
	private List getSubjectValue(boolean graph) throws Exception {
		List container = new LinkedList();

		// 型
		// 共通項目設定を利用
		if (new Short((short) 1).equals(item.get(IProfileItem.TYPE_USAGE))) {
			ComSubjectData data = ProfileUtil.getComTypeData(profile, exam);

			// 全選択
			if (new Short((short) 1).equals(cmItem.get(IProfileItem.TYPE_SELECTION))) {
				// グラフ表示対象を取得するなら設定値から
				if (graph) {
					if (data != null)
						container.addAll(
							CollectionUtil.array2List(
								ProfileUtil.getSubjectAllValue(data, true)));

				// そうでなければ科目別成績から抽出する
				} else {
					container.addAll(this.getExistsSubCDList(1));
				}

			// 個別選択
			} else {
				if (data != null)
					container.addAll(
						CollectionUtil.array2List(
							ProfileUtil.getSubjectIndValue(data, graph)));
			}

		// 個別選択
		} else if (new Short((short) 0).equals(item.get(IProfileItem.TYPE_USAGE))) {
			container.addAll(
				CollectionUtil.array2List(
					ProfileUtil.getSubjectIndValue(
						(ComSubjectData) item.get(IProfileItem.TYPE_IND), graph)));
		}

		// 科目
		// 共通項目設定を利用
		if (new Short((short) 1).equals(item.get(IProfileItem.COURSE_USAGE))) {
			ComSubjectData data = ProfileUtil.getComCourseData(profile, exam);

			// 全選択
			if (new Short((short) 1).equals(cmItem.get(IProfileItem.COURSE_SELECTION))) {
				// グラフ表示対象を取得するなら設定値から
				if (graph) {
					if (data != null)
						container.addAll(
							CollectionUtil.array2List(
								ProfileUtil.getSubjectAllValue(data, true)));

				// そうでなければ科目別成績から抽出する
				} else {
					container.addAll(this.getExistsSubCDList(2));
				}

			// 個別選択
			} else {
				if (data != null)
					container.addAll(
						CollectionUtil.array2List(
							ProfileUtil.getSubjectIndValue(data, graph)));
			}

		// 個別選択
		} else if (new Short((short) 0).equals(item.get(IProfileItem.COURSE_USAGE))) {
			container.addAll(
				CollectionUtil.array2List(
					ProfileUtil.getSubjectIndValue(
						(ComSubjectData) item.get(IProfileItem.COURSE_IND), graph)));
		}

		return container;
	}

	/**
	 * 比較対象高校の設定値を取得する
	 * @return
	 */
	protected List getCompSchoolList() {
		List container = new ArrayList(); // 入れ物

		List value = (List) cmItem.get(IProfileItem.SCHOOL_COMP_SCHOOL);

		// 設定が無いのはありえない
		if (value == null || value.size() == 0) {
			throw new IllegalArgumentException("比較対象高校の設定値が不正です。");
		}

		Iterator ite = value.iterator();
		while (ite.hasNext()) {
			CompSchoolData data = (CompSchoolData) ite.next();

			SheetSchoolData s = new SheetSchoolData();
			s.setSchoolCD(data.getSchoolCD());
			s.setGraphFlag(data.getGraphDisp());
			container.add(s);
		}

		// コード順なら並べ替えておく
		if (new Short((short) 2).equals(item.get(IProfileItem.SCHOOL_ORDER))) {
			Collections.sort(container);
		}

		return container;
	}

	/**
	 * 受験生がいる型・科目コードのリストを取得する
	 * 営業は考慮しなくてよい、、、はず
	 * @param mode
	 * @return
	 */
	protected List getExistsSubCDList(int mode) throws Exception {
		List container = new LinkedList();

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Query query = QueryLoader.getInstance().load("sheet04");

			switch (mode) {
				// 型
				case 1: query.append("AND rs.subcd >= '7000' "); break;
				// 科目
				case 2: query.append("AND rs.subcd < '7000' "); break;
			}

			query.append("GROUP BY rs.subcd");

//			// クラス成績概況・個人成績推移のみクラス別から集計する
//			if (this.getCategory().equals(IProfileCategory.C_COND_TRANS)) {
//				query.replaceAll("subrecord_s", "subrecord_i");
//			}

			ps = conn.prepareStatement(query.toString());
			ps.setString(1, exam.getExamYear());
			ps.setString(2, exam.getExamCD());
			ps.setString(3, profile.getBundleCD());

			rs = ps.executeQuery();
			while (rs.next()) container.add(rs.getString(1));
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}

		return container;
	}

	/**
	 * 型･科目の設定値を取得する
	 * @return
	 */
	protected String[] getSubjectCDArray() throws Exception {
		return (String[]) this.getSubjectValue(false).toArray(new String[0]);
	}

	/**
	 * 型・科目のグラフ表示対象リストを取得する
	 * @return
	 */
	protected List getSubjectGraphList() throws Exception {
		return this.getSubjectValue(true);
	}

	/**
	 * 過年度の表示の設定値を取得する
	 * @return 年度の配列（降順）
	 */
	protected String[] getUnivYearArray() {
		List container = new ArrayList(); // 入れ物

		// プロファイルから比較対象年度の設定を取得する
		String[] years = (String[]) item.get(IProfileItem.PREV_DISP);
		// 最小年度
		int min = Integer.parseInt(KNUtil.getCurrentYear()) - 6;

		// 詰める
		if (years != null) {
			for (int i=0; i<years.length; i++) {
				int year = Integer.parseInt(exam.getExamYear()) - Integer.parseInt(years[i]);
				if (year >= min) container.add(String.valueOf(year));
			}
		}

		// 2008年度以降の高2記述模試
		// 2007年度以前とは過年度比較不可
		if ("65".equals(exam.getExamCD()) && "2008".compareTo(exam.getExamYear()) < 1) {
			int max = 6 - (2008 - Integer.parseInt(exam.getExamYear()));
			for (int i = 1; i <= max; i++) {
				container.remove((2008 - i) + "");
			}
		}

		// 対象年度も入れる
		container.add(exam.getExamYear());

		Collections.sort(container); // ソート
		Collections.reverse(container); // 降順

		return (String[]) container.toArray(new String[0]);
	}

	/**
	 * 比較対象年度の設定値を取得する
	 * @return 年度の配列（降順）
	 */
	protected String[] getCompYearArray() {
		List container = new ArrayList(); // 入れ物

		// プロファイルから比較対象年度の設定を取得する
		String[] years = (String[])cmItem.get(IProfileItem.PREV_COMP_YEAR);

		// 詰める
		for (int i=0; i<years.length; i++) {
			container.add(
				String.valueOf(
					Integer.parseInt(exam.getExamYear()) - Integer.parseInt(years[i])
				)
			);
		}

		// 対象年度も入れる
		container.add(exam.getExamYear());

		Collections.sort(container); // ソート
		Collections.reverse(container); // 降順

		return (String[]) container.toArray(new String[0]);
	}

	/**
	 * 指定された模試が模試セッションに存在するかどうか
	 * @param examYear
	 * @param examCD
	 * @return
	 */
	protected boolean isExistExam(String examYear, String examCD) {
		return examSession.getExamData(examYear, examCD) != null;
	}

	/**
	 * 集計用志望大学へデータを入れる
	 */
	protected void insertIntoCountUniv() throws Exception {

		PreparedStatement ps = null;
		try {

			final List container;

			// 上位20校
			// ※営業は個別選択のみ
			if (new Short((short) 1).equals(cmItem.get(IProfileItem.UNIV_SELECTION_FORMULA))) {
				UnivBean bean = new UnivBean();
				bean.setConnection(null, this.conn);
				bean.setExam(exam);
				bean.setBundleCD(profile.getBundleCD());
				bean.execute();

				container = new LinkedList();

				Iterator ite = bean.getFullList().iterator();
				while (ite.hasNext()) {
					UnivData data = (UnivData) ite.next();
					container.add(data.getUnivCode());
				}

			// 個別選択
			} else {
				// プロファイル設定値
				container =
					CollectionUtil.array2List((String[])cmItem.get(IProfileItem.UNIV));

				// コード順ならソート
				if (new Short((short) 2).equals(item.get(IProfileItem.UNIV_ORDER))) {
					Collections.sort(container);
				}
			}

			ps = conn.prepareStatement(QueryLoader.getInstance().load("sheet05").toString());

			int count = 0;
			Iterator ite = container.iterator();
			while (ite.hasNext()) {
				final String univ = (String) ite.next();
				ps.setString(1, univ);
				ps.setInt(2, count++);
				ps.setString(3, univ.substring(0, 1));
				ps.execute();
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}

		// TODO デバッグ用
		//System.out.println(SQLUtil.getAllRecord(conn, "countuniv"));
	}

	/**
	 * 集計用比較対象クラスへデータを入れる
	 * @param examYear 模試年度
	 * @param examCD 模試コード
	 * @throws SQLException
	 */
	protected void insertIntoCountCompClass() throws SQLException, IOException {

	    // 全選択かどうか
	    boolean all =
	        new Short((short) 1).equals(cmItem.get(IProfileItem.CLASS_SELECTION))	;

	    // 設定値を取得する
		List c = new ArrayList();
		if (cmItem.containsKey(IProfileItem.CLASS_COMP_CLASS)) {
			List container = (List)cmItem.get(IProfileItem.CLASS_COMP_CLASS);

			int index = container.indexOf(
			        new CompClassData(exam.getExamYear(), exam.getExamCD()));

			if (index >= 0) {
				CompClassData data = (CompClassData) container.get(index);

			    // 全選択
				if (all && data.getAClassData() != null) {
			        c.addAll(data.getAClassData());

				// 個別選択
				} else if (!all && data.getIClassData() != null){
				    c.addAll(data.getIClassData());
				}
			}
		}

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
		    // 設定値がないか全選択なら設定値を抽出する
			if (c.size() == 0 || all) {

			    // グラフ表示対象は抽出しておく
			    Set graph = new HashSet();
		        Iterator ite = c.iterator();
		        while (ite.hasNext()) {
		            ComClassData data = (ComClassData) ite.next();
		            if (data.getGraphDisp() == 1) graph.add(data.getKey());
		        }

			    // 設定値はクリア
			    c.clear();

				Query query = QueryLoader.getInstance().load("sheet02");

				// 比較対象クラスを抽出するテーブルは画面ごとに異なる
				if (IProfileCategory.S_CLASS_QUE.equals(this.getCategory())) {
					query.replaceAll("subrecord_c", "qrecord_c");
				} else if (IProfileCategory.S_CLASS_UNIV.equals(this.getCategory())) {
					query.replaceAll("subrecord_c", "ratingnumber_c");
				} else if (IProfileCategory.C_COMP_QUE.equals(this.getCategory())) {
					query.replaceAll("subrecord_c", "qrecord_c");
				} else if (IProfileCategory.C_COMP_UNIV.equals(this.getCategory())) {
					query.replaceAll("subrecord_c", "ratingnumber_c");
				}

				ps = conn.prepareStatement(query.toString());
				ps.setString(1, exam.getExamYear()); // 模試年度
				ps.setString(2, exam.getExamCD()); // 模試コード
				ps.setString(3, profile.getBundleCD()); // 一括コード
				rs = ps.executeQuery();

				while (rs.next()) {
				    ComClassData data = new ComClassData();
				    data.setGrade(rs.getShort(1));
				    data.setClassName(rs.getString(2));
				    if (graph.contains(data.getKey())) data.setGraphDisp((short) 1);
				    c.add(data);
				}

				rs.close();
				ps.close();
			}

			ps = conn.prepareStatement("INSERT INTO countcompclass VALUES(?, ?, ?, ?)");

			int count = 0; // 表示順のカウント

			// 選択順
			if (new Short((short) 1).equals(item.get(IProfileItem.CLASS_ORDER))) {
				Iterator ite = c.iterator();
				while (ite.hasNext()) {
					ComClassData data = (ComClassData) ite.next();

					ps.setInt(1, data.getGrade());
					ps.setString(2, data.getClassName());
					ps.setShort(3, data.getGraphDisp());
					ps.setInt(4, count++);
					ps.execute();
				}

			// コード順
			} else {
				// 昇順にする
				Collections.sort(c);

				// 3,2,1年
				for (int i=3 ; i>0; i--) {
					Iterator ite = c.iterator();
					while (ite.hasNext()) {
						ComClassData data = (ComClassData) ite.next();

						if (data.getGrade() != i) continue;

						ps.setInt(1, data.getGrade());
						ps.setString(2, data.getClassName());
						ps.setString(3, String.valueOf(data.getGraphDisp()));
						ps.setInt(4, count++);
						ps.execute();
					}
				}

				// 4,5年
				Iterator ite = c.iterator();
				while (ite.hasNext()) {
					ComClassData data = (ComClassData) ite.next();

					if (data.getGrade() <= 3) continue;

					ps.setInt(1, data.getGrade());
					ps.setString(2, data.getClassName());
					ps.setString(3, String.valueOf(data.getGraphDisp()));
					ps.setInt(4, count++);
					ps.execute();
				}
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}

		// TODO デバッグ用
		//System.out.println(SQLUtil.getAllRecord(conn, "countcompclass"));
	}

	/**
	 * 科目コード変換テーブルにレコードを追加する
	 * @throws SQLException
	 */
	protected void insertIntoSubCDTrans(ExamData[] exam) throws Exception {

		KNUtil.buildSubCDTransTable(conn, exam);

		// TODO デバッグ用
		//System.out.println(SQLUtil.getAllRecord(conn, "subcdtrans"));
	}

	/**
	 * 模試コード変換テーブルのセットアップ
	 */
	protected void insertIntoExamCdTrans() throws SQLException {
		insertIntoExamCdTrans(new ExamData[]{exam});
	}

	/**
	 * 模試コード変換テーブルのセットアップ
	 *
	 * @param 対象模試データの配列
	 * @throws SQLException
	 */
	protected void insertIntoExamCdTrans(final ExamData[] exams) throws SQLException {

		KNUtil.buildExamCdTransTable(conn, exams);

		// TODO デバッグ用
		//System.out.println(SQLUtil.getAllRecord(conn, "examcdtrans"));
	}

	/**
	 * @return 科目別成績（全国）における現役高卒区分の最小値
	 * @throws SQLException
	 */
	protected String getMinStudentDiv() throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					"SELECT MIN(studentdiv) FROM subrecord_a "
					+ "WHERE examyear = ? AND examcd = ?");
			// 模試年度
			ps.setString(1, exam.getExamYear());
			// 模試コード
			ps.setString(2, exam.getExamCD());

			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString(1);
			} else {
				throw new SQLException(
						"科目別成績（全国）のレコードがありません。");
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	/**
	 * 集計用担当クラスへ設定値を入れる
	 * @throws SQLException
	 */
	protected void insertIntoCountChargeClass() throws SQLException {

		// 入れ物
		final List container = new ArrayList();

		// 印刷対象クラス
		final String print = (String) item.get(IProfileItem.PRINT_CLASS);

		// 全て
		if (print == null) {
			container.addAll((List) cmItem.get(IProfileItem.CLASS));
			Collections.sort(container);
		// 個別
		} else {
			container.add(new ChargeClassData(print.split(":", 2)));
		}

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("INSERT INTO countchargeclass VALUES(?, ?, ?)");
			int count = 0; // 表示順のカウント
			for (final Iterator ite = container.iterator(); ite.hasNext();) {
				final ChargeClassData data = (ChargeClassData) ite.next();
				ps.setInt(1, data.getGrade()); // 学年
				ps.setString(2, data.getClassName()); // クラス
				ps.setInt(3, count++);
				ps.execute();
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}

		// TODO デバッグ用
		//System.out.println(SQLUtil.getAllRecord(conn, "countchargeclass"));
	}

	/**
	 * 集計用担当クラスをセットアップする
	 * （テキスト出力用）
	 *
	 * @throws SQLException
	 * @throws IOException
	 */
	protected void setupCountChargeClass() throws SQLException, IOException {

		// 担当クラスの設定値
		final List charge = (List) cmItem.get(IProfileItem.CLASS);
		// 担当クラスの年度
		final String year = (String) cmItem.get(IProfileItem.CLASS_YEAR);

		// 通常クラス
		PreparedStatement ps1 = null;
		try {
			ps1 = conn.prepareStatement("INSERT INTO countchargeclass VALUES(?, ?, 1)");

			for (final Iterator ite = charge.iterator(); ite.hasNext(); ) {
				final ChargeClassData data = (ChargeClassData) ite.next();

				if (data.getClassName().length() > 2) {
					continue;
				}
				ps1.setInt(1, data.getGrade());
				ps1.setString(2, data.getClassName());
				ps1.executeUpdate();
			}

		} finally {
			DbUtils.closeQuietly(ps1);
		}

		// 複合クラス
		PreparedStatement ps2 = null;
		try {
			ps2 = conn.prepareStatement(QueryLoader.getInstance().load("t06").toString());
			ps2.setString(1, year); // 年度
			ps2.setString(2, login.getUserID()); // 学校コード

			for (final Iterator ite = charge.iterator(); ite.hasNext(); ) {
				final ChargeClassData data = (ChargeClassData) ite.next();

				if (data.getClassName().length() <= 2) {
					continue;
				}
				ps2.setInt(3, data.getGrade());
				ps2.setString(4, data.getClassName());
				ps2.executeUpdate();
			}

		} finally {
			DbUtils.closeQuietly(ps2);
		}

		// TODO デバッグ用
		//System.out.println(SQLUtil.getAllRecord(conn, "countchargeclass"));
	}

	/**
	 * 有効模試テーブルへデータを入れる
	 */
	protected void insertIntoExistsExam() throws SQLException {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("INSERT INTO existsexam VALUES(?, ?)");

			String[] years = examSession.getYears();

			for (int i=0; i<years.length; i++) {
				Iterator ite = ((List)examSession.getExamMap().get(years[i])).iterator();
				while (ite.hasNext()) {
					ExamData data = (ExamData) ite.next();

					ps.setString(1, data.getExamYear()); // 模試年度
					ps.setString(2, data.getExamCD()); // 模試コード
					ps.execute();

					// デバッグ用
					//System.out.println("有効模試：" + data.getExamYear() + ":" + data.getExamCD());
				}
			}

		} finally {
			DbUtils.closeQuietly(ps);
		}

		// TODO デバッグ用
		//System.out.println(SQLUtil.getAllRecord(conn, "existsexam"));
	}

	/**
	 * 同系統で過去の模試の模試データ配列を取得する
	 * insertIntoExistsExam()を呼び出しておくこと
	 * @return
	 */
	protected ExamData[] getPastExamArray() throws Exception {
		List container = new ArrayList();

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Query query = QueryLoader.getInstance().load("sheet03");
			this.outDate2InDate(query); // データ開放日の書き換え

			ps = conn.prepareStatement(query.toString());
			ps.setString(1, exam.getExamTypeCD()); // 模試種類コード
			ps.setString(2, exam.getExamYear()); // 模試年度
			ps.setInt(3, Integer.parseInt(exam.getTargetGrade())); // 対象学年
			ps.setString(4, String.valueOf(Integer.parseInt(exam.getExamYear()) - 1)); // 模試年度（前年度）
			ps.setInt(5, Integer.parseInt(exam.getTargetGrade()) - 1); // 対象学年（前年度）
			ps.setString(6, String.valueOf(Integer.parseInt(exam.getExamYear()) - 2)); // 模試年度（前々年度）
			ps.setInt(7, Integer.parseInt(exam.getTargetGrade()) - 2); // 対象学年（前々年度）
			ps.setString(8, exam.getDataOpenDate()); // データ開放日

			rs = ps.executeQuery();
			while (rs.next()) {
				ExamData data = new ExamData();
				data.setExamYear(rs.getString(1));
				data.setExamCD(rs.getString(2));
				data.setExamNameAbbr(rs.getString(3));
				data.setTargetGrade(rs.getString(4));
				data.setInpleDate(rs.getString(5));
				data.setDispSequence(rs.getInt(6));
				data.setDataOpenDate(rs.getString(7));
				container.add(data);
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}

		// TOP7
		if (container.size() > 7) {
			return (ExamData[]) container.subList(
					0, 7).toArray(new ExamData[7]);
		} else {
			return (ExamData[]) container.toArray(
					new ExamData[container.size()]);
		}
	}

	/**
	 * @param code 出力する科目コードの配列
	 * @return 設問別成績が存在しない科目のみ対象となっているか
	 */
	protected boolean hasAllNoQuestionSubject(final String[] code) {

		if (noQuestionMap.containsKey(exam.getExamCD())) {
			final Set set = (Set) noQuestionMap.get(exam.getExamCD());
			for (int i = 0; i < code.length; i++) {
				if (!set.contains(code[i])) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}


	// --------------------------------------------------------------------------------

	/**
	 * @param string
	 */
	public void setAction(String string) {
		action = string;
	}

	/**
	 * @param data
	 */
	public void setExam(ExamData data) {
		exam = data;
	}

	/**
	 * @param session
	 */
	public void setExamSession(ExamSession session) {
		examSession = session;
	}

	/**
	 * @param session
	 */
	public void setLogin(LoginSession session) {
		login = session;
	}

	/**
	 * @param list
	 */
	public void setOutfileList(ArrayList list) {
		outfileList = list;
	}

	/**
	 * @param string
	 */
	public void setPastExam1(String string) {
		pastExam1 = string;
	}

	/**
	 * @param string
	 */
	public void setPastExam2(String string) {
		pastExam2 = string;
	}

	/**
	 * @param profile
	 */
	public void setProfile(Profile profile) {
		this.profile = profile;

		// アイテムマップを初期化する
		if (profile != null) {
			item = profile.getItemMap(getCategory());
			cmItem = profile.getItemMap(IProfileCategory.CM);
		}
	}

	/**
	 * @param string
	 */
	public void setSessionKey(String string) {
		sessionKey = string;
	}

	/**
	 * @param log 設定する log。
	 */
	public void setLog(KNLog log) {
		this.log = log;
	}

	/**
	 * @param pSession 設定する session。
	 */
	public void setSession(HttpSession pSession) {
		session = pSession;
	}

	/**
	 * @param pBackward 設定する backward。
	 */
	public void setBackward(String pBackward) {
		backward = pBackward;
	}

	/**
	 * @param pSheetLog 設定する sheetLog。
	 */
	public void setSheetLog(KNSheetLog pSheetLog) {
		sheetLog = pSheetLog;
	}

}

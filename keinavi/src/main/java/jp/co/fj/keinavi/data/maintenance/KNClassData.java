package jp.co.fj.keinavi.data.maintenance;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 *
 * クラスデータ
 * 
 * 2005.11.24	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class KNClassData {

	// 年度
	private final String year;
	// 学年
	private final int grade;
	// クラス
	private final String className;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pYear 年度
	 * @param pGrade 学年
	 * @param pClassName クラス
	 */
	public KNClassData(final String pYear, final int pGrade,
			final String pClassName) {
		
		this.year = pYear;
		this.grade = pGrade;
		this.className = pClassName;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(final Object obj) {
		
		if (!(obj instanceof KNClassData)) {
			return false;
		}

		KNClassData data = (KNClassData) obj;
		
		return new EqualsBuilder()
				.append(this.year, data.year)
				.append(this.grade, data.grade)
				.append(this.className, data.className)
				.isEquals();
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		
		return new HashCodeBuilder(17, 37)
				.append(year)
				.append(grade)
				.append(className)
				.toHashCode();
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		
		return new ToStringBuilder(this)
				.append("Year", year)
				.append("Grade", grade)
				.append("ClassName", className)
				.toString();
	}
	
	/**
	 * @return className を戻します。
	 */
	public String getClassName() {
		return className;
	}
	/**
	 * @return grade を戻します。
	 */
	public int getGrade() {
		return grade;
	}
	/**
	 * @return year を戻します。
	 */
	public String getYear() {
		return year;
	}
	
}

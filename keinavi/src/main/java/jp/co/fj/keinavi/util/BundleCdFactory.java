package jp.co.fj.keinavi.util;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;

/**
 *
 * 一括コードファクトリ
 * 
 * 2006.02.03	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public abstract class BundleCdFactory {

	private static BundleCdFactory FACTORY;
	
	/**
	 * @param login ログイン情報
	 * @param profile プロファイル
	 * @param examCd 模試コード
	 * @return 一括コード
	 */
	protected abstract String create(LoginSession login, Profile profile, String examCd);
	
	/**
	 * @param login ログイン情報
	 * @param profile プロファイル
	 * @param examCd 模試コード
	 * @return 一括コード
	 */
	public static String getBundleCd(final LoginSession login,
			final Profile profile, final String examCd) {

		if (FACTORY == null) {
			return profile.getBundleCD();
		} else {
			return FACTORY.create(login, profile, examCd);
		}
	}

	/**
	 * @param pInstance 設定する instance。
	 */
	public static void setFactory(final BundleCdFactory factory) {
		FACTORY = factory;
	}
	
}

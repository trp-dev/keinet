/**
 * 校内成績分析−過回比較　偏差値分布
 *      Excelファイル編集
 * 作成日: 2019/09/25
 * @author      DP)H.Nonaka
 */

package jp.co.fj.keinavi.excel.school;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.cm.CefrCommon;
import jp.co.fj.keinavi.excel.cm.CefrCommon.SIDE;
import jp.co.fj.keinavi.excel.data.school.S52CefrAcqStatusListBean;
import jp.co.fj.keinavi.excel.data.school.S52Item;
import jp.co.fj.keinavi.excel.data.school.S52PtCefrAcqStatusListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class S52_11 {

        private int noerror = 0; // 正常終了
        private int errfread = 1; // ファイルreadエラー
        private int errfwrite = 2; // ファイルwriteエラー
        private int errfdata = 3; // データ設定エラー

        private CM cm = new CM(); // 共通関数用クラス インスタンス
        private CefrCommon cefrCm = null;

        private String masterfile = "S52_11"; // ファイル名

        private static final int MAX_INDEX_COUNT = 8; // ブロック最大数

        private final int MAX_COL_COUNT = 14; // 表内最大CEFR列数
        private final String TOTAL_SHIKEN_NAME = "全試験";

        private int intSecuFlg = 0; // セキュリティスタンプフラグ
        private String strGakkomei = ""; // 学校名
        private String strMshmei = ""; // 模試名
        private String strMshDate = ""; // 模試実施基準日

        // Enum(Excel座標 列,行)
        public enum CURSOL {
                SECURITY_STAMP(29, 0),
                SCHOOL_NAME(0, 3),
                ALL_EXAM_TABLE_COPY_START(0, 1),
                ALL_EXAM_TABLE_COPY_END(15, 8),
                EXAM_TABLE_COPY_A_START(0, 1),
                EXAM_TABLE_COPY_A_END(15, 8),
                EXAM_TABLE_COPY_B_START(1, 1),
                EXAM_TABLE_COPY_B_END(15, 8),
                EXAM_TABLE_PASTE_START(0, 7),
                EXAM_TABLE_LINE_A1_START(1, 9),
                EXAM_TABLE_LINE_A2_START(1, 20),
                EXAM_TABLE_LINE_A3_START(1, 31),
                EXAM_TABLE_LINE_A4_START(1, 42),
                EXAM_TABLE_LINE_A5_START(1, 53),
                EXAM_TABLE_LINE_A6_START(1, 64),
                EXAM_TABLE_LINE_A7_START(1, 75),
                EXAM_TABLE_LINE_A8_START(1, 86),
                EXAM_TABLE_LINE_B1_START(16, 9),
                EXAM_TABLE_LINE_B2_START(16, 20),
                EXAM_TABLE_LINE_B3_START(16, 31),
                EXAM_TABLE_LINE_B4_START(16, 42),
                EXAM_TABLE_LINE_B5_START(16, 53),
                EXAM_TABLE_LINE_B6_START(16, 64),
                EXAM_TABLE_LINE_B7_START(16, 75),
                EXAM_TABLE_LINE_B8_START(16, 86),
                TERMINAL(0,0);

                private final int col;
                private final int row;

                private CURSOL(int col, int row) {
                        this.col = col;
                        this.row = row;
                }

                public int getCol() {
                        return this.col;
                }

                public int getRow() {
                        return this.row;
                }
        }

/*
 *      Excel編集メイン
 *              S52Item S52Item: データクラス
 *              String outfile: 出力Excelファイル名（フルパス）
 *              int             intSaveFlg: 1:保存 2:印刷 3:保存／印刷
 *              String  UserID：ユーザーID
 *              戻り値: 0:正常終了、0以外:異常終了
 */
        public int s52_11EditExcel(S52Item S52Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

                KNLog log = KNLog.getInstance(null,null,null);
                log.Ep("S52_11","S52_11帳票作成開始","");

                // マスタExcel読み込み
                HSSFWorkbook            workbook        = null;

                // マスタExcel読み込み
                workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
                if( workbook==null ){
                        return errfread;
                }

                try {

                        // Excel認定試験CEFR取得状況共通関数インスタンス生成
                        cefrCm = new CefrCommon(workbook);

                        // シート内最大ブロック数
                        cefrCm.setMaxRowCount(MAX_INDEX_COUNT);
                        // テンプレートシート指定
                        cefrCm.setTemplateWorkbook();

                        // セキュリティスタンプフラグ
                        this.intSecuFlg = S52Item.getIntSecuFlg();
                        // 学校名
                        this.strGakkomei = S52Item.getStrGakkomei();
                        // 模試名
                        this.strMshmei = S52Item.getStrMshmei();
                        // 模試実施基準日
                        this.strMshDate = S52Item.getStrMshDate();
                        // 自校受験者がいる試験を対象にするフラグ
                        cefrCm.setIntTargetCheckBoxFlg(S52Item.getIntTargetCheckBoxFlg());

                        // 新ページ作成処理
                        this.newCreateSheet(cm);

                        // CEFR取得状況データセット
                        ArrayList S52CefrAcqStatusList = S52Item.getS52CefrAcqStatusList();

                        if (S52CefrAcqStatusList.size() > 0) {

                                // CEFR取得状況枠設定
                                this.copyCefrAcqStatusFrame(cm);
                                // CEFR取得状況表データ設定
                                this.setCefrData(S52CefrAcqStatusList, cm);

                                // CEFR取得状況データセット
                                ArrayList S52PtCefrAcqStatusList = S52Item.getS52PtCefrAcqStatusList();

                                if (S52PtCefrAcqStatusList.size() > 0) {
                                    // 認定試験別・合計CEFR取得状況表作成
                                    this.setPtCefrAcqStatus(S52PtCefrAcqStatusList, cm);
                                }

                                // テンプレートシート削除
                                if( workbook.getSheetIndex(CefrCommon.templateSheetName) >= 0){
                                        workbook.removeSheetAt(workbook.getSheetIndex(CefrCommon.templateSheetName));
                                }

                                //2019/10/03 QQ)Oosaki アクティブシートを選択 ADD START
                                workbook.getSheetAt(1).setSelected(true);
                                //2019/10/03 QQ)Oosaki アクティブシートを選択 ADD END

                                // Excelファイル保存
                                boolean bolRet = false;
                                bolRet = cm.bolFileSave(intSaveFlg, outfilelist, cefrCm.getWorkbook(), UserID, 0, masterfile, cefrCm.getMaxSheetIndex());

                                if( bolRet == false ){
                                        return errfwrite;
                                }
                        }
                } catch(Exception e) {
                        log.Err("S52_11","データセットエラー",e.toString());
                        e.printStackTrace();
                        return errfdata;
                }

                log.Ep("S52_11","S52_11帳票作成終了","");
                return noerror;
        }

        /**
         * <DL>
         * <DT>全試験　表貼り付け
         * </DL>
         * @param cm Excel共通関数インスタンス
         */
        public void copyCefrAcqStatusFrame(CM cm) {

                // テンプレートカーソル位置設定
                int tempCurColS = CURSOL.ALL_EXAM_TABLE_COPY_START.getCol();
                int tempCurRowS = CURSOL.ALL_EXAM_TABLE_COPY_START.getRow();
                int tempCurColE = CURSOL.ALL_EXAM_TABLE_COPY_END.getCol();
                int tempCurRowE = CURSOL.ALL_EXAM_TABLE_COPY_END.getRow();

                // 貼り付けカーソル位置
                int pasteCurCol = CURSOL.EXAM_TABLE_PASTE_START.getCol();
                int pasteCurRow = CURSOL.EXAM_TABLE_PASTE_START.getRow();

                // 表テンプレートコピー
                cm.copyCellArea(cefrCm.getWorkSheet(), cefrCm.getTemplateWorkbook(), tempCurColS, tempCurRowS, tempCurColE, tempCurRowE, pasteCurCol, pasteCurRow);
        }

        /**
         * <DL>
         * <DT>CEFR取得状況表データ設定
         * </DL>
         * @param list
         * @param cm Excel共通関数インスタンス
         */
        public void setCefrData(ArrayList list, CM cm) {

            String moshi = "";   // 模試月
            String bakCefrLevel = "";

                if (list.size() > 0) {

                        // カーソル位置初期化
                        this.setInitCursolPosition();

                        // "全試験"
                        cefrCm.setCurRow(cefrCm.getCurRow() - 2);
                        this.setTableStr(cm, TOTAL_SHIKEN_NAME);
                        // カーソル位置初期化
                        this.setInitCursolPosition();

                        Iterator itrList = list.iterator();
                        while( itrList.hasNext() ) {

                                S52CefrAcqStatusListBean bean = (S52CefrAcqStatusListBean)itrList.next();

                                // 表内最大CEFR列数を超える場合、処理中断
                                if (cefrCm.getCurCol() - cefrCm.getInitialCurCol() > this.MAX_COL_COUNT
                                        && !cefrCm.totalCefrCd.equals(bean.getCefrlevelcd())) {
                                        continue;
                                }

                                // 過回模試名領域
                                if (bean.getCefrlevelcd().toString().equals(cefrCm.totalCefrCd)) {
                                    cefrCm.setCurCol(0);

                                    // 模試名セット
                                    moshi = cm.setTaisyouMoshi(this.strMshDate);       // 模試月取得
                                    this.setTableStr(cm, bean.getExamname() + moshi);
                                    cefrCm.setCurRow(cefrCm.getCurRow() - 1);
                                    cefrCm.setCurCol(cefrCm.getCurCol() + 1);
                                }
                                // CEFRレベル名領域
                                if (!bean.getCefrlevelcd().toString().equals(bakCefrLevel)) {
                                    if (bakCefrLevel.toString().equals("")) {
                                        cefrCm.setCurCol(cefrCm.getCurCol() + 1);
                                    } else {
                                        if (bakCefrLevel.toString().equals(cefrCm.totalCefrCd)) {
                                            cefrCm.setCurCol(cefrCm.getCurCol() + 1);
                                        } else {
                                            cefrCm.setCurCol(cefrCm.getCurCol() + 2);
                                        }
                                    }
                                    cefrCm.setCurRow(cefrCm.getInitialCurRow() - 1);

                                    this.setTableStr(cm, bean.getCerflevelname_abbr());

                                    bakCefrLevel = bean.getCefrlevelcd().toString();
                                }


                                // 2019/10/10 QQ)Ooseto 共通テスト対応 UPD START
                                // CEFRレベル別　受験者数

                                //cefrCm.setCerfNumber(cm, bean.getCefrlevelcd(), bean.getNumbers(), bean.getFlg());
                                cefrCm.setCerfNumber(cm, bean.getCefrlevelcd(), bean.getNumbers(), Float.parseFloat("99.9"), bean.getFlg());

                                if (!bean.getCefrlevelcd().toString().equals(cefrCm.totalCefrCd)) {
                                    // カーソル位置指定
                                    cefrCm.setCurRow(cefrCm.getCurRow() - 1);
                                    cefrCm.setCurCol(cefrCm.getCurCol() + 1);

                                    // CEFRレベル別　構成比
                                    //cefrCm.setCerfCompratio3(cm, bean.getCefrlevelcd(), bean.getCompratio(), bean.getFlg());
                                    cefrCm.setCerfCompratio(cm, bean.getCefrlevelcd(), 99, bean.getCompratio(), bean.getFlg());

                                    // カーソル位置指定
                                    cefrCm.setCurCol(cefrCm.getCurCol() - 1);
                                }
                                // 2019/10/10 QQ)Ooseto 共通テスト対応 UPD END
                        }
                }
        }

        /**
         * <DL>
         * <DT>カーソル位置初期設定
         * </DL>
         */
        public void setInitCursolPosition() {

                // 行位置の決定
                switch(cefrCm.getMaxRowIndex()) {
                        case 0:
                                cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A1_START.getRow());
                                break;
                        case 1:
                                cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A2_START.getRow());
                                break;
                        case 2:
                                cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A3_START.getRow());
                                break;
                        case 3:
                                cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A4_START.getRow());
                                break;
                        case 4:
                                cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A5_START.getRow());
                                break;
                        case 5:
                                cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A6_START.getRow());
                                break;
                        case 6:
                                cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A7_START.getRow());
                                break;
                        case 7:
                                cefrCm.setInitialCurRow(CURSOL.EXAM_TABLE_LINE_A8_START.getRow());
                                break;
                }

                // 列位置の決定
                if (CefrCommon.SIDE.A.equals(cefrCm.getSide())) {
                        cefrCm.setInitialCurCol(CURSOL.EXAM_TABLE_LINE_A1_START.getCol());
                } else {
                        cefrCm.setInitialCurCol(CURSOL.EXAM_TABLE_LINE_B1_START.getCol());
                }

                // カーソル位置指定
                cefrCm.setCurCol(cefrCm.getInitialCurCol());
                cefrCm.setCurRow(cefrCm.getInitialCurRow());
        }

        /**
         * <DL>
         * <DT>CEFRレベル　タイトル設定
         * </DL>
         * @param cm Excel共通関数インスタンス
         * @param str CEFRレベル　タイトル
         */
        public void setTableCefrName(CM cm, String str) {
                // カーソル位置指定
                cefrCm.setCurRow(cefrCm.getInitialCurRow() + 1);

                if (StringUtils.isNotEmpty(str)) {
                        cefrCm.setCellValueAddRowCountUp(cm, str);
                } else {
                        cefrCm.setCurRow(cefrCm.getCurRow() + 1);
                }
        }

        /**
         * <DL>
         * <DT>文字列設定
         * </DL>
         * @param cm Excel共通関数インスタンス
         * @param str 文字列
         */
        public void setTableStr(CM cm, String str) {
                if (StringUtils.isNotEmpty(str)) {
                        cefrCm.setCellValueAddRowCountUp(cm, str);
                } else {
                        cefrCm.setCurRow(cefrCm.getCurRow() + 1);
                }
        }

        /**
         * <DL>
         * <DT>改表キー作成
         * <DD>参加試験コード + ":" + 参加試験レベルコード
         * </DL>
         * @param bean DB検索結果Bean
         * @return 生成キー
         */
        private String createKeyTable(S52PtCefrAcqStatusListBean bean) {
                return bean.getEngptcd() + ":" + bean.getEngptlevelcd();
        }

        /**
         * <DL>
         * <DT>改行キー作成
         * <DD>参加試験コード
         * </DL>
         * @param bean DB検索結果Bean
         * @return 生成キー
         */
        private String createKeyRow(S52PtCefrAcqStatusListBean bean) {
                return bean.getEngptcd();
        }

        /**
         * <DL>
         * <DT>表テンプレートコピー処理
         * </DL>
         * @param cm Excel共通関数インスタンス
         */
        private void copyTableTemplate(CM cm) {

                // テンプレートカーソル位置設定
                int tempCurColS = CURSOL.EXAM_TABLE_COPY_A_START.getCol();
                int tempCurRowS = CURSOL.EXAM_TABLE_COPY_A_START.getRow();
                int tempCurColE = CURSOL.EXAM_TABLE_COPY_A_END.getCol();
                int tempCurRowE = CURSOL.EXAM_TABLE_COPY_A_END.getRow();
                if (!SIDE.A.equals(cefrCm.getSide())) {
                        tempCurColS = CURSOL.EXAM_TABLE_COPY_B_START.getCol();
                        tempCurColE = CURSOL.EXAM_TABLE_COPY_B_END.getCol();
                }

                // 貼り付けカーソル位置
                int pasteCurCol = cefrCm.getCurCol();
                int pasteCurRow = cefrCm.getCurRow();
                if (SIDE.A.equals(cefrCm.getSide())) {
                        pasteCurCol--;
                }

                // 表テンプレートコピー
                cm.copyCellArea(cefrCm.getWorkSheet(), cefrCm.getTemplateWorkbook(), tempCurColS, tempCurRowS, tempCurColE, tempCurRowE, pasteCurCol, pasteCurRow);
        }

        /**
         * <DL>
         * <DT>認定試験別・合計CEFR取得状況表作成
         * </DL>
         * @param list
         * @param cm Excel共通関数インスタンス
         */
        public void setPtCefrAcqStatus(ArrayList list, CM cm) {

                String moshi = "";   // 模試月
                String bakCefrLevel = "";
                Integer intCount = 0;
                String nowKeyTable = "";
                String nowKeyRow = "";
                boolean bolTableHeaderFlg = false;
                //boolean bolTableDataFlg = false;
                boolean retCheckKeyTable = false;
                boolean retCheckKeyRow = false;

// 2019/10/08 QQ)Tanioka テンプレートのみが出力されないよう修正 ADD START
                boolean firstFlg = true;
// 2019/10/08 QQ)Tanioka テンプレートのみが出力されないよう修正 ADD END

                if (list.size() > 0) {

                        // 改行実施
                        cefrCm.setTableSide(SIDE.A);
// 2019/10/08 QQ)Tanioka テンプレートのみが出力されないよう修正 DEL START
//                      // カーソル位置初期化
//                      this.setInitCursolPosition();
//                      cefrCm.setCurRow(cefrCm.getCurRow() - 2);
//
//                      // 表テンプレートコピー
//                      this.copyTableTemplate(cm);
//
//                      // カーソル位置初期化
//                      this.setInitCursolPosition();
//
//                      // カーソル位置初期化
//                      this.setInitCursolPosition();
//                      cefrCm.setCurRow(cefrCm.getCurRow() - 2);
//
//                      // 表テンプレートコピー
//                      this.copyTableTemplate(cm);
//2019/10/08 QQ)Tanioka テンプレートのみが出力されないよう修正 DEL END

                        // カーソル位置初期化
                        this.setInitCursolPosition();

                        Iterator itrList = list.iterator();
                        while( itrList.hasNext() ) {

                                S52PtCefrAcqStatusListBean bean = (S52PtCefrAcqStatusListBean)itrList.next();

                                if (intCount.equals(0)) {
                                    // 改表キーによる改表チェック
                                    nowKeyTable = this.createKeyTable(bean);
                                    retCheckKeyTable = cefrCm.checkKeyTable(nowKeyTable);
                                    // 改行キーによる改行チェック
                                    nowKeyRow = this.createKeyRow(bean);
                                    retCheckKeyRow = cefrCm.checkKeyRow(nowKeyRow);
                                }

                                // 改表キーによる改表チェック
                                nowKeyTable = this.createKeyTable(bean);
                                retCheckKeyTable = cefrCm.checkKeyTable(nowKeyTable);

                                // データが存在しない英語認定試験は表示しない
                                // ※当年度自校受験者がいる試験を対象にする場合のみ
                                boolean viewFlag = true;
                                if(bean.getFlg().equals("0")) {
                                    viewFlag = false;
                                }
                                if(!cefrCm.checkIntTargetCheckBoxFlg()) {
                                    viewFlag = true;
                                }

                                if (!retCheckKeyTable && viewFlag) {
                                        // 改表処理

                                        bolTableHeaderFlg = false;
                                        //bolTableDataFlg = false;

                                        // 改行キーによる改行チェック
                                        nowKeyRow = this.createKeyRow(bean);
                                        retCheckKeyRow = cefrCm.checkKeyRow(nowKeyRow);

                                        // 表配置に伴う改行チェック
                                        boolean retCheckTableSide = cefrCm.checkTableSide();

                                        if (retCheckKeyRow && retCheckTableSide) {
                                                // 改行は実施しない
                                                cefrCm.setTableSide(SIDE.B);

                                        } else {
                                                // 改行キー相違により改行実施
                                                cefrCm.setTableSide(SIDE.A);

                                                // 改ページ判定
                                                boolean retCheckNewPage = cefrCm.checkNewPage();
                                                if (!retCheckNewPage) {
                                                        // 改ページ実施
                                                        this.newCreateSheet(cm);
                                                }
                                        }

                                        // カーソル位置初期化
                                        this.setInitCursolPosition();
                                        cefrCm.setCurRow(cefrCm.getCurRow() - 2);

                                        // 表テンプレートコピー
                                        this.copyTableTemplate(cm);

                                        // カーソル位置初期化
                                        this.setInitCursolPosition();

                                        intCount = 0;
                                }

                                // 英語認定試験名・試験レベル名
                                if (!bolTableHeaderFlg && viewFlag) {

// 2019/10/08 QQ)Tanioka テンプレートのみが出力されないよう修正 ADD START
                                    //一度目のみテンプレートをセット
                                    if(firstFlg) {

                                    // カーソル位置初期化
                                    this.setInitCursolPosition();
                                    cefrCm.setCurRow(cefrCm.getCurRow() - 2);

                                    // 表テンプレートコピー
                                    this.copyTableTemplate(cm);

                                    // カーソル位置初期化
                                    this.setInitCursolPosition();

                                            firstFlg = false;
                                    }
//2019/10/08 QQ)Tanioka テンプレートのみが出力されないよう修正 ADD END

                                    // フラグOFF時、値を設定
                                        String str = bean.getEngptname_abbr().trim();
                                        if (StringUtils.isNotEmpty(bean.getLevelflg())
                                                && bean.getLevelflg().equals("1")) {
                                                // レベルありフラグONの場合、「認定試験名の短縮名 ＋ 半角SP ＋ 試験レベル名の短縮名」を設定
                                                str = str.concat(" ").concat(bean.getEngptlevelname_abbr().trim());
                                        }
                                        //cefrCm.setTableName(cm, str);
                                        // カーソル位置初期化
                                        this.setInitCursolPosition();
                                        cefrCm.setCurRow(cefrCm.getCurRow() - 2);
                                        this.setTableStr(cm, str);
                                        // カーソル位置初期化
                                        this.setInitCursolPosition();

                                        // 延人数設定
                                        //cefrCm.setTableHeaderPerson(cm, this.TOTAL_PERSON_NAME);

                                        bolTableHeaderFlg = true;
                                }

                                // 表内で初回データ設定時はカーソル位置を指定
                                //if (!bolTableDataFlg && viewFlag) {
                                //        cefrCm.setCurCol(cefrCm.getInitialCurCol() + 1);
                                //        bolTableDataFlg = true;
                                //}

                                // 合計行以外で、表内最大CEFR列数を超える場合、処理スキップ
                                if (cefrCm.getCurCol() - cefrCm.getInitialCurCol() > MAX_COL_COUNT
                                        && !cefrCm.totalCefrCd.equals(bean.getCefrlevelcd())) {
                                        continue;
                                }

                                // 過回模試名領域
/*                                if (SIDE.A.equals(cefrCm.getSide()) && bean.getCefrlevelcd().toString().equals(cefrCm.totalCefrCd)) {

                                    cefrCm.setCurCol(cefrCm.getCurCol() - 1);

                                    // 模試名セット
                                    moshi =cm.setTaisyouMoshi(this.strMshDate);       // 模試月取得
                                    this.setTableStr(cm, bean.getExamname() + moshi);
                                    cefrCm.setCurRow(cefrCm.getCurRow() - 1);
                                    cefrCm.setCurCol(cefrCm.getCurCol() + 1);
                                }
*/
                                // CEFRレベル名領域
/*                                if (!bean.getCefrlevelcd().toString().equals(bakCefrLevel)) {
                                    if (!intCount.equals(0)) {
                                        if (bakCefrLevel.toString().equals(cefrCm.totalCefrCd)) {
                                            cefrCm.setCurCol(cefrCm.getCurCol() + 1);
                                        } else {
                                            cefrCm.setCurCol(cefrCm.getCurCol() + 2);
                                        }
                                    }
                                    cefrCm.setCurRow(cefrCm.getInitialCurRow() - 1);

                                    this.setTableStr(cm, bean.getCerflevelname_abbr());

                                    bakCefrLevel = bean.getCefrlevelcd().toString();
                                }
*/
                                if(viewFlag) {

                                    // 過回模試名領域
                                    if (SIDE.A.equals(cefrCm.getSide()) && bean.getCefrlevelcd().toString().equals(cefrCm.totalCefrCd)) {
                                        cefrCm.setCurCol(0);

                                        // 模試名セット
                                        moshi =cm.setTaisyouMoshi(this.strMshDate);       // 模試月取得
                                        this.setTableStr(cm, bean.getExamname() + moshi);
                                        cefrCm.setCurRow(cefrCm.getCurRow() - 1);
                                        cefrCm.setCurCol(cefrCm.getCurCol() + 1);
                                    }

                                    // CEFRレベル名領域
                                    if (!bean.getCefrlevelcd().toString().equals(bakCefrLevel)) {
                                        if (!intCount.equals(0)) {
                                            if (bakCefrLevel.toString().equals(cefrCm.totalCefrCd)) {
                                                cefrCm.setCurCol(cefrCm.getCurCol() + 1);
                                            } else {
                                                cefrCm.setCurCol(cefrCm.getCurCol() + 2);
                                            }
                                        }
                                        cefrCm.setCurRow(cefrCm.getInitialCurRow() - 1);

                                        this.setTableStr(cm, bean.getCerflevelname_abbr());

                                        bakCefrLevel = bean.getCefrlevelcd().toString();
                                    }


                                    // 2019/10/10 QQ)Ooseto 共通テスト対応 UPD START
                                    // CEFRレベル別　受験者数

                                    //cefrCm.setCerfNumber(cm, bean.getCefrlevelcd(), bean.getNumbers(), bean.getFlg());
                                    cefrCm.setCerfNumber(cm, bean.getCefrlevelcd(), bean.getNumbers(), Float.parseFloat("99.9"), bean.getFlg());

                                    if (!bean.getCefrlevelcd().toString().equals(cefrCm.totalCefrCd)) {
                                        // カーソル位置指定
                                        cefrCm.setCurRow(cefrCm.getCurRow() - 1);
                                        cefrCm.setCurCol(cefrCm.getCurCol() + 1);

                                        // CEFRレベル別　構成比
                                        //cefrCm.setCerfCompratio3(cm, bean.getCefrlevelcd(), bean.getCompratio(), bean.getFlg());
                                        cefrCm.setCerfCompratio(cm, bean.getCefrlevelcd(), 99, bean.getCompratio(), bean.getFlg());

                                        // カーソル位置指定
                                        cefrCm.setCurCol(cefrCm.getCurCol() - 1);
                                    }
                                    // 2019/10/10 QQ)Ooseto 共通テスト対応 UPD END
                                    intCount += 1;
                                }
                        }
                }
        }

        /**
         * <DL>
         * <DT>改ページ処理
         * </DL>
         * @param cm Excel共通関数インスタンス
         */
        public void newCreateSheet(CM cm) {

                cefrCm.initSheetSetting();

                // ヘッダ右側に帳票作成日時を表示する
                cm.setHeader(cefrCm.getWorkbook(), cefrCm.getWorkSheet());

                // セキュリティスタンプセット
                cefrCm.setCurCol(CURSOL.SECURITY_STAMP.getCol());
                cefrCm.setCurRow(CURSOL.SECURITY_STAMP.getRow());
                String secuFlg = cm.setSecurity(cefrCm.getWorkbook(), cefrCm.getWorkSheet(), intSecuFlg, cefrCm.getCurCol(), cefrCm.getCurCol() + 1);
                cefrCm.setCellValueAddRowCountUp(cm, secuFlg);

                // 学校名セット
                cefrCm.setCurCol(CURSOL.SCHOOL_NAME.getCol());
                cefrCm.setCurRow(CURSOL.SCHOOL_NAME.getRow());
                cefrCm.setCellValueAddRowCountUp(cm, "学校名　：" + cm.toString(strGakkomei));

                // 対象模試セット
                String moshi = cm.setTaisyouMoshi(strMshDate); // 模試月取得
                cefrCm.setCellValueAddRowCountUp(cm, cm.getTargetExamLabel() + "：" + cm.toString(strMshmei) + moshi);

                // 表示対象セット
                String cefrMySchoolOnlyLabel = "";
                if (cefrCm.checkIntTargetCheckBoxFlg()) {
                        cefrMySchoolOnlyLabel = cm.getCefrMySchoolOnlyLabel3();
                }
                cefrCm.setCellValueAddRowCountUp(cm, cm.getCefrTargetLabel() + "：" + cefrMySchoolOnlyLabel);
        }
}
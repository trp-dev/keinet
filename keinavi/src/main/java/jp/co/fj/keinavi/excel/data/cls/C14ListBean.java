package jp.co.fj.keinavi.excel.data.cls;

import java.util.ArrayList;
/**
 * クラス成績概況−設問別個人成績データリスト
 * 作成日: 2004/07/15
 * @author	H.Fujimoto
 */
public class C14ListBean {
	//学年
	private String strGrade = "";
	//クラス
	private String strClass = "";
	//科目別成績データリスト
	private ArrayList c14KmkList = new ArrayList();

	/*----------*/
	/* Get      */
	/*----------*/

	public ArrayList getC14KmkList() {
		return this.c14KmkList;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public String getStrGrade() {
		return this.strGrade;
	}

	/*----------*/
	/* Set      */
	/*----------*/

	public void setC14KmkList(ArrayList c14KmkList) {
		this.c14KmkList = c14KmkList;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}

}
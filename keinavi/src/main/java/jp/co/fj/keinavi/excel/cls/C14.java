/**
 * 校内成績分析−クラス比較　成績概況（設問別個人成績）
 * 出力する帳票の判断
 * 作成日: 2004/09/02
 * @author	H.Fujimoto
 */
 
package jp.co.fj.keinavi.excel.cls;

import java.util.*;

import jp.co.fj.keinavi.excel.data.cls.*;
import jp.co.fj.keinavi.excel.cm.*;
import jp.co.fj.keinavi.util.log.*;

public class C14 {

	/*
	 * フラグにより帳票出力の判断をする。
	 * 戻り値: true:正常終了、false:異常終了
	 */

	private CM cm = new CM();		//共通関数用クラス インスタンス

	public boolean c14( C14Item c14Item, ArrayList outfilelist, int saveFlg, String UserID, KNSheetLog sheetLog ) {

		KNLog log = KNLog.getInstance(null,null,null);

		try{
			
//			C14_01 exceledit = new C14_01();
//			exceledit.c14_01EditExcel( c14Item, outfilelist, saveFlg, UserID );

			int ret = 0;
			//C14Itemから各帳票を出力
			if ( c14Item.getIntSortFlg()==0 ) {
				throw new Exception("S01 ERROR : フラグ異常");
			}
			if ( c14Item.getIntSortFlg()==1 || c14Item.getIntSortFlg()==2) {
				if( cm.toString(c14Item.getStrYear()).equals("") || cm.toString(c14Item.getStrMshmei()).equals("")){
					log.Err("0"+ret+"C14_01","パラメータエラー","");
					return false;
				}

				log.Ep("C14_01","C14_01帳票作成開始","");
				C14_01 exceledit = new C14_01();
				ret = exceledit.c14_01EditExcel( c14Item, outfilelist, saveFlg, UserID );
				if (ret!=0) {
					log.Err("0"+ret+"C14_01","帳票作成エラー","");
					return false;
				}
				log.Ep("C14_01","C14_01帳票作成終了","");
				sheetLog.add("C14_01");
			}
			
		} catch(Exception e) {
//			System.out.println(e.toString());
			log.Err("99C14","帳票作成エラー",e.toString());
			return false;
		}
		return true;
	}

}
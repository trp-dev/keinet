/*
 * 作成日: 2004/08/31
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.com_set.cm;

import java.io.Serializable;

/**
 * 型・科目データを保持するクラス
 * 
 * @author kawai
 */
public class Subject implements Serializable {

	private String subCD; // 型・科目コード
	private String subName; // 型・科目名称
	private int examinees = 0; // 受験人数

	/**
	 * コンストラクタ
	 */
	public Subject() {
	}

	/**
	 * コンストラクタ
	 */
	public Subject(String subCD) {
		this.subCD = subCD;
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return this.subCD.equals(((Subject) obj).subCD);
	}

	/**
	 * @return
	 */
	public int getExaminees() {
		return examinees;
	}

	/**
	 * @return
	 */
	public String getSubCD() {
		return subCD;
	}

	/**
	 * @return
	 */
	public String getSubName() {
		return subName;
	}

	/**
	 * @param i
	 */
	public void setExaminees(int i) {
		examinees = i;
	}

	/**
	 * @param string
	 */
	public void setSubCD(String string) {
		subCD = string;
	}

	/**
	 * @param string
	 */
	public void setSubName(String string) {
		subName = string;
	}

}

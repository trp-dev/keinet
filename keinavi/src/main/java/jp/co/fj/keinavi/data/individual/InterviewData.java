/*
 * 作成日: 2004/08/10
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class InterviewData {

	private String individualId; 		//個人ID
	private String koIndividualId;	//相手方システム個人ID
	private String firstRegistrYdt; 	//初期登録日時 yyyymmddhhssmm
	private String createDate;			//作成日 yyyymmdd
	private String renewalDate;		//更新日時
	private String title;				//タイトル 全角１５文字まで
	private String text;				//テキスト 全角２００文字まで
	
	/**
	 * @return
	 */
	public String getCreateDate() {
		return createDate;
	}

	/**
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param string
	 */
	public void setCreateDate(String string) {
		createDate = string;
	}

	/**
	 * @param string
	 */
	public void setText(String string) {
		text = string;
	}

	/**
	 * @param string
	 */
	public void setTitle(String string) {
		title = string;
	}

	/**
	 * @return
	 */
	public String getFirstRegistrYdt() {
		return firstRegistrYdt;
	}

	/**
	 * @return
	 */
	public String getIndividualId() {
		return individualId;
	}

	/**
	 * @param string
	 */
	public void setFirstRegistrYdt(String string) {
		firstRegistrYdt = string;
	}

	/**
	 * @param string
	 */
	public void setIndividualId(String string) {
		individualId = string;
	}


	/**
	 * @return
	 */
	public String getRenewalDate() {
		return renewalDate;
	}

	/**
	 * @param string
	 */
	public void setRenewalDate(String string) {
		renewalDate = string;
	}

	/**
	 * @return koIndividualId を戻します。
	 */
	public String getKoIndividualId() {
		return koIndividualId;
	}

	/**
	 * @param pKoIndividualId 設定する koIndividualId。
	 */
	public void setKoIndividualId(String pKoIndividualId) {
		koIndividualId = pKoIndividualId;
	}

}

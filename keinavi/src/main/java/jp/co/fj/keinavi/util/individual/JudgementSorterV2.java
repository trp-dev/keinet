package jp.co.fj.keinavi.util.individual;

import java.util.Comparator;

import jp.co.fj.kawaijuku.judgement.beans.detail.UnivDetail;
import jp.co.fj.kawaijuku.judgement.beans.judge.Judgement;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.data.individual.UnivKeiNavi;

/**
 * 判定結果ソートクラス
 * 
 * 2004.10.27   Tomohisa YAMADA - Totec
 * 				新規作成
 * 
 * 2005.03.15   Tomohisa YAMADA - Totec
 * 				[1]センター評価のソートに、
 * 				マークで二次を判定した場合を考慮する
 * 
 * 2005.06.16   Tomohisa YAMADA - Totec
 * 				[2]JudgementBeanの[72]を引き継ぐ
 * 
 * 2005.06.27   Tomohisa YAMADA - Totec
 * 				[3]UnivFactoryDBBranchの[17]を引き継ぐ
 * 
 * 2005.12.27   Tomohisa YAMADA - Totec
 * 				[4]一般私大のときは無条件で二次成績をみる
 * 
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 *              センターリサーチ５段階評価対応
 *
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 *
 */
public class JudgementSorterV2 {

	/**
	 * [1] modify
	 * センター評価のよい順番にソート
	 * 
	 */
	public class SortByCenterScoreRank implements Comparator{

		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {
			
		Judgement j1 = (Judgement) o1;
		Judgement j2 = (Judgement) o2;
		
		int j1judgement = j1.getCJudgement();
		int j2judgement = j2.getCJudgement();
		
		int j1ghjudgement = j1.getCGhJudgement();
		int j2ghjudgement = j2.getCGhJudgement();
		
		double j1rank = ((UnivKeiNavi)j1.getUniv()).getCenScoreRate();
		double j2rank = ((UnivKeiNavi)j2.getUniv()).getCenScoreRate();
		
        //[4] del start
//		//マークで二次を判定していたら、二次の成績で比較する
//		if(j1.isSecondByMark){
        //[4] del end
        //[4] add start
        //一般私大なら、二次の成績で比較する
        if ("".equals(j1.getUniv().getSchedule().trim())) {
        //[4] add end
			j1judgement = j1.getSJudgement();
			j1ghjudgement = j1.getSGhJudgement();
			j1rank = j1.getUniv().getRank();
		}
		//センター配点比率が無効ならば、成績も無効化する
		//else if(j1.university.c_maximumScale < 1){//[3] del
		else if(((UnivDetail)j1.getUniv().getDetailHash().get(j1.getSelectedBranch())).getCAllotPointRate() == JudgementConstants.Detail.ALLOT_POINT_RATE_NA){//[3] add
			j1judgement = 0;	
		}
        //[4] del start
//		if(j2.isSecondByMark){
        //[4] del end
        //[4] add start
        if ("".equals(j2.getUniv().getSchedule().trim())) {
        //[4] add end
			j2judgement = j2.getSJudgement();
			j2ghjudgement = j2.getSGhJudgement();
			j2rank = j2.getUniv().getRank();
		}
		//else if(j2.university.c_maximumScale < 1){//[3] del
		else if(((UnivDetail)j2.getUniv().getDetailHash().get(j2.getSelectedBranch())).getCAllotPointRate() == JudgementConstants.Detail.ALLOT_POINT_RATE_NA){//[3] add
			j2judgement = 0;	
		}

		//return compareByScoreScoreRate(j1cjudgement, j1.c_ghjudgement, j1.university.cenScoreRate, j2cjudgement, j2.c_ghjudgement, j2.university.cenScoreRate, j1, j2);//[1] del
		
		//[1] add start
		//２のみ判定不能
		if(j1judgement != JudgementConstants.JUDGE_NP 
				&& j2judgement == JudgementConstants.JUDGE_NP){
			
			return -1;
		}
		//１のみ判定不能
		else if(j1judgement == JudgementConstants.JUDGE_NP 
				&& j2judgement != JudgementConstants.JUDGE_NP){
			
			return 1;
		}
		
		//判定なし
		if (j1judgement != JudgementConstants.Judge.JUDGE_NA 
				&& j2judgement == JudgementConstants.Judge.JUDGE_NA) {
			return -1;
		}
		
		if (j1judgement == JudgementConstants.Judge.JUDGE_NA 
				&& j2judgement != JudgementConstants.Judge.JUDGE_NA) {
			return +1;
		}
		
		//両方共判定不能
		if(j1judgement == JudgementConstants.JUDGE_NP 
				&& j2judgement == JudgementConstants.JUDGE_NP){
			
			//１のみマークで二次を判定
			if(j1.isSecondByMark() && !j2.isSecondByMark()){
				return 1;
			}
			//２のみマークで二次を判定
			else if(!j2.isSecondByMark() && j2.isSecondByMark()){
				return -1;
			}
			//両方ともマークで二次を判定
			else if(j1.isSecondByMark() && j2.isSecondByMark()){
				//ランクでソート
				int rank = compareByRank((int)j1rank, (int)j2rank);
				if(rank == 0){
					return compareByUFDeptSortKey(j1, j2);
				}else{
					return rank;
				}
			}
			//両方共マークでセンター判定
			else{ //(!j1.isSecondByMark && !j2.isSecondByMark){
				//ボーダー得点率でソート
				int scoreRate = compareByScoreRate(j1rank, j2rank);
				if(scoreRate == 0){
					return compareByUFDeptSortKey(j1, j2);
				}else{
					return scoreRate;
				}
			}
		}
		//両方共判定不能でない
		else{
			int ghjudge = compareByGHJudgement(j1ghjudgement, j2ghjudgement);
			//ＧＨ判定が同じ
			if(ghjudge == 0){
				int judge = compareByJudgement(j1judgement, j2judgement);
				//判定が同じ
				if(judge == 0){
					//１のみマークで二次を判定
					if(j1.isSecondByMark() && !j2.isSecondByMark()){
						return 1;
					}
					//２のみマークで二次を判定
					else if(!j2.isSecondByMark() && j2.isSecondByMark()){
						return -1;
					}
					//両方ともマークで二次を判定
					else if(j1.isSecondByMark() && j2.isSecondByMark()){
						//ランクでソート
						int rank = compareByRank((int)j1rank, (int)j2rank);
						if(rank == 0){
							return compareByUFDeptSortKey(j1, j2);
						}else{
							return rank;
						}
					}
					//両方共マークでセンター判定
					else{ //(!j1.isSecondByMark && !j2.isSecondByMark){
						//ボーダー得点率でソート
						int scoreRate = compareByScoreRate(j1rank, j2rank);
						if(scoreRate == 0){
							return compareByUFDeptSortKey(j1, j2);
						}else{
							return scoreRate;
						}
					}
				}else{
					return judge;
				}
			}else{
				return ghjudge;
			}
		}
		//[1] add end
	}	
	}
	
	/**
	 * 二次評価のよい順番にソート
	 * 
	 */
	public class SortBySecondScoreRank implements Comparator{

		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {	
			
			Judgement j1 = (Judgement) o1;
			Judgement j2 = (Judgement) o2;
			
			int j1sjudgement = j1.getSJudgement();

			if(((UnivDetail)j1.getUniv().getDetailHash().get(j1.getSelectedBranch())).getSAllotPointRate() == JudgementConstants.Detail.ALLOT_POINT_RATE_NA){//[3] add
				//判定不能にする
				j1sjudgement = -1;
			}
			
			int j2sjudgement = j2.getSJudgement();

			if(((UnivDetail)j2.getUniv().getDetailHash().get(j2.getSelectedBranch())).getSAllotPointRate() == JudgementConstants.Detail.ALLOT_POINT_RATE_NA){//[3] add
				//判定不能にする
				j2sjudgement = -1;
			}
			
			return compareByScoreRank(j1sjudgement, j1.getSGhJudgement(), j1.getUniv().getRank(), j2sjudgement, j2.getSGhJudgement(), j2.getUniv().getRank(), j1, j2);

		}	
	}
	
	/**
	 * スコア、ランクの比較
	 * @param j1judgement
	 * @param j1ghjudgement
	 * @param j1rank
	 * @param j2judgement
	 * @param j2ghjudgement
	 * @param j2rank
	 * @return
	 */	
	private int compareByScoreRank(
			int j1judgement, 
			int j1ghjudgement, 
			int j1rank, 
			int j2judgement, 
			int j2ghjudgement, 
			int j2rank, 
			Judgement j1, 
			Judgement j2){

		if(j1judgement > 0 && j2judgement < 1){
		//２のみ判定不能
			return -1;
		}else if(j1judgement < 1 && j2judgement > 0){
		//１のみ判定不能
			return 1;
		}else{
		//両方共判定不能”でない”時
			int ghjudge = compareByGHJudgement(j1ghjudgement, j2ghjudgement);
			if(ghjudge == 0){
				int judge = compareByJudgement(j1judgement, j2judgement);
				if(judge == 0){
					int rank = compareByRank(j1rank, j2rank);
					if(rank == 0){
						return compareByUFDeptSortKey(j1, j2);
					}else{
						return rank;	
					}
				}else{
					return judge;
				}
			}else{
				return ghjudge;
			}
		}
	}
	
	/**
	 * カナ付50音順番号〜学科コード比較
	 * @param j1
	 * @param j2
	 * @return
	 */
	private int compareByUFDeptSortKey(Judgement j1, Judgement j2){
		return KanaNumComparator.getInstance().compare(j1.getUniv(), j2.getUniv());
	}
	
	/**
	 * [1] modify
	 * ランク比較
	 * 21, 0, 1, 2, 3, ..., 14, 99, -99
	 * 【良い】       ←→      【悪い】
	 * @param r1
	 * @param r2
	 * @return
	 */
	private int compareByRank(int r1, int r2){
		//[2] add start
		//両方が最高
		if(r1 == 22 && r2 == 22){
			return 0;
		}
		//１のみが最高
		else if(r1 == 22 && r2 != 22){
			return -1;
		}
		//２のみが最高
		else if(r1 != 22 && r2 == 22){
			return 1;
		}
		else{
		//[2] add end
			//両方が最高
			if(r1 == 21 && r2 == 21){
				return 0;
			}
			//１のみが最高
			else if(r1 == 21 && r2 != 21){
				return -1;
			}
			//２のみが最高
			else if(r1 != 21 && r2 == 21){
				return 1;
			}
			else{
				//両方が最低(-99, 99←これは無視でオッケー)
				if(r1 == -99 && r2 == -99){
					return 0;
				}
				//１のみが最低
				else if(r1 == -99 && r2 != -99){
					return 1;
				}
				//２のみが最低
				else if(r1 != -99 && r2 == -99){
					return -1;
				}
				else{
					//rankの低いほうがランクが高い(既に21と99は考慮してある)
					if(r1 < r2){
						return -1;
					}else if(r1 > r2){
						return 1;	
					}else{
						return 0;
					}	
				}
			}
		}//[2] add
	}
	
	/**
	 * [1] modify
	 * センターボーダー得点率 
	 * @param r1
	 * @param r2
	 * @return
	 */
	private int compareByScoreRate(double r1, double r2){
		//両方とも無効値
		if(r1 == 99 && r2 == 99){
			return 0;
		}
		//１のみ無効値
		else if(r1 == 99 && r2 != 99){
			return 1;
		}
		//２のみ無効値
		else if(r1 != 99 && r2 == 99){
			return -1;
		}
		else{
			//ボーダー得点率が高い方が上にくる
			if(r1 > r2){
				return -1;
			}else if(r1 < r2){
				return 1;
			}else{
				return 0;
			}	
		}
	}
	
	/**
	 * 判定比較
	 *  0:判定不能
	 * -1:単なるＧ判定(一科目も受けていない)
	 * @param j1
	 * @param j2
	 * @return
	 */
	private int compareByJudgement(int j1, int j2){
		//両方共判定不能
		if(j1 == 0 && j2 == 0){
			return 0;
		}
		//１のみ判定不能
		else if(j1 == 0 && j2 != 0){
			return 1;
		}
		//２のみ判定不能
		else if(j1 != 0 && j2 == 0){
			return -1;
		}
		else{
			//judgementの高いほうが良い判定
			//１のほうがよい判定
			if(j1 > j2){
				return -1;
			}
			//２のほうがよい判定
			else if(j1 < j2){
				return 1;
			}
			//両方共同じ判定(単なるＧ含む)
			else{
				return 0;
			}
		}
	}
	
	/**
	 * GH判定比較
	 * 1＝Ｈ判定
	 * 2＝Ｇ判定
	 * @param j1
	 * @param j2
	 * @return
	 */
	private int compareByGHJudgement(int j1, int j2){
		
		if (j1 != JudgementConstants.Judge.JUDGE_G 
				&& j2 == JudgementConstants.Judge.JUDGE_G) {
			return -1;
		}
		
		if (j2 != JudgementConstants.Judge.JUDGE_G 
				&& j1 == JudgementConstants.Judge.JUDGE_G) {
			return +1;
		}
		
		if (j1 != JudgementConstants.Judge.JUDGE_SHARP 
				&& j2 == JudgementConstants.Judge.JUDGE_SHARP) {
			return -1;
		}
		
		if (j2 != JudgementConstants.Judge.JUDGE_SHARP 
				&& j1 == JudgementConstants.Judge.JUDGE_SHARP) {
			return +1;
		}
		
		if (j1 != JudgementConstants.Judge.JUDGE_H 
				&& j2 == JudgementConstants.Judge.JUDGE_H) {
			return -1;
		}
		
		if (j2 != JudgementConstants.Judge.JUDGE_H
				&& j1 == JudgementConstants.Judge.JUDGE_H) {
			return +1;
		}
		
		return 0;
	}
	
	
}

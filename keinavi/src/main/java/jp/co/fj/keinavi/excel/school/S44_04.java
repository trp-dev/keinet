package jp.co.fj.keinavi.excel.school;

//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.ListIterator;

import jp.co.fj.keinavi.excel.cm.CM;
import jp.co.fj.keinavi.excel.data.school.S44GakkoListBean;
import jp.co.fj.keinavi.excel.data.school.S44HyoukaNinzuListBean;
import jp.co.fj.keinavi.excel.data.school.S44Item;
import jp.co.fj.keinavi.excel.data.school.S44ListBean;
import jp.co.fj.keinavi.util.log.KNLog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * 作成日: 2004/09/09
 * @author Ito.Y
 *
 * 校内成績分析−他校比較−志望大学評価別人数　大学（学部・日程あり）出力処理
 *
 */
public class S44_04 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー

	private CM cm = new CM();		//共通関数用クラス インスタンス

	private String		strArea			= "A1:AI58";	//印刷範囲
	private String		mastersheet 	= "tmp";		//テンプレートシート名
	private int		intMaxSheetSr	= 50;			//MAXシート数
	private int		intMaxRow		= 50;			//MAX行数

	// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
	//// 2019/09/30 QQ) 共通テスト対応 UPD START
	//private int		intMaxClassSr   = 3;			//MAXクラス数
	//// 2019/09/30 QQ) 共通テスト対応 UPD END
	private int            intMaxClassSr   = 5;                    //MAXクラス数
	// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END

	private int intDataStartRow = 8;					//データセット開始行

	// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
	//// 2019/09/30 QQ) 共通テスト対応 UPD START
	//private int intDataStartCol = 13;					//データセット開始列
	//// 2019/09/30 QQ) 共通テスト対応 UPD END
	private int intDataStartCol = 10;                                      //データセット開始列
        // 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END

	final private String masterfile0 = "S44_04" ;
	final private String masterfile1 = "S44_04" ;
	private String masterfile = "" ;

	final private String strCenterResearch = "38";

	/*
	 * 	Excel編集メイン
	 * 		S44Item S44Item: データクラス
	 * 		String 	masterfile: マスタExcelファイル名（フルパス）
	 * 		String 	outfile: 出力Excelファイル名（フルパス）
	 * 		int		intSaveFlg: 1:保存 2:印刷 3:保存／印刷
	 * 		String	UserID：ユーザーID
	 * 		戻り値: 0:正常終了、0以外:異常終了
	 */
	public int s44_04EditExcel(S44Item s44Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		//テンプレートの決定
		if (s44Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		/** セット処理 **/
		boolean bolBookCngFlg = true;			//改ファイルフラグ
		boolean bolSheetCngFlg = true;			//改シートフラグ

		FileInputStream	fin = null;
		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFCell	workCell		= null;
		HSSFRow		workRow			= null;
		HSSFWorkbook	tmpworkbook = null;

		int intMaxSheetIndex = 0;					//シート作成数カウンタ

		int intRow = 0;
		int intCol = 0;

		String strGenekiKbn = "";
		String strDaigakuCode = "";
		String strGkbCode = "";
		String strNtiCode = "";
		String strHyoukaKbn = "";
		String strNendo = "";

		//2004.09.27Add
		String strKokushiKbn = "";
		String strStKokushiKbn = "";
		int intChangeSheetRow = 0;
		//2004.09.27AddEnd

		int intLoopCnt = 0;
		int intNinzuListCnt = 0;
		int intSheetCngCount = 0;
		int intBookCngCount = 1;

		boolean bolDispHyoukaFlg = false;
		boolean bolDispGkbFlg = false;
		boolean bolDispNtiFlg = false;
		boolean bolDispDaigakuFlg = false;
		String strHyouka = "";

		String strDispTarget = "";

		ArrayList WorkBookList = new ArrayList();
		ArrayList WorkSheetList = new ArrayList();

		int intSheetCnt=0;

		int intBookIndex = -1;
		int intSheetIndex = -1;

		int intSheetMei = -1;

		int intSetRow = intDataStartRow;

		boolean bolClassDispFlg=true;

		KNLog log = KNLog.getInstance(null,null,null);
		ArrayList HyoukaTitleList = new ArrayList();

		log.Ep("S44_04","S44_04帳票作成開始","");

		// 基本ファイルを読込む
		S44ListBean s44ListBean = new S44ListBean();

		try{
			/** S44Item編集 **/
			s44Item  = editS44( s44Item );

			if(cm.toString(s44Item.getStrMshCd()).equals(cm.toString(strCenterResearch))){
				HyoukaTitleList.add(0,"◎");
				HyoukaTitleList.add(1,"○");
				HyoukaTitleList.add(2,"△");
			}else{
				HyoukaTitleList.add(0,"A");
				HyoukaTitleList.add(1,"B");
				HyoukaTitleList.add(2,"C");
			}

			// 基本ファイルを読込む
			ArrayList s44List = s44Item.getS44List();
			ListIterator itr = s44List.listIterator();

//			if( itr.hasNext() == false ){
//				return errfdata;
//			}

			/** 大学・学部・日程・評価 **/
			// S44ListBean
			while( itr.hasNext() ){
				s44ListBean = (S44ListBean)itr.next();

				// 大学名
				bolDispDaigakuFlg = false;
				if( !cm.toString(strDaigakuCode).equals(cm.toString(s44ListBean.getStrDaigakuCd()))){
					bolDispDaigakuFlg = true;
					strHyoukaKbn = "";
					strNtiCode = "";
					strGkbCode = "";
					//大学名表示
					strDaigakuCode = s44ListBean.getStrDaigakuCd();
				}

				// 学部
				bolDispGkbFlg = false;
				if( !cm.toString(strGkbCode).equals(cm.toString(s44ListBean.getStrGakubuCd())) ){
					bolDispGkbFlg = true;
					strHyoukaKbn = "";
					strNtiCode = "";
					strGkbCode = s44ListBean.getStrGakubuCd();
				}

				// 日程
				bolDispNtiFlg = false;
				if( !cm.toString(strNtiCode).equals(cm.toString(s44ListBean.getStrNtiCd())) ){
					bolDispNtiFlg = true;
					strHyoukaKbn = "";
					strNtiCode = s44ListBean.getStrNtiCd();
				}

				// 評価区分
				bolDispHyoukaFlg = false;
				if( !cm.toString(strHyoukaKbn).equals(cm.toString(s44ListBean.getStrHyouka())) ){
					bolDispHyoukaFlg = true;
					//評価区分表示
					strHyoukaKbn = s44ListBean.getStrHyouka();
					strHyouka = "";
					switch( Integer.parseInt(strHyoukaKbn) ){
						case 1:
							// 2019/09/30 QQ) 共通テスト対応 UPD START
							//strHyouka = "センター";
							strHyouka = "共テ";
							// 2019/09/30 QQ) 共通テスト対応 UPD END
							break;
						case 2:
							strHyouka = "2次・私大";
							break;
						case 3:
							strHyouka = "総合";
							break;
					}
				}

				// 表示対象
				if( !cm.toString(strGenekiKbn).equals(cm.toString(s44ListBean.getStrGenKouKbn())) ){
					strGenekiKbn = s44ListBean.getStrGenKouKbn();
					if( cm.toString(strGenekiKbn).equals("0") ){
						strDispTarget = "全体";
					}
					else if( cm.toString(strGenekiKbn).equals("1") ){
						strDispTarget = "現役生";
					}
					else if( cm.toString(strGenekiKbn).equals("2") ){
						strDispTarget = "高卒生";
					}

					if( bolSheetCngFlg == false ){
						bolSheetCngFlg = true;
					}
				}

				//2004.9.27 Add
				int intDaigakuCd = (int)Integer.parseInt(s44ListBean.getStrDaigakuCd());
				if( intDaigakuCd >= 1001 && intDaigakuCd <= 1899 ){
					strKokushiKbn = "国公立大";
				}
				if( intDaigakuCd == 1900 || (intDaigakuCd >= 2001 && intDaigakuCd <= 2999) ){
					strKokushiKbn = "私立大";
				}
				if( intDaigakuCd >= 3001 && intDaigakuCd <= 4999 ){
					strKokushiKbn = "短大";
				}
				if( intDaigakuCd >= 5001 && intDaigakuCd <= 7999 ){
					strKokushiKbn = "その他";
				}

				if( (!cm.toString(strKokushiKbn).equals(cm.toString(strStKokushiKbn))) ){
					bolSheetCngFlg = true;
					strStKokushiKbn = strKokushiKbn;
				}
				//2004.9.27 Add End

				//クラス別評価人数リスト読み込み クラス名をExcelへセット
				ArrayList s44GakkoList = s44ListBean.getS44GakkoList();
				ListIterator itr2 = s44GakkoList.listIterator();

				// Listの取得
				S44GakkoListBean s44GakkoListBean = new S44GakkoListBean();
				S44GakkoListBean s44GakkoListBeanHomeData = new S44GakkoListBean();

				S44GakkoListBean HomeDataBean = new S44GakkoListBean();

				//クラス数取得
				int intClassSr = s44GakkoList.size();

				int intSheetSr = 0;
				if( (intClassSr - 1) % intMaxClassSr > 0 ){
					intSheetSr = (int)Math.ceil((intClassSr - 1) / intMaxClassSr) + 1;
				}else{
					intSheetSr = (int)Math.ceil((intClassSr - 1) / intMaxClassSr);
				}

				int intLoopCnt2 = 0;
				int intNendoCount = 0;
				intChangeSheetRow = 0;

				boolean bolHomeDataDispFlg=false;
				/** 学校名・学年・クラス **/
				// S44GakkoListBean
				while( itr2.hasNext() ){
					String strClassmei = "";
					s44GakkoListBean = (S44GakkoListBean)itr2.next();

					// Listの取得
					ArrayList s44HyoukaNinzuList = s44GakkoListBean.getS44HyoukaNinzuList();
					S44HyoukaNinzuListBean s44HyoukaNinzuListBean = new S44HyoukaNinzuListBean();
					ListIterator itr3 = s44HyoukaNinzuList.listIterator();

					// 自校情報表示判定
					if( intLoopCnt2 == 0 ){
						bolHomeDataDispFlg = true;
						HomeDataBean = s44GakkoListBean;
						s44GakkoListBean = (S44GakkoListBean)itr2.next();
						intNendoCount = s44HyoukaNinzuList.size();
						intChangeSheetRow = (Math.abs( intMaxRow / intNendoCount ) * intNendoCount) + intDataStartRow;
						intLoopCnt2++;

						s44HyoukaNinzuList = s44GakkoListBean.getS44HyoukaNinzuList();
						s44HyoukaNinzuListBean = new S44HyoukaNinzuListBean();
						itr3 = s44HyoukaNinzuList.listIterator();
					}

					// 始点情報（Col）
					if( intLoopCnt2 % intMaxClassSr == 0 ){
					        // 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
						//// 2019/09/30 QQ) 共通テスト対応 UPD START
						//intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 8 );
						//// 2019/09/30 QQ) 共通テスト対応 UPD END
						intCol = intDataStartCol + ( ( intMaxClassSr - 1 ) * 5 );
						// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END
					}else{
					        // 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
						//// 2019/09/30 QQ) 共通テスト対応 UPD START
						//intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 8 );
						//// 2019/09/30 QQ) 共通テスト対応 UPD END
					        intCol = intDataStartCol + ((( intLoopCnt2 % intMaxClassSr ) - 1 ) * 5 );
					        // 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END
					}

//					intCol = intDataStartCol + ( ( intMaxClassSr - ( intLoopCnt2 % intMaxClassSr ) ) * 5 );

					/** 年度・志望者数 **/
					// S44HyoukaNinzuListBean
					int intHyoukaNinzuListIndex = 0;
					while( itr3.hasNext() ){
						s44HyoukaNinzuListBean = (S44HyoukaNinzuListBean)itr3.next();

						// 規定行数を表示したら線を引く
						if( (intSetRow  >= intChangeSheetRow) ||((bolSheetCngFlg == true)&&(intMaxSheetIndex != 0)) ){
							bolSheetCngFlg = true;
							for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//								int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
								int intBIndex = (intSindex + 1) / intMaxSheetSr;
								if((intSindex + 1)%intMaxSheetSr!=0){
									intBIndex++;
								}
								workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
								workSheet = (HSSFSheet)WorkSheetList.get(intSindex);

								// 罫線出力(セルの上部に太線を引く）

								// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
								//// 2019/09/30 QQ) 共通テスト対応 UPD START
								//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//									intSetRow, intSetRow, 0, 36, false, true, false, false );
								//// 2019/09/30 QQ) 共通テスト対応 UPD END
								cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                                                        intSetRow, intSetRow, 0, 34, false, true, false, false );
								// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END

								if( workSheet != null ){
									int intRemoveStartRow = intChangeSheetRow;
									if( intMaxRow + intDataStartRow > intRemoveStartRow ){
										for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
											//行削除処理
											workSheet.removeRow(workSheet.getRow(index));
										}
										//罫線出力（セルの上部に太線を出力）

										// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
										//// 2019/09/30 QQ) 共通テスト対応 UPD START
										//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
										//				intRemoveStartRow, intRemoveStartRow, 0, 36, false, true, false, false);
										//// 2019/09/30 QQ) 共通テスト対応 UPD END
										cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
                                                                                        intRemoveStartRow, intRemoveStartRow, 0, 34, false, true, false, false);
										// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END
									}
								}

							}

							////////////////////保存処理//////////////////
							if( ((HSSFWorkbook)WorkBookList.get(0)).getNumberOfSheets() > intMaxSheetSr ){
								tmpworkbook = (HSSFWorkbook)WorkBookList.get(0);

								boolean bolRet = false;
								// Excelファイル保存
								bolRet = cm.bolFileSave(intSaveFlg, outfilelist, tmpworkbook, UserID, intBookCngCount, masterfile, intMaxSheetSr);

								if( bolRet == false ){
									log.Err("0"+bolRet+"S44_03","帳票作成エラー","");
									return errfwrite;
								}
								intBookCngCount++;

								// 削除
								WorkBookList.remove(0);
								for(int a=0; a<intMaxSheetSr;a++){
									WorkSheetList.remove(0);
								}
								tmpworkbook=null;
								intBookIndex--;
								intSheetIndex -= intMaxSheetSr;
							}
						}

						// 改シート判定
						if( bolSheetCngFlg == true ){

							bolDispHyoukaFlg = true;
							bolDispDaigakuFlg = true;
							bolDispGkbFlg = true;
							bolDispNtiFlg = true;

							intSetRow = intDataStartRow;
							bolClassDispFlg = true;
							bolHomeDataDispFlg = true;

							for( int intIndex = 0; intIndex < intSheetSr; intIndex ++ ){

								if( ( intMaxSheetIndex >= intMaxSheetSr ) || ( bolBookCngFlg == true ) ){
									workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
									if( workbook.equals(null) ){
										log.Err("02S44_03","帳票作成エラー","");
										return errfread;
									}
									WorkBookList.add(workbook);
									intBookIndex++;

									bolBookCngFlg = false;
									intMaxSheetIndex = 0;
									workbook = (HSSFWorkbook)WorkBookList.get(intBookIndex);
								}

								//シート作成
								// シートテンプレートのコピー
								intSheetMei = intMaxSheetIndex + 1;
								workSheet = workbook.cloneSheet(0);
								WorkSheetList.add(workSheet);
								intSheetIndex++;
								workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex);

								// ヘッダ右側に帳票作成日時を表示する
								cm.setHeader(workbook, workSheet);

								// セキュリティスタンプセット

								// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
								////2019/10/03 QQ)Oosaki 共通テスト対応 UPD START
								////// 2019/09/30 QQ) 共通テスト対応 UPD START
								////String secuFlg = cm.setSecurity( workbook, workSheet, s44Item.getIntSecuFlg(), 31, 36 );
								////workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
								////// 2019/09/30 QQ) 共通テスト対応 UPD END
								//String secuFlg = cm.setSecurity( workbook, workSheet, s44Item.getIntSecuFlg(), 34, 36 );
								//workCell = cm.setCell( workSheet, workRow, workCell, 0, 34 );
								////2019/10/03 QQ)Oosaki 共通テスト対応 UPD END
                                                                String secuFlg = cm.setSecurity( workbook, workSheet, s44Item.getIntSecuFlg(), 32, 34 );
                                                                workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
                                                                // 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END

								workCell.setCellValue(secuFlg);

								// 学校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
								workCell.setCellValue( "学校名　：" + s44Item.getStrGakkomei() );

								// 模試月取得
								String moshi =cm.setTaisyouMoshi( s44Item.getStrMshDate() );

								// 対象模試セット
								workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
								workCell.setCellValue( "対象模試：" + s44Item.getStrMshmei() + moshi);

								//表示対象
								workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
								workCell.setCellValue( "表示対象：" + strDispTarget );

								//評価別人数 見出し行
								for( int i = 0; i <= intMaxClassSr; i++ ){
								        // 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
									//// 2019/09/30 QQ) 共通テスト対応 UPD START
									//int intTitleSetCol = (i * 8) + ( intDataStartCol - 6 );
									//// 2019/09/30 QQ) 共通テスト対応 UPD END
								        int intTitleSetCol = (i * 5) + ( intDataStartCol - 3 );
								        // 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END

									//出願予定・第1志望

									//2019/10/03 QQ)Oosaki 共通テスト対応 DEL START
									//// 2019/09/30 QQ) 共通テスト対応 ADD START
									//
									////if (strKokushiKbn == "国公立大") {
									////	workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
									////	workCell.setCellValue( "出願予定" );
									////} else {
									////	workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
									////	workCell.setCellValue( "第１志望" );
									////}
									//
									//workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol-1 );
									//workCell.setCellValue( "第１志望" );
									//// 2019/09/30 QQ) 共通テスト対応 ADD END
									//2019/10/03 QQ)Oosaki 共通テスト対応 DEL END

									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol );
									workCell.setCellValue( (String)HyoukaTitleList.get(0) );

									// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
									// 2019/09/30 QQ) 共通テスト対応 UPD START
									//workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+2 );
									// 2019/09/30 QQ) 共通テスト対応 UPD END
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+1 );
                                                                        workCell.setCellValue( (String)HyoukaTitleList.get(1) );
                                                                        // 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END

									workCell.setCellValue( (String)HyoukaTitleList.get(1) );

									// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
									//// 2019/09/30 QQ) 共通テスト対応 UPD START
									////workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+4 );
									//// 2019/09/30 QQ) 共通テスト対応 UPD END
									workCell = cm.setCell( workSheet, workRow, workCell, 7, intTitleSetCol+2 );
									// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END

									workCell.setCellValue( (String)HyoukaTitleList.get(2) );
								}

								intMaxSheetIndex++;		//シート数カウンタインクリメント
								bolSheetCngFlg = false;

								intLoopCnt = 0;
							}	//ForEnd

						}	//if( bolSheetCngFlg == true )

						intSheetCnt = (int)Math.ceil( (double)intLoopCnt2 / (double)intMaxClassSr );
						workSheet = (HSSFSheet)WorkSheetList.get(intSheetIndex -  (intSheetSr - intSheetCnt)  );

						int intSheetNo = intSheetIndex -  (intSheetSr - intSheetCnt);
						int intBIndex = (int)Math.ceil((double)(intSheetNo + 1) / (double)intMaxSheetSr );
						workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);

						if( intCol == intDataStartCol ){

							//データセット
							//自校データ・大学名ほか表示
							//自校学校名
							workCell = cm.setCell( workSheet, workRow, workCell, 5, 5 );
							workCell.setCellValue(HomeDataBean.getStrGakkomei());
							//自校データ
							ListIterator itrHome = HomeDataBean.getS44HyoukaNinzuList().listIterator();
							int intLoopIndex = 0;
							while( itrHome.hasNext() ){
								S44HyoukaNinzuListBean HomeHyoukaNinzuListBean = (S44HyoukaNinzuListBean) itrHome.next();
								boolean ret = setData( workSheet, HomeHyoukaNinzuListBean, intSetRow + intLoopIndex , 5 );
								if( ret == false ){
									log.Err("0"+ret+"S44_04","帳票作成エラー","");
									return errfdata;
								}
								intLoopIndex++;
							}

							if( bolDispHyoukaFlg == true ){
								//罫線出力(セルの上部（評価区分−年度までの列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
															intSetRow, intSetRow, 3, 3, false, true, false, false);
								//評価区分表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 3);
								workCell.setCellValue( strHyouka );

							}

							if( bolDispNtiFlg == true ){
								//罫線出力(セルの上部（日程の列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
															intSetRow, intSetRow, 2, 3, false, true, false, false);
								//日程表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 2);
								workCell.setCellValue( s44ListBean.getStrNittei() );

							}

							if( bolDispGkbFlg == true ){
								//罫線出力(セルの上部（学部の列に標準の太さの線を引く）
								cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_THIN, HSSFColor.BLACK.index,
															intSetRow, intSetRow, 1, 3, false, true, false, false);
								//学部表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 1);
								workCell.setCellValue( s44ListBean.getStrGakubuMei() );

							}

							if( bolDispDaigakuFlg == true ){
								//罫線出力(セルの上部に太線を引く）

							        // 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
								//// 2019/09/30 QQ) 共通テスト対応 UPD START
								//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
								//									intSetRow, intSetRow, 0, 36, false, true, false, false );
								//// 2019/09/30 QQ) 共通テスト対応 UPD END
							        cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
							                intSetRow, intSetRow, 0, 34, false, true, false, false );
							        // 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END

								//大学名表示
								workCell = cm.setCell( workSheet, workRow, workCell, intSetRow, 0 );
								workCell.setCellValue(s44ListBean.getStrDaigakuMei());
							}

						}


						bolHomeDataDispFlg = false;

						boolean ret = setData( workSheet, s44HyoukaNinzuListBean, intSetRow + intHyoukaNinzuListIndex, intCol);
						if( ret != true){
							log.Err("0"+ret+"S44_04","帳票作成エラー","");
							return errfdata;
						}
						else{
							intHyoukaNinzuListIndex++;
							intLoopCnt++;
						}

						//

					}	//WhileEnd( itr3 )


					//クラス名
					if( bolClassDispFlg == true ){
						String strGakko = s44GakkoListBean.getStrGakkomei();
						workCell = cm.setCell( workSheet, workRow, workCell, 5, intCol );
						workCell.setCellValue(strGakko);
					}
					intLoopCnt2++;
				}	//WhileEnd( itr2 )

				intSetRow += intNendoCount;
				bolClassDispFlg = false;

			}	//WhileEnd( itr )

			for( int intSindex = intSheetIndex - (intSheetCnt-1); intSindex <= intSheetIndex; intSindex++ ){
//				int intBIndex = (int)Math.ceil((double)(intSindex + 1) / (double)intMaxSheetSr );
				int intBIndex = (intSindex + 1) / intMaxSheetSr;
				if((intSindex + 1)%intMaxSheetSr!=0){
					intBIndex++;
				}
				workbook = (HSSFWorkbook)WorkBookList.get(intBIndex-1);
				workSheet = (HSSFSheet)WorkSheetList.get(intSindex);

				//罫線出力(セルの上部に太線を引く）

				// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
				//// 2019/09/30 QQ) 共通テスト対応 UPD START
				//cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
				//									intSetRow, intSetRow, 0, 36, false, true, false, false );
				//// 2019/09/30 QQ) 共通テスト対応 UPD END
				cm.setLine( workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK.index,
                                        intSetRow, intSetRow, 0, 34, false, true, false, false );
				// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END

				if( workSheet != null ){
					int intRemoveStartRow = intChangeSheetRow;
					if( intMaxRow + intDataStartRow > intRemoveStartRow ){
						for( int index=intRemoveStartRow; index < intMaxRow + intDataStartRow; index++ ){
							//行削除処理
							workSheet.removeRow(workSheet.getRow(index));
						}
						//罫線出力（セルの上部に太線を出力）

						// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
						//// 2019/09/30 QQ) 共通テスト対応 UPD START
						//cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
						//				intRemoveStartRow, intRemoveStartRow, 0, 36, false, true, false, false);
						//// 2019/09/30 QQ) 共通テスト対応 UPD END
						cm.setLine(workbook, workSheet, HSSFCellStyle.BORDER_MEDIUM, HSSFColor.BLACK .index,
                                                        intRemoveStartRow, intRemoveStartRow, 0, 34, false, true, false, false);
						// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END
					}
				}

			}

			//ファイル保存
			for( int intBookSr = 0; intBookSr < WorkBookList.size(); intBookSr++ ){
				workbook = (HSSFWorkbook)WorkBookList.get(intBookSr);

//				// 元のテンプレートの削除
//				workbook.removeSheetAt(workbook.getSheetIndex(mastersheet));

				boolean bolRet = false;
				// Excelファイル保存
				if( (intBookCngCount != 1)||((intBookCngCount == 1)&&(WorkBookList.size()>1)) ){
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, intBookCngCount, masterfile, intMaxSheetIndex);
				}else{
					bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
				}

				if( bolRet == false ){
					log.Err("0"+bolRet+"S44_04","帳票作成エラー","");
					return errfwrite;
				}
				intBookCngCount++;
			}

			//データ無し
			if ( WorkBookList.size() == 0 ){
				workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
				workSheet = workbook.cloneSheet(0);
				intMaxSheetIndex = 1;

				// ヘッダ右側に帳票作成日時を表示する
				cm.setHeader(workbook, workSheet);

				// セキュリティスタンプセット

				// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
				////2019/10/03 QQ)Oosaki 共通テスト対応 UPD START
				////// 2019/09/30 QQ) 共通テスト対応 UPD START
				////String secuFlg = cm.setSecurity( workbook, workSheet, s44Item.getIntSecuFlg(), 31, 36 );
				////workCell = cm.setCell( workSheet, workRow, workCell, 0, 31 );
				////// 2019/09/30 QQ) 共通テスト対応 UPD END
				//String secuFlg = cm.setSecurity( workbook, workSheet, s44Item.getIntSecuFlg(), 34, 36 );
				//workCell = cm.setCell( workSheet, workRow, workCell, 0, 34 );
				////2019/10/03 QQ)Oosaki 共通テスト対応 UPD END
	                        String secuFlg = cm.setSecurity( workbook, workSheet, s44Item.getIntSecuFlg(), 32, 34 );
                                workCell = cm.setCell( workSheet, workRow, workCell, 0, 32 );
                                // 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END

				workCell.setCellValue(secuFlg);

				// 学校名セット
				workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
				workCell.setCellValue( "学校名　：" + cm.toString(s44Item.getStrGakkomei()) );

				// 模試月取得
				String moshi =cm.setTaisyouMoshi( s44Item.getStrMshDate() );

				// 対象模試セット
				workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
				workCell.setCellValue( "対象模試：" + s44Item.getStrMshmei() + moshi);

				//表示対象
				workCell = cm.setCell( workSheet, workRow, workCell, 4, 0 );
				workCell.setCellValue( "表示対象：" );

				boolean bolRet = false;
				bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);
			}

		}
		catch( Exception e ){
			e.printStackTrace();
			log.Err("99S44","帳票作成エラー",e.toString());
			return errfdata;
		}

		log.Ep("S44_04","S44_04帳票作成終了","");

		return noerror;
	}

	/**
	 * S44Item加算・編集
	 * @param s44Item
	 * @return
	 */
	private S44Item editS44( S44Item s44Item ) throws Exception{
		try{
			S44Item		s44ItemEdt	= new S44Item();
			String		daigakuCd	= "";
			String		gakubuCd	= "";
			String		gakkaCd		= "";
			String 		genekiKbn 	= "";

			// 学部計用
			boolean	centerGKBFlg	= false;
			boolean	secondGKBFlg	= false;
			boolean	totalGKBFlg	= false;

			ArrayList s44GakkoListGKBAdd		= new ArrayList();
			ArrayList s44GakkoListGKBAddCnt	= new ArrayList();
			ArrayList s44GakkoListGKBAddSnd	= new ArrayList();
			ArrayList s44GakkoListGKBAddTtl	= new ArrayList();
			ArrayList s44NinzuListGKBAdd		= new ArrayList();

			S44ListBean s44ListBeanGKBAddCnt = new S44ListBean();
			S44ListBean s44ListBeanGKBAddSnd = new S44ListBean();
			S44ListBean s44ListBeanGKBAddTtl = new S44ListBean();

			// 日程計用
			boolean	zenCenterNTIFlg	= false;
			boolean	zenSecondNTIFlg	= false;
			boolean	zenTotalNTIFlg	= false;
			boolean	chuCenterNTIFlg	= false;
			boolean	chuSecondNTIFlg	= false;
			boolean	chuTotalNTIFlg	= false;
			boolean	kokCenterNTIFlg	= false;
			boolean	kokSecondNTIFlg	= false;
			boolean	kokTotalNTIFlg	= false;
			boolean	ipnCenterNTIFlg	= false;
			boolean	ipnSecondNTIFlg	= false;
			boolean	ipnTotalNTIFlg	= false;
			boolean	cenCenterNTIFlg	= false;
			boolean	cenSecondNTIFlg	= false;
			boolean	cenTotalNTIFlg	= false;

			ArrayList s44GakkoListNTIAdd		= new ArrayList();
			ArrayList s44GakkoListNTIAddZenCnt		= new ArrayList();
			ArrayList s44GakkoListNTIAddZenSnd		= new ArrayList();
			ArrayList s44GakkoListNTIAddZenTtl		= new ArrayList();
			ArrayList s44GakkoListNTIAddChuCnt		= new ArrayList();
			ArrayList s44GakkoListNTIAddChuSnd		= new ArrayList();
			ArrayList s44GakkoListNTIAddChuTtl		= new ArrayList();
			ArrayList s44GakkoListNTIAddKokCnt		= new ArrayList();
			ArrayList s44GakkoListNTIAddKokSnd		= new ArrayList();
			ArrayList s44GakkoListNTIAddKokTtl		= new ArrayList();
			ArrayList s44GakkoListNTIAddIpnCnt		= new ArrayList();
			ArrayList s44GakkoListNTIAddIpnSnd		= new ArrayList();
			ArrayList s44GakkoListNTIAddIpnTtl		= new ArrayList();
			ArrayList s44GakkoListNTIAddCenCnt		= new ArrayList();
			ArrayList s44GakkoListNTIAddCenSnd		= new ArrayList();
			ArrayList s44GakkoListNTIAddCenTtl		= new ArrayList();
			ArrayList s44NinzuListNTIAdd		= new ArrayList();

			S44ListBean s44ListBeanNTIAddZenCnt = new S44ListBean();
			S44ListBean s44ListBeanNTIAddZenSnd = new S44ListBean();
			S44ListBean s44ListBeanNTIAddZenTtl = new S44ListBean();
			S44ListBean s44ListBeanNTIAddChuCnt = new S44ListBean();
			S44ListBean s44ListBeanNTIAddChuSnd = new S44ListBean();
			S44ListBean s44ListBeanNTIAddChuTtl = new S44ListBean();
			S44ListBean s44ListBeanNTIAddKokCnt = new S44ListBean();
			S44ListBean s44ListBeanNTIAddKokSnd = new S44ListBean();
			S44ListBean s44ListBeanNTIAddKokTtl = new S44ListBean();
			S44ListBean s44ListBeanNTIAddIpnCnt = new S44ListBean();
			S44ListBean s44ListBeanNTIAddIpnSnd = new S44ListBean();
			S44ListBean s44ListBeanNTIAddIpnTtl = new S44ListBean();
			S44ListBean s44ListBeanNTIAddCenCnt = new S44ListBean();
			S44ListBean s44ListBeanNTIAddCenSnd = new S44ListBean();
			S44ListBean s44ListBeanNTIAddCenTtl = new S44ListBean();

			// 大学計用
			boolean	centerDGKFlg	= false;
			boolean	secondDGKFlg	= false;
			boolean	totalDGKFlg	= false;

			ArrayList s44GakkoListDGKAdd		= new ArrayList();
			ArrayList s44GakkoListDGKAddCnt		= new ArrayList();
			ArrayList s44GakkoListDGKAddSnd		= new ArrayList();
			ArrayList s44GakkoListDGKAddTtl		= new ArrayList();
			ArrayList s44NinzuListDGKAdd		= new ArrayList();

			S44ListBean s44ListBeanDGKAddCnt = new S44ListBean();
			S44ListBean s44ListBeanDGKAddSnd = new S44ListBean();
			S44ListBean s44ListBeanDGKAddTtl = new S44ListBean();

			s44ItemEdt.setStrGakkomei( s44Item.getStrGakkomei() );
			s44ItemEdt.setStrMshmei( s44Item.getStrMshmei() );
			s44ItemEdt.setStrMshDate( s44Item.getStrMshDate() );
			s44ItemEdt.setStrMshCd( s44Item.getStrMshCd() );
			s44ItemEdt.setIntSecuFlg( s44Item.getIntSecuFlg() );
			s44ItemEdt.setIntDaiTotalFlg( s44Item.getIntDaiTotalFlg() );


			/** 大学・学部・学科・日程・評価 **/
			ArrayList s44List		= s44Item.getS44List();
			ArrayList s44ListEdt	= new ArrayList();
			ListIterator itr = s44List.listIterator();
			while( itr.hasNext() ){
				S44ListBean s44ListBean		= (S44ListBean) itr.next();
				S44ListBean s44ListBeanEdt	= new S44ListBean();

				//-----------------------------------------------------------------------------------------
				//大学計・学部計を出力
				//-----------------------------------------------------------------------------------------
				if( !cm.toString(gakubuCd).equals( cm.toString(s44ListBean.getStrGakubuCd()) ) ||
						!cm.toString(daigakuCd).equals( cm.toString(s44ListBean.getStrDaigakuCd()))
						|| !cm.toString(genekiKbn).equals(cm.toString(s44ListBean.getStrGenKouKbn()))) {

					gakubuCd = s44ListBean.getStrGakubuCd();

					if(centerGKBFlg == true){
						s44ListBeanGKBAddCnt.setS44GakkoList( s44GakkoListGKBAddCnt );
						s44ListEdt.add( s44ListBeanGKBAddCnt );
						s44GakkoListGKBAddCnt = new ArrayList();
						centerGKBFlg = false;
					}
					if(secondGKBFlg == true){
						s44ListBeanGKBAddSnd.setS44GakkoList( s44GakkoListGKBAddSnd );
						s44ListEdt.add( s44ListBeanGKBAddSnd );
						s44GakkoListGKBAddSnd = new ArrayList();
						secondGKBFlg = false;
					}
					if(totalGKBFlg == true){
						s44ListBeanGKBAddTtl.setS44GakkoList( s44GakkoListGKBAddTtl );
						s44ListEdt.add( s44ListBeanGKBAddTtl );
						s44GakkoListGKBAddTtl = new ArrayList();
						totalGKBFlg = false;
					}
					s44ListBeanGKBAddCnt = new S44ListBean();
					s44ListBeanGKBAddSnd = new S44ListBean();
					s44ListBeanGKBAddTtl = new S44ListBean();
					s44NinzuListGKBAdd = new ArrayList();
				}
				// 大学コードが変わる→日程計・大学計の保存
//////////////////
//				if( !cm.toString(daigakuCd).equals( cm.toString(s44ListBean.getStrDaigakuCd()) ) ){
//////////////////
				if( (!cm.toString(daigakuCd).equals( cm.toString(s44ListBean.getStrDaigakuCd())))
					||( !cm.toString(genekiKbn).equals(cm.toString(s44ListBean.getStrGenKouKbn()))) ){
					genekiKbn = s44ListBean.getStrGenKouKbn();

					daigakuCd = s44ListBean.getStrDaigakuCd();

					// 日程計
					if(zenCenterNTIFlg == true){
						s44ListBeanNTIAddZenCnt.setS44GakkoList( s44GakkoListNTIAddZenCnt );
						s44ListEdt.add( s44ListBeanNTIAddZenCnt );
						s44GakkoListNTIAddZenCnt = new ArrayList();
						zenCenterNTIFlg = false;
					}
					if(zenSecondNTIFlg == true){
						s44ListBeanNTIAddZenSnd.setS44GakkoList( s44GakkoListNTIAddZenSnd );
						s44ListEdt.add( s44ListBeanNTIAddZenSnd );
						s44GakkoListNTIAddZenSnd = new ArrayList();
						zenSecondNTIFlg = false;
					}
					if(zenTotalNTIFlg == true){
						s44ListBeanNTIAddZenTtl.setS44GakkoList( s44GakkoListNTIAddZenTtl );
						s44ListEdt.add( s44ListBeanNTIAddZenTtl );
						s44GakkoListNTIAddZenTtl = new ArrayList();
						zenTotalNTIFlg = false;
					}
					if(chuCenterNTIFlg == true){
						s44ListBeanNTIAddChuCnt.setS44GakkoList( s44GakkoListNTIAddChuCnt );
						s44ListEdt.add( s44ListBeanNTIAddChuCnt );
						s44GakkoListNTIAddChuCnt = new ArrayList();
						chuCenterNTIFlg = false;
					}
					if(chuSecondNTIFlg == true){
						s44ListBeanNTIAddChuSnd.setS44GakkoList( s44GakkoListNTIAddChuSnd );
						s44ListEdt.add( s44ListBeanNTIAddChuSnd );
						s44GakkoListNTIAddChuSnd = new ArrayList();
						chuSecondNTIFlg = false;
					}
					if(chuTotalNTIFlg == true){
						s44ListBeanNTIAddChuTtl.setS44GakkoList( s44GakkoListNTIAddChuTtl );
						s44ListEdt.add( s44ListBeanNTIAddChuTtl );
						s44GakkoListNTIAddChuTtl = new ArrayList();
						chuTotalNTIFlg = false;
					}
					if(kokCenterNTIFlg == true){
						s44ListBeanNTIAddKokCnt.setS44GakkoList( s44GakkoListNTIAddKokCnt );
						s44ListEdt.add( s44ListBeanNTIAddKokCnt );
						s44GakkoListNTIAddKokCnt = new ArrayList();
						kokCenterNTIFlg = false;
					}
					if(kokSecondNTIFlg == true){
						s44ListBeanNTIAddKokSnd.setS44GakkoList( s44GakkoListNTIAddKokSnd );
						s44ListEdt.add( s44ListBeanNTIAddKokSnd );
						s44GakkoListNTIAddKokSnd = new ArrayList();
						kokSecondNTIFlg = false;
					}
					if(kokTotalNTIFlg == true){
						s44ListBeanNTIAddKokTtl.setS44GakkoList( s44GakkoListNTIAddKokTtl );
						s44ListEdt.add( s44ListBeanNTIAddKokTtl );
						s44GakkoListNTIAddKokTtl = new ArrayList();
						kokTotalNTIFlg = false;
					}
					if(ipnCenterNTIFlg == true){
						s44ListBeanNTIAddIpnCnt.setS44GakkoList( s44GakkoListNTIAddIpnCnt );
						s44ListEdt.add( s44ListBeanNTIAddIpnCnt );
						s44GakkoListNTIAddIpnCnt = new ArrayList();
						ipnCenterNTIFlg = false;
					}
					if(ipnSecondNTIFlg == true){
						s44ListBeanNTIAddIpnSnd.setS44GakkoList( s44GakkoListNTIAddIpnSnd );
						s44ListEdt.add( s44ListBeanNTIAddIpnSnd );
						s44GakkoListNTIAddIpnSnd = new ArrayList();
						ipnSecondNTIFlg = false;
					}
					if(ipnTotalNTIFlg == true){
						s44ListBeanNTIAddIpnTtl.setS44GakkoList( s44GakkoListNTIAddIpnTtl );
						s44ListEdt.add( s44ListBeanNTIAddIpnTtl );
						s44GakkoListNTIAddIpnTtl = new ArrayList();
						ipnTotalNTIFlg = false;
					}
					if(cenCenterNTIFlg == true){
						s44ListBeanNTIAddCenCnt.setS44GakkoList( s44GakkoListNTIAddCenCnt );
						s44ListEdt.add( s44ListBeanNTIAddCenCnt );
						s44GakkoListNTIAddCenCnt = new ArrayList();
						cenCenterNTIFlg = false;
					}
					if(cenSecondNTIFlg == true){
						s44ListBeanNTIAddCenSnd.setS44GakkoList( s44GakkoListNTIAddCenSnd );
						s44ListEdt.add( s44ListBeanNTIAddCenSnd );
						s44GakkoListNTIAddCenSnd = new ArrayList();
						cenSecondNTIFlg = false;
					}
					if(cenTotalNTIFlg == true){
						s44ListBeanNTIAddCenTtl.setS44GakkoList( s44GakkoListNTIAddCenTtl );
						s44ListEdt.add( s44ListBeanNTIAddCenTtl );
						s44GakkoListNTIAddCenTtl = new ArrayList();
						cenTotalNTIFlg = false;
					}

					// 大学計
					if(centerDGKFlg == true){
						s44ListBeanDGKAddCnt.setS44GakkoList( s44GakkoListDGKAddCnt );
						s44ListEdt.add( s44ListBeanDGKAddCnt );
						s44GakkoListDGKAddCnt = new ArrayList();
						centerDGKFlg = false;
					}
					if(secondDGKFlg == true){
						s44ListBeanDGKAddSnd.setS44GakkoList( s44GakkoListDGKAddSnd );
						s44ListEdt.add( s44ListBeanDGKAddSnd );
						s44GakkoListDGKAddSnd = new ArrayList();
						secondDGKFlg = false;
					}
					if(totalDGKFlg == true){
						s44ListBeanDGKAddTtl.setS44GakkoList( s44GakkoListDGKAddTtl );
						s44ListEdt.add( s44ListBeanDGKAddTtl );
						s44GakkoListDGKAddTtl = new ArrayList();
						totalDGKFlg = false;
					}
					s44ListBeanNTIAddZenCnt	= new S44ListBean();
					s44ListBeanNTIAddZenSnd	= new S44ListBean();
					s44ListBeanNTIAddZenTtl	= new S44ListBean();
					s44ListBeanNTIAddChuCnt	= new S44ListBean();
					s44ListBeanNTIAddChuSnd	= new S44ListBean();
					s44ListBeanNTIAddChuTtl	= new S44ListBean();
					s44ListBeanNTIAddKokCnt	= new S44ListBean();
					s44ListBeanNTIAddKokSnd	= new S44ListBean();
					s44ListBeanNTIAddKokTtl	= new S44ListBean();
					s44ListBeanNTIAddIpnCnt	= new S44ListBean();
					s44ListBeanNTIAddIpnSnd	= new S44ListBean();
					s44ListBeanNTIAddIpnTtl	= new S44ListBean();
					s44ListBeanNTIAddCenCnt	= new S44ListBean();
					s44ListBeanNTIAddCenSnd	= new S44ListBean();
					s44ListBeanNTIAddCenTtl	= new S44ListBean();
					s44NinzuListNTIAdd = new ArrayList();

					s44ListBeanDGKAddCnt = new S44ListBean();
					s44ListBeanDGKAddSnd = new S44ListBean();
					s44ListBeanDGKAddTtl = new S44ListBean();
					s44NinzuListDGKAdd = new ArrayList();
				}

				// 基本部分の移し変え
				s44ListBeanEdt.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
				s44ListBeanEdt.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
				s44ListBeanEdt.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
				s44ListBeanEdt.setStrGakubuCd( s44ListBean.getStrGakubuCd() );
				s44ListBeanEdt.setStrGakubuMei( s44ListBean.getStrGakubuMei() );
				s44ListBeanEdt.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
				s44ListBeanEdt.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
				s44ListBeanEdt.setStrNtiCd( s44ListBean.getStrNtiCd() );
				s44ListBeanEdt.setStrNittei( s44ListBean.getStrNittei() );
				s44ListBeanEdt.setStrHyouka( s44ListBean.getStrHyouka() );

				// 学部計値の保存部
				switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
					case 1:
						s44ListBeanGKBAddCnt.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanGKBAddCnt.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanGKBAddCnt.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanGKBAddCnt.setStrGakubuCd( s44ListBean.getStrGakubuCd() );
						s44ListBeanGKBAddCnt.setStrGakubuMei( s44ListBean.getStrGakubuMei() );
						s44ListBeanGKBAddCnt.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanGKBAddCnt.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanGKBAddCnt.setStrNtiCd( "NTI-GKB" );
						s44ListBeanGKBAddCnt.setStrNittei( "学部計" );
						s44ListBeanGKBAddCnt.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListGKBAdd = deepCopyCl( s44GakkoListGKBAddCnt );
						centerGKBFlg = true;
						break;
					case 2:
						s44ListBeanGKBAddSnd.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanGKBAddSnd.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanGKBAddSnd.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanGKBAddSnd.setStrGakubuCd( s44ListBean.getStrGakubuCd() );
						s44ListBeanGKBAddSnd.setStrGakubuMei( s44ListBean.getStrGakubuMei() );
						s44ListBeanGKBAddSnd.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanGKBAddSnd.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanGKBAddSnd.setStrNtiCd( "NTI-GKB" );
						s44ListBeanGKBAddSnd.setStrNittei( "学部計" );
						s44ListBeanGKBAddSnd.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListGKBAdd = deepCopyCl( s44GakkoListGKBAddSnd );
						secondGKBFlg = true;
						break;
					case 3:
						s44ListBeanGKBAddTtl.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanGKBAddTtl.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanGKBAddTtl.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanGKBAddTtl.setStrGakubuCd( s44ListBean.getStrGakubuCd() );
						s44ListBeanGKBAddTtl.setStrGakubuMei( s44ListBean.getStrGakubuMei() );
						s44ListBeanGKBAddTtl.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanGKBAddTtl.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanGKBAddTtl.setStrNtiCd( "NTI-GKB" );
						s44ListBeanGKBAddTtl.setStrNittei( "学部計" );
						s44ListBeanGKBAddTtl.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListGKBAdd = deepCopyCl( s44GakkoListGKBAddTtl );
						totalGKBFlg = true;
						break;
				}

				// 大学計値の保存部
				switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
					case 1:
						s44ListBeanDGKAddCnt.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanDGKAddCnt.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanDGKAddCnt.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanDGKAddCnt.setStrGakubuCd( "NTI-DGK" );
						s44ListBeanDGKAddCnt.setStrGakubuMei( "大学計" );
						s44ListBeanDGKAddCnt.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanDGKAddCnt.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanDGKAddCnt.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanDGKAddCnt.setStrNittei( "" );
						s44ListBeanDGKAddCnt.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListDGKAdd = deepCopyCl( s44GakkoListDGKAddCnt );
						centerDGKFlg = true;
						break;
					case 2:
						s44ListBeanDGKAddSnd.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanDGKAddSnd.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanDGKAddSnd.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanDGKAddSnd.setStrGakubuCd( "NTI-DGK" );
						s44ListBeanDGKAddSnd.setStrGakubuMei( "大学計" );
						s44ListBeanDGKAddSnd.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanDGKAddSnd.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanDGKAddSnd.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanDGKAddSnd.setStrNittei( "" );
						s44ListBeanDGKAddSnd.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListDGKAdd = deepCopyCl( s44GakkoListDGKAddSnd );
						secondDGKFlg = true;
						break;
					case 3:
						s44ListBeanDGKAddTtl.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanDGKAddTtl.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanDGKAddTtl.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanDGKAddTtl.setStrGakubuCd( "NTI-DGK" );
						s44ListBeanDGKAddTtl.setStrGakubuMei( "大学計" );
						s44ListBeanDGKAddTtl.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanDGKAddTtl.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanDGKAddTtl.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanDGKAddTtl.setStrNittei( "" );
						s44ListBeanDGKAddTtl.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListDGKAdd = deepCopyCl( s44GakkoListDGKAddTtl );
						totalDGKFlg = true;
						break;
				}

				// 日程計値の保存部
				if( cm.toString(s44ListBean.getStrNtiCd()).equals("D") ){		 // 前期
					switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
						case 1:
						s44ListBeanNTIAddZenCnt.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddZenCnt.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddZenCnt.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddZenCnt.setStrGakubuCd( "NTI-ZEN" );
						s44ListBeanNTIAddZenCnt.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddZenCnt.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddZenCnt.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddZenCnt.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddZenCnt.setStrNittei( "" );
						s44ListBeanNTIAddZenCnt.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddZenCnt );
						zenCenterNTIFlg = true;
							break;
						case 2:
						s44ListBeanNTIAddZenSnd.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddZenSnd.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddZenSnd.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddZenSnd.setStrGakubuCd( "NTI-ZEN" );
						s44ListBeanNTIAddZenSnd.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddZenSnd.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddZenSnd.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddZenSnd.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddZenSnd.setStrNittei( "" );
						s44ListBeanNTIAddZenSnd.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddZenSnd );
						zenSecondNTIFlg = true;
							break;
						case 3:
						s44ListBeanNTIAddZenTtl.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddZenTtl.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddZenTtl.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddZenTtl.setStrGakubuCd( "NTI-ZEN" );
						s44ListBeanNTIAddZenTtl.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddZenTtl.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddZenTtl.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddZenTtl.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddZenTtl.setStrNittei( "" );
						s44ListBeanNTIAddZenTtl.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddZenTtl );
						zenTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(s44ListBean.getStrNtiCd()).equals("E") ){	 // 中期
					switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
						case 1:
						s44ListBeanNTIAddChuCnt.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddChuCnt.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddChuCnt.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddChuCnt.setStrGakubuCd( "NTI-CHU" );
						s44ListBeanNTIAddChuCnt.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddChuCnt.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddChuCnt.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddChuCnt.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddChuCnt.setStrNittei( "" );
						s44ListBeanNTIAddChuCnt.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddChuCnt );
						chuCenterNTIFlg = true;
							break;
						case 2:
						s44ListBeanNTIAddChuSnd.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddChuSnd.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddChuSnd.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddChuSnd.setStrGakubuCd( "NTI-CHU" );
						s44ListBeanNTIAddChuSnd.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddChuSnd.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddChuSnd.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddChuSnd.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddChuSnd.setStrNittei( "" );
						s44ListBeanNTIAddChuSnd.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddChuSnd );
						chuSecondNTIFlg = true;
							break;
						case 3:
						s44ListBeanNTIAddChuTtl.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddChuTtl.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddChuTtl.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddChuTtl.setStrGakubuCd( "NTI-CHU" );
						s44ListBeanNTIAddChuTtl.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddChuTtl.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddChuTtl.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddChuTtl.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddChuTtl.setStrNittei( "" );
						s44ListBeanNTIAddChuTtl.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddChuTtl );
						chuTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(s44ListBean.getStrNtiCd()).equals("C") ){	 // 後期
					switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
						case 1:
						s44ListBeanNTIAddKokCnt.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddKokCnt.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddKokCnt.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddKokCnt.setStrGakubuCd( "NTI-KOK" );
						s44ListBeanNTIAddKokCnt.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddKokCnt.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddKokCnt.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddKokCnt.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddKokCnt.setStrNittei( "" );
						s44ListBeanNTIAddKokCnt.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddKokCnt );
						kokCenterNTIFlg = true;
							break;
						case 2:
						s44ListBeanNTIAddKokSnd.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddKokSnd.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddKokSnd.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddKokSnd.setStrGakubuCd( "NTI-KOK" );
						s44ListBeanNTIAddKokSnd.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddKokSnd.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddKokSnd.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddKokSnd.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddKokSnd.setStrNittei( "" );
						s44ListBeanNTIAddKokSnd.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddKokSnd );
						kokSecondNTIFlg = true;
							break;
						case 3:
						s44ListBeanNTIAddKokTtl.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddKokTtl.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddKokTtl.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddKokTtl.setStrGakubuCd( "NTI-KOK" );
						s44ListBeanNTIAddKokTtl.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddKokTtl.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddKokTtl.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddKokTtl.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddKokTtl.setStrNittei( "" );
						s44ListBeanNTIAddKokTtl.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddKokTtl );
						kokTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(s44ListBean.getStrNtiCd()).equals("N") ){	 // 一般型
					switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
						case 1:
						s44ListBeanNTIAddIpnCnt.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddIpnCnt.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddIpnCnt.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddIpnCnt.setStrGakubuCd( "NTI-IPN" );
						s44ListBeanNTIAddIpnCnt.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddIpnCnt.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddIpnCnt.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddIpnCnt.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddIpnCnt.setStrNittei( "" );
						s44ListBeanNTIAddIpnCnt.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddIpnCnt );
						ipnCenterNTIFlg = true;
							break;
						case 2:
						s44ListBeanNTIAddIpnSnd.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddIpnSnd.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddIpnSnd.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddIpnSnd.setStrGakubuCd( "NTI-IPN" );
						s44ListBeanNTIAddIpnSnd.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddIpnSnd.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddIpnSnd.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddIpnSnd.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddIpnSnd.setStrNittei( "" );
						s44ListBeanNTIAddIpnSnd.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddIpnSnd );
						ipnSecondNTIFlg = true;
							break;
						case 3:
						s44ListBeanNTIAddIpnTtl.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddIpnTtl.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddIpnTtl.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddIpnTtl.setStrGakubuCd( "NTI-IPN" );
						s44ListBeanNTIAddIpnTtl.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddIpnTtl.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddIpnTtl.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddIpnTtl.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddIpnTtl.setStrNittei( "" );
						s44ListBeanNTIAddIpnTtl.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddIpnTtl );
						ipnTotalNTIFlg = true;
							break;
					}
				}else if( cm.toString(s44ListBean.getStrNtiCd()).equals("1") ){	 // センター型
					switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
						case 1:
						s44ListBeanNTIAddCenCnt.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddCenCnt.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddCenCnt.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddCenCnt.setStrGakubuCd( "NTI-CEN" );
						s44ListBeanNTIAddCenCnt.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddCenCnt.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddCenCnt.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddCenCnt.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddCenCnt.setStrNittei( "" );
						s44ListBeanNTIAddCenCnt.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddCenCnt );
						cenCenterNTIFlg = true;
							break;
						case 2:
						s44ListBeanNTIAddCenSnd.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddCenSnd.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddCenSnd.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddCenSnd.setStrGakubuCd( "NTI-CEN" );
						s44ListBeanNTIAddCenSnd.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddCenSnd.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddCenSnd.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddCenSnd.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddCenSnd.setStrNittei( "" );
						s44ListBeanNTIAddCenSnd.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddCenSnd );
						cenSecondNTIFlg = true;
							break;
						case 3:
						s44ListBeanNTIAddCenTtl.setStrGenKouKbn( s44ListBean.getStrGenKouKbn() );
						s44ListBeanNTIAddCenTtl.setStrDaigakuCd( s44ListBean.getStrDaigakuCd() );
						s44ListBeanNTIAddCenTtl.setStrDaigakuMei( s44ListBean.getStrDaigakuMei() );
						s44ListBeanNTIAddCenTtl.setStrGakubuCd( "NTI-CEN" );
						s44ListBeanNTIAddCenTtl.setStrGakubuMei( s44ListBean.getStrNittei() + "計" );
						s44ListBeanNTIAddCenTtl.setStrGakkaCd( s44ListBean.getStrGakkaCd() );
						s44ListBeanNTIAddCenTtl.setStrGakkaMei( s44ListBean.getStrGakkaMei() );
						s44ListBeanNTIAddCenTtl.setStrNtiCd( s44ListBean.getStrNtiCd() );
						s44ListBeanNTIAddCenTtl.setStrNittei( "" );
						s44ListBeanNTIAddCenTtl.setStrHyouka( s44ListBean.getStrHyouka() );
						s44GakkoListNTIAdd = deepCopyCl( s44GakkoListNTIAddCenTtl );
						cenTotalNTIFlg = true;
							break;
					}
				}

				//
				ArrayList s44GakkoList		= s44ListBean.getS44GakkoList();
				ArrayList s44GakkoListEdt	= new ArrayList();
				ListIterator itrClass = s44GakkoList.listIterator();
				int b = 0;
				boolean firstFlgGKB3 = true;
				boolean firstFlgDGK3 = true;
				boolean firstFlgNTI3 = true;
				if(s44GakkoListGKBAdd.size() == 0){
					firstFlgGKB3 = false;
				}
				if(s44GakkoListDGKAdd.size() == 0){
					firstFlgDGK3 = false;
				}
				if(s44GakkoListNTIAdd.size() == 0){
					firstFlgNTI3 = false;
				}
				/** 学校・学年・クラス **/
				while( itrClass.hasNext() ){
					S44GakkoListBean s44GakkoListBean = (S44GakkoListBean)itrClass.next();
					S44GakkoListBean s44GakkoListBeanEdt = new S44GakkoListBean();
					S44GakkoListBean s44GakkoListBeanDGKAdd = null;
					S44GakkoListBean s44GakkoListBeanGKBAdd = null;
					S44GakkoListBean s44GakkoListBeanNTIAdd = null;

					// 移し変え
					s44GakkoListBeanEdt.setStrGakkomei( s44GakkoListBean.getStrGakkomei() );

					// 大学計用データ呼び出し
					if(firstFlgDGK3 == true){
						s44GakkoListBeanDGKAdd = (S44GakkoListBean) s44GakkoListDGKAdd.get(b);
						s44NinzuListDGKAdd = deepCopyNz( s44GakkoListBeanDGKAdd.getS44HyoukaNinzuList() );
					}else{
						s44GakkoListBeanDGKAdd = new S44GakkoListBean();

						s44GakkoListBeanDGKAdd.setStrGakkomei( s44GakkoListBean.getStrGakkomei() );
					}

					// 学部計用データ呼び出し
					if(firstFlgGKB3 == true){
						s44GakkoListBeanGKBAdd = (S44GakkoListBean) s44GakkoListGKBAdd.get(b);
						s44NinzuListGKBAdd = deepCopyNz( s44GakkoListBeanGKBAdd.getS44HyoukaNinzuList() );
					}else{
						s44GakkoListBeanGKBAdd = new S44GakkoListBean();

						s44GakkoListBeanGKBAdd.setStrGakkomei( s44GakkoListBean.getStrGakkomei() );
					}

					// 日程計用データ呼び出し
					if(firstFlgNTI3 == true){
						s44GakkoListBeanNTIAdd = (S44GakkoListBean) s44GakkoListNTIAdd.get(b);
						s44NinzuListNTIAdd = deepCopyNz( s44GakkoListBeanNTIAdd.getS44HyoukaNinzuList() );
					}else{
						s44GakkoListBeanNTIAdd = new S44GakkoListBean();

						s44GakkoListBeanNTIAdd.setStrGakkomei( s44GakkoListBean.getStrGakkomei() );
					}

					/** 年度・人数 **/
					ArrayList s44NinzuList = s44GakkoListBean.getS44HyoukaNinzuList();
					ArrayList s44NinzuListEdt = new ArrayList();
					ListIterator itrNinzu = s44NinzuList.listIterator();
					int a = 0;
					boolean firstFlgDGK2 = true;
					boolean firstFlgGKB2 = true;
					boolean firstFlgNTI2 = true;
					if(s44NinzuListDGKAdd.size() == 0){
						firstFlgDGK2 = false;
					}
					if(s44NinzuListGKBAdd.size() == 0){
						firstFlgGKB2 = false;
					}
					if(s44NinzuListNTIAdd.size() == 0){
						firstFlgNTI2 = false;
					}
					while( itrNinzu.hasNext() ){
						S44HyoukaNinzuListBean s44NinzuBean = (S44HyoukaNinzuListBean)itrNinzu.next();
						S44HyoukaNinzuListBean s44NinzuBeanEdt = new S44HyoukaNinzuListBean();
						S44HyoukaNinzuListBean s44NinzuBeanDGKAdd = null;
						S44HyoukaNinzuListBean s44NinzuBeanGKBAdd = null;
						S44HyoukaNinzuListBean s44NinzuBeanNTIAdd = null;

						// 移し変え
						s44NinzuBeanEdt.setStrNendo( s44NinzuBean.getStrNendo() );
						s44NinzuBeanEdt.setIntSoshibo( s44NinzuBean.getIntSoshibo() );
						s44NinzuBeanEdt.setIntDai1shibo( s44NinzuBean.getIntDai1shibo() );
						s44NinzuBeanEdt.setIntHyoukaA( s44NinzuBean.getIntHyoukaA() );
						s44NinzuBeanEdt.setIntHyoukaB( s44NinzuBean.getIntHyoukaB() );
						s44NinzuBeanEdt.setIntHyoukaC( s44NinzuBean.getIntHyoukaC() );

						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//s44NinzuBeanEdt.setIntHyoukaA_Hukumu( s44NinzuBean.getIntHyoukaA_Hukumu() );
						//s44NinzuBeanEdt.setIntHyoukaB_Hukumu( s44NinzuBean.getIntHyoukaB_Hukumu() );
						//s44NinzuBeanEdt.setIntHyoukaC_Hukumu( s44NinzuBean.getIntHyoukaC_Hukumu() );
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

						s44NinzuListEdt.add( s44NinzuBeanEdt );

						// 加算処理(大学計用)
						if(firstFlgDGK2 == true){
							s44NinzuBeanDGKAdd = (S44HyoukaNinzuListBean) s44NinzuListDGKAdd.get(a);
						}else{
							s44NinzuBeanDGKAdd = new S44HyoukaNinzuListBean();

							s44NinzuBeanDGKAdd.setStrNendo( s44NinzuBean.getStrNendo() );
//							s44NinzuBeanDGKAdd.setIntSoshibo( 0 );
//							s44NinzuBeanDGKAdd.setIntDai1shibo( 0 );
//							s44NinzuBeanDGKAdd.setIntHyoukaA( 0 );
//							s44NinzuBeanDGKAdd.setIntHyoukaB( 0 );
//							s44NinzuBeanDGKAdd.setIntHyoukaC( 0 );
						}

						if(s44NinzuBean.getIntSoshibo()!=-999){
//							s44NinzuBeanDGKAdd.setIntSoshibo( s44NinzuBeanDGKAdd.getIntSoshibo() + s44NinzuBean.getIntSoshibo() );
							if(s44NinzuBeanDGKAdd.getIntSoshibo()!=-999){
								s44NinzuBeanDGKAdd.setIntSoshibo(s44NinzuBeanDGKAdd.getIntSoshibo() + s44NinzuBean.getIntSoshibo());
							}else{
								s44NinzuBeanDGKAdd.setIntSoshibo(s44NinzuBean.getIntSoshibo());
							}
						}
						if(s44NinzuBean.getIntDai1shibo()!=-999){
//							s44NinzuBeanDGKAdd.setIntDai1shibo( s44NinzuBeanDGKAdd.getIntDai1shibo() + s44NinzuBean.getIntDai1shibo() );
							if(s44NinzuBeanDGKAdd.getIntDai1shibo()!=-999){
								s44NinzuBeanDGKAdd.setIntDai1shibo(s44NinzuBeanDGKAdd.getIntDai1shibo() + s44NinzuBean.getIntDai1shibo());
							}else{
								s44NinzuBeanDGKAdd.setIntDai1shibo(s44NinzuBean.getIntDai1shibo());
							}
						}
						if(s44NinzuBean.getIntHyoukaA()!=-999){
//							s44NinzuBeanDGKAdd.setIntHyoukaA( s44NinzuBeanDGKAdd.getIntHyoukaA() + s44NinzuBean.getIntHyoukaA() );
							if(s44NinzuBeanDGKAdd.getIntHyoukaA()!=-999){
								s44NinzuBeanDGKAdd.setIntHyoukaA(s44NinzuBeanDGKAdd.getIntHyoukaA() + s44NinzuBean.getIntHyoukaA());
							}else{
								s44NinzuBeanDGKAdd.setIntHyoukaA(s44NinzuBean.getIntHyoukaA());
							}
						}

						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(s44NinzuBean.getIntHyoukaA_Hukumu()!=-999){
						//	if(s44NinzuBeanDGKAdd.getIntHyoukaA_Hukumu()!=-999){
						//		s44NinzuBeanDGKAdd.setIntHyoukaA_Hukumu(s44NinzuBeanDGKAdd.getIntHyoukaA_Hukumu() + s44NinzuBean.getIntHyoukaA_Hukumu());
						//	}else{
						//		s44NinzuBeanDGKAdd.setIntHyoukaA_Hukumu(s44NinzuBean.getIntHyoukaA_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

						if(s44NinzuBean.getIntHyoukaB()!=-999){
//							s44NinzuBeanDGKAdd.setIntHyoukaB( s44NinzuBeanDGKAdd.getIntHyoukaB() + s44NinzuBean.getIntHyoukaB() );
							if(s44NinzuBeanDGKAdd.getIntHyoukaB()!=-999){
								s44NinzuBeanDGKAdd.setIntHyoukaB(s44NinzuBeanDGKAdd.getIntHyoukaB() + s44NinzuBean.getIntHyoukaB());
							}else{
								s44NinzuBeanDGKAdd.setIntHyoukaB(s44NinzuBean.getIntHyoukaB());
							}
						}

						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(s44NinzuBean.getIntHyoukaB_Hukumu()!=-999){
						//	if(s44NinzuBeanDGKAdd.getIntHyoukaB_Hukumu()!=-999){
						//		s44NinzuBeanDGKAdd.setIntHyoukaB_Hukumu(s44NinzuBeanDGKAdd.getIntHyoukaB_Hukumu() + s44NinzuBean.getIntHyoukaB_Hukumu());
						//	}else{
						//		s44NinzuBeanDGKAdd.setIntHyoukaB_Hukumu(s44NinzuBean.getIntHyoukaB_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

						if(s44NinzuBean.getIntHyoukaC()!=-999){
//							s44NinzuBeanDGKAdd.setIntHyoukaC( s44NinzuBeanDGKAdd.getIntHyoukaC() + s44NinzuBean.getIntHyoukaC() );
							if(s44NinzuBeanDGKAdd.getIntHyoukaC()!=-999){
								s44NinzuBeanDGKAdd.setIntHyoukaC(s44NinzuBeanDGKAdd.getIntHyoukaC() + s44NinzuBean.getIntHyoukaC());
							}else{
								s44NinzuBeanDGKAdd.setIntHyoukaC(s44NinzuBean.getIntHyoukaC());
							}
						}

						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(s44NinzuBean.getIntHyoukaC_Hukumu()!=-999){
						//	if(s44NinzuBeanDGKAdd.getIntHyoukaC_Hukumu()!=-999){
						//		s44NinzuBeanDGKAdd.setIntHyoukaC_Hukumu(s44NinzuBeanDGKAdd.getIntHyoukaC_Hukumu() + s44NinzuBean.getIntHyoukaC_Hukumu());
						//	}else{
						//		s44NinzuBeanDGKAdd.setIntHyoukaC_Hukumu(s44NinzuBean.getIntHyoukaC_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

						if(firstFlgDGK2 == true){
							s44NinzuListDGKAdd.set(a,s44NinzuBeanDGKAdd);
						}else{
							s44NinzuListDGKAdd.add(s44NinzuBeanDGKAdd);
						}

						// 加算処理(学部計用)
						if(firstFlgGKB2 == true){
							s44NinzuBeanGKBAdd = (S44HyoukaNinzuListBean) s44NinzuListGKBAdd.get(a);
						}else{
							s44NinzuBeanGKBAdd = new S44HyoukaNinzuListBean();

							s44NinzuBeanGKBAdd.setStrNendo( s44NinzuBean.getStrNendo() );
//							s44NinzuBeanGKBAdd.setIntSoshibo( 0 );
//							s44NinzuBeanGKBAdd.setIntDai1shibo( 0 );
//							s44NinzuBeanGKBAdd.setIntHyoukaA( 0 );
//							s44NinzuBeanGKBAdd.setIntHyoukaB( 0 );
//							s44NinzuBeanGKBAdd.setIntHyoukaC( 0 );
						}

						if(s44NinzuBean.getIntSoshibo()!=-999){
//							s44NinzuBeanGKBAdd.setIntSoshibo( s44NinzuBeanGKBAdd.getIntSoshibo() + s44NinzuBean.getIntSoshibo() );
							if(s44NinzuBeanGKBAdd.getIntSoshibo()!=-999){
								s44NinzuBeanGKBAdd.setIntSoshibo(s44NinzuBeanGKBAdd.getIntSoshibo() + s44NinzuBean.getIntSoshibo());
							}else{
								s44NinzuBeanGKBAdd.setIntSoshibo(s44NinzuBean.getIntSoshibo());
							}
						}
						if(s44NinzuBean.getIntDai1shibo()!=-999){
//							s44NinzuBeanGKBAdd.setIntDai1shibo( s44NinzuBeanGKBAdd.getIntDai1shibo() + s44NinzuBean.getIntDai1shibo() );
							if(s44NinzuBeanGKBAdd.getIntDai1shibo()!=-999){
								s44NinzuBeanGKBAdd.setIntDai1shibo(s44NinzuBeanGKBAdd.getIntDai1shibo() + s44NinzuBean.getIntDai1shibo());
							}else{
								s44NinzuBeanGKBAdd.setIntDai1shibo(s44NinzuBean.getIntDai1shibo());
							}
						}
						if(s44NinzuBean.getIntHyoukaA()!=-999){
//							s44NinzuBeanGKBAdd.setIntHyoukaA( s44NinzuBeanGKBAdd.getIntHyoukaA() + s44NinzuBean.getIntHyoukaA() );
							if(s44NinzuBeanGKBAdd.getIntHyoukaA()!=-999){
								s44NinzuBeanGKBAdd.setIntHyoukaA(s44NinzuBeanGKBAdd.getIntHyoukaA() + s44NinzuBean.getIntHyoukaA());
							}else{
								s44NinzuBeanGKBAdd.setIntHyoukaA(s44NinzuBean.getIntHyoukaA());
							}
						}

						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(s44NinzuBean.getIntHyoukaA_Hukumu()!=-999){
						//	if(s44NinzuBeanGKBAdd.getIntHyoukaA_Hukumu()!=-999){
						//		s44NinzuBeanGKBAdd.setIntHyoukaA_Hukumu(s44NinzuBeanGKBAdd.getIntHyoukaA_Hukumu() + s44NinzuBean.getIntHyoukaA_Hukumu());
						//	}else{
						//		s44NinzuBeanGKBAdd.setIntHyoukaA_Hukumu(s44NinzuBean.getIntHyoukaA_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

						if(s44NinzuBean.getIntHyoukaB()!=-999){
//							s44NinzuBeanGKBAdd.setIntHyoukaB( s44NinzuBeanGKBAdd.getIntHyoukaB() + s44NinzuBean.getIntHyoukaB() );
							if(s44NinzuBeanGKBAdd.getIntHyoukaB()!=-999){
								s44NinzuBeanGKBAdd.setIntHyoukaB(s44NinzuBeanGKBAdd.getIntHyoukaB() + s44NinzuBean.getIntHyoukaB());
							}else{
								s44NinzuBeanGKBAdd.setIntHyoukaB(s44NinzuBean.getIntHyoukaB());
							}
						}

						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(s44NinzuBean.getIntHyoukaB_Hukumu()!=-999){
						//	if(s44NinzuBeanGKBAdd.getIntHyoukaB_Hukumu()!=-999){
						//		s44NinzuBeanGKBAdd.setIntHyoukaB_Hukumu(s44NinzuBeanGKBAdd.getIntHyoukaB_Hukumu() + s44NinzuBean.getIntHyoukaB_Hukumu());
						//	}else{
						//		s44NinzuBeanGKBAdd.setIntHyoukaB_Hukumu(s44NinzuBean.getIntHyoukaB_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

						if(s44NinzuBean.getIntHyoukaC()!=-999){
//							s44NinzuBeanGKBAdd.setIntHyoukaC( s44NinzuBeanGKBAdd.getIntHyoukaC() + s44NinzuBean.getIntHyoukaC() );
							if(s44NinzuBeanGKBAdd.getIntHyoukaC()!=-999){
								s44NinzuBeanGKBAdd.setIntHyoukaC(s44NinzuBeanGKBAdd.getIntHyoukaC() + s44NinzuBean.getIntHyoukaC());
							}else{
								s44NinzuBeanGKBAdd.setIntHyoukaC(s44NinzuBean.getIntHyoukaC());
							}
						}

						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(s44NinzuBean.getIntHyoukaC_Hukumu()!=-999){
						//	if(s44NinzuBeanGKBAdd.getIntHyoukaC_Hukumu()!=-999){
						//		s44NinzuBeanGKBAdd.setIntHyoukaC_Hukumu(s44NinzuBeanGKBAdd.getIntHyoukaC_Hukumu() + s44NinzuBean.getIntHyoukaC_Hukumu());
						//	}else{
						//		s44NinzuBeanGKBAdd.setIntHyoukaC_Hukumu(s44NinzuBean.getIntHyoukaC_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

						if(firstFlgGKB2 == true){
							s44NinzuListGKBAdd.set(a,s44NinzuBeanGKBAdd);
						}else{
							s44NinzuListGKBAdd.add(s44NinzuBeanGKBAdd);
						}

						// 加算処理(日程計用)
						if(firstFlgNTI2 == true){
							s44NinzuBeanNTIAdd = (S44HyoukaNinzuListBean) s44NinzuListNTIAdd.get(a);
						}else{
							s44NinzuBeanNTIAdd = new S44HyoukaNinzuListBean();

							s44NinzuBeanNTIAdd.setStrNendo( s44NinzuBean.getStrNendo() );
//							s44NinzuBeanNTIAdd.setIntSoshibo( 0 );
//							s44NinzuBeanNTIAdd.setIntDai1shibo( 0 );
//							s44NinzuBeanNTIAdd.setIntHyoukaA( 0 );
//							s44NinzuBeanNTIAdd.setIntHyoukaB( 0 );
//							s44NinzuBeanNTIAdd.setIntHyoukaC( 0 );
						}

						if(s44NinzuBean.getIntSoshibo()!=-999){
//							s44NinzuBeanNTIAdd.setIntSoshibo( s44NinzuBeanNTIAdd.getIntSoshibo() + s44NinzuBean.getIntSoshibo() );
							if(s44NinzuBeanNTIAdd.getIntSoshibo()!=-999){
								s44NinzuBeanNTIAdd.setIntSoshibo(s44NinzuBeanNTIAdd.getIntSoshibo() + s44NinzuBean.getIntSoshibo());
							}else{
								s44NinzuBeanNTIAdd.setIntSoshibo(s44NinzuBean.getIntSoshibo());
							}
						}
						if(s44NinzuBean.getIntDai1shibo()!=-999){
//							s44NinzuBeanNTIAdd.setIntDai1shibo( s44NinzuBeanNTIAdd.getIntDai1shibo() + s44NinzuBean.getIntDai1shibo() );
							if(s44NinzuBeanNTIAdd.getIntDai1shibo()!=-999){
								s44NinzuBeanNTIAdd.setIntDai1shibo(s44NinzuBeanNTIAdd.getIntDai1shibo() + s44NinzuBean.getIntDai1shibo());
							}else{
								s44NinzuBeanNTIAdd.setIntDai1shibo(s44NinzuBean.getIntDai1shibo());
							}
						}
						if(s44NinzuBean.getIntHyoukaA()!=-999){
//							s44NinzuBeanNTIAdd.setIntHyoukaA( s44NinzuBeanNTIAdd.getIntHyoukaA() + s44NinzuBean.getIntHyoukaA() );
							if(s44NinzuBeanNTIAdd.getIntHyoukaA()!=-999){
								s44NinzuBeanNTIAdd.setIntHyoukaA(s44NinzuBeanNTIAdd.getIntHyoukaA() + s44NinzuBean.getIntHyoukaA());
							}else{
								s44NinzuBeanNTIAdd.setIntHyoukaA(s44NinzuBean.getIntHyoukaA());
							}
						}

						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(s44NinzuBean.getIntHyoukaA_Hukumu()!=-999){
						//	if(s44NinzuBeanNTIAdd.getIntHyoukaA_Hukumu()!=-999){
						//		s44NinzuBeanNTIAdd.setIntHyoukaA_Hukumu(s44NinzuBeanNTIAdd.getIntHyoukaA_Hukumu() + s44NinzuBean.getIntHyoukaA_Hukumu());
						//	}else{
						//		s44NinzuBeanNTIAdd.setIntHyoukaA_Hukumu(s44NinzuBean.getIntHyoukaA_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

						if(s44NinzuBean.getIntHyoukaB()!=-999){
//							s44NinzuBeanNTIAdd.setIntHyoukaB( s44NinzuBeanNTIAdd.getIntHyoukaB() + s44NinzuBean.getIntHyoukaB() );
							if(s44NinzuBeanNTIAdd.getIntHyoukaB()!=-999){
								s44NinzuBeanNTIAdd.setIntHyoukaB(s44NinzuBeanNTIAdd.getIntHyoukaB() + s44NinzuBean.getIntHyoukaB());
							}else{
								s44NinzuBeanNTIAdd.setIntHyoukaB(s44NinzuBean.getIntHyoukaB());
							}
						}

						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(s44NinzuBean.getIntHyoukaB_Hukumu()!=-999){
						//	if(s44NinzuBeanNTIAdd.getIntHyoukaB_Hukumu()!=-999){
						//		s44NinzuBeanNTIAdd.setIntHyoukaB_Hukumu(s44NinzuBeanNTIAdd.getIntHyoukaB_Hukumu() + s44NinzuBean.getIntHyoukaB_Hukumu());
						//	}else{
						//		s44NinzuBeanNTIAdd.setIntHyoukaB_Hukumu(s44NinzuBean.getIntHyoukaB_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

						if(s44NinzuBean.getIntHyoukaC()!=-999){
//							s44NinzuBeanNTIAdd.setIntHyoukaC( s44NinzuBeanNTIAdd.getIntHyoukaC() + s44NinzuBean.getIntHyoukaC() );
							if(s44NinzuBeanNTIAdd.getIntHyoukaC()!=-999){
								s44NinzuBeanNTIAdd.setIntHyoukaC(s44NinzuBeanNTIAdd.getIntHyoukaC() + s44NinzuBean.getIntHyoukaC());
							}else{
								s44NinzuBeanNTIAdd.setIntHyoukaC(s44NinzuBean.getIntHyoukaC());
							}
						}

						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
						//// 2019/09/30 QQ) 共通テスト対応 ADD START
						//if(s44NinzuBean.getIntHyoukaC_Hukumu()!=-999){
						//	if(s44NinzuBeanNTIAdd.getIntHyoukaC_Hukumu()!=-999){
						//		s44NinzuBeanNTIAdd.setIntHyoukaC_Hukumu(s44NinzuBeanNTIAdd.getIntHyoukaC_Hukumu() + s44NinzuBean.getIntHyoukaC_Hukumu());
						//	}else{
						//		s44NinzuBeanNTIAdd.setIntHyoukaC_Hukumu(s44NinzuBean.getIntHyoukaC_Hukumu());
						//	}
						//}
						//// 2019/09/30 QQ) 共通テスト対応 ADD END
						// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

						if(firstFlgNTI2 == true){
							s44NinzuListNTIAdd.set(a,s44NinzuBeanNTIAdd);
						}else{
							s44NinzuListNTIAdd.add(s44NinzuBeanNTIAdd);
						}
						a++;
					}

					// 格納
					s44GakkoListBeanEdt.setS44HyoukaNinzuList( s44NinzuListEdt );
					s44GakkoListEdt.add( s44GakkoListBeanEdt );

					// 大学計用
					s44GakkoListBeanDGKAdd.setS44HyoukaNinzuList( deepCopyNz(s44NinzuListDGKAdd) );
					if(firstFlgDGK3 == true){
						s44GakkoListDGKAdd.set(b,s44GakkoListBeanDGKAdd);
					}else{
						s44GakkoListDGKAdd.add(s44GakkoListBeanDGKAdd);
					}
					// 学部計用
					s44GakkoListBeanGKBAdd.setS44HyoukaNinzuList( deepCopyNz(s44NinzuListGKBAdd) );
					if(firstFlgGKB3 == true){
						s44GakkoListGKBAdd.set(b,s44GakkoListBeanGKBAdd);
					}else{
						s44GakkoListGKBAdd.add(s44GakkoListBeanGKBAdd);
					}
					// 日程計用
					s44GakkoListBeanNTIAdd.setS44HyoukaNinzuList( deepCopyNz(s44NinzuListNTIAdd) );
					if(firstFlgNTI3 == true){
						s44GakkoListNTIAdd.set(b,s44GakkoListBeanNTIAdd);
					}else{
						s44GakkoListNTIAdd.add(s44GakkoListBeanNTIAdd);
					}
					s44NinzuListDGKAdd = new ArrayList();
					s44NinzuListGKBAdd = new ArrayList();
					s44NinzuListNTIAdd = new ArrayList();
					b++;
				}
				// 格納
				s44ListBeanEdt.setS44GakkoList( s44GakkoListEdt );
				s44ListEdt.add( s44ListBeanEdt );
				// 大学計用
				switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
					case 1:
						s44GakkoListDGKAddCnt = deepCopyCl( s44GakkoListDGKAdd );
						break;
					case 2:
						s44GakkoListDGKAddSnd = deepCopyCl( s44GakkoListDGKAdd );
						break;
					case 3:
						s44GakkoListDGKAddTtl = deepCopyCl( s44GakkoListDGKAdd );
						break;
				}
				// 学部計用
				switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
					case 1:
						s44GakkoListGKBAddCnt = deepCopyCl( s44GakkoListGKBAdd );
						break;
					case 2:
						s44GakkoListGKBAddSnd = deepCopyCl( s44GakkoListGKBAdd );
						break;
					case 3:
						s44GakkoListGKBAddTtl = deepCopyCl( s44GakkoListGKBAdd );
						break;
				}
				// 日程計用
				if( cm.toString(s44ListBean.getStrNtiCd()).equals("D") ){		 // 前期
					switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
						case 1:
							s44GakkoListNTIAddZenCnt = deepCopyCl( s44GakkoListNTIAdd );
							break;
						case 2:
							s44GakkoListNTIAddZenSnd = deepCopyCl( s44GakkoListNTIAdd );
							break;
						case 3:
							s44GakkoListNTIAddZenTtl = deepCopyCl( s44GakkoListNTIAdd );
							break;
					}
				}else if( cm.toString(s44ListBean.getStrNtiCd()).equals("E") ){	 // 中期
					switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
						case 1:
							s44GakkoListNTIAddChuCnt = deepCopyCl( s44GakkoListNTIAdd );
							break;
						case 2:
							s44GakkoListNTIAddChuSnd = deepCopyCl( s44GakkoListNTIAdd );
							break;
						case 3:
							s44GakkoListNTIAddChuTtl = deepCopyCl( s44GakkoListNTIAdd );
							break;
					}
				}else if( cm.toString(s44ListBean.getStrNtiCd()).equals("C") ){	 // 後期
					switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
						case 1:
							s44GakkoListNTIAddKokCnt = deepCopyCl( s44GakkoListNTIAdd );
							break;
						case 2:
							s44GakkoListNTIAddKokSnd = deepCopyCl( s44GakkoListNTIAdd );
							break;
						case 3:
							s44GakkoListNTIAddKokTtl = deepCopyCl( s44GakkoListNTIAdd );
							break;
					}
				}else if( cm.toString(s44ListBean.getStrNtiCd()).equals("N") ){	 // 一般型
					switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
						case 1:
							s44GakkoListNTIAddIpnCnt = deepCopyCl( s44GakkoListNTIAdd );
							break;
						case 2:
							s44GakkoListNTIAddIpnSnd = deepCopyCl( s44GakkoListNTIAdd );
							break;
						case 3:
							s44GakkoListNTIAddIpnTtl = deepCopyCl( s44GakkoListNTIAdd );
							break;
					}
				}else if( cm.toString(s44ListBean.getStrNtiCd()).equals("1") ){	 // センター型
					switch( Integer.parseInt(s44ListBean.getStrHyouka()) ){
						case 1:
							s44GakkoListNTIAddCenCnt = deepCopyCl( s44GakkoListNTIAdd );
							break;
						case 2:
							s44GakkoListNTIAddCenSnd = deepCopyCl( s44GakkoListNTIAdd );
							break;
						case 3:
							s44GakkoListNTIAddCenTtl = deepCopyCl( s44GakkoListNTIAdd );
							break;
					}
				}

				s44GakkoListDGKAdd = new ArrayList();
				s44GakkoListGKBAdd = new ArrayList();
				s44GakkoListNTIAdd = new ArrayList();
			}


			// 学部コードが変わる→学部計の保存
			if(centerGKBFlg == true){
				s44ListBeanGKBAddCnt.setS44GakkoList( s44GakkoListGKBAddCnt );
				s44ListEdt.add( s44ListBeanGKBAddCnt );

				centerGKBFlg = false;
			}
			if(secondGKBFlg == true){
				s44ListBeanGKBAddSnd.setS44GakkoList( s44GakkoListGKBAddSnd );
				s44ListEdt.add( s44ListBeanGKBAddSnd );

				secondGKBFlg = false;
			}
			if(totalGKBFlg == true){
				s44ListBeanGKBAddTtl.setS44GakkoList( s44GakkoListGKBAddTtl );
				s44ListEdt.add( s44ListBeanGKBAddTtl );

				totalGKBFlg = false;
			}

			// 大学コードが変わる→日程計・大学計の保存
			// 日程計
			if(zenCenterNTIFlg == true){
				s44ListBeanNTIAddZenCnt.setS44GakkoList( s44GakkoListNTIAddZenCnt );
				s44ListEdt.add( s44ListBeanNTIAddZenCnt );

				zenCenterNTIFlg = false;
			}
			if(zenSecondNTIFlg == true){
				s44ListBeanNTIAddZenSnd.setS44GakkoList( s44GakkoListNTIAddZenSnd );
				s44ListEdt.add( s44ListBeanNTIAddZenSnd );

				zenSecondNTIFlg = false;
			}
			if(zenTotalNTIFlg == true){
				s44ListBeanNTIAddZenTtl.setS44GakkoList( s44GakkoListNTIAddZenTtl );
				s44ListEdt.add( s44ListBeanNTIAddZenTtl );

				zenTotalNTIFlg = false;
			}
			if(chuCenterNTIFlg == true){
				s44ListBeanNTIAddChuCnt.setS44GakkoList( s44GakkoListNTIAddChuCnt );
				s44ListEdt.add( s44ListBeanNTIAddChuCnt );

				chuCenterNTIFlg = false;
			}
			if(chuSecondNTIFlg == true){
				s44ListBeanNTIAddChuSnd.setS44GakkoList( s44GakkoListNTIAddChuSnd );
				s44ListEdt.add( s44ListBeanNTIAddChuSnd );

				chuSecondNTIFlg = false;
			}
			if(chuTotalNTIFlg == true){
				s44ListBeanNTIAddChuTtl.setS44GakkoList( s44GakkoListNTIAddChuTtl );
				s44ListEdt.add( s44ListBeanNTIAddChuTtl );

				chuTotalNTIFlg = false;
			}
			if(kokCenterNTIFlg == true){
				s44ListBeanNTIAddKokCnt.setS44GakkoList( s44GakkoListNTIAddKokCnt );
				s44ListEdt.add( s44ListBeanNTIAddKokCnt );

				kokCenterNTIFlg = false;
			}
			if(kokSecondNTIFlg == true){
				s44ListBeanNTIAddKokSnd.setS44GakkoList( s44GakkoListNTIAddKokSnd );
				s44ListEdt.add( s44ListBeanNTIAddKokSnd );

				kokSecondNTIFlg = false;
			}
			if(kokTotalNTIFlg == true){
				s44ListBeanNTIAddKokTtl.setS44GakkoList( s44GakkoListNTIAddKokTtl );
				s44ListEdt.add( s44ListBeanNTIAddKokTtl );

				kokTotalNTIFlg = false;
			}
			if(ipnCenterNTIFlg == true){
				s44ListBeanNTIAddIpnCnt.setS44GakkoList( s44GakkoListNTIAddIpnCnt );
				s44ListEdt.add( s44ListBeanNTIAddIpnCnt );

				ipnCenterNTIFlg = false;
			}
			if(ipnSecondNTIFlg == true){
				s44ListBeanNTIAddIpnSnd.setS44GakkoList( s44GakkoListNTIAddIpnSnd );
				s44ListEdt.add( s44ListBeanNTIAddIpnSnd );

				ipnSecondNTIFlg = false;
			}
			if(ipnTotalNTIFlg == true){
				s44ListBeanNTIAddIpnTtl.setS44GakkoList( s44GakkoListNTIAddIpnTtl );
				s44ListEdt.add( s44ListBeanNTIAddIpnTtl );

				ipnTotalNTIFlg = false;
			}
			if(cenCenterNTIFlg == true){
				s44ListBeanNTIAddCenCnt.setS44GakkoList( s44GakkoListNTIAddCenCnt );
				s44ListEdt.add( s44ListBeanNTIAddCenCnt );

				cenCenterNTIFlg = false;
			}
			if(cenSecondNTIFlg == true){
				s44ListBeanNTIAddCenSnd.setS44GakkoList( s44GakkoListNTIAddCenSnd );
				s44ListEdt.add( s44ListBeanNTIAddCenSnd );

				cenSecondNTIFlg = false;
			}
			if(cenTotalNTIFlg == true){
				s44ListBeanNTIAddCenTtl.setS44GakkoList( s44GakkoListNTIAddCenTtl );
				s44ListEdt.add( s44ListBeanNTIAddCenTtl );

				cenTotalNTIFlg = false;
			}

			// 大学計
			if(centerDGKFlg == true){
				s44ListBeanDGKAddCnt.setS44GakkoList( s44GakkoListDGKAddCnt );
				s44ListEdt.add( s44ListBeanDGKAddCnt );

				centerDGKFlg = false;
			}
			if(secondDGKFlg == true){
				s44ListBeanDGKAddSnd.setS44GakkoList( s44GakkoListDGKAddSnd );
				s44ListEdt.add( s44ListBeanDGKAddSnd );

				secondDGKFlg = false;
			}
			if(totalDGKFlg == true){
				s44ListBeanDGKAddTtl.setS44GakkoList( s44GakkoListDGKAddTtl );
				s44ListEdt.add( s44ListBeanDGKAddTtl );

				totalDGKFlg = false;
			}

			// 格納
			s44ItemEdt.setS44List( s44ListEdt );

			return s44ItemEdt;
		}catch(Exception e){
			e.toString();
			throw e;

		}
	}

	/**
	 *
	 * @param s44NinzuList
	 * @return
	 */
	private ArrayList deepCopyNz(ArrayList s44NinzuList){
		ListIterator itrNinzu = s44NinzuList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrNinzu.hasNext()){
			S44HyoukaNinzuListBean s44NinzuBean = (S44HyoukaNinzuListBean)itrNinzu.next();
			S44HyoukaNinzuListBean s44NinzuBeanCopy = new S44HyoukaNinzuListBean();

			s44NinzuBeanCopy.setStrNendo( s44NinzuBean.getStrNendo() );
			s44NinzuBeanCopy.setIntSoshibo( s44NinzuBean.getIntSoshibo() );
			s44NinzuBeanCopy.setIntDai1shibo( s44NinzuBean.getIntDai1shibo() );
			s44NinzuBeanCopy.setIntHyoukaA( s44NinzuBean.getIntHyoukaA() );
			s44NinzuBeanCopy.setIntHyoukaB( s44NinzuBean.getIntHyoukaB() );
			s44NinzuBeanCopy.setIntHyoukaC( s44NinzuBean.getIntHyoukaC() );

			// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
			//// 2019/09/30 QQ) 共通テスト対応 ADD START
			//s44NinzuBeanCopy.setIntHyoukaA_Hukumu( s44NinzuBean.getIntHyoukaA_Hukumu() );
			//s44NinzuBeanCopy.setIntHyoukaB_Hukumu( s44NinzuBean.getIntHyoukaB_Hukumu() );
			//s44NinzuBeanCopy.setIntHyoukaC_Hukumu( s44NinzuBean.getIntHyoukaC_Hukumu() );
			//// 2019/09/30 QQ) 共通テスト対応 ADD END
			// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

			copyList.add( s44NinzuBeanCopy );
		}

		return copyList;
	}

	/**
	 *
	 * @param s44GakkoList
	 * @return
	 */
	private ArrayList deepCopyCl(ArrayList s44GakkoList){
		ListIterator itrClass = s44GakkoList.listIterator();
		ArrayList copyList = new ArrayList();
		while(itrClass.hasNext()){
			S44GakkoListBean s44ClassBean = (S44GakkoListBean)itrClass.next();
			S44GakkoListBean s44ClassBeanCopy = new S44GakkoListBean();

			s44ClassBeanCopy.setStrGakkomei( s44ClassBean.getStrGakkomei() );
			s44ClassBeanCopy.setS44HyoukaNinzuList( deepCopyNz(s44ClassBean.getS44HyoukaNinzuList()) );

			copyList.add( s44ClassBeanCopy );
		}

		return copyList;
	}

	/**
	 *
	 * @param s44List
	 * @return
	 */
	private ArrayList deepCopy(ArrayList s44List){
		ListIterator itr = s44List.listIterator();
		ArrayList copyList = new ArrayList();
		while(itr.hasNext()){
			S44ListBean s44Bean = (S44ListBean)itr.next();
			S44ListBean s44BeanCopy = new S44ListBean();

			s44BeanCopy.setS44GakkoList(  deepCopyCl(s44Bean.getS44GakkoList()) );
			s44BeanCopy.setStrDaigakuCd( s44Bean.getStrDaigakuCd() );
			s44BeanCopy.setStrDaigakuMei( s44Bean.getStrDaigakuMei() );
			s44BeanCopy.setStrGakkaCd( s44Bean.getStrGakkaCd() );
			s44BeanCopy.setStrGakkaMei( s44Bean.getStrGakkaMei() );
			s44BeanCopy.setStrGakubuCd( s44Bean.getStrGakubuCd() );
			s44BeanCopy.setStrGakubuMei( s44Bean.getStrGakubuMei() );
			s44BeanCopy.setStrGenKouKbn( s44Bean.getStrGenKouKbn() );
			s44BeanCopy.setStrHyouka( s44Bean.getStrHyouka() );
			s44BeanCopy.setStrNtiCd( s44Bean.getStrNtiCd() );
			s44BeanCopy.setStrNittei( s44Bean.getStrNtiCd() );

			copyList.add( s44BeanCopy );
		}

		return copyList;
	}

	/*
	 * 	Excel編集
	 * 		HSSFWorkbook 	workbook: Excelワークブック
	 * 		ArrayList 		s44HyoukaNinzuList:評価別人数リスト
	 * 		int				intRow:開始行
	 * 		int				intCol:開始列
	 * 		戻り値: なし
	 */
	private boolean setData(HSSFSheet workSheet, S44HyoukaNinzuListBean s44HyoukaNinzuListBean, int intRow, int intCol ){

		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		int		maxSheetIndex	= 0;
		int		row = intRow;


		// 基本ファイルを読込む
		try{

			//模試年度
			workCell = cm.setCell( workSheet, workRow, workCell, row, 4);
			workCell.setCellValue( s44HyoukaNinzuListBean.getStrNendo() + "年度" );

			//全国総志望者数
			if( s44HyoukaNinzuListBean.getIntSoshibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol);
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntSoshibo() );
			}

			//志望者数（第一志望）
			if( s44HyoukaNinzuListBean.getIntDai1shibo() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 1);
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntDai1shibo() );
			}

			//評価別人数
			if( s44HyoukaNinzuListBean.getIntHyoukaA() != -999 ){
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 2 );
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntHyoukaA() );
			}

			// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
			//// 2019/09/30 QQ) 共通テスト対応 ADD START
			//if( s44HyoukaNinzuListBean.getIntHyoukaA_Hukumu() != -999 ){
			//	workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
			//	workCell.setCellValue( s44HyoukaNinzuListBean.getIntHyoukaA_Hukumu() );
			//}
			//// 2019/09/30 QQ) 共通テスト対応 ADD END
			// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

			if( s44HyoukaNinzuListBean.getIntHyoukaB() != -999 ){
			        // 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
				//// 2019/09/30 QQ) 共通テスト対応 UPD START
				//workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
				//// 2019/09/30 QQ) 共通テスト対応 UPD END
				workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 3 );
				// 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntHyoukaB() );
			}

			// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
			//// 2019/09/30 QQ) 共通テスト対応 ADD START
			//if( s44HyoukaNinzuListBean.getIntHyoukaB_Hukumu() != -999 ){
			//	workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 5 );
			//	workCell.setCellValue( s44HyoukaNinzuListBean.getIntHyoukaB_Hukumu() );
			//}
			//// 2019/09/30 QQ) 共通テスト対応 ADD END
			// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

			if( s44HyoukaNinzuListBean.getIntHyoukaC() != -999 ){
			        // 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD START
				//// 2019/09/30 QQ) 共通テスト対応 UPD START
				//workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 6 );
				//// 2019/09/30 QQ) 共通テスト対応 UPD END
			        workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 4 );
			        // 2019/11/25 QQ)nagai 英語認定試験延期対応 UPD END
				workCell.setCellValue( s44HyoukaNinzuListBean.getIntHyoukaC() );
			}

			// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START
			//// 2019/09/30 QQ) 共通テスト対応 ADD START
			//if( s44HyoukaNinzuListBean.getIntHyoukaC_Hukumu() != -999 ){
			//	workCell = cm.setCell( workSheet, workRow, workCell, row, intCol + 7 );
			//	workCell.setCellValue( s44HyoukaNinzuListBean.getIntHyoukaC_Hukumu() );
			//}
			//// 2019/09/30 QQ) 共通テスト対応 ADD END
			// 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}

		return true;
	}

}
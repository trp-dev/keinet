/*
 * 作成日: 2004/09/09
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.help;

import java.io.Serializable;

/**
 * @author nino
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class HelpList implements Serializable  {
	private String helpId;			// ヘルプID
	private String titleStr;		// タイトル文字列
	private String category;		// カテゴリ
	private String dispSequence;	// 表示順
	private String explan;			// 説明文
	private String update;			// 更新日時

	/**
	 * @return
	 */
	public String getHelpId() {
		return helpId;
	}

	/**
	 * @return
	 */
	public String getTitleStr() {
		return titleStr;
	}

	/**
	 * @return
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @return
	 */
	public String getDispSequence() {
		return dispSequence;
	}

	/**
	 * @return
	 */
	public String getExplan() {
		return explan;
	}

	/**
	 * @return
	 */
	public String getUpdate() {
		return update;
	}

	/**
	 * @param string
	 */
	public void setHelpId(String string) {
		helpId = string;
	}

	/**
	 * @param string
	 */
	public void setTitleStr(String string) {
		titleStr = string;
	}

	/**
	 * @param string
	 */
	public void setCategory(String string) {
		category = string;
	}

	/**
	 * @param string
	 */
	public void setDispSequence(String string) {
		dispSequence = string;
	}

	/**
	 * @param string
	 */
	public void setExplan(String string) {
		explan = string;
	}

	/**
	 * @param string
	 */
	public void setUpdate(String string) {
		update = string;
	}

}

/*
 * 作成日: 2004/08/04
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.sheet.cls;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.beans.sheet.NoQuestionException;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.excel.data.cls.C33ClassListBean;
import jp.co.fj.keinavi.excel.data.cls.C33Item;
import jp.co.fj.keinavi.excel.data.cls.C33ListBean;
import jp.co.fj.keinavi.excel.cls.C33;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 2005.04.06	Yoshimoto KAWAI - Totec
 * 				出力種別フラグ対応
 * 
 * 2009.10.13   Fujito URAKAWA - Totec
 *              「全体成績」追加対応 
 * 
 * @author kawai
 *
 */
public class C33SheetBean extends AbstractSheetBean {

	// データクラス
	private final C33Item data = new C33Item();

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		String[] code = super.getSubjectCDArray(); // 型・科目設定値
		List graph = super.getSubjectGraphList(); // 型・科目グラフ表示対象
		
		String tmp_kmkcd = "";	// 科目コードチェック用変数

		// 設問別成績存在チェック
		if (hasAllNoQuestionSubject(code)) {
			throw new NoQuestionException();
		}

		// データを詰める
		data.setStrGakkomei(profile.getBundleName()); // 学校名
		data.setStrMshmei(exam.getExamName()); // 模試名
		data.setStrMshDate(exam.getInpleDate()); // 模試実施基準日
		data.setIntHyouFlg(super.getIntFlag(IProfileItem.CHART)); // 表フラグ
		data.setIntGraphFlg(super.getIntFlag(IProfileItem.GRAPH)); // グラフフラグ
		data.setIntSecuFlg(super.getIntFlag(IProfileItem.PRINT_STAMP)); // セキュリティスタンプ
		data.setIntShubetsuFlg(super.getOutputType()); // 出力種別フラグ

		// ワークテーブルのセットアップ
		super.insertIntoCountCompClass();

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		try {
			// データリスト
			Query query = QueryLoader.getInstance().load("s33_1");
			query.setStringArray(1, code); // 型・科目コード

			ps1 = conn.prepareStatement(query.toString());
			ps1.setString(1, exam.getExamYear()); // 模試年度
			ps1.setString(2, exam.getExamCD()); // 模試コード

			// クラスデータリスト（自校）
			ps2 = conn.prepareStatement(QueryLoader.getInstance().load("s33_2").toString());
			ps2.setString(1, exam.getExamYear()); // 模試年度
			ps2.setString(2, exam.getExamCD()); // 模試コード
			ps2.setString(3, profile.getBundleCD()); // 一括コード		

			// クラスデータリスト（クラス）
			ps3 = conn.prepareStatement(QueryLoader.getInstance().load("s33_3").toString());
			ps3.setString(1, exam.getExamYear()); // 模試年度
			ps3.setString(2, exam.getExamCD()); // 模試コード
			ps3.setString(3, profile.getBundleCD()); // 一括コード		
			ps3.setString(6, exam.getExamYear()); // 模試年度
			ps3.setString(7, exam.getExamCD()); // 模試コード
			ps3.setString(8, profile.getBundleCD()); // 一括コード		
			ps3.setString(11, exam.getExamYear()); // 模試年度
			ps3.setString(12, exam.getExamCD()); // 模試コード
			
			// 科目別クラスデータリスト（自校）
			{
				ps4 = conn.prepareStatement(QueryLoader.getInstance().load("s33_4").toString());
				ps4.setString(1, exam.getExamYear());
				ps4.setString(2, exam.getExamCD());
				ps4.setString(3, profile.getBundleCD());
			}
			
			// 科目別クラスデータリスト（クラス）
			{
				ps5 = conn.prepareStatement(QueryLoader.getInstance().load("s33_5").toString());
				ps5.setString(1, exam.getExamYear());
				ps5.setString(2, exam.getExamCD());
				ps5.setString(3, profile.getBundleCD());
				ps5.setString(5, exam.getExamYear());
				ps5.setString(6, exam.getExamCD());
				ps5.setString(8, profile.getBundleCD());
			}


			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				
				// 科目コードが異なる場合のみ、全体成績データをセットする
				if (!tmp_kmkcd.equals(rs1.getString(1))) {
					
					putTotalSubjectData(rs1, ps4, ps5, graph, profile);
					
				}
				
				C33ListBean bean = new C33ListBean();
				bean.setStrKmkCd(rs1.getString(1));
				bean.setStrKmkmei(rs1.getString(2));
				bean.setIntDispKmkFlg(graph.contains(rs1.getString(1)) ? 1 : 0);
				bean.setStrHaitenKmk(rs1.getString(3));
				bean.setStrSetsuNo(rs1.getString(4));
				bean.setStrSetsuMei1(rs1.getString(5));
				bean.setStrSetsuHaiten(rs1.getString(6));
				data.getC33List().add(bean);

				// 科目コード
				ps2.setString(4, rs1.getString(1));
				ps3.setString(4, rs1.getString(1));
				ps3.setString(9, rs1.getString(1));
				ps3.setString(13, rs1.getString(1));
				// 設問番号
				ps2.setInt(5, rs1.getInt(7));
				ps3.setInt(5, rs1.getInt(7));
				ps3.setInt(10, rs1.getInt(7));
				ps3.setInt(14, rs1.getInt(7));

				// 自校
				{
					C33ClassListBean c = new C33ClassListBean();
					c.setStrGakkomei(profile.getBundleName());
					c.setIntDispClassFlg(1);

					rs2 = ps2.executeQuery();

					if (rs2.next()) {
						c.setIntNinzu(rs2.getInt(1));
						c.setFloTokuritsu(rs2.getFloat(2));
					} else {
						c.setIntNinzu(-999);
						c.setFloTokuritsu(-999);
					}
					
					rs2.close();

					bean.getC33ClassList().add(c);					
				}
				
				// クラス
				{
					rs3 = ps3.executeQuery();
					while (rs3.next()) {
						// Beanに詰める
						C33ClassListBean c = new C33ClassListBean();
						c.setStrGrade(rs3.getString(1));
						c.setStrClass(rs3.getString(2));
						c.setIntDispClassFlg(rs3.getInt(3));
						c.setIntNinzu(rs3.getInt(4));
						c.setFloTokuritsu(rs3.getFloat(5));
						bean.getC33ClassList().add(c);
					}
					rs3.close();
				}
				
				// 処理中の科目コードをセット
				tmp_kmkcd = rs1.getString(1);
				
			}
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(rs2);
			DbUtils.closeQuietly(rs3);
			DbUtils.closeQuietly(ps1);
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(ps3);
			DbUtils.closeQuietly(ps4);
			DbUtils.closeQuietly(ps5);
		}
	}

	/**
	 * 設問別成績の先頭に「全体成績」データをセットする
	 * @param rs1
	 * @param ps4
	 * @param ps5
	 * @param graph 
	 * @param profile 
	 * @throws SQLException 
	 */
	private void putTotalSubjectData(ResultSet rs1, PreparedStatement ps4,
			PreparedStatement ps5, List graph, Profile profile) throws SQLException {
		
		ResultSet rs4 = null;
		ResultSet rs5 = null;
		
		try {
			C33ListBean bean = new C33ListBean();
			bean.setStrKmkCd(rs1.getString(1));
			bean.setStrKmkmei(rs1.getString(2));
			bean.setIntDispKmkFlg(graph.contains(rs1.getString(1)) ? 1 : 0);
			bean.setStrHaitenKmk(rs1.getString(3));
			bean.setStrSetsuNo("");							// 設問Noに「""」をセット
			bean.setStrSetsuMei1("全体成績");				// 設問名に「"全体成績"」をセット
			bean.setStrSetsuHaiten(rs1.getString(3));		// 配点に科目配点をセット
			data.getC33List().add(bean);
	
			// 科目コード
			ps4.setString(4, rs1.getString(1));
			ps5.setString(4, rs1.getString(1));
			ps5.setString(7, rs1.getString(1));
	
			// 自校
			{
				C33ClassListBean c = new C33ClassListBean();
				c.setStrGakkomei(profile.getBundleName());
				c.setIntDispClassFlg(1);
				rs4 = ps4.executeQuery();
				if (rs4.next()) {
					c.setIntNinzu(rs4.getInt(1));
					c.setFloTokuritsu(rs4.getFloat(2));
				} else {
					c.setIntNinzu(-999);
					c.setFloTokuritsu(-999);
				}
				rs4.close();
				bean.getC33ClassList().add(c);	
			}
			
			// クラス
			{
				rs5 = ps5.executeQuery();
				while (rs5.next()) {
					// Beanに詰める
					C33ClassListBean c = new C33ClassListBean();
					c.setStrGrade(rs5.getString(1));
					c.setStrClass(rs5.getString(2));
					c.setIntDispClassFlg(rs5.getInt(3));
					c.setIntNinzu(rs5.getInt(4));
					c.setFloTokuritsu(rs5.getFloat(5));
					bean.getC33ClassList().add(c);
				}
				rs5.close();
			}
		}
		finally {
			DbUtils.closeQuietly(rs4);
			DbUtils.closeQuietly(rs5);
		}
	}

	/**
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
	 */
	protected double calcNumberOfPrint() {
		return getNumberOfPrint("C33_01");
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return IProfileCategory.C_COMP_QUE;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return new C33()
			.c33(this.data, this.outfileList, super.getAction(), this.sessionKey, sheetLog);
	}

}

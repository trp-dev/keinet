package jp.co.fj.keinavi.beans.sheet.school.data;

/**
 *
 * S52_11データクラス
 * 
 * 2007.07.26	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class S52_11Data extends S42_15Data {

	/**
	 * @return 科目リストデータのインスタンス
	 */
	public S42_15SubjectListData createSubjectListData() {
		return new S52_11SubjectListData();
	}

	/**
	 * @return センター到達指標リストデータのインスタンス
	 */
	public S42_15ReachLevelListData createReachLevelListData() {
		return new S52_11ReachLevelListData();
	}

	/**
	 * @return 過回リストデータのインスタンス
	 */
	public S52_11ExamListData createExamListData() {
		return new S52_11ExamListData();
	}
	
	/**
	 * @return スコア帯リストデータのインスタンス
	 */
	public S42_15ScoreZoneListData createScoreZoneListData() {
		return new S52_11ScoreZoneListData();
	}
	
}

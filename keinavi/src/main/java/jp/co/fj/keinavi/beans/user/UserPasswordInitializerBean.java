package jp.co.fj.keinavi.beans.user;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 利用者パスワード初期化Bean
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class UserPasswordInitializerBean extends DefaultBean {

	// 学校コード
	private final String schoolCd;
	// 利用者ID
	private final String loginId;
	
	/**
	 * コンストラクタ
	 */
	public UserPasswordInitializerBean(final String schoolCd, final String loginId) {
		
		if (schoolCd == null) {
			throw new IllegalArgumentException("学校コードがNULLです。");
		}
		
		if (loginId == null) {
			throw new IllegalArgumentException("利用者IDがNULLです。");
		}
		
		this.schoolCd = schoolCd;
		this.loginId = loginId;
	}

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(QueryLoader.getInstance().load("u03").toString());
			
			// 学校コード
			ps.setString(1, schoolCd);
			// 利用者ID
			ps.setString(2, loginId);
			
			if (ps.executeUpdate() == 0) {
				throw new SQLException(
						"管理者のパスワードは更新できません。学校ID="
						+ schoolCd + "/利用者ID=" + loginId);
			}
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
}

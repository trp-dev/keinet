package jp.co.fj.keinavi.servlets.cls;

import java.sql.Connection;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import jp.co.fj.keinavi.beans.ClassExamBean;
import jp.co.fj.keinavi.beans.com_set.ProfileChargeClassBean;
import jp.co.fj.keinavi.beans.com_set.SubjectCountBean;
import jp.co.fj.keinavi.beans.exam.ExamFilterManager;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.servlets.DefaultHttpServlet;

/**
 * 
 * クラス成績分析Servlet（基本クラス）
 * 
 * 
 * 2005.02.25	Yoshimoto KAWAI - Totec
 * 				担当クラスの年度対応
 * 
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 * 
 * @author kawai
 * 
 */
public abstract class AbstractCServlet extends DefaultHttpServlet {

	protected static final Set topSet = new HashSet(); // トップ画面の画面IDセット
	protected static final Set detailSet = new HashSet(); // 詳細画面の画面IDセット
	protected static final Set typeSet = new HashSet(); // 型の設定がある画面IDセット
	protected static final Set courseSet = new HashSet(); // 科目の設定がある画面IDセット

	static {
		topSet.add("c001");
		topSet.add("c101");
		detailSet.add("c002");
		detailSet.add("c003");
		detailSet.add("c004");
		detailSet.add("c005");
		detailSet.add("c006");
		detailSet.add("c102");
		detailSet.add("c103");
		detailSet.add("c104");
		detailSet.add("c105");
		typeSet.add("c002");
		typeSet.add("c004");
		typeSet.add("c102");
		typeSet.add("c103");
		courseSet.add("c002");
		courseSet.add("c004");
		courseSet.add("c005");
		courseSet.add("c102");
		courseSet.add("c103");
		courseSet.add("c104");
	}

	/**
	 * 担当クラスの個人が受験した模試の模試セッションを取得する。
	 * ただし、担当クラスを利用しない画面であるなら学校単位の模試セッションを返す。
	 * 
	 * @param request
	 * @return
	 * @throws ServletException
	 */
	protected ExamSession getClassExamSession(final HttpServletRequest request) throws ServletException {

		final ExamSession examSession;
		
		// クラス比較の詳細画面なら模試セッションはそのまま
		if ("c102".equals(super.getForward(request))
				|| "c103".equals(super.getForward(request))
				|| "c104".equals(super.getForward(request))
				|| "c105".equals(super.getForward(request))) {
			examSession = getExamSession(request);
		// そうでなければ模試セッションを再構築
		} else {
			examSession = rebuildExamSession(request);
		}
		
		// フィルタを通して返す
		return ExamFilterManager.rebuild(getForward(request),
				getLoginSession(request), examSession);
	}
	
	/**
	 * 模試セッションを再構築する
	 * 
	 * @param request
	 * @throws ServletException 
	 */
	protected ExamSession rebuildExamSession(
			final HttpServletRequest request) throws ServletException {
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			con.setAutoCommit(false);
		
			final ClassExamBean bean = new ClassExamBean(
					getExamSession(request).getPublicExamSet());
			bean.setConnection(null, con);
			bean.setLoginSession(getLoginSession(request));
			bean.setProfile(getProfile(request));
			bean.execute();
			con.commit();
			
			return bean.getExamSession();
		} catch (final Exception e){
			super.rollback(con);
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}
	}
	
	/**
	 * 担当クラスを取得してrequestへセットする
	 * @param request
	 */
	protected void setupChargeClass(
			final HttpServletRequest request) throws ServletException {
		
		// 担当クラスBean
		final ProfileChargeClassBean bean =
				new ProfileChargeClassBean(getProfile(request));
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			bean.setConnection(null, con);
			bean.execute();
		} catch (final Exception e){
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request, con);
		}

		request.setAttribute("ChargeClass", bean);
	}
	
	/**
	 * @param con DBコネクション
	 * @param schoolCd 学校コード
	 * @param examYear 模試年度
	 * @param examCd 模試コード
	 * @return 科目カウントBean
	 * @throws Exception SQL例外
	 */
	protected SubjectCountBean createSubjectCountBean(
			final Connection con, final String schoolCd,
			final String examYear, final String examCd) throws Exception{
		
		SubjectCountBean bean = new SubjectCountBean(schoolCd);
		bean.setConnection(null, con);
		bean.setExamYear(examYear);
		bean.setExamCd(examCd);
		bean.execute();
		return bean;
	}

}

package jp.co.fj.keinavi.beans.helpdesk;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.util.PasswordGenerator;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 * 
 * DLサービス校の利用者情報作成Bean
 *
 * 2008/12/24	[新規作成]
 *
 * @author	Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class LoginId4DLBean extends DefaultBean {

	// パスワード生成クラス
	private static final PasswordGenerator PWDGENERATOR = new PasswordGenerator(
			"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");

	// 学校コード
	private String schoolCd;
	// パスワード
	private String defaultPassword;
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
	
		// 利用者情報が存在しなければ作る
		if (!exists()) {
			create();
		}
	}

	// 利用者情報が存在するか
	private boolean exists() throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("u01"));
			ps.setString(1, schoolCd);
			rs = ps.executeQuery();
			return rs.next();
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	private void create() throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("hd07"));

			// 管理者
			ps.setString(1, schoolCd); // 学校コード
			ps.setString(2, "admin"); // 利用者ID
			ps.setString(3, "システム管理者"); // 利用者名
			ps.setString(4, defaultPassword); // ログインパスワード
			ps.setString(5, defaultPassword); // デフォルトパスワード
			ps.setString(6, "1"); // 管理者フラグ
			ps.setString(7, "1"); // 有効・無効フラグ
			ps.setString(8, "1"); // 高校別データDL機能フラグ
			ps.setString(9, "1"); // 模試・問題集申込機能フラグ
			ps.executeUpdate();

			String password = null;

			// user1
			password = PWDGENERATOR.generate(8);
			ps.setString(2, "user1"); // 利用者ID
			ps.setString(3, "ユーザ１"); // 利用者名
			ps.setString(4, password); // ログインパスワード
			ps.setString(5, password); // デフォルトパスワード
			ps.setString(6, "2"); // 管理者フラグ
			ps.setString(7, "1"); // 有効・無効フラグ
			ps.setString(8, "3"); // 高校別データDL機能フラグ
			ps.setString(9, "3"); // 模試・問題集申込機能フラグ
			ps.executeUpdate();
			
			// user2
			password = PWDGENERATOR.generate(8);
			ps.setString(2, "user2"); // 利用者ID
			ps.setString(3, "ユーザ２"); // 利用者名
			ps.setString(4, password); // ログインパスワード
			ps.setString(5, password); // デフォルトパスワード
			ps.setString(6, "2"); // 管理者フラグ
			ps.setString(7, "2"); // 有効・無効フラグ
			ps.setString(8, "3"); // 高校別データDL機能フラグ
			ps.setString(9, "1"); // 模試・問題集申込機能フラグ
			ps.executeUpdate();
			
			// user3
			password = PWDGENERATOR.generate(8);
			ps.setString(2, "user3"); // 利用者ID
			ps.setString(3, "ユーザ３"); // 利用者名
			ps.setString(4, password); // ログインパスワード
			ps.setString(5, password); // デフォルトパスワード
			ps.setString(6, "2"); // 管理者フラグ
			ps.setString(7, "2"); // 有効・無効フラグ
			ps.setString(8, "1"); // 高校別データDL機能フラグ
			ps.setString(9, "3"); // 模試・問題集申込機能フラグ
			ps.executeUpdate();
			
			// user4
			password = PWDGENERATOR.generate(8);
			ps.setString(2, "user4"); // 利用者ID
			ps.setString(3, "ユーザ４"); // 利用者名
			ps.setString(4, password); // ログインパスワード
			ps.setString(5, password); // デフォルトパスワード
			ps.setString(6, "2"); // 管理者フラグ
			ps.setString(7, "2"); // 有効・無効フラグ
			ps.setString(8, "1"); // 高校別データDL機能フラグ
			ps.setString(9, "1"); // 模試・問題集申込機能フラグ
			ps.executeUpdate();
			
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	/**
	 * @param schoolCd the schoolCd to set
	 */
	public void setSchoolCd(String schoolCd) {
		this.schoolCd = schoolCd;
	}
	
	/**
	 * @param defaultPassword the defaultPassword to set
	 */
	public void setDefaultPassword(String defaultPassword) {
		this.defaultPassword = defaultPassword;
	}

}

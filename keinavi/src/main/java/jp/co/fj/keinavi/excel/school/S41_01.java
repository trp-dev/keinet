/**
 * 校内成績分析−他校比較　成績概況（他校比較）
 * 	Excelファイル編集
 * 作成日: 2004/08/11
 * @author	T.Sakai
 */

package jp.co.fj.keinavi.excel.school;

import java.io.*;
import java.util.*;

import jp.co.fj.keinavi.excel.data.school.*;
import jp.co.fj.keinavi.excel.cm.*;
import jp.co.fj.keinavi.util.log.*;

import org.apache.poi.hssf.usermodel.*;

public class S41_01 {

	private int noerror = 0;		// 正常終了
	private int errfread = 1;		// ファイルreadエラー
	private int errfwrite = 2;	// ファイルwriteエラー
	private int errfdata = 3;		// データ設定エラー
	
	private CM cm = new CM();		//共通関数用クラス インスタンス
	
//	private String		strArea			= "A1:AO57";	//印刷範囲
	
	final private String masterfile0 = "S41_01";
	final private String masterfile1 = "NS41_01";
	private String masterfile = "";

/*
 * 	Excel編集メイン
 * 		S13Item s41Item: データクラス
 * 		String masterfile: マスタExcelファイル名（フルパス）
 * 		String outfile: 出力Excelファイル名（フルパス）
 * 		戻り値: 0:正常終了、0以外:異常終了
 */
 	public int s41_01EditExcel(S41Item s41Item, ArrayList outfilelist, int intSaveFlg, String UserID) {

		FileInputStream	fin = null;
		HSSFWorkbook	workbook = null;
		HSSFSheet	workSheet		= null;
		HSSFRow		workRow			= null;
		HSSFCell	workCell		= null;
		
		KNLog log = KNLog.getInstance(null,null,null);
		
		//テンプレートの決定
		if (s41Item.getIntShubetsuFlg() == 1){
			masterfile = masterfile1;
		} else{
			masterfile = masterfile0;
		}

		int		intMaxSheetIndex	= 0;
		
		//マスタExcel読み込み
		workbook = cm.getMasterWorkBook(masterfile, intSaveFlg);
		if( workbook==null ){
			return errfread;
		}
		
		// 基本ファイルを読込む
		S41ListBean s41ListBean = new S41ListBean();

		try {
			
			// データセット
			ArrayList s41List = s41Item.getS41List();
			Iterator itr = s41List.iterator();
			int row = 0;				//行
			int col = 0;				//列
			int gakkoCnt = 0;			//他校カウンタ
			int kmkCnt = 0;			//科目カウンタ
			int hyouCnt = 0;			//表カウンタ（0…上段、1…下段）
			String kmk = "";			//科目チェック用
			int maxGakko = 0;			//MAX他校数
			boolean kmkFlg = true;	//型→科目変更フラグ
			boolean kataExistFlg = false;	//型有無フラグ
			
			while( itr.hasNext() ) {
				s41ListBean = (S41ListBean)itr.next();

				//型有無チェック 2004.11.17 add
				if (Integer.parseInt(s41ListBean.getStrKmkCd()) >= 7000) {
					kataExistFlg = true;
				}
				
				//科目が変わる時のチェック
				if ( !cm.toString(kmk).equals("") ) {
					if (kmkFlg==true) {
						//型があるときのみ型→科目のチェックをする 2004.11.17 add
						if (kataExistFlg == true){
							//型から科目に変わる時のチェック
							if (Integer.parseInt(s41ListBean.getStrKmkCd()) < 7000) {
								//他校数が20以下のとき改表をする
								if (maxGakko<=20) {
									if (hyouCnt==0) {
										hyouCnt = 1;
									} else if (hyouCnt==1) {
										hyouCnt = 0;
									}
								}
								col = 0;
								kmkCnt = 0;
								kmkFlg = false;
							}
						}else{
							kmkFlg = false;
						}
					}
					//他校数が20以下で型･科目が10のとき改表をする
					if (kmkCnt==10 && maxGakko<=20) {
						if (hyouCnt==0) {
							hyouCnt = 1;
						} else if (hyouCnt==1) {
							hyouCnt = 0;
						}
						col = 0;
						kmkCnt = 0;
					}
					//他校数が21以上のとき表上段からデータセットされるようにする
					if (maxGakko>20) {
						hyouCnt = 0;
						if (kmkCnt==10) {
							col = 0;
							kmkCnt = 0;
						}

					}
				}

				//他校が20以下のときは20型･科目ごと、他校が21以上のときは10型･科目ごとに改シート
				if ( kmkCnt==0 && gakkoCnt==0 && hyouCnt==0 ) {
					// シートテンプレートのコピー
					workSheet = workbook.cloneSheet(0);
					intMaxSheetIndex++;

					// ヘッダ右側に帳票作成日時を表示する
					cm.setHeader(workbook, workSheet);
					
					// セキュリティスタンプセット
					String secuFlg = cm.setSecurity( workbook, workSheet, s41Item.getIntSecuFlg() ,38 ,40 );
					workCell = cm.setCell( workSheet, workRow, workCell, 0, 38 );
					workCell.setCellValue(secuFlg);
					
					// 学校名セット
					workCell = cm.setCell( workSheet, workRow, workCell, 2, 0 );
					workCell.setCellValue( "学校名　：" + cm.toString(s41Item.getStrGakkomei()) );

					// 模試月取得
					String moshi =cm.setTaisyouMoshi( s41Item.getStrMshDate() );
					// 対象模試セット
					workCell = cm.setCell( workSheet, workRow, workCell, 3, 0 );
					workCell.setCellValue( "対象模試：" + cm.toString(s41Item.getStrMshmei()) + moshi);
					hyouCnt = 0;
					col = 0;
				}
				
				if ( hyouCnt==0 ) {
					row = 4;
				}
				if ( hyouCnt==1 ) {
					row = 33;
				}
				
				if ( kmkCnt==0 || !cm.toString(kmk).equals(cm.toString(s41ListBean.getStrKmkmei())) ) {
					// 型・科目名セット
					workCell = cm.setCell( workSheet, workRow, workCell, row++, col+1 );
					workCell.setCellValue( s41ListBean.getStrKmkmei() );

					// 配点セット
					if ( !cm.toString(s41ListBean.getStrHaitenKmk()).equals("") ) {
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						workCell.setCellValue( "（" + s41ListBean.getStrHaitenKmk() + "）" );
					}
				}
				if ( hyouCnt==0 ) {
					row = 7;
				}
				if ( hyouCnt==1 ) {
					row = 36;
				}
				
				// 基本ファイルを読込む
				S41GakkoListBean s41GakkoListBean = new S41GakkoListBean();
				
				// 他校データセット
				ArrayList s41GakkoList = s41ListBean.getS41GakkoList();
				Iterator itrGakko = s41GakkoList.iterator();
				
				int ninzu = 0;
				float heikin = 0;
				float hensa = 0;
				
				while ( itrGakko.hasNext() ){
					s41GakkoListBean = (S41GakkoListBean)itrGakko.next();
					if (gakkoCnt==0) {
						if (col==0) {
							// 自校名セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col );
							workCell.setCellValue( s41Item.getStrGakkomei() );
						}
						// 人数セット
						ninzu = s41GakkoListBean.getIntNinzu();
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						if ( s41GakkoListBean.getIntNinzu() != -999 ) {
							workCell.setCellValue( s41GakkoListBean.getIntNinzu() );
						}
						// 平均点セット
						heikin = s41GakkoListBean.getFloHeikin();
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
						if ( s41GakkoListBean.getFloHeikin() != -999.0 ) {
							workCell.setCellValue( s41GakkoListBean.getFloHeikin() );
						}
						// 平均偏差値セット
						hensa = s41GakkoListBean.getFloHensa();
						workCell = cm.setCell( workSheet, workRow, workCell, row++, col+4 );
						if ( s41GakkoListBean.getFloHensa() != -999.0 ) {
							workCell.setCellValue( s41GakkoListBean.getFloHensa() );
						}
					} else {
						if (gakkoCnt==21) {
							if ( kmkCnt==0 || !cm.toString(kmk).equals(cm.toString(s41ListBean.getStrKmkmei())) ) {
								// 型・科目名セット
								workCell = cm.setCell( workSheet, workRow, workCell, 33, col+1 );
								workCell.setCellValue( s41ListBean.getStrKmkmei() );
								// 配点セット
								if ( !cm.toString(s41ListBean.getStrHaitenKmk()).equals("") ) {
									workCell = cm.setCell( workSheet, workRow, workCell, 34, col+1 );
									workCell.setCellValue( "（" + s41ListBean.getStrHaitenKmk() + "）" );
								}
							}
							row = 36;
							if (col==0) {
								// 自校名セット
								workCell = cm.setCell( workSheet, workRow, workCell, row, col );
								workCell.setCellValue( s41Item.getStrGakkomei() );
							}
							// 人数セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
							if ( ninzu != -999 ) {
								workCell.setCellValue( ninzu );
							}
							// 平均点セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
							if ( heikin != -999.0 ) {
								workCell.setCellValue( heikin );
							}
							// 平均偏差値セット
							workCell = cm.setCell( workSheet, workRow, workCell, row++, col+4 );
							if ( hensa != -999.0 ) {
								workCell.setCellValue( hensa );
							}
							hyouCnt = 1;
						}
						if (col==0) {
							// 他校名セット
							workCell = cm.setCell( workSheet, workRow, workCell, row, col );
							workCell.setCellValue( s41GakkoListBean.getStrGakkomei() );
						}
						// 人数セット
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+1 );
						if ( s41GakkoListBean.getIntNinzu() != -999 ) {
							workCell.setCellValue( s41GakkoListBean.getIntNinzu() );
						}
						// 平均点セット
						workCell = cm.setCell( workSheet, workRow, workCell, row, col+2 );
						if ( s41GakkoListBean.getFloHeikin() != -999.0 ) {
							workCell.setCellValue( s41GakkoListBean.getFloHeikin() );
						}
						// *セット
						if ( s41GakkoListBean.getFloHensa() > hensa ) {
							workCell = cm.setCell( workSheet, workRow, workCell, row, col+3 );
							workCell.setCellValue("*");
						}
						// 平均偏差値セット
						workCell = cm.setCell( workSheet, workRow, workCell, row++, col+4 );
						if ( s41GakkoListBean.getFloHensa() != -999.0 ) {
							workCell.setCellValue( s41GakkoListBean.getFloHensa() );
						}
					}
					maxGakko = gakkoCnt;
					gakkoCnt++;
				}
				gakkoCnt = 0;
				kmkCnt++;
				col = col + 4;
				kmk = s41ListBean.getStrKmkmei();
			}
			
			// Excelファイル保存
			boolean bolRet = false;
			bolRet = cm.bolFileSave(intSaveFlg, outfilelist, workbook, UserID, 0, masterfile, intMaxSheetIndex);

			if( bolRet == false ){
				return errfwrite;					
			}
			
		} catch(Exception e) {
			log.Err("S41_01","データセットエラー",e.toString());
			return errfdata;
		}
		
		return noerror;
	}

}
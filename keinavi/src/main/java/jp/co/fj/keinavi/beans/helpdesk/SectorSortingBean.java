package jp.co.fj.keinavi.beans.helpdesk;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.data.login.SectorData;
import jp.co.fj.keinavi.data.login.SectorSession;

import com.fjh.beans.DefaultBean;

/**
 * 部門分類リスト取得Bean
 * 
 * <2010年度マーク高２模試対応>
 * 2011.01.24	Tomohisa YAMADA
 * 				アップロード機能
 * 
 * @author kawai
 */
public class SectorSortingBean extends DefaultBean {

	private SectorSession sectorSession; // 部門セッション

	/**
	 * Bean実行
	 */
	public void execute() throws java.lang.Exception, java.sql.SQLException {
		sectorSession = new SectorSession();

		StringBuffer query = new StringBuffer();
		query.append("SELECT ss.sectorcd, ss.hsbusidivcd, ss.sectorsortingcd, ss.sectorname, NVL((SELECT count(cs.schoolcd) FROM correspondingschool cs WHERE cs.sectorsortingcd = ss.sectorsortingcd GROUP BY cs.sectorsortingcd), 0) AS downloadable_count FROM sectorsorting ss");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				SectorData data = new SectorData();
				data.setSectorCD(rs.getString(1));
				data.setHsBusiDivCD(rs.getString(2));
				data.setSectorSortingCD(rs.getString(3));
				data.setSectorName(rs.getString(4));
				data.setDownloadableCount(rs.getInt("downloadable_count"));
				sectorSession.getSectorList().add(data);
			}
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
			
	}


	/**
	 * @return
	 */
	public SectorSession getSectorSession() {
		return sectorSession;
	}

}


package jp.co.fj.keinavi.beans.txt_out;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

/**
 *
 * 模試別成績データ
 *
 *
 * @author TOTEC)URAKAWA.Fujito
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)HASE.Shoji
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)MORITA.Yuuichirou
 *
 */
public class OutputExamSeiseki extends AbstractSheetBean {

    //serialVersionUID
    private static final long serialVersionUID = -7303582577604826634L;

    // マーク系の模試かどうか
    protected boolean mark = false;
    // オープン模試かどうか
    protected boolean open = true;
    // センターリサーチかどうか
    protected boolean cr = false;
    // 記述系か私大模試かどうか
    protected boolean isWrtnOrPrivate;
    // 第１解答科目対象模試かどうか
    protected boolean ansno1 = false;

    // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD START
    // 年度による定義ファイルの変更
    protected static final String HEADER2019 = "2019";
    // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 ADD END

    protected static final String PREFIX_PROP = "v3_";

    /**
     * 初期化処理を行う
     */
    protected void init() {
        // マーク系かどうか
        this.mark = "01".equals(exam.getExamTypeCD());
        // センターリサーチかどうか
        this.cr = "38".equals(exam.getExamCD());
        // 記述系か私大模試どうか
        this.isWrtnOrPrivate = "02".equals(exam.getExamTypeCD()) || "03".equals(exam.getExamTypeCD()) || KNUtil.isH3PStage(exam) || JudgementConstants.Exam.Code.WRTN_2GRADE.equals(exam.getExamCD());
        // 第１解答科目対象模試かどうか
        this.ansno1 = "01".equals(exam.getExamCD()) || "02".equals(exam.getExamCD()) || "03".equals(exam.getExamCD()) || "04".equals(exam.getExamCD()) || "38".equals(exam.getExamCD());
    }

    // 実行関数
    public void execute() throws Exception {

        this.init();

        // 担当クラスを一時テーブルへ展開する
        super.setupCountChargeClass();

        OutPutParamSet set = new OutPutParamSet(login, exam, profile, "t102", sessionKey);

        List header = set.getHeader();
        File outPutFile = set.getOutPutFile();

        OutputTextBean out = new OutputTextBean();
        out.setOutFile(outPutFile);
        out.setOutputTextList(getAllData());
        //---------------------------------------------
        // [2004/11/09 nino] ヘッダ部の配点名を再設定
        // [2004/11/01 nino] ヘッダ部の科目名を再設定
        //---------------------------------------------
        header = setExamName(header, exam);
        //---------------------------------------------

        // ------------------------------------------------------------
        // 2005.02.16
        // Yoshimoto KAWAI - Totec
        // 余白部分の項目名書き換え
        // センターリサーチは
        // 古文換算→古文得点
        // 漢文換算→漢文得点
        if (this.mark) {
            ((String[]) header.get(118))[2] = this.cr ? "古文得点" : "古文換算";
            ((String[]) header.get(119))[2] = this.cr ? "漢文得点" : "漢文換算";
            ((String[]) header.get(120))[2] = "現古偏差";
        }
        // ------------------------------------------------------------

        out.setHeadTextList(header);
        out.setOutTarget(set.getTarget());
        out.setOutType(set.getOutType());
        out.execute();

        this.outfileList.add(outPutFile.getPath());
    }

    /**
     * 模試成績データと志望校情報を取得
     * @return
     */
    protected LinkedList getAllData() throws SQLException, FileNotFoundException, IOException {
        LinkedHashMap seisekiMap = new LinkedHashMap();
        LinkedHashMap candMap = new LinkedHashMap();
        LinkedList dataList = null;

        seisekiMap = getExamSeiseki();
        candMap = getCandidate();
        dataList = editData(seisekiMap, candMap);

        // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START
        //        // ------------------------------------------------------------
        //        // 2005.01.17
        //        // Yoshimoto KAWAI - Totec
        //        // センターリサーチなら年度に1を足す
        //        if (this.cr) {
        //            Integer key = new Integer(2);
        //            Iterator ite = dataList.iterator();
        //            while (ite.hasNext()) {
        //                Map map = (Map) ite.next();
        //                map.put(key, String.valueOf(Integer.parseInt((String) map.get(key)) + 1));
        //            }
        //        }
        //        // ------------------------------------------------------------
        // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END

        return dataList;
    }

    /**
     * 結果を編集
     * @param seisekiMap
     * @param candMap
     * @return
     */
    protected LinkedList editData(LinkedHashMap seisekiMap, LinkedHashMap candMap) throws FileNotFoundException, IOException {

        // [2004/11/01 nino] ソート順を変更（学年、クラス、クラス番号）
        // 成績側の個人IDをソートする
        ArrayList mstList = new ArrayList();
        Set key = seisekiMap.keySet();
        Iterator it = key.iterator();
        while (it.hasNext()) {
            mstList.add((String) it.next());
        }
        // Collections.sort(mstList);  // ★[2004/11/01 nino] 個人IDでのソートをやめる★

        // 対象模試から表示順情報を取得する
        // [2004/11/10 nino] 模試科目1〜22で科目コード重複の対策でorderStr[][]を使用
        //                   orderStr[0] : ダミー
        //                   orderStr[1]〜orderStr[22] : 科目1〜科目22
        String[][] orderStr = new String[23][];
        getShowOrder(exam, orderStr);

        // 各Mapから個人IDをkeyとする情報を取得
        Iterator mstit = mstList.iterator();
        LinkedList returnList = new LinkedList();
        //最初の行をセット
        //setFirstLise(returnList, targetExam);
        while (mstit.hasNext()) {
            String indkey = (String) mstit.next();
            LinkedList seisekiList = (LinkedList) seisekiMap.get(indkey);
            LinkedList candList = null;
            if (candMap.containsKey(indkey)) {
                candList = (LinkedList) candMap.get(indkey);
            } else {
                candList = new LinkedList();
            }

            // 個人データをセット
            setLineData(seisekiList, candList, orderStr, returnList);
        }

        return returnList;
    }

    /**
     * 表示順序データを取得する
     * @param targetExam  対象模試コード
     * @param orderStr    表示順情報（[2004/11/10 nino] 模試コード重複対応用）
     * @return
     */
    protected void getShowOrder(ExamData exam, String[][] orderStr) throws FileNotFoundException, IOException {

        //模試コードに対応したプロパティファイル名を取得
        String subjectfile = getPropertyFileName(exam);
        //設定を読み込む
        InputStream in = getClass().getResourceAsStream(subjectfile);

        if (in == null) {
            throw new IllegalArgumentException("properties file not found.");
        }

        Properties prop = new Properties();

        try {
            prop.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 模試コードに対応した教科名と表示順番を取得する
        for (int i = 1; i < orderStr.length; i++) {
            final String key = "S_CD" + StringUtils.leftPad(i + "", 2, "0");
            orderStr[i] = splitComma(prop.getProperty(key));
        }
    }

    /**
     * 模試ごとに参照するプロパティファイルを指定する
     * @param exam 対象模試
     * @return
     */
    protected String getPropertyFileName(ExamData exam) {

        // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START
        String year = exam.getExamYear();
        // 2020/04/08 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END

        List<String> noExamType = Arrays.asList("01", "02", "03", "19", "20", "22", "23", "24", "25");

        Map<String, List<String>> examMap = new HashMap<String, List<String>>();
        // 全統マーク模試、全統センター試験プレテスト、全統マーク高２
        examMap.put("01", Arrays.asList("01", "02", "03", "04", "66"));
        // 全統記述模試、全統私大模試、全統記述高２模試
        if (year.compareTo(HEADER2019) >= 0) {
            examMap.put("02", Arrays.asList("05", "06", "07", "09", "10", "65", "67"));
        } else {
            examMap.put("02", Arrays.asList("05", "06", "07", "09", "10", "67"));
        }
        // 全統高１模試、全統高２模試
        examMap.put("03", Arrays.asList("61", "62", "63", "71", "72", "73", "74"));
        ////////////////////オープン/////////////////////
        // 北大
        examMap.put("04", Arrays.asList("41"));
        // 東北大
        examMap.put("05", Arrays.asList("42", "43"));
        // 東大
        examMap.put("06", Arrays.asList("12", "13"));
        // 東工大
        examMap.put("07", Arrays.asList("21"));
        // 一橋大
        examMap.put("08", Arrays.asList("22"));
        // 名大
        examMap.put("09", Arrays.asList("18", "19"));
        // 京大
        examMap.put("10", Arrays.asList("15", "16"));
        // 阪大
        examMap.put("11", Arrays.asList("24"));
        // 神戸大
        examMap.put("12", Arrays.asList("25"));
        // 広大
        examMap.put("13", Arrays.asList("26"));
        // 九州大
        examMap.put("14", Arrays.asList("27"));
        // 慶大
        examMap.put("15", Arrays.asList("30"));
        // 早大
        examMap.put("16", Arrays.asList("29"));
        // 医進模試
        examMap.put("17", Arrays.asList("32"));
        // 早大・慶大
        examMap.put("18", Arrays.asList("31"));
        ////////////////////オープン/////////////////////
        // センター・リサーチ
        examMap.put("19", Arrays.asList("38"));
        // 新テスト
        examMap.put("20", Arrays.asList("28", "68", "78"));
        // 高１・２記述
        if (year.compareTo(HEADER2019) >= 0) {
            examMap.put("22", Arrays.asList("75"));
        } else {
            examMap.put("22", Arrays.asList("65", "75"));
        }
        // 高１プレステージ
        examMap.put("23", Arrays.asList(KNUtil.CD_H1PSTAGE));
        // 高２プレステージ
        examMap.put("24", Arrays.asList(KNUtil.CD_H2PSTAGE));
        // 高３プレステージ
        examMap.put("25", Arrays.asList(KNUtil.CD_H3PSTAGE));

        //受験学力測定テスト
        if (KNUtil.isAbilityExam(exam)) {
            this.open = false;
            return getSubjectMasterProperties("21", year);
        }

        // プロパティファイルの取得
        for (String key : examMap.keySet()) {
            if (examMap.get(key).contains(exam.getExamCD())) {
                if (noExamType.contains(key)) {
                    this.open = false;
                }
                return getSubjectMasterProperties(key, year);
            }
        }

        throw new InternalError("不正な模試コードです。" + exam.getExamCD());
    }

    /**
     *
     * @param examType
     * @param year
     * @return
     */
    private String getSubjectMasterProperties(String examType, String year) {
        String fileName = null;
        int min = Integer.parseInt(HEADER2019);
        for (int i = Integer.parseInt(year); i > min; i--) {
            fileName = "subjectmaster".concat(examType).concat("_").concat(String.valueOf(i)).concat(".properties");
            if (getClass().getResourceAsStream(PREFIX_PROP + fileName) != null) {
                return fileName;
            }
        }
        fileName = "subjectmaster".concat(examType).concat(".properties");
        return fileName;

    }

    /**
     * ヘッダ科目名を取得
     * @param targetExam
     * @return
     */
    protected String[] getHeadProperties(ExamData exam) throws FileNotFoundException, IOException {

        //模試コードに対応したプロパティファイル名を取得
        String subjectfile = getPropertyFileName(exam);

        //設定を読み込む
        InputStream in = OutputExamSeiseki.class.getResourceAsStream("head.properties");

        if (in == null) {
            throw new IllegalArgumentException("properties file not found.");
        }

        Properties prop = new Properties();
        try {
            prop.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] headName1 = splitComma(prop.getProperty("HEADNAME1"));
        String[] headName2 = splitComma(prop.getProperty("HEADNAME2"));
        String[] headName3 = splitComma(prop.getProperty("HEADNAME3"));

        // マーク系・記述系・センターリサーチ
        if ("subjectmaster01.properties".equals(subjectfile) || "subjectmaster02.properties".equals(subjectfile) || "subjectmaster19.properties".equals(subjectfile)) {
            return headName1;

            // 高１・２
        } else if ("subjectmaster03.properties".equals(subjectfile)) {
            return headName2;

            // オープン系
        } else {
            return headName3;
        }
    }

    /**
    * 文字列をカンマで分割して文字列配列にする
    * ※空文字列も含む
    * @param string
    * @return
    */
    public static String[] splitComma(String string) {
        List container = new LinkedList(); // 入れ物

        int index = 0;
        int start = 0;
        while ((index = string.indexOf(',', start)) != -1) {
            container.add(string.substring(start, index));
            start = index + 1;
        }
        container.add(string.substring(start, string.length()));

        return (String[]) container.toArray(new String[0]);
    }

    /**
     * ヘッダ項目の配点（換算得点）、科目名をセット
     * @param headList
     * @param targetExam
     * @param sExamTypeMark
     */
    protected LinkedList setExamName(List headerList, ExamData exam) throws FileNotFoundException, IOException {
        final int SEISEKISET = 5; // 成績データ集合
        final int SUBLENGTH = 22; // 一行に表示する成績データの個数
        final int HEADLENGTH = 8; // 行ヘッダの長さ

        String[] headExam = getHeadProperties(exam);
        LinkedList headData = new LinkedList();

        // 科目名をセット
        String str = null;
        int iPos = 0;
        for (int listCnt = 0; listCnt < headerList.size(); listCnt++) {
            String[] data = (String[]) headerList.get(listCnt);

            if (HEADLENGTH <= listCnt && listCnt < HEADLENGTH + SEISEKISET * SUBLENGTH) {

                iPos = (listCnt - HEADLENGTH) % SEISEKISET;
                if (iPos == 1 && data.length > 2) {
                    // 科目名
                    str = data[2];
                    str = headExam[(int) ((listCnt - HEADLENGTH) / SEISEKISET)];
                    data[2] = str;
                }

                // [2004/11/09 nino] 配点（記述模試の場合）／換算得点（マーク模試の場合）
                //                   デフォルトは"配01"〜"配22"
                if (iPos == 2 && data.length > 2) {
                    str = data[2];
                    // ターゲット模試がマーク模試かをチェック
                    if (this.mark) {
                        str = "換" + str.substring(1);
                    }
                    data[2] = str;
                }
            }
            headData.add(data);
        }
        return headData;
    }

    /**
     * @param string
     * @param i
     * @param string2
     * @return
     */
    protected Object getHeadIndex2(String string, int i, String string2) {
        String id = String.valueOf(i);
        return string + id + string2;
    }

    /**
     * タイトルを用意
     * @param string
     * @param i
     * @return
     */
    protected String getHeadIndex(String string, int i) {
        String id = String.valueOf(i);
        if (id.length() == 1) {
            id = "0" + id;
        }
        return string + id;
    }

    /**
     * 模試科目マスタに存在しない科目の科目名をセットする
     *
     * @param value
     */
    protected void setSubjectName(String[] value) {
        if (this.open)
            return; // オープン系なら科目名を新たにセットすることはしない

        if ("3170".equals(value[10])) {
            value[11] = "国�T現";
            value[15] = "3170";
        } else if ("3570".equals(value[10])) {
            value[11] = "国�T�U現";
            value[15] = "3570";
        } else if ("3470".equals(value[10])) {
            value[11] = "国�T現古";
            value[15] = "3470";
        } else if ("3870".equals(value[10])) {
            value[11] = "国�T�U現古";
            value[15] = "3870";
        } else if ("3120".equals(value[10])) {
            value[11] = "現代文";
            value[15] = "3120";
        } else if ("3210".equals(value[10])) {
            value[11] = "現・古";
            value[15] = "3210";
        }
    }

    /**
     * 行情報をセット
     * @param seisekiList 成績情報格納リスト（内部はString[10]）
     * @param candList    志望校情報格納リスト（内部はString[13]）
     * @param orderMap    表示順情報（key:科目コード、Object:String=表示位置１〜２２）
     * @param orderStr    表示順情報（[2004/11/10 nino] 模試コード重複対応用）
     * @param returnList
     */
    protected void setLineData(LinkedList seisekiList, LinkedList candList, String[][] orderStr, LinkedList returnList) {
        final int CANDIDATEPOS = 122;
        final int SEISEKISET = 5;
        final int HEADLENGTH = 8; // 行ヘッダの長さ
        final int CANDSET = 7; // 志望校データ集合

        // 配点／換算得点
        final int INDEX3;
        if (this.mark)
            INDEX3 = 10; // マーク系は換算得点
        else
            INDEX3 = 14; // それ以外は配点

        // [2004/11/01 nino] 表示順序対応
        // 科目の位置で複数の科目が存在する場合は表示順序の若いものを表示する
        LinkedHashMap<Integer, String> dispMap = new LinkedHashMap<Integer, String>(); // 表示順序リスト

        LinkedHashMap<Integer, String> lineMap = new LinkedHashMap<Integer, String>();
        //行ヘッダを作成する
        String[] header = (String[]) seisekiList.get(0);
        lineMap.put(1, header[0]); // 通番（個人ID）
        lineMap.put(2, header[1]); // 年度
        lineMap.put(3, header[2]); // 模試コード
        lineMap.put(4, header[3]); // 学校コード
        lineMap.put(5, header[4]); // 学年
        lineMap.put(6, header[5]); // クラス
        lineMap.put(7, header[6]); // クラス番号
        lineMap.put(8, header[7]); // カナ氏名

        Iterator seiIt = seisekiList.iterator();
        while (seiIt.hasNext()) {
            String[] seiseki = (String[]) seiIt.next();

            // [2004/10/29 nino] SUBSTR(C.SUBCD,1,3) → SUBSTR(C.SUBCD,1,4) に変更
            //                   ※テキスト出力は科目コード４桁に変更
            //                     科目コードのkeyは３桁のまま判定
            //String subcd = seiseki[8];					// 科目コードを取得
            //			String subcd;
            //			if (seiseki[8].length() >= 3) {
            //				subcd = seiseki[8].substring(0, 3);			// 科目コード先頭3桁を取得
            //			}else{
            //				subcd = seiseki[8];							// 科目コードを取得
            //			}
            // ------------------------------------------------------------
            // 2004.12.01
            // Yoshimoto KAWAI - Totec
            // オープン系は４桁のままにする
            String subcd;
            if (this.open) {
                subcd = seiseki[8]; // 科目コードを取得
            } else {
                subcd = seiseki[8].substring(0, 3); // 科目コード先頭3桁を取得
            }
            // ------------------------------------------------------------

            //------------------------------------------------------------------------------
            // [2004/11/10 nino] 模試コード重複対応
            //↓----------------------------------------------------------------------------
            //if (orderMap.containsKey(subcd)) {
            //	String pos = (String)orderMap.get(subcd);		// その科目の表示位置を取得
            //	// [nino 2004/10/25] 表示位置"9"からセットするため(-1)する
            //	// int iPos = new Integer(pos).intValue();		// 表示位置をkeyにMapに格納
            //	int iPos = new Integer(pos).intValue() - 1;		// 表示位置をkeyにMapに格納
            //------------------------------------------------------------------------------
            int subPos = 0; // 表示位置初期化（ダミー位置）
            while (subPos < orderStr.length) {
                subPos++;
                subPos = getSubjectPos(orderStr, subcd, subPos); // その科目の表示位置を取得
                if (subPos >= orderStr.length) {
                    break;
                }
                int iPos = subPos - 1; // 表示位置をkeyにMapに格納
                //↑----------------------------------------------------------------------------

                // ------------------------------------------------------------
                // 2005.02.15
                // Yoshimoto KAWAI - Totec
                // 特定の科目名をセットする
                setSubjectName(seiseki);
                // ------------------------------------------------------------

                //----------------------------------------------------------------------------------------
                // [2004/11/01 nino] 表示順序対応
                //↓--------------------------------------------------------------------------------------

                if (seiseki[13] == null) {
                    seiseki[13] = seiseki[8];
                }
                if (!dispMap.containsKey(iPos)) {
                    dispMap.put(iPos, new String("10000"));
                }
                int dispSequence = Integer.valueOf(dispMap.get(iPos).toString()).intValue();
                if (dispSequence > Integer.valueOf(seiseki[13]).intValue()) {
                    // 表示順序格納
                    dispMap.put(iPos, seiseki[13]);
                    //↑--------------------------------------------------------------------------------------

                    lineMap.put((iPos * SEISEKISET + HEADLENGTH + 1), seiseki[8]);
                    lineMap.put((iPos * SEISEKISET + HEADLENGTH + 2), seiseki[9]);
                    lineMap.put((iPos * SEISEKISET + HEADLENGTH + 3), seiseki[INDEX3]);
                    lineMap.put((iPos * SEISEKISET + HEADLENGTH + 4), seiseki[11]);
                    lineMap.put((iPos * SEISEKISET + HEADLENGTH + 5), seiseki[12]);
                }
            }

            //マーク模試時のみ表示
            if (this.mark) {
                // [2004/10/29 nino] マーク模試時の科目コード引き当ては３桁のままでＯＫ
                // [2004/10/25 nino] seiseki[10]はgetDouble()で取得  古文換算得点、漢文換算得点は整数値

                String value;
                if (this.cr)
                    value = seiseki[11]; // センタリサーチは得点
                else
                    value = seiseki[10]; // それ以外は換算得点

                //3270,3670
                if ("327".equals(subcd) || "367".equals(subcd) || "322".equals(subcd)) {
                    if (value == null) {
                        lineMap.put(119, null);
                    } else {
                        lineMap.put(119, String.valueOf((int) Double.valueOf(value).doubleValue()));
                    }
                }
                //3370,3770
                if ("337".equals(subcd) || "377".equals(subcd) || "332".equals(subcd)) {
                    if (value == null) {
                        lineMap.put(120, null);
                    } else {
                        lineMap.put(120, String.valueOf((int) Double.valueOf(value).doubleValue()));
                    }
                }
                //3470,3870
                if ("347".equals(subcd) || "387".equals(subcd) || "321".equals(subcd)) {
                    // 現古は全国偏差値項目(seiseki[12])をセットする
                    //lineMap.put(new Integer("121"), seiseki[10]);
                    lineMap.put(121, seiseki[12]);
                }

            }
        }

        Iterator candIt = candList.iterator();
        while (candIt.hasNext()) {
            String[] cand = (String[]) candIt.next();
            String candcd = cand[1]; // 志望順を取得
            int iCandPos = Integer.parseInt(candcd) - 1; // 表示位置をkeyにMapを格納
            lineMap.put((iCandPos * CANDSET + CANDIDATEPOS), cand[2]);
            lineMap.put((iCandPos * CANDSET + CANDIDATEPOS + 1), cand[3]);
            lineMap.put((iCandPos * CANDSET + CANDIDATEPOS + 2), cand[6]);
            lineMap.put((iCandPos * CANDSET + CANDIDATEPOS + 3), cand[7]);
            lineMap.put((iCandPos * CANDSET + CANDIDATEPOS + 4), cand[8]);
            lineMap.put((iCandPos * CANDSET + CANDIDATEPOS + 5), cand[9]);
            lineMap.put((iCandPos * CANDSET + CANDIDATEPOS + 6), cand[10]);
        }

        // 不足分のkeyをセット
        setLackKey(lineMap, 184);
        // Mapをソート

        // 行情報を追加
        returnList.add(lineMap);
    }

    /**
     * 模試科目コードより科目表示位置(1〜22)を取得
     * @param orderStr  表示順情報 [0]:ダミー  [1]〜[22]:科目1〜科目22
     * @param subCD     科目コード３桁
     * @param staPos    検索開始位置
     * @return
     */
    protected int getSubjectPos(String[][] orderStr, String subCD, int staPos) {
        int iPos = staPos;
        boolean judge = false;
        while (iPos < orderStr.length) {
            for (int i = 0; i < orderStr[iPos].length; i++) {
                if (subCD.equals(orderStr[iPos][i])) {
                    judge = true;
                    break;
                }
            }
            if (judge) {
                break;
            }
            iPos++;
        }
        return iPos;
    }

    /**
     * 不足しているkeyをセット
     * @param lineMap
     */
    protected void setLackKey(LinkedHashMap<Integer, String> lineMap, int count) {
        for (int i = 0; i < count; i++) {
            if (!lineMap.containsKey((i + 1))) {
                lineMap.put((i + 1), new String(""));
            }
        }
    }

    /**
     * 志望校情報を取得
     */
    private LinkedHashMap getCandidate() throws SQLException {

        LinkedHashMap candMap = new LinkedHashMap();

        PreparedStatement ps = null;
        ResultSet rs = null;
        Query query = null;
        try {
            query = QueryLoader.getInstance().load("t02");

            if ("1".equals(exam.getMasterDiv())) {
                query.replaceAll("univmaster_basic", "H12_UNIVMASTER");
                //				query.replaceAll("u.univcd", "u.collegecd");
                query.replaceAll("u.unigdiv", "NULL");
                //				query.replaceAll("u.univname_abbr", "u.collegename_abbr");
            }

            ps = conn.prepareStatement(query.toString());
            ps.setString(1, login.getUserID());
            ps.setString(2, exam.getExamYear());
            ps.setString(3, exam.getExamCD());
            ps.setString(4, exam.getExamDiv());

            rs = ps.executeQuery();
            while (rs.next()) {

                String indID = rs.getString(1); // 個人ID
                List candDataList = null; // 模試リスト

                // 模試マップから年度をキーとして模試リストを取得する
                if (candMap.containsKey(indID)) {
                    candDataList = (List) candMap.get(indID);
                    // なければ作る
                } else {
                    candDataList = new LinkedList();
                    candMap.put(indID, candDataList);
                }

                String[] data = new String[12];
                data[0] = rs.getString(1); // 個人ID
                data[1] = rs.getString(2); // 志望ランク
                data[2] = rs.getString(3); // 大学コード
                data[4] = rs.getString(5); // 学部コード
                data[5] = rs.getString(6); // 学科コード

                // 大学名
                // 新形式は短縮名
                if (this instanceof OutputExamSeiseki2 || this instanceof OutputExamSeiseki3) {

                    final StringBuffer buff = new StringBuffer();

                    // 大学名
                    buff.append(rightPad(rs.getString(14), 7));
                    // 学部名
                    buff.append(rightPad(rs.getString(15), 5));
                    // 学科名
                    buff.append(rightPad(rs.getString(16), 6));

                    data[3] = buff.toString();
                    // 旧形式は超短縮名
                } else {
                    data[3] = rs.getString(4);
                }

                // 日程コード
                String scheduleCd = rs.getString(13);

                // マーク系かつ日程コードが "C", "D", "E", "1" なら換算得点
                // 2010.11.15 "N"も対象に追加
                if (this.mark && ("C".equals(scheduleCd) || "D".equals(scheduleCd) || "E".equals(scheduleCd) || "N".equals(scheduleCd) || "1".equals(scheduleCd))) {

                    data[6] = rs.getString(7);

                    // そうでなければ評価偏差値
                } else {
                    if (rs.getString(8) == null)
                        data[6] = "";
                    else
                        data[6] = String.valueOf(rs.getDouble(8));
                }

                data[7] = rs.getString(9); // 一時評価
                data[8] = rs.getString(10); // 二次評価
                data[9] = rs.getString(11); // 総合評価ポイント
                data[10] = rs.getString(12); // 総合評価
                data[11] = rs.getString(17); // 大学コード(5桁)
                candDataList.add(data);
            }

        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(ps);
        }
        return candMap;
    }

    private String rightPad(final String value, final int size) {
        return value == null ? StringUtils.rightPad("", size, '　') : StringUtils.rightPad(value, size, '　');
    }

    /**
     * 模試成績データを取得
     * @return
     */
    private LinkedHashMap getExamSeiseki() throws SQLException {

        LinkedHashMap seisekiMap = new LinkedHashMap();

        // 受験学力測定テストかどうか
        final boolean isAbilityExam = KNUtil.isAbilityExam(exam);

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(QueryLoader.getInstance().load("t03").toString());
            ps.setString(1, login.getUserID());
            ps.setString(2, exam.getExamYear());
            ps.setString(3, exam.getExamCD());
            rs = ps.executeQuery();

            while (rs.next()) {
                String indID = rs.getString(1); // 個人ID
                List seisekiDataList = null; // 模試リスト

                // 模試マップから年度をキーとして模試リストを取得する
                if (seisekiMap.containsKey(indID)) {
                    seisekiDataList = (List) seisekiMap.get(indID);
                    // なければ作る
                } else {
                    seisekiDataList = new LinkedList();
                    seisekiMap.put(indID, seisekiDataList);
                }

                // [nino 2004/10/25] 配点／換算得点、全国偏差値をgetDouble()で取得

                String[] data = new String[19];
                data[0] = rs.getString(1); // 個人ID
                data[1] = rs.getString(2); // 対象年度
                data[2] = rs.getString(3); // 模試コード
                data[3] = rs.getString(4); // 学校コード
                data[4] = rs.getString(5); // 学年
                data[5] = rs.getString(6); // クラス
                data[6] = rs.getString(7); // クラス番号
                data[7] = rs.getString(8); // 名前（カナ）
                if (rs.getString("EXAMTYPE") == null) {
                    data[8] = ""; // 受験型
                } else {
                    data[8] = rs.getString("EXAMTYPE"); // 受験型
                }
                if (rs.getString("BUNRICODE") == null) {
                    data[9] = ""; // 文理コード
                } else {
                    data[9] = rs.getString("BUNRICODE"); // 文理コード
                }
                data[10] = rs.getString(9); // 科目コード
                data[11] = rs.getString(10); // 科目名
                data[12] = rs.getString(12); // 換算得点
                data[13] = rs.getString(13); // 得点

                // NULLは空文字列
                if (rs.getString(14) == null) {
                    data[14] = "";
                    // 受験学力測定テストはセンター到達エリア
                } else if (isAbilityExam) {
                    data[14] = Integer.toString((int) (Math.round(rs.getFloat(14) * 10)));
                    // それ以外は全国偏差値
                } else {
                    data[14] = rs.getString(14);
                }

                data[15] = rs.getString(16); // 表示順序
                data[16] = rs.getString(11); // 配点
                data[17] = rs.getString(17); // 範囲区分
                data[18] = rs.getString(15); // 学力レベル

                seisekiDataList.add(data);
            }
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(ps);
        }

        return seisekiMap;
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
     */
    protected String getCategory() {
        return null;
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
     */
    protected boolean createSheet() {
        return true;
    }

}

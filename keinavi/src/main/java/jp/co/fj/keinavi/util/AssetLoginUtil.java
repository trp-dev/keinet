package jp.co.fj.keinavi.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;

/**
 *
 * 資産認証のユーティリティです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class AssetLoginUtil {

    /** 資産認証クッキーの名前 */
    private static final String COOKIE_NAME = "keinavi-asset";

    /** ハッシュ値計算用Salt */
    private static final String SALT = "xQ7P%z)!Kn$b";

    /** タイムスタンプとハッシュ値のセパレータ文字 */
    private static final String SEPARATOR = "_";

    /**
     * コンストラクタです。
     */
    private AssetLoginUtil() {
    }

    /**
     * 資産認証クッキーを発行します。
     *
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     */
    public static void publishCookie(HttpServletRequest request, HttpServletResponse response) {
        if (request.getAttribute(COOKIE_NAME) == null) {
            String ts = Long.toString(System.currentTimeMillis());
            response.addCookie(new Cookie(COOKIE_NAME, ts + SEPARATOR + toHash(ts)));
            /* 1回のリクエストで1度だけ発行するようにフラグを記録する */
            request.setAttribute(COOKIE_NAME, Boolean.TRUE);
        }
    }

    /**
     * 文字列の改ざん検知用ハッシュ値を計算します。
     *
     * @param str 文字列
     * @return ハッシュ値
     */
    private static String toHash(String str) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        byte[] sha256;
        try {
            sha256 = digest.digest((str + SALT).getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        return new String(Hex.encodeHex(sha256));
    }

    /**
     * 資産認証クッキーの存在チェックを行います。
     *
     * @param request {@link HttpServletRequest}
     * @return 資産認証クッキーが存在しているならtrue
     */
    public static boolean checkCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                if (COOKIE_NAME.equals(cookies[i].getName())) {
                    String[] values = StringUtils.split(cookies[i].getValue(), SEPARATOR);
                    if (values == null || values.length != 2 || values[0] == null || values[1] == null) {
                        return false;
                    }
                    if (!values[1].equals(toHash(values[0]))) {
                        return false;
                    }
                    try {
                        return System.currentTimeMillis() - Long.parseLong(values[0]) < 60 * 60 * 1000;
                    } catch (NumberFormatException e) {
                        return false;
                    }
                }
            }
        }
        return false;
    }

}

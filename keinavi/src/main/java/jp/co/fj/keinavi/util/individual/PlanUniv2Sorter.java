/*
 * 作成日: 2004/10/29
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.util.individual;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import jp.co.fj.keinavi.data.individual.PlanUnivData2;

/**
 * @author Administrator
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class PlanUniv2Sorter {
	
	public class SortByExamPlanDateWCenter implements Comparator{

		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 * 日程コードが１
		 */
		public int compare(Object o1, Object o2) {
			PlanUnivData2 d1 = (PlanUnivData2) o1;
			PlanUnivData2 d2 = (PlanUnivData2) o2;
			if(d1.getScheduleCd().equals("1") && !d2.getScheduleCd().equals("1")){
				return -1;
			}else if(!d1.getScheduleCd().equals("1") && d2.getScheduleCd().equals("1")){
				return 1;
			}else{
				return new SortByExamPlanDate().compare(d1, d2);
			}
		}
	}

	public class SortByExamPlanDate implements Comparator{

		/* (非 Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 * 受験日の初日
		 */
		public int compare(Object o1, Object o2) {
			PlanUnivData2 d1 = (PlanUnivData2) o1;
			PlanUnivData2 d2 = (PlanUnivData2) o2;
			//日程コードが１のものは受験日ではソートしない
			if(d1.getScheduleCd().equals("1") && !d2.getScheduleCd().equals("1")){
				return -1;
			}
			else if(!d1.getScheduleCd().equals("1") && d2.getScheduleCd().equals("1")){
				return 1;
			}
			//両方日程コードが１
			else if(d1.getScheduleCd().equals("1") && d2.getScheduleCd().equals("1")){
				return compareUnivCd(d1,d2);
			}
			//両方日程コードが１じゃない
			else{
				int firstDate1 = getFirstDate(d1);
				int firstDate2 = getFirstDate(d2);
				if(firstDate1 == -1 && firstDate2 == -1){
					return compareUnivCd(d1,d2);
					//return 0;
				}else if(firstDate1 != -1 && firstDate2 == -1){
					return -1;
				}else if(firstDate1 == -1 && firstDate2 != -1){
					return 1;
				}else{
					if(firstDate1 < firstDate2){
						return -1;
					}else if(firstDate1 > firstDate2){
						return 1;
					}else{
						return compareUnivCd(d1,d2);
						//return 0;
					}
				}
			}
		}
		private int getFirstDate(PlanUnivData2 pud){
			int firstDate = -1;
			if(pud != null){
				Map map = pud.getPlanMap();
				if(map != null && map.size() > 0){
					Set set = map.entrySet();
					Iterator ite = set.iterator();
					while(ite.hasNext()){
						Map.Entry me = (Map.Entry) ite.next();
						PlanUnivData2 data = (PlanUnivData2) me.getValue();
						int[] examDates = data.getExamPlanDate();
						if(examDates != null && examDates.length > 0){
							for(int i=0; i<examDates.length; i++){
								int tempDate = examDates[i];
								//１〜３月は特別扱い
								if(((int)tempDate / 100) == 1 || ((int)tempDate / 100) == 2 || ((int)tempDate / 100) == 3){
									tempDate += 1200;
								}
								if(i == 0 || tempDate < firstDate){
									firstDate = tempDate;//examDates[i];
								}
							}
						}
					}
				}
			}
			return firstDate;
		}
		private int compareUnivCd(PlanUnivData2 d1, PlanUnivData2 d2){
			/* 国私区分で並べ替える */
			int result = d1.getNatlPvtDiv().compareTo(d2.getNatlPvtDiv());
			if (result == 0) {
				/* カナ付50音順番号〜学科コードで並べ替える */
				result = KanaNumComparator.getInstance().compare(d1, d2);
				if (result == 0) {
					/* 入試形態で並べ替える */
					return new Integer(d1.getEntExamModeCd()).compareTo(new Integer(d2.getEntExamModeCd()));
				}
			}
			return result;
		}
		
	}
}
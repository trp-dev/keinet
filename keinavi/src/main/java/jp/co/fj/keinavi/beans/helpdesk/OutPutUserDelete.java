/*
 * 作成日: 2015/10/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.helpdesk;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import jp.co.fj.keinavi.beans.maintenance.MainteLogBean;
import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;

/**
 * ユーザ管理（ユーザ削除用TSV出力処理）
 *
 * @author Hiroyuki Nishiyama - QuiQsoft
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class OutPutUserDelete extends AbstractSheetBean {

	// ファイル分割単位
	private int divNum = 100;

	// ヘッダー情報
	private List header = null;
	// 出力テキスト種別
	private int outType = 0;
	// 出力対象項目
	private Integer target[] = null;

	// 出力ファイル
	private File outPutFile = null;
	// コネクション
	private Connection con;

	// ログ出力モジュール
	private final MainteLogBean mainteLog = new MainteLogBean("HELP");

	// コンストラクタ
	public OutPutUserDelete(){

	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return true;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		return "";
	}

	public void execute()throws IOException, SQLException, Exception{

		int fileCnt = 0;

		// コネクション設定
		con = super.conn;
		mainteLog.setConnection(null, conn);

		try {
			OutPutParamSet set = new OutPutParamSet(login, sessionKey, "hd101", "2");

			// パラメータマスタを取得
			StringBuffer conPrmSelSb = new StringBuffer();
			conPrmSelSb.append(" SELECT T1.SETTINGVALUE1");
			conPrmSelSb.append("   FROM CONTRACT_PARAM T1");
			conPrmSelSb.append("  WHERE 1=1");
			conPrmSelSb.append("    AND T1.PARAJUDGECD = ?");
			conPrmSelSb.append("    AND T1.PARAKEY     = ?");
			conPrmSelSb.append(" ORDER BY");
			conPrmSelSb.append("        T1.PARAKEY");
			String div = selectContractParam(conPrmSelSb, "00002", "00001");
			String userIdPreFix = selectContractParam(conPrmSelSb, "00003", "00001");

			// 分割単位を設定
			try {
				divNum = Integer.parseInt(div);
			} catch (NumberFormatException nEx) {
				divNum = 100;
			}

			// ファイル分割単位設定
			set.setDivNum(divNum);
			outType =set.getOutType();
			target = set.getTarget();

			// パラメータ設定
			OutputTsvBean out = new OutputTsvBean();
			out.setOutType(outType);      // 出力形式
			out.setOutTarget(target);     // 出力項目
			out.setOutputTextList(SelectDeleteUser(userIdPreFix)); // データ取得
			out.setHeadTextList(header);  // ヘッダー設定
			out.setDivNum(divNum);       // ファイル分割単位設定

			// 処理対象でループ
			for (int i = 0; i < out.getOutputTextList().size(); i++) {

				// ファイル分割
				// 分割単位に達した or 最終レコード
				if ( (i + 1) % divNum  == 0 || i >= out.getOutputTextList().size() - 1) {
					fileCnt++;

					// ファイルオブジェクト作成
					set.setDate(new Date());
					set.createOutPutFile(fileCnt);
					outPutFile = set.getOutPutFile();
					out.setOutFile(outPutFile);

					// ファイル出力
					out.execute();
					this.outfileList.add(outPutFile.getPath());
					sheetLog.add(outPutFile.getName());

				}

			}

			// ログ出力
			mainteLog.etcLog("ユーザ削除：ユーザ削除用ファイルを" + out.getOutputTextList().size() + "件出力");

		} catch (Exception e) {
			con.rollback();
			throw e;
		} finally {
			con.commit();
		}

	}
	/**
	 *
	 * 契約校マスタから削除対象ユーザを取得する
	 *
	 * @param userIdPreFix ユーザIDプレフィックス
	 * @return 取得結果
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private ArrayList SelectDeleteUser(String userIdPreFix) throws SQLException {

		// 返却値
		ArrayList list = new ArrayList();
		LinkedHashMap data = null;

		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer delUseSb = new StringBuffer();

		// 日付
		String fromDate = "";
		String toDate = "";

		try {

			// 日付取得用
			Date dt = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);

			// 去年の５月１日
			fromDate = String.valueOf(cal.get(Calendar.YEAR)  - 1) + "0501";
			// 今年の４月末
			toDate = String.valueOf(cal.get(Calendar.YEAR)) + "0430";

			// SELECT文作成
			delUseSb.setLength(0);
			delUseSb.append(" SELECT T1.USERID");
			delUseSb.append("   FROM PACTSCHOOL T1");
			delUseSb.append("  WHERE 1=1");
			delUseSb.append("    AND (");
										// 期限切れ
			delUseSb.append("          (");
			delUseSb.append("           T1.ISSUED_BEFOREDATE BETWEEN ? AND ?");
			delUseSb.append("          )");
			delUseSb.append("        OR EXISTS");
										// 私書箱機能提供フラグが 1 ⇒ 0
			delUseSb.append("          (SELECT 1");
			delUseSb.append("             FROM PACTSCHOOLSAVE T2");
			delUseSb.append("            WHERE 1=1");
			delUseSb.append("              AND T1.FEATPROVFLG = '0'");
			delUseSb.append("              AND T2.FEATPROVFLG = '1'");
			delUseSb.append("              AND T1.USERID      = T2.USERID");
			delUseSb.append("          )");
			delUseSb.append("        )");
			delUseSb.append(" ORDER BY");
			delUseSb.append("        T1.USERID");

			ps = con.prepareStatement(delUseSb.toString());
			ps.setString(1, fromDate);
			ps.setString(2, toDate);

			rs = ps.executeQuery();
			while (rs.next()) {
				data = new LinkedHashMap();
				//data.put(new Integer("1"), rs.getString(1));
				data.put(new Integer("1"), userIdPreFix + rs.getString(1));
				list.add(data);
			}

		} catch (SQLException ex) {
			throw new SQLException("契約校マスタの取得に失敗しました。");
		} catch (Exception ex) {
			throw new SQLException("契約校マスタの取得に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
		}
		return list;
	}

	/**
	 *
	 * 証明書発行マスタからパラメータを取得する
	 *
	 * @param sb           SQL文
	 * @param paraJudgeCd  パラメータ識別コード
	 * @param paraKey      パラメータキー
	 * @return 取得結果
	 * @throws java.sql.SQLException 　　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	private String selectContractParam(StringBuffer sb, String paraJudgeCd, String paraKey)
		throws SQLException {

		String retVal = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			stmt = con.prepareStatement(sb.toString());
			stmt.setString(1, paraJudgeCd);
			stmt.setString(2, paraKey);

			rs = stmt.executeQuery();
			while (rs.next()) {
				retVal = rs.getString(1);
			}

			if (retVal == null) retVal = "";

		} catch (SQLException ex) {
			throw new SQLException("証明書発行マスタの取得に失敗しました。");
		} catch (Exception ex) {
			throw new SQLException("証明書発行マスタの取得に失敗しました。");
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return retVal;
	}
}

package jp.co.fj.keinavi.beans.profile;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.com_set.CompSchoolData;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 標準プロファイルBean
 *
 * 2006.07.25	[新規作成]
 *
 * <大規模改修>
 * 2015.12.17 Hiroyuki Nishiyama - QuiQsoft
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class StandardProfileBean extends DefaultBean
		implements IProfileCategory, IProfileItem {

	//2019/07/26 QQ)Nagai 共通テスト対応 UPD START
	//// 2015/12/17 QQ)Nishiyama 大規模改修 UPD START
	////private static final String BASE_PROFILE_NAME = "ひな型１（マーク模試標準）";
	//private static final String BASE_PROFILE_NAME = "ひな型０（マーク模試標準）";
	//// 2015/12/17 QQ)Nishiyama 大規模改修 UPD END
	private static final String BASE_PROFILE_NAME = "ひな型０（共通テスト模試標準）";
	//2019/07/26 QQ)Nagai 共通テスト対応 UPD END
	private static final String BASE_FOLDER_NAME = "ひな型プロファイル";

	// ログイン情報
	private LoginSession loginSession;
	// 一括コード
	private String bundleCd;
	// 標準プロファイルID
	private String profileId;

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// ひな型プロファイルのプロファイルID
		profileId = getTemplateProfileId();

		// NULLなら作る
		if (profileId == null) {
			profileId = createTemplateProfile();
		}
	}

	// ひな型プロファイルのプロファイルIDを取得する
	private String getTemplateProfileId() throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("w11"));
			ps.setString(1, BASE_FOLDER_NAME);
			ps.setString(2, BASE_PROFILE_NAME);
			ps.setString(3, bundleCd);

			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getString(1);
			} else {
				return null;
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	// ひな型フォルダのフォルダIDを取得する
	private String getTemplateFolderId() throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					"SELECT profilefid FROM profilefolder "
					+ "WHERE profilefname = ? AND bundlecd = ? "
					+ "ORDER BY renewaldate DESC");

			ps.setString(1, BASE_FOLDER_NAME);
			ps.setString(2, bundleCd);

			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getString(1);
			} else {
				return null;
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	// ひな型プロファイルを作る
	private String createTemplateProfile() throws Exception {

		final Profile profile = getDefaultProfile();

		// 作成者は河合塾
		profile.setUserID(null);
		// プロファイル名
		profile.setProfileName(BASE_PROFILE_NAME);
		// 代行でも公開プロファイルとする
		profile.setSectorSortingCD(null);

		//・比較対象年度：　直近の４年分
		profile.getItemMap(CM).put(
				PREV_COMP_YEAR, new String[]{"1", "2", "3", "4"});

		//・比較対象高校：　１校（全国）　【グラフ表示対象でもある】
		//※比較対象高校以外については、グラフ表示対象はなし。
		final List list = new ArrayList();

		CompSchoolData data = new CompSchoolData();
		data.setSchoolCD("99999");
		data.setGraphDisp((short) 1);
		list.add(data);

		profile.getItemMap(CM).put(
				SCHOOL_COMP_SCHOOL, list);

		// ひな型フォルダID
		final String folderId = getTemplateFolderId();

		// なければ作る
		if (folderId == null) {
			profile.setFolderID(createTemplateFolder());
		} else {
			profile.setFolderID(folderId);
		}

		ProfileUpdateBean bean = new ProfileUpdateBean();
		bean.setConnection(null, conn);
		bean.setProfile(profile);
		bean.setLoginSession(loginSession);
		bean.setStandard();
		bean.execute();

		return profile.getProfileID();
	}

	// デフォルトプロファイルを取得する
	private Profile getDefaultProfile() throws Exception {

		ProfileBean bean = new ProfileBean();
		bean.setLoginSession(loginSession);
		bean.setBundleCD(bundleCd);
		bean.execute();
		return bean.getProfile();
	}

	// ひな型フォルダを作る
	private String createTemplateFolder() throws Exception {

		ProfileFolderUpdateBean bean = new ProfileFolderUpdateBean();
		bean.setConnection(null, conn);
		bean.setFolderID("");
		bean.setFolderName(BASE_FOLDER_NAME);
		bean.setComment("");
		bean.setLoginSession(loginSession);
		bean.setBundleCD(bundleCd);
		bean.setStandard();
		bean.execute();

		// 営業代行の場合は非公開フォルダとなるので公開する
		if (loginSession.isDeputy()) {
			bean.publish();
		}

		return bean.getFolderID();
	}

	/**
	 * @param pBundleCd 設定する bundleCd。
	 */
	public void setBundleCd(String pBundleCd) {
		bundleCd = pBundleCd;
	}

	/**
	 * @param pLoginSession 設定する loginSession。
	 */
	public void setLoginSession(LoginSession pLoginSession) {
		loginSession = pLoginSession;
	}

	/**
	 * @return profileId を戻します。
	 */
	public String getProfileId() {
		return profileId;
	}

}

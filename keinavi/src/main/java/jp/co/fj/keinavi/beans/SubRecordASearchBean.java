package jp.co.fj.keinavi.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.kawaijuku.judgement.data.Examination;
import jp.co.fj.keinavi.data.SubRecordAData;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import com.fjh.beans.DefaultSearchBean;

/**
 *
 * 科目別成績（全国）取得
 *
 *
 * <2010年度改修>
 * 2010.04.08	Tomohisa YAMADA - Totec
 * 				新規作成
 * 
 * 
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 */
public class SubRecordASearchBean extends DefaultSearchBean {

	//模試年度
	private String examYear;
	//模試コード
	private String examCd;
	//現役高卒区分
	private String studentDiv;
	
	/**
	 * コンストラクタ
	 * @param year 模試年度
	 * @param code 模試コード
	 * @param studentDiv 現役高卒区分
	 */
	public SubRecordASearchBean(String year, String code, String studentDiv) {
		this.examYear = year;
		this.examCd = code;
		this.studentDiv = studentDiv;
	}
	
	/**
	 * 実行
	 */
	public void execute() throws SQLException, Exception {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			
			Query query = QueryLoader.getInstance().load("search_subrecord_a");
			
			//現役高卒区分が存在すれば
			//条件として設定する
			if (studentDiv != null) {
				query.append(" AND a.studentdiv = '" + studentDiv +"' ");
			}
			
			ps = conn.prepareStatement(query.toString());
			
			ps.setString(1, examYear);
			ps.setString(2, examCd);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				
				recordSet.add(
						new SubRecordAData(
							rs.getString("examyear"), 
							rs.getString("examcd"), 
							rs.getString("subcd"),
							rs.getString("studentdiv"), 
							rs.getDouble("avgpnt"), 
							rs.getDouble("avgdeviation"), 
							rs.getDouble("avgscorerate"),
							rs.getDouble("stddeviation"), 
							rs.getInt("highestpnt"), 
							rs.getInt("lowestpnt"), 
							rs.getInt("numbers")));
			}
			
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	/**
	 * 科目別成績（全国）を取得する
	 * ※試験が存在しない場合はnullを返す
	 * @param markExam マーク模試
	 * @param con ＤＢコネクション
	 * @return 科目別成績（全国）
	 * @throws Exception
	 */
	public static List searchNullSet(
			Examination exam, Connection con) throws Exception {
		
		//マーク系科目別成績（全国）
        List subrecas = null;
        
        //マーク模試が存在する場合は
        //科目別成績（全国）を取得
        if (exam != null) {		
			SubRecordASearchBean sras = 
				new SubRecordASearchBean(
						exam.getExamYear(), 
						exam.getExamCd(),
						"0");//現役高卒区分は固定で「0」を設定
			sras.setConnection("", con);
			sras.execute();
			
			subrecas = sras.getRecordSet();
        }
        
        return subrecas;
	}

}

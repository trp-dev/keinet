package jp.co.fj.keinavi.beans.sheet.school.data;

import java.util.List;

/**
 *
 * 科目リストデータ
 * 
 * 2007.07.26	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class S52_11SubjectListData extends S42_15SubjectListData {
	
	/**
	 * @param data 追加する過年度リストデータ
	 */
	public void addExamListData(final S52_11ExamListData data) {
		data.setSubjectListData(this);
		getBundleListData().add(data);
	}

	/**
	 * @return yearListData
	 */
	public List getExamListData() {
		return getBundleListData();
	}
	
}

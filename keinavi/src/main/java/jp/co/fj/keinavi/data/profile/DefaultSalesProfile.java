/*
 * 作成日: 2004/07/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.profile;

import java.util.ArrayList;

import jp.co.fj.keinavi.interfaces.IProfileCategory;
import jp.co.fj.keinavi.interfaces.IProfileItem;


/**
 * @author kawai
 *
 * 営業部用のプロファイルの初期値を保持するクラス
 */
public class DefaultSalesProfile extends AbstractDefaultProfile {

	/**
	 * カテゴリマップに初期値をセットする
	 */
	public DefaultSalesProfile() {
		// 1.共通項目設定
		getItemMap(IProfileCategory.CM).put(IProfileItem.TYPE_SELECTION, new Short("2"));
		getItemMap(IProfileCategory.CM).put(IProfileItem.COURSE_SELECTION, new Short("2"));
		getItemMap(IProfileCategory.CM).put(IProfileItem.UNIV_SELECTION_FORMULA, new Short("2"));
		getItemMap(IProfileCategory.CM).put(IProfileItem.TYPE_COMMON, new ArrayList());
		getItemMap(IProfileCategory.CM).put(IProfileItem.TYPE_IND, new ArrayList());
		getItemMap(IProfileCategory.CM).put(IProfileItem.COURSE_COMMON, new ArrayList());
		getItemMap(IProfileCategory.CM).put(IProfileItem.COURSE_IND, new ArrayList());
		// 2.校内成績分析
		// 高校間比較（成績概況）
		setPrintOutTarget(IProfileCategory.S_OTHER_SCORE);
		setType(IProfileCategory.S_OTHER_SCORE);
		setCourse(IProfileCategory.S_OTHER_SCORE);
		getItemMap(IProfileCategory.S_OTHER_SCORE).put(IProfileItem.CHART, new Short("1"));
		getItemMap(IProfileCategory.S_OTHER_SCORE).put(IProfileItem.IND_SCORE_RANK, new Short("0"));
		getItemMap(IProfileCategory.S_OTHER_SCORE).put(IProfileItem.PREV_DISP, new Short("1"));
		getItemMap(IProfileCategory.S_OTHER_SCORE).put(IProfileItem.IND_TOTAL_AVG, new Short("1"));
		getItemMap(IProfileCategory.S_OTHER_SCORE).put(IProfileItem.IND_ACTIVE_AVG, new Short("1"));
		getItemMap(IProfileCategory.S_OTHER_SCORE).put(IProfileItem.IND_GRAD_AVG, new Short("1"));
		getItemMap(IProfileCategory.S_OTHER_SCORE).put(IProfileItem.IND_SCHOOL_NUM, new Short("1"));
		getItemMap(IProfileCategory.S_OTHER_SCORE).put(IProfileItem.SCHOOL_ORDER, new Short("1"));
		getItemMap(IProfileCategory.S_OTHER_SCORE).put(IProfileItem.IND_UPPER_MARK, new Short("1"));
		setStamp(IProfileCategory.S_OTHER_SCORE);
		// 高校間比較（偏差値分布）
		setPrintOutTarget(IProfileCategory.S_OTHER_DEV);
		setType(IProfileCategory.S_OTHER_DEV);
		setCourse(IProfileCategory.S_OTHER_DEV);
		getItemMap(IProfileCategory.S_OTHER_DEV).put(IProfileItem.CHART_COMP_RATIO, new Short("12"));
		getItemMap(IProfileCategory.S_OTHER_DEV).put(IProfileItem.PREV_DISP, new Short("1"));
		getItemMap(IProfileCategory.S_OTHER_DEV).put(IProfileItem.SCHOOL_ORDER, new Short("1"));
		setStamp(IProfileCategory.S_OTHER_DEV);
		// 高校間比較（設問別成績）
		setPrintOutTarget(IProfileCategory.S_OTHER_QUE);
		setCourse(IProfileCategory.S_OTHER_QUE);
		getItemMap(IProfileCategory.S_OTHER_QUE).put(IProfileItem.SCHOOL_ORDER, new Short("1"));
		getItemMap(IProfileCategory.S_OTHER_QUE).put(IProfileItem.IND_UPPER_MARK, new Short("1"));
		setStamp(IProfileCategory.S_OTHER_QUE);
		// 高校間比較（志望大学評価別人数）
		setPrintOutTarget(IProfileCategory.S_OTHER_UNIV);
		getItemMap(IProfileCategory.S_OTHER_UNIV).put(IProfileItem.PREV_DISP, new Short("1"));
		getItemMap(IProfileCategory.S_OTHER_UNIV).put(IProfileItem.UNIV_ORDER, new Short("1"));
		getItemMap(IProfileCategory.S_OTHER_UNIV).put(IProfileItem.UNIV_COUNTING_DIV, new Short("3"));
		getItemMap(IProfileCategory.S_OTHER_UNIV).put(IProfileItem.SCHOOL_ORDER, new Short("1"));
		getItemMap(IProfileCategory.S_OTHER_UNIV).put(IProfileItem.UNIV_RATING, new String[]{"1", "2", "3"});
		setStamp(IProfileCategory.S_OTHER_UNIV);
		// 過回比較（成績概況）
		setPrintOutTarget(IProfileCategory.S_PAST_SCORE);
		setType(IProfileCategory.S_PAST_SCORE);
		setCourse(IProfileCategory.S_PAST_SCORE);
		getItemMap(IProfileCategory.S_PAST_SCORE).put(IProfileItem.SCHOOL_ORDER, new Short("1"));
		setStamp(IProfileCategory.S_PAST_SCORE);
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.data.profile.AbstractDefaultProfile#setStamp(java.lang.String)
	 */
	protected void setStamp(String category) {
		getItemMap(category).put(IProfileItem.PRINT_STAMP, new Short("14"));
	}

}

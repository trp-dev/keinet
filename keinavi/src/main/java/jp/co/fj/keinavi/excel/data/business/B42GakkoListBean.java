package jp.co.fj.keinavi.excel.data.business;

//import org.apache.struts.action.ActionForm;
import java.util.ArrayList;
/**
 * �Z�����ѕ��́|���Z��r�|�΍��l���z �w�Z���X�g
 * �쐬��: 2004/07/19
 * @author	A.Iwata
 */
public class B42GakkoListBean {
	//�w�Z��
	private String strGakkomei = "";
	//�͋[�N�x
	private String strNendo = "";
	//�l��
	private int intNinzu = 0;
	//���ϓ_
	private float floHeikin = 0;
	//���ϕ΍��l
	private float floHensa = 0;
	//�΍��l���z���X�g
	private ArrayList b42BnpList = new ArrayList();

	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrNendo() {
		return this.strNendo;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public float getFloHeikin() {
		return this.floHeikin;
	}
	public float getFloHensa() {
		return this.floHensa;
	}
	public ArrayList getB42BnpList() {
		return this.b42BnpList;
	}

	/*----------*/
	/* Set      */
	/*----------*/
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrNendo(String strNendo) {
		this.strNendo = strNendo;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setFloHeikin(float floHeikin) {
		this.floHeikin = floHeikin;
	}
	public void setFloHensa(float floHensa) {
		this.floHensa = floHensa;
	}
	public void setB42BnpList(ArrayList b42BnpList) {
		this.b42BnpList = b42BnpList;
	}
}

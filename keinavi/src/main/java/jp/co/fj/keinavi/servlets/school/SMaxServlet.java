package jp.co.fj.keinavi.servlets.school;

import java.io.IOException;
import java.sql.Connection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.com_set.CompClassBean;
import jp.co.fj.keinavi.beans.com_set.SubjectBean;
import jp.co.fj.keinavi.beans.help.OnepointBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.com_set.ComSubjectData;
import jp.co.fj.keinavi.data.com_set.cm.Subject;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.AbstractActionFormFactory;
import jp.co.fj.keinavi.forms.school.SMaxForm;
import jp.co.fj.keinavi.interfaces.IProfileItem;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.ProfileUtil;

/**
 * 構内成績分析 - 詳細画面サーブレット
 *
 * 2005.02.15 Yoshimoto KAWAI - Totec
 *            対象模試を保持するように変更
 *
 * 2005.04.05	Yoshimoto KAWAI - Totec
 * 				requestへ現年度をセットした
 *
 * 2005.04.20	Yoshimoto KAWAI - Totec
 * 				requestへ新テストフラグをセットした
 *
 * @author kawai
 *
 */
public class SMaxServlet extends AbstractSServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(
	 * 			javax.servlet.http.HttpServletRequest,
	 * 			javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {

		// アクションフォームの取得 - request scope
		SMaxForm form = (SMaxForm) getActionForm(request,
				"jp.co.fj.keinavi.forms.school.SMaxForm");

		// ログイン情報
		final LoginSession login = getLoginSession(request);
		// プロファイル
		final Profile profile = getProfile(request);
		// 模試セッション
		final ExamSession examSession = getExamSession(request);
		// フィルタリングを有効にするためリクエストにセットする
		request.setAttribute(ExamSession.SESSION_KEY, examSession);
		// 対象模試データ
		ExamData exam = examSession.getExamData(form.getTargetYear(), form.getTargetExam());


		// 転送元が詳細画面
		if (detailSet.contains(super.getBackward(request))) {
			// 模試を変更した場合
			if ("1".equals(form.getChangeExam())) {
				// 型の設定があるなら設定値を再ロード
				if (AbstractSServlet.typeSet.contains(super.getForward(request))) {
					Map item = AbstractActionFormFactory
								.getFactory(super.getForward(request))
								.getItemMap(request);

					ComSubjectData data = (ComSubjectData) item.get(IProfileItem.TYPE_IND);

					if (data == null) {
						form.setAnalyzeType(null);
						form.setGraphType(null);
					} else {
						form.setAnalyzeType(ProfileUtil.getSubjectIndValue(data, false));
						form.setGraphType(ProfileUtil.getSubjectIndValue(data, true));
					}
				}

				// 科目の設定があるなら設定値を再ロード
				if (AbstractSServlet.courseSet.contains(super.getForward(request))) {
					Map item = AbstractActionFormFactory
								.getFactory(super.getForward(request))
								.getItemMap(request);

					ComSubjectData data = (ComSubjectData) item.get(IProfileItem.COURSE_IND);

					if (data == null) {
						form.setAnalyzeCourse(null);
						form.setGraphCourse(null);
					} else {
						form.setAnalyzeCourse(ProfileUtil.getSubjectIndValue(data, false));
						form.setGraphCourse(ProfileUtil.getSubjectIndValue(data, true));
					}
				}

				// 対象模試が存在しなければリストの最上位
				if (exam == null) {
					exam = (ExamData) examSession.getExamList(form.getTargetYear()).get(0);
					form.setTargetYear(exam.getExamYear());
					form.setTargetExam(exam.getExamCD());
				}
			}

			// 対象模試を保持する
			// NULLが来る可能性があるので排除
			if (form.getTargetYear() != null) {
				profile.setTargetYear(form.getTargetYear());
				profile.setTargetExam(form.getTargetExam());
			}

		// そうでなければ初期化する
		} else {
			form = (SMaxForm) AbstractActionFormFactory
				.getFactory(getForward(request))
				.createActionForm(request);

			// 対象模試を取得し直す
			exam = examSession.getExamData(form.getTargetYear(), form.getTargetExam());
		}

		// リクエストへアクションフォームをセット
		request.setAttribute("form", form);

		// 詳細画面の画面IDなら遷移する
		if (detailSet.contains(getForward(request))) {
			Connection con = null; // DBコネクション
			try {
				con = getConnectionPool(request);

				//---------------------------------------------------
				// 画面単位のワンポイントアドバイス（画面ID指定）
				//---------------------------------------------------
				OnepointBean onebean = new OnepointBean();
				onebean.setConnection(null, con);
				onebean.setScreenID(super.getForward(request));
				onebean.execute();
				request.setAttribute("OnepointBean", onebean);
				//---------------------------------------------------

				// 型
				{
					SubjectBean bean = new SubjectBean(login);
					bean.setConnection(null, con);
					bean.setMode("1");
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.execute();
					request.setAttribute("TypeBean", bean);

					// 初期値（受験人数が１人以上の型）
					if (form.getAnalyzeType() == null) {
						List container = new LinkedList();
						Iterator ite = bean.getFullList().iterator();
						while (ite.hasNext()) {
							Subject s = (Subject) ite.next();
							if (s.getExaminees() == 0) continue;
							container.add(s.getSubCD());
						}
						form.setAnalyzeType((String[])container.toArray(new String[0]));
					}
				}

				// 科目
				{
					SubjectBean bean = new SubjectBean(login);
					bean.setConnection(null, con);
					bean.setMode("2");
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.execute();
					request.setAttribute("CourseBean", bean);

					// 初期値（受験人数が１人以上の型）
					if (form.getAnalyzeCourse() == null) {
						List container = new LinkedList();
						Iterator ite = bean.getFullList().iterator();
						while (ite.hasNext()) {
							Subject s = (Subject) ite.next();
							if (s.getExaminees() == 0) continue;
							container.add(s.getSubCD());
						}
						form.setAnalyzeCourse((String[])container.toArray(new String[0]));
					}
				}

				// 比較対象クラスBean
				// Taglibでの評価用
				{
					CompClassBean bean = new CompClassBean();
					bean.setConnection(null, con);
					bean.setTargetYear(form.getTargetYear());
					bean.setTargetExam(form.getTargetExam());
					bean.setBundleCD(getBundleCd(login, profile, form.getTargetExam()));
					bean.setSchoolCD(login.getUserID());
					bean.execute();
					request.setAttribute("CompClassBean", bean);
				}

				// 科目カウントBean
				request.setAttribute("SubjectCountBean",
						createSubjectCountBean(
								con, login.getUserID(),
								form.getTargetYear(), form.getTargetExam()));

			} catch (final Exception e) {
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			// リクエストへ対象模試データをセット
			request.setAttribute("ExamData", exam);
			// リクエストへ現年度をセット
			request.setAttribute("CurrentYear", KNUtil.getCurrentYear());
			// 対象模試が新テストかどうか
			request.setAttribute("NewExam",
					Boolean.valueOf(KNUtil.isNewExam(exam)));
			// センターリサーチフラグ
			request.setAttribute("isCenterResearch",
					Boolean.valueOf(KNUtil.isCenterResearch(exam)));
			// 2019/07/18 QQ)Tanioka 共通テスト対応 ADD START
			// プレ共通テスト(旧センタープレ)系フラグ
			request.setAttribute("isCenterP",
					Boolean.valueOf(KNUtil.isCenterP(exam)));
			// プライムステージ(プレステージ)系フラグ
			request.setAttribute("isPStage",
					Boolean.valueOf(KNUtil.isPStage(exam)));
			// マーク系フラグ
			request.setAttribute("isMarkExam",
					Boolean.valueOf(KNUtil.isMarkExam(exam)));
			// 記述系フラグ
			request.setAttribute("isWrtnExam",
					Boolean.valueOf(KNUtil.isWrtnExam(exam)));
			// 2019/07/18 QQ)Tanioka 共通テスト対応 ADD END
// 2019/11/08 QQ)Tanioka 共通テスト対応 ADD START
                        // 記述系2フラグ
                        request.setAttribute("isWrtnExam2",
                                        Boolean.valueOf(KNUtil.isWrtnExam2(exam)));
// 2019/11/08 QQ)Tanioka 共通テスト対応 ADD END
			// 対象模試が受験学力測定テストかどうか
			request.setAttribute("isAbilityExam",
					Boolean.valueOf(KNUtil.isAbilityExam(exam)));
			// 対象模試が高1・2大学マスタ利用かどうか
			request.setAttribute("isUniv12Exam",
					Boolean.valueOf(KNUtil.isUniv12Exam(exam)));
			//「過年度の表示」非表示フラグをセット
			setHideUnivYear(request, exam);

			super.forward(request, response, JSP_SMAX);

		// 不明なら転送
		} else {
			// 保存処理
			if ("1".equals(form.getSave())) {
				AbstractActionFormFactory
					.getFactory(getBackward(request))
					.restore(request);
				profile.setChanged(true);
			}

			// requestスコープの模試セッションは消しておく
			request.removeAttribute(ExamSession.SESSION_KEY);

			super.forward(request, response, SERVLET_DISPATCHER);
		}
	}

}

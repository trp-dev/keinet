/*
 * 作成日: 2004/10/21
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.beans.txt_out;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.AbstractSheetBean;
import jp.co.fj.keinavi.util.sql.QueryLoader;


/**
 * 
 * 個人の設問別成績データテキスト出力
 *
 * @author 二宮淳行
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class OutPutZbf560 extends AbstractSheetBean{

	// 対象年度
	private String targetYear = null;
	// 対象模試
	private String targetExam = null;
	// 学校コード
	private String schoolCd = null;
	// ヘッダー情報
	private List header = null;
	
	// 出力テキスト種別
	private int outType = 0;
	// 出力対象項目
	private Integer target[] = null;

	// 出力ファイル
	private File outPutFile = null;
	// コネクション
	private Connection con;
	
	// 一括コードの配列
	//private String[] bundleCd;
	
	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#createSheet()
	 */
	protected boolean createSheet() {
		return true;
	}

	public void execute()throws IOException, SQLException, Exception{

		// 担当クラスを一時テーブルへ展開する
		super.setupCountChargeClass();
		
		OutPutParamSet set = new OutPutParamSet(login, exam, profile, "t103",sessionKey);
		
		targetYear = set.getTargetYear();
		targetExam = set.getTargetExam();
		schoolCd = set.getSchoolCd();
		header = set.getHeader();
		outType =set.getOutType();
		target = set.getTarget();
		outPutFile =set.getOutPutFile();
		con = super.conn;

//		//------------------------------------------------
//		// 子コードを含んだ学校コードを取得
//		//------------------------------------------------
//		// 一括コードの一覧を集計区分Beanから抽出する
//		{
//			CountingDivBean bean = new CountingDivBean();
//			bean.setConnection(null, con);
//			bean.setSchoolCD(this.schoolCd);
//			bean.execute();
//			bundleCd = bean.getCountingDivCodeArray();
//		}
//		//------------------------------------------------

		OutputTextBean out = new OutputTextBean();
		out.setOutFile(outPutFile);	
		out.setOutputTextList(getZbf560());
		out.setHeadTextList(header);
		out.setOutTarget(target);
		out.setOutType(outType);
		out.execute();

		this.outfileList.add(outPutFile.getPath());
		sheetLog.add(outPutFile.getName());
		/*---- テスト用 ---
		File file = null;
		if(outType ==1) file = new File("D:\\Temp\\zbf560.csv");
		else file = new File("D:\\Temp\\zbf560.txt");
		TextInfoBean txt = new TextInfoBean();
		txt.execute();

		OutputTextBean out = new OutputTextBean();
		out.setOutFile(file);
		out.setOutputTextList(getZbf530());
		out.setHeadTextList(txt.getInfoList("t103"));
		out.setOutTarget(target);
		out.setOutType(outType);
		out.execute();
		----*/

	}
	
	
	/**
	 * 個人の設問別成績を取得
	 * 
	 * @return list 個人の設問別成績のレコードオブジェクト
	 * @throws java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @exception java.sql.SQLException　カーソル、コネクションのクローズに失敗した場合スローされる。
	 * @see java.sql.SQLException
	 */
	public LinkedList getZbf560() throws SQLException {
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		LinkedList list = new LinkedList();
		String[] rsData = new String[18];		// 読み込みデータ
		String[] listData = new String[72];	// リストデータ

		int    qnoCount=0;
		// キー項目
		String individualID ="";
		String examYear ="";
		String examCD   ="";
		String subcd    ="";
		// キー項目記憶
		String p_individualID ="";
		String p_examYear ="";
		String p_examCD   ="";
		String p_subcd    ="";

//		String query = ""
//		+" SELECT /*+ LEADING(bi) */ qi.INDIVIDUALID, qi.EXAMYEAR,      "
//		+"        qi.EXAMCD, qi.BUNDLECD,            "
//		+"        bi.HOMESCHOOLCD,                   "
//		+"        hi.GRADE, hi.CLASS, hi.CLASS_NO,   "
//		+"        bi.NAME_KANA,                      "
//		+"        qi.SUBCD, es.SUBALLOTPNT, si.SCORE,"
//		+"        si.A_DEVIATION, si.S_DEVIATION,    "
//		+"        qi.QUESTION_NO, eq.QALLOTPNT,      "
//		+"        qi.QSCORE, qi.A_DEVIATION          "
//		+" FROM   QRECORD_I   qi, BASICINFO    bi, HISTORYINFO hi,"
//		+"        EXAMSUBJECT es, EXAMQUESTION eq, SUBRECORD_I si "
//		+" WHERE  qi.EXAMYEAR    = '" + targetYear + "' "
//		+"   AND  qi.EXAMCD      = '" + targetExam + "' "
//		//----------------------------------------------------------------------
//		// [2004/11/04 nino] 子コードを含んだ学校コードで抽出する
//		//+"   AND  qi.BUNDLECD IN (" + schoolCd + ")  "
//		//+"   AND  qi.BUNDLECD IN (" + thisSchools + ")  "
//		+"   AND  bi.SCHOOLCD IN (" + thisSchools + ")  "
//		//----------------------------------------------------------------------
//		+"   AND  qi.INDIVIDUALID=bi.INDIVIDUALID"
//		+"   AND  qi.INDIVIDUALID=hi.INDIVIDUALID"
//		+"   AND  qi.EXAMYEAR    =hi.YEAR        "
//		+"   AND  qi.EXAMYEAR    =es.EXAMYEAR    "
//		+"   AND  qi.EXAMCD      =es.EXAMCD      "
//		+"   AND  qi.SUBCD       =es.SUBCD       "
//		+"   AND  qi.EXAMYEAR    =eq.EXAMYEAR    "
//		+"   AND  qi.EXAMCD      =eq.EXAMCD      "
//		+"   AND  qi.SUBCD       =eq.SUBCD       "
//		+"   AND  qi.QUESTION_NO =eq.QUESTION_NO "
//		+"   AND  qi.INDIVIDUALID=si.INDIVIDUALID"
//		+"   AND  qi.EXAMYEAR    =si.EXAMYEAR    "
//		+"   AND  qi.EXAMCD      =si.EXAMCD      "
//		+"   AND  qi.SUBCD       =si.SUBCD       "
//		+" ORDER BY  qi.EXAMCD, qi.BUNDLECD, hi.GRADE, hi.CLASS, hi.CLASS_NO, qi.SUBCD, qi.QUESTION_NO ";

		try {
			stmt = con.prepareStatement(QueryLoader.getInstance().load("t01").toString());
			stmt.setString(1, this.schoolCd);
			stmt.setString(2, this.targetYear);
			stmt.setString(3, this.targetExam);
			
			rs = stmt.executeQuery();
            
			while (rs.next()) {
				
				// SELECTしたデータを一時変数に格納
				for (int i = 0; i < rsData.length; i++) {
					// 小数点以下ありの項目はgetDouble()で取得
					// 全国偏差値、校内偏差値、設問別全国偏差値
					if ( i == 12 || i == 13 || i == 17 ) {
						if ( rs.getString(i + 1) == null ) {
							rsData[i] = rs.getString(i + 1);
						} else {
							rsData[i] = String.valueOf(rs.getDouble(i + 1));
						}
					} else {
						rsData[i] = rs.getString(i + 1);
					}
				}

				// キー項目をセット
				individualID=rsData[ 0];	// 個人ID
				examYear    =rsData[ 1];	// 模試年度
				examCD      =rsData[ 2];	// 模試コード
				subcd       =rsData[ 9];	// 科目コード

				// キー項目の内容に変化があればデータを格納＆初期化
				if (!p_individualID.equals(individualID) || 
					!p_examYear.equals(examYear) ||
					!p_examCD.equals(examCD) ||
					!p_subcd.equals(subcd) ) {
					
					// リストデータに格納
					if (qnoCount > 0) {
						setListData(listData, list);
					}

					// データ初期化
					qnoCount = 0;
					for (int i = 0; i < listData.length; i++) {
						listData[i] = null;
					}

					// 模試コード〜校内偏差値までをセット
					for (int i = 0; i < 12; i++) {
						listData[i] = rsData[i+2];
					}
				}
				// 格納可能カウンターのカウントアップ
				qnoCount++;

				// キー項目記憶
				p_individualID=individualID;
				p_examYear    =examYear;
				p_examCD      =examCD;
				p_subcd       =subcd;
				
				// 設問番号
				int question_No = Integer.valueOf(rsData[14]).intValue();
				// 設問番号"01"〜"20"の範囲
				if ( 1 <= question_No && question_No <= 20 ) {
					// 配点
					listData[12 + (question_No-1) * 3 + 0] = rsData[15];
					// 得点
					listData[12 + (question_No-1) * 3 + 1] = rsData[16];
					// 全国偏差値
					listData[12 + (question_No-1) * 3 + 2] = rsData[17];
				}

			}
			// 格納可能データがあればリストデータに格納
			if (qnoCount > 0) {
				setListData(listData, list);
			}

		} catch (Exception e) {
			throw new SQLException(e.getMessage());
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		return list;
	}


	/**
	 * 一時的に記憶したリストデータを格納する
	 *
	 * @param 	listData	リストデータ
	 *          list        格納するリスト
	 *
	 */
	private void setListData(String[] listData, LinkedList list) {
		LinkedHashMap data = new LinkedHashMap();
		for (int i = 0; i<listData.length; i++) {
			data.put(new Integer(i+1), listData[i]);
		}
		list.add(data);
	}

	/**
	 * @param connection
	 */
	public void setCon(Connection connection) {
		con = connection;
	}

	/**
	 * @param string
	 */
	public void setSchoolCd(String string) {
		schoolCd = string;
	}

	/**
	 * @param string
	 */
	public void setTargetExam(String string) {
		targetExam = string;
	}

	/**
	 * @param string
	 */
	public void setTargetYear(String string) {
		targetYear = string;
	}

	/**
	 * @param list
	 */
	public void setHeader(LinkedList list) {
		header = list;
	}

	/**
	 * @param strings
	 */
	//public void setTarget(String[] strings) {
	//	target = strings;
	//}

	/**
	 * @param i
	 */
	public void setOutType(int i) {
		outType = i;
	}

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
	 */
	protected String getCategory() {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	/**
	 * @return
	 */
	public Integer[] getTarget() {
		return target;
	}

	/**
	 * @param integers
	 */
	public void setTarget(Integer[] integers) {
		target = integers;
	}

}
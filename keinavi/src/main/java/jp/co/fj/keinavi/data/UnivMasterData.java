package jp.co.fj.keinavi.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import jp.co.fj.kawaijuku.judgement.data.Univ;
import jp.co.fj.kawaijuku.judgement.factory.FactoryManager;

/**
 *
 * メモリからロードした大学マスタ情報を保持するクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivMasterData {

    /** 大学インスタンスリスト */
    private final ArrayList univAll;

    /** 情報誌用科目データマップ */
    private final Map univInfoMap;

    /** 大学インスタンスマップ */
    private final Map univMap;

    /**
     * コンストラクタです。
     *
     * @param univAll 大学インスタンスリスト
     * @param pubMap 公表用大学マスタマップ
     */
    public UnivMasterData(FactoryManager manager) {
        this.univAll = (ArrayList) manager.getDatas();
        this.univInfoMap = manager.getUnivInfoMap();
        this.univMap = new HashMap(univAll.size() * 4 / 3);
        for (Iterator ite = univAll.iterator(); ite.hasNext();) {
            Univ univ = (Univ) ite.next();
            univMap.put(univ.getUniqueKey(), univ);
        }
    }

    /**
     * 大学インスタンスリストを返します。
     *
     * @return 大学インスタンスリスト
     */
    public ArrayList getUnivAll() {
        return univAll;
    }

    /**
     * 情報誌用科目データマップを返します。
     *
     * @return 情報誌用科目データマップ
     */
    public Map getUnivInfoMap() {
        return univInfoMap;
    }

    /**
     * 大学インスタンスマップを返します。
     *
     * @return 大学インスタンスマップ
     */
    public Map getUnivMap() {
        return univMap;
    }

}

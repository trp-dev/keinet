package jp.co.fj.keinavi.beans.user;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.data.user.LoginUserData;
import jp.co.fj.keinavi.util.message.MessageLoader;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 管理者情報更新Bean
 * 
 * 2006.8.2	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class AdminUserUpdateBean extends DefaultBean {

	// 学校コード
	private final String schoolCd;
	// 利用者データ
	private final LoginUserData loginUserData;
	
	/**
	 * コンストラクタ
	 */
	public AdminUserUpdateBean(	final String schoolCd,
			final LoginUserData loginUserData) {
		this.schoolCd = schoolCd;
		this.loginUserData = loginUserData;
	}
	
	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		// 同じIDが契約校マスタにあってはならない
		if (existsPactSchool()) {
			throw new KNBeanException(MessageLoader.getInstance().getMessage("u006a"));
		}
		
		// IDが変わっていないなら、やることはない
		if (loginUserData.getLoginId().equals(
				loginUserData.getOriginalLoginId())) {
			return;
		}

		//「admin」は不可
		if ("admin".equals(loginUserData.getLoginId())) {
			throw new KNBeanException("利用者IDに「admin」は登録できません。");
		}
		
		// 利用者情報更新
		updateLoginIdManage();
		// プロファイル更新（Kei-Navi）
		updateProfile("keinavi.profile");
		// プロファイルフォルダ更新（Kei-Navi）
		updateProfile("keinavi.profilefolder");
		// プロファイル更新（校内成績処理システム）
		updateProfile("keispro.profile");
		// プロファイルフォルダ更新（校内成績処理システム）
		updateProfile("keispro.profilefolder");
	}
	
	protected boolean existsPactSchool() throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					"SELECT 1 FROM pactschool "
					+ "WHERE userid = ? AND schoolcd = ?");
			// 利用者ID（変更後）
			ps.setString(1, loginUserData.getLoginId());
			// 学校コード
			ps.setString(2, schoolCd);
			// 実行
			rs = ps.executeQuery();
			// レコードがあるかどうか
			return rs.next();
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	protected void updateLoginIdManage() throws Exception {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("u07"));
			
			// 利用者ID（変更後）
			ps.setString(1, loginUserData.getLoginId());
			// 学校コード
			ps.setString(2, schoolCd);
			// 利用者ID（変更前）
			ps.setString(3, loginUserData.getOriginalLoginId());
			// 利用者ID（変更後）
			ps.setString(4, loginUserData.getLoginId());
			
			// 更新レコードゼロ→IDが重複している
			if (ps.executeUpdate() == 0) {
				throw new KNBeanException(MessageLoader.getInstance().getMessage("u006a"));
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	
	protected void updateProfile(final String tableName) throws SQLException {
		
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					"UPDATE " + tableName + " SET userid = ? "
					+ "WHERE userid = ? AND bundlecd = ? "
					+ "AND sectorsortingcd IS NULL");
			
			// 利用者ID（変更後）
			ps.setString(1, loginUserData.getLoginId());
			// 利用者ID（変更前）
			ps.setString(2, loginUserData.getOriginalLoginId());
			// 学校コード
			ps.setString(3, schoolCd);
			
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

}

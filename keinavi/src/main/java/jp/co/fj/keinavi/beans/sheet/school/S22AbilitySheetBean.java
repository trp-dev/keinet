package jp.co.fj.keinavi.beans.sheet.school;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.beans.sheet.common.ISheetData;
import jp.co.fj.keinavi.beans.sheet.excel.school.S22_11ExcelCreator;
import jp.co.fj.keinavi.beans.sheet.school.data.S22_11Data;
import jp.co.fj.keinavi.beans.sheet.school.data.S22_11ScoreZoneListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S22_11SubjectListData;
import jp.co.fj.keinavi.beans.sheet.school.data.S22_11YearListData;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * 校内成績分析 - 過年度比較 - 偏差値分布
 * 受験学力測定テスト用のSheetBean
 *
 * 2007.07.26	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class S22AbilitySheetBean extends S42AbilitySheetBean {

    // 帳票データ
    private final S22_11Data data = (S22_11Data) getData();

    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws Exception {

        // 科目変換テーブルのセットアップ
        insertIntoSubCDTrans(new ExamData[]{exam});
        // 基本情報のセット
        setBasicInfo();
        // 科目情報のセット
        setSubjectInfo();
        // センター到達エリアのセット
        setCenterInfo();
        // 過年度データのセット
        setYearInfo();
        // スコア帯データのセット
        setScoreInfo();
    }

    // 基本情報のセット
    private void setBasicInfo() {
        // 一括コード
        data.setBundleCd(profile.getBundleCD());
        // 学校名
        data.setBundleName(profile.getBundleName());
        // 模試年度
        data.setExamYear(exam.getExamYear());
        // 模試コード
        data.setExamCd(exam.getExamCD());
        // 模試名
        data.setExamName(exam.getExamName());
        // 実施日
        data.setInpleDate(exam.getInpleDate());
        // セキュリティスタンプ
        data.setSecurityStamp(getIntFlag(PRINT_STAMP));
    }

    // 過年度データのセット
    private void setYearInfo() throws SQLException {

        PreparedStatement ps = null;
        try {
            final Query query = QueryLoader.getInstance().load("s42_3_4");

            // 比較対象年度
            query.setStringArray(1, getCompYearArray());
            // データ開放日書き換え
            outDate2InDate(query);

            // 生成
            ps = conn.prepareStatement(query.toString());
            // 一括コード
            ps.setString(1, data.getBundleCd());
            // 模試コード
            ps.setString(2, data.getExamCd());
            // データセット
            setYearInfo(ps);
        } finally {
            DbUtils.closeQuietly(ps);
        }
    }

    private void setYearInfo(final PreparedStatement ps) throws SQLException {

        ResultSet rs = null;
        try {
            // 全ての科目について取得する
            for (final Iterator ite = data.getSubjectListData(
                    ).iterator(); ite.hasNext();) {

                final S22_11SubjectListData s = (
                        S22_11SubjectListData) ite.next();

                // 科目コード
                ps.setString(3, s.getSubCd());

                rs = ps.executeQuery();
                while (rs.next()) {
                    final S22_11YearListData y = data.createYearListData();
                    y.setExamYear(rs.getString(3));
                    y.setSubCd(rs.getString(4));
                    y.setAvgPnt(rs.getDouble(5));
                    y.setNumbers(rs.getInt(6));
                    s.addYearListData(y);
                }
                rs.close();
            }
        } finally {
            DbUtils.closeQuietly(rs);
        }
    }

    // スコア帯リストデータをセットする
    private void setScoreInfo() throws SQLException {

        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(
                    QueryLoader.getInstance().getQuery("s42_3_7"));

            // 模試コード
            ps.setString(4, data.getExamCd());

            for (final Iterator ite = data.getSubjectListData(
                    ).iterator(); ite.hasNext();) {
                final S22_11SubjectListData s = (
                        S22_11SubjectListData) ite.next();
                setScoreInfo(ps, s.getYearListData());
            }
        } finally {
            DbUtils.closeQuietly(ps);
        }
    }

    private void setScoreInfo(final PreparedStatement ps,
            final List yearListData) throws SQLException {

        for (final Iterator ite = yearListData.iterator(
                ); ite.hasNext();) {

            final S22_11YearListData y = (S22_11YearListData) ite.next();

            // 科目コードが無ければ処理しない
            // ※成績が存在しない過年度など
            if (y.getSubCd() == null) {
                continue;
            }

            // 模試年度
            ps.setString(1, y.getExamYear());
            // 科目コード
            ps.setString(2, y.getSubCd());
            // 一括コード
            ps.setString(3, data.getBundleCd());

            ResultSet rs = null;
            try {
                rs = ps.executeQuery();
                while (rs.next()) {
                    final S22_11ScoreZoneListData s = (
                            S22_11ScoreZoneListData) data.createScoreZoneListData();
                    s.setDevZoneCd(rs.getString(1));
                    s.setHighLimit(rs.getDouble(2));
                    s.setLowLimit(rs.getDouble(3));
                    s.setNumbers(rs.getInt(4));
                    y.addScoreZoneListData(s);
                }
            } finally {
                DbUtils.closeQuietly(rs);
            }
        }
    }

    /**
     * @return データクラス
     */
    protected ISheetData createData() {
        return new S22_11Data();
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#calcNumberOfPrint()
     */
    protected double calcNumberOfPrint() {
        return getNumberOfPrint("S42_01");
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.AbstractSheetBean#getCategory()
     */
    protected String getCategory() {
        return S_PREV_DEV;
    }

    /**
     * @see jp.co.fj.keinavi.beans.sheet.common.AbstractExtSheetBean#create()
     */
    protected void create() throws Exception {
        sheetLog.add("S22_11");
        new S22_11ExcelCreator(data, sessionKey, outfileList, getAction()).execute();
    }

}

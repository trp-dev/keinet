package jp.co.fj.keinavi.beans.individual;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.kawaijuku.judgement.beans.detail.UnivDetail;
import jp.co.fj.kawaijuku.judgement.beans.judge.Judgement;
import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.data.individual.UnivKeiNavi;
import jp.co.fj.keinavi.util.individual.IPropsLoader;
import jp.co.fj.keinavi.util.individual.JudgementSorterV2;

import com.fjh.beans.DefaultBean;

/**
 * 
 * こだわり条件判定実行クラス
 * 
 * 
 * 2004.09.23   Tomohisa YAMADA - Totec
 * 				新規作成
 * 
 * 2005.01.24   Tomohisa YAMADA - Totec
 * 				[2]新課程対応
 * 
 * 2005.03.08   Tomohisa YAMADA - Totec
 * 				[3]最大表示件数変更
 * 
 * 2005.03.15   Tomohisa YAMADA - Totec
 * 				[4]ソート時にマーク模試で二次判定を考慮する
 * 
 * 2005.04.08   Tomohisa YAMADA - Totec
 * 				[5]ScoreBeanの[35]に伴う修正
 * 
 * 2005.04.21   Tomohisa YAMADA - Totec
 * 				[6]ソートバグ修正
 * 
 * 2005.06.01   Tomohisa YAMADA - Totec
 * 				[7]理系・文系絞り込み修正
 * 				（理系・文系は無視で分野だけで絞り込みをする)
 * 
 * 2005.06.27   Tomohisa YAMADA - Totec
 * 				[8]UnivFactoryDBBranchの[17]を引き継ぐ
 * 
 * 2005.11.07   Tomohisa YAMADA - Totec
 * 				[9][4]の不具合修正
 * 
 * 2005.12.27   Tomohisa YAMADA - Totec
 * 				[10][9]の不具合の修正
 *
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 *              ・センター・リサーチ５段階評価対応
 *              ・判定処理の隠蔽
 *              
 * 2009.10.02	Tomohisa YAMADA - Totec
 * 				入試日程変更対応
 * 
 * 2009.11.27	Tomohisa YAMADA - Totec
 * 				バグ修正：入試日条件を４〜で選択するとエラーになる
 * 				※4月は翌年扱いとするロジックを使用していたため
 * 
 * 2009.12.03	Shoji HASE - Totec
 * 				UNIVDIVCD(１桁)→UNIDIV（２桁）変更にともない、条件分岐定数の変更
 *
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 *
 */
public class JudgeByCondBean extends DefaultBean{

	//static final int MAX_RECORD_NUM = 20;//表示するMAX件数 //[3] del

	/** IN PARAMETER */
	private List univAllList;
	private String[] districtProviso;//地区条件(県コードprefectureをセット)
	private String searchStemmaDiv;//系統選択　1：中系統、2：小系統（文系）、3：小系統（理系）
	private String[] stemmaProviso;//中系統 || 分野コード
	private String descImposeDiv;// 1:（センター利用大）2次・独自試験を課さない 
	private String subImposeDiv;//試験科目課し区分 0:課さない、1:課す
	private String[] imposeSub;//課し試験科目
	private String[] schoolDivProviso;//学校区分条件
	//private String startDateProviso;//入試開始日条件
	//private String endDateProviso;//入試終了日条件
	private String startMonth;
	private String startDate;
	private String endMonth;
	private String endDate;
	
	private String raingDiv;//評価試験区分 1：センターまたは2次・私大、2：総合（ドッキング）
	private String[] raingProviso;//評価範囲条件 1：A判定、2：B判定、3：C判定、4：D判定、5：E判定。コードはコンマ区切りで保持。
	private String[] outOfTergetProviso;//対象外条件
	
	private String dateSearchDiv;//入試区分
	
	private Score scoreBean;
	//private boolean isUseNewLogic;//[5] del
	
	/** OUT PARAMETER */
	private List recordSet; 
	
	// 対象模試がセンタープレの場合、
	// 一般私大、短大の2次判定を＃３記述ではなく
	// センタープレの成績を優先利用するかどうかのフラグ
	// インターネット模試判定において「優先しない」とするため
	// デフォルト値はfalseとする
	private boolean precedeCenterPre = false;
	
	/**
	 * 注：判定を行う前に絞り込める条件(a)と絞り込めない条件(b)がある。
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		int MAX_RECORD_NUM = Integer.parseInt(IPropsLoader.getInstance().getMessage("DISP_NUM_BY_COND"));//[3] add
		
		List judgeTargetList = new ArrayList();
		judgeTargetList.addAll(univAllList);
		
		List judgedList = null;
		
		/** ステップ1 条件で大学を絞り込む */
		if(judgeTargetList != null && judgeTargetList.size() > 0){
			
			//地区の絞り込み
			if(districtProviso != null && districtProviso.length > 0){
				judgeTargetList = restrictDistrict(judgeTargetList);
			}
			
			//系統の絞り込み
			//1：中系統の場合
			if(searchStemmaDiv != null && searchStemmaDiv.trim().equals("1")){
				if(stemmaProviso != null && stemmaProviso.length > 0){
					judgeTargetList = restrictMStemma(judgeTargetList);
				}
			}
			//2, 3：文系・理系の場合
			else {
				if(stemmaProviso != null && stemmaProviso.length > 0) {
					judgeTargetList = restrictRegion(judgeTargetList);
				}
			}

			//学校区分の絞り込み
			if(schoolDivProviso != null && schoolDivProviso.length > 0){
				judgeTargetList = restrictSchoolDiv(judgeTargetList);
			}
			
			//入試日条件絞り込み
			if(dateSearchDiv.equals("1")){//入試日区分が１の時だけ絞り込みを行う
				if(startMonth != null && startDate != null && endMonth != null && endDate != null){
					judgeTargetList = restrictExamDate(judgeTargetList);
				}
			}
			
			// 対象外絞り込み
			if(outOfTergetProviso != null && outOfTergetProviso.length > 0){
				judgeTargetList = restrictOutOfTarget(judgeTargetList);
			}
			
			
			//★ ここからは単位がUnivではなくて、JudgementBeanにかわる
			//★ 判定した結果のJudgementBeanをjudgementListに詰めていく
			
			//（センター利用大）2次・独自試験を課さない
			if(descImposeDiv != null && descImposeDiv.trim().equals("1")){
				judgedList = restrictImpose(judgeTargetList, false, true);
			}
			//（センター利用大）2次・独自試験を課す
			else{
				//試験科目課し区分 1:課す
				if(subImposeDiv.trim().equals("1")){
					//if(imposeSub != null && imposeSub.length > 0){//中でチェックしているので、ここではチェックしない
						judgedList = restrictImpose(judgeTargetList, true, true);
					//}
				}
				//試験科目課し区分 2:課さない
				else{
					//if(imposeSub != null && imposeSub.length > 0){//中でチェックしているので、ここではチェックしない
						judgedList = restrictImpose(judgeTargetList, true, false);
					//}
				}
			}
			
			//判定結果からの絞り込み
			if(judgedList != null && judgedList.size() > 0 && raingProviso != null && raingProviso.length > 0){
				judgedList = restrictJudgement(judgedList);
			}

			//結果のソート
			//if(raingDiv != null && raingDiv.equals("1")){//絶対に１か２//[6] del
				//センターまたは二次評価順にソート
				if(scoreBean.isTargetMarkExam()){
				   //選択対象模試がマークならばセンター評価順にソート
				   Collections.sort(judgedList, new JudgementSorterV2().new SortByCenterScoreRank());
				}else{
				   //選択対象模試が記述模試ならば二次評価順にソート
				   Collections.sort(judgedList, new JudgementSorterV2().new SortBySecondScoreRank());
				 }
			//[6] del start
//			}else{
//				Collections.sort(judgedList, new JudgementSorterV2().new SortByTotalScoreRank());
//			}
			//[6] del end
			
			//最大表示件数まで減らす
			if(judgedList.size() > MAX_RECORD_NUM){
				judgedList = judgedList.subList(0, MAX_RECORD_NUM);
			}
		}
		
		recordSet = judgedList;
	}
	
	/**
	 * 判定結果を絞りこむ
	 * raingDiv;//評価試験区分 1：センターまたは2次・私大、2：総合（ドッキング
	 * ※対象模試がマークならセンター判定、記述なら二次判定で絞り込む
	 * raingProviso;//評価範囲条件 1：A判定、2：B判定、3：C判定、4：D判定、5：E判定。コードはコンマ区切りで保持。
	 * @param judgedList
	 * @return
	 */
	private List restrictJudgement(List judgedList){
		
		int aValue = JudgementConstants.JUDGE_A;
		int bValue = JudgementConstants.JUDGE_B;
		int cValue = JudgementConstants.JUDGE_C;
		int dValue = JudgementConstants.JUDGE_D;
		int eValue = JudgementConstants.JUDGE_E;
			
		Iterator ite = judgedList.iterator();
		while(ite.hasNext()){
			
			Judgement jb = (Judgement) ite.next();
			
			int judgement = 0;
			int ghjudgement = 0;
			int maximumScale = 0;

            //対象模試がマークで大学が一般私大以外
            if (scoreBean.isTargetMarkExam() && !"".equals(jb.getUniv().getSchedule().trim())) {//[10]add
				judgement = jb.getCJudgement();
				ghjudgement = jb.getCGhJudgement();
				maximumScale = ((UnivDetail)jb.getUniv().getDetailHash().get(jb.getSelectedBranch())).getCAllotPointRate();//[8] add
                
            // 対象模試が記述   
			}else{
				judgement = jb.getSJudgement();
				ghjudgement = jb.getSGhJudgement();
				maximumScale = ((UnivDetail)jb.getUniv().getDetailHash().get(jb.getSelectedBranch())).getSAllotPointRate();//[8] add
                
			}
            
			boolean rmFlg = true;
			for(int i=0; i<raingProviso.length; i++){
				
				//センターまたは2次・私大
				if (raingDiv.equals("1")) {
					
					//A判定
					if (raingProviso[i].equals("1")){
						if(judgement == aValue && ghjudgement == 0 && maximumScale > 0){
							//選択：センターまたは2次で、A判定　結果：センターのGとHでないA判定
							rmFlg =false;
						}
					}
					//B判定
					else if(raingProviso[i].equals("2")){
						if(judgement == bValue && ghjudgement == 0 && maximumScale > 0){
							rmFlg =false;
						}
					}
					//C判定
					else if(raingProviso[i].equals("3")){
						if(judgement == cValue && ghjudgement == 0 && maximumScale > 0){
							rmFlg =false;
						}
					}
					//D判定
					else if(raingProviso[i].equals("4")){
						if(judgement == dValue && ghjudgement == 0 && maximumScale > 0){
							rmFlg =false;
						}	
					}
					//E判定
					else if(raingProviso[i].equals("5")){
						if(judgement == eValue && ghjudgement == 0 && maximumScale > 0){
							rmFlg =false;
						}
					}
				}
				//総合（ドッキング）
				else if (raingDiv.equals("2")) {
					
					//A判定
					if (raingProviso[i].equals("1") 
							&& jb.getTJudgement() == aValue 
							&& jb.getTGhJudgement() == 0 
							&& ((UnivDetail)jb.getUniv().getDetailHash().get(jb.getSelectedBranch())).getCAllotPointRate() != JudgementConstants.Detail.ALLOT_POINT_RATE_NA 
							&& ((UnivDetail)jb.getUniv().getDetailHash().get(jb.getSelectedBranch())).getSAllotPointRate() != JudgementConstants.Detail.ALLOT_POINT_RATE_NA){//[8] add
						rmFlg =false;	
					}
					//B判定
					if (raingProviso[i].equals("2") 
							&& jb.getTJudgement() == bValue 
							&& jb.getTGhJudgement() == 0 
							&& ((UnivDetail)jb.getUniv().getDetailHash().get(jb.getSelectedBranch())).getCAllotPointRate() != JudgementConstants.Detail.ALLOT_POINT_RATE_NA 
							&& ((UnivDetail)jb.getUniv().getDetailHash().get(jb.getSelectedBranch())).getSAllotPointRate() != JudgementConstants.Detail.ALLOT_POINT_RATE_NA){//[8] add
						rmFlg =false;	
					}
					//C判定
					if (raingProviso[i].equals("3") 
							&& jb.getTJudgement() == cValue 
							&& jb.getTGhJudgement() == 0 
							&& ((UnivDetail)jb.getUniv().getDetailHash().get(jb.getSelectedBranch())).getCAllotPointRate() != JudgementConstants.Detail.ALLOT_POINT_RATE_NA 
							&& ((UnivDetail)jb.getUniv().getDetailHash().get(jb.getSelectedBranch())).getSAllotPointRate() != JudgementConstants.Detail.ALLOT_POINT_RATE_NA){//[8] add
						rmFlg =false;	
					}
					//D判定
					if (raingProviso[i].equals("4") 
							&& jb.getTJudgement() == dValue 
							&& jb.getTGhJudgement() == 0 
							&& ((UnivDetail)jb.getUniv().getDetailHash().get(jb.getSelectedBranch())).getCAllotPointRate() != JudgementConstants.Detail.ALLOT_POINT_RATE_NA 
							&& ((UnivDetail)jb.getUniv().getDetailHash().get(jb.getSelectedBranch())).getSAllotPointRate() != JudgementConstants.Detail.ALLOT_POINT_RATE_NA){//[8] add
						rmFlg =false;	
					}
					//E判定
					if (raingProviso[i].equals("5") 
							&& jb.getTJudgement() == eValue 
							&& jb.getTGhJudgement() == 0 
							&& ((UnivDetail)jb.getUniv().getDetailHash().get(jb.getSelectedBranch())).getCAllotPointRate() != JudgementConstants.Detail.ALLOT_POINT_RATE_NA 
							&& ((UnivDetail)jb.getUniv().getDetailHash().get(jb.getSelectedBranch())).getSAllotPointRate() != JudgementConstants.Detail.ALLOT_POINT_RATE_NA){//[8] add
						rmFlg =false;
					}
				}
			}
					
			if(rmFlg){
				ite.remove();
			}
		}
		//System.out.println("判定結果 size of judgeTargetList : "+judgedList.size());
		return judgedList;
	}
	
	/**
	 * センタープレでの成績を差し替えを有効化
	 */
	public void setPrecedeCenterPre() {
		precedeCenterPre = true;
	}
	
	/**
	 * @param judgeTargetList 絞込み済みの大学リスト
	 * @param impose2nd 二次を課す・課さない
	 * @param imposeSub 科目を課す・課さない
	 * @return 判定結果リスト
	 */
	private List restrictImpose(List judgeTargetList, boolean impose2ndB, boolean imposeSubB) throws Exception {
		
		final SearchImposeSubBean sisb = new SearchImposeSubBean();
		sisb.setSubjectArray(imposeSub);
		sisb.setUnivAll(judgeTargetList);
		sisb.setScore(scoreBean);
		sisb.setImpose2nd(impose2ndB);
		sisb.setImposeSub(imposeSubB);
		if (precedeCenterPre) {
			sisb.setPrecedeCenterPre();
		}
		sisb.execute();
		
		return sisb.getJudgements();
	}
	
	/**
	 * 対象外の絞り込み
	 * @param judgeTargetList
	 * @return
	 */
	private List restrictOutOfTarget(List judgeTargetList){
		boolean[] newOutOfTP = new boolean[]{false, false, false};
		for(int i=0; i<outOfTergetProviso.length; i++){
			if(!outOfTergetProviso[i].trim().equals("")){//空の場合でもサイズは１
				newOutOfTP[Integer.parseInt(outOfTergetProviso[i]) -1] = true;
			}
		}
		Iterator iteS = judgeTargetList.iterator();
		while(iteS.hasNext()){
			UnivKeiNavi u = (UnivKeiNavi)iteS.next();
			boolean rmFlg = false;
			//女子大を除く
			if(u.getFlag_women() == 2 && newOutOfTP[0]){
				rmFlg = true;
			}
			//二部を除く
			else if(u.getFlag_night() == 2 && newOutOfTP[1]){
				rmFlg = true;
			}
			//夜間を除く
			else if(u.getFlag_night() == 3 && newOutOfTP[2]){
				rmFlg = true;
			}
			if(rmFlg){
				iteS.remove();
			}
		}
		//System.out.println("対象外 size of judgeTargetList : "+judgeTargetList.size());
		return judgeTargetList;
	}
	
	/**
	 * 入試日の絞り込み
	 * @param judgeTargetList
	 * @return
	 * @throws Exception 
	 */
	private List restrictExamDate(List list) throws Exception{
		
		final List dates = getDateList(
				Integer.parseInt(startMonth), 
				Integer.parseInt(startDate), 
				Integer.parseInt(endMonth), 
				Integer.parseInt(endDate));
		
		for (Iterator ite = list.iterator(); ite.hasNext();) {
			
			UnivKeiNavi univ = (UnivKeiNavi) ite.next();
			
			//除去対象とするかのフラグ
			boolean shouldRemove = true;
			
			for (Iterator ite2 = dates.iterator(); ite2.hasNext();) {
				
				Date date = (Date) ite2.next();
				
				//日付重複がひとつでも見つかれば除去しない
				if (univ.isDateMatched(date)) {
					shouldRemove = false;
					break;
				}
			}
			
			//削除対象となった大学をリストから除去
			if (shouldRemove) {
				ite.remove();
			}
		}
		
		return list;
	}
	
	private List getDateList(int month1, int date1, int month2, int date2) {
		
		Calendar today = Calendar.getInstance();
		
		//カレンダーに指定の日付を設定する
		Calendar calendar1 = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		//※年は今年を使う
		calendar1.clear();
		calendar2.clear();
		
		int year1 = today.get(Calendar.YEAR);
		
		//1月〜3月は翌年として扱う
		if (month1 < 4 && month1 > 0) {
			year1 ++;
		}
		
		int year2 = today.get(Calendar.YEAR);
		
		//1月〜3月は翌年として扱う
		if (month2 < 4 && month2 > 0) {
			year2 ++;
		}
		
		calendar1.set(year1, month1 - 1, date1);
		calendar2.set(year2, month2 - 1, date2);
		
		List dateList = new ArrayList();
		
//		java.text.SimpleDateFormat test = new java.text.SimpleDateFormat("yyyy/MM/dd");
			
		do {
			
//			System.out.println(test.format(calendar1.getTime()));
			
			//リストに日付を追加
			dateList.add(calendar1.getTime());
			
			//開始日に１日加算する
			calendar1.add(Calendar.DAY_OF_YEAR, 1);

		} while (!calendar1.getTime().after(calendar2.getTime()));
		
		return dateList;
	}
	
	
	/**
	 * 大学区分の絞り込み
	 * @param judgeTargetList
	 * @return
	 */
	private List restrictSchoolDiv(List judgeTargetList){
		Iterator iteB = judgeTargetList.iterator();
		while(iteB.hasNext()){
			UnivKeiNavi u = (UnivKeiNavi)iteB.next();
			boolean rmFlg = true;
			for(int i=0; i<schoolDivProviso.length; i++){
				if(schoolDivProviso[i].equals("1") && (u.getUnivDivCd().equals("01") || u.getUnivDivCd().equals("02")) && u.getSchedule().equals("D"))//国公立大前期
					rmFlg = false;
				else if(schoolDivProviso[i].equals("2") && (u.getUnivDivCd().equals("01") || u.getUnivDivCd().equals("02")) && u.getSchedule().equals("E"))//国公立大後期
					rmFlg = false;
				else if(schoolDivProviso[i].equals("3") && (u.getUnivDivCd().equals("01") || u.getUnivDivCd().equals("02")) && u.getSchedule().equals("C"))//国公立大中期
					rmFlg = false;
				else if(schoolDivProviso[i].equals("4") && u.getUnivDivCd().equals("03") && !u.getSchedule().equals("1"))//私立大一般
					rmFlg = false;
				else if(schoolDivProviso[i].equals("5") && u.getUnivDivCd().equals("03") && u.getSchedule().equals("1"))//私立大センター利用
					rmFlg = false;
				else if(schoolDivProviso[i].equals("6") && u.getUnivDivCd().equals("04") && !u.getSchedule().equals("1"))//国公立短大一般
					rmFlg = false;
				else if(schoolDivProviso[i].equals("7") && u.getUnivDivCd().equals("04") && u.getSchedule().equals("1"))//国公立短大センター利用
					rmFlg = false;
				else if(schoolDivProviso[i].equals("8") && u.getUnivDivCd().equals("05") && !u.getSchedule().equals("1"))//私立短大一般
					rmFlg = false;
				else if(schoolDivProviso[i].equals("9") && u.getUnivDivCd().equals("05") && u.getSchedule().equals("1"))//私立短大センター利用
					rmFlg = false;
				else if(schoolDivProviso[i].equals("10") && (u.getUnivDivCd().equals("06") || u.getUnivDivCd().equals("07")))//文部科学省所管外
					rmFlg = false;
				else if(schoolDivProviso[i].equals("11") && (u.getUnivDivCd().equals("08") || u.getUnivDivCd().equals("09")))//専門学校
					rmFlg = false;
			}
			if(rmFlg){
				iteB.remove();
			}
		}
		//System.out.println("学校区分 size of judgeTargetList : "+judgeTargetList.size());
		return judgeTargetList;
	}
	
	/**
	 * 分野(文系・理系)の絞り込み
	 * @param judgeTargetList
	 * @return
	 */
	private List restrictRegion(List judgeTargetList){
		Iterator iteS = judgeTargetList.iterator();
		while (iteS.hasNext()) {
			UnivKeiNavi u = (UnivKeiNavi) iteS.next();
			boolean rmFlg = true;
			//[7] del start
//			if (searchStemmaDiv.equals("2") && !u.bunriCd.equals("1")) { //選択が文系でこの大学が文系でない
//				rmFlg = true;
//			} else if (searchStemmaDiv.equals("2")	&& !u.bunriCd.equals("1")) { //選択が理系でこの大学が理系でない
//				rmFlg = true;
//			} else { //選択した系と同じ
			//[7] del end
				if(u.getRegionList() != null && u.getRegionList().size() > 0){
					Iterator iteMR = u.getRegionList().iterator();
					while(iteMR.hasNext()){
						String[] regionCd = (String[]) iteMR.next();
						for (int i = 0; i < stemmaProviso.length; i++) {
							if (regionCd != null && regionCd[0] != null && regionCd[0].equals(stemmaProviso[i])) {
								//分野コードが選択範囲にある
								rmFlg = false;
							}
						}	
					}
				}	
//			}//[7] del
			if(rmFlg){
				iteS.remove();
			}
		}
		//System.out.println("分野（文系・理系）条件 size of judgeTargetList : "+judgeTargetList.size());
		return judgeTargetList;
	}
	
	/**
	 * 中系統絞り込み
	 * @param judgedTargetList
	 * @return
	 */
	private List restrictMStemma(List judgeTargetList){
		Iterator iteS = judgeTargetList.iterator();
		while(iteS.hasNext()){
			UnivKeiNavi u = (UnivKeiNavi)iteS.next();
			boolean rmFlg = true;
			if(u.getMStemmaList() != null && u.getMStemmaList().size() > 0){
				Iterator iteMS = u.getMStemmaList().iterator();
				while(iteMS.hasNext()){
					String[] mStemma = (String[]) iteMS.next();
					for(int i=0; i<stemmaProviso.length; i++){
						if(mStemma != null && mStemma[0] != null && mStemma[0].equals(stemmaProviso[i])){
							rmFlg = false;
						}
					}
				}	
			}
			if(rmFlg){
				iteS.remove();
			}
		}
		//System.out.println("中系統条件 size of judgeTargetList : "+judgeTargetList.size());
		return judgeTargetList;
	}
	
	/**
	 * 地区の絞り込み
	 * @param judgedList
	 * @return
	 */
	private List restrictDistrict(List judgeTargetList){
		Iterator iteP = judgeTargetList.iterator();
		while(iteP.hasNext()){
			UnivKeiNavi u = (UnivKeiNavi)iteP.next();
			boolean rmFlg = true;
			for(int i=0; i<districtProviso.length; i++){
				if(u.getPrefCd().equals(districtProviso[i])){
					rmFlg = false;
				}
			}
			if(rmFlg){
				iteP.remove();
			}
		}
		//System.out.println("地区条件 size of judgeTargetList : "+judgeTargetList.size());
		return judgeTargetList;
	}
	
	/**
	 * @return
	 */
	public List getUnivAllList() {
		return univAllList;
	}

	/**
	 * @param list
	 */
	public void setUnivAllList(List list) {
		univAllList = list;
	}

	/**
	 * @return
	 */
	public Score getScoreBean() {
		return scoreBean;
	}

	/**
	 * @param bean
	 */
	public void setScoreBean(Score bean) {
		scoreBean = bean;
	}

	/**
	 * @return
	 */
	public List getRecordSet() {
		return recordSet;
	}

	/**
	 * @param list
	 */
	public void setRecordSet(List list) {
		recordSet = list;
	}

	/**
	 * @return
	 */
	public String getDescImposeDiv() {
		return descImposeDiv;
	}

	/**
	 * @return
	 */
	public String[] getDistrictProviso() {
		return districtProviso;
	}

	/**
	 * @return
	 */
	public String[] getImposeSub() {
		return imposeSub;
	}

	/**
	 * @return
	 */
	public String[] getOutOfTergetProviso() {
		return outOfTergetProviso;
	}

	/**
	 * @return
	 */
	public String getRaingDiv() {
		return raingDiv;
	}

	/**
	 * @return
	 */
	public String[] getRaingProviso() {
		return raingProviso;
	}

	/**
	 * @return
	 */
	public String[] getSchoolDivProviso() {
		return schoolDivProviso;
	}

	/**
	 * @return
	 */
	public String getSearchStemmaDiv() {
		return searchStemmaDiv;
	}

	/**
	 * @return
	 */
	public String getSubImposeDiv() {
		return subImposeDiv;
	}

	/**
	 * @param string
	 */
	public void setDescImposeDiv(String string) {
		descImposeDiv = string;
	}

	/**
	 * @param strings
	 */
	public void setDistrictProviso(String[] strings) {
		districtProviso = strings;
	}

	/**
	 * @param strings
	 */
	public void setImposeSub(String[] strings) {
		imposeSub = strings;
	}

	/**
	 * @param strings
	 */
	public void setOutOfTergetProviso(String[] strings) {
		outOfTergetProviso = strings;
	}

	/**
	 * @param string
	 */
	public void setRaingDiv(String string) {
		raingDiv = string;
	}

	/**
	 * @param strings
	 */
	public void setRaingProviso(String[] strings) {
		raingProviso = strings;
	}

	/**
	 * @param strings
	 */
	public void setSchoolDivProviso(String[] strings) {
		schoolDivProviso = strings;
	}

	/**
	 * @param string
	 */
	public void setSearchStemmaDiv(String string) {
		searchStemmaDiv = string;
	}

	/**
	 * @param string
	 */
	public void setSubImposeDiv(String string) {
		subImposeDiv = string;
	}

	/**
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @return
	 */
	public String getEndMonth() {
		return endMonth;
	}

	/**
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @return
	 */
	public String getStartMonth() {
		return startMonth;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string) {
		endDate = string;
	}

	/**
	 * @param string
	 */
	public void setEndMonth(String string) {
		endMonth = string;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string) {
		startDate = string;
	}

	/**
	 * @param string
	 */
	public void setStartMonth(String string) {
		startMonth = string;
	}

	/**
	 * @return
	 */
	public String[] getStemmaProviso() {
		return stemmaProviso;
	}

	/**
	 * @param strings
	 */
	public void setStemmaProviso(String[] strings) {
		stemmaProviso = strings;
	}

	/**
	 * @return
	 */
	public String getDateSearchDiv() {
		return dateSearchDiv;
	}

	/**
	 * @param string
	 */
	public void setDateSearchDiv(String string) {
		dateSearchDiv = string;
	}

}
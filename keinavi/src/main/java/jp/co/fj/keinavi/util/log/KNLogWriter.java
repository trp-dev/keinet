package jp.co.fj.keinavi.util.log;

import java.util.logging.*;

import jp.co.fj.keinavi.util.*;

/**
 * ログをファイルに出力するクラス
 *
 * @Date		2004/06/09
 * @author	TOTEC)Nishiyama
 */
abstract public class KNLogWriter {

	/**
	 * ログ出力オブジェクトをインスタンス化する
	 * 
	 * @return ログのインスタンス
	 */
	public static KNLogWriter factory() throws Exception { return null; }

	/**
	 *
	 * ログを出力する
	 * 
	 * @param rec ログレコード
	 */
	public void LogOutput(LogRecord rec) throws Exception { output(rec); }

	/**
	 * ログのパラメータを取得する
	 * 
	 * @return ログ名
	 */
	abstract public String getLogName();
	
	/**
	 * ログのファイルサイズ上限値を取得する。
	 * 
	 * @return ログのファイルサイズ上限値
	 */
	abstract protected int getLogLimit();
	
	/**
	 * ログのファイル履歴の上限値を取得する。
	 * 
	 * @return ログのファイル履歴上限値
	 */
	abstract protected int getLogCount();
	
	/**
	 * ロガーを取得する。
	 * 
	 * @return ロガーのインスタンス
	 */
	protected Logger getLogger() { return Logger.getLogger(getLogName()); }

	/**
	 * ログを初期化する
	 */
	protected void initLog() throws Exception { _initLog(); }

	/**
	 * ログを初期化する
	 */
	protected void _initLog() throws Exception {
		
		FileHandler fh = new FileHandler(getLogFileName(), getLogLimit(), getLogCount(), true);
		fh.setFormatter(new KNLogFormatter());
		Logger log = getLogger();
		log.addHandler(fh);
	}

	/**
	 * ログファイルへの出力
	 *
	 * @param rec ログレコード
	 */
	abstract protected void output(LogRecord rec) throws Exception;

	/**
	 * 出力ファイルのパスの取得
	 *
	 * @return 出力ファイルパス
	 */
	protected String getKNLOG() {
		String FilePath = "";
		
		// KNLOGを出力するディレクトリを取得する。
		try {
			FilePath = KNCommonProperty.getKNLogRootPath();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return FilePath;
	}

	/**
	 *
	 * 出力ファイル名の取得
	 *
	 * @return 出力ファイル名
	 */
	abstract protected String getLogFileName();

	/**
	 *
	 * 出力ファイルのパスの取得
	 *
	 * @return 出力ファイルパス
	 */
	abstract protected  String getLogFilePath();
}

package jp.co.fj.keinavi.servlets.maintenance;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.maintenance.DeclarationUpdateBean;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.forms.maintenance.X010Form;

/**
 *
 * 作業中宣言サーブレット
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class X010Servlet extends AbstractMServlet {

	/**
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet
	 * 			#execute(javax.servlet.http.HttpServletRequest,
	 * 					javax.servlet.http.HttpServletResponse)
	 */
	public void execute(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {
		
		// アクションフォーム
		final X010Form form = (X010Form) getActionForm(request,
				"jp.co.fj.keinavi.forms.maintenance.X010Form");
		// ログインセッション
		final LoginSession login = getLoginSession(request);
		
		Connection con = null;
		try {
			con = getConnectionPool(request);
			
			// 設定
			if ("1".equals(form.getActionMode())) {
				DeclarationUpdateBean bean = new DeclarationUpdateBean(
						login.getUserID());
				bean.setConnection(null, con);
				bean.setLoginId(login.getAccount());
				bean.execute();
			// 解除
			} else if ("2".equals(form.getActionMode())) {
				DeclarationUpdateBean bean = new DeclarationUpdateBean(
						login.getUserID(), false);
				bean.setConnection(null, con);
				bean.execute();
			}

			con.commit();
			
		} catch (final Exception e) {
			rollback(con);
			throw createServletException(e);
		} finally {
			releaseConnectionPool(request,  con);
		}
		
		// 作業中宣言を更新する
		loadDeclaration(request);
		
		forward(request, response, JSP_X010);
	}

}

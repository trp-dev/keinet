package jp.co.fj.keinavi.excel.personal;

import java.util.Iterator;
import java.util.List;

import jp.co.fj.keinavi.excel.data.personal.I13SeisekiListBean;
import jp.co.fj.keinavi.util.KNUtil;

/**
 * 
 *
 * 個人帳票作成用Utilクラス
 * 
 * 2006/09/06   [新規作成]
 * 
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 *
 */
public class PersonalUtil {
    
//    /**
//     * 個人情報ヘッダ作成
//     * @param student 生徒情報
//     * @return ヘッダ文字列
//     */
//    public static String toPersonalInfo(final I11ListBean student) {
//
//        final StringBuffer buf = new StringBuffer();
//        
//        String sex = "不明";
//        
//        if ("1".equals(student.getStrSex())) {
//            sex = "男";
//        } else if("2".equals(student.getStrSex())) {
//            sex = "女";
//        }
//        
//        String name = student.getStrKana();
//        
//        if (student.getStrShimei().length() > 0) {
//            name = student.getStrShimei() + "（" + student.getStrKana() + "）";
//        }
//        
//        buf.append("氏名　　：");
//        buf.append(name);
//        buf.append("　");
//        buf.append("性別：");
//        buf.append(sex);
//        
//        return buf.toString();
//    }
    
    
    //■以下、帳票別Utilクラス
    
    /**
     * 連続個人成績表専用Util
     */
    public static class SeqRec {
        
        /**
         * 連続個人成績表における受験学力テストが存在する時の注釈
         */
        private static final String REMARK_2JPNS = 
            "※受験学力測定テストで国語2科目受験の場合、理科の欄に国語2科目めが表示されます。";
        
        /**
         * 連続個人成績表における受験学力テストが存在する時の注釈を適用
         * @param seisekis 成績（模試）一覧データ
         * @return 注釈
         */
        public static String remark2Jpns(final List seisekis) { 
            
            final String NONE = "";
            
            if (isAbilityExist(seisekis)) {
                return REMARK_2JPNS;
            }
            
            return NONE;
        }
        
        /**
         * 成績一覧【７番目以内】に受験学力測定が存在するかどうか
         * @param seisekis 成績（模試）一覧データ
         * @return 成績一覧に受験学力測定が存在するかどうか
         */
        public static boolean isAbilityExist(final List seisekis) {
            
            final Iterator ite = seisekis.iterator();
            int count = 0;
            while (ite.hasNext()) {
                
                final I13SeisekiListBean seiseki = 
                    (I13SeisekiListBean) ite.next();
                
                if (!seiseki.getI13KyokaSeisekiList().isEmpty()) {
                	count++;
                }
                
                //模試一覧に受験学力測定が存在する
                if (KNUtil.TYPECD_ABILITY.equals(seiseki.getStrMshTypeCd())) {
                    return count <= 7;
                }
            }

            return false;
        }
    }
    
//    public static class BalChart {
//        
//    }
//    
//    public static class BalChartAvg {
//        
//    }
//    
//    public static class SubTrans {
//        
//    }
//    
//    public static class CrsTrans {
//        
//    }
    
}

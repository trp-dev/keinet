package jp.co.fj.keinavi.data.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * 利用者データ
 * 
 * 2005.10.11	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class LoginUserData implements Serializable {

	/** セッションキー */
	public static final String SESSION_KEY = "LoginUserData";

	// 利用者ID
	private String loginId;
	// 変更前の利用者ID
	private String originalLoginId;
	// 利用者名
	private String loginName;
	// 初期パスワード
	private String defaultPwd;
	// 初期パスワード変更
	private boolean isChangedPwd;
	// 管理者権限
	private boolean isManager;
	// メンテナンス権限
	private boolean isMaintainer;
	// Kei-Navi機能権限
	private short knFunctionFlag;
	// 校内成績処理機能権限
	private short scFunctionFlag;
	// 入試結果機能権限
	private short eeFunctionFlag;
	// 答案閲覧機能権限
	private short abFunctionFlag;
	// 担当クラスデータリスト
	private final List chargeClassList = new ArrayList();
	
	/**
	 * 担当クラス設定を追加する
	 * 
	 * @param year
	 * @param grade
	 * @param className
	 */
	public void addChargeClass(final String year, final int grade, final String className) {
		
		final UserClassData data = new UserClassData();
		data.setYear(year);
		data.setGrade(grade);
		data.setClassName(className);
		chargeClassList.add(data);
	}
	
	/**
	 * @return chargeClassList を戻します。
	 */
	public List getChargeClassList() {
		return chargeClassList;
	}
	/**
	 * @return defaultPwd を戻します。
	 */
	public String getDefaultPwd() {
		return defaultPwd;
	}
	/**
	 * @param defaultPwd 設定する defaultPwd。
	 */
	public void setDefaultPwd(String defaultPwd) {
		this.defaultPwd = defaultPwd;
	}
	/**
	 * @return eeFunctionFlag を戻します。
	 */
	public short getEeFunctionFlag() {
		return eeFunctionFlag;
	}
	/**
	 * @param eeFunctionFlag 設定する eeFunctionFlag。
	 */
	public void setEeFunctionFlag(short eeFunctionFlag) {
		this.eeFunctionFlag = eeFunctionFlag;
	}
	/**
	 * @return isChangedPwd を戻します。
	 */
	public boolean isChangedPwd() {
		return isChangedPwd;
	}
	/**
	 * @param isChangedPwd 設定する isChangedPwd。
	 */
	public void setChangedPwd(boolean isChangedPwd) {
		this.isChangedPwd = isChangedPwd;
	}
	/**
	 * @return isMaintainer を戻します。
	 */
	public boolean isMaintainer() {
		return isMaintainer;
	}
	/**
	 * @param isMaintainer 設定する isMaintainer。
	 */
	public void setMaintainer(boolean isMaintainer) {
		this.isMaintainer = isMaintainer;
	}
	/**
	 * @return isManager を戻します。
	 */
	public boolean isManager() {
		return isManager;
	}
	/**
	 * @param isManager 設定する isManager。
	 */
	public void setManager(boolean isManager) {
		this.isManager = isManager;
	}
	/**
	 * @return knFunctionFlag を戻します。
	 */
	public short getKnFunctionFlag() {
		return knFunctionFlag;
	}
	/**
	 * @param knFunctionFlag 設定する knFunctionFlag。
	 */
	public void setKnFunctionFlag(short knFunctionFlag) {
		this.knFunctionFlag = knFunctionFlag;
	}
	/**
	 * @return loginId を戻します。
	 */
	public String getLoginId() {
		return loginId;
	}
	/**
	 * @param loginId 設定する loginId。
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	/**
	 * @return loginName を戻します。
	 */
	public String getLoginName() {
		return loginName;
	}
	/**
	 * @param loginName 設定する loginName。
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	/**
	 * @return scFunctionFlag を戻します。
	 */
	public short getScFunctionFlag() {
		return scFunctionFlag;
	}
	/**
	 * @param scFunctionFlag 設定する scFunctionFlag。
	 */
	public void setScFunctionFlag(short scFunctionFlag) {
		this.scFunctionFlag = scFunctionFlag;
	}
	/**
	 * @return originalLoginId を戻します。
	 */
	public String getOriginalLoginId() {
		return originalLoginId;
	}
	/**
	 * @param pOriginalLoginId 設定する originalLoginId。
	 */
	public void setOriginalLoginId(String pOriginalLoginId) {
		originalLoginId = pOriginalLoginId;
	}
	/**
	 * @return abFunctionFlag を戻します。
	 */
	public short getAbFunctionFlag() {
		return abFunctionFlag;
	}
	/**
	 * @param abFunctionFlag 設定する abFunctionFlag。
	 */
	public void setAbFunctionFlag(short abFunctionFlag) {
		this.abFunctionFlag = abFunctionFlag;
	}

}

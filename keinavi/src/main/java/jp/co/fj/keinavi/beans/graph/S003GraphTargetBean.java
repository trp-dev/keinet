package jp.co.fj.keinavi.beans.graph;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * Zà¬ÑªÍ - Zà¬Ñ - Î·lªz
 * 
 * 2006.07.27	[VKì¬]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class S003GraphTargetBean extends BaseGraphCheckerBean {

	/**
	 * RXgN^
	 * 
	 * @param id vt@CJeSID
	 */
	public S003GraphTargetBean(final String id) {
		super(id);
	}

	/**
	 * @see jp.co.fj.keinavi.beans.graph.AbstractGraphCheckerBean#hasSubjectGraphSheet(
	 * 			javax.servlet.http.HttpServletRequest)
	 */
	public boolean hasSubjectGraphSheet(HttpServletRequest request) {
		
		// Î·lÑÊxªzOt
		if (isValidRange(request, GRAPH_DIST)) {
			return true;
		}
		
		// Î·lÑÊlÏÝã°Ot
		if (isValidRange(request, GRAPH_BUILDUP)) {
			return true;
		}
		
		// Î·lÑÊ\¬äOt
		if (isValidRange(request, GRAPH_COMP_RATIO)) {
			return true;
		}
		
		return false;
	}

}

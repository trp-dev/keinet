package jp.co.fj.keinavi.data.individual;

import java.io.Serializable;

import jp.co.fj.kawaijuku.judgement.data.SubjectRecord;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * 科目別データ保持用クラス
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SubRecordData extends SubjectRecord implements Serializable, Cloneable, Comparable {

    private static final long serialVersionUID = -5207847028943886633L;

    private String personId;
    private String examYear;
    private String examCd;

    private String bundleCd;

    private String sRank;
    private String cRank;
    private String sDeviation;

    private String dispSequence;
    private String sDeviationPre;	//前回偏差値

    // 基礎科目フラグ
    private String basicFlg;

    //not in table, but needed here
    private String subAllotPnt;
    private String subName;
    private String comparedCDeviation;

    // 2019/12/02 QQ)Ooseto 英語認定試験延期対応 DEL START
    //2019/09/11 QQ)Oosaki 共通テスト対応 ADD START
    //private String cefr;
    //2019/09/11 QQ)Oosaki 共通テスト対応 ADD END
    // 2019/12/02 QQ)Ooseto 英語認定試験延期対応 DEL END

    /**
     * クローンメソッド
     */
    public Object clone() {
       try {
           return (super.clone());
       } catch (CloneNotSupportedException e) {
           // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD START
           //throw (new InternalError(e.getMessage()));
           throw new InternalError(e);
           // 2019/10/16 QQ)Ooseto 共通テスト対応 UPD END
       }
    }

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Object o2) {
        SubRecordData s2 = (SubRecordData) o2;
        int thisDispSequence;
        int targetDispSequence;

        try{
            thisDispSequence = Integer.parseInt(this.getDispSequence());
        }catch(Exception e){
            thisDispSequence = 9999;
        }
        try{
            targetDispSequence = Integer.parseInt(s2.getDispSequence());
        }catch(Exception e){
            targetDispSequence = 9999;
        }

        if (thisDispSequence < targetDispSequence) {
            return -1;
        } else if (thisDispSequence > targetDispSequence) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * 空のデータを返す
     * @return
     */
    public static SubRecordData getEmptyData(){
        SubRecordData empty = new SubRecordData();
        empty.setSubName("");
        empty.setScore("");
        empty.setSubAllotPnt("");
        empty.setCvsScore("");
        empty.setCDeviation("");
        empty.setScope("");//[1] add
        return empty;
    }

    /**
     * @return
     */
    public String getBundleCd() {
        return bundleCd;
    }

    /**
     * @return
     */
    public String getCRank() {
        return cRank;
    }

    /**
     * @return
     */
    public String getExamCd() {
        return examCd;
    }

    /**
     * @return
     */
    public String getExamYear() {
        return examYear;
    }

    /**
     * @return
     */
    public String getPersonId() {
        return personId;
    }

    /**
     * @return
     */
    public String getSDeviation() {
        return sDeviation;
    }

    /**
     * @return
     */
    public String getSRank() {
        return sRank;
    }

    /**
     * @param string
     */
    public void setBundleCd(String string) {
        bundleCd = string;
    }

    /**
     * @param string
     */
    public void setCRank(String string) {
        cRank = string;
    }

    /**
     * @param string
     */
    public void setExamCd(String string) {
        examCd = string;
    }

    /**
     * @param string
     */
    public void setExamYear(String string) {
        examYear = string;
    }

    /**
     * @param string
     */
    public void setPersonId(String string) {
        personId = string;
    }

    /**
     * @param string
     */
    public void setSDeviation(String string) {
        sDeviation = string;
    }

    /**
     * @param string
     */
    public void setSRank(String string) {
        sRank = string;
    }

    /**
     * @return
     */
    public String getSubName() {
        return subName;
    }

    /**
     * @param string
     */
    public void setSubName(String string) {
        subName = string;
    }

    /**
     * @return
     */
    public String getSubAllotPntStr() {
        return subAllotPnt;
    }

    /**
     * @param string
     */
    public void setSubAllotPnt(String string) {
        subAllotPnt = string;
    }

    /**
     * @return
     */
    public String getComparedCDeviation() {
        return comparedCDeviation;
    }

    /**
     * @param string
     */
    public void setComparedCDeviation(String string) {
        comparedCDeviation = string;
    }

    /**
     * @return
     */
    public String getDispSequence() {
        return dispSequence;
    }

    /**
     * @param string
     */
    public void setDispSequence(String string) {
        dispSequence = string;
    }

    /**
     * @return
     */
    public String getSDeviationPre() {
        return sDeviationPre;
    }

    /**
     * @param string
     */
    public void setSDeviationPre(String string) {
        sDeviationPre = string;
    }

    // 2019/12/02 QQ)Ooseto 英語認定試験延期対応 DEL START
    //2019/09/11 QQ)Oosaki 共通テスト対応 ADD START
//    /**
//     * @return
//     */
//    public String getCefr() {
//        return cefr;
//    }
//
//    /**
//     * @param string
//     */
//    public void setCefr(String string) {
//        cefr = string;
//    }
    //2019/09/11 QQ)Oosaki 共通テスト対応 ADD END
    // 2019/12/02 QQ)Ooseto 英語認定試験延期対応 DEL END

    /**
     * 基礎科目フラグを返します。
     *
     * @return basicFlg 基礎科目フラグ
     */
    public  String getBasicFlg() {
        return basicFlg;
    }

    /**
     * 基礎科目フラグを設定します。
     *
     * @param basicFlg 基礎科目フラグ
     */
    public  void setBasicFlg(String basicFlg) {
        this.basicFlg = basicFlg;
    }

    static interface Ignore {
        @JsonIgnore
        String getPersonId();
        @JsonIgnore
        String getExamYear();
        @JsonIgnore
        String getExamCd();
        @JsonIgnore
        String getBundleCd();
        @JsonIgnore
        int getDispSequence();
        @JsonIgnore
        String getBasicFlg();
        @JsonIgnore
        String getSubName();
        @JsonIgnore
        String getComparedCDeviation();
        @JsonIgnore
        String getSDeviationPre();
        @JsonIgnore
        String getSDeviation();
        @JsonIgnore
        String getSubAllotPntStr();
        @JsonIgnore
        String getSRank();
        @JsonIgnore
        String getCRank();
        @JsonIgnore
        String getJpnHyoka1();
        @JsonIgnore
        String getJpnHyoka2();
        @JsonIgnore
        String getJpnHyoka3();
        @JsonIgnore
        String getJpnHyokaAll();
    }

    /**
     *
     * @see java.lang.Object#toString()
     */
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        String result = null;
        try {
            mapper.addMixIn(SubjectRecord.class, Ignore.class);
            result = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

}

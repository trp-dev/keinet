package jp.co.fj.keinavi.servlets.individual;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean;
import jp.co.fj.keinavi.beans.individual.judgement.JudgementListBean;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.ICommonMap;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.individual.I205Form;
import jp.co.fj.keinavi.servlets.KNServletException;

/**
 * 
 * 判定結果詳細表示サーブレット
 * 
 * 
 * 2004.08.03   Tomohisa YAMADA - Totec
 *              新規作成
 *
 * 2005.02.23   Tomohisa YAMADA - Totec
 * 				[1]判定結果をDetailPageBeanに一本化する
 * 
 * 2005.03.02   Tomohisa YAMADA - Totec
 * 				[2]I301Servletの[1]を適用する
 * 
 * 2005.07.04   Tomohisa YAMADA - Totec
 * 				[3]セッションがきれた場合または大学が
 * 				存在しない場合に適切なエラーをかえす
 * 
 * 2005.07.04   Tomohisa YAMADA - Totec
 * 				[4]判定結果が存在しなければ公表科目も
 * 				表示できないようにする
 * 
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 *              ・センター・リサーチ５段階評価対応
 *              ・判定処理の隠蔽
 *
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 *
 */
public class I205Servlet extends IndividualServlet{
	
	protected void execute(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
			
		//フォームデータを取得
		I205Form i205Form = null;
		try {
			i205Form = (I205Form)super.getActionForm(request, "jp.co.fj.keinavi.forms.individual.I205Form");
		} catch (Exception e) {
			KNServletException k = new KNServletException("0I205にてフォームの取得に失敗しました。" + e);
			k.setErrorCode("00I205010101");
			throw k;
		}
		
		HttpSession session = request.getSession();
		
		//プロファイル情報
		Profile profile = (Profile) session.getAttribute(Profile.SESSION_KEY);
		//個人共通
		ICommonMap iCommonMap = (ICommonMap) session.getAttribute(ICommonMap.SESSION_KEY);
		//模試データ
		ExamSession examSession = (ExamSession) session.getAttribute(ExamSession.SESSION_KEY);	

		if ("i205".equals(getForward(request))) {//転送:JSP
			
			String action = i205Form.getActionValue();
			
			String uniqueKey = i205Form.getUniqueKey();
			
			String branch = i205Form.getSelectBranch();
			
			//セッションに格納してある結果一覧を取得
			//JudgementListBean jlb = (JudgementListBean)session.getAttribute("useListBean");//[2] del
			//[2] add start
			JudgementListBean jlb;
			if(i205Form.getJudgeNo() != null && i205Form.getJudgeNo().trim().equals("2")){
				jlb = (JudgementListBean)session.getAttribute("useListBean2");
			}else{
				jlb = (JudgementListBean)session.getAttribute("useListBean");
			}
			//[4] del start
//			//公表科目画面用
//			session.setAttribute("publicBean", jlb);
			//[4] del end
			//[2] add end
		  	
			try {
				
				//対象の判定結果インスタンスを取得
				final DetailPageBean bean = searchResult(jlb, uniqueKey);
		
			  	//JudgementBeanの取得が出来なかった場合は、一覧画面を再表示する
			  	//if(!beanFlg){//[3] del
			  	//[3] add start
			  	if(jlb == null){
					KNServletException e = new KNServletException("詳細データの取得に失敗しました。");
					e.setErrorCode("1006");
					e.setErrorMessage("結果一覧が存在しないため、これ以上の操作はできません。");
					throw e;			
			  	} 
			  	//判定結果が取得できなければエラー※必ずあるはず！
			  	else if(bean == null) {
					KNServletException e = new KNServletException("詳細データの取得に失敗しました。");
					e.setErrorCode("1006");
					e.setErrorMessage("結果一覧から削除されたため、これ以上操作は出来ません。");
					throw e;
					//[3] add end
			  	}
				
				//表示用データを再作成
				//枝番切り替えでの再表示（=遷移元がi205）にも対応
				DetailPageBean detail = 
						recreate(
							bean, 
							branch,
							"i205".equals(getBackward(request)));
				
				request.setAttribute("PageBean", detail);
			} 
			catch (Exception e1) {
				e1.printStackTrace();
				KNServletException k = new KNServletException("0I205にてエラーが発生しました。" + e1);
				k.setErrorCode("00I205010101");
				throw k;
			}

			request.setAttribute("i205Form", i205Form);
			
			iCommonMap.setTargetPersonId(i205Form.getTargetPersonId());
			super.forward(request, response, JSP_I205);
						
		}else{//転送:SERVLET
			
			iCommonMap.setTargetPersonId(i205Form.getTargetPersonId());
			super.forward(request, response, SERVLET_DISPATCHER);
			
		}
	
	}
	
	/**
	 * 枝番指定において再判定を実施
	 */
	private DetailPageBean recreate(
			final DetailPageBean detail, 
			String branch, 
			boolean appointed) throws Exception {
		
		DetailPageBean result = null;
		
		//枝番指定での再判定
		if (branch != null) {
			result = (DetailPageBean) detail.rejudge(branch);
		}
		//単なる再判定
		else {
			result = (DetailPageBean) detail.rejudge();
		}
		
		return result;
	}
	
	/**
	 * 対象の判定インスタンスを検索
	 * @param jlb
	 * @param uniqueKey
	 * @return
	 * @throws Exception 
	 */
	public static DetailPageBean searchResult(JudgementListBean jlb, String uniqueKey) throws Exception {
		
	  	if(jlb != null){
			for(int i=0;i<(jlb.getRecordSet()).size();i++){
				String workUniqueKey = ((DetailPageBean)(jlb.getRecordSet()).get(i)).getUniqueKey();
				if(workUniqueKey.equals(uniqueKey)){
					//対象の判定結果を返す
			  		return (DetailPageBean) jlb.getRecordSet().get(i);
				}
			}
	  	}
	  	
	  	//判定結果がなければnullを返す
	  	return null;
	}
}

package jp.co.fj.keinavi.data.maintenance;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 *
 * 履歴情報データ
 * 
 * 2005.12.06	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class KNHistoryData {

	private final String year;
	private final String grade;
	private final String className;
	private final String classNo;
	
	/**
	 * コンストラクタ
	 * 
	 * @param pYear 年度
	 * @param pGrade 学年
	 * @param pClassName クラス
	 * @param pClassNo クラス番号
	 */
	public KNHistoryData(final String pYear, final String pGrade,
			final String pClassName, final String pClassNo) {
		
		this.year = pYear;
		this.grade = pGrade;
		this.className = pClassName;
		this.classNo = pClassNo;
	}
	
	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(final Object obj) {
		
		if (!(obj instanceof KNHistoryData)) {
			return false;
		}

		KNHistoryData data = (KNHistoryData) obj;
		
		return new EqualsBuilder()
				.append(this.year, data.year)
				.append(this.grade, data.grade)
				.append(this.className, data.className)
				.append(this.classNo, data.classNo)
				.isEquals();
	}

	/* (非 Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		
		return new HashCodeBuilder(17, 37)
				.append(this.year)
				.append(this.grade)
				.append(this.className)
				.append(this.classNo)
				.toHashCode();
	}

}

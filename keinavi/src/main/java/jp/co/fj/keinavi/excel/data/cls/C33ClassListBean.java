package jp.co.fj.keinavi.excel.data.cls;

/**
 * 偏差値分布（クラス比較）データクラス
 * 作成日: 2004/08/17
 * @author	Ito.Y
 */
public class C33ClassListBean {
	//学校名
	private String strGakkomei = "";
	//学年
	private String strGrade = "";
	//クラス名
	private String strClass = "";
	//クラス用グラフ表示フラグ
	private int intDispClassFlg = 0;
	//人数
	private int intNinzu = 0;
	//得点率
	private float floTokuritsu = 0;
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrGakkomei() {
		return this.strGakkomei;
	}
	public String getStrGrade() {
		return this.strGrade;
	}
	public String getStrClass() {
		return this.strClass;
	}
	public int getIntDispClassFlg() {
		return this.intDispClassFlg;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public float getFloTokuritsu() {
		return this.floTokuritsu;
	}

	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setStrGakkomei(String strGakkomei) {
		this.strGakkomei = strGakkomei;
	}
	public void setStrGrade(String strGrade) {
		this.strGrade = strGrade;
	}
	public void setStrClass(String strClass) {
		this.strClass = strClass;
	}
	public void setIntDispClassFlg(int intDispClassFlg) {
		this.intDispClassFlg = intDispClassFlg;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setFloTokuritsu(float floTokuritsu) {
		this.floTokuritsu = floTokuritsu;
	}

}

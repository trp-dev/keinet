package jp.co.fj.keinavi.servlets.login;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.login.AssetLoginBean;
import jp.co.fj.keinavi.servlets.DefaultLoginServlet;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.AssetLoginUtil;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * [as001] 資産認証ログイン画面のサーブレットです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class AssetLoginServlet extends DefaultLoginServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = -6405410016903148398L;

    /** 画面ID */
    private static final String SCREEN_ID = "as001";

    /**
     * {@inheritDoc}
     */
    public void execute(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        if (SCREEN_ID.equals(getBackward(request))) {
            /* 転送元がログイン画面の場合 */

            /* 資産認証を行う */
            doAuth(request);

            if (getErrorMessage(request) == null) {
                /* エラーメッセージなし */
                /* 資産認証クッキーを発行する */
                AssetLoginUtil.publishCookie(request, response);

                /* 次画面のサーブレットパスをパラメータから取得する */
                String next = request.getParameter("next");

                if (next != null && next.startsWith("/")) {
                    /* 有効な次画面情報があるならリダイレクトする */
                    response.sendRedirect(KNUtil.getContextPath(request) + next);
                } else {
                    /* そうでなければ資産インデックス画面にリダイレクトする */
                    response.sendRedirect(KNUtil.getContextPath(request) + "/public/info/index.html");
                }
            } else {
                /* エラーメッセージあり→画面を再表示する */
                forward2Jsp(request, response);
            }
        } else {
            /* ログイン画面を表示する */
            forward2Jsp(request, response, SCREEN_ID);
        }
    }

    /**
     * 資産認証を行います。
     *
     * @param request {@link HttpServletRequest}
     * @throws ServletException 想定外の例外
     */
    private void doAuth(final HttpServletRequest request) throws ServletException {
        Connection con = null;
        try {
            con = getConnectionPool(request);
            con.setAutoCommit(false);
            AssetLoginBean bean = new AssetLoginBean();
            bean.setConnection(null, con);
            bean.setSchoolId(request.getParameter("account"));
            bean.setUserId(request.getParameter("user"));
            bean.setPassword(request.getParameter("password"));
            bean.execute();
        } catch (final KNServletException e) {
            setErrorMessage(request, e);
        } catch (final Exception e) {
            throw createServletException(e);
        } finally {
            releaseConnectionPool(request, con);
        }
    }

}

package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.keinavi.data.maintenance.AnswerSheetData;
import jp.co.fj.keinavi.util.JpnStringConv;
import jp.co.fj.keinavi.util.sql.Query;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 受験届修正 - 受験届リストBean
 * 
 * 2006.10.10	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class MAnswerSheetListBean extends DefaultBean {

	// 学校コード
	private final String schoolCd;
	
	// 対象年度
	private String targetYear;
	// 対象学年
	private int targetGrade = -1;
	// 対象クラス
	private String targetClass;
	// 頭文字
	private String initialKana;
	// 対象模試コード
	private String targetExamCd;
	// ヘルプデスクかどうか
	private boolean isHelpDesk;
	
	// 受験届データのリスト
	private final List answerSheetList = new ArrayList();
	
	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 学校コード
	 */
	public MAnswerSheetListBean(final String pSchoolCd) {
		this.schoolCd = pSchoolCd;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = createPreparedStatement();
			rs = ps.executeQuery();
			while (rs.next()) {
				final AnswerSheetData data = new AnswerSheetData(0);
				// 解答用紙番号
				data.setAnswerSheetNo(rs.getString(1));
				// 模試年度
				data.setExamYear(rs.getString(2));
				// 模試コード
				data.setExamCd(rs.getString(3));
				// 学年
				data.setGrade(rs.getInt(4));
				// クラス
				data.setClassName(rs.getString(5));
				// クラス番号
				data.setClassNo(rs.getString(6));
				// 性別
				data.setSex(rs.getString(7));
				// ひらがな氏名
				data.setNameHiragana(
						JpnStringConv.kkanaHan2Hkana(rs.getString(8)));
				// 漢字氏名
				data.setNameKanji(rs.getString(9));
				// 生年月日
				data.setBirthday(rs.getDate(10));
				// 電話番号
				data.setTelNo(rs.getString(11));
				// 個人ID
				data.setIndividualId(rs.getString(12));
				// リストに保持する
				answerSheetList.add(data);
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}
	
	// SQLを作る
	private String createQuery() throws SQLException {
		
		final Query query = QueryLoader.getInstance().load("m25");

		// ヘルプデスクなら内部データ開放日
		if (isHelpDesk) {
			query.replaceAll("out_dataopendate", "in_dataopendate");
		}
		
		// 頭文字
		// 50音以外
		if (initialKana == null) {
			query.append("AND (ri.name_kana IS NULL OR TRANSLATE(");
			query.append("TO_SEARCHSTR(SUBSTR(ri.name_kana, 1, 1)), ");
			query.append("'.ｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜ', '.') ");
			query.append("IS NOT NULL) ");
		// 50音
		} else if (initialKana.length() > 0) {
			query.append("AND TO_SEARCHSTR(SUBSTR(ri.name_kana, 1, 1)) = ? ");
		}
		
		// ORDER BY
		query.append("ORDER BY class_no, sex, name_kana, answersheet_no");
		
		return query.toString();
	}

	/**
	 * @return PreparedStatement
	 */
	private PreparedStatement createPreparedStatement() throws Exception {
		
		final PreparedStatement ps = conn.prepareStatement(createQuery());

		// 対象年度（対象模試年度）
		ps.setString(1, targetYear);
		// 対象模試コード
		ps.setString(2, targetExamCd);
		// 学校コード
		ps.setString(3, schoolCd);
		// 学校コード
		ps.setString(4, schoolCd);
		// 対象学年
		ps.setInt(5, targetGrade);
		// 対象クラス
		ps.setString(6, targetClass);
		// 頭文字
		if (!StringUtils.isEmpty(initialKana)) {
			ps.setString(7, initialKana);
		}
		
		return ps;
	}
		
	/**
	 * @return targetYear を戻します。
	 */
	public String getTargetYear() {
		return targetYear;
	}

	/**
	 * @param pTargetGrade 設定する targetGrade。
	 */
	public void setTargetGrade(final String pTargetGrade) {
		targetGrade = Integer.parseInt(pTargetGrade);
	}

	/**
	 * @param pTargetYear 設定する targetYear。
	 */
	public void setTargetYear(final String pTargetYear) {
		targetYear = pTargetYear;
	}

	/**
	 * @param pTargetClass 設定する targetClass。
	 */
	public void setTargetClass(String pTargetClass) {
		targetClass = pTargetClass;
	}

	/**
	 * @param pTargetGrade 設定する targetGrade。
	 */
	public void setTargetGrade(int pTargetGrade) {
		targetGrade = pTargetGrade;
	}

	/**
	 * @param pInitialKana 設定する initialKana。
	 */
	public void setInitialKana(String pInitialKana) {
		initialKana = pInitialKana;
	}

	/**
	 * @return answerSheetList を戻します。
	 */
	public List getAnswerSheetList() {
		return answerSheetList;
	}

	/**
	 * @param pTargetExamCd 設定する targetExamCd。
	 */
	public void setTargetExamCd(String pTargetExamCd) {
		targetExamCd = pTargetExamCd;
	}

	/**
	 * @param pIsHelpDesk 設定する isHelpDesk
	 */
	public void setHelpDesk(boolean pIsHelpDesk) {
		isHelpDesk = pIsHelpDesk;
	}

}

/*
 * 作成日: 2004/07/28
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.forms.txt_out.factory;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import jp.co.fj.keinavi.forms.txt_out.TMaxForm;

/**
 * @author 奥村ゆかり
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class CommonFormFactory {

	TMaxForm form; // アクションフォーム
	Map item; // アイテムマップ

	List outTarget = new LinkedList(); // 出力項目対象

	/**
	 * コンストラクタ
	 * @param form
	 * @param item
	 */
	public CommonFormFactory(TMaxForm form, Map item) {
		this.form = form;
		this.item = item;
	}


	/**
	 * 出力項目対象をフォームにセットする
	 */
	public void setOutTarget() {
		form.setOutTarget((String[]) outTarget.toArray(new String[0]));
	}

	/**
	 *
	 *  ファイル出力形式をセットする
	 * 
	 * 1:CSV　2:固定長
	 *
	 * @return void
	 * 
	 */
	public void setTxtType() {

		if (item.containsKey("1402")) {
			form.setTxtType(((Short) item.get("1402")).toString());
			// 初期値
		} else {
			form.setTxtType("1");
		}
	}
	
	/**
	 * @return
	 */
	public TMaxForm getForm() {
		return form;
	}

	/**
	 * @param form
	 */
	public void setForm(TMaxForm form) {
		this.form = form;
	}

}

package jp.co.fj.keinavi.beans.maintenance;

import java.sql.Connection;
import java.sql.SQLException;

import jp.co.fj.keinavi.beans.KNBeanException;
import jp.co.fj.keinavi.data.maintenance.KNStudentData;
import jp.co.fj.keinavi.util.KNUtil;
import jp.co.fj.keinavi.util.message.MessageLoader;

import com.fjh.beans.DefaultBean;

/**
 *
 * 生徒情報更新Bean
 * 
 * 2006.11.02	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public class StudentUpdateBean extends DefaultBean {

	// 生徒情報登録Bean
	private final StudentInsertBean bean;
	// 現年度
	private final String currentYear = KNUtil.getCurrentYear();
	// 前年度
	private final String lastYear = (Integer.parseInt(currentYear) - 1) + "";
	// 前々年度
	private final String beforeLastYear = (Integer.parseInt(currentYear) - 2) + "";

	/**
	 * コンストラクタ
	 * 
	 * @param pSchoolCd 学校コード
	 */
	public StudentUpdateBean(final String pSchoolCd) {
		bean = new StudentInsertBean(pSchoolCd);
	}
	
	/**
	 * @see jp.co.fj.keispro.beans.common.csv.CSVLineRegisterBean#execute()
	 */
	public void execute() throws Exception {
		
		// 生徒データ
		final KNStudentData data = bean.getStudent();
		
		// 学籍基本情報の更新
		if (!bean.updateBasicinfo()) {
			throw new Exception("学籍基本情報の更新に失敗しました。");
		}

		// 学籍履歴情報の更新
		// 前々年度
		if (data.getBeforeLastGrade() >= 0) {
			bean.updateHistoryinfo(data.getIndividualId(), beforeLastYear,
					data.getBeforeLastGrade(), data.getBeforeLastClassName(),
					data.getBeforeLastClassNo(), data.getOBeforeLastGrade(),
					data.getOBeforeLastClassName());
		}
		// 前年度
		if (data.getLastGrade() >= 0) {
			bean.updateHistoryinfo(data.getIndividualId(), lastYear,
					data.getLastGrade(), data.getLastClassName(),
					data.getLastClassNo(), data.getOLastGrade(),
					data.getOLastClassName());
		}
		// 今年度
		if (data.getCurrentGrade() >= 0) {
			bean.updateHistoryinfo(data.getIndividualId(), currentYear,
					data.getCurrentGrade(), data.getCurrentClassName(),
					data.getCurrentClassNo(), data.getOCurrentGrade(),
					data.getOCurrentClassName());
		}
		
		// 更新ログ
		// 今年度があるなら
		if (data.getCurrentGrade() >= 0) {
			bean.updateLog(data.getIndividualId(), currentYear,
					data.getCurrentGrade(), data.getCurrentClassName(),
					data.getCurrentClassNo(), data.getNameKana());
		// なければ前年度
		} else if (data.getLastGrade() >= 0) {
			bean.updateLog(data.getIndividualId(), currentYear,
					data.getLastGrade(), data.getLastClassName(),
					data.getLastClassNo(), data.getNameKana());
		// それでなければ前々年度
		} else if (data.getBeforeLastGrade() >= 0) {
			bean.updateLog(data.getIndividualId(), currentYear,
					data.getBeforeLastGrade(), data.getBeforeLastClassName(),
					data.getBeforeLastClassNo(), data.getNameKana());
		}
		
		// 識別マーク更新
		bean.updateCMark();
	}

	/**
	 * 集計学年・クラスを更新する
	 * ※統合で行う処理と同じ
	 * 
	 * @throws SQLException SQL例外
	 */
	public void updateCntGrade() throws SQLException {
		bean.updateCntGrade();
	}
	
	/**
	 * 履歴情報重複チェック
	 * ※execute前に必ず一度は実行すること
	 * 
	 * @param subIndividualId 従生徒個人ID
	 * @throws Exception 不正データ例外
	 */
	public void validateDuplication(
			final String subIndividualId) throws Exception {
		
		// データチェック
		bean.validate();
		// 生徒データ
		final KNStudentData data = bean.getStudent();
		// 重複チェック
		final StringBuffer info = new StringBuffer();
		
		// 前々年度
		if (bean.isDuplication(data.getIndividualId(), beforeLastYear,
				data.getBeforeLastGrade(), data.getBeforeLastClassName(),
				data.getBeforeLastClassNo(), subIndividualId)) {
			info.append("\\n");
			info.append(beforeLastYear + "年度");
			info.append(data.getBeforeLastGrade() + "年");
			info.append(data.getBeforeLastClassName() + "クラス");
			info.append(data.getBeforeLastClassNo() + "番");
		}
		
		// 前年度
		if (bean.isDuplication(data.getIndividualId(), lastYear,
				data.getLastGrade(), data.getLastClassName(),
				data.getLastClassNo(), subIndividualId)) {
			info.append("\\n");
			info.append(lastYear + "年度");
			info.append(data.getLastGrade() + "年");
			info.append(data.getLastClassName() + "クラス");
			info.append(data.getLastClassNo() + "番");
		}

		// 今年度
		if (bean.isDuplication(data.getIndividualId(), currentYear,
				data.getCurrentGrade(), data.getCurrentClassName(),
				data.getCurrentClassNo(), subIndividualId)) {
			info.append("\\n");
			info.append(currentYear + "年度");
			info.append(data.getCurrentGrade() + "年");
			info.append(data.getCurrentClassName() + "クラス");
			info.append(data.getCurrentClassNo() + "番");
		}
		
		// 重複エラー
		if (info.length() > 0) {
			throw new KNBeanException(
					MessageLoader.getInstance().getMessage("m011a") + info, "1");
		}
	}
	
	/**
	 * @see com.fjh.beans.DefaultBean#setConnection(
	 * 			java.lang.String, java.sql.Connection)
	 */
	public void setConnection(final String pName,
			final Connection pConn) {
		super.setConnection(pName, pConn);
		bean.setConnection(pName, pConn);
	}

	/**
	 * @param pStudent 設定する student。
	 */
	public void setStudent(final KNStudentData pStudent) {
		bean.setStudent(pStudent);
	}

}

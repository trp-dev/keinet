package jp.co.fj.keinavi.beans.individual;

import java.sql.SQLException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.keinavi.beans.SubRecordASearchBean;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.judgement.beans.ScoreKeiNavi;
import jp.co.fj.keinavi.util.individual.ExamSorter;
import jp.co.fj.keinavi.util.individual.IPropsLoader;

import com.fjh.beans.DefaultSearchBean;

/**
 *
 *「この生徒が受けた」最新のマーク模試と最新の記述模試を取得
 *
 *
 * 2004.11.13	Tomohisa YAMADA - Totec
 * 				新規作成
 *
 * 2005.03.08	Tomohisa YAMADA - Totec
 * 				I301Servletの[2]を受け継ぐ
 * 
 * 2005.04.07	Tomohisa YAMADA - Totec
 * 				対象模試のセット
 * 
 * 2005.04.08	Tomohisa YAMADA - Totec
 * 				ScoreBeanの[35]に伴う修正
 * 
 * <2010年度改修>
 * 2009.11.20	Tomohisa YAMADA - Totec
 * 				マスタデータの持ち方変更
 * 
 * 2010.04.08	Tomohisa YAMADA - Totec
 * 				数学�@の偏差値を動的に算出する
 * 
 * 
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 */
public class ScoreSearchBean extends DefaultSearchBean{

	/**
	 * 模試セッション
	 */
	private ExamSession examSession;
	
	/**
	 * 個人ＩＤs
	 */
	private String[] individualIds;
	
	/**
	 * [1] add
	 * 年度制限
	 */
	private String limitedYear;

	/**
	 * [3] add
	 * 選択模試
	 */
	private ExaminationData targetExam;
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 * 1. 「この生徒が受けた」最新のマーク模試と最新の記述模試
	 * 2. 模試区分01,02に限定する
	 * 3. センターリサーチ・高２模試を除く
	 */
	public void execute() throws SQLException, Exception {
		
		if(individualIds == null) throw new Exception("生徒ID配列が存在しません");
		
		for(int i=0; i<individualIds.length; i++){
			
			//「この生徒が受けた」最新の模試（仕様）
			ExaminationData[] exams = new ExaminationData[]{null, null};
			
			TakenExamSearchBean tesb = new TakenExamSearchBean();
			tesb.setIndividualId(individualIds[i]);
			tesb.setLimitedYear(limitedYear);//[1] add
			tesb.setExamSession(examSession);
			//対象を模試区分01,02に限定する
			tesb.setExamTypeCds(new String[]{
				IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_MARK"), 
				IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_WRTN")});
			//センターリサーチ・高２模試を除く
			tesb.setExcludeExamCds(	new String[]{
				IPropsLoader.getInstance().getMessage("CODE_EXAM_CENTERRESEARCH"),
				IPropsLoader.getInstance().getMessage("CODE_EXAM_MARK_2NDGRADE"),
				IPropsLoader.getInstance().getMessage("CODE_EXAM_WRTN_2NDGRADE")});
			tesb.setConnection("", conn);
			tesb.execute();
			
			List takenExams = tesb.getRecordSet();
			if(takenExams != null && takenExams.size() > 0){//この生徒がひとつ以上模試を受けていれば
				//外部開放日順にソート
				Collections.sort(takenExams, new ExamSorter().new SortByOutDataOpenDate());
				//この生徒が受けた全ての模試の一番最初のマークと記述を取得
				Iterator ite2 = takenExams.iterator();
				while(ite2.hasNext()){
					ExaminationData exam = (ExaminationData) ite2.next();
					if(exams[0] == null && exam.getExamTypeCd().equals(IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_MARK"))){//最初のマークは最新のマーク
						exams[0] = exam;
					}else if(exams[1] == null && exam.getExamTypeCd().equals(IPropsLoader.getInstance().getMessage("CODE_EXAMTYPECD_WRTN"))){
						exams[1] = exam;
					}
				}
			} 
								
			//マーク模試成績の取得（模試を全く受けていない場合以外は絶対に存在する）
			SubRecordSearchBean srsb = null;
			if(exams[0] != null){
				srsb = new SubRecordSearchBean();
				srsb.setIndividualId(individualIds[i]);
				srsb.setExamYear(exams[0].getExamYear());
				srsb.setExamCd(exams[0].getExamCd());
				srsb.setConnection("", conn);
				srsb.execute();
			}
			//[3] del start
			//この生徒が一切マーク模試を受けていない場合は空で埋める
//			else{
//				exams[0] = ExaminationData.getEmptyExaminationData();
//			}
			//[3] del end
	
			//記述模試成績の取得（マーク模試選択時はドッキング先が存在しない）
			SubRecordSearchBean srsb2 = null;
			if(exams[1] != null){
				srsb2 = new SubRecordSearchBean();
				srsb2.setIndividualId(individualIds[i]);
				srsb2.setExamYear(exams[1].getExamYear());
				srsb2.setExamCd(exams[1].getExamCd());
				srsb2.setConnection("", conn);
				srsb2.execute();
			}
			//[3] del start
			//この生徒が一切記述模試を受けていない場合は空で埋める
//			else{
//				exams[1] = ExaminationData.getEmptyExaminationData();
//			}
			//[3] del end
							
			//ScoreBeanに取得してきた成績をセット
			Score sb = new ScoreKeiNavi(
					SubRecordASearchBean.searchNullSet(
							exams[0], 
							conn));
			sb.setIndividualId(individualIds[i]);
			if(srsb != null){
				if(srsb.getRecordSet() != null && srsb.getRecordSet().size() > 0){//[3] add
					sb.setMark(srsb.getRecordSet());
				//[3] add start
				}else{
					sb.setMark(null);
				}
				//[3] add end
			}else{
				sb.setMark(null);
			}
			if(srsb2 != null){
				if(srsb2.getRecordSet() != null && srsb2.getRecordSet().size() > 0){//[3] add
					sb.setWrtn(srsb2.getRecordSet());
				//[3] add start
				}else{
					sb.setWrtn(null);
				}
				//[3] add end
			}else{
				sb.setWrtn(null);
			}
			sb.setCExam(exams[0]);//[0]はマーク模試
			sb.setSExam(exams[1]);//[1]は記述模試
			
//			//execute前に対象模試をセットしなければならない [2]add //[3] del
//			sb.setTargetExam(sb.getCExam());//[2] add //[3] del
			sb.setTargetExam(targetExam);//[3] add
			
			sb.execute();
							
			//スコアリストに追加
			recordSet.add(sb);
			
		}	
	}

	/**
	 * @return
	 */
	public ExamSession getExamSession() {
		return examSession;
	}

	/**
	 * @return
	 */
	public String[] getIndividualIds() {
		return individualIds;
	}

	/**
	 * @param session
	 */
	public void setExamSession(ExamSession session) {
		examSession = session;
	}

	/**
	 * @param strings
	 */
	public void setIndividualIds(String[] strings) {
		individualIds = strings;
	}

	/**
	 * @return
	 */
	public String getLimitedYear() {
		return limitedYear;
	}

	/**
	 * @param string
	 */
	public void setLimitedYear(String string) {
		limitedYear = string;
	}

	/**
	 * @return
	 */
	public ExaminationData getTargetExam() {
		return targetExam;
	}

	/**
	 * @param data
	 */
	public void setTargetExam(ExaminationData data) {
		targetExam = data;
	}

}

package jp.co.fj.keinavi.excel.data.school;

//import java.util.ArrayList;
/**
 * �Z�����ѕ��́|�߉��r�|���ъT�� �ߔN�x��r�p�f�[�^���X�g
 * �쐬��: 2004/07/21
 * @author	A.Iwata
 */
public class S51YearListBean {
	//�N�x
	private String strNendo = "";
	//�͎���
	private String strMshmei = "";
	//�͎����{���
	private String strMshDate = "";
	//�󌱐l��
	private int intNinzu = 0;
	//���ϓ_
	private float floHeikin = 0;
	//���ϕ΍��l
	private float floHensa = 0;
	
	/*----------*/
	/* Get      */
	/*----------*/
	public String getStrNendo() {
		return this.strNendo;
	}
	public String getStrMshmei() {
		return this.strMshmei;
	}
	public String getStrMshDate() {
		return this.strMshDate;
	}
	public int getIntNinzu() {
		return this.intNinzu;
	}
	public float getFloHeikin() {
		return this.floHeikin;
	}
	public float getFloHensa() {
		return this.floHensa;
	}


	/*---------------*/	
	/* Set */	
	/*---------------*/	
	public void setStrNendo(String strNendo) {
		this.strNendo = strNendo;
	}
	public void setStrMshmei(String strMshmei) {
		this.strMshmei = strMshmei;
	}
	public void setStrMshDate(String strMshDate) {
		this.strMshDate = strMshDate;
	}
	public void setIntNinzu(int intNinzu) {
		this.intNinzu = intNinzu;
	}
	public void setFloHeikin(float floHeikin) {
		this.floHeikin = floHeikin;
	}
	public void setFloHensa(float floHensa) {
		this.floHensa = floHensa;
	}
}

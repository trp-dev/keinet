package jp.co.fj.keinavi.beans.individual;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.sql.Query;

/**
 *
 * 判定履歴削除
 * 
 * <2010年度改修>
 * 2010.01.19	Tomohisa YAMADA - TOTEC
 * 				新規作成
 *
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 * 
 */
public class DetermHstDeleteBean extends I301Bean {
	
	/**
	 * 個人ID
	 */
	private String individualId;
	
	/**
	 * 削除キー
	 * ※複数の場合
	 */
	private String[] values;
	
	/**
	 * 大学コード
	 */
	private String univcd;
	
	/**
	 * 学部コード
	 */
	private String facultycd;
	
	/**
	 * 学科コード
	 */
	private String deptcd;
	
	/**
	 * コンストラクタ
	 */
	public DetermHstDeleteBean(String id, String[] strings) {
		individualId = id;
		values = strings;
	}
	
	/**
	 * コンストラクタ
	 */
	public DetermHstDeleteBean(
			String id, 
			String univcd, 
			String facultycd, 
			String deptcd) {
		this.individualId = id;
		this.univcd = univcd;
		this.facultycd = facultycd;
		this.deptcd = deptcd;
	}
	
	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		//複数を実行
		if (values != null) {
			delete(values);
		}
		//単数を実行
		else {
			delete(new String[]{univcd + "," + facultycd + "," + deptcd});
		}
	}
	
	/**
	 * 削除
	 * @param values
	 * @throws Exception
	 */
	private void delete(String[] values) throws Exception {
		PreparedStatement ps = null;
		try {
			
			Query query = new Query("DELETE FROM determhistory WHERE individualid = ? ");
			query.append("AND univcd = ? AND facultycd = ? AND deptcd = ? ");
			
			ps = conn.prepareStatement(query.toString());
			ps.setString(1, individualId);
			
			for(int i = 0; values.length > i; i++) {
				String[] univ = CollectionUtil.splitComma(values[i]);
				ps.setString(2, univ[0]);
				ps.setString(3, univ[1]);
				ps.setString(4, univ[2]);
				ps.executeUpdate();
			}
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

}

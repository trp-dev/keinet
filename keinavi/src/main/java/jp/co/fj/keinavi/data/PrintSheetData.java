/*
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data;

import java.io.Serializable;

/**
 * 帳票を作成する際のデータを保持する
 *
 */
public class PrintSheetData implements Serializable {

	private String[] examcd;
	private String[] examyear;

	/* (非 Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	

	/**
	 * @return
	 */
	public String[] getExamcd() {
		return examcd;
	}

	/**
	 * @return
	 */
	public String[] getExamyear() {
		return examyear;
	}

	/**
	 * @param strings
	 */
	public void setExamcd(String[] strings) {
		examcd = strings;
	}

	/**
	 * @param strings
	 */
	public void setExamyear(String[] strings) {
		examyear = strings;
	}

}

package jp.co.fj.keinavi.servlets.txt_out;

import java.io.IOException;
import java.sql.Connection;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.keinavi.beans.com_set.ComSetStatusBean;
import jp.co.fj.keinavi.beans.txt_out.TextInfoBean;
import jp.co.fj.keinavi.data.ExamData;
import jp.co.fj.keinavi.data.ExamSession;
import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.profile.Profile;
import jp.co.fj.keinavi.forms.txt_out.T001Form;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.KNUtil;

/**
 *
 * 2005.05.29	Yoshimoto KAWAI - Totec
 * 				ComSetStatusBeanのセットメンバ追加対応
 *
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 * <2010年度マーク高２模試対応>
 * 2011.01.11   Tomohisa YAMADA - Totec
 *              倫政対応・マーク高２対応有
 *
 * @author okumura
 *
 */
public class T001Servlet extends AbstractTServlet {

	protected static final Set topSet = new HashSet(); // トップ画面の画面IDセット

	/* (非 Javadoc)
	 * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// ログイン情報
		LoginSession login = super.getLoginSession(request);
		// プロファイル
		Profile profile = super.getProfile(request);
		// アクションフォームの取得
		T001Form form = (T001Form) super.getActionForm(request,
						"jp.co.fj.keinavi.forms.txt_out.T001Form");

		// テキスト出力トップからの遷移なら対象年度と対象模試を更新
		if ("t001".equals(super.getBackward(request))
			|| "t101".equals(super.getBackward(request))
			|| "t201".equals(super.getBackward(request))) {

			// NULLが来る可能性があるので排除
			if (form.getTargetYear() != null) {
				profile.setTargetYear(form.getTargetYear());
				profile.setTargetExam(form.getTargetExam());
			}
		// そうでなければ設定値をロード
		} else {
			form.setTargetYear(profile.getTargetYear());
			form.setTargetExam(profile.getTargetExam());
			form.setScrollX("0");
			form.setScrollY("0");
		}

		// チェックされたフォーム
		Set outItem = CollectionUtil.array2Set(form.getOutItem());

//		// 一括出力　出力形式初期化
//		profile.getItemMap("050100").put("1402", new Short("0"));

		// 転送元がt001かつ変更済み
		// プロファイルを保存
		if ("t001".equals(getBackward(request)) && "1".equals(form.getChanged())) {
			//	成績概況:一括出力対象
			profile.getItemMap("050201").put("1301",outItem.contains("CountScore")	? new Short("1") : new Short("0"));
			//	成績概況:他校比較
			profile.getItemMap("050201").put("0403",outItem.contains("CountScoreSchool")	? new Short("1") : new Short("0"));
			//	成績概況:クラス比較
			profile.getItemMap("050201").put("0503",outItem.contains("CountScoreClass")	? new Short("1") : new Short("0"));

			// 偏差値分布:一括出力対象
			profile.getItemMap("050202").put("1301",outItem.contains("CountDiv")? new Short("1"): new Short("0"));
			//	偏差値分布:他校比較
			profile.getItemMap("050202").put("0403",outItem.contains("CountDivSchool")	? new Short("1") : new Short("0"));
			//	偏差値分布:クラス比較
			profile.getItemMap("050202").put("0503",outItem.contains("CountDivClass")	? new Short("1") : new Short("0"));

			// 設問別成績:一括出力対象
			profile.getItemMap("050203").put("1301",outItem.contains("CountQue")? new Short("1"): new Short("0"));
			//	設問別成績 :他校比較
			profile.getItemMap("050203").put("0403",outItem.contains("CountQueSchool")	? new Short("1") : new Short("0"));
			//	設問別成績 :クラス比較
			profile.getItemMap("050203").put("0503",outItem.contains("CountQueClass")	? new Short("1") : new Short("0"));

			// 志望大学評価別人数:一括出力対象
			profile.getItemMap("050204").put("1301",outItem.contains("CountRating")? new Short("1"): new Short("0"));
			//	設問別成績 :他校比較
			profile.getItemMap("050204").put("0403",outItem.contains("CountRatingSchool")	? new Short("1") : new Short("0"));
			//	設問別成績 :クラス比較
			profile.getItemMap("050204").put("0503",outItem.contains("CountRatingClass")	? new Short("1") : new Short("0"));

			// 一括出力は共通の出力形式に格納
			profile.getItemMap("050100").put("1402", new Short ((form.getPackageTxtType()).toString()) );

			// 保存フラグを立てる
			profile.setChanged(true);

		// 転送元がt101かつ変更済み
		} else if ("t101".equals(getBackward(request)) && "1".equals(form.getChanged())) {
			// 模試別成績データ:一括出力対象
			profile.getItemMap("050301").put("1301",outItem.contains("IndExam")? new Short("1"): new Short("0"));
			// 設問別成績:一括出力対象
			profile.getItemMap("050302").put("1301",outItem.contains("IndQue")? new Short("1"): new Short("0"));
			// 模試別成績データ:一括出力対象
			profile.getItemMap("050303").put("1301",outItem.contains("IndExam2")? new Short("1"): new Short("0"));
			// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD START
			// 模試別成績データ:一括出力対象
			profile.getItemMap("050304").put("1301",outItem.contains("IndDescExam")? new Short("1"): new Short("0"));
			// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD END

			// 一括出力は共通の出力形式に格納
			profile.getItemMap("050100").put("1402", new Short ((form.getPackageTxtType()).toString()) );

			// 保存フラグを立てる
			if ("1".equals(form.getChanged())) profile.setChanged(true);

		// 転送元がt201かつ変更済み
		} else if ("t201".equals(getBackward(request)) && "1".equals(form.getChanged())) {
			// 一括出力は共通の出力形式に格納
			profile.getItemMap("050100").put("1402", new Short ((form.getPackageTxtType()).toString()) );

			// 保存フラグを立てる
			if ("1".equals(form.getChanged())) profile.setChanged(true);
		}

		// 教育委員会・営業（代行）・プライバシー保護学校については
		// 個人データの一括出力は行わない
		if (login.isBoard() || login.isDeputy() || login.isPrivacyProtection()) {
			// 模試別成績データ（旧形式）:一括出力対象
			profile.getItemMap("050301").put("1301", new Short("0"));
			// 設問別成績:一括出力対象
			profile.getItemMap("050302").put("1301", new Short("0"));
			// 模試別成績データ（新形式）:一括出力対象
			profile.getItemMap("050303").put("1301", new Short("0"));
			// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD START
			// （記述系模試）学力要素別成績データ:一括出力対象
			profile.getItemMap("050304").put("1301", new Short("0"));
			// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD END
		}

		// 一括出力は共通
		profile.getItemMap("050100").put("1401", form.getPackageTxtType());

		/* 2004/11/23 T.Yamada
		 * デフォルトでプロファイルに宣言していないため、nullで落ちることがある
		 * 今更DefaultProfileを変更できないので、ここでnullチェックをして
		 * nullならデフォルト値をセットする
		 */
		if(profile.getItemMap("050201").get("0403") == null){
			profile.getItemMap("050201").put("0403",new Short("0"));
		}
		if( profile.getItemMap("050201").get("0503") == null){
			profile.getItemMap("050201").put("0503",new Short("0"));
		}
		if(profile.getItemMap("050100").get("1402") == null){
			profile.getItemMap("050100").put("1402", new Short("1"));
		}
		if(profile.getItemMap("050201").get("1402") == null){
			profile.getItemMap("050201").put("1402", new Short("1"));
		}
		if(profile.getItemMap("050201").get("1403") == null){
			profile.getItemMap("050201").put("1403", new String(""));
		}

		// 転送先がトップ画面
		if ("t001".equals(getForward(request)) || "t101".equals(getForward(request)) || "t201".equals(getForward(request))) {

			// 模試セッション
			final ExamSession examSession = super.getClassExamSession(request);
			request.setAttribute(ExamSession.SESSION_KEY, examSession);

			// 担当クラスをセットする
			super.setupChargeClass(request);

			// アクションフォームをrequestへセット
			request.setAttribute("form", form);

			// 対象模試
			ExamData exam = examSession.getExamData(form.getTargetYear(), form.getTargetExam());

			// 対象模試が存在しなければリストの最上位
			if (exam == null) {
				String year = null;

				// 対象年度があれば採用
				if (examSession.getExamMap().containsKey(form.getTargetYear())) {
					year = form.getTargetYear();

				// なければ対象年度は最新年度
				} else if (examSession.getYears().length > 0) {
					year = examSession.getYears()[0];
				}

				// 年度がなければ（模試がなければ）空文字列を入れておく
				if (year == null) {
					form.setTargetYear("");
					form.setTargetExam("");

				// あるなら一番上の模試
				} else {
					exam = (ExamData) examSession.getExamList(year).get(0);
					form.setTargetYear(exam.getExamYear());
					form.setTargetExam(exam.getExamCD());
				}
			}

			Connection con = null; // DBコネクション
			try {
				con = getConnectionPool(request);
				con.setAutoCommit(false);

				// 共通項目設定状況Bean
				ComSetStatusBean bean = new ComSetStatusBean();
				bean.setConnection(null, con);
				bean.setProfile(profile);
				bean.setExam(exam);
				bean.setMode(4);
				bean.setLogin(login);
				bean.setExamSession(examSession);
				bean.execute();
				request.setAttribute("ComSetStatusBean", bean);

				// 出力対象テーブルマップ
				request.setAttribute("TextInfoBean", TextInfoBean.getInstance(exam));

				// 一時表クリア
				con.commit();

			} catch (final Exception e) {
				rollback(con);
				throw createServletException(e);
			} finally {
				releaseConnectionPool(request, con);
			}

			// センターリサーチフラグ
			request.setAttribute("isCenterResearch",
					Boolean.valueOf(KNUtil.isCenterResearch(exam)));

			// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD START
			// 記述系フラグ
			request.setAttribute("isWrtnExam",
					Boolean.valueOf(KNUtil.isWrtnExam(exam)));
// 2019/11/08 QQ)Tanioka 共通テスト対応 ADD START
                        // 記述系2フラグ
                        request.setAttribute("isWrtnExam2",
                                        Boolean.valueOf(KNUtil.isWrtnExam2(exam)));
// 2019/11/08 QQ)Tanioka 共通テスト対応 ADD END
			// プライムステージ系フラグ
                        request.setAttribute("isPStage",
                                        Boolean.valueOf(KNUtil.isPStage(exam)));
			// 2019/09/11 QQ)Tanioka （記述系模試）学力要素別成績データ追加 ADD END

			// テキスト出力トップ画面へ遷移
			super.forward(request, response, JSP_T001);

		// 不明なら転送
		} else {
			// requestスコープの模試セッションは消しておく
			request.removeAttribute(ExamSession.SESSION_KEY);
			super.forward(request, response, SERVLET_DISPATCHER);
		}

	}

}
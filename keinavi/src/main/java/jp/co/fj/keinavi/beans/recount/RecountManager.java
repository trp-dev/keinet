package jp.co.fj.keinavi.beans.recount;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

/**
 * 
 * 再集計処理マネージャ
 * 
 * 2007.08.07	[新規作成]
 * 				jp.co.fj.keinavi.beans.recount.Recount をリファクタリング
 * 
 * 
 * @author Yoshimoto KAWAI - TOTEC
 */
public class RecountManager {

	/**
	 * 再集計処理を登録する
	 * 
	 * @param con DBコネクション
	 * @param schoolCd 対象学校CD
	 * @throws Exception SQL例外
	 */
	public boolean regist(final Connection con,
			final String schoolCd) throws Exception {
		
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(
					QueryLoader.getInstance().getQuery("recount04"));
			ps.setString(1, schoolCd);
			ps.setString(2, schoolCd);
			return ps.executeUpdate() > 0;
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	/**
	 * 再集計処理中であるか
	 * 
	 * @param con DBコネクション
	 * @param schoolCd 対象学校CD
	 * @throws Exception SQL例外
	 */
	public boolean isProcess(final Connection con,
			final String schoolCd) throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement(
					QueryLoader.getInstance().getQuery("recount05"));
			ps.setString(1, schoolCd);
			rs = ps.executeQuery();
			return rs.next();
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

}
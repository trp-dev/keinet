package jp.co.fj.keinavi.beans.login;

import java.io.IOException;
import java.security.cert.X509Certificate;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.LoginSession;
import jp.co.fj.keinavi.data.login.Certification;
import jp.co.fj.keinavi.data.login.PactSchoolData;
import jp.co.fj.keinavi.interfaces.ILogin;
import jp.co.fj.keinavi.servlets.KNServletException;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.date.ShortDateUtil;
import jp.co.fj.keinavi.util.message.MessageLoader;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 契約校向けログイン認証Bean
 *
 *
 * 2005.10.07	Yoshimoto KAWAI - TOTEC
 * 				機能エンハンス60対応
 *
 *
 * @author kawai
 *
 */
public class SchoolLoginBean extends DefaultBean {

	private LoginSession loginSession; // ログインセッション
	private X509Certificate[] cert; // 証明書オブジェクト
	private String account; // 高校ID
	private String password; // パスワード
	private String simple; // メニューモード
	private String user; // ユーザID

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {

		// 今日の日付
		final String today = new ShortDateUtil().date2String();
		// 体験版かどうか
		final boolean trial = this.account.equals(KNCommonProperty.getTrialUserID());

		// 契約校マスタを取得する
		PactSchoolData pact = new PactSchoolData(); // 契約校データ
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			ps1 = conn.prepareStatement(QueryLoader.getInstance().load("w04").toString());
			ps1.setString(1, account);
			rs1 = ps1.executeQuery();

			// データを詰める
			if (rs1.next()) {
				pact.setSchoolCD(rs1.getString(1));
				pact.setPactDiv(rs1.getShort(2));
				pact.setCertCN(rs1.getString(3));
				pact.setCertOU(rs1.getString(4));
				pact.setCertO(rs1.getString(5));
				pact.setCertL(rs1.getString(6));
				pact.setCertST(rs1.getString(7));
				pact.setCertC(rs1.getString(8));
				pact.setCertUserDiv(rs1.getShort(9));
				pact.setPasswd(rs1.getString(10));
				pact.setFeatprovMode(rs1.getShort(11));
				pact.setValidDate(rs1.getString(12));
				pact.setCertFlag(rs1.getString(13));
				pact.setPactTerm(rs1.getString(14));

			// 無ければ例外
			} else {
				final KNServletException e = createServletException(
						"契約校データの取得に失敗しました。");
				e.setErrorMessage(getFailureMessage());
				throw e;
			}
		} finally {
			DbUtils.closeQuietly(rs1);
			DbUtils.closeQuietly(ps1);
		}

		// 契約期限のチェック
		if (pact.getPactTerm() == null || today.compareTo(pact.getPactTerm()) > 0) {
			final KNServletException e = createServletException(
					"契約期限が切れています。");
			e.setErrorMessage("契約期限が切れています。");
			throw e;
		}

		// 証明書を認証する
		// 証明書がないか体験版の場合
		if (cert == null || cert.length == 0 || trial) {
			// TODO テスト用
			if ("00000011".equals(account)) loginSession.setCert(true);
			if ("00000012".equals(account)) loginSession.setCert(true);
			if ("00000013".equals(account)) loginSession.setCert(true);
			if ("00000014".equals(account)) loginSession.setCert(true);
			//if ("testkw  ".equals(account)) loginSession.setCert(true);
			//if ("ke24197z".equals(account)) loginSession.setCert(true);
			//if ("ke01101a".equals(form.getAccount())) login.setCert(true);

		// 証明書がある場合
		} else {
			// 証明書有効フラグのチェック
			if (!"1".equals(pact.getCertFlag())) {
				final KNServletException e = createServletException(
						"証明書有効フラグが無効です。");
				e.setErrorMessage(getFailureMessage());
				throw e;
			}

			Certification c = new Certification(cert[0]);

			// 認証失敗ならエラー
			if (!pact.equals(c)) {
				System.out.println("DB: " + pact.toString());
				System.out.println("Cert: " + c.toString());
				final KNServletException e = createServletException(
						"証明書の比較に失敗しました。");
				e.setErrorMessage(MessageLoader.getInstance().getMessage("w004a"));
				throw e;
			}

			// 証明書ありました
			loginSession.setCert(true);
		}

		// 証明書があればセッション数をカウントアップする
		if (loginSession.isCert()) {
			LoginTransactionBean bean = new LoginTransactionBean();
			bean.setConnection(DBName, conn);
			bean.setUserID(account);
			bean.setCountUp(true);
			bean.execute();
		}

		// ログインセッションを作る
		// 体験版
		if (trial) {
			loginSession.setLoginID(this.account);
			loginSession.setAccount(this.user);
			loginSession.setUserID("23061");
			loginSession.setUserMode(ILogin.SCHOOL_NORMAL);
			loginSession.setUserName("河合塾学園");
			loginSession.setPactDiv(pact.getPactDiv());
			loginSession.setUserDiv(pact.getCertUserDiv());
			loginSession.setFeatprovMode(pact.getFeatprovMode());
			loginSession.setSimple("1".equals(this.simple));
			loginSession.setDbKey(KNCommonProperty.getFDBSID());
			loginSession.setPrefCD("23");
			loginSession.setCert(true);
			loginSession.setTrialMode((short) 1);
			loginSession.setManager(false);
			loginSession.setMaintainer(false);
		// それ以外
		} else {
			PreparedStatement ps2 = null;
			ResultSet rs2 = null;
			try {
				ps2 = conn.prepareStatement(
					"SELECT bundlename, stopdate, newgetdate, facultycd FROM school WHERE bundlecd = ?");
				ps2.setString(1, pact.getSchoolCD());
				rs2 = ps2.executeQuery();

				if (rs2.next()) {
					// 停止日のチェック
					if (rs2.getString(2) != null && rs2.getString(2).compareTo(today + "0000") < 0) {
						final KNServletException e = createServletException(
								"停止日を過ぎています。");
						e.setErrorMessage(getFailureMessage());
						throw e;
					}

					// 新規取得日のチェック
					if (rs2.getString(3) != null && rs2.getString(3).compareTo(today + "0000") >= 0) {
						final KNServletException e = createServletException(
								"新規取得日を過ぎていません。");
						e.setErrorMessage(getFailureMessage());
						throw e;
					}

					loginSession.setLoginID(this.account);
					loginSession.setAccount(this.user);
					loginSession.setUserID(pact.getSchoolCD());
					loginSession.setUserMode(ILogin.SCHOOL_NORMAL);
					loginSession.setUserName(rs2.getString(1));
					loginSession.setPactDiv(pact.getPactDiv());
					loginSession.setUserDiv(pact.getCertUserDiv());
					loginSession.setFeatprovMode(pact.getFeatprovMode());
					loginSession.setSimple("1".equals(this.simple));
					loginSession.setPrefCD(rs2.getString(4));
					loginSession.setDbKey(KNCommonProperty.getNDBSID());
					loginSession.setPrivacyProtection();
				} else {
					final KNServletException e = createServletException(
							"学校マスタの取得に失敗しました。");
					e.setErrorMessage(getFailureMessage());
					throw e;
				}
			} finally {
				DbUtils.closeQuietly(rs2);
				DbUtils.closeQuietly(ps2);
			}
		}

		// 利用者認証を行う
		final UserAuthBean bean = new UserAuthBean(loginSession, password);
		bean.setConnection(null, conn);
		bean.execute();

		// 非契約校の有効・無効チェック
		if (!loginSession.isPact() && !loginSession.isMaintainer()) {
			final KNServletException e = createServletException(
					"有効利用者チェックエラー");
			e.setErrorMessage("入力した利用者IDは現在無効になっています。");
			throw e;
		}
	}

	/**
	 * 認証失敗時のエラーメッセージを取得する
	 * @return
	 * @throws IOException
	 */
	private String getFailureMessage() throws IOException {
		if ("1".equals(this.simple)) {
			return MessageLoader.getInstance().getMessage("w003a");
		} else {
			return MessageLoader.getInstance().getMessage("w028a");
		}
	}

	// ログイン例外オブジェクトを作る
	// [学校ID 利用者ID メッセージ]
	private KNServletException createServletException(final String message) {

		final StringBuffer buff = new StringBuffer();
		buff.append(account);
		buff.append(" ");
		if (user != null) {
			buff.append(user);
			buff.append(" ");
		}
		buff.append(message);

		final KNServletException e = new KNServletException(buff.toString());
		e.setErrorCode("1");
		return e;
	}

	/**
	 * @param string
	 */
	public void setAccount(String string) {
		account = string;
	}

	/**
	 * @param certificates
	 */
	public void setCert(X509Certificate[] certificates) {
		cert = certificates;
	}

	/**
	 * @param string
	 */
	public void setPassword(String string) {
		password = string;
	}

	/**
	 * @param string
	 */
	public void setSimple(String string) {
		simple = string;
	}

	/**
	 * @param session
	 */
	public void setLoginSession(LoginSession session) {
		loginSession = session;
	}

	public void setUser(String user) {
		this.user = user;
	}
}

/*
 * 作成日: 2004/07/22
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
package jp.co.fj.keinavi.data.individual;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Administrator
 *
 * 教科マスタテーブルの化身
 * プラスその他付属機能
 * 
 */
public class CourseData implements Serializable {

	private String courseCd;
	private String courseName;
	
	//この教科に所属する科目データの集合
	private Collection examSubjectDatas;
	
	/**
	 * この教科に所属する科目の科目コードの
	 * 最大値を返す
	 * @param 教科コード
	 * @return 科目最大値
	 */
	public static String getMaxCd(String cd){
		String index = cd.substring(0, 1);
		return index.concat("999");
	}
	public String getMaxCd(){
		return CourseData.getMaxCd(getCourseCd());
	}
	/**
	 * この教科に所属する科目の科目コードの
	 * 最小値を返す
	 * @param 教科コード
	 * @return 科目最小値
	 */
	public static String getMinCd(String cd){
		String index = cd.substring(0, 1);
		return index.concat("001");
	}
	public String getMinCd(){
		return CourseData.getMinCd(getCourseCd());
	}
	
	/**
	 * @return
	 */
	public String getCourseCd() {
		return courseCd;
	}

	/**
	 * @return
	 */
	public String getCourseName() {
		return courseName;
	}

	/**
	 * @return
	 */
	public Collection getExamSubjectDatas() {
		return examSubjectDatas;
	}

	/**
	 * @param string
	 */
	public void setCourseCd(String string) {
		courseCd = string;
	}

	/**
	 * @param string
	 */
	public void setCourseName(String string) {
		courseName = string;
	}

	/**
	 * @param collection
	 */
	public void setExamSubjectDatas(Collection collection) {
		examSubjectDatas = collection;
	}

}

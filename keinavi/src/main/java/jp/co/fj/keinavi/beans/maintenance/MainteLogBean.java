package jp.co.fj.keinavi.beans.maintenance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.keinavi.data.maintenance.KNStudentData;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * メンテログBean
 *
 * 2006.11.08	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 *
 */
public class MainteLogBean extends DefaultBean {

	// 学校コード
	private final String schoolCd;

	// コンストラクタ
	public MainteLogBean(final String pSchoolCd) {
		schoolCd = pSchoolCd;
	}

	/**
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws Exception {
	}

	/**
	 * 新規登録ログを出力
	 *
	 * @param individualId 個人ID
	 * @param year 年度
	 * @param grade 学年
	 * @param className クラス名
	 * @param classNo クラス番号
	 * @param nameKana カナ氏名
	 * @throws SQLException SQL例外
	 */
	void insertLog(final String individualId, final String year,
			final int grade, final String className, final String classNo,
			final String nameKana) throws SQLException {
		output(individualId, year, grade, className, classNo, nameKana,
				"生徒情報を新規登録");
	}

	/**
	 * 更新ログを出力
	 *
	 * @param individualId 個人ID
	 * @param year 年度
	 * @param grade 学年
	 * @param className クラス名
	 * @param classNo クラス番号
	 * @param nameKana カナ氏名
	 * @throws SQLException SQL例外
	 */
	void updateLog(final String individualId, final String year,
			final int grade, final String className, final String classNo,
			final String nameKana) throws SQLException {
		output(individualId, year, grade, className, classNo, nameKana,
				"生徒情報を更新");
	}

	/**
	 * 削除ログを出力
	 *
	 * @param individualId 削除する個人Id
	 * @throws SQLException SQL例外
	 */
	void deleteLog(final String[] individualId) throws SQLException {
		for (int i = 0; i < individualId.length; i++) {
			final KNStudentData data = getStudentData(individualId[i]);
			output(individualId[i], data.getYear(),
					data.getGrade(), data.getClassName(),
					data.getClassNo(), data.getNameKana(),
					"生徒情報を削除");
		}
	}

	/**
	 * 統合ログを出力
	 *
	 * @param mainId 主生徒ID
	 * @param subId 従生徒ID
	 * @throws SQLException SQL例外
	 */
	void studentIntegrationLog(final String mainId,
			final String subId) throws SQLException {

		// 主生徒
		final KNStudentData main = getStudentData(mainId);
		output(main.getIndividualId(), main.getYear(),
				main.getGrade(), main.getClassName(),
				main.getClassNo(), main.getNameKana(),
				"従生徒情報[個人ID:" + subId + "]を取り込み");

		// 従生徒
		final KNStudentData sub = getStudentData(subId);
		output(sub.getIndividualId(), sub.getYear(),
				sub.getGrade(), sub.getClassName(),
				sub.getClassNo(), sub.getNameKana(),
				"主生徒情報[個人ID:" + mainId + "]へ移行");
	}

	/**
	 * 結合ログを出力
	 *
	 * @param examYear 模試年度
	 * @param examCd 模試コード
	 * @param beforeId 変更前の個人Id
	 * @param afterId 変更後の個人Id
	 * @throws SQLException SQL例外
	 */
	void examIntegrationLog(final String examYear,
			final String examCd, final String beforeId,
			final String afterId) throws SQLException {

		// 変更後生徒のログ
		final KNStudentData after = getStudentData(afterId);
		output(afterId, after.getYear(), after.getGrade(),
				after.getClassName(), after.getClassNo(),
				after.getNameKana(), "模試情報[" + examYear + ","
				+ examCd + "]を個人ID[" + beforeId + "]から取り込み");

		// 変更前生徒のログ
		final KNStudentData before = getStudentData(beforeId);
		output(beforeId, before.getYear(), before.getGrade(),
				before.getClassName(), before.getClassNo(),
				before.getNameKana(), "模試情報[" + examYear + ","
				+ examCd + "]を個人ID[" + afterId + "]へ移行");
	}

	// 2015/10/16 QQ)Nishiyama デジタル証明書対応 ADD START
	/**
	 * 汎用ログを出力
	 *
	 * @param logData 操作内容
	 * @throws SQLException SQL例外
	 */
	public void etcLog(String logData) throws SQLException {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("INSERT INTO maintelog values(?,systimestamp,?)");
			ps.setString(1, schoolCd);
			ps.setString(2, logData);
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}
	// 2015/10/16 QQ)Nishiyama デジタル証明書対応 ADD END

	// 個人IDから生徒情報を取得
	private KNStudentData getStudentData(
			final String individualId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
					QueryLoader.getInstance().getQuery("m29"));
			ps.setString(1, schoolCd);
			ps.setString(2, individualId);
			rs = ps.executeQuery();
			if (rs.next()) {
				final KNStudentData data = new KNStudentData(0);
				data.setIndividualId(individualId);
				data.setYear(rs.getString(1));
				data.setGrade(rs.getInt(2));
				data.setClassName(rs.getString(3));
				data.setClassNo(rs.getString(4));
				data.setNameKana(rs.getString(5));
				return data;
			} else {
				return null;
			}
		} finally {
			DbUtils.closeQuietly(null, ps, rs);
		}
	}

	// ログ出力
	private void output(final String individualId, final String year,
			final int grade, final String className, final String classNo,
			final String nameKana, final String info) throws SQLException {

		final StringBuffer buff = new StringBuffer();

		// 個人ID
		buff.append(individualId);
		buff.append(',');

		// 年度
		buff.append(year);
		buff.append(',');

		// 学年
		if (grade >= 0) {
			buff.append(grade);
		}
		buff.append(',');

		// クラス
		buff.append(className);
		buff.append(',');

		// クラス番号
		buff.append(classNo);
		buff.append(',');

		// カナ氏名
		buff.append(nameKana);
		buff.append(',');

		// 操作内容
		buff.append('"');
		buff.append(info);
		buff.append('"');

		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("INSERT INTO "
					+ "maintelog values(?,systimestamp,?)");
			ps.setString(1, schoolCd);
			ps.setString(2, buff.toString());
			ps.executeUpdate();
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

}

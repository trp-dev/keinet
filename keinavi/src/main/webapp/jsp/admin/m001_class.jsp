<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<!--複合クラス管理-->
<table border="0" cellpadding="0" cellspacing="0" width="272">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="272">
<tr>
<td width="270" height="36" bgcolor="#E5EEF3">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td><a href="#"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→"></a></td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><a href="javascript:ctrlSubmit(4)"><b class="text14">複合クラス管理</b></a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="270" bgcolor="#FFFFFF" align="center">
<table border="0" cellpadding="0" cellspacing="0" width="258">
<tr>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td width="250" height="70" valign="top"><span class="text12-hh">複数のクラスを１つの別のクラスとして集計するための指定を行います。</span></td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="258"><img src="./shared_lib/img/parts/dot_blue.gif" width="258" height="1" border="0" alt="" vspace="5"><br></td>
</tr>
<tr>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td width="258"><span class="text10">最終更新日：<c:out value="${compClassLastDate}" /></span></td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/複合クラス管理-->

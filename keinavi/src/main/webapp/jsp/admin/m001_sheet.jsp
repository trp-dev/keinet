<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<!--受験届修正-->
<table border="0" cellpadding="0" cellspacing="0" width="272">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="272">
<tr>
<td width="270" height="36" bgcolor="#E5EEF3">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td><a href="#"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→"></a></td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
<%--	<td><a href="javascript:ctrlSubmit(3)"><b class="text14">（生徒情報管理−模試結合先変更）</b></a></td>	--%>
<c:choose>
<c:when test="${ LoginSession.trial }">
	<td><b class="text14"><font color="#2986B1"><u>（生徒情報管理−模試結合先変更）</u></font></b></td>
</c:when>
<c:otherwise>
	<td><a href="javascript:ctrlSubmit(3)"><b class="text14">（生徒情報管理−模試結合先変更）</b></a></td>
</c:otherwise>
</c:choose>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
</tr>
</table>
</td>
</tr>
<tr>
<td width="270" bgcolor="#FFFFFF" align="center">
<table border="0" cellpadding="0" cellspacing="0" width="258">
<tr>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td width="250" height="70" valign="top"><span class="text12-hh">ある生徒に別の生徒の模試情報が結合しているケースについて、ここで変更し正しくすることができます。
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="258"><img src="./shared_lib/img/parts/dot_blue.gif" width="258" height="1" border="0" alt="" vspace="5"><br></td>
</tr>
<tr>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td width="258"><span class="text10">最終更新日：<c:out value="${correctionLastDate}" /></span></td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/受験届修正-->

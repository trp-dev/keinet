<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／メンテナンス</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<script type="text/javascript" src="./js/JString.js"></script>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
	<%@ include file="/jsp/script/ctrl_submit.jsp" %>

	function onCancel() {
		document.forms[0].actionMode.value = "";
		document.forms[0].forward.value = "m301";
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}

	function Regist() {
		var message = "";

		// 入力チェック（プロファイル名）
		if (document.forms[0].className.value == "") {
			message += "複合クラス名を入力してください。\n";
		}

		var js = new JString();

		// 入力チェック（複合クラス名の文字数）
		js.setString(document.forms[0].className.value);
		if (js.getLength() > 28) {
			message += "複合クラス名は全角１４文字までです。\n";
		}

		// 入力チェック（\マーク不可）
		if (new Validator().isWQuotesOrComma(document.forms[0].className.value)) {
			message += "複合クラス名を正しく入力してください。\n";
		}

		// 入力チェック（コメントの文字数）
		js.setString(document.forms[0].comment.value);
		if (js.getLength() > 100) {
			message += "コメントは全角５０文字までです。\n";
		}

		// クラスチェック数
		if (new FormUtil().countChecked(document.forms[0].classList) == 0) {
			message += "クラスを選択してください。\n";
		}

		if (message != "") {
			alert(message);
			return;
		}

		document.forms[0].actionMode.value = "<c:out value="${ form.actionMode }" />";
		document.forms[0].forward.value = "m301";
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}

	function init() {
		startTimer();
	}

// -->
</script>
</head>

<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="M302Servlet" />" method="POST" onsubmit="return false">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="m302">
<input name="actionMode" type="hidden" value="">
<input name="targetYear" type="hidden" value="<c:out value="${form.targetYear}" />">
<input name="targetGrade" type="hidden" value="<c:out value="${form.targetGrade}" />">
<input name="targetClassgCd" type="hidden" value="<c:out value="${form.targetClassgCd}" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">
<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="174"><img src="./mainte/img/ttl_mainte.gif" width="140" height="24" border="0" alt="メンテナンス"><br></td>
<td width="1" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="723"><img src="./mainte/img/ttl_mainte_read.gif" width="326" height="16" border="0" alt="生徒情報を一元的に管理することができます。"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->
<!--コンテンツメニュー-->
<%@ include file="/jsp/admin/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--複合クラス管理（編集）-->
<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="13" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="814" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">複合クラス管理（編集）</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--説明-->
<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="846"><span class="text14-hh">各項目の編集後、「登録」ボタンを押してください。<br>
</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="826">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="817">

<table border="0" cellpadding="0" cellspacing="0" width="817">
<tr valign="top">
<td colspan="4" width="817" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="817" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="817" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="817" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="804" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">複合クラス名</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="817" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="817" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->
<!--複合クラス名-->
<table border="0" cellpadding="0" cellspacing="0" width="826">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="826">
<tr valign="top">
<td width="824" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="796">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="13" cellspacing="1" width="796">
<tr valign="top">
<td width="794" bgcolor="#FFFFFF" align="center">

<table border="0" cellpadding="4" cellspacing="2" width="674">
<!--クラス名-->
<tr valign="top">
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">クラス名</b></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="30" name="className" value="<c:if test="${ not empty CompositeBean.compositeList }"><c:out value="${ CompositeBean.compositeList[0].className }" /></c:if>" style="width:200px;" maxlength="28"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""></td>
<td><span class="text12">全角14文字以内</span></td>
</tr>
</table>
</td>
</tr>
<!--/クラス名-->
<!--コメント-->
<tr valign="top">
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">コメント</b></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><textarea rows="3" cols="40" name="comment" style="width:500px;"><c:if test="${ not empty CompositeBean.compositeList }"><c:out value="${ CompositeBean.compositeList[0].comment }" /></c:if></textarea><br>
<span class="text12">※全角50文字まで</span></td>
</tr>
</table>
</td>
</tr>
<!--/コメント-->
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/複合クラス名-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="826">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="817">

<table border="0" cellpadding="0" cellspacing="0" width="817">
<tr valign="top">
<td colspan="4" width="817" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="817" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="817" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="817" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="804" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">登録クラス　</b><font class="text12" style="color:#FFFFFF;">対象年度&nbsp;：&nbsp;<c:out value="${ form.targetYear }" />　対象学年&nbsp;：&nbsp;<c:out value="${ form.targetGrade }" />年</font></td>
</tr>
<tr valign="top">
<td colspan="4" width="817" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="817" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->
<!--登録クラス-->
<table border="0" cellpadding="0" cellspacing="0" width="826">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="826">
<tr valign="top">
<td width="824" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="796">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="13" cellspacing="1" width="796">
<tr valign="top">
<td width="794" bgcolor="#FFFFFF" align="center">

<!--リスト-->

<table border="0" cellpadding="2" cellspacing="2" width="674">
<tr>
<td width="6%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">&nbsp;</span></td>
<td width="26%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">クラス名</span></td>
</tr>
</table>
</td>
<td width="11%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">人数</span></td>
</tr>
</table>
</td>
<td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">&nbsp;</span></td>
<td width="27%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">クラス名</span></td>
</tr>
</table>
</td>
<td width="11%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">人数</span></td>
</tr>
</table>
</td>
</tr>
<!--set-->

<c:if test="${ not empty CompositeBean.compositeList }">
<c:forEach begin="1" end="${ CompositeBean.halfSize }" varStatus="status">
	<c:set var="index" value="${ (status.index - 1) * 2 }" />
	<tr height="27">
	<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ index + 1 }" /></span></td>
	<td bgcolor="#F4E5D6" align="center">
	<input type="checkbox" name="classList" value="<c:out value="${ CompositeBean.compositeList[index].clas }" />"<c:if test="${ CompositeBean.compositeList[index].selectJudge }"> checked</c:if>></td>
	<td bgcolor="#F4E5D6">
		<table border="0" cellpadding="4" cellspacing="0">
		<tr>
		<td><span class="text12"><c:out value="${ CompositeBean.compositeList[index].clas }" /></span></td>
		</tr>
		</table>
	</td>
	<td bgcolor="#F4E5D6" align="right" nowrap>
		<table border="0" cellpadding="4" cellspacing="0">
		<tr>
		<td><span class="text12"><c:out value="${ CompositeBean.compositeList[index].number }" />人</span></td>
		</tr>
		</table>
	</td>
	<td bgcolor="#FFFFFF">&nbsp;</td>
	<c:choose>
		<c:when test="${ status.last && CompositeBean.size % 2 == 1 }">
		<td bgcolor="#E1E6EB" align="center"><span class="text12">&nbsp;</span></td>
		<td bgcolor="#F4E5D6" align="center"><span class="text12">&nbsp;</span></td>
		<td bgcolor="#F4E5D6"><span class="text12">&nbsp;</span></td>
		<td bgcolor="#F4E5D6" align="right" nowrap><span class="text12">&nbsp;</span></td>
		</c:when>
		<c:otherwise>
		<c:set var="index" value="${ (status.index - 1) * 2  + 1 }" />
		<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ index + 1 }" /></span></td>
		<td bgcolor="#F4E5D6" align="center">
		<input type="checkbox" name="classList" value="<c:out value="${ CompositeBean.compositeList[index].clas }" />"<c:if test="${ CompositeBean.compositeList[index].selectJudge }"> checked</c:if>></td>
		<td bgcolor="#F4E5D6">
			<table border="0" cellpadding="4" cellspacing="0">
			<tr>
			<td><span class="text12"><c:out value="${ CompositeBean.compositeList[index].clas }" /></span></td>
			</tr>
			</table>
		</td>
		<td bgcolor="#F4E5D6" align="right" nowrap>
			<table border="0" cellpadding="4" cellspacing="0">
			<tr>
			<td><span class="text12"><c:out value="${ CompositeBean.compositeList[index].number }" />人</span></td>
			</tr>
			</table>
		</td>
		</c:otherwise>
	</c:choose>
	</tr>
</c:forEach>
</c:if>

<!--/set-->
</table>
<!--/リスト-->


</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/登録クラス-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/複合クラス管理（編集）-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/概要-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><input type="button" value="キャンセル" class="text12" style="width:180px;" onClick="javascript:onCancel()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td><input type="button" value="登録" class="text12" style="width:180px;" onClick="javascript:Regist()"<c:if test="${ empty CompositeBean.compositeList }"> disabled=disabled</c:if>></td> --%>
<td><input type="button" value="登録" class="text12" style="width:180px;" onClick="javascript:Regist()"<c:if test="${ empty CompositeBean.compositeList || LoginSession.trial }"> disabled=disabled</c:if>></td>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td align="center"><span class="text12">※キャンセルを押すと、登録せずに一覧画面に戻ります。</span></td>
</tr>
</table>
</div>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/admin/footer.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

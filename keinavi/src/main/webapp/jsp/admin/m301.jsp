<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／メンテナンス</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
	<%@ include file="/jsp/script/ctrl_submit.jsp" %>

    var gradeData = new Array();
    <c:forEach var="grade" items="${Combo.gradeDataList}" varStatus="status">
     gradeData[<c:out value="${status.index}" />] = new Array(
        "<c:out value="${grade.year}" />",
        "<c:out value="${grade.grade}" />");
    </c:forEach>

    // 学年のコンボを初期化する
    function initGrade(initial) {
        var form =  document.forms[0];

        // 年度の値
        var year = getSelectedValue(form.targetYear);
        if (year == "") return;

        var select = document.forms[0].targetGrade;
        clearOptions(select);

        for (var i=0; i<gradeData.length; i++) {
            if (gradeData[i][0] == year) {
                var o = new Option(gradeData[i][1] + "年", gradeData[i][1]);
                if (o.value == initial) o.selected = true;
                select.options[select.options.length] = o;
            }
        }
    }

    //（汎用的）選択されているセレクト部品の値を取得する
    function getSelectedValue(obj) {
        if (obj == null) return "";
        var index = obj.selectedIndex;
        if (index >= 0) return obj.options[index].value;
        return "";
    }

    // オプションクリア
    function clearOptions(obj) {
        if (!obj.options) return;
        for (var i=0; i<obj.options.length; i++) {
            obj.options[i] = null;
        }
        obj.options.length = 0;
    }

    function onUpd(i) {
        document.forms[0].target = "_self";
        document.forms[0].forward.value = "m302";
        document.forms[0].targetClassgCd.value = i;
        document.forms[0].actionMode.value = 13;
        document.forms[0].submit();
    }

    function onDel(i) {
        if (confirm("削除しますか？")) {
            document.forms[0].target = "_self";
            document.forms[0].forward.value = "m301";
            document.forms[0].targetClassgCd.value = i;
            document.forms[0].actionMode.value = 14;
            document.forms[0].submit();
        }
    }

    function onReg() {
        document.forms[0].target = "_self";
        document.forms[0].forward.value = "m302";
        document.forms[0].targetClassgCd.value = "9999999," + document.forms[0].targetYear.value + "," + document.forms[0].targetGrade.value;
        document.forms[0].actionMode.value = 15;
        document.forms[0].submit();
    }

    function SearchView() {
        document.forms[0].target = "_self";
        document.forms[0].forward.value = "m301";
        document.forms[0].actionMode.value = 4;
        document.forms[0].submit();
    }

    function init() {
    	startTimer();
        initGrade("<c:out value="${form.targetGrade}" />");
    }

// -->
</script>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init();" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="M301Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="m301">
<input name="actionMode" type="hidden" value="">
<input name="targetClassgCd" type="hidden" value="">
<input name="schoolCd" type="hidden" value="">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">
<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="174"><img src="./mainte/img/ttl_mainte.gif" width="140" height="24" border="0" alt="メンテナンス"><br></td>
<td width="1" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="723"><img src="./mainte/img/ttl_mainte_read.gif" width="326" height="16" border="0" alt="生徒情報を一元的に管理することができます。"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->
<!--コンテンツメニュー-->
<%@ include file="/jsp/admin/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--複合クラス管理（一覧）-->
<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="13" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="814" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">複合クラス管理（一覧）</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--説明-->
<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="846"><span class="text14-hh">対象年度・学年を選択すると、複合クラスの一覧が表示されます。複合クラス構成の確認と編集は「編集」をクリックしてください。<br>また、複合クラスを新しく登録する場合は「新規登録」ボタンを押してください。<br>
</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--対象年度・学年・クラスの選択-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="2" cellspacing="1" width="846">
<tr>
<td width="844" bgcolor="#FFFFFF">

<table border="0" cellpadding="0" cellspacing="4" width="840">
<tr>
<td><b class="text12">対象年度・学年の選択</b></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="2" width="840">
<tr height="32">
<td width="734" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><span class="text12">年度&nbsp;：&nbsp;</span></td>
<td>
<select name="targetYear" class="text12" style="width:60px" onchange="initGrade()">
    <c:forEach var="year" items="${Combo.yearList}">
        <option value="<c:out value="${year}" />"<c:if test="${ form.targetYear == year }"> selected</c:if>><c:out value="${year}" /></option>
    </c:forEach>
</select>
</td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><span class="text12">学年&nbsp;：&nbsp;</span></td>
<td><select name="targetGrade" style="width:50px" class="text12"></select></td>
</tr>
</table>
</td>
<td width="100" bgcolor="#F4E5D6" align="center"><input type="button" value="一覧表示" class="text12" style="width:80px;" onClick="SearchView()"<c:if test="${ empty Combo.gradeDataList }"> disabled=disabled</c:if>></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--対象年度・学年・クラスの選択-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--ツールバー-->
<table border="0" cellpadding="4" cellspacing="0" width="846">
<tr>
<td width="846" bgcolor="#93A3AD">

<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="新規登録" class="text12" style="width:100px;" onClick="javascript:onReg()"<c:if test="${ empty Combo.gradeDataList }"> disabled=disabled</c:if>></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ツールバー-->

<!--リスト-->
<table id="foo" border="0" cellpadding="4" cellspacing="2" width="850">
<!--項目-->
<tr bgcolor="#CDD7DD">
<td width="5%" align="center">&nbsp;</td>
<td width="25%" align="center"><span class="text12">複合クラス名</span></td>
<td width="53%" align="center"><span class="text12">コメント</span></td>
<td width="7%" align="center"><span class="text12">人数</span></td>
<td width="5%" align="center">&nbsp;</td>
<td width="5%" align="center">&nbsp;</td>
</tr>

<c:forEach var="data" items="${CompositeBean.compositeList}" varStatus="status">
<tr height="27" bgcolor="#F4E5D6">
    <td align="center" bgcolor="#E1E6EB"><span class="text12"><c:out value="${ status.index + 1 }" /></span></td>
    <td><span class="text12"><c:out value="${data.className}" /></span></td>
    <td><span class="text12"><kn:pre><c:out value="${data.comment}" /></kn:pre></span></td>
    <td align="right"><span class="text12"><c:out value="${data.number}" />人</span></td>
    <td align="center"><c:if test="${ data.number > 0 }"><span class="text12"><a href="javascript:onUpd('<c:out value="${data.groupCode},${form.targetYear},${form.targetGrade}" />')">編集</a></span></c:if></td>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td align="center"><span class="text12"><a href="javascript:onDel('<c:out value="${data.groupCode},${form.targetYear},${form.targetGrade}" />')">削除</a></span></td> --%>
<c:choose>
<c:when test="${LoginSession.trial}">
	<td align="center"><span class="text12"><font color="#2986B1"><u>削除</u></font></span></td>
</c:when>
<c:otherwise>
    <td align="center"><span class="text12"><a href="javascript:onDel('<c:out value="${data.groupCode},${form.targetYear},${form.targetGrade}" />')">削除</a></span></td>
</c:otherwise>
</c:choose>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
</tr>
</c:forEach>

<!--/項目-->
</table>
<!--/リスト-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/複合クラス管理（一覧）-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/概要-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#FBD49F" align="center">

<input type="button" value="メンテナンス完了　（プロファイル選択画面へ）" class="text12" style="width:320px;" onClick="submitMenu('w002')">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/admin/footer.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／メンテナンス</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
	<%@ include file="/jsp/script/ctrl_submit.jsp" %>

	function againTotal() {
		if (confirm("模試データの再集計をしますか？")) {
			document.forms[0].target = "_self";
			document.forms[0].action="<c:url value="M001Servlet" />";
			document.forms[0].method = "POST";
			document.forms[0].actionMode.value = 0;
			document.forms[0].forward.value = "m001";
			document.forms[0].submit();
		}
	}

	function init() {
		startTimer();
		<%-- 再集計処理中ならリロード設定 --%>
		<c:if test="${ RecountProcess }">
		setTimeout("ctrlSubmit(1)", 1000 * 10);
		</c:if>
	}

// -->
</script>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="M001Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="<c:out value="${param.forward}" />">
<input type="hidden" name="actionMode" value="">
<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">
<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="174"><img src="./mainte/img/ttl_mainte.gif" width="140" height="24" border="0" alt="メンテナンス"><br></td>
<td width="1" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="523"><img src="./mainte/img/ttl_mainte_read.gif" width="326" height="16" border="0" alt="生徒情報を一元的に管理することができます。"><br></td>
<td width="200" align="right">
	<!--
		<img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→">　<a href="https://kjp.keinet.ne.jp/www/keinet/user-manage/henkou.cgi" target="_blank"><b class="text14">学校ＩＤ用パスワード変更</b></a>
	--><br>
</td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->
<!--コンテンツメニュー-->
<%@ include file="/jsp/admin/menu.jsp" %>
<!--/コンテンツメニュー-->

<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">
<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr valign="top">
<td width="778">

<c:if test="${ MenuSecurity.menu['701'] }">
<span class="text14-hh">下記のメニューからメンテナンス作業を行い、必要に応じて「模試データ再集計」を実行してください。<br></span>
</c:if>

</td>
</tr>
</table>
</div>
<!--/説明-->

<!--メニュー-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="15" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">

<c:choose>
<%-- プライバシー保護学校 --%>
<c:when test="${not MenuSecurity.menu['701'] }">
<td width="272"><%@ include file="/jsp/admin/m001_class.jsp" %></td>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="272"><img src="./shared_lib/img/parts/sp.gif" width="272" height="1" border="0" alt=""></td>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="272"><img src="./shared_lib/img/parts/sp.gif" width="272" height="1" border="0" alt=""></td>
</c:when>
<%-- それ以外 --%>
<c:otherwise>
<td width="272"><%@ include file="/jsp/admin/m001_student.jsp" %></td>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="272"><%@ include file="/jsp/admin/m001_sheet.jsp" %></td>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="272"><%@ include file="/jsp/admin/m001_class.jsp" %></td>
</c:otherwise>
</c:choose>

</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/メニュー-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--模試データ再集計-->
<c:if test="${ MenuSecurity.menu['701'] }">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="3" cellspacing="1" width="878">

<tr valign="top">
<td width="876" bgcolor="#FFFFFF">
<table border="0" cellpadding="3" cellspacing="0" width="870">
<tr>
<td bgcolor="#E1E6EB" align="center"><b class="text14">模試データ再集計</b>
</td>
</tr>
</table>
</td>
</tr>

<tr valign="top">
<td width="876" bgcolor="#FFFFFF" align="center">
<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="5" width="860">
<tr>
<td width="860" align="center"><span class="text12-hh">学年やクラス名を修正した場合は下のボタンをクリックしてください。</span></td>
</tr>
<tr>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
<%--
	<td width="860" align="center"><input type="button" value="再集計依頼ボタン" class="text12" style="width:160px;" onclick="againTotal()"<c:if test="${RecountProcess}">disabled=disabled</c:if>></td>
--%>
<td width="860" align="center"><input type="button" value="再集計依頼ボタン" class="text12" style="width:160px;" onclick="againTotal()"<c:if test="${RecountProcess || LoginSession.trial}">disabled=disabled</c:if>></td>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
</tr>
<c:if test="${RecountProcess}">
<tr>
<td width="860" align="center"><font class="text12-hh" color="#FF6E0E">再集計実行中のため、現在再集計依頼ボタンは押下できません。</font></td>
</tr>
</c:if>
<tr>
<td width="860"><img src="./shared_lib/img/parts/dot_blue.gif" width="860" height="1" border="0" alt="" vspace="5"><br></td>
</tr>
<tr>
<td width="860" align="center"><span class="text10"></span></td>
</tr>
</table>
<!--/ボタン-->
</td>
</tr>

</table>
</td>
</tr>
</table>
</c:if>
<!--/模試データ再集計-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/概要-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#FBD49F" align="center">

<input type="button" value="メンテナンス完了　（プロファイル選択画面へ）" class="text12" style="width:320px;" onclick="submitMenu('w002')">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="40" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/admin/footer.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

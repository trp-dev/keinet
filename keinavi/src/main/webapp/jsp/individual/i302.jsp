<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<%--
<jsp:useBean id="planUnivSearchBean" scope="request" class="jp.co.fj.keinavi.beans.individual.PlanUnivSearchBean" />
--%>
<jsp:useBean id="planUnivSearchBean" scope="request" type="jp.co.fj.keinavi.beans.individual.PlanUnivSearchBean" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript" src="./js/JString.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<SCRIPT type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>
	<%@ include file="/jsp/script/open_sample.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
	<%@ include file="/jsp/script/open_schedule_alert.jsp" %>
    <%@ include file="/jsp/script/clear_disabled.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/printDialog_close.jsp" %>
	 var printDialogPattern = "";
	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

	<%--
		この画面ではcheckboxだけdisabledを解除するため 、
		clear_disabled.jspの関数を上書きする
	--%>
	function clearElementDisabled(e, type) {
		if (e.disabled && e.type == "checkbox") {
			_disabled[_disabled.length] = e;
			e.disabled = false;
		}
	}

	<%-- 重複チェック用データ --%>
	var univData = new Array();
	<%-- 行の初期背景色 --%>
	var rowBgColor = new Array();
	<c:forEach var="univData" items="${planUnivSearchBean.nestedPlanUnivList}">
		<c:set var="rowid" value="row-${univData.univCd},${univData.facultyCd},${univData.deptCd}" />
		<c:choose>
			<c:when test="${univData.united}">
				rowBgColor["<c:out value="${rowid}" />"] = "#E1E6EB";
			</c:when>
			<c:otherwise>
				rowBgColor["<c:out value="${rowid}" />"] = "#F4E5D6";
			</c:otherwise>
		</c:choose>
		<c:forEach var="dataList" items="${univData.divDataMap}">
			<c:forEach var="inpleDateDivData" items="${dataList.value}">
				univData["<c:out value="${inpleDateDivData.dateDivKey}" />"] = "<c:out value="${univData.univNameAbbr}" /> <c:out value="${univData.facultyNameAbbr}" /> <c:out value="${univData.deptNameAbbr}" /> <c:out value="${inpleDateDivData.entExamInpleUniqueDate}" />（<c:if test="${ not empty inpleDateDivData.schoolProventDivName}"><kn:schoolProventDiv code="${inpleDateDivData.schoolProventDiv}" style="1"/> </c:if><kn:entExamDiv_1_2order code="${inpleDateDivData.entExamDiv_1_2order}" style="1"/>）";
			</c:forEach>
		</c:forEach>
	</c:forEach>
	/**
	 * 共通サブミッション
	 */
	function submitForm(forward) {
		<%--スクロール位置 0426 K.Kondo--%>
		if(forward=='<c:out value="${param.forward}" />') {
			document.forms[0].scrollX.value = document.body.scrollLeft;
			document.forms[0].scrollY.value = document.body.scrollTop;
		} else {
			document.forms[0].scrollX.value = 0;
			document.forms[0].scrollY.value = 0;
		}
		if(forward == 'i205'){
			//document.forms[0].action = "I205Servlet";
			op = window.open("blank.html", "sheet", "width=930,height=600,resizable=yes,scrollbars=yes,status=yes,titlebar=yes");
			op.moveTo(50, 50);
			document.forms[0].target = "sheet";
		}else if(forward == 'i303'){
			//document.forms[0].action = "I303Servlet";
			window.open("blank.html", "sheet", "resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=680,height=580");
			document.forms[0].target = "sheet";
		}else if(forward == 'i304'){
			//document.forms[0].action = "I304Servlet";
			window.open("blank.html", "sheet", "resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=680,height=580");
			document.forms[0].target = "sheet";
		}else{
			document.getElementById("MainLayer").style.visibility = "hidden";
			document.getElementById("MainLayer").style.top = -2000;
			document.getElementById("LoadingLayer").style.visibility = "visible";
			document.forms[0].target = "_self";
		}
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		clearDisabled();
		document.forms[0].submit();
		restoreDisabled();
	}
	/**
	 * 戻るボタン
	 */
	function submitBack() {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "i301";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		clearDisabled(document.forms[0].elements);
		document.forms[0].submit();
		restoreDisabled();
	}
	/**
	 * 新規登録サブミッション
	 */
	function submitNew(){
		document.forms[0].button.value = "new"
		submitForm('i303');
	}
	/**
	 * 編集サブミッション
	 */
	function submitEdit(thisUnivValue){
		document.forms[0].univValue4Detail.value = thisUnivValue;
		document.forms[0].button.value = "edit"
		submitForm('i304');
	}
	/**
	 * 更新サブミッション
	 */
	function submitUpdate(){
		//checkboxとtextboxの値を合体!
		var univValues = document.forms[0].elements['univValuePre'];
		if(univValues != null){
			if(univValues.length){

				for(var i=0; i<univValues.length; i++){
					if(document.forms[0].elements["univValuePre"][i].checked){
						document.forms[0].elements["univValue"][i].value += ",on";
					}else{
						document.forms[0].elements["univValue"][i].value += ",off";
					}
					document.forms[0].elements["univValue"][i].value += "," + document.forms[0].elements["provEntExamSite"][i].value;
				}
			}else{
					if(document.forms[0].elements["univValuePre"].checked){
						document.forms[0].elements["univValue"].value += ",on";
					}else{
						document.forms[0].elements["univValue"].value += ",off";
					}
					document.forms[0].elements["univValue"].value += "," + document.forms[0].elements["provEntExamSite"].value;
			}
		}
		document.forms[0].button.value = "update";
		submitForm('i302');
	}
	/**
	 * 複製サブミッション
	 */
	function submitCopy(thisUnivValue){
		document.forms[0].univValue4Detail.value = thisUnivValue;
		document.forms[0].button.value = "copy";
		submitForm('i304');
	}
	/**
	 * 削除サブミッション
	 */
	function submitDelete(thisUnivValue){
		document.forms[0].univValue4Detail.value = thisUnivValue;
		document.forms[0].button.value = "delete";
		submitForm('i302');
	}
	/**
	 * 詳細画面表示のための大学ユニークキーの設定
	 */
	function setUniqueKey(key){
		document.forms[0].uniqueKey.value = key;
	}
	/**
	 * 詳細画面表示のためのサブミッション
	 */
	function submitDetail(){
		submitForm('i205');
	}
	/**
	 * 判定履歴結果からのサブミッション
	 */
	function registerFromJudge(forward){
		document.forms[0].button.value = "registerFromJudge";
		submitForm('<c:out value="${param.forward}" />');
	}
	/**
	 * 更新アラートボックス
	 */
	function nomalCheck(){
		var message = "";

		var univValues = document.forms[0].elements['univValuePre'];
		if(univValues != null){
			if(univValues.length){
				for(var i=0; i<univValues.length; i++){
					if(document.forms[0].elements["univValuePre"][i].checked){
						var site = document.forms[0].elements["provEntExamSite"][i].value;
						var js = new JString();

						// 入力チェック（コメントの文字数）
						js.setString(site);

						if (js.getLength() > 10) {
							message = "地方試験地は全角５文字までです。\n";
							break;
						}
					}
				}
			}else{
				if(document.forms[0].elements["univValuePre"].checked){
					var site = document.forms[0].elements["provEntExamSite"].value;
					var js = new JString();
					// 入力チェック（コメントの文字数）
					js.setString(site);
					if (js.getLength() > 10) {
						message = "地方試験地は全角５文字までです。\n";
					}
				}
			}
		}

		<%-- 受験日重複チェック --%>
		if (message.length == 0 && univValues != null) {

			var e = document.forms[0].elements["univValuePre"];
			var array = new Array();
			if (e.length) {
				for (var i = 0; i < e.length; i++) {
					array[i] = e[i];
				}
			} else {
				array[0] = e;
			}

			<%-- 重複をハッシュに保持する --%>
			var hash = new Array();
			for (var i = 0; i < array.length; i++) {
				if (array[i].checked) {
					var month = document.forms[0].elements["month_" + array[i].value].value;
					var date = document.forms[0].elements["date_" + array[i].value].value;
					var key = month + "/" + date;
					if (hash[key]) {
						hash[key][hash[key].length] = array[i].value;
					} else {
						hash[key] = new Array();
						hash[key][0] = array[i].value;
					}
				}
			}

			<%-- 背景色を戻す --%>
			for (var id in rowBgColor) {
				document.getElementById(id).style.backgroundColor = rowBgColor[id];
			}

			<%-- チェック処理 --%>
			for (var key in hash) {
				if (hash[key].length > 1) {
					message += "\n";
					for (var i = 0; i < hash[key].length; i++) {
						message += "・" + univData[hash[key][i]] + "\n";
						var row = document.getElementById("row-" + hash[key][i].substring(0, 12));
						row.style.backgroundColor = "#FFAD8C";
					}
				}
			}
			if (message.length > 0) {
				message = "以下の受験日が重複しています。\n" + message;
			}

			<%-- 国公立大学の日程重複チェック --%>
			var scheduleCd = document.forms[0].elements["scheduleCd"];
			if (scheduleCd.length) {

				var counterD = 0;<%-- 前期 --%>
				var counterC = 0;<%-- 中期 --%>
				var counterE = 0;<%-- 後期 --%>
				for (var i = 0; i < scheduleCd.length; i++) {
					if (scheduleCd[i].value == "D") {
						counterD++;
					} else if (scheduleCd[i].value == "C") {
						counterC++;
					} else if (scheduleCd[i].value == "E") {
						counterE++;
					}
				}

				var buff = "";

				if (counterD > 1) {
					buff += "前期";
				}

				if (counterC > 1) {
					if (buff.length > 0) {
						buff += "・";
					}
					buff += "中期";
				}

				if (counterE > 1) {
					if (buff.length > 0) {
						buff += "・";
					}
					buff += "後期";
				}

				if (buff.length > 0) {
					buff = "※国公立大学" + buff + "<kn:message id ="i302a" />";
					if (message.length > 0) {
						message += "\n\n";
						message += buff;
					} else {
						alert(buff);
					}
				}
			}
		}

		if (message.length == 0) {
			javascript:submitUpdate();
		} else {
			alert(message);
		}
	}
	/**
	 * 削除アラートボックス
	 */
	function deletionCheck(key){
		if (confirm("削除しますか？")){
			javascript:submitDelete(key);
		}
	}
		/**
	 * チェックボックスをオフにした時に、
	 * 付随するHIDDENも無効化する
	 */
	function disableBox(index){
		document.getElementById("month_"+index).disabled = true;
		document.getElementById("date_"+index).disabled = true;
	}
	/**
	 * チェックボックスをオンにした時に、
	 * 付随するHIDDENを有効化する
	 */
	function enableBox(index){
		document.getElementById("month_"+index).disabled = false;
		document.getElementById("date_"+index).disabled = false;
	}
	/**
	 * サーブレットエラー表示
	 */
	function showErrorMessage(){
		if('<c:out value="${form.errorMessage}" />'.length != 0) {
			alert('<c:out value="${form.errorMessage}" />');
		}
	}
	/**
	 * 生徒切り替え
	 */
	<%@ include file="/jsp/individual/include/student_rotator.jsp" %>
	/**
	 印刷ダイアログ
	  */
	<%@ include file="/jsp/individual/include/printDialog.jsp" %>
	/**
	 * 印刷チェック
	 */
	function printCheck() {
		if (!validate()) {
			window.alert("出力フォーマットを選択してください。");
		} else {
			printDialog();
		}
	}
	// 表示
	function printView() {
		if (!validate()) {
			window.alert("出力フォーマットを選択してください。");
		} else {
			document.forms[0].printStatus.value = "1";
			printSheet();
		}
	}
	// 入力チェック
	function validate() {
		if (document.forms[0].textFileFormat.value == "2") {
			return true;
		}
		for (i=0; i < document.forms[0].textFormat.length; i++) {
			if (document.forms[0].textFormat[i].checked) {
				return true;
			}
		}
		return false;
	}
	// 出力フォーマット状態変更
	function changeTextFormatStatus(value) {
		var flag = value == "2" ? "disabled" : false;
		document.forms[0].textFormat[0].disabled = flag;
		document.forms[0].textFormat[1].disabled = flag;
		document.forms[0].textFormatSub[0].disabled = flag;
		document.forms[0].textFormatSub[1].disabled = flag;
	}

	/**
	 * 初期化
	 */
	function init(){
		startTimer();
		<%--スクロール位置 0426 K.Kondo--%>
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
		//1.選択中の生徒まで回す
		findStudent();
		//2.選択中の生徒が一人ならArrowボタンを無効化する
		if(getStudentNum() == 1) disableArrows();
		//選択中生徒の学年によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
		gradeChange();
		//出力フォーマット状態変更
		changeTextFormatStatus(document.forms[0].textFileFormat.value);

		<%if(planUnivSearchBean.getNestedPlanUnivList().size() <= 0) {%>
		//更新ボタン使用不可
		document.forms[0].updateButton.disabled = true;
		//注意表示・非表示
		document.getElementById("warning").style.display = "none";
		<%} else {%>
		//受験予定大学がありません表示、非表示
		document.getElementById("list").style.display = "none";
		<%}%>
		//ロード中
		<%@ include file="/jsp/script/loading.jsp" %>

		showErrorMessage();
	}
//-->
</SCRIPT>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I302Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="button" value="">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="targetYear" value="<c:out value="${iCommonMap.targetExamYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${iCommonMap.targetExamCode}" />">
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="printStatus" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">

<%-- 更新・削除・編集などで必要な表示中の一覧データ --%>
<c:forEach var="univData" items="${planUnivSearchBean.nestedPlanUnivList}" varStatus="status">
	<c:forEach var="dataList" items="${univData.divDataMap}" varStatus="status2">
		<c:forEach var="data" items="${dataList.value}" varStatus="status3">
			<input type="hidden" name="univValue" value="<c:out value="${data.dateDivKey}"/>">
		</c:forEach>
	</c:forEach>
</c:forEach>

<%-- 詳細表示 --%>
<input type="hidden" name="uniqueKey" value="">
<input type="hidden" name="univValue4Detail" value="">

<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->


<!--個人手帳-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--概要-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--メニュー切り替え-->
<%@ include file="/jsp/individual/include/menu_switch.jsp" %>
<!--/メニュー切り替え-->
<!--小タイトル-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="832">

<table border="0" cellpadding="0" cellspacing="0" width="832">
<tr valign="top">
<td colspan="4" width="832" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="832" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="832" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="816" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">受験予定大学一覧</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="832" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="832" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/小タイトル-->
<!--説明-->
<div style="margin-top:12px;">
<table border="0" cellpadding="0" cellspacing="0" width="828">
<tr valign="top">
<td nowrap><span class="text14">判定履歴（受験予定候補）から受験予定として登録された大学の一覧です。<br>受験日を設定すると受験予定大学としてスケジュールに反映されます。<br>
また、「新規登録」ボタンから大学を新規に追加することができます。<br></span></td>
<td align="right"><%@ include file="/jsp/individual/include/text_out_format_sub.jsp" %></td>
</tr>
</table>
</div>
<!--/説明-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--リスト-->
<table border="0" cellpadding="0" cellspacing="0" width="852">
<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
<td width="832">

<table border="0" cellpadding="3" cellspacing="2" width="832">
<tr>
<td colspan="11" width="100%" bgcolor="#93A3AD">
<table border="0" cellpadding="2" cellspacing="3">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="新規登録" class="text12" style="width:100px;" onclick="javascript:submitNew()"></td>
</tr>
</table>
</td>
</tr>
<!--項目-->
<tr height="22" bgcolor="#CDD7DD" align="center">
<td width="5%"><span class="text12">&nbsp;</span></td>
<td width="16%"><span class="text12">大学</span></td>
<td width="13%"><span class="text12">学部</span></td>
<td width="13%"><span class="text12">学科</span></td>
<td width="9%"><span class="text12">入試形態</span></td>
<%-- ガイドラインが一件でも存在する場合のみ列の表示--%>
<c:if test="${isGuideExist}">
	<td width="5%"><span class="text12">&nbsp;</span></td>
</c:if>
<td width="16%"><span class="text12">受験日</span></td>
<td width="13%"><span class="text12">地方試験地</span></td>
<td width="4%"><span class="text12">&nbsp;</span></td>
<td width="4%"><span class="text12">&nbsp;</span></td>
<td width="4%"><span class="text12">&nbsp;</span></td>
</tr>
<!--/項目-->
<c:forEach var="univData" items="${planUnivSearchBean.nestedPlanUnivList}" varStatus="status">
		<c:set var="shidaiNitteiUrl" value="${ univData.shidaiNitteiUrl }" />
		<c:choose>
			<c:when test="${univData.united}">
				<c:set var="bgcolor" value="#E1E6EB" />
			</c:when>
			<c:otherwise>
				<c:set var="bgcolor" value="#F4E5D6" />
			</c:otherwise>
		</c:choose>
<tr height="27" bgcolor="<c:out value="${bgcolor}" />" id="row-<c:out value="${univData.univCd}" />,<c:out value="${univData.facultyCd}" />,<c:out value="${univData.deptCd}" />">
	<input type="hidden" name="scheduleCd" value="<c:out value="${ univData.scheduleCd }" />">
	<td align="center"><span class="text12"><c:if test="${!univData.united}"><a href="javascript:setUniqueKey('<c:out value="${univData.uniqueKey}" />');submitDetail();">詳細</a></c:if></span></td>
	<td>
	<table border="0" cellpadding="4" cellspacing="0">
	<tr>
	<td><span class="text12"><c:out value="${univData.univNameAbbr}" /></span></td>
	</tr>
	</table>
	</td>
	<td>
	<table border="0" cellpadding="4" cellspacing="0">
	<tr>
	<td><span class="text12"><c:out value="${univData.facultyNameAbbr}" /></span></td>
	</tr>
	</table>
	</td>
	<td>
	<table border="0" cellpadding="4" cellspacing="0">
	<tr>
	<td><span class="text12"><c:out value="${univData.deptNameAbbr}" /></span></td>
	</tr>
	</table>
	</td>
	<td align="center"><span class="text12"><c:out value="${univData.entExamOdeName}" /></span></td>
	<%-- ガイドラインが一件でも存在する場合のみ列の表示--%>
	<c:if test="${isGuideExist}">
		<td align="center"><span class="text12"><c:if test="${univData.guideLine != ''}"><a href="javascript:submitEdit('<c:out value="${univData.planUnivDataKey}" />')">注</a></c:if></span></td>
	</c:if>
	<td>
		<table cellpadding="0" cellspacing="0">
			<c:if test="${univData.divDataMapSize == 0}">
				<tr>
					<td height="25px">
					<table border="0" cellpadding="0" cellspacing="0">
					<tr>
					<td></td>
					<td></td>
					</tr>
					</table>
					</td>
				</tr>
			</c:if>
			<c:forEach var="dataList" items="${univData.divDataMap}" varStatus="status2">
				<c:forEach var="inpleDateDivData" items="${dataList.value}" varStatus="status3">
				<tr>
					<td height="25px">
					<table border="0" cellpadding="0" cellspacing="0">
					<tr>
					<td><input onclick="javascript: this.checked ? enableBox('<c:out value="${inpleDateDivData.dateDivKey}" />') : disableBox('<c:out value="${inpleDateDivData.dateDivKey}" />')" type="checkbox" name="univValuePre" value="<c:out value="${inpleDateDivData.dateDivKey}" />" <c:if test="${inpleDateDivData.registered}">checked</c:if>>
						<input id="month_<c:out value="${inpleDateDivData.dateDivKey}" />" type="hidden" name="month" value="<c:out value="${inpleDateDivData.entExamInpleMonth}"/>" <c:if test="${!inpleDateDivData.registered}">disabled</c:if>>
						<input id="date_<c:out value="${inpleDateDivData.dateDivKey}" />" type="hidden" name="date" value="<c:out value="${inpleDateDivData.entExamInpleDate}"/>" <c:if test="${!inpleDateDivData.registered}">disabled</c:if>>
						</td>
					<td><span class="text12"><c:out value="${inpleDateDivData.entExamInpleUniqueDate}" />（<c:if test="${ not empty inpleDateDivData.schoolProventDivName}"><kn:schoolProventDiv code="${inpleDateDivData.schoolProventDiv}" style="1"/> </c:if><kn:entExamDiv_1_2order code="${inpleDateDivData.entExamDiv_1_2order}" style="1"/>）</span></td>
					</tr>
					</table>
					</td>
				</tr>
				</c:forEach>
			</c:forEach>
		</table>
	</td>
	<td>
		<table cellpadding="0" cellspacing="0">
			<c:if test="${univData.divDataMapSize == 0}">
				<tr>
					<td align="center" height="25px"></td>
				</tr>
			</c:if>
			<c:forEach var="dataList" items="${univData.divDataMap}" varStatus="status2">
				<c:forEach var="inpleDateDivData" items="${dataList.value}" varStatus="status3">
				<tr>
					<td align="center" height="25px"><input type="text" size="10" class="text12" maxlength="10" name="provEntExamSite" style="width:94px;" value="<c:out value="${inpleDateDivData.provEntExamSite}" />"></td>
				</tr>
				</c:forEach>
			</c:forEach>
		</table>
	</td>
	<td align="center" nowrap><span class="text12"><c:if test="${!univData.united}"><a href="javascript:submitEdit('<c:out value="${univData.planUnivDataKey}" />')">編集</a></c:if></span></td>
	<td align="center" nowrap><span class="text12"><c:if test="${!univData.united}"><a href="javascript:submitCopy('<c:out value="${univData.planUnivDataKey}" />')">複製</a></c:if></span></td>
	<td align="center" nowrap><span class="text12"><a href="javascript:deletionCheck('<c:out value="${univData.planUnivDataKey}" />')">削除</a></span></td>
	</tr>
</c:forEach>
<!--/1段目-->
</table>



<!--ありません-->
<div id="list">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="848" height="50">
<tr>
<td align="center"><span class="text14">受験予定大学がありません</span></td>
</tr>
</table>
</div>
</div>
<!--ありません-->



<!--注意-->
<div id="warning">
<table border="0" cellpadding="0" cellspacing="2" width="832">
<tr>
<td><span class="text12">※<img src="./shared_lib/img/parts/color_box_04.gif" width="27" height="11" border="0" alt="□" hspace="5" align="absmiddle">は大学の公表する入試要綱が変更されておりますので、一旦削除して、再度登録する必要があります。</span></td>
</tr>
<tr>
<td><span class="text12">※試験日程および試験地については必ず大学発表の学生募集要項で確認してください。<a href="#here" onclick="openScheduleAlert(2);">「入試日」に関するご注意</a></span></td>
</tr>
</table>
</div>
<!--/注意-->
</td>
</tr>
</table>
<!--/リスト-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="3" border="0" alt=""><br></td>
<td width="828" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="3" border="0" alt=""><br></td>
<td width="828">
<table border="0" cellpadding="0" cellspacing="0" width="828">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="828">
<tr valign="top">
<td width="826" bgcolor="#FBD49F" align="center">

<input type="button" name="updateButton" value="更新" class="text12" style="width:189px;" onclick="javascript:nomalCheck()">

</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/個人手帳-->

<!--セキュリティスタンプの選択-->
<%@ include file="/jsp/individual/include/security_stamp.jsp" %>
<!--/セキュリティスタンプの選択-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->


<!--テキストフォーマット-->
<%@ include file="/jsp/individual/include/text_out_format.jsp" %>
<!--/テキストフォーマット-->

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>

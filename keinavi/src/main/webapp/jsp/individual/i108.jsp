<%--0502 K.Kondo 新テストの場合全国平均を出さない--%>
<%--0524 K.Kondo 対象模試が新テストの場合は、アプレットの全国平均ラインを出さない--%>
<%--060130 K.Kondo 設問タブの幅調節 --%>
<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Collection" %>
<%@ page import="jp.co.fj.keinavi.data.individual.QuestionData" %>
<%@ page import="jp.co.fj.keinavi.data.individual.SubjectData" %>
<%@ page import="jp.co.fj.keinavi.data.help.HelpList" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<jsp:useBean id="i108Bean" scope="request" class="jp.co.fj.keinavi.beans.individual.I108Bean" />
<jsp:useBean id="i108Data" scope="request" class="jp.co.fj.keinavi.data.individual.I108Data" />
<jsp:useBean id="form" scope="request" class="jp.co.fj.keinavi.forms.individual.I108Form" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<SCRIPT type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    <%@ include file="/jsp/script/submit_change.jsp" %>
    <%@ include file="/jsp/script/submit_exit.jsp" %>
    <%@ include file="/jsp/script/submit_save.jsp" %>
    <%@ include file="/jsp/script/submit_menu.jsp" %>
    <%@ include file="/jsp/script/open_common.jsp" %>
    <%@ include file="/jsp/script/open_sample.jsp" %>
    <%@ include file="/jsp/script/submit_openOnepoint.jsp" %>
    <%@ include file="/jsp/script/submit_help.jsp" %>
    <%@ include file="/jsp/script/clear_disabled.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/printDialog_close.jsp" %>
	var printDialogPattern = "";
	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

    <%-- 対象模試リスト --%>
    <c:set var="examList" value="${iCommonMap.examComboData.listIV}"/>

    /**
     * サブミッション
     */
    function submitForm(forward) {
        <%--スクロール位置 0426 K.Kondo--%>
        if(forward=='<c:out value="${param.forward}" />') {
            document.forms[0].scrollX.value = document.body.scrollLeft;
            document.forms[0].scrollY.value = document.body.scrollTop;
        } else {
            document.forms[0].scrollX.value = 0;
            document.forms[0].scrollY.value = 0;
        }
        if(forward == 'i109'){
        	<%-- 2016/01/21 QQ)Hisakawa 大規模改修 UPD START --%>
        	// window.open("blank.html", "sheet", "resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=920");
            window.open("blank.html", "sheet", "resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=920,height=800");
        	<%-- 2016/01/21 QQ)Hisakawa 大規模改修 UPD END   --%>
            document.forms[0].target = "sheet";
            document.forms[0].action="<c:url value="I109Servlet" />";
        }else{
            clearDisabled();
            document.getElementById("MainLayer").style.visibility = "hidden";
            document.getElementById("MainLayer").style.top = -2000;
            document.getElementById("LoadingLayer").style.visibility = "visible";
            document.forms[0].target = "_self";
        }
        document.forms[0].forward.value = forward;
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 戻るボタン
     */
    function submitBack() {
        document.forms[0].target = "_self";
        document.forms[0].forward.value = "i003";
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 再表示
     */
    function submitRefresh(subcd, subnm) {
        document.forms[0].targetSubCode.value = subcd;
        document.forms[0].targetSubName.value = subnm;
        submitForm('<c:out value="${param.forward}" />');
    }
    /**
     *
     */
    function listnone() {
        <%
            Collection csub2 = i108Bean.getSubDatas();
            java.util.Iterator cd2 = csub2.iterator();
            boolean dspflg = false;
            while(cd2.hasNext()) {
                SubjectData data = (SubjectData)cd2.next();
                String temp = data.getSubjectCd();
                if(temp.equals(form.getTargetSubCode())) {
                    dspflg = true;
                    break;
                }
            }
            if(!dspflg) {
        %>
            document.getElementById("list").style.display = "none";
        <%}%>
    }

    /**
     * 生徒切り替え
     */
    <%@ include file="/jsp/individual/include/student_rotator.jsp" %>
    /**
     * 印刷ダイアログ
     */
    <%@ include file="/jsp/individual/include/printDialog.jsp" %>
    /**
     * 印刷チェック
     */
    function printCheck() {
        printDialog();
    }
    /**
     * 一括印刷チェック
     */
    function printAllCheck() {
        printAllDialog();
    }
    // 表示
    function printView() {
        document.forms[0].printStatus.value = "1";
        printSheet();
    }
    /**
     * 初期化
     */
    function init(){
        startTimer();
        <%--スクロール位置 0426 K.Kondo--%>
        window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

        //1.選択中の生徒まで回す
        findStudent();
        //2.選択中の生徒が一人ならArrowボタンを無効化する
        if(getStudentNum() == 1) disableArrows();

        //選択中生徒の学年によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
        gradeChange();

        //アプレットに表示する内容が無い時は、下のテーブルも消す
        listnone();

        //ロード中
        <%@ include file="/jsp/script/loading.jsp" %>

        <%-- アプレットを本来の位置へ移動する --%>
        $('#AppletLayer').children().prependTo('#AppletCell');
    }
//-->
</SCRIPT>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="AppletLayer" style="position:absolute;top:250px;left:450px;width:1px;height:1px;overflow:hidden;">
<!-- アプレット部：サーブレットよりVALUEを受けとるようにすること=========== -->
<%-- 2016/01/18 QQ)Hisakawa 大規模改修 DEL START --%>
<%--
<c:forEach var="data" items="${i108Bean.recordSet}">
<APPLET CODEBASE="./applets/" ARCHIVE="Applet.jar" CODE="LineGraphAppletQ" id="rc" WIDTH="848" HEIGHT="320">

<%
    //科目タブ切り替え
    String dispSelect = "";	//グラフ表示／非表示

    Collection csub = i108Bean.getSubDatas();
    java.util.Iterator cd = csub.iterator();
    boolean flag = false;
    while(cd.hasNext()) {
        SubjectData data = (SubjectData)cd.next();
        String temp = data.getSubjectCd();
        if(temp.equals(form.getTargetSubCode())) {
            flag = true;
            break;
        }
    }
    if(flag==true){
        dispSelect = "on";
        form.setDispSelect("on");
    }else{
        dispSelect = "off";
        form.setDispSelect("off");
    }
%>

    <param name="examName" value="<c:out value="${data.examName}" />">
    <param name="dispSelect" value="<%=dispSelect%>">
    <param name="yTitle" value="得点率％">
    <c:choose>
    <c:when test="${iCommonMap.targetExamTypeCode != '31'}">
    <param name="LineItemNum" value="2">
    </c:when>
    <c:otherwise>
    <param name="LineItemNum" value="1">
    </c:otherwise>
    </c:choose>
    <param name="LineItemNameS" value="対象生徒">
    <param name="LineItemName" value="校内平均<c:if test="${iCommonMap.targetExamTypeCode != '31'}">,全国平均</c:if><c:if test="${form.dispScholarLevel != ''}">,<c:out value="${form.dispScholarLevel}"/>レベル</c:if>">
    <param name="examValueS" value="<c:out value="${data.examValueS}" />">
    <param name="examValue0" value="<c:out value="${data.examValue0}" />">
    <c:if test="${iCommonMap.targetExamTypeCode != '31'}">
    <param name="examValue1" value="<c:out value="${data.examValue1}" />">
    </c:if>
    <param name="examValueL" value="<c:out value="${data.examValueL}" />">
    <param name="BarSize" value="10">
    <param name="colorDAT" value="1,0,13,14,13,14,2">
</APPLET>
<br>
</c:forEach>
<%if(i108Bean.getRecordSet().size() == 0) {%>
<APPLET CODEBASE="./applets/" ARCHIVE="Applet.jar" CODE="LineGraphAppletQ" id="rc" WIDTH="848" HEIGHT="320">
    <param name="examName" value="">
    <param name="dispSelect" value="off">
    <param name="yTitle" value="">
    <param name="LineItemNum" value="0">
    <param name="LineItemNameS" value="">
    <param name="LineItemName" value="">
    <param name="examValueS" value="">
    <param name="examValueL" value="">
    <param name="examValue0" value="">
    <param name="examValue1" value="">
    <param name="BarSize" value="10">
    <param name="colorDAT" value="1,0,13,14,13,14">
</APPLET>
<%}%>
--%>
<%-- 2016/01/18 QQ)Hisakawa 大規模改修 DEL END   --%>
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I108Servlet" />" method="POST">
<%-- 共通 --%>
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="printStatus" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">
<input type="hidden" name="targetYear" value="<c:out value="${iCommonMap.targetExamYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${iCommonMap.targetExamCode}" />">
<input type="hidden" name="printFlag" value="">
<%-- 生徒切り替え --%>
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="targetSubCode" value="<c:out value="${form.targetSubCode}" />">
<input type="hidden" name="targetSubName" value="<c:out value="${form.targetSubName}" />">
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<input type="hidden" name="targetScholarLevel" value="<c:out value="${i108Bean.targetScholarLevel}" />">
<input type="hidden" name="changeDispLevel" value="">
<input type="hidden" name="dispSubCode" value="<c:out value="${form.targetSubCode}" />">


<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->


<!--成績分析-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--分析メニュー-->
<%@ include file="/jsp/individual/include/bunseki_menu.jsp" %>
<!--/分析メニュー-->

<!--概要-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="12" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="698">
<table border="0" cellpadding="0" cellspacing="0" width="698">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="682">

<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td colspan="5" width="682" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="682" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="682" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="666" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">設問別成績</b></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="682" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="682" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="150" valign="bottom">
<table border="0" cellpadding="0" cellspacing="0" width="150">
<tr>
<td align="center" height="40"><input type="button" class="text12" onclick="openSample('<c:out value="${param.forward}" />');" value="サンプルの表示"></td>
</tr>
<tr>
<td bgcolor="#8CA9BB" height="2"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
</table>
<!--/大タイトル-->

<!--模試の種類-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="37" border="0" alt=""><br></td>
<td width="844">

<table border="0" cellpadding="0" cellspacing="3" width="844">
<tr>
<td width="369">
<table border="0" cellpadding="0" cellspacing="0" width="369">
<tr>
<td bgcolor="#E1E6EB">
    <table border="0" cellpadding="3" cellspacing="0">
    <tr>
    <td><b class="text12">&nbsp;対象年度：</b></td>
    <td><%@ include file="/jsp/individual/include/targetExamYear.jsp" %></td>
    </tr>
    <tr>
    <td><b class="text12">&nbsp;対象模試：</b></td>
    <td><%@ include file="/jsp/individual/include/targetExamCode.jsp" %></td>
    </tr>
    </table>
</td>

</tr>
</table>

<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="#here" onclick="submitForm('i109');"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→"></a></td>
<td><span class="text12"><a href="#here" onclick="submitForm('i109');">過回模試グラフを表示（別ウィンドウ）</a></span></td>
</tr>
</table>
</div>

</td>
<td bgcolor="#E1E6EB" rowspan="2" width="398">
    <table>
    <tr>
    <td colspan="2" align="left"><b class="text12">&lt;&lt;オプション&gt;&gt;</b></td>
    </tr>
    <tr>
    <td colspan="2">
    <table width="100%">
    <td align="left"><span class="text12">&nbsp;対象生徒の学力レベル：<c:out value="${i108Bean.targetScholarLevel}"/></span></td>
    <td align="right"><span class="text12">表示学力レベル：&nbsp;&nbsp;</span>
        <select name="dispScholarLevel" class="text12">
            <option value=""></option>
            <c:set var="level" value="" />
            <c:forEach var="levellist" items="${ i108Bean.levelDatas }">
            <option value="<c:out value="${ levellist }" />" <c:if test="${ levellist eq form.dispScholarLevel }"> selected</c:if> ><c:out value="${ levellist }" /></option>
            </c:forEach>
        </select>&nbsp;&nbsp;
    </td>
    </table>
    </td>
    </tr>
    <tr>
    <td colspan="2"><span class="text10">※これらのオプション項目を変更した場合は、[再表示]ボタンをクリックしてください。</span></td>
    </tr>
    </table>
</td>

<td bgcolor="#E1E6EB" rowspan="2" width="71" align="center">
    <input type="button" class="text12" onclick="submitForm('<c:out value="${ param.forward }" />');" value="再表示" style="width:60px;" >
</td>
</tr>
</table>

</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="37" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="848" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/模試の種類-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<div align="left">
<!--メニュー切り替え-->
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<%--060130 K.Kondo 設問タブの幅調節 start--%>
<c:set var="tabNum" value="0" />
<c:forEach var="subData" items="${i108Bean.subDatas}" varStatus="status">
    <c:set var="tabNum" value="${tabNum + 1}" />
</c:forEach>

<c:choose>
    <c:when test="${ tabNum <= 9 }">
        <c:set var="colWidth" value="${ 90 }" />
    </c:when>
    <c:otherwise>
        <c:set var="colWidth" value="${ (848 - (tabNum - 1) * 4) / tabNum }" />
    </c:otherwise>
</c:choose>

<c:forEach var="subData" items="${i108Bean.subDatas}" varStatus="status">

    <c:if test="${status.index != 0}">
            <td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
    </c:if>
    <c:choose>
        <c:when test="${form.targetSubCode eq subData.subjectCd}">
            <td width="<c:out value="${ colWidth }" />" height="24">
            <!--2-->

            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
            <td bgcolor="#FFAD8C">
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
            <td width="100%" height="22" bgcolor="#FFAD8C" align="center"><span class="text12"><c:out value="${subData.subjectName}" /></span></td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <!--/2-->
            </td>
        </c:when>
        <c:otherwise>
            <td width="<c:out value="${ colWidth }" />" height="24">
            <!--2-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
            <td bgcolor="#8CA9BB">
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
            <td width="100%" height="22" bgcolor="#E5EEF3" align="center"><span class="text12">
            <a href="javascript:submitRefresh('<c:out value="${subData.subjectCd}" />', '<c:out value="${subData.subjectName}" />')">
            <c:out value="${subData.subjectName}" /></a></span></td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <!--/2-->
            </td>
        </c:otherwise>
    </c:choose>
</c:forEach>
<%--060130 K.Kondo 設問タブの幅調節 end--%>
</tr>
</table>
<!-----メニュー2段目
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">英語</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">数学IA</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">数学IIB</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">国語I</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">物理IA</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">化学IA</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">地理A</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">地理A</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">地理A</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">地理A</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">地理A</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="49"><img src="./shared_lib/img/parts/sp.gif" width="49" height="24" border="0" alt=""><br></td>
</tr>
</table>
--/メニュー2段目------------>

<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td bgcolor="#FFAD8C"><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/メニュー切り替え-->
</div>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--グラフイメージ-->
<table border="0" cellpadding="0" cellspacing="0" width=se>
<tr>
<%-- 2016/01/18 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td width="848" id="AppletCell"> --%>
<td width="848">
<%-- 2016/02/29 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<img src="./GraphServlet" width="848" height="320" border="0">
  --%>
<img src="<c:url value="./GraphServlet" />"  width="848" height="320" border="0">
<%-- 2016/02/29 QQ)Nishiyama 大規模改修 UPD END   --%>
<%-- 2016/01/18 QQ)Hisakawa 大規模改修 UPD END   --%>
</td>
</tr>
</table>
<!--/グラフイメージ-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--リスト-->
<div id='list'>
<c:forEach var="data" items="${i108Bean.recordSet}">
<table border="0" cellpadding="5" cellspacing="2" width="852">
<!--項目-->
<!--内容-->
<c:forEach var="x" begin="0" end="9" step="1">
    <c:choose>
        <c:when test="${x == 0}">
            <tr height="22" bgcolor="#CDD7DD">
        </c:when>
        <c:otherwise>
            <tr height="36">
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${x == 0}">
            <td colspan="2" width="10%"><span class="text12">設問番号</span></td>
        </c:when>
        <c:when test="${x == 1}">
            <td colspan="2" bgcolor="#CDD7DD"><span class="text12">内容</span></td>
        </c:when>
        <c:when test="${x == 2}">
            <td colspan="2" bgcolor="#CDD7DD"><span class="text12">配点</span></td>
        </c:when>
        <c:when test="${x == 3}">
            <td rowspan="2" width="6%" bgcolor="#CDD7DD" nowrap><span class="text12">対象生徒</span></td>
            <td width="4%" bgcolor="#CDD7DD" nowrap><span class="text12">得点</span></td>
        </c:when>
        <c:when test="${x == 4}">
            <td width="4%" bgcolor="#CDD7DD" nowrap><span class="text12">得点率</span></td>
        </c:when>
        <c:when test="${x == 5}">
            <td colspan="2" width="10%" bgcolor="#CDD7DD"><span class="text12"><c:if test="${form.dispScholarLevel != ''}"><c:out value="${form.dispScholarLevel}"/>レベル</c:if>得点率</span></td>
        </c:when>
        <c:when test="${x == 6}">
            <td rowspan="2" width="6%" bgcolor="#CDD7DD" nowrap><span class="text12">校内平均</span></td>
            <td width="4%" bgcolor="#CDD7DD" nowrap><span class="text12">得点</span></td>
        </c:when>
        <c:when test="${x == 7}">
            <td width="4%" bgcolor="#CDD7DD" nowrap><span class="text12">得点率</span></td>
        </c:when>
        <c:when test="${x == 8 && iCommonMap.targetExamTypeCode != '31'}">
            <td rowspan="2" width="6%" bgcolor="#CDD7DD" nowrap><span class="text12">全国平均</span></td>
            <td width="4%" bgcolor="#CDD7DD" nowrap><span class="text12">得点</span></td>
        </c:when>
        <c:when test="${x == 9 && iCommonMap.targetExamTypeCode != '31'}">
            <td width="4%" bgcolor="#CDD7DD" nowrap><span class="text12">得点率</span></td>
        </c:when>
    </c:choose>
    <c:forEach var="y" begin="0" end="10" step="1">
        <c:choose>
            <c:when test="${x == 0}">
            <td bgcolor="#E1E6EB" align="right" width="8%">
            <span class="text12">
                <c:if test="${form.dispSelect == 'on'}"><c:out value="${data.tableData[x][y]}" /></c:if>
            </span></td>
            </c:when>
            <c:when test="${x == 1}">
                <td bgcolor="#F4E5D6" width="8%"><span class="text12"><c:out value="${data.tableData[x][y]}" /></span></td>
            </c:when>
            <c:otherwise>
                <c:if test="${!(x > 6 && iCommonMap.targetExamTypeCode == '31')}">
                    <td bgcolor="#F4E5D6" align="right" width="8%"><span class="text12"><c:out value="${data.tableData[x][y]}" /></span></td>
                </c:if>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</tr>
</c:forEach>
<!--/内容-->
</table>
</c:forEach>
</div>
<!--/リスト-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/成績分析-->

<!--セキュリティスタンプの選択-->
<%@ include file="/jsp/individual/include/security_stamp.jsp" %>
<!--/セキュリティスタンプの選択-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--印刷対象生徒-->
<%@ include file="/jsp/individual/include/printStudent.jsp" %>
<!--/印刷対象生徒-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--ケイコさん-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="784">
<tr valign="top">
<td width="72" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="702">

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="678" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="678" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="678" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="678" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr>
<td width="12" background="./shared_lib/img/parts/wb_arrow_l_long.gif"><img src="./shared_lib/img/parts/sp.gif" width="12" height="5" border="0" alt=""><br></td>
<td width="688" bgcolor="#F9EEE5">
<!--アドバイス-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="688">
<tr valign="top">
<td width="131" align="center"><img src="./shared_lib/img/parts/word_one_point.gif" width="86" height="32" border="0" alt="ワンポイントアドバイス！！" vspace="5"><br></td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="5" border="0" alt=""><br></td>
<td width="511">


<div style="margin-top:5px;margin-bottom:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="511">
<%@ include file="/jsp/shared_lib/I_Onepoint.jsp" %>
</table>
</div>


</td>
<td width="25"><img src="./shared_lib/img/parts/sp.gif" width="25" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/アドバイス-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="678" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="678" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="678" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="678" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>

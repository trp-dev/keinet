<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<script type="text/javascript">
<!--
	var TIME_IN_SEC = 3;

	function goNow(){
		setTimeout("submitForm('no_need')", 1000 * TIME_IN_SEC );
	}
	
	/**
	 * サブミッション
	 */
	function submitForm(forward) {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "<c:out value="${form.trueForward}" />";
		document.forms[0].backward.value = "<c:out value="${form.trueBackward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	
//-->
</script>
</head>
<body onload="goNow()" bgcolor="#F4EDE2">
<form action="<c:url value="IOnJudgeServlet" />" method="POST" onSubmit="return false">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<%-- formの値を引き継ぐ必要がある --%>
<input type="hidden" name="trueForward" value="<c:out value="${form.trueForward}"/>">
<input type="hidden" name="trueBackward" value="<c:out value="${form.trueBackward}"/>">
<%-- 共通 --%>
<input type="hidden" name="targetPersonId" value="<c:out value="${form.targetPersonId}"/>">
<input type="hidden" name="judgeStudent" value="<c:out value="${form.judgeStudent}"/>">
<input type="hidden" name="printStudent" value="<c:out value="${form.printStudent}"/>">
<input type="hidden" name="printStamp" value="<c:out value="${form.printStamp}"/>">
<input type="hidden" name="indStudentUsage" value="<c:out value="${form.indStudentUsage}"/>">
<%-- I204 --%>
<input type="hidden" name="button" value="<c:out value="${form.button}"/>">
<%-- I401 --%>
<input type="hidden" name="targetYear" value="<c:out value="${iCommonMap.targetExamYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${iCommonMap.targetExamCode}" />">
<input type="hidden" name="targetExamYear" value="<c:out value="${form.targetExamYear}"/>">
<input type="hidden" name="targetExamCode" value="<c:out value="${form.targetExamCode}"/>">
<input type="hidden" name="targetExamName" value="<c:out value="${form.targetExamName}"/>">
<input type="hidden" name="interviewSheet" value="<c:out value="${form.interviewSheet}"/>">
<c:forEach var="interviewForm" items="${form.interviewForms}" varStatus="status">
	<input type="hidden" name="interviewForms" value="<c:out value="${interviewForm}"/>">
</c:forEach>
<input type="hidden" name="report" value="<c:out value="${form.report}"/>">
<input type="hidden" name="examDoc" value="<c:out value="${form.examDoc}"/>">
<input type="hidden" name="printFlag" value="<c:out value="${form.printFlag}"/>">
<%-- I302 --%>
<c:forEach var="textFormat" items="${form.textFormat}" varStatus="status">
	<input type="hidden" name="textFormat" value="<c:out value="${textFormat}"/>">
</c:forEach>
<center>
<br><br>判定の実行中です。<br>しばらくお待ちください。
</center>
</form>
</body>
</html>

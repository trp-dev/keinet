<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.keinavi.data.individual.IExamData" %>
<%@ page import="jp.co.fj.keinavi.data.individual.CourseData" %>
<%@ page import="jp.co.fj.keinavi.util.GeneralUtil" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<jsp:useBean id="i105Bean" scope="request" class="jp.co.fj.keinavi.beans.individual.I105Bean" />
<jsp:useBean id="form" scope="request" class="jp.co.fj.keinavi.forms.individual.I105Form" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<SCRIPT type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    /**
     * サブミットリフレッシュ
     */
    function submitRefresh() {
        submitForm('<c:out value="${param.forward}" />');
    }
    /**
     * サブミット
     */
    function submitForm(forward) {
        <%--スクロール位置 0426 K.Kondo--%>
        if(forward=='<c:out value="${param.forward}" />') {
            document.forms[0].scrollX.value = document.body.scrollLeft;
            document.forms[0].scrollY.value = document.body.scrollTop;
        } else {
            document.forms[0].scrollX.value = 0;
            document.forms[0].scrollY.value = 0;
        }
        document.forms[0].target = "_self";
        document.forms[0].forward.value = forward;
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 戻るボタン
     */
    function submitBack() {
        document.forms[0].target = "_self";
        document.forms[0].forward.value = "<c:out value="${param.backward}" />";
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * フォームデータ
     */
    var COMBOX = new Array(
    <%
    boolean firstFlg = true;
    for (java.util.Iterator it=i105Bean.getComboxDatas().iterator();  it.hasNext(); ) {
        IExamData data = (IExamData)it.next();
        if(!firstFlg){
            out.println(",");
        }
        out.print("new Array(");
        out.println("\"" + data.getExamCd() + "\"");
        out.println(",");
        out.println("\""+ data.getExamName() + "\"");
        out.print(")");
        firstFlg = false;
    }

    %>
    );
    /**
     * コンボボックス
     */
    function loadCombox(obj1){
        <%if(form.getTargetExamCode() != null){%>
        for(var i=0; i<COMBOX.length; i++){
            var input = document.createElement("OPTION");
            input.text = COMBOX[i][1];
            input.value = COMBOX[i][0];
            if(COMBOX[i][0] == "<%=form.getTargetExamCode()%>")
            input.selected = true;
            obj1.add(input);
        }
    <%}%>
    }

    /**
     *
     */
    function listnone() {
        if(<%=i105Bean.getExaminationDatas().size()%> <= 0){
            document.getElementById("list").style.display = "none";
        }
    }
    /**
     * 初期化
     */
    function init(){
        startTimer();
        <%--スクロール位置 0426 K.Kondo--%>
        window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

        loadCombox(document.forms[0].targetExamCode.options);
        listnone();

        //ロード中
        <%@ include file="/jsp/script/loading.jsp" %>

        <%-- アプレットを本来の位置へ移動する --%>
        $('#AppletLayer').children().prependTo('#AppletCell');
    }
//-->
</SCRIPT>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="AppletLayer" style="position:absolute;top:250px;left:450px;width:1px;height:1px;overflow:hidden;">
<!-- アプレット部：サーブレットよりVALUEを受けとるようにすること=========== -->
<%-- 2016/01/14 QQ)Hisakawa 大規模改修 DEL START --%>
<%--
<APPLET CODEBASE="./applets/" ARCHIVE="Applet.jar" CODE="BalanceChartApplet.class" id="rc" WIDTH="848" HEIGHT="320">
<PARAM NAME="dataSelect" VALUE="off">
<PARAM NAME="dispSelect" VALUE="on">
<PARAM NAME="dispZoneL" VALUE="<%=Float.valueOf(form.getLowerRange()).intValue()%>">
<PARAM NAME="dispZoneH" VALUE="<%=Float.valueOf(form.getUpperRange()).intValue()%>">
<PARAM NAME="subjectNum" VALUE="<%=i105Bean.getCourseDatas().size()%>">
<PARAM NAME="subjectDAT" VALUE="<%
                                int subCount = 0;
                                for (java.util.Iterator it=i105Bean.getCourseDatas().iterator(); it.hasNext();) {
                                    if(subCount != 0) out.print(",");
                                    out.print(((CourseData)it.next()).getCourseName());
                                    subCount ++;
                                }
                                %>">
<PARAM NAME="itemTITLE" VALUE="□対象生徒偏差値,■合格者平均偏差値">

<PARAM NAME="itemNUM" VALUE="<%=i105Bean.getExaminationDatas().size()%>">
<%
String moshiString = "";
String[] dataStrings = new String[i105Bean.getExaminationDatas().size()];
int moshiCount = 0;
String maxEngSubCd = "";
String maxMathSubCd = "";
String maxJapSubCd = "";
String maxSciSubCd = "";
String maxSocSubCd = "";

for (java.util.Iterator it=i105Bean.getExaminationDatas().iterator(); it.hasNext();) {
    IExamData data = (IExamData)it.next();

//	maxEngSubCd = "1000";
//	maxMathSubCd = "2000";
//	maxJapSubCd = "3000";
//	maxSciSubCd = "4000";
//	maxSocSubCd = "5000";

    String lowRange = form.getLowerRange();
    //String upRange = form.getUpperRange();
    String upRange = "100.0";
    if(moshiCount != 0) {
        //大学の場合
        moshiString += ",";
        dataStrings[moshiCount] += ",";

        if(form.getAvgChoice().equals("1")) {
            if(i105Bean.getCourseDatas().size() > 3) {
                if(form.isMarkexamflg() && form.isDispflg()){
                    //平均を表示だけど、平均値が存在するのは理科・地歴のみ、他は全部MAX
                    dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
                }else{
                    //平均を表示だけど、平均値が存在するのは理科・地歴のみ、他は全部MAX
                    dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
                }
            } else {
                if(form.isMarkexamflg() && form.isDispflg()){
                    dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
                }else{
                    dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
                }
            }
        } else if(form.getAvgChoice().equals("3")) {
            //第1解答科目
            if(i105Bean.getCourseDatas().size() > 3) {
                    if(form.isMarkexamflg() && form.isDispflg()){
                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
                        if (!maxSciSubCd.equals("-999")) {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+",";
                        } else {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+",";
                        }
                        if (!maxSocSubCd.equals("-999")) {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
                        } else {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
                        }
                    }else{
                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
                        if (!maxSciSubCd.equals("-999")) {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+",";
                        } else {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+",";
                        }
                        if (!maxSocSubCd.equals("-999")) {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
                        } else {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
                        }
                    }
            } else {
                    if(form.isMarkexamflg() && form.isDispflg()){
                        dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
                    }else{
                        dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
                    }
            }
        } else {
            if(i105Bean.getCourseDatas().size() > 3) {
                if(form.isMarkexamflg() && form.isDispflg()){
                    dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
                }else{
                    dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
                }
            } else {
                if(form.isMarkexamflg() && form.isDispflg()){
                    dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
                }else{
                    dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
                }

            }
        }
    } else {
        //模試の場合
        if(form.getAvgChoice().equals("1")) {
            if(i105Bean.getCourseDatas().size() > 3) {
                //平均を表示だけど、平均値が存在するのは理科・地歴のみ、他は全部MAX
                if(form.isMarkexamflg() && form.isDispflg()){
                    dataStrings[moshiCount] =
                                            GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                            GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                            GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
                                            GeneralUtil.toRangeLimit(data.getAvgSci(), lowRange, upRange)+","+
                                            GeneralUtil.toRangeLimit(data.getAvgSoc(), lowRange, upRange);

                                            maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                            maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                            maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                }else{
                    dataStrings[moshiCount] =
                                            GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                            GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                            GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
                                            GeneralUtil.toRangeLimit(data.getAvgSci(), lowRange, upRange)+","+
                                            GeneralUtil.toRangeLimit(data.getAvgSoc(), lowRange, upRange);

                                            maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                            maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                            maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                }
            } else {
                dataStrings[moshiCount] =
                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
            }
        } else if(form.getAvgChoice().equals("3")) {
            // 第1解答科目表示
            if(i105Bean.getCourseDatas().size() > 3) {
                    if(form.isMarkexamflg() && form.isDispflg()){
                        dataStrings[moshiCount] =
                                                GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAns1stSci(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAns1stSoc(), lowRange, upRange);

                                                maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                                maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                                maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                                                maxSciSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSci());
                                                maxSocSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSoc());
                    }else{
                        dataStrings[moshiCount] =
                                                GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAns1stSci(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAns1stSoc(), lowRange, upRange);

                                                maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                                maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                                maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                                                maxSciSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSci());
                                                maxSocSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSoc());
                    }
            } else {

                if(form.isMarkexamflg() && form.isDispflg()){
                                dataStrings[moshiCount] =
                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                                }else{
                                dataStrings[moshiCount] =
                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                                }
            }
        } else {
            if(i105Bean.getCourseDatas().size() > 3) {
                dataStrings[moshiCount] =
                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxSci(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxSoc(), lowRange, upRange);

                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                                        maxSciSubCd = GeneralUtil.to999(data.getMaxSciCode());
                                        maxSocSubCd = GeneralUtil.to999(data.getMaxSocCode());
            } else {
                dataStrings[moshiCount] =
                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
            }
        }
    }
    moshiString += data.getExamName();
    moshiCount ++;
}
%>
<PARAM NAME="itemDAT" VALUE="<%=moshiString%>">
<%for(int i=0; i<dataStrings.length; i++){%>

    <PARAM NAME="DAT<%=i+1%>" VALUE="<%=dataStrings[i]%>">
<%}%>
<PARAM NAME="colorDAT" VALUE="1,0,2,15,13,14">
</APPLET>
<!-- ======================================================================== -->
--%>
<%-- 2016/01/14 QQ)Hisakawa 大規模改修 DEL END   --%>
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I105Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="targetPersonId" value="<c:out value="${sessionScope['iCommonMap'].targetPersonId}" />">
<input type="hidden" name="lowerRange" value="<c:out value="${form.lowerRange}" />">
<input type="hidden" name="upperRange" value="<c:out value="${form.upperRange}" />">
<input type="hidden" name="avgChoice" value="<c:out value="${form.avgChoice}" />">
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="863" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="871" bgcolor="#FFFFFF" align="center">

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="832">

<table border="0" cellpadding="0" cellspacing="0" width="832">
<tr valign="top">
<td colspan="4" width="832" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="832" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="832" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="816" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">バランスチャート（合格者平均）　※前回模試グラフ</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="832" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="832" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--模試の種類-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="37" border="0" alt=""><br></td>
<td width="844">

<table border="0" cellpadding="0" cellspacing="3" width="844">
<tr>
<td width="838">
<table border="0" cellpadding="0" cellspacing="0" width="838">
<tr>
<td width="765" bgcolor="#E1E6EB">
<table border="0" cellpadding="6" cellspacing="0">
<tr>
<td><b class="text14">表示する前回模試の選択：</b></td>
<td><span class="text12">対象年度</span></td>
<td><select size="1" style="width:55px;">
                <option><c:out value="${form.targetExamYear}" /></td>
<td><span class="text12">対象模試</span></td>
<td><SELECT SIZE="1" NAME="targetExamCode" style="width:290px;"></SELECT></td>
</tr>
</table>
</td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="31" border="0" alt=""><br></td>
<td width="71" bgcolor="#F4E5D6" align="center"><input type="button" value="再表示" class="text12" style="width:60px;" onclick="javascript:submitRefresh()"></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="37" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="848" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/模試の種類-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--グラフイメージ-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<%-- 2016/01/14 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td width="848" id="AppletCell"> --%>
<%-- <br></td> --%>
<td width="848">
<%-- 2016/02/29 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<img src="./GraphServlet" width="848" height="320" border="0">
  --%>
<img src="<c:url value="./GraphServlet" />"  width="848" height="320" border="0">
<%-- 2016/02/29 QQ)Nishiyama 大規模改修 UPD END   --%>
</td>
<%-- 2016/01/14 QQ)Hisakawa 大規模改修 UPD END   --%>
</tr>
</table>
<!--/グラフイメージ-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<div id = "list">
<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="852">
<!--項目-->
<tr height="22" bgcolor="#CDD7DD" align="center">
<td colspan="2" width="29%"><span class="text12">&nbsp;</span></td>
<c:forEach var="courseData" items="${i105Bean.courseDatas}" varStatus="status">
    <td width="13%"><span class="text12"><c:out value="${courseData.courseName}" /></span></td>
</c:forEach>
</tr>
<!--/項目-->
<!--1段目-->
<%
int count = 0;
for (java.util.Iterator it=i105Bean.getExaminationDatas().iterator(); it.hasNext();) {
    IExamData data = (IExamData)it.next();

    if(count == 0){%>
        <tr height="36">
        <td colspan="2"  bgcolor="#E1E6EB"><span class="text12"><%=data.getExamName()%>（偏差値/学力レベル）</span></td>
        <%if(form.getAvgChoice().equals("1")){%>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAvgEng()%><% if(!"".equals(data.getAvgEng())){ %>/<%= data.getLevelAvgEng() %><%}%></span></td>
            <!---->
            <c:choose>
            <c:when test="${form.markexamflg && form.dispflg}">
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAvgMath()%><% if(!"".equals(data.getAvgMath())){ %>/<%= data.getLevelAvgMath() %><%}%></span></td>
            </c:when>
            <c:otherwise>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubMath()%><% if(!"".equals(data.getTargetSubMath())){ %>/<%= data.getLevelTargetMath() %><%}%></span></td>
            </c:otherwise>
            </c:choose>
            <!--/-->

            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubJap()%><% if(!"".equals(data.getTargetSubJap())){ %>/<%= data.getLevelTargetJap() %><%}%></span></td>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAvgSci()%><% if(!"".equals(data.getAvgSci())){ %>/<%= data.getLevelAvgSci() %><%}%></span></td>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAvgSoc()%><% if(!"".equals(data.getAvgSoc())){ %>/<%= data.getLevelAvgSoc() %><%}%></span></td>
        <%} else if(form.getAvgChoice().equals("3")){%>
            <!--第1解答課目-->
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAvgEng()%><% if(!"".equals(data.getAvgEng())){ %>/<%= data.getLevelAvgEng() %><%}%></span></td>
            <!---->
            <c:choose>
            <c:when test="${form.markexamflg && form.dispflg}">
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAvgMath()%><% if(!"".equals(data.getAvgMath())){ %>/<%= data.getLevelAvgMath() %><%}%></span></td>
            </c:when>
            <c:otherwise>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubMath()%><% if(!"".equals(data.getTargetSubMath())){ %>/<%= data.getLevelTargetMath() %><%}%></span></td>
            </c:otherwise>
            </c:choose>
            <!--/-->

            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubJap()%><% if(!"".equals(data.getTargetSubJap())){ %>/<%= data.getLevelTargetJap() %><%}%></span></td>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAns1stSci()%><% if(!"".equals(data.getAns1stSci())){ %>/<%= data.getLevelAns1stSci() %><%}%></span></td>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAns1stSoc()%><% if(!"".equals(data.getAns1stSoc())){ %>/<%= data.getLevelAns1stSoc() %><%}%></span></td>
        <%}else{%>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAvgEng()%><% if(!"".equals(data.getAvgEng())){ %>/<%= data.getLevelAvgEng() %><%}%></span></td>

            <!---->
            <c:choose>
            <c:when test="${form.markexamflg && form.dispflg}">
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAvgMath()%><% if(!"".equals(data.getAvgMath())){ %>/<%= data.getLevelAvgMath() %><%}%></span></td>
            </c:when>
            <c:otherwise>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubMath()%><% if(!"".equals(data.getTargetSubMath())){ %>/<%= data.getLevelTargetMath() %><%}%></span></td>
            </c:otherwise>
            </c:choose>
            <!--/-->

            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubJap()%><% if(!"".equals(data.getTargetSubJap())){ %>/<%= data.getLevelTargetJap() %><%}%></span></td>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubSci()%><% if(!"".equals(data.getTargetSubSci())){ %>/<%= data.getLevelTargetSci() %><%}%></span></td>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubSoc()%><% if(!"".equals(data.getTargetSubSoc())){ %>/<%= data.getLevelTargetSoc() %><%}%></span></td>
        <%}%>
        </tr>
    <%}else{%>
        <tr height="36">
        <% if (count == 1) {%>
            <td bgcolor="#CDD7DD"  align="center" rowspan="<%= i105Bean.getExaminationDatas().size() -1 %>" width="6%">
            <span class="text12">合格者<br>平均<br>偏差値</span></td>
        <% } %>
        <% if (count >= 1) {%>
            <td bgcolor="#E1E6EB"><span class="text12"><%=data.getExamName()%></span></td>
        <% } %>
        <%if(form.getAvgChoice().equals("1")){%>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubEng()%></span></td>

            <!---->
            <c:choose>
            <c:when test="${form.markexamflg && form.dispflg}">
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAvgMathSingle()%></span></td>
            </c:when>
            <c:otherwise>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubMath()%></span></td>
            </c:otherwise>
            </c:choose>
            <!--/-->

            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubJap()%></span></td>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAvgSciSingle()%></span></td>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAvgSocSingle()%></span></td>
        <%} else if(form.getAvgChoice().equals("3")){%>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubEng()%></span></td>

            <!---->
            <c:choose>
            <c:when test="${form.markexamflg && form.dispflg}">
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAvgMathSingle()%></span></td>
            </c:when>
            <c:otherwise>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubMath()%></span></td>
            </c:otherwise>
            </c:choose>
            <!--/-->

            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubJap()%></span></td>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetAns1stSubSci()%></span></td>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetAns1stSubSoc()%></span></td>
        <%}else{%>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubEng()%></span></td>

            <!---->
            <c:choose>
            <c:when test="${form.markexamflg && form.dispflg}">
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getAvgMathSingle()%></span></td>
            </c:when>
            <c:otherwise>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubMath()%></span></td>
            </c:otherwise>
            </c:choose>
            <!--/-->

            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubJap()%></span></td>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubSci()%></span></td>
            <td bgcolor="#F4E5D6" align="center"><span class="text12"><%=data.getTargetSubSoc()%></span></td>
        <%}%>
        </tr>
<%
    }
    count ++;
}
%>
<!--/1段目-->

<!--
<tr height="36">
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td align="center"><span class="text12">名古屋大学　工学部<br>電気電子情報工-前</span></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6" align="center"><span class="text12">45.0</span></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12">48.0</span></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12">49.0</span></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12">47.3</span></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12">53.0</span></td>
</tr>
/2段目-->
</table>
<!--/リスト-->
</div>

<!--ボタン-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td align="center"><input type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;" onclick="window.close()"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw_01.jsp" %>
<!--/FOOTER-->
</form>
</div>

</body>
</html>

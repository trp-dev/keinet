<%@ page contentType="text/html;charset=MS932" isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE>Kei-Navi／エラー</TITLE>
<META http-equiv=Content-Type content="text/html; charset=Shift_JIS">
<SCRIPT src="./shared_lib/style/stylesheet.js" type=text/javascript></SCRIPT>
<NOSCRIPT><LINK href="./shared_lib/style/stylesheet.css" type=text/css 
rel=stylesheet></NOSCRIPT>
<SCRIPT type="text/javascript">
<!--
    
	/**
	 * サブミット
	 */
	function submitForm(forward) {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 戻るボタン
	 */
	function submitBack() {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "i003";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * メニューページ遷移
	 */
	function back2Menu(){
		submitForm('i003');
	}
	/**
	 * 自動画面遷移
	 */
	var TIME_IN_SEC = 10;
	function goNow(){
		setTimeout("back2Menu()", 1000 * TIME_IN_SEC );
	}
	/**
	 * 初期化
	 */
	 function init(){
	 	goNow();
	 }
//-->
</SCRIPT>
<META content="MSHTML 6.00.2800.1458" name=GENERATOR>
</HEAD>
<BODY onload="init()" text=#2b2c2e link=#2986b1 bgColor=#f4ede2 leftMargin=0 topMargin=0 marginheight="0" marginwidth="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="I003Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="targetExamYear" value="<c:out value="sessionScope['iCommonMap'].targetExamYear"/>">
<input type="hidden" name="targetExamCode" value="<c:out value="sessionScope['iCommonMap'].targetExamCode"/>">
<input type="hidden" name="targetPersonId" value="<c:out value="sessionScope['iCommonMap'].targetPersonId"/>">
<input type="hidden" name="targetYear" value="<c:out value="sessionScope['iCommonMap'].targetExamYear"/>">
<input type="hidden" name="targetCode" value="<c:out value="sessionScope['iCommonMap'].targetExamCode"/>">
<TABLE cellSpacing=0 cellPadding=0 width=997 border=0>
  <TBODY>
  <TR vAlign=top>
    <TD width=997><IMG height=15 alt="" src="./shared_lib/img/parts/sp.gif" 
      width=2 border=0><BR></TD></TR></TBODY></TABLE><!--/spacer--><!--上部　オレンジライン-->
<TABLE cellSpacing=0 cellPadding=0 width=997 border=0>
  <TBODY>
  <TR vAlign=top>
    <TD width=12><IMG height=1 alt="" src="./shared_lib/img/parts/sp.gif" 
      width=12 border=0><BR></TD>
    <TD width=12><IMG height=11 alt="" 
      src="./shared_lib/img/parts/com_cnr_lt.gif" width=12 border=0><BR></TD>
    <TD width=960 background="./shared_lib/img/parts/com_bk_t.gif"><IMG height=11 
      alt="" src="./shared_lib/img/parts/sp.gif" width=960 border=0><BR></TD>
    <TD width=13><IMG height=11 alt="" 
      src="./shared_lib/img/parts/com_cnr_rt.gif" width=13 
  border=0><BR></TD></TR></TBODY></TABLE><!--/上部　オレンジライン--><!--MAIN-->
<TABLE cellSpacing=0 cellPadding=0 width=997 border=0>
  <TBODY>
  <TR vAlign=top>
    <TD width=12><IMG height=500 alt="" src="./shared_lib/img/parts/sp.gif" 
      width=12 border=0><BR></TD>
    <TD width=8 background="./shared_lib/img/parts/com_bk_l.gif"><IMG height=2 
      alt="" src="./shared_lib/img/parts/sp.gif" width=8 border=0><BR></TD>
    <TD align=middle width=968 bgColor=#ffffff><!--タイトルロゴ-->
      <DIV style="MARGIN-TOP: 20px">
      <TABLE cellSpacing=0 cellPadding=0 border=0>
        <TBODY>
        <TR vAlign=top>
          <TD><IMG height=96 alt="Kei-Navi (Kei-Net Step Up Navigator)" 
            src="./profile/img/top_logo.gif" width=467 
        border=0><BR></TD></TR></TBODY></TABLE></DIV><!--/タイトルロゴ-->

      <DIV style="MARGIN-TOP: 30px">
      <TABLE cellSpacing=0 cellPadding=0 width=859 border=0>
        <TBODY>
        <TR vAlign=top>
          <TD bgColor=#8ca9bb>
            <TABLE cellSpacing=1 cellPadding=0 width=859 border=0>
              <TBODY>
              <TR vAlign=top>
                <TD align=middle width=857 bgColor=#eff2f3>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TBODY>
                    <TR vAlign=top>
                      <TD width="100%"><IMG height=7 alt="" 
                        src="./shared_lib/img/parts/sp.gif" width=1 
                    border=0><BR></TD></TR></TBODY></TABLE><!--/spacer-->
                    <B CLASS="text14">
判定用データをロード中です。<br>しばらくおまちください。
　　　　　　　　　　</B>
                  　<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TBODY>
                    <TR vAlign=top>
                      <TD width="100%"><IMG height=7 alt="" 
                        src="./shared_lib/img/parts/sp.gif" width=1　border=0><BR></TD></TR></TBODY></TABLE><!--/spacer-->

<input type="button" value="　戻　る　" onclick="back2Menu();">

                    <TBODY>
                    <TR vAlign=top>
                      <TD width="100%"><IMG height=7 alt="" 
                        src="./shared_lib/img/parts/sp.gif" width=1 
                    border=0><BR></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD>

<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>

</TR></TBODY></TABLE></DIV><!--/成績分析を始めましょう！--><!--お問い合わせ-->
<TABLE cellSpacing=0 cellPadding=0 width=997 border=0>
  <TBODY>
  <TR vAlign=top>
    <TD width=12><IMG height=1 alt="" src="./shared_lib/img/parts/sp.gif" 
      width=12 border=0><BR></TD>
    <TD width=12><IMG height=15 alt="" 
      src="./shared_lib/img/parts/com_cnr_lb_w.gif" width=12 border=0><BR></TD>
    <TD width=960 background="./shared_lib/img/parts/com_bk_d.gif"><IMG height=15 
      alt="" src="./shared_lib/img/parts/sp.gif" width=960 border=0><BR></TD>
    <TD width=13><IMG height=15 alt="" 
      src="./shared_lib/img/parts/com_cnr_rb.gif" width=13 
  border=0><BR></TD></TR></TBODY></TABLE><!--/下部　ドロップシャドウ--><!--FOOTER-->
<DIV style="MARGIN-TOP: 7px">
<TABLE cellSpacing=0 cellPadding=0 width=984 border=0>
  <TBODY>
  <TR vAlign=top>
    <TD align=middle width=984><IMG height="20" 
      alt="Copyright: Kawaijuku Educational Information Network" 
      src="./shared_lib/include/footer/img/copyright.gif" width="380" 
  border="0"><BR></TD></TR></TBODY></TABLE></DIV><!--/FOOTER--></FORM></BODY></HTML>

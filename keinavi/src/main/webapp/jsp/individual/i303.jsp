<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/Validator.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<SCRIPT type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	/**
	 * サブミッション
	 */
	function submitForm(forward) {
		<%--スクロール位置 0426 K.Kondo--%>
		if(forward=='<c:out value="${param.forward}" />') {
			document.forms[0].scrollX.value = document.body.scrollLeft;
			document.forms[0].scrollY.value = document.body.scrollTop;
		} else {
			document.forms[0].scrollX.value = 0;
			document.forms[0].scrollY.value = 0;
		}
		document.forms[0].target = "_self";
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 戻るボタン
	 */
	function submitBack() {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "<c:out value="${param.backward}" />";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 頭文字検索サブミッション
	 */
	function submitChar(char){
		document.forms[0].button.value = 'char';
		document.forms[0].charValue.value = char;
		submitForm('i303');
	}
	/**
	 * 名前・コード検索サブミッション
	 */
	function submitSearch(){
		if(check()){
			document.forms[0].button.value = 'search';
			submitForm('i303');
		}
	}
	/**
	 * 編集サブミッション
	 */
	function submitEdit(){
		submitForm('i304');
	}
	/**
	 * キャンセルサブミッション
	 */
	function submitCancel(){
		window.close();
	}
	/**
	 * 入力チェック
	 */
	var pushed = false;
	function check() {
		// 入力チェック用オブジェクト
		var val = new Validator();
		// 大学名はひらがな
		if (!val.isHiragana(document.forms[0].searchStr.value)) {
			alert("大学名はひらがなで入力してください。");
			return false;
		}else{
			return true;
		}
		// ボタンは押されたことになる
		pushed = true;
	}
	/**
	 * 大学コンボ
	 */
	<%@ include file="/jsp/individual/include/univ_combox.jsp" %>
	/**
	 * 初期化
	 */
	function init(){
		startTimer();
		<%--スクロール位置 0426 K.Kondo--%>
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

		<c:if test="${univSearchBean.nestedUnivListSize != 0}">loadUnivList();</c:if>
		<c:if test="${univSearchBean.nestedUnivListSize == 0}">document.forms[0].editButton.disabled = true;</c:if>

		//ロード中
		<%@ include file="/jsp/script/loading.jsp" %>
	}
//-->
</SCRIPT>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I303Servlet" />" method="POST" onSubmit="return false">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="targetPersonId" value="<c:out value="${form.targetPersonId}" />">
<input type="hidden" name="button" value="">
<input type="hidden" name="charValue" value="">

<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">

<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="554">

<table border="0" cellpadding="0" cellspacing="0" width="554">
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="538" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">受験予定大学（新規登録）</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<div style="margin-top:5px;">
<table border="0" cellpadding="2" cellspacing="0" width="570">
<tr valign="top">
<td><span class="text14">受験を予定している大学を検索し、学科まで選択した状態で「編集画面へ」ボタンを押してください。</span></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="563">
<tr>
<td width="20"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="543"><b class="text14">大学検索</b></td>
</tr>
</table>
<!--大学名・大学コード検索-->
<div style="margin-top:4px;">
<table border="0" cellpadding="0" cellspacing="0" width="563">
<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="184" valign="top">
<!--直接入力-->
<table border="0" cellpadding="5" cellspacing="0" width="184">
<tr>
<td width="184" bgcolor="#CDD7DD"><b class="text12">&nbsp;大学名直接入力</b></td>
</tr>
</table>
<img src="./shared_lib/img/parts/sp.gif" width="184" height="2" border="0" alt=""><br>

<table border="0" cellpadding="0" cellspacing="0" width="184">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="184">
<tr valign="top">
<td width="182" height="195" bgcolor="#FBD49F" align="center">
<!--大学名orコード-->
<div style="margin-top:30px;">
<table border="0" cellpadding="0" cellspacing="0" width="165">
<tr>
<td><input type="hidden" name="searchMode" value="name">&nbsp;</td>
<!--
<td width="23"><input type="radio" name="searchMode" value="name"<c:if test="${ form.searchMode == 'name' }"> checked</c:if>></td>
<td width="59"><span class="text12">大学名</span></td>
<td width="23"><input type="radio"  name="searchMode" value="code"<c:if test="${ form.searchMode == 'code' }"> checked</c:if>></td>
<td width="60"><span class="text12">大学コード</span></td>
-->
</tr>
</table>
</div>
<!--/大学名orコード-->
<!--テキストボックス-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="165">
<tr>
<td width="165"><input type="text" size="20" class="text12" name="searchStr" style="width:165px;" value="<c:out value="${form.searchStr}" />"></td>
</tr>
</table>
</div>
<!--/テキストボックス-->
<!--検索ボタン-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="165">
<tr>
<td align="center"><input type="button" value="&nbsp;検索&nbsp;" class="text12" style="width:85px;" onclick="submitSearch()">
</td>
</tr>
</table>
</div>
<!--/検索ボタン-->
<!--注意-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><span class="text12">※</span></td>
<td><span class="text12">大学名はひらがなで<br>入力してください</span></td>
</tr>
</table>
</div>
<!--/注意-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/直接入力-->
</td>
<td width="22" align="center"><b class="text12">or</b></td>
<td width="337" valign="top">
<!--大学名頭文字指定-->
<table border="0" cellpadding="5" cellspacing="0" width="337">
<tr>
<td width="337" bgcolor="#CDD7DD"><b class="text12">&nbsp;大学名頭文字指定</b></td>
</tr>
</table>
<img src="./shared_lib/img/parts/sp.gif" width="337" height="2" border="0" alt=""><br>

<table border="0" cellpadding="0" cellspacing="0" width="337">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="337">
<tr valign="top">
<td width="335" height="195" bgcolor="#FBD49F" align="center">
<!--リスト-->
<div style="margin-top:5px;">
<table border="0" width="325">
<tr>
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD START --%>
<%--
<td width="30" height="30"><input type="button" value="あ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="か" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="さ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="た" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="な" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="は" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ま" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="や" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ら" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="わ" style="width:23px;" onclick="submitChar(this.value)"></td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="い" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="き" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="し" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ち" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="に" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ひ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="み" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
<td width="30" height="30"><input type="button" value="り" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="う" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="く" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="す" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="つ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ぬ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ふ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="む" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ゆ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="る" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="え" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="け" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="せ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="て" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ね" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="へ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="め" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
<td width="30" height="30"><input type="button" value="れ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="お" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="こ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="そ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="と" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="の" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ほ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="も" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="よ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ろ" style="width:23px;" onclick="submitChar(this.value)"></td>
--%>
<td width="30" height="30"><input type="button" value="あ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="か" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="さ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="た" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="な" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="は" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ま" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="や" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ら" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="わ" class="iniText" onclick="submitChar(this.value)"></td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="い" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="き" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="し" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ち" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="に" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ひ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="み" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
<td width="30" height="30"><input type="button" value="り" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="う" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="く" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="す" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="つ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ぬ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ふ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="む" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ゆ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="る" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="え" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="け" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="せ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="て" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ね" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="へ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="め" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
<td width="30" height="30"><input type="button" value="れ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="お" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="こ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="そ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="と" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="の" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ほ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="も" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="よ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ろ" class="iniText" onclick="submitChar(this.value)"></td>
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD START --%>
<td width="30" height="30"><br>
</td>
</tr>
</table>
</div>
<!--/リスト-->
<!--注意-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="335">
<tr>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="325"><span class="text12">※大学名の頭文字を選択してください</span></td>
</tr>
</table>
</div>
<!--/注意-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/大学名頭文字指定-->
</td>
</tr>
</table>
</div>
<!--/大学名・大学コード検索-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<c:if test="${univSearchBean.nestedUnivListSize != 0}">

	<table border="0" cellpadding="0" cellspacing="0" width="563">
	<tr>
	<td width="20"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
	<td width="543"><b class="text14">学科の選択</b><span class="text12">　※複数指定はできません</span></td>
	</tr>
	</table>
	<!--学科の選択-->
	<div style="margin-top:4px;">
	<table border="0" cellpadding="0" cellspacing="0" width="563">
	<tr>
	<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
	<td width="543" bgcolor="#F4E5D6">
	<table border="0" cellpadding="8" cellspacing="0" width="543">
	<tr>
	<td width="543">

	<table border="0" cellpadding="0" cellspacing="0" width="527">
	<tr>
	<td width="163" valign="top">
	<!--大学リスト-->
	<table border="0" cellpadding="0" cellspacing="0" width="163">
	<tr>
	<td width="163" bgcolor="#FFFFFF">
	<table border="0" cellpadding="0" cellspacing="2" width="163">
	<tr>
	<td width="159" height="17" bgcolor="#CDD7DD" align="center"><b class="text12">大学リスト</b></td>
	</tr>
	<tr>
	<td width="159"><select size="9" class="text12" style="width:159px;background-color:#CCC;" id="univCd" name="univCd" onchange="loadFacultyList()"></select></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--/大学リスト-->
	</td>
	<td width="19" align="center"><img src="./shared_lib/img/parts/arrow_right_darkblue_b.gif" width="8" height="39" border="0" alt="→"><br></td>
	<td width="163" valign="top">
	<!--学部リスト-->
	<table border="0" cellpadding="0" cellspacing="0" width="163">
	<tr>
	<td width="163" bgcolor="#FFFFFF">
	<table border="0" cellpadding="0" cellspacing="2" width="163">
	<tr>
	<td width="159" height="17" bgcolor="#CDD7DD" align="center"><b class="text12">学部リスト</b></td>
	</tr>
	<tr>
	<td width="159"><select size="9" class="text12" style="width:159px;background-color:#CCC;" id="facultyCd" name="facultyCd" onchange="loadDeptList()"></select></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--/学部リスト-->
	</td>
	<td width="19" align="center"><img src="./shared_lib/img/parts/arrow_right_darkblue_b.gif" width="8" height="39" border="0" alt="→"><br></td>
	<td width="163" valign="top">
	<!--学科リスト-->
	<table border="0" cellpadding="0" cellspacing="0" width="163">
	<tr>
	<td width="163" bgcolor="#FFFFFF">
	<table border="0" cellpadding="0" cellspacing="2" width="163">
	<tr>
	<td width="159" height="17" bgcolor="#CDD7DD" align="center"><b class="text12">学科リスト</b></td>
	</tr>
	<tr>
	<td width="159" align="center"><select size="9" class="text12" style="width:159px;background-color:#CCC;" id="deptCd" name="deptCd"></select></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--/学科リスト-->
	</td>
	</tr>
	</table>

	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</div>
	<!--学科の選択-->
</c:if>
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="15">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->


<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="570">
<tr valign="top">
<td width="568" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="&nbsp;キャンセル&nbsp;" class="text12" style="width:120px;" onclick="submitCancel()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td>

<input type="button" name="editButton" value="&nbsp;編集画面へ&nbsp;" class="text12" style="width:120px;" onclick="submitEdit()">

</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td align="center"><span class="text12">※キャンセルを押すと、登録せずにウィンドウを閉じます。</span></td>
</tr>
</table>
</div>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>

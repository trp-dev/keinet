<%@ page contentType="text/html;charset=MS932"  %>
<%@ page import="jp.co.fj.keinavi.util.individual.IPropsLoader" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />

<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/DatePicker.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">

<!--
    /**
     * 共通項目
     */
    <%@ include file="/jsp/script/timer.jsp" %>
    <%@ include file="/jsp/script/submit_change.jsp" %>
    <%@ include file="/jsp/script/submit_exit.jsp" %>
    <%@ include file="/jsp/script/submit_save.jsp" %>
    <%@ include file="/jsp/script/submit_menu.jsp" %>
    <%@ include file="/jsp/script/open_common.jsp" %>
    // ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

    <%-- 二次科目理科4単位科目のチェックボックスvalue定義 --%>
    var sci4Values = {};
    sci4Values.physics = 1;
    sci4Values.chemistry = 1;
    sci4Values.biology = 1;
    sci4Values.earthScience = 1;

    <%-- 二次科目理科基礎科目のチェックボックスvalue定義 --%>
    var sciBasicValues = {};
    sciBasicValues.physicsBasic = 1;
    sciBasicValues.chemistryBasic = 1;
    sciBasicValues.biologyBasic = 1;
    sciBasicValues.earthScienceBasic = 1;

    /**
     * サブミット
     */
    function submitForm(forward) {
        <%--スクロール位置 0426 K.Kondo--%>
        if(forward=='<c:out value="${param.forward}" />') {
            document.forms[0].scrollX.value = document.body.scrollLeft;
            document.forms[0].scrollY.value = document.body.scrollTop;
        } else {
            document.forms[0].scrollX.value = 0;
            document.forms[0].scrollY.value = 0;
        }
        document.getElementById("MainLayer").style.visibility = "hidden";
        document.getElementById("MainLayer").style.top = -2000;
        document.getElementById("LoadingLayer").style.visibility = "visible";

        document.forms[0].target = "_self";
        document.forms[0].forward.value = forward;
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";

        //サブミットした際に下記の物のdisabledがtrueだったら、falseにする。
        if(document.getElementById('subImposeDivA').disabled == true){
            document.getElementById('subImposeDivA').disabled = false;
            document.getElementById('subImposeDivB').disabled = false;
            var subs = document.all.item('imposeSub');
            for(var i=0; i<subs.length; i++){
                    subs[i].disabled = false;
            }
        }

        <c:if test="${sessionScope['iCommonMap'].bunsekiMode}">
            if(document.getElementById('indStudentUsageA').disabled == true){
                document.getElementById('indStudentUsageA').disabled = false;
                document.getElementById('indStudentUsageB').disabled = false;
            }
        </c:if>

        enableMD();//サブミットする直前に入試日を有効化してあげる

        document.forms[0].submit();
    }

    /**
     * 戻るボタン
     */
    function submitBack() {
        document.forms[0].target = "_self";
        document.forms[0].forward.value = "i201";
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }

    /**
     * 条件保存
     */
    function submitProviso() {
        if(isValidPeriod())
            submitForm('i202');
        else
            alert("開始・終了日付が不正です");
    }

    /**
     * 判定
     */
    function submitJudge() {
        if(isValidPeriod())
            submitForm('i204');
        else
            alert("開始・終了日付が不正です");
    }

    /**
     * 開始・終了日付チェック
     */
    function isValidPeriod(){

        var sMValue = document.forms[0].startMonth.options[document.forms[0].startMonth.selectedIndex].value;
        var eMValue = document.forms[0].endMonth.options[document.forms[0].endMonth.selectedIndex].value;
        var sDValue = document.forms[0].startDate.options[document.forms[0].startDate.selectedIndex].value;
        var eDValue = document.forms[0].endDate.options[document.forms[0].endDate.selectedIndex].value;

        if(sMValue == '1') sMValue = '13';
        if(sMValue == '2') sMValue = '14';
        if(sMValue == '3') sMValue = '15';
        if(eMValue == '1') eMValue = '13';
        if(eMValue == '2') eMValue = '14';
        if(eMValue == '3') eMValue = '15';
        if(parseInt(eMValue) - parseInt(sMValue) > 0){
            return true;
        }else if(parseInt(eMValue) - parseInt(sMValue) < 0){
            return false;
        }else{//if(parseInt(eMValue) - parseInt(sMValue) == 0){
            if(parseInt(eDValue) - parseInt(sDValue) >= 0){
                return true;
            }else{
                return false;
            }
        }
    }

    /**
     * フォームデフォルト設定
     */
    function restoreForm(){

        <%--searchStemmaDiv--%>
        var ssd = document.all.item('searchStemmaDiv');
        for(var i=0; i<ssd.options.length; i++){
            if(ssd.options[i].value == '<c:out value="${form.searchStemmaDiv}"/>'){
                ssd.options[i].selected = true;
                switchStemma();
            }
        }

        <%--descImposeDiv--%>
        var descImpose = document.all.item("descImposeDiv");
        if(descImpose.value == '<c:out value="${form.descImposeDiv}"/>'){
            descImpose.checked = true;
        }
        <%--subImposeDiv--%>
        var subImposeDiv = document.all.item("subImposeDiv");
        for(var i=0; i<subImposeDiv.length; i++){
            if(subImposeDiv[i].value == '<c:out value="${form.subImposeDiv}"/>'){
                subImposeDiv[i].checked = true;
            }
        }
        <%--imposeSub--%>
        var imposeSub = document.all.item("imposeSub");
        for(var i=0; i<imposeSub.length; i++){
            <c:forEach var="sid" items="${form.imposeSub}">if(imposeSub[i].value == '<c:out value="${sid}"/>')imposeSub[i].checked = true;</c:forEach>
        }
        <%--schoolDivProviso--%>
        var schoolDivProviso = document.all.item("schoolDivProviso");
        for(var i=0; i<schoolDivProviso.length; i++){
            <c:forEach var="sdp" items="${form.schoolDivProviso}">if(schoolDivProviso[i].value == '<c:out value="${sdp}"/>')schoolDivProviso[i].checked = true;</c:forEach>
        }
        <%--startDateProvisoとendDateProvisoはDatePickerで初期化--%>
        //do nothing
        <%--raingDiv--%>
        var raingDiv = document.all.item("raingDiv");
        for(var i=0; i<raingDiv.length; i++){
            if(raingDiv[i].value == '<c:out value="${form.raingDiv}"/>'){
                raingDiv[i].checked = true;
            }
        }
        <%--raingProviso--%>
        var raingProviso = document.all.item("raingProviso");
        for(var i=0; i<raingProviso.length; i++){
            <c:forEach var="sdp" items="${form.raingProviso}">if(raingProviso[i].value == '<c:out value="${sdp}"/>')raingProviso[i].checked = true;</c:forEach>
        }
        <%--outOfTergetProviso--%>
        var outOfTergetProviso = document.all.item("outOfTergetProviso");
        for(var i=0; i<outOfTergetProviso.length; i++){
            <c:forEach var="sdp" items="${form.outOfTergetProviso}">if(outOfTergetProviso[i].value == '<c:out value="${sdp}"/>')outOfTergetProviso[i].checked = true;</c:forEach>
        }

        <%-- dateSearchDiv --%>
        if(<c:out value="${form.dateSearchDiv}"/> == '0'){//無効化
            disableMD();
        }else{//デフォルトでチェックを付ける
            document.all.item("dateSearchDiv").checked = true;
        }
    }

    /**
     * 判定条件保存ダイアログ
     */
    function SaveMessage(){
        alert("こだわり判定の判定条件を保存しました。");
    }

    /**
     * 対象模試を受けていない生徒の場合にメッセージを出す
     */
    function messageCheck(){
        var notTakenMessage = "対象模試を受験していないため、判定できません。ただし、条件保存を行うことは可能です。";
<%--didTakeExamを使用するように修正 0407 K.Kondo delete
        for(var i=0; i<personString.length; i++){
            if(personString[i][0] == document.forms[0].targetPersonId.value){
                if(personString[i][4] == 'false'){
                    document.all.notTakenMessage.innerHTML = notTakenMessage;
                }
            }
        }
--%>
        if(didTakeExam() == 'false') {
            document.all.notTakenMessage.innerHTML = notTakenMessage;
        }
    }

    /**
     * 生徒を回す
     */
    <%@ include file="/jsp/individual/include/student_rotator.jsp" %>
    <%--
        チェックボックスのIDについて: id = a_b_c_d_e
        a : 1=対象地区選択 2=学部系統選択 3=その他選択
                b(1) : 01=北海道地区 02=東北地区 ...
                        c : 01=北海道 02=青森 ...
                b(2) : 2=中系統
                        c : 11=中系統文系 10=中系統理系
                            d: 11=文・人文 21=社会・国際 ...
                                e : 01=文学 02=外国語 03=哲・史・教・心 ...
                                    f : 0100=日本文 0200=外国文 0300=外国語 ...
                b(3) :
    --%>

    /**
     * 子チェックボックスに自動でチェックをつける
     */
    function checkChild(self){
        var iDLength = self.id.length;
        for(var i=0; i<document.forms[0].elements.length; i++){
            if(document.forms[0].elements[i].id.substring(0, iDLength) == self.id){
                if(self.checked == true)
                    document.forms[0].elements[i].checked = true;
                else
                    document.forms[0].elements[i].checked = false;
            }
        }
    }

    /**
     * 分野のチェックを外した際、小系統・中系統のチェックを外す
     */
    function uncheckSstema(self){

        if(self.checked == false){
            var parentId = self.id.substring(0,10);

            for(var i=0; i<document.forms[0].elements.length; i++){
                if(document.forms[0].elements[i].id == parentId){
                    if(self.checked == false){
                        document.forms[0].elements[i].checked = false;
                    }
                }
            }
            var parentId1 = self.id.substring(0, self.id.lastIndexOf("_"));
            for(var i=0; i<document.forms[0].elements.length; i++){
                if(document.forms[0].elements[i].id == parentId1){
                    if(self.checked == false){
                        document.forms[0].elements[i].checked = false;
                    }
                }
            }
        }
    }

    /**
     * 小系統のチェックをはずした際に、中系統のチェックをはずす
     */
    function uncheckMstema(self){

        if(self.checked == false){
            var parentId1 = self.id.substring(0, self.id.lastIndexOf("_"));
            for(var i=0; i<document.forms[0].elements.length; i++){
                if(document.forms[0].elements[i].id == parentId1){
                    if(self.checked == false){
                        document.forms[0].elements[i].checked = false;
                    }
                }
            }
        }
    }

    /**
     * 自分のチェックをつけた際に、兄弟もすべてついていたら親もつける
     */
    function checkParent(self){
        var chkFlg = true;
        var parentId = self.id.substring(0, self.id.lastIndexOf("_"));
        if(self.checked == true){
            for(var i=0; i<document.forms[0].elements.length; i++){
                if(document.forms[0].elements[i].id != parentId && document.forms[0].elements[i].id.length > parentId.length && document.forms[0].elements[i].id.substring(0, self.id.lastIndexOf("_")) == parentId){
                    if(document.forms[0].elements[i].checked == false){
                        chkFlg = false;
                    }
                }
            }

            if(chkFlg == true){
                if(document.getElementById(parentId) != null){
                    document.getElementById(parentId).checked = true;
                    //今付けた親の親もチェックする
                    checkParent(document.getElementById(parentId));
                }
            }
        }
     }

    /**
     * 全ての地区にチェックをつける
     */
    function checkAllPrefs(){
        for(var i=0; i<document.forms[0].elements.length; i++){
            if(document.forms[0].elements[i].name == "pref" || document.forms[0].elements[i].name == "block"){
                document.forms[0].elements[i].checked = true;
            }
        }
    }

    /**
     * 系統のドロップダウンボックスで内容を
     * 切り替える
     */
    function switchStemma(){
        document.getElementById('stemma2').style.display = 'none';
        document.getElementById('stemma11').style.display = 'none';
        document.getElementById('stemma10').style.display = 'none';
        document.getElementById('stemmaNote').style.display = 'none';
        if(document.forms[0].searchStemmaDiv.options[document.forms[0].searchStemmaDiv.selectedIndex].value == '1'){//中系統
            document.getElementById('stemma2').style.display = 'inline';
        }
        if(document.forms[0].searchStemmaDiv.options[document.forms[0].searchStemmaDiv.selectedIndex].value == '2'){//小系統文系
            document.getElementById('stemma11').style.display = 'inline';
            document.getElementById('stemmaNote').style.display = 'inline';
        }
        if(document.forms[0].searchStemmaDiv.options[document.forms[0].searchStemmaDiv.selectedIndex].value == '3'){//小系統理系
            document.getElementById('stemma10').style.display = 'inline';
            document.getElementById('stemmaNote').style.display = 'inline';
        }
    }

    /**
     * 分野選択をすべてクリアする
     */
    function clearStemma(){
        for(var i=0; i < document.forms[0].elements.length; i++){
            if(document.forms[0].elements[i].id.split('_')[0] == '2'){
                document.forms[0].elements[i].checked = false;
            }

        }
    }

    /**
     * 小系統をの▼を押すと分野が現れる
     */
    function expand(id){
        var open = "▽";
        var close = "▼";

        if(document.getElementById(id).style.display == 'none'){
            document.getElementById(id).style.display = 'inline';
            document.getElementById('a_'.concat(id.split('_')[0]).concat('_').concat(id.split('_')[1])).innerHTML = open;
        }else{
            document.getElementById(id).style.display = 'none';
            document.getElementById('a_'.concat(id.split('_')[0]).concat('_').concat(id.split('_')[1])).innerHTML = close;
        }
    }

    /**
     * 「（センター利用大）2次・独自試験を課さない」にチェックが入ると2次科目を課さないにする
     */
    function ImposeDivCK(){
        if(document.getElementById('ImposeDiv').checked == true){
            document.getElementById('subImposeDivA').disabled = true;
            document.getElementById('subImposeDivB').disabled = true;

            var subs = document.all.item('imposeSub');
            for(var i=0; i<subs.length; i++){
                    subs[i].disabled = true;
            }

        }else{
            document.getElementById('subImposeDivA').disabled = false;
            document.getElementById('subImposeDivB').disabled = false;

            var subs = document.all.item('imposeSub');
            for(var i=0; i<subs.length; i++){
                <%-- 理科基礎科目以外を処理する --%>
                if (!sciBasicValues[subs[i].value]) {
                    subs[i].disabled = false;
                    <%-- 理科専門科目のチェック状態によって、理科基礎科目の状態を変更する --%>
                    if (sci4Values[subs[i].value]) {
                    	document.getElementById(subs[i].value + 'BasicCheckBox').disabled = !subs[i].checked;
                    }
                }
            }
        }
    }

    /**
     *  「条件保存されていない生徒の処理」ラジオボタンを選択中の生徒全員、選択時のみ、操作可能にする。
     */
    function JudgeStudentCK(){
        var flag = document.forms[0].judgeStudent.value != '1';
        document.getElementById('indStudentUsageA').disabled = flag;
        document.getElementById('indStudentUsageASub').disabled = flag;
        document.getElementById('indStudentUsageB').disabled = flag;
        document.getElementById('indStudentUsageBSub').disabled = flag;
    }

    /**
     *  課す・課さないチェックの動きで科目チェックボックスをクリアする
     */
    function ImposeSubClear(){
        var subs = document.all.item('imposeSub');
        for(var i=0; i<subs.length; i++){
            subs[i].checked = false
            <%-- 理科基礎科目は状態を無効にする  --%>
            if (sciBasicValues[subs[i].value]) {
                subs[i].disabled = true;
            }
        }
    }

    /**
     *  課す・課さないチェックの動きで科目チェックボックスの動きを変える
     */
    function ImposeSubCK(self){
        if(document.getElementById('subImposeDivA').checked != true){
            var subs = document.all.item('imposeSub');
            //自分以外全てのチェックをはずす
            for(var i=0; i<subs.length; i++){
                if (subs[i] != self) {
                    subs[i].checked = false;
                    <%-- 理科基礎科目は状態を無効にする  --%>
                    if (sciBasicValues[subs[i].value]) {
                        subs[i].disabled = true;
                    }
                }
            }
        }
        <%-- 理科4単位科目がチェックされた場合の処理を行う --%>
        if (sci4Values[self.value]) {
            var basic = document.getElementById(self.value + 'BasicCheckBox');
            if (self.checked) {
                basic.disabled = false;
            } else {
                basic.disabled = true;
                basic.checked = false;
            }
        }
    }

    /**
     * 数学用連動チェック
     * 下位の数学をすべてチェック
     */
    function checkMath(index){
        if(document.getElementById('subImposeDivA').checked == true){
            //課す
            for(var i=1; i<=index; i++){
                document.getElementById("m_"+i).checked = true;
            }
        }else{
            //課さない
            ImposeSubCK(index);
            for(var i=1; i<=5; i++){
                document.getElementById("m_"+i).checked = true;
            }
        }
    }

    /**
     * 数学用連動アンチェック
     * 上位の数学をすべてアンチェック
     */
    function uncheckMath(index){
        if(document.getElementById('subImposeDivA').checked == true){
            //課す
            for(var i=index; i<=5; i++){
                document.getElementById("m_"+i).checked = false;
            }
        }else{
            //課さない
            ImposeSubCK(index);
            for(var i=1; i<=5; i++){
                document.getElementById("m_"+i).checked = false;
            }
        }
    }

    /**
     * 国語用連動チェック
     * 下位の国語をすべてチェック
     */
    function checkJap(index){
        if(document.getElementById('subImposeDivA').checked == true){
            //課す
            for(var i=1; i<index+1; i++){
                document.getElementById("j_"+i).checked = true;
            }
        }else{
            //課さない
            ImposeSubCK(index);

            for(var i=1; i<4; i++){
                document.getElementById("j_"+i).checked = true;
            }
        }
    }

    /**
     * 国語用連動アンチェック
     * 上位の国語をすべてアンチェック
     */
    function uncheckJap(index){
        if(document.getElementById('subImposeDivA').checked == true){
            //課す
            var max = 3;
            for(var i=index; i<max+1; i++){
                document.getElementById("j_"+i).checked = false;
            }
        }else{
            //課さない
            var max = 4;
            for(var i=1; i<max; i++){
                document.getElementById("j_"+i).checked = false;
            }
        }
    }

    /**
     * 入試日条件を有効化する
     */
    function enableMD(){
        document.getElementById('MM').disabled = false;
        document.getElementById('MM2').disabled = false;
        document.getElementById('DD').disabled = false;
        document.getElementById('DD2').disabled = false;
    }

    /**
     * 入試日条件を無効化する
     */
    function disableMD(){
        document.getElementById('MM').disabled = true;
        document.getElementById('MM2').disabled = true;
        document.getElementById('DD').disabled = true;
        document.getElementById('DD2').disabled = true;
    }

    // 都道府県データ
    var data = new Array();
    <c:forEach var="block" items="${ PrefBean.blockCodeList }">
        data["<c:out value="${block}" />"] = new Array(
        <c:forEach var="pref" items="${ PrefBean.blockMap[block] }" varStatus="status">
            <c:if test="${ status.index > 0 }">,</c:if>
            "<c:out value="${pref}" />"
        </c:forEach>
        );
    </c:forEach>

    function checkAll() {
        checkElemtns("block", true);
        checkElemtns("pref", true);
    }

    function checkBlock(obj) {
        var form = document.forms[0];
        var block = data[obj.value];
        var elements = form.elements["pref"];
        for (var i=0; i<elements.length; i++) {
            for (var j=0; j<block.length; j++) {
                if (block[j] == elements[i].value) elements[i].checked = obj.checked;
            }
        }
    }

    function checkPref(obj) {
        var form = document.forms[0];
        var e1 = form.elements["block"];
        var e2 = form.elements["pref"];
        var block = "";
        // チェックされた都道府県がある地区を探す
        for (var i=0; i<e1.length; i++) {
            var pref = data[e1[i].value];
            for (var j=0; j<pref.length; j++) {
                if (pref[j] == obj.value) {
                    block = e1[i].value;
                    break;
                }
            }
        }
        // 同じ地区の都道府県が全てチェックされているかどうか
        var flag = true;
        for (var i=0; i<e2.length; i++) {
            var pref = data[block];
            for (var j=0; j<pref.length; j++) {
                if (pref[j] == e2[i].value) {
                    if (!e2[i].checked) flag = false;
                }
            }
        }
        // 地区のチェック状態を変更
        for (var i=0; i<e1.length; i++) {
            if (e1[i].value == block) e1[i].checked = flag;
        }
    }

    /**
     * 判定できるかどうかチェック
     */
    function isJudgable(){
        var flg = true;
        <c:choose>
            <c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
                var choice = document.forms[0].elements["judgeStudent"].options[document.forms[0].elements["judgeStudent"].selectedIndex].value;
                if(choice != '1'){//表示中の生徒一人を選択している
                    if(!didTakeExam()){
                        flg = false;
                    }
                }
            </c:when>
            <c:otherwise>
                if(!didTakeExam()){
                    flg = false;
                }
            </c:otherwise>
        </c:choose>
        if(!flg){
            window.alert("この生徒は対象模試を受験していないので判定できません");
        }
        return flg;
    }
<%--student_rotatorに移動 0407 K.Kondo delte
    /**
     * 表示中の生徒が対象模試を受験しているかどうか
     */
    function didTakeExam(){
        var flg = true;
        for(var i=0; i<personString.length; i++){
            if(personString[i][0] == document.forms[0].targetPersonId.value){
                if(personString[i][4] == 'false'){
                    flg = false;
                }
            }
        }
        return flg;
    }
--%>

    /**
     * 初期化
     */
    var ps, ps2;//コンボボックスグローバル化
    function init(){
        startTimer();
        <%--スクロール位置 0426 K.Kondo--%>
        window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
        //1.選択中の生徒まで回す
        findStudent();
        //2.選択中の生徒が一人ならArrowボタンを無効化する
        if(getStudentNum() == 1) disableArrows();

        //選択中生徒の学年によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
        gradeChange();

        switchStemma();
        messageCheck();
        ps = new DatePicker(<c:out value="${form.thisYear}" />, <c:out value="${form.startMonth}" />, <c:out value="${form.startDate}" />, 'MM', 'DD');
        ps2 = new DatePicker(<c:out value="${form.thisYear}" />, <c:out value="${form.endMonth}" />, <c:out value="${form.endDate}" />, 'MM2', 'DD2');
        restoreForm();
        ImposeDivCK();

        //判定対象生徒のinit処理
        <c:if test="${sessionScope['iCommonMap'].bunsekiMode}">JudgeStudentCK();</c:if>

        //条件保存ダイアログ
        <c:if test="${form.saveflg}">SaveMessage();</c:if>

        //ロード中
        <%@ include file="/jsp/script/loading.jsp" %>
    }
//-->
</script>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I202Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="button" value="byCond">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">
<%-- page unique params --%>
<input type="hidden" name="thisYear" value="<c:out value="${form.thisYear}"/>">
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->


<!--志望大学判定-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--概要-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr valign="top">
<td width="183">
<!--●●●左側●●●-->

<!--志望大学判定トップへ-->
<table border="0" cellpadding="0" cellspacing="0" width="183">
<tr valign="top">
<td colspan="2" width="171" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="171" height="1" border="0" alt=""><br></td>
<td rowspan="2" width="12" background="./individual/img/tbl_cnr_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="170" bgcolor="#4694AF">

<table border="0" cellpadding="0" cellspacing="0" width="170">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="23" border="0" alt=""><br></td>
<td width="13"><a href="#"><img src="./shared_lib/img/parts/arrow_left_white.gif" width="8" height="9" border="0" alt=""></a></td>
<td width="151">
<span class="text12"><a href="javascript:submitForm('i201')" style="color:#FFFFFF;">志望大学判定トップ</a></span></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/志望大学判定トップへ-->

<!--STEP-->
<table border="0" cellpadding="0" cellspacing="0" width="183">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#4694AF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="180" bgcolor="#F3F5F5" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--step1-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr height="22" bgcolor="#FF8F0E">
<td width="55" align="right"><img src="./shared_lib/img/step/step1_cur.gif" width="46" height="12" border="0" alt="STEP1"><br></td>
<td width="13"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="94"><b class="text12" style="color:#FFFFFF;">条件設定</b></td>
</tr>
</table>
<!--/step1-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr>
<td width="162" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step2-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step2_def.gif" width="46" height="12" border="0" alt="STEP2"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="94" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">判　定</b></td>
</tr>
</table>
<!--/step2-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="4" width="183" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="183" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/STEP-->
<!--/●●●左側●●●-->
</td>
<td width="17"><img src="./shared_lib/img/parts/sp.gif" width="17" height="1" border="0" alt=""><br></td>
<td width="678">
<!--●●●右側●●●-->
<table border="0" cellpadding="0" cellspacing="0" width="678">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="678">
<tr>
<td width="676" bgcolor="#FFFFFF"><span class="text12">&nbsp;<b>対象模試：</b><c:out value="${sessionScope['iCommonMap'].targetExamName}"/></span></td>
</tr>
<tr>
<td width="676" bgcolor="#FFFFFF">

<table border="0" cellpadding="0" cellspacing="0" width="662">
<tr>
<td width="24"><img src="./shared_lib/img/parts/sp.gif" width="24" height="1" border="0" alt=""><br></td>
<td width="638"><b class="text14-hh" style="color:#FF6E0E"><span id="notTakenMessage"></span></b></td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="662">
<tr>
<td width="22"><img src="./shared_lib/img/parts/sp.gif" width="22" height="1" border="0" alt=""><br></td>
<td width="634">

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="634">
<tr>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="200" valign="bottom"><b class="text14">&nbsp;1.対象地区の選択</b><br></td>
<td align="right" width="430">

<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1">
<tr valign="top">
<td bgcolor="#FBD49F">

    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <c:if test="${sessionScope['iCommonMap'].bunsekiMode}">
        <td>
        <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td><select name="judgeStudentSub" class="text12" style="width:162;" onchange = "document.forms[0].judgeStudent.selectedIndex=this.selectedIndex;JudgeStudentCK();">
            <option value="1" <c:if test="${form.judgeStudent == '1'}">selected</c:if> selected> 選択中の生徒すべて</option>
            <option value="2" <c:if test="${form.judgeStudent == '2'}">selected</c:if>> 表示中の生徒一人</option>
            </select></td>
        </tr>
        <tr>
            <td>
            <table border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td colspan="4"><img src="./shared_lib/img/parts/sp.gif" width="1" height="4" border="0" alt=""></td>
            </tr>
            <tr>
            <td colspan="4"><span class="text10">&nbsp;条件保存されていない生徒の処理</span></td>
            </tr>
            <tr>
            <td><input type="radio" id = "indStudentUsageASub" name="indStudentUsageSub" value="1" onclick="document.forms[0].indStudentUsage[0].checked=true;" <c:if test="${form.indStudentUsage == '1'}">checked</c:if>></td>
            <td width="80"><span class="text12">判定しない</span></td>
            <td><input type="radio" id = "indStudentUsageBSub" name="indStudentUsageSub" value="2" onclick="document.forms[0].indStudentUsage[1].checked=true;" <c:if test="${form.indStudentUsage == '2'}">checked</c:if>></td>
            <td><span class="text12">オマカセ判定</span></td>
            </tr>
            </table>
            </td>
        </tr>
        </table>
        </td>
        <td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""></td>
    </c:if>
    <td>
    <table border="0" cellpadding="0" cellspacing="0">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="5" cellspacing="1">
    <tr bgcolor="#FFFFFF">
    <td align="center"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><a href="javascript:submitJudge()" onclick="return isJudgable();"><img src="./shared_lib/img/btn/hantei2.gif" width="96" height="36" border="0" alt="判定"></a><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
</tr>
</table>
</td>
</tr>
<tr>
<td colspan="4"><img src="./shared_lib/img/parts/sp.gif" width="1" height="4" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボタン-->

<!--対象地区の選択-->

<%
    // 地区のチェックマップ
    java.util.HashMap c1 = new java.util.HashMap();
    pageContext.setAttribute("c1", c1);
    // 県のチェックマップ
    java.util.HashMap c2 = new java.util.HashMap();
    pageContext.setAttribute("c2", c2);
%>
<c:forEach var="block" items="${form.block}">
<% c1.put(pageContext.getAttribute("block"), " checked"); %>
</c:forEach>
<c:forEach var="pref" items="${form.pref}">
<% c2.put(pageContext.getAttribute("pref"), " checked"); %>
</c:forEach>

<table border="0" cellpadding="5" cellspacing="2" width="634">
<tr>
<td colspan="2" width="100%" bgcolor="#93A3AD">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input onclick="checkAllPrefs()" type="button" value="&nbsp;すべて選択&nbsp;" class="text12" style="width:100px;"></td>
</tr>
</table>
</td>
</tr>
<!--北海道-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="01" onClick="checkBlock(this)"<c:out value="${c1['01']}" />></td>
<td><span class="text12">北海道地区</span></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="01" onClick="checkPref(this)"<c:out value="${c2['01']}" />></td>
<td width="40"><span class="text12">北海道</span></td>
</tr>
</table>
</td>
</tr>
<!--/北海道-->

<!--東北地区-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="02" onClick="checkBlock(this)"<c:out value="${c1['02']}" />></td>
<td><span class="text12">東北地区</span></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="02" onClick="checkPref(this)"<c:out value="${c2['02']}" />></td>
<td width="40"><span class="text12">青森</span></td>
<td width="20"><input type="checkbox" name="pref" value="03" onClick="checkPref(this)"<c:out value="${c2['03']}" />></td>
<td width="40"><span class="text12">岩手</span></td>
<td width="20"><input type="checkbox" name="pref" value="04" onClick="checkPref(this)"<c:out value="${c2['04']}" />></td>
<td width="40"><span class="text12">宮城</span></td>
<td width="20"><input type="checkbox" name="pref" value="05" onClick="checkPref(this)"<c:out value="${c2['05']}" />></td>
<td width="40"><span class="text12">秋田</span></td>
<td width="20"><input type="checkbox" name="pref" value="06" onClick="checkPref(this)"<c:out value="${c2['06']}" />></td>
<td width="40"><span class="text12">山形</span></td>
<td width="20"><input type="checkbox" name="pref" value="07" onClick="checkPref(this)"<c:out value="${c2['07']}" />></td>
<td width="40"><span class="text12">福島</span></td>
</tr>
</table>
</td>
</tr>
<!--/東北地区-->

<!--関東地区-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="03" onClick="checkBlock(this)"<c:out value="${c1['03']}" />></td>
<td><span class="text12">関東地区</span></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="08" onClick="checkPref(this)"<c:out value="${c2['08']}" />></td>
<td width="40"><span class="text12">茨城</span></td>
<td width="20"><input type="checkbox" name="pref" value="09" onClick="checkPref(this)"<c:out value="${c2['09']}" />></td>
<td width="40"><span class="text12">栃木</span></td>
<td width="20"><input type="checkbox" name="pref" value="10" onClick="checkPref(this)"<c:out value="${c2['10']}" />></td>
<td width="40"><span class="text12">群馬</span></td>
<td width="20"><input type="checkbox" name="pref" value="11" onClick="checkPref(this)"<c:out value="${c2['11']}" />></td>
<td width="40"><span class="text12">埼玉</span></td>
<td width="20"><input type="checkbox" name="pref" value="12" onClick="checkPref(this)"<c:out value="${c2['12']}" />></td>
<td width="40"><span class="text12">千葉</span></td>
<td width="20"><input type="checkbox" name="pref" value="13" onClick="checkPref(this)"<c:out value="${c2['13']}" />></td>
<td width="40"><span class="text12">東京</span></td>
</tr>
<tr>
<td width="20"><input type="checkbox" name="pref" value="14" onClick="checkPref(this)"<c:out value="${c2['14']}" />></td>
<td width="40"><span class="text12">神奈川</span></td>
<td width="20"></td>
<td width="40"></td>
<td width="20"></td>
<td width="40"></td>
<td width="20"></td>
<td width="40"></td>
<td width="20"></td>
<td width="40"></td>
<td width="20"></td>
<td width="40"></td>
</tr>
</table>
</td>
</tr>
<!--/関東地区-->
<!--甲信越地区-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="04" onClick="checkBlock(this)"<c:out value="${c1['04']}" />></td>
<td><span class="text12">甲信越地区</span></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="15" onClick="checkPref(this)"<c:out value="${c2['15']}" />></td>
<td width="40"><span class="text12">新潟</span></td>
<td width="20"><input type="checkbox" name="pref" value="19" onClick="checkPref(this)"<c:out value="${c2['19']}" />></td>
<td width="40"><span class="text12">山梨</span></td>
<td width="20"><input type="checkbox" name="pref" value="20" onClick="checkPref(this)"<c:out value="${c2['20']}" />></td>
<td width="40"><span class="text12">長野</span></td>
</tr>
</table>
</td>
</tr>
<!--/甲信越地区-->

<!--北陸地区-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="05" onClick="checkBlock(this)"<c:out value="${c1['05']}" />></td>
<td><span class="text12">北陸地区</span></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="16" onClick="checkPref(this)"<c:out value="${c2['16']}" />></td>
<td width="40"><span class="text12">富山</span></td>
<td width="20"><input type="checkbox" name="pref" value="17" onClick="checkPref(this)"<c:out value="${c2['17']}" />></td>
<td width="40"><span class="text12">石川</span></td>
<td width="20"><input type="checkbox" name="pref" value="18" onClick="checkPref(this)"<c:out value="${c2['18']}" />></td>
<td width="40"><span class="text12">福井</span></td>
</tr>
</table>
</td>
</tr>
<!--/北陸地区-->

<!--東海地区-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="06" onClick="checkBlock(this)"<c:out value="${c1['06']}" />></td>
<td><span class="text12">東海地区</span></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="21" onClick="checkPref(this)"<c:out value="${c2['21']}" />></td>
<td width="40"><span class="text12">岐阜</span></td>
<td width="20"><input type="checkbox" name="pref" value="22" onClick="checkPref(this)"<c:out value="${c2['22']}" />></td>
<td width="40"><span class="text12">静岡</span></td>
<td width="20"><input type="checkbox" name="pref" value="23" onClick="checkPref(this)"<c:out value="${c2['23']}" />></td>
<td width="40"><span class="text12">愛知</span></td>
<td width="20"><input type="checkbox" name="pref" value="24" onClick="checkPref(this)"<c:out value="${c2['24']}" />></td>
<td width="40"><span class="text12">三重</span></td>
</tr>
</table>
</td>
</tr>
<!--/東海地区-->

<!--近畿地区-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="07" onClick="checkBlock(this)"<c:out value="${c1['07']}" />></td>
<td><span class="text12">近畿地区</span></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="25" onClick="checkPref(this)"<c:out value="${c2['25']}" />></td>
<td width="40"><span class="text12">滋賀</span></td>
<td width="20"><input type="checkbox" name="pref" value="26" onClick="checkPref(this)"<c:out value="${c2['26']}" />></td>
<td width="40"><span class="text12">京都</span></td>
<td width="20"><input type="checkbox" name="pref" value="27" onClick="checkPref(this)"<c:out value="${c2['27']}" />></td>
<td width="40"><span class="text12">大阪</span></td>
<td width="20"><input type="checkbox" name="pref" value="28" onClick="checkPref(this)"<c:out value="${c2['28']}" />></td>
<td width="40"><span class="text12">兵庫</span></td>
<td width="20"><input type="checkbox" name="pref" value="29" onClick="checkPref(this)"<c:out value="${c2['29']}" />></td>
<td width="40"><span class="text12">奈良</span></td>
<td width="20"><input type="checkbox" name="pref" value="30" onClick="checkPref(this)"<c:out value="${c2['30']}" />></td>
<td width="40"><span class="text12">和歌山</span></td>
</tr>
</table>
</td>
</tr>
<!--/近畿地区-->

<!--中国地区-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="08" onClick="checkBlock(this)"<c:out value="${c1['08']}" />></td>
<td><span class="text12">中国地区</span></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="31" onClick="checkPref(this)"<c:out value="${c2['31']}" />></td>
<td width="40"><span class="text12">鳥取</span></td>
<td width="20"><input type="checkbox" name="pref" value="32" onClick="checkPref(this)"<c:out value="${c2['32']}" />></td>
<td width="40"><span class="text12">島根</span></td>
<td width="20"><input type="checkbox" name="pref" value="33" onClick="checkPref(this)"<c:out value="${c2['33']}" />></td>
<td width="40"><span class="text12">岡山</span></td>
<td width="20"><input type="checkbox" name="pref" value="34" onClick="checkPref(this)"<c:out value="${c2['34']}" />></td>
<td width="40"><span class="text12">広島</span></td>
<td width="20"><input type="checkbox" name="pref" value="35" onClick="checkPref(this)"<c:out value="${c2['35']}" />></td>
<td width="40"><span class="text12">山口</span></td>
</tr>
</table>
</td>
</tr>
<!--/中国地区-->

<!--四国地区-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="09" onClick="checkBlock(this)"<c:out value="${c1['09']}" />></td>
<td><span class="text12">四国地区</span></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="36" onClick="checkPref(this)"<c:out value="${c2['36']}" />></td>
<td width="40"><span class="text12">徳島</span></td>
<td width="20"><input type="checkbox" name="pref" value="37" onClick="checkPref(this)"<c:out value="${c2['37']}" />></td>
<td width="40"><span class="text12">香川</span></td>
<td width="20"><input type="checkbox" name="pref" value="38" onClick="checkPref(this)"<c:out value="${c2['38']}" />></td>
<td width="40"><span class="text12">愛媛</span></td>
<td width="20"><input type="checkbox" name="pref" value="39" onClick="checkPref(this)"<c:out value="${c2['39']}" />></td>
<td width="40"><span class="text12">高知</span></td>
</tr>
</table>
</td>
</tr>
<!--/四国地区-->

<!--九州・沖縄地区-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="10" onClick="checkBlock(this)"<c:out value="${c1['10']}" />></td>
<td><span class="text12">九州・沖縄地区</span></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="40" onClick="checkPref(this)"<c:out value="${c2['40']}" />></td>
<td width="40"><span class="text12">福岡</span></td>
<td width="20"><input type="checkbox" name="pref" value="41" onClick="checkPref(this)"<c:out value="${c2['41']}" />></td>
<td width="40"><span class="text12">佐賀</span></td>
<td width="20"><input type="checkbox" name="pref" value="42" onClick="checkPref(this)"<c:out value="${c2['42']}" />></td>
<td width="40"><span class="text12">長崎</span></td>
<td width="20"><input type="checkbox" name="pref" value="43" onClick="checkPref(this)"<c:out value="${c2['43']}" />></td>
<td width="40"><span class="text12">熊本</span></td>
<td width="20"><input type="checkbox" name="pref" value="44" onClick="checkPref(this)"<c:out value="${c2['44']}" />></td>
<td width="40"><span class="text12">大分</span></td>
<td width="20"><input type="checkbox" name="pref" value="45" onClick="checkPref(this)"<c:out value="${c2['45']}" />></td>
<td width="40"><span class="text12">宮崎</span></td>
</tr>
<tr>
<td width="20"><input type="checkbox" name="pref" value="46" onClick="checkPref(this)"<c:out value="${c2['46']}" />></td>
<td width="40"><span class="text12">鹿児島</span></td>
<td width="20"><input type="checkbox" name="pref" value="47" onClick="checkPref(this)"<c:out value="${c2['47']}" />></td>
<td width="40"><span class="text12">沖縄</span></td>
<td width="20"></td>
<td width="40"></td>
<td width="20"></td>
<td width="40"></td>
<td width="20"></td>
<td width="40"></td>
<td width="20"></td>
<td width="40"></td>
</tr>
</table>
</td>
</tr>
<!--/九州・沖縄地区-->
</table>

<!--/対象地区の選択-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<c:forEach var="block" items="${form.block}">
<% c1.put(pageContext.getAttribute("block"), " checked"); %>
</c:forEach>
<c:forEach var="pref" items="${form.pref}">
<% c2.put(pageContext.getAttribute("pref"), " checked"); %>
</c:forEach>

<b class="text14">&nbsp;2.学部系統の選択</b><br>

<!--系統の選択-->
<table border="0" cellpadding="5" cellspacing="2" width="634">
<tr valign="top">
<td width="20%" bgcolor="#E1E6EB">
<!--系統区分-->
<table border="0" cellpadding="0" cellspacing="4">
<tr>
<td><b class="text12">系統区分：</b></td>
</tr>
<tr>
<td><select name="searchStemmaDiv" class="text12" style="width:99px;" onchange="clearStemma();switchStemma();">
<option value="1">中系統</option>
<option value="2">小系統（文系）</option>
<option value="3">小系統（理系）</option>
</select></td>
</tr>
<tr>
<td><div style="margin-top:5px;" id="stemmaNote"><span class="text10-hh"><font style="color:#189CC8;"><u>▼</u></font>をクリックするとさらに細かい分野を選択することができます。</span></div></td>
</tr>
</table>
<!--/系統区分-->
</td>
<td width="80%" bgcolor="#F4E5D6">
<!--中系統リスト-->
<table border="0" cellpadding="2" cellspacing="0" id="stemma2">
<tr>
<td width="20"><input id="2_2_11_11" type="checkbox" name="stemma2Code" value="0011" <c:if test="${form.mstemma0011 == '1' && form.mstemmaflg}">checked</c:if>></td>
<td width="120"><span class="text12">文・人文</span></td>
<td width="20"><input id="2_2_11_21" type="checkbox" name="stemma2Code" value="0021" <c:if test="${form.mstemma0021 == '1' && form.mstemmaflg}">checked</c:if>></td>
<td width="120"><span class="text12">社会・国際</span></td>
<td width="20"><input id="2_2_11_22" type="checkbox" name="stemma2Code" value="0022" <c:if test="${form.mstemma0022 == '1' && form.mstemmaflg}">checked</c:if>></td>
<td width="120"><span class="text12">法・政治</span></td>
</tr>
<tr>
<td width="20"><input id="2_2_11_23" type="checkbox" name="stemma2Code" value="0023" <c:if test="${form.mstemma0023 == '1' && form.mstemmaflg}">checked</c:if>></td>
<td width="120"><span class="text12">経済・経営・商</span></td>
<td width="20"><input id="2_2_01_31" type="checkbox" name="stemma2Code" value="0031" <c:if test="${form.mstemma0031 == '1' && form.mstemmaflg}">checked</c:if>></td>
<td width="120"><span class="text12">教育(教員養成)*国公立大のみ</span></td>
<td width="20"><input id="2_2_01_32" type="checkbox" name="stemma2Code" value="0032" <c:if test="${form.mstemma0032 == '1' && form.mstemmaflg}">checked</c:if>></td>
<td width="120"><span class="text12">教育(総合科学)*国公立大のみ</span></td>
</tr>
<tr>
<td width="20"><input id="2_2_10_41" type="checkbox" name="stemma2Code" value="0041" <c:if test="${form.mstemma0041 == '1' && form.mstemmaflg}">checked</c:if>></td>
<td width="120"><span class="text12">理</span></td>
<td width="20"><input id="2_2_10_42" type="checkbox" name="stemma2Code" value="0042" <c:if test="${form.mstemma0042 == '1' && form.mstemmaflg}">checked</c:if>></td>
<td width="120"><span class="text12">工</span></td>
<td width="20"><input id="2_2_10_43" type="checkbox" name="stemma2Code" value="0043" <c:if test="${form.mstemma0043 == '1' && form.mstemmaflg}">checked</c:if>></td>
<td width="120"><span class="text12">農</span></td>
</tr>
<tr>
<td width="20"><input id="2_2_10_51" type="checkbox" name="stemma2Code" value="0051" <c:if test="${form.mstemma0051 == '1' && form.mstemmaflg}">checked</c:if>></td>
<td width="120"><span class="text12">医・歯・薬・保健</span></td>
<td width="20"><input id="2_2_01_61" type="checkbox" name="stemma2Code" value="0061" <c:if test="${form.mstemma0061 == '1' && form.mstemmaflg}">checked</c:if>></td>
<td width="120"><span class="text12">生活科学</span></td>
<td width="20"><input id="2_2_01_62" type="checkbox" name="stemma2Code" value="0062" <c:if test="${form.mstemma0062 == '1' && form.mstemmaflg}">checked</c:if>></td>
<td width="120"><span class="text12">芸術・スポーツ科学</span></td>
</tr>
<tr>
<td width="20"><input id="2_2_01_71" type="checkbox" name="stemma2Code" value="0071" <c:if test="${form.mstemma0071 == '1' && form.mstemmaflg}">checked</c:if>></td>
<td width="120"><span class="text12">総合・環境・人間・情報</span></td>
<td width="20"></td>
<td width="120"></td>
<td width="20"></td>
<td width="120"></td>
</tr>
</table>
<!--/中系統リスト-->
<!--小系統リスト(文系)-->
<table border="0" cellpadding="2" cellspacing="0" id="stemma11">
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_11_11A" type="checkbox" name="stemma2Code" value="0011" <c:if test="${form.mstemma0011 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">文・人文</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--文・人文-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_11_11A_01" type="checkbox" name="stemma11Code" value="01" <c:if test="${form.sstemma01 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">文学　<a id="a_11_01" href="javascript:expand('11_01')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg01 && form.stemmaAflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg01 && form.stemmaAflg}">
    <tr id="11_01" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="11_01" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_11_11A_01_0100" type="checkbox" name="stemmaCode" value="0100" <c:if test="${form.region0100 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">日本文</span></td>
    <td width="24"><input id="2_2_11_11A_01_0200" type="checkbox" name="stemmaCode" value="0200" <c:if test="${form.region0200 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">外国文</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
<tr>
<td width="20"><input id="2_2_11_11A_02" type="checkbox" name="stemmaCode" value="0300" <c:if test="${form.sstemma02 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
<td><span class="text12">外国語</span></td>
</tr>
<tr>
<td width="20"><input id="2_2_11_11A_03" type="checkbox" name="stemma11Code" value="03" <c:if test="${form.sstemma03 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">哲・史・教・心　<a id="a_11_03" href="javascript:expand('11_03')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg03 && form.stemmaAflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->
</a></span></td>

</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg03 && form.stemmaAflg}">
    <tr id="11_03" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="11_03" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_11_11A_03_0400" type="checkbox" name="stemmaCode" value="0400" <c:if test="${form.region0400 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">哲・倫理・宗教</span></td>
    <td width="24"><input id="2_2_11_11A_03_0500" type="checkbox" name="stemmaCode" value="0500" <c:if test="${form.region0500 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">史・地理</span></td>
    <td width="24"><input id="2_2_11_11A_03_0600" type="checkbox" name="stemmaCode" value="0600" <c:if test="${form.region0600 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">教育</span></td>
    <td width="24"><input id="2_2_11_11A_03_0700" type="checkbox" name="stemmaCode" value="0700" <c:if test="${form.region0700 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">心理</span></td>
    <td width="24"><input id="2_2_11_11A_03_0800" type="checkbox" name="stemmaCode" value="0800" <c:if test="${form.region0800 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">地域・国際</span></td>
    <td width="24"><input id="2_2_11_11A_03_0900" type="checkbox" name="stemmaCode" value="0900" <c:if test="${form.region0900 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">文化・教養</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--/文・人文-->
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_11_21A" type="checkbox" name="stemma2Code" value="0021" <c:if test="${form.mstemma0021 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">社会・国際</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--社会・国際-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_11_21A_03" type="checkbox" name="stemma11Code" value="04" <c:if test="${form.sstemma04 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">社会・社会福祉　<a id="a_11_04" href="javascript:expand('11_04')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg04 && form.stemmaAflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg04 && form.stemmaAflg}">
    <tr id="11_04" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="11_04" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_11_21A_03_1000" type="checkbox" name="stemmaCode" value="1000" <c:if test="${form.region1000 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">社会</span></td>
    <td width="24"><input id="2_2_11_21A_03_1000" type="checkbox" name="stemmaCode" value="1100" <c:if test="${form.region1100 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">社会福祉</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
<tr>
<td width="20"><input id="2_2_11_21A_05" type="checkbox" name="stemma11Code" value="05" <c:if test="${form.sstemma05 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">国際　<a id="a_11_05" href="javascript:expand('11_05')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg05 && form.stemmaAflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg05 && form.stemmaAflg}">
    <tr id="11_05" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="11_05" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_11_21A_05_1200" type="checkbox" name="stemmaCode" value="1200" <c:if test="${form.region1200 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">国際法</span></td>
    <td width="24"><input id="2_2_11_21A_05_1300" type="checkbox" name="stemmaCode" value="1300" <c:if test="${form.region1300 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">国際経済</span></td>
    <td width="24"><input id="2_2_11_21A_05_1400" type="checkbox" name="stemmaCode" value="1400" <c:if test="${form.region1400 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">国際関係</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--/社会・国際-->
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_11_22A" type="checkbox" name="stemma2Code" value="0022" <c:if test="${form.mstemma0022 == '1' && form.stemmaAflg == true}">checked</c:if> onclick="checkChild(this);checkParent(this)"></td>
<td><span class="text12">法・政治</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--法・政治-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_11_22A_06" type="checkbox" name="stemma11Code" value="06" <c:if test="${form.sstemma06 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this);uncheckSstema(this);checkParent(this)"></td>
<td><span class="text12">法・政治　<a id="a_11_06" href="javascript:expand('11_06')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg06 && form.stemmaAflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg06 && form.stemmaAflg}">
    <tr id="11_06" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="11_06" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_11_22A_06_1500" type="checkbox" name="stemmaCode" value="1500" <c:if test="${form.region1500 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">法</span></td>
    <td width="24"><input id="2_2_11_22A_06_1600" type="checkbox" name="stemmaCode" value="1600" <c:if test="${form.region1600 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">政治</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--法・政治-->
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_11_23A" type="checkbox" name="stemma2Code" value="0023" <c:if test="${form.mstemma0023 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">経済・経営・商</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--法・政治-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_11_23A_07" type="checkbox" name="stemmaCode" value="1700" <c:if test="${form.sstemma07 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">経済</span></td>
</tr>
<tr>
<td width="20"><input id="2_2_11_23A_08" type="checkbox" name="stemma11Code" value="08" <c:if test="${form.sstemma08 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">経営・商・会計　<a id="a_11_08" href="javascript:expand('11_08')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg08 && form.stemmaAflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg08 && form.stemmaAflg}">
    <tr id="11_08" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="11_08" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_11_23A_08_1800" type="checkbox" name="stemmaCode" value="1800" <c:if test="${form.region1800 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">経営</span></td>
    <td width="24"><input id="2_2_11_23A_08_1900" type="checkbox" name="stemmaCode" value="1900" <c:if test="${form.region1900 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">経営情報</span></td>
    <td width="24"><input id="2_2_11_23A_08_2000" type="checkbox" name="stemmaCode" value="2000" <c:if test="${form.region2000 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">商・会計・他</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--法・政治-->
</td>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_31A" type="checkbox" name="stemma2Code" value="0031" <c:if test="${form.mstemma0031 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">教育(教員養成)*国公立大のみ</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--法・政治-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_31A_09" type="checkbox" name="stemmaCode" value="2100" <c:if test="${form.sstemma09 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">教員 - 教科</span></td>
</tr>
<tr>
<td width="20"><input id="2_2_01_31A_10" type="checkbox" name="stemmaCode" value="2200" <c:if test="${form.sstemma10 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">教員 - 実技</span></td>
</tr>
<tr>
<td width="20"><input id="2_2_01_31A_11" type="checkbox" name="stemma11Code" value="11" <c:if test="${form.sstemma11 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">教員 - 他　<a id="a_11_11" href="javascript:expand('11_11')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg11 && form.stemmaAflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg11 && form.stemmaAflg}">
    <tr id="11_11" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="11_11" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_01_31A_11_2300" type="checkbox" name="stemmaCode" value="2300" <c:if test="${form.region2300 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">幼稚園</span></td>
    <td width="24"><input id="2_2_01_31A_11_2400" type="checkbox" name="stemmaCode" value="2400" <c:if test="${form.region2400 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">養護</span></td>
    <td width="24"><input id="2_2_01_31A_11_2500" type="checkbox" name="stemmaCode" value="2500" <c:if test="${form.region2500 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">特別支援学校</span></td>
    <td width="24"><input id="2_2_01_31A_11_2600" type="checkbox" name="stemmaCode" value="2600" <c:if test="${form.region2600 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">その他教員養成</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--法・政治-->
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_32A" type="checkbox" name="stemma2Code" value="0032" <c:if test="${form.mstemma0032 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">教育(総合科学)*国公立大のみ</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--法・政治-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_32A_12" type="checkbox" name="stemma11Code" value="12" <c:if test="${form.sstemma12 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this);uncheckSstema(this);checkParent(this)"></td>
<td><span class="text12">教育(総合科学)*国公立大のみ　<a id="a_11_12" href="javascript:expand('11_12')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg12 && form.stemmaAflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg12 && form.stemmaAflg}">
    <tr id="11_12" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="11_12" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_01_32A_12_2700" type="checkbox" name="stemmaCode" value="2700" <c:if test="${form.region2700 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総課 - スポーツ</span></td>
    <td width="24"><input id="2_2_01_32A_12_2800" type="checkbox" name="stemmaCode" value="2800" <c:if test="${form.region2800 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総課 - 芸術・デザイン</span></td>
    <td width="24"><input id="2_2_01_32A_12_2900" type="checkbox" name="stemmaCode" value="2900" <c:if test="${form.region2900 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総課 - 情報</span></td>
    <td width="24"><input id="2_2_01_32A_12_3000" type="checkbox" name="stemmaCode" value="3000" <c:if test="${form.region3000 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総課 - 国際・言語・文化</span></td>
    <td width="24"><input id="2_2_01_32A_12_3100" type="checkbox" name="stemmaCode" value="3100" <c:if test="${form.region3100 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総課 - 心理・臨床</span></td>
    <td width="24"><input id="2_2_01_32A_12_3200" type="checkbox" name="stemmaCode" value="3200" <c:if test="${form.region3200 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総課 - 地域・社会・生活</span></td>
    <td width="24"><input id="2_2_01_32A_12_3300" type="checkbox" name="stemmaCode" value="3300" <c:if test="${form.region3300== '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総課 - 自然・環境・他</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--法・政治-->
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_61A" type="checkbox" name="stemma2Code" value="0061" <c:if test="${form.mstemma0061 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">生活科学</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--法・政治-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_61A_22" type="checkbox" name="stemma11Code" value="22" <c:if test="${form.sstemma22 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">生活科学　<a id="a_11_22" href="javascript:expand('11_22')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg22 && form.stemmaAflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg22 && form.stemmaAflg}">
    <tr id="11_22" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="11_22" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_01_61A_22_6500" type="checkbox" name="stemmaCode" value="6500" <c:if test="${form.region6500 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">食物・栄養</span></td>
    <td width="24"><input id="2_2_01_61A_22_6600" type="checkbox" name="stemmaCode" value="6600" <c:if test="${form.region6600 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">被服</span></td>
    <td width="24"><input id="2_2_01_61A_22_6700" type="checkbox" name="stemmaCode" value="6700" <c:if test="${form.region6700 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">児童</span></td>
    <td width="24"><input id="2_2_01_61A_22_6800" type="checkbox" name="stemmaCode" value="6800" <c:if test="${form.region6800 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">住居</span></td>
    <td width="24"><input id="2_2_01_61A_22_6900" type="checkbox" name="stemmaCode" value="6900" <c:if test="${form.region6900 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">生活科学</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_62A" type="checkbox" name="stemma2Code" value="0062" <c:if test="${form.mstemma0062 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">芸術・スポーツ科学</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--法・政治-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_62A_24" type="checkbox" name="stemma11Code" value="24" <c:if test="${form.sstemma24 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">芸術・スポーツ科学　<a id="a_11_24" href="javascript:expand('11_24')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg24 && form.stemmaAflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg24 && form.stemmaAflg}">
    <tr id="11_24" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="11_24" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_01_62A_24_7400" type="checkbox" name="stemmaCode" value="7400" <c:if test="${form.region7400 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">美術・デザイン</span></td>
    <td width="24"><input id="2_2_01_62A_24_7500" type="checkbox" name="stemmaCode" value="7500" <c:if test="${form.region7500 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">音楽</span></td>
    <td width="24"><input id="2_2_01_62A_24_7600" type="checkbox" name="stemmaCode" value="7600" <c:if test="${form.region7600 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">その他芸術</span></td>
    <td width="24"><input id="2_2_01_62A_24_7700" type="checkbox" name="stemmaCode" value="7700" <c:if test="${form.region7700 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">芸術理論</span></td>
    <td width="24"><input id="2_2_01_62A_24_7800" type="checkbox" name="stemmaCode" value="7800" <c:if test="${form.region7800 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">スポーツ・健康</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--法・政治-->
<!--法・政治-->
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_71A" type="checkbox" name="stemma2Code" value="0071" <c:if test="${form.mstemma0071 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">総合・環境・人間・情報</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--法・政治-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_71A_23" type="checkbox" name="stemma11Code" value="23" <c:if test="${form.sstemma23 == '1' && form.stemmaAflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">総・環・人・情　<a id="a_11_23" href="javascript:expand('11_23')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg23 && form.stemmaAflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg23 && form.stemmaAflg}">
    <tr id="11_23" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="11_23" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_01_71A_23_7000" type="checkbox" name="stemmaCode" value="7000" <c:if test="${form.region7000 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総合</span></td>
    <td width="24"><input id="2_2_01_71A_23_7100" type="checkbox" name="stemmaCode" value="7100" <c:if test="${form.region7100 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">環境</span></td>
    <td width="24"><input id="2_2_01_71A_23_7200" type="checkbox" name="stemmaCode" value="7200" <c:if test="${form.region7200 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">人間</span></td>
    <td width="24"><input id="2_2_01_71A_23_7300" type="checkbox" name="stemmaCode" value="7300" <c:if test="${form.region7300 == '1' && form.stemmaAflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">情報</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--法・政治-->
</td>
</tr>
</table>
<!--/小系統リスト(理系)-->
<!--小系統リスト(理系)-->
<table border="0" cellpadding="2" cellspacing="0" id="stemma10">
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_10_41S" type="checkbox" name="stemma2Code" value="0041" <c:if test="${form.mstemma0041 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">理</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--文・人文-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_10_41S_13" type="checkbox" name="stemma10Code" value="13" <c:if test="${form.sstemma13 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">理　<a id="a_10_13" href="javascript:expand('10_13')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg13 && form.stemmaSflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg13 && form.stemmaSflg}">
    <tr id="10_13" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="10_13" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_10_41S_13_3400" type="checkbox" name="stemmaCode" value="3400" <c:if test="${form.region3400 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">数学・数理情報</span></td>
    <td width="24"><input id="2_2_10_41S_13_3500" type="checkbox" name="stemmaCode" value="3500" <c:if test="${form.region3500 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">物理</span></td>
    <td width="24"><input id="2_2_10_41S_13_3600" type="checkbox" name="stemmaCode" value="3600" <c:if test="${form.region3600 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">化学</span></td>
    <td width="24"><input id="2_2_10_41S_13_3700" type="checkbox" name="stemmaCode" value="3700" <c:if test="${form.region3700 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">生物</span></td>
    <td width="24"><input id="2_2_10_41S_13_3800" type="checkbox" name="stemmaCode" value="3800" <c:if test="${form.region3800 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">地学・他</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--/文・人文-->
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_10_42S" type="checkbox" name="stemma2Code" value="0042" <c:if test="${form.mstemma0042 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">工</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--社会・国際-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_10_42S_14" type="checkbox" name="stemma10Code" value="14" <c:if test="${form.sstemma14 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">機械・航空　<a id="a_10_14" href="javascript:expand('10_14')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg14 && form.stemmaSflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg14 && form.stemmaSflg}">
    <tr id="10_14" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="10_14" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_10_42S_14_3900" type="checkbox" name="stemmaCode" value="3900" <c:if test="${form.region3900 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">機械</span></td>
    <td width="24"><input id="2_2_10_42S_14_4000" type="checkbox" name="stemmaCode" value="4000" <c:if test="${form.region4000 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">航空・宇宙</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
<tr>
<td width="20"><input id="2_2_10_42S_15" type="checkbox" name="stemma10Code" value="15" <c:if test="${form.sstemma15 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">電気電子・情報　<a id="a_10_15" href="javascript:expand('10_15')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg15 && form.stemmaSflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg15 && form.stemmaSflg}">
    <tr id="10_15" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="10_15" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_10_42S_15_4100" type="checkbox" name="stemmaCode" value="4100" <c:if test="${form.region4100 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">電気・電子</span></td>
    <td width="24"><input id="2_2_10_42S_15_4200" type="checkbox" name="stemmaCode" value="4200" <c:if test="${form.region4200 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">通信・情報</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
<tr>
<td width="20"><input id="2_2_10_42S_16" type="checkbox" name="stemma10Code" value="16" <c:if test="${form.sstemma16 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">土木・建築　<a id="a_10_16" href="javascript:expand('10_16')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg16 && form.stemmaSflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg16 && form.stemmaSflg}">
    <tr id="10_16" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="10_16" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_10_42S_16_4300" type="checkbox" name="stemmaCode" value="4300" <c:if test="${form.region4300 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">建築</span></td>
    <td width="24"><input id="2_2_10_42S_16_4400" type="checkbox" name="stemmaCode" value="4400" <c:if test="${form.region4400 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">土木・環境</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
<tr>
<td width="20"><input id="2_2_10_42S_17" type="checkbox" name="stemma10Code" value="17" <c:if test="${form.sstemma17 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">応化・応物・資　<a id="a_10_17" href="javascript:expand('10_17')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg17 && form.stemmaSflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg17 && form.stemmaSflg}">
    <tr id="10_17" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="10_17" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_10_42S_17_4500" type="checkbox" name="stemmaCode" value="4500" <c:if test="${form.region4500 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">応用化学</span></td>
    <td width="24"><input id="2_2_10_42S_17_4600" type="checkbox" name="stemmaCode" value="4600" <c:if test="${form.region4600 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">材料・物質工</span></td>
    <td width="24"><input id="2_2_10_42S_17_4700" type="checkbox" name="stemmaCode" value="4700" <c:if test="${form.region4700 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">応用物理</span></td>
    <td width="24"><input id="2_2_10_42S_17_4800" type="checkbox" name="stemmaCode" value="4800" <c:if test="${form.region4800 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">資源・エネルギー</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
<tr>
<td width="20"><input id="2_2_10_42S_18" type="checkbox" name="stemma10Code" value="18" <c:if test="${form.sstemma18 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">生物・経営工他　<a id="a_10_18" href="javascript:expand('10_18')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg18 && form.stemmaSflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg18 && form.stemmaSflg}">
    <tr id="10_18" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="10_18" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_10_42S_18_4900" type="checkbox" name="stemmaCode" value="4900" <c:if test="${form.region4900 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">生物工</span></td>
    <td width="24"><input id="2_2_10_42S_18_5000" type="checkbox" name="stemmaCode" value="5000" <c:if test="${form.region5000 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">経営工・管理工</span></td>
    <td width="24"><input id="2_2_10_42S_18_5100" type="checkbox" name="stemmaCode" value="5100" <c:if test="${form.region5100 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">船舶・海洋</span></td>
    <td width="24"><input id="2_2_10_42S_18_5200" type="checkbox" name="stemmaCode" value="5200" <c:if test="${form.region5200 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">デザイン工・他</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--/社会・国際-->
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_10_43S" type="checkbox" name="stemma2Code" value="0043" <c:if test="${form.mstemma0043 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">農</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--法・政治-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_10_43S_19" type="checkbox" name="stemma10Code" value="19" <c:if test="${form.sstemma19 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">農　<a id="a_10_19" href="javascript:expand('10_19')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg19 && form.stemmaSflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg19 && form.stemmaSflg}">
    <tr id="10_19" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="10_19" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_10_43S_19_5300" type="checkbox" name="stemmaCode" value="5300" <c:if test="${form.region5300 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">生物生産・応用生命</span></td>
    <td width="24"><input id="2_2_10_43S_19_5400" type="checkbox" name="stemmaCode" value="5400" <c:if test="${form.region5400 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">環境科学</span></td>
    <td width="24"><input id="2_2_10_43S_19_5500" type="checkbox" name="stemmaCode" value="5500" <c:if test="${form.region5500 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">経済システム</span></td>
    <td width="24"><input id="2_2_10_43S_19_5600" type="checkbox" name="stemmaCode" value="5600" <c:if test="${form.region5600 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">獣医</span></td>
    <td width="24"><input id="2_2_10_43S_19_5700" type="checkbox" name="stemmaCode" value="5700" <c:if test="${form.region5700 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">酪農・畜産</span></td>
    <td width="24"><input id="2_2_10_43S_19_5800" type="checkbox" name="stemmaCode" value="5800" <c:if test="${form.region5800 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">水産</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--法・政治-->
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_10_51S" type="checkbox" name="stemma2Code" value="0051" <c:if test="${form.mstemma0051 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">医・歯・薬・保健</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--法・政治-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_10_51S_20" type="checkbox" name="stemma10Code" value="20" <c:if test="${form.sstemma20 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">医・歯　<a id="a_10_20" href="javascript:expand('10_20')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg20 && form.stemmaSflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg20 && form.stemmaSflg}">
    <tr id="10_20" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="10_20" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_10_51S_20_5900" type="checkbox" name="stemmaCode" value="5900" <c:if test="${form.region5900 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">医</span></td>
    <td width="24"><input id="2_2_10_51S_20_6000" type="checkbox" name="stemmaCode" value="6000" <c:if test="${form.region6000 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">歯</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
<tr>
<td width="20"><input id="2_2_10_51S_21" type="checkbox" name="stemma10Code" value="21" <c:if test="${form.sstemma21 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">薬・看護・保健　<a id="a_10_21" href="javascript:expand('10_21')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg21 && form.stemmaSflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg21 && form.stemmaSflg}">
    <tr id="10_21" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="10_21" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_10_51S_21_6100" type="checkbox" name="stemmaCode" value="6100" <c:if test="${form.region6100 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">薬</span></td>
    <td width="24"><input id="2_2_10_51S_21_6200" type="checkbox" name="stemmaCode" value="6200" <c:if test="${form.region6200 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">看護</span></td>
    <td width="24"><input id="2_2_10_51S_21_6300" type="checkbox" name="stemmaCode" value="6300" <c:if test="${form.region6300 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">医療技術</span></td>
    <td width="24"><input id="2_2_10_51S_21_6400" type="checkbox" name="stemmaCode" value="6400" <c:if test="${form.region6400 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">保健・福祉・他</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--法・政治-->
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_31S" type="checkbox" name="stemma2Code" value="0031" <c:if test="${form.mstemma0031 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">教育(教員養成)*国公立大のみ</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--法・政治-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_31S_09" type="checkbox" name="stemmaCode" value="2100" <c:if test="${form.sstemma09 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">教員 - 教科</span></td>
</tr>
<tr>
<td width="20"><input id="2_2_01_31S_10" type="checkbox" name="stemmaCode" value="2200" <c:if test="${form.sstemma10 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">教員 - 実技</span></td>
</tr>
<tr>
<td width="20"><input id="2_2_01_31S_11" type="checkbox" name="stemma10Code" value="11" <c:if test="${form.sstemma11 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)" ></td>
<td><span class="text12">教員 - 他　<a id="a_10_11" href="javascript:expand('10_11')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg11 && form.stemmaSflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg11 && form.stemmaSflg}">
    <tr id="10_11" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="10_11" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_01_31S_11_2300" type="checkbox" name="stemmaCode" value="2300" <c:if test="${form.region2300 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">幼稚園</span></td>
    <td width="24"><input id="2_2_01_31S_11_2400" type="checkbox" name="stemmaCode" value="2400" <c:if test="${form.region2400 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">養護</span></td>
    <td width="24"><input id="2_2_01_31S_11_2500" type="checkbox" name="stemmaCode" value="2500" <c:if test="${form.region2500 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">特別支援学校</span></td>
    <td width="24"><input id="2_2_01_31S_11_2600" type="checkbox" name="stemmaCode" value="2600" <c:if test="${form.region2600 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">その他教員養成</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--法・政治-->
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_32S" type="checkbox" name="stemma2Code" value="0032" <c:if test="${form.mstemma0032 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">教育(総合科学)*国公立大のみ</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--法・政治-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_32S_12" type="checkbox" name="stemma10Code" value="12" <c:if test="${form.sstemma12 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">教育(総合科学)*国公立大のみ　<a id="a_10_12" href="javascript:expand('10_12')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg12 && form.stemmaSflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg12 && form.stemmaSflg}">
    <tr id="10_12" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="10_12" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_01_32S_12_2700" type="checkbox" name="stemmaCode" value="2700" <c:if test="${form.region2700 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総課 - スポーツ</span></td>
    <td width="24"><input id="2_2_01_32S_12_2800" type="checkbox" name="stemmaCode" value="2800" <c:if test="${form.region2800 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総課 - 芸術・デザイン</span></td>
    <td width="24"><input id="2_2_01_32S_12_2900" type="checkbox" name="stemmaCode" value="2900" <c:if test="${form.region2900 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総課 - 情報</span></td>
    <td width="24"><input id="2_2_01_32S_12_3000" type="checkbox" name="stemmaCode" value="3000" <c:if test="${form.region3000 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総課 - 国際・言語・文化</span></td>
    <td width="24"><input id="2_2_01_32S_12_3100" type="checkbox" name="stemmaCode" value="3100" <c:if test="${form.region3100 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総課 - 心理・臨床</span></td>
    <td width="24"><input id="2_2_01_32S_12_3200" type="checkbox" name="stemmaCode" value="3200" <c:if test="${form.region3200 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総課 - 地域・社会・生活</span></td>
    <td width="24"><input id="2_2_01_32S_12_3300" type="checkbox" name="stemmaCode" value="3300" <c:if test="${form.region3300 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総課 - 自然・環境・他</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--法・政治-->
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_61S" type="checkbox" name="stemma2Code" value="0061" <c:if test="${form.mstemma0061 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">生活科学</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--法・政治-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_61S_22" type="checkbox" name="stemma10Code" value="22" <c:if test="${form.sstemma22 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">生活科学　<a id="a_10_22" href="javascript:expand('10_22')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg22 && form.stemmaSflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg22 && form.stemmaSflg}">
    <tr id="10_22" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="10_22" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_01_61S_22_6500" type="checkbox" name="stemmaCode" value="6500" <c:if test="${form.region6500 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">食物・栄養</span></td>
    <td width="24"><input id="2_2_01_61S_22_6600" type="checkbox" name="stemmaCode" value="6600" <c:if test="${form.region6600 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">被服</span></td>
    <td width="24"><input id="2_2_01_61S_22_6700" type="checkbox" name="stemmaCode" value="6700" <c:if test="${form.region6700 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">児童</span></td>
    <td width="24"><input id="2_2_01_61S_22_6800" type="checkbox" name="stemmaCode" value="6800" <c:if test="${form.region6800 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">住居</span></td>
    <td width="24"><input id="2_2_01_61S_22_6900" type="checkbox" name="stemmaCode" value="6900" <c:if test="${form.region6900 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">生活科学</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--法・政治-->
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_62S" type="checkbox" name="stemma2Code" value="0062" <c:if test="${form.mstemma0062 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this)" ></td>
<td><span class="text12">芸術・スポーツ科学</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--法・政治-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_62S_24" type="checkbox" name="stemma10Code" value="24" <c:if test="${form.sstemma24 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">芸術・スポーツ科学　<a id="a_10_24" href="javascript:expand('10_24')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg24 && form.stemmaSflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg24 && form.stemmaSflg}">
    <tr id="10_24" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="10_24" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_01_62S_24_7400" type="checkbox" name="stemmaCode" value="7400" <c:if test="${form.region7400 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">美術・デザイン</span></td>
    <td width="24"><input id="2_2_01_62S_24_7500" type="checkbox" name="stemmaCode" value="7500" <c:if test="${form.region7500 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">音楽</span></td>
    <td width="24"><input id="2_2_01_62S_24_7600" type="checkbox" name="stemmaCode" value="7600" <c:if test="${form.region7600 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">その他芸術</span></td>
    <td width="24"><input id="2_2_01_62S_24_7700" type="checkbox" name="stemmaCode" value="7700" <c:if test="${form.region7700 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">芸術理論</span></td>
    <td width="24"><input id="2_2_01_62S_24_7800" type="checkbox" name="stemmaCode" value="7800" <c:if test="${form.region7800 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">スポーツ・健康</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<!--法・政治-->
<!--法・政治-->
</td>
</tr>
<tr valign="top">
<td width="15%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_71S" type="checkbox" name="stemma2Code" value="0071" <c:if test="${form.mstemma0071 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this)"></td>
<td><span class="text12">総合・環境・人間・情報</span></td>
</tr>
</table>
</td>
<td width="65%" bgcolor="#F4E5D6">
<!--法・政治-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input id="2_2_01_71S_23" type="checkbox" name="stemma10Code" value="23" <c:if test="${form.sstemma23 == '1' && form.stemmaSflg}">checked</c:if> onclick="checkChild(this);uncheckMstema(this);checkParent(this)"></td>
<td><span class="text12">総・環・人・情　<a id="a_10_23" href="javascript:expand('10_23')">

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg23 && form.stemmaSflg}">
    <u>▽</u>
</c:when>
<c:otherwise>
    <u>▼</u>
</c:otherwise>
</c:choose>
<!--/分野表示-->

</a></span></td>
</tr>

<!--分野表示-->
<c:choose>
<c:when test="${form.regionflg23 && form.stemmaSflg}">
    <tr id="10_23" style="display:inline">
</c:when>
<c:otherwise>
    <tr id="10_23" style="display:none">
</c:otherwise>
</c:choose>
<!--/分野表示-->

    <td width="20">&nbsp;</td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="360">
    <tr>
    <td width="360" height="27" bgcolor="#FAF2EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="24"><input id="2_2_01_71S_23_7000" type="checkbox" name="stemmaCode" value="7000" <c:if test="${form.region7000 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">総合</span></td>
    <td width="24"><input id="2_2_01_71S_23_7100" type="checkbox" name="stemmaCode" value="7100" <c:if test="${form.region7100 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">環境</span></td>
    <td width="24"><input id="2_2_01_71S_23_7200" type="checkbox" name="stemmaCode" value="7200" <c:if test="${form.region7200 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">人間</span></td>
    <td width="24"><input id="2_2_01_71S_23_7300" type="checkbox" name="stemmaCode" value="7300" <c:if test="${form.region7300 == '1' && form.stemmaSflg}">checked</c:if> onclick="uncheckSstema(this);checkParent(this)"></td>
    <td width="50"><span class="text12">情報</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
</table>

</td>
</tr>
</table>
<!--/小系統リスト(理系)-->
</td>
</tr>
</table>
<!--系統の選択-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<b class="text14">&nbsp;3.科目の選択</b><br>

<!--科目の選択-->
<table border="0" cellpadding="5" cellspacing="2" width="634">
<tr valign="top">
<td width="20%" bgcolor="#E1E6EB">
<!--2次科目-->
<table border="0" cellpadding="0" cellspacing="4">
<tr>
<td><span class="text12">2次科目</span></td>
</tr>
<tr>
<td><div style="margin-top:5px;"><span class="text10-hh">※指定できるのは国公立2次試験・私大試験科目です。</span></div></td>
</tr>
</table>
<!--/2次科目-->
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" id = "ImposeDiv" name="descImposeDiv" value="1" onclick="ImposeDivCK()"></td>
<td><span class="text12">（センター利用大）2次・独自試験を課さない</span></td>
</tr>
</table>
<div style="margin-top:5px;">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td><span class="text12"><font style="color:#758A98">▼</font>以下の試験科目を</span></td>
<td width="20"><input type="radio" id = "subImposeDivA" name="subImposeDiv" value="1" onclick ="ImposeSubClear()"></td>
<td width="45"><span class="text12">課す</span></td>
<td width="20"><input type="radio" id = "subImposeDivB" name="subImposeDiv" value="2" onclick ="ImposeSubClear()"></td>
<td><span class="text12">課さない</span></td>
</tr>
</table>
</div>
<!--ボーダー-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="480">
<tr>
<td width="480" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="480" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/ボーダー-->
<div style="margin-top:3px;">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="imposeSub" value="english" onclick="ImposeSubCK(this)"></td>
<td width="90"><span class="text12">英語</span></td>
<td width="20"></td>
<td width="90"></td>
<td width="20"></td>
<td width="90"></td>
<td width="20"></td>
<td width="90"></td>
</tr>
<tr>
<td width="20"><input id="m_1" type="checkbox" name="imposeSub" value="math1" onclick="javascript: this.checked ? checkMath(1) : uncheckMath(1)"></td>
<td width="90"><span class="text12">数学I</span></td>
<td width="20"><input id="m_2" type="checkbox" name="imposeSub" value="math1A" onclick="javascript: this.checked ? checkMath(2) : uncheckMath(2)"></td>
<td width="90"><span class="text12">数学IA</span></td>
<td width="20"><input id="m_3" type="checkbox" name="imposeSub" value="math2A" onclick="javascript: this.checked ? checkMath(3) : uncheckMath(3)"></td>
<td width="90"><span class="text12">数学IIA</span></td>
<td width="20"><input id="m_4" type="checkbox" name="imposeSub" value="math2B" onclick="javascript: this.checked ? checkMath(4) : uncheckMath(4)"></td>
<td width="90"><span class="text12">数学IIB</span></td>
</tr>
<tr>
<td width="20"><input id="m_5" type="checkbox" name="imposeSub" value="math3B" onclick="javascript: this.checked ? checkMath(5) : uncheckMath(5)"></td>
<td width="90"><span class="text12">数学III</span></td>
<td width="20"></td>
<td width="90"></td>
<td width="20"></td>
<td width="90"></td>
<td width="20"></td>
<td width="90"></td>
</tr>
<tr>
<td width="20"><input id="j_1" type="checkbox" name="imposeSub" value="japaneseLit" onclick="javascript: this.checked ? checkJap(1) : uncheckJap(1)"></td>
<td width="90"><span class="text12">国語（現代文）</span></td>
<td width="20"><input id="j_2" type="checkbox" name="imposeSub" value="jClassics" onclick="javascript: this.checked ? checkJap(2) : uncheckJap(2)"></td>
<td width="90"><span class="text12">国語（現・古）</span></td>
<td width="20"><input id="j_3" type="checkbox" name="imposeSub" value="japanese" onclick="javascript: this.checked ? checkJap(3) : uncheckJap(3)"></td>
<td width="90"><span class="text12">国語（現・古・漢）</span></td>
<td width="20"></td>
<td width="90"></td>
</tr>
<tr>
<td width="20"><input type="checkbox" name="imposeSub" value="physics" onclick="ImposeSubCK(this)"></td>
<td width="200" colspan="3"><span class="text12">物理&nbsp;（※基礎のみ&nbsp;→<input type="checkbox" name="imposeSub" value="physicsBasic" id="physicsBasicCheckBox">）</span></td>
<td width="20"><input type="checkbox" name="imposeSub" value="chemistry" onclick="ImposeSubCK(this)"></td>
<td width="200" colspan="3"><span class="text12">化学&nbsp;（※基礎のみ&nbsp;→<input type="checkbox" name="imposeSub" value="chemistryBasic" id="chemistryBasicCheckBox">）</span></td>
</tr>
<tr>
<td width="20"><input type="checkbox" name="imposeSub" value="biology" onclick="ImposeSubCK(this)"></td>
<td width="200" colspan="3"><span class="text12">生物&nbsp;（※基礎のみ&nbsp;→<input type="checkbox" name="imposeSub" value="biologyBasic" id="biologyBasicCheckBox">）</span></td>
<td width="20"><input type="checkbox" name="imposeSub" value="earthScience" onclick="ImposeSubCK(this)"></td>
<td width="200" colspan="3"><span class="text12">地学&nbsp;（※基礎のみ&nbsp;→<input type="checkbox" name="imposeSub" value="earthScienceBasic" id="earthScienceBasicCheckBox">）</span></td>
</tr>
<tr>
<td width="20"><input type="checkbox" name="imposeSub" value="jHistory" onclick="ImposeSubCK(this)"></td>
<td width="90"><span class="text12">日本史</span></td>
<td width="20"><input type="checkbox" name="imposeSub" value="wHistory" onclick="ImposeSubCK(this)"></td>
<td width="90"><span class="text12">世界史</span></td>
<td width="20"><input type="checkbox" name="imposeSub" value="geography" onclick="ImposeSubCK(this)"></td>
<td width="90"><span class="text12">地理</span></td>
<td width="20"><input type="checkbox" name="imposeSub" value="ethic" onclick="ImposeSubCK(this)"></td>
<td width="90"><span class="text12">倫理</span></td>
</tr>
<tr>
<td width="20"><input type="checkbox" name="imposeSub" value="politics" onclick="ImposeSubCK(this)"></td>
<td width="90"><span class="text12">政治経済</span></td>
<td width="20"></td>
<td width="90"></td>
<td width="20"></td>
<td width="90"></td>
<td width="20"></td>
<td width="90"></td>
</tr>
<tr>
<td width="20"><input type="checkbox" name="imposeSub" value="essay" onclick="ImposeSubCK(this)"></td>
<td width="90"><span class="text12">小論文</span></td>
<td width="20"><input type="checkbox" name="imposeSub" value="comprehensive" onclick="ImposeSubCK(this)"></td>
<td width="90"><span class="text12">総合科目</span></td>
<td width="20"><input type="checkbox" name="imposeSub" value="interview" onclick="ImposeSubCK(this)"></td>
<td width="90"><span class="text12">面接</span></td>
<td width="20"><input type="checkbox" name="imposeSub" value="performance" onclick="ImposeSubCK(this)"></td>
<td width="90"><span class="text12">実技</span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>
<!--科目の選択-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<b class="text14">&nbsp;4.その他の条件の選択</b><br>

<!--その他の条件の選択-->
<table border="0" cellpadding="5" cellspacing="2" width="634">
<!--学校区分-->
<tr valign="top">
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="4">
<tr>
<td><span class="text12">学校区分</span></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<% if(iCommonMap.getTargetExamCode().equals(IPropsLoader.getInstance().getMessage("CODE_EXAM_CENTERRESEARCH"))){%>
<tr>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="1"></td>
<td width="130"><span class="text12">国公立大前期</span></td>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="2"></td>
<td width="130"><span class="text12">国公立大後期</span></td>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="3"></td>
<td width="130"><span class="text12">国公立大中期</span></td>
</tr>
<tr>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="5"></td>
<td width="130"><span class="text12">私立大センター利用</span></td>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="7"></td>
<td width="130"><span class="text12">国公立短大センター利用</span></td>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="9"></td>
<td width="130"><span class="text12">私立短大センター利用</span></td>
</tr>
<tr style="display:none">
<td width="20"><input type="checkbox" name="schoolDivProviso" value="4"></td>
<td width="130"><span class="text12">私立大一般</span></td>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="8"></td>
<td width="130"><span class="text12">私立短大一般</span></td>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="6"></td>
<td width="130"><span class="text12">国公立短大一般</span></td>
</tr>
<tr style="display:none">
<td width="20"><input type="checkbox" name="schoolDivProviso" value="10"></td>
<td width="130"><span class="text12">文部科学省所管外</span></td>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="11"></td>
<td width="130"><span class="text12">専門学校</span></td>
<td width="20"></td>
<td width="130"></td>
</tr>
<%}else{%>
<tr>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="1"></td>
<td width="130"><span class="text12">国公立大前期</span></td>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="2"></td>
<td width="130"><span class="text12">国公立大後期</span></td>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="3"></td>
<td width="130"><span class="text12">国公立大中期</span></td>
</tr>
<tr>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="4"></td>
<td width="130"><span class="text12">私立大一般</span></td>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="5"></td>
<td width="130"><span class="text12">私立大センター利用</span></td>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="6"></td>
<td width="130"><span class="text12">国公立短大一般</span></td>
</tr>
<tr>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="7"></td>
<td width="130"><span class="text12">国公立短大センター利用</span></td>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="8"></td>
<td width="130"><span class="text12">私立短大一般</span></td>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="9"></td>
<td width="130"><span class="text12">私立短大センター利用</span></td>
</tr>
<tr>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="10"></td>
<td width="130"><span class="text12">文部科学省所管外</span></td>
<td width="20"><input type="checkbox" name="schoolDivProviso" value="11"></td>
<td width="130"><span class="text12">専門学校</span></td>
<td width="20"></td>
<td width="130"></td>
</tr>
<%}%>
</table>
</td>
</tr>
<!--/学校区分-->
<!--入試日-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="4">
<tr>
<td><span class="text12">入試日</span></td>
</tr>
<tr>
<td><div style="margin-top:5px;"><span class="text10-hh">※「入試日が複数にわたる場合の１日でも該当する場合」、「１次試験合格者のみが受験できる２次試験」もヒットする場合があります。</span></div></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
    <div>
    <table border="0" cellpadding="2" cellspacing="0" width="480">
    <tr>
    <td><input type="checkbox" name="dateSearchDiv" value="1" onclick="javascript: this.checked ? enableMD() : disableMD()"><span class="text12">入試日条件を有効化する</span></td>
    </tr>
    </table>
    </div>
    <!--ボーダー-->
    <div align="center">
    <table border="0" cellpadding="0" cellspacing="0" width="480">
    <tr>
    <td width="480" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="480" height="1" border="0" alt=""><br></td>
    </tr>
    </table>
    </div>
    <!--/ボーダー-->
    <div>
    <table border="0" cellpadding="4" cellspacing="0">
    <tr>
    <td><select ID="MM" name="startMonth" onchange="ps.ChgMon('MM', 'DD')">
    </select></td>
    <td><select ID="DD" name="startDate" onchange="ps.ChgDay('DD')">
    </select></td>
    <td><span class="text12">から</span></td>
    <td><select ID="MM2" name="endMonth" onchange="ps2.ChgMon('MM2', 'DD2')">
    </select></td>
    <td><select ID="DD2" name="endDate" onchange="ps2.ChgDay('DD2')">
    </select></td>
    </tr>
    </table>
    </div>
    <div style="margin:5px;">
    <span class="text12">入試日による検索は通年で可能で、詳細画面に入試日が表示されますが、次年度入試への内容更新は、国公立大が毎年1月下旬（センター･リサーチデータ提供後）、私立大が毎年10月上旬（第２回全統記述模試データ提供後）となります。</span>
    </div>
</td>
</tr>
<!--/入試日-->
<!--評価範囲-->
<tr valign="top">
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="4">
<tr>
<td><span class="text12">評価範囲</span></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="24"><input type="radio" name="raingDiv" value="1" checked></td>
<td width="150"><span class="text12">センターまたは2次・私大</span></td>
<td width="24"><input type="radio" name="raingDiv" value="2"></td>
<td width="150"><span class="text12">総合（ドッキング）</span></td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" width="470">
<tr>

<td width="24"><input type="checkbox" name="raingProviso" value="1"></td>
<td width="70"><span class="text12">A判定</span></td>
<td width="24"><input type="checkbox" name="raingProviso" value="2"></td>
<td width="70"><span class="text12">B判定</span></td>
<td width="24"><input type="checkbox" name="raingProviso" value="3"></td>
<td width="70"><span class="text12">C判定</span></td>
<td width="24"><input type="checkbox" name="raingProviso" value="4"></td>
<td width="70"><span class="text12">D判定</span></td>
<td width="24"><input type="checkbox" name="raingProviso" value="5"></td>
<td width="70"><span class="text12">E判定</span></td>

</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<!--/評価範囲-->
<!--対象外-->
<tr valign="top">
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="4">
<tr>
<td><span class="text12">対象外</span></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" width="332">
<tr>
<td width="24"><input type="checkbox" name="outOfTergetProviso" value="1"></td>
<td width="70"><span class="text12">女子大</span></td>
<td width="24"><input type="checkbox" name="outOfTergetProviso" value="2"></td>
<td width="70"><span class="text12">二部</span></td>
<td width="24"><input type="checkbox" name="outOfTergetProviso" value="3"></td>
<td width="120"><span class="text12">夜間主（フレックス）</span></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<!--/対象外-->
</table>
<!--その他の条件の選択-->

<!--矢印-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="10">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
</div>
<!--/矢印-->

<!--ボタン-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><input type="button" onclick="submitProviso()" value="&nbsp;条件保存&nbsp;" class="text12" style="width:189px;"></td>
</tr>
</table>
</div>
<!--/ボタン-->


<!--矢印-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="10">
<tr>
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
</div>
<!--/矢印-->


<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="634">
<tr valign="top">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="632">

<table border="0" cellpadding="0" cellspacing="0" width="630">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="630">
<tr valign="top">
<td width="628" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td>

<!-- モードで判別-->
<c:choose>
<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
<select name="judgeStudent" class="text12" style="width:162;" onchange = "document.forms[0].judgeStudentSub.selectedIndex=this.selectedIndex;JudgeStudentCK();">
    <option value="1" <c:if test="${form.judgeStudent == '1'}">selected</c:if> selected> 選択中の生徒すべて</option>
    <option value="2" <c:if test="${form.judgeStudent == '2'}">selected</c:if>> 表示中の生徒一人</option>
    </td>
    <td><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
    <td>
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td colspan="4"><span class="text10">&nbsp;条件保存されていない生徒の処理</span></td>
    </tr>
    <tr>
    <td><input type="radio" id = "indStudentUsageA" name="indStudentUsage" value="1" onclick="document.forms[0].indStudentUsageSub[0].checked=true;" <c:if test="${form.indStudentUsage == '1'}">checked</c:if>></td>
    <td width="80"><span class="text12">判定しない</span></td>
    <td><input type="radio" id = "indStudentUsageB" name="indStudentUsage" value="2" onclick="document.forms[0].indStudentUsageSub[1].checked=true;" <c:if test="${form.indStudentUsage == '2'}">checked</c:if>></td>
    <td><span class="text12">オマカセ判定</span></td>
    </tr>
    </select>
    </c:when>
    <c:otherwise>
        <input type="hidden" name="judgeStudent" value="2">
        </td>
        <td><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
        <td>
        <table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td colspan="4"></td>
        </tr>
        <tr>
        <td></td>
        <td width="80"></td>
        <td><input type="hidden" name="indStudentUsage" value="1"></td>
        <td></td>
        </tr>
</c:otherwise>
</c:choose>
<!--/ モードで判別-->

</table>
</td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="614">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="614">
<tr valign="top">
<td width="612" bgcolor="#FFFFFF" align="center"><a href="javascript:submitJudge()" onclick="return isJudgable();"><img src="./shared_lib/img/btn/hantei2.gif" width="96" height="36" border="0" alt="判定"></a><br></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/●●●右側●●●-->
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/志望大学判定-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>
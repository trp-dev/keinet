<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setBundle basename="jp.co.fj.keinavi.util.individual.iprops" var="bundle" />
<c:set var="SEL_NUM_BY_UNIV"><fmt:message key="SEL_NUM_BY_UNIV" bundle="${bundle}" /></c:set>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<jsp:useBean id="form" scope="request" class="jp.co.fj.keinavi.forms.individual.I203Form" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
	/**
	 * 共通項目
	 */
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>
	// ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	/**
	 * サブミッション
	 */
	function submitForm(forward) {
		<%--スクロール位置 0426 K.Kondo--%>
		if(forward=='<c:out value="${param.forward}" />') {
			document.forms[0].scrollX.value = document.body.scrollLeft;
			document.forms[0].scrollY.value = document.body.scrollTop;
		} else {
			document.forms[0].scrollX.value = 0;
			document.forms[0].scrollY.value = 0;
		}
		document.getElementById("MainLayer").style.visibility = "hidden";
		document.getElementById("MainLayer").style.top = -2000;
		document.getElementById("LoadingLayer").style.visibility = "visible";
		document.forms[0].target = "_self";
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 戻るボタン
	 */
	function submitBack() {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "i201";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 頭文字サブミッション
	 */
	function submitChar(char){
		document.forms[0].button.value = 'char';
		document.forms[0].charValue.value = char;
		submitForm('i203');
	}
	/**
	 * 大学名・大学コードサブミッション
	 */
	function submitSearch(){
		if(check()){
			document.forms[0].button.value = 'search';
			submitForm('i203');
		}
	}
	/**
	 * 判定サブミッション
	 */
	function submitJudge(){
		submitForm('i204');
	}
	/**
	 * WINDOW CLOSE
	 */
	function submitCancel(){
		window.close();
	}
	/**
	 * 入力チェック
	 */
	var pushed = false;
	function check() {
		// 入力チェック用オブジェクト
		var val = new Validator();
		// 大学名はひらがな
		if (!val.isHiragana(document.forms[0].searchStr.value)) {
			alert("大学名はひらがなで入力してください。");
			return false;
		}else{
			return true;
		}

		// ボタンは押されたことになる
		pushed = true;
	}
	/**
	 * 判定一覧に一件以上あれば
	 * 判定対象一覧用ヘッダーを表示する
	 */
	function showHeader3(){
		var tbl = document.getElementById('list');
		if((tbl.rows.length -1) == 0 ){
			document.getElementById('header3').style.display = "none";
		}else{
			document.getElementById('header3').style.display = "inline";
		}
	}
	/**
	 * 生徒切り替え
	 */
	<%@ include file="/jsp/individual/include/student_rotator.jsp" %>
	/**
	 * 大学コンボ
	 */
	<%@ include file="/jsp/individual/include/univ_combox_203.jsp" %>
	/**
	 * 大学削除
	 */
	function deleteList(ID){
		// 指定の行を削除
		if (confirm("判定対象一覧から削除しますか？")) {
			var tbl = document.getElementById('list')
			for(var i=0; i<tbl.rows.length; i++){
				if(tbl.rows[i].id == ID){
					tbl.deleteRow(i);
				}
			}
			refreshId();
		}
		showHeader3();
	}
	/**
	 * IDの再計算と割りあて
	 */
	 function refreshId(){
	 	var table = document.getElementById('list');
	 	for(var i=1; i<table.rows.length; i++){
			table.rows[i].cells[0].innerHTML = "<span class=\"text12\">"+i+"</span>";
	 	}
	 }
	/**
	 * 大学追加チェック
	 */
	<%--
		2004.10.25
		Yoshimoto KAWAI - Totec
		HIDDENが単数の場合のチェックが出来ていなかったので修正
	--%>
	function isAddable(univ, fac, dept){
		var returnStr = true;
		var value = univ + "," + fac + "," + dept;
		var univValues = document.forms[0].elements['univValue'];
		if (univValues != null) {
			// 複数
			if (univValues.length) {
				for(var i=0; i<univValues.length; i++){
					if(univValues[i].value == value) returnStr = false;
				}
			// 単数
			} else {
				if (univValues.value == value) returnStr = false;
			}
		}
		return returnStr;
	}
	/**
	 * サイズが最大値を超えていないかチェック
	 */
	function isUnderMax(){
		var depts = document.getElementById('deptCd');
		//alert(depts.options[0].selected);
		var selectedCount = 0;
		for(var i=0; i<depts.options.length; i++){
			if(depts.options[i].selected){
				selectedCount ++;
			}
		}

		var tbl = document.getElementById('list');
		if((tbl.rows.length + selectedCount -1) < MAX_ADDABLE_NUM){
			return true;
		}else{
			window.alert("<c:out value="${SEL_NUM_BY_UNIV}" />件以上は登録できません");
			return false;
		}
	}
	/**
	 * 大学リスト初期化
	 * もともと選択されていたものをもどす
	 */
	 function initList(uI, uV, uN){
	 	var tbl = document.getElementById('list');

	 		        	var newRow = tbl.insertRow(tbl.rows.length);
		    			newRow.setAttribute("id", ++magicNum);

			        	//リストナンバー
						var newCell2 = newRow.insertCell();
						newCell2.width = "4%";
						newCell2.style.backgroundColor = "#E1E6EB";
						newCell2.align = "center";

						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
				       	// var span = document.createElement("<span class=\"text12\">");
				       	var span = document.createElement("span");
				       	span.setAttribute("class", "text12");
						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

				       	    span.appendChild(document.createTextNode(uI));
						newCell2.appendChild(span);

						//大学・学部・学科表示
						var newCell3 = newRow.insertCell();
						newCell3.width = "87%";
						newCell3.style.backgroundColor = "#F4E5D6";

						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
				       	// var span2 = document.createElement("<span class=\"text12\">");
				       	var span2 = document.createElement("span");
				       	span2.setAttribute("class", "text12");
						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

						span2.appendChild(document.createTextNode(uN));
						newCell3.appendChild(span2);

						//削除ボタン
						var newCell4 = newRow.insertCell();
						newCell4.width = "9%";
						newCell4.style.backgroundColor = "#F4E5D6";
						newCell4.align = "center";

						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
				       	// var input4 = document.createElement("<input onclick=\"deleteList("+magicNum+");\">");
						var input4 = document.createElement("input");
						input4.setAttribute("onclick", "deleteList(" + magicNum + ")");
						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

					   	input4.type = "button";
					   	input4.value = "削除";
					   	input4.style.width = "40px";
						newCell4.appendChild(input4);

						//hidden values
	        			var newCell = newRow.insertCell();
	        			//uniqueCode

						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
			        	// var input0 = document.createElement("<input name=\"univValue\" type=\"hidden\">");
	        			var input0 = document.createElement("input");
	        			input0.setAttribute("name", "univValue");
	        			input0.setAttribute("type", "hidden");
	        			<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

			        	input0.setAttribute("type", "hidden");
			        	input0.setAttribute("value", uV);
			        	newCell.appendChild(input0);
			        	//name for disp

						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
			        	// var input01 = document.createElement("<input name=\"univValueName\">");
						var input01 = document.createElement("input");
						input01.setAttribute("name", "univValueName");
						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

			        	input01.setAttribute("type", "hidden");
			        	input01.setAttribute("value", uN);
			        	newCell.appendChild(input01);
	 }
	/**
	 * 大学追加
	 */
	var magicNum = 0;
	//var MAX_ADDABLE_NUM = 51;
	var MAX_ADDABLE_NUM = <c:out value="${SEL_NUM_BY_UNIV}" />+1;
	function addList(){
	    // 新しい行を追加
	    var tbl = document.getElementById('list');

        	//univValueの作成
        	var text = "";
			var univ =  document.forms[0].elements['univCd'][document.forms[0].elements['univCd'].selectedIndex];
			var faculty = document.forms[0].elements['facultyCd'][document.forms[0].elements['facultyCd'].selectedIndex];
        	var depts = document.forms[0].elements['deptCd'];

        	for(var i=0; i<depts.length; i++){
        		if(depts[i].selected){
        			if(isAddable(univ.value, faculty.value, depts[i].value)){
      				//まだないので追加できる

	        			//row
	        			magicNum ++;
	        			var newRow = tbl.insertRow(tbl.rows.length);
		    			newRow.setAttribute("id", magicNum);

			        	//リストナンバー
						var newCell2 = newRow.insertCell();
						newCell2.width = "4%";
						newCell2.style.backgroundColor = "#E1E6EB";
						newCell2.align = "center";

						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
				       	// var span = document.createElement("<span class=\"text12\">");
						var span = document.createElement("span");
						span.setAttribute("class", "text12");
						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

				       	if(document.forms[0].elements['univValue'] == null)
				       	   	span.appendChild(document.createTextNode(1));
				       	else{
				       		if(document.forms[0].elements['univValue'].length == undefined)
				       			span.appendChild(document.createTextNode(2));
				       		else
				       			span.appendChild(document.createTextNode(document.forms[0].elements['univValue'].length+1));
				       	}
						newCell2.appendChild(span);

						//大学・学部・学科表示
						var newCell3 = newRow.insertCell();
						newCell3.width = "87%";
						newCell3.style.backgroundColor = "#F4E5D6";

						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
				       	// var span2 = document.createElement("<span class=\"text12\">");
						var span2 = document.createElement("span");
						span2.setAttribute("class", "text12");
						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

						span2.appendChild(document.createTextNode(univ.text+" "+faculty.text+" "+depts[i].text));
						newCell3.appendChild(span2);

						//削除ボタン
						var newCell4 = newRow.insertCell();
						newCell4.width = "9%";
						newCell4.style.backgroundColor = "#F4E5D6";
						newCell4.align = "center";

						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
						// var input4 = document.createElement("<input onclick=\"deleteList("+magicNum+")\">");
				       	var input4 = document.createElement("input");
				       	input4.setAttribute("onclick", "deleteList(" + magicNum + ")");
				     	<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

					   	input4.type = "button";
					   	input4.value = "削除";
					   	input4.style.width = "40px";
						newCell4.appendChild(input4);

						//hidden values
	        			var newCell = newRow.insertCell();
	        			//uniqueCode

						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
			        	// var input0 = document.createElement("<input name=\"univValue\" type=\"hidden\">");
						var input0 = document.createElement("input");
						input0.setAttribute("name", "univValue");
						input0.setAttribute("type", "hidden");
						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

			        	input0.setAttribute("type", "hidden");
			        	input0.setAttribute("value", univ.value+","+faculty.value+","+depts[i].value);
			        	newCell.appendChild(input0);
			        	//name for disp

						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
			        	// var input01 = document.createElement("<input name=\"univValueName\">");
						var input01 = document.createElement("input");
			        	input01.setAttribute("name", "univValueName");
			        	<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

			        	input01.setAttribute("type", "hidden");
			        	input01.setAttribute("value", univ.text+" "+faculty.text+" "+depts[i].text);
			        	newCell.appendChild(input01);

					}
        		}
        	}
        	showHeader3();
	}
	/**
	 * すべて選択
	 */
	function selectAll(check){
		for(var i=0; i<document.forms[0].deptCd.options.length; i++){
			if(check)
				document.forms[0].deptCd.options[i].selected = true;
			else
				document.forms[0].deptCd.options[i].selected = false;
		}
	}
	/**
	 * 判定できるかどうかチェック
	 */
	function isJudgable(){
		if(document.forms[0].elements['univValue'] == null || document.forms[0].elements['univValue'].length == 0){
			window.alert("判定対象一覧が登録されておりません");
			return false;
		}else {
			var flg = true;
			<c:choose>
				<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
					var choice = document.forms[0].elements["judgeStudent"].options[document.forms[0].elements["judgeStudent"].selectedIndex].value;
					if(choice != '1'){//表示中の生徒一人を選択している
						if(!didTakeExam()){
							flg = false;
						}
					}
				</c:when>
				<c:otherwise>
					if(!didTakeExam()){
						flg = false;
					}
				</c:otherwise>
			</c:choose>
			if(!flg){
				window.alert("この生徒は対象模試を受験していないので判定できません");
			}
			return flg;
		}
	}
<%--student_rotatorに移動 0407 K.Kondo delete
	/**
	 * 表示中の生徒が対象模試を受験しているかどうか
	 */
	function didTakeExam(){
		var flg = true;
		for(var i=0; i<personString.length; i++){
			if(personString[i][0] == document.forms[0].targetPersonId.value){
				if(personString[i][4] == 'false'){
					flg = false;
				}
			}
		}
		return flg;
	}
--%>
	/**
	 * 初期化
	 */
	function init(){

		startTimer();
		<%--スクロール位置 0426 K.Kondo--%>
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

		//1.選択中の生徒まで回す
		findStudent();
		//2.選択中の生徒が一人ならArrowボタンを無効化する
		if(getStudentNum() == 1) disableArrows();

	 	<%if(form.getUnivValueName() != null && form.getUnivValueName().length > 0){%>
	 		<%for(int i=0; i<form.getUnivValueName().length; i++){%>
	 			initList(<%=i+1%>, "<%=form.getUnivValue()[i]%>", "<%=form.getUnivValueName()[i]%>");
	 		<%}%>
	 	<%}%>

		<c:if test="${nestedUnivListSize != null && nestedUnivListSize > 0}">
		//選択されている大学を表示
			loadUnivList();
		</c:if>

		//選択中生徒の学年によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
		gradeChange();

		//ヘッダーの表示・非表示
		showHeader3();

		//ロード中
		<%@ include file="/jsp/script/loading.jsp" %>

	}
//-->
</script>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I203Servlet" />" method="post" onSubmit="return false">
<%-- 共通 --%>
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">
<%-- XXX --%>
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="charValue" value="">
<%-- 判定タイプ --%>
<input type="hidden" name="button" value="byUniv">
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">


<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->


<!--志望大学判定-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--概要-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr valign="top">
<td width="183">
<!--●●●左側●●●-->

<!--志望大学判定トップへ-->
<table border="0" cellpadding="0" cellspacing="0" width="183">
<tr valign="top">
<td colspan="2" width="171" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="171" height="1" border="0" alt=""><br></td>
<td rowspan="2" width="12" background="./individual/img/tbl_cnr_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="170" bgcolor="#4694AF">

<table border="0" cellpadding="0" cellspacing="0" width="170">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="23" border="0" alt=""><br></td>
<td width="13"><a href="#"><img src="./shared_lib/img/parts/arrow_left_white.gif" width="8" height="9" border="0" alt=""></a></td>
<td width="151">
<span class="text12"><a href="javascript:submitForm('i201')" style="color:#FFFFFF;">志望大学判定トップ</a></span></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/志望大学判定トップへ-->

<!--STEP-->
<table border="0" cellpadding="0" cellspacing="0" width="183">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#4694AF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="180" bgcolor="#F3F5F5" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--step1-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr height="22" bgcolor="#FF8F0E">
<td width="55" align="right"><img src="./shared_lib/img/step/step1_cur.gif" width="46" height="12" border="0" alt="STEP1"><br></td>
<td width="13"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="94"><b class="text12" style="color:#FFFFFF;">条件設定</b></td>
</tr>
</table>
<!--/step1-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr>
<td width="162" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step2-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step2_def.gif" width="46" height="12" border="0" alt="STEP2"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="94" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">判　定</b></td>
</tr>
</table>
<!--/step2-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="4" width="183" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="183" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/STEP-->
<!--/●●●左側●●●-->
</td>
<td width="17"><img src="./shared_lib/img/parts/sp.gif" width="17" height="1" border="0" alt=""><br></td>
<td width="678">
<!--●●●右側●●●-->
<table border="0" cellpadding="0" cellspacing="0" width="678">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="678">
<tr>
<td width="676" bgcolor="#FFFFFF"><span class="text12">&nbsp;<b>対象模試：</b><c:out value="${sessionScope['iCommonMap'].targetExamName}"/></span></td>
</tr>
<tr>
<td width="676" bgcolor="#FFFFFF">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="662">
<tr>
<td width="22"><img src="./shared_lib/img/parts/sp.gif" width="22" height="1" border="0" alt=""><br></td>
<td width="634"><b class="text14">&nbsp;1.大学検索</b><br>

<!--大学名・大学コード検索-->
<div style="margin-top:2px;">
<table border="0" cellpadding="0" cellspacing="0" width="634">
<tr>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="271" valign="top">
<!--直接入力-->
<table border="0" cellpadding="5" cellspacing="0" width="271">
<tr>
<td width="271" bgcolor="#CDD7DD"><b class="text12">&nbsp;大学名直接入力</b></td>
</tr>
</table>
<img src="./shared_lib/img/parts/sp.gif" width="271" height="2" border="0" alt=""><br>

<table border="0" cellpadding="0" cellspacing="0" width="271">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="271">
<tr valign="top">
<td width="269" height="195" bgcolor="#FBD49F" align="center">
<!--大学名orコード-->
<div style="margin-top:30px;">
<table border="0" cellpadding="0" cellspacing="0" width="250">
<tr>
<td><input type="hidden" name="searchMode" value="name">&nbsp;</td>
<!--
<td width="23"><input type="radio" name="searchMode" value="name"<c:if test="${ form.searchMode == 'name' }"> checked</c:if>></td>
<td width="60"><span class="text12">大学名</span></td>
<td width="23"><input type="radio"  name="searchMode" value="code"<c:if test="${ form.searchMode == 'code' }"> checked</c:if>></td>
<td width="144"><span class="text12">大学コード</span></td>
 -->
</tr>
</table>
</div>
<!--/大学名orコード-->
<!--テキストボックス-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="250">
<tr>
<td width="250"><input type="text" size="30" class="text12" name="searchStr" style="width:250px;" value="<c:out value="${form.searchStr}" />"></td>
</tr>
</table>
</div>
<!--/テキストボックス-->
<!--検索ボタン-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="250">
<tr>
<td align="center"><input type="button" value="&nbsp;検索&nbsp;" class="text12" style="width:105px;" onclick="submitSearch()">
</td>
</tr>
</table>
</div>
<!--/検索ボタン-->
<!--注意-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><span class="text12">※</span></td>
<td><span class="text12">大学名はひらがなで入力してください</span></td>
</tr>
</table>
</div>
<!--/注意-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/直接入力-->
</td>
<td width="22" align="center"><b class="text12">or</b></td>
<td width="337" valign="top">
<!--大学名頭文字指定-->
<table border="0" cellpadding="5" cellspacing="0" width="337">
<tr>
<td width="337" bgcolor="#CDD7DD"><b class="text12">&nbsp;大学名頭文字指定</b></td>
</tr>
</table>
<img src="./shared_lib/img/parts/sp.gif" width="337" height="2" border="0" alt=""><br>

<table border="0" cellpadding="0" cellspacing="0" width="337">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="337">
<tr valign="top">
<td width="335" height="195" bgcolor="#FBD49F" align="center">
<!--リスト-->
<div style="margin-top:5px;">
<table border="0" width="325">
<tr>
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD START --%>
<%--
<td width="30" height="30"><input type="button" value="あ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="か" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="さ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="た" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="な" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="は" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ま" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="や" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ら" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="わ" style="width:23px;" onclick="submitChar(this.value)"></td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="い" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="き" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="し" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ち" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="に" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ひ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="み" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
<td width="30" height="30"><input type="button" value="り" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="う" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="く" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="す" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="つ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ぬ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ふ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="む" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ゆ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="る" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="え" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="け" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="せ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="て" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ね" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="へ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="め" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
<td width="30" height="30"><input type="button" value="れ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="お" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="こ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="そ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="と" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="の" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ほ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="も" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="よ" style="width:23px;" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ろ" style="width:23px;" onclick="submitChar(this.value)"></td>
--%>
<td width="30" height="30"><input type="button" value="あ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="か" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="さ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="た" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="な" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="は" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ま" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="や" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ら" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="わ" class="iniText" onclick="submitChar(this.value)"></td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="い" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="き" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="し" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ち" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="に" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ひ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="み" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
<td width="30" height="30"><input type="button" value="り" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="う" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="く" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="す" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="つ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ぬ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ふ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="む" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ゆ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="る" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="え" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="け" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="せ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="て" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ね" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="へ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="め" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
<td width="30" height="30"><input type="button" value="れ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><br>
</td>
</tr>
<tr>
<td width="30" height="30"><input type="button" value="お" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="こ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="そ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="と" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="の" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ほ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="も" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="よ" class="iniText" onclick="submitChar(this.value)"></td>
<td width="30" height="30"><input type="button" value="ろ" class="iniText" onclick="submitChar(this.value)"></td>
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD START --%>
<td width="30" height="30"><br>
</td>
</tr>
</table>
</div>
<!--/リスト-->
<!--注意-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="335">
<tr>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="325"><span class="text12">※大学名の頭文字を選択してください</span></td>
</tr>
</table>
</div>
<!--/注意-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/大学名頭文字指定-->
</td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/大学名・大学コード検索-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<c:if test="${(nestedUnivListSize != null && nestedUnivListSize > 0)}">

	<b class="text14">&nbsp;2.判定対象の選択</b><span class="text12">　※［Control］キーで複数指定</span><br>

	<!--判定対象の選択-->
	<div style="margin-top:2px;">
	<table border="0" cellpadding="0" cellspacing="0" width="634">
	<tr>
	<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
	<td width="630" bgcolor="#F4E5D6">
	<table border="0" cellpadding="14" cellspacing="0" width="630">
	<tr>
	<td width="630">

	<table border="0" cellpadding="0" cellspacing="0" width="602">
	<tr valign="top">
	<td width="180">
	<!--大学リスト-->
	<table border="0" cellpadding="0" cellspacing="0" width="180">
	<tr>
	<td width="180" bgcolor="#FFFFFF">
	<table border="0" cellpadding="0" cellspacing="2" width="180">
	<tr>
	<td width="176" height="17" bgcolor="#CDD7DD" align="center"><b class="text12">大学リスト</b></td>
	</tr>
	<tr>
	<td width="176">
	<select size="9" class="text12" style="width:176px;background-color:#CCC;" id="univCd" name="univCd" onchange="loadFacultyList()"></select></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--/大学リスト-->
	</td>
	<td width="31" align="center"><img src="./shared_lib/img/parts/arrow_right_darkblue_b.gif" width="8" height="39" border="0" alt="→" vspace="50"><br></td>
	<td width="180">
	<!--学部リスト-->
	<table border="0" cellpadding="0" cellspacing="0" width="180">
	<tr>
	<td width="180" bgcolor="#FFFFFF">
	<table border="0" cellpadding="0" cellspacing="2" width="180">
	<tr>
	<td width="176" height="17" bgcolor="#CDD7DD" align="center"><b class="text12">学部リスト</b></td>
	</tr>
	<tr>
	<td width="176"><select size="9" class="text12" style="width:176px;background-color:#CCC;" id="facultyCd" name="facultyCd" onchange="loadDeptList()"></select></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--/学部リスト-->
	</td>
	<td width="31" align="center"><img src="./shared_lib/img/parts/arrow_right_darkblue_b.gif" width="8" height="39" border="0" alt="→" vspace="50"><br></td>
	<td width="180">
	<!--学科リスト-->
	<table border="0" cellpadding="0" cellspacing="0" width="180">
	<tr>
	<td width="180" bgcolor="#FFFFFF">
	<table border="0" cellpadding="0" cellspacing="2" width="180">
	<tr>
	<td width="176" height="17" bgcolor="#CDD7DD" align="center"><b class="text12">学科リスト</b></td>
	</tr>
	<tr>
	<td width="176" height="17" bgcolor="#FFFFFF" align="right"><span class="text12"><a href="javascript:selectAll(true)"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4" align="absmiddle">すべてを選択</a></span></td>
	</tr>
	<tr>
	<td width="176" align="center"><select size="9" class="text12" multiple style="width:176px;background-color:#CCC;" id="deptCd" name="deptCd"></select><img src="./shared_lib/img/parts/arrow_down_darkblue_s.gif" width="34" height="7" border="0" alt="↓"></td>
	</tr>
	<tr>
	<td width="176" align="center"><input type="button" name="" value="一覧に追加" style="width:174px;" onclick="javascript: isUnderMax() ? addList() : void(0)"><br><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--/学科リスト-->
	</td>
	</tr>
	</table>

	</td>
	</tr>
	</table>
	</td>
	<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
	</tr>
	</table>
	</div>
	<!--判定対象の選択-->
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
</c:if>

<div id="header3" style="display: none"><b class="text14">&nbsp;3.判定対象一覧</b><span class="text12">　※最大<c:out value="${SEL_NUM_BY_UNIV}" />学科の登録ができます</span><br></div>

<!--リスト-->
<table border="0" cellpadding="5" cellspacing="2" width="634" name="list" id="list">
<!--set--><tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>

<!--/set-->
</table>
<!--リスト-->



<!--矢印-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="10">
<tr>
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
</div>
<!--/矢印-->


<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="634">
<tr valign="top">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="632">

<table border="0" cellpadding="0" cellspacing="0" width="630">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="630">
<tr valign="top">
<td width="628" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td>

<!-- モードの判別-->
<c:choose>
<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
	<select name="judgeStudent" class="text12" style="width:156;">
		<option value="1" <c:if test="${form.judgeStudent == '1'}">selected</c:if>>選択中の生徒すべて</option>
		<option value="2" <c:if test="${form.judgeStudent == '2'}">selected</c:if>>表示中の生徒一人</option>
	</select>
</c:when>
<c:otherwise>
	<input type="hidden" name="judgeStudent" value="2">
</c:otherwise>
</c:choose>

<!--/ モードの判別-->

</td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="614">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="614">
<tr valign="top">
<td width="612" bgcolor="#FFFFFF" align="center"><a href="javascript:submitJudge()" onclick="return isJudgable();"><img src="./shared_lib/img/btn/hantei2.gif" width="96" height="36" border="0" alt="判定"></a><br></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/●●●右側●●●-->
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/志望大学判定-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>

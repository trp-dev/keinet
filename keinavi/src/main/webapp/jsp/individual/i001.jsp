<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    <%@ include file="/jsp/script/submit_change.jsp" %>
    <%@ include file="/jsp/script/submit_exit.jsp" %>
    <%@ include file="/jsp/script/submit_save.jsp" %>
    <%@ include file="/jsp/script/submit_menu.jsp" %>
    <%@ include file="/jsp/script/open_common.jsp" %>
    // ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>


    function submitForm(forward) {
        document.getElementById("MainLayer").style.visibility = "hidden"
        document.getElementById("MainLayer").style.top = -2000;
        document.getElementById("LoadingLayer").style.visibility = "visible";
        document.forms[0].target = "_self";
        document.forms[0].forward.value = forward;
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }

    function submitBack() {
        document.getElementById("MainLayer").style.visibility = "hidden"
        document.getElementById("MainLayer").style.top = -2000;
        document.getElementById("LoadingLayer").style.visibility = "visible";
        document.forms[0].target = "_self";
        document.forms[0].forward.value = "w008";
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }

    function preSubmit(mode){
        document.forms[0].mode.value = mode;
        submitForm('i002');
    }

    function init() {
    	startTimer();
        document.getElementById("LoadingLayer").style.visibility = "hidden";
        document.getElementById("MainLayer").style.visibility = "visible";
    }

// -->
</script>
</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" onload="init()">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I001Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="targetYear" value="<c:out value="${Profile.targetYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${Profile.targetExam}" />">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changed" value="<c:choose><c:when test="${Profile.changed}">1</c:when><c:otherwise>0</c:otherwise></c:choose>">


<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!-- ●●● -->
<table border="0" cellpadding="0" cellspacing="0" width="808">
<tr>
<td>
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="808">
<tr>
<td width="808"><img src="./individual/img/ttl_mode.gif" width="119" height="24" border="0" alt="モード選択"><br></td>
</tr>
</table>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--説明-->
<table border="0" cellpadding="0" cellspacing="0" width="758">
<tr>
<td width="808"><span class="text14-hh">個人成績分析では、用途に応じて２種類のモードを用意しています。<br></span></td>
</tr>
</table>
<!--/説明-->
</td>
<td>
<!-- ■■■ -->
<table border="0" cellpadding="0" cellspacing="0" width="100">
<tr>
<td width="100%">
</td>
</tr>
</table>
<!-- /■■■ -->
</td>
</tr>
</table>
<!-- /●●● -->
<!--モード選択-->
<div style="margin-top:10px;" align="center">
<table border="0" cellpadding="0" cellspacing="0" width="530">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="4" cellspacing="1" width="530">

<tr valign="top">
<td width="522" bgcolor="#EFF2F3">
<table border="0" cellpadding="3" cellspacing="0" width="522">
<tr>
<td bgcolor="#758A98" align="center"><b class="text14" style="color:#FFFFFF">モード選択</b>
</td>
</tr>
</table>
</td>
</tr>

<tr valign="top">
<td width="522" bgcolor="#FFFFFF" align="center">
<!--ボタン-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="512">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="4" cellspacing="1" width="512">
<tr valign="top">
<td bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><input type="button" value="&nbsp;分析モード&nbsp;" class="text12" style="width:130px;" onclick="preSubmit('0')"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td><input type="button" value="&nbsp;面談モード&nbsp;" class="text12" style="width:130px;" onclick="preSubmit('1')"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br>
<!--/ボタン-->
</td>
</tr>

</table>
</td>
</tr>
</table>
</div>
<!--/モード選択-->

<!--２つのモード-->
<div style="margin-top:15px;" align="center">
<!--
<table border="0" cellpadding="0" cellspacing="0" width="630">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="630">
<tr>
<td width="628" height="31" bgcolor="#EFF2F3" align="center"><img src="./individual/img/ttl_read.gif" width="431" height="18" border="0" alt="生徒と個人面談をするときにも安心して利用できます"><br></td>
</tr>
<tr valign="top">
<td width="628" bgcolor="#EFF2F3" align="center">
-->

<!--
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr>
<td height="25" align="center"><span class="text14">〜　個人成績を保護するための2つのモード　〜</span></td>
</tr>
</table>
-->

<!--中身-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="8" cellspacing="1" width="600">
<tr valign="top">
<td bgcolor="#FFFFFF" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="561">
<tr valign="top">
<td width="250">
<!--分析モード-->
<table border="0" cellpadding="0" cellspacing="0" width="250">
<tr valign="top">
<td width="250" align="center"><A href="#"  onclick="preSubmit('0')"><img src="./individual/img/illust01.gif" width="104" height="75" border="0" alt="分析モード"></A><br></td>
</tr>
<tr valign="top">
<td width="250"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="250" align="left"><span class="text12-hh"><b>分析モード：</b><br>
選択した複数の生徒について表示できます。<BR>成績分析を行うための機能がすべて利用可能となっています。特定のクラス・グループの傾向の分析に適しています。</span></td>
</tr>
</table>
<!--/分析モード-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="250">
<!--面談モード-->
<table border="0" cellpadding="0" cellspacing="0" width="250">
<tr valign="top">
<td width="250" align="center"><A href="#"  onclick="preSubmit('1')"><img src="./individual/img/illust02.gif" width="156" height="75" border="0" alt="面談モード"></A><br></td>
</tr>
<tr valign="top">
<td width="250"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="250" align="left"><span class="text12-hh"><b>面談モード：</b><br>
選択した生徒一人の生徒について表示できます。<br>個人面談に適しています。</span></td>
</tr>
</table>
<!--/面談モード-->
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/中身-->

<!--
</td>
</tr>

</table>
</td>
</tr>
</table>
-->
</div>
<!--/２つのモード-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>

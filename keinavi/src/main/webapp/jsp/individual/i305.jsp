<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<SCRIPT type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>
	<%@ include file="/jsp/script/open_sample.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
	<%@ include file="/jsp/script/open_schedule_alert.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/printDialog_close.jsp" %>
	 var printDialogPattern = "";
	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

	/**
	 * サブミット
	 */
	function submitForm(forward) {
		<%--スクロール位置 0426 K.Kondo--%>
		if(forward=='<c:out value="${param.forward}" />') {
			document.forms[0].scrollX.value = document.body.scrollLeft;
			document.forms[0].scrollY.value = document.body.scrollTop;
		} else {
			document.forms[0].scrollX.value = 0;
			document.forms[0].scrollY.value = 0;
		}
		document.getElementById("MainLayer").style.visibility = "hidden";
		document.getElementById("MainLayer").style.top = -2000;
		document.getElementById("LoadingLayer").style.visibility = "visible";
		document.forms[0].target = "_self";
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 戻るボタン
	 */
	function submitBack() {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "i301";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 生徒切り替え
	 */
	<%@ include file="/jsp/individual/include/student_rotator.jsp" %>
	/**
	 * 印刷ダイアログ
	 */
	<%@ include file="/jsp/individual/include/printDialog.jsp" %>
	/**
	 * 印刷チェック
	 */
	function printCheck() {
		if (!validate()) {
			window.alert("出力フォーマットを選択してください。");
		} else {
			printDialog();
		}
	}
	// 表示
	function printView() {
		if (!validate()) {
			window.alert("出力フォーマットを選択してください。");
		} else {
			document.forms[0].printStatus.value = "1";
			printSheet();
		}
	}
	// 入力チェック
	function validate() {
		for (i=0; i < document.forms[0].textFormat.length; i++) {
			if (document.forms[0].textFormat[i].checked) {
				return true;
			}
		}
		return false;
	}
	/**
	 * 初期化
	 */
	function init(){
		startTimer();
		<%--スクロール位置 0426 K.Kondo--%>
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

		//1.選択中の生徒まで回す
		findStudent();
		//2.選択中の生徒が一人ならArrowボタンを無効化する
		if(getStudentNum() == 1) disableArrows();

		//選択中生徒の学年によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
		gradeChange();

		//ロード中
		<%@ include file="/jsp/script/loading.jsp" %>
	}
//-->
</SCRIPT>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I305Servlet" />" method="POST">
<input type="hidden" name="forward">
<input type="hidden" name="backward">
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="printStatus" value="">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="targetYear" value="<c:out value="${iCommonMap.targetExamYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${iCommonMap.targetExamCode}" />">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">

<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->


<!--個人手帳-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--概要-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><span class="text12"><b>センター判定対象模試：</b><c:out value="${markExamName}" /></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td><span class="text12"><b>2次判定対象模試：</b><c:out value="${wrtnExamName}" /></span></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--メニュー切り替え-->
<%@ include file="/jsp/individual/include/menu_switch.jsp" %>
<!--/メニュー切り替え-->
<!--小タイトル-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="832">

<table border="0" cellpadding="0" cellspacing="0" width="832">
<tr valign="top">
<td colspan="4" width="832" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="832" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="832" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="816" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">スケジュール（表）</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="832" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="832" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/小タイトル-->
<!--説明-->
<div style="margin-top:12px;">
<table border="0" cellpadding="0" cellspacing="0" width="828">
<tr valign="top">
<td>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td align="right"><span class="text14">受験予定大学一覧で受験日が設定されている大学のスケジュールです。<br>※<a href="#here" onclick="openScheduleAlert(3);">入試日程に関するご注意</a></span></td>
	</tr>
	</table>
</td>
<td align="right"><%@ include file="/jsp/individual/include/text_out_format_sub.jsp" %></td>
</tr>
</table>
</div>
<!--/説明-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--リスト-->
<table border="0" cellpadding="0" cellspacing="0" width="852">
<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
<td width="832">

<table border="0" cellpadding="2" cellspacing="2" width="832">
<!--項目-->
<tr bgcolor="#CDD7DD">
<td rowspan="2" width="21%" align="center"><span class="text12">受験大学・学部・学科</span></td>
<td rowspan="2" width="5%" align="center" valign="top" nowrap><span class="text12" style="writing-mode:tb-rl;">
&nbsp;試験日
</span></td>
<td rowspan="2" width="5%" align="center" valign="top" nowrap><span class="text12" style="writing-mode:tb-rl;">
&nbsp;受験日
</span></td>
<td rowspan="2" width="6%" align="center" valign="top" nowrap><span class="text12" style="writing-mode:tb-rl;">
&nbsp;地方試験地
</span></td>
<td rowspan="2" width="5%" align="center" valign="top" nowrap><span class="text12" style="writing-mode:tb-rl;">
&nbsp;出願締切日
</span></td>
<td rowspan="2" width="5%" align="center" valign="top" nowrap><span class="text12" style="writing-mode:tb-rl;">
&nbsp;合格発表日
</span></td>
<td rowspan="2" width="5%" align="center" valign="top" nowrap><span class="text12" style="writing-mode:tb-rl;">
&nbsp;入学手続締切日
</span></td>
<td colspan="2" width="8%" align="center" nowrap><span class="text12">難易度ランク</span></td>
<td rowspan="2" width="28%" align="center"><span class="text12">入試科目（配点）</span><br><span class="text12">※上段：センター　下段：２次</span></td>
<%-- 総合の非表示のためcolspanを3から2に変更<td colspan="3" width="12%" align="center"><span class="text12">合格可能性判定</span></td>--%>
<td colspan="2" width="12%" align="center"><span class="text12">合格可能性判定</span></td>
</tr>

<tr bgcolor="#CDD7DD">
<td width="4%" valign="top" nowrap><span class="text12" style="writing-mode:tb-rl;">
&nbsp;得点率<br>&nbsp;センターボーダー
</span></td>
<td width="4%" valign="top" nowrap><span class="text12" style="writing-mode:tb-rl;">
&nbsp;偏差値<br>&nbsp;難易度ランク
</span></td>
<td width="4%" align="center" valign="top" nowrap><span class="text12" style="writing-mode:tb-rl;">
&nbsp;センター
</span></td>
<td width="4%" align="center" valign="top" nowrap><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br><span class="text12">2<br>次
</span></td>
<%-- 総合の非表示
<td width="4%" align="center" valign="top" nowrap><span class="text12" style="writing-mode:tb-rl;">
&nbsp;総合
</span></td>
--%>
</tr>
<!--/項目-->
<c:forEach var="data" items="${resultList}" varStatus="status">
	<tr>
	<td bgcolor="#E1E6EB" rowspan="2">
	<table border="0" cellpadding="4" cellspacing="0">
	<tr>
	<td><span class="text12"><c:out value="${data.univNameAbbr}" /> <c:out value="${data.facultyNameAbbr}" /> <c:out value="${data.deptNameAbbr}" /> (<c:out value="${data.entExamModeName}" />)</span></td>
	</tr>
	</table>
	</td>
	<td bgcolor="#F4E5D6" align="center" rowspan="2"><span class="text12">
	<%-- 試験日 --%>
	<c:forEach var="planData" items="${data.planMap}">
		<c:out value="${planData.value.entExamInpleDateStr}" />
		<br>
	</c:forEach>
	</span></td>
	<td bgcolor="#F4E5D6" align="center" rowspan="2"><span class="text12">
	<%-- 受験日 --%>
	<c:forEach var="planData" items="${data.planMap}">
		<c:forEach var="item" items="${planData.value.examPlanDateStr}" varStatus="status"><c:if test="${item != null}"><c:if test="${status.index != 0}">,</c:if><c:out value="${item}"/></c:if></c:forEach>
		<br>
	</c:forEach>
	</span></td>
	<td bgcolor="#F4E5D6" align="center" rowspan="2"><span class="text12">
	<%-- 受験地 --%>
	<c:forEach var="planData" items="${data.planMap}">
		<c:forEach var="item" items="${planData.value.provEntExamSite}" varStatus="status"><c:if test="${item != null}"><c:if test="${status.index != 0}">,</c:if><c:out value="${item}"/></c:if></c:forEach>
		<br>
	</c:forEach>
	</span></td>
	<td bgcolor="#F4E5D6" align="center" rowspan="2"><span class="text12">
	<%-- 出願締切日 --%>
	<c:forEach var="planData" items="${data.planMap}">
		<c:out value="${planData.value.appliDeadline}" />
		<br>
	</c:forEach>
	</span></td>
	<td bgcolor="#F4E5D6" align="center" rowspan="2"><span class="text12">
	<%-- 合格発表日 --%>
	<c:forEach var="planData" items="${data.planMap}">
		<c:out value="${planData.value.sucAnnDate}" />
		<br>
	</c:forEach>
	</span></td>
	<td bgcolor="#F4E5D6" align="center" rowspan="2"><span class="text12">
	<%-- 入学手続締切日 --%>
	<c:forEach var="planData" items="${data.planMap}">
		<c:out value="${planData.value.proceDeadLine}" />
		<br>
	</c:forEach>
	</span></td>
	<%-- センターボーダー得点率 --%>
	<td bgcolor="#F4E5D6" align="center" rowspan="2"><span class="text12"><c:out value="${data.cenScoreRateStr}" /></span></td>
	<%-- 難易度ランク偏差値 --%>
	<td bgcolor="#F4E5D6" align="center" rowspan="2"><span class="text12"><c:out value="${data.rankLLimitsStr}" /></span></td>
	<td bgcolor="#F4E5D6"><p class="text12" style="margin:0;padding:0 4px;"><c:out value="${data.courseAllot1}" /><c:if test="${ empty data.courseAllot1 }">&nbsp;</c:if></p></td>
	<td bgcolor="#F4E5D6" align="center" rowspan="2"><span class="text12"><c:out value="${data.centerRating}" /></span></td>
	<td bgcolor="#F4E5D6" align="center" rowspan="2"><span class="text12"><c:out value="${data.secondRating}" /></span></td>
	<%-- 総合の非表示
	<td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${data.totalRating}" /></span></td>
	--%>
	</tr>
	<tr>
	<td bgcolor="#F4E5D6"><p class="text12" style="margin:0;padding:0 4px;"><c:out value="${data.courseAllot2}" /><c:if test="${ empty data.courseAllot2 }">&nbsp;</c:if></p></td>
	</tr>
</c:forEach>

</table>
</td>
</tr>
</table>
<!--/リスト-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/個人手帳-->
<!--セキュリティスタンプの選択-->
<%@ include file="/jsp/individual/include/security_stamp.jsp" %>
<!--/セキュリティスタンプの選択-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--テキストフォーマット-->
<%@ include file="/jsp/individual/include/text_out_format.jsp" %>
<!--/テキストフォーマット-->

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->


<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>


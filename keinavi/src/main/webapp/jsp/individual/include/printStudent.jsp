<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>

<%-- 2019/07/09 QQ)Tanioka ダウンロード変更対応 ADD START --%>
<SCRIPT type="text/javascript">
<!--
	<%@ include file="/jsp/script/download.jsp" %>

	function printStudent() {
		//2019/08/22 QQ)nagai 個人成績分析の画面の対象帳票が出力されない不具合を修正 UPD START
		//printDialogPattern = "printDialog2";
		printDialogPattern = "printStudent";
		//2019/08/22 QQ)nagai 個人成績分析の画面の対象帳票が出力されない不具合を修正 UPD END
		download(1);
	}
//-->
</script>
<%-- 2019/07/09 QQ)Tanioka ダウンロード変更対応 ADD END   --%>

<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td>

<!-- モードの判別-->
<c:choose>
<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
	<select name="printStudent" class="text12" style="width:156;" onChange="this.form.printStudent_2.value=this[this.selectedIndex].value">
	<option  value="1"<c:if test="${ form.printStudent == '1'}">selected</c:if>>選択中の生徒全員</option>
	<option  value="2"<c:if test="${ form.printStudent == '2'}">selected</c:if>>表示中の生徒一人</option>
	</select>
</c:when>
<c:otherwise>
	<input type="hidden" name="printStudent" value="2">
</c:otherwise>
</c:choose>
<!--/ モードの判別-->

</td>
</tr>
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="6" cellspacing="1" width="892" bgcolor="#8CA9BB">
<tr>
<td bgcolor="#FFFFFF" align="center">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><a href="#here" onclick="printStudent()"><img src="./shared_lib/img/btn/hozon_w.gif" width="114" height="35" border="0" alt="保存"></a></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
	<%--0323 kondo 生徒選択 - クラスコンボ追加修正--%>
	/**
	 * 選択中のクラスデータ
	 */
	var classString = new Array();
	<c:forEach var="string" items="${sessionScope['iCommonMap'].dispClasses}" varStatus="status">
	classString[<c:out value="${status.index}"/>] = new Array('<c:out value="${status.index}"/>','<c:out value="${string}"/>');</c:forEach>

	/**
	 * 選択中の生徒データ
	 */
	<c:set var="targetExamId" value="${ iCommonMap.targetExamYear }${ iCommonMap.targetExamCode }" />
	var studentString = new Array();
	<c:forEach var="dispPersons" items="${sessionScope['iCommonMap'].dispStudents}" varStatus="status">
	studentString[<c:out value="${status.index}"/>] = new Array();
	<c:forEach var="dispPerson" items="${dispPersons}" varStatus="status2">studentString[<c:out value="${status.index}"/>][<c:out value="${status2.index}"/>] = new Array('<c:out value="${dispPerson.studentPersonId}"/>','<c:out value="${dispPerson.studentGrade}"/>','<c:out value="${dispPerson.studentClass}"/>','<c:out value="${dispPerson.studentClassNo}"/>','<c:out value="${dispPerson.studentNameKana}"/>','<c:out value="${dispPerson.takenExam[targetExamId]}" default="false" />');
	</c:forEach>
	</c:forEach>

	/**
	 * クラスコンボのオプションリストを作成する
	 */
	function createClassOption(combo, array){
		for (var i = 0; i < array.length; i++){
			var op = document.createElement("OPTION");
			op.text = array[i][1] + ' ｸﾗｽ';
			op.value = array[i][0];
			combo.add(op);
		}
		combo.options[0].selected = true;
	}
	/**
	 * 生徒コンボのオプションリストを作成する
	 */
	function createStudentOption(combo, array){
		for (var i = 0; i < array.length; i++){
			var op = document.createElement("OPTION");
			op.text = (array[i][3] + '番 ' + array[i][4]);
			op.value = array[i][0];
			combo.add(op);
		}
		combo.options[0].selected = true;
	}
	/**
	 * クラスコンボのオプションリストを作成
	 */
	function createClassList(){
		createClassOption(document.forms[0].selectClass, classString);
		createStudentOption();
	}
	/**
	 * 生徒コンボのオプションリストを作成
	 */
	function createStudentList(){
		clearStudentList();
		createStudentOption(document.forms[0].selectStudent, studentString[document.forms[0].selectClass.selectedIndex]);
	}
	/**
	 * 生徒コンボのオプションリストをクリア
	 */
	function clearStudentList(){
		var length = document.forms[0].selectStudent.length;
		for(var i=0; i<length; i++){
			document.forms[0].selectStudent.remove(0);
		}
	}
	/**
	 * 選択生徒を、選択クラス内の次の生徒にする。
	 */
	function roteteRight(){
		roteteSide(+1);
		showStudents();
	}
	/**
	 * 選択生徒を、選択クラス内の前の生徒にする。
	 */
	function roteteLeft(){
		roteteSide(-1);
		showStudents();
	}
	/**
	 * 選択生徒を、選択クラス内の i番後 の生徒にする。
	 */
	function roteteSide(i) {
		var combo = document.forms[0].elements['selectStudent'];
		var index = combo.selectedIndex + i;
		if(index >= combo.length){
			index = 0;
		} else if(index < 0){
			index = combo.length - 1;
		}
		combo.options[index].selected = true;
	}
	/**
	 * 「>」ボタンでサブミット
	 */
	function rightAndGo(){
		roteteRight();
		<c:if test="${param.forward != 'i003' && param.forward != 'i201' && param.forward != 'i203' && param.forward != 'i401'}">
		if(studentString[document.forms[0].selectClass.selectedIndex].length != 1){
			submitForm('<c:out value="${param.forward}" />');
		}
		</c:if>
	}
	/**
	 * 「<」ボタンでサブミット
	 */
	function leftAndGo(){
		roteteLeft();
		<c:if test="${param.forward != 'i003' && param.forward != 'i201' && param.forward != 'i203' && param.forward != 'i401'}">
		if(studentString[document.forms[0].selectClass.selectedIndex].length != 1) {
			submitForm('<c:out value="${param.forward}" />');
		}
		</c:if>
	}
	/**
	 * コンボ選択時サブミット
	 */
	 function changeAndGo() {
	 	showStudents();
	 	<c:if test="${param.forward != 'i003' && param.forward != 'i201' && param.forward != 'i203' && param.forward != 'i401'}">
		if(getStudentNum() != 1) {
			submitForm('<c:out value="${param.forward}" />');
		}
		</c:if>
	 }
	/**
	 * 生徒を最新表示
	 */
	function showStudents(){
		document.forms[0].targetPersonId.value = studentString[document.forms[0].selectClass.selectedIndex][document.forms[0].selectStudent.selectedIndex][0];
		document.all.personGrade.innerHTML = studentString[document.forms[0].selectClass.selectedIndex][document.forms[0].selectStudent.selectedIndex][1] + "年";
	}
	/**
	 * 選択した生徒が見つかるまで、リストを回す
	 */
	function findStudent(){
		<c:choose>
		<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
		initStudentList();
		</c:when>
		<c:otherwise>
		document.forms[0].targetPersonId.value = '<c:out value="${sessionScope['iCommonMap'].selectedIndividuals[0].studentPersonId}" />'
		</c:otherwise>
		</c:choose>
	}
	/**
	 * 「<」と「>」ボタンを無効化する
	 */
	function disableArrows(){
		<c:if test="${sessionScope['iCommonMap'].bunsekiMode}">
		document.forms[0].elements['leftArrow'].disabled = true;
		document.forms[0].elements['rightArrow'].disabled = true;
		</c:if>
	}
	/**
	 * 選択中の生徒数を返す
	 */
	function getStudentNum(){
		<c:choose>
		<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
		var studentNum = 0;
		var classCombo = document.forms[0].elements['selectClass'];
		for(var i=0; i<classCombo.length; i++){
			studentNum += studentString[i].length;
		}
		return studentNum;
		</c:when>
		<c:otherwise>
		return 1;
		</c:otherwise>
		</c:choose>
	}
	/**
	 * 選択中の生徒の学年
	 */
	function dispGrade(){
		return studentString[0][0][1];
	}
	/**
	 * 対象生徒がその模試を受けているかを返します
	 */
	function didTakeExam(){
		var flg = true;
		if(studentString[selectedClassIndex()][selectedStudentIndex()][5] == 'false') {
			flg = false;
		}
		return flg;
	}
	/**
	 * 選択中のクラスコンボのIndexを返す。
	 */
	function selectedClassIndex() {
		<c:choose>
		<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
		return document.forms[0].selectClass.selectedIndex;
		</c:when>
		<c:otherwise>
		return 0;
		</c:otherwise>
		</c:choose>
	}
	/**
	 * 選択中の生徒コンボのIndexを返す。
	 */
	function selectedStudentIndex() {
		<c:choose>
		<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
		return document.forms[0].selectStudent.selectedIndex;
		</c:when>
		<c:otherwise>
		return 0;
		</c:otherwise>
		</c:choose>
	}
	/**
	 * 選択中の生徒のクラスのインデックスを返す。
	 */
	function findClassIndex(){
		for(var i=0; i<studentString.length; i++){
			for(var j=0; j<studentString[i].length; j++) {
				if(studentString[i][j][0] == '<c:out value="${sessionScope['iCommonMap'].targetPersonId}"/>') {
					return i;
				}
			}
		}
	}
	/**
	 * 選択中の生徒の生徒のインデックスを返す。
	 */
	function findStudentIndex(){
		for(var i=0; i<studentString.length; i++){
			for(var j=0; j<studentString[i].length; j++) {
				if(studentString[i][j][0] == '<c:out value="${sessionScope['iCommonMap'].targetPersonId}"/>') {
					return j;
				}
			}
		}
	}
	function initStudentList() {
		var classCombo = document.forms[0].elements['selectClass'];
		var studentCombo = document.forms[0].elements['selectStudent'];

		//クラスコンボオプションの作成
		createClassOption(classCombo, classString);
		//クラスコンボの選択
		classCombo.options[findClassIndex()].selected = true;

		//生徒コンボオプションの作成
		createStudentOption(studentCombo, studentString[classCombo.selectedIndex]);
		//生徒コンボの選択
		studentCombo.options[findStudentIndex()].selected = true;

		showStudents();
	}


	<%--
	/**
	 * 生徒を表示
	 */
	function showStudents(){
		document.forms[0].targetPersonId.value = personString[selectedIndex][0];
		document.all.personInfo.innerHTML = personString[selectedIndex][1]+"年"+personString[selectedIndex][2]+"クラス"+personString[selectedIndex][5]+"番"+" "+personString[selectedIndex][3];
	}
	/**
	 * 右に生徒を回す
	 */
	function roteteRight(){
		selectedIndex += 1;
		if(selectedIndex >= personString.length){
			selectedIndex = 0;
		}
		showStudents();
	}
	/**
	 * 左に生徒を回す
	 */
	function roteteLeft(){
		selectedIndex -= 1;
		if(selectedIndex < 0){
			selectedIndex = personString.length - 1;
		}
		showStudents();
	}
	/**
	 * 「>」ボタンでサブミット
	 */
	function rightAndGo(){
		roteteRight();
		<c:if test="${param.forward != 'i003' && param.forward != 'i201' && param.forward != 'i203' && param.forward != 'i401'}">
			if(personString.length != 1){
				submitForm('<c:out value="${param.forward}" />');
			}
		</c:if>
	}
	/**
	 * 「<」ボタンでサブミット
	 */
	function leftAndGo(){
		roteteLeft();
		<c:if test="${param.forward != 'i003' && param.forward != 'i201' && param.forward != 'i203' && param.forward != 'i401'}">
			if(personString.length != 1){
				submitForm('<c:out value="${param.forward}" />');
			}
		</c:if>
	}
	/**
	 * 選択中の生徒数を返す
	 */
	function getStudentNum(){
		return personString.length;
	}
	--%>
	<%--
	/**
	 * 「<」と「>」ボタンを無効化する
	 */
	function disableArrows(){
	--%>
		<%-- 分析モード時のみボタンが存在する --%>
	<%--
		<c:if test="${sessionScope['iCommonMap'].bunsekiMode}">
			document.forms[0].elements['leftArrow'].disabled = true;
			document.forms[0].elements['rightArrow'].disabled = true;
		</c:if>
	}
	--%>
	<%--
	/**
	 * 選択した生徒が見つかるまで、リストを回す
	 */
	function findStudent(){
		createClassOption();
    	findStudent2();
    	showStudents();
		while(document.forms[0].targetPersonId.value != '<c:out value="${sessionScope['iCommonMap'].targetPersonId}"/>'){
			roteteRight();
		}
	}
	--%>
	/**
	 * 選択中の生徒の学年・選択もしによって、表示を切り替える
	 */
	function gradeChange(){
		<%--if(personString[0][1] >= 3){--%>
		if(dispGrade() >= 3){
		<%--対象生徒が「３、４，５」年生--%>
			if("<%=iCommonMap.getTargetExamTypeCode()%>" == "01" || "<%=iCommonMap.getTargetExamTypeCode()%>" == "02"){
				<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL START --%>
				<%--
				document.getElementById("memo_hidden").style.display = "inline";
				--%>
				<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL END   --%>
				<%-- 対象模試があるときのみ下記のメニューがある --%>
				<c:if test="${sessionScope['iCommonMap'].targetExamExist}">
					<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL START --%>
					<%--
					document.getElementById("graf_hidden").style.display = "inline";
					document.getElementById("note_hidden").style.display = "inline";
					document.getElementById("judge_hidden").style.display = "inline";
					document.getElementById("easyPrint_hidden").style.display = "inline";
					--%>
					<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL END   --%>
				</c:if>
			}else{
				<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL START --%>
				<%--
				document.getElementById("memo_hidden").style.display = "inline";
				--%>
				<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL END   --%>
				<%-- 対象模試があるときのみ下記のメニューがある --%>
				<c:if test="${sessionScope['iCommonMap'].targetExamExist}">
					<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL START --%>
					<%--
					document.getElementById("graf_hidden").style.display = "inline";
					document.getElementById("note_hidden").style.display = "inline";
					document.getElementById("judge_hidden").style.display = "none";
					document.getElementById("easyPrint_hidden").style.display = "inline";
					--%>
					<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL END   --%>
				</c:if>
			}
		}
		<%--if(personString[0][1] == 1 || personString[0][1] == 2){--%>
		if(dispGrade() == 1 || dispGrade() == 2){
		<%--対象生徒が「１，２」年生--%>
			<%--if(("<%=iCommonMap.getTargetExamTypeCode()%>" == "01" || "<%=iCommonMap.getTargetExamTypeCode()%>" == "02") && ("<%=iCommonMap.getTargetExamCode()%>" != "66" && "<%=iCommonMap.getTargetExamCode()%>" != "67")){ //新課程対応 del --%>
			if("<%=iCommonMap.getTargetExamTypeCode()%>" == "01" || "<%=iCommonMap.getTargetExamTypeCode()%>" == "02"){<%-- 新課程対応 add --%>
				<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL START --%>
				<%--
				document.getElementById("memo_hidden").style.display = "inline";
				--%>
				<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL END   --%>
				<%-- 対象模試があるときのみ下記のメニューがある --%>
				<c:if test="${sessionScope['iCommonMap'].targetExamExist}">
					<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL START --%>
					<%--
					document.getElementById("graf_hidden").style.display = "inline";
					document.getElementById("note_hidden").style.display = "none";
					document.getElementById("judge_hidden").style.display = "inline";
					document.getElementById("easyPrint_hidden").style.display = "inline";
					--%>
					<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL END   --%>
				</c:if>
			}else{
				<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL START --%>
				<%--
				document.getElementById("memo_hidden").style.display = "inline";
				--%>
				<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL END   --%>
				<%-- 対象模試があるときのみ下記のメニューがある --%>
				<c:if test="${sessionScope['iCommonMap'].targetExamExist}">
					<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL START --%>
					<%--
					document.getElementById("graf_hidden").style.display = "inline";
					document.getElementById("note_hidden").style.display = "none";
					document.getElementById("judge_hidden").style.display = "none";
					document.getElementById("easyPrint_hidden").style.display = "inline";
					--%>
					<%-- 2019/07/19 Hics)Ueta 機能削除にともなう導線の削除 DEL END   --%>
				</c:if>
			}
		}
	}


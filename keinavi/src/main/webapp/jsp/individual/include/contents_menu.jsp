<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<!--コンテンツメニュー-->
<table border="0" cellpadding="0" cellspacing="0" width="908" height="38" background="./shared_lib/img/parts/contents_menu_span.gif">
<tr>
<%-- 対象模試が存在しないときは面談メモのみ観覧可能 --%>
<c:if test="${sessionScope['iCommonMap'].targetExamExist}">
	<c:choose>
		<c:when test="${param.forward == 'i101' || param.forward == 'i102' || param.forward == 'i103' || param.forward == 'i104' || param.forward == 'i106' || param.forward == 'i107' || param.forward == 'i108'}">
			<td ID = "graf_hidden" width="114"><img src="./shared_lib/img/btn/cur/i_menu_01.gif" width="114" height="38" border="0" alt="成績分析"></td>
			<td ID = "graf_hidden" width="5"><img src="./shared_lib/img/parts/contents_menu_span.gif" width="5" height="38" border="0" alt=""></td>
		</c:when>
		<c:otherwise>
			<td ID = "graf_hidden" width="114"><a href="javascript:submitForm('i101')"><img src="./shared_lib/img/btn/def/i_menu_01.gif" width="114" height="38" border="0" alt="成績分析"></a></td>
			<td ID = "graf_hidden" width="5"><img src="./shared_lib/img/parts/contents_menu_span.gif" width="5" height="38" border="0" alt=""></td>
		</c:otherwise>
	</c:choose>
</c:if>
<%-- 2019/07/17 Hics)Ueta 機能削除にともなう導線の削除 DEL START --%>
<%--
<c:if test="${sessionScope['iCommonMap'].targetExamExist}">
	<c:choose>
		<c:when test="${param.forward == 'i201' || param.forward == 'i202' || param.forward == 'i203' || param.forward == 'i204'}">
			<td ID = "judge_hidden" width="114"><img src="./shared_lib/img/btn/cur/i_menu_02.gif" width="114" height="38" border="0" alt="志望大学判定"></td>
			<td ID = "judge_hidden" width="5"><img src="./shared_lib/img/parts/contents_menu_span.gif" width="5" height="38" border="0" alt=""></td>
		</c:when>
		<c:otherwise>
			<td ID = "judge_hidden" width="114"><a href="javascript:submitForm('i201')"><img src="./shared_lib/img/btn/def/i_menu_02.gif" width="114" height="38" border="0" alt="志望大学判定"></a></td>
			<td ID = "judge_hidden" width="5"><img src="./shared_lib/img/parts/contents_menu_span.gif" width="5" height="38" border="0" alt=""></td>
		</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${sessionScope['iCommonMap'].targetExamExist}">
	<c:choose>
		<c:when test="${param.forward == 'i301' || param.forward == 'i302' || param.forward == 'i305' || param.forward == 'i306'}">
			<td ID = "note_hidden" width="114"><img src="./shared_lib/img/btn/cur/i_menu_03.gif" width="114" height="38" border="0" alt="個人手帳"></td>
			<td ID = "note_hidden" width="5"><img src="./shared_lib/img/parts/contents_menu_span.gif" width="5" height="38" border="0" alt=""></td>
		</c:when>
		<c:otherwise>
			<td ID = "note_hidden" width="114"><a href="javascript:submitForm('i301')"><img src="./shared_lib/img/btn/def/i_menu_03.gif" width="114" height="38" border="0" alt="個人手帳"></a></td>
			<td ID = "note_hidden" width="5"><img src="./shared_lib/img/parts/contents_menu_span.gif" width="5" height="38" border="0" alt=""></td>
		</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${sessionScope['iCommonMap'].targetExamExist}">
	<c:choose>
		<c:when test="${param.forward == 'i401'}">
			<td ID = "easyPrint_hidden" width="114"><img src="./shared_lib/img/btn/cur/i_menu_04.gif" width="114" height="38" border="0" alt="かんたん印刷"></td>
			<td ID = "easyPrint_hidden" width="5"><img src="./shared_lib/img/parts/contents_menu_span.gif" width="5" height="38" border="0" alt=""></td>
		</c:when>
		<c:otherwise>
			<td ID = "easyPrint_hidden" width="114"><a href="javascript:submitForm('i401')"><img src="./shared_lib/img/btn/def/i_menu_04.gif" width="114" height="38" border="0" alt="かんたん印刷"></a></td>
			<td ID = "easyPrint_hidden" width="5"><img src="./shared_lib/img/parts/contents_menu_span.gif" width="5" height="38" border="0" alt=""></td>
		</c:otherwise>
	</c:choose>
</c:if>
--%>
<%-- 2019/07/17 Hics)Ueta 機能削除にともなう導線の削除 DEL END   --%>
<c:choose>
	<c:when test="${param.forward == 'i501'}">
		<td ID = "memo_hidden" width="114"><img src="./shared_lib/img/btn/cur/i_menu_05.gif" width="114" height="38" border="0" alt="面談メモ"></td>
	</c:when>
	<c:otherwise>
		<td ID = "memo_hidden"><a href="javascript:submitForm('i501')"><img src="./shared_lib/img/btn/def/i_menu_05.gif" width="114" height="38" border="0" alt="面談メモ"></a></td>
	</c:otherwise>
</c:choose>

<td ID = "memo_hidden"><img src="./shared_lib/img/parts/sp.gif" width="318" height="38" border="0" alt="" background="./shared_lib/img/parts/contents_menu_span.gif"><br></td>
</tr>
</table>
<!--/コンテンツメニュー-->

<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<!--メニュー切り替え-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<c:choose>
	<c:when test="${param.forward == 'i301'}">
		<td width="200" height="24" bgcolor="#FFAD8C" align="center"><span class="text14">1. 受験予定大学候補</span></td>
	</c:when>
	<c:otherwise>
		<td width="200" height="24">
			<table border="0" cellpadding="0" cellspacing="0" width="200">
			<tr>
			<td bgcolor="#8CA9BB">
			<table border="0" cellpadding="0" cellspacing="1" width="200">
			<tr>
			<td width="198" height="22" bgcolor="#E5EEF3" align="center"><span class="text14"><a href="javascript:submitForm('i301')">1. 受験予定大学候補</a></span></td>
			</tr>
			</table>
			</td>
			</tr>
			</table>

		</td>
	</c:otherwise>
</c:choose>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="7"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt=""><br></td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<c:choose>
	<c:when test="${param.forward == 'i302'}">
		<td width="200" height="24" bgcolor="#FFAD8C" align="center"><span class="text14">2. 受験予定大学一覧</span></td>
	</c:when>
	<c:otherwise>
		<td width="200" height="24">
			<table border="0" cellpadding="0" cellspacing="0" width="200">
			<tr>
			<td bgcolor="#8CA9BB">
			<table border="0" cellpadding="0" cellspacing="1" width="200">
			<tr>
			<td width="198" height="22" bgcolor="#E5EEF3" align="center"><span class="text14"><a href="javascript:submitForm('i302')">2. 受験予定大学一覧</a></span></td>
			</tr>
			</table>
			</td>
			</tr>
			</table>

		</td>
	</c:otherwise>
</c:choose>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="7"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt=""><br></td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<c:choose>
	<c:when test="${param.forward == 'i305'}">
		<td width="200" height="24" bgcolor="#FFAD8C" align="center"><span class="text14">3. スケジュール（表）</span></td>
	</c:when>
	<c:otherwise>
		<td width="200" height="24">
			<table border="0" cellpadding="0" cellspacing="0" width="200">
			<tr>
			<td bgcolor="#8CA9BB">
			<table border="0" cellpadding="0" cellspacing="1" width="200">
			<tr>
			<td width="198" height="22" bgcolor="#E5EEF3" align="center"><span class="text14"><a href="javascript:submitForm('i305')">3. スケジュール（表）</a></span></td>
			</tr>
			</table>
			</td>
			</tr>
			</table>

		</td>
	</c:otherwise>
</c:choose>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="7"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt=""><br></td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<c:choose>
	<c:when test="${param.forward == 'i306'}">
		<td width="200" height="24" bgcolor="#FFAD8C" align="center"><span class="text14">4. スケジュール（カレンダー）</span></td>
	</c:when>
	<c:otherwise>
		<td width="200" height="24">
			<table border="0" cellpadding="0" cellspacing="0" width="200">
			<tr>
			<td bgcolor="#8CA9BB">
			<table border="0" cellpadding="0" cellspacing="1" width="200">
			<tr>
			<td width="198" height="22" bgcolor="#E5EEF3" align="center"><span class="text14"><a href="javascript:submitForm('i306')">4. スケジュール（カレンダー）</a></span></td>
			</tr>
			</table>
			</td>
			</tr>
			</table>

		</td>
	</c:otherwise>
</c:choose>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td bgcolor="#FFAD8C"><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/メニュー切り替え-->
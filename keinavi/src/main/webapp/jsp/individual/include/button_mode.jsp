<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<!--ボタン＆モード-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr bgcolor="#828282">
<td width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="968" height="32">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td width="155">
<!--戻るボタン-->
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" onclick="submitBack()" class="text12" style="width:138px;"></td>
</tr>
</table>
<!--/戻るボタン-->
</td>

<c:choose>
	<c:when test="${param.forward == 'i002'}">
		<td width="792" align="right">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td>
				<span class="text12" style="color:#FFFFFF;">
				<b>モード&nbsp;：&nbsp;</b>
				<c:choose>
					<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">分析モード</c:when>
					<c:when test="${sessionScope['iCommonMap'].mendanMode}">面談モード</c:when>
					<c:otherwise>エラーモード</c:otherwise>
				</c:choose>
				</span>
			</td>
			<td>
				<c:choose>
					<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">&nbsp;&nbsp;<input type="button" value="面談モードへ" onclick="document.forms[0].changeMode.value='1';submitForm('<c:out value="${ param.forward }" />');"></c:when>
					<c:when test="${sessionScope['iCommonMap'].mendanMode}">&nbsp;&nbsp;<input type="button" value="分析モードへ" onclick="document.forms[0].changeMode.value='1';submitForm('<c:out value="${ param.forward }" />');"></c:when>
				</c:choose>
			</td>
			</tr>
			</table>
		</td>
		<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="32" border="0" alt=""><br></td>
		</tr>
		</table>
		</td>
	</c:when>

	<c:when test="${param.forward == 'i003'}">
		<td width="230">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="13"><a href="#"><img src="./shared_lib/img/parts/arrow_right_white_g.gif" width="7" height="9" border="0" alt="→"></a><br></td>
		<td><a href="javascript:submitForm('i002')" style="color:#FF9;"><b class="text12">生徒選択画面へ</b></a></td>
		</tr>
		</table>
		</td>
		<td width="562" align="right">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td>
				<span class="text12" style="color:#FFFFFF;">
				<b>モード&nbsp;：&nbsp;</b>
				<c:choose>
					<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">分析モード</c:when>
					<c:when test="${sessionScope['iCommonMap'].mendanMode}">面談モード</c:when>
					<c:otherwise>エラーモード</c:otherwise>
				</c:choose>
				</span>
			</td>
			<td>
				<c:choose>
					<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">&nbsp;&nbsp;<input type="button" value="面談モードへ" onclick="document.forms[0].changeMode.value='1';submitForm('<c:out value="${ param.forward }" />');"></c:when>
					<c:when test="${sessionScope['iCommonMap'].mendanMode}">&nbsp;&nbsp;<input type="button" value="分析モードへ" onclick="document.forms[0].changeMode.value='1';submitForm('<c:out value="${ param.forward }" />');"></c:when>
				</c:choose>
			</td>
			</tr>
			</table>
		</td>
		<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="32" border="0" alt=""><br></td>
		</tr>
		</table>
		</td>
	</c:when>

	<c:otherwise>
		<td width="115">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="13"><a href="#"><img src="./shared_lib/img/parts/arrow_right_white_g.gif" width="7" height="9" border="0" alt="→"></a><br></td>
		<td><a href="javascript:submitForm('i002')" style="color:#FF9;"><b class="text12">生徒選択画面へ</b></a></td>
		</tr>
		</table>
		</td>
		<td width="125">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="13"><a href="#"><img src="./shared_lib/img/parts/arrow_right_white_g.gif" width="7" height="9" border="0" alt="→"></a><br></td>
		<td><a href="javascript:submitForm('i003')" style="color:#FF9;"><b class="text12">メニュー選択</b></a></td>
		</tr>
		</table>
		</td>
		<td width="552" align="right">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td>
				<span class="text12" style="color:#FFFFFF;">
				<b>モード&nbsp;：&nbsp;</b>
				<c:choose>
					<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">分析モード</c:when>
					<c:when test="${sessionScope['iCommonMap'].mendanMode}">面談モード</c:when>
					<c:otherwise>エラーモード</c:otherwise>
				</c:choose>
				</span>
			</td>
			<td>
				<c:choose>
					<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">&nbsp;&nbsp;<input type="button" value="面談モードへ" onclick="document.forms[0].changeMode.value='1';submitForm('<c:out value="${ param.forward }" />');"></c:when>
					<c:when test="${sessionScope['iCommonMap'].mendanMode}">&nbsp;&nbsp;<input type="button" value="分析モードへ" onclick="document.forms[0].changeMode.value='1';submitForm('<c:out value="${ param.forward }" />');"></c:when>
				</c:choose>
			</td>
			</tr>
			</table>
		</td>
		<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="32" border="0" alt=""><br></td>
		</tr>
		</table>
		</td>
	</c:otherwise>
</c:choose>

</tr>
<tr bgcolor="#828282">
<td width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボタン＆モード-->
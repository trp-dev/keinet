<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<!--出力フォーマット-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="892">
<tr>
<td width="692" align="right">

<table border="0" cellpadding="0" cellspacing="0">

<c:if test="${ param.forward == 'i302' }">
<!-- モードの判別-->
<c:choose>
<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
<tr>
<td colspan="4" align="center">
	<select name="printStudent" class="text12" style="width:156;" onchange="document.forms[0].printStudentSub.selectedIndex=this.selectedIndex;">
	<option  value="1"<c:if test="${ form.printStudent == '1'}">selected</c:if>>選択中の生徒全員</option>
	<option  value="2"<c:if test="${ form.printStudent == '2'}">selected</c:if>>表示中の生徒一人</option>
	</select>
</td>
</tr>
<tr>
<td colspan="4"><img src="./shared_lib/img/parts/sp.gif" width="1" height="4" border="0" alt=""><br></td>
</tr>
</c:when>
<c:otherwise>
<input type="hidden" name="printStudent" value="2">
</c:otherwise>
</c:choose>
<!--/ モードの判別-->
</c:if>

<tr>
<c:if test="${ param.forward != 'i302' }">
<!-- モードの判別-->
<c:choose>
<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
<td>
	<select name="printStudent" class="text12" style="width:156;" onchange="document.forms[0].printStudentSub.selectedIndex=this.selectedIndex;">
	<option  value="1"<c:if test="${ form.printStudent == '1'}">selected</c:if>>選択中の生徒全員</option>
	<option  value="2"<c:if test="${ form.printStudent == '2'}">selected</c:if>>表示中の生徒一人</option>
	</select>
</td>
</c:when>
<c:otherwise>
<input type="hidden" name="printStudent" value="2">
</c:otherwise>
</c:choose>
<!--/ モードの判別-->
</c:if>
<c:if test="${ param.forward == 'i302' }">
<td><b class="text12">帳票種類：&nbsp;</b></td>
<td><select name="textFileFormat" class="text12" style="width:156;" onchange="document.forms[0].textFileFormatSub.selectedIndex=this.selectedIndex;changeTextFormatStatus(this.value);">
	<option value="1"<c:if test="${ form.textFileFormat == '1'}">selected</c:if>>受験大学スケジュール表</option>
	<option value="2"<c:if test="${ form.textFileFormat == '2'}">selected</c:if>>志望校判定</option>
	</select></td>
</c:if>
<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><b class="text12">出力フォーマット：&nbsp;</b></td>
<td><input type="checkbox" name="textFormat" value="1" onclick="document.forms[0].textFormatSub[0].checked=this.checked;" <c:if test="${ form.textFormat[0] == '1'}">checked</c:if>></td>
<td width="50"><span class="text12">先生用</span></td>
<td><input type="checkbox" name="textFormat" value="2" onclick="document.forms[0].textFormatSub[1].checked=this.checked;" <c:if test="${ form.textFormat[1] == '2'}">checked</c:if>></td>
<td><span class="text12">生徒用</span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="200" align="right"><span class="text12"><a href="javascript:openSample('<c:out value="${param.forward}" />')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4" align="absmiddle">サンプルの表示</a></span></td>
</tr>
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="6" cellspacing="1" width="892" bgcolor="#8CA9BB">
<tr>
<td bgcolor="#FFFFFF" align="center">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><a href="#here" onclick="printView();"><img src="./shared_lib/img/btn/hyouji_w.gif" width="114" height="35" border="0" alt="表示"></a></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""></td>
<td><a href="#here" onclick="printCheck();"><img src="./shared_lib/img/btn/print_w.gif" width="114" height="35" border="0" alt="印刷"></a></td>
</tr>
</table>
</td>
</tr>
</table>
<!--/出力フォーマット-->
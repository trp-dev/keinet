<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<%-- 2019/07/09 QQ)Tanioka ダウンロード変更対応 ADD START --%>
<script type="text/javascript" src="./js/jquery.blockUI.js"></script>
<SCRIPT type="text/javascript">
<!--
	<%@ include file="/jsp/script/download.jsp" %>

	function bunsekiMenuValidate() {
		var flag = 0;

		var form = document.forms[0];

		var sheet1 = form.printTarget[0]; //個人成績表
		var sheet2 = form.printTarget[1]; //教科分析シート
		var sheet3 = form.printTarget[2]; //科目別成績推移
		var sheet4 = form.printTarget[3]; //設問別成績推移

		if (sheet1.checked && !sheet1.disabled) { flag = 1; }
		if (sheet2.checked && !sheet2.disabled) { flag = 1; }
		if (sheet3.checked && !sheet3.disabled) { flag = 1; }
		if (sheet4.checked && !sheet4.disabled) { flag = 1; }

		if (flag == 0) {
			// 2020/2/13 QQ)Ooseto 共通テスト対応 UPD START
			//window.alert("印刷したい資料を選択してください。");
			window.alert("保存したい資料を選択してください。");
			// 2020/2/13 QQ)Ooseto 共通テスト対応 UPD END
		} else {
	   		printDialogPattern = "printDialog2";
			download();
		}
	}
//-->
</script>
<%-- 2019/07/09 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="4" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF">

<table border="0" cellpadding="0" cellspacing="0" width="868">
<tr>
<td width="672">
<!--メニュー-->
<table border="0" cellpadding="4" cellspacing="2" width="672">
<tr>
<td width="16%" bgcolor="#CDD7DD">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><b class="text12">分析メニュー</b></td>
</tr>
</table>
</td>

<%--■個人成績表--%>
<c:choose>
	<%--現画面--%>
	<c:when test="${param.forward == 'i101' || param.forward == 'i102'}">
		<td width="14%" bgcolor="#FFAD8C" align="center"><span class="text12">個人成績表</span></td>
	</c:when>
	<c:otherwise>
		<td width="14%" bgcolor="#F4E5D6" align="center"><span class="text12"><a href="javascript:submitForm('i101')">個人成績表</a></span></td>
	</c:otherwise>
</c:choose>

<%--■バランスチャート--%>
<c:choose>
	<%--現画面--%>
	<c:when test="${param.forward == 'i103'}">
		<td width="14%" bgcolor="#FFAD8C" align="center"><span class="text12">バランス<br>チャート</span></td>
	</c:when>
	<c:otherwise>
		<kn:choose>
			<%--受験学力測定--%>
			<kn:examNotWhen id="ability" var="${iCommonMap.examData}">
				<td width="14%" bgcolor="#F4E5D6" align="center"><span class="text12"><a href="javascript:submitForm('i103')">バランス<br>チャート</a></span></td>
			</kn:examNotWhen>
			<%--グレーアウト--%>
			<kn:otherwise>
				<td width="14%" bgcolor="#E1E6EB" align="center"><span class="text12">バランス<br>チャート</span></td>
			</kn:otherwise>
		</kn:choose>
	</c:otherwise>
</c:choose>

<%--■バランスチャート（合格者平均）--%>
<c:choose>
	<%--現画面--%>
	<c:when test="${param.forward == 'i104'}">
		<td width="14%" bgcolor="#FFAD8C" align="center"><span class="text12">バランス<br>チャート<br>（合格者平均）</span></td>
	</c:when>
	<c:otherwise>
		<kn:choose>
			<%--◆マーク高2[66]・記述高2[67]・学力測定以外のマーク(01)・記述(02)・私大模試(03)--%>
			<kn:examWhen id="patternI" var="${iCommonMap.examData}">
				<td width="14%" bgcolor="#F4E5D6" align="center"><span class="text12"><a href="javascript:submitForm('i104')">バランス<br>チャート<br>（合格者平均）</a></span></td>
			</kn:examWhen>
			<%--グレーアウト--%>
			<kn:otherwise>
				<td width="14%" bgcolor="#E1E6EB" align="center"><span class="text12">バランス<br>チャート<br>（合格者平均）</span></td>
			</kn:otherwise>
		</kn:choose>
	</c:otherwise>
</c:choose>

<%--■成績推移グラフ--%>
<c:choose>
	<%--現画面--%>
	<c:when test="${param.forward == 'i106'}">
		<td width="14%" bgcolor="#FFAD8C" align="center"><span class="text12">成績推移グラフ</span></td>
	</c:when>
	<c:otherwise>
		<kn:choose>
			<%--受験学力測定以外--%>
			<kn:examNotWhen id="ability" var="${iCommonMap.examData}">
				<td width="14%" bgcolor="#F4E5D6" align="center"><span class="text12"><a href="javascript:submitForm('i106')">成績推移グラフ</a></span></td>
			</kn:examNotWhen>
			<%--グレーアウト--%>
			<kn:otherwise>
					<td width="14%" bgcolor="#E1E6EB" align="center"><span class="text12">成績推移グラフ</span></td>
			</kn:otherwise>
		</kn:choose>
	</c:otherwise>
</c:choose>

<%--■科目別成績推移グラフ--%>
<c:choose>
	<%--現画面--%>
	<c:when test="${param.forward == 'i107'}">
		<td width="14%" bgcolor="#FFAD8C" align="center"><span class="text12">科目別<br>成績推移グラフ</span></td>
	</c:when>
	<c:otherwise>
		<kn:choose>
			<%--受験学力測定以外--%>
			<kn:examNotWhen id="ability" var="${iCommonMap.examData}">
				<td width="14%" bgcolor="#F4E5D6" align="center"><span class="text12"><a href="javascript:submitForm('i107')">科目別<br>成績推移グラフ</a></span></td>
			</kn:examNotWhen>
			<%--グレーアウト--%>
			<kn:otherwise>
				<td width="14%" bgcolor="#E1E6EB" align="center"><span class="text12">科目別<br>成績推移グラフ</span></td>
			</kn:otherwise>
		</kn:choose>
	</c:otherwise>
</c:choose>

<%--■設問別成績--%>
<c:choose>
	<%--現画面--%>
	<c:when test="${param.forward == 'i108'}">
		<td width="14%" bgcolor="#FFAD8C" align="center"><span class="text12">設問別成績</span></td>
	</c:when>
	<c:otherwise>
		<kn:choose>
			<%--受験学力測定・センターリサーチ以外--%>
			<kn:examWhen id="patternIV" var="${iCommonMap.examData}">
				<td width="14%" bgcolor="#F4E5D6" align="center"><span class="text12"><a href="javascript:submitForm('i108')">設問別成績</a></span></td>
			</kn:examWhen>
			<%--グレーアウト--%>
			<kn:otherwise>
				<td width="14%" bgcolor="#E1E6EB" align="center"><span class="text12">設問別成績</span></td>
			</kn:otherwise>
		</kn:choose>
	</c:otherwise>
</c:choose>

</tr>
<tr>
<td bgcolor="#CDD7DD">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><b class="text12">出力対象</b></td>
</tr>
</table>
</td>

<%--●連続個人成績表--%>
<td bgcolor="#E5EEF3" align="center">
	<input type="checkbox" name="printTarget" value="0"
		<c:if test="${form.printTarget[0] == '1'}">checked</c:if>
	>
</td>

<%--●教科分析シート--%>
<td colspan="3" width="42%" bgcolor="#E5EEF3" align="center">
	<input type="checkbox" name="printTarget" value="1"
		<c:if test="${form.printTarget[1] == '1'}">checked</c:if>
		<kn:choose>
			<%--受験学力測定--%>
			<kn:examWhen id="ability" var="${iCommonMap.examData}">disabled</kn:examWhen>
		</kn:choose>
	>
</td>

<%--●成績推移グラフ--%>
<td bgcolor="#E5EEF3" align="center">
	<input type="checkbox" name="printTarget" value="2"
		<c:if test="${form.printTarget[2] == '1'}">checked</c:if>
		<kn:choose>
			<%--受験学力測定--%>
			<kn:examWhen id="ability" var="${iCommonMap.examData}">disabled</kn:examWhen>
		</kn:choose>
	>
</td>

<%--●設問別成績グラフ--%>
<td bgcolor="#E5EEF3" align="center">
	<input type="checkbox" name="printTarget" value="3"
		<c:if test="${form.printTarget[3] == '1'}">checked</c:if>
		<kn:choose>
			<%--受験学力測定・センターリサーチ--%>
			<kn:examNotWhen id="patternIV" var="${iCommonMap.examData}">disabled</kn:examNotWhen>
		</kn:choose>
	>
</td>

</tr>
</table>
<!--メニュー-->
</td>
<td width="22" align="center"><img src="./shared_lib/img/parts/arrow_right_darkblue_b.gif" width="8" height="39" border="0" alt="→"></td>
<td width="172">
<!--印刷-->
<table border="0" cellpadding="0" cellspacing="0" width="172">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="172">
<tr valign="top">
<td width="170" bgcolor="#FBD49F" align="center">
<!--選択-->
<table border="0" cellpadding="0" cellspacing="0" width="157">
<tr>
<td width="157">

<!-- モードの判別-->
<c:choose>
<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
	<select name="printStudent_2" class="text12" style="width:157;" onChange="this.form.printStudent.value=this[this.selectedIndex].value">
	<option  value="1"<c:if test="${ form.printStudent == '1'}">selected</c:if>>選択中の生徒全員</option>
	<option  value="2"<c:if test="${ form.printStudent == '2'}">selected</c:if>>表示中の生徒一人</option>
	</select>
</c:when>

<c:otherwise>

</c:otherwise>
</c:choose>
<!--/ モードの判別-->

</td>
</tr>
<tr>
<td width="157"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/選択-->
<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="157">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="157">
<tr>
<td width="155" bgcolor="#FFFFFF" align="center"><a href="#here" onclick="bunsekiMenuValidate()"><img src="./shared_lib/img/btn/hozon_w.gif" width="124" height="36" border="0" alt="保存"></a><br></td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/印刷-->
</td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<%-- 2016/1/6 QQ)Hisakawa ��K�͉��C UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa ��K�͉��C UPD END --%>
	/********************************************************************************
	*** EXAM COMBO BOX **************************************************************
	********************************************************************************/
	var univString = new Array(
		<c:forEach var="nestedUnivData" items="${nestedUnivList}" varStatus="status">
			<c:if test="${status.index != 0}">,</c:if>
			new Array('<c:out value="${nestedUnivData.univCd}"/>','<c:out value="${nestedUnivData.univNameAbbr}"/>')
		</c:forEach>
	);
	var facultyString = new Array(
		<c:forEach var="nestedUnivData" items="${nestedUnivList}" varStatus="status">
			<c:if test="${status.index != 0}">,</c:if>
			new Array(
				<c:forEach var="facultyData" items="${nestedUnivData.facultyList}" varStatus="status2">
					<c:if test="${status2.index != 0}">,</c:if>
					new Array('<c:out value="${facultyData.facultyCd}"/>','<c:out value="${facultyData.facultyNameAbbr}"/>')
				</c:forEach>
			)
		</c:forEach>
	);
	var deptString = new Array(
		<c:forEach var="nestedUnivData" items="${nestedUnivList}" varStatus="status">
			<c:if test="${status.index != 0}">,</c:if>
			new Array(
				<c:forEach var="facultyData" items="${nestedUnivData.facultyList}" varStatus="status2">
					<c:if test="${status2.index != 0}">,</c:if>
					new Array(
						<c:forEach var="deptData" items="${facultyData.deptList}" varStatus="status3">
							<c:if test="${status3.index != 0}">,</c:if>
							new Array('<c:out value="${deptData.deptCd}"/>','<c:out value="${deptData.deptNameAbbr}"/>')
						</c:forEach>
					)
				</c:forEach>
			)
		</c:forEach>
	);
	function loadUnivList(){
		createList(document.forms[0].univCd, univString);
		loadFacultyList();
	}
	function loadFacultyList(){
		for(var i=0; i<univString.length; i++){
			if(document.forms[0].univCd.options[document.forms[0].univCd.selectedIndex].value == univString[i][0]){
				createList(document.forms[0].facultyCd, facultyString[i]);
			}
		}
		loadDeptList();
	}
	function loadDeptList(){
		for(var i=0; i<univString.length; i++){
			if(document.forms[0].univCd.options[document.forms[0].univCd.selectedIndex].value == univString[i][0]){
				for(var j=0; j<facultyString[i].length; j++){
					if(document.forms[0].facultyCd.options[document.forms[0].facultyCd.selectedIndex].value == facultyString[i][j][0]){
						createList(document.forms[0].deptCd, deptString[i][j]);
					}
				}
			}
		}

	}
	function createList(objList, objArray){
		objList.length = 0; // clear
	    var nMax = objArray.length;
	    var nLoop = 0;
	    for (nLoop = 0; nLoop < nMax; nLoop++){
	        // oAdd = document.createElement("<option style='background-color:#FFFFFF'>");
	        oAdd = document.createElement("option");
          	oAdd.setAttribute("style", "background-color:#FFFFFF");

	        objList.appendChild(oAdd);
	        <%-- 2015/12/24 QQ)Hisakawa ��K�͉��C UPD START --%>
	        <%-- objList.childNodes[nLoop].setAttribute('value', objArray[nLoop][0]); --%>
	        objList.children[nLoop].setAttribute('value', objArray[nLoop][0]);
	       	<%-- 2015/12/24 QQ)Hisakawa ��K�͉��C UPD END --%>

			if(nLoop == 0){
	        	<%-- 2015/12/24 QQ)Hisakawa ��K�͉��C UPD START --%>
				<%-- objList.childNodes[nLoop].setAttribute('selected', true); --%>
				objList.children[nLoop].setAttribute('selected', true);
	        	<%-- 2015/12/24 QQ)Hisakawa ��K�͉��C UPD END --%>
			}else{
	        	<%-- 2015/12/24 QQ)Hisakawa ��K�͉��C DEL START --%>
				<%--
				objList.childNodes[nLoop].setAttribute('selected', false);
				--%>
	        	<%-- 2015/12/24 QQ)Hisakawa ��K�͉��C DEL END --%>
			}

	        oAddx= document.createTextNode(objArray[nLoop][1]);
	        <%-- 2015/12/24 QQ)Hisakawa ��K�͉��C UPD START --%>
	        <%--
	        if(objList.childNodes[nLoop].firstChild  != undefined)
	            objList.childNodes[nLoop].removeChild(objList.childNodes[nLoop].firstChild);
	        objList.childNodes[nLoop].appendChild(oAddx);
	        --%>

	        if(objList.children[nLoop].firstChild  != undefined)
	            objList.children[nLoop].removeChild(objList.children[nLoop].firstChild);
	        objList.children[nLoop].appendChild(oAddx);
	        <%-- 2015/12/24 QQ)Hisakawa ��K�͉��C UPD END --%>
	    }
	}
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<div style="margin-top:12px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<c:choose>
	<c:when test="${param.forward == 'i101' || param.forward == 'i102' || param.forward == 'i103' || param.forward == 'i104' || param.forward == 'i105' || param.forward == 'i106' || param.forward == 'i107' || param.forward == 'i108' || param.forward == 'i109'}">
		<td width="48"><img src="./shared_lib/img/parts/icon_bunseki.gif" width="39" height="39" border="0" alt="成績分析"><br></td>
		<td width="309"><b class="text16" style="color:#657681">成績分析</b></td>
	</c:when>
	<c:when test="${param.forward == 'i201' || param.forward == 'i202' || param.forward == 'i203' || param.forward == 'i204' || param.forward == 'i205' || param.forward == 'iOnJudge'}">
		<td width="48"><img src="./shared_lib/img/parts/icon_daigaku.gif" width="39" height="39" border="0" alt="志望大学判定"><br></td>
		<td width="309"><b class="text16" style="color:#657681">志望大学判定</b></td>
	</c:when>
	<c:when test="${param.forward == 'i301' || param.forward == 'i302' || param.forward == 'i303' || param.forward == 'i304' || param.forward == 'i305' || param.forward == 'i306'}">
		<td width="48"><img src="./shared_lib/img/parts/icon_techou.gif" width="39" height="39" border="0" alt="個人手帳"><br></td>
		<td width="309"><b class="text16" style="color:#657681">個人手帳</b></td>
	</c:when>
	<c:when test="${param.forward == 'i401'}">
		<td width="48"><img src="./shared_lib/img/parts/icon_print.gif" width="39" height="39" border="0" alt="面談資料かんたん印刷"><br></td>
		<td width="309"><b class="text16" style="color:#657681">面談資料かんたん印刷</b></td>
	</c:when>
	<c:when test="${param.forward == 'i501' || param.forward == 'i502'}">
		<td width="48"><img src="./shared_lib/img/parts/icon_memo.gif" width="39" height="39" border="0" alt="面談メモ"><br></td>
		<td width="309"><b class="text16" style="color:#657681">面談メモ</b></td>
	</c:when>
	<c:otherwise>
		<font color="red"><b>エラー</b></font>
	</c:otherwise>
</c:choose>
<td width="542">
<!--選択方法-->
<%-- 2016/05/06 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <table border="0" cellpadding="0" cellspacing="0" width="542" height="38"> --%>
<table border="0" cellpadding="0" cellspacing="0" width="542" height="38" style="padding-top:1px; margin-bottom:1px;">
<%-- 2016/05/06 QQ)Hisakawa 大規模改修 UPD END   --%>
<tr>
<td rowspan="3" width="8"><img src="./individual/img/tbl_l.gif" width="8" height="38" border="0" alt=""><br></td>
<%-- 2016/05/09 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td width="542" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="542" height="1" border="0" alt=""><br></td> --%>
<td width="542" height="0" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="542" height="1" border="0" alt=""><br></td>
<%-- 2016/05/09 QQ)Hisakawa 大規模改修 UPD END   --%>
<td rowspan="3" width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="38" border="0" alt=""><br></td>
</tr>
<tr>
<td width="542" bgcolor="#FAFAFA">
<table border="0" cellpadding="0" cellspacing="0" width="542">
<tr height="30">

<!-- モードの判別-->
<c:choose>
<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
	<td width="180" bgcolor="#8CA9BB" align="center"><b class="text14" style="color:#FFFFFF">選択中の生徒&nbsp;→&nbsp;
	<c:out value="${sessionScope['iCommonMap'].selectedIndividualsSize}" />人</b></td>
</c:when>
<c:otherwise>
	<td width="180" bgcolor="#8CA9BB" align="center"><b class="text14" style="color:#FFFFFF">選択中の生徒</b></td>
</c:otherwise>
</c:choose>
<!--/ モードの判別-->

<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="30" border="0" alt=""><br></td>
<td width="350" bgcolor="#F9EEE5">
<table border="0" cellpadding="2" cellspacing="0" width="350">
<tr height="30">
<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
<%--0323 kondo　クラスコンボ　生徒選択コンボ追加--%>

<c:choose>
<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
<td width="70" align="center"><b class="text12-lh"><span id="personGrade">不明</span>
<td width="80"><select name="selectClass" class="text12-lh" style="width:77px;" onchange="createStudentList();changeAndGo();"></select></td>
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD START --%>
<%--
<td width="30"><input type="button" name="leftArrow" value="&nbsp;＜&nbsp;" class="text12" style="width:27px;height:19px;" onclick="leftAndGo();"></td>
<td width="160"><select name="selectStudent" class="text12-lh" style="width:157px;" onchange="changeAndGo();"></select></td>
<td width="30"><input type="button" name="rightArrow" value="&nbsp;＞&nbsp;" class="text12" style="width:27px;height:19px;" onclick="rightAndGo();"></td>
--%>
<td width="30"><input type="button" name="leftArrow" value="&nbsp;＜&nbsp;" class="text12" style="width:27px;height:19px;padding:0px;" onclick="leftAndGo();"></td>
<td width="160"><select name="selectStudent" class="text12-lh" style="width:157px;" onchange="changeAndGo();"></select></td>
<td width="30"><input type="button" name="rightArrow" value="&nbsp;＞&nbsp;" class="text12" style="width:27px;height:19px;padding:0px;" onclick="rightAndGo();"></td>
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD END   --%>
</c:when>
<c:otherwise>
<td width="347" align="center"><b class="text12-lh">
<c:out value="${sessionScope['iCommonMap'].selectedIndividuals[0].studentGrade}" />年
<c:out value="${sessionScope['iCommonMap'].selectedIndividuals[0].studentClass}" />ｸﾗｽ
<c:out value="${sessionScope['iCommonMap'].selectedIndividuals[0].studentClassNo}" />番
<c:out value="${sessionScope['iCommonMap'].selectedIndividuals[0].studentNameKana}" />
</b></td>

</c:otherwise>
</c:choose>

<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="30" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<tr>
<%-- 2016/05/06 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td width="512" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="512" height="1" border="0" alt=""><br></td> --%>
<td width="512" height="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="512" height="1" border="0" alt=""><br></td>
<%-- 2016/05/06 QQ)Hisakawa 大規模改修 UPD END   --%>
</tr>
</table>
<!--/選択方法-->
</td>
</tr>
</table>
</div>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
var _isBundle = true;
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>

// 個人成績共通項目-印刷確認ダイアログ
function printDialog(isBundle){

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 UPD START --%>
//	returnValue = showModalDialog(
//			"<c:url value="PrintDialog" />",
//			null,
//			"status:no;help:no;scroll:no;dialogWidth:665px;dialogHeight:450px");

	// 印刷する
//	if (returnValue == 2) {
//		document.forms[0].printStatus.value = isBundle ? "3" : "2";
//		printSheet();
	// 印刷して保存
//	} else if (returnValue == 1) {
//		document.forms[0].printStatus.value = isBundle ? "4" : "8";
//		printSheet();
	// キャンセル
//	} else {
//		return;
//	}

    _isBundle = isBundle;
    printDialogPattern = "printDialog1";
    // 印刷確認ダイアログ　オープン
    $(printDialogObj).dialog('open');

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 UPD END   --%>
}

// 個人成績共通項目-一括印刷確認ダイアログ
function printAllDialog(){

	var flag = 0;

	var form = document.forms[0];

	var sheet1 = form.printTarget[0]; //個人成績表
	var sheet2 = form.printTarget[1]; //教科分析シート
	var sheet3 = form.printTarget[2]; //科目別成績推移
	var sheet4 = form.printTarget[3]; //設問別成績推移

	if (sheet1.checked && !sheet1.disabled) { flag = 1; }
	if (sheet2.checked && !sheet2.disabled) { flag = 1; }
	if (sheet3.checked && !sheet3.disabled) { flag = 1; }
	if (sheet4.checked && !sheet4.disabled) { flag = 1; }

	if (flag == 0) {
		<%-- 2020/2/13 QQ)Ooseto 共通テスト対応 UPD START --%>
		//window.alert("印刷したい資料を選択してください。");
		window.alert("保存したい資料を選択してください。");
		<%-- 2020/2/13 QQ)Ooseto 共通テスト対応 UPD END --%>
	} else {

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 UPD START --%>
//		returnValue = showModalDialog(
//				"<c:url value="PrintDialog" />",
//				null,
//				"status:no;help:no;scroll:no;dialogWidth:665px;dialogHeight:450px");

		// 印刷する
//		if (returnValue == 2) {
//			document.forms[0].printStatus.value = "3";
//			printSheet();
		// 印刷して保存
//		} else if (returnValue == 1) {
//			document.forms[0].printStatus.value = "4";
//			printSheet();
		// キャンセル
//		} else {
//			return;
//		}

   		 printDialogPattern = "printDialog2";
		// 印刷確認ダイアログ　オープン
		$(printDialogObj).dialog('open');

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 UPD END   --%>
	}
}

function printSheet() {

	var form = document.forms[0];

	<%-- 無効解除 --%>
	var array = new Array();
	for (var i = 0; i < form.elements.length; i++) {
		if (form.elements[i].disabled) {
			form.elements[i].disabled = false;
			array[array.length] = form.elements[i];
		}
	}

	form.forward.value = "sheet";
	form.backward.value = "<c:out value="${param.forward}" />";
	form.printFlag.value = form.printStatus.value;
	form.target = "_self";
	form.submit();

	<%-- 無効復元 --%>
	for (var i = 0; i < array.length; i++) {
		array[i].disabled = "disabled";
	}
}

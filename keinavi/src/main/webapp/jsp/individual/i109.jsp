<%--0502 K.Kondo 新テストの場合全国平均を出さない--%>
<%--0524 K.Kondo 対象模試が新テストの場合は、アプレットの全国平均ラインを出さない--%>
<%--0130 K.Kondo 設問タブの幅調節--%>
<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.keinavi.data.individual.QuestionData" %>
<%@ page import="jp.co.fj.keinavi.data.individual.SubjectData" %>
<%@ page import="jp.co.fj.keinavi.data.individual.ExaminationData" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Collection" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<jsp:useBean id="i109Bean" scope="request" class="jp.co.fj.keinavi.beans.individual.I109Bean" />
<jsp:useBean id="form" scope="request" class="jp.co.fj.keinavi.forms.individual.I109Form" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<SCRIPT type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    /**
     * サブミット
     */
    function submitForm(forward) {
        <%--スクロール位置 0426 K.Kondo--%>
        if(forward=='<c:out value="${param.forward}" />') {
            document.forms[0].scrollX.value = document.body.scrollLeft;
            document.forms[0].scrollY.value = document.body.scrollTop;
        } else {
            document.forms[0].scrollX.value = 0;
            document.forms[0].scrollY.value = 0;
        }
        document.forms[0].target = "_self";
        document.forms[0].forward.value = forward;
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 戻るボタン
     */
    function submitBack() {
        document.forms[0].target = "_self";
        document.forms[0].forward.value = "i003";
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 科目ボタンサブミット
     */
    function pushSubButton(subcd, subnm) {
        document.forms[0].targetSubCode.value = subcd;
        document.forms[0].targetSubName.value = subnm;
        submitForm('<c:out value="${param.forward}" />');
    }
    /**
     *
     */
    function pushSubmitButton() {
        document.forms[0].targetSubCode.value = "<c:out value="${form.targetSubCode}" />";
        document.forms[0].targetSubName.value = "<c:out value="${form.targetSubName}" />";
        submitForm('<c:out value="${param.forward}" />');
    }
    /**
     *
     */
    function listnone() {
        <%
            Collection csub2 = i109Bean.getSubDatas();
            java.util.Iterator cd2 = csub2.iterator();
            boolean dspflg = false;
            while(cd2.hasNext()) {
                SubjectData data = (SubjectData)cd2.next();
                String temp = data.getSubjectCd();
                if(temp.equals(form.getTargetSubCode())) {
                    dspflg = true;
                    break;
                }
            }
            if(!dspflg) {
        %>
            document.getElementById("list").style.display = "none";
        <%}%>
    }
    /**
     * フォームデータ
     */
    <%
    if(i109Bean.isComboData()) {
        out.println(i109Bean.getExamComboData().getYearString());
    } else {
        out.println("var yearString = new Array();");
    }
    %>
    var examString = new Array();
    <%
        if(i109Bean.isComboData()) {
            for(int i=0; i<i109Bean.getExamComboData().getExamStrings().length; i++){
                out.println(i109Bean.getExamComboData().getExamStrings()[i]);
            }
        }
    %>
    /**
     *
     */
    function loadTargetExamYear(){
        create(document.forms[0].targetExamYear, yearString);
        loadTargetExamCode();
    }
    /**
     *
     */
    function loadTargetExamCode(){

        for(var i=0; i<yearString.length; i++){
            if(document.forms[0].targetExamYear.options[document.forms[0].targetExamYear.selectedIndex].value == yearString[i][0]){
                create(document.forms[0].targetExamCode, examString[i]);
            }
        }
    }
    /**
     *
     */
<%-- 2015/12/24 QQ)Hisakawa 大規模改修 UPD START --%>
/*
    function create(objList, objArray){
        var nMax = objArray.length;
        var nLoop = 0;
        for (nLoop = 0; nLoop < nMax; nLoop++){
            oAdd = document.createElement('OPTION');
            if(objList.childNodes[nLoop]  != undefined)
                objList.removeChild(objList.childNodes[nLoop]);
            objList.appendChild(oAdd);
            objList.childNodes[nLoop].setAttribute('value', objArray[nLoop][0]);

            if(objList == document.forms[0].elements['targetExamYear'] && objArray[nLoop][0] == '<%=i109Bean.getTargetExamYear()%>'){
                objList.childNodes[nLoop].setAttribute('selected', true);
            }else if(objList == document.forms[0].elements['targetExamCode'] && objArray[nLoop][0] == '<%=i109Bean.getTargetExamCode()%>'){
                objList.childNodes[nLoop].setAttribute('selected', true);
            }

            oAddx= document.createTextNode(objArray[nLoop][1]);
            if(objList.childNodes[nLoop].firstChild  != undefined)
                objList.childNodes[nLoop].removeChild(objList.childNodes[nLoop].firstChild);
            objList.childNodes[nLoop].appendChild(oAddx);
        }
        objList.length=nLoop;
    }
*/

    function create(objList, objArray){
        var nMax = objArray.length;
        var nLoop = 0;
        for (nLoop = 0; nLoop < nMax; nLoop++){
            oAdd = document.createElement('OPTION');
            if(objList.children[nLoop]  != undefined)
                objList.removeChild(objList.children[nLoop]);
            objList.appendChild(oAdd);
            objList.children[nLoop].setAttribute('value', objArray[nLoop][0]);

            if(objList == document.forms[0].elements['targetExamYear'] && objArray[nLoop][0] == '<%=i109Bean.getTargetExamYear()%>'){
                objList.children[nLoop].setAttribute('selected', true);
            }else if(objList == document.forms[0].elements['targetExamCode'] && objArray[nLoop][0] == '<%=i109Bean.getTargetExamCode()%>'){
                objList.children[nLoop].setAttribute('selected', true);
            }

            oAddx= document.createTextNode(objArray[nLoop][1]);
            if(objList.children[nLoop].firstChild  != undefined)
                objList.children[nLoop].removeChild(objList.children[nLoop].firstChild);
            objList.children[nLoop].appendChild(oAddx);
        }
        objList.length=nLoop;
    }
<%-- 2015/12/24 QQ)Hisakawa 大規模改修 UPD END   --%>
    /**
     * 初期化
     */
    function init(){
        startTimer();
        <%--スクロール位置 0426 K.Kondo--%>
        window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

        loadTargetExamYear();

        //アプレットに表示する内容が無い時は、下のテーブルも消す
        listnone();

        //ロード中
        <%@ include file="/jsp/script/loading.jsp" %>

        <%-- アプレットを本来の位置へ移動する --%>
        $('#AppletLayer').children().prependTo('#AppletCell');
    }
//-->
</SCRIPT>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="AppletLayer" style="position:absolute;top:250px;left:450px;width:1px;height:1px;overflow:hidden;">
<!-- アプレット部：サーブレットよりVALUEを受けとるようにすること=========== -->
<%-- 2016/01/18 QQ)Hisakawa 大規模改修 DEL START --%>
<%--
<c:forEach var="data" items="${i109Bean.recordSet}">
<APPLET CODEBASE="./applets/" ARCHIVE="Applet.jar" CODE="LineGraphAppletQ" id="rc" WIDTH="848" HEIGHT="320">

<%
    //科目タブ切り替え
    String dispSelect = "";	//グラフ表示／非表示

    Collection csub = i109Bean.getSubDatas();
    java.util.Iterator cd = csub.iterator();
    boolean flag = false;
    while(cd.hasNext()) {
        SubjectData data = (SubjectData)cd.next();
        String temp = data.getSubjectCd();
        if(temp.equals(form.getTargetSubCode())) {
            flag = true;
            break;
        }
    }
    if(flag==true){
        dispSelect = "on";
        form.setDispSelect("on");
    }else{
        dispSelect = "off";
        form.setDispSelect("off");
    }

%>

    <param name="examName" value="<c:out value="${data.examName}" />">
    <param name="dispSelect" value="<%=dispSelect%>">
    <param name="yTitle" value="得点率％">
    <c:choose>
    <c:when test="${iCommonMap.targetExamTypeCode != '31'}">
    <param name="LineItemNum" value="2">
    </c:when>
    <c:otherwise>
    <param name="LineItemNum" value="1">
    </c:otherwise>
    </c:choose>
    <param name="LineItemNameS" value="対象生徒">
    <param name="LineItemName" value="校内平均<c:if test="${iCommonMap.targetExamTypeCode != '31'}">,全国平均</c:if><c:if test="${form.dispScholarLevel != ''}">,<c:out value="${form.dispScholarLevel}"/>レベル</c:if>">
    <param name="examValueS" value="<c:out value="${data.examValueS}" />">
    <param name="examValue0" value="<c:out value="${data.examValue0}" />">
    <c:if test="${iCommonMap.targetExamTypeCode != '31'}">
    <param name="examValue1" value="<c:out value="${data.examValue1}" />">
    </c:if>
    <param name="examValueL" value="<c:out value="${data.examValueL}" />">
    <param name="BarSize" value="10">
    <param name="colorDAT" value="1,0,13,14,13,14,2">
</APPLET>
<br>
</c:forEach>
<%if(i109Bean.getRecordSet().size() == 0) {%>
<APPLET CODEBASE="./applets/" ARCHIVE="Applet.jar" CODE="LineGraphAppletQ" id="rc" WIDTH="848" HEIGHT="320">
    <param name="examName" value="">
    <param name="dispSelect" value="off">
    <param name="yTitle" value="">
    <param name="LineItemNum" value="0">
    <param name="LineItemNameS" value="">
    <param name="LineItemName" value="">
    <param name="examValueS" value="">
    <param name="examValue0" value="">
    <param name="examValue1" value="">
    <param name="examValueL" value="">
    <param name="BarSize" value="10">
    <param name="colorDAT" value="1,0,13,14,13,14,2">
</APPLET>
<%}%>
--%>
<%-- 2016/01/18 QQ)Hisakawa 大規模改修 DEL END   --%>
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I109Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="targetPersonId" value="<c:out value="${sessionScope['iCommonData'].targetPersonId}" />">
<input type="hidden" name="targetSubCode" value="<c:out value="${form.targetSubCode}" />">
<input type="hidden" name="targetSubName" value="<c:out value="${form.targetSubName}" />">
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="863" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="871" bgcolor="#FFFFFF" align="center">

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="832">

<table border="0" cellpadding="0" cellspacing="0" width="832">
<tr valign="top">
<td colspan="4" width="832" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="832" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="832" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="816" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">設問別成績　※過回模試グラフ</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="832" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="832" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--模試の種類-->
<table border="0" cellpadding="0" cellspacing="0" width="832">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="37" border="0" alt=""><br></td>
<td width="848">

<table border="0" cellpadding="0" cellspacing="3" width="832">
<tr>
<td width="832">
<table border="0" cellpadding="0" cellspacing="0" width="832">
<tr>
<td width="369" bgcolor="#E1E6EB">
    <table border="0" cellpadding="3" cellspacing="0">
    <tr>
    <td colspan="2"><b class="text14">表示する過回模試の選択：</b></td>
    </tr>
    <tr>
    <td><span class="text12">対象年度</span></td>
    <td><SELECT name="targetExamYear" style="width:55px;" id="targetExamYear" onchange="loadTargetExamCode()" class="text12"></SELECT></td>
    </tr>
    <tr>
    <td><span class="text12">対象模試</span></td>
    <td><SELECT name="targetExamCode" style="width:290px;" id="targetExamCode" onchange="" class="text12"></SELECT></td>
    </tr>
    </table>
</td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="31" border="0" alt=""><br></td>
<td bgcolor="#E1E6EB" width="398">
    <table>
    <tr>
    <td celspan="2" align="left"><b class="text12">&lt;&lt;オプション&gt;&gt;</b></td>
    </tr>
    <tr>
    <td>
    <table width="100%">
    <td align="left"><span class="text12">&nbsp;対象生徒の学力レベル：<c:out value="${i109Bean.targetScholarLevel}"/></span></td>
    <td align="right"><span class="text12">表示学力レベル：&nbsp;&nbsp;</span>
        <select name="dispScholarLevel" class="text12">
            <option value=""></option>
            <c:set var="level" value="" />
            <c:forEach var="levellist" items="${ i109Bean.levelDatas }">
            <option value="<c:out value="${ levellist }" />" <c:if test="${ levellist eq form.dispScholarLevel }"> selected</c:if> ><c:out value="${ levellist }" /></option>
            </c:forEach>
        </select>&nbsp;&nbsp;
    </td>
    </table>
    </td>
    </tr>
    <tr>
    <td celspan="2"><span class="text10">※これらのオプション項目を変更した場合は、[再表示]ボタンをクリックしてください。</span></td>
    </tr>
    </table>
</td>

<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="31" border="0" alt=""><br></td>
<td width="71" bgcolor="#F4E5D6" align="center"><input type="button" value="再表示" class="text12" style="width:60px;" onclick="javascript:pushSubmitButton()"></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="37" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="848" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/模試の種類-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--メニュー切り替え-->
<div align="left">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<%--060130 K.Kondo 設問タブの幅調節 start--%>
<c:set var="tabNum" value="0" />
<c:forEach var="subData" items="${i109Bean.subDatas}" varStatus="status">
    <c:set var="tabNum" value="${tabNum + 1}" />
</c:forEach>

<c:choose>
    <c:when test="${ tabNum <= 9 }">
        <c:set var="colWidth" value="${ 90 }" />
    </c:when>
    <c:otherwise>
        <c:set var="colWidth" value="${ (838 - (tabNum - 1) * 4) / tabNum }" />
    </c:otherwise>
</c:choose>

<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="24" border="0" alt=""><br></td>
<c:forEach var="subData" items="${i109Bean.subDatas}" varStatus="status">
    <c:if test="${status.index != 0}">
            <td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
    </c:if>
    <c:choose>
        <c:when test="${form.targetSubCode eq subData.subjectCd}">
            <td width="<c:out value="${ colWidth }" />" height="24">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
            <td bgcolor="#FFAD8C">
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
            <td width="100%" height="22" bgcolor="#FFAD8C" align="center"><span class="text12"><c:out value="${subData.subjectName}" /></span>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            </td>
        </c:when>
        <c:otherwise>
            <td width="<c:out value="${ colWidth }" />" height="24">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
            <td bgcolor="#8CA9BB">
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
            <td width="100%" height="22" bgcolor="#E5EEF3" align="center"><span class="text12">
            <a href="javascript:pushSubButton('<c:out value="${subData.subjectCd}" />', '<c:out value="${subData.subjectName}" />')">
            <c:out value="${subData.subjectName}" /></a></span>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            </td>
        </c:otherwise>
    </c:choose>
</c:forEach>
<%--0130 K.Kondo 設問タブの幅調節 end--%>
</tr>
</table>


<!-----メニュー2段目
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>


<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">英語</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">数学IA</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">数学IIB</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">国語I</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">物理IA</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">化学IA</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">地理A</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">地理A</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">地理A</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">地理A</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="69" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="69">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="69">
<tr>
<td width="67" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="#">地理A</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="49"><img src="./shared_lib/img/parts/sp.gif" width="49" height="24" border="0" alt=""><br></td>
</tr>
</table>
/メニュー2段目------------>

<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="839" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""></td>
<td bgcolor="#FFAD8C"><img src="./shared_lib/img/parts/sp.gif" width="839" height="2" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/メニュー切り替え-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--グラフイメージ-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<%-- 2016/01/18 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td width="848" id="AppletCell"> --%>
<td width="848">
<%-- 2016/02/29 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<img src="./GraphServlet" width="848" height="320" border="0">
  --%>
<img src="<c:url value="./GraphServlet" />"  width="848" height="320" border="0">
<%-- 2016/02/29 QQ)Nishiyama 大規模改修 UPD END   --%>
<%-- 2016/01/18 QQ)Hisakawa 大規模改修 UPD END   --%>
</td>
</tr>
</table>
<!--/グラフイメージ-->
<!-- ======================================================================== -->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--リスト-->
<div id='list'>
<c:forEach var="data" items="${i109Bean.recordSet}">
<table border="0" cellpadding="5" cellspacing="2" width="852">
<!--項目-->
<!--内容-->
<c:forEach var="x" begin="0" end="9" step="1">
    <c:choose>
        <c:when test="${x == 0}">
            <tr height="22" bgcolor="#CDD7DD">
        </c:when>
        <c:otherwise>
            <tr height="36">
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${x == 0}">
            <td colspan="2" width="10%"><span class="text12">設問番号</span></td>
        </c:when>
        <c:when test="${x == 1}">
            <td colspan="2" bgcolor="#CDD7DD"><span class="text12">内容</span></td>
        </c:when>
        <c:when test="${x == 2}">
            <td colspan="2" bgcolor="#CDD7DD"><span class="text12">配点</span></td>
        </c:when>
        <c:when test="${x == 3}">
            <td rowspan="2" width="6%" bgcolor="#CDD7DD" nowrap><span class="text12">対象生徒</span></td>
            <td width="4%" bgcolor="#CDD7DD" nowrap><span class="text12">得点</span></td>
        </c:when>
        <c:when test="${x == 4}">
            <td width="4%" bgcolor="#CDD7DD" nowrap><span class="text12">得点率</span></td>
        </c:when>
        <c:when test="${x == 5}">
            <td colspan="2" width="10%" bgcolor="#CDD7DD"><span class="text12"><c:out value="${form.dispScholarLevel}"/>レベル得点率</span></td>
        </c:when>
        <c:when test="${x == 6}">
            <td rowspan="2" width="6%" bgcolor="#CDD7DD" nowrap><span class="text12">校内平均</span></td>
            <td width="4%" bgcolor="#CDD7DD" nowrap><span class="text12">得点</span></td>
        </c:when>
        <c:when test="${x == 7}">
            <td width="4%" bgcolor="#CDD7DD" nowrap><span class="text12">得点率</span></td>
        </c:when>
        <c:when test="${x == 8 && iCommonMap.targetExamTypeCode != '31'}">
            <td rowspan="2" width="6%" bgcolor="#CDD7DD" nowrap><span class="text12">全国平均</span></td>
            <td width="4%" bgcolor="#CDD7DD" nowrap><span class="text12">得点</span></td>
        </c:when>
        <c:when test="${x == 9 && iCommonMap.targetExamTypeCode != '31'}">
            <td width="4%" bgcolor="#CDD7DD" nowrap><span class="text12">得点率</span></td>
        </c:when>
    </c:choose>
    <c:forEach var="y" begin="0" end="10" step="1">
        <c:choose>
            <c:when test="${x == 0}">
            <td bgcolor="#E1E6EB" align="right" width="8%">
            <span class="text12">
                <c:if test="${form.dispSelect == 'on'}"><c:out value="${data.tableData[x][y]}" /></c:if>
            </span></td>
            </c:when>
            <c:when test="${x == 1}">
                <td bgcolor="#F4E5D6" width="8%"><span class="text12"><c:out value="${data.tableData[x][y]}" /></span></td>
            </c:when>
            <c:otherwise>
                <c:if test="${!(x > 6 && iCommonMap.targetExamTypeCode == '31')}">
                    <td bgcolor="#F4E5D6" align="right" width="8%"><span class="text12"><c:out value="${data.tableData[x][y]}" /></span></td>
                </c:if>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</tr>
</c:forEach>
<!--/内容-->
</table>
</c:forEach>
</div>
<!--/リスト-->

<!--ボタン-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td align="center"><input type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;" onclick="window.close()"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw_01.jsp" %>
<!--/FOOTER-->
</form>
</div>

</body>
</html>

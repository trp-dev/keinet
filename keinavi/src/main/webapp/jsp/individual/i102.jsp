<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<jsp:useBean id="i102Bean" scope="request" class="jp.co.fj.keinavi.beans.individual.I102Bean" />
<jsp:useBean id="form" scope="request" class="jp.co.fj.keinavi.forms.individual.I102Form" />
<%@ page import="jp.co.fj.keinavi.data.individual.ExaminationData" %>
<%@ page import="jp.co.fj.keinavi.data.individual.CandidateRatingData" %>
<%@ page import="jp.co.fj.keinavi.data.help.HelpList" %>
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script language="JavaScript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>
	<%@ include file="/jsp/script/open_sample.jsp" %>
	<%@ include file="/jsp/script/submit_openOnepoint.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
    <%@ include file="/jsp/script/clear_disabled.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/printDialog_close.jsp" %>
	 var printDialogPattern = "";
	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

	<%-- 対象模試リスト --%>
	<c:set var="examList" value="${ iCommonMap.examComboData.examList }" />

	/**
	 * サブミット
	 */
	function submitForm(forward) {
		clearDisabled();
		<%--スクロール位置 0426 K.Kondo--%>
		if(forward=='i101' || forward=='i102') {
			document.forms[0].scrollX.value = document.body.scrollLeft;
			document.forms[0].scrollY.value = document.body.scrollTop;
		} else {
			document.forms[0].scrollX.value = 0;
			document.forms[0].scrollY.value = 0;
		}
        document.getElementById("MainLayer").style.visibility = "hidden";
        document.getElementById("MainLayer").style.top = -2000;
        document.getElementById("LoadingLayer").style.visibility = "visible";
		document.forms[0].target = "_self";
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 戻るボタン
	 */
	function submitBack() {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "i003";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * アプレットに表示する内容が無い時は、下のテーブルも消す
	 */
	function listnone() {
		<%if(i102Bean.getPersonData().getExaminationDatas() != null) {%>
			if(<%=i102Bean.getPersonData().getExaminationDatas().size()%> <= 0){
				document.getElementById("list").style.display = "none";
			}
		<%} else {%>
			document.getElementById("list").style.display = "none";
		<%}%>
	}
	/**
	 * 生徒切り替え
	 */
	<%@ include file="/jsp/individual/include/student_rotator.jsp" %>
	/**
	 * 印刷ダイアログ
	 */
	<%@ include file="/jsp/individual/include/printDialog.jsp" %>
	/**
	 * 印刷チェック
	 */
	function printCheck() {
		printDialog();
	}
	/**
	 * 一括印刷チェック
	 */
	function printAllCheck() {
		printAllDialog();
	}
	// 表示
	function printView() {
		document.forms[0].printStatus.value = "1";
		printSheet();
	}
	/**
	 * 初期化
	 */
	function init(){
		startTimer();
		<%--スクロール位置 0426 K.Kondo--%>
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

		//1.選択中の生徒まで回す
		findStudent();
		//2.選択中の生徒が一人ならArrowボタンを無効化する
		if(getStudentNum() == 1) disableArrows();

		//学年選択デフォルト
		for(i=0; i<document.forms[0].targetGrade.options.length; i++){
			if(document.forms[0].targetGrade.options[i].value == '<%=form.getTargetGrade()%>'){
				document.forms[0].targetGrade.options[i].selected = true;
			}
		}
		//模試種類デフォルト
		for(i=0; i<document.forms[0].targetExamType.options.length; i++){
			if(document.forms[0].targetExamType.options[i].value == '<%=form.getTargetExamType()%>'){
				document.forms[0].targetExamType.options[i].selected = true;
			}
		}
		//選択中生徒の学年によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
		gradeChange();

		//アプレットに表示する内容が無い時は、下のテーブルも消す
		listnone();

		//ロード中
		<%@ include file="/jsp/script/loading.jsp" %>
	}
//-->
</script>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I102Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="targetYear" value="<c:out value="${iCommonMap.targetExamYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${iCommonMap.targetExamCode}" />">
<input type="hidden" name="printStatus" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->


<!--成績分析-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--分析メニュー-->
<%@ include file="/jsp/individual/include/bunseki_menu.jsp" %>
<!--/分析メニュー-->

<!--概要-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="12" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="698">
<table border="0" cellpadding="0" cellspacing="0" width="698">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="682">

<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td colspan="5" width="682" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="682" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="682" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="666" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">個人成績表</b></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="682" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="682" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="150" valign="bottom">
<table border="0" cellpadding="0" cellspacing="0" width="150">
<tr>
<td align="center" height="40"><input type="button" class="text12" onclick="openSample('<c:out value="${param.forward}" />');" value="サンプルの表示"></td>
</tr>
<tr>
<td bgcolor="#8CA9BB" height="2"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
</table>
<!--/大タイトル-->

<!--模試の種類-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="360">
	<table border="0" cellpadding="3" cellspacing="3" width="360" height="76">
	<tr>
	<td bgcolor="#E1E6EB">
		<table border="0" cellpadding="3" cellspacing="0">
		<tr>
		<td><span class="text12">対象年度：</span></td>
		<td><%@ include file="/jsp/individual/include/targetExamYear.jsp" %></td>
		</tr>
		<tr>
		<td><span class="text12">対象模試：</span></td>
		<td><%@ include file="/jsp/individual/include/targetExamCode.jsp" %></td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="482">
	<table border="0" cellpadding="3" cellspacing="3" width="482" height="76">
	<tr>
	<td bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="0" width="390">
		<tr>
		<td>
			<div><b class="text12">&lt;&lt;オプション&gt;&gt;</b></div>
			<table border="0" cellpadding="3" cellspacing="0">
			<tr>
			<td><span class="text12">表示する学年：</span></td>
			<td><select name="targetGrade" size="1" class="text12">
						<option value="1">現学年のみ</option>
						<option value="2">全ての学年</option></select></td>
			<td><span class="text12">模試の種類：</span></td>
			<td><select name="targetExamType" size="1">
						<option value="1">全ての種類</option>
						<option value="2">同系統模試のみ</option></select></td>
			</tr>
			</table>
			<div style="margin-top:3px;"><span class="text10">※これらのオプション項目を変更した場合は、［再表示］ボタンをクリックしてください。</span></div>
		</td>
		</tr>
		</table>
	</td>
	<td width="71" bgcolor="#F4E5D6" align="center"><input type="button" value="再表示" class="text12" style="width:60px;" onclick="submitForm('i102');"></td>
	</tr>
	</table>
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="5" width="848" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/模試の種類-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--メニュー切り替え-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="159" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="159">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="159">
<tr>
<td width="157" height="22" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="javascript:submitForm('i101');">科目別成績</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="159" height="24" bgcolor="#FFAD8C" align="center"><span class="text12">志望大学別成績</span></td>
<td width="526"><img src="./shared_lib/img/parts/sp.gif" width="526" height="24" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td bgcolor="#FFAD8C"><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/メニュー切り替え-->

<!--リスト-->
<div id="list">
<table border="0" cellpadding="0" cellspacing="2" width="852">
<!--項目-->
<tr height="22" bgcolor="#CDD7DD" align="center">
<td rowspan="2" width="9%"><span class="text12">模試名</span></td>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
<%-- <td colspan="2" rowspan="2" width="30%"><span class="text12">志望大学</span></td>					 --%>
<%-- <td colspan="3" width="16%"><span class="text12">共通テスト<br>（英語認定試験含まない）</span></td> --%>
<%-- <td colspan="3" width="19%"><span class="text12">共通テスト<br>（英語認定試験含む）</span></td>	 --%>
<%-- <td colspan="3" width="14%"><span class="text12">2次</span></td>		 --%>
<%-- <td colspan="2" width="10%"><span class="text12">総合</span></td>		 --%>
<%-- </tr>																	 --%>
<%-- <tr height="22" bgcolor="#CDD7DD" align="center">						 --%>
<%-- <td width="6%"><span class="text12">ボーダー</span></td>				 --%>
<%-- <td width="7%"><span class="text12">評価得点<br>/満点</span></td>		 --%>
<%-- <td width="3%"><span class="text12">評価</span></td>					 --%>
<%-- <td width="6%"><span class="text12">ボーダー</span></td>				 --%>
<%-- <td width="7%"><span class="text12">評価得点<br>/満点</span></td>		 --%>
<%-- <td width="6%"><span class="text12">評価</span></td>					 --%>
<%-- <td width="5%"><span class="text12">ランク<br>偏差値</span></td>		 --%>
<%-- <td width="5%"><span class="text12">評価<br>偏差値</span></td>			 --%>
<%-- <td width="4%"><span class="text12">評価</span></td>					 --%>
<%-- <td width="6%"><span class="text12">評価<br>ポイント</span></td>		 --%>
<%-- <td width="4%"><span class="text12">評価</span></td>					 --%>
<td colspan="2" rowspan="2" width="35%"><span class="text12">志望大学</span></td>
<td colspan="3" width="20%"><span class="text12">共通テスト</span></td>
<td colspan="3" width="20%"><span class="text12">2次</span></td>
<td colspan="2" width="20%"><span class="text12">総合</span></td>
<td colspan="2" rowspan="2" width="35%"><span class="text12">順位</span></td>
</tr>
<tr height="22" bgcolor="#CDD7DD" align="center">
<td width="7%"><span class="text12">ボーダー</span></td>
<td width="7%"><span class="text12">評価得点<br>/満点</span></td>
<td width="4%"><span class="text12">評価</span></td>
<td width="7%"><span class="text12">ランク<br>偏差値</span></td>
<td width="7%"><span class="text12">評価<br>偏差値</span></td>
<td width="4%"><span class="text12">評価</span></td>
<td width="10%"><span class="text12">評価<br>ポイント</span></td>
<td width="4%"><span class="text12">評価</span></td>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD END   --%>
</tr>
<!--/項目-->

<!--第3回マーク模試-->
<!--1段目-->
<c:forEach var="examinationData" items="${i102Bean.personData.examinationDatas}" varStatus="status">
<c:forEach var="candidateRatingData" items="${examinationData.candidateRatingDatas}" varStatus="status2">
<c:choose>
	<c:when test='${status2.index == 0}'>
		<tr>
		<td rowspan="<c:out value='${examinationData.candidateRatingLength}'/>" width="9%" bgcolor="#E1E6EB" align="center" valign="top">
		<table border="0" cellpadding="4" cellspacing="0">
		<tr>
		<td align="center"><span class="text12"><c:out value="${examinationData.examName}" /></span></td>
		</tr>
		</table>
		</td>
	</c:when>
	<c:otherwise>
		<tr>
	</c:otherwise>
</c:choose>
<td width="2%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value='${status2.count}'/></span></td>
<td width="29%" bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12"><c:out value="${candidateRatingData.superAbbrName}" /></span></td>
</tr>
</table>
</td>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
<%-- <td width="6%" bgcolor="#F4E5D6" align="center"> --%>
<td width="7%" bgcolor="#F4E5D6" align="center">
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD END   --%>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
<%-- <td><span class="text12"><c:out value="${candidateRatingData.cenBorderNoexa}" /></span></td> --%>
<td><span class="text12"><c:out value="${candidateRatingData.cenBorder}" /></span></td>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD END   --%>
</tr>
</table>
</td>
<td width="7%" bgcolor="#F4E5D6" align="center">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
<%-- <td><span class="text12"><c:out value="${candidateRatingData.ratingCvsScore}" /></td> --%>
<td><span class="text12"><c:out value="${candidateRatingData.ratingCvsScore}" /></span></td>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD END   --%>
</tr>
</table>
</td>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
<%-- <td width="5%" bgcolor="#F4E5D6" align="center"><span><pre class="text12-aa"><c:out value="${candidateRatingData.markGRating}" /></pre></span></td> --%>
<%-- <td width="6%" bgcolor="#F4E5D6" align="center"> --%>
<td width="4%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${candidateRatingData.markGRating}" /></span></td>
<td width="7%" bgcolor="#F4E5D6" align="center">
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD END   --%>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
<%-- <td><span class="text12"><c:out value="${candidateRatingData.cenBorder}" /></span></td> --%>
<td><span class="text12"><c:out value="${candidateRatingData.rankLimits}" /></span></td>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD END   --%>
</tr>
</table>
</td>
<td width="7%" bgcolor="#F4E5D6" align="center">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
<%-- <td><span class="text12"><c:out value="${candidateRatingData.ratingCvsScore_engpt}" /><c:out value="${candidateRatingData.entexamfullpntCenter}" /></span></td> --%>
<td><span class="text12"><c:out value="${candidateRatingData.ratingDeviation}" /></span></td>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD END   --%>
</tr>
</table>
</td>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
<%-- <td width="5%" bgcolor="#F4E5D6" align="center"><span><span><pre class="text12-aa"><c:out value="${candidateRatingData.markGRating_engpt}" /></pre></span></td> --%>
<%-- <td width="5%" bgcolor="#F4E5D6" align="center"> --%>
<td width="4%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${candidateRatingData.discriptionGRating}" /></span></td>
<td width="10%" bgcolor="#F4E5D6" align="right">
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD END   --%>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
<%-- <td><span class="text12"><c:out value="${candidateRatingData.rankLimits}" /></span></td> --%>
<td><span class="text12"><c:out value="${candidateRatingData.ratingPoint}" /></span></td>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD END   --%>
</tr>
</table>
</td>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
<%-- <td width="5%" bgcolor="#F4E5D6" align="center"> --%>
<%-- <table border="0" cellpadding="4" cellspacing="0"> --%>
<%-- <tr> --%>
<%-- <td><span class="text12"><c:out value="${candidateRatingData.ratingDeviation}" /></span></td> --%>
<%-- </tr> --%>
<%-- </table> --%>
<%-- </td> --%>
<%-- <td width="4%" bgcolor="#F4E5D6" align="center"><span><pre class="text12-aa"><c:out value="${candidateRatingData.discriptionGRating}" /></pre></span></td> --%>
<%-- <td width="6%" bgcolor="#F4E5D6" align="right"> --%>
<%-- <table border="0" cellpadding="4" cellspacing="0"> --%>
<%-- <tr> --%>
<%-- <td><span class="text12"><c:out value="${candidateRatingData.ratingPoint}" /></span></td> --%>
<%-- </tr> --%>
<%-- </table> --%>
<%-- </td> --%>
<%-- <td width="3%" bgcolor="#F4E5D6" align="center"><span><pre class="text12-aa"><c:out value="${candidateRatingData.totalRating}" /></pre></span></td> --%>
<td width="4%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${candidateRatingData.totalRating}" /></span></td>
<td width="4%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${candidateRatingData.totalCandidateRank}" /></span></td>
</tr>
<%-- 2019/12/03 QQ)Tanioka 英語認定試験延期対応 UPD END   --%>
</c:forEach>
</c:forEach>
<!--sample

<tr>
<td rowspan="2" width="9%" bgcolor="#E1E6EB" align="center" valign="top">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td align="center"><span class="text12">第2回<br>マーク模試</span></td>
</tr>
</table>
</td>
<td width="2%" bgcolor="#F4E5D6" align="center"><span class="text12">1</span></td>
<td width="29%" bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">都留文科　文　国文-前</span></td>
</tr>
</table>
</td>
<td width="7%" bgcolor="#F4E5D6" align="center">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">300</span></td>
</tr>
</table>
</td>
<td width="7%" bgcolor="#F4E5D6" align="center">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">100</span></td>
</tr>
</table>
</td>
<td width="4%" bgcolor="#F4E5D6" align="center"><span class="text12">E</span></td>
<td width="7%" bgcolor="#F4E5D6" align="center">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">50.0</span></td>
</tr>
</table>
</td>
<td width="7%" bgcolor="#F4E5D6" align="center">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">50.0</span></td>
</tr>
</table>
</td>
<td width="4%" bgcolor="#F4E5D6" align="center"><span class="text12">E</span></td>
<td width="10%" bgcolor="#F4E5D6" align="right">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">171</span></td>
</tr>
</table>
</td>
<td width="4%" bgcolor="#F4E5D6" align="center"><span class="text12">E</span></td>
<td width="10%" bgcolor="#F4E5D6" align="right">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">276/300</span></td>
</tr>
</table>
</td>
</tr>

<tr>
<td width="2%" bgcolor="#F4E5D6" align="center"><span class="text12">2</span></td>
<td width="29%" bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">三重　人文　文化-後</span></td>
</tr>
</table>
</td>
<td width="7%" bgcolor="#F4E5D6" align="center">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">200</span></td>
</tr>
</table>
</td>
<td width="7%" bgcolor="#F4E5D6" align="center">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">100</span></td>
</tr>
</table>
</td>
<td width="4%" bgcolor="#F4E5D6" align="center"><span class="text12">G</span></td>
<td width="7%" bgcolor="#F4E5D6" align="center">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">50.0</span></td>
</tr>
</table>
</td>
<td width="7%" bgcolor="#F4E5D6" align="center">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">50.0</span></td>
</tr>
</table>
</td>
<td width="4%" bgcolor="#F4E5D6" align="center"><span class="text12">G</span></td>
<td width="10%" bgcolor="#F4E5D6" align="right">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">161</span></td>
</tr>
</table>
</td>
<td width="4%" bgcolor="#F4E5D6" align="center"><span class="text12">G</span></td>
<td width="10%" bgcolor="#F4E5D6" align="right">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">276/300</span></td>
</tr>
</table>
</td>
</tr>
/2段目
/Sample-->
</table>
</div>
<!--/リスト-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/成績分析-->

<!--セキュリティスタンプの選択-->
<%@ include file="/jsp/individual/include/security_stamp.jsp" %>
<!--/セキュリティスタンプの選択-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--印刷対象生徒-->
<%@ include file="/jsp/individual/include/printStudent.jsp" %>
<!--/印刷対象生徒-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--ケイコさん-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="784">
<tr valign="top">
<td width="72" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="702">

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="678" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="678" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="678" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="678" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr>
<td width="12" background="./shared_lib/img/parts/wb_arrow_l_long.gif"><img src="./shared_lib/img/parts/sp.gif" width="12" height="5" border="0" alt=""><br></td>
<td width="688" bgcolor="#F9EEE5">
<!--アドバイス-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="688">
<tr valign="top">
<td width="131" align="center"><img src="./shared_lib/img/parts/word_one_point.gif" width="86" height="32" border="0" alt="ワンポイントアドバイス！！" vspace="5"><br></td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="5" border="0" alt=""><br></td>
<td width="511">


<div style="margin-top:5px;margin-bottom:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="511">
<%@ include file="/jsp/shared_lib/I_Onepoint.jsp" %>
</table>
</div>


</td>
<td width="25"><img src="./shared_lib/img/parts/sp.gif" width="25" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/アドバイス-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="678" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="678" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="678" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="678" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
</form>
</div>
</body>
</html>

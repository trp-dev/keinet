<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript" src="./js/DatePicker.js"></script>
<script type="text/javascript" src="./js/JString.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<SCRIPT LANGUAGE="JavaScript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>
	<%@ include file="/jsp/script/open_schedule_alert.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	/**
	 * サブミッション
	 */
	function submitForm(forward) {
		<%--スクロール位置 0426 K.Kondo--%>
		if(forward=='<c:out value="${param.forward}" />') {
			document.forms[0].scrollX.value = document.body.scrollLeft;
			document.forms[0].scrollY.value = document.body.scrollTop;
		} else {
			document.forms[0].scrollX.value = 0;
			document.forms[0].scrollY.value = 0;
		}
		//年度をまたぐ可能性がある
		//document.forms[0].year.value = WYEAR;
		document.forms[0].target = "_self";
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 戻るボタン
	 */
	function submitBack() {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "i303";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}

	/**
	 * キャンセルサブミッション
	 */
	function submitCancel(){
		window.close();
	}
	/**
	 * 複製時にunivCdをdisabledしているが、値を飛ばすために
	 * サブミット時に有効化する
	 */
	function enableUnivCd(){
		document.forms[0].univCd.disabled = false;
	}
	/**
	 * 再表示サブミッション
	 */
	function submitRefresh(){
		<c:if test="${form.button == 'copy'}">enableUnivCd();</c:if>
		document.forms[0].refresh.value = "true";
		submitForm('i304');
	}
	/**
	 * 登録サブミッション
	 */
	function submitUpdate(){
		<c:if test="${form.button == 'copy'}">enableUnivCd();</c:if>
		//if(isMultipleDate()){
			document.forms[0].button.value = "update";
			submitForm('i304');
		//	alert("OK");
		//}else{
		//	alert("日付が重複しています");
		//}
	}

	/**
	 * 更新確認＆サブミッション
	 */
	function UpdateCk(){

			var message = "";
			var js = new JString();
			// 入力チェック（プロファイル名の文字数）
			js.setString(document.forms[0].remarks.value);
			if (js.getLength() > 40) {
				message += "備考欄は全角20文字までです。\n";
			}

			var provEntExamSites = document.forms[0].elements["provEntExamSite"];

			if(provEntExamSites != null){
				if(provEntExamSites.length == void(0)) {
					var site = document.forms[0].elements["provEntExamSite"].value;
					var js = new JString();

					// 入力チェック（コメントの文字数）
					js.setString(site);

					if (js.getLength() > 10) {
						message = "地方試験地は全角５文字までです。\n";
					}
				} else {
					for(var i=0; i<provEntExamSites.length; i++){
						if(!document.forms[0].elements["provEntExamSite"][i].disabled) {
							var site = document.forms[0].elements["provEntExamSite"][i].value;
							var js = new JString();

							// 入力チェック（コメントの文字数）
							js.setString(site);

							if (js.getLength() > 10) {
								message = "地方試験地は全角５文字までです。\n";
								break;
							}
						}
					}
				}
			}

			if (message != "") {
				alert(message);
				return false;
			}else{
				submitUpdate();
			}
	}
	/**
	 * 同じ区分内で同じ日が選択されていないかチェック
	 */
	/*
	function isMultipleDate(){
		var months = document.getElementsByName("month");
		var dates = document.getElementsByName("date");

		// まずは区分ごとにリストを作成して、作成した区分のリストにさらに日付を入れるリストを作成
		preDiv1 = "";
		preDiv2 = "";
		preDiv3 = "";
		var tempArray = new Array();
		alert("months.length "+months.length);
		for(var i=0; i<months.length; i++){
			var iid = months[i].id.split();
			var div1 = iid[4];//区分１
			var div2 = iid[5];//区分２
			var div3 = iid[6];//区分３
			if(div1 != preDiv1 && div2 != preDiv2 && div3 != preDiv3){
				tempArray[tempArray.length] = new Array();//区分ごとに配列を作成
			}
			tempArray[tempArray.length -1][tempArray[tempArray.length-1].length] = months[i].value+"/"+dates[i].value;
			preDiv1 = div1;
			preDiv2 = div2;
			preDiv3 = div3;
		}
		// 区分ごとに日付の重複をチェック
		var okFlag = true;
		for(var i=0; i<tempArray.length; i++){
			for(var j=0; j<tempArray[i].length; j++){
				for(var k=0; k<tempArray[i].length; k++){
					if(j != k && tempArray[i][j] == tempArray[i][k]){
						okFlag = false;
					}
				}
			}
		}

		return okFlag;
	}
	*/
	/**
	 * サーブレットエラー表示
	 */
	function showErrorMessage(){
		if('<c:out value="${form.errorMessage}" />'.length != 0) {
			alert('<c:out value="${form.errorMessage}" />');
		}
	}
	/**
	 * 日付コンボ削除
	 */
	function deleteDate(ID, tblName){
		// 指定の行を削除
		var tbl = document.getElementById(tblName);

		for(var i=0; i<tbl.rows.length; i++){
			if(tbl.rows[i].id == ID){
				tbl.deleteRow(i);
			}
		}
	}
	/**
	 * チェックボックスをオフにした時に、
	 * 付随する受験地ボックスも無効化する
	 */
	function disableBox(index){
		document.getElementById("site_"+index).disabled = true;
		document.getElementById("month_"+index).disabled = true;
		document.getElementById("date_"+index).disabled = true;
		document.getElementById("div1_"+index).disabled = true;
		document.getElementById("div2_"+index).disabled = true;
		document.getElementById("div3_"+index).disabled = true;
	}
	/**
	 * チェックボックスをオンにした時に、
	 * 付随する受験地ボックスを有効化する
	 */
	function enableBox(index){
		document.getElementById("site_"+index).disabled = false;
		document.getElementById("month_"+index).disabled = false;
		document.getElementById("date_"+index).disabled = false;
		document.getElementById("div1_"+index).disabled = false;
		document.getElementById("div2_"+index).disabled = false;
		document.getElementById("div3_"+index).disabled = false;
	}
	/**
	 * 日付追加
	 */
	function addDate(divKey, div1, div2, div3){
		magicNum ++;
	    // 新しい行を追加
	    var tbl = document.getElementById(divKey);
	    var newRow = tbl.insertRow(tbl.rows.length);
	    newRow.setAttribute("id", magicNum);

			//univValueの作成
        	var newCell = newRow.insertCell();
        		newCell.style.backgroundColor = "#F4E5D6";
        		newCell.width = "30";

        		<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
        		// var input0 = document.createElement("<input id=\"new\" name=\"univValue\">");
        		var input0 = document.createElement("input");
        		input0.setAttribute("id", "new");
        		input0.setAttribute("name", "univValue");
        		<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

        		input0.setAttribute("type", "hidden");
				input0.setAttribute("value", divKey);
        	newCell.appendChild(input0);

			//date
        	var newCell2 = newRow.insertCell();
        		newCell2.style.backgroundColor = "#F4E5D6";

        		<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
	        	// var table2 = document.createElement("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
        		var table2 = document.createElement("table");
        		table2.setAttribute("border", "0");
        		table2.setAttribute("cellpadding", "0");
        		table2.setAttribute("cellspacing", "0");
        		<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

	        	newCell2.appendChild(table2);
	        	var newRow2 = table2.insertRow();
	        	var newCell2_1 = newRow2.insertCell();
		        	var img2 = document.createElement("img");
		        	img2.src = "./shared_lib/img/parts/sp.gif";
		        	img2.width = "4";
		        	img2.height = "1";
		        	img2.border = "0";
		        	img2.alt = "";
		        newCell2_1.appendChild(img2);

		        var newCell2_2 = newRow2.insertCell();
        			<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
		        	// var ms = document.createElement("<select class=\"text12\" name=\"month\" onchange=\"ps2["+ps2.length+"].ChgMon('MM_"+magicNum+"', 'DD_"+magicNum+"')\"></select>");
        			var ms = document.createElement("select");
        			ms.setAttribute("class", "text12");
        			ms.setAttribute("name", "month");
        			ms.setAttribute("onchange", "ps2[" + ps2.length + "].ChgMon('MM_" + magicNum + "', 'DD_" + magicNum + "')");
            		<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

	        		ms.setAttribute("id", "MM_"+magicNum);
				newCell2_2.appendChild(ms);

				var newCell2_3 = newRow2.insertCell();
					var img23 = document.createElement("img");
		        	img23.src = "./shared_lib/img/parts/sp.gif";
		        	img23.width = "5";
		        	img23.height = "1";
		        	img23.border = "0";
		        	img23.alt = "";
				newCell2_3.appendChild(img23);

				var newCell2_4 = newRow2.insertCell();
    				<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
					// var ds = document.createElement("<select class=\"text12\" name=\"date\" onchange=\"ps2["+ps2.length+"].ChgDay('DD_"+magicNum+"')\"></select>");
        			var ds = document.createElement("select");
        			ds.setAttribute("class", "text12");
        			ds.setAttribute("name", "date");
        			ds.setAttribute("onchange", "ps2[" + ps2.length + "].ChgDay('DD_" + magicNum + "')");
            		<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

			   		ds.setAttribute("id", "DD_"+magicNum);
				newCell2_4.appendChild(ds);

				var newCell2_5 = newRow2.insertCell();
					var img25 = document.createElement("img");
		        	img25.src = "./shared_lib/img/parts/sp.gif";
		        	img25.width = "10";
		        	img25.height = "1";
		        	img25.border = "0";
		        	img25.alt = "";
				newCell2_5.appendChild(img25);

				var newCell2_6 = newRow2.insertCell();
						<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
						// var span2 = document.createElement("<span class=\"text12\">");
						var span2 = document.createElement("span");
						span2.setAttribute("class", "text12");
	        			<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

						var anchor2 = document.createElement('a');
						anchor2.appendChild(document.createTextNode("削除"));
						anchor2.setAttribute("href", "javascript:deleteDate("+magicNum+", '"+divKey+"')");
					span2.appendChild(anchor2);
				newCell2_6.appendChild(span2);

			//site
        	var newCell3 = newRow.insertCell();
        	newCell3.width = "223";
        	newCell3.style.backgroundColor = "#F4E5D6";

        		<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
	        	// var table3 = document.createElement("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
        		var table3 = document.createElement("table");
        		table3.setAttribute("border", "0");
        		table3.setAttribute("cellpadding", "0");
        		table3.setAttribute("cellspacing", "0");
        		<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

	        	newCell3.appendChild(table3);
             	var newRow3 = table3.insertRow();
		        	var newCell3_1 = newRow3.insertCell();
			        	var img3 = document.createElement("img");
			        	img3.src = "./shared_lib/img/parts/sp.gif";
			        	img3.width = "4";
			        	img3.height = "1";
			        	img3.border = "0";
			        	img3.alt = "";
			        	newCell3_1.appendChild(img3);
			        var newCell3_2 = newRow3.insertCell();

			        	<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
			        	// var site = document.createElement("<input MAXLENGTH=\"10\" type=\"text\" name='provEntExamSite' class=\"text12\">");
			        	var site = document.createElement("input");
			        	site.setAttribute("MAXLENGTH", "10");
			        	site.setAttribute("type", "text");
			        	site.setAttribute("name", "provEntExamSite");
			        	site.setAttribute("class", "text12");
			        	<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

			        	site.size = "10";
			        	site.style.width = "80px";
		   				site.value = "";
		   				newCell3_2.appendChild(site);

		   				<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
		   				// var div_1 = document.createElement("<input type=\"hidden\" name=\"entExamDiv1\">");
		   				var div_1 = document.createElement("input");
		   				div_1.setAttribute("type", "hidden");
		   				div_1.setAttribute("name", "entExamDiv1");

			        	// var div_2 = document.createElement("<input type=\"hidden\" name=\"entExamDiv2\">");
		   				var div_2 = document.createElement("input");
		   				div_2.setAttribute("type", "hidden");
		   				div_2.setAttribute("name", "entExamDiv2");

						// var div_3 = document.createElement("<input type=\"hidden\" name=\"entExamDiv3\">");
		   				var div_3 = document.createElement("input");
		   				div_3.setAttribute("type", "hidden");
		   				div_3.setAttribute("name", "entExamDiv3");
			        	<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

			        	div_1.value = div1;
			        	div_2.value = div2;
			        	div_3.value = div3;
			        	newCell3_2.appendChild(div_1);
			        	newCell3_2.appendChild(div_2);
			        	newCell3_2.appendChild(div_3);

		   			var newCell3_3 = newRow3.insertCell();
		   				var img3 = document.createElement("img");
			        	img3.src = "./shared_lib/img/parts/sp.gif";
			        	img3.width = "5";
			        	img3.height = "1";
			        	img3.border = "0";
			        	img3.alt = "";
		   			var newCell3_4 = newRow3.insertCell();
		   				<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
		   				// var span3 = document.createElement("<span class=\"text12\">");
		   				var span3 = document.createElement("span");
		   				span3.setAttribute("class", "text12");
		   				<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

		   				span3.appendChild(document.createTextNode(" ＜全角5文字まで＞"));
		   				newCell3_4.appendChild(span3);

			var today = new Date();
			<%--
				2005.3.10 DatePicker用の年度をFormから取得するように変更
				ps2[ps2.length] = new DatePicker(<c:out value="${sessionScope['iCommonMap'].targetExamYear}" />, today.getMonth()+1, today.getDate(), 'MM_'+magicNum, 'DD_'+magicNum);
			--%>
			ps2[ps2.length] = new DatePicker(<c:out value="${form.year}" />, today.getMonth()+1, today.getDate(), 'MM_'+magicNum, 'DD_'+magicNum);

	}
	/**
	 * 大学コンボボックス
	 */
	<c:if test="${form.button == 'copy'}">
	/**
	 * 模試コンボボックス
	 */
	var univString = new Array(
		<c:forEach var="nestedUnivData" items="${univSearchBean.nestedUnivList}" varStatus="status">
			<c:if test="${status.index != 0}">,</c:if>
			new Array('<c:out value="${nestedUnivData.univCd}"/>','<c:out value="${nestedUnivData.univNameAbbr}"/>')
		</c:forEach>
	);
	var facultyString = new Array(
		<c:forEach var="nestedUnivData" items="${univSearchBean.nestedUnivList}" varStatus="status">
			<c:if test="${status.index != 0}">,</c:if>
			new Array(
				<c:forEach var="facultyData" items="${nestedUnivData.facultyList}" varStatus="status2">
					<c:if test="${status2.index != 0}">,</c:if>
					new Array('<c:out value="${facultyData.facultyCd}"/>','<c:out value="${facultyData.facultyNameAbbr}"/>')
				</c:forEach>
			)
		</c:forEach>
	);
	var deptString = new Array(
		<c:forEach var="nestedUnivData" items="${univSearchBean.nestedUnivList}" varStatus="status">
			<c:if test="${status.index != 0}">,</c:if>
			new Array(
				<c:forEach var="facultyData" items="${nestedUnivData.facultyList}" varStatus="status2">
					<c:if test="${status2.index != 0}">,</c:if>
					new Array(
						<c:forEach var="deptData" items="${facultyData.deptList}" varStatus="status3">
							<c:if test="${status3.index != 0}">,</c:if>
							new Array('<c:out value="${deptData.deptCd}"/>','<c:out value="${deptData.deptNameAbbr}"/>')
						</c:forEach>
					)
				</c:forEach>
			)
		</c:forEach>
	);
	/**
	 * 大学情報ロード
	 */
	function loadUnivList(){
		createList(document.forms[0].univCd, univString);
		loadFacultyList();
	}
	/**
	 * 学部情報ロード
	 */
	function loadFacultyList(){
		for(var i=0; i<univString.length; i++){
			if(document.forms[0].univCd.options[document.forms[0].univCd.selectedIndex].value == univString[i][0]){
				createList(document.forms[0].facultyCd, facultyString[i]);
			}
		}
		loadDeptList();
	}
	/**
	 * 学科情報ロード
	 */
	function loadDeptList(){
		for(var i=0; i<univString.length; i++){
			if(document.forms[0].univCd.options[document.forms[0].univCd.selectedIndex].value == univString[i][0]){
				for(var j=0; j<facultyString[i].length; j++){
					if(document.forms[0].facultyCd.options[document.forms[0].facultyCd.selectedIndex].value == facultyString[i][j][0]){
						createList(document.forms[0].deptCd, deptString[i][j]);
					}
				}
			}
		}
	}
	/**
	 * 共通コンボ作成
	 */
<%-- 2015/12/24 QQ)Hisakawa 大規模改修 UPD START --%>
/*
	function createList(objList, objArray){
	    var nMax = objArray.length;
	    var nLoop = 0;
	    for (nLoop = 0; nLoop < nMax; nLoop++){
	        oAdd = document.createElement('OPTION');
	        if(objList.childNodes[nLoop]  != undefined)
	            objList.removeChild(objList.childNodes[nLoop]);
	        objList.appendChild(oAdd);
	        objList.childNodes[nLoop].setAttribute('value', objArray[nLoop][0]);

	        if(objList == document.forms[0].elements['univCd'] && nLoop == 0){
	        	objList.childNodes[nLoop].setAttribute('selected', true);
	        }
	        if(objList == document.forms[0].elements['facultyCd'] && objArray[nLoop][0] == '<c:out value="${form.facultyCd}"/>' ){
	        	objList.childNodes[nLoop].setAttribute('selected', true);
	        }
	        if(objList == document.forms[0].elements['deptCd'] && objArray[nLoop][0] == '<c:out value="${form.deptCd}"/>' ){
	        	objList.childNodes[nLoop].setAttribute('selected', true);
	        }
	        oAddx= document.createTextNode(objArray[nLoop][1]);
	        if(objList.childNodes[nLoop].firstChild  != undefined)
	            objList.childNodes[nLoop].removeChild(objList.childNodes[nLoop].firstChild);
	        objList.childNodes[nLoop].appendChild(oAddx);
	    }
	    objList.length=nLoop;
	}
*/

	function createList(objList, objArray){
	    var nMax = objArray.length;
	    var nLoop = 0;
	    for (nLoop = 0; nLoop < nMax; nLoop++){
	        oAdd = document.createElement('OPTION');
	        if(objList.children[nLoop]  != undefined)
	            objList.removeChild(objList.children[nLoop]);
	        objList.appendChild(oAdd);
	        objList.children[nLoop].setAttribute('value', objArray[nLoop][0]);

	        if(objList == document.forms[0].elements['univCd'] && nLoop == 0){
	        	objList.children[nLoop].setAttribute('selected', true);
	        }
	        if(objList == document.forms[0].elements['facultyCd'] && objArray[nLoop][0] == '<c:out value="${form.facultyCd}"/>' ){
	        	objList.children[nLoop].setAttribute('selected', true);
	        }
	        if(objList == document.forms[0].elements['deptCd'] && objArray[nLoop][0] == '<c:out value="${form.deptCd}"/>' ){
	        	objList.children[nLoop].setAttribute('selected', true);
	        }
	        oAddx= document.createTextNode(objArray[nLoop][1]);
	        if(objList.children[nLoop].firstChild  != undefined)
	            objList.children[nLoop].removeChild(objList.children[nLoop].firstChild);
	        objList.children[nLoop].appendChild(oAddx);
	    }
	    objList.length=nLoop;
	}
	</c:if>
<%-- 2015/12/24 QQ)Hisakawa 大規模改修 UPD END   --%>
	/**
	 * 初期化
	 */
	var magicNum = 0;
	var ps, ps2;
	function init(){
		startTimer();
		<%--スクロール位置 0426 K.Kondo--%>
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

		ps = new Array();
			<c:set var="magicNum" value="0"/>
			<c:forEach var="univData" items="${planUnivSearchBean.nestedPlanUnivList}" varStatus="status">
				<c:forEach var="dataList" items="${univData.divDataMap}" varStatus="status2">
					<c:forEach var="data" items="${dataList.value}" varStatus="status3">
						<c:if test="${!data.inplePlan && data.plan}">
		<%--
			DatePickerに渡す年度をFormから取得するように変更
		ps[<c:out value="${magicNum}"/>] = new DatePicker(<c:out value="${sessionScope['iCommonMap'].targetExamYear}" />, <c:out value="${data.entExamInpleMonth}"/>, <c:out value="${data.entExamInpleDate}"/>, 'MM_<c:out value="${magicNum}"/>', 'DD_<c:out value="${magicNum}"/>');
		--%>
		ps[<c:out value="${magicNum}"/>] = new DatePicker(<c:out value="${form.year}" />, <c:out value="${data.entExamInpleMonth}"/>, <c:out value="${data.entExamInpleDate}"/>, 'MM_<c:out value="${magicNum}"/>', 'DD_<c:out value="${magicNum}"/>');
							<c:set var="magicNum" value="${magicNum + 1}"/>
						</c:if>
					</c:forEach>
				</c:forEach>
			</c:forEach>
		magicNum = <c:out value="${magicNum}"/>;

		ps2 = new Array();

		<c:if test="${form.button == 'copy'}">
			loadUnivList();
		</c:if>

		//ロード中
		<%@ include file="/jsp/script/loading.jsp" %>

		showErrorMessage();
	}
//-->
</SCRIPT>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I304Servlet" />" method="POST">
<%-- 共通パラメター --%>
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<%-- i304依存パラメター --%>
<input type="hidden" name="targetPersonId" value="<c:out value="${form.targetPersonId}" />">
<input type="hidden" name="button" value="<c:out value="${form.button}"/>">
<input type="hidden" name="preButton" value="<c:out value="${form.button}"/>">
<input type="hidden" name="refresh" value="false">
<input type="hidden" name="univValue4Detail" value="<c:out value="${form.univValue4Detail}"/>">
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="554">

<table border="0" cellpadding="0" cellspacing="0" width="554">
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="538" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">
<c:choose>
	<c:when test="${form.button == 'edit'}">受験予定大学（編集）</c:when>
	<c:when test="${form.button == 'new'}">受験予定大学（新規）</c:when>
	<c:when test="${form.button == 'copy'}">受験予定大学（複製）</c:when>
	<c:otherwise>エラー</c:otherwise>
</c:choose>
</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<div style="margin-top:5px;">
<table border="0" cellpadding="2" cellspacing="0" width="570">
<tr valign="top">
<td><span class="text14">各項目の編集後、「登録して閉じる」ボタンを押してください。</span></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="546">
<tr>
<td width="546"><b class="text14">1.受験学科</b></td>
</tr>
</table>
<!--受験学科-->
<table border="0" cellpadding="2" cellspacing="2" width="550">
<!--大学-->
<tr>
<td width="94" bgcolor="#E1E6EB">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">大学</span></td>
</tr>
</table>
</td>
<td width="450" bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">
<c:choose>
	<c:when test="${form.button == 'copy'}">
		<select size="1" class="text12" style="width:200px;" id="univCd" name="univCd" onchange="loadFacultyList();submitRefresh()" disabled></select>
	</c:when>
	<c:otherwise>
		<c:forEach var="nestedUnivData" items="${univSearchBean.nestedUnivList}" varStatus="status">
			<c:if test="${nestedUnivData.univCd == form.univCd}">
				<c:out value="${nestedUnivData.univNameAbbr}"/>
				<input type="hidden" name="univCd" value="<c:out value="${form.univCd}"/>"></c:if>
		</c:forEach>
	</c:otherwise>
</c:choose>
</span></td>
</tr>
</table>
</td>
</tr>
<!--/大学-->
<!--学部-->
<tr>
<td width="94" bgcolor="#E1E6EB">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">学部</span></td>
</tr>
</table>
</td>
<td width="450" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td><span class="text12">
<c:choose>
	<c:when test="${form.button == 'copy'}">
		<select size="1" class="text12" style="width:200px;" id="facultyCd" name="facultyCd" onchange="loadDeptList();submitRefresh()"></select>
	</c:when>
	<c:otherwise>
		<c:forEach var="nestedUnivData" items="${univSearchBean.nestedUnivList}" varStatus="status">
			<c:if test="${nestedUnivData.univCd == form.univCd}">
				<c:forEach var="facultyData" items="${nestedUnivData.facultyList}" varStatus="status2">
					<c:if test="${facultyData.facultyCd == form.facultyCd}">
						<c:out value="${facultyData.facultyNameAbbr}"/>
						<input type="hidden" name="facultyCd" value="<c:out value="${form.facultyCd}"/>"></c:if>
				</c:forEach>
			</c:if>
		</c:forEach>
	</c:otherwise>
</c:choose>
</span></td>
</tr>
</table>
</td>
</tr>
<!--/学部-->
<!--学科-->
<tr>
<td width="94" bgcolor="#E1E6EB">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">学科</span></td>
</tr>
</table>
</td>
<td width="450" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td><span class="text12">
<c:choose>
	<c:when test="${form.button == 'copy'}">
		<select size="1" class="text12" style="width:200px;" id="deptCd" name="deptCd" onchange="submitRefresh()"></select>
	</c:when>
	<c:otherwise>
		<c:forEach var="nestedUnivData" items="${univSearchBean.nestedUnivList}" varStatus="status">
			<c:if test="${nestedUnivData.univCd == form.univCd}">
				<c:forEach var="facultyData" items="${nestedUnivData.facultyList}" varStatus="status2">
					<c:if test="${facultyData.facultyCd == form.facultyCd}">
						<c:forEach var="deptData" items="${facultyData.deptList}" varStatus="status3">
							<c:if test="${deptData.deptCd == form.deptCd}">
								<c:out value="${deptData.deptNameAbbr}"/>
								<input type="hidden" name="deptCd" value="<c:out value="${form.deptCd}"/>"></c:if>
						</c:forEach>
					</c:if>
				</c:forEach>
			</c:if>
		</c:forEach>
	</c:otherwise>
</c:choose>
</span></td>
</tr>
</table>
</td>
</tr>
<!--/学科-->
</table>
<!--/受験学科-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="546">
<tr>
<td width="546"><b class="text14">2．受験予定日</b><span class="text12">&nbsp;&nbsp;&nbsp;&nbsp;※<a href="#here" onclick="openScheduleAlert(2);">入試日程に関するご注意</a></span></td>
</tr>
</table>
<!--受験予定日-->
<table border="0" cellpadding="2" cellspacing="2" width="550">
<!--入試形態-->
<tr>
<td width="97" bgcolor="#E1E6EB">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">入試形態</span></td>
</tr>
</table>
</td>
<td colspan="3" width="447" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td>
<select size="1" class="text12" tyle="width:160px;" name="entExamModeCd" onchange="submitRefresh()"><c:forEach var="data" items="${entExamModeBean.entExamModeList}" varStatus="status2">
<option value="<c:out value="${data.entExamModeCd}"/>" <c:if test="${form.entExamModeCd == data.entExamModeCd}">selected</c:if>><c:out value="${data.entExamModeName}"/></option>
</c:forEach></select>
</td>
</tr>
</table>
</td>
</tr>
<!--/入試形態-->
<!--試験日-->
<tr>
<td width="97" bgcolor="#E1E6EB">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">試験日</span></td>
</tr>
</table>
</td>
<td colspan="3" width="447" bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">
<c:set var="isStr" value="false"/>
<c:set var="shidaiNitteiUrl" value="" />
<c:forEach var="rec" items="${planUnivSearchBean.nestedPlanUnivList}" varStatus="status">
	<c:set var="shidaiNitteiUrl" value="${ rec.shidaiNitteiUrl }" />
	<c:choose>
		<c:when test="${rec.entExamInpleDateStrFull == ''}">※試験日データがありません。下の受験日から直接設定してください。</c:when>
		<c:otherwise><c:out value="${rec.entExamInpleDateStrFull}"/></c:otherwise>
	</c:choose>
	<c:set var="isStr" value="true"/>
</c:forEach>
<c:if test="${isStr == 'false'}">※試験日データがありません。下の受験日から直接設定してください。</c:if>
</span></td>
</tr>
</table>
</td>
</tr>
<!--/試験日-->
<!--受験予定日／試験地-->
<tr>
<td rowspan="2" width="97" bgcolor="#E1E6EB">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">受験予定日／<br>試験地<br><br></span><span class="text12">※試験日程および試験地については必ず大学発表の学生募集要項で確認してください。<br><a href="#here" onclick="openScheduleAlert(2);">「入試日」に関するご注意</a></span></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6" style="border:0px; border-style:none; padding:0px; margin:0px; spacing:0px;" valign="top">


<c:choose>
	<%-- 日付が一日もない場合 --%>
	<c:when test="${!isDateExist}">
<table  border="0" cellpadding="0" cellspacing="0" id="yo" style="border:0px; border-style:none; padding:0px; margin:0px;">
<tr bgcolor="#FFFFFF" style="border:0px; border-style:none; padding:0px; margin:0px;">
<td style="border:0px; border-style:none; padding:0px; margin:0px;">
			<table border="0" cellpadding="0" cellspacing="1" id="newTbl">
				<tr>
					<%-- 必ず1,1,1にする--%>
					<td colspan="3" rowspan="0" width="447" bgcolor="#F4E5D6" align="left"><input type="button" value="追加" class="text12" style="width:50px;" onclick="addDate('newTbl', '1', '1', '1')"></td>
				</tr>
			</table>
</td></tr></table>
	</c:when>

	<%-- 日付がある場合 --%>
	<c:otherwise>
<table  border="0" cellpadding="0" cellspacing="0" id="yo" style="border:0px; border-style:none; padding:0px; margin:0px;">
<c:forEach var="univData" items="${planUnivSearchBean.nestedPlanUnivList}" varStatus="status">
<tr bgcolor="#FFFFFF" style="border:0px; border-style:none; padding:0px; margin:0px;">
<td style="border:0px; border-style:none; padding:0px; margin:0px;">
	<table border="0" cellpadding="0" cellspacing="0"  style="border:0px; border-style:none; padding:0px; margin:0px;">
		<c:set var="dropNum" value="0"/>
		<%-- 区分マップ --%>
		<c:forEach var="dataList" items="${univData.divDataMap}" varStatus="status2">
		<tr style="border:0px; border-style:none; padding:0px; margin:0px;">
		<td style="border:0px; border-style:none; padding:0px; margin:0px;">
		<table border="0" cellpadding="4" cellspacing="1" id="<c:out value="${dataList.key}"/>">
			<%-- 日付一覧表示 --%>
			<c:forEach var="data" items="${dataList.value}" varStatus="status3">
				<%-- 追加ボタン --%>
				<c:if test="${status3.index == 0}">
				<tr>
					<td colspan="3" rowspan="0" width="447" bgcolor="#F4E5D6" align="left">
					<input type="button" value="追加" class="text12" style="width:50px;" onclick="addDate('<c:out value="${dataList.key}"/>', '<c:out value="${data.entExamDiv_1_2order}"/>', '<c:out value="${data.schoolProventDiv}"/>', '<c:out value="${data.entExamDiv_1_2term}"/>')">
					<span class="text12">(<c:if test="${ not empty data.schoolProventDivName }"><kn:schoolProventDiv code="${data.schoolProventDiv}" style="2"/>/</c:if><kn:entExamDiv_1_2order code="${data.entExamDiv_1_2order}" style="1"/>)　<c:out value="${data.branchName}"/></span></td>
				</tr>
				</c:if>
				<tr id="<c:out value="${data.dateDivKey}"/>">
				<c:choose>
					<%-- 受験日のみ場合はドロップダウン表示 --%>
					<c:when test="${!data.inplePlan && data.plan}">
						<%-- ドロップダウン --%>
						<td bgcolor="#F4E5D6" width="30"></td>
						<%-- 日付 --%>
						<td width="190" bgcolor="#F4E5D6">
							<table border="0" cellpadding="0" cellspacing="0">
							<tr>
							<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
							<td class="text12">
								<select ID="MM_<c:out value="${dropNum}"/>" class="text12" name="month" onchange="ps[<c:out value="${dropNum}"/>].ChgMon('MM_<c:out value="${dropNum}"/>', 'DD_<c:out value="${dropNum}"/>')"></select>
								<select ID="DD_<c:out value="${dropNum}"/>" class="text12" name="date" onchange="ps[<c:out value="${dropNum}"/>].ChgDay('DD_<c:out value="${dropNum}"/>')"></select>
							</td>
							<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
							<td><span class="text12"><a href="javascript:deleteDate('<c:out value="${data.dateDivKey}"/>', '<c:out value="${dataList.key}"/>')">削除</a></span></td>
							</tr>
							</table>
						</td>
						<c:set var="dropNum" value="${dropNum+1}"/>
						<%-- 受験地 --%>
						<td width="228" bgcolor="#F4E5D6">
							<table border="0" cellpadding="0" cellspacing="0">
							<tr>
							<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
							<td><input MAXLENGTH="10" id="site_<c:out value="${data.dateDivKey}"/>" type="text" size="10" class="text12" style="width:80px;"  name="provEntExamSite" value="<c:out value="${data.provEntExamSite}"/>"></td>
							<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
							<td><span class="text12">＜全角5文字まで＞</span></td>
							</tr>
							</table>
						</td>
						<input type="hidden" name="entExamDiv1" value="<c:out value="${data.entExamDiv_1_2order}"/>">
						<input type="hidden" name="entExamDiv2" value="<c:out value="${data.schoolProventDiv}"/>">
						<input type="hidden" name="entExamDiv3" value="<c:out value="${data.entExamDiv_1_2term}"/>">
					</c:when>
					<%-- 入試日程のみ、または入試日程と受験日が同じ場合はチェックボックス表示 --%>
					<c:otherwise>
						<%-- チェックボックス --%>
						<td bgcolor="#F4E5D6" width="30">
							<%-- 入試日程と受験日が同じ場合はチェックをつける --%>
							<input onclick="javascript: this.checked ? enableBox('<c:out value="${data.dateDivKey}"/>') : disableBox('<c:out value="${data.dateDivKey}"/>')" type="checkbox" name="univValuePre" value="<c:out value="${data.dateDivKey}"/>" <c:if test="${data.inplePlan}">checked</c:if>>

							<input id="month_<c:out value="${data.dateDivKey}"/>" type="hidden" name="month" value="<c:out value="${data.entExamInpleMonth}"/>" <c:if test="${!data.inplePlan}">disabled</c:if>>
							<input id="date_<c:out value="${data.dateDivKey}"/>" type="hidden" name="date" value="<c:out value="${data.entExamInpleDate}"/>" <c:if test="${!data.inplePlan}">disabled</c:if>>
						</td>
						<%-- 日付 --%>
						<td width="190" bgcolor="#F4E5D6">
							<table border="0" cellpadding="0" cellspacing="0">
							<tr>
							<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
							<td><span class="text12">
								<c:out value="${data.entExamInpleUniqueDate}"/></span>
							</td>
							<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
							<td><%--<span class="text12"><a href="javascript: deleteDate('<c:out value="${data.dateDivKey}"/>', '<c:out value="${dataList.key}"/>')">削除</a></span>--%></td>
							</tr>
							</table>
						</td>
						<%-- 受験地 --%>
						<td width="228" bgcolor="#F4E5D6">
							<table border="0" cellpadding="0" cellspacing="0">
							<tr>
							<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
							<td><input MAXLENGTH="10" id="site_<c:out value="${data.dateDivKey}"/>" type="text" size="10" class="text12" style="width:80px;"  name="provEntExamSite" value="<c:out value="${data.provEntExamSite}"/>" <c:if test="${!data.inplePlan}">disabled</c:if>></td>
							<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
							<td><span class="text12">＜全角5文字まで＞</span></td>
							</tr>
							</table>
						</td>
						<input id="div1_<c:out value="${data.dateDivKey}"/>" type="hidden" name="entExamDiv1" value="<c:out value="${data.entExamDiv_1_2order}"/>" <c:if test="${!data.inplePlan}">disabled</c:if>>
						<input id="div2_<c:out value="${data.dateDivKey}"/>" type="hidden" name="entExamDiv2" value="<c:out value="${data.schoolProventDiv}"/>" <c:if test="${!data.inplePlan}">disabled</c:if>>
						<input id="div3_<c:out value="${data.dateDivKey}"/>" type="hidden" name="entExamDiv3" value="<c:out value="${data.entExamDiv_1_2term}"/>" <c:if test="${!data.inplePlan}">disabled</c:if>>
					</c:otherwise>
				</c:choose>
				</tr>
		</c:forEach>
		</table>
		</td></tr>
	</c:forEach>
	</table>
	</td></tr>
</c:forEach>
</table>
	</c:otherwise>
	</c:choose>
</td>
</tr>

<!--
<tr>
<td width="160" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td><select name="" class="text12">
<option value="">10月</option>
</select></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><select name="" class="text12">
<option value="">11日</option>
</select></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><span class="text12"><a href="#">削除</a></span></td>
</tr>
</table>
</td>
<td width="223" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td><input type="text" size="10" class="text12" name="" style="width:80px;"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><span class="text12">＜全角5文字まで＞</span></td>
</tr>
</table>
</td>
</tr>
/受験予定日／試験地-->
</table>
<!--/受験予定日-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

	<table border="0" cellpadding="0" cellspacing="0" width="546">
	<tr>
	<td width="546"><b class="text14">3.備考</b></td>
	</tr>
	</table>

	<!--備考-->
	<table border="0" cellpadding="2" cellspacing="2" width="550">
	<tr>
		<td width="94" bgcolor="#E1E6EB">
			<table border="0" cellpadding="4" cellspacing="0">
			<tr>
			<td><span class="text12">備考</span></td>
			</tr>
			</table>
		</td>
		<td width="450" bgcolor="#F4E5D6">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
			<td>

				<input MAXLENGTH="40" type="text" size="10" class="text12" name="remarks" style="width:240px;" value="<c:out value="${form.remarks}"/>">

			</td>
			<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
			<td><span class="text12">＜全角20文字まで＞</span></td>
			</tr>
			</table>
		</td>
	</tr>

<c:if test="${guideRemarksData != null}">
	<!--ガイドライン-->
	<tr>
		<td width="94" bgcolor="#E1E6EB">
			<table border="0" cellpadding="4" cellspacing="0">
			<tr>
			<td><span class="text12">ガイドライン</span></td>
			</tr>
			</table>
		</td>
		<td width="450" bgcolor="#F4E5D6">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
			<td><span class="text12"><c:out value="${guideRemarksData.remarks}"/></span></td>
			<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
			<td><span class="text12"></span></td>
			</tr>
			</table>
		</td>
	</tr>
</c:if>
	<!--/ガイドライン-->

	</table>
	<!--/備考-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="15">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="570">
<tr valign="top">
<td width="568" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="&nbsp;キャンセル&nbsp;" class="text12" style="width:120px;" onclick="submitCancel()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td><input type="button" value="&nbsp;登録して閉じる&nbsp;" class="text12" style="width:120px;" onclick="UpdateCk()"></td>
</tr>
</table>

</td>
</tr>
</table>

</td>
</tr>
</table>


<!--/ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td align="center"><span class="text12">※キャンセルを押すと、登録せずにウィンドウを閉じます。</span></td>
</tr>
</table>
</div>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->

<!--戻るボタン-->
<c:choose>
<c:when test="${param.backward == 'i303' || form.button == 'new'}">
	<table border="0" cellpadding="0" cellspacing="0" width="650">
	<tr>
	<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
	<td width="630"><!--前のページに戻るボタン--><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onclick="submitBack()"></td>
	</tr>
	</table>
</c:when>

<c:otherwise>
	<table border="0" cellpadding="0" cellspacing="0" width="650">
	<tr>
	<td width="20"></td>
	<td width="630"></td>
	</tr>
	</table>
</c:otherwise>
</c:choose>
<!--/戻るボタン-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>

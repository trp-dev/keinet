<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>
    // ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	/**
	 * 子ウィンドウからの再表示サブミッション
	 */
	function submitClosed(){
		submitForm('i501');
	}
	/**
	 * サブミッション
	 */
	function submitForm(forward) {
		<%--スクロール位置 0426 K.Kondo--%>
		if(forward=='<c:out value="${param.forward}" />') {
			document.forms[0].scrollX.value = document.body.scrollLeft;
			document.forms[0].scrollY.value = document.body.scrollTop;
		} else {
			document.forms[0].scrollX.value = 0;
			document.forms[0].scrollY.value = 0;
		}
		if(forward == 'i502'){
			document.forms[0].action="<c:url value="I502Servlet" />"
			window.open("blank.html", "sheet", "resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=675,height=510");
			document.forms[0].target = "sheet";
		}else{
			document.getElementById("MainLayer").style.visibility = "hidden";
			document.getElementById("MainLayer").style.top = -2000;
			document.getElementById("LoadingLayer").style.visibility = "visible";
			document.forms[0].target = "_self";
		}
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 戻るボタン
	 */
	function submitBack() {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "i003";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 編集サブミッション
	 */
	function submitEdit(key1, key2, key3) {
		document.forms[0].button.value= 'edit';
		document.forms[0].individualId.value = key1;
		document.forms[0].koIndividualId.value = key2;
		document.forms[0].firstRegistrYdt.value = key3;
		submitForm('i502');
	}
	/**
	 * 削除サブミッション
	 */
	function submitDelete(key1, key2, key3) {
		document.forms[0].button.value = 'delete';
		document.forms[0].individualId.value = key1;
		document.forms[0].koIndividualId.value = key2;
		document.forms[0].firstRegistrYdt.value = key3;
		submitForm('i501');
	}
	/**
	 * 新規登録サブミッション
	 */
	function submitNew(){
		document.forms[0].individualId.value = document.forms[0].targetPersonId.value;
		document.forms[0].button.value = 'new';
		submitForm('i502');
	}
	/**
	 * ページ切り替えサブミッション
	 */
	function submitPage(page) {
		document.forms[0].page.value = page;
		submitForm('i501');
	}
	/**
	 *
	 */
	function nomalCheck(key){
		if (confirm("面談メモを変更しますか？")) {
			//ok
			javascript:submitEdit(key);
		}
	}
	/**
	 * 削除確認
	 */
	function deletionCheck(key1, key2, key3) {

		if (confirm("面談メモを削除しますか？")){
			//ok
			javascript:submitDelete(key1, key2, key3);

		}
	}
	/**
	 * 生徒切り替え
	 */
	<%@ include file="/jsp/individual/include/student_rotator.jsp" %>
	/**
	 * 初期化
	 */
	function init(){
		startTimer();
		<%--スクロール位置 0426 K.Kondo--%>
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
		//1.選択中の生徒まで回す
		findStudent();
		//2.選択中の生徒が一人ならArrowボタンを無効化する
		if(getStudentNum() == 1) disableArrows();

		//選択中生徒の学年によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
		gradeChange();

		//ロード中
		<%@ include file="/jsp/script/loading.jsp" %>
	}
//-->
</script>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I501Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="individualId" value="">
<input type="hidden" name="koIndividualId" value="">
<input type="hidden" name="button" value="">
<input type="hidden" name="firstRegistrYdt" value="">
<input type="hidden" name="page" value="<c:out value="${form.page}" />">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->


<!--面談メモ-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--概要-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--説明-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="862">
<tr>
<td width="22"><img src="./shared_lib/img/parts/sp.gif" width="22" height="1" border="0" alt=""><br></td>
<td width="830"><span class="text14">メモを自由に書き込むことができます。面談結果の記入などにお役立てください。（全角200字まで入力可能）<br></span></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/説明-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上ページング-->
<table border="0" cellpadding="0" cellspacing="0" width="862">
<tr valign="top">
<td width="22"><img src="./shared_lib/img/parts/sp.gif" width="22" height="1" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="830">
<tr valign="top">
<td width="828" bgcolor="#EFF2F3" align="center">
<!--表示-->
<table border="0" cellpadding="0" cellspacing="0" width="804">
<tr>
<td width="804" align="center"><span class="text12">
全<b><c:out value="${SearchBean.recordCount}" /></b>件中　<c:out value="${SearchBean.beginCount}" />〜<c:out value="${SearchBean.endCount}" />件表示
</span></td>
</tr>
<tr>
<td width="804"><img src="./shared_lib/img/parts/dot_blue.gif" width="804" height="1" border="0" alt="" vspace="6"><br></td>
</tr>
<tr>
<td width="804" align="center">
<%@ include file="/jsp/individual/include/ipage.jsp" %>
</td>
</tr>
</table>
<!--/表示-->
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/上ページング-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--リスト-->
<table border="0" cellpadding="0" cellspacing="0" width="862">
<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="834">
<%-- 2015/12/21 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <table border="0" cellpadding="2" cellspacing="2" width="834"> --%>
<table border="0" cellpadding="2" cellspacing="2" width="834" style="word-break:break-all;">
<%-- 2015/12/21 QQ)Hisakawa 大規模改修 UPD END   --%>
<tr bgcolor="#93A3AD">
<td colspan="4">
<table border="0" cellpadding="2" cellspacing="2">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="新規メモ" class="text12" style="width:100px;" onclick="submitNew()"></td>
</tr>
</table>
</td>
</tr>
<!--項目-->
<tr height="22" bgcolor="#CDD7DD" align="center">
<td width="21%"><span class="text12">作成日／タイトル</span></td>
<td width="59%"><span class="text12">面談メモ</span></td>
<td width="10%"><span class="text12">更新日</span></td>
<td width="10%"><span class="text12">編集機能</span></td>
</tr>
<!--/項目-->
<!--set-->
<c:forEach var="i501Data" items="${SearchBean.recordSet}" varStatus="status">
	<tr>
	<td bgcolor="#E1E6EB">
	<table border="0" cellpadding="6" cellspacing="0">
	<tr>
	<td><span class="text10"><c:out value="${i501Data.createDate}" /><br></span>
	<b class="text12"><c:out value="${i501Data.title}" /><br></b></td>
	</tr>
	</table>
	</td>
	<td bgcolor="#F4E5D6">
	<table border="0" cellpadding="6" cellspacing="0">
	<tr>
	<td><span class="text12-hh">

	<kn:pre><c:out value="${i501Data.text}" /></kn:pre>

	</span></td>
	</tr>
	</table>
	</td>
	<td bgcolor="#F4E5D6" align="center"><span class="text10"><c:out value="${i501Data.renewalDate}"/></span></td>
	<td bgcolor="#F4E5D6" align="center"><span class="text12"><a href="#here" onclick="submitEdit('<c:out value="${i501Data.individualId}"/>','<c:out value="${i501Data.koIndividualId}"/>','<c:out value="${i501Data.firstRegistrYdt}"/>')">変更</a>｜<a href="#here" onclick="deletionCheck('<c:out value="${i501Data.individualId}"/>','<c:out value="${i501Data.koIndividualId}"/>','<c:out value="${i501Data.firstRegistrYdt}"/>')">削除</a></span></td>
	</tr>
</c:forEach>
<!--/set-->
</table>
</td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/リスト-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--下ページング-->
<table border="0" cellpadding="0" cellspacing="0" width="862">
<tr>
<td width="22"><img src="./shared_lib/img/parts/sp.gif" width="22" height="1" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="830">
<tr valign="top">
<td width="828" bgcolor="#EFF2F3" align="center">

<%@ include file="/jsp/individual/include/ipage.jsp" %>

</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/下ページング-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="23" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/面談メモ-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>

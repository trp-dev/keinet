<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.keinavi.util.KNUtil" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi^Βl¬ΡͺΝ</title>
<script type="text/javascript" src="<%=KNUtil.getContextPath(request)%>/shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="<%=KNUtil.getContextPath(request)%>/shared_lib/style/stylesheet.css"></noscript>
</head>

<body bgcolor="#F4E5D6" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form>
<input type="hidden" name="targetExamYear">
<input type="hidden" name="targetExamCode">
<input type="hidden" name="targetExamName">
<input type="hidden" name="targetExamCodePrev" value="">
<input type="hidden" name="targetGrade" value="">
<input type="hidden" name="targetClass" value="">
<input type="hidden" name="targetSubject" value="">
<input type="hidden" name="forward">
<input type="hidden" name="backward">
<%--XN[Κu 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="">
<input type="hidden" name="scrollY" value="">
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="changeMode" value="">

<!--Xg-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td bgcolor="#FFFFFF">
		  <table border="0" cellpadding="2" cellspacing="2" width="100%" id="foo">
			<tr bgcolor="#CDD7DD">
				<td width="8%">&nbsp;</td>
				<c:choose>
					<c:when test="${iCommonMap.bunsekiMode}">
						<td colspan="3" width="27%" align="center"><span class="text12"><a href="#here" onclick="parent.orderByClassNo()">wN^NX^Τ</a></span></td>
						<td width="26%" align="center"><span class="text12">Ό</span></td>
						<td width="15%" align="center"><span class="text12">ΘΪ</span></td>
						<td width="12%" align="center"><span class="text12"><a href="#here" onclick="parent.orderByADeviation()">Ξ·l</a></span></td>
						<td width="12%" align="center"><span class="text12"><a href="#here" onclick="parent.orderByComparedCDeviation()">OρΖδr</a></span></td>
					</c:when>
					<c:otherwise>
						<td colspan="3" width="27%" align="center"><span class="text12">wN^NX^Τ</span></td>
						<td width="65%" align="center"><span class="text12">Ό</span></td>
					</c:otherwise>
				</c:choose>
			</tr>
			</table>
		</td>
	</tr>
</table>
<!--/Xg-->
</form>
</body>
</html>
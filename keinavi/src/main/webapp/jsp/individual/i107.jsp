<%--060130 K.Kondo 科目タブの幅調節--%>
<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.keinavi.data.individual.ExaminationData" %>
<%@ page import="jp.co.fj.keinavi.data.individual.IExamData" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Collection" %>
<%@ page import="jp.co.fj.keinavi.data.individual.SubjectData" %>
<%@ page import="jp.co.fj.keinavi.data.individual.PrevSuccessData" %>
<%@ page import="jp.co.fj.keinavi.data.individual.SubRecordData" %>
<%@ page import="jp.co.fj.keinavi.data.individual.SubRecordIData" %>
<%@ page import="jp.co.fj.keinavi.data.help.HelpList" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<jsp:useBean id="i107Bean" scope="request" class="jp.co.fj.keinavi.beans.individual.I107Bean" />
<jsp:useBean id="form" scope="request" class="jp.co.fj.keinavi.forms.individual.I107Form" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<SCRIPT type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    <%@ include file="/jsp/script/submit_change.jsp" %>
    <%@ include file="/jsp/script/submit_exit.jsp" %>
    <%@ include file="/jsp/script/submit_save.jsp" %>
    <%@ include file="/jsp/script/submit_menu.jsp" %>
    <%@ include file="/jsp/script/open_common.jsp" %>
    <%@ include file="/jsp/script/open_sample.jsp" %>
    <%@ include file="/jsp/script/submit_openOnepoint.jsp" %>
    <%@ include file="/jsp/script/submit_help.jsp" %>
    <%@ include file="/jsp/script/clear_disabled.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/printDialog_close.jsp" %>
	var printDialogPattern = "";
	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

    <%-- 対象模試リスト --%>
    <c:set var="examList" value="${iCommonMap.examComboData.listNotAbility}"/>

    /**
     * サブミット
     */
    function submitForm(forward) {
        clearDisabled();
        <%--スクロール位置 0426 K.Kondo--%>
        if(forward=='<c:out value="${param.forward}" />') {
            document.forms[0].scrollX.value = document.body.scrollLeft;
            document.forms[0].scrollY.value = document.body.scrollTop;
        } else {
            document.forms[0].scrollX.value = 0;
            document.forms[0].scrollY.value = 0;
        }
        document.getElementById("MainLayer").style.visibility = "hidden";
        document.getElementById("MainLayer").style.top = -2000;
        document.getElementById("LoadingLayer").style.visibility = "visible";
        document.forms[0].target = "_self";
        document.forms[0].forward.value = forward;
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 戻るボタン
     */
    function submitBack() {
        document.forms[0].target = "_self";
        document.forms[0].forward.value = "i003";
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 再表示サブミット
     */
    function submitRefresh() {
        if(checkCombox()){
            submitForm('<c:out value="${param.forward}" />');
        }else{
            alert("志望大学にて同一大学が選択されています。異なる大学を選択し直してください。");
        }
    }
    /**
     * 科目タブサブミット
     */
    function pushSubButton(subcd, subnm) {
        document.forms[0].targetSubCode.value = subcd;
        document.forms[0].targetSubName.value = subnm;
        submitRefresh();
    }
    /**
     *
     */
    function pushSubmitButton() {
        document.forms[0].targetSubCode.value = "<%=form.getTargetSubCode()%>";
        document.forms[0].targetSubName.value = "<%=form.getTargetSubName()%>";
        submitRefresh();
    }
    /**
     * コンボチェック
     */
    function checkCombox(){
        var flag = true;
        if(document.forms[0].targetCandidateRank1.value != "")
            if(document.forms[0].targetCandidateRank1.value == document.forms[0].targetCandidateRank2.value || document.forms[0].targetCandidateRank1.value == document.forms[0].targetCandidateRank3.value)
                flag = false;
        if(document.forms[0].targetCandidateRank2.value != "")
            if(document.forms[0].targetCandidateRank2.value == document.forms[0].targetCandidateRank1.value || document.forms[0].targetCandidateRank2.value == document.forms[0].targetCandidateRank3.value)
                flag = false;
        if(document.forms[0].targetCandidateRank3.value != "")
            if(document.forms[0].targetCandidateRank3.value == document.forms[0].targetCandidateRank1.value || document.forms[0].targetCandidateRank2.value == document.forms[0].targetCandidateRank3.value)
                flag = false;
        return flag;
    }

    /**
     * フォームデータ
     */
    var LOWER = new Array("30.0", "35.0", "40.0", "45.0", "50.0");
    <%-- 2019/08/09 Hics)Ueta 偏差値の最大値変更 UPD START --%>
  	//var UPPER = new Array("50.0", "55.0", "60.0", "65.0", "70.0");
    var UPPER = new Array("50.0", "55.0", "60.0", "65.0", "70.0", "75.0", "80.0");
    <%-- 2019/08/09 Hics)Ueta 偏差値の最大値変更 UPD END   --%>
    var COMBOX = new Array(
        <%
        //大学リストを作成
        boolean firstFlg = true;
        for (java.util.Iterator it=i107Bean.getComboxDatas().iterator();  it.hasNext(); ) {
            ExaminationData data = (ExaminationData)it.next();
            if(!firstFlg){
                out.println(",");
            }
            out.print("new Array(");
            //志望順位
            out.println("\"" + data.getExamCd() + "\"");
            out.println(",");
            //大学名称を結合
            out.println("\""+ data.getExamName() + "\"");
            out.print(")");
            firstFlg = false;
        }%>
    );
    /**
     * コンボボックス
     */
    function loadCombox(obj1, obj2, obj3){
        <%if(form.getTargetCandidateRank1() != null && i107Bean.getComboxDatas().size() > 0){%>
        var blank1 = document.createElement("OPTION");
        blank1.text = "";
        blank1.value = "10";
        obj1.add(blank1);
        for(var i=0; i<COMBOX.length; i++){
            var input = document.createElement("OPTION");
            input.text = COMBOX[i][1];
            input.value = COMBOX[i][0];
            if(COMBOX[i][0] == "<%=form.getTargetCandidateRank1()%>")
            input.selected = true;
            obj1.add(input);
        }
        <%}%>
        <%if(form.getTargetCandidateRank2() != null && i107Bean.getComboxDatas().size() > 1){%>
        var blank2 = document.createElement("OPTION");
        blank2.text = "";
        blank2.value = "11";
        obj2.add(blank2);
        for(var i=0; i<COMBOX.length; i++){
            var input = document.createElement("OPTION");
            input.text = COMBOX[i][1];
            input.value = COMBOX[i][0];
            if(COMBOX[i][0] == "<%=form.getTargetCandidateRank2()%>")
            input.selected = true;
            obj2.add(input);
        }
        <%}%>
        <%if(form.getTargetCandidateRank3() != null && i107Bean.getComboxDatas().size() > 2){%>
        var blank3 = document.createElement("OPTION");
        blank3.text = "";
        blank3.value = "12";
        obj3.add(blank3);
        for(var i=0; i<COMBOX.length; i++){
            var input = document.createElement("OPTION");
            input.text = COMBOX[i][1];
            input.value = COMBOX[i][0];
            if(COMBOX[i][0] == "<%=form.getTargetCandidateRank3()%>")
            input.selected = true;
            obj3.add(input);
        }
        <%}%>
    }
    /**
     * 偏差値範囲
     */
    function loadDevBox(lowerObj, upperObj){
        for(var i=0; i<LOWER.length; i++){
            var input = document.createElement("OPTION");
            input.text = LOWER[i];
            input.value = LOWER[i];
            if(LOWER[i] == "<%=form.getLowerRange()%>")
            input.selected = true;
            lowerObj.add(input);
        }
        for(var i=0; i<UPPER.length; i++){
            var input = document.createElement("OPTION");
            input.text = UPPER[i];
            input.value = UPPER[i];
            if(UPPER[i] == "<%=form.getUpperRange()%>")
            input.selected = true;
            upperObj.add(input);
        }
    }
    /**
     * 生徒切り替え
     */
    <%@ include file="/jsp/individual/include/student_rotator.jsp" %>
    /**
     * 印刷ダイアログ
     */
    <%@ include file="/jsp/individual/include/printDialog.jsp" %>
    /**
     * 印刷チェック
     */
    function printCheck() {
        if (checkCombox()) {
            printDialog();
        }else{
            alert("志望大学にて同一大学が選択されています。異なる大学を選択し直してください。");
        }
    }
    /**
     * 一括印刷チェック
     */
    function printAllCheck() {
        if (checkCombox()) {
            printAllDialog();
        } else {
            alert("志望大学にて同一大学が選択されています。異なる大学を選択し直してください。");
        }
    }
    // 表示
    function printView() {
        if (checkCombox()) {
            document.forms[0].printStatus.value = "1";
            printSheet();
        } else {
            alert("志望大学にて同一大学が選択されています。異なる大学を選択し直してください。");
        }
    }
    /**
     * 初期化
     */
    function init(){
        startTimer();
        <%--スクロール位置 0426 K.Kondo--%>
        window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

        //1.選択中の生徒まで回す
        findStudent();
        //2.選択中の生徒が一人ならArrowボタンを無効化する
        if(getStudentNum() == 1) disableArrows();

        //学年選択デフォルト
        for(i=0; i<document.forms[0].targetGrade.options.length; i++){
            if(document.forms[0].targetGrade.options[i].value == '<%=form.getTargetGrade()%>'){
                document.forms[0].targetGrade.options[i].selected = true;
            }
        }
        loadCombox(document.forms[0].targetCandidateRank1.options, document.forms[0].targetCandidateRank2.options, document.forms[0].targetCandidateRank3.options);
        loadDevBox(document.forms[0].lowerRange.options, document.forms[0].upperRange.options);


        //選択中生徒の学年によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
        gradeChange();

        //ロード中
        <%@ include file="/jsp/script/loading.jsp" %>

        <%-- アプレットを本来の位置へ移動する --%>
        $('#AppletLayer').children().prependTo('#AppletCell');
    }
//-->
</SCRIPT>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="AppletLayer" style="position:absolute;top:250px;left:450px;width:1px;height:1px;overflow:hidden;">
<!-- アプレット部：サーブレットよりVALUEを受けとるようにすること=========== -->
<%-- 2016/01/15 QQ)Hisakawa 大規模改修 DEL START --%>
<%--
    <APPLET CODEBASE="./applets/" ARCHIVE="Applet.jar" CODE="LineGraphApplet2" id="rc" WIDTH="848" HEIGHT="320">
          <%
              //アプレットで使うデータをすべて取得

              //大学
              String univName = "";
              String univDev = "";
            String dispSelect = "";	//グラフ表示／非表示
              int univCount = 0;
              for(java.util.Iterator it=i107Bean.getPrevSuccessDatas().iterator(); it.hasNext();) {
                  PrevSuccessData pdata = (PrevSuccessData)it.next();
                  if(univCount != 0) {
                      univName += ",";
                      univDev += ",";
                  } else {
                      univCount++;
                  }
                univName += pdata.getUnivName();
                if(pdata.getSucAvgDev().equals("")) {
                    univDev += "-999.0";
                } else {
                    univDev += pdata.getSucAvgDev();
                }
            }

            Collection csub = i107Bean.getSubDatas();
            java.util.Iterator cd = csub.iterator();
            boolean flag = false;
            while(cd.hasNext()) {
                SubjectData data = (SubjectData)cd.next();
                String temp = data.getSubjectCd();
                if(temp.equals(form.getTargetSubCode())) {
                    flag = true;
                    break;
                }
            }
            if(flag==true){
                dispSelect = "on";
            }else{
                dispSelect = "off";
            }

            if(i107Bean.getExaminationDataSize() <= 0){
                dispSelect = "off";
            }
          %>

        <param name="examName" value="<%=i107Bean.getExamNames()%>">
        <param name="dispSelect" value="<%=dispSelect%>">
        <param name="yTitle" value="偏差値">
        <param name="HensaZoneLow" value="<%=Float.valueOf(form.getLowerRange()).intValue()%>">
        <param name="HensaZoneHigh" value="<%=Float.valueOf(form.getUpperRange()).intValue()%>">
        <param name="BarItemNum" value="<%=i107Bean.getPrevSuccessDatas().size()%>">
        <param name="LineItemNum" value="<%=i107Bean.getTargetExamNum()%>">
        <param name="LineItemNameS" value="<%=i107Bean.getLineItemNameS()%>">
        <param name="LineItemName" value="<%=univName%>">
        <param name="examValue0" value="<%=i107Bean.getExamDeviations()%>">
        <param name="examValue1" value="<%=i107Bean.getExamDeviations2()%>">
        <param name="examValue2" value="<%=i107Bean.getExamDeviations3()%>">

        <param name="univValue" value="<%=univDev%>">
        <param name="itemTitle" value="□対象生徒偏差値,□合格者平均偏差値">
        <param name="colorDat" value="1,0,2,15,4,5,13,14">
        <param name="BarSize" value="15">
   </APPLET>
--%>
<%-- 2016/01/15 QQ)Hisakawa 大規模改修 DEL END   --%>

<!-- ======================================================================== -->
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I107Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="targetYear" value="<c:out value="${iCommonMap.targetExamYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${iCommonMap.targetExamCode}" />">
<!-- 科目タブ切り替えに使用 -->
<INPUT TYPE = "HIDDEN" NAME ="targetSubCode" VALUE = '<%=form.getTargetSubCode()%>'>
<INPUT TYPE = "HIDDEN" NAME ="targetSubName" VALUE = '<%=form.getTargetSubName()%>'>
<input type="hidden" name="printStatus" value="">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">


<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->


<!--成績分析-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--分析メニュー-->
<%@ include file="/jsp/individual/include/bunseki_menu.jsp" %>
<!--/分析メニュー-->

<!--概要-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="12" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="698">
<table border="0" cellpadding="0" cellspacing="0" width="698">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="682">

<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td colspan="5" width="682" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="682" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="682" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="666" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">科目別成績推移グラフ</b></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="682" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="682" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="150" valign="bottom">
<table border="0" cellpadding="0" cellspacing="0" width="150">
<tr>
<td align="center" height="40"><input type="button" class="text12" onclick="openSample('<c:out value="${param.forward}" />');" value="サンプルの表示"></td>
</tr>
<tr>
<td bgcolor="#8CA9BB" height="2"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
</table>
<!--/大タイトル-->

<!--模試の種類-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="340">
    <table border="0" cellpadding="3" cellspacing="3" width="340" height="164">
    <tr>
    <td bgcolor="#E1E6EB">
        <table border="0" cellpadding="3" cellspacing="0">
        <tr>
        <td><span class="text12">対象年度：</span></td>
        <td><%@ include file="/jsp/individual/include/targetExamYear.jsp" %></td>
        </tr>
        <tr>
        <td><span class="text12">対象模試：</span></td>
        <td><%@ include file="/jsp/individual/include/targetExamCode.jsp" %></td>
        </tr>
        </table>
    </td>
    </tr>
    </table>
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="502">
    <table border="0" cellpadding="3" cellspacing="3" width="502" height="164">
    <tr>
    <td bgcolor="#F4E5D6">
        <table border="0" cellpadding="0" cellspacing="0" width="410">
        <tr>
        <td>
            <div><b class="text12">&lt;&lt;オプション&gt;&gt;</b></div>
            <!--学年＆偏差値範囲-->
            <table border="0" cellpadding="3" cellspacing="0">
            <tr>
            <td width="86"><span class="text12">表示する学年：</span></td>
            <td><select name="targetGrade" size="1">
                        <option value="1">現学年のみ</option>
                        <option value="2">全ての学年</option></select></td>
            <td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
            <td><span class="text12">偏差値範囲：</span></td>
            <td><SELECT SIZE="1" NAME="lowerRange"></SELECT></td>
            <td><span class="text12">〜</span></td>
            <td><SELECT SIZE="1" NAME="upperRange"></SELECT></td>
            </tr>
            </table>
            <!--/学年＆偏差値範囲-->
            <!--志望大学-->
            <c:choose>
            <c:when test="${(iCommonMap.targetExamTypeCode == '01' || iCommonMap.targetExamTypeCode == '02' || iCommonMap.targetExamTypeCode == '03') && (iCommonMap.targetExamCode != '66' && iCommonMap.targetExamCode != '67')}">
                <table border="0" cellpadding="3" cellspacing="0">
                <tr>
                <td rowspan="3" width="86" valign="top"><span class="text12">志望大学：</span></td>
                <td><SELECT SIZE="1" NAME="targetCandidateRank1" style="width:290px;"></SELECT></td>
                </tr>
                <tr>
                <td colspan="2"><SELECT SIZE="1" NAME="targetCandidateRank2" style="width:290px;"></SELECT></td>
                </tr>
                <tr>
                <td colspan="2"><SELECT SIZE="1" NAME="targetCandidateRank3" style="width:290px;"></SELECT></td>
                </tr>
                </table>
            </c:when>
            <c:otherwise>
                <img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt="">
                <input type="hidden" name="targetCandidateRank1" value="1">
                <input type="hidden" name="targetCandidateRank2" value="2">
                <input type="hidden" name="targetCandidateRank3" value="3">
            </c:otherwise>
            </c:choose>
            <!--/志望大学-->
            <div style="margin-top:3px;"><span class="text10">※これらのオプション項目を変更した場合は、［再表示］ボタンをクリックしてください。</span></div>
        </td>
        </tr>
        </table>
    </td>
    <td width="71" bgcolor="#F4E5D6" align="center"><input type="button" value="再表示" class="text12" style="width:60px;" onclick="pushSubmitButton();"></td>
    </tr>
    </table>
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="5" width="848" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/模試の種類-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--メニュー切り替え-->
<div align="left">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<%--060130 K.Kondo 科目タブの幅調節 start--%>
<c:set var="tabNum" value="0" />
<c:forEach var="subData" items="${i107Bean.subDatas}" varStatus="status">
    <c:set var="tabNum" value="${tabNum + 1}" />
</c:forEach>

<c:choose>
    <c:when test="${ tabNum <= 9 }">
        <c:set var="colWidth" value="${ 90 }" />
    </c:when>
    <c:otherwise>
        <c:set var="colWidth" value="${ (848 - (tabNum - 1) * 4) / tabNum }" />
    </c:otherwise>
</c:choose>

<c:forEach var="subData" items="${i107Bean.subDatas}" varStatus="status">

    <c:if test="${status.index != 0}">
            <td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
    </c:if>
    <c:choose>
        <c:when test="${form.targetSubCode eq subData.subjectCd}">
            <td width="<c:out value="${ colWidth }" />" height="24">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
            <td bgcolor="#FFAD8C">
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
            <td width="100%" height="22" bgcolor="#FFAD8C" align="center"><span class="text12"><c:out value="${subData.subjectName}" /></span>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            </td>
        </c:when>
        <c:otherwise>
            <td width="<c:out value="${ colWidth }" />" height="24">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
            <td bgcolor="#8CA9BB">
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
            <td width="100%" height="22" bgcolor="#E5EEF3" align="center"><span class="text12">
            <a href="javascript:pushSubButton('<c:out value="${subData.subjectCd}" />', '<c:out value="${subData.subjectName}" />')">
            <c:out value="${subData.subjectName}" /></a></span>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            </td>
        </c:otherwise>
    </c:choose>
</c:forEach>
<%--060130 K.Kondo 科目タブの幅調節 start--%>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td bgcolor="#FFAD8C"><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
</table>
</div>

<!--/メニュー切り替え-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--グラフイメージ-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<%-- 2016/01/15 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td width="848" id="AppletCell"> --%>
<td width="848">
<%-- 2016/02/29 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<img src="./GraphServlet" width="848" height="320" border="0">
  --%>
<img src="<c:url value="./GraphServlet" />"  width="848" height="320" border="0">
<%-- 2016/02/29 QQ)Nishiyama 大規模改修 UPD END   --%>
<%-- 2016/01/15 QQ)Hisakawa 大規模改修 UPD END   --%>
<br></td>
</tr>
</table>
<!--/グラフイメージ-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<%-- 科目タブが一件もなければ非表示 --%>
<c:if test="${tabNum != 0}">
<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="852">

<%if(i107Bean.getExaminationDataSize() != 0) {%>
<!--項目-->
<tr height="22" bgcolor="#CDD7DD" align="center">
<td colspan="2" width="29%"><span class="text12">&nbsp;</span></td>
<c:forEach var="examinationData" items="${i107Bean.examinationDatas}" varStatus="status">
    <td width="9%"><span class="text12"><c:out value="${examinationData.examName}" /></span></td>
</c:forEach>
</tr>
<!--/項目-->
<%}%>

<!--選択模試-->
<%if(i107Bean.getExaminationDataSize() != 0) {%>
<tr height="36">
<td bgcolor="#E1E6EB" colspan="2"><span class="text12"><%=i107Bean.getRowName1()%></span></td>
<%}%>
<%
    int count = 0;
    for (java.util.Iterator it=i107Bean.getExaminationDatas().iterator(); it.hasNext(); ) {
            IExamData examinationData = (IExamData)it.next();
            if(examinationData != null){
                SubRecordIData subData = (SubRecordIData)examinationData.getSubRecordIData(form.getTargetSubCode());
                out.println("<td bgcolor=\"#F4E5D6\" align=\"center\"><span class=\"text12\">");
                if(subData != null && !"".equals(subData.getCDeviation())){
                    out.println(subData.getCDeviation()+ "/" + subData.getScholarlevel());
                }
                count ++;
                out.println("</span></td>");
            }
    }
    for(int i=0; i<7-count; i++){
        out.println("<td bgcolor=\"#F4E5D6\" align=\"center\"><span class=\"text12\"></span></td>");
    }
    out.println("</tr>");
%>

<%if(!(i107Bean.getTargetExamYear2() == null)) {%>
<tr height="36">
<td bgcolor="#E1E6EB" colspan="2"><span class="text12"><%=i107Bean.getRowName2()%></span></td>
<%}%>
<%
    if(!(i107Bean.getTargetExamYear2() == null)) {
        int count2 = 0;
        for (java.util.Iterator it=i107Bean.getExaminationDatas2().iterator(); it.hasNext(); ) {
                IExamData examinationData = (IExamData)it.next();
                if(examinationData != null){
                    SubRecordIData subData = (SubRecordIData)examinationData.getSubRecordIData(form.getTargetSubCode());
                    out.println("<td bgcolor=\"#F4E5D6\" align=\"center\"><span class=\"text12\">");
                    if(subData != null && !"".equals(subData.getCDeviation())){
                        out.println(subData.getCDeviation()+ "/" + subData.getScholarlevel());
                    }
                    count2 ++;
                    out.println("</span></td>");
                }
        }
        for(int i=0; i<7-count2; i++){
            out.println("<td bgcolor=\"#F4E5D6\" align=\"center\"><span class=\"text12\"></span></td>");
        }
        out.println("</tr>");
    }
%>
<%	if(!(i107Bean.getTargetExamYear3() == null)) {%>
<tr height="36">
<td bgcolor="#E1E6EB" colspan="2"><span class="text12"><%=i107Bean.getRowName3()%></span></td>
<%}%>
<%
    if(!(i107Bean.getTargetExamYear3() == null)) {
        int count3 = 0;
        for (java.util.Iterator it=i107Bean.getExaminationDatas3().iterator(); it.hasNext(); ) {
            IExamData examinationData = (IExamData)it.next();
            if(examinationData != null){
                SubRecordIData subData = (SubRecordIData)examinationData.getSubRecordIData(form.getTargetSubCode());
                out.println("<td bgcolor=\"#F4E5D6\" align=\"center\"><span class=\"text12\">");
                if(subData != null && !"".equals(subData.getCDeviation())){
                    out.println(subData.getCDeviation()+ "/" + subData.getScholarlevel());
                }
                count3 ++;
                out.println("</span></td>");
            }
        }
        for(int i=0; i<7-count3; i++){
            out.println("<td bgcolor=\"#F4E5D6\" align=\"center\"><span class=\"text12\"></span></td>");
        }
        out.println("</tr>");
    }
%>
<!--/選択模試-->

<!--大学-->
<c:forEach var="prevSuccessData" items="${i107Bean.prevSuccessDatas}" varStatus="status">
<tr height="36">
<c:if test="${status.index == 0}">
    <td bgcolor="#CDD7DD" rowspan="7" width="6%"><span class="text12">合格者<br>平均<br>偏差値</span></td>
</c:if>
<td bgcolor="#E1E6EB"><span class="text12"><c:out value="${prevSuccessData.univName}" /></span></td>
    <c:forEach var="i" begin="1" end="7" step="1">
        <td bgcolor="#F4E5D6" align="center">
            <c:if test="${i == i107Bean.examinationDataSize}">
                <span class="text12"><c:out value="${prevSuccessData.sucAvgDev}" /></span>
            </c:if>
        </td>
    </c:forEach>
</tr>
</c:forEach>
<!--/大学-->

</table>
<!--/リスト-->
</c:if>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/成績分析-->

<!--セキュリティスタンプの選択-->
<%@ include file="/jsp/individual/include/security_stamp.jsp" %>
<!--/セキュリティスタンプの選択-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--印刷対象生徒-->
<%@ include file="/jsp/individual/include/printStudent.jsp" %>
<!--/印刷対象生徒-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--ケイコさん-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="784">
<tr valign="top">
<td width="72" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="702">

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="678" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="678" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="678" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="678" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr>
<td width="12" background="./shared_lib/img/parts/wb_arrow_l_long.gif"><img src="./shared_lib/img/parts/sp.gif" width="12" height="5" border="0" alt=""><br></td>
<td width="688" bgcolor="#F9EEE5">
<!--アドバイス-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="688">
<tr valign="top">
<td width="131" align="center"><img src="./shared_lib/img/parts/word_one_point.gif" width="86" height="32" border="0" alt="ワンポイントアドバイス！！" vspace="5"><br></td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="5" border="0" alt=""><br></td>
<td width="511">


<div style="margin-top:5px;margin-bottom:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="511">
<%@ include file="/jsp/shared_lib/I_Onepoint.jsp" %>
</table>
</div>


</td>
<td width="25"><img src="./shared_lib/img/parts/sp.gif" width="25" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/アドバイス-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="678" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="678" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="678" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="678" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>

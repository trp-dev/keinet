<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.keinavi.data.individual.ExaminationData" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<jsp:useBean id="form" scope="request" class="jp.co.fj.keinavi.forms.individual.I401Form" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<%-- 2019/07/09 QQ)Tanioka ダウンロード変更対応 ADD START --%>
<script type="text/javascript" src="./js/jquery.blockUI.js"></script>
<%-- 2019/07/09 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
<SCRIPT type="text/javascript">
<!--
	<%-- 2019/07/09 QQ)Tanioka ダウンロード変更対応 ADD START --%>
	<%@ include file="/jsp/script/download.jsp" %>
	<%-- 2019/07/09 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>
	<%@ include file="/jsp/script/open_sample.jsp" %>
    <%@ include file="/jsp/script/submit_help.jsp" %>
    <%@ include file="/jsp/script/submit_help.jsp" %>
    <%@ include file="/jsp/script/clear_disabled.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/printDialog_close.jsp" %>
	 var printDialogPattern = "";
	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

	<%-- 対象模試リスト --%>
	<c:set var="examList" value="${ iCommonMap.examComboData.examList }" />

	/**
	 * サブミット
	 */
	function submitForm(forward) {
		clearDisabled();
		<%--スクロール位置 0426 K.Kondo--%>
		if(forward=='<c:out value="${param.forward}" />') {
			document.forms[0].scrollX.value = document.body.scrollLeft;
			document.forms[0].scrollY.value = document.body.scrollTop;
		} else {
			document.forms[0].scrollX.value = 0;
			document.forms[0].scrollY.value = 0;
		}
		document.getElementById("MainLayer").style.visibility = "hidden";
		document.getElementById("MainLayer").style.top = -2000;
		document.getElementById("LoadingLayer").style.visibility = "visible";
		document.forms[0].target = "_self";
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 戻るボタン
	 */
	function submitBack() {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "i003";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 生徒切り替え
	 */
	<%@ include file="/jsp/individual/include/student_rotator.jsp" %>
	/**
	 * プリントダイアログ
	 */
	<%@ include file="/jsp/individual/include/printDialog.jsp" %>
	/**
	 * 個人成績共通項目-印刷確認ダイアログ(i401のscript)
	 */
	function printCheck(Forward){

		flag = 0;
		var form = document.forms[0];
		var noPrint = form.interviewSheet[0]; //印刷しない
		var iv1 = form.interviewSheet[1]; //面談シート１
		var iv2 = form.interviewSheet[2]; //面談シート２
		var iv3 = form.interviewSheet[3]; //面談シート３
		var iv4 = form.interviewSheet[4]; //面談シート４
		var kyo = form.interviewForms[0]; //教科分析
		var sub = form.interviewForms[1]; //科目別成績
		var que = form.interviewForms[2]; //設問別成績
		var rep = form.report; //連続個人成績表
		var edc = form.examDoc; //長期休暇スケジュール

		if (noPrint.checked) { /*何もしなくて良い*/ }
		if (iv1.checked && !iv1.disabled) { flag = 1; }
		if (iv2.checked) { flag = 1; }
		if (iv3.checked) { flag = 1; }
		if (iv4.checked) { flag = 1; }
		if (kyo.checked && !kyo.disabled) { flag = 1; }
		if (sub.checked && !sub.disabled) { flag = 1; }
		if (que.checked && !que.disabled) { flag = 1; }
		if (rep.checked) { flag = 1; }
		if (edc.checked && !edc.disabled) { flag = 1; }

		if (flag !=1) {
			// 2020/2/13 QQ)Ooseto 共通テスト対応 UPD START
			//window.alert("印刷したい資料を選択してください。");
			window.alert("保存したい資料を選択してください。");
			// 2020/2/13 QQ)Ooseto 共通テスト対応 UPD END
		} else {
			<%-- 2019/07/09 QQ)Tanioka ダウンロード変更対応 UPD START --%>
			//printDialog(true);
			download(1);
			<%-- 2019/07/09 QQ)Tanioka ダウンロード変更対応 UPD END   --%>
		}
	}

	/**
	 * 初期化
	 */
	function init(){
		startTimer();
		<%--スクロール位置 0426 K.Kondo--%>
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
		//1.選択中の生徒まで回す
		findStudent();
		//2.選択中の生徒が一人ならArrowボタンを無効化する
		if(getStudentNum() == 1) disableArrows();

		//選択中生徒の学年によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
		gradeChange();

		//ロード中
		<%@ include file="/jsp/script/loading.jsp" %>
	}
//-->
</script>
</head>
<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I401Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="targetYear" value="<c:out value="${iCommonMap.targetExamYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${iCommonMap.targetExamCode}" />">
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="printStatus" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->

<!--面談資料かんたん印刷-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--概要-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="right">
	<table border="0" cellpadding="3" cellspacing="0">
	<tr>
	<td><span class="text12">対象年度：</span></td>
	<td><%@ include file="/jsp/individual/include/targetExamYear.jsp" %></td>
	<td><span class="text12">対象模試：</span></td>
	<td><%@ include file="/jsp/individual/include/targetExamCode.jsp" %></td>
	</tr>
	</table>
</td>
</tr>
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="4" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="851">
<tr valign="top">
<td width="21"><img src="./shared_lib/img/parts/sp.gif" width="21" height="1" border="0" alt=""><br></td>
<td width="830">
	<table border="0" cellpadding="0" cellspacing="0" width="830">
	<tr>
	<td>
		<!--ケイコさん-->
		<table border="0" cellpadding="0" cellspacing="0" width="520">
		<tr valign="top">
		<td width="52" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
		<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
		<td width="458">

		<table border="0" cellpadding="0" cellspacing="0" width="458">
		<tr valign="top">
		<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
		<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
		<td width="434" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="434" height="2" border="0" alt=""><br></td>
		<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
		</tr>
		<tr valign="top">
		<td width="434" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="434" height="5" border="0" alt=""><br></td>
		</tr>
		</table>

		<table border="0" cellpadding="0" cellspacing="0" width="458">
		<tr>
		<td width="12">
		<table border="0" cellpadding="0" cellspacing="0" width="12">
		<tr valign="top">
		<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="18" border="0" alt=""><br></td>
		<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
		</tr>
		<tr valign="top">
		<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
		</tr>
		<tr valign="top">
		<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="18" border="0" alt=""><br></td>
		<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
		</tr>
		</table>
		</td>
		<td width="444" bgcolor="#F9EEE5">
		<!--文言-->
		<table border="0" cellpadding="0" cellspacing="0" width="444">
		<tr>
		<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
		<td width="414"><span class="text14">以下から印刷したい資料を選択し、「印刷」を行ってください。<br>
		面談資料を個別で印刷される場合は、<a href="javascript:submitForm('i101')">成績分析</a>より行ってください。</span></td>
		<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
		</tr>
		</table>
		<!--/文言-->
		</td>
		<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
		</tr>
		</table>

		<table border="0" cellpadding="0" cellspacing="0" width="458">
		<tr valign="top">
		<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
		<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
		<td width="434" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="434" height="5" border="0" alt=""><br></td>
		<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
		</tr>
		<tr valign="top">
		<td width="434" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="434" height="2" border="0" alt=""><br></td>
		</tr>
		</table>

		</td>
		</tr>
		</table>
		<!--/ケイコさん-->
	</td>
	<td align="right">
		<!--印刷対象生徒-->
		<table border="0" cellpadding="0" cellspacing="0">
		<tr valign="top">
		<td bgcolor="#8CA9BB">
		<table border="0" cellpadding="7" cellspacing="1">
		<tr valign="top">
		<td bgcolor="#FBD49F" align="center">

		<table border="0" cellpadding="0" cellspacing="0">
		<c:if test="${sessionScope['iCommonMap'].bunsekiMode}">
		<tr>
		<td>
			<select name="printStudent" class="text12" style="width:156;">
			<option  value="1"<c:if test="${ form.printStudent == '1'}">selected</c:if>>選択中の生徒全員</option>
			<option  value="2"<c:if test="${ form.printStudent == '2'}">selected</c:if>>表示中の生徒一人</option>
			</select>
		</td>
		</tr>
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="7" border="0" alt=""><br></td>
		</tr>
		</c:if>
		<tr>
		<td align="center">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr valign="top">
			<td bgcolor="#8CA9BB">
			<table border="0" cellpadding="6" cellspacing="1">
			<tr valign="top">
			<td bgcolor="#FFFFFF" align="center"><a href="#here" onclick="printCheck();"><img src="./shared_lib/img/btn/hozon.gif" width="114" height="35" border="0" alt="保存"></a><br></td>
			</tr>
			</table>
			</td>
			</tr>
			</table>
		</td>
		</tr>
		</table>

		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		<!--/印刷対象生徒-->
	</td>
	</tr>
	</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="851">
<tr valign="top">
<td width="21"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="570">
<!--成績分析面談シート-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr>
<td width="570" bgcolor="#758A98">
<table border="0" cellpadding="0" cellspacing="0" height="27">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td><b class="text16" style="color:#FFFFFF;">成績分析面談シート</b></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="64" height="1" border="0" alt=""><br></td>
<td><input type="radio" name="interviewSheet" value="1" <c:if test="${(form.interviewSheet == '1')}">checked</c:if>></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
<td><b class="text14" style="color:#FFFFFF;">印刷しない</b></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="570"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>


<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="570">
<tr valign="top">
<td width="568" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="568">
<tr>
<td colspan="9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br></td>
</tr>
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td>
<!--シート1-->
<c:set var="bgcolor" value="#E5EEF3"/>
<c:set var="bgcolor2" value="#FFFFFF"/>
<c:set var="disabled" value="false"/>
<kn:choose>
	<%--受験学力測定--%>
	<kn:examWhen id="ability" var="${iCommonMap.examData}">
		<c:set var="bgcolor" value="#E1E6EB"/>
		<c:set var="bgcolor2" value="#E1E6EB"/>
		<c:set var="disabled" value="true"/>
	</kn:examWhen>
	<%--パターンI--%>
	<kn:examWhen id="patternI" var="${iCommonMap.examData}">
		<c:set var="bgcolor" value="#E5EEF3"/>
		<c:set var="bgcolor2" value="#FFFFFF"/>
		<c:set var="disabled" value="false"/>
	</kn:examWhen>
	<kn:otherwise>
		<c:set var="bgcolor" value="#E1E6EB"/>
		<c:set var="bgcolor2" value="#E1E6EB"/>
		<c:set var="disabled" value="true"/>
	</kn:otherwise>
</kn:choose>
<table border="0" cellpadding="0" cellspacing="0" width="132">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="4" cellspacing="1" width="132">
<tr>
<td width="94" bgcolor="<c:out value="${bgcolor}"/>">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="radio" name="interviewSheet" value="5"
	<c:if test="${form.interviewSheet == '5'}">checked</c:if>
	<c:if test="${disabled}">disabled</c:if>
></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
<td><span class="text12">シート1</span></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="132" bgcolor="<c:out value="${bgcolor2}"/>" align="center">
	<table border="0" cellpadding="0" cellspacing="7">
	<tr>
	<td><img src="./individual/img/graph00.gif" width="106" height="66" border="0" alt="シート1"><br></td>
	</tr>
	<tr>
	<td align="center"><span class="text12"><a href="javascript:openSample('i410')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span></td>
	</tr>
	</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/シート1-->
</td>
<td><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td>
<!--シート2-->
<table border="0" cellpadding="0" cellspacing="0" width="132">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="4" cellspacing="1" width="132">
<tr>
<td width="94" bgcolor="#E5EEF3">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="radio" name="interviewSheet" value="2" <c:if test="${form.interviewSheet == '2'}">checked</c:if>></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
<td><span class="text12">シート2</span></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="132" bgcolor="#FFFFFF" align="center">
<table border="0" cellpadding="0" cellspacing="7">
<tr>
<td><img src="./individual/img/graph01.gif" width="106" height="66" border="0" alt="シート2"><br></td>
</tr>
<tr>
<td align="center"><span class="text12"><a href="javascript:openSample('i411')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/シート2-->
</td>
<td><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td>
<!--シート3-->
<table border="0" cellpadding="0" cellspacing="0" width="132">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="4" cellspacing="1" width="132">
<tr>
<td width="94" bgcolor="#E5EEF3">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="radio" name="interviewSheet" value="3" <c:if test="${form.interviewSheet == '3'}">checked</c:if>></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
<td><span class="text12">シート3</span></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="132" bgcolor="#FFFFFF" align="center">
<table border="0" cellpadding="0" cellspacing="7">
<tr>
<td><img src="./individual/img/graph02.gif" width="106" height="66" border="0" alt="シート3"><br></td>
</tr>
<tr>
<td align="center"><span class="text12"><a href="javascript:openSample('i412')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/シート3-->
</td>
<td><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td>
<!--シート4-->
<table border="0" cellpadding="0" cellspacing="0" width="132">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="4" cellspacing="1" width="132">
<tr>
<td width="94" bgcolor="#E5EEF3">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="radio" name="interviewSheet" value="4" <c:if test="${form.interviewSheet == '4'}">checked</c:if>></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
<td><span class="text12">シート4</span></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="132" bgcolor="#FFFFFF" align="center">
<table border="0" cellpadding="0" cellspacing="7">
<tr>
<td><img src="./individual/img/graph03.gif" width="106" height="66" border="0" alt="シート4"><br></td>
</tr>
<tr>
<td align="center"><span class="text12"><a href="javascript:openSample('i413')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/シート4-->
</td>
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/成績分析面談シート-->
</td>
<td width="6" rowspan="3"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="254" rowspan="3" style="border:1px solid #8CA9BB;background-color:#EFF2F3;padding:6px;">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="42"><img src="./shared_lib/img/parts/icon_hyoji.gif" width="30" height="30" border="0" alt="表示・印刷形式の選択" hspace="3"></td>
<td><b class="text14" style="color:#657681;">表示・印刷形式の選択</b></td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1">
<tr valign="top">
<td bgcolor="#FFFFFF">

<div style="margin:8px;">
<span class="text12">連続個人成績表・<br>教科／科目別成績推グラフの対象学年</span>
</div>

<div style="margin:8px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="834"><b class="text14-hh">出力する学年</b></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>

<div style="margin:8px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""></td>
<td><input type="radio" name="targetGrade" value="2"<c:if test="${ form.targetGrade == '2' }"> checked</c:if>></td>
<td><span class="text12">全ての学年</span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""></td>
<td><input type="radio" name="targetGrade" value="1"<c:if test="${ form.targetGrade == '1' }"> checked</c:if>></td>
<td><span class="text12">現学年のみ</span></td>
</tr>
</table>
</div>

</td>
</tr>
</table>
</td>
</tr>
</table>

<div style="margin:8px;">
<b class="text14">印刷に際しては常に以下が対象となります。<br></b>
<span class="text10"><br>
◎バランスチャートで扱う模試は選択中の模試と同じ学年の全ての種類のもの。<br><br>
◎バランスチャート（合格者平均）で扱う模試は選択中の模試、かつ志望順位３位までの大学。<br><br>
◎科目別成績推移で扱う模試は選択中の模試と同じ種類のもの。<br><br>
◎設問別成績で扱う科目は選択中の模試のもの。<br><br>
◎バランスチャート、バランスチャート（合格者平均）、教科別成績推移で同一教科内で複数の科目を受験している教科の偏差値は下記のように表示。<br>
<br>
○第２・第３回全統マーク、センタープレテスト、<br>
　&nbsp;センター・リサーチ　　→　第１解答科目<br>
<br>
○上記以外の模試　　→　各教科の平均<br></span>
</div>

</td>
</tr>
<td>
<td colspan="4"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="21"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="570">
<!--その他-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr>
<td width="570" bgcolor="#758A98">
<table border="0" cellpadding="0" cellspacing="0" height="27">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td><b class="text16" style="color:#FFFFFF;">その他</b></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="570"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>


<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="570">
<tr valign="top">
<td width="568" bgcolor="#FFFFFF" align="center">
<!--面談用帳票-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr>
<td width="99" align="center"><b class="text14" style="color:#657681">面談用帳票</b></td>
<td width="142">
<!--教科分析シート-->
<c:set var="bgcolor" value="#E5EEF3"/>
<c:set var="bgcolo2" value="#FFFFFF"/>
<c:set var="disabled" value="false"/>
<kn:choose>
	<%--受験学力測定--%>
	<kn:examWhen id="ability" var="${iCommonMap.examData}">
		<c:set var="bgcolor" value="#E1E6EB"/>
		<c:set var="bgcolor2" value="#E1E6EB"/>
		<c:set var="disabled" value="true"/>
	</kn:examWhen>
	<kn:otherwise>
		<c:set var="bgcolor" value="#E5EEF3"/>
		<c:set var="bgcolor2" value="#FFFFFF"/>
		<c:set var="disabled" value="false"/>
	</kn:otherwise>
</kn:choose>
<table border="0" cellpadding="0" cellspacing="0" width="142">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="142">
<tr>
<td width="142" bgcolor="<c:out value="${bgcolor}"/>">
<table border="0" cellpadding="0" cellspacing="4">
<tr>
<td><input type="checkbox" name="interviewForms" value="0"
	<c:if test="${form.interviewForms[0] == '1'}">checked</c:if>
	<c:if test="${disabled}">disabled</c:if>
></td>
<td><span class="text12">教科分析シート</span></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="142" bgcolor="<c:out value="${bgcolor2}"/>" align="center">
<table border="0" cellpadding="0" cellspacing="5">
<tr>
<td><img src="./individual/img/graph04.gif" width="106" height="66" border="0" alt="教科分析シート"><br></td>
</tr>
<tr>
<td align="center"><span class="text12"><a href="javascript:openSample('i414')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/教科分析シート-->
</td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="142">
<!--成績推移グラフ-->
<c:set var="bgcolor" value="#E5EEF3"/>
<c:set var="bgcolo2" value="#FFFFFF"/>
<c:set var="disabled" value="false"/>
<kn:choose>
	<%--受験学力測定--%>
	<kn:examWhen id="ability" var="${iCommonMap.examData}">
		<c:set var="bgcolor" value="#E1E6EB"/>
		<c:set var="bgcolor2" value="#E1E6EB"/>
		<c:set var="disabled" value="true"/>
	</kn:examWhen>
	<kn:otherwise>
		<c:set var="bgcolor" value="#E5EEF3"/>
		<c:set var="bgcolor2" value="#FFFFFF"/>
		<c:set var="disabled" value="false"/>
	</kn:otherwise>
</kn:choose>
<table border="0" cellpadding="0" cellspacing="0" width="142">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="142">
<tr>
<td width="142" bgcolor="<c:out value="${bgcolor}"/>">
<table border="0" cellpadding="0" cellspacing="4">
<tr>
<td><input type="checkbox" name="interviewForms" value="1"
	<c:if test="${form.interviewForms[1] == '1'}">checked</c:if>
	<c:if test="${disabled}">disabled</c:if>
></td>
<td><span class="text12">成績推移グラフ</span></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="142" bgcolor="<c:out value="${bgcolor2}"/>" align="center">
<table border="0" cellpadding="0" cellspacing="5">
<tr>
<td><img src="./individual/img/graph05.gif" width="106" height="66" border="0" alt="成績推移グラフ"><br></td>
</tr>
<tr>
<td align="center"><span class="text12"><a href="javascript:openSample('i415')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/成績推移グラフ-->
</td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="142">
<!--設問別成績グラフ-->
<c:set var="bgcolor" value="#E5EEF3"/>
<c:set var="bgcolo2" value="#FFFFFF"/>
<c:set var="disabled" value="false"/>
<kn:choose>
	<%--受験学力測定・センターリサーチ以外--%>
	<kn:examNotWhen id="patternIV" var="${iCommonMap.examData}">
		<c:set var="bgcolor" value="#E1E6EB"/>
		<c:set var="bgcolor2" value="#E1E6EB"/>
		<c:set var="disabled" value="true"/>
	</kn:examNotWhen>
	<kn:otherwise>
		<c:set var="bgcolor" value="#E5EEF3"/>
		<c:set var="bgcolor2" value="#FFFFFF"/>
		<c:set var="disabled" value="false"/>
	</kn:otherwise>
</kn:choose>
<table border="0" cellpadding="0" cellspacing="0" width="142">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="142">
<tr>
<td width="142" bgcolor="<c:out value="${bgcolor}"/>">
<table border="0" cellpadding="0" cellspacing="4">
<tr>
<td><input type="checkbox" name="interviewForms" value="2"
	<c:if test="${form.interviewForms[2] == '1'}">checked</c:if>
	<c:if test="${disabled}">disabled</c:if>
></td>
<td><span class="text12">設問別成績グラフ</span></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="142" bgcolor="<c:out value="${bgcolor2}"/>" align="center">
<table border="0" cellpadding="0" cellspacing="5">
<tr>
<td><img src="./individual/img/graph06.gif" width="106" height="66" border="0" alt="成績推移グラフ"><br></td>
</tr>
<tr>
<td align="center"><span class="text12"><a href="javascript:openSample('i416')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/設問別成績グラフ-->
</td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/面談用帳票-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr>
<td width="550"><img src="./shared_lib/img/parts/dot_blue.gif" width="550" height="1" border="0" alt="" vspace="6"></td>
</tr>
</table>
<!--/ボーダー-->

<!--個人成績表-->
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr>
<td width="99" align="center"><b class="text14" style="color:#657681">個人成績表</b></td>
<td width="142">
<!--連続個人成績表-->
<table border="0" cellpadding="0" cellspacing="0" width="142">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="142">
<tr>
<td width="142" bgcolor="#E5EEF3">
<table border="0" cellpadding="0" cellspacing="4">
<tr>
<td><input type="checkbox" name="report" value="1" <c:if test="${form.report == '1'}">checked</c:if>></td>
<td><span class="text12">連続個人成績表</span></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="142" bgcolor="#FFFFFF" align="center">
<table border="0" cellpadding="0" cellspacing="5">
<tr>
<td><img src="./individual/img/graph07.gif" width="106" height="67" border="0" alt="連続個人成績表"><br></td>
</tr>
<tr>
<td align="center"><span class="text12"><a href="javascript:openSample('i409')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/連続個人成績表-->
</td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="99" align="center"><b class="text14" style="color:#657681">受験用資料</b></td>
<td width="142">
<!--志望大学判定一覧-->
<c:set var="bgcolor" value="#E5EEF3"/>
<c:set var="bgcolor2" value="#FFFFFF"/>
<c:set var="disabled" value="false"/>
<kn:choose>
	<%--受験学力測定--%>
	<kn:examWhen id="ability" var="${iCommonMap.examData}">
		<c:set var="bgcolor" value="#E1E6EB"/>
		<c:set var="bgcolor2" value="#E1E6EB"/>
		<c:set var="disabled" value="true"/>
	</kn:examWhen>
	<%--パターンII--%>
	<kn:examWhen id="patternII" var="${iCommonMap.examData}">
		<c:set var="bgcolor" value="#E5EEF3"/>
		<c:set var="bgcolor2" value="#FFFFFF"/>
		<c:set var="disabled" value="false"/>
	</kn:examWhen>
	<kn:otherwise>
		<c:set var="bgcolor" value="#E1E6EB"/>
		<c:set var="bgcolor2" value="#E1E6EB"/>
		<c:set var="disabled" value="true"/>
	</kn:otherwise>
</kn:choose>
<table border="0" cellpadding="0" cellspacing="0" width="142">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="142">
<tr>
<td width="142" bgcolor="<c:out value="${bgcolor}"/>">
<table border="0" cellpadding="0" cellspacing="4">
<tr>
<td><input type="checkbox" name="examDoc" value="1"
	<c:if test="${form.examDoc == '1'}">checked</c:if>
	<c:if test="${disabled}">disabled</c:if>
></td>
<td><span class="text12">志望大学判定一覧</span></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="142" bgcolor="<c:out value="${bgcolor2}"/>" align="center">
<table border="0" cellpadding="0" cellspacing="5">
<tr>
<td><img src="./individual/img/graph09.gif" width="106" height="67" border="0" alt="志望大学判定一覧"><br></td>
</tr>
<tr>
<td align="center"><span class="text12"><a href="javascript:openSample('i418')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/志望大学判定一覧-->
</td>
<td width="58"><img src="./shared_lib/img/parts/sp.gif" width="58" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/個人成績表-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/その他-->
</td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/面談資料かんたん印刷-->

<!--セキュリティスタンプの選択-->
<%@ include file="/jsp/individual/include/security_stamp.jsp" %>
<!--/セキュリティスタンプの選択-->


</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>

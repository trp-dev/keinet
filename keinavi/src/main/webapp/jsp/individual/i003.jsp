<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<jsp:useBean id="i003Bean" scope="request" class="jp.co.fj.keinavi.beans.individual.I003Bean" />
<jsp:useBean id="i003Form" scope="request" class="jp.co.fj.keinavi.forms.individual.I003Form" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript" src="./js/jquery.blockUI.js"></script>
<script type="text/javascript">
<!--
	<%@ include file="/jsp/script/download.jsp" %>
    <%@ include file="/jsp/script/timer.jsp" %>
    <%@ include file="/jsp/script/submit_change.jsp" %>
    <%@ include file="/jsp/script/submit_exit.jsp" %>
    <%@ include file="/jsp/script/submit_save.jsp" %>
    <%@ include file="/jsp/script/submit_menu.jsp" %>
    <%@ include file="/jsp/script/open_common.jsp" %>
	<%@ include file="/jsp/script/open_sample.jsp" %>
    <%@ include file="/jsp/script/clear_disabled.jsp" %>

    // ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>

    <%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/dialog_close.jsp" %>

    var confirmDialogPattern = "";
    <%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
	 var printDialogPattern = "";

    /**
     * サブミッション
     */
    function submitForm(forward) {
        <%--スクロール位置 0426 K.Kondo--%>
        if(forward=='<c:out value="${param.forward}" />') {
            document.forms[0].scrollX.value = document.body.scrollLeft;
            document.forms[0].scrollY.value = document.body.scrollTop;
        } else {
            document.forms[0].scrollX.value = 0;
            document.forms[0].scrollY.value = 0;
        }
        document.getElementById("MainLayer").style.visibility = "hidden";
        document.getElementById("MainLayer").style.top = -2000;
        document.getElementById("LoadingLayer").style.visibility = "visible";
        document.forms[0].target = "_self";
        if (document.forms[0].targetExamYear.selectedIndex >= 0)
            document.forms[0].elements['targetExamName'].value = document.forms[0].targetExamCode.options[document.forms[0].targetExamCode.selectedIndex].text;
        document.forms[0].forward.value = forward;
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 戻るボタン
     */
    function submitBack() {
        document.getElementById("MainLayer").style.visibility = "hidden";
        document.getElementById("MainLayer").style.top = -2000;
        document.getElementById("LoadingLayer").style.visibility = "visible";
        document.forms[0].target = "_self";
        document.forms[0].forward.value = "i002";
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 無効模試選択時にクリックした場合の、ボタン無効用リンク
     */
    function submitdame() {}
    /**
     * 模試コンボ
     */
    <%=iCommonMap.getExamComboData().getYearString()%>
    var examString = new Array();
    <%
        for(int i=0; i<iCommonMap.getExamComboData().getExamStrings().length; i++){
            out.println(iCommonMap.getExamComboData().getExamStrings()[i]);
        }
    %>
    /**
     * 対象年度ロード
     */
    function loadTargetExamYear(){
        create(document.forms[0].targetExamYear, yearString);
        loadTargetExamCode();
    }
    /**
     * 対象模試ロード
     */
    function loadTargetExamCode(){

        for(var i=0; i<yearString.length; i++){
            if(document.forms[0].targetExamYear.options[document.forms[0].targetExamYear.selectedIndex].value == yearString[i][0]){
                create(document.forms[0].targetExamCode, examString[i]);
            }
        }
        setExamTypeCd();
    }
    /**
     * 動的コンボ作成
     */
<%-- 2015/12/24 QQ)Hisakawa 大規模改修 UPD START --%>
    function create(objList, objArray){
        var nMax = objArray.length;
        var nLoop = 0;
        for (nLoop = 0; nLoop < nMax; nLoop++){
            oAdd = document.createElement('OPTION');
            if(objList.children[nLoop]  != undefined)
                objList.removeChild(objList.children[nLoop]);
            objList.appendChild(oAdd);
            objList.children[nLoop].setAttribute('value', objArray[nLoop][0]);

            if(objList == document.forms[0].elements['targetExamYear'] && objArray[nLoop][0] == '<c:out value="${iCommonMap.targetExamYear}" />'){
                objList.children[nLoop].setAttribute('selected', true);
            }else if(objList == document.forms[0].elements['targetExamCode'] && objArray[nLoop][0] == '<c:out value="${iCommonMap.targetExamCode}" />'){
                objList.children[nLoop].setAttribute('selected', true);
            }

            oAddx= document.createTextNode(objArray[nLoop][1]);
            if(objList.children[nLoop].firstChild  != undefined)
                objList.children[nLoop].removeChild(objList.children[nLoop].firstChild);
            objList.children[nLoop].appendChild(oAddx);
        }
        objList.length=nLoop;
    }
<%-- 2015/12/24 QQ)Hisakawa 大規模改修 UPD END   --%>
    /**
     * 生徒切り替え
     */
    <%@ include file="/jsp/individual/include/student_rotator.jsp" %>
    /**
     * 下記の条件の時大学判定リンクをグレー表示
     */
    var examTypeCd;
    function setExamTypeCd(){
        var examObj = examString[document.forms[0].targetExamYear.selectedIndex];
        for(var i=0; i<examObj.length; i++){
            if(i == document.forms[0].targetExamCode.selectedIndex){
                examTypeCd = examObj[i][2];
                examCd     = examObj[i][0];
            }
        }
    }
    /**
     * 共通項目設定に渡す模試年度を動的に変更する
     */
    function setHiddenYear(self){
        if (document.forms[0].targetExamYear.selectedIndex >= 0)
            document.forms[0].targetYear.value = document.forms[0].targetExamYear.options[document.forms[0].targetExamYear.selectedIndex].value;
    }
    /**
     * 共通項目設定に渡す模試コードを動的に変更する
     */
    function setHiddenExam(self){
        if (document.forms[0].targetExamYear.selectedIndex >= 0)
            document.forms[0].targetExam.value = document.forms[0].targetExamCode.options[document.forms[0].targetExamCode.selectedIndex].value;
    }
	/**
	 * 個人成績共通項目-印刷確認ダイアログ(i401のscript)
	 */
	function printCheck(Forward){

		flag = 0;
		var form = document.forms[0];
		var iv1 = form.interviewSheet[0]; //面談シート１
		var iv2 = form.interviewSheet[1]; //面談シート２
		var kyo = form.interviewForms[0]; //教科分析
		var sub = form.interviewForms[1]; //科目別成績
		var que = form.interviewForms[2]; //設問別成績
		var rep = form.report; //連続個人成績表

		if (iv1.checked && !iv1.disabled) { flag = 1; }
		if (iv2.checked) { flag = 1; }
		if (kyo.checked && !kyo.disabled) { flag = 1; }
		if (sub.checked && !sub.disabled) { flag = 1; }
		if (que.checked && !que.disabled) { flag = 1; }
		if (rep.checked) { flag = 1; }

		if (flag !=1) {
            window.alert("保存したい資料を選択してください。");
		} else {
            download(1);
		}
        return false;
	}
    /**
     * 初期化
     */
    function init(){
        startTimer();
        <%--スクロール位置 0426 K.Kondo--%>
        window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

        //1.選択中の生徒まで回す
        findStudent();
        //2.選択中の生徒が一人ならArrowボタンを無効化する
        if(getStudentNum() == 1) disableArrows();

        //1.選択中の生徒の学年によって表示を変える(個人手帳リンク出す・出さない)

        //4.模試コンボボックスのロード
        if(examString.length > 0)
            loadTargetExamYear();

        //5.ロード中
        <%@ include file="/jsp/script/loading.jsp" %>
    }
// -->
</script>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I003Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">

<input type="hidden" name="targetYear" value="<c:out value="${iCommonMap.targetExamYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${iCommonMap.targetExamCode}" />">
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="selectGrade" value="<c:out value="${iCommonMap.selectedGrade}" />">

<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${i003Form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${i003Form.scrollY}" />">

<input type="hidden" name="printFlag" value="">
<input type="hidden" name="printStatus" value="">
<input type="hidden" name="mode" value="">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="387"><img src="./individual/img/ttl_select_menu.gif" width="141" height="23" border="0" alt="メニュー選択"><br></td>
<td width="551">
<!--選択方法-->
<table border="0" cellpadding="0" cellspacing="0" width="551" height="38">
<tr>
<td rowspan="3" width="8"><img src="./individual/img/tbl_l.gif" width="8" height="38" border="0" alt=""><br></td>
<td width="542" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="542" height="1" border="0" alt=""><br></td>
<td rowspan="3" width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="38" border="0" alt=""><br></td>
</tr>
<tr>
<td width="542" bgcolor="#FAFAFA">
<table border="0" cellpadding="0" cellspacing="0" width="542">
<tr height="30">

<!-- モードの判別-->
<c:choose>
<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
    <td width="180" bgcolor="#8CA9BB" align="center"><b class="text14" style="color:#FFFFFF">選択中の生徒&nbsp;→&nbsp;
    <c:out value="${sessionScope['iCommonMap'].selectedIndividualsSize}" />人</b></td>
</c:when>
<c:otherwise>
    <td width="180" bgcolor="#8CA9BB" align="center"><b class="text14" style="color:#FFFFFF">選択中の生徒</b></td>
</c:otherwise>
</c:choose>
<!--/ モードの判別-->
<%--0323 kondo　クラスコンボ　生徒選択コンボ追加--%>
<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="30" border="0" alt=""><br></td>
<td width="350" bgcolor="#F9EEE5">
<table border="0" cellpadding="2" cellspacing="0" width="350">
<tr height="30">
<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>

<c:choose>
<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
<td width="70" align="center"><b class="text12-lh"><span id="personGrade">不明</span>
<td width="80"><select name="selectClass" class="text12-lh" style="width:77px;" onchange="createStudentList();changeAndGo();"></select></td>
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD START --%>
<%--
<td width="30"><input type="button" name="leftArrow" value="&nbsp;＜&nbsp;" class="text12" style="width:27px;height:19px;" onclick="roteteLeft();"></td>
<td width="160"><select name="selectStudent" class="text12-lh" style="width:157px;" onchange="showStudents();"></select></td>
<td width="30"><input type="button" name="rightArrow" value="&nbsp;＞&nbsp;" class="text12" style="width:27px;height:19px;" onclick="roteteRight();"></td>
--%>
<td width="30"><input type="button" name="leftArrow" value="&nbsp;＜&nbsp;" class="text12" style="width:27px;height:19px;padding:0px" onclick="roteteLeft();"></td>
<td width="160"><select name="selectStudent" class="text12-lh" style="width:157px;" onchange="changeAndGo();"></select></td>
<td width="30"><input type="button" name="rightArrow" value="&nbsp;＞&nbsp;" class="text12" style="width:27px;height:19px;padding:0px" onclick="roteteRight();"></td>
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD END   --%>
</c:when>
<c:otherwise>
<td width="347" align="center"><b class="text12-lh">
<c:out value="${sessionScope['iCommonMap'].selectedIndividuals[0].studentGrade}" />年
<c:out value="${sessionScope['iCommonMap'].selectedIndividuals[0].studentClass}" />ｸﾗｽ
<c:out value="${sessionScope['iCommonMap'].selectedIndividuals[0].studentClassNo}" />番
<c:out value="${sessionScope['iCommonMap'].selectedIndividuals[0].studentNameKana}" />
</b></td>
</c:otherwise>
</c:choose>

<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="30" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<tr>
<%-- 2016/05/06 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td width="542" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="542" height="1" border="0" alt=""><br></td> --%>
<td width="542" height="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="542" height="1" border="0" alt=""><br></td>
<%-- 2016/05/06 QQ)Hisakawa 大規模改修 UPD END   --%>
</tr>
</table>
<!--/選択方法-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--メニュー選択-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="878">
<tr valign="top">
<td width="876" bgcolor="#FFFFFF" align="center">
<!--ケイコさん-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr valign="top">
<td width="72" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="508">

<table border="0" cellpadding="0" cellspacing="0" width="508">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="484" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="484" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="484" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="484" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="508">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="18" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="18" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="494" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="494">
<tr>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="464"><span class="text14">個々の成績を画面で確認／出力する場合は「成績分析」が、<br>
面談資料のみ必要な場合は「かんたん出力」が便利です。
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="508">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="484" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="484" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="484" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="484" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--ツールバー-->
<table border="0" cellpadding="4" cellspacing="0" width="840">
<tr>
<td width="840" bgcolor="#93A3AD">

<table border="0" cellpadding="2" cellspacing="0">
<tr>

<td><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td><b class="text14" style="color:#FFFFFF;">対象模試の選択&nbsp;：</b></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><font class="text12" style="color:#FFFFFF;">対象年度&nbsp;：&nbsp;</font></td>
<td><%@ include file="/jsp/individual/include/targetExamYear.jsp" %></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><font class="text12" style="color:#FFFFFF;">対象模試&nbsp;：&nbsp;</font></td>
<td><%@ include file="/jsp/individual/include/targetExamCode.jsp" %>
<input type="hidden" name="targetExamName" id="targetExamName">

</td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ツールバー-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--メニュー-->
<table border="0" cellpadding="0" cellspacing="0" width="840">
<tbody><tr valign="top">
<td width="619">
<!--分析＆判定-->
<table border="0" cellpadding="0" cellspacing="0" width="840">
<tbody><tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="840">
<tbody><tr valign="top">
<td width="617" bgcolor="#FBD49F" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</tbody></table>
<!--/spacer-->
<!--成績分析-->
<table border="0" cellpadding="0" cellspacing="0" width="812">
<tbody><tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="812">
<tbody><tr valign="top">
<td <c:choose><c:when test="${sessionScope['iCommonMap'].targetExamExist}">bgcolor="#FFFFFF"</c:when><c:otherwise>bgcolor="#CCCCCC"</c:otherwise></c:choose> width="589">

<div style="margin-top:12px;">
<table border="0" cellpadding="0" cellspacing="0" width="810">
<tbody><tr valign="top">
<td width="106" align="center"><!--アイコン--><img src="./individual/img/icon_seiseki.gif" width="81" height="84" border="0" alt="成績分析"><br></td>
<td width="684">

<table border="0" cellpadding="0" cellspacing="0" width="684">
<tbody><tr>
<td width="684"><b class="text12-hh">模試結果から生徒を詳細分析！</b></td>
</tr>
</tbody></table>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br>

<table border="0" cellpadding="0" cellspacing="0" width="684">
<tbody><tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="21" border="0" alt=""><br></td>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="21" border="0" alt=""><br></td>
<td width="673"><b class="text16" style="color:#657681">成績分析</b></td>
</tr>
<tr>
<td colspan="3" width="684"><img src="./shared_lib/img/parts/dot_blue.gif" width="684" height="1" border="0" alt="" vspace="2"><br></td>
</tr>
</tbody></table>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br>

<table border="0" cellpadding="0" cellspacing="0" width="684">
<tbody><tr>
<td width="559"><span class="text12">個人成績の詳細な分析を表やグラフを用いて行います。</span></td>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="105"><a <c:if test="${sessionScope['iCommonMap'].targetExamExist}">href="javascript:submitForm('i101')"</c:if>><img src="./shared_lib/img/btn/bunseki.gif" width="105" height="36" border="0" alt="分析する"></a><br></td>
</tr>
</tbody></table>

</td>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
</tr>
</tbody></table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</tbody></table>
<!--/spacer-->
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
<!--/成績分析-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
</tr>
</tbody></table>
<!--/spacer-->


<!--かんたん出力-->
<table border="0" cellpadding="0" cellspacing="0" width="812" style="background-color:#ffffff; border:#8CA9BB 1px solid">
	<tbody>
		<tr>
			<td style="padding:12px; width: 84px;">
				<!--アイコン--><img src="./individual/img/icon_print.gif" width="81" height="84" border="0" alt="成績分析">
			</td>
			<td style="width: 450px;">
				<div style="background-color: #FF8F0E; width: 5px; display: inline;">&nbsp;</div>
				<b class="text16" style="color:#657681">面談資料かんたん出力</b><br>
				<img src="./shared_lib/img/parts/dot_blue.gif" width="50%" height="1" border="0" alt="" vspace="2"><br>
				<span class="text12">予め設定された条件で面談資料を一括出力！</span>
			</td>
			<td rowspan="5" style="padding:12px; width: auto;">
				<!-- 出力の注意文 -->
				<div style="border:1px solid #8CA9BB; background-color:#EFF2F3;">

					<div style="margin:10px;">
						<b class="text14">出力に際しては常に以下が対象となります。<br></b>
						<span class="text10"><br>
						◎バランスチャートで扱う模試は選択中の模試と同学年の全ての種類のもの。<br><br>
						◎バランスチャート（合格者平均）で扱う模試は選択中の模試、大学は志望順位３まで。<br><br>
						◎科目別成績推移で扱う模試、設問別成績で扱う科目は選択中の模試と同種類のもの。<br><br>
						◎バランスチャート（合格者平均含）、教科別成績推移で教科内複数科目を受験している場合は下記の偏差値を表示。<br><br>
						○共通テスト模試およびリサーチ<br>
						　　 → 第１解答科目<br>
						○上記以外の模試　→　各教科の平均
					</span></div>
				</div>

				<!-- 対象学年の選択 -->
				<div style="margin-top: 5px; border:1px solid #8CA9BB; background-color:#EFF2F3; padding:6px;">
					<table border="0" cellpadding="0" cellspacing="0">
						<tbody>
						<tr>
							<td width="42"><img src="./shared_lib/img/parts/icon_hyoji.gif" width="30" height="30" border="0" alt="表示・出力形式の選択" hspace="3"></td>
							<td><b class="text14" style="color:#657681;">対象学年の選択</b></td>
						</tr>
						</tbody>
					</table>

					<!--spacer-->
					<div>
						<img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br>
					</div>

					<!--/spacer-->
					<div style="border: #8CA9BB 1px solid; background-color: #ffffff; padding: 8px;">
						<table border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td style="vertical-align: top">
										<img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→">
									</td>
									<td style="padding-left: 4px;">
										<span class="text12">連続個人成績表・教科／科目別成績推グラフの対象学年</span>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<table border="0" cellpadding="0" cellspacing="0" style="margin-top: 8px;">
											<tbody><tr>
												<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""></td>
												<td><input type="radio" name="targetGrade" value="2"<c:if test="${ i003Form.targetGrade == '2' }"> checked</c:if>></td>
												<td><span class="text12">全ての学年</span></td>
												<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""></td>
												<td><input type="radio" name="targetGrade" value="1"<c:if test="${ i003Form.targetGrade == '1' }"> checked</c:if>></td>
												<td><span class="text12">現学年のみ</span></td>
											</tr>
										</tbody></table>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<!-- セキュリティスタンプの選択 -->
				<div style="margin-top: 5px; border:1px solid #8CA9BB; background-color:#EFF2F3; padding:6px;">
					<table border="0" cellpadding="0" cellspacing="0">
						<tbody>
						<tr>
							<td width="42"><img src="./shared_lib/img/parts/icon_secu.gif" width="30" height="30" border="0" alt="セキュリティスタンプの選択" hspace="3"></td>
							<td><b class="text14" style="color:#657681;">セキュリティスタンプの選択</b></td>
						</tr>
						</tbody>
					</table>



					<!--spacer-->
					<div>
						<img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br>
					</div>

					<!--/spacer-->
					<div style="border: #8CA9BB 1px solid; background-color: #ffffff; padding: 8px;">
						<table border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td style="vertical-align: top">
										<img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→">
									</td>
                                    <td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""></td>
                                    <td><input type="radio" name="printStamp" value="1" <c:if test="${ i003Form.printStamp == '1'}">checked</c:if>></td>
									<td><span class="text12">機密</span></td>
									<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""></td>
                                    <td><input type="radio" name="printStamp" value="5" <c:if test="${ i003Form.printStamp == '5'}">checked</c:if>></td>
									<td><span class="text12">なし</span></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<!--出力対象生徒-->
				<div style="margin-top: 5px; border:1px solid #8CA9BB; background-color: #FBD49F; padding:6px; text-align: center;">
                    <c:if test="${sessionScope['iCommonMap'].bunsekiMode}">
					<select name="printStudent" class="text12" style="width:156;">
                        <option  value="1"<c:if test="${ i003Form.printStudent == '1'}">selected</c:if>>選択中の生徒全員</option>
                        <option  value="2"<c:if test="${ i003Form.printStudent == '2'}">selected</c:if>>表示中の生徒一人</option>
					</select>
                    </c:if>
					<!--spacer-->
					<div>
						<img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br>
					</div>

					<!--/spacer-->
					<div style="border: #8CA9BB 1px solid; background-color: #ffffff; padding: 8px;">
						<a href="" onclick="return printCheck();"><img src="./shared_lib/img/btn/hozon_w.gif" width="114" height="35" border="0" alt="出力"></a>
					</div>
				</div>
				<!--/出力対象生徒-->

			</td>
		</tr><tr>
		</tr>
			<tr><td colspan="2">
				<!--ケイコさん-->
				<table border="0" cellpadding="0" cellspacing="0" style="margin: 0 15px 0 30px;" width="500">
					<tbody><tr valign="top">
						<td width="52" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
						<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
						<td width="438">

							<table border="0" cellpadding="0" cellspacing="0" width="438">
								<tbody><tr valign="top">
									<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
									<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
									<td width="414" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="414" height="2" border="0" alt=""><br></td>
									<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
								</tr>
								<tr valign="top">
									<td width="414" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="414" height="5" border="0" alt=""><br></td>
								</tr>
							</tbody></table>

							<table border="0" cellpadding="0" cellspacing="0" width="438">
								<tbody><tr>
									<td width="12">
										<table border="0" cellpadding="0" cellspacing="0" width="12">
											<tbody><tr valign="top">
												<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="18" border="0" alt=""><br></td>
												<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
											</tr>
											<tr valign="top">
												<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
											</tr>
											<tr valign="top">
												<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="18" border="0" alt=""><br></td>
												<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
											</tr>
										</tbody></table>
									</td>
									<td width="424" bgcolor="#F9EEE5">
										<!--文言-->
										<table border="0" cellpadding="0" cellspacing="0" width="424">
										<tbody><tr>
										<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
										<td width="394"><span class="text14">以下から必要な資料を選択し、「保存」してください。</span></td>
										<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
										</tr>
										</tbody></table>
										<!--/文言-->
									</td>
									<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
								</tr>
							</tbody></table>

							<table border="0" cellpadding="0" cellspacing="0" width="438">
								<tbody><tr valign="top">
									<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
									<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
									<td width="414" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="414" height="5" border="0" alt=""><br></td>
									<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
								</tr>
								<tr valign="top">
									<td width="414" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="414" height="2" border="0" alt=""><br></td>
								</tr>
							</tbody></table>

						</td>
					</tr>
				</tbody></table>
				<!--/ケイコさん-->
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: left; padding: 12px; padding-bottom: 0;">
				<!--成績分析面談シート-->
				<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px">
								<img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→">
							</td>
							<td style="width: auto">
								<div style="border:1px solid #8CA9BB; background-color: #758A98; padding: 4px; padding-left: 8px; text-align: left;">
									<b class="text16" style="color:#FFFFFF;">成績分析面談シート</b>
								</div>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>
								<div style="margin-top: 3px; border:1px solid #8CA9BB; background-color: #FBD49F; padding: 6px 12px; text-align: center;">
									<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
										<tbody><tr>
											<td style="width: 33%;" align="center">
												<!--シート1-->
												<div style="width: 130px; border: #8CA9BB 1px solid;">
													<div style='text-align: left; vertical-align: top; background-color: #E5EEF3; padding: 0px 4px;'>
														<input type="checkbox" name="report" value="1" <c:if test="${i003Form.report == '1'}">checked</c:if>>
														<span class="text12">シート1</span>
													</div>
													<div style='text-align: center; vertical-align: top; background-color: #ffffff; padding: 7px; border-top: #8CA9BB 1px solid;'>
														<img src="./individual/img/graph07.gif" width="106" height="67" border="0" alt="シート1"><br>
														<img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br>
														<span class="text12"><a href="javascript:openSample('i409')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span>
													</div>
												</div>
												<!--/シート1-->
											</td>
											<td style="width: 33%;" align="center">
												<!--シート2-->
                                                <c:set var="bgcolor" value="#E5EEF3"/>
                                                <c:set var="bgcolor2" value="#FFFFFF"/>
                                                <c:set var="disabled" value="false"/>
                                                <kn:choose>
                                                    <%--受験学力測定--%>
                                                    <kn:examWhen id="ability" var="${iCommonMap.examData}">
                                                        <c:set var="bgcolor" value="#E1E6EB"/>
                                                        <c:set var="bgcolor2" value="#E1E6EB"/>
                                                        <c:set var="disabled" value="true"/>
                                                    </kn:examWhen>
                                                    <%--パターンI--%>
                                                    <kn:examWhen id="patternI" var="${iCommonMap.examData}">
                                                        <c:set var="bgcolor" value="#E5EEF3"/>
                                                        <c:set var="bgcolor2" value="#FFFFFF"/>
                                                        <c:set var="disabled" value="false"/>
                                                    </kn:examWhen>
                                                    <kn:otherwise>
                                                        <c:set var="bgcolor" value="#E1E6EB"/>
                                                        <c:set var="bgcolor2" value="#E1E6EB"/>
                                                        <c:set var="disabled" value="true"/>
                                                    </kn:otherwise>
                                                </kn:choose>
												<div style="width: 130px; border: #8CA9BB 1px solid;">
													<div style="text-align: left; vertical-align: top; background-color: <c:out value="${bgcolor}"/>; padding: 0px 4px;">
														<input type="checkbox" name="interviewSheet" value="5" <c:if test="${i003Form.interviewSheet[0] == '1'}"> checked</c:if><c:if test="${disabled}"> disabled</c:if>>
														<span class="text12">シート2</span>
													</div>
													<div style="text-align: center; vertical-align: top; background-color: <c:out value="${bgcolor2}"/>; padding: 7px; border-top: #8CA9BB 1px solid;">
														<img src="./individual/img/graph00.gif" width="106" height="67" border="0" alt="シート2"><br>
														<img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br>
														<span class="text12"><a href="javascript:openSample('i410')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span>
													</div>
												</div>
												<!--/シート2-->
											</td>
											<td style="width: 33%;" align="center">
												<!--シート3-->
												<div style="width: 130px; border: #8CA9BB 1px solid;">
													<div style="text-align: left; vertical-align: top; background-color: #E5EEF3; padding: 0px 4px;">
														<input type="checkbox" name="interviewSheet" value="2" <c:if test="${i003Form.interviewSheet[1] == '1'}"> checked</c:if>>
														<span class="text12">シート3</span>
													</div>
													<div style="text-align: center; vertical-align: top; background-color: #ffffff; padding: 7px; border-top: #8CA9BB 1px solid;">
														<img src="./individual/img/graph01.gif" width="106" height="67" border="0" alt="シート3"><br>
														<img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br>
														<span class="text12"><a href="javascript:openSample('i411')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span>
													</div>
												</div>
												<!--/シート3-->
											</td>
										</tr>
									</tbody></table>



								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<!--/成績分析面談シート-->
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: left; padding: 0 12px 6px;">
				<!--面談用帳票-->
				<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
					<tbody>
						<tr>
							<td style="width: 20px">
								<img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→">
							</td>
							<td style="width: auto">
								<div style="border:1px solid #8CA9BB; background-color: #758A98; padding: 4px; padding-left: 8px; text-align: left;">
									<b class="text16" style="color:#FFFFFF;">面談用帳票</b>
								</div>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>
								<div style="margin-top: 3px; border:1px solid #8CA9BB; background-color: #ffffff; padding: 6px 12px; text-align: center;">
									<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
										<tbody><tr>
											<td style="width: 33%;" align="center">
												<!--教科分析シート-->
                                                <c:set var="bgcolor" value="#E5EEF3"/>
                                                <c:set var="bgcolo2" value="#FFFFFF"/>
                                                <c:set var="disabled" value="false"/>
                                                <kn:choose>
                                                    <%--受験学力測定--%>
                                                    <kn:examWhen id="ability" var="${iCommonMap.examData}">
                                                        <c:set var="bgcolor" value="#E1E6EB"/>
                                                        <c:set var="bgcolor2" value="#E1E6EB"/>
                                                        <c:set var="disabled" value="true"/>
                                                    </kn:examWhen>
                                                    <kn:otherwise>
                                                        <c:set var="bgcolor" value="#E5EEF3"/>
                                                        <c:set var="bgcolor2" value="#FFFFFF"/>
                                                        <c:set var="disabled" value="false"/>
                                                    </kn:otherwise>
                                                </kn:choose>
												<div style="width: 130px; border: #8CA9BB 1px solid;">
													<div style='text-align: left; vertical-align: top; background-color: <c:out value="${bgcolor}"/>; padding: 0px 4px;'>
                                                        <input type="checkbox" name="interviewForms" value="0" <c:if test="${i003Form.interviewForms[0] == '1'}"> checked</c:if><c:if test="${disabled}"> disabled</c:if>>
														<span class="text12">教科分析シート</span>
													</div>
													<div style='text-align: center; vertical-align: top; background-color: <c:out value="${bgcolor2}"/>; padding: 7px; border-top: #8CA9BB 1px solid;'>
														<img src="./individual/img/graph04.gif" width="106" height="67" border="0" alt="教科分析シート"><br>
														<img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br>
														<span class="text12"><a href="javascript:openSample('i414')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span>
													</div>
												</div>
												<!--/教科分析シート-->
											</td>
											<td style="width: 33%;" align="center">
												<!--成績推移グラフ-->
                                                <c:set var="bgcolor" value="#E5EEF3"/>
                                                <c:set var="bgcolo2" value="#FFFFFF"/>
                                                <c:set var="disabled" value="false"/>
                                                <kn:choose>
                                                    <%--受験学力測定--%>
                                                    <kn:examWhen id="ability" var="${iCommonMap.examData}">
                                                        <c:set var="bgcolor" value="#E1E6EB"/>
                                                        <c:set var="bgcolor2" value="#E1E6EB"/>
                                                        <c:set var="disabled" value="true"/>
                                                    </kn:examWhen>
                                                    <kn:otherwise>
                                                        <c:set var="bgcolor" value="#E5EEF3"/>
                                                        <c:set var="bgcolor2" value="#FFFFFF"/>
                                                        <c:set var="disabled" value="false"/>
                                                    </kn:otherwise>
                                                </kn:choose>
												<div style="width: 130px; border: #8CA9BB 1px solid;">
													<div style="text-align: left; vertical-align: top; background-color: <c:out value="${bgcolor}"/>; padding: 0px 4px;">
														<input type="checkbox" name="interviewForms" value="1" <c:if test="${i003Form.interviewForms[1] == '1'}"> checked</c:if><c:if test="${disabled}"> disabled</c:if>>
														<span class="text12">成績推移グラフ</span>
													</div>
													<div style="text-align: center; vertical-align: top; background-color: <c:out value="${bgcolor2}"/>; padding: 7px; border-top: #8CA9BB 1px solid;">
														<img src="./individual/img/graph05.gif" width="106" height="67" border="0" alt="成績推移グラフ"><br>
														<img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br>
														<span class="text12"><a href="javascript:openSample('i415')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span>
													</div>
												</div>
												<!--/成績推移グラフ-->
											</td>
											<td style="width: 33%;" align="center">
												<!--設問別成績グラフ-->
                                                <c:set var="bgcolor" value="#E5EEF3"/>
                                                <c:set var="bgcolo2" value="#FFFFFF"/>
                                                <c:set var="disabled" value="false"/>
                                                <kn:choose>
                                                    <%--受験学力測定・センターリサーチ以外--%>
                                                    <kn:examNotWhen id="patternIV" var="${iCommonMap.examData}">
                                                        <c:set var="bgcolor" value="#E1E6EB"/>
                                                        <c:set var="bgcolor2" value="#E1E6EB"/>
                                                        <c:set var="disabled" value="true"/>
                                                    </kn:examNotWhen>
                                                    <kn:otherwise>
                                                        <c:set var="bgcolor" value="#E5EEF3"/>
                                                        <c:set var="bgcolor2" value="#FFFFFF"/>
                                                        <c:set var="disabled" value="false"/>
                                                    </kn:otherwise>
                                                </kn:choose>
												<div style="width: 130px; border: #8CA9BB 1px solid;">
													<div style="text-align: left; vertical-align: top; background-color: <c:out value="${bgcolor}"/>; padding: 0px 4px;">
														<input type="checkbox" name="interviewForms" value="2" <c:if test="${i003Form.interviewForms[2] == '1'}"> checked</c:if><c:if test="${disabled}"> disabled</c:if>>
														<span class="text12">設問別成績グラフ</span>
													</div>
													<div style="text-align: center; vertical-align: top; background-color: <c:out value="${bgcolor2}"/>; padding: 7px; border-top: #8CA9BB 1px solid;">
														<img src="./individual/img/graph06.gif" width="106" height="67" border="0" alt="設問別成績グラフ"><br>
														<img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br>
														<span class="text12"><a href="javascript:openSample('i416')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4">サンプルの表示</a></span>
													</div>
												</div>
												<!--/設問別成績グラフ-->
											</td>
										</tr>
									</tbody></table>



								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<!--/面談用帳票-->
			</td>
		</tr>
	</tbody>
</table>




<!--spacer-->
<div>
	<img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt="">
</div>
<!--/spacer-->


<!--面談メモ-->
<table border="0" cellpadding="0" cellspacing="0" width="812">
	<tbody>
		<tr valign="top">
			<td bgcolor="#8CA9BB">
				<table border="0" cellpadding="0" cellspacing="1" width="812">
					<tbody>
						<tr valign="top">
							<td bgcolor="#FFFFFF" width="589">

								<div style="margin-top: 12px;">
									<table border="0" cellpadding="0" cellspacing="0" width="810">
										<tbody>
											<tr valign="top">
												<td width="106" align="center">
													<!--アイコン-->
													<img src="./individual/img/icon_seiseki.gif" width="81" height="84" border="0" alt="成績分析"><br>
												</td>
												<td width="684">

													<table border="0" cellpadding="0" cellspacing="0" width="684">
														<tbody>
															<tr>
																<td width="684"><b class="text12-hh">面談内容や分析結果を自由にメモ！</b></td>
															</tr>
														</tbody>
													</table> <img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br>

													<table border="0" cellpadding="0" cellspacing="0" width="684">
														<tbody>
															<tr>
																<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="21" border="0" alt=""><br></td>
																<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="21" border="0" alt=""><br></td>
																<td width="673"><b class="text16" style="color: #657681">面談メモ</b></td>
															</tr>
															<tr>
																<td colspan="3" width="684"><img src="./shared_lib/img/parts/dot_blue.gif" width="684" height="1" border="0" alt="" vspace="2"><br></td>
															</tr>
														</tbody>
													</table> <img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br>

													<table border="0" cellpadding="0" cellspacing="0" width="684">
														<tbody>
															<tr>
																<td align="right"><input type="button" value="&nbsp;メモを開く&nbsp;" class="text12" style="width: 110px;" onclick="submitForm('i501')"></td>
															</tr>
														</tbody>
													</table>

												</td>
												<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
											</tr>
										</tbody>
									</table>
								</div> <!--spacer-->
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tbody>
										<tr valign="top">
											<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
										</tr>
									</tbody>
								</table> <!--/spacer-->
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table> <!--/面談メモ-->





<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</tbody></table>
<!--/spacer-->

</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
<!--/分析＆判定-->
</td>
</tr>
</tbody></table>
<!--/メニュー-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/概要-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/メニュー選択-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</div>
</body>
</html>

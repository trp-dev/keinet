<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.kawaijuku.judgement.util.JudgementUtil" %>
<jsp:useBean id="PageBean" scope="request" class="jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean" />
<jsp:useBean id="i205Form" scope="request" class="jp.co.fj.keinavi.forms.individual.I205Form" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script language="JavaScript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    <%@ include file="/jsp/script/submit_change.jsp" %>
    <%@ include file="/jsp/script/submit_exit.jsp" %>
    <%@ include file="/jsp/script/submit_save.jsp" %>
    <%@ include file="/jsp/script/submit_menu.jsp" %>
    <%@ include file="/jsp/script/open_common.jsp" %>
    <%@ include file="/jsp/script/open_schedule_alert.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

    /**
     * サブミッション
     */
    function submitForm(forward) {
        <%--スクロール位置 0426 K.Kondo--%>
        if(forward=='<c:out value="${param.forward}" />') {
            document.forms[0].scrollX.value = document.body.scrollLeft;
            document.forms[0].scrollY.value = document.body.scrollTop;
        } else {
            document.forms[0].scrollX.value = 0;
            document.forms[0].scrollY.value = 0;
        }
        document.forms[0].target = "_self";
        document.forms[0].forward.value = forward;
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 公表科目サブミッション
     */
    function submitOpenSub() {
        var form = document.forms[0];
        // 出力先ウィンドウを開く
		op = window.open("", "i206", "width=930,height=680,resizable=yes,scrollbars=yes,status=yes,titlebar=yes");
        op.moveTo(50, 50);
        form.forward.value = "i206";
        form.backward.value = "<c:out value="${param.forward}" />";
        form.target = "i206";
        form.submit();
    }
    /**
     * 枝番設定
     */
    function setBranchInfo(branch){
        document.forms[0].selectBranch.value = branch;
    }

    /**
     * 初期化
     */
    function init(){
        startTimer();
        <%--スクロール位置 0426 K.Kondo--%>
        window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
    }
//-->
</script>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" style="word-break: keep-all;">
<form action="<c:url value="I205Servlet" />" method="post">
<%-- hiddens --%>
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<!-- 隠れフィールド　大学・学部・学科情報 -->
<input type="hidden" name="uniqueKey" value="<%=encTableNBSP(PageBean.getUniqueKey())%>">
<!-- 隠れフィールド　選択された枝番 -->
<input type="hidden"name="selectBranch" value="">
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${i205Form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${i205Form.scrollY}" />">
<%-- /hiddens --%>
<input type="hidden" name="judgeNo" value="<c:out value="${i205Form.judgeNo}" />"><%-- 2005.6.23 add --%>

<input type="hidden" name="targetPersonId" value="<c:out value="${sessionScope['iCommonMap'].targetPersonId}" />"><%-- 2005.7.4 add --%>

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="863" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="871" bgcolor="#FFFFFF" align="center">

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="804">

<table border="0" cellpadding="0" cellspacing="0" width="804">
<tr valign="top">
<td colspan="5" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="388" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;"><c:out value="${PageBean.univName}"/></b></td>
<td width="400" bgcolor="#758A98" align="right">

<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><span class="text14" style="color:#FFFFFF;"><b>学部&nbsp;：&nbsp;</b><c:out value="${PageBean.deptName}"/></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="2" border="0" alt=""><br></td>
<td><span class="text14" style="color:#FFFFFF;"><b>学科&nbsp;：&nbsp;</b><c:out value="${PageBean.subName}"/></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
<tr valign="top">
<td colspan="5" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--情報-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><span class="text12"><b>本部所在地&nbsp;：&nbsp;</b><c:out value="${PageBean.prefName}"/></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="2" border="0" alt=""><br></td>
<td><span class="text12"><b>キャンパス所在地&nbsp;：&nbsp;</b><c:out value="${PageBean.prefName_sh}"/></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="2" border="0" alt=""><br></td>
<c:if test="${ PageBean.examCapaRel ne '8' and PageBean.capacity ne '0' }">
  <td><span class="text12"><b>定員&nbsp;：&nbsp;</b><c:if test="${ PageBean.examCapaRel eq '9' }">推定</c:if><c:out value="${PageBean.capacity}"/>人</span></td>
  <td><img src="./shared_lib/img/parts/sp.gif" width="20" height="2" border="0" alt=""><br></td>
</c:if>
<td><span class="text12"><b>入試日&nbsp;：&nbsp;</b><c:out value="${PageBean.dateCorrelation}"/></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="2" border="0" alt=""><br></td>
<td><span class="text12">※<a href="#here" onclick="openScheduleAlert(1);">入試日程に関するご注意</a></span></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/情報-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!---->
<table border="0" cellpadding="4" cellspacing="0" width="820">
<tr>
<td width="820" bgcolor="#93A3AD">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="7"><img src="./shared_lib/img/parts/sp.gif" width="7" height="2" border="0" alt=""><br></td>
<td><b class="text12" style="color:#FFFFFF;">判定科目</b></td>
<td width="653"><img src="./shared_lib/img/parts/sp.gif" width="15" height="2" border="0" alt=""><br></td>
<td width="100">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input onclick="submitOpenSub()" type="button" value="公表科目表示" class="text12" style="width:95px;"></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>


<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="1" bgcolor="#93A3AD"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>

<td width="818" height="22">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="7"><img src="./shared_lib/img/parts/sp.gif" width="7" height="2" border="0" alt=""><br></td>
<td><span class="text12">
<b>センター判定対象模試&nbsp;：&nbsp;</b>
<c:out value="${PageBean.c_examName}"/>
</span></td>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="2" border="0" alt=""><br></td>
<td><span class="text12">
<b>2次判定対象模試&nbsp;：&nbsp;</b>
<c:out value="${PageBean.s_examName}"/>
</span></td>
</tr>
</table>
</td>

<td width="1" bgcolor="#93A3AD"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/-->

<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="14" cellspacing="1" width="820">
<tr valign="top">
<td width="818" bgcolor="#EFF2F3" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="12" cellspacing="1" width="790">
<tr valign="top">
<td width="788" bgcolor="#FFFFFF">

<!--センター-->
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <table border="0" cellpadding="2" cellspacing="2" width="764"> --%>
<table border="0" cellpadding="2" cellspacing="0" width="760" style="margin: 0px 2px;">
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 UPD END   --%>
<!--タイトルー-->
<tr height="22" bgcolor="#8CA9BB">
<td colspan="11" width="100%"><b class="text12" style="color:#FFFFFF;">&nbsp;センター</b></td>
</tr>
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 ADD START --%>
</table>
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 ADD END   --%>
<!--/タイトル-->
<!--公表科目-->
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 ADD START --%>
<table border="0" cellpadding="2" cellspacing="2" width="764" style="table-layout:fixed;">
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 ADD END   --%>
<tr>
<td rowspan="4" width="7%" bgcolor="#CDD7DD" align="center"><b class="text16"><%=PageBean.getCLetterJudgement()%></b></td>
<td width="9%" bgcolor="#E1E6EB" align="center"><b class="text12">判定科目</b></td>
<td colspan="7" width="56%" bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12-hh">
    <!-- 判定科目 -->

    <c:out value="${PageBean.c_subString}"/>

</span></td>
</tr>
</table>
</td>
<td width="28%" bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">
</span></td>
</tr>
</table>
</td>
</tr>
<!--/公表科目-->
<!--配点-->
<tr height="17">
<td rowspan="2" width="9%" bgcolor="#E1E6EB">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><b class="text12">配点</b></td>
</tr>
</table>
</td>
<td width="8%" bgcolor="#E1E6EB" align="center"><span class="text12">英語</span></td>
<td width="8%" bgcolor="#E1E6EB" align="center"><span class="text12">数学</span></td>
<td width="8%" bgcolor="#E1E6EB" align="center"><span class="text12">国語</span></td>
<td width="8%" bgcolor="#E1E6EB" align="center"><span class="text12">理科</span></td>
<td width="8%" bgcolor="#E1E6EB" align="center"><span class="text12">地・公</span></td>
<td width="8%" bgcolor="#E1E6EB" align="center"><span class="text12"></span></td>
<td width="8%" bgcolor="#CDD7DD" align="center"><span class="text12">満点</span></td>
<td width="28%" bgcolor="#FFAD8C" align="center"><span class="text12">評価得点</span></td>
</tr>
<tr height="17">
<td width="8%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${PageBean.allot1Eng}"/></span></td>
<td width="8%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${PageBean.allot1Mat}"/></span></td>
<td width="8%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${PageBean.allot1Jap}"/></span></td>
<td width="8%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${PageBean.allot1Sci}"/></span></td>
<td width="8%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${PageBean.allot1Soc}"/></span></td>
<td width="8%" bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td width="8%" bgcolor="#F4E5D6" align="center"><b class="text12"><c:out value="${PageBean.CFullPoint}"/></b></td>
<td width="28%" bgcolor="#F4E5D6" align="center"><b class="text12"><c:out value="${PageBean.c_scoreStr}"/></b></td>
</tr>
<!--/配点-->
<!--評価基準-->
<tr>
<td width="9%" bgcolor="#E1E6EB">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><b class="text12">評価基準</b></td>
</tr>
</table>
</td>
<td colspan="8" width="84%" bgcolor="#EFF2F3" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="624">
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" width="624">
<tr>

    <td width="110"><img src="./shared_lib/img/parts/sp.gif" width="110" height="1" border="0" alt=""><br></td>
    <td width="126"><span class="text12"><c:out value="${PageBean.CDLine}"/></span></td>
    <td width="126"><span class="text12"><c:out value="${PageBean.CCLine}"/></span></td>
    <td width="122"><span class="text12"><c:out value="${PageBean.CBLine}"/></span></td>
    <td width="140"><span class="text12"><c:out value="${PageBean.CALine}"/></span></td>

</tr>
</table>
</td>
</tr>
</tr>
<tr>

<td width="624" height="18" background="./individual/img/bar_i205.gif"><img src="./shared_lib/img/parts/sp.gif" width="275" height="1" border="0" alt="">
<span class="text12" style="color:#FFFFFF;">（ボーダー<b><c:out value="${PageBean.borderPointStr}"/></b>）</span></td>

</tr>
</table>

    <table border="0" cellpadding="0" cellspacing="0" width="624">
    <tr>
    <td width="115">
    <!--E-->
    <c:if test="${PageBean.c_judgement == 1}">
        <table border="0" cellpadding="0" cellspacing="0" width="115">
        <tr>
        <td width="2" bgcolor="#65A0C6"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
        <td width="111" align="center"><span class="text12">評価得点：<b><c:out value="${PageBean.c_scoreStr}"/></b></span></td>
        <td width="2" bgcolor="#65A0C6"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
        </tr>
        <tr>
        <td colspan="3" width="115" bgcolor="#65A0C6"><img src="./shared_lib/img/parts/sp.gif" width="115" height="2" border="0" alt=""><br></td>
        </tr>
        </table>
    </c:if>
    <!--/E-->
    </td>
    <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
    <td width="120">
    <!--D-->
    <c:if test="${PageBean.c_judgement == 2}">
        <table border="0" cellpadding="0" cellspacing="0" width="120">
        <tr>
        <td width="2" bgcolor="#0A9F7A"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
        <td width="116" align="center"><span class="text12">評価得点：<b><c:out value="${PageBean.c_scoreStr}"/></b></span></td>
        <td width="2" bgcolor="#0A9F7A"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
        </tr>
        <tr>
        <td colspan="3" width="120" bgcolor="#0A9F7A"><img src="./shared_lib/img/parts/sp.gif" width="120" height="2" border="0" alt=""><br></td>
        </tr>
        </table>
    </c:if>
    <!--/D-->
    </td>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="120">
    <!--C-->
    <c:if test="${PageBean.c_judgement == 3}">
        <table border="0" cellpadding="0" cellspacing="0" width="120">
        <tr>
        <td width="2" bgcolor="#7AAB78"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
        <td width="116" align="center"><span class="text12">評価得点：<b><c:out value="${PageBean.c_scoreStr}"/></b></span></td>
        <td width="2" bgcolor="#7AAB78"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
        </tr>
        <tr>
        <td colspan="3" width="120" bgcolor="#7AAB78"><img src="./shared_lib/img/parts/sp.gif" width="120" height="2" border="0" alt=""><br></td>
        </tr>
        </table>
    </c:if>
    <!--/C-->
    </td>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="120">
    <!--B-->
    <c:if test="${PageBean.c_judgement == 4}">
        <table border="0" cellpadding="0" cellspacing="0" width="120">
        <tr>
        <td width="2" bgcolor="#EBA225"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
        <td width="116" align="center"><span class="text12">評価得点：<b><c:out value="${PageBean.c_scoreStr}"/></b></span></td>
        <td width="2" bgcolor="#EBA225"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
        </tr>
        <tr>
        <td colspan="3" width="120" bgcolor="#EBA225"><img src="./shared_lib/img/parts/sp.gif" width="120" height="2" border="0" alt=""><br></td>
        </tr>
        </table>
    </c:if>
    <!--/B-->
    </td>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="128">
    <!--A-->
    <c:if test="${PageBean.c_judgement == 5}">
        <table border="0" cellpadding="0" cellspacing="0" width="128">
        <tr>
        <td width="2" bgcolor="#D95556"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
        <td width="124" align="center"><span class="text12">評価得点：<b><c:out value="${PageBean.c_scoreStr}"/></b></span></td>
        <td width="2" bgcolor="#D95556"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
        </tr>
        <tr>
        <td colspan="3" width="128" bgcolor="#D95556"><img src="./shared_lib/img/parts/sp.gif" width="128" height="2" border="0" alt=""><br></td>
        </tr>
        </table>
    </c:if>
    <!--/A-->
    </td>
    </tr>
    </table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
<!--/評価基準-->
</table>
<!--/センター-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--二次-->
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <table border="0" cellpadding="2" cellspacing="2" width="764"> --%>
<table border="0" cellpadding="2" cellspacing="0" width="760" style="margin: 0px 2px;">
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 UPD END   --%>
<!--タイトルー-->
<tr height="22" bgcolor="#8CA9BB">
<td colspan="11" width="100%"><b class="text12" style="color:#FFFFFF;">&nbsp;２次</b></td>
</tr>
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 ADD START --%>
</table>
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 ADD END   --%>
<!--/タイトル-->
<!--公表科目-->
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 ADD START --%>
<table border="0" cellpadding="2" cellspacing="2" width="764" style="table-layout:fixed;">
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 ADD END   --%>
<tr>
<td rowspan="4" width="7%" bgcolor="#CDD7DD" align="center"><b class="text16"><%=PageBean.getSLetterJudgement()%></b></td>
<td width="9%" bgcolor="#E1E6EB" align="center"><b class="text12">判定科目</b></td>
<td colspan="7" width="56%" bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12-hh"><c:out value="${PageBean.s_subString}"/><br>
</span></td>
</tr>
</table>
</td>
<td colspan="2" width="28%" bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12">
</span></td>
</tr>
</table>
</td>
</tr>
<!--/公表科目-->
<!--配点-->
<tr height="17">
<td rowspan="2" width="9%" bgcolor="#E1E6EB">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><b class="text12">配点</b></td>
</tr>
</table>
</td>
<td width="8%" bgcolor="#E1E6EB" align="center"><span class="text12">英語</span></td>
<td width="8%" bgcolor="#E1E6EB" align="center"><span class="text12">数学</span></td>
<td width="8%" bgcolor="#E1E6EB" align="center"><span class="text12">国語</span></td>
<td width="8%" bgcolor="#E1E6EB" align="center"><span class="text12">理科</span></td>
<td width="8%" bgcolor="#E1E6EB" align="center"><span class="text12">地・公</span></td>
<td width="8%" bgcolor="#E1E6EB" align="center"><span class="text12">その他</span></td>
<td width="8%" bgcolor="#CDD7DD" align="center"><span class="text12">満点</span></td>
<td width="18%" bgcolor="#FFAD8C" align="center"><span class="text12">２次ランク</span></td>
<td width="10%" bgcolor="#FFAD8C" align="center"><span class="text12">評価偏差値</span></td>
</tr>
<tr height="17">
<td width="8%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${PageBean.allot2Eng}"/></span></td>
<td width="8%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${PageBean.allot2Mat}"/></span></td>
<td width="8%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${PageBean.allot2Jap}"/></span></td>
<td width="8%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${PageBean.allot2Sci}"/></span></td>
<td width="8%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${PageBean.allot2Soc}"/></span></td>
<td width="8%" bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${PageBean.otherAllot}"/></span></td>
<td width="8%" bgcolor="#F4E5D6" align="center"><b class="text12"><c:out value="${PageBean.SFullPoint}"/></b></td>
<td width="18%" bgcolor="#F4E5D6" align="center"><b class="text12"><c:out value="${PageBean.rankName}"/><c:if test="${PageBean.rankName != ' -- ' && PageBean.rankName != 'BF'}">（<c:out value="${PageBean.rankLLimitsStr}"/>-<c:out value="${PageBean.rankHLimitsStr}"/>）</c:if></b></td>
<td width="10%" bgcolor="#F4E5D6" align="center"><b class="text12"><c:out value="${PageBean.s_scoreStr}"/></b></td>
</tr>
<!--/配点-->
<!--評価基準-->
<tr>
<td width="9%" bgcolor="#E1E6EB">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><b class="text12">評価基準</b></td>
</tr>
</table>
</td>
<td colspan="9" width="84%" bgcolor="#EFF2F3" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="624">
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" width="624">
<tr>

    <td width="110"><img src="./shared_lib/img/parts/sp.gif" width="110" height="1" border="0" alt=""><br></td>
    <td width="126"><span class="text12"><%=PageBean.getSDLine()%></span></td>
    <td width="126"><span class="text12"><%=PageBean.getSCLine()%></span></td>
    <td width="122"><span class="text12"><%=PageBean.getSBLine()%></span></td>
    <td width="140"><span class="text12"><%=PageBean.getSALine()%></span></td>

</tr>
</table>
</td>
</tr>

<tr>

    <td width="624" height="18" background="./individual/img/bar_i205.gif"><img src="./shared_lib/img/parts/sp.gif" width="275" height="1" border="0" alt=""></td>

</tr>
</table>

    <table border="0" cellpadding="0" cellspacing="0" width="624">
    <tr>
    <td width="115">
    <!--E-->
    <c:if test="${PageBean.s_judgement == 1}">
    <table border="0" cellpadding="0" cellspacing="0" width="115">
    <tr>
    <td width="2" bgcolor="#65A0C6"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
    <td width="111" align="center"><span class="text12">評価偏差値：<b><c:out value="${PageBean.s_scoreStr}"/></b></span></td>
    <td width="2" bgcolor="#65A0C6"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
    </tr>
    <tr>
    <td colspan="3" width="115" bgcolor="#65A0C6"><img src="./shared_lib/img/parts/sp.gif" width="115" height="2" border="0" alt=""><br></td>
    </tr>
    </table>
    </c:if>
    <!--/E-->
    </td>
    <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
    <td width="120">
    <!--D-->
    <c:if test="${PageBean.s_judgement == 2}">
    <table border="0" cellpadding="0" cellspacing="0" width="120">
    <tr>
    <td width="2" bgcolor="#0A9F7A"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
    <td width="116" align="center"><span class="text12">評価偏差値：<b><c:out value="${PageBean.s_scoreStr}"/></b></span></td>
    <td width="2" bgcolor="#0A9F7A"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
    </tr>
    <tr>
    <td colspan="3" width="120" bgcolor="#0A9F7A"><img src="./shared_lib/img/parts/sp.gif" width="120" height="2" border="0" alt=""><br></td>
    </tr>
    </table>
    </c:if>
    <!--/D-->
    </td>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="120">
    <!--C-->
    <c:if test="${PageBean.s_judgement == 3}">
    <table border="0" cellpadding="0" cellspacing="0" width="120">
    <tr>
    <td width="2" bgcolor="#7AAB78"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
    <td width="116" align="center"><span class="text12">評価偏差値：<b><c:out value="${PageBean.s_scoreStr}"/></b></span></td>
    <td width="2" bgcolor="#7AAB78"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
    </tr>
    <tr>
    <td colspan="3" width="120" bgcolor="#7AAB78"><img src="./shared_lib/img/parts/sp.gif" width="120" height="2" border="0" alt=""><br></td>
    </tr>
    </table>
    </c:if>
    <!--/C-->
    </td>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="120">
    <!--B-->
    <c:if test="${PageBean.s_judgement == 4}">
    <table border="0" cellpadding="0" cellspacing="0" width="120">
    <tr>
    <td width="2" bgcolor="#EBA225"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
    <td width="116" align="center"><span class="text12">評価偏差値：<b><c:out value="${PageBean.s_scoreStr}"/></b></span></td>
    <td width="2" bgcolor="#EBA225"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
    </tr>
    <tr>
    <td colspan="3" width="120" bgcolor="#EBA225"><img src="./shared_lib/img/parts/sp.gif" width="120" height="2" border="0" alt=""><br></td>
    </tr>
    </table>
    </c:if>
    <!--/B-->
    </td>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="128">
    <!--A-->
    <c:if test="${PageBean.s_judgement == 5}">
    <table border="0" cellpadding="0" cellspacing="0" width="128">
    <tr>
    <td width="2" bgcolor="#D95556"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
    <td width="124" align="center"><span class="text12">評価偏差値：<b><c:out value="${PageBean.s_scoreStr}"/></b></span></td>
    <td width="2" bgcolor="#D95556"><img src="./shared_lib/img/parts/sp.gif" width="2" height="16" border="0" alt=""><br></td>
    </tr>
    <tr>
    <td colspan="3" width="128" bgcolor="#D95556"><img src="./shared_lib/img/parts/sp.gif" width="128" height="2" border="0" alt=""><br></td>
    </tr>
    </table>
    </c:if>
    <!--/A-->
    </td>
    </tr>
    </table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
</tr>
<!--/評価基準-->
</table>
<!--/二次-->

<!--注意-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--その他-->
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <table border="0" cellpadding="2" cellspacing="2" width="764"> --%>
<table border="0" cellpadding="2" cellspacing="0" width="760" style="margin: 0px 2px;">
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 UPD END   --%>
<!--タイトルー-->
<tr height="22" bgcolor="#8CA9BB">
<td colspan="4" width="100%"><b class="text12" style="color:#FFFFFF;">&nbsp;総合</b></td>
</tr>
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 ADD START --%>
</table>
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 ADD END   --%>
<!--/タイトル-->
<!--総合ポイント-->
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 ADD START --%>
<table border="0" cellpadding="2" cellspacing="2" width="764" style="table-layout:fixed;">
<%-- 2016/02/02 QQ)Hisakawa 大規模改修 ADD END   --%>
<tr height="22">
<td rowspan="3" width="7%" bgcolor="#CDD7DD" align="center"><b class="text16"><%=PageBean.getTLetterJudgement()%></b></td>
<td colspan="2" width="25%" bgcolor="#E1E6EB"><b class="text12">&nbsp;総合ポイント</b></td>
<td width="68%" bgcolor="#F4E5D6" align="center"><b class="text12"><c:out value="${PageBean.t_scoreStr}"/></b></td>
</tr>
<!--/総合ポイント-->
<!--配点ウェイト-->
<tr height="22">
<td width="15%" rowspan="2" bgcolor="#E1E6EB"><b class="text12">&nbsp;配点ウェイト（点）</b></td>
<td width="10%" bgcolor="#E1E6EB"><b class="text12">&nbsp;センター</b></td>
<td width="68%" bgcolor="#F4E5D6" align="center"><b class="text12"><c:out value="${PageBean.CAllotPntRateAll}"/></b></td>
</tr>
<tr height="22">
<td width="10%" bgcolor="#E1E6EB"><b class="text12">&nbsp;２次</b></td>
<td width="68%" bgcolor="#F4E5D6" align="center"><b class="text12"><c:out value="${PageBean.SAllotPntRateAll}"/></b></td>
</tr>
<!--/配点ウェイト-->
</table>
<!--/その他-->

<div style="margin:8px 8px 0px 8px">
<span class="text12">ここに表示されている定員・入試科目・配点・選考方法・入試日程等は変更される可能性がありますので、<br>必ず大学発表の学生募集要項で確認してください。</span>
</div>

</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/概要-->


<!--ボタン-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td align="center"><input onclick="javascript:window.close()" type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw_01.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</body>
</html>
<%!
/**
 * 表の中に””（空文字）があるときに"--"を設定してくれるメソッド（ＮＮ対応の場合は必ずとおす）
 * encは実行済み
 * @param	value  表内に設定する値Value値
 * @return	"value" または "&nbsp;"
 */
public String encTableHAI(String value){
    if(value == null || value.equals("")){
        return ("--");
      }else{
        return (enc(value));
      }
}
private static String getAction(int Flg){
 if(Flg == 1){
   return("BranchfavoriteDetail");
  }else{
   return("BranchlistDetail");
  }
}

/**
 * !)¶!)!)!)!)!)!)!)!)!)u<!)v!)u>!)v!)u&!)v!)u"!)v!)u'!)v!)!) entity !)\!)L!)!)!)!)!)!)!)!)!)!)!)!)!)B
 * @param	strSrc		!)!)!)!)!)¶!)!)!)!)
 * @return	!)!)!)!)!)!)!)!)!)¶!)!)!)!)
 */
public static final String enc(String strSrc){
  int				nLen;
  if(strSrc == null || (nLen = strSrc.length()) <= 0)
  return "";

  StringBuffer	sbEnc = new StringBuffer(nLen * 2);

  for(int i = 0; i < nLen; i++){
    char	c;
    switch(c = strSrc.charAt(i)){
      case '<':	sbEnc.append("&lt;");	break;
      case '>':	sbEnc.append("&gt;");	break;
      case '&':	sbEnc.append("&amp;");	break;
      case '"':	sbEnc.append("&quot;");	break;
      case '\'':sbEnc.append("&#39;");	break;
      case '\\':sbEnc.append("&yen;");	break;
      default: sbEnc.append(c); break;
    }
  }
  return sbEnc.toString();
}
/**
 * !)\!)!)!)!)!)!)!)h!)h!)i!)!)!)¶!)!)!)j!)!)!)!)!)!)!)!)!)!)!)!)&nbsp;!)!)!)!)!)!)!)!)!)!)!)!)!)!)!)!)!)!)!)\!)b!)h!)i!)m!)m!)!)!)!)!)!)!)!)!)!)!)!)!)K!)!)!)!)!)¨!)!)!)j
 * enc!)!)!)!)!)s!)!)!)!)
 * @param	value  !)\!)!)!)!)!)!)!)!)!)!)!)!)!)lValue!)l
 * @return	"value" !)!)!)!)!)!) "&nbsp;"
 */
public static final String encTableNBSP(String value){
  if(value.equals("")){
    return ("&nbsp;");
  }else{
    return (enc(value));
  }
}
%>
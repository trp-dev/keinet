<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Collection" %>
<%@ page import="jp.co.fj.keinavi.data.individual.SubRecordData" %>
<%@ page import="jp.co.fj.keinavi.data.individual.StudentData" %>
<%@ page import="jp.co.fj.keinavi.data.individual.ExaminationData" %>
<%@ page import="jp.co.fj.keinavi.util.KNUtil" %>
<jsp:useBean id="i002Bean" scope="request" class="jp.co.fj.keinavi.beans.individual.I002Bean" />
<jsp:useBean id="i002Form" scope="request" class="jp.co.fj.keinavi.forms.individual.I002Form" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    <%@ include file="/jsp/script/submit_change.jsp" %>
    <%@ include file="/jsp/script/submit_exit.jsp" %>
    <%@ include file="/jsp/script/submit_save.jsp" %>
    <%@ include file="/jsp/script/submit_menu.jsp" %>
    <%@ include file="/jsp/script/open_common.jsp" %>
    <%@ include file="/jsp/script/open_charge.jsp" %>
    // ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>

    <%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/dialog_close.jsp" %>

    var confirmDialogPattern = "";
    <%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

    // 担当クラス変更時は全クラス
    function submitClosed() {
        document.forms[0].targetGrade.value = "";
        document.forms[0].targetClass.value = "all";
        submitForm('i002');
    }

    // --------------------------------------------------
    // 模試データ
    var exam = new Array();
    <c:forEach var="exam" items="${ExamDataList}" varStatus="status">
        exam[<c:out value="${status.index}" />] = new Array(
            "<c:out value="${exam.examYear}" />",
            "<c:out value="${exam.examCD}" />",
            "<c:out value="${exam.examName}" />",
            "<c:out value="${exam.examTypeCD}" />",
            "<c:out value="${exam.targetGrade}" />",
            "<c:out value="${exam.dataOpenDate}" />",
            "<c:out value="${exam.dispSequence}" />");
    </c:forEach>

    // --------------------------------------------------
    // チェック状態を保持する配列
    var checked = new Array();
    var container = new Array();
    <c:forEach var="person" items="${i002Form.personId}" varStatus="status">
        checked["<c:out value="${person}" />"] = true;
        container[<c:out value="${status.index}" />] = "<c:out value="${person}" />";
    </c:forEach>

    function selectPerson() {
        var obj = frames['inside'].event.srcElement;

        <c:if test="${ not iCommonMap.bunsekiMode }">
            // 面談モードはラジオボタンなのでチェック状態はクリアする
            checked = new Array();
        </c:if>

        checked[obj.value] = obj.checked;
    }
    // --------------------------------------------------
    // 表示クラスデータ
    var classData = new Array();
    <c:forEach var="class" items="${ChargeClass.classList}" varStatus="status">
        classData[<c:out value="${status.index}" />] = new Array(
            "<c:out value="${class.grade}" />",
            "<c:out value="${class.key}" />",
            "<c:out value="${class.className}" />");
    </c:forEach>
    // --------------------------------------------------

    /**
     * サブミッション
     */
    function submitForm(forward) {
        var subForm = frames['inside'].document.forms[0];
        var form = document.forms[0];
        <%--スクロール位置 0426 K.Kondo--%>
        if(forward=='<c:out value="${param.forward}" />') {
            subForm.scrollX.value = document.body.scrollLeft;
            subForm.scrollY.value = document.body.scrollTop;
        } else {
            subForm.scrollX.value = 0;
            subForm.scrollY.value = 0;
        }
        document.getElementById("MainLayer").style.visibility = "hidden"
        document.getElementById("MainLayer").style.top = -2000;
        document.getElementById("LoadingLayer").style.visibility = "visible";

        if (form.targetExamYear.selectedIndex >= 0)
            subForm.targetExamYear.value = form.targetExamYear.options[form.targetExamYear.selectedIndex].value;
        if (form.targetExamCode.selectedIndex >= 0)
            subForm.targetExamCode.value = form.targetExamCode.options[form.targetExamCode.selectedIndex].value;
        if (form.targetExamName.selectedIndex >= 0)
            subForm.targetExamName.value = form.targetExamCode.options[form.targetExamCode.selectedIndex].text;
        if (form.targetExamCodePrev.selectedIndex >= 0)
            subForm.targetExamCodePrev.value = form.targetExamCodePrev.options[form.targetExamCodePrev.selectedIndex].value;
        if (form.targetGrade.selectedIndex >= 0)
            subForm.targetGrade.value = form.targetGrade.options[form.targetGrade.selectedIndex].value;
        if (form.targetClass.selectedIndex >= 0)
            subForm.targetClass.value = form.targetClass.options[form.targetClass.selectedIndex].value;

        <c:if test="${iCommonMap.bunsekiMode}">
            if (form.targetSubject.selectedIndex >= 0)
                subForm.targetSubject.value = form.targetSubject.options[form.targetSubject.selectedIndex].value;
        </c:if>

        subForm.changeMode.value = form.changeMode.value;

        setUpPersonId(true);

        subForm.forward.value = forward;
        subForm.backward.value =  "<c:out value="${param.forward}" />";
        subForm.target = "_parent";
        subForm.method = "POST";
        subForm.action = "<%=KNUtil.getContextPath(request)%>/<c:url value="I002Servlet" />";
        subForm.submit();
    }

    // 画面に無い生徒を保持する
    function setUpPersonId(mode) {
        // 学年の選択がなければ中止
        if (document.forms[0].targetGrade.selectedIndex < 0) return;
        // 選択されている学年
        var grade = document.forms[0].targetGrade.options[document.forms[0].targetGrade.selectedIndex].value;
        // 生徒のチェックボックスまたはラジオボタン
        var e = frames['inside'].document.forms[0].personId;
        // チェックボックスがなければ中止
        if (!e) return;

        // 学年の変更がない場合
        if (grade == "<c:out value="${i002Form.targetGrade}" />") {
            var registed = new Array(); // 選択されている生徒の入れ物
            if (e.length) {
                for (var i=0; i<e.length; i++) {
                    if (e[i].checked) registed[e[i].value] = true;
                }
            } else if (e.checked) {
                registed[e.value] = true;
            }

            for (var i=0; i<container.length; i++) {
                var id = container[i];
                if (registed[id]) continue;
                if (checked[id]) {
                    var body = frames['inside'].document.getElementsByTagName("FORM").item(0);

                    <%-- 2015/12/18 QQ)Hisakawa 大規模改修 ADD START --%>
                    var input = frames['inside'].document.createElement("input");

                    input.setAttribute("type", "hidden");
                    input.setAttribute("name", "personId");
                    input.setAttribute("value", id);
                     <%-- 2015/12/18 QQ)Hisakawa 大規模改修 ADD END   --%>

                    body.appendChild(
                         <%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
                        // frames['inside'].document.createElement("<input type=\"hidden\" name=\"personId\" value=\"" + id + "\">")
                        input
                         <%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>
                    );
                }
            }
        // 学年に変更があれば生徒の選択はクリア
        } else if (mode) {
            if (e.length) {
                for (var i=0; i<e.length; i++) {
                    e[i].checked = false;
                }
            } else {
                e.checked = false;
            }
        }
    }

    /**
     * 戻るボタン
     */
    function submitBack() {
        document.getElementById("MainLayer").style.visibility = "hidden"
        document.getElementById("MainLayer").style.top = -2000;
        document.getElementById("LoadingLayer").style.visibility = "visible";
        document.forms[0].target = "_self";
        document.forms[0].forward.value = "i001";
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }

    /**
     * メニュー選択へのサブミッション
     */
    function submitNext(frameId){
        setUpPersonId(false);
        if(checkStudentChecks(frameId)){
            document.getElementById("MainLayer").style.visibility = "hidden"
            document.getElementById("MainLayer").style.top = -2000;
            document.getElementById("LoadingLayer").style.visibility = "visible";
            <c:if test="${iCommonMap.bunsekiMode}">
            var form = document.forms[0];
            if (form.targetExamYear.selectedIndex >= 0)
                frames[frameId].document.forms[0].elements['targetExamYear'].value = document.forms[0].targetExamYear.options[document.forms[0].targetExamYear.selectedIndex].value;
            if (form.targetExamCode.selectedIndex >= 0)
                frames[frameId].document.forms[0].elements['targetExamCode'].value = document.forms[0].targetExamCode.options[document.forms[0].targetExamCode.selectedIndex].value;
            if (form.targetExamName.selectedIndex >= 0)
                frames[frameId].document.forms[0].elements['targetExamName'].value = document.forms[0].targetExamCode.options[document.forms[0].targetExamCode.selectedIndex].text;
            </c:if>
            frames[frameId].document.forms[0].forward.value = "i003";
            frames[frameId].document.forms[0].backward.value = "i002";
            frames[frameId].document.forms[0].target = "_parent";
            frames[frameId].document.forms[0].method = "POST";
            frames[frameId].document.forms[0].action = "<%=KNUtil.getContextPath(request)%>/<c:url value="I002Servlet" />";
            frames[frameId].document.forms[0].submit();
        }else{
            alert("生徒を選択して下さい。");
        }
    }

    /**
     * 生徒情報
     */
    var index = 0;
    var personString = new Array();
    <c:forEach var="data" items="${recordList}">
        personString[index++] = new Array(
            '<c:out value="${data.studentPersonId}"/>'
            ,'<c:out value="${data.studentGrade}"/>'
            ,'<c:out value="${data.studentClass}"/>'
            ,'<c:out value="${data.studentClassNo}"/>'
            ,'<c:out value="${data.studentNameKana}"/>'
            ,'<c:out value="${data.subRecordData.subName}"/>'
            <kn:choose>
                <%--受験学力測定テスト--%>
                <kn:examWhen id="ability" var="${iCommonMap.examData}">
                    ,'-999'
                    ,'-999'
                </kn:examWhen>
                <kn:otherwise>
                    ,'<c:out value="${data.subRecordData.SDeviation}"/>'
                    ,'<c:out value="${data.subRecordData.comparedCDeviation}" default="-999"/>'
                </kn:otherwise>
            </kn:choose>
            ,'<c:out value="${data.studentSortNo}"/>'
        );
    </c:forEach>
    /**
     * 科目情報
     */
    var subjectString  = new Array();
    <c:forEach var="data" items="${subjectList}" varStatus="status">
        subjectString[<c:out value="${status.index}"/>] = new Array('<c:out value="${data.subjectCd}"/>', '<c:out value="${data.subjectName}"/>');
    </c:forEach>
    /**
     * ソート
     */
    var hikakuUp = false;
    var hensaUp = false;
    var classUp = true;
    function sort(index) {
            personString.sort(
                function (a, b) {
                    if (a[index] == b[index]){
                        return 0;
                    }else if (a[index] > b[index]){
                        return 1;
                    }else if (a[index] < b[index]){
                        return -1;
                    }
                }
            );
            if(index == 2){
                if(!classUp)
                    personString.reverse();
                classUp = !classUp;
            }else if(index == 6){
                if(!hensaUp)
                    personString.reverse();
                hensaUp = !hensaUp;
            }else if(index == 7){
                if(!hikakuUp)
                    personString.reverse();
                hikakuUp = !hikakuUp;
            }

        // 再描画
        destroy_table("inside", 'foo');
        create_table("inside", 'foo', personString);
    }

    // クラス番号順にソートする
    function orderByClassNo() {
        personString.sort(
            function (a, b) {
                // まず学年表示番号でソート
                if (a[8] == b[8]){
                    // 学年がいっしょならクラスでソート
                    if (a[2] == b[2]){
                        // クラスがいっしょならクラス番号でソート
                        if (a[3] == b[3]){
                            return 0;
                        }else if (a[3] > b[3]){
                            return 1;
                        }else if (a[3] < b[3]){
                            return -1;
                        }
                    }else if (a[2] > b[2]){
                        return 1;
                    }else if (a[2] < b[2]){
                        return -1;
                    }
                }else if (a[8] > b[8]){
                    return 1;
                }else if (a[8] < b[8]){
                    return -1;
                }
            }
        );
        // 再描画
        destroy_table("inside", 'foo');
        create_table("inside", 'foo', personString);
    }

    // 偏差値降順にソートする
    function orderByADeviation() {
        personString.sort(
            function (a, b) {
                if (a[6] == b[6]){
                    // まず学年表示番号でソート
                    if (a[8] == b[8]){
                        // 学年がいっしょならクラスでソート
                        if (a[2] == b[2]){
                            // クラスがいっしょならクラス番号でソート
                            if (a[3] == b[3]){
                                return 0;
                            }else if (a[3] > b[3]){
                                return 1;
                            }else if (a[3] < b[3]){
                                return -1;
                            }
                        }else if (a[2] > b[2]){
                            return 1;
                        }else if (a[2] < b[2]){
                            return -1;
                        }
                    }else if (a[8] > b[8]){
                        return 1;
                    }else if (a[8] < b[8]){
                        return -1;
                    }
                }else if (a[6] > b[6]){
                    return -1;
                }else if (a[6] < b[6]){
                    return 1;
                }
            }
        );
        // 再描画
        destroy_table("inside", 'foo');
        create_table("inside", 'foo', personString);
    }

    // 前回と比較を降順にソートする
    function orderByComparedCDeviation() {
        personString.sort(
            function (a, b) {
                if (a[7] == b[7]){
                    // まず学年表示番号でソート
                    if (a[8] == b[8]){
                        // 学年がいっしょならクラスでソート
                        if (a[2] == b[2]){
                            // クラスがいっしょならクラス番号でソート
                            if (a[3] == b[3]){
                                return 0;
                            }else if (a[3] > b[3]){
                                return 1;
                            }else if (a[3] < b[3]){
                                return -1;
                            }
                        }else if (a[2] > b[2]){
                            return 1;
                        }else if (a[2] < b[2]){
                            return -1;
                        }
                    }else if (a[8] > b[8]){
                        return 1;
                    }else if (a[8] < b[8]){
                        return -1;
                    }
                }else if (a[7] - 0 > b[7] - 0){
                    return -1;
                }else if (a[7] - 0 < b[7] - 0){
                    return 1;
                }
            }
        );
        // 再描画
        destroy_table("inside", 'foo');
        create_table("inside", 'foo', personString);
    }

    /**
     * 生徒一覧作成
     */
    function create_table(frameId, tableId, array){
        for(var i=0; i<array.length; i++){
            add_row(frameId, tableId, array[i]);
        }
        var e = frames['inside'].document.forms[0].elements['personId'];
        if (!e) return;
        if (e.length) {
            for(var i=0; i<e.length; i++){
                if (checked[e[i].value]) e[i].checked = true;
            }
        } else {
            if (checked[e.value]) e.checked = true;
        }
    }
    /**
     * 生徒一覧
     */
    function add_row(frameId, tableId, ary2) {
        table = frames[frameId].document.getElementById(tableId);
        new_row = table.insertRow(table.rows.length);
        new_row.style.height = "27";
        new_row.style.backgroundColor = "#F4E5D6";

        <c:choose>
            <c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
                <%-- 分析モードはチェックボックス --%>
                 <%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
                // var input = frames[frameId].document.createElement("<input type=\"checkbox\" name=\"personId\">");
                var input = frames[frameId].document.createElement("input");
                input.setAttribute("type", "checkbox");
                input.setAttribute("name", "personId");
                 <%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

                input.setAttribute("id", "personId");
                input.setAttribute("value", ary2[0]);
                // --------------------------------------------------
                // イベントを設定
                // input.attachEvent("onclick", window.self.selectPerson);
                if(input.addEventListener){
                    input.addEventListener('click', window.self.selectPerson, false);
                }else if(input.attachEvent){
                    input.attachEvent('onclick', window.self.selectPerson);
                }
                // --------------------------------------------------
            </c:when>
            <c:otherwise>
                <%-- 面談モードはラジオボタン --%>
                //var input = frames[frameId].document.createElement('input');
                //MSDNによると以上の方法は使えない
                //http://msdn.microsoft.com/library/default.asp?url=/workshop/author/dhtml/reference/properties/name_2.asp
                 <%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
                // var input = frames[frameId].document.createElement("<input type=\"radio\" name=\"personId\">");
                // input.setAttribute("id", "personId");
                // input.setAttribute("value", ary2[0]);


                var input = frames[frameId].document.createElement("input");
                input.setAttribute("type", "radio");
                input.setAttribute("name", "personId");
                 <%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>

                input.setAttribute("id", "personId");
                input.setAttribute("value", ary2[0]);
                // --------------------------------------------------
                // イベントを設定
                // input.attachEvent("onclick", window.self.selectPerson);
                if(input.addEventListener){
                    input.addEventListener('click', window.self.selectPerson, false);
                }else if(input.attachEvent){
                    input.attachEvent('onclick', window.self.selectPerson);
                }
                // --------------------------------------------------
            </c:otherwise>
        </c:choose>

        <%-- デフォルトのチェックをつける （チェック状態は配列で一元管理するのでコメントアウト）
        <c:if test="${i002From != null && i002Form.personId != null}">
            <c:forEach var="id" items="${i002Form.personId}">
                if(ary2[0] == '<c:out value="id"/>'){
                    input.checked = "true";
                }
            </c:forEach>
        </c:if>
        --%>

        // ラジオボタン・チェックボックス
        col1 = new_row.insertCell(0);
        col1.align = "center";
        col1.appendChild(input);
        // 学年
        col2 = new_row.insertCell(1);
        col2.align = "center";
        col2.className = "text12";
        col2.style.width = "9%";
        col2.appendChild(frames[frameId].document.createTextNode(ary2[1]));
        // クラス
        col3 = new_row.insertCell(2);
        col3.align = "center";
        col3.className = "text12";
        col3.style.width = "9%";
        col3.appendChild(frames[frameId].document.createTextNode(ary2[2]));
        // クラス番号
        col4 = new_row.insertCell(3);
        col4.align = "center";
        col4.className = "text12";
        col4.appendChild(frames[frameId].document.createTextNode(ary2[3]));
        // 氏名
        col5 = new_row.insertCell(4);
        col5.className = "text12";
        col5.appendChild(frames[frameId].document.createTextNode(ary2[4]));

        <%-- 分析モードのみ --%>
        <c:if test="${iCommonMap.bunsekiMode}">
            // 科目
            col6 = new_row.insertCell(5);
            col6.align = "center";
            col6.className = "text12";
            col6.appendChild(frames[frameId].document.createTextNode(ary2[5]));

            // 偏差値
            col7 = new_row.insertCell(6);
            col7.align = "center";
            col7.className = "text12";

            var deviation = parseFloat(ary2[6]);

            if (deviation == -999 || isNaN(deviation)) {
                deviation = "";
            } else {
                deviation = ary2[6];
            }

            col7.appendChild(frames[frameId].document.createTextNode(deviation));

            // 前回差
            col8 = new_row.insertCell(7);
            col8.align = "center";
            col8.className = "text12";

            var value = parseFloat(ary2[7]);
            // レコードなし
            if (value == -999 || isNaN(value)) {
                value = "";
            // UP
            } else if (value > 0) {
                if (value % 1 == 0) value += ".0"
                value = "△" + value;
            // DOWN
            } else if (value < 0) {
                value = 0 - value;
                if (value % 1 == 0) value += ".0"
                value = "▼" + value;
            }
            col8.appendChild(frames[frameId].document.createTextNode(value));
        </c:if>
    }

    /**
     * 生徒一覧削除
     */
    function destroy_table(frameId, tableId){
        table = frames[frameId].document.getElementById(tableId);
        while(1 < table.rows.length){
            table.deleteRow(1);
        }
    }

    /**
     * 生徒一覧
     */
    function loadDatas(frameId, array){
        create_table(frameId, 'foo', personString);
    }

    /**
     * クラスロード
     */
    function loadClass(){
        var form = document.forms[0];
        var option = form.targetClass;
        var index = form.targetGrade.selectedIndex;

        if (index < 0) return;

        var grade = form.targetGrade.options[index].value;

        // オプションクリア
        for (var i=0; option.options != null && i<option.options.length; i++) {
            option.options[i] = null;
        }
        option.options.length = 0;

        // 生成
        option.options[option.options.length] = new Option("全担当クラス", "all");
        for (var i=0; i<classData.length; i++){
            if (classData[i][0] == grade) {
                var o = new Option(classData[i][2], classData[i][1]);
                if ("<c:out value="${i002Form.targetClass}" />" == o.value) o.selected = true;
                option.options[option.options.length] = o;
            }
        }
    }

    /**
     * 科目ロード
     */
    function loadSubject(){
        create2(document.forms[0].targetSubject, subjectString);
        //showDatas('inside');
    }

    /**
     * 対象年度ロード
     */
    function loadTargetExamYear(){
        //create2(document.forms[0].targetExamYear, yearString);
        loadTargetExamName();
    }

    /**
     * 今回対象模試ロード
     */
    function loadTargetExamName(){
        var form = document.forms[0];
        var year = getSelectedValue(form.targetExamYear);
        var option = form.targetExamCode;
        clearOption(option);

        // 生成
        for (var i=0; i<exam.length; i++){
            if (exam[i][0] == year) {
                var o = new Option(exam[i][2], exam[i][1]);
                if ("<c:out value="${i002Form.targetExamCode}" />" == o.value) o.selected = true;
                option.options[option.options.length] = o;
            }
        }

        loadPrevExamName();
    }

    /**
     * 前回対象模試ロード
     */
    function loadPrevExamName(){
        var form = document.forms[0];
        var year = getSelectedValue(form.targetExamYear);
        var current = getSelectedValue(form.targetExamCode);
        var option = form.targetExamCodePrev;
        clearOption(option);

        // 今回対象模試の情報を取得する
        var type = "";
        var grade = "";
        var date = "";
        for (var i=0; i<exam.length; i++){
            if (exam[i][0] == year && exam[i][1] == current) {
                type = exam[i][3];
                grade = exam[i][4];
                date = exam[i][5];
            }
        }

        // 模試種類が同じ過去の模試で同年度・同学年
        for (var i=0; i<exam.length; i++){
            if (exam[i][0] == year && exam[i][3] == type && exam[i][4] == grade && exam[i][5] < date) {
                var o = new Option(exam[i][2], exam[i][1]);
                if ("<c:out value="${i002Form.targetExamCodePrev}" />" == o.value) o.selected = true;
                option.options[option.options.length] = o;
            }
        }
    }

    //（汎用的）選択されているセレクト部品の値を取得する
    function getSelectedValue(obj) {
        if (obj == null) return "";
        var index = obj.selectedIndex;
        if (index >= 0) return obj.options[index].value;
        return "";
    }

    // オプションクリア
    function clearOption(obj) {
        if (!obj.options) return;
        for (var i=0; i<obj.options.length; i++) {
            obj.options[i] = null;
        }
        obj.options.length = 0;
    }

    /**
     * http://www.ybi.co.jp/koike/qa2500/qa2867.htm
     */
<%-- 2015/12/24 QQ)Hisakawa 大規模改修 UPD START --%>
    function create2(objList, objArray){
        if(objArray != null && objArray.length > 0){
        var nMax = objArray.length;
        var nLoop = 0;
        for (nLoop = 0; nLoop < nMax; nLoop++){
            oAdd = document.createElement('OPTION');
            if(objList.children[nLoop]  != undefined)
                objList.removeChild(objList.children[nLoop]);
            objList.appendChild(oAdd);
            objList.children[nLoop].setAttribute('value', objArray[nLoop][0]);

            if(objList == document.forms[0].elements['targetExamYear'] && objArray[nLoop][0] == '<%=i002Form.getTargetExamYear()%>'){
                objList.children[nLoop].setAttribute('selected', true);
            }else if(objList == document.forms[0].elements['targetExamCode'] && objArray[nLoop][0] == '<%=i002Form.getTargetExamCode()%>'){
                objList.children[nLoop].setAttribute('selected', true);
            }else if(objList == document.forms[0].elements['targetExamCodePrev'] && objArray[nLoop][0] == '<%=i002Form.getTargetExamCodePrev()%>'){
                objList.children[nLoop].setAttribute('selected', true);
            }else if(objList == document.forms[0].elements['targetGrade'] && objArray[nLoop][0] == '<%=i002Form.getTargetGrade()%>'){
                objList.children[nLoop].setAttribute('selected', true);
            }else if(objList == document.forms[0].elements['targetClass'] && objArray[nLoop][0] == '<c:out value="${i002Form.targetClass}" />'){
                objList.children[nLoop].setAttribute('selected', true);
            }else if(objList == document.forms[0].elements['targetSubject'] && objArray[nLoop][0] == '<%=i002Form.getTargetSubject()%>'){
                objList.children[nLoop].setAttribute('selected', true);
            }

            oAddx= document.createTextNode(objArray[nLoop][1]);
            if(objList.children[nLoop].firstChild  != undefined)
                objList.children[nLoop].removeChild(objList.children[nLoop].firstChild);
            objList.children[nLoop].appendChild(oAddx);
        }
        objList.length=nLoop;
        }
    }
<%-- 2015/12/24 QQ)Hisakawa 大規模改修 UPD END   --%>

    /**
     * メニュー選択へ進んでもよいかどうかのチェック
     */
    function checkStudentChecks(frameId){
        // チェックがあればOK
        var e = frames[frameId].document.forms[0].personId;

        if (e == null) {
            return false;
        } else if (e.length) {
            for (i=0; i<e.length; i++){
                if (e[i].checked) return true;
            }
        } else {
            if (e.checked) return true;
        }

        // 隠れてる生徒がいないか
        for (var i=0; i<container.length; i++) {
            if (checked[container[i]]) return true;
        }
        return false;
    }

    /**
     * 全ての生徒を選択
     */
    function checkAll(frameId, cName, check) {
        for (i=0,n=frames[frameId].document.forms[0].elements.length;i<n;i++){
            var obj = frames[frameId].document.forms[0].elements[i];
            if (obj.name.indexOf(cName) != -1){
                obj.checked = check;
                checked[obj.value] = check;
            }
        }
    }
    /**
     * 共通項目設定に渡す模試年度を動的に変更する
     */
    function setHiddenYear(self){
        document.forms[0].targetYear.value = document.forms[0].targetExamYear.options[document.forms[0].targetExamYear.selectedIndex].value;
    }
    /**
     * 共通項目設定に渡す模試コードを動的に変更する
     */
    function setHiddenExam(self){
        document.forms[0].targetExam.value = document.forms[0].targetExamCode.options[document.forms[0].targetExamCode.selectedIndex].value;
    }
    /**
     * 初期化
     */
    function init(){
        startTimer();
        <%--スクロール位置 0426 K.Kondo--%>
        var subForm = frames['inside'].document.forms[0];
        subForm.scrollX.value = '<c:out value="${i002Form.scrollX}" />';
        subForm.scrollY.value = '<c:out value="${i002Form.scrollY}" />';
        window.scrollTo(subForm.scrollX.value, subForm.scrollY.value);
        //生徒
        loadDatas('inside', personString);
        //模試
        loadTargetExamYear();
        //科目
        <c:if test="${iCommonMap.bunsekiMode}">loadSubject();</c:if>
        //クラス
        loadClass();
        //orderByClassNo();

        //ロード中
        <%@ include file="/jsp/script/loading.jsp" %>
    }
// -->
</script>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I002Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">
<input type="hidden" name="targetYear" value="<c:out value="${iCommonMap.targetExamYear}" default="${Profile.targetYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${iCommonMap.targetExamCode}" default="${Profile.targetExam}" />">

<input type="hidden" name="changed" value="<c:choose><c:when test="${Profile.changed}">1</c:when><c:otherwise>0</c:otherwise></c:choose>">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="160"><img src="./individual/img/ttl_kojin.gif" width="149" height="24" border="0" alt="個人成績分析"><br></td>
<td width="1" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="737"><img src="./individual/img/ttl_kojin_read.gif" width="262" height="16" border="0" alt="個人別に詳細な成績分析を行います。"><br></td>
</tr>
</table>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="27" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="899">

<table border="0" cellpadding="0" cellspacing="0" width="899">
<tr valign="top">
<td colspan="4" width="899" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="899" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="899" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="899" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="886" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">担当クラス</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="899" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="899" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->

<!--担当クラス-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="878">
<tr valign="top">
<td width="876" bgcolor="#FFFFFF" align="center">
<b class="text14">
<c:choose>
    <c:when test="${ empty ChargeClass.classList }">設定なし</c:when>
    <c:otherwise>
        <c:out value="${ChargeClass.year}" />年度
        <c:forEach var="class" items="${ChargeClass.classList}">
            &nbsp;<c:out value="${class.grade}" />年<c:out value="${class.className}" />
        </c:forEach>
    </c:otherwise>
</c:choose>
</b></td>
</tr>
</table>
</td>
</tr>
</table>

<div style="margin-top:8px;">
<!--設定ボタン--><input onclick="javascript:openCharge()" type="button" value="&nbsp;設定変更&nbsp;" class="text12" style="width:120px;">
</div>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/担当クラス-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->


<!--生徒一覧-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="878">
<tr valign="top">
<td width="876" bgcolor="#FFFFFF" align="center">
<!--ケイコさん-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="624">
<tr valign="top">
<td width="72" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="542">

<table border="0" cellpadding="0" cellspacing="0" width="542">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="518" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="518" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="518" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="518" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="542">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="15" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="15" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="528" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="528">
<tr>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="498"><span class="text14">担当クラスに設定されている生徒を一覧表示しています。<br>
分析もしくは資料を印刷したい生徒を選択し、「メニュー選択へ」を押してください。</span></td>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="542">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="518" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="518" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="518" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="518" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--ツールバー-->
<table border="0" cellpadding="4" cellspacing="0" width="833">
<tr>
<td width="833" bgcolor="#93A3AD">

<table border="0" cellpadding="0" cellspacing="0" width="825">
<tr>

<td width="110">
<c:if test="${iCommonMap.bunsekiMode}">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="すべて選択" class="text12" style="width:100px;" onclick="javascript:checkAll('inside', 'personId', true);"></td>
</tr>
</table>
</c:if>
</td>

<td width="110">
<c:if test="${iCommonMap.bunsekiMode}">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="すべて解除" class="text12" style="width:100px;" onclick="javascript:checkAll('inside', 'personId', false);"></td>
</tr>
</table>
</c:if>
</td>

<td width="602" align="right">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><font class="text12" style="color:#FFFFFF;">表示学年&nbsp;：&nbsp;</font></td>
<td>
<SELECT class="text12" id="targetGrade" name="targetGrade" onChange="loadClass()">
<c:forEach var="grade" items="${GradeList}">
<option value="<c:out value="${grade}" />"<c:if test="${ i002Form.targetGrade == grade }"> selected</c:if>><c:out value="${grade}" /></option>
</c:forEach>
</SELECT>
</td>

<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><font class="text12" style="color:#FFFFFF;">表示クラス&nbsp;：&nbsp;</font></td>
<td><SELECT class="text12" id="targetClass" name="targetClass"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>

<td>
<c:if test="${iCommonMap.bunsekiMode}">
<font class="text12" style="color:#FFFFFF;">表示科目&nbsp;：&nbsp;</font>
</c:if>
</td>

<td>
<c:if test="${iCommonMap.bunsekiMode}">
<SELECT class="text12" id="targetSubject" name="targetSubject" style="width:130px;">
</c:if>
</td>

<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="64" bgcolor="#FFFFFF" align="center"><input type="button" value="再表示" class="text12" style="width:60px;" ONCLICK="submitForm('i002')"></td>
</tr>
</table>
</td>
<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ツールバー-->

<a name="here"></a>
<%-- check check check --%>
<iframe src="<c:url value="./jsp/individual/i002_list.jsp" />" name="inside" width="834" height="320" scrolling="yes"></iframe>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/概要-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/生徒一覧-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<div <c:if test="${sessionScope['iCommonMap'].mendanMode}">style="display:none"</c:if>>
<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="899">

<table border="0" cellpadding="0" cellspacing="0" width="899">
<tr valign="top">
<td colspan="4" width="899" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="899" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="899" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="899" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="886" bgcolor="#758A98">

<table border="0" cellpadding="0" cellspacing="0" width="886">
<tr>
<td width="7"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><br></td>
<td width="374"><b class="text14" style="color:#FFFFFF;">参照模試の選択</b></td>
<td width="500" align="right"><font class="text12" style="color:#FFFFFF;">※前回模試は、今回模試で選択された模試と同系統の模試のみ選択可能です。</font></td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
<tr valign="top">
<td colspan="4" width="899" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="899" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->


<!--参照模試の選択-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="878">
<tr valign="top">
<td width="876" bgcolor="#FFFFFF" align="center">
<!--今回-->
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="70"><b class="text14">今回</b></td>
<td><span class="text14">対象年度&nbsp;：&nbsp;</span></td>
<td>

<SELECT class="text12" id="targetExamYear" name="targetExamYear" onChange="loadTargetExamName();setHiddenYear(this);">
<c:forEach var="year" items="${ExamYearList}">
    <option value="<c:out value="${year}" />"<c:if test="${ i002Form.targetExamYear == year }"> selected</c:if>><c:out value="${year}" /></option>
</c:forEach>
</SELECT>

</td>
<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td><span class="text14">対象模試&nbsp;：&nbsp;</span></td>
<td>

<SELECT class="text12" id="targetExamCode" name="targetExamCode" style="width:290px;" onChange="loadPrevExamName();setHiddenExam(this);"></SELECT>
<input type="hidden" name="targetExamName" value="">

</td>
</tr>
</table>
<!--/今回-->
</td>
</tr>
<tr valign="top">
<td width="876" bgcolor="#FFFFFF" align="center">
<!--前回-->
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="70"><b class="text14">前回</b></td>
<td><!-- deleted ... --></td>
<td><!-- deleted ... --></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td><span class="text14">対象模試&nbsp;：&nbsp;</span></td>
<td>

<SELECT class="text12" id="targetExamCodePrev" name="targetExamCodePrev" style="width:290px;" onChange=""></SELECT>
<INPUT TYPE="BUTTON" VALUE=" 再表示 " ONCLICK="submitForm('i002')">
</td>
</tr>
</table>
<!--/前回-->
</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/参照模試の選択-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->
</div>

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;メニュー選択へ&nbsp;" class="text12" style="width:140px;" onclick="submitNext('inside')">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</div>
</body>
</html>

<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<jsp:useBean id="pageBean" scope="request" type="jp.co.fj.keinavi.beans.individual.judgement.SubjectPageBean" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script language="JavaScript">
<!--
  <%@ include file="/jsp/script/timer.jsp" %>
  <%@ include file="/jsp/script/open_schedule_alert.jsp" %>

  function init() {
    startTimer();
  }

//-->
</script>
<style type="text/css">
span.full1 {
    background-color: #f49e00;
    color: #ffffff;
    margin-left: 5px;
    padding:3px;
}
span.full2 {
    background-color: #00ac71;
    color: #ffffff;
    margin-left: 5px;
    padding:3px;
}
</style>
</head>

<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="I206Servlet" />" method="post">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="863" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="871" bgcolor="#FFFFFF" align="center">

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="804">

<table border="0" cellpadding="0" cellspacing="0" width="804">
<tr valign="top">
<td colspan="5" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="388" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;"><c:out value="${pageBean.univName}"/></b></td>
<td width="400" bgcolor="#758A98" align="right">

<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><span class="text14" style="color:#FFFFFF;"><b>学部&nbsp;：&nbsp;</b><c:out value="${pageBean.facultyName}"/></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="2" border="0" alt=""><br></td>
<td><span class="text14" style="color:#FFFFFF;"><b>学科&nbsp;：&nbsp;</b><c:out value="${pageBean.deptName}"/></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
<tr valign="top">
<td colspan="5" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--情報-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><span class="text12"><b>本部所在地&nbsp;：&nbsp;</b><c:out value="${pageBean.prefName}"/></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="2" border="0" alt=""><br></td>
<td><span class="text12"><b>キャンパス所在地&nbsp;：&nbsp;</b><c:out value="${pageBean.prefName_sh}"/></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="2" border="0" alt=""><br></td>
<c:if test="${ pageBean.examCapaRel ne '8' and pageBean.capacity ne '0' }">
  <td><span class="text12"><b>定員&nbsp;：&nbsp;</b><c:if test="${ pageBean.examCapaRel eq '9' }">推定</c:if><c:out value="${pageBean.capacity}"/>人</span></td>
  <td><img src="./shared_lib/img/parts/sp.gif" width="20" height="2" border="0" alt=""><br></td>
</c:if>
<td><span class="text12"><b>入試日&nbsp;：&nbsp;</b><c:out value="${pageBean.dateCorrelation}"/></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="2" border="0" alt=""><br></td>
<td><span class="text12">※<a href="#here" onclick="openScheduleAlert(1);">入試日程に関するご注意</a></span></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/情報-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!---->
<table border="0" cellpadding="4" cellspacing="0" width="820">
<tr>
<td width="820" bgcolor="#93A3AD">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="7"><img src="./shared_lib/img/parts/sp.gif" width="7" height="25" border="0" alt=""><br></td>
<td><b class="text12" style="color:#FFFFFF;">公表科目</b></td>
</tr>
</table>
</td>
</tr>
</table>
<!--/-->

<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="14" cellspacing="1" width="820">
<tr valign="top">
<td width="818" bgcolor="#EFF2F3" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="12" cellspacing="1" width="790">
<tr valign="top">
<td width="788" bgcolor="#FFFFFF">

<table border="0" cellpadding="2" cellspacing="2" width="764">
<c:set var="detailPageListSize"><%=pageBean.getDetailPageList().size()%></c:set>
<c:set var="branchNameCount" value="0" />
<c:forEach var="detail" items="${pageBean.detailPageList}" varStatus="status">
  <c:if test="${ not empty detail.branchName }"><c:set var="branchNameCount" value="${ branchNameCount + 1 }" /></c:if>
</c:forEach>
<c:forEach var="detail" items="${pageBean.detailPageList}" varStatus="status">
<!--タイトルー-->
<tr height="22" bgcolor="#8CA9BB">
<td colspan="9" width="100%"><b class="text12" style="color:#FFFFFF;">&nbsp;科目・配点
<c:if test="${detailPageListSize > 1}">
  <c:choose>
    <c:when test="${ branchNameCount eq 0 }">【<c:out value="${status.count}" />】</c:when>
    <c:when test="${ not empty detail.branchName }">【<c:out value="${detail.branchName}" />】</c:when>
  </c:choose>
</c:if>
</b></td>
</tr>
<!--/タイトル-->
<!--センター-->
<c:if test="${detail.CExist}">
<!--タイプ-->
<tr height="22" bgcolor="#CDD7DD">
<td colspan="9" width="100%"><b class="text12">&nbsp;センター</b></td>
</tr>
<!--教科数-->
<tr>
<td width="9%" bgcolor="#E1E6EB" align="center"><b class="text12">教科数</b></td>
<td colspan="8" width="84%" bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12-hh">
<c:out value="${detail.cenCourseNum}"/><br>
</span></td>
</tr>
</table>
</td>
</tr>
<!--/教科数-->
<!--公表科目-->
<tr>
<td width="9%" bgcolor="#E1E6EB" align="center"><b class="text12">公表科目</b></td>
<td colspan="8" width="84%" bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12-hh">
<c:out value="${detail.c_subString}"/>
<c:if test="${detail.firstAnsFlgSci}"><span class="full1">&nbsp;理1&nbsp;</span></c:if>
<c:if test="${detail.firstAnsFlgSoc}"><span class="full2">&nbsp;地公1&nbsp;</span></c:if>
</span></td>
</tr>
</table>
</td>
</tr>
<!--/公表科目-->
<!--配点-->
<tr>
<td width="9%" bgcolor="#E1E6EB" align="center"><b class="text12">配点</b></td>
<td colspan="8" width="84%" bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12-hh">
<c:out value="${detail.allot1}"/><br>
</span></td>
</tr>
</table>
</td>
</tr>
<!--/配点-->
<!--/タイプ-->
</c:if>
<!--/センター-->
<!--二次-->
<c:if test="${detail.SExist}">
<!--タイプ-->
<tr height="22" bgcolor="#CDD7DD">
<td colspan="9" width="100%"><b class="text12">&nbsp;２次</b></td>
</tr>
<!--公表科目-->
<tr>
<td width="9%" bgcolor="#E1E6EB" align="center"><b class="text12">公表科目</b></td>
<td colspan="8" width="84%" bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12-hh">
<c:out value="${detail.s_subString}"/><br>
</span></td>
</tr>
</table>
</td>
</tr>
<!--/公表科目-->
<!--配点-->
<tr>
<td width="9%" bgcolor="#E1E6EB" align="center"><b class="text12">配点</b></td>
<td colspan="8" width="84%" bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12-hh">
<c:out value="${detail.allot2}"/><br>
</span></td>
</tr>
</table>
</td>
</tr>
<!--/配点-->
<!--/タイプ-->
<!--spacer-->
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<!--/spacer-->
</c:if>
<!--/二次-->
</c:forEach>
</table>

<div style="margin:8px 8px 0px 8px">
<span class="text12">ここに表示されている定員・入試科目・配点・選考方法・入試日程等は変更される可能性がありますので、<br>必ず大学発表の学生募集要項で確認してください。</span>
</div>

</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/概要-->


<!--ボタン-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td align="center"><input onclick="javascript:window.close()" type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw_01.jsp" %>
<!--/FOOTER-->
</form>
</div>
</body>
</html>

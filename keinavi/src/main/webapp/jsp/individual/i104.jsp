<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.keinavi.data.individual.ExaminationData" %>
<%@ page import="jp.co.fj.keinavi.data.individual.CourseData" %>
<%@ page import="jp.co.fj.keinavi.util.GeneralUtil" %>
<%@ page import="jp.co.fj.keinavi.data.help.HelpList" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<jsp:useBean id="i104Bean" scope="request" class="jp.co.fj.keinavi.beans.individual.I104Bean" />
<jsp:useBean id="form" scope="request" class="jp.co.fj.keinavi.forms.individual.I104Form" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<SCRIPT type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    <%@ include file="/jsp/script/submit_change.jsp" %>
    <%@ include file="/jsp/script/submit_exit.jsp" %>
    <%@ include file="/jsp/script/submit_save.jsp" %>
    <%@ include file="/jsp/script/submit_menu.jsp" %>
    <%@ include file="/jsp/script/open_common.jsp" %>
    <%@ include file="/jsp/script/open_sample.jsp" %>
    <%@ include file="/jsp/script/submit_openOnepoint.jsp" %>
    <%@ include file="/jsp/script/submit_help.jsp" %>
    <%@ include file="/jsp/script/clear_disabled.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/printDialog_close.jsp" %>
	 var printDialogPattern = "";
	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

    <%-- 対象模試リスト --%>
    <c:set var="examList" value="${ iCommonMap.examComboData.listI }" />

    /**
     * サブミット
     */
    function submitForm(forward) {
        <%--スクロール位置 0426 K.Kondo--%>
        if(forward=='<c:out value="${param.forward}" />') {
            document.forms[0].scrollX.value = document.body.scrollLeft;
            document.forms[0].scrollY.value = document.body.scrollTop;
        } else {
            document.forms[0].scrollX.value = 0;
            document.forms[0].scrollY.value = 0;
        }
        if(forward == 'i105'){
        	<%-- 2016/01/21 QQ)Hisakawa 大規模改修 UPD START --%>
            // window.open("blank.html", "sheet", "resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=920");
            window.open("blank.html", "sheet", "resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=920,height=800");
        	<%-- 2016/01/21 QQ)Hisakawa 大規模改修 UPD END   --%>
            document.forms[0].target = "sheet";
            document.forms[0].action = "<c:url value="I105Servlet" />";
        }else{
            clearDisabled();
            document.getElementById("MainLayer").style.visibility = "hidden";
            document.getElementById("MainLayer").style.top = -2000;
            document.getElementById("LoadingLayer").style.visibility = "visible";
            document.forms[0].target = "_self";
        }
        document.forms[0].forward.value = forward;
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 戻るボタン
     */
    function submitBack() {
        document.forms[0].target = "_self";
        document.forms[0].forward.value = "i003";
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 再表示
     */
    function submitRefresh() {
        if(checkCombox()){
            submitForm('i104');
        }else{
            alert("志望大学にて同一大学が選択されています。異なる大学を選択し直してください。");
        }
    }
    /**
     * コンボチ重複チェック
     */
    function checkCombox(){
        var flag = true;
        if(document.forms[0].targetCandidateRank1.value != "")
            if(document.forms[0].targetCandidateRank1.value == document.forms[0].targetCandidateRank2.value || document.forms[0].targetCandidateRank1.value == document.forms[0].targetCandidateRank3.value)
                flag = false;
        if(document.forms[0].targetCandidateRank2.value != "")
            if(document.forms[0].targetCandidateRank2.value == document.forms[0].targetCandidateRank1.value || document.forms[0].targetCandidateRank2.value == document.forms[0].targetCandidateRank3.value)
                flag = false;
        if(document.forms[0].targetCandidateRank3.value != "")
            if(document.forms[0].targetCandidateRank3.value == document.forms[0].targetCandidateRank1.value || document.forms[0].targetCandidateRank2.value == document.forms[0].targetCandidateRank3.value)
                flag = false;
        return flag;
    }
    /**
     *
     */
    function listnone() {
        if(<%=i104Bean.getExaminationDatas().size()%> <= 0){
            document.getElementById("list").style.display = "none";
        }
    }
    /**
     * 生徒切り替え
     */
    <%@ include file="/jsp/individual/include/student_rotator.jsp" %>
    /**
     * 印刷ダイアログ
     */
    <%@ include file="/jsp/individual/include/printDialog.jsp" %>
    /**
     * 印刷チェック
     */
    function printCheck() {
        if (checkCombox()) {
            printDialog();
        } else {
            alert("志望大学にて同一大学が選択されています。異なる大学を選択し直してください。");
        }
    }

    /**
     * 一括印刷チェック
     */
    function printAllCheck() {
        if (checkCombox()) {
            printAllDialog();
        } else {
            alert("志望大学にて同一大学が選択されています。異なる大学を選択し直してください。");
        }
    }
    // 表示
    function printView() {
        if (checkCombox()) {
            document.forms[0].printStatus.value = "1";
            printSheet();
        } else {
            alert("志望大学にて同一大学が選択されています。異なる大学を選択し直してください。");
        }
    }
    /**
     * フォームデータ
     */
    var LOWER = new Array("30.0", "35.0", "40.0", "45.0", "50.0");
    <%-- 2019/08/09 Hics)Ueta 偏差値の最大値変更 UPD START --%>
    //var UPPER = new Array("50.0", "55.0", "60.0", "65.0", "70.0");
    var UPPER = new Array("50.0", "55.0", "60.0", "65.0", "70.0", "75.0", "80.0");
    <%-- 2019/08/09 Hics)Ueta 偏差値の最大値変更 UPD END   --%>
    var COMBOX = new Array(
        <%
        //大学リストを作成
        boolean firstFlg = true;
        for (java.util.Iterator it=i104Bean.getComboxDatas().iterator();  it.hasNext(); ) {
            ExaminationData data = (ExaminationData)it.next();
            if(!firstFlg){
                out.println(",");
            }
            out.print("new Array(");
            //志望順位
            out.println("\"" + data.getExamCd() + "\"");
            out.println(",");
            //大学名称を結合
            out.println("\""+ data.getExamName() + "\"");
            out.print(")");
            firstFlg = false;
        }

        %>
    );
    /**
     * コンボ
     */
    function loadCombox(obj1, obj2, obj3){
        <%if(form.getTargetCandidateRank1() != null && i104Bean.getComboxDatas().size() > 0){%>
            var blank1 = document.createElement("OPTION");
            blank1.text = "";
            blank1.value = "10";
            obj1.add(blank1);
            for(var i=0; i<COMBOX.length; i++){
                var input = document.createElement("OPTION");
                input.text = COMBOX[i][1];
                input.value = COMBOX[i][0];
                if(COMBOX[i][0] == "<%=form.getTargetCandidateRank1()%>")
                input.selected = true;
                obj1.add(input);
            }
        <%}%>
        <%if(form.getTargetCandidateRank2() != null && i104Bean.getComboxDatas().size() > 1){%>
            var blank2 = document.createElement("OPTION");
            blank2.text = "";
            blank2.value = "11";
            obj2.add(blank2);
            for(var i=0; i<COMBOX.length; i++){
                var input = document.createElement("OPTION");
                input.text = COMBOX[i][1];
                input.value = COMBOX[i][0];
                if(COMBOX[i][0] == "<%=form.getTargetCandidateRank2()%>")
                input.selected = true;
                obj2.add(input);
            }
        <%}%>
        <%if(form.getTargetCandidateRank3() != null && i104Bean.getComboxDatas().size() > 2){%>
            var blank3 = document.createElement("OPTION");
            blank3.text = "";
            blank3.value = "12";
            obj3.add(blank3);
            for(var i=0; i<COMBOX.length; i++){
                var input = document.createElement("OPTION");
                input.text = COMBOX[i][1];
                input.value = COMBOX[i][0];
                if(COMBOX[i][0] == "<%=form.getTargetCandidateRank3()%>")
                input.selected = true;
                obj3.add(input);
            }
        <%}%>
    }
    /**
     * 偏差値範囲
     */
    function loadDevBox(lowerObj, upperObj){
        for(var i=0; i<LOWER.length; i++){
            var input = document.createElement("OPTION");
            input.text = LOWER[i];
            input.value = LOWER[i];
            if(LOWER[i] == "<c:out value="${form.lowerRange}"/>")
            input.selected = true;
            lowerObj.add(input);
        }
        for(var i=0; i<UPPER.length; i++){
            var input = document.createElement("OPTION");
            input.text = UPPER[i];
            input.value = UPPER[i];
            if(UPPER[i] == "<c:out value="${form.upperRange}"/>")
            input.selected = true;
            upperObj.add(input);
        }
    }
    /**
     * 平均・最高
     */
    function loadAvgChk(formObj){
        for(var i=0; i<formObj.length; i++){
            if(formObj[i].value == <%if(form.getAvgChoice().equals("1")) out.println("\"1\""); else if (form.getAvgChoice().equals("2")) out.println("\"2\""); else out.println("\"3\"");%>)
            formObj[i].checked = true;
        }
    }
    /**
     * 第1解答科目
     */
    function loadAns1st(formObj){
        <%if (!i104Bean.isAns1stExam()) {%>
            for(var i=0; i<formObj.length; i++){
                if(formObj[i].value == "3") {
                    formObj[i].disabled = true;
                }
            }
        <%}%>
    }
    /**
     * 初期化
     */
    function init(){
        startTimer();
        <%--スクロール位置 0426 K.Kondo--%>
        window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

        //1.選択中の生徒まで回す
        findStudent();
        //2.選択中の生徒が一人ならArrowボタンを無効化する
        if(getStudentNum() == 1) disableArrows();

        loadCombox(document.forms[0].targetCandidateRank1.options, document.forms[0].targetCandidateRank2.options, document.forms[0].targetCandidateRank3.options);
        loadDevBox(document.forms[0].lowerRange.options, document.forms[0].upperRange.options);
        loadAvgChk(document.forms[0].avgChoice);
        loadAns1st(document.forms[0].avgChoice);

        //選択中生徒の学年によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
        gradeChange();
        listnone();

        //ロード中
        <%@ include file="/jsp/script/loading.jsp" %>

        <%-- アプレットを本来の位置へ移動する --%>
        $('#AppletLayer').children().prependTo('#AppletCell');
    }
//-->
</SCRIPT>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="AppletLayer" style="position:absolute;top:250px;left:450px;width:1px;height:1px;overflow:hidden;">
<!-- アプレット部：サーブレットよりVALUEを受けとるようにすること=========== -->
<%-- 2016/01/14 QQ)Hisakawa 大規模改修 DEL START --%>
<%--
<APPLET CODEBASE="./applets/" ARCHIVE="Applet.jar" CODE="BalanceChartApplet.class" id="rc" WIDTH="848" HEIGHT="320">
<PARAM NAME="dataSelect" VALUE="off">
<PARAM NAME="dispSelect" VALUE="on">
<PARAM NAME="dispZoneL" VALUE="<%=Float.valueOf(form.getLowerRange()).intValue()%>">
<PARAM NAME="dispZoneH" VALUE="<%=Float.valueOf(form.getUpperRange()).intValue()%>">
<PARAM NAME="subjectNum" VALUE="<%=i104Bean.getCourseDatas().size()%>">
<PARAM NAME="subjectDAT" VALUE="<%
                                int subCount = 0;
                                for (java.util.Iterator it=i104Bean.getCourseDatas().iterator(); it.hasNext();) {
                                    if(subCount != 0) out.print(",");
                                    out.print(((CourseData)it.next()).getCourseName());
                                    subCount ++;
                                }
                                %>">
<PARAM NAME="itemTITLE" VALUE="□対象生徒偏差値,■合格者平均偏差値">

<PARAM NAME="itemNUM" VALUE="<%=i104Bean.getExaminationDatas().size()%>">
<%
String moshiString = "";
String[] dataStrings = new String[i104Bean.getExaminationDatas().size()];
int moshiCount = 0;
String maxEngSubCd = "";
String maxMathSubCd = "";
String maxJapSubCd = "";
String maxSciSubCd = "";
String maxSocSubCd = "";

for (java.util.Iterator it=i104Bean.getExaminationDatas().iterator(); it.hasNext();) {
    ExaminationData data = (ExaminationData)it.next();

    String lowRange = form.getLowerRange();

    String upRange = "100.0";
    if(moshiCount != 0) {
        //大学の場合
        moshiString += ",";
        dataStrings[moshiCount] += ",";

        if(form.getAvgChoice().equals("1")) {
            //平均表示
            if(i104Bean.getCourseDatas().size() > 3) {
                    if(form.isMarkexamflg() && form.isDispflg()){
                        //平均を表示だけど、平均値が存在するのは理科・地歴のみ、他は全部MAXでマーク模試
                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
                    }else{
                        //平均を表示だけど、平均値が存在するのは理科・地歴のみ、他は全部MAXでマーク模試以外
                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
                    }
            } else {
                    if(form.isMarkexamflg() && form.isDispflg()){
                        dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
                    }else{
                        dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
                    }
            }
        } else if(form.getAvgChoice().equals("3")) {
            //第1解答科目
            if(i104Bean.getCourseDatas().size() > 3) {
                    if(form.isMarkexamflg() && form.isDispflg()){
                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
                        if (!maxSciSubCd.equals("-999")) {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+",";
                        } else {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+",";
                        }
                        if (!maxSocSubCd.equals("-999")) {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
                        } else {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
                        }
                    }else{
                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
                        if (!maxSciSubCd.equals("-999")) {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+",";
                        } else {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSciSingle(), lowRange, upRange)+",";
                        }
                        if (!maxSocSubCd.equals("-999")) {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
                        } else {
                            dataStrings[moshiCount] += GeneralUtil.toRangeLimit(data.getAvgSocSingle(), lowRange, upRange);
                        }
                    }
            } else {
                    if(form.isMarkexamflg() && form.isDispflg()){
                        dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
                    }else{
                        dataStrings[moshiCount] =	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange);
                    }
            }
        } else {
            //平均表示良いほう表示
            if(i104Bean.getCourseDatas().size() > 3) {
                    //良い方マーク模試
                    if(form.isMarkexamflg() && form.isDispflg()){

                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
                    }else{
                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxSciSubCd).getCDeviation(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxSocSubCd).getCDeviation(), lowRange, upRange);
                    }
            } else {
                    //良い方マーク模試以外
                    if(form.isMarkexamflg() && form.isDispflg()){
                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getAvgMathSingle(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
                    }else{
                        dataStrings[moshiCount] = 	GeneralUtil.toRangeLimit(data.getTargetSubEng(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxMathSubCd).getCDeviation(), lowRange, upRange)+","+
                                                    GeneralUtil.toRangeLimit(data.getSubRecordData(maxJapSubCd).getCDeviation(), lowRange, upRange)+",";
                    }
            }
        }
    } else {
        //模試の場合
        if(form.getAvgChoice().equals("1")) {
            //平均表示
            if(i104Bean.getCourseDatas().size() > 3) {
                    //平均を表示だけど、平均値が存在するのは理科・地歴のみ、他は全部MAX
                    if(form.isMarkexamflg() && form.isDispflg()){
                        dataStrings[moshiCount] =
                                                GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAvgSci(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAvgSoc(), lowRange, upRange);

                                                maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                                maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                                maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                    }else{
                        dataStrings[moshiCount] =
                                                GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAvgSci(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAvgSoc(), lowRange, upRange);

                                                maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                                maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                                maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                    }
            } else {

                if(form.isMarkexamflg() && form.isDispflg()){
                                dataStrings[moshiCount] =
                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                                }else{
                                dataStrings[moshiCount] =
                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                                }
            }
        } else if(form.getAvgChoice().equals("3")) {
            // 第1解答科目表示
            if(i104Bean.getCourseDatas().size() > 3) {
                    if(form.isMarkexamflg() && form.isDispflg()){
                        dataStrings[moshiCount] =
                                                GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAns1stSci(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAns1stSoc(), lowRange, upRange);

                                                maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                                maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                                maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                                                maxSciSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSci());
                                                maxSocSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSoc());
                    }else{
                        dataStrings[moshiCount] =
                                                GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAns1stSci(), lowRange, upRange)+","+
                                                GeneralUtil.toRangeLimit(data.getAns1stSoc(), lowRange, upRange);

                                                maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                                maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                                maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                                                maxSciSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSci());
                                                maxSocSubCd = GeneralUtil.to999(data.getAns1stSubjectCodeSoc());
                    }
            } else {

                if(form.isMarkexamflg() && form.isDispflg()){
                                dataStrings[moshiCount] =
                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                                }else{
                                dataStrings[moshiCount] =
                                        GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                        GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

                                        maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                        maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                        maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                                }
            }
        } else {
                //良い方表示
                if(i104Bean.getCourseDatas().size() > 3) {
                    dataStrings[moshiCount] =
                                            GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                            GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                            GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange)+","+
                                            GeneralUtil.toRangeLimit(data.getMaxSci(), lowRange, upRange)+","+
                                            GeneralUtil.toRangeLimit(data.getMaxSoc(), lowRange, upRange);

                                            maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                            maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                            maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                                            maxSciSubCd = GeneralUtil.to999(data.getMaxSciCode());
                                            maxSocSubCd = GeneralUtil.to999(data.getMaxSocCode());
                } else {
                    dataStrings[moshiCount] =
                                            GeneralUtil.toRangeLimit(data.getMaxEng(), lowRange, upRange)+","+
                                            GeneralUtil.toRangeLimit(data.getMaxMath(), lowRange, upRange)+","+
                                            GeneralUtil.toRangeLimit(data.getMaxJap(), lowRange, upRange);

                                            maxEngSubCd = GeneralUtil.to999(data.getMaxEngCode());
                                            maxMathSubCd = GeneralUtil.to999(data.getMaxMathCode());
                                            maxJapSubCd = GeneralUtil.to999(data.getMaxJapCode());
                }
            }
    }
    moshiString += data.getExamName();
    moshiCount ++;
}
%>
<PARAM NAME="itemDAT" VALUE="<%=moshiString%>">
<%for(int i=0; i<dataStrings.length; i++){%>

    <PARAM NAME="DAT<%=i+1%>" VALUE="<%=dataStrings[i]%>">

<%}%>
<PARAM NAME="colorDAT" VALUE="1,0,2,15,13,14">
</APPLET>
--%>
<%-- 2016/01/14 QQ)Hisakawa 大規模改修 DEL END   --%>
<!-- ======================================================================== -->
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I104Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="targetYear" value="<c:out value="${iCommonMap.targetExamYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${iCommonMap.targetExamCode}" />">
<input type="hidden" name="printStatus" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->


<!--成績分析-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--分析メニュー-->
<%@ include file="/jsp/individual/include/bunseki_menu.jsp" %>
<!--/分析メニュー-->

<!--概要-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="12" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="698">
<table border="0" cellpadding="0" cellspacing="0" width="698">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="682">

<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td colspan="5" width="682" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="682" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="682" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="666" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">バランスチャート（合格者平均）</b></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="682" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="682" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="150" valign="bottom">
<table border="0" cellpadding="0" cellspacing="0" width="150">
<tr>
<td align="center" height="40"><input type="button" class="text12" onclick="openSample('<c:out value="${param.forward}" />');" value="サンプルの表示"></td>
</tr>
<tr>
<td bgcolor="#8CA9BB" height="2"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
</table>
<!--/大タイトル-->

<!--模試の種類-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="340">
    <table border="0" cellpadding="3" cellspacing="3" width="340" height="169">
    <tr>
    <td bgcolor="#E1E6EB">
        <table border="0" cellpadding="3" cellspacing="0">
        <tr>
        <td><span class="text12">対象年度：</span></td>
        <td><%@ include file="/jsp/individual/include/targetExamYear.jsp" %></td>
        </tr>
        <tr>
        <td><span class="text12">対象模試：</span></td>
        <td><%@ include file="/jsp/individual/include/targetExamCode.jsp" %></td>
        </tr>
        </table>
    </td>
    </tr>
    </table>
    <div style="margin:0px 0px 3px 3px;">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="12"><a href="#here" onclick="submitForm('i105');"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→"></a></td>
    <td><span class="text12"><a href="#here" onclick="submitForm('i105');">前回模試グラフを表示（別ウィンドウ）</a></span></td>
    </tr>
    </table>
    </div>
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="502">
    <table border="0" cellpadding="3" cellspacing="3" width="502" height="186">
    <tr>
    <td bgcolor="#F4E5D6">
        <table border="0" cellpadding="0" cellspacing="0" width="410">
        <tr>
        <td>
            <div><b class="text12">&lt;&lt;オプション&gt;&gt;</b></div>
            <!--偏差値範囲-->
            <table border="0" cellpadding="3" cellspacing="0">
            <tr>
            <td width="96"><span class="text12">偏差値範囲：</span></td>
            <td><SELECT SIZE="1" NAME="lowerRange"></SELECT></td>
            <td><span class="text12">〜</span></td>
            <td><SELECT SIZE="1" NAME="upperRange"></SELECT></td>
            </tr>
            </table>
            <!--/偏差値範囲-->
            <!--1教科複数受験-->
            <table border="0" cellpadding="3" cellspacing="0">
            <tr>
            <td width="96"><span class="text12">1教科複数受験：</span></td>
            <td><INPUT TYPE="RADIO" NAME="avgChoice" VALUE="3"</td>
            <td><span class="text12">第1解答科目<BR>を表示</span></td>
            <td><INPUT TYPE="RADIO" NAME="avgChoice" VALUE="1"</td>
            <td><span class="text12">平均を表示</span></td>
            <td><INPUT TYPE="RADIO" NAME="avgChoice" VALUE="2"</td>
            <td><span class="text12">良い方を表示</span></td>
            </tr>
            </table>
            <!--/1教科複数受験-->
            <!--志望大学-->
            <table border="0" cellpadding="3" cellspacing="0">
            <tr>
            <td rowspan="3" valign="top" width="96"><span class="text12">志望大学：</span></td>
            <td><SELECT SIZE="1" NAME="targetCandidateRank1" style="width:290px;"></SELECT></td>
            </tr>
            <tr>
            <td colspan="2"><SELECT SIZE="1" NAME="targetCandidateRank2" style="width:290px;"></SELECT></td>
            </tr>
            <tr>
            <td colspan="2"><SELECT SIZE="1" NAME="targetCandidateRank3" style="width:290px;"></SELECT></td>
            </tr>
            </table>
            <!--/志望大学-->
            <div style="margin-top:3px;"><span class="text10">※これらのオプション項目を変更した場合は、［再表示］ボタンをクリックしてください。</span></div>
        </td>
        </tr>
        </table>
    </td>
    <td width="71" bgcolor="#F4E5D6" align="center"><input type="button" value="再表示" class="text12" style="width:60px;" onclick="submitRefresh();"></td>
    </tr>
    </table>
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="5" width="848" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/模試の種類-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--グラフイメージ-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<%-- 2016/01/14 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td width="848" id="AppletCell"> --%>
<td width="848">
<%-- 2016/02/29 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<img src="./GraphServlet" width="848" height="320" border="0">
  --%>
<img src="<c:url value="./GraphServlet" />"  width="848" height="320" border="0">
<%-- 2016/02/29 QQ)Nishiyama 大規模改修 UPD END   --%>
<%-- 2016/01/14 QQ)Hisakawa 大規模改修 UPD END   --%>
<br></td>
</tr>
</table>
<!--/グラフイメージ-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--リスト-->
<div id='list'>
<table border="0" cellpadding="2" cellspacing="2" width="852">
<!--項目-->
<tr height="22" bgcolor="#CDD7DD" align="center">
<td colspan="2" width="29%"><span class="text12">&nbsp;</span></td>
    <c:forEach items="${i104Bean.courseDatas}" var="courseData" varStatus="status">
        <td width="13%"><span class="text12"><c:out value="${courseData.courseName}" escapeXml="false"/></span></td>
    </c:forEach>
</tr>
<!--/項目-->
<!--/1段目-->
    <c:forEach items="${i104Bean.examinationDatas}" var="examinationData" varStatus="status2" end="4">
        <tr height="36">
        <c:if test="${status2.index == 0}">
            <td bgcolor="#E1E6EB" colspan="2"><span class="text12"><c:out value="${examinationData.examName}" />（偏差値/学力レベル）</span></td>
        </c:if>
        <c:if test="${status2.index == 1}">
            <td bgcolor="#CDD7DD" rowspan="<c:out value="${i104Bean.examinationDataSize -1}" />" width="6%" align="center">
            <span class="text12">合格者<br>平均<br>偏差値</span></td>
        </c:if>
        <c:if test="${status2.index >= 1}">
            <td bgcolor="#E1E6EB"><span class="text12"><c:out value="${examinationData.examName}" /></span></td>
        </c:if>
        <c:choose>
            <c:when test="${status2.index == 0}">
                    <c:choose>
                        <c:when test="${form.avgChoice eq '1'}">
                            <!--模試平均-->
                            <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.avgEng}" /><c:if test="${examinationData.avgEng ne ''}">/</c:if><c:out value="${examinationData.levelAvgEng}" /></span></td>
                            <!--マーク模試-->
                            <c:choose>
                            <c:when test="${form.markexamflg && form.dispflg}">
                                <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.avgMath}" /><c:if test="${examinationData.avgMath ne ''}">/</c:if><c:out value="${examinationData.levelAvgMath}" /></span></td>
                            </c:when>
                            <c:otherwise>
                                <!--マーク模試以外-->
                                <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubMath}" /><c:if test="${examinationData.targetSubMath ne ''}">/</c:if><c:out value="${examinationData.levelTargetMath}" /></span></td>
                            </c:otherwise>
                            </c:choose>
                            <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubJap}" /><c:if test="${examinationData.targetSubJap ne ''}">/</c:if><c:out value="${examinationData.levelTargetJap}" /></span></td>

                            <%if(i104Bean.getCourseDatas().size() > 3){%>
                                <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.avgSci}" /><c:if test="${examinationData.avgSci ne ''}">/</c:if><c:out value="${examinationData.levelAvgSci}" /></span></td>
                                <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.avgSoc}" /><c:if test="${examinationData.avgSoc ne ''}">/</c:if><c:out value="${examinationData.levelAvgSoc}" /></span></td>
                            <%}%>

                        </c:when>
                        <c:when test="${form.avgChoice eq '3'}">
                            <!--第1解答課目-->
                            <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.avgEng}" /><c:if test="${examinationData.avgEng ne ''}">/</c:if><c:out value="${examinationData.levelAvgEng}" /></span></td>
                            <!--マーク模試-->
                            <c:choose>
                            <c:when test="${form.markexamflg && form.dispflg}">
                                <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.avgMath}" /><c:if test="${examinationData.avgMath ne ''}">/</c:if><c:out value="${examinationData.levelAvgMath}" /></span></td>
                            </c:when>
                            <c:otherwise>
                                <!--マーク模試以外-->
                                <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubMath}" /><c:if test="${examinationData.targetSubMath ne ''}">/</c:if><c:out value="${examinationData.levelTargetMath}" /></span></td>
                            </c:otherwise>
                            </c:choose>
                            <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubJap}" /><c:if test="${examinationData.targetSubJap ne ''}">/</c:if><c:out value="${examinationData.levelTargetJap}" /></span></td>

                            <%if(i104Bean.getCourseDatas().size() > 3){%>
                                <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.ans1stSci}" /><c:if test="${examinationData.ans1stSci ne ''}">/</c:if><c:out value="${examinationData.levelAns1stSci}" /></span></td>
                                <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.ans1stSoc}" /><c:if test="${examinationData.ans1stSoc ne ''}">/</c:if><c:out value="${examinationData.levelAns1stSoc}" /></span></td>
                            <%}%>

                        </c:when>
                        <c:otherwise>
                            <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.avgEng}" /><c:if test="${examinationData.avgEng ne ''}">/</c:if><c:out value="${examinationData.levelAvgEng}" /></span></td>

                            <!---->
                            <c:choose>
                            <c:when test="${form.markexamflg && form.dispflg}">
                            <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.avgMath}" /><c:if test="${examinationData.avgMath ne ''}">/</c:if><c:out value="${examinationData.levelAvgMath}" /></span></td>
                            </c:when>
                            <c:otherwise>
                            <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubMath}" /><c:if test="${examinationData.targetSubMath ne ''}">/</c:if><c:out value="${examinationData.levelTargetMath}" /></span></td>
                            </c:otherwise>
                            </c:choose>
                            <!--/-->
                            <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubJap}" /><c:if test="${examinationData.targetSubJap ne ''}">/</c:if><c:out value="${examinationData.levelTargetJap}" /></span></td>

                            <%if(i104Bean.getCourseDatas().size() > 3){%>
                                <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubSci}" /><c:if test="${examinationData.targetSubSci ne ''}">/</c:if><c:out value="${examinationData.levelTargetSci}" /></span></td>
                                <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubSoc}" /><c:if test="${examinationData.targetSubSoc ne ''}">/</c:if><c:out value="${examinationData.levelTargetSoc}" /></span></td>
                            <%}%>
                        </c:otherwise>
                    </c:choose>
            </c:when>
            <c:otherwise>
                <c:choose>
                    <c:when test="${form.avgChoice eq '1'}">

                        <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubEng}" /></span></td>
                        <!---->
                        <c:choose>
                        <c:when test="${form.markexamflg && form.dispflg}">
                        <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.avgMathSingle}" /></span></td>
                        </c:when>
                        <c:otherwise>
                        <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubMath}" /></span></td>
                        </c:otherwise>
                        </c:choose>
                        <!--/-->
                        <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubJap}" /></span></td>
                        <%if(i104Bean.getCourseDatas().size() > 3){%>
                            <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.avgSciSingle}" /></span></td>
                            <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.avgSocSingle}" /></span></td>
                        <%}%>
                    </c:when>
                    <c:when test="${form.avgChoice eq '3'}">
                        <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubEng}" /></span></td>
                        <!---->
                        <c:choose>
                        <c:when test="${form.markexamflg && form.dispflg}">
                        <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.avgMathSingle}" /></span></td>
                        </c:when>
                        <c:otherwise>
                        <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubMath}" /></span></td>
                        </c:otherwise>
                        </c:choose>
                        <!--/-->
                        <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubJap}" /></span></td>
                        <%if(i104Bean.getCourseDatas().size() > 3){%>
                            <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetAns1stSubSci}" /></span></td>
                            <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetAns1stSubSoc}" /></span></td>
                        <%}%>
                    </c:when>
                    <c:otherwise>
                        <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubEng}" /></span></td>
                        <!---->
                        <c:choose>
                        <c:when test="${form.markexamflg && form.dispflg}">
                        <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.avgMathSingle}" /></span></td>
                        </c:when>
                        <c:otherwise>
                        <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubMath}" /></span></td>
                        </c:otherwise>
                        </c:choose>
                        <!--/-->
                        <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubJap}" /></span></td>
                        <%if(i104Bean.getCourseDatas().size() > 3){%>
                            <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubSci}" /></span></td>
                            <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${examinationData.targetSubSoc}" /></span></td>
                        <%}%>
                    </c:otherwise>
                </c:choose>
            </c:otherwise>
        </c:choose>
        </tr>
    </c:forEach>

</table>
</div>
<!--/リスト-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/成績分析-->

<!--セキュリティスタンプの選択-->
<%@ include file="/jsp/individual/include/security_stamp.jsp" %>
<!--/セキュリティスタンプの選択-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--印刷対象生徒-->
<%@ include file="/jsp/individual/include/printStudent.jsp" %>
<!--/印刷対象生徒-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--ケイコさん-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="784">
<tr valign="top">
<td width="72" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="702">

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="678" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="678" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="678" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="678" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr>
<td width="12" background="./shared_lib/img/parts/wb_arrow_l_long.gif"><img src="./shared_lib/img/parts/sp.gif" width="12" height="5" border="0" alt=""><br></td>
<td width="688" bgcolor="#F9EEE5">
<!--アドバイス-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="688">
<tr valign="top">
<td width="131" align="center"><img src="./shared_lib/img/parts/word_one_point.gif" width="86" height="32" border="0" alt="ワンポイントアドバイス！！" vspace="5"><br></td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="5" border="0" alt=""><br></td>
<td width="511">


<div style="margin-top:5px;margin-bottom:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="511">
<%@ include file="/jsp/shared_lib/I_Onepoint.jsp" %>
</table>
</div>


</td>
<td width="25"><img src="./shared_lib/img/parts/sp.gif" width="25" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/アドバイス-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="678" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="678" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="678" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="678" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>

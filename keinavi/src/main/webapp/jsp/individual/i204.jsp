<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<jsp:useBean id="useListBean" scope="session" class="jp.co.fj.keinavi.beans.individual.judgement.JudgementListBean" />
<%@ page import="jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean" %>
<c:set value="individual" var="category" />
<html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>
	<%@ include file="/jsp/script/open_sample.jsp" %>
    // ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/printDialog_close.jsp" %>
	 var printDialogPattern = "";
	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

	/**
	 * サブミッション
	 */
	function submitForm(forward) {
		<%--スクロール位置 0426 K.Kondo--%>
		if(forward=='<c:out value="${param.forward}" />') {
			document.forms[0].scrollX.value = document.body.scrollLeft;
			document.forms[0].scrollY.value = document.body.scrollTop;
		} else {
			document.forms[0].scrollX.value = 0;
			document.forms[0].scrollY.value = 0;
		}
		document.getElementById("MainLayer").style.visibility = "hidden";
		document.getElementById("MainLayer").style.top = -2000;
		document.getElementById("LoadingLayer").style.visibility = "visible";
		document.forms[0].target = "_self";
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 詳細表示サブミッション
	 */
	function submitDetail() {
		var form = document.forms[0];
		// 出力先ウィンドウを開く
		op = window.open("", "i205", "width=930,height=600,resizable=yes,scrollbars=yes,status=yes,titlebar=yes");
		op.moveTo(50, 50);
		form.forward.value = "i205";
		form.backward.value = "<c:out value="${param.forward}" />";
		form.target = "i205";
		form.submit();
	}
	/**
	 * 戻るボタン
	 */
	function submitBack() {
		document.forms[0].target = "_self";
		<c:choose>
			<c:when test="${form.button == 'byUniv'}">document.forms[0].forward.value = "i203";</c:when>
			<c:when test="${form.button == 'byCond'}">document.forms[0].forward.value = "i202";</c:when>
			<c:otherwise>document.forms[0].forward.value = "i201";</c:otherwise>
		</c:choose>
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * アクション
	 */
	function setAction(button, forward){
    	document.forms[0].actionValue.value = button;
    	submitForm(forward);
	}
	/**
	 * 詳細表示用キー設定
	 */
	function setUniqueKey(uniqueKey){
		document.forms[0].uniqueKey.value = uniqueKey;
	}
	/**
	 * 編集・複製・削除用キー設定
	 */
	function setUnivValue4Detail(uniqueKey){
		document.forms[0].univValue4Detail.value = uniqueKey;
	}
	/**
	 * 生徒切り替え
	 */
	<%@ include file="/jsp/individual/include/student_rotator.jsp" %>
	/**
	 * 印刷ダイアログ
	 */
	<%@ include file="/jsp/individual/include/printDialog.jsp" %>
	/**
	 * 印刷チェック
	 */
	function printCheck() {
		printDialog();
	}
	// 表示
	function printView() {
		document.forms[0].printStatus.value = "1";
		printSheet();
	}
	/**
	 * 初期化
	 */
	function init(){
		startTimer();
		<%--スクロール位置 0426 K.Kondo--%>
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
		//1.選択中の生徒まで回す
		findStudent();
		//2.選択中の生徒が一人ならArrowボタンを無効化する
		if(getStudentNum() == 1) disableArrows();

<%--選択中の生徒の学年の取り方を変更 0407 K.Kondo delete
		//選択中の生徒の学年によって表示を変える(個人手帳リンク出す・出さない)
		if(personString[0][1] == 1 || personString[0][1] == 2){
			document.getElementById("note").style.display = "none";
		}else{
			document.getElementById("note").style.display = "inline";
		}
--%>
		if(dispGrade() == 1 || dispGrade() == 2){
			document.getElementById("note").style.display = "none";
		}else{
			document.getElementById("note").style.display = "inline";
		}

		//選択中生徒の学年によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
		gradeChange();

		//ロード中
		<%@ include file="/jsp/script/loading.jsp" %>
	}
//-->
</script>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I204Servlet" />" method="post">
<%-- 共通 --%>
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">
<%-- 対象生徒 --%>
<input type="hidden" name="targetPersonId" value="">
<%-- アクション 必要？ --%>
<input type="hidden" name="button" value="<c:out value="${form.button}"/>">
<%-- 詳細画面遷移 --%>
<input type="hidden" name="uniqueKey" value="">
<%-- 帳票関連 --%>
<input type="hidden" name="printStatus" value="">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="targetYear" value="<c:out value="${iCommonMap.targetExamYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${iCommonMap.targetExamCode}" />">

<%-- なんの為だったっけ？ --%>
<c:forEach var="univValue" items="${form.univValue}">
	<input type="hidden" name="univValue" value="<c:out value="${univValue}"/>">
</c:forEach>
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">


<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->


<!--志望大学判定-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--概要-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr valign="top">
<td width="183">
<!--●●●左側●●●-->

<!--志望大学判定トップへ-->
<table border="0" cellpadding="0" cellspacing="0" width="183">
<tr valign="top">
<td colspan="2" width="171" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="171" height="1" border="0" alt=""><br></td>
<td rowspan="2" width="12" background="./individual/img/tbl_cnr_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="170" bgcolor="#4694AF">

<table border="0" cellpadding="0" cellspacing="0" width="170">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="23" border="0" alt=""><br></td>
<td width="13"><a href="#"><img src="./shared_lib/img/parts/arrow_left_white.gif" width="8" height="9" border="0" alt=""></a></td>
<td width="151">
<span class="text12"><a href="javascript:submitForm('i201')" style="color:#FFFFFF;">志望大学判定トップ</a></span></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/志望大学判定トップへ-->

<!--STEP-->
<table border="0" cellpadding="0" cellspacing="0" width="183">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#4694AF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="180" bgcolor="#F3F5F5" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--step1-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step1_def.gif" width="46" height="12" border="0" alt="STEP1"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="94" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">条件設定</b></td>
</tr>
</table>
<!--/step1-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr>
<td width="162" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step2-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr height="22" bgcolor="#FF8F0E">
<td width="55" align="right"><img src="./shared_lib/img/step/step2_cur.gif" width="46" height="12" border="0" alt="STEP2"><br></td>
<td width="13"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="94"><b class="text12" style="color:#FFFFFF;">判　定</b></td>
</tr>
</table>
<!--/step2-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="4" width="183" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="183" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/STEP-->
<!--/●●●左側●●●-->
</td>
<td width="17"><img src="./shared_lib/img/parts/sp.gif" width="17" height="1" border="0" alt=""><br></td>
<td width="678">
<!--●●●右側●●●-->
<table border="0" cellpadding="0" cellspacing="0" width="678">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="678">
<tr>
<td width="676" bgcolor="#FFFFFF"><span class="text12">&nbsp;<b>対象模試：</b><c:out value="${sessionScope['iCommonMap'].targetExamName}"/></span></td>
</tr>
<tr>
<td width="676" bgcolor="#FFFFFF">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="662">
<tr>
<td width="22"><img src="./shared_lib/img/parts/sp.gif" width="22" height="1" border="0" alt=""><br></td>
<td width="634">

<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="634">
<tr valign="top">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="630">

<table border="0" cellpadding="0" cellspacing="0" width="630">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="621">

<table border="0" cellpadding="0" cellspacing="0" width="621">
<tr valign="top">
<td colspan="2" width="621" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="621" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="621" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="621" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="618" bgcolor="#758A98">
<table border="0" cellpadding="0" cellspacing="0" width="618">
<tr>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""></td>
<td width="608"><b class="text14" style="color:#FFFFFF;">
結果一覧（<c:choose>
<c:when test="${sessionScope['judgedBy'] eq 'byCond'}">こだわり判定(条件指定)</c:when>
<c:when test="${sessionScope['judgedBy'] eq 'byUniv'}">こだわり判定(大学指定)</c:when>
<c:when test="${sessionScope['judgedBy'] eq 'byAuto'}">オマカセ判定</c:when>
<c:otherwise><font color="red"><b>エラーモード</b></font></c:otherwise>
</c:choose>）</b></td>
</tr>
</table>
</td>
</tr>
<tr valign="top">
<td colspan="2" width="621" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="621" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="630" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="630" height="1" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<%if(useListBean.isExamTaken() && !useListBean.isJudged()){%>
	<table border="0" cellpadding="2" cellspacing="2" width="634">
		<tr>
			<td align="center"><span class="text14">この生徒では判定を行っていません。</span></td></tr>
	</table>
<%}else if(!useListBean.isExamTaken() && !useListBean.isJudged()){%>
	<table border="0" cellpadding="2" cellspacing="2" width="634">
		<tr>
			<td align="center"><span class="text14">この生徒は対象模試を受験していないので、判定できません。</span></td></tr>
	</table>
<%}else if(useListBean.getRecordSet().size() == 0){%>
	<table border="0" cellpadding="2" cellspacing="2" width="634">
		<tr>
			<td align="center"><span class="text14">条件に一致する大学はありませんでした。</span></td></tr>
	</table>
<%}else{%>
	<!--リスト-->
	<table border="0" cellpadding="2" cellspacing="2" width="634">
	<!--項目-->
	<tr bgcolor="#CDD7DD" align="center">
	<td rowspan="2" width="7%"><span class="text12">&nbsp;</span></td>
	<td rowspan="2" width="16%"><span class="text12">大学</span></td>
	<td rowspan="2" width="13%"><span class="text12">学部</span></td>
	<td rowspan="2" width="13%"><span class="text12">学科</span></td>
	<td rowspan="2" width="8%"><span class="text12">所在地</span></td>
	<td rowspan="2" width="8%"><span class="text12">センター</span></td>
	<td rowspan="2" width="8%"><span class="text12">2次</span></td>
	<td colspan="2" width="16%"><span class="text12">総合</span></td>
	<td rowspan="2" width="11%"><span class="text12">配点比率</span></td>
	</tr>
	<tr bgcolor="#CDD7DD" align="center">
	<td width="8%"><span class="text12">ポイント</span></td>
	<td width="8%"><span class="text12">評価</span></td>
	</tr>
	<!--/項目-->
<%for(int i=0;i<useListBean.getRecordSet().size();i++){%>
	<!--set-->
	<tr>
	<td bgcolor="#E1E6EB" align="center"><span class="text12"><a href="javascript:setUniqueKey('<%=(((DetailPageBean)useListBean.getRecordSet().get(i)).getUniqueKey())%>');submitDetail()">詳細</a></span></td>
	<td bgcolor="#F4E5D6">
	<table border="0" cellpadding="4" cellspacing="0">
	<tr>
	<td><span class="text12"><%=(((DetailPageBean)useListBean.getRecordSet().get(i)).getUnivName())%></span></td>
	</tr>
	</table>
	</td>
	<td bgcolor="#F4E5D6">
	<table border="0" cellpadding="4" cellspacing="0">
	<tr>
	<td><span class="text12"><%=(((DetailPageBean)useListBean.getRecordSet().get(i)).getDeptName())%></span></td>
	</tr>
	</table>
	</td>
	<td bgcolor="#F4E5D6">
	<table border="0" cellpadding="4" cellspacing="0">
	<tr>
	<td><span class="text12"><%=(((DetailPageBean)useListBean.getRecordSet().get(i)).getSubName())%></span></td>
	</tr>
	</table>
	</td>
	<td bgcolor="#F4E5D6" align="center"><span class="text12"><%=(((DetailPageBean)useListBean.getRecordSet().get(i)).getPrefName())%></span></td>
	<td bgcolor="#F4E5D6" align="center"><span class="text12">
	<%=(((DetailPageBean)useListBean.getRecordSet().get(i)).getCLetterJudgement())%>
	</span></td>
	<td bgcolor="#F4E5D6" align="center"><span class="text12">
	<%=(((DetailPageBean)useListBean.getRecordSet().get(i)).getSLetterJudgement())%>
	</span></td>
	<td bgcolor="#F4E5D6" align="center"><span class="text12">
	<%=(((DetailPageBean)useListBean.getRecordSet().get(i)).getT_scoreStr())%>
	</span></td>
	<td bgcolor="#F4E5D6" align="center"><span class="text12">
	<%=(((DetailPageBean)useListBean.getRecordSet().get(i)).getTLetterJudgement())%>
	</span></td>
	<td bgcolor="#F4E5D6" align="center">
	<table border="0" cellpadding="4" cellspacing="0" width="50">
	<tr>
	<td><span class="text12"><%=(((DetailPageBean)useListBean.getRecordSet().get(i)).getCAllotPntRateAll())%>:<%=(((DetailPageBean)useListBean.getRecordSet().get(i)).getSAllotPntRateAll())%></span></td>
	</tr>
	</table>
	</td>
	</tr>
	<!--/set-->
<%}%>
	</table>
	<!--/リスト-->
<%}%>


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/●●●右側●●●-->
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/志望大学判定-->

<!--セキュリティスタンプの選択-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#EFF2F3" align="center">

<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td width="42"><img src="./shared_lib/img/parts/icon_secu.gif" width="30" height="30" border="0" alt="セキュリティスタンプの選択" hspace="3"></td>
<td width="836"><b class="text14" style="color:#657681;">セキュリティスタンプの選択</b></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="878">
<tr valign="top">
<td width="876" bgcolor="#FFFFFF">

<div style="margin-top:8px;">
<table border="0" cellpadding="0" cellspacing="0" width="876">
<tr>
<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="834">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="22"><input type="radio" name="printStamp" value="1" <c:if test="${ form.printStamp == '1'}">checked</c:if>></td>
<td width="88"><span class="text12">機密</span></td>
<td width="22"><input type="radio" name="printStamp" value="5" <c:if test="${ form.printStamp == '5'}">checked</c:if>></td>
<td width="88"><span class="text12">なし</span></td>
</tr>
</table>

</td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/セキュリティスタンプの選択-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->


<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="892">
<tr>
<td width="523" align="right">
<c:choose>
<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
	<select name="judgeStudent" class="text12" style="width:156;">
	<option value="1" <c:if test="${form.judgeStudent == '1'}">selected</c:if>>選択中の生徒すべて</option>
	<option value="2" <c:if test="${form.judgeStudent == '2'}">selected</c:if>>表示中の生徒一人</option>>
</select>
</c:when>
<c:otherwise>
	<!--何もなし-->
</c:otherwise>
</c:choose>

</td>
<td width="369" align="right"><span class="text12"><a href="javascript:openSample('<c:out value="${param.forward}" />')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="4" align="absmiddle">サンプルの表示</a></span></td>
</tr>
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="6" cellspacing="1" width="892" bgcolor="#8CA9BB">
<tr>
<td bgcolor="#FFFFFF" align="center">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><a href="#here" onclick="printView();"><img src="./shared_lib/img/btn/hyouji_w.gif" width="114" height="35" border="0" alt="表示"></a></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><a href="#here" onclick="printCheck();"><img src="./shared_lib/img/btn/print_w.gif" width="114" height="35" border="0" alt="印刷"></a><br></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--ケイコさん-->
<div id="note">
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="145"><img src="./shared_lib/img/parts/sp.gif" width="145" height="1" border="0" alt=""><br></td>
<td width="524">

<table border="0" cellpadding="0" cellspacing="0" width="524">
<tr valign="top">
<td width="72" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="442">

<table border="0" cellpadding="0" cellspacing="0" width="442">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="418" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="418" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="418" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="418" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="442">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="15" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="15" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="45" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="428" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="428">
<tr>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="498" align="center">
<b class="text14">判定結果が個人手帳の判定履歴に登録されました！</b>
<div style="margin-top:7px;"><a href="javascript:submitForm('i301')"><img src="./shared_lib/img/btn/techou.gif" width="120" height="38" border="0" alt="個人手帳"></a></div></td>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="442">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="418" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="418" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="418" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="418" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>

</td>
<td width="239"><img src="./shared_lib/img/parts/sp.gif" width="239" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/ケイコさん-->
</div>

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>

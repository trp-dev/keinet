<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/DateSelector.js"></script>
<script type="text/javascript" src="./js/JString.js"></script>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	/**
	 * サブミッション
	 */
	function submitForm(forward) {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 戻るボタン
	 */		
	function submitBack() {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "<c:out value="${param.backward}" />";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 登録して閉じるサブミッション
	 */
	function submitUpdate(){
		submitForm('i502');
	}
	/**
	 * キャンセルサブミッション
	 */
	function submitCancel(){
		window.close();
	}
	/**
	 * 文字長チェック
	 */
	function lengthCheak(){
		
		var message = "";
	
		var js = new JString();
		// 入力チェック（プロファイル名の文字数）
		js.setString(document.forms[0].title.value);
		if (js.getLength() > 30) {
			message += "テキストタイトルは全角15文字までです。\n";
		}

		// 入力チェック（コメントの文字数）
		js.setString(document.forms[0].text.value);
		if (js.getLength() > 400) {
			message += "テキストは全角200文字までです。\n";
		}

		if (message != "") {
			alert(message);
			return false;
		}else{
			submitUpdate();
		}
	}
	/**
	 * 初期化
	 */
	function init() {
		startTimer();
		WYEAR = <c:out value="${form.year}" />;
		WMON  = <c:out value="${form.month}" />;
		WDAY  = <c:out value="${form.date}" />;
		DayList('DD',WYEAR,WMON,WDAY);
		MonList('MM',WMON);
		YearList('YY',WYEAR,3);
		
		//ロード中
		<%@ include file="/jsp/script/loading.jsp" %>
	}
//-->
</script>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I502Servlet" />" method="POST" onSubmit="return false">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="button" value="update">
<input type="hidden" name="targetPersonId" value="<c:out value="${iCommonMap.targetPersonId}" />">
<input type="hidden" name="individualId" value="<c:out value="${i502Bean.i502Data.individualId}" />">
<input type="hidden" name="koIndividualId" value="<c:out value="${i502Bean.i502Data.koIndividualId}" />">
<input type="hidden" name="firstRegistrYdt" value="<c:out value="${i502Bean.i502Data.firstRegistrYdt}" />">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="200" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="554">

<table border="0" cellpadding="0" cellspacing="0" width="554">
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="538" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">面談メモ編集</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<div style="margin-top:5px;">
<table border="0" cellpadding="2" cellspacing="0" width="570">
<tr valign="top">
<td><span class="text14">下記の項目に記入の上、「登録」ボタンを押してください。</span></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="4" cellspacing="2" width="550">
<!--作成日-->
<tr>
<td width="100" bgcolor="#E1E6EB">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><b class="text12">作成日</b></td>
</tr>
</table>
</td>
<td width="450" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td><select ID="YY" onchange="ChgYear()" name="year"></select></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><select ID="MM" onchange="ChgMon()"  name="month"></select></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><select ID="DD" onchange="ChgDay()"  name="date"></select></td>
</tr>
</table>
</td>
</tr>
<!--/作成日-->
<!--タイトル-->
<tr>
<td width="100" bgcolor="#E1E6EB">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><b class="text12">タイトル</b></td>
</tr>
</table>
</td>
<td width="450" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td><input type="text" maxlength="30" size="20" class="text12" name="title" style="width:250px;" value="<c:out value="${i502Bean.i502Data.title}" />"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><span class="text12">※全角15文字まで</span></td>
</tr>
</table>
</td>
</tr>
<!--/タイトル-->
<!--テキスト-->
<tr valign="top">
<td width="100" bgcolor="#E1E6EB">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><b class="text12">テキスト</b></td>
</tr>
</table>
</td>
<td width="450" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td>

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><textarea rows="6" cols="40" name="text" style="width:420px;"><c:out value="${i502Bean.i502Data.text}" /></textarea></td>
</tr>
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td><span class="text12">※全角200文字まで</span></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
<!--/テキスト-->
</table>



<!--矢印-->
<table border="0" cellpadding="0" cellspacing="15">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="570">
<tr valign="top">
<td width="568" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="&nbsp;キャンセル&nbsp;" class="text12" style="width:120px;" onclick="submitCancel()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td><input type="button" value="&nbsp;登録して閉じる&nbsp;" class="text12" style="width:120px;" onclick="lengthCheak()"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td align="center"><span class="text12">※キャンセルを押すと、登録せずにウィンドウを閉じます。</span></td>
</tr>
</table>
</div>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->


<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw.jsp" %>
<!--/FOOTER-->
</form>
</div>

</body>
</html>

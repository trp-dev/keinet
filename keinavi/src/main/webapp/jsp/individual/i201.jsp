<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="form" scope="request" class="jp.co.fj.keinavi.forms.individual.I201Form" />
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>
    // ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	/**
	 * サブミット
	 */
	function submitForm(forward) {
		<%--スクロール位置 0426 K.Kondo--%>
		if(forward=='<c:out value="${param.forward}" />') {
			document.forms[0].scrollX.value = document.body.scrollLeft;
			document.forms[0].scrollY.value = document.body.scrollTop;
		} else {
			document.forms[0].scrollX.value = 0;
			document.forms[0].scrollY.value = 0;
		}
		document.getElementById("MainLayer").style.visibility = "hidden";
		document.getElementById("MainLayer").style.top = -2000;
		document.getElementById("LoadingLayer").style.visibility = "visible";
		document.forms[0].target = "_self";
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * サブミット
	 */
	function submitBack() {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "i003";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 大学指定判定サブミット
	 */
	function submitJudgeUniv(){
		setByUniv();
		submitForm('i203');
	}
	/**
	 * サブミット
	 */
	function setByUniv(){
		document.forms[0].button.value = "byUniv";
	}
	/**
	 * こだわり判定サブミット
	 */
	function submitJudgeCond(){
		setByCond();
		submitForm('i202');
	}
	/**
	 * サブミット
	 */
	function setByCond(){
		document.forms[0].button.value = "byCond";
	}
	/**
	 * オマカセ判定サブミット
	 */
	function submitJudgeAuto(){
		setByAuto();
		submitForm('i204');
	}
	/**
	 * サブミット
	 */
	function setByAuto(){
		document.forms[0].button.value = "byAuto";
	}
	/**
	 * 生徒切り替え
	 */
	<%@ include file="/jsp/individual/include/student_rotator.jsp" %>
	/**
	 * 判定できるかどうかチェック
	 */
	function isJudgable(){
		var flg = true;
		<c:choose>
			<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
				var choice = document.forms[0].elements["judgeStudent"].options[document.forms[0].elements["judgeStudent"].selectedIndex].value;
				if(choice != '1'){//表示中の生徒一人を選択している
					if(!didTakeExam()){
						flg = false;
					}
				}
			</c:when>
			<c:otherwise>
				if(!didTakeExam()){
					flg = false;
				}
			</c:otherwise>
		</c:choose>
		if(!flg){
			window.alert("この生徒は対象模試を受験していないので判定できません");
		}
		return flg;
	}
<%--student_rotatorに移動 0407 K.Kondo delete
	/**
	 * 表示中の生徒が対象模試を受験しているかどうか
	 */
	function didTakeExam(){
		var flg = true;
		for(var i=0; i<personString.length; i++){
			if(personString[i][0] == document.forms[0].targetPersonId.value){
				if(personString[i][4] == 'false'){
					flg = false;
				}
			}
		}
		return flg;
	}
--%>
	/**
	 * 初期化
	 */
	function init(){
		startTimer();
		<%--スクロール位置 0426 K.Kondo--%>
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

		//1.選択中の生徒まで回す
		findStudent();
		//2.選択中の生徒が一人ならArrowボタンを無効化する
		if(getStudentNum() == 1) disableArrows();

		//選択中生徒の学年によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
		gradeChange();

		//ロード中
		<%@ include file="/jsp/script/loading.jsp" %>
	}
//-->
</script>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I201Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="button" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->


<!--志望大学判定-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--概要-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF"><span class="text12"><b>対象模試：</b><c:out value="${sessionScope['iCommonMap'].targetExamName}"/></span></td>
</tr>
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--説明-->
<table border="0" cellpadding="0" cellspacing="0" width="862">
<tr valign="top">
<td width="24"><img src="./shared_lib/img/parts/sp.gif" width="24" height="1" border="0" alt=""><br></td>
<td width="838"><span class="text14-hh">下記のメニューから大学の検索と判定が行えます。<br>判定を参考に志望大学を決定し、受験予定大学を登録してください。　※全統マーク高２模試除く。</span></td>
</tr>
</table>
<!--/説明-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--判定-->
<table border="0" cellpadding="0" cellspacing="0" width="862">
<tr valign="top">
<td width="24"><img src="./shared_lib/img/parts/sp.gif" width="24" height="1" border="0" alt=""><br></td>
<td width="398">
<!--オマカセ判定-->
<table border="0" cellpadding="0" cellspacing="0" width="398">
<tr bgcolor="#758A98">
<td width="14"><img src="./shared_lib/img/parts/sp.gif" width="14" height="26" border="0" alt=""><br></td>
<td width="384"><b class="text16" style="color:#FFFFFF;">オマカセ判定</b></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="398">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="398">
<tr valign="top">
<td width="396" bgcolor="#FBD49F">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="376">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="376">
<tr valign="top">
<td width="374" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="354">
<tr valign="top">
<td width="73"><img src="./shared_lib/img/illust/man01.gif" width="63" height="99" border="0" alt="男性"><br></td>
<td width="281">
<!--ふきだし-->
<table border="0" cellpadding="0" cellspacing="0" width="281">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="257" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="257" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="257" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="257" height="5" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="281">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="4" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="4" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="4" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="4" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="267" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="267">
<tr>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="247"><b class="text12">オススメな大学を教えて！</b></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="281">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="257" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="257" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="257" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="257" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/ふきだし-->
<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="281">
<tr>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="276"><span class="text12-hh"><b>自動で検索して判定！</b><br>模試の志望大学の傾向から最適な志望大学を検索して自動で判定を行います。</span></td>
</tr>
</table>
</div>
<!--/説明-->
<!--判定フォーム-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="281">
<tr>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>

<td width="161">
<!-- モードの判別-->
<c:choose>
<c:when test="${sessionScope['iCommonMap'].bunsekiMode}">
<select name="judgeStudent" class="text12" style="width:158px;">
	<option value="1" <c:if test="${form.judgeStudent == '1'}">selected</c:if>>選択中の生徒すべて</option>
	<option value="2" <c:if test="${form.judgeStudent == '2'}">selected</c:if>>表示中の生徒一人</option>
</select>
</c:when>
<c:otherwise>
	<input type="hidden" name="judgeStudent" value="2">
</c:otherwise>
</c:choose>
<!--/ モードの判別-->
</td>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="100"><input type="button" value="判定する" onclick="isJudgable() ? submitJudgeAuto() : void(0)" class="text12" style="width:98px;"></td>
</tr>
</table>
</div>
<!--/判定フォーム-->
</td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="24" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


</td>
</tr>
</table>
</td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/オマカセ判定-->
</td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="398">
<!--こだわり判定-->
<table border="0" cellpadding="0" cellspacing="0" width="398">
<tr bgcolor="#758A98">
<td width="14"><img src="./shared_lib/img/parts/sp.gif" width="14" height="26" border="0" alt=""><br></td>
<td width="384"><b class="text16" style="color:#FFFFFF;">こだわり判定</b></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="398">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="398">
<tr valign="top">
<td width="396" bgcolor="#FBD49F">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="376">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="376">
<tr valign="top">
<td width="374" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="354">
<tr valign="top">
<td width="73"><img src="./shared_lib/img/illust/woman03.gif" width="63" height="103" border="0" alt="女性"><br></td>
<td width="281">
<!--ふきだし-->
<table border="0" cellpadding="0" cellspacing="0" width="281">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="257" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="257" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="257" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="257" height="5" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="281">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="4" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="4" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="4" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="4" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="267" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="267">
<tr>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="247"><b class="text12">条件は、アレとコレとソレと、、、、、</b></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="281">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="257" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="257" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="257" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="257" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/ふきだし-->
<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="281">
<tr valign="top">
<td width="132">
<!--左-->
<table border="0" cellpadding="0" cellspacing="0" width="132">
<tr>
<td width="132"><span class="text12-hh">
<b>条件指定</b><br>
個々の生徒の条件を詳細に指定して志望大学を検索し、判定！<br>
<font style="color:#FF6E0E">※検索条件は生徒個人ごとに保存されます。</font>
</span></td>
</tr>
</table>
<!--/左-->
</td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#2B2B2B"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="132">
<!--右-->
<table border="0" cellpadding="0" cellspacing="0" width="132">
<tr>
<td width="132"><span class="text12-hh">
<b>大学指定</b><br>
大学・学部・学科を個別に指定して判定！<br><br><br>
</span></td>
</tr>
</table>
<!--/右-->
</td>
</tr>
<tr valign="top">
<td width="132"><!--左ボタン--><input type="button" value="条件指定に進む" class="text12" style="width:132px;" onclick="submitJudgeCond()"></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#2B2B2B"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="132"><!--右ボタン--><input type="button" value="大学指定に進む" class="text12" style="width:132px;" onclick="submitJudgeUniv()"></td>
</tr>
</table>
</div>
<!--/説明-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/こだわり判定-->
</td>
<td width="31"><img src="./shared_lib/img/parts/sp.gif" width="31" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/判定-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/志望大学判定-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>

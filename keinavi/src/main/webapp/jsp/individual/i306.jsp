<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setBundle basename="jp.co.fj.keinavi.util.individual.iprops" var="bundle" />
<c:set var="DISP_DAY_LENGTH"><fmt:message key="DISP_DAY_LENGTH" bundle="${bundle}" /></c:set>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<%--
年度計算変更に伴いDatePickerを特殊なDatePickerRevに変更
--%>
<script type="text/javascript" src="./js/DatePickerRev.js"></script>
<SCRIPT type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>
	<%@ include file="/jsp/script/open_sample.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
	<%@ include file="/jsp/script/open_schedule_alert.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/printDialog_close.jsp" %>
	 var printDialogPattern = "";
	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

	/**
	 * サブミット
	 */
	function submitForm(forward) {
		<%--スクロール位置 0509 K.Kondo--%>
		document.forms[0].scrollX.value = document.body.scrollLeft;
		document.forms[0].scrollY.value = document.body.scrollTop;

		document.getElementById("MainLayer").style.visibility = "hidden";
		document.getElementById("MainLayer").style.top = -2000;
		document.getElementById("LoadingLayer").style.visibility = "visible";
		//年度をまたぐ可能性がある
		document.forms[0].year.value = WYEAR;
		document.forms[0].target = "_self";
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 戻るボタン
	 */
	function submitBack() {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "i301";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 再表示
	 */
	<%-- 2005.6.20 modify
	function submitRefresh(){ --%>
	function submitRefresh(refType){
		document.forms[0].refType.value = refType;
		submitForm('i306');
	}
	/**
	 * 生徒切り替え
	 */
	<%@ include file="/jsp/individual/include/student_rotator.jsp" %>
	/**
	 * 印刷ダイアログ
	 */
	<%@ include file="/jsp/individual/include/printDialog.jsp" %>
	/**
	 * 印刷チェック
	 */
	function printCheck() {
		if (!validate()) {
			window.alert("出力フォーマットを選択してください。");
		} else {
			printDialog();
		}
	}
	// 表示
	function printView() {
		if (!validate()) {
			window.alert("出力フォーマットを選択してください。");
		} else {
			document.forms[0].printStatus.value = "1";
			printSheet();
		}
	}
	// 入力チェック
	function validate() {
		for (i=0; i < document.forms[0].textFormat.length; i++) {
			if (document.forms[0].textFormat[i].checked) {
				return true;
			}
		}
		return false;
	}
	/**
	 * 初期化
	 */
	var ps;
	function init(){
		startTimer();
		<%--スクロール位置 0509 K.Kondo--%>
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
		//1.選択中の生徒まで回す
		findStudent();
		//2.選択中の生徒が一人ならArrowボタンを無効化する
		if(getStudentNum() == 1) disableArrows();
		//3.日付コンボロード
		ps = new DatePickerRev(<c:out value="${form.year}" />, <c:out value="${form.month}" />, <c:out value="${form.date}" />, 'MM', 'DD');
		//4.選択中生徒の学年によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
		gradeChange();

		//ロード中
		<%@ include file="/jsp/script/loading.jsp" %>
	}
//-->
</SCRIPT>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I306Servlet" />" method="POST">
<input type="hidden" name="forward">
<input type="hidden" name="backward">
<input type="hidden" name="year" value="">
<input type="hidden" name="printStatus" value="">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="targetYear" value="<c:out value="${iCommonMap.targetExamYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${iCommonMap.targetExamCode}" />">
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">

<%--スクロール位置 0509 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<%-- 2005.6.20 add start --%>
<input type="hidden" name="originalMonth" value="<c:out value="${form.month}" />">
<input type="hidden" name="originalDate" value="<c:out value="${form.date}" />">
<input type="hidden" name="refType" value="">
<%-- 2005.6.20 add end --%>

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->


<!--個人手帳-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--概要-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><span class="text12"><b>センター判定対象模試：</b><c:out value="${markExamName}" /></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td><span class="text12"><b>2次判定対象模試：</b><c:out value="${wrtnExamName}" /></span></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--メニュー切り替え-->
<%@ include file="/jsp/individual/include/menu_switch.jsp" %>
<!--/メニュー切り替え-->
<!--小タイトル-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="832">

<table border="0" cellpadding="0" cellspacing="0" width="832">
<tr valign="top">
<td colspan="4" width="832" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="832" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="832" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="816" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">スケジュール（カレンダー）</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="832" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="832" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/小タイトル-->
<!--説明-->
<div style="margin-top:12px;">
<table border="0" cellpadding="0" cellspacing="0" width="828">
<tr valign="top">
<td>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td align="right"><span class="text14">受験予定大学一覧で受験日が設定されている大学のスケジュールです。<br>※<a href="#here" onclick="openScheduleAlert(3);">入試日程に関するご注意</a></span></td>
	</tr>
	</table>
</td>
<td align="right"><%@ include file="/jsp/individual/include/text_out_format_sub.jsp" %></td>
</tr>
</table>
</div>
<!--/説明-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--アイコン説明-->
<table border="0" cellpadding="0" cellspacing="0" width="852">
<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
<td width="832" align="right">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="26"><img src="./individual/img/icon_sche01.gif" width="21" height="21" border="0" alt="出願締切日"><br></td>
<td><span class="text12">出願締切日</span></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="26"><img src="./individual/img/icon_sche02.gif" width="21" height="21" border="0" alt="受験日"><br></td>
<td><span class="text12">受験日</span></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="26"><img src="./individual/img/icon_sche03.gif" width="21" height="21" border="0" alt="合格発表日"><br></td>
<td><span class="text12">合格発表日</span></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="26"><img src="./individual/img/icon_sche04.gif" width="21" height="21" border="0" alt="入学手続き締切日"><br></td>
<td><span class="text12">入学手続き締切日</span></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/アイコン説明-->

<!--リスト-->
<table border="0" cellpadding="0" cellspacing="0" width="852">
<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
<td width="832">

<table border="0" cellpadding="4" cellspacing="2" width="832">
<tr bgcolor="#93A3AD">
<td colspan="<c:out value="${DISP_DAY_LENGTH + 4}" />">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
	<td width="100"><b class="text12" style="color:#FFFFFF;">表示開始日&nbsp;：</b></td>
	<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
	<td width="20"><select ID="MM" name="month" onchange="ps.ChgMon('MM', 'DD')"></select></td>
	<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
	<td width="20"><select ID="DD" name="date" onchange="ps.ChgDay('DD')"></select></td>
	<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
	<td width="100">
		<table border="0" cellpadding="2" cellspacing="0">
		<tr>
		<td bgcolor="#FFFFFF"><input type="button" value="更新" class="text12" style="width:80px;" onclick="submitRefresh('0')"></td>
		</tr>
		</table>
	</td>
	<%-- 2005.6.20 add start --%>
	<td align="right">
		<table border="0" cellpadding="2" cellspacing="0" align="right">
		<tr>
		<td bgcolor="#FFFFFF"><input type="button" name="leftDay" value=" < " onclick="submitRefresh('-1')" <c:if test="${form.min}">disabled</c:if>></td>
		<td><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""></td>
		<td bgcolor="#FFFFFF"><input type="button" name="rightDay" value=" > " onclick="submitRefresh('1')" <c:if test="${form.max}">disabled</c:if>></td>
		</tr>
		</table>
	</td>
	<%-- 2005.6.20 add end --%>
</tr>
</table>

</td>
</tr>
<!--項目-->
<tr bgcolor="#CDD7DD">
	<td rowspan="3" width="37%" align="center"><span class="text12">受験大学・学部・学科</span></td>
	<%--総合非表示の為colspanを2に変更<td colspan="3" width="9%"><span class="text12">合格可能性<br>判定</span></td>--%>
	<td colspan="2" width="9%"><span class="text12">合格可能性<br>判定</span></td>
		<c:forEach var="yearData" items="${i306Bean.yearMonthDateData.yearList}">
			<c:forEach var="monthData" items="${yearData.monthList}">
				<c:if test="${monthData.month != thisMonth}">
					<td colspan="<c:out value="${monthData.dateListSize}" />" align="center"><span class="text12"><c:out value="${monthData.month}" />月</span></td>
				</c:if>
				<c:set var="thisMonth" value="${monthData.month}" />
			</c:forEach>
		</c:forEach>
</tr>
<tr bgcolor="#E1E6EB">
	<td rowspan="2" width="3%" align="center" valign="top" nowrap><span class="text12" style="writing-mode:tb-rl;">センター</span></td>
	<td rowspan="2" width="3%" align="center" valign="top" nowrap><span class="text12">2<br>次</span></td>
	<%-- 総合は非表示
	<td rowspan="2" width="3%" align="center" valign="top" nowrap><span class="text12" style="writing-mode:tb-rl;">総合</span></td>
	--%>
		<c:forEach var="yearData" items="${i306Bean.yearMonthDateData.yearList}">
			<c:forEach var="monthData" items="${yearData.monthList}">
				<c:forEach var="dateData" items="${monthData.dateList}">
					<td width="2%" align="center"><span class="text12"><c:out value="${dateData.date}" /></span></td>
				</c:forEach>
			</c:forEach>
		</c:forEach>
</tr>
<tr bgcolor="#E1E6EB">
		<c:forEach var="yearData" items="${i306Bean.yearMonthDateData.yearList}">
			<c:forEach var="monthData" items="${yearData.monthList}">
				<c:forEach var="dateData" items="${monthData.dateList}">
					<td width="2%" align="center"><span class="text12"><c:out value="${dateData.day}" /></span></td>
				</c:forEach>
			</c:forEach>
		</c:forEach>
</tr>
<!--/項目-->
<!--set-->
	<c:forEach var="data" items="${i306Bean.resultList}" varStatus="status">
		<tr>
			<td bgcolor="#E1E6EB" ><span class="text12"><c:out value="${data.univNameAbbr}" /> <c:out value="${data.facultyNameAbbr}" /> <c:out value="${data.deptNameAbbr}" /> (<c:out value="${data.entExamModeName}" />)</span></td>
			<td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${data.centerRating}" /></span></td>
			<td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${data.secondRating}" /></span></td>
			<%-- 総合は非表示
			<td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${data.totalRating}" /></span></td>
			--%>
			<c:forEach var="date" items="${data.dateArray}" varStatus="status2">
				<c:choose>
					<c:when test="${date eq '1'}"><td bgcolor="#7AAB78" align="center"><b class="text12" style="color:#FFFFFF;">願</b></td></c:when>
					<c:when test="${date eq '2'}"><td bgcolor="#65A0C6" align="center"><b class="text12" style="color:#FFFFFF;">〆</b></td></c:when>
					<c:when test="${date eq '3'}"><td bgcolor="#EBA225" align="center"><b class="text12" style="color:#FFFFFF;">受</b></td></c:when>
					<c:when test="${date eq '4'}"><td bgcolor="#D95556" align="center"><b class="text12" style="color:#FFFFFF;">発</b></td></c:when>
					<c:otherwise><td bgcolor="#F4E5D6" align="center"><span class="text12">&nbsp;</span></td></c:otherwise>
				</c:choose>

			</c:forEach>
		</tr>
	</c:forEach>
<!--/set-->
</table>
</td>
</tr>
</table>
<!--/リスト-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>

<!--/個人手帳-->
<!--セキュリティスタンプの選択-->
<%@ include file="/jsp/individual/include/security_stamp.jsp" %>
<!--/セキュリティスタンプの選択-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--テキストフォーマット-->
<%@ include file="/jsp/individual/include/text_out_format.jsp" %>
<!--/テキストフォーマット-->

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->


<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>

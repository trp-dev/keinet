<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.keinavi.data.individual.ExaminationData" %>
<%@ page import="jp.co.fj.keinavi.data.individual.SubRecordData" %>
<%@ page import="java.util.List" %>
<%@ page import="jp.co.fj.keinavi.data.help.HelpList" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<jsp:useBean id="i101Bean" scope="request" class="jp.co.fj.keinavi.beans.individual.I101Bean" />
<jsp:useBean id="form" scope="request" class="jp.co.fj.keinavi.forms.individual.I101Form" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script language="JavaScript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    <%@ include file="/jsp/script/submit_change.jsp" %>
    <%@ include file="/jsp/script/submit_exit.jsp" %>
    <%@ include file="/jsp/script/submit_save.jsp" %>
    <%@ include file="/jsp/script/submit_menu.jsp" %>
    <%@ include file="/jsp/script/open_common.jsp" %>
    <%@ include file="/jsp/script/open_sample.jsp" %>
    <%@ include file="/jsp/script/submit_openOnepoint.jsp" %>
    <%@ include file="/jsp/script/submit_help.jsp" %>
    <%@ include file="/jsp/script/clear_disabled.jsp" %>

	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/printDialog_close.jsp" %>
	 var printDialogPattern = "";
	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

    <%-- 対象模試リスト --%>
    <c:set var="examList" value="${ iCommonMap.examComboData.examList }" />

    /**
     * サブミッション
     */
    function submitForm(forward) {
        clearDisabled();
        <%--スクロール位置 0426 K.Kondo--%>
        if(forward=='i101' || forward=='i102') {
            document.forms[0].scrollX.value = document.body.scrollLeft;
            document.forms[0].scrollY.value = document.body.scrollTop;
        } else {
            document.forms[0].scrollX.value = 0;
            document.forms[0].scrollY.value = 0;
        }
        document.getElementById("MainLayer").style.visibility = "hidden";
        document.getElementById("MainLayer").style.top = -2000;
        document.getElementById("LoadingLayer").style.visibility = "visible";
        document.forms[0].target = "_self";
        document.forms[0].forward.value = forward;
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 戻るボタン
     */
    function submitBack() {
        document.forms[0].target = "_self";
        document.forms[0].forward.value = "i003";
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 表示するデータが無い時のテーブル非表示
     */
    function listnone() {
        <%if(i101Bean.getStudentData().getExaminationDatas() != null) {%>
            if(<%=i101Bean.getStudentData().getExaminationDatas().size()%> <= 0){
                document.getElementById("list").style.display = "none";
                document.getElementById("datanone").style.display = "inline";
            }else{
                document.getElementById("datanone").style.display = "none";
            }
        <%} else {%>
            document.getElementById("list").style.display = "none";
            document.getElementById("datanone").style.display = "inline";
        <%}%>
    }
    /**
     * 生徒切り替え
     */
    <%@ include file="/jsp/individual/include/student_rotator.jsp" %>
    /**
     * 印刷ダイアログ
     */
    <%@ include file="/jsp/individual/include/printDialog.jsp" %>
    /**
     * 印刷チェック
     */
    function printCheck() {
        printDialog();
    }
    /**
     * 一括印刷チェック
     */
    function printAllCheck() {
        printAllDialog();
    }
    // 表示
    function printView() {
        document.forms[0].printStatus.value = "1";
        printSheet();
    }
    /**
     * 初期化
     */
    function init(){
        startTimer();
        <%--スクロール位置 0426 K.Kondo--%>
        window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

        //1.選択中の生徒まで回す
        findStudent();
        //2.選択中の生徒が一人ならArrowボタンを無効化する
        if(getStudentNum() == 1) disableArrows();

        //3.学年選択デフォルト
        for(i=0; i<document.forms[0].targetGrade.options.length; i++){
            if(document.forms[0].targetGrade.options[i].value == '<%=form.getTargetGrade()%>'){
                document.forms[0].targetGrade.options[i].selected = true;
            }
        }
        //4.模試種類デフォルト
        for(i=0; i<document.forms[0].targetExamType.options.length; i++){
            if(document.forms[0].targetExamType.options[i].value == '<%=form.getTargetExamType()%>'){
                document.forms[0].targetExamType.options[i].selected = true;
            }
        }
        //5.選択中生徒の学年・模試によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
        gradeChange();
        //6.アプレットに表示する内容が無い時は、下のテーブルも消す
        listnone();
        //ロード中
        <%@ include file="/jsp/script/loading.jsp" %>
    }
//-->
</script>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I101Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="targetYear" value="<c:out value="${iCommonMap.targetExamYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${iCommonMap.targetExamCode}" />">
<input type="hidden" name="printStatus" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeMode" value="">
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->


<!--成績分析-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--分析メニュー-->
<%@ include file="/jsp/individual/include/bunseki_menu.jsp" %>
<!--/分析メニュー-->

<!--概要-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="12" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="698">
<table border="0" cellpadding="0" cellspacing="0" width="698">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="682">

<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td colspan="5" width="682" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="682" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="682" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="666" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">個人成績表</b></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="682" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="682" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="682" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="150" valign="bottom">
<table border="0" cellpadding="0" cellspacing="0" width="150">
<tr>
<td align="center" height="40"><input type="button" class="text12" onclick="openSample('<c:out value="${param.forward}" />');" value="サンプルの表示"></td>
</tr>
<tr>
<td bgcolor="#8CA9BB" height="2"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
</table>
<!--/大タイトル-->

<!--模試の種類-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="360">
    <table border="0" cellpadding="3" cellspacing="3" width="360" height="76">
    <tr>
    <td bgcolor="#E1E6EB">
        <table border="0" cellpadding="3" cellspacing="0">
        <tr>
        <td><span class="text12">対象年度：</span></td>
        <td><%@ include file="/jsp/individual/include/targetExamYear.jsp" %></td>
        </tr>
        <tr>
        <td><span class="text12">対象模試：</span></td>
        <td><%@ include file="/jsp/individual/include/targetExamCode.jsp" %></td>
        </tr>
        </table>
    </td>
    </tr>
    </table>
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="482">
    <table border="0" cellpadding="3" cellspacing="3" width="482" height="76">
    <tr>
    <td bgcolor="#F4E5D6">
        <table border="0" cellpadding="0" cellspacing="0" width="390">
        <tr>
        <td>
            <div><b class="text12">&lt;&lt;オプション&gt;&gt;</b></div>
            <table border="0" cellpadding="3" cellspacing="0">
            <tr>
            <td><span class="text12">表示する学年：</span></td>
            <td><select name="targetGrade" size="1" class="text12">
                        <option value="1">現学年のみ</option>
                        <option value="2">全ての学年</option></select></td>
            <td><span class="text12">模試の種類：</span></td>
            <td><select name="targetExamType" size="1">
                        <option value="1">全ての種類</option>
                        <option value="2">同系統模試のみ</option></select></td>
            </tr>
            </table>
            <div style="margin-top:3px;"><span class="text10">※これらのオプション項目を変更した場合は、［再表示］ボタンをクリックしてください。</span></div>
        </td>
        </tr>
        </table>
    </td>
    <td width="71" bgcolor="#F4E5D6" align="center"><input type="button" value="再表示" class="text12" style="width:60px;" onclick="submitForm('i101');"></td>
    </tr>
    </table>
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="5" width="848" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/模試の種類-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<%--受験学力測定--%>
<kn:examContain id="ability" items="${i101Bean.studentData.examinationDatas}">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr align="right">
<td>
<span class="text12">※受験学力測定テストで国語２科目受験の場合、<br>理科の欄に国語２科目めが表示されます。&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
</td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</kn:examContain>

<div id="list">
<!--メニュー切り替え-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="159" height="24" bgcolor="#FFAD8C" align="center"><span class="text12">科目別成績</span></td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="159" height="24">

<table border="0" cellpadding="0" cellspacing="0" width="159">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="159">
<tr>
<td width="157" height="32" bgcolor="#E5EEF3" align="center"><span class="text12"><a href="javascript:submitForm('i102');">志望大学別成績</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<td width="526" align="right">
<%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
<%--<span class="text10" >※英語CEFRは本人申告中、レベルの高い方を表示しています。理科・地歴公民について、
	<br>第２解答科目の共通テスト換算得点の方が第１解答科目の共通テスト換算得点よりも&nbsp;&nbsp;&nbsp;
	<br><div style="margin-right:43%">高いときに"*"を表示しています。</div>
--%>
<span class="text10" >
	※理科・地歴公民について、第２解答科目の共通テスト換算得点の方が&nbsp;&nbsp;&nbsp;
	<br>第１解答科目の共通テスト換算得点よりも高いときに"*"を表示しています。
<%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
</span>
</td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td bgcolor="#FFAD8C"><img src="./shared_lib/img/parts/sp.gif" width="848" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/メニュー切り替え-->

    <table border="0" cellpadding="0" cellspacing="2" width="852">
    <!--項目-->
    <tr height="22" bgcolor="#CDD7DD" align="center">
    <td rowspan="2" width="10%"><span class="text12">模試名</span></td>
    <td rowspan="2" width="7%"><span class="text12">項目</span></td>
    <td rowspan="2" colspan="3" width="12%"><span class="text12">英語</span></td>
    <td rowspan="2" colspan="3" width="18%"><span class="text12">数学</span></td>
<%-- 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START --%>
    <%-- <td rowspan="2" colspan="2" width="12%"><span class="text12">国語</span></td> --%>
    <td rowspan="2" width="6%"><span class="text12">国語</span></td>
<%-- 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DPD START --%>
    <td colspan="3" width="18%"><span class="text12">理科</span></td>
    <td colspan="2" width="12%"><span class="text12">地歴公民</span></td>
    <td rowspan="2" width="6%"><span class="text12">総合</span></td>
    </tr>
    <tr height="22" bgcolor="#CDD7DD" align="center">
    <td width="6%"><span class="text12">(第1)</span></td>
    <td width="6%" nowrap><span class="text12">(第2/基1)</span></td>
    <td width="6%"><span class="text12">(基2)</span></td>
    <td width="6%"><span class="text12">(第1)</span></td>
    <td width="6%"><span class="text12">(第2)</span></td>
    </tr>
    <!--/項目-->
    <c:forEach var="exam" items="${i101Bean.studentData.examinationDatas}">

    <c:set var="record1" value="${exam.engCourse[0]}"/>
    <c:set var="record2" value="${exam.engCourse[1]}"/>
    <c:set var="record3" value="${exam.engCourse[2]}"/>
    <c:set var="record4" value="${exam.matCourse[0]}"/>
    <c:set var="record5" value="${exam.matCourse[1]}"/>
    <c:set var="record6" value="${exam.matCourse[2]}"/>
    <c:set var="record7" value="${exam.japCourse[0]}"/>
    <c:set var="record8" value="${exam.japCourse[1]}"/>
    <c:set var="record9" value="${exam.sciCourse[0]}"/>
    <c:set var="record10" value="${exam.sciCourse[1]}"/>
    <c:set var="record11" value="${exam.sciCourse[2]}"/>
    <c:set var="record12" value="${exam.socCourse[0]}"/>
    <c:set var="record13" value="${exam.socCourse[1]}"/>
    <c:set var="record14" value="${exam.allCourse[0]}"/>

    <%-- 理科の先頭が基礎科目なら、先頭を空欄にする --%>
    <c:if test="${ record9.basicFlg eq '1' }">
        <c:set var="record11" value="${record10}" />
        <c:set var="record10" value="${record9}" />
        <c:remove var="record9" />
    </c:if>

    <kn:choose>
        <%--受験学力測定--%>
        <kn:examWhen id="ability" var="${exam}">
            <c:set var="record9" value="${exam.japCourse[1]}"/>
        </kn:examWhen>
    </kn:choose>

    <!--1段目-->
    <tr height="22" bgcolor="#E1E6EB">
    <td rowspan="2" width="9%" align="center"><span class="text12"><c:out value="${exam.examName}"/></span></td>
    <td width="7%">
    <table border="0" cellpadding="4" cellspacing="0">
    <tr>
    <td><span class="text12">科目</span></td>
    </tr>
    </table>
    </td>
    <td width="6%" align="center"><span class="text12"><c:out value="${record1.subName}"/></span></td>
    <td width="6%" align="center"><span class="text12"><c:out value="${record2.subName}"/></span></td>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
    <%-- <kn:choose>
    	<kn:examWhen id="mark" var="${exam}">
    		<td width="6%" align="center"><span class="text12"><c:out value="ＣＥＦＲ"/></span></td>
     	</kn:examWhen>
            <kn:otherwise>
            <td width="6%" align="center"><span class="text12"><c:out value=""/></span></td>
        </kn:otherwise>
    </kn:choose> --%>
    <td width="6%" align="center"><span class="text12"><c:out value="${record3.subName}"/></span></td>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
    <td width="6%" align="center"><span class="text12"><c:out value="${record4.subName}"/></span></td>
    <td width="6%" align="center"><span class="text12"><c:out value="${record5.subName}"/></span></td>
    <td width="6%" align="center"><span class="text12"><c:out value="${record6.subName}"/></span></td>
    <td width="6%" align="center"><span class="text12"><c:out value="${record7.subName}"/></span></td>
<%-- 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START --%>
    <%-- <td width="6%" align="center"><span class="text12"><c:out value="${record8.subName}"/></span></td> --%>
<%-- 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START --%>
    <td width="6%" align="center"><span class="text12"><c:out value="${record9.subName}"/></span></td>
    <td width="6%" align="center"><span class="text12"><c:out value="${record10.subName}"/></span></td>
    <td width="6%" align="center"><span class="text12"><c:out value="${record11.subName}"/></span></td>
    <td width="6%" align="center"><span class="text12"><c:out value="${record12.subName}"/></span></td>
    <td width="6%" align="center"><span class="text12"><c:out value="${record13.subName}"/></span></td>
    <td width="6%" align="center"><span class="text12"><c:out value="${record14.subName}"/></span></td>
    </tr>

    <tr bgcolor="#F4E5D6">
    <td width="7%">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr height="34">
    <td><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
    <td><span class="text12">

        <kn:choose>
            <%--受験学力測定--%>
            <kn:examWhen id="ability" var="${exam}">
                配点<br>スコア
            </kn:examWhen>
            <kn:otherwise>
                配点<br>得点
            </kn:otherwise>
        </kn:choose>

    </span></td>
    </tr>


    <tr>
    <td colspan="2" bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="34">
    <td><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
    <td><span class="text12">

        <kn:choose>
            <%--受験学力測定--%>
            <kn:examWhen id="ability" var="${exam}">
                到達エリア
            </kn:examWhen>
            <%--マーク系--%>
            <kn:examWhen id="mark" var="${exam}">
                <kn:choose>
                    <%--センターリサーチ--%>
                    <kn:examWhen id="center" var="${exam}">
                        偏差値
                    </kn:examWhen>
                    <kn:otherwise>
                        換算得点<br>偏差値
                    </kn:otherwise>
                </kn:choose>
            </kn:examWhen>
            <kn:otherwise>
                偏差値
            </kn:otherwise>
        </kn:choose>

    </span></td>
    </tr>

    <%--学力レベル start--%>
    <tr>
    <td colspan="2" bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="17">
    <td><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
    <td><span class="text10">

        学力レベル

    </span></td>
    </tr>
    <%--学力レベル end--%>

    </table>
    </td>

    <td width="6%">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">

    <tr height="34">
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
    <%--<td align="center"><span class="text12"><c:out value="${record1.subAllotPnt}" default=""/><br><c:out value="${record1.score}" default=""/></span></td>--%>
    <td align="center"><span class="text12"><c:out value="${record1.subAllotPntStr}" default=""/><br><c:out value="${record1.score}" default=""/></span></td>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
    </tr>

    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="34">
    <td align="center"><span class="text12"><kn:examIf id="patternIII" var="${exam}"><c:out value="${record1.cvsScore}" default=""/><br></kn:examIf>

    <kn:choose>
        <%--受験学力測定--%>
        <kn:examWhen id="ability" var="${exam}">
            <c:out value="${record1.level}" default=""/></kn:examWhen>
        <kn:otherwise>
            <c:out value="${record1.CDeviation}" default=""/></kn:otherwise>
    </kn:choose>

    </span></td>
    </tr>
    <%--学力レベル[英語1] start--%>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="17">
    <td align="center"><span class="text12">

        <c:out value="${record1.scholarlevel}" default=""/>

    </span></td>
    </tr>
    <%--学力レベル[英語1] end--%>
    </table>
    </td>
    <td width="6%">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr height="34">
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
    <%--<td align="center"><span class="text12"><c:out value="${record2.subAllotPnt}" default=""/><br><c:out value="${record2.score}" default=""/></span></td>--%>
    <td align="center"><span class="text12"><c:out value="${record2.subAllotPntStr}" default=""/><br><c:out value="${record2.score}" default=""/></span></td>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
    </tr>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="34">
    <td align="center"><span class="text12"><kn:examIf id="patternIII" var="${exam}"><c:out value="${record2.cvsScore}" default=""/><br></kn:examIf>

    <kn:choose>
        <%--受験学力測定--%>
        <kn:examWhen id="ability" var="${exam}">
            <c:out value="${record2.level}" default=""/></kn:examWhen>
        <kn:otherwise>
            <c:out value="${record2.CDeviation}" default=""/></kn:otherwise>
    </kn:choose>

    </span></td>
    </tr>
    <%--学力レベル[英語2] start--%>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="17">
    <td align="center"><span class="text12">

        <c:out value="${record2.scholarlevel}" default=""/>

    </span></td>
    </tr>
    <%--学力レベル[英語2] end--%>
    </table>
    </td>
    <td width="6%">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr height="34">
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
    <%--
    <kn:choose>
    	<kn:examWhen id="mark" var="${exam}">
    		<td align="center"><span class="text12"><c:out value="" default=""/><br><c:out value="${record1.cefr}" default=""/></span></td>
    	</kn:examWhen>
    	<kn:otherwise>
    		<td align="center"><span class="text12"><c:out value="" default=""/><br><c:out value="" default=""/></span></td>
    	</kn:otherwise>
    </kn:choose>
    --%>
    <td align="center"><span class="text12"><c:out value="${record3.subAllotPntStr}" default=""/><br><c:out value="${record3.score}" default=""/></span></td>
    </tr>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="34">
    <%--<td align="center"></td>--%>
    <td align="center"><span class="text12"><kn:examIf id="patternIII" var="${exam}"><c:out value="${record3.cvsScore}" default=""/><br></kn:examIf>

    <kn:choose>
        <%--受験学力測定--%>
        <kn:examWhen id="ability" var="${exam}">
            <c:out value="${record3.level}" default=""/></kn:examWhen>
        <kn:otherwise>
            <c:out value="${record3.CDeviation}" default=""/></kn:otherwise>
    </kn:choose>

    </span></td>
    </tr>
        <%--学力レベル[英語3] start--%>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="17">
    <td align="center"><span class="text12">
    <c:out value="${record3.scholarlevel}" default=""/>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
    </span></td>
    </tr>
    <%--学力レベル[英語3] end--%>
    </table>
    </td>
    <td width="6%">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr height="34">
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
    <%--<td align="center"><span class="text12"><c:out value="${record4.subAllotPnt}" default=""/><br><c:out value="${record4.score}" default=""/></span></td>--%>
    <td align="center"><span class="text12"><c:out value="${record4.subAllotPntStr}" default=""/><br><c:out value="${record4.score}" default=""/></span></td>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
    </tr>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="34">
    <td align="center"><span class="text12"><kn:examIf id="patternIII" var="${exam}"><c:out value="${record4.cvsScore}" default=""/><br></kn:examIf>

    <kn:choose>
        <%--受験学力測定--%>
        <kn:examWhen id="ability" var="${exam}">
            <c:out value="${record4.level}" default=""/></kn:examWhen>
        <kn:otherwise>
            <c:out value="${record4.CDeviation}" default=""/></kn:otherwise>
    </kn:choose>

    </span></td>
    </tr>
    <%--学力レベル[数学1] start--%>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="17">
    <td align="center"><span class="text12">

        <c:out value="${record4.scholarlevel}" default=""/>

    </span></td>
    </tr>
    <%--学力レベル[数学1] end--%>
    </table>
    </td>
    <td width="6%">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr height="34">
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
    <%--<td align="center"><span class="text12"><c:out value="${record5.subAllotPnt}" default=""/><br><c:out value="${record5.score}" default=""/></span></td>--%>
    <td align="center"><span class="text12"><c:out value="${record5.subAllotPntStr}" default=""/><br><c:out value="${record5.score}" default=""/></span></td>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
    </tr>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="34">
    <td align="center"><span class="text12"><kn:examIf id="patternIII" var="${exam}"><c:out value="${record5.cvsScore}" default=""/><br></kn:examIf>

    <kn:choose>
        <%--受験学力測定--%>
        <kn:examWhen id="ability" var="${exam}">
            <c:out value="${record5.level}" default=""/></kn:examWhen>
        <kn:otherwise>
            <c:out value="${record5.CDeviation}" default=""/></kn:otherwise>
    </kn:choose>

    </span></td>
    </tr>
    <%--学力レベル[数学2] start--%>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="17">
    <td align="center"><span class="text12">

        <c:out value="${record5.scholarlevel}" default=""/>

    </span></td>
    </tr>
    <%--学力レベル[数学2] end--%>
    </table>
    </td>
    <td width="6%">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr height="34">
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
    <%--<td align="center"><span class="text12"><c:out value="${record6.subAllotPnt}" default=""/><br><c:out value="${record6.score}" default=""/></span></td>--%>
    <td align="center"><span class="text12"><c:out value="${record6.subAllotPntStr}" default=""/><br><c:out value="${record6.score}" default=""/></span></td>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
    </tr>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="34">
    <td align="center"><span class="text12"><kn:examIf id="patternIII" var="${exam}"><c:out value="${record6.cvsScore}" default=""/><br></kn:examIf>

    <kn:choose>
        <%--受験学力測定--%>
        <kn:examWhen id="ability" var="${exam}">
            <c:out value="${record6.level}" default=""/></kn:examWhen>
        <kn:otherwise>
            <c:out value="${record6.CDeviation}" default=""/></kn:otherwise>
    </kn:choose>

    </span></td>
    </tr>

    <%--学力レベル[数学3] start--%>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="17">
    <td align="center"><span class="text12">

        <c:out value="${record6.scholarlevel}" default=""/>

    </span></td>
    </tr>
    <%--学力レベル[数学3] end--%>

    </table>
    </td>
    <td width="6%">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr height="34">
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
    <%--<td align="center"><span class="text12"><c:out value="${record7.subAllotPnt}" default=""/><br><c:out value="${record7.score}" default=""/></span></td>--%>
    <td align="center"><span class="text12"><c:out value="${record7.subAllotPntStr}" default=""/><br><c:out value="${record7.score}" default=""/></span></td>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
    </tr>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="34">
    <td align="center"><span class="text12"><kn:examIf id="patternIII" var="${exam}"><c:out value="${record7.cvsScore}" default=""/><br></kn:examIf>

    <kn:choose>
        <%--受験学力測定--%>
        <kn:examWhen id="ability" var="${exam}">
            <c:out value="${record7.level}" default=""/></kn:examWhen>
        <kn:otherwise>
            <c:out value="${record7.CDeviation}" default=""/></kn:otherwise>
    </kn:choose>

    </span></td>
    </tr>
    <%--学力レベル[国語] start--%>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="17">
    <td align="center"><span class="text12">

        <c:out value="${record7.scholarlevel}" default=""/>

    </span></td>
    </tr>
    <%--学力レベル[国語] end--%>

     </table>
    </td>
<%-- 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START --%>
<%--     <td width="6%"> --%>
<%--     <table border="0" cellpadding="0" cellspacing="0" width="100%"> --%>
<%--     <tr height="34"> --%>
<%--     <td align="center"><span class="text12"><c:out value="" default=""/><br><c:out value="${record8.score}" default=""/></span></td> --%>
<%--     </tr> --%>
<%--     <tr> --%>
<%--     <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td> --%>
<%--     </tr> --%>
<%--     <tr height="34"> --%>
<%--     <td align="center"> --%>
<%--     </tr> --%>
    <%--学力レベル[国語(記)] start--%>
<%--     <tr> --%>
<%--     <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td> --%>
<%--     </tr> --%>
<%--     <tr height="17"> --%>
<%--     <td align="center"><span class="text12"> --%>

<%--         <c:out value="" default=""/> --%>

<%--     </span></td> --%>
<%--     </tr> --%>
    <%--学力レベル[国語（記）] end--%>
<%--     </table> --%>
<%--     </td> --%>
<%-- 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END --%>
    <td width="6%">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr height="34">
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
    <%--<td align="center"><span class="text12"><c:out value="${record9.subAllotPnt}" default=""/><br><c:out value="${record9.score}" default=""/></span></td>--%>
     <td align="center"><span class="text12"><c:out value="${record9.subAllotPntStr}" default=""/><br><c:out value="${record9.score}" default=""/></span></td>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
    </tr>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="34">
    <td align="center"><span class="text12"><kn:examIf id="patternIII" var="${exam}"><c:out value="${record9.cvsScore}" default=""/><br></kn:examIf>
    <kn:choose>
        <%--受験学力測定--%>
        <kn:examWhen id="ability" var="${exam}">
            <c:out value="${record9.level}" default=""/></kn:examWhen>
        <kn:otherwise>
            <c:out value="${record9.CDeviation}" default=""/></kn:otherwise>
    </kn:choose>

    </span></td>
    </tr>
    <%--学力レベル[理科1] start--%>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="17">
    <td align="center"><span class="text12">

        <c:out value="${record9.scholarlevel}" default=""/>

    </span></td>
    </tr>
    <%--学力レベル[理科1] end--%>
    </table>
    </td>
    <td width="6%">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr height="34">
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
    <%--<td align="center"><span class="text12"><c:out value="${record10.subAllotPnt}" default=""/><br><c:out value="${record10.score}" default=""/></span></td>--%>
    <td align="center"><span class="text12"><c:out value="${record10.subAllotPntStr}" default=""/><br><c:out value="${record10.score}" default=""/></span></td>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
    </tr>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="34">
    <td align="center"><span class="text12"><kn:examIf id="patternIII" var="${exam}"><c:out value="${record10.cvsScore}" default=""/><br></kn:examIf>

    <kn:choose>
        <%--受験学力測定--%>
        <kn:examWhen id="ability" var="${exam}">
            <c:out value="${record10.level}" default=""/></kn:examWhen>
        <kn:otherwise>
            <c:out value="${record10.CDeviation}" default=""/></kn:otherwise>
    </kn:choose>

    </span></td>
    </tr>
    <%--学力レベル[理科2] start--%>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="17">
    <td align="center"><span class="text12">

        <c:out value="${record10.scholarlevel}" default=""/>

    </span></td>
    </tr>
    <%--学力レベル[理科2] end--%>
    </table>
    </td>
    <td width="6%">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr height="34">
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
    <%--<td align="center"><span class="text12"><c:out value="${record11.subAllotPnt}" default=""/><br><c:out value="${record11.score}" default=""/></span></td>--%>
    <td align="center"><span class="text12"><c:out value="${record11.subAllotPntStr}" default=""/><br><c:out value="${record11.score}" default=""/></span></td>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
    </tr>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="34">
    <td align="center"><span class="text12"><kn:examIf id="patternIII" var="${exam}"><c:out value="${record11.cvsScore}" default=""/><br></kn:examIf>

    <kn:choose>
        <%--受験学力測定--%>
        <kn:examWhen id="ability" var="${exam}">
            <c:out value="${record11.level}" default=""/></kn:examWhen>
        <kn:otherwise>
            <c:out value="${record11.CDeviation}" default=""/></kn:otherwise>
    </kn:choose>

    </span></td>
    </tr>
    <%--学力レベル[理科3] start--%>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="17">
    <td align="center"><span class="text12">

        <c:out value="${record11.scholarlevel}" default=""/>

    </span></td>
    </tr>
    <%--学力レベル[理科3] end--%>
    </table>
    </td>
    <td width="6%">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr height="34">
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
    <%--<td align="center"><span class="text12"><c:out value="${record12.subAllotPnt}" default=""/><br><c:out value="${record12.score}" default=""/></span></td>--%>
    <td align="center"><span class="text12"><c:out value="${record12.subAllotPntStr}" default=""/><br><c:out value="${record12.score}" default=""/></span></td>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
    </tr>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="34">
    <td align="center"><span class="text12"><kn:examIf id="patternIII" var="${exam}"><c:out value="${record12.cvsScore}" default=""/><br></kn:examIf>

    <kn:choose>
        <%--受験学力測定--%>
        <kn:examWhen id="ability" var="${exam}">
            <c:out value="${record12.level}" default=""/></kn:examWhen>
        <kn:otherwise>
            <c:out value="${record12.CDeviation}" default=""/></kn:otherwise>
    </kn:choose>

    </span></td>
    </tr>
    <%--学力レベル[地歴・公民1] start--%>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="17">
    <td align="center"><span class="text12">

        <c:out value="${record12.scholarlevel}" default=""/>

    </span></td>
    </tr>
    <%--学力レベル[地歴・公民1] end--%>
    </table>
    </td>
    <td width="6%">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr height="34">
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
    <%--<td align="center"><span class="text12"><c:out value="${record13.subAllotPnt}" default=""/><br><c:out value="${record13.score}" default=""/></span></td>--%>
    <td align="center"><span class="text12"><c:out value="${record13.subAllotPntStr}" default=""/><br><c:out value="${record13.score}" default=""/></span></td>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
    </tr>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="34">
    <td align="center"><span class="text12"><kn:examIf id="patternIII" var="${exam}"><c:out value="${record13.cvsScore}" default=""/><br></kn:examIf>

    <kn:choose>
        <%--受験学力測定--%>
        <kn:examWhen id="ability" var="${exam}">
            <c:out value="${record13.level}" default=""/></kn:examWhen>
        <kn:otherwise>
            <c:out value="${record13.CDeviation}" default=""/></kn:otherwise>
    </kn:choose>

    </span></td>
    </tr>
    <%--学力レベル[地歴・公民2] start--%>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="17">
    <td align="center"><span class="text12">

        <c:out value="${record13.scholarlevel}" default=""/>

    </span></td>
    </tr>
    <%--学力レベル[地歴・公民2] end--%>
    </table>
    </td>
    <td width="6%">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr height="34">
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD START --%>
    <%--<td align="center"><span class="text12"><c:out value="${record14.subAllotPnt}" default=""/><br><c:out value="${record14.score}" default=""/></span></td>--%>
    <td align="center"><span class="text12"><c:out value="${record14.subAllotPntStr}" default=""/><br><c:out value="${record14.score}" default=""/></span></td>
    <%-- 2019/12/02 QQ)Ooseto 英語認定試験延期対応 UPD END --%>
    </tr>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="34">
    <td align="center"><span class="text12"><kn:examIf id="patternIII" var="${exam}"><c:out value="${record14.cvsScore}" default=""/><br></kn:examIf>

    <kn:choose>
        <%--受験学力測定--%>
        <kn:examWhen id="ability" var="${exam}">
            <c:out value="${record14.level}" default=""/></kn:examWhen>
        <kn:otherwise>
            <c:out value="${record14.CDeviation}" default=""/></kn:otherwise>
    </kn:choose>

    </span></td>
    </tr>
        <%--学力レベル[総合] start--%>
    <tr>
    <td bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    </tr>
    <tr height="17">
    <td align="center"><span class="text12">

        <c:out value="${record14.scholarlevel}" default=""/>

    </span></td>
    </tr>
    <%--学力レベル[総合] end--%>
    </table>
    </td>
    </tr>
    <!--1段目-->
    </c:forEach>
    </table>
</div>

<div id = "datanone">
    <table border="0" cellpadding="0" cellspacing="0" width="848" height="100">
    <tr>
    <td align="left"><span class="text18"><FONT COLOR = "#CCCCCC">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;表示するデータがありません</FONT></span></td>
    </tr>
    </table>
</div>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/成績分析-->

<!--セキュリティスタンプの選択-->
<%@ include file="/jsp/individual/include/security_stamp.jsp" %>
<!--/セキュリティスタンプの選択-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->


<!--印刷対象生徒-->
<%@ include file="/jsp/individual/include/printStudent.jsp" %>
<!--/印刷対象生徒-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--ケイコさん-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="784">
<tr valign="top">
<td width="72" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="702">

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="678" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="678" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="678" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="678" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr>
<td width="12" background="./shared_lib/img/parts/wb_arrow_l_long.gif"><img src="./shared_lib/img/parts/sp.gif" width="12" height="5" border="0" alt=""><br></td>
<td width="688" bgcolor="#F9EEE5">
<!--アドバイス-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="688">
<tr valign="top">
<td width="131" align="center"><img src="./shared_lib/img/parts/word_one_point.gif" width="86" height="32" border="0" alt="ワンポイントアドバイス！！" vspace="5"><br></td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="5" border="0" alt=""><br></td>
<td width="511">


<div style="margin-top:5px;margin-bottom:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="511">
<%@ include file="/jsp/shared_lib/I_Onepoint.jsp" %>
</table>
</div>


</td>
<td width="25"><img src="./shared_lib/img/parts/sp.gif" width="25" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/アドバイス-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="702">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="678" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="678" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="678" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="678" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

</form>
</div>
</body>
</html>

<%--
	2005.3.2	K.Kondo 	[1]
	2005.3.3	T.Yamada 	[2]模試で記入された大学の判定と判定履歴の判定を別の判定結果に収める
	2005.4.6 	T.Yamada	[3]対象模試がマークまたは記述でなければ判定を行っていないので、詳細リンクをださない。
	2005.5.27	K.Kondo		[4]模試に記入された大学が無い場合と、判定履歴に大学が無い場合のメッセージを分ける。
--%>
<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="iCommonMap" scope="session" class="jp.co.fj.keinavi.data.individual.ICommonMap" />
<c:set value="individual" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／個人成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<SCRIPT type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	<%-- フォームユーティリティ --%>
	var util = new FormUtil();

	<%-- 受験予定大学登録情報 --%>
	var registed = new Array();
	<c:forEach var="i301Data" items="${i301Bean.i301DatasAtJudge}" varStatus="status">
		<c:if test="${i301Data.registered}">
			registed["<c:out value="${i301Data.univValue}" />"] = new Array("<c:out value="${i301Data.univName_Abbr}" />", "<c:out value="${i301Data.facultyName_Abbr}" />", "<c:out value="${i301Data.deptName_Abbr}" />");
		</c:if>
	</c:forEach>

	/**
	 * 共通サブミッション
	 */
	function submitForm(forward) {
		<%--スクロール位置 0426 K.Kondo--%>
		if(forward=='<c:out value="${param.forward}" />') {
			document.forms[0].scrollX.value = document.body.scrollLeft;
			document.forms[0].scrollY.value = document.body.scrollTop;
		} else {
			document.forms[0].scrollX.value = 0;
			document.forms[0].scrollY.value = 0;
		}
		if(forward == 'i205'){
			op = window.open("blank.html", "sheet", "width=930,height=600,resizable=yes,scrollbars=yes,status=yes,titlebar=yes");
			op.moveTo(50, 50);
			document.forms[0].target = "sheet";
			document.forms[0].action="<c:url value="I205Servlet" />";
		}else{
			document.getElementById("MainLayer").style.visibility = "hidden";
			document.getElementById("MainLayer").style.top = -2000;
			document.getElementById("LoadingLayer").style.visibility = "visible";
			document.forms[0].target = "_self";
		}
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	/**
	 * 戻るボタン
	 */
	function submitBack() {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "i003";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}
	<%-- [2] add start --%>
	/**
	 * 模試で記入された大学=1
	 * 判定履歴=1
	 *
	 */
	function setJudgeNo(no){
		document.forms[0].judgeNo.value = no;
	}
	<%-- [2] add end--%>
	/**
	 * 詳細画面表示のための大学ユニークキーの設定
	 */
	function setUniqueKey(key){
		document.forms[0].uniqueKey.value = key;
	}
	/**
	 * 詳細画面表示のための判定履歴にあった模試の
	 * 模試年度と模試コードの設定
	 * これって必要か？
	 */
	function setExamInfo(year, cd){
		document.forms[0].examYear.value = year;
		document.forms[0].examCd.value = cd;
	}
	/**
	 * 詳細画面表示のためのサブミッション
	 */
	function submitDetail(){
		submitForm('i205');
	}
	/**
	 * 判定履歴結果からのサブミッション
	 */
	function registerFromJudge(forward){
		document.forms[0].button.value = "registerFromJudge";
		submitForm(forward);
	}

	function createDupliErrorMessage(obj) {
		var array = registed[obj.value];
		if (obj.checked && array) {
			var msg = "・";
			if (array[0].length > 0) {
				msg += array[0] + " ";
			}
			if (array[1].length > 0) {
				msg += array[1] + " ";
			}
			msg += array[2] + "\n";
			return msg;
		} else {
			return "";
		}
	}

	function deleteFromJudge(forward){
		if (confirm("<kn:message id="i301c" />")) {
			document.forms[0].button.value = "deleteFromJudge";
			submitForm(forward);
		}
	}

	function listnone() {
		<%--削除[4] 050527 K.Kondo
		if('<c:out value="${i301Bean.i301DatasAtJudgeSize}" />' <= 0) {
			document.getElementById("planunivDisp").style.display = "none";
		} else {
			document.getElementById("noHistoryDisp").style.display = "none";
		}
		--%>
		<%--模試に記入された大学が無い場合と、判定履歴が無い場合と表示を変える[4] 050527 K.Kondo --%>
		if('<c:out value="${i301Bean.i301DatasAtExamSize}" />' > 0) {
			document.getElementById("noExamDisp").style.display = "none";
		}
		if('<c:out value="${i301Bean.i301DatasAtJudgeSize}" />' > 0) {
			document.getElementById("noHistoryDisp").style.display = "none";
		}
	}
	/**
	 * 生徒切り替え
	 */
	<%@ include file="/jsp/individual/include/student_rotator.jsp" %>

	<%-- 登録ボタンの状態変更 --%>
	function changeRegBtnState() {
		var count = util.countChecked(document.forms[0].univValueAtExam)
				+ util.countChecked(document.forms[0].univValueAtJudge);
		document.forms[0].regJudeUnivButton.disabled = count > 0 ? false : "disabled";;
	}

	<%-- 削除ボタンの状態変更 --%>
	function changeDelBtnState() {
		document.forms[0].delJudeUnivButton.disabled = util.countChecked(
				document.forms[0].univDeleteValue) > 0 ? false : "disabled";
	}

	/**
	 * 初期化
	 */
	function init(){
		startTimer();
		<%--スクロール位置 0426 K.Kondo--%>
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
		//1.選択中の生徒まで回す
		findStudent();
		//2.選択中の生徒が一人ならArrowボタンを無効化する
		if(getStudentNum() == 1) disableArrows();

		//表示するデータが無い時に、テーブルを非表示にする
		listnone();

		//選択中生徒の学年によって表示を変える(/jsp/individual/include/student_rotator.jsp)記述
		gradeChange();

		//判定リンク表示・非表示
		if("<%=iCommonMap.getTargetExamTypeCode()%>" == "01" || "<%=iCommonMap.getTargetExamTypeCode()%>" == "02"){
			document.getElementById("judge_button").style.display = "inline";
			document.getElementById("judge_button_upper").style.display = "inline";
			document.getElementById("judge_button_upper2").style.display = "none";
		}else{
			document.getElementById("judge_button").style.display = "none";
			document.getElementById("judge_button_upper").style.display = "none";
			document.getElementById("judge_button_upper2").style.display = "inline";
		}

		//ロード中
		<%@ include file="/jsp/script/loading.jsp" %>

		//メッセージ処理
		<c:if test="${ not empty i301Bean.errorMessage }">
			alert("<c:out value="${i301Bean.errorMessage}" />");
		</c:if>
	}
//-->
</SCRIPT>
</head>

<body onload="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;visibility:hidden;top:0px;left:0px">
<form action="<c:url value="I301Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="targetPersonId" value="">
<input type="hidden" name="button" value="">
<input type="hidden" name="changeMode" value="">
<%-- 詳細表示用パラメター --%>
<input type="hidden" name="uniqueKey" value="">
<input type="hidden" name="examYear" value="">
<input type="hidden" name="examCd" value="">
<input type="hidden" name="judgeNo" value=""><%-- [2] add --%>
<%--スクロール位置 0426 K.Kondo--%>
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--ボタン＆モード-->
<%@ include file="/jsp/individual/include/button_mode.jsp" %>
<!--/ボタン＆モード-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/individual/include/contents_menu.jsp" %>
<!--/コンテンツメニュー-->


<!--個人手帳-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<%@ include file="/jsp/individual/include/sub_title.jsp" %>
<!--/中タイトル-->

<!--概要-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="878">

<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--メニュー切り替え-->
<%@ include file="/jsp/individual/include/menu_switch.jsp" %>
<!--/メニュー切り替え-->
<!--小タイトル-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="832">

<table border="0" cellpadding="0" cellspacing="0" width="832">
<tr valign="top">
<td colspan="4" width="832" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="832" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="832" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="816" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">受験予定大学候補</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="832" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="832" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="832" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/小タイトル-->

<!--受験予定大学表示-->
<div id = 'planunivDisp'>

<!--説明-->
<div style="margin-top:12px;">
<div id="judge_button_upper">
<table border="0" cellpadding="0" cellspacing="0" width="828">
<tr>
<td width="668" valign="top"><span class="text14">下記の中から受験予定大学として登録したい大学にチェックし、「受験予定大学に登録」ボタンを押してください。<br>下記に目的の大学がない場合は、右の「志望大学判定」ボタンをクリックし、検索・判定して加えてください。</span></td>
<td width="160" valign="top"><a href="javascript:submitForm('i201')"><img src="./shared_lib/img/btn/daigaku.gif" width="156" height="37" border="0" alt="志望大学判定"></a></div></td>
</tr>
</table>
</div>
</div>
<div style="margin-top:12px;">
<div id="judge_button_upper2">
<table border="0" cellpadding="0" cellspacing="0" width="828">
<tr>
<td width="828" valign="top"><span class="text14">下記の中から受験予定大学として登録したい大学にチェックし、「受験予定大学に登録」ボタンを押してください。</span></td>
</tr>
</table>
</div>
</div>
<!--/説明-->

<!---->
<div style="margin-top:48px;">
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr valign="bottom">
<td width="20"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="828"><b class="text16" style="color:#657681;">模試で記入された大学</b></td>
</tr>
</table>
</div>
<!--/-->

<!--リスト-->
<table border="0" cellpadding="0" cellspacing="0" width="852">

<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
<td width="832">

<table border="0" cellpadding="2" cellspacing="2" width="832">
<tr>
<td colspan="11" width="100%">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><span class="text12"><b>対象模試：</b><c:out value="${sessionScope['iCommonMap'].targetExamName}" /></span></td>
</tr>
</table>
</td>
</tr>

<tr>
<td colspan="11" width="100%" bgcolor="#93A3AD">
	<table border="0" cellpadding="3" cellspacing="0">
	<tr height="27">
	<td><input type="checkbox" name="examUnivCtrl" onclick="util.checkAllElements(this.checked, 'univValueAtExam');changeRegBtnState();"></td>
	<td><b class="text12" style="color:#FFF;">登録対象をすべて選択　　（チェックを外すとすべて解除）</b></td>
	</tr>
	</table>
</td>
</tr>

<!--項目-->
<tr bgcolor="#CDD7DD" align="center">
<td rowspan="2" width="7%"><span class="text12">受験予定<br>登録</span></td>
<td rowspan="2" width="5%"><span class="text12">&nbsp;</span></td>
<td rowspan="2" width="16%"><span class="text12">大学</span></td>
<td rowspan="2" width="13%"><span class="text12">学部</span></td>
<td rowspan="2" width="13%"><span class="text12">学科</span></td>
<td rowspan="2" width="6%"><span class="text12">所在地</span></td>
<td rowspan="2" width="6%"><span class="text12">センター</span></td>
<td rowspan="2" width="6%"><span class="text12">2次</span></td>
<td colspan="2" width="12%"><span class="text12">総合</span></td>
<td rowspan="2" width="16%"><span class="text12">配点比率</span></td>
</tr>
<tr bgcolor="#CDD7DD" align="center">
<td width="6%"><span class="text12">ポイント</span></td>
<td width="6%"><span class="text12">評価</span></td>
</tr>
<!--/項目-->
<!--カレントset-->
<c:forEach var="i301Data" items="${i301Bean.i301DatasAtExam}" varStatus="status">
<tr>
<td bgcolor="#E1E6EB" align="center">
	<c:choose>
		<c:when test="${!i301Data.exist}">
			<c:set var="bgcolor" value="#E1E6EB" />
		</c:when>
		<c:when test="${i301Data.registered}">
			<c:set var="bgcolor" value="#FFAD8C" />
		</c:when>
		<c:when test="${!i301Data.registered}">
			<c:set var="bgcolor" value="#F4E5D6" />
			<input type="checkbox" name="univValueAtExam" value="<c:out value="${i301Data.univValue}" />" onclick="changeRegBtnState();">
		</c:when>
	</c:choose>
</td>
<td bgcolor="<c:out value="${bgcolor}" />" align="center">
	<c:if test="${i301Data.exist}">
		<span class="text12">
			<c:if test="${isJudgable}">
				<a href="javascript:setUniqueKey('<c:out value="${i301Data.uniqueKey}" />');setExamInfo('<c:out value="${i301Bean.examYear}" />', '<c:out value="${i301Bean.examCd}" />');setJudgeNo('1');<%--[2] add--%>submitDetail();">詳細</a>
			</c:if>
		</span></td>
	</c:if>
<td bgcolor="<c:out value="${bgcolor}" />">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12"><c:out value="${i301Data.univName_Abbr}" /></span></td>
</tr>
</table>
</td>
<td bgcolor="<c:out value="${bgcolor}" />">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12"><c:out value="${i301Data.facultyName_Abbr}" /></span></td>
</tr>
</table>
</td>
<td bgcolor="<c:out value="${bgcolor}" />">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12"><c:out value="${i301Data.deptName_Abbr}" /></span></td>
</tr>
</table>
</td>
<td bgcolor="<c:out value="${bgcolor}" />" align="center"><span class="text12"><c:out value="${i301Data.prefName}" /></span></td>
<td bgcolor="<c:out value="${bgcolor}" />" align="center"><span class="text12"><c:out value="${i301Data.markGRating}" /></span></td>
<td bgcolor="<c:out value="${bgcolor}" />" align="center"><span class="text12"><c:out value="${i301Data.discriptionGRating}" /></span></td>
<td bgcolor="<c:out value="${bgcolor}" />" align="center"><span class="text12"><c:out value="${i301Data.ratingPoint}" /></span></td>
<td bgcolor="<c:out value="${bgcolor}" />" align="center"><span class="text12"><c:out value="${i301Data.totalRating}" /></span></td>
<td bgcolor="<c:out value="${bgcolor}" />" align="center">
<table border="0" cellpadding="4" cellspacing="0" width="50">
<tr>
<td nowrap><span class="text12"><c:out value="${i301Data.allotPntRate1}" />:<c:out value="${i301Data.allotPntRate2}" /></span></td>
</tr>
</table>
</td>
</tr>
</c:forEach>
</table>

<%--模試に記入された大学無しメッセージ[4] 050527 K.Kondo--%>
<!--模試に記入された大学無し表示-->
<div id = 'noExamDisp'>
<table bgcolor="#F4E5D6" border="0" cellpadding="2" cellspacing="2" width="832" height="50">
<tr>
<td align="center"><span class="text14">模試で記入された大学がありません。</span></td>
</tr>
</table>
</div>
<!--/模試に記入された大学無し表示>

<!--注意-->
<table border="0" cellpadding="0" cellspacing="2" width="832">
<tr>
<td align="right"><span class="text12">※<img src="./shared_lib/img/parts/color_box_03.gif" width="27" height="11" border="0" alt="□" hspace="5" align="absmiddle">は登録済の大学</span></td>
</tr>
</table>
<!--/注意-->
</td>
</tr>
</table>
<!--/リスト-->

<!---->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="20"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="828"><b class="text16" style="color:#657681;">判定履歴</b></td>
</tr>
</table>
<!--/-->

<!--リスト-->
<table border="0" cellpadding="0" cellspacing="0" width="852">
<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
<td width="832">

<table border="0" cellpadding="2" cellspacing="2" width="832">
<tr>
<td colspan="11" width="100%">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><span class="text12"><b>判定対象模試：</b><c:out value="${i301Bean.examName}" /></span></td>
</tr>
</table>
</td>
</tr>

<tr>
<td colspan="12" width="100%" bgcolor="#93A3AD">
	<table border="0" cellpadding="3" cellspacing="0">
	<tr height="27">
	<td><input type="checkbox" name="historyCtrl1" onclick="util.checkAllElements(this.checked, 'univValueAtJudge');changeRegBtnState();document.forms[0].historyCtrl2.checked=this.checked;"></td>
	<td><b class="text12" style="color:#FFF;">登録対象をすべて選択　　</b></td>
	<td><input type="checkbox" name="deleteCtrl1" onclick="util.checkAllElements(this.checked, 'univDeleteValue');changeDelBtnState();document.forms[0].deleteCtrl2.checked=this.checked;"></td>
	<td><b class="text12" style="color:#FFF;">削除対象をすべて選択　　（チェックを外すとすべて解除）</b></td>
	</tr>
	</table>
</td>
</tr>
<!--項目-->
<tr bgcolor="#CDD7DD" align="center">
<td rowspan="2" width="7%"><span class="text12">受験予定<br>登録</span></td>
<td rowspan="2" width="5%"><span class="text12">&nbsp;</span></td>
<td rowspan="2" width="16%"><span class="text12">大学</span></td>
<td rowspan="2" width="13%"><span class="text12">学部</span></td>
<td rowspan="2" width="13%"><span class="text12">学科</span></td>
<td rowspan="2" width="6%"><span class="text12">所在地</span></td>
<td rowspan="2" width="6%"><span class="text12">センター</span></td>
<td rowspan="2" width="6%"><span class="text12">2次</span></td>
<td colspan="2" width="12%"><span class="text12">総合</span></td>
<td rowspan="2" width="9%"><span class="text12">配点比率</span></td>
<td rowspan="2" width="7%"><span class="text12">判定履歴<br>削除</span></td>
</tr>
<tr bgcolor="#CDD7DD" align="center">
<td width="6%"><span class="text12">ポイント</span></td>
<td width="6%"><span class="text12">評価</span></td>
</tr>
<!--/項目-->
<!--カレントset-->
<c:forEach var="i301Data" items="${i301Bean.i301DatasAtJudge}" varStatus="status">
<tr>
<td bgcolor="#E1E6EB" align="center">
	<c:choose>
		<c:when test="${!i301Data.exist}">
			<c:set var="bgcolor" value="#E1E6EB" />
		</c:when>
		<c:when test="${i301Data.registered}">
			<c:set var="bgcolor" value="#FFAD8C" />
		</c:when>
		<c:when test="${!i301Data.registered}">
			<c:set var="bgcolor" value="#F4E5D6" />
			<input type="checkbox" name="univValueAtJudge" value="<c:out value="${i301Data.univValue}" />" onclick="changeRegBtnState();">
		</c:when>
	</c:choose>
</td>
<td bgcolor="<c:out value="${bgcolor}" />" align="center">
	<c:if test="${i301Data.exist}">
		<span class="text12"><a href="javascript:setUniqueKey('<c:out value="${i301Data.uniqueKey}" />');setExamInfo('<c:out value="${i301Bean.examYear}" />', '<c:out value="${i301Bean.examCd}" />');setJudgeNo('2');<%--[2] add--%>submitDetail();">詳細</a></span>
	</c:if>
</td>
<td bgcolor="<c:out value="${bgcolor}" />">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12"><c:out value="${i301Data.univName_Abbr}" /></span></td>
</tr>
</table>
</td>
<td bgcolor="<c:out value="${bgcolor}" />">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12"><c:out value="${i301Data.facultyName_Abbr}" /></span></td>
</tr>
</table>
</td>
<td bgcolor="<c:out value="${bgcolor}" />">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12"><c:out value="${i301Data.deptName_Abbr}" /></span></td>
</tr>
</table>
</td>
<td bgcolor="<c:out value="${bgcolor}" />" align="center"><span class="text12"><c:out value="${i301Data.prefName}" /></span></td>
<td bgcolor="<c:out value="${bgcolor}" />" align="center"><span class="text12"><c:out value="${i301Data.markGRating}" /></span></td>
<td bgcolor="<c:out value="${bgcolor}" />" align="center"><span class="text12"><c:out value="${i301Data.discriptionGRating}" /></span></td>
<td bgcolor="<c:out value="${bgcolor}" />" align="center"><span class="text12"><c:out value="${i301Data.ratingPoint}" /></span></td>
<td bgcolor="<c:out value="${bgcolor}" />" align="center"><span class="text12"><c:out value="${i301Data.totalRating}" /></span></td>
<td bgcolor="<c:out value="${bgcolor}" />" align="center">
<table border="0" cellpadding="4" cellspacing="0" width="50">
<tr>
<td nowrap><span class="text12"><c:out value="${i301Data.allotPntRate1}" />:<c:out value="${i301Data.allotPntRate2}" /></span></td>
</tr>
</table>
</td>
<td bgcolor="#E1E6EB" align="center">
	<input type="checkbox" name="univDeleteValue" value="<c:out value="${i301Data.univValue}" />" onclick="changeDelBtnState();">
</td>
</tr>
</c:forEach>

<tr>
<td colspan="12" width="100%" bgcolor="#93A3AD">
	<table border="0" cellpadding="3" cellspacing="0">
	<tr height="27">
	<td><input type="checkbox" name="historyCtrl2" onclick="util.checkAllElements(this.checked, 'univValueAtJudge');changeRegBtnState();document.forms[0].historyCtrl1.checked=this.checked;"></td>
	<td><b class="text12" style="color:#FFF;">登録対象をすべて選択　　</b></td>
	<td><input type="checkbox" name="deleteCtrl2" onclick="util.checkAllElements(this.checked, 'univDeleteValue');changeDelBtnState();;document.forms[0].deleteCtrl1.checked=this.checked;"></td>
	<td><b class="text12" style="color:#FFF;">削除対象をすべて選択　　（チェックを外すとすべて解除）</b></td>
	</tr>
	</table>
</td>
</tr>

</table>

<%--判定履歴無しメッセージ 050527[4] K.Kondo--%>
<!--判定履歴無し表示-->
<div id = 'noHistoryDisp'>
<table bgcolor="#F4E5D6" border="0" cellpadding="2" cellspacing="2" width="832" height="50">
<tr>
<td align="center"><span class="text14">判定履歴がありません、志望校判定を行って下さい。</span></td>
</tr>
</table>
</div>
<!--/判定履歴無し表示-->

<!--注意-->
<table border="0" cellpadding="0" cellspacing="2" width="832">
<tr>
<td align="right"><span class="text12">※<img src="./shared_lib/img/parts/color_box_03.gif" width="27" height="11" border="0" alt="□" hspace="5" align="absmiddle">は登録済の大学</span></td>
</tr>
</table>
<!--/注意-->
</td>
</tr>
</table>
<!--/リスト-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr valign="top">
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="3" border="0" alt=""><br></td>
<td width="828" height="19" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="848">
<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="3" border="0" alt=""><br></td>
<td width="828">
<table border="0" cellpadding="0" cellspacing="0" width="828">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="828">
<tr valign="top">
<td width="826" bgcolor="#FBD49F">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
	<td align="left"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><input type="button" name="regJudeUnivButton" value="受験予定大学に登録" class="text12" style="width:189px;" onclick="registerFromJudge('i301')" disabled></td>
	<td align="right"><input type="button" name="delJudeUnivButton" value="判定履歴から削除" class="text12" style="width:189px;" onclick="deleteFromJudge('i301')" disabled><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""></td>
	</tr>
	</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ボタン-->





</div>
<!--/受験予定大学表示！-->






<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/個人手帳-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--ケイコさん-->
<div id="judge_button">
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="155"><img src="./shared_lib/img/parts/sp.gif" width="155" height="1" border="0" alt=""><br></td>
<td width="524">

<table border="0" cellpadding="0" cellspacing="0" width="524">
<tr valign="top">
<td width="72" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="442">

<table border="0" cellpadding="0" cellspacing="0" width="442">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="418" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="418" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="418" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="418" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="442">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="15" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="15" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="45" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="428" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="428">
<tr>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="498" align="center">
<b class="text14">新しく受験予定大学を検索する場合は、志望大学判定へ！</b>
<div style="margin-top:7px;"><a href="javascript:submitForm('i201')"><img src="./shared_lib/img/btn/daigaku.gif" width="156" height="37" border="0" alt="志望大学判定"></a></div></td>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="442">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="418" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="418" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="418" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="418" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>

</td>
<td width="229"><img src="./shared_lib/img/parts/sp.gif" width="229" height="2" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/ケイコさん-->


</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</div>

</body>
</html>

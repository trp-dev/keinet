<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／利用者管理</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/DOMUtil.js"></script>
<script type="text/javascript" src="./js/Browser.js"></script>
<script type="text/javascript" src="./js/SwitchLayer.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<%@ include file="/jsp/script/dialog_close.jsp" %>

var confirmDialogPattern = "";
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	<%-- レイヤ操作クラス --%>
	var sw = new SwitchLayer();
	<%-- フォームユーティリティ --%>
	var util = new FormUtil();

	<%-- 年度別の学年リストとテーブルIDリスト --%>
	var gradeList = new Array();
	var tableList = new Array();
	<c:forEach var="year" items="${ ClassBean.yearList }">
		gradeList["<c:out value="${year}" />"] = new Array(
		<c:forEach var="grade" items="${ ClassBean.classYearMap[year] }" varStatus="status">
			"<c:out value="${grade.grade}" />"<c:if test="${ not status.last }">,</c:if>
		</c:forEach>
		);
		<c:forEach var="grade" items="${ ClassBean.classYearMap[year] }" varStatus="status">
			tableList[tableList.length] = "Table-<c:out value="${year}" />-<c:out value="${grade.grade}" />";
		</c:forEach>
	</c:forEach>

	function submitForm() {

		// 入力チェック
		if (util.getCheckedValue(document.forms[0].mode) == "2"
				&& util.countChecked(document.forms[0].classes) == 0) {

			alert("<kn:message id="u007a" />");
			return;
		}

		document.forms[0].submit();
	}

	function windowClose() {
		// 変更がなければそのまま閉じる
		if (document.forms[0].changed.value == 0) {
			window.close();

		// 変更があるなら確認ダイアログを表示
		} else {

//			var arg = new Array("<kn:message id="cm011c" />", "終了", "登録して終了", "キャンセル");
//			returnValue = showModalDialog(
//					"<c:url value="Dialog" />",
//					arg,
//					"status:no;help:no;scroll:no;dialogWidth:360px;dialogHeight:160px");
			// 閉じる
//			if (returnValue == 1) {
//				window.close();

			// 設定して閉じる
//			} else if (returnValue == 2) {
//				submitForm();
			// キャンセル（デフォルト）
//			} else {
//			}

			<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
			var dialogMsg = "<kn:message id="cm011c" />";

			// ダイアログ画面のパラメータ指定
			// メッセージ
			document.getElementById("dialogMsg").innerHTML = dialogMsg;
			// ボタンの文言
			document.getElementById("dialogBtn1").value = "終了";
			document.getElementById("dialogBtn2").value = "登録して終了";
			document.getElementById("dialogBtn3").value = "キャンセル";

			confirmDialogPattern = "u003";

			// サイズの指定
			$( '#confirmDialogDiv' ).dialog( { width: 410, height: 200 } );

		    // 確認ダイアログ　オープン
		    $(confirmDialogObj).dialog('open');
			<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
		}
	}

	function formChanged() {
		document.forms[0].changed.value = "1";
	}

	function changeYear() {
		initGrade();
	}

	<%-- 表示するテーブルを切り替える --%>
	function selectTable() {

		for (var i=0; i<tableList.length; i++) {
			sw.hideLayer(tableList[i]);
		}

		if (util.getCheckedValue(document.forms[0].mode) == "1") return;

		sw.showLayer( getCurrentTableId());
	}

	function getCurrentTableId() {
		return  "Table-"
				+ document.forms[0].year.value
				+ "-" + document.forms[0].grade.value;
	}

	<%-- 学年のセレクトボックスを初期化する --%>
	function initGrade() {

		var options = document.forms[0].grade.options;
		options.length = 0;
		var e = gradeList[document.forms[0].year.value];
		for (var i=0; i<e.length; i++) {
			options[i] = new Option(e[i] + "年", e[i]);
		}

		selectTable();
	}

	function changeMode(value) {

		if (value == "1") {
			sw.getLayer("LeftCell").style.backgroundColor = "#FFAD8C";
			sw.getLayer("RightCell").style.backgroundColor = "#FFFFFF";
			selectTable();
			sw.hideLayer("ClassTable");
		} else {
			sw.getLayer("RightCell").style.backgroundColor = "#FFAD8C";
			sw.getLayer("LeftCell").style.backgroundColor = "#FFFFFF";
			sw.showLayer("ClassTable");
			selectTable();
		}
	}

	function selectCheckbox(flag) {

		var e = document.forms[0].classes;

		// なし
		if (!e) {
			return;

		// 複数
		} else if (e.length) {

			var current = document.forms[0].year.value
					+ "," + document.forms[0].grade.value;
			for (var i = 0; i < e.length; i++) {
				if (current == e[i].value.substring(0, current.length)) {
					e[i].checked = flag;
				}
			}

		// 単数
		} else {
			e.checked = flag;
		}
	}

	<%-- ページ初期化処理 --%>
	function init() {
		startTimer();
		initGrade();
		changeMode(util.getCheckedValue(document.forms[0].mode));
	}

// -->
</script>
</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="U003Servlet" />" method="POST">
<input type="hidden" name="forward" value="u003">
<input type="hidden" name="backward" value="u003">
<input type="hidden" name="changed" value="0">


<input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value=""><input type="hidden" name="charge" value="">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="554">

<table border="0" cellpadding="0" cellspacing="0" width="554">
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="538" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">担当クラスの変更</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<div style="margin-top:8px;">
<table border="0" cellpadding="2" cellspacing="0" width="570">
<tr valign="top">
<td><span class="text14">利用者が担当クラスとして設定可能とするクラスにチェックをして、登録してください。設定を解除したい場合はチェックをはずして同様の操作をしてください。</span></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="1" width="566" bgcolor="#8CA9BB">
<tr>
<td bgcolor="#FFFFFF" width="564">
	<table border="0" cellspacing="2" cellpadding="3" width="564">
	<tr>
	<td align="center" bgcolor="#E1E6EB" width="40"><input type="radio" name="mode" value="1" onclick="changeMode(this.value);formChanged();"<c:if test="${ empty LoginUserData.chargeClassList }"> checked</c:if>></td>
	<td width="225" id="LeftCell"><span class="text12">制限なし</td>
	<td align="center" bgcolor="#E1E6EB" width="40"><input type="radio" name="mode" value="2" onclick="changeMode(this.value);formChanged()"<c:if test="${ not empty LoginUserData.chargeClassList }"> checked</c:if>></td>
	<td width="225" id="RightCell"><span class="text12">クラス指定</span></td>
	</table>
</td>
</tr>
</table>

<div id="ClassTable" style="position:absolute;visibility:hidden;top:0px;left:0px">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="5" cellspacing="1" width="566" bgcolor="#8CA9BB">
	<tr>
		<td bgcolor="#FFFFFF">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""></td>
<td>

<!--対象学年-->
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="60"><span class="text12">対象年度：</span></td>
<td width="80">
<select name="year" class="text12" style="width:60px;" onChange="changeYear()">
<c:forEach var="year" items="${ClassBean.yearList}">
	<option value="<c:out value="${year}" />"><c:out value="${year}" /></option>
</c:forEach>
</select>
</td>
</tr>
<tr>
<td colspan="2"><img src="./shared_lib/img/parts/sp.gif" width="0" height="5" border="0" alt=""></td>
<tr>
<td width="60"><span class="text12">対象学年：</span></td>
<td width="80">
<select name="grade" class="text12" style="width:60px;" onChange="selectTable()"></select>
</td>
</tr>
</table>
<!--/対象学年-->

<!--選択ボタン-->
<table border="0" cellpadding="5" cellspacing="2">
<tr>
	<td><input class="text12" type="button" value="全て選択" style="width:120px;" onclick="selectCheckbox(true)"></td>
	<td><input class="text12" type="button" value="全て解除" style="width:120px;" onclick="selectCheckbox(false)"></td>
</tr>
</table>
<!--/選択ボタン-->

</td>
</tr>
</table>

<c:if test="${ not empty ClassBean.classYearMap }">
<c:forEach var="year" items="${ ClassBean.yearList }">
	<c:forEach var="grade" items="${ ClassBean.classYearMap[year] }">
		<div id="Table-<c:out value="${ year }" />-<c:out value="${ grade.grade }" />" style="position:absolute;visibility:hidden;top:0px;left:0px">
		<table border="0" cellpadding="2" cellspacing="2" width="554">
		<tr>
		<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
		<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">&nbsp;</span></td>
		<td width="8%" bgcolor="#CDD7DD" align="center"><span class="text12">学年</span></td>
		<td width="18%" bgcolor="#CDD7DD">
		<table border="0" cellpadding="4" cellspacing="0">
		<tr>
		<td><span class="text12">クラス名</span></td>
		</tr>
		</table>
		</td>
		<td width="11%" bgcolor="#CDD7DD">
		<table border="0" cellpadding="4" cellspacing="0">
		<tr>
		<td><span class="text12">人数</span></td>
		</tr>
		</table>
		</td>
		<td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
		<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
		<td width="8%" bgcolor="#CDD7DD" align="center"><span class="text12">&nbsp;</span></td>
		<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">学年</span></td>
		<td width="19%" bgcolor="#CDD7DD">
		<table border="0" cellpadding="4" cellspacing="0">
		<tr>
		<td><span class="text12">クラス名</span></td>
		</tr>
		</table>
		</td>
		<td width="11%" bgcolor="#CDD7DD">
		<table border="0" cellpadding="4" cellspacing="0">
		<tr>
		<td><span class="text12">人数</span></td>
		</tr>
		</table>
		</td>
		</tr>

		<c:forEach begin="0" end="${ grade.halfSize - 1 }" varStatus="status">
			<c:set var="leftClass" value="${ grade.classList[status.index] }" />
			<c:set var="leftKey" value="${ leftClass.year },${ leftClass.grade },${ leftClass.key }" />
			<c:choose>
				<c:when test="${ CheckedMap[leftKey] }">
					<c:set var="leftColor" value="#FFAD8C" />
				</c:when>
				<c:otherwise>
					<c:set var="leftColor" value="#F4E5D6" />
				</c:otherwise>
			</c:choose>
			<tr height="27">
			<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ status.count }" /></span></td>
			<td bgcolor="<c:out value="${ leftColor }" />" align="center"><input type="checkbox" name="classes" value="<c:out value="${ leftKey }" />" onclick="formChanged();"<c:if test="${ CheckedMap[leftKey] }"> checked</c:if>></td>
			<td bgcolor="<c:out value="${ leftColor }" />" align="center"><span class="text12"><c:out value="${ leftClass.grade }" />年</span></td>
			<td bgcolor="<c:out value="${ leftColor }" />">
			<table border="0" cellpadding="4" cellspacing="0">
			<tr>
			<td><span class="text12"><c:out value="${ leftClass.className }" /></span></td>
			</tr>
			</table>
			</td>
			<td bgcolor="<c:out value="${ leftColor }" />" align="right" nowrap>
			<table border="0" cellpadding="4" cellspacing="0">
			<tr>
			<td><span class="text12"><c:out value="${ leftClass.examinees }" />人</span></td>
			</tr>
			</table>
			</td>
			<td bgcolor="#FFFFFF">&nbsp;</td>
			<c:choose>
				<c:when test="${ grade.size % 2 == 0 || not status.last }">
					<c:set var="rightClass" value="${ grade.classList[status.index + grade.halfSize ] }" />
					<c:set var="rightKey" value="${ rightClass.year },${ rightClass.grade },${ rightClass.key }" />
					<c:choose>
						<c:when test="${ CheckedMap[rightKey] }">
							<c:set var="rightColor" value="#FFAD8C" />
						</c:when>
						<c:otherwise>
							<c:set var="rightColor" value="#F4E5D6" />
						</c:otherwise>
					</c:choose>
					<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ status.count + grade.halfSize }" /></span></td>
					<td bgcolor="<c:out value="${ rightColor }" />" align="center"><input type="checkbox" name="classes" value="<c:out value="${ rightKey }" />" onclick="formChanged();"<c:if test="${ CheckedMap[rightKey] }"> checked</c:if>></td>
					<td bgcolor="<c:out value="${ rightColor }" />" align="center"><span class="text12"><c:out value="${ rightClass.grade }" />年</span></td>
					<td bgcolor="<c:out value="${ rightColor }" />">
					<table border="0" cellpadding="4" cellspacing="0">
					<tr>
					<td><span class="text12"><c:out value="${ rightClass.className }" /></span></td>
					</tr>
					</table>
					</td>
					<td bgcolor="<c:out value="${ rightColor }" />" align="right" nowrap>
					<table border="0" cellpadding="4" cellspacing="0">
					<tr>
					<td><span class="text12"><c:out value="${ rightClass.examinees }" />人</span></td>
					</tr>
					</table>
					</td>
				</c:when>
				<c:otherwise>
					<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ status.count + grade.halfSize }" /></span></td>
					<td bgcolor="#F4E5D6" align="center"></td>
					<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
					<td bgcolor="#F4E5D6">
					<table border="0" cellpadding="4" cellspacing="0">
					<tr>
					<td><span class="text12"></span></td>
					</tr>
					</table>
					</td>
					<td bgcolor="#F4E5D6" align="right" nowrap>
					<table border="0" cellpadding="4" cellspacing="0">
					<tr>
					<td><span class="text12"></span></td>
					</tr>
					</table>
					</td>
				</c:otherwise>
			</c:choose>
			</tr>
		</c:forEach>
		</table>
		</div>
	</c:forEach>
</c:forEach>
</c:if>

	</tr>
</table>
</div>

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="10">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="566" align="center">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="566">
<tr>
<td bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><input type="button" value="&nbsp;キャンセル&nbsp;" class="text12" style="width:120px;" onClick="windowClose()"></td>
		<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
		<td><input type="button" value="&nbsp;登録して閉じる&nbsp;" class="text12" style="width:120px;" onClick="submitForm()"></td>
	</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td align="center"><span class="text12">※キャンセルを押すと、保存せずにウィンドウを閉じます。</span></td>
</tr>
</table>
</div>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</body>
</html>

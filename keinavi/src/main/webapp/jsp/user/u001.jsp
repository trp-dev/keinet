<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>
<title>Kei-Navi／利用者管理</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>
<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD START --%>
<script type="text/javascript" src="./js/jquery.blockUI.js"></script>
<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<script type="text/javascript">
<!--
	<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD START --%>
	<%@ include file="/jsp/script/download.jsp" %>
	<%@ include file="/jsp/script/submit_top.jsp" %>
	<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/printDialog_close.jsp" %>
	 var printDialogPattern = "";
	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

	<%-- フォーム操作クラス --%>
	var util = new FormUtil();

		function submitPrint() {

			if (util.countChecked(document.forms[0].loginId) == 0) {
				alert("<kn:message id="u001a" />");
				return;
			}

			<%-- 2016/01/08 QQ)Nishiyama 大規模改修 UPD START --%>
			//var returnValue = showModalDialog(
			//		"<c:url value="PrintDialog" />",
			//		null,
			//		"status:no;help:no;scroll:no;dialogWidth:665px;dialogHeight:450px");

			//switch (returnValue) {
			//	case 1: document.forms[0].printFlag.value = 8; break;
			//	case 2: document.forms[0].printFlag.value = 2; break;
			//	default: return;
			//}

			//document.forms[0].forward.value = "sheet";
			//document.forms[0].submit();

			printDialogPattern = "u001";

			<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 UPD START --%>
			// 印刷確認ダイアログ　オープン
			//$(printDialogObj).dialog('open');
			download(1);
			<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 UPD END   --%>
			<%-- 2016/01/08 QQ)Nishiyama 大規模改修 UPD END   --%>
		}

	<%-- 利用者新規 --%>
	function submitAdd() {
		document.forms[0].targetLoginId.value = "";
		submitMenu('u002');
	}

	<%-- 利用者編集 --%>
		function submitEdit(id) {
			document.forms[0].targetLoginId.value = id;
			submitMenu('u002');
		}

	<%-- 利用者削除 --%>
	function submitDelete(id) {
		if (confirm("<kn:message id="u001c" />")) {
			document.forms[0].actionMode.value = "1";
			document.forms[0].targetLoginId.value = id;
			submitMenu('u001');
		}
	}

	<%-- パスワード初期化 --%>
	function submitInitPassword(id) {
		if (confirm("<kn:message id="u002c" />")) {
			document.forms[0].actionMode.value = "2";
			document.forms[0].targetLoginId.value = id;
			submitMenu('u001');
		}
	}

	function init() {
		startTimer();
	}

// -->
</script>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="U001Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="u001">
<input type="hidden" name="actionMode" value="">
<input type="hidden" name="targetLoginId" value="">
<input type="hidden" name="save" value="0">
<input type="hidden" name="printFlag" value="">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->

<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
	<tr valign="top">
		<td width="12">
			<img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br>
		</td>
		<td width="8" background="./shared_lib/img/parts/com_bk_l.gif">
			<img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br>
		</td>
		<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
	<tr valign="top">
		<td width="30">
			<img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br>
		</td>
		<td width="908">


<!--タイトル-->
<div style="margin-top:23px;">
	<table border="0" cellpadding="0" cellspacing="0" width="908">
		<tr>
			<td width="174">
				<img src="./user/img/ttl_user.gif" width="128" height="24" border="0" alt="利用者管理"><br>
			</td>
			<td width="1" bgcolor="#657681">
				<img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br>
			</td>
			<td width="10">
				<img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br>
			</td>
			<td width="723">
				<img src="./user/img/ttl_user_read.gif" width="364" height="16" border="0" alt="利用者情報を一元的に管理することができます。"><br>
			</td>
		</tr>
	</table>
</div>
<!--/タイトル-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
		<td width="100%">
			<img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt="">
		</td>
	</tr>
</table>
<!--/spacer-->


<!--タイトルバーkkk-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
	<tr valign="top">
		<td width="2" bgcolor="#8CA9BB">
			<img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br>
		</td>
		<td width="892">
			<table border="0" cellpadding="0" cellspacing="0" width="892">
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#8CA9BB">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="2" border="0" alt=""><br>
					</td>
				</tr>
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="3" border="0" alt=""><br>
					</td>
				</tr>
				<tr>
					<td width="5" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br>
					</td>
					<td width="7" bgcolor="#FF8F0E">
						<img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br>
					</td>
					<td width="4" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br>
					</td>
					<td width="576" bgcolor="#758A98">
						<img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt="">
						<b class="text16" style="color:#FFFFFF;">利用者一覧</b>
					</td>
					<td width="300" bgcolor="#758A98" align="right"><br></td>
				</tr>
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="3" border="0" alt=""><br>
					</td>
				</tr>
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#8CA9BB">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="2" border="0" alt=""><br>
					</td>
				</tr>
			</table>
		</td>
		<td width="14">
			<img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/タイトルバーkkk-->



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
		<td width="100%">
			<img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt="">
		</td>
	</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="18"><br></td>
<td width="890">

<!--新規登録ボタンkkk-->
<table border="0" cellpadding="0" cellspacing="0" width="250">
	<td bgcolor="#8CA9BB">
		<table border="0" cellpadding="6" cellspacing="1" width="250">
			<tr>
				<td bgcolor="#FBD49F" align="center">
						<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
						<%-- <input type="button" value="新規登録" class="text12" style="width:140px;" onclick="submitAdd()"> --%>
						<input type="button" value="新規登録" class="text12" style="width:140px;" onclick="submitAdd()"<c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>>
						<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
				</td>
			</tr>
		</table>
	</td>
</table>
<!--/新規登録ボタンkkk-->

<!--現在の利用者はテキストkkk-->
<table border="0" cellpadding="0" cellspacing="0" width="890">
	<tr valign="top">
		<td bgcolor="#FFFFFF">
			<table border="0" cellpadding="6" cellspacing="1" width="880">
				<tr valign="top">
					<td bgcolor="#FFFFFF" align="left">
						<span class="text14-hh">現在の利用者は以下の通りです。先頭に管理者ＩＤを表示しています。「保存」をクリックすると、指定したIDの利用者登録通知書が作成できます。<br></span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!--/現在の利用者はテキストkkk-->

<table border="0" cellpadding="0" cellspacing="5" width="890">
	<tr>
		<td width="110"><input type="button" value="全て選択" style="width:100px" onclick="util.changeChecked(document.forms[0].loginId, true)"></td>
		<td width="110"><input type="button" value="全て解除" style="width:100px" onclick="util.changeChecked(document.forms[0].loginId, false)"></td>
		<td width="760" align="right"><span class="text14-hh">○：可　　▲：集計データのみ可　　×：不可</span></td>
	</tr>
</table>

</td>
</tr>
<table>

<!--ユーザー 一覧-->
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
<%--
<TABLE border="0" cellspacing="2" cellpadding="4">
	<TR bgcolor="#E1E6EB">
		<TH width="35" nowrap rowspan="3"><b class="text12">印刷<br>指定</b></TH>
		<TH width="70" nowrap rowspan="3"><b class="text12">利用者ID</b></TH>
		<TH width="160" nowrap rowspan="3" align="center"><b class="text12">利用者名</b></TH>
		<TH width="70" nowrap rowspan="3"><b class="text12">初期PW</b></TH>
		<TH width="35" nowrap rowspan="3"><b class="text12">初期<BR>PW<br>変更</b></TH>
		<TH width="35" nowrap rowspan="3"><b class="text12">メンテ<BR>ナンス<BR>権限</b></TH>
	 	<TH height="40" nowrap colspan="4"><b class="text12">機能権限</b></TH>
		<TH width="210" nowrap rowspan="3"><b class="text12">担当クラス</b></TH>
		<TH width="72" nowrap rowspan="3" colspan="2"><span class="text12"><br></span></TH>
	</TR>
	<TR bgcolor="#E1E6EB">
		<TH width="70" rowspan="2" width="40"><b class="text12">Kei-<br>Navi</b></TH>
		<TH height="40" nowrap colspan="2"><b class="text12">校内成績処理</b></TH>
		<TH width="70" rowspan="2" width="50"><b class="text12">答案閲覧<br>システム</b></TH>
	</TR>
	<TR bgcolor="#E1E6EB">
		<TH width="35"><b class="text12">成績<BR>分析</b></TH>
		<TH width="35" height="80"><b class="text12">入試<BR>結果<BR>調査</b></TH>
	</TR>
--%>

<TABLE border="0" cellspacing="2" cellpadding="4"  style="word-break:break-all;">
	<TR bgcolor="#E1E6EB">
		<TH width="35" nowrap rowspan="2"><b class="text12">指定</b></TH>
		<TH width="70" nowrap rowspan="2"><b class="text12">利用者ID</b></TH>
		<TH width="160" nowrap rowspan="2" align="center"><b class="text12">利用者名</b></TH>
		<TH width="70" nowrap rowspan="2"><b class="text12">初期PW</b></TH>
		<TH width="35" nowrap rowspan="2"><b class="text12">初期<BR>PW<br>変更</b></TH>
		<TH width="35" nowrap rowspan="2"><b class="text12">メンテ<BR>ナンス<BR>権限</b></TH>
	 	<TH height="40" nowrap colspan="2"><b class="text12">機能権限</b></TH>
		<TH width="210" nowrap rowspan="2"><b class="text12">担当クラス</b></TH>
		<TH width="72" nowrap rowspan="2" colspan="2"><span class="text12"><br></span></TH>
	</TR>
	<TR bgcolor="#E1E6EB">
		<TH width="70" rowspan="1" width="40"><b class="text12">Kei-<br>Navi</b></TH>
		<TH width="70" rowspan="1" width="50"><b class="text12">答案閲覧<br>システム</b></TH>
	</TR>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END   --%>

	<c:forEach var="user" items="${ LoginUserBean.loginUserList }">
	<c:choose>
	<c:when test="${ user.manager }">
	<TR bgcolor="#F4E5D6">
		<TD width="35" height="40" align="center"><input type="checkbox" name="loginId" value="<c:out value="${ user.loginId }" />"<c:if test="${ LoginIdChecked[user.loginId] }"> checked</c:if>></TD>
		<TD width="70" align="center"><font color="Red"><b class="text12"><c:out value="${ user.loginId }" /></b></font></TD>
		<TD width="160"><font color="Red"><b class="text12">システム管理者</b></font></TD>
		<TD width="70" align="center"><font color="Red"><b class="text12"><c:out value="${ user.defaultPwd }" /></b></font></TD>
		<TD width="35" align="center"><font color="Red"><b class="text12"><c:choose><c:when test="${ user.changedPwd }">有</c:when><c:otherwise>無</c:otherwise></c:choose></b></font></TD>
		<TD width="35" align="center"><font color="Red"><b class="text12">○</b></font></TD>
		<%-- 2015/12/16 QQ)Hisakawa 大規模改修 DEL START --%>
		<%-- <TD width="35" align="center"><font color="Red"><b class="text12">○</b></font></TD>	--%>
		<%-- 2015/12/16 QQ)Hisakawa 大規模改修 DEL END   --%>
		<TD width="70" align="center"><font color="Red"><b class="text12">○</b></font></TD>
		<%-- 2015/12/16 QQ)Hisakawa 大規模改修 DEL START --%>
		<%-- <TD width="35" align="center"><font color="Red"><b class="text12">○</b></font></TD>	--%>
		<%-- <TD width="35" align="center"><font color="Red"><b class="text12">○</b></font></TD>	--%>
		<%-- <TD width="50" align="center"><font color="Red"><b class="text12">○</b></font></TD>	--%>
		<%-- 2015/12/16 QQ)Hisakawa 大規模改修 DEL END   --%>
		<TD width="70" align="center"><font color="Red"><b class="text12">○</b></font></TD>
		<TD width="210" align="center"><font color="Red"><b class="text12">制限なし</b></font></TD>

		<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
		<%-- <TD width="35" align="center"><a href="javascript:submitEdit('<c:out value="${ user.loginId }" />')"><span class="text12">編集</span></a></TD> --%>
		<c:choose>
		<c:when test="${ LoginSession.trial }">
			<TD width="35" align="center"><span class="text12"><font color="#2986B1"><u>編集</u></font></span></TD>
		</c:when>
		<c:otherwise>
			<TD width="35" align="center"><a href="javascript:submitEdit('<c:out value="${ user.loginId }" />')"><span class="text12">編集</span></a></TD>
		</c:otherwise>
		</c:choose>
		<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END   --%>

		<TD width="35" align="center"><br></TD>
	</TR>
	</c:when>
	<c:otherwise>
	<TR bgcolor="#F4E5D6">
		<TD width="35" height="40" rowspan="2" align="center"><input type="checkbox" name="loginId" value="<c:out value="${ user.loginId }" />"<c:if test="${ LoginIdChecked[user.loginId] }"> checked</c:if>></TD>
		<TD width="70" rowspan="2" align="center"><span class="text12"><c:out value="${ user.loginId }" /></span></TD>
		<TD width="160" rowspan="2"><span class="text12"><c:out value="${ user.loginName }" /></span></TD>
		<TD width="70" rowspan="2" align="center"><span class="text12"><c:out value="${ user.defaultPwd }" /></span></TD>
		<TD width="35" rowspan="2" align="center"><span class="text12"><c:choose><c:when test="${ user.changedPwd }">有</c:when><c:otherwise>無</c:otherwise></c:choose></span></TD>
		<TD width="35" rowspan="2" align="center"><span class="text12"><c:choose><c:when test="${ user.maintainer }">○</c:when><c:otherwise>×</c:otherwise></c:choose></span></TD>
		<%-- 2015/12/16 QQ)Hisakawa 大規模改修 DEL START --%>
		<%-- <TD width="35" rowspan="2" align="center"><span class="text12"><c:choose><c:when test="${ user.knFunctionFlag == 1 }">○</c:when><c:when test="${ user.knFunctionFlag == 2 }">▲</c:when><c:otherwise>×</c:otherwise></c:choose></span></TD>	--%>
		<%-- 2015/12/16 QQ)Hisakawa 大規模改修 DEL END   --%>
		<TD width="70" rowspan="2" align="center"><span class="text12"><c:choose><c:when test="${ user.knFunctionFlag == 1 }">○</c:when><c:when test="${ user.knFunctionFlag == 2 }">▲</c:when><c:otherwise>×</c:otherwise></c:choose></span></TD>
		<%-- 2015/12/16 QQ)Hisakawa 大規模改修 DEL START --%>
		<%-- <TD width="35" rowspan="2" align="center"><span class="text12"><c:choose><c:when test="${ user.scFunctionFlag == 1 }">○</c:when><c:when test="${ user.scFunctionFlag == 2 }">▲</c:when><c:otherwise>×</c:otherwise></c:choose></span></TD>	--%>
		<%-- <TD width="35" rowspan="2" align="center"><span class="text12"><c:choose><c:when test="${ user.eeFunctionFlag == 1 }">○</c:when><c:otherwise>×</c:otherwise></c:choose></span></TD>	--%>
		<%-- <TD width="50" rowspan="2" align="center"><span class="text12"><c:choose><c:when test="${ user.abFunctionFlag == 1 }">○</c:when><c:otherwise>×</c:otherwise></c:choose></span></TD>	--%>
		<%-- 2015/12/16 QQ)Hisakawa 大規模改修 DEL END   --%>
		<TD width="70" rowspan="2" align="center"><span class="text12"><c:choose><c:when test="${ user.abFunctionFlag == 1 }">○</c:when><c:otherwise>×</c:otherwise></c:choose></span></TD>
		<TD width="210" rowspan="2"<c:if test="${ empty user.chargeClassList }"> align="center"</c:if>><span class="text12"><kn:userClass /></span></TD>

		<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
		<%--
		<TD width="35" align="center"><a href="javascript:submitEdit('<c:out value="${ user.loginId }" />')"><span class="text12">編集</span></a></TD>
		<TD width="35" align="center"><a href="javascript:submitDelete('<c:out value="${ user.loginId }" />')"><span class="text12">削除</span></a></TD>
		--%>
 		<c:choose>
		<c:when test="${ LoginSession.trial }">
			<TD width="35" align="center"><span class="text12"><font color="#2986B1"><u>編集</u></font></span></TD>
			<TD width="35" align="center"><span class="text12"><font color="#2986B1"><u>削除</u></font></span></TD>
		</c:when>
		<c:otherwise>
			<TD width="35" align="center"><a href="javascript:submitEdit('<c:out value="${ user.loginId }" />')"><span class="text12">編集</span></a></TD>
			<TD width="35" align="center"><a href="javascript:submitDelete('<c:out value="${ user.loginId }" />')"><span class="text12">削除</span></a></TD>
		</c:otherwise>
		</c:choose>
		<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END   --%>
	</TR>
	<TR bgcolor="#F4E5D6">
		<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
		<%-- <TD colspan="2" align="center"><a href="javascript:submitInitPassword('<c:out value="${ user.loginId }" />')"><span class="text12">PW初期化</span></a></TD> --%>
		<c:choose>
		<c:when test="${ LoginSession.trial }">
			<TD colspan="2" align="center"><span class="text12"><font color="#2986B1"><u>PW初期化</u></font></span></TD>
		</c:when>
		<c:otherwise>
			<TD colspan="2" align="center"><a href="javascript:submitInitPassword('<c:out value="${ user.loginId }" />')"><span class="text12">PW初期化</span></a></TD>
		</c:otherwise>
		</c:choose>
		<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END   --%>
	</TR>
	</c:otherwise>
	</c:choose>
	</c:forEach>
</TABLE>
<!--/ユーザー 一覧-->

<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="18"><br></td>
<td width="890">

<table border="0" cellpadding="0" cellspacing="5" width="890">
	<tr>
		<td width="110"><input type="button" value="全て選択" style="width:100px" onclick="util.changeChecked(document.forms[0].loginId, true)"></td>
		<td width="110"><input type="button" value="全て解除" style="width:100px" onclick="util.changeChecked(document.forms[0].loginId, false)"></td>
		<td width="760" align="right"><span class="text14-hh">○：可　　▲：集計データのみ可　　×：不可</span></td>
	</tr>
</table>

</td>
</tr>
<table>

<!--/印刷ボタン-->
<div style="text-align:center;margin-top:20px;width:908px">
		<c:if test="${ not LoginSession.trial }"><a href="javascript:submitPrint()"></c:if><img src="./shared_lib/img/btn/hozon_w.gif" width="114" height="34" border="0" alt="保存"></a><br>
</div>
<!--/印刷ボタン-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
	<tr>
		<td width="908" height="28" align="center">
			<img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br>
		</td>
	</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
	<tr valign="top">
		<td bgcolor="#8CA9BB">
			<table border="0" cellpadding="6" cellspacing="1" width="908">
				<tr valign="top">
					<td width="906" bgcolor="#FBD49F" align="center">
						<input type="button" value="利用者管理完了　（プロファイル選択画面へ）" class="text12" style="width:320px;" onclick="submitMenu('w002')">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
		<td width="100%">
			<img src="./shared_lib/img/parts/sp.gif" width="1" height="40" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/spacer-->

		</td>
		<td width="30">
			<img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/コンテンツ-->



		</td>
		<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif">
			<img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
	<tr valign="top">
		<td width="12">
			<img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br>
		</td>
		<td width="12">
			<img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br>
		</td>
		<td width="960" background="./shared_lib/img/parts/com_bk_d.gif">
			<img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br>
		</td>
		<td width="13">
			<img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/下部　ドロップシャドウ-->



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
	<tr valign="top">
		<td width="997">
			<img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/admin/footer.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

</form>
</body>
</html>

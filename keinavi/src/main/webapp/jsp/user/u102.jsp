<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi/利用者情報確認</title>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css">
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>

	<%-- 検査用クラス --%>
	var validator = new Validator();

	function submitChange() {

		var message = "";
		var form = document.forms[0];

		if (form.prePassword.value.length == 0) {
			message += "<kn:message id="u008a" />\n";
		}

		if (form.newPassword.value.length == 0) {
			message += "<kn:message id="u010a" />\n";

		} else if (form.newPassword.value == form.newPasswordConf.value) {
			if (form.newPassword.value.length < 4
					|| form.newPassword.value.length > 8
					|| !validator.isAlphanumeric(form.newPassword.value)) {
				message += "<kn:message id="u011a" />\n";
			}

		} else {
			message += "<kn:message id="u012a" />\n";
		}

		if (message != "") {
			alert(message);
			return;
		}

		form.actionMode.value = "1";
		submitMenu('u101');
	}

	function init() {
		startTimer();
		var message = "<c:out value="${ ErrorMessage }" />";
		if (message != "") alert(message);
	}

//-->
</script>
</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="U102Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="u102">
<input type="hidden" name="actionMode" value="">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="554">

<table border="0" cellpadding="0" cellspacing="0" width="554">
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="538" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt="">
<b class="text16" style="color:#FFFFFF;">パスワード変更</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<div style="margin:10px 0px 10px 20px;">
	<table border="0" cellpadding="2" cellspacing="0" width="570">
		<tr valign="top">
			<td><span class="text14">パスワード変更を行います。パスワードは半角英数字４文字以上８文字以下です。</span></td>
		</tr>
	</table>
</div>

<!--メイン枠-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="500">
<tr>
<td bgcolor="#8CA9BB">
	<table border="0" cellpadding="20" cellspacing="1" width="500">
	<tr>
	<td bgcolor="#FFFFFF" align="center">

<table border="0" cellpadding="2" cellspacing="3">
	<tr>
		<td width="120px"><b class="text12">利用者ID</b></td>
		<td width="30px"><b class="text12">:</b></td>
		<td width="120px" align="center" style="padding-right:40px"><span class="text12"><c:out value="${ LoginUserData.loginId }" /></span></td>
	</tr>
	<tr>
		<td width="120px"><b class="text12">現パスワード</b></td>
		<td width="30px"><b class="text12">:</b></td>
		<td width="120px" align="center" style="padding-right:40px"><input maxlength="8" type="password" name="prePassword" class="text12" style="width:120px;"></td>
	</tr>
	<tr>
		<td width="120px"><b class="text12">新パスワード</b></td>
		<td width="30px"><b class="text12">:</b></td>
		<td width="120px" align="center" style="padding-right:40px"><input maxlength="8" type="password" name="newPassword" class="text12" style="width:120px;"></td>
	</tr>
	<tr>
		<td width="120px"><b class="text12">新パスワード（確認）</b></td>
		<td width="30px"><b class="text12">:</b></td>
		<td width="120px" align="center" style="padding-right:40px"><input maxlength="8" type="password" name="newPasswordConf" class="text12" style="width:120px;"></td>
	</tr>
</table>

	</td>
	</tr>
	</table>
</td>
</tr>
</table>
</div>
<!--/メイン枠-->



<!--キャンセル・登録ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="500">
<tr>
<td bgcolor="#8CA9BB">
	<table border="0" cellpadding="6" cellspacing="1" width="500">
	<tr>
	<td bgcolor="#FBD49F" align="center">
		<table border="0" cellpadding="5" cellspacing="0">
		<tr>
		<td>
			<input type="button" value="キャンセル" class="text12" style="width:140px;" onclick="submitMenu('u101')">
		</td>
		<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""></td>
		<td>
			<input type="button" value="実行" class="text12" style="width:120px;" onclick="submitChange()">
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
</td>
</tr>
</table>
</div>
<!--/キャンセル・登録ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw.jsp" %>
<!--/FOOTER-->



</form>
</body>
</html>
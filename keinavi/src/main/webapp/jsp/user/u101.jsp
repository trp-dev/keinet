<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／利用者情報確認</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/Validator.js"></script>
<SCRIPT type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>

	function submitChange() {
		submitMenu('u102');
	}

	function init() {
		startTimer();
	}

//-->
</SCRIPT>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="U101Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="u101">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="554">

<table border="0" cellpadding="0" cellspacing="0" width="554">
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="538" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt="">
<b class="text16" style="color:#FFFFFF;">利用者情報確認</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<div style="margin:10px 0px 10px 20px;">
	<table border="0" cellpadding="2" cellspacing="0" width="570">
		<tr valign="top">
			<td><span class="text14">ご利用の利用者情報は以下のとおりです。</span></td>
		</tr>
	</table>
</div>

<table border="0" cellpadding="0" cellspacing="0" width="563">
<tr>
<td>

<!--メイン枠-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="563">
	<tr valign="top">
		<td bgcolor="#8CA9BB">
			<table border="0" cellpadding="0" cellspacing="1" width="563">
				<td bgcolor="#FFFFFF">
					<table border="0" cellpadding="0" cellspacing="0" width="561">
						<tr valign="top">
							<td width="561" align="center">



<!--利用者ＩＤテキストボックス-->
<div style="margin-top:17px;">
<table border="0" cellpadding="0" cellspacing="0" width="543">
	<tr>
		<td width="65"><b class="text12">利用者ID</b></td>
		<td width="20"><b class="text12">:</b></td>
		<td width="150"><span class="text12"><c:out value="${ LoginUserData.loginId }" /></span></td>
		<td width="308"></td>
	</tr>
</table>
</div>
<!--/利用者ＩＤテキストボックス-->



<!--利用者名テキストボックス-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="543">
	<tr>
		<td width="65"><b class="text12">利用者名</b></td>
		<td width="20"><b class="text12">:</b></td>
		<td width="150"><span class="text12"><c:out value="${ LoginUserData.loginName }" /></span></td>
		<td width="308"></td>
	</tr>
</table>
</div>
<!--/利用者名テキストボックス-->



<!--パスワードテキスト-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="543">
	<tr>
		<td width="65"><b class="text12">パスワード</b></td>
		<td width="20"><b class="text12">:</b></td>
		<td width="458">
			<span class="text12">********　</span>
			<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
			<%-- <input type="button" value="パスワード変更" class="text12" style="width:120px;" onclick="submitChange()"> --%>
			<input type="button" value="パスワード変更" class="text12" style="width:120px;" onclick="submitChange()"<c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>>
			<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
		</td>
	</tr>
</table>
</div>
<!--/パスワードテキスト-->



<!--利用権限ラベル-->
<div style="margin-top:17px;">
<table border="0" cellpadding="0" cellspacing="0" width="543">
	<tr>
		<td width="543"><b class="text12">利用権限 : </b></td>
	</tr>
</table>
</div>
<!--/利用権限ラベル-->



<!--機能権限テーブル-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="543">
	<tr valign="top">
		<td width="543">
			<table border="0" cellpadding="3" cellspacing="2" width="543">
				<tr align="center">

				<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
				<%--
					<td width="375" bgcolor="#E1E6EB" colspan="3"><b class="text12">メンテナンス権限</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.maintainer }">可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				</tr>
				<tr align="center">
					<td width="125" bgcolor="#E1E6EB" rowspan= "4"><b class="text12">機能権限</b></td>
					<td width="250" bgcolor="#E1E6EB" colspan="2"><b class="text12">Kei-Navi</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.knFunctionFlag == 1 }">可</c:when><c:when test="${ LoginUserData.knFunctionFlag == 2 }">集計データのみ可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				</tr>
				<tr align="center">
					<td width="125" bgcolor="#E1E6EB" rowspan= "2"><b class="text12">校内成績処理</b></td>
					<td width="125" bgcolor="#E1E6EB"><b class="text12">成績分析</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.scFunctionFlag == 1 }">可</c:when><c:when test="${ LoginUserData.scFunctionFlag == 2 }">集計データのみ可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				</tr>
				<tr align="center">
					<td width="125" bgcolor="#E1E6EB"><b class="text12">入試結果調査</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.eeFunctionFlag == 1 }">可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				</tr>
				<tr align="center">
					<td width="250" bgcolor="#E1E6EB" colspan="2"><b class="text12">答案閲覧システム</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.abFunctionFlag == 1 }">可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				--%>

					<td width="375" bgcolor="#E1E6EB" colspan="2"><b class="text12">メンテナンス権限</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.maintainer }">可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				</tr>
				<tr align="center">
					<td width="125" bgcolor="#E1E6EB" rowspan= "2"><b class="text12">機能権限</b></td>
					<td width="250" bgcolor="#E1E6EB" colspan="1"><b class="text12">Kei-Navi</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.knFunctionFlag == 1 }">可</c:when><c:when test="${ LoginUserData.knFunctionFlag == 2 }">集計データのみ可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				</tr>
				<tr align="center">
					<td width="250" bgcolor="#E1E6EB" colspan="1"><b class="text12">答案閲覧システム</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.abFunctionFlag == 1 }">可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END   --%>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>
<!--/機能権限テーブル-->



<!--担当クラス設定変更ボタン-->
<div style="margin-top:17px;">
<table border="0" cellpadding="0" cellspacing="0" width="543">
	<tr>
		<td width="543"><b class="text12">担当クラス : </b></td>
	</tr>
</table>
</div>
<!--/担当クラス設定変更ボタン-->



<!--担当クラステーブル-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="543">
	<tr valign="top">
		<td bgcolor="#FFFFFF" width="543">
			<table border="0" cellpadding="0" cellspacing="0" width="543">
				<tr valign="top">
					<td bgcolor="#8CA9BB">
						<table border="0" cellpadding="9" cellspacing="1" width="543">
							<tr valign="top">
								<td width="543" bgcolor="#FFFFFF"<c:if test="${ empty LoginUserData.chargeClassList }"> align="center"</c:if>>
									<span class="text12"><c:set var="user" value="${ LoginUserData }" /><kn:userClass /></span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>
<!--/担当クラステーブル-->




<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
		<td width="100%">
			<img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt="">
		</td>
	</tr>
</table>
<!--/spacer-->






							</td>
						</tr>
					</table>
				</td>
			</table>
		</td>
	</tr>
</table>
</div>
<!--/メイン枠-->



<!--キャンセル・登録ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="563">
	<tr valign="top">
		<td bgcolor="#FFFFFFF" width="563" align="center">
			<table border="0" cellpadding="0" cellspacing="0" width="563">
				<td bgcolor="#8CA9BB">
					<table border="0" cellpadding="6" cellspacing="1" width="563">
						<tr valign="top">
							<td bgcolor="#FBD49F" align="center">
								<table border="0" cellpadding="0" cellspacing="0" width="206">
									<tr valign="top">
										<td width="45"></td>
										<td width="108">
											<input type="button" value="閉じる" class="text12" style="width:120px;" onclick="self.close()">
										</td>
										<td width="45"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</table>
		</td>
	</tr>
</table>
</div>
<!--/キャンセル・登録ボタン-->

</td>
</tr>
</table>


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

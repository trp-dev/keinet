<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／利用者管理</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	function init() {
		startTimer();
	}

// -->
</script>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="U004Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="<c:out value="${param.forward}" />">

<c:forEach var="loginId" items="${paramValues.loginId}">
<input type="hidden" name="loginId" value="<c:out value="${loginId}" />">
</c:forEach>

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
	<tr valign="top">
		<td width="12">
			<img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br>
		</td>
		<td width="12">
			<img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br>
		</td>
		<td width="960" background="./shared_lib/img/parts/com_bk_t.gif">
			<img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br>
		</td>
		<td width="13">
			<img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
	<tr valign="top">
		<td width="12">
			<img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br>
		</td>
		<td width="8" background="./shared_lib/img/parts/com_bk_l.gif">
			<img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br>
		</td>
		<td width="968" bgcolor="#FFFFFF">


<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->


<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
	<tr valign="top">
		<td width="30">
			<img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br>
		</td>
		<td width="908" align="center">



<!--タイトル-->
<div style="margin-top:23px;">
	<table border="0" cellpadding="0" cellspacing="0" width="908">
		<tr>
			<td width="174">
				<img src="./user/img/ttl_user.gif" width="128" height="24" border="0" alt="利用者管理"><br>
			</td>
			<td width="1" bgcolor="#657681">
				<img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br>
			</td>
			<td width="10">
				<img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br>
			</td>
			<td width="723">
				<img src="./user/img/ttl_user_read.gif" width="364" height="16" border="0" alt="利用者情報を一元的に管理することができます。"><br>
			</td>
		</tr>
	</table>
</div>
<!--/タイトル-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
		<td width="100%">
			<img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt="">
		</td>
	</tr>
</table>
<!--/spacer-->


<!--タイトルバーkkk-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
	<tr valign="top">
		<td width="2" bgcolor="#8CA9BB">
			<img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br>
		</td>
		<td width="892">
			<table border="0" cellpadding="0" cellspacing="0" width="892">
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#8CA9BB">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="2" border="0" alt=""><br>
					</td>
				</tr>
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="3" border="0" alt=""><br>
					</td>
				</tr>
				<tr>
					<td width="5" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br>
					</td>
					<td width="7" bgcolor="#FF8F0E">
						<img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br>
					</td>
					<td width="4" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br>
					</td>
					<td width="576" bgcolor="#758A98">
						<img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt="">
						<b class="text16" style="color:#FFFFFF;">利用者情報登録・編集</b>
					</td>
					<td width="300" bgcolor="#758A98" align="right"><br></td>
				</tr>
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="3" border="0" alt=""><br>
					</td>
				</tr>
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#8CA9BB">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="2" border="0" alt=""><br>
					</td>
				</tr>
			</table>
		</td>
		<td width="14">
			<img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/タイトルバーkkk-->

<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td>

<!--タイトル-->
<div style="margin-top:12px;margin-left:12px;">
<table border="0" cellpadding="0" cellspacing="10" width="580">
<tr>
<td align="center"><span style="font-weight:bold;font-family:Arial Black;color:#FF8F0E;font-size:25px">利用者登録通知</span><br></td>
</tr>
<tr>
<td align="center"><span class="text14-hh">以下のとおり利用者登録を行いました。登録内容を確認してください。</span></td>
</tr>
</table>
</div>
<!--/タイトル-->

<!--メイン枠-->
<div style="margin-top:12px;margin-left:12px;">
<table bgcolor="#8CA9BB" border="0" cellpadding="0" cellspacing="0" width="580">
<tr>
<td>
	<table border="0" cellpadding="10" cellspacing="1" width="580">
	<tr>
	<td align="center" bgcolor="#FFFFFF" width="558">

<!--利用者ＩＤテキストボックス-->
<div>
<table border="0" cellpadding="0" cellspacing="0" width="558">
	<tr>
		<td width="100"><b class="text12">利用者ID</b></td>
		<td width="20"><b class="text12">:</b></td>
		<td width="150"><span class="text12"><c:out value="${ LoginUserData.loginId }" /></span></td>
		<td width="288"><br></td>
	</tr>
</table>
</div>
<!--/利用者ＩＤテキストボックス-->



<!--利用者名テキストボックス-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="558">
	<tr>
		<td width="100"><b class="text12">利用者名</b></td>
		<td width="20"><b class="text12">:</b></td>
		<td width="150"><span class="text12"><c:out value="${ LoginUserData.loginName }" /></span></td>
		<td width="288"><br></td>
	</tr>
</table>
</div>
<!--/利用者名テキストボックス-->



<!--パスワードテキスト-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="558">
	<tr>
		<td width="100"><b class="text12">初期パスワード</b></td>
		<td width="20"><b class="text12">:</b></td>
		<td width="150"><span class="text12"><c:out value="${ LoginUserData.defaultPwd }" /></span></td>
		<td width="288"><br></td>
	</tr>
</table>
</div>
<!--/パスワードテキスト-->



<!--利用権限ラベル-->
<div style="margin-top:17px;">
<table border="0" cellpadding="0" cellspacing="0" width="543">
	<tr>
		<td width="543"><b class="text12">利用権限 : </b></td>
	</tr>
</table>
</div>
<!--/利用権限ラベル-->



<!--機能権限テーブル-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="543">
	<tr valign="top">
		<td width="543">
			<table border="0" cellpadding="3" cellspacing="2" width="543">
				<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD START --%>
				<%--
				<tr align="center">
					<td width="375" bgcolor="#E1E6EB" colspan="3"><b class="text12">メンテナンス権限</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.maintainer }">可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				</tr>
				<tr align="center">
					<td width="125" bgcolor="#E1E6EB" rowspan= "4"><b class="text12">機能権限</b></td>
					<td width="250" bgcolor="#E1E6EB" colspan="2"><b class="text12">Kei-Navi</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.knFunctionFlag == 1 }">可</c:when><c:when test="${ LoginUserData.knFunctionFlag == 2 }">集計データのみ可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				</tr>
				<tr align="center">
					<td width="125" bgcolor="#E1E6EB" rowspan= "2"><b class="text12">校内成績処理</b></td>
					<td width="125" bgcolor="#E1E6EB"><b class="text12">成績分析</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.scFunctionFlag == 1 }">可</c:when><c:when test="${ LoginUserData.scFunctionFlag == 2 }">集計データのみ可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				</tr>
				<tr align="center">
					<td width="125" bgcolor="#E1E6EB"><b class="text12">入試結果調査</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.eeFunctionFlag == 1 }">可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				</tr>
				<tr align="center">
					<td width="250" bgcolor="#E1E6EB" colspan="2"><b class="text12">答案閲覧システム</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.abFunctionFlag == 1 }">可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				</tr>
				--%>
				<tr align="center">
					<td width="375" bgcolor="#E1E6EB" colspan="2"><b class="text12">メンテナンス権限</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.maintainer }">可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				</tr>
				<tr align="center">
					<td width="125" bgcolor="#E1E6EB" rowspan= "2"><b class="text12">機能権限</b></td>
					<td width="250" bgcolor="#E1E6EB" colspan="1"><b class="text12">Kei-Navi</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.knFunctionFlag == 1 }">可</c:when><c:when test="${ LoginUserData.knFunctionFlag == 2 }">集計データのみ可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				</tr>
				<tr align="center">
					<td width="250" bgcolor="#E1E6EB" colspan="1"><b class="text12">答案閲覧システム</b></td>
					<td width="168" bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.abFunctionFlag == 1 }">可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
				</tr>
				<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD END   --%>
			</table>
		</td>
	</tr>
</table>
</div>
<!--/機能権限テーブル-->



<!--担当クラス設定変更ボタン-->
<div style="margin-top:17px;">
<table border="0" cellpadding="0" cellspacing="0" width="543">
	<tr>
		<td width="543"><b class="text12">担当クラス : </b></td>
	</tr>
</table>
</div>
<!--/担当クラス設定変更ボタン-->



<!--担当クラステーブル-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" bgcolor="#8CA9BB" width="543">
	<tr>
		<td>
			<table border="0" cellpadding="10" cellspacing="1" width="543">
				<tr>
					<td bgcolor="#FFFFFF"<c:if test="${ empty LoginUserData.chargeClassList }"> align="center"</c:if>>
						<span class="text12"><c:set var="user" value="${ LoginUserData }" /><kn:userClass /></span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>
<!--/担当クラステーブル-->

		</td>
		</tr>
		</table>
</td>
</tr>
</table>
</div>
<!--/メイン枠-->



<!--キャンセル・登録ボタン-->
<div style="margin-top:15px;margin-left:12px;">
<table border="0" cellpadding="0" cellspacing="0" width="580">
	<td bgcolor="#8CA9BB">
		<table border="0" cellpadding="6" cellspacing="1" width="580">
			<tr>
				<td bgcolor="#FBD49F" align="center">
					<table border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td>
								<input type="button" value="利用者一覧へ戻る" class="text12" style="width:140px;" onclick="submitMenu('u001')">
							</td>
							<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""></td>
							<td>
								<input type="button" value="印刷" class="text12" style="width:120px;" onclick="window.print()">
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</table>
</div>
<!--/キャンセル・登録ボタン-->

</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
		<td width="100%">
			<img src="./shared_lib/img/parts/sp.gif" width="1" height="40" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/spacer-->



		</td>
		<td width="30">
			<img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/コンテンツ-->





		</td>
		<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif">
			<img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
	<tr valign="top">
		<td width="12">
			<img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br>
		</td>
		<td width="12">
			<img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br>
		</td>
		<td width="960" background="./shared_lib/img/parts/com_bk_d.gif">
			<img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br>
		</td>
		<td width="13">
			<img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/下部　ドロップシャドウ-->



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
	<tr valign="top">
		<td width="997">
			<img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/spacer-->


<!--FOOTER-->
<%@ include file="/jsp/admin/footer.jsp" %>
<!--/FOOTER-->

</form>
</body>
</html>

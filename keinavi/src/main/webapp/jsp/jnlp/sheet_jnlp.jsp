<%@ page contentType="application/x-java-jnlp-file;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<?xml version="1.0"?>
<jnlp spec="1.0+"
	codebase="<c:out value="${ CodeBase }" />">

	<information>
		<title>Kei-Navi Download Manager</title>
		<vendor>Gakkohojin Kawaijuku</vendor>
	</information>

	<security>
		<all-permissions />
	</security>

	<resources>
		<j2se version="1.4+" />
		<jar href="jws/kndm2.jar" />
	</resources>

	<%-- 志望大学評価別人数が出力対象となっているか --%>
	<c:set var="univ" value="false" />
	<c:choose>
		<c:when test="${ param.backward == 's104' }">
			<c:set var="univ" value="true" />
		</c:when>
		<c:when test="${ param.backward == 's205' }">
			<c:set var="univ" value="true" />
		</c:when>
		<c:when test="${ param.backward == 's305' }">
			<c:set var="univ" value="true" />
		</c:when>
		<c:when test="${ param.backward == 'c006' }">
			<c:set var="univ" value="true" />
		</c:when>
		<c:when test="${ param.backward == 'c105' }">
			<c:set var="univ" value="true" />
		</c:when>
		<c:when test="${ param.backward == 'b005' }">
			<c:set var="univ" value="true" />
		</c:when>
		<c:when test="${ param.backward == 's001' || param.backward == 's101' || param.backward == 's201' || param.backward == 's301' || param.backward == 's401' }">
			<c:choose>
				<c:when test="${ param.univFlag1 == '1' }">
					<c:set var="univ" value="true" />
				</c:when>
				<c:when test="${ param.univFlag2 == '1' }">
					<c:set var="univ" value="true" />
				</c:when>
				<c:when test="${ param.univFlag3 == '1' }">
					<c:set var="univ" value="true" />
				</c:when>
			</c:choose>
		</c:when>
		<c:when test="${ param.backward == 'c001' || param.backward == 'c101'}">
			<c:choose>
				<c:when test="${ param.univFlag4 == '1' }">
					<c:set var="univ" value="true" />
				</c:when>
				<c:when test="${ param.univFlag5 == '1' }">
					<c:set var="univ" value="true" />
				</c:when>
			</c:choose>
		</c:when>
		<c:when test="${ param.backward == 'b001' || param.backward == 'b101' }">
			<c:choose>
				<c:when test="${ univFlag6 == '1' }">
					<c:set var="univ" value="true" />
				</c:when>
			</c:choose>
		</c:when>
	</c:choose>

	<application-desc main-class="jp.co.fj.keinavi.jws.kndm.DownloadManager">
		<argument><c:out value="${ param.printFlag }" /><c:if test="${ empty(param.printFlag) }">[empty]</c:if></argument>
		<argument><c:out value="${ param.targetYear }" /><c:if test="${ empty(param.targetYear) }">[empty]</c:if></argument>
		<argument><c:out value="${ param.targetExam }" /><c:if test="${ empty(param.targetExam) }">[empty]</c:if></argument>
		<argument><c:out value="${ param.pastExam1 }" /><c:if test="${ empty(param.pastExam1) }">[empty]</c:if></argument>
		<argument><c:out value="${ param.pastExam2 }" /><c:if test="${ empty(param.pastExam2) }">[empty]</c:if></argument>
		<argument><c:out value="${ param.backward }" /><c:if test="${ empty(param.backward) }">[empty]</c:if></argument>
		<argument><c:out value="${ param.targetExamCode1 }" /><c:if test="${ empty(param.targetExamCode1) }">[empty]</c:if></argument>
		<argument><c:out value="${ param.targetExamCode2 }" /><c:if test="${ empty(param.targetExamCode2) }">[empty]</c:if></argument>
		<argument><c:out value="${ param.targetExamCode3 }" /><c:if test="${ empty(param.targetExamCode3) }">[empty]</c:if></argument>
		<argument><c:out value="${ param.targetCandidateRank1 }" /><c:if test="${ empty(param.targetCandidateRank1) }">[empty]</c:if></argument>
		<argument><c:out value="${ param.targetCandidateRank2 }" /><c:if test="${ empty(param.targetCandidateRank2) }">[empty]</c:if></argument>
		<argument><c:out value="${ param.targetCandidateRank3 }" /><c:if test="${ empty(param.targetCandidateRank3) }">[empty]</c:if></argument>
		<argument><c:out value="${ param.button }" /><c:if test="${ empty(param.button) }">[empty]</c:if></argument>
		<argument><c:out value="${ param.sessionId }" /></argument>
		<argument><c:if test="${ univ }">true</c:if><c:if test="${ not univ }">false</c:if></argument>
		<argument><c:out value="${ Cookie }" /><c:if test="${ empty(Cookie) }">[empty]</c:if></argument>
	</application-desc>

</jnlp>

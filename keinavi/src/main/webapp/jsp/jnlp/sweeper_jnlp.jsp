<%@ page contentType="application/x-java-jnlp-file;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<?xml version="1.0"?>
<jnlp spec="1.0+"
	codebase="<c:out value="${ CodeBase }" />">

	<information>
		<title>Kei-Navi Temporary File Sweeper</title>
		<vendor>Gakkohojin Kawaijuku</vendor>
	</information>

	<security>
		<all-permissions />
	</security>

	<resources>
		<j2se version="1.4+" />
		<jar href="jws/knts2.jar" />
	</resources>

	<application-desc main-class="jp.co.fj.keinavi.jws.knts.TempFileSweeper" />

</jnlp>

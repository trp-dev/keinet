<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<jsp:useBean id="InformDetailBean" scope="request" class="jp.co.fj.keinavi.beans.news.InformDetailBean" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<c:if test="${ form.displayDiv == '0' }"><c:set var="title" value="お知らせ" /></c:if>
<c:if test="${ form.displayDiv == '1' }"><c:set var="title" value="Kei-Net注目情報" /></c:if>
<title>Kei-Navi／<c:out value="${title}" /></title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>

<SCRIPT type="text/javascript">
<!--
    /********************************************************************************
    *** SUBMITION *******************************************************************
    ********************************************************************************/
    function submitBack(page) {
        document.forms[0].forward.value = "n001";
        document.forms[0].backward.value = "n002";
        // ページの指定がなければ前回表示ページを指定
        if ( page == "" ) {
            document.forms[0].page.value = "<c:out value="${param.page}" />";
        } else {
            document.forms[0].page.value = "1";
        }
        document.forms[0].displayDiv.value = "<c:out value="${param.displayDiv}" />";
        document.forms[0].submit();
    }

//-->
</SCRIPT>

</head>

<body oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0">
<form action="<c:url value="NewsTop" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="page" value="">
<input type="hidden" name="displayDiv" value="displayDiv">

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="200" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">

<!--ヘルプナビ-->
<!--# include virtual="./shared_lib/include/content/help_link_n001.html"-->
<!--/ヘルプナビ-->

<!-- h001 -->
<% if (InformDetailBean.getFormId().equals("n001") ) { %>

<!--戻るボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="621">
<tr bgcolor="#828282">
<td width="621"><img src="./shared_lib/img/parts/sp.gif" width="621" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="621" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;"  onClick="javascript:submitBack('1')"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr bgcolor="#828282">
<td width="621"><img src="./shared_lib/img/parts/sp.gif" width="621" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/戻るボタン-->

<% } %>
<!--/h001 -->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="561">

<table border="0" cellpadding="0" cellspacing="0" width="561">
<tr valign="top">
<td colspan="4" width="561" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="561" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="561" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="561" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="547" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;"><c:out value="${title}" /></b></td>
</tr>
<tr valign="top">
<td colspan="4" width="561" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="561" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="570" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="570" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!-- h001 -->
<% if (InformDetailBean.getFormId().equals("n001") ) { %>
<!--一覧へ戻る-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="568" bgcolor="#EFF2F3">
<table border="0" cellpadding="4" cellspacing="1" width="568">
<tr valign="top">
<td width="566"><span class="text12">
<a href="javascript:submitBack('')">＜＜一覧へ戻る</a>
</span></td>
</tr>
</table>
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="570" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/一覧へ戻る-->
<% } %>
<!--/h001 -->

<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr>
<td width="550" bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="550">
<!--記事タイトル-->
<tr>
<td width="548" bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="536">
<tr>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="526"><span class="text10"><%=InformDetailBean.getDispDate()%><br></span>
<b class="text12-hh"><%=InformDetailBean.getTitle()%></b></td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<!--/記事タイトル-->
<!--記事-->
<tr>
<td width="548" bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="536">
<tr>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="526">
<span class="text12-hh">
<%=InformDetailBean.getText()%>
</span></td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<!--/記事-->
</table>
</td>
</tr>
</table>
</div>



<!--ボーダー-->
<div style="margin-top:25px;">
<table border="0" cellpadding="0" cellspacing="5" width="570">
<tr valign="top">
<td width="570" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="570" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/ボーダー-->

<!--ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;" onClick="window.close()"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--FOOTER-->
<!--# include virtual="./shared_lib/include/footer/footer_sw_03.html"-->
<!--/FOOTER-->
</form>
</body>
</html>

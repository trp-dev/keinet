<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／メンテナンス</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js">
</script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">
<!--

    <%@ include file="/jsp/script/timer.jsp" %>

	function submitSetting() {
		if (confirm("<kn:message id="x007c" />")) {
			document.forms[0].actionMode.value = "1";
			document.forms[0].submit();
		}
	}

	function submitCancel() {
		if (confirm("<kn:message id="x008c" />")) {
			document.forms[0].actionMode.value = "2";
			document.forms[0].submit();
		}
	}

	function init() {
		startTimer();
		window.focus();
		<c:if test="${ param.actionMode > '0' }">
			if (window.opener.refreshWindow) {
				window.opener.refreshWindow();
			}
		</c:if>
	}

// -->
</script>

</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="X010Servlet" />" method="POST">
<input name="forward" type="hidden" value="x010">
<input name="backward" type="hidden" value="x010">
<input name="actionMode" type="hidden" value="0">
<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="200" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="561">

<table border="0" cellpadding="0" cellspacing="0" width="561">
<tr valign="top">
<td colspan="4" width="561" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="561" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="561" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="561" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="547" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">生徒情報管理（作業中宣言）</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="561" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="561" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="561" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="561" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<div style="margin-top:10px;">
	<div style="width:550px;text-align:left;margin:10px;">
		<span class="text14-hh">生徒情報を登録・修正・削除する時には、「作業中宣言」を行ってください。<br>
		Kei-Naviにて、他の人が登録・修正・削除する際に警告メッセージを表示します。</span>
	</div>
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="541">
	<table border="0" cellpadding="0" cellspacing="0" width="541">
	<tr valign="top">
	<td colspan="4" width="541" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="541" height="1" border="0" alt=""><br></td>
	</tr>
	<tr valign="top">
	<td colspan="4" width="541" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="541" height="3" border="0" alt=""><br></td>
	</tr>
	<tr>
	<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
	<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
	<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
	<td width="527" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">現在の「作業中宣言」状況</b></td>
	</tr>
	<tr valign="top">
	<td colspan="4" width="541" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="541" height="3" border="0" alt=""><br></td>
	</tr>
	</table>
</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="550" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="550" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr>
<td bgcolor="#8CA9BB" width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td bgcolor="#EFF2F3" width="548" align="center">
	<div style="margin:7px 0px 7px 0px;">
	<table bgcolor="#8CA9BB" border="0" cellpadding="0" cellspacing="1" width="534">
	<tr>
	<td bgcolor="#FFFFFF">
		<div style="margin:10px;">
			<c:choose>
			<c:when test="${ empty DeclarationBean.time }">
				<span class="text12">作業中宣言はありません。</span>
			</c:when>
			<c:otherwise>
				<span class="text12"><c:out value="${ DeclarationBean.loginName }" />　さんが作業中宣言しています。<br>登録日時　<kn:formatDate value="${DeclarationBean.time}" pattern="yyyy/MM/dd HH:mm" /></span>
			</c:otherwise>
			</c:choose>
		</div>
		<div style="margin:10px;">
		</div>
	</td>
	</tr>
	</table>
	</div>
</td>
<td bgcolor="#8CA9BB" width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" bgcolor="#8CA9BB" width="550"><img src="./shared_lib/img/parts/sp.gif" width="550" height="1" border="0" alt=""><br></td>
</tr>
</table>
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="541">
	<table border="0" cellpadding="0" cellspacing="0" width="541">
	<tr valign="top">
	<td colspan="4" width="541" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="541" height="1" border="0" alt=""><br></td>
	</tr>
	<tr valign="top">
	<td colspan="4" width="541" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="541" height="3" border="0" alt=""><br></td>
	</tr>
	<tr>
	<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
	<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
	<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
	<td width="527" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">作業中宣言の設定・解除</b></td>
	</tr>
	<tr valign="top">
	<td colspan="4" width="541" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="541" height="3" border="0" alt=""><br></td>
	</tr>
	</table>
</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="550" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="550" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr>
<td bgcolor="#8CA9BB" width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td bgcolor="#EFF2F3" width="548" align="center">
	<div style="margin:7px 0px 7px 0px;">
	<table bgcolor="#8CA9BB" border="0" cellpadding="0" cellspacing="1" width="534">
	<tr>
	<td bgcolor="#FFFFFF">
		<div style="margin:10px;">
			<input type="button" onclick="submitSetting()" value="作業中宣言を設定する" style="width:180px;vertical-align:middle;">&nbsp;&nbsp;<span class="text12">作業中宣言を設定します。</span>
		</div>
		<div style="margin:10px;">
			<input type="button" onclick="submitCancel()" value="作業中宣言を解除する" style="width:180px;vertical-align:middle;">&nbsp;&nbsp;<span class="text12">作業中宣言を解除します。</span>
		</div>
	</td>
	</tr>
	</table>
	</div>
</td>
<td bgcolor="#8CA9BB" width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" bgcolor="#8CA9BB" width="560"><img src="./shared_lib/img/parts/sp.gif" width="550" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<div style="width:100%;text-align:center;margin:10px;">
	<img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓">
</div>
<table bgcolor="#8CA9BB" border="0" cellpadding="7" cellspacing="1" width="550">
<tr>
<td align="center" bgcolor="#FBD49F">
	<input type="button" value="閉じる" class="text12" style="width:100px;" onClick="self.close()">
</td>
</tr>
</table>
</div>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->

<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="635">
<tr valign="top">
<td width="635" align="right"><img src="./shared_lib/include/footer/img/copyright.gif" width="380" height="20" border="0" alt="Copyright: Kawaijuku Educational Information Network"><br></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br>

</form>
</body>
</html>

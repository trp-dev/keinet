<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／メンテナンス</title>
<script type="text/javascript" src="./js/prototype.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<script type="text/javascript" src="./js/JString.js"></script>
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	<%@ include file="/jsp/script/submit_form.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
	<%@ include file="/jsp/script/ctrl_submit.jsp" %>
	<%@ include file="/jsp/script/validate_string.jsp" %>

	var validator = new Validator();

	<%-- 主情報のみ変更 --%>
	function submitRegist() {
		if (validate(false)) {
			submitForm("m101", "1", true);
			showLoadingLayer();
		}
	}

	<%-- 登録・統合 --%>
	function submitIntegration() {
		if (validate(true)) {
			submitForm("m107", "4", true);
		}
	}

	<%-- 主従切替・従情報から外す --%>
	function submitSubAction(actionMode) {
		submitForm("m105", actionMode, true);
	}

	<%-- 受験届修正画面への遷移 --%>
	function submitAnswer(year, code) {
		document.forms[0].initIndividualId.value = "<c:out value="${ form.targetIndividualId }" />";
		document.forms[0].initExamYear.value = year;
		document.forms[0].initExamCd.value = code;
		submitForm("m201");
	}

	<%-- 引数は従情報で上書きするかどうかのフラグ --%>
	function validate(isIntegration) {
		var message = "";
		var form = document.forms[0];

		<%-- クラス --%>
		message += validateClass(form.className, "<kn:message id="x004a" />\n");
		message += validateClass(form.lastClassName, "<kn:message id="x004a" />\n");
		message += validateClass(form.beforeLastClassName, "<kn:message id="x004a" />\n");

		<%-- クラス番号 --%>
		message += validateClassNo(form.classNo, "<kn:message id="x005a" />\n");
		message += validateClassNo(form.lastClassNo, "<kn:message id="x005a" />\n");
		message += validateClassNo(form.beforeLastClassNo, "<kn:message id="x005a" />\n");

		<%-- かな氏名 --%>
		if ((!isIntegration || !document.forms[0].overwriteNameKana.checked)
				&& !isKanaName(form.nameKana.value)) {
			message += "<kn:message id="x006a" />\n";
		}

		<%-- 漢字氏名 --%>
		if ((!isIntegration || !document.forms[0].overwriteNameKanji.checked)
				&& !isKanjiName(form.nameKanji.value)) {
			message += "<kn:message id="x007a" />\n";
		}

		<%-- 生年月日 --%>
		if ((!isIntegration || !document.forms[0].overwriteBirthday.checked)
				&& !isBirthday(form.birthYear.value, form.birthMonth.value, form.birthDate.value)) {
			message += "<kn:message id="x008a" />\n";
		}

		<%-- 電話番号 --%>
		if ((!isIntegration || !document.forms[0].overwriteTelNo.checked)
				 && !isTelNo(form.telNo.value)) {
			message += "<kn:message id="x009a" />\n";
		}

		if (message != "") {
			alert(message);
			return false;
		}

		return true;
	}

	function validateClass(obj, msg) {
		if (obj && (obj.value == "" || !validator.isAlphanumeric(obj.value))) {
			return msg;
		} else {
			return "";
		}
	}

	function validateClassNo(obj, msg) {
		if (obj && (obj.value == "" || !validator.isNumber(obj.value))) {
			return msg;
		} else {
			return "";
		}
	}

	<%-- ロード中レイヤを表示する --%>
	function showLoadingLayer() {
		$("MainLayer").style.visibility = "hidden";
		window.scrollTo(0, 0);
		$("LoadingLayer").style.visibility = "visible";
	}

	function init() {
		startTimer();
		<c:if test="${ form.backward == 'm105' }">
			window.scrollTo(
					document.forms[0].scrollX.value,
					document.forms[0].scrollY.value);
		</c:if>
		<c:choose>
		<%-- 履歴情報重複 --%>
		<c:when test="${ ErrorCode == '1' }">
			<c:choose>
			<c:when test="${ param.forward == 'm107' }">
				<c:set var="errorMessage" value="${ ErrorMessage }\n\n現在の作業を続行しますか？\n\n※統合後は、同じ[年度・学年・クラス・クラス番号]を持つ\n　生徒情報が複数になります。そのままの状態では、今後の\n　受験された模試の結合に支障が出る可能性があります。\n　いずれかの生徒情報を必ず修正しておいてください。" />
			</c:when>
			<c:otherwise>
				<c:set var="errorMessage" value="${ ErrorMessage }\n\n現在の作業を続行しますか？\n\n※統合後は、同じ[年度・学年・クラス・クラス番号]を持つ\n　生徒情報が複数になります。そのままの状態では、今後の\n　受験された模試の結合に支障が出る可能性があります。\n　いずれかの生徒情報を必ず修正しておいてください。" />
			</c:otherwise>
			</c:choose>
			if (confirm("<c:out value="${ errorMessage }" />")) {
				document.forms[0].forcingFlag.value = "1";
				showLoadingLayer();
				submitForm("<c:out value="${ param.forward }" />", "<c:out value="${ param.actionMode }" />");
			}
		</c:when>
		<%-- それ以外のエラー --%>
		<c:when test="${ not empty ErrorMessage }">
			alert("<c:out value="${ ErrorMessage }" />");
		</c:when>
		</c:choose>
	}

// -->
</script>
</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init();" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">

<div id="LoadingLayer" style="position:absolute;top:250px;left:450px;visibility:hidden;">
ロード中です ...
</div>

<div id="MainLayer">
<form action="<c:url value="M105Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="m105">
<input type="hidden" name="actionMode" value="">
<input type="hidden" name="scrollX" value="<c:out value="${ form.scrollX }" />">
<input type="hidden" name="scrollY" value="<c:out value="${ form.scrollY }" />">
<input type="hidden" name="editFlag" value="1">
<input type="hidden" name="targetIndividualId" value="<c:out value="${ form.targetIndividualId }" />">
<input type="hidden" name="oGrade" value="<c:out value="${ form.OGrade }" />">
<input type="hidden" name="oClass" value="<c:out value="${ form.OClass }" />">
<input type="hidden" name="oLastGrade" value="<c:out value="${ form.OLastGrade }" />">
<input type="hidden" name="oLastClassName" value="<c:out value="${ form.OLastClassName }" />">
<input type="hidden" name="oBeforeLastGrade" value="<c:out value="${ form.OBeforeLastGrade }" />">
<input type="hidden" name="oBeforeLastClassName" value="<c:out value="${ form.OBeforeLastClassName }" />">
<input type="hidden" name="forcingFlag" value="">
<input type="hidden" name="initIndividualId" value="">
<input type="hidden" name="initExamYear" value="">
<input type="hidden" name="initExamCd" value="">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">
<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="174"><img src="./mainte/img/ttl_mainte.gif" width="140" height="24" border="0" alt="メンテナンス"><br></td>
<td width="1" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="723"><img src="./mainte/img/ttl_mainte_read.gif" width="326" height="16" border="0" alt="生徒情報を一元的に管理することができます。"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->
<!--コンテンツメニュー-->
<%@ include file="/jsp/admin/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--生徒情報管理（一覧）-->
<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="13" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td colspan="5" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="414" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">生徒情報編集・編集して統合</b></td>
<td width="400" bgcolor="#758A98" align="right"></td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--主情報-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="821">

<table border="0" cellpadding="0" cellspacing="0" width="821">
<tr valign="top">
<td colspan="4" width="821" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="821" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="821" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="821" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="807" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">主情報</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="821" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="821" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="821" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="821" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<%-- 2016/03/10 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<div style="border-style:solid;border-width:0px 1px 1px 1px;border-color:#8CA9BB;width:830px;background-color:#EFF2F3;">
<div style="border-style:solid;border-width:1px;border-color:#8CA9BB;width:808px;background-color:#FFFFFF;margin:10px;padding:12px;">
  --%>
<div style="border-style:solid;border-width:1px 1px 1px 1px;border-color:#8CA9BB;width:830px;background-color:#EFF2F3;">
<div style="border-style:solid;border-width:1px;border-color:#8CA9BB;width:808px;background-color:#FFFFFF;margin:10px;padding:0px;">
<%-- 2016/03/10 QQ)Nishiyama 大規模改修 UPD END   --%>

<!--説明-->
<table border="0" cellpadding="0" cellspacing="0" width="782">
<%-- 2016/03/10 QQ)Nishiyama 大規模改修 ADD START --%>
<tr><td colspan="2"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""></td></tr>
<%-- 2016/03/10 QQ)Nishiyama 大規模改修 ADD END   --%>
<tr>
<td width="20" valign="top"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt=""></td>
<td width="762">
<span class="text14-hh">主情報の生徒情報が表示されています。<br>
編集したい項目を選択または入力の上、「登録」ボタンを押してください。<br>
</span>
<font class="text14-hh" color="#FF6E0E">※生徒情報の編集は、この生徒に関わる全てのデータに影響しますので、<br>
<span style="visibility:hidden;">※</span>編集の際には十分にお気を付けください。</font></td>
</tr>
</table>
<!--/説明-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--編集項目--->
<table border="0" cellpadding="0" cellspacing="0" width="782">
<tr>
<td width="476">

<table border="0" cellpadding="4" cellspacing="2">
<!--対象年度-->
<tr>
<td width="134" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">対象年度</b></td>
</tr>
</table>
</td>

<c:if test="${ form.beforeLastGrade > 0 }">
<td width="100" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ CurrentYear - 2 }" />年度</span></td>
</tr>
</table>
</td>
</c:if>

<c:if test="${ form.lastGrade > 0 }">
<td width="100" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ CurrentYear - 1 }" />年度</span></td>
</tr>
</table>
</td>
</c:if>

<c:if test="${ form.grade > 0 }">
<td width="100" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ CurrentYear }" />年度</span></td>
</tr>
</table>
</td>
</c:if>

</tr>
<!--/対象年度-->
<!--学年-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">学年</b></td>
</tr>
</table>
</td>

<c:if test="${ form.beforeLastGrade > 0 }">
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td>
<select name="beforeLastGrade" class="text12">
<option value="1"<c:if test="${ form.beforeLastGrade == '1' }"> selected</c:if>>1</option>
<option value="2"<c:if test="${ form.beforeLastGrade == '2' }"> selected</c:if>>2</option>
<option value="3"<c:if test="${ form.beforeLastGrade == '3' }"> selected</c:if>>3</option>
<option value="4"<c:if test="${ form.beforeLastGrade == '4' }"> selected</c:if>>4</option>
<option value="5"<c:if test="${ form.beforeLastGrade == '5' }"> selected</c:if>>5</option>
</select>
</td>
</tr>
</table>
</td>
</c:if>

<c:if test="${ form.lastGrade > 0 }">
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td>
<select name="lastGrade" class="text12">
<option value="1"<c:if test="${ form.lastGrade == '1' }"> selected</c:if>>1</option>
<option value="2"<c:if test="${ form.lastGrade == '2' }"> selected</c:if>>2</option>
<option value="3"<c:if test="${ form.lastGrade == '3' }"> selected</c:if>>3</option>
<option value="4"<c:if test="${ form.lastGrade == '4' }"> selected</c:if>>4</option>
<option value="5"<c:if test="${ form.lastGrade == '5' }"> selected</c:if>>5</option>
</select>
</td>
</tr>
</table>
</td>
</c:if>

<c:if test="${ form.grade > 0 }">
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td>
<select name="grade" class="text12">
<option value="1"<c:if test="${ form.grade == '1' }"> selected</c:if>>1</option>
<option value="2"<c:if test="${ form.grade == '2' }"> selected</c:if>>2</option>
<option value="3"<c:if test="${ form.grade == '3' }"> selected</c:if>>3</option>
<option value="4"<c:if test="${ form.grade == '4' }"> selected</c:if>>4</option>
<option value="5"<c:if test="${ form.grade == '5' }"> selected</c:if>>5</option>
</select>
</td>
</tr>
</table>
</td>
</c:if>

</tr>
<!--/学年-->
<!--クラス-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">クラス</b></td>
</tr>
</table>
</td>

<c:if test="${ form.beforeLastGrade > 0 }">
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="3" name="beforeLastClassName" value="<c:out value="${ form.beforeLastClassName }" />" maxlength="2"></td>
</tr>
</table>
</td>
</c:if>

<c:if test="${ form.lastGrade > 0 }">
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="3" name="lastClassName" value="<c:out value="${ form.lastClassName }" />" maxlength="2"></td>
</tr>
</table>
</td>
</c:if>

<c:if test="${ form.grade > 0 }">
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="3" name="className" value="<c:out value="${ form.className }" />" maxlength="2"></td>
</tr>
</table>
</td>
</c:if>

</tr>
<!--/クラス-->
<!--クラス番号-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">クラス番号</b></td>
</tr>
</table>
</td>

<c:if test="${ form.beforeLastGrade > 0 }">
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="6" name="beforeLastClassNo" value="<c:out value="${ form.beforeLastClassNo }" />" maxlength="5"></td>
</tr>
</table>
</td>
</c:if>

<c:if test="${ form.lastGrade > 0 }">
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="6" name="lastClassNo" value="<c:out value="${ form.lastClassNo }" />" maxlength="5"></td>
</tr>
</table>
</td>
</c:if>

<c:if test="${ form.grade > 0 }">
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="6" name="classNo" value="<c:out value="${ form.classNo }" />" maxlength="5"></td>
</tr>
</table>
</td>
</c:if>

</tr>
<!--/クラス番号-->
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="4" cellspacing="2" width="476">
<!--性別-->
<tr>
<td width="134" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">性別</b></td>
</tr>
</table>
</td>
<td width="320" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><select name="sex" class="text12">
<option value="1"<c:if test="${ form.sex == '1' }"> selected</c:if>>男</option>
<option value="2"<c:if test="${ form.sex == '2' }"> selected</c:if>>女</option>
</select></td>
</tr>
</table>
</td>
</tr>
<!--/性別-->
<!--かな氏名-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">かな氏名</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="30" name="nameKana" value="<c:out value="${ form.nameKana }" />" maxlength="14"></td>
</tr>
</table>
</td>
</tr>
<!--/かな氏名-->
<!--漢字氏名-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">漢字氏名</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="30" name="nameKanji" value="<c:out value="${ form.nameKanji }" />" maxlength="20"></td>
</tr>
</table>
</td>
</tr>
<!--/漢字氏名-->
<!--生年月日-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">生年月日</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="4" name="birthYear" value="<c:out value="${ form.birthYear }" />" maxlength="4"></td>
<td><span class="text12">&nbsp;年</span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="2" name="birthMonth" value="<c:out value="${ form.birthMonth }" />" maxlength="2"></td>
<td><span class="text12">&nbsp;月</span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="2" name="birthDate" value="<c:out value="${ form.birthDate }" />"maxlength="2"></td>
<td><span class="text12">&nbsp;日</span></td>
</tr>
</table>
</td>
</tr>
<!--/生年月日-->
<!--電話番号-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">電話番号</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="13" name="telNo" value="<c:out value="${ form.telNo }" />" maxlength="11"></td>
</tr>
</table>
</td>
</tr>
<!--/電話番号-->
</table>

</td>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""></td>
<td width="300" valign="top">
<div style="margin:4px;">
<b class="text12">受験模試一覧</b><BR>
<font class="text12">クリックした模試の受験届情報を表示する画面に切り替わります。</font>
</div>
<div style="width:300px;background-color:#F4E5D6;height:252px;overflow-y:scroll;border-width:2px;border-style:inset;">
<table border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF" width="100%">
<c:set var="takenExamMap" value="${ SelectedStudentBean.mainStudentData.takenExamMap }" />
<c:forEach var="exam" varStatus="status" items="${ ScoreExamListBean.examList }">
	<c:if test="${ takenExamMap[exam.examId] }">
	<tr height="27" bgcolor="#F4E5D6">
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
	<%-- <td><a href="#here" onclick="submitAnswer('<c:out value="${ exam.examYear }" />','<c:out value="${ exam.examCD }" />');"><span class="text12"><c:out value="${ exam.examName }" /></span></a></td> --%>
	<c:choose>
	<c:when test="${ LoginSession.trial }">
		<td><span class="text12"><font color="#2986B1"><u><c:out value="${ exam.examName }" /></u></font></span></td>
	</c:when>
	<c:otherwise>
	    <td><a href="#here" onclick="submitAnswer('<c:out value="${ exam.examYear }" />','<c:out value="${ exam.examCD }" />');"><span class="text12"><c:out value="${ exam.examName }" /></span></a></td>
	</c:otherwise>
	</c:choose>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
	</tr>
	</c:if>
</c:forEach>
</table>
</div>
</td>
</tr>
<%-- 2016/03/10 QQ)Nishiyama 大規模改修 ADD START --%>
<tr><td><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""></td></tr>
<%-- 2016/03/10 QQ)Nishiyama 大規模改修 ADD END --%>
</table>
<!--/編集項目-->

</div>
</div>
<!--/主情報-->

<c:if test="${ not empty SelectedStudentBean.subStudentData }">
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->


<!--従情報-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="821">

<table border="0" cellpadding="0" cellspacing="0" width="821">
<tr valign="top">
<td colspan="4" width="821" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="821" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="821" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="821" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="807" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">従情報</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="821" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="821" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="821" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="821" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<%-- 2016/03/10 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<div style="border-style:solid;border-width:0px 1px 1px 1px;border-color:#8CA9BB;width:830px;background-color:#EFF2F3;">
<div style="border-style:solid;border-width:1px;border-color:#8CA9BB;width:808px;background-color:#FFFFFF;margin:10px;padding:12px;">
  --%>
<div style="border-style:solid;border-width:1px 1px 1px 1px;border-color:#8CA9BB;width:830px;background-color:#EFF2F3;">
<div style="border-style:solid;border-width:1px;border-color:#8CA9BB;width:808px;background-color:#FFFFFF;margin:10px;padding:0px;">
<%-- 2016/03/10 QQ)Nishiyama 大規模改修 UPD END   --%>

<!--説明-->
<table border="0" cellpadding="0" cellspacing="0" width="782">
<%-- 2016/03/10 QQ)Nishiyama 大規模改修 ADD START --%>
<tr><td colspan="2"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""></td></tr>
<%-- 2016/03/10 QQ)Nishiyama 大規模改修 ADD END   --%>
<tr>
<td width="20" valign="top"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt=""></td>
<td width="762">
<span class="text14-hh">従情報の生徒情報が表示されています。必要に応じて統合したい項目を選択し、「統合」ボタンを押してください。<br></span>
</tr>
</table>
<!--/説明-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<%-- 2016/03/10 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2" width="100%">
  --%>
<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2" width="790">
<%-- 2016/03/10 QQ)Nishiyama 大規模改修 UPD END --%>
<tr bgcolor="#CDD7DD">
<td align="center" colspan="3"><span class="text12"><c:out value="${ CurrentYear - 2 }" />年度</span></span><br><span class="text10">学年／クラス／番号</span></td>
<td align="center" colspan="3"><span class="text12"><c:out value="${ CurrentYear - 1 }" />年度</span></span><br><span class="text10">学年／クラス／番号</span></td>
<td align="center" colspan="3"><span class="text12"><c:out value="${ CurrentYear }" />年度</span></span><br><span class="text10">学年／クラス／番号</span></td>
<td align="center"><span class="text12">性別</span></td>
<td align="center"><span class="text12">かな氏名</span></td>
<td align="center"><span class="text12">漢字氏名</span></td>
<td align="center"><span class="text12">生年月日</span></td>
<td align="center"><span class="text12">電話番号</span></td>
</tr>

<c:set var="student" value="${ SelectedStudentBean.subStudentData }" />
<tr height="27" bgcolor="#F4E5D6">
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.beforeLastGrade >= 0 }"><c:out value="${ student.beforeLastGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.beforeLastClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.beforeLastClassNo }" /></span></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.lastGrade >= 0 }"><c:out value="${ student.lastGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.lastClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.lastClassNo }" /></span></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.currentGrade >= 0 }"><c:out value="${ student.currentGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.currentClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.currentClassNo }" /></span></td>
	<td width="4%" align="center"><span class="text12"><c:choose><c:when test="${ student.sex == '1' }">男</c:when><c:when test="${ student.sex == '2' }">女</c:when><c:otherwise>不明</c:otherwise></c:choose></span></td>
	<td width="15%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><input type="checkbox" name="overwriteNameKana" value="<c:out value="${ student.individualId }" />"<c:if test="${ form.overwriteNameKana == student.individualId }"> checked</c:if>></td>
		<td><span class="text12"><c:out value="${ student.nameHiragana }" /></span></td>
		</tr>
		</table>
	</td>
	<td width="14%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><input type="checkbox" name="overwriteNameKanji" value="<c:out value="${ student.individualId }" />"<c:if test="${ form.overwriteNameKanji == student.individualId }"> checked</c:if>></td>
		<td><span class="text12"><c:out value="${ student.nameKanji }" /></span></td>
		</tr>
		</table>
	</td>
	<td width="14%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><input type="checkbox" name="overwriteBirthday" value="<c:out value="${ student.individualId }" />"<c:if test="${ form.overwriteBirthday == student.individualId }"> checked</c:if>></td>
		<td><span class="text12"><kn:formatDate value="${ student.birthday }" pattern="yyyy/MM/dd" /></span></td>
		</tr>
		</table>
	</td>
	<td width="14%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><input type="checkbox" name="overwriteTelNo" value="<c:out value="${ student.individualId }" />"<c:if test="${ form.overwriteTelNo == student.individualId }"> checked</c:if>></td>
		<td><span class="text12"><c:out value="${ student.telNo }" /></span></td>
		</tr>
		</table>
	</td>
</tr>

</table>

<%-- 2016/03/10 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<div style="margin-top:8px;text-align:left;">
  --%>
<div style="margin:8px;text-align:left;">
<%-- 2016/03/10 QQ)Nishiyama 大規模改修 UPD END --%>
<input type="button" value="主従切替" class="text12" style="width:100px;" onclick="submitSubAction('2');">
<input type="button" value="従情報から外す" class="text12" style="width:120px;" onclick="submitSubAction('3');">
</div>

</div>
</div>
<!--/従情報-->
</c:if>

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/生徒情報管理（一覧）-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/概要-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#FBD49F" align="center">

<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
<input type="button" value="生徒情報管理画面へ（※）" class="text12" style="width:150px;" onclick="submitForm('m101');">
<%-- <input type="button" value="主情報のみ変更" class="text12" style="width:120px;" onclick="submitRegist();"> --%>
<input type="button" value="主情報のみ変更" class="text12" style="width:120px;" onclick="submitRegist();"<c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>>
<c:if test="${ not empty SelectedStudentBean.subStudentData }">
<%-- <input type="button" value="変更・統合" class="text12" style="width:120px;" onclick="submitIntegration();"> --%>
<input type="button" value="変更・統合" class="text12" style="width:120px;" onclick="submitIntegration();"<c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>>
</c:if>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<div style="margin-top:8px;text-align:center;">
<span class="text14">※（生徒情報管理−模試結合先変更）画面に戻りたい場合は、右上の受験模試一覧の模試名をクリックしてください。</span>
</div>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/admin/footer.jsp" %>
<!--/FOOTER-->

</form>
</div>

</body>
</html>
<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD END   --%>


<%-- 2016/01/07 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>作業中宣言</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">

	function init() {


	}

</script>
</head>
<body bgcolor="#FFAD8C" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return false" onLoad="init()">
<form>
  --%>
<%-- 2016/01/07 QQ)Nishiyama 大規模改修 DEL END   --%>

<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ↓ダイアログの定義 -->
<div id="workingDialogDiv">
<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD END   --%>

<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr>
<td align="center">

<table border="0" cellpadding="0" cellspacing="1" width="400" bgcolor="#8CA9BB"><tr><td>
<table border="0" cellpadding="0" cellspacing="0" width="398" bgcolor="#FFFFFF">
  <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""></td>
  </tr>
  <tr>
    <td align="center" nowrap>
      <span class="text12"><c:out value="${ DeclarationBean.loginName }" />　さんが作業中宣言しています。<br>登録日時　<kn:formatDate value="${DeclarationBean.time}" pattern="yyyy/MM/dd HH:mm" /><br>作業を続けますか？</span>
    </td>
  </tr>
  <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""></td>
  </tr>
  <tr>
    <td align="center" nowrap>
      <%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD START --%>
      <%--
      <input type="button" style="width:120px" value="キャンセル" onClick="self.close()">
        --%>
      <input type="button" style="width:120px" value="キャンセル" onClick="workingDialogClose(9);">
      <%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD END   --%>

      <img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt="">

      <%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD START --%>
      <%--
      <input type="button" style="width:120px" value="続行" onClick="returnValue=1;self.close()">
        --%>
      <input type="button" style="width:120px" value="続行" onClick="workingDialogClose(1);">
      <%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD END   --%>
    </td>
  </tr>
  <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""></td>
  </tr>
</table>

</td>
</tr>
</table>
</td></tr></table>

<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD START --%>
</div>
<!-- ↑ダイアログの定義 -->
<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD END   --%>

<%-- 2016/01/07 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
</form>
</body>
</html>
  --%>
<%-- 2016/01/07 QQ)Nishiyama 大規模改修 DEL END   --%>

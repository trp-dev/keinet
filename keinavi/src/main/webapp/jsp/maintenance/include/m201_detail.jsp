<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>

<%-- セッションから選択された受験届を取り出す --%>
<c:forEach var="student" items="${ AnswerSheetListBean.answerSheetList }">
	<c:set var="sheetId" value="${ student.examYear },${ student.examCd },${ student.answerSheetNo }" />
	<c:if test="${ param.targetSheetId eq sheetId }">
		<c:set var="StudentData" value="${ student }" />
	</c:if>
</c:forEach>

<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2" width="100%">
<tr height="27">
<td bgcolor="#E1E6EB" width="30%"><b class="text12">&nbsp;性別</b></td>
<td bgcolor="#F4E5D6" width="70%"><span class="text12">&nbsp;<c:choose><c:when test="${ StudentData.sex eq '1' }">男</c:when><c:when test="${ StudentData.sex eq '2' }">女</c:when><c:otherwise>不明</c:otherwise></c:choose></span></td>
</tr>
<tr height="27">
<td bgcolor="#E1E6EB" width="30%"><b class="text12">&nbsp;かな氏名</b></td>
<td bgcolor="#F4E5D6" width="70%"><span class="text12">&nbsp;<c:out value="${ StudentData.nameHiragana }" /></span></td>
</tr>
<tr height="27">
<td bgcolor="#E1E6EB" width="30%"><b class="text12">&nbsp;漢字氏名</b></td>
<td bgcolor="#F4E5D6" width="70%"><span class="text12">&nbsp;<c:out value="${ StudentData.nameKanji }" /></span></td>
</tr>
<tr height="27">
<td bgcolor="#E1E6EB" width="30%"><b class="text12">&nbsp;生年月日</b></td>
<td bgcolor="#F4E5D6" width="70%"><span class="text12">&nbsp;<kn:formatDate value="${ StudentData.birthday }" pattern="yyyy/MM/dd" /></span></td>
</tr>
<tr height="27">
<td bgcolor="#E1E6EB" width="30%"><b class="text12">&nbsp;電話番号</b></td>
<td bgcolor="#F4E5D6" width="70%"><span class="text12">&nbsp;<c:out value="${ StudentData.telNo }" /></span></td>
</tr>
</table>

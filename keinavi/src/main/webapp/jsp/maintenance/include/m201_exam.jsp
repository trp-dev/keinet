<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>

<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2" width="100%">

<c:forEach var="exam" items="${ TakenExamListBean.takenExamList }">
<tr height="27" bgcolor="#F4E5D6">
	<td><span class="text12">&nbsp;&nbsp;<c:out value="${ exam.examName }" /></span></td>
</tr>
</c:forEach>

</table>

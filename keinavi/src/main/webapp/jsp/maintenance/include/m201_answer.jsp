<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>

<c:if test="${ not empty AnswerSheetListBean.answerSheetList }">
<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2" width="100%" id="AnswerTable">

<c:forEach var="student" items="${ AnswerSheetListBean.answerSheetList }">
<tr height="27" bgcolor="#F4E5D6">
	<td width="10%" align="center">
		<a name="<c:out value="${ student.examYear },${ student.examCd },${ student.answerSheetNo }" />"></a>
		<input type="radio" name="targetSheetId" value="<c:out value="${ student.examYear },${ student.examCd },${ student.answerSheetNo }" />" onclick="loadAnswerSheetDetail(this);">
	</td>
	<td width="8%" align="center"><span class="text12"><c:if test="${ student.grade >= 0 }"><c:out value="${ student.grade }" /></c:if></span></td>
	<td width="8%" align="center"><span class="text12"><c:out value="${ student.className }" /></span></td>
	<td width="14%" align="center"><span class="text12"><c:out value="${ student.classNo }" /></span></td>
	<td width="10%" align="center"><span class="text12"><c:choose><c:when test="${ student.sex eq '1' }">男</c:when><c:when test="${ student.sex eq '2' }">女</c:when><c:otherwise>不明</c:otherwise></c:choose></span></td>
	<td width="40%"><span class="text12"><a href="#here" onclick="submitEdit('<c:out value="${ student.individualId }" />');"><c:out value="${ student.nameHiragana }" default="（かな氏名なし）" /></a></span></td>
</tr>
</c:forEach>

</table>
</c:if>

<c:if test="${ empty AnswerSheetListBean.answerSheetList }">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr><td align="center"><b class="text14">該当情報なし</b></td></tr>
</table>
</c:if>
